/*===========================================================================*/
/*   (Cfa/approx.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cfa/approx.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CFA_APPROX_TYPE_DEFINITIONS
#define BGL_CFA_APPROX_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_scnstz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_nodez00;
		obj_t BgL_classz00;
		obj_t BgL_locz00;
	}               *BgL_scnstz00_bglt;

	typedef struct BgL_cvarz00_bgl
	{
		header_t header;
		obj_t widening;
		bool_t BgL_macrozf3zf3;
	}              *BgL_cvarz00_bglt;

	typedef struct BgL_sexitz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_handlerz00;
		bool_t BgL_detachedzf3zf3;
	}               *BgL_sexitz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_literalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}                 *BgL_literalz00_bglt;

	typedef struct BgL_patchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
		struct BgL_varz00_bgl *BgL_refz00;
		long BgL_indexz00;
		obj_t BgL_patchidz00;
	}               *BgL_patchz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_genpatchidz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		long BgL_indexz00;
		long BgL_rindexz00;
	}                    *BgL_genpatchidz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_approxz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		bool_t BgL_typezd2lockedzf3z21;
		obj_t BgL_allocsz00;
		bool_t BgL_topzf3zf3;
		long BgL_lostzd2stampzd2;
		obj_t BgL_dupz00;
		bool_t BgL_haszd2procedurezf3z21;
	}                *BgL_approxz00_bglt;

	typedef struct BgL_cfunzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                      *BgL_cfunzf2cinfozf2_bglt;

	typedef struct BgL_externzd2sfunzf2cinfoz20_bgl
	{
		bool_t BgL_polymorphiczf3zf3;
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                               *BgL_externzd2sfunzf2cinfoz20_bglt;

	typedef struct BgL_internzd2sfunzf2cinfoz20_bgl
	{
		bool_t BgL_polymorphiczf3zf3;
		struct BgL_approxz00_bgl *BgL_approxz00;
		long BgL_stampz00;
	}                               *BgL_internzd2sfunzf2cinfoz20_bglt;

	typedef struct BgL_scnstzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                       *BgL_scnstzf2cinfozf2_bglt;

	typedef struct BgL_prezd2clozd2envz00_bgl
	{
	}                         *BgL_prezd2clozd2envz00_bglt;

	typedef struct BgL_svarzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		bool_t BgL_clozd2envzf3z21;
		long BgL_stampz00;
	}                      *BgL_svarzf2cinfozf2_bglt;

	typedef struct BgL_cvarzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                      *BgL_cvarzf2cinfozf2_bglt;

	typedef struct BgL_sexitzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                       *BgL_sexitzf2cinfozf2_bglt;

	typedef struct BgL_reshapedzd2localzd2_bgl
	{
		obj_t BgL_bindingzd2valuezd2;
	}                          *BgL_reshapedzd2localzd2_bglt;

	typedef struct BgL_reshapedzd2globalzd2_bgl
	{
	}                           *BgL_reshapedzd2globalzd2_bglt;

	typedef struct BgL_literalzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                         *BgL_literalzf2cinfozf2_bglt;

	typedef struct BgL_genpatchidzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                            *BgL_genpatchidzf2cinfozf2_bglt;

	typedef struct BgL_patchzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                       *BgL_patchzf2cinfozf2_bglt;

	typedef struct BgL_kwotezf2nodezf2_bgl
	{
		struct BgL_nodez00_bgl *BgL_nodez00;
	}                      *BgL_kwotezf2nodezf2_bglt;

	typedef struct BgL_kwotezf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                       *BgL_kwotezf2cinfozf2_bglt;

	typedef struct BgL_appzd2lyzf2cinfoz20_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                          *BgL_appzd2lyzf2cinfoz20_bglt;

	typedef struct BgL_funcallzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		struct BgL_approxz00_bgl *BgL_vazd2approxzd2;
		bool_t BgL_arityzd2errorzd2noticedzf3zf3;
		bool_t BgL_typezd2errorzd2noticedzf3zf3;
	}                         *BgL_funcallzf2cinfozf2_bglt;

	typedef struct BgL_setqzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                      *BgL_setqzf2cinfozf2_bglt;

	typedef struct BgL_conditionalzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                             *BgL_conditionalzf2cinfozf2_bglt;

	typedef struct BgL_failzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                      *BgL_failzf2cinfozf2_bglt;

	typedef struct BgL_switchzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                        *BgL_switchzf2cinfozf2_bglt;

	typedef struct BgL_setzd2exzd2itzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                               *BgL_setzd2exzd2itzf2cinfozf2_bglt;

	typedef struct BgL_jumpzd2exzd2itzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                                *BgL_jumpzd2exzd2itzf2cinfozf2_bglt;

	typedef struct BgL_prezd2makezd2boxz00_bgl
	{
	}                          *BgL_prezd2makezd2boxz00_bglt;

	typedef struct BgL_makezd2boxzf2cinfoz20_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                            *BgL_makezd2boxzf2cinfoz20_bglt;

	typedef struct BgL_makezd2boxzf2ozd2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		struct BgL_approxz00_bgl *BgL_valuezd2approxzd2;
	}                                *BgL_makezd2boxzf2ozd2cinfozf2_bglt;

	typedef struct BgL_boxzd2setz12zf2cinfoz32_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                              *BgL_boxzd2setz12zf2cinfoz32_bglt;

	typedef struct BgL_boxzd2refzf2cinfoz20_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                           *BgL_boxzd2refzf2cinfoz20_bglt;

	typedef struct BgL_boxzd2setz12zf2ozd2cinfoze0_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                                  *BgL_boxzd2setz12zf2ozd2cinfoze0_bglt;

	typedef struct BgL_boxzd2refzf2ozd2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                               *BgL_boxzd2refzf2ozd2cinfozf2_bglt;

	typedef struct BgL_tclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		struct BgL_globalz00_bgl *BgL_holderz00;
		obj_t BgL_wideningz00;
		long BgL_depthz00;
		bool_t BgL_finalzf3zf3;
		obj_t BgL_constructorz00;
		obj_t BgL_virtualzd2slotszd2numberz00;
		bool_t BgL_abstractzf3zf3;
		obj_t BgL_widezd2typezd2;
		obj_t BgL_subclassesz00;
	}                *BgL_tclassz00_bglt;


#endif													// BGL_CFA_APPROX_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62approxzd2setzd2topz12z70zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62literalzf2Cinfozd2valuez42zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62nodezd2keyzd2setz12z70zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_setzd2unionzf2typez12ze70zd5zzcfa_approxz00(BgL_approxz00_bglt,
		BgL_approxz00_bglt, obj_t);
	static obj_t BGl_z62appzd2lyzf2Cinfozd2locz90zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2loczf2zzcfa_approxz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62boxzd2refzf2Cinfozd2keyzd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2symbolza2z00zztype_cachez00;
	static BgL_varz00_bglt BGl_z62boxzd2refzf2Cinfozd2varz90zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62reshapedzd2localzd2fastzd2alphazd2setz12z70zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2bodyzd2setz12z50zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_svarz00_bglt
		BGl_makezd2prezd2clozd2envzd2zzcfa_approxz00(obj_t);
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2stackzd2allocatorz42zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t BGl_z62genpatchidzf2Cinfozd2effectz42zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2thezd2closurez42zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sexitzf2Cinfozd2handlerz20zzcfa_approxz00(BgL_sexitz00_bglt);
	BGL_EXPORTED_DECL long
		BGl_reshapedzd2globalzd2occurrencewz00zzcfa_approxz00(BgL_globalz00_bglt);
	extern obj_t BGl_makezd2structzd2appz00zzcfa_info2z00;
	static obj_t BGl_z62approxzd2lostzd2stampz62zzcfa_approxz00(obj_t, obj_t);
	static BgL_boxzd2refzd2_bglt
		BGl_z62boxzd2refzf2Ozd2Cinfozd2nilz42zzcfa_approxz00(obj_t);
	static BgL_typez00_bglt BGl_z62switchzf2Cinfozd2typez42zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2stackzd2allocatorzd2setz12z82zzcfa_approxz00
		(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	BGL_EXPORTED_DECL BgL_sfunz00_bglt
		BGl_makezd2externzd2sfunzf2Cinfozf2zzcfa_approxz00(long, obj_t, obj_t,
		obj_t, bool_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, bool_t,
		BgL_approxz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_funcallzf2Cinfozd2functionsz20zzcfa_approxz00(BgL_funcallz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzf2Cinfozd2failsafezd2setz12ze0zzcfa_approxz00(BgL_cfunz00_bglt,
		obj_t);
	static obj_t BGl_z62prezd2makezd2boxzd2vtypezd2setz12z70zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2stackablezd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_scnstzf2Cinfozd2loczd2setz12ze0zzcfa_approxz00(BgL_scnstz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_literalzf2Cinfozd2valuezd2setz12ze0zzcfa_approxz00(BgL_literalz00_bglt,
		obj_t);
	static BgL_approxz00_bglt
		BGl_z62funcallzf2Cinfozd2approxz42zzcfa_approxz00(obj_t, obj_t);
	static obj_t
		BGl_z62reshapedzd2localzd2volatilezd2setz12za2zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2argszd2setz12z50zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_kwotezf2Cinfozd2locz20zzcfa_approxz00(BgL_kwotez00_bglt);
	static BgL_setzd2exzd2itz00_bglt
		BGl_z62setzd2exzd2itzf2Cinfozd2nilz42zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2argszd2retescapezd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_valuez00_bglt
		BGl_reshapedzd2globalzd2valuez00zzcfa_approxz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_reshapedzd2globalzd2userzf3zf3zzcfa_approxz00(BgL_globalz00_bglt);
	static BgL_approxz00_bglt
		BGl_z62switchzf2Cinfozd2approxz42zzcfa_approxz00(obj_t, obj_t);
	static obj_t
		BGl_z62reshapedzd2globalzd2modulezd2setz12za2zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_genpatchidzf2Cinfozd2sidezd2effectzf2zzcfa_approxz00
		(BgL_genpatchidz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_funcallzf2Cinfozd2arityzd2errorzd2noticedzf3zd3zzcfa_approxz00
		(BgL_funcallz00_bglt);
	static obj_t BGl_z62reshapedzd2globalzd2modulez62zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62makezd2boxzf2Cinfozd2sidezd2effectz42zzcfa_approxz00(obj_t, obj_t);
	static obj_t
		BGl_z62conditionalzf2Cinfozd2sidezd2effectz90zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2initzd2setz12zc0zzcfa_approxz00(BgL_globalz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2boxzf2Ozd2Cinfozd2stackablez20zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt);
	static BgL_approxz00_bglt
		BGl_z62failzf2Cinfozd2approxz42zzcfa_approxz00(obj_t, obj_t);
	static BgL_svarz00_bglt BGl_z62makezd2svarzf2Cinfoz42zzcfa_approxz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_sfunz00_bglt
		BGl_z62internzd2sfunzf2Cinfozd2nilz90zzcfa_approxz00(obj_t);
	static obj_t BGl_z62cvarzf2Cinfozd2macrozf3zb1zzcfa_approxz00(obj_t, obj_t);
	extern obj_t BGl_za2pairzd2nilza2zd2zztype_cachez00;
	BGL_EXPORTED_DECL bool_t BGl_makezd2boxzf2Cinfozf3zd3zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_boxzd2setz12zf2Cinfozd2varzd2setz12z20zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt, BgL_varz00_bglt);
	static obj_t BGl_z62internzd2sfunzf2Cinfozd2arityz90zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL long
		BGl_patchzf2Cinfozd2indexz20zzcfa_approxz00(BgL_patchz00_bglt);
	static obj_t BGl_z62internzd2sfunzf2Cinfozd2stackablez90zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_jumpzd2exzd2itzf2Cinfozd2exitz20zzcfa_approxz00
		(BgL_jumpzd2exzd2itz00_bglt);
	extern obj_t BGl_za2bignumza2z00zztype_cachez00;
	static BgL_nodez00_bglt
		BGl_z62prezd2makezd2boxzd2valuezb0zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2evalzf3zd2setz12z33zzcfa_approxz00
		(BgL_globalz00_bglt, bool_t);
	extern obj_t BGl_approxz00zzcfa_infoz00;
	extern obj_t BGl_setzd2lengthzd2zzcfa_setz00(obj_t);
	static obj_t
		BGl_z62boxzd2refzf2Cinfozd2sidezd2effectzd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62boxzd2refzf2Ozd2Cinfozd2locz42zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_funcallzf2Cinfozd2strengthz20zzcfa_approxz00(BgL_funcallz00_bglt);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_conditionalzf2Cinfozd2truez20zzcfa_approxz00(BgL_conditionalz00_bglt);
	static obj_t BGl_z62externzd2sfunzf2Cinfozf3zb1zzcfa_approxz00(obj_t, obj_t);
	extern obj_t BGl_za2bboolza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2localzd2accessz00zzcfa_approxz00(BgL_localz00_bglt);
	static BgL_approxz00_bglt
		BGl_z62makezd2emptyzd2approxz62zzcfa_approxz00(obj_t);
	static obj_t BGl_z62declarezd2approxzd2setsz12z70zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_funcallzf2Cinfozd2approxz20zzcfa_approxz00(BgL_funcallz00_bglt);
	static obj_t BGl_z62shapezd2approx1580zb0zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62setzd2exzd2itzf2Cinfozd2locz42zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2dssslzd2keywordszd2setz12z82zzcfa_approxz00
		(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzf2Cinfozd2methodzd2setz12ze0zzcfa_approxz00(BgL_cfunz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_genpatchidzf2Cinfozf3z01zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2localzd2bindingzd2valuezd2zzcfa_approxz00(BgL_localz00_bglt);
	static obj_t BGl_z62internzd2sfunzf2Cinfozd2effectz90zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t BGl_z62patchzf2Cinfozd2patchidzd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_makezd2setz12zc0zzcfa_setz00(obj_t);
	BGL_EXPORTED_DECL BgL_literalz00_bglt
		BGl_literalzf2Cinfozd2nilz20zzcfa_approxz00(void);
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t BGl_z62externzd2sfunzf2Cinfozd2effectz90zzcfa_approxz00(obj_t,
		obj_t);
	extern obj_t BGl_continuezd2cfaz12zc0zzcfa_iteratez00(obj_t);
	static obj_t BGl_z62svarzf2Cinfozf3z63zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2failsafezf2zzcfa_approxz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_boxzd2setz12zf2Cinfozd2vtypezd2setz12z20zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt, BgL_typez00_bglt);
	static obj_t
		BGl_z62genpatchidzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2evaluablezf3zd2setz12z33zzcfa_approxz00
		(BgL_globalz00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2propertyzd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	static BgL_typez00_bglt
		BGl_z62appzd2lyzf2Cinfozd2typez90zzcfa_approxz00(obj_t, obj_t);
	static obj_t
		BGl_z62reshapedzd2globalzd2evaluablezf3zd2setz12z51zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62internzd2sfunzf2Cinfozd2locz90zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2propertyzd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62cfunzf2Cinfozd2predicatezd2ofzd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_failz00_bglt
		BGl_failzf2Cinfozd2nilz20zzcfa_approxz00(void);
	static obj_t
		BGl_z62setzd2exzd2itzf2Cinfozd2typezd2setz12z82zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2boxzf2Ozd2Cinfozd2vtypezd2setz12ze0zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt, BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2thezd2closurezd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	static BgL_nodez00_bglt
		BGl_z62boxzd2setz12zf2Cinfozd2valuez82zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_makezd2boxzd2_bglt
		BGl_makezd2prezd2makezd2boxzd2zzcfa_approxz00(obj_t, BgL_typez00_bglt,
		obj_t, obj_t, BgL_nodez00_bglt, BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_genpatchidzf2Cinfozd2indexzd2setz12ze0zzcfa_approxz00
		(BgL_genpatchidz00_bglt, long);
	static obj_t BGl_z62reshapedzd2globalzd2importz62zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2dssslzd2keywordsz20zzcfa_approxz00
		(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_patchzf2Cinfozd2patchidz20zzcfa_approxz00(BgL_patchz00_bglt);
	extern obj_t BGl_za2structza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t
		BGl_setzd2exzd2itzf2Cinfozd2onexitzd2setz12ze0zzcfa_approxz00
		(BgL_setzd2exzd2itz00_bglt, BgL_nodez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2optionalszf2zzcfa_approxz00(BgL_sfunz00_bglt);
	static obj_t
		BGl_z62reshapedzd2localzd2accesszd2setz12za2zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2sidezd2effectzd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62kwotezf2nodezf3z63zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_declarezd2approxzd2setsz12z12zzcfa_approxz00(void);
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2argszd2namez42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_genpatchidzf2Cinfozd2keyzd2setz12ze0zzcfa_approxz00
		(BgL_genpatchidz00_bglt, obj_t);
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2argszd2retescapez42zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2importzd2setz12zc0zzcfa_approxz00
		(BgL_globalz00_bglt, obj_t);
	static obj_t BGl_z62svarzf2Cinfozd2stampz42zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62patchzf2Cinfozf3z63zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_cfunz00_bglt
		BGl_makezd2cfunzf2Cinfoz20zzcfa_approxz00(long, obj_t, obj_t, obj_t, bool_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, bool_t, bool_t, obj_t,
		BgL_approxz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_literalzf2Cinfozd2locz20zzcfa_approxz00(BgL_literalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_switchzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_approxz00
		(BgL_switchz00_bglt, obj_t);
	static BgL_valuez00_bglt
		BGl_z62reshapedzd2globalzd2valuez62zzcfa_approxz00(obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_z62patchzf2Cinfozd2approxz42zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62reshapedzd2globalzd2userzf3z91zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_appzd2lyzd2_bglt
		BGl_makezd2appzd2lyzf2Cinfozf2zzcfa_approxz00(obj_t, BgL_typez00_bglt,
		BgL_nodez00_bglt, BgL_nodez00_bglt, BgL_approxz00_bglt);
	extern obj_t BGl_failz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itzf2Cinfozf2zzcfa_infoz00;
	static BgL_patchz00_bglt BGl_z62patchzf2Cinfozd2nilz42zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_failzf2Cinfozd2locz20zzcfa_approxz00(BgL_failz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2argszd2noescapezd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62cfunzf2Cinfozd2effectz42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_boxzd2setz12zf2Cinfozd2valueze0zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2localzd2fastzd2alphazd2setz12z12zzcfa_approxz00
		(BgL_localz00_bglt, obj_t);
	extern obj_t BGl_sexitz00zzast_varz00;
	BGL_EXPORTED_DECL obj_t
		BGl_setzd2exzd2itzf2Cinfozd2varzd2setz12ze0zzcfa_approxz00
		(BgL_setzd2exzd2itz00_bglt, BgL_varz00_bglt);
	static obj_t BGl_z62genpatchidzf2Cinfozd2rindexz42zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2typezd2setz12zc0zzcfa_approxz00(BgL_globalz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL BgL_kwotez00_bglt
		BGl_makezd2kwotezf2Cinfoz20zzcfa_approxz00(obj_t, BgL_typez00_bglt, obj_t,
		BgL_approxz00_bglt);
	static BgL_nodez00_bglt BGl_z62failzf2Cinfozd2msgz42zzcfa_approxz00(obj_t,
		obj_t);
	static BgL_approxz00_bglt
		BGl_z62literalzf2Cinfozd2approxz42zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzcfa_approxz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2boxzf2Cinfozd2sidezd2effectzd2setz12ze0zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt, obj_t);
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2thezd2closurezd2globalz90zzcfa_approxz00
		(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62jumpzd2exzd2itzf2Cinfozd2exitz42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_boxzd2refzf2Cinfozd2sidezd2effectz20zzcfa_approxz00
		(BgL_boxzd2refzd2_bglt);
	static obj_t
		BGl_z62makezd2boxzf2Cinfozd2sidezd2effectzd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62conditionalzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cfunzf2Cinfozd2methodz42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_conditionalzf2Cinfozd2testz20zzcfa_approxz00(BgL_conditionalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2jvmzd2typezd2namez00zzcfa_approxz00
		(BgL_globalz00_bglt);
	extern obj_t BGl_boxzd2setz12zf2Cinfoz32zzcfa_infoz00;
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2loczd2setz12z32zzcfa_approxz00(BgL_sfunz00_bglt,
		obj_t);
	static obj_t
		BGl_z62boxzd2refzf2Ozd2Cinfozd2varzd2setz12z82zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_setzd2exzd2itzf2Cinfozd2approxz20zzcfa_approxz00
		(BgL_setzd2exzd2itz00_bglt);
	extern obj_t BGl_prezd2makezd2boxz00zzcfa_infoz00;
	static obj_t BGl_z62reshapedzd2localzd2accessz62zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2sidezd2effectz20zzcfa_approxz00
		(BgL_sfunz00_bglt);
	static obj_t BGl_z62approxzd2typezd2lockedzf3z91zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_funcallz00_bglt
		BGl_makezd2funcallzf2Cinfoz20zzcfa_approxz00(obj_t, BgL_typez00_bglt,
		BgL_nodez00_bglt, obj_t, obj_t, obj_t, BgL_approxz00_bglt,
		BgL_approxz00_bglt, bool_t, bool_t);
	static BgL_typez00_bglt BGl_z62approxzd2typezb0zzcfa_approxz00(obj_t, obj_t);
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2effectzd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62boxzd2setz12zf2Ozd2Cinfozd2varzd2setz12z90zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2stackablezd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_literalzf2Cinfozd2valuez20zzcfa_approxz00(BgL_literalz00_bglt);
	extern obj_t BGl_genpatchidzf2Cinfozf2zzcfa_infoz00;
	BGL_EXPORTED_DECL BgL_switchz00_bglt
		BGl_makezd2switchzf2Cinfoz20zzcfa_approxz00(obj_t, BgL_typez00_bglt, obj_t,
		obj_t, BgL_nodez00_bglt, obj_t, BgL_typez00_bglt, BgL_approxz00_bglt);
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2topzf3zd2setz12za3zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62patchzf2Cinfozd2locz42zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62svarzf2Cinfozd2clozd2envzf3z63zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t BGl_z62getzd2nodezd2atomzd2valuezb0zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00(obj_t,
		BgL_approxz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2thezd2closurezd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62internzd2sfunzf2Cinfozd2propertyz90zzcfa_approxz00(obj_t,
		obj_t);
	extern obj_t BGl_appzd2lyzf2Cinfoz20zzcfa_infoz00;
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2fastzd2alphazd2setz12z12zzcfa_approxz00
		(BgL_globalz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_literalzf2Cinfozd2approxz20zzcfa_approxz00(BgL_literalz00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_funcallzf2Cinfozd2typez20zzcfa_approxz00(BgL_funcallz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2strengthzd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	static BgL_boxzd2setz12zc0_bglt
		BGl_z62boxzd2setz12zf2Cinfozd2nilz82zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_boxzd2refzf2Cinfozd2sidezd2effectzd2setz12ze0zzcfa_approxz00
		(BgL_boxzd2refzd2_bglt, obj_t);
	static obj_t
		BGl_z62reshapedzd2globalzd2fastzd2alphazd2setz12z70zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2strengthzd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_appzd2lyzf2Cinfozd2typezd2setz12z32zzcfa_approxz00(BgL_appzd2lyzd2_bglt,
		BgL_typez00_bglt);
	static BgL_globalz00_bglt
		BGl_z62makezd2reshapedzd2globalz62zzcfa_approxz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_switchzf2Cinfozd2sidezd2effectzf2zzcfa_approxz00(BgL_switchz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2sidezd2effectzd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_boxzd2refzd2_bglt
		BGl_boxzd2refzf2Ozd2Cinfozd2nilz20zzcfa_approxz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_prezd2makezd2boxzd2keyzd2zzcfa_approxz00(BgL_makezd2boxzd2_bglt);
	static obj_t
		BGl_z62prezd2makezd2boxzd2stackablezd2setz12z70zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_funcallzf2Cinfozf3z01zzcfa_approxz00(obj_t);
	static BgL_typez00_bglt BGl_z62patchzf2Cinfozd2typez42zzcfa_approxz00(obj_t,
		obj_t);
	static BgL_varz00_bglt
		BGl_z62boxzd2refzf2Ozd2Cinfozd2varz42zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62failzf2Cinfozd2typezd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_sexitzf2Cinfozf3z01zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL BgL_setqz00_bglt
		BGl_setqzf2Cinfozd2nilz20zzcfa_approxz00(void);
	extern obj_t BGl_boxzd2refzf2Ozd2Cinfozf2zzcfa_infoz00;
	BGL_EXPORTED_DECL BgL_setzd2exzd2itz00_bglt
		BGl_setzd2exzd2itzf2Cinfozd2nilz20zzcfa_approxz00(void);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_scnstzf2Cinfozd2approxz20zzcfa_approxz00(BgL_scnstz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_boxzd2refzf2Cinfozd2vtypezd2setz12z32zzcfa_approxz00
		(BgL_boxzd2refzd2_bglt, BgL_typez00_bglt);
	extern obj_t BGl_makezd2boxzf2Cinfoz20zzcfa_infoz00;
	static obj_t BGl_z62cfunzf2Cinfozd2infixzf3zd2setz12z71zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2classzd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_sexitz00_bglt
		BGl_makezd2sexitzf2Cinfoz20zzcfa_approxz00(obj_t, bool_t,
		BgL_approxz00_bglt);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_switchzf2Cinfozd2approxz20zzcfa_approxz00(BgL_switchz00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_prezd2makezd2boxzd2typezd2zzcfa_approxz00(BgL_makezd2boxzd2_bglt);
	static obj_t BGl_z62kwotezf2Cinfozd2valuez42zzcfa_approxz00(obj_t, obj_t);
	static obj_t
		BGl_z62setzd2exzd2itzf2Cinfozd2bodyzd2setz12z82zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	static BgL_varz00_bglt
		BGl_z62setzd2exzd2itzf2Cinfozd2varz42zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62reshapedzd2globalzd2removablez62zzcfa_approxz00(obj_t,
		obj_t);
	static BgL_varz00_bglt BGl_z62patchzf2Cinfozd2refz42zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62makezd2boxzf2Cinfozd2typezd2setz12z50zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62conditionalzf2Cinfozd2typezd2setz12z82zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_setzd2exzd2itzf2Cinfozd2onexitz20zzcfa_approxz00
		(BgL_setzd2exzd2itz00_bglt);
	extern obj_t BGl_reshapedzd2localzd2zzcfa_infoz00;
	static BgL_approxz00_bglt
		BGl_z62setqzf2Cinfozd2approxz42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cfunzf2Cinfozd2macrozf3zd3zzcfa_approxz00(BgL_cfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2localzd2valzd2noescapezd2setz12z12zzcfa_approxz00
		(BgL_localz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2pragmazd2setz12zc0zzcfa_approxz00
		(BgL_globalz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_kwotezf2nodezd2valuez20zzcfa_approxz00(BgL_kwotez00_bglt);
	extern obj_t BGl_cvarzf2Cinfozf2zzcfa_infoz00;
	static obj_t
		BGl_z62boxzd2setz12zf2Cinfozd2valuezd2setz12z42zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_boxzd2refzf2Ozd2Cinfozd2vtypezd2setz12ze0zzcfa_approxz00
		(BgL_boxzd2refzd2_bglt, BgL_typez00_bglt);
	static obj_t BGl_z62prezd2makezd2boxzf3z91zzcfa_approxz00(obj_t, obj_t);
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2stackzd2allocatorz42zzcfa_approxz00(obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62conditionalzf2Cinfozd2falsez42zzcfa_approxz00(obj_t, obj_t);
	extern obj_t BGl_switchzf2Cinfozf2zzcfa_infoz00;
	static obj_t BGl_z62boxzd2setz12zf2Cinfozd2locz82zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t BGl_z62emptyzd2approxzd2alloczf3z91zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_makezd2reshapedzd2globalz00zzcfa_approxz00(obj_t, obj_t,
		BgL_typez00_bglt, BgL_valuez00_bglt, obj_t, obj_t, obj_t, long, long,
		bool_t, obj_t, obj_t, bool_t, bool_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62reshapedzd2localzd2namez62zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_boxzd2refzf2Ozd2Cinfozd2locz20zzcfa_approxz00(BgL_boxzd2refzd2_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_setzd2exzd2itzf2Cinfozf3z01zzcfa_approxz00(obj_t);
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2dssslzd2keywordsz42zzcfa_approxz00(obj_t,
		obj_t);
	static BgL_scnstz00_bglt BGl_z62scnstzf2Cinfozd2nilz42zzcfa_approxz00(obj_t);
	static obj_t
		BGl_z62cfunzf2Cinfozd2argszd2noescapezd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzf2Cinfozd2argszd2noescapezf2zzcfa_approxz00(BgL_cfunz00_bglt);
	static obj_t
		BGl_z62funcallzf2Cinfozd2typezd2errorzd2noticedzf3zb1zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zzcfa_approxz00(void);
	BGL_EXPORTED_DECL BgL_conditionalz00_bglt
		BGl_conditionalzf2Cinfozd2nilz20zzcfa_approxz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_failzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00(BgL_failz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_genpatchidzf2Cinfozd2typez20zzcfa_approxz00(BgL_genpatchidz00_bglt);
	static obj_t BGl_z62reshapedzd2globalzd2srcz62zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setqzf2Cinfozd2locz20zzcfa_approxz00(BgL_setqz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_setzd2exzd2itzf2Cinfozd2locz20zzcfa_approxz00
		(BgL_setzd2exzd2itz00_bglt);
	static obj_t
		BGl_z62conditionalzf2Cinfozd2falsezd2setz12z82zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	static BgL_sfunz00_bglt
		BGl_z62makezd2externzd2sfunzf2Cinfoz90zzcfa_approxz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2polymorphiczf3zd2setz12zc1zzcfa_approxz00
		(BgL_sfunz00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2boxzf2Cinfozd2valuezd2setz12z32zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt, BgL_nodez00_bglt);
	static BgL_approxz00_bglt
		BGl_z62cvarzf2Cinfozd2approxz42zzcfa_approxz00(obj_t, obj_t);
	extern BgL_typez00_bglt
		BGl_getzd2bigloozd2typez00zztype_cachez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL BgL_conditionalz00_bglt
		BGl_makezd2conditionalzf2Cinfoz20zzcfa_approxz00(obj_t, BgL_typez00_bglt,
		obj_t, obj_t, BgL_nodez00_bglt, BgL_nodez00_bglt, BgL_nodez00_bglt,
		BgL_approxz00_bglt);
	static obj_t
		BGl_z62boxzd2refzf2Ozd2Cinfozd2sidezd2effectz90zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2boxzf2Ozd2Cinfozd2stackablezd2setz12ze0zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt, obj_t);
	static BgL_cfunz00_bglt BGl_z62cfunzf2Cinfozd2nilz42zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_boxzd2refzf2Cinfozd2typezf2zzcfa_approxz00(BgL_boxzd2refzd2_bglt);
	static obj_t
		BGl_z62reshapedzd2globalzd2aliaszd2setz12za2zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_boxzd2refzd2_bglt
		BGl_boxzd2refzf2Cinfozd2nilzf2zzcfa_approxz00(void);
	static obj_t
		BGl_z62makezd2boxzf2Ozd2Cinfozd2stackablezd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_z62svarzf2Cinfozd2approxz42zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62forzd2eachzd2approxzd2alloczb0zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_literalzf2Cinfozf2zzcfa_infoz00;
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2thezd2closurez20zzcfa_approxz00
		(BgL_sfunz00_bglt);
	static obj_t BGl_z62structzd2approxzf3z43zzcfa_approxz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62kwotezf2nodezd2typez42zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_approxzd2lostzd2stampzd2setz12zc0zzcfa_approxz00(BgL_approxz00_bglt,
		long);
	BGL_EXPORTED_DECL obj_t
		BGl_switchzf2Cinfozd2itemzd2typezd2setz12z32zzcfa_approxz00
		(BgL_switchz00_bglt, BgL_typez00_bglt);
	static obj_t BGl_z62scnstzf2Cinfozd2locz42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_conditionalzf2Cinfozd2locz20zzcfa_approxz00(BgL_conditionalz00_bglt);
	static BgL_nodez00_bglt BGl_z62appzd2lyzf2Cinfozd2argz90zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t BGl_z62boxzd2refzf2Cinfozd2typezd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62makezd2boxzf2Ozd2Cinfozd2sidezd2effectz90zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62boxzd2setz12zf2Ozd2Cinfozd2vtypezd2setz12z90zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62makezd2boxzf2Cinfozd2valuez90zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2accesszd2setz12zc0zzcfa_approxz00
		(BgL_globalz00_bglt, obj_t);
	static BgL_nodez00_bglt
		BGl_z62conditionalzf2Cinfozd2truez42zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62funcallzf2Cinfozd2strengthz42zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_failzf2Cinfozd2typez20zzcfa_approxz00(BgL_failz00_bglt);
	static obj_t BGl_z62approxzd2allocszb0zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_jumpzd2exzd2itzf2Cinfozf3z01zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_makezd2boxzf2Ozd2Cinfozf3z01zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2topzf3zd2setz12zc1zzcfa_approxz00
		(BgL_sfunz00_bglt, bool_t);
	extern obj_t BGl_za2listza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t
		BGl_prezd2makezd2boxzd2typezd2setz12z12zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt, BgL_typez00_bglt);
	static BgL_approxz00_bglt
		BGl_z62kwotezf2Cinfozd2approxz42zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62cfunzf2Cinfozd2methodzd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2polymorphiczf3zd2setz12zc1zzcfa_approxz00
		(BgL_sfunz00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_boxzd2refzf2Cinfozd2loczf2zzcfa_approxz00(BgL_boxzd2refzd2_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_conditionalzf2Cinfozd2sidezd2effectzf2zzcfa_approxz00
		(BgL_conditionalz00_bglt);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_boxzd2setz12zf2Ozd2Cinfozd2approxz32zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt);
	extern obj_t BGl_prezd2clozd2envz00zzcfa_infoz00;
	BGL_EXPORTED_DECL BgL_svarz00_bglt
		BGl_svarzf2Cinfozd2nilz20zzcfa_approxz00(void);
	static obj_t
		BGl_z62boxzd2setz12zf2Ozd2Cinfozd2typezd2setz12z90zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2argszd2noescapezd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62boxzd2refzf2Cinfozd2keyz90zzcfa_approxz00(obj_t, obj_t);
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2thezd2closurezd2globalzd2setz12z50zzcfa_approxz00
		(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2localzd2valzd2noescapezd2zzcfa_approxz00(BgL_localz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2pragmaz00zzcfa_approxz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2strengthzf2zzcfa_approxz00(BgL_sfunz00_bglt);
	static obj_t
		BGl_z62prezd2makezd2boxzd2sidezd2effectzd2setz12za2zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_funcallzf2Cinfozd2strengthzd2setz12ze0zzcfa_approxz00
		(BgL_funcallz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_conditionalzf2Cinfozd2truezd2setz12ze0zzcfa_approxz00
		(BgL_conditionalz00_bglt, BgL_nodez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2argszd2retescapez20zzcfa_approxz00
		(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL BgL_sexitz00_bglt
		BGl_sexitzf2Cinfozd2nilz20zzcfa_approxz00(void);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_makezd2boxzf2Cinfozd2valuezf2zzcfa_approxz00(BgL_makezd2boxzd2_bglt);
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2predicatezd2ofz42zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_boxzd2setz12zf2Ozd2Cinfozd2valuezd2setz12zf2zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt, BgL_nodez00_bglt);
	static obj_t BGl_z62boxzd2setz12zf2Cinfozf3za3zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_approxzd2typezd2lockedzf3zf3zzcfa_approxz00(BgL_approxz00_bglt);
	static BgL_makezd2boxzd2_bglt
		BGl_z62makezd2boxzf2Cinfozd2nilz90zzcfa_approxz00(obj_t);
	static BgL_conditionalz00_bglt
		BGl_z62conditionalzf2Cinfozd2nilz42zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_cfunzf2Cinfozf3z01zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2accessz00zzcfa_approxz00(BgL_globalz00_bglt);
	static BgL_typez00_bglt
		BGl_z62genpatchidzf2Cinfozd2typez42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2localzd2removablez00zzcfa_approxz00(BgL_localz00_bglt);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_funcallzf2Cinfozd2vazd2approxzf2zzcfa_approxz00(BgL_funcallz00_bglt);
	static obj_t BGl_z62approxzd2dupzd2setz12z70zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_appzd2lyzf2Cinfozd2argzd2setz12z32zzcfa_approxz00(BgL_appzd2lyzd2_bglt,
		BgL_nodez00_bglt);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t
		BGl_patchzf2Cinfozd2valuez20zzcfa_approxz00(BgL_patchz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_prezd2makezd2boxzd2keyzd2setz12z12zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2argszd2retescapezd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_svarzf2Cinfozd2clozd2envzf3z01zzcfa_approxz00(BgL_svarz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_getzd2nodezd2atomzd2valuezd2zzcfa_approxz00(BgL_nodez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_scnstzf2Cinfozd2nodez20zzcfa_approxz00(BgL_scnstz00_bglt);
	extern obj_t BGl_svarz00zzast_varz00;
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2classzd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2failsafezf2zzcfa_approxz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_svarzf2Cinfozd2locz20zzcfa_approxz00(BgL_svarz00_bglt);
	static BgL_approxz00_bglt
		BGl_z62unionzd2approxzd2filterz12z70zzcfa_approxz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62makezd2boxzf2Ozd2Cinfozd2keyz42zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62reshapedzd2localzd2valzd2noescapezd2setz12z70zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_prezd2makezd2boxzd2valuezd2zzcfa_approxz00(BgL_makezd2boxzd2_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_literalzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00(BgL_literalz00_bglt,
		BgL_typez00_bglt);
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2predicatezd2ofzd2setz12z82zzcfa_approxz00
		(obj_t, obj_t, obj_t);
	static BgL_typez00_bglt
		BGl_z62reshapedzd2localzd2typez62zzcfa_approxz00(obj_t, obj_t);
	extern obj_t BGl_failzf2Cinfozf2zzcfa_infoz00;
	BGL_EXPORTED_DECL bool_t
		BGl_externzd2sfunzf2Cinfozf3zd3zzcfa_approxz00(obj_t);
	static BgL_varz00_bglt
		BGl_z62boxzd2setz12zf2Cinfozd2varz82zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62reshapedzd2globalzd2namez62zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2classzf2zzcfa_approxz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62makezd2boxzf2Cinfozd2locz90zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62conditionalzf2Cinfozd2locz42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_varz00_bglt
		BGl_boxzd2refzf2Ozd2Cinfozd2varz20zzcfa_approxz00(BgL_boxzd2refzd2_bglt);
	static BgL_nodez00_bglt
		BGl_z62conditionalzf2Cinfozd2testz42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2stackablezd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62cfunzf2Cinfozd2topzf3zd2setz12z71zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_z62setzd2exzd2itzf2Cinfozd2approxz42zzcfa_approxz00(obj_t, obj_t);
	static obj_t
		BGl_z62genpatchidzf2Cinfozd2rindexzd2setz12z82zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62makezd2boxzf2Cinfozd2stackablez90zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t BGl_z62funcallzf2Cinfozd2functionsz42zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_varz00_bglt
		BGl_setqzf2Cinfozd2varz20zzcfa_approxz00(BgL_setqz00_bglt);
	static BgL_typez00_bglt
		BGl_z62prezd2makezd2boxzd2vtypezb0zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_varz00_bglt
		BGl_setzd2exzd2itzf2Cinfozd2varz20zzcfa_approxz00
		(BgL_setzd2exzd2itz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2boxzf2Ozd2Cinfozd2sidezd2effectzd2setz12z32zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt, obj_t);
	static obj_t
		BGl_z62makezd2boxzf2Ozd2Cinfozd2approxzd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_cvarz00_bglt
		BGl_makezd2cvarzf2Cinfoz20zzcfa_approxz00(bool_t, BgL_approxz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_switchzf2Cinfozd2testzd2setz12ze0zzcfa_approxz00(BgL_switchz00_bglt,
		BgL_nodez00_bglt);
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2dssslzd2keywordszd2setz12z82zzcfa_approxz00
		(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_failzf2Cinfozd2procz20zzcfa_approxz00(BgL_failz00_bglt);
	extern obj_t BGl_setqzf2Cinfozf2zzcfa_infoz00;
	static obj_t
		BGl_z62genpatchidzf2Cinfozd2typezd2setz12z82zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2boxzf2Ozd2Cinfozd2keyz20zzcfa_approxz00(BgL_makezd2boxzd2_bglt);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_unionzd2approxzd2filterz12z12zzcfa_approxz00(BgL_approxz00_bglt,
		BgL_approxz00_bglt);
	static obj_t BGl_z62svarzf2Cinfozd2loczd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cfunzf2Cinfozd2arityz42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2thezd2closurezd2globalzd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2localzd2valuezd2setz12zc0zzcfa_approxz00(BgL_localz00_bglt,
		BgL_valuez00_bglt);
	static obj_t BGl_z62switchzf2Cinfozd2keyz42zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62reshapedzd2globalzd2evaluablezf3z91zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_patchzf2Cinfozd2indexzd2setz12ze0zzcfa_approxz00(BgL_patchz00_bglt,
		long);
	BGL_EXPORTED_DECL obj_t
		BGl_emptyzd2approxzd2alloczf3zf3zzcfa_approxz00(BgL_approxz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2localzd2namez00zzcfa_approxz00(BgL_localz00_bglt);
	static obj_t
		BGl_z62boxzd2setz12zf2Cinfozd2varzd2setz12z42zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2bodyzd2setz12z50zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2namez00zzcfa_approxz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_conditionalzf2Cinfozd2testzd2setz12ze0zzcfa_approxz00
		(BgL_conditionalz00_bglt, BgL_nodez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzf2Cinfozd2thezd2closurezd2setz12z32zzcfa_approxz00
		(BgL_cfunz00_bglt, obj_t);
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2predicatezd2ofzd2setz12z82zzcfa_approxz00
		(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2srcz00zzcfa_approxz00(BgL_globalz00_bglt);
	static obj_t BGl_z62appzd2lyzf2Cinfozd2typezd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_setzd2anyzd2zzcfa_setz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzf2Cinfozd2topzf3zd2setz12z13zzcfa_approxz00(BgL_cfunz00_bglt,
		bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzf2Cinfozd2predicatezd2ofzf2zzcfa_approxz00(BgL_cfunz00_bglt);
	static BgL_nodez00_bglt BGl_z62setqzf2Cinfozd2valuez42zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62genpatchidzf2Cinfozd2exprza2zd2setz12z20zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2effectzd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2localzd2volatilezd2setz12zc0zzcfa_approxz00
		(BgL_localz00_bglt, bool_t);
	static BgL_typez00_bglt
		BGl_z62boxzd2setz12zf2Cinfozd2vtypez82zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_funcallzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00(BgL_funcallz00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_cfunz00zzast_varz00;
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzf2Cinfozd2argszd2retescapezf2zzcfa_approxz00(BgL_cfunz00_bglt);
	static BgL_typez00_bglt BGl_z62setqzf2Cinfozd2typez42zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_approxz00
		(BgL_cfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_cfunzf2Cinfozd2approxz20zzcfa_approxz00(BgL_cfunz00_bglt);
	static obj_t BGl_z62scnstzf2Cinfozd2classz42zzcfa_approxz00(obj_t, obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_patchzf2Cinfozd2approxz20zzcfa_approxz00(BgL_patchz00_bglt);
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2argszd2setz12z50zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62appzd2lyzf2Cinfozd2argzd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static BgL_scnstz00_bglt BGl_z62makezd2scnstzf2Cinfoz42zzcfa_approxz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_reshapedzd2localzd2occurrencez00zzcfa_approxz00(BgL_localz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_setqzf2Cinfozf3z01zzcfa_approxz00(obj_t);
	static obj_t BGl_z62prezd2makezd2boxzd2keyzd2setz12z70zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62approxzf3z91zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_appzd2lyzf2Cinfozd2funzf2zzcfa_approxz00(BgL_appzd2lyzd2_bglt);
	BGL_EXPORTED_DECL BgL_varz00_bglt
		BGl_boxzd2refzf2Cinfozd2varzf2zzcfa_approxz00(BgL_boxzd2refzd2_bglt);
	static BgL_nodez00_bglt
		BGl_z62setzd2exzd2itzf2Cinfozd2onexitz42zzcfa_approxz00(obj_t, obj_t);
	static obj_t
		BGl_z62funcallzf2Cinfozd2arityzd2errorzd2noticedzf3zb1zzcfa_approxz00(obj_t,
		obj_t);
	extern obj_t BGl_kwotezf2Cinfozf2zzcfa_infoz00;
	static obj_t
		BGl_z62approxzd2checkzd2haszd2procedurezf3z43zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_boxzd2refzf2Cinfozf3zd3zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2dssslzd2keywordsz20zzcfa_approxz00
		(BgL_sfunz00_bglt);
	extern obj_t BGl_za2bllongza2z00zztype_cachez00;
	static obj_t BGl_z62externzd2sfunzf2Cinfozd2propertyz90zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2predicatezd2ofz20zzcfa_approxz00
		(BgL_sfunz00_bglt);
	static obj_t
		BGl_z62approxzd2haszd2procedurezf3zd2setz12z51zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_patchz00_bglt
		BGl_makezd2patchzf2Cinfoz20zzcfa_approxz00(obj_t, BgL_typez00_bglt, obj_t,
		BgL_varz00_bglt, long, obj_t, BgL_approxz00_bglt);
	static obj_t BGl_z62externzd2sfunzf2Cinfozd2stackablez90zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL long
		BGl_approxzd2lostzd2stampz00zzcfa_approxz00(BgL_approxz00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_switchzf2Cinfozd2itemzd2typezf2zzcfa_approxz00(BgL_switchz00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_switchzf2Cinfozd2typez20zzcfa_approxz00(BgL_switchz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_prezd2makezd2boxzd2stackablezd2zzcfa_approxz00(BgL_makezd2boxzd2_bglt);
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2argszd2retescapez42zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62boxzd2refzf2Ozd2Cinfozd2keyzd2setz12z82zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_reshapedzd2localzf3z21zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_boxzd2setz12zf2Cinfozd2vtypeze0zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt);
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2stampzd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cfunzf2Cinfozd2topzf3zd3zzcfa_approxz00(BgL_cfunz00_bglt);
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2stackzd2allocatorzd2setz12z82zzcfa_approxz00
		(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_approxz00_bglt,
		BgL_approxz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzf2Cinfozd2stackzd2allocatorzf2zzcfa_approxz00(BgL_cfunz00_bglt);
	extern obj_t BGl_disablezd2Xzd2Tz12z12zzcfa_procedurez00(BgL_approxz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL BgL_switchz00_bglt
		BGl_switchzf2Cinfozd2nilz20zzcfa_approxz00(void);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_failzf2Cinfozd2approxz20zzcfa_approxz00(BgL_failz00_bglt);
	static BgL_conditionalz00_bglt
		BGl_z62makezd2conditionalzf2Cinfoz42zzcfa_approxz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzf2Cinfozd2predicatezd2ofzd2setz12z32zzcfa_approxz00
		(BgL_cfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cvarzf2Cinfozd2macrozf3zd3zzcfa_approxz00(BgL_cvarz00_bglt);
	static obj_t BGl_z62reshapedzd2globalzd2idz62zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzcfa_approxz00(void);
	BGL_EXPORTED_DECL long
		BGl_reshapedzd2localzd2occurrencewz00zzcfa_approxz00(BgL_localz00_bglt);
	BGL_EXPORTED_DECL BgL_boxzd2setz12zc0_bglt
		BGl_boxzd2setz12zf2Cinfozd2nilze0zzcfa_approxz00(void);
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2thezd2closurezd2setz12z82zzcfa_approxz00
		(obj_t, obj_t, obj_t);
	extern obj_t BGl_internzd2sfunzf2Cinfoz20zzcfa_infoz00;
	static obj_t
		BGl_z62makezd2boxzf2Ozd2Cinfozd2valuezd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62jumpzd2exzd2itzf2Cinfozd2valuezd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzf2Cinfozd2argszd2noescapezd2setz12z32zzcfa_approxz00
		(BgL_cfunz00_bglt, obj_t);
	static obj_t BGl_z62cfunzf2Cinfozd2infixzf3zb1zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2classzf2zzcfa_approxz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62approxzd2lostzd2stampzd2setz12za2zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2boxzf2Ozd2Cinfozd2keyzd2setz12ze0zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt, obj_t);
	static BgL_typez00_bglt
		BGl_z62reshapedzd2globalzd2typez62zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setqzf2Cinfozd2valuezd2setz12ze0zzcfa_approxz00(BgL_setqz00_bglt,
		BgL_nodez00_bglt);
	extern obj_t BGl_kwotezf2nodezf2zzcfa_infoz00;
	static obj_t BGl_z62getzd2nodezd2atomzd2valuezd21584z62zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_makezd2emptyzd2approxz00zzcfa_approxz00(void);
	static obj_t BGl_z62getzd2nodezd2atomzd2valuezd21586z62zzcfa_approxz00(obj_t,
		obj_t);
	static BgL_boxzd2refzd2_bglt
		BGl_z62makezd2boxzd2refzf2Cinfoz90zzcfa_approxz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2stackzd2allocatorzd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2optionalszf2zzcfa_approxz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62boxzd2refzf2Ozd2Cinfozd2keyz42zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2sidezd2effectzd2setz12z82zzcfa_approxz00
		(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2localzd2userzf3zd2setz12z33zzcfa_approxz00(BgL_localz00_bglt,
		bool_t);
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2argszd2namez42zzcfa_approxz00(obj_t, obj_t);
	extern obj_t BGl_sexitzf2Cinfozf2zzcfa_infoz00;
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2stackablezd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cfunzf2Cinfozf3z63zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62sexitzf2Cinfozf3z63zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62cfunzf2Cinfozd2failsafezd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62literalzf2Cinfozd2valuezd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62prezd2makezd2boxzd2typezd2setz12z70zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62reshapedzd2globalzd2namezd2setz12za2zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static BgL_localz00_bglt
		BGl_z62reshapedzd2localzd2nilz62zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_switchzf2Cinfozd2locz20zzcfa_approxz00(BgL_switchz00_bglt);
	static BgL_globalz00_bglt
		BGl_z62reshapedzd2globalzd2nilz62zzcfa_approxz00(obj_t);
	static BgL_cvarz00_bglt BGl_z62cvarzf2Cinfozd2nilz42zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_appzd2lyzf2Cinfozd2typezf2zzcfa_approxz00(BgL_appzd2lyzd2_bglt);
	BGL_EXPORTED_DECL BgL_appzd2lyzd2_bglt
		BGl_appzd2lyzf2Cinfozd2nilzf2zzcfa_approxz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzf2Cinfozd2thezd2closurezf2zzcfa_approxz00(BgL_cfunz00_bglt);
	extern obj_t BGl_setzd2everyzd2zzcfa_setz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_failz00_bglt
		BGl_makezd2failzf2Cinfoz20zzcfa_approxz00(obj_t, BgL_typez00_bglt,
		BgL_nodez00_bglt, BgL_nodez00_bglt, BgL_nodez00_bglt, BgL_approxz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2bodyzd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_boxzd2setz12zf2Cinfozd2locze0zzcfa_approxz00(BgL_boxzd2setz12zc0_bglt);
	static obj_t BGl_z62reshapedzd2globalzd2initz62zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_patchzf2Cinfozd2valuezd2setz12ze0zzcfa_approxz00(BgL_patchz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_reshapedzd2localzd2typez00zzcfa_approxz00(BgL_localz00_bglt);
	static obj_t
		BGl_z62reshapedzd2globalzd2valuezd2setz12za2zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62reshapedzd2globalzd2pragmaz62zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_reshapedzd2globalzd2typez00zzcfa_approxz00(BgL_globalz00_bglt);
	static BgL_makezd2boxzd2_bglt
		BGl_z62prezd2makezd2boxzd2nilzb0zzcfa_approxz00(obj_t);
	static BgL_approxz00_bglt
		BGl_unionzd2approxzf2typez12z32zzcfa_approxz00(BgL_approxz00_bglt,
		BgL_approxz00_bglt, BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sexitzf2Cinfozd2detachedzf3zd2setz12z13zzcfa_approxz00
		(BgL_sexitz00_bglt, bool_t);
	BGL_EXPORTED_DECL BgL_setqz00_bglt
		BGl_makezd2setqzf2Cinfoz20zzcfa_approxz00(obj_t, BgL_typez00_bglt,
		BgL_varz00_bglt, BgL_nodez00_bglt, BgL_approxz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2argszd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62reshapedzd2globalzd2accessz62zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2modulezd2setz12zc0zzcfa_approxz00
		(BgL_globalz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_funcallzf2Cinfozd2argszd2setz12ze0zzcfa_approxz00(BgL_funcallz00_bglt,
		obj_t);
	static BgL_nodez00_bglt BGl_z62kwotezf2nodezd2nodez42zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_appzd2lyzf2Cinfozd2loczf2zzcfa_approxz00(BgL_appzd2lyzd2_bglt);
	static BgL_svarz00_bglt BGl_z62prezd2clozd2envzd2nilzb0zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2failsafezd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_svarzf2Cinfozd2loczd2setz12ze0zzcfa_approxz00(BgL_svarz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_conditionalzf2Cinfozd2keyzd2setz12ze0zzcfa_approxz00
		(BgL_conditionalz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzf2Cinfozd2effectz20zzcfa_approxz00(BgL_cfunz00_bglt);
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2failsafezd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2epairza2z00zztype_cachez00;
	static obj_t BGl_z62reshapedzd2globalzd2occurrencez62zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2initz00zzcfa_approxz00(BgL_globalz00_bglt);
	static obj_t
		BGl_z62reshapedzd2globalzd2libraryzd2setz12za2zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62reshapedzd2localzd2removablezd2setz12za2zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2stackablezf2zzcfa_approxz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_prezd2makezd2boxzd2sidezd2effectzd2setz12zc0zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt, obj_t);
	static obj_t
		BGl_z62genpatchidzf2Cinfozd2effectzd2setz12z82zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2thezd2closurezd2setz12z82zzcfa_approxz00
		(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_kwotezf2Cinfozd2typezd2setz12ze0zzcfa_approxz00(BgL_kwotez00_bglt,
		BgL_typez00_bglt);
	BGL_IMPORT obj_t make_struct(obj_t, int, obj_t);
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2dssslzd2keywordsz42zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzf2Cinfozd2methodz20zzcfa_approxz00(BgL_cfunz00_bglt);
	static obj_t BGl_z62prezd2makezd2boxzd2loczb0zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2strengthzf2zzcfa_approxz00(BgL_sfunz00_bglt);
	extern obj_t BGl_svarzf2Cinfozf2zzcfa_infoz00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_makezd2procedurezd2appz00zzcfa_info2z00;
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2sidezd2effectzd2setz12z82zzcfa_approxz00
		(obj_t, obj_t, obj_t);
	static obj_t BGl_z62setqzf2Cinfozd2valuezd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_boxzd2setz12zf2Ozd2Cinfozd2valuez32zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt);
	BGL_EXPORTED_DECL bool_t BGl_cvarzf2Cinfozf3z01zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_sexitzf2Cinfozd2detachedzf3zd3zzcfa_approxz00(BgL_sexitz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_prezd2makezd2boxzd2valuezd2setz12z12zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt, BgL_nodez00_bglt);
	static obj_t BGl_z62failzf2Cinfozd2msgzd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_kwotezf2Cinfozd2approxz20zzcfa_approxz00(BgL_kwotez00_bglt);
	static obj_t BGl_z62getzd2nodezd2atomzd2value1581zb0zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2localzd2fastzd2alphazd2zzcfa_approxz00(BgL_localz00_bglt);
	static obj_t BGl_z62prezd2clozd2envzd2loczb0zzcfa_approxz00(obj_t, obj_t);
	static BgL_typez00_bglt
		BGl_z62makezd2boxzf2Cinfozd2vtypez90zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62switchzf2Cinfozd2testzd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_reshapedzd2globalzd2evalzf3zf3zzcfa_approxz00(BgL_globalz00_bglt);
	static obj_t BGl_z62setqzf2Cinfozf3z63zzcfa_approxz00(obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62switchzf2Cinfozd2testz42zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t BGl_z62reshapedzd2localzd2idz62zzcfa_approxz00(obj_t, obj_t);
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2polymorphiczf3z63zzcfa_approxz00(obj_t,
		obj_t);
	extern bool_t BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2localzd2occurrencewzd2setz12zc0zzcfa_approxz00
		(BgL_localz00_bglt, long);
	BGL_EXPORTED_DECL obj_t
		BGl_jumpzd2exzd2itzf2Cinfozd2exitzd2setz12ze0zzcfa_approxz00
		(BgL_jumpzd2exzd2itz00_bglt, BgL_nodez00_bglt);
	static obj_t
		BGl_z62reshapedzd2localzd2occurrencewzd2setz12za2zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62patchzf2Cinfozd2indexzd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62setzd2exzd2itzf2Cinfozd2varzd2setz12z82zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62reshapedzd2globalzd2removablezd2setz12za2zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2jvmzd2typezd2namezd2setz12zc0zzcfa_approxz00
		(BgL_globalz00_bglt, obj_t);
	static obj_t
		BGl_z62makezd2boxzf2Ozd2Cinfozd2typezd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62jumpzd2exzd2itzf2Cinfozd2typezd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62kwotezf2Cinfozd2typez42zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2argszd2namez20zzcfa_approxz00(BgL_sfunz00_bglt);
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2loczd2setz12z50zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_makezd2boxzd2_bglt
		BGl_makezd2boxzf2Cinfozd2nilzf2zzcfa_approxz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_conditionalzf2Cinfozd2falsezd2setz12ze0zzcfa_approxz00
		(BgL_conditionalz00_bglt, BgL_nodez00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_prezd2clozd2envzf3zf3zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_boxzd2setz12zf2Cinfozd2approxze0zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt);
	BGL_EXPORTED_DECL BgL_boxzd2setz12zc0_bglt
		BGl_makezd2boxzd2setz12zf2Ozd2Cinfoz32zzcfa_approxz00(obj_t,
		BgL_typez00_bglt, BgL_varz00_bglt, BgL_nodez00_bglt, BgL_typez00_bglt,
		BgL_approxz00_bglt);
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2sidezd2effectz42zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2localzd2namezd2setz12zc0zzcfa_approxz00(BgL_localz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_kwotezf2Cinfozd2valuez20zzcfa_approxz00(BgL_kwotez00_bglt);
	static obj_t
		BGl_z62reshapedzd2globalzd2userzf3zd2setz12z51zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_za2nilza2z00zz__evalz00;
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_makezd2boxzf2Cinfozd2vtypezf2zzcfa_approxz00(BgL_makezd2boxzd2_bglt);
	static obj_t BGl_z62approxzd2topzf3zd2setz12z83zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	static BgL_approxz00_bglt BGl_z62makezd2typezd2approxz62zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62makezd2boxzf2Ozd2Cinfozd2sidezd2effectzd2setz12z50zzcfa_approxz00
		(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62setzd2exzd2itzf2Cinfozd2bodyz42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_setqzf2Cinfozd2approxz20zzcfa_approxz00(BgL_setqz00_bglt);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_unionzd2approx2z12zc0zzcfa_approxz00(BgL_approxz00_bglt,
		BgL_approxz00_bglt);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_makezd2approxzd2zzcfa_approxz00(BgL_typez00_bglt, bool_t, obj_t, bool_t,
		long, obj_t, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2keyszf2zzcfa_approxz00(BgL_sfunz00_bglt);
	static BgL_approxz00_bglt
		BGl_z62boxzd2setz12zf2Ozd2Cinfozd2approxz50zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_literalzf2Cinfozf3z01zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL BgL_kwotez00_bglt
		BGl_makezd2kwotezf2nodez20zzcfa_approxz00(obj_t, BgL_typez00_bglt, obj_t,
		BgL_nodez00_bglt);
	static obj_t BGl_z62reshapedzd2globalzd2initzd2setz12za2zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2strengthzd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_kwotez00_bglt
		BGl_kwotezf2nodezd2nilz20zzcfa_approxz00(void);
	static BgL_failz00_bglt BGl_z62failzf2Cinfozd2nilz42zzcfa_approxz00(obj_t);
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2strengthzd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static BgL_typez00_bglt
		BGl_z62boxzd2setz12zf2Cinfozd2typez82zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62kwotezf2Cinfozd2typezd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_varz00_bglt
		BGl_boxzd2setz12zf2Cinfozd2varze0zzcfa_approxz00(BgL_boxzd2setz12zc0_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2srczd2setz12zc0zzcfa_approxz00(BgL_globalz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2boxzf2Ozd2Cinfozd2sidezd2effectzf2zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt);
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2thezd2closurez42zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2boxzf2Cinfozd2loczf2zzcfa_approxz00(BgL_makezd2boxzd2_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzf2Cinfozd2effectzd2setz12ze0zzcfa_approxz00(BgL_cfunz00_bglt,
		obj_t);
	static BgL_nodez00_bglt BGl_z62funcallzf2Cinfozd2funz42zzcfa_approxz00(obj_t,
		obj_t);
	static BgL_typez00_bglt
		BGl_z62switchzf2Cinfozd2itemzd2typez90zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62switchzf2Cinfozd2sidezd2effectz90zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_failzf2Cinfozd2objz20zzcfa_approxz00(BgL_failz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_boxzd2refzf2Ozd2Cinfozd2keyz20zzcfa_approxz00(BgL_boxzd2refzd2_bglt);
	extern obj_t BGl_funcallzf2Cinfozf2zzcfa_infoz00;
	static obj_t BGl_z62prezd2makezd2boxzd2stackablezb0zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2dssslzd2keywordszd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL long
		BGl_svarzf2Cinfozd2stampz20zzcfa_approxz00(BgL_svarz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2libraryz00zzcfa_approxz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_boxzd2setz12zf2Ozd2Cinfozd2typez32zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt);
	extern obj_t BGl_cvarz00zzast_varz00;
	static obj_t
		BGl_z62cfunzf2Cinfozd2argszd2retescapezd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_prezd2makezd2boxzd2vtypezd2zzcfa_approxz00(BgL_makezd2boxzd2_bglt);
	static obj_t BGl_z62sexitzf2Cinfozd2detachedzf3zb1zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sexitzf2Cinfozd2handlerzd2setz12ze0zzcfa_approxz00(BgL_sexitz00_bglt,
		obj_t);
	extern obj_t BGl_patchz00zzast_nodez00;
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_cvarzf2Cinfozd2approxz20zzcfa_approxz00(BgL_cvarz00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_boxzd2refzf2Ozd2Cinfozd2vtypez20zzcfa_approxz00(BgL_boxzd2refzd2_bglt);
	static BgL_approxz00_bglt
		BGl_z62funcallzf2Cinfozd2vazd2approxz90zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_approxz00_bglt);
	static obj_t
		BGl_z62boxzd2refzf2Cinfozd2vtypezd2setz12z50zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2topzf3zd2setz12zc1zzcfa_approxz00
		(BgL_sfunz00_bglt, bool_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_setzd2exzd2itzf2Cinfozd2bodyz20zzcfa_approxz00
		(BgL_setzd2exzd2itz00_bglt);
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_reshapedzd2globalzd2nilz00zzcfa_approxz00(void);
	static BgL_kwotez00_bglt BGl_z62kwotezf2Cinfozd2nilz42zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2boxzf2Cinfozd2sidezd2effectz20zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_svarzf2Cinfozd2approxz20zzcfa_approxz00(BgL_svarz00_bglt);
	static obj_t BGl_z62reshapedzd2globalzd2evalzf3z91zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62boxzd2setz12zf2Cinfozd2vtypezd2setz12z42zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_kwotezf2nodezd2locz20zzcfa_approxz00(BgL_kwotez00_bglt);
	static obj_t BGl_z62failzf2Cinfozd2locz42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2thezd2closurezd2globalzf2zzcfa_approxz00
		(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_appzd2lyzf2Cinfozf3zd3zzcfa_approxz00(obj_t);
	static obj_t BGl_z62switchzf2Cinfozd2clausesz42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_funcallzf2Cinfozd2funzd2setz12ze0zzcfa_approxz00(BgL_funcallz00_bglt,
		BgL_nodez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_switchzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00(BgL_switchz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_prezd2makezd2boxzd2stackablezd2setz12z12zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt, obj_t);
	static obj_t BGl_z62literalzf2Cinfozd2typezd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62reshapedzd2localzd2occurrencezd2setz12za2zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62funcallzf2Cinfozd2typezd2errorzd2noticedzf3zd2setz12z71zzcfa_approxz00
		(obj_t, obj_t, obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	static BgL_approxz00_bglt
		BGl_z62boxzd2refzf2Cinfozd2approxz90zzcfa_approxz00(obj_t, obj_t);
	extern obj_t BGl_za2intza2z00zztype_cachez00;
	static obj_t BGl_z62cfunzf2Cinfozd2argszd2noescapez90zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t BGl_z62externzd2sfunzf2Cinfozd2classz90zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_conditionalzf2Cinfozd2keyz20zzcfa_approxz00(BgL_conditionalz00_bglt);
	static obj_t BGl_z62genpatchidzf2Cinfozd2keyzd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static BgL_boxzd2setz12zc0_bglt
		BGl_z62makezd2boxzd2setz12zf2Ozd2Cinfoz50zzcfa_approxz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_z62boxzd2setz12zf2Cinfozd2approxz82zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62kwotezf2nodezd2typezd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_switchzf2Cinfozf3z01zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2boxzf2Cinfozd2vtypezd2setz12z32zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt, BgL_typez00_bglt);
	static obj_t BGl_requirezd2initializa7ationz75zzcfa_approxz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_boxzd2refzf2Ozd2Cinfozd2typezd2setz12ze0zzcfa_approxz00
		(BgL_boxzd2refzd2_bglt, BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2bodyzf2zzcfa_approxz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62kwotezf2Cinfozd2locz42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_jumpzd2exzd2itz00_bglt
		BGl_makezd2jumpzd2exzd2itzf2Cinfoz20zzcfa_approxz00(obj_t, BgL_typez00_bglt,
		BgL_nodez00_bglt, BgL_nodez00_bglt, BgL_approxz00_bglt);
	BGL_EXPORTED_DECL BgL_makezd2boxzd2_bglt
		BGl_makezd2makezd2boxzf2Ozd2Cinfoz20zzcfa_approxz00(obj_t, BgL_typez00_bglt,
		obj_t, obj_t, BgL_nodez00_bglt, BgL_typez00_bglt, obj_t, BgL_approxz00_bglt,
		BgL_approxz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2classzd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_conditionalzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00
		(BgL_conditionalz00_bglt, BgL_typez00_bglt);
	static obj_t BGl_z62reshapedzd2globalzd2fastzd2alphazb0zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t BGl_z62conszd2approxzf3z43zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_externzd2sfunzf2Cinfozd2polymorphiczf3z01zzcfa_approxz00
		(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL BgL_svarz00_bglt
		BGl_makezd2svarzf2Cinfoz20zzcfa_approxz00(obj_t, BgL_approxz00_bglt, bool_t,
		long);
	BGL_EXPORTED_DECL obj_t
		BGl_boxzd2refzf2Cinfozd2keyzf2zzcfa_approxz00(BgL_boxzd2refzd2_bglt);
	extern obj_t BGl_za2vectorza2z00zztype_cachez00;
	static obj_t BGl_z62cfunzf2Cinfozd2thezd2closurez90zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_switchzf2Cinfozd2clausesz20zzcfa_approxz00(BgL_switchz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2removablezd2setz12zc0zzcfa_approxz00
		(BgL_globalz00_bglt, obj_t);
	extern obj_t BGl_patchzf2Cinfozf2zzcfa_infoz00;
	static BgL_setzd2exzd2itz00_bglt
		BGl_z62makezd2setzd2exzd2itzf2Cinfoz42zzcfa_approxz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_svarzf2Cinfozd2clozd2envzf3zd2setz12zc1zzcfa_approxz00(BgL_svarz00_bglt,
		bool_t);
	static obj_t BGl_z62makezd2boxzf2Cinfozf3zb1zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62conditionalzf2Cinfozf3z63zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62patchzf2Cinfozd2valuezd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_boxzd2refzf2Cinfozd2approxzf2zzcfa_approxz00(BgL_boxzd2refzd2_bglt);
	static BgL_funcallz00_bglt
		BGl_z62funcallzf2Cinfozd2nilz42zzcfa_approxz00(obj_t);
	static obj_t BGl_z62reshapedzd2globalzd2typezd2setz12za2zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static BgL_typez00_bglt
		BGl_z62boxzd2refzf2Cinfozd2vtypez90zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_externzd2sfunzf2Cinfozd2topzf3z01zzcfa_approxz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2localzd2accesszd2setz12zc0zzcfa_approxz00(BgL_localz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_patchzf2Cinfozd2typez20zzcfa_approxz00(BgL_patchz00_bglt);
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	extern obj_t BGl_conszd2appzd2zzcfa_info2z00;
	static BgL_typez00_bglt
		BGl_z62boxzd2refzf2Ozd2Cinfozd2typez42zzcfa_approxz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62boxzd2refzf2Cinfozd2sidezd2effectz42zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_genpatchidzf2Cinfozd2rindexzd2setz12ze0zzcfa_approxz00
		(BgL_genpatchidz00_bglt, long);
	BGL_EXPORTED_DECL bool_t BGl_kwotezf2Cinfozf3z01zzcfa_approxz00(obj_t);
	static obj_t BGl_z62funcallzf2Cinfozf3z63zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_kwotezf2nodezd2typezd2setz12ze0zzcfa_approxz00(BgL_kwotez00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_failzf2Cinfozd2msgzd2setz12ze0zzcfa_approxz00(BgL_failz00_bglt,
		BgL_nodez00_bglt);
	static obj_t BGl_z62approxzd2typezd2setz12z70zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_tclassz00zzobject_classz00;
	BGL_EXPORTED_DECL obj_t
		BGl_genpatchidzf2Cinfozd2exprza2z82zzcfa_approxz00(BgL_genpatchidz00_bglt);
	static obj_t BGl_z62reshapedzd2globalzf3z43zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62cvarzf2Cinfozf3z63zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2boxzf2Ozd2Cinfozd2approxzd2setz12ze0zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt, BgL_approxz00_bglt);
	static obj_t BGl_z62cfunzf2Cinfozd2predicatezd2ofz90zzcfa_approxz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_setqz00_bglt BGl_z62setqzf2Cinfozd2nilz42zzcfa_approxz00(obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_z62boxzd2refzf2Ozd2Cinfozf3z63zzcfa_approxz00(obj_t, obj_t);
	static BgL_typez00_bglt
		BGl_z62boxzd2setz12zf2Ozd2Cinfozd2typez50zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62funcallzf2Cinfozd2typezd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2argszd2noescapez20zzcfa_approxz00
		(BgL_sfunz00_bglt);
	static BgL_typez00_bglt
		BGl_z62boxzd2refzf2Ozd2Cinfozd2vtypez42zzcfa_approxz00(obj_t, obj_t);
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2argszd2noescapez42zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2polymorphiczf3zd2setz12za3zzcfa_approxz00
		(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_approxzf3zf3zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_boxzd2refzf2Ozd2Cinfozd2sidezd2effectzf2zzcfa_approxz00
		(BgL_boxzd2refzd2_bglt);
	BGL_IMPORT obj_t BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(obj_t);
	static obj_t BGl_z62boxzd2setz12zf2Ozd2Cinfozf3z71zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2idz00zzcfa_approxz00(BgL_globalz00_bglt);
	static obj_t BGl_z62approxzd2setzd2typez12z70zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_setzd2exzd2itz00_bglt
		BGl_makezd2setzd2exzd2itzf2Cinfoz20zzcfa_approxz00(obj_t, BgL_typez00_bglt,
		BgL_varz00_bglt, BgL_nodez00_bglt, BgL_nodez00_bglt, BgL_approxz00_bglt);
	static obj_t
		BGl_z62makezd2boxzf2Ozd2Cinfozd2stackablez42zzcfa_approxz00(obj_t, obj_t);
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2argszd2noescapezd2setz12z82zzcfa_approxz00
		(obj_t, obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzcfa_approxz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2occurrencewzd2setz12zc0zzcfa_approxz00
		(BgL_globalz00_bglt, long);
	static obj_t BGl_z62funcallzf2Cinfozd2locz42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_patchzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00(BgL_patchz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_conditionalzf2Cinfozd2approxz20zzcfa_approxz00(BgL_conditionalz00_bglt);
	BGL_EXPORTED_DECL BgL_boxzd2refzd2_bglt
		BGl_makezd2boxzd2refzf2Cinfozf2zzcfa_approxz00(obj_t, BgL_typez00_bglt,
		obj_t, obj_t, BgL_varz00_bglt, BgL_typez00_bglt, BgL_approxz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_boxzd2setz12zf2Ozd2Cinfozd2vtypezd2setz12zf2zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt, BgL_typez00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_boxzd2refzf2Ozd2Cinfozd2typez20zzcfa_approxz00(BgL_boxzd2refzd2_bglt);
	static obj_t
		BGl_z62reshapedzd2globalzd2evalzf3zd2setz12z51zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2keyzb0zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzf2Cinfozd2argszd2retescapezd2setz12z32zzcfa_approxz00
		(BgL_cfunz00_bglt, obj_t);
	static BgL_genpatchidz00_bglt
		BGl_z62genpatchidzf2Cinfozd2nilz42zzcfa_approxz00(obj_t);
	static obj_t BGl_z62setqzf2Cinfozd2locz42zzcfa_approxz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62boxzd2setz12zf2Ozd2Cinfozd2valuez50zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62makezd2boxzf2Cinfozd2keyz90zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62conditionalzf2Cinfozd2keyz42zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62reshapedzd2localzd2removablez62zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_localz00_bglt
		BGl_reshapedzd2localzd2nilz00zzcfa_approxz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_prezd2makezd2boxzd2sidezd2effectz00zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt);
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2effectzd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_z62sexitzf2Cinfozd2approxz42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_jumpzd2exzd2itzf2Cinfozd2valuez20zzcfa_approxz00
		(BgL_jumpzd2exzd2itz00_bglt);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_makezd2boxzf2Ozd2Cinfozd2valuez20zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt);
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2argszd2retescapezd2setz12z82zzcfa_approxz00
		(obj_t, obj_t, obj_t);
	static BgL_cfunz00_bglt BGl_z62makezd2cfunzf2Cinfoz42zzcfa_approxz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_boxzd2refzd2_bglt
		BGl_z62makezd2boxzd2refzf2Ozd2Cinfoz42zzcfa_approxz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzcfa_approxz00(void);
	BGL_EXPORTED_DECL BgL_patchz00_bglt
		BGl_patchzf2Cinfozd2nilz20zzcfa_approxz00(void);
	static BgL_makezd2boxzd2_bglt
		BGl_z62makezd2makezd2boxzf2Ozd2Cinfoz42zzcfa_approxz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_jumpzd2exzd2itz00_bglt
		BGl_z62makezd2jumpzd2exzd2itzf2Cinfoz42zzcfa_approxz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2polymorphiczf3zd2setz12za3zzcfa_approxz00
		(obj_t, obj_t, obj_t);
	static obj_t BGl_z62internzd2sfunzf2Cinfozd2failsafez90zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2localzd2removablezd2setz12zc0zzcfa_approxz00
		(BgL_localz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2propertyzf2zzcfa_approxz00(BgL_sfunz00_bglt);
	static BgL_approxz00_bglt BGl_z62makezd2approxzb0zzcfa_approxz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_boxzd2setz12zf2Cinfozd2typeze0zzcfa_approxz00(BgL_boxzd2setz12zc0_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2localzd2occurrencezd2setz12zc0zzcfa_approxz00
		(BgL_localz00_bglt, long);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_appzd2lyzf2Cinfozd2argzf2zzcfa_approxz00(BgL_appzd2lyzd2_bglt);
	static BgL_svarz00_bglt BGl_z62makezd2prezd2clozd2envzb0zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t BGl_z62cfunzf2Cinfozd2argszd2typez90zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31949ze3ze5zzcfa_approxz00(obj_t, obj_t);
	static BgL_literalz00_bglt
		BGl_z62makezd2literalzf2Cinfoz42zzcfa_approxz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62internzd2sfunzf2Cinfozf3zb1zzcfa_approxz00(obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_z62appzd2lyzf2Cinfozd2approxz90zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_internzd2sfunzf2Cinfozd2approxzf2zzcfa_approxz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62internzd2sfunzf2Cinfozd2classz90zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2stampzd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt, long);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_externzd2sfunzf2Cinfozd2approxzf2zzcfa_approxz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL BgL_genpatchidz00_bglt
		BGl_genpatchidzf2Cinfozd2nilz20zzcfa_approxz00(void);
	static obj_t BGl_z62genpatchidzf2Cinfozd2locz42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_boxzd2setz12zf2Cinfozd2typezd2setz12z20zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt, BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_scnstzf2Cinfozd2classz20zzcfa_approxz00(BgL_scnstz00_bglt);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	static obj_t BGl_z62procedurezd2approxzf3z43zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2argszf2zzcfa_approxz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL BgL_boxzd2refzd2_bglt
		BGl_makezd2boxzd2refzf2Ozd2Cinfoz20zzcfa_approxz00(obj_t, BgL_typez00_bglt,
		obj_t, obj_t, BgL_varz00_bglt, BgL_typez00_bglt, BgL_approxz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_patchzf2Cinfozd2locz20zzcfa_approxz00(BgL_patchz00_bglt);
	static obj_t BGl_z62failzf2Cinfozd2proczd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62makezd2boxzf2Cinfozd2valuezd2setz12z50zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_appzd2lyzf2Cinfozd2funzd2setz12z32zzcfa_approxz00(BgL_appzd2lyzd2_bglt,
		BgL_nodez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_boxzd2refzf2Cinfozd2varzd2setz12z32zzcfa_approxz00
		(BgL_boxzd2refzd2_bglt, BgL_varz00_bglt);
	extern obj_t BGl_za2longza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t
		BGl_setzd2exzd2itzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00
		(BgL_setzd2exzd2itz00_bglt, BgL_typez00_bglt);
	static obj_t BGl_z62externzd2sfunzf2Cinfozd2keysz90zzcfa_approxz00(obj_t,
		obj_t);
	extern obj_t BGl_makezd2vectorzd2appz00zzcfa_info2z00;
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2localzd2typezd2setz12zc0zzcfa_approxz00(BgL_localz00_bglt,
		BgL_typez00_bglt);
	static BgL_svarz00_bglt BGl_z62svarzf2Cinfozd2nilz42zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzf2Cinfozd2argszd2typezf2zzcfa_approxz00(BgL_cfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2thezd2closurezd2globalzf2zzcfa_approxz00
		(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2occurrencezd2setz12zc0zzcfa_approxz00
		(BgL_globalz00_bglt, long);
	extern bool_t BGl_czd2subtypezf3z21zztype_miscz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_z62patchzf2Cinfozd2typezd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62reshapedzd2globalzd2occurrencewz62zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62reshapedzd2globalzd2occurrencezd2setz12za2zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_genpatchidzf2Cinfozd2locz20zzcfa_approxz00(BgL_genpatchidz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2sidezd2effectz20zzcfa_approxz00
		(BgL_sfunz00_bglt);
	static obj_t BGl_z62cfunzf2Cinfozd2effectzd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2loczd2setz12z50zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL long
		BGl_internzd2sfunzf2Cinfozd2stampzf2zzcfa_approxz00(BgL_sfunz00_bglt);
	extern obj_t BGl_za2bstringza2z00zztype_cachez00;
	static obj_t BGl_z62prezd2clozd2envzd2loczd2setz12z70zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_failzf2Cinfozd2proczd2setz12ze0zzcfa_approxz00(BgL_failz00_bglt,
		BgL_nodez00_bglt);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_jumpzd2exzd2itzf2Cinfozd2approxz20zzcfa_approxz00
		(BgL_jumpzd2exzd2itz00_bglt);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_makezd2boxzf2Ozd2Cinfozd2approxz20zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt);
	static BgL_approxz00_bglt BGl_z62unionzd2approxz12za2zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_approxzd2haszd2procedurezf3zf3zzcfa_approxz00(BgL_approxz00_bglt);
	BGL_EXPORTED_DECL BgL_varz00_bglt
		BGl_patchzf2Cinfozd2refz20zzcfa_approxz00(BgL_patchz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_approxzd2topzf3z21zzcfa_approxz00(BgL_approxz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2aliaszd2setz12zc0zzcfa_approxz00(BgL_globalz00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_genpatchidzf2Cinfozd2approxz20zzcfa_approxz00(BgL_genpatchidz00_bglt);
	static obj_t BGl_z62funcallzf2Cinfozd2argszd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2keyszf2zzcfa_approxz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_boxzd2setz12zf2Ozd2Cinfozd2vtypez32zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt);
	static obj_t BGl_z62funcallzf2Cinfozd2argsz42zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62svarzf2Cinfozd2locz42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2bodyzd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cfunzf2Cinfozd2infixzf3zd3zzcfa_approxz00(BgL_cfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2thezd2closurez20zzcfa_approxz00
		(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_boxzd2refzf2Cinfozd2vtypezf2zzcfa_approxz00(BgL_boxzd2refzd2_bglt);
	static obj_t
		BGl_z62reshapedzd2globalzd2occurrencewzd2setz12za2zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62funcallzf2Cinfozd2funzd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_scnstz00_bglt
		BGl_scnstzf2Cinfozd2nilz20zzcfa_approxz00(void);
	static obj_t BGl_z62switchzf2Cinfozd2typezd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_sfunz00_bglt
		BGl_makezd2internzd2sfunzf2Cinfozf2zzcfa_approxz00(long, obj_t, obj_t,
		obj_t, bool_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, bool_t,
		BgL_approxz00_bglt, long);
	BGL_EXPORTED_DECL obj_t
		BGl_genpatchidzf2Cinfozd2exprza2zd2setz12z42zzcfa_approxz00
		(BgL_genpatchidz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2stackzd2allocatorzd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_scnstzf2Cinfozf3z01zzcfa_approxz00(obj_t);
	static obj_t BGl_z62prezd2clozd2envzf3z91zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_prezd2clozd2envzd2loczd2setz12z12zzcfa_approxz00(BgL_svarz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_boxzd2setz12zf2Ozd2Cinfozd2varzd2setz12zf2zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt, BgL_varz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2argszd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62prezd2makezd2boxzd2valuezd2setz12z70zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_nodezd2keyzd2setz12z12zzcfa_approxz00(BgL_nodezf2effectzf2_bglt, obj_t);
	static BgL_varz00_bglt BGl_z62setqzf2Cinfozd2varz42zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t BGl_z62externzd2sfunzf2Cinfozd2bodyz90zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t BGl_z62vectorzd2approxzf3z43zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_cfunzf2Cinfozd2arityz20zzcfa_approxz00(BgL_cfunz00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_conditionalzf2Cinfozd2typez20zzcfa_approxz00(BgL_conditionalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2boxzf2Cinfozd2keyzd2setz12z32zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_approxzd2checkzd2haszd2procedurezf3z21zzcfa_approxz00
		(BgL_approxz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_svarzf2Cinfozd2stampzd2setz12ze0zzcfa_approxz00(BgL_svarz00_bglt, long);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2libraryzd2setz12zc0zzcfa_approxz00
		(BgL_globalz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_switchzf2Cinfozd2keyz20zzcfa_approxz00(BgL_switchz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_boxzd2setz12zf2Ozd2Cinfozd2typezd2setz12zf2zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt, BgL_typez00_bglt);
	static obj_t BGl_z62appzd2lyzf2Cinfozd2funzd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_genpatchidzf2Cinfozd2effectzd2setz12ze0zzcfa_approxz00
		(BgL_genpatchidz00_bglt, obj_t);
	extern obj_t BGl_reshapedzd2globalzd2zzcfa_infoz00;
	BGL_EXPORTED_DECL BgL_sfunz00_bglt
		BGl_externzd2sfunzf2Cinfozd2nilzf2zzcfa_approxz00(void);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_kwotezf2nodezd2typez20zzcfa_approxz00(BgL_kwotez00_bglt);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_failzf2Cinfozd2msgz20zzcfa_approxz00(BgL_failz00_bglt);
	static obj_t
		BGl_z62boxzd2refzf2Ozd2Cinfozd2vtypezd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2argszd2noescapezd2setz12z82zzcfa_approxz00
		(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62makezd2boxzf2Ozd2Cinfozd2vtypezd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_externzd2sfunzf2Cinfozd2arityzf2zzcfa_approxz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2stackablezf2zzcfa_approxz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_scnstzf2Cinfozd2locz20zzcfa_approxz00(BgL_scnstz00_bglt);
	extern obj_t BGl_za2realza2z00zztype_cachez00;
	static BgL_kwotez00_bglt BGl_z62makezd2kwotezf2Cinfoz42zzcfa_approxz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62genpatchidzf2Cinfozd2indexzd2setz12z82zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_externzd2sfunzf2Cinfoz20zzcfa_infoz00;
	extern obj_t BGl_setzd2forzd2eachz00zzcfa_setz00(obj_t, obj_t);
	static obj_t
		BGl_z62setzd2exzd2itzf2Cinfozd2onexitzd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62funcallzf2Cinfozd2arityzd2errorzd2noticedzf3zd2setz12z71zzcfa_approxz00
		(obj_t, obj_t, obj_t);
	static obj_t BGl_z62externzd2sfunzf2Cinfozd2topzf3z63zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t BGl_z62internzd2sfunzf2Cinfozd2optionalsz90zzcfa_approxz00(obj_t,
		obj_t);
	extern obj_t BGl_setzd2unionz12zc0zzcfa_setz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_setqzf2Cinfozd2valuez20zzcfa_approxz00(BgL_setqz00_bglt);
	static obj_t
		BGl_z62makezd2boxzf2Cinfozd2stackablezd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62funcallzf2Cinfozd2functionszd2setz12z82zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_makezd2typezd2alloczd2approxzd2zzcfa_approxz00(BgL_typez00_bglt,
		BgL_nodez00_bglt);
	static BgL_typez00_bglt BGl_z62failzf2Cinfozd2typez42zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t BGl_z62genpatchidzf2Cinfozd2exprza2ze0zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2effectzf2zzcfa_approxz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_reshapedzd2localzd2volatilez00zzcfa_approxz00(BgL_localz00_bglt);
	static obj_t
		BGl_z62reshapedzd2globalzd2importzd2setz12za2zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2dssslzd2keywordszd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2bodyzf2zzcfa_approxz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_patchzf2Cinfozd2patchidzd2setz12ze0zzcfa_approxz00(BgL_patchz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setzd2exzd2itzf2Cinfozd2bodyzd2setz12ze0zzcfa_approxz00
		(BgL_setzd2exzd2itz00_bglt, BgL_nodez00_bglt);
	BGL_EXPORTED_DECL BgL_scnstz00_bglt
		BGl_makezd2scnstzf2Cinfoz20zzcfa_approxz00(obj_t, obj_t, obj_t,
		BgL_approxz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2removablez00zzcfa_approxz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzf2Cinfozd2stackzd2allocatorzd2setz12z32zzcfa_approxz00
		(BgL_cfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2effectzf2zzcfa_approxz00(BgL_sfunz00_bglt);
	static obj_t
		BGl_z62cfunzf2Cinfozd2stackzd2allocatorzd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62approxzd2haszd2procedurezf3z91zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t BGl_z62reshapedzd2localzd2keyz62zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_boxzd2setz12zf2Cinfozd2valuezd2setz12z20zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt, BgL_nodez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2loczf2zzcfa_approxz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2propertyzd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	static obj_t BGl_z62reshapedzd2localzd2valuezd2setz12za2zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2propertyzd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_approxzd2dupzd2zzcfa_approxz00(BgL_approxz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_jumpzd2exzd2itzf2Cinfozd2valuezd2setz12ze0zzcfa_approxz00
		(BgL_jumpzd2exzd2itz00_bglt, BgL_nodez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2boxzf2Ozd2Cinfozd2valuezd2setz12ze0zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt, BgL_nodez00_bglt);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t BGl_z62boxzd2refzf2Cinfozf3zb1zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_internzd2sfunzf2Cinfozd2topzf3z01zzcfa_approxz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62cfunzf2Cinfozd2failsafez42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_appzd2lyzf2Cinfozd2approxzf2zzcfa_approxz00(BgL_appzd2lyzd2_bglt);
	static obj_t BGl_z62patchzf2Cinfozd2indexz42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_nodezd2keyzd2zzcfa_approxz00(BgL_nodezf2effectzf2_bglt);
	BGL_EXPORTED_DECL bool_t BGl_boxzd2setz12zf2Cinfozf3zc1zzcfa_approxz00(obj_t);
	extern obj_t BGl_za2pairza2z00zztype_cachez00;
	static obj_t
		BGl_z62reshapedzd2globalzd2jvmzd2typezd2namezd2setz12za2zzcfa_approxz00
		(obj_t, obj_t, obj_t);
	static BgL_sexitz00_bglt BGl_z62sexitzf2Cinfozd2nilz42zzcfa_approxz00(obj_t);
	static obj_t BGl_z62prezd2makezd2boxzd2keyzb0zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2argszd2namez20zzcfa_approxz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_approxzd2dupzd2setz12z12zzcfa_approxz00(BgL_approxz00_bglt, obj_t);
	static obj_t BGl_z62setqzf2Cinfozd2varzd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62reshapedzd2globalzd2libraryz62zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t BGl_z62cfunzf2Cinfozd2argszd2retescapez90zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2failsafezd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62genpatchidzf2Cinfozf3z63zzcfa_approxz00(obj_t, obj_t);
	static BgL_sexitz00_bglt BGl_z62makezd2sexitzf2Cinfoz42zzcfa_approxz00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_switchzf2Cinfozd2testz20zzcfa_approxz00(BgL_switchz00_bglt);
	extern obj_t BGl_za2procedureza2z00zztype_cachez00;
	static BgL_nodez00_bglt
		BGl_z62makezd2boxzf2Ozd2Cinfozd2valuez42zzcfa_approxz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62jumpzd2exzd2itzf2Cinfozd2valuez42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2localzd2idz00zzcfa_approxz00(BgL_localz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_genpatchidzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00
		(BgL_genpatchidz00_bglt, BgL_typez00_bglt);
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2failsafezd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62literalzf2Cinfozd2typez42zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t BGl_z62reshapedzd2localzd2occurrencez62zzcfa_approxz00(obj_t,
		obj_t);
	static BgL_literalz00_bglt
		BGl_z62literalzf2Cinfozd2nilz42zzcfa_approxz00(obj_t);
	static obj_t
		BGl_z62genpatchidzf2Cinfozd2sidezd2effectz90zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_reshapedzd2globalzd2evaluablezf3zf3zzcfa_approxz00(BgL_globalz00_bglt);
	static obj_t BGl_z62svarzf2Cinfozd2stampzd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62kwotezf2nodezd2valuez42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_genpatchidzf2Cinfozd2effectz20zzcfa_approxz00(BgL_genpatchidz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_conditionalzf2Cinfozf3z01zzcfa_approxz00(obj_t);
	static BgL_sfunz00_bglt
		BGl_z62externzd2sfunzf2Cinfozd2nilz90zzcfa_approxz00(obj_t);
	static obj_t BGl_z62approxzd2dupzb0zzcfa_approxz00(obj_t, obj_t);
	static BgL_makezd2boxzd2_bglt
		BGl_z62makezd2prezd2makezd2boxzb0zzcfa_approxz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62internzd2sfunzf2Cinfozd2strengthz90zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_boxzd2refzf2Ozd2Cinfozd2varzd2setz12ze0zzcfa_approxz00
		(BgL_boxzd2refzd2_bglt, BgL_varz00_bglt);
	BGL_EXPORTED_DECL BgL_genpatchidz00_bglt
		BGl_makezd2genpatchidzf2Cinfoz20zzcfa_approxz00(obj_t, BgL_typez00_bglt,
		obj_t, obj_t, obj_t, obj_t, long, long, BgL_approxz00_bglt);
	static obj_t BGl_z62setzd2exzd2itzf2Cinfozf3z63zzcfa_approxz00(obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_z62makezd2boxzf2Ozd2Cinfozd2valuezd2approxz90zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_prezd2makezd2boxzd2vtypezd2setz12z12zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt, BgL_typez00_bglt);
	extern obj_t BGl_za2belongza2z00zztype_cachez00;
	static obj_t BGl_z62kwotezf2Cinfozf3z63zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2effectzd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	static BgL_boxzd2setz12zc0_bglt
		BGl_z62boxzd2setz12zf2Ozd2Cinfozd2nilz50zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_boxzd2refzf2Ozd2Cinfozd2sidezd2effectzd2setz12z32zzcfa_approxz00
		(BgL_boxzd2refzd2_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_failzf2Cinfozf3z01zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL BgL_cfunz00_bglt
		BGl_cfunzf2Cinfozd2nilz20zzcfa_approxz00(void);
	BGL_EXPORTED_DECL long
		BGl_genpatchidzf2Cinfozd2indexz20zzcfa_approxz00(BgL_genpatchidz00_bglt);
	static BgL_cvarz00_bglt BGl_z62makezd2cvarzf2Cinfoz42zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62literalzf2Cinfozd2locz42zzcfa_approxz00(obj_t, obj_t);
	static obj_t
		BGl_z62reshapedzd2localzd2bindingzd2valuezb0zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62externzd2sfunzf2Cinfozd2argsz90zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t BGl_z62approxzd2topzf3z43zzcfa_approxz00(obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62failzf2Cinfozd2procz42zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t BGl_z62failzf2Cinfozd2objzd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static BgL_appzd2lyzd2_bglt
		BGl_z62makezd2appzd2lyzf2Cinfoz90zzcfa_approxz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static obj_t BGl_z62boxzd2refzf2Cinfozd2varzd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62externzd2sfunzf2Cinfozd2failsafez90zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL long
		BGl_internzd2sfunzf2Cinfozd2arityzf2zzcfa_approxz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62externzd2sfunzf2Cinfozd2locz90zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62reshapedzd2globalzd2pragmazd2setz12za2zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2propertyzf2zzcfa_approxz00(BgL_sfunz00_bglt);
	static BgL_boxzd2setz12zc0_bglt
		BGl_z62makezd2boxzd2setz12zf2Cinfoz82zzcfa_approxz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_approxzd2haszd2procedurezf3zd2setz12z33zzcfa_approxz00
		(BgL_approxz00_bglt, bool_t);
	static obj_t BGl_za2alloczd2setza2zd2zzcfa_approxz00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_makezd2boxzd2_bglt
		BGl_prezd2makezd2boxzd2nilzd2zzcfa_approxz00(void);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_funcallzf2Cinfozd2funz20zzcfa_approxz00(BgL_funcallz00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcfa_approxz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_procedurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_loosez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_iteratez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_setz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_collectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info2z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_infoz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_coercionz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__evalz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2predicatezd2ofz42zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62makezd2boxzf2Ozd2Cinfozd2keyzd2setz12z82zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2boxzf2Cinfozd2keyzf2zzcfa_approxz00(BgL_makezd2boxzd2_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_setqzf2Cinfozd2typez20zzcfa_approxz00(BgL_setqz00_bglt);
	BGL_EXPORTED_DECL BgL_boxzd2setz12zc0_bglt
		BGl_boxzd2setz12zf2Ozd2Cinfozd2nilz32zzcfa_approxz00(void);
	static obj_t BGl_z62boxzd2setz12zf2Ozd2Cinfozd2locz50zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62reshapedzd2localzd2userzf3zd2setz12z51zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2argszf2zzcfa_approxz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62reshapedzd2localzd2namezd2setz12za2zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_sexitzf2Cinfozd2approxz20zzcfa_approxz00(BgL_sexitz00_bglt);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_boxzd2refzf2Ozd2Cinfozd2approxz20zzcfa_approxz00(BgL_boxzd2refzd2_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2boxzf2Cinfozd2typezd2setz12z32zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt, BgL_typez00_bglt);
	extern obj_t BGl_boxzd2setz12zf2Ozd2Cinfoze0zzcfa_infoz00;
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2argszd2retescapezd2setz12z82zzcfa_approxz00
		(obj_t, obj_t, obj_t);
	static BgL_switchz00_bglt
		BGl_z62makezd2switchzf2Cinfoz42zzcfa_approxz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2aliasz00zzcfa_approxz00(BgL_globalz00_bglt);
	static obj_t BGl_z62internzd2sfunzf2Cinfozd2keysz90zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_scnstzf2Cinfozd2classzd2setz12ze0zzcfa_approxz00(BgL_scnstz00_bglt,
		obj_t);
	static BgL_typez00_bglt
		BGl_z62boxzd2setz12zf2Ozd2Cinfozd2vtypez50zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62setqzf2Cinfozd2typezd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_getzd2allocszd2zzcfa_collectz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2stackzd2allocatorz20zzcfa_approxz00
		(BgL_sfunz00_bglt);
	static BgL_typez00_bglt
		BGl_z62setzd2exzd2itzf2Cinfozd2typez42zzcfa_approxz00(obj_t, obj_t);
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2thezd2closurezd2globalzd2setz12z50zzcfa_approxz00
		(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_prezd2makezd2boxzd2loczd2zzcfa_approxz00(BgL_makezd2boxzd2_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_makezd2boxzf2Ozd2Cinfozd2vtypez20zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt);
	static obj_t BGl_z62appzd2lyzf2Cinfozf3zb1zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzf2Cinfozd2sidezd2effectzf2zzcfa_approxz00(BgL_cfunz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_internzd2sfunzf2Cinfozf3zd3zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL BgL_literalz00_bglt
		BGl_makezd2literalzf2Cinfoz20zzcfa_approxz00(obj_t, BgL_typez00_bglt, obj_t,
		BgL_approxz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_conditionalzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_approxz00
		(BgL_conditionalz00_bglt, obj_t);
	static obj_t
		BGl_z62switchzf2Cinfozd2itemzd2typezd2setz12z50zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62reshapedzd2globalzd2srczd2setz12za2zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_tprintz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static BgL_makezd2boxzd2_bglt
		BGl_z62makezd2makezd2boxzf2Cinfoz90zzcfa_approxz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_genpatchidz00_bglt
		BGl_z62makezd2genpatchidzf2Cinfoz42zzcfa_approxz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	static BgL_sfunz00_bglt
		BGl_z62makezd2internzd2sfunzf2Cinfoz90zzcfa_approxz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_boxzd2setz12zf2Ozd2Cinfozd2locz32zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt);
	static obj_t
		BGl_z62reshapedzd2globalzd2accesszd2setz12za2zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62reshapedzd2localzf3z43zzcfa_approxz00(obj_t, obj_t);
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	static obj_t BGl_z62cfunzf2Cinfozd2topzf3zb1zzcfa_approxz00(obj_t, obj_t);
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_z62switchzf2Cinfozf3z63zzcfa_approxz00(obj_t, obj_t);
	static BgL_localz00_bglt
		BGl_z62makezd2reshapedzd2localz62zzcfa_approxz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62sexitzf2Cinfozd2handlerzd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2topzf3zd2setz12za3zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzcfa_approxz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcfa_approxz00(void);
	static BgL_typez00_bglt
		BGl_z62prezd2makezd2boxzd2typezb0zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_boxzd2refzf2Cinfozd2keyzd2setz12z32zzcfa_approxz00
		(BgL_boxzd2refzd2_bglt, obj_t);
	static obj_t BGl_z62scnstzf2Cinfozd2nodez42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setqzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00(BgL_setqz00_bglt,
		BgL_typez00_bglt);
	static BgL_typez00_bglt
		BGl_z62makezd2boxzf2Cinfozd2typez90zzcfa_approxz00(obj_t, obj_t);
	static BgL_typez00_bglt
		BGl_z62conditionalzf2Cinfozd2typez42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_prezd2makezd2boxzf3zf3zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt BGl_approxzd2nilzd2zzcfa_approxz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzcfa_approxz00(void);
	extern obj_t BGl_scnstz00zzast_varz00;
	BGL_EXPORTED_DECL long
		BGl_genpatchidzf2Cinfozd2rindexz20zzcfa_approxz00(BgL_genpatchidz00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_setzd2exzd2itzf2Cinfozd2typez20zzcfa_approxz00
		(BgL_setzd2exzd2itz00_bglt);
	static obj_t BGl_z62cfunzf2Cinfozd2macrozf3zb1zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2valuezd2setz12zc0zzcfa_approxz00(BgL_globalz00_bglt,
		BgL_valuez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzf2Cinfozd2failsafez20zzcfa_approxz00(BgL_cfunz00_bglt);
	BGL_EXPORTED_DECL BgL_funcallz00_bglt
		BGl_funcallzf2Cinfozd2nilz20zzcfa_approxz00(void);
	static obj_t BGl_z62reshapedzd2localzd2valzd2noescapezb0zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_funcallzf2Cinfozd2typezd2errorzd2noticedzf3zd2setz12z13zzcfa_approxz00
		(BgL_funcallz00_bglt, bool_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_kwotezf2Cinfozd2typez20zzcfa_approxz00(BgL_kwotez00_bglt);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_makezd2boxzf2Cinfozd2approxzf2zzcfa_approxz00(BgL_makezd2boxzd2_bglt);
	static obj_t
		BGl_z62conditionalzf2Cinfozd2truezd2setz12z82zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62funcallzf2Cinfozd2strengthzd2setz12z82zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62boxzd2setz12zf2Ozd2Cinfozd2valuezd2setz12z90zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_declarezd2setz12zc0zzcfa_setz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setqzf2Cinfozd2varzd2setz12ze0zzcfa_approxz00(BgL_setqz00_bglt,
		BgL_varz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_patchzf2Cinfozf3z01zzcfa_approxz00(obj_t);
	static BgL_approxz00_bglt
		BGl_z62makezd2typezd2alloczd2approxzb0zzcfa_approxz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_conditionalzf2Cinfozf2zzcfa_infoz00;
	BGL_EXPORTED_DECL obj_t
		BGl_approxzd2topzf3zd2setz12ze1zzcfa_approxz00(BgL_approxz00_bglt, bool_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_makezd2typezd2approxz00zzcfa_approxz00(BgL_typez00_bglt);
	extern obj_t
		BGl_findzd2commonzd2superzd2classzd2zzobject_classz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_z62reshapedzd2localzd2fastzd2alphazb0zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t BGl_z62internzd2sfunzf2Cinfozd2bodyz90zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62boxzd2refzf2Ozd2Cinfozd2sidezd2effectzd2setz12z50zzcfa_approxz00
		(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2bnilza2z00zztype_cachez00;
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2classzd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_genpatchidzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_approxz00
		(BgL_genpatchidz00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_boxzd2setz12zf2Ozd2Cinfozf3z13zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2modulez00zzcfa_approxz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_funcallzf2Cinfozd2argsz20zzcfa_approxz00(BgL_funcallz00_bglt);
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2polymorphiczf3z63zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL long
		BGl_reshapedzd2globalzd2occurrencez00zzcfa_approxz00(BgL_globalz00_bglt);
	static obj_t BGl_z62cfunzf2Cinfozd2stackzd2allocatorz90zzcfa_approxz00(obj_t,
		obj_t);
	static BgL_boxzd2refzd2_bglt
		BGl_z62boxzd2refzf2Cinfozd2nilz90zzcfa_approxz00(obj_t);
	static obj_t BGl_z62reshapedzd2globalzd2aliasz62zzcfa_approxz00(obj_t, obj_t);
	static BgL_approxz00_bglt BGl_z62unionzd2approx2z12za2zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static BgL_failz00_bglt BGl_z62makezd2failzf2Cinfoz42zzcfa_approxz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2thezd2closurezd2globalzd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_funcallzf2Cinfozd2locz20zzcfa_approxz00(BgL_funcallz00_bglt);
	extern obj_t BGl_jumpzd2exzd2itzf2Cinfozf2zzcfa_infoz00;
	extern obj_t BGl_makezd2boxzf2Ozd2Cinfozf2zzcfa_infoz00;
	static obj_t BGl_z62literalzf2Cinfozf3z63zzcfa_approxz00(obj_t, obj_t);
	static BgL_approxz00_bglt BGl_z62approxzd2nilzb0zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2predicatezd2ofzd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	static BgL_kwotez00_bglt BGl_z62kwotezf2nodezd2nilz42zzcfa_approxz00(obj_t);
	static obj_t BGl_z62reshapedzd2localzd2occurrencewz62zzcfa_approxz00(obj_t,
		obj_t);
	extern obj_t BGl_za2keywordza2z00zztype_cachez00;
	static obj_t
		BGl_z62svarzf2Cinfozd2clozd2envzf3zd2setz12za3zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62sexitzf2Cinfozd2handlerz42zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62internzd2sfunzf2Cinfozd2topzf3z63zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2predicatezd2ofz20zzcfa_approxz00
		(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_jumpzd2exzd2itzf2Cinfozd2typez20zzcfa_approxz00
		(BgL_jumpzd2exzd2itz00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_makezd2boxzf2Ozd2Cinfozd2typez20zzcfa_approxz00(BgL_makezd2boxzd2_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2boxzf2Cinfozd2stackablezd2setz12z32zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_approxzd2allocszd2zzcfa_approxz00(BgL_approxz00_bglt);
	static BgL_nodez00_bglt BGl_z62failzf2Cinfozd2objz42zzcfa_approxz00(obj_t,
		obj_t);
	static BgL_setqz00_bglt BGl_z62makezd2setqzf2Cinfoz42zzcfa_approxz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2argszd2noescapez20zzcfa_approxz00
		(BgL_sfunz00_bglt);
	static obj_t BGl_z62scnstzf2Cinfozf3z63zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62internzd2sfunzf2Cinfozd2stampz90zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2argszd2noescapez42zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_kwotezf2nodezd2nodez20zzcfa_approxz00(BgL_kwotez00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31807ze3ze5zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62makezd2boxzf2Ozd2Cinfozf3z63zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62jumpzd2exzd2itzf2Cinfozf3z63zzcfa_approxz00(obj_t, obj_t);
	static BgL_funcallz00_bglt
		BGl_z62makezd2funcallzf2Cinfoz42zzcfa_approxz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_valuez00_bglt
		BGl_z62reshapedzd2localzd2valuez62zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62reshapedzd2localzd2userzf3z91zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_svarz00_bglt
		BGl_prezd2clozd2envzd2nilzd2zzcfa_approxz00(void);
	static BgL_varz00_bglt
		BGl_z62boxzd2setz12zf2Ozd2Cinfozd2varz50zzcfa_approxz00(obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_z62makezd2boxzf2Ozd2Cinfozd2approxz42zzcfa_approxz00(obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_z62jumpzd2exzd2itzf2Cinfozd2approxz42zzcfa_approxz00(obj_t, obj_t);
	static obj_t
		BGl_z62switchzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_failzf2Cinfozd2objzd2setz12ze0zzcfa_approxz00(BgL_failz00_bglt,
		BgL_nodez00_bglt);
	static obj_t BGl_z62failzf2Cinfozf3z63zzcfa_approxz00(obj_t, obj_t);
	static obj_t BGl_z62boxzd2refzf2Cinfozd2locz90zzcfa_approxz00(obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_z62genpatchidzf2Cinfozd2approxz42zzcfa_approxz00(obj_t, obj_t);
	extern obj_t BGl_za2unspecza2z00zztype_cachez00;
	static BgL_makezd2boxzd2_bglt
		BGl_z62makezd2boxzf2Ozd2Cinfozd2nilz42zzcfa_approxz00(obj_t);
	static BgL_jumpzd2exzd2itz00_bglt
		BGl_z62jumpzd2exzd2itzf2Cinfozd2nilz42zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_conditionalzf2Cinfozd2falsez20zzcfa_approxz00(BgL_conditionalz00_bglt);
	BGL_EXPORTED_DECL BgL_boxzd2setz12zc0_bglt
		BGl_makezd2boxzd2setz12zf2Cinfoze0zzcfa_approxz00(obj_t, BgL_typez00_bglt,
		BgL_varz00_bglt, BgL_nodez00_bglt, BgL_typez00_bglt, BgL_approxz00_bglt);
	static BgL_nodez00_bglt BGl_z62appzd2lyzf2Cinfozd2funz90zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t BGl_z62kwotezf2nodezd2locz42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2importz00zzcfa_approxz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2predicatezd2ofzd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jumpzd2exzd2itzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00
		(BgL_jumpzd2exzd2itz00_bglt, BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2boxzf2Ozd2Cinfozd2typezd2setz12ze0zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt, BgL_typez00_bglt);
	extern obj_t BGl_genpatchidz00zzast_nodez00;
	static BgL_typez00_bglt BGl_z62funcallzf2Cinfozd2typez42zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2loczd2setz12z32zzcfa_approxz00(BgL_sfunz00_bglt,
		obj_t);
	static BgL_approxz00_bglt
		BGl_z62makezd2boxzf2Cinfozd2approxz90zzcfa_approxz00(obj_t, obj_t);
	static obj_t
		BGl_z62conditionalzf2Cinfozd2testzd2setz12z82zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	static BgL_approxz00_bglt
		BGl_z62conditionalzf2Cinfozd2approxz42zzcfa_approxz00(obj_t, obj_t);
	static BgL_patchz00_bglt BGl_z62makezd2patchzf2Cinfoz42zzcfa_approxz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_switchzf2Cinfozd2keyzd2setz12ze0zzcfa_approxz00(BgL_switchz00_bglt,
		obj_t);
	extern obj_t BGl_za2bintza2z00zztype_cachez00;
	static obj_t
		BGl_z62cfunzf2Cinfozd2thezd2closurezd2setz12z50zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_setzd2extendz12zc0zzcfa_setz00(obj_t, obj_t);
	static obj_t
		BGl_z62reshapedzd2globalzd2jvmzd2typezd2namez62zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62sexitzf2Cinfozd2detachedzf3zd2setz12z71zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2boxzf2Cinfozd2stackablezf2zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt);
	extern obj_t BGl_scnstzf2Cinfozf2zzcfa_infoz00;
	static obj_t
		BGl_getzd2srczd2approxzd2typezd2zzcfa_approxz00(BgL_approxz00_bglt);
	extern obj_t BGl_literalz00zzast_nodez00;
	BGL_EXPORTED_DECL BgL_valuez00_bglt
		BGl_reshapedzd2localzd2valuez00zzcfa_approxz00(BgL_localz00_bglt);
	static obj_t
		BGl_z62externzd2sfunzf2Cinfozd2sidezd2effectz42zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_reshapedzd2localzd2userzf3zf3zzcfa_approxz00(BgL_localz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2userzf3zd2setz12z33zzcfa_approxz00
		(BgL_globalz00_bglt, bool_t);
	static obj_t BGl_z62genpatchidzf2Cinfozd2keyz42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2namezd2setz12zc0zzcfa_approxz00(BgL_globalz00_bglt,
		obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_prezd2clozd2envzd2loczd2zzcfa_approxz00(BgL_svarz00_bglt);
	BGL_EXPORTED_DECL BgL_varz00_bglt
		BGl_boxzd2setz12zf2Ozd2Cinfozd2varz32zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt);
	extern obj_t BGl_za2bcharza2z00zztype_cachez00;
	static BgL_approxz00_bglt
		BGl_z62scnstzf2Cinfozd2approxz42zzcfa_approxz00(obj_t, obj_t);
	static obj_t
		BGl_z62cfunzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_svarzf2Cinfozf3z01zzcfa_approxz00(obj_t);
	static obj_t BGl_z62prezd2makezd2boxzd2sidezd2effectz62zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL long
		BGl_reshapedzd2localzd2keyz00zzcfa_approxz00(BgL_localz00_bglt);
	static obj_t BGl_z62patchzf2Cinfozd2valuez42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_jumpzd2exzd2itz00_bglt
		BGl_jumpzd2exzd2itzf2Cinfozd2nilz20zzcfa_approxz00(void);
	BGL_EXPORTED_DECL BgL_makezd2boxzd2_bglt
		BGl_makezd2boxzf2Ozd2Cinfozd2nilz20zzcfa_approxz00(void);
	static obj_t BGl_z62makezd2boxzf2Ozd2Cinfozd2locz42zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t BGl_z62jumpzd2exzd2itzf2Cinfozd2locz42zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t BGl_z62genpatchidzf2Cinfozd2indexz42zzcfa_approxz00(obj_t,
		obj_t);
	static BgL_switchz00_bglt
		BGl_z62switchzf2Cinfozd2nilz42zzcfa_approxz00(obj_t);
	static obj_t BGl_z62makezd2boxzf2Cinfozd2keyzd2setz12z50zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62conditionalzf2Cinfozd2keyzd2setz12z82zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_boxzd2refzf2Cinfoz20zzcfa_infoz00;
	static obj_t BGl_z62externzd2sfunzf2Cinfozd2arityz90zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_boxzd2refzf2Cinfozd2typezd2setz12z32zzcfa_approxz00
		(BgL_boxzd2refzd2_bglt, BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_funcallzf2Cinfozd2arityzd2errorzd2noticedzf3zd2setz12z13zzcfa_approxz00
		(BgL_funcallz00_bglt, bool_t);
	BGL_EXPORTED_DECL BgL_makezd2boxzd2_bglt
		BGl_makezd2makezd2boxzf2Cinfozf2zzcfa_approxz00(obj_t, BgL_typez00_bglt,
		obj_t, obj_t, BgL_nodez00_bglt, BgL_typez00_bglt, obj_t,
		BgL_approxz00_bglt);
	static obj_t BGl_z62patchzf2Cinfozd2patchidz42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_kwotezf2nodezf3z01zzcfa_approxz00(obj_t);
	static obj_t BGl_z62externzd2sfunzf2Cinfozd2strengthz90zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_makezd2boxzf2Ozd2Cinfozd2valuezd2approxzf2zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt);
	static BgL_typez00_bglt
		BGl_z62makezd2boxzf2Ozd2Cinfozd2typez42zzcfa_approxz00(obj_t, obj_t);
	static BgL_typez00_bglt
		BGl_z62jumpzd2exzd2itzf2Cinfozd2typez42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_approxzd2typezd2setz12z12zzcfa_approxz00(BgL_approxz00_bglt,
		BgL_typez00_bglt);
	static BgL_approxz00_bglt
		BGl_z62internzd2sfunzf2Cinfozd2approxz90zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_reshapedzd2globalzf3z21zzcfa_approxz00(obj_t);
	static BgL_approxz00_bglt
		BGl_z62externzd2sfunzf2Cinfozd2approxz90zzcfa_approxz00(obj_t, obj_t);
	extern obj_t BGl_setzd2ze3vectorz31zzcfa_setz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_genpatchidzf2Cinfozd2keyz20zzcfa_approxz00(BgL_genpatchidz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_boxzd2refzf2Ozd2Cinfozf3z01zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL BgL_localz00_bglt
		BGl_makezd2reshapedzd2localz00zzcfa_approxz00(obj_t, obj_t,
		BgL_typez00_bglt, BgL_valuez00_bglt, obj_t, obj_t, obj_t, long, long,
		bool_t, long, obj_t, bool_t, obj_t);
	static obj_t
		BGl_z62boxzd2setz12zf2Cinfozd2typezd2setz12z42zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62makezd2boxzf2Cinfozd2vtypezd2setz12z50zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62reshapedzd2localzd2volatilez62zzcfa_approxz00(obj_t,
		obj_t);
	static obj_t BGl_z62internzd2sfunzf2Cinfozd2argsz90zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_internzd2sfunzf2Cinfozd2polymorphiczf3z01zzcfa_approxz00
		(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_jumpzd2exzd2itzf2Cinfozd2locz20zzcfa_approxz00
		(BgL_jumpzd2exzd2itz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2boxzf2Ozd2Cinfozd2locz20zzcfa_approxz00(BgL_makezd2boxzd2_bglt);
	static BgL_approxz00_bglt
		BGl_z62boxzd2refzf2Ozd2Cinfozd2approxz42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_cvarz00_bglt
		BGl_cvarzf2Cinfozd2nilz20zzcfa_approxz00(void);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_literalzf2Cinfozd2typez20zzcfa_approxz00(BgL_literalz00_bglt);
	static obj_t
		BGl_z62boxzd2refzf2Ozd2Cinfozd2typezd2setz12z82zzcfa_approxz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62switchzf2Cinfozd2locz42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_makezd2boxzf2Cinfozd2typezf2zzcfa_approxz00(BgL_makezd2boxzd2_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_approxzd2setzd2typez12z12zzcfa_approxz00(BgL_approxz00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static BgL_typez00_bglt
		BGl_z62boxzd2refzf2Cinfozd2typez90zzcfa_approxz00(obj_t, obj_t);
	static BgL_appzd2lyzd2_bglt
		BGl_z62appzd2lyzf2Cinfozd2nilz90zzcfa_approxz00(obj_t);
	BGL_EXPORTED_DECL BgL_sfunz00_bglt
		BGl_internzd2sfunzf2Cinfozd2nilzf2zzcfa_approxz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_internzd2sfunzf2Cinfozd2stackzd2allocatorz20zzcfa_approxz00
		(BgL_sfunz00_bglt);
	extern obj_t BGl_cfunzf2Cinfozf2zzcfa_infoz00;
	static obj_t BGl_z62scnstzf2Cinfozd2classzd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static BgL_kwotez00_bglt BGl_z62makezd2kwotezf2nodez42zzcfa_approxz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t BGl_z62reshapedzd2localzd2typezd2setz12za2zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62internzd2sfunzf2Cinfozd2thezd2closurezd2globalz90zzcfa_approxz00
		(obj_t, obj_t);
	extern obj_t BGl_valloczf2Cinfozb2optimz40zzcfa_info3z00;
	static obj_t
		BGl_z62jumpzd2exzd2itzf2Cinfozd2exitzd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static BgL_typez00_bglt
		BGl_z62makezd2boxzf2Ozd2Cinfozd2vtypez42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_funcallzf2Cinfozd2typezd2errorzd2noticedzf3zd3zzcfa_approxz00
		(BgL_funcallz00_bglt);
	static obj_t BGl_z62cfunzf2Cinfozd2sidezd2effectz90zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_boxzd2refzf2Ozd2Cinfozd2keyzd2setz12ze0zzcfa_approxz00
		(BgL_boxzd2refzd2_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_externzd2sfunzf2Cinfozd2argszd2retescapez20zzcfa_approxz00
		(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_funcallzf2Cinfozd2functionszd2setz12ze0zzcfa_approxz00
		(BgL_funcallz00_bglt, obj_t);
	static obj_t BGl_z62switchzf2Cinfozd2keyzd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62externzd2sfunzf2Cinfozd2optionalsz90zzcfa_approxz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_approxzd2typezd2zzcfa_approxz00(BgL_approxz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzf2Cinfozd2infixzf3zd2setz12z13zzcfa_approxz00(BgL_cfunz00_bglt,
		bool_t);
	static obj_t BGl_z62scnstzf2Cinfozd2loczd2setz12z82zzcfa_approxz00(obj_t,
		obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_z62cfunzf2Cinfozd2approxz42zzcfa_approxz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_reshapedzd2globalzd2fastzd2alphazd2zzcfa_approxz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL BgL_kwotez00_bglt
		BGl_kwotezf2Cinfozd2nilz20zzcfa_approxz00(void);
	static obj_t __cnst[7];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2externzd2sfunzf2Cinfozd2envz20zzcfa_approxz00,
		BgL_bgl_za762makeza7d2extern2242z00,
		BGl_z62makezd2externzd2sfunzf2Cinfoz90zzcfa_approxz00, 0L, BUNSPEC, 24);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzf3zd2envzf3zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2243z00,
		BGl_z62reshapedzd2globalzf3z43zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Ozd2Cinfozd2sidezd2effectzd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22244za7,
		BGl_z62makezd2boxzf2Ozd2Cinfozd2sidezd2effectzd2setz12z50zzcfa_approxz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2exzd2itzf2Cinfozd2varzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762setza7d2exza7d2it2245za7,
		BGl_z62setzd2exzd2itzf2Cinfozd2varz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2sexitzf2Cinfozd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2sexitza72246za7,
		BGl_z62makezd2sexitzf2Cinfoz42zzcfa_approxz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Ozd2Cinfozd2loczd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22247za7,
		BGl_z62makezd2boxzf2Ozd2Cinfozd2locz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jumpzd2exzd2itzf2Cinfozd2loczd2envzf2zzcfa_approxz00,
		BgL_bgl_za762jumpza7d2exza7d2i2248za7,
		BGl_z62jumpzd2exzd2itzf2Cinfozd2locz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2polymorphiczf3zd2envzd3zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2249z00,
		BGl_z62internzd2sfunzf2Cinfozd2polymorphiczf3z63zzcfa_approxz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_conditionalzf2Cinfozd2keyzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762conditionalza7f2250z00,
		BGl_z62conditionalzf2Cinfozd2keyzd2setz12z82zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_unionzd2approxzd2filterz12zd2envzc0zzcfa_approxz00,
		BgL_bgl_za762unionza7d2appro2251z00,
		BGl_z62unionzd2approxzd2filterz12z70zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Ozd2Cinfozd2keyzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22252za7,
		BGl_z62makezd2boxzf2Ozd2Cinfozd2keyz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_conditionalzf2Cinfozd2falsezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762conditionalza7f2253z00,
		BGl_z62conditionalzf2Cinfozd2falsez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2evalzf3zd2setz12zd2envze1zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2254z00,
		BGl_z62reshapedzd2globalzd2evalzf3zd2setz12z51zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2optionalszd2envz20zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2255z00,
		BGl_z62externzd2sfunzf2Cinfozd2optionalsz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_setqzf2Cinfozd2loczd2envzf2zzcfa_approxz00,
		BgL_bgl_za762setqza7f2cinfoza72256za7,
		BGl_z62setqzf2Cinfozd2locz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_approxzd2typezd2envz00zzcfa_approxz00,
		BgL_bgl_za762approxza7d2type2257z00, BGl_z62approxzd2typezb0zzcfa_approxz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setqzf2Cinfozd2varzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762setqza7f2cinfoza72258za7,
		BGl_z62setqzf2Cinfozd2varzd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvarzf2Cinfozd2approxzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762cvarza7f2cinfoza72259za7,
		BGl_z62cvarzf2Cinfozd2approxz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2localzd2fastzd2alphazd2envz00zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2lo2260z00,
		BGl_z62reshapedzd2localzd2fastzd2alphazb0zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Cinfozd2valuezd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22261za7,
		BGl_z62makezd2boxzf2Cinfozd2valuezd2setz12z50zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2localzd2keyzd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2lo2262z00,
		BGl_z62reshapedzd2localzd2keyz62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genpatchidzf2Cinfozd2sidezd2effectzd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762genpatchidza7f22263z00,
		BGl_z62genpatchidzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_switchzf2Cinfozd2testzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762switchza7f2cinf2264z00,
		BGl_z62switchzf2Cinfozd2testz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Cinfozd2approxzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762svarza7f2cinfoza72265za7,
		BGl_z62svarzf2Cinfozd2approxz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_svarzf2Cinfozd2loczd2envzf2zzcfa_approxz00,
		BgL_bgl_za762svarza7f2cinfoza72266za7,
		BGl_z62svarzf2Cinfozd2locz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2failsafezd2envz20zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2267z00,
		BGl_z62externzd2sfunzf2Cinfozd2failsafez90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_failzf2Cinfozd2msgzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762failza7f2cinfoza72268za7,
		BGl_z62failzf2Cinfozd2msgz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_kwotezf2nodezd2typezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762kwoteza7f2nodeza72269za7,
		BGl_z62kwotezf2nodezd2typez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2localzd2typezd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2lo2270z00,
		BGl_z62reshapedzd2localzd2typez62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Cinfozd2sidezd2effectzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2c2271za7,
		BGl_z62boxzd2refzf2Cinfozd2sidezd2effectzd2setz12z82zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_patchzf2Cinfozd2patchidzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762patchza7f2cinfo2272z00,
		BGl_z62patchzf2Cinfozd2patchidz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2dssslzd2keywordszd2envzf2zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2273z00,
		BGl_z62externzd2sfunzf2Cinfozd2dssslzd2keywordsz42zzcfa_approxz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Cinfozd2typezd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2c2274za7,
		BGl_z62boxzd2refzf2Cinfozd2typezd2setz12z50zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Ozd2Cinfozd2varzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2o2275za7,
		BGl_z62boxzd2refzf2Ozd2Cinfozd2varzd2setz12z82zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Cinfozd2nilzd2envz20zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22276za7,
		BGl_z62makezd2boxzf2Cinfozd2nilz90zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Cinfozd2detachedzf3zd2setz12zd2envzc1zzcfa_approxz00,
		BgL_bgl_za762sexitza7f2cinfo2277z00,
		BGl_z62sexitzf2Cinfozd2detachedzf3zd2setz12z71zzcfa_approxz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2232z00zzcfa_approxz00,
		BgL_bgl_za762getza7d2nodeza7d22278za7,
		BGl_z62getzd2nodezd2atomzd2value1581zb0zzcfa_approxz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2234z00zzcfa_approxz00,
		BgL_bgl_za762shapeza7d2appro2279z00,
		BGl_z62shapezd2approx1580zb0zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_funcallzf2Cinfozd2functionszd2envzf2zzcfa_approxz00,
		BgL_bgl_za762funcallza7f2cin2280z00,
		BGl_z62funcallzf2Cinfozd2functionsz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2236z00zzcfa_approxz00,
		BgL_bgl_za762getza7d2nodeza7d22281za7,
		BGl_z62getzd2nodezd2atomzd2valuezd21584z62zzcfa_approxz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2238z00zzcfa_approxz00,
		BgL_bgl_za762getza7d2nodeza7d22282za7,
		BGl_z62getzd2nodezd2atomzd2valuezd21586z62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Cinfozd2vtypezd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22283za7,
		BGl_z62makezd2boxzf2Cinfozd2vtypezd2setz12z50zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_kwotezf2Cinfozd2valuezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762kwoteza7f2cinfo2284z00,
		BGl_z62kwotezf2Cinfozd2valuez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2cvarzf2Cinfozd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2cvarza7f2285za7,
		BGl_z62makezd2cvarzf2Cinfoz42zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2makezd2boxzd2typezd2envz00zzcfa_approxz00,
		BgL_bgl_za762preza7d2makeza7d22286za7,
		BGl_z62prezd2makezd2boxzd2typezb0zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_switchzf2Cinfozd2approxzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762switchza7f2cinf2287z00,
		BGl_z62switchzf2Cinfozd2approxz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_literalzf2Cinfozd2typezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762literalza7f2cin2288z00,
		BGl_z62literalzf2Cinfozd2typez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cvarzf2Cinfozd2nilzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762cvarza7f2cinfoza72289za7,
		BGl_z62cvarzf2Cinfozd2nilz42zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setqzf2Cinfozd2approxzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762setqza7f2cinfoza72290za7,
		BGl_z62setqzf2Cinfozd2approxz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzf2Cinfozd2predicatezd2ofzd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72291za7,
		BGl_z62cfunzf2Cinfozd2predicatezd2ofzd2setz12z50zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genpatchidzf2Cinfozd2rindexzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762genpatchidza7f22292z00,
		BGl_z62genpatchidzf2Cinfozd2rindexz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2arityzd2envz20zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2293z00,
		BGl_z62internzd2sfunzf2Cinfozd2arityz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2setz12zf2Cinfozd2typezd2envz32zzcfa_approxz00,
		BgL_bgl_za762boxza7d2setza712za72294z00,
		BGl_z62boxzd2setz12zf2Cinfozd2typez82zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Cinfozd2sidezd2effectzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22295za7,
		BGl_z62makezd2boxzf2Cinfozd2sidezd2effectzd2setz12z82zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_failzf2Cinfozd2objzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762failza7f2cinfoza72296za7,
		BGl_z62failzf2Cinfozd2objz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2setz12zf2Ozd2Cinfozd2nilzd2envze0zzcfa_approxz00,
		BgL_bgl_za762boxza7d2setza712za72297z00,
		BGl_z62boxzd2setz12zf2Ozd2Cinfozd2nilz50zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Ozd2Cinfozd2vtypezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2o2298za7,
		BGl_z62boxzd2refzf2Ozd2Cinfozd2vtypez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_patchzf2Cinfozd2approxzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762patchza7f2cinfo2299z00,
		BGl_z62patchzf2Cinfozd2approxz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2setz12zf2Ozd2Cinfozf3zd2envzc1zzcfa_approxz00,
		BgL_bgl_za762boxza7d2setza712za72300z00,
		BGl_z62boxzd2setz12zf2Ozd2Cinfozf3z71zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genpatchidzf2Cinfozd2typezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762genpatchidza7f22301z00,
		BGl_z62genpatchidzf2Cinfozd2typezd2setz12z82zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2localzd2idzd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2lo2302z00,
		BGl_z62reshapedzd2localzd2idz62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2occurrencewzd2setz12zd2envz12zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2303z00,
		BGl_z62reshapedzd2globalzd2occurrencewzd2setz12za2zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genpatchidzf2Cinfozd2loczd2envzf2zzcfa_approxz00,
		BgL_bgl_za762genpatchidza7f22304z00,
		BGl_z62genpatchidzf2Cinfozd2locz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genpatchidzf2Cinfozd2keyzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762genpatchidza7f22305z00,
		BGl_z62genpatchidzf2Cinfozd2keyz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2argszd2noescapezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2306z00,
		BGl_z62internzd2sfunzf2Cinfozd2argszd2noescapez42zzcfa_approxz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_failzf2Cinfozd2objzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762failza7f2cinfoza72307za7,
		BGl_z62failzf2Cinfozd2objzd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2keyszd2envz20zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2308z00,
		BGl_z62internzd2sfunzf2Cinfozd2keysz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_unionzd2approx2z12zd2envz12zzcfa_approxz00,
		BgL_bgl_za762unionza7d2appro2309z00,
		BGl_z62unionzd2approx2z12za2zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzf2Cinfozd2effectzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72310za7,
		BGl_z62cfunzf2Cinfozd2effectz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzf2Cinfozd2macrozf3zd2envz01zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72311za7,
		BGl_z62cfunzf2Cinfozd2macrozf3zb1zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzd2setz12zf2Cinfozd2envz32zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7d22312za7,
		BGl_z62makezd2boxzd2setz12zf2Cinfoz82zzcfa_approxz00, 0L, BUNSPEC, 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2exzd2itzf2Cinfozd2typezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762setza7d2exza7d2it2313za7,
		BGl_z62setzd2exzd2itzf2Cinfozd2typez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Ozd2Cinfozd2loczd2envzf2zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2o2314za7,
		BGl_z62boxzd2refzf2Ozd2Cinfozd2locz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_funcallzf2Cinfozd2typezd2errorzd2noticedzf3zd2envz01zzcfa_approxz00,
		BgL_bgl_za762funcallza7f2cin2315z00,
		BGl_z62funcallzf2Cinfozd2typezd2errorzd2noticedzf3zb1zzcfa_approxz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2loczd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2316z00,
		BGl_z62internzd2sfunzf2Cinfozd2loczd2setz12z50zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Ozd2Cinfozd2keyzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2o2317za7,
		BGl_z62boxzd2refzf2Ozd2Cinfozd2keyz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_approxzd2typezd2lockedzf3zd2envz21zzcfa_approxz00,
		BgL_bgl_za762approxza7d2type2318z00,
		BGl_z62approxzd2typezd2lockedzf3z91zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2sidezd2effectzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2319z00,
		BGl_z62externzd2sfunzf2Cinfozd2sidezd2effectz42zzcfa_approxz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_funcallzf2Cinfozd2approxzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762funcallza7f2cin2320z00,
		BGl_z62funcallzf2Cinfozd2approxz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_patchzf2Cinfozf3zd2envzd3zzcfa_approxz00,
		BgL_bgl_za762patchza7f2cinfo2321z00,
		BGl_z62patchzf2Cinfozf3z63zzcfa_approxz00, 0L, BUNSPEC, 1);
	extern obj_t BGl_shapezd2envzd2zztools_shapez00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2argszd2noescapezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2322z00,
		BGl_z62externzd2sfunzf2Cinfozd2argszd2noescapez42zzcfa_approxz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_funcallzf2Cinfozd2argszd2envzf2zzcfa_approxz00,
		BgL_bgl_za762funcallza7f2cin2323z00,
		BGl_z62funcallzf2Cinfozd2argsz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2modulezd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2324z00,
		BGl_z62reshapedzd2globalzd2modulez62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzf2Cinfozd2arityzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72325za7,
		BGl_z62cfunzf2Cinfozd2arityz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Cinfozd2varzd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2c2326za7,
		BGl_z62boxzd2refzf2Cinfozd2varzd2setz12z50zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_appzd2lyzf2Cinfozd2funzd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762appza7d2lyza7f2ci2327za7,
		BGl_z62appzd2lyzf2Cinfozd2funzd2setz12z50zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2failzf2Cinfozd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2failza7f2328za7,
		BGl_z62makezd2failzf2Cinfoz42zzcfa_approxz00, 0L, BUNSPEC, 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2thezd2closurezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2329z00,
		BGl_z62internzd2sfunzf2Cinfozd2thezd2closurezd2setz12z82zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_failzf2Cinfozd2nilzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762failza7f2cinfoza72330za7,
		BGl_z62failzf2Cinfozd2nilz42zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2thezd2closurezd2globalzd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2331z00,
		BGl_z62externzd2sfunzf2Cinfozd2thezd2closurezd2globalzd2setz12z50zzcfa_approxz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2stackzd2allocatorzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2332z00,
		BGl_z62externzd2sfunzf2Cinfozd2stackzd2allocatorz42zzcfa_approxz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genpatchidzf2Cinfozd2effectzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762genpatchidza7f22333z00,
		BGl_z62genpatchidzf2Cinfozd2effectz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_funcallzf2Cinfozd2funzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762funcallza7f2cin2334z00,
		BGl_z62funcallzf2Cinfozd2funzd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_funcallzf2Cinfozd2arityzd2errorzd2noticedzf3zd2setz12zd2envzc1zzcfa_approxz00,
		BgL_bgl_za762funcallza7f2cin2335z00,
		BGl_z62funcallzf2Cinfozd2arityzd2errorzd2noticedzf3zd2setz12z71zzcfa_approxz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Ozd2Cinfozd2typezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22336za7,
		BGl_z62makezd2boxzf2Ozd2Cinfozd2typez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jumpzd2exzd2itzf2Cinfozd2typezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762jumpza7d2exza7d2i2337za7,
		BGl_z62jumpzd2exzd2itzf2Cinfozd2typez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2predicatezd2ofzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2338z00,
		BGl_z62externzd2sfunzf2Cinfozd2predicatezd2ofz42zzcfa_approxz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_conditionalzf2Cinfozd2falsezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762conditionalza7f2339z00,
		BGl_z62conditionalzf2Cinfozd2falsezd2setz12z82zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2setz12zf2Cinfozd2vtypezd2envz32zzcfa_approxz00,
		BgL_bgl_za762boxza7d2setza712za72340z00,
		BGl_z62boxzd2setz12zf2Cinfozd2vtypez82zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Ozd2Cinfozf3zd2envzd3zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2o2341za7,
		BGl_z62boxzd2refzf2Ozd2Cinfozf3z63zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_switchzf2Cinfozd2testzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762switchza7f2cinf2342z00,
		BGl_z62switchzf2Cinfozd2testzd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2thezd2closurezd2globalzd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2343z00,
		BGl_z62internzd2sfunzf2Cinfozd2thezd2closurezd2globalzd2setz12z50zzcfa_approxz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_kwotezf2nodezd2valuezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762kwoteza7f2nodeza72344za7,
		BGl_z62kwotezf2nodezd2valuez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2pragmazd2setz12zd2envz12zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2345z00,
		BGl_z62reshapedzd2globalzd2pragmazd2setz12za2zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2localzd2valzd2noescapezd2setz12zd2envzc0zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2lo2346z00,
		BGl_z62reshapedzd2localzd2valzd2noescapezd2setz12z70zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2valuezd2setz12zd2envz12zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2347z00,
		BGl_z62reshapedzd2globalzd2valuezd2setz12za2zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2propertyzd2envz20zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2348z00,
		BGl_z62externzd2sfunzf2Cinfozd2propertyz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_approxzd2dupzd2envz00zzcfa_approxz00,
		BgL_bgl_za762approxza7d2dupza72349za7,
		BGl_z62approxzd2dupzb0zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2setz12zf2Cinfozf3zd2envz13zzcfa_approxz00,
		BgL_bgl_za762boxza7d2setza712za72350z00,
		BGl_z62boxzd2setz12zf2Cinfozf3za3zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2exzd2itzf2Cinfozd2loczd2envzf2zzcfa_approxz00,
		BgL_bgl_za762setza7d2exza7d2it2351za7,
		BGl_z62setzd2exzd2itzf2Cinfozd2locz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2argszd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2352z00,
		BGl_z62externzd2sfunzf2Cinfozd2argszd2setz12z50zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_approxzd2setzd2topz12zd2envzc0zzcfa_approxz00,
		BgL_bgl_za762approxza7d2setza72353za7,
		BGl_z62approxzd2setzd2topz12z70zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2makezd2boxzd2keyzd2setz12zd2envzc0zzcfa_approxz00,
		BgL_bgl_za762preza7d2makeza7d22354za7,
		BGl_z62prezd2makezd2boxzd2keyzd2setz12z70zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_patchzf2Cinfozd2valuezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762patchza7f2cinfo2355z00,
		BGl_z62patchzf2Cinfozd2valuez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_literalzf2Cinfozd2nilzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762literalza7f2cin2356z00,
		BGl_z62literalzf2Cinfozd2nilz42zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_conditionalzf2Cinfozd2typezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762conditionalza7f2357z00,
		BGl_z62conditionalzf2Cinfozd2typez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Cinfozd2varzd2envz20zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2c2358za7,
		BGl_z62boxzd2refzf2Cinfozd2varz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_appzd2lyzf2Cinfozd2funzd2envz20zzcfa_approxz00,
		BgL_bgl_za762appza7d2lyza7f2ci2359za7,
		BGl_z62appzd2lyzf2Cinfozd2funz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_funcallzf2Cinfozd2loczd2envzf2zzcfa_approxz00,
		BgL_bgl_za762funcallza7f2cin2360z00,
		BGl_z62funcallzf2Cinfozd2locz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2predicatezd2ofzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2361z00,
		BGl_z62internzd2sfunzf2Cinfozd2predicatezd2ofz42zzcfa_approxz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_appzd2lyzf2Cinfozd2typezd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762appza7d2lyza7f2ci2362za7,
		BGl_z62appzd2lyzf2Cinfozd2typezd2setz12z50zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2makezd2boxzf2Cinfozd2envz20zzcfa_approxz00,
		BgL_bgl_za762makeza7d2makeza7d2363za7,
		BGl_z62makezd2makezd2boxzf2Cinfoz90zzcfa_approxz00, 0L, BUNSPEC, 8);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2genpatchidzf2Cinfozd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2genpat2364z00,
		BGl_z62makezd2genpatchidzf2Cinfoz42zzcfa_approxz00, 0L, BUNSPEC, 9);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2setqzf2Cinfozd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2setqza7f2365za7,
		BGl_z62makezd2setqzf2Cinfoz42zzcfa_approxz00, 0L, BUNSPEC, 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2stackablezd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2366z00,
		BGl_z62internzd2sfunzf2Cinfozd2stackablezd2setz12z50zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_failzf2Cinfozd2typezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762failza7f2cinfoza72367za7,
		BGl_z62failzf2Cinfozd2typez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sexitzf2Cinfozf3zd2envzd3zzcfa_approxz00,
		BgL_bgl_za762sexitza7f2cinfo2368z00,
		BGl_z62sexitzf2Cinfozf3z63zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzf2Cinfozd2failsafezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72369za7,
		BGl_z62cfunzf2Cinfozd2failsafezd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2localzd2volatilezd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2lo2370z00,
		BGl_z62reshapedzd2localzd2volatilez62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2effectzd2envz20zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2371z00,
		BGl_z62internzd2sfunzf2Cinfozd2effectz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2polymorphiczf3zd2setz12zd2envz13zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2372z00,
		BGl_z62externzd2sfunzf2Cinfozd2polymorphiczf3zd2setz12za3zzcfa_approxz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2setz12zf2Ozd2Cinfozd2vtypezd2envze0zzcfa_approxz00,
		BgL_bgl_za762boxza7d2setza712za72373z00,
		BGl_z62boxzd2setz12zf2Ozd2Cinfozd2vtypez50zzcfa_approxz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2approxzd2envz00zzcfa_approxz00,
		BgL_bgl_za762makeza7d2approx2374z00, BGl_z62makezd2approxzb0zzcfa_approxz00,
		0L, BUNSPEC, 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2localzd2valuezd2setz12zd2envz12zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2lo2375z00,
		BGl_z62reshapedzd2localzd2valuezd2setz12za2zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2setz12zf2Cinfozd2valuezd2envz32zzcfa_approxz00,
		BgL_bgl_za762boxza7d2setza712za72376z00,
		BGl_z62boxzd2setz12zf2Cinfozd2valuez82zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2bodyzd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2377z00,
		BGl_z62externzd2sfunzf2Cinfozd2bodyzd2setz12z50zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Cinfozd2clozd2envzf3zd2setz12zd2envz13zzcfa_approxz00,
		BgL_bgl_za762svarza7f2cinfoza72378za7,
		BGl_z62svarzf2Cinfozd2clozd2envzf3zd2setz12za3zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2namezd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2379z00,
		BGl_z62reshapedzd2globalzd2namez62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_conditionalzf2Cinfozd2truezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762conditionalza7f2380z00,
		BGl_z62conditionalzf2Cinfozd2truezd2setz12z82zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_funcallzf2Cinfozd2strengthzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762funcallza7f2cin2381z00,
		BGl_z62funcallzf2Cinfozd2strengthzd2setz12z82zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Ozd2Cinfozd2keyzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2o2382za7,
		BGl_z62boxzd2refzf2Ozd2Cinfozd2keyzd2setz12z82zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2internzd2sfunzf2Cinfozd2envz20zzcfa_approxz00,
		BgL_bgl_za762makeza7d2intern2383z00,
		BGl_z62makezd2internzd2sfunzf2Cinfoz90zzcfa_approxz00, 0L, BUNSPEC, 25);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2stampzd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2384z00,
		BGl_z62internzd2sfunzf2Cinfozd2stampzd2setz12z50zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2accesszd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2385z00,
		BGl_z62reshapedzd2globalzd2accessz62zzcfa_approxz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfunzf2Cinfozf3zd2envzd3zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72386za7,
		BGl_z62cfunzf2Cinfozf3z63zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2localzd2valuezd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2lo2387z00,
		BGl_z62reshapedzd2localzd2valuez62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2switchzf2Cinfozd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2switch2388z00,
		BGl_z62makezd2switchzf2Cinfoz42zzcfa_approxz00, 0L, BUNSPEC, 8);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Ozd2Cinfozd2nilzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22389za7,
		BGl_z62makezd2boxzf2Ozd2Cinfozd2nilz42zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jumpzd2exzd2itzf2Cinfozd2nilzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762jumpza7d2exza7d2i2390za7,
		BGl_z62jumpzd2exzd2itzf2Cinfozd2nilz42zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Cinfozd2keyzd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22391za7,
		BGl_z62makezd2boxzf2Cinfozd2keyzd2setz12z50zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2idzd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2392z00,
		BGl_z62reshapedzd2globalzd2idz62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_conditionalzf2Cinfozf3zd2envzd3zzcfa_approxz00,
		BgL_bgl_za762conditionalza7f2393z00,
		BGl_z62conditionalzf2Cinfozf3z63zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2thezd2closurezd2globalzd2envz20zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2394z00,
		BGl_z62externzd2sfunzf2Cinfozd2thezd2closurezd2globalz90zzcfa_approxz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzf2Cinfozd2argszd2typezd2envz20zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72395za7,
		BGl_z62cfunzf2Cinfozd2argszd2typez90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2exzd2itzf2Cinfozd2onexitzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762setza7d2exza7d2it2396za7,
		BGl_z62setzd2exzd2itzf2Cinfozd2onexitzd2setz12z82zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_kwotezf2nodezf3zd2envzd3zzcfa_approxz00,
		BgL_bgl_za762kwoteza7f2nodeza72397za7,
		BGl_z62kwotezf2nodezf3z63zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2sidezd2effectzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2398z00,
		BGl_z62internzd2sfunzf2Cinfozd2sidezd2effectz42zzcfa_approxz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_setqzf2Cinfozd2nilzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762setqza7f2cinfoza72399za7,
		BGl_z62setqzf2Cinfozd2nilz42zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2importzd2setz12zd2envz12zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2400z00,
		BGl_z62reshapedzd2globalzd2importzd2setz12za2zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2effectzd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2401z00,
		BGl_z62externzd2sfunzf2Cinfozd2effectzd2setz12z50zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_patchzf2Cinfozd2patchidzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762patchza7f2cinfo2402z00,
		BGl_z62patchzf2Cinfozd2patchidzd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2bodyzd2envz20zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2403z00,
		BGl_z62internzd2sfunzf2Cinfozd2bodyz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2dssslzd2keywordszd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2404z00,
		BGl_z62externzd2sfunzf2Cinfozd2dssslzd2keywordszd2setz12z82zzcfa_approxz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2makezd2boxzd2sidezd2effectzd2envzd2zzcfa_approxz00,
		BgL_bgl_za762preza7d2makeza7d22405za7,
		BGl_z62prezd2makezd2boxzd2sidezd2effectz62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2localzd2nilzd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2lo2406z00,
		BGl_z62reshapedzd2localzd2nilz62zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_funcallzf2Cinfozd2argszd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762funcallza7f2cin2407z00,
		BGl_z62funcallzf2Cinfozd2argszd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2modulezd2setz12zd2envz12zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2408z00,
		BGl_z62reshapedzd2globalzd2modulezd2setz12za2zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_vectorzd2approxzf3zd2envzf3zzcfa_approxz00,
		BgL_bgl_za762vectorza7d2appr2409z00,
		BGl_z62vectorzd2approxzf3z43zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2appzd2lyzf2Cinfozd2envz20zzcfa_approxz00,
		BgL_bgl_za762makeza7d2appza7d22410za7,
		BGl_z62makezd2appzd2lyzf2Cinfoz90zzcfa_approxz00, 0L, BUNSPEC, 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2svarzf2Cinfozd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2svarza7f2411za7,
		BGl_z62makezd2svarzf2Cinfoz42zzcfa_approxz00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2keyszd2envz20zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2412z00,
		BGl_z62externzd2sfunzf2Cinfozd2keysz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cvarzf2Cinfozd2macrozf3zd2envz01zzcfa_approxz00,
		BgL_bgl_za762cvarza7f2cinfoza72413za7,
		BGl_z62cvarzf2Cinfozd2macrozf3zb1zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_svarzf2Cinfozd2nilzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762svarza7f2cinfoza72414za7,
		BGl_z62svarzf2Cinfozd2nilz42zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2localzd2fastzd2alphazd2setz12zd2envzc0zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2lo2415z00,
		BGl_z62reshapedzd2localzd2fastzd2alphazd2setz12z70zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Ozd2Cinfozd2valuezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22416za7,
		BGl_z62makezd2boxzf2Ozd2Cinfozd2valuezd2setz12z82zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jumpzd2exzd2itzf2Cinfozd2valuezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762jumpza7d2exza7d2i2417za7,
		BGl_z62jumpzd2exzd2itzf2Cinfozd2valuezd2setz12z82zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2localzd2occurrencezd2setz12zd2envz12zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2lo2418z00,
		BGl_z62reshapedzd2localzd2occurrencezd2setz12za2zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_conditionalzf2Cinfozd2testzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762conditionalza7f2419z00,
		BGl_z62conditionalzf2Cinfozd2testzd2setz12z82zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2argszd2retescapezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2420z00,
		BGl_z62internzd2sfunzf2Cinfozd2argszd2retescapez42zzcfa_approxz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2approxzd2envz20zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2421z00,
		BGl_z62externzd2sfunzf2Cinfozd2approxz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2setz12zf2Ozd2Cinfozd2valuezd2envze0zzcfa_approxz00,
		BgL_bgl_za762boxza7d2setza712za72422z00,
		BGl_z62boxzd2setz12zf2Ozd2Cinfozd2valuez50zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzf2Cinfozd2infixzf3zd2setz12zd2envzc1zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72423za7,
		BGl_z62cfunzf2Cinfozd2infixzf3zd2setz12z71zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_procedurezd2approxzf3zd2envzf3zzcfa_approxz00,
		BgL_bgl_za762procedureza7d2a2424z00,
		BGl_z62procedurezd2approxzf3z43zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2userzf3zd2setz12zd2envze1zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2425z00,
		BGl_z62reshapedzd2globalzd2userzf3zd2setz12z51zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2localzd2userzf3zd2envz21zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2lo2426z00,
		BGl_z62reshapedzd2localzd2userzf3z91zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2argszd2envz20zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2427z00,
		BGl_z62internzd2sfunzf2Cinfozd2argsz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2topzf3zd2setz12zd2envz13zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2428z00,
		BGl_z62externzd2sfunzf2Cinfozd2topzf3zd2setz12za3zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_conszd2approxzf3zd2envzf3zzcfa_approxz00,
		BgL_bgl_za762consza7d2approx2429z00,
		BGl_z62conszd2approxzf3z43zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Cinfozd2clozd2envzf3zd2envzd3zzcfa_approxz00,
		BgL_bgl_za762svarza7f2cinfoza72430za7,
		BGl_z62svarzf2Cinfozd2clozd2envzf3z63zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2argszd2retescapezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2431z00,
		BGl_z62externzd2sfunzf2Cinfozd2argszd2retescapezd2setz12z82zzcfa_approxz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2occurrencezd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2432z00,
		BGl_z62reshapedzd2globalzd2occurrencez62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2aliaszd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2433z00,
		BGl_z62reshapedzd2globalzd2aliasz62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_approxzd2checkzd2haszd2procedurezf3zd2envzf3zzcfa_approxz00,
		BgL_bgl_za762approxza7d2chec2434z00,
		BGl_z62approxzd2checkzd2haszd2procedurezf3z43zzcfa_approxz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Ozd2Cinfozd2vtypezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22435za7,
		BGl_z62makezd2boxzf2Ozd2Cinfozd2vtypezd2setz12z82zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2arityzd2envz20zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2436z00,
		BGl_z62externzd2sfunzf2Cinfozd2arityz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2patchzf2Cinfozd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2patchza72437za7,
		BGl_z62makezd2patchzf2Cinfoz42zzcfa_approxz00, 0L, BUNSPEC, 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_switchzf2Cinfozd2sidezd2effectzd2envz20zzcfa_approxz00,
		BgL_bgl_za762switchza7f2cinf2438z00,
		BGl_z62switchzf2Cinfozd2sidezd2effectz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2optionalszd2envz20zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2439z00,
		BGl_z62internzd2sfunzf2Cinfozd2optionalsz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Ozd2Cinfozd2valuezd2approxzd2envz20zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22440za7,
		BGl_z62makezd2boxzf2Ozd2Cinfozd2valuezd2approxz90zzcfa_approxz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_prezd2clozd2envzf3zd2envz21zzcfa_approxz00,
		BgL_bgl_za762preza7d2cloza7d2e2441za7,
		BGl_z62prezd2clozd2envzf3z91zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzf2Cinfozd2topzf3zd2envz01zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72442za7,
		BGl_z62cfunzf2Cinfozd2topzf3zb1zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_scnstzf2Cinfozd2loczd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762scnstza7f2cinfo2443z00,
		BGl_z62scnstzf2Cinfozd2loczd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2stackablezd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2444z00,
		BGl_z62externzd2sfunzf2Cinfozd2stackablezd2setz12z50zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2reshapedzd2localzd2envzd2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2reshap2445z00,
		BGl_z62makezd2reshapedzd2localz62zzcfa_approxz00, 0L, BUNSPEC, 14);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2exzd2itzf2Cinfozd2bodyzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762setza7d2exza7d2it2446za7,
		BGl_z62setzd2exzd2itzf2Cinfozd2bodyzd2setz12z82zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Ozd2Cinfozd2typezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2o2447za7,
		BGl_z62boxzd2refzf2Ozd2Cinfozd2typezd2setz12z82zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Cinfozd2vtypezd2envz20zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22448za7,
		BGl_z62makezd2boxzf2Cinfozd2vtypez90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Cinfozd2keyzd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2c2449za7,
		BGl_z62boxzd2refzf2Cinfozd2keyzd2setz12z50zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Ozd2Cinfozd2vtypezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2o2450za7,
		BGl_z62boxzd2refzf2Ozd2Cinfozd2vtypezd2setz12z82zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_approxzd2haszd2procedurezf3zd2setz12zd2envze1zzcfa_approxz00,
		BgL_bgl_za762approxza7d2hasza72451za7,
		BGl_z62approxzd2haszd2procedurezf3zd2setz12z51zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzf2Cinfozd2infixzf3zd2envz01zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72452za7,
		BGl_z62cfunzf2Cinfozd2infixzf3zb1zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2setz12zf2Cinfozd2varzd2envz32zzcfa_approxz00,
		BgL_bgl_za762boxza7d2setza712za72453z00,
		BGl_z62boxzd2setz12zf2Cinfozd2varz82zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzf2Cinfozd2methodzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72454za7,
		BGl_z62cfunzf2Cinfozd2methodz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzf2Cinfozd2argszd2noescapezd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72455za7,
		BGl_z62cfunzf2Cinfozd2argszd2noescapezd2setz12z50zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sexitzf2Cinfozd2nilzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762sexitza7f2cinfo2456z00,
		BGl_z62sexitzf2Cinfozd2nilz42zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Cinfozd2sidezd2effectzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2c2457za7,
		BGl_z62boxzd2refzf2Cinfozd2sidezd2effectz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_switchzf2Cinfozd2keyzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762switchza7f2cinf2458z00,
		BGl_z62switchzf2Cinfozd2keyzd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genpatchidzf2Cinfozd2nilzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762genpatchidza7f22459z00,
		BGl_z62genpatchidzf2Cinfozd2nilz42zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genpatchidzf2Cinfozd2rindexzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762genpatchidza7f22460z00,
		BGl_z62genpatchidzf2Cinfozd2rindexzd2setz12z82zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2localzd2userzf3zd2setz12zd2envze1zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2lo2461z00,
		BGl_z62reshapedzd2localzd2userzf3zd2setz12z51zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzf2Cinfozd2predicatezd2ofzd2envz20zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72462za7,
		BGl_z62cfunzf2Cinfozd2predicatezd2ofz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Cinfozd2approxzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762sexitza7f2cinfo2463z00,
		BGl_z62sexitzf2Cinfozd2approxz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_setqzf2Cinfozf3zd2envzd3zzcfa_approxz00,
		BgL_bgl_za762setqza7f2cinfoza72464za7,
		BGl_z62setqzf2Cinfozf3z63zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2localzd2occurrencezd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2lo2465z00,
		BGl_z62reshapedzd2localzd2occurrencez62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_getzd2nodezd2atomzd2valuezd2envz00zzcfa_approxz00,
		BgL_bgl_za762getza7d2nodeza7d22466za7,
		BGl_z62getzd2nodezd2atomzd2valuezb0zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2predicatezd2ofzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2467z00,
		BGl_z62internzd2sfunzf2Cinfozd2predicatezd2ofzd2setz12z82zzcfa_approxz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_scnstzf2Cinfozd2loczd2envzf2zzcfa_approxz00,
		BgL_bgl_za762scnstza7f2cinfo2468z00,
		BGl_z62scnstzf2Cinfozd2locz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2stackablezd2envz20zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2469z00,
		BGl_z62externzd2sfunzf2Cinfozd2stackablez90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2typezd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2470z00,
		BGl_z62reshapedzd2globalzd2typez62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Ozd2Cinfozd2nilzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2o2471za7,
		BGl_z62boxzd2refzf2Ozd2Cinfozd2nilz42zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2sidezd2effectzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2472z00,
		BGl_z62internzd2sfunzf2Cinfozd2sidezd2effectzd2setz12z82zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzd2setz12zf2Ozd2Cinfozd2envze0zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7d22473za7,
		BGl_z62makezd2boxzd2setz12zf2Ozd2Cinfoz50zzcfa_approxz00, 0L, BUNSPEC, 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2setz12zf2Cinfozd2approxzd2envz32zzcfa_approxz00,
		BgL_bgl_za762boxza7d2setza712za72474z00,
		BGl_z62boxzd2setz12zf2Cinfozd2approxz82zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_literalzf2Cinfozd2valuezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762literalza7f2cin2475z00,
		BGl_z62literalzf2Cinfozd2valuezd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Ozd2Cinfozd2sidezd2effectzd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2o2476za7,
		BGl_z62boxzd2refzf2Ozd2Cinfozd2sidezd2effectzd2setz12z50zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2removablezd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2477z00,
		BGl_z62reshapedzd2globalzd2removablez62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2scnstzf2Cinfozd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2scnstza72478za7,
		BGl_z62makezd2scnstzf2Cinfoz42zzcfa_approxz00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Ozd2Cinfozd2approxzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22479za7,
		BGl_z62makezd2boxzf2Ozd2Cinfozd2approxz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jumpzd2exzd2itzf2Cinfozd2approxzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762jumpza7d2exza7d2i2480za7,
		BGl_z62jumpzd2exzd2itzf2Cinfozd2approxz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_failzf2Cinfozd2proczd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762failza7f2cinfoza72481za7,
		BGl_z62failzf2Cinfozd2proczd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_failzf2Cinfozd2proczd2envzf2zzcfa_approxz00,
		BgL_bgl_za762failza7f2cinfoza72482za7,
		BGl_z62failzf2Cinfozd2procz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Cinfozd2loczd2envz20zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2c2483za7,
		BGl_z62boxzd2refzf2Cinfozd2locz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Cinfozd2keyzd2envz20zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2c2484za7,
		BGl_z62boxzd2refzf2Cinfozd2keyz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2argszd2noescapezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2485z00,
		BGl_z62externzd2sfunzf2Cinfozd2argszd2noescapezd2setz12z82zzcfa_approxz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2exzd2itzf2Cinfozd2varzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762setza7d2exza7d2it2486za7,
		BGl_z62setzd2exzd2itzf2Cinfozd2varzd2setz12z82zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzf2Cinfozd2failsafezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72487za7,
		BGl_z62cfunzf2Cinfozd2failsafez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2thezd2closurezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2488z00,
		BGl_z62externzd2sfunzf2Cinfozd2thezd2closurez42zzcfa_approxz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2strengthzd2envz20zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2489z00,
		BGl_z62externzd2sfunzf2Cinfozd2strengthz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2classzd2envz20zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2490z00,
		BGl_z62internzd2sfunzf2Cinfozd2classz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Cinfozd2valuezd2envz20zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22491za7,
		BGl_z62makezd2boxzf2Cinfozd2valuez90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2exzd2itzf2Cinfozf3zd2envzd3zzcfa_approxz00,
		BgL_bgl_za762setza7d2exza7d2it2492za7,
		BGl_z62setzd2exzd2itzf2Cinfozf3z63zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_conditionalzf2Cinfozd2truezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762conditionalza7f2493z00,
		BGl_z62conditionalzf2Cinfozd2truez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_funcallzf2Cinfozd2strengthzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762funcallza7f2cin2494z00,
		BGl_z62funcallzf2Cinfozd2strengthz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2argszd2retescapezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2495z00,
		BGl_z62externzd2sfunzf2Cinfozd2argszd2retescapez42zzcfa_approxz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genpatchidzf2Cinfozd2exprza2zd2setz12zd2envz90zzcfa_approxz00,
		BgL_bgl_za762genpatchidza7f22496z00,
		BGl_z62genpatchidzf2Cinfozd2exprza2zd2setz12z20zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_kwotezf2Cinfozf3zd2envzd3zzcfa_approxz00,
		BgL_bgl_za762kwoteza7f2cinfo2497z00,
		BGl_z62kwotezf2Cinfozf3z63zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Cinfozd2detachedzf3zd2envz01zzcfa_approxz00,
		BgL_bgl_za762sexitza7f2cinfo2498z00,
		BGl_z62sexitzf2Cinfozd2detachedzf3zb1zzcfa_approxz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cvarzf2Cinfozf3zd2envzd3zzcfa_approxz00,
		BgL_bgl_za762cvarza7f2cinfoza72499za7,
		BGl_z62cvarzf2Cinfozf3z63zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2localzd2volatilezd2setz12zd2envz12zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2lo2500z00,
		BGl_z62reshapedzd2localzd2volatilezd2setz12za2zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2effectzd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2501z00,
		BGl_z62internzd2sfunzf2Cinfozd2effectzd2setz12z50zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2funcallzf2Cinfozd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2funcal2502z00,
		BGl_z62makezd2funcallzf2Cinfoz42zzcfa_approxz00, 0L, BUNSPEC, 10);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_scnstzf2Cinfozd2nodezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762scnstza7f2cinfo2503z00,
		BGl_z62scnstzf2Cinfozd2nodez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2conditionalzf2Cinfozd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2condit2504z00,
		BGl_z62makezd2conditionalzf2Cinfoz42zzcfa_approxz00, 0L, BUNSPEC, 8);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2failsafezd2envz20zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2505z00,
		BGl_z62internzd2sfunzf2Cinfozd2failsafez90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2thezd2closurezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2506z00,
		BGl_z62externzd2sfunzf2Cinfozd2thezd2closurezd2setz12z82zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Ozd2Cinfozd2vtypezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22507za7,
		BGl_z62makezd2boxzf2Ozd2Cinfozd2vtypez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2makezd2boxzd2loczd2envz00zzcfa_approxz00,
		BgL_bgl_za762preza7d2makeza7d22508za7,
		BGl_z62prezd2makezd2boxzd2loczb0zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_patchzf2Cinfozd2typezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762patchza7f2cinfo2509z00,
		BGl_z62patchzf2Cinfozd2typezd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_switchzf2Cinfozd2itemzd2typezd2envz20zzcfa_approxz00,
		BgL_bgl_za762switchza7f2cinf2510z00,
		BGl_z62switchzf2Cinfozd2itemzd2typez90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_approxzd2lostzd2stampzd2envzd2zzcfa_approxz00,
		BgL_bgl_za762approxza7d2lost2511z00,
		BGl_z62approxzd2lostzd2stampz62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozf3zd2envz01zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2512z00,
		BGl_z62externzd2sfunzf2Cinfozf3zb1zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_conditionalzf2Cinfozd2loczd2envzf2zzcfa_approxz00,
		BgL_bgl_za762conditionalza7f2513z00,
		BGl_z62conditionalzf2Cinfozd2locz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2makezd2boxzd2keyzd2envz00zzcfa_approxz00,
		BgL_bgl_za762preza7d2makeza7d22514za7,
		BGl_z62prezd2makezd2boxzd2keyzb0zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setqzf2Cinfozd2valuezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762setqza7f2cinfoza72515za7,
		BGl_z62setqzf2Cinfozd2valuez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzf2Cinfozd2argszd2retescapezd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72516za7,
		BGl_z62cfunzf2Cinfozd2argszd2retescapezd2setz12z50zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_conditionalzf2Cinfozd2keyzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762conditionalza7f2517z00,
		BGl_z62conditionalzf2Cinfozd2keyz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Ozd2Cinfozf3zd2envzd3zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22518za7,
		BGl_z62makezd2boxzf2Ozd2Cinfozf3z63zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jumpzd2exzd2itzf2Cinfozf3zd2envzd3zzcfa_approxz00,
		BgL_bgl_za762jumpza7d2exza7d2i2519za7,
		BGl_z62jumpzd2exzd2itzf2Cinfozf3z63zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2exzd2itzf2Cinfozd2nilzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762setza7d2exza7d2it2520za7,
		BGl_z62setzd2exzd2itzf2Cinfozd2nilz42zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_kwotezf2nodezd2nodezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762kwoteza7f2nodeza72521za7,
		BGl_z62kwotezf2nodezd2nodez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzf2Cinfozd2stackzd2allocatorzd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72522za7,
		BGl_z62cfunzf2Cinfozd2stackzd2allocatorzd2setz12z50zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2bodyzd2envz20zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2523z00,
		BGl_z62externzd2sfunzf2Cinfozd2bodyz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzf2Cinfozd2stackzd2allocatorzd2envz20zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72524za7,
		BGl_z62cfunzf2Cinfozd2stackzd2allocatorz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Cinfozd2typezd2envz20zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22525za7,
		BGl_z62makezd2boxzf2Cinfozd2typez90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2setz12zf2Cinfozd2valuezd2setz12zd2envzf2zzcfa_approxz00,
		BgL_bgl_za762boxza7d2setza712za72526z00,
		BGl_z62boxzd2setz12zf2Cinfozd2valuezd2setz12z42zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2importzd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2527z00,
		BGl_z62reshapedzd2globalzd2importz62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_funcallzf2Cinfozd2nilzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762funcallza7f2cin2528z00,
		BGl_z62funcallzf2Cinfozd2nilz42zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Cinfozd2vtypezd2envz20zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2c2529za7,
		BGl_z62boxzd2refzf2Cinfozd2vtypez90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Cinfozd2approxzd2envz20zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2c2530za7,
		BGl_z62boxzd2refzf2Cinfozd2approxz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2srczd2setz12zd2envz12zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2531z00,
		BGl_z62reshapedzd2globalzd2srczd2setz12za2zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzf2Cinfozd2thezd2closurezd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72532za7,
		BGl_z62cfunzf2Cinfozd2thezd2closurezd2setz12z50zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_conditionalzf2Cinfozd2testzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762conditionalza7f2533z00,
		BGl_z62conditionalzf2Cinfozd2testz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2argszd2namezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2534z00,
		BGl_z62externzd2sfunzf2Cinfozd2argszd2namez42zzcfa_approxz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_failzf2Cinfozd2msgzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762failza7f2cinfoza72535za7,
		BGl_z62failzf2Cinfozd2msgzd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_kwotezf2nodezd2typezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762kwoteza7f2nodeza72536za7,
		BGl_z62kwotezf2nodezd2typezd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2223z00zzcfa_approxz00,
		BgL_bgl_string2223za700za7za7c2537za7, "dst is not a procedure", 22);
	      DEFINE_STRING(BGl_string2224z00zzcfa_approxz00,
		BgL_bgl_string2224za700za7za7c2538za7, "APRES TYPE ", 11);
	      DEFINE_STRING(BGl_string2225z00zzcfa_approxz00,
		BgL_bgl_string2225za700za7za7c2539za7, ":", 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_funcallzf2Cinfozd2typezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762funcallza7f2cin2540z00,
		BGl_z62funcallzf2Cinfozd2typezd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2226z00zzcfa_approxz00,
		BgL_bgl_string2226za700za7za7c2541za7, ",", 1);
	      DEFINE_STRING(BGl_string2227z00zzcfa_approxz00,
		BgL_bgl_string2227za700za7za7c2542za7, "Cfa/approx.scm", 14);
	      DEFINE_STRING(BGl_string2228z00zzcfa_approxz00,
		BgL_bgl_string2228za700za7za7c2543za7, "APRES UNLESS ", 13);
	      DEFINE_STRING(BGl_string2229z00zzcfa_approxz00,
		BgL_bgl_string2229za700za7za7c2544za7, "APRES X-T ", 10);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2namezd2setz12zd2envz12zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2545z00,
		BGl_z62reshapedzd2globalzd2namezd2setz12za2zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_approxzd2topzf3zd2setz12zd2envz33zzcfa_approxz00,
		BgL_bgl_za762approxza7d2topza72546za7,
		BGl_z62approxzd2topzf3zd2setz12z83zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Ozd2Cinfozd2approxzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2o2547za7,
		BGl_z62boxzd2refzf2Ozd2Cinfozd2approxz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_nodezd2keyzd2setz12zd2envzc0zzcfa_approxz00,
		BgL_bgl_za762nodeza7d2keyza7d22548za7,
		BGl_z62nodezd2keyzd2setz12z70zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2argszd2envz20zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2549z00,
		BGl_z62externzd2sfunzf2Cinfozd2argsz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2exzd2itzf2Cinfozd2bodyzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762setza7d2exza7d2it2550za7,
		BGl_z62setzd2exzd2itzf2Cinfozd2bodyz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_switchzf2Cinfozd2loczd2envzf2zzcfa_approxz00,
		BgL_bgl_za762switchza7f2cinf2551z00,
		BGl_z62switchzf2Cinfozd2locz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2setz12zf2Cinfozd2vtypezd2setz12zd2envzf2zzcfa_approxz00,
		BgL_bgl_za762boxza7d2setza712za72552z00,
		BGl_z62boxzd2setz12zf2Cinfozd2vtypezd2setz12z42zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2setz12zf2Ozd2Cinfozd2typezd2setz12zd2envz20zzcfa_approxz00,
		BgL_bgl_za762boxza7d2setza712za72553z00,
		BGl_z62boxzd2setz12zf2Ozd2Cinfozd2typezd2setz12z90zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_switchzf2Cinfozd2keyzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762switchza7f2cinf2554z00,
		BGl_z62switchzf2Cinfozd2keyz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2libraryzd2setz12zd2envz12zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2555z00,
		BGl_z62reshapedzd2globalzd2libraryzd2setz12za2zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Cinfozd2stampzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762svarza7f2cinfoza72556za7,
		BGl_z62svarzf2Cinfozd2stampzd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2exzd2itzf2Cinfozd2typezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762setza7d2exza7d2it2557za7,
		BGl_z62setzd2exzd2itzf2Cinfozd2typezd2setz12z82zzcfa_approxz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2230z00zzcfa_approxz00,
		BgL_bgl_string2230za700za7za7c2558za7, "APRES ALLOC/TMP ", 16);
	      DEFINE_STRING(BGl_string2231z00zzcfa_approxz00,
		BgL_bgl_string2231za700za7za7c2559za7, "APRES union ", 12);
	      DEFINE_STRING(BGl_string2233z00zzcfa_approxz00,
		BgL_bgl_string2233za700za7za7c2560za7, "get-node-atom-value1581", 23);
	      DEFINE_STRING(BGl_string2235z00zzcfa_approxz00,
		BgL_bgl_string2235za700za7za7c2561za7, "shape", 5);
	      DEFINE_STRING(BGl_string2237z00zzcfa_approxz00,
		BgL_bgl_string2237za700za7za7c2562za7, "get-node-atom-value", 19);
	      DEFINE_STRING(BGl_string2239z00zzcfa_approxz00,
		BgL_bgl_string2239za700za7za7c2563za7, "cfa_approx", 10);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2makezd2boxzd2stackablezd2setz12zd2envzc0zzcfa_approxz00,
		BgL_bgl_za762preza7d2makeza7d22564za7,
		BGl_z62prezd2makezd2boxzd2stackablezd2setz12z70zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_switchzf2Cinfozd2typezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762switchza7f2cinf2565z00,
		BGl_z62switchzf2Cinfozd2typezd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Cinfozd2stackablezd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22566za7,
		BGl_z62makezd2boxzf2Cinfozd2stackablezd2setz12z50zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2240z00zzcfa_approxz00,
		BgL_bgl_string2240za700za7za7c2567za7,
		"top:false top:true approx (no-atom-value) approx-set-top! approx-set-type! union ",
		81);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2argszd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2568z00,
		BGl_z62internzd2sfunzf2Cinfozd2argszd2setz12z50zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Ozd2Cinfozd2valuezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22569za7,
		BGl_z62makezd2boxzf2Ozd2Cinfozd2valuez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jumpzd2exzd2itzf2Cinfozd2valuezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762jumpza7d2exza7d2i2570za7,
		BGl_z62jumpzd2exzd2itzf2Cinfozd2valuez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2makezd2boxzf2Ozd2Cinfozd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2makeza7d2571za7,
		BGl_z62makezd2makezd2boxzf2Ozd2Cinfoz42zzcfa_approxz00, 0L, BUNSPEC, 9);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2jumpzd2exzd2itzf2Cinfozd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2jumpza7d2572za7,
		BGl_z62makezd2jumpzd2exzd2itzf2Cinfoz42zzcfa_approxz00, 0L, BUNSPEC, 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_conditionalzf2Cinfozd2sidezd2effectzd2envz20zzcfa_approxz00,
		BgL_bgl_za762conditionalza7f2573z00,
		BGl_z62conditionalzf2Cinfozd2sidezd2effectz90zzcfa_approxz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genpatchidzf2Cinfozd2sidezd2effectzd2envz20zzcfa_approxz00,
		BgL_bgl_za762genpatchidza7f22574z00,
		BGl_z62genpatchidzf2Cinfozd2sidezd2effectz90zzcfa_approxz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setqzf2Cinfozd2typezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762setqza7f2cinfoza72575za7,
		BGl_z62setqzf2Cinfozd2typezd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2localzd2removablezd2setz12zd2envz12zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2lo2576z00,
		BGl_z62reshapedzd2localzd2removablezd2setz12za2zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2kwotezf2nodezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2kwoteza72577za7,
		BGl_z62makezd2kwotezf2nodez42zzcfa_approxz00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_literalzf2Cinfozf3zd2envzd3zzcfa_approxz00,
		BgL_bgl_za762literalza7f2cin2578z00,
		BGl_z62literalzf2Cinfozf3z63zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2initzd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2579z00,
		BGl_z62reshapedzd2globalzd2initz62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_emptyzd2approxzd2alloczf3zd2envz21zzcfa_approxz00,
		BgL_bgl_za762emptyza7d2appro2580z00,
		BGl_z62emptyzd2approxzd2alloczf3z91zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_patchzf2Cinfozd2indexzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762patchza7f2cinfo2581z00,
		BGl_z62patchzf2Cinfozd2indexzd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2setz12zf2Cinfozd2varzd2setz12zd2envzf2zzcfa_approxz00,
		BgL_bgl_za762boxza7d2setza712za72582z00,
		BGl_z62boxzd2setz12zf2Cinfozd2varzd2setz12z42zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Cinfozf3zd2envz01zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22583za7,
		BGl_z62makezd2boxzf2Cinfozf3zb1zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2topzf3zd2envzd3zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2584z00,
		BGl_z62internzd2sfunzf2Cinfozd2topzf3z63zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_appzd2lyzf2Cinfozd2argzd2envz20zzcfa_approxz00,
		BgL_bgl_za762appza7d2lyza7f2ci2585za7,
		BGl_z62appzd2lyzf2Cinfozd2argz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Ozd2Cinfozd2sidezd2effectzd2envz20zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22586za7,
		BGl_z62makezd2boxzf2Ozd2Cinfozd2sidezd2effectz90zzcfa_approxz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzf2Cinfozd2argszd2noescapezd2envz20zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72587za7,
		BGl_z62cfunzf2Cinfozd2argszd2noescapez90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzf2Cinfozd2argszd2retescapezd2envz20zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72588za7,
		BGl_z62cfunzf2Cinfozd2argszd2retescapez90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_funcallzf2Cinfozd2vazd2approxzd2envz20zzcfa_approxz00,
		BgL_bgl_za762funcallza7f2cin2589z00,
		BGl_z62funcallzf2Cinfozd2vazd2approxz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2localzd2removablezd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2lo2590z00,
		BGl_z62reshapedzd2localzd2removablez62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2setz12zf2Ozd2Cinfozd2varzd2envze0zzcfa_approxz00,
		BgL_bgl_za762boxza7d2setza712za72591z00,
		BGl_z62boxzd2setz12zf2Ozd2Cinfozd2varz50zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2clozd2envzd2loczd2envz00zzcfa_approxz00,
		BgL_bgl_za762preza7d2cloza7d2e2592za7,
		BGl_z62prezd2clozd2envzd2loczb0zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2typezd2approxzd2envzd2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2typeza7d2593za7,
		BGl_z62makezd2typezd2approxz62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzd2refzf2Ozd2Cinfozd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7d22594za7,
		BGl_z62makezd2boxzd2refzf2Ozd2Cinfoz42zzcfa_approxz00, 0L, BUNSPEC, 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Ozd2Cinfozd2sidezd2effectzd2envz20zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2o2595za7,
		BGl_z62boxzd2refzf2Ozd2Cinfozd2sidezd2effectz90zzcfa_approxz00, 0L, BUNSPEC,
		1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_approxzf3zd2envz21zzcfa_approxz00,
		BgL_bgl_za762approxza7f3za791za72596z00, BGl_z62approxzf3z91zzcfa_approxz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2nilzd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2597z00,
		BGl_z62reshapedzd2globalzd2nilz62zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Ozd2Cinfozd2stackablezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22598za7,
		BGl_z62makezd2boxzf2Ozd2Cinfozd2stackablezd2setz12z82zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2evaluablezf3zd2setz12zd2envze1zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2599z00,
		BGl_z62reshapedzd2globalzd2evaluablezf3zd2setz12z51zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2loczd2envz20zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2600z00,
		BGl_z62internzd2sfunzf2Cinfozd2locz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2setzd2exzd2itzf2Cinfozd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2setza7d22601za7,
		BGl_z62makezd2setzd2exzd2itzf2Cinfoz42zzcfa_approxz00, 0L, BUNSPEC, 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_kwotezf2nodezd2loczd2envzf2zzcfa_approxz00,
		BgL_bgl_za762kwoteza7f2nodeza72602za7,
		BGl_z62kwotezf2nodezd2locz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2localzd2occurrencewzd2setz12zd2envz12zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2lo2603z00,
		BGl_z62reshapedzd2localzd2occurrencewzd2setz12za2zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2setz12zf2Cinfozd2loczd2envz32zzcfa_approxz00,
		BgL_bgl_za762boxza7d2setza712za72604z00,
		BGl_z62boxzd2setz12zf2Cinfozd2locz82zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2bodyzd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2605z00,
		BGl_z62internzd2sfunzf2Cinfozd2bodyzd2setz12z50zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2thezd2closurezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2606z00,
		BGl_z62internzd2sfunzf2Cinfozd2thezd2closurez42zzcfa_approxz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_conditionalzf2Cinfozd2approxzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762conditionalza7f2607z00,
		BGl_z62conditionalzf2Cinfozd2approxz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2jvmzd2typezd2namezd2setz12zd2envz12zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2608z00,
		BGl_z62reshapedzd2globalzd2jvmzd2typezd2namezd2setz12za2zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_approxzd2typezd2setz12zd2envzc0zzcfa_approxz00,
		BgL_bgl_za762approxza7d2type2609z00,
		BGl_z62approxzd2typezd2setz12z70zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Cinfozd2vtypezd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2c2610za7,
		BGl_z62boxzd2refzf2Cinfozd2vtypezd2setz12z50zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_scnstzf2Cinfozd2approxzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762scnstza7f2cinfo2611z00,
		BGl_z62scnstzf2Cinfozd2approxz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2effectzd2envz20zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2612z00,
		BGl_z62externzd2sfunzf2Cinfozd2effectz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_patchzf2Cinfozd2refzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762patchza7f2cinfo2613z00,
		BGl_z62patchzf2Cinfozd2refz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_approxzd2haszd2procedurezf3zd2envz21zzcfa_approxz00,
		BgL_bgl_za762approxza7d2hasza72614za7,
		BGl_z62approxzd2haszd2procedurezf3z91zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2classzd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2615z00,
		BGl_z62externzd2sfunzf2Cinfozd2classzd2setz12z50zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_appzd2lyzf2Cinfozd2loczd2envz20zzcfa_approxz00,
		BgL_bgl_za762appza7d2lyza7f2ci2616za7,
		BGl_z62appzd2lyzf2Cinfozd2locz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_funcallzf2Cinfozd2arityzd2errorzd2noticedzf3zd2envz01zzcfa_approxz00,
		BgL_bgl_za762funcallza7f2cin2617z00,
		BGl_z62funcallzf2Cinfozd2arityzd2errorzd2noticedzf3zb1zzcfa_approxz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_approxzd2setzd2typez12zd2envzc0zzcfa_approxz00,
		BgL_bgl_za762approxza7d2setza72618za7,
		BGl_z62approxzd2setzd2typez12z70zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2propertyzd2envz20zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2619z00,
		BGl_z62internzd2sfunzf2Cinfozd2propertyz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2pragmazd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2620z00,
		BGl_z62reshapedzd2globalzd2pragmaz62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2localzd2valzd2noescapezd2envz00zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2lo2621z00,
		BGl_z62reshapedzd2localzd2valzd2noescapezb0zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_funcallzf2Cinfozd2typezd2errorzd2noticedzf3zd2setz12zd2envzc1zzcfa_approxz00,
		BgL_bgl_za762funcallza7f2cin2622z00,
		BGl_z62funcallzf2Cinfozd2typezd2errorzd2noticedzf3zd2setz12z71zzcfa_approxz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2jvmzd2typezd2namezd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2623z00,
		BGl_z62reshapedzd2globalzd2jvmzd2typezd2namez62zzcfa_approxz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_funcallzf2Cinfozd2functionszd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762funcallza7f2cin2624z00,
		BGl_z62funcallzf2Cinfozd2functionszd2setz12z82zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_scnstzf2Cinfozf3zd2envzd3zzcfa_approxz00,
		BgL_bgl_za762scnstza7f2cinfo2625z00,
		BGl_z62scnstzf2Cinfozf3z63zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2stackzd2allocatorzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2626z00,
		BGl_z62externzd2sfunzf2Cinfozd2stackzd2allocatorzd2setz12z82zzcfa_approxz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2makezd2boxzd2valuezd2setz12zd2envzc0zzcfa_approxz00,
		BgL_bgl_za762preza7d2makeza7d22627za7,
		BGl_z62prezd2makezd2boxzd2valuezd2setz12z70zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzf2Cinfozd2methodzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72628za7,
		BGl_z62cfunzf2Cinfozd2methodzd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_patchzf2Cinfozd2loczd2envzf2zzcfa_approxz00,
		BgL_bgl_za762patchza7f2cinfo2629z00,
		BGl_z62patchzf2Cinfozd2locz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Cinfozd2typezd2envz20zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2c2630za7,
		BGl_z62boxzd2refzf2Cinfozd2typez90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_scnstzf2Cinfozd2classzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762scnstza7f2cinfo2631z00,
		BGl_z62scnstzf2Cinfozd2classzd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2evaluablezf3zd2envz21zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2632z00,
		BGl_z62reshapedzd2globalzd2evaluablezf3z91zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2prezd2clozd2envzd2envz00zzcfa_approxz00,
		BgL_bgl_za762makeza7d2preza7d22633za7,
		BGl_z62makezd2prezd2clozd2envzb0zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2literalzf2Cinfozd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2litera2634z00,
		BGl_z62makezd2literalzf2Cinfoz42zzcfa_approxz00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozf3zd2envz01zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2635z00,
		BGl_z62internzd2sfunzf2Cinfozf3zb1zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzf2Cinfozd2sidezd2effectzd2envz20zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72636za7,
		BGl_z62cfunzf2Cinfozd2sidezd2effectz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_approxzd2allocszd2envz00zzcfa_approxz00,
		BgL_bgl_za762approxza7d2allo2637z00,
		BGl_z62approxzd2allocszb0zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2makezd2boxzd2vtypezd2setz12zd2envzc0zzcfa_approxz00,
		BgL_bgl_za762preza7d2makeza7d22638za7,
		BGl_z62prezd2makezd2boxzd2vtypezd2setz12z70zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2stampzd2envz20zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2639z00,
		BGl_z62internzd2sfunzf2Cinfozd2stampz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2localzd2namezd2setz12zd2envz12zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2lo2640z00,
		BGl_z62reshapedzd2localzd2namezd2setz12za2zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2topzf3zd2setz12zd2envz13zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2641z00,
		BGl_z62internzd2sfunzf2Cinfozd2topzf3zd2setz12za3zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_kwotezf2Cinfozd2loczd2envzf2zzcfa_approxz00,
		BgL_bgl_za762kwoteza7f2cinfo2642z00,
		BGl_z62kwotezf2Cinfozd2locz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_approxzd2topzf3zd2envzf3zzcfa_approxz00,
		BgL_bgl_za762approxza7d2topza72643za7,
		BGl_z62approxzd2topzf3z43zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_conditionalzf2Cinfozd2typezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762conditionalza7f2644z00,
		BGl_z62conditionalzf2Cinfozd2typezd2setz12z82zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2loczd2envz20zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2645z00,
		BGl_z62externzd2sfunzf2Cinfozd2locz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_patchzf2Cinfozd2indexzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762patchza7f2cinfo2646z00,
		BGl_z62patchzf2Cinfozd2indexz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_appzd2lyzf2Cinfozd2approxzd2envz20zzcfa_approxz00,
		BgL_bgl_za762appza7d2lyza7f2ci2647za7,
		BGl_z62appzd2lyzf2Cinfozd2approxz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_scnstzf2Cinfozd2nilzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762scnstza7f2cinfo2648z00,
		BGl_z62scnstzf2Cinfozd2nilz42zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Ozd2Cinfozd2keyzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22649za7,
		BGl_z62makezd2boxzf2Ozd2Cinfozd2keyzd2setz12z82zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2srczd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2650z00,
		BGl_z62reshapedzd2globalzd2srcz62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2loczd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2651z00,
		BGl_z62externzd2sfunzf2Cinfozd2loczd2setz12z50zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2localzd2accesszd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2lo2652z00,
		BGl_z62reshapedzd2localzd2accessz62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2setz12zf2Cinfozd2typezd2setz12zd2envzf2zzcfa_approxz00,
		BgL_bgl_za762boxza7d2setza712za72653z00,
		BGl_z62boxzd2setz12zf2Cinfozd2typezd2setz12z42zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genpatchidzf2Cinfozd2exprza2zd2envz50zzcfa_approxz00,
		BgL_bgl_za762genpatchidza7f22654z00,
		BGl_z62genpatchidzf2Cinfozd2exprza2ze0zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2setz12zf2Ozd2Cinfozd2typezd2envze0zzcfa_approxz00,
		BgL_bgl_za762boxza7d2setza712za72655z00,
		BGl_z62boxzd2setz12zf2Ozd2Cinfozd2typez50zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2libraryzd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2656z00,
		BGl_z62reshapedzd2globalzd2libraryz62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Cinfozd2stampzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762svarza7f2cinfoza72657za7,
		BGl_z62svarzf2Cinfozd2stampz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2dssslzd2keywordszd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2658z00,
		BGl_z62internzd2sfunzf2Cinfozd2dssslzd2keywordszd2setz12z82zzcfa_approxz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Cinfozd2nilzd2envz20zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2c2659za7,
		BGl_z62boxzd2refzf2Cinfozd2nilz90zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2propertyzd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2660z00,
		BGl_z62internzd2sfunzf2Cinfozd2propertyzd2setz12z50zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genpatchidzf2Cinfozd2effectzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762genpatchidza7f22661z00,
		BGl_z62genpatchidzf2Cinfozd2effectzd2setz12z82zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_funcallzf2Cinfozd2typezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762funcallza7f2cin2662z00,
		BGl_z62funcallzf2Cinfozd2typez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_literalzf2Cinfozd2approxzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762literalza7f2cin2663z00,
		BGl_z62literalzf2Cinfozd2approxz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2fastzd2alphazd2setz12zd2envzc0zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2664z00,
		BGl_z62reshapedzd2globalzd2fastzd2alphazd2setz12z70zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2prezd2makezd2boxzd2envz00zzcfa_approxz00,
		BgL_bgl_za762makeza7d2preza7d22665za7,
		BGl_z62makezd2prezd2makezd2boxzb0zzcfa_approxz00, 0L, BUNSPEC, 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2occurrencezd2setz12zd2envz12zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2666z00,
		BGl_z62reshapedzd2globalzd2occurrencezd2setz12za2zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2makezd2boxzd2stackablezd2envz00zzcfa_approxz00,
		BgL_bgl_za762preza7d2makeza7d22667za7,
		BGl_z62prezd2makezd2boxzd2stackablezb0zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_switchzf2Cinfozd2typezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762switchza7f2cinf2668z00,
		BGl_z62switchzf2Cinfozd2typez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2classzd2envz20zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2669z00,
		BGl_z62externzd2sfunzf2Cinfozd2classz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_funcallzf2Cinfozf3zd2envzd3zzcfa_approxz00,
		BgL_bgl_za762funcallza7f2cin2670z00,
		BGl_z62funcallzf2Cinfozf3z63zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2sidezd2effectzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2671z00,
		BGl_z62externzd2sfunzf2Cinfozd2sidezd2effectzd2setz12z82zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2makezd2boxzd2typezd2setz12zd2envzc0zzcfa_approxz00,
		BgL_bgl_za762preza7d2makeza7d22672za7,
		BGl_z62prezd2makezd2boxzd2typezd2setz12z70zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2valuezd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2673z00,
		BGl_z62reshapedzd2globalzd2valuez62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2argszd2retescapezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2674z00,
		BGl_z62internzd2sfunzf2Cinfozd2argszd2retescapezd2setz12z82zzcfa_approxz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2exzd2itzf2Cinfozd2onexitzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762setza7d2exza7d2it2675za7,
		BGl_z62setzd2exzd2itzf2Cinfozd2onexitz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_prezd2makezd2boxzf3zd2envz21zzcfa_approxz00,
		BgL_bgl_za762preza7d2makeza7d22676za7,
		BGl_z62prezd2makezd2boxzf3z91zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2propertyzd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2677z00,
		BGl_z62externzd2sfunzf2Cinfozd2propertyzd2setz12z50zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2strengthzd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2678z00,
		BGl_z62internzd2sfunzf2Cinfozd2strengthzd2setz12z50zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2makezd2boxzd2sidezd2effectzd2setz12zd2envz12zzcfa_approxz00,
		BgL_bgl_za762preza7d2makeza7d22679za7,
		BGl_z62prezd2makezd2boxzd2sidezd2effectzd2setz12za2zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2stackablezd2envz20zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2680z00,
		BGl_z62internzd2sfunzf2Cinfozd2stackablez90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2kwotezf2Cinfozd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2kwoteza72681za7,
		BGl_z62makezd2kwotezf2Cinfoz42zzcfa_approxz00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2typezd2setz12zd2envz12zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2682z00,
		BGl_z62reshapedzd2globalzd2typezd2setz12za2zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_kwotezf2Cinfozd2typezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762kwoteza7f2cinfo2683z00,
		BGl_z62kwotezf2Cinfozd2typez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2makezd2boxzd2nilzd2envz00zzcfa_approxz00,
		BgL_bgl_za762preza7d2makeza7d22684za7,
		BGl_z62prezd2makezd2boxzd2nilzb0zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2topzf3zd2envzd3zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2685z00,
		BGl_z62externzd2sfunzf2Cinfozd2topzf3z63zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Cinfozd2loczd2envz20zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22686za7,
		BGl_z62makezd2boxzf2Cinfozd2locz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setqzf2Cinfozd2valuezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762setqza7f2cinfoza72687za7,
		BGl_z62setqzf2Cinfozd2valuezd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2setz12zf2Ozd2Cinfozd2valuezd2setz12zd2envz20zzcfa_approxz00,
		BgL_bgl_za762boxza7d2setza712za72688z00,
		BGl_z62boxzd2setz12zf2Ozd2Cinfozd2valuezd2setz12z90zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_conditionalzf2Cinfozd2nilzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762conditionalza7f2689z00,
		BGl_z62conditionalzf2Cinfozd2nilz42zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Cinfozd2keyzd2envz20zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22690za7,
		BGl_z62makezd2boxzf2Cinfozd2keyz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2makezd2boxzd2vtypezd2envz00zzcfa_approxz00,
		BgL_bgl_za762preza7d2makeza7d22691za7,
		BGl_z62prezd2makezd2boxzd2vtypezb0zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzf2Cinfozd2sidezd2effectzd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72692za7,
		BGl_z62cfunzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genpatchidzf2Cinfozf3zd2envzd3zzcfa_approxz00,
		BgL_bgl_za762genpatchidza7f22693z00,
		BGl_z62genpatchidzf2Cinfozf3z63zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2failsafezd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2694z00,
		BGl_z62internzd2sfunzf2Cinfozd2failsafezd2setz12z50zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2evalzf3zd2envz21zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2695z00,
		BGl_z62reshapedzd2globalzd2evalzf3z91zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2thezd2closurezd2globalzd2envz20zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2696z00,
		BGl_z62internzd2sfunzf2Cinfozd2thezd2closurezd2globalz90zzcfa_approxz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Cinfozf3zd2envz01zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2c2697za7,
		BGl_z62boxzd2refzf2Cinfozf3zb1zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2strengthzd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2698z00,
		BGl_z62externzd2sfunzf2Cinfozd2strengthzd2setz12z50zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genpatchidzf2Cinfozd2indexzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762genpatchidza7f22699z00,
		BGl_z62genpatchidzf2Cinfozd2indexzd2setz12z82zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_conditionalzf2Cinfozd2sidezd2effectzd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762conditionalza7f2700z00,
		BGl_z62conditionalzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Ozd2Cinfozd2typezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2o2701za7,
		BGl_z62boxzd2refzf2Ozd2Cinfozd2typez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2setz12zf2Ozd2Cinfozd2vtypezd2setz12zd2envz20zzcfa_approxz00,
		BgL_bgl_za762boxza7d2setza712za72702z00,
		BGl_z62boxzd2setz12zf2Ozd2Cinfozd2vtypezd2setz12z90zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzd2refzf2Cinfozd2envz20zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7d22703za7,
		BGl_z62makezd2boxzd2refzf2Cinfoz90zzcfa_approxz00, 0L, BUNSPEC, 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_declarezd2approxzd2setsz12zd2envzc0zzcfa_approxz00,
		BgL_bgl_za762declareza7d2app2704z00,
		BGl_z62declarezd2approxzd2setsz12z70zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2setz12zf2Ozd2Cinfozd2loczd2envze0zzcfa_approxz00,
		BgL_bgl_za762boxza7d2setza712za72705z00,
		BGl_z62boxzd2setz12zf2Ozd2Cinfozd2locz50zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_setqzf2Cinfozd2varzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762setqza7f2cinfoza72706za7,
		BGl_z62setqzf2Cinfozd2varz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2userzf3zd2envz21zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2707z00,
		BGl_z62reshapedzd2globalzd2userzf3z91zzcfa_approxz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_failzf2Cinfozf3zd2envzd3zzcfa_approxz00,
		BgL_bgl_za762failza7f2cinfoza72708za7,
		BGl_z62failzf2Cinfozf3z63zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2aliaszd2setz12zd2envz12zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2709z00,
		BGl_z62reshapedzd2globalzd2aliaszd2setz12za2zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Cinfozd2loczd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762svarza7f2cinfoza72710za7,
		BGl_z62svarzf2Cinfozd2loczd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2failsafezd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2711z00,
		BGl_z62externzd2sfunzf2Cinfozd2failsafezd2setz12z50zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_switchzf2Cinfozd2nilzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762switchza7f2cinf2712z00,
		BGl_z62switchzf2Cinfozd2nilz42zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Ozd2Cinfozd2stackablezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22713za7,
		BGl_z62makezd2boxzf2Ozd2Cinfozd2stackablez42zzcfa_approxz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2initzd2setz12zd2envz12zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2714z00,
		BGl_z62reshapedzd2globalzd2initzd2setz12za2zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_switchzf2Cinfozd2clauseszd2envzf2zzcfa_approxz00,
		BgL_bgl_za762switchza7f2cinf2715z00,
		BGl_z62switchzf2Cinfozd2clausesz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jumpzd2exzd2itzf2Cinfozd2exitzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762jumpza7d2exza7d2i2716za7,
		BGl_z62jumpzd2exzd2itzf2Cinfozd2exitz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Ozd2Cinfozd2typezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22717za7,
		BGl_z62makezd2boxzf2Ozd2Cinfozd2typezd2setz12z82zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jumpzd2exzd2itzf2Cinfozd2typezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762jumpza7d2exza7d2i2718za7,
		BGl_z62jumpzd2exzd2itzf2Cinfozd2typezd2setz12z82zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2predicatezd2ofzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2719z00,
		BGl_z62externzd2sfunzf2Cinfozd2predicatezd2ofzd2setz12z82zzcfa_approxz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_funcallzf2Cinfozd2funzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762funcallza7f2cin2720z00,
		BGl_z62funcallzf2Cinfozd2funz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_patchzf2Cinfozd2typezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762patchza7f2cinfo2721z00,
		BGl_z62patchzf2Cinfozd2typez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2localzd2accesszd2setz12zd2envz12zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2lo2722z00,
		BGl_z62reshapedzd2localzd2accesszd2setz12za2zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzf2Cinfozd2effectzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72723za7,
		BGl_z62cfunzf2Cinfozd2effectzd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_failzf2Cinfozd2typezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762failza7f2cinfoza72724za7,
		BGl_z62failzf2Cinfozd2typezd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_approxzd2dupzd2setz12zd2envzc0zzcfa_approxz00,
		BgL_bgl_za762approxza7d2dupza72725za7,
		BGl_z62approxzd2dupzd2setz12z70zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_scnstzf2Cinfozd2classzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762scnstza7f2cinfo2726z00,
		BGl_z62scnstzf2Cinfozd2classz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_kwotezf2Cinfozd2approxzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762kwoteza7f2cinfo2727z00,
		BGl_z62kwotezf2Cinfozd2approxz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzf2Cinfozd2approxzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72728za7,
		BGl_z62cfunzf2Cinfozd2approxz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_appzd2lyzf2Cinfozd2argzd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762appza7d2lyza7f2ci2729za7,
		BGl_z62appzd2lyzf2Cinfozd2argzd2setz12z50zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2polymorphiczf3zd2setz12zd2envz13zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2730z00,
		BGl_z62internzd2sfunzf2Cinfozd2polymorphiczf3zd2setz12za3zzcfa_approxz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Cinfozd2sidezd2effectzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22731za7,
		BGl_z62makezd2boxzf2Cinfozd2sidezd2effectz42zzcfa_approxz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_appzd2lyzf2Cinfozd2typezd2envz20zzcfa_approxz00,
		BgL_bgl_za762appza7d2lyza7f2ci2732za7,
		BGl_z62appzd2lyzf2Cinfozd2typez90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_forzd2eachzd2approxzd2alloczd2envz00zzcfa_approxz00,
		BgL_bgl_za762forza7d2eachza7d22733za7,
		BGl_z62forzd2eachzd2approxzd2alloczb0zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jumpzd2exzd2itzf2Cinfozd2exitzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762jumpza7d2exza7d2i2734za7,
		BGl_z62jumpzd2exzd2itzf2Cinfozd2exitzd2setz12z82zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2makezd2boxzd2valuezd2envz00zzcfa_approxz00,
		BgL_bgl_za762preza7d2makeza7d22735za7,
		BGl_z62prezd2makezd2boxzd2valuezb0zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Cinfozd2handlerzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762sexitza7f2cinfo2736z00,
		BGl_z62sexitzf2Cinfozd2handlerz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2argszd2namezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2737z00,
		BGl_z62internzd2sfunzf2Cinfozd2argszd2namez42zzcfa_approxz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2accesszd2setz12zd2envz12zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2738z00,
		BGl_z62reshapedzd2globalzd2accesszd2setz12za2zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genpatchidzf2Cinfozd2keyzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762genpatchidza7f22739z00,
		BGl_z62genpatchidzf2Cinfozd2keyzd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_unionzd2approxz12zd2envz12zzcfa_approxz00,
		BgL_bgl_za762unionza7d2appro2740z00,
		BGl_z62unionzd2approxz12za2zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2classzd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2741z00,
		BGl_z62internzd2sfunzf2Cinfozd2classzd2setz12z50zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2cfunzf2Cinfozd2envzf2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2cfunza7f2742za7,
		BGl_z62makezd2cfunzf2Cinfoz42zzcfa_approxz00, 0L, BUNSPEC, 15);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2clozd2envzd2nilzd2envz00zzcfa_approxz00,
		BgL_bgl_za762preza7d2cloza7d2e2743za7,
		BGl_z62prezd2clozd2envzd2nilzb0zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genpatchidzf2Cinfozd2indexzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762genpatchidza7f22744z00,
		BGl_z62genpatchidzf2Cinfozd2indexz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfunzf2Cinfozd2nilzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72745za7,
		BGl_z62cfunzf2Cinfozd2nilz42zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genpatchidzf2Cinfozd2approxzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762genpatchidza7f22746z00,
		BGl_z62genpatchidzf2Cinfozd2approxz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_failzf2Cinfozd2approxzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762failza7f2cinfoza72747za7,
		BGl_z62failzf2Cinfozd2approxz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_failzf2Cinfozd2loczd2envzf2zzcfa_approxz00,
		BgL_bgl_za762failza7f2cinfoza72748za7,
		BGl_z62failzf2Cinfozd2locz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2nilzd2envz20zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2749z00,
		BGl_z62internzd2sfunzf2Cinfozd2nilz90zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2removablezd2setz12zd2envz12zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2750z00,
		BGl_z62reshapedzd2globalzd2removablezd2setz12za2zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_kwotezf2nodezd2nilzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762kwoteza7f2nodeza72751za7,
		BGl_z62kwotezf2nodezd2nilz42zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2setz12zf2Cinfozd2nilzd2envz32zzcfa_approxz00,
		BgL_bgl_za762boxza7d2setza712za72752z00,
		BGl_z62boxzd2setz12zf2Cinfozd2nilz82zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2localzd2occurrencewzd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2lo2753z00,
		BGl_z62reshapedzd2localzd2occurrencewz62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2localzd2namezd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2lo2754z00,
		BGl_z62reshapedzd2localzd2namez62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2strengthzd2envz20zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2755z00,
		BGl_z62internzd2sfunzf2Cinfozd2strengthz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Cinfozd2approxzd2envz20zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22756za7,
		BGl_z62makezd2boxzf2Cinfozd2approxz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2emptyzd2approxzd2envzd2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2emptyza72757za7,
		BGl_z62makezd2emptyzd2approxz62zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genpatchidzf2Cinfozd2typezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762genpatchidza7f22758z00,
		BGl_z62genpatchidzf2Cinfozd2typez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2exzd2itzf2Cinfozd2approxzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762setza7d2exza7d2it2759za7,
		BGl_z62setzd2exzd2itzf2Cinfozd2approxz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_setqzf2Cinfozd2typezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762setqza7f2cinfoza72760za7,
		BGl_z62setqzf2Cinfozd2typez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_literalzf2Cinfozd2valuezd2envzf2zzcfa_approxz00,
		BgL_bgl_za762literalza7f2cin2761z00,
		BGl_z62literalzf2Cinfozd2valuez42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2localzd2bindingzd2valuezd2envz00zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2lo2762z00,
		BGl_z62reshapedzd2localzd2bindingzd2valuezb0zzcfa_approxz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_appzd2lyzf2Cinfozd2nilzd2envz20zzcfa_approxz00,
		BgL_bgl_za762appza7d2lyza7f2ci2763za7,
		BGl_z62appzd2lyzf2Cinfozd2nilz90zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2localzd2typezd2setz12zd2envz12zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2lo2764z00,
		BGl_z62reshapedzd2localzd2typezd2setz12za2zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2dssslzd2keywordszd2envzf2zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2765z00,
		BGl_z62internzd2sfunzf2Cinfozd2dssslzd2keywordsz42zzcfa_approxz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2occurrencewzd2envzd2zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2766z00,
		BGl_z62reshapedzd2globalzd2occurrencewz62zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2refzf2Ozd2Cinfozd2varzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762boxza7d2refza7f2o2767za7,
		BGl_z62boxzd2refzf2Ozd2Cinfozd2varz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2typezd2alloczd2approxzd2envz00zzcfa_approxz00,
		BgL_bgl_za762makeza7d2typeza7d2768za7,
		BGl_z62makezd2typezd2alloczd2approxzb0zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_switchzf2Cinfozf3zd2envzd3zzcfa_approxz00,
		BgL_bgl_za762switchza7f2cinf2769z00,
		BGl_z62switchzf2Cinfozf3z63zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_switchzf2Cinfozd2sidezd2effectzd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762switchza7f2cinf2770z00,
		BGl_z62switchzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_literalzf2Cinfozd2loczd2envzf2zzcfa_approxz00,
		BgL_bgl_za762literalza7f2cin2771z00,
		BGl_z62literalzf2Cinfozd2locz42zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_patchzf2Cinfozd2nilzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762patchza7f2cinfo2772z00,
		BGl_z62patchzf2Cinfozd2nilz42zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2polymorphiczf3zd2envzd3zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2773z00,
		BGl_z62externzd2sfunzf2Cinfozd2polymorphiczf3z63zzcfa_approxz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2setz12zf2Ozd2Cinfozd2approxzd2envze0zzcfa_approxz00,
		BgL_bgl_za762boxza7d2setza712za72774z00,
		BGl_z62boxzd2setz12zf2Ozd2Cinfozd2approxz50zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2argszd2noescapezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2775z00,
		BGl_z62internzd2sfunzf2Cinfozd2argszd2noescapezd2setz12z82zzcfa_approxz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2stackzd2allocatorzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2776z00,
		BGl_z62internzd2sfunzf2Cinfozd2stackzd2allocatorz42zzcfa_approxz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_approxzd2nilzd2envz00zzcfa_approxz00,
		BgL_bgl_za762approxza7d2nilza72777za7,
		BGl_z62approxzd2nilzb0zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_structzd2approxzf3zd2envzf3zzcfa_approxz00,
		BgL_bgl_za762structza7d2appr2778z00,
		BGl_z62structzd2approxzf3z43zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_kwotezf2Cinfozd2typezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762kwoteza7f2cinfo2779z00,
		BGl_z62kwotezf2Cinfozd2typezd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2reshapedzd2globalzd2envzd2zzcfa_approxz00,
		BgL_bgl_za762makeza7d2reshap2780z00,
		BGl_z62makezd2reshapedzd2globalz62zzcfa_approxz00, 0L, BUNSPEC, 20);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_nodezd2keyzd2envz00zzcfa_approxz00,
		BgL_bgl_za762nodeza7d2keyza7b02781za7, BGl_z62nodezd2keyzb0zzcfa_approxz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2approxzd2envz20zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2782z00,
		BGl_z62internzd2sfunzf2Cinfozd2approxz90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_internzd2sfunzf2Cinfozd2stackzd2allocatorzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762internza7d2sfun2783z00,
		BGl_z62internzd2sfunzf2Cinfozd2stackzd2allocatorzd2setz12z82zzcfa_approxz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzf2Cinfozd2topzf3zd2setz12zd2envzc1zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72784za7,
		BGl_z62cfunzf2Cinfozd2topzf3zd2setz12z71zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Cinfozd2stackablezd2envz20zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22785za7,
		BGl_z62makezd2boxzf2Cinfozd2stackablez90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_boxzd2setz12zf2Ozd2Cinfozd2varzd2setz12zd2envz20zzcfa_approxz00,
		BgL_bgl_za762boxza7d2setza712za72786z00,
		BGl_z62boxzd2setz12zf2Ozd2Cinfozd2varzd2setz12z90zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2clozd2envzd2loczd2setz12zd2envzc0zzcfa_approxz00,
		BgL_bgl_za762preza7d2cloza7d2e2787za7,
		BGl_z62prezd2clozd2envzd2loczd2setz12z70zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Ozd2Cinfozd2approxzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22788za7,
		BGl_z62makezd2boxzf2Ozd2Cinfozd2approxzd2setz12z82zzcfa_approxz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Cinfozd2handlerzd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762sexitza7f2cinfo2789z00,
		BGl_z62sexitzf2Cinfozd2handlerzd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_kwotezf2Cinfozd2nilzd2envzf2zzcfa_approxz00,
		BgL_bgl_za762kwoteza7f2cinfo2790z00,
		BGl_z62kwotezf2Cinfozd2nilz42zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_reshapedzd2globalzd2fastzd2alphazd2envz00zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2gl2791z00,
		BGl_z62reshapedzd2globalzd2fastzd2alphazb0zzcfa_approxz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_svarzf2Cinfozf3zd2envzd3zzcfa_approxz00,
		BgL_bgl_za762svarza7f2cinfoza72792za7,
		BGl_z62svarzf2Cinfozf3z63zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2boxzf2Cinfozd2typezd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762makeza7d2boxza7f22793za7,
		BGl_z62makezd2boxzf2Cinfozd2typezd2setz12z50zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzf2Cinfozd2thezd2closurezd2envz20zzcfa_approxz00,
		BgL_bgl_za762cfunza7f2cinfoza72794za7,
		BGl_z62cfunzf2Cinfozd2thezd2closurez90zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_externzd2sfunzf2Cinfozd2nilzd2envz20zzcfa_approxz00,
		BgL_bgl_za762externza7d2sfun2795z00,
		BGl_z62externzd2sfunzf2Cinfozd2nilz90zzcfa_approxz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_appzd2lyzf2Cinfozf3zd2envz01zzcfa_approxz00,
		BgL_bgl_za762appza7d2lyza7f2ci2796za7,
		BGl_z62appzd2lyzf2Cinfozf3zb1zzcfa_approxz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_patchzf2Cinfozd2valuezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762patchza7f2cinfo2797z00,
		BGl_z62patchzf2Cinfozd2valuezd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_literalzf2Cinfozd2typezd2setz12zd2envz32zzcfa_approxz00,
		BgL_bgl_za762literalza7f2cin2798z00,
		BGl_z62literalzf2Cinfozd2typezd2setz12z82zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_switchzf2Cinfozd2itemzd2typezd2setz12zd2envze0zzcfa_approxz00,
		BgL_bgl_za762switchza7f2cinf2799z00,
		BGl_z62switchzf2Cinfozd2itemzd2typezd2setz12z50zzcfa_approxz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_approxzd2lostzd2stampzd2setz12zd2envz12zzcfa_approxz00,
		BgL_bgl_za762approxza7d2lost2800z00,
		BGl_z62approxzd2lostzd2stampzd2setz12za2zzcfa_approxz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_reshapedzd2localzf3zd2envzf3zzcfa_approxz00,
		BgL_bgl_za762reshapedza7d2lo2801z00,
		BGl_z62reshapedzd2localzf3z43zzcfa_approxz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcfa_approxz00));
		     ADD_ROOT((void *) (&BGl_za2alloczd2setza2zd2zzcfa_approxz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcfa_approxz00(long
		BgL_checksumz00_9449, char *BgL_fromz00_9450)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcfa_approxz00))
				{
					BGl_requirezd2initializa7ationz75zzcfa_approxz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcfa_approxz00();
					BGl_libraryzd2moduleszd2initz00zzcfa_approxz00();
					BGl_cnstzd2initzd2zzcfa_approxz00();
					BGl_importedzd2moduleszd2initz00zzcfa_approxz00();
					BGl_genericzd2initzd2zzcfa_approxz00();
					BGl_methodzd2initzd2zzcfa_approxz00();
					return BGl_toplevelzd2initzd2zzcfa_approxz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcfa_approxz00(void)
	{
		{	/* Cfa/approx.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cfa_approx");
			BGl_modulezd2initializa7ationz75zz__evalz00(0L, "cfa_approx");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "cfa_approx");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cfa_approx");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cfa_approx");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "cfa_approx");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "cfa_approx");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L,
				"cfa_approx");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cfa_approx");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cfa_approx");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"cfa_approx");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cfa_approx");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "cfa_approx");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "cfa_approx");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcfa_approxz00(void)
	{
		{	/* Cfa/approx.scm 15 */
			{	/* Cfa/approx.scm 15 */
				obj_t BgL_cportz00_8646;

				{	/* Cfa/approx.scm 15 */
					obj_t BgL_stringz00_8653;

					BgL_stringz00_8653 = BGl_string2240z00zzcfa_approxz00;
					{	/* Cfa/approx.scm 15 */
						obj_t BgL_startz00_8654;

						BgL_startz00_8654 = BINT(0L);
						{	/* Cfa/approx.scm 15 */
							obj_t BgL_endz00_8655;

							BgL_endz00_8655 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_8653)));
							{	/* Cfa/approx.scm 15 */

								BgL_cportz00_8646 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_8653, BgL_startz00_8654, BgL_endz00_8655);
				}}}}
				{
					long BgL_iz00_8647;

					BgL_iz00_8647 = 6L;
				BgL_loopz00_8648:
					if ((BgL_iz00_8647 == -1L))
						{	/* Cfa/approx.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Cfa/approx.scm 15 */
							{	/* Cfa/approx.scm 15 */
								obj_t BgL_arg2241z00_8649;

								{	/* Cfa/approx.scm 15 */

									{	/* Cfa/approx.scm 15 */
										obj_t BgL_locationz00_8651;

										BgL_locationz00_8651 = BBOOL(((bool_t) 0));
										{	/* Cfa/approx.scm 15 */

											BgL_arg2241z00_8649 =
												BGl_readz00zz__readerz00(BgL_cportz00_8646,
												BgL_locationz00_8651);
										}
									}
								}
								{	/* Cfa/approx.scm 15 */
									int BgL_tmpz00_9484;

									BgL_tmpz00_9484 = (int) (BgL_iz00_8647);
									CNST_TABLE_SET(BgL_tmpz00_9484, BgL_arg2241z00_8649);
							}}
							{	/* Cfa/approx.scm 15 */
								int BgL_auxz00_8652;

								BgL_auxz00_8652 = (int) ((BgL_iz00_8647 - 1L));
								{
									long BgL_iz00_9489;

									BgL_iz00_9489 = (long) (BgL_auxz00_8652);
									BgL_iz00_8647 = BgL_iz00_9489;
									goto BgL_loopz00_8648;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcfa_approxz00(void)
	{
		{	/* Cfa/approx.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcfa_approxz00(void)
	{
		{	/* Cfa/approx.scm 15 */
			BGl_za2alloczd2setza2zd2zzcfa_approxz00 = BUNSPEC;
			return BUNSPEC;
		}

	}



/* make-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_makezd2approxzd2zzcfa_approxz00(BgL_typez00_bglt BgL_type1821z00_23,
		bool_t BgL_typezd2lockedzf31822z21_24, obj_t BgL_allocs1823z00_25,
		bool_t BgL_topzf31824zf3_26, long BgL_lostzd2stamp1825zd2_27,
		obj_t BgL_dup1826z00_28, bool_t BgL_haszd2procedurezf31827z21_29)
	{
		{	/* Cfa/cinfo.sch 661 */
			{	/* Cfa/cinfo.sch 661 */
				BgL_approxz00_bglt BgL_new1176z00_8657;

				{	/* Cfa/cinfo.sch 661 */
					BgL_approxz00_bglt BgL_new1175z00_8658;

					BgL_new1175z00_8658 =
						((BgL_approxz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_approxz00_bgl))));
					{	/* Cfa/cinfo.sch 661 */
						long BgL_arg1626z00_8659;

						BgL_arg1626z00_8659 = BGL_CLASS_NUM(BGl_approxz00zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1175z00_8658), BgL_arg1626z00_8659);
					}
					BgL_new1176z00_8657 = BgL_new1175z00_8658;
				}
				((((BgL_approxz00_bglt) COBJECT(BgL_new1176z00_8657))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1821z00_23), BUNSPEC);
				((((BgL_approxz00_bglt) COBJECT(BgL_new1176z00_8657))->
						BgL_typezd2lockedzf3z21) =
					((bool_t) BgL_typezd2lockedzf31822z21_24), BUNSPEC);
				((((BgL_approxz00_bglt) COBJECT(BgL_new1176z00_8657))->BgL_allocsz00) =
					((obj_t) BgL_allocs1823z00_25), BUNSPEC);
				((((BgL_approxz00_bglt) COBJECT(BgL_new1176z00_8657))->BgL_topzf3zf3) =
					((bool_t) BgL_topzf31824zf3_26), BUNSPEC);
				((((BgL_approxz00_bglt) COBJECT(BgL_new1176z00_8657))->
						BgL_lostzd2stampzd2) =
					((long) BgL_lostzd2stamp1825zd2_27), BUNSPEC);
				((((BgL_approxz00_bglt) COBJECT(BgL_new1176z00_8657))->BgL_dupz00) =
					((obj_t) BgL_dup1826z00_28), BUNSPEC);
				((((BgL_approxz00_bglt) COBJECT(BgL_new1176z00_8657))->
						BgL_haszd2procedurezf3z21) =
					((bool_t) BgL_haszd2procedurezf31827z21_29), BUNSPEC);
				return BgL_new1176z00_8657;
			}
		}

	}



/* &make-approx */
	BgL_approxz00_bglt BGl_z62makezd2approxzb0zzcfa_approxz00(obj_t
		BgL_envz00_7157, obj_t BgL_type1821z00_7158,
		obj_t BgL_typezd2lockedzf31822z21_7159, obj_t BgL_allocs1823z00_7160,
		obj_t BgL_topzf31824zf3_7161, obj_t BgL_lostzd2stamp1825zd2_7162,
		obj_t BgL_dup1826z00_7163, obj_t BgL_haszd2procedurezf31827z21_7164)
	{
		{	/* Cfa/cinfo.sch 661 */
			return
				BGl_makezd2approxzd2zzcfa_approxz00(
				((BgL_typez00_bglt) BgL_type1821z00_7158),
				CBOOL(BgL_typezd2lockedzf31822z21_7159), BgL_allocs1823z00_7160,
				CBOOL(BgL_topzf31824zf3_7161),
				(long) CINT(BgL_lostzd2stamp1825zd2_7162), BgL_dup1826z00_7163,
				CBOOL(BgL_haszd2procedurezf31827z21_7164));
		}

	}



/* approx? */
	BGL_EXPORTED_DEF bool_t BGl_approxzf3zf3zzcfa_approxz00(obj_t BgL_objz00_30)
	{
		{	/* Cfa/cinfo.sch 662 */
			{	/* Cfa/cinfo.sch 662 */
				obj_t BgL_classz00_8660;

				BgL_classz00_8660 = BGl_approxz00zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_30))
					{	/* Cfa/cinfo.sch 662 */
						BgL_objectz00_bglt BgL_arg1807z00_8661;

						BgL_arg1807z00_8661 = (BgL_objectz00_bglt) (BgL_objz00_30);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 662 */
								long BgL_idxz00_8662;

								BgL_idxz00_8662 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8661);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8662 + 1L)) == BgL_classz00_8660);
							}
						else
							{	/* Cfa/cinfo.sch 662 */
								bool_t BgL_res2167z00_8665;

								{	/* Cfa/cinfo.sch 662 */
									obj_t BgL_oclassz00_8666;

									{	/* Cfa/cinfo.sch 662 */
										obj_t BgL_arg1815z00_8667;
										long BgL_arg1816z00_8668;

										BgL_arg1815z00_8667 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 662 */
											long BgL_arg1817z00_8669;

											BgL_arg1817z00_8669 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8661);
											BgL_arg1816z00_8668 = (BgL_arg1817z00_8669 - OBJECT_TYPE);
										}
										BgL_oclassz00_8666 =
											VECTOR_REF(BgL_arg1815z00_8667, BgL_arg1816z00_8668);
									}
									{	/* Cfa/cinfo.sch 662 */
										bool_t BgL__ortest_1115z00_8670;

										BgL__ortest_1115z00_8670 =
											(BgL_classz00_8660 == BgL_oclassz00_8666);
										if (BgL__ortest_1115z00_8670)
											{	/* Cfa/cinfo.sch 662 */
												BgL_res2167z00_8665 = BgL__ortest_1115z00_8670;
											}
										else
											{	/* Cfa/cinfo.sch 662 */
												long BgL_odepthz00_8671;

												{	/* Cfa/cinfo.sch 662 */
													obj_t BgL_arg1804z00_8672;

													BgL_arg1804z00_8672 = (BgL_oclassz00_8666);
													BgL_odepthz00_8671 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8672);
												}
												if ((1L < BgL_odepthz00_8671))
													{	/* Cfa/cinfo.sch 662 */
														obj_t BgL_arg1802z00_8673;

														{	/* Cfa/cinfo.sch 662 */
															obj_t BgL_arg1803z00_8674;

															BgL_arg1803z00_8674 = (BgL_oclassz00_8666);
															BgL_arg1802z00_8673 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8674,
																1L);
														}
														BgL_res2167z00_8665 =
															(BgL_arg1802z00_8673 == BgL_classz00_8660);
													}
												else
													{	/* Cfa/cinfo.sch 662 */
														BgL_res2167z00_8665 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2167z00_8665;
							}
					}
				else
					{	/* Cfa/cinfo.sch 662 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &approx? */
	obj_t BGl_z62approxzf3z91zzcfa_approxz00(obj_t BgL_envz00_7165,
		obj_t BgL_objz00_7166)
	{
		{	/* Cfa/cinfo.sch 662 */
			return BBOOL(BGl_approxzf3zf3zzcfa_approxz00(BgL_objz00_7166));
		}

	}



/* approx-nil */
	BGL_EXPORTED_DEF BgL_approxz00_bglt BGl_approxzd2nilzd2zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 663 */
			{	/* Cfa/cinfo.sch 663 */
				obj_t BgL_classz00_4741;

				BgL_classz00_4741 = BGl_approxz00zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 663 */
					obj_t BgL__ortest_1117z00_4742;

					BgL__ortest_1117z00_4742 = BGL_CLASS_NIL(BgL_classz00_4741);
					if (CBOOL(BgL__ortest_1117z00_4742))
						{	/* Cfa/cinfo.sch 663 */
							return ((BgL_approxz00_bglt) BgL__ortest_1117z00_4742);
						}
					else
						{	/* Cfa/cinfo.sch 663 */
							return
								((BgL_approxz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4741));
						}
				}
			}
		}

	}



/* &approx-nil */
	BgL_approxz00_bglt BGl_z62approxzd2nilzb0zzcfa_approxz00(obj_t
		BgL_envz00_7167)
	{
		{	/* Cfa/cinfo.sch 663 */
			return BGl_approxzd2nilzd2zzcfa_approxz00();
		}

	}



/* approx-has-procedure? */
	BGL_EXPORTED_DEF bool_t
		BGl_approxzd2haszd2procedurezf3zf3zzcfa_approxz00(BgL_approxz00_bglt
		BgL_oz00_31)
	{
		{	/* Cfa/cinfo.sch 664 */
			return
				(((BgL_approxz00_bglt) COBJECT(BgL_oz00_31))->
				BgL_haszd2procedurezf3z21);
		}

	}



/* &approx-has-procedure? */
	obj_t BGl_z62approxzd2haszd2procedurezf3z91zzcfa_approxz00(obj_t
		BgL_envz00_7168, obj_t BgL_oz00_7169)
	{
		{	/* Cfa/cinfo.sch 664 */
			return
				BBOOL(BGl_approxzd2haszd2procedurezf3zf3zzcfa_approxz00(
					((BgL_approxz00_bglt) BgL_oz00_7169)));
		}

	}



/* approx-has-procedure?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_approxzd2haszd2procedurezf3zd2setz12z33zzcfa_approxz00
		(BgL_approxz00_bglt BgL_oz00_32, bool_t BgL_vz00_33)
	{
		{	/* Cfa/cinfo.sch 665 */
			return
				((((BgL_approxz00_bglt) COBJECT(BgL_oz00_32))->
					BgL_haszd2procedurezf3z21) = ((bool_t) BgL_vz00_33), BUNSPEC);
		}

	}



/* &approx-has-procedure?-set! */
	obj_t BGl_z62approxzd2haszd2procedurezf3zd2setz12z51zzcfa_approxz00(obj_t
		BgL_envz00_7170, obj_t BgL_oz00_7171, obj_t BgL_vz00_7172)
	{
		{	/* Cfa/cinfo.sch 665 */
			return
				BGl_approxzd2haszd2procedurezf3zd2setz12z33zzcfa_approxz00(
				((BgL_approxz00_bglt) BgL_oz00_7171), CBOOL(BgL_vz00_7172));
		}

	}



/* approx-dup */
	BGL_EXPORTED_DEF obj_t BGl_approxzd2dupzd2zzcfa_approxz00(BgL_approxz00_bglt
		BgL_oz00_34)
	{
		{	/* Cfa/cinfo.sch 666 */
			return (((BgL_approxz00_bglt) COBJECT(BgL_oz00_34))->BgL_dupz00);
		}

	}



/* &approx-dup */
	obj_t BGl_z62approxzd2dupzb0zzcfa_approxz00(obj_t BgL_envz00_7173,
		obj_t BgL_oz00_7174)
	{
		{	/* Cfa/cinfo.sch 666 */
			return
				BGl_approxzd2dupzd2zzcfa_approxz00(
				((BgL_approxz00_bglt) BgL_oz00_7174));
		}

	}



/* approx-dup-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_approxzd2dupzd2setz12z12zzcfa_approxz00(BgL_approxz00_bglt BgL_oz00_35,
		obj_t BgL_vz00_36)
	{
		{	/* Cfa/cinfo.sch 667 */
			return
				((((BgL_approxz00_bglt) COBJECT(BgL_oz00_35))->BgL_dupz00) =
				((obj_t) BgL_vz00_36), BUNSPEC);
		}

	}



/* &approx-dup-set! */
	obj_t BGl_z62approxzd2dupzd2setz12z70zzcfa_approxz00(obj_t BgL_envz00_7175,
		obj_t BgL_oz00_7176, obj_t BgL_vz00_7177)
	{
		{	/* Cfa/cinfo.sch 667 */
			return
				BGl_approxzd2dupzd2setz12z12zzcfa_approxz00(
				((BgL_approxz00_bglt) BgL_oz00_7176), BgL_vz00_7177);
		}

	}



/* approx-lost-stamp */
	BGL_EXPORTED_DEF long
		BGl_approxzd2lostzd2stampz00zzcfa_approxz00(BgL_approxz00_bglt BgL_oz00_37)
	{
		{	/* Cfa/cinfo.sch 668 */
			return (((BgL_approxz00_bglt) COBJECT(BgL_oz00_37))->BgL_lostzd2stampzd2);
		}

	}



/* &approx-lost-stamp */
	obj_t BGl_z62approxzd2lostzd2stampz62zzcfa_approxz00(obj_t BgL_envz00_7178,
		obj_t BgL_oz00_7179)
	{
		{	/* Cfa/cinfo.sch 668 */
			return
				BINT(BGl_approxzd2lostzd2stampz00zzcfa_approxz00(
					((BgL_approxz00_bglt) BgL_oz00_7179)));
		}

	}



/* approx-lost-stamp-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_approxzd2lostzd2stampzd2setz12zc0zzcfa_approxz00(BgL_approxz00_bglt
		BgL_oz00_38, long BgL_vz00_39)
	{
		{	/* Cfa/cinfo.sch 669 */
			return
				((((BgL_approxz00_bglt) COBJECT(BgL_oz00_38))->BgL_lostzd2stampzd2) =
				((long) BgL_vz00_39), BUNSPEC);
		}

	}



/* &approx-lost-stamp-set! */
	obj_t BGl_z62approxzd2lostzd2stampzd2setz12za2zzcfa_approxz00(obj_t
		BgL_envz00_7180, obj_t BgL_oz00_7181, obj_t BgL_vz00_7182)
	{
		{	/* Cfa/cinfo.sch 669 */
			return
				BGl_approxzd2lostzd2stampzd2setz12zc0zzcfa_approxz00(
				((BgL_approxz00_bglt) BgL_oz00_7181), (long) CINT(BgL_vz00_7182));
		}

	}



/* approx-top? */
	BGL_EXPORTED_DEF bool_t
		BGl_approxzd2topzf3z21zzcfa_approxz00(BgL_approxz00_bglt BgL_oz00_40)
	{
		{	/* Cfa/cinfo.sch 670 */
			return (((BgL_approxz00_bglt) COBJECT(BgL_oz00_40))->BgL_topzf3zf3);
		}

	}



/* &approx-top? */
	obj_t BGl_z62approxzd2topzf3z43zzcfa_approxz00(obj_t BgL_envz00_7183,
		obj_t BgL_oz00_7184)
	{
		{	/* Cfa/cinfo.sch 670 */
			return
				BBOOL(BGl_approxzd2topzf3z21zzcfa_approxz00(
					((BgL_approxz00_bglt) BgL_oz00_7184)));
		}

	}



/* approx-top?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_approxzd2topzf3zd2setz12ze1zzcfa_approxz00(BgL_approxz00_bglt
		BgL_oz00_41, bool_t BgL_vz00_42)
	{
		{	/* Cfa/cinfo.sch 671 */
			return
				((((BgL_approxz00_bglt) COBJECT(BgL_oz00_41))->BgL_topzf3zf3) =
				((bool_t) BgL_vz00_42), BUNSPEC);
		}

	}



/* &approx-top?-set! */
	obj_t BGl_z62approxzd2topzf3zd2setz12z83zzcfa_approxz00(obj_t BgL_envz00_7185,
		obj_t BgL_oz00_7186, obj_t BgL_vz00_7187)
	{
		{	/* Cfa/cinfo.sch 671 */
			return
				BGl_approxzd2topzf3zd2setz12ze1zzcfa_approxz00(
				((BgL_approxz00_bglt) BgL_oz00_7186), CBOOL(BgL_vz00_7187));
		}

	}



/* approx-allocs */
	BGL_EXPORTED_DEF obj_t
		BGl_approxzd2allocszd2zzcfa_approxz00(BgL_approxz00_bglt BgL_oz00_43)
	{
		{	/* Cfa/cinfo.sch 672 */
			return (((BgL_approxz00_bglt) COBJECT(BgL_oz00_43))->BgL_allocsz00);
		}

	}



/* &approx-allocs */
	obj_t BGl_z62approxzd2allocszb0zzcfa_approxz00(obj_t BgL_envz00_7188,
		obj_t BgL_oz00_7189)
	{
		{	/* Cfa/cinfo.sch 672 */
			return
				BGl_approxzd2allocszd2zzcfa_approxz00(
				((BgL_approxz00_bglt) BgL_oz00_7189));
		}

	}



/* approx-type-locked? */
	BGL_EXPORTED_DEF bool_t
		BGl_approxzd2typezd2lockedzf3zf3zzcfa_approxz00(BgL_approxz00_bglt
		BgL_oz00_46)
	{
		{	/* Cfa/cinfo.sch 674 */
			return
				(((BgL_approxz00_bglt) COBJECT(BgL_oz00_46))->BgL_typezd2lockedzf3z21);
		}

	}



/* &approx-type-locked? */
	obj_t BGl_z62approxzd2typezd2lockedzf3z91zzcfa_approxz00(obj_t
		BgL_envz00_7190, obj_t BgL_oz00_7191)
	{
		{	/* Cfa/cinfo.sch 674 */
			return
				BBOOL(BGl_approxzd2typezd2lockedzf3zf3zzcfa_approxz00(
					((BgL_approxz00_bglt) BgL_oz00_7191)));
		}

	}



/* approx-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_approxzd2typezd2zzcfa_approxz00(BgL_approxz00_bglt BgL_oz00_49)
	{
		{	/* Cfa/cinfo.sch 676 */
			return (((BgL_approxz00_bglt) COBJECT(BgL_oz00_49))->BgL_typez00);
		}

	}



/* &approx-type */
	BgL_typez00_bglt BGl_z62approxzd2typezb0zzcfa_approxz00(obj_t BgL_envz00_7192,
		obj_t BgL_oz00_7193)
	{
		{	/* Cfa/cinfo.sch 676 */
			return
				BGl_approxzd2typezd2zzcfa_approxz00(
				((BgL_approxz00_bglt) BgL_oz00_7193));
		}

	}



/* approx-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_approxzd2typezd2setz12z12zzcfa_approxz00(BgL_approxz00_bglt BgL_oz00_50,
		BgL_typez00_bglt BgL_vz00_51)
	{
		{	/* Cfa/cinfo.sch 677 */
			return
				((((BgL_approxz00_bglt) COBJECT(BgL_oz00_50))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_51), BUNSPEC);
		}

	}



/* &approx-type-set! */
	obj_t BGl_z62approxzd2typezd2setz12z70zzcfa_approxz00(obj_t BgL_envz00_7194,
		obj_t BgL_oz00_7195, obj_t BgL_vz00_7196)
	{
		{	/* Cfa/cinfo.sch 677 */
			return
				BGl_approxzd2typezd2setz12z12zzcfa_approxz00(
				((BgL_approxz00_bglt) BgL_oz00_7195),
				((BgL_typez00_bglt) BgL_vz00_7196));
		}

	}



/* make-cfun/Cinfo */
	BGL_EXPORTED_DEF BgL_cfunz00_bglt
		BGl_makezd2cfunzf2Cinfoz20zzcfa_approxz00(long BgL_arity1805z00_52,
		obj_t BgL_sidezd2effect1806zd2_53, obj_t BgL_predicatezd2of1807zd2_54,
		obj_t BgL_stackzd2allocator1808zd2_55, bool_t BgL_topzf31809zf3_56,
		obj_t BgL_thezd2closure1810zd2_57, obj_t BgL_effect1811z00_58,
		obj_t BgL_failsafe1812z00_59, obj_t BgL_argszd2noescape1813zd2_60,
		obj_t BgL_argszd2retescape1814zd2_61, obj_t BgL_argszd2type1815zd2_62,
		bool_t BgL_macrozf31816zf3_63, bool_t BgL_infixzf31817zf3_64,
		obj_t BgL_method1818z00_65, BgL_approxz00_bglt BgL_approx1819z00_66)
	{
		{	/* Cfa/cinfo.sch 680 */
			{	/* Cfa/cinfo.sch 680 */
				BgL_cfunz00_bglt BgL_new1180z00_8675;

				{	/* Cfa/cinfo.sch 680 */
					BgL_cfunz00_bglt BgL_tmp1178z00_8676;
					BgL_cfunzf2cinfozf2_bglt BgL_wide1179z00_8677;

					{
						BgL_cfunz00_bglt BgL_auxz00_9584;

						{	/* Cfa/cinfo.sch 680 */
							BgL_cfunz00_bglt BgL_new1177z00_8678;

							BgL_new1177z00_8678 =
								((BgL_cfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_cfunz00_bgl))));
							{	/* Cfa/cinfo.sch 680 */
								long BgL_arg1629z00_8679;

								BgL_arg1629z00_8679 = BGL_CLASS_NUM(BGl_cfunz00zzast_varz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1177z00_8678),
									BgL_arg1629z00_8679);
							}
							{	/* Cfa/cinfo.sch 680 */
								BgL_objectz00_bglt BgL_tmpz00_9589;

								BgL_tmpz00_9589 = ((BgL_objectz00_bglt) BgL_new1177z00_8678);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9589, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1177z00_8678);
							BgL_auxz00_9584 = BgL_new1177z00_8678;
						}
						BgL_tmp1178z00_8676 = ((BgL_cfunz00_bglt) BgL_auxz00_9584);
					}
					BgL_wide1179z00_8677 =
						((BgL_cfunzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cfunzf2cinfozf2_bgl))));
					{	/* Cfa/cinfo.sch 680 */
						obj_t BgL_auxz00_9597;
						BgL_objectz00_bglt BgL_tmpz00_9595;

						BgL_auxz00_9597 = ((obj_t) BgL_wide1179z00_8677);
						BgL_tmpz00_9595 = ((BgL_objectz00_bglt) BgL_tmp1178z00_8676);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9595, BgL_auxz00_9597);
					}
					((BgL_objectz00_bglt) BgL_tmp1178z00_8676);
					{	/* Cfa/cinfo.sch 680 */
						long BgL_arg1627z00_8680;

						BgL_arg1627z00_8680 =
							BGL_CLASS_NUM(BGl_cfunzf2Cinfozf2zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1178z00_8676), BgL_arg1627z00_8680);
					}
					BgL_new1180z00_8675 = ((BgL_cfunz00_bglt) BgL_tmp1178z00_8676);
				}
				((((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt) BgL_new1180z00_8675)))->BgL_arityz00) =
					((long) BgL_arity1805z00_52), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1180z00_8675)))->
						BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1806zd2_53), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1180z00_8675)))->
						BgL_predicatezd2ofzd2) =
					((obj_t) BgL_predicatezd2of1807zd2_54), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1180z00_8675)))->
						BgL_stackzd2allocatorzd2) =
					((obj_t) BgL_stackzd2allocator1808zd2_55), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1180z00_8675)))->
						BgL_topzf3zf3) = ((bool_t) BgL_topzf31809zf3_56), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1180z00_8675)))->
						BgL_thezd2closurezd2) =
					((obj_t) BgL_thezd2closure1810zd2_57), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1180z00_8675)))->
						BgL_effectz00) = ((obj_t) BgL_effect1811z00_58), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1180z00_8675)))->
						BgL_failsafez00) = ((obj_t) BgL_failsafe1812z00_59), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1180z00_8675)))->
						BgL_argszd2noescapezd2) =
					((obj_t) BgL_argszd2noescape1813zd2_60), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1180z00_8675)))->
						BgL_argszd2retescapezd2) =
					((obj_t) BgL_argszd2retescape1814zd2_61), BUNSPEC);
				((((BgL_cfunz00_bglt) COBJECT(((BgL_cfunz00_bglt)
									BgL_new1180z00_8675)))->BgL_argszd2typezd2) =
					((obj_t) BgL_argszd2type1815zd2_62), BUNSPEC);
				((((BgL_cfunz00_bglt) COBJECT(((BgL_cfunz00_bglt)
									BgL_new1180z00_8675)))->BgL_macrozf3zf3) =
					((bool_t) BgL_macrozf31816zf3_63), BUNSPEC);
				((((BgL_cfunz00_bglt) COBJECT(((BgL_cfunz00_bglt)
									BgL_new1180z00_8675)))->BgL_infixzf3zf3) =
					((bool_t) BgL_infixzf31817zf3_64), BUNSPEC);
				((((BgL_cfunz00_bglt) COBJECT(((BgL_cfunz00_bglt)
									BgL_new1180z00_8675)))->BgL_methodz00) =
					((obj_t) BgL_method1818z00_65), BUNSPEC);
				{
					BgL_cfunzf2cinfozf2_bglt BgL_auxz00_9633;

					{
						obj_t BgL_auxz00_9634;

						{	/* Cfa/cinfo.sch 680 */
							BgL_objectz00_bglt BgL_tmpz00_9635;

							BgL_tmpz00_9635 = ((BgL_objectz00_bglt) BgL_new1180z00_8675);
							BgL_auxz00_9634 = BGL_OBJECT_WIDENING(BgL_tmpz00_9635);
						}
						BgL_auxz00_9633 = ((BgL_cfunzf2cinfozf2_bglt) BgL_auxz00_9634);
					}
					((((BgL_cfunzf2cinfozf2_bglt) COBJECT(BgL_auxz00_9633))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1819z00_66), BUNSPEC);
				}
				return BgL_new1180z00_8675;
			}
		}

	}



/* &make-cfun/Cinfo */
	BgL_cfunz00_bglt BGl_z62makezd2cfunzf2Cinfoz42zzcfa_approxz00(obj_t
		BgL_envz00_7197, obj_t BgL_arity1805z00_7198,
		obj_t BgL_sidezd2effect1806zd2_7199, obj_t BgL_predicatezd2of1807zd2_7200,
		obj_t BgL_stackzd2allocator1808zd2_7201, obj_t BgL_topzf31809zf3_7202,
		obj_t BgL_thezd2closure1810zd2_7203, obj_t BgL_effect1811z00_7204,
		obj_t BgL_failsafe1812z00_7205, obj_t BgL_argszd2noescape1813zd2_7206,
		obj_t BgL_argszd2retescape1814zd2_7207, obj_t BgL_argszd2type1815zd2_7208,
		obj_t BgL_macrozf31816zf3_7209, obj_t BgL_infixzf31817zf3_7210,
		obj_t BgL_method1818z00_7211, obj_t BgL_approx1819z00_7212)
	{
		{	/* Cfa/cinfo.sch 680 */
			return
				BGl_makezd2cfunzf2Cinfoz20zzcfa_approxz00(
				(long) CINT(BgL_arity1805z00_7198), BgL_sidezd2effect1806zd2_7199,
				BgL_predicatezd2of1807zd2_7200, BgL_stackzd2allocator1808zd2_7201,
				CBOOL(BgL_topzf31809zf3_7202), BgL_thezd2closure1810zd2_7203,
				BgL_effect1811z00_7204, BgL_failsafe1812z00_7205,
				BgL_argszd2noescape1813zd2_7206, BgL_argszd2retescape1814zd2_7207,
				BgL_argszd2type1815zd2_7208, CBOOL(BgL_macrozf31816zf3_7209),
				CBOOL(BgL_infixzf31817zf3_7210), BgL_method1818z00_7211,
				((BgL_approxz00_bglt) BgL_approx1819z00_7212));
		}

	}



/* cfun/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_cfunzf2Cinfozf3z01zzcfa_approxz00(obj_t
		BgL_objz00_67)
	{
		{	/* Cfa/cinfo.sch 681 */
			{	/* Cfa/cinfo.sch 681 */
				obj_t BgL_classz00_8681;

				BgL_classz00_8681 = BGl_cfunzf2Cinfozf2zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_67))
					{	/* Cfa/cinfo.sch 681 */
						BgL_objectz00_bglt BgL_arg1807z00_8682;

						BgL_arg1807z00_8682 = (BgL_objectz00_bglt) (BgL_objz00_67);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 681 */
								long BgL_idxz00_8683;

								BgL_idxz00_8683 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8682);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8683 + 4L)) == BgL_classz00_8681);
							}
						else
							{	/* Cfa/cinfo.sch 681 */
								bool_t BgL_res2168z00_8686;

								{	/* Cfa/cinfo.sch 681 */
									obj_t BgL_oclassz00_8687;

									{	/* Cfa/cinfo.sch 681 */
										obj_t BgL_arg1815z00_8688;
										long BgL_arg1816z00_8689;

										BgL_arg1815z00_8688 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 681 */
											long BgL_arg1817z00_8690;

											BgL_arg1817z00_8690 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8682);
											BgL_arg1816z00_8689 = (BgL_arg1817z00_8690 - OBJECT_TYPE);
										}
										BgL_oclassz00_8687 =
											VECTOR_REF(BgL_arg1815z00_8688, BgL_arg1816z00_8689);
									}
									{	/* Cfa/cinfo.sch 681 */
										bool_t BgL__ortest_1115z00_8691;

										BgL__ortest_1115z00_8691 =
											(BgL_classz00_8681 == BgL_oclassz00_8687);
										if (BgL__ortest_1115z00_8691)
											{	/* Cfa/cinfo.sch 681 */
												BgL_res2168z00_8686 = BgL__ortest_1115z00_8691;
											}
										else
											{	/* Cfa/cinfo.sch 681 */
												long BgL_odepthz00_8692;

												{	/* Cfa/cinfo.sch 681 */
													obj_t BgL_arg1804z00_8693;

													BgL_arg1804z00_8693 = (BgL_oclassz00_8687);
													BgL_odepthz00_8692 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8693);
												}
												if ((4L < BgL_odepthz00_8692))
													{	/* Cfa/cinfo.sch 681 */
														obj_t BgL_arg1802z00_8694;

														{	/* Cfa/cinfo.sch 681 */
															obj_t BgL_arg1803z00_8695;

															BgL_arg1803z00_8695 = (BgL_oclassz00_8687);
															BgL_arg1802z00_8694 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8695,
																4L);
														}
														BgL_res2168z00_8686 =
															(BgL_arg1802z00_8694 == BgL_classz00_8681);
													}
												else
													{	/* Cfa/cinfo.sch 681 */
														BgL_res2168z00_8686 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2168z00_8686;
							}
					}
				else
					{	/* Cfa/cinfo.sch 681 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &cfun/Cinfo? */
	obj_t BGl_z62cfunzf2Cinfozf3z63zzcfa_approxz00(obj_t BgL_envz00_7213,
		obj_t BgL_objz00_7214)
	{
		{	/* Cfa/cinfo.sch 681 */
			return BBOOL(BGl_cfunzf2Cinfozf3z01zzcfa_approxz00(BgL_objz00_7214));
		}

	}



/* cfun/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_cfunz00_bglt
		BGl_cfunzf2Cinfozd2nilz20zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 682 */
			{	/* Cfa/cinfo.sch 682 */
				obj_t BgL_classz00_4792;

				BgL_classz00_4792 = BGl_cfunzf2Cinfozf2zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 682 */
					obj_t BgL__ortest_1117z00_4793;

					BgL__ortest_1117z00_4793 = BGL_CLASS_NIL(BgL_classz00_4792);
					if (CBOOL(BgL__ortest_1117z00_4793))
						{	/* Cfa/cinfo.sch 682 */
							return ((BgL_cfunz00_bglt) BgL__ortest_1117z00_4793);
						}
					else
						{	/* Cfa/cinfo.sch 682 */
							return
								((BgL_cfunz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4792));
						}
				}
			}
		}

	}



/* &cfun/Cinfo-nil */
	BgL_cfunz00_bglt BGl_z62cfunzf2Cinfozd2nilz42zzcfa_approxz00(obj_t
		BgL_envz00_7215)
	{
		{	/* Cfa/cinfo.sch 682 */
			return BGl_cfunzf2Cinfozd2nilz20zzcfa_approxz00();
		}

	}



/* cfun/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_cfunzf2Cinfozd2approxz20zzcfa_approxz00(BgL_cfunz00_bglt BgL_oz00_68)
	{
		{	/* Cfa/cinfo.sch 683 */
			{
				BgL_cfunzf2cinfozf2_bglt BgL_auxz00_9677;

				{
					obj_t BgL_auxz00_9678;

					{	/* Cfa/cinfo.sch 683 */
						BgL_objectz00_bglt BgL_tmpz00_9679;

						BgL_tmpz00_9679 = ((BgL_objectz00_bglt) BgL_oz00_68);
						BgL_auxz00_9678 = BGL_OBJECT_WIDENING(BgL_tmpz00_9679);
					}
					BgL_auxz00_9677 = ((BgL_cfunzf2cinfozf2_bglt) BgL_auxz00_9678);
				}
				return
					(((BgL_cfunzf2cinfozf2_bglt) COBJECT(BgL_auxz00_9677))->
					BgL_approxz00);
			}
		}

	}



/* &cfun/Cinfo-approx */
	BgL_approxz00_bglt BGl_z62cfunzf2Cinfozd2approxz42zzcfa_approxz00(obj_t
		BgL_envz00_7216, obj_t BgL_oz00_7217)
	{
		{	/* Cfa/cinfo.sch 683 */
			return
				BGl_cfunzf2Cinfozd2approxz20zzcfa_approxz00(
				((BgL_cfunz00_bglt) BgL_oz00_7217));
		}

	}



/* cfun/Cinfo-method */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzf2Cinfozd2methodz20zzcfa_approxz00(BgL_cfunz00_bglt BgL_oz00_71)
	{
		{	/* Cfa/cinfo.sch 685 */
			return
				(((BgL_cfunz00_bglt) COBJECT(
						((BgL_cfunz00_bglt) BgL_oz00_71)))->BgL_methodz00);
		}

	}



/* &cfun/Cinfo-method */
	obj_t BGl_z62cfunzf2Cinfozd2methodz42zzcfa_approxz00(obj_t BgL_envz00_7218,
		obj_t BgL_oz00_7219)
	{
		{	/* Cfa/cinfo.sch 685 */
			return
				BGl_cfunzf2Cinfozd2methodz20zzcfa_approxz00(
				((BgL_cfunz00_bglt) BgL_oz00_7219));
		}

	}



/* cfun/Cinfo-method-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzf2Cinfozd2methodzd2setz12ze0zzcfa_approxz00(BgL_cfunz00_bglt
		BgL_oz00_72, obj_t BgL_vz00_73)
	{
		{	/* Cfa/cinfo.sch 686 */
			return
				((((BgL_cfunz00_bglt) COBJECT(
							((BgL_cfunz00_bglt) BgL_oz00_72)))->BgL_methodz00) =
				((obj_t) BgL_vz00_73), BUNSPEC);
		}

	}



/* &cfun/Cinfo-method-set! */
	obj_t BGl_z62cfunzf2Cinfozd2methodzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_7220, obj_t BgL_oz00_7221, obj_t BgL_vz00_7222)
	{
		{	/* Cfa/cinfo.sch 686 */
			return
				BGl_cfunzf2Cinfozd2methodzd2setz12ze0zzcfa_approxz00(
				((BgL_cfunz00_bglt) BgL_oz00_7221), BgL_vz00_7222);
		}

	}



/* cfun/Cinfo-infix? */
	BGL_EXPORTED_DEF bool_t
		BGl_cfunzf2Cinfozd2infixzf3zd3zzcfa_approxz00(BgL_cfunz00_bglt BgL_oz00_74)
	{
		{	/* Cfa/cinfo.sch 687 */
			return
				(((BgL_cfunz00_bglt) COBJECT(
						((BgL_cfunz00_bglt) BgL_oz00_74)))->BgL_infixzf3zf3);
		}

	}



/* &cfun/Cinfo-infix? */
	obj_t BGl_z62cfunzf2Cinfozd2infixzf3zb1zzcfa_approxz00(obj_t BgL_envz00_7223,
		obj_t BgL_oz00_7224)
	{
		{	/* Cfa/cinfo.sch 687 */
			return
				BBOOL(BGl_cfunzf2Cinfozd2infixzf3zd3zzcfa_approxz00(
					((BgL_cfunz00_bglt) BgL_oz00_7224)));
		}

	}



/* cfun/Cinfo-infix?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzf2Cinfozd2infixzf3zd2setz12z13zzcfa_approxz00(BgL_cfunz00_bglt
		BgL_oz00_75, bool_t BgL_vz00_76)
	{
		{	/* Cfa/cinfo.sch 688 */
			return
				((((BgL_cfunz00_bglt) COBJECT(
							((BgL_cfunz00_bglt) BgL_oz00_75)))->BgL_infixzf3zf3) =
				((bool_t) BgL_vz00_76), BUNSPEC);
		}

	}



/* &cfun/Cinfo-infix?-set! */
	obj_t BGl_z62cfunzf2Cinfozd2infixzf3zd2setz12z71zzcfa_approxz00(obj_t
		BgL_envz00_7225, obj_t BgL_oz00_7226, obj_t BgL_vz00_7227)
	{
		{	/* Cfa/cinfo.sch 688 */
			return
				BGl_cfunzf2Cinfozd2infixzf3zd2setz12z13zzcfa_approxz00(
				((BgL_cfunz00_bglt) BgL_oz00_7226), CBOOL(BgL_vz00_7227));
		}

	}



/* cfun/Cinfo-macro? */
	BGL_EXPORTED_DEF bool_t
		BGl_cfunzf2Cinfozd2macrozf3zd3zzcfa_approxz00(BgL_cfunz00_bglt BgL_oz00_77)
	{
		{	/* Cfa/cinfo.sch 689 */
			return
				(((BgL_cfunz00_bglt) COBJECT(
						((BgL_cfunz00_bglt) BgL_oz00_77)))->BgL_macrozf3zf3);
		}

	}



/* &cfun/Cinfo-macro? */
	obj_t BGl_z62cfunzf2Cinfozd2macrozf3zb1zzcfa_approxz00(obj_t BgL_envz00_7228,
		obj_t BgL_oz00_7229)
	{
		{	/* Cfa/cinfo.sch 689 */
			return
				BBOOL(BGl_cfunzf2Cinfozd2macrozf3zd3zzcfa_approxz00(
					((BgL_cfunz00_bglt) BgL_oz00_7229)));
		}

	}



/* cfun/Cinfo-args-type */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzf2Cinfozd2argszd2typezf2zzcfa_approxz00(BgL_cfunz00_bglt
		BgL_oz00_80)
	{
		{	/* Cfa/cinfo.sch 691 */
			return
				(((BgL_cfunz00_bglt) COBJECT(
						((BgL_cfunz00_bglt) BgL_oz00_80)))->BgL_argszd2typezd2);
		}

	}



/* &cfun/Cinfo-args-type */
	obj_t BGl_z62cfunzf2Cinfozd2argszd2typez90zzcfa_approxz00(obj_t
		BgL_envz00_7230, obj_t BgL_oz00_7231)
	{
		{	/* Cfa/cinfo.sch 691 */
			return
				BGl_cfunzf2Cinfozd2argszd2typezf2zzcfa_approxz00(
				((BgL_cfunz00_bglt) BgL_oz00_7231));
		}

	}



/* cfun/Cinfo-args-retescape */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzf2Cinfozd2argszd2retescapezf2zzcfa_approxz00(BgL_cfunz00_bglt
		BgL_oz00_83)
	{
		{	/* Cfa/cinfo.sch 693 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_83)))->BgL_argszd2retescapezd2);
		}

	}



/* &cfun/Cinfo-args-retescape */
	obj_t BGl_z62cfunzf2Cinfozd2argszd2retescapez90zzcfa_approxz00(obj_t
		BgL_envz00_7232, obj_t BgL_oz00_7233)
	{
		{	/* Cfa/cinfo.sch 693 */
			return
				BGl_cfunzf2Cinfozd2argszd2retescapezf2zzcfa_approxz00(
				((BgL_cfunz00_bglt) BgL_oz00_7233));
		}

	}



/* cfun/Cinfo-args-retescape-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzf2Cinfozd2argszd2retescapezd2setz12z32zzcfa_approxz00
		(BgL_cfunz00_bglt BgL_oz00_84, obj_t BgL_vz00_85)
	{
		{	/* Cfa/cinfo.sch 694 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_84)))->BgL_argszd2retescapezd2) =
				((obj_t) BgL_vz00_85), BUNSPEC);
		}

	}



/* &cfun/Cinfo-args-retescape-set! */
	obj_t BGl_z62cfunzf2Cinfozd2argszd2retescapezd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_7234, obj_t BgL_oz00_7235, obj_t BgL_vz00_7236)
	{
		{	/* Cfa/cinfo.sch 694 */
			return
				BGl_cfunzf2Cinfozd2argszd2retescapezd2setz12z32zzcfa_approxz00(
				((BgL_cfunz00_bglt) BgL_oz00_7235), BgL_vz00_7236);
		}

	}



/* cfun/Cinfo-args-noescape */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzf2Cinfozd2argszd2noescapezf2zzcfa_approxz00(BgL_cfunz00_bglt
		BgL_oz00_86)
	{
		{	/* Cfa/cinfo.sch 695 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_86)))->BgL_argszd2noescapezd2);
		}

	}



/* &cfun/Cinfo-args-noescape */
	obj_t BGl_z62cfunzf2Cinfozd2argszd2noescapez90zzcfa_approxz00(obj_t
		BgL_envz00_7237, obj_t BgL_oz00_7238)
	{
		{	/* Cfa/cinfo.sch 695 */
			return
				BGl_cfunzf2Cinfozd2argszd2noescapezf2zzcfa_approxz00(
				((BgL_cfunz00_bglt) BgL_oz00_7238));
		}

	}



/* cfun/Cinfo-args-noescape-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzf2Cinfozd2argszd2noescapezd2setz12z32zzcfa_approxz00
		(BgL_cfunz00_bglt BgL_oz00_87, obj_t BgL_vz00_88)
	{
		{	/* Cfa/cinfo.sch 696 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_87)))->BgL_argszd2noescapezd2) =
				((obj_t) BgL_vz00_88), BUNSPEC);
		}

	}



/* &cfun/Cinfo-args-noescape-set! */
	obj_t BGl_z62cfunzf2Cinfozd2argszd2noescapezd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_7239, obj_t BgL_oz00_7240, obj_t BgL_vz00_7241)
	{
		{	/* Cfa/cinfo.sch 696 */
			return
				BGl_cfunzf2Cinfozd2argszd2noescapezd2setz12z32zzcfa_approxz00(
				((BgL_cfunz00_bglt) BgL_oz00_7240), BgL_vz00_7241);
		}

	}



/* cfun/Cinfo-failsafe */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzf2Cinfozd2failsafez20zzcfa_approxz00(BgL_cfunz00_bglt BgL_oz00_89)
	{
		{	/* Cfa/cinfo.sch 697 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_89)))->BgL_failsafez00);
		}

	}



/* &cfun/Cinfo-failsafe */
	obj_t BGl_z62cfunzf2Cinfozd2failsafez42zzcfa_approxz00(obj_t BgL_envz00_7242,
		obj_t BgL_oz00_7243)
	{
		{	/* Cfa/cinfo.sch 697 */
			return
				BGl_cfunzf2Cinfozd2failsafez20zzcfa_approxz00(
				((BgL_cfunz00_bglt) BgL_oz00_7243));
		}

	}



/* cfun/Cinfo-failsafe-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzf2Cinfozd2failsafezd2setz12ze0zzcfa_approxz00(BgL_cfunz00_bglt
		BgL_oz00_90, obj_t BgL_vz00_91)
	{
		{	/* Cfa/cinfo.sch 698 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_90)))->BgL_failsafez00) =
				((obj_t) BgL_vz00_91), BUNSPEC);
		}

	}



/* &cfun/Cinfo-failsafe-set! */
	obj_t BGl_z62cfunzf2Cinfozd2failsafezd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_7244, obj_t BgL_oz00_7245, obj_t BgL_vz00_7246)
	{
		{	/* Cfa/cinfo.sch 698 */
			return
				BGl_cfunzf2Cinfozd2failsafezd2setz12ze0zzcfa_approxz00(
				((BgL_cfunz00_bglt) BgL_oz00_7245), BgL_vz00_7246);
		}

	}



/* cfun/Cinfo-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzf2Cinfozd2effectz20zzcfa_approxz00(BgL_cfunz00_bglt BgL_oz00_92)
	{
		{	/* Cfa/cinfo.sch 699 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_92)))->BgL_effectz00);
		}

	}



/* &cfun/Cinfo-effect */
	obj_t BGl_z62cfunzf2Cinfozd2effectz42zzcfa_approxz00(obj_t BgL_envz00_7247,
		obj_t BgL_oz00_7248)
	{
		{	/* Cfa/cinfo.sch 699 */
			return
				BGl_cfunzf2Cinfozd2effectz20zzcfa_approxz00(
				((BgL_cfunz00_bglt) BgL_oz00_7248));
		}

	}



/* cfun/Cinfo-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzf2Cinfozd2effectzd2setz12ze0zzcfa_approxz00(BgL_cfunz00_bglt
		BgL_oz00_93, obj_t BgL_vz00_94)
	{
		{	/* Cfa/cinfo.sch 700 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_93)))->BgL_effectz00) =
				((obj_t) BgL_vz00_94), BUNSPEC);
		}

	}



/* &cfun/Cinfo-effect-set! */
	obj_t BGl_z62cfunzf2Cinfozd2effectzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_7249, obj_t BgL_oz00_7250, obj_t BgL_vz00_7251)
	{
		{	/* Cfa/cinfo.sch 700 */
			return
				BGl_cfunzf2Cinfozd2effectzd2setz12ze0zzcfa_approxz00(
				((BgL_cfunz00_bglt) BgL_oz00_7250), BgL_vz00_7251);
		}

	}



/* cfun/Cinfo-the-closure */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzf2Cinfozd2thezd2closurezf2zzcfa_approxz00(BgL_cfunz00_bglt
		BgL_oz00_95)
	{
		{	/* Cfa/cinfo.sch 701 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_95)))->BgL_thezd2closurezd2);
		}

	}



/* &cfun/Cinfo-the-closure */
	obj_t BGl_z62cfunzf2Cinfozd2thezd2closurez90zzcfa_approxz00(obj_t
		BgL_envz00_7252, obj_t BgL_oz00_7253)
	{
		{	/* Cfa/cinfo.sch 701 */
			return
				BGl_cfunzf2Cinfozd2thezd2closurezf2zzcfa_approxz00(
				((BgL_cfunz00_bglt) BgL_oz00_7253));
		}

	}



/* cfun/Cinfo-the-closure-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzf2Cinfozd2thezd2closurezd2setz12z32zzcfa_approxz00(BgL_cfunz00_bglt
		BgL_oz00_96, obj_t BgL_vz00_97)
	{
		{	/* Cfa/cinfo.sch 702 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_96)))->BgL_thezd2closurezd2) =
				((obj_t) BgL_vz00_97), BUNSPEC);
		}

	}



/* &cfun/Cinfo-the-closure-set! */
	obj_t BGl_z62cfunzf2Cinfozd2thezd2closurezd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_7254, obj_t BgL_oz00_7255, obj_t BgL_vz00_7256)
	{
		{	/* Cfa/cinfo.sch 702 */
			return
				BGl_cfunzf2Cinfozd2thezd2closurezd2setz12z32zzcfa_approxz00(
				((BgL_cfunz00_bglt) BgL_oz00_7255), BgL_vz00_7256);
		}

	}



/* cfun/Cinfo-top? */
	BGL_EXPORTED_DEF bool_t
		BGl_cfunzf2Cinfozd2topzf3zd3zzcfa_approxz00(BgL_cfunz00_bglt BgL_oz00_98)
	{
		{	/* Cfa/cinfo.sch 703 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_98)))->BgL_topzf3zf3);
		}

	}



/* &cfun/Cinfo-top? */
	obj_t BGl_z62cfunzf2Cinfozd2topzf3zb1zzcfa_approxz00(obj_t BgL_envz00_7257,
		obj_t BgL_oz00_7258)
	{
		{	/* Cfa/cinfo.sch 703 */
			return
				BBOOL(BGl_cfunzf2Cinfozd2topzf3zd3zzcfa_approxz00(
					((BgL_cfunz00_bglt) BgL_oz00_7258)));
		}

	}



/* cfun/Cinfo-top?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzf2Cinfozd2topzf3zd2setz12z13zzcfa_approxz00(BgL_cfunz00_bglt
		BgL_oz00_99, bool_t BgL_vz00_100)
	{
		{	/* Cfa/cinfo.sch 704 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_99)))->BgL_topzf3zf3) =
				((bool_t) BgL_vz00_100), BUNSPEC);
		}

	}



/* &cfun/Cinfo-top?-set! */
	obj_t BGl_z62cfunzf2Cinfozd2topzf3zd2setz12z71zzcfa_approxz00(obj_t
		BgL_envz00_7259, obj_t BgL_oz00_7260, obj_t BgL_vz00_7261)
	{
		{	/* Cfa/cinfo.sch 704 */
			return
				BGl_cfunzf2Cinfozd2topzf3zd2setz12z13zzcfa_approxz00(
				((BgL_cfunz00_bglt) BgL_oz00_7260), CBOOL(BgL_vz00_7261));
		}

	}



/* cfun/Cinfo-stack-allocator */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzf2Cinfozd2stackzd2allocatorzf2zzcfa_approxz00(BgL_cfunz00_bglt
		BgL_oz00_101)
	{
		{	/* Cfa/cinfo.sch 705 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_101)))->BgL_stackzd2allocatorzd2);
		}

	}



/* &cfun/Cinfo-stack-allocator */
	obj_t BGl_z62cfunzf2Cinfozd2stackzd2allocatorz90zzcfa_approxz00(obj_t
		BgL_envz00_7262, obj_t BgL_oz00_7263)
	{
		{	/* Cfa/cinfo.sch 705 */
			return
				BGl_cfunzf2Cinfozd2stackzd2allocatorzf2zzcfa_approxz00(
				((BgL_cfunz00_bglt) BgL_oz00_7263));
		}

	}



/* cfun/Cinfo-stack-allocator-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzf2Cinfozd2stackzd2allocatorzd2setz12z32zzcfa_approxz00
		(BgL_cfunz00_bglt BgL_oz00_102, obj_t BgL_vz00_103)
	{
		{	/* Cfa/cinfo.sch 706 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_102)))->BgL_stackzd2allocatorzd2) =
				((obj_t) BgL_vz00_103), BUNSPEC);
		}

	}



/* &cfun/Cinfo-stack-allocator-set! */
	obj_t BGl_z62cfunzf2Cinfozd2stackzd2allocatorzd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_7264, obj_t BgL_oz00_7265, obj_t BgL_vz00_7266)
	{
		{	/* Cfa/cinfo.sch 706 */
			return
				BGl_cfunzf2Cinfozd2stackzd2allocatorzd2setz12z32zzcfa_approxz00(
				((BgL_cfunz00_bglt) BgL_oz00_7265), BgL_vz00_7266);
		}

	}



/* cfun/Cinfo-predicate-of */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzf2Cinfozd2predicatezd2ofzf2zzcfa_approxz00(BgL_cfunz00_bglt
		BgL_oz00_104)
	{
		{	/* Cfa/cinfo.sch 707 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_104)))->BgL_predicatezd2ofzd2);
		}

	}



/* &cfun/Cinfo-predicate-of */
	obj_t BGl_z62cfunzf2Cinfozd2predicatezd2ofz90zzcfa_approxz00(obj_t
		BgL_envz00_7267, obj_t BgL_oz00_7268)
	{
		{	/* Cfa/cinfo.sch 707 */
			return
				BGl_cfunzf2Cinfozd2predicatezd2ofzf2zzcfa_approxz00(
				((BgL_cfunz00_bglt) BgL_oz00_7268));
		}

	}



/* cfun/Cinfo-predicate-of-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzf2Cinfozd2predicatezd2ofzd2setz12z32zzcfa_approxz00
		(BgL_cfunz00_bglt BgL_oz00_105, obj_t BgL_vz00_106)
	{
		{	/* Cfa/cinfo.sch 708 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_105)))->BgL_predicatezd2ofzd2) =
				((obj_t) BgL_vz00_106), BUNSPEC);
		}

	}



/* &cfun/Cinfo-predicate-of-set! */
	obj_t BGl_z62cfunzf2Cinfozd2predicatezd2ofzd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_7269, obj_t BgL_oz00_7270, obj_t BgL_vz00_7271)
	{
		{	/* Cfa/cinfo.sch 708 */
			return
				BGl_cfunzf2Cinfozd2predicatezd2ofzd2setz12z32zzcfa_approxz00(
				((BgL_cfunz00_bglt) BgL_oz00_7270), BgL_vz00_7271);
		}

	}



/* cfun/Cinfo-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzf2Cinfozd2sidezd2effectzf2zzcfa_approxz00(BgL_cfunz00_bglt
		BgL_oz00_107)
	{
		{	/* Cfa/cinfo.sch 709 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_107)))->BgL_sidezd2effectzd2);
		}

	}



/* &cfun/Cinfo-side-effect */
	obj_t BGl_z62cfunzf2Cinfozd2sidezd2effectz90zzcfa_approxz00(obj_t
		BgL_envz00_7272, obj_t BgL_oz00_7273)
	{
		{	/* Cfa/cinfo.sch 709 */
			return
				BGl_cfunzf2Cinfozd2sidezd2effectzf2zzcfa_approxz00(
				((BgL_cfunz00_bglt) BgL_oz00_7273));
		}

	}



/* cfun/Cinfo-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_approxz00(BgL_cfunz00_bglt
		BgL_oz00_108, obj_t BgL_vz00_109)
	{
		{	/* Cfa/cinfo.sch 710 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_108)))->BgL_sidezd2effectzd2) =
				((obj_t) BgL_vz00_109), BUNSPEC);
		}

	}



/* &cfun/Cinfo-side-effect-set! */
	obj_t BGl_z62cfunzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_7274, obj_t BgL_oz00_7275, obj_t BgL_vz00_7276)
	{
		{	/* Cfa/cinfo.sch 710 */
			return
				BGl_cfunzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_approxz00(
				((BgL_cfunz00_bglt) BgL_oz00_7275), BgL_vz00_7276);
		}

	}



/* cfun/Cinfo-arity */
	BGL_EXPORTED_DEF long
		BGl_cfunzf2Cinfozd2arityz20zzcfa_approxz00(BgL_cfunz00_bglt BgL_oz00_110)
	{
		{	/* Cfa/cinfo.sch 711 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_110)))->BgL_arityz00);
		}

	}



/* &cfun/Cinfo-arity */
	obj_t BGl_z62cfunzf2Cinfozd2arityz42zzcfa_approxz00(obj_t BgL_envz00_7277,
		obj_t BgL_oz00_7278)
	{
		{	/* Cfa/cinfo.sch 711 */
			return
				BINT(BGl_cfunzf2Cinfozd2arityz20zzcfa_approxz00(
					((BgL_cfunz00_bglt) BgL_oz00_7278)));
		}

	}



/* make-extern-sfun/Cinfo */
	BGL_EXPORTED_DEF BgL_sfunz00_bglt
		BGl_makezd2externzd2sfunzf2Cinfozf2zzcfa_approxz00(long
		BgL_arity1780z00_113, obj_t BgL_sidezd2effect1781zd2_114,
		obj_t BgL_predicatezd2of1782zd2_115, obj_t BgL_stackzd2allocator1783zd2_116,
		bool_t BgL_topzf31784zf3_117, obj_t BgL_thezd2closure1785zd2_118,
		obj_t BgL_effect1786z00_119, obj_t BgL_failsafe1787z00_120,
		obj_t BgL_argszd2noescape1788zd2_121, obj_t BgL_argszd2retescape1789zd2_122,
		obj_t BgL_property1790z00_123, obj_t BgL_args1791z00_124,
		obj_t BgL_argszd2name1792zd2_125, obj_t BgL_body1793z00_126,
		obj_t BgL_class1794z00_127, obj_t BgL_dssslzd2keywords1795zd2_128,
		obj_t BgL_loc1796z00_129, obj_t BgL_optionals1797z00_130,
		obj_t BgL_keys1798z00_131, obj_t BgL_thezd2closurezd2global1799z00_132,
		obj_t BgL_strength1800z00_133, obj_t BgL_stackable1801z00_134,
		bool_t BgL_polymorphiczf31802zf3_135,
		BgL_approxz00_bglt BgL_approx1803z00_136)
	{
		{	/* Cfa/cinfo.sch 715 */
			{	/* Cfa/cinfo.sch 715 */
				BgL_sfunz00_bglt BgL_new1185z00_8696;

				{	/* Cfa/cinfo.sch 715 */
					BgL_sfunz00_bglt BgL_tmp1183z00_8697;
					BgL_externzd2sfunzf2cinfoz20_bglt BgL_wide1184z00_8698;

					{
						BgL_sfunz00_bglt BgL_auxz00_9792;

						{	/* Cfa/cinfo.sch 715 */
							BgL_sfunz00_bglt BgL_new1182z00_8699;

							BgL_new1182z00_8699 =
								((BgL_sfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_sfunz00_bgl))));
							{	/* Cfa/cinfo.sch 715 */
								long BgL_arg1642z00_8700;

								BgL_arg1642z00_8700 = BGL_CLASS_NUM(BGl_sfunz00zzast_varz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1182z00_8699),
									BgL_arg1642z00_8700);
							}
							{	/* Cfa/cinfo.sch 715 */
								BgL_objectz00_bglt BgL_tmpz00_9797;

								BgL_tmpz00_9797 = ((BgL_objectz00_bglt) BgL_new1182z00_8699);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9797, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1182z00_8699);
							BgL_auxz00_9792 = BgL_new1182z00_8699;
						}
						BgL_tmp1183z00_8697 = ((BgL_sfunz00_bglt) BgL_auxz00_9792);
					}
					BgL_wide1184z00_8698 =
						((BgL_externzd2sfunzf2cinfoz20_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_externzd2sfunzf2cinfoz20_bgl))));
					{	/* Cfa/cinfo.sch 715 */
						obj_t BgL_auxz00_9805;
						BgL_objectz00_bglt BgL_tmpz00_9803;

						BgL_auxz00_9805 = ((obj_t) BgL_wide1184z00_8698);
						BgL_tmpz00_9803 = ((BgL_objectz00_bglt) BgL_tmp1183z00_8697);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9803, BgL_auxz00_9805);
					}
					((BgL_objectz00_bglt) BgL_tmp1183z00_8697);
					{	/* Cfa/cinfo.sch 715 */
						long BgL_arg1630z00_8701;

						BgL_arg1630z00_8701 =
							BGL_CLASS_NUM(BGl_externzd2sfunzf2Cinfoz20zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1183z00_8697), BgL_arg1630z00_8701);
					}
					BgL_new1185z00_8696 = ((BgL_sfunz00_bglt) BgL_tmp1183z00_8697);
				}
				((((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt) BgL_new1185z00_8696)))->BgL_arityz00) =
					((long) BgL_arity1780z00_113), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1185z00_8696)))->
						BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1781zd2_114), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1185z00_8696)))->
						BgL_predicatezd2ofzd2) =
					((obj_t) BgL_predicatezd2of1782zd2_115), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1185z00_8696)))->
						BgL_stackzd2allocatorzd2) =
					((obj_t) BgL_stackzd2allocator1783zd2_116), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1185z00_8696)))->
						BgL_topzf3zf3) = ((bool_t) BgL_topzf31784zf3_117), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1185z00_8696)))->
						BgL_thezd2closurezd2) =
					((obj_t) BgL_thezd2closure1785zd2_118), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1185z00_8696)))->
						BgL_effectz00) = ((obj_t) BgL_effect1786z00_119), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1185z00_8696)))->
						BgL_failsafez00) = ((obj_t) BgL_failsafe1787z00_120), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1185z00_8696)))->
						BgL_argszd2noescapezd2) =
					((obj_t) BgL_argszd2noescape1788zd2_121), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1185z00_8696)))->
						BgL_argszd2retescapezd2) =
					((obj_t) BgL_argszd2retescape1789zd2_122), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1185z00_8696)))->BgL_propertyz00) =
					((obj_t) BgL_property1790z00_123), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1185z00_8696)))->BgL_argsz00) =
					((obj_t) BgL_args1791z00_124), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1185z00_8696)))->BgL_argszd2namezd2) =
					((obj_t) BgL_argszd2name1792zd2_125), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1185z00_8696)))->BgL_bodyz00) =
					((obj_t) BgL_body1793z00_126), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1185z00_8696)))->BgL_classz00) =
					((obj_t) BgL_class1794z00_127), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1185z00_8696)))->BgL_dssslzd2keywordszd2) =
					((obj_t) BgL_dssslzd2keywords1795zd2_128), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1185z00_8696)))->BgL_locz00) =
					((obj_t) BgL_loc1796z00_129), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1185z00_8696)))->BgL_optionalsz00) =
					((obj_t) BgL_optionals1797z00_130), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1185z00_8696)))->BgL_keysz00) =
					((obj_t) BgL_keys1798z00_131), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1185z00_8696)))->BgL_thezd2closurezd2globalz00) =
					((obj_t) BgL_thezd2closurezd2global1799z00_132), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1185z00_8696)))->BgL_strengthz00) =
					((obj_t) BgL_strength1800z00_133), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1185z00_8696)))->BgL_stackablez00) =
					((obj_t) BgL_stackable1801z00_134), BUNSPEC);
				{
					BgL_externzd2sfunzf2cinfoz20_bglt BgL_auxz00_9857;

					{
						obj_t BgL_auxz00_9858;

						{	/* Cfa/cinfo.sch 715 */
							BgL_objectz00_bglt BgL_tmpz00_9859;

							BgL_tmpz00_9859 = ((BgL_objectz00_bglt) BgL_new1185z00_8696);
							BgL_auxz00_9858 = BGL_OBJECT_WIDENING(BgL_tmpz00_9859);
						}
						BgL_auxz00_9857 =
							((BgL_externzd2sfunzf2cinfoz20_bglt) BgL_auxz00_9858);
					}
					((((BgL_externzd2sfunzf2cinfoz20_bglt) COBJECT(BgL_auxz00_9857))->
							BgL_polymorphiczf3zf3) =
						((bool_t) BgL_polymorphiczf31802zf3_135), BUNSPEC);
				}
				{
					BgL_externzd2sfunzf2cinfoz20_bglt BgL_auxz00_9864;

					{
						obj_t BgL_auxz00_9865;

						{	/* Cfa/cinfo.sch 715 */
							BgL_objectz00_bglt BgL_tmpz00_9866;

							BgL_tmpz00_9866 = ((BgL_objectz00_bglt) BgL_new1185z00_8696);
							BgL_auxz00_9865 = BGL_OBJECT_WIDENING(BgL_tmpz00_9866);
						}
						BgL_auxz00_9864 =
							((BgL_externzd2sfunzf2cinfoz20_bglt) BgL_auxz00_9865);
					}
					((((BgL_externzd2sfunzf2cinfoz20_bglt) COBJECT(BgL_auxz00_9864))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1803z00_136), BUNSPEC);
				}
				return BgL_new1185z00_8696;
			}
		}

	}



/* &make-extern-sfun/Cinfo */
	BgL_sfunz00_bglt BGl_z62makezd2externzd2sfunzf2Cinfoz90zzcfa_approxz00(obj_t
		BgL_envz00_7279, obj_t BgL_arity1780z00_7280,
		obj_t BgL_sidezd2effect1781zd2_7281, obj_t BgL_predicatezd2of1782zd2_7282,
		obj_t BgL_stackzd2allocator1783zd2_7283, obj_t BgL_topzf31784zf3_7284,
		obj_t BgL_thezd2closure1785zd2_7285, obj_t BgL_effect1786z00_7286,
		obj_t BgL_failsafe1787z00_7287, obj_t BgL_argszd2noescape1788zd2_7288,
		obj_t BgL_argszd2retescape1789zd2_7289, obj_t BgL_property1790z00_7290,
		obj_t BgL_args1791z00_7291, obj_t BgL_argszd2name1792zd2_7292,
		obj_t BgL_body1793z00_7293, obj_t BgL_class1794z00_7294,
		obj_t BgL_dssslzd2keywords1795zd2_7295, obj_t BgL_loc1796z00_7296,
		obj_t BgL_optionals1797z00_7297, obj_t BgL_keys1798z00_7298,
		obj_t BgL_thezd2closurezd2global1799z00_7299,
		obj_t BgL_strength1800z00_7300, obj_t BgL_stackable1801z00_7301,
		obj_t BgL_polymorphiczf31802zf3_7302, obj_t BgL_approx1803z00_7303)
	{
		{	/* Cfa/cinfo.sch 715 */
			return
				BGl_makezd2externzd2sfunzf2Cinfozf2zzcfa_approxz00(
				(long) CINT(BgL_arity1780z00_7280), BgL_sidezd2effect1781zd2_7281,
				BgL_predicatezd2of1782zd2_7282, BgL_stackzd2allocator1783zd2_7283,
				CBOOL(BgL_topzf31784zf3_7284), BgL_thezd2closure1785zd2_7285,
				BgL_effect1786z00_7286, BgL_failsafe1787z00_7287,
				BgL_argszd2noescape1788zd2_7288, BgL_argszd2retescape1789zd2_7289,
				BgL_property1790z00_7290, BgL_args1791z00_7291,
				BgL_argszd2name1792zd2_7292, BgL_body1793z00_7293,
				BgL_class1794z00_7294, BgL_dssslzd2keywords1795zd2_7295,
				BgL_loc1796z00_7296, BgL_optionals1797z00_7297, BgL_keys1798z00_7298,
				BgL_thezd2closurezd2global1799z00_7299, BgL_strength1800z00_7300,
				BgL_stackable1801z00_7301, CBOOL(BgL_polymorphiczf31802zf3_7302),
				((BgL_approxz00_bglt) BgL_approx1803z00_7303));
		}

	}



/* extern-sfun/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_externzd2sfunzf2Cinfozf3zd3zzcfa_approxz00(obj_t
		BgL_objz00_137)
	{
		{	/* Cfa/cinfo.sch 716 */
			{	/* Cfa/cinfo.sch 716 */
				obj_t BgL_classz00_8702;

				BgL_classz00_8702 = BGl_externzd2sfunzf2Cinfoz20zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_137))
					{	/* Cfa/cinfo.sch 716 */
						BgL_objectz00_bglt BgL_arg1807z00_8703;

						BgL_arg1807z00_8703 = (BgL_objectz00_bglt) (BgL_objz00_137);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 716 */
								long BgL_idxz00_8704;

								BgL_idxz00_8704 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8703);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8704 + 4L)) == BgL_classz00_8702);
							}
						else
							{	/* Cfa/cinfo.sch 716 */
								bool_t BgL_res2169z00_8707;

								{	/* Cfa/cinfo.sch 716 */
									obj_t BgL_oclassz00_8708;

									{	/* Cfa/cinfo.sch 716 */
										obj_t BgL_arg1815z00_8709;
										long BgL_arg1816z00_8710;

										BgL_arg1815z00_8709 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 716 */
											long BgL_arg1817z00_8711;

											BgL_arg1817z00_8711 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8703);
											BgL_arg1816z00_8710 = (BgL_arg1817z00_8711 - OBJECT_TYPE);
										}
										BgL_oclassz00_8708 =
											VECTOR_REF(BgL_arg1815z00_8709, BgL_arg1816z00_8710);
									}
									{	/* Cfa/cinfo.sch 716 */
										bool_t BgL__ortest_1115z00_8712;

										BgL__ortest_1115z00_8712 =
											(BgL_classz00_8702 == BgL_oclassz00_8708);
										if (BgL__ortest_1115z00_8712)
											{	/* Cfa/cinfo.sch 716 */
												BgL_res2169z00_8707 = BgL__ortest_1115z00_8712;
											}
										else
											{	/* Cfa/cinfo.sch 716 */
												long BgL_odepthz00_8713;

												{	/* Cfa/cinfo.sch 716 */
													obj_t BgL_arg1804z00_8714;

													BgL_arg1804z00_8714 = (BgL_oclassz00_8708);
													BgL_odepthz00_8713 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8714);
												}
												if ((4L < BgL_odepthz00_8713))
													{	/* Cfa/cinfo.sch 716 */
														obj_t BgL_arg1802z00_8715;

														{	/* Cfa/cinfo.sch 716 */
															obj_t BgL_arg1803z00_8716;

															BgL_arg1803z00_8716 = (BgL_oclassz00_8708);
															BgL_arg1802z00_8715 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8716,
																4L);
														}
														BgL_res2169z00_8707 =
															(BgL_arg1802z00_8715 == BgL_classz00_8702);
													}
												else
													{	/* Cfa/cinfo.sch 716 */
														BgL_res2169z00_8707 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2169z00_8707;
							}
					}
				else
					{	/* Cfa/cinfo.sch 716 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &extern-sfun/Cinfo? */
	obj_t BGl_z62externzd2sfunzf2Cinfozf3zb1zzcfa_approxz00(obj_t BgL_envz00_7304,
		obj_t BgL_objz00_7305)
	{
		{	/* Cfa/cinfo.sch 716 */
			return
				BBOOL(BGl_externzd2sfunzf2Cinfozf3zd3zzcfa_approxz00(BgL_objz00_7305));
		}

	}



/* extern-sfun/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_sfunz00_bglt
		BGl_externzd2sfunzf2Cinfozd2nilzf2zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 717 */
			{	/* Cfa/cinfo.sch 717 */
				obj_t BgL_classz00_4845;

				BgL_classz00_4845 = BGl_externzd2sfunzf2Cinfoz20zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 717 */
					obj_t BgL__ortest_1117z00_4846;

					BgL__ortest_1117z00_4846 = BGL_CLASS_NIL(BgL_classz00_4845);
					if (CBOOL(BgL__ortest_1117z00_4846))
						{	/* Cfa/cinfo.sch 717 */
							return ((BgL_sfunz00_bglt) BgL__ortest_1117z00_4846);
						}
					else
						{	/* Cfa/cinfo.sch 717 */
							return
								((BgL_sfunz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4845));
						}
				}
			}
		}

	}



/* &extern-sfun/Cinfo-nil */
	BgL_sfunz00_bglt BGl_z62externzd2sfunzf2Cinfozd2nilz90zzcfa_approxz00(obj_t
		BgL_envz00_7306)
	{
		{	/* Cfa/cinfo.sch 717 */
			return BGl_externzd2sfunzf2Cinfozd2nilzf2zzcfa_approxz00();
		}

	}



/* extern-sfun/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_externzd2sfunzf2Cinfozd2approxzf2zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_138)
	{
		{	/* Cfa/cinfo.sch 718 */
			{
				BgL_externzd2sfunzf2cinfoz20_bglt BgL_auxz00_9907;

				{
					obj_t BgL_auxz00_9908;

					{	/* Cfa/cinfo.sch 718 */
						BgL_objectz00_bglt BgL_tmpz00_9909;

						BgL_tmpz00_9909 = ((BgL_objectz00_bglt) BgL_oz00_138);
						BgL_auxz00_9908 = BGL_OBJECT_WIDENING(BgL_tmpz00_9909);
					}
					BgL_auxz00_9907 =
						((BgL_externzd2sfunzf2cinfoz20_bglt) BgL_auxz00_9908);
				}
				return
					(((BgL_externzd2sfunzf2cinfoz20_bglt) COBJECT(BgL_auxz00_9907))->
					BgL_approxz00);
			}
		}

	}



/* &extern-sfun/Cinfo-approx */
	BgL_approxz00_bglt
		BGl_z62externzd2sfunzf2Cinfozd2approxz90zzcfa_approxz00(obj_t
		BgL_envz00_7307, obj_t BgL_oz00_7308)
	{
		{	/* Cfa/cinfo.sch 718 */
			return
				BGl_externzd2sfunzf2Cinfozd2approxzf2zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7308));
		}

	}



/* extern-sfun/Cinfo-polymorphic? */
	BGL_EXPORTED_DEF bool_t
		BGl_externzd2sfunzf2Cinfozd2polymorphiczf3z01zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_141)
	{
		{	/* Cfa/cinfo.sch 720 */
			{
				BgL_externzd2sfunzf2cinfoz20_bglt BgL_auxz00_9916;

				{
					obj_t BgL_auxz00_9917;

					{	/* Cfa/cinfo.sch 720 */
						BgL_objectz00_bglt BgL_tmpz00_9918;

						BgL_tmpz00_9918 = ((BgL_objectz00_bglt) BgL_oz00_141);
						BgL_auxz00_9917 = BGL_OBJECT_WIDENING(BgL_tmpz00_9918);
					}
					BgL_auxz00_9916 =
						((BgL_externzd2sfunzf2cinfoz20_bglt) BgL_auxz00_9917);
				}
				return
					(((BgL_externzd2sfunzf2cinfoz20_bglt) COBJECT(BgL_auxz00_9916))->
					BgL_polymorphiczf3zf3);
			}
		}

	}



/* &extern-sfun/Cinfo-polymorphic? */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2polymorphiczf3z63zzcfa_approxz00(obj_t
		BgL_envz00_7309, obj_t BgL_oz00_7310)
	{
		{	/* Cfa/cinfo.sch 720 */
			return
				BBOOL(BGl_externzd2sfunzf2Cinfozd2polymorphiczf3z01zzcfa_approxz00(
					((BgL_sfunz00_bglt) BgL_oz00_7310)));
		}

	}



/* extern-sfun/Cinfo-polymorphic?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2polymorphiczf3zd2setz12zc1zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_142, bool_t BgL_vz00_143)
	{
		{	/* Cfa/cinfo.sch 721 */
			{
				BgL_externzd2sfunzf2cinfoz20_bglt BgL_auxz00_9926;

				{
					obj_t BgL_auxz00_9927;

					{	/* Cfa/cinfo.sch 721 */
						BgL_objectz00_bglt BgL_tmpz00_9928;

						BgL_tmpz00_9928 = ((BgL_objectz00_bglt) BgL_oz00_142);
						BgL_auxz00_9927 = BGL_OBJECT_WIDENING(BgL_tmpz00_9928);
					}
					BgL_auxz00_9926 =
						((BgL_externzd2sfunzf2cinfoz20_bglt) BgL_auxz00_9927);
				}
				return
					((((BgL_externzd2sfunzf2cinfoz20_bglt) COBJECT(BgL_auxz00_9926))->
						BgL_polymorphiczf3zf3) = ((bool_t) BgL_vz00_143), BUNSPEC);
			}
		}

	}



/* &extern-sfun/Cinfo-polymorphic?-set! */
	obj_t
		BGl_z62externzd2sfunzf2Cinfozd2polymorphiczf3zd2setz12za3zzcfa_approxz00
		(obj_t BgL_envz00_7311, obj_t BgL_oz00_7312, obj_t BgL_vz00_7313)
	{
		{	/* Cfa/cinfo.sch 721 */
			return
				BGl_externzd2sfunzf2Cinfozd2polymorphiczf3zd2setz12zc1zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7312), CBOOL(BgL_vz00_7313));
		}

	}



/* extern-sfun/Cinfo-stackable */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2stackablezf2zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_144)
	{
		{	/* Cfa/cinfo.sch 722 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_144)))->BgL_stackablez00);
		}

	}



/* &extern-sfun/Cinfo-stackable */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2stackablez90zzcfa_approxz00(obj_t
		BgL_envz00_7314, obj_t BgL_oz00_7315)
	{
		{	/* Cfa/cinfo.sch 722 */
			return
				BGl_externzd2sfunzf2Cinfozd2stackablezf2zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7315));
		}

	}



/* extern-sfun/Cinfo-stackable-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2stackablezd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_145, obj_t BgL_vz00_146)
	{
		{	/* Cfa/cinfo.sch 723 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_145)))->BgL_stackablez00) =
				((obj_t) BgL_vz00_146), BUNSPEC);
		}

	}



/* &extern-sfun/Cinfo-stackable-set! */
	obj_t
		BGl_z62externzd2sfunzf2Cinfozd2stackablezd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_7316, obj_t BgL_oz00_7317, obj_t BgL_vz00_7318)
	{
		{	/* Cfa/cinfo.sch 723 */
			return
				BGl_externzd2sfunzf2Cinfozd2stackablezd2setz12z32zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7317), BgL_vz00_7318);
		}

	}



/* extern-sfun/Cinfo-strength */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2strengthzf2zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_147)
	{
		{	/* Cfa/cinfo.sch 724 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_147)))->BgL_strengthz00);
		}

	}



/* &extern-sfun/Cinfo-strength */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2strengthz90zzcfa_approxz00(obj_t
		BgL_envz00_7319, obj_t BgL_oz00_7320)
	{
		{	/* Cfa/cinfo.sch 724 */
			return
				BGl_externzd2sfunzf2Cinfozd2strengthzf2zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7320));
		}

	}



/* extern-sfun/Cinfo-strength-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2strengthzd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_148, obj_t BgL_vz00_149)
	{
		{	/* Cfa/cinfo.sch 725 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_148)))->BgL_strengthz00) =
				((obj_t) BgL_vz00_149), BUNSPEC);
		}

	}



/* &extern-sfun/Cinfo-strength-set! */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2strengthzd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_7321, obj_t BgL_oz00_7322, obj_t BgL_vz00_7323)
	{
		{	/* Cfa/cinfo.sch 725 */
			return
				BGl_externzd2sfunzf2Cinfozd2strengthzd2setz12z32zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7322), BgL_vz00_7323);
		}

	}



/* extern-sfun/Cinfo-the-closure-global */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2thezd2closurezd2globalzf2zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_150)
	{
		{	/* Cfa/cinfo.sch 726 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_150)))->BgL_thezd2closurezd2globalz00);
		}

	}



/* &extern-sfun/Cinfo-the-closure-global */
	obj_t
		BGl_z62externzd2sfunzf2Cinfozd2thezd2closurezd2globalz90zzcfa_approxz00
		(obj_t BgL_envz00_7324, obj_t BgL_oz00_7325)
	{
		{	/* Cfa/cinfo.sch 726 */
			return
				BGl_externzd2sfunzf2Cinfozd2thezd2closurezd2globalzf2zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7325));
		}

	}



/* extern-sfun/Cinfo-the-closure-global-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2thezd2closurezd2globalzd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_151, obj_t BgL_vz00_152)
	{
		{	/* Cfa/cinfo.sch 727 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_151)))->
					BgL_thezd2closurezd2globalz00) = ((obj_t) BgL_vz00_152), BUNSPEC);
		}

	}



/* &extern-sfun/Cinfo-the-closure-global-set! */
	obj_t
		BGl_z62externzd2sfunzf2Cinfozd2thezd2closurezd2globalzd2setz12z50zzcfa_approxz00
		(obj_t BgL_envz00_7326, obj_t BgL_oz00_7327, obj_t BgL_vz00_7328)
	{
		{	/* Cfa/cinfo.sch 727 */
			return
				BGl_externzd2sfunzf2Cinfozd2thezd2closurezd2globalzd2setz12z32zzcfa_approxz00
				(((BgL_sfunz00_bglt) BgL_oz00_7327), BgL_vz00_7328);
		}

	}



/* extern-sfun/Cinfo-keys */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2keyszf2zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_153)
	{
		{	/* Cfa/cinfo.sch 728 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_153)))->BgL_keysz00);
		}

	}



/* &extern-sfun/Cinfo-keys */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2keysz90zzcfa_approxz00(obj_t
		BgL_envz00_7329, obj_t BgL_oz00_7330)
	{
		{	/* Cfa/cinfo.sch 728 */
			return
				BGl_externzd2sfunzf2Cinfozd2keyszf2zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7330));
		}

	}



/* extern-sfun/Cinfo-optionals */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2optionalszf2zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_156)
	{
		{	/* Cfa/cinfo.sch 730 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_156)))->BgL_optionalsz00);
		}

	}



/* &extern-sfun/Cinfo-optionals */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2optionalsz90zzcfa_approxz00(obj_t
		BgL_envz00_7331, obj_t BgL_oz00_7332)
	{
		{	/* Cfa/cinfo.sch 730 */
			return
				BGl_externzd2sfunzf2Cinfozd2optionalszf2zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7332));
		}

	}



/* extern-sfun/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2loczf2zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_159)
	{
		{	/* Cfa/cinfo.sch 732 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_159)))->BgL_locz00);
		}

	}



/* &extern-sfun/Cinfo-loc */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2locz90zzcfa_approxz00(obj_t
		BgL_envz00_7333, obj_t BgL_oz00_7334)
	{
		{	/* Cfa/cinfo.sch 732 */
			return
				BGl_externzd2sfunzf2Cinfozd2loczf2zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7334));
		}

	}



/* extern-sfun/Cinfo-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2loczd2setz12z32zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_160, obj_t BgL_vz00_161)
	{
		{	/* Cfa/cinfo.sch 733 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_160)))->BgL_locz00) =
				((obj_t) BgL_vz00_161), BUNSPEC);
		}

	}



/* &extern-sfun/Cinfo-loc-set! */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2loczd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_7335, obj_t BgL_oz00_7336, obj_t BgL_vz00_7337)
	{
		{	/* Cfa/cinfo.sch 733 */
			return
				BGl_externzd2sfunzf2Cinfozd2loczd2setz12z32zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7336), BgL_vz00_7337);
		}

	}



/* extern-sfun/Cinfo-dsssl-keywords */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2dssslzd2keywordsz20zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_162)
	{
		{	/* Cfa/cinfo.sch 734 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_162)))->BgL_dssslzd2keywordszd2);
		}

	}



/* &extern-sfun/Cinfo-dsssl-keywords */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2dssslzd2keywordsz42zzcfa_approxz00(obj_t
		BgL_envz00_7338, obj_t BgL_oz00_7339)
	{
		{	/* Cfa/cinfo.sch 734 */
			return
				BGl_externzd2sfunzf2Cinfozd2dssslzd2keywordsz20zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7339));
		}

	}



/* extern-sfun/Cinfo-dsssl-keywords-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2dssslzd2keywordszd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_163, obj_t BgL_vz00_164)
	{
		{	/* Cfa/cinfo.sch 735 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_163)))->BgL_dssslzd2keywordszd2) =
				((obj_t) BgL_vz00_164), BUNSPEC);
		}

	}



/* &extern-sfun/Cinfo-dsssl-keywords-set! */
	obj_t
		BGl_z62externzd2sfunzf2Cinfozd2dssslzd2keywordszd2setz12z82zzcfa_approxz00
		(obj_t BgL_envz00_7340, obj_t BgL_oz00_7341, obj_t BgL_vz00_7342)
	{
		{	/* Cfa/cinfo.sch 735 */
			return
				BGl_externzd2sfunzf2Cinfozd2dssslzd2keywordszd2setz12ze0zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7341), BgL_vz00_7342);
		}

	}



/* extern-sfun/Cinfo-class */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2classzf2zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_165)
	{
		{	/* Cfa/cinfo.sch 736 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_165)))->BgL_classz00);
		}

	}



/* &extern-sfun/Cinfo-class */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2classz90zzcfa_approxz00(obj_t
		BgL_envz00_7343, obj_t BgL_oz00_7344)
	{
		{	/* Cfa/cinfo.sch 736 */
			return
				BGl_externzd2sfunzf2Cinfozd2classzf2zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7344));
		}

	}



/* extern-sfun/Cinfo-class-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2classzd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_166, obj_t BgL_vz00_167)
	{
		{	/* Cfa/cinfo.sch 737 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_166)))->BgL_classz00) =
				((obj_t) BgL_vz00_167), BUNSPEC);
		}

	}



/* &extern-sfun/Cinfo-class-set! */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2classzd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_7345, obj_t BgL_oz00_7346, obj_t BgL_vz00_7347)
	{
		{	/* Cfa/cinfo.sch 737 */
			return
				BGl_externzd2sfunzf2Cinfozd2classzd2setz12z32zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7346), BgL_vz00_7347);
		}

	}



/* extern-sfun/Cinfo-body */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2bodyzf2zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_168)
	{
		{	/* Cfa/cinfo.sch 738 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_168)))->BgL_bodyz00);
		}

	}



/* &extern-sfun/Cinfo-body */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2bodyz90zzcfa_approxz00(obj_t
		BgL_envz00_7348, obj_t BgL_oz00_7349)
	{
		{	/* Cfa/cinfo.sch 738 */
			return
				BGl_externzd2sfunzf2Cinfozd2bodyzf2zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7349));
		}

	}



/* extern-sfun/Cinfo-body-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2bodyzd2setz12z32zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_169, obj_t BgL_vz00_170)
	{
		{	/* Cfa/cinfo.sch 739 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_169)))->BgL_bodyz00) =
				((obj_t) BgL_vz00_170), BUNSPEC);
		}

	}



/* &extern-sfun/Cinfo-body-set! */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2bodyzd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_7350, obj_t BgL_oz00_7351, obj_t BgL_vz00_7352)
	{
		{	/* Cfa/cinfo.sch 739 */
			return
				BGl_externzd2sfunzf2Cinfozd2bodyzd2setz12z32zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7351), BgL_vz00_7352);
		}

	}



/* extern-sfun/Cinfo-args-name */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2argszd2namez20zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_171)
	{
		{	/* Cfa/cinfo.sch 740 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_171)))->BgL_argszd2namezd2);
		}

	}



/* &extern-sfun/Cinfo-args-name */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2argszd2namez42zzcfa_approxz00(obj_t
		BgL_envz00_7353, obj_t BgL_oz00_7354)
	{
		{	/* Cfa/cinfo.sch 740 */
			return
				BGl_externzd2sfunzf2Cinfozd2argszd2namez20zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7354));
		}

	}



/* extern-sfun/Cinfo-args */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2argszf2zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_174)
	{
		{	/* Cfa/cinfo.sch 742 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_174)))->BgL_argsz00);
		}

	}



/* &extern-sfun/Cinfo-args */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2argsz90zzcfa_approxz00(obj_t
		BgL_envz00_7355, obj_t BgL_oz00_7356)
	{
		{	/* Cfa/cinfo.sch 742 */
			return
				BGl_externzd2sfunzf2Cinfozd2argszf2zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7356));
		}

	}



/* extern-sfun/Cinfo-args-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2argszd2setz12z32zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_175, obj_t BgL_vz00_176)
	{
		{	/* Cfa/cinfo.sch 743 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_175)))->BgL_argsz00) =
				((obj_t) BgL_vz00_176), BUNSPEC);
		}

	}



/* &extern-sfun/Cinfo-args-set! */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2argszd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_7357, obj_t BgL_oz00_7358, obj_t BgL_vz00_7359)
	{
		{	/* Cfa/cinfo.sch 743 */
			return
				BGl_externzd2sfunzf2Cinfozd2argszd2setz12z32zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7358), BgL_vz00_7359);
		}

	}



/* extern-sfun/Cinfo-property */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2propertyzf2zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_177)
	{
		{	/* Cfa/cinfo.sch 744 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_177)))->BgL_propertyz00);
		}

	}



/* &extern-sfun/Cinfo-property */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2propertyz90zzcfa_approxz00(obj_t
		BgL_envz00_7360, obj_t BgL_oz00_7361)
	{
		{	/* Cfa/cinfo.sch 744 */
			return
				BGl_externzd2sfunzf2Cinfozd2propertyzf2zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7361));
		}

	}



/* extern-sfun/Cinfo-property-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2propertyzd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_178, obj_t BgL_vz00_179)
	{
		{	/* Cfa/cinfo.sch 745 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_178)))->BgL_propertyz00) =
				((obj_t) BgL_vz00_179), BUNSPEC);
		}

	}



/* &extern-sfun/Cinfo-property-set! */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2propertyzd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_7362, obj_t BgL_oz00_7363, obj_t BgL_vz00_7364)
	{
		{	/* Cfa/cinfo.sch 745 */
			return
				BGl_externzd2sfunzf2Cinfozd2propertyzd2setz12z32zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7363), BgL_vz00_7364);
		}

	}



/* extern-sfun/Cinfo-args-retescape */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2argszd2retescapez20zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_180)
	{
		{	/* Cfa/cinfo.sch 746 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_180)))->BgL_argszd2retescapezd2);
		}

	}



/* &extern-sfun/Cinfo-args-retescape */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2argszd2retescapez42zzcfa_approxz00(obj_t
		BgL_envz00_7365, obj_t BgL_oz00_7366)
	{
		{	/* Cfa/cinfo.sch 746 */
			return
				BGl_externzd2sfunzf2Cinfozd2argszd2retescapez20zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7366));
		}

	}



/* extern-sfun/Cinfo-args-retescape-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2argszd2retescapezd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_181, obj_t BgL_vz00_182)
	{
		{	/* Cfa/cinfo.sch 747 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_181)))->BgL_argszd2retescapezd2) =
				((obj_t) BgL_vz00_182), BUNSPEC);
		}

	}



/* &extern-sfun/Cinfo-args-retescape-set! */
	obj_t
		BGl_z62externzd2sfunzf2Cinfozd2argszd2retescapezd2setz12z82zzcfa_approxz00
		(obj_t BgL_envz00_7367, obj_t BgL_oz00_7368, obj_t BgL_vz00_7369)
	{
		{	/* Cfa/cinfo.sch 747 */
			return
				BGl_externzd2sfunzf2Cinfozd2argszd2retescapezd2setz12ze0zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7368), BgL_vz00_7369);
		}

	}



/* extern-sfun/Cinfo-args-noescape */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2argszd2noescapez20zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_183)
	{
		{	/* Cfa/cinfo.sch 748 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_183)))->BgL_argszd2noescapezd2);
		}

	}



/* &extern-sfun/Cinfo-args-noescape */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2argszd2noescapez42zzcfa_approxz00(obj_t
		BgL_envz00_7370, obj_t BgL_oz00_7371)
	{
		{	/* Cfa/cinfo.sch 748 */
			return
				BGl_externzd2sfunzf2Cinfozd2argszd2noescapez20zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7371));
		}

	}



/* extern-sfun/Cinfo-args-noescape-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2argszd2noescapezd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_184, obj_t BgL_vz00_185)
	{
		{	/* Cfa/cinfo.sch 749 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_184)))->BgL_argszd2noescapezd2) =
				((obj_t) BgL_vz00_185), BUNSPEC);
		}

	}



/* &extern-sfun/Cinfo-args-noescape-set! */
	obj_t
		BGl_z62externzd2sfunzf2Cinfozd2argszd2noescapezd2setz12z82zzcfa_approxz00
		(obj_t BgL_envz00_7372, obj_t BgL_oz00_7373, obj_t BgL_vz00_7374)
	{
		{	/* Cfa/cinfo.sch 749 */
			return
				BGl_externzd2sfunzf2Cinfozd2argszd2noescapezd2setz12ze0zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7373), BgL_vz00_7374);
		}

	}



/* extern-sfun/Cinfo-failsafe */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2failsafezf2zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_186)
	{
		{	/* Cfa/cinfo.sch 750 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_186)))->BgL_failsafez00);
		}

	}



/* &extern-sfun/Cinfo-failsafe */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2failsafez90zzcfa_approxz00(obj_t
		BgL_envz00_7375, obj_t BgL_oz00_7376)
	{
		{	/* Cfa/cinfo.sch 750 */
			return
				BGl_externzd2sfunzf2Cinfozd2failsafezf2zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7376));
		}

	}



/* extern-sfun/Cinfo-failsafe-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2failsafezd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_187, obj_t BgL_vz00_188)
	{
		{	/* Cfa/cinfo.sch 751 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_187)))->BgL_failsafez00) =
				((obj_t) BgL_vz00_188), BUNSPEC);
		}

	}



/* &extern-sfun/Cinfo-failsafe-set! */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2failsafezd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_7377, obj_t BgL_oz00_7378, obj_t BgL_vz00_7379)
	{
		{	/* Cfa/cinfo.sch 751 */
			return
				BGl_externzd2sfunzf2Cinfozd2failsafezd2setz12z32zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7378), BgL_vz00_7379);
		}

	}



/* extern-sfun/Cinfo-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2effectzf2zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_189)
	{
		{	/* Cfa/cinfo.sch 752 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_189)))->BgL_effectz00);
		}

	}



/* &extern-sfun/Cinfo-effect */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2effectz90zzcfa_approxz00(obj_t
		BgL_envz00_7380, obj_t BgL_oz00_7381)
	{
		{	/* Cfa/cinfo.sch 752 */
			return
				BGl_externzd2sfunzf2Cinfozd2effectzf2zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7381));
		}

	}



/* extern-sfun/Cinfo-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2effectzd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_190, obj_t BgL_vz00_191)
	{
		{	/* Cfa/cinfo.sch 753 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_190)))->BgL_effectz00) =
				((obj_t) BgL_vz00_191), BUNSPEC);
		}

	}



/* &extern-sfun/Cinfo-effect-set! */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2effectzd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_7382, obj_t BgL_oz00_7383, obj_t BgL_vz00_7384)
	{
		{	/* Cfa/cinfo.sch 753 */
			return
				BGl_externzd2sfunzf2Cinfozd2effectzd2setz12z32zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7383), BgL_vz00_7384);
		}

	}



/* extern-sfun/Cinfo-the-closure */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2thezd2closurez20zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_192)
	{
		{	/* Cfa/cinfo.sch 754 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_192)))->BgL_thezd2closurezd2);
		}

	}



/* &extern-sfun/Cinfo-the-closure */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2thezd2closurez42zzcfa_approxz00(obj_t
		BgL_envz00_7385, obj_t BgL_oz00_7386)
	{
		{	/* Cfa/cinfo.sch 754 */
			return
				BGl_externzd2sfunzf2Cinfozd2thezd2closurez20zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7386));
		}

	}



/* extern-sfun/Cinfo-the-closure-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2thezd2closurezd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_193, obj_t BgL_vz00_194)
	{
		{	/* Cfa/cinfo.sch 755 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_193)))->BgL_thezd2closurezd2) =
				((obj_t) BgL_vz00_194), BUNSPEC);
		}

	}



/* &extern-sfun/Cinfo-the-closure-set! */
	obj_t
		BGl_z62externzd2sfunzf2Cinfozd2thezd2closurezd2setz12z82zzcfa_approxz00
		(obj_t BgL_envz00_7387, obj_t BgL_oz00_7388, obj_t BgL_vz00_7389)
	{
		{	/* Cfa/cinfo.sch 755 */
			return
				BGl_externzd2sfunzf2Cinfozd2thezd2closurezd2setz12ze0zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7388), BgL_vz00_7389);
		}

	}



/* extern-sfun/Cinfo-top? */
	BGL_EXPORTED_DEF bool_t
		BGl_externzd2sfunzf2Cinfozd2topzf3z01zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_195)
	{
		{	/* Cfa/cinfo.sch 756 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_195)))->BgL_topzf3zf3);
		}

	}



/* &extern-sfun/Cinfo-top? */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2topzf3z63zzcfa_approxz00(obj_t
		BgL_envz00_7390, obj_t BgL_oz00_7391)
	{
		{	/* Cfa/cinfo.sch 756 */
			return
				BBOOL(BGl_externzd2sfunzf2Cinfozd2topzf3z01zzcfa_approxz00(
					((BgL_sfunz00_bglt) BgL_oz00_7391)));
		}

	}



/* extern-sfun/Cinfo-top?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2topzf3zd2setz12zc1zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_196, bool_t BgL_vz00_197)
	{
		{	/* Cfa/cinfo.sch 757 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_196)))->BgL_topzf3zf3) =
				((bool_t) BgL_vz00_197), BUNSPEC);
		}

	}



/* &extern-sfun/Cinfo-top?-set! */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2topzf3zd2setz12za3zzcfa_approxz00(obj_t
		BgL_envz00_7392, obj_t BgL_oz00_7393, obj_t BgL_vz00_7394)
	{
		{	/* Cfa/cinfo.sch 757 */
			return
				BGl_externzd2sfunzf2Cinfozd2topzf3zd2setz12zc1zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7393), CBOOL(BgL_vz00_7394));
		}

	}



/* extern-sfun/Cinfo-stack-allocator */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2stackzd2allocatorz20zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_198)
	{
		{	/* Cfa/cinfo.sch 758 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_198)))->BgL_stackzd2allocatorzd2);
		}

	}



/* &extern-sfun/Cinfo-stack-allocator */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2stackzd2allocatorz42zzcfa_approxz00(obj_t
		BgL_envz00_7395, obj_t BgL_oz00_7396)
	{
		{	/* Cfa/cinfo.sch 758 */
			return
				BGl_externzd2sfunzf2Cinfozd2stackzd2allocatorz20zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7396));
		}

	}



/* extern-sfun/Cinfo-stack-allocator-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2stackzd2allocatorzd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_199, obj_t BgL_vz00_200)
	{
		{	/* Cfa/cinfo.sch 759 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_199)))->BgL_stackzd2allocatorzd2) =
				((obj_t) BgL_vz00_200), BUNSPEC);
		}

	}



/* &extern-sfun/Cinfo-stack-allocator-set! */
	obj_t
		BGl_z62externzd2sfunzf2Cinfozd2stackzd2allocatorzd2setz12z82zzcfa_approxz00
		(obj_t BgL_envz00_7397, obj_t BgL_oz00_7398, obj_t BgL_vz00_7399)
	{
		{	/* Cfa/cinfo.sch 759 */
			return
				BGl_externzd2sfunzf2Cinfozd2stackzd2allocatorzd2setz12ze0zzcfa_approxz00
				(((BgL_sfunz00_bglt) BgL_oz00_7398), BgL_vz00_7399);
		}

	}



/* extern-sfun/Cinfo-predicate-of */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2predicatezd2ofz20zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_201)
	{
		{	/* Cfa/cinfo.sch 760 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_201)))->BgL_predicatezd2ofzd2);
		}

	}



/* &extern-sfun/Cinfo-predicate-of */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2predicatezd2ofz42zzcfa_approxz00(obj_t
		BgL_envz00_7400, obj_t BgL_oz00_7401)
	{
		{	/* Cfa/cinfo.sch 760 */
			return
				BGl_externzd2sfunzf2Cinfozd2predicatezd2ofz20zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7401));
		}

	}



/* extern-sfun/Cinfo-predicate-of-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2predicatezd2ofzd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_202, obj_t BgL_vz00_203)
	{
		{	/* Cfa/cinfo.sch 761 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_202)))->BgL_predicatezd2ofzd2) =
				((obj_t) BgL_vz00_203), BUNSPEC);
		}

	}



/* &extern-sfun/Cinfo-predicate-of-set! */
	obj_t
		BGl_z62externzd2sfunzf2Cinfozd2predicatezd2ofzd2setz12z82zzcfa_approxz00
		(obj_t BgL_envz00_7402, obj_t BgL_oz00_7403, obj_t BgL_vz00_7404)
	{
		{	/* Cfa/cinfo.sch 761 */
			return
				BGl_externzd2sfunzf2Cinfozd2predicatezd2ofzd2setz12ze0zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7403), BgL_vz00_7404);
		}

	}



/* extern-sfun/Cinfo-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2sidezd2effectz20zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_204)
	{
		{	/* Cfa/cinfo.sch 762 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_204)))->BgL_sidezd2effectzd2);
		}

	}



/* &extern-sfun/Cinfo-side-effect */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2sidezd2effectz42zzcfa_approxz00(obj_t
		BgL_envz00_7405, obj_t BgL_oz00_7406)
	{
		{	/* Cfa/cinfo.sch 762 */
			return
				BGl_externzd2sfunzf2Cinfozd2sidezd2effectz20zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7406));
		}

	}



/* extern-sfun/Cinfo-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_externzd2sfunzf2Cinfozd2sidezd2effectzd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_205, obj_t BgL_vz00_206)
	{
		{	/* Cfa/cinfo.sch 763 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_205)))->BgL_sidezd2effectzd2) =
				((obj_t) BgL_vz00_206), BUNSPEC);
		}

	}



/* &extern-sfun/Cinfo-side-effect-set! */
	obj_t
		BGl_z62externzd2sfunzf2Cinfozd2sidezd2effectzd2setz12z82zzcfa_approxz00
		(obj_t BgL_envz00_7407, obj_t BgL_oz00_7408, obj_t BgL_vz00_7409)
	{
		{	/* Cfa/cinfo.sch 763 */
			return
				BGl_externzd2sfunzf2Cinfozd2sidezd2effectzd2setz12ze0zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7408), BgL_vz00_7409);
		}

	}



/* extern-sfun/Cinfo-arity */
	BGL_EXPORTED_DEF long
		BGl_externzd2sfunzf2Cinfozd2arityzf2zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_207)
	{
		{	/* Cfa/cinfo.sch 764 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_207)))->BgL_arityz00);
		}

	}



/* &extern-sfun/Cinfo-arity */
	obj_t BGl_z62externzd2sfunzf2Cinfozd2arityz90zzcfa_approxz00(obj_t
		BgL_envz00_7410, obj_t BgL_oz00_7411)
	{
		{	/* Cfa/cinfo.sch 764 */
			return
				BINT(BGl_externzd2sfunzf2Cinfozd2arityzf2zzcfa_approxz00(
					((BgL_sfunz00_bglt) BgL_oz00_7411)));
		}

	}



/* make-intern-sfun/Cinfo */
	BGL_EXPORTED_DEF BgL_sfunz00_bglt
		BGl_makezd2internzd2sfunzf2Cinfozf2zzcfa_approxz00(long
		BgL_arity1753z00_210, obj_t BgL_sidezd2effect1754zd2_211,
		obj_t BgL_predicatezd2of1755zd2_212, obj_t BgL_stackzd2allocator1756zd2_213,
		bool_t BgL_topzf31757zf3_214, obj_t BgL_thezd2closure1758zd2_215,
		obj_t BgL_effect1759z00_216, obj_t BgL_failsafe1760z00_217,
		obj_t BgL_argszd2noescape1761zd2_218, obj_t BgL_argszd2retescape1762zd2_219,
		obj_t BgL_property1763z00_220, obj_t BgL_args1764z00_221,
		obj_t BgL_argszd2name1765zd2_222, obj_t BgL_body1766z00_223,
		obj_t BgL_class1768z00_224, obj_t BgL_dssslzd2keywords1769zd2_225,
		obj_t BgL_loc1770z00_226, obj_t BgL_optionals1771z00_227,
		obj_t BgL_keys1772z00_228, obj_t BgL_thezd2closurezd2global1773z00_229,
		obj_t BgL_strength1774z00_230, obj_t BgL_stackable1775z00_231,
		bool_t BgL_polymorphiczf31776zf3_232,
		BgL_approxz00_bglt BgL_approx1777z00_233, long BgL_stamp1778z00_234)
	{
		{	/* Cfa/cinfo.sch 768 */
			{	/* Cfa/cinfo.sch 768 */
				BgL_sfunz00_bglt BgL_new1189z00_8717;

				{	/* Cfa/cinfo.sch 768 */
					BgL_sfunz00_bglt BgL_tmp1187z00_8718;
					BgL_internzd2sfunzf2cinfoz20_bglt BgL_wide1188z00_8719;

					{
						BgL_sfunz00_bglt BgL_auxz00_10099;

						{	/* Cfa/cinfo.sch 768 */
							BgL_sfunz00_bglt BgL_new1186z00_8720;

							BgL_new1186z00_8720 =
								((BgL_sfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_sfunz00_bgl))));
							{	/* Cfa/cinfo.sch 768 */
								long BgL_arg1650z00_8721;

								BgL_arg1650z00_8721 = BGL_CLASS_NUM(BGl_sfunz00zzast_varz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1186z00_8720),
									BgL_arg1650z00_8721);
							}
							{	/* Cfa/cinfo.sch 768 */
								BgL_objectz00_bglt BgL_tmpz00_10104;

								BgL_tmpz00_10104 = ((BgL_objectz00_bglt) BgL_new1186z00_8720);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10104, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1186z00_8720);
							BgL_auxz00_10099 = BgL_new1186z00_8720;
						}
						BgL_tmp1187z00_8718 = ((BgL_sfunz00_bglt) BgL_auxz00_10099);
					}
					BgL_wide1188z00_8719 =
						((BgL_internzd2sfunzf2cinfoz20_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_internzd2sfunzf2cinfoz20_bgl))));
					{	/* Cfa/cinfo.sch 768 */
						obj_t BgL_auxz00_10112;
						BgL_objectz00_bglt BgL_tmpz00_10110;

						BgL_auxz00_10112 = ((obj_t) BgL_wide1188z00_8719);
						BgL_tmpz00_10110 = ((BgL_objectz00_bglt) BgL_tmp1187z00_8718);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10110, BgL_auxz00_10112);
					}
					((BgL_objectz00_bglt) BgL_tmp1187z00_8718);
					{	/* Cfa/cinfo.sch 768 */
						long BgL_arg1646z00_8722;

						BgL_arg1646z00_8722 =
							BGL_CLASS_NUM(BGl_internzd2sfunzf2Cinfoz20zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1187z00_8718), BgL_arg1646z00_8722);
					}
					BgL_new1189z00_8717 = ((BgL_sfunz00_bglt) BgL_tmp1187z00_8718);
				}
				((((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt) BgL_new1189z00_8717)))->BgL_arityz00) =
					((long) BgL_arity1753z00_210), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1189z00_8717)))->
						BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1754zd2_211), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1189z00_8717)))->
						BgL_predicatezd2ofzd2) =
					((obj_t) BgL_predicatezd2of1755zd2_212), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1189z00_8717)))->
						BgL_stackzd2allocatorzd2) =
					((obj_t) BgL_stackzd2allocator1756zd2_213), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1189z00_8717)))->
						BgL_topzf3zf3) = ((bool_t) BgL_topzf31757zf3_214), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1189z00_8717)))->
						BgL_thezd2closurezd2) =
					((obj_t) BgL_thezd2closure1758zd2_215), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1189z00_8717)))->
						BgL_effectz00) = ((obj_t) BgL_effect1759z00_216), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1189z00_8717)))->
						BgL_failsafez00) = ((obj_t) BgL_failsafe1760z00_217), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1189z00_8717)))->
						BgL_argszd2noescapezd2) =
					((obj_t) BgL_argszd2noescape1761zd2_218), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1189z00_8717)))->
						BgL_argszd2retescapezd2) =
					((obj_t) BgL_argszd2retescape1762zd2_219), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1189z00_8717)))->BgL_propertyz00) =
					((obj_t) BgL_property1763z00_220), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1189z00_8717)))->BgL_argsz00) =
					((obj_t) BgL_args1764z00_221), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1189z00_8717)))->BgL_argszd2namezd2) =
					((obj_t) BgL_argszd2name1765zd2_222), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1189z00_8717)))->BgL_bodyz00) =
					((obj_t) BgL_body1766z00_223), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1189z00_8717)))->BgL_classz00) =
					((obj_t) BgL_class1768z00_224), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1189z00_8717)))->BgL_dssslzd2keywordszd2) =
					((obj_t) BgL_dssslzd2keywords1769zd2_225), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1189z00_8717)))->BgL_locz00) =
					((obj_t) BgL_loc1770z00_226), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1189z00_8717)))->BgL_optionalsz00) =
					((obj_t) BgL_optionals1771z00_227), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1189z00_8717)))->BgL_keysz00) =
					((obj_t) BgL_keys1772z00_228), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1189z00_8717)))->BgL_thezd2closurezd2globalz00) =
					((obj_t) BgL_thezd2closurezd2global1773z00_229), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1189z00_8717)))->BgL_strengthz00) =
					((obj_t) BgL_strength1774z00_230), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1189z00_8717)))->BgL_stackablez00) =
					((obj_t) BgL_stackable1775z00_231), BUNSPEC);
				{
					BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_10164;

					{
						obj_t BgL_auxz00_10165;

						{	/* Cfa/cinfo.sch 768 */
							BgL_objectz00_bglt BgL_tmpz00_10166;

							BgL_tmpz00_10166 = ((BgL_objectz00_bglt) BgL_new1189z00_8717);
							BgL_auxz00_10165 = BGL_OBJECT_WIDENING(BgL_tmpz00_10166);
						}
						BgL_auxz00_10164 =
							((BgL_internzd2sfunzf2cinfoz20_bglt) BgL_auxz00_10165);
					}
					((((BgL_internzd2sfunzf2cinfoz20_bglt) COBJECT(BgL_auxz00_10164))->
							BgL_polymorphiczf3zf3) =
						((bool_t) BgL_polymorphiczf31776zf3_232), BUNSPEC);
				}
				{
					BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_10171;

					{
						obj_t BgL_auxz00_10172;

						{	/* Cfa/cinfo.sch 768 */
							BgL_objectz00_bglt BgL_tmpz00_10173;

							BgL_tmpz00_10173 = ((BgL_objectz00_bglt) BgL_new1189z00_8717);
							BgL_auxz00_10172 = BGL_OBJECT_WIDENING(BgL_tmpz00_10173);
						}
						BgL_auxz00_10171 =
							((BgL_internzd2sfunzf2cinfoz20_bglt) BgL_auxz00_10172);
					}
					((((BgL_internzd2sfunzf2cinfoz20_bglt) COBJECT(BgL_auxz00_10171))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1777z00_233), BUNSPEC);
				}
				{
					BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_10178;

					{
						obj_t BgL_auxz00_10179;

						{	/* Cfa/cinfo.sch 768 */
							BgL_objectz00_bglt BgL_tmpz00_10180;

							BgL_tmpz00_10180 = ((BgL_objectz00_bglt) BgL_new1189z00_8717);
							BgL_auxz00_10179 = BGL_OBJECT_WIDENING(BgL_tmpz00_10180);
						}
						BgL_auxz00_10178 =
							((BgL_internzd2sfunzf2cinfoz20_bglt) BgL_auxz00_10179);
					}
					((((BgL_internzd2sfunzf2cinfoz20_bglt) COBJECT(BgL_auxz00_10178))->
							BgL_stampz00) = ((long) BgL_stamp1778z00_234), BUNSPEC);
				}
				return BgL_new1189z00_8717;
			}
		}

	}



/* &make-intern-sfun/Cinfo */
	BgL_sfunz00_bglt BGl_z62makezd2internzd2sfunzf2Cinfoz90zzcfa_approxz00(obj_t
		BgL_envz00_7412, obj_t BgL_arity1753z00_7413,
		obj_t BgL_sidezd2effect1754zd2_7414, obj_t BgL_predicatezd2of1755zd2_7415,
		obj_t BgL_stackzd2allocator1756zd2_7416, obj_t BgL_topzf31757zf3_7417,
		obj_t BgL_thezd2closure1758zd2_7418, obj_t BgL_effect1759z00_7419,
		obj_t BgL_failsafe1760z00_7420, obj_t BgL_argszd2noescape1761zd2_7421,
		obj_t BgL_argszd2retescape1762zd2_7422, obj_t BgL_property1763z00_7423,
		obj_t BgL_args1764z00_7424, obj_t BgL_argszd2name1765zd2_7425,
		obj_t BgL_body1766z00_7426, obj_t BgL_class1768z00_7427,
		obj_t BgL_dssslzd2keywords1769zd2_7428, obj_t BgL_loc1770z00_7429,
		obj_t BgL_optionals1771z00_7430, obj_t BgL_keys1772z00_7431,
		obj_t BgL_thezd2closurezd2global1773z00_7432,
		obj_t BgL_strength1774z00_7433, obj_t BgL_stackable1775z00_7434,
		obj_t BgL_polymorphiczf31776zf3_7435, obj_t BgL_approx1777z00_7436,
		obj_t BgL_stamp1778z00_7437)
	{
		{	/* Cfa/cinfo.sch 768 */
			return
				BGl_makezd2internzd2sfunzf2Cinfozf2zzcfa_approxz00(
				(long) CINT(BgL_arity1753z00_7413), BgL_sidezd2effect1754zd2_7414,
				BgL_predicatezd2of1755zd2_7415, BgL_stackzd2allocator1756zd2_7416,
				CBOOL(BgL_topzf31757zf3_7417), BgL_thezd2closure1758zd2_7418,
				BgL_effect1759z00_7419, BgL_failsafe1760z00_7420,
				BgL_argszd2noescape1761zd2_7421, BgL_argszd2retescape1762zd2_7422,
				BgL_property1763z00_7423, BgL_args1764z00_7424,
				BgL_argszd2name1765zd2_7425, BgL_body1766z00_7426,
				BgL_class1768z00_7427, BgL_dssslzd2keywords1769zd2_7428,
				BgL_loc1770z00_7429, BgL_optionals1771z00_7430, BgL_keys1772z00_7431,
				BgL_thezd2closurezd2global1773z00_7432, BgL_strength1774z00_7433,
				BgL_stackable1775z00_7434, CBOOL(BgL_polymorphiczf31776zf3_7435),
				((BgL_approxz00_bglt) BgL_approx1777z00_7436),
				(long) CINT(BgL_stamp1778z00_7437));
		}

	}



/* intern-sfun/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_internzd2sfunzf2Cinfozf3zd3zzcfa_approxz00(obj_t
		BgL_objz00_235)
	{
		{	/* Cfa/cinfo.sch 769 */
			{	/* Cfa/cinfo.sch 769 */
				obj_t BgL_classz00_8723;

				BgL_classz00_8723 = BGl_internzd2sfunzf2Cinfoz20zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_235))
					{	/* Cfa/cinfo.sch 769 */
						BgL_objectz00_bglt BgL_arg1807z00_8724;

						BgL_arg1807z00_8724 = (BgL_objectz00_bglt) (BgL_objz00_235);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 769 */
								long BgL_idxz00_8725;

								BgL_idxz00_8725 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8724);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8725 + 4L)) == BgL_classz00_8723);
							}
						else
							{	/* Cfa/cinfo.sch 769 */
								bool_t BgL_res2170z00_8728;

								{	/* Cfa/cinfo.sch 769 */
									obj_t BgL_oclassz00_8729;

									{	/* Cfa/cinfo.sch 769 */
										obj_t BgL_arg1815z00_8730;
										long BgL_arg1816z00_8731;

										BgL_arg1815z00_8730 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 769 */
											long BgL_arg1817z00_8732;

											BgL_arg1817z00_8732 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8724);
											BgL_arg1816z00_8731 = (BgL_arg1817z00_8732 - OBJECT_TYPE);
										}
										BgL_oclassz00_8729 =
											VECTOR_REF(BgL_arg1815z00_8730, BgL_arg1816z00_8731);
									}
									{	/* Cfa/cinfo.sch 769 */
										bool_t BgL__ortest_1115z00_8733;

										BgL__ortest_1115z00_8733 =
											(BgL_classz00_8723 == BgL_oclassz00_8729);
										if (BgL__ortest_1115z00_8733)
											{	/* Cfa/cinfo.sch 769 */
												BgL_res2170z00_8728 = BgL__ortest_1115z00_8733;
											}
										else
											{	/* Cfa/cinfo.sch 769 */
												long BgL_odepthz00_8734;

												{	/* Cfa/cinfo.sch 769 */
													obj_t BgL_arg1804z00_8735;

													BgL_arg1804z00_8735 = (BgL_oclassz00_8729);
													BgL_odepthz00_8734 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8735);
												}
												if ((4L < BgL_odepthz00_8734))
													{	/* Cfa/cinfo.sch 769 */
														obj_t BgL_arg1802z00_8736;

														{	/* Cfa/cinfo.sch 769 */
															obj_t BgL_arg1803z00_8737;

															BgL_arg1803z00_8737 = (BgL_oclassz00_8729);
															BgL_arg1802z00_8736 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8737,
																4L);
														}
														BgL_res2170z00_8728 =
															(BgL_arg1802z00_8736 == BgL_classz00_8723);
													}
												else
													{	/* Cfa/cinfo.sch 769 */
														BgL_res2170z00_8728 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2170z00_8728;
							}
					}
				else
					{	/* Cfa/cinfo.sch 769 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &intern-sfun/Cinfo? */
	obj_t BGl_z62internzd2sfunzf2Cinfozf3zb1zzcfa_approxz00(obj_t BgL_envz00_7438,
		obj_t BgL_objz00_7439)
	{
		{	/* Cfa/cinfo.sch 769 */
			return
				BBOOL(BGl_internzd2sfunzf2Cinfozf3zd3zzcfa_approxz00(BgL_objz00_7439));
		}

	}



/* intern-sfun/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_sfunz00_bglt
		BGl_internzd2sfunzf2Cinfozd2nilzf2zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 770 */
			{	/* Cfa/cinfo.sch 770 */
				obj_t BgL_classz00_4901;

				BgL_classz00_4901 = BGl_internzd2sfunzf2Cinfoz20zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 770 */
					obj_t BgL__ortest_1117z00_4902;

					BgL__ortest_1117z00_4902 = BGL_CLASS_NIL(BgL_classz00_4901);
					if (CBOOL(BgL__ortest_1117z00_4902))
						{	/* Cfa/cinfo.sch 770 */
							return ((BgL_sfunz00_bglt) BgL__ortest_1117z00_4902);
						}
					else
						{	/* Cfa/cinfo.sch 770 */
							return
								((BgL_sfunz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4901));
						}
				}
			}
		}

	}



/* &intern-sfun/Cinfo-nil */
	BgL_sfunz00_bglt BGl_z62internzd2sfunzf2Cinfozd2nilz90zzcfa_approxz00(obj_t
		BgL_envz00_7440)
	{
		{	/* Cfa/cinfo.sch 770 */
			return BGl_internzd2sfunzf2Cinfozd2nilzf2zzcfa_approxz00();
		}

	}



/* intern-sfun/Cinfo-stamp */
	BGL_EXPORTED_DEF long
		BGl_internzd2sfunzf2Cinfozd2stampzf2zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_236)
	{
		{	/* Cfa/cinfo.sch 771 */
			{
				BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_10222;

				{
					obj_t BgL_auxz00_10223;

					{	/* Cfa/cinfo.sch 771 */
						BgL_objectz00_bglt BgL_tmpz00_10224;

						BgL_tmpz00_10224 = ((BgL_objectz00_bglt) BgL_oz00_236);
						BgL_auxz00_10223 = BGL_OBJECT_WIDENING(BgL_tmpz00_10224);
					}
					BgL_auxz00_10222 =
						((BgL_internzd2sfunzf2cinfoz20_bglt) BgL_auxz00_10223);
				}
				return
					(((BgL_internzd2sfunzf2cinfoz20_bglt) COBJECT(BgL_auxz00_10222))->
					BgL_stampz00);
			}
		}

	}



/* &intern-sfun/Cinfo-stamp */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2stampz90zzcfa_approxz00(obj_t
		BgL_envz00_7441, obj_t BgL_oz00_7442)
	{
		{	/* Cfa/cinfo.sch 771 */
			return
				BINT(BGl_internzd2sfunzf2Cinfozd2stampzf2zzcfa_approxz00(
					((BgL_sfunz00_bglt) BgL_oz00_7442)));
		}

	}



/* intern-sfun/Cinfo-stamp-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2stampzd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_237, long BgL_vz00_238)
	{
		{	/* Cfa/cinfo.sch 772 */
			{
				BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_10232;

				{
					obj_t BgL_auxz00_10233;

					{	/* Cfa/cinfo.sch 772 */
						BgL_objectz00_bglt BgL_tmpz00_10234;

						BgL_tmpz00_10234 = ((BgL_objectz00_bglt) BgL_oz00_237);
						BgL_auxz00_10233 = BGL_OBJECT_WIDENING(BgL_tmpz00_10234);
					}
					BgL_auxz00_10232 =
						((BgL_internzd2sfunzf2cinfoz20_bglt) BgL_auxz00_10233);
				}
				return
					((((BgL_internzd2sfunzf2cinfoz20_bglt) COBJECT(BgL_auxz00_10232))->
						BgL_stampz00) = ((long) BgL_vz00_238), BUNSPEC);
		}}

	}



/* &intern-sfun/Cinfo-stamp-set! */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2stampzd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_7443, obj_t BgL_oz00_7444, obj_t BgL_vz00_7445)
	{
		{	/* Cfa/cinfo.sch 772 */
			return
				BGl_internzd2sfunzf2Cinfozd2stampzd2setz12z32zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7444), (long) CINT(BgL_vz00_7445));
		}

	}



/* intern-sfun/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_internzd2sfunzf2Cinfozd2approxzf2zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_239)
	{
		{	/* Cfa/cinfo.sch 773 */
			{
				BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_10242;

				{
					obj_t BgL_auxz00_10243;

					{	/* Cfa/cinfo.sch 773 */
						BgL_objectz00_bglt BgL_tmpz00_10244;

						BgL_tmpz00_10244 = ((BgL_objectz00_bglt) BgL_oz00_239);
						BgL_auxz00_10243 = BGL_OBJECT_WIDENING(BgL_tmpz00_10244);
					}
					BgL_auxz00_10242 =
						((BgL_internzd2sfunzf2cinfoz20_bglt) BgL_auxz00_10243);
				}
				return
					(((BgL_internzd2sfunzf2cinfoz20_bglt) COBJECT(BgL_auxz00_10242))->
					BgL_approxz00);
			}
		}

	}



/* &intern-sfun/Cinfo-approx */
	BgL_approxz00_bglt
		BGl_z62internzd2sfunzf2Cinfozd2approxz90zzcfa_approxz00(obj_t
		BgL_envz00_7446, obj_t BgL_oz00_7447)
	{
		{	/* Cfa/cinfo.sch 773 */
			return
				BGl_internzd2sfunzf2Cinfozd2approxzf2zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7447));
		}

	}



/* intern-sfun/Cinfo-polymorphic? */
	BGL_EXPORTED_DEF bool_t
		BGl_internzd2sfunzf2Cinfozd2polymorphiczf3z01zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_242)
	{
		{	/* Cfa/cinfo.sch 775 */
			{
				BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_10251;

				{
					obj_t BgL_auxz00_10252;

					{	/* Cfa/cinfo.sch 775 */
						BgL_objectz00_bglt BgL_tmpz00_10253;

						BgL_tmpz00_10253 = ((BgL_objectz00_bglt) BgL_oz00_242);
						BgL_auxz00_10252 = BGL_OBJECT_WIDENING(BgL_tmpz00_10253);
					}
					BgL_auxz00_10251 =
						((BgL_internzd2sfunzf2cinfoz20_bglt) BgL_auxz00_10252);
				}
				return
					(((BgL_internzd2sfunzf2cinfoz20_bglt) COBJECT(BgL_auxz00_10251))->
					BgL_polymorphiczf3zf3);
			}
		}

	}



/* &intern-sfun/Cinfo-polymorphic? */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2polymorphiczf3z63zzcfa_approxz00(obj_t
		BgL_envz00_7448, obj_t BgL_oz00_7449)
	{
		{	/* Cfa/cinfo.sch 775 */
			return
				BBOOL(BGl_internzd2sfunzf2Cinfozd2polymorphiczf3z01zzcfa_approxz00(
					((BgL_sfunz00_bglt) BgL_oz00_7449)));
		}

	}



/* intern-sfun/Cinfo-polymorphic?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2polymorphiczf3zd2setz12zc1zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_243, bool_t BgL_vz00_244)
	{
		{	/* Cfa/cinfo.sch 776 */
			{
				BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_10261;

				{
					obj_t BgL_auxz00_10262;

					{	/* Cfa/cinfo.sch 776 */
						BgL_objectz00_bglt BgL_tmpz00_10263;

						BgL_tmpz00_10263 = ((BgL_objectz00_bglt) BgL_oz00_243);
						BgL_auxz00_10262 = BGL_OBJECT_WIDENING(BgL_tmpz00_10263);
					}
					BgL_auxz00_10261 =
						((BgL_internzd2sfunzf2cinfoz20_bglt) BgL_auxz00_10262);
				}
				return
					((((BgL_internzd2sfunzf2cinfoz20_bglt) COBJECT(BgL_auxz00_10261))->
						BgL_polymorphiczf3zf3) = ((bool_t) BgL_vz00_244), BUNSPEC);
			}
		}

	}



/* &intern-sfun/Cinfo-polymorphic?-set! */
	obj_t
		BGl_z62internzd2sfunzf2Cinfozd2polymorphiczf3zd2setz12za3zzcfa_approxz00
		(obj_t BgL_envz00_7450, obj_t BgL_oz00_7451, obj_t BgL_vz00_7452)
	{
		{	/* Cfa/cinfo.sch 776 */
			return
				BGl_internzd2sfunzf2Cinfozd2polymorphiczf3zd2setz12zc1zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7451), CBOOL(BgL_vz00_7452));
		}

	}



/* intern-sfun/Cinfo-stackable */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2stackablezf2zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_245)
	{
		{	/* Cfa/cinfo.sch 777 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_245)))->BgL_stackablez00);
		}

	}



/* &intern-sfun/Cinfo-stackable */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2stackablez90zzcfa_approxz00(obj_t
		BgL_envz00_7453, obj_t BgL_oz00_7454)
	{
		{	/* Cfa/cinfo.sch 777 */
			return
				BGl_internzd2sfunzf2Cinfozd2stackablezf2zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7454));
		}

	}



/* intern-sfun/Cinfo-stackable-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2stackablezd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_246, obj_t BgL_vz00_247)
	{
		{	/* Cfa/cinfo.sch 778 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_246)))->BgL_stackablez00) =
				((obj_t) BgL_vz00_247), BUNSPEC);
		}

	}



/* &intern-sfun/Cinfo-stackable-set! */
	obj_t
		BGl_z62internzd2sfunzf2Cinfozd2stackablezd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_7455, obj_t BgL_oz00_7456, obj_t BgL_vz00_7457)
	{
		{	/* Cfa/cinfo.sch 778 */
			return
				BGl_internzd2sfunzf2Cinfozd2stackablezd2setz12z32zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7456), BgL_vz00_7457);
		}

	}



/* intern-sfun/Cinfo-strength */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2strengthzf2zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_248)
	{
		{	/* Cfa/cinfo.sch 779 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_248)))->BgL_strengthz00);
		}

	}



/* &intern-sfun/Cinfo-strength */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2strengthz90zzcfa_approxz00(obj_t
		BgL_envz00_7458, obj_t BgL_oz00_7459)
	{
		{	/* Cfa/cinfo.sch 779 */
			return
				BGl_internzd2sfunzf2Cinfozd2strengthzf2zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7459));
		}

	}



/* intern-sfun/Cinfo-strength-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2strengthzd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_249, obj_t BgL_vz00_250)
	{
		{	/* Cfa/cinfo.sch 780 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_249)))->BgL_strengthz00) =
				((obj_t) BgL_vz00_250), BUNSPEC);
		}

	}



/* &intern-sfun/Cinfo-strength-set! */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2strengthzd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_7460, obj_t BgL_oz00_7461, obj_t BgL_vz00_7462)
	{
		{	/* Cfa/cinfo.sch 780 */
			return
				BGl_internzd2sfunzf2Cinfozd2strengthzd2setz12z32zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7461), BgL_vz00_7462);
		}

	}



/* intern-sfun/Cinfo-the-closure-global */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2thezd2closurezd2globalzf2zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_251)
	{
		{	/* Cfa/cinfo.sch 781 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_251)))->BgL_thezd2closurezd2globalz00);
		}

	}



/* &intern-sfun/Cinfo-the-closure-global */
	obj_t
		BGl_z62internzd2sfunzf2Cinfozd2thezd2closurezd2globalz90zzcfa_approxz00
		(obj_t BgL_envz00_7463, obj_t BgL_oz00_7464)
	{
		{	/* Cfa/cinfo.sch 781 */
			return
				BGl_internzd2sfunzf2Cinfozd2thezd2closurezd2globalzf2zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7464));
		}

	}



/* intern-sfun/Cinfo-the-closure-global-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2thezd2closurezd2globalzd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_252, obj_t BgL_vz00_253)
	{
		{	/* Cfa/cinfo.sch 782 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_252)))->
					BgL_thezd2closurezd2globalz00) = ((obj_t) BgL_vz00_253), BUNSPEC);
		}

	}



/* &intern-sfun/Cinfo-the-closure-global-set! */
	obj_t
		BGl_z62internzd2sfunzf2Cinfozd2thezd2closurezd2globalzd2setz12z50zzcfa_approxz00
		(obj_t BgL_envz00_7465, obj_t BgL_oz00_7466, obj_t BgL_vz00_7467)
	{
		{	/* Cfa/cinfo.sch 782 */
			return
				BGl_internzd2sfunzf2Cinfozd2thezd2closurezd2globalzd2setz12z32zzcfa_approxz00
				(((BgL_sfunz00_bglt) BgL_oz00_7466), BgL_vz00_7467);
		}

	}



/* intern-sfun/Cinfo-keys */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2keyszf2zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_254)
	{
		{	/* Cfa/cinfo.sch 783 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_254)))->BgL_keysz00);
		}

	}



/* &intern-sfun/Cinfo-keys */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2keysz90zzcfa_approxz00(obj_t
		BgL_envz00_7468, obj_t BgL_oz00_7469)
	{
		{	/* Cfa/cinfo.sch 783 */
			return
				BGl_internzd2sfunzf2Cinfozd2keyszf2zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7469));
		}

	}



/* intern-sfun/Cinfo-optionals */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2optionalszf2zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_257)
	{
		{	/* Cfa/cinfo.sch 785 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_257)))->BgL_optionalsz00);
		}

	}



/* &intern-sfun/Cinfo-optionals */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2optionalsz90zzcfa_approxz00(obj_t
		BgL_envz00_7470, obj_t BgL_oz00_7471)
	{
		{	/* Cfa/cinfo.sch 785 */
			return
				BGl_internzd2sfunzf2Cinfozd2optionalszf2zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7471));
		}

	}



/* intern-sfun/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2loczf2zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_260)
	{
		{	/* Cfa/cinfo.sch 787 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_260)))->BgL_locz00);
		}

	}



/* &intern-sfun/Cinfo-loc */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2locz90zzcfa_approxz00(obj_t
		BgL_envz00_7472, obj_t BgL_oz00_7473)
	{
		{	/* Cfa/cinfo.sch 787 */
			return
				BGl_internzd2sfunzf2Cinfozd2loczf2zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7473));
		}

	}



/* intern-sfun/Cinfo-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2loczd2setz12z32zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_261, obj_t BgL_vz00_262)
	{
		{	/* Cfa/cinfo.sch 788 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_261)))->BgL_locz00) =
				((obj_t) BgL_vz00_262), BUNSPEC);
		}

	}



/* &intern-sfun/Cinfo-loc-set! */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2loczd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_7474, obj_t BgL_oz00_7475, obj_t BgL_vz00_7476)
	{
		{	/* Cfa/cinfo.sch 788 */
			return
				BGl_internzd2sfunzf2Cinfozd2loczd2setz12z32zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7475), BgL_vz00_7476);
		}

	}



/* intern-sfun/Cinfo-dsssl-keywords */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2dssslzd2keywordsz20zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_263)
	{
		{	/* Cfa/cinfo.sch 789 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_263)))->BgL_dssslzd2keywordszd2);
		}

	}



/* &intern-sfun/Cinfo-dsssl-keywords */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2dssslzd2keywordsz42zzcfa_approxz00(obj_t
		BgL_envz00_7477, obj_t BgL_oz00_7478)
	{
		{	/* Cfa/cinfo.sch 789 */
			return
				BGl_internzd2sfunzf2Cinfozd2dssslzd2keywordsz20zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7478));
		}

	}



/* intern-sfun/Cinfo-dsssl-keywords-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2dssslzd2keywordszd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_264, obj_t BgL_vz00_265)
	{
		{	/* Cfa/cinfo.sch 790 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_264)))->BgL_dssslzd2keywordszd2) =
				((obj_t) BgL_vz00_265), BUNSPEC);
		}

	}



/* &intern-sfun/Cinfo-dsssl-keywords-set! */
	obj_t
		BGl_z62internzd2sfunzf2Cinfozd2dssslzd2keywordszd2setz12z82zzcfa_approxz00
		(obj_t BgL_envz00_7479, obj_t BgL_oz00_7480, obj_t BgL_vz00_7481)
	{
		{	/* Cfa/cinfo.sch 790 */
			return
				BGl_internzd2sfunzf2Cinfozd2dssslzd2keywordszd2setz12ze0zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7480), BgL_vz00_7481);
		}

	}



/* intern-sfun/Cinfo-class */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2classzf2zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_266)
	{
		{	/* Cfa/cinfo.sch 791 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_266)))->BgL_classz00);
		}

	}



/* &intern-sfun/Cinfo-class */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2classz90zzcfa_approxz00(obj_t
		BgL_envz00_7482, obj_t BgL_oz00_7483)
	{
		{	/* Cfa/cinfo.sch 791 */
			return
				BGl_internzd2sfunzf2Cinfozd2classzf2zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7483));
		}

	}



/* intern-sfun/Cinfo-class-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2classzd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_267, obj_t BgL_vz00_268)
	{
		{	/* Cfa/cinfo.sch 792 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_267)))->BgL_classz00) =
				((obj_t) BgL_vz00_268), BUNSPEC);
		}

	}



/* &intern-sfun/Cinfo-class-set! */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2classzd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_7484, obj_t BgL_oz00_7485, obj_t BgL_vz00_7486)
	{
		{	/* Cfa/cinfo.sch 792 */
			return
				BGl_internzd2sfunzf2Cinfozd2classzd2setz12z32zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7485), BgL_vz00_7486);
		}

	}



/* intern-sfun/Cinfo-body */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2bodyzf2zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_269)
	{
		{	/* Cfa/cinfo.sch 793 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_269)))->BgL_bodyz00);
		}

	}



/* &intern-sfun/Cinfo-body */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2bodyz90zzcfa_approxz00(obj_t
		BgL_envz00_7487, obj_t BgL_oz00_7488)
	{
		{	/* Cfa/cinfo.sch 793 */
			return
				BGl_internzd2sfunzf2Cinfozd2bodyzf2zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7488));
		}

	}



/* intern-sfun/Cinfo-body-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2bodyzd2setz12z32zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_270, obj_t BgL_vz00_271)
	{
		{	/* Cfa/cinfo.sch 794 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_270)))->BgL_bodyz00) =
				((obj_t) BgL_vz00_271), BUNSPEC);
		}

	}



/* &intern-sfun/Cinfo-body-set! */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2bodyzd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_7489, obj_t BgL_oz00_7490, obj_t BgL_vz00_7491)
	{
		{	/* Cfa/cinfo.sch 794 */
			return
				BGl_internzd2sfunzf2Cinfozd2bodyzd2setz12z32zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7490), BgL_vz00_7491);
		}

	}



/* intern-sfun/Cinfo-args-name */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2argszd2namez20zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_272)
	{
		{	/* Cfa/cinfo.sch 795 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_272)))->BgL_argszd2namezd2);
		}

	}



/* &intern-sfun/Cinfo-args-name */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2argszd2namez42zzcfa_approxz00(obj_t
		BgL_envz00_7492, obj_t BgL_oz00_7493)
	{
		{	/* Cfa/cinfo.sch 795 */
			return
				BGl_internzd2sfunzf2Cinfozd2argszd2namez20zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7493));
		}

	}



/* intern-sfun/Cinfo-args */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2argszf2zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_275)
	{
		{	/* Cfa/cinfo.sch 797 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_275)))->BgL_argsz00);
		}

	}



/* &intern-sfun/Cinfo-args */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2argsz90zzcfa_approxz00(obj_t
		BgL_envz00_7494, obj_t BgL_oz00_7495)
	{
		{	/* Cfa/cinfo.sch 797 */
			return
				BGl_internzd2sfunzf2Cinfozd2argszf2zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7495));
		}

	}



/* intern-sfun/Cinfo-args-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2argszd2setz12z32zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_276, obj_t BgL_vz00_277)
	{
		{	/* Cfa/cinfo.sch 798 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_276)))->BgL_argsz00) =
				((obj_t) BgL_vz00_277), BUNSPEC);
		}

	}



/* &intern-sfun/Cinfo-args-set! */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2argszd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_7496, obj_t BgL_oz00_7497, obj_t BgL_vz00_7498)
	{
		{	/* Cfa/cinfo.sch 798 */
			return
				BGl_internzd2sfunzf2Cinfozd2argszd2setz12z32zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7497), BgL_vz00_7498);
		}

	}



/* intern-sfun/Cinfo-property */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2propertyzf2zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_278)
	{
		{	/* Cfa/cinfo.sch 799 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_278)))->BgL_propertyz00);
		}

	}



/* &intern-sfun/Cinfo-property */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2propertyz90zzcfa_approxz00(obj_t
		BgL_envz00_7499, obj_t BgL_oz00_7500)
	{
		{	/* Cfa/cinfo.sch 799 */
			return
				BGl_internzd2sfunzf2Cinfozd2propertyzf2zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7500));
		}

	}



/* intern-sfun/Cinfo-property-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2propertyzd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_279, obj_t BgL_vz00_280)
	{
		{	/* Cfa/cinfo.sch 800 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_279)))->BgL_propertyz00) =
				((obj_t) BgL_vz00_280), BUNSPEC);
		}

	}



/* &intern-sfun/Cinfo-property-set! */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2propertyzd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_7501, obj_t BgL_oz00_7502, obj_t BgL_vz00_7503)
	{
		{	/* Cfa/cinfo.sch 800 */
			return
				BGl_internzd2sfunzf2Cinfozd2propertyzd2setz12z32zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7502), BgL_vz00_7503);
		}

	}



/* intern-sfun/Cinfo-args-retescape */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2argszd2retescapez20zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_281)
	{
		{	/* Cfa/cinfo.sch 801 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_281)))->BgL_argszd2retescapezd2);
		}

	}



/* &intern-sfun/Cinfo-args-retescape */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2argszd2retescapez42zzcfa_approxz00(obj_t
		BgL_envz00_7504, obj_t BgL_oz00_7505)
	{
		{	/* Cfa/cinfo.sch 801 */
			return
				BGl_internzd2sfunzf2Cinfozd2argszd2retescapez20zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7505));
		}

	}



/* intern-sfun/Cinfo-args-retescape-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2argszd2retescapezd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_282, obj_t BgL_vz00_283)
	{
		{	/* Cfa/cinfo.sch 802 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_282)))->BgL_argszd2retescapezd2) =
				((obj_t) BgL_vz00_283), BUNSPEC);
		}

	}



/* &intern-sfun/Cinfo-args-retescape-set! */
	obj_t
		BGl_z62internzd2sfunzf2Cinfozd2argszd2retescapezd2setz12z82zzcfa_approxz00
		(obj_t BgL_envz00_7506, obj_t BgL_oz00_7507, obj_t BgL_vz00_7508)
	{
		{	/* Cfa/cinfo.sch 802 */
			return
				BGl_internzd2sfunzf2Cinfozd2argszd2retescapezd2setz12ze0zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7507), BgL_vz00_7508);
		}

	}



/* intern-sfun/Cinfo-args-noescape */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2argszd2noescapez20zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_284)
	{
		{	/* Cfa/cinfo.sch 803 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_284)))->BgL_argszd2noescapezd2);
		}

	}



/* &intern-sfun/Cinfo-args-noescape */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2argszd2noescapez42zzcfa_approxz00(obj_t
		BgL_envz00_7509, obj_t BgL_oz00_7510)
	{
		{	/* Cfa/cinfo.sch 803 */
			return
				BGl_internzd2sfunzf2Cinfozd2argszd2noescapez20zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7510));
		}

	}



/* intern-sfun/Cinfo-args-noescape-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2argszd2noescapezd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_285, obj_t BgL_vz00_286)
	{
		{	/* Cfa/cinfo.sch 804 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_285)))->BgL_argszd2noescapezd2) =
				((obj_t) BgL_vz00_286), BUNSPEC);
		}

	}



/* &intern-sfun/Cinfo-args-noescape-set! */
	obj_t
		BGl_z62internzd2sfunzf2Cinfozd2argszd2noescapezd2setz12z82zzcfa_approxz00
		(obj_t BgL_envz00_7511, obj_t BgL_oz00_7512, obj_t BgL_vz00_7513)
	{
		{	/* Cfa/cinfo.sch 804 */
			return
				BGl_internzd2sfunzf2Cinfozd2argszd2noescapezd2setz12ze0zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7512), BgL_vz00_7513);
		}

	}



/* intern-sfun/Cinfo-failsafe */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2failsafezf2zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_287)
	{
		{	/* Cfa/cinfo.sch 805 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_287)))->BgL_failsafez00);
		}

	}



/* &intern-sfun/Cinfo-failsafe */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2failsafez90zzcfa_approxz00(obj_t
		BgL_envz00_7514, obj_t BgL_oz00_7515)
	{
		{	/* Cfa/cinfo.sch 805 */
			return
				BGl_internzd2sfunzf2Cinfozd2failsafezf2zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7515));
		}

	}



/* intern-sfun/Cinfo-failsafe-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2failsafezd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_288, obj_t BgL_vz00_289)
	{
		{	/* Cfa/cinfo.sch 806 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_288)))->BgL_failsafez00) =
				((obj_t) BgL_vz00_289), BUNSPEC);
		}

	}



/* &intern-sfun/Cinfo-failsafe-set! */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2failsafezd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_7516, obj_t BgL_oz00_7517, obj_t BgL_vz00_7518)
	{
		{	/* Cfa/cinfo.sch 806 */
			return
				BGl_internzd2sfunzf2Cinfozd2failsafezd2setz12z32zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7517), BgL_vz00_7518);
		}

	}



/* intern-sfun/Cinfo-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2effectzf2zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_290)
	{
		{	/* Cfa/cinfo.sch 807 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_290)))->BgL_effectz00);
		}

	}



/* &intern-sfun/Cinfo-effect */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2effectz90zzcfa_approxz00(obj_t
		BgL_envz00_7519, obj_t BgL_oz00_7520)
	{
		{	/* Cfa/cinfo.sch 807 */
			return
				BGl_internzd2sfunzf2Cinfozd2effectzf2zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7520));
		}

	}



/* intern-sfun/Cinfo-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2effectzd2setz12z32zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_291, obj_t BgL_vz00_292)
	{
		{	/* Cfa/cinfo.sch 808 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_291)))->BgL_effectz00) =
				((obj_t) BgL_vz00_292), BUNSPEC);
		}

	}



/* &intern-sfun/Cinfo-effect-set! */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2effectzd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_7521, obj_t BgL_oz00_7522, obj_t BgL_vz00_7523)
	{
		{	/* Cfa/cinfo.sch 808 */
			return
				BGl_internzd2sfunzf2Cinfozd2effectzd2setz12z32zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7522), BgL_vz00_7523);
		}

	}



/* intern-sfun/Cinfo-the-closure */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2thezd2closurez20zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_293)
	{
		{	/* Cfa/cinfo.sch 809 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_293)))->BgL_thezd2closurezd2);
		}

	}



/* &intern-sfun/Cinfo-the-closure */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2thezd2closurez42zzcfa_approxz00(obj_t
		BgL_envz00_7524, obj_t BgL_oz00_7525)
	{
		{	/* Cfa/cinfo.sch 809 */
			return
				BGl_internzd2sfunzf2Cinfozd2thezd2closurez20zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7525));
		}

	}



/* intern-sfun/Cinfo-the-closure-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2thezd2closurezd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_294, obj_t BgL_vz00_295)
	{
		{	/* Cfa/cinfo.sch 810 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_294)))->BgL_thezd2closurezd2) =
				((obj_t) BgL_vz00_295), BUNSPEC);
		}

	}



/* &intern-sfun/Cinfo-the-closure-set! */
	obj_t
		BGl_z62internzd2sfunzf2Cinfozd2thezd2closurezd2setz12z82zzcfa_approxz00
		(obj_t BgL_envz00_7526, obj_t BgL_oz00_7527, obj_t BgL_vz00_7528)
	{
		{	/* Cfa/cinfo.sch 810 */
			return
				BGl_internzd2sfunzf2Cinfozd2thezd2closurezd2setz12ze0zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7527), BgL_vz00_7528);
		}

	}



/* intern-sfun/Cinfo-top? */
	BGL_EXPORTED_DEF bool_t
		BGl_internzd2sfunzf2Cinfozd2topzf3z01zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_296)
	{
		{	/* Cfa/cinfo.sch 811 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_296)))->BgL_topzf3zf3);
		}

	}



/* &intern-sfun/Cinfo-top? */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2topzf3z63zzcfa_approxz00(obj_t
		BgL_envz00_7529, obj_t BgL_oz00_7530)
	{
		{	/* Cfa/cinfo.sch 811 */
			return
				BBOOL(BGl_internzd2sfunzf2Cinfozd2topzf3z01zzcfa_approxz00(
					((BgL_sfunz00_bglt) BgL_oz00_7530)));
		}

	}



/* intern-sfun/Cinfo-top?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2topzf3zd2setz12zc1zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_297, bool_t BgL_vz00_298)
	{
		{	/* Cfa/cinfo.sch 812 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_297)))->BgL_topzf3zf3) =
				((bool_t) BgL_vz00_298), BUNSPEC);
		}

	}



/* &intern-sfun/Cinfo-top?-set! */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2topzf3zd2setz12za3zzcfa_approxz00(obj_t
		BgL_envz00_7531, obj_t BgL_oz00_7532, obj_t BgL_vz00_7533)
	{
		{	/* Cfa/cinfo.sch 812 */
			return
				BGl_internzd2sfunzf2Cinfozd2topzf3zd2setz12zc1zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7532), CBOOL(BgL_vz00_7533));
		}

	}



/* intern-sfun/Cinfo-stack-allocator */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2stackzd2allocatorz20zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_299)
	{
		{	/* Cfa/cinfo.sch 813 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_299)))->BgL_stackzd2allocatorzd2);
		}

	}



/* &intern-sfun/Cinfo-stack-allocator */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2stackzd2allocatorz42zzcfa_approxz00(obj_t
		BgL_envz00_7534, obj_t BgL_oz00_7535)
	{
		{	/* Cfa/cinfo.sch 813 */
			return
				BGl_internzd2sfunzf2Cinfozd2stackzd2allocatorz20zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7535));
		}

	}



/* intern-sfun/Cinfo-stack-allocator-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2stackzd2allocatorzd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_300, obj_t BgL_vz00_301)
	{
		{	/* Cfa/cinfo.sch 814 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_300)))->BgL_stackzd2allocatorzd2) =
				((obj_t) BgL_vz00_301), BUNSPEC);
		}

	}



/* &intern-sfun/Cinfo-stack-allocator-set! */
	obj_t
		BGl_z62internzd2sfunzf2Cinfozd2stackzd2allocatorzd2setz12z82zzcfa_approxz00
		(obj_t BgL_envz00_7536, obj_t BgL_oz00_7537, obj_t BgL_vz00_7538)
	{
		{	/* Cfa/cinfo.sch 814 */
			return
				BGl_internzd2sfunzf2Cinfozd2stackzd2allocatorzd2setz12ze0zzcfa_approxz00
				(((BgL_sfunz00_bglt) BgL_oz00_7537), BgL_vz00_7538);
		}

	}



/* intern-sfun/Cinfo-predicate-of */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2predicatezd2ofz20zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_302)
	{
		{	/* Cfa/cinfo.sch 815 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_302)))->BgL_predicatezd2ofzd2);
		}

	}



/* &intern-sfun/Cinfo-predicate-of */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2predicatezd2ofz42zzcfa_approxz00(obj_t
		BgL_envz00_7539, obj_t BgL_oz00_7540)
	{
		{	/* Cfa/cinfo.sch 815 */
			return
				BGl_internzd2sfunzf2Cinfozd2predicatezd2ofz20zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7540));
		}

	}



/* intern-sfun/Cinfo-predicate-of-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2predicatezd2ofzd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_303, obj_t BgL_vz00_304)
	{
		{	/* Cfa/cinfo.sch 816 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_303)))->BgL_predicatezd2ofzd2) =
				((obj_t) BgL_vz00_304), BUNSPEC);
		}

	}



/* &intern-sfun/Cinfo-predicate-of-set! */
	obj_t
		BGl_z62internzd2sfunzf2Cinfozd2predicatezd2ofzd2setz12z82zzcfa_approxz00
		(obj_t BgL_envz00_7541, obj_t BgL_oz00_7542, obj_t BgL_vz00_7543)
	{
		{	/* Cfa/cinfo.sch 816 */
			return
				BGl_internzd2sfunzf2Cinfozd2predicatezd2ofzd2setz12ze0zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7542), BgL_vz00_7543);
		}

	}



/* intern-sfun/Cinfo-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2sidezd2effectz20zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_305)
	{
		{	/* Cfa/cinfo.sch 817 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_305)))->BgL_sidezd2effectzd2);
		}

	}



/* &intern-sfun/Cinfo-side-effect */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2sidezd2effectz42zzcfa_approxz00(obj_t
		BgL_envz00_7544, obj_t BgL_oz00_7545)
	{
		{	/* Cfa/cinfo.sch 817 */
			return
				BGl_internzd2sfunzf2Cinfozd2sidezd2effectz20zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7545));
		}

	}



/* intern-sfun/Cinfo-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_internzd2sfunzf2Cinfozd2sidezd2effectzd2setz12ze0zzcfa_approxz00
		(BgL_sfunz00_bglt BgL_oz00_306, obj_t BgL_vz00_307)
	{
		{	/* Cfa/cinfo.sch 818 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_306)))->BgL_sidezd2effectzd2) =
				((obj_t) BgL_vz00_307), BUNSPEC);
		}

	}



/* &intern-sfun/Cinfo-side-effect-set! */
	obj_t
		BGl_z62internzd2sfunzf2Cinfozd2sidezd2effectzd2setz12z82zzcfa_approxz00
		(obj_t BgL_envz00_7546, obj_t BgL_oz00_7547, obj_t BgL_vz00_7548)
	{
		{	/* Cfa/cinfo.sch 818 */
			return
				BGl_internzd2sfunzf2Cinfozd2sidezd2effectzd2setz12ze0zzcfa_approxz00(
				((BgL_sfunz00_bglt) BgL_oz00_7547), BgL_vz00_7548);
		}

	}



/* intern-sfun/Cinfo-arity */
	BGL_EXPORTED_DEF long
		BGl_internzd2sfunzf2Cinfozd2arityzf2zzcfa_approxz00(BgL_sfunz00_bglt
		BgL_oz00_308)
	{
		{	/* Cfa/cinfo.sch 819 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_308)))->BgL_arityz00);
		}

	}



/* &intern-sfun/Cinfo-arity */
	obj_t BGl_z62internzd2sfunzf2Cinfozd2arityz90zzcfa_approxz00(obj_t
		BgL_envz00_7549, obj_t BgL_oz00_7550)
	{
		{	/* Cfa/cinfo.sch 819 */
			return
				BINT(BGl_internzd2sfunzf2Cinfozd2arityzf2zzcfa_approxz00(
					((BgL_sfunz00_bglt) BgL_oz00_7550)));
		}

	}



/* make-scnst/Cinfo */
	BGL_EXPORTED_DEF BgL_scnstz00_bglt
		BGl_makezd2scnstzf2Cinfoz20zzcfa_approxz00(obj_t BgL_node1748z00_311,
		obj_t BgL_class1749z00_312, obj_t BgL_loc1750z00_313,
		BgL_approxz00_bglt BgL_approx1751z00_314)
	{
		{	/* Cfa/cinfo.sch 823 */
			{	/* Cfa/cinfo.sch 823 */
				BgL_scnstz00_bglt BgL_new1193z00_8738;

				{	/* Cfa/cinfo.sch 823 */
					BgL_scnstz00_bglt BgL_tmp1191z00_8739;
					BgL_scnstzf2cinfozf2_bglt BgL_wide1192z00_8740;

					{
						BgL_scnstz00_bglt BgL_auxz00_10434;

						{	/* Cfa/cinfo.sch 823 */
							BgL_scnstz00_bglt BgL_new1190z00_8741;

							BgL_new1190z00_8741 =
								((BgL_scnstz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_scnstz00_bgl))));
							{	/* Cfa/cinfo.sch 823 */
								long BgL_arg1654z00_8742;

								BgL_arg1654z00_8742 = BGL_CLASS_NUM(BGl_scnstz00zzast_varz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1190z00_8741),
									BgL_arg1654z00_8742);
							}
							{	/* Cfa/cinfo.sch 823 */
								BgL_objectz00_bglt BgL_tmpz00_10439;

								BgL_tmpz00_10439 = ((BgL_objectz00_bglt) BgL_new1190z00_8741);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10439, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1190z00_8741);
							BgL_auxz00_10434 = BgL_new1190z00_8741;
						}
						BgL_tmp1191z00_8739 = ((BgL_scnstz00_bglt) BgL_auxz00_10434);
					}
					BgL_wide1192z00_8740 =
						((BgL_scnstzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_scnstzf2cinfozf2_bgl))));
					{	/* Cfa/cinfo.sch 823 */
						obj_t BgL_auxz00_10447;
						BgL_objectz00_bglt BgL_tmpz00_10445;

						BgL_auxz00_10447 = ((obj_t) BgL_wide1192z00_8740);
						BgL_tmpz00_10445 = ((BgL_objectz00_bglt) BgL_tmp1191z00_8739);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10445, BgL_auxz00_10447);
					}
					((BgL_objectz00_bglt) BgL_tmp1191z00_8739);
					{	/* Cfa/cinfo.sch 823 */
						long BgL_arg1651z00_8743;

						BgL_arg1651z00_8743 =
							BGL_CLASS_NUM(BGl_scnstzf2Cinfozf2zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1191z00_8739), BgL_arg1651z00_8743);
					}
					BgL_new1193z00_8738 = ((BgL_scnstz00_bglt) BgL_tmp1191z00_8739);
				}
				((((BgL_scnstz00_bglt) COBJECT(
								((BgL_scnstz00_bglt) BgL_new1193z00_8738)))->BgL_nodez00) =
					((obj_t) BgL_node1748z00_311), BUNSPEC);
				((((BgL_scnstz00_bglt) COBJECT(((BgL_scnstz00_bglt)
									BgL_new1193z00_8738)))->BgL_classz00) =
					((obj_t) BgL_class1749z00_312), BUNSPEC);
				((((BgL_scnstz00_bglt) COBJECT(((BgL_scnstz00_bglt)
									BgL_new1193z00_8738)))->BgL_locz00) =
					((obj_t) BgL_loc1750z00_313), BUNSPEC);
				{
					BgL_scnstzf2cinfozf2_bglt BgL_auxz00_10461;

					{
						obj_t BgL_auxz00_10462;

						{	/* Cfa/cinfo.sch 823 */
							BgL_objectz00_bglt BgL_tmpz00_10463;

							BgL_tmpz00_10463 = ((BgL_objectz00_bglt) BgL_new1193z00_8738);
							BgL_auxz00_10462 = BGL_OBJECT_WIDENING(BgL_tmpz00_10463);
						}
						BgL_auxz00_10461 = ((BgL_scnstzf2cinfozf2_bglt) BgL_auxz00_10462);
					}
					((((BgL_scnstzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10461))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1751z00_314), BUNSPEC);
				}
				return BgL_new1193z00_8738;
			}
		}

	}



/* &make-scnst/Cinfo */
	BgL_scnstz00_bglt BGl_z62makezd2scnstzf2Cinfoz42zzcfa_approxz00(obj_t
		BgL_envz00_7551, obj_t BgL_node1748z00_7552, obj_t BgL_class1749z00_7553,
		obj_t BgL_loc1750z00_7554, obj_t BgL_approx1751z00_7555)
	{
		{	/* Cfa/cinfo.sch 823 */
			return
				BGl_makezd2scnstzf2Cinfoz20zzcfa_approxz00(BgL_node1748z00_7552,
				BgL_class1749z00_7553, BgL_loc1750z00_7554,
				((BgL_approxz00_bglt) BgL_approx1751z00_7555));
		}

	}



/* scnst/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_scnstzf2Cinfozf3z01zzcfa_approxz00(obj_t
		BgL_objz00_315)
	{
		{	/* Cfa/cinfo.sch 824 */
			{	/* Cfa/cinfo.sch 824 */
				obj_t BgL_classz00_8744;

				BgL_classz00_8744 = BGl_scnstzf2Cinfozf2zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_315))
					{	/* Cfa/cinfo.sch 824 */
						BgL_objectz00_bglt BgL_arg1807z00_8745;

						BgL_arg1807z00_8745 = (BgL_objectz00_bglt) (BgL_objz00_315);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 824 */
								long BgL_idxz00_8746;

								BgL_idxz00_8746 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8745);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8746 + 3L)) == BgL_classz00_8744);
							}
						else
							{	/* Cfa/cinfo.sch 824 */
								bool_t BgL_res2171z00_8749;

								{	/* Cfa/cinfo.sch 824 */
									obj_t BgL_oclassz00_8750;

									{	/* Cfa/cinfo.sch 824 */
										obj_t BgL_arg1815z00_8751;
										long BgL_arg1816z00_8752;

										BgL_arg1815z00_8751 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 824 */
											long BgL_arg1817z00_8753;

											BgL_arg1817z00_8753 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8745);
											BgL_arg1816z00_8752 = (BgL_arg1817z00_8753 - OBJECT_TYPE);
										}
										BgL_oclassz00_8750 =
											VECTOR_REF(BgL_arg1815z00_8751, BgL_arg1816z00_8752);
									}
									{	/* Cfa/cinfo.sch 824 */
										bool_t BgL__ortest_1115z00_8754;

										BgL__ortest_1115z00_8754 =
											(BgL_classz00_8744 == BgL_oclassz00_8750);
										if (BgL__ortest_1115z00_8754)
											{	/* Cfa/cinfo.sch 824 */
												BgL_res2171z00_8749 = BgL__ortest_1115z00_8754;
											}
										else
											{	/* Cfa/cinfo.sch 824 */
												long BgL_odepthz00_8755;

												{	/* Cfa/cinfo.sch 824 */
													obj_t BgL_arg1804z00_8756;

													BgL_arg1804z00_8756 = (BgL_oclassz00_8750);
													BgL_odepthz00_8755 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8756);
												}
												if ((3L < BgL_odepthz00_8755))
													{	/* Cfa/cinfo.sch 824 */
														obj_t BgL_arg1802z00_8757;

														{	/* Cfa/cinfo.sch 824 */
															obj_t BgL_arg1803z00_8758;

															BgL_arg1803z00_8758 = (BgL_oclassz00_8750);
															BgL_arg1802z00_8757 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8758,
																3L);
														}
														BgL_res2171z00_8749 =
															(BgL_arg1802z00_8757 == BgL_classz00_8744);
													}
												else
													{	/* Cfa/cinfo.sch 824 */
														BgL_res2171z00_8749 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2171z00_8749;
							}
					}
				else
					{	/* Cfa/cinfo.sch 824 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &scnst/Cinfo? */
	obj_t BGl_z62scnstzf2Cinfozf3z63zzcfa_approxz00(obj_t BgL_envz00_7556,
		obj_t BgL_objz00_7557)
	{
		{	/* Cfa/cinfo.sch 824 */
			return BBOOL(BGl_scnstzf2Cinfozf3z01zzcfa_approxz00(BgL_objz00_7557));
		}

	}



/* scnst/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_scnstz00_bglt
		BGl_scnstzf2Cinfozd2nilz20zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 825 */
			{	/* Cfa/cinfo.sch 825 */
				obj_t BgL_classz00_4957;

				BgL_classz00_4957 = BGl_scnstzf2Cinfozf2zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 825 */
					obj_t BgL__ortest_1117z00_4958;

					BgL__ortest_1117z00_4958 = BGL_CLASS_NIL(BgL_classz00_4957);
					if (CBOOL(BgL__ortest_1117z00_4958))
						{	/* Cfa/cinfo.sch 825 */
							return ((BgL_scnstz00_bglt) BgL__ortest_1117z00_4958);
						}
					else
						{	/* Cfa/cinfo.sch 825 */
							return
								((BgL_scnstz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4957));
						}
				}
			}
		}

	}



/* &scnst/Cinfo-nil */
	BgL_scnstz00_bglt BGl_z62scnstzf2Cinfozd2nilz42zzcfa_approxz00(obj_t
		BgL_envz00_7558)
	{
		{	/* Cfa/cinfo.sch 825 */
			return BGl_scnstzf2Cinfozd2nilz20zzcfa_approxz00();
		}

	}



/* scnst/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_scnstzf2Cinfozd2approxz20zzcfa_approxz00(BgL_scnstz00_bglt BgL_oz00_316)
	{
		{	/* Cfa/cinfo.sch 826 */
			{
				BgL_scnstzf2cinfozf2_bglt BgL_auxz00_10501;

				{
					obj_t BgL_auxz00_10502;

					{	/* Cfa/cinfo.sch 826 */
						BgL_objectz00_bglt BgL_tmpz00_10503;

						BgL_tmpz00_10503 = ((BgL_objectz00_bglt) BgL_oz00_316);
						BgL_auxz00_10502 = BGL_OBJECT_WIDENING(BgL_tmpz00_10503);
					}
					BgL_auxz00_10501 = ((BgL_scnstzf2cinfozf2_bglt) BgL_auxz00_10502);
				}
				return
					(((BgL_scnstzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10501))->
					BgL_approxz00);
			}
		}

	}



/* &scnst/Cinfo-approx */
	BgL_approxz00_bglt BGl_z62scnstzf2Cinfozd2approxz42zzcfa_approxz00(obj_t
		BgL_envz00_7559, obj_t BgL_oz00_7560)
	{
		{	/* Cfa/cinfo.sch 826 */
			return
				BGl_scnstzf2Cinfozd2approxz20zzcfa_approxz00(
				((BgL_scnstz00_bglt) BgL_oz00_7560));
		}

	}



/* scnst/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_scnstzf2Cinfozd2locz20zzcfa_approxz00(BgL_scnstz00_bglt BgL_oz00_319)
	{
		{	/* Cfa/cinfo.sch 828 */
			return
				(((BgL_scnstz00_bglt) COBJECT(
						((BgL_scnstz00_bglt) BgL_oz00_319)))->BgL_locz00);
		}

	}



/* &scnst/Cinfo-loc */
	obj_t BGl_z62scnstzf2Cinfozd2locz42zzcfa_approxz00(obj_t BgL_envz00_7561,
		obj_t BgL_oz00_7562)
	{
		{	/* Cfa/cinfo.sch 828 */
			return
				BGl_scnstzf2Cinfozd2locz20zzcfa_approxz00(
				((BgL_scnstz00_bglt) BgL_oz00_7562));
		}

	}



/* scnst/Cinfo-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_scnstzf2Cinfozd2loczd2setz12ze0zzcfa_approxz00(BgL_scnstz00_bglt
		BgL_oz00_320, obj_t BgL_vz00_321)
	{
		{	/* Cfa/cinfo.sch 829 */
			return
				((((BgL_scnstz00_bglt) COBJECT(
							((BgL_scnstz00_bglt) BgL_oz00_320)))->BgL_locz00) =
				((obj_t) BgL_vz00_321), BUNSPEC);
		}

	}



/* &scnst/Cinfo-loc-set! */
	obj_t BGl_z62scnstzf2Cinfozd2loczd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_7563, obj_t BgL_oz00_7564, obj_t BgL_vz00_7565)
	{
		{	/* Cfa/cinfo.sch 829 */
			return
				BGl_scnstzf2Cinfozd2loczd2setz12ze0zzcfa_approxz00(
				((BgL_scnstz00_bglt) BgL_oz00_7564), BgL_vz00_7565);
		}

	}



/* scnst/Cinfo-class */
	BGL_EXPORTED_DEF obj_t
		BGl_scnstzf2Cinfozd2classz20zzcfa_approxz00(BgL_scnstz00_bglt BgL_oz00_322)
	{
		{	/* Cfa/cinfo.sch 830 */
			return
				(((BgL_scnstz00_bglt) COBJECT(
						((BgL_scnstz00_bglt) BgL_oz00_322)))->BgL_classz00);
		}

	}



/* &scnst/Cinfo-class */
	obj_t BGl_z62scnstzf2Cinfozd2classz42zzcfa_approxz00(obj_t BgL_envz00_7566,
		obj_t BgL_oz00_7567)
	{
		{	/* Cfa/cinfo.sch 830 */
			return
				BGl_scnstzf2Cinfozd2classz20zzcfa_approxz00(
				((BgL_scnstz00_bglt) BgL_oz00_7567));
		}

	}



/* scnst/Cinfo-class-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_scnstzf2Cinfozd2classzd2setz12ze0zzcfa_approxz00(BgL_scnstz00_bglt
		BgL_oz00_323, obj_t BgL_vz00_324)
	{
		{	/* Cfa/cinfo.sch 831 */
			return
				((((BgL_scnstz00_bglt) COBJECT(
							((BgL_scnstz00_bglt) BgL_oz00_323)))->BgL_classz00) =
				((obj_t) BgL_vz00_324), BUNSPEC);
		}

	}



/* &scnst/Cinfo-class-set! */
	obj_t BGl_z62scnstzf2Cinfozd2classzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_7568, obj_t BgL_oz00_7569, obj_t BgL_vz00_7570)
	{
		{	/* Cfa/cinfo.sch 831 */
			return
				BGl_scnstzf2Cinfozd2classzd2setz12ze0zzcfa_approxz00(
				((BgL_scnstz00_bglt) BgL_oz00_7569), BgL_vz00_7570);
		}

	}



/* scnst/Cinfo-node */
	BGL_EXPORTED_DEF obj_t
		BGl_scnstzf2Cinfozd2nodez20zzcfa_approxz00(BgL_scnstz00_bglt BgL_oz00_325)
	{
		{	/* Cfa/cinfo.sch 832 */
			return
				(((BgL_scnstz00_bglt) COBJECT(
						((BgL_scnstz00_bglt) BgL_oz00_325)))->BgL_nodez00);
		}

	}



/* &scnst/Cinfo-node */
	obj_t BGl_z62scnstzf2Cinfozd2nodez42zzcfa_approxz00(obj_t BgL_envz00_7571,
		obj_t BgL_oz00_7572)
	{
		{	/* Cfa/cinfo.sch 832 */
			return
				BGl_scnstzf2Cinfozd2nodez20zzcfa_approxz00(
				((BgL_scnstz00_bglt) BgL_oz00_7572));
		}

	}



/* make-pre-clo-env */
	BGL_EXPORTED_DEF BgL_svarz00_bglt
		BGl_makezd2prezd2clozd2envzd2zzcfa_approxz00(obj_t BgL_loc1746z00_328)
	{
		{	/* Cfa/cinfo.sch 836 */
			{	/* Cfa/cinfo.sch 836 */
				BgL_svarz00_bglt BgL_new1197z00_8759;

				{	/* Cfa/cinfo.sch 836 */
					BgL_svarz00_bglt BgL_tmp1195z00_8760;
					BgL_prezd2clozd2envz00_bglt BgL_wide1196z00_8761;

					{
						BgL_svarz00_bglt BgL_auxz00_10530;

						{	/* Cfa/cinfo.sch 836 */
							BgL_svarz00_bglt BgL_new1194z00_8762;

							BgL_new1194z00_8762 =
								((BgL_svarz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_svarz00_bgl))));
							{	/* Cfa/cinfo.sch 836 */
								long BgL_arg1663z00_8763;

								BgL_arg1663z00_8763 = BGL_CLASS_NUM(BGl_svarz00zzast_varz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1194z00_8762),
									BgL_arg1663z00_8763);
							}
							{	/* Cfa/cinfo.sch 836 */
								BgL_objectz00_bglt BgL_tmpz00_10535;

								BgL_tmpz00_10535 = ((BgL_objectz00_bglt) BgL_new1194z00_8762);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10535, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1194z00_8762);
							BgL_auxz00_10530 = BgL_new1194z00_8762;
						}
						BgL_tmp1195z00_8760 = ((BgL_svarz00_bglt) BgL_auxz00_10530);
					}
					BgL_wide1196z00_8761 =
						((BgL_prezd2clozd2envz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_prezd2clozd2envz00_bgl))));
					{	/* Cfa/cinfo.sch 836 */
						obj_t BgL_auxz00_10543;
						BgL_objectz00_bglt BgL_tmpz00_10541;

						BgL_auxz00_10543 = ((obj_t) BgL_wide1196z00_8761);
						BgL_tmpz00_10541 = ((BgL_objectz00_bglt) BgL_tmp1195z00_8760);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10541, BgL_auxz00_10543);
					}
					((BgL_objectz00_bglt) BgL_tmp1195z00_8760);
					{	/* Cfa/cinfo.sch 836 */
						long BgL_arg1661z00_8764;

						BgL_arg1661z00_8764 =
							BGL_CLASS_NUM(BGl_prezd2clozd2envz00zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1195z00_8760), BgL_arg1661z00_8764);
					}
					BgL_new1197z00_8759 = ((BgL_svarz00_bglt) BgL_tmp1195z00_8760);
				}
				((((BgL_svarz00_bglt) COBJECT(
								((BgL_svarz00_bglt) BgL_new1197z00_8759)))->BgL_locz00) =
					((obj_t) BgL_loc1746z00_328), BUNSPEC);
				return BgL_new1197z00_8759;
			}
		}

	}



/* &make-pre-clo-env */
	BgL_svarz00_bglt BGl_z62makezd2prezd2clozd2envzb0zzcfa_approxz00(obj_t
		BgL_envz00_7573, obj_t BgL_loc1746z00_7574)
	{
		{	/* Cfa/cinfo.sch 836 */
			return BGl_makezd2prezd2clozd2envzd2zzcfa_approxz00(BgL_loc1746z00_7574);
		}

	}



/* pre-clo-env? */
	BGL_EXPORTED_DEF bool_t BGl_prezd2clozd2envzf3zf3zzcfa_approxz00(obj_t
		BgL_objz00_329)
	{
		{	/* Cfa/cinfo.sch 837 */
			{	/* Cfa/cinfo.sch 837 */
				obj_t BgL_classz00_8765;

				BgL_classz00_8765 = BGl_prezd2clozd2envz00zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_329))
					{	/* Cfa/cinfo.sch 837 */
						BgL_objectz00_bglt BgL_arg1807z00_8766;

						BgL_arg1807z00_8766 = (BgL_objectz00_bglt) (BgL_objz00_329);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 837 */
								long BgL_idxz00_8767;

								BgL_idxz00_8767 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8766);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8767 + 3L)) == BgL_classz00_8765);
							}
						else
							{	/* Cfa/cinfo.sch 837 */
								bool_t BgL_res2172z00_8770;

								{	/* Cfa/cinfo.sch 837 */
									obj_t BgL_oclassz00_8771;

									{	/* Cfa/cinfo.sch 837 */
										obj_t BgL_arg1815z00_8772;
										long BgL_arg1816z00_8773;

										BgL_arg1815z00_8772 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 837 */
											long BgL_arg1817z00_8774;

											BgL_arg1817z00_8774 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8766);
											BgL_arg1816z00_8773 = (BgL_arg1817z00_8774 - OBJECT_TYPE);
										}
										BgL_oclassz00_8771 =
											VECTOR_REF(BgL_arg1815z00_8772, BgL_arg1816z00_8773);
									}
									{	/* Cfa/cinfo.sch 837 */
										bool_t BgL__ortest_1115z00_8775;

										BgL__ortest_1115z00_8775 =
											(BgL_classz00_8765 == BgL_oclassz00_8771);
										if (BgL__ortest_1115z00_8775)
											{	/* Cfa/cinfo.sch 837 */
												BgL_res2172z00_8770 = BgL__ortest_1115z00_8775;
											}
										else
											{	/* Cfa/cinfo.sch 837 */
												long BgL_odepthz00_8776;

												{	/* Cfa/cinfo.sch 837 */
													obj_t BgL_arg1804z00_8777;

													BgL_arg1804z00_8777 = (BgL_oclassz00_8771);
													BgL_odepthz00_8776 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8777);
												}
												if ((3L < BgL_odepthz00_8776))
													{	/* Cfa/cinfo.sch 837 */
														obj_t BgL_arg1802z00_8778;

														{	/* Cfa/cinfo.sch 837 */
															obj_t BgL_arg1803z00_8779;

															BgL_arg1803z00_8779 = (BgL_oclassz00_8771);
															BgL_arg1802z00_8778 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8779,
																3L);
														}
														BgL_res2172z00_8770 =
															(BgL_arg1802z00_8778 == BgL_classz00_8765);
													}
												else
													{	/* Cfa/cinfo.sch 837 */
														BgL_res2172z00_8770 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2172z00_8770;
							}
					}
				else
					{	/* Cfa/cinfo.sch 837 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &pre-clo-env? */
	obj_t BGl_z62prezd2clozd2envzf3z91zzcfa_approxz00(obj_t BgL_envz00_7575,
		obj_t BgL_objz00_7576)
	{
		{	/* Cfa/cinfo.sch 837 */
			return BBOOL(BGl_prezd2clozd2envzf3zf3zzcfa_approxz00(BgL_objz00_7576));
		}

	}



/* pre-clo-env-nil */
	BGL_EXPORTED_DEF BgL_svarz00_bglt
		BGl_prezd2clozd2envzd2nilzd2zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 838 */
			{	/* Cfa/cinfo.sch 838 */
				obj_t BgL_classz00_5008;

				BgL_classz00_5008 = BGl_prezd2clozd2envz00zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 838 */
					obj_t BgL__ortest_1117z00_5009;

					BgL__ortest_1117z00_5009 = BGL_CLASS_NIL(BgL_classz00_5008);
					if (CBOOL(BgL__ortest_1117z00_5009))
						{	/* Cfa/cinfo.sch 838 */
							return ((BgL_svarz00_bglt) BgL__ortest_1117z00_5009);
						}
					else
						{	/* Cfa/cinfo.sch 838 */
							return
								((BgL_svarz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5008));
						}
				}
			}
		}

	}



/* &pre-clo-env-nil */
	BgL_svarz00_bglt BGl_z62prezd2clozd2envzd2nilzb0zzcfa_approxz00(obj_t
		BgL_envz00_7577)
	{
		{	/* Cfa/cinfo.sch 838 */
			return BGl_prezd2clozd2envzd2nilzd2zzcfa_approxz00();
		}

	}



/* pre-clo-env-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_prezd2clozd2envzd2loczd2zzcfa_approxz00(BgL_svarz00_bglt BgL_oz00_330)
	{
		{	/* Cfa/cinfo.sch 839 */
			return
				(((BgL_svarz00_bglt) COBJECT(
						((BgL_svarz00_bglt) BgL_oz00_330)))->BgL_locz00);
		}

	}



/* &pre-clo-env-loc */
	obj_t BGl_z62prezd2clozd2envzd2loczb0zzcfa_approxz00(obj_t BgL_envz00_7578,
		obj_t BgL_oz00_7579)
	{
		{	/* Cfa/cinfo.sch 839 */
			return
				BGl_prezd2clozd2envzd2loczd2zzcfa_approxz00(
				((BgL_svarz00_bglt) BgL_oz00_7579));
		}

	}



/* pre-clo-env-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_prezd2clozd2envzd2loczd2setz12z12zzcfa_approxz00(BgL_svarz00_bglt
		BgL_oz00_331, obj_t BgL_vz00_332)
	{
		{	/* Cfa/cinfo.sch 840 */
			return
				((((BgL_svarz00_bglt) COBJECT(
							((BgL_svarz00_bglt) BgL_oz00_331)))->BgL_locz00) =
				((obj_t) BgL_vz00_332), BUNSPEC);
		}

	}



/* &pre-clo-env-loc-set! */
	obj_t BGl_z62prezd2clozd2envzd2loczd2setz12z70zzcfa_approxz00(obj_t
		BgL_envz00_7580, obj_t BgL_oz00_7581, obj_t BgL_vz00_7582)
	{
		{	/* Cfa/cinfo.sch 840 */
			return
				BGl_prezd2clozd2envzd2loczd2setz12z12zzcfa_approxz00(
				((BgL_svarz00_bglt) BgL_oz00_7581), BgL_vz00_7582);
		}

	}



/* make-svar/Cinfo */
	BGL_EXPORTED_DEF BgL_svarz00_bglt
		BGl_makezd2svarzf2Cinfoz20zzcfa_approxz00(obj_t BgL_loc1741z00_333,
		BgL_approxz00_bglt BgL_approx1742z00_334,
		bool_t BgL_clozd2envzf31743z21_335, long BgL_stamp1744z00_336)
	{
		{	/* Cfa/cinfo.sch 843 */
			{	/* Cfa/cinfo.sch 843 */
				BgL_svarz00_bglt BgL_new1201z00_8780;

				{	/* Cfa/cinfo.sch 843 */
					BgL_svarz00_bglt BgL_tmp1199z00_8781;
					BgL_svarzf2cinfozf2_bglt BgL_wide1200z00_8782;

					{
						BgL_svarz00_bglt BgL_auxz00_10593;

						{	/* Cfa/cinfo.sch 843 */
							BgL_svarz00_bglt BgL_new1198z00_8783;

							BgL_new1198z00_8783 =
								((BgL_svarz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_svarz00_bgl))));
							{	/* Cfa/cinfo.sch 843 */
								long BgL_arg1678z00_8784;

								BgL_arg1678z00_8784 = BGL_CLASS_NUM(BGl_svarz00zzast_varz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1198z00_8783),
									BgL_arg1678z00_8784);
							}
							{	/* Cfa/cinfo.sch 843 */
								BgL_objectz00_bglt BgL_tmpz00_10598;

								BgL_tmpz00_10598 = ((BgL_objectz00_bglt) BgL_new1198z00_8783);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10598, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1198z00_8783);
							BgL_auxz00_10593 = BgL_new1198z00_8783;
						}
						BgL_tmp1199z00_8781 = ((BgL_svarz00_bglt) BgL_auxz00_10593);
					}
					BgL_wide1200z00_8782 =
						((BgL_svarzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_svarzf2cinfozf2_bgl))));
					{	/* Cfa/cinfo.sch 843 */
						obj_t BgL_auxz00_10606;
						BgL_objectz00_bglt BgL_tmpz00_10604;

						BgL_auxz00_10606 = ((obj_t) BgL_wide1200z00_8782);
						BgL_tmpz00_10604 = ((BgL_objectz00_bglt) BgL_tmp1199z00_8781);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10604, BgL_auxz00_10606);
					}
					((BgL_objectz00_bglt) BgL_tmp1199z00_8781);
					{	/* Cfa/cinfo.sch 843 */
						long BgL_arg1675z00_8785;

						BgL_arg1675z00_8785 =
							BGL_CLASS_NUM(BGl_svarzf2Cinfozf2zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1199z00_8781), BgL_arg1675z00_8785);
					}
					BgL_new1201z00_8780 = ((BgL_svarz00_bglt) BgL_tmp1199z00_8781);
				}
				((((BgL_svarz00_bglt) COBJECT(
								((BgL_svarz00_bglt) BgL_new1201z00_8780)))->BgL_locz00) =
					((obj_t) BgL_loc1741z00_333), BUNSPEC);
				{
					BgL_svarzf2cinfozf2_bglt BgL_auxz00_10616;

					{
						obj_t BgL_auxz00_10617;

						{	/* Cfa/cinfo.sch 843 */
							BgL_objectz00_bglt BgL_tmpz00_10618;

							BgL_tmpz00_10618 = ((BgL_objectz00_bglt) BgL_new1201z00_8780);
							BgL_auxz00_10617 = BGL_OBJECT_WIDENING(BgL_tmpz00_10618);
						}
						BgL_auxz00_10616 = ((BgL_svarzf2cinfozf2_bglt) BgL_auxz00_10617);
					}
					((((BgL_svarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10616))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1742z00_334), BUNSPEC);
				}
				{
					BgL_svarzf2cinfozf2_bglt BgL_auxz00_10623;

					{
						obj_t BgL_auxz00_10624;

						{	/* Cfa/cinfo.sch 843 */
							BgL_objectz00_bglt BgL_tmpz00_10625;

							BgL_tmpz00_10625 = ((BgL_objectz00_bglt) BgL_new1201z00_8780);
							BgL_auxz00_10624 = BGL_OBJECT_WIDENING(BgL_tmpz00_10625);
						}
						BgL_auxz00_10623 = ((BgL_svarzf2cinfozf2_bglt) BgL_auxz00_10624);
					}
					((((BgL_svarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10623))->
							BgL_clozd2envzf3z21) =
						((bool_t) BgL_clozd2envzf31743z21_335), BUNSPEC);
				}
				{
					BgL_svarzf2cinfozf2_bglt BgL_auxz00_10630;

					{
						obj_t BgL_auxz00_10631;

						{	/* Cfa/cinfo.sch 843 */
							BgL_objectz00_bglt BgL_tmpz00_10632;

							BgL_tmpz00_10632 = ((BgL_objectz00_bglt) BgL_new1201z00_8780);
							BgL_auxz00_10631 = BGL_OBJECT_WIDENING(BgL_tmpz00_10632);
						}
						BgL_auxz00_10630 = ((BgL_svarzf2cinfozf2_bglt) BgL_auxz00_10631);
					}
					((((BgL_svarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10630))->
							BgL_stampz00) = ((long) BgL_stamp1744z00_336), BUNSPEC);
				}
				return BgL_new1201z00_8780;
			}
		}

	}



/* &make-svar/Cinfo */
	BgL_svarz00_bglt BGl_z62makezd2svarzf2Cinfoz42zzcfa_approxz00(obj_t
		BgL_envz00_7583, obj_t BgL_loc1741z00_7584, obj_t BgL_approx1742z00_7585,
		obj_t BgL_clozd2envzf31743z21_7586, obj_t BgL_stamp1744z00_7587)
	{
		{	/* Cfa/cinfo.sch 843 */
			return
				BGl_makezd2svarzf2Cinfoz20zzcfa_approxz00(BgL_loc1741z00_7584,
				((BgL_approxz00_bglt) BgL_approx1742z00_7585),
				CBOOL(BgL_clozd2envzf31743z21_7586),
				(long) CINT(BgL_stamp1744z00_7587));
		}

	}



/* svar/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_svarzf2Cinfozf3z01zzcfa_approxz00(obj_t
		BgL_objz00_337)
	{
		{	/* Cfa/cinfo.sch 844 */
			{	/* Cfa/cinfo.sch 844 */
				obj_t BgL_classz00_8786;

				BgL_classz00_8786 = BGl_svarzf2Cinfozf2zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_337))
					{	/* Cfa/cinfo.sch 844 */
						BgL_objectz00_bglt BgL_arg1807z00_8787;

						BgL_arg1807z00_8787 = (BgL_objectz00_bglt) (BgL_objz00_337);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 844 */
								long BgL_idxz00_8788;

								BgL_idxz00_8788 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8787);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8788 + 3L)) == BgL_classz00_8786);
							}
						else
							{	/* Cfa/cinfo.sch 844 */
								bool_t BgL_res2173z00_8791;

								{	/* Cfa/cinfo.sch 844 */
									obj_t BgL_oclassz00_8792;

									{	/* Cfa/cinfo.sch 844 */
										obj_t BgL_arg1815z00_8793;
										long BgL_arg1816z00_8794;

										BgL_arg1815z00_8793 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 844 */
											long BgL_arg1817z00_8795;

											BgL_arg1817z00_8795 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8787);
											BgL_arg1816z00_8794 = (BgL_arg1817z00_8795 - OBJECT_TYPE);
										}
										BgL_oclassz00_8792 =
											VECTOR_REF(BgL_arg1815z00_8793, BgL_arg1816z00_8794);
									}
									{	/* Cfa/cinfo.sch 844 */
										bool_t BgL__ortest_1115z00_8796;

										BgL__ortest_1115z00_8796 =
											(BgL_classz00_8786 == BgL_oclassz00_8792);
										if (BgL__ortest_1115z00_8796)
											{	/* Cfa/cinfo.sch 844 */
												BgL_res2173z00_8791 = BgL__ortest_1115z00_8796;
											}
										else
											{	/* Cfa/cinfo.sch 844 */
												long BgL_odepthz00_8797;

												{	/* Cfa/cinfo.sch 844 */
													obj_t BgL_arg1804z00_8798;

													BgL_arg1804z00_8798 = (BgL_oclassz00_8792);
													BgL_odepthz00_8797 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8798);
												}
												if ((3L < BgL_odepthz00_8797))
													{	/* Cfa/cinfo.sch 844 */
														obj_t BgL_arg1802z00_8799;

														{	/* Cfa/cinfo.sch 844 */
															obj_t BgL_arg1803z00_8800;

															BgL_arg1803z00_8800 = (BgL_oclassz00_8792);
															BgL_arg1802z00_8799 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8800,
																3L);
														}
														BgL_res2173z00_8791 =
															(BgL_arg1802z00_8799 == BgL_classz00_8786);
													}
												else
													{	/* Cfa/cinfo.sch 844 */
														BgL_res2173z00_8791 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2173z00_8791;
							}
					}
				else
					{	/* Cfa/cinfo.sch 844 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &svar/Cinfo? */
	obj_t BGl_z62svarzf2Cinfozf3z63zzcfa_approxz00(obj_t BgL_envz00_7588,
		obj_t BgL_objz00_7589)
	{
		{	/* Cfa/cinfo.sch 844 */
			return BBOOL(BGl_svarzf2Cinfozf3z01zzcfa_approxz00(BgL_objz00_7589));
		}

	}



/* svar/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_svarz00_bglt
		BGl_svarzf2Cinfozd2nilz20zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 845 */
			{	/* Cfa/cinfo.sch 845 */
				obj_t BgL_classz00_5061;

				BgL_classz00_5061 = BGl_svarzf2Cinfozf2zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 845 */
					obj_t BgL__ortest_1117z00_5062;

					BgL__ortest_1117z00_5062 = BGL_CLASS_NIL(BgL_classz00_5061);
					if (CBOOL(BgL__ortest_1117z00_5062))
						{	/* Cfa/cinfo.sch 845 */
							return ((BgL_svarz00_bglt) BgL__ortest_1117z00_5062);
						}
					else
						{	/* Cfa/cinfo.sch 845 */
							return
								((BgL_svarz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5061));
						}
				}
			}
		}

	}



/* &svar/Cinfo-nil */
	BgL_svarz00_bglt BGl_z62svarzf2Cinfozd2nilz42zzcfa_approxz00(obj_t
		BgL_envz00_7590)
	{
		{	/* Cfa/cinfo.sch 845 */
			return BGl_svarzf2Cinfozd2nilz20zzcfa_approxz00();
		}

	}



/* svar/Cinfo-stamp */
	BGL_EXPORTED_DEF long
		BGl_svarzf2Cinfozd2stampz20zzcfa_approxz00(BgL_svarz00_bglt BgL_oz00_338)
	{
		{	/* Cfa/cinfo.sch 846 */
			{
				BgL_svarzf2cinfozf2_bglt BgL_auxz00_10672;

				{
					obj_t BgL_auxz00_10673;

					{	/* Cfa/cinfo.sch 846 */
						BgL_objectz00_bglt BgL_tmpz00_10674;

						BgL_tmpz00_10674 = ((BgL_objectz00_bglt) BgL_oz00_338);
						BgL_auxz00_10673 = BGL_OBJECT_WIDENING(BgL_tmpz00_10674);
					}
					BgL_auxz00_10672 = ((BgL_svarzf2cinfozf2_bglt) BgL_auxz00_10673);
				}
				return
					(((BgL_svarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10672))->
					BgL_stampz00);
			}
		}

	}



/* &svar/Cinfo-stamp */
	obj_t BGl_z62svarzf2Cinfozd2stampz42zzcfa_approxz00(obj_t BgL_envz00_7591,
		obj_t BgL_oz00_7592)
	{
		{	/* Cfa/cinfo.sch 846 */
			return
				BINT(BGl_svarzf2Cinfozd2stampz20zzcfa_approxz00(
					((BgL_svarz00_bglt) BgL_oz00_7592)));
		}

	}



/* svar/Cinfo-stamp-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_svarzf2Cinfozd2stampzd2setz12ze0zzcfa_approxz00(BgL_svarz00_bglt
		BgL_oz00_339, long BgL_vz00_340)
	{
		{	/* Cfa/cinfo.sch 847 */
			{
				BgL_svarzf2cinfozf2_bglt BgL_auxz00_10682;

				{
					obj_t BgL_auxz00_10683;

					{	/* Cfa/cinfo.sch 847 */
						BgL_objectz00_bglt BgL_tmpz00_10684;

						BgL_tmpz00_10684 = ((BgL_objectz00_bglt) BgL_oz00_339);
						BgL_auxz00_10683 = BGL_OBJECT_WIDENING(BgL_tmpz00_10684);
					}
					BgL_auxz00_10682 = ((BgL_svarzf2cinfozf2_bglt) BgL_auxz00_10683);
				}
				return
					((((BgL_svarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10682))->
						BgL_stampz00) = ((long) BgL_vz00_340), BUNSPEC);
		}}

	}



/* &svar/Cinfo-stamp-set! */
	obj_t BGl_z62svarzf2Cinfozd2stampzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_7593, obj_t BgL_oz00_7594, obj_t BgL_vz00_7595)
	{
		{	/* Cfa/cinfo.sch 847 */
			return
				BGl_svarzf2Cinfozd2stampzd2setz12ze0zzcfa_approxz00(
				((BgL_svarz00_bglt) BgL_oz00_7594), (long) CINT(BgL_vz00_7595));
		}

	}



/* svar/Cinfo-clo-env? */
	BGL_EXPORTED_DEF bool_t
		BGl_svarzf2Cinfozd2clozd2envzf3z01zzcfa_approxz00(BgL_svarz00_bglt
		BgL_oz00_341)
	{
		{	/* Cfa/cinfo.sch 848 */
			{
				BgL_svarzf2cinfozf2_bglt BgL_auxz00_10692;

				{
					obj_t BgL_auxz00_10693;

					{	/* Cfa/cinfo.sch 848 */
						BgL_objectz00_bglt BgL_tmpz00_10694;

						BgL_tmpz00_10694 = ((BgL_objectz00_bglt) BgL_oz00_341);
						BgL_auxz00_10693 = BGL_OBJECT_WIDENING(BgL_tmpz00_10694);
					}
					BgL_auxz00_10692 = ((BgL_svarzf2cinfozf2_bglt) BgL_auxz00_10693);
				}
				return
					(((BgL_svarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10692))->
					BgL_clozd2envzf3z21);
			}
		}

	}



/* &svar/Cinfo-clo-env? */
	obj_t BGl_z62svarzf2Cinfozd2clozd2envzf3z63zzcfa_approxz00(obj_t
		BgL_envz00_7596, obj_t BgL_oz00_7597)
	{
		{	/* Cfa/cinfo.sch 848 */
			return
				BBOOL(BGl_svarzf2Cinfozd2clozd2envzf3z01zzcfa_approxz00(
					((BgL_svarz00_bglt) BgL_oz00_7597)));
		}

	}



/* svar/Cinfo-clo-env?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_svarzf2Cinfozd2clozd2envzf3zd2setz12zc1zzcfa_approxz00(BgL_svarz00_bglt
		BgL_oz00_342, bool_t BgL_vz00_343)
	{
		{	/* Cfa/cinfo.sch 849 */
			{
				BgL_svarzf2cinfozf2_bglt BgL_auxz00_10702;

				{
					obj_t BgL_auxz00_10703;

					{	/* Cfa/cinfo.sch 849 */
						BgL_objectz00_bglt BgL_tmpz00_10704;

						BgL_tmpz00_10704 = ((BgL_objectz00_bglt) BgL_oz00_342);
						BgL_auxz00_10703 = BGL_OBJECT_WIDENING(BgL_tmpz00_10704);
					}
					BgL_auxz00_10702 = ((BgL_svarzf2cinfozf2_bglt) BgL_auxz00_10703);
				}
				return
					((((BgL_svarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10702))->
						BgL_clozd2envzf3z21) = ((bool_t) BgL_vz00_343), BUNSPEC);
			}
		}

	}



/* &svar/Cinfo-clo-env?-set! */
	obj_t BGl_z62svarzf2Cinfozd2clozd2envzf3zd2setz12za3zzcfa_approxz00(obj_t
		BgL_envz00_7598, obj_t BgL_oz00_7599, obj_t BgL_vz00_7600)
	{
		{	/* Cfa/cinfo.sch 849 */
			return
				BGl_svarzf2Cinfozd2clozd2envzf3zd2setz12zc1zzcfa_approxz00(
				((BgL_svarz00_bglt) BgL_oz00_7599), CBOOL(BgL_vz00_7600));
		}

	}



/* svar/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_svarzf2Cinfozd2approxz20zzcfa_approxz00(BgL_svarz00_bglt BgL_oz00_344)
	{
		{	/* Cfa/cinfo.sch 850 */
			{
				BgL_svarzf2cinfozf2_bglt BgL_auxz00_10712;

				{
					obj_t BgL_auxz00_10713;

					{	/* Cfa/cinfo.sch 850 */
						BgL_objectz00_bglt BgL_tmpz00_10714;

						BgL_tmpz00_10714 = ((BgL_objectz00_bglt) BgL_oz00_344);
						BgL_auxz00_10713 = BGL_OBJECT_WIDENING(BgL_tmpz00_10714);
					}
					BgL_auxz00_10712 = ((BgL_svarzf2cinfozf2_bglt) BgL_auxz00_10713);
				}
				return
					(((BgL_svarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10712))->
					BgL_approxz00);
			}
		}

	}



/* &svar/Cinfo-approx */
	BgL_approxz00_bglt BGl_z62svarzf2Cinfozd2approxz42zzcfa_approxz00(obj_t
		BgL_envz00_7601, obj_t BgL_oz00_7602)
	{
		{	/* Cfa/cinfo.sch 850 */
			return
				BGl_svarzf2Cinfozd2approxz20zzcfa_approxz00(
				((BgL_svarz00_bglt) BgL_oz00_7602));
		}

	}



/* svar/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_svarzf2Cinfozd2locz20zzcfa_approxz00(BgL_svarz00_bglt BgL_oz00_347)
	{
		{	/* Cfa/cinfo.sch 852 */
			return
				(((BgL_svarz00_bglt) COBJECT(
						((BgL_svarz00_bglt) BgL_oz00_347)))->BgL_locz00);
		}

	}



/* &svar/Cinfo-loc */
	obj_t BGl_z62svarzf2Cinfozd2locz42zzcfa_approxz00(obj_t BgL_envz00_7603,
		obj_t BgL_oz00_7604)
	{
		{	/* Cfa/cinfo.sch 852 */
			return
				BGl_svarzf2Cinfozd2locz20zzcfa_approxz00(
				((BgL_svarz00_bglt) BgL_oz00_7604));
		}

	}



/* svar/Cinfo-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_svarzf2Cinfozd2loczd2setz12ze0zzcfa_approxz00(BgL_svarz00_bglt
		BgL_oz00_348, obj_t BgL_vz00_349)
	{
		{	/* Cfa/cinfo.sch 853 */
			return
				((((BgL_svarz00_bglt) COBJECT(
							((BgL_svarz00_bglt) BgL_oz00_348)))->BgL_locz00) =
				((obj_t) BgL_vz00_349), BUNSPEC);
		}

	}



/* &svar/Cinfo-loc-set! */
	obj_t BGl_z62svarzf2Cinfozd2loczd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_7605, obj_t BgL_oz00_7606, obj_t BgL_vz00_7607)
	{
		{	/* Cfa/cinfo.sch 853 */
			return
				BGl_svarzf2Cinfozd2loczd2setz12ze0zzcfa_approxz00(
				((BgL_svarz00_bglt) BgL_oz00_7606), BgL_vz00_7607);
		}

	}



/* make-cvar/Cinfo */
	BGL_EXPORTED_DEF BgL_cvarz00_bglt
		BGl_makezd2cvarzf2Cinfoz20zzcfa_approxz00(bool_t BgL_macrozf31738zf3_350,
		BgL_approxz00_bglt BgL_approx1739z00_351)
	{
		{	/* Cfa/cinfo.sch 856 */
			{	/* Cfa/cinfo.sch 856 */
				BgL_cvarz00_bglt BgL_new1205z00_8801;

				{	/* Cfa/cinfo.sch 856 */
					BgL_cvarz00_bglt BgL_tmp1203z00_8802;
					BgL_cvarzf2cinfozf2_bglt BgL_wide1204z00_8803;

					{
						BgL_cvarz00_bglt BgL_auxz00_10729;

						{	/* Cfa/cinfo.sch 856 */
							BgL_cvarz00_bglt BgL_new1202z00_8804;

							BgL_new1202z00_8804 =
								((BgL_cvarz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_cvarz00_bgl))));
							{	/* Cfa/cinfo.sch 856 */
								long BgL_arg1688z00_8805;

								BgL_arg1688z00_8805 = BGL_CLASS_NUM(BGl_cvarz00zzast_varz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1202z00_8804),
									BgL_arg1688z00_8805);
							}
							{	/* Cfa/cinfo.sch 856 */
								BgL_objectz00_bglt BgL_tmpz00_10734;

								BgL_tmpz00_10734 = ((BgL_objectz00_bglt) BgL_new1202z00_8804);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10734, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1202z00_8804);
							BgL_auxz00_10729 = BgL_new1202z00_8804;
						}
						BgL_tmp1203z00_8802 = ((BgL_cvarz00_bglt) BgL_auxz00_10729);
					}
					BgL_wide1204z00_8803 =
						((BgL_cvarzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cvarzf2cinfozf2_bgl))));
					{	/* Cfa/cinfo.sch 856 */
						obj_t BgL_auxz00_10742;
						BgL_objectz00_bglt BgL_tmpz00_10740;

						BgL_auxz00_10742 = ((obj_t) BgL_wide1204z00_8803);
						BgL_tmpz00_10740 = ((BgL_objectz00_bglt) BgL_tmp1203z00_8802);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10740, BgL_auxz00_10742);
					}
					((BgL_objectz00_bglt) BgL_tmp1203z00_8802);
					{	/* Cfa/cinfo.sch 856 */
						long BgL_arg1681z00_8806;

						BgL_arg1681z00_8806 =
							BGL_CLASS_NUM(BGl_cvarzf2Cinfozf2zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1203z00_8802), BgL_arg1681z00_8806);
					}
					BgL_new1205z00_8801 = ((BgL_cvarz00_bglt) BgL_tmp1203z00_8802);
				}
				((((BgL_cvarz00_bglt) COBJECT(
								((BgL_cvarz00_bglt) BgL_new1205z00_8801)))->BgL_macrozf3zf3) =
					((bool_t) BgL_macrozf31738zf3_350), BUNSPEC);
				{
					BgL_cvarzf2cinfozf2_bglt BgL_auxz00_10752;

					{
						obj_t BgL_auxz00_10753;

						{	/* Cfa/cinfo.sch 856 */
							BgL_objectz00_bglt BgL_tmpz00_10754;

							BgL_tmpz00_10754 = ((BgL_objectz00_bglt) BgL_new1205z00_8801);
							BgL_auxz00_10753 = BGL_OBJECT_WIDENING(BgL_tmpz00_10754);
						}
						BgL_auxz00_10752 = ((BgL_cvarzf2cinfozf2_bglt) BgL_auxz00_10753);
					}
					((((BgL_cvarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10752))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1739z00_351), BUNSPEC);
				}
				return BgL_new1205z00_8801;
			}
		}

	}



/* &make-cvar/Cinfo */
	BgL_cvarz00_bglt BGl_z62makezd2cvarzf2Cinfoz42zzcfa_approxz00(obj_t
		BgL_envz00_7608, obj_t BgL_macrozf31738zf3_7609,
		obj_t BgL_approx1739z00_7610)
	{
		{	/* Cfa/cinfo.sch 856 */
			return
				BGl_makezd2cvarzf2Cinfoz20zzcfa_approxz00(CBOOL
				(BgL_macrozf31738zf3_7609),
				((BgL_approxz00_bglt) BgL_approx1739z00_7610));
		}

	}



/* cvar/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_cvarzf2Cinfozf3z01zzcfa_approxz00(obj_t
		BgL_objz00_352)
	{
		{	/* Cfa/cinfo.sch 857 */
			{	/* Cfa/cinfo.sch 857 */
				obj_t BgL_classz00_8807;

				BgL_classz00_8807 = BGl_cvarzf2Cinfozf2zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_352))
					{	/* Cfa/cinfo.sch 857 */
						BgL_objectz00_bglt BgL_arg1807z00_8808;

						BgL_arg1807z00_8808 = (BgL_objectz00_bglt) (BgL_objz00_352);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 857 */
								long BgL_idxz00_8809;

								BgL_idxz00_8809 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8808);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8809 + 3L)) == BgL_classz00_8807);
							}
						else
							{	/* Cfa/cinfo.sch 857 */
								bool_t BgL_res2174z00_8812;

								{	/* Cfa/cinfo.sch 857 */
									obj_t BgL_oclassz00_8813;

									{	/* Cfa/cinfo.sch 857 */
										obj_t BgL_arg1815z00_8814;
										long BgL_arg1816z00_8815;

										BgL_arg1815z00_8814 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 857 */
											long BgL_arg1817z00_8816;

											BgL_arg1817z00_8816 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8808);
											BgL_arg1816z00_8815 = (BgL_arg1817z00_8816 - OBJECT_TYPE);
										}
										BgL_oclassz00_8813 =
											VECTOR_REF(BgL_arg1815z00_8814, BgL_arg1816z00_8815);
									}
									{	/* Cfa/cinfo.sch 857 */
										bool_t BgL__ortest_1115z00_8817;

										BgL__ortest_1115z00_8817 =
											(BgL_classz00_8807 == BgL_oclassz00_8813);
										if (BgL__ortest_1115z00_8817)
											{	/* Cfa/cinfo.sch 857 */
												BgL_res2174z00_8812 = BgL__ortest_1115z00_8817;
											}
										else
											{	/* Cfa/cinfo.sch 857 */
												long BgL_odepthz00_8818;

												{	/* Cfa/cinfo.sch 857 */
													obj_t BgL_arg1804z00_8819;

													BgL_arg1804z00_8819 = (BgL_oclassz00_8813);
													BgL_odepthz00_8818 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8819);
												}
												if ((3L < BgL_odepthz00_8818))
													{	/* Cfa/cinfo.sch 857 */
														obj_t BgL_arg1802z00_8820;

														{	/* Cfa/cinfo.sch 857 */
															obj_t BgL_arg1803z00_8821;

															BgL_arg1803z00_8821 = (BgL_oclassz00_8813);
															BgL_arg1802z00_8820 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8821,
																3L);
														}
														BgL_res2174z00_8812 =
															(BgL_arg1802z00_8820 == BgL_classz00_8807);
													}
												else
													{	/* Cfa/cinfo.sch 857 */
														BgL_res2174z00_8812 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2174z00_8812;
							}
					}
				else
					{	/* Cfa/cinfo.sch 857 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &cvar/Cinfo? */
	obj_t BGl_z62cvarzf2Cinfozf3z63zzcfa_approxz00(obj_t BgL_envz00_7611,
		obj_t BgL_objz00_7612)
	{
		{	/* Cfa/cinfo.sch 857 */
			return BBOOL(BGl_cvarzf2Cinfozf3z01zzcfa_approxz00(BgL_objz00_7612));
		}

	}



/* cvar/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_cvarz00_bglt
		BGl_cvarzf2Cinfozd2nilz20zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 858 */
			{	/* Cfa/cinfo.sch 858 */
				obj_t BgL_classz00_5117;

				BgL_classz00_5117 = BGl_cvarzf2Cinfozf2zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 858 */
					obj_t BgL__ortest_1117z00_5118;

					BgL__ortest_1117z00_5118 = BGL_CLASS_NIL(BgL_classz00_5117);
					if (CBOOL(BgL__ortest_1117z00_5118))
						{	/* Cfa/cinfo.sch 858 */
							return ((BgL_cvarz00_bglt) BgL__ortest_1117z00_5118);
						}
					else
						{	/* Cfa/cinfo.sch 858 */
							return
								((BgL_cvarz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5117));
						}
				}
			}
		}

	}



/* &cvar/Cinfo-nil */
	BgL_cvarz00_bglt BGl_z62cvarzf2Cinfozd2nilz42zzcfa_approxz00(obj_t
		BgL_envz00_7613)
	{
		{	/* Cfa/cinfo.sch 858 */
			return BGl_cvarzf2Cinfozd2nilz20zzcfa_approxz00();
		}

	}



/* cvar/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_cvarzf2Cinfozd2approxz20zzcfa_approxz00(BgL_cvarz00_bglt BgL_oz00_353)
	{
		{	/* Cfa/cinfo.sch 859 */
			{
				BgL_cvarzf2cinfozf2_bglt BgL_auxz00_10793;

				{
					obj_t BgL_auxz00_10794;

					{	/* Cfa/cinfo.sch 859 */
						BgL_objectz00_bglt BgL_tmpz00_10795;

						BgL_tmpz00_10795 = ((BgL_objectz00_bglt) BgL_oz00_353);
						BgL_auxz00_10794 = BGL_OBJECT_WIDENING(BgL_tmpz00_10795);
					}
					BgL_auxz00_10793 = ((BgL_cvarzf2cinfozf2_bglt) BgL_auxz00_10794);
				}
				return
					(((BgL_cvarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10793))->
					BgL_approxz00);
			}
		}

	}



/* &cvar/Cinfo-approx */
	BgL_approxz00_bglt BGl_z62cvarzf2Cinfozd2approxz42zzcfa_approxz00(obj_t
		BgL_envz00_7614, obj_t BgL_oz00_7615)
	{
		{	/* Cfa/cinfo.sch 859 */
			return
				BGl_cvarzf2Cinfozd2approxz20zzcfa_approxz00(
				((BgL_cvarz00_bglt) BgL_oz00_7615));
		}

	}



/* cvar/Cinfo-macro? */
	BGL_EXPORTED_DEF bool_t
		BGl_cvarzf2Cinfozd2macrozf3zd3zzcfa_approxz00(BgL_cvarz00_bglt BgL_oz00_356)
	{
		{	/* Cfa/cinfo.sch 861 */
			return
				(((BgL_cvarz00_bglt) COBJECT(
						((BgL_cvarz00_bglt) BgL_oz00_356)))->BgL_macrozf3zf3);
		}

	}



/* &cvar/Cinfo-macro? */
	obj_t BGl_z62cvarzf2Cinfozd2macrozf3zb1zzcfa_approxz00(obj_t BgL_envz00_7616,
		obj_t BgL_oz00_7617)
	{
		{	/* Cfa/cinfo.sch 861 */
			return
				BBOOL(BGl_cvarzf2Cinfozd2macrozf3zd3zzcfa_approxz00(
					((BgL_cvarz00_bglt) BgL_oz00_7617)));
		}

	}



/* make-sexit/Cinfo */
	BGL_EXPORTED_DEF BgL_sexitz00_bglt
		BGl_makezd2sexitzf2Cinfoz20zzcfa_approxz00(obj_t BgL_handler1734z00_359,
		bool_t BgL_detachedzf31735zf3_360, BgL_approxz00_bglt BgL_approx1736z00_361)
	{
		{	/* Cfa/cinfo.sch 865 */
			{	/* Cfa/cinfo.sch 865 */
				BgL_sexitz00_bglt BgL_new1209z00_8822;

				{	/* Cfa/cinfo.sch 865 */
					BgL_sexitz00_bglt BgL_tmp1207z00_8823;
					BgL_sexitzf2cinfozf2_bglt BgL_wide1208z00_8824;

					{
						BgL_sexitz00_bglt BgL_auxz00_10807;

						{	/* Cfa/cinfo.sch 865 */
							BgL_sexitz00_bglt BgL_new1206z00_8825;

							BgL_new1206z00_8825 =
								((BgL_sexitz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_sexitz00_bgl))));
							{	/* Cfa/cinfo.sch 865 */
								long BgL_arg1691z00_8826;

								BgL_arg1691z00_8826 = BGL_CLASS_NUM(BGl_sexitz00zzast_varz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1206z00_8825),
									BgL_arg1691z00_8826);
							}
							{	/* Cfa/cinfo.sch 865 */
								BgL_objectz00_bglt BgL_tmpz00_10812;

								BgL_tmpz00_10812 = ((BgL_objectz00_bglt) BgL_new1206z00_8825);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10812, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1206z00_8825);
							BgL_auxz00_10807 = BgL_new1206z00_8825;
						}
						BgL_tmp1207z00_8823 = ((BgL_sexitz00_bglt) BgL_auxz00_10807);
					}
					BgL_wide1208z00_8824 =
						((BgL_sexitzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_sexitzf2cinfozf2_bgl))));
					{	/* Cfa/cinfo.sch 865 */
						obj_t BgL_auxz00_10820;
						BgL_objectz00_bglt BgL_tmpz00_10818;

						BgL_auxz00_10820 = ((obj_t) BgL_wide1208z00_8824);
						BgL_tmpz00_10818 = ((BgL_objectz00_bglt) BgL_tmp1207z00_8823);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10818, BgL_auxz00_10820);
					}
					((BgL_objectz00_bglt) BgL_tmp1207z00_8823);
					{	/* Cfa/cinfo.sch 865 */
						long BgL_arg1689z00_8827;

						BgL_arg1689z00_8827 =
							BGL_CLASS_NUM(BGl_sexitzf2Cinfozf2zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1207z00_8823), BgL_arg1689z00_8827);
					}
					BgL_new1209z00_8822 = ((BgL_sexitz00_bglt) BgL_tmp1207z00_8823);
				}
				((((BgL_sexitz00_bglt) COBJECT(
								((BgL_sexitz00_bglt) BgL_new1209z00_8822)))->BgL_handlerz00) =
					((obj_t) BgL_handler1734z00_359), BUNSPEC);
				((((BgL_sexitz00_bglt) COBJECT(((BgL_sexitz00_bglt)
									BgL_new1209z00_8822)))->BgL_detachedzf3zf3) =
					((bool_t) BgL_detachedzf31735zf3_360), BUNSPEC);
				{
					BgL_sexitzf2cinfozf2_bglt BgL_auxz00_10832;

					{
						obj_t BgL_auxz00_10833;

						{	/* Cfa/cinfo.sch 865 */
							BgL_objectz00_bglt BgL_tmpz00_10834;

							BgL_tmpz00_10834 = ((BgL_objectz00_bglt) BgL_new1209z00_8822);
							BgL_auxz00_10833 = BGL_OBJECT_WIDENING(BgL_tmpz00_10834);
						}
						BgL_auxz00_10832 = ((BgL_sexitzf2cinfozf2_bglt) BgL_auxz00_10833);
					}
					((((BgL_sexitzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10832))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1736z00_361), BUNSPEC);
				}
				return BgL_new1209z00_8822;
			}
		}

	}



/* &make-sexit/Cinfo */
	BgL_sexitz00_bglt BGl_z62makezd2sexitzf2Cinfoz42zzcfa_approxz00(obj_t
		BgL_envz00_7618, obj_t BgL_handler1734z00_7619,
		obj_t BgL_detachedzf31735zf3_7620, obj_t BgL_approx1736z00_7621)
	{
		{	/* Cfa/cinfo.sch 865 */
			return
				BGl_makezd2sexitzf2Cinfoz20zzcfa_approxz00(BgL_handler1734z00_7619,
				CBOOL(BgL_detachedzf31735zf3_7620),
				((BgL_approxz00_bglt) BgL_approx1736z00_7621));
		}

	}



/* sexit/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_sexitzf2Cinfozf3z01zzcfa_approxz00(obj_t
		BgL_objz00_362)
	{
		{	/* Cfa/cinfo.sch 866 */
			{	/* Cfa/cinfo.sch 866 */
				obj_t BgL_classz00_8828;

				BgL_classz00_8828 = BGl_sexitzf2Cinfozf2zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_362))
					{	/* Cfa/cinfo.sch 866 */
						BgL_objectz00_bglt BgL_arg1807z00_8829;

						BgL_arg1807z00_8829 = (BgL_objectz00_bglt) (BgL_objz00_362);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 866 */
								long BgL_idxz00_8830;

								BgL_idxz00_8830 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8829);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8830 + 3L)) == BgL_classz00_8828);
							}
						else
							{	/* Cfa/cinfo.sch 866 */
								bool_t BgL_res2175z00_8833;

								{	/* Cfa/cinfo.sch 866 */
									obj_t BgL_oclassz00_8834;

									{	/* Cfa/cinfo.sch 866 */
										obj_t BgL_arg1815z00_8835;
										long BgL_arg1816z00_8836;

										BgL_arg1815z00_8835 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 866 */
											long BgL_arg1817z00_8837;

											BgL_arg1817z00_8837 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8829);
											BgL_arg1816z00_8836 = (BgL_arg1817z00_8837 - OBJECT_TYPE);
										}
										BgL_oclassz00_8834 =
											VECTOR_REF(BgL_arg1815z00_8835, BgL_arg1816z00_8836);
									}
									{	/* Cfa/cinfo.sch 866 */
										bool_t BgL__ortest_1115z00_8838;

										BgL__ortest_1115z00_8838 =
											(BgL_classz00_8828 == BgL_oclassz00_8834);
										if (BgL__ortest_1115z00_8838)
											{	/* Cfa/cinfo.sch 866 */
												BgL_res2175z00_8833 = BgL__ortest_1115z00_8838;
											}
										else
											{	/* Cfa/cinfo.sch 866 */
												long BgL_odepthz00_8839;

												{	/* Cfa/cinfo.sch 866 */
													obj_t BgL_arg1804z00_8840;

													BgL_arg1804z00_8840 = (BgL_oclassz00_8834);
													BgL_odepthz00_8839 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8840);
												}
												if ((3L < BgL_odepthz00_8839))
													{	/* Cfa/cinfo.sch 866 */
														obj_t BgL_arg1802z00_8841;

														{	/* Cfa/cinfo.sch 866 */
															obj_t BgL_arg1803z00_8842;

															BgL_arg1803z00_8842 = (BgL_oclassz00_8834);
															BgL_arg1802z00_8841 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8842,
																3L);
														}
														BgL_res2175z00_8833 =
															(BgL_arg1802z00_8841 == BgL_classz00_8828);
													}
												else
													{	/* Cfa/cinfo.sch 866 */
														BgL_res2175z00_8833 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2175z00_8833;
							}
					}
				else
					{	/* Cfa/cinfo.sch 866 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &sexit/Cinfo? */
	obj_t BGl_z62sexitzf2Cinfozf3z63zzcfa_approxz00(obj_t BgL_envz00_7622,
		obj_t BgL_objz00_7623)
	{
		{	/* Cfa/cinfo.sch 866 */
			return BBOOL(BGl_sexitzf2Cinfozf3z01zzcfa_approxz00(BgL_objz00_7623));
		}

	}



/* sexit/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_sexitz00_bglt
		BGl_sexitzf2Cinfozd2nilz20zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 867 */
			{	/* Cfa/cinfo.sch 867 */
				obj_t BgL_classz00_5169;

				BgL_classz00_5169 = BGl_sexitzf2Cinfozf2zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 867 */
					obj_t BgL__ortest_1117z00_5170;

					BgL__ortest_1117z00_5170 = BGL_CLASS_NIL(BgL_classz00_5169);
					if (CBOOL(BgL__ortest_1117z00_5170))
						{	/* Cfa/cinfo.sch 867 */
							return ((BgL_sexitz00_bglt) BgL__ortest_1117z00_5170);
						}
					else
						{	/* Cfa/cinfo.sch 867 */
							return
								((BgL_sexitz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5169));
						}
				}
			}
		}

	}



/* &sexit/Cinfo-nil */
	BgL_sexitz00_bglt BGl_z62sexitzf2Cinfozd2nilz42zzcfa_approxz00(obj_t
		BgL_envz00_7624)
	{
		{	/* Cfa/cinfo.sch 867 */
			return BGl_sexitzf2Cinfozd2nilz20zzcfa_approxz00();
		}

	}



/* sexit/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_sexitzf2Cinfozd2approxz20zzcfa_approxz00(BgL_sexitz00_bglt BgL_oz00_363)
	{
		{	/* Cfa/cinfo.sch 868 */
			{
				BgL_sexitzf2cinfozf2_bglt BgL_auxz00_10873;

				{
					obj_t BgL_auxz00_10874;

					{	/* Cfa/cinfo.sch 868 */
						BgL_objectz00_bglt BgL_tmpz00_10875;

						BgL_tmpz00_10875 = ((BgL_objectz00_bglt) BgL_oz00_363);
						BgL_auxz00_10874 = BGL_OBJECT_WIDENING(BgL_tmpz00_10875);
					}
					BgL_auxz00_10873 = ((BgL_sexitzf2cinfozf2_bglt) BgL_auxz00_10874);
				}
				return
					(((BgL_sexitzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10873))->
					BgL_approxz00);
			}
		}

	}



/* &sexit/Cinfo-approx */
	BgL_approxz00_bglt BGl_z62sexitzf2Cinfozd2approxz42zzcfa_approxz00(obj_t
		BgL_envz00_7625, obj_t BgL_oz00_7626)
	{
		{	/* Cfa/cinfo.sch 868 */
			return
				BGl_sexitzf2Cinfozd2approxz20zzcfa_approxz00(
				((BgL_sexitz00_bglt) BgL_oz00_7626));
		}

	}



/* sexit/Cinfo-detached? */
	BGL_EXPORTED_DEF bool_t
		BGl_sexitzf2Cinfozd2detachedzf3zd3zzcfa_approxz00(BgL_sexitz00_bglt
		BgL_oz00_366)
	{
		{	/* Cfa/cinfo.sch 870 */
			return
				(((BgL_sexitz00_bglt) COBJECT(
						((BgL_sexitz00_bglt) BgL_oz00_366)))->BgL_detachedzf3zf3);
		}

	}



/* &sexit/Cinfo-detached? */
	obj_t BGl_z62sexitzf2Cinfozd2detachedzf3zb1zzcfa_approxz00(obj_t
		BgL_envz00_7627, obj_t BgL_oz00_7628)
	{
		{	/* Cfa/cinfo.sch 870 */
			return
				BBOOL(BGl_sexitzf2Cinfozd2detachedzf3zd3zzcfa_approxz00(
					((BgL_sexitz00_bglt) BgL_oz00_7628)));
		}

	}



/* sexit/Cinfo-detached?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sexitzf2Cinfozd2detachedzf3zd2setz12z13zzcfa_approxz00(BgL_sexitz00_bglt
		BgL_oz00_367, bool_t BgL_vz00_368)
	{
		{	/* Cfa/cinfo.sch 871 */
			return
				((((BgL_sexitz00_bglt) COBJECT(
							((BgL_sexitz00_bglt) BgL_oz00_367)))->BgL_detachedzf3zf3) =
				((bool_t) BgL_vz00_368), BUNSPEC);
		}

	}



/* &sexit/Cinfo-detached?-set! */
	obj_t BGl_z62sexitzf2Cinfozd2detachedzf3zd2setz12z71zzcfa_approxz00(obj_t
		BgL_envz00_7629, obj_t BgL_oz00_7630, obj_t BgL_vz00_7631)
	{
		{	/* Cfa/cinfo.sch 871 */
			return
				BGl_sexitzf2Cinfozd2detachedzf3zd2setz12z13zzcfa_approxz00(
				((BgL_sexitz00_bglt) BgL_oz00_7630), CBOOL(BgL_vz00_7631));
		}

	}



/* sexit/Cinfo-handler */
	BGL_EXPORTED_DEF obj_t
		BGl_sexitzf2Cinfozd2handlerz20zzcfa_approxz00(BgL_sexitz00_bglt
		BgL_oz00_369)
	{
		{	/* Cfa/cinfo.sch 872 */
			return
				(((BgL_sexitz00_bglt) COBJECT(
						((BgL_sexitz00_bglt) BgL_oz00_369)))->BgL_handlerz00);
		}

	}



/* &sexit/Cinfo-handler */
	obj_t BGl_z62sexitzf2Cinfozd2handlerz42zzcfa_approxz00(obj_t BgL_envz00_7632,
		obj_t BgL_oz00_7633)
	{
		{	/* Cfa/cinfo.sch 872 */
			return
				BGl_sexitzf2Cinfozd2handlerz20zzcfa_approxz00(
				((BgL_sexitz00_bglt) BgL_oz00_7633));
		}

	}



/* sexit/Cinfo-handler-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sexitzf2Cinfozd2handlerzd2setz12ze0zzcfa_approxz00(BgL_sexitz00_bglt
		BgL_oz00_370, obj_t BgL_vz00_371)
	{
		{	/* Cfa/cinfo.sch 873 */
			return
				((((BgL_sexitz00_bglt) COBJECT(
							((BgL_sexitz00_bglt) BgL_oz00_370)))->BgL_handlerz00) =
				((obj_t) BgL_vz00_371), BUNSPEC);
		}

	}



/* &sexit/Cinfo-handler-set! */
	obj_t BGl_z62sexitzf2Cinfozd2handlerzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_7634, obj_t BgL_oz00_7635, obj_t BgL_vz00_7636)
	{
		{	/* Cfa/cinfo.sch 873 */
			return
				BGl_sexitzf2Cinfozd2handlerzd2setz12ze0zzcfa_approxz00(
				((BgL_sexitz00_bglt) BgL_oz00_7635), BgL_vz00_7636);
		}

	}



/* make-reshaped-local */
	BGL_EXPORTED_DEF BgL_localz00_bglt
		BGl_makezd2reshapedzd2localz00zzcfa_approxz00(obj_t BgL_id1719z00_372,
		obj_t BgL_name1720z00_373, BgL_typez00_bglt BgL_type1721z00_374,
		BgL_valuez00_bglt BgL_value1722z00_375, obj_t BgL_access1723z00_376,
		obj_t BgL_fastzd2alpha1724zd2_377, obj_t BgL_removable1725z00_378,
		long BgL_occurrence1726z00_379, long BgL_occurrencew1727z00_380,
		bool_t BgL_userzf31728zf3_381, long BgL_key1729z00_382,
		obj_t BgL_valzd2noescape1730zd2_383, bool_t BgL_volatile1731z00_384,
		obj_t BgL_bindingzd2value1732zd2_385)
	{
		{	/* Cfa/cinfo.sch 876 */
			{	/* Cfa/cinfo.sch 876 */
				BgL_localz00_bglt BgL_new1213z00_8843;

				{	/* Cfa/cinfo.sch 876 */
					BgL_localz00_bglt BgL_tmp1211z00_8844;
					BgL_reshapedzd2localzd2_bglt BgL_wide1212z00_8845;

					{
						BgL_localz00_bglt BgL_auxz00_10900;

						{	/* Cfa/cinfo.sch 876 */
							BgL_localz00_bglt BgL_new1210z00_8846;

							BgL_new1210z00_8846 =
								((BgL_localz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_localz00_bgl))));
							{	/* Cfa/cinfo.sch 876 */
								long BgL_arg1699z00_8847;

								BgL_arg1699z00_8847 = BGL_CLASS_NUM(BGl_localz00zzast_varz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1210z00_8846),
									BgL_arg1699z00_8847);
							}
							{	/* Cfa/cinfo.sch 876 */
								BgL_objectz00_bglt BgL_tmpz00_10905;

								BgL_tmpz00_10905 = ((BgL_objectz00_bglt) BgL_new1210z00_8846);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10905, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1210z00_8846);
							BgL_auxz00_10900 = BgL_new1210z00_8846;
						}
						BgL_tmp1211z00_8844 = ((BgL_localz00_bglt) BgL_auxz00_10900);
					}
					BgL_wide1212z00_8845 =
						((BgL_reshapedzd2localzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_reshapedzd2localzd2_bgl))));
					{	/* Cfa/cinfo.sch 876 */
						obj_t BgL_auxz00_10913;
						BgL_objectz00_bglt BgL_tmpz00_10911;

						BgL_auxz00_10913 = ((obj_t) BgL_wide1212z00_8845);
						BgL_tmpz00_10911 = ((BgL_objectz00_bglt) BgL_tmp1211z00_8844);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10911, BgL_auxz00_10913);
					}
					((BgL_objectz00_bglt) BgL_tmp1211z00_8844);
					{	/* Cfa/cinfo.sch 876 */
						long BgL_arg1692z00_8848;

						BgL_arg1692z00_8848 =
							BGL_CLASS_NUM(BGl_reshapedzd2localzd2zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1211z00_8844), BgL_arg1692z00_8848);
					}
					BgL_new1213z00_8843 = ((BgL_localz00_bglt) BgL_tmp1211z00_8844);
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_new1213z00_8843)))->BgL_idz00) =
					((obj_t) BgL_id1719z00_372), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1213z00_8843)))->BgL_namez00) =
					((obj_t) BgL_name1720z00_373), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1213z00_8843)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1721z00_374), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1213z00_8843)))->BgL_valuez00) =
					((BgL_valuez00_bglt) BgL_value1722z00_375), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1213z00_8843)))->BgL_accessz00) =
					((obj_t) BgL_access1723z00_376), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1213z00_8843)))->BgL_fastzd2alphazd2) =
					((obj_t) BgL_fastzd2alpha1724zd2_377), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1213z00_8843)))->BgL_removablez00) =
					((obj_t) BgL_removable1725z00_378), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1213z00_8843)))->BgL_occurrencez00) =
					((long) BgL_occurrence1726z00_379), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1213z00_8843)))->BgL_occurrencewz00) =
					((long) BgL_occurrencew1727z00_380), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1213z00_8843)))->BgL_userzf3zf3) =
					((bool_t) BgL_userzf31728zf3_381), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
									BgL_new1213z00_8843)))->BgL_keyz00) =
					((long) BgL_key1729z00_382), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
									BgL_new1213z00_8843)))->BgL_valzd2noescapezd2) =
					((obj_t) BgL_valzd2noescape1730zd2_383), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
									BgL_new1213z00_8843)))->BgL_volatilez00) =
					((bool_t) BgL_volatile1731z00_384), BUNSPEC);
				{
					BgL_reshapedzd2localzd2_bglt BgL_auxz00_10947;

					{
						obj_t BgL_auxz00_10948;

						{	/* Cfa/cinfo.sch 876 */
							BgL_objectz00_bglt BgL_tmpz00_10949;

							BgL_tmpz00_10949 = ((BgL_objectz00_bglt) BgL_new1213z00_8843);
							BgL_auxz00_10948 = BGL_OBJECT_WIDENING(BgL_tmpz00_10949);
						}
						BgL_auxz00_10947 =
							((BgL_reshapedzd2localzd2_bglt) BgL_auxz00_10948);
					}
					((((BgL_reshapedzd2localzd2_bglt) COBJECT(BgL_auxz00_10947))->
							BgL_bindingzd2valuezd2) =
						((obj_t) BgL_bindingzd2value1732zd2_385), BUNSPEC);
				}
				return BgL_new1213z00_8843;
			}
		}

	}



/* &make-reshaped-local */
	BgL_localz00_bglt BGl_z62makezd2reshapedzd2localz62zzcfa_approxz00(obj_t
		BgL_envz00_7637, obj_t BgL_id1719z00_7638, obj_t BgL_name1720z00_7639,
		obj_t BgL_type1721z00_7640, obj_t BgL_value1722z00_7641,
		obj_t BgL_access1723z00_7642, obj_t BgL_fastzd2alpha1724zd2_7643,
		obj_t BgL_removable1725z00_7644, obj_t BgL_occurrence1726z00_7645,
		obj_t BgL_occurrencew1727z00_7646, obj_t BgL_userzf31728zf3_7647,
		obj_t BgL_key1729z00_7648, obj_t BgL_valzd2noescape1730zd2_7649,
		obj_t BgL_volatile1731z00_7650, obj_t BgL_bindingzd2value1732zd2_7651)
	{
		{	/* Cfa/cinfo.sch 876 */
			return
				BGl_makezd2reshapedzd2localz00zzcfa_approxz00(BgL_id1719z00_7638,
				BgL_name1720z00_7639, ((BgL_typez00_bglt) BgL_type1721z00_7640),
				((BgL_valuez00_bglt) BgL_value1722z00_7641), BgL_access1723z00_7642,
				BgL_fastzd2alpha1724zd2_7643, BgL_removable1725z00_7644,
				(long) CINT(BgL_occurrence1726z00_7645),
				(long) CINT(BgL_occurrencew1727z00_7646),
				CBOOL(BgL_userzf31728zf3_7647), (long) CINT(BgL_key1729z00_7648),
				BgL_valzd2noescape1730zd2_7649, CBOOL(BgL_volatile1731z00_7650),
				BgL_bindingzd2value1732zd2_7651);
		}

	}



/* reshaped-local? */
	BGL_EXPORTED_DEF bool_t BGl_reshapedzd2localzf3z21zzcfa_approxz00(obj_t
		BgL_objz00_386)
	{
		{	/* Cfa/cinfo.sch 877 */
			{	/* Cfa/cinfo.sch 877 */
				obj_t BgL_classz00_8849;

				BgL_classz00_8849 = BGl_reshapedzd2localzd2zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_386))
					{	/* Cfa/cinfo.sch 877 */
						BgL_objectz00_bglt BgL_arg1807z00_8850;

						BgL_arg1807z00_8850 = (BgL_objectz00_bglt) (BgL_objz00_386);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 877 */
								long BgL_idxz00_8851;

								BgL_idxz00_8851 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8850);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8851 + 3L)) == BgL_classz00_8849);
							}
						else
							{	/* Cfa/cinfo.sch 877 */
								bool_t BgL_res2176z00_8854;

								{	/* Cfa/cinfo.sch 877 */
									obj_t BgL_oclassz00_8855;

									{	/* Cfa/cinfo.sch 877 */
										obj_t BgL_arg1815z00_8856;
										long BgL_arg1816z00_8857;

										BgL_arg1815z00_8856 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 877 */
											long BgL_arg1817z00_8858;

											BgL_arg1817z00_8858 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8850);
											BgL_arg1816z00_8857 = (BgL_arg1817z00_8858 - OBJECT_TYPE);
										}
										BgL_oclassz00_8855 =
											VECTOR_REF(BgL_arg1815z00_8856, BgL_arg1816z00_8857);
									}
									{	/* Cfa/cinfo.sch 877 */
										bool_t BgL__ortest_1115z00_8859;

										BgL__ortest_1115z00_8859 =
											(BgL_classz00_8849 == BgL_oclassz00_8855);
										if (BgL__ortest_1115z00_8859)
											{	/* Cfa/cinfo.sch 877 */
												BgL_res2176z00_8854 = BgL__ortest_1115z00_8859;
											}
										else
											{	/* Cfa/cinfo.sch 877 */
												long BgL_odepthz00_8860;

												{	/* Cfa/cinfo.sch 877 */
													obj_t BgL_arg1804z00_8861;

													BgL_arg1804z00_8861 = (BgL_oclassz00_8855);
													BgL_odepthz00_8860 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8861);
												}
												if ((3L < BgL_odepthz00_8860))
													{	/* Cfa/cinfo.sch 877 */
														obj_t BgL_arg1802z00_8862;

														{	/* Cfa/cinfo.sch 877 */
															obj_t BgL_arg1803z00_8863;

															BgL_arg1803z00_8863 = (BgL_oclassz00_8855);
															BgL_arg1802z00_8862 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8863,
																3L);
														}
														BgL_res2176z00_8854 =
															(BgL_arg1802z00_8862 == BgL_classz00_8849);
													}
												else
													{	/* Cfa/cinfo.sch 877 */
														BgL_res2176z00_8854 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2176z00_8854;
							}
					}
				else
					{	/* Cfa/cinfo.sch 877 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &reshaped-local? */
	obj_t BGl_z62reshapedzd2localzf3z43zzcfa_approxz00(obj_t BgL_envz00_7652,
		obj_t BgL_objz00_7653)
	{
		{	/* Cfa/cinfo.sch 877 */
			return BBOOL(BGl_reshapedzd2localzf3z21zzcfa_approxz00(BgL_objz00_7653));
		}

	}



/* reshaped-local-nil */
	BGL_EXPORTED_DEF BgL_localz00_bglt
		BGl_reshapedzd2localzd2nilz00zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 878 */
			{	/* Cfa/cinfo.sch 878 */
				obj_t BgL_classz00_5221;

				BgL_classz00_5221 = BGl_reshapedzd2localzd2zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 878 */
					obj_t BgL__ortest_1117z00_5222;

					BgL__ortest_1117z00_5222 = BGL_CLASS_NIL(BgL_classz00_5221);
					if (CBOOL(BgL__ortest_1117z00_5222))
						{	/* Cfa/cinfo.sch 878 */
							return ((BgL_localz00_bglt) BgL__ortest_1117z00_5222);
						}
					else
						{	/* Cfa/cinfo.sch 878 */
							return
								((BgL_localz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5221));
						}
				}
			}
		}

	}



/* &reshaped-local-nil */
	BgL_localz00_bglt BGl_z62reshapedzd2localzd2nilz62zzcfa_approxz00(obj_t
		BgL_envz00_7654)
	{
		{	/* Cfa/cinfo.sch 878 */
			return BGl_reshapedzd2localzd2nilz00zzcfa_approxz00();
		}

	}



/* reshaped-local-binding-value */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2localzd2bindingzd2valuezd2zzcfa_approxz00(BgL_localz00_bglt
		BgL_oz00_387)
	{
		{	/* Cfa/cinfo.sch 879 */
			{
				BgL_reshapedzd2localzd2_bglt BgL_auxz00_10993;

				{
					obj_t BgL_auxz00_10994;

					{	/* Cfa/cinfo.sch 879 */
						BgL_objectz00_bglt BgL_tmpz00_10995;

						BgL_tmpz00_10995 = ((BgL_objectz00_bglt) BgL_oz00_387);
						BgL_auxz00_10994 = BGL_OBJECT_WIDENING(BgL_tmpz00_10995);
					}
					BgL_auxz00_10993 = ((BgL_reshapedzd2localzd2_bglt) BgL_auxz00_10994);
				}
				return
					(((BgL_reshapedzd2localzd2_bglt) COBJECT(BgL_auxz00_10993))->
					BgL_bindingzd2valuezd2);
			}
		}

	}



/* &reshaped-local-binding-value */
	obj_t BGl_z62reshapedzd2localzd2bindingzd2valuezb0zzcfa_approxz00(obj_t
		BgL_envz00_7655, obj_t BgL_oz00_7656)
	{
		{	/* Cfa/cinfo.sch 879 */
			return
				BGl_reshapedzd2localzd2bindingzd2valuezd2zzcfa_approxz00(
				((BgL_localz00_bglt) BgL_oz00_7656));
		}

	}



/* reshaped-local-volatile */
	BGL_EXPORTED_DEF bool_t
		BGl_reshapedzd2localzd2volatilez00zzcfa_approxz00(BgL_localz00_bglt
		BgL_oz00_390)
	{
		{	/* Cfa/cinfo.sch 881 */
			return
				(((BgL_localz00_bglt) COBJECT(
						((BgL_localz00_bglt) BgL_oz00_390)))->BgL_volatilez00);
		}

	}



/* &reshaped-local-volatile */
	obj_t BGl_z62reshapedzd2localzd2volatilez62zzcfa_approxz00(obj_t
		BgL_envz00_7657, obj_t BgL_oz00_7658)
	{
		{	/* Cfa/cinfo.sch 881 */
			return
				BBOOL(BGl_reshapedzd2localzd2volatilez00zzcfa_approxz00(
					((BgL_localz00_bglt) BgL_oz00_7658)));
		}

	}



/* reshaped-local-volatile-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2localzd2volatilezd2setz12zc0zzcfa_approxz00(BgL_localz00_bglt
		BgL_oz00_391, bool_t BgL_vz00_392)
	{
		{	/* Cfa/cinfo.sch 882 */
			return
				((((BgL_localz00_bglt) COBJECT(
							((BgL_localz00_bglt) BgL_oz00_391)))->BgL_volatilez00) =
				((bool_t) BgL_vz00_392), BUNSPEC);
		}

	}



/* &reshaped-local-volatile-set! */
	obj_t BGl_z62reshapedzd2localzd2volatilezd2setz12za2zzcfa_approxz00(obj_t
		BgL_envz00_7659, obj_t BgL_oz00_7660, obj_t BgL_vz00_7661)
	{
		{	/* Cfa/cinfo.sch 882 */
			return
				BGl_reshapedzd2localzd2volatilezd2setz12zc0zzcfa_approxz00(
				((BgL_localz00_bglt) BgL_oz00_7660), CBOOL(BgL_vz00_7661));
		}

	}



/* reshaped-local-val-noescape */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2localzd2valzd2noescapezd2zzcfa_approxz00(BgL_localz00_bglt
		BgL_oz00_393)
	{
		{	/* Cfa/cinfo.sch 883 */
			return
				(((BgL_localz00_bglt) COBJECT(
						((BgL_localz00_bglt) BgL_oz00_393)))->BgL_valzd2noescapezd2);
		}

	}



/* &reshaped-local-val-noescape */
	obj_t BGl_z62reshapedzd2localzd2valzd2noescapezb0zzcfa_approxz00(obj_t
		BgL_envz00_7662, obj_t BgL_oz00_7663)
	{
		{	/* Cfa/cinfo.sch 883 */
			return
				BGl_reshapedzd2localzd2valzd2noescapezd2zzcfa_approxz00(
				((BgL_localz00_bglt) BgL_oz00_7663));
		}

	}



/* reshaped-local-val-noescape-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2localzd2valzd2noescapezd2setz12z12zzcfa_approxz00
		(BgL_localz00_bglt BgL_oz00_394, obj_t BgL_vz00_395)
	{
		{	/* Cfa/cinfo.sch 884 */
			return
				((((BgL_localz00_bglt) COBJECT(
							((BgL_localz00_bglt) BgL_oz00_394)))->BgL_valzd2noescapezd2) =
				((obj_t) BgL_vz00_395), BUNSPEC);
		}

	}



/* &reshaped-local-val-noescape-set! */
	obj_t
		BGl_z62reshapedzd2localzd2valzd2noescapezd2setz12z70zzcfa_approxz00(obj_t
		BgL_envz00_7664, obj_t BgL_oz00_7665, obj_t BgL_vz00_7666)
	{
		{	/* Cfa/cinfo.sch 884 */
			return
				BGl_reshapedzd2localzd2valzd2noescapezd2setz12z12zzcfa_approxz00(
				((BgL_localz00_bglt) BgL_oz00_7665), BgL_vz00_7666);
		}

	}



/* reshaped-local-key */
	BGL_EXPORTED_DEF long
		BGl_reshapedzd2localzd2keyz00zzcfa_approxz00(BgL_localz00_bglt BgL_oz00_396)
	{
		{	/* Cfa/cinfo.sch 885 */
			return
				(((BgL_localz00_bglt) COBJECT(
						((BgL_localz00_bglt) BgL_oz00_396)))->BgL_keyz00);
		}

	}



/* &reshaped-local-key */
	obj_t BGl_z62reshapedzd2localzd2keyz62zzcfa_approxz00(obj_t BgL_envz00_7667,
		obj_t BgL_oz00_7668)
	{
		{	/* Cfa/cinfo.sch 885 */
			return
				BINT(BGl_reshapedzd2localzd2keyz00zzcfa_approxz00(
					((BgL_localz00_bglt) BgL_oz00_7668)));
		}

	}



/* reshaped-local-user? */
	BGL_EXPORTED_DEF bool_t
		BGl_reshapedzd2localzd2userzf3zf3zzcfa_approxz00(BgL_localz00_bglt
		BgL_oz00_399)
	{
		{	/* Cfa/cinfo.sch 887 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_399)))->BgL_userzf3zf3);
		}

	}



/* &reshaped-local-user? */
	obj_t BGl_z62reshapedzd2localzd2userzf3z91zzcfa_approxz00(obj_t
		BgL_envz00_7669, obj_t BgL_oz00_7670)
	{
		{	/* Cfa/cinfo.sch 887 */
			return
				BBOOL(BGl_reshapedzd2localzd2userzf3zf3zzcfa_approxz00(
					((BgL_localz00_bglt) BgL_oz00_7670)));
		}

	}



/* reshaped-local-user?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2localzd2userzf3zd2setz12z33zzcfa_approxz00(BgL_localz00_bglt
		BgL_oz00_400, bool_t BgL_vz00_401)
	{
		{	/* Cfa/cinfo.sch 888 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_400)))->BgL_userzf3zf3) =
				((bool_t) BgL_vz00_401), BUNSPEC);
		}

	}



/* &reshaped-local-user?-set! */
	obj_t BGl_z62reshapedzd2localzd2userzf3zd2setz12z51zzcfa_approxz00(obj_t
		BgL_envz00_7671, obj_t BgL_oz00_7672, obj_t BgL_vz00_7673)
	{
		{	/* Cfa/cinfo.sch 888 */
			return
				BGl_reshapedzd2localzd2userzf3zd2setz12z33zzcfa_approxz00(
				((BgL_localz00_bglt) BgL_oz00_7672), CBOOL(BgL_vz00_7673));
		}

	}



/* reshaped-local-occurrencew */
	BGL_EXPORTED_DEF long
		BGl_reshapedzd2localzd2occurrencewz00zzcfa_approxz00(BgL_localz00_bglt
		BgL_oz00_402)
	{
		{	/* Cfa/cinfo.sch 889 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_402)))->BgL_occurrencewz00);
		}

	}



/* &reshaped-local-occurrencew */
	obj_t BGl_z62reshapedzd2localzd2occurrencewz62zzcfa_approxz00(obj_t
		BgL_envz00_7674, obj_t BgL_oz00_7675)
	{
		{	/* Cfa/cinfo.sch 889 */
			return
				BINT(BGl_reshapedzd2localzd2occurrencewz00zzcfa_approxz00(
					((BgL_localz00_bglt) BgL_oz00_7675)));
		}

	}



/* reshaped-local-occurrencew-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2localzd2occurrencewzd2setz12zc0zzcfa_approxz00
		(BgL_localz00_bglt BgL_oz00_403, long BgL_vz00_404)
	{
		{	/* Cfa/cinfo.sch 890 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_403)))->BgL_occurrencewz00) =
				((long) BgL_vz00_404), BUNSPEC);
		}

	}



/* &reshaped-local-occurrencew-set! */
	obj_t BGl_z62reshapedzd2localzd2occurrencewzd2setz12za2zzcfa_approxz00(obj_t
		BgL_envz00_7676, obj_t BgL_oz00_7677, obj_t BgL_vz00_7678)
	{
		{	/* Cfa/cinfo.sch 890 */
			return
				BGl_reshapedzd2localzd2occurrencewzd2setz12zc0zzcfa_approxz00(
				((BgL_localz00_bglt) BgL_oz00_7677), (long) CINT(BgL_vz00_7678));
		}

	}



/* reshaped-local-occurrence */
	BGL_EXPORTED_DEF long
		BGl_reshapedzd2localzd2occurrencez00zzcfa_approxz00(BgL_localz00_bglt
		BgL_oz00_405)
	{
		{	/* Cfa/cinfo.sch 891 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_405)))->BgL_occurrencez00);
		}

	}



/* &reshaped-local-occurrence */
	obj_t BGl_z62reshapedzd2localzd2occurrencez62zzcfa_approxz00(obj_t
		BgL_envz00_7679, obj_t BgL_oz00_7680)
	{
		{	/* Cfa/cinfo.sch 891 */
			return
				BINT(BGl_reshapedzd2localzd2occurrencez00zzcfa_approxz00(
					((BgL_localz00_bglt) BgL_oz00_7680)));
		}

	}



/* reshaped-local-occurrence-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2localzd2occurrencezd2setz12zc0zzcfa_approxz00
		(BgL_localz00_bglt BgL_oz00_406, long BgL_vz00_407)
	{
		{	/* Cfa/cinfo.sch 892 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_406)))->BgL_occurrencez00) =
				((long) BgL_vz00_407), BUNSPEC);
		}

	}



/* &reshaped-local-occurrence-set! */
	obj_t BGl_z62reshapedzd2localzd2occurrencezd2setz12za2zzcfa_approxz00(obj_t
		BgL_envz00_7681, obj_t BgL_oz00_7682, obj_t BgL_vz00_7683)
	{
		{	/* Cfa/cinfo.sch 892 */
			return
				BGl_reshapedzd2localzd2occurrencezd2setz12zc0zzcfa_approxz00(
				((BgL_localz00_bglt) BgL_oz00_7682), (long) CINT(BgL_vz00_7683));
		}

	}



/* reshaped-local-removable */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2localzd2removablez00zzcfa_approxz00(BgL_localz00_bglt
		BgL_oz00_408)
	{
		{	/* Cfa/cinfo.sch 893 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_408)))->BgL_removablez00);
		}

	}



/* &reshaped-local-removable */
	obj_t BGl_z62reshapedzd2localzd2removablez62zzcfa_approxz00(obj_t
		BgL_envz00_7684, obj_t BgL_oz00_7685)
	{
		{	/* Cfa/cinfo.sch 893 */
			return
				BGl_reshapedzd2localzd2removablez00zzcfa_approxz00(
				((BgL_localz00_bglt) BgL_oz00_7685));
		}

	}



/* reshaped-local-removable-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2localzd2removablezd2setz12zc0zzcfa_approxz00
		(BgL_localz00_bglt BgL_oz00_409, obj_t BgL_vz00_410)
	{
		{	/* Cfa/cinfo.sch 894 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_409)))->BgL_removablez00) =
				((obj_t) BgL_vz00_410), BUNSPEC);
		}

	}



/* &reshaped-local-removable-set! */
	obj_t BGl_z62reshapedzd2localzd2removablezd2setz12za2zzcfa_approxz00(obj_t
		BgL_envz00_7686, obj_t BgL_oz00_7687, obj_t BgL_vz00_7688)
	{
		{	/* Cfa/cinfo.sch 894 */
			return
				BGl_reshapedzd2localzd2removablezd2setz12zc0zzcfa_approxz00(
				((BgL_localz00_bglt) BgL_oz00_7687), BgL_vz00_7688);
		}

	}



/* reshaped-local-fast-alpha */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2localzd2fastzd2alphazd2zzcfa_approxz00(BgL_localz00_bglt
		BgL_oz00_411)
	{
		{	/* Cfa/cinfo.sch 895 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_411)))->BgL_fastzd2alphazd2);
		}

	}



/* &reshaped-local-fast-alpha */
	obj_t BGl_z62reshapedzd2localzd2fastzd2alphazb0zzcfa_approxz00(obj_t
		BgL_envz00_7689, obj_t BgL_oz00_7690)
	{
		{	/* Cfa/cinfo.sch 895 */
			return
				BGl_reshapedzd2localzd2fastzd2alphazd2zzcfa_approxz00(
				((BgL_localz00_bglt) BgL_oz00_7690));
		}

	}



/* reshaped-local-fast-alpha-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2localzd2fastzd2alphazd2setz12z12zzcfa_approxz00
		(BgL_localz00_bglt BgL_oz00_412, obj_t BgL_vz00_413)
	{
		{	/* Cfa/cinfo.sch 896 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_412)))->BgL_fastzd2alphazd2) =
				((obj_t) BgL_vz00_413), BUNSPEC);
		}

	}



/* &reshaped-local-fast-alpha-set! */
	obj_t BGl_z62reshapedzd2localzd2fastzd2alphazd2setz12z70zzcfa_approxz00(obj_t
		BgL_envz00_7691, obj_t BgL_oz00_7692, obj_t BgL_vz00_7693)
	{
		{	/* Cfa/cinfo.sch 896 */
			return
				BGl_reshapedzd2localzd2fastzd2alphazd2setz12z12zzcfa_approxz00(
				((BgL_localz00_bglt) BgL_oz00_7692), BgL_vz00_7693);
		}

	}



/* reshaped-local-access */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2localzd2accessz00zzcfa_approxz00(BgL_localz00_bglt
		BgL_oz00_414)
	{
		{	/* Cfa/cinfo.sch 897 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_414)))->BgL_accessz00);
		}

	}



/* &reshaped-local-access */
	obj_t BGl_z62reshapedzd2localzd2accessz62zzcfa_approxz00(obj_t
		BgL_envz00_7694, obj_t BgL_oz00_7695)
	{
		{	/* Cfa/cinfo.sch 897 */
			return
				BGl_reshapedzd2localzd2accessz00zzcfa_approxz00(
				((BgL_localz00_bglt) BgL_oz00_7695));
		}

	}



/* reshaped-local-access-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2localzd2accesszd2setz12zc0zzcfa_approxz00(BgL_localz00_bglt
		BgL_oz00_415, obj_t BgL_vz00_416)
	{
		{	/* Cfa/cinfo.sch 898 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_415)))->BgL_accessz00) =
				((obj_t) BgL_vz00_416), BUNSPEC);
		}

	}



/* &reshaped-local-access-set! */
	obj_t BGl_z62reshapedzd2localzd2accesszd2setz12za2zzcfa_approxz00(obj_t
		BgL_envz00_7696, obj_t BgL_oz00_7697, obj_t BgL_vz00_7698)
	{
		{	/* Cfa/cinfo.sch 898 */
			return
				BGl_reshapedzd2localzd2accesszd2setz12zc0zzcfa_approxz00(
				((BgL_localz00_bglt) BgL_oz00_7697), BgL_vz00_7698);
		}

	}



/* reshaped-local-value */
	BGL_EXPORTED_DEF BgL_valuez00_bglt
		BGl_reshapedzd2localzd2valuez00zzcfa_approxz00(BgL_localz00_bglt
		BgL_oz00_417)
	{
		{	/* Cfa/cinfo.sch 899 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_417)))->BgL_valuez00);
		}

	}



/* &reshaped-local-value */
	BgL_valuez00_bglt BGl_z62reshapedzd2localzd2valuez62zzcfa_approxz00(obj_t
		BgL_envz00_7699, obj_t BgL_oz00_7700)
	{
		{	/* Cfa/cinfo.sch 899 */
			return
				BGl_reshapedzd2localzd2valuez00zzcfa_approxz00(
				((BgL_localz00_bglt) BgL_oz00_7700));
		}

	}



/* reshaped-local-value-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2localzd2valuezd2setz12zc0zzcfa_approxz00(BgL_localz00_bglt
		BgL_oz00_418, BgL_valuez00_bglt BgL_vz00_419)
	{
		{	/* Cfa/cinfo.sch 900 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_418)))->BgL_valuez00) =
				((BgL_valuez00_bglt) BgL_vz00_419), BUNSPEC);
		}

	}



/* &reshaped-local-value-set! */
	obj_t BGl_z62reshapedzd2localzd2valuezd2setz12za2zzcfa_approxz00(obj_t
		BgL_envz00_7701, obj_t BgL_oz00_7702, obj_t BgL_vz00_7703)
	{
		{	/* Cfa/cinfo.sch 900 */
			return
				BGl_reshapedzd2localzd2valuezd2setz12zc0zzcfa_approxz00(
				((BgL_localz00_bglt) BgL_oz00_7702),
				((BgL_valuez00_bglt) BgL_vz00_7703));
		}

	}



/* reshaped-local-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_reshapedzd2localzd2typez00zzcfa_approxz00(BgL_localz00_bglt
		BgL_oz00_420)
	{
		{	/* Cfa/cinfo.sch 901 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_420)))->BgL_typez00);
		}

	}



/* &reshaped-local-type */
	BgL_typez00_bglt BGl_z62reshapedzd2localzd2typez62zzcfa_approxz00(obj_t
		BgL_envz00_7704, obj_t BgL_oz00_7705)
	{
		{	/* Cfa/cinfo.sch 901 */
			return
				BGl_reshapedzd2localzd2typez00zzcfa_approxz00(
				((BgL_localz00_bglt) BgL_oz00_7705));
		}

	}



/* reshaped-local-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2localzd2typezd2setz12zc0zzcfa_approxz00(BgL_localz00_bglt
		BgL_oz00_421, BgL_typez00_bglt BgL_vz00_422)
	{
		{	/* Cfa/cinfo.sch 902 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_421)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_422), BUNSPEC);
		}

	}



/* &reshaped-local-type-set! */
	obj_t BGl_z62reshapedzd2localzd2typezd2setz12za2zzcfa_approxz00(obj_t
		BgL_envz00_7706, obj_t BgL_oz00_7707, obj_t BgL_vz00_7708)
	{
		{	/* Cfa/cinfo.sch 902 */
			return
				BGl_reshapedzd2localzd2typezd2setz12zc0zzcfa_approxz00(
				((BgL_localz00_bglt) BgL_oz00_7707),
				((BgL_typez00_bglt) BgL_vz00_7708));
		}

	}



/* reshaped-local-name */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2localzd2namez00zzcfa_approxz00(BgL_localz00_bglt
		BgL_oz00_423)
	{
		{	/* Cfa/cinfo.sch 903 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_423)))->BgL_namez00);
		}

	}



/* &reshaped-local-name */
	obj_t BGl_z62reshapedzd2localzd2namez62zzcfa_approxz00(obj_t BgL_envz00_7709,
		obj_t BgL_oz00_7710)
	{
		{	/* Cfa/cinfo.sch 903 */
			return
				BGl_reshapedzd2localzd2namez00zzcfa_approxz00(
				((BgL_localz00_bglt) BgL_oz00_7710));
		}

	}



/* reshaped-local-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2localzd2namezd2setz12zc0zzcfa_approxz00(BgL_localz00_bglt
		BgL_oz00_424, obj_t BgL_vz00_425)
	{
		{	/* Cfa/cinfo.sch 904 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_424)))->BgL_namez00) =
				((obj_t) BgL_vz00_425), BUNSPEC);
		}

	}



/* &reshaped-local-name-set! */
	obj_t BGl_z62reshapedzd2localzd2namezd2setz12za2zzcfa_approxz00(obj_t
		BgL_envz00_7711, obj_t BgL_oz00_7712, obj_t BgL_vz00_7713)
	{
		{	/* Cfa/cinfo.sch 904 */
			return
				BGl_reshapedzd2localzd2namezd2setz12zc0zzcfa_approxz00(
				((BgL_localz00_bglt) BgL_oz00_7712), BgL_vz00_7713);
		}

	}



/* reshaped-local-id */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2localzd2idz00zzcfa_approxz00(BgL_localz00_bglt BgL_oz00_426)
	{
		{	/* Cfa/cinfo.sch 905 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_426)))->BgL_idz00);
		}

	}



/* &reshaped-local-id */
	obj_t BGl_z62reshapedzd2localzd2idz62zzcfa_approxz00(obj_t BgL_envz00_7714,
		obj_t BgL_oz00_7715)
	{
		{	/* Cfa/cinfo.sch 905 */
			return
				BGl_reshapedzd2localzd2idz00zzcfa_approxz00(
				((BgL_localz00_bglt) BgL_oz00_7715));
		}

	}



/* make-reshaped-global */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_makezd2reshapedzd2globalz00zzcfa_approxz00(obj_t BgL_id1698z00_429,
		obj_t BgL_name1699z00_430, BgL_typez00_bglt BgL_type1700z00_431,
		BgL_valuez00_bglt BgL_value1701z00_432, obj_t BgL_access1702z00_433,
		obj_t BgL_fastzd2alpha1703zd2_434, obj_t BgL_removable1704z00_435,
		long BgL_occurrence1705z00_436, long BgL_occurrencew1706z00_437,
		bool_t BgL_userzf31707zf3_438, obj_t BgL_module1708z00_439,
		obj_t BgL_import1709z00_440, bool_t BgL_evaluablezf31710zf3_441,
		bool_t BgL_evalzf31711zf3_442, obj_t BgL_library1712z00_443,
		obj_t BgL_pragma1713z00_444, obj_t BgL_src1714z00_445,
		obj_t BgL_jvmzd2typezd2name1715z00_446, obj_t BgL_init1716z00_447,
		obj_t BgL_alias1717z00_448)
	{
		{	/* Cfa/cinfo.sch 909 */
			{	/* Cfa/cinfo.sch 909 */
				BgL_globalz00_bglt BgL_new1217z00_8864;

				{	/* Cfa/cinfo.sch 909 */
					BgL_globalz00_bglt BgL_tmp1215z00_8865;
					BgL_reshapedzd2globalzd2_bglt BgL_wide1216z00_8866;

					{
						BgL_globalz00_bglt BgL_auxz00_11109;

						{	/* Cfa/cinfo.sch 909 */
							BgL_globalz00_bglt BgL_new1214z00_8867;

							BgL_new1214z00_8867 =
								((BgL_globalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_globalz00_bgl))));
							{	/* Cfa/cinfo.sch 909 */
								long BgL_arg1701z00_8868;

								BgL_arg1701z00_8868 = BGL_CLASS_NUM(BGl_globalz00zzast_varz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1214z00_8867),
									BgL_arg1701z00_8868);
							}
							{	/* Cfa/cinfo.sch 909 */
								BgL_objectz00_bglt BgL_tmpz00_11114;

								BgL_tmpz00_11114 = ((BgL_objectz00_bglt) BgL_new1214z00_8867);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11114, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1214z00_8867);
							BgL_auxz00_11109 = BgL_new1214z00_8867;
						}
						BgL_tmp1215z00_8865 = ((BgL_globalz00_bglt) BgL_auxz00_11109);
					}
					BgL_wide1216z00_8866 =
						((BgL_reshapedzd2globalzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_reshapedzd2globalzd2_bgl))));
					{	/* Cfa/cinfo.sch 909 */
						obj_t BgL_auxz00_11122;
						BgL_objectz00_bglt BgL_tmpz00_11120;

						BgL_auxz00_11122 = ((obj_t) BgL_wide1216z00_8866);
						BgL_tmpz00_11120 = ((BgL_objectz00_bglt) BgL_tmp1215z00_8865);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11120, BgL_auxz00_11122);
					}
					((BgL_objectz00_bglt) BgL_tmp1215z00_8865);
					{	/* Cfa/cinfo.sch 909 */
						long BgL_arg1700z00_8869;

						BgL_arg1700z00_8869 =
							BGL_CLASS_NUM(BGl_reshapedzd2globalzd2zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1215z00_8865), BgL_arg1700z00_8869);
					}
					BgL_new1217z00_8864 = ((BgL_globalz00_bglt) BgL_tmp1215z00_8865);
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_new1217z00_8864)))->BgL_idz00) =
					((obj_t) BgL_id1698z00_429), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1217z00_8864)))->BgL_namez00) =
					((obj_t) BgL_name1699z00_430), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1217z00_8864)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1700z00_431), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1217z00_8864)))->BgL_valuez00) =
					((BgL_valuez00_bglt) BgL_value1701z00_432), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1217z00_8864)))->BgL_accessz00) =
					((obj_t) BgL_access1702z00_433), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1217z00_8864)))->BgL_fastzd2alphazd2) =
					((obj_t) BgL_fastzd2alpha1703zd2_434), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1217z00_8864)))->BgL_removablez00) =
					((obj_t) BgL_removable1704z00_435), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1217z00_8864)))->BgL_occurrencez00) =
					((long) BgL_occurrence1705z00_436), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1217z00_8864)))->BgL_occurrencewz00) =
					((long) BgL_occurrencew1706z00_437), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1217z00_8864)))->BgL_userzf3zf3) =
					((bool_t) BgL_userzf31707zf3_438), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1217z00_8864)))->BgL_modulez00) =
					((obj_t) BgL_module1708z00_439), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1217z00_8864)))->BgL_importz00) =
					((obj_t) BgL_import1709z00_440), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1217z00_8864)))->BgL_evaluablezf3zf3) =
					((bool_t) BgL_evaluablezf31710zf3_441), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1217z00_8864)))->BgL_evalzf3zf3) =
					((bool_t) BgL_evalzf31711zf3_442), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1217z00_8864)))->BgL_libraryz00) =
					((obj_t) BgL_library1712z00_443), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1217z00_8864)))->BgL_pragmaz00) =
					((obj_t) BgL_pragma1713z00_444), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1217z00_8864)))->BgL_srcz00) =
					((obj_t) BgL_src1714z00_445), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1217z00_8864)))->BgL_jvmzd2typezd2namez00) =
					((obj_t) BgL_jvmzd2typezd2name1715z00_446), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1217z00_8864)))->BgL_initz00) =
					((obj_t) BgL_init1716z00_447), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1217z00_8864)))->BgL_aliasz00) =
					((obj_t) BgL_alias1717z00_448), BUNSPEC);
				return BgL_new1217z00_8864;
			}
		}

	}



/* &make-reshaped-global */
	BgL_globalz00_bglt BGl_z62makezd2reshapedzd2globalz62zzcfa_approxz00(obj_t
		BgL_envz00_7716, obj_t BgL_id1698z00_7717, obj_t BgL_name1699z00_7718,
		obj_t BgL_type1700z00_7719, obj_t BgL_value1701z00_7720,
		obj_t BgL_access1702z00_7721, obj_t BgL_fastzd2alpha1703zd2_7722,
		obj_t BgL_removable1704z00_7723, obj_t BgL_occurrence1705z00_7724,
		obj_t BgL_occurrencew1706z00_7725, obj_t BgL_userzf31707zf3_7726,
		obj_t BgL_module1708z00_7727, obj_t BgL_import1709z00_7728,
		obj_t BgL_evaluablezf31710zf3_7729, obj_t BgL_evalzf31711zf3_7730,
		obj_t BgL_library1712z00_7731, obj_t BgL_pragma1713z00_7732,
		obj_t BgL_src1714z00_7733, obj_t BgL_jvmzd2typezd2name1715z00_7734,
		obj_t BgL_init1716z00_7735, obj_t BgL_alias1717z00_7736)
	{
		{	/* Cfa/cinfo.sch 909 */
			return
				BGl_makezd2reshapedzd2globalz00zzcfa_approxz00(BgL_id1698z00_7717,
				BgL_name1699z00_7718, ((BgL_typez00_bglt) BgL_type1700z00_7719),
				((BgL_valuez00_bglt) BgL_value1701z00_7720), BgL_access1702z00_7721,
				BgL_fastzd2alpha1703zd2_7722, BgL_removable1704z00_7723,
				(long) CINT(BgL_occurrence1705z00_7724),
				(long) CINT(BgL_occurrencew1706z00_7725),
				CBOOL(BgL_userzf31707zf3_7726), BgL_module1708z00_7727,
				BgL_import1709z00_7728, CBOOL(BgL_evaluablezf31710zf3_7729),
				CBOOL(BgL_evalzf31711zf3_7730), BgL_library1712z00_7731,
				BgL_pragma1713z00_7732, BgL_src1714z00_7733,
				BgL_jvmzd2typezd2name1715z00_7734, BgL_init1716z00_7735,
				BgL_alias1717z00_7736);
		}

	}



/* reshaped-global? */
	BGL_EXPORTED_DEF bool_t BGl_reshapedzd2globalzf3z21zzcfa_approxz00(obj_t
		BgL_objz00_449)
	{
		{	/* Cfa/cinfo.sch 910 */
			{	/* Cfa/cinfo.sch 910 */
				obj_t BgL_classz00_8870;

				BgL_classz00_8870 = BGl_reshapedzd2globalzd2zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_449))
					{	/* Cfa/cinfo.sch 910 */
						BgL_objectz00_bglt BgL_arg1807z00_8871;

						BgL_arg1807z00_8871 = (BgL_objectz00_bglt) (BgL_objz00_449);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 910 */
								long BgL_idxz00_8872;

								BgL_idxz00_8872 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8871);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8872 + 3L)) == BgL_classz00_8870);
							}
						else
							{	/* Cfa/cinfo.sch 910 */
								bool_t BgL_res2177z00_8875;

								{	/* Cfa/cinfo.sch 910 */
									obj_t BgL_oclassz00_8876;

									{	/* Cfa/cinfo.sch 910 */
										obj_t BgL_arg1815z00_8877;
										long BgL_arg1816z00_8878;

										BgL_arg1815z00_8877 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 910 */
											long BgL_arg1817z00_8879;

											BgL_arg1817z00_8879 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8871);
											BgL_arg1816z00_8878 = (BgL_arg1817z00_8879 - OBJECT_TYPE);
										}
										BgL_oclassz00_8876 =
											VECTOR_REF(BgL_arg1815z00_8877, BgL_arg1816z00_8878);
									}
									{	/* Cfa/cinfo.sch 910 */
										bool_t BgL__ortest_1115z00_8880;

										BgL__ortest_1115z00_8880 =
											(BgL_classz00_8870 == BgL_oclassz00_8876);
										if (BgL__ortest_1115z00_8880)
											{	/* Cfa/cinfo.sch 910 */
												BgL_res2177z00_8875 = BgL__ortest_1115z00_8880;
											}
										else
											{	/* Cfa/cinfo.sch 910 */
												long BgL_odepthz00_8881;

												{	/* Cfa/cinfo.sch 910 */
													obj_t BgL_arg1804z00_8882;

													BgL_arg1804z00_8882 = (BgL_oclassz00_8876);
													BgL_odepthz00_8881 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8882);
												}
												if ((3L < BgL_odepthz00_8881))
													{	/* Cfa/cinfo.sch 910 */
														obj_t BgL_arg1802z00_8883;

														{	/* Cfa/cinfo.sch 910 */
															obj_t BgL_arg1803z00_8884;

															BgL_arg1803z00_8884 = (BgL_oclassz00_8876);
															BgL_arg1802z00_8883 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8884,
																3L);
														}
														BgL_res2177z00_8875 =
															(BgL_arg1802z00_8883 == BgL_classz00_8870);
													}
												else
													{	/* Cfa/cinfo.sch 910 */
														BgL_res2177z00_8875 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2177z00_8875;
							}
					}
				else
					{	/* Cfa/cinfo.sch 910 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &reshaped-global? */
	obj_t BGl_z62reshapedzd2globalzf3z43zzcfa_approxz00(obj_t BgL_envz00_7737,
		obj_t BgL_objz00_7738)
	{
		{	/* Cfa/cinfo.sch 910 */
			return BBOOL(BGl_reshapedzd2globalzf3z21zzcfa_approxz00(BgL_objz00_7738));
		}

	}



/* reshaped-global-nil */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_reshapedzd2globalzd2nilz00zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 911 */
			{	/* Cfa/cinfo.sch 911 */
				obj_t BgL_classz00_5272;

				BgL_classz00_5272 = BGl_reshapedzd2globalzd2zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 911 */
					obj_t BgL__ortest_1117z00_5273;

					BgL__ortest_1117z00_5273 = BGL_CLASS_NIL(BgL_classz00_5272);
					if (CBOOL(BgL__ortest_1117z00_5273))
						{	/* Cfa/cinfo.sch 911 */
							return ((BgL_globalz00_bglt) BgL__ortest_1117z00_5273);
						}
					else
						{	/* Cfa/cinfo.sch 911 */
							return
								((BgL_globalz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5272));
						}
				}
			}
		}

	}



/* &reshaped-global-nil */
	BgL_globalz00_bglt BGl_z62reshapedzd2globalzd2nilz62zzcfa_approxz00(obj_t
		BgL_envz00_7739)
	{
		{	/* Cfa/cinfo.sch 911 */
			return BGl_reshapedzd2globalzd2nilz00zzcfa_approxz00();
		}

	}



/* reshaped-global-alias */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2aliasz00zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_450)
	{
		{	/* Cfa/cinfo.sch 912 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_450)))->BgL_aliasz00);
		}

	}



/* &reshaped-global-alias */
	obj_t BGl_z62reshapedzd2globalzd2aliasz62zzcfa_approxz00(obj_t
		BgL_envz00_7740, obj_t BgL_oz00_7741)
	{
		{	/* Cfa/cinfo.sch 912 */
			return
				BGl_reshapedzd2globalzd2aliasz00zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7741));
		}

	}



/* reshaped-global-alias-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2aliaszd2setz12zc0zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_451, obj_t BgL_vz00_452)
	{
		{	/* Cfa/cinfo.sch 913 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_451)))->BgL_aliasz00) =
				((obj_t) BgL_vz00_452), BUNSPEC);
		}

	}



/* &reshaped-global-alias-set! */
	obj_t BGl_z62reshapedzd2globalzd2aliaszd2setz12za2zzcfa_approxz00(obj_t
		BgL_envz00_7742, obj_t BgL_oz00_7743, obj_t BgL_vz00_7744)
	{
		{	/* Cfa/cinfo.sch 913 */
			return
				BGl_reshapedzd2globalzd2aliaszd2setz12zc0zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7743), BgL_vz00_7744);
		}

	}



/* reshaped-global-init */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2initz00zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_453)
	{
		{	/* Cfa/cinfo.sch 914 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_453)))->BgL_initz00);
		}

	}



/* &reshaped-global-init */
	obj_t BGl_z62reshapedzd2globalzd2initz62zzcfa_approxz00(obj_t BgL_envz00_7745,
		obj_t BgL_oz00_7746)
	{
		{	/* Cfa/cinfo.sch 914 */
			return
				BGl_reshapedzd2globalzd2initz00zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7746));
		}

	}



/* reshaped-global-init-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2initzd2setz12zc0zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_454, obj_t BgL_vz00_455)
	{
		{	/* Cfa/cinfo.sch 915 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_454)))->BgL_initz00) =
				((obj_t) BgL_vz00_455), BUNSPEC);
		}

	}



/* &reshaped-global-init-set! */
	obj_t BGl_z62reshapedzd2globalzd2initzd2setz12za2zzcfa_approxz00(obj_t
		BgL_envz00_7747, obj_t BgL_oz00_7748, obj_t BgL_vz00_7749)
	{
		{	/* Cfa/cinfo.sch 915 */
			return
				BGl_reshapedzd2globalzd2initzd2setz12zc0zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7748), BgL_vz00_7749);
		}

	}



/* reshaped-global-jvm-type-name */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2jvmzd2typezd2namez00zzcfa_approxz00
		(BgL_globalz00_bglt BgL_oz00_456)
	{
		{	/* Cfa/cinfo.sch 916 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_456)))->BgL_jvmzd2typezd2namez00);
		}

	}



/* &reshaped-global-jvm-type-name */
	obj_t BGl_z62reshapedzd2globalzd2jvmzd2typezd2namez62zzcfa_approxz00(obj_t
		BgL_envz00_7750, obj_t BgL_oz00_7751)
	{
		{	/* Cfa/cinfo.sch 916 */
			return
				BGl_reshapedzd2globalzd2jvmzd2typezd2namez00zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7751));
		}

	}



/* reshaped-global-jvm-type-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2jvmzd2typezd2namezd2setz12zc0zzcfa_approxz00
		(BgL_globalz00_bglt BgL_oz00_457, obj_t BgL_vz00_458)
	{
		{	/* Cfa/cinfo.sch 917 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_457)))->BgL_jvmzd2typezd2namez00) =
				((obj_t) BgL_vz00_458), BUNSPEC);
		}

	}



/* &reshaped-global-jvm-type-name-set! */
	obj_t
		BGl_z62reshapedzd2globalzd2jvmzd2typezd2namezd2setz12za2zzcfa_approxz00
		(obj_t BgL_envz00_7752, obj_t BgL_oz00_7753, obj_t BgL_vz00_7754)
	{
		{	/* Cfa/cinfo.sch 917 */
			return
				BGl_reshapedzd2globalzd2jvmzd2typezd2namezd2setz12zc0zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7753), BgL_vz00_7754);
		}

	}



/* reshaped-global-src */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2srcz00zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_459)
	{
		{	/* Cfa/cinfo.sch 918 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_459)))->BgL_srcz00);
		}

	}



/* &reshaped-global-src */
	obj_t BGl_z62reshapedzd2globalzd2srcz62zzcfa_approxz00(obj_t BgL_envz00_7755,
		obj_t BgL_oz00_7756)
	{
		{	/* Cfa/cinfo.sch 918 */
			return
				BGl_reshapedzd2globalzd2srcz00zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7756));
		}

	}



/* reshaped-global-src-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2srczd2setz12zc0zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_460, obj_t BgL_vz00_461)
	{
		{	/* Cfa/cinfo.sch 919 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_460)))->BgL_srcz00) =
				((obj_t) BgL_vz00_461), BUNSPEC);
		}

	}



/* &reshaped-global-src-set! */
	obj_t BGl_z62reshapedzd2globalzd2srczd2setz12za2zzcfa_approxz00(obj_t
		BgL_envz00_7757, obj_t BgL_oz00_7758, obj_t BgL_vz00_7759)
	{
		{	/* Cfa/cinfo.sch 919 */
			return
				BGl_reshapedzd2globalzd2srczd2setz12zc0zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7758), BgL_vz00_7759);
		}

	}



/* reshaped-global-pragma */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2pragmaz00zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_462)
	{
		{	/* Cfa/cinfo.sch 920 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_462)))->BgL_pragmaz00);
		}

	}



/* &reshaped-global-pragma */
	obj_t BGl_z62reshapedzd2globalzd2pragmaz62zzcfa_approxz00(obj_t
		BgL_envz00_7760, obj_t BgL_oz00_7761)
	{
		{	/* Cfa/cinfo.sch 920 */
			return
				BGl_reshapedzd2globalzd2pragmaz00zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7761));
		}

	}



/* reshaped-global-pragma-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2pragmazd2setz12zc0zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_463, obj_t BgL_vz00_464)
	{
		{	/* Cfa/cinfo.sch 921 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_463)))->BgL_pragmaz00) =
				((obj_t) BgL_vz00_464), BUNSPEC);
		}

	}



/* &reshaped-global-pragma-set! */
	obj_t BGl_z62reshapedzd2globalzd2pragmazd2setz12za2zzcfa_approxz00(obj_t
		BgL_envz00_7762, obj_t BgL_oz00_7763, obj_t BgL_vz00_7764)
	{
		{	/* Cfa/cinfo.sch 921 */
			return
				BGl_reshapedzd2globalzd2pragmazd2setz12zc0zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7763), BgL_vz00_7764);
		}

	}



/* reshaped-global-library */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2libraryz00zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_465)
	{
		{	/* Cfa/cinfo.sch 922 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_465)))->BgL_libraryz00);
		}

	}



/* &reshaped-global-library */
	obj_t BGl_z62reshapedzd2globalzd2libraryz62zzcfa_approxz00(obj_t
		BgL_envz00_7765, obj_t BgL_oz00_7766)
	{
		{	/* Cfa/cinfo.sch 922 */
			return
				BGl_reshapedzd2globalzd2libraryz00zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7766));
		}

	}



/* reshaped-global-library-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2libraryzd2setz12zc0zzcfa_approxz00
		(BgL_globalz00_bglt BgL_oz00_466, obj_t BgL_vz00_467)
	{
		{	/* Cfa/cinfo.sch 923 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_466)))->BgL_libraryz00) =
				((obj_t) BgL_vz00_467), BUNSPEC);
		}

	}



/* &reshaped-global-library-set! */
	obj_t BGl_z62reshapedzd2globalzd2libraryzd2setz12za2zzcfa_approxz00(obj_t
		BgL_envz00_7767, obj_t BgL_oz00_7768, obj_t BgL_vz00_7769)
	{
		{	/* Cfa/cinfo.sch 923 */
			return
				BGl_reshapedzd2globalzd2libraryzd2setz12zc0zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7768), BgL_vz00_7769);
		}

	}



/* reshaped-global-eval? */
	BGL_EXPORTED_DEF bool_t
		BGl_reshapedzd2globalzd2evalzf3zf3zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_468)
	{
		{	/* Cfa/cinfo.sch 924 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_468)))->BgL_evalzf3zf3);
		}

	}



/* &reshaped-global-eval? */
	obj_t BGl_z62reshapedzd2globalzd2evalzf3z91zzcfa_approxz00(obj_t
		BgL_envz00_7770, obj_t BgL_oz00_7771)
	{
		{	/* Cfa/cinfo.sch 924 */
			return
				BBOOL(BGl_reshapedzd2globalzd2evalzf3zf3zzcfa_approxz00(
					((BgL_globalz00_bglt) BgL_oz00_7771)));
		}

	}



/* reshaped-global-eval?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2evalzf3zd2setz12z33zzcfa_approxz00
		(BgL_globalz00_bglt BgL_oz00_469, bool_t BgL_vz00_470)
	{
		{	/* Cfa/cinfo.sch 925 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_469)))->BgL_evalzf3zf3) =
				((bool_t) BgL_vz00_470), BUNSPEC);
		}

	}



/* &reshaped-global-eval?-set! */
	obj_t BGl_z62reshapedzd2globalzd2evalzf3zd2setz12z51zzcfa_approxz00(obj_t
		BgL_envz00_7772, obj_t BgL_oz00_7773, obj_t BgL_vz00_7774)
	{
		{	/* Cfa/cinfo.sch 925 */
			return
				BGl_reshapedzd2globalzd2evalzf3zd2setz12z33zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7773), CBOOL(BgL_vz00_7774));
		}

	}



/* reshaped-global-evaluable? */
	BGL_EXPORTED_DEF bool_t
		BGl_reshapedzd2globalzd2evaluablezf3zf3zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_471)
	{
		{	/* Cfa/cinfo.sch 926 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_471)))->BgL_evaluablezf3zf3);
		}

	}



/* &reshaped-global-evaluable? */
	obj_t BGl_z62reshapedzd2globalzd2evaluablezf3z91zzcfa_approxz00(obj_t
		BgL_envz00_7775, obj_t BgL_oz00_7776)
	{
		{	/* Cfa/cinfo.sch 926 */
			return
				BBOOL(BGl_reshapedzd2globalzd2evaluablezf3zf3zzcfa_approxz00(
					((BgL_globalz00_bglt) BgL_oz00_7776)));
		}

	}



/* reshaped-global-evaluable?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2evaluablezf3zd2setz12z33zzcfa_approxz00
		(BgL_globalz00_bglt BgL_oz00_472, bool_t BgL_vz00_473)
	{
		{	/* Cfa/cinfo.sch 927 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_472)))->BgL_evaluablezf3zf3) =
				((bool_t) BgL_vz00_473), BUNSPEC);
		}

	}



/* &reshaped-global-evaluable?-set! */
	obj_t BGl_z62reshapedzd2globalzd2evaluablezf3zd2setz12z51zzcfa_approxz00(obj_t
		BgL_envz00_7777, obj_t BgL_oz00_7778, obj_t BgL_vz00_7779)
	{
		{	/* Cfa/cinfo.sch 927 */
			return
				BGl_reshapedzd2globalzd2evaluablezf3zd2setz12z33zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7778), CBOOL(BgL_vz00_7779));
		}

	}



/* reshaped-global-import */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2importz00zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_474)
	{
		{	/* Cfa/cinfo.sch 928 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_474)))->BgL_importz00);
		}

	}



/* &reshaped-global-import */
	obj_t BGl_z62reshapedzd2globalzd2importz62zzcfa_approxz00(obj_t
		BgL_envz00_7780, obj_t BgL_oz00_7781)
	{
		{	/* Cfa/cinfo.sch 928 */
			return
				BGl_reshapedzd2globalzd2importz00zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7781));
		}

	}



/* reshaped-global-import-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2importzd2setz12zc0zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_475, obj_t BgL_vz00_476)
	{
		{	/* Cfa/cinfo.sch 929 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_475)))->BgL_importz00) =
				((obj_t) BgL_vz00_476), BUNSPEC);
		}

	}



/* &reshaped-global-import-set! */
	obj_t BGl_z62reshapedzd2globalzd2importzd2setz12za2zzcfa_approxz00(obj_t
		BgL_envz00_7782, obj_t BgL_oz00_7783, obj_t BgL_vz00_7784)
	{
		{	/* Cfa/cinfo.sch 929 */
			return
				BGl_reshapedzd2globalzd2importzd2setz12zc0zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7783), BgL_vz00_7784);
		}

	}



/* reshaped-global-module */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2modulez00zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_477)
	{
		{	/* Cfa/cinfo.sch 930 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_477)))->BgL_modulez00);
		}

	}



/* &reshaped-global-module */
	obj_t BGl_z62reshapedzd2globalzd2modulez62zzcfa_approxz00(obj_t
		BgL_envz00_7785, obj_t BgL_oz00_7786)
	{
		{	/* Cfa/cinfo.sch 930 */
			return
				BGl_reshapedzd2globalzd2modulez00zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7786));
		}

	}



/* reshaped-global-module-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2modulezd2setz12zc0zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_478, obj_t BgL_vz00_479)
	{
		{	/* Cfa/cinfo.sch 931 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_478)))->BgL_modulez00) =
				((obj_t) BgL_vz00_479), BUNSPEC);
		}

	}



/* &reshaped-global-module-set! */
	obj_t BGl_z62reshapedzd2globalzd2modulezd2setz12za2zzcfa_approxz00(obj_t
		BgL_envz00_7787, obj_t BgL_oz00_7788, obj_t BgL_vz00_7789)
	{
		{	/* Cfa/cinfo.sch 931 */
			return
				BGl_reshapedzd2globalzd2modulezd2setz12zc0zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7788), BgL_vz00_7789);
		}

	}



/* reshaped-global-user? */
	BGL_EXPORTED_DEF bool_t
		BGl_reshapedzd2globalzd2userzf3zf3zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_480)
	{
		{	/* Cfa/cinfo.sch 932 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_480)))->BgL_userzf3zf3);
		}

	}



/* &reshaped-global-user? */
	obj_t BGl_z62reshapedzd2globalzd2userzf3z91zzcfa_approxz00(obj_t
		BgL_envz00_7790, obj_t BgL_oz00_7791)
	{
		{	/* Cfa/cinfo.sch 932 */
			return
				BBOOL(BGl_reshapedzd2globalzd2userzf3zf3zzcfa_approxz00(
					((BgL_globalz00_bglt) BgL_oz00_7791)));
		}

	}



/* reshaped-global-user?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2userzf3zd2setz12z33zzcfa_approxz00
		(BgL_globalz00_bglt BgL_oz00_481, bool_t BgL_vz00_482)
	{
		{	/* Cfa/cinfo.sch 933 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_481)))->BgL_userzf3zf3) =
				((bool_t) BgL_vz00_482), BUNSPEC);
		}

	}



/* &reshaped-global-user?-set! */
	obj_t BGl_z62reshapedzd2globalzd2userzf3zd2setz12z51zzcfa_approxz00(obj_t
		BgL_envz00_7792, obj_t BgL_oz00_7793, obj_t BgL_vz00_7794)
	{
		{	/* Cfa/cinfo.sch 933 */
			return
				BGl_reshapedzd2globalzd2userzf3zd2setz12z33zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7793), CBOOL(BgL_vz00_7794));
		}

	}



/* reshaped-global-occurrencew */
	BGL_EXPORTED_DEF long
		BGl_reshapedzd2globalzd2occurrencewz00zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_483)
	{
		{	/* Cfa/cinfo.sch 934 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_483)))->BgL_occurrencewz00);
		}

	}



/* &reshaped-global-occurrencew */
	obj_t BGl_z62reshapedzd2globalzd2occurrencewz62zzcfa_approxz00(obj_t
		BgL_envz00_7795, obj_t BgL_oz00_7796)
	{
		{	/* Cfa/cinfo.sch 934 */
			return
				BINT(BGl_reshapedzd2globalzd2occurrencewz00zzcfa_approxz00(
					((BgL_globalz00_bglt) BgL_oz00_7796)));
		}

	}



/* reshaped-global-occurrencew-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2occurrencewzd2setz12zc0zzcfa_approxz00
		(BgL_globalz00_bglt BgL_oz00_484, long BgL_vz00_485)
	{
		{	/* Cfa/cinfo.sch 935 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_484)))->BgL_occurrencewz00) =
				((long) BgL_vz00_485), BUNSPEC);
		}

	}



/* &reshaped-global-occurrencew-set! */
	obj_t BGl_z62reshapedzd2globalzd2occurrencewzd2setz12za2zzcfa_approxz00(obj_t
		BgL_envz00_7797, obj_t BgL_oz00_7798, obj_t BgL_vz00_7799)
	{
		{	/* Cfa/cinfo.sch 935 */
			return
				BGl_reshapedzd2globalzd2occurrencewzd2setz12zc0zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7798), (long) CINT(BgL_vz00_7799));
		}

	}



/* reshaped-global-occurrence */
	BGL_EXPORTED_DEF long
		BGl_reshapedzd2globalzd2occurrencez00zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_486)
	{
		{	/* Cfa/cinfo.sch 936 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_486)))->BgL_occurrencez00);
		}

	}



/* &reshaped-global-occurrence */
	obj_t BGl_z62reshapedzd2globalzd2occurrencez62zzcfa_approxz00(obj_t
		BgL_envz00_7800, obj_t BgL_oz00_7801)
	{
		{	/* Cfa/cinfo.sch 936 */
			return
				BINT(BGl_reshapedzd2globalzd2occurrencez00zzcfa_approxz00(
					((BgL_globalz00_bglt) BgL_oz00_7801)));
		}

	}



/* reshaped-global-occurrence-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2occurrencezd2setz12zc0zzcfa_approxz00
		(BgL_globalz00_bglt BgL_oz00_487, long BgL_vz00_488)
	{
		{	/* Cfa/cinfo.sch 937 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_487)))->BgL_occurrencez00) =
				((long) BgL_vz00_488), BUNSPEC);
		}

	}



/* &reshaped-global-occurrence-set! */
	obj_t BGl_z62reshapedzd2globalzd2occurrencezd2setz12za2zzcfa_approxz00(obj_t
		BgL_envz00_7802, obj_t BgL_oz00_7803, obj_t BgL_vz00_7804)
	{
		{	/* Cfa/cinfo.sch 937 */
			return
				BGl_reshapedzd2globalzd2occurrencezd2setz12zc0zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7803), (long) CINT(BgL_vz00_7804));
		}

	}



/* reshaped-global-removable */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2removablez00zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_489)
	{
		{	/* Cfa/cinfo.sch 938 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_489)))->BgL_removablez00);
		}

	}



/* &reshaped-global-removable */
	obj_t BGl_z62reshapedzd2globalzd2removablez62zzcfa_approxz00(obj_t
		BgL_envz00_7805, obj_t BgL_oz00_7806)
	{
		{	/* Cfa/cinfo.sch 938 */
			return
				BGl_reshapedzd2globalzd2removablez00zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7806));
		}

	}



/* reshaped-global-removable-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2removablezd2setz12zc0zzcfa_approxz00
		(BgL_globalz00_bglt BgL_oz00_490, obj_t BgL_vz00_491)
	{
		{	/* Cfa/cinfo.sch 939 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_490)))->BgL_removablez00) =
				((obj_t) BgL_vz00_491), BUNSPEC);
		}

	}



/* &reshaped-global-removable-set! */
	obj_t BGl_z62reshapedzd2globalzd2removablezd2setz12za2zzcfa_approxz00(obj_t
		BgL_envz00_7807, obj_t BgL_oz00_7808, obj_t BgL_vz00_7809)
	{
		{	/* Cfa/cinfo.sch 939 */
			return
				BGl_reshapedzd2globalzd2removablezd2setz12zc0zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7808), BgL_vz00_7809);
		}

	}



/* reshaped-global-fast-alpha */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2fastzd2alphazd2zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_492)
	{
		{	/* Cfa/cinfo.sch 940 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_492)))->BgL_fastzd2alphazd2);
		}

	}



/* &reshaped-global-fast-alpha */
	obj_t BGl_z62reshapedzd2globalzd2fastzd2alphazb0zzcfa_approxz00(obj_t
		BgL_envz00_7810, obj_t BgL_oz00_7811)
	{
		{	/* Cfa/cinfo.sch 940 */
			return
				BGl_reshapedzd2globalzd2fastzd2alphazd2zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7811));
		}

	}



/* reshaped-global-fast-alpha-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2fastzd2alphazd2setz12z12zzcfa_approxz00
		(BgL_globalz00_bglt BgL_oz00_493, obj_t BgL_vz00_494)
	{
		{	/* Cfa/cinfo.sch 941 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_493)))->BgL_fastzd2alphazd2) =
				((obj_t) BgL_vz00_494), BUNSPEC);
		}

	}



/* &reshaped-global-fast-alpha-set! */
	obj_t BGl_z62reshapedzd2globalzd2fastzd2alphazd2setz12z70zzcfa_approxz00(obj_t
		BgL_envz00_7812, obj_t BgL_oz00_7813, obj_t BgL_vz00_7814)
	{
		{	/* Cfa/cinfo.sch 941 */
			return
				BGl_reshapedzd2globalzd2fastzd2alphazd2setz12z12zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7813), BgL_vz00_7814);
		}

	}



/* reshaped-global-access */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2accessz00zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_495)
	{
		{	/* Cfa/cinfo.sch 942 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_495)))->BgL_accessz00);
		}

	}



/* &reshaped-global-access */
	obj_t BGl_z62reshapedzd2globalzd2accessz62zzcfa_approxz00(obj_t
		BgL_envz00_7815, obj_t BgL_oz00_7816)
	{
		{	/* Cfa/cinfo.sch 942 */
			return
				BGl_reshapedzd2globalzd2accessz00zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7816));
		}

	}



/* reshaped-global-access-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2accesszd2setz12zc0zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_496, obj_t BgL_vz00_497)
	{
		{	/* Cfa/cinfo.sch 943 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_496)))->BgL_accessz00) =
				((obj_t) BgL_vz00_497), BUNSPEC);
		}

	}



/* &reshaped-global-access-set! */
	obj_t BGl_z62reshapedzd2globalzd2accesszd2setz12za2zzcfa_approxz00(obj_t
		BgL_envz00_7817, obj_t BgL_oz00_7818, obj_t BgL_vz00_7819)
	{
		{	/* Cfa/cinfo.sch 943 */
			return
				BGl_reshapedzd2globalzd2accesszd2setz12zc0zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7818), BgL_vz00_7819);
		}

	}



/* reshaped-global-value */
	BGL_EXPORTED_DEF BgL_valuez00_bglt
		BGl_reshapedzd2globalzd2valuez00zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_498)
	{
		{	/* Cfa/cinfo.sch 944 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_498)))->BgL_valuez00);
		}

	}



/* &reshaped-global-value */
	BgL_valuez00_bglt BGl_z62reshapedzd2globalzd2valuez62zzcfa_approxz00(obj_t
		BgL_envz00_7820, obj_t BgL_oz00_7821)
	{
		{	/* Cfa/cinfo.sch 944 */
			return
				BGl_reshapedzd2globalzd2valuez00zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7821));
		}

	}



/* reshaped-global-value-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2valuezd2setz12zc0zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_499, BgL_valuez00_bglt BgL_vz00_500)
	{
		{	/* Cfa/cinfo.sch 945 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_499)))->BgL_valuez00) =
				((BgL_valuez00_bglt) BgL_vz00_500), BUNSPEC);
		}

	}



/* &reshaped-global-value-set! */
	obj_t BGl_z62reshapedzd2globalzd2valuezd2setz12za2zzcfa_approxz00(obj_t
		BgL_envz00_7822, obj_t BgL_oz00_7823, obj_t BgL_vz00_7824)
	{
		{	/* Cfa/cinfo.sch 945 */
			return
				BGl_reshapedzd2globalzd2valuezd2setz12zc0zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7823),
				((BgL_valuez00_bglt) BgL_vz00_7824));
		}

	}



/* reshaped-global-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_reshapedzd2globalzd2typez00zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_501)
	{
		{	/* Cfa/cinfo.sch 946 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_501)))->BgL_typez00);
		}

	}



/* &reshaped-global-type */
	BgL_typez00_bglt BGl_z62reshapedzd2globalzd2typez62zzcfa_approxz00(obj_t
		BgL_envz00_7825, obj_t BgL_oz00_7826)
	{
		{	/* Cfa/cinfo.sch 946 */
			return
				BGl_reshapedzd2globalzd2typez00zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7826));
		}

	}



/* reshaped-global-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2typezd2setz12zc0zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_502, BgL_typez00_bglt BgL_vz00_503)
	{
		{	/* Cfa/cinfo.sch 947 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_502)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_503), BUNSPEC);
		}

	}



/* &reshaped-global-type-set! */
	obj_t BGl_z62reshapedzd2globalzd2typezd2setz12za2zzcfa_approxz00(obj_t
		BgL_envz00_7827, obj_t BgL_oz00_7828, obj_t BgL_vz00_7829)
	{
		{	/* Cfa/cinfo.sch 947 */
			return
				BGl_reshapedzd2globalzd2typezd2setz12zc0zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7828),
				((BgL_typez00_bglt) BgL_vz00_7829));
		}

	}



/* reshaped-global-name */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2namez00zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_504)
	{
		{	/* Cfa/cinfo.sch 948 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_504)))->BgL_namez00);
		}

	}



/* &reshaped-global-name */
	obj_t BGl_z62reshapedzd2globalzd2namez62zzcfa_approxz00(obj_t BgL_envz00_7830,
		obj_t BgL_oz00_7831)
	{
		{	/* Cfa/cinfo.sch 948 */
			return
				BGl_reshapedzd2globalzd2namez00zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7831));
		}

	}



/* reshaped-global-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2namezd2setz12zc0zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_505, obj_t BgL_vz00_506)
	{
		{	/* Cfa/cinfo.sch 949 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_505)))->BgL_namez00) =
				((obj_t) BgL_vz00_506), BUNSPEC);
		}

	}



/* &reshaped-global-name-set! */
	obj_t BGl_z62reshapedzd2globalzd2namezd2setz12za2zzcfa_approxz00(obj_t
		BgL_envz00_7832, obj_t BgL_oz00_7833, obj_t BgL_vz00_7834)
	{
		{	/* Cfa/cinfo.sch 949 */
			return
				BGl_reshapedzd2globalzd2namezd2setz12zc0zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7833), BgL_vz00_7834);
		}

	}



/* reshaped-global-id */
	BGL_EXPORTED_DEF obj_t
		BGl_reshapedzd2globalzd2idz00zzcfa_approxz00(BgL_globalz00_bglt
		BgL_oz00_507)
	{
		{	/* Cfa/cinfo.sch 950 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_507)))->BgL_idz00);
		}

	}



/* &reshaped-global-id */
	obj_t BGl_z62reshapedzd2globalzd2idz62zzcfa_approxz00(obj_t BgL_envz00_7835,
		obj_t BgL_oz00_7836)
	{
		{	/* Cfa/cinfo.sch 950 */
			return
				BGl_reshapedzd2globalzd2idz00zzcfa_approxz00(
				((BgL_globalz00_bglt) BgL_oz00_7836));
		}

	}



/* make-literal/Cinfo */
	BGL_EXPORTED_DEF BgL_literalz00_bglt
		BGl_makezd2literalzf2Cinfoz20zzcfa_approxz00(obj_t BgL_loc1693z00_510,
		BgL_typez00_bglt BgL_type1694z00_511, obj_t BgL_value1695z00_512,
		BgL_approxz00_bglt BgL_approx1696z00_513)
	{
		{	/* Cfa/cinfo.sch 954 */
			{	/* Cfa/cinfo.sch 954 */
				BgL_literalz00_bglt BgL_new1221z00_8885;

				{	/* Cfa/cinfo.sch 954 */
					BgL_literalz00_bglt BgL_tmp1219z00_8886;
					BgL_literalzf2cinfozf2_bglt BgL_wide1220z00_8887;

					{
						BgL_literalz00_bglt BgL_auxz00_11377;

						{	/* Cfa/cinfo.sch 954 */
							BgL_literalz00_bglt BgL_new1218z00_8888;

							BgL_new1218z00_8888 =
								((BgL_literalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_literalz00_bgl))));
							{	/* Cfa/cinfo.sch 954 */
								long BgL_arg1703z00_8889;

								BgL_arg1703z00_8889 =
									BGL_CLASS_NUM(BGl_literalz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1218z00_8888),
									BgL_arg1703z00_8889);
							}
							{	/* Cfa/cinfo.sch 954 */
								BgL_objectz00_bglt BgL_tmpz00_11382;

								BgL_tmpz00_11382 = ((BgL_objectz00_bglt) BgL_new1218z00_8888);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11382, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1218z00_8888);
							BgL_auxz00_11377 = BgL_new1218z00_8888;
						}
						BgL_tmp1219z00_8886 = ((BgL_literalz00_bglt) BgL_auxz00_11377);
					}
					BgL_wide1220z00_8887 =
						((BgL_literalzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_literalzf2cinfozf2_bgl))));
					{	/* Cfa/cinfo.sch 954 */
						obj_t BgL_auxz00_11390;
						BgL_objectz00_bglt BgL_tmpz00_11388;

						BgL_auxz00_11390 = ((obj_t) BgL_wide1220z00_8887);
						BgL_tmpz00_11388 = ((BgL_objectz00_bglt) BgL_tmp1219z00_8886);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11388, BgL_auxz00_11390);
					}
					((BgL_objectz00_bglt) BgL_tmp1219z00_8886);
					{	/* Cfa/cinfo.sch 954 */
						long BgL_arg1702z00_8890;

						BgL_arg1702z00_8890 =
							BGL_CLASS_NUM(BGl_literalzf2Cinfozf2zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1219z00_8886), BgL_arg1702z00_8890);
					}
					BgL_new1221z00_8885 = ((BgL_literalz00_bglt) BgL_tmp1219z00_8886);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1221z00_8885)))->BgL_locz00) =
					((obj_t) BgL_loc1693z00_510), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1221z00_8885)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1694z00_511), BUNSPEC);
				((((BgL_atomz00_bglt) COBJECT(((BgL_atomz00_bglt)
									BgL_new1221z00_8885)))->BgL_valuez00) =
					((obj_t) BgL_value1695z00_512), BUNSPEC);
				{
					BgL_literalzf2cinfozf2_bglt BgL_auxz00_11404;

					{
						obj_t BgL_auxz00_11405;

						{	/* Cfa/cinfo.sch 954 */
							BgL_objectz00_bglt BgL_tmpz00_11406;

							BgL_tmpz00_11406 = ((BgL_objectz00_bglt) BgL_new1221z00_8885);
							BgL_auxz00_11405 = BGL_OBJECT_WIDENING(BgL_tmpz00_11406);
						}
						BgL_auxz00_11404 = ((BgL_literalzf2cinfozf2_bglt) BgL_auxz00_11405);
					}
					((((BgL_literalzf2cinfozf2_bglt) COBJECT(BgL_auxz00_11404))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1696z00_513), BUNSPEC);
				}
				return BgL_new1221z00_8885;
			}
		}

	}



/* &make-literal/Cinfo */
	BgL_literalz00_bglt BGl_z62makezd2literalzf2Cinfoz42zzcfa_approxz00(obj_t
		BgL_envz00_7837, obj_t BgL_loc1693z00_7838, obj_t BgL_type1694z00_7839,
		obj_t BgL_value1695z00_7840, obj_t BgL_approx1696z00_7841)
	{
		{	/* Cfa/cinfo.sch 954 */
			return
				BGl_makezd2literalzf2Cinfoz20zzcfa_approxz00(BgL_loc1693z00_7838,
				((BgL_typez00_bglt) BgL_type1694z00_7839), BgL_value1695z00_7840,
				((BgL_approxz00_bglt) BgL_approx1696z00_7841));
		}

	}



/* literal/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_literalzf2Cinfozf3z01zzcfa_approxz00(obj_t
		BgL_objz00_514)
	{
		{	/* Cfa/cinfo.sch 955 */
			{	/* Cfa/cinfo.sch 955 */
				obj_t BgL_classz00_8891;

				BgL_classz00_8891 = BGl_literalzf2Cinfozf2zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_514))
					{	/* Cfa/cinfo.sch 955 */
						BgL_objectz00_bglt BgL_arg1807z00_8892;

						BgL_arg1807z00_8892 = (BgL_objectz00_bglt) (BgL_objz00_514);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 955 */
								long BgL_idxz00_8893;

								BgL_idxz00_8893 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8892);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8893 + 4L)) == BgL_classz00_8891);
							}
						else
							{	/* Cfa/cinfo.sch 955 */
								bool_t BgL_res2178z00_8896;

								{	/* Cfa/cinfo.sch 955 */
									obj_t BgL_oclassz00_8897;

									{	/* Cfa/cinfo.sch 955 */
										obj_t BgL_arg1815z00_8898;
										long BgL_arg1816z00_8899;

										BgL_arg1815z00_8898 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 955 */
											long BgL_arg1817z00_8900;

											BgL_arg1817z00_8900 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8892);
											BgL_arg1816z00_8899 = (BgL_arg1817z00_8900 - OBJECT_TYPE);
										}
										BgL_oclassz00_8897 =
											VECTOR_REF(BgL_arg1815z00_8898, BgL_arg1816z00_8899);
									}
									{	/* Cfa/cinfo.sch 955 */
										bool_t BgL__ortest_1115z00_8901;

										BgL__ortest_1115z00_8901 =
											(BgL_classz00_8891 == BgL_oclassz00_8897);
										if (BgL__ortest_1115z00_8901)
											{	/* Cfa/cinfo.sch 955 */
												BgL_res2178z00_8896 = BgL__ortest_1115z00_8901;
											}
										else
											{	/* Cfa/cinfo.sch 955 */
												long BgL_odepthz00_8902;

												{	/* Cfa/cinfo.sch 955 */
													obj_t BgL_arg1804z00_8903;

													BgL_arg1804z00_8903 = (BgL_oclassz00_8897);
													BgL_odepthz00_8902 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8903);
												}
												if ((4L < BgL_odepthz00_8902))
													{	/* Cfa/cinfo.sch 955 */
														obj_t BgL_arg1802z00_8904;

														{	/* Cfa/cinfo.sch 955 */
															obj_t BgL_arg1803z00_8905;

															BgL_arg1803z00_8905 = (BgL_oclassz00_8897);
															BgL_arg1802z00_8904 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8905,
																4L);
														}
														BgL_res2178z00_8896 =
															(BgL_arg1802z00_8904 == BgL_classz00_8891);
													}
												else
													{	/* Cfa/cinfo.sch 955 */
														BgL_res2178z00_8896 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2178z00_8896;
							}
					}
				else
					{	/* Cfa/cinfo.sch 955 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &literal/Cinfo? */
	obj_t BGl_z62literalzf2Cinfozf3z63zzcfa_approxz00(obj_t BgL_envz00_7842,
		obj_t BgL_objz00_7843)
	{
		{	/* Cfa/cinfo.sch 955 */
			return BBOOL(BGl_literalzf2Cinfozf3z01zzcfa_approxz00(BgL_objz00_7843));
		}

	}



/* literal/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_literalz00_bglt
		BGl_literalzf2Cinfozd2nilz20zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 956 */
			{	/* Cfa/cinfo.sch 956 */
				obj_t BgL_classz00_5323;

				BgL_classz00_5323 = BGl_literalzf2Cinfozf2zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 956 */
					obj_t BgL__ortest_1117z00_5324;

					BgL__ortest_1117z00_5324 = BGL_CLASS_NIL(BgL_classz00_5323);
					if (CBOOL(BgL__ortest_1117z00_5324))
						{	/* Cfa/cinfo.sch 956 */
							return ((BgL_literalz00_bglt) BgL__ortest_1117z00_5324);
						}
					else
						{	/* Cfa/cinfo.sch 956 */
							return
								((BgL_literalz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5323));
						}
				}
			}
		}

	}



/* &literal/Cinfo-nil */
	BgL_literalz00_bglt BGl_z62literalzf2Cinfozd2nilz42zzcfa_approxz00(obj_t
		BgL_envz00_7844)
	{
		{	/* Cfa/cinfo.sch 956 */
			return BGl_literalzf2Cinfozd2nilz20zzcfa_approxz00();
		}

	}



/* literal/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_literalzf2Cinfozd2approxz20zzcfa_approxz00(BgL_literalz00_bglt
		BgL_oz00_515)
	{
		{	/* Cfa/cinfo.sch 957 */
			{
				BgL_literalzf2cinfozf2_bglt BgL_auxz00_11445;

				{
					obj_t BgL_auxz00_11446;

					{	/* Cfa/cinfo.sch 957 */
						BgL_objectz00_bglt BgL_tmpz00_11447;

						BgL_tmpz00_11447 = ((BgL_objectz00_bglt) BgL_oz00_515);
						BgL_auxz00_11446 = BGL_OBJECT_WIDENING(BgL_tmpz00_11447);
					}
					BgL_auxz00_11445 = ((BgL_literalzf2cinfozf2_bglt) BgL_auxz00_11446);
				}
				return
					(((BgL_literalzf2cinfozf2_bglt) COBJECT(BgL_auxz00_11445))->
					BgL_approxz00);
			}
		}

	}



/* &literal/Cinfo-approx */
	BgL_approxz00_bglt BGl_z62literalzf2Cinfozd2approxz42zzcfa_approxz00(obj_t
		BgL_envz00_7845, obj_t BgL_oz00_7846)
	{
		{	/* Cfa/cinfo.sch 957 */
			return
				BGl_literalzf2Cinfozd2approxz20zzcfa_approxz00(
				((BgL_literalz00_bglt) BgL_oz00_7846));
		}

	}



/* literal/Cinfo-value */
	BGL_EXPORTED_DEF obj_t
		BGl_literalzf2Cinfozd2valuez20zzcfa_approxz00(BgL_literalz00_bglt
		BgL_oz00_518)
	{
		{	/* Cfa/cinfo.sch 959 */
			return
				(((BgL_atomz00_bglt) COBJECT(
						((BgL_atomz00_bglt) BgL_oz00_518)))->BgL_valuez00);
		}

	}



/* &literal/Cinfo-value */
	obj_t BGl_z62literalzf2Cinfozd2valuez42zzcfa_approxz00(obj_t BgL_envz00_7847,
		obj_t BgL_oz00_7848)
	{
		{	/* Cfa/cinfo.sch 959 */
			return
				BGl_literalzf2Cinfozd2valuez20zzcfa_approxz00(
				((BgL_literalz00_bglt) BgL_oz00_7848));
		}

	}



/* literal/Cinfo-value-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_literalzf2Cinfozd2valuezd2setz12ze0zzcfa_approxz00(BgL_literalz00_bglt
		BgL_oz00_519, obj_t BgL_vz00_520)
	{
		{	/* Cfa/cinfo.sch 960 */
			return
				((((BgL_atomz00_bglt) COBJECT(
							((BgL_atomz00_bglt) BgL_oz00_519)))->BgL_valuez00) =
				((obj_t) BgL_vz00_520), BUNSPEC);
		}

	}



/* &literal/Cinfo-value-set! */
	obj_t BGl_z62literalzf2Cinfozd2valuezd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_7849, obj_t BgL_oz00_7850, obj_t BgL_vz00_7851)
	{
		{	/* Cfa/cinfo.sch 960 */
			return
				BGl_literalzf2Cinfozd2valuezd2setz12ze0zzcfa_approxz00(
				((BgL_literalz00_bglt) BgL_oz00_7850), BgL_vz00_7851);
		}

	}



/* literal/Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_literalzf2Cinfozd2typez20zzcfa_approxz00(BgL_literalz00_bglt
		BgL_oz00_521)
	{
		{	/* Cfa/cinfo.sch 961 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_521)))->BgL_typez00);
		}

	}



/* &literal/Cinfo-type */
	BgL_typez00_bglt BGl_z62literalzf2Cinfozd2typez42zzcfa_approxz00(obj_t
		BgL_envz00_7852, obj_t BgL_oz00_7853)
	{
		{	/* Cfa/cinfo.sch 961 */
			return
				BGl_literalzf2Cinfozd2typez20zzcfa_approxz00(
				((BgL_literalz00_bglt) BgL_oz00_7853));
		}

	}



/* literal/Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_literalzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00(BgL_literalz00_bglt
		BgL_oz00_522, BgL_typez00_bglt BgL_vz00_523)
	{
		{	/* Cfa/cinfo.sch 962 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_522)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_523), BUNSPEC);
		}

	}



/* &literal/Cinfo-type-set! */
	obj_t BGl_z62literalzf2Cinfozd2typezd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_7854, obj_t BgL_oz00_7855, obj_t BgL_vz00_7856)
	{
		{	/* Cfa/cinfo.sch 962 */
			return
				BGl_literalzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00(
				((BgL_literalz00_bglt) BgL_oz00_7855),
				((BgL_typez00_bglt) BgL_vz00_7856));
		}

	}



/* literal/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_literalzf2Cinfozd2locz20zzcfa_approxz00(BgL_literalz00_bglt
		BgL_oz00_524)
	{
		{	/* Cfa/cinfo.sch 963 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_524)))->BgL_locz00);
		}

	}



/* &literal/Cinfo-loc */
	obj_t BGl_z62literalzf2Cinfozd2locz42zzcfa_approxz00(obj_t BgL_envz00_7857,
		obj_t BgL_oz00_7858)
	{
		{	/* Cfa/cinfo.sch 963 */
			return
				BGl_literalzf2Cinfozd2locz20zzcfa_approxz00(
				((BgL_literalz00_bglt) BgL_oz00_7858));
		}

	}



/* make-genpatchid/Cinfo */
	BGL_EXPORTED_DEF BgL_genpatchidz00_bglt
		BGl_makezd2genpatchidzf2Cinfoz20zzcfa_approxz00(obj_t BgL_loc1683z00_527,
		BgL_typez00_bglt BgL_type1684z00_528, obj_t BgL_sidezd2effect1685zd2_529,
		obj_t BgL_key1686z00_530, obj_t BgL_exprza21687za2_531,
		obj_t BgL_effect1688z00_532, long BgL_index1689z00_533,
		long BgL_rindex1690z00_534, BgL_approxz00_bglt BgL_approx1691z00_535)
	{
		{	/* Cfa/cinfo.sch 967 */
			{	/* Cfa/cinfo.sch 967 */
				BgL_genpatchidz00_bglt BgL_new1225z00_8906;

				{	/* Cfa/cinfo.sch 967 */
					BgL_genpatchidz00_bglt BgL_tmp1223z00_8907;
					BgL_genpatchidzf2cinfozf2_bglt BgL_wide1224z00_8908;

					{
						BgL_genpatchidz00_bglt BgL_auxz00_11475;

						{	/* Cfa/cinfo.sch 967 */
							BgL_genpatchidz00_bglt BgL_new1222z00_8909;

							BgL_new1222z00_8909 =
								((BgL_genpatchidz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_genpatchidz00_bgl))));
							{	/* Cfa/cinfo.sch 967 */
								long BgL_arg1708z00_8910;

								BgL_arg1708z00_8910 =
									BGL_CLASS_NUM(BGl_genpatchidz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1222z00_8909),
									BgL_arg1708z00_8910);
							}
							{	/* Cfa/cinfo.sch 967 */
								BgL_objectz00_bglt BgL_tmpz00_11480;

								BgL_tmpz00_11480 = ((BgL_objectz00_bglt) BgL_new1222z00_8909);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11480, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1222z00_8909);
							BgL_auxz00_11475 = BgL_new1222z00_8909;
						}
						BgL_tmp1223z00_8907 = ((BgL_genpatchidz00_bglt) BgL_auxz00_11475);
					}
					BgL_wide1224z00_8908 =
						((BgL_genpatchidzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_genpatchidzf2cinfozf2_bgl))));
					{	/* Cfa/cinfo.sch 967 */
						obj_t BgL_auxz00_11488;
						BgL_objectz00_bglt BgL_tmpz00_11486;

						BgL_auxz00_11488 = ((obj_t) BgL_wide1224z00_8908);
						BgL_tmpz00_11486 = ((BgL_objectz00_bglt) BgL_tmp1223z00_8907);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11486, BgL_auxz00_11488);
					}
					((BgL_objectz00_bglt) BgL_tmp1223z00_8907);
					{	/* Cfa/cinfo.sch 967 */
						long BgL_arg1705z00_8911;

						BgL_arg1705z00_8911 =
							BGL_CLASS_NUM(BGl_genpatchidzf2Cinfozf2zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1223z00_8907), BgL_arg1705z00_8911);
					}
					BgL_new1225z00_8906 = ((BgL_genpatchidz00_bglt) BgL_tmp1223z00_8907);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1225z00_8906)))->BgL_locz00) =
					((obj_t) BgL_loc1683z00_527), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1225z00_8906)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1684z00_528), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1225z00_8906)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1685zd2_529), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1225z00_8906)))->BgL_keyz00) =
					((obj_t) BgL_key1686z00_530), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1225z00_8906)))->BgL_exprza2za2) =
					((obj_t) BgL_exprza21687za2_531), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1225z00_8906)))->BgL_effectz00) =
					((obj_t) BgL_effect1688z00_532), BUNSPEC);
				((((BgL_genpatchidz00_bglt) COBJECT(((BgL_genpatchidz00_bglt)
									BgL_new1225z00_8906)))->BgL_indexz00) =
					((long) BgL_index1689z00_533), BUNSPEC);
				((((BgL_genpatchidz00_bglt) COBJECT(((BgL_genpatchidz00_bglt)
									BgL_new1225z00_8906)))->BgL_rindexz00) =
					((long) BgL_rindex1690z00_534), BUNSPEC);
				{
					BgL_genpatchidzf2cinfozf2_bglt BgL_auxz00_11512;

					{
						obj_t BgL_auxz00_11513;

						{	/* Cfa/cinfo.sch 967 */
							BgL_objectz00_bglt BgL_tmpz00_11514;

							BgL_tmpz00_11514 = ((BgL_objectz00_bglt) BgL_new1225z00_8906);
							BgL_auxz00_11513 = BGL_OBJECT_WIDENING(BgL_tmpz00_11514);
						}
						BgL_auxz00_11512 =
							((BgL_genpatchidzf2cinfozf2_bglt) BgL_auxz00_11513);
					}
					((((BgL_genpatchidzf2cinfozf2_bglt) COBJECT(BgL_auxz00_11512))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1691z00_535), BUNSPEC);
				}
				return BgL_new1225z00_8906;
			}
		}

	}



/* &make-genpatchid/Cinfo */
	BgL_genpatchidz00_bglt
		BGl_z62makezd2genpatchidzf2Cinfoz42zzcfa_approxz00(obj_t BgL_envz00_7859,
		obj_t BgL_loc1683z00_7860, obj_t BgL_type1684z00_7861,
		obj_t BgL_sidezd2effect1685zd2_7862, obj_t BgL_key1686z00_7863,
		obj_t BgL_exprza21687za2_7864, obj_t BgL_effect1688z00_7865,
		obj_t BgL_index1689z00_7866, obj_t BgL_rindex1690z00_7867,
		obj_t BgL_approx1691z00_7868)
	{
		{	/* Cfa/cinfo.sch 967 */
			return
				BGl_makezd2genpatchidzf2Cinfoz20zzcfa_approxz00(BgL_loc1683z00_7860,
				((BgL_typez00_bglt) BgL_type1684z00_7861),
				BgL_sidezd2effect1685zd2_7862, BgL_key1686z00_7863,
				BgL_exprza21687za2_7864, BgL_effect1688z00_7865,
				(long) CINT(BgL_index1689z00_7866), (long) CINT(BgL_rindex1690z00_7867),
				((BgL_approxz00_bglt) BgL_approx1691z00_7868));
		}

	}



/* genpatchid/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_genpatchidzf2Cinfozf3z01zzcfa_approxz00(obj_t
		BgL_objz00_536)
	{
		{	/* Cfa/cinfo.sch 968 */
			{	/* Cfa/cinfo.sch 968 */
				obj_t BgL_classz00_8912;

				BgL_classz00_8912 = BGl_genpatchidzf2Cinfozf2zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_536))
					{	/* Cfa/cinfo.sch 968 */
						BgL_objectz00_bglt BgL_arg1807z00_8913;

						BgL_arg1807z00_8913 = (BgL_objectz00_bglt) (BgL_objz00_536);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 968 */
								long BgL_idxz00_8914;

								BgL_idxz00_8914 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8913);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8914 + 5L)) == BgL_classz00_8912);
							}
						else
							{	/* Cfa/cinfo.sch 968 */
								bool_t BgL_res2179z00_8917;

								{	/* Cfa/cinfo.sch 968 */
									obj_t BgL_oclassz00_8918;

									{	/* Cfa/cinfo.sch 968 */
										obj_t BgL_arg1815z00_8919;
										long BgL_arg1816z00_8920;

										BgL_arg1815z00_8919 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 968 */
											long BgL_arg1817z00_8921;

											BgL_arg1817z00_8921 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8913);
											BgL_arg1816z00_8920 = (BgL_arg1817z00_8921 - OBJECT_TYPE);
										}
										BgL_oclassz00_8918 =
											VECTOR_REF(BgL_arg1815z00_8919, BgL_arg1816z00_8920);
									}
									{	/* Cfa/cinfo.sch 968 */
										bool_t BgL__ortest_1115z00_8922;

										BgL__ortest_1115z00_8922 =
											(BgL_classz00_8912 == BgL_oclassz00_8918);
										if (BgL__ortest_1115z00_8922)
											{	/* Cfa/cinfo.sch 968 */
												BgL_res2179z00_8917 = BgL__ortest_1115z00_8922;
											}
										else
											{	/* Cfa/cinfo.sch 968 */
												long BgL_odepthz00_8923;

												{	/* Cfa/cinfo.sch 968 */
													obj_t BgL_arg1804z00_8924;

													BgL_arg1804z00_8924 = (BgL_oclassz00_8918);
													BgL_odepthz00_8923 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8924);
												}
												if ((5L < BgL_odepthz00_8923))
													{	/* Cfa/cinfo.sch 968 */
														obj_t BgL_arg1802z00_8925;

														{	/* Cfa/cinfo.sch 968 */
															obj_t BgL_arg1803z00_8926;

															BgL_arg1803z00_8926 = (BgL_oclassz00_8918);
															BgL_arg1802z00_8925 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8926,
																5L);
														}
														BgL_res2179z00_8917 =
															(BgL_arg1802z00_8925 == BgL_classz00_8912);
													}
												else
													{	/* Cfa/cinfo.sch 968 */
														BgL_res2179z00_8917 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2179z00_8917;
							}
					}
				else
					{	/* Cfa/cinfo.sch 968 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &genpatchid/Cinfo? */
	obj_t BGl_z62genpatchidzf2Cinfozf3z63zzcfa_approxz00(obj_t BgL_envz00_7869,
		obj_t BgL_objz00_7870)
	{
		{	/* Cfa/cinfo.sch 968 */
			return
				BBOOL(BGl_genpatchidzf2Cinfozf3z01zzcfa_approxz00(BgL_objz00_7870));
		}

	}



/* genpatchid/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_genpatchidz00_bglt
		BGl_genpatchidzf2Cinfozd2nilz20zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 969 */
			{	/* Cfa/cinfo.sch 969 */
				obj_t BgL_classz00_5375;

				BgL_classz00_5375 = BGl_genpatchidzf2Cinfozf2zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 969 */
					obj_t BgL__ortest_1117z00_5376;

					BgL__ortest_1117z00_5376 = BGL_CLASS_NIL(BgL_classz00_5375);
					if (CBOOL(BgL__ortest_1117z00_5376))
						{	/* Cfa/cinfo.sch 969 */
							return ((BgL_genpatchidz00_bglt) BgL__ortest_1117z00_5376);
						}
					else
						{	/* Cfa/cinfo.sch 969 */
							return
								((BgL_genpatchidz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5375));
						}
				}
			}
		}

	}



/* &genpatchid/Cinfo-nil */
	BgL_genpatchidz00_bglt BGl_z62genpatchidzf2Cinfozd2nilz42zzcfa_approxz00(obj_t
		BgL_envz00_7871)
	{
		{	/* Cfa/cinfo.sch 969 */
			return BGl_genpatchidzf2Cinfozd2nilz20zzcfa_approxz00();
		}

	}



/* genpatchid/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_genpatchidzf2Cinfozd2approxz20zzcfa_approxz00(BgL_genpatchidz00_bglt
		BgL_oz00_537)
	{
		{	/* Cfa/cinfo.sch 970 */
			{
				BgL_genpatchidzf2cinfozf2_bglt BgL_auxz00_11555;

				{
					obj_t BgL_auxz00_11556;

					{	/* Cfa/cinfo.sch 970 */
						BgL_objectz00_bglt BgL_tmpz00_11557;

						BgL_tmpz00_11557 = ((BgL_objectz00_bglt) BgL_oz00_537);
						BgL_auxz00_11556 = BGL_OBJECT_WIDENING(BgL_tmpz00_11557);
					}
					BgL_auxz00_11555 =
						((BgL_genpatchidzf2cinfozf2_bglt) BgL_auxz00_11556);
				}
				return
					(((BgL_genpatchidzf2cinfozf2_bglt) COBJECT(BgL_auxz00_11555))->
					BgL_approxz00);
			}
		}

	}



/* &genpatchid/Cinfo-approx */
	BgL_approxz00_bglt BGl_z62genpatchidzf2Cinfozd2approxz42zzcfa_approxz00(obj_t
		BgL_envz00_7872, obj_t BgL_oz00_7873)
	{
		{	/* Cfa/cinfo.sch 970 */
			return
				BGl_genpatchidzf2Cinfozd2approxz20zzcfa_approxz00(
				((BgL_genpatchidz00_bglt) BgL_oz00_7873));
		}

	}



/* genpatchid/Cinfo-rindex */
	BGL_EXPORTED_DEF long
		BGl_genpatchidzf2Cinfozd2rindexz20zzcfa_approxz00(BgL_genpatchidz00_bglt
		BgL_oz00_540)
	{
		{	/* Cfa/cinfo.sch 972 */
			return
				(((BgL_genpatchidz00_bglt) COBJECT(
						((BgL_genpatchidz00_bglt) BgL_oz00_540)))->BgL_rindexz00);
		}

	}



/* &genpatchid/Cinfo-rindex */
	obj_t BGl_z62genpatchidzf2Cinfozd2rindexz42zzcfa_approxz00(obj_t
		BgL_envz00_7874, obj_t BgL_oz00_7875)
	{
		{	/* Cfa/cinfo.sch 972 */
			return
				BINT(BGl_genpatchidzf2Cinfozd2rindexz20zzcfa_approxz00(
					((BgL_genpatchidz00_bglt) BgL_oz00_7875)));
		}

	}



/* genpatchid/Cinfo-rindex-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_genpatchidzf2Cinfozd2rindexzd2setz12ze0zzcfa_approxz00
		(BgL_genpatchidz00_bglt BgL_oz00_541, long BgL_vz00_542)
	{
		{	/* Cfa/cinfo.sch 973 */
			return
				((((BgL_genpatchidz00_bglt) COBJECT(
							((BgL_genpatchidz00_bglt) BgL_oz00_541)))->BgL_rindexz00) =
				((long) BgL_vz00_542), BUNSPEC);
		}

	}



/* &genpatchid/Cinfo-rindex-set! */
	obj_t BGl_z62genpatchidzf2Cinfozd2rindexzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_7876, obj_t BgL_oz00_7877, obj_t BgL_vz00_7878)
	{
		{	/* Cfa/cinfo.sch 973 */
			return
				BGl_genpatchidzf2Cinfozd2rindexzd2setz12ze0zzcfa_approxz00(
				((BgL_genpatchidz00_bglt) BgL_oz00_7877), (long) CINT(BgL_vz00_7878));
		}

	}



/* genpatchid/Cinfo-index */
	BGL_EXPORTED_DEF long
		BGl_genpatchidzf2Cinfozd2indexz20zzcfa_approxz00(BgL_genpatchidz00_bglt
		BgL_oz00_543)
	{
		{	/* Cfa/cinfo.sch 974 */
			return
				(((BgL_genpatchidz00_bglt) COBJECT(
						((BgL_genpatchidz00_bglt) BgL_oz00_543)))->BgL_indexz00);
		}

	}



/* &genpatchid/Cinfo-index */
	obj_t BGl_z62genpatchidzf2Cinfozd2indexz42zzcfa_approxz00(obj_t
		BgL_envz00_7879, obj_t BgL_oz00_7880)
	{
		{	/* Cfa/cinfo.sch 974 */
			return
				BINT(BGl_genpatchidzf2Cinfozd2indexz20zzcfa_approxz00(
					((BgL_genpatchidz00_bglt) BgL_oz00_7880)));
		}

	}



/* genpatchid/Cinfo-index-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_genpatchidzf2Cinfozd2indexzd2setz12ze0zzcfa_approxz00
		(BgL_genpatchidz00_bglt BgL_oz00_544, long BgL_vz00_545)
	{
		{	/* Cfa/cinfo.sch 975 */
			return
				((((BgL_genpatchidz00_bglt) COBJECT(
							((BgL_genpatchidz00_bglt) BgL_oz00_544)))->BgL_indexz00) =
				((long) BgL_vz00_545), BUNSPEC);
		}

	}



/* &genpatchid/Cinfo-index-set! */
	obj_t BGl_z62genpatchidzf2Cinfozd2indexzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_7881, obj_t BgL_oz00_7882, obj_t BgL_vz00_7883)
	{
		{	/* Cfa/cinfo.sch 975 */
			return
				BGl_genpatchidzf2Cinfozd2indexzd2setz12ze0zzcfa_approxz00(
				((BgL_genpatchidz00_bglt) BgL_oz00_7882), (long) CINT(BgL_vz00_7883));
		}

	}



/* genpatchid/Cinfo-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_genpatchidzf2Cinfozd2effectz20zzcfa_approxz00(BgL_genpatchidz00_bglt
		BgL_oz00_546)
	{
		{	/* Cfa/cinfo.sch 976 */
			return
				(((BgL_externz00_bglt) COBJECT(
						((BgL_externz00_bglt) BgL_oz00_546)))->BgL_effectz00);
		}

	}



/* &genpatchid/Cinfo-effect */
	obj_t BGl_z62genpatchidzf2Cinfozd2effectz42zzcfa_approxz00(obj_t
		BgL_envz00_7884, obj_t BgL_oz00_7885)
	{
		{	/* Cfa/cinfo.sch 976 */
			return
				BGl_genpatchidzf2Cinfozd2effectz20zzcfa_approxz00(
				((BgL_genpatchidz00_bglt) BgL_oz00_7885));
		}

	}



/* genpatchid/Cinfo-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_genpatchidzf2Cinfozd2effectzd2setz12ze0zzcfa_approxz00
		(BgL_genpatchidz00_bglt BgL_oz00_547, obj_t BgL_vz00_548)
	{
		{	/* Cfa/cinfo.sch 977 */
			return
				((((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_oz00_547)))->BgL_effectz00) =
				((obj_t) BgL_vz00_548), BUNSPEC);
		}

	}



/* &genpatchid/Cinfo-effect-set! */
	obj_t BGl_z62genpatchidzf2Cinfozd2effectzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_7886, obj_t BgL_oz00_7887, obj_t BgL_vz00_7888)
	{
		{	/* Cfa/cinfo.sch 977 */
			return
				BGl_genpatchidzf2Cinfozd2effectzd2setz12ze0zzcfa_approxz00(
				((BgL_genpatchidz00_bglt) BgL_oz00_7887), BgL_vz00_7888);
		}

	}



/* genpatchid/Cinfo-expr* */
	BGL_EXPORTED_DEF obj_t
		BGl_genpatchidzf2Cinfozd2exprza2z82zzcfa_approxz00(BgL_genpatchidz00_bglt
		BgL_oz00_549)
	{
		{	/* Cfa/cinfo.sch 978 */
			return
				(((BgL_externz00_bglt) COBJECT(
						((BgL_externz00_bglt) BgL_oz00_549)))->BgL_exprza2za2);
		}

	}



/* &genpatchid/Cinfo-expr* */
	obj_t BGl_z62genpatchidzf2Cinfozd2exprza2ze0zzcfa_approxz00(obj_t
		BgL_envz00_7889, obj_t BgL_oz00_7890)
	{
		{	/* Cfa/cinfo.sch 978 */
			return
				BGl_genpatchidzf2Cinfozd2exprza2z82zzcfa_approxz00(
				((BgL_genpatchidz00_bglt) BgL_oz00_7890));
		}

	}



/* genpatchid/Cinfo-expr*-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_genpatchidzf2Cinfozd2exprza2zd2setz12z42zzcfa_approxz00
		(BgL_genpatchidz00_bglt BgL_oz00_550, obj_t BgL_vz00_551)
	{
		{	/* Cfa/cinfo.sch 979 */
			return
				((((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_oz00_550)))->BgL_exprza2za2) =
				((obj_t) BgL_vz00_551), BUNSPEC);
		}

	}



/* &genpatchid/Cinfo-expr*-set! */
	obj_t BGl_z62genpatchidzf2Cinfozd2exprza2zd2setz12z20zzcfa_approxz00(obj_t
		BgL_envz00_7891, obj_t BgL_oz00_7892, obj_t BgL_vz00_7893)
	{
		{	/* Cfa/cinfo.sch 979 */
			return
				BGl_genpatchidzf2Cinfozd2exprza2zd2setz12z42zzcfa_approxz00(
				((BgL_genpatchidz00_bglt) BgL_oz00_7892), BgL_vz00_7893);
		}

	}



/* genpatchid/Cinfo-key */
	BGL_EXPORTED_DEF obj_t
		BGl_genpatchidzf2Cinfozd2keyz20zzcfa_approxz00(BgL_genpatchidz00_bglt
		BgL_oz00_552)
	{
		{	/* Cfa/cinfo.sch 980 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_552)))->BgL_keyz00);
		}

	}



/* &genpatchid/Cinfo-key */
	obj_t BGl_z62genpatchidzf2Cinfozd2keyz42zzcfa_approxz00(obj_t BgL_envz00_7894,
		obj_t BgL_oz00_7895)
	{
		{	/* Cfa/cinfo.sch 980 */
			return
				BGl_genpatchidzf2Cinfozd2keyz20zzcfa_approxz00(
				((BgL_genpatchidz00_bglt) BgL_oz00_7895));
		}

	}



/* genpatchid/Cinfo-key-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_genpatchidzf2Cinfozd2keyzd2setz12ze0zzcfa_approxz00
		(BgL_genpatchidz00_bglt BgL_oz00_553, obj_t BgL_vz00_554)
	{
		{	/* Cfa/cinfo.sch 981 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_553)))->BgL_keyz00) =
				((obj_t) BgL_vz00_554), BUNSPEC);
		}

	}



/* &genpatchid/Cinfo-key-set! */
	obj_t BGl_z62genpatchidzf2Cinfozd2keyzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_7896, obj_t BgL_oz00_7897, obj_t BgL_vz00_7898)
	{
		{	/* Cfa/cinfo.sch 981 */
			return
				BGl_genpatchidzf2Cinfozd2keyzd2setz12ze0zzcfa_approxz00(
				((BgL_genpatchidz00_bglt) BgL_oz00_7897), BgL_vz00_7898);
		}

	}



/* genpatchid/Cinfo-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_genpatchidzf2Cinfozd2sidezd2effectzf2zzcfa_approxz00
		(BgL_genpatchidz00_bglt BgL_oz00_555)
	{
		{	/* Cfa/cinfo.sch 982 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_555)))->BgL_sidezd2effectzd2);
		}

	}



/* &genpatchid/Cinfo-side-effect */
	obj_t BGl_z62genpatchidzf2Cinfozd2sidezd2effectz90zzcfa_approxz00(obj_t
		BgL_envz00_7899, obj_t BgL_oz00_7900)
	{
		{	/* Cfa/cinfo.sch 982 */
			return
				BGl_genpatchidzf2Cinfozd2sidezd2effectzf2zzcfa_approxz00(
				((BgL_genpatchidz00_bglt) BgL_oz00_7900));
		}

	}



/* genpatchid/Cinfo-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_genpatchidzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_approxz00
		(BgL_genpatchidz00_bglt BgL_oz00_556, obj_t BgL_vz00_557)
	{
		{	/* Cfa/cinfo.sch 983 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_556)))->
					BgL_sidezd2effectzd2) = ((obj_t) BgL_vz00_557), BUNSPEC);
		}

	}



/* &genpatchid/Cinfo-side-effect-set! */
	obj_t
		BGl_z62genpatchidzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_7901, obj_t BgL_oz00_7902, obj_t BgL_vz00_7903)
	{
		{	/* Cfa/cinfo.sch 983 */
			return
				BGl_genpatchidzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_approxz00(
				((BgL_genpatchidz00_bglt) BgL_oz00_7902), BgL_vz00_7903);
		}

	}



/* genpatchid/Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_genpatchidzf2Cinfozd2typez20zzcfa_approxz00(BgL_genpatchidz00_bglt
		BgL_oz00_558)
	{
		{	/* Cfa/cinfo.sch 984 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_558)))->BgL_typez00);
		}

	}



/* &genpatchid/Cinfo-type */
	BgL_typez00_bglt BGl_z62genpatchidzf2Cinfozd2typez42zzcfa_approxz00(obj_t
		BgL_envz00_7904, obj_t BgL_oz00_7905)
	{
		{	/* Cfa/cinfo.sch 984 */
			return
				BGl_genpatchidzf2Cinfozd2typez20zzcfa_approxz00(
				((BgL_genpatchidz00_bglt) BgL_oz00_7905));
		}

	}



/* genpatchid/Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_genpatchidzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00
		(BgL_genpatchidz00_bglt BgL_oz00_559, BgL_typez00_bglt BgL_vz00_560)
	{
		{	/* Cfa/cinfo.sch 985 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_559)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_560), BUNSPEC);
		}

	}



/* &genpatchid/Cinfo-type-set! */
	obj_t BGl_z62genpatchidzf2Cinfozd2typezd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_7906, obj_t BgL_oz00_7907, obj_t BgL_vz00_7908)
	{
		{	/* Cfa/cinfo.sch 985 */
			return
				BGl_genpatchidzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00(
				((BgL_genpatchidz00_bglt) BgL_oz00_7907),
				((BgL_typez00_bglt) BgL_vz00_7908));
		}

	}



/* genpatchid/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_genpatchidzf2Cinfozd2locz20zzcfa_approxz00(BgL_genpatchidz00_bglt
		BgL_oz00_561)
	{
		{	/* Cfa/cinfo.sch 986 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_561)))->BgL_locz00);
		}

	}



/* &genpatchid/Cinfo-loc */
	obj_t BGl_z62genpatchidzf2Cinfozd2locz42zzcfa_approxz00(obj_t BgL_envz00_7909,
		obj_t BgL_oz00_7910)
	{
		{	/* Cfa/cinfo.sch 986 */
			return
				BGl_genpatchidzf2Cinfozd2locz20zzcfa_approxz00(
				((BgL_genpatchidz00_bglt) BgL_oz00_7910));
		}

	}



/* make-patch/Cinfo */
	BGL_EXPORTED_DEF BgL_patchz00_bglt
		BGl_makezd2patchzf2Cinfoz20zzcfa_approxz00(obj_t BgL_loc1675z00_564,
		BgL_typez00_bglt BgL_type1676z00_565, obj_t BgL_value1677z00_566,
		BgL_varz00_bglt BgL_ref1678z00_567, long BgL_index1679z00_568,
		obj_t BgL_patchid1680z00_569, BgL_approxz00_bglt BgL_approx1681z00_570)
	{
		{	/* Cfa/cinfo.sch 990 */
			{	/* Cfa/cinfo.sch 990 */
				BgL_patchz00_bglt BgL_new1229z00_8927;

				{	/* Cfa/cinfo.sch 990 */
					BgL_patchz00_bglt BgL_tmp1227z00_8928;
					BgL_patchzf2cinfozf2_bglt BgL_wide1228z00_8929;

					{
						BgL_patchz00_bglt BgL_auxz00_11629;

						{	/* Cfa/cinfo.sch 990 */
							BgL_patchz00_bglt BgL_new1226z00_8930;

							BgL_new1226z00_8930 =
								((BgL_patchz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_patchz00_bgl))));
							{	/* Cfa/cinfo.sch 990 */
								long BgL_arg1710z00_8931;

								BgL_arg1710z00_8931 = BGL_CLASS_NUM(BGl_patchz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1226z00_8930),
									BgL_arg1710z00_8931);
							}
							{	/* Cfa/cinfo.sch 990 */
								BgL_objectz00_bglt BgL_tmpz00_11634;

								BgL_tmpz00_11634 = ((BgL_objectz00_bglt) BgL_new1226z00_8930);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11634, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1226z00_8930);
							BgL_auxz00_11629 = BgL_new1226z00_8930;
						}
						BgL_tmp1227z00_8928 = ((BgL_patchz00_bglt) BgL_auxz00_11629);
					}
					BgL_wide1228z00_8929 =
						((BgL_patchzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_patchzf2cinfozf2_bgl))));
					{	/* Cfa/cinfo.sch 990 */
						obj_t BgL_auxz00_11642;
						BgL_objectz00_bglt BgL_tmpz00_11640;

						BgL_auxz00_11642 = ((obj_t) BgL_wide1228z00_8929);
						BgL_tmpz00_11640 = ((BgL_objectz00_bglt) BgL_tmp1227z00_8928);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11640, BgL_auxz00_11642);
					}
					((BgL_objectz00_bglt) BgL_tmp1227z00_8928);
					{	/* Cfa/cinfo.sch 990 */
						long BgL_arg1709z00_8932;

						BgL_arg1709z00_8932 =
							BGL_CLASS_NUM(BGl_patchzf2Cinfozf2zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1227z00_8928), BgL_arg1709z00_8932);
					}
					BgL_new1229z00_8927 = ((BgL_patchz00_bglt) BgL_tmp1227z00_8928);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1229z00_8927)))->BgL_locz00) =
					((obj_t) BgL_loc1675z00_564), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1229z00_8927)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1676z00_565), BUNSPEC);
				((((BgL_atomz00_bglt) COBJECT(((BgL_atomz00_bglt)
									BgL_new1229z00_8927)))->BgL_valuez00) =
					((obj_t) BgL_value1677z00_566), BUNSPEC);
				((((BgL_patchz00_bglt) COBJECT(((BgL_patchz00_bglt)
									BgL_new1229z00_8927)))->BgL_refz00) =
					((BgL_varz00_bglt) BgL_ref1678z00_567), BUNSPEC);
				((((BgL_patchz00_bglt) COBJECT(((BgL_patchz00_bglt)
									BgL_new1229z00_8927)))->BgL_indexz00) =
					((long) BgL_index1679z00_568), BUNSPEC);
				((((BgL_patchz00_bglt) COBJECT(((BgL_patchz00_bglt)
									BgL_new1229z00_8927)))->BgL_patchidz00) =
					((obj_t) BgL_patchid1680z00_569), BUNSPEC);
				{
					BgL_patchzf2cinfozf2_bglt BgL_auxz00_11662;

					{
						obj_t BgL_auxz00_11663;

						{	/* Cfa/cinfo.sch 990 */
							BgL_objectz00_bglt BgL_tmpz00_11664;

							BgL_tmpz00_11664 = ((BgL_objectz00_bglt) BgL_new1229z00_8927);
							BgL_auxz00_11663 = BGL_OBJECT_WIDENING(BgL_tmpz00_11664);
						}
						BgL_auxz00_11662 = ((BgL_patchzf2cinfozf2_bglt) BgL_auxz00_11663);
					}
					((((BgL_patchzf2cinfozf2_bglt) COBJECT(BgL_auxz00_11662))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1681z00_570), BUNSPEC);
				}
				return BgL_new1229z00_8927;
			}
		}

	}



/* &make-patch/Cinfo */
	BgL_patchz00_bglt BGl_z62makezd2patchzf2Cinfoz42zzcfa_approxz00(obj_t
		BgL_envz00_7911, obj_t BgL_loc1675z00_7912, obj_t BgL_type1676z00_7913,
		obj_t BgL_value1677z00_7914, obj_t BgL_ref1678z00_7915,
		obj_t BgL_index1679z00_7916, obj_t BgL_patchid1680z00_7917,
		obj_t BgL_approx1681z00_7918)
	{
		{	/* Cfa/cinfo.sch 990 */
			return
				BGl_makezd2patchzf2Cinfoz20zzcfa_approxz00(BgL_loc1675z00_7912,
				((BgL_typez00_bglt) BgL_type1676z00_7913), BgL_value1677z00_7914,
				((BgL_varz00_bglt) BgL_ref1678z00_7915),
				(long) CINT(BgL_index1679z00_7916), BgL_patchid1680z00_7917,
				((BgL_approxz00_bglt) BgL_approx1681z00_7918));
		}

	}



/* patch/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_patchzf2Cinfozf3z01zzcfa_approxz00(obj_t
		BgL_objz00_571)
	{
		{	/* Cfa/cinfo.sch 991 */
			{	/* Cfa/cinfo.sch 991 */
				obj_t BgL_classz00_8933;

				BgL_classz00_8933 = BGl_patchzf2Cinfozf2zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_571))
					{	/* Cfa/cinfo.sch 991 */
						BgL_objectz00_bglt BgL_arg1807z00_8934;

						BgL_arg1807z00_8934 = (BgL_objectz00_bglt) (BgL_objz00_571);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 991 */
								long BgL_idxz00_8935;

								BgL_idxz00_8935 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8934);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8935 + 4L)) == BgL_classz00_8933);
							}
						else
							{	/* Cfa/cinfo.sch 991 */
								bool_t BgL_res2180z00_8938;

								{	/* Cfa/cinfo.sch 991 */
									obj_t BgL_oclassz00_8939;

									{	/* Cfa/cinfo.sch 991 */
										obj_t BgL_arg1815z00_8940;
										long BgL_arg1816z00_8941;

										BgL_arg1815z00_8940 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 991 */
											long BgL_arg1817z00_8942;

											BgL_arg1817z00_8942 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8934);
											BgL_arg1816z00_8941 = (BgL_arg1817z00_8942 - OBJECT_TYPE);
										}
										BgL_oclassz00_8939 =
											VECTOR_REF(BgL_arg1815z00_8940, BgL_arg1816z00_8941);
									}
									{	/* Cfa/cinfo.sch 991 */
										bool_t BgL__ortest_1115z00_8943;

										BgL__ortest_1115z00_8943 =
											(BgL_classz00_8933 == BgL_oclassz00_8939);
										if (BgL__ortest_1115z00_8943)
											{	/* Cfa/cinfo.sch 991 */
												BgL_res2180z00_8938 = BgL__ortest_1115z00_8943;
											}
										else
											{	/* Cfa/cinfo.sch 991 */
												long BgL_odepthz00_8944;

												{	/* Cfa/cinfo.sch 991 */
													obj_t BgL_arg1804z00_8945;

													BgL_arg1804z00_8945 = (BgL_oclassz00_8939);
													BgL_odepthz00_8944 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8945);
												}
												if ((4L < BgL_odepthz00_8944))
													{	/* Cfa/cinfo.sch 991 */
														obj_t BgL_arg1802z00_8946;

														{	/* Cfa/cinfo.sch 991 */
															obj_t BgL_arg1803z00_8947;

															BgL_arg1803z00_8947 = (BgL_oclassz00_8939);
															BgL_arg1802z00_8946 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8947,
																4L);
														}
														BgL_res2180z00_8938 =
															(BgL_arg1802z00_8946 == BgL_classz00_8933);
													}
												else
													{	/* Cfa/cinfo.sch 991 */
														BgL_res2180z00_8938 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2180z00_8938;
							}
					}
				else
					{	/* Cfa/cinfo.sch 991 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &patch/Cinfo? */
	obj_t BGl_z62patchzf2Cinfozf3z63zzcfa_approxz00(obj_t BgL_envz00_7919,
		obj_t BgL_objz00_7920)
	{
		{	/* Cfa/cinfo.sch 991 */
			return BBOOL(BGl_patchzf2Cinfozf3z01zzcfa_approxz00(BgL_objz00_7920));
		}

	}



/* patch/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_patchz00_bglt
		BGl_patchzf2Cinfozd2nilz20zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 992 */
			{	/* Cfa/cinfo.sch 992 */
				obj_t BgL_classz00_5427;

				BgL_classz00_5427 = BGl_patchzf2Cinfozf2zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 992 */
					obj_t BgL__ortest_1117z00_5428;

					BgL__ortest_1117z00_5428 = BGL_CLASS_NIL(BgL_classz00_5427);
					if (CBOOL(BgL__ortest_1117z00_5428))
						{	/* Cfa/cinfo.sch 992 */
							return ((BgL_patchz00_bglt) BgL__ortest_1117z00_5428);
						}
					else
						{	/* Cfa/cinfo.sch 992 */
							return
								((BgL_patchz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5427));
						}
				}
			}
		}

	}



/* &patch/Cinfo-nil */
	BgL_patchz00_bglt BGl_z62patchzf2Cinfozd2nilz42zzcfa_approxz00(obj_t
		BgL_envz00_7921)
	{
		{	/* Cfa/cinfo.sch 992 */
			return BGl_patchzf2Cinfozd2nilz20zzcfa_approxz00();
		}

	}



/* patch/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_patchzf2Cinfozd2approxz20zzcfa_approxz00(BgL_patchz00_bglt BgL_oz00_572)
	{
		{	/* Cfa/cinfo.sch 993 */
			{
				BgL_patchzf2cinfozf2_bglt BgL_auxz00_11705;

				{
					obj_t BgL_auxz00_11706;

					{	/* Cfa/cinfo.sch 993 */
						BgL_objectz00_bglt BgL_tmpz00_11707;

						BgL_tmpz00_11707 = ((BgL_objectz00_bglt) BgL_oz00_572);
						BgL_auxz00_11706 = BGL_OBJECT_WIDENING(BgL_tmpz00_11707);
					}
					BgL_auxz00_11705 = ((BgL_patchzf2cinfozf2_bglt) BgL_auxz00_11706);
				}
				return
					(((BgL_patchzf2cinfozf2_bglt) COBJECT(BgL_auxz00_11705))->
					BgL_approxz00);
			}
		}

	}



/* &patch/Cinfo-approx */
	BgL_approxz00_bglt BGl_z62patchzf2Cinfozd2approxz42zzcfa_approxz00(obj_t
		BgL_envz00_7922, obj_t BgL_oz00_7923)
	{
		{	/* Cfa/cinfo.sch 993 */
			return
				BGl_patchzf2Cinfozd2approxz20zzcfa_approxz00(
				((BgL_patchz00_bglt) BgL_oz00_7923));
		}

	}



/* patch/Cinfo-patchid */
	BGL_EXPORTED_DEF obj_t
		BGl_patchzf2Cinfozd2patchidz20zzcfa_approxz00(BgL_patchz00_bglt
		BgL_oz00_575)
	{
		{	/* Cfa/cinfo.sch 995 */
			return
				(((BgL_patchz00_bglt) COBJECT(
						((BgL_patchz00_bglt) BgL_oz00_575)))->BgL_patchidz00);
		}

	}



/* &patch/Cinfo-patchid */
	obj_t BGl_z62patchzf2Cinfozd2patchidz42zzcfa_approxz00(obj_t BgL_envz00_7924,
		obj_t BgL_oz00_7925)
	{
		{	/* Cfa/cinfo.sch 995 */
			return
				BGl_patchzf2Cinfozd2patchidz20zzcfa_approxz00(
				((BgL_patchz00_bglt) BgL_oz00_7925));
		}

	}



/* patch/Cinfo-patchid-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_patchzf2Cinfozd2patchidzd2setz12ze0zzcfa_approxz00(BgL_patchz00_bglt
		BgL_oz00_576, obj_t BgL_vz00_577)
	{
		{	/* Cfa/cinfo.sch 996 */
			return
				((((BgL_patchz00_bglt) COBJECT(
							((BgL_patchz00_bglt) BgL_oz00_576)))->BgL_patchidz00) =
				((obj_t) BgL_vz00_577), BUNSPEC);
		}

	}



/* &patch/Cinfo-patchid-set! */
	obj_t BGl_z62patchzf2Cinfozd2patchidzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_7926, obj_t BgL_oz00_7927, obj_t BgL_vz00_7928)
	{
		{	/* Cfa/cinfo.sch 996 */
			return
				BGl_patchzf2Cinfozd2patchidzd2setz12ze0zzcfa_approxz00(
				((BgL_patchz00_bglt) BgL_oz00_7927), BgL_vz00_7928);
		}

	}



/* patch/Cinfo-index */
	BGL_EXPORTED_DEF long
		BGl_patchzf2Cinfozd2indexz20zzcfa_approxz00(BgL_patchz00_bglt BgL_oz00_578)
	{
		{	/* Cfa/cinfo.sch 997 */
			return
				(((BgL_patchz00_bglt) COBJECT(
						((BgL_patchz00_bglt) BgL_oz00_578)))->BgL_indexz00);
		}

	}



/* &patch/Cinfo-index */
	obj_t BGl_z62patchzf2Cinfozd2indexz42zzcfa_approxz00(obj_t BgL_envz00_7929,
		obj_t BgL_oz00_7930)
	{
		{	/* Cfa/cinfo.sch 997 */
			return
				BINT(BGl_patchzf2Cinfozd2indexz20zzcfa_approxz00(
					((BgL_patchz00_bglt) BgL_oz00_7930)));
		}

	}



/* patch/Cinfo-index-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_patchzf2Cinfozd2indexzd2setz12ze0zzcfa_approxz00(BgL_patchz00_bglt
		BgL_oz00_579, long BgL_vz00_580)
	{
		{	/* Cfa/cinfo.sch 998 */
			return
				((((BgL_patchz00_bglt) COBJECT(
							((BgL_patchz00_bglt) BgL_oz00_579)))->BgL_indexz00) =
				((long) BgL_vz00_580), BUNSPEC);
		}

	}



/* &patch/Cinfo-index-set! */
	obj_t BGl_z62patchzf2Cinfozd2indexzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_7931, obj_t BgL_oz00_7932, obj_t BgL_vz00_7933)
	{
		{	/* Cfa/cinfo.sch 998 */
			return
				BGl_patchzf2Cinfozd2indexzd2setz12ze0zzcfa_approxz00(
				((BgL_patchz00_bglt) BgL_oz00_7932), (long) CINT(BgL_vz00_7933));
		}

	}



/* patch/Cinfo-ref */
	BGL_EXPORTED_DEF BgL_varz00_bglt
		BGl_patchzf2Cinfozd2refz20zzcfa_approxz00(BgL_patchz00_bglt BgL_oz00_581)
	{
		{	/* Cfa/cinfo.sch 999 */
			return
				(((BgL_patchz00_bglt) COBJECT(
						((BgL_patchz00_bglt) BgL_oz00_581)))->BgL_refz00);
		}

	}



/* &patch/Cinfo-ref */
	BgL_varz00_bglt BGl_z62patchzf2Cinfozd2refz42zzcfa_approxz00(obj_t
		BgL_envz00_7934, obj_t BgL_oz00_7935)
	{
		{	/* Cfa/cinfo.sch 999 */
			return
				BGl_patchzf2Cinfozd2refz20zzcfa_approxz00(
				((BgL_patchz00_bglt) BgL_oz00_7935));
		}

	}



/* patch/Cinfo-value */
	BGL_EXPORTED_DEF obj_t
		BGl_patchzf2Cinfozd2valuez20zzcfa_approxz00(BgL_patchz00_bglt BgL_oz00_584)
	{
		{	/* Cfa/cinfo.sch 1001 */
			return
				(((BgL_atomz00_bglt) COBJECT(
						((BgL_atomz00_bglt) BgL_oz00_584)))->BgL_valuez00);
		}

	}



/* &patch/Cinfo-value */
	obj_t BGl_z62patchzf2Cinfozd2valuez42zzcfa_approxz00(obj_t BgL_envz00_7936,
		obj_t BgL_oz00_7937)
	{
		{	/* Cfa/cinfo.sch 1001 */
			return
				BGl_patchzf2Cinfozd2valuez20zzcfa_approxz00(
				((BgL_patchz00_bglt) BgL_oz00_7937));
		}

	}



/* patch/Cinfo-value-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_patchzf2Cinfozd2valuezd2setz12ze0zzcfa_approxz00(BgL_patchz00_bglt
		BgL_oz00_585, obj_t BgL_vz00_586)
	{
		{	/* Cfa/cinfo.sch 1002 */
			return
				((((BgL_atomz00_bglt) COBJECT(
							((BgL_atomz00_bglt) BgL_oz00_585)))->BgL_valuez00) =
				((obj_t) BgL_vz00_586), BUNSPEC);
		}

	}



/* &patch/Cinfo-value-set! */
	obj_t BGl_z62patchzf2Cinfozd2valuezd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_7938, obj_t BgL_oz00_7939, obj_t BgL_vz00_7940)
	{
		{	/* Cfa/cinfo.sch 1002 */
			return
				BGl_patchzf2Cinfozd2valuezd2setz12ze0zzcfa_approxz00(
				((BgL_patchz00_bglt) BgL_oz00_7939), BgL_vz00_7940);
		}

	}



/* patch/Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_patchzf2Cinfozd2typez20zzcfa_approxz00(BgL_patchz00_bglt BgL_oz00_587)
	{
		{	/* Cfa/cinfo.sch 1003 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_587)))->BgL_typez00);
		}

	}



/* &patch/Cinfo-type */
	BgL_typez00_bglt BGl_z62patchzf2Cinfozd2typez42zzcfa_approxz00(obj_t
		BgL_envz00_7941, obj_t BgL_oz00_7942)
	{
		{	/* Cfa/cinfo.sch 1003 */
			return
				BGl_patchzf2Cinfozd2typez20zzcfa_approxz00(
				((BgL_patchz00_bglt) BgL_oz00_7942));
		}

	}



/* patch/Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_patchzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00(BgL_patchz00_bglt
		BgL_oz00_588, BgL_typez00_bglt BgL_vz00_589)
	{
		{	/* Cfa/cinfo.sch 1004 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_588)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_589), BUNSPEC);
		}

	}



/* &patch/Cinfo-type-set! */
	obj_t BGl_z62patchzf2Cinfozd2typezd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_7943, obj_t BgL_oz00_7944, obj_t BgL_vz00_7945)
	{
		{	/* Cfa/cinfo.sch 1004 */
			return
				BGl_patchzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00(
				((BgL_patchz00_bglt) BgL_oz00_7944),
				((BgL_typez00_bglt) BgL_vz00_7945));
		}

	}



/* patch/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_patchzf2Cinfozd2locz20zzcfa_approxz00(BgL_patchz00_bglt BgL_oz00_590)
	{
		{	/* Cfa/cinfo.sch 1005 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_590)))->BgL_locz00);
		}

	}



/* &patch/Cinfo-loc */
	obj_t BGl_z62patchzf2Cinfozd2locz42zzcfa_approxz00(obj_t BgL_envz00_7946,
		obj_t BgL_oz00_7947)
	{
		{	/* Cfa/cinfo.sch 1005 */
			return
				BGl_patchzf2Cinfozd2locz20zzcfa_approxz00(
				((BgL_patchz00_bglt) BgL_oz00_7947));
		}

	}



/* make-kwote/node */
	BGL_EXPORTED_DEF BgL_kwotez00_bglt
		BGl_makezd2kwotezf2nodez20zzcfa_approxz00(obj_t BgL_loc1670z00_593,
		BgL_typez00_bglt BgL_type1671z00_594, obj_t BgL_value1672z00_595,
		BgL_nodez00_bglt BgL_node1673z00_596)
	{
		{	/* Cfa/cinfo.sch 1009 */
			{	/* Cfa/cinfo.sch 1009 */
				BgL_kwotez00_bglt BgL_new1233z00_8948;

				{	/* Cfa/cinfo.sch 1009 */
					BgL_kwotez00_bglt BgL_tmp1231z00_8949;
					BgL_kwotezf2nodezf2_bglt BgL_wide1232z00_8950;

					{
						BgL_kwotez00_bglt BgL_auxz00_11757;

						{	/* Cfa/cinfo.sch 1009 */
							BgL_kwotez00_bglt BgL_new1230z00_8951;

							BgL_new1230z00_8951 =
								((BgL_kwotez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_kwotez00_bgl))));
							{	/* Cfa/cinfo.sch 1009 */
								long BgL_arg1714z00_8952;

								BgL_arg1714z00_8952 = BGL_CLASS_NUM(BGl_kwotez00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1230z00_8951),
									BgL_arg1714z00_8952);
							}
							{	/* Cfa/cinfo.sch 1009 */
								BgL_objectz00_bglt BgL_tmpz00_11762;

								BgL_tmpz00_11762 = ((BgL_objectz00_bglt) BgL_new1230z00_8951);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11762, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1230z00_8951);
							BgL_auxz00_11757 = BgL_new1230z00_8951;
						}
						BgL_tmp1231z00_8949 = ((BgL_kwotez00_bglt) BgL_auxz00_11757);
					}
					BgL_wide1232z00_8950 =
						((BgL_kwotezf2nodezf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_kwotezf2nodezf2_bgl))));
					{	/* Cfa/cinfo.sch 1009 */
						obj_t BgL_auxz00_11770;
						BgL_objectz00_bglt BgL_tmpz00_11768;

						BgL_auxz00_11770 = ((obj_t) BgL_wide1232z00_8950);
						BgL_tmpz00_11768 = ((BgL_objectz00_bglt) BgL_tmp1231z00_8949);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11768, BgL_auxz00_11770);
					}
					((BgL_objectz00_bglt) BgL_tmp1231z00_8949);
					{	/* Cfa/cinfo.sch 1009 */
						long BgL_arg1711z00_8953;

						BgL_arg1711z00_8953 =
							BGL_CLASS_NUM(BGl_kwotezf2nodezf2zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1231z00_8949), BgL_arg1711z00_8953);
					}
					BgL_new1233z00_8948 = ((BgL_kwotez00_bglt) BgL_tmp1231z00_8949);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1233z00_8948)))->BgL_locz00) =
					((obj_t) BgL_loc1670z00_593), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1233z00_8948)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1671z00_594), BUNSPEC);
				((((BgL_kwotez00_bglt) COBJECT(((BgL_kwotez00_bglt)
									BgL_new1233z00_8948)))->BgL_valuez00) =
					((obj_t) BgL_value1672z00_595), BUNSPEC);
				{
					BgL_kwotezf2nodezf2_bglt BgL_auxz00_11784;

					{
						obj_t BgL_auxz00_11785;

						{	/* Cfa/cinfo.sch 1009 */
							BgL_objectz00_bglt BgL_tmpz00_11786;

							BgL_tmpz00_11786 = ((BgL_objectz00_bglt) BgL_new1233z00_8948);
							BgL_auxz00_11785 = BGL_OBJECT_WIDENING(BgL_tmpz00_11786);
						}
						BgL_auxz00_11784 = ((BgL_kwotezf2nodezf2_bglt) BgL_auxz00_11785);
					}
					((((BgL_kwotezf2nodezf2_bglt) COBJECT(BgL_auxz00_11784))->
							BgL_nodez00) = ((BgL_nodez00_bglt) BgL_node1673z00_596), BUNSPEC);
				}
				return BgL_new1233z00_8948;
			}
		}

	}



/* &make-kwote/node */
	BgL_kwotez00_bglt BGl_z62makezd2kwotezf2nodez42zzcfa_approxz00(obj_t
		BgL_envz00_7948, obj_t BgL_loc1670z00_7949, obj_t BgL_type1671z00_7950,
		obj_t BgL_value1672z00_7951, obj_t BgL_node1673z00_7952)
	{
		{	/* Cfa/cinfo.sch 1009 */
			return
				BGl_makezd2kwotezf2nodez20zzcfa_approxz00(BgL_loc1670z00_7949,
				((BgL_typez00_bglt) BgL_type1671z00_7950), BgL_value1672z00_7951,
				((BgL_nodez00_bglt) BgL_node1673z00_7952));
		}

	}



/* kwote/node? */
	BGL_EXPORTED_DEF bool_t BGl_kwotezf2nodezf3z01zzcfa_approxz00(obj_t
		BgL_objz00_597)
	{
		{	/* Cfa/cinfo.sch 1010 */
			{	/* Cfa/cinfo.sch 1010 */
				obj_t BgL_classz00_8954;

				BgL_classz00_8954 = BGl_kwotezf2nodezf2zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_597))
					{	/* Cfa/cinfo.sch 1010 */
						BgL_objectz00_bglt BgL_arg1807z00_8955;

						BgL_arg1807z00_8955 = (BgL_objectz00_bglt) (BgL_objz00_597);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 1010 */
								long BgL_idxz00_8956;

								BgL_idxz00_8956 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8955);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8956 + 3L)) == BgL_classz00_8954);
							}
						else
							{	/* Cfa/cinfo.sch 1010 */
								bool_t BgL_res2181z00_8959;

								{	/* Cfa/cinfo.sch 1010 */
									obj_t BgL_oclassz00_8960;

									{	/* Cfa/cinfo.sch 1010 */
										obj_t BgL_arg1815z00_8961;
										long BgL_arg1816z00_8962;

										BgL_arg1815z00_8961 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 1010 */
											long BgL_arg1817z00_8963;

											BgL_arg1817z00_8963 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8955);
											BgL_arg1816z00_8962 = (BgL_arg1817z00_8963 - OBJECT_TYPE);
										}
										BgL_oclassz00_8960 =
											VECTOR_REF(BgL_arg1815z00_8961, BgL_arg1816z00_8962);
									}
									{	/* Cfa/cinfo.sch 1010 */
										bool_t BgL__ortest_1115z00_8964;

										BgL__ortest_1115z00_8964 =
											(BgL_classz00_8954 == BgL_oclassz00_8960);
										if (BgL__ortest_1115z00_8964)
											{	/* Cfa/cinfo.sch 1010 */
												BgL_res2181z00_8959 = BgL__ortest_1115z00_8964;
											}
										else
											{	/* Cfa/cinfo.sch 1010 */
												long BgL_odepthz00_8965;

												{	/* Cfa/cinfo.sch 1010 */
													obj_t BgL_arg1804z00_8966;

													BgL_arg1804z00_8966 = (BgL_oclassz00_8960);
													BgL_odepthz00_8965 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8966);
												}
												if ((3L < BgL_odepthz00_8965))
													{	/* Cfa/cinfo.sch 1010 */
														obj_t BgL_arg1802z00_8967;

														{	/* Cfa/cinfo.sch 1010 */
															obj_t BgL_arg1803z00_8968;

															BgL_arg1803z00_8968 = (BgL_oclassz00_8960);
															BgL_arg1802z00_8967 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8968,
																3L);
														}
														BgL_res2181z00_8959 =
															(BgL_arg1802z00_8967 == BgL_classz00_8954);
													}
												else
													{	/* Cfa/cinfo.sch 1010 */
														BgL_res2181z00_8959 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2181z00_8959;
							}
					}
				else
					{	/* Cfa/cinfo.sch 1010 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &kwote/node? */
	obj_t BGl_z62kwotezf2nodezf3z63zzcfa_approxz00(obj_t BgL_envz00_7953,
		obj_t BgL_objz00_7954)
	{
		{	/* Cfa/cinfo.sch 1010 */
			return BBOOL(BGl_kwotezf2nodezf3z01zzcfa_approxz00(BgL_objz00_7954));
		}

	}



/* kwote/node-nil */
	BGL_EXPORTED_DEF BgL_kwotez00_bglt
		BGl_kwotezf2nodezd2nilz20zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 1011 */
			{	/* Cfa/cinfo.sch 1011 */
				obj_t BgL_classz00_5479;

				BgL_classz00_5479 = BGl_kwotezf2nodezf2zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 1011 */
					obj_t BgL__ortest_1117z00_5480;

					BgL__ortest_1117z00_5480 = BGL_CLASS_NIL(BgL_classz00_5479);
					if (CBOOL(BgL__ortest_1117z00_5480))
						{	/* Cfa/cinfo.sch 1011 */
							return ((BgL_kwotez00_bglt) BgL__ortest_1117z00_5480);
						}
					else
						{	/* Cfa/cinfo.sch 1011 */
							return
								((BgL_kwotez00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5479));
						}
				}
			}
		}

	}



/* &kwote/node-nil */
	BgL_kwotez00_bglt BGl_z62kwotezf2nodezd2nilz42zzcfa_approxz00(obj_t
		BgL_envz00_7955)
	{
		{	/* Cfa/cinfo.sch 1011 */
			return BGl_kwotezf2nodezd2nilz20zzcfa_approxz00();
		}

	}



/* kwote/node-node */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_kwotezf2nodezd2nodez20zzcfa_approxz00(BgL_kwotez00_bglt BgL_oz00_598)
	{
		{	/* Cfa/cinfo.sch 1012 */
			{
				BgL_kwotezf2nodezf2_bglt BgL_auxz00_11825;

				{
					obj_t BgL_auxz00_11826;

					{	/* Cfa/cinfo.sch 1012 */
						BgL_objectz00_bglt BgL_tmpz00_11827;

						BgL_tmpz00_11827 = ((BgL_objectz00_bglt) BgL_oz00_598);
						BgL_auxz00_11826 = BGL_OBJECT_WIDENING(BgL_tmpz00_11827);
					}
					BgL_auxz00_11825 = ((BgL_kwotezf2nodezf2_bglt) BgL_auxz00_11826);
				}
				return
					(((BgL_kwotezf2nodezf2_bglt) COBJECT(BgL_auxz00_11825))->BgL_nodez00);
			}
		}

	}



/* &kwote/node-node */
	BgL_nodez00_bglt BGl_z62kwotezf2nodezd2nodez42zzcfa_approxz00(obj_t
		BgL_envz00_7956, obj_t BgL_oz00_7957)
	{
		{	/* Cfa/cinfo.sch 1012 */
			return
				BGl_kwotezf2nodezd2nodez20zzcfa_approxz00(
				((BgL_kwotez00_bglt) BgL_oz00_7957));
		}

	}



/* kwote/node-value */
	BGL_EXPORTED_DEF obj_t
		BGl_kwotezf2nodezd2valuez20zzcfa_approxz00(BgL_kwotez00_bglt BgL_oz00_601)
	{
		{	/* Cfa/cinfo.sch 1014 */
			return
				(((BgL_kwotez00_bglt) COBJECT(
						((BgL_kwotez00_bglt) BgL_oz00_601)))->BgL_valuez00);
		}

	}



/* &kwote/node-value */
	obj_t BGl_z62kwotezf2nodezd2valuez42zzcfa_approxz00(obj_t BgL_envz00_7958,
		obj_t BgL_oz00_7959)
	{
		{	/* Cfa/cinfo.sch 1014 */
			return
				BGl_kwotezf2nodezd2valuez20zzcfa_approxz00(
				((BgL_kwotez00_bglt) BgL_oz00_7959));
		}

	}



/* kwote/node-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_kwotezf2nodezd2typez20zzcfa_approxz00(BgL_kwotez00_bglt BgL_oz00_604)
	{
		{	/* Cfa/cinfo.sch 1016 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_604)))->BgL_typez00);
		}

	}



/* &kwote/node-type */
	BgL_typez00_bglt BGl_z62kwotezf2nodezd2typez42zzcfa_approxz00(obj_t
		BgL_envz00_7960, obj_t BgL_oz00_7961)
	{
		{	/* Cfa/cinfo.sch 1016 */
			return
				BGl_kwotezf2nodezd2typez20zzcfa_approxz00(
				((BgL_kwotez00_bglt) BgL_oz00_7961));
		}

	}



/* kwote/node-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_kwotezf2nodezd2typezd2setz12ze0zzcfa_approxz00(BgL_kwotez00_bglt
		BgL_oz00_605, BgL_typez00_bglt BgL_vz00_606)
	{
		{	/* Cfa/cinfo.sch 1017 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_605)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_606), BUNSPEC);
		}

	}



/* &kwote/node-type-set! */
	obj_t BGl_z62kwotezf2nodezd2typezd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_7962, obj_t BgL_oz00_7963, obj_t BgL_vz00_7964)
	{
		{	/* Cfa/cinfo.sch 1017 */
			return
				BGl_kwotezf2nodezd2typezd2setz12ze0zzcfa_approxz00(
				((BgL_kwotez00_bglt) BgL_oz00_7963),
				((BgL_typez00_bglt) BgL_vz00_7964));
		}

	}



/* kwote/node-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_kwotezf2nodezd2locz20zzcfa_approxz00(BgL_kwotez00_bglt BgL_oz00_607)
	{
		{	/* Cfa/cinfo.sch 1018 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_607)))->BgL_locz00);
		}

	}



/* &kwote/node-loc */
	obj_t BGl_z62kwotezf2nodezd2locz42zzcfa_approxz00(obj_t BgL_envz00_7965,
		obj_t BgL_oz00_7966)
	{
		{	/* Cfa/cinfo.sch 1018 */
			return
				BGl_kwotezf2nodezd2locz20zzcfa_approxz00(
				((BgL_kwotez00_bglt) BgL_oz00_7966));
		}

	}



/* make-kwote/Cinfo */
	BGL_EXPORTED_DEF BgL_kwotez00_bglt
		BGl_makezd2kwotezf2Cinfoz20zzcfa_approxz00(obj_t BgL_loc1665z00_610,
		BgL_typez00_bglt BgL_type1666z00_611, obj_t BgL_value1667z00_612,
		BgL_approxz00_bglt BgL_approx1668z00_613)
	{
		{	/* Cfa/cinfo.sch 1022 */
			{	/* Cfa/cinfo.sch 1022 */
				BgL_kwotez00_bglt BgL_new1237z00_8969;

				{	/* Cfa/cinfo.sch 1022 */
					BgL_kwotez00_bglt BgL_tmp1235z00_8970;
					BgL_kwotezf2cinfozf2_bglt BgL_wide1236z00_8971;

					{
						BgL_kwotez00_bglt BgL_auxz00_11851;

						{	/* Cfa/cinfo.sch 1022 */
							BgL_kwotez00_bglt BgL_new1234z00_8972;

							BgL_new1234z00_8972 =
								((BgL_kwotez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_kwotez00_bgl))));
							{	/* Cfa/cinfo.sch 1022 */
								long BgL_arg1718z00_8973;

								BgL_arg1718z00_8973 = BGL_CLASS_NUM(BGl_kwotez00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1234z00_8972),
									BgL_arg1718z00_8973);
							}
							{	/* Cfa/cinfo.sch 1022 */
								BgL_objectz00_bglt BgL_tmpz00_11856;

								BgL_tmpz00_11856 = ((BgL_objectz00_bglt) BgL_new1234z00_8972);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11856, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1234z00_8972);
							BgL_auxz00_11851 = BgL_new1234z00_8972;
						}
						BgL_tmp1235z00_8970 = ((BgL_kwotez00_bglt) BgL_auxz00_11851);
					}
					BgL_wide1236z00_8971 =
						((BgL_kwotezf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_kwotezf2cinfozf2_bgl))));
					{	/* Cfa/cinfo.sch 1022 */
						obj_t BgL_auxz00_11864;
						BgL_objectz00_bglt BgL_tmpz00_11862;

						BgL_auxz00_11864 = ((obj_t) BgL_wide1236z00_8971);
						BgL_tmpz00_11862 = ((BgL_objectz00_bglt) BgL_tmp1235z00_8970);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11862, BgL_auxz00_11864);
					}
					((BgL_objectz00_bglt) BgL_tmp1235z00_8970);
					{	/* Cfa/cinfo.sch 1022 */
						long BgL_arg1717z00_8974;

						BgL_arg1717z00_8974 =
							BGL_CLASS_NUM(BGl_kwotezf2Cinfozf2zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1235z00_8970), BgL_arg1717z00_8974);
					}
					BgL_new1237z00_8969 = ((BgL_kwotez00_bglt) BgL_tmp1235z00_8970);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1237z00_8969)))->BgL_locz00) =
					((obj_t) BgL_loc1665z00_610), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1237z00_8969)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1666z00_611), BUNSPEC);
				((((BgL_kwotez00_bglt) COBJECT(((BgL_kwotez00_bglt)
									BgL_new1237z00_8969)))->BgL_valuez00) =
					((obj_t) BgL_value1667z00_612), BUNSPEC);
				{
					BgL_kwotezf2cinfozf2_bglt BgL_auxz00_11878;

					{
						obj_t BgL_auxz00_11879;

						{	/* Cfa/cinfo.sch 1022 */
							BgL_objectz00_bglt BgL_tmpz00_11880;

							BgL_tmpz00_11880 = ((BgL_objectz00_bglt) BgL_new1237z00_8969);
							BgL_auxz00_11879 = BGL_OBJECT_WIDENING(BgL_tmpz00_11880);
						}
						BgL_auxz00_11878 = ((BgL_kwotezf2cinfozf2_bglt) BgL_auxz00_11879);
					}
					((((BgL_kwotezf2cinfozf2_bglt) COBJECT(BgL_auxz00_11878))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1668z00_613), BUNSPEC);
				}
				return BgL_new1237z00_8969;
			}
		}

	}



/* &make-kwote/Cinfo */
	BgL_kwotez00_bglt BGl_z62makezd2kwotezf2Cinfoz42zzcfa_approxz00(obj_t
		BgL_envz00_7967, obj_t BgL_loc1665z00_7968, obj_t BgL_type1666z00_7969,
		obj_t BgL_value1667z00_7970, obj_t BgL_approx1668z00_7971)
	{
		{	/* Cfa/cinfo.sch 1022 */
			return
				BGl_makezd2kwotezf2Cinfoz20zzcfa_approxz00(BgL_loc1665z00_7968,
				((BgL_typez00_bglt) BgL_type1666z00_7969), BgL_value1667z00_7970,
				((BgL_approxz00_bglt) BgL_approx1668z00_7971));
		}

	}



/* kwote/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_kwotezf2Cinfozf3z01zzcfa_approxz00(obj_t
		BgL_objz00_614)
	{
		{	/* Cfa/cinfo.sch 1023 */
			{	/* Cfa/cinfo.sch 1023 */
				obj_t BgL_classz00_8975;

				BgL_classz00_8975 = BGl_kwotezf2Cinfozf2zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_614))
					{	/* Cfa/cinfo.sch 1023 */
						BgL_objectz00_bglt BgL_arg1807z00_8976;

						BgL_arg1807z00_8976 = (BgL_objectz00_bglt) (BgL_objz00_614);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 1023 */
								long BgL_idxz00_8977;

								BgL_idxz00_8977 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8976);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8977 + 3L)) == BgL_classz00_8975);
							}
						else
							{	/* Cfa/cinfo.sch 1023 */
								bool_t BgL_res2182z00_8980;

								{	/* Cfa/cinfo.sch 1023 */
									obj_t BgL_oclassz00_8981;

									{	/* Cfa/cinfo.sch 1023 */
										obj_t BgL_arg1815z00_8982;
										long BgL_arg1816z00_8983;

										BgL_arg1815z00_8982 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 1023 */
											long BgL_arg1817z00_8984;

											BgL_arg1817z00_8984 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8976);
											BgL_arg1816z00_8983 = (BgL_arg1817z00_8984 - OBJECT_TYPE);
										}
										BgL_oclassz00_8981 =
											VECTOR_REF(BgL_arg1815z00_8982, BgL_arg1816z00_8983);
									}
									{	/* Cfa/cinfo.sch 1023 */
										bool_t BgL__ortest_1115z00_8985;

										BgL__ortest_1115z00_8985 =
											(BgL_classz00_8975 == BgL_oclassz00_8981);
										if (BgL__ortest_1115z00_8985)
											{	/* Cfa/cinfo.sch 1023 */
												BgL_res2182z00_8980 = BgL__ortest_1115z00_8985;
											}
										else
											{	/* Cfa/cinfo.sch 1023 */
												long BgL_odepthz00_8986;

												{	/* Cfa/cinfo.sch 1023 */
													obj_t BgL_arg1804z00_8987;

													BgL_arg1804z00_8987 = (BgL_oclassz00_8981);
													BgL_odepthz00_8986 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8987);
												}
												if ((3L < BgL_odepthz00_8986))
													{	/* Cfa/cinfo.sch 1023 */
														obj_t BgL_arg1802z00_8988;

														{	/* Cfa/cinfo.sch 1023 */
															obj_t BgL_arg1803z00_8989;

															BgL_arg1803z00_8989 = (BgL_oclassz00_8981);
															BgL_arg1802z00_8988 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8989,
																3L);
														}
														BgL_res2182z00_8980 =
															(BgL_arg1802z00_8988 == BgL_classz00_8975);
													}
												else
													{	/* Cfa/cinfo.sch 1023 */
														BgL_res2182z00_8980 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2182z00_8980;
							}
					}
				else
					{	/* Cfa/cinfo.sch 1023 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &kwote/Cinfo? */
	obj_t BGl_z62kwotezf2Cinfozf3z63zzcfa_approxz00(obj_t BgL_envz00_7972,
		obj_t BgL_objz00_7973)
	{
		{	/* Cfa/cinfo.sch 1023 */
			return BBOOL(BGl_kwotezf2Cinfozf3z01zzcfa_approxz00(BgL_objz00_7973));
		}

	}



/* kwote/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_kwotez00_bglt
		BGl_kwotezf2Cinfozd2nilz20zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 1024 */
			{	/* Cfa/cinfo.sch 1024 */
				obj_t BgL_classz00_5531;

				BgL_classz00_5531 = BGl_kwotezf2Cinfozf2zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 1024 */
					obj_t BgL__ortest_1117z00_5532;

					BgL__ortest_1117z00_5532 = BGL_CLASS_NIL(BgL_classz00_5531);
					if (CBOOL(BgL__ortest_1117z00_5532))
						{	/* Cfa/cinfo.sch 1024 */
							return ((BgL_kwotez00_bglt) BgL__ortest_1117z00_5532);
						}
					else
						{	/* Cfa/cinfo.sch 1024 */
							return
								((BgL_kwotez00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5531));
						}
				}
			}
		}

	}



/* &kwote/Cinfo-nil */
	BgL_kwotez00_bglt BGl_z62kwotezf2Cinfozd2nilz42zzcfa_approxz00(obj_t
		BgL_envz00_7974)
	{
		{	/* Cfa/cinfo.sch 1024 */
			return BGl_kwotezf2Cinfozd2nilz20zzcfa_approxz00();
		}

	}



/* kwote/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_kwotezf2Cinfozd2approxz20zzcfa_approxz00(BgL_kwotez00_bglt BgL_oz00_615)
	{
		{	/* Cfa/cinfo.sch 1025 */
			{
				BgL_kwotezf2cinfozf2_bglt BgL_auxz00_11919;

				{
					obj_t BgL_auxz00_11920;

					{	/* Cfa/cinfo.sch 1025 */
						BgL_objectz00_bglt BgL_tmpz00_11921;

						BgL_tmpz00_11921 = ((BgL_objectz00_bglt) BgL_oz00_615);
						BgL_auxz00_11920 = BGL_OBJECT_WIDENING(BgL_tmpz00_11921);
					}
					BgL_auxz00_11919 = ((BgL_kwotezf2cinfozf2_bglt) BgL_auxz00_11920);
				}
				return
					(((BgL_kwotezf2cinfozf2_bglt) COBJECT(BgL_auxz00_11919))->
					BgL_approxz00);
			}
		}

	}



/* &kwote/Cinfo-approx */
	BgL_approxz00_bglt BGl_z62kwotezf2Cinfozd2approxz42zzcfa_approxz00(obj_t
		BgL_envz00_7975, obj_t BgL_oz00_7976)
	{
		{	/* Cfa/cinfo.sch 1025 */
			return
				BGl_kwotezf2Cinfozd2approxz20zzcfa_approxz00(
				((BgL_kwotez00_bglt) BgL_oz00_7976));
		}

	}



/* kwote/Cinfo-value */
	BGL_EXPORTED_DEF obj_t
		BGl_kwotezf2Cinfozd2valuez20zzcfa_approxz00(BgL_kwotez00_bglt BgL_oz00_618)
	{
		{	/* Cfa/cinfo.sch 1027 */
			return
				(((BgL_kwotez00_bglt) COBJECT(
						((BgL_kwotez00_bglt) BgL_oz00_618)))->BgL_valuez00);
		}

	}



/* &kwote/Cinfo-value */
	obj_t BGl_z62kwotezf2Cinfozd2valuez42zzcfa_approxz00(obj_t BgL_envz00_7977,
		obj_t BgL_oz00_7978)
	{
		{	/* Cfa/cinfo.sch 1027 */
			return
				BGl_kwotezf2Cinfozd2valuez20zzcfa_approxz00(
				((BgL_kwotez00_bglt) BgL_oz00_7978));
		}

	}



/* kwote/Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_kwotezf2Cinfozd2typez20zzcfa_approxz00(BgL_kwotez00_bglt BgL_oz00_621)
	{
		{	/* Cfa/cinfo.sch 1029 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_621)))->BgL_typez00);
		}

	}



/* &kwote/Cinfo-type */
	BgL_typez00_bglt BGl_z62kwotezf2Cinfozd2typez42zzcfa_approxz00(obj_t
		BgL_envz00_7979, obj_t BgL_oz00_7980)
	{
		{	/* Cfa/cinfo.sch 1029 */
			return
				BGl_kwotezf2Cinfozd2typez20zzcfa_approxz00(
				((BgL_kwotez00_bglt) BgL_oz00_7980));
		}

	}



/* kwote/Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_kwotezf2Cinfozd2typezd2setz12ze0zzcfa_approxz00(BgL_kwotez00_bglt
		BgL_oz00_622, BgL_typez00_bglt BgL_vz00_623)
	{
		{	/* Cfa/cinfo.sch 1030 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_622)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_623), BUNSPEC);
		}

	}



/* &kwote/Cinfo-type-set! */
	obj_t BGl_z62kwotezf2Cinfozd2typezd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_7981, obj_t BgL_oz00_7982, obj_t BgL_vz00_7983)
	{
		{	/* Cfa/cinfo.sch 1030 */
			return
				BGl_kwotezf2Cinfozd2typezd2setz12ze0zzcfa_approxz00(
				((BgL_kwotez00_bglt) BgL_oz00_7982),
				((BgL_typez00_bglt) BgL_vz00_7983));
		}

	}



/* kwote/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_kwotezf2Cinfozd2locz20zzcfa_approxz00(BgL_kwotez00_bglt BgL_oz00_624)
	{
		{	/* Cfa/cinfo.sch 1031 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_624)))->BgL_locz00);
		}

	}



/* &kwote/Cinfo-loc */
	obj_t BGl_z62kwotezf2Cinfozd2locz42zzcfa_approxz00(obj_t BgL_envz00_7984,
		obj_t BgL_oz00_7985)
	{
		{	/* Cfa/cinfo.sch 1031 */
			return
				BGl_kwotezf2Cinfozd2locz20zzcfa_approxz00(
				((BgL_kwotez00_bglt) BgL_oz00_7985));
		}

	}



/* make-app-ly/Cinfo */
	BGL_EXPORTED_DEF BgL_appzd2lyzd2_bglt
		BGl_makezd2appzd2lyzf2Cinfozf2zzcfa_approxz00(obj_t BgL_loc1656z00_627,
		BgL_typez00_bglt BgL_type1657z00_628, BgL_nodez00_bglt BgL_fun1658z00_629,
		BgL_nodez00_bglt BgL_arg1662z00_630,
		BgL_approxz00_bglt BgL_approx1663z00_631)
	{
		{	/* Cfa/cinfo.sch 1035 */
			{	/* Cfa/cinfo.sch 1035 */
				BgL_appzd2lyzd2_bglt BgL_new1242z00_8990;

				{	/* Cfa/cinfo.sch 1035 */
					BgL_appzd2lyzd2_bglt BgL_tmp1239z00_8991;
					BgL_appzd2lyzf2cinfoz20_bglt BgL_wide1240z00_8992;

					{
						BgL_appzd2lyzd2_bglt BgL_auxz00_11945;

						{	/* Cfa/cinfo.sch 1035 */
							BgL_appzd2lyzd2_bglt BgL_new1238z00_8993;

							BgL_new1238z00_8993 =
								((BgL_appzd2lyzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_appzd2lyzd2_bgl))));
							{	/* Cfa/cinfo.sch 1035 */
								long BgL_arg1722z00_8994;

								BgL_arg1722z00_8994 =
									BGL_CLASS_NUM(BGl_appzd2lyzd2zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1238z00_8993),
									BgL_arg1722z00_8994);
							}
							{	/* Cfa/cinfo.sch 1035 */
								BgL_objectz00_bglt BgL_tmpz00_11950;

								BgL_tmpz00_11950 = ((BgL_objectz00_bglt) BgL_new1238z00_8993);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11950, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1238z00_8993);
							BgL_auxz00_11945 = BgL_new1238z00_8993;
						}
						BgL_tmp1239z00_8991 = ((BgL_appzd2lyzd2_bglt) BgL_auxz00_11945);
					}
					BgL_wide1240z00_8992 =
						((BgL_appzd2lyzf2cinfoz20_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_appzd2lyzf2cinfoz20_bgl))));
					{	/* Cfa/cinfo.sch 1035 */
						obj_t BgL_auxz00_11958;
						BgL_objectz00_bglt BgL_tmpz00_11956;

						BgL_auxz00_11958 = ((obj_t) BgL_wide1240z00_8992);
						BgL_tmpz00_11956 = ((BgL_objectz00_bglt) BgL_tmp1239z00_8991);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11956, BgL_auxz00_11958);
					}
					((BgL_objectz00_bglt) BgL_tmp1239z00_8991);
					{	/* Cfa/cinfo.sch 1035 */
						long BgL_arg1720z00_8995;

						BgL_arg1720z00_8995 =
							BGL_CLASS_NUM(BGl_appzd2lyzf2Cinfoz20zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1239z00_8991), BgL_arg1720z00_8995);
					}
					BgL_new1242z00_8990 = ((BgL_appzd2lyzd2_bglt) BgL_tmp1239z00_8991);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1242z00_8990)))->BgL_locz00) =
					((obj_t) BgL_loc1656z00_627), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1242z00_8990)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1657z00_628), BUNSPEC);
				((((BgL_appzd2lyzd2_bglt) COBJECT(((BgL_appzd2lyzd2_bglt)
									BgL_new1242z00_8990)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_fun1658z00_629), BUNSPEC);
				((((BgL_appzd2lyzd2_bglt) COBJECT(((BgL_appzd2lyzd2_bglt)
									BgL_new1242z00_8990)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_arg1662z00_630), BUNSPEC);
				{
					BgL_appzd2lyzf2cinfoz20_bglt BgL_auxz00_11974;

					{
						obj_t BgL_auxz00_11975;

						{	/* Cfa/cinfo.sch 1035 */
							BgL_objectz00_bglt BgL_tmpz00_11976;

							BgL_tmpz00_11976 = ((BgL_objectz00_bglt) BgL_new1242z00_8990);
							BgL_auxz00_11975 = BGL_OBJECT_WIDENING(BgL_tmpz00_11976);
						}
						BgL_auxz00_11974 =
							((BgL_appzd2lyzf2cinfoz20_bglt) BgL_auxz00_11975);
					}
					((((BgL_appzd2lyzf2cinfoz20_bglt) COBJECT(BgL_auxz00_11974))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1663z00_631), BUNSPEC);
				}
				return BgL_new1242z00_8990;
			}
		}

	}



/* &make-app-ly/Cinfo */
	BgL_appzd2lyzd2_bglt BGl_z62makezd2appzd2lyzf2Cinfoz90zzcfa_approxz00(obj_t
		BgL_envz00_7986, obj_t BgL_loc1656z00_7987, obj_t BgL_type1657z00_7988,
		obj_t BgL_fun1658z00_7989, obj_t BgL_arg1662z00_7990,
		obj_t BgL_approx1663z00_7991)
	{
		{	/* Cfa/cinfo.sch 1035 */
			return
				BGl_makezd2appzd2lyzf2Cinfozf2zzcfa_approxz00(BgL_loc1656z00_7987,
				((BgL_typez00_bglt) BgL_type1657z00_7988),
				((BgL_nodez00_bglt) BgL_fun1658z00_7989),
				((BgL_nodez00_bglt) BgL_arg1662z00_7990),
				((BgL_approxz00_bglt) BgL_approx1663z00_7991));
		}

	}



/* app-ly/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_appzd2lyzf2Cinfozf3zd3zzcfa_approxz00(obj_t
		BgL_objz00_632)
	{
		{	/* Cfa/cinfo.sch 1036 */
			{	/* Cfa/cinfo.sch 1036 */
				obj_t BgL_classz00_8996;

				BgL_classz00_8996 = BGl_appzd2lyzf2Cinfoz20zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_632))
					{	/* Cfa/cinfo.sch 1036 */
						BgL_objectz00_bglt BgL_arg1807z00_8997;

						BgL_arg1807z00_8997 = (BgL_objectz00_bglt) (BgL_objz00_632);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 1036 */
								long BgL_idxz00_8998;

								BgL_idxz00_8998 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8997);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8998 + 3L)) == BgL_classz00_8996);
							}
						else
							{	/* Cfa/cinfo.sch 1036 */
								bool_t BgL_res2183z00_9001;

								{	/* Cfa/cinfo.sch 1036 */
									obj_t BgL_oclassz00_9002;

									{	/* Cfa/cinfo.sch 1036 */
										obj_t BgL_arg1815z00_9003;
										long BgL_arg1816z00_9004;

										BgL_arg1815z00_9003 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 1036 */
											long BgL_arg1817z00_9005;

											BgL_arg1817z00_9005 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8997);
											BgL_arg1816z00_9004 = (BgL_arg1817z00_9005 - OBJECT_TYPE);
										}
										BgL_oclassz00_9002 =
											VECTOR_REF(BgL_arg1815z00_9003, BgL_arg1816z00_9004);
									}
									{	/* Cfa/cinfo.sch 1036 */
										bool_t BgL__ortest_1115z00_9006;

										BgL__ortest_1115z00_9006 =
											(BgL_classz00_8996 == BgL_oclassz00_9002);
										if (BgL__ortest_1115z00_9006)
											{	/* Cfa/cinfo.sch 1036 */
												BgL_res2183z00_9001 = BgL__ortest_1115z00_9006;
											}
										else
											{	/* Cfa/cinfo.sch 1036 */
												long BgL_odepthz00_9007;

												{	/* Cfa/cinfo.sch 1036 */
													obj_t BgL_arg1804z00_9008;

													BgL_arg1804z00_9008 = (BgL_oclassz00_9002);
													BgL_odepthz00_9007 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9008);
												}
												if ((3L < BgL_odepthz00_9007))
													{	/* Cfa/cinfo.sch 1036 */
														obj_t BgL_arg1802z00_9009;

														{	/* Cfa/cinfo.sch 1036 */
															obj_t BgL_arg1803z00_9010;

															BgL_arg1803z00_9010 = (BgL_oclassz00_9002);
															BgL_arg1802z00_9009 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9010,
																3L);
														}
														BgL_res2183z00_9001 =
															(BgL_arg1802z00_9009 == BgL_classz00_8996);
													}
												else
													{	/* Cfa/cinfo.sch 1036 */
														BgL_res2183z00_9001 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2183z00_9001;
							}
					}
				else
					{	/* Cfa/cinfo.sch 1036 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &app-ly/Cinfo? */
	obj_t BGl_z62appzd2lyzf2Cinfozf3zb1zzcfa_approxz00(obj_t BgL_envz00_7992,
		obj_t BgL_objz00_7993)
	{
		{	/* Cfa/cinfo.sch 1036 */
			return BBOOL(BGl_appzd2lyzf2Cinfozf3zd3zzcfa_approxz00(BgL_objz00_7993));
		}

	}



/* app-ly/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_appzd2lyzd2_bglt
		BGl_appzd2lyzf2Cinfozd2nilzf2zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 1037 */
			{	/* Cfa/cinfo.sch 1037 */
				obj_t BgL_classz00_5583;

				BgL_classz00_5583 = BGl_appzd2lyzf2Cinfoz20zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 1037 */
					obj_t BgL__ortest_1117z00_5584;

					BgL__ortest_1117z00_5584 = BGL_CLASS_NIL(BgL_classz00_5583);
					if (CBOOL(BgL__ortest_1117z00_5584))
						{	/* Cfa/cinfo.sch 1037 */
							return ((BgL_appzd2lyzd2_bglt) BgL__ortest_1117z00_5584);
						}
					else
						{	/* Cfa/cinfo.sch 1037 */
							return
								((BgL_appzd2lyzd2_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5583));
						}
				}
			}
		}

	}



/* &app-ly/Cinfo-nil */
	BgL_appzd2lyzd2_bglt BGl_z62appzd2lyzf2Cinfozd2nilz90zzcfa_approxz00(obj_t
		BgL_envz00_7994)
	{
		{	/* Cfa/cinfo.sch 1037 */
			return BGl_appzd2lyzf2Cinfozd2nilzf2zzcfa_approxz00();
		}

	}



/* app-ly/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_appzd2lyzf2Cinfozd2approxzf2zzcfa_approxz00(BgL_appzd2lyzd2_bglt
		BgL_oz00_633)
	{
		{	/* Cfa/cinfo.sch 1038 */
			{
				BgL_appzd2lyzf2cinfoz20_bglt BgL_auxz00_12017;

				{
					obj_t BgL_auxz00_12018;

					{	/* Cfa/cinfo.sch 1038 */
						BgL_objectz00_bglt BgL_tmpz00_12019;

						BgL_tmpz00_12019 = ((BgL_objectz00_bglt) BgL_oz00_633);
						BgL_auxz00_12018 = BGL_OBJECT_WIDENING(BgL_tmpz00_12019);
					}
					BgL_auxz00_12017 = ((BgL_appzd2lyzf2cinfoz20_bglt) BgL_auxz00_12018);
				}
				return
					(((BgL_appzd2lyzf2cinfoz20_bglt) COBJECT(BgL_auxz00_12017))->
					BgL_approxz00);
			}
		}

	}



/* &app-ly/Cinfo-approx */
	BgL_approxz00_bglt BGl_z62appzd2lyzf2Cinfozd2approxz90zzcfa_approxz00(obj_t
		BgL_envz00_7995, obj_t BgL_oz00_7996)
	{
		{	/* Cfa/cinfo.sch 1038 */
			return
				BGl_appzd2lyzf2Cinfozd2approxzf2zzcfa_approxz00(
				((BgL_appzd2lyzd2_bglt) BgL_oz00_7996));
		}

	}



/* app-ly/Cinfo-arg */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_appzd2lyzf2Cinfozd2argzf2zzcfa_approxz00(BgL_appzd2lyzd2_bglt
		BgL_oz00_636)
	{
		{	/* Cfa/cinfo.sch 1040 */
			return
				(((BgL_appzd2lyzd2_bglt) COBJECT(
						((BgL_appzd2lyzd2_bglt) BgL_oz00_636)))->BgL_argz00);
		}

	}



/* &app-ly/Cinfo-arg */
	BgL_nodez00_bglt BGl_z62appzd2lyzf2Cinfozd2argz90zzcfa_approxz00(obj_t
		BgL_envz00_7997, obj_t BgL_oz00_7998)
	{
		{	/* Cfa/cinfo.sch 1040 */
			return
				BGl_appzd2lyzf2Cinfozd2argzf2zzcfa_approxz00(
				((BgL_appzd2lyzd2_bglt) BgL_oz00_7998));
		}

	}



/* app-ly/Cinfo-arg-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_appzd2lyzf2Cinfozd2argzd2setz12z32zzcfa_approxz00(BgL_appzd2lyzd2_bglt
		BgL_oz00_637, BgL_nodez00_bglt BgL_vz00_638)
	{
		{	/* Cfa/cinfo.sch 1041 */
			return
				((((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_oz00_637)))->BgL_argz00) =
				((BgL_nodez00_bglt) BgL_vz00_638), BUNSPEC);
		}

	}



/* &app-ly/Cinfo-arg-set! */
	obj_t BGl_z62appzd2lyzf2Cinfozd2argzd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_7999, obj_t BgL_oz00_8000, obj_t BgL_vz00_8001)
	{
		{	/* Cfa/cinfo.sch 1041 */
			return
				BGl_appzd2lyzf2Cinfozd2argzd2setz12z32zzcfa_approxz00(
				((BgL_appzd2lyzd2_bglt) BgL_oz00_8000),
				((BgL_nodez00_bglt) BgL_vz00_8001));
		}

	}



/* app-ly/Cinfo-fun */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_appzd2lyzf2Cinfozd2funzf2zzcfa_approxz00(BgL_appzd2lyzd2_bglt
		BgL_oz00_639)
	{
		{	/* Cfa/cinfo.sch 1042 */
			return
				(((BgL_appzd2lyzd2_bglt) COBJECT(
						((BgL_appzd2lyzd2_bglt) BgL_oz00_639)))->BgL_funz00);
		}

	}



/* &app-ly/Cinfo-fun */
	BgL_nodez00_bglt BGl_z62appzd2lyzf2Cinfozd2funz90zzcfa_approxz00(obj_t
		BgL_envz00_8002, obj_t BgL_oz00_8003)
	{
		{	/* Cfa/cinfo.sch 1042 */
			return
				BGl_appzd2lyzf2Cinfozd2funzf2zzcfa_approxz00(
				((BgL_appzd2lyzd2_bglt) BgL_oz00_8003));
		}

	}



/* app-ly/Cinfo-fun-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_appzd2lyzf2Cinfozd2funzd2setz12z32zzcfa_approxz00(BgL_appzd2lyzd2_bglt
		BgL_oz00_640, BgL_nodez00_bglt BgL_vz00_641)
	{
		{	/* Cfa/cinfo.sch 1043 */
			return
				((((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_oz00_640)))->BgL_funz00) =
				((BgL_nodez00_bglt) BgL_vz00_641), BUNSPEC);
		}

	}



/* &app-ly/Cinfo-fun-set! */
	obj_t BGl_z62appzd2lyzf2Cinfozd2funzd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_8004, obj_t BgL_oz00_8005, obj_t BgL_vz00_8006)
	{
		{	/* Cfa/cinfo.sch 1043 */
			return
				BGl_appzd2lyzf2Cinfozd2funzd2setz12z32zzcfa_approxz00(
				((BgL_appzd2lyzd2_bglt) BgL_oz00_8005),
				((BgL_nodez00_bglt) BgL_vz00_8006));
		}

	}



/* app-ly/Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_appzd2lyzf2Cinfozd2typezf2zzcfa_approxz00(BgL_appzd2lyzd2_bglt
		BgL_oz00_642)
	{
		{	/* Cfa/cinfo.sch 1044 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_642)))->BgL_typez00);
		}

	}



/* &app-ly/Cinfo-type */
	BgL_typez00_bglt BGl_z62appzd2lyzf2Cinfozd2typez90zzcfa_approxz00(obj_t
		BgL_envz00_8007, obj_t BgL_oz00_8008)
	{
		{	/* Cfa/cinfo.sch 1044 */
			return
				BGl_appzd2lyzf2Cinfozd2typezf2zzcfa_approxz00(
				((BgL_appzd2lyzd2_bglt) BgL_oz00_8008));
		}

	}



/* app-ly/Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_appzd2lyzf2Cinfozd2typezd2setz12z32zzcfa_approxz00(BgL_appzd2lyzd2_bglt
		BgL_oz00_643, BgL_typez00_bglt BgL_vz00_644)
	{
		{	/* Cfa/cinfo.sch 1045 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_643)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_644), BUNSPEC);
		}

	}



/* &app-ly/Cinfo-type-set! */
	obj_t BGl_z62appzd2lyzf2Cinfozd2typezd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_8009, obj_t BgL_oz00_8010, obj_t BgL_vz00_8011)
	{
		{	/* Cfa/cinfo.sch 1045 */
			return
				BGl_appzd2lyzf2Cinfozd2typezd2setz12z32zzcfa_approxz00(
				((BgL_appzd2lyzd2_bglt) BgL_oz00_8010),
				((BgL_typez00_bglt) BgL_vz00_8011));
		}

	}



/* app-ly/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_appzd2lyzf2Cinfozd2loczf2zzcfa_approxz00(BgL_appzd2lyzd2_bglt
		BgL_oz00_645)
	{
		{	/* Cfa/cinfo.sch 1046 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_645)))->BgL_locz00);
		}

	}



/* &app-ly/Cinfo-loc */
	obj_t BGl_z62appzd2lyzf2Cinfozd2locz90zzcfa_approxz00(obj_t BgL_envz00_8012,
		obj_t BgL_oz00_8013)
	{
		{	/* Cfa/cinfo.sch 1046 */
			return
				BGl_appzd2lyzf2Cinfozd2loczf2zzcfa_approxz00(
				((BgL_appzd2lyzd2_bglt) BgL_oz00_8013));
		}

	}



/* make-funcall/Cinfo */
	BGL_EXPORTED_DEF BgL_funcallz00_bglt
		BGl_makezd2funcallzf2Cinfoz20zzcfa_approxz00(obj_t BgL_loc1644z00_648,
		BgL_typez00_bglt BgL_type1645z00_649, BgL_nodez00_bglt BgL_fun1646z00_650,
		obj_t BgL_args1647z00_651, obj_t BgL_strength1648z00_652,
		obj_t BgL_functions1649z00_653, BgL_approxz00_bglt BgL_approx1651z00_654,
		BgL_approxz00_bglt BgL_vazd2approx1652zd2_655,
		bool_t BgL_arityzd2errorzd2noticedzf31653zf3_656,
		bool_t BgL_typezd2errorzd2noticedzf31654zf3_657)
	{
		{	/* Cfa/cinfo.sch 1050 */
			{	/* Cfa/cinfo.sch 1050 */
				BgL_funcallz00_bglt BgL_new1246z00_9011;

				{	/* Cfa/cinfo.sch 1050 */
					BgL_funcallz00_bglt BgL_tmp1244z00_9012;
					BgL_funcallzf2cinfozf2_bglt BgL_wide1245z00_9013;

					{
						BgL_funcallz00_bglt BgL_auxz00_12057;

						{	/* Cfa/cinfo.sch 1050 */
							BgL_funcallz00_bglt BgL_new1243z00_9014;

							BgL_new1243z00_9014 =
								((BgL_funcallz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_funcallz00_bgl))));
							{	/* Cfa/cinfo.sch 1050 */
								long BgL_arg1733z00_9015;

								BgL_arg1733z00_9015 =
									BGL_CLASS_NUM(BGl_funcallz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1243z00_9014),
									BgL_arg1733z00_9015);
							}
							{	/* Cfa/cinfo.sch 1050 */
								BgL_objectz00_bglt BgL_tmpz00_12062;

								BgL_tmpz00_12062 = ((BgL_objectz00_bglt) BgL_new1243z00_9014);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12062, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1243z00_9014);
							BgL_auxz00_12057 = BgL_new1243z00_9014;
						}
						BgL_tmp1244z00_9012 = ((BgL_funcallz00_bglt) BgL_auxz00_12057);
					}
					BgL_wide1245z00_9013 =
						((BgL_funcallzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_funcallzf2cinfozf2_bgl))));
					{	/* Cfa/cinfo.sch 1050 */
						obj_t BgL_auxz00_12070;
						BgL_objectz00_bglt BgL_tmpz00_12068;

						BgL_auxz00_12070 = ((obj_t) BgL_wide1245z00_9013);
						BgL_tmpz00_12068 = ((BgL_objectz00_bglt) BgL_tmp1244z00_9012);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12068, BgL_auxz00_12070);
					}
					((BgL_objectz00_bglt) BgL_tmp1244z00_9012);
					{	/* Cfa/cinfo.sch 1050 */
						long BgL_arg1724z00_9016;

						BgL_arg1724z00_9016 =
							BGL_CLASS_NUM(BGl_funcallzf2Cinfozf2zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1244z00_9012), BgL_arg1724z00_9016);
					}
					BgL_new1246z00_9011 = ((BgL_funcallz00_bglt) BgL_tmp1244z00_9012);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1246z00_9011)))->BgL_locz00) =
					((obj_t) BgL_loc1644z00_648), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1246z00_9011)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1645z00_649), BUNSPEC);
				((((BgL_funcallz00_bglt) COBJECT(((BgL_funcallz00_bglt)
									BgL_new1246z00_9011)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_fun1646z00_650), BUNSPEC);
				((((BgL_funcallz00_bglt) COBJECT(((BgL_funcallz00_bglt)
									BgL_new1246z00_9011)))->BgL_argsz00) =
					((obj_t) BgL_args1647z00_651), BUNSPEC);
				((((BgL_funcallz00_bglt) COBJECT(((BgL_funcallz00_bglt)
									BgL_new1246z00_9011)))->BgL_strengthz00) =
					((obj_t) BgL_strength1648z00_652), BUNSPEC);
				((((BgL_funcallz00_bglt) COBJECT(((BgL_funcallz00_bglt)
									BgL_new1246z00_9011)))->BgL_functionsz00) =
					((obj_t) BgL_functions1649z00_653), BUNSPEC);
				{
					BgL_funcallzf2cinfozf2_bglt BgL_auxz00_12090;

					{
						obj_t BgL_auxz00_12091;

						{	/* Cfa/cinfo.sch 1050 */
							BgL_objectz00_bglt BgL_tmpz00_12092;

							BgL_tmpz00_12092 = ((BgL_objectz00_bglt) BgL_new1246z00_9011);
							BgL_auxz00_12091 = BGL_OBJECT_WIDENING(BgL_tmpz00_12092);
						}
						BgL_auxz00_12090 = ((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_12091);
					}
					((((BgL_funcallzf2cinfozf2_bglt) COBJECT(BgL_auxz00_12090))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1651z00_654), BUNSPEC);
				}
				{
					BgL_funcallzf2cinfozf2_bglt BgL_auxz00_12097;

					{
						obj_t BgL_auxz00_12098;

						{	/* Cfa/cinfo.sch 1050 */
							BgL_objectz00_bglt BgL_tmpz00_12099;

							BgL_tmpz00_12099 = ((BgL_objectz00_bglt) BgL_new1246z00_9011);
							BgL_auxz00_12098 = BGL_OBJECT_WIDENING(BgL_tmpz00_12099);
						}
						BgL_auxz00_12097 = ((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_12098);
					}
					((((BgL_funcallzf2cinfozf2_bglt) COBJECT(BgL_auxz00_12097))->
							BgL_vazd2approxzd2) =
						((BgL_approxz00_bglt) BgL_vazd2approx1652zd2_655), BUNSPEC);
				}
				{
					BgL_funcallzf2cinfozf2_bglt BgL_auxz00_12104;

					{
						obj_t BgL_auxz00_12105;

						{	/* Cfa/cinfo.sch 1050 */
							BgL_objectz00_bglt BgL_tmpz00_12106;

							BgL_tmpz00_12106 = ((BgL_objectz00_bglt) BgL_new1246z00_9011);
							BgL_auxz00_12105 = BGL_OBJECT_WIDENING(BgL_tmpz00_12106);
						}
						BgL_auxz00_12104 = ((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_12105);
					}
					((((BgL_funcallzf2cinfozf2_bglt) COBJECT(BgL_auxz00_12104))->
							BgL_arityzd2errorzd2noticedzf3zf3) =
						((bool_t) BgL_arityzd2errorzd2noticedzf31653zf3_656), BUNSPEC);
				}
				{
					BgL_funcallzf2cinfozf2_bglt BgL_auxz00_12111;

					{
						obj_t BgL_auxz00_12112;

						{	/* Cfa/cinfo.sch 1050 */
							BgL_objectz00_bglt BgL_tmpz00_12113;

							BgL_tmpz00_12113 = ((BgL_objectz00_bglt) BgL_new1246z00_9011);
							BgL_auxz00_12112 = BGL_OBJECT_WIDENING(BgL_tmpz00_12113);
						}
						BgL_auxz00_12111 = ((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_12112);
					}
					((((BgL_funcallzf2cinfozf2_bglt) COBJECT(BgL_auxz00_12111))->
							BgL_typezd2errorzd2noticedzf3zf3) =
						((bool_t) BgL_typezd2errorzd2noticedzf31654zf3_657), BUNSPEC);
				}
				return BgL_new1246z00_9011;
			}
		}

	}



/* &make-funcall/Cinfo */
	BgL_funcallz00_bglt BGl_z62makezd2funcallzf2Cinfoz42zzcfa_approxz00(obj_t
		BgL_envz00_8014, obj_t BgL_loc1644z00_8015, obj_t BgL_type1645z00_8016,
		obj_t BgL_fun1646z00_8017, obj_t BgL_args1647z00_8018,
		obj_t BgL_strength1648z00_8019, obj_t BgL_functions1649z00_8020,
		obj_t BgL_approx1651z00_8021, obj_t BgL_vazd2approx1652zd2_8022,
		obj_t BgL_arityzd2errorzd2noticedzf31653zf3_8023,
		obj_t BgL_typezd2errorzd2noticedzf31654zf3_8024)
	{
		{	/* Cfa/cinfo.sch 1050 */
			return
				BGl_makezd2funcallzf2Cinfoz20zzcfa_approxz00(BgL_loc1644z00_8015,
				((BgL_typez00_bglt) BgL_type1645z00_8016),
				((BgL_nodez00_bglt) BgL_fun1646z00_8017), BgL_args1647z00_8018,
				BgL_strength1648z00_8019, BgL_functions1649z00_8020,
				((BgL_approxz00_bglt) BgL_approx1651z00_8021),
				((BgL_approxz00_bglt) BgL_vazd2approx1652zd2_8022),
				CBOOL(BgL_arityzd2errorzd2noticedzf31653zf3_8023),
				CBOOL(BgL_typezd2errorzd2noticedzf31654zf3_8024));
		}

	}



/* funcall/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_funcallzf2Cinfozf3z01zzcfa_approxz00(obj_t
		BgL_objz00_658)
	{
		{	/* Cfa/cinfo.sch 1051 */
			{	/* Cfa/cinfo.sch 1051 */
				obj_t BgL_classz00_9017;

				BgL_classz00_9017 = BGl_funcallzf2Cinfozf2zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_658))
					{	/* Cfa/cinfo.sch 1051 */
						BgL_objectz00_bglt BgL_arg1807z00_9018;

						BgL_arg1807z00_9018 = (BgL_objectz00_bglt) (BgL_objz00_658);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 1051 */
								long BgL_idxz00_9019;

								BgL_idxz00_9019 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9018);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9019 + 3L)) == BgL_classz00_9017);
							}
						else
							{	/* Cfa/cinfo.sch 1051 */
								bool_t BgL_res2184z00_9022;

								{	/* Cfa/cinfo.sch 1051 */
									obj_t BgL_oclassz00_9023;

									{	/* Cfa/cinfo.sch 1051 */
										obj_t BgL_arg1815z00_9024;
										long BgL_arg1816z00_9025;

										BgL_arg1815z00_9024 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 1051 */
											long BgL_arg1817z00_9026;

											BgL_arg1817z00_9026 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9018);
											BgL_arg1816z00_9025 = (BgL_arg1817z00_9026 - OBJECT_TYPE);
										}
										BgL_oclassz00_9023 =
											VECTOR_REF(BgL_arg1815z00_9024, BgL_arg1816z00_9025);
									}
									{	/* Cfa/cinfo.sch 1051 */
										bool_t BgL__ortest_1115z00_9027;

										BgL__ortest_1115z00_9027 =
											(BgL_classz00_9017 == BgL_oclassz00_9023);
										if (BgL__ortest_1115z00_9027)
											{	/* Cfa/cinfo.sch 1051 */
												BgL_res2184z00_9022 = BgL__ortest_1115z00_9027;
											}
										else
											{	/* Cfa/cinfo.sch 1051 */
												long BgL_odepthz00_9028;

												{	/* Cfa/cinfo.sch 1051 */
													obj_t BgL_arg1804z00_9029;

													BgL_arg1804z00_9029 = (BgL_oclassz00_9023);
													BgL_odepthz00_9028 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9029);
												}
												if ((3L < BgL_odepthz00_9028))
													{	/* Cfa/cinfo.sch 1051 */
														obj_t BgL_arg1802z00_9030;

														{	/* Cfa/cinfo.sch 1051 */
															obj_t BgL_arg1803z00_9031;

															BgL_arg1803z00_9031 = (BgL_oclassz00_9023);
															BgL_arg1802z00_9030 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9031,
																3L);
														}
														BgL_res2184z00_9022 =
															(BgL_arg1802z00_9030 == BgL_classz00_9017);
													}
												else
													{	/* Cfa/cinfo.sch 1051 */
														BgL_res2184z00_9022 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2184z00_9022;
							}
					}
				else
					{	/* Cfa/cinfo.sch 1051 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &funcall/Cinfo? */
	obj_t BGl_z62funcallzf2Cinfozf3z63zzcfa_approxz00(obj_t BgL_envz00_8025,
		obj_t BgL_objz00_8026)
	{
		{	/* Cfa/cinfo.sch 1051 */
			return BBOOL(BGl_funcallzf2Cinfozf3z01zzcfa_approxz00(BgL_objz00_8026));
		}

	}



/* funcall/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_funcallz00_bglt
		BGl_funcallzf2Cinfozd2nilz20zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 1052 */
			{	/* Cfa/cinfo.sch 1052 */
				obj_t BgL_classz00_5638;

				BgL_classz00_5638 = BGl_funcallzf2Cinfozf2zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 1052 */
					obj_t BgL__ortest_1117z00_5639;

					BgL__ortest_1117z00_5639 = BGL_CLASS_NIL(BgL_classz00_5638);
					if (CBOOL(BgL__ortest_1117z00_5639))
						{	/* Cfa/cinfo.sch 1052 */
							return ((BgL_funcallz00_bglt) BgL__ortest_1117z00_5639);
						}
					else
						{	/* Cfa/cinfo.sch 1052 */
							return
								((BgL_funcallz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5638));
						}
				}
			}
		}

	}



/* &funcall/Cinfo-nil */
	BgL_funcallz00_bglt BGl_z62funcallzf2Cinfozd2nilz42zzcfa_approxz00(obj_t
		BgL_envz00_8027)
	{
		{	/* Cfa/cinfo.sch 1052 */
			return BGl_funcallzf2Cinfozd2nilz20zzcfa_approxz00();
		}

	}



/* funcall/Cinfo-type-error-noticed? */
	BGL_EXPORTED_DEF bool_t
		BGl_funcallzf2Cinfozd2typezd2errorzd2noticedzf3zd3zzcfa_approxz00
		(BgL_funcallz00_bglt BgL_oz00_659)
	{
		{	/* Cfa/cinfo.sch 1053 */
			{
				BgL_funcallzf2cinfozf2_bglt BgL_auxz00_12156;

				{
					obj_t BgL_auxz00_12157;

					{	/* Cfa/cinfo.sch 1053 */
						BgL_objectz00_bglt BgL_tmpz00_12158;

						BgL_tmpz00_12158 = ((BgL_objectz00_bglt) BgL_oz00_659);
						BgL_auxz00_12157 = BGL_OBJECT_WIDENING(BgL_tmpz00_12158);
					}
					BgL_auxz00_12156 = ((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_12157);
				}
				return
					(((BgL_funcallzf2cinfozf2_bglt) COBJECT(BgL_auxz00_12156))->
					BgL_typezd2errorzd2noticedzf3zf3);
			}
		}

	}



/* &funcall/Cinfo-type-error-noticed? */
	obj_t
		BGl_z62funcallzf2Cinfozd2typezd2errorzd2noticedzf3zb1zzcfa_approxz00(obj_t
		BgL_envz00_8028, obj_t BgL_oz00_8029)
	{
		{	/* Cfa/cinfo.sch 1053 */
			return
				BBOOL(BGl_funcallzf2Cinfozd2typezd2errorzd2noticedzf3zd3zzcfa_approxz00(
					((BgL_funcallz00_bglt) BgL_oz00_8029)));
		}

	}



/* funcall/Cinfo-type-error-noticed?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_funcallzf2Cinfozd2typezd2errorzd2noticedzf3zd2setz12z13zzcfa_approxz00
		(BgL_funcallz00_bglt BgL_oz00_660, bool_t BgL_vz00_661)
	{
		{	/* Cfa/cinfo.sch 1054 */
			{
				BgL_funcallzf2cinfozf2_bglt BgL_auxz00_12166;

				{
					obj_t BgL_auxz00_12167;

					{	/* Cfa/cinfo.sch 1054 */
						BgL_objectz00_bglt BgL_tmpz00_12168;

						BgL_tmpz00_12168 = ((BgL_objectz00_bglt) BgL_oz00_660);
						BgL_auxz00_12167 = BGL_OBJECT_WIDENING(BgL_tmpz00_12168);
					}
					BgL_auxz00_12166 = ((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_12167);
				}
				return
					((((BgL_funcallzf2cinfozf2_bglt) COBJECT(BgL_auxz00_12166))->
						BgL_typezd2errorzd2noticedzf3zf3) =
					((bool_t) BgL_vz00_661), BUNSPEC);
			}
		}

	}



/* &funcall/Cinfo-type-error-noticed?-set! */
	obj_t
		BGl_z62funcallzf2Cinfozd2typezd2errorzd2noticedzf3zd2setz12z71zzcfa_approxz00
		(obj_t BgL_envz00_8030, obj_t BgL_oz00_8031, obj_t BgL_vz00_8032)
	{
		{	/* Cfa/cinfo.sch 1054 */
			return
				BGl_funcallzf2Cinfozd2typezd2errorzd2noticedzf3zd2setz12z13zzcfa_approxz00
				(((BgL_funcallz00_bglt) BgL_oz00_8031), CBOOL(BgL_vz00_8032));
		}

	}



/* funcall/Cinfo-arity-error-noticed? */
	BGL_EXPORTED_DEF bool_t
		BGl_funcallzf2Cinfozd2arityzd2errorzd2noticedzf3zd3zzcfa_approxz00
		(BgL_funcallz00_bglt BgL_oz00_662)
	{
		{	/* Cfa/cinfo.sch 1055 */
			{
				BgL_funcallzf2cinfozf2_bglt BgL_auxz00_12176;

				{
					obj_t BgL_auxz00_12177;

					{	/* Cfa/cinfo.sch 1055 */
						BgL_objectz00_bglt BgL_tmpz00_12178;

						BgL_tmpz00_12178 = ((BgL_objectz00_bglt) BgL_oz00_662);
						BgL_auxz00_12177 = BGL_OBJECT_WIDENING(BgL_tmpz00_12178);
					}
					BgL_auxz00_12176 = ((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_12177);
				}
				return
					(((BgL_funcallzf2cinfozf2_bglt) COBJECT(BgL_auxz00_12176))->
					BgL_arityzd2errorzd2noticedzf3zf3);
			}
		}

	}



/* &funcall/Cinfo-arity-error-noticed? */
	obj_t
		BGl_z62funcallzf2Cinfozd2arityzd2errorzd2noticedzf3zb1zzcfa_approxz00(obj_t
		BgL_envz00_8033, obj_t BgL_oz00_8034)
	{
		{	/* Cfa/cinfo.sch 1055 */
			return
				BBOOL(BGl_funcallzf2Cinfozd2arityzd2errorzd2noticedzf3zd3zzcfa_approxz00
				(((BgL_funcallz00_bglt) BgL_oz00_8034)));
		}

	}



/* funcall/Cinfo-arity-error-noticed?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_funcallzf2Cinfozd2arityzd2errorzd2noticedzf3zd2setz12z13zzcfa_approxz00
		(BgL_funcallz00_bglt BgL_oz00_663, bool_t BgL_vz00_664)
	{
		{	/* Cfa/cinfo.sch 1056 */
			{
				BgL_funcallzf2cinfozf2_bglt BgL_auxz00_12186;

				{
					obj_t BgL_auxz00_12187;

					{	/* Cfa/cinfo.sch 1056 */
						BgL_objectz00_bglt BgL_tmpz00_12188;

						BgL_tmpz00_12188 = ((BgL_objectz00_bglt) BgL_oz00_663);
						BgL_auxz00_12187 = BGL_OBJECT_WIDENING(BgL_tmpz00_12188);
					}
					BgL_auxz00_12186 = ((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_12187);
				}
				return
					((((BgL_funcallzf2cinfozf2_bglt) COBJECT(BgL_auxz00_12186))->
						BgL_arityzd2errorzd2noticedzf3zf3) =
					((bool_t) BgL_vz00_664), BUNSPEC);
			}
		}

	}



/* &funcall/Cinfo-arity-error-noticed?-set! */
	obj_t
		BGl_z62funcallzf2Cinfozd2arityzd2errorzd2noticedzf3zd2setz12z71zzcfa_approxz00
		(obj_t BgL_envz00_8035, obj_t BgL_oz00_8036, obj_t BgL_vz00_8037)
	{
		{	/* Cfa/cinfo.sch 1056 */
			return
				BGl_funcallzf2Cinfozd2arityzd2errorzd2noticedzf3zd2setz12z13zzcfa_approxz00
				(((BgL_funcallz00_bglt) BgL_oz00_8036), CBOOL(BgL_vz00_8037));
		}

	}



/* funcall/Cinfo-va-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_funcallzf2Cinfozd2vazd2approxzf2zzcfa_approxz00(BgL_funcallz00_bglt
		BgL_oz00_665)
	{
		{	/* Cfa/cinfo.sch 1057 */
			{
				BgL_funcallzf2cinfozf2_bglt BgL_auxz00_12196;

				{
					obj_t BgL_auxz00_12197;

					{	/* Cfa/cinfo.sch 1057 */
						BgL_objectz00_bglt BgL_tmpz00_12198;

						BgL_tmpz00_12198 = ((BgL_objectz00_bglt) BgL_oz00_665);
						BgL_auxz00_12197 = BGL_OBJECT_WIDENING(BgL_tmpz00_12198);
					}
					BgL_auxz00_12196 = ((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_12197);
				}
				return
					(((BgL_funcallzf2cinfozf2_bglt) COBJECT(BgL_auxz00_12196))->
					BgL_vazd2approxzd2);
			}
		}

	}



/* &funcall/Cinfo-va-approx */
	BgL_approxz00_bglt
		BGl_z62funcallzf2Cinfozd2vazd2approxz90zzcfa_approxz00(obj_t
		BgL_envz00_8038, obj_t BgL_oz00_8039)
	{
		{	/* Cfa/cinfo.sch 1057 */
			return
				BGl_funcallzf2Cinfozd2vazd2approxzf2zzcfa_approxz00(
				((BgL_funcallz00_bglt) BgL_oz00_8039));
		}

	}



/* funcall/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_funcallzf2Cinfozd2approxz20zzcfa_approxz00(BgL_funcallz00_bglt
		BgL_oz00_668)
	{
		{	/* Cfa/cinfo.sch 1059 */
			{
				BgL_funcallzf2cinfozf2_bglt BgL_auxz00_12205;

				{
					obj_t BgL_auxz00_12206;

					{	/* Cfa/cinfo.sch 1059 */
						BgL_objectz00_bglt BgL_tmpz00_12207;

						BgL_tmpz00_12207 = ((BgL_objectz00_bglt) BgL_oz00_668);
						BgL_auxz00_12206 = BGL_OBJECT_WIDENING(BgL_tmpz00_12207);
					}
					BgL_auxz00_12205 = ((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_12206);
				}
				return
					(((BgL_funcallzf2cinfozf2_bglt) COBJECT(BgL_auxz00_12205))->
					BgL_approxz00);
			}
		}

	}



/* &funcall/Cinfo-approx */
	BgL_approxz00_bglt BGl_z62funcallzf2Cinfozd2approxz42zzcfa_approxz00(obj_t
		BgL_envz00_8040, obj_t BgL_oz00_8041)
	{
		{	/* Cfa/cinfo.sch 1059 */
			return
				BGl_funcallzf2Cinfozd2approxz20zzcfa_approxz00(
				((BgL_funcallz00_bglt) BgL_oz00_8041));
		}

	}



/* funcall/Cinfo-functions */
	BGL_EXPORTED_DEF obj_t
		BGl_funcallzf2Cinfozd2functionsz20zzcfa_approxz00(BgL_funcallz00_bglt
		BgL_oz00_671)
	{
		{	/* Cfa/cinfo.sch 1061 */
			return
				(((BgL_funcallz00_bglt) COBJECT(
						((BgL_funcallz00_bglt) BgL_oz00_671)))->BgL_functionsz00);
		}

	}



/* &funcall/Cinfo-functions */
	obj_t BGl_z62funcallzf2Cinfozd2functionsz42zzcfa_approxz00(obj_t
		BgL_envz00_8042, obj_t BgL_oz00_8043)
	{
		{	/* Cfa/cinfo.sch 1061 */
			return
				BGl_funcallzf2Cinfozd2functionsz20zzcfa_approxz00(
				((BgL_funcallz00_bglt) BgL_oz00_8043));
		}

	}



/* funcall/Cinfo-functions-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_funcallzf2Cinfozd2functionszd2setz12ze0zzcfa_approxz00
		(BgL_funcallz00_bglt BgL_oz00_672, obj_t BgL_vz00_673)
	{
		{	/* Cfa/cinfo.sch 1062 */
			return
				((((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_oz00_672)))->BgL_functionsz00) =
				((obj_t) BgL_vz00_673), BUNSPEC);
		}

	}



/* &funcall/Cinfo-functions-set! */
	obj_t BGl_z62funcallzf2Cinfozd2functionszd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8044, obj_t BgL_oz00_8045, obj_t BgL_vz00_8046)
	{
		{	/* Cfa/cinfo.sch 1062 */
			return
				BGl_funcallzf2Cinfozd2functionszd2setz12ze0zzcfa_approxz00(
				((BgL_funcallz00_bglt) BgL_oz00_8045), BgL_vz00_8046);
		}

	}



/* funcall/Cinfo-strength */
	BGL_EXPORTED_DEF obj_t
		BGl_funcallzf2Cinfozd2strengthz20zzcfa_approxz00(BgL_funcallz00_bglt
		BgL_oz00_674)
	{
		{	/* Cfa/cinfo.sch 1063 */
			return
				(((BgL_funcallz00_bglt) COBJECT(
						((BgL_funcallz00_bglt) BgL_oz00_674)))->BgL_strengthz00);
		}

	}



/* &funcall/Cinfo-strength */
	obj_t BGl_z62funcallzf2Cinfozd2strengthz42zzcfa_approxz00(obj_t
		BgL_envz00_8047, obj_t BgL_oz00_8048)
	{
		{	/* Cfa/cinfo.sch 1063 */
			return
				BGl_funcallzf2Cinfozd2strengthz20zzcfa_approxz00(
				((BgL_funcallz00_bglt) BgL_oz00_8048));
		}

	}



/* funcall/Cinfo-strength-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_funcallzf2Cinfozd2strengthzd2setz12ze0zzcfa_approxz00
		(BgL_funcallz00_bglt BgL_oz00_675, obj_t BgL_vz00_676)
	{
		{	/* Cfa/cinfo.sch 1064 */
			return
				((((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_oz00_675)))->BgL_strengthz00) =
				((obj_t) BgL_vz00_676), BUNSPEC);
		}

	}



/* &funcall/Cinfo-strength-set! */
	obj_t BGl_z62funcallzf2Cinfozd2strengthzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8049, obj_t BgL_oz00_8050, obj_t BgL_vz00_8051)
	{
		{	/* Cfa/cinfo.sch 1064 */
			return
				BGl_funcallzf2Cinfozd2strengthzd2setz12ze0zzcfa_approxz00(
				((BgL_funcallz00_bglt) BgL_oz00_8050), BgL_vz00_8051);
		}

	}



/* funcall/Cinfo-args */
	BGL_EXPORTED_DEF obj_t
		BGl_funcallzf2Cinfozd2argsz20zzcfa_approxz00(BgL_funcallz00_bglt
		BgL_oz00_677)
	{
		{	/* Cfa/cinfo.sch 1065 */
			return
				(((BgL_funcallz00_bglt) COBJECT(
						((BgL_funcallz00_bglt) BgL_oz00_677)))->BgL_argsz00);
		}

	}



/* &funcall/Cinfo-args */
	obj_t BGl_z62funcallzf2Cinfozd2argsz42zzcfa_approxz00(obj_t BgL_envz00_8052,
		obj_t BgL_oz00_8053)
	{
		{	/* Cfa/cinfo.sch 1065 */
			return
				BGl_funcallzf2Cinfozd2argsz20zzcfa_approxz00(
				((BgL_funcallz00_bglt) BgL_oz00_8053));
		}

	}



/* funcall/Cinfo-args-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_funcallzf2Cinfozd2argszd2setz12ze0zzcfa_approxz00(BgL_funcallz00_bglt
		BgL_oz00_678, obj_t BgL_vz00_679)
	{
		{	/* Cfa/cinfo.sch 1066 */
			return
				((((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_oz00_678)))->BgL_argsz00) =
				((obj_t) BgL_vz00_679), BUNSPEC);
		}

	}



/* &funcall/Cinfo-args-set! */
	obj_t BGl_z62funcallzf2Cinfozd2argszd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8054, obj_t BgL_oz00_8055, obj_t BgL_vz00_8056)
	{
		{	/* Cfa/cinfo.sch 1066 */
			return
				BGl_funcallzf2Cinfozd2argszd2setz12ze0zzcfa_approxz00(
				((BgL_funcallz00_bglt) BgL_oz00_8055), BgL_vz00_8056);
		}

	}



/* funcall/Cinfo-fun */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_funcallzf2Cinfozd2funz20zzcfa_approxz00(BgL_funcallz00_bglt
		BgL_oz00_680)
	{
		{	/* Cfa/cinfo.sch 1067 */
			return
				(((BgL_funcallz00_bglt) COBJECT(
						((BgL_funcallz00_bglt) BgL_oz00_680)))->BgL_funz00);
		}

	}



/* &funcall/Cinfo-fun */
	BgL_nodez00_bglt BGl_z62funcallzf2Cinfozd2funz42zzcfa_approxz00(obj_t
		BgL_envz00_8057, obj_t BgL_oz00_8058)
	{
		{	/* Cfa/cinfo.sch 1067 */
			return
				BGl_funcallzf2Cinfozd2funz20zzcfa_approxz00(
				((BgL_funcallz00_bglt) BgL_oz00_8058));
		}

	}



/* funcall/Cinfo-fun-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_funcallzf2Cinfozd2funzd2setz12ze0zzcfa_approxz00(BgL_funcallz00_bglt
		BgL_oz00_681, BgL_nodez00_bglt BgL_vz00_682)
	{
		{	/* Cfa/cinfo.sch 1068 */
			return
				((((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_oz00_681)))->BgL_funz00) =
				((BgL_nodez00_bglt) BgL_vz00_682), BUNSPEC);
		}

	}



/* &funcall/Cinfo-fun-set! */
	obj_t BGl_z62funcallzf2Cinfozd2funzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8059, obj_t BgL_oz00_8060, obj_t BgL_vz00_8061)
	{
		{	/* Cfa/cinfo.sch 1068 */
			return
				BGl_funcallzf2Cinfozd2funzd2setz12ze0zzcfa_approxz00(
				((BgL_funcallz00_bglt) BgL_oz00_8060),
				((BgL_nodez00_bglt) BgL_vz00_8061));
		}

	}



/* funcall/Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_funcallzf2Cinfozd2typez20zzcfa_approxz00(BgL_funcallz00_bglt
		BgL_oz00_683)
	{
		{	/* Cfa/cinfo.sch 1069 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_683)))->BgL_typez00);
		}

	}



/* &funcall/Cinfo-type */
	BgL_typez00_bglt BGl_z62funcallzf2Cinfozd2typez42zzcfa_approxz00(obj_t
		BgL_envz00_8062, obj_t BgL_oz00_8063)
	{
		{	/* Cfa/cinfo.sch 1069 */
			return
				BGl_funcallzf2Cinfozd2typez20zzcfa_approxz00(
				((BgL_funcallz00_bglt) BgL_oz00_8063));
		}

	}



/* funcall/Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_funcallzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00(BgL_funcallz00_bglt
		BgL_oz00_684, BgL_typez00_bglt BgL_vz00_685)
	{
		{	/* Cfa/cinfo.sch 1070 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_684)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_685), BUNSPEC);
		}

	}



/* &funcall/Cinfo-type-set! */
	obj_t BGl_z62funcallzf2Cinfozd2typezd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8064, obj_t BgL_oz00_8065, obj_t BgL_vz00_8066)
	{
		{	/* Cfa/cinfo.sch 1070 */
			return
				BGl_funcallzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00(
				((BgL_funcallz00_bglt) BgL_oz00_8065),
				((BgL_typez00_bglt) BgL_vz00_8066));
		}

	}



/* funcall/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_funcallzf2Cinfozd2locz20zzcfa_approxz00(BgL_funcallz00_bglt
		BgL_oz00_686)
	{
		{	/* Cfa/cinfo.sch 1071 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_686)))->BgL_locz00);
		}

	}



/* &funcall/Cinfo-loc */
	obj_t BGl_z62funcallzf2Cinfozd2locz42zzcfa_approxz00(obj_t BgL_envz00_8067,
		obj_t BgL_oz00_8068)
	{
		{	/* Cfa/cinfo.sch 1071 */
			return
				BGl_funcallzf2Cinfozd2locz20zzcfa_approxz00(
				((BgL_funcallz00_bglt) BgL_oz00_8068));
		}

	}



/* make-setq/Cinfo */
	BGL_EXPORTED_DEF BgL_setqz00_bglt
		BGl_makezd2setqzf2Cinfoz20zzcfa_approxz00(obj_t BgL_loc1637z00_689,
		BgL_typez00_bglt BgL_type1638z00_690, BgL_varz00_bglt BgL_var1639z00_691,
		BgL_nodez00_bglt BgL_value1640z00_692,
		BgL_approxz00_bglt BgL_approx1642z00_693)
	{
		{	/* Cfa/cinfo.sch 1075 */
			{	/* Cfa/cinfo.sch 1075 */
				BgL_setqz00_bglt BgL_new1251z00_9032;

				{	/* Cfa/cinfo.sch 1075 */
					BgL_setqz00_bglt BgL_tmp1248z00_9033;
					BgL_setqzf2cinfozf2_bglt BgL_wide1249z00_9034;

					{
						BgL_setqz00_bglt BgL_auxz00_12260;

						{	/* Cfa/cinfo.sch 1075 */
							BgL_setqz00_bglt BgL_new1247z00_9035;

							BgL_new1247z00_9035 =
								((BgL_setqz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_setqz00_bgl))));
							{	/* Cfa/cinfo.sch 1075 */
								long BgL_arg1735z00_9036;

								BgL_arg1735z00_9036 = BGL_CLASS_NUM(BGl_setqz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1247z00_9035),
									BgL_arg1735z00_9036);
							}
							{	/* Cfa/cinfo.sch 1075 */
								BgL_objectz00_bglt BgL_tmpz00_12265;

								BgL_tmpz00_12265 = ((BgL_objectz00_bglt) BgL_new1247z00_9035);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12265, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1247z00_9035);
							BgL_auxz00_12260 = BgL_new1247z00_9035;
						}
						BgL_tmp1248z00_9033 = ((BgL_setqz00_bglt) BgL_auxz00_12260);
					}
					BgL_wide1249z00_9034 =
						((BgL_setqzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_setqzf2cinfozf2_bgl))));
					{	/* Cfa/cinfo.sch 1075 */
						obj_t BgL_auxz00_12273;
						BgL_objectz00_bglt BgL_tmpz00_12271;

						BgL_auxz00_12273 = ((obj_t) BgL_wide1249z00_9034);
						BgL_tmpz00_12271 = ((BgL_objectz00_bglt) BgL_tmp1248z00_9033);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12271, BgL_auxz00_12273);
					}
					((BgL_objectz00_bglt) BgL_tmp1248z00_9033);
					{	/* Cfa/cinfo.sch 1075 */
						long BgL_arg1734z00_9037;

						BgL_arg1734z00_9037 =
							BGL_CLASS_NUM(BGl_setqzf2Cinfozf2zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1248z00_9033), BgL_arg1734z00_9037);
					}
					BgL_new1251z00_9032 = ((BgL_setqz00_bglt) BgL_tmp1248z00_9033);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1251z00_9032)))->BgL_locz00) =
					((obj_t) BgL_loc1637z00_689), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1251z00_9032)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1638z00_690), BUNSPEC);
				((((BgL_setqz00_bglt) COBJECT(((BgL_setqz00_bglt)
									BgL_new1251z00_9032)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_var1639z00_691), BUNSPEC);
				((((BgL_setqz00_bglt) COBJECT(((BgL_setqz00_bglt)
									BgL_new1251z00_9032)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_value1640z00_692), BUNSPEC);
				{
					BgL_setqzf2cinfozf2_bglt BgL_auxz00_12289;

					{
						obj_t BgL_auxz00_12290;

						{	/* Cfa/cinfo.sch 1075 */
							BgL_objectz00_bglt BgL_tmpz00_12291;

							BgL_tmpz00_12291 = ((BgL_objectz00_bglt) BgL_new1251z00_9032);
							BgL_auxz00_12290 = BGL_OBJECT_WIDENING(BgL_tmpz00_12291);
						}
						BgL_auxz00_12289 = ((BgL_setqzf2cinfozf2_bglt) BgL_auxz00_12290);
					}
					((((BgL_setqzf2cinfozf2_bglt) COBJECT(BgL_auxz00_12289))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1642z00_693), BUNSPEC);
				}
				return BgL_new1251z00_9032;
			}
		}

	}



/* &make-setq/Cinfo */
	BgL_setqz00_bglt BGl_z62makezd2setqzf2Cinfoz42zzcfa_approxz00(obj_t
		BgL_envz00_8069, obj_t BgL_loc1637z00_8070, obj_t BgL_type1638z00_8071,
		obj_t BgL_var1639z00_8072, obj_t BgL_value1640z00_8073,
		obj_t BgL_approx1642z00_8074)
	{
		{	/* Cfa/cinfo.sch 1075 */
			return
				BGl_makezd2setqzf2Cinfoz20zzcfa_approxz00(BgL_loc1637z00_8070,
				((BgL_typez00_bglt) BgL_type1638z00_8071),
				((BgL_varz00_bglt) BgL_var1639z00_8072),
				((BgL_nodez00_bglt) BgL_value1640z00_8073),
				((BgL_approxz00_bglt) BgL_approx1642z00_8074));
		}

	}



/* setq/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_setqzf2Cinfozf3z01zzcfa_approxz00(obj_t
		BgL_objz00_694)
	{
		{	/* Cfa/cinfo.sch 1076 */
			{	/* Cfa/cinfo.sch 1076 */
				obj_t BgL_classz00_9038;

				BgL_classz00_9038 = BGl_setqzf2Cinfozf2zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_694))
					{	/* Cfa/cinfo.sch 1076 */
						BgL_objectz00_bglt BgL_arg1807z00_9039;

						BgL_arg1807z00_9039 = (BgL_objectz00_bglt) (BgL_objz00_694);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 1076 */
								long BgL_idxz00_9040;

								BgL_idxz00_9040 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9039);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9040 + 3L)) == BgL_classz00_9038);
							}
						else
							{	/* Cfa/cinfo.sch 1076 */
								bool_t BgL_res2185z00_9043;

								{	/* Cfa/cinfo.sch 1076 */
									obj_t BgL_oclassz00_9044;

									{	/* Cfa/cinfo.sch 1076 */
										obj_t BgL_arg1815z00_9045;
										long BgL_arg1816z00_9046;

										BgL_arg1815z00_9045 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 1076 */
											long BgL_arg1817z00_9047;

											BgL_arg1817z00_9047 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9039);
											BgL_arg1816z00_9046 = (BgL_arg1817z00_9047 - OBJECT_TYPE);
										}
										BgL_oclassz00_9044 =
											VECTOR_REF(BgL_arg1815z00_9045, BgL_arg1816z00_9046);
									}
									{	/* Cfa/cinfo.sch 1076 */
										bool_t BgL__ortest_1115z00_9048;

										BgL__ortest_1115z00_9048 =
											(BgL_classz00_9038 == BgL_oclassz00_9044);
										if (BgL__ortest_1115z00_9048)
											{	/* Cfa/cinfo.sch 1076 */
												BgL_res2185z00_9043 = BgL__ortest_1115z00_9048;
											}
										else
											{	/* Cfa/cinfo.sch 1076 */
												long BgL_odepthz00_9049;

												{	/* Cfa/cinfo.sch 1076 */
													obj_t BgL_arg1804z00_9050;

													BgL_arg1804z00_9050 = (BgL_oclassz00_9044);
													BgL_odepthz00_9049 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9050);
												}
												if ((3L < BgL_odepthz00_9049))
													{	/* Cfa/cinfo.sch 1076 */
														obj_t BgL_arg1802z00_9051;

														{	/* Cfa/cinfo.sch 1076 */
															obj_t BgL_arg1803z00_9052;

															BgL_arg1803z00_9052 = (BgL_oclassz00_9044);
															BgL_arg1802z00_9051 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9052,
																3L);
														}
														BgL_res2185z00_9043 =
															(BgL_arg1802z00_9051 == BgL_classz00_9038);
													}
												else
													{	/* Cfa/cinfo.sch 1076 */
														BgL_res2185z00_9043 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2185z00_9043;
							}
					}
				else
					{	/* Cfa/cinfo.sch 1076 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &setq/Cinfo? */
	obj_t BGl_z62setqzf2Cinfozf3z63zzcfa_approxz00(obj_t BgL_envz00_8075,
		obj_t BgL_objz00_8076)
	{
		{	/* Cfa/cinfo.sch 1076 */
			return BBOOL(BGl_setqzf2Cinfozf3z01zzcfa_approxz00(BgL_objz00_8076));
		}

	}



/* setq/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_setqz00_bglt
		BGl_setqzf2Cinfozd2nilz20zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 1077 */
			{	/* Cfa/cinfo.sch 1077 */
				obj_t BgL_classz00_5695;

				BgL_classz00_5695 = BGl_setqzf2Cinfozf2zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 1077 */
					obj_t BgL__ortest_1117z00_5696;

					BgL__ortest_1117z00_5696 = BGL_CLASS_NIL(BgL_classz00_5695);
					if (CBOOL(BgL__ortest_1117z00_5696))
						{	/* Cfa/cinfo.sch 1077 */
							return ((BgL_setqz00_bglt) BgL__ortest_1117z00_5696);
						}
					else
						{	/* Cfa/cinfo.sch 1077 */
							return
								((BgL_setqz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5695));
						}
				}
			}
		}

	}



/* &setq/Cinfo-nil */
	BgL_setqz00_bglt BGl_z62setqzf2Cinfozd2nilz42zzcfa_approxz00(obj_t
		BgL_envz00_8077)
	{
		{	/* Cfa/cinfo.sch 1077 */
			return BGl_setqzf2Cinfozd2nilz20zzcfa_approxz00();
		}

	}



/* setq/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_setqzf2Cinfozd2approxz20zzcfa_approxz00(BgL_setqz00_bglt BgL_oz00_695)
	{
		{	/* Cfa/cinfo.sch 1078 */
			{
				BgL_setqzf2cinfozf2_bglt BgL_auxz00_12332;

				{
					obj_t BgL_auxz00_12333;

					{	/* Cfa/cinfo.sch 1078 */
						BgL_objectz00_bglt BgL_tmpz00_12334;

						BgL_tmpz00_12334 = ((BgL_objectz00_bglt) BgL_oz00_695);
						BgL_auxz00_12333 = BGL_OBJECT_WIDENING(BgL_tmpz00_12334);
					}
					BgL_auxz00_12332 = ((BgL_setqzf2cinfozf2_bglt) BgL_auxz00_12333);
				}
				return
					(((BgL_setqzf2cinfozf2_bglt) COBJECT(BgL_auxz00_12332))->
					BgL_approxz00);
			}
		}

	}



/* &setq/Cinfo-approx */
	BgL_approxz00_bglt BGl_z62setqzf2Cinfozd2approxz42zzcfa_approxz00(obj_t
		BgL_envz00_8078, obj_t BgL_oz00_8079)
	{
		{	/* Cfa/cinfo.sch 1078 */
			return
				BGl_setqzf2Cinfozd2approxz20zzcfa_approxz00(
				((BgL_setqz00_bglt) BgL_oz00_8079));
		}

	}



/* setq/Cinfo-value */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_setqzf2Cinfozd2valuez20zzcfa_approxz00(BgL_setqz00_bglt BgL_oz00_698)
	{
		{	/* Cfa/cinfo.sch 1080 */
			return
				(((BgL_setqz00_bglt) COBJECT(
						((BgL_setqz00_bglt) BgL_oz00_698)))->BgL_valuez00);
		}

	}



/* &setq/Cinfo-value */
	BgL_nodez00_bglt BGl_z62setqzf2Cinfozd2valuez42zzcfa_approxz00(obj_t
		BgL_envz00_8080, obj_t BgL_oz00_8081)
	{
		{	/* Cfa/cinfo.sch 1080 */
			return
				BGl_setqzf2Cinfozd2valuez20zzcfa_approxz00(
				((BgL_setqz00_bglt) BgL_oz00_8081));
		}

	}



/* setq/Cinfo-value-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_setqzf2Cinfozd2valuezd2setz12ze0zzcfa_approxz00(BgL_setqz00_bglt
		BgL_oz00_699, BgL_nodez00_bglt BgL_vz00_700)
	{
		{	/* Cfa/cinfo.sch 1081 */
			return
				((((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_oz00_699)))->BgL_valuez00) =
				((BgL_nodez00_bglt) BgL_vz00_700), BUNSPEC);
		}

	}



/* &setq/Cinfo-value-set! */
	obj_t BGl_z62setqzf2Cinfozd2valuezd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8082, obj_t BgL_oz00_8083, obj_t BgL_vz00_8084)
	{
		{	/* Cfa/cinfo.sch 1081 */
			return
				BGl_setqzf2Cinfozd2valuezd2setz12ze0zzcfa_approxz00(
				((BgL_setqz00_bglt) BgL_oz00_8083), ((BgL_nodez00_bglt) BgL_vz00_8084));
		}

	}



/* setq/Cinfo-var */
	BGL_EXPORTED_DEF BgL_varz00_bglt
		BGl_setqzf2Cinfozd2varz20zzcfa_approxz00(BgL_setqz00_bglt BgL_oz00_701)
	{
		{	/* Cfa/cinfo.sch 1082 */
			return
				(((BgL_setqz00_bglt) COBJECT(
						((BgL_setqz00_bglt) BgL_oz00_701)))->BgL_varz00);
		}

	}



/* &setq/Cinfo-var */
	BgL_varz00_bglt BGl_z62setqzf2Cinfozd2varz42zzcfa_approxz00(obj_t
		BgL_envz00_8085, obj_t BgL_oz00_8086)
	{
		{	/* Cfa/cinfo.sch 1082 */
			return
				BGl_setqzf2Cinfozd2varz20zzcfa_approxz00(
				((BgL_setqz00_bglt) BgL_oz00_8086));
		}

	}



/* setq/Cinfo-var-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_setqzf2Cinfozd2varzd2setz12ze0zzcfa_approxz00(BgL_setqz00_bglt
		BgL_oz00_702, BgL_varz00_bglt BgL_vz00_703)
	{
		{	/* Cfa/cinfo.sch 1083 */
			return
				((((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_oz00_702)))->BgL_varz00) =
				((BgL_varz00_bglt) BgL_vz00_703), BUNSPEC);
		}

	}



/* &setq/Cinfo-var-set! */
	obj_t BGl_z62setqzf2Cinfozd2varzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8087, obj_t BgL_oz00_8088, obj_t BgL_vz00_8089)
	{
		{	/* Cfa/cinfo.sch 1083 */
			return
				BGl_setqzf2Cinfozd2varzd2setz12ze0zzcfa_approxz00(
				((BgL_setqz00_bglt) BgL_oz00_8088), ((BgL_varz00_bglt) BgL_vz00_8089));
		}

	}



/* setq/Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_setqzf2Cinfozd2typez20zzcfa_approxz00(BgL_setqz00_bglt BgL_oz00_704)
	{
		{	/* Cfa/cinfo.sch 1084 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_704)))->BgL_typez00);
		}

	}



/* &setq/Cinfo-type */
	BgL_typez00_bglt BGl_z62setqzf2Cinfozd2typez42zzcfa_approxz00(obj_t
		BgL_envz00_8090, obj_t BgL_oz00_8091)
	{
		{	/* Cfa/cinfo.sch 1084 */
			return
				BGl_setqzf2Cinfozd2typez20zzcfa_approxz00(
				((BgL_setqz00_bglt) BgL_oz00_8091));
		}

	}



/* setq/Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_setqzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00(BgL_setqz00_bglt
		BgL_oz00_705, BgL_typez00_bglt BgL_vz00_706)
	{
		{	/* Cfa/cinfo.sch 1085 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_705)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_706), BUNSPEC);
		}

	}



/* &setq/Cinfo-type-set! */
	obj_t BGl_z62setqzf2Cinfozd2typezd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8092, obj_t BgL_oz00_8093, obj_t BgL_vz00_8094)
	{
		{	/* Cfa/cinfo.sch 1085 */
			return
				BGl_setqzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00(
				((BgL_setqz00_bglt) BgL_oz00_8093), ((BgL_typez00_bglt) BgL_vz00_8094));
		}

	}



/* setq/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_setqzf2Cinfozd2locz20zzcfa_approxz00(BgL_setqz00_bglt BgL_oz00_707)
	{
		{	/* Cfa/cinfo.sch 1086 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_707)))->BgL_locz00);
		}

	}



/* &setq/Cinfo-loc */
	obj_t BGl_z62setqzf2Cinfozd2locz42zzcfa_approxz00(obj_t BgL_envz00_8095,
		obj_t BgL_oz00_8096)
	{
		{	/* Cfa/cinfo.sch 1086 */
			return
				BGl_setqzf2Cinfozd2locz20zzcfa_approxz00(
				((BgL_setqz00_bglt) BgL_oz00_8096));
		}

	}



/* make-conditional/Cinfo */
	BGL_EXPORTED_DEF BgL_conditionalz00_bglt
		BGl_makezd2conditionalzf2Cinfoz20zzcfa_approxz00(obj_t BgL_loc1628z00_710,
		BgL_typez00_bglt BgL_type1629z00_711, obj_t BgL_sidezd2effect1630zd2_712,
		obj_t BgL_key1631z00_713, BgL_nodez00_bglt BgL_test1632z00_714,
		BgL_nodez00_bglt BgL_true1633z00_715, BgL_nodez00_bglt BgL_false1634z00_716,
		BgL_approxz00_bglt BgL_approx1635z00_717)
	{
		{	/* Cfa/cinfo.sch 1090 */
			{	/* Cfa/cinfo.sch 1090 */
				BgL_conditionalz00_bglt BgL_new1255z00_9053;

				{	/* Cfa/cinfo.sch 1090 */
					BgL_conditionalz00_bglt BgL_tmp1253z00_9054;
					BgL_conditionalzf2cinfozf2_bglt BgL_wide1254z00_9055;

					{
						BgL_conditionalz00_bglt BgL_auxz00_12372;

						{	/* Cfa/cinfo.sch 1090 */
							BgL_conditionalz00_bglt BgL_new1252z00_9056;

							BgL_new1252z00_9056 =
								((BgL_conditionalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_conditionalz00_bgl))));
							{	/* Cfa/cinfo.sch 1090 */
								long BgL_arg1737z00_9057;

								BgL_arg1737z00_9057 =
									BGL_CLASS_NUM(BGl_conditionalz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1252z00_9056),
									BgL_arg1737z00_9057);
							}
							{	/* Cfa/cinfo.sch 1090 */
								BgL_objectz00_bglt BgL_tmpz00_12377;

								BgL_tmpz00_12377 = ((BgL_objectz00_bglt) BgL_new1252z00_9056);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12377, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1252z00_9056);
							BgL_auxz00_12372 = BgL_new1252z00_9056;
						}
						BgL_tmp1253z00_9054 = ((BgL_conditionalz00_bglt) BgL_auxz00_12372);
					}
					BgL_wide1254z00_9055 =
						((BgL_conditionalzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_conditionalzf2cinfozf2_bgl))));
					{	/* Cfa/cinfo.sch 1090 */
						obj_t BgL_auxz00_12385;
						BgL_objectz00_bglt BgL_tmpz00_12383;

						BgL_auxz00_12385 = ((obj_t) BgL_wide1254z00_9055);
						BgL_tmpz00_12383 = ((BgL_objectz00_bglt) BgL_tmp1253z00_9054);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12383, BgL_auxz00_12385);
					}
					((BgL_objectz00_bglt) BgL_tmp1253z00_9054);
					{	/* Cfa/cinfo.sch 1090 */
						long BgL_arg1736z00_9058;

						BgL_arg1736z00_9058 =
							BGL_CLASS_NUM(BGl_conditionalzf2Cinfozf2zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1253z00_9054), BgL_arg1736z00_9058);
					}
					BgL_new1255z00_9053 = ((BgL_conditionalz00_bglt) BgL_tmp1253z00_9054);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1255z00_9053)))->BgL_locz00) =
					((obj_t) BgL_loc1628z00_710), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1255z00_9053)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1629z00_711), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1255z00_9053)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1630zd2_712), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1255z00_9053)))->BgL_keyz00) =
					((obj_t) BgL_key1631z00_713), BUNSPEC);
				((((BgL_conditionalz00_bglt) COBJECT(((BgL_conditionalz00_bglt)
									BgL_new1255z00_9053)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_test1632z00_714), BUNSPEC);
				((((BgL_conditionalz00_bglt) COBJECT(((BgL_conditionalz00_bglt)
									BgL_new1255z00_9053)))->BgL_truez00) =
					((BgL_nodez00_bglt) BgL_true1633z00_715), BUNSPEC);
				((((BgL_conditionalz00_bglt) COBJECT(((BgL_conditionalz00_bglt)
									BgL_new1255z00_9053)))->BgL_falsez00) =
					((BgL_nodez00_bglt) BgL_false1634z00_716), BUNSPEC);
				{
					BgL_conditionalzf2cinfozf2_bglt BgL_auxz00_12407;

					{
						obj_t BgL_auxz00_12408;

						{	/* Cfa/cinfo.sch 1090 */
							BgL_objectz00_bglt BgL_tmpz00_12409;

							BgL_tmpz00_12409 = ((BgL_objectz00_bglt) BgL_new1255z00_9053);
							BgL_auxz00_12408 = BGL_OBJECT_WIDENING(BgL_tmpz00_12409);
						}
						BgL_auxz00_12407 =
							((BgL_conditionalzf2cinfozf2_bglt) BgL_auxz00_12408);
					}
					((((BgL_conditionalzf2cinfozf2_bglt) COBJECT(BgL_auxz00_12407))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1635z00_717), BUNSPEC);
				}
				return BgL_new1255z00_9053;
			}
		}

	}



/* &make-conditional/Cinfo */
	BgL_conditionalz00_bglt
		BGl_z62makezd2conditionalzf2Cinfoz42zzcfa_approxz00(obj_t BgL_envz00_8097,
		obj_t BgL_loc1628z00_8098, obj_t BgL_type1629z00_8099,
		obj_t BgL_sidezd2effect1630zd2_8100, obj_t BgL_key1631z00_8101,
		obj_t BgL_test1632z00_8102, obj_t BgL_true1633z00_8103,
		obj_t BgL_false1634z00_8104, obj_t BgL_approx1635z00_8105)
	{
		{	/* Cfa/cinfo.sch 1090 */
			return
				BGl_makezd2conditionalzf2Cinfoz20zzcfa_approxz00(BgL_loc1628z00_8098,
				((BgL_typez00_bglt) BgL_type1629z00_8099),
				BgL_sidezd2effect1630zd2_8100, BgL_key1631z00_8101,
				((BgL_nodez00_bglt) BgL_test1632z00_8102),
				((BgL_nodez00_bglt) BgL_true1633z00_8103),
				((BgL_nodez00_bglt) BgL_false1634z00_8104),
				((BgL_approxz00_bglt) BgL_approx1635z00_8105));
		}

	}



/* conditional/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_conditionalzf2Cinfozf3z01zzcfa_approxz00(obj_t
		BgL_objz00_718)
	{
		{	/* Cfa/cinfo.sch 1091 */
			{	/* Cfa/cinfo.sch 1091 */
				obj_t BgL_classz00_9059;

				BgL_classz00_9059 = BGl_conditionalzf2Cinfozf2zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_718))
					{	/* Cfa/cinfo.sch 1091 */
						BgL_objectz00_bglt BgL_arg1807z00_9060;

						BgL_arg1807z00_9060 = (BgL_objectz00_bglt) (BgL_objz00_718);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 1091 */
								long BgL_idxz00_9061;

								BgL_idxz00_9061 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9060);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9061 + 4L)) == BgL_classz00_9059);
							}
						else
							{	/* Cfa/cinfo.sch 1091 */
								bool_t BgL_res2186z00_9064;

								{	/* Cfa/cinfo.sch 1091 */
									obj_t BgL_oclassz00_9065;

									{	/* Cfa/cinfo.sch 1091 */
										obj_t BgL_arg1815z00_9066;
										long BgL_arg1816z00_9067;

										BgL_arg1815z00_9066 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 1091 */
											long BgL_arg1817z00_9068;

											BgL_arg1817z00_9068 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9060);
											BgL_arg1816z00_9067 = (BgL_arg1817z00_9068 - OBJECT_TYPE);
										}
										BgL_oclassz00_9065 =
											VECTOR_REF(BgL_arg1815z00_9066, BgL_arg1816z00_9067);
									}
									{	/* Cfa/cinfo.sch 1091 */
										bool_t BgL__ortest_1115z00_9069;

										BgL__ortest_1115z00_9069 =
											(BgL_classz00_9059 == BgL_oclassz00_9065);
										if (BgL__ortest_1115z00_9069)
											{	/* Cfa/cinfo.sch 1091 */
												BgL_res2186z00_9064 = BgL__ortest_1115z00_9069;
											}
										else
											{	/* Cfa/cinfo.sch 1091 */
												long BgL_odepthz00_9070;

												{	/* Cfa/cinfo.sch 1091 */
													obj_t BgL_arg1804z00_9071;

													BgL_arg1804z00_9071 = (BgL_oclassz00_9065);
													BgL_odepthz00_9070 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9071);
												}
												if ((4L < BgL_odepthz00_9070))
													{	/* Cfa/cinfo.sch 1091 */
														obj_t BgL_arg1802z00_9072;

														{	/* Cfa/cinfo.sch 1091 */
															obj_t BgL_arg1803z00_9073;

															BgL_arg1803z00_9073 = (BgL_oclassz00_9065);
															BgL_arg1802z00_9072 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9073,
																4L);
														}
														BgL_res2186z00_9064 =
															(BgL_arg1802z00_9072 == BgL_classz00_9059);
													}
												else
													{	/* Cfa/cinfo.sch 1091 */
														BgL_res2186z00_9064 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2186z00_9064;
							}
					}
				else
					{	/* Cfa/cinfo.sch 1091 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &conditional/Cinfo? */
	obj_t BGl_z62conditionalzf2Cinfozf3z63zzcfa_approxz00(obj_t BgL_envz00_8106,
		obj_t BgL_objz00_8107)
	{
		{	/* Cfa/cinfo.sch 1091 */
			return
				BBOOL(BGl_conditionalzf2Cinfozf3z01zzcfa_approxz00(BgL_objz00_8107));
		}

	}



/* conditional/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_conditionalz00_bglt
		BGl_conditionalzf2Cinfozd2nilz20zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 1092 */
			{	/* Cfa/cinfo.sch 1092 */
				obj_t BgL_classz00_5747;

				BgL_classz00_5747 = BGl_conditionalzf2Cinfozf2zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 1092 */
					obj_t BgL__ortest_1117z00_5748;

					BgL__ortest_1117z00_5748 = BGL_CLASS_NIL(BgL_classz00_5747);
					if (CBOOL(BgL__ortest_1117z00_5748))
						{	/* Cfa/cinfo.sch 1092 */
							return ((BgL_conditionalz00_bglt) BgL__ortest_1117z00_5748);
						}
					else
						{	/* Cfa/cinfo.sch 1092 */
							return
								((BgL_conditionalz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5747));
						}
				}
			}
		}

	}



/* &conditional/Cinfo-nil */
	BgL_conditionalz00_bglt
		BGl_z62conditionalzf2Cinfozd2nilz42zzcfa_approxz00(obj_t BgL_envz00_8108)
	{
		{	/* Cfa/cinfo.sch 1092 */
			return BGl_conditionalzf2Cinfozd2nilz20zzcfa_approxz00();
		}

	}



/* conditional/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_conditionalzf2Cinfozd2approxz20zzcfa_approxz00(BgL_conditionalz00_bglt
		BgL_oz00_719)
	{
		{	/* Cfa/cinfo.sch 1093 */
			{
				BgL_conditionalzf2cinfozf2_bglt BgL_auxz00_12451;

				{
					obj_t BgL_auxz00_12452;

					{	/* Cfa/cinfo.sch 1093 */
						BgL_objectz00_bglt BgL_tmpz00_12453;

						BgL_tmpz00_12453 = ((BgL_objectz00_bglt) BgL_oz00_719);
						BgL_auxz00_12452 = BGL_OBJECT_WIDENING(BgL_tmpz00_12453);
					}
					BgL_auxz00_12451 =
						((BgL_conditionalzf2cinfozf2_bglt) BgL_auxz00_12452);
				}
				return
					(((BgL_conditionalzf2cinfozf2_bglt) COBJECT(BgL_auxz00_12451))->
					BgL_approxz00);
			}
		}

	}



/* &conditional/Cinfo-approx */
	BgL_approxz00_bglt BGl_z62conditionalzf2Cinfozd2approxz42zzcfa_approxz00(obj_t
		BgL_envz00_8109, obj_t BgL_oz00_8110)
	{
		{	/* Cfa/cinfo.sch 1093 */
			return
				BGl_conditionalzf2Cinfozd2approxz20zzcfa_approxz00(
				((BgL_conditionalz00_bglt) BgL_oz00_8110));
		}

	}



/* conditional/Cinfo-false */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_conditionalzf2Cinfozd2falsez20zzcfa_approxz00(BgL_conditionalz00_bglt
		BgL_oz00_722)
	{
		{	/* Cfa/cinfo.sch 1095 */
			return
				(((BgL_conditionalz00_bglt) COBJECT(
						((BgL_conditionalz00_bglt) BgL_oz00_722)))->BgL_falsez00);
		}

	}



/* &conditional/Cinfo-false */
	BgL_nodez00_bglt BGl_z62conditionalzf2Cinfozd2falsez42zzcfa_approxz00(obj_t
		BgL_envz00_8111, obj_t BgL_oz00_8112)
	{
		{	/* Cfa/cinfo.sch 1095 */
			return
				BGl_conditionalzf2Cinfozd2falsez20zzcfa_approxz00(
				((BgL_conditionalz00_bglt) BgL_oz00_8112));
		}

	}



/* conditional/Cinfo-false-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_conditionalzf2Cinfozd2falsezd2setz12ze0zzcfa_approxz00
		(BgL_conditionalz00_bglt BgL_oz00_723, BgL_nodez00_bglt BgL_vz00_724)
	{
		{	/* Cfa/cinfo.sch 1096 */
			return
				((((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_oz00_723)))->BgL_falsez00) =
				((BgL_nodez00_bglt) BgL_vz00_724), BUNSPEC);
		}

	}



/* &conditional/Cinfo-false-set! */
	obj_t BGl_z62conditionalzf2Cinfozd2falsezd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8113, obj_t BgL_oz00_8114, obj_t BgL_vz00_8115)
	{
		{	/* Cfa/cinfo.sch 1096 */
			return
				BGl_conditionalzf2Cinfozd2falsezd2setz12ze0zzcfa_approxz00(
				((BgL_conditionalz00_bglt) BgL_oz00_8114),
				((BgL_nodez00_bglt) BgL_vz00_8115));
		}

	}



/* conditional/Cinfo-true */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_conditionalzf2Cinfozd2truez20zzcfa_approxz00(BgL_conditionalz00_bglt
		BgL_oz00_725)
	{
		{	/* Cfa/cinfo.sch 1097 */
			return
				(((BgL_conditionalz00_bglt) COBJECT(
						((BgL_conditionalz00_bglt) BgL_oz00_725)))->BgL_truez00);
		}

	}



/* &conditional/Cinfo-true */
	BgL_nodez00_bglt BGl_z62conditionalzf2Cinfozd2truez42zzcfa_approxz00(obj_t
		BgL_envz00_8116, obj_t BgL_oz00_8117)
	{
		{	/* Cfa/cinfo.sch 1097 */
			return
				BGl_conditionalzf2Cinfozd2truez20zzcfa_approxz00(
				((BgL_conditionalz00_bglt) BgL_oz00_8117));
		}

	}



/* conditional/Cinfo-true-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_conditionalzf2Cinfozd2truezd2setz12ze0zzcfa_approxz00
		(BgL_conditionalz00_bglt BgL_oz00_726, BgL_nodez00_bglt BgL_vz00_727)
	{
		{	/* Cfa/cinfo.sch 1098 */
			return
				((((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_oz00_726)))->BgL_truez00) =
				((BgL_nodez00_bglt) BgL_vz00_727), BUNSPEC);
		}

	}



/* &conditional/Cinfo-true-set! */
	obj_t BGl_z62conditionalzf2Cinfozd2truezd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8118, obj_t BgL_oz00_8119, obj_t BgL_vz00_8120)
	{
		{	/* Cfa/cinfo.sch 1098 */
			return
				BGl_conditionalzf2Cinfozd2truezd2setz12ze0zzcfa_approxz00(
				((BgL_conditionalz00_bglt) BgL_oz00_8119),
				((BgL_nodez00_bglt) BgL_vz00_8120));
		}

	}



/* conditional/Cinfo-test */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_conditionalzf2Cinfozd2testz20zzcfa_approxz00(BgL_conditionalz00_bglt
		BgL_oz00_728)
	{
		{	/* Cfa/cinfo.sch 1099 */
			return
				(((BgL_conditionalz00_bglt) COBJECT(
						((BgL_conditionalz00_bglt) BgL_oz00_728)))->BgL_testz00);
		}

	}



/* &conditional/Cinfo-test */
	BgL_nodez00_bglt BGl_z62conditionalzf2Cinfozd2testz42zzcfa_approxz00(obj_t
		BgL_envz00_8121, obj_t BgL_oz00_8122)
	{
		{	/* Cfa/cinfo.sch 1099 */
			return
				BGl_conditionalzf2Cinfozd2testz20zzcfa_approxz00(
				((BgL_conditionalz00_bglt) BgL_oz00_8122));
		}

	}



/* conditional/Cinfo-test-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_conditionalzf2Cinfozd2testzd2setz12ze0zzcfa_approxz00
		(BgL_conditionalz00_bglt BgL_oz00_729, BgL_nodez00_bglt BgL_vz00_730)
	{
		{	/* Cfa/cinfo.sch 1100 */
			return
				((((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_oz00_729)))->BgL_testz00) =
				((BgL_nodez00_bglt) BgL_vz00_730), BUNSPEC);
		}

	}



/* &conditional/Cinfo-test-set! */
	obj_t BGl_z62conditionalzf2Cinfozd2testzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8123, obj_t BgL_oz00_8124, obj_t BgL_vz00_8125)
	{
		{	/* Cfa/cinfo.sch 1100 */
			return
				BGl_conditionalzf2Cinfozd2testzd2setz12ze0zzcfa_approxz00(
				((BgL_conditionalz00_bglt) BgL_oz00_8124),
				((BgL_nodez00_bglt) BgL_vz00_8125));
		}

	}



/* conditional/Cinfo-key */
	BGL_EXPORTED_DEF obj_t
		BGl_conditionalzf2Cinfozd2keyz20zzcfa_approxz00(BgL_conditionalz00_bglt
		BgL_oz00_731)
	{
		{	/* Cfa/cinfo.sch 1101 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_731)))->BgL_keyz00);
		}

	}



/* &conditional/Cinfo-key */
	obj_t BGl_z62conditionalzf2Cinfozd2keyz42zzcfa_approxz00(obj_t
		BgL_envz00_8126, obj_t BgL_oz00_8127)
	{
		{	/* Cfa/cinfo.sch 1101 */
			return
				BGl_conditionalzf2Cinfozd2keyz20zzcfa_approxz00(
				((BgL_conditionalz00_bglt) BgL_oz00_8127));
		}

	}



/* conditional/Cinfo-key-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_conditionalzf2Cinfozd2keyzd2setz12ze0zzcfa_approxz00
		(BgL_conditionalz00_bglt BgL_oz00_732, obj_t BgL_vz00_733)
	{
		{	/* Cfa/cinfo.sch 1102 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_732)))->BgL_keyz00) =
				((obj_t) BgL_vz00_733), BUNSPEC);
		}

	}



/* &conditional/Cinfo-key-set! */
	obj_t BGl_z62conditionalzf2Cinfozd2keyzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8128, obj_t BgL_oz00_8129, obj_t BgL_vz00_8130)
	{
		{	/* Cfa/cinfo.sch 1102 */
			return
				BGl_conditionalzf2Cinfozd2keyzd2setz12ze0zzcfa_approxz00(
				((BgL_conditionalz00_bglt) BgL_oz00_8129), BgL_vz00_8130);
		}

	}



/* conditional/Cinfo-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_conditionalzf2Cinfozd2sidezd2effectzf2zzcfa_approxz00
		(BgL_conditionalz00_bglt BgL_oz00_734)
	{
		{	/* Cfa/cinfo.sch 1103 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_734)))->BgL_sidezd2effectzd2);
		}

	}



/* &conditional/Cinfo-side-effect */
	obj_t BGl_z62conditionalzf2Cinfozd2sidezd2effectz90zzcfa_approxz00(obj_t
		BgL_envz00_8131, obj_t BgL_oz00_8132)
	{
		{	/* Cfa/cinfo.sch 1103 */
			return
				BGl_conditionalzf2Cinfozd2sidezd2effectzf2zzcfa_approxz00(
				((BgL_conditionalz00_bglt) BgL_oz00_8132));
		}

	}



/* conditional/Cinfo-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_conditionalzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_approxz00
		(BgL_conditionalz00_bglt BgL_oz00_735, obj_t BgL_vz00_736)
	{
		{	/* Cfa/cinfo.sch 1104 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_735)))->
					BgL_sidezd2effectzd2) = ((obj_t) BgL_vz00_736), BUNSPEC);
		}

	}



/* &conditional/Cinfo-side-effect-set! */
	obj_t
		BGl_z62conditionalzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_8133, obj_t BgL_oz00_8134, obj_t BgL_vz00_8135)
	{
		{	/* Cfa/cinfo.sch 1104 */
			return
				BGl_conditionalzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_approxz00(
				((BgL_conditionalz00_bglt) BgL_oz00_8134), BgL_vz00_8135);
		}

	}



/* conditional/Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_conditionalzf2Cinfozd2typez20zzcfa_approxz00(BgL_conditionalz00_bglt
		BgL_oz00_737)
	{
		{	/* Cfa/cinfo.sch 1105 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_737)))->BgL_typez00);
		}

	}



/* &conditional/Cinfo-type */
	BgL_typez00_bglt BGl_z62conditionalzf2Cinfozd2typez42zzcfa_approxz00(obj_t
		BgL_envz00_8136, obj_t BgL_oz00_8137)
	{
		{	/* Cfa/cinfo.sch 1105 */
			return
				BGl_conditionalzf2Cinfozd2typez20zzcfa_approxz00(
				((BgL_conditionalz00_bglt) BgL_oz00_8137));
		}

	}



/* conditional/Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_conditionalzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00
		(BgL_conditionalz00_bglt BgL_oz00_738, BgL_typez00_bglt BgL_vz00_739)
	{
		{	/* Cfa/cinfo.sch 1106 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_738)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_739), BUNSPEC);
		}

	}



/* &conditional/Cinfo-type-set! */
	obj_t BGl_z62conditionalzf2Cinfozd2typezd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8138, obj_t BgL_oz00_8139, obj_t BgL_vz00_8140)
	{
		{	/* Cfa/cinfo.sch 1106 */
			return
				BGl_conditionalzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00(
				((BgL_conditionalz00_bglt) BgL_oz00_8139),
				((BgL_typez00_bglt) BgL_vz00_8140));
		}

	}



/* conditional/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_conditionalzf2Cinfozd2locz20zzcfa_approxz00(BgL_conditionalz00_bglt
		BgL_oz00_740)
	{
		{	/* Cfa/cinfo.sch 1107 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_740)))->BgL_locz00);
		}

	}



/* &conditional/Cinfo-loc */
	obj_t BGl_z62conditionalzf2Cinfozd2locz42zzcfa_approxz00(obj_t
		BgL_envz00_8141, obj_t BgL_oz00_8142)
	{
		{	/* Cfa/cinfo.sch 1107 */
			return
				BGl_conditionalzf2Cinfozd2locz20zzcfa_approxz00(
				((BgL_conditionalz00_bglt) BgL_oz00_8142));
		}

	}



/* make-fail/Cinfo */
	BGL_EXPORTED_DEF BgL_failz00_bglt
		BGl_makezd2failzf2Cinfoz20zzcfa_approxz00(obj_t BgL_loc1619z00_743,
		BgL_typez00_bglt BgL_type1620z00_744, BgL_nodez00_bglt BgL_proc1621z00_745,
		BgL_nodez00_bglt BgL_msg1622z00_746, BgL_nodez00_bglt BgL_obj1623z00_747,
		BgL_approxz00_bglt BgL_approx1625z00_748)
	{
		{	/* Cfa/cinfo.sch 1111 */
			{	/* Cfa/cinfo.sch 1111 */
				BgL_failz00_bglt BgL_new1260z00_9074;

				{	/* Cfa/cinfo.sch 1111 */
					BgL_failz00_bglt BgL_tmp1257z00_9075;
					BgL_failzf2cinfozf2_bglt BgL_wide1258z00_9076;

					{
						BgL_failz00_bglt BgL_auxz00_12516;

						{	/* Cfa/cinfo.sch 1111 */
							BgL_failz00_bglt BgL_new1256z00_9077;

							BgL_new1256z00_9077 =
								((BgL_failz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_failz00_bgl))));
							{	/* Cfa/cinfo.sch 1111 */
								long BgL_arg1739z00_9078;

								BgL_arg1739z00_9078 = BGL_CLASS_NUM(BGl_failz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1256z00_9077),
									BgL_arg1739z00_9078);
							}
							{	/* Cfa/cinfo.sch 1111 */
								BgL_objectz00_bglt BgL_tmpz00_12521;

								BgL_tmpz00_12521 = ((BgL_objectz00_bglt) BgL_new1256z00_9077);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12521, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1256z00_9077);
							BgL_auxz00_12516 = BgL_new1256z00_9077;
						}
						BgL_tmp1257z00_9075 = ((BgL_failz00_bglt) BgL_auxz00_12516);
					}
					BgL_wide1258z00_9076 =
						((BgL_failzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_failzf2cinfozf2_bgl))));
					{	/* Cfa/cinfo.sch 1111 */
						obj_t BgL_auxz00_12529;
						BgL_objectz00_bglt BgL_tmpz00_12527;

						BgL_auxz00_12529 = ((obj_t) BgL_wide1258z00_9076);
						BgL_tmpz00_12527 = ((BgL_objectz00_bglt) BgL_tmp1257z00_9075);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12527, BgL_auxz00_12529);
					}
					((BgL_objectz00_bglt) BgL_tmp1257z00_9075);
					{	/* Cfa/cinfo.sch 1111 */
						long BgL_arg1738z00_9079;

						BgL_arg1738z00_9079 =
							BGL_CLASS_NUM(BGl_failzf2Cinfozf2zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1257z00_9075), BgL_arg1738z00_9079);
					}
					BgL_new1260z00_9074 = ((BgL_failz00_bglt) BgL_tmp1257z00_9075);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1260z00_9074)))->BgL_locz00) =
					((obj_t) BgL_loc1619z00_743), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1260z00_9074)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1620z00_744), BUNSPEC);
				((((BgL_failz00_bglt) COBJECT(((BgL_failz00_bglt)
									BgL_new1260z00_9074)))->BgL_procz00) =
					((BgL_nodez00_bglt) BgL_proc1621z00_745), BUNSPEC);
				((((BgL_failz00_bglt) COBJECT(((BgL_failz00_bglt)
									BgL_new1260z00_9074)))->BgL_msgz00) =
					((BgL_nodez00_bglt) BgL_msg1622z00_746), BUNSPEC);
				((((BgL_failz00_bglt) COBJECT(((BgL_failz00_bglt)
									BgL_new1260z00_9074)))->BgL_objz00) =
					((BgL_nodez00_bglt) BgL_obj1623z00_747), BUNSPEC);
				{
					BgL_failzf2cinfozf2_bglt BgL_auxz00_12547;

					{
						obj_t BgL_auxz00_12548;

						{	/* Cfa/cinfo.sch 1111 */
							BgL_objectz00_bglt BgL_tmpz00_12549;

							BgL_tmpz00_12549 = ((BgL_objectz00_bglt) BgL_new1260z00_9074);
							BgL_auxz00_12548 = BGL_OBJECT_WIDENING(BgL_tmpz00_12549);
						}
						BgL_auxz00_12547 = ((BgL_failzf2cinfozf2_bglt) BgL_auxz00_12548);
					}
					((((BgL_failzf2cinfozf2_bglt) COBJECT(BgL_auxz00_12547))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1625z00_748), BUNSPEC);
				}
				return BgL_new1260z00_9074;
			}
		}

	}



/* &make-fail/Cinfo */
	BgL_failz00_bglt BGl_z62makezd2failzf2Cinfoz42zzcfa_approxz00(obj_t
		BgL_envz00_8143, obj_t BgL_loc1619z00_8144, obj_t BgL_type1620z00_8145,
		obj_t BgL_proc1621z00_8146, obj_t BgL_msg1622z00_8147,
		obj_t BgL_obj1623z00_8148, obj_t BgL_approx1625z00_8149)
	{
		{	/* Cfa/cinfo.sch 1111 */
			return
				BGl_makezd2failzf2Cinfoz20zzcfa_approxz00(BgL_loc1619z00_8144,
				((BgL_typez00_bglt) BgL_type1620z00_8145),
				((BgL_nodez00_bglt) BgL_proc1621z00_8146),
				((BgL_nodez00_bglt) BgL_msg1622z00_8147),
				((BgL_nodez00_bglt) BgL_obj1623z00_8148),
				((BgL_approxz00_bglt) BgL_approx1625z00_8149));
		}

	}



/* fail/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_failzf2Cinfozf3z01zzcfa_approxz00(obj_t
		BgL_objz00_749)
	{
		{	/* Cfa/cinfo.sch 1112 */
			{	/* Cfa/cinfo.sch 1112 */
				obj_t BgL_classz00_9080;

				BgL_classz00_9080 = BGl_failzf2Cinfozf2zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_749))
					{	/* Cfa/cinfo.sch 1112 */
						BgL_objectz00_bglt BgL_arg1807z00_9081;

						BgL_arg1807z00_9081 = (BgL_objectz00_bglt) (BgL_objz00_749);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 1112 */
								long BgL_idxz00_9082;

								BgL_idxz00_9082 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9081);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9082 + 3L)) == BgL_classz00_9080);
							}
						else
							{	/* Cfa/cinfo.sch 1112 */
								bool_t BgL_res2187z00_9085;

								{	/* Cfa/cinfo.sch 1112 */
									obj_t BgL_oclassz00_9086;

									{	/* Cfa/cinfo.sch 1112 */
										obj_t BgL_arg1815z00_9087;
										long BgL_arg1816z00_9088;

										BgL_arg1815z00_9087 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 1112 */
											long BgL_arg1817z00_9089;

											BgL_arg1817z00_9089 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9081);
											BgL_arg1816z00_9088 = (BgL_arg1817z00_9089 - OBJECT_TYPE);
										}
										BgL_oclassz00_9086 =
											VECTOR_REF(BgL_arg1815z00_9087, BgL_arg1816z00_9088);
									}
									{	/* Cfa/cinfo.sch 1112 */
										bool_t BgL__ortest_1115z00_9090;

										BgL__ortest_1115z00_9090 =
											(BgL_classz00_9080 == BgL_oclassz00_9086);
										if (BgL__ortest_1115z00_9090)
											{	/* Cfa/cinfo.sch 1112 */
												BgL_res2187z00_9085 = BgL__ortest_1115z00_9090;
											}
										else
											{	/* Cfa/cinfo.sch 1112 */
												long BgL_odepthz00_9091;

												{	/* Cfa/cinfo.sch 1112 */
													obj_t BgL_arg1804z00_9092;

													BgL_arg1804z00_9092 = (BgL_oclassz00_9086);
													BgL_odepthz00_9091 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9092);
												}
												if ((3L < BgL_odepthz00_9091))
													{	/* Cfa/cinfo.sch 1112 */
														obj_t BgL_arg1802z00_9093;

														{	/* Cfa/cinfo.sch 1112 */
															obj_t BgL_arg1803z00_9094;

															BgL_arg1803z00_9094 = (BgL_oclassz00_9086);
															BgL_arg1802z00_9093 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9094,
																3L);
														}
														BgL_res2187z00_9085 =
															(BgL_arg1802z00_9093 == BgL_classz00_9080);
													}
												else
													{	/* Cfa/cinfo.sch 1112 */
														BgL_res2187z00_9085 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2187z00_9085;
							}
					}
				else
					{	/* Cfa/cinfo.sch 1112 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &fail/Cinfo? */
	obj_t BGl_z62failzf2Cinfozf3z63zzcfa_approxz00(obj_t BgL_envz00_8150,
		obj_t BgL_objz00_8151)
	{
		{	/* Cfa/cinfo.sch 1112 */
			return BBOOL(BGl_failzf2Cinfozf3z01zzcfa_approxz00(BgL_objz00_8151));
		}

	}



/* fail/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_failz00_bglt
		BGl_failzf2Cinfozd2nilz20zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 1113 */
			{	/* Cfa/cinfo.sch 1113 */
				obj_t BgL_classz00_5799;

				BgL_classz00_5799 = BGl_failzf2Cinfozf2zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 1113 */
					obj_t BgL__ortest_1117z00_5800;

					BgL__ortest_1117z00_5800 = BGL_CLASS_NIL(BgL_classz00_5799);
					if (CBOOL(BgL__ortest_1117z00_5800))
						{	/* Cfa/cinfo.sch 1113 */
							return ((BgL_failz00_bglt) BgL__ortest_1117z00_5800);
						}
					else
						{	/* Cfa/cinfo.sch 1113 */
							return
								((BgL_failz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5799));
						}
				}
			}
		}

	}



/* &fail/Cinfo-nil */
	BgL_failz00_bglt BGl_z62failzf2Cinfozd2nilz42zzcfa_approxz00(obj_t
		BgL_envz00_8152)
	{
		{	/* Cfa/cinfo.sch 1113 */
			return BGl_failzf2Cinfozd2nilz20zzcfa_approxz00();
		}

	}



/* fail/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_failzf2Cinfozd2approxz20zzcfa_approxz00(BgL_failz00_bglt BgL_oz00_750)
	{
		{	/* Cfa/cinfo.sch 1114 */
			{
				BgL_failzf2cinfozf2_bglt BgL_auxz00_12591;

				{
					obj_t BgL_auxz00_12592;

					{	/* Cfa/cinfo.sch 1114 */
						BgL_objectz00_bglt BgL_tmpz00_12593;

						BgL_tmpz00_12593 = ((BgL_objectz00_bglt) BgL_oz00_750);
						BgL_auxz00_12592 = BGL_OBJECT_WIDENING(BgL_tmpz00_12593);
					}
					BgL_auxz00_12591 = ((BgL_failzf2cinfozf2_bglt) BgL_auxz00_12592);
				}
				return
					(((BgL_failzf2cinfozf2_bglt) COBJECT(BgL_auxz00_12591))->
					BgL_approxz00);
			}
		}

	}



/* &fail/Cinfo-approx */
	BgL_approxz00_bglt BGl_z62failzf2Cinfozd2approxz42zzcfa_approxz00(obj_t
		BgL_envz00_8153, obj_t BgL_oz00_8154)
	{
		{	/* Cfa/cinfo.sch 1114 */
			return
				BGl_failzf2Cinfozd2approxz20zzcfa_approxz00(
				((BgL_failz00_bglt) BgL_oz00_8154));
		}

	}



/* fail/Cinfo-obj */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_failzf2Cinfozd2objz20zzcfa_approxz00(BgL_failz00_bglt BgL_oz00_753)
	{
		{	/* Cfa/cinfo.sch 1116 */
			return
				(((BgL_failz00_bglt) COBJECT(
						((BgL_failz00_bglt) BgL_oz00_753)))->BgL_objz00);
		}

	}



/* &fail/Cinfo-obj */
	BgL_nodez00_bglt BGl_z62failzf2Cinfozd2objz42zzcfa_approxz00(obj_t
		BgL_envz00_8155, obj_t BgL_oz00_8156)
	{
		{	/* Cfa/cinfo.sch 1116 */
			return
				BGl_failzf2Cinfozd2objz20zzcfa_approxz00(
				((BgL_failz00_bglt) BgL_oz00_8156));
		}

	}



/* fail/Cinfo-obj-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_failzf2Cinfozd2objzd2setz12ze0zzcfa_approxz00(BgL_failz00_bglt
		BgL_oz00_754, BgL_nodez00_bglt BgL_vz00_755)
	{
		{	/* Cfa/cinfo.sch 1117 */
			return
				((((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_oz00_754)))->BgL_objz00) =
				((BgL_nodez00_bglt) BgL_vz00_755), BUNSPEC);
		}

	}



/* &fail/Cinfo-obj-set! */
	obj_t BGl_z62failzf2Cinfozd2objzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8157, obj_t BgL_oz00_8158, obj_t BgL_vz00_8159)
	{
		{	/* Cfa/cinfo.sch 1117 */
			return
				BGl_failzf2Cinfozd2objzd2setz12ze0zzcfa_approxz00(
				((BgL_failz00_bglt) BgL_oz00_8158), ((BgL_nodez00_bglt) BgL_vz00_8159));
		}

	}



/* fail/Cinfo-msg */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_failzf2Cinfozd2msgz20zzcfa_approxz00(BgL_failz00_bglt BgL_oz00_756)
	{
		{	/* Cfa/cinfo.sch 1118 */
			return
				(((BgL_failz00_bglt) COBJECT(
						((BgL_failz00_bglt) BgL_oz00_756)))->BgL_msgz00);
		}

	}



/* &fail/Cinfo-msg */
	BgL_nodez00_bglt BGl_z62failzf2Cinfozd2msgz42zzcfa_approxz00(obj_t
		BgL_envz00_8160, obj_t BgL_oz00_8161)
	{
		{	/* Cfa/cinfo.sch 1118 */
			return
				BGl_failzf2Cinfozd2msgz20zzcfa_approxz00(
				((BgL_failz00_bglt) BgL_oz00_8161));
		}

	}



/* fail/Cinfo-msg-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_failzf2Cinfozd2msgzd2setz12ze0zzcfa_approxz00(BgL_failz00_bglt
		BgL_oz00_757, BgL_nodez00_bglt BgL_vz00_758)
	{
		{	/* Cfa/cinfo.sch 1119 */
			return
				((((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_oz00_757)))->BgL_msgz00) =
				((BgL_nodez00_bglt) BgL_vz00_758), BUNSPEC);
		}

	}



/* &fail/Cinfo-msg-set! */
	obj_t BGl_z62failzf2Cinfozd2msgzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8162, obj_t BgL_oz00_8163, obj_t BgL_vz00_8164)
	{
		{	/* Cfa/cinfo.sch 1119 */
			return
				BGl_failzf2Cinfozd2msgzd2setz12ze0zzcfa_approxz00(
				((BgL_failz00_bglt) BgL_oz00_8163), ((BgL_nodez00_bglt) BgL_vz00_8164));
		}

	}



/* fail/Cinfo-proc */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_failzf2Cinfozd2procz20zzcfa_approxz00(BgL_failz00_bglt BgL_oz00_759)
	{
		{	/* Cfa/cinfo.sch 1120 */
			return
				(((BgL_failz00_bglt) COBJECT(
						((BgL_failz00_bglt) BgL_oz00_759)))->BgL_procz00);
		}

	}



/* &fail/Cinfo-proc */
	BgL_nodez00_bglt BGl_z62failzf2Cinfozd2procz42zzcfa_approxz00(obj_t
		BgL_envz00_8165, obj_t BgL_oz00_8166)
	{
		{	/* Cfa/cinfo.sch 1120 */
			return
				BGl_failzf2Cinfozd2procz20zzcfa_approxz00(
				((BgL_failz00_bglt) BgL_oz00_8166));
		}

	}



/* fail/Cinfo-proc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_failzf2Cinfozd2proczd2setz12ze0zzcfa_approxz00(BgL_failz00_bglt
		BgL_oz00_760, BgL_nodez00_bglt BgL_vz00_761)
	{
		{	/* Cfa/cinfo.sch 1121 */
			return
				((((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_oz00_760)))->BgL_procz00) =
				((BgL_nodez00_bglt) BgL_vz00_761), BUNSPEC);
		}

	}



/* &fail/Cinfo-proc-set! */
	obj_t BGl_z62failzf2Cinfozd2proczd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8167, obj_t BgL_oz00_8168, obj_t BgL_vz00_8169)
	{
		{	/* Cfa/cinfo.sch 1121 */
			return
				BGl_failzf2Cinfozd2proczd2setz12ze0zzcfa_approxz00(
				((BgL_failz00_bglt) BgL_oz00_8168), ((BgL_nodez00_bglt) BgL_vz00_8169));
		}

	}



/* fail/Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_failzf2Cinfozd2typez20zzcfa_approxz00(BgL_failz00_bglt BgL_oz00_762)
	{
		{	/* Cfa/cinfo.sch 1122 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_762)))->BgL_typez00);
		}

	}



/* &fail/Cinfo-type */
	BgL_typez00_bglt BGl_z62failzf2Cinfozd2typez42zzcfa_approxz00(obj_t
		BgL_envz00_8170, obj_t BgL_oz00_8171)
	{
		{	/* Cfa/cinfo.sch 1122 */
			return
				BGl_failzf2Cinfozd2typez20zzcfa_approxz00(
				((BgL_failz00_bglt) BgL_oz00_8171));
		}

	}



/* fail/Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_failzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00(BgL_failz00_bglt
		BgL_oz00_763, BgL_typez00_bglt BgL_vz00_764)
	{
		{	/* Cfa/cinfo.sch 1123 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_763)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_764), BUNSPEC);
		}

	}



/* &fail/Cinfo-type-set! */
	obj_t BGl_z62failzf2Cinfozd2typezd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8172, obj_t BgL_oz00_8173, obj_t BgL_vz00_8174)
	{
		{	/* Cfa/cinfo.sch 1123 */
			return
				BGl_failzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00(
				((BgL_failz00_bglt) BgL_oz00_8173), ((BgL_typez00_bglt) BgL_vz00_8174));
		}

	}



/* fail/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_failzf2Cinfozd2locz20zzcfa_approxz00(BgL_failz00_bglt BgL_oz00_765)
	{
		{	/* Cfa/cinfo.sch 1124 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_765)))->BgL_locz00);
		}

	}



/* &fail/Cinfo-loc */
	obj_t BGl_z62failzf2Cinfozd2locz42zzcfa_approxz00(obj_t BgL_envz00_8175,
		obj_t BgL_oz00_8176)
	{
		{	/* Cfa/cinfo.sch 1124 */
			return
				BGl_failzf2Cinfozd2locz20zzcfa_approxz00(
				((BgL_failz00_bglt) BgL_oz00_8176));
		}

	}



/* make-switch/Cinfo */
	BGL_EXPORTED_DEF BgL_switchz00_bglt
		BGl_makezd2switchzf2Cinfoz20zzcfa_approxz00(obj_t BgL_loc1610z00_768,
		BgL_typez00_bglt BgL_type1611z00_769, obj_t BgL_sidezd2effect1612zd2_770,
		obj_t BgL_key1613z00_771, BgL_nodez00_bglt BgL_test1614z00_772,
		obj_t BgL_clauses1615z00_773, BgL_typez00_bglt BgL_itemzd2type1616zd2_774,
		BgL_approxz00_bglt BgL_approx1617z00_775)
	{
		{	/* Cfa/cinfo.sch 1128 */
			{	/* Cfa/cinfo.sch 1128 */
				BgL_switchz00_bglt BgL_new1264z00_9095;

				{	/* Cfa/cinfo.sch 1128 */
					BgL_switchz00_bglt BgL_tmp1262z00_9096;
					BgL_switchzf2cinfozf2_bglt BgL_wide1263z00_9097;

					{
						BgL_switchz00_bglt BgL_auxz00_12640;

						{	/* Cfa/cinfo.sch 1128 */
							BgL_switchz00_bglt BgL_new1261z00_9098;

							BgL_new1261z00_9098 =
								((BgL_switchz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_switchz00_bgl))));
							{	/* Cfa/cinfo.sch 1128 */
								long BgL_arg1746z00_9099;

								BgL_arg1746z00_9099 = BGL_CLASS_NUM(BGl_switchz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1261z00_9098),
									BgL_arg1746z00_9099);
							}
							{	/* Cfa/cinfo.sch 1128 */
								BgL_objectz00_bglt BgL_tmpz00_12645;

								BgL_tmpz00_12645 = ((BgL_objectz00_bglt) BgL_new1261z00_9098);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12645, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1261z00_9098);
							BgL_auxz00_12640 = BgL_new1261z00_9098;
						}
						BgL_tmp1262z00_9096 = ((BgL_switchz00_bglt) BgL_auxz00_12640);
					}
					BgL_wide1263z00_9097 =
						((BgL_switchzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_switchzf2cinfozf2_bgl))));
					{	/* Cfa/cinfo.sch 1128 */
						obj_t BgL_auxz00_12653;
						BgL_objectz00_bglt BgL_tmpz00_12651;

						BgL_auxz00_12653 = ((obj_t) BgL_wide1263z00_9097);
						BgL_tmpz00_12651 = ((BgL_objectz00_bglt) BgL_tmp1262z00_9096);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12651, BgL_auxz00_12653);
					}
					((BgL_objectz00_bglt) BgL_tmp1262z00_9096);
					{	/* Cfa/cinfo.sch 1128 */
						long BgL_arg1740z00_9100;

						BgL_arg1740z00_9100 =
							BGL_CLASS_NUM(BGl_switchzf2Cinfozf2zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1262z00_9096), BgL_arg1740z00_9100);
					}
					BgL_new1264z00_9095 = ((BgL_switchz00_bglt) BgL_tmp1262z00_9096);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1264z00_9095)))->BgL_locz00) =
					((obj_t) BgL_loc1610z00_768), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1264z00_9095)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1611z00_769), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1264z00_9095)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1612zd2_770), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1264z00_9095)))->BgL_keyz00) =
					((obj_t) BgL_key1613z00_771), BUNSPEC);
				((((BgL_switchz00_bglt) COBJECT(((BgL_switchz00_bglt)
									BgL_new1264z00_9095)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_test1614z00_772), BUNSPEC);
				((((BgL_switchz00_bglt) COBJECT(((BgL_switchz00_bglt)
									BgL_new1264z00_9095)))->BgL_clausesz00) =
					((obj_t) BgL_clauses1615z00_773), BUNSPEC);
				((((BgL_switchz00_bglt) COBJECT(((BgL_switchz00_bglt)
									BgL_new1264z00_9095)))->BgL_itemzd2typezd2) =
					((BgL_typez00_bglt) BgL_itemzd2type1616zd2_774), BUNSPEC);
				{
					BgL_switchzf2cinfozf2_bglt BgL_auxz00_12675;

					{
						obj_t BgL_auxz00_12676;

						{	/* Cfa/cinfo.sch 1128 */
							BgL_objectz00_bglt BgL_tmpz00_12677;

							BgL_tmpz00_12677 = ((BgL_objectz00_bglt) BgL_new1264z00_9095);
							BgL_auxz00_12676 = BGL_OBJECT_WIDENING(BgL_tmpz00_12677);
						}
						BgL_auxz00_12675 = ((BgL_switchzf2cinfozf2_bglt) BgL_auxz00_12676);
					}
					((((BgL_switchzf2cinfozf2_bglt) COBJECT(BgL_auxz00_12675))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1617z00_775), BUNSPEC);
				}
				return BgL_new1264z00_9095;
			}
		}

	}



/* &make-switch/Cinfo */
	BgL_switchz00_bglt BGl_z62makezd2switchzf2Cinfoz42zzcfa_approxz00(obj_t
		BgL_envz00_8177, obj_t BgL_loc1610z00_8178, obj_t BgL_type1611z00_8179,
		obj_t BgL_sidezd2effect1612zd2_8180, obj_t BgL_key1613z00_8181,
		obj_t BgL_test1614z00_8182, obj_t BgL_clauses1615z00_8183,
		obj_t BgL_itemzd2type1616zd2_8184, obj_t BgL_approx1617z00_8185)
	{
		{	/* Cfa/cinfo.sch 1128 */
			return
				BGl_makezd2switchzf2Cinfoz20zzcfa_approxz00(BgL_loc1610z00_8178,
				((BgL_typez00_bglt) BgL_type1611z00_8179),
				BgL_sidezd2effect1612zd2_8180, BgL_key1613z00_8181,
				((BgL_nodez00_bglt) BgL_test1614z00_8182), BgL_clauses1615z00_8183,
				((BgL_typez00_bglt) BgL_itemzd2type1616zd2_8184),
				((BgL_approxz00_bglt) BgL_approx1617z00_8185));
		}

	}



/* switch/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_switchzf2Cinfozf3z01zzcfa_approxz00(obj_t
		BgL_objz00_776)
	{
		{	/* Cfa/cinfo.sch 1129 */
			{	/* Cfa/cinfo.sch 1129 */
				obj_t BgL_classz00_9101;

				BgL_classz00_9101 = BGl_switchzf2Cinfozf2zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_776))
					{	/* Cfa/cinfo.sch 1129 */
						BgL_objectz00_bglt BgL_arg1807z00_9102;

						BgL_arg1807z00_9102 = (BgL_objectz00_bglt) (BgL_objz00_776);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 1129 */
								long BgL_idxz00_9103;

								BgL_idxz00_9103 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9102);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9103 + 4L)) == BgL_classz00_9101);
							}
						else
							{	/* Cfa/cinfo.sch 1129 */
								bool_t BgL_res2188z00_9106;

								{	/* Cfa/cinfo.sch 1129 */
									obj_t BgL_oclassz00_9107;

									{	/* Cfa/cinfo.sch 1129 */
										obj_t BgL_arg1815z00_9108;
										long BgL_arg1816z00_9109;

										BgL_arg1815z00_9108 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 1129 */
											long BgL_arg1817z00_9110;

											BgL_arg1817z00_9110 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9102);
											BgL_arg1816z00_9109 = (BgL_arg1817z00_9110 - OBJECT_TYPE);
										}
										BgL_oclassz00_9107 =
											VECTOR_REF(BgL_arg1815z00_9108, BgL_arg1816z00_9109);
									}
									{	/* Cfa/cinfo.sch 1129 */
										bool_t BgL__ortest_1115z00_9111;

										BgL__ortest_1115z00_9111 =
											(BgL_classz00_9101 == BgL_oclassz00_9107);
										if (BgL__ortest_1115z00_9111)
											{	/* Cfa/cinfo.sch 1129 */
												BgL_res2188z00_9106 = BgL__ortest_1115z00_9111;
											}
										else
											{	/* Cfa/cinfo.sch 1129 */
												long BgL_odepthz00_9112;

												{	/* Cfa/cinfo.sch 1129 */
													obj_t BgL_arg1804z00_9113;

													BgL_arg1804z00_9113 = (BgL_oclassz00_9107);
													BgL_odepthz00_9112 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9113);
												}
												if ((4L < BgL_odepthz00_9112))
													{	/* Cfa/cinfo.sch 1129 */
														obj_t BgL_arg1802z00_9114;

														{	/* Cfa/cinfo.sch 1129 */
															obj_t BgL_arg1803z00_9115;

															BgL_arg1803z00_9115 = (BgL_oclassz00_9107);
															BgL_arg1802z00_9114 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9115,
																4L);
														}
														BgL_res2188z00_9106 =
															(BgL_arg1802z00_9114 == BgL_classz00_9101);
													}
												else
													{	/* Cfa/cinfo.sch 1129 */
														BgL_res2188z00_9106 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2188z00_9106;
							}
					}
				else
					{	/* Cfa/cinfo.sch 1129 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &switch/Cinfo? */
	obj_t BGl_z62switchzf2Cinfozf3z63zzcfa_approxz00(obj_t BgL_envz00_8186,
		obj_t BgL_objz00_8187)
	{
		{	/* Cfa/cinfo.sch 1129 */
			return BBOOL(BGl_switchzf2Cinfozf3z01zzcfa_approxz00(BgL_objz00_8187));
		}

	}



/* switch/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_switchz00_bglt
		BGl_switchzf2Cinfozd2nilz20zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 1130 */
			{	/* Cfa/cinfo.sch 1130 */
				obj_t BgL_classz00_5851;

				BgL_classz00_5851 = BGl_switchzf2Cinfozf2zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 1130 */
					obj_t BgL__ortest_1117z00_5852;

					BgL__ortest_1117z00_5852 = BGL_CLASS_NIL(BgL_classz00_5851);
					if (CBOOL(BgL__ortest_1117z00_5852))
						{	/* Cfa/cinfo.sch 1130 */
							return ((BgL_switchz00_bglt) BgL__ortest_1117z00_5852);
						}
					else
						{	/* Cfa/cinfo.sch 1130 */
							return
								((BgL_switchz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5851));
						}
				}
			}
		}

	}



/* &switch/Cinfo-nil */
	BgL_switchz00_bglt BGl_z62switchzf2Cinfozd2nilz42zzcfa_approxz00(obj_t
		BgL_envz00_8188)
	{
		{	/* Cfa/cinfo.sch 1130 */
			return BGl_switchzf2Cinfozd2nilz20zzcfa_approxz00();
		}

	}



/* switch/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_switchzf2Cinfozd2approxz20zzcfa_approxz00(BgL_switchz00_bglt
		BgL_oz00_777)
	{
		{	/* Cfa/cinfo.sch 1131 */
			{
				BgL_switchzf2cinfozf2_bglt BgL_auxz00_12718;

				{
					obj_t BgL_auxz00_12719;

					{	/* Cfa/cinfo.sch 1131 */
						BgL_objectz00_bglt BgL_tmpz00_12720;

						BgL_tmpz00_12720 = ((BgL_objectz00_bglt) BgL_oz00_777);
						BgL_auxz00_12719 = BGL_OBJECT_WIDENING(BgL_tmpz00_12720);
					}
					BgL_auxz00_12718 = ((BgL_switchzf2cinfozf2_bglt) BgL_auxz00_12719);
				}
				return
					(((BgL_switchzf2cinfozf2_bglt) COBJECT(BgL_auxz00_12718))->
					BgL_approxz00);
			}
		}

	}



/* &switch/Cinfo-approx */
	BgL_approxz00_bglt BGl_z62switchzf2Cinfozd2approxz42zzcfa_approxz00(obj_t
		BgL_envz00_8189, obj_t BgL_oz00_8190)
	{
		{	/* Cfa/cinfo.sch 1131 */
			return
				BGl_switchzf2Cinfozd2approxz20zzcfa_approxz00(
				((BgL_switchz00_bglt) BgL_oz00_8190));
		}

	}



/* switch/Cinfo-item-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_switchzf2Cinfozd2itemzd2typezf2zzcfa_approxz00(BgL_switchz00_bglt
		BgL_oz00_780)
	{
		{	/* Cfa/cinfo.sch 1133 */
			return
				(((BgL_switchz00_bglt) COBJECT(
						((BgL_switchz00_bglt) BgL_oz00_780)))->BgL_itemzd2typezd2);
		}

	}



/* &switch/Cinfo-item-type */
	BgL_typez00_bglt BGl_z62switchzf2Cinfozd2itemzd2typez90zzcfa_approxz00(obj_t
		BgL_envz00_8191, obj_t BgL_oz00_8192)
	{
		{	/* Cfa/cinfo.sch 1133 */
			return
				BGl_switchzf2Cinfozd2itemzd2typezf2zzcfa_approxz00(
				((BgL_switchz00_bglt) BgL_oz00_8192));
		}

	}



/* switch/Cinfo-item-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_switchzf2Cinfozd2itemzd2typezd2setz12z32zzcfa_approxz00
		(BgL_switchz00_bglt BgL_oz00_781, BgL_typez00_bglt BgL_vz00_782)
	{
		{	/* Cfa/cinfo.sch 1134 */
			return
				((((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_oz00_781)))->BgL_itemzd2typezd2) =
				((BgL_typez00_bglt) BgL_vz00_782), BUNSPEC);
		}

	}



/* &switch/Cinfo-item-type-set! */
	obj_t BGl_z62switchzf2Cinfozd2itemzd2typezd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_8193, obj_t BgL_oz00_8194, obj_t BgL_vz00_8195)
	{
		{	/* Cfa/cinfo.sch 1134 */
			return
				BGl_switchzf2Cinfozd2itemzd2typezd2setz12z32zzcfa_approxz00(
				((BgL_switchz00_bglt) BgL_oz00_8194),
				((BgL_typez00_bglt) BgL_vz00_8195));
		}

	}



/* switch/Cinfo-clauses */
	BGL_EXPORTED_DEF obj_t
		BGl_switchzf2Cinfozd2clausesz20zzcfa_approxz00(BgL_switchz00_bglt
		BgL_oz00_783)
	{
		{	/* Cfa/cinfo.sch 1135 */
			return
				(((BgL_switchz00_bglt) COBJECT(
						((BgL_switchz00_bglt) BgL_oz00_783)))->BgL_clausesz00);
		}

	}



/* &switch/Cinfo-clauses */
	obj_t BGl_z62switchzf2Cinfozd2clausesz42zzcfa_approxz00(obj_t BgL_envz00_8196,
		obj_t BgL_oz00_8197)
	{
		{	/* Cfa/cinfo.sch 1135 */
			return
				BGl_switchzf2Cinfozd2clausesz20zzcfa_approxz00(
				((BgL_switchz00_bglt) BgL_oz00_8197));
		}

	}



/* switch/Cinfo-test */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_switchzf2Cinfozd2testz20zzcfa_approxz00(BgL_switchz00_bglt BgL_oz00_786)
	{
		{	/* Cfa/cinfo.sch 1137 */
			return
				(((BgL_switchz00_bglt) COBJECT(
						((BgL_switchz00_bglt) BgL_oz00_786)))->BgL_testz00);
		}

	}



/* &switch/Cinfo-test */
	BgL_nodez00_bglt BGl_z62switchzf2Cinfozd2testz42zzcfa_approxz00(obj_t
		BgL_envz00_8198, obj_t BgL_oz00_8199)
	{
		{	/* Cfa/cinfo.sch 1137 */
			return
				BGl_switchzf2Cinfozd2testz20zzcfa_approxz00(
				((BgL_switchz00_bglt) BgL_oz00_8199));
		}

	}



/* switch/Cinfo-test-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_switchzf2Cinfozd2testzd2setz12ze0zzcfa_approxz00(BgL_switchz00_bglt
		BgL_oz00_787, BgL_nodez00_bglt BgL_vz00_788)
	{
		{	/* Cfa/cinfo.sch 1138 */
			return
				((((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_oz00_787)))->BgL_testz00) =
				((BgL_nodez00_bglt) BgL_vz00_788), BUNSPEC);
		}

	}



/* &switch/Cinfo-test-set! */
	obj_t BGl_z62switchzf2Cinfozd2testzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8200, obj_t BgL_oz00_8201, obj_t BgL_vz00_8202)
	{
		{	/* Cfa/cinfo.sch 1138 */
			return
				BGl_switchzf2Cinfozd2testzd2setz12ze0zzcfa_approxz00(
				((BgL_switchz00_bglt) BgL_oz00_8201),
				((BgL_nodez00_bglt) BgL_vz00_8202));
		}

	}



/* switch/Cinfo-key */
	BGL_EXPORTED_DEF obj_t
		BGl_switchzf2Cinfozd2keyz20zzcfa_approxz00(BgL_switchz00_bglt BgL_oz00_789)
	{
		{	/* Cfa/cinfo.sch 1139 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_789)))->BgL_keyz00);
		}

	}



/* &switch/Cinfo-key */
	obj_t BGl_z62switchzf2Cinfozd2keyz42zzcfa_approxz00(obj_t BgL_envz00_8203,
		obj_t BgL_oz00_8204)
	{
		{	/* Cfa/cinfo.sch 1139 */
			return
				BGl_switchzf2Cinfozd2keyz20zzcfa_approxz00(
				((BgL_switchz00_bglt) BgL_oz00_8204));
		}

	}



/* switch/Cinfo-key-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_switchzf2Cinfozd2keyzd2setz12ze0zzcfa_approxz00(BgL_switchz00_bglt
		BgL_oz00_790, obj_t BgL_vz00_791)
	{
		{	/* Cfa/cinfo.sch 1140 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_790)))->BgL_keyz00) =
				((obj_t) BgL_vz00_791), BUNSPEC);
		}

	}



/* &switch/Cinfo-key-set! */
	obj_t BGl_z62switchzf2Cinfozd2keyzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8205, obj_t BgL_oz00_8206, obj_t BgL_vz00_8207)
	{
		{	/* Cfa/cinfo.sch 1140 */
			return
				BGl_switchzf2Cinfozd2keyzd2setz12ze0zzcfa_approxz00(
				((BgL_switchz00_bglt) BgL_oz00_8206), BgL_vz00_8207);
		}

	}



/* switch/Cinfo-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_switchzf2Cinfozd2sidezd2effectzf2zzcfa_approxz00(BgL_switchz00_bglt
		BgL_oz00_792)
	{
		{	/* Cfa/cinfo.sch 1141 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_792)))->BgL_sidezd2effectzd2);
		}

	}



/* &switch/Cinfo-side-effect */
	obj_t BGl_z62switchzf2Cinfozd2sidezd2effectz90zzcfa_approxz00(obj_t
		BgL_envz00_8208, obj_t BgL_oz00_8209)
	{
		{	/* Cfa/cinfo.sch 1141 */
			return
				BGl_switchzf2Cinfozd2sidezd2effectzf2zzcfa_approxz00(
				((BgL_switchz00_bglt) BgL_oz00_8209));
		}

	}



/* switch/Cinfo-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_switchzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_approxz00
		(BgL_switchz00_bglt BgL_oz00_793, obj_t BgL_vz00_794)
	{
		{	/* Cfa/cinfo.sch 1142 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_793)))->
					BgL_sidezd2effectzd2) = ((obj_t) BgL_vz00_794), BUNSPEC);
		}

	}



/* &switch/Cinfo-side-effect-set! */
	obj_t BGl_z62switchzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_8210, obj_t BgL_oz00_8211, obj_t BgL_vz00_8212)
	{
		{	/* Cfa/cinfo.sch 1142 */
			return
				BGl_switchzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_approxz00(
				((BgL_switchz00_bglt) BgL_oz00_8211), BgL_vz00_8212);
		}

	}



/* switch/Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_switchzf2Cinfozd2typez20zzcfa_approxz00(BgL_switchz00_bglt BgL_oz00_795)
	{
		{	/* Cfa/cinfo.sch 1143 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_795)))->BgL_typez00);
		}

	}



/* &switch/Cinfo-type */
	BgL_typez00_bglt BGl_z62switchzf2Cinfozd2typez42zzcfa_approxz00(obj_t
		BgL_envz00_8213, obj_t BgL_oz00_8214)
	{
		{	/* Cfa/cinfo.sch 1143 */
			return
				BGl_switchzf2Cinfozd2typez20zzcfa_approxz00(
				((BgL_switchz00_bglt) BgL_oz00_8214));
		}

	}



/* switch/Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_switchzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00(BgL_switchz00_bglt
		BgL_oz00_796, BgL_typez00_bglt BgL_vz00_797)
	{
		{	/* Cfa/cinfo.sch 1144 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_796)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_797), BUNSPEC);
		}

	}



/* &switch/Cinfo-type-set! */
	obj_t BGl_z62switchzf2Cinfozd2typezd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8215, obj_t BgL_oz00_8216, obj_t BgL_vz00_8217)
	{
		{	/* Cfa/cinfo.sch 1144 */
			return
				BGl_switchzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00(
				((BgL_switchz00_bglt) BgL_oz00_8216),
				((BgL_typez00_bglt) BgL_vz00_8217));
		}

	}



/* switch/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_switchzf2Cinfozd2locz20zzcfa_approxz00(BgL_switchz00_bglt BgL_oz00_798)
	{
		{	/* Cfa/cinfo.sch 1145 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_798)))->BgL_locz00);
		}

	}



/* &switch/Cinfo-loc */
	obj_t BGl_z62switchzf2Cinfozd2locz42zzcfa_approxz00(obj_t BgL_envz00_8218,
		obj_t BgL_oz00_8219)
	{
		{	/* Cfa/cinfo.sch 1145 */
			return
				BGl_switchzf2Cinfozd2locz20zzcfa_approxz00(
				((BgL_switchz00_bglt) BgL_oz00_8219));
		}

	}



/* make-set-ex-it/Cinfo */
	BGL_EXPORTED_DEF BgL_setzd2exzd2itz00_bglt
		BGl_makezd2setzd2exzd2itzf2Cinfoz20zzcfa_approxz00(obj_t BgL_loc1603z00_801,
		BgL_typez00_bglt BgL_type1604z00_802, BgL_varz00_bglt BgL_var1605z00_803,
		BgL_nodez00_bglt BgL_body1606z00_804,
		BgL_nodez00_bglt BgL_onexit1607z00_805,
		BgL_approxz00_bglt BgL_approx1608z00_806)
	{
		{	/* Cfa/cinfo.sch 1149 */
			{	/* Cfa/cinfo.sch 1149 */
				BgL_setzd2exzd2itz00_bglt BgL_new1268z00_9116;

				{	/* Cfa/cinfo.sch 1149 */
					BgL_setzd2exzd2itz00_bglt BgL_tmp1266z00_9117;
					BgL_setzd2exzd2itzf2cinfozf2_bglt BgL_wide1267z00_9118;

					{
						BgL_setzd2exzd2itz00_bglt BgL_auxz00_12778;

						{	/* Cfa/cinfo.sch 1149 */
							BgL_setzd2exzd2itz00_bglt BgL_new1265z00_9119;

							BgL_new1265z00_9119 =
								((BgL_setzd2exzd2itz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_setzd2exzd2itz00_bgl))));
							{	/* Cfa/cinfo.sch 1149 */
								long BgL_arg1748z00_9120;

								BgL_arg1748z00_9120 =
									BGL_CLASS_NUM(BGl_setzd2exzd2itz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1265z00_9119),
									BgL_arg1748z00_9120);
							}
							{	/* Cfa/cinfo.sch 1149 */
								BgL_objectz00_bglt BgL_tmpz00_12783;

								BgL_tmpz00_12783 = ((BgL_objectz00_bglt) BgL_new1265z00_9119);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12783, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1265z00_9119);
							BgL_auxz00_12778 = BgL_new1265z00_9119;
						}
						BgL_tmp1266z00_9117 =
							((BgL_setzd2exzd2itz00_bglt) BgL_auxz00_12778);
					}
					BgL_wide1267z00_9118 =
						((BgL_setzd2exzd2itzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_setzd2exzd2itzf2cinfozf2_bgl))));
					{	/* Cfa/cinfo.sch 1149 */
						obj_t BgL_auxz00_12791;
						BgL_objectz00_bglt BgL_tmpz00_12789;

						BgL_auxz00_12791 = ((obj_t) BgL_wide1267z00_9118);
						BgL_tmpz00_12789 = ((BgL_objectz00_bglt) BgL_tmp1266z00_9117);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12789, BgL_auxz00_12791);
					}
					((BgL_objectz00_bglt) BgL_tmp1266z00_9117);
					{	/* Cfa/cinfo.sch 1149 */
						long BgL_arg1747z00_9121;

						BgL_arg1747z00_9121 =
							BGL_CLASS_NUM(BGl_setzd2exzd2itzf2Cinfozf2zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1266z00_9117), BgL_arg1747z00_9121);
					}
					BgL_new1268z00_9116 =
						((BgL_setzd2exzd2itz00_bglt) BgL_tmp1266z00_9117);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1268z00_9116)))->BgL_locz00) =
					((obj_t) BgL_loc1603z00_801), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1268z00_9116)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1604z00_802), BUNSPEC);
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(((BgL_setzd2exzd2itz00_bglt)
									BgL_new1268z00_9116)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_var1605z00_803), BUNSPEC);
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(((BgL_setzd2exzd2itz00_bglt)
									BgL_new1268z00_9116)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_body1606z00_804), BUNSPEC);
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(((BgL_setzd2exzd2itz00_bglt)
									BgL_new1268z00_9116)))->BgL_onexitz00) =
					((BgL_nodez00_bglt) BgL_onexit1607z00_805), BUNSPEC);
				{
					BgL_setzd2exzd2itzf2cinfozf2_bglt BgL_auxz00_12809;

					{
						obj_t BgL_auxz00_12810;

						{	/* Cfa/cinfo.sch 1149 */
							BgL_objectz00_bglt BgL_tmpz00_12811;

							BgL_tmpz00_12811 = ((BgL_objectz00_bglt) BgL_new1268z00_9116);
							BgL_auxz00_12810 = BGL_OBJECT_WIDENING(BgL_tmpz00_12811);
						}
						BgL_auxz00_12809 =
							((BgL_setzd2exzd2itzf2cinfozf2_bglt) BgL_auxz00_12810);
					}
					((((BgL_setzd2exzd2itzf2cinfozf2_bglt) COBJECT(BgL_auxz00_12809))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1608z00_806), BUNSPEC);
				}
				return BgL_new1268z00_9116;
			}
		}

	}



/* &make-set-ex-it/Cinfo */
	BgL_setzd2exzd2itz00_bglt
		BGl_z62makezd2setzd2exzd2itzf2Cinfoz42zzcfa_approxz00(obj_t BgL_envz00_8220,
		obj_t BgL_loc1603z00_8221, obj_t BgL_type1604z00_8222,
		obj_t BgL_var1605z00_8223, obj_t BgL_body1606z00_8224,
		obj_t BgL_onexit1607z00_8225, obj_t BgL_approx1608z00_8226)
	{
		{	/* Cfa/cinfo.sch 1149 */
			return
				BGl_makezd2setzd2exzd2itzf2Cinfoz20zzcfa_approxz00(BgL_loc1603z00_8221,
				((BgL_typez00_bglt) BgL_type1604z00_8222),
				((BgL_varz00_bglt) BgL_var1605z00_8223),
				((BgL_nodez00_bglt) BgL_body1606z00_8224),
				((BgL_nodez00_bglt) BgL_onexit1607z00_8225),
				((BgL_approxz00_bglt) BgL_approx1608z00_8226));
		}

	}



/* set-ex-it/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_setzd2exzd2itzf2Cinfozf3z01zzcfa_approxz00(obj_t
		BgL_objz00_807)
	{
		{	/* Cfa/cinfo.sch 1150 */
			{	/* Cfa/cinfo.sch 1150 */
				obj_t BgL_classz00_9122;

				BgL_classz00_9122 = BGl_setzd2exzd2itzf2Cinfozf2zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_807))
					{	/* Cfa/cinfo.sch 1150 */
						BgL_objectz00_bglt BgL_arg1807z00_9123;

						BgL_arg1807z00_9123 = (BgL_objectz00_bglt) (BgL_objz00_807);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 1150 */
								long BgL_idxz00_9124;

								BgL_idxz00_9124 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9123);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9124 + 3L)) == BgL_classz00_9122);
							}
						else
							{	/* Cfa/cinfo.sch 1150 */
								bool_t BgL_res2189z00_9127;

								{	/* Cfa/cinfo.sch 1150 */
									obj_t BgL_oclassz00_9128;

									{	/* Cfa/cinfo.sch 1150 */
										obj_t BgL_arg1815z00_9129;
										long BgL_arg1816z00_9130;

										BgL_arg1815z00_9129 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 1150 */
											long BgL_arg1817z00_9131;

											BgL_arg1817z00_9131 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9123);
											BgL_arg1816z00_9130 = (BgL_arg1817z00_9131 - OBJECT_TYPE);
										}
										BgL_oclassz00_9128 =
											VECTOR_REF(BgL_arg1815z00_9129, BgL_arg1816z00_9130);
									}
									{	/* Cfa/cinfo.sch 1150 */
										bool_t BgL__ortest_1115z00_9132;

										BgL__ortest_1115z00_9132 =
											(BgL_classz00_9122 == BgL_oclassz00_9128);
										if (BgL__ortest_1115z00_9132)
											{	/* Cfa/cinfo.sch 1150 */
												BgL_res2189z00_9127 = BgL__ortest_1115z00_9132;
											}
										else
											{	/* Cfa/cinfo.sch 1150 */
												long BgL_odepthz00_9133;

												{	/* Cfa/cinfo.sch 1150 */
													obj_t BgL_arg1804z00_9134;

													BgL_arg1804z00_9134 = (BgL_oclassz00_9128);
													BgL_odepthz00_9133 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9134);
												}
												if ((3L < BgL_odepthz00_9133))
													{	/* Cfa/cinfo.sch 1150 */
														obj_t BgL_arg1802z00_9135;

														{	/* Cfa/cinfo.sch 1150 */
															obj_t BgL_arg1803z00_9136;

															BgL_arg1803z00_9136 = (BgL_oclassz00_9128);
															BgL_arg1802z00_9135 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9136,
																3L);
														}
														BgL_res2189z00_9127 =
															(BgL_arg1802z00_9135 == BgL_classz00_9122);
													}
												else
													{	/* Cfa/cinfo.sch 1150 */
														BgL_res2189z00_9127 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2189z00_9127;
							}
					}
				else
					{	/* Cfa/cinfo.sch 1150 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &set-ex-it/Cinfo? */
	obj_t BGl_z62setzd2exzd2itzf2Cinfozf3z63zzcfa_approxz00(obj_t BgL_envz00_8227,
		obj_t BgL_objz00_8228)
	{
		{	/* Cfa/cinfo.sch 1150 */
			return
				BBOOL(BGl_setzd2exzd2itzf2Cinfozf3z01zzcfa_approxz00(BgL_objz00_8228));
		}

	}



/* set-ex-it/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_setzd2exzd2itz00_bglt
		BGl_setzd2exzd2itzf2Cinfozd2nilz20zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 1151 */
			{	/* Cfa/cinfo.sch 1151 */
				obj_t BgL_classz00_5903;

				BgL_classz00_5903 = BGl_setzd2exzd2itzf2Cinfozf2zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 1151 */
					obj_t BgL__ortest_1117z00_5904;

					BgL__ortest_1117z00_5904 = BGL_CLASS_NIL(BgL_classz00_5903);
					if (CBOOL(BgL__ortest_1117z00_5904))
						{	/* Cfa/cinfo.sch 1151 */
							return ((BgL_setzd2exzd2itz00_bglt) BgL__ortest_1117z00_5904);
						}
					else
						{	/* Cfa/cinfo.sch 1151 */
							return
								((BgL_setzd2exzd2itz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5903));
						}
				}
			}
		}

	}



/* &set-ex-it/Cinfo-nil */
	BgL_setzd2exzd2itz00_bglt
		BGl_z62setzd2exzd2itzf2Cinfozd2nilz42zzcfa_approxz00(obj_t BgL_envz00_8229)
	{
		{	/* Cfa/cinfo.sch 1151 */
			return BGl_setzd2exzd2itzf2Cinfozd2nilz20zzcfa_approxz00();
		}

	}



/* set-ex-it/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_setzd2exzd2itzf2Cinfozd2approxz20zzcfa_approxz00
		(BgL_setzd2exzd2itz00_bglt BgL_oz00_808)
	{
		{	/* Cfa/cinfo.sch 1152 */
			{
				BgL_setzd2exzd2itzf2cinfozf2_bglt BgL_auxz00_12853;

				{
					obj_t BgL_auxz00_12854;

					{	/* Cfa/cinfo.sch 1152 */
						BgL_objectz00_bglt BgL_tmpz00_12855;

						BgL_tmpz00_12855 = ((BgL_objectz00_bglt) BgL_oz00_808);
						BgL_auxz00_12854 = BGL_OBJECT_WIDENING(BgL_tmpz00_12855);
					}
					BgL_auxz00_12853 =
						((BgL_setzd2exzd2itzf2cinfozf2_bglt) BgL_auxz00_12854);
				}
				return
					(((BgL_setzd2exzd2itzf2cinfozf2_bglt) COBJECT(BgL_auxz00_12853))->
					BgL_approxz00);
			}
		}

	}



/* &set-ex-it/Cinfo-approx */
	BgL_approxz00_bglt
		BGl_z62setzd2exzd2itzf2Cinfozd2approxz42zzcfa_approxz00(obj_t
		BgL_envz00_8230, obj_t BgL_oz00_8231)
	{
		{	/* Cfa/cinfo.sch 1152 */
			return
				BGl_setzd2exzd2itzf2Cinfozd2approxz20zzcfa_approxz00(
				((BgL_setzd2exzd2itz00_bglt) BgL_oz00_8231));
		}

	}



/* set-ex-it/Cinfo-onexit */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_setzd2exzd2itzf2Cinfozd2onexitz20zzcfa_approxz00
		(BgL_setzd2exzd2itz00_bglt BgL_oz00_811)
	{
		{	/* Cfa/cinfo.sch 1154 */
			return
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
						((BgL_setzd2exzd2itz00_bglt) BgL_oz00_811)))->BgL_onexitz00);
		}

	}



/* &set-ex-it/Cinfo-onexit */
	BgL_nodez00_bglt BGl_z62setzd2exzd2itzf2Cinfozd2onexitz42zzcfa_approxz00(obj_t
		BgL_envz00_8232, obj_t BgL_oz00_8233)
	{
		{	/* Cfa/cinfo.sch 1154 */
			return
				BGl_setzd2exzd2itzf2Cinfozd2onexitz20zzcfa_approxz00(
				((BgL_setzd2exzd2itz00_bglt) BgL_oz00_8233));
		}

	}



/* set-ex-it/Cinfo-onexit-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_setzd2exzd2itzf2Cinfozd2onexitzd2setz12ze0zzcfa_approxz00
		(BgL_setzd2exzd2itz00_bglt BgL_oz00_812, BgL_nodez00_bglt BgL_vz00_813)
	{
		{	/* Cfa/cinfo.sch 1155 */
			return
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_oz00_812)))->BgL_onexitz00) =
				((BgL_nodez00_bglt) BgL_vz00_813), BUNSPEC);
		}

	}



/* &set-ex-it/Cinfo-onexit-set! */
	obj_t BGl_z62setzd2exzd2itzf2Cinfozd2onexitzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8234, obj_t BgL_oz00_8235, obj_t BgL_vz00_8236)
	{
		{	/* Cfa/cinfo.sch 1155 */
			return
				BGl_setzd2exzd2itzf2Cinfozd2onexitzd2setz12ze0zzcfa_approxz00(
				((BgL_setzd2exzd2itz00_bglt) BgL_oz00_8235),
				((BgL_nodez00_bglt) BgL_vz00_8236));
		}

	}



/* set-ex-it/Cinfo-body */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_setzd2exzd2itzf2Cinfozd2bodyz20zzcfa_approxz00(BgL_setzd2exzd2itz00_bglt
		BgL_oz00_814)
	{
		{	/* Cfa/cinfo.sch 1156 */
			return
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
						((BgL_setzd2exzd2itz00_bglt) BgL_oz00_814)))->BgL_bodyz00);
		}

	}



/* &set-ex-it/Cinfo-body */
	BgL_nodez00_bglt BGl_z62setzd2exzd2itzf2Cinfozd2bodyz42zzcfa_approxz00(obj_t
		BgL_envz00_8237, obj_t BgL_oz00_8238)
	{
		{	/* Cfa/cinfo.sch 1156 */
			return
				BGl_setzd2exzd2itzf2Cinfozd2bodyz20zzcfa_approxz00(
				((BgL_setzd2exzd2itz00_bglt) BgL_oz00_8238));
		}

	}



/* set-ex-it/Cinfo-body-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_setzd2exzd2itzf2Cinfozd2bodyzd2setz12ze0zzcfa_approxz00
		(BgL_setzd2exzd2itz00_bglt BgL_oz00_815, BgL_nodez00_bglt BgL_vz00_816)
	{
		{	/* Cfa/cinfo.sch 1157 */
			return
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_oz00_815)))->BgL_bodyz00) =
				((BgL_nodez00_bglt) BgL_vz00_816), BUNSPEC);
		}

	}



/* &set-ex-it/Cinfo-body-set! */
	obj_t BGl_z62setzd2exzd2itzf2Cinfozd2bodyzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8239, obj_t BgL_oz00_8240, obj_t BgL_vz00_8241)
	{
		{	/* Cfa/cinfo.sch 1157 */
			return
				BGl_setzd2exzd2itzf2Cinfozd2bodyzd2setz12ze0zzcfa_approxz00(
				((BgL_setzd2exzd2itz00_bglt) BgL_oz00_8240),
				((BgL_nodez00_bglt) BgL_vz00_8241));
		}

	}



/* set-ex-it/Cinfo-var */
	BGL_EXPORTED_DEF BgL_varz00_bglt
		BGl_setzd2exzd2itzf2Cinfozd2varz20zzcfa_approxz00(BgL_setzd2exzd2itz00_bglt
		BgL_oz00_817)
	{
		{	/* Cfa/cinfo.sch 1158 */
			return
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
						((BgL_setzd2exzd2itz00_bglt) BgL_oz00_817)))->BgL_varz00);
		}

	}



/* &set-ex-it/Cinfo-var */
	BgL_varz00_bglt BGl_z62setzd2exzd2itzf2Cinfozd2varz42zzcfa_approxz00(obj_t
		BgL_envz00_8242, obj_t BgL_oz00_8243)
	{
		{	/* Cfa/cinfo.sch 1158 */
			return
				BGl_setzd2exzd2itzf2Cinfozd2varz20zzcfa_approxz00(
				((BgL_setzd2exzd2itz00_bglt) BgL_oz00_8243));
		}

	}



/* set-ex-it/Cinfo-var-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_setzd2exzd2itzf2Cinfozd2varzd2setz12ze0zzcfa_approxz00
		(BgL_setzd2exzd2itz00_bglt BgL_oz00_818, BgL_varz00_bglt BgL_vz00_819)
	{
		{	/* Cfa/cinfo.sch 1159 */
			return
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_oz00_818)))->BgL_varz00) =
				((BgL_varz00_bglt) BgL_vz00_819), BUNSPEC);
		}

	}



/* &set-ex-it/Cinfo-var-set! */
	obj_t BGl_z62setzd2exzd2itzf2Cinfozd2varzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8244, obj_t BgL_oz00_8245, obj_t BgL_vz00_8246)
	{
		{	/* Cfa/cinfo.sch 1159 */
			return
				BGl_setzd2exzd2itzf2Cinfozd2varzd2setz12ze0zzcfa_approxz00(
				((BgL_setzd2exzd2itz00_bglt) BgL_oz00_8245),
				((BgL_varz00_bglt) BgL_vz00_8246));
		}

	}



/* set-ex-it/Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_setzd2exzd2itzf2Cinfozd2typez20zzcfa_approxz00(BgL_setzd2exzd2itz00_bglt
		BgL_oz00_820)
	{
		{	/* Cfa/cinfo.sch 1160 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_820)))->BgL_typez00);
		}

	}



/* &set-ex-it/Cinfo-type */
	BgL_typez00_bglt BGl_z62setzd2exzd2itzf2Cinfozd2typez42zzcfa_approxz00(obj_t
		BgL_envz00_8247, obj_t BgL_oz00_8248)
	{
		{	/* Cfa/cinfo.sch 1160 */
			return
				BGl_setzd2exzd2itzf2Cinfozd2typez20zzcfa_approxz00(
				((BgL_setzd2exzd2itz00_bglt) BgL_oz00_8248));
		}

	}



/* set-ex-it/Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_setzd2exzd2itzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00
		(BgL_setzd2exzd2itz00_bglt BgL_oz00_821, BgL_typez00_bglt BgL_vz00_822)
	{
		{	/* Cfa/cinfo.sch 1161 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_821)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_822), BUNSPEC);
		}

	}



/* &set-ex-it/Cinfo-type-set! */
	obj_t BGl_z62setzd2exzd2itzf2Cinfozd2typezd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8249, obj_t BgL_oz00_8250, obj_t BgL_vz00_8251)
	{
		{	/* Cfa/cinfo.sch 1161 */
			return
				BGl_setzd2exzd2itzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00(
				((BgL_setzd2exzd2itz00_bglt) BgL_oz00_8250),
				((BgL_typez00_bglt) BgL_vz00_8251));
		}

	}



/* set-ex-it/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_setzd2exzd2itzf2Cinfozd2locz20zzcfa_approxz00(BgL_setzd2exzd2itz00_bglt
		BgL_oz00_823)
	{
		{	/* Cfa/cinfo.sch 1162 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_823)))->BgL_locz00);
		}

	}



/* &set-ex-it/Cinfo-loc */
	obj_t BGl_z62setzd2exzd2itzf2Cinfozd2locz42zzcfa_approxz00(obj_t
		BgL_envz00_8252, obj_t BgL_oz00_8253)
	{
		{	/* Cfa/cinfo.sch 1162 */
			return
				BGl_setzd2exzd2itzf2Cinfozd2locz20zzcfa_approxz00(
				((BgL_setzd2exzd2itz00_bglt) BgL_oz00_8253));
		}

	}



/* make-jump-ex-it/Cinfo */
	BGL_EXPORTED_DEF BgL_jumpzd2exzd2itz00_bglt
		BGl_makezd2jumpzd2exzd2itzf2Cinfoz20zzcfa_approxz00(obj_t
		BgL_loc1597z00_826, BgL_typez00_bglt BgL_type1598z00_827,
		BgL_nodez00_bglt BgL_exit1599z00_828, BgL_nodez00_bglt BgL_value1600z00_829,
		BgL_approxz00_bglt BgL_approx1601z00_830)
	{
		{	/* Cfa/cinfo.sch 1166 */
			{	/* Cfa/cinfo.sch 1166 */
				BgL_jumpzd2exzd2itz00_bglt BgL_new1272z00_9137;

				{	/* Cfa/cinfo.sch 1166 */
					BgL_jumpzd2exzd2itz00_bglt BgL_tmp1270z00_9138;
					BgL_jumpzd2exzd2itzf2cinfozf2_bglt BgL_wide1271z00_9139;

					{
						BgL_jumpzd2exzd2itz00_bglt BgL_auxz00_12902;

						{	/* Cfa/cinfo.sch 1166 */
							BgL_jumpzd2exzd2itz00_bglt BgL_new1269z00_9140;

							BgL_new1269z00_9140 =
								((BgL_jumpzd2exzd2itz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_jumpzd2exzd2itz00_bgl))));
							{	/* Cfa/cinfo.sch 1166 */
								long BgL_arg1750z00_9141;

								BgL_arg1750z00_9141 =
									BGL_CLASS_NUM(BGl_jumpzd2exzd2itz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1269z00_9140),
									BgL_arg1750z00_9141);
							}
							{	/* Cfa/cinfo.sch 1166 */
								BgL_objectz00_bglt BgL_tmpz00_12907;

								BgL_tmpz00_12907 = ((BgL_objectz00_bglt) BgL_new1269z00_9140);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12907, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1269z00_9140);
							BgL_auxz00_12902 = BgL_new1269z00_9140;
						}
						BgL_tmp1270z00_9138 =
							((BgL_jumpzd2exzd2itz00_bglt) BgL_auxz00_12902);
					}
					BgL_wide1271z00_9139 =
						((BgL_jumpzd2exzd2itzf2cinfozf2_bglt)
						BOBJECT(GC_MALLOC(sizeof(struct
									BgL_jumpzd2exzd2itzf2cinfozf2_bgl))));
					{	/* Cfa/cinfo.sch 1166 */
						obj_t BgL_auxz00_12915;
						BgL_objectz00_bglt BgL_tmpz00_12913;

						BgL_auxz00_12915 = ((obj_t) BgL_wide1271z00_9139);
						BgL_tmpz00_12913 = ((BgL_objectz00_bglt) BgL_tmp1270z00_9138);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12913, BgL_auxz00_12915);
					}
					((BgL_objectz00_bglt) BgL_tmp1270z00_9138);
					{	/* Cfa/cinfo.sch 1166 */
						long BgL_arg1749z00_9142;

						BgL_arg1749z00_9142 =
							BGL_CLASS_NUM(BGl_jumpzd2exzd2itzf2Cinfozf2zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1270z00_9138), BgL_arg1749z00_9142);
					}
					BgL_new1272z00_9137 =
						((BgL_jumpzd2exzd2itz00_bglt) BgL_tmp1270z00_9138);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1272z00_9137)))->BgL_locz00) =
					((obj_t) BgL_loc1597z00_826), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1272z00_9137)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1598z00_827), BUNSPEC);
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(((BgL_jumpzd2exzd2itz00_bglt)
									BgL_new1272z00_9137)))->BgL_exitz00) =
					((BgL_nodez00_bglt) BgL_exit1599z00_828), BUNSPEC);
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(((BgL_jumpzd2exzd2itz00_bglt)
									BgL_new1272z00_9137)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_value1600z00_829), BUNSPEC);
				{
					BgL_jumpzd2exzd2itzf2cinfozf2_bglt BgL_auxz00_12931;

					{
						obj_t BgL_auxz00_12932;

						{	/* Cfa/cinfo.sch 1166 */
							BgL_objectz00_bglt BgL_tmpz00_12933;

							BgL_tmpz00_12933 = ((BgL_objectz00_bglt) BgL_new1272z00_9137);
							BgL_auxz00_12932 = BGL_OBJECT_WIDENING(BgL_tmpz00_12933);
						}
						BgL_auxz00_12931 =
							((BgL_jumpzd2exzd2itzf2cinfozf2_bglt) BgL_auxz00_12932);
					}
					((((BgL_jumpzd2exzd2itzf2cinfozf2_bglt) COBJECT(BgL_auxz00_12931))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1601z00_830), BUNSPEC);
				}
				return BgL_new1272z00_9137;
			}
		}

	}



/* &make-jump-ex-it/Cinfo */
	BgL_jumpzd2exzd2itz00_bglt
		BGl_z62makezd2jumpzd2exzd2itzf2Cinfoz42zzcfa_approxz00(obj_t
		BgL_envz00_8254, obj_t BgL_loc1597z00_8255, obj_t BgL_type1598z00_8256,
		obj_t BgL_exit1599z00_8257, obj_t BgL_value1600z00_8258,
		obj_t BgL_approx1601z00_8259)
	{
		{	/* Cfa/cinfo.sch 1166 */
			return
				BGl_makezd2jumpzd2exzd2itzf2Cinfoz20zzcfa_approxz00(BgL_loc1597z00_8255,
				((BgL_typez00_bglt) BgL_type1598z00_8256),
				((BgL_nodez00_bglt) BgL_exit1599z00_8257),
				((BgL_nodez00_bglt) BgL_value1600z00_8258),
				((BgL_approxz00_bglt) BgL_approx1601z00_8259));
		}

	}



/* jump-ex-it/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_jumpzd2exzd2itzf2Cinfozf3z01zzcfa_approxz00(obj_t
		BgL_objz00_831)
	{
		{	/* Cfa/cinfo.sch 1167 */
			{	/* Cfa/cinfo.sch 1167 */
				obj_t BgL_classz00_9143;

				BgL_classz00_9143 = BGl_jumpzd2exzd2itzf2Cinfozf2zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_831))
					{	/* Cfa/cinfo.sch 1167 */
						BgL_objectz00_bglt BgL_arg1807z00_9144;

						BgL_arg1807z00_9144 = (BgL_objectz00_bglt) (BgL_objz00_831);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 1167 */
								long BgL_idxz00_9145;

								BgL_idxz00_9145 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9144);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9145 + 3L)) == BgL_classz00_9143);
							}
						else
							{	/* Cfa/cinfo.sch 1167 */
								bool_t BgL_res2190z00_9148;

								{	/* Cfa/cinfo.sch 1167 */
									obj_t BgL_oclassz00_9149;

									{	/* Cfa/cinfo.sch 1167 */
										obj_t BgL_arg1815z00_9150;
										long BgL_arg1816z00_9151;

										BgL_arg1815z00_9150 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 1167 */
											long BgL_arg1817z00_9152;

											BgL_arg1817z00_9152 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9144);
											BgL_arg1816z00_9151 = (BgL_arg1817z00_9152 - OBJECT_TYPE);
										}
										BgL_oclassz00_9149 =
											VECTOR_REF(BgL_arg1815z00_9150, BgL_arg1816z00_9151);
									}
									{	/* Cfa/cinfo.sch 1167 */
										bool_t BgL__ortest_1115z00_9153;

										BgL__ortest_1115z00_9153 =
											(BgL_classz00_9143 == BgL_oclassz00_9149);
										if (BgL__ortest_1115z00_9153)
											{	/* Cfa/cinfo.sch 1167 */
												BgL_res2190z00_9148 = BgL__ortest_1115z00_9153;
											}
										else
											{	/* Cfa/cinfo.sch 1167 */
												long BgL_odepthz00_9154;

												{	/* Cfa/cinfo.sch 1167 */
													obj_t BgL_arg1804z00_9155;

													BgL_arg1804z00_9155 = (BgL_oclassz00_9149);
													BgL_odepthz00_9154 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9155);
												}
												if ((3L < BgL_odepthz00_9154))
													{	/* Cfa/cinfo.sch 1167 */
														obj_t BgL_arg1802z00_9156;

														{	/* Cfa/cinfo.sch 1167 */
															obj_t BgL_arg1803z00_9157;

															BgL_arg1803z00_9157 = (BgL_oclassz00_9149);
															BgL_arg1802z00_9156 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9157,
																3L);
														}
														BgL_res2190z00_9148 =
															(BgL_arg1802z00_9156 == BgL_classz00_9143);
													}
												else
													{	/* Cfa/cinfo.sch 1167 */
														BgL_res2190z00_9148 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2190z00_9148;
							}
					}
				else
					{	/* Cfa/cinfo.sch 1167 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &jump-ex-it/Cinfo? */
	obj_t BGl_z62jumpzd2exzd2itzf2Cinfozf3z63zzcfa_approxz00(obj_t
		BgL_envz00_8260, obj_t BgL_objz00_8261)
	{
		{	/* Cfa/cinfo.sch 1167 */
			return
				BBOOL(BGl_jumpzd2exzd2itzf2Cinfozf3z01zzcfa_approxz00(BgL_objz00_8261));
		}

	}



/* jump-ex-it/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_jumpzd2exzd2itz00_bglt
		BGl_jumpzd2exzd2itzf2Cinfozd2nilz20zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 1168 */
			{	/* Cfa/cinfo.sch 1168 */
				obj_t BgL_classz00_5955;

				BgL_classz00_5955 = BGl_jumpzd2exzd2itzf2Cinfozf2zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 1168 */
					obj_t BgL__ortest_1117z00_5956;

					BgL__ortest_1117z00_5956 = BGL_CLASS_NIL(BgL_classz00_5955);
					if (CBOOL(BgL__ortest_1117z00_5956))
						{	/* Cfa/cinfo.sch 1168 */
							return ((BgL_jumpzd2exzd2itz00_bglt) BgL__ortest_1117z00_5956);
						}
					else
						{	/* Cfa/cinfo.sch 1168 */
							return
								((BgL_jumpzd2exzd2itz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5955));
						}
				}
			}
		}

	}



/* &jump-ex-it/Cinfo-nil */
	BgL_jumpzd2exzd2itz00_bglt
		BGl_z62jumpzd2exzd2itzf2Cinfozd2nilz42zzcfa_approxz00(obj_t BgL_envz00_8262)
	{
		{	/* Cfa/cinfo.sch 1168 */
			return BGl_jumpzd2exzd2itzf2Cinfozd2nilz20zzcfa_approxz00();
		}

	}



/* jump-ex-it/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_jumpzd2exzd2itzf2Cinfozd2approxz20zzcfa_approxz00
		(BgL_jumpzd2exzd2itz00_bglt BgL_oz00_832)
	{
		{	/* Cfa/cinfo.sch 1169 */
			{
				BgL_jumpzd2exzd2itzf2cinfozf2_bglt BgL_auxz00_12974;

				{
					obj_t BgL_auxz00_12975;

					{	/* Cfa/cinfo.sch 1169 */
						BgL_objectz00_bglt BgL_tmpz00_12976;

						BgL_tmpz00_12976 = ((BgL_objectz00_bglt) BgL_oz00_832);
						BgL_auxz00_12975 = BGL_OBJECT_WIDENING(BgL_tmpz00_12976);
					}
					BgL_auxz00_12974 =
						((BgL_jumpzd2exzd2itzf2cinfozf2_bglt) BgL_auxz00_12975);
				}
				return
					(((BgL_jumpzd2exzd2itzf2cinfozf2_bglt) COBJECT(BgL_auxz00_12974))->
					BgL_approxz00);
			}
		}

	}



/* &jump-ex-it/Cinfo-approx */
	BgL_approxz00_bglt
		BGl_z62jumpzd2exzd2itzf2Cinfozd2approxz42zzcfa_approxz00(obj_t
		BgL_envz00_8263, obj_t BgL_oz00_8264)
	{
		{	/* Cfa/cinfo.sch 1169 */
			return
				BGl_jumpzd2exzd2itzf2Cinfozd2approxz20zzcfa_approxz00(
				((BgL_jumpzd2exzd2itz00_bglt) BgL_oz00_8264));
		}

	}



/* jump-ex-it/Cinfo-value */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_jumpzd2exzd2itzf2Cinfozd2valuez20zzcfa_approxz00
		(BgL_jumpzd2exzd2itz00_bglt BgL_oz00_835)
	{
		{	/* Cfa/cinfo.sch 1171 */
			return
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
						((BgL_jumpzd2exzd2itz00_bglt) BgL_oz00_835)))->BgL_valuez00);
		}

	}



/* &jump-ex-it/Cinfo-value */
	BgL_nodez00_bglt BGl_z62jumpzd2exzd2itzf2Cinfozd2valuez42zzcfa_approxz00(obj_t
		BgL_envz00_8265, obj_t BgL_oz00_8266)
	{
		{	/* Cfa/cinfo.sch 1171 */
			return
				BGl_jumpzd2exzd2itzf2Cinfozd2valuez20zzcfa_approxz00(
				((BgL_jumpzd2exzd2itz00_bglt) BgL_oz00_8266));
		}

	}



/* jump-ex-it/Cinfo-value-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jumpzd2exzd2itzf2Cinfozd2valuezd2setz12ze0zzcfa_approxz00
		(BgL_jumpzd2exzd2itz00_bglt BgL_oz00_836, BgL_nodez00_bglt BgL_vz00_837)
	{
		{	/* Cfa/cinfo.sch 1172 */
			return
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_oz00_836)))->BgL_valuez00) =
				((BgL_nodez00_bglt) BgL_vz00_837), BUNSPEC);
		}

	}



/* &jump-ex-it/Cinfo-value-set! */
	obj_t BGl_z62jumpzd2exzd2itzf2Cinfozd2valuezd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8267, obj_t BgL_oz00_8268, obj_t BgL_vz00_8269)
	{
		{	/* Cfa/cinfo.sch 1172 */
			return
				BGl_jumpzd2exzd2itzf2Cinfozd2valuezd2setz12ze0zzcfa_approxz00(
				((BgL_jumpzd2exzd2itz00_bglt) BgL_oz00_8268),
				((BgL_nodez00_bglt) BgL_vz00_8269));
		}

	}



/* jump-ex-it/Cinfo-exit */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_jumpzd2exzd2itzf2Cinfozd2exitz20zzcfa_approxz00
		(BgL_jumpzd2exzd2itz00_bglt BgL_oz00_838)
	{
		{	/* Cfa/cinfo.sch 1173 */
			return
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
						((BgL_jumpzd2exzd2itz00_bglt) BgL_oz00_838)))->BgL_exitz00);
		}

	}



/* &jump-ex-it/Cinfo-exit */
	BgL_nodez00_bglt BGl_z62jumpzd2exzd2itzf2Cinfozd2exitz42zzcfa_approxz00(obj_t
		BgL_envz00_8270, obj_t BgL_oz00_8271)
	{
		{	/* Cfa/cinfo.sch 1173 */
			return
				BGl_jumpzd2exzd2itzf2Cinfozd2exitz20zzcfa_approxz00(
				((BgL_jumpzd2exzd2itz00_bglt) BgL_oz00_8271));
		}

	}



/* jump-ex-it/Cinfo-exit-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jumpzd2exzd2itzf2Cinfozd2exitzd2setz12ze0zzcfa_approxz00
		(BgL_jumpzd2exzd2itz00_bglt BgL_oz00_839, BgL_nodez00_bglt BgL_vz00_840)
	{
		{	/* Cfa/cinfo.sch 1174 */
			return
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_oz00_839)))->BgL_exitz00) =
				((BgL_nodez00_bglt) BgL_vz00_840), BUNSPEC);
		}

	}



/* &jump-ex-it/Cinfo-exit-set! */
	obj_t BGl_z62jumpzd2exzd2itzf2Cinfozd2exitzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8272, obj_t BgL_oz00_8273, obj_t BgL_vz00_8274)
	{
		{	/* Cfa/cinfo.sch 1174 */
			return
				BGl_jumpzd2exzd2itzf2Cinfozd2exitzd2setz12ze0zzcfa_approxz00(
				((BgL_jumpzd2exzd2itz00_bglt) BgL_oz00_8273),
				((BgL_nodez00_bglt) BgL_vz00_8274));
		}

	}



/* jump-ex-it/Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_jumpzd2exzd2itzf2Cinfozd2typez20zzcfa_approxz00
		(BgL_jumpzd2exzd2itz00_bglt BgL_oz00_841)
	{
		{	/* Cfa/cinfo.sch 1175 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_841)))->BgL_typez00);
		}

	}



/* &jump-ex-it/Cinfo-type */
	BgL_typez00_bglt BGl_z62jumpzd2exzd2itzf2Cinfozd2typez42zzcfa_approxz00(obj_t
		BgL_envz00_8275, obj_t BgL_oz00_8276)
	{
		{	/* Cfa/cinfo.sch 1175 */
			return
				BGl_jumpzd2exzd2itzf2Cinfozd2typez20zzcfa_approxz00(
				((BgL_jumpzd2exzd2itz00_bglt) BgL_oz00_8276));
		}

	}



/* jump-ex-it/Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jumpzd2exzd2itzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00
		(BgL_jumpzd2exzd2itz00_bglt BgL_oz00_842, BgL_typez00_bglt BgL_vz00_843)
	{
		{	/* Cfa/cinfo.sch 1176 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_842)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_843), BUNSPEC);
		}

	}



/* &jump-ex-it/Cinfo-type-set! */
	obj_t BGl_z62jumpzd2exzd2itzf2Cinfozd2typezd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8277, obj_t BgL_oz00_8278, obj_t BgL_vz00_8279)
	{
		{	/* Cfa/cinfo.sch 1176 */
			return
				BGl_jumpzd2exzd2itzf2Cinfozd2typezd2setz12ze0zzcfa_approxz00(
				((BgL_jumpzd2exzd2itz00_bglt) BgL_oz00_8278),
				((BgL_typez00_bglt) BgL_vz00_8279));
		}

	}



/* jump-ex-it/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_jumpzd2exzd2itzf2Cinfozd2locz20zzcfa_approxz00
		(BgL_jumpzd2exzd2itz00_bglt BgL_oz00_844)
	{
		{	/* Cfa/cinfo.sch 1177 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_844)))->BgL_locz00);
		}

	}



/* &jump-ex-it/Cinfo-loc */
	obj_t BGl_z62jumpzd2exzd2itzf2Cinfozd2locz42zzcfa_approxz00(obj_t
		BgL_envz00_8280, obj_t BgL_oz00_8281)
	{
		{	/* Cfa/cinfo.sch 1177 */
			return
				BGl_jumpzd2exzd2itzf2Cinfozd2locz20zzcfa_approxz00(
				((BgL_jumpzd2exzd2itz00_bglt) BgL_oz00_8281));
		}

	}



/* make-pre-make-box */
	BGL_EXPORTED_DEF BgL_makezd2boxzd2_bglt
		BGl_makezd2prezd2makezd2boxzd2zzcfa_approxz00(obj_t BgL_loc1589z00_847,
		BgL_typez00_bglt BgL_type1590z00_848, obj_t BgL_sidezd2effect1591zd2_849,
		obj_t BgL_key1592z00_850, BgL_nodez00_bglt BgL_value1593z00_851,
		BgL_typez00_bglt BgL_vtype1594z00_852, obj_t BgL_stackable1595z00_853)
	{
		{	/* Cfa/cinfo.sch 1181 */
			{	/* Cfa/cinfo.sch 1181 */
				BgL_makezd2boxzd2_bglt BgL_new1276z00_9158;

				{	/* Cfa/cinfo.sch 1181 */
					BgL_makezd2boxzd2_bglt BgL_tmp1274z00_9159;
					BgL_prezd2makezd2boxz00_bglt BgL_wide1275z00_9160;

					{
						BgL_makezd2boxzd2_bglt BgL_auxz00_13014;

						{	/* Cfa/cinfo.sch 1181 */
							BgL_makezd2boxzd2_bglt BgL_new1273z00_9161;

							BgL_new1273z00_9161 =
								((BgL_makezd2boxzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_makezd2boxzd2_bgl))));
							{	/* Cfa/cinfo.sch 1181 */
								long BgL_arg1752z00_9162;

								BgL_arg1752z00_9162 =
									BGL_CLASS_NUM(BGl_makezd2boxzd2zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1273z00_9161),
									BgL_arg1752z00_9162);
							}
							{	/* Cfa/cinfo.sch 1181 */
								BgL_objectz00_bglt BgL_tmpz00_13019;

								BgL_tmpz00_13019 = ((BgL_objectz00_bglt) BgL_new1273z00_9161);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13019, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1273z00_9161);
							BgL_auxz00_13014 = BgL_new1273z00_9161;
						}
						BgL_tmp1274z00_9159 = ((BgL_makezd2boxzd2_bglt) BgL_auxz00_13014);
					}
					BgL_wide1275z00_9160 =
						((BgL_prezd2makezd2boxz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_prezd2makezd2boxz00_bgl))));
					{	/* Cfa/cinfo.sch 1181 */
						obj_t BgL_auxz00_13027;
						BgL_objectz00_bglt BgL_tmpz00_13025;

						BgL_auxz00_13027 = ((obj_t) BgL_wide1275z00_9160);
						BgL_tmpz00_13025 = ((BgL_objectz00_bglt) BgL_tmp1274z00_9159);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13025, BgL_auxz00_13027);
					}
					((BgL_objectz00_bglt) BgL_tmp1274z00_9159);
					{	/* Cfa/cinfo.sch 1181 */
						long BgL_arg1751z00_9163;

						BgL_arg1751z00_9163 =
							BGL_CLASS_NUM(BGl_prezd2makezd2boxz00zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1274z00_9159), BgL_arg1751z00_9163);
					}
					BgL_new1276z00_9158 = ((BgL_makezd2boxzd2_bglt) BgL_tmp1274z00_9159);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1276z00_9158)))->BgL_locz00) =
					((obj_t) BgL_loc1589z00_847), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1276z00_9158)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1590z00_848), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1276z00_9158)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1591zd2_849), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1276z00_9158)))->BgL_keyz00) =
					((obj_t) BgL_key1592z00_850), BUNSPEC);
				((((BgL_makezd2boxzd2_bglt) COBJECT(((BgL_makezd2boxzd2_bglt)
									BgL_new1276z00_9158)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_value1593z00_851), BUNSPEC);
				((((BgL_makezd2boxzd2_bglt) COBJECT(((BgL_makezd2boxzd2_bglt)
									BgL_new1276z00_9158)))->BgL_vtypez00) =
					((BgL_typez00_bglt) BgL_vtype1594z00_852), BUNSPEC);
				((((BgL_makezd2boxzd2_bglt) COBJECT(((BgL_makezd2boxzd2_bglt)
									BgL_new1276z00_9158)))->BgL_stackablez00) =
					((obj_t) BgL_stackable1595z00_853), BUNSPEC);
				return BgL_new1276z00_9158;
			}
		}

	}



/* &make-pre-make-box */
	BgL_makezd2boxzd2_bglt BGl_z62makezd2prezd2makezd2boxzb0zzcfa_approxz00(obj_t
		BgL_envz00_8282, obj_t BgL_loc1589z00_8283, obj_t BgL_type1590z00_8284,
		obj_t BgL_sidezd2effect1591zd2_8285, obj_t BgL_key1592z00_8286,
		obj_t BgL_value1593z00_8287, obj_t BgL_vtype1594z00_8288,
		obj_t BgL_stackable1595z00_8289)
	{
		{	/* Cfa/cinfo.sch 1181 */
			return
				BGl_makezd2prezd2makezd2boxzd2zzcfa_approxz00(BgL_loc1589z00_8283,
				((BgL_typez00_bglt) BgL_type1590z00_8284),
				BgL_sidezd2effect1591zd2_8285, BgL_key1592z00_8286,
				((BgL_nodez00_bglt) BgL_value1593z00_8287),
				((BgL_typez00_bglt) BgL_vtype1594z00_8288), BgL_stackable1595z00_8289);
		}

	}



/* pre-make-box? */
	BGL_EXPORTED_DEF bool_t BGl_prezd2makezd2boxzf3zf3zzcfa_approxz00(obj_t
		BgL_objz00_854)
	{
		{	/* Cfa/cinfo.sch 1182 */
			{	/* Cfa/cinfo.sch 1182 */
				obj_t BgL_classz00_9164;

				BgL_classz00_9164 = BGl_prezd2makezd2boxz00zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_854))
					{	/* Cfa/cinfo.sch 1182 */
						BgL_objectz00_bglt BgL_arg1807z00_9165;

						BgL_arg1807z00_9165 = (BgL_objectz00_bglt) (BgL_objz00_854);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 1182 */
								long BgL_idxz00_9166;

								BgL_idxz00_9166 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9165);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9166 + 4L)) == BgL_classz00_9164);
							}
						else
							{	/* Cfa/cinfo.sch 1182 */
								bool_t BgL_res2191z00_9169;

								{	/* Cfa/cinfo.sch 1182 */
									obj_t BgL_oclassz00_9170;

									{	/* Cfa/cinfo.sch 1182 */
										obj_t BgL_arg1815z00_9171;
										long BgL_arg1816z00_9172;

										BgL_arg1815z00_9171 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 1182 */
											long BgL_arg1817z00_9173;

											BgL_arg1817z00_9173 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9165);
											BgL_arg1816z00_9172 = (BgL_arg1817z00_9173 - OBJECT_TYPE);
										}
										BgL_oclassz00_9170 =
											VECTOR_REF(BgL_arg1815z00_9171, BgL_arg1816z00_9172);
									}
									{	/* Cfa/cinfo.sch 1182 */
										bool_t BgL__ortest_1115z00_9174;

										BgL__ortest_1115z00_9174 =
											(BgL_classz00_9164 == BgL_oclassz00_9170);
										if (BgL__ortest_1115z00_9174)
											{	/* Cfa/cinfo.sch 1182 */
												BgL_res2191z00_9169 = BgL__ortest_1115z00_9174;
											}
										else
											{	/* Cfa/cinfo.sch 1182 */
												long BgL_odepthz00_9175;

												{	/* Cfa/cinfo.sch 1182 */
													obj_t BgL_arg1804z00_9176;

													BgL_arg1804z00_9176 = (BgL_oclassz00_9170);
													BgL_odepthz00_9175 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9176);
												}
												if ((4L < BgL_odepthz00_9175))
													{	/* Cfa/cinfo.sch 1182 */
														obj_t BgL_arg1802z00_9177;

														{	/* Cfa/cinfo.sch 1182 */
															obj_t BgL_arg1803z00_9178;

															BgL_arg1803z00_9178 = (BgL_oclassz00_9170);
															BgL_arg1802z00_9177 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9178,
																4L);
														}
														BgL_res2191z00_9169 =
															(BgL_arg1802z00_9177 == BgL_classz00_9164);
													}
												else
													{	/* Cfa/cinfo.sch 1182 */
														BgL_res2191z00_9169 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2191z00_9169;
							}
					}
				else
					{	/* Cfa/cinfo.sch 1182 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &pre-make-box? */
	obj_t BGl_z62prezd2makezd2boxzf3z91zzcfa_approxz00(obj_t BgL_envz00_8290,
		obj_t BgL_objz00_8291)
	{
		{	/* Cfa/cinfo.sch 1182 */
			return BBOOL(BGl_prezd2makezd2boxzf3zf3zzcfa_approxz00(BgL_objz00_8291));
		}

	}



/* pre-make-box-nil */
	BGL_EXPORTED_DEF BgL_makezd2boxzd2_bglt
		BGl_prezd2makezd2boxzd2nilzd2zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 1183 */
			{	/* Cfa/cinfo.sch 1183 */
				obj_t BgL_classz00_6006;

				BgL_classz00_6006 = BGl_prezd2makezd2boxz00zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 1183 */
					obj_t BgL__ortest_1117z00_6007;

					BgL__ortest_1117z00_6007 = BGL_CLASS_NIL(BgL_classz00_6006);
					if (CBOOL(BgL__ortest_1117z00_6007))
						{	/* Cfa/cinfo.sch 1183 */
							return ((BgL_makezd2boxzd2_bglt) BgL__ortest_1117z00_6007);
						}
					else
						{	/* Cfa/cinfo.sch 1183 */
							return
								((BgL_makezd2boxzd2_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_6006));
						}
				}
			}
		}

	}



/* &pre-make-box-nil */
	BgL_makezd2boxzd2_bglt BGl_z62prezd2makezd2boxzd2nilzb0zzcfa_approxz00(obj_t
		BgL_envz00_8292)
	{
		{	/* Cfa/cinfo.sch 1183 */
			return BGl_prezd2makezd2boxzd2nilzd2zzcfa_approxz00();
		}

	}



/* pre-make-box-stackable */
	BGL_EXPORTED_DEF obj_t
		BGl_prezd2makezd2boxzd2stackablezd2zzcfa_approxz00(BgL_makezd2boxzd2_bglt
		BgL_oz00_855)
	{
		{	/* Cfa/cinfo.sch 1184 */
			return
				(((BgL_makezd2boxzd2_bglt) COBJECT(
						((BgL_makezd2boxzd2_bglt) BgL_oz00_855)))->BgL_stackablez00);
		}

	}



/* &pre-make-box-stackable */
	obj_t BGl_z62prezd2makezd2boxzd2stackablezb0zzcfa_approxz00(obj_t
		BgL_envz00_8293, obj_t BgL_oz00_8294)
	{
		{	/* Cfa/cinfo.sch 1184 */
			return
				BGl_prezd2makezd2boxzd2stackablezd2zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8294));
		}

	}



/* pre-make-box-stackable-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_prezd2makezd2boxzd2stackablezd2setz12z12zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt BgL_oz00_856, obj_t BgL_vz00_857)
	{
		{	/* Cfa/cinfo.sch 1185 */
			return
				((((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_oz00_856)))->BgL_stackablez00) =
				((obj_t) BgL_vz00_857), BUNSPEC);
		}

	}



/* &pre-make-box-stackable-set! */
	obj_t BGl_z62prezd2makezd2boxzd2stackablezd2setz12z70zzcfa_approxz00(obj_t
		BgL_envz00_8295, obj_t BgL_oz00_8296, obj_t BgL_vz00_8297)
	{
		{	/* Cfa/cinfo.sch 1185 */
			return
				BGl_prezd2makezd2boxzd2stackablezd2setz12z12zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8296), BgL_vz00_8297);
		}

	}



/* pre-make-box-vtype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_prezd2makezd2boxzd2vtypezd2zzcfa_approxz00(BgL_makezd2boxzd2_bglt
		BgL_oz00_858)
	{
		{	/* Cfa/cinfo.sch 1186 */
			return
				(((BgL_makezd2boxzd2_bglt) COBJECT(
						((BgL_makezd2boxzd2_bglt) BgL_oz00_858)))->BgL_vtypez00);
		}

	}



/* &pre-make-box-vtype */
	BgL_typez00_bglt BGl_z62prezd2makezd2boxzd2vtypezb0zzcfa_approxz00(obj_t
		BgL_envz00_8298, obj_t BgL_oz00_8299)
	{
		{	/* Cfa/cinfo.sch 1186 */
			return
				BGl_prezd2makezd2boxzd2vtypezd2zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8299));
		}

	}



/* pre-make-box-vtype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_prezd2makezd2boxzd2vtypezd2setz12z12zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt BgL_oz00_859, BgL_typez00_bglt BgL_vz00_860)
	{
		{	/* Cfa/cinfo.sch 1187 */
			return
				((((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_oz00_859)))->BgL_vtypez00) =
				((BgL_typez00_bglt) BgL_vz00_860), BUNSPEC);
		}

	}



/* &pre-make-box-vtype-set! */
	obj_t BGl_z62prezd2makezd2boxzd2vtypezd2setz12z70zzcfa_approxz00(obj_t
		BgL_envz00_8300, obj_t BgL_oz00_8301, obj_t BgL_vz00_8302)
	{
		{	/* Cfa/cinfo.sch 1187 */
			return
				BGl_prezd2makezd2boxzd2vtypezd2setz12z12zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8301),
				((BgL_typez00_bglt) BgL_vz00_8302));
		}

	}



/* pre-make-box-value */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_prezd2makezd2boxzd2valuezd2zzcfa_approxz00(BgL_makezd2boxzd2_bglt
		BgL_oz00_861)
	{
		{	/* Cfa/cinfo.sch 1188 */
			return
				(((BgL_makezd2boxzd2_bglt) COBJECT(
						((BgL_makezd2boxzd2_bglt) BgL_oz00_861)))->BgL_valuez00);
		}

	}



/* &pre-make-box-value */
	BgL_nodez00_bglt BGl_z62prezd2makezd2boxzd2valuezb0zzcfa_approxz00(obj_t
		BgL_envz00_8303, obj_t BgL_oz00_8304)
	{
		{	/* Cfa/cinfo.sch 1188 */
			return
				BGl_prezd2makezd2boxzd2valuezd2zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8304));
		}

	}



/* pre-make-box-value-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_prezd2makezd2boxzd2valuezd2setz12z12zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt BgL_oz00_862, BgL_nodez00_bglt BgL_vz00_863)
	{
		{	/* Cfa/cinfo.sch 1189 */
			return
				((((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_oz00_862)))->BgL_valuez00) =
				((BgL_nodez00_bglt) BgL_vz00_863), BUNSPEC);
		}

	}



/* &pre-make-box-value-set! */
	obj_t BGl_z62prezd2makezd2boxzd2valuezd2setz12z70zzcfa_approxz00(obj_t
		BgL_envz00_8305, obj_t BgL_oz00_8306, obj_t BgL_vz00_8307)
	{
		{	/* Cfa/cinfo.sch 1189 */
			return
				BGl_prezd2makezd2boxzd2valuezd2setz12z12zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8306),
				((BgL_nodez00_bglt) BgL_vz00_8307));
		}

	}



/* pre-make-box-key */
	BGL_EXPORTED_DEF obj_t
		BGl_prezd2makezd2boxzd2keyzd2zzcfa_approxz00(BgL_makezd2boxzd2_bglt
		BgL_oz00_864)
	{
		{	/* Cfa/cinfo.sch 1190 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_864)))->BgL_keyz00);
		}

	}



/* &pre-make-box-key */
	obj_t BGl_z62prezd2makezd2boxzd2keyzb0zzcfa_approxz00(obj_t BgL_envz00_8308,
		obj_t BgL_oz00_8309)
	{
		{	/* Cfa/cinfo.sch 1190 */
			return
				BGl_prezd2makezd2boxzd2keyzd2zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8309));
		}

	}



/* pre-make-box-key-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_prezd2makezd2boxzd2keyzd2setz12z12zzcfa_approxz00(BgL_makezd2boxzd2_bglt
		BgL_oz00_865, obj_t BgL_vz00_866)
	{
		{	/* Cfa/cinfo.sch 1191 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_865)))->BgL_keyz00) =
				((obj_t) BgL_vz00_866), BUNSPEC);
		}

	}



/* &pre-make-box-key-set! */
	obj_t BGl_z62prezd2makezd2boxzd2keyzd2setz12z70zzcfa_approxz00(obj_t
		BgL_envz00_8310, obj_t BgL_oz00_8311, obj_t BgL_vz00_8312)
	{
		{	/* Cfa/cinfo.sch 1191 */
			return
				BGl_prezd2makezd2boxzd2keyzd2setz12z12zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8311), BgL_vz00_8312);
		}

	}



/* pre-make-box-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_prezd2makezd2boxzd2sidezd2effectz00zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt BgL_oz00_867)
	{
		{	/* Cfa/cinfo.sch 1192 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_867)))->BgL_sidezd2effectzd2);
		}

	}



/* &pre-make-box-side-effect */
	obj_t BGl_z62prezd2makezd2boxzd2sidezd2effectz62zzcfa_approxz00(obj_t
		BgL_envz00_8313, obj_t BgL_oz00_8314)
	{
		{	/* Cfa/cinfo.sch 1192 */
			return
				BGl_prezd2makezd2boxzd2sidezd2effectz00zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8314));
		}

	}



/* pre-make-box-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_prezd2makezd2boxzd2sidezd2effectzd2setz12zc0zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt BgL_oz00_868, obj_t BgL_vz00_869)
	{
		{	/* Cfa/cinfo.sch 1193 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_868)))->
					BgL_sidezd2effectzd2) = ((obj_t) BgL_vz00_869), BUNSPEC);
		}

	}



/* &pre-make-box-side-effect-set! */
	obj_t BGl_z62prezd2makezd2boxzd2sidezd2effectzd2setz12za2zzcfa_approxz00(obj_t
		BgL_envz00_8315, obj_t BgL_oz00_8316, obj_t BgL_vz00_8317)
	{
		{	/* Cfa/cinfo.sch 1193 */
			return
				BGl_prezd2makezd2boxzd2sidezd2effectzd2setz12zc0zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8316), BgL_vz00_8317);
		}

	}



/* pre-make-box-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_prezd2makezd2boxzd2typezd2zzcfa_approxz00(BgL_makezd2boxzd2_bglt
		BgL_oz00_870)
	{
		{	/* Cfa/cinfo.sch 1194 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_870)))->BgL_typez00);
		}

	}



/* &pre-make-box-type */
	BgL_typez00_bglt BGl_z62prezd2makezd2boxzd2typezb0zzcfa_approxz00(obj_t
		BgL_envz00_8318, obj_t BgL_oz00_8319)
	{
		{	/* Cfa/cinfo.sch 1194 */
			return
				BGl_prezd2makezd2boxzd2typezd2zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8319));
		}

	}



/* pre-make-box-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_prezd2makezd2boxzd2typezd2setz12z12zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt BgL_oz00_871, BgL_typez00_bglt BgL_vz00_872)
	{
		{	/* Cfa/cinfo.sch 1195 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_871)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_872), BUNSPEC);
		}

	}



/* &pre-make-box-type-set! */
	obj_t BGl_z62prezd2makezd2boxzd2typezd2setz12z70zzcfa_approxz00(obj_t
		BgL_envz00_8320, obj_t BgL_oz00_8321, obj_t BgL_vz00_8322)
	{
		{	/* Cfa/cinfo.sch 1195 */
			return
				BGl_prezd2makezd2boxzd2typezd2setz12z12zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8321),
				((BgL_typez00_bglt) BgL_vz00_8322));
		}

	}



/* pre-make-box-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_prezd2makezd2boxzd2loczd2zzcfa_approxz00(BgL_makezd2boxzd2_bglt
		BgL_oz00_873)
	{
		{	/* Cfa/cinfo.sch 1196 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_873)))->BgL_locz00);
		}

	}



/* &pre-make-box-loc */
	obj_t BGl_z62prezd2makezd2boxzd2loczb0zzcfa_approxz00(obj_t BgL_envz00_8323,
		obj_t BgL_oz00_8324)
	{
		{	/* Cfa/cinfo.sch 1196 */
			return
				BGl_prezd2makezd2boxzd2loczd2zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8324));
		}

	}



/* make-make-box/Cinfo */
	BGL_EXPORTED_DEF BgL_makezd2boxzd2_bglt
		BGl_makezd2makezd2boxzf2Cinfozf2zzcfa_approxz00(obj_t BgL_loc1579z00_876,
		BgL_typez00_bglt BgL_type1580z00_877, obj_t BgL_sidezd2effect1581zd2_878,
		obj_t BgL_key1582z00_879, BgL_nodez00_bglt BgL_value1583z00_880,
		BgL_typez00_bglt BgL_vtype1584z00_881, obj_t BgL_stackable1585z00_882,
		BgL_approxz00_bglt BgL_approx1586z00_883)
	{
		{	/* Cfa/cinfo.sch 1200 */
			{	/* Cfa/cinfo.sch 1200 */
				BgL_makezd2boxzd2_bglt BgL_new1280z00_9179;

				{	/* Cfa/cinfo.sch 1200 */
					BgL_makezd2boxzd2_bglt BgL_tmp1278z00_9180;
					BgL_makezd2boxzf2cinfoz20_bglt BgL_wide1279z00_9181;

					{
						BgL_makezd2boxzd2_bglt BgL_auxz00_13139;

						{	/* Cfa/cinfo.sch 1200 */
							BgL_makezd2boxzd2_bglt BgL_new1277z00_9182;

							BgL_new1277z00_9182 =
								((BgL_makezd2boxzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_makezd2boxzd2_bgl))));
							{	/* Cfa/cinfo.sch 1200 */
								long BgL_arg1754z00_9183;

								BgL_arg1754z00_9183 =
									BGL_CLASS_NUM(BGl_makezd2boxzd2zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1277z00_9182),
									BgL_arg1754z00_9183);
							}
							{	/* Cfa/cinfo.sch 1200 */
								BgL_objectz00_bglt BgL_tmpz00_13144;

								BgL_tmpz00_13144 = ((BgL_objectz00_bglt) BgL_new1277z00_9182);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13144, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1277z00_9182);
							BgL_auxz00_13139 = BgL_new1277z00_9182;
						}
						BgL_tmp1278z00_9180 = ((BgL_makezd2boxzd2_bglt) BgL_auxz00_13139);
					}
					BgL_wide1279z00_9181 =
						((BgL_makezd2boxzf2cinfoz20_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_makezd2boxzf2cinfoz20_bgl))));
					{	/* Cfa/cinfo.sch 1200 */
						obj_t BgL_auxz00_13152;
						BgL_objectz00_bglt BgL_tmpz00_13150;

						BgL_auxz00_13152 = ((obj_t) BgL_wide1279z00_9181);
						BgL_tmpz00_13150 = ((BgL_objectz00_bglt) BgL_tmp1278z00_9180);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13150, BgL_auxz00_13152);
					}
					((BgL_objectz00_bglt) BgL_tmp1278z00_9180);
					{	/* Cfa/cinfo.sch 1200 */
						long BgL_arg1753z00_9184;

						BgL_arg1753z00_9184 =
							BGL_CLASS_NUM(BGl_makezd2boxzf2Cinfoz20zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1278z00_9180), BgL_arg1753z00_9184);
					}
					BgL_new1280z00_9179 = ((BgL_makezd2boxzd2_bglt) BgL_tmp1278z00_9180);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1280z00_9179)))->BgL_locz00) =
					((obj_t) BgL_loc1579z00_876), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1280z00_9179)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1580z00_877), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1280z00_9179)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1581zd2_878), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1280z00_9179)))->BgL_keyz00) =
					((obj_t) BgL_key1582z00_879), BUNSPEC);
				((((BgL_makezd2boxzd2_bglt) COBJECT(((BgL_makezd2boxzd2_bglt)
									BgL_new1280z00_9179)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_value1583z00_880), BUNSPEC);
				((((BgL_makezd2boxzd2_bglt) COBJECT(((BgL_makezd2boxzd2_bglt)
									BgL_new1280z00_9179)))->BgL_vtypez00) =
					((BgL_typez00_bglt) BgL_vtype1584z00_881), BUNSPEC);
				((((BgL_makezd2boxzd2_bglt) COBJECT(((BgL_makezd2boxzd2_bglt)
									BgL_new1280z00_9179)))->BgL_stackablez00) =
					((obj_t) BgL_stackable1585z00_882), BUNSPEC);
				{
					BgL_makezd2boxzf2cinfoz20_bglt BgL_auxz00_13174;

					{
						obj_t BgL_auxz00_13175;

						{	/* Cfa/cinfo.sch 1200 */
							BgL_objectz00_bglt BgL_tmpz00_13176;

							BgL_tmpz00_13176 = ((BgL_objectz00_bglt) BgL_new1280z00_9179);
							BgL_auxz00_13175 = BGL_OBJECT_WIDENING(BgL_tmpz00_13176);
						}
						BgL_auxz00_13174 =
							((BgL_makezd2boxzf2cinfoz20_bglt) BgL_auxz00_13175);
					}
					((((BgL_makezd2boxzf2cinfoz20_bglt) COBJECT(BgL_auxz00_13174))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1586z00_883), BUNSPEC);
				}
				return BgL_new1280z00_9179;
			}
		}

	}



/* &make-make-box/Cinfo */
	BgL_makezd2boxzd2_bglt
		BGl_z62makezd2makezd2boxzf2Cinfoz90zzcfa_approxz00(obj_t BgL_envz00_8325,
		obj_t BgL_loc1579z00_8326, obj_t BgL_type1580z00_8327,
		obj_t BgL_sidezd2effect1581zd2_8328, obj_t BgL_key1582z00_8329,
		obj_t BgL_value1583z00_8330, obj_t BgL_vtype1584z00_8331,
		obj_t BgL_stackable1585z00_8332, obj_t BgL_approx1586z00_8333)
	{
		{	/* Cfa/cinfo.sch 1200 */
			return
				BGl_makezd2makezd2boxzf2Cinfozf2zzcfa_approxz00(BgL_loc1579z00_8326,
				((BgL_typez00_bglt) BgL_type1580z00_8327),
				BgL_sidezd2effect1581zd2_8328, BgL_key1582z00_8329,
				((BgL_nodez00_bglt) BgL_value1583z00_8330),
				((BgL_typez00_bglt) BgL_vtype1584z00_8331), BgL_stackable1585z00_8332,
				((BgL_approxz00_bglt) BgL_approx1586z00_8333));
		}

	}



/* make-box/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_makezd2boxzf2Cinfozf3zd3zzcfa_approxz00(obj_t
		BgL_objz00_884)
	{
		{	/* Cfa/cinfo.sch 1201 */
			{	/* Cfa/cinfo.sch 1201 */
				obj_t BgL_classz00_9185;

				BgL_classz00_9185 = BGl_makezd2boxzf2Cinfoz20zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_884))
					{	/* Cfa/cinfo.sch 1201 */
						BgL_objectz00_bglt BgL_arg1807z00_9186;

						BgL_arg1807z00_9186 = (BgL_objectz00_bglt) (BgL_objz00_884);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 1201 */
								long BgL_idxz00_9187;

								BgL_idxz00_9187 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9186);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9187 + 4L)) == BgL_classz00_9185);
							}
						else
							{	/* Cfa/cinfo.sch 1201 */
								bool_t BgL_res2192z00_9190;

								{	/* Cfa/cinfo.sch 1201 */
									obj_t BgL_oclassz00_9191;

									{	/* Cfa/cinfo.sch 1201 */
										obj_t BgL_arg1815z00_9192;
										long BgL_arg1816z00_9193;

										BgL_arg1815z00_9192 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 1201 */
											long BgL_arg1817z00_9194;

											BgL_arg1817z00_9194 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9186);
											BgL_arg1816z00_9193 = (BgL_arg1817z00_9194 - OBJECT_TYPE);
										}
										BgL_oclassz00_9191 =
											VECTOR_REF(BgL_arg1815z00_9192, BgL_arg1816z00_9193);
									}
									{	/* Cfa/cinfo.sch 1201 */
										bool_t BgL__ortest_1115z00_9195;

										BgL__ortest_1115z00_9195 =
											(BgL_classz00_9185 == BgL_oclassz00_9191);
										if (BgL__ortest_1115z00_9195)
											{	/* Cfa/cinfo.sch 1201 */
												BgL_res2192z00_9190 = BgL__ortest_1115z00_9195;
											}
										else
											{	/* Cfa/cinfo.sch 1201 */
												long BgL_odepthz00_9196;

												{	/* Cfa/cinfo.sch 1201 */
													obj_t BgL_arg1804z00_9197;

													BgL_arg1804z00_9197 = (BgL_oclassz00_9191);
													BgL_odepthz00_9196 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9197);
												}
												if ((4L < BgL_odepthz00_9196))
													{	/* Cfa/cinfo.sch 1201 */
														obj_t BgL_arg1802z00_9198;

														{	/* Cfa/cinfo.sch 1201 */
															obj_t BgL_arg1803z00_9199;

															BgL_arg1803z00_9199 = (BgL_oclassz00_9191);
															BgL_arg1802z00_9198 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9199,
																4L);
														}
														BgL_res2192z00_9190 =
															(BgL_arg1802z00_9198 == BgL_classz00_9185);
													}
												else
													{	/* Cfa/cinfo.sch 1201 */
														BgL_res2192z00_9190 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2192z00_9190;
							}
					}
				else
					{	/* Cfa/cinfo.sch 1201 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &make-box/Cinfo? */
	obj_t BGl_z62makezd2boxzf2Cinfozf3zb1zzcfa_approxz00(obj_t BgL_envz00_8334,
		obj_t BgL_objz00_8335)
	{
		{	/* Cfa/cinfo.sch 1201 */
			return
				BBOOL(BGl_makezd2boxzf2Cinfozf3zd3zzcfa_approxz00(BgL_objz00_8335));
		}

	}



/* make-box/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_makezd2boxzd2_bglt
		BGl_makezd2boxzf2Cinfozd2nilzf2zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 1202 */
			{	/* Cfa/cinfo.sch 1202 */
				obj_t BgL_classz00_6057;

				BgL_classz00_6057 = BGl_makezd2boxzf2Cinfoz20zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 1202 */
					obj_t BgL__ortest_1117z00_6058;

					BgL__ortest_1117z00_6058 = BGL_CLASS_NIL(BgL_classz00_6057);
					if (CBOOL(BgL__ortest_1117z00_6058))
						{	/* Cfa/cinfo.sch 1202 */
							return ((BgL_makezd2boxzd2_bglt) BgL__ortest_1117z00_6058);
						}
					else
						{	/* Cfa/cinfo.sch 1202 */
							return
								((BgL_makezd2boxzd2_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_6057));
						}
				}
			}
		}

	}



/* &make-box/Cinfo-nil */
	BgL_makezd2boxzd2_bglt BGl_z62makezd2boxzf2Cinfozd2nilz90zzcfa_approxz00(obj_t
		BgL_envz00_8336)
	{
		{	/* Cfa/cinfo.sch 1202 */
			return BGl_makezd2boxzf2Cinfozd2nilzf2zzcfa_approxz00();
		}

	}



/* make-box/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_makezd2boxzf2Cinfozd2approxzf2zzcfa_approxz00(BgL_makezd2boxzd2_bglt
		BgL_oz00_885)
	{
		{	/* Cfa/cinfo.sch 1203 */
			{
				BgL_makezd2boxzf2cinfoz20_bglt BgL_auxz00_13217;

				{
					obj_t BgL_auxz00_13218;

					{	/* Cfa/cinfo.sch 1203 */
						BgL_objectz00_bglt BgL_tmpz00_13219;

						BgL_tmpz00_13219 = ((BgL_objectz00_bglt) BgL_oz00_885);
						BgL_auxz00_13218 = BGL_OBJECT_WIDENING(BgL_tmpz00_13219);
					}
					BgL_auxz00_13217 =
						((BgL_makezd2boxzf2cinfoz20_bglt) BgL_auxz00_13218);
				}
				return
					(((BgL_makezd2boxzf2cinfoz20_bglt) COBJECT(BgL_auxz00_13217))->
					BgL_approxz00);
			}
		}

	}



/* &make-box/Cinfo-approx */
	BgL_approxz00_bglt BGl_z62makezd2boxzf2Cinfozd2approxz90zzcfa_approxz00(obj_t
		BgL_envz00_8337, obj_t BgL_oz00_8338)
	{
		{	/* Cfa/cinfo.sch 1203 */
			return
				BGl_makezd2boxzf2Cinfozd2approxzf2zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8338));
		}

	}



/* make-box/Cinfo-stackable */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2boxzf2Cinfozd2stackablezf2zzcfa_approxz00(BgL_makezd2boxzd2_bglt
		BgL_oz00_888)
	{
		{	/* Cfa/cinfo.sch 1205 */
			return
				(((BgL_makezd2boxzd2_bglt) COBJECT(
						((BgL_makezd2boxzd2_bglt) BgL_oz00_888)))->BgL_stackablez00);
		}

	}



/* &make-box/Cinfo-stackable */
	obj_t BGl_z62makezd2boxzf2Cinfozd2stackablez90zzcfa_approxz00(obj_t
		BgL_envz00_8339, obj_t BgL_oz00_8340)
	{
		{	/* Cfa/cinfo.sch 1205 */
			return
				BGl_makezd2boxzf2Cinfozd2stackablezf2zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8340));
		}

	}



/* make-box/Cinfo-stackable-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2boxzf2Cinfozd2stackablezd2setz12z32zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt BgL_oz00_889, obj_t BgL_vz00_890)
	{
		{	/* Cfa/cinfo.sch 1206 */
			return
				((((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_oz00_889)))->BgL_stackablez00) =
				((obj_t) BgL_vz00_890), BUNSPEC);
		}

	}



/* &make-box/Cinfo-stackable-set! */
	obj_t BGl_z62makezd2boxzf2Cinfozd2stackablezd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_8341, obj_t BgL_oz00_8342, obj_t BgL_vz00_8343)
	{
		{	/* Cfa/cinfo.sch 1206 */
			return
				BGl_makezd2boxzf2Cinfozd2stackablezd2setz12z32zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8342), BgL_vz00_8343);
		}

	}



/* make-box/Cinfo-vtype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_makezd2boxzf2Cinfozd2vtypezf2zzcfa_approxz00(BgL_makezd2boxzd2_bglt
		BgL_oz00_891)
	{
		{	/* Cfa/cinfo.sch 1207 */
			return
				(((BgL_makezd2boxzd2_bglt) COBJECT(
						((BgL_makezd2boxzd2_bglt) BgL_oz00_891)))->BgL_vtypez00);
		}

	}



/* &make-box/Cinfo-vtype */
	BgL_typez00_bglt BGl_z62makezd2boxzf2Cinfozd2vtypez90zzcfa_approxz00(obj_t
		BgL_envz00_8344, obj_t BgL_oz00_8345)
	{
		{	/* Cfa/cinfo.sch 1207 */
			return
				BGl_makezd2boxzf2Cinfozd2vtypezf2zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8345));
		}

	}



/* make-box/Cinfo-vtype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2boxzf2Cinfozd2vtypezd2setz12z32zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt BgL_oz00_892, BgL_typez00_bglt BgL_vz00_893)
	{
		{	/* Cfa/cinfo.sch 1208 */
			return
				((((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_oz00_892)))->BgL_vtypez00) =
				((BgL_typez00_bglt) BgL_vz00_893), BUNSPEC);
		}

	}



/* &make-box/Cinfo-vtype-set! */
	obj_t BGl_z62makezd2boxzf2Cinfozd2vtypezd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_8346, obj_t BgL_oz00_8347, obj_t BgL_vz00_8348)
	{
		{	/* Cfa/cinfo.sch 1208 */
			return
				BGl_makezd2boxzf2Cinfozd2vtypezd2setz12z32zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8347),
				((BgL_typez00_bglt) BgL_vz00_8348));
		}

	}



/* make-box/Cinfo-value */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_makezd2boxzf2Cinfozd2valuezf2zzcfa_approxz00(BgL_makezd2boxzd2_bglt
		BgL_oz00_894)
	{
		{	/* Cfa/cinfo.sch 1209 */
			return
				(((BgL_makezd2boxzd2_bglt) COBJECT(
						((BgL_makezd2boxzd2_bglt) BgL_oz00_894)))->BgL_valuez00);
		}

	}



/* &make-box/Cinfo-value */
	BgL_nodez00_bglt BGl_z62makezd2boxzf2Cinfozd2valuez90zzcfa_approxz00(obj_t
		BgL_envz00_8349, obj_t BgL_oz00_8350)
	{
		{	/* Cfa/cinfo.sch 1209 */
			return
				BGl_makezd2boxzf2Cinfozd2valuezf2zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8350));
		}

	}



/* make-box/Cinfo-value-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2boxzf2Cinfozd2valuezd2setz12z32zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt BgL_oz00_895, BgL_nodez00_bglt BgL_vz00_896)
	{
		{	/* Cfa/cinfo.sch 1210 */
			return
				((((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_oz00_895)))->BgL_valuez00) =
				((BgL_nodez00_bglt) BgL_vz00_896), BUNSPEC);
		}

	}



/* &make-box/Cinfo-value-set! */
	obj_t BGl_z62makezd2boxzf2Cinfozd2valuezd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_8351, obj_t BgL_oz00_8352, obj_t BgL_vz00_8353)
	{
		{	/* Cfa/cinfo.sch 1210 */
			return
				BGl_makezd2boxzf2Cinfozd2valuezd2setz12z32zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8352),
				((BgL_nodez00_bglt) BgL_vz00_8353));
		}

	}



/* make-box/Cinfo-key */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2boxzf2Cinfozd2keyzf2zzcfa_approxz00(BgL_makezd2boxzd2_bglt
		BgL_oz00_897)
	{
		{	/* Cfa/cinfo.sch 1211 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_897)))->BgL_keyz00);
		}

	}



/* &make-box/Cinfo-key */
	obj_t BGl_z62makezd2boxzf2Cinfozd2keyz90zzcfa_approxz00(obj_t BgL_envz00_8354,
		obj_t BgL_oz00_8355)
	{
		{	/* Cfa/cinfo.sch 1211 */
			return
				BGl_makezd2boxzf2Cinfozd2keyzf2zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8355));
		}

	}



/* make-box/Cinfo-key-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2boxzf2Cinfozd2keyzd2setz12z32zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt BgL_oz00_898, obj_t BgL_vz00_899)
	{
		{	/* Cfa/cinfo.sch 1212 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_898)))->BgL_keyz00) =
				((obj_t) BgL_vz00_899), BUNSPEC);
		}

	}



/* &make-box/Cinfo-key-set! */
	obj_t BGl_z62makezd2boxzf2Cinfozd2keyzd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_8356, obj_t BgL_oz00_8357, obj_t BgL_vz00_8358)
	{
		{	/* Cfa/cinfo.sch 1212 */
			return
				BGl_makezd2boxzf2Cinfozd2keyzd2setz12z32zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8357), BgL_vz00_8358);
		}

	}



/* make-box/Cinfo-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2boxzf2Cinfozd2sidezd2effectz20zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt BgL_oz00_900)
	{
		{	/* Cfa/cinfo.sch 1213 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_900)))->BgL_sidezd2effectzd2);
		}

	}



/* &make-box/Cinfo-side-effect */
	obj_t BGl_z62makezd2boxzf2Cinfozd2sidezd2effectz42zzcfa_approxz00(obj_t
		BgL_envz00_8359, obj_t BgL_oz00_8360)
	{
		{	/* Cfa/cinfo.sch 1213 */
			return
				BGl_makezd2boxzf2Cinfozd2sidezd2effectz20zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8360));
		}

	}



/* make-box/Cinfo-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2boxzf2Cinfozd2sidezd2effectzd2setz12ze0zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt BgL_oz00_901, obj_t BgL_vz00_902)
	{
		{	/* Cfa/cinfo.sch 1214 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_901)))->
					BgL_sidezd2effectzd2) = ((obj_t) BgL_vz00_902), BUNSPEC);
		}

	}



/* &make-box/Cinfo-side-effect-set! */
	obj_t
		BGl_z62makezd2boxzf2Cinfozd2sidezd2effectzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8361, obj_t BgL_oz00_8362, obj_t BgL_vz00_8363)
	{
		{	/* Cfa/cinfo.sch 1214 */
			return
				BGl_makezd2boxzf2Cinfozd2sidezd2effectzd2setz12ze0zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8362), BgL_vz00_8363);
		}

	}



/* make-box/Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_makezd2boxzf2Cinfozd2typezf2zzcfa_approxz00(BgL_makezd2boxzd2_bglt
		BgL_oz00_903)
	{
		{	/* Cfa/cinfo.sch 1215 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_903)))->BgL_typez00);
		}

	}



/* &make-box/Cinfo-type */
	BgL_typez00_bglt BGl_z62makezd2boxzf2Cinfozd2typez90zzcfa_approxz00(obj_t
		BgL_envz00_8364, obj_t BgL_oz00_8365)
	{
		{	/* Cfa/cinfo.sch 1215 */
			return
				BGl_makezd2boxzf2Cinfozd2typezf2zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8365));
		}

	}



/* make-box/Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2boxzf2Cinfozd2typezd2setz12z32zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt BgL_oz00_904, BgL_typez00_bglt BgL_vz00_905)
	{
		{	/* Cfa/cinfo.sch 1216 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_904)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_905), BUNSPEC);
		}

	}



/* &make-box/Cinfo-type-set! */
	obj_t BGl_z62makezd2boxzf2Cinfozd2typezd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_8366, obj_t BgL_oz00_8367, obj_t BgL_vz00_8368)
	{
		{	/* Cfa/cinfo.sch 1216 */
			return
				BGl_makezd2boxzf2Cinfozd2typezd2setz12z32zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8367),
				((BgL_typez00_bglt) BgL_vz00_8368));
		}

	}



/* make-box/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2boxzf2Cinfozd2loczf2zzcfa_approxz00(BgL_makezd2boxzd2_bglt
		BgL_oz00_906)
	{
		{	/* Cfa/cinfo.sch 1217 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_906)))->BgL_locz00);
		}

	}



/* &make-box/Cinfo-loc */
	obj_t BGl_z62makezd2boxzf2Cinfozd2locz90zzcfa_approxz00(obj_t BgL_envz00_8369,
		obj_t BgL_oz00_8370)
	{
		{	/* Cfa/cinfo.sch 1217 */
			return
				BGl_makezd2boxzf2Cinfozd2loczf2zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8370));
		}

	}



/* make-make-box/O-Cinfo */
	BGL_EXPORTED_DEF BgL_makezd2boxzd2_bglt
		BGl_makezd2makezd2boxzf2Ozd2Cinfoz20zzcfa_approxz00(obj_t
		BgL_loc1568z00_909, BgL_typez00_bglt BgL_type1569z00_910,
		obj_t BgL_sidezd2effect1570zd2_911, obj_t BgL_key1571z00_912,
		BgL_nodez00_bglt BgL_value1572z00_913,
		BgL_typez00_bglt BgL_vtype1574z00_914, obj_t BgL_stackable1575z00_915,
		BgL_approxz00_bglt BgL_approx1576z00_916,
		BgL_approxz00_bglt BgL_valuezd2approx1577zd2_917)
	{
		{	/* Cfa/cinfo.sch 1221 */
			{	/* Cfa/cinfo.sch 1221 */
				BgL_makezd2boxzd2_bglt BgL_new1284z00_9200;

				{	/* Cfa/cinfo.sch 1221 */
					BgL_makezd2boxzd2_bglt BgL_tmp1282z00_9201;
					BgL_makezd2boxzf2ozd2cinfozf2_bglt BgL_wide1283z00_9202;

					{
						BgL_makezd2boxzd2_bglt BgL_auxz00_13281;

						{	/* Cfa/cinfo.sch 1221 */
							BgL_makezd2boxzd2_bglt BgL_new1281z00_9203;

							BgL_new1281z00_9203 =
								((BgL_makezd2boxzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_makezd2boxzd2_bgl))));
							{	/* Cfa/cinfo.sch 1221 */
								long BgL_arg1761z00_9204;

								BgL_arg1761z00_9204 =
									BGL_CLASS_NUM(BGl_makezd2boxzd2zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1281z00_9203),
									BgL_arg1761z00_9204);
							}
							{	/* Cfa/cinfo.sch 1221 */
								BgL_objectz00_bglt BgL_tmpz00_13286;

								BgL_tmpz00_13286 = ((BgL_objectz00_bglt) BgL_new1281z00_9203);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13286, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1281z00_9203);
							BgL_auxz00_13281 = BgL_new1281z00_9203;
						}
						BgL_tmp1282z00_9201 = ((BgL_makezd2boxzd2_bglt) BgL_auxz00_13281);
					}
					BgL_wide1283z00_9202 =
						((BgL_makezd2boxzf2ozd2cinfozf2_bglt)
						BOBJECT(GC_MALLOC(sizeof(struct
									BgL_makezd2boxzf2ozd2cinfozf2_bgl))));
					{	/* Cfa/cinfo.sch 1221 */
						obj_t BgL_auxz00_13294;
						BgL_objectz00_bglt BgL_tmpz00_13292;

						BgL_auxz00_13294 = ((obj_t) BgL_wide1283z00_9202);
						BgL_tmpz00_13292 = ((BgL_objectz00_bglt) BgL_tmp1282z00_9201);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13292, BgL_auxz00_13294);
					}
					((BgL_objectz00_bglt) BgL_tmp1282z00_9201);
					{	/* Cfa/cinfo.sch 1221 */
						long BgL_arg1755z00_9205;

						BgL_arg1755z00_9205 =
							BGL_CLASS_NUM(BGl_makezd2boxzf2Ozd2Cinfozf2zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1282z00_9201), BgL_arg1755z00_9205);
					}
					BgL_new1284z00_9200 = ((BgL_makezd2boxzd2_bglt) BgL_tmp1282z00_9201);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1284z00_9200)))->BgL_locz00) =
					((obj_t) BgL_loc1568z00_909), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1284z00_9200)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1569z00_910), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1284z00_9200)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1570zd2_911), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1284z00_9200)))->BgL_keyz00) =
					((obj_t) BgL_key1571z00_912), BUNSPEC);
				((((BgL_makezd2boxzd2_bglt) COBJECT(((BgL_makezd2boxzd2_bglt)
									BgL_new1284z00_9200)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_value1572z00_913), BUNSPEC);
				((((BgL_makezd2boxzd2_bglt) COBJECT(((BgL_makezd2boxzd2_bglt)
									BgL_new1284z00_9200)))->BgL_vtypez00) =
					((BgL_typez00_bglt) BgL_vtype1574z00_914), BUNSPEC);
				((((BgL_makezd2boxzd2_bglt) COBJECT(((BgL_makezd2boxzd2_bglt)
									BgL_new1284z00_9200)))->BgL_stackablez00) =
					((obj_t) BgL_stackable1575z00_915), BUNSPEC);
				{
					BgL_makezd2boxzf2ozd2cinfozf2_bglt BgL_auxz00_13316;

					{
						obj_t BgL_auxz00_13317;

						{	/* Cfa/cinfo.sch 1221 */
							BgL_objectz00_bglt BgL_tmpz00_13318;

							BgL_tmpz00_13318 = ((BgL_objectz00_bglt) BgL_new1284z00_9200);
							BgL_auxz00_13317 = BGL_OBJECT_WIDENING(BgL_tmpz00_13318);
						}
						BgL_auxz00_13316 =
							((BgL_makezd2boxzf2ozd2cinfozf2_bglt) BgL_auxz00_13317);
					}
					((((BgL_makezd2boxzf2ozd2cinfozf2_bglt) COBJECT(BgL_auxz00_13316))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1576z00_916), BUNSPEC);
				}
				{
					BgL_makezd2boxzf2ozd2cinfozf2_bglt BgL_auxz00_13323;

					{
						obj_t BgL_auxz00_13324;

						{	/* Cfa/cinfo.sch 1221 */
							BgL_objectz00_bglt BgL_tmpz00_13325;

							BgL_tmpz00_13325 = ((BgL_objectz00_bglt) BgL_new1284z00_9200);
							BgL_auxz00_13324 = BGL_OBJECT_WIDENING(BgL_tmpz00_13325);
						}
						BgL_auxz00_13323 =
							((BgL_makezd2boxzf2ozd2cinfozf2_bglt) BgL_auxz00_13324);
					}
					((((BgL_makezd2boxzf2ozd2cinfozf2_bglt) COBJECT(BgL_auxz00_13323))->
							BgL_valuezd2approxzd2) =
						((BgL_approxz00_bglt) BgL_valuezd2approx1577zd2_917), BUNSPEC);
				}
				return BgL_new1284z00_9200;
			}
		}

	}



/* &make-make-box/O-Cinfo */
	BgL_makezd2boxzd2_bglt
		BGl_z62makezd2makezd2boxzf2Ozd2Cinfoz42zzcfa_approxz00(obj_t
		BgL_envz00_8371, obj_t BgL_loc1568z00_8372, obj_t BgL_type1569z00_8373,
		obj_t BgL_sidezd2effect1570zd2_8374, obj_t BgL_key1571z00_8375,
		obj_t BgL_value1572z00_8376, obj_t BgL_vtype1574z00_8377,
		obj_t BgL_stackable1575z00_8378, obj_t BgL_approx1576z00_8379,
		obj_t BgL_valuezd2approx1577zd2_8380)
	{
		{	/* Cfa/cinfo.sch 1221 */
			return
				BGl_makezd2makezd2boxzf2Ozd2Cinfoz20zzcfa_approxz00(BgL_loc1568z00_8372,
				((BgL_typez00_bglt) BgL_type1569z00_8373),
				BgL_sidezd2effect1570zd2_8374, BgL_key1571z00_8375,
				((BgL_nodez00_bglt) BgL_value1572z00_8376),
				((BgL_typez00_bglt) BgL_vtype1574z00_8377), BgL_stackable1575z00_8378,
				((BgL_approxz00_bglt) BgL_approx1576z00_8379),
				((BgL_approxz00_bglt) BgL_valuezd2approx1577zd2_8380));
		}

	}



/* make-box/O-Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_makezd2boxzf2Ozd2Cinfozf3z01zzcfa_approxz00(obj_t
		BgL_objz00_918)
	{
		{	/* Cfa/cinfo.sch 1222 */
			{	/* Cfa/cinfo.sch 1222 */
				obj_t BgL_classz00_9206;

				BgL_classz00_9206 = BGl_makezd2boxzf2Ozd2Cinfozf2zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_918))
					{	/* Cfa/cinfo.sch 1222 */
						BgL_objectz00_bglt BgL_arg1807z00_9207;

						BgL_arg1807z00_9207 = (BgL_objectz00_bglt) (BgL_objz00_918);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 1222 */
								long BgL_idxz00_9208;

								BgL_idxz00_9208 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9207);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9208 + 4L)) == BgL_classz00_9206);
							}
						else
							{	/* Cfa/cinfo.sch 1222 */
								bool_t BgL_res2193z00_9211;

								{	/* Cfa/cinfo.sch 1222 */
									obj_t BgL_oclassz00_9212;

									{	/* Cfa/cinfo.sch 1222 */
										obj_t BgL_arg1815z00_9213;
										long BgL_arg1816z00_9214;

										BgL_arg1815z00_9213 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 1222 */
											long BgL_arg1817z00_9215;

											BgL_arg1817z00_9215 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9207);
											BgL_arg1816z00_9214 = (BgL_arg1817z00_9215 - OBJECT_TYPE);
										}
										BgL_oclassz00_9212 =
											VECTOR_REF(BgL_arg1815z00_9213, BgL_arg1816z00_9214);
									}
									{	/* Cfa/cinfo.sch 1222 */
										bool_t BgL__ortest_1115z00_9216;

										BgL__ortest_1115z00_9216 =
											(BgL_classz00_9206 == BgL_oclassz00_9212);
										if (BgL__ortest_1115z00_9216)
											{	/* Cfa/cinfo.sch 1222 */
												BgL_res2193z00_9211 = BgL__ortest_1115z00_9216;
											}
										else
											{	/* Cfa/cinfo.sch 1222 */
												long BgL_odepthz00_9217;

												{	/* Cfa/cinfo.sch 1222 */
													obj_t BgL_arg1804z00_9218;

													BgL_arg1804z00_9218 = (BgL_oclassz00_9212);
													BgL_odepthz00_9217 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9218);
												}
												if ((4L < BgL_odepthz00_9217))
													{	/* Cfa/cinfo.sch 1222 */
														obj_t BgL_arg1802z00_9219;

														{	/* Cfa/cinfo.sch 1222 */
															obj_t BgL_arg1803z00_9220;

															BgL_arg1803z00_9220 = (BgL_oclassz00_9212);
															BgL_arg1802z00_9219 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9220,
																4L);
														}
														BgL_res2193z00_9211 =
															(BgL_arg1802z00_9219 == BgL_classz00_9206);
													}
												else
													{	/* Cfa/cinfo.sch 1222 */
														BgL_res2193z00_9211 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2193z00_9211;
							}
					}
				else
					{	/* Cfa/cinfo.sch 1222 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &make-box/O-Cinfo? */
	obj_t BGl_z62makezd2boxzf2Ozd2Cinfozf3z63zzcfa_approxz00(obj_t
		BgL_envz00_8381, obj_t BgL_objz00_8382)
	{
		{	/* Cfa/cinfo.sch 1222 */
			return
				BBOOL(BGl_makezd2boxzf2Ozd2Cinfozf3z01zzcfa_approxz00(BgL_objz00_8382));
		}

	}



/* make-box/O-Cinfo-nil */
	BGL_EXPORTED_DEF BgL_makezd2boxzd2_bglt
		BGl_makezd2boxzf2Ozd2Cinfozd2nilz20zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 1223 */
			{	/* Cfa/cinfo.sch 1223 */
				obj_t BgL_classz00_6110;

				BgL_classz00_6110 = BGl_makezd2boxzf2Ozd2Cinfozf2zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 1223 */
					obj_t BgL__ortest_1117z00_6111;

					BgL__ortest_1117z00_6111 = BGL_CLASS_NIL(BgL_classz00_6110);
					if (CBOOL(BgL__ortest_1117z00_6111))
						{	/* Cfa/cinfo.sch 1223 */
							return ((BgL_makezd2boxzd2_bglt) BgL__ortest_1117z00_6111);
						}
					else
						{	/* Cfa/cinfo.sch 1223 */
							return
								((BgL_makezd2boxzd2_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_6110));
						}
				}
			}
		}

	}



/* &make-box/O-Cinfo-nil */
	BgL_makezd2boxzd2_bglt
		BGl_z62makezd2boxzf2Ozd2Cinfozd2nilz42zzcfa_approxz00(obj_t BgL_envz00_8383)
	{
		{	/* Cfa/cinfo.sch 1223 */
			return BGl_makezd2boxzf2Ozd2Cinfozd2nilz20zzcfa_approxz00();
		}

	}



/* make-box/O-Cinfo-value-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_makezd2boxzf2Ozd2Cinfozd2valuezd2approxzf2zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt BgL_oz00_919)
	{
		{	/* Cfa/cinfo.sch 1224 */
			{
				BgL_makezd2boxzf2ozd2cinfozf2_bglt BgL_auxz00_13367;

				{
					obj_t BgL_auxz00_13368;

					{	/* Cfa/cinfo.sch 1224 */
						BgL_objectz00_bglt BgL_tmpz00_13369;

						BgL_tmpz00_13369 = ((BgL_objectz00_bglt) BgL_oz00_919);
						BgL_auxz00_13368 = BGL_OBJECT_WIDENING(BgL_tmpz00_13369);
					}
					BgL_auxz00_13367 =
						((BgL_makezd2boxzf2ozd2cinfozf2_bglt) BgL_auxz00_13368);
				}
				return
					(((BgL_makezd2boxzf2ozd2cinfozf2_bglt) COBJECT(BgL_auxz00_13367))->
					BgL_valuezd2approxzd2);
			}
		}

	}



/* &make-box/O-Cinfo-value-approx */
	BgL_approxz00_bglt
		BGl_z62makezd2boxzf2Ozd2Cinfozd2valuezd2approxz90zzcfa_approxz00(obj_t
		BgL_envz00_8384, obj_t BgL_oz00_8385)
	{
		{	/* Cfa/cinfo.sch 1224 */
			return
				BGl_makezd2boxzf2Ozd2Cinfozd2valuezd2approxzf2zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8385));
		}

	}



/* make-box/O-Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_makezd2boxzf2Ozd2Cinfozd2approxz20zzcfa_approxz00(BgL_makezd2boxzd2_bglt
		BgL_oz00_922)
	{
		{	/* Cfa/cinfo.sch 1226 */
			{
				BgL_makezd2boxzf2ozd2cinfozf2_bglt BgL_auxz00_13376;

				{
					obj_t BgL_auxz00_13377;

					{	/* Cfa/cinfo.sch 1226 */
						BgL_objectz00_bglt BgL_tmpz00_13378;

						BgL_tmpz00_13378 = ((BgL_objectz00_bglt) BgL_oz00_922);
						BgL_auxz00_13377 = BGL_OBJECT_WIDENING(BgL_tmpz00_13378);
					}
					BgL_auxz00_13376 =
						((BgL_makezd2boxzf2ozd2cinfozf2_bglt) BgL_auxz00_13377);
				}
				return
					(((BgL_makezd2boxzf2ozd2cinfozf2_bglt) COBJECT(BgL_auxz00_13376))->
					BgL_approxz00);
			}
		}

	}



/* &make-box/O-Cinfo-approx */
	BgL_approxz00_bglt
		BGl_z62makezd2boxzf2Ozd2Cinfozd2approxz42zzcfa_approxz00(obj_t
		BgL_envz00_8386, obj_t BgL_oz00_8387)
	{
		{	/* Cfa/cinfo.sch 1226 */
			return
				BGl_makezd2boxzf2Ozd2Cinfozd2approxz20zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8387));
		}

	}



/* make-box/O-Cinfo-approx-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2boxzf2Ozd2Cinfozd2approxzd2setz12ze0zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt BgL_oz00_923, BgL_approxz00_bglt BgL_vz00_924)
	{
		{	/* Cfa/cinfo.sch 1227 */
			{
				BgL_makezd2boxzf2ozd2cinfozf2_bglt BgL_auxz00_13385;

				{
					obj_t BgL_auxz00_13386;

					{	/* Cfa/cinfo.sch 1227 */
						BgL_objectz00_bglt BgL_tmpz00_13387;

						BgL_tmpz00_13387 = ((BgL_objectz00_bglt) BgL_oz00_923);
						BgL_auxz00_13386 = BGL_OBJECT_WIDENING(BgL_tmpz00_13387);
					}
					BgL_auxz00_13385 =
						((BgL_makezd2boxzf2ozd2cinfozf2_bglt) BgL_auxz00_13386);
				}
				return
					((((BgL_makezd2boxzf2ozd2cinfozf2_bglt) COBJECT(BgL_auxz00_13385))->
						BgL_approxz00) = ((BgL_approxz00_bglt) BgL_vz00_924), BUNSPEC);
			}
		}

	}



/* &make-box/O-Cinfo-approx-set! */
	obj_t BGl_z62makezd2boxzf2Ozd2Cinfozd2approxzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8388, obj_t BgL_oz00_8389, obj_t BgL_vz00_8390)
	{
		{	/* Cfa/cinfo.sch 1227 */
			return
				BGl_makezd2boxzf2Ozd2Cinfozd2approxzd2setz12ze0zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8389),
				((BgL_approxz00_bglt) BgL_vz00_8390));
		}

	}



/* make-box/O-Cinfo-stackable */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2boxzf2Ozd2Cinfozd2stackablez20zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt BgL_oz00_925)
	{
		{	/* Cfa/cinfo.sch 1228 */
			return
				(((BgL_makezd2boxzd2_bglt) COBJECT(
						((BgL_makezd2boxzd2_bglt) BgL_oz00_925)))->BgL_stackablez00);
		}

	}



/* &make-box/O-Cinfo-stackable */
	obj_t BGl_z62makezd2boxzf2Ozd2Cinfozd2stackablez42zzcfa_approxz00(obj_t
		BgL_envz00_8391, obj_t BgL_oz00_8392)
	{
		{	/* Cfa/cinfo.sch 1228 */
			return
				BGl_makezd2boxzf2Ozd2Cinfozd2stackablez20zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8392));
		}

	}



/* make-box/O-Cinfo-stackable-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2boxzf2Ozd2Cinfozd2stackablezd2setz12ze0zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt BgL_oz00_926, obj_t BgL_vz00_927)
	{
		{	/* Cfa/cinfo.sch 1229 */
			return
				((((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_oz00_926)))->BgL_stackablez00) =
				((obj_t) BgL_vz00_927), BUNSPEC);
		}

	}



/* &make-box/O-Cinfo-stackable-set! */
	obj_t
		BGl_z62makezd2boxzf2Ozd2Cinfozd2stackablezd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8393, obj_t BgL_oz00_8394, obj_t BgL_vz00_8395)
	{
		{	/* Cfa/cinfo.sch 1229 */
			return
				BGl_makezd2boxzf2Ozd2Cinfozd2stackablezd2setz12ze0zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8394), BgL_vz00_8395);
		}

	}



/* make-box/O-Cinfo-vtype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_makezd2boxzf2Ozd2Cinfozd2vtypez20zzcfa_approxz00(BgL_makezd2boxzd2_bglt
		BgL_oz00_928)
	{
		{	/* Cfa/cinfo.sch 1230 */
			return
				(((BgL_makezd2boxzd2_bglt) COBJECT(
						((BgL_makezd2boxzd2_bglt) BgL_oz00_928)))->BgL_vtypez00);
		}

	}



/* &make-box/O-Cinfo-vtype */
	BgL_typez00_bglt BGl_z62makezd2boxzf2Ozd2Cinfozd2vtypez42zzcfa_approxz00(obj_t
		BgL_envz00_8396, obj_t BgL_oz00_8397)
	{
		{	/* Cfa/cinfo.sch 1230 */
			return
				BGl_makezd2boxzf2Ozd2Cinfozd2vtypez20zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8397));
		}

	}



/* make-box/O-Cinfo-vtype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2boxzf2Ozd2Cinfozd2vtypezd2setz12ze0zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt BgL_oz00_929, BgL_typez00_bglt BgL_vz00_930)
	{
		{	/* Cfa/cinfo.sch 1231 */
			return
				((((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_oz00_929)))->BgL_vtypez00) =
				((BgL_typez00_bglt) BgL_vz00_930), BUNSPEC);
		}

	}



/* &make-box/O-Cinfo-vtype-set! */
	obj_t BGl_z62makezd2boxzf2Ozd2Cinfozd2vtypezd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8398, obj_t BgL_oz00_8399, obj_t BgL_vz00_8400)
	{
		{	/* Cfa/cinfo.sch 1231 */
			return
				BGl_makezd2boxzf2Ozd2Cinfozd2vtypezd2setz12ze0zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8399),
				((BgL_typez00_bglt) BgL_vz00_8400));
		}

	}



/* make-box/O-Cinfo-value */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_makezd2boxzf2Ozd2Cinfozd2valuez20zzcfa_approxz00(BgL_makezd2boxzd2_bglt
		BgL_oz00_931)
	{
		{	/* Cfa/cinfo.sch 1232 */
			return
				(((BgL_makezd2boxzd2_bglt) COBJECT(
						((BgL_makezd2boxzd2_bglt) BgL_oz00_931)))->BgL_valuez00);
		}

	}



/* &make-box/O-Cinfo-value */
	BgL_nodez00_bglt BGl_z62makezd2boxzf2Ozd2Cinfozd2valuez42zzcfa_approxz00(obj_t
		BgL_envz00_8401, obj_t BgL_oz00_8402)
	{
		{	/* Cfa/cinfo.sch 1232 */
			return
				BGl_makezd2boxzf2Ozd2Cinfozd2valuez20zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8402));
		}

	}



/* make-box/O-Cinfo-value-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2boxzf2Ozd2Cinfozd2valuezd2setz12ze0zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt BgL_oz00_932, BgL_nodez00_bglt BgL_vz00_933)
	{
		{	/* Cfa/cinfo.sch 1233 */
			return
				((((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_oz00_932)))->BgL_valuez00) =
				((BgL_nodez00_bglt) BgL_vz00_933), BUNSPEC);
		}

	}



/* &make-box/O-Cinfo-value-set! */
	obj_t BGl_z62makezd2boxzf2Ozd2Cinfozd2valuezd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8403, obj_t BgL_oz00_8404, obj_t BgL_vz00_8405)
	{
		{	/* Cfa/cinfo.sch 1233 */
			return
				BGl_makezd2boxzf2Ozd2Cinfozd2valuezd2setz12ze0zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8404),
				((BgL_nodez00_bglt) BgL_vz00_8405));
		}

	}



/* make-box/O-Cinfo-key */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2boxzf2Ozd2Cinfozd2keyz20zzcfa_approxz00(BgL_makezd2boxzd2_bglt
		BgL_oz00_934)
	{
		{	/* Cfa/cinfo.sch 1234 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_934)))->BgL_keyz00);
		}

	}



/* &make-box/O-Cinfo-key */
	obj_t BGl_z62makezd2boxzf2Ozd2Cinfozd2keyz42zzcfa_approxz00(obj_t
		BgL_envz00_8406, obj_t BgL_oz00_8407)
	{
		{	/* Cfa/cinfo.sch 1234 */
			return
				BGl_makezd2boxzf2Ozd2Cinfozd2keyz20zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8407));
		}

	}



/* make-box/O-Cinfo-key-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2boxzf2Ozd2Cinfozd2keyzd2setz12ze0zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt BgL_oz00_935, obj_t BgL_vz00_936)
	{
		{	/* Cfa/cinfo.sch 1235 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_935)))->BgL_keyz00) =
				((obj_t) BgL_vz00_936), BUNSPEC);
		}

	}



/* &make-box/O-Cinfo-key-set! */
	obj_t BGl_z62makezd2boxzf2Ozd2Cinfozd2keyzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8408, obj_t BgL_oz00_8409, obj_t BgL_vz00_8410)
	{
		{	/* Cfa/cinfo.sch 1235 */
			return
				BGl_makezd2boxzf2Ozd2Cinfozd2keyzd2setz12ze0zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8409), BgL_vz00_8410);
		}

	}



/* make-box/O-Cinfo-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2boxzf2Ozd2Cinfozd2sidezd2effectzf2zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt BgL_oz00_937)
	{
		{	/* Cfa/cinfo.sch 1236 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_937)))->BgL_sidezd2effectzd2);
		}

	}



/* &make-box/O-Cinfo-side-effect */
	obj_t BGl_z62makezd2boxzf2Ozd2Cinfozd2sidezd2effectz90zzcfa_approxz00(obj_t
		BgL_envz00_8411, obj_t BgL_oz00_8412)
	{
		{	/* Cfa/cinfo.sch 1236 */
			return
				BGl_makezd2boxzf2Ozd2Cinfozd2sidezd2effectzf2zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8412));
		}

	}



/* make-box/O-Cinfo-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2boxzf2Ozd2Cinfozd2sidezd2effectzd2setz12z32zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt BgL_oz00_938, obj_t BgL_vz00_939)
	{
		{	/* Cfa/cinfo.sch 1237 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_938)))->
					BgL_sidezd2effectzd2) = ((obj_t) BgL_vz00_939), BUNSPEC);
		}

	}



/* &make-box/O-Cinfo-side-effect-set! */
	obj_t
		BGl_z62makezd2boxzf2Ozd2Cinfozd2sidezd2effectzd2setz12z50zzcfa_approxz00
		(obj_t BgL_envz00_8413, obj_t BgL_oz00_8414, obj_t BgL_vz00_8415)
	{
		{	/* Cfa/cinfo.sch 1237 */
			return
				BGl_makezd2boxzf2Ozd2Cinfozd2sidezd2effectzd2setz12z32zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8414), BgL_vz00_8415);
		}

	}



/* make-box/O-Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_makezd2boxzf2Ozd2Cinfozd2typez20zzcfa_approxz00(BgL_makezd2boxzd2_bglt
		BgL_oz00_940)
	{
		{	/* Cfa/cinfo.sch 1238 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_940)))->BgL_typez00);
		}

	}



/* &make-box/O-Cinfo-type */
	BgL_typez00_bglt BGl_z62makezd2boxzf2Ozd2Cinfozd2typez42zzcfa_approxz00(obj_t
		BgL_envz00_8416, obj_t BgL_oz00_8417)
	{
		{	/* Cfa/cinfo.sch 1238 */
			return
				BGl_makezd2boxzf2Ozd2Cinfozd2typez20zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8417));
		}

	}



/* make-box/O-Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2boxzf2Ozd2Cinfozd2typezd2setz12ze0zzcfa_approxz00
		(BgL_makezd2boxzd2_bglt BgL_oz00_941, BgL_typez00_bglt BgL_vz00_942)
	{
		{	/* Cfa/cinfo.sch 1239 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_941)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_942), BUNSPEC);
		}

	}



/* &make-box/O-Cinfo-type-set! */
	obj_t BGl_z62makezd2boxzf2Ozd2Cinfozd2typezd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8418, obj_t BgL_oz00_8419, obj_t BgL_vz00_8420)
	{
		{	/* Cfa/cinfo.sch 1239 */
			return
				BGl_makezd2boxzf2Ozd2Cinfozd2typezd2setz12ze0zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8419),
				((BgL_typez00_bglt) BgL_vz00_8420));
		}

	}



/* make-box/O-Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2boxzf2Ozd2Cinfozd2locz20zzcfa_approxz00(BgL_makezd2boxzd2_bglt
		BgL_oz00_943)
	{
		{	/* Cfa/cinfo.sch 1240 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_943)))->BgL_locz00);
		}

	}



/* &make-box/O-Cinfo-loc */
	obj_t BGl_z62makezd2boxzf2Ozd2Cinfozd2locz42zzcfa_approxz00(obj_t
		BgL_envz00_8421, obj_t BgL_oz00_8422)
	{
		{	/* Cfa/cinfo.sch 1240 */
			return
				BGl_makezd2boxzf2Ozd2Cinfozd2locz20zzcfa_approxz00(
				((BgL_makezd2boxzd2_bglt) BgL_oz00_8422));
		}

	}



/* make-box-set!/Cinfo */
	BGL_EXPORTED_DEF BgL_boxzd2setz12zc0_bglt
		BGl_makezd2boxzd2setz12zf2Cinfoze0zzcfa_approxz00(obj_t BgL_loc1561z00_946,
		BgL_typez00_bglt BgL_type1562z00_947, BgL_varz00_bglt BgL_var1563z00_948,
		BgL_nodez00_bglt BgL_value1564z00_949,
		BgL_typez00_bglt BgL_vtype1565z00_950,
		BgL_approxz00_bglt BgL_approx1566z00_951)
	{
		{	/* Cfa/cinfo.sch 1244 */
			{	/* Cfa/cinfo.sch 1244 */
				BgL_boxzd2setz12zc0_bglt BgL_new1288z00_9221;

				{	/* Cfa/cinfo.sch 1244 */
					BgL_boxzd2setz12zc0_bglt BgL_tmp1286z00_9222;
					BgL_boxzd2setz12zf2cinfoz32_bglt BgL_wide1287z00_9223;

					{
						BgL_boxzd2setz12zc0_bglt BgL_auxz00_13450;

						{	/* Cfa/cinfo.sch 1244 */
							BgL_boxzd2setz12zc0_bglt BgL_new1285z00_9224;

							BgL_new1285z00_9224 =
								((BgL_boxzd2setz12zc0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_boxzd2setz12zc0_bgl))));
							{	/* Cfa/cinfo.sch 1244 */
								long BgL_arg1765z00_9225;

								BgL_arg1765z00_9225 =
									BGL_CLASS_NUM(BGl_boxzd2setz12zc0zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1285z00_9224),
									BgL_arg1765z00_9225);
							}
							{	/* Cfa/cinfo.sch 1244 */
								BgL_objectz00_bglt BgL_tmpz00_13455;

								BgL_tmpz00_13455 = ((BgL_objectz00_bglt) BgL_new1285z00_9224);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13455, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1285z00_9224);
							BgL_auxz00_13450 = BgL_new1285z00_9224;
						}
						BgL_tmp1286z00_9222 = ((BgL_boxzd2setz12zc0_bglt) BgL_auxz00_13450);
					}
					BgL_wide1287z00_9223 =
						((BgL_boxzd2setz12zf2cinfoz32_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_boxzd2setz12zf2cinfoz32_bgl))));
					{	/* Cfa/cinfo.sch 1244 */
						obj_t BgL_auxz00_13463;
						BgL_objectz00_bglt BgL_tmpz00_13461;

						BgL_auxz00_13463 = ((obj_t) BgL_wide1287z00_9223);
						BgL_tmpz00_13461 = ((BgL_objectz00_bglt) BgL_tmp1286z00_9222);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13461, BgL_auxz00_13463);
					}
					((BgL_objectz00_bglt) BgL_tmp1286z00_9222);
					{	/* Cfa/cinfo.sch 1244 */
						long BgL_arg1762z00_9226;

						BgL_arg1762z00_9226 =
							BGL_CLASS_NUM(BGl_boxzd2setz12zf2Cinfoz32zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1286z00_9222), BgL_arg1762z00_9226);
					}
					BgL_new1288z00_9221 =
						((BgL_boxzd2setz12zc0_bglt) BgL_tmp1286z00_9222);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1288z00_9221)))->BgL_locz00) =
					((obj_t) BgL_loc1561z00_946), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1288z00_9221)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1562z00_947), BUNSPEC);
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(((BgL_boxzd2setz12zc0_bglt)
									BgL_new1288z00_9221)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_var1563z00_948), BUNSPEC);
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(((BgL_boxzd2setz12zc0_bglt)
									BgL_new1288z00_9221)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_value1564z00_949), BUNSPEC);
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(((BgL_boxzd2setz12zc0_bglt)
									BgL_new1288z00_9221)))->BgL_vtypez00) =
					((BgL_typez00_bglt) BgL_vtype1565z00_950), BUNSPEC);
				{
					BgL_boxzd2setz12zf2cinfoz32_bglt BgL_auxz00_13481;

					{
						obj_t BgL_auxz00_13482;

						{	/* Cfa/cinfo.sch 1244 */
							BgL_objectz00_bglt BgL_tmpz00_13483;

							BgL_tmpz00_13483 = ((BgL_objectz00_bglt) BgL_new1288z00_9221);
							BgL_auxz00_13482 = BGL_OBJECT_WIDENING(BgL_tmpz00_13483);
						}
						BgL_auxz00_13481 =
							((BgL_boxzd2setz12zf2cinfoz32_bglt) BgL_auxz00_13482);
					}
					((((BgL_boxzd2setz12zf2cinfoz32_bglt) COBJECT(BgL_auxz00_13481))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1566z00_951), BUNSPEC);
				}
				return BgL_new1288z00_9221;
			}
		}

	}



/* &make-box-set!/Cinfo */
	BgL_boxzd2setz12zc0_bglt
		BGl_z62makezd2boxzd2setz12zf2Cinfoz82zzcfa_approxz00(obj_t BgL_envz00_8423,
		obj_t BgL_loc1561z00_8424, obj_t BgL_type1562z00_8425,
		obj_t BgL_var1563z00_8426, obj_t BgL_value1564z00_8427,
		obj_t BgL_vtype1565z00_8428, obj_t BgL_approx1566z00_8429)
	{
		{	/* Cfa/cinfo.sch 1244 */
			return
				BGl_makezd2boxzd2setz12zf2Cinfoze0zzcfa_approxz00(BgL_loc1561z00_8424,
				((BgL_typez00_bglt) BgL_type1562z00_8425),
				((BgL_varz00_bglt) BgL_var1563z00_8426),
				((BgL_nodez00_bglt) BgL_value1564z00_8427),
				((BgL_typez00_bglt) BgL_vtype1565z00_8428),
				((BgL_approxz00_bglt) BgL_approx1566z00_8429));
		}

	}



/* box-set!/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_boxzd2setz12zf2Cinfozf3zc1zzcfa_approxz00(obj_t
		BgL_objz00_952)
	{
		{	/* Cfa/cinfo.sch 1245 */
			{	/* Cfa/cinfo.sch 1245 */
				obj_t BgL_classz00_9227;

				BgL_classz00_9227 = BGl_boxzd2setz12zf2Cinfoz32zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_952))
					{	/* Cfa/cinfo.sch 1245 */
						BgL_objectz00_bglt BgL_arg1807z00_9228;

						BgL_arg1807z00_9228 = (BgL_objectz00_bglt) (BgL_objz00_952);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 1245 */
								long BgL_idxz00_9229;

								BgL_idxz00_9229 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9228);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9229 + 3L)) == BgL_classz00_9227);
							}
						else
							{	/* Cfa/cinfo.sch 1245 */
								bool_t BgL_res2194z00_9232;

								{	/* Cfa/cinfo.sch 1245 */
									obj_t BgL_oclassz00_9233;

									{	/* Cfa/cinfo.sch 1245 */
										obj_t BgL_arg1815z00_9234;
										long BgL_arg1816z00_9235;

										BgL_arg1815z00_9234 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 1245 */
											long BgL_arg1817z00_9236;

											BgL_arg1817z00_9236 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9228);
											BgL_arg1816z00_9235 = (BgL_arg1817z00_9236 - OBJECT_TYPE);
										}
										BgL_oclassz00_9233 =
											VECTOR_REF(BgL_arg1815z00_9234, BgL_arg1816z00_9235);
									}
									{	/* Cfa/cinfo.sch 1245 */
										bool_t BgL__ortest_1115z00_9237;

										BgL__ortest_1115z00_9237 =
											(BgL_classz00_9227 == BgL_oclassz00_9233);
										if (BgL__ortest_1115z00_9237)
											{	/* Cfa/cinfo.sch 1245 */
												BgL_res2194z00_9232 = BgL__ortest_1115z00_9237;
											}
										else
											{	/* Cfa/cinfo.sch 1245 */
												long BgL_odepthz00_9238;

												{	/* Cfa/cinfo.sch 1245 */
													obj_t BgL_arg1804z00_9239;

													BgL_arg1804z00_9239 = (BgL_oclassz00_9233);
													BgL_odepthz00_9238 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9239);
												}
												if ((3L < BgL_odepthz00_9238))
													{	/* Cfa/cinfo.sch 1245 */
														obj_t BgL_arg1802z00_9240;

														{	/* Cfa/cinfo.sch 1245 */
															obj_t BgL_arg1803z00_9241;

															BgL_arg1803z00_9241 = (BgL_oclassz00_9233);
															BgL_arg1802z00_9240 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9241,
																3L);
														}
														BgL_res2194z00_9232 =
															(BgL_arg1802z00_9240 == BgL_classz00_9227);
													}
												else
													{	/* Cfa/cinfo.sch 1245 */
														BgL_res2194z00_9232 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2194z00_9232;
							}
					}
				else
					{	/* Cfa/cinfo.sch 1245 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &box-set!/Cinfo? */
	obj_t BGl_z62boxzd2setz12zf2Cinfozf3za3zzcfa_approxz00(obj_t BgL_envz00_8430,
		obj_t BgL_objz00_8431)
	{
		{	/* Cfa/cinfo.sch 1245 */
			return
				BBOOL(BGl_boxzd2setz12zf2Cinfozf3zc1zzcfa_approxz00(BgL_objz00_8431));
		}

	}



/* box-set!/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_boxzd2setz12zc0_bglt
		BGl_boxzd2setz12zf2Cinfozd2nilze0zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 1246 */
			{	/* Cfa/cinfo.sch 1246 */
				obj_t BgL_classz00_6164;

				BgL_classz00_6164 = BGl_boxzd2setz12zf2Cinfoz32zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 1246 */
					obj_t BgL__ortest_1117z00_6165;

					BgL__ortest_1117z00_6165 = BGL_CLASS_NIL(BgL_classz00_6164);
					if (CBOOL(BgL__ortest_1117z00_6165))
						{	/* Cfa/cinfo.sch 1246 */
							return ((BgL_boxzd2setz12zc0_bglt) BgL__ortest_1117z00_6165);
						}
					else
						{	/* Cfa/cinfo.sch 1246 */
							return
								((BgL_boxzd2setz12zc0_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_6164));
						}
				}
			}
		}

	}



/* &box-set!/Cinfo-nil */
	BgL_boxzd2setz12zc0_bglt
		BGl_z62boxzd2setz12zf2Cinfozd2nilz82zzcfa_approxz00(obj_t BgL_envz00_8432)
	{
		{	/* Cfa/cinfo.sch 1246 */
			return BGl_boxzd2setz12zf2Cinfozd2nilze0zzcfa_approxz00();
		}

	}



/* box-set!/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_boxzd2setz12zf2Cinfozd2approxze0zzcfa_approxz00(BgL_boxzd2setz12zc0_bglt
		BgL_oz00_953)
	{
		{	/* Cfa/cinfo.sch 1247 */
			{
				BgL_boxzd2setz12zf2cinfoz32_bglt BgL_auxz00_13525;

				{
					obj_t BgL_auxz00_13526;

					{	/* Cfa/cinfo.sch 1247 */
						BgL_objectz00_bglt BgL_tmpz00_13527;

						BgL_tmpz00_13527 = ((BgL_objectz00_bglt) BgL_oz00_953);
						BgL_auxz00_13526 = BGL_OBJECT_WIDENING(BgL_tmpz00_13527);
					}
					BgL_auxz00_13525 =
						((BgL_boxzd2setz12zf2cinfoz32_bglt) BgL_auxz00_13526);
				}
				return
					(((BgL_boxzd2setz12zf2cinfoz32_bglt) COBJECT(BgL_auxz00_13525))->
					BgL_approxz00);
			}
		}

	}



/* &box-set!/Cinfo-approx */
	BgL_approxz00_bglt
		BGl_z62boxzd2setz12zf2Cinfozd2approxz82zzcfa_approxz00(obj_t
		BgL_envz00_8433, obj_t BgL_oz00_8434)
	{
		{	/* Cfa/cinfo.sch 1247 */
			return
				BGl_boxzd2setz12zf2Cinfozd2approxze0zzcfa_approxz00(
				((BgL_boxzd2setz12zc0_bglt) BgL_oz00_8434));
		}

	}



/* box-set!/Cinfo-vtype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_boxzd2setz12zf2Cinfozd2vtypeze0zzcfa_approxz00(BgL_boxzd2setz12zc0_bglt
		BgL_oz00_956)
	{
		{	/* Cfa/cinfo.sch 1249 */
			return
				(((BgL_boxzd2setz12zc0_bglt) COBJECT(
						((BgL_boxzd2setz12zc0_bglt) BgL_oz00_956)))->BgL_vtypez00);
		}

	}



/* &box-set!/Cinfo-vtype */
	BgL_typez00_bglt BGl_z62boxzd2setz12zf2Cinfozd2vtypez82zzcfa_approxz00(obj_t
		BgL_envz00_8435, obj_t BgL_oz00_8436)
	{
		{	/* Cfa/cinfo.sch 1249 */
			return
				BGl_boxzd2setz12zf2Cinfozd2vtypeze0zzcfa_approxz00(
				((BgL_boxzd2setz12zc0_bglt) BgL_oz00_8436));
		}

	}



/* box-set!/Cinfo-vtype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_boxzd2setz12zf2Cinfozd2vtypezd2setz12z20zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt BgL_oz00_957, BgL_typez00_bglt BgL_vz00_958)
	{
		{	/* Cfa/cinfo.sch 1250 */
			return
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_oz00_957)))->BgL_vtypez00) =
				((BgL_typez00_bglt) BgL_vz00_958), BUNSPEC);
		}

	}



/* &box-set!/Cinfo-vtype-set! */
	obj_t BGl_z62boxzd2setz12zf2Cinfozd2vtypezd2setz12z42zzcfa_approxz00(obj_t
		BgL_envz00_8437, obj_t BgL_oz00_8438, obj_t BgL_vz00_8439)
	{
		{	/* Cfa/cinfo.sch 1250 */
			return
				BGl_boxzd2setz12zf2Cinfozd2vtypezd2setz12z20zzcfa_approxz00(
				((BgL_boxzd2setz12zc0_bglt) BgL_oz00_8438),
				((BgL_typez00_bglt) BgL_vz00_8439));
		}

	}



/* box-set!/Cinfo-value */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_boxzd2setz12zf2Cinfozd2valueze0zzcfa_approxz00(BgL_boxzd2setz12zc0_bglt
		BgL_oz00_959)
	{
		{	/* Cfa/cinfo.sch 1251 */
			return
				(((BgL_boxzd2setz12zc0_bglt) COBJECT(
						((BgL_boxzd2setz12zc0_bglt) BgL_oz00_959)))->BgL_valuez00);
		}

	}



/* &box-set!/Cinfo-value */
	BgL_nodez00_bglt BGl_z62boxzd2setz12zf2Cinfozd2valuez82zzcfa_approxz00(obj_t
		BgL_envz00_8440, obj_t BgL_oz00_8441)
	{
		{	/* Cfa/cinfo.sch 1251 */
			return
				BGl_boxzd2setz12zf2Cinfozd2valueze0zzcfa_approxz00(
				((BgL_boxzd2setz12zc0_bglt) BgL_oz00_8441));
		}

	}



/* box-set!/Cinfo-value-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_boxzd2setz12zf2Cinfozd2valuezd2setz12z20zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt BgL_oz00_960, BgL_nodez00_bglt BgL_vz00_961)
	{
		{	/* Cfa/cinfo.sch 1252 */
			return
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_oz00_960)))->BgL_valuez00) =
				((BgL_nodez00_bglt) BgL_vz00_961), BUNSPEC);
		}

	}



/* &box-set!/Cinfo-value-set! */
	obj_t BGl_z62boxzd2setz12zf2Cinfozd2valuezd2setz12z42zzcfa_approxz00(obj_t
		BgL_envz00_8442, obj_t BgL_oz00_8443, obj_t BgL_vz00_8444)
	{
		{	/* Cfa/cinfo.sch 1252 */
			return
				BGl_boxzd2setz12zf2Cinfozd2valuezd2setz12z20zzcfa_approxz00(
				((BgL_boxzd2setz12zc0_bglt) BgL_oz00_8443),
				((BgL_nodez00_bglt) BgL_vz00_8444));
		}

	}



/* box-set!/Cinfo-var */
	BGL_EXPORTED_DEF BgL_varz00_bglt
		BGl_boxzd2setz12zf2Cinfozd2varze0zzcfa_approxz00(BgL_boxzd2setz12zc0_bglt
		BgL_oz00_962)
	{
		{	/* Cfa/cinfo.sch 1253 */
			return
				(((BgL_boxzd2setz12zc0_bglt) COBJECT(
						((BgL_boxzd2setz12zc0_bglt) BgL_oz00_962)))->BgL_varz00);
		}

	}



/* &box-set!/Cinfo-var */
	BgL_varz00_bglt BGl_z62boxzd2setz12zf2Cinfozd2varz82zzcfa_approxz00(obj_t
		BgL_envz00_8445, obj_t BgL_oz00_8446)
	{
		{	/* Cfa/cinfo.sch 1253 */
			return
				BGl_boxzd2setz12zf2Cinfozd2varze0zzcfa_approxz00(
				((BgL_boxzd2setz12zc0_bglt) BgL_oz00_8446));
		}

	}



/* box-set!/Cinfo-var-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_boxzd2setz12zf2Cinfozd2varzd2setz12z20zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt BgL_oz00_963, BgL_varz00_bglt BgL_vz00_964)
	{
		{	/* Cfa/cinfo.sch 1254 */
			return
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_oz00_963)))->BgL_varz00) =
				((BgL_varz00_bglt) BgL_vz00_964), BUNSPEC);
		}

	}



/* &box-set!/Cinfo-var-set! */
	obj_t BGl_z62boxzd2setz12zf2Cinfozd2varzd2setz12z42zzcfa_approxz00(obj_t
		BgL_envz00_8447, obj_t BgL_oz00_8448, obj_t BgL_vz00_8449)
	{
		{	/* Cfa/cinfo.sch 1254 */
			return
				BGl_boxzd2setz12zf2Cinfozd2varzd2setz12z20zzcfa_approxz00(
				((BgL_boxzd2setz12zc0_bglt) BgL_oz00_8448),
				((BgL_varz00_bglt) BgL_vz00_8449));
		}

	}



/* box-set!/Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_boxzd2setz12zf2Cinfozd2typeze0zzcfa_approxz00(BgL_boxzd2setz12zc0_bglt
		BgL_oz00_965)
	{
		{	/* Cfa/cinfo.sch 1255 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_965)))->BgL_typez00);
		}

	}



/* &box-set!/Cinfo-type */
	BgL_typez00_bglt BGl_z62boxzd2setz12zf2Cinfozd2typez82zzcfa_approxz00(obj_t
		BgL_envz00_8450, obj_t BgL_oz00_8451)
	{
		{	/* Cfa/cinfo.sch 1255 */
			return
				BGl_boxzd2setz12zf2Cinfozd2typeze0zzcfa_approxz00(
				((BgL_boxzd2setz12zc0_bglt) BgL_oz00_8451));
		}

	}



/* box-set!/Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_boxzd2setz12zf2Cinfozd2typezd2setz12z20zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt BgL_oz00_966, BgL_typez00_bglt BgL_vz00_967)
	{
		{	/* Cfa/cinfo.sch 1256 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_966)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_967), BUNSPEC);
		}

	}



/* &box-set!/Cinfo-type-set! */
	obj_t BGl_z62boxzd2setz12zf2Cinfozd2typezd2setz12z42zzcfa_approxz00(obj_t
		BgL_envz00_8452, obj_t BgL_oz00_8453, obj_t BgL_vz00_8454)
	{
		{	/* Cfa/cinfo.sch 1256 */
			return
				BGl_boxzd2setz12zf2Cinfozd2typezd2setz12z20zzcfa_approxz00(
				((BgL_boxzd2setz12zc0_bglt) BgL_oz00_8453),
				((BgL_typez00_bglt) BgL_vz00_8454));
		}

	}



/* box-set!/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_boxzd2setz12zf2Cinfozd2locze0zzcfa_approxz00(BgL_boxzd2setz12zc0_bglt
		BgL_oz00_968)
	{
		{	/* Cfa/cinfo.sch 1257 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_968)))->BgL_locz00);
		}

	}



/* &box-set!/Cinfo-loc */
	obj_t BGl_z62boxzd2setz12zf2Cinfozd2locz82zzcfa_approxz00(obj_t
		BgL_envz00_8455, obj_t BgL_oz00_8456)
	{
		{	/* Cfa/cinfo.sch 1257 */
			return
				BGl_boxzd2setz12zf2Cinfozd2locze0zzcfa_approxz00(
				((BgL_boxzd2setz12zc0_bglt) BgL_oz00_8456));
		}

	}



/* make-box-ref/Cinfo */
	BGL_EXPORTED_DEF BgL_boxzd2refzd2_bglt
		BGl_makezd2boxzd2refzf2Cinfozf2zzcfa_approxz00(obj_t BgL_loc1553z00_971,
		BgL_typez00_bglt BgL_type1554z00_972, obj_t BgL_sidezd2effect1555zd2_973,
		obj_t BgL_key1556z00_974, BgL_varz00_bglt BgL_var1557z00_975,
		BgL_typez00_bglt BgL_vtype1558z00_976,
		BgL_approxz00_bglt BgL_approx1559z00_977)
	{
		{	/* Cfa/cinfo.sch 1261 */
			{	/* Cfa/cinfo.sch 1261 */
				BgL_boxzd2refzd2_bglt BgL_new1292z00_9242;

				{	/* Cfa/cinfo.sch 1261 */
					BgL_boxzd2refzd2_bglt BgL_tmp1290z00_9243;
					BgL_boxzd2refzf2cinfoz20_bglt BgL_wide1291z00_9244;

					{
						BgL_boxzd2refzd2_bglt BgL_auxz00_13574;

						{	/* Cfa/cinfo.sch 1261 */
							BgL_boxzd2refzd2_bglt BgL_new1289z00_9245;

							BgL_new1289z00_9245 =
								((BgL_boxzd2refzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_boxzd2refzd2_bgl))));
							{	/* Cfa/cinfo.sch 1261 */
								long BgL_arg1770z00_9246;

								BgL_arg1770z00_9246 =
									BGL_CLASS_NUM(BGl_boxzd2refzd2zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1289z00_9245),
									BgL_arg1770z00_9246);
							}
							{	/* Cfa/cinfo.sch 1261 */
								BgL_objectz00_bglt BgL_tmpz00_13579;

								BgL_tmpz00_13579 = ((BgL_objectz00_bglt) BgL_new1289z00_9245);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13579, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1289z00_9245);
							BgL_auxz00_13574 = BgL_new1289z00_9245;
						}
						BgL_tmp1290z00_9243 = ((BgL_boxzd2refzd2_bglt) BgL_auxz00_13574);
					}
					BgL_wide1291z00_9244 =
						((BgL_boxzd2refzf2cinfoz20_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_boxzd2refzf2cinfoz20_bgl))));
					{	/* Cfa/cinfo.sch 1261 */
						obj_t BgL_auxz00_13587;
						BgL_objectz00_bglt BgL_tmpz00_13585;

						BgL_auxz00_13587 = ((obj_t) BgL_wide1291z00_9244);
						BgL_tmpz00_13585 = ((BgL_objectz00_bglt) BgL_tmp1290z00_9243);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13585, BgL_auxz00_13587);
					}
					((BgL_objectz00_bglt) BgL_tmp1290z00_9243);
					{	/* Cfa/cinfo.sch 1261 */
						long BgL_arg1767z00_9247;

						BgL_arg1767z00_9247 =
							BGL_CLASS_NUM(BGl_boxzd2refzf2Cinfoz20zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1290z00_9243), BgL_arg1767z00_9247);
					}
					BgL_new1292z00_9242 = ((BgL_boxzd2refzd2_bglt) BgL_tmp1290z00_9243);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1292z00_9242)))->BgL_locz00) =
					((obj_t) BgL_loc1553z00_971), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1292z00_9242)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1554z00_972), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1292z00_9242)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1555zd2_973), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1292z00_9242)))->BgL_keyz00) =
					((obj_t) BgL_key1556z00_974), BUNSPEC);
				((((BgL_boxzd2refzd2_bglt) COBJECT(((BgL_boxzd2refzd2_bglt)
									BgL_new1292z00_9242)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_var1557z00_975), BUNSPEC);
				((((BgL_boxzd2refzd2_bglt) COBJECT(((BgL_boxzd2refzd2_bglt)
									BgL_new1292z00_9242)))->BgL_vtypez00) =
					((BgL_typez00_bglt) BgL_vtype1558z00_976), BUNSPEC);
				{
					BgL_boxzd2refzf2cinfoz20_bglt BgL_auxz00_13607;

					{
						obj_t BgL_auxz00_13608;

						{	/* Cfa/cinfo.sch 1261 */
							BgL_objectz00_bglt BgL_tmpz00_13609;

							BgL_tmpz00_13609 = ((BgL_objectz00_bglt) BgL_new1292z00_9242);
							BgL_auxz00_13608 = BGL_OBJECT_WIDENING(BgL_tmpz00_13609);
						}
						BgL_auxz00_13607 =
							((BgL_boxzd2refzf2cinfoz20_bglt) BgL_auxz00_13608);
					}
					((((BgL_boxzd2refzf2cinfoz20_bglt) COBJECT(BgL_auxz00_13607))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1559z00_977), BUNSPEC);
				}
				return BgL_new1292z00_9242;
			}
		}

	}



/* &make-box-ref/Cinfo */
	BgL_boxzd2refzd2_bglt BGl_z62makezd2boxzd2refzf2Cinfoz90zzcfa_approxz00(obj_t
		BgL_envz00_8457, obj_t BgL_loc1553z00_8458, obj_t BgL_type1554z00_8459,
		obj_t BgL_sidezd2effect1555zd2_8460, obj_t BgL_key1556z00_8461,
		obj_t BgL_var1557z00_8462, obj_t BgL_vtype1558z00_8463,
		obj_t BgL_approx1559z00_8464)
	{
		{	/* Cfa/cinfo.sch 1261 */
			return
				BGl_makezd2boxzd2refzf2Cinfozf2zzcfa_approxz00(BgL_loc1553z00_8458,
				((BgL_typez00_bglt) BgL_type1554z00_8459),
				BgL_sidezd2effect1555zd2_8460, BgL_key1556z00_8461,
				((BgL_varz00_bglt) BgL_var1557z00_8462),
				((BgL_typez00_bglt) BgL_vtype1558z00_8463),
				((BgL_approxz00_bglt) BgL_approx1559z00_8464));
		}

	}



/* box-ref/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_boxzd2refzf2Cinfozf3zd3zzcfa_approxz00(obj_t
		BgL_objz00_978)
	{
		{	/* Cfa/cinfo.sch 1262 */
			{	/* Cfa/cinfo.sch 1262 */
				obj_t BgL_classz00_9248;

				BgL_classz00_9248 = BGl_boxzd2refzf2Cinfoz20zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_978))
					{	/* Cfa/cinfo.sch 1262 */
						BgL_objectz00_bglt BgL_arg1807z00_9249;

						BgL_arg1807z00_9249 = (BgL_objectz00_bglt) (BgL_objz00_978);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 1262 */
								long BgL_idxz00_9250;

								BgL_idxz00_9250 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9249);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9250 + 4L)) == BgL_classz00_9248);
							}
						else
							{	/* Cfa/cinfo.sch 1262 */
								bool_t BgL_res2195z00_9253;

								{	/* Cfa/cinfo.sch 1262 */
									obj_t BgL_oclassz00_9254;

									{	/* Cfa/cinfo.sch 1262 */
										obj_t BgL_arg1815z00_9255;
										long BgL_arg1816z00_9256;

										BgL_arg1815z00_9255 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 1262 */
											long BgL_arg1817z00_9257;

											BgL_arg1817z00_9257 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9249);
											BgL_arg1816z00_9256 = (BgL_arg1817z00_9257 - OBJECT_TYPE);
										}
										BgL_oclassz00_9254 =
											VECTOR_REF(BgL_arg1815z00_9255, BgL_arg1816z00_9256);
									}
									{	/* Cfa/cinfo.sch 1262 */
										bool_t BgL__ortest_1115z00_9258;

										BgL__ortest_1115z00_9258 =
											(BgL_classz00_9248 == BgL_oclassz00_9254);
										if (BgL__ortest_1115z00_9258)
											{	/* Cfa/cinfo.sch 1262 */
												BgL_res2195z00_9253 = BgL__ortest_1115z00_9258;
											}
										else
											{	/* Cfa/cinfo.sch 1262 */
												long BgL_odepthz00_9259;

												{	/* Cfa/cinfo.sch 1262 */
													obj_t BgL_arg1804z00_9260;

													BgL_arg1804z00_9260 = (BgL_oclassz00_9254);
													BgL_odepthz00_9259 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9260);
												}
												if ((4L < BgL_odepthz00_9259))
													{	/* Cfa/cinfo.sch 1262 */
														obj_t BgL_arg1802z00_9261;

														{	/* Cfa/cinfo.sch 1262 */
															obj_t BgL_arg1803z00_9262;

															BgL_arg1803z00_9262 = (BgL_oclassz00_9254);
															BgL_arg1802z00_9261 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9262,
																4L);
														}
														BgL_res2195z00_9253 =
															(BgL_arg1802z00_9261 == BgL_classz00_9248);
													}
												else
													{	/* Cfa/cinfo.sch 1262 */
														BgL_res2195z00_9253 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2195z00_9253;
							}
					}
				else
					{	/* Cfa/cinfo.sch 1262 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &box-ref/Cinfo? */
	obj_t BGl_z62boxzd2refzf2Cinfozf3zb1zzcfa_approxz00(obj_t BgL_envz00_8465,
		obj_t BgL_objz00_8466)
	{
		{	/* Cfa/cinfo.sch 1262 */
			return BBOOL(BGl_boxzd2refzf2Cinfozf3zd3zzcfa_approxz00(BgL_objz00_8466));
		}

	}



/* box-ref/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_boxzd2refzd2_bglt
		BGl_boxzd2refzf2Cinfozd2nilzf2zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 1263 */
			{	/* Cfa/cinfo.sch 1263 */
				obj_t BgL_classz00_6216;

				BgL_classz00_6216 = BGl_boxzd2refzf2Cinfoz20zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 1263 */
					obj_t BgL__ortest_1117z00_6217;

					BgL__ortest_1117z00_6217 = BGL_CLASS_NIL(BgL_classz00_6216);
					if (CBOOL(BgL__ortest_1117z00_6217))
						{	/* Cfa/cinfo.sch 1263 */
							return ((BgL_boxzd2refzd2_bglt) BgL__ortest_1117z00_6217);
						}
					else
						{	/* Cfa/cinfo.sch 1263 */
							return
								((BgL_boxzd2refzd2_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_6216));
						}
				}
			}
		}

	}



/* &box-ref/Cinfo-nil */
	BgL_boxzd2refzd2_bglt BGl_z62boxzd2refzf2Cinfozd2nilz90zzcfa_approxz00(obj_t
		BgL_envz00_8467)
	{
		{	/* Cfa/cinfo.sch 1263 */
			return BGl_boxzd2refzf2Cinfozd2nilzf2zzcfa_approxz00();
		}

	}



/* box-ref/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_boxzd2refzf2Cinfozd2approxzf2zzcfa_approxz00(BgL_boxzd2refzd2_bglt
		BgL_oz00_979)
	{
		{	/* Cfa/cinfo.sch 1264 */
			{
				BgL_boxzd2refzf2cinfoz20_bglt BgL_auxz00_13650;

				{
					obj_t BgL_auxz00_13651;

					{	/* Cfa/cinfo.sch 1264 */
						BgL_objectz00_bglt BgL_tmpz00_13652;

						BgL_tmpz00_13652 = ((BgL_objectz00_bglt) BgL_oz00_979);
						BgL_auxz00_13651 = BGL_OBJECT_WIDENING(BgL_tmpz00_13652);
					}
					BgL_auxz00_13650 = ((BgL_boxzd2refzf2cinfoz20_bglt) BgL_auxz00_13651);
				}
				return
					(((BgL_boxzd2refzf2cinfoz20_bglt) COBJECT(BgL_auxz00_13650))->
					BgL_approxz00);
			}
		}

	}



/* &box-ref/Cinfo-approx */
	BgL_approxz00_bglt BGl_z62boxzd2refzf2Cinfozd2approxz90zzcfa_approxz00(obj_t
		BgL_envz00_8468, obj_t BgL_oz00_8469)
	{
		{	/* Cfa/cinfo.sch 1264 */
			return
				BGl_boxzd2refzf2Cinfozd2approxzf2zzcfa_approxz00(
				((BgL_boxzd2refzd2_bglt) BgL_oz00_8469));
		}

	}



/* box-ref/Cinfo-vtype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_boxzd2refzf2Cinfozd2vtypezf2zzcfa_approxz00(BgL_boxzd2refzd2_bglt
		BgL_oz00_982)
	{
		{	/* Cfa/cinfo.sch 1266 */
			return
				(((BgL_boxzd2refzd2_bglt) COBJECT(
						((BgL_boxzd2refzd2_bglt) BgL_oz00_982)))->BgL_vtypez00);
		}

	}



/* &box-ref/Cinfo-vtype */
	BgL_typez00_bglt BGl_z62boxzd2refzf2Cinfozd2vtypez90zzcfa_approxz00(obj_t
		BgL_envz00_8470, obj_t BgL_oz00_8471)
	{
		{	/* Cfa/cinfo.sch 1266 */
			return
				BGl_boxzd2refzf2Cinfozd2vtypezf2zzcfa_approxz00(
				((BgL_boxzd2refzd2_bglt) BgL_oz00_8471));
		}

	}



/* box-ref/Cinfo-vtype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_boxzd2refzf2Cinfozd2vtypezd2setz12z32zzcfa_approxz00
		(BgL_boxzd2refzd2_bglt BgL_oz00_983, BgL_typez00_bglt BgL_vz00_984)
	{
		{	/* Cfa/cinfo.sch 1267 */
			return
				((((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_oz00_983)))->BgL_vtypez00) =
				((BgL_typez00_bglt) BgL_vz00_984), BUNSPEC);
		}

	}



/* &box-ref/Cinfo-vtype-set! */
	obj_t BGl_z62boxzd2refzf2Cinfozd2vtypezd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_8472, obj_t BgL_oz00_8473, obj_t BgL_vz00_8474)
	{
		{	/* Cfa/cinfo.sch 1267 */
			return
				BGl_boxzd2refzf2Cinfozd2vtypezd2setz12z32zzcfa_approxz00(
				((BgL_boxzd2refzd2_bglt) BgL_oz00_8473),
				((BgL_typez00_bglt) BgL_vz00_8474));
		}

	}



/* box-ref/Cinfo-var */
	BGL_EXPORTED_DEF BgL_varz00_bglt
		BGl_boxzd2refzf2Cinfozd2varzf2zzcfa_approxz00(BgL_boxzd2refzd2_bglt
		BgL_oz00_985)
	{
		{	/* Cfa/cinfo.sch 1268 */
			return
				(((BgL_boxzd2refzd2_bglt) COBJECT(
						((BgL_boxzd2refzd2_bglt) BgL_oz00_985)))->BgL_varz00);
		}

	}



/* &box-ref/Cinfo-var */
	BgL_varz00_bglt BGl_z62boxzd2refzf2Cinfozd2varz90zzcfa_approxz00(obj_t
		BgL_envz00_8475, obj_t BgL_oz00_8476)
	{
		{	/* Cfa/cinfo.sch 1268 */
			return
				BGl_boxzd2refzf2Cinfozd2varzf2zzcfa_approxz00(
				((BgL_boxzd2refzd2_bglt) BgL_oz00_8476));
		}

	}



/* box-ref/Cinfo-var-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_boxzd2refzf2Cinfozd2varzd2setz12z32zzcfa_approxz00(BgL_boxzd2refzd2_bglt
		BgL_oz00_986, BgL_varz00_bglt BgL_vz00_987)
	{
		{	/* Cfa/cinfo.sch 1269 */
			return
				((((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_oz00_986)))->BgL_varz00) =
				((BgL_varz00_bglt) BgL_vz00_987), BUNSPEC);
		}

	}



/* &box-ref/Cinfo-var-set! */
	obj_t BGl_z62boxzd2refzf2Cinfozd2varzd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_8477, obj_t BgL_oz00_8478, obj_t BgL_vz00_8479)
	{
		{	/* Cfa/cinfo.sch 1269 */
			return
				BGl_boxzd2refzf2Cinfozd2varzd2setz12z32zzcfa_approxz00(
				((BgL_boxzd2refzd2_bglt) BgL_oz00_8478),
				((BgL_varz00_bglt) BgL_vz00_8479));
		}

	}



/* box-ref/Cinfo-key */
	BGL_EXPORTED_DEF obj_t
		BGl_boxzd2refzf2Cinfozd2keyzf2zzcfa_approxz00(BgL_boxzd2refzd2_bglt
		BgL_oz00_988)
	{
		{	/* Cfa/cinfo.sch 1270 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_988)))->BgL_keyz00);
		}

	}



/* &box-ref/Cinfo-key */
	obj_t BGl_z62boxzd2refzf2Cinfozd2keyz90zzcfa_approxz00(obj_t BgL_envz00_8480,
		obj_t BgL_oz00_8481)
	{
		{	/* Cfa/cinfo.sch 1270 */
			return
				BGl_boxzd2refzf2Cinfozd2keyzf2zzcfa_approxz00(
				((BgL_boxzd2refzd2_bglt) BgL_oz00_8481));
		}

	}



/* box-ref/Cinfo-key-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_boxzd2refzf2Cinfozd2keyzd2setz12z32zzcfa_approxz00(BgL_boxzd2refzd2_bglt
		BgL_oz00_989, obj_t BgL_vz00_990)
	{
		{	/* Cfa/cinfo.sch 1271 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_989)))->BgL_keyz00) =
				((obj_t) BgL_vz00_990), BUNSPEC);
		}

	}



/* &box-ref/Cinfo-key-set! */
	obj_t BGl_z62boxzd2refzf2Cinfozd2keyzd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_8482, obj_t BgL_oz00_8483, obj_t BgL_vz00_8484)
	{
		{	/* Cfa/cinfo.sch 1271 */
			return
				BGl_boxzd2refzf2Cinfozd2keyzd2setz12z32zzcfa_approxz00(
				((BgL_boxzd2refzd2_bglt) BgL_oz00_8483), BgL_vz00_8484);
		}

	}



/* box-ref/Cinfo-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_boxzd2refzf2Cinfozd2sidezd2effectz20zzcfa_approxz00
		(BgL_boxzd2refzd2_bglt BgL_oz00_991)
	{
		{	/* Cfa/cinfo.sch 1272 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_991)))->BgL_sidezd2effectzd2);
		}

	}



/* &box-ref/Cinfo-side-effect */
	obj_t BGl_z62boxzd2refzf2Cinfozd2sidezd2effectz42zzcfa_approxz00(obj_t
		BgL_envz00_8485, obj_t BgL_oz00_8486)
	{
		{	/* Cfa/cinfo.sch 1272 */
			return
				BGl_boxzd2refzf2Cinfozd2sidezd2effectz20zzcfa_approxz00(
				((BgL_boxzd2refzd2_bglt) BgL_oz00_8486));
		}

	}



/* box-ref/Cinfo-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_boxzd2refzf2Cinfozd2sidezd2effectzd2setz12ze0zzcfa_approxz00
		(BgL_boxzd2refzd2_bglt BgL_oz00_992, obj_t BgL_vz00_993)
	{
		{	/* Cfa/cinfo.sch 1273 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_992)))->
					BgL_sidezd2effectzd2) = ((obj_t) BgL_vz00_993), BUNSPEC);
		}

	}



/* &box-ref/Cinfo-side-effect-set! */
	obj_t
		BGl_z62boxzd2refzf2Cinfozd2sidezd2effectzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8487, obj_t BgL_oz00_8488, obj_t BgL_vz00_8489)
	{
		{	/* Cfa/cinfo.sch 1273 */
			return
				BGl_boxzd2refzf2Cinfozd2sidezd2effectzd2setz12ze0zzcfa_approxz00(
				((BgL_boxzd2refzd2_bglt) BgL_oz00_8488), BgL_vz00_8489);
		}

	}



/* box-ref/Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_boxzd2refzf2Cinfozd2typezf2zzcfa_approxz00(BgL_boxzd2refzd2_bglt
		BgL_oz00_994)
	{
		{	/* Cfa/cinfo.sch 1274 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_994)))->BgL_typez00);
		}

	}



/* &box-ref/Cinfo-type */
	BgL_typez00_bglt BGl_z62boxzd2refzf2Cinfozd2typez90zzcfa_approxz00(obj_t
		BgL_envz00_8490, obj_t BgL_oz00_8491)
	{
		{	/* Cfa/cinfo.sch 1274 */
			return
				BGl_boxzd2refzf2Cinfozd2typezf2zzcfa_approxz00(
				((BgL_boxzd2refzd2_bglt) BgL_oz00_8491));
		}

	}



/* box-ref/Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_boxzd2refzf2Cinfozd2typezd2setz12z32zzcfa_approxz00
		(BgL_boxzd2refzd2_bglt BgL_oz00_995, BgL_typez00_bglt BgL_vz00_996)
	{
		{	/* Cfa/cinfo.sch 1275 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_995)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_996), BUNSPEC);
		}

	}



/* &box-ref/Cinfo-type-set! */
	obj_t BGl_z62boxzd2refzf2Cinfozd2typezd2setz12z50zzcfa_approxz00(obj_t
		BgL_envz00_8492, obj_t BgL_oz00_8493, obj_t BgL_vz00_8494)
	{
		{	/* Cfa/cinfo.sch 1275 */
			return
				BGl_boxzd2refzf2Cinfozd2typezd2setz12z32zzcfa_approxz00(
				((BgL_boxzd2refzd2_bglt) BgL_oz00_8493),
				((BgL_typez00_bglt) BgL_vz00_8494));
		}

	}



/* box-ref/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_boxzd2refzf2Cinfozd2loczf2zzcfa_approxz00(BgL_boxzd2refzd2_bglt
		BgL_oz00_997)
	{
		{	/* Cfa/cinfo.sch 1276 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_997)))->BgL_locz00);
		}

	}



/* &box-ref/Cinfo-loc */
	obj_t BGl_z62boxzd2refzf2Cinfozd2locz90zzcfa_approxz00(obj_t BgL_envz00_8495,
		obj_t BgL_oz00_8496)
	{
		{	/* Cfa/cinfo.sch 1276 */
			return
				BGl_boxzd2refzf2Cinfozd2loczf2zzcfa_approxz00(
				((BgL_boxzd2refzd2_bglt) BgL_oz00_8496));
		}

	}



/* make-box-set!/O-Cinfo */
	BGL_EXPORTED_DEF BgL_boxzd2setz12zc0_bglt
		BGl_makezd2boxzd2setz12zf2Ozd2Cinfoz32zzcfa_approxz00(obj_t
		BgL_loc1546z00_1000, BgL_typez00_bglt BgL_type1547z00_1001,
		BgL_varz00_bglt BgL_var1548z00_1002, BgL_nodez00_bglt BgL_value1549z00_1003,
		BgL_typez00_bglt BgL_vtype1550z00_1004,
		BgL_approxz00_bglt BgL_approx1551z00_1005)
	{
		{	/* Cfa/cinfo.sch 1280 */
			{	/* Cfa/cinfo.sch 1280 */
				BgL_boxzd2setz12zc0_bglt BgL_new1296z00_9263;

				{	/* Cfa/cinfo.sch 1280 */
					BgL_boxzd2setz12zc0_bglt BgL_tmp1294z00_9264;
					BgL_boxzd2setz12zf2ozd2cinfoze0_bglt BgL_wide1295z00_9265;

					{
						BgL_boxzd2setz12zc0_bglt BgL_auxz00_13706;

						{	/* Cfa/cinfo.sch 1280 */
							BgL_boxzd2setz12zc0_bglt BgL_new1293z00_9266;

							BgL_new1293z00_9266 =
								((BgL_boxzd2setz12zc0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_boxzd2setz12zc0_bgl))));
							{	/* Cfa/cinfo.sch 1280 */
								long BgL_arg1773z00_9267;

								BgL_arg1773z00_9267 =
									BGL_CLASS_NUM(BGl_boxzd2setz12zc0zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1293z00_9266),
									BgL_arg1773z00_9267);
							}
							{	/* Cfa/cinfo.sch 1280 */
								BgL_objectz00_bglt BgL_tmpz00_13711;

								BgL_tmpz00_13711 = ((BgL_objectz00_bglt) BgL_new1293z00_9266);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13711, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1293z00_9266);
							BgL_auxz00_13706 = BgL_new1293z00_9266;
						}
						BgL_tmp1294z00_9264 = ((BgL_boxzd2setz12zc0_bglt) BgL_auxz00_13706);
					}
					BgL_wide1295z00_9265 =
						((BgL_boxzd2setz12zf2ozd2cinfoze0_bglt)
						BOBJECT(GC_MALLOC(sizeof(struct
									BgL_boxzd2setz12zf2ozd2cinfoze0_bgl))));
					{	/* Cfa/cinfo.sch 1280 */
						obj_t BgL_auxz00_13719;
						BgL_objectz00_bglt BgL_tmpz00_13717;

						BgL_auxz00_13719 = ((obj_t) BgL_wide1295z00_9265);
						BgL_tmpz00_13717 = ((BgL_objectz00_bglt) BgL_tmp1294z00_9264);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13717, BgL_auxz00_13719);
					}
					((BgL_objectz00_bglt) BgL_tmp1294z00_9264);
					{	/* Cfa/cinfo.sch 1280 */
						long BgL_arg1771z00_9268;

						BgL_arg1771z00_9268 =
							BGL_CLASS_NUM(BGl_boxzd2setz12zf2Ozd2Cinfoze0zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1294z00_9264), BgL_arg1771z00_9268);
					}
					BgL_new1296z00_9263 =
						((BgL_boxzd2setz12zc0_bglt) BgL_tmp1294z00_9264);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1296z00_9263)))->BgL_locz00) =
					((obj_t) BgL_loc1546z00_1000), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1296z00_9263)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1547z00_1001), BUNSPEC);
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(((BgL_boxzd2setz12zc0_bglt)
									BgL_new1296z00_9263)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_var1548z00_1002), BUNSPEC);
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(((BgL_boxzd2setz12zc0_bglt)
									BgL_new1296z00_9263)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_value1549z00_1003), BUNSPEC);
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(((BgL_boxzd2setz12zc0_bglt)
									BgL_new1296z00_9263)))->BgL_vtypez00) =
					((BgL_typez00_bglt) BgL_vtype1550z00_1004), BUNSPEC);
				{
					BgL_boxzd2setz12zf2ozd2cinfoze0_bglt BgL_auxz00_13737;

					{
						obj_t BgL_auxz00_13738;

						{	/* Cfa/cinfo.sch 1280 */
							BgL_objectz00_bglt BgL_tmpz00_13739;

							BgL_tmpz00_13739 = ((BgL_objectz00_bglt) BgL_new1296z00_9263);
							BgL_auxz00_13738 = BGL_OBJECT_WIDENING(BgL_tmpz00_13739);
						}
						BgL_auxz00_13737 =
							((BgL_boxzd2setz12zf2ozd2cinfoze0_bglt) BgL_auxz00_13738);
					}
					((((BgL_boxzd2setz12zf2ozd2cinfoze0_bglt) COBJECT(BgL_auxz00_13737))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1551z00_1005), BUNSPEC);
				}
				return BgL_new1296z00_9263;
			}
		}

	}



/* &make-box-set!/O-Cinfo */
	BgL_boxzd2setz12zc0_bglt
		BGl_z62makezd2boxzd2setz12zf2Ozd2Cinfoz50zzcfa_approxz00(obj_t
		BgL_envz00_8497, obj_t BgL_loc1546z00_8498, obj_t BgL_type1547z00_8499,
		obj_t BgL_var1548z00_8500, obj_t BgL_value1549z00_8501,
		obj_t BgL_vtype1550z00_8502, obj_t BgL_approx1551z00_8503)
	{
		{	/* Cfa/cinfo.sch 1280 */
			return
				BGl_makezd2boxzd2setz12zf2Ozd2Cinfoz32zzcfa_approxz00
				(BgL_loc1546z00_8498, ((BgL_typez00_bglt) BgL_type1547z00_8499),
				((BgL_varz00_bglt) BgL_var1548z00_8500),
				((BgL_nodez00_bglt) BgL_value1549z00_8501),
				((BgL_typez00_bglt) BgL_vtype1550z00_8502),
				((BgL_approxz00_bglt) BgL_approx1551z00_8503));
		}

	}



/* box-set!/O-Cinfo? */
	BGL_EXPORTED_DEF bool_t
		BGl_boxzd2setz12zf2Ozd2Cinfozf3z13zzcfa_approxz00(obj_t BgL_objz00_1006)
	{
		{	/* Cfa/cinfo.sch 1281 */
			{	/* Cfa/cinfo.sch 1281 */
				obj_t BgL_classz00_9269;

				BgL_classz00_9269 = BGl_boxzd2setz12zf2Ozd2Cinfoze0zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_1006))
					{	/* Cfa/cinfo.sch 1281 */
						BgL_objectz00_bglt BgL_arg1807z00_9270;

						BgL_arg1807z00_9270 = (BgL_objectz00_bglt) (BgL_objz00_1006);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 1281 */
								long BgL_idxz00_9271;

								BgL_idxz00_9271 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9270);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9271 + 3L)) == BgL_classz00_9269);
							}
						else
							{	/* Cfa/cinfo.sch 1281 */
								bool_t BgL_res2196z00_9274;

								{	/* Cfa/cinfo.sch 1281 */
									obj_t BgL_oclassz00_9275;

									{	/* Cfa/cinfo.sch 1281 */
										obj_t BgL_arg1815z00_9276;
										long BgL_arg1816z00_9277;

										BgL_arg1815z00_9276 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 1281 */
											long BgL_arg1817z00_9278;

											BgL_arg1817z00_9278 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9270);
											BgL_arg1816z00_9277 = (BgL_arg1817z00_9278 - OBJECT_TYPE);
										}
										BgL_oclassz00_9275 =
											VECTOR_REF(BgL_arg1815z00_9276, BgL_arg1816z00_9277);
									}
									{	/* Cfa/cinfo.sch 1281 */
										bool_t BgL__ortest_1115z00_9279;

										BgL__ortest_1115z00_9279 =
											(BgL_classz00_9269 == BgL_oclassz00_9275);
										if (BgL__ortest_1115z00_9279)
											{	/* Cfa/cinfo.sch 1281 */
												BgL_res2196z00_9274 = BgL__ortest_1115z00_9279;
											}
										else
											{	/* Cfa/cinfo.sch 1281 */
												long BgL_odepthz00_9280;

												{	/* Cfa/cinfo.sch 1281 */
													obj_t BgL_arg1804z00_9281;

													BgL_arg1804z00_9281 = (BgL_oclassz00_9275);
													BgL_odepthz00_9280 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9281);
												}
												if ((3L < BgL_odepthz00_9280))
													{	/* Cfa/cinfo.sch 1281 */
														obj_t BgL_arg1802z00_9282;

														{	/* Cfa/cinfo.sch 1281 */
															obj_t BgL_arg1803z00_9283;

															BgL_arg1803z00_9283 = (BgL_oclassz00_9275);
															BgL_arg1802z00_9282 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9283,
																3L);
														}
														BgL_res2196z00_9274 =
															(BgL_arg1802z00_9282 == BgL_classz00_9269);
													}
												else
													{	/* Cfa/cinfo.sch 1281 */
														BgL_res2196z00_9274 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2196z00_9274;
							}
					}
				else
					{	/* Cfa/cinfo.sch 1281 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &box-set!/O-Cinfo? */
	obj_t BGl_z62boxzd2setz12zf2Ozd2Cinfozf3z71zzcfa_approxz00(obj_t
		BgL_envz00_8504, obj_t BgL_objz00_8505)
	{
		{	/* Cfa/cinfo.sch 1281 */
			return
				BBOOL(BGl_boxzd2setz12zf2Ozd2Cinfozf3z13zzcfa_approxz00
				(BgL_objz00_8505));
		}

	}



/* box-set!/O-Cinfo-nil */
	BGL_EXPORTED_DEF BgL_boxzd2setz12zc0_bglt
		BGl_boxzd2setz12zf2Ozd2Cinfozd2nilz32zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 1282 */
			{	/* Cfa/cinfo.sch 1282 */
				obj_t BgL_classz00_6268;

				BgL_classz00_6268 = BGl_boxzd2setz12zf2Ozd2Cinfoze0zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 1282 */
					obj_t BgL__ortest_1117z00_6269;

					BgL__ortest_1117z00_6269 = BGL_CLASS_NIL(BgL_classz00_6268);
					if (CBOOL(BgL__ortest_1117z00_6269))
						{	/* Cfa/cinfo.sch 1282 */
							return ((BgL_boxzd2setz12zc0_bglt) BgL__ortest_1117z00_6269);
						}
					else
						{	/* Cfa/cinfo.sch 1282 */
							return
								((BgL_boxzd2setz12zc0_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_6268));
						}
				}
			}
		}

	}



/* &box-set!/O-Cinfo-nil */
	BgL_boxzd2setz12zc0_bglt
		BGl_z62boxzd2setz12zf2Ozd2Cinfozd2nilz50zzcfa_approxz00(obj_t
		BgL_envz00_8506)
	{
		{	/* Cfa/cinfo.sch 1282 */
			return BGl_boxzd2setz12zf2Ozd2Cinfozd2nilz32zzcfa_approxz00();
		}

	}



/* box-set!/O-Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_boxzd2setz12zf2Ozd2Cinfozd2approxz32zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt BgL_oz00_1007)
	{
		{	/* Cfa/cinfo.sch 1283 */
			{
				BgL_boxzd2setz12zf2ozd2cinfoze0_bglt BgL_auxz00_13781;

				{
					obj_t BgL_auxz00_13782;

					{	/* Cfa/cinfo.sch 1283 */
						BgL_objectz00_bglt BgL_tmpz00_13783;

						BgL_tmpz00_13783 = ((BgL_objectz00_bglt) BgL_oz00_1007);
						BgL_auxz00_13782 = BGL_OBJECT_WIDENING(BgL_tmpz00_13783);
					}
					BgL_auxz00_13781 =
						((BgL_boxzd2setz12zf2ozd2cinfoze0_bglt) BgL_auxz00_13782);
				}
				return
					(((BgL_boxzd2setz12zf2ozd2cinfoze0_bglt) COBJECT(BgL_auxz00_13781))->
					BgL_approxz00);
			}
		}

	}



/* &box-set!/O-Cinfo-approx */
	BgL_approxz00_bglt
		BGl_z62boxzd2setz12zf2Ozd2Cinfozd2approxz50zzcfa_approxz00(obj_t
		BgL_envz00_8507, obj_t BgL_oz00_8508)
	{
		{	/* Cfa/cinfo.sch 1283 */
			return
				BGl_boxzd2setz12zf2Ozd2Cinfozd2approxz32zzcfa_approxz00(
				((BgL_boxzd2setz12zc0_bglt) BgL_oz00_8508));
		}

	}



/* box-set!/O-Cinfo-vtype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_boxzd2setz12zf2Ozd2Cinfozd2vtypez32zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt BgL_oz00_1010)
	{
		{	/* Cfa/cinfo.sch 1285 */
			return
				(((BgL_boxzd2setz12zc0_bglt) COBJECT(
						((BgL_boxzd2setz12zc0_bglt) BgL_oz00_1010)))->BgL_vtypez00);
		}

	}



/* &box-set!/O-Cinfo-vtype */
	BgL_typez00_bglt
		BGl_z62boxzd2setz12zf2Ozd2Cinfozd2vtypez50zzcfa_approxz00(obj_t
		BgL_envz00_8509, obj_t BgL_oz00_8510)
	{
		{	/* Cfa/cinfo.sch 1285 */
			return
				BGl_boxzd2setz12zf2Ozd2Cinfozd2vtypez32zzcfa_approxz00(
				((BgL_boxzd2setz12zc0_bglt) BgL_oz00_8510));
		}

	}



/* box-set!/O-Cinfo-vtype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_boxzd2setz12zf2Ozd2Cinfozd2vtypezd2setz12zf2zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt BgL_oz00_1011, BgL_typez00_bglt BgL_vz00_1012)
	{
		{	/* Cfa/cinfo.sch 1286 */
			return
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_oz00_1011)))->BgL_vtypez00) =
				((BgL_typez00_bglt) BgL_vz00_1012), BUNSPEC);
		}

	}



/* &box-set!/O-Cinfo-vtype-set! */
	obj_t BGl_z62boxzd2setz12zf2Ozd2Cinfozd2vtypezd2setz12z90zzcfa_approxz00(obj_t
		BgL_envz00_8511, obj_t BgL_oz00_8512, obj_t BgL_vz00_8513)
	{
		{	/* Cfa/cinfo.sch 1286 */
			return
				BGl_boxzd2setz12zf2Ozd2Cinfozd2vtypezd2setz12zf2zzcfa_approxz00(
				((BgL_boxzd2setz12zc0_bglt) BgL_oz00_8512),
				((BgL_typez00_bglt) BgL_vz00_8513));
		}

	}



/* box-set!/O-Cinfo-value */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_boxzd2setz12zf2Ozd2Cinfozd2valuez32zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt BgL_oz00_1013)
	{
		{	/* Cfa/cinfo.sch 1287 */
			return
				(((BgL_boxzd2setz12zc0_bglt) COBJECT(
						((BgL_boxzd2setz12zc0_bglt) BgL_oz00_1013)))->BgL_valuez00);
		}

	}



/* &box-set!/O-Cinfo-value */
	BgL_nodez00_bglt
		BGl_z62boxzd2setz12zf2Ozd2Cinfozd2valuez50zzcfa_approxz00(obj_t
		BgL_envz00_8514, obj_t BgL_oz00_8515)
	{
		{	/* Cfa/cinfo.sch 1287 */
			return
				BGl_boxzd2setz12zf2Ozd2Cinfozd2valuez32zzcfa_approxz00(
				((BgL_boxzd2setz12zc0_bglt) BgL_oz00_8515));
		}

	}



/* box-set!/O-Cinfo-value-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_boxzd2setz12zf2Ozd2Cinfozd2valuezd2setz12zf2zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt BgL_oz00_1014, BgL_nodez00_bglt BgL_vz00_1015)
	{
		{	/* Cfa/cinfo.sch 1288 */
			return
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_oz00_1014)))->BgL_valuez00) =
				((BgL_nodez00_bglt) BgL_vz00_1015), BUNSPEC);
		}

	}



/* &box-set!/O-Cinfo-value-set! */
	obj_t BGl_z62boxzd2setz12zf2Ozd2Cinfozd2valuezd2setz12z90zzcfa_approxz00(obj_t
		BgL_envz00_8516, obj_t BgL_oz00_8517, obj_t BgL_vz00_8518)
	{
		{	/* Cfa/cinfo.sch 1288 */
			return
				BGl_boxzd2setz12zf2Ozd2Cinfozd2valuezd2setz12zf2zzcfa_approxz00(
				((BgL_boxzd2setz12zc0_bglt) BgL_oz00_8517),
				((BgL_nodez00_bglt) BgL_vz00_8518));
		}

	}



/* box-set!/O-Cinfo-var */
	BGL_EXPORTED_DEF BgL_varz00_bglt
		BGl_boxzd2setz12zf2Ozd2Cinfozd2varz32zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt BgL_oz00_1016)
	{
		{	/* Cfa/cinfo.sch 1289 */
			return
				(((BgL_boxzd2setz12zc0_bglt) COBJECT(
						((BgL_boxzd2setz12zc0_bglt) BgL_oz00_1016)))->BgL_varz00);
		}

	}



/* &box-set!/O-Cinfo-var */
	BgL_varz00_bglt BGl_z62boxzd2setz12zf2Ozd2Cinfozd2varz50zzcfa_approxz00(obj_t
		BgL_envz00_8519, obj_t BgL_oz00_8520)
	{
		{	/* Cfa/cinfo.sch 1289 */
			return
				BGl_boxzd2setz12zf2Ozd2Cinfozd2varz32zzcfa_approxz00(
				((BgL_boxzd2setz12zc0_bglt) BgL_oz00_8520));
		}

	}



/* box-set!/O-Cinfo-var-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_boxzd2setz12zf2Ozd2Cinfozd2varzd2setz12zf2zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt BgL_oz00_1017, BgL_varz00_bglt BgL_vz00_1018)
	{
		{	/* Cfa/cinfo.sch 1290 */
			return
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_oz00_1017)))->BgL_varz00) =
				((BgL_varz00_bglt) BgL_vz00_1018), BUNSPEC);
		}

	}



/* &box-set!/O-Cinfo-var-set! */
	obj_t BGl_z62boxzd2setz12zf2Ozd2Cinfozd2varzd2setz12z90zzcfa_approxz00(obj_t
		BgL_envz00_8521, obj_t BgL_oz00_8522, obj_t BgL_vz00_8523)
	{
		{	/* Cfa/cinfo.sch 1290 */
			return
				BGl_boxzd2setz12zf2Ozd2Cinfozd2varzd2setz12zf2zzcfa_approxz00(
				((BgL_boxzd2setz12zc0_bglt) BgL_oz00_8522),
				((BgL_varz00_bglt) BgL_vz00_8523));
		}

	}



/* box-set!/O-Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_boxzd2setz12zf2Ozd2Cinfozd2typez32zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt BgL_oz00_1019)
	{
		{	/* Cfa/cinfo.sch 1291 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_1019)))->BgL_typez00);
		}

	}



/* &box-set!/O-Cinfo-type */
	BgL_typez00_bglt
		BGl_z62boxzd2setz12zf2Ozd2Cinfozd2typez50zzcfa_approxz00(obj_t
		BgL_envz00_8524, obj_t BgL_oz00_8525)
	{
		{	/* Cfa/cinfo.sch 1291 */
			return
				BGl_boxzd2setz12zf2Ozd2Cinfozd2typez32zzcfa_approxz00(
				((BgL_boxzd2setz12zc0_bglt) BgL_oz00_8525));
		}

	}



/* box-set!/O-Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_boxzd2setz12zf2Ozd2Cinfozd2typezd2setz12zf2zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt BgL_oz00_1020, BgL_typez00_bglt BgL_vz00_1021)
	{
		{	/* Cfa/cinfo.sch 1292 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_1020)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_1021), BUNSPEC);
		}

	}



/* &box-set!/O-Cinfo-type-set! */
	obj_t BGl_z62boxzd2setz12zf2Ozd2Cinfozd2typezd2setz12z90zzcfa_approxz00(obj_t
		BgL_envz00_8526, obj_t BgL_oz00_8527, obj_t BgL_vz00_8528)
	{
		{	/* Cfa/cinfo.sch 1292 */
			return
				BGl_boxzd2setz12zf2Ozd2Cinfozd2typezd2setz12zf2zzcfa_approxz00(
				((BgL_boxzd2setz12zc0_bglt) BgL_oz00_8527),
				((BgL_typez00_bglt) BgL_vz00_8528));
		}

	}



/* box-set!/O-Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_boxzd2setz12zf2Ozd2Cinfozd2locz32zzcfa_approxz00
		(BgL_boxzd2setz12zc0_bglt BgL_oz00_1022)
	{
		{	/* Cfa/cinfo.sch 1293 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_1022)))->BgL_locz00);
		}

	}



/* &box-set!/O-Cinfo-loc */
	obj_t BGl_z62boxzd2setz12zf2Ozd2Cinfozd2locz50zzcfa_approxz00(obj_t
		BgL_envz00_8529, obj_t BgL_oz00_8530)
	{
		{	/* Cfa/cinfo.sch 1293 */
			return
				BGl_boxzd2setz12zf2Ozd2Cinfozd2locz32zzcfa_approxz00(
				((BgL_boxzd2setz12zc0_bglt) BgL_oz00_8530));
		}

	}



/* make-box-ref/O-Cinfo */
	BGL_EXPORTED_DEF BgL_boxzd2refzd2_bglt
		BGl_makezd2boxzd2refzf2Ozd2Cinfoz20zzcfa_approxz00(obj_t
		BgL_loc1537z00_1025, BgL_typez00_bglt BgL_type1538z00_1026,
		obj_t BgL_sidezd2effect1539zd2_1027, obj_t BgL_key1540z00_1028,
		BgL_varz00_bglt BgL_var1541z00_1029, BgL_typez00_bglt BgL_vtype1542z00_1030,
		BgL_approxz00_bglt BgL_approx1543z00_1031)
	{
		{	/* Cfa/cinfo.sch 1297 */
			{	/* Cfa/cinfo.sch 1297 */
				BgL_boxzd2refzd2_bglt BgL_new1300z00_9284;

				{	/* Cfa/cinfo.sch 1297 */
					BgL_boxzd2refzd2_bglt BgL_tmp1298z00_9285;
					BgL_boxzd2refzf2ozd2cinfozf2_bglt BgL_wide1299z00_9286;

					{
						BgL_boxzd2refzd2_bglt BgL_auxz00_13830;

						{	/* Cfa/cinfo.sch 1297 */
							BgL_boxzd2refzd2_bglt BgL_new1297z00_9287;

							BgL_new1297z00_9287 =
								((BgL_boxzd2refzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_boxzd2refzd2_bgl))));
							{	/* Cfa/cinfo.sch 1297 */
								long BgL_arg1798z00_9288;

								BgL_arg1798z00_9288 =
									BGL_CLASS_NUM(BGl_boxzd2refzd2zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1297z00_9287),
									BgL_arg1798z00_9288);
							}
							{	/* Cfa/cinfo.sch 1297 */
								BgL_objectz00_bglt BgL_tmpz00_13835;

								BgL_tmpz00_13835 = ((BgL_objectz00_bglt) BgL_new1297z00_9287);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13835, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1297z00_9287);
							BgL_auxz00_13830 = BgL_new1297z00_9287;
						}
						BgL_tmp1298z00_9285 = ((BgL_boxzd2refzd2_bglt) BgL_auxz00_13830);
					}
					BgL_wide1299z00_9286 =
						((BgL_boxzd2refzf2ozd2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_boxzd2refzf2ozd2cinfozf2_bgl))));
					{	/* Cfa/cinfo.sch 1297 */
						obj_t BgL_auxz00_13843;
						BgL_objectz00_bglt BgL_tmpz00_13841;

						BgL_auxz00_13843 = ((obj_t) BgL_wide1299z00_9286);
						BgL_tmpz00_13841 = ((BgL_objectz00_bglt) BgL_tmp1298z00_9285);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13841, BgL_auxz00_13843);
					}
					((BgL_objectz00_bglt) BgL_tmp1298z00_9285);
					{	/* Cfa/cinfo.sch 1297 */
						long BgL_arg1775z00_9289;

						BgL_arg1775z00_9289 =
							BGL_CLASS_NUM(BGl_boxzd2refzf2Ozd2Cinfozf2zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1298z00_9285), BgL_arg1775z00_9289);
					}
					BgL_new1300z00_9284 = ((BgL_boxzd2refzd2_bglt) BgL_tmp1298z00_9285);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1300z00_9284)))->BgL_locz00) =
					((obj_t) BgL_loc1537z00_1025), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1300z00_9284)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1538z00_1026), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1300z00_9284)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1539zd2_1027), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1300z00_9284)))->BgL_keyz00) =
					((obj_t) BgL_key1540z00_1028), BUNSPEC);
				((((BgL_boxzd2refzd2_bglt) COBJECT(((BgL_boxzd2refzd2_bglt)
									BgL_new1300z00_9284)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_var1541z00_1029), BUNSPEC);
				((((BgL_boxzd2refzd2_bglt) COBJECT(((BgL_boxzd2refzd2_bglt)
									BgL_new1300z00_9284)))->BgL_vtypez00) =
					((BgL_typez00_bglt) BgL_vtype1542z00_1030), BUNSPEC);
				{
					BgL_boxzd2refzf2ozd2cinfozf2_bglt BgL_auxz00_13863;

					{
						obj_t BgL_auxz00_13864;

						{	/* Cfa/cinfo.sch 1297 */
							BgL_objectz00_bglt BgL_tmpz00_13865;

							BgL_tmpz00_13865 = ((BgL_objectz00_bglt) BgL_new1300z00_9284);
							BgL_auxz00_13864 = BGL_OBJECT_WIDENING(BgL_tmpz00_13865);
						}
						BgL_auxz00_13863 =
							((BgL_boxzd2refzf2ozd2cinfozf2_bglt) BgL_auxz00_13864);
					}
					((((BgL_boxzd2refzf2ozd2cinfozf2_bglt) COBJECT(BgL_auxz00_13863))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1543z00_1031), BUNSPEC);
				}
				return BgL_new1300z00_9284;
			}
		}

	}



/* &make-box-ref/O-Cinfo */
	BgL_boxzd2refzd2_bglt
		BGl_z62makezd2boxzd2refzf2Ozd2Cinfoz42zzcfa_approxz00(obj_t BgL_envz00_8531,
		obj_t BgL_loc1537z00_8532, obj_t BgL_type1538z00_8533,
		obj_t BgL_sidezd2effect1539zd2_8534, obj_t BgL_key1540z00_8535,
		obj_t BgL_var1541z00_8536, obj_t BgL_vtype1542z00_8537,
		obj_t BgL_approx1543z00_8538)
	{
		{	/* Cfa/cinfo.sch 1297 */
			return
				BGl_makezd2boxzd2refzf2Ozd2Cinfoz20zzcfa_approxz00(BgL_loc1537z00_8532,
				((BgL_typez00_bglt) BgL_type1538z00_8533),
				BgL_sidezd2effect1539zd2_8534, BgL_key1540z00_8535,
				((BgL_varz00_bglt) BgL_var1541z00_8536),
				((BgL_typez00_bglt) BgL_vtype1542z00_8537),
				((BgL_approxz00_bglt) BgL_approx1543z00_8538));
		}

	}



/* box-ref/O-Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_boxzd2refzf2Ozd2Cinfozf3z01zzcfa_approxz00(obj_t
		BgL_objz00_1032)
	{
		{	/* Cfa/cinfo.sch 1298 */
			{	/* Cfa/cinfo.sch 1298 */
				obj_t BgL_classz00_9290;

				BgL_classz00_9290 = BGl_boxzd2refzf2Ozd2Cinfozf2zzcfa_infoz00;
				if (BGL_OBJECTP(BgL_objz00_1032))
					{	/* Cfa/cinfo.sch 1298 */
						BgL_objectz00_bglt BgL_arg1807z00_9291;

						BgL_arg1807z00_9291 = (BgL_objectz00_bglt) (BgL_objz00_1032);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo.sch 1298 */
								long BgL_idxz00_9292;

								BgL_idxz00_9292 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9291);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9292 + 4L)) == BgL_classz00_9290);
							}
						else
							{	/* Cfa/cinfo.sch 1298 */
								bool_t BgL_res2197z00_9295;

								{	/* Cfa/cinfo.sch 1298 */
									obj_t BgL_oclassz00_9296;

									{	/* Cfa/cinfo.sch 1298 */
										obj_t BgL_arg1815z00_9297;
										long BgL_arg1816z00_9298;

										BgL_arg1815z00_9297 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo.sch 1298 */
											long BgL_arg1817z00_9299;

											BgL_arg1817z00_9299 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9291);
											BgL_arg1816z00_9298 = (BgL_arg1817z00_9299 - OBJECT_TYPE);
										}
										BgL_oclassz00_9296 =
											VECTOR_REF(BgL_arg1815z00_9297, BgL_arg1816z00_9298);
									}
									{	/* Cfa/cinfo.sch 1298 */
										bool_t BgL__ortest_1115z00_9300;

										BgL__ortest_1115z00_9300 =
											(BgL_classz00_9290 == BgL_oclassz00_9296);
										if (BgL__ortest_1115z00_9300)
											{	/* Cfa/cinfo.sch 1298 */
												BgL_res2197z00_9295 = BgL__ortest_1115z00_9300;
											}
										else
											{	/* Cfa/cinfo.sch 1298 */
												long BgL_odepthz00_9301;

												{	/* Cfa/cinfo.sch 1298 */
													obj_t BgL_arg1804z00_9302;

													BgL_arg1804z00_9302 = (BgL_oclassz00_9296);
													BgL_odepthz00_9301 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9302);
												}
												if ((4L < BgL_odepthz00_9301))
													{	/* Cfa/cinfo.sch 1298 */
														obj_t BgL_arg1802z00_9303;

														{	/* Cfa/cinfo.sch 1298 */
															obj_t BgL_arg1803z00_9304;

															BgL_arg1803z00_9304 = (BgL_oclassz00_9296);
															BgL_arg1802z00_9303 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9304,
																4L);
														}
														BgL_res2197z00_9295 =
															(BgL_arg1802z00_9303 == BgL_classz00_9290);
													}
												else
													{	/* Cfa/cinfo.sch 1298 */
														BgL_res2197z00_9295 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2197z00_9295;
							}
					}
				else
					{	/* Cfa/cinfo.sch 1298 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &box-ref/O-Cinfo? */
	obj_t BGl_z62boxzd2refzf2Ozd2Cinfozf3z63zzcfa_approxz00(obj_t BgL_envz00_8539,
		obj_t BgL_objz00_8540)
	{
		{	/* Cfa/cinfo.sch 1298 */
			return
				BBOOL(BGl_boxzd2refzf2Ozd2Cinfozf3z01zzcfa_approxz00(BgL_objz00_8540));
		}

	}



/* box-ref/O-Cinfo-nil */
	BGL_EXPORTED_DEF BgL_boxzd2refzd2_bglt
		BGl_boxzd2refzf2Ozd2Cinfozd2nilz20zzcfa_approxz00(void)
	{
		{	/* Cfa/cinfo.sch 1299 */
			{	/* Cfa/cinfo.sch 1299 */
				obj_t BgL_classz00_6320;

				BgL_classz00_6320 = BGl_boxzd2refzf2Ozd2Cinfozf2zzcfa_infoz00;
				{	/* Cfa/cinfo.sch 1299 */
					obj_t BgL__ortest_1117z00_6321;

					BgL__ortest_1117z00_6321 = BGL_CLASS_NIL(BgL_classz00_6320);
					if (CBOOL(BgL__ortest_1117z00_6321))
						{	/* Cfa/cinfo.sch 1299 */
							return ((BgL_boxzd2refzd2_bglt) BgL__ortest_1117z00_6321);
						}
					else
						{	/* Cfa/cinfo.sch 1299 */
							return
								((BgL_boxzd2refzd2_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_6320));
						}
				}
			}
		}

	}



/* &box-ref/O-Cinfo-nil */
	BgL_boxzd2refzd2_bglt
		BGl_z62boxzd2refzf2Ozd2Cinfozd2nilz42zzcfa_approxz00(obj_t BgL_envz00_8541)
	{
		{	/* Cfa/cinfo.sch 1299 */
			return BGl_boxzd2refzf2Ozd2Cinfozd2nilz20zzcfa_approxz00();
		}

	}



/* box-ref/O-Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_boxzd2refzf2Ozd2Cinfozd2approxz20zzcfa_approxz00(BgL_boxzd2refzd2_bglt
		BgL_oz00_1033)
	{
		{	/* Cfa/cinfo.sch 1300 */
			{
				BgL_boxzd2refzf2ozd2cinfozf2_bglt BgL_auxz00_13906;

				{
					obj_t BgL_auxz00_13907;

					{	/* Cfa/cinfo.sch 1300 */
						BgL_objectz00_bglt BgL_tmpz00_13908;

						BgL_tmpz00_13908 = ((BgL_objectz00_bglt) BgL_oz00_1033);
						BgL_auxz00_13907 = BGL_OBJECT_WIDENING(BgL_tmpz00_13908);
					}
					BgL_auxz00_13906 =
						((BgL_boxzd2refzf2ozd2cinfozf2_bglt) BgL_auxz00_13907);
				}
				return
					(((BgL_boxzd2refzf2ozd2cinfozf2_bglt) COBJECT(BgL_auxz00_13906))->
					BgL_approxz00);
			}
		}

	}



/* &box-ref/O-Cinfo-approx */
	BgL_approxz00_bglt
		BGl_z62boxzd2refzf2Ozd2Cinfozd2approxz42zzcfa_approxz00(obj_t
		BgL_envz00_8542, obj_t BgL_oz00_8543)
	{
		{	/* Cfa/cinfo.sch 1300 */
			return
				BGl_boxzd2refzf2Ozd2Cinfozd2approxz20zzcfa_approxz00(
				((BgL_boxzd2refzd2_bglt) BgL_oz00_8543));
		}

	}



/* box-ref/O-Cinfo-vtype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_boxzd2refzf2Ozd2Cinfozd2vtypez20zzcfa_approxz00(BgL_boxzd2refzd2_bglt
		BgL_oz00_1036)
	{
		{	/* Cfa/cinfo.sch 1302 */
			return
				(((BgL_boxzd2refzd2_bglt) COBJECT(
						((BgL_boxzd2refzd2_bglt) BgL_oz00_1036)))->BgL_vtypez00);
		}

	}



/* &box-ref/O-Cinfo-vtype */
	BgL_typez00_bglt BGl_z62boxzd2refzf2Ozd2Cinfozd2vtypez42zzcfa_approxz00(obj_t
		BgL_envz00_8544, obj_t BgL_oz00_8545)
	{
		{	/* Cfa/cinfo.sch 1302 */
			return
				BGl_boxzd2refzf2Ozd2Cinfozd2vtypez20zzcfa_approxz00(
				((BgL_boxzd2refzd2_bglt) BgL_oz00_8545));
		}

	}



/* box-ref/O-Cinfo-vtype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_boxzd2refzf2Ozd2Cinfozd2vtypezd2setz12ze0zzcfa_approxz00
		(BgL_boxzd2refzd2_bglt BgL_oz00_1037, BgL_typez00_bglt BgL_vz00_1038)
	{
		{	/* Cfa/cinfo.sch 1303 */
			return
				((((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_oz00_1037)))->BgL_vtypez00) =
				((BgL_typez00_bglt) BgL_vz00_1038), BUNSPEC);
		}

	}



/* &box-ref/O-Cinfo-vtype-set! */
	obj_t BGl_z62boxzd2refzf2Ozd2Cinfozd2vtypezd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8546, obj_t BgL_oz00_8547, obj_t BgL_vz00_8548)
	{
		{	/* Cfa/cinfo.sch 1303 */
			return
				BGl_boxzd2refzf2Ozd2Cinfozd2vtypezd2setz12ze0zzcfa_approxz00(
				((BgL_boxzd2refzd2_bglt) BgL_oz00_8547),
				((BgL_typez00_bglt) BgL_vz00_8548));
		}

	}



/* box-ref/O-Cinfo-var */
	BGL_EXPORTED_DEF BgL_varz00_bglt
		BGl_boxzd2refzf2Ozd2Cinfozd2varz20zzcfa_approxz00(BgL_boxzd2refzd2_bglt
		BgL_oz00_1039)
	{
		{	/* Cfa/cinfo.sch 1304 */
			return
				(((BgL_boxzd2refzd2_bglt) COBJECT(
						((BgL_boxzd2refzd2_bglt) BgL_oz00_1039)))->BgL_varz00);
		}

	}



/* &box-ref/O-Cinfo-var */
	BgL_varz00_bglt BGl_z62boxzd2refzf2Ozd2Cinfozd2varz42zzcfa_approxz00(obj_t
		BgL_envz00_8549, obj_t BgL_oz00_8550)
	{
		{	/* Cfa/cinfo.sch 1304 */
			return
				BGl_boxzd2refzf2Ozd2Cinfozd2varz20zzcfa_approxz00(
				((BgL_boxzd2refzd2_bglt) BgL_oz00_8550));
		}

	}



/* box-ref/O-Cinfo-var-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_boxzd2refzf2Ozd2Cinfozd2varzd2setz12ze0zzcfa_approxz00
		(BgL_boxzd2refzd2_bglt BgL_oz00_1040, BgL_varz00_bglt BgL_vz00_1041)
	{
		{	/* Cfa/cinfo.sch 1305 */
			return
				((((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_oz00_1040)))->BgL_varz00) =
				((BgL_varz00_bglt) BgL_vz00_1041), BUNSPEC);
		}

	}



/* &box-ref/O-Cinfo-var-set! */
	obj_t BGl_z62boxzd2refzf2Ozd2Cinfozd2varzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8551, obj_t BgL_oz00_8552, obj_t BgL_vz00_8553)
	{
		{	/* Cfa/cinfo.sch 1305 */
			return
				BGl_boxzd2refzf2Ozd2Cinfozd2varzd2setz12ze0zzcfa_approxz00(
				((BgL_boxzd2refzd2_bglt) BgL_oz00_8552),
				((BgL_varz00_bglt) BgL_vz00_8553));
		}

	}



/* box-ref/O-Cinfo-key */
	BGL_EXPORTED_DEF obj_t
		BGl_boxzd2refzf2Ozd2Cinfozd2keyz20zzcfa_approxz00(BgL_boxzd2refzd2_bglt
		BgL_oz00_1042)
	{
		{	/* Cfa/cinfo.sch 1306 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_1042)))->BgL_keyz00);
		}

	}



/* &box-ref/O-Cinfo-key */
	obj_t BGl_z62boxzd2refzf2Ozd2Cinfozd2keyz42zzcfa_approxz00(obj_t
		BgL_envz00_8554, obj_t BgL_oz00_8555)
	{
		{	/* Cfa/cinfo.sch 1306 */
			return
				BGl_boxzd2refzf2Ozd2Cinfozd2keyz20zzcfa_approxz00(
				((BgL_boxzd2refzd2_bglt) BgL_oz00_8555));
		}

	}



/* box-ref/O-Cinfo-key-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_boxzd2refzf2Ozd2Cinfozd2keyzd2setz12ze0zzcfa_approxz00
		(BgL_boxzd2refzd2_bglt BgL_oz00_1043, obj_t BgL_vz00_1044)
	{
		{	/* Cfa/cinfo.sch 1307 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_1043)))->BgL_keyz00) =
				((obj_t) BgL_vz00_1044), BUNSPEC);
		}

	}



/* &box-ref/O-Cinfo-key-set! */
	obj_t BGl_z62boxzd2refzf2Ozd2Cinfozd2keyzd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8556, obj_t BgL_oz00_8557, obj_t BgL_vz00_8558)
	{
		{	/* Cfa/cinfo.sch 1307 */
			return
				BGl_boxzd2refzf2Ozd2Cinfozd2keyzd2setz12ze0zzcfa_approxz00(
				((BgL_boxzd2refzd2_bglt) BgL_oz00_8557), BgL_vz00_8558);
		}

	}



/* box-ref/O-Cinfo-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_boxzd2refzf2Ozd2Cinfozd2sidezd2effectzf2zzcfa_approxz00
		(BgL_boxzd2refzd2_bglt BgL_oz00_1045)
	{
		{	/* Cfa/cinfo.sch 1308 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_1045)))->
				BgL_sidezd2effectzd2);
		}

	}



/* &box-ref/O-Cinfo-side-effect */
	obj_t BGl_z62boxzd2refzf2Ozd2Cinfozd2sidezd2effectz90zzcfa_approxz00(obj_t
		BgL_envz00_8559, obj_t BgL_oz00_8560)
	{
		{	/* Cfa/cinfo.sch 1308 */
			return
				BGl_boxzd2refzf2Ozd2Cinfozd2sidezd2effectzf2zzcfa_approxz00(
				((BgL_boxzd2refzd2_bglt) BgL_oz00_8560));
		}

	}



/* box-ref/O-Cinfo-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_boxzd2refzf2Ozd2Cinfozd2sidezd2effectzd2setz12z32zzcfa_approxz00
		(BgL_boxzd2refzd2_bglt BgL_oz00_1046, obj_t BgL_vz00_1047)
	{
		{	/* Cfa/cinfo.sch 1309 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_1046)))->
					BgL_sidezd2effectzd2) = ((obj_t) BgL_vz00_1047), BUNSPEC);
		}

	}



/* &box-ref/O-Cinfo-side-effect-set! */
	obj_t
		BGl_z62boxzd2refzf2Ozd2Cinfozd2sidezd2effectzd2setz12z50zzcfa_approxz00
		(obj_t BgL_envz00_8561, obj_t BgL_oz00_8562, obj_t BgL_vz00_8563)
	{
		{	/* Cfa/cinfo.sch 1309 */
			return
				BGl_boxzd2refzf2Ozd2Cinfozd2sidezd2effectzd2setz12z32zzcfa_approxz00(
				((BgL_boxzd2refzd2_bglt) BgL_oz00_8562), BgL_vz00_8563);
		}

	}



/* box-ref/O-Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_boxzd2refzf2Ozd2Cinfozd2typez20zzcfa_approxz00(BgL_boxzd2refzd2_bglt
		BgL_oz00_1048)
	{
		{	/* Cfa/cinfo.sch 1310 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_1048)))->BgL_typez00);
		}

	}



/* &box-ref/O-Cinfo-type */
	BgL_typez00_bglt BGl_z62boxzd2refzf2Ozd2Cinfozd2typez42zzcfa_approxz00(obj_t
		BgL_envz00_8564, obj_t BgL_oz00_8565)
	{
		{	/* Cfa/cinfo.sch 1310 */
			return
				BGl_boxzd2refzf2Ozd2Cinfozd2typez20zzcfa_approxz00(
				((BgL_boxzd2refzd2_bglt) BgL_oz00_8565));
		}

	}



/* box-ref/O-Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_boxzd2refzf2Ozd2Cinfozd2typezd2setz12ze0zzcfa_approxz00
		(BgL_boxzd2refzd2_bglt BgL_oz00_1049, BgL_typez00_bglt BgL_vz00_1050)
	{
		{	/* Cfa/cinfo.sch 1311 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_1049)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_1050), BUNSPEC);
		}

	}



/* &box-ref/O-Cinfo-type-set! */
	obj_t BGl_z62boxzd2refzf2Ozd2Cinfozd2typezd2setz12z82zzcfa_approxz00(obj_t
		BgL_envz00_8566, obj_t BgL_oz00_8567, obj_t BgL_vz00_8568)
	{
		{	/* Cfa/cinfo.sch 1311 */
			return
				BGl_boxzd2refzf2Ozd2Cinfozd2typezd2setz12ze0zzcfa_approxz00(
				((BgL_boxzd2refzd2_bglt) BgL_oz00_8567),
				((BgL_typez00_bglt) BgL_vz00_8568));
		}

	}



/* box-ref/O-Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_boxzd2refzf2Ozd2Cinfozd2locz20zzcfa_approxz00(BgL_boxzd2refzd2_bglt
		BgL_oz00_1051)
	{
		{	/* Cfa/cinfo.sch 1312 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_1051)))->BgL_locz00);
		}

	}



/* &box-ref/O-Cinfo-loc */
	obj_t BGl_z62boxzd2refzf2Ozd2Cinfozd2locz42zzcfa_approxz00(obj_t
		BgL_envz00_8569, obj_t BgL_oz00_8570)
	{
		{	/* Cfa/cinfo.sch 1312 */
			return
				BGl_boxzd2refzf2Ozd2Cinfozd2locz20zzcfa_approxz00(
				((BgL_boxzd2refzd2_bglt) BgL_oz00_8570));
		}

	}



/* declare-approx-sets! */
	BGL_EXPORTED_DEF obj_t BGl_declarezd2approxzd2setsz12z12zzcfa_approxz00(void)
	{
		{	/* Cfa/approx.scm 62 */
			return (BGl_za2alloczd2setza2zd2zzcfa_approxz00 =
				BGl_declarezd2setz12zc0zzcfa_setz00
				(BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00
					(BGl_getzd2allocszd2zzcfa_collectz00())), BUNSPEC);
		}

	}



/* &declare-approx-sets! */
	obj_t BGl_z62declarezd2approxzd2setsz12z70zzcfa_approxz00(obj_t
		BgL_envz00_8571)
	{
		{	/* Cfa/approx.scm 62 */
			return BGl_declarezd2approxzd2setsz12z12zzcfa_approxz00();
		}

	}



/* node-key */
	BGL_EXPORTED_DEF obj_t
		BGl_nodezd2keyzd2zzcfa_approxz00(BgL_nodezf2effectzf2_bglt BgL_nodez00_1054)
	{
		{	/* Cfa/approx.scm 69 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(BgL_nodez00_1054))->BgL_keyz00);
		}

	}



/* &node-key */
	obj_t BGl_z62nodezd2keyzb0zzcfa_approxz00(obj_t BgL_envz00_8572,
		obj_t BgL_nodez00_8573)
	{
		{	/* Cfa/approx.scm 69 */
			return
				BGl_nodezd2keyzd2zzcfa_approxz00(
				((BgL_nodezf2effectzf2_bglt) BgL_nodez00_8573));
		}

	}



/* node-key-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_nodezd2keyzd2setz12z12zzcfa_approxz00(BgL_nodezf2effectzf2_bglt
		BgL_nodez00_1055, obj_t BgL_keyz00_1056)
	{
		{	/* Cfa/approx.scm 75 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(BgL_nodez00_1055))->BgL_keyz00) =
				((obj_t) BgL_keyz00_1056), BUNSPEC);
		}

	}



/* &node-key-set! */
	obj_t BGl_z62nodezd2keyzd2setz12z70zzcfa_approxz00(obj_t BgL_envz00_8574,
		obj_t BgL_nodez00_8575, obj_t BgL_keyz00_8576)
	{
		{	/* Cfa/approx.scm 75 */
			return
				BGl_nodezd2keyzd2setz12z12zzcfa_approxz00(
				((BgL_nodezf2effectzf2_bglt) BgL_nodez00_8575), BgL_keyz00_8576);
		}

	}



/* approx-check-has-procedure? */
	BGL_EXPORTED_DEF bool_t
		BGl_approxzd2checkzd2haszd2procedurezf3z21zzcfa_approxz00(BgL_approxz00_bglt
		BgL_approxz00_1057)
	{
		{	/* Cfa/approx.scm 81 */
			{	/* Cfa/approx.scm 82 */
				obj_t BgL_resz00_8584;

				BgL_resz00_8584 = MAKE_CELL(BFALSE);
				{	/* Cfa/approx.scm 84 */
					obj_t BgL_zc3z04anonymousza31807ze3z87_8577;

					BgL_zc3z04anonymousza31807ze3z87_8577 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31807ze3ze5zzcfa_approxz00, (int) (1L),
						(int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31807ze3z87_8577, (int) (0L),
						((obj_t) BgL_resz00_8584));
					BGl_setzd2forzd2eachz00zzcfa_setz00
						(BgL_zc3z04anonymousza31807ze3z87_8577,
						(((BgL_approxz00_bglt) COBJECT(BgL_approxz00_1057))->
							BgL_allocsz00));
				}
				return CBOOL(CELL_REF(BgL_resz00_8584));
			}
		}

	}



/* &approx-check-has-procedure? */
	obj_t BGl_z62approxzd2checkzd2haszd2procedurezf3z43zzcfa_approxz00(obj_t
		BgL_envz00_8578, obj_t BgL_approxz00_8579)
	{
		{	/* Cfa/approx.scm 81 */
			return
				BBOOL(BGl_approxzd2checkzd2haszd2procedurezf3z21zzcfa_approxz00(
					((BgL_approxz00_bglt) BgL_approxz00_8579)));
		}

	}



/* &<@anonymous:1807> */
	obj_t BGl_z62zc3z04anonymousza31807ze3ze5zzcfa_approxz00(obj_t
		BgL_envz00_8580, obj_t BgL_appz00_8582)
	{
		{	/* Cfa/approx.scm 83 */
			{	/* Cfa/approx.scm 84 */
				obj_t BgL_resz00_8581;

				BgL_resz00_8581 = PROCEDURE_REF(BgL_envz00_8580, (int) (0L));
				{	/* Cfa/approx.scm 84 */
					bool_t BgL_test2959z00_13986;

					{	/* Cfa/approx.scm 84 */
						obj_t BgL_classz00_9305;

						BgL_classz00_9305 = BGl_makezd2procedurezd2appz00zzcfa_info2z00;
						if (BGL_OBJECTP(BgL_appz00_8582))
							{	/* Cfa/approx.scm 84 */
								BgL_objectz00_bglt BgL_arg1807z00_9306;

								BgL_arg1807z00_9306 = (BgL_objectz00_bglt) (BgL_appz00_8582);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cfa/approx.scm 84 */
										long BgL_idxz00_9307;

										BgL_idxz00_9307 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9306);
										BgL_test2959z00_13986 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_9307 + 4L)) == BgL_classz00_9305);
									}
								else
									{	/* Cfa/approx.scm 84 */
										bool_t BgL_res2198z00_9310;

										{	/* Cfa/approx.scm 84 */
											obj_t BgL_oclassz00_9311;

											{	/* Cfa/approx.scm 84 */
												obj_t BgL_arg1815z00_9312;
												long BgL_arg1816z00_9313;

												BgL_arg1815z00_9312 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cfa/approx.scm 84 */
													long BgL_arg1817z00_9314;

													BgL_arg1817z00_9314 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9306);
													BgL_arg1816z00_9313 =
														(BgL_arg1817z00_9314 - OBJECT_TYPE);
												}
												BgL_oclassz00_9311 =
													VECTOR_REF(BgL_arg1815z00_9312, BgL_arg1816z00_9313);
											}
											{	/* Cfa/approx.scm 84 */
												bool_t BgL__ortest_1115z00_9315;

												BgL__ortest_1115z00_9315 =
													(BgL_classz00_9305 == BgL_oclassz00_9311);
												if (BgL__ortest_1115z00_9315)
													{	/* Cfa/approx.scm 84 */
														BgL_res2198z00_9310 = BgL__ortest_1115z00_9315;
													}
												else
													{	/* Cfa/approx.scm 84 */
														long BgL_odepthz00_9316;

														{	/* Cfa/approx.scm 84 */
															obj_t BgL_arg1804z00_9317;

															BgL_arg1804z00_9317 = (BgL_oclassz00_9311);
															BgL_odepthz00_9316 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_9317);
														}
														if ((4L < BgL_odepthz00_9316))
															{	/* Cfa/approx.scm 84 */
																obj_t BgL_arg1802z00_9318;

																{	/* Cfa/approx.scm 84 */
																	obj_t BgL_arg1803z00_9319;

																	BgL_arg1803z00_9319 = (BgL_oclassz00_9311);
																	BgL_arg1802z00_9318 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9319,
																		4L);
																}
																BgL_res2198z00_9310 =
																	(BgL_arg1802z00_9318 == BgL_classz00_9305);
															}
														else
															{	/* Cfa/approx.scm 84 */
																BgL_res2198z00_9310 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2959z00_13986 = BgL_res2198z00_9310;
									}
							}
						else
							{	/* Cfa/approx.scm 84 */
								BgL_test2959z00_13986 = ((bool_t) 0);
							}
					}
					if (BgL_test2959z00_13986)
						{	/* Cfa/approx.scm 85 */
							obj_t BgL_auxz00_9320;

							BgL_auxz00_9320 = BTRUE;
							return CELL_SET(BgL_resz00_8581, BgL_auxz00_9320);
						}
					else
						{	/* Cfa/approx.scm 84 */
							return BFALSE;
						}
				}
			}
		}

	}



/* union-approx! */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_approxz00_bglt BgL_dstz00_1058,
		BgL_approxz00_bglt BgL_srcz00_1059)
	{
		{	/* Cfa/approx.scm 92 */
			if (
				(((BgL_approxz00_bglt) COBJECT(BgL_srcz00_1059))->
					BgL_haszd2procedurezf3z21))
				{	/* Cfa/approx.scm 94 */
					((((BgL_approxz00_bglt) COBJECT(BgL_dstz00_1058))->
							BgL_haszd2procedurezf3z21) = ((bool_t) ((bool_t) 1)), BUNSPEC);
				}
			else
				{	/* Cfa/approx.scm 94 */
					BFALSE;
				}
			{	/* Cfa/approx.scm 97 */
				obj_t BgL_arg1812z00_4041;

				BgL_arg1812z00_4041 =
					BGl_getzd2srczd2approxzd2typezd2zzcfa_approxz00(BgL_srcz00_1059);
				BGl_approxzd2setzd2typez12z12zzcfa_approxz00(BgL_dstz00_1058,
					((BgL_typez00_bglt) BgL_arg1812z00_4041));
			}
			{	/* Cfa/approx.scm 99 */
				bool_t BgL_test2965z00_14015;

				{	/* Cfa/approx.scm 99 */
					BgL_typez00_bglt BgL_arg1837z00_4053;

					BgL_arg1837z00_4053 =
						(((BgL_approxz00_bglt) COBJECT(BgL_dstz00_1058))->BgL_typez00);
					BgL_test2965z00_14015 =
						(((obj_t) BgL_arg1837z00_4053) == BGl_za2_za2z00zztype_cachez00);
				}
				if (BgL_test2965z00_14015)
					{	/* Cfa/approx.scm 99 */
						BFALSE;
					}
				else
					{	/* Cfa/approx.scm 101 */
						bool_t BgL_test2966z00_14019;

						{	/* Cfa/approx.scm 101 */
							obj_t BgL_arg1836z00_4052;

							BgL_arg1836z00_4052 =
								(((BgL_approxz00_bglt) COBJECT(BgL_srcz00_1059))->
								BgL_allocsz00);
							BgL_test2966z00_14019 =
								CBOOL(BGl_setzd2anyzd2zzcfa_setz00
								(BGl_vectorzd2approxzf3zd2envzf3zzcfa_approxz00,
									BgL_arg1836z00_4052));
						}
						if (BgL_test2966z00_14019)
							{	/* Cfa/approx.scm 101 */
								BGl_approxzd2setzd2typez12z12zzcfa_approxz00(BgL_dstz00_1058,
									((BgL_typez00_bglt) BGl_za2vectorza2z00zztype_cachez00));
							}
						else
							{	/* Cfa/approx.scm 103 */
								bool_t BgL_test2967z00_14025;

								{	/* Cfa/approx.scm 103 */
									bool_t BgL_test2968z00_14026;

									{	/* Cfa/approx.scm 103 */
										BgL_typez00_bglt BgL_arg1835z00_4051;

										BgL_arg1835z00_4051 =
											(((BgL_approxz00_bglt) COBJECT(BgL_srcz00_1059))->
											BgL_typez00);
										BgL_test2968z00_14026 =
											(((obj_t) BgL_arg1835z00_4051) ==
											BGl_za2_za2z00zztype_cachez00);
									}
									if (BgL_test2968z00_14026)
										{	/* Cfa/approx.scm 103 */
											BgL_test2967z00_14025 =
												(((BgL_approxz00_bglt) COBJECT(BgL_srcz00_1059))->
												BgL_topzf3zf3);
										}
									else
										{	/* Cfa/approx.scm 103 */
											BgL_test2967z00_14025 = ((bool_t) 0);
										}
								}
								if (BgL_test2967z00_14025)
									{	/* Cfa/approx.scm 103 */
										BGl_approxzd2setzd2typez12z12zzcfa_approxz00
											(BgL_dstz00_1058,
											((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00));
									}
								else
									{	/* Cfa/approx.scm 103 */
										BFALSE;
									}
							}
					}
			}
			{	/* Cfa/approx.scm 106 */
				bool_t BgL_test2969z00_14033;

				{	/* Cfa/approx.scm 106 */
					bool_t BgL_test2970z00_14034;

					{	/* Cfa/approx.scm 106 */
						BgL_typez00_bglt BgL_arg1845z00_4061;

						BgL_arg1845z00_4061 =
							(((BgL_approxz00_bglt) COBJECT(BgL_dstz00_1058))->BgL_typez00);
						BgL_test2970z00_14034 =
							(
							((obj_t) BgL_arg1845z00_4061) ==
							BGl_za2procedureza2z00zztype_cachez00);
					}
					if (BgL_test2970z00_14034)
						{	/* Cfa/approx.scm 106 */
							BgL_test2969z00_14033 = ((bool_t) 1);
						}
					else
						{	/* Cfa/approx.scm 107 */
							BgL_typez00_bglt BgL_arg1844z00_4060;

							BgL_arg1844z00_4060 =
								(((BgL_approxz00_bglt) COBJECT(BgL_dstz00_1058))->BgL_typez00);
							BgL_test2969z00_14033 =
								(
								((obj_t) BgL_arg1844z00_4060) == BGl_za2_za2z00zztype_cachez00);
						}
				}
				if (BgL_test2969z00_14033)
					{	/* Cfa/approx.scm 106 */
						BFALSE;
					}
				else
					{	/* Cfa/approx.scm 106 */
						BGl_disablezd2Xzd2Tz12z12zzcfa_procedurez00(BgL_srcz00_1059,
							BGl_string2223z00zzcfa_approxz00);
					}
			}
			if ((((BgL_approxz00_bglt) COBJECT(BgL_srcz00_1059))->BgL_topzf3zf3))
				{	/* Cfa/approx.scm 110 */
					BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_dstz00_1058);
				}
			else
				{	/* Cfa/approx.scm 110 */
					BFALSE;
				}
			{	/* Cfa/approx.scm 113 */
				bool_t BgL_test2972z00_14045;

				{	/* Cfa/approx.scm 113 */
					obj_t BgL_arg1851z00_4067;
					obj_t BgL_arg1852z00_4068;

					BgL_arg1851z00_4067 =
						(((BgL_approxz00_bglt) COBJECT(BgL_dstz00_1058))->BgL_allocsz00);
					BgL_arg1852z00_4068 =
						(((BgL_approxz00_bglt) COBJECT(BgL_srcz00_1059))->BgL_allocsz00);
					{	/* Cfa/approx.scm 113 */
						obj_t BgL_list1853z00_4069;

						BgL_list1853z00_4069 = MAKE_YOUNG_PAIR(BgL_arg1852z00_4068, BNIL);
						BgL_test2972z00_14045 =
							CBOOL(BGl_setzd2unionz12zc0zzcfa_setz00(BgL_arg1851z00_4067,
								BgL_list1853z00_4069));
					}
				}
				if (BgL_test2972z00_14045)
					{	/* Cfa/approx.scm 113 */
						BGl_continuezd2cfaz12zc0zzcfa_iteratez00(CNST_TABLE_REF(0));
					}
				else
					{	/* Cfa/approx.scm 113 */
						BFALSE;
					}
			}
			return BgL_dstz00_1058;
		}

	}



/* &union-approx! */
	BgL_approxz00_bglt BGl_z62unionzd2approxz12za2zzcfa_approxz00(obj_t
		BgL_envz00_8586, obj_t BgL_dstz00_8587, obj_t BgL_srcz00_8588)
	{
		{	/* Cfa/approx.scm 92 */
			return
				BGl_unionzd2approxz12zc0zzcfa_approxz00(
				((BgL_approxz00_bglt) BgL_dstz00_8587),
				((BgL_approxz00_bglt) BgL_srcz00_8588));
		}

	}



/* union-approx2! */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_unionzd2approx2z12zc0zzcfa_approxz00(BgL_approxz00_bglt BgL_dstz00_1060,
		BgL_approxz00_bglt BgL_srcz00_1061)
	{
		{	/* Cfa/approx.scm 118 */
			if (
				(((BgL_approxz00_bglt) COBJECT(BgL_srcz00_1061))->
					BgL_haszd2procedurezf3z21))
				{	/* Cfa/approx.scm 120 */
					((((BgL_approxz00_bglt) COBJECT(BgL_dstz00_1060))->
							BgL_haszd2procedurezf3z21) = ((bool_t) ((bool_t) 1)), BUNSPEC);
				}
			else
				{	/* Cfa/approx.scm 120 */
					BFALSE;
				}
			{	/* Cfa/approx.scm 123 */
				obj_t BgL_arg1856z00_4071;

				BgL_arg1856z00_4071 =
					BGl_getzd2srczd2approxzd2typezd2zzcfa_approxz00(BgL_srcz00_1061);
				BGl_approxzd2setzd2typez12z12zzcfa_approxz00(BgL_dstz00_1060,
					((BgL_typez00_bglt) BgL_arg1856z00_4071));
			}
			{	/* Cfa/approx.scm 124 */
				obj_t BgL_arg1857z00_4072;
				obj_t BgL_arg1858z00_4073;

				{	/* Cfa/approx.scm 124 */
					obj_t BgL_tmpz00_14062;

					BgL_tmpz00_14062 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1857z00_4072 = BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_14062);
				}
				BgL_arg1858z00_4073 =
					BGl_shapez00zztools_shapez00(((obj_t) BgL_dstz00_1060));
				{	/* Cfa/approx.scm 124 */
					obj_t BgL_list1859z00_4074;

					{	/* Cfa/approx.scm 124 */
						obj_t BgL_arg1860z00_4075;

						{	/* Cfa/approx.scm 124 */
							obj_t BgL_arg1862z00_4076;

							{	/* Cfa/approx.scm 124 */
								obj_t BgL_arg1863z00_4077;

								{	/* Cfa/approx.scm 124 */
									obj_t BgL_arg1864z00_4078;

									{	/* Cfa/approx.scm 124 */
										obj_t BgL_arg1866z00_4079;

										BgL_arg1866z00_4079 =
											MAKE_YOUNG_PAIR(BgL_arg1858z00_4073, BNIL);
										BgL_arg1864z00_4078 =
											MAKE_YOUNG_PAIR(BGl_string2224z00zzcfa_approxz00,
											BgL_arg1866z00_4079);
									}
									BgL_arg1863z00_4077 =
										MAKE_YOUNG_PAIR(BGl_string2225z00zzcfa_approxz00,
										BgL_arg1864z00_4078);
								}
								BgL_arg1862z00_4076 =
									MAKE_YOUNG_PAIR(BINT(124L), BgL_arg1863z00_4077);
							}
							BgL_arg1860z00_4075 =
								MAKE_YOUNG_PAIR(BGl_string2226z00zzcfa_approxz00,
								BgL_arg1862z00_4076);
						}
						BgL_list1859z00_4074 =
							MAKE_YOUNG_PAIR(BGl_string2227z00zzcfa_approxz00,
							BgL_arg1860z00_4075);
					}
					BGl_tprintz00zz__r4_output_6_10_3z00(BgL_arg1857z00_4072,
						BgL_list1859z00_4074);
				}
			}
			{	/* Cfa/approx.scm 126 */
				bool_t BgL_test2974z00_14075;

				{	/* Cfa/approx.scm 126 */
					BgL_typez00_bglt BgL_arg1873z00_4085;

					BgL_arg1873z00_4085 =
						(((BgL_approxz00_bglt) COBJECT(BgL_dstz00_1060))->BgL_typez00);
					BgL_test2974z00_14075 =
						(((obj_t) BgL_arg1873z00_4085) == BGl_za2_za2z00zztype_cachez00);
				}
				if (BgL_test2974z00_14075)
					{	/* Cfa/approx.scm 126 */
						BFALSE;
					}
				else
					{	/* Cfa/approx.scm 127 */
						bool_t BgL_test2975z00_14079;

						{	/* Cfa/approx.scm 127 */
							obj_t BgL_arg1872z00_4084;

							BgL_arg1872z00_4084 =
								(((BgL_approxz00_bglt) COBJECT(BgL_srcz00_1061))->
								BgL_allocsz00);
							BgL_test2975z00_14079 =
								CBOOL(BGl_setzd2anyzd2zzcfa_setz00
								(BGl_vectorzd2approxzf3zd2envzf3zzcfa_approxz00,
									BgL_arg1872z00_4084));
						}
						if (BgL_test2975z00_14079)
							{	/* Cfa/approx.scm 127 */
								BGl_approxzd2setzd2typez12z12zzcfa_approxz00(BgL_dstz00_1060,
									((BgL_typez00_bglt) BGl_za2vectorza2z00zztype_cachez00));
							}
						else
							{	/* Cfa/approx.scm 127 */
								BFALSE;
							}
					}
			}
			{	/* Cfa/approx.scm 129 */
				obj_t BgL_arg1874z00_4086;
				obj_t BgL_arg1875z00_4087;

				{	/* Cfa/approx.scm 129 */
					obj_t BgL_tmpz00_14085;

					BgL_tmpz00_14085 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1874z00_4086 = BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_14085);
				}
				BgL_arg1875z00_4087 =
					BGl_shapez00zztools_shapez00(((obj_t) BgL_dstz00_1060));
				{	/* Cfa/approx.scm 129 */
					obj_t BgL_list1876z00_4088;

					{	/* Cfa/approx.scm 129 */
						obj_t BgL_arg1877z00_4089;

						{	/* Cfa/approx.scm 129 */
							obj_t BgL_arg1878z00_4090;

							{	/* Cfa/approx.scm 129 */
								obj_t BgL_arg1879z00_4091;

								{	/* Cfa/approx.scm 129 */
									obj_t BgL_arg1880z00_4092;

									{	/* Cfa/approx.scm 129 */
										obj_t BgL_arg1882z00_4093;

										BgL_arg1882z00_4093 =
											MAKE_YOUNG_PAIR(BgL_arg1875z00_4087, BNIL);
										BgL_arg1880z00_4092 =
											MAKE_YOUNG_PAIR(BGl_string2228z00zzcfa_approxz00,
											BgL_arg1882z00_4093);
									}
									BgL_arg1879z00_4091 =
										MAKE_YOUNG_PAIR(BGl_string2225z00zzcfa_approxz00,
										BgL_arg1880z00_4092);
								}
								BgL_arg1878z00_4090 =
									MAKE_YOUNG_PAIR(BINT(129L), BgL_arg1879z00_4091);
							}
							BgL_arg1877z00_4089 =
								MAKE_YOUNG_PAIR(BGl_string2226z00zzcfa_approxz00,
								BgL_arg1878z00_4090);
						}
						BgL_list1876z00_4088 =
							MAKE_YOUNG_PAIR(BGl_string2227z00zzcfa_approxz00,
							BgL_arg1877z00_4089);
					}
					BGl_tprintz00zz__r4_output_6_10_3z00(BgL_arg1874z00_4086,
						BgL_list1876z00_4088);
				}
			}
			{	/* Cfa/approx.scm 131 */
				bool_t BgL_test2976z00_14098;

				{	/* Cfa/approx.scm 131 */
					bool_t BgL_test2977z00_14099;

					{	/* Cfa/approx.scm 131 */
						BgL_typez00_bglt BgL_arg1890z00_4101;

						BgL_arg1890z00_4101 =
							(((BgL_approxz00_bglt) COBJECT(BgL_dstz00_1060))->BgL_typez00);
						BgL_test2977z00_14099 =
							(
							((obj_t) BgL_arg1890z00_4101) ==
							BGl_za2procedureza2z00zztype_cachez00);
					}
					if (BgL_test2977z00_14099)
						{	/* Cfa/approx.scm 131 */
							BgL_test2976z00_14098 = ((bool_t) 1);
						}
					else
						{	/* Cfa/approx.scm 132 */
							BgL_typez00_bglt BgL_arg1889z00_4100;

							BgL_arg1889z00_4100 =
								(((BgL_approxz00_bglt) COBJECT(BgL_dstz00_1060))->BgL_typez00);
							BgL_test2976z00_14098 =
								(
								((obj_t) BgL_arg1889z00_4100) == BGl_za2_za2z00zztype_cachez00);
						}
				}
				if (BgL_test2976z00_14098)
					{	/* Cfa/approx.scm 131 */
						BFALSE;
					}
				else
					{	/* Cfa/approx.scm 131 */
						BGl_disablezd2Xzd2Tz12z12zzcfa_procedurez00(BgL_srcz00_1061,
							BGl_string2223z00zzcfa_approxz00);
					}
			}
			{	/* Cfa/approx.scm 134 */
				obj_t BgL_arg1891z00_4102;
				obj_t BgL_arg1892z00_4103;

				{	/* Cfa/approx.scm 134 */
					obj_t BgL_tmpz00_14107;

					BgL_tmpz00_14107 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1891z00_4102 = BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_14107);
				}
				BgL_arg1892z00_4103 =
					BGl_shapez00zztools_shapez00(((obj_t) BgL_dstz00_1060));
				{	/* Cfa/approx.scm 134 */
					obj_t BgL_list1893z00_4104;

					{	/* Cfa/approx.scm 134 */
						obj_t BgL_arg1894z00_4105;

						{	/* Cfa/approx.scm 134 */
							obj_t BgL_arg1896z00_4106;

							{	/* Cfa/approx.scm 134 */
								obj_t BgL_arg1897z00_4107;

								{	/* Cfa/approx.scm 134 */
									obj_t BgL_arg1898z00_4108;

									{	/* Cfa/approx.scm 134 */
										obj_t BgL_arg1899z00_4109;

										BgL_arg1899z00_4109 =
											MAKE_YOUNG_PAIR(BgL_arg1892z00_4103, BNIL);
										BgL_arg1898z00_4108 =
											MAKE_YOUNG_PAIR(BGl_string2229z00zzcfa_approxz00,
											BgL_arg1899z00_4109);
									}
									BgL_arg1897z00_4107 =
										MAKE_YOUNG_PAIR(BGl_string2225z00zzcfa_approxz00,
										BgL_arg1898z00_4108);
								}
								BgL_arg1896z00_4106 =
									MAKE_YOUNG_PAIR(BINT(134L), BgL_arg1897z00_4107);
							}
							BgL_arg1894z00_4105 =
								MAKE_YOUNG_PAIR(BGl_string2226z00zzcfa_approxz00,
								BgL_arg1896z00_4106);
						}
						BgL_list1893z00_4104 =
							MAKE_YOUNG_PAIR(BGl_string2227z00zzcfa_approxz00,
							BgL_arg1894z00_4105);
					}
					BGl_tprintz00zz__r4_output_6_10_3z00(BgL_arg1891z00_4102,
						BgL_list1893z00_4104);
				}
			}
			if ((((BgL_approxz00_bglt) COBJECT(BgL_srcz00_1061))->BgL_topzf3zf3))
				{	/* Cfa/approx.scm 136 */
					BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_dstz00_1060);
				}
			else
				{	/* Cfa/approx.scm 136 */
					BFALSE;
				}
			{	/* Cfa/approx.scm 138 */
				obj_t BgL_arg1901z00_4111;
				obj_t BgL_arg1902z00_4112;

				{	/* Cfa/approx.scm 138 */
					obj_t BgL_tmpz00_14123;

					BgL_tmpz00_14123 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1901z00_4111 = BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_14123);
				}
				BgL_arg1902z00_4112 =
					BGl_shapez00zztools_shapez00(((obj_t) BgL_dstz00_1060));
				{	/* Cfa/approx.scm 138 */
					obj_t BgL_list1903z00_4113;

					{	/* Cfa/approx.scm 138 */
						obj_t BgL_arg1904z00_4114;

						{	/* Cfa/approx.scm 138 */
							obj_t BgL_arg1906z00_4115;

							{	/* Cfa/approx.scm 138 */
								obj_t BgL_arg1910z00_4116;

								{	/* Cfa/approx.scm 138 */
									obj_t BgL_arg1911z00_4117;

									{	/* Cfa/approx.scm 138 */
										obj_t BgL_arg1912z00_4118;

										BgL_arg1912z00_4118 =
											MAKE_YOUNG_PAIR(BgL_arg1902z00_4112, BNIL);
										BgL_arg1911z00_4117 =
											MAKE_YOUNG_PAIR(BGl_string2230z00zzcfa_approxz00,
											BgL_arg1912z00_4118);
									}
									BgL_arg1910z00_4116 =
										MAKE_YOUNG_PAIR(BGl_string2225z00zzcfa_approxz00,
										BgL_arg1911z00_4117);
								}
								BgL_arg1906z00_4115 =
									MAKE_YOUNG_PAIR(BINT(138L), BgL_arg1910z00_4116);
							}
							BgL_arg1904z00_4114 =
								MAKE_YOUNG_PAIR(BGl_string2226z00zzcfa_approxz00,
								BgL_arg1906z00_4115);
						}
						BgL_list1903z00_4113 =
							MAKE_YOUNG_PAIR(BGl_string2227z00zzcfa_approxz00,
							BgL_arg1904z00_4114);
					}
					BGl_tprintz00zz__r4_output_6_10_3z00(BgL_arg1901z00_4111,
						BgL_list1903z00_4113);
				}
			}
			{	/* Cfa/approx.scm 140 */
				bool_t BgL_test2979z00_14136;

				{	/* Cfa/approx.scm 140 */
					obj_t BgL_arg1917z00_4123;
					obj_t BgL_arg1918z00_4124;

					BgL_arg1917z00_4123 =
						(((BgL_approxz00_bglt) COBJECT(BgL_dstz00_1060))->BgL_allocsz00);
					BgL_arg1918z00_4124 =
						(((BgL_approxz00_bglt) COBJECT(BgL_srcz00_1061))->BgL_allocsz00);
					{	/* Cfa/approx.scm 140 */
						obj_t BgL_list1919z00_4125;

						BgL_list1919z00_4125 = MAKE_YOUNG_PAIR(BgL_arg1918z00_4124, BNIL);
						BgL_test2979z00_14136 =
							CBOOL(BGl_setzd2unionz12zc0zzcfa_setz00(BgL_arg1917z00_4123,
								BgL_list1919z00_4125));
					}
				}
				if (BgL_test2979z00_14136)
					{	/* Cfa/approx.scm 140 */
						BGl_continuezd2cfaz12zc0zzcfa_iteratez00(CNST_TABLE_REF(0));
					}
				else
					{	/* Cfa/approx.scm 140 */
						BFALSE;
					}
			}
			{	/* Cfa/approx.scm 142 */
				obj_t BgL_arg1920z00_4126;
				obj_t BgL_arg1923z00_4127;

				{	/* Cfa/approx.scm 142 */
					obj_t BgL_tmpz00_14144;

					BgL_tmpz00_14144 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1920z00_4126 = BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_14144);
				}
				BgL_arg1923z00_4127 =
					BGl_shapez00zztools_shapez00(((obj_t) BgL_dstz00_1060));
				{	/* Cfa/approx.scm 142 */
					obj_t BgL_list1924z00_4128;

					{	/* Cfa/approx.scm 142 */
						obj_t BgL_arg1925z00_4129;

						{	/* Cfa/approx.scm 142 */
							obj_t BgL_arg1926z00_4130;

							{	/* Cfa/approx.scm 142 */
								obj_t BgL_arg1927z00_4131;

								{	/* Cfa/approx.scm 142 */
									obj_t BgL_arg1928z00_4132;

									{	/* Cfa/approx.scm 142 */
										obj_t BgL_arg1929z00_4133;

										BgL_arg1929z00_4133 =
											MAKE_YOUNG_PAIR(BgL_arg1923z00_4127, BNIL);
										BgL_arg1928z00_4132 =
											MAKE_YOUNG_PAIR(BGl_string2231z00zzcfa_approxz00,
											BgL_arg1929z00_4133);
									}
									BgL_arg1927z00_4131 =
										MAKE_YOUNG_PAIR(BGl_string2225z00zzcfa_approxz00,
										BgL_arg1928z00_4132);
								}
								BgL_arg1926z00_4130 =
									MAKE_YOUNG_PAIR(BINT(142L), BgL_arg1927z00_4131);
							}
							BgL_arg1925z00_4129 =
								MAKE_YOUNG_PAIR(BGl_string2226z00zzcfa_approxz00,
								BgL_arg1926z00_4130);
						}
						BgL_list1924z00_4128 =
							MAKE_YOUNG_PAIR(BGl_string2227z00zzcfa_approxz00,
							BgL_arg1925z00_4129);
					}
					BGl_tprintz00zz__r4_output_6_10_3z00(BgL_arg1920z00_4126,
						BgL_list1924z00_4128);
				}
			}
			return BgL_dstz00_1060;
		}

	}



/* &union-approx2! */
	BgL_approxz00_bglt BGl_z62unionzd2approx2z12za2zzcfa_approxz00(obj_t
		BgL_envz00_8591, obj_t BgL_dstz00_8592, obj_t BgL_srcz00_8593)
	{
		{	/* Cfa/approx.scm 118 */
			return
				BGl_unionzd2approx2z12zc0zzcfa_approxz00(
				((BgL_approxz00_bglt) BgL_dstz00_8592),
				((BgL_approxz00_bglt) BgL_srcz00_8593));
		}

	}



/* union-approx/type! */
	BgL_approxz00_bglt
		BGl_unionzd2approxzf2typez12z32zzcfa_approxz00(BgL_approxz00_bglt
		BgL_dstz00_1062, BgL_approxz00_bglt BgL_srcz00_1063,
		BgL_typez00_bglt BgL_tyz00_1064)
	{
		{	/* Cfa/approx.scm 149 */
			if ((((BgL_approxz00_bglt) COBJECT(BgL_srcz00_1063))->BgL_topzf3zf3))
				{	/* Cfa/approx.scm 162 */
					BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_dstz00_1062);
				}
			else
				{	/* Cfa/approx.scm 162 */
					BFALSE;
				}
			if (BGl_bigloozd2typezf3z21zztype_typez00(BgL_tyz00_1064))
				{	/* Cfa/approx.scm 164 */
					if ((((obj_t) BgL_tyz00_1064) == BGl_za2vectorza2z00zztype_cachez00))
						{	/* Cfa/approx.scm 166 */
							if (CBOOL(BGl_setzd2unionzf2typez12ze70zd5zzcfa_approxz00
									(BgL_dstz00_1062, BgL_srcz00_1063,
										BGl_vectorzd2approxzf3zd2envzf3zzcfa_approxz00)))
								{	/* Cfa/approx.scm 167 */
									BGl_continuezd2cfaz12zc0zzcfa_iteratez00(CNST_TABLE_REF(0));
								}
							else
								{	/* Cfa/approx.scm 167 */
									BFALSE;
								}
						}
					else
						{	/* Cfa/approx.scm 166 */
							if (
								(((obj_t) BgL_tyz00_1064) ==
									BGl_za2procedureza2z00zztype_cachez00))
								{	/* Cfa/approx.scm 169 */
									if (
										(((BgL_approxz00_bglt) COBJECT(BgL_srcz00_1063))->
											BgL_haszd2procedurezf3z21))
										{	/* Cfa/approx.scm 170 */
											((((BgL_approxz00_bglt) COBJECT(BgL_dstz00_1062))->
													BgL_haszd2procedurezf3z21) =
												((bool_t) ((bool_t) 1)), BUNSPEC);
										}
									else
										{	/* Cfa/approx.scm 170 */
											BFALSE;
										}
									if (CBOOL(BGl_setzd2unionzf2typez12ze70zd5zzcfa_approxz00
											(BgL_dstz00_1062, BgL_srcz00_1063,
												BGl_procedurezd2approxzf3zd2envzf3zzcfa_approxz00)))
										{	/* Cfa/approx.scm 172 */
											BGl_continuezd2cfaz12zc0zzcfa_iteratez00(CNST_TABLE_REF
												(0));
										}
									else
										{	/* Cfa/approx.scm 172 */
											BFALSE;
										}
								}
							else
								{	/* Cfa/approx.scm 174 */
									bool_t BgL_test2987z00_14184;

									if (
										(((obj_t) BgL_tyz00_1064) ==
											BGl_za2pairza2z00zztype_cachez00))
										{	/* Cfa/approx.scm 174 */
											BgL_test2987z00_14184 = ((bool_t) 1);
										}
									else
										{	/* Cfa/approx.scm 174 */
											if (
												(((obj_t) BgL_tyz00_1064) ==
													BGl_za2epairza2z00zztype_cachez00))
												{	/* Cfa/approx.scm 175 */
													BgL_test2987z00_14184 = ((bool_t) 1);
												}
											else
												{	/* Cfa/approx.scm 175 */
													if (
														(((obj_t) BgL_tyz00_1064) ==
															BGl_za2pairzd2nilza2zd2zztype_cachez00))
														{	/* Cfa/approx.scm 176 */
															BgL_test2987z00_14184 = ((bool_t) 1);
														}
													else
														{	/* Cfa/approx.scm 176 */
															BgL_test2987z00_14184 =
																(
																((obj_t) BgL_tyz00_1064) ==
																BGl_za2listza2z00zztype_cachez00);
														}
												}
										}
									if (BgL_test2987z00_14184)
										{	/* Cfa/approx.scm 174 */
											if (CBOOL(BGl_setzd2unionzf2typez12ze70zd5zzcfa_approxz00
													(BgL_dstz00_1062, BgL_srcz00_1063,
														BGl_conszd2approxzf3zd2envzf3zzcfa_approxz00)))
												{	/* Cfa/approx.scm 178 */
													BGl_continuezd2cfaz12zc0zzcfa_iteratez00
														(CNST_TABLE_REF(0));
												}
											else
												{	/* Cfa/approx.scm 178 */
													BFALSE;
												}
										}
									else
										{	/* Cfa/approx.scm 174 */
											if (
												(((obj_t) BgL_tyz00_1064) ==
													BGl_za2structza2z00zztype_cachez00))
												{	/* Cfa/approx.scm 180 */
													if (CBOOL
														(BGl_setzd2unionzf2typez12ze70zd5zzcfa_approxz00
															(BgL_dstz00_1062, BgL_srcz00_1063,
																BGl_structzd2approxzf3zd2envzf3zzcfa_approxz00)))
														{	/* Cfa/approx.scm 181 */
															BGl_continuezd2cfaz12zc0zzcfa_iteratez00
																(CNST_TABLE_REF(0));
														}
													else
														{	/* Cfa/approx.scm 181 */
															BFALSE;
														}
												}
											else
												{	/* Cfa/approx.scm 183 */
													bool_t BgL_test2994z00_14209;

													if (
														(((obj_t) BgL_tyz00_1064) ==
															BGl_za2symbolza2z00zztype_cachez00))
														{	/* Cfa/approx.scm 183 */
															BgL_test2994z00_14209 = ((bool_t) 1);
														}
													else
														{	/* Cfa/approx.scm 183 */
															if (
																(((obj_t) BgL_tyz00_1064) ==
																	BGl_za2keywordza2z00zztype_cachez00))
																{	/* Cfa/approx.scm 184 */
																	BgL_test2994z00_14209 = ((bool_t) 1);
																}
															else
																{	/* Cfa/approx.scm 184 */
																	if (
																		(((obj_t) BgL_tyz00_1064) ==
																			BGl_za2unspecza2z00zztype_cachez00))
																		{	/* Cfa/approx.scm 185 */
																			BgL_test2994z00_14209 = ((bool_t) 1);
																		}
																	else
																		{	/* Cfa/approx.scm 185 */
																			if (
																				(((obj_t) BgL_tyz00_1064) ==
																					BGl_za2bstringza2z00zztype_cachez00))
																				{	/* Cfa/approx.scm 186 */
																					BgL_test2994z00_14209 = ((bool_t) 1);
																				}
																			else
																				{	/* Cfa/approx.scm 186 */
																					if (
																						(((obj_t) BgL_tyz00_1064) ==
																							BGl_za2bintza2z00zztype_cachez00))
																						{	/* Cfa/approx.scm 187 */
																							BgL_test2994z00_14209 =
																								((bool_t) 1);
																						}
																					else
																						{	/* Cfa/approx.scm 187 */
																							if (
																								(((obj_t) BgL_tyz00_1064) ==
																									BGl_za2belongza2z00zztype_cachez00))
																								{	/* Cfa/approx.scm 188 */
																									BgL_test2994z00_14209 =
																										((bool_t) 1);
																								}
																							else
																								{	/* Cfa/approx.scm 188 */
																									if (
																										(((obj_t) BgL_tyz00_1064) ==
																											BGl_za2bllongza2z00zztype_cachez00))
																										{	/* Cfa/approx.scm 189 */
																											BgL_test2994z00_14209 =
																												((bool_t) 1);
																										}
																									else
																										{	/* Cfa/approx.scm 189 */
																											if (
																												(((obj_t)
																														BgL_tyz00_1064) ==
																													BGl_za2bignumza2z00zztype_cachez00))
																												{	/* Cfa/approx.scm 190 */
																													BgL_test2994z00_14209
																														= ((bool_t) 1);
																												}
																											else
																												{	/* Cfa/approx.scm 190 */
																													if (
																														(((obj_t)
																																BgL_tyz00_1064)
																															==
																															BGl_za2realza2z00zztype_cachez00))
																														{	/* Cfa/approx.scm 191 */
																															BgL_test2994z00_14209
																																= ((bool_t) 1);
																														}
																													else
																														{	/* Cfa/approx.scm 191 */
																															if (
																																(((obj_t)
																																		BgL_tyz00_1064)
																																	==
																																	BGl_za2bcharza2z00zztype_cachez00))
																																{	/* Cfa/approx.scm 192 */
																																	BgL_test2994z00_14209
																																		=
																																		((bool_t)
																																		1);
																																}
																															else
																																{	/* Cfa/approx.scm 192 */
																																	if (
																																		(((obj_t)
																																				BgL_tyz00_1064)
																																			==
																																			BGl_za2nilza2z00zz__evalz00))
																																		{	/* Cfa/approx.scm 193 */
																																			BgL_test2994z00_14209
																																				=
																																				(
																																				(bool_t)
																																				1);
																																		}
																																	else
																																		{	/* Cfa/approx.scm 193 */
																																			BgL_test2994z00_14209
																																				=
																																				(((obj_t) BgL_tyz00_1064) == BGl_za2bboolza2z00zztype_cachez00);
																																		}
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																		}
																}
														}
													if (BgL_test2994z00_14209)
														{	/* Cfa/approx.scm 183 */
															BFALSE;
														}
													else
														{	/* Cfa/approx.scm 195 */
															bool_t BgL_test3006z00_14245;

															{	/* Cfa/approx.scm 195 */
																obj_t BgL_arg1943z00_4148;
																obj_t BgL_arg1944z00_4149;

																BgL_arg1943z00_4148 =
																	(((BgL_approxz00_bglt)
																		COBJECT(BgL_dstz00_1062))->BgL_allocsz00);
																BgL_arg1944z00_4149 =
																	(((BgL_approxz00_bglt)
																		COBJECT(BgL_srcz00_1063))->BgL_allocsz00);
																{	/* Cfa/approx.scm 195 */
																	obj_t BgL_list1945z00_4150;

																	BgL_list1945z00_4150 =
																		MAKE_YOUNG_PAIR(BgL_arg1944z00_4149, BNIL);
																	BgL_test3006z00_14245 =
																		CBOOL(BGl_setzd2unionz12zc0zzcfa_setz00
																		(BgL_arg1943z00_4148,
																			BgL_list1945z00_4150));
																}
															}
															if (BgL_test3006z00_14245)
																{	/* Cfa/approx.scm 195 */
																	BGl_continuezd2cfaz12zc0zzcfa_iteratez00
																		(CNST_TABLE_REF(0));
																}
															else
																{	/* Cfa/approx.scm 195 */
																	BFALSE;
																}
														}
												}
										}
								}
						}
				}
			else
				{	/* Cfa/approx.scm 164 */
					BFALSE;
				}
			return BgL_dstz00_1062;
		}

	}



/* set-union/type!~0 */
	obj_t BGl_setzd2unionzf2typez12ze70zd5zzcfa_approxz00(BgL_approxz00_bglt
		BgL_dstz00_4151, BgL_approxz00_bglt BgL_srcz00_4152, obj_t BgL_predz00_4153)
	{
		{	/* Cfa/approx.scm 160 */
			{	/* Cfa/approx.scm 153 */
				obj_t BgL_resz00_8607;
				obj_t BgL_setz00_4156;

				BgL_resz00_8607 = MAKE_CELL(BFALSE);
				BgL_setz00_4156 =
					(((BgL_approxz00_bglt) COBJECT(BgL_dstz00_4151))->BgL_allocsz00);
				{	/* Cfa/approx.scm 155 */
					obj_t BgL_arg1948z00_4158;

					BgL_arg1948z00_4158 =
						(((BgL_approxz00_bglt) COBJECT(BgL_srcz00_4152))->BgL_allocsz00);
					{	/* Cfa/approx.scm 156 */
						obj_t BgL_zc3z04anonymousza31949ze3z87_8594;

						BgL_zc3z04anonymousza31949ze3z87_8594 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31949ze3ze5zzcfa_approxz00, (int) (1L),
							(int) (3L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31949ze3z87_8594, (int) (0L),
							BgL_setz00_4156);
						PROCEDURE_SET(BgL_zc3z04anonymousza31949ze3z87_8594, (int) (1L),
							((obj_t) BgL_resz00_8607));
						PROCEDURE_SET(BgL_zc3z04anonymousza31949ze3z87_8594, (int) (2L),
							BgL_predz00_4153);
						BGl_setzd2forzd2eachz00zzcfa_setz00
							(BgL_zc3z04anonymousza31949ze3z87_8594, BgL_arg1948z00_4158);
				}}
				return CELL_REF(BgL_resz00_8607);
			}
		}

	}



/* &<@anonymous:1949> */
	obj_t BGl_z62zc3z04anonymousza31949ze3ze5zzcfa_approxz00(obj_t
		BgL_envz00_8595, obj_t BgL_vz00_8599)
	{
		{	/* Cfa/approx.scm 155 */
			{	/* Cfa/approx.scm 156 */
				obj_t BgL_setz00_8596;
				obj_t BgL_resz00_8597;
				obj_t BgL_predz00_8598;

				BgL_setz00_8596 = PROCEDURE_REF(BgL_envz00_8595, (int) (0L));
				BgL_resz00_8597 = PROCEDURE_REF(BgL_envz00_8595, (int) (1L));
				BgL_predz00_8598 = ((obj_t) PROCEDURE_REF(BgL_envz00_8595, (int) (2L)));
				{	/* Cfa/approx.scm 156 */
					bool_t BgL_test3007z00_14273;

					if (CBOOL(BGL_PROCEDURE_CALL1(BgL_predz00_8598, BgL_vz00_8599)))
						{	/* Cfa/approx.scm 156 */
							bool_t BgL_test3009z00_14280;

							{	/* Cfa/approx.scm 156 */
								obj_t BgL_keyz00_9321;

								BgL_keyz00_9321 =
									(((BgL_nodezf2effectzf2_bglt) COBJECT(
											((BgL_nodezf2effectzf2_bglt) BgL_vz00_8599)))->
									BgL_keyz00);
								{	/* Cfa/approx.scm 156 */

									{	/* Cfa/approx.scm 156 */
										long BgL_tmpz00_14283;

										{	/* Cfa/approx.scm 156 */
											long BgL_tmpz00_14284;

											{	/* Cfa/approx.scm 156 */
												unsigned char BgL_tmpz00_14288;

												{	/* Cfa/approx.scm 156 */
													obj_t BgL_stringz00_9322;

													BgL_stringz00_9322 =
														STRUCT_REF(((obj_t) BgL_setz00_8596), (int) (0L));
													BgL_tmpz00_14288 =
														STRING_REF(BgL_stringz00_9322,
														(long) CINT(CAR(((obj_t) BgL_keyz00_9321))));
												}
												BgL_tmpz00_14284 = (BgL_tmpz00_14288);
											}
											BgL_tmpz00_14283 =
												(
												(long) CINT(CDR(
														((obj_t) BgL_keyz00_9321))) & BgL_tmpz00_14284);
										}
										BgL_test3009z00_14280 = (BgL_tmpz00_14283 > 0L);
							}}}
							if (BgL_test3009z00_14280)
								{	/* Cfa/approx.scm 156 */
									BgL_test3007z00_14273 = ((bool_t) 0);
								}
							else
								{	/* Cfa/approx.scm 156 */
									BgL_test3007z00_14273 = ((bool_t) 1);
								}
						}
					else
						{	/* Cfa/approx.scm 156 */
							BgL_test3007z00_14273 = ((bool_t) 0);
						}
					if (BgL_test3007z00_14273)
						{	/* Cfa/approx.scm 156 */
							BGl_setzd2extendz12zc0zzcfa_setz00(BgL_setz00_8596,
								BgL_vz00_8599);
							{	/* Cfa/approx.scm 158 */
								obj_t BgL_auxz00_9323;

								BgL_auxz00_9323 = BTRUE;
								return CELL_SET(BgL_resz00_8597, BgL_auxz00_9323);
							}
						}
					else
						{	/* Cfa/approx.scm 156 */
							return BFALSE;
						}
				}
			}
		}

	}



/* union-approx-filter! */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_unionzd2approxzd2filterz12z12zzcfa_approxz00(BgL_approxz00_bglt
		BgL_dstz00_1065, BgL_approxz00_bglt BgL_srcz00_1066)
	{
		{	/* Cfa/approx.scm 207 */
			{	/* Cfa/approx.scm 208 */
				BgL_typez00_bglt BgL_tyz00_4168;

				BgL_tyz00_4168 =
					(((BgL_approxz00_bglt) COBJECT(BgL_dstz00_1065))->BgL_typez00);
				{	/* Cfa/approx.scm 209 */
					bool_t BgL_test3010z00_14301;

					if (
						(((BgL_approxz00_bglt) COBJECT(BgL_dstz00_1065))->
							BgL_typezd2lockedzf3z21))
						{	/* Cfa/approx.scm 209 */
							if ((((obj_t) BgL_tyz00_4168) == BGl_za2_za2z00zztype_cachez00))
								{	/* Cfa/approx.scm 209 */
									BgL_test3010z00_14301 = ((bool_t) 1);
								}
							else
								{	/* Cfa/approx.scm 209 */
									BgL_test3010z00_14301 =
										(
										((obj_t) BgL_tyz00_4168) ==
										BGl_za2objza2z00zztype_cachez00);
								}
						}
					else
						{	/* Cfa/approx.scm 209 */
							BgL_test3010z00_14301 = ((bool_t) 1);
						}
					if (BgL_test3010z00_14301)
						{	/* Cfa/approx.scm 209 */
							BGL_TAIL return
								BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_dstz00_1065,
								BgL_srcz00_1066);
						}
					else
						{	/* Cfa/approx.scm 209 */
							return
								BGl_unionzd2approxzf2typez12z32zzcfa_approxz00(BgL_dstz00_1065,
								BgL_srcz00_1066, BgL_tyz00_4168);
						}
				}
			}
		}

	}



/* &union-approx-filter! */
	BgL_approxz00_bglt BGl_z62unionzd2approxzd2filterz12z70zzcfa_approxz00(obj_t
		BgL_envz00_8609, obj_t BgL_dstz00_8610, obj_t BgL_srcz00_8611)
	{
		{	/* Cfa/approx.scm 207 */
			return
				BGl_unionzd2approxzd2filterz12z12zzcfa_approxz00(
				((BgL_approxz00_bglt) BgL_dstz00_8610),
				((BgL_approxz00_bglt) BgL_srcz00_8611));
		}

	}



/* &vector-approx? */
	obj_t BGl_z62vectorzd2approxzf3z43zzcfa_approxz00(obj_t BgL_envz00_8589,
		obj_t BgL_xz00_8590)
	{
		{	/* Cfa/approx.scm 216 */
			{	/* Cfa/approx.scm 217 */
				bool_t BgL_tmpz00_14314;

				{	/* Cfa/approx.scm 217 */
					bool_t BgL__ortest_1301z00_9324;

					{	/* Cfa/approx.scm 217 */
						obj_t BgL_classz00_9325;

						BgL_classz00_9325 = BGl_makezd2vectorzd2appz00zzcfa_info2z00;
						if (BGL_OBJECTP(BgL_xz00_8590))
							{	/* Cfa/approx.scm 217 */
								BgL_objectz00_bglt BgL_arg1807z00_9326;

								BgL_arg1807z00_9326 = (BgL_objectz00_bglt) (BgL_xz00_8590);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cfa/approx.scm 217 */
										long BgL_idxz00_9327;

										BgL_idxz00_9327 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9326);
										BgL__ortest_1301z00_9324 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_9327 + 4L)) == BgL_classz00_9325);
									}
								else
									{	/* Cfa/approx.scm 217 */
										bool_t BgL_res2199z00_9330;

										{	/* Cfa/approx.scm 217 */
											obj_t BgL_oclassz00_9331;

											{	/* Cfa/approx.scm 217 */
												obj_t BgL_arg1815z00_9332;
												long BgL_arg1816z00_9333;

												BgL_arg1815z00_9332 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cfa/approx.scm 217 */
													long BgL_arg1817z00_9334;

													BgL_arg1817z00_9334 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9326);
													BgL_arg1816z00_9333 =
														(BgL_arg1817z00_9334 - OBJECT_TYPE);
												}
												BgL_oclassz00_9331 =
													VECTOR_REF(BgL_arg1815z00_9332, BgL_arg1816z00_9333);
											}
											{	/* Cfa/approx.scm 217 */
												bool_t BgL__ortest_1115z00_9335;

												BgL__ortest_1115z00_9335 =
													(BgL_classz00_9325 == BgL_oclassz00_9331);
												if (BgL__ortest_1115z00_9335)
													{	/* Cfa/approx.scm 217 */
														BgL_res2199z00_9330 = BgL__ortest_1115z00_9335;
													}
												else
													{	/* Cfa/approx.scm 217 */
														long BgL_odepthz00_9336;

														{	/* Cfa/approx.scm 217 */
															obj_t BgL_arg1804z00_9337;

															BgL_arg1804z00_9337 = (BgL_oclassz00_9331);
															BgL_odepthz00_9336 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_9337);
														}
														if ((4L < BgL_odepthz00_9336))
															{	/* Cfa/approx.scm 217 */
																obj_t BgL_arg1802z00_9338;

																{	/* Cfa/approx.scm 217 */
																	obj_t BgL_arg1803z00_9339;

																	BgL_arg1803z00_9339 = (BgL_oclassz00_9331);
																	BgL_arg1802z00_9338 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9339,
																		4L);
																}
																BgL_res2199z00_9330 =
																	(BgL_arg1802z00_9338 == BgL_classz00_9325);
															}
														else
															{	/* Cfa/approx.scm 217 */
																BgL_res2199z00_9330 = ((bool_t) 0);
															}
													}
											}
										}
										BgL__ortest_1301z00_9324 = BgL_res2199z00_9330;
									}
							}
						else
							{	/* Cfa/approx.scm 217 */
								BgL__ortest_1301z00_9324 = ((bool_t) 0);
							}
					}
					if (BgL__ortest_1301z00_9324)
						{	/* Cfa/approx.scm 217 */
							BgL_tmpz00_14314 = BgL__ortest_1301z00_9324;
						}
					else
						{	/* Cfa/approx.scm 217 */
							obj_t BgL_classz00_9340;

							BgL_classz00_9340 = BGl_valloczf2Cinfozb2optimz40zzcfa_info3z00;
							if (BGL_OBJECTP(BgL_xz00_8590))
								{	/* Cfa/approx.scm 217 */
									BgL_objectz00_bglt BgL_arg1809z00_9341;
									long BgL_arg1810z00_9342;

									BgL_arg1809z00_9341 = (BgL_objectz00_bglt) (BgL_xz00_8590);
									BgL_arg1810z00_9342 = BGL_CLASS_DEPTH(BgL_classz00_9340);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Cfa/approx.scm 217 */
											long BgL_idxz00_9343;

											BgL_idxz00_9343 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1809z00_9341);
											BgL_tmpz00_14314 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_9343 + BgL_arg1810z00_9342)) ==
												BgL_classz00_9340);
										}
									else
										{	/* Cfa/approx.scm 217 */
											bool_t BgL_res2200z00_9346;

											{	/* Cfa/approx.scm 217 */
												obj_t BgL_oclassz00_9347;

												{	/* Cfa/approx.scm 217 */
													obj_t BgL_arg1815z00_9348;
													long BgL_arg1816z00_9349;

													BgL_arg1815z00_9348 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Cfa/approx.scm 217 */
														long BgL_arg1817z00_9350;

														BgL_arg1817z00_9350 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1809z00_9341);
														BgL_arg1816z00_9349 =
															(BgL_arg1817z00_9350 - OBJECT_TYPE);
													}
													BgL_oclassz00_9347 =
														VECTOR_REF(BgL_arg1815z00_9348,
														BgL_arg1816z00_9349);
												}
												{	/* Cfa/approx.scm 217 */
													bool_t BgL__ortest_1115z00_9351;

													BgL__ortest_1115z00_9351 =
														(BgL_classz00_9340 == BgL_oclassz00_9347);
													if (BgL__ortest_1115z00_9351)
														{	/* Cfa/approx.scm 217 */
															BgL_res2200z00_9346 = BgL__ortest_1115z00_9351;
														}
													else
														{	/* Cfa/approx.scm 217 */
															long BgL_odepthz00_9352;

															{	/* Cfa/approx.scm 217 */
																obj_t BgL_arg1804z00_9353;

																BgL_arg1804z00_9353 = (BgL_oclassz00_9347);
																BgL_odepthz00_9352 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_9353);
															}
															if ((BgL_arg1810z00_9342 < BgL_odepthz00_9352))
																{	/* Cfa/approx.scm 217 */
																	obj_t BgL_arg1802z00_9354;

																	{	/* Cfa/approx.scm 217 */
																		obj_t BgL_arg1803z00_9355;

																		BgL_arg1803z00_9355 = (BgL_oclassz00_9347);
																		BgL_arg1802z00_9354 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_9355,
																			BgL_arg1810z00_9342);
																	}
																	BgL_res2200z00_9346 =
																		(BgL_arg1802z00_9354 == BgL_classz00_9340);
																}
															else
																{	/* Cfa/approx.scm 217 */
																	BgL_res2200z00_9346 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_tmpz00_14314 = BgL_res2200z00_9346;
										}
								}
							else
								{	/* Cfa/approx.scm 217 */
									BgL_tmpz00_14314 = ((bool_t) 0);
								}
						}
				}
				return BBOOL(BgL_tmpz00_14314);
			}
		}

	}



/* &procedure-approx? */
	obj_t BGl_z62procedurezd2approxzf3z43zzcfa_approxz00(obj_t BgL_envz00_8601,
		obj_t BgL_xz00_8602)
	{
		{	/* Cfa/approx.scm 222 */
			{	/* Cfa/approx.scm 223 */
				bool_t BgL_tmpz00_14362;

				{	/* Cfa/approx.scm 223 */
					obj_t BgL_classz00_9356;

					BgL_classz00_9356 = BGl_makezd2procedurezd2appz00zzcfa_info2z00;
					if (BGL_OBJECTP(BgL_xz00_8602))
						{	/* Cfa/approx.scm 223 */
							BgL_objectz00_bglt BgL_arg1807z00_9357;

							BgL_arg1807z00_9357 = (BgL_objectz00_bglt) (BgL_xz00_8602);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Cfa/approx.scm 223 */
									long BgL_idxz00_9358;

									BgL_idxz00_9358 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9357);
									BgL_tmpz00_14362 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_9358 + 4L)) == BgL_classz00_9356);
								}
							else
								{	/* Cfa/approx.scm 223 */
									bool_t BgL_res2201z00_9361;

									{	/* Cfa/approx.scm 223 */
										obj_t BgL_oclassz00_9362;

										{	/* Cfa/approx.scm 223 */
											obj_t BgL_arg1815z00_9363;
											long BgL_arg1816z00_9364;

											BgL_arg1815z00_9363 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Cfa/approx.scm 223 */
												long BgL_arg1817z00_9365;

												BgL_arg1817z00_9365 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9357);
												BgL_arg1816z00_9364 =
													(BgL_arg1817z00_9365 - OBJECT_TYPE);
											}
											BgL_oclassz00_9362 =
												VECTOR_REF(BgL_arg1815z00_9363, BgL_arg1816z00_9364);
										}
										{	/* Cfa/approx.scm 223 */
											bool_t BgL__ortest_1115z00_9366;

											BgL__ortest_1115z00_9366 =
												(BgL_classz00_9356 == BgL_oclassz00_9362);
											if (BgL__ortest_1115z00_9366)
												{	/* Cfa/approx.scm 223 */
													BgL_res2201z00_9361 = BgL__ortest_1115z00_9366;
												}
											else
												{	/* Cfa/approx.scm 223 */
													long BgL_odepthz00_9367;

													{	/* Cfa/approx.scm 223 */
														obj_t BgL_arg1804z00_9368;

														BgL_arg1804z00_9368 = (BgL_oclassz00_9362);
														BgL_odepthz00_9367 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_9368);
													}
													if ((4L < BgL_odepthz00_9367))
														{	/* Cfa/approx.scm 223 */
															obj_t BgL_arg1802z00_9369;

															{	/* Cfa/approx.scm 223 */
																obj_t BgL_arg1803z00_9370;

																BgL_arg1803z00_9370 = (BgL_oclassz00_9362);
																BgL_arg1802z00_9369 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9370,
																	4L);
															}
															BgL_res2201z00_9361 =
																(BgL_arg1802z00_9369 == BgL_classz00_9356);
														}
													else
														{	/* Cfa/approx.scm 223 */
															BgL_res2201z00_9361 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_tmpz00_14362 = BgL_res2201z00_9361;
								}
						}
					else
						{	/* Cfa/approx.scm 223 */
							BgL_tmpz00_14362 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_14362);
			}
		}

	}



/* &cons-approx? */
	obj_t BGl_z62conszd2approxzf3z43zzcfa_approxz00(obj_t BgL_envz00_8603,
		obj_t BgL_xz00_8604)
	{
		{	/* Cfa/approx.scm 228 */
			{	/* Cfa/approx.scm 229 */
				bool_t BgL_tmpz00_14386;

				{	/* Cfa/approx.scm 229 */
					obj_t BgL_classz00_9371;

					BgL_classz00_9371 = BGl_conszd2appzd2zzcfa_info2z00;
					if (BGL_OBJECTP(BgL_xz00_8604))
						{	/* Cfa/approx.scm 229 */
							BgL_objectz00_bglt BgL_arg1807z00_9372;

							BgL_arg1807z00_9372 = (BgL_objectz00_bglt) (BgL_xz00_8604);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Cfa/approx.scm 229 */
									long BgL_idxz00_9373;

									BgL_idxz00_9373 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9372);
									BgL_tmpz00_14386 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_9373 + 4L)) == BgL_classz00_9371);
								}
							else
								{	/* Cfa/approx.scm 229 */
									bool_t BgL_res2202z00_9376;

									{	/* Cfa/approx.scm 229 */
										obj_t BgL_oclassz00_9377;

										{	/* Cfa/approx.scm 229 */
											obj_t BgL_arg1815z00_9378;
											long BgL_arg1816z00_9379;

											BgL_arg1815z00_9378 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Cfa/approx.scm 229 */
												long BgL_arg1817z00_9380;

												BgL_arg1817z00_9380 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9372);
												BgL_arg1816z00_9379 =
													(BgL_arg1817z00_9380 - OBJECT_TYPE);
											}
											BgL_oclassz00_9377 =
												VECTOR_REF(BgL_arg1815z00_9378, BgL_arg1816z00_9379);
										}
										{	/* Cfa/approx.scm 229 */
											bool_t BgL__ortest_1115z00_9381;

											BgL__ortest_1115z00_9381 =
												(BgL_classz00_9371 == BgL_oclassz00_9377);
											if (BgL__ortest_1115z00_9381)
												{	/* Cfa/approx.scm 229 */
													BgL_res2202z00_9376 = BgL__ortest_1115z00_9381;
												}
											else
												{	/* Cfa/approx.scm 229 */
													long BgL_odepthz00_9382;

													{	/* Cfa/approx.scm 229 */
														obj_t BgL_arg1804z00_9383;

														BgL_arg1804z00_9383 = (BgL_oclassz00_9377);
														BgL_odepthz00_9382 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_9383);
													}
													if ((4L < BgL_odepthz00_9382))
														{	/* Cfa/approx.scm 229 */
															obj_t BgL_arg1802z00_9384;

															{	/* Cfa/approx.scm 229 */
																obj_t BgL_arg1803z00_9385;

																BgL_arg1803z00_9385 = (BgL_oclassz00_9377);
																BgL_arg1802z00_9384 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9385,
																	4L);
															}
															BgL_res2202z00_9376 =
																(BgL_arg1802z00_9384 == BgL_classz00_9371);
														}
													else
														{	/* Cfa/approx.scm 229 */
															BgL_res2202z00_9376 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_tmpz00_14386 = BgL_res2202z00_9376;
								}
						}
					else
						{	/* Cfa/approx.scm 229 */
							BgL_tmpz00_14386 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_14386);
			}
		}

	}



/* &struct-approx? */
	obj_t BGl_z62structzd2approxzf3z43zzcfa_approxz00(obj_t BgL_envz00_8605,
		obj_t BgL_xz00_8606)
	{
		{	/* Cfa/approx.scm 234 */
			{	/* Cfa/approx.scm 235 */
				bool_t BgL_tmpz00_14410;

				{	/* Cfa/approx.scm 235 */
					obj_t BgL_classz00_9386;

					BgL_classz00_9386 = BGl_makezd2structzd2appz00zzcfa_info2z00;
					if (BGL_OBJECTP(BgL_xz00_8606))
						{	/* Cfa/approx.scm 235 */
							BgL_objectz00_bglt BgL_arg1807z00_9387;

							BgL_arg1807z00_9387 = (BgL_objectz00_bglt) (BgL_xz00_8606);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Cfa/approx.scm 235 */
									long BgL_idxz00_9388;

									BgL_idxz00_9388 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9387);
									BgL_tmpz00_14410 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_9388 + 4L)) == BgL_classz00_9386);
								}
							else
								{	/* Cfa/approx.scm 235 */
									bool_t BgL_res2203z00_9391;

									{	/* Cfa/approx.scm 235 */
										obj_t BgL_oclassz00_9392;

										{	/* Cfa/approx.scm 235 */
											obj_t BgL_arg1815z00_9393;
											long BgL_arg1816z00_9394;

											BgL_arg1815z00_9393 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Cfa/approx.scm 235 */
												long BgL_arg1817z00_9395;

												BgL_arg1817z00_9395 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9387);
												BgL_arg1816z00_9394 =
													(BgL_arg1817z00_9395 - OBJECT_TYPE);
											}
											BgL_oclassz00_9392 =
												VECTOR_REF(BgL_arg1815z00_9393, BgL_arg1816z00_9394);
										}
										{	/* Cfa/approx.scm 235 */
											bool_t BgL__ortest_1115z00_9396;

											BgL__ortest_1115z00_9396 =
												(BgL_classz00_9386 == BgL_oclassz00_9392);
											if (BgL__ortest_1115z00_9396)
												{	/* Cfa/approx.scm 235 */
													BgL_res2203z00_9391 = BgL__ortest_1115z00_9396;
												}
											else
												{	/* Cfa/approx.scm 235 */
													long BgL_odepthz00_9397;

													{	/* Cfa/approx.scm 235 */
														obj_t BgL_arg1804z00_9398;

														BgL_arg1804z00_9398 = (BgL_oclassz00_9392);
														BgL_odepthz00_9397 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_9398);
													}
													if ((4L < BgL_odepthz00_9397))
														{	/* Cfa/approx.scm 235 */
															obj_t BgL_arg1802z00_9399;

															{	/* Cfa/approx.scm 235 */
																obj_t BgL_arg1803z00_9400;

																BgL_arg1803z00_9400 = (BgL_oclassz00_9392);
																BgL_arg1802z00_9399 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9400,
																	4L);
															}
															BgL_res2203z00_9391 =
																(BgL_arg1802z00_9399 == BgL_classz00_9386);
														}
													else
														{	/* Cfa/approx.scm 235 */
															BgL_res2203z00_9391 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_tmpz00_14410 = BgL_res2203z00_9391;
								}
						}
					else
						{	/* Cfa/approx.scm 235 */
							BgL_tmpz00_14410 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_14410);
			}
		}

	}



/* get-src-approx-type */
	obj_t BGl_getzd2srczd2approxzd2typezd2zzcfa_approxz00(BgL_approxz00_bglt
		BgL_az00_1071)
	{
		{	/* Cfa/approx.scm 244 */
			{	/* Cfa/approx.scm 245 */
				BgL_typez00_bglt BgL_typez00_4173;

				BgL_typez00_4173 =
					(((BgL_approxz00_bglt) COBJECT(BgL_az00_1071))->BgL_typez00);
				if ((((obj_t) BgL_typez00_4173) == BGl_za2_za2z00zztype_cachez00))
					{	/* Cfa/approx.scm 247 */
						return ((obj_t) BgL_typez00_4173);
					}
				else
					{	/* Cfa/approx.scm 249 */
						bool_t BgL_test3035z00_14439;

						{	/* Cfa/approx.scm 249 */
							obj_t BgL_arg1960z00_4180;

							BgL_arg1960z00_4180 =
								BGl_setzd2lengthzd2zzcfa_setz00(
								(((BgL_approxz00_bglt) COBJECT(BgL_az00_1071))->BgL_allocsz00));
							BgL_test3035z00_14439 = ((long) CINT(BgL_arg1960z00_4180) == 0L);
						}
						if (BgL_test3035z00_14439)
							{	/* Cfa/approx.scm 249 */
								return ((obj_t) BgL_typez00_4173);
							}
						else
							{	/* Cfa/approx.scm 252 */
								obj_t BgL_allocsz00_4177;

								BgL_allocsz00_4177 =
									(((BgL_approxz00_bglt) COBJECT(BgL_az00_1071))->
									BgL_allocsz00);
								if (CBOOL(BGl_setzd2anyzd2zzcfa_setz00
										(BGl_vectorzd2approxzf3zd2envzf3zzcfa_approxz00,
											BgL_allocsz00_4177)))
									{	/* Cfa/approx.scm 254 */
										bool_t BgL_test3037z00_14449;

										if (
											(((obj_t) BgL_typez00_4173) ==
												BGl_za2vectorza2z00zztype_cachez00))
											{	/* Cfa/approx.scm 254 */
												BgL_test3037z00_14449 =
													CBOOL(BGl_setzd2everyzd2zzcfa_setz00
													(BGl_vectorzd2approxzf3zd2envzf3zzcfa_approxz00,
														BgL_allocsz00_4177));
											}
										else
											{	/* Cfa/approx.scm 254 */
												BgL_test3037z00_14449 = ((bool_t) 0);
											}
										if (BgL_test3037z00_14449)
											{	/* Cfa/approx.scm 254 */
												return BGl_za2vectorza2z00zztype_cachez00;
											}
										else
											{	/* Cfa/approx.scm 254 */
												return BGl_za2objza2z00zztype_cachez00;
											}
									}
								else
									{	/* Cfa/approx.scm 253 */
										return ((obj_t) BgL_typez00_4173);
									}
							}
					}
			}
		}

	}



/* approx-set-type! */
	BGL_EXPORTED_DEF obj_t
		BGl_approxzd2setzd2typez12z12zzcfa_approxz00(BgL_approxz00_bglt
		BgL_dstz00_1072, BgL_typez00_bglt BgL_typez00_1073)
	{
		{	/* Cfa/approx.scm 266 */
			{	/* Cfa/approx.scm 267 */
				BgL_typez00_bglt BgL_dtypez00_4182;

				BgL_dtypez00_4182 =
					(((BgL_approxz00_bglt) COBJECT(BgL_dstz00_1072))->BgL_typez00);
				if (
					(((BgL_approxz00_bglt) COBJECT(BgL_dstz00_1072))->
						BgL_typezd2lockedzf3z21))
					{	/* Cfa/approx.scm 269 */
						return BFALSE;
					}
				else
					{	/* Cfa/approx.scm 269 */
						if ((((obj_t) BgL_typez00_1073) == BGl_za2_za2z00zztype_cachez00))
							{	/* Cfa/approx.scm 271 */
								return BFALSE;
							}
						else
							{	/* Cfa/approx.scm 271 */
								if ((((obj_t) BgL_dtypez00_4182) == ((obj_t) BgL_typez00_1073)))
									{	/* Cfa/approx.scm 273 */
										return BFALSE;
									}
								else
									{	/* Cfa/approx.scm 275 */
										bool_t BgL_test3042z00_14466;

										{	/* Cfa/approx.scm 275 */
											bool_t BgL_test3043z00_14467;

											if (
												(((obj_t) BgL_dtypez00_4182) ==
													BGl_za2longza2z00zztype_cachez00))
												{	/* Cfa/approx.scm 275 */
													BgL_test3043z00_14467 =
														(
														((obj_t) BgL_typez00_1073) ==
														BGl_za2intza2z00zztype_cachez00);
												}
											else
												{	/* Cfa/approx.scm 275 */
													BgL_test3043z00_14467 = ((bool_t) 0);
												}
											if (BgL_test3043z00_14467)
												{	/* Cfa/approx.scm 275 */
													BgL_test3042z00_14466 = ((bool_t) 1);
												}
											else
												{	/* Cfa/approx.scm 276 */
													bool_t BgL_test3045z00_14473;

													if (
														(((obj_t) BgL_dtypez00_4182) ==
															BGl_za2intza2z00zztype_cachez00))
														{	/* Cfa/approx.scm 276 */
															BgL_test3045z00_14473 =
																(
																((obj_t) BgL_typez00_1073) ==
																BGl_za2longza2z00zztype_cachez00);
														}
													else
														{	/* Cfa/approx.scm 276 */
															BgL_test3045z00_14473 = ((bool_t) 0);
														}
													if (BgL_test3045z00_14473)
														{	/* Cfa/approx.scm 276 */
															BgL_test3042z00_14466 = ((bool_t) 1);
														}
													else
														{	/* Cfa/approx.scm 276 */
															if (
																(((obj_t) BgL_dtypez00_4182) ==
																	BGl_za2bintza2z00zztype_cachez00))
																{	/* Cfa/approx.scm 277 */
																	bool_t BgL__ortest_1303z00_4243;

																	BgL__ortest_1303z00_4243 =
																		(
																		((obj_t) BgL_typez00_1073) ==
																		BGl_za2intza2z00zztype_cachez00);
																	if (BgL__ortest_1303z00_4243)
																		{	/* Cfa/approx.scm 277 */
																			BgL_test3042z00_14466 =
																				BgL__ortest_1303z00_4243;
																		}
																	else
																		{	/* Cfa/approx.scm 277 */
																			BgL_test3042z00_14466 =
																				(
																				((obj_t) BgL_typez00_1073) ==
																				BGl_za2longza2z00zztype_cachez00);
																		}
																}
															else
																{	/* Cfa/approx.scm 277 */
																	BgL_test3042z00_14466 = ((bool_t) 0);
																}
														}
												}
										}
										if (BgL_test3042z00_14466)
											{	/* Cfa/approx.scm 275 */
												return BFALSE;
											}
										else
											{	/* Cfa/approx.scm 280 */
												bool_t BgL_test3049z00_14487;

												{	/* Cfa/approx.scm 280 */
													bool_t BgL_test3050z00_14488;

													if (
														(((obj_t) BgL_dtypez00_4182) ==
															BGl_za2longza2z00zztype_cachez00))
														{	/* Cfa/approx.scm 280 */
															BgL_test3050z00_14488 = ((bool_t) 1);
														}
													else
														{	/* Cfa/approx.scm 280 */
															BgL_test3050z00_14488 =
																(
																((obj_t) BgL_dtypez00_4182) ==
																BGl_za2intza2z00zztype_cachez00);
														}
													if (BgL_test3050z00_14488)
														{	/* Cfa/approx.scm 280 */
															BgL_test3049z00_14487 =
																(
																((obj_t) BgL_typez00_1073) ==
																BGl_za2bintza2z00zztype_cachez00);
														}
													else
														{	/* Cfa/approx.scm 280 */
															BgL_test3049z00_14487 = ((bool_t) 0);
														}
												}
												if (BgL_test3049z00_14487)
													{	/* Cfa/approx.scm 280 */
														{	/* Cfa/approx.scm 281 */
															BgL_typez00_bglt BgL_vz00_6601;

															BgL_vz00_6601 =
																((BgL_typez00_bglt)
																BGl_za2bintza2z00zztype_cachez00);
															((((BgL_approxz00_bglt)
																		COBJECT(BgL_dstz00_1072))->BgL_typez00) =
																((BgL_typez00_bglt) BgL_vz00_6601), BUNSPEC);
														}
														return BTRUE;
													}
												else
													{	/* Cfa/approx.scm 283 */
														bool_t BgL_test3052z00_14498;

														if (
															(((obj_t) BgL_dtypez00_4182) ==
																BGl_za2epairza2z00zztype_cachez00))
															{	/* Cfa/approx.scm 283 */
																BgL_test3052z00_14498 =
																	(
																	((obj_t) BgL_typez00_1073) ==
																	BGl_za2pairza2z00zztype_cachez00);
															}
														else
															{	/* Cfa/approx.scm 283 */
																BgL_test3052z00_14498 = ((bool_t) 0);
															}
														if (BgL_test3052z00_14498)
															{	/* Cfa/approx.scm 283 */
																{	/* Cfa/approx.scm 285 */
																	BgL_typez00_bglt BgL_vz00_6603;

																	BgL_vz00_6603 =
																		((BgL_typez00_bglt)
																		BGl_za2pairza2z00zztype_cachez00);
																	((((BgL_approxz00_bglt)
																				COBJECT(BgL_dstz00_1072))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) BgL_vz00_6603),
																		BUNSPEC);
																}
																return
																	BGl_continuezd2cfaz12zc0zzcfa_iteratez00
																	(CNST_TABLE_REF(1));
															}
														else
															{	/* Cfa/approx.scm 287 */
																bool_t BgL_test3054z00_14508;

																if (
																	(((obj_t) BgL_dtypez00_4182) ==
																		BGl_za2pairzd2nilza2zd2zztype_cachez00))
																	{	/* Cfa/approx.scm 288 */
																		bool_t BgL__ortest_1304z00_4236;

																		BgL__ortest_1304z00_4236 =
																			(
																			((obj_t) BgL_typez00_1073) ==
																			BGl_za2bnilza2z00zztype_cachez00);
																		if (BgL__ortest_1304z00_4236)
																			{	/* Cfa/approx.scm 288 */
																				BgL_test3054z00_14508 =
																					BgL__ortest_1304z00_4236;
																			}
																		else
																			{	/* Cfa/approx.scm 289 */
																				bool_t BgL__ortest_1305z00_4237;

																				BgL__ortest_1305z00_4237 =
																					(
																					((obj_t) BgL_typez00_1073) ==
																					BGl_za2pairza2z00zztype_cachez00);
																				if (BgL__ortest_1305z00_4237)
																					{	/* Cfa/approx.scm 289 */
																						BgL_test3054z00_14508 =
																							BgL__ortest_1305z00_4237;
																					}
																				else
																					{	/* Cfa/approx.scm 289 */
																						bool_t BgL__ortest_1306z00_4238;

																						BgL__ortest_1306z00_4238 =
																							(
																							((obj_t) BgL_typez00_1073) ==
																							BGl_za2epairza2z00zztype_cachez00);
																						if (BgL__ortest_1306z00_4238)
																							{	/* Cfa/approx.scm 289 */
																								BgL_test3054z00_14508 =
																									BgL__ortest_1306z00_4238;
																							}
																						else
																							{	/* Cfa/approx.scm 289 */
																								BgL_test3054z00_14508 =
																									(
																									((obj_t) BgL_typez00_1073) ==
																									BGl_za2listza2z00zztype_cachez00);
																							}
																					}
																			}
																	}
																else
																	{	/* Cfa/approx.scm 287 */
																		BgL_test3054z00_14508 = ((bool_t) 0);
																	}
																if (BgL_test3054z00_14508)
																	{	/* Cfa/approx.scm 287 */
																		return BFALSE;
																	}
																else
																	{	/* Cfa/approx.scm 293 */
																		bool_t BgL_test3059z00_14523;

																		if (BGl_bigloozd2typezf3z21zztype_typez00
																			(BgL_dtypez00_4182))
																			{	/* Cfa/approx.scm 293 */
																				BgL_typez00_bglt BgL_arg1990z00_4235;

																				BgL_arg1990z00_4235 =
																					BGl_getzd2bigloozd2typez00zztype_cachez00
																					(BgL_typez00_1073);
																				BgL_test3059z00_14523 =
																					(((obj_t) BgL_dtypez00_4182) ==
																					((obj_t) BgL_arg1990z00_4235));
																			}
																		else
																			{	/* Cfa/approx.scm 293 */
																				BgL_test3059z00_14523 = ((bool_t) 0);
																			}
																		if (BgL_test3059z00_14523)
																			{	/* Cfa/approx.scm 293 */
																				return BTRUE;
																			}
																		else
																			{	/* Cfa/approx.scm 297 */
																				bool_t BgL_test3061z00_14530;

																				{	/* Cfa/approx.scm 297 */
																					bool_t BgL_test3062z00_14531;

																					if (BGl_bigloozd2typezf3z21zztype_typez00(BgL_dtypez00_4182))
																						{	/* Cfa/approx.scm 297 */
																							BgL_typez00_bglt
																								BgL_arg1989z00_4233;
																							BgL_arg1989z00_4233 =
																								BGl_getzd2bigloozd2typez00zztype_cachez00
																								(BgL_typez00_1073);
																							BgL_test3062z00_14531 =
																								(((obj_t) BgL_dtypez00_4182) ==
																								((obj_t) BgL_arg1989z00_4233));
																						}
																					else
																						{	/* Cfa/approx.scm 297 */
																							BgL_test3062z00_14531 =
																								((bool_t) 0);
																						}
																					if (BgL_test3062z00_14531)
																						{	/* Cfa/approx.scm 297 */
																							BgL_test3061z00_14530 =
																								((bool_t) 1);
																						}
																					else
																						{	/* Cfa/approx.scm 297 */
																							if (BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_1073))
																								{	/* Cfa/approx.scm 298 */
																									BgL_typez00_bglt
																										BgL_arg1988z00_4231;
																									BgL_arg1988z00_4231 =
																										BGl_getzd2bigloozd2typez00zztype_cachez00
																										(BgL_dtypez00_4182);
																									BgL_test3061z00_14530 =
																										(((obj_t) BgL_typez00_1073)
																										==
																										((obj_t)
																											BgL_arg1988z00_4231));
																								}
																							else
																								{	/* Cfa/approx.scm 298 */
																									BgL_test3061z00_14530 =
																										((bool_t) 0);
																								}
																						}
																				}
																				if (BgL_test3061z00_14530)
																					{	/* Cfa/approx.scm 297 */
																						if (
																							(((obj_t) BgL_typez00_1073) ==
																								BGl_za2objza2z00zztype_cachez00))
																							{	/* Cfa/approx.scm 300 */
																								((((BgL_approxz00_bglt)
																											COBJECT
																											(BgL_dstz00_1072))->
																										BgL_typez00) =
																									((BgL_typez00_bglt)
																										BgL_typez00_1073), BUNSPEC);
																							}
																						else
																							{	/* Cfa/approx.scm 300 */
																								BFALSE;
																							}
																						return BTRUE;
																					}
																				else
																					{	/* Cfa/approx.scm 303 */
																						bool_t BgL_test3066z00_14548;

																						{	/* Cfa/approx.scm 303 */
																							bool_t BgL_test3067z00_14549;

																							if (
																								(((obj_t) BgL_dtypez00_4182) ==
																									BGl_za2bnilza2z00zztype_cachez00))
																								{	/* Cfa/approx.scm 303 */
																									BgL_test3067z00_14549 =
																										((bool_t) 1);
																								}
																							else
																								{	/* Cfa/approx.scm 303 */
																									if (
																										(((obj_t) BgL_dtypez00_4182)
																											==
																											BGl_za2pairza2z00zztype_cachez00))
																										{	/* Cfa/approx.scm 303 */
																											BgL_test3067z00_14549 =
																												((bool_t) 1);
																										}
																									else
																										{	/* Cfa/approx.scm 303 */
																											BgL_test3067z00_14549 =
																												(
																												((obj_t)
																													BgL_dtypez00_4182) ==
																												BGl_za2epairza2z00zztype_cachez00);
																										}
																								}
																							if (BgL_test3067z00_14549)
																								{	/* Cfa/approx.scm 304 */
																									bool_t
																										BgL__ortest_1308z00_4225;
																									BgL__ortest_1308z00_4225 =
																										(((obj_t) BgL_typez00_1073)
																										==
																										BGl_za2bnilza2z00zztype_cachez00);
																									if (BgL__ortest_1308z00_4225)
																										{	/* Cfa/approx.scm 304 */
																											BgL_test3066z00_14548 =
																												BgL__ortest_1308z00_4225;
																										}
																									else
																										{	/* Cfa/approx.scm 305 */
																											bool_t
																												BgL__ortest_1309z00_4226;
																											BgL__ortest_1309z00_4226 =
																												(((obj_t)
																													BgL_typez00_1073) ==
																												BGl_za2pairza2z00zztype_cachez00);
																											if (BgL__ortest_1309z00_4226)
																												{	/* Cfa/approx.scm 305 */
																													BgL_test3066z00_14548
																														=
																														BgL__ortest_1309z00_4226;
																												}
																											else
																												{	/* Cfa/approx.scm 305 */
																													bool_t
																														BgL__ortest_1310z00_4227;
																													BgL__ortest_1310z00_4227
																														=
																														(((obj_t)
																															BgL_typez00_1073)
																														==
																														BGl_za2epairza2z00zztype_cachez00);
																													if (BgL__ortest_1310z00_4227)
																														{	/* Cfa/approx.scm 305 */
																															BgL_test3066z00_14548
																																=
																																BgL__ortest_1310z00_4227;
																														}
																													else
																														{	/* Cfa/approx.scm 306 */
																															bool_t
																																BgL__ortest_1311z00_4228;
																															BgL__ortest_1311z00_4228
																																=
																																(((obj_t)
																																	BgL_typez00_1073)
																																==
																																BGl_za2pairzd2nilza2zd2zztype_cachez00);
																															if (BgL__ortest_1311z00_4228)
																																{	/* Cfa/approx.scm 306 */
																																	BgL_test3066z00_14548
																																		=
																																		BgL__ortest_1311z00_4228;
																																}
																															else
																																{	/* Cfa/approx.scm 306 */
																																	BgL_test3066z00_14548
																																		=
																																		(((obj_t)
																																			BgL_typez00_1073)
																																		==
																																		BGl_za2listza2z00zztype_cachez00);
																																}
																														}
																												}
																										}
																								}
																							else
																								{	/* Cfa/approx.scm 303 */
																									BgL_test3066z00_14548 =
																										((bool_t) 0);
																								}
																						}
																						if (BgL_test3066z00_14548)
																							{	/* Cfa/approx.scm 303 */
																								{	/* Cfa/approx.scm 309 */
																									BgL_typez00_bglt
																										BgL_vz00_6607;
																									BgL_vz00_6607 =
																										((BgL_typez00_bglt)
																										BGl_za2pairzd2nilza2zd2zztype_cachez00);
																									((((BgL_approxz00_bglt)
																												COBJECT
																												(BgL_dstz00_1072))->
																											BgL_typez00) =
																										((BgL_typez00_bglt)
																											BgL_vz00_6607), BUNSPEC);
																								}
																								return
																									BGl_continuezd2cfaz12zc0zzcfa_iteratez00
																									(CNST_TABLE_REF(1));
																							}
																						else
																							{	/* Cfa/approx.scm 311 */
																								bool_t BgL_test3074z00_14576;

																								{	/* Cfa/approx.scm 311 */
																									bool_t BgL_test3075z00_14577;

																									{	/* Cfa/approx.scm 311 */
																										obj_t BgL_classz00_6608;

																										BgL_classz00_6608 =
																											BGl_tclassz00zzobject_classz00;
																										{	/* Cfa/approx.scm 311 */
																											BgL_objectz00_bglt
																												BgL_arg1807z00_6610;
																											{	/* Cfa/approx.scm 311 */
																												obj_t BgL_tmpz00_14578;

																												BgL_tmpz00_14578 =
																													((obj_t)
																													((BgL_objectz00_bglt)
																														BgL_dtypez00_4182));
																												BgL_arg1807z00_6610 =
																													(BgL_objectz00_bglt)
																													(BgL_tmpz00_14578);
																											}
																											if (BGL_CONDEXPAND_ISA_ARCH64())
																												{	/* Cfa/approx.scm 311 */
																													long BgL_idxz00_6616;

																													BgL_idxz00_6616 =
																														BGL_OBJECT_INHERITANCE_NUM
																														(BgL_arg1807z00_6610);
																													BgL_test3075z00_14577
																														=
																														(VECTOR_REF
																														(BGl_za2inheritancesza2z00zz__objectz00,
																															(BgL_idxz00_6616 +
																																2L)) ==
																														BgL_classz00_6608);
																												}
																											else
																												{	/* Cfa/approx.scm 311 */
																													bool_t
																														BgL_res2204z00_6641;
																													{	/* Cfa/approx.scm 311 */
																														obj_t
																															BgL_oclassz00_6624;
																														{	/* Cfa/approx.scm 311 */
																															obj_t
																																BgL_arg1815z00_6632;
																															long
																																BgL_arg1816z00_6633;
																															BgL_arg1815z00_6632
																																=
																																(BGl_za2classesza2z00zz__objectz00);
																															{	/* Cfa/approx.scm 311 */
																																long
																																	BgL_arg1817z00_6634;
																																BgL_arg1817z00_6634
																																	=
																																	BGL_OBJECT_CLASS_NUM
																																	(BgL_arg1807z00_6610);
																																BgL_arg1816z00_6633
																																	=
																																	(BgL_arg1817z00_6634
																																	-
																																	OBJECT_TYPE);
																															}
																															BgL_oclassz00_6624
																																=
																																VECTOR_REF
																																(BgL_arg1815z00_6632,
																																BgL_arg1816z00_6633);
																														}
																														{	/* Cfa/approx.scm 311 */
																															bool_t
																																BgL__ortest_1115z00_6625;
																															BgL__ortest_1115z00_6625
																																=
																																(BgL_classz00_6608
																																==
																																BgL_oclassz00_6624);
																															if (BgL__ortest_1115z00_6625)
																																{	/* Cfa/approx.scm 311 */
																																	BgL_res2204z00_6641
																																		=
																																		BgL__ortest_1115z00_6625;
																																}
																															else
																																{	/* Cfa/approx.scm 311 */
																																	long
																																		BgL_odepthz00_6626;
																																	{	/* Cfa/approx.scm 311 */
																																		obj_t
																																			BgL_arg1804z00_6627;
																																		BgL_arg1804z00_6627
																																			=
																																			(BgL_oclassz00_6624);
																																		BgL_odepthz00_6626
																																			=
																																			BGL_CLASS_DEPTH
																																			(BgL_arg1804z00_6627);
																																	}
																																	if (
																																		(2L <
																																			BgL_odepthz00_6626))
																																		{	/* Cfa/approx.scm 311 */
																																			obj_t
																																				BgL_arg1802z00_6629;
																																			{	/* Cfa/approx.scm 311 */
																																				obj_t
																																					BgL_arg1803z00_6630;
																																				BgL_arg1803z00_6630
																																					=
																																					(BgL_oclassz00_6624);
																																				BgL_arg1802z00_6629
																																					=
																																					BGL_CLASS_ANCESTORS_REF
																																					(BgL_arg1803z00_6630,
																																					2L);
																																			}
																																			BgL_res2204z00_6641
																																				=
																																				(BgL_arg1802z00_6629
																																				==
																																				BgL_classz00_6608);
																																		}
																																	else
																																		{	/* Cfa/approx.scm 311 */
																																			BgL_res2204z00_6641
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																														}
																													}
																													BgL_test3075z00_14577
																														=
																														BgL_res2204z00_6641;
																												}
																										}
																									}
																									if (BgL_test3075z00_14577)
																										{	/* Cfa/approx.scm 311 */
																											obj_t BgL_classz00_6642;

																											BgL_classz00_6642 =
																												BGl_tclassz00zzobject_classz00;
																											{	/* Cfa/approx.scm 311 */
																												BgL_objectz00_bglt
																													BgL_arg1807z00_6644;
																												{	/* Cfa/approx.scm 311 */
																													obj_t
																														BgL_tmpz00_14601;
																													BgL_tmpz00_14601 =
																														((obj_t)
																														BgL_typez00_1073);
																													BgL_arg1807z00_6644 =
																														(BgL_objectz00_bglt)
																														(BgL_tmpz00_14601);
																												}
																												if (BGL_CONDEXPAND_ISA_ARCH64())
																													{	/* Cfa/approx.scm 311 */
																														long
																															BgL_idxz00_6650;
																														BgL_idxz00_6650 =
																															BGL_OBJECT_INHERITANCE_NUM
																															(BgL_arg1807z00_6644);
																														BgL_test3074z00_14576
																															=
																															(VECTOR_REF
																															(BGl_za2inheritancesza2z00zz__objectz00,
																																(BgL_idxz00_6650
																																	+ 2L)) ==
																															BgL_classz00_6642);
																													}
																												else
																													{	/* Cfa/approx.scm 311 */
																														bool_t
																															BgL_res2205z00_6675;
																														{	/* Cfa/approx.scm 311 */
																															obj_t
																																BgL_oclassz00_6658;
																															{	/* Cfa/approx.scm 311 */
																																obj_t
																																	BgL_arg1815z00_6666;
																																long
																																	BgL_arg1816z00_6667;
																																BgL_arg1815z00_6666
																																	=
																																	(BGl_za2classesza2z00zz__objectz00);
																																{	/* Cfa/approx.scm 311 */
																																	long
																																		BgL_arg1817z00_6668;
																																	BgL_arg1817z00_6668
																																		=
																																		BGL_OBJECT_CLASS_NUM
																																		(BgL_arg1807z00_6644);
																																	BgL_arg1816z00_6667
																																		=
																																		(BgL_arg1817z00_6668
																																		-
																																		OBJECT_TYPE);
																																}
																																BgL_oclassz00_6658
																																	=
																																	VECTOR_REF
																																	(BgL_arg1815z00_6666,
																																	BgL_arg1816z00_6667);
																															}
																															{	/* Cfa/approx.scm 311 */
																																bool_t
																																	BgL__ortest_1115z00_6659;
																																BgL__ortest_1115z00_6659
																																	=
																																	(BgL_classz00_6642
																																	==
																																	BgL_oclassz00_6658);
																																if (BgL__ortest_1115z00_6659)
																																	{	/* Cfa/approx.scm 311 */
																																		BgL_res2205z00_6675
																																			=
																																			BgL__ortest_1115z00_6659;
																																	}
																																else
																																	{	/* Cfa/approx.scm 311 */
																																		long
																																			BgL_odepthz00_6660;
																																		{	/* Cfa/approx.scm 311 */
																																			obj_t
																																				BgL_arg1804z00_6661;
																																			BgL_arg1804z00_6661
																																				=
																																				(BgL_oclassz00_6658);
																																			BgL_odepthz00_6660
																																				=
																																				BGL_CLASS_DEPTH
																																				(BgL_arg1804z00_6661);
																																		}
																																		if (
																																			(2L <
																																				BgL_odepthz00_6660))
																																			{	/* Cfa/approx.scm 311 */
																																				obj_t
																																					BgL_arg1802z00_6663;
																																				{	/* Cfa/approx.scm 311 */
																																					obj_t
																																						BgL_arg1803z00_6664;
																																					BgL_arg1803z00_6664
																																						=
																																						(BgL_oclassz00_6658);
																																					BgL_arg1802z00_6663
																																						=
																																						BGL_CLASS_ANCESTORS_REF
																																						(BgL_arg1803z00_6664,
																																						2L);
																																				}
																																				BgL_res2205z00_6675
																																					=
																																					(BgL_arg1802z00_6663
																																					==
																																					BgL_classz00_6642);
																																			}
																																		else
																																			{	/* Cfa/approx.scm 311 */
																																				BgL_res2205z00_6675
																																					=
																																					(
																																					(bool_t)
																																					0);
																																			}
																																	}
																															}
																														}
																														BgL_test3074z00_14576
																															=
																															BgL_res2205z00_6675;
																													}
																											}
																										}
																									else
																										{	/* Cfa/approx.scm 311 */
																											BgL_test3074z00_14576 =
																												((bool_t) 0);
																										}
																								}
																								if (BgL_test3074z00_14576)
																									{	/* Cfa/approx.scm 312 */
																										obj_t BgL_superz00_4215;

																										BgL_superz00_4215 =
																											BGl_findzd2commonzd2superzd2classzd2zzobject_classz00
																											(((BgL_typez00_bglt)
																												BgL_dtypez00_4182),
																											((BgL_typez00_bglt)
																												BgL_typez00_1073));
																										if ((BgL_superz00_4215 ==
																												((obj_t)
																													BgL_dtypez00_4182)))
																											{	/* Cfa/approx.scm 314 */
																												return BFALSE;
																											}
																										else
																											{	/* Cfa/approx.scm 314 */
																												if (CBOOL
																													(BgL_superz00_4215))
																													{	/* Cfa/approx.scm 316 */
																														((((BgL_approxz00_bglt) COBJECT(BgL_dstz00_1072))->BgL_typez00) = ((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_superz00_4215)), BUNSPEC);
																														return
																															BGl_continuezd2cfaz12zc0zzcfa_iteratez00
																															(CNST_TABLE_REF
																															(1));
																													}
																												else
																													{	/* Cfa/approx.scm 316 */
																														{	/* Cfa/approx.scm 321 */
																															BgL_typez00_bglt
																																BgL_vz00_6679;
																															BgL_vz00_6679 =
																																(
																																(BgL_typez00_bglt)
																																BGl_za2objza2z00zztype_cachez00);
																															((((BgL_approxz00_bglt) COBJECT(BgL_dstz00_1072))->BgL_typez00) = ((BgL_typez00_bglt) BgL_vz00_6679), BUNSPEC);
																														}
																														return
																															BGl_continuezd2cfaz12zc0zzcfa_iteratez00
																															(CNST_TABLE_REF
																															(1));
																													}
																											}
																									}
																								else
																									{	/* Cfa/approx.scm 311 */
																										if (
																											(((obj_t)
																													BgL_dtypez00_4182) ==
																												BGl_za2objza2z00zztype_cachez00))
																											{	/* Cfa/approx.scm 323 */
																												return BFALSE;
																											}
																										else
																											{	/* Cfa/approx.scm 325 */
																												bool_t
																													BgL_test3085z00_14642;
																												if (BGl_bigloozd2typezf3z21zztype_typez00(BgL_dtypez00_4182))
																													{	/* Cfa/approx.scm 325 */
																														BgL_test3085z00_14642
																															= ((bool_t) 0);
																													}
																												else
																													{	/* Cfa/approx.scm 325 */
																														if (BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_1073))
																															{	/* Cfa/approx.scm 325 */
																																BgL_test3085z00_14642
																																	=
																																	((bool_t) 0);
																															}
																														else
																															{	/* Cfa/approx.scm 325 */
																																BgL_test3085z00_14642
																																	=
																																	((bool_t) 1);
																															}
																													}
																												if (BgL_test3085z00_14642)
																													{	/* Cfa/approx.scm 325 */
																														if (BGl_czd2subtypezf3z21zztype_miscz00(BgL_typez00_1073, BgL_dtypez00_4182))
																															{	/* Cfa/approx.scm 327 */
																																return BFALSE;
																															}
																														else
																															{	/* Cfa/approx.scm 327 */
																																if (BGl_czd2subtypezf3z21zztype_miscz00(BgL_dtypez00_4182, BgL_typez00_1073))
																																	{	/* Cfa/approx.scm 329 */
																																		((((BgL_approxz00_bglt) COBJECT(BgL_dstz00_1072))->BgL_typez00) = ((BgL_typez00_bglt) BgL_typez00_1073), BUNSPEC);
																																		return
																																			BGl_continuezd2cfaz12zc0zzcfa_iteratez00
																																			(CNST_TABLE_REF
																																			(1));
																																	}
																																else
																																	{	/* Cfa/approx.scm 329 */
																																		{	/* Cfa/approx.scm 333 */
																																			BgL_typez00_bglt
																																				BgL_vz00_6683;
																																			BgL_vz00_6683
																																				=
																																				(
																																				(BgL_typez00_bglt)
																																				BGl_za2objza2z00zztype_cachez00);
																																			((((BgL_approxz00_bglt) COBJECT(BgL_dstz00_1072))->BgL_typez00) = ((BgL_typez00_bglt) BgL_vz00_6683), BUNSPEC);
																																		}
																																		return
																																			BGl_continuezd2cfaz12zc0zzcfa_iteratez00
																																			(CNST_TABLE_REF
																																			(1));
																																	}
																															}
																													}
																												else
																													{	/* Cfa/approx.scm 325 */
																														if (
																															(((obj_t)
																																	BgL_dtypez00_4182)
																																==
																																BGl_za2_za2z00zztype_cachez00))
																															{	/* Cfa/approx.scm 335 */
																																((((BgL_approxz00_bglt) COBJECT(BgL_dstz00_1072))->BgL_typez00) = ((BgL_typez00_bglt) BgL_typez00_1073), BUNSPEC);
																																return
																																	BGl_continuezd2cfaz12zc0zzcfa_iteratez00
																																	(CNST_TABLE_REF
																																	(1));
																															}
																														else
																															{	/* Cfa/approx.scm 335 */
																																{	/* Cfa/approx.scm 339 */
																																	BgL_typez00_bglt
																																		BgL_vz00_6687;
																																	BgL_vz00_6687
																																		=
																																		(
																																		(BgL_typez00_bglt)
																																		BGl_za2objza2z00zztype_cachez00);
																																	((((BgL_approxz00_bglt) COBJECT(BgL_dstz00_1072))->BgL_typez00) = ((BgL_typez00_bglt) BgL_vz00_6687), BUNSPEC);
																																}
																																return
																																	BGl_continuezd2cfaz12zc0zzcfa_iteratez00
																																	(CNST_TABLE_REF
																																	(1));
																															}
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* &approx-set-type! */
	obj_t BGl_z62approxzd2setzd2typez12z70zzcfa_approxz00(obj_t BgL_envz00_8612,
		obj_t BgL_dstz00_8613, obj_t BgL_typez00_8614)
	{
		{	/* Cfa/approx.scm 266 */
			return
				BGl_approxzd2setzd2typez12z12zzcfa_approxz00(
				((BgL_approxz00_bglt) BgL_dstz00_8613),
				((BgL_typez00_bglt) BgL_typez00_8614));
		}

	}



/* approx-set-top! */
	BGL_EXPORTED_DEF obj_t
		BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_approxz00_bglt
		BgL_dstz00_1074)
	{
		{	/* Cfa/approx.scm 345 */
		BGl_approxzd2setzd2topz12z12zzcfa_approxz00:
			if ((((BgL_approxz00_bglt) COBJECT(BgL_dstz00_1074))->BgL_topzf3zf3))
				{	/* Cfa/approx.scm 347 */
					BFALSE;
				}
			else
				{	/* Cfa/approx.scm 347 */
					((((BgL_approxz00_bglt) COBJECT(BgL_dstz00_1074))->BgL_topzf3zf3) =
						((bool_t) ((bool_t) 1)), BUNSPEC);
					BGl_continuezd2cfaz12zc0zzcfa_iteratez00(CNST_TABLE_REF(2));
				}
			{	/* Cfa/approx.scm 350 */
				bool_t BgL_test3092z00_14676;

				{	/* Cfa/approx.scm 350 */
					obj_t BgL_arg1995z00_4249;

					BgL_arg1995z00_4249 =
						(((BgL_approxz00_bglt) COBJECT(BgL_dstz00_1074))->BgL_dupz00);
					{	/* Cfa/approx.scm 350 */
						obj_t BgL_classz00_6690;

						BgL_classz00_6690 = BGl_approxz00zzcfa_infoz00;
						if (BGL_OBJECTP(BgL_arg1995z00_4249))
							{	/* Cfa/approx.scm 350 */
								BgL_objectz00_bglt BgL_arg1807z00_6692;

								BgL_arg1807z00_6692 =
									(BgL_objectz00_bglt) (BgL_arg1995z00_4249);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cfa/approx.scm 350 */
										long BgL_idxz00_6698;

										BgL_idxz00_6698 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6692);
										BgL_test3092z00_14676 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_6698 + 1L)) == BgL_classz00_6690);
									}
								else
									{	/* Cfa/approx.scm 350 */
										bool_t BgL_res2206z00_6723;

										{	/* Cfa/approx.scm 350 */
											obj_t BgL_oclassz00_6706;

											{	/* Cfa/approx.scm 350 */
												obj_t BgL_arg1815z00_6714;
												long BgL_arg1816z00_6715;

												BgL_arg1815z00_6714 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cfa/approx.scm 350 */
													long BgL_arg1817z00_6716;

													BgL_arg1817z00_6716 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6692);
													BgL_arg1816z00_6715 =
														(BgL_arg1817z00_6716 - OBJECT_TYPE);
												}
												BgL_oclassz00_6706 =
													VECTOR_REF(BgL_arg1815z00_6714, BgL_arg1816z00_6715);
											}
											{	/* Cfa/approx.scm 350 */
												bool_t BgL__ortest_1115z00_6707;

												BgL__ortest_1115z00_6707 =
													(BgL_classz00_6690 == BgL_oclassz00_6706);
												if (BgL__ortest_1115z00_6707)
													{	/* Cfa/approx.scm 350 */
														BgL_res2206z00_6723 = BgL__ortest_1115z00_6707;
													}
												else
													{	/* Cfa/approx.scm 350 */
														long BgL_odepthz00_6708;

														{	/* Cfa/approx.scm 350 */
															obj_t BgL_arg1804z00_6709;

															BgL_arg1804z00_6709 = (BgL_oclassz00_6706);
															BgL_odepthz00_6708 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_6709);
														}
														if ((1L < BgL_odepthz00_6708))
															{	/* Cfa/approx.scm 350 */
																obj_t BgL_arg1802z00_6711;

																{	/* Cfa/approx.scm 350 */
																	obj_t BgL_arg1803z00_6712;

																	BgL_arg1803z00_6712 = (BgL_oclassz00_6706);
																	BgL_arg1802z00_6711 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6712,
																		1L);
																}
																BgL_res2206z00_6723 =
																	(BgL_arg1802z00_6711 == BgL_classz00_6690);
															}
														else
															{	/* Cfa/approx.scm 350 */
																BgL_res2206z00_6723 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test3092z00_14676 = BgL_res2206z00_6723;
									}
							}
						else
							{	/* Cfa/approx.scm 350 */
								BgL_test3092z00_14676 = ((bool_t) 0);
							}
					}
				}
				if (BgL_test3092z00_14676)
					{	/* Cfa/approx.scm 351 */
						obj_t BgL_arg1994z00_4248;

						BgL_arg1994z00_4248 =
							(((BgL_approxz00_bglt) COBJECT(BgL_dstz00_1074))->BgL_dupz00);
						{
							BgL_approxz00_bglt BgL_dstz00_14701;

							BgL_dstz00_14701 = ((BgL_approxz00_bglt) BgL_arg1994z00_4248);
							BgL_dstz00_1074 = BgL_dstz00_14701;
							goto BGl_approxzd2setzd2topz12z12zzcfa_approxz00;
						}
					}
				else
					{	/* Cfa/approx.scm 350 */
						return BFALSE;
					}
			}
		}

	}



/* &approx-set-top! */
	obj_t BGl_z62approxzd2setzd2topz12z70zzcfa_approxz00(obj_t BgL_envz00_8615,
		obj_t BgL_dstz00_8616)
	{
		{	/* Cfa/approx.scm 345 */
			return
				BGl_approxzd2setzd2topz12z12zzcfa_approxz00(
				((BgL_approxz00_bglt) BgL_dstz00_8616));
		}

	}



/* make-empty-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_makezd2emptyzd2approxz00zzcfa_approxz00(void)
	{
		{	/* Cfa/approx.scm 356 */
			{	/* Cfa/approx.scm 357 */
				obj_t BgL_allocsz00_4250;

				BgL_allocsz00_4250 =
					BGl_makezd2setz12zc0zzcfa_setz00
					(BGl_za2alloczd2setza2zd2zzcfa_approxz00);
				{	/* Cfa/approx.scm 358 */
					BgL_approxz00_bglt BgL_new1314z00_4251;

					{	/* Cfa/approx.scm 359 */
						BgL_approxz00_bglt BgL_new1313z00_4252;

						BgL_new1313z00_4252 =
							((BgL_approxz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_approxz00_bgl))));
						{	/* Cfa/approx.scm 359 */
							long BgL_arg1996z00_4253;

							BgL_arg1996z00_4253 = BGL_CLASS_NUM(BGl_approxz00zzcfa_infoz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1313z00_4252),
								BgL_arg1996z00_4253);
						}
						BgL_new1314z00_4251 = BgL_new1313z00_4252;
					}
					((((BgL_approxz00_bglt) COBJECT(BgL_new1314z00_4251))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt)
								BGl_za2_za2z00zztype_cachez00)), BUNSPEC);
					((((BgL_approxz00_bglt) COBJECT(BgL_new1314z00_4251))->
							BgL_typezd2lockedzf3z21) = ((bool_t) ((bool_t) 0)), BUNSPEC);
					((((BgL_approxz00_bglt) COBJECT(BgL_new1314z00_4251))->
							BgL_allocsz00) = ((obj_t) BgL_allocsz00_4250), BUNSPEC);
					((((BgL_approxz00_bglt) COBJECT(BgL_new1314z00_4251))->
							BgL_topzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
					((((BgL_approxz00_bglt) COBJECT(BgL_new1314z00_4251))->
							BgL_lostzd2stampzd2) = ((long) -1L), BUNSPEC);
					((((BgL_approxz00_bglt) COBJECT(BgL_new1314z00_4251))->BgL_dupz00) =
						((obj_t) BUNSPEC), BUNSPEC);
					((((BgL_approxz00_bglt) COBJECT(BgL_new1314z00_4251))->
							BgL_haszd2procedurezf3z21) = ((bool_t) ((bool_t) 0)), BUNSPEC);
					return BgL_new1314z00_4251;
				}
			}
		}

	}



/* &make-empty-approx */
	BgL_approxz00_bglt BGl_z62makezd2emptyzd2approxz62zzcfa_approxz00(obj_t
		BgL_envz00_8617)
	{
		{	/* Cfa/approx.scm 356 */
			return BGl_makezd2emptyzd2approxz00zzcfa_approxz00();
		}

	}



/* make-type-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_makezd2typezd2approxz00zzcfa_approxz00(BgL_typez00_bglt
		BgL_typez00_1075)
	{
		{	/* Cfa/approx.scm 365 */
			{	/* Cfa/approx.scm 366 */
				obj_t BgL_allocsz00_4254;

				BgL_allocsz00_4254 =
					BGl_makezd2setz12zc0zzcfa_setz00
					(BGl_za2alloczd2setza2zd2zzcfa_approxz00);
				{	/* Cfa/approx.scm 367 */
					BgL_approxz00_bglt BgL_new1316z00_4255;

					{	/* Cfa/approx.scm 369 */
						BgL_approxz00_bglt BgL_new1315z00_4256;

						BgL_new1315z00_4256 =
							((BgL_approxz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_approxz00_bgl))));
						{	/* Cfa/approx.scm 369 */
							long BgL_arg1997z00_4257;

							BgL_arg1997z00_4257 = BGL_CLASS_NUM(BGl_approxz00zzcfa_infoz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1315z00_4256),
								BgL_arg1997z00_4257);
						}
						BgL_new1316z00_4255 = BgL_new1315z00_4256;
					}
					((((BgL_approxz00_bglt) COBJECT(BgL_new1316z00_4255))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_typez00_1075), BUNSPEC);
					{
						bool_t BgL_auxz00_14725;

						if ((((obj_t) BgL_typez00_1075) == BGl_za2_za2z00zztype_cachez00))
							{	/* Cfa/approx.scm 368 */
								BgL_auxz00_14725 = ((bool_t) 0);
							}
						else
							{	/* Cfa/approx.scm 368 */
								BgL_auxz00_14725 = ((bool_t) 1);
							}
						((((BgL_approxz00_bglt) COBJECT(BgL_new1316z00_4255))->
								BgL_typezd2lockedzf3z21) =
							((bool_t) BgL_auxz00_14725), BUNSPEC);
					}
					((((BgL_approxz00_bglt) COBJECT(BgL_new1316z00_4255))->
							BgL_allocsz00) = ((obj_t) BgL_allocsz00_4254), BUNSPEC);
					((((BgL_approxz00_bglt) COBJECT(BgL_new1316z00_4255))->
							BgL_topzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
					((((BgL_approxz00_bglt) COBJECT(BgL_new1316z00_4255))->
							BgL_lostzd2stampzd2) = ((long) -1L), BUNSPEC);
					((((BgL_approxz00_bglt) COBJECT(BgL_new1316z00_4255))->BgL_dupz00) =
						((obj_t) BUNSPEC), BUNSPEC);
					((((BgL_approxz00_bglt) COBJECT(BgL_new1316z00_4255))->
							BgL_haszd2procedurezf3z21) = ((bool_t) ((bool_t) 0)), BUNSPEC);
					return BgL_new1316z00_4255;
				}
			}
		}

	}



/* &make-type-approx */
	BgL_approxz00_bglt BGl_z62makezd2typezd2approxz62zzcfa_approxz00(obj_t
		BgL_envz00_8618, obj_t BgL_typez00_8619)
	{
		{	/* Cfa/approx.scm 365 */
			return
				BGl_makezd2typezd2approxz00zzcfa_approxz00(
				((BgL_typez00_bglt) BgL_typez00_8619));
		}

	}



/* make-type-alloc-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_makezd2typezd2alloczd2approxzd2zzcfa_approxz00(BgL_typez00_bglt
		BgL_typez00_1076, BgL_nodez00_bglt BgL_allocz00_1077)
	{
		{	/* Cfa/approx.scm 375 */
			{	/* Cfa/approx.scm 376 */
				obj_t BgL_allocsz00_4258;

				BgL_allocsz00_4258 =
					BGl_makezd2setz12zc0zzcfa_setz00
					(BGl_za2alloczd2setza2zd2zzcfa_approxz00);
				BGl_setzd2extendz12zc0zzcfa_setz00(BgL_allocsz00_4258,
					((obj_t) BgL_allocz00_1077));
				{	/* Cfa/approx.scm 378 */
					BgL_approxz00_bglt BgL_new1319z00_4259;

					{	/* Cfa/approx.scm 380 */
						BgL_approxz00_bglt BgL_new1317z00_4260;

						BgL_new1317z00_4260 =
							((BgL_approxz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_approxz00_bgl))));
						{	/* Cfa/approx.scm 380 */
							long BgL_arg1998z00_4261;

							BgL_arg1998z00_4261 = BGL_CLASS_NUM(BGl_approxz00zzcfa_infoz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1317z00_4260),
								BgL_arg1998z00_4261);
						}
						BgL_new1319z00_4259 = BgL_new1317z00_4260;
					}
					((((BgL_approxz00_bglt) COBJECT(BgL_new1319z00_4259))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_typez00_1076), BUNSPEC);
					{
						bool_t BgL_auxz00_14745;

						if ((((obj_t) BgL_typez00_1076) == BGl_za2_za2z00zztype_cachez00))
							{	/* Cfa/approx.scm 379 */
								BgL_auxz00_14745 = ((bool_t) 0);
							}
						else
							{	/* Cfa/approx.scm 379 */
								BgL_auxz00_14745 = ((bool_t) 1);
							}
						((((BgL_approxz00_bglt) COBJECT(BgL_new1319z00_4259))->
								BgL_typezd2lockedzf3z21) =
							((bool_t) BgL_auxz00_14745), BUNSPEC);
					}
					((((BgL_approxz00_bglt) COBJECT(BgL_new1319z00_4259))->
							BgL_allocsz00) = ((obj_t) BgL_allocsz00_4258), BUNSPEC);
					((((BgL_approxz00_bglt) COBJECT(BgL_new1319z00_4259))->
							BgL_topzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
					((((BgL_approxz00_bglt) COBJECT(BgL_new1319z00_4259))->
							BgL_lostzd2stampzd2) = ((long) -1L), BUNSPEC);
					((((BgL_approxz00_bglt) COBJECT(BgL_new1319z00_4259))->BgL_dupz00) =
						((obj_t) BUNSPEC), BUNSPEC);
					{
						bool_t BgL_auxz00_14754;

						{	/* Cfa/approx.scm 382 */
							obj_t BgL_classz00_6733;

							BgL_classz00_6733 = BGl_makezd2procedurezd2appz00zzcfa_info2z00;
							{	/* Cfa/approx.scm 382 */
								BgL_objectz00_bglt BgL_arg1807z00_6735;

								{	/* Cfa/approx.scm 382 */
									obj_t BgL_tmpz00_14755;

									BgL_tmpz00_14755 = ((obj_t) BgL_allocz00_1077);
									BgL_arg1807z00_6735 = (BgL_objectz00_bglt) (BgL_tmpz00_14755);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cfa/approx.scm 382 */
										long BgL_idxz00_6741;

										BgL_idxz00_6741 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6735);
										{	/* Cfa/approx.scm 382 */
											obj_t BgL_arg1800z00_6742;

											{	/* Cfa/approx.scm 382 */
												long BgL_arg1801z00_6743;

												BgL_arg1801z00_6743 = (BgL_idxz00_6741 + 4L);
												{	/* Cfa/approx.scm 382 */
													obj_t BgL_vectorz00_6745;

													BgL_vectorz00_6745 =
														BGl_za2inheritancesza2z00zz__objectz00;
													BgL_arg1800z00_6742 =
														VECTOR_REF(BgL_vectorz00_6745, BgL_arg1801z00_6743);
											}}
											BgL_auxz00_14754 =
												(BgL_arg1800z00_6742 == BgL_classz00_6733);
									}}
								else
									{	/* Cfa/approx.scm 382 */
										bool_t BgL_res2207z00_6766;

										{	/* Cfa/approx.scm 382 */
											obj_t BgL_oclassz00_6749;

											{	/* Cfa/approx.scm 382 */
												obj_t BgL_arg1815z00_6757;
												long BgL_arg1816z00_6758;

												BgL_arg1815z00_6757 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cfa/approx.scm 382 */
													long BgL_arg1817z00_6759;

													BgL_arg1817z00_6759 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6735);
													BgL_arg1816z00_6758 =
														(BgL_arg1817z00_6759 - OBJECT_TYPE);
												}
												BgL_oclassz00_6749 =
													VECTOR_REF(BgL_arg1815z00_6757, BgL_arg1816z00_6758);
											}
											{	/* Cfa/approx.scm 382 */
												bool_t BgL__ortest_1115z00_6750;

												BgL__ortest_1115z00_6750 =
													(BgL_classz00_6733 == BgL_oclassz00_6749);
												if (BgL__ortest_1115z00_6750)
													{	/* Cfa/approx.scm 382 */
														BgL_res2207z00_6766 = BgL__ortest_1115z00_6750;
													}
												else
													{	/* Cfa/approx.scm 382 */
														long BgL_odepthz00_6751;

														{	/* Cfa/approx.scm 382 */
															obj_t BgL_arg1804z00_6752;

															BgL_arg1804z00_6752 = (BgL_oclassz00_6749);
															BgL_odepthz00_6751 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_6752);
														}
														if ((4L < BgL_odepthz00_6751))
															{	/* Cfa/approx.scm 382 */
																obj_t BgL_arg1802z00_6754;

																{	/* Cfa/approx.scm 382 */
																	obj_t BgL_arg1803z00_6755;

																	BgL_arg1803z00_6755 = (BgL_oclassz00_6749);
																	BgL_arg1802z00_6754 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6755,
																		4L);
																}
																BgL_res2207z00_6766 =
																	(BgL_arg1802z00_6754 == BgL_classz00_6733);
															}
														else
															{	/* Cfa/approx.scm 382 */
																BgL_res2207z00_6766 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_auxz00_14754 = BgL_res2207z00_6766;
									}
							}
						}
						((((BgL_approxz00_bglt) COBJECT(BgL_new1319z00_4259))->
								BgL_haszd2procedurezf3z21) =
							((bool_t) BgL_auxz00_14754), BUNSPEC);
					}
					return BgL_new1319z00_4259;
				}
			}
		}

	}



/* &make-type-alloc-approx */
	BgL_approxz00_bglt BGl_z62makezd2typezd2alloczd2approxzb0zzcfa_approxz00(obj_t
		BgL_envz00_8620, obj_t BgL_typez00_8621, obj_t BgL_allocz00_8622)
	{
		{	/* Cfa/approx.scm 375 */
			return
				BGl_makezd2typezd2alloczd2approxzd2zzcfa_approxz00(
				((BgL_typez00_bglt) BgL_typez00_8621),
				((BgL_nodez00_bglt) BgL_allocz00_8622));
		}

	}



/* for-each-approx-alloc */
	BGL_EXPORTED_DEF obj_t BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00(obj_t
		BgL_procz00_1079, BgL_approxz00_bglt BgL_approxz00_1080)
	{
		{	/* Cfa/approx.scm 406 */
			return
				BGl_setzd2forzd2eachz00zzcfa_setz00(BgL_procz00_1079,
				(((BgL_approxz00_bglt) COBJECT(BgL_approxz00_1080))->BgL_allocsz00));
		}

	}



/* &for-each-approx-alloc */
	obj_t BGl_z62forzd2eachzd2approxzd2alloczb0zzcfa_approxz00(obj_t
		BgL_envz00_8623, obj_t BgL_procz00_8624, obj_t BgL_approxz00_8625)
	{
		{	/* Cfa/approx.scm 406 */
			return
				BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00(BgL_procz00_8624,
				((BgL_approxz00_bglt) BgL_approxz00_8625));
		}

	}



/* empty-approx-alloc? */
	BGL_EXPORTED_DEF obj_t
		BGl_emptyzd2approxzd2alloczf3zf3zzcfa_approxz00(BgL_approxz00_bglt
		BgL_approxz00_1081)
	{
		{	/* Cfa/approx.scm 413 */
			{	/* Cfa/approx.scm 414 */
				obj_t BgL_arg2000z00_6769;

				BgL_arg2000z00_6769 =
					BGl_setzd2lengthzd2zzcfa_setz00(
					(((BgL_approxz00_bglt) COBJECT(BgL_approxz00_1081))->BgL_allocsz00));
				return BBOOL(((long) CINT(BgL_arg2000z00_6769) == 0L));
		}}

	}



/* &empty-approx-alloc? */
	obj_t BGl_z62emptyzd2approxzd2alloczf3z91zzcfa_approxz00(obj_t
		BgL_envz00_8626, obj_t BgL_approxz00_8627)
	{
		{	/* Cfa/approx.scm 413 */
			return
				BGl_emptyzd2approxzd2alloczf3zf3zzcfa_approxz00(
				((BgL_approxz00_bglt) BgL_approxz00_8627));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcfa_approxz00(void)
	{
		{	/* Cfa/approx.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcfa_approxz00(void)
	{
		{	/* Cfa/approx.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_getzd2nodezd2atomzd2valuezd2envz00zzcfa_approxz00,
				BGl_proc2232z00zzcfa_approxz00, BGl_nodez00zzast_nodez00,
				BGl_string2233z00zzcfa_approxz00);
		}

	}



/* &get-node-atom-value1581 */
	obj_t BGl_z62getzd2nodezd2atomzd2value1581zb0zzcfa_approxz00(obj_t
		BgL_envz00_8629, obj_t BgL_nodez00_8630)
	{
		{	/* Cfa/approx.scm 421 */
			return CNST_TABLE_REF(3);
		}

	}



/* get-node-atom-value */
	BGL_EXPORTED_DEF obj_t
		BGl_getzd2nodezd2atomzd2valuezd2zzcfa_approxz00(BgL_nodez00_bglt
		BgL_nodez00_1082)
	{
		{	/* Cfa/approx.scm 421 */
			{	/* Cfa/approx.scm 421 */
				obj_t BgL_method1582z00_4270;

				{	/* Cfa/approx.scm 421 */
					obj_t BgL_res2212z00_6803;

					{	/* Cfa/approx.scm 421 */
						long BgL_objzd2classzd2numz00_6774;

						BgL_objzd2classzd2numz00_6774 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_1082));
						{	/* Cfa/approx.scm 421 */
							obj_t BgL_arg1811z00_6775;

							BgL_arg1811z00_6775 =
								PROCEDURE_REF
								(BGl_getzd2nodezd2atomzd2valuezd2envz00zzcfa_approxz00,
								(int) (1L));
							{	/* Cfa/approx.scm 421 */
								int BgL_offsetz00_6778;

								BgL_offsetz00_6778 = (int) (BgL_objzd2classzd2numz00_6774);
								{	/* Cfa/approx.scm 421 */
									long BgL_offsetz00_6779;

									BgL_offsetz00_6779 =
										((long) (BgL_offsetz00_6778) - OBJECT_TYPE);
									{	/* Cfa/approx.scm 421 */
										long BgL_modz00_6780;

										BgL_modz00_6780 =
											(BgL_offsetz00_6779 >> (int) ((long) ((int) (4L))));
										{	/* Cfa/approx.scm 421 */
											long BgL_restz00_6782;

											BgL_restz00_6782 =
												(BgL_offsetz00_6779 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Cfa/approx.scm 421 */

												{	/* Cfa/approx.scm 421 */
													obj_t BgL_bucketz00_6784;

													BgL_bucketz00_6784 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_6775), BgL_modz00_6780);
													BgL_res2212z00_6803 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_6784), BgL_restz00_6782);
					}}}}}}}}
					BgL_method1582z00_4270 = BgL_res2212z00_6803;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1582z00_4270,
					((obj_t) BgL_nodez00_1082));
			}
		}

	}



/* &get-node-atom-value */
	obj_t BGl_z62getzd2nodezd2atomzd2valuezb0zzcfa_approxz00(obj_t
		BgL_envz00_8631, obj_t BgL_nodez00_8632)
	{
		{	/* Cfa/approx.scm 421 */
			return
				BGl_getzd2nodezd2atomzd2valuezd2zzcfa_approxz00(
				((BgL_nodez00_bglt) BgL_nodez00_8632));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcfa_approxz00(void)
	{
		{	/* Cfa/approx.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shapezd2envzd2zztools_shapez00, BGl_approxz00zzcfa_infoz00,
				BGl_proc2234z00zzcfa_approxz00, BGl_string2235z00zzcfa_approxz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2nodezd2atomzd2valuezd2envz00zzcfa_approxz00,
				BGl_atomz00zzast_nodez00, BGl_proc2236z00zzcfa_approxz00,
				BGl_string2237z00zzcfa_approxz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2nodezd2atomzd2valuezd2envz00zzcfa_approxz00,
				BGl_varz00zzast_nodez00, BGl_proc2238z00zzcfa_approxz00,
				BGl_string2237z00zzcfa_approxz00);
		}

	}



/* &get-node-atom-value-1586 */
	obj_t BGl_z62getzd2nodezd2atomzd2valuezd21586z62zzcfa_approxz00(obj_t
		BgL_envz00_8636, obj_t BgL_nodez00_8637)
	{
		{	/* Cfa/approx.scm 433 */
			{	/* Cfa/approx.scm 434 */
				BgL_variablez00_bglt BgL_vz00_9403;

				BgL_vz00_9403 =
					(((BgL_varz00_bglt) COBJECT(
							((BgL_varz00_bglt) BgL_nodez00_8637)))->BgL_variablez00);
				{	/* Cfa/approx.scm 435 */
					bool_t BgL_test3102z00_14831;

					{	/* Cfa/approx.scm 435 */
						bool_t BgL_test3103z00_14832;

						{	/* Cfa/approx.scm 435 */
							obj_t BgL_classz00_9404;

							BgL_classz00_9404 = BGl_reshapedzd2localzd2zzcfa_infoz00;
							{	/* Cfa/approx.scm 435 */
								BgL_objectz00_bglt BgL_arg1807z00_9405;

								{	/* Cfa/approx.scm 435 */
									obj_t BgL_tmpz00_14833;

									BgL_tmpz00_14833 =
										((obj_t) ((BgL_objectz00_bglt) BgL_vz00_9403));
									BgL_arg1807z00_9405 = (BgL_objectz00_bglt) (BgL_tmpz00_14833);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cfa/approx.scm 435 */
										long BgL_idxz00_9406;

										BgL_idxz00_9406 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9405);
										BgL_test3103z00_14832 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_9406 + 3L)) == BgL_classz00_9404);
									}
								else
									{	/* Cfa/approx.scm 435 */
										bool_t BgL_res2213z00_9409;

										{	/* Cfa/approx.scm 435 */
											obj_t BgL_oclassz00_9410;

											{	/* Cfa/approx.scm 435 */
												obj_t BgL_arg1815z00_9411;
												long BgL_arg1816z00_9412;

												BgL_arg1815z00_9411 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cfa/approx.scm 435 */
													long BgL_arg1817z00_9413;

													BgL_arg1817z00_9413 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9405);
													BgL_arg1816z00_9412 =
														(BgL_arg1817z00_9413 - OBJECT_TYPE);
												}
												BgL_oclassz00_9410 =
													VECTOR_REF(BgL_arg1815z00_9411, BgL_arg1816z00_9412);
											}
											{	/* Cfa/approx.scm 435 */
												bool_t BgL__ortest_1115z00_9414;

												BgL__ortest_1115z00_9414 =
													(BgL_classz00_9404 == BgL_oclassz00_9410);
												if (BgL__ortest_1115z00_9414)
													{	/* Cfa/approx.scm 435 */
														BgL_res2213z00_9409 = BgL__ortest_1115z00_9414;
													}
												else
													{	/* Cfa/approx.scm 435 */
														long BgL_odepthz00_9415;

														{	/* Cfa/approx.scm 435 */
															obj_t BgL_arg1804z00_9416;

															BgL_arg1804z00_9416 = (BgL_oclassz00_9410);
															BgL_odepthz00_9415 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_9416);
														}
														if ((3L < BgL_odepthz00_9415))
															{	/* Cfa/approx.scm 435 */
																obj_t BgL_arg1802z00_9417;

																{	/* Cfa/approx.scm 435 */
																	obj_t BgL_arg1803z00_9418;

																	BgL_arg1803z00_9418 = (BgL_oclassz00_9410);
																	BgL_arg1802z00_9417 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9418,
																		3L);
																}
																BgL_res2213z00_9409 =
																	(BgL_arg1802z00_9417 == BgL_classz00_9404);
															}
														else
															{	/* Cfa/approx.scm 435 */
																BgL_res2213z00_9409 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test3103z00_14832 = BgL_res2213z00_9409;
									}
							}
						}
						if (BgL_test3103z00_14832)
							{	/* Cfa/approx.scm 435 */
								obj_t BgL_arg2019z00_9419;

								{
									BgL_reshapedzd2localzd2_bglt BgL_auxz00_14856;

									{
										obj_t BgL_auxz00_14857;

										{	/* Cfa/approx.scm 435 */
											BgL_objectz00_bglt BgL_tmpz00_14858;

											BgL_tmpz00_14858 =
												((BgL_objectz00_bglt)
												((BgL_localz00_bglt) BgL_vz00_9403));
											BgL_auxz00_14857 = BGL_OBJECT_WIDENING(BgL_tmpz00_14858);
										}
										BgL_auxz00_14856 =
											((BgL_reshapedzd2localzd2_bglt) BgL_auxz00_14857);
									}
									BgL_arg2019z00_9419 =
										(((BgL_reshapedzd2localzd2_bglt)
											COBJECT(BgL_auxz00_14856))->BgL_bindingzd2valuezd2);
								}
								{	/* Cfa/approx.scm 435 */
									obj_t BgL_classz00_9420;

									BgL_classz00_9420 = BGl_nodez00zzast_nodez00;
									if (BGL_OBJECTP(BgL_arg2019z00_9419))
										{	/* Cfa/approx.scm 435 */
											BgL_objectz00_bglt BgL_arg1807z00_9421;

											BgL_arg1807z00_9421 =
												(BgL_objectz00_bglt) (BgL_arg2019z00_9419);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Cfa/approx.scm 435 */
													long BgL_idxz00_9422;

													BgL_idxz00_9422 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9421);
													BgL_test3102z00_14831 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_9422 + 1L)) == BgL_classz00_9420);
												}
											else
												{	/* Cfa/approx.scm 435 */
													bool_t BgL_res2214z00_9425;

													{	/* Cfa/approx.scm 435 */
														obj_t BgL_oclassz00_9426;

														{	/* Cfa/approx.scm 435 */
															obj_t BgL_arg1815z00_9427;
															long BgL_arg1816z00_9428;

															BgL_arg1815z00_9427 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Cfa/approx.scm 435 */
																long BgL_arg1817z00_9429;

																BgL_arg1817z00_9429 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9421);
																BgL_arg1816z00_9428 =
																	(BgL_arg1817z00_9429 - OBJECT_TYPE);
															}
															BgL_oclassz00_9426 =
																VECTOR_REF(BgL_arg1815z00_9427,
																BgL_arg1816z00_9428);
														}
														{	/* Cfa/approx.scm 435 */
															bool_t BgL__ortest_1115z00_9430;

															BgL__ortest_1115z00_9430 =
																(BgL_classz00_9420 == BgL_oclassz00_9426);
															if (BgL__ortest_1115z00_9430)
																{	/* Cfa/approx.scm 435 */
																	BgL_res2214z00_9425 =
																		BgL__ortest_1115z00_9430;
																}
															else
																{	/* Cfa/approx.scm 435 */
																	long BgL_odepthz00_9431;

																	{	/* Cfa/approx.scm 435 */
																		obj_t BgL_arg1804z00_9432;

																		BgL_arg1804z00_9432 = (BgL_oclassz00_9426);
																		BgL_odepthz00_9431 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_9432);
																	}
																	if ((1L < BgL_odepthz00_9431))
																		{	/* Cfa/approx.scm 435 */
																			obj_t BgL_arg1802z00_9433;

																			{	/* Cfa/approx.scm 435 */
																				obj_t BgL_arg1803z00_9434;

																				BgL_arg1803z00_9434 =
																					(BgL_oclassz00_9426);
																				BgL_arg1802z00_9433 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_9434, 1L);
																			}
																			BgL_res2214z00_9425 =
																				(BgL_arg1802z00_9433 ==
																				BgL_classz00_9420);
																		}
																	else
																		{	/* Cfa/approx.scm 435 */
																			BgL_res2214z00_9425 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test3102z00_14831 = BgL_res2214z00_9425;
												}
										}
									else
										{	/* Cfa/approx.scm 435 */
											BgL_test3102z00_14831 = ((bool_t) 0);
										}
								}
							}
						else
							{	/* Cfa/approx.scm 435 */
								BgL_test3102z00_14831 = ((bool_t) 0);
							}
					}
					if (BgL_test3102z00_14831)
						{	/* Cfa/approx.scm 436 */
							obj_t BgL_arg2018z00_9435;

							{
								BgL_reshapedzd2localzd2_bglt BgL_auxz00_14886;

								{
									obj_t BgL_auxz00_14887;

									{	/* Cfa/approx.scm 436 */
										BgL_objectz00_bglt BgL_tmpz00_14888;

										BgL_tmpz00_14888 =
											((BgL_objectz00_bglt)
											((BgL_localz00_bglt) BgL_vz00_9403));
										BgL_auxz00_14887 = BGL_OBJECT_WIDENING(BgL_tmpz00_14888);
									}
									BgL_auxz00_14886 =
										((BgL_reshapedzd2localzd2_bglt) BgL_auxz00_14887);
								}
								BgL_arg2018z00_9435 =
									(((BgL_reshapedzd2localzd2_bglt) COBJECT(BgL_auxz00_14886))->
									BgL_bindingzd2valuezd2);
							}
							return
								BGl_getzd2nodezd2atomzd2valuezd2zzcfa_approxz00(
								((BgL_nodez00_bglt) BgL_arg2018z00_9435));
						}
					else
						{	/* Cfa/approx.scm 435 */
							return CNST_TABLE_REF(3);
						}
				}
			}
		}

	}



/* &get-node-atom-value-1584 */
	obj_t BGl_z62getzd2nodezd2atomzd2valuezd21584z62zzcfa_approxz00(obj_t
		BgL_envz00_8638, obj_t BgL_nodez00_8639)
	{
		{	/* Cfa/approx.scm 427 */
			return
				(((BgL_atomz00_bglt) COBJECT(
						((BgL_atomz00_bglt) BgL_nodez00_8639)))->BgL_valuez00);
		}

	}



/* &shape-approx1580 */
	obj_t BGl_z62shapezd2approx1580zb0zzcfa_approxz00(obj_t BgL_envz00_8640,
		obj_t BgL_expz00_8641)
	{
		{	/* Cfa/approx.scm 387 */
			{	/* Cfa/approx.scm 389 */
				obj_t BgL_keysz00_9438;

				BgL_keysz00_9438 =
					BGl_setzd2ze3vectorz31zzcfa_setz00(
					(((BgL_approxz00_bglt) COBJECT(
								((BgL_approxz00_bglt) BgL_expz00_8641)))->BgL_allocsz00));
				{	/* Cfa/approx.scm 390 */
					long BgL_slenz00_9439;

					BgL_slenz00_9439 = (VECTOR_LENGTH(((obj_t) BgL_keysz00_9438)) + 2L);
					{	/* Cfa/approx.scm 391 */
						obj_t BgL_structz00_9440;

						{	/* Cfa/approx.scm 392 */
							obj_t BgL_keyz00_9441;
							int BgL_lenz00_9442;

							BgL_keyz00_9441 = CNST_TABLE_REF(4);
							BgL_lenz00_9442 = (int) (BgL_slenz00_9439);
							BgL_structz00_9440 =
								make_struct(BgL_keyz00_9441, BgL_lenz00_9442, BUNSPEC);
						}
						{	/* Cfa/approx.scm 392 */

							{	/* Cfa/approx.scm 393 */
								obj_t BgL_auxz00_14910;
								int BgL_tmpz00_14908;

								BgL_auxz00_14910 =
									(((BgL_typez00_bglt) COBJECT(
											(((BgL_approxz00_bglt) COBJECT(
														((BgL_approxz00_bglt) BgL_expz00_8641)))->
												BgL_typez00)))->BgL_idz00);
								BgL_tmpz00_14908 = (int) (0L);
								STRUCT_SET(BgL_structz00_9440, BgL_tmpz00_14908,
									BgL_auxz00_14910);
							}
							{	/* Cfa/approx.scm 394 */
								obj_t BgL_arg2006z00_9443;

								if (
									(((BgL_approxz00_bglt) COBJECT(
												((BgL_approxz00_bglt) BgL_expz00_8641)))->
										BgL_topzf3zf3))
									{	/* Cfa/approx.scm 394 */
										BgL_arg2006z00_9443 = CNST_TABLE_REF(5);
									}
								else
									{	/* Cfa/approx.scm 394 */
										BgL_arg2006z00_9443 = CNST_TABLE_REF(6);
									}
								{	/* Cfa/approx.scm 394 */
									int BgL_tmpz00_14920;

									BgL_tmpz00_14920 = (int) (1L);
									STRUCT_SET(BgL_structz00_9440, BgL_tmpz00_14920,
										BgL_arg2006z00_9443);
							}}
							{
								long BgL_rz00_9445;
								long BgL_wz00_9446;

								BgL_rz00_9445 = 2L;
								BgL_wz00_9446 = 0L;
							BgL_loopz00_9444:
								if (
									(BgL_wz00_9446 == VECTOR_LENGTH(((obj_t) BgL_keysz00_9438))))
									{	/* Cfa/approx.scm 397 */
										return BgL_structz00_9440;
									}
								else
									{	/* Cfa/approx.scm 397 */
										{	/* Cfa/approx.scm 400 */
											obj_t BgL_arg2010z00_9447;

											{	/* Cfa/approx.scm 400 */
												obj_t BgL_arg2011z00_9448;

												BgL_arg2011z00_9448 =
													VECTOR_REF(((obj_t) BgL_keysz00_9438), BgL_wz00_9446);
												BgL_arg2010z00_9447 =
													(((BgL_nodezf2effectzf2_bglt) COBJECT(
															((BgL_nodezf2effectzf2_bglt)
																BgL_arg2011z00_9448)))->BgL_keyz00);
											}
											{	/* Cfa/approx.scm 400 */
												int BgL_tmpz00_14931;

												BgL_tmpz00_14931 = (int) (BgL_rz00_9445);
												STRUCT_SET(BgL_structz00_9440, BgL_tmpz00_14931,
													BgL_arg2010z00_9447);
										}}
										{
											long BgL_wz00_14936;
											long BgL_rz00_14934;

											BgL_rz00_14934 = (BgL_rz00_9445 + 1L);
											BgL_wz00_14936 = (BgL_wz00_9446 + 1L);
											BgL_wz00_9446 = BgL_wz00_14936;
											BgL_rz00_9445 = BgL_rz00_14934;
											goto BgL_loopz00_9444;
										}
									}
							}
						}
					}
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcfa_approxz00(void)
	{
		{	/* Cfa/approx.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2239z00zzcfa_approxz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2239z00zzcfa_approxz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2239z00zzcfa_approxz00));
			BGl_modulezd2initializa7ationz75zztype_coercionz00(116865673L,
				BSTRING_TO_STRING(BGl_string2239z00zzcfa_approxz00));
			BGl_modulezd2initializa7ationz75zztype_miscz00(49975086L,
				BSTRING_TO_STRING(BGl_string2239z00zzcfa_approxz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2239z00zzcfa_approxz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2239z00zzcfa_approxz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string2239z00zzcfa_approxz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2239z00zzcfa_approxz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2239z00zzcfa_approxz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string2239z00zzcfa_approxz00));
			BGl_modulezd2initializa7ationz75zzcfa_infoz00(3788352L,
				BSTRING_TO_STRING(BGl_string2239z00zzcfa_approxz00));
			BGl_modulezd2initializa7ationz75zzcfa_info2z00(0L,
				BSTRING_TO_STRING(BGl_string2239z00zzcfa_approxz00));
			BGl_modulezd2initializa7ationz75zzcfa_info3z00(0L,
				BSTRING_TO_STRING(BGl_string2239z00zzcfa_approxz00));
			BGl_modulezd2initializa7ationz75zzcfa_collectz00(220306886L,
				BSTRING_TO_STRING(BGl_string2239z00zzcfa_approxz00));
			BGl_modulezd2initializa7ationz75zzcfa_setz00(5932721L,
				BSTRING_TO_STRING(BGl_string2239z00zzcfa_approxz00));
			BGl_modulezd2initializa7ationz75zzcfa_iteratez00(131412063L,
				BSTRING_TO_STRING(BGl_string2239z00zzcfa_approxz00));
			BGl_modulezd2initializa7ationz75zzcfa_loosez00(471177480L,
				BSTRING_TO_STRING(BGl_string2239z00zzcfa_approxz00));
			BGl_modulezd2initializa7ationz75zzcfa_procedurez00(227655313L,
				BSTRING_TO_STRING(BGl_string2239z00zzcfa_approxz00));
			return
				BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string2239z00zzcfa_approxz00));
		}

	}

#ifdef __cplusplus
}
#endif
