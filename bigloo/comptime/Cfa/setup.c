/*===========================================================================*/
/*   (Cfa/setup.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cfa/setup.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CFA_SETUP_TYPE_DEFINITIONS
#define BGL_CFA_SETUP_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_scnstz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_nodez00;
		obj_t BgL_classz00;
		obj_t BgL_locz00;
	}               *BgL_scnstz00_bglt;

	typedef struct BgL_cvarz00_bgl
	{
		header_t header;
		obj_t widening;
		bool_t BgL_macrozf3zf3;
	}              *BgL_cvarz00_bglt;

	typedef struct BgL_sexitz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_handlerz00;
		bool_t BgL_detachedzf3zf3;
	}               *BgL_sexitz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_literalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}                 *BgL_literalz00_bglt;

	typedef struct BgL_patchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
		struct BgL_varz00_bgl *BgL_refz00;
		long BgL_indexz00;
		obj_t BgL_patchidz00;
	}               *BgL_patchz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_pragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_formatz00;
		obj_t BgL_srfi0z00;
	}                *BgL_pragmaz00_bglt;

	typedef struct BgL_genpatchidz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		long BgL_indexz00;
		long BgL_rindexz00;
	}                    *BgL_genpatchidz00_bglt;

	typedef struct BgL_getfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_getfieldz00_bglt;

	typedef struct BgL_setfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_setfieldz00_bglt;

	typedef struct BgL_newz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_argszd2typezd2;
	}             *BgL_newz00_bglt;

	typedef struct BgL_instanceofz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_classz00;
	}                    *BgL_instanceofz00_bglt;

	typedef struct BgL_castzd2nullzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
	}                     *BgL_castzd2nullzd2_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_approxz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		bool_t BgL_typezd2lockedzf3z21;
		obj_t BgL_allocsz00;
		bool_t BgL_topzf3zf3;
		long BgL_lostzd2stampzd2;
		obj_t BgL_dupz00;
		bool_t BgL_haszd2procedurezf3z21;
	}                *BgL_approxz00_bglt;

	typedef struct BgL_cfunzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                      *BgL_cfunzf2cinfozf2_bglt;

	typedef struct BgL_externzd2sfunzf2cinfoz20_bgl
	{
		bool_t BgL_polymorphiczf3zf3;
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                               *BgL_externzd2sfunzf2cinfoz20_bglt;

	typedef struct BgL_internzd2sfunzf2cinfoz20_bgl
	{
		bool_t BgL_polymorphiczf3zf3;
		struct BgL_approxz00_bgl *BgL_approxz00;
		long BgL_stampz00;
	}                               *BgL_internzd2sfunzf2cinfoz20_bglt;

	typedef struct BgL_scnstzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                       *BgL_scnstzf2cinfozf2_bglt;

	typedef struct BgL_prezd2clozd2envz00_bgl
	{
	}                         *BgL_prezd2clozd2envz00_bglt;

	typedef struct BgL_svarzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		bool_t BgL_clozd2envzf3z21;
		long BgL_stampz00;
	}                      *BgL_svarzf2cinfozf2_bglt;

	typedef struct BgL_cvarzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                      *BgL_cvarzf2cinfozf2_bglt;

	typedef struct BgL_sexitzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                       *BgL_sexitzf2cinfozf2_bglt;

	typedef struct BgL_reshapedzd2localzd2_bgl
	{
		obj_t BgL_bindingzd2valuezd2;
	}                          *BgL_reshapedzd2localzd2_bglt;

	typedef struct BgL_reshapedzd2globalzd2_bgl
	{
	}                           *BgL_reshapedzd2globalzd2_bglt;

	typedef struct BgL_literalzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                         *BgL_literalzf2cinfozf2_bglt;

	typedef struct BgL_genpatchidzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                            *BgL_genpatchidzf2cinfozf2_bglt;

	typedef struct BgL_patchzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                       *BgL_patchzf2cinfozf2_bglt;

	typedef struct BgL_kwotezf2nodezf2_bgl
	{
		struct BgL_nodez00_bgl *BgL_nodez00;
	}                      *BgL_kwotezf2nodezf2_bglt;

	typedef struct BgL_kwotezf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                       *BgL_kwotezf2cinfozf2_bglt;

	typedef struct BgL_appzd2lyzf2cinfoz20_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                          *BgL_appzd2lyzf2cinfoz20_bglt;

	typedef struct BgL_funcallzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		struct BgL_approxz00_bgl *BgL_vazd2approxzd2;
		bool_t BgL_arityzd2errorzd2noticedzf3zf3;
		bool_t BgL_typezd2errorzd2noticedzf3zf3;
	}                         *BgL_funcallzf2cinfozf2_bglt;

	typedef struct BgL_setqzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                      *BgL_setqzf2cinfozf2_bglt;

	typedef struct BgL_conditionalzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                             *BgL_conditionalzf2cinfozf2_bglt;

	typedef struct BgL_failzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                      *BgL_failzf2cinfozf2_bglt;

	typedef struct BgL_switchzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                        *BgL_switchzf2cinfozf2_bglt;

	typedef struct BgL_setzd2exzd2itzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                               *BgL_setzd2exzd2itzf2cinfozf2_bglt;

	typedef struct BgL_jumpzd2exzd2itzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                                *BgL_jumpzd2exzd2itzf2cinfozf2_bglt;

	typedef struct BgL_makezd2procedurezd2appz00_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		obj_t BgL_valueszd2approxzd2;
		long BgL_lostzd2stampzd2;
		bool_t BgL_xzd2tzf3z21;
		bool_t BgL_xz00;
		bool_t BgL_tz00;
		struct BgL_variablez00_bgl *BgL_ownerz00;
		obj_t BgL_stackzd2stampzd2;
	}                                *BgL_makezd2procedurezd2appz00_bglt;

	typedef struct BgL_pragmazf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                        *BgL_pragmazf2cinfozf2_bglt;

	typedef struct BgL_getfieldzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                          *BgL_getfieldzf2cinfozf2_bglt;

	typedef struct BgL_setfieldzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                          *BgL_setfieldzf2cinfozf2_bglt;

	typedef struct BgL_newzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                     *BgL_newzf2cinfozf2_bglt;

	typedef struct BgL_instanceofzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                            *BgL_instanceofzf2cinfozf2_bglt;

	typedef struct BgL_castzd2nullzf2cinfoz20_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                             *BgL_castzd2nullzf2cinfoz20_bglt;


#endif													// BGL_CFA_SETUP_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62nodezd2setupz12zd2cast1814z70zzcfa_setupz00(obj_t, obj_t);
	extern obj_t BGl_newzf2Cinfozf2zzcfa_info3z00;
	static obj_t BGl_z62nodezd2setupz12zd2genpatch1802z70zzcfa_setupz00(obj_t,
		obj_t);
	extern obj_t BGl_closurez00zzast_nodez00;
	static obj_t BGl_z62nodezd2setupz12zd2setq1816z70zzcfa_setupz00(obj_t, obj_t);
	static obj_t BGl_z62variablezd2valuezd2setup1765z62zzcfa_setupz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd2setupz12zd2getfield1804z70zzcfa_setupz00(obj_t,
		obj_t);
	static obj_t BGl_z62variablezd2valuezd2setup1768z62zzcfa_setupz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_pragmazf2Cinfozf2zzcfa_info3z00;
	extern obj_t BGl_syncz00zzast_nodez00;
	static obj_t BGl_z62variablezd2valuezd2setup1770z62zzcfa_setupz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62variablezd2valuezd2setup1772z62zzcfa_setupz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62variablezd2valuezd2setup1774z62zzcfa_setupz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62variablezd2valuezd2setup1776z62zzcfa_setupz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62variablezd2valuezd2setup1778z62zzcfa_setupz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t BGl_z62variablezd2valuezd2setup1780z62zzcfa_setupz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2structza2z00zztype_cachez00;
	extern obj_t BGl_sequencez00zzast_nodez00;
	static obj_t BGl_z62nodezd2setupz12zd2kwote1760z70zzcfa_setupz00(obj_t,
		obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itzf2Cinfozf2zzcfa_infoz00;
	extern obj_t BGl_sexitz00zzast_varz00;
	static obj_t BGl_z62nodezd2setupz12zd2setfield1806z70zzcfa_setupz00(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2setupz12zd2literal1756z70zzcfa_setupz00(obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zzcfa_setupz00(void);
	static obj_t BGl_z62nodezd2setupz12zd2patch1758z70zzcfa_setupz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_genpatchidzf2Cinfozf2zzcfa_infoz00;
	extern obj_t BGl_appzd2lyzf2Cinfoz20zzcfa_infoz00;
	static obj_t BGl_z62nodezd2setupz12zd2sync1786z70zzcfa_setupz00(obj_t, obj_t);
	extern obj_t BGl_reshapedzd2localzd2zzcfa_infoz00;
	extern obj_t BGl_cvarzf2Cinfozf2zzcfa_infoz00;
	extern obj_t BGl_switchzf2Cinfozf2zzcfa_infoz00;
	static obj_t BGl_z62nodezd2setupz12zd2var1764z70zzcfa_setupz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzcfa_setupz00(void);
	static obj_t BGl_z62nodezd2setupz12zd2pragma1800z70zzcfa_setupz00(obj_t,
		obj_t);
	extern obj_t BGl_literalzf2Cinfozf2zzcfa_infoz00;
	static obj_t BGl_z62nodezd2setupz12zd2setzd2exzd2i1828z70zzcfa_setupz00(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2setupz12zd2conditio1818z70zzcfa_setupz00(obj_t,
		obj_t);
	extern obj_t BGl_prezd2clozd2envz00zzcfa_infoz00;
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t
		BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00;
	extern obj_t BGl_castzd2nullzf2Cinfoz20zzcfa_info3z00;
	extern obj_t BGl_pragmaz00zzast_nodez00;
	extern obj_t BGl_svarz00zzast_varz00;
	static obj_t BGl_z62nodezd2setupz12zd2closure1782z70zzcfa_setupz00(obj_t,
		obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	extern obj_t BGl_failzf2Cinfozf2zzcfa_infoz00;
	extern obj_t BGl_setqzf2Cinfozf2zzcfa_infoz00;
	static bool_t BGl_alloczd2typezf3z21zzcfa_setupz00(BgL_typez00_bglt);
	extern obj_t BGl_cfunz00zzast_varz00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	extern obj_t BGl_kwotezf2Cinfozf2zzcfa_infoz00;
	static obj_t BGl_z62nodezd2setupz12zd2castzd2nul1812za2zzcfa_setupz00(obj_t,
		obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzcfa_setupz00(void);
	extern obj_t BGl_internzd2sfunzf2Cinfoz20zzcfa_infoz00;
	extern obj_t BGl_kwotezf2nodezf2zzcfa_infoz00;
	extern BgL_approxz00_bglt BGl_makezd2emptyzd2approxz00zzcfa_approxz00(void);
	extern obj_t BGl_instanceofzf2Cinfozf2zzcfa_info3z00;
	extern obj_t BGl_sexitzf2Cinfozf2zzcfa_infoz00;
	extern obj_t BGl_za2optimzd2cfazd2applyzd2trackingzf3za2z21zzengine_paramz00;
	extern obj_t BGl_castzd2nullzd2zzast_nodez00;
	extern obj_t BGl_svarzf2Cinfozf2zzcfa_infoz00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd2setupz12zd2sequence1784z70zzcfa_setupz00(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2setupz12zd2letzd2fun1824za2zzcfa_setupz00(obj_t,
		obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	BGL_IMPORT obj_t
		BGl_findzd2superzd2classzd2methodzd2zz__objectz00(BgL_objectz00_bglt, obj_t,
		obj_t);
	static obj_t BGl_z62funzd2setupz12zd2sfun1792z70zzcfa_setupz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2setupz12zd2new1808z70zzcfa_setupz00(obj_t, obj_t);
	static obj_t BGl_z62nodezd2setupz12zd2kwotezf2no1762z82zzcfa_setupz00(obj_t,
		obj_t);
	extern obj_t BGl_funcallzf2Cinfozf2zzcfa_infoz00;
	extern obj_t BGl_cvarz00zzast_varz00;
	extern obj_t BGl_patchz00zzast_nodez00;
	extern obj_t BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_approxz00_bglt);
	extern obj_t BGl_setqz00zzast_nodez00;
	extern obj_t BGl_getfieldzf2Cinfozf2zzcfa_info3z00;
	BGL_EXPORTED_DECL obj_t BGl_nodezd2setupza2z12z62zzcfa_setupz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzcfa_setupz00 = BUNSPEC;
	static obj_t BGl_z62nodezd2setupz12zd2switch1822z70zzcfa_setupz00(obj_t,
		obj_t);
	extern obj_t BGl_za2vectorza2z00zztype_cachez00;
	extern obj_t BGl_funz00zzast_varz00;
	extern obj_t BGl_newz00zzast_nodez00;
	extern obj_t BGl_patchzf2Cinfozf2zzcfa_infoz00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_z62nodezd2setupz12zd2appzd2ly1796za2zzcfa_setupz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62setzd2initialzd2approxz12z70zzcfa_setupz00(obj_t, obj_t);
	static obj_t BGl_z62nodezd2setupz12zd2fail1820z70zzcfa_setupz00(obj_t, obj_t);
	extern bool_t BGl_pairzd2optimzf3z21zzcfa_pairz00(void);
	static obj_t BGl_toplevelzd2initzd2zzcfa_setupz00(void);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzcfa_setupz00(void);
	extern obj_t BGl_getfieldz00zzast_nodez00;
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	static obj_t BGl_z62nodezd2setupz12zd2letzd2var1826za2zzcfa_setupz00(obj_t,
		obj_t);
	extern obj_t BGl_za2longza2z00zztype_cachez00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_setzd2initialzd2approxz12z12zzcfa_setupz00(obj_t);
	extern obj_t BGl_reshapedzd2globalzd2zzcfa_infoz00;
	extern obj_t BGl_externzd2sfunzf2Cinfoz20zzcfa_infoz00;
	BGL_EXPORTED_DECL obj_t
		BGl_nodezd2setupz12zc0zzcfa_setupz00(BgL_nodez00_bglt);
	static obj_t BGl_z62nodezd2setupz12zd2funcall1798z70zzcfa_setupz00(obj_t,
		obj_t);
	static obj_t BGl_z62funzd2setupz12zd2cfun1794z70zzcfa_setupz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_za2pairza2z00zztype_cachez00;
	static obj_t BGl_funzd2setupz12zc0zzcfa_setupz00(BgL_funz00_bglt, obj_t);
	extern BgL_typez00_bglt BGl_getzd2typezd2atomz00zztype_typeofz00(obj_t);
	static obj_t BGl_z62nodezd2setupza2z12z00zzcfa_setupz00(obj_t, obj_t);
	static obj_t BGl_z62nodezd2setupz12zd2instance1810z70zzcfa_setupz00(obj_t,
		obj_t);
	extern obj_t BGl_za2procedureza2z00zztype_cachez00;
	static obj_t BGl_z62nodezd2setupz12zd2jumpzd2exzd21830z70zzcfa_setupz00(obj_t,
		obj_t);
	static obj_t BGl_z62funzd2setupz12za2zzcfa_setupz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62nodezd2setupz12zd2app1788z70zzcfa_setupz00(obj_t, obj_t);
	static obj_t BGl_z62nodezd2setupz121752za2zzcfa_setupz00(obj_t, obj_t);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcfa_setupz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_pairz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_approxz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info2z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_infoz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	static obj_t BGl_z62nodezd2setupz12za2zzcfa_setupz00(obj_t, obj_t);
	extern obj_t BGl_localz00zzast_varz00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzcfa_setupz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcfa_setupz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzcfa_setupz00(void);
	extern obj_t BGl_scnstz00zzast_varz00;
	static obj_t
		BGl_variablezd2valuezd2setupz12z12zzcfa_setupz00(BgL_valuez00_bglt,
		BgL_variablez00_bglt);
	extern obj_t BGl_conditionalzf2Cinfozf2zzcfa_infoz00;
	extern BgL_approxz00_bglt
		BGl_makezd2typezd2approxz00zzcfa_approxz00(BgL_typez00_bglt);
	extern obj_t BGl_instanceofz00zzast_nodez00;
	extern obj_t BGl_jumpzd2exzd2itzf2Cinfozf2zzcfa_infoz00;
	BGL_IMPORT obj_t BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_valuez00zzast_varz00;
	extern obj_t BGl_za2unspecza2z00zztype_cachez00;
	extern obj_t BGl_setfieldz00zzast_nodez00;
	static obj_t BGl_z62variablezd2valuezd2setupz12z70zzcfa_setupz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_genpatchidz00zzast_nodez00;
	extern obj_t BGl_scnstzf2Cinfozf2zzcfa_infoz00;
	extern obj_t BGl_literalz00zzast_nodez00;
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t BGl_prezd2makezd2procedurezd2appzd2zzcfa_info2z00;
	extern BgL_typez00_bglt BGl_getzd2typezd2kwotez00zztype_typeofz00(obj_t);
	static obj_t BGl_z62funzd2setupz121789za2zzcfa_setupz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_setfieldzf2Cinfozf2zzcfa_info3z00;
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t
		BGl_za2optimzd2cfazd2forcezd2loosezd2localzd2functionzf3za2z21zzengine_paramz00;
	extern obj_t BGl_cfunzf2Cinfozf2zzcfa_infoz00;
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t __cnst[7];


	   
		 
		DEFINE_STRING(BGl_string2311z00zzcfa_setupz00,
		BgL_bgl_string2311za700za7za7c2336za7, "fun-setup!", 10);
	      DEFINE_STRING(BGl_string2331z00zzcfa_setupz00,
		BgL_bgl_string2331za700za7za7c2337za7, "Unexpected closure", 18);
	      DEFINE_STRING(BGl_string2332z00zzcfa_setupz00,
		BgL_bgl_string2332za700za7za7c2338za7, "cfa_setup", 9);
	      DEFINE_STRING(BGl_string2333z00zzcfa_setupz00,
		BgL_bgl_string2333za700za7za7c2339za7,
		"static already-done (sfun sgfun) import read node-setup!1752 variable-value-setup1765 ",
		86);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2285z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72340za7,
		BGl_z62nodezd2setupz121752za2zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2287z00zzcfa_setupz00,
		BgL_bgl_za762variableza7d2va2341z00,
		BGl_z62variablezd2valuezd2setup1765z62zzcfa_setupz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2289z00zzcfa_setupz00,
		BgL_bgl_za762funza7d2setupza712342za7,
		BGl_z62funzd2setupz121789za2zzcfa_setupz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2292z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72343za7,
		BGl_z62nodezd2setupz12zd2literal1756z70zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2294z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72344za7,
		BGl_z62nodezd2setupz12zd2patch1758z70zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2295z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72345za7,
		BGl_z62nodezd2setupz12zd2kwote1760z70zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2296z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72346za7,
		BGl_z62nodezd2setupz12zd2kwotezf2no1762z82zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2297z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72347za7,
		BGl_z62nodezd2setupz12zd2var1764z70zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2298z00zzcfa_setupz00,
		BgL_bgl_za762variableza7d2va2348z00,
		BGl_z62variablezd2valuezd2setup1768z62zzcfa_setupz00, 0L, BUNSPEC, 2);
	     
		DEFINE_STATIC_BGL_GENERIC
		(BGl_variablezd2valuezd2setupz12zd2envzc0zzcfa_setupz00,
		BgL_bgl_za762variableza7d2va2349z00,
		BGl_z62variablezd2valuezd2setupz12z70zzcfa_setupz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72350za7,
		BGl_z62nodezd2setupz12za2zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2300z00zzcfa_setupz00,
		BgL_bgl_za762variableza7d2va2351z00,
		BGl_z62variablezd2valuezd2setup1770z62zzcfa_setupz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2301z00zzcfa_setupz00,
		BgL_bgl_za762variableza7d2va2352z00,
		BGl_z62variablezd2valuezd2setup1772z62zzcfa_setupz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2302z00zzcfa_setupz00,
		BgL_bgl_za762variableza7d2va2353z00,
		BGl_z62variablezd2valuezd2setup1774z62zzcfa_setupz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2303z00zzcfa_setupz00,
		BgL_bgl_za762variableza7d2va2354z00,
		BGl_z62variablezd2valuezd2setup1776z62zzcfa_setupz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2304z00zzcfa_setupz00,
		BgL_bgl_za762variableza7d2va2355z00,
		BGl_z62variablezd2valuezd2setup1778z62zzcfa_setupz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2305z00zzcfa_setupz00,
		BgL_bgl_za762variableza7d2va2356z00,
		BGl_z62variablezd2valuezd2setup1780z62zzcfa_setupz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2306z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72357za7,
		BGl_z62nodezd2setupz12zd2closure1782z70zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2307z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72358za7,
		BGl_z62nodezd2setupz12zd2sequence1784z70zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2308z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72359za7,
		BGl_z62nodezd2setupz12zd2sync1786z70zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2309z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72360za7,
		BGl_z62nodezd2setupz12zd2app1788z70zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2310z00zzcfa_setupz00,
		BgL_bgl_za762funza7d2setupza712361za7,
		BGl_z62funzd2setupz12zd2sfun1792z70zzcfa_setupz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2312z00zzcfa_setupz00,
		BgL_bgl_za762funza7d2setupza712362za7,
		BGl_z62funzd2setupz12zd2cfun1794z70zzcfa_setupz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2313z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72363za7,
		BGl_z62nodezd2setupz12zd2appzd2ly1796za2zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2314z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72364za7,
		BGl_z62nodezd2setupz12zd2funcall1798z70zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2315z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72365za7,
		BGl_z62nodezd2setupz12zd2pragma1800z70zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2316z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72366za7,
		BGl_z62nodezd2setupz12zd2genpatch1802z70zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2317z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72367za7,
		BGl_z62nodezd2setupz12zd2getfield1804z70zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2318z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72368za7,
		BGl_z62nodezd2setupz12zd2setfield1806z70zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2319z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72369za7,
		BGl_z62nodezd2setupz12zd2new1808z70zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2320z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72370za7,
		BGl_z62nodezd2setupz12zd2instance1810z70zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2321z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72371za7,
		BGl_z62nodezd2setupz12zd2castzd2nul1812za2zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2322z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72372za7,
		BGl_z62nodezd2setupz12zd2cast1814z70zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2323z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72373za7,
		BGl_z62nodezd2setupz12zd2setq1816z70zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2324z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72374za7,
		BGl_z62nodezd2setupz12zd2conditio1818z70zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2325z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72375za7,
		BGl_z62nodezd2setupz12zd2fail1820z70zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2326z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72376za7,
		BGl_z62nodezd2setupz12zd2switch1822z70zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2327z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72377za7,
		BGl_z62nodezd2setupz12zd2letzd2fun1824za2zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2328z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72378za7,
		BGl_z62nodezd2setupz12zd2letzd2var1826za2zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2329z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72379za7,
		BGl_z62nodezd2setupz12zd2setzd2exzd2i1828z70zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2330z00zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72380za7,
		BGl_z62nodezd2setupz12zd2jumpzd2exzd21830z70zzcfa_setupz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2286z00zzcfa_setupz00,
		BgL_bgl_string2286za700za7za7c2381za7, "node-setup!1752", 15);
	      DEFINE_STRING(BGl_string2288z00zzcfa_setupz00,
		BgL_bgl_string2288za700za7za7c2382za7, "variable-value-setup1765", 24);
	      DEFINE_STRING(BGl_string2290z00zzcfa_setupz00,
		BgL_bgl_string2290za700za7za7c2383za7, "fun-setup!1789", 14);
	      DEFINE_STRING(BGl_string2291z00zzcfa_setupz00,
		BgL_bgl_string2291za700za7za7c2384za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string2293z00zzcfa_setupz00,
		BgL_bgl_string2293za700za7za7c2385za7, "node-setup!", 11);
	      DEFINE_STRING(BGl_string2299z00zzcfa_setupz00,
		BgL_bgl_string2299za700za7za7c2386za7, "variable-value-setup!", 21);
	      DEFINE_STATIC_BGL_GENERIC(BGl_funzd2setupz12zd2envz12zzcfa_setupz00,
		BgL_bgl_za762funza7d2setupza712387za7,
		BGl_z62funzd2setupz12za2zzcfa_setupz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2initialzd2approxz12zd2envzc0zzcfa_setupz00,
		BgL_bgl_za762setza7d2initial2388z00,
		BGl_z62setzd2initialzd2approxz12z70zzcfa_setupz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_nodezd2setupza2z12zd2envzb0zzcfa_setupz00,
		BgL_bgl_za762nodeza7d2setupza72389za7,
		BGl_z62nodezd2setupza2z12z00zzcfa_setupz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcfa_setupz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcfa_setupz00(long
		BgL_checksumz00_6223, char *BgL_fromz00_6224)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcfa_setupz00))
				{
					BGl_requirezd2initializa7ationz75zzcfa_setupz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcfa_setupz00();
					BGl_libraryzd2moduleszd2initz00zzcfa_setupz00();
					BGl_cnstzd2initzd2zzcfa_setupz00();
					BGl_importedzd2moduleszd2initz00zzcfa_setupz00();
					BGl_genericzd2initzd2zzcfa_setupz00();
					BGl_methodzd2initzd2zzcfa_setupz00();
					return BGl_toplevelzd2initzd2zzcfa_setupz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcfa_setupz00(void)
	{
		{	/* Cfa/setup.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cfa_setup");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "cfa_setup");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cfa_setup");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cfa_setup");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cfa_setup");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cfa_setup");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "cfa_setup");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"cfa_setup");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "cfa_setup");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cfa_setup");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcfa_setupz00(void)
	{
		{	/* Cfa/setup.scm 15 */
			{	/* Cfa/setup.scm 15 */
				obj_t BgL_cportz00_5801;

				{	/* Cfa/setup.scm 15 */
					obj_t BgL_stringz00_5808;

					BgL_stringz00_5808 = BGl_string2333z00zzcfa_setupz00;
					{	/* Cfa/setup.scm 15 */
						obj_t BgL_startz00_5809;

						BgL_startz00_5809 = BINT(0L);
						{	/* Cfa/setup.scm 15 */
							obj_t BgL_endz00_5810;

							BgL_endz00_5810 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_5808)));
							{	/* Cfa/setup.scm 15 */

								BgL_cportz00_5801 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_5808, BgL_startz00_5809, BgL_endz00_5810);
				}}}}
				{
					long BgL_iz00_5802;

					BgL_iz00_5802 = 6L;
				BgL_loopz00_5803:
					if ((BgL_iz00_5802 == -1L))
						{	/* Cfa/setup.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Cfa/setup.scm 15 */
							{	/* Cfa/setup.scm 15 */
								obj_t BgL_arg2335z00_5804;

								{	/* Cfa/setup.scm 15 */

									{	/* Cfa/setup.scm 15 */
										obj_t BgL_locationz00_5806;

										BgL_locationz00_5806 = BBOOL(((bool_t) 0));
										{	/* Cfa/setup.scm 15 */

											BgL_arg2335z00_5804 =
												BGl_readz00zz__readerz00(BgL_cportz00_5801,
												BgL_locationz00_5806);
										}
									}
								}
								{	/* Cfa/setup.scm 15 */
									int BgL_tmpz00_6254;

									BgL_tmpz00_6254 = (int) (BgL_iz00_5802);
									CNST_TABLE_SET(BgL_tmpz00_6254, BgL_arg2335z00_5804);
							}}
							{	/* Cfa/setup.scm 15 */
								int BgL_auxz00_5807;

								BgL_auxz00_5807 = (int) ((BgL_iz00_5802 - 1L));
								{
									long BgL_iz00_6259;

									BgL_iz00_6259 = (long) (BgL_auxz00_5807);
									BgL_iz00_5802 = BgL_iz00_6259;
									goto BgL_loopz00_5803;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcfa_setupz00(void)
	{
		{	/* Cfa/setup.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcfa_setupz00(void)
	{
		{	/* Cfa/setup.scm 15 */
			return BUNSPEC;
		}

	}



/* set-initial-approx! */
	BGL_EXPORTED_DEF obj_t BGl_setzd2initialzd2approxz12z12zzcfa_setupz00(obj_t
		BgL_globalsz00_3)
	{
		{	/* Cfa/setup.scm 38 */
			{
				obj_t BgL_l1736z00_3475;

				{	/* Cfa/setup.scm 40 */
					bool_t BgL_tmpz00_6262;

					BgL_l1736z00_3475 = BgL_globalsz00_3;
				BgL_zc3z04anonymousza31834ze3z87_3476:
					if (PAIRP(BgL_l1736z00_3475))
						{	/* Cfa/setup.scm 40 */
							{	/* Cfa/setup.scm 42 */
								obj_t BgL_globalz00_3478;

								BgL_globalz00_3478 = CAR(BgL_l1736z00_3475);
								{	/* Cfa/setup.scm 43 */
									BgL_valuez00_bglt BgL_funz00_3479;

									BgL_funz00_3479 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_globalz00_3478))))->
										BgL_valuez00);
									BGl_funzd2setupz12zc0zzcfa_setupz00(((BgL_funz00_bglt)
											BgL_funz00_3479), BgL_globalz00_3478);
									{	/* Cfa/setup.scm 45 */
										obj_t BgL_g1735z00_3480;

										BgL_g1735z00_3480 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_funz00_3479)))->BgL_argsz00);
										{
											obj_t BgL_l1733z00_3482;

											BgL_l1733z00_3482 = BgL_g1735z00_3480;
										BgL_zc3z04anonymousza31836ze3z87_3483:
											if (PAIRP(BgL_l1733z00_3482))
												{	/* Cfa/setup.scm 49 */
													{	/* Cfa/setup.scm 46 */
														obj_t BgL_localz00_3485;

														BgL_localz00_3485 = CAR(BgL_l1733z00_3482);
														{	/* Cfa/setup.scm 46 */
															BgL_reshapedzd2localzd2_bglt BgL_wide1175z00_3488;

															BgL_wide1175z00_3488 =
																((BgL_reshapedzd2localzd2_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_reshapedzd2localzd2_bgl))));
															{	/* Cfa/setup.scm 46 */
																obj_t BgL_auxz00_6281;
																BgL_objectz00_bglt BgL_tmpz00_6277;

																BgL_auxz00_6281 =
																	((obj_t) BgL_wide1175z00_3488);
																BgL_tmpz00_6277 =
																	((BgL_objectz00_bglt)
																	((BgL_localz00_bglt)
																		((BgL_localz00_bglt) BgL_localz00_3485)));
																BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6277,
																	BgL_auxz00_6281);
															}
															((BgL_objectz00_bglt)
																((BgL_localz00_bglt)
																	((BgL_localz00_bglt) BgL_localz00_3485)));
															{	/* Cfa/setup.scm 46 */
																long BgL_arg1838z00_3489;

																BgL_arg1838z00_3489 =
																	BGL_CLASS_NUM
																	(BGl_reshapedzd2localzd2zzcfa_infoz00);
																BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
																			(BgL_localz00_bglt) ((BgL_localz00_bglt)
																				BgL_localz00_3485))),
																	BgL_arg1838z00_3489);
															}
															((BgL_localz00_bglt)
																((BgL_localz00_bglt)
																	((BgL_localz00_bglt) BgL_localz00_3485)));
														}
														{
															BgL_reshapedzd2localzd2_bglt BgL_auxz00_6295;

															{
																obj_t BgL_auxz00_6296;

																{	/* Cfa/setup.scm 46 */
																	BgL_objectz00_bglt BgL_tmpz00_6297;

																	BgL_tmpz00_6297 =
																		((BgL_objectz00_bglt)
																		((BgL_localz00_bglt)
																			((BgL_localz00_bglt) BgL_localz00_3485)));
																	BgL_auxz00_6296 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_6297);
																}
																BgL_auxz00_6295 =
																	((BgL_reshapedzd2localzd2_bglt)
																	BgL_auxz00_6296);
															}
															((((BgL_reshapedzd2localzd2_bglt)
																		COBJECT(BgL_auxz00_6295))->
																	BgL_bindingzd2valuezd2) =
																((obj_t) BFALSE), BUNSPEC);
														}
														((BgL_localz00_bglt)
															((BgL_localz00_bglt) BgL_localz00_3485));
														{	/* Cfa/setup.scm 47 */
															BgL_valuez00_bglt BgL_arg1839z00_3491;

															BgL_arg1839z00_3491 =
																(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			((BgL_localz00_bglt)
																				BgL_localz00_3485))))->BgL_valuez00);
															BGl_variablezd2valuezd2setupz12z12zzcfa_setupz00
																(BgL_arg1839z00_3491,
																((BgL_variablez00_bglt) BgL_localz00_3485));
													}}
													{
														obj_t BgL_l1733z00_6311;

														BgL_l1733z00_6311 = CDR(BgL_l1733z00_3482);
														BgL_l1733z00_3482 = BgL_l1733z00_6311;
														goto BgL_zc3z04anonymousza31836ze3z87_3483;
													}
												}
											else
												{	/* Cfa/setup.scm 49 */
													((bool_t) 1);
												}
										}
									}
								}
								{	/* Cfa/setup.scm 50 */
									obj_t BgL_arg1842z00_3494;

									BgL_arg1842z00_3494 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt)
													(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_globalz00_bglt) BgL_globalz00_3478))))->
														BgL_valuez00))))->BgL_bodyz00);
									BGl_nodezd2setupz12zc0zzcfa_setupz00(((BgL_nodez00_bglt)
											BgL_arg1842z00_3494));
								}
							}
							{
								obj_t BgL_l1736z00_6320;

								BgL_l1736z00_6320 = CDR(BgL_l1736z00_3475);
								BgL_l1736z00_3475 = BgL_l1736z00_6320;
								goto BgL_zc3z04anonymousza31834ze3z87_3476;
							}
						}
					else
						{	/* Cfa/setup.scm 40 */
							BgL_tmpz00_6262 = ((bool_t) 1);
						}
					return BBOOL(BgL_tmpz00_6262);
				}
			}
		}

	}



/* &set-initial-approx! */
	obj_t BGl_z62setzd2initialzd2approxz12z70zzcfa_setupz00(obj_t BgL_envz00_5661,
		obj_t BgL_globalsz00_5662)
	{
		{	/* Cfa/setup.scm 38 */
			return
				BGl_setzd2initialzd2approxz12z12zzcfa_setupz00(BgL_globalsz00_5662);
		}

	}



/* alloc-type? */
	bool_t BGl_alloczd2typezf3z21zzcfa_setupz00(BgL_typez00_bglt BgL_typez00_10)
	{
		{	/* Cfa/setup.scm 111 */
			if ((((obj_t) BgL_typez00_10) == BGl_za2vectorza2z00zztype_cachez00))
				{	/* Cfa/setup.scm 113 */
					return ((bool_t) 1);
				}
			else
				{	/* Cfa/setup.scm 113 */
					if (
						(((obj_t) BgL_typez00_10) == BGl_za2procedureza2z00zztype_cachez00))
						{	/* Cfa/setup.scm 114 */
							return ((bool_t) 1);
						}
					else
						{	/* Cfa/setup.scm 114 */
							if (
								(((obj_t) BgL_typez00_10) ==
									BGl_za2structza2z00zztype_cachez00))
								{	/* Cfa/setup.scm 115 */
									return ((bool_t) 1);
								}
							else
								{	/* Cfa/setup.scm 115 */
									if (
										(((obj_t) BgL_typez00_10) ==
											BGl_za2pairza2z00zztype_cachez00))
										{	/* Cfa/setup.scm 116 */
											return BGl_pairzd2optimzf3z21zzcfa_pairz00();
										}
									else
										{	/* Cfa/setup.scm 116 */
											return ((bool_t) 0);
										}
								}
						}
				}
		}

	}



/* node-setup*! */
	BGL_EXPORTED_DEF obj_t BGl_nodezd2setupza2z12z62zzcfa_setupz00(obj_t
		BgL_nodeza2za2_55)
	{
		{	/* Cfa/setup.scm 487 */
			{
				obj_t BgL_l1750z00_3499;

				{	/* Cfa/setup.scm 488 */
					bool_t BgL_tmpz00_6337;

					BgL_l1750z00_3499 = BgL_nodeza2za2_55;
				BgL_zc3z04anonymousza31845ze3z87_3500:
					if (PAIRP(BgL_l1750z00_3499))
						{	/* Cfa/setup.scm 488 */
							{	/* Cfa/setup.scm 488 */
								obj_t BgL_arg1847z00_3502;

								BgL_arg1847z00_3502 = CAR(BgL_l1750z00_3499);
								BGl_nodezd2setupz12zc0zzcfa_setupz00(
									((BgL_nodez00_bglt) BgL_arg1847z00_3502));
							}
							{
								obj_t BgL_l1750z00_6343;

								BgL_l1750z00_6343 = CDR(BgL_l1750z00_3499);
								BgL_l1750z00_3499 = BgL_l1750z00_6343;
								goto BgL_zc3z04anonymousza31845ze3z87_3500;
							}
						}
					else
						{	/* Cfa/setup.scm 488 */
							BgL_tmpz00_6337 = ((bool_t) 1);
						}
					return BBOOL(BgL_tmpz00_6337);
				}
			}
		}

	}



/* &node-setup*! */
	obj_t BGl_z62nodezd2setupza2z12z00zzcfa_setupz00(obj_t BgL_envz00_5663,
		obj_t BgL_nodeza2za2_5664)
	{
		{	/* Cfa/setup.scm 487 */
			return BGl_nodezd2setupza2z12z62zzcfa_setupz00(BgL_nodeza2za2_5664);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcfa_setupz00(void)
	{
		{	/* Cfa/setup.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcfa_setupz00(void)
	{
		{	/* Cfa/setup.scm 15 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_proc2285z00zzcfa_setupz00, BGl_nodez00zzast_nodez00,
				BGl_string2286z00zzcfa_setupz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_variablezd2valuezd2setupz12zd2envzc0zzcfa_setupz00,
				BGl_proc2287z00zzcfa_setupz00, BGl_valuez00zzast_varz00,
				BGl_string2288z00zzcfa_setupz00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_funzd2setupz12zd2envz12zzcfa_setupz00,
				BGl_proc2289z00zzcfa_setupz00, BGl_funz00zzast_varz00,
				BGl_string2290z00zzcfa_setupz00);
		}

	}



/* &fun-setup!1789 */
	obj_t BGl_z62funzd2setupz121789za2zzcfa_setupz00(obj_t BgL_envz00_5668,
		obj_t BgL_funz00_5669, obj_t BgL_varz00_5670)
	{
		{	/* Cfa/setup.scm 232 */
			{	/* Cfa/setup.scm 233 */
				bool_t BgL_test2399z00_6350;

				{	/* Cfa/setup.scm 233 */
					bool_t BgL_test2400z00_6351;

					{	/* Cfa/setup.scm 233 */
						obj_t BgL_classz00_5813;

						BgL_classz00_5813 = BGl_globalz00zzast_varz00;
						if (BGL_OBJECTP(BgL_varz00_5670))
							{	/* Cfa/setup.scm 233 */
								BgL_objectz00_bglt BgL_arg1807z00_5814;

								BgL_arg1807z00_5814 = (BgL_objectz00_bglt) (BgL_varz00_5670);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cfa/setup.scm 233 */
										long BgL_idxz00_5815;

										BgL_idxz00_5815 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5814);
										BgL_test2400z00_6351 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_5815 + 2L)) == BgL_classz00_5813);
									}
								else
									{	/* Cfa/setup.scm 233 */
										bool_t BgL_res2252z00_5818;

										{	/* Cfa/setup.scm 233 */
											obj_t BgL_oclassz00_5819;

											{	/* Cfa/setup.scm 233 */
												obj_t BgL_arg1815z00_5820;
												long BgL_arg1816z00_5821;

												BgL_arg1815z00_5820 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cfa/setup.scm 233 */
													long BgL_arg1817z00_5822;

													BgL_arg1817z00_5822 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5814);
													BgL_arg1816z00_5821 =
														(BgL_arg1817z00_5822 - OBJECT_TYPE);
												}
												BgL_oclassz00_5819 =
													VECTOR_REF(BgL_arg1815z00_5820, BgL_arg1816z00_5821);
											}
											{	/* Cfa/setup.scm 233 */
												bool_t BgL__ortest_1115z00_5823;

												BgL__ortest_1115z00_5823 =
													(BgL_classz00_5813 == BgL_oclassz00_5819);
												if (BgL__ortest_1115z00_5823)
													{	/* Cfa/setup.scm 233 */
														BgL_res2252z00_5818 = BgL__ortest_1115z00_5823;
													}
												else
													{	/* Cfa/setup.scm 233 */
														long BgL_odepthz00_5824;

														{	/* Cfa/setup.scm 233 */
															obj_t BgL_arg1804z00_5825;

															BgL_arg1804z00_5825 = (BgL_oclassz00_5819);
															BgL_odepthz00_5824 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_5825);
														}
														if ((2L < BgL_odepthz00_5824))
															{	/* Cfa/setup.scm 233 */
																obj_t BgL_arg1802z00_5826;

																{	/* Cfa/setup.scm 233 */
																	obj_t BgL_arg1803z00_5827;

																	BgL_arg1803z00_5827 = (BgL_oclassz00_5819);
																	BgL_arg1802z00_5826 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5827,
																		2L);
																}
																BgL_res2252z00_5818 =
																	(BgL_arg1802z00_5826 == BgL_classz00_5813);
															}
														else
															{	/* Cfa/setup.scm 233 */
																BgL_res2252z00_5818 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2400z00_6351 = BgL_res2252z00_5818;
									}
							}
						else
							{	/* Cfa/setup.scm 233 */
								BgL_test2400z00_6351 = ((bool_t) 0);
							}
					}
					if (BgL_test2400z00_6351)
						{	/* Cfa/setup.scm 233 */
							bool_t BgL_test2405z00_6374;

							{	/* Cfa/setup.scm 233 */
								obj_t BgL_classz00_5828;

								BgL_classz00_5828 = BGl_reshapedzd2globalzd2zzcfa_infoz00;
								if (BGL_OBJECTP(BgL_varz00_5670))
									{	/* Cfa/setup.scm 233 */
										BgL_objectz00_bglt BgL_arg1807z00_5829;

										BgL_arg1807z00_5829 =
											(BgL_objectz00_bglt) (BgL_varz00_5670);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Cfa/setup.scm 233 */
												long BgL_idxz00_5830;

												BgL_idxz00_5830 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5829);
												BgL_test2405z00_6374 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_5830 + 3L)) == BgL_classz00_5828);
											}
										else
											{	/* Cfa/setup.scm 233 */
												bool_t BgL_res2253z00_5833;

												{	/* Cfa/setup.scm 233 */
													obj_t BgL_oclassz00_5834;

													{	/* Cfa/setup.scm 233 */
														obj_t BgL_arg1815z00_5835;
														long BgL_arg1816z00_5836;

														BgL_arg1815z00_5835 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Cfa/setup.scm 233 */
															long BgL_arg1817z00_5837;

															BgL_arg1817z00_5837 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5829);
															BgL_arg1816z00_5836 =
																(BgL_arg1817z00_5837 - OBJECT_TYPE);
														}
														BgL_oclassz00_5834 =
															VECTOR_REF(BgL_arg1815z00_5835,
															BgL_arg1816z00_5836);
													}
													{	/* Cfa/setup.scm 233 */
														bool_t BgL__ortest_1115z00_5838;

														BgL__ortest_1115z00_5838 =
															(BgL_classz00_5828 == BgL_oclassz00_5834);
														if (BgL__ortest_1115z00_5838)
															{	/* Cfa/setup.scm 233 */
																BgL_res2253z00_5833 = BgL__ortest_1115z00_5838;
															}
														else
															{	/* Cfa/setup.scm 233 */
																long BgL_odepthz00_5839;

																{	/* Cfa/setup.scm 233 */
																	obj_t BgL_arg1804z00_5840;

																	BgL_arg1804z00_5840 = (BgL_oclassz00_5834);
																	BgL_odepthz00_5839 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_5840);
																}
																if ((3L < BgL_odepthz00_5839))
																	{	/* Cfa/setup.scm 233 */
																		obj_t BgL_arg1802z00_5841;

																		{	/* Cfa/setup.scm 233 */
																			obj_t BgL_arg1803z00_5842;

																			BgL_arg1803z00_5842 =
																				(BgL_oclassz00_5834);
																			BgL_arg1802z00_5841 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_5842, 3L);
																		}
																		BgL_res2253z00_5833 =
																			(BgL_arg1802z00_5841 ==
																			BgL_classz00_5828);
																	}
																else
																	{	/* Cfa/setup.scm 233 */
																		BgL_res2253z00_5833 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2405z00_6374 = BgL_res2253z00_5833;
											}
									}
								else
									{	/* Cfa/setup.scm 233 */
										BgL_test2405z00_6374 = ((bool_t) 0);
									}
							}
							if (BgL_test2405z00_6374)
								{	/* Cfa/setup.scm 233 */
									BgL_test2399z00_6350 = ((bool_t) 0);
								}
							else
								{	/* Cfa/setup.scm 233 */
									BgL_test2399z00_6350 = ((bool_t) 1);
								}
						}
					else
						{	/* Cfa/setup.scm 233 */
							BgL_test2399z00_6350 = ((bool_t) 0);
						}
				}
				if (BgL_test2399z00_6350)
					{	/* Cfa/setup.scm 233 */
						{	/* Cfa/setup.scm 234 */
							BgL_reshapedzd2globalzd2_bglt BgL_wide1236z00_5843;

							BgL_wide1236z00_5843 =
								((BgL_reshapedzd2globalzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_reshapedzd2globalzd2_bgl))));
							{	/* Cfa/setup.scm 234 */
								obj_t BgL_auxz00_6402;
								BgL_objectz00_bglt BgL_tmpz00_6398;

								BgL_auxz00_6402 = ((obj_t) BgL_wide1236z00_5843);
								BgL_tmpz00_6398 =
									((BgL_objectz00_bglt)
									((BgL_globalz00_bglt)
										((BgL_globalz00_bglt) BgL_varz00_5670)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6398, BgL_auxz00_6402);
							}
							((BgL_objectz00_bglt)
								((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_varz00_5670)));
							{	/* Cfa/setup.scm 234 */
								long BgL_arg1856z00_5844;

								BgL_arg1856z00_5844 =
									BGL_CLASS_NUM(BGl_reshapedzd2globalzd2zzcfa_infoz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_globalz00_bglt)
											((BgL_globalz00_bglt) BgL_varz00_5670))),
									BgL_arg1856z00_5844);
							}
							((BgL_globalz00_bglt)
								((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_varz00_5670)));
						}
						((obj_t)
							((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_varz00_5670)));
					}
				else
					{	/* Cfa/setup.scm 233 */
						BFALSE;
					}
			}
			return BUNSPEC;
		}

	}



/* &variable-value-setup1765 */
	obj_t BGl_z62variablezd2valuezd2setup1765z62zzcfa_setupz00(obj_t
		BgL_envz00_5671, obj_t BgL_valuez00_5672, obj_t BgL_variablez00_5673)
	{
		{	/* Cfa/setup.scm 122 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(0),
				BGl_string2291z00zzcfa_setupz00,
				((obj_t) ((BgL_valuez00_bglt) BgL_valuez00_5672)));
		}

	}



/* &node-setup!1752 */
	obj_t BGl_z62nodezd2setupz121752za2zzcfa_setupz00(obj_t BgL_envz00_5674,
		obj_t BgL_nodez00_5675)
	{
		{	/* Cfa/setup.scm 56 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(1),
				BGl_string2291z00zzcfa_setupz00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_5675)));
		}

	}



/* node-setup! */
	BGL_EXPORTED_DEF obj_t BGl_nodezd2setupz12zc0zzcfa_setupz00(BgL_nodez00_bglt
		BgL_nodez00_4)
	{
		{	/* Cfa/setup.scm 56 */
			{	/* Cfa/setup.scm 56 */
				obj_t BgL_method1754z00_3529;

				{	/* Cfa/setup.scm 56 */
					obj_t BgL_res2258z00_4756;

					{	/* Cfa/setup.scm 56 */
						long BgL_objzd2classzd2numz00_4727;

						BgL_objzd2classzd2numz00_4727 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_4));
						{	/* Cfa/setup.scm 56 */
							obj_t BgL_arg1811z00_4728;

							BgL_arg1811z00_4728 =
								PROCEDURE_REF(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
								(int) (1L));
							{	/* Cfa/setup.scm 56 */
								int BgL_offsetz00_4731;

								BgL_offsetz00_4731 = (int) (BgL_objzd2classzd2numz00_4727);
								{	/* Cfa/setup.scm 56 */
									long BgL_offsetz00_4732;

									BgL_offsetz00_4732 =
										((long) (BgL_offsetz00_4731) - OBJECT_TYPE);
									{	/* Cfa/setup.scm 56 */
										long BgL_modz00_4733;

										BgL_modz00_4733 =
											(BgL_offsetz00_4732 >> (int) ((long) ((int) (4L))));
										{	/* Cfa/setup.scm 56 */
											long BgL_restz00_4735;

											BgL_restz00_4735 =
												(BgL_offsetz00_4732 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Cfa/setup.scm 56 */

												{	/* Cfa/setup.scm 56 */
													obj_t BgL_bucketz00_4737;

													BgL_bucketz00_4737 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_4728), BgL_modz00_4733);
													BgL_res2258z00_4756 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_4737), BgL_restz00_4735);
					}}}}}}}}
					BgL_method1754z00_3529 = BgL_res2258z00_4756;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1754z00_3529, ((obj_t) BgL_nodez00_4));
			}
		}

	}



/* &node-setup! */
	obj_t BGl_z62nodezd2setupz12za2zzcfa_setupz00(obj_t BgL_envz00_5676,
		obj_t BgL_nodez00_5677)
	{
		{	/* Cfa/setup.scm 56 */
			return
				BGl_nodezd2setupz12zc0zzcfa_setupz00(
				((BgL_nodez00_bglt) BgL_nodez00_5677));
		}

	}



/* variable-value-setup! */
	obj_t BGl_variablezd2valuezd2setupz12z12zzcfa_setupz00(BgL_valuez00_bglt
		BgL_valuez00_11, BgL_variablez00_bglt BgL_variablez00_12)
	{
		{	/* Cfa/setup.scm 122 */
			{	/* Cfa/setup.scm 122 */
				obj_t BgL_method1766z00_3530;

				{	/* Cfa/setup.scm 122 */
					obj_t BgL_res2263z00_4787;

					{	/* Cfa/setup.scm 122 */
						long BgL_objzd2classzd2numz00_4758;

						BgL_objzd2classzd2numz00_4758 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_valuez00_11));
						{	/* Cfa/setup.scm 122 */
							obj_t BgL_arg1811z00_4759;

							BgL_arg1811z00_4759 =
								PROCEDURE_REF
								(BGl_variablezd2valuezd2setupz12zd2envzc0zzcfa_setupz00,
								(int) (1L));
							{	/* Cfa/setup.scm 122 */
								int BgL_offsetz00_4762;

								BgL_offsetz00_4762 = (int) (BgL_objzd2classzd2numz00_4758);
								{	/* Cfa/setup.scm 122 */
									long BgL_offsetz00_4763;

									BgL_offsetz00_4763 =
										((long) (BgL_offsetz00_4762) - OBJECT_TYPE);
									{	/* Cfa/setup.scm 122 */
										long BgL_modz00_4764;

										BgL_modz00_4764 =
											(BgL_offsetz00_4763 >> (int) ((long) ((int) (4L))));
										{	/* Cfa/setup.scm 122 */
											long BgL_restz00_4766;

											BgL_restz00_4766 =
												(BgL_offsetz00_4763 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Cfa/setup.scm 122 */

												{	/* Cfa/setup.scm 122 */
													obj_t BgL_bucketz00_4768;

													BgL_bucketz00_4768 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_4759), BgL_modz00_4764);
													BgL_res2263z00_4787 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_4768), BgL_restz00_4766);
					}}}}}}}}
					BgL_method1766z00_3530 = BgL_res2263z00_4787;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1766z00_3530,
					((obj_t) BgL_valuez00_11), ((obj_t) BgL_variablez00_12));
			}
		}

	}



/* &variable-value-setup! */
	obj_t BGl_z62variablezd2valuezd2setupz12z70zzcfa_setupz00(obj_t
		BgL_envz00_5678, obj_t BgL_valuez00_5679, obj_t BgL_variablez00_5680)
	{
		{	/* Cfa/setup.scm 122 */
			return
				BGl_variablezd2valuezd2setupz12z12zzcfa_setupz00(
				((BgL_valuez00_bglt) BgL_valuez00_5679),
				((BgL_variablez00_bglt) BgL_variablez00_5680));
		}

	}



/* fun-setup! */
	obj_t BGl_funzd2setupz12zc0zzcfa_setupz00(BgL_funz00_bglt BgL_funz00_31,
		obj_t BgL_varz00_32)
	{
		{	/* Cfa/setup.scm 232 */
			{	/* Cfa/setup.scm 232 */
				obj_t BgL_method1790z00_3531;

				{	/* Cfa/setup.scm 232 */
					obj_t BgL_res2268z00_4818;

					{	/* Cfa/setup.scm 232 */
						long BgL_objzd2classzd2numz00_4789;

						BgL_objzd2classzd2numz00_4789 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_funz00_31));
						{	/* Cfa/setup.scm 232 */
							obj_t BgL_arg1811z00_4790;

							BgL_arg1811z00_4790 =
								PROCEDURE_REF(BGl_funzd2setupz12zd2envz12zzcfa_setupz00,
								(int) (1L));
							{	/* Cfa/setup.scm 232 */
								int BgL_offsetz00_4793;

								BgL_offsetz00_4793 = (int) (BgL_objzd2classzd2numz00_4789);
								{	/* Cfa/setup.scm 232 */
									long BgL_offsetz00_4794;

									BgL_offsetz00_4794 =
										((long) (BgL_offsetz00_4793) - OBJECT_TYPE);
									{	/* Cfa/setup.scm 232 */
										long BgL_modz00_4795;

										BgL_modz00_4795 =
											(BgL_offsetz00_4794 >> (int) ((long) ((int) (4L))));
										{	/* Cfa/setup.scm 232 */
											long BgL_restz00_4797;

											BgL_restz00_4797 =
												(BgL_offsetz00_4794 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Cfa/setup.scm 232 */

												{	/* Cfa/setup.scm 232 */
													obj_t BgL_bucketz00_4799;

													BgL_bucketz00_4799 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_4790), BgL_modz00_4795);
													BgL_res2268z00_4818 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_4799), BgL_restz00_4797);
					}}}}}}}}
					BgL_method1790z00_3531 = BgL_res2268z00_4818;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1790z00_3531,
					((obj_t) BgL_funz00_31), BgL_varz00_32);
			}
		}

	}



/* &fun-setup! */
	obj_t BGl_z62funzd2setupz12za2zzcfa_setupz00(obj_t BgL_envz00_5681,
		obj_t BgL_funz00_5682, obj_t BgL_varz00_5683)
	{
		{	/* Cfa/setup.scm 232 */
			return
				BGl_funzd2setupz12zc0zzcfa_setupz00(
				((BgL_funz00_bglt) BgL_funz00_5682), BgL_varz00_5683);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcfa_setupz00(void)
	{
		{	/* Cfa/setup.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_literalz00zzast_nodez00, BGl_proc2292z00zzcfa_setupz00,
				BGl_string2293z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00, BGl_patchz00zzast_nodez00,
				BGl_proc2294z00zzcfa_setupz00, BGl_string2293z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00, BGl_kwotez00zzast_nodez00,
				BGl_proc2295z00zzcfa_setupz00, BGl_string2293z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_kwotezf2nodezf2zzcfa_infoz00, BGl_proc2296z00zzcfa_setupz00,
				BGl_string2293z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00, BGl_varz00zzast_nodez00,
				BGl_proc2297z00zzcfa_setupz00, BGl_string2293z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_variablezd2valuezd2setupz12zd2envzc0zzcfa_setupz00,
				BGl_sfunz00zzast_varz00, BGl_proc2298z00zzcfa_setupz00,
				BGl_string2299z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_variablezd2valuezd2setupz12zd2envzc0zzcfa_setupz00,
				BGl_svarz00zzast_varz00, BGl_proc2300z00zzcfa_setupz00,
				BGl_string2299z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_variablezd2valuezd2setupz12zd2envzc0zzcfa_setupz00,
				BGl_prezd2clozd2envz00zzcfa_infoz00, BGl_proc2301z00zzcfa_setupz00,
				BGl_string2299z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_variablezd2valuezd2setupz12zd2envzc0zzcfa_setupz00,
				BGl_sexitz00zzast_varz00, BGl_proc2302z00zzcfa_setupz00,
				BGl_string2299z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_variablezd2valuezd2setupz12zd2envzc0zzcfa_setupz00,
				BGl_scnstzf2Cinfozf2zzcfa_infoz00, BGl_proc2303z00zzcfa_setupz00,
				BGl_string2299z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_variablezd2valuezd2setupz12zd2envzc0zzcfa_setupz00,
				BGl_scnstz00zzast_varz00, BGl_proc2304z00zzcfa_setupz00,
				BGl_string2299z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_variablezd2valuezd2setupz12zd2envzc0zzcfa_setupz00,
				BGl_cvarz00zzast_varz00, BGl_proc2305z00zzcfa_setupz00,
				BGl_string2299z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_closurez00zzast_nodez00, BGl_proc2306z00zzcfa_setupz00,
				BGl_string2293z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_sequencez00zzast_nodez00, BGl_proc2307z00zzcfa_setupz00,
				BGl_string2293z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00, BGl_syncz00zzast_nodez00,
				BGl_proc2308z00zzcfa_setupz00, BGl_string2293z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00, BGl_appz00zzast_nodez00,
				BGl_proc2309z00zzcfa_setupz00, BGl_string2293z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_funzd2setupz12zd2envz12zzcfa_setupz00, BGl_sfunz00zzast_varz00,
				BGl_proc2310z00zzcfa_setupz00, BGl_string2311z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_funzd2setupz12zd2envz12zzcfa_setupz00, BGl_cfunz00zzast_varz00,
				BGl_proc2312z00zzcfa_setupz00, BGl_string2311z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc2313z00zzcfa_setupz00,
				BGl_string2293z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_funcallz00zzast_nodez00, BGl_proc2314z00zzcfa_setupz00,
				BGl_string2293z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00, BGl_pragmaz00zzast_nodez00,
				BGl_proc2315z00zzcfa_setupz00, BGl_string2293z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_genpatchidz00zzast_nodez00, BGl_proc2316z00zzcfa_setupz00,
				BGl_string2293z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_getfieldz00zzast_nodez00, BGl_proc2317z00zzcfa_setupz00,
				BGl_string2293z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_setfieldz00zzast_nodez00, BGl_proc2318z00zzcfa_setupz00,
				BGl_string2293z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00, BGl_newz00zzast_nodez00,
				BGl_proc2319z00zzcfa_setupz00, BGl_string2293z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_instanceofz00zzast_nodez00, BGl_proc2320z00zzcfa_setupz00,
				BGl_string2293z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_castzd2nullzd2zzast_nodez00, BGl_proc2321z00zzcfa_setupz00,
				BGl_string2293z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00, BGl_castz00zzast_nodez00,
				BGl_proc2322z00zzcfa_setupz00, BGl_string2293z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00, BGl_setqz00zzast_nodez00,
				BGl_proc2323z00zzcfa_setupz00, BGl_string2293z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc2324z00zzcfa_setupz00,
				BGl_string2293z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00, BGl_failz00zzast_nodez00,
				BGl_proc2325z00zzcfa_setupz00, BGl_string2293z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00, BGl_switchz00zzast_nodez00,
				BGl_proc2326z00zzcfa_setupz00, BGl_string2293z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc2327z00zzcfa_setupz00,
				BGl_string2293z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc2328z00zzcfa_setupz00,
				BGl_string2293z00zzcfa_setupz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc2329z00zzcfa_setupz00,
				BGl_string2293z00zzcfa_setupz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc2330z00zzcfa_setupz00,
				BGl_string2293z00zzcfa_setupz00);
		}

	}



/* &node-setup!-jump-ex-1830 */
	obj_t BGl_z62nodezd2setupz12zd2jumpzd2exzd21830z70zzcfa_setupz00(obj_t
		BgL_envz00_5720, obj_t BgL_nodez00_5721)
	{
		{	/* Cfa/setup.scm 476 */
			BGl_nodezd2setupz12zc0zzcfa_setupz00(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_5721)))->BgL_exitz00));
			BGl_nodezd2setupz12zc0zzcfa_setupz00(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_5721)))->BgL_valuez00));
			{	/* Cfa/setup.scm 480 */
				BgL_jumpzd2exzd2itzf2cinfozf2_bglt BgL_wide1363z00_5849;

				BgL_wide1363z00_5849 =
					((BgL_jumpzd2exzd2itzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_jumpzd2exzd2itzf2cinfozf2_bgl))));
				{	/* Cfa/setup.scm 480 */
					obj_t BgL_auxz00_6574;
					BgL_objectz00_bglt BgL_tmpz00_6570;

					BgL_auxz00_6574 = ((obj_t) BgL_wide1363z00_5849);
					BgL_tmpz00_6570 =
						((BgL_objectz00_bglt)
						((BgL_jumpzd2exzd2itz00_bglt)
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_5721)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6570, BgL_auxz00_6574);
				}
				((BgL_objectz00_bglt)
					((BgL_jumpzd2exzd2itz00_bglt)
						((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_5721)));
				{	/* Cfa/setup.scm 480 */
					long BgL_arg2058z00_5850;

					BgL_arg2058z00_5850 =
						BGL_CLASS_NUM(BGl_jumpzd2exzd2itzf2Cinfozf2zzcfa_infoz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_jumpzd2exzd2itz00_bglt)
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_5721))),
						BgL_arg2058z00_5850);
				}
				((BgL_jumpzd2exzd2itz00_bglt)
					((BgL_jumpzd2exzd2itz00_bglt)
						((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_5721)));
			}
			{
				BgL_jumpzd2exzd2itzf2cinfozf2_bglt BgL_auxz00_6588;

				{
					obj_t BgL_auxz00_6589;

					{	/* Cfa/setup.scm 481 */
						BgL_objectz00_bglt BgL_tmpz00_6590;

						BgL_tmpz00_6590 =
							((BgL_objectz00_bglt)
							((BgL_jumpzd2exzd2itz00_bglt)
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_5721)));
						BgL_auxz00_6589 = BGL_OBJECT_WIDENING(BgL_tmpz00_6590);
					}
					BgL_auxz00_6588 =
						((BgL_jumpzd2exzd2itzf2cinfozf2_bglt) BgL_auxz00_6589);
				}
				((((BgL_jumpzd2exzd2itzf2cinfozf2_bglt) COBJECT(BgL_auxz00_6588))->
						BgL_approxz00) =
					((BgL_approxz00_bglt)
						BGl_makezd2typezd2approxz00zzcfa_approxz00(((BgL_typez00_bglt)
								BGl_za2objza2z00zztype_cachez00))), BUNSPEC);
			}
			((BgL_jumpzd2exzd2itz00_bglt)
				((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_5721));
			{	/* Cfa/setup.scm 482 */
				BgL_approxz00_bglt BgL_arg2059z00_5851;

				{
					BgL_jumpzd2exzd2itzf2cinfozf2_bglt BgL_auxz00_6601;

					{
						obj_t BgL_auxz00_6602;

						{	/* Cfa/setup.scm 482 */
							BgL_objectz00_bglt BgL_tmpz00_6603;

							BgL_tmpz00_6603 =
								((BgL_objectz00_bglt)
								((BgL_jumpzd2exzd2itz00_bglt)
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_5721)));
							BgL_auxz00_6602 = BGL_OBJECT_WIDENING(BgL_tmpz00_6603);
						}
						BgL_auxz00_6601 =
							((BgL_jumpzd2exzd2itzf2cinfozf2_bglt) BgL_auxz00_6602);
					}
					BgL_arg2059z00_5851 =
						(((BgL_jumpzd2exzd2itzf2cinfozf2_bglt) COBJECT(BgL_auxz00_6601))->
						BgL_approxz00);
				}
				return BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_arg2059z00_5851);
			}
		}

	}



/* &node-setup!-set-ex-i1828 */
	obj_t BGl_z62nodezd2setupz12zd2setzd2exzd2i1828z70zzcfa_setupz00(obj_t
		BgL_envz00_5722, obj_t BgL_nodez00_5723)
	{
		{	/* Cfa/setup.scm 463 */
			BGl_nodezd2setupz12zc0zzcfa_setupz00(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_5723)))->BgL_bodyz00));
			BGl_nodezd2setupz12zc0zzcfa_setupz00(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_5723)))->BgL_onexitz00));
			{	/* Cfa/setup.scm 467 */
				BgL_varz00_bglt BgL_arg2049z00_5853;

				BgL_arg2049z00_5853 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_5723)))->BgL_varz00);
				BGl_nodezd2setupz12zc0zzcfa_setupz00(
					((BgL_nodez00_bglt) BgL_arg2049z00_5853));
			}
			{	/* Cfa/setup.scm 468 */
				BgL_localz00_bglt BgL_tmp1352z00_5854;

				BgL_tmp1352z00_5854 =
					((BgL_localz00_bglt)
					(((BgL_varz00_bglt) COBJECT(
								(((BgL_setzd2exzd2itz00_bglt) COBJECT(
											((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_5723)))->
									BgL_varz00)))->BgL_variablez00));
				{	/* Cfa/setup.scm 468 */
					BgL_reshapedzd2localzd2_bglt BgL_wide1354z00_5855;

					BgL_wide1354z00_5855 =
						((BgL_reshapedzd2localzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_reshapedzd2localzd2_bgl))));
					{	/* Cfa/setup.scm 468 */
						obj_t BgL_auxz00_6629;
						BgL_objectz00_bglt BgL_tmpz00_6626;

						BgL_auxz00_6629 = ((obj_t) BgL_wide1354z00_5855);
						BgL_tmpz00_6626 =
							((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_tmp1352z00_5854));
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6626, BgL_auxz00_6629);
					}
					((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_tmp1352z00_5854));
					{	/* Cfa/setup.scm 468 */
						long BgL_arg2050z00_5856;

						BgL_arg2050z00_5856 =
							BGL_CLASS_NUM(BGl_reshapedzd2localzd2zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt)
								((BgL_localz00_bglt) BgL_tmp1352z00_5854)),
							BgL_arg2050z00_5856);
					}
					((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_tmp1352z00_5854));
				}
				{
					BgL_reshapedzd2localzd2_bglt BgL_auxz00_6640;

					{
						obj_t BgL_auxz00_6641;

						{	/* Cfa/setup.scm 468 */
							BgL_objectz00_bglt BgL_tmpz00_6642;

							BgL_tmpz00_6642 =
								((BgL_objectz00_bglt)
								((BgL_localz00_bglt) BgL_tmp1352z00_5854));
							BgL_auxz00_6641 = BGL_OBJECT_WIDENING(BgL_tmpz00_6642);
						}
						BgL_auxz00_6640 = ((BgL_reshapedzd2localzd2_bglt) BgL_auxz00_6641);
					}
					((((BgL_reshapedzd2localzd2_bglt) COBJECT(BgL_auxz00_6640))->
							BgL_bindingzd2valuezd2) = ((obj_t) BFALSE), BUNSPEC);
				}
				((BgL_localz00_bglt) BgL_tmp1352z00_5854);
			}
			{	/* Cfa/setup.scm 469 */
				BgL_setzd2exzd2itzf2cinfozf2_bglt BgL_wide1358z00_5857;

				BgL_wide1358z00_5857 =
					((BgL_setzd2exzd2itzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_setzd2exzd2itzf2cinfozf2_bgl))));
				{	/* Cfa/setup.scm 469 */
					obj_t BgL_auxz00_6654;
					BgL_objectz00_bglt BgL_tmpz00_6650;

					BgL_auxz00_6654 = ((obj_t) BgL_wide1358z00_5857);
					BgL_tmpz00_6650 =
						((BgL_objectz00_bglt)
						((BgL_setzd2exzd2itz00_bglt)
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_5723)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6650, BgL_auxz00_6654);
				}
				((BgL_objectz00_bglt)
					((BgL_setzd2exzd2itz00_bglt)
						((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_5723)));
				{	/* Cfa/setup.scm 469 */
					long BgL_arg2052z00_5858;

					BgL_arg2052z00_5858 =
						BGL_CLASS_NUM(BGl_setzd2exzd2itzf2Cinfozf2zzcfa_infoz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_setzd2exzd2itz00_bglt)
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_5723))),
						BgL_arg2052z00_5858);
				}
				((BgL_setzd2exzd2itz00_bglt)
					((BgL_setzd2exzd2itz00_bglt)
						((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_5723)));
			}
			{
				BgL_setzd2exzd2itzf2cinfozf2_bglt BgL_auxz00_6668;

				{
					obj_t BgL_auxz00_6669;

					{	/* Cfa/setup.scm 470 */
						BgL_objectz00_bglt BgL_tmpz00_6670;

						BgL_tmpz00_6670 =
							((BgL_objectz00_bglt)
							((BgL_setzd2exzd2itz00_bglt)
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_5723)));
						BgL_auxz00_6669 = BGL_OBJECT_WIDENING(BgL_tmpz00_6670);
					}
					BgL_auxz00_6668 =
						((BgL_setzd2exzd2itzf2cinfozf2_bglt) BgL_auxz00_6669);
				}
				((((BgL_setzd2exzd2itzf2cinfozf2_bglt) COBJECT(BgL_auxz00_6668))->
						BgL_approxz00) =
					((BgL_approxz00_bglt)
						BGl_makezd2typezd2approxz00zzcfa_approxz00(((BgL_typez00_bglt)
								BGl_za2objza2z00zztype_cachez00))), BUNSPEC);
			}
			((BgL_setzd2exzd2itz00_bglt)
				((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_5723));
			{	/* Cfa/setup.scm 471 */
				BgL_approxz00_bglt BgL_arg2055z00_5859;

				{
					BgL_setzd2exzd2itzf2cinfozf2_bglt BgL_auxz00_6681;

					{
						obj_t BgL_auxz00_6682;

						{	/* Cfa/setup.scm 471 */
							BgL_objectz00_bglt BgL_tmpz00_6683;

							BgL_tmpz00_6683 =
								((BgL_objectz00_bglt)
								((BgL_setzd2exzd2itz00_bglt)
									((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_5723)));
							BgL_auxz00_6682 = BGL_OBJECT_WIDENING(BgL_tmpz00_6683);
						}
						BgL_auxz00_6681 =
							((BgL_setzd2exzd2itzf2cinfozf2_bglt) BgL_auxz00_6682);
					}
					BgL_arg2055z00_5859 =
						(((BgL_setzd2exzd2itzf2cinfozf2_bglt) COBJECT(BgL_auxz00_6681))->
						BgL_approxz00);
				}
				return BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_arg2055z00_5859);
			}
		}

	}



/* &node-setup!-let-var1826 */
	obj_t BGl_z62nodezd2setupz12zd2letzd2var1826za2zzcfa_setupz00(obj_t
		BgL_envz00_5724, obj_t BgL_nodez00_5725)
	{
		{	/* Cfa/setup.scm 436 */
			{	/* Cfa/setup.scm 439 */
				obj_t BgL_g1749z00_5861;

				BgL_g1749z00_5861 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_5725)))->BgL_bindingsz00);
				{
					obj_t BgL_l1747z00_5863;

					BgL_l1747z00_5863 = BgL_g1749z00_5861;
				BgL_zc3z04anonymousza32037ze3z87_5862:
					if (PAIRP(BgL_l1747z00_5863))
						{	/* Cfa/setup.scm 439 */
							{	/* Cfa/setup.scm 440 */
								obj_t BgL_bindingz00_5864;

								BgL_bindingz00_5864 = CAR(BgL_l1747z00_5863);
								{	/* Cfa/setup.scm 440 */
									obj_t BgL_varz00_5865;
									obj_t BgL_valz00_5866;

									BgL_varz00_5865 = CAR(((obj_t) BgL_bindingz00_5864));
									BgL_valz00_5866 = CDR(((obj_t) BgL_bindingz00_5864));
									{	/* Cfa/setup.scm 442 */
										BgL_valuez00_bglt BgL_arg2039z00_5867;

										BgL_arg2039z00_5867 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt) BgL_varz00_5865))))->
											BgL_valuez00);
										BGl_variablezd2valuezd2setupz12z12zzcfa_setupz00
											(BgL_arg2039z00_5867,
											((BgL_variablez00_bglt) BgL_varz00_5865));
									}
									BGl_nodezd2setupz12zc0zzcfa_setupz00(
										((BgL_nodez00_bglt) BgL_valz00_5866));
									{	/* Cfa/setup.scm 447 */
										BgL_reshapedzd2localzd2_bglt BgL_wide1349z00_5868;

										BgL_wide1349z00_5868 =
											((BgL_reshapedzd2localzd2_bglt)
											BOBJECT(GC_MALLOC(sizeof(struct
														BgL_reshapedzd2localzd2_bgl))));
										{	/* Cfa/setup.scm 447 */
											obj_t BgL_auxz00_6712;
											BgL_objectz00_bglt BgL_tmpz00_6708;

											BgL_auxz00_6712 = ((obj_t) BgL_wide1349z00_5868);
											BgL_tmpz00_6708 =
												((BgL_objectz00_bglt)
												((BgL_localz00_bglt)
													((BgL_localz00_bglt) BgL_varz00_5865)));
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6708, BgL_auxz00_6712);
										}
										((BgL_objectz00_bglt)
											((BgL_localz00_bglt)
												((BgL_localz00_bglt) BgL_varz00_5865)));
										{	/* Cfa/setup.scm 447 */
											long BgL_arg2040z00_5869;

											BgL_arg2040z00_5869 =
												BGL_CLASS_NUM(BGl_reshapedzd2localzd2zzcfa_infoz00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt)
													((BgL_localz00_bglt)
														((BgL_localz00_bglt) BgL_varz00_5865))),
												BgL_arg2040z00_5869);
										}
										((BgL_localz00_bglt)
											((BgL_localz00_bglt)
												((BgL_localz00_bglt) BgL_varz00_5865)));
									}
									{
										obj_t BgL_auxz00_6734;
										BgL_reshapedzd2localzd2_bglt BgL_auxz00_6726;

										if (
											((((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_localz00_bglt) BgL_varz00_5865))))->
													BgL_accessz00) == CNST_TABLE_REF(2)))
											{	/* Cfa/setup.scm 448 */
												BgL_auxz00_6734 = BgL_valz00_5866;
											}
										else
											{	/* Cfa/setup.scm 448 */
												BgL_auxz00_6734 = BFALSE;
											}
										{
											obj_t BgL_auxz00_6727;

											{	/* Cfa/setup.scm 448 */
												BgL_objectz00_bglt BgL_tmpz00_6728;

												BgL_tmpz00_6728 =
													((BgL_objectz00_bglt)
													((BgL_localz00_bglt)
														((BgL_localz00_bglt) BgL_varz00_5865)));
												BgL_auxz00_6727 = BGL_OBJECT_WIDENING(BgL_tmpz00_6728);
											}
											BgL_auxz00_6726 =
												((BgL_reshapedzd2localzd2_bglt) BgL_auxz00_6727);
										}
										((((BgL_reshapedzd2localzd2_bglt)
													COBJECT(BgL_auxz00_6726))->BgL_bindingzd2valuezd2) =
											((obj_t) BgL_auxz00_6734), BUNSPEC);
									}
									((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_varz00_5865));
									BNIL;
								}
							}
							{
								obj_t BgL_l1747z00_6744;

								BgL_l1747z00_6744 = CDR(BgL_l1747z00_5863);
								BgL_l1747z00_5863 = BgL_l1747z00_6744;
								goto BgL_zc3z04anonymousza32037ze3z87_5862;
							}
						}
					else
						{	/* Cfa/setup.scm 439 */
							((bool_t) 1);
						}
				}
			}
			BGl_nodezd2setupz12zc0zzcfa_setupz00(
				(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_5725)))->BgL_bodyz00));
			return BNIL;
		}

	}



/* &node-setup!-let-fun1824 */
	obj_t BGl_z62nodezd2setupz12zd2letzd2fun1824za2zzcfa_setupz00(obj_t
		BgL_envz00_5726, obj_t BgL_nodez00_5727)
	{
		{	/* Cfa/setup.scm 419 */
			{	/* Cfa/setup.scm 421 */
				obj_t BgL_g1746z00_5871;

				BgL_g1746z00_5871 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_5727)))->BgL_localsz00);
				{
					obj_t BgL_l1744z00_5873;

					BgL_l1744z00_5873 = BgL_g1746z00_5871;
				BgL_zc3z04anonymousza32023ze3z87_5872:
					if (PAIRP(BgL_l1744z00_5873))
						{	/* Cfa/setup.scm 421 */
							{	/* Cfa/setup.scm 422 */
								obj_t BgL_lz00_5874;

								BgL_lz00_5874 = CAR(BgL_l1744z00_5873);
								{	/* Cfa/setup.scm 422 */
									BgL_reshapedzd2localzd2_bglt BgL_wide1339z00_5875;

									BgL_wide1339z00_5875 =
										((BgL_reshapedzd2localzd2_bglt)
										BOBJECT(GC_MALLOC(sizeof(struct
													BgL_reshapedzd2localzd2_bgl))));
									{	/* Cfa/setup.scm 422 */
										obj_t BgL_auxz00_6759;
										BgL_objectz00_bglt BgL_tmpz00_6755;

										BgL_auxz00_6759 = ((obj_t) BgL_wide1339z00_5875);
										BgL_tmpz00_6755 =
											((BgL_objectz00_bglt)
											((BgL_localz00_bglt)
												((BgL_localz00_bglt) BgL_lz00_5874)));
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6755, BgL_auxz00_6759);
									}
									((BgL_objectz00_bglt)
										((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_lz00_5874)));
									{	/* Cfa/setup.scm 422 */
										long BgL_arg2025z00_5876;

										BgL_arg2025z00_5876 =
											BGL_CLASS_NUM(BGl_reshapedzd2localzd2zzcfa_infoz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt)
												((BgL_localz00_bglt)
													((BgL_localz00_bglt) BgL_lz00_5874))),
											BgL_arg2025z00_5876);
									}
									((BgL_localz00_bglt)
										((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_lz00_5874)));
								}
								{
									BgL_reshapedzd2localzd2_bglt BgL_auxz00_6773;

									{
										obj_t BgL_auxz00_6774;

										{	/* Cfa/setup.scm 422 */
											BgL_objectz00_bglt BgL_tmpz00_6775;

											BgL_tmpz00_6775 =
												((BgL_objectz00_bglt)
												((BgL_localz00_bglt)
													((BgL_localz00_bglt) BgL_lz00_5874)));
											BgL_auxz00_6774 = BGL_OBJECT_WIDENING(BgL_tmpz00_6775);
										}
										BgL_auxz00_6773 =
											((BgL_reshapedzd2localzd2_bglt) BgL_auxz00_6774);
									}
									((((BgL_reshapedzd2localzd2_bglt) COBJECT(BgL_auxz00_6773))->
											BgL_bindingzd2valuezd2) = ((obj_t) BFALSE), BUNSPEC);
								}
								((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_lz00_5874));
								{	/* Cfa/setup.scm 423 */
									BgL_valuez00_bglt BgL_funz00_5877;

									BgL_funz00_5877 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_lz00_5874))))->BgL_valuez00);
									{	/* Cfa/setup.scm 424 */
										obj_t BgL_g1743z00_5878;

										BgL_g1743z00_5878 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_funz00_5877)))->BgL_argsz00);
										{
											obj_t BgL_l1741z00_5880;

											BgL_l1741z00_5880 = BgL_g1743z00_5878;
										BgL_zc3z04anonymousza32026ze3z87_5879:
											if (PAIRP(BgL_l1741z00_5880))
												{	/* Cfa/setup.scm 428 */
													{	/* Cfa/setup.scm 425 */
														obj_t BgL_localz00_5881;

														BgL_localz00_5881 = CAR(BgL_l1741z00_5880);
														{	/* Cfa/setup.scm 425 */
															BgL_reshapedzd2localzd2_bglt BgL_wide1343z00_5882;

															BgL_wide1343z00_5882 =
																((BgL_reshapedzd2localzd2_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_reshapedzd2localzd2_bgl))));
															{	/* Cfa/setup.scm 425 */
																obj_t BgL_auxz00_6797;
																BgL_objectz00_bglt BgL_tmpz00_6793;

																BgL_auxz00_6797 =
																	((obj_t) BgL_wide1343z00_5882);
																BgL_tmpz00_6793 =
																	((BgL_objectz00_bglt)
																	((BgL_localz00_bglt)
																		((BgL_localz00_bglt) BgL_localz00_5881)));
																BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6793,
																	BgL_auxz00_6797);
															}
															((BgL_objectz00_bglt)
																((BgL_localz00_bglt)
																	((BgL_localz00_bglt) BgL_localz00_5881)));
															{	/* Cfa/setup.scm 425 */
																long BgL_arg2029z00_5883;

																BgL_arg2029z00_5883 =
																	BGL_CLASS_NUM
																	(BGl_reshapedzd2localzd2zzcfa_infoz00);
																BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
																			(BgL_localz00_bglt) ((BgL_localz00_bglt)
																				BgL_localz00_5881))),
																	BgL_arg2029z00_5883);
															}
															((BgL_localz00_bglt)
																((BgL_localz00_bglt)
																	((BgL_localz00_bglt) BgL_localz00_5881)));
														}
														{
															BgL_reshapedzd2localzd2_bglt BgL_auxz00_6811;

															{
																obj_t BgL_auxz00_6812;

																{	/* Cfa/setup.scm 425 */
																	BgL_objectz00_bglt BgL_tmpz00_6813;

																	BgL_tmpz00_6813 =
																		((BgL_objectz00_bglt)
																		((BgL_localz00_bglt)
																			((BgL_localz00_bglt) BgL_localz00_5881)));
																	BgL_auxz00_6812 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_6813);
																}
																BgL_auxz00_6811 =
																	((BgL_reshapedzd2localzd2_bglt)
																	BgL_auxz00_6812);
															}
															((((BgL_reshapedzd2localzd2_bglt)
																		COBJECT(BgL_auxz00_6811))->
																	BgL_bindingzd2valuezd2) =
																((obj_t) BFALSE), BUNSPEC);
														}
														((BgL_localz00_bglt)
															((BgL_localz00_bglt) BgL_localz00_5881));
														{	/* Cfa/setup.scm 426 */
															BgL_valuez00_bglt BgL_arg2030z00_5884;

															BgL_arg2030z00_5884 =
																(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			((BgL_localz00_bglt)
																				BgL_localz00_5881))))->BgL_valuez00);
															BGl_variablezd2valuezd2setupz12z12zzcfa_setupz00
																(BgL_arg2030z00_5884,
																((BgL_variablez00_bglt) BgL_localz00_5881));
													}}
													{
														obj_t BgL_l1741z00_6827;

														BgL_l1741z00_6827 = CDR(BgL_l1741z00_5880);
														BgL_l1741z00_5880 = BgL_l1741z00_6827;
														goto BgL_zc3z04anonymousza32026ze3z87_5879;
													}
												}
											else
												{	/* Cfa/setup.scm 428 */
													((bool_t) 1);
												}
										}
									}
									{	/* Cfa/setup.scm 429 */
										obj_t BgL_arg2033z00_5885;

										BgL_arg2033z00_5885 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_funz00_5877)))->BgL_bodyz00);
										BGl_nodezd2setupz12zc0zzcfa_setupz00(
											((BgL_nodez00_bglt) BgL_arg2033z00_5885));
									}
								}
							}
							{
								obj_t BgL_l1744z00_6833;

								BgL_l1744z00_6833 = CDR(BgL_l1744z00_5873);
								BgL_l1744z00_5873 = BgL_l1744z00_6833;
								goto BgL_zc3z04anonymousza32023ze3z87_5872;
							}
						}
					else
						{	/* Cfa/setup.scm 421 */
							((bool_t) 1);
						}
				}
			}
			return
				BGl_nodezd2setupz12zc0zzcfa_setupz00(
				(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_5727)))->BgL_bodyz00));
		}

	}



/* &node-setup!-switch1822 */
	obj_t BGl_z62nodezd2setupz12zd2switch1822z70zzcfa_setupz00(obj_t
		BgL_envz00_5728, obj_t BgL_nodez00_5729)
	{
		{	/* Cfa/setup.scm 407 */
			BGl_nodezd2setupz12zc0zzcfa_setupz00(
				(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_5729)))->BgL_testz00));
			{	/* Cfa/setup.scm 410 */
				obj_t BgL_g1740z00_5887;

				BgL_g1740z00_5887 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_5729)))->BgL_clausesz00);
				{
					obj_t BgL_l1738z00_5889;

					BgL_l1738z00_5889 = BgL_g1740z00_5887;
				BgL_zc3z04anonymousza32018ze3z87_5888:
					if (PAIRP(BgL_l1738z00_5889))
						{	/* Cfa/setup.scm 410 */
							{	/* Cfa/setup.scm 411 */
								obj_t BgL_clausez00_5890;

								BgL_clausez00_5890 = CAR(BgL_l1738z00_5889);
								{	/* Cfa/setup.scm 411 */
									obj_t BgL_arg2020z00_5891;

									BgL_arg2020z00_5891 = CDR(((obj_t) BgL_clausez00_5890));
									BGl_nodezd2setupz12zc0zzcfa_setupz00(
										((BgL_nodez00_bglt) BgL_arg2020z00_5891));
								}
							}
							{
								obj_t BgL_l1738z00_6850;

								BgL_l1738z00_6850 = CDR(BgL_l1738z00_5889);
								BgL_l1738z00_5889 = BgL_l1738z00_6850;
								goto BgL_zc3z04anonymousza32018ze3z87_5888;
							}
						}
					else
						{	/* Cfa/setup.scm 410 */
							((bool_t) 1);
						}
				}
			}
			{	/* Cfa/setup.scm 413 */
				BgL_switchzf2cinfozf2_bglt BgL_wide1334z00_5892;

				BgL_wide1334z00_5892 =
					((BgL_switchzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_switchzf2cinfozf2_bgl))));
				{	/* Cfa/setup.scm 413 */
					obj_t BgL_auxz00_6857;
					BgL_objectz00_bglt BgL_tmpz00_6853;

					BgL_auxz00_6857 = ((obj_t) BgL_wide1334z00_5892);
					BgL_tmpz00_6853 =
						((BgL_objectz00_bglt)
						((BgL_switchz00_bglt) ((BgL_switchz00_bglt) BgL_nodez00_5729)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6853, BgL_auxz00_6857);
				}
				((BgL_objectz00_bglt)
					((BgL_switchz00_bglt) ((BgL_switchz00_bglt) BgL_nodez00_5729)));
				{	/* Cfa/setup.scm 413 */
					long BgL_arg2022z00_5893;

					BgL_arg2022z00_5893 =
						BGL_CLASS_NUM(BGl_switchzf2Cinfozf2zzcfa_infoz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_switchz00_bglt)
								((BgL_switchz00_bglt) BgL_nodez00_5729))), BgL_arg2022z00_5893);
				}
				((BgL_switchz00_bglt)
					((BgL_switchz00_bglt) ((BgL_switchz00_bglt) BgL_nodez00_5729)));
			}
			{
				BgL_switchzf2cinfozf2_bglt BgL_auxz00_6871;

				{
					obj_t BgL_auxz00_6872;

					{	/* Cfa/setup.scm 414 */
						BgL_objectz00_bglt BgL_tmpz00_6873;

						BgL_tmpz00_6873 =
							((BgL_objectz00_bglt)
							((BgL_switchz00_bglt) ((BgL_switchz00_bglt) BgL_nodez00_5729)));
						BgL_auxz00_6872 = BGL_OBJECT_WIDENING(BgL_tmpz00_6873);
					}
					BgL_auxz00_6871 = ((BgL_switchzf2cinfozf2_bglt) BgL_auxz00_6872);
				}
				((((BgL_switchzf2cinfozf2_bglt) COBJECT(BgL_auxz00_6871))->
						BgL_approxz00) =
					((BgL_approxz00_bglt) BGl_makezd2emptyzd2approxz00zzcfa_approxz00()),
					BUNSPEC);
			}
			return
				((obj_t)
				((BgL_switchz00_bglt) ((BgL_switchz00_bglt) BgL_nodez00_5729)));
		}

	}



/* &node-setup!-fail1820 */
	obj_t BGl_z62nodezd2setupz12zd2fail1820z70zzcfa_setupz00(obj_t
		BgL_envz00_5730, obj_t BgL_nodez00_5731)
	{
		{	/* Cfa/setup.scm 396 */
			BGl_nodezd2setupz12zc0zzcfa_setupz00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_5731)))->BgL_procz00));
			BGl_nodezd2setupz12zc0zzcfa_setupz00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_5731)))->BgL_msgz00));
			BGl_nodezd2setupz12zc0zzcfa_setupz00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_5731)))->BgL_objz00));
			{	/* Cfa/setup.scm 401 */
				BgL_failzf2cinfozf2_bglt BgL_wide1329z00_5895;

				BgL_wide1329z00_5895 =
					((BgL_failzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_failzf2cinfozf2_bgl))));
				{	/* Cfa/setup.scm 401 */
					obj_t BgL_auxz00_6898;
					BgL_objectz00_bglt BgL_tmpz00_6894;

					BgL_auxz00_6898 = ((obj_t) BgL_wide1329z00_5895);
					BgL_tmpz00_6894 =
						((BgL_objectz00_bglt)
						((BgL_failz00_bglt) ((BgL_failz00_bglt) BgL_nodez00_5731)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6894, BgL_auxz00_6898);
				}
				((BgL_objectz00_bglt)
					((BgL_failz00_bglt) ((BgL_failz00_bglt) BgL_nodez00_5731)));
				{	/* Cfa/setup.scm 401 */
					long BgL_arg2016z00_5896;

					BgL_arg2016z00_5896 = BGL_CLASS_NUM(BGl_failzf2Cinfozf2zzcfa_infoz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_failz00_bglt)
								((BgL_failz00_bglt) BgL_nodez00_5731))), BgL_arg2016z00_5896);
				}
				((BgL_failz00_bglt)
					((BgL_failz00_bglt) ((BgL_failz00_bglt) BgL_nodez00_5731)));
			}
			{
				BgL_failzf2cinfozf2_bglt BgL_auxz00_6912;

				{
					obj_t BgL_auxz00_6913;

					{	/* Cfa/setup.scm 402 */
						BgL_objectz00_bglt BgL_tmpz00_6914;

						BgL_tmpz00_6914 =
							((BgL_objectz00_bglt)
							((BgL_failz00_bglt) ((BgL_failz00_bglt) BgL_nodez00_5731)));
						BgL_auxz00_6913 = BGL_OBJECT_WIDENING(BgL_tmpz00_6914);
					}
					BgL_auxz00_6912 = ((BgL_failzf2cinfozf2_bglt) BgL_auxz00_6913);
				}
				((((BgL_failzf2cinfozf2_bglt) COBJECT(BgL_auxz00_6912))->
						BgL_approxz00) =
					((BgL_approxz00_bglt)
						BGl_makezd2typezd2approxz00zzcfa_approxz00(((BgL_typez00_bglt)
								BGl_za2objza2z00zztype_cachez00))), BUNSPEC);
			}
			return
				((obj_t) ((BgL_failz00_bglt) ((BgL_failz00_bglt) BgL_nodez00_5731)));
		}

	}



/* &node-setup!-conditio1818 */
	obj_t BGl_z62nodezd2setupz12zd2conditio1818z70zzcfa_setupz00(obj_t
		BgL_envz00_5732, obj_t BgL_nodez00_5733)
	{
		{	/* Cfa/setup.scm 385 */
			BGl_nodezd2setupz12zc0zzcfa_setupz00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_5733)))->BgL_testz00));
			BGl_nodezd2setupz12zc0zzcfa_setupz00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_5733)))->BgL_truez00));
			BGl_nodezd2setupz12zc0zzcfa_setupz00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_5733)))->BgL_falsez00));
			{	/* Cfa/setup.scm 390 */
				BgL_conditionalzf2cinfozf2_bglt BgL_wide1324z00_5898;

				BgL_wide1324z00_5898 =
					((BgL_conditionalzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_conditionalzf2cinfozf2_bgl))));
				{	/* Cfa/setup.scm 390 */
					obj_t BgL_auxz00_6940;
					BgL_objectz00_bglt BgL_tmpz00_6936;

					BgL_auxz00_6940 = ((obj_t) BgL_wide1324z00_5898);
					BgL_tmpz00_6936 =
						((BgL_objectz00_bglt)
						((BgL_conditionalz00_bglt)
							((BgL_conditionalz00_bglt) BgL_nodez00_5733)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6936, BgL_auxz00_6940);
				}
				((BgL_objectz00_bglt)
					((BgL_conditionalz00_bglt)
						((BgL_conditionalz00_bglt) BgL_nodez00_5733)));
				{	/* Cfa/setup.scm 390 */
					long BgL_arg2012z00_5899;

					BgL_arg2012z00_5899 =
						BGL_CLASS_NUM(BGl_conditionalzf2Cinfozf2zzcfa_infoz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_conditionalz00_bglt)
								((BgL_conditionalz00_bglt) BgL_nodez00_5733))),
						BgL_arg2012z00_5899);
				}
				((BgL_conditionalz00_bglt)
					((BgL_conditionalz00_bglt)
						((BgL_conditionalz00_bglt) BgL_nodez00_5733)));
			}
			{
				BgL_conditionalzf2cinfozf2_bglt BgL_auxz00_6954;

				{
					obj_t BgL_auxz00_6955;

					{	/* Cfa/setup.scm 391 */
						BgL_objectz00_bglt BgL_tmpz00_6956;

						BgL_tmpz00_6956 =
							((BgL_objectz00_bglt)
							((BgL_conditionalz00_bglt)
								((BgL_conditionalz00_bglt) BgL_nodez00_5733)));
						BgL_auxz00_6955 = BGL_OBJECT_WIDENING(BgL_tmpz00_6956);
					}
					BgL_auxz00_6954 = ((BgL_conditionalzf2cinfozf2_bglt) BgL_auxz00_6955);
				}
				((((BgL_conditionalzf2cinfozf2_bglt) COBJECT(BgL_auxz00_6954))->
						BgL_approxz00) =
					((BgL_approxz00_bglt) BGl_makezd2emptyzd2approxz00zzcfa_approxz00()),
					BUNSPEC);
			}
			return
				((obj_t)
				((BgL_conditionalz00_bglt)
					((BgL_conditionalz00_bglt) BgL_nodez00_5733)));
		}

	}



/* &node-setup!-setq1816 */
	obj_t BGl_z62nodezd2setupz12zd2setq1816z70zzcfa_setupz00(obj_t
		BgL_envz00_5734, obj_t BgL_nodez00_5735)
	{
		{	/* Cfa/setup.scm 374 */
			BGl_nodezd2setupz12zc0zzcfa_setupz00(
				(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_5735)))->BgL_valuez00));
			{	/* Tools/trace.sch 53 */
				BgL_varz00_bglt BgL_arg2007z00_5901;

				BgL_arg2007z00_5901 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_5735)))->BgL_varz00);
				BGl_nodezd2setupz12zc0zzcfa_setupz00(
					((BgL_nodez00_bglt) BgL_arg2007z00_5901));
			}
			{	/* Tools/trace.sch 53 */
				BgL_setqzf2cinfozf2_bglt BgL_wide1319z00_5902;

				BgL_wide1319z00_5902 =
					((BgL_setqzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_setqzf2cinfozf2_bgl))));
				{	/* Tools/trace.sch 53 */
					obj_t BgL_auxz00_6979;
					BgL_objectz00_bglt BgL_tmpz00_6975;

					BgL_auxz00_6979 = ((obj_t) BgL_wide1319z00_5902);
					BgL_tmpz00_6975 =
						((BgL_objectz00_bglt)
						((BgL_setqz00_bglt) ((BgL_setqz00_bglt) BgL_nodez00_5735)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6975, BgL_auxz00_6979);
				}
				((BgL_objectz00_bglt)
					((BgL_setqz00_bglt) ((BgL_setqz00_bglt) BgL_nodez00_5735)));
				{	/* Tools/trace.sch 53 */
					long BgL_arg2008z00_5903;

					BgL_arg2008z00_5903 = BGL_CLASS_NUM(BGl_setqzf2Cinfozf2zzcfa_infoz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_setqz00_bglt)
								((BgL_setqz00_bglt) BgL_nodez00_5735))), BgL_arg2008z00_5903);
				}
				((BgL_setqz00_bglt)
					((BgL_setqz00_bglt) ((BgL_setqz00_bglt) BgL_nodez00_5735)));
			}
			{
				BgL_setqzf2cinfozf2_bglt BgL_auxz00_6993;

				{
					obj_t BgL_auxz00_6994;

					{	/* Tools/trace.sch 53 */
						BgL_objectz00_bglt BgL_tmpz00_6995;

						BgL_tmpz00_6995 =
							((BgL_objectz00_bglt)
							((BgL_setqz00_bglt) ((BgL_setqz00_bglt) BgL_nodez00_5735)));
						BgL_auxz00_6994 = BGL_OBJECT_WIDENING(BgL_tmpz00_6995);
					}
					BgL_auxz00_6993 = ((BgL_setqzf2cinfozf2_bglt) BgL_auxz00_6994);
				}
				((((BgL_setqzf2cinfozf2_bglt) COBJECT(BgL_auxz00_6993))->
						BgL_approxz00) =
					((BgL_approxz00_bglt)
						BGl_makezd2typezd2approxz00zzcfa_approxz00(((BgL_typez00_bglt)
								BGl_za2unspecza2z00zztype_cachez00))), BUNSPEC);
			}
			return
				((obj_t) ((BgL_setqz00_bglt) ((BgL_setqz00_bglt) BgL_nodez00_5735)));
		}

	}



/* &node-setup!-cast1814 */
	obj_t BGl_z62nodezd2setupz12zd2cast1814z70zzcfa_setupz00(obj_t
		BgL_envz00_5736, obj_t BgL_nodez00_5737)
	{
		{	/* Cfa/setup.scm 367 */
			return
				BGl_nodezd2setupz12zc0zzcfa_setupz00(
				(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_5737)))->BgL_argz00));
		}

	}



/* &node-setup!-cast-nul1812 */
	obj_t BGl_z62nodezd2setupz12zd2castzd2nul1812za2zzcfa_setupz00(obj_t
		BgL_envz00_5738, obj_t BgL_nodez00_5739)
	{
		{	/* Cfa/setup.scm 359 */
			{	/* Cfa/setup.scm 361 */
				BgL_castzd2nullzf2cinfoz20_bglt BgL_wide1313z00_5906;

				BgL_wide1313z00_5906 =
					((BgL_castzd2nullzf2cinfoz20_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_castzd2nullzf2cinfoz20_bgl))));
				{	/* Cfa/setup.scm 361 */
					obj_t BgL_auxz00_7015;
					BgL_objectz00_bglt BgL_tmpz00_7011;

					BgL_auxz00_7015 = ((obj_t) BgL_wide1313z00_5906);
					BgL_tmpz00_7011 =
						((BgL_objectz00_bglt)
						((BgL_castzd2nullzd2_bglt)
							((BgL_castzd2nullzd2_bglt) BgL_nodez00_5739)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7011, BgL_auxz00_7015);
				}
				((BgL_objectz00_bglt)
					((BgL_castzd2nullzd2_bglt)
						((BgL_castzd2nullzd2_bglt) BgL_nodez00_5739)));
				{	/* Cfa/setup.scm 361 */
					long BgL_arg2002z00_5907;

					BgL_arg2002z00_5907 =
						BGL_CLASS_NUM(BGl_castzd2nullzf2Cinfoz20zzcfa_info3z00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_castzd2nullzd2_bglt)
								((BgL_castzd2nullzd2_bglt) BgL_nodez00_5739))),
						BgL_arg2002z00_5907);
				}
				((BgL_castzd2nullzd2_bglt)
					((BgL_castzd2nullzd2_bglt)
						((BgL_castzd2nullzd2_bglt) BgL_nodez00_5739)));
			}
			{
				BgL_approxz00_bglt BgL_auxz00_7037;
				BgL_castzd2nullzf2cinfoz20_bglt BgL_auxz00_7029;

				{	/* Cfa/setup.scm 362 */
					BgL_typez00_bglt BgL_arg2003z00_5908;

					BgL_arg2003z00_5908 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_castzd2nullzd2_bglt) BgL_nodez00_5739))))->BgL_typez00);
					BgL_auxz00_7037 =
						BGl_makezd2typezd2approxz00zzcfa_approxz00(BgL_arg2003z00_5908);
				}
				{
					obj_t BgL_auxz00_7030;

					{	/* Cfa/setup.scm 362 */
						BgL_objectz00_bglt BgL_tmpz00_7031;

						BgL_tmpz00_7031 =
							((BgL_objectz00_bglt)
							((BgL_castzd2nullzd2_bglt)
								((BgL_castzd2nullzd2_bglt) BgL_nodez00_5739)));
						BgL_auxz00_7030 = BGL_OBJECT_WIDENING(BgL_tmpz00_7031);
					}
					BgL_auxz00_7029 = ((BgL_castzd2nullzf2cinfoz20_bglt) BgL_auxz00_7030);
				}
				((((BgL_castzd2nullzf2cinfoz20_bglt) COBJECT(BgL_auxz00_7029))->
						BgL_approxz00) = ((BgL_approxz00_bglt) BgL_auxz00_7037), BUNSPEC);
			}
			return
				((obj_t)
				((BgL_castzd2nullzd2_bglt)
					((BgL_castzd2nullzd2_bglt) BgL_nodez00_5739)));
		}

	}



/* &node-setup!-instance1810 */
	obj_t BGl_z62nodezd2setupz12zd2instance1810z70zzcfa_setupz00(obj_t
		BgL_envz00_5740, obj_t BgL_nodez00_5741)
	{
		{	/* Cfa/setup.scm 349 */
			{	/* Cfa/setup.scm 351 */
				obj_t BgL_arg1998z00_5910;

				BgL_arg1998z00_5910 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_instanceofz00_bglt) BgL_nodez00_5741))))->BgL_exprza2za2);
				BGl_nodezd2setupza2z12z62zzcfa_setupz00(BgL_arg1998z00_5910);
			}
			{	/* Cfa/setup.scm 352 */
				BgL_instanceofzf2cinfozf2_bglt BgL_wide1308z00_5911;

				BgL_wide1308z00_5911 =
					((BgL_instanceofzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_instanceofzf2cinfozf2_bgl))));
				{	/* Cfa/setup.scm 352 */
					obj_t BgL_auxz00_7055;
					BgL_objectz00_bglt BgL_tmpz00_7051;

					BgL_auxz00_7055 = ((obj_t) BgL_wide1308z00_5911);
					BgL_tmpz00_7051 =
						((BgL_objectz00_bglt)
						((BgL_instanceofz00_bglt)
							((BgL_instanceofz00_bglt) BgL_nodez00_5741)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7051, BgL_auxz00_7055);
				}
				((BgL_objectz00_bglt)
					((BgL_instanceofz00_bglt)
						((BgL_instanceofz00_bglt) BgL_nodez00_5741)));
				{	/* Cfa/setup.scm 352 */
					long BgL_arg1999z00_5912;

					BgL_arg1999z00_5912 =
						BGL_CLASS_NUM(BGl_instanceofzf2Cinfozf2zzcfa_info3z00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_instanceofz00_bglt)
								((BgL_instanceofz00_bglt) BgL_nodez00_5741))),
						BgL_arg1999z00_5912);
				}
				((BgL_instanceofz00_bglt)
					((BgL_instanceofz00_bglt)
						((BgL_instanceofz00_bglt) BgL_nodez00_5741)));
			}
			{
				BgL_approxz00_bglt BgL_auxz00_7077;
				BgL_instanceofzf2cinfozf2_bglt BgL_auxz00_7069;

				{	/* Cfa/setup.scm 353 */
					BgL_typez00_bglt BgL_arg2000z00_5913;

					BgL_arg2000z00_5913 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_instanceofz00_bglt) BgL_nodez00_5741))))->BgL_typez00);
					BgL_auxz00_7077 =
						BGl_makezd2typezd2approxz00zzcfa_approxz00(BgL_arg2000z00_5913);
				}
				{
					obj_t BgL_auxz00_7070;

					{	/* Cfa/setup.scm 353 */
						BgL_objectz00_bglt BgL_tmpz00_7071;

						BgL_tmpz00_7071 =
							((BgL_objectz00_bglt)
							((BgL_instanceofz00_bglt)
								((BgL_instanceofz00_bglt) BgL_nodez00_5741)));
						BgL_auxz00_7070 = BGL_OBJECT_WIDENING(BgL_tmpz00_7071);
					}
					BgL_auxz00_7069 = ((BgL_instanceofzf2cinfozf2_bglt) BgL_auxz00_7070);
				}
				((((BgL_instanceofzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7069))->
						BgL_approxz00) = ((BgL_approxz00_bglt) BgL_auxz00_7077), BUNSPEC);
			}
			((BgL_instanceofz00_bglt) ((BgL_instanceofz00_bglt) BgL_nodez00_5741));
			{	/* Cfa/setup.scm 354 */
				BgL_approxz00_bglt BgL_arg2001z00_5914;

				{
					BgL_instanceofzf2cinfozf2_bglt BgL_auxz00_7085;

					{
						obj_t BgL_auxz00_7086;

						{	/* Cfa/setup.scm 354 */
							BgL_objectz00_bglt BgL_tmpz00_7087;

							BgL_tmpz00_7087 =
								((BgL_objectz00_bglt)
								((BgL_instanceofz00_bglt)
									((BgL_instanceofz00_bglt) BgL_nodez00_5741)));
							BgL_auxz00_7086 = BGL_OBJECT_WIDENING(BgL_tmpz00_7087);
						}
						BgL_auxz00_7085 =
							((BgL_instanceofzf2cinfozf2_bglt) BgL_auxz00_7086);
					}
					BgL_arg2001z00_5914 =
						(((BgL_instanceofzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7085))->
						BgL_approxz00);
				}
				return BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_arg2001z00_5914);
			}
		}

	}



/* &node-setup!-new1808 */
	obj_t BGl_z62nodezd2setupz12zd2new1808z70zzcfa_setupz00(obj_t BgL_envz00_5742,
		obj_t BgL_nodez00_5743)
	{
		{	/* Cfa/setup.scm 339 */
			{	/* Cfa/setup.scm 341 */
				obj_t BgL_arg1994z00_5916;

				BgL_arg1994z00_5916 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_newz00_bglt) BgL_nodez00_5743))))->BgL_exprza2za2);
				BGl_nodezd2setupza2z12z62zzcfa_setupz00(BgL_arg1994z00_5916);
			}
			{	/* Cfa/setup.scm 342 */
				BgL_newzf2cinfozf2_bglt BgL_wide1303z00_5917;

				BgL_wide1303z00_5917 =
					((BgL_newzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_newzf2cinfozf2_bgl))));
				{	/* Cfa/setup.scm 342 */
					obj_t BgL_auxz00_7104;
					BgL_objectz00_bglt BgL_tmpz00_7100;

					BgL_auxz00_7104 = ((obj_t) BgL_wide1303z00_5917);
					BgL_tmpz00_7100 =
						((BgL_objectz00_bglt)
						((BgL_newz00_bglt) ((BgL_newz00_bglt) BgL_nodez00_5743)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7100, BgL_auxz00_7104);
				}
				((BgL_objectz00_bglt)
					((BgL_newz00_bglt) ((BgL_newz00_bglt) BgL_nodez00_5743)));
				{	/* Cfa/setup.scm 342 */
					long BgL_arg1995z00_5918;

					BgL_arg1995z00_5918 = BGL_CLASS_NUM(BGl_newzf2Cinfozf2zzcfa_info3z00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_newz00_bglt)
								((BgL_newz00_bglt) BgL_nodez00_5743))), BgL_arg1995z00_5918);
				}
				((BgL_newz00_bglt)
					((BgL_newz00_bglt) ((BgL_newz00_bglt) BgL_nodez00_5743)));
			}
			{
				BgL_approxz00_bglt BgL_auxz00_7126;
				BgL_newzf2cinfozf2_bglt BgL_auxz00_7118;

				{	/* Cfa/setup.scm 343 */
					BgL_typez00_bglt BgL_arg1996z00_5919;

					BgL_arg1996z00_5919 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_newz00_bglt) BgL_nodez00_5743))))->BgL_typez00);
					BgL_auxz00_7126 =
						BGl_makezd2typezd2approxz00zzcfa_approxz00(BgL_arg1996z00_5919);
				}
				{
					obj_t BgL_auxz00_7119;

					{	/* Cfa/setup.scm 343 */
						BgL_objectz00_bglt BgL_tmpz00_7120;

						BgL_tmpz00_7120 =
							((BgL_objectz00_bglt)
							((BgL_newz00_bglt) ((BgL_newz00_bglt) BgL_nodez00_5743)));
						BgL_auxz00_7119 = BGL_OBJECT_WIDENING(BgL_tmpz00_7120);
					}
					BgL_auxz00_7118 = ((BgL_newzf2cinfozf2_bglt) BgL_auxz00_7119);
				}
				((((BgL_newzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7118))->BgL_approxz00) =
					((BgL_approxz00_bglt) BgL_auxz00_7126), BUNSPEC);
			}
			((BgL_newz00_bglt) ((BgL_newz00_bglt) BgL_nodez00_5743));
			{	/* Cfa/setup.scm 344 */
				BgL_approxz00_bglt BgL_arg1997z00_5920;

				{
					BgL_newzf2cinfozf2_bglt BgL_auxz00_7134;

					{
						obj_t BgL_auxz00_7135;

						{	/* Cfa/setup.scm 344 */
							BgL_objectz00_bglt BgL_tmpz00_7136;

							BgL_tmpz00_7136 =
								((BgL_objectz00_bglt)
								((BgL_newz00_bglt) ((BgL_newz00_bglt) BgL_nodez00_5743)));
							BgL_auxz00_7135 = BGL_OBJECT_WIDENING(BgL_tmpz00_7136);
						}
						BgL_auxz00_7134 = ((BgL_newzf2cinfozf2_bglt) BgL_auxz00_7135);
					}
					BgL_arg1997z00_5920 =
						(((BgL_newzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7134))->
						BgL_approxz00);
				}
				return BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_arg1997z00_5920);
			}
		}

	}



/* &node-setup!-setfield1806 */
	obj_t BGl_z62nodezd2setupz12zd2setfield1806z70zzcfa_setupz00(obj_t
		BgL_envz00_5744, obj_t BgL_nodez00_5745)
	{
		{	/* Cfa/setup.scm 329 */
			{	/* Cfa/setup.scm 331 */
				obj_t BgL_arg1990z00_5922;

				BgL_arg1990z00_5922 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_setfieldz00_bglt) BgL_nodez00_5745))))->BgL_exprza2za2);
				BGl_nodezd2setupza2z12z62zzcfa_setupz00(BgL_arg1990z00_5922);
			}
			{	/* Cfa/setup.scm 332 */
				BgL_setfieldzf2cinfozf2_bglt BgL_wide1298z00_5923;

				BgL_wide1298z00_5923 =
					((BgL_setfieldzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_setfieldzf2cinfozf2_bgl))));
				{	/* Cfa/setup.scm 332 */
					obj_t BgL_auxz00_7153;
					BgL_objectz00_bglt BgL_tmpz00_7149;

					BgL_auxz00_7153 = ((obj_t) BgL_wide1298z00_5923);
					BgL_tmpz00_7149 =
						((BgL_objectz00_bglt)
						((BgL_setfieldz00_bglt) ((BgL_setfieldz00_bglt) BgL_nodez00_5745)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7149, BgL_auxz00_7153);
				}
				((BgL_objectz00_bglt)
					((BgL_setfieldz00_bglt) ((BgL_setfieldz00_bglt) BgL_nodez00_5745)));
				{	/* Cfa/setup.scm 332 */
					long BgL_arg1991z00_5924;

					BgL_arg1991z00_5924 =
						BGL_CLASS_NUM(BGl_setfieldzf2Cinfozf2zzcfa_info3z00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_setfieldz00_bglt)
								((BgL_setfieldz00_bglt) BgL_nodez00_5745))),
						BgL_arg1991z00_5924);
				}
				((BgL_setfieldz00_bglt)
					((BgL_setfieldz00_bglt) ((BgL_setfieldz00_bglt) BgL_nodez00_5745)));
			}
			{
				BgL_approxz00_bglt BgL_auxz00_7175;
				BgL_setfieldzf2cinfozf2_bglt BgL_auxz00_7167;

				{	/* Cfa/setup.scm 333 */
					BgL_typez00_bglt BgL_arg1992z00_5925;

					BgL_arg1992z00_5925 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_setfieldz00_bglt) BgL_nodez00_5745))))->BgL_typez00);
					BgL_auxz00_7175 =
						BGl_makezd2typezd2approxz00zzcfa_approxz00(BgL_arg1992z00_5925);
				}
				{
					obj_t BgL_auxz00_7168;

					{	/* Cfa/setup.scm 333 */
						BgL_objectz00_bglt BgL_tmpz00_7169;

						BgL_tmpz00_7169 =
							((BgL_objectz00_bglt)
							((BgL_setfieldz00_bglt)
								((BgL_setfieldz00_bglt) BgL_nodez00_5745)));
						BgL_auxz00_7168 = BGL_OBJECT_WIDENING(BgL_tmpz00_7169);
					}
					BgL_auxz00_7167 = ((BgL_setfieldzf2cinfozf2_bglt) BgL_auxz00_7168);
				}
				((((BgL_setfieldzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7167))->
						BgL_approxz00) = ((BgL_approxz00_bglt) BgL_auxz00_7175), BUNSPEC);
			}
			((BgL_setfieldz00_bglt) ((BgL_setfieldz00_bglt) BgL_nodez00_5745));
			{	/* Cfa/setup.scm 334 */
				BgL_approxz00_bglt BgL_arg1993z00_5926;

				{
					BgL_setfieldzf2cinfozf2_bglt BgL_auxz00_7183;

					{
						obj_t BgL_auxz00_7184;

						{	/* Cfa/setup.scm 334 */
							BgL_objectz00_bglt BgL_tmpz00_7185;

							BgL_tmpz00_7185 =
								((BgL_objectz00_bglt)
								((BgL_setfieldz00_bglt)
									((BgL_setfieldz00_bglt) BgL_nodez00_5745)));
							BgL_auxz00_7184 = BGL_OBJECT_WIDENING(BgL_tmpz00_7185);
						}
						BgL_auxz00_7183 = ((BgL_setfieldzf2cinfozf2_bglt) BgL_auxz00_7184);
					}
					BgL_arg1993z00_5926 =
						(((BgL_setfieldzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7183))->
						BgL_approxz00);
				}
				return BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_arg1993z00_5926);
			}
		}

	}



/* &node-setup!-getfield1804 */
	obj_t BGl_z62nodezd2setupz12zd2getfield1804z70zzcfa_setupz00(obj_t
		BgL_envz00_5746, obj_t BgL_nodez00_5747)
	{
		{	/* Cfa/setup.scm 319 */
			{	/* Cfa/setup.scm 321 */
				obj_t BgL_arg1986z00_5928;

				BgL_arg1986z00_5928 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_getfieldz00_bglt) BgL_nodez00_5747))))->BgL_exprza2za2);
				BGl_nodezd2setupza2z12z62zzcfa_setupz00(BgL_arg1986z00_5928);
			}
			{	/* Cfa/setup.scm 322 */
				BgL_getfieldzf2cinfozf2_bglt BgL_wide1293z00_5929;

				BgL_wide1293z00_5929 =
					((BgL_getfieldzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_getfieldzf2cinfozf2_bgl))));
				{	/* Cfa/setup.scm 322 */
					obj_t BgL_auxz00_7202;
					BgL_objectz00_bglt BgL_tmpz00_7198;

					BgL_auxz00_7202 = ((obj_t) BgL_wide1293z00_5929);
					BgL_tmpz00_7198 =
						((BgL_objectz00_bglt)
						((BgL_getfieldz00_bglt) ((BgL_getfieldz00_bglt) BgL_nodez00_5747)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7198, BgL_auxz00_7202);
				}
				((BgL_objectz00_bglt)
					((BgL_getfieldz00_bglt) ((BgL_getfieldz00_bglt) BgL_nodez00_5747)));
				{	/* Cfa/setup.scm 322 */
					long BgL_arg1987z00_5930;

					BgL_arg1987z00_5930 =
						BGL_CLASS_NUM(BGl_getfieldzf2Cinfozf2zzcfa_info3z00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_getfieldz00_bglt)
								((BgL_getfieldz00_bglt) BgL_nodez00_5747))),
						BgL_arg1987z00_5930);
				}
				((BgL_getfieldz00_bglt)
					((BgL_getfieldz00_bglt) ((BgL_getfieldz00_bglt) BgL_nodez00_5747)));
			}
			{
				BgL_approxz00_bglt BgL_auxz00_7224;
				BgL_getfieldzf2cinfozf2_bglt BgL_auxz00_7216;

				{	/* Cfa/setup.scm 323 */
					BgL_typez00_bglt BgL_arg1988z00_5931;

					BgL_arg1988z00_5931 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_getfieldz00_bglt) BgL_nodez00_5747))))->BgL_typez00);
					BgL_auxz00_7224 =
						BGl_makezd2typezd2approxz00zzcfa_approxz00(BgL_arg1988z00_5931);
				}
				{
					obj_t BgL_auxz00_7217;

					{	/* Cfa/setup.scm 323 */
						BgL_objectz00_bglt BgL_tmpz00_7218;

						BgL_tmpz00_7218 =
							((BgL_objectz00_bglt)
							((BgL_getfieldz00_bglt)
								((BgL_getfieldz00_bglt) BgL_nodez00_5747)));
						BgL_auxz00_7217 = BGL_OBJECT_WIDENING(BgL_tmpz00_7218);
					}
					BgL_auxz00_7216 = ((BgL_getfieldzf2cinfozf2_bglt) BgL_auxz00_7217);
				}
				((((BgL_getfieldzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7216))->
						BgL_approxz00) = ((BgL_approxz00_bglt) BgL_auxz00_7224), BUNSPEC);
			}
			((BgL_getfieldz00_bglt) ((BgL_getfieldz00_bglt) BgL_nodez00_5747));
			{	/* Cfa/setup.scm 324 */
				BgL_approxz00_bglt BgL_arg1989z00_5932;

				{
					BgL_getfieldzf2cinfozf2_bglt BgL_auxz00_7232;

					{
						obj_t BgL_auxz00_7233;

						{	/* Cfa/setup.scm 324 */
							BgL_objectz00_bglt BgL_tmpz00_7234;

							BgL_tmpz00_7234 =
								((BgL_objectz00_bglt)
								((BgL_getfieldz00_bglt)
									((BgL_getfieldz00_bglt) BgL_nodez00_5747)));
							BgL_auxz00_7233 = BGL_OBJECT_WIDENING(BgL_tmpz00_7234);
						}
						BgL_auxz00_7232 = ((BgL_getfieldzf2cinfozf2_bglt) BgL_auxz00_7233);
					}
					BgL_arg1989z00_5932 =
						(((BgL_getfieldzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7232))->
						BgL_approxz00);
				}
				return BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_arg1989z00_5932);
			}
		}

	}



/* &node-setup!-genpatch1802 */
	obj_t BGl_z62nodezd2setupz12zd2genpatch1802z70zzcfa_setupz00(obj_t
		BgL_envz00_5748, obj_t BgL_nodez00_5749)
	{
		{	/* Cfa/setup.scm 310 */
			{	/* Cfa/setup.scm 312 */
				obj_t BgL_arg1984z00_5934;

				BgL_arg1984z00_5934 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_genpatchidz00_bglt) BgL_nodez00_5749))))->BgL_exprza2za2);
				BGl_nodezd2setupza2z12z62zzcfa_setupz00(BgL_arg1984z00_5934);
			}
			{	/* Cfa/setup.scm 313 */
				BgL_genpatchidzf2cinfozf2_bglt BgL_wide1288z00_5935;

				BgL_wide1288z00_5935 =
					((BgL_genpatchidzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_genpatchidzf2cinfozf2_bgl))));
				{	/* Cfa/setup.scm 313 */
					obj_t BgL_auxz00_7251;
					BgL_objectz00_bglt BgL_tmpz00_7247;

					BgL_auxz00_7251 = ((obj_t) BgL_wide1288z00_5935);
					BgL_tmpz00_7247 =
						((BgL_objectz00_bglt)
						((BgL_genpatchidz00_bglt)
							((BgL_genpatchidz00_bglt) BgL_nodez00_5749)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7247, BgL_auxz00_7251);
				}
				((BgL_objectz00_bglt)
					((BgL_genpatchidz00_bglt)
						((BgL_genpatchidz00_bglt) BgL_nodez00_5749)));
				{	/* Cfa/setup.scm 313 */
					long BgL_arg1985z00_5936;

					BgL_arg1985z00_5936 =
						BGL_CLASS_NUM(BGl_genpatchidzf2Cinfozf2zzcfa_infoz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_genpatchidz00_bglt)
								((BgL_genpatchidz00_bglt) BgL_nodez00_5749))),
						BgL_arg1985z00_5936);
				}
				((BgL_genpatchidz00_bglt)
					((BgL_genpatchidz00_bglt)
						((BgL_genpatchidz00_bglt) BgL_nodez00_5749)));
			}
			{
				BgL_genpatchidzf2cinfozf2_bglt BgL_auxz00_7265;

				{
					obj_t BgL_auxz00_7266;

					{	/* Cfa/setup.scm 314 */
						BgL_objectz00_bglt BgL_tmpz00_7267;

						BgL_tmpz00_7267 =
							((BgL_objectz00_bglt)
							((BgL_genpatchidz00_bglt)
								((BgL_genpatchidz00_bglt) BgL_nodez00_5749)));
						BgL_auxz00_7266 = BGL_OBJECT_WIDENING(BgL_tmpz00_7267);
					}
					BgL_auxz00_7265 = ((BgL_genpatchidzf2cinfozf2_bglt) BgL_auxz00_7266);
				}
				((((BgL_genpatchidzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7265))->
						BgL_approxz00) =
					((BgL_approxz00_bglt)
						BGl_makezd2typezd2approxz00zzcfa_approxz00(((BgL_typez00_bglt)
								BGl_za2longza2z00zztype_cachez00))), BUNSPEC);
			}
			return
				((obj_t)
				((BgL_genpatchidz00_bglt) ((BgL_genpatchidz00_bglt) BgL_nodez00_5749)));
		}

	}



/* &node-setup!-pragma1800 */
	obj_t BGl_z62nodezd2setupz12zd2pragma1800z70zzcfa_setupz00(obj_t
		BgL_envz00_5750, obj_t BgL_nodez00_5751)
	{
		{	/* Cfa/setup.scm 300 */
			{	/* Cfa/setup.scm 302 */
				obj_t BgL_arg1980z00_5938;

				BgL_arg1980z00_5938 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_pragmaz00_bglt) BgL_nodez00_5751))))->BgL_exprza2za2);
				BGl_nodezd2setupza2z12z62zzcfa_setupz00(BgL_arg1980z00_5938);
			}
			{	/* Cfa/setup.scm 303 */
				BgL_pragmazf2cinfozf2_bglt BgL_wide1283z00_5939;

				BgL_wide1283z00_5939 =
					((BgL_pragmazf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_pragmazf2cinfozf2_bgl))));
				{	/* Cfa/setup.scm 303 */
					obj_t BgL_auxz00_7288;
					BgL_objectz00_bglt BgL_tmpz00_7284;

					BgL_auxz00_7288 = ((obj_t) BgL_wide1283z00_5939);
					BgL_tmpz00_7284 =
						((BgL_objectz00_bglt)
						((BgL_pragmaz00_bglt) ((BgL_pragmaz00_bglt) BgL_nodez00_5751)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7284, BgL_auxz00_7288);
				}
				((BgL_objectz00_bglt)
					((BgL_pragmaz00_bglt) ((BgL_pragmaz00_bglt) BgL_nodez00_5751)));
				{	/* Cfa/setup.scm 303 */
					long BgL_arg1981z00_5940;

					BgL_arg1981z00_5940 =
						BGL_CLASS_NUM(BGl_pragmazf2Cinfozf2zzcfa_info3z00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_pragmaz00_bglt)
								((BgL_pragmaz00_bglt) BgL_nodez00_5751))), BgL_arg1981z00_5940);
				}
				((BgL_pragmaz00_bglt)
					((BgL_pragmaz00_bglt) ((BgL_pragmaz00_bglt) BgL_nodez00_5751)));
			}
			{
				BgL_approxz00_bglt BgL_auxz00_7310;
				BgL_pragmazf2cinfozf2_bglt BgL_auxz00_7302;

				{	/* Cfa/setup.scm 304 */
					BgL_typez00_bglt BgL_arg1982z00_5941;

					BgL_arg1982z00_5941 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_pragmaz00_bglt) BgL_nodez00_5751))))->BgL_typez00);
					BgL_auxz00_7310 =
						BGl_makezd2typezd2approxz00zzcfa_approxz00(BgL_arg1982z00_5941);
				}
				{
					obj_t BgL_auxz00_7303;

					{	/* Cfa/setup.scm 304 */
						BgL_objectz00_bglt BgL_tmpz00_7304;

						BgL_tmpz00_7304 =
							((BgL_objectz00_bglt)
							((BgL_pragmaz00_bglt) ((BgL_pragmaz00_bglt) BgL_nodez00_5751)));
						BgL_auxz00_7303 = BGL_OBJECT_WIDENING(BgL_tmpz00_7304);
					}
					BgL_auxz00_7302 = ((BgL_pragmazf2cinfozf2_bglt) BgL_auxz00_7303);
				}
				((((BgL_pragmazf2cinfozf2_bglt) COBJECT(BgL_auxz00_7302))->
						BgL_approxz00) = ((BgL_approxz00_bglt) BgL_auxz00_7310), BUNSPEC);
			}
			((BgL_pragmaz00_bglt) ((BgL_pragmaz00_bglt) BgL_nodez00_5751));
			{	/* Cfa/setup.scm 305 */
				BgL_approxz00_bglt BgL_arg1983z00_5942;

				{
					BgL_pragmazf2cinfozf2_bglt BgL_auxz00_7318;

					{
						obj_t BgL_auxz00_7319;

						{	/* Cfa/setup.scm 305 */
							BgL_objectz00_bglt BgL_tmpz00_7320;

							BgL_tmpz00_7320 =
								((BgL_objectz00_bglt)
								((BgL_pragmaz00_bglt) ((BgL_pragmaz00_bglt) BgL_nodez00_5751)));
							BgL_auxz00_7319 = BGL_OBJECT_WIDENING(BgL_tmpz00_7320);
						}
						BgL_auxz00_7318 = ((BgL_pragmazf2cinfozf2_bglt) BgL_auxz00_7319);
					}
					BgL_arg1983z00_5942 =
						(((BgL_pragmazf2cinfozf2_bglt) COBJECT(BgL_auxz00_7318))->
						BgL_approxz00);
				}
				return BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_arg1983z00_5942);
			}
		}

	}



/* &node-setup!-funcall1798 */
	obj_t BGl_z62nodezd2setupz12zd2funcall1798z70zzcfa_setupz00(obj_t
		BgL_envz00_5752, obj_t BgL_nodez00_5753)
	{
		{	/* Cfa/setup.scm 283 */
			BGl_nodezd2setupz12zc0zzcfa_setupz00(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_5753)))->BgL_funz00));
			BGl_nodezd2setupza2z12z62zzcfa_setupz00(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_5753)))->BgL_argsz00));
			if (CBOOL
				(BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00))
				{	/* Cfa/setup.scm 287 */
					{	/* Cfa/setup.scm 288 */
						BgL_funcallzf2cinfozf2_bglt BgL_wide1273z00_5944;

						BgL_wide1273z00_5944 =
							((BgL_funcallzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_funcallzf2cinfozf2_bgl))));
						{	/* Cfa/setup.scm 288 */
							obj_t BgL_auxz00_7341;
							BgL_objectz00_bglt BgL_tmpz00_7337;

							BgL_auxz00_7341 = ((obj_t) BgL_wide1273z00_5944);
							BgL_tmpz00_7337 =
								((BgL_objectz00_bglt)
								((BgL_funcallz00_bglt)
									((BgL_funcallz00_bglt) BgL_nodez00_5753)));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7337, BgL_auxz00_7341);
						}
						((BgL_objectz00_bglt)
							((BgL_funcallz00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_5753)));
						{	/* Cfa/setup.scm 288 */
							long BgL_arg1969z00_5945;

							BgL_arg1969z00_5945 =
								BGL_CLASS_NUM(BGl_funcallzf2Cinfozf2zzcfa_infoz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_funcallz00_bglt)
										((BgL_funcallz00_bglt) BgL_nodez00_5753))),
								BgL_arg1969z00_5945);
						}
						((BgL_funcallz00_bglt)
							((BgL_funcallz00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_5753)));
					}
					{
						BgL_funcallzf2cinfozf2_bglt BgL_auxz00_7355;

						{
							obj_t BgL_auxz00_7356;

							{	/* Cfa/setup.scm 289 */
								BgL_objectz00_bglt BgL_tmpz00_7357;

								BgL_tmpz00_7357 =
									((BgL_objectz00_bglt)
									((BgL_funcallz00_bglt)
										((BgL_funcallz00_bglt) BgL_nodez00_5753)));
								BgL_auxz00_7356 = BGL_OBJECT_WIDENING(BgL_tmpz00_7357);
							}
							BgL_auxz00_7355 = ((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_7356);
						}
						((((BgL_funcallzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7355))->
								BgL_approxz00) =
							((BgL_approxz00_bglt)
								BGl_makezd2emptyzd2approxz00zzcfa_approxz00()), BUNSPEC);
					}
					{
						BgL_funcallzf2cinfozf2_bglt BgL_auxz00_7365;

						{
							obj_t BgL_auxz00_7366;

							{	/* Cfa/setup.scm 290 */
								BgL_objectz00_bglt BgL_tmpz00_7367;

								BgL_tmpz00_7367 =
									((BgL_objectz00_bglt)
									((BgL_funcallz00_bglt)
										((BgL_funcallz00_bglt) BgL_nodez00_5753)));
								BgL_auxz00_7366 = BGL_OBJECT_WIDENING(BgL_tmpz00_7367);
							}
							BgL_auxz00_7365 = ((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_7366);
						}
						((((BgL_funcallzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7365))->
								BgL_vazd2approxzd2) =
							((BgL_approxz00_bglt)
								BGl_makezd2emptyzd2approxz00zzcfa_approxz00()), BUNSPEC);
					}
					{
						bool_t BgL_auxz00_7383;
						BgL_funcallzf2cinfozf2_bglt BgL_auxz00_7375;

						{	/* Cfa/setup.scm 290 */
							obj_t BgL_arg1970z00_5946;

							{	/* Cfa/setup.scm 290 */
								obj_t BgL_arg1971z00_5947;

								{	/* Cfa/setup.scm 290 */
									obj_t BgL_classz00_5948;

									BgL_classz00_5948 = BGl_funcallzf2Cinfozf2zzcfa_infoz00;
									BgL_arg1971z00_5947 = BGL_CLASS_ALL_FIELDS(BgL_classz00_5948);
								}
								BgL_arg1970z00_5946 = VECTOR_REF(BgL_arg1971z00_5947, 8L);
							}
							BgL_auxz00_7383 =
								CBOOL(BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
								(BgL_arg1970z00_5946));
						}
						{
							obj_t BgL_auxz00_7376;

							{	/* Cfa/setup.scm 290 */
								BgL_objectz00_bglt BgL_tmpz00_7377;

								BgL_tmpz00_7377 =
									((BgL_objectz00_bglt)
									((BgL_funcallz00_bglt)
										((BgL_funcallz00_bglt) BgL_nodez00_5753)));
								BgL_auxz00_7376 = BGL_OBJECT_WIDENING(BgL_tmpz00_7377);
							}
							BgL_auxz00_7375 = ((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_7376);
						}
						((((BgL_funcallzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7375))->
								BgL_arityzd2errorzd2noticedzf3zf3) =
							((bool_t) BgL_auxz00_7383), BUNSPEC);
					}
					{
						bool_t BgL_auxz00_7397;
						BgL_funcallzf2cinfozf2_bglt BgL_auxz00_7389;

						{	/* Cfa/setup.scm 290 */
							obj_t BgL_arg1972z00_5949;

							{	/* Cfa/setup.scm 290 */
								obj_t BgL_arg1973z00_5950;

								{	/* Cfa/setup.scm 290 */
									obj_t BgL_classz00_5951;

									BgL_classz00_5951 = BGl_funcallzf2Cinfozf2zzcfa_infoz00;
									BgL_arg1973z00_5950 = BGL_CLASS_ALL_FIELDS(BgL_classz00_5951);
								}
								BgL_arg1972z00_5949 = VECTOR_REF(BgL_arg1973z00_5950, 9L);
							}
							BgL_auxz00_7397 =
								CBOOL(BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
								(BgL_arg1972z00_5949));
						}
						{
							obj_t BgL_auxz00_7390;

							{	/* Cfa/setup.scm 290 */
								BgL_objectz00_bglt BgL_tmpz00_7391;

								BgL_tmpz00_7391 =
									((BgL_objectz00_bglt)
									((BgL_funcallz00_bglt)
										((BgL_funcallz00_bglt) BgL_nodez00_5753)));
								BgL_auxz00_7390 = BGL_OBJECT_WIDENING(BgL_tmpz00_7391);
							}
							BgL_auxz00_7389 = ((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_7390);
						}
						((((BgL_funcallzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7389))->
								BgL_typezd2errorzd2noticedzf3zf3) =
							((bool_t) BgL_auxz00_7397), BUNSPEC);
					}
					return
						((obj_t)
						((BgL_funcallz00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_5753)));
				}
			else
				{	/* Cfa/setup.scm 287 */
					{	/* Cfa/setup.scm 292 */
						BgL_funcallzf2cinfozf2_bglt BgL_wide1277z00_5952;

						BgL_wide1277z00_5952 =
							((BgL_funcallzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_funcallzf2cinfozf2_bgl))));
						{	/* Cfa/setup.scm 292 */
							obj_t BgL_auxz00_7411;
							BgL_objectz00_bglt BgL_tmpz00_7407;

							BgL_auxz00_7411 = ((obj_t) BgL_wide1277z00_5952);
							BgL_tmpz00_7407 =
								((BgL_objectz00_bglt)
								((BgL_funcallz00_bglt)
									((BgL_funcallz00_bglt) BgL_nodez00_5753)));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7407, BgL_auxz00_7411);
						}
						((BgL_objectz00_bglt)
							((BgL_funcallz00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_5753)));
						{	/* Cfa/setup.scm 292 */
							long BgL_arg1974z00_5953;

							BgL_arg1974z00_5953 =
								BGL_CLASS_NUM(BGl_funcallzf2Cinfozf2zzcfa_infoz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_funcallz00_bglt)
										((BgL_funcallz00_bglt) BgL_nodez00_5753))),
								BgL_arg1974z00_5953);
						}
						((BgL_funcallz00_bglt)
							((BgL_funcallz00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_5753)));
					}
					{
						BgL_funcallzf2cinfozf2_bglt BgL_auxz00_7425;

						{
							obj_t BgL_auxz00_7426;

							{	/* Cfa/setup.scm 293 */
								BgL_objectz00_bglt BgL_tmpz00_7427;

								BgL_tmpz00_7427 =
									((BgL_objectz00_bglt)
									((BgL_funcallz00_bglt)
										((BgL_funcallz00_bglt) BgL_nodez00_5753)));
								BgL_auxz00_7426 = BGL_OBJECT_WIDENING(BgL_tmpz00_7427);
							}
							BgL_auxz00_7425 = ((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_7426);
						}
						((((BgL_funcallzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7425))->
								BgL_approxz00) =
							((BgL_approxz00_bglt)
								BGl_makezd2typezd2approxz00zzcfa_approxz00(((BgL_typez00_bglt)
										BGl_za2objza2z00zztype_cachez00))), BUNSPEC);
					}
					{
						BgL_funcallzf2cinfozf2_bglt BgL_auxz00_7436;

						{
							obj_t BgL_auxz00_7437;

							{	/* Cfa/setup.scm 294 */
								BgL_objectz00_bglt BgL_tmpz00_7438;

								BgL_tmpz00_7438 =
									((BgL_objectz00_bglt)
									((BgL_funcallz00_bglt)
										((BgL_funcallz00_bglt) BgL_nodez00_5753)));
								BgL_auxz00_7437 = BGL_OBJECT_WIDENING(BgL_tmpz00_7438);
							}
							BgL_auxz00_7436 = ((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_7437);
						}
						((((BgL_funcallzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7436))->
								BgL_vazd2approxzd2) =
							((BgL_approxz00_bglt)
								BGl_makezd2typezd2approxz00zzcfa_approxz00(((BgL_typez00_bglt)
										BGl_za2objza2z00zztype_cachez00))), BUNSPEC);
					}
					{
						bool_t BgL_auxz00_7455;
						BgL_funcallzf2cinfozf2_bglt BgL_auxz00_7447;

						{	/* Cfa/setup.scm 294 */
							obj_t BgL_arg1975z00_5954;

							{	/* Cfa/setup.scm 294 */
								obj_t BgL_arg1976z00_5955;

								{	/* Cfa/setup.scm 294 */
									obj_t BgL_classz00_5956;

									BgL_classz00_5956 = BGl_funcallzf2Cinfozf2zzcfa_infoz00;
									BgL_arg1976z00_5955 = BGL_CLASS_ALL_FIELDS(BgL_classz00_5956);
								}
								BgL_arg1975z00_5954 = VECTOR_REF(BgL_arg1976z00_5955, 8L);
							}
							BgL_auxz00_7455 =
								CBOOL(BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
								(BgL_arg1975z00_5954));
						}
						{
							obj_t BgL_auxz00_7448;

							{	/* Cfa/setup.scm 294 */
								BgL_objectz00_bglt BgL_tmpz00_7449;

								BgL_tmpz00_7449 =
									((BgL_objectz00_bglt)
									((BgL_funcallz00_bglt)
										((BgL_funcallz00_bglt) BgL_nodez00_5753)));
								BgL_auxz00_7448 = BGL_OBJECT_WIDENING(BgL_tmpz00_7449);
							}
							BgL_auxz00_7447 = ((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_7448);
						}
						((((BgL_funcallzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7447))->
								BgL_arityzd2errorzd2noticedzf3zf3) =
							((bool_t) BgL_auxz00_7455), BUNSPEC);
					}
					{
						bool_t BgL_auxz00_7469;
						BgL_funcallzf2cinfozf2_bglt BgL_auxz00_7461;

						{	/* Cfa/setup.scm 294 */
							obj_t BgL_arg1977z00_5957;

							{	/* Cfa/setup.scm 294 */
								obj_t BgL_arg1978z00_5958;

								{	/* Cfa/setup.scm 294 */
									obj_t BgL_classz00_5959;

									BgL_classz00_5959 = BGl_funcallzf2Cinfozf2zzcfa_infoz00;
									BgL_arg1978z00_5958 = BGL_CLASS_ALL_FIELDS(BgL_classz00_5959);
								}
								BgL_arg1977z00_5957 = VECTOR_REF(BgL_arg1978z00_5958, 9L);
							}
							BgL_auxz00_7469 =
								CBOOL(BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
								(BgL_arg1977z00_5957));
						}
						{
							obj_t BgL_auxz00_7462;

							{	/* Cfa/setup.scm 294 */
								BgL_objectz00_bglt BgL_tmpz00_7463;

								BgL_tmpz00_7463 =
									((BgL_objectz00_bglt)
									((BgL_funcallz00_bglt)
										((BgL_funcallz00_bglt) BgL_nodez00_5753)));
								BgL_auxz00_7462 = BGL_OBJECT_WIDENING(BgL_tmpz00_7463);
							}
							BgL_auxz00_7461 = ((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_7462);
						}
						((((BgL_funcallzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7461))->
								BgL_typezd2errorzd2noticedzf3zf3) =
							((bool_t) BgL_auxz00_7469), BUNSPEC);
					}
					((BgL_funcallz00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_5753));
					{	/* Cfa/setup.scm 295 */
						BgL_approxz00_bglt BgL_arg1979z00_5960;

						{
							BgL_funcallzf2cinfozf2_bglt BgL_auxz00_7477;

							{
								obj_t BgL_auxz00_7478;

								{	/* Cfa/setup.scm 295 */
									BgL_objectz00_bglt BgL_tmpz00_7479;

									BgL_tmpz00_7479 =
										((BgL_objectz00_bglt)
										((BgL_funcallz00_bglt)
											((BgL_funcallz00_bglt) BgL_nodez00_5753)));
									BgL_auxz00_7478 = BGL_OBJECT_WIDENING(BgL_tmpz00_7479);
								}
								BgL_auxz00_7477 =
									((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_7478);
							}
							BgL_arg1979z00_5960 =
								(((BgL_funcallzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7477))->
								BgL_vazd2approxzd2);
						}
						return
							BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_arg1979z00_5960);
					}
				}
		}

	}



/* &node-setup!-app-ly1796 */
	obj_t BGl_z62nodezd2setupz12zd2appzd2ly1796za2zzcfa_setupz00(obj_t
		BgL_envz00_5754, obj_t BgL_nodez00_5755)
	{
		{	/* Cfa/setup.scm 268 */
			BGl_nodezd2setupz12zc0zzcfa_setupz00(
				(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_5755)))->BgL_funz00));
			BGl_nodezd2setupz12zc0zzcfa_setupz00(
				(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_5755)))->BgL_argz00));
			if (CBOOL
				(BGl_za2optimzd2cfazd2applyzd2trackingzf3za2z21zzengine_paramz00))
				{	/* Cfa/setup.scm 272 */
					{	/* Cfa/setup.scm 273 */
						BgL_appzd2lyzf2cinfoz20_bglt BgL_wide1264z00_5962;

						BgL_wide1264z00_5962 =
							((BgL_appzd2lyzf2cinfoz20_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_appzd2lyzf2cinfoz20_bgl))));
						{	/* Cfa/setup.scm 273 */
							obj_t BgL_auxz00_7500;
							BgL_objectz00_bglt BgL_tmpz00_7496;

							BgL_auxz00_7500 = ((obj_t) BgL_wide1264z00_5962);
							BgL_tmpz00_7496 =
								((BgL_objectz00_bglt)
								((BgL_appzd2lyzd2_bglt)
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_5755)));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7496, BgL_auxz00_7500);
						}
						((BgL_objectz00_bglt)
							((BgL_appzd2lyzd2_bglt)
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_5755)));
						{	/* Cfa/setup.scm 273 */
							long BgL_arg1964z00_5963;

							BgL_arg1964z00_5963 =
								BGL_CLASS_NUM(BGl_appzd2lyzf2Cinfoz20zzcfa_infoz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_appzd2lyzd2_bglt)
										((BgL_appzd2lyzd2_bglt) BgL_nodez00_5755))),
								BgL_arg1964z00_5963);
						}
						((BgL_appzd2lyzd2_bglt)
							((BgL_appzd2lyzd2_bglt)
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_5755)));
					}
					{
						BgL_appzd2lyzf2cinfoz20_bglt BgL_auxz00_7514;

						{
							obj_t BgL_auxz00_7515;

							{	/* Cfa/setup.scm 274 */
								BgL_objectz00_bglt BgL_tmpz00_7516;

								BgL_tmpz00_7516 =
									((BgL_objectz00_bglt)
									((BgL_appzd2lyzd2_bglt)
										((BgL_appzd2lyzd2_bglt) BgL_nodez00_5755)));
								BgL_auxz00_7515 = BGL_OBJECT_WIDENING(BgL_tmpz00_7516);
							}
							BgL_auxz00_7514 =
								((BgL_appzd2lyzf2cinfoz20_bglt) BgL_auxz00_7515);
						}
						((((BgL_appzd2lyzf2cinfoz20_bglt) COBJECT(BgL_auxz00_7514))->
								BgL_approxz00) =
							((BgL_approxz00_bglt)
								BGl_makezd2emptyzd2approxz00zzcfa_approxz00()), BUNSPEC);
					}
					return
						((obj_t)
						((BgL_appzd2lyzd2_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_5755)));
				}
			else
				{	/* Cfa/setup.scm 272 */
					{	/* Cfa/setup.scm 276 */
						BgL_appzd2lyzf2cinfoz20_bglt BgL_wide1268z00_5964;

						BgL_wide1268z00_5964 =
							((BgL_appzd2lyzf2cinfoz20_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_appzd2lyzf2cinfoz20_bgl))));
						{	/* Cfa/setup.scm 276 */
							obj_t BgL_auxz00_7532;
							BgL_objectz00_bglt BgL_tmpz00_7528;

							BgL_auxz00_7532 = ((obj_t) BgL_wide1268z00_5964);
							BgL_tmpz00_7528 =
								((BgL_objectz00_bglt)
								((BgL_appzd2lyzd2_bglt)
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_5755)));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7528, BgL_auxz00_7532);
						}
						((BgL_objectz00_bglt)
							((BgL_appzd2lyzd2_bglt)
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_5755)));
						{	/* Cfa/setup.scm 276 */
							long BgL_arg1965z00_5965;

							BgL_arg1965z00_5965 =
								BGL_CLASS_NUM(BGl_appzd2lyzf2Cinfoz20zzcfa_infoz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_appzd2lyzd2_bglt)
										((BgL_appzd2lyzd2_bglt) BgL_nodez00_5755))),
								BgL_arg1965z00_5965);
						}
						((BgL_appzd2lyzd2_bglt)
							((BgL_appzd2lyzd2_bglt)
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_5755)));
					}
					{
						BgL_appzd2lyzf2cinfoz20_bglt BgL_auxz00_7546;

						{
							obj_t BgL_auxz00_7547;

							{	/* Cfa/setup.scm 277 */
								BgL_objectz00_bglt BgL_tmpz00_7548;

								BgL_tmpz00_7548 =
									((BgL_objectz00_bglt)
									((BgL_appzd2lyzd2_bglt)
										((BgL_appzd2lyzd2_bglt) BgL_nodez00_5755)));
								BgL_auxz00_7547 = BGL_OBJECT_WIDENING(BgL_tmpz00_7548);
							}
							BgL_auxz00_7546 =
								((BgL_appzd2lyzf2cinfoz20_bglt) BgL_auxz00_7547);
						}
						((((BgL_appzd2lyzf2cinfoz20_bglt) COBJECT(BgL_auxz00_7546))->
								BgL_approxz00) =
							((BgL_approxz00_bglt)
								BGl_makezd2typezd2approxz00zzcfa_approxz00(((BgL_typez00_bglt)
										BGl_za2objza2z00zztype_cachez00))), BUNSPEC);
					}
					((BgL_appzd2lyzd2_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_5755));
					{	/* Cfa/setup.scm 278 */
						BgL_approxz00_bglt BgL_arg1966z00_5966;

						{
							BgL_appzd2lyzf2cinfoz20_bglt BgL_auxz00_7559;

							{
								obj_t BgL_auxz00_7560;

								{	/* Cfa/setup.scm 278 */
									BgL_objectz00_bglt BgL_tmpz00_7561;

									BgL_tmpz00_7561 =
										((BgL_objectz00_bglt)
										((BgL_appzd2lyzd2_bglt)
											((BgL_appzd2lyzd2_bglt) BgL_nodez00_5755)));
									BgL_auxz00_7560 = BGL_OBJECT_WIDENING(BgL_tmpz00_7561);
								}
								BgL_auxz00_7559 =
									((BgL_appzd2lyzf2cinfoz20_bglt) BgL_auxz00_7560);
							}
							BgL_arg1966z00_5966 =
								(((BgL_appzd2lyzf2cinfoz20_bglt) COBJECT(BgL_auxz00_7559))->
								BgL_approxz00);
						}
						return
							BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_arg1966z00_5966);
					}
				}
		}

	}



/* &fun-setup!-cfun1794 */
	obj_t BGl_z62funzd2setupz12zd2cfun1794z70zzcfa_setupz00(obj_t BgL_envz00_5756,
		obj_t BgL_funz00_5757, obj_t BgL_varz00_5758)
	{
		{	/* Cfa/setup.scm 257 */
			{
				BgL_cfunz00_bglt BgL_auxz00_7569;

				{	/* Cfa/setup.scm 258 */
					bool_t BgL_test2417z00_7570;

					{	/* Cfa/setup.scm 258 */
						obj_t BgL_classz00_5968;

						BgL_classz00_5968 = BGl_reshapedzd2globalzd2zzcfa_infoz00;
						if (BGL_OBJECTP(BgL_varz00_5758))
							{	/* Cfa/setup.scm 258 */
								BgL_objectz00_bglt BgL_arg1807z00_5969;

								BgL_arg1807z00_5969 = (BgL_objectz00_bglt) (BgL_varz00_5758);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cfa/setup.scm 258 */
										long BgL_idxz00_5970;

										BgL_idxz00_5970 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5969);
										BgL_test2417z00_7570 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_5970 + 3L)) == BgL_classz00_5968);
									}
								else
									{	/* Cfa/setup.scm 258 */
										bool_t BgL_res2279z00_5973;

										{	/* Cfa/setup.scm 258 */
											obj_t BgL_oclassz00_5974;

											{	/* Cfa/setup.scm 258 */
												obj_t BgL_arg1815z00_5975;
												long BgL_arg1816z00_5976;

												BgL_arg1815z00_5975 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cfa/setup.scm 258 */
													long BgL_arg1817z00_5977;

													BgL_arg1817z00_5977 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5969);
													BgL_arg1816z00_5976 =
														(BgL_arg1817z00_5977 - OBJECT_TYPE);
												}
												BgL_oclassz00_5974 =
													VECTOR_REF(BgL_arg1815z00_5975, BgL_arg1816z00_5976);
											}
											{	/* Cfa/setup.scm 258 */
												bool_t BgL__ortest_1115z00_5978;

												BgL__ortest_1115z00_5978 =
													(BgL_classz00_5968 == BgL_oclassz00_5974);
												if (BgL__ortest_1115z00_5978)
													{	/* Cfa/setup.scm 258 */
														BgL_res2279z00_5973 = BgL__ortest_1115z00_5978;
													}
												else
													{	/* Cfa/setup.scm 258 */
														long BgL_odepthz00_5979;

														{	/* Cfa/setup.scm 258 */
															obj_t BgL_arg1804z00_5980;

															BgL_arg1804z00_5980 = (BgL_oclassz00_5974);
															BgL_odepthz00_5979 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_5980);
														}
														if ((3L < BgL_odepthz00_5979))
															{	/* Cfa/setup.scm 258 */
																obj_t BgL_arg1802z00_5981;

																{	/* Cfa/setup.scm 258 */
																	obj_t BgL_arg1803z00_5982;

																	BgL_arg1803z00_5982 = (BgL_oclassz00_5974);
																	BgL_arg1802z00_5981 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5982,
																		3L);
																}
																BgL_res2279z00_5973 =
																	(BgL_arg1802z00_5981 == BgL_classz00_5968);
															}
														else
															{	/* Cfa/setup.scm 258 */
																BgL_res2279z00_5973 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2417z00_7570 = BgL_res2279z00_5973;
									}
							}
						else
							{	/* Cfa/setup.scm 258 */
								BgL_test2417z00_7570 = ((bool_t) 0);
							}
					}
					if (BgL_test2417z00_7570)
						{	/* Cfa/setup.scm 258 */
							BFALSE;
						}
					else
						{	/* Cfa/setup.scm 258 */
							{	/* Cfa/setup.scm 259 */
								BgL_reshapedzd2globalzd2_bglt BgL_wide1254z00_5983;

								BgL_wide1254z00_5983 =
									((BgL_reshapedzd2globalzd2_bglt)
									BOBJECT(GC_MALLOC(sizeof(struct
												BgL_reshapedzd2globalzd2_bgl))));
								{	/* Cfa/setup.scm 259 */
									obj_t BgL_auxz00_7598;
									BgL_objectz00_bglt BgL_tmpz00_7594;

									BgL_auxz00_7598 = ((obj_t) BgL_wide1254z00_5983);
									BgL_tmpz00_7594 =
										((BgL_objectz00_bglt)
										((BgL_globalz00_bglt)
											((BgL_globalz00_bglt) BgL_varz00_5758)));
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7594, BgL_auxz00_7598);
								}
								((BgL_objectz00_bglt)
									((BgL_globalz00_bglt)
										((BgL_globalz00_bglt) BgL_varz00_5758)));
								{	/* Cfa/setup.scm 259 */
									long BgL_arg1959z00_5984;

									BgL_arg1959z00_5984 =
										BGL_CLASS_NUM(BGl_reshapedzd2globalzd2zzcfa_infoz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt)
											((BgL_globalz00_bglt)
												((BgL_globalz00_bglt) BgL_varz00_5758))),
										BgL_arg1959z00_5984);
								}
								((BgL_globalz00_bglt)
									((BgL_globalz00_bglt)
										((BgL_globalz00_bglt) BgL_varz00_5758)));
							}
							((obj_t)
								((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_varz00_5758)));
				}}
				{	/* Cfa/setup.scm 260 */
					BgL_approxz00_bglt BgL_approxz00_5985;

					{	/* Cfa/setup.scm 260 */
						BgL_typez00_bglt BgL_arg1961z00_5986;

						BgL_arg1961z00_5986 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt) BgL_varz00_5758))))->BgL_typez00);
						BgL_approxz00_5985 =
							BGl_makezd2typezd2approxz00zzcfa_approxz00(BgL_arg1961z00_5986);
					}
					BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_approxz00_5985);
					{	/* Cfa/setup.scm 262 */
						BgL_cfunzf2cinfozf2_bglt BgL_wide1258z00_5987;

						BgL_wide1258z00_5987 =
							((BgL_cfunzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_cfunzf2cinfozf2_bgl))));
						{	/* Cfa/setup.scm 262 */
							obj_t BgL_auxz00_7625;
							BgL_objectz00_bglt BgL_tmpz00_7621;

							BgL_auxz00_7625 = ((obj_t) BgL_wide1258z00_5987);
							BgL_tmpz00_7621 =
								((BgL_objectz00_bglt)
								((BgL_cfunz00_bglt) ((BgL_cfunz00_bglt) BgL_funz00_5757)));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7621, BgL_auxz00_7625);
						}
						((BgL_objectz00_bglt)
							((BgL_cfunz00_bglt) ((BgL_cfunz00_bglt) BgL_funz00_5757)));
						{	/* Cfa/setup.scm 262 */
							long BgL_arg1960z00_5988;

							BgL_arg1960z00_5988 =
								BGL_CLASS_NUM(BGl_cfunzf2Cinfozf2zzcfa_infoz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_cfunz00_bglt)
										((BgL_cfunz00_bglt) BgL_funz00_5757))),
								BgL_arg1960z00_5988);
						}
						((BgL_cfunz00_bglt)
							((BgL_cfunz00_bglt) ((BgL_cfunz00_bglt) BgL_funz00_5757)));
					}
					{
						BgL_cfunzf2cinfozf2_bglt BgL_auxz00_7639;

						{
							obj_t BgL_auxz00_7640;

							{	/* Cfa/setup.scm 263 */
								BgL_objectz00_bglt BgL_tmpz00_7641;

								BgL_tmpz00_7641 =
									((BgL_objectz00_bglt)
									((BgL_cfunz00_bglt) ((BgL_cfunz00_bglt) BgL_funz00_5757)));
								BgL_auxz00_7640 = BGL_OBJECT_WIDENING(BgL_tmpz00_7641);
							}
							BgL_auxz00_7639 = ((BgL_cfunzf2cinfozf2_bglt) BgL_auxz00_7640);
						}
						((((BgL_cfunzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7639))->
								BgL_approxz00) =
							((BgL_approxz00_bglt) BgL_approxz00_5985), BUNSPEC);
					}
					BgL_auxz00_7569 =
						((BgL_cfunz00_bglt) ((BgL_cfunz00_bglt) BgL_funz00_5757));
				}
				return ((obj_t) BgL_auxz00_7569);
			}
		}

	}



/* &fun-setup!-sfun1792 */
	obj_t BGl_z62funzd2setupz12zd2sfun1792z70zzcfa_setupz00(obj_t BgL_envz00_5759,
		obj_t BgL_funz00_5760, obj_t BgL_varz00_5761)
	{
		{	/* Cfa/setup.scm 240 */
			{
				BgL_sfunz00_bglt BgL_auxz00_7651;

				{	/* Cfa/setup.scm 241 */
					bool_t BgL_test2422z00_7652;

					{	/* Cfa/setup.scm 241 */
						bool_t BgL_test2423z00_7653;

						{	/* Cfa/setup.scm 241 */
							obj_t BgL_classz00_5990;

							BgL_classz00_5990 = BGl_globalz00zzast_varz00;
							if (BGL_OBJECTP(BgL_varz00_5761))
								{	/* Cfa/setup.scm 241 */
									BgL_objectz00_bglt BgL_arg1807z00_5991;

									BgL_arg1807z00_5991 = (BgL_objectz00_bglt) (BgL_varz00_5761);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Cfa/setup.scm 241 */
											long BgL_idxz00_5992;

											BgL_idxz00_5992 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5991);
											BgL_test2423z00_7653 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_5992 + 2L)) == BgL_classz00_5990);
										}
									else
										{	/* Cfa/setup.scm 241 */
											bool_t BgL_res2276z00_5995;

											{	/* Cfa/setup.scm 241 */
												obj_t BgL_oclassz00_5996;

												{	/* Cfa/setup.scm 241 */
													obj_t BgL_arg1815z00_5997;
													long BgL_arg1816z00_5998;

													BgL_arg1815z00_5997 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Cfa/setup.scm 241 */
														long BgL_arg1817z00_5999;

														BgL_arg1817z00_5999 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5991);
														BgL_arg1816z00_5998 =
															(BgL_arg1817z00_5999 - OBJECT_TYPE);
													}
													BgL_oclassz00_5996 =
														VECTOR_REF(BgL_arg1815z00_5997,
														BgL_arg1816z00_5998);
												}
												{	/* Cfa/setup.scm 241 */
													bool_t BgL__ortest_1115z00_6000;

													BgL__ortest_1115z00_6000 =
														(BgL_classz00_5990 == BgL_oclassz00_5996);
													if (BgL__ortest_1115z00_6000)
														{	/* Cfa/setup.scm 241 */
															BgL_res2276z00_5995 = BgL__ortest_1115z00_6000;
														}
													else
														{	/* Cfa/setup.scm 241 */
															long BgL_odepthz00_6001;

															{	/* Cfa/setup.scm 241 */
																obj_t BgL_arg1804z00_6002;

																BgL_arg1804z00_6002 = (BgL_oclassz00_5996);
																BgL_odepthz00_6001 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_6002);
															}
															if ((2L < BgL_odepthz00_6001))
																{	/* Cfa/setup.scm 241 */
																	obj_t BgL_arg1802z00_6003;

																	{	/* Cfa/setup.scm 241 */
																		obj_t BgL_arg1803z00_6004;

																		BgL_arg1803z00_6004 = (BgL_oclassz00_5996);
																		BgL_arg1802z00_6003 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_6004, 2L);
																	}
																	BgL_res2276z00_5995 =
																		(BgL_arg1802z00_6003 == BgL_classz00_5990);
																}
															else
																{	/* Cfa/setup.scm 241 */
																	BgL_res2276z00_5995 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2423z00_7653 = BgL_res2276z00_5995;
										}
								}
							else
								{	/* Cfa/setup.scm 241 */
									BgL_test2423z00_7653 = ((bool_t) 0);
								}
						}
						if (BgL_test2423z00_7653)
							{	/* Cfa/setup.scm 241 */
								bool_t BgL_test2428z00_7676;

								{	/* Cfa/setup.scm 241 */
									obj_t BgL_classz00_6005;

									BgL_classz00_6005 = BGl_reshapedzd2globalzd2zzcfa_infoz00;
									if (BGL_OBJECTP(BgL_varz00_5761))
										{	/* Cfa/setup.scm 241 */
											BgL_objectz00_bglt BgL_arg1807z00_6006;

											BgL_arg1807z00_6006 =
												(BgL_objectz00_bglt) (BgL_varz00_5761);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Cfa/setup.scm 241 */
													long BgL_idxz00_6007;

													BgL_idxz00_6007 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6006);
													BgL_test2428z00_7676 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_6007 + 3L)) == BgL_classz00_6005);
												}
											else
												{	/* Cfa/setup.scm 241 */
													bool_t BgL_res2277z00_6010;

													{	/* Cfa/setup.scm 241 */
														obj_t BgL_oclassz00_6011;

														{	/* Cfa/setup.scm 241 */
															obj_t BgL_arg1815z00_6012;
															long BgL_arg1816z00_6013;

															BgL_arg1815z00_6012 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Cfa/setup.scm 241 */
																long BgL_arg1817z00_6014;

																BgL_arg1817z00_6014 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6006);
																BgL_arg1816z00_6013 =
																	(BgL_arg1817z00_6014 - OBJECT_TYPE);
															}
															BgL_oclassz00_6011 =
																VECTOR_REF(BgL_arg1815z00_6012,
																BgL_arg1816z00_6013);
														}
														{	/* Cfa/setup.scm 241 */
															bool_t BgL__ortest_1115z00_6015;

															BgL__ortest_1115z00_6015 =
																(BgL_classz00_6005 == BgL_oclassz00_6011);
															if (BgL__ortest_1115z00_6015)
																{	/* Cfa/setup.scm 241 */
																	BgL_res2277z00_6010 =
																		BgL__ortest_1115z00_6015;
																}
															else
																{	/* Cfa/setup.scm 241 */
																	long BgL_odepthz00_6016;

																	{	/* Cfa/setup.scm 241 */
																		obj_t BgL_arg1804z00_6017;

																		BgL_arg1804z00_6017 = (BgL_oclassz00_6011);
																		BgL_odepthz00_6016 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_6017);
																	}
																	if ((3L < BgL_odepthz00_6016))
																		{	/* Cfa/setup.scm 241 */
																			obj_t BgL_arg1802z00_6018;

																			{	/* Cfa/setup.scm 241 */
																				obj_t BgL_arg1803z00_6019;

																				BgL_arg1803z00_6019 =
																					(BgL_oclassz00_6011);
																				BgL_arg1802z00_6018 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_6019, 3L);
																			}
																			BgL_res2277z00_6010 =
																				(BgL_arg1802z00_6018 ==
																				BgL_classz00_6005);
																		}
																	else
																		{	/* Cfa/setup.scm 241 */
																			BgL_res2277z00_6010 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2428z00_7676 = BgL_res2277z00_6010;
												}
										}
									else
										{	/* Cfa/setup.scm 241 */
											BgL_test2428z00_7676 = ((bool_t) 0);
										}
								}
								if (BgL_test2428z00_7676)
									{	/* Cfa/setup.scm 241 */
										BgL_test2422z00_7652 = ((bool_t) 0);
									}
								else
									{	/* Cfa/setup.scm 241 */
										BgL_test2422z00_7652 = ((bool_t) 1);
									}
							}
						else
							{	/* Cfa/setup.scm 241 */
								BgL_test2422z00_7652 = ((bool_t) 0);
							}
					}
					if (BgL_test2422z00_7652)
						{	/* Cfa/setup.scm 241 */
							{	/* Cfa/setup.scm 242 */
								BgL_reshapedzd2globalzd2_bglt BgL_wide1240z00_6020;

								BgL_wide1240z00_6020 =
									((BgL_reshapedzd2globalzd2_bglt)
									BOBJECT(GC_MALLOC(sizeof(struct
												BgL_reshapedzd2globalzd2_bgl))));
								{	/* Cfa/setup.scm 242 */
									obj_t BgL_auxz00_7704;
									BgL_objectz00_bglt BgL_tmpz00_7700;

									BgL_auxz00_7704 = ((obj_t) BgL_wide1240z00_6020);
									BgL_tmpz00_7700 =
										((BgL_objectz00_bglt)
										((BgL_globalz00_bglt)
											((BgL_globalz00_bglt) BgL_varz00_5761)));
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7700, BgL_auxz00_7704);
								}
								((BgL_objectz00_bglt)
									((BgL_globalz00_bglt)
										((BgL_globalz00_bglt) BgL_varz00_5761)));
								{	/* Cfa/setup.scm 242 */
									long BgL_arg1949z00_6021;

									BgL_arg1949z00_6021 =
										BGL_CLASS_NUM(BGl_reshapedzd2globalzd2zzcfa_infoz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt)
											((BgL_globalz00_bglt)
												((BgL_globalz00_bglt) BgL_varz00_5761))),
										BgL_arg1949z00_6021);
								}
								((BgL_globalz00_bglt)
									((BgL_globalz00_bglt)
										((BgL_globalz00_bglt) BgL_varz00_5761)));
							}
							((obj_t)
								((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_varz00_5761)));
						}
					else
						{	/* Cfa/setup.scm 241 */
							BFALSE;
						}
				}
				{	/* Cfa/setup.scm 243 */
					bool_t BgL_test2433z00_7721;

					{	/* Cfa/setup.scm 243 */
						bool_t BgL_test2434z00_7722;

						{	/* Cfa/setup.scm 243 */
							obj_t BgL_classz00_6022;

							BgL_classz00_6022 = BGl_globalz00zzast_varz00;
							if (BGL_OBJECTP(BgL_varz00_5761))
								{	/* Cfa/setup.scm 243 */
									BgL_objectz00_bglt BgL_arg1807z00_6023;

									BgL_arg1807z00_6023 = (BgL_objectz00_bglt) (BgL_varz00_5761);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Cfa/setup.scm 243 */
											long BgL_idxz00_6024;

											BgL_idxz00_6024 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6023);
											BgL_test2434z00_7722 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_6024 + 2L)) == BgL_classz00_6022);
										}
									else
										{	/* Cfa/setup.scm 243 */
											bool_t BgL_res2278z00_6027;

											{	/* Cfa/setup.scm 243 */
												obj_t BgL_oclassz00_6028;

												{	/* Cfa/setup.scm 243 */
													obj_t BgL_arg1815z00_6029;
													long BgL_arg1816z00_6030;

													BgL_arg1815z00_6029 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Cfa/setup.scm 243 */
														long BgL_arg1817z00_6031;

														BgL_arg1817z00_6031 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6023);
														BgL_arg1816z00_6030 =
															(BgL_arg1817z00_6031 - OBJECT_TYPE);
													}
													BgL_oclassz00_6028 =
														VECTOR_REF(BgL_arg1815z00_6029,
														BgL_arg1816z00_6030);
												}
												{	/* Cfa/setup.scm 243 */
													bool_t BgL__ortest_1115z00_6032;

													BgL__ortest_1115z00_6032 =
														(BgL_classz00_6022 == BgL_oclassz00_6028);
													if (BgL__ortest_1115z00_6032)
														{	/* Cfa/setup.scm 243 */
															BgL_res2278z00_6027 = BgL__ortest_1115z00_6032;
														}
													else
														{	/* Cfa/setup.scm 243 */
															long BgL_odepthz00_6033;

															{	/* Cfa/setup.scm 243 */
																obj_t BgL_arg1804z00_6034;

																BgL_arg1804z00_6034 = (BgL_oclassz00_6028);
																BgL_odepthz00_6033 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_6034);
															}
															if ((2L < BgL_odepthz00_6033))
																{	/* Cfa/setup.scm 243 */
																	obj_t BgL_arg1802z00_6035;

																	{	/* Cfa/setup.scm 243 */
																		obj_t BgL_arg1803z00_6036;

																		BgL_arg1803z00_6036 = (BgL_oclassz00_6028);
																		BgL_arg1802z00_6035 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_6036, 2L);
																	}
																	BgL_res2278z00_6027 =
																		(BgL_arg1802z00_6035 == BgL_classz00_6022);
																}
															else
																{	/* Cfa/setup.scm 243 */
																	BgL_res2278z00_6027 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2434z00_7722 = BgL_res2278z00_6027;
										}
								}
							else
								{	/* Cfa/setup.scm 243 */
									BgL_test2434z00_7722 = ((bool_t) 0);
								}
						}
						if (BgL_test2434z00_7722)
							{	/* Cfa/setup.scm 243 */
								BgL_test2433z00_7721 =
									(
									(((BgL_globalz00_bglt) COBJECT(
												((BgL_globalz00_bglt) BgL_varz00_5761)))->
										BgL_importz00) == CNST_TABLE_REF(3));
							}
						else
							{	/* Cfa/setup.scm 243 */
								BgL_test2433z00_7721 = ((bool_t) 0);
							}
					}
					if (BgL_test2433z00_7721)
						{	/* Cfa/setup.scm 244 */
							BgL_approxz00_bglt BgL_approxz00_6037;

							{	/* Cfa/setup.scm 244 */
								BgL_typez00_bglt BgL_arg1954z00_6038;

								BgL_arg1954z00_6038 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_varz00_5761))))->BgL_typez00);
								BgL_approxz00_6037 =
									BGl_makezd2typezd2approxz00zzcfa_approxz00
									(BgL_arg1954z00_6038);
							}
							BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_approxz00_6037);
							{	/* Cfa/setup.scm 246 */
								BgL_externzd2sfunzf2cinfoz20_bglt BgL_wide1245z00_6039;

								BgL_wide1245z00_6039 =
									((BgL_externzd2sfunzf2cinfoz20_bglt)
									BOBJECT(GC_MALLOC(sizeof(struct
												BgL_externzd2sfunzf2cinfoz20_bgl))));
								{	/* Cfa/setup.scm 246 */
									obj_t BgL_auxz00_7759;
									BgL_objectz00_bglt BgL_tmpz00_7755;

									BgL_auxz00_7759 = ((obj_t) BgL_wide1245z00_6039);
									BgL_tmpz00_7755 =
										((BgL_objectz00_bglt)
										((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_5760)));
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7755, BgL_auxz00_7759);
								}
								((BgL_objectz00_bglt)
									((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_5760)));
								{	/* Cfa/setup.scm 246 */
									long BgL_arg1953z00_6040;

									BgL_arg1953z00_6040 =
										BGL_CLASS_NUM(BGl_externzd2sfunzf2Cinfoz20zzcfa_infoz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt)
											((BgL_sfunz00_bglt)
												((BgL_sfunz00_bglt) BgL_funz00_5760))),
										BgL_arg1953z00_6040);
								}
								((BgL_sfunz00_bglt)
									((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_5760)));
							}
							{
								BgL_externzd2sfunzf2cinfoz20_bglt BgL_auxz00_7773;

								{
									obj_t BgL_auxz00_7774;

									{	/* Cfa/setup.scm 247 */
										BgL_objectz00_bglt BgL_tmpz00_7775;

										BgL_tmpz00_7775 =
											((BgL_objectz00_bglt)
											((BgL_sfunz00_bglt)
												((BgL_sfunz00_bglt) BgL_funz00_5760)));
										BgL_auxz00_7774 = BGL_OBJECT_WIDENING(BgL_tmpz00_7775);
									}
									BgL_auxz00_7773 =
										((BgL_externzd2sfunzf2cinfoz20_bglt) BgL_auxz00_7774);
								}
								((((BgL_externzd2sfunzf2cinfoz20_bglt)
											COBJECT(BgL_auxz00_7773))->BgL_polymorphiczf3zf3) =
									((bool_t) ((bool_t) 0)), BUNSPEC);
							}
							{
								BgL_externzd2sfunzf2cinfoz20_bglt BgL_auxz00_7782;

								{
									obj_t BgL_auxz00_7783;

									{	/* Cfa/setup.scm 247 */
										BgL_objectz00_bglt BgL_tmpz00_7784;

										BgL_tmpz00_7784 =
											((BgL_objectz00_bglt)
											((BgL_sfunz00_bglt)
												((BgL_sfunz00_bglt) BgL_funz00_5760)));
										BgL_auxz00_7783 = BGL_OBJECT_WIDENING(BgL_tmpz00_7784);
									}
									BgL_auxz00_7782 =
										((BgL_externzd2sfunzf2cinfoz20_bglt) BgL_auxz00_7783);
								}
								((((BgL_externzd2sfunzf2cinfoz20_bglt)
											COBJECT(BgL_auxz00_7782))->BgL_approxz00) =
									((BgL_approxz00_bglt) BgL_approxz00_6037), BUNSPEC);
							}
							BgL_auxz00_7651 =
								((BgL_sfunz00_bglt)
								((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_5760)));
						}
					else
						{	/* Cfa/setup.scm 248 */
							BgL_approxz00_bglt BgL_approxz00_6041;

							{	/* Cfa/setup.scm 248 */
								BgL_typez00_bglt BgL_arg1956z00_6042;

								BgL_arg1956z00_6042 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_varz00_5761)))->BgL_typez00);
								BgL_approxz00_6041 =
									BGl_makezd2typezd2approxz00zzcfa_approxz00
									(BgL_arg1956z00_6042);
							}
							if (CBOOL
								(BGl_za2optimzd2cfazd2forcezd2loosezd2localzd2functionzf3za2z21zzengine_paramz00))
								{	/* Cfa/setup.scm 249 */
									BGl_approxzd2setzd2topz12z12zzcfa_approxz00
										(BgL_approxz00_6041);
								}
							else
								{	/* Cfa/setup.scm 249 */
									BFALSE;
								}
							{	/* Cfa/setup.scm 251 */
								BgL_internzd2sfunzf2cinfoz20_bglt BgL_wide1249z00_6043;

								BgL_wide1249z00_6043 =
									((BgL_internzd2sfunzf2cinfoz20_bglt)
									BOBJECT(GC_MALLOC(sizeof(struct
												BgL_internzd2sfunzf2cinfoz20_bgl))));
								{	/* Cfa/setup.scm 251 */
									obj_t BgL_auxz00_7805;
									BgL_objectz00_bglt BgL_tmpz00_7801;

									BgL_auxz00_7805 = ((obj_t) BgL_wide1249z00_6043);
									BgL_tmpz00_7801 =
										((BgL_objectz00_bglt)
										((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_5760)));
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7801, BgL_auxz00_7805);
								}
								((BgL_objectz00_bglt)
									((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_5760)));
								{	/* Cfa/setup.scm 251 */
									long BgL_arg1955z00_6044;

									BgL_arg1955z00_6044 =
										BGL_CLASS_NUM(BGl_internzd2sfunzf2Cinfoz20zzcfa_infoz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt)
											((BgL_sfunz00_bglt)
												((BgL_sfunz00_bglt) BgL_funz00_5760))),
										BgL_arg1955z00_6044);
								}
								((BgL_sfunz00_bglt)
									((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_5760)));
							}
							{
								BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_7819;

								{
									obj_t BgL_auxz00_7820;

									{	/* Cfa/setup.scm 252 */
										BgL_objectz00_bglt BgL_tmpz00_7821;

										BgL_tmpz00_7821 =
											((BgL_objectz00_bglt)
											((BgL_sfunz00_bglt)
												((BgL_sfunz00_bglt) BgL_funz00_5760)));
										BgL_auxz00_7820 = BGL_OBJECT_WIDENING(BgL_tmpz00_7821);
									}
									BgL_auxz00_7819 =
										((BgL_internzd2sfunzf2cinfoz20_bglt) BgL_auxz00_7820);
								}
								((((BgL_internzd2sfunzf2cinfoz20_bglt)
											COBJECT(BgL_auxz00_7819))->BgL_polymorphiczf3zf3) =
									((bool_t) ((bool_t) 0)), BUNSPEC);
							}
							{
								BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_7828;

								{
									obj_t BgL_auxz00_7829;

									{	/* Cfa/setup.scm 252 */
										BgL_objectz00_bglt BgL_tmpz00_7830;

										BgL_tmpz00_7830 =
											((BgL_objectz00_bglt)
											((BgL_sfunz00_bglt)
												((BgL_sfunz00_bglt) BgL_funz00_5760)));
										BgL_auxz00_7829 = BGL_OBJECT_WIDENING(BgL_tmpz00_7830);
									}
									BgL_auxz00_7828 =
										((BgL_internzd2sfunzf2cinfoz20_bglt) BgL_auxz00_7829);
								}
								((((BgL_internzd2sfunzf2cinfoz20_bglt)
											COBJECT(BgL_auxz00_7828))->BgL_approxz00) =
									((BgL_approxz00_bglt) BgL_approxz00_6041), BUNSPEC);
							}
							{
								BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_7837;

								{
									obj_t BgL_auxz00_7838;

									{	/* Cfa/setup.scm 252 */
										BgL_objectz00_bglt BgL_tmpz00_7839;

										BgL_tmpz00_7839 =
											((BgL_objectz00_bglt)
											((BgL_sfunz00_bglt)
												((BgL_sfunz00_bglt) BgL_funz00_5760)));
										BgL_auxz00_7838 = BGL_OBJECT_WIDENING(BgL_tmpz00_7839);
									}
									BgL_auxz00_7837 =
										((BgL_internzd2sfunzf2cinfoz20_bglt) BgL_auxz00_7838);
								}
								((((BgL_internzd2sfunzf2cinfoz20_bglt)
											COBJECT(BgL_auxz00_7837))->BgL_stampz00) =
									((long) -1L), BUNSPEC);
							}
							BgL_auxz00_7651 =
								((BgL_sfunz00_bglt)
								((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_5760)));
				}}
				return ((obj_t) BgL_auxz00_7651);
			}
		}

	}



/* &node-setup!-app1788 */
	obj_t BGl_z62nodezd2setupz12zd2app1788z70zzcfa_setupz00(obj_t BgL_envz00_5762,
		obj_t BgL_nodez00_5763)
	{
		{	/* Cfa/setup.scm 222 */
			BGl_nodezd2setupza2z12z62zzcfa_setupz00(
				(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_5763)))->BgL_argsz00));
			{	/* Tools/trace.sch 53 */
				BgL_variablez00_bglt BgL_variablez00_6046;

				BgL_variablez00_6046 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_nodez00_5763)))->BgL_funz00)))->
					BgL_variablez00);
				{	/* Tools/trace.sch 53 */
					BgL_valuez00_bglt BgL_arg1944z00_6047;

					BgL_arg1944z00_6047 =
						(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_6046))->
						BgL_valuez00);
					return BGl_funzd2setupz12zc0zzcfa_setupz00(((BgL_funz00_bglt)
							BgL_arg1944z00_6047), ((obj_t) BgL_variablez00_6046));
				}
			}
		}

	}



/* &node-setup!-sync1786 */
	obj_t BGl_z62nodezd2setupz12zd2sync1786z70zzcfa_setupz00(obj_t
		BgL_envz00_5764, obj_t BgL_nodez00_5765)
	{
		{	/* Cfa/setup.scm 213 */
			BGl_nodezd2setupz12zc0zzcfa_setupz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_5765)))->BgL_mutexz00));
			BGl_nodezd2setupz12zc0zzcfa_setupz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_5765)))->BgL_prelockz00));
			return
				BGl_nodezd2setupz12zc0zzcfa_setupz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_5765)))->BgL_bodyz00));
		}

	}



/* &node-setup!-sequence1784 */
	obj_t BGl_z62nodezd2setupz12zd2sequence1784z70zzcfa_setupz00(obj_t
		BgL_envz00_5766, obj_t BgL_nodez00_5767)
	{
		{	/* Cfa/setup.scm 206 */
			return
				BGl_nodezd2setupza2z12z62zzcfa_setupz00(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_5767)))->BgL_nodesz00));
		}

	}



/* &node-setup!-closure1782 */
	obj_t BGl_z62nodezd2setupz12zd2closure1782z70zzcfa_setupz00(obj_t
		BgL_envz00_5768, obj_t BgL_nodez00_5769)
	{
		{	/* Cfa/setup.scm 200 */
			{	/* Cfa/setup.scm 201 */
				obj_t BgL_arg1938z00_6051;

				BgL_arg1938z00_6051 =
					BGl_shapez00zztools_shapez00(
					((obj_t) ((BgL_closurez00_bglt) BgL_nodez00_5769)));
				return
					BGl_internalzd2errorzd2zztools_errorz00
					(BGl_string2293z00zzcfa_setupz00, BGl_string2331z00zzcfa_setupz00,
					BgL_arg1938z00_6051);
			}
		}

	}



/* &variable-value-setup1780 */
	obj_t BGl_z62variablezd2valuezd2setup1780z62zzcfa_setupz00(obj_t
		BgL_envz00_5770, obj_t BgL_valuez00_5771, obj_t BgL_varz00_5772)
	{
		{	/* Cfa/setup.scm 191 */
			{	/* Cfa/setup.scm 192 */
				BgL_cvarzf2cinfozf2_bglt BgL_wide1229z00_6053;

				BgL_wide1229z00_6053 =
					((BgL_cvarzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_cvarzf2cinfozf2_bgl))));
				{	/* Cfa/setup.scm 192 */
					obj_t BgL_auxz00_7881;
					BgL_objectz00_bglt BgL_tmpz00_7877;

					BgL_auxz00_7881 = ((obj_t) BgL_wide1229z00_6053);
					BgL_tmpz00_7877 =
						((BgL_objectz00_bglt)
						((BgL_cvarz00_bglt) ((BgL_cvarz00_bglt) BgL_valuez00_5771)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7877, BgL_auxz00_7881);
				}
				((BgL_objectz00_bglt)
					((BgL_cvarz00_bglt) ((BgL_cvarz00_bglt) BgL_valuez00_5771)));
				{	/* Cfa/setup.scm 192 */
					long BgL_arg1932z00_6054;

					BgL_arg1932z00_6054 = BGL_CLASS_NUM(BGl_cvarzf2Cinfozf2zzcfa_infoz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_cvarz00_bglt)
								((BgL_cvarz00_bglt) BgL_valuez00_5771))), BgL_arg1932z00_6054);
				}
				((BgL_cvarz00_bglt)
					((BgL_cvarz00_bglt) ((BgL_cvarz00_bglt) BgL_valuez00_5771)));
			}
			{
				BgL_approxz00_bglt BgL_auxz00_7903;
				BgL_cvarzf2cinfozf2_bglt BgL_auxz00_7895;

				{	/* Cfa/setup.scm 193 */
					BgL_typez00_bglt BgL_arg1933z00_6055;

					BgL_arg1933z00_6055 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_varz00_5772)))->BgL_typez00);
					BgL_auxz00_7903 =
						BGl_makezd2typezd2approxz00zzcfa_approxz00(BgL_arg1933z00_6055);
				}
				{
					obj_t BgL_auxz00_7896;

					{	/* Cfa/setup.scm 193 */
						BgL_objectz00_bglt BgL_tmpz00_7897;

						BgL_tmpz00_7897 =
							((BgL_objectz00_bglt)
							((BgL_cvarz00_bglt) ((BgL_cvarz00_bglt) BgL_valuez00_5771)));
						BgL_auxz00_7896 = BGL_OBJECT_WIDENING(BgL_tmpz00_7897);
					}
					BgL_auxz00_7895 = ((BgL_cvarzf2cinfozf2_bglt) BgL_auxz00_7896);
				}
				((((BgL_cvarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7895))->
						BgL_approxz00) = ((BgL_approxz00_bglt) BgL_auxz00_7903), BUNSPEC);
			}
			((BgL_cvarz00_bglt) ((BgL_cvarz00_bglt) BgL_valuez00_5771));
			{	/* Cfa/setup.scm 194 */
				bool_t BgL_test2440z00_7910;

				{	/* Cfa/setup.scm 194 */
					BgL_typez00_bglt BgL_arg1937z00_6056;

					BgL_arg1937z00_6056 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_varz00_5772)))->BgL_typez00);
					BgL_test2440z00_7910 =
						BGl_alloczd2typezf3z21zzcfa_setupz00(BgL_arg1937z00_6056);
				}
				if (BgL_test2440z00_7910)
					{	/* Cfa/setup.scm 195 */
						BgL_approxz00_bglt BgL_arg1936z00_6057;

						{
							BgL_cvarzf2cinfozf2_bglt BgL_auxz00_7914;

							{
								obj_t BgL_auxz00_7915;

								{	/* Cfa/setup.scm 195 */
									BgL_objectz00_bglt BgL_tmpz00_7916;

									BgL_tmpz00_7916 =
										((BgL_objectz00_bglt)
										((BgL_cvarz00_bglt)
											((BgL_cvarz00_bglt) BgL_valuez00_5771)));
									BgL_auxz00_7915 = BGL_OBJECT_WIDENING(BgL_tmpz00_7916);
								}
								BgL_auxz00_7914 = ((BgL_cvarzf2cinfozf2_bglt) BgL_auxz00_7915);
							}
							BgL_arg1936z00_6057 =
								(((BgL_cvarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7914))->
								BgL_approxz00);
						}
						return
							BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_arg1936z00_6057);
					}
				else
					{	/* Cfa/setup.scm 194 */
						return BFALSE;
					}
			}
		}

	}



/* &variable-value-setup1778 */
	obj_t BGl_z62variablezd2valuezd2setup1778z62zzcfa_setupz00(obj_t
		BgL_envz00_5773, obj_t BgL_valuez00_5774, obj_t BgL_varz00_5775)
	{
		{	/* Cfa/setup.scm 169 */
			{	/* Tools/trace.sch 53 */
				bool_t BgL_test2441z00_7924;

				{	/* Tools/trace.sch 53 */
					obj_t BgL_classz00_6059;

					BgL_classz00_6059 = BGl_globalz00zzast_varz00;
					if (BGL_OBJECTP(BgL_varz00_5775))
						{	/* Tools/trace.sch 53 */
							BgL_objectz00_bglt BgL_arg1807z00_6060;

							BgL_arg1807z00_6060 = (BgL_objectz00_bglt) (BgL_varz00_5775);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Tools/trace.sch 53 */
									long BgL_idxz00_6061;

									BgL_idxz00_6061 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6060);
									BgL_test2441z00_7924 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_6061 + 2L)) == BgL_classz00_6059);
								}
							else
								{	/* Tools/trace.sch 53 */
									bool_t BgL_res2274z00_6064;

									{	/* Tools/trace.sch 53 */
										obj_t BgL_oclassz00_6065;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_arg1815z00_6066;
											long BgL_arg1816z00_6067;

											BgL_arg1815z00_6066 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Tools/trace.sch 53 */
												long BgL_arg1817z00_6068;

												BgL_arg1817z00_6068 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6060);
												BgL_arg1816z00_6067 =
													(BgL_arg1817z00_6068 - OBJECT_TYPE);
											}
											BgL_oclassz00_6065 =
												VECTOR_REF(BgL_arg1815z00_6066, BgL_arg1816z00_6067);
										}
										{	/* Tools/trace.sch 53 */
											bool_t BgL__ortest_1115z00_6069;

											BgL__ortest_1115z00_6069 =
												(BgL_classz00_6059 == BgL_oclassz00_6065);
											if (BgL__ortest_1115z00_6069)
												{	/* Tools/trace.sch 53 */
													BgL_res2274z00_6064 = BgL__ortest_1115z00_6069;
												}
											else
												{	/* Tools/trace.sch 53 */
													long BgL_odepthz00_6070;

													{	/* Tools/trace.sch 53 */
														obj_t BgL_arg1804z00_6071;

														BgL_arg1804z00_6071 = (BgL_oclassz00_6065);
														BgL_odepthz00_6070 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_6071);
													}
													if ((2L < BgL_odepthz00_6070))
														{	/* Tools/trace.sch 53 */
															obj_t BgL_arg1802z00_6072;

															{	/* Tools/trace.sch 53 */
																obj_t BgL_arg1803z00_6073;

																BgL_arg1803z00_6073 = (BgL_oclassz00_6065);
																BgL_arg1802z00_6072 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6073,
																	2L);
															}
															BgL_res2274z00_6064 =
																(BgL_arg1802z00_6072 == BgL_classz00_6059);
														}
													else
														{	/* Tools/trace.sch 53 */
															BgL_res2274z00_6064 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2441z00_7924 = BgL_res2274z00_6064;
								}
						}
					else
						{	/* Tools/trace.sch 53 */
							BgL_test2441z00_7924 = ((bool_t) 0);
						}
				}
				if (BgL_test2441z00_7924)
					{	/* Tools/trace.sch 53 */
						bool_t BgL_test2446z00_7947;

						{	/* Tools/trace.sch 53 */
							bool_t BgL_test2447z00_7948;

							{	/* Tools/trace.sch 53 */
								obj_t BgL_arg1929z00_6074;

								BgL_arg1929z00_6074 =
									(((BgL_globalz00_bglt) COBJECT(
											((BgL_globalz00_bglt) BgL_varz00_5775)))->BgL_modulez00);
								BgL_test2447z00_7948 =
									(BgL_arg1929z00_6074 ==
									BGl_za2moduleza2z00zzmodule_modulez00);
							}
							if (BgL_test2447z00_7948)
								{	/* Tools/trace.sch 53 */
									if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
												(((BgL_scnstz00_bglt) COBJECT(
															((BgL_scnstz00_bglt) BgL_valuez00_5774)))->
													BgL_classz00), CNST_TABLE_REF(4))))
										{	/* Tools/trace.sch 53 */
											obj_t BgL_arg1927z00_6075;

											BgL_arg1927z00_6075 =
												(((BgL_scnstz00_bglt) COBJECT(
														((BgL_scnstz00_bglt) BgL_valuez00_5774)))->
												BgL_nodez00);
											{	/* Tools/trace.sch 53 */
												obj_t BgL_classz00_6076;

												BgL_classz00_6076 =
													BGl_prezd2makezd2procedurezd2appzd2zzcfa_info2z00;
												if (BGL_OBJECTP(BgL_arg1927z00_6075))
													{	/* Tools/trace.sch 53 */
														BgL_objectz00_bglt BgL_arg1807z00_6077;

														BgL_arg1807z00_6077 =
															(BgL_objectz00_bglt) (BgL_arg1927z00_6075);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Tools/trace.sch 53 */
																long BgL_idxz00_6078;

																BgL_idxz00_6078 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_6077);
																BgL_test2446z00_7947 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_6078 + 4L)) ==
																	BgL_classz00_6076);
															}
														else
															{	/* Tools/trace.sch 53 */
																bool_t BgL_res2275z00_6081;

																{	/* Tools/trace.sch 53 */
																	obj_t BgL_oclassz00_6082;

																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_arg1815z00_6083;
																		long BgL_arg1816z00_6084;

																		BgL_arg1815z00_6083 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Tools/trace.sch 53 */
																			long BgL_arg1817z00_6085;

																			BgL_arg1817z00_6085 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_6077);
																			BgL_arg1816z00_6084 =
																				(BgL_arg1817z00_6085 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_6082 =
																			VECTOR_REF(BgL_arg1815z00_6083,
																			BgL_arg1816z00_6084);
																	}
																	{	/* Tools/trace.sch 53 */
																		bool_t BgL__ortest_1115z00_6086;

																		BgL__ortest_1115z00_6086 =
																			(BgL_classz00_6076 == BgL_oclassz00_6082);
																		if (BgL__ortest_1115z00_6086)
																			{	/* Tools/trace.sch 53 */
																				BgL_res2275z00_6081 =
																					BgL__ortest_1115z00_6086;
																			}
																		else
																			{	/* Tools/trace.sch 53 */
																				long BgL_odepthz00_6087;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1804z00_6088;

																					BgL_arg1804z00_6088 =
																						(BgL_oclassz00_6082);
																					BgL_odepthz00_6087 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_6088);
																				}
																				if ((4L < BgL_odepthz00_6087))
																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1802z00_6089;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1803z00_6090;

																							BgL_arg1803z00_6090 =
																								(BgL_oclassz00_6082);
																							BgL_arg1802z00_6089 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_6090, 4L);
																						}
																						BgL_res2275z00_6081 =
																							(BgL_arg1802z00_6089 ==
																							BgL_classz00_6076);
																					}
																				else
																					{	/* Tools/trace.sch 53 */
																						BgL_res2275z00_6081 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2446z00_7947 = BgL_res2275z00_6081;
															}
													}
												else
													{	/* Tools/trace.sch 53 */
														BgL_test2446z00_7947 = ((bool_t) 0);
													}
											}
										}
									else
										{	/* Tools/trace.sch 53 */
											BgL_test2446z00_7947 = ((bool_t) 0);
										}
								}
							else
								{	/* Tools/trace.sch 53 */
									BgL_test2446z00_7947 = ((bool_t) 0);
								}
						}
						if (BgL_test2446z00_7947)
							{	/* Tools/trace.sch 53 */
								obj_t BgL_nodez00_6091;

								BgL_nodez00_6091 =
									(((BgL_scnstz00_bglt) COBJECT(
											((BgL_scnstz00_bglt) BgL_valuez00_5774)))->BgL_nodez00);
								BGl_nodezd2setupz12zc0zzcfa_setupz00(
									((BgL_nodez00_bglt) BgL_nodez00_6091));
								{	/* Tools/trace.sch 53 */
									BgL_scnstzf2cinfozf2_bglt BgL_wide1217z00_6092;

									BgL_wide1217z00_6092 =
										((BgL_scnstzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_scnstzf2cinfozf2_bgl))));
									{	/* Tools/trace.sch 53 */
										obj_t BgL_auxz00_7991;
										BgL_objectz00_bglt BgL_tmpz00_7987;

										BgL_auxz00_7991 = ((obj_t) BgL_wide1217z00_6092);
										BgL_tmpz00_7987 =
											((BgL_objectz00_bglt)
											((BgL_scnstz00_bglt)
												((BgL_scnstz00_bglt) BgL_valuez00_5774)));
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7987, BgL_auxz00_7991);
									}
									((BgL_objectz00_bglt)
										((BgL_scnstz00_bglt)
											((BgL_scnstz00_bglt) BgL_valuez00_5774)));
									{	/* Tools/trace.sch 53 */
										long BgL_arg1923z00_6093;

										BgL_arg1923z00_6093 =
											BGL_CLASS_NUM(BGl_scnstzf2Cinfozf2zzcfa_infoz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt)
												((BgL_scnstz00_bglt)
													((BgL_scnstz00_bglt) BgL_valuez00_5774))),
											BgL_arg1923z00_6093);
									}
									((BgL_scnstz00_bglt)
										((BgL_scnstz00_bglt)
											((BgL_scnstz00_bglt) BgL_valuez00_5774)));
								}
								{
									BgL_approxz00_bglt BgL_auxz00_8013;
									BgL_scnstzf2cinfozf2_bglt BgL_auxz00_8005;

									{
										BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_8014;

										{
											obj_t BgL_auxz00_8015;

											{	/* Tools/trace.sch 53 */
												BgL_objectz00_bglt BgL_tmpz00_8016;

												BgL_tmpz00_8016 =
													((BgL_objectz00_bglt)
													((BgL_appz00_bglt) BgL_nodez00_6091));
												BgL_auxz00_8015 = BGL_OBJECT_WIDENING(BgL_tmpz00_8016);
											}
											BgL_auxz00_8014 =
												((BgL_makezd2procedurezd2appz00_bglt) BgL_auxz00_8015);
										}
										BgL_auxz00_8013 =
											(((BgL_makezd2procedurezd2appz00_bglt)
												COBJECT(BgL_auxz00_8014))->BgL_approxz00);
									}
									{
										obj_t BgL_auxz00_8006;

										{	/* Tools/trace.sch 53 */
											BgL_objectz00_bglt BgL_tmpz00_8007;

											BgL_tmpz00_8007 =
												((BgL_objectz00_bglt)
												((BgL_scnstz00_bglt)
													((BgL_scnstz00_bglt) BgL_valuez00_5774)));
											BgL_auxz00_8006 = BGL_OBJECT_WIDENING(BgL_tmpz00_8007);
										}
										BgL_auxz00_8005 =
											((BgL_scnstzf2cinfozf2_bglt) BgL_auxz00_8006);
									}
									((((BgL_scnstzf2cinfozf2_bglt) COBJECT(BgL_auxz00_8005))->
											BgL_approxz00) =
										((BgL_approxz00_bglt) BgL_auxz00_8013), BUNSPEC);
								}
								return
									((obj_t)
									((BgL_scnstz00_bglt)
										((BgL_scnstz00_bglt) BgL_valuez00_5774)));
							}
						else
							{	/* Tools/trace.sch 53 */
								BgL_scnstz00_bglt BgL_valuez00_6094;

								{	/* Tools/trace.sch 53 */
									BgL_scnstzf2cinfozf2_bglt BgL_wide1221z00_6095;

									BgL_wide1221z00_6095 =
										((BgL_scnstzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_scnstzf2cinfozf2_bgl))));
									{	/* Tools/trace.sch 53 */
										obj_t BgL_auxz00_8031;
										BgL_objectz00_bglt BgL_tmpz00_8027;

										BgL_auxz00_8031 = ((obj_t) BgL_wide1221z00_6095);
										BgL_tmpz00_8027 =
											((BgL_objectz00_bglt)
											((BgL_scnstz00_bglt)
												((BgL_scnstz00_bglt) BgL_valuez00_5774)));
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8027, BgL_auxz00_8031);
									}
									((BgL_objectz00_bglt)
										((BgL_scnstz00_bglt)
											((BgL_scnstz00_bglt) BgL_valuez00_5774)));
									{	/* Tools/trace.sch 53 */
										long BgL_arg1925z00_6096;

										BgL_arg1925z00_6096 =
											BGL_CLASS_NUM(BGl_scnstzf2Cinfozf2zzcfa_infoz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt)
												((BgL_scnstz00_bglt)
													((BgL_scnstz00_bglt) BgL_valuez00_5774))),
											BgL_arg1925z00_6096);
									}
									((BgL_scnstz00_bglt)
										((BgL_scnstz00_bglt)
											((BgL_scnstz00_bglt) BgL_valuez00_5774)));
								}
								{
									BgL_approxz00_bglt BgL_auxz00_8053;
									BgL_scnstzf2cinfozf2_bglt BgL_auxz00_8045;

									{	/* Tools/trace.sch 53 */
										BgL_typez00_bglt BgL_arg1926z00_6097;

										BgL_arg1926z00_6097 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_varz00_5775)))->
											BgL_typez00);
										BgL_auxz00_8053 =
											BGl_makezd2typezd2approxz00zzcfa_approxz00
											(BgL_arg1926z00_6097);
									}
									{
										obj_t BgL_auxz00_8046;

										{	/* Tools/trace.sch 53 */
											BgL_objectz00_bglt BgL_tmpz00_8047;

											BgL_tmpz00_8047 =
												((BgL_objectz00_bglt)
												((BgL_scnstz00_bglt)
													((BgL_scnstz00_bglt) BgL_valuez00_5774)));
											BgL_auxz00_8046 = BGL_OBJECT_WIDENING(BgL_tmpz00_8047);
										}
										BgL_auxz00_8045 =
											((BgL_scnstzf2cinfozf2_bglt) BgL_auxz00_8046);
									}
									((((BgL_scnstzf2cinfozf2_bglt) COBJECT(BgL_auxz00_8045))->
											BgL_approxz00) =
										((BgL_approxz00_bglt) BgL_auxz00_8053), BUNSPEC);
								}
								BgL_valuez00_6094 =
									((BgL_scnstz00_bglt) ((BgL_scnstz00_bglt) BgL_valuez00_5774));
								{	/* Tools/trace.sch 53 */
									BgL_approxz00_bglt BgL_arg1924z00_6098;

									{
										BgL_scnstzf2cinfozf2_bglt BgL_auxz00_8060;

										{
											obj_t BgL_auxz00_8061;

											{	/* Tools/trace.sch 53 */
												BgL_objectz00_bglt BgL_tmpz00_8062;

												BgL_tmpz00_8062 =
													((BgL_objectz00_bglt) BgL_valuez00_6094);
												BgL_auxz00_8061 = BGL_OBJECT_WIDENING(BgL_tmpz00_8062);
											}
											BgL_auxz00_8060 =
												((BgL_scnstzf2cinfozf2_bglt) BgL_auxz00_8061);
										}
										BgL_arg1924z00_6098 =
											(((BgL_scnstzf2cinfozf2_bglt) COBJECT(BgL_auxz00_8060))->
											BgL_approxz00);
									}
									return
										BGl_approxzd2setzd2topz12z12zzcfa_approxz00
										(BgL_arg1924z00_6098);
								}
							}
					}
				else
					{	/* Tools/trace.sch 53 */
						{	/* Tools/trace.sch 53 */
							BgL_scnstzf2cinfozf2_bglt BgL_wide1225z00_6099;

							BgL_wide1225z00_6099 =
								((BgL_scnstzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_scnstzf2cinfozf2_bgl))));
							{	/* Tools/trace.sch 53 */
								obj_t BgL_auxz00_8073;
								BgL_objectz00_bglt BgL_tmpz00_8069;

								BgL_auxz00_8073 = ((obj_t) BgL_wide1225z00_6099);
								BgL_tmpz00_8069 =
									((BgL_objectz00_bglt)
									((BgL_scnstz00_bglt)
										((BgL_scnstz00_bglt) BgL_valuez00_5774)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8069, BgL_auxz00_8073);
							}
							((BgL_objectz00_bglt)
								((BgL_scnstz00_bglt) ((BgL_scnstz00_bglt) BgL_valuez00_5774)));
							{	/* Tools/trace.sch 53 */
								long BgL_arg1930z00_6100;

								BgL_arg1930z00_6100 =
									BGL_CLASS_NUM(BGl_scnstzf2Cinfozf2zzcfa_infoz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_scnstz00_bglt)
											((BgL_scnstz00_bglt) BgL_valuez00_5774))),
									BgL_arg1930z00_6100);
							}
							((BgL_scnstz00_bglt)
								((BgL_scnstz00_bglt) ((BgL_scnstz00_bglt) BgL_valuez00_5774)));
						}
						{
							BgL_approxz00_bglt BgL_auxz00_8095;
							BgL_scnstzf2cinfozf2_bglt BgL_auxz00_8087;

							{	/* Tools/trace.sch 53 */
								BgL_typez00_bglt BgL_arg1931z00_6101;

								BgL_arg1931z00_6101 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_varz00_5775)))->BgL_typez00);
								BgL_auxz00_8095 =
									BGl_makezd2typezd2approxz00zzcfa_approxz00
									(BgL_arg1931z00_6101);
							}
							{
								obj_t BgL_auxz00_8088;

								{	/* Tools/trace.sch 53 */
									BgL_objectz00_bglt BgL_tmpz00_8089;

									BgL_tmpz00_8089 =
										((BgL_objectz00_bglt)
										((BgL_scnstz00_bglt)
											((BgL_scnstz00_bglt) BgL_valuez00_5774)));
									BgL_auxz00_8088 = BGL_OBJECT_WIDENING(BgL_tmpz00_8089);
								}
								BgL_auxz00_8087 = ((BgL_scnstzf2cinfozf2_bglt) BgL_auxz00_8088);
							}
							((((BgL_scnstzf2cinfozf2_bglt) COBJECT(BgL_auxz00_8087))->
									BgL_approxz00) =
								((BgL_approxz00_bglt) BgL_auxz00_8095), BUNSPEC);
						}
						return
							((obj_t)
							((BgL_scnstz00_bglt) ((BgL_scnstz00_bglt) BgL_valuez00_5774)));
					}
			}
		}

	}



/* &variable-value-setup1776 */
	obj_t BGl_z62variablezd2valuezd2setup1776z62zzcfa_setupz00(obj_t
		BgL_envz00_5776, obj_t BgL_valuez00_5777, obj_t BgL_varz00_5778)
	{
		{	/* Cfa/setup.scm 163 */
			return CNST_TABLE_REF(5);
		}

	}



/* &variable-value-setup1774 */
	obj_t BGl_z62variablezd2valuezd2setup1774z62zzcfa_setupz00(obj_t
		BgL_envz00_5779, obj_t BgL_valuez00_5780, obj_t BgL_varz00_5781)
	{
		{	/* Cfa/setup.scm 156 */
			{
				BgL_sexitz00_bglt BgL_auxz00_8104;

				{	/* Cfa/setup.scm 157 */
					BgL_sexitzf2cinfozf2_bglt BgL_wide1213z00_6104;

					BgL_wide1213z00_6104 =
						((BgL_sexitzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_sexitzf2cinfozf2_bgl))));
					{	/* Cfa/setup.scm 157 */
						obj_t BgL_auxz00_8110;
						BgL_objectz00_bglt BgL_tmpz00_8106;

						BgL_auxz00_8110 = ((obj_t) BgL_wide1213z00_6104);
						BgL_tmpz00_8106 =
							((BgL_objectz00_bglt)
							((BgL_sexitz00_bglt) ((BgL_sexitz00_bglt) BgL_valuez00_5780)));
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8106, BgL_auxz00_8110);
					}
					((BgL_objectz00_bglt)
						((BgL_sexitz00_bglt) ((BgL_sexitz00_bglt) BgL_valuez00_5780)));
					{	/* Cfa/setup.scm 157 */
						long BgL_arg1910z00_6105;

						BgL_arg1910z00_6105 =
							BGL_CLASS_NUM(BGl_sexitzf2Cinfozf2zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt)
								((BgL_sexitz00_bglt)
									((BgL_sexitz00_bglt) BgL_valuez00_5780))),
							BgL_arg1910z00_6105);
					}
					((BgL_sexitz00_bglt)
						((BgL_sexitz00_bglt) ((BgL_sexitz00_bglt) BgL_valuez00_5780)));
				}
				{
					BgL_approxz00_bglt BgL_auxz00_8132;
					BgL_sexitzf2cinfozf2_bglt BgL_auxz00_8124;

					{	/* Cfa/setup.scm 158 */
						BgL_typez00_bglt BgL_arg1911z00_6106;

						BgL_arg1911z00_6106 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_varz00_5781)))->BgL_typez00);
						BgL_auxz00_8132 =
							BGl_makezd2typezd2approxz00zzcfa_approxz00(BgL_arg1911z00_6106);
					}
					{
						obj_t BgL_auxz00_8125;

						{	/* Cfa/setup.scm 158 */
							BgL_objectz00_bglt BgL_tmpz00_8126;

							BgL_tmpz00_8126 =
								((BgL_objectz00_bglt)
								((BgL_sexitz00_bglt) ((BgL_sexitz00_bglt) BgL_valuez00_5780)));
							BgL_auxz00_8125 = BGL_OBJECT_WIDENING(BgL_tmpz00_8126);
						}
						BgL_auxz00_8124 = ((BgL_sexitzf2cinfozf2_bglt) BgL_auxz00_8125);
					}
					((((BgL_sexitzf2cinfozf2_bglt) COBJECT(BgL_auxz00_8124))->
							BgL_approxz00) = ((BgL_approxz00_bglt) BgL_auxz00_8132), BUNSPEC);
				}
				BgL_auxz00_8104 =
					((BgL_sexitz00_bglt) ((BgL_sexitz00_bglt) BgL_valuez00_5780));
				return ((obj_t) BgL_auxz00_8104);
			}
		}

	}



/* &variable-value-setup1772 */
	obj_t BGl_z62variablezd2valuezd2setup1772z62zzcfa_setupz00(obj_t
		BgL_envz00_5782, obj_t BgL_valuez00_5783, obj_t BgL_varz00_5784)
	{
		{	/* Cfa/setup.scm 148 */
			{

				{	/* Cfa/setup.scm 148 */
					obj_t BgL_nextzd2method1771zd2_6109;

					BgL_nextzd2method1771zd2_6109 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_svarz00_bglt) BgL_valuez00_5783)),
						BGl_variablezd2valuezd2setupz12zd2envzc0zzcfa_setupz00,
						BGl_prezd2clozd2envz00zzcfa_infoz00);
					BGL_PROCEDURE_CALL2(BgL_nextzd2method1771zd2_6109,
						((obj_t) ((BgL_svarz00_bglt) BgL_valuez00_5783)), BgL_varz00_5784);
				}
				{	/* Cfa/setup.scm 151 */
					BgL_valuez00_bglt BgL_arg1906z00_6110;

					BgL_arg1906z00_6110 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_varz00_5784))))->BgL_valuez00);
					{
						BgL_svarzf2cinfozf2_bglt BgL_auxz00_8153;

						{
							obj_t BgL_auxz00_8154;

							{	/* Cfa/setup.scm 151 */
								BgL_objectz00_bglt BgL_tmpz00_8155;

								BgL_tmpz00_8155 =
									((BgL_objectz00_bglt)
									((BgL_svarz00_bglt) BgL_arg1906z00_6110));
								BgL_auxz00_8154 = BGL_OBJECT_WIDENING(BgL_tmpz00_8155);
							}
							BgL_auxz00_8153 = ((BgL_svarzf2cinfozf2_bglt) BgL_auxz00_8154);
						}
						return
							((((BgL_svarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_8153))->
								BgL_clozd2envzf3z21) = ((bool_t) ((bool_t) 1)), BUNSPEC);
					}
				}
			}
		}

	}



/* &variable-value-setup1770 */
	obj_t BGl_z62variablezd2valuezd2setup1770z62zzcfa_setupz00(obj_t
		BgL_envz00_5785, obj_t BgL_valuez00_5786, obj_t BgL_varz00_5787)
	{
		{	/* Cfa/setup.scm 135 */
			{	/* Cfa/setup.scm 136 */
				BgL_typez00_bglt BgL_typz00_6113;

				BgL_typz00_6113 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_varz00_5787)))->BgL_typez00);
				{	/* Cfa/setup.scm 137 */
					bool_t BgL_test2453z00_8163;

					{	/* Cfa/setup.scm 137 */
						obj_t BgL_classz00_6114;

						BgL_classz00_6114 = BGl_globalz00zzast_varz00;
						{	/* Cfa/setup.scm 137 */
							BgL_objectz00_bglt BgL_arg1807z00_6115;

							{	/* Cfa/setup.scm 137 */
								obj_t BgL_tmpz00_8164;

								BgL_tmpz00_8164 =
									((obj_t) ((BgL_variablez00_bglt) BgL_varz00_5787));
								BgL_arg1807z00_6115 = (BgL_objectz00_bglt) (BgL_tmpz00_8164);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Cfa/setup.scm 137 */
									long BgL_idxz00_6116;

									BgL_idxz00_6116 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6115);
									BgL_test2453z00_8163 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_6116 + 2L)) == BgL_classz00_6114);
								}
							else
								{	/* Cfa/setup.scm 137 */
									bool_t BgL_res2273z00_6119;

									{	/* Cfa/setup.scm 137 */
										obj_t BgL_oclassz00_6120;

										{	/* Cfa/setup.scm 137 */
											obj_t BgL_arg1815z00_6121;
											long BgL_arg1816z00_6122;

											BgL_arg1815z00_6121 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Cfa/setup.scm 137 */
												long BgL_arg1817z00_6123;

												BgL_arg1817z00_6123 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6115);
												BgL_arg1816z00_6122 =
													(BgL_arg1817z00_6123 - OBJECT_TYPE);
											}
											BgL_oclassz00_6120 =
												VECTOR_REF(BgL_arg1815z00_6121, BgL_arg1816z00_6122);
										}
										{	/* Cfa/setup.scm 137 */
											bool_t BgL__ortest_1115z00_6124;

											BgL__ortest_1115z00_6124 =
												(BgL_classz00_6114 == BgL_oclassz00_6120);
											if (BgL__ortest_1115z00_6124)
												{	/* Cfa/setup.scm 137 */
													BgL_res2273z00_6119 = BgL__ortest_1115z00_6124;
												}
											else
												{	/* Cfa/setup.scm 137 */
													long BgL_odepthz00_6125;

													{	/* Cfa/setup.scm 137 */
														obj_t BgL_arg1804z00_6126;

														BgL_arg1804z00_6126 = (BgL_oclassz00_6120);
														BgL_odepthz00_6125 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_6126);
													}
													if ((2L < BgL_odepthz00_6125))
														{	/* Cfa/setup.scm 137 */
															obj_t BgL_arg1802z00_6127;

															{	/* Cfa/setup.scm 137 */
																obj_t BgL_arg1803z00_6128;

																BgL_arg1803z00_6128 = (BgL_oclassz00_6120);
																BgL_arg1802z00_6127 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6128,
																	2L);
															}
															BgL_res2273z00_6119 =
																(BgL_arg1802z00_6127 == BgL_classz00_6114);
														}
													else
														{	/* Cfa/setup.scm 137 */
															BgL_res2273z00_6119 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2453z00_8163 = BgL_res2273z00_6119;
								}
						}
					}
					if (BgL_test2453z00_8163)
						{	/* Cfa/setup.scm 138 */
							BgL_svarz00_bglt BgL_valuez00_6129;

							{	/* Cfa/setup.scm 138 */
								BgL_svarzf2cinfozf2_bglt BgL_wide1205z00_6130;

								BgL_wide1205z00_6130 =
									((BgL_svarzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_svarzf2cinfozf2_bgl))));
								{	/* Cfa/setup.scm 138 */
									obj_t BgL_auxz00_8192;
									BgL_objectz00_bglt BgL_tmpz00_8188;

									BgL_auxz00_8192 = ((obj_t) BgL_wide1205z00_6130);
									BgL_tmpz00_8188 =
										((BgL_objectz00_bglt)
										((BgL_svarz00_bglt)
											((BgL_svarz00_bglt) BgL_valuez00_5786)));
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8188, BgL_auxz00_8192);
								}
								((BgL_objectz00_bglt)
									((BgL_svarz00_bglt) ((BgL_svarz00_bglt) BgL_valuez00_5786)));
								{	/* Cfa/setup.scm 138 */
									long BgL_arg1903z00_6131;

									BgL_arg1903z00_6131 =
										BGL_CLASS_NUM(BGl_svarzf2Cinfozf2zzcfa_infoz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt)
											((BgL_svarz00_bglt)
												((BgL_svarz00_bglt) BgL_valuez00_5786))),
										BgL_arg1903z00_6131);
								}
								((BgL_svarz00_bglt)
									((BgL_svarz00_bglt) ((BgL_svarz00_bglt) BgL_valuez00_5786)));
							}
							{
								BgL_svarzf2cinfozf2_bglt BgL_auxz00_8206;

								{
									obj_t BgL_auxz00_8207;

									{	/* Cfa/setup.scm 139 */
										BgL_objectz00_bglt BgL_tmpz00_8208;

										BgL_tmpz00_8208 =
											((BgL_objectz00_bglt)
											((BgL_svarz00_bglt)
												((BgL_svarz00_bglt) BgL_valuez00_5786)));
										BgL_auxz00_8207 = BGL_OBJECT_WIDENING(BgL_tmpz00_8208);
									}
									BgL_auxz00_8206 =
										((BgL_svarzf2cinfozf2_bglt) BgL_auxz00_8207);
								}
								((((BgL_svarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_8206))->
										BgL_approxz00) =
									((BgL_approxz00_bglt)
										BGl_makezd2typezd2approxz00zzcfa_approxz00
										(BgL_typz00_6113)), BUNSPEC);
							}
							{
								BgL_svarzf2cinfozf2_bglt BgL_auxz00_8216;

								{
									obj_t BgL_auxz00_8217;

									{	/* Cfa/setup.scm 139 */
										BgL_objectz00_bglt BgL_tmpz00_8218;

										BgL_tmpz00_8218 =
											((BgL_objectz00_bglt)
											((BgL_svarz00_bglt)
												((BgL_svarz00_bglt) BgL_valuez00_5786)));
										BgL_auxz00_8217 = BGL_OBJECT_WIDENING(BgL_tmpz00_8218);
									}
									BgL_auxz00_8216 =
										((BgL_svarzf2cinfozf2_bglt) BgL_auxz00_8217);
								}
								((((BgL_svarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_8216))->
										BgL_clozd2envzf3z21) = ((bool_t) ((bool_t) 0)), BUNSPEC);
							}
							{
								BgL_svarzf2cinfozf2_bglt BgL_auxz00_8225;

								{
									obj_t BgL_auxz00_8226;

									{	/* Cfa/setup.scm 139 */
										BgL_objectz00_bglt BgL_tmpz00_8227;

										BgL_tmpz00_8227 =
											((BgL_objectz00_bglt)
											((BgL_svarz00_bglt)
												((BgL_svarz00_bglt) BgL_valuez00_5786)));
										BgL_auxz00_8226 = BGL_OBJECT_WIDENING(BgL_tmpz00_8227);
									}
									BgL_auxz00_8225 =
										((BgL_svarzf2cinfozf2_bglt) BgL_auxz00_8226);
								}
								((((BgL_svarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_8225))->
										BgL_stampz00) = ((long) -1L), BUNSPEC);
							}
							BgL_valuez00_6129 =
								((BgL_svarz00_bglt) ((BgL_svarz00_bglt) BgL_valuez00_5786));
							{	/* Cfa/setup.scm 140 */
								bool_t BgL_test2457z00_8236;

								if (
									((((BgL_globalz00_bglt) COBJECT(
													((BgL_globalz00_bglt)
														((BgL_variablez00_bglt) BgL_varz00_5787))))->
											BgL_importz00) == CNST_TABLE_REF(6)))
									{	/* Cfa/setup.scm 140 */
										BgL_test2457z00_8236 = ((bool_t) 0);
									}
								else
									{	/* Cfa/setup.scm 140 */
										BgL_test2457z00_8236 =
											BGl_alloczd2typezf3z21zzcfa_setupz00(BgL_typz00_6113);
									}
								if (BgL_test2457z00_8236)
									{	/* Cfa/setup.scm 141 */
										BgL_approxz00_bglt BgL_arg1901z00_6132;

										{
											BgL_svarzf2cinfozf2_bglt BgL_auxz00_8244;

											{
												obj_t BgL_auxz00_8245;

												{	/* Cfa/setup.scm 141 */
													BgL_objectz00_bglt BgL_tmpz00_8246;

													BgL_tmpz00_8246 =
														((BgL_objectz00_bglt) BgL_valuez00_6129);
													BgL_auxz00_8245 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_8246);
												}
												BgL_auxz00_8244 =
													((BgL_svarzf2cinfozf2_bglt) BgL_auxz00_8245);
											}
											BgL_arg1901z00_6132 =
												(((BgL_svarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_8244))->
												BgL_approxz00);
										}
										return
											BGl_approxzd2setzd2topz12z12zzcfa_approxz00
											(BgL_arg1901z00_6132);
									}
								else
									{	/* Cfa/setup.scm 140 */
										return BFALSE;
									}
							}
						}
					else
						{	/* Cfa/setup.scm 137 */
							{	/* Cfa/setup.scm 142 */
								BgL_svarzf2cinfozf2_bglt BgL_wide1209z00_6133;

								BgL_wide1209z00_6133 =
									((BgL_svarzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_svarzf2cinfozf2_bgl))));
								{	/* Cfa/setup.scm 142 */
									obj_t BgL_auxz00_8257;
									BgL_objectz00_bglt BgL_tmpz00_8253;

									BgL_auxz00_8257 = ((obj_t) BgL_wide1209z00_6133);
									BgL_tmpz00_8253 =
										((BgL_objectz00_bglt)
										((BgL_svarz00_bglt)
											((BgL_svarz00_bglt) BgL_valuez00_5786)));
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8253, BgL_auxz00_8257);
								}
								((BgL_objectz00_bglt)
									((BgL_svarz00_bglt) ((BgL_svarz00_bglt) BgL_valuez00_5786)));
								{	/* Cfa/setup.scm 142 */
									long BgL_arg1904z00_6134;

									BgL_arg1904z00_6134 =
										BGL_CLASS_NUM(BGl_svarzf2Cinfozf2zzcfa_infoz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt)
											((BgL_svarz00_bglt)
												((BgL_svarz00_bglt) BgL_valuez00_5786))),
										BgL_arg1904z00_6134);
								}
								((BgL_svarz00_bglt)
									((BgL_svarz00_bglt) ((BgL_svarz00_bglt) BgL_valuez00_5786)));
							}
							{
								BgL_svarzf2cinfozf2_bglt BgL_auxz00_8271;

								{
									obj_t BgL_auxz00_8272;

									{	/* Cfa/setup.scm 143 */
										BgL_objectz00_bglt BgL_tmpz00_8273;

										BgL_tmpz00_8273 =
											((BgL_objectz00_bglt)
											((BgL_svarz00_bglt)
												((BgL_svarz00_bglt) BgL_valuez00_5786)));
										BgL_auxz00_8272 = BGL_OBJECT_WIDENING(BgL_tmpz00_8273);
									}
									BgL_auxz00_8271 =
										((BgL_svarzf2cinfozf2_bglt) BgL_auxz00_8272);
								}
								((((BgL_svarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_8271))->
										BgL_approxz00) =
									((BgL_approxz00_bglt)
										BGl_makezd2typezd2approxz00zzcfa_approxz00
										(BgL_typz00_6113)), BUNSPEC);
							}
							{
								BgL_svarzf2cinfozf2_bglt BgL_auxz00_8281;

								{
									obj_t BgL_auxz00_8282;

									{	/* Cfa/setup.scm 143 */
										BgL_objectz00_bglt BgL_tmpz00_8283;

										BgL_tmpz00_8283 =
											((BgL_objectz00_bglt)
											((BgL_svarz00_bglt)
												((BgL_svarz00_bglt) BgL_valuez00_5786)));
										BgL_auxz00_8282 = BGL_OBJECT_WIDENING(BgL_tmpz00_8283);
									}
									BgL_auxz00_8281 =
										((BgL_svarzf2cinfozf2_bglt) BgL_auxz00_8282);
								}
								((((BgL_svarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_8281))->
										BgL_clozd2envzf3z21) = ((bool_t) ((bool_t) 0)), BUNSPEC);
							}
							{
								BgL_svarzf2cinfozf2_bglt BgL_auxz00_8290;

								{
									obj_t BgL_auxz00_8291;

									{	/* Cfa/setup.scm 143 */
										BgL_objectz00_bglt BgL_tmpz00_8292;

										BgL_tmpz00_8292 =
											((BgL_objectz00_bglt)
											((BgL_svarz00_bglt)
												((BgL_svarz00_bglt) BgL_valuez00_5786)));
										BgL_auxz00_8291 = BGL_OBJECT_WIDENING(BgL_tmpz00_8292);
									}
									BgL_auxz00_8290 =
										((BgL_svarzf2cinfozf2_bglt) BgL_auxz00_8291);
								}
								((((BgL_svarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_8290))->
										BgL_stampz00) = ((long) -1L), BUNSPEC);
							}
							return
								((obj_t)
								((BgL_svarz00_bglt) ((BgL_svarz00_bglt) BgL_valuez00_5786)));
						}
				}
			}
		}

	}



/* &variable-value-setup1768 */
	obj_t BGl_z62variablezd2valuezd2setup1768z62zzcfa_setupz00(obj_t
		BgL_envz00_5788, obj_t BgL_valuez00_5789, obj_t BgL_varz00_5790)
	{
		{	/* Cfa/setup.scm 127 */
			return BUNSPEC;
		}

	}



/* &node-setup!-var1764 */
	obj_t BGl_z62nodezd2setupz12zd2var1764z70zzcfa_setupz00(obj_t BgL_envz00_5791,
		obj_t BgL_nodez00_5792)
	{
		{	/* Cfa/setup.scm 95 */
			BGl_variablezd2valuezd2setupz12z12zzcfa_setupz00(
				(((BgL_variablez00_bglt) COBJECT(
							(((BgL_varz00_bglt) COBJECT(
										((BgL_varz00_bglt) BgL_nodez00_5792)))->BgL_variablez00)))->
					BgL_valuez00),
				(((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt) BgL_nodez00_5792)))->
					BgL_variablez00));
			{	/* Cfa/setup.scm 100 */
				bool_t BgL_test2459z00_8308;

				{	/* Cfa/setup.scm 100 */
					bool_t BgL_test2460z00_8309;

					{	/* Cfa/setup.scm 100 */
						BgL_variablez00_bglt BgL_arg1894z00_6137;

						BgL_arg1894z00_6137 =
							(((BgL_varz00_bglt) COBJECT(
									((BgL_varz00_bglt) BgL_nodez00_5792)))->BgL_variablez00);
						{	/* Cfa/setup.scm 100 */
							obj_t BgL_classz00_6138;

							BgL_classz00_6138 = BGl_localz00zzast_varz00;
							{	/* Cfa/setup.scm 100 */
								BgL_objectz00_bglt BgL_arg1807z00_6139;

								{	/* Cfa/setup.scm 100 */
									obj_t BgL_tmpz00_8312;

									BgL_tmpz00_8312 =
										((obj_t) ((BgL_objectz00_bglt) BgL_arg1894z00_6137));
									BgL_arg1807z00_6139 = (BgL_objectz00_bglt) (BgL_tmpz00_8312);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cfa/setup.scm 100 */
										long BgL_idxz00_6140;

										BgL_idxz00_6140 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6139);
										BgL_test2460z00_8309 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_6140 + 2L)) == BgL_classz00_6138);
									}
								else
									{	/* Cfa/setup.scm 100 */
										bool_t BgL_res2269z00_6143;

										{	/* Cfa/setup.scm 100 */
											obj_t BgL_oclassz00_6144;

											{	/* Cfa/setup.scm 100 */
												obj_t BgL_arg1815z00_6145;
												long BgL_arg1816z00_6146;

												BgL_arg1815z00_6145 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cfa/setup.scm 100 */
													long BgL_arg1817z00_6147;

													BgL_arg1817z00_6147 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6139);
													BgL_arg1816z00_6146 =
														(BgL_arg1817z00_6147 - OBJECT_TYPE);
												}
												BgL_oclassz00_6144 =
													VECTOR_REF(BgL_arg1815z00_6145, BgL_arg1816z00_6146);
											}
											{	/* Cfa/setup.scm 100 */
												bool_t BgL__ortest_1115z00_6148;

												BgL__ortest_1115z00_6148 =
													(BgL_classz00_6138 == BgL_oclassz00_6144);
												if (BgL__ortest_1115z00_6148)
													{	/* Cfa/setup.scm 100 */
														BgL_res2269z00_6143 = BgL__ortest_1115z00_6148;
													}
												else
													{	/* Cfa/setup.scm 100 */
														long BgL_odepthz00_6149;

														{	/* Cfa/setup.scm 100 */
															obj_t BgL_arg1804z00_6150;

															BgL_arg1804z00_6150 = (BgL_oclassz00_6144);
															BgL_odepthz00_6149 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_6150);
														}
														if ((2L < BgL_odepthz00_6149))
															{	/* Cfa/setup.scm 100 */
																obj_t BgL_arg1802z00_6151;

																{	/* Cfa/setup.scm 100 */
																	obj_t BgL_arg1803z00_6152;

																	BgL_arg1803z00_6152 = (BgL_oclassz00_6144);
																	BgL_arg1802z00_6151 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6152,
																		2L);
																}
																BgL_res2269z00_6143 =
																	(BgL_arg1802z00_6151 == BgL_classz00_6138);
															}
														else
															{	/* Cfa/setup.scm 100 */
																BgL_res2269z00_6143 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2460z00_8309 = BgL_res2269z00_6143;
									}
							}
						}
					}
					if (BgL_test2460z00_8309)
						{	/* Cfa/setup.scm 100 */
							bool_t BgL_test2464z00_8335;

							{	/* Cfa/setup.scm 100 */
								BgL_variablez00_bglt BgL_arg1893z00_6153;

								BgL_arg1893z00_6153 =
									(((BgL_varz00_bglt) COBJECT(
											((BgL_varz00_bglt) BgL_nodez00_5792)))->BgL_variablez00);
								{	/* Cfa/setup.scm 100 */
									obj_t BgL_classz00_6154;

									BgL_classz00_6154 = BGl_reshapedzd2localzd2zzcfa_infoz00;
									{	/* Cfa/setup.scm 100 */
										BgL_objectz00_bglt BgL_arg1807z00_6155;

										{	/* Cfa/setup.scm 100 */
											obj_t BgL_tmpz00_8338;

											BgL_tmpz00_8338 =
												((obj_t) ((BgL_objectz00_bglt) BgL_arg1893z00_6153));
											BgL_arg1807z00_6155 =
												(BgL_objectz00_bglt) (BgL_tmpz00_8338);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Cfa/setup.scm 100 */
												long BgL_idxz00_6156;

												BgL_idxz00_6156 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6155);
												BgL_test2464z00_8335 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_6156 + 3L)) == BgL_classz00_6154);
											}
										else
											{	/* Cfa/setup.scm 100 */
												bool_t BgL_res2270z00_6159;

												{	/* Cfa/setup.scm 100 */
													obj_t BgL_oclassz00_6160;

													{	/* Cfa/setup.scm 100 */
														obj_t BgL_arg1815z00_6161;
														long BgL_arg1816z00_6162;

														BgL_arg1815z00_6161 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Cfa/setup.scm 100 */
															long BgL_arg1817z00_6163;

															BgL_arg1817z00_6163 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6155);
															BgL_arg1816z00_6162 =
																(BgL_arg1817z00_6163 - OBJECT_TYPE);
														}
														BgL_oclassz00_6160 =
															VECTOR_REF(BgL_arg1815z00_6161,
															BgL_arg1816z00_6162);
													}
													{	/* Cfa/setup.scm 100 */
														bool_t BgL__ortest_1115z00_6164;

														BgL__ortest_1115z00_6164 =
															(BgL_classz00_6154 == BgL_oclassz00_6160);
														if (BgL__ortest_1115z00_6164)
															{	/* Cfa/setup.scm 100 */
																BgL_res2270z00_6159 = BgL__ortest_1115z00_6164;
															}
														else
															{	/* Cfa/setup.scm 100 */
																long BgL_odepthz00_6165;

																{	/* Cfa/setup.scm 100 */
																	obj_t BgL_arg1804z00_6166;

																	BgL_arg1804z00_6166 = (BgL_oclassz00_6160);
																	BgL_odepthz00_6165 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_6166);
																}
																if ((3L < BgL_odepthz00_6165))
																	{	/* Cfa/setup.scm 100 */
																		obj_t BgL_arg1802z00_6167;

																		{	/* Cfa/setup.scm 100 */
																			obj_t BgL_arg1803z00_6168;

																			BgL_arg1803z00_6168 =
																				(BgL_oclassz00_6160);
																			BgL_arg1802z00_6167 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_6168, 3L);
																		}
																		BgL_res2270z00_6159 =
																			(BgL_arg1802z00_6167 ==
																			BgL_classz00_6154);
																	}
																else
																	{	/* Cfa/setup.scm 100 */
																		BgL_res2270z00_6159 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2464z00_8335 = BgL_res2270z00_6159;
											}
									}
								}
							}
							if (BgL_test2464z00_8335)
								{	/* Cfa/setup.scm 100 */
									BgL_test2459z00_8308 = ((bool_t) 0);
								}
							else
								{	/* Cfa/setup.scm 100 */
									BgL_test2459z00_8308 = ((bool_t) 1);
								}
						}
					else
						{	/* Cfa/setup.scm 100 */
							BgL_test2459z00_8308 = ((bool_t) 0);
						}
				}
				if (BgL_test2459z00_8308)
					{	/* Cfa/setup.scm 101 */
						BgL_localz00_bglt BgL_tmp1195z00_6169;

						BgL_tmp1195z00_6169 =
							((BgL_localz00_bglt)
							(((BgL_varz00_bglt) COBJECT(
										((BgL_varz00_bglt) BgL_nodez00_5792)))->BgL_variablez00));
						{	/* Cfa/setup.scm 101 */
							BgL_reshapedzd2localzd2_bglt BgL_wide1197z00_6170;

							BgL_wide1197z00_6170 =
								((BgL_reshapedzd2localzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_reshapedzd2localzd2_bgl))));
							{	/* Cfa/setup.scm 101 */
								obj_t BgL_auxz00_8368;
								BgL_objectz00_bglt BgL_tmpz00_8365;

								BgL_auxz00_8368 = ((obj_t) BgL_wide1197z00_6170);
								BgL_tmpz00_8365 =
									((BgL_objectz00_bglt)
									((BgL_localz00_bglt) BgL_tmp1195z00_6169));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8365, BgL_auxz00_8368);
							}
							((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_tmp1195z00_6169));
							{	/* Cfa/setup.scm 101 */
								long BgL_arg1882z00_6171;

								BgL_arg1882z00_6171 =
									BGL_CLASS_NUM(BGl_reshapedzd2localzd2zzcfa_infoz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_localz00_bglt) BgL_tmp1195z00_6169)),
									BgL_arg1882z00_6171);
							}
							((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_tmp1195z00_6169));
						}
						{
							BgL_reshapedzd2localzd2_bglt BgL_auxz00_8379;

							{
								obj_t BgL_auxz00_8380;

								{	/* Cfa/setup.scm 101 */
									BgL_objectz00_bglt BgL_tmpz00_8381;

									BgL_tmpz00_8381 =
										((BgL_objectz00_bglt)
										((BgL_localz00_bglt) BgL_tmp1195z00_6169));
									BgL_auxz00_8380 = BGL_OBJECT_WIDENING(BgL_tmpz00_8381);
								}
								BgL_auxz00_8379 =
									((BgL_reshapedzd2localzd2_bglt) BgL_auxz00_8380);
							}
							((((BgL_reshapedzd2localzd2_bglt) COBJECT(BgL_auxz00_8379))->
									BgL_bindingzd2valuezd2) = ((obj_t) BFALSE), BUNSPEC);
						}
						return ((obj_t) ((BgL_localz00_bglt) BgL_tmp1195z00_6169));
					}
				else
					{	/* Cfa/setup.scm 102 */
						bool_t BgL_test2468z00_8389;

						{	/* Cfa/setup.scm 102 */
							bool_t BgL_test2469z00_8390;

							{	/* Cfa/setup.scm 102 */
								BgL_variablez00_bglt BgL_arg1892z00_6172;

								BgL_arg1892z00_6172 =
									(((BgL_varz00_bglt) COBJECT(
											((BgL_varz00_bglt) BgL_nodez00_5792)))->BgL_variablez00);
								{	/* Cfa/setup.scm 102 */
									obj_t BgL_classz00_6173;

									BgL_classz00_6173 = BGl_globalz00zzast_varz00;
									{	/* Cfa/setup.scm 102 */
										BgL_objectz00_bglt BgL_arg1807z00_6174;

										{	/* Cfa/setup.scm 102 */
											obj_t BgL_tmpz00_8393;

											BgL_tmpz00_8393 =
												((obj_t) ((BgL_objectz00_bglt) BgL_arg1892z00_6172));
											BgL_arg1807z00_6174 =
												(BgL_objectz00_bglt) (BgL_tmpz00_8393);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Cfa/setup.scm 102 */
												long BgL_idxz00_6175;

												BgL_idxz00_6175 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6174);
												BgL_test2469z00_8390 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_6175 + 2L)) == BgL_classz00_6173);
											}
										else
											{	/* Cfa/setup.scm 102 */
												bool_t BgL_res2271z00_6178;

												{	/* Cfa/setup.scm 102 */
													obj_t BgL_oclassz00_6179;

													{	/* Cfa/setup.scm 102 */
														obj_t BgL_arg1815z00_6180;
														long BgL_arg1816z00_6181;

														BgL_arg1815z00_6180 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Cfa/setup.scm 102 */
															long BgL_arg1817z00_6182;

															BgL_arg1817z00_6182 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6174);
															BgL_arg1816z00_6181 =
																(BgL_arg1817z00_6182 - OBJECT_TYPE);
														}
														BgL_oclassz00_6179 =
															VECTOR_REF(BgL_arg1815z00_6180,
															BgL_arg1816z00_6181);
													}
													{	/* Cfa/setup.scm 102 */
														bool_t BgL__ortest_1115z00_6183;

														BgL__ortest_1115z00_6183 =
															(BgL_classz00_6173 == BgL_oclassz00_6179);
														if (BgL__ortest_1115z00_6183)
															{	/* Cfa/setup.scm 102 */
																BgL_res2271z00_6178 = BgL__ortest_1115z00_6183;
															}
														else
															{	/* Cfa/setup.scm 102 */
																long BgL_odepthz00_6184;

																{	/* Cfa/setup.scm 102 */
																	obj_t BgL_arg1804z00_6185;

																	BgL_arg1804z00_6185 = (BgL_oclassz00_6179);
																	BgL_odepthz00_6184 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_6185);
																}
																if ((2L < BgL_odepthz00_6184))
																	{	/* Cfa/setup.scm 102 */
																		obj_t BgL_arg1802z00_6186;

																		{	/* Cfa/setup.scm 102 */
																			obj_t BgL_arg1803z00_6187;

																			BgL_arg1803z00_6187 =
																				(BgL_oclassz00_6179);
																			BgL_arg1802z00_6186 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_6187, 2L);
																		}
																		BgL_res2271z00_6178 =
																			(BgL_arg1802z00_6186 ==
																			BgL_classz00_6173);
																	}
																else
																	{	/* Cfa/setup.scm 102 */
																		BgL_res2271z00_6178 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2469z00_8390 = BgL_res2271z00_6178;
											}
									}
								}
							}
							if (BgL_test2469z00_8390)
								{	/* Cfa/setup.scm 102 */
									bool_t BgL_test2473z00_8416;

									{	/* Cfa/setup.scm 102 */
										BgL_variablez00_bglt BgL_arg1891z00_6188;

										BgL_arg1891z00_6188 =
											(((BgL_varz00_bglt) COBJECT(
													((BgL_varz00_bglt) BgL_nodez00_5792)))->
											BgL_variablez00);
										{	/* Cfa/setup.scm 102 */
											obj_t BgL_classz00_6189;

											BgL_classz00_6189 = BGl_reshapedzd2globalzd2zzcfa_infoz00;
											{	/* Cfa/setup.scm 102 */
												BgL_objectz00_bglt BgL_arg1807z00_6190;

												{	/* Cfa/setup.scm 102 */
													obj_t BgL_tmpz00_8419;

													BgL_tmpz00_8419 =
														((obj_t)
														((BgL_objectz00_bglt) BgL_arg1891z00_6188));
													BgL_arg1807z00_6190 =
														(BgL_objectz00_bglt) (BgL_tmpz00_8419);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Cfa/setup.scm 102 */
														long BgL_idxz00_6191;

														BgL_idxz00_6191 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6190);
														BgL_test2473z00_8416 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_6191 + 3L)) == BgL_classz00_6189);
													}
												else
													{	/* Cfa/setup.scm 102 */
														bool_t BgL_res2272z00_6194;

														{	/* Cfa/setup.scm 102 */
															obj_t BgL_oclassz00_6195;

															{	/* Cfa/setup.scm 102 */
																obj_t BgL_arg1815z00_6196;
																long BgL_arg1816z00_6197;

																BgL_arg1815z00_6196 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Cfa/setup.scm 102 */
																	long BgL_arg1817z00_6198;

																	BgL_arg1817z00_6198 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6190);
																	BgL_arg1816z00_6197 =
																		(BgL_arg1817z00_6198 - OBJECT_TYPE);
																}
																BgL_oclassz00_6195 =
																	VECTOR_REF(BgL_arg1815z00_6196,
																	BgL_arg1816z00_6197);
															}
															{	/* Cfa/setup.scm 102 */
																bool_t BgL__ortest_1115z00_6199;

																BgL__ortest_1115z00_6199 =
																	(BgL_classz00_6189 == BgL_oclassz00_6195);
																if (BgL__ortest_1115z00_6199)
																	{	/* Cfa/setup.scm 102 */
																		BgL_res2272z00_6194 =
																			BgL__ortest_1115z00_6199;
																	}
																else
																	{	/* Cfa/setup.scm 102 */
																		long BgL_odepthz00_6200;

																		{	/* Cfa/setup.scm 102 */
																			obj_t BgL_arg1804z00_6201;

																			BgL_arg1804z00_6201 =
																				(BgL_oclassz00_6195);
																			BgL_odepthz00_6200 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_6201);
																		}
																		if ((3L < BgL_odepthz00_6200))
																			{	/* Cfa/setup.scm 102 */
																				obj_t BgL_arg1802z00_6202;

																				{	/* Cfa/setup.scm 102 */
																					obj_t BgL_arg1803z00_6203;

																					BgL_arg1803z00_6203 =
																						(BgL_oclassz00_6195);
																					BgL_arg1802z00_6202 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_6203, 3L);
																				}
																				BgL_res2272z00_6194 =
																					(BgL_arg1802z00_6202 ==
																					BgL_classz00_6189);
																			}
																		else
																			{	/* Cfa/setup.scm 102 */
																				BgL_res2272z00_6194 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2473z00_8416 = BgL_res2272z00_6194;
													}
											}
										}
									}
									if (BgL_test2473z00_8416)
										{	/* Cfa/setup.scm 102 */
											BgL_test2468z00_8389 = ((bool_t) 0);
										}
									else
										{	/* Cfa/setup.scm 102 */
											BgL_test2468z00_8389 = ((bool_t) 1);
										}
								}
							else
								{	/* Cfa/setup.scm 102 */
									BgL_test2468z00_8389 = ((bool_t) 0);
								}
						}
						if (BgL_test2468z00_8389)
							{	/* Cfa/setup.scm 103 */
								BgL_globalz00_bglt BgL_tmp1199z00_6204;

								BgL_tmp1199z00_6204 =
									((BgL_globalz00_bglt)
									(((BgL_varz00_bglt) COBJECT(
												((BgL_varz00_bglt) BgL_nodez00_5792)))->
										BgL_variablez00));
								{	/* Cfa/setup.scm 103 */
									BgL_reshapedzd2globalzd2_bglt BgL_wide1201z00_6205;

									BgL_wide1201z00_6205 =
										((BgL_reshapedzd2globalzd2_bglt)
										BOBJECT(GC_MALLOC(sizeof(struct
													BgL_reshapedzd2globalzd2_bgl))));
									{	/* Cfa/setup.scm 103 */
										obj_t BgL_auxz00_8449;
										BgL_objectz00_bglt BgL_tmpz00_8446;

										BgL_auxz00_8449 = ((obj_t) BgL_wide1201z00_6205);
										BgL_tmpz00_8446 =
											((BgL_objectz00_bglt)
											((BgL_globalz00_bglt) BgL_tmp1199z00_6204));
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8446, BgL_auxz00_8449);
									}
									((BgL_objectz00_bglt)
										((BgL_globalz00_bglt) BgL_tmp1199z00_6204));
									{	/* Cfa/setup.scm 103 */
										long BgL_arg1890z00_6206;

										BgL_arg1890z00_6206 =
											BGL_CLASS_NUM(BGl_reshapedzd2globalzd2zzcfa_infoz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt)
												((BgL_globalz00_bglt) BgL_tmp1199z00_6204)),
											BgL_arg1890z00_6206);
									}
									((BgL_globalz00_bglt)
										((BgL_globalz00_bglt) BgL_tmp1199z00_6204));
								}
								return ((obj_t) ((BgL_globalz00_bglt) BgL_tmp1199z00_6204));
							}
						else
							{	/* Cfa/setup.scm 102 */
								return BFALSE;
							}
					}
			}
		}

	}



/* &node-setup!-kwote/no1762 */
	obj_t BGl_z62nodezd2setupz12zd2kwotezf2no1762z82zzcfa_setupz00(obj_t
		BgL_envz00_5793, obj_t BgL_kwotez00_5794)
	{
		{	/* Cfa/setup.scm 88 */
			{	/* Cfa/setup.scm 90 */
				BgL_nodez00_bglt BgL_arg1869z00_6208;

				{
					BgL_kwotezf2nodezf2_bglt BgL_auxz00_8462;

					{
						obj_t BgL_auxz00_8463;

						{	/* Cfa/setup.scm 90 */
							BgL_objectz00_bglt BgL_tmpz00_8464;

							BgL_tmpz00_8464 =
								((BgL_objectz00_bglt) ((BgL_kwotez00_bglt) BgL_kwotez00_5794));
							BgL_auxz00_8463 = BGL_OBJECT_WIDENING(BgL_tmpz00_8464);
						}
						BgL_auxz00_8462 = ((BgL_kwotezf2nodezf2_bglt) BgL_auxz00_8463);
					}
					BgL_arg1869z00_6208 =
						(((BgL_kwotezf2nodezf2_bglt) COBJECT(BgL_auxz00_8462))->
						BgL_nodez00);
				}
				return BGl_nodezd2setupz12zc0zzcfa_setupz00(BgL_arg1869z00_6208);
			}
		}

	}



/* &node-setup!-kwote1760 */
	obj_t BGl_z62nodezd2setupz12zd2kwote1760z70zzcfa_setupz00(obj_t
		BgL_envz00_5795, obj_t BgL_nodez00_5796)
	{
		{	/* Cfa/setup.scm 78 */
			{	/* Cfa/setup.scm 80 */
				BgL_approxz00_bglt BgL_approxz00_6210;

				BgL_approxz00_6210 =
					BGl_makezd2typezd2approxz00zzcfa_approxz00
					(BGl_getzd2typezd2kwotez00zztype_typeofz00((((BgL_kwotez00_bglt)
								COBJECT(((BgL_kwotez00_bglt) BgL_nodez00_5796)))->
							BgL_valuez00)));
				((((BgL_approxz00_bglt) COBJECT(BgL_approxz00_6210))->BgL_topzf3zf3) =
					((bool_t) ((bool_t) 1)), BUNSPEC);
				{	/* Cfa/setup.scm 82 */
					BgL_kwotezf2cinfozf2_bglt BgL_wide1191z00_6211;

					BgL_wide1191z00_6211 =
						((BgL_kwotezf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_kwotezf2cinfozf2_bgl))));
					{	/* Cfa/setup.scm 82 */
						obj_t BgL_auxz00_8481;
						BgL_objectz00_bglt BgL_tmpz00_8477;

						BgL_auxz00_8481 = ((obj_t) BgL_wide1191z00_6211);
						BgL_tmpz00_8477 =
							((BgL_objectz00_bglt)
							((BgL_kwotez00_bglt) ((BgL_kwotez00_bglt) BgL_nodez00_5796)));
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8477, BgL_auxz00_8481);
					}
					((BgL_objectz00_bglt)
						((BgL_kwotez00_bglt) ((BgL_kwotez00_bglt) BgL_nodez00_5796)));
					{	/* Cfa/setup.scm 82 */
						long BgL_arg1864z00_6212;

						BgL_arg1864z00_6212 =
							BGL_CLASS_NUM(BGl_kwotezf2Cinfozf2zzcfa_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt)
								((BgL_kwotez00_bglt)
									((BgL_kwotez00_bglt) BgL_nodez00_5796))),
							BgL_arg1864z00_6212);
					}
					((BgL_kwotez00_bglt)
						((BgL_kwotez00_bglt) ((BgL_kwotez00_bglt) BgL_nodez00_5796)));
				}
				{
					BgL_kwotezf2cinfozf2_bglt BgL_auxz00_8495;

					{
						obj_t BgL_auxz00_8496;

						{	/* Cfa/setup.scm 83 */
							BgL_objectz00_bglt BgL_tmpz00_8497;

							BgL_tmpz00_8497 =
								((BgL_objectz00_bglt)
								((BgL_kwotez00_bglt) ((BgL_kwotez00_bglt) BgL_nodez00_5796)));
							BgL_auxz00_8496 = BGL_OBJECT_WIDENING(BgL_tmpz00_8497);
						}
						BgL_auxz00_8495 = ((BgL_kwotezf2cinfozf2_bglt) BgL_auxz00_8496);
					}
					((((BgL_kwotezf2cinfozf2_bglt) COBJECT(BgL_auxz00_8495))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approxz00_6210), BUNSPEC);
				}
				return
					((obj_t)
					((BgL_kwotez00_bglt) ((BgL_kwotez00_bglt) BgL_nodez00_5796)));
			}
		}

	}



/* &node-setup!-patch1758 */
	obj_t BGl_z62nodezd2setupz12zd2patch1758z70zzcfa_setupz00(obj_t
		BgL_envz00_5797, obj_t BgL_nodez00_5798)
	{
		{	/* Cfa/setup.scm 69 */
			{	/* Cfa/setup.scm 71 */
				obj_t BgL_arg1860z00_6214;

				BgL_arg1860z00_6214 =
					(((BgL_atomz00_bglt) COBJECT(
							((BgL_atomz00_bglt)
								((BgL_patchz00_bglt) BgL_nodez00_5798))))->BgL_valuez00);
				BGl_nodezd2setupz12zc0zzcfa_setupz00(
					((BgL_nodez00_bglt) BgL_arg1860z00_6214));
			}
			{	/* Cfa/setup.scm 72 */
				BgL_patchzf2cinfozf2_bglt BgL_wide1186z00_6215;

				BgL_wide1186z00_6215 =
					((BgL_patchzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_patchzf2cinfozf2_bgl))));
				{	/* Cfa/setup.scm 72 */
					obj_t BgL_auxz00_8517;
					BgL_objectz00_bglt BgL_tmpz00_8513;

					BgL_auxz00_8517 = ((obj_t) BgL_wide1186z00_6215);
					BgL_tmpz00_8513 =
						((BgL_objectz00_bglt)
						((BgL_patchz00_bglt) ((BgL_patchz00_bglt) BgL_nodez00_5798)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8513, BgL_auxz00_8517);
				}
				((BgL_objectz00_bglt)
					((BgL_patchz00_bglt) ((BgL_patchz00_bglt) BgL_nodez00_5798)));
				{	/* Cfa/setup.scm 72 */
					long BgL_arg1862z00_6216;

					BgL_arg1862z00_6216 =
						BGL_CLASS_NUM(BGl_patchzf2Cinfozf2zzcfa_infoz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_patchz00_bglt)
								((BgL_patchz00_bglt) BgL_nodez00_5798))), BgL_arg1862z00_6216);
				}
				((BgL_patchz00_bglt)
					((BgL_patchz00_bglt) ((BgL_patchz00_bglt) BgL_nodez00_5798)));
			}
			{
				BgL_approxz00_bglt BgL_auxz00_8539;
				BgL_patchzf2cinfozf2_bglt BgL_auxz00_8531;

				{	/* Cfa/setup.scm 73 */
					BgL_typez00_bglt BgL_arg1863z00_6217;

					BgL_arg1863z00_6217 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_patchz00_bglt) BgL_nodez00_5798))))->BgL_typez00);
					BgL_auxz00_8539 =
						BGl_makezd2typezd2approxz00zzcfa_approxz00(BgL_arg1863z00_6217);
				}
				{
					obj_t BgL_auxz00_8532;

					{	/* Cfa/setup.scm 73 */
						BgL_objectz00_bglt BgL_tmpz00_8533;

						BgL_tmpz00_8533 =
							((BgL_objectz00_bglt)
							((BgL_patchz00_bglt) ((BgL_patchz00_bglt) BgL_nodez00_5798)));
						BgL_auxz00_8532 = BGL_OBJECT_WIDENING(BgL_tmpz00_8533);
					}
					BgL_auxz00_8531 = ((BgL_patchzf2cinfozf2_bglt) BgL_auxz00_8532);
				}
				((((BgL_patchzf2cinfozf2_bglt) COBJECT(BgL_auxz00_8531))->
						BgL_approxz00) = ((BgL_approxz00_bglt) BgL_auxz00_8539), BUNSPEC);
			}
			return
				((obj_t) ((BgL_patchz00_bglt) ((BgL_patchz00_bglt) BgL_nodez00_5798)));
		}

	}



/* &node-setup!-literal1756 */
	obj_t BGl_z62nodezd2setupz12zd2literal1756z70zzcfa_setupz00(obj_t
		BgL_envz00_5799, obj_t BgL_nodez00_5800)
	{
		{	/* Cfa/setup.scm 61 */
			{	/* Cfa/setup.scm 63 */
				BgL_literalzf2cinfozf2_bglt BgL_wide1180z00_6219;

				BgL_wide1180z00_6219 =
					((BgL_literalzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_literalzf2cinfozf2_bgl))));
				{	/* Cfa/setup.scm 63 */
					obj_t BgL_auxz00_8553;
					BgL_objectz00_bglt BgL_tmpz00_8549;

					BgL_auxz00_8553 = ((obj_t) BgL_wide1180z00_6219);
					BgL_tmpz00_8549 =
						((BgL_objectz00_bglt)
						((BgL_literalz00_bglt) ((BgL_literalz00_bglt) BgL_nodez00_5800)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8549, BgL_auxz00_8553);
				}
				((BgL_objectz00_bglt)
					((BgL_literalz00_bglt) ((BgL_literalz00_bglt) BgL_nodez00_5800)));
				{	/* Cfa/setup.scm 63 */
					long BgL_arg1857z00_6220;

					BgL_arg1857z00_6220 =
						BGL_CLASS_NUM(BGl_literalzf2Cinfozf2zzcfa_infoz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_literalz00_bglt)
								((BgL_literalz00_bglt) BgL_nodez00_5800))),
						BgL_arg1857z00_6220);
				}
				((BgL_literalz00_bglt)
					((BgL_literalz00_bglt) ((BgL_literalz00_bglt) BgL_nodez00_5800)));
			}
			{
				BgL_approxz00_bglt BgL_auxz00_8575;
				BgL_literalzf2cinfozf2_bglt BgL_auxz00_8567;

				{	/* Cfa/setup.scm 64 */
					BgL_typez00_bglt BgL_arg1858z00_6221;

					{	/* Cfa/setup.scm 64 */
						obj_t BgL_arg1859z00_6222;

						BgL_arg1859z00_6222 =
							(((BgL_atomz00_bglt) COBJECT(
									((BgL_atomz00_bglt)
										((BgL_literalz00_bglt) BgL_nodez00_5800))))->BgL_valuez00);
						BgL_arg1858z00_6221 =
							BGl_getzd2typezd2atomz00zztype_typeofz00(BgL_arg1859z00_6222);
					}
					BgL_auxz00_8575 =
						BGl_makezd2typezd2approxz00zzcfa_approxz00(BgL_arg1858z00_6221);
				}
				{
					obj_t BgL_auxz00_8568;

					{	/* Cfa/setup.scm 64 */
						BgL_objectz00_bglt BgL_tmpz00_8569;

						BgL_tmpz00_8569 =
							((BgL_objectz00_bglt)
							((BgL_literalz00_bglt) ((BgL_literalz00_bglt) BgL_nodez00_5800)));
						BgL_auxz00_8568 = BGL_OBJECT_WIDENING(BgL_tmpz00_8569);
					}
					BgL_auxz00_8567 = ((BgL_literalzf2cinfozf2_bglt) BgL_auxz00_8568);
				}
				((((BgL_literalzf2cinfozf2_bglt) COBJECT(BgL_auxz00_8567))->
						BgL_approxz00) = ((BgL_approxz00_bglt) BgL_auxz00_8575), BUNSPEC);
			}
			return
				((obj_t)
				((BgL_literalz00_bglt) ((BgL_literalz00_bglt) BgL_nodez00_5800)));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcfa_setupz00(void)
	{
		{	/* Cfa/setup.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2332z00zzcfa_setupz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2332z00zzcfa_setupz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2332z00zzcfa_setupz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2332z00zzcfa_setupz00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string2332z00zzcfa_setupz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string2332z00zzcfa_setupz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2332z00zzcfa_setupz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2332z00zzcfa_setupz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2332z00zzcfa_setupz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2332z00zzcfa_setupz00));
			BGl_modulezd2initializa7ationz75zzcfa_infoz00(3788352L,
				BSTRING_TO_STRING(BGl_string2332z00zzcfa_setupz00));
			BGl_modulezd2initializa7ationz75zzcfa_info2z00(0L,
				BSTRING_TO_STRING(BGl_string2332z00zzcfa_setupz00));
			BGl_modulezd2initializa7ationz75zzcfa_info3z00(0L,
				BSTRING_TO_STRING(BGl_string2332z00zzcfa_setupz00));
			BGl_modulezd2initializa7ationz75zzcfa_approxz00(285971837L,
				BSTRING_TO_STRING(BGl_string2332z00zzcfa_setupz00));
			return
				BGl_modulezd2initializa7ationz75zzcfa_pairz00(37668556L,
				BSTRING_TO_STRING(BGl_string2332z00zzcfa_setupz00));
		}

	}

#ifdef __cplusplus
}
#endif
