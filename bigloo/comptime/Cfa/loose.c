/*===========================================================================*/
/*   (Cfa/loose.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cfa/loose.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CFA_LOOSE_TYPE_DEFINITIONS
#define BGL_CFA_LOOSE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_approxz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		bool_t BgL_typezd2lockedzf3z21;
		obj_t BgL_allocsz00;
		bool_t BgL_topzf3zf3;
		long BgL_lostzd2stampzd2;
		obj_t BgL_dupz00;
		bool_t BgL_haszd2procedurezf3z21;
	}                *BgL_approxz00_bglt;


#endif													// BGL_CFA_LOOSE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_objectzd2initzd2zzcfa_loosez00(void);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00(obj_t,
		BgL_approxz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2loosez12zc0zzcfa_loosez00(BgL_globalz00_bglt,
		BgL_approxz00_bglt);
	static obj_t BGl_methodzd2initzd2zzcfa_loosez00(void);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static BgL_approxz00_bglt BGl_z62loosez12z70zzcfa_loosez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_disablezd2Xzd2Tz12z12zzcfa_procedurez00(BgL_approxz00_bglt,
		obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzcfa_loosez00(void);
	extern obj_t BGl_za2cfazd2stampza2zd2zzcfa_iteratez00;
	static obj_t BGl_z62globalzd2loosez12za2zzcfa_loosez00(obj_t, obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzcfa_loosez00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzcfa_loosez00(void);
	static obj_t BGl_genericzd2initzd2zzcfa_loosez00(void);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_loosez12z12zzcfa_loosez00(BgL_approxz00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62loosezd2allocz121488za2zzcfa_loosez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_loosezd2allocz12zc0zzcfa_loosez00(BgL_nodez00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcfa_loosez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_procedurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_approxz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info2z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_infoz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_setz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_iteratez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_nodez00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzcfa_loosez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcfa_loosez00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzcfa_loosez00(void);
	static obj_t BGl_z62loosezd2allocz12za2zzcfa_loosez00(obj_t, obj_t);
	static obj_t __cnst[3];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1932z00zzcfa_loosez00,
		BgL_bgl_za762looseza7d2alloc1938z00,
		BGl_z62loosezd2allocz121488za2zzcfa_loosez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_loosez12zd2envzc0zzcfa_loosez00,
		BgL_bgl_za762looseza712za770za7za71939za7, BGl_z62loosez12z70zzcfa_loosez00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2loosez12zd2envz12zzcfa_loosez00,
		BgL_bgl_za762globalza7d2loos1940z00,
		BGl_z62globalzd2loosez12za2zzcfa_loosez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1931z00zzcfa_loosez00,
		BgL_bgl_string1931za700za7za7c1941za7, "global track is lost", 20);
	      DEFINE_STRING(BGl_string1933z00zzcfa_loosez00,
		BgL_bgl_string1933za700za7za7c1942za7, "loose-alloc!1488", 16);
	      DEFINE_STRING(BGl_string1934z00zzcfa_loosez00,
		BgL_bgl_string1934za700za7za7c1943za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string1935z00zzcfa_loosez00,
		BgL_bgl_string1935za700za7za7c1944za7, "cfa_loose", 9);
	      DEFINE_STRING(BGl_string1936z00zzcfa_loosez00,
		BgL_bgl_string1936za700za7za7c1945za7,
		"loose-alloc!1488 all (import export) ", 37);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_loosezd2allocz12zd2envz12zzcfa_loosez00,
		BgL_bgl_za762looseza7d2alloc1946z00,
		BGl_z62loosezd2allocz12za2zzcfa_loosez00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcfa_loosez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcfa_loosez00(long
		BgL_checksumz00_3571, char *BgL_fromz00_3572)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcfa_loosez00))
				{
					BGl_requirezd2initializa7ationz75zzcfa_loosez00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcfa_loosez00();
					BGl_libraryzd2moduleszd2initz00zzcfa_loosez00();
					BGl_cnstzd2initzd2zzcfa_loosez00();
					BGl_importedzd2moduleszd2initz00zzcfa_loosez00();
					BGl_genericzd2initzd2zzcfa_loosez00();
					return BGl_toplevelzd2initzd2zzcfa_loosez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcfa_loosez00(void)
	{
		{	/* Cfa/loose.scm 16 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cfa_loose");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "cfa_loose");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cfa_loose");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cfa_loose");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cfa_loose");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cfa_loose");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "cfa_loose");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "cfa_loose");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cfa_loose");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "cfa_loose");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcfa_loosez00(void)
	{
		{	/* Cfa/loose.scm 16 */
			{	/* Cfa/loose.scm 16 */
				obj_t BgL_cportz00_3559;

				{	/* Cfa/loose.scm 16 */
					obj_t BgL_stringz00_3566;

					BgL_stringz00_3566 = BGl_string1936z00zzcfa_loosez00;
					{	/* Cfa/loose.scm 16 */
						obj_t BgL_startz00_3567;

						BgL_startz00_3567 = BINT(0L);
						{	/* Cfa/loose.scm 16 */
							obj_t BgL_endz00_3568;

							BgL_endz00_3568 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3566)));
							{	/* Cfa/loose.scm 16 */

								BgL_cportz00_3559 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3566, BgL_startz00_3567, BgL_endz00_3568);
				}}}}
				{
					long BgL_iz00_3560;

					BgL_iz00_3560 = 2L;
				BgL_loopz00_3561:
					if ((BgL_iz00_3560 == -1L))
						{	/* Cfa/loose.scm 16 */
							return BUNSPEC;
						}
					else
						{	/* Cfa/loose.scm 16 */
							{	/* Cfa/loose.scm 16 */
								obj_t BgL_arg1937z00_3562;

								{	/* Cfa/loose.scm 16 */

									{	/* Cfa/loose.scm 16 */
										obj_t BgL_locationz00_3564;

										BgL_locationz00_3564 = BBOOL(((bool_t) 0));
										{	/* Cfa/loose.scm 16 */

											BgL_arg1937z00_3562 =
												BGl_readz00zz__readerz00(BgL_cportz00_3559,
												BgL_locationz00_3564);
										}
									}
								}
								{	/* Cfa/loose.scm 16 */
									int BgL_tmpz00_3601;

									BgL_tmpz00_3601 = (int) (BgL_iz00_3560);
									CNST_TABLE_SET(BgL_tmpz00_3601, BgL_arg1937z00_3562);
							}}
							{	/* Cfa/loose.scm 16 */
								int BgL_auxz00_3565;

								BgL_auxz00_3565 = (int) ((BgL_iz00_3560 - 1L));
								{
									long BgL_iz00_3606;

									BgL_iz00_3606 = (long) (BgL_auxz00_3565);
									BgL_iz00_3560 = BgL_iz00_3606;
									goto BgL_loopz00_3561;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcfa_loosez00(void)
	{
		{	/* Cfa/loose.scm 16 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcfa_loosez00(void)
	{
		{	/* Cfa/loose.scm 16 */
			return BUNSPEC;
		}

	}



/* loose! */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_loosez12z12zzcfa_loosez00(BgL_approxz00_bglt BgL_approxz00_23,
		obj_t BgL_ownerz00_24)
	{
		{	/* Cfa/loose.scm 36 */
			if (
				((((BgL_approxz00_bglt) COBJECT(BgL_approxz00_23))->
						BgL_lostzd2stampzd2) <
					(long) CINT(BGl_za2cfazd2stampza2zd2zzcfa_iteratez00)))
				{	/* Cfa/loose.scm 39 */
					((((BgL_approxz00_bglt) COBJECT(BgL_approxz00_23))->
							BgL_lostzd2stampzd2) =
						((long) (long) CINT(BGl_za2cfazd2stampza2zd2zzcfa_iteratez00)),
						BUNSPEC);
					BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00
						(BGl_loosezd2allocz12zd2envz12zzcfa_loosez00, BgL_approxz00_23);
				}
			else
				{	/* Cfa/loose.scm 39 */
					BFALSE;
				}
			return BgL_approxz00_23;
		}

	}



/* &loose! */
	BgL_approxz00_bglt BGl_z62loosez12z70zzcfa_loosez00(obj_t BgL_envz00_3546,
		obj_t BgL_approxz00_3547, obj_t BgL_ownerz00_3548)
	{
		{	/* Cfa/loose.scm 36 */
			return
				BGl_loosez12z12zzcfa_loosez00(
				((BgL_approxz00_bglt) BgL_approxz00_3547), BgL_ownerz00_3548);
		}

	}



/* global-loose! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzd2loosez12zc0zzcfa_loosez00(BgL_globalz00_bglt BgL_globalz00_26,
		BgL_approxz00_bglt BgL_approxz00_27)
	{
		{	/* Cfa/loose.scm 59 */
			if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
						(((BgL_globalz00_bglt) COBJECT(BgL_globalz00_26))->BgL_importz00),
						CNST_TABLE_REF(0))))
				{	/* Cfa/loose.scm 62 */
					return
						((obj_t)
						BGl_loosez12z12zzcfa_loosez00(BgL_approxz00_27, CNST_TABLE_REF(1)));
				}
			else
				{	/* Cfa/loose.scm 62 */
					return
						BGl_disablezd2Xzd2Tz12z12zzcfa_procedurez00(BgL_approxz00_27,
						BGl_string1931z00zzcfa_loosez00);
				}
		}

	}



/* &global-loose! */
	obj_t BGl_z62globalzd2loosez12za2zzcfa_loosez00(obj_t BgL_envz00_3551,
		obj_t BgL_globalz00_3552, obj_t BgL_approxz00_3553)
	{
		{	/* Cfa/loose.scm 59 */
			return
				BGl_globalzd2loosez12zc0zzcfa_loosez00(
				((BgL_globalz00_bglt) BgL_globalz00_3552),
				((BgL_approxz00_bglt) BgL_approxz00_3553));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcfa_loosez00(void)
	{
		{	/* Cfa/loose.scm 16 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcfa_loosez00(void)
	{
		{	/* Cfa/loose.scm 16 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_loosezd2allocz12zd2envz12zzcfa_loosez00,
				BGl_proc1932z00zzcfa_loosez00, BGl_nodez00zzast_nodez00,
				BGl_string1933z00zzcfa_loosez00);
		}

	}



/* &loose-alloc!1488 */
	obj_t BGl_z62loosezd2allocz121488za2zzcfa_loosez00(obj_t BgL_envz00_3555,
		obj_t BgL_nodez00_3556)
	{
		{	/* Cfa/loose.scm 48 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(2),
				BGl_string1934z00zzcfa_loosez00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_3556)));
		}

	}



/* loose-alloc! */
	BGL_EXPORTED_DEF obj_t BGl_loosezd2allocz12zc0zzcfa_loosez00(BgL_nodez00_bglt
		BgL_nodez00_25)
	{
		{	/* Cfa/loose.scm 48 */
			{	/* Cfa/loose.scm 48 */
				obj_t BgL_method1489z00_3028;

				{	/* Cfa/loose.scm 48 */
					obj_t BgL_res1930z00_3545;

					{	/* Cfa/loose.scm 48 */
						long BgL_objzd2classzd2numz00_3516;

						BgL_objzd2classzd2numz00_3516 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_25));
						{	/* Cfa/loose.scm 48 */
							obj_t BgL_arg1811z00_3517;

							BgL_arg1811z00_3517 =
								PROCEDURE_REF(BGl_loosezd2allocz12zd2envz12zzcfa_loosez00,
								(int) (1L));
							{	/* Cfa/loose.scm 48 */
								int BgL_offsetz00_3520;

								BgL_offsetz00_3520 = (int) (BgL_objzd2classzd2numz00_3516);
								{	/* Cfa/loose.scm 48 */
									long BgL_offsetz00_3521;

									BgL_offsetz00_3521 =
										((long) (BgL_offsetz00_3520) - OBJECT_TYPE);
									{	/* Cfa/loose.scm 48 */
										long BgL_modz00_3522;

										BgL_modz00_3522 =
											(BgL_offsetz00_3521 >> (int) ((long) ((int) (4L))));
										{	/* Cfa/loose.scm 48 */
											long BgL_restz00_3524;

											BgL_restz00_3524 =
												(BgL_offsetz00_3521 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Cfa/loose.scm 48 */

												{	/* Cfa/loose.scm 48 */
													obj_t BgL_bucketz00_3526;

													BgL_bucketz00_3526 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_3517), BgL_modz00_3522);
													BgL_res1930z00_3545 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_3526), BgL_restz00_3524);
					}}}}}}}}
					BgL_method1489z00_3028 = BgL_res1930z00_3545;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1489z00_3028, ((obj_t) BgL_nodez00_25));
			}
		}

	}



/* &loose-alloc! */
	obj_t BGl_z62loosezd2allocz12za2zzcfa_loosez00(obj_t BgL_envz00_3549,
		obj_t BgL_nodez00_3550)
	{
		{	/* Cfa/loose.scm 48 */
			return
				BGl_loosezd2allocz12zc0zzcfa_loosez00(
				((BgL_nodez00_bglt) BgL_nodez00_3550));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcfa_loosez00(void)
	{
		{	/* Cfa/loose.scm 16 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcfa_loosez00(void)
	{
		{	/* Cfa/loose.scm 16 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1935z00zzcfa_loosez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1935z00zzcfa_loosez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1935z00zzcfa_loosez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1935z00zzcfa_loosez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1935z00zzcfa_loosez00));
			BGl_modulezd2initializa7ationz75zzcfa_iteratez00(131412063L,
				BSTRING_TO_STRING(BGl_string1935z00zzcfa_loosez00));
			BGl_modulezd2initializa7ationz75zzcfa_setz00(5932721L,
				BSTRING_TO_STRING(BGl_string1935z00zzcfa_loosez00));
			BGl_modulezd2initializa7ationz75zzcfa_infoz00(3788352L,
				BSTRING_TO_STRING(BGl_string1935z00zzcfa_loosez00));
			BGl_modulezd2initializa7ationz75zzcfa_info2z00(0L,
				BSTRING_TO_STRING(BGl_string1935z00zzcfa_loosez00));
			BGl_modulezd2initializa7ationz75zzcfa_approxz00(285971837L,
				BSTRING_TO_STRING(BGl_string1935z00zzcfa_loosez00));
			return
				BGl_modulezd2initializa7ationz75zzcfa_procedurez00(227655313L,
				BSTRING_TO_STRING(BGl_string1935z00zzcfa_loosez00));
		}

	}

#ifdef __cplusplus
}
#endif
