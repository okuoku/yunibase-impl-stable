/*===========================================================================*/
/*   (Cfa/cinfo3.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cfa/cinfo3.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CFA_INFO3_TYPE_DEFINITIONS
#define BGL_CFA_INFO3_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_pragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_formatz00;
		obj_t BgL_srfi0z00;
	}                *BgL_pragmaz00_bglt;

	typedef struct BgL_privatez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
	}                 *BgL_privatez00_bglt;

	typedef struct BgL_getfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_getfieldz00_bglt;

	typedef struct BgL_setfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_setfieldz00_bglt;

	typedef struct BgL_newz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_argszd2typezd2;
	}             *BgL_newz00_bglt;

	typedef struct BgL_vallocz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                *BgL_vallocz00_bglt;

	typedef struct BgL_vrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}              *BgL_vrefz00_bglt;

	typedef struct BgL_vsetz12z12_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}                 *BgL_vsetz12z12_bglt;

	typedef struct BgL_vlengthz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_ftypez00;
	}                 *BgL_vlengthz00_bglt;

	typedef struct BgL_instanceofz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_classz00;
	}                    *BgL_instanceofz00_bglt;

	typedef struct BgL_castzd2nullzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
	}                     *BgL_castzd2nullzd2_bglt;

	typedef struct BgL_approxz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		bool_t BgL_typezd2lockedzf3z21;
		obj_t BgL_allocsz00;
		bool_t BgL_topzf3zf3;
		long BgL_lostzd2stampzd2;
		obj_t BgL_dupz00;
		bool_t BgL_haszd2procedurezf3z21;
	}                *BgL_approxz00_bglt;

	typedef struct BgL_pragmazf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                        *BgL_pragmazf2cinfozf2_bglt;

	typedef struct BgL_getfieldzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                          *BgL_getfieldzf2cinfozf2_bglt;

	typedef struct BgL_setfieldzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                          *BgL_setfieldzf2cinfozf2_bglt;

	typedef struct BgL_newzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                     *BgL_newzf2cinfozf2_bglt;

	typedef struct BgL_instanceofzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                            *BgL_instanceofzf2cinfozf2_bglt;

	typedef struct BgL_castzd2nullzf2cinfoz20_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                             *BgL_castzd2nullzf2cinfoz20_bglt;

	typedef struct BgL_vrefzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		bool_t BgL_tvectorzf3zf3;
	}                      *BgL_vrefzf2cinfozf2_bglt;

	typedef struct BgL_vsetz12zf2cinfoze0_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		bool_t BgL_tvectorzf3zf3;
	}                         *BgL_vsetz12zf2cinfoze0_bglt;

	typedef struct BgL_vlengthzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		bool_t BgL_tvectorzf3zf3;
	}                         *BgL_vlengthzf2cinfozf2_bglt;

	typedef struct BgL_prezd2valloczf2cinfoz20_bgl
	{
		struct BgL_variablez00_bgl *BgL_ownerz00;
	}                              *BgL_prezd2valloczf2cinfoz20_bglt;

	typedef struct BgL_valloczf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                        *BgL_valloczf2cinfozf2_bglt;

	typedef struct BgL_valloczf2cinfozb2optimz40_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		struct BgL_approxz00_bgl *BgL_valuezd2approxzd2;
		long BgL_lostzd2stampzd2;
		struct BgL_variablez00_bgl *BgL_ownerz00;
		bool_t BgL_stackablezf3zf3;
		obj_t BgL_stackzd2stampzd2;
		bool_t BgL_seenzf3zf3;
	}                                *BgL_valloczf2cinfozb2optimz40_bglt;


#endif													// BGL_CFA_INFO3_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static BgL_castzd2nullzd2_bglt BGl_z62lambda1927z62zzcfa_info3z00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_pragmaz00_bglt BGl_z62lambda1767z62zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_pragmaz00_bglt
		BGl_makezd2pragmazf2Cinfoz20zzcfa_info3z00(obj_t, BgL_typez00_bglt, obj_t,
		obj_t, obj_t, obj_t, obj_t, BgL_approxz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32010ze3ze5zzcfa_info3z00(obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_z62valloczf2Cinfozd2approxz42zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62getfieldzf2Cinfozd2czd2formatz90zzcfa_info3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_instanceofz00_bglt
		BGl_makezd2instanceofzf2Cinfoz20zzcfa_info3z00(obj_t, BgL_typez00_bglt,
		obj_t, obj_t, obj_t, obj_t, obj_t, BgL_typez00_bglt, BgL_approxz00_bglt);
	static BgL_castzd2nullzd2_bglt BGl_z62lambda1930z62zzcfa_info3z00(obj_t,
		obj_t);
	static BgL_castzd2nullzd2_bglt BGl_z62lambda1933z62zzcfa_info3z00(obj_t,
		obj_t);
	static BgL_instanceofz00_bglt
		BGl_z62instanceofzf2Cinfozd2nilz42zzcfa_info3z00(obj_t);
	static BgL_newz00_bglt BGl_z62newzf2Cinfozd2nilz42zzcfa_info3z00(obj_t);
	BGL_EXPORTED_DECL BgL_setfieldz00_bglt
		BGl_setfieldzf2Cinfozd2nilz20zzcfa_info3z00(void);
	static obj_t
		BGl_z62valloczf2Cinfozb2optimzd2typezd2setz12z30zzcfa_info3z00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_newzf2Cinfozf2zzcfa_info3z00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_vlengthzf2Cinfozd2ftypez20zzcfa_info3z00(BgL_vlengthz00_bglt);
	static obj_t BGl_z62pragmazf2Cinfozd2exprza2zd2setz12z20zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62getfieldzf2Cinfozd2typezd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62valloczf2Cinfozd2typez42zzcfa_info3z00(obj_t,
		obj_t);
	static BgL_approxz00_bglt BGl_z62lambda1940z62zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62valloczf2Cinfozd2exprza2zd2setz12z20zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda1941z62zzcfa_info3z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_vlengthzf2Cinfozd2vtypez20zzcfa_info3z00(BgL_vlengthz00_bglt);
	static BgL_vrefz00_bglt BGl_z62lambda1948z62zzcfa_info3z00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62castzd2nullzf2Cinfozd2sidezd2effectzd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62castzd2nullzf2Cinfozd2sidezd2effectz42zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_prezd2valloczf2Cinfozd2ftypezf2zzcfa_info3z00(BgL_vallocz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31953ze3ze5zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_pragmazf2Cinfozf2zzcfa_info3z00 = BUNSPEC;
	static BgL_vrefz00_bglt BGl_z62lambda1951z62zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_instanceofz00_bglt
		BGl_instanceofzf2Cinfozd2nilz20zzcfa_info3z00(void);
	static obj_t BGl_z62instanceofzf2Cinfozd2locz42zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62newzf2Cinfozd2locz42zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setfieldzf2Cinfozd2locz20zzcfa_info3z00(BgL_setfieldz00_bglt);
	static BgL_vrefz00_bglt BGl_z62lambda1954z62zzcfa_info3z00(obj_t, obj_t);
	extern obj_t BGl_approxz00zzcfa_infoz00;
	BGL_EXPORTED_DECL obj_t
		BGl_newzf2Cinfozd2keyz20zzcfa_info3z00(BgL_newz00_bglt);
	static obj_t
		BGl_z62prezd2valloczf2Cinfozd2sidezd2effectzd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62vsetz12zf2Cinfozd2effectz50zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_vrefzf2Cinfozd2keyzd2setz12ze0zzcfa_info3z00(BgL_vrefz00_bglt, obj_t);
	static obj_t
		BGl_z62prezd2valloczf2Cinfozd2ftypezd2setz12z50zzcfa_info3z00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62prezd2valloczf2Cinfozd2effectzd2setz12z50zzcfa_info3z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_prezd2valloczf2Cinfozd2effectzf2zzcfa_info3z00(BgL_vallocz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_vsetz12zf2Cinfozd2unsafez32zzcfa_info3z00(BgL_vsetz12z12_bglt);
	static obj_t BGl_z62zc3z04anonymousza31881ze3ze5zzcfa_info3z00(obj_t, obj_t);
	static BgL_vsetz12z12_bglt
		BGl_z62vsetz12zf2Cinfozd2nilz50zzcfa_info3z00(obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_newzf2Cinfozd2typez20zzcfa_info3z00(BgL_newz00_bglt);
	static BgL_approxz00_bglt BGl_z62lambda1961z62zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62getfieldzf2Cinfozd2keyzd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda1962z62zzcfa_info3z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62getfieldzf2Cinfozd2effectzd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda1967z62zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62lambda1968z62zzcfa_info3z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_valloczf2Cinfozb2optimzd2valuezd2approxz40zzcfa_info3z00
		(BgL_vallocz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_castzd2nullzf2Cinfozd2effectzd2setz12z32zzcfa_info3z00
		(BgL_castzd2nullzd2_bglt, obj_t);
	static BgL_approxz00_bglt
		BGl_z62valloczf2Cinfozb2optimzd2valuezd2approxz22zzcfa_info3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_variablez00_bglt
		BGl_valloczf2Cinfozb2optimzd2ownerz92zzcfa_info3z00(BgL_vallocz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_pragmazf2Cinfozd2exprza2z82zzcfa_info3z00(BgL_pragmaz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_instanceofzf2Cinfozd2locz20zzcfa_info3z00(BgL_instanceofz00_bglt);
	static BgL_instanceofz00_bglt
		BGl_z62makezd2instanceofzf2Cinfoz42zzcfa_info3z00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_vsetz12z12_bglt BGl_z62lambda1976z62zzcfa_info3z00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_instanceofzf2Cinfozd2exprza2z82zzcfa_info3z00(BgL_instanceofz00_bglt);
	static obj_t
		BGl_z62valloczf2Cinfozb2optimzd2sidezd2effectzd2setz12ze2zzcfa_info3z00
		(obj_t, obj_t, obj_t);
	static BgL_vsetz12z12_bglt BGl_z62lambda1979z62zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_instanceofzf2Cinfozd2effectz20zzcfa_info3z00(BgL_instanceofz00_bglt);
	static obj_t
		BGl_z62valloczf2Cinfozb2optimzd2ftypezd2setz12z30zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62valloczf2Cinfozb2optimzd2effectzd2setz12z30zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_getfieldzf2Cinfozd2typez20zzcfa_info3z00(BgL_getfieldz00_bglt);
	static obj_t BGl_z62vsetz12zf2Cinfozd2locz50zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62vsetz12zf2Cinfozd2exprza2zd2setz12z32zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static BgL_vsetz12z12_bglt BGl_z62lambda1982z62zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_valloczf2Cinfozd2otypez20zzcfa_info3z00(BgL_vallocz00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_vsetz12zf2Cinfozd2otypez32zzcfa_info3z00(BgL_vsetz12z12_bglt);
	static obj_t BGl_z62setfieldzf2Cinfozd2sidezd2effectz90zzcfa_info3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_vrefzf2Cinfozd2ftypezd2setz12ze0zzcfa_info3z00(BgL_vrefz00_bglt,
		BgL_typez00_bglt);
	static BgL_approxz00_bglt BGl_z62lambda1989z62zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_instanceofzf2Cinfozf3z01zzcfa_info3z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_vlengthzf2Cinfozd2effectzd2setz12ze0zzcfa_info3z00(BgL_vlengthz00_bglt,
		obj_t);
	static obj_t BGl_z62setfieldzf2Cinfozd2otypezd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_vsetz12z12_bglt
		BGl_makezd2vsetz12zf2Cinfoz32zzcfa_info3z00(obj_t, BgL_typez00_bglt, obj_t,
		obj_t, obj_t, obj_t, obj_t, BgL_typez00_bglt, BgL_typez00_bglt,
		BgL_typez00_bglt, bool_t, BgL_approxz00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_newzf2Cinfozd2czd2formatzf2zzcfa_info3z00(BgL_newz00_bglt);
	static obj_t BGl_z62pragmazf2Cinfozd2formatz42zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzcfa_info3z00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozb2optimzd2czd2formatz40zzcfa_info3z00
		(BgL_vallocz00_bglt);
	static obj_t BGl_z62lambda1990z62zzcfa_info3z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1995z62zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62valloczf2Cinfozd2effectz42zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62lambda1996z62zzcfa_info3z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_castzd2nullzf2Cinfozd2exprza2zd2setz12z90zzcfa_info3z00
		(BgL_castzd2nullzd2_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_castzd2nullzf2Cinfozf3zd3zzcfa_info3z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_newzf2Cinfozd2typezd2setz12ze0zzcfa_info3z00(BgL_newz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_valloczf2Cinfozd2approxz20zzcfa_info3z00(BgL_vallocz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32123ze3ze5zzcfa_info3z00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_prezd2valloczf2Cinfoz20zzcfa_info3z00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_vsetz12zf2Cinfozd2effectzd2setz12zf2zzcfa_info3z00(BgL_vsetz12z12_bglt,
		obj_t);
	static obj_t BGl_z62setfieldzf2Cinfozd2czd2formatz90zzcfa_info3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_pragmazf2Cinfozd2sidezd2effectzf2zzcfa_info3z00(BgL_pragmaz00_bglt);
	static obj_t BGl_z62setfieldzf2Cinfozd2keyz42zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62valloczf2Cinfozb2optimzd2seenzf3z03zzcfa_info3z00(obj_t,
		obj_t);
	static BgL_vallocz00_bglt BGl_z62makezd2valloczf2Cinfoz42zzcfa_info3z00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62pragmazf2Cinfozd2effectzd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_newzf2Cinfozd2keyzd2setz12ze0zzcfa_info3z00(BgL_newz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_getfieldzf2Cinfozd2otypezd2setz12ze0zzcfa_info3z00(BgL_getfieldz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_newzf2Cinfozd2effectzd2setz12ze0zzcfa_info3z00(BgL_newz00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_getfieldzf2Cinfozf3z01zzcfa_info3z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozb2optimzd2keyzd2setz12z52zzcfa_info3z00
		(BgL_vallocz00_bglt, obj_t);
	static obj_t BGl_z62vrefzf2Cinfozd2ftypezd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_vsetz12zf2Cinfozd2approxz32zzcfa_info3z00(BgL_vsetz12z12_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_getfieldzf2Cinfozd2keyz20zzcfa_info3z00(BgL_getfieldz00_bglt);
	static obj_t BGl_methodzd2initzd2zzcfa_info3z00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozb2optimzd2effectzd2setz12z52zzcfa_info3z00
		(BgL_vallocz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_instanceofzf2Cinfozd2exprza2zd2setz12z42zzcfa_info3z00
		(BgL_instanceofz00_bglt, obj_t);
	static obj_t BGl_z62setfieldzf2Cinfozd2keyzd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62vrefzf2Cinfozd2tvectorzf3zb1zzcfa_info3z00(obj_t, obj_t);
	static BgL_pragmaz00_bglt BGl_z62pragmazf2Cinfozd2nilz42zzcfa_info3z00(obj_t);
	static BgL_typez00_bglt
		BGl_z62prezd2valloczf2Cinfozd2ftypez90zzcfa_info3z00(obj_t, obj_t);
	static BgL_getfieldz00_bglt
		BGl_z62makezd2getfieldzf2Cinfoz42zzcfa_info3z00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62vsetz12zf2Cinfozd2tvectorzf3za3zzcfa_info3z00(obj_t,
		obj_t);
	static obj_t BGl_z62getfieldzf2Cinfozd2fnamez42zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62setfieldzf2Cinfozd2exprza2ze0zzcfa_info3z00(obj_t, obj_t);
	static obj_t
		BGl_z62instanceofzf2Cinfozd2classzd2setz12z82zzcfa_info3z00(obj_t, obj_t,
		obj_t);
	static BgL_vallocz00_bglt BGl_z62valloczf2Cinfozd2nilz42zzcfa_info3z00(obj_t);
	static BgL_typez00_bglt BGl_z62vsetz12zf2Cinfozd2ftypez50zzcfa_info3z00(obj_t,
		obj_t);
	static obj_t
		BGl_z62instanceofzf2Cinfozd2effectzd2setz12z82zzcfa_info3z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62newzf2Cinfozd2effectzd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setfieldzf2Cinfozd2effectzd2setz12ze0zzcfa_info3z00
		(BgL_setfieldz00_bglt, obj_t);
	static obj_t BGl_z62pragmazf2Cinfozd2locz42zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_getfieldzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_info3z00
		(BgL_getfieldz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozb2optimzd2exprza2z30zzcfa_info3z00(BgL_vallocz00_bglt);
	BGL_EXPORTED_DECL BgL_variablez00_bglt
		BGl_prezd2valloczf2Cinfozd2ownerzf2zzcfa_info3z00(BgL_vallocz00_bglt);
	static BgL_typez00_bglt BGl_z62vsetz12zf2Cinfozd2vtypez50zzcfa_info3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_pragmazf2Cinfozd2formatz20zzcfa_info3z00(BgL_pragmaz00_bglt);
	static BgL_typez00_bglt
		BGl_z62valloczf2Cinfozb2optimzd2ftypezf0zzcfa_info3z00(obj_t, obj_t);
	static obj_t
		BGl_z62getfieldzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_vsetz12zf2Cinfozd2tvectorzf3zc1zzcfa_info3z00(BgL_vsetz12z12_bglt);
	static BgL_approxz00_bglt
		BGl_z62instanceofzf2Cinfozd2approxz42zzcfa_info3z00(obj_t, obj_t);
	static BgL_approxz00_bglt BGl_z62newzf2Cinfozd2approxz42zzcfa_info3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_setfieldzf2Cinfozd2approxz20zzcfa_info3z00(BgL_setfieldz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_getfieldzf2Cinfozd2fnamez20zzcfa_info3z00(BgL_getfieldz00_bglt);
	static obj_t BGl_z62prezd2valloczf2Cinfozf3zb1zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozd2effectz20zzcfa_info3z00(BgL_vallocz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31997ze3ze5zzcfa_info3z00(obj_t);
	static BgL_approxz00_bglt
		BGl_z62valloczf2Cinfozb2optimzd2approxzf0zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62instanceofzf2Cinfozd2exprza2ze0zzcfa_info3z00(obj_t,
		obj_t);
	static obj_t BGl_z62newzf2Cinfozd2exprza2ze0zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setfieldzf2Cinfozd2exprza2z82zzcfa_info3z00(BgL_setfieldz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_vrefzf2Cinfozd2keyz20zzcfa_info3z00(BgL_vrefz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_vlengthzf2Cinfozd2keyz20zzcfa_info3z00(BgL_vlengthz00_bglt);
	static obj_t BGl_z62valloczf2Cinfozd2locz42zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_castzd2nullzf2Cinfoz20zzcfa_info3z00 = BUNSPEC;
	static obj_t BGl_z62vlengthzf2Cinfozd2exprza2ze0zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62newzf2Cinfozd2argszd2typez90zzcfa_info3z00(obj_t, obj_t);
	extern obj_t BGl_pragmaz00zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_castzd2nullzf2Cinfozd2typezd2setz12z32zzcfa_info3z00
		(BgL_castzd2nullzd2_bglt, BgL_typez00_bglt);
	static obj_t
		BGl_z62setfieldzf2Cinfozd2exprza2zd2setz12z20zzcfa_info3z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_vsetz12zf2Cinfozd2sidezd2effectzd2setz12z20zzcfa_info3z00
		(BgL_vsetz12z12_bglt, obj_t);
	static BgL_typez00_bglt BGl_z62vlengthzf2Cinfozd2typez42zzcfa_info3z00(obj_t,
		obj_t);
	static BgL_approxz00_bglt BGl_z62lambda2103z62zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62lambda2104z62zzcfa_info3z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2109z62zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62valloczf2Cinfozb2optimzf3zd1zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_prezd2valloczf2Cinfozd2czd2formatz20zzcfa_info3z00(BgL_vallocz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_vsetz12zf2Cinfozd2effectz32zzcfa_info3z00(BgL_vsetz12z12_bglt);
	static obj_t
		BGl_z62vlengthzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static BgL_vrefz00_bglt BGl_z62makezd2vrefzf2Cinfoz42zzcfa_info3z00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozb2optimzd2ftypezd2setz12z52zzcfa_info3z00
		(BgL_vallocz00_bglt, BgL_typez00_bglt);
	static obj_t BGl_z62lambda2110z62zzcfa_info3z00(obj_t, obj_t, obj_t);
	static BgL_variablez00_bglt BGl_z62lambda2115z62zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62lambda2116z62zzcfa_info3z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozd2sidezd2effectzf2zzcfa_info3z00(BgL_vallocz00_bglt);
	static obj_t BGl_z62lambda2121z62zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62lambda2122z62zzcfa_info3z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62setfieldzf2Cinfozf3z63zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62lambda2128z62zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62lambda2129z62zzcfa_info3z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_vlengthzf2Cinfozf3z01zzcfa_info3z00(obj_t);
	static obj_t BGl_z62valloczf2Cinfozd2effectzd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_z62setfieldzf2Cinfozd2approxz42zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozb2optimzd2typezd2setz12z52zzcfa_info3z00
		(BgL_vallocz00_bglt, BgL_typez00_bglt);
	static BgL_typez00_bglt
		BGl_z62getfieldzf2Cinfozd2ftypez42zzcfa_info3z00(obj_t, obj_t);
	static BgL_vrefz00_bglt BGl_z62vrefzf2Cinfozd2nilz42zzcfa_info3z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_castzd2nullzf2Cinfozd2exprza2z50zzcfa_info3z00(BgL_castzd2nullzd2_bglt);
	static obj_t
		BGl_z62vlengthzf2Cinfozd2tvectorzf3zd2setz12z71zzcfa_info3z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62vrefzf2Cinfozd2exprza2ze0zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_vsetz12zf2Cinfozd2typez32zzcfa_info3z00(BgL_vsetz12z12_bglt);
	static obj_t BGl_z62newzf2Cinfozd2argszd2typezd2setz12z50zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda2135z62zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_getfieldzf2Cinfozd2exprza2zd2setz12z42zzcfa_info3z00
		(BgL_getfieldz00_bglt, obj_t);
	static obj_t BGl_z62lambda2136z62zzcfa_info3z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62prezd2valloczf2Cinfozd2effectz90zzcfa_info3z00(obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62valloczf2Cinfozd2ftypez42zzcfa_info3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_vsetz12zf2Cinfoze0zzcfa_info3z00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozb2optimzd2stackzd2stampzd2setz12z80zzcfa_info3z00
		(BgL_vallocz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_prezd2valloczf2Cinfozd2keyzd2setz12z32zzcfa_info3z00(BgL_vallocz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_vrefzf2Cinfozd2tvectorzf3zd3zzcfa_info3z00(BgL_vrefz00_bglt);
	BGL_EXPORTED_DECL BgL_getfieldz00_bglt
		BGl_makezd2getfieldzf2Cinfoz20zzcfa_info3z00(obj_t, BgL_typez00_bglt, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, BgL_typez00_bglt, BgL_typez00_bglt,
		BgL_approxz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_newzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_info3z00(BgL_newz00_bglt,
		obj_t);
	static obj_t
		BGl_z62prezd2valloczf2Cinfozd2otypezd2setz12z50zzcfa_info3z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62getfieldzf2Cinfozd2ftypezd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_getfieldzf2Cinfozd2ftypez20zzcfa_info3z00(BgL_getfieldz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_pragmazf2Cinfozd2typezd2setz12ze0zzcfa_info3z00(BgL_pragmaz00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_z62vrefzf2Cinfozd2locz42zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62vrefzf2Cinfozd2unsafez42zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_prezd2valloczf2Cinfozd2effectzd2setz12z32zzcfa_info3z00
		(BgL_vallocz00_bglt, obj_t);
	static obj_t BGl_z62instanceofzf2Cinfozd2effectz42zzcfa_info3z00(obj_t,
		obj_t);
	static obj_t BGl_z62newzf2Cinfozd2effectz42zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setfieldzf2Cinfozd2effectz20zzcfa_info3z00(BgL_setfieldz00_bglt);
	static obj_t
		BGl_z62valloczf2Cinfozb2optimzd2stackzd2stampz22zzcfa_info3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_valloczf2Cinfozd2typez20zzcfa_info3z00(BgL_vallocz00_bglt);
	static obj_t BGl_z62valloczf2Cinfozb2optimzd2effectzf0zzcfa_info3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_vlengthzf2Cinfozf2zzcfa_info3z00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_vrefzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_info3z00(BgL_vrefz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_castzd2nullzf2Cinfozd2approxzf2zzcfa_info3z00(BgL_castzd2nullzd2_bglt);
	static obj_t BGl_z62setfieldzf2Cinfozd2typezd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62setfieldzf2Cinfozd2typez42zzcfa_info3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_vallocz00_bglt
		BGl_valloczf2Cinfozb2optimzd2nilz92zzcfa_info3z00(void);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozd2ftypezd2setz12ze0zzcfa_info3z00(BgL_vallocz00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_gczd2rootszd2initz00zzcfa_info3z00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_vrefzf2Cinfozd2vtypezd2setz12ze0zzcfa_info3z00(BgL_vrefz00_bglt,
		BgL_typez00_bglt);
	static obj_t
		BGl_z62valloczf2Cinfozb2optimzd2otypezd2setz12z30zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_vlengthzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_info3z00
		(BgL_vlengthz00_bglt, obj_t);
	extern obj_t BGl_vallocz00zzast_nodez00;
	static obj_t BGl_z62instanceofzf2Cinfozd2keyz42zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62newzf2Cinfozd2keyz42zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setfieldzf2Cinfozd2keyz20zzcfa_info3z00(BgL_setfieldz00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_vrefzf2Cinfozd2ftypez20zzcfa_info3z00(BgL_vrefz00_bglt);
	static BgL_castzd2nullzd2_bglt
		BGl_z62castzd2nullzf2Cinfozd2nilz90zzcfa_info3z00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_instanceofzf2Cinfozf2zzcfa_info3z00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_vrefzf2Cinfozd2otypezd2setz12ze0zzcfa_info3z00(BgL_vrefz00_bglt,
		BgL_typez00_bglt);
	static BgL_typez00_bglt BGl_z62pragmazf2Cinfozd2typez42zzcfa_info3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_vlengthzf2Cinfozd2exprza2z82zzcfa_info3z00(BgL_vlengthz00_bglt);
	static obj_t BGl_z62castzd2nullzf2Cinfozd2exprza2z32zzcfa_info3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_newzf2Cinfozd2argszd2typezf2zzcfa_info3z00(BgL_newz00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_vrefzf2Cinfozd2vtypez20zzcfa_info3z00(BgL_vrefz00_bglt);
	static BgL_newz00_bglt BGl_z62makezd2newzf2Cinfoz42zzcfa_info3z00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62valloczf2Cinfozb2optimzd2lostzd2stampz22zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozb2optimzd2locz92zzcfa_info3z00(BgL_vallocz00_bglt);
	static obj_t
		BGl_z62pragmazf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_valloczf2Cinfozb2optimzd2typez92zzcfa_info3z00(BgL_vallocz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_prezd2valloczf2Cinfozd2exprza2z50zzcfa_info3z00(BgL_vallocz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozb2optimzd2lostzd2stampzd2setz12z80zzcfa_info3z00
		(BgL_vallocz00_bglt, long);
	static BgL_variablez00_bglt
		BGl_z62prezd2valloczf2Cinfozd2ownerz90zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_valloczf2Cinfozb2optimzf3zb3zzcfa_info3z00(obj_t);
	static obj_t BGl_z62vsetz12zf2Cinfozd2effectzd2setz12z90zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_vlengthzf2Cinfozd2czd2formatzf2zzcfa_info3z00(BgL_vlengthz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_setfieldzf2Cinfozd2ftypezd2setz12ze0zzcfa_info3z00(BgL_setfieldz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_instanceofzf2Cinfozd2keyz20zzcfa_info3z00(BgL_instanceofz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_vrefzf2Cinfozd2tvectorzf3zd2setz12z13zzcfa_info3z00(BgL_vrefz00_bglt,
		bool_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_vrefzf2Cinfozd2typez20zzcfa_info3z00(BgL_vrefz00_bglt);
	static obj_t BGl_z62castzd2nullzf2Cinfozd2locz90zzcfa_info3z00(obj_t, obj_t);
	static obj_t
		BGl_z62prezd2valloczf2Cinfozd2sidezd2effectz42zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62setfieldzf2Cinfozd2effectz42zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62getfieldzf2Cinfozd2sidezd2effectz90zzcfa_info3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_vlengthzf2Cinfozd2vtypezd2setz12ze0zzcfa_info3z00(BgL_vlengthz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_instanceofzf2Cinfozd2typez20zzcfa_info3z00(BgL_instanceofz00_bglt);
	static obj_t BGl_z62vsetz12zf2Cinfozd2keyz50zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_castzd2nullzf2Cinfozd2sidezd2effectz20zzcfa_info3z00
		(BgL_castzd2nullzd2_bglt);
	static BgL_variablez00_bglt
		BGl_z62valloczf2Cinfozb2optimzd2ownerzf0zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31905ze3ze5zzcfa_info3z00(obj_t, obj_t);
	extern obj_t BGl_castzd2nullzd2zzast_nodez00;
	static obj_t BGl_z62vrefzf2Cinfozd2vtypezd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_prezd2valloczf2Cinfozd2ftypezd2setz12z32zzcfa_info3z00
		(BgL_vallocz00_bglt, BgL_typez00_bglt);
	static obj_t
		BGl_z62valloczf2Cinfozb2optimzd2sidezd2effectz22zzcfa_info3z00(obj_t,
		obj_t);
	extern obj_t BGl_variablez00zzast_varz00;
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozb2optimzd2stackzd2stampz40zzcfa_info3z00
		(BgL_vallocz00_bglt);
	static obj_t BGl_z62vrefzf2Cinfozd2otypezd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static BgL_approxz00_bglt BGl_z62lambda1800z62zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62lambda1801z62zzcfa_info3z00(obj_t, obj_t, obj_t);
	static BgL_typez00_bglt
		BGl_z62setfieldzf2Cinfozd2otypez42zzcfa_info3z00(obj_t, obj_t);
	static BgL_approxz00_bglt BGl_z62vrefzf2Cinfozd2approxz42zzcfa_info3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_vsetz12zf2Cinfozf3z13zzcfa_info3z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_vlengthzf2Cinfozd2sidezd2effectzf2zzcfa_info3z00(BgL_vlengthz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_vlengthzf2Cinfozd2keyzd2setz12ze0zzcfa_info3z00(BgL_vlengthz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_castzd2nullzf2Cinfozd2effectzf2zzcfa_info3z00(BgL_castzd2nullzd2_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_instanceofzf2Cinfozd2czd2formatzf2zzcfa_info3z00
		(BgL_instanceofz00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_valloczf2Cinfozb2optimzd2otypez92zzcfa_info3z00(BgL_vallocz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_prezd2valloczf2Cinfozd2typezd2setz12z32zzcfa_info3z00
		(BgL_vallocz00_bglt, BgL_typez00_bglt);
	static BgL_approxz00_bglt
		BGl_z62vlengthzf2Cinfozd2approxz42zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_vallocz00_bglt
		BGl_makezd2valloczf2Cinfoz20zzcfa_info3z00(obj_t, BgL_typez00_bglt, obj_t,
		obj_t, obj_t, obj_t, obj_t, BgL_typez00_bglt, BgL_typez00_bglt,
		BgL_approxz00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_instanceofzf2Cinfozd2classz20zzcfa_info3z00(BgL_instanceofz00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_setfieldzf2Cinfozd2otypez20zzcfa_info3z00(BgL_setfieldz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_instanceofzf2Cinfozd2typezd2setz12ze0zzcfa_info3z00
		(BgL_instanceofz00_bglt, BgL_typez00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31835ze3ze5zzcfa_info3z00(obj_t, obj_t);
	static BgL_getfieldz00_bglt BGl_z62lambda1823z62zzcfa_info3z00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_typez00_bglt
		BGl_z62instanceofzf2Cinfozd2typez42zzcfa_info3z00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62newzf2Cinfozd2typez42zzcfa_info3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_setfieldzf2Cinfozd2typez20zzcfa_info3z00(BgL_setfieldz00_bglt);
	BGL_EXPORTED_DECL BgL_vallocz00_bglt
		BGl_makezd2valloczf2Cinfozb2optimz92zzcfa_info3z00(obj_t, BgL_typez00_bglt,
		obj_t, obj_t, obj_t, obj_t, obj_t, BgL_typez00_bglt, BgL_typez00_bglt,
		BgL_approxz00_bglt, BgL_approxz00_bglt, long, BgL_variablez00_bglt, bool_t,
		obj_t, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_vrefzf2Cinfozd2effectzd2setz12ze0zzcfa_info3z00(BgL_vrefz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_vlengthzf2Cinfozd2typez20zzcfa_info3z00(BgL_vlengthz00_bglt);
	static obj_t
		BGl_z62prezd2valloczf2Cinfozd2exprza2zd2setz12zf2zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static BgL_vallocz00_bglt
		BGl_z62prezd2valloczf2Cinfozd2nilz90zzcfa_info3z00(obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_getfieldzf2Cinfozd2approxz20zzcfa_info3z00(BgL_getfieldz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_vsetz12zf2Cinfozd2sidezd2effectze0zzcfa_info3z00(BgL_vsetz12z12_bglt);
	static BgL_getfieldz00_bglt BGl_z62lambda1833z62zzcfa_info3z00(obj_t, obj_t);
	static BgL_getfieldz00_bglt BGl_z62lambda1836z62zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_vlengthzf2Cinfozd2approxz20zzcfa_info3z00(BgL_vlengthz00_bglt);
	BGL_EXPORTED_DECL BgL_castzd2nullzd2_bglt
		BGl_castzd2nullzf2Cinfozd2nilzf2zzcfa_info3z00(void);
	static obj_t BGl_z62pragmazf2Cinfozd2typezd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62pragmazf2Cinfozd2sidezd2effectz90zzcfa_info3z00(obj_t,
		obj_t);
	static obj_t
		BGl_z62valloczf2Cinfozb2optimzd2stackablezf3z03zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_newzf2Cinfozd2sidezd2effectzf2zzcfa_info3z00(BgL_newz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_pragmazf2Cinfozf3z01zzcfa_info3z00(obj_t);
	static BgL_approxz00_bglt BGl_z62lambda1844z62zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_instanceofzf2Cinfozd2keyzd2setz12ze0zzcfa_info3z00
		(BgL_instanceofz00_bglt, obj_t);
	static obj_t BGl_z62lambda1845z62zzcfa_info3z00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62valloczf2Cinfozb2optimzd2approxzd2setz12z30zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_instanceofzf2Cinfozd2effectzd2setz12ze0zzcfa_info3z00
		(BgL_instanceofz00_bglt, obj_t);
	static BgL_approxz00_bglt
		BGl_z62castzd2nullzf2Cinfozd2approxz90zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_valloczf2Cinfozf2zzcfa_info3z00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_vallocz00_bglt
		BGl_prezd2valloczf2Cinfozd2nilzf2zzcfa_info3z00(void);
	static obj_t BGl_z62prezd2valloczf2Cinfozd2locz90zzcfa_info3z00(obj_t, obj_t);
	static obj_t
		BGl_z62valloczf2Cinfozb2optimzd2exprza2zd2setz12z92zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static BgL_vallocz00_bglt
		BGl_z62valloczf2Cinfozb2optimzd2nilzf0zzcfa_info3z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_vsetz12zf2Cinfozd2ftypezd2setz12zf2zzcfa_info3z00(BgL_vsetz12z12_bglt,
		BgL_typez00_bglt);
	static BgL_setfieldz00_bglt BGl_z62lambda1852z62zzcfa_info3z00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_setfieldz00_bglt BGl_z62lambda1855z62zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_castzd2nullzf2Cinfozd2loczf2zzcfa_info3z00(BgL_castzd2nullzd2_bglt);
	static BgL_setfieldz00_bglt BGl_z62lambda1858z62zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozb2optimzd2sidezd2effectz40zzcfa_info3z00
		(BgL_vallocz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_vrefzf2Cinfozd2exprza2zd2setz12z42zzcfa_info3z00(BgL_vrefz00_bglt,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_getfieldzf2Cinfozf2zzcfa_info3z00 = BUNSPEC;
	BGL_EXPORTED_DECL bool_t BGl_vrefzf2Cinfozf3z01zzcfa_info3z00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzcfa_info3z00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_valloczf2Cinfozd2ftypez20zzcfa_info3z00(BgL_vallocz00_bglt);
	static BgL_typez00_bglt BGl_z62vrefzf2Cinfozd2otypez42zzcfa_info3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_getfieldzf2Cinfozd2effectzd2setz12ze0zzcfa_info3z00
		(BgL_getfieldz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_vsetz12zf2Cinfozd2ftypez32zzcfa_info3z00(BgL_vsetz12z12_bglt);
	static obj_t BGl_z62zc3z04anonymousza31766ze3ze5zzcfa_info3z00(obj_t, obj_t);
	static BgL_approxz00_bglt BGl_z62lambda1867z62zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62lambda1868z62zzcfa_info3z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_prezd2valloczf2Cinfozd2loczf2zzcfa_info3z00(BgL_vallocz00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_prezd2valloczf2Cinfozd2typezf2zzcfa_info3z00(BgL_vallocz00_bglt);
	static obj_t BGl_z62valloczf2Cinfozb2optimzd2loczf0zzcfa_info3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_vsetz12zf2Cinfozd2vtypez32zzcfa_info3z00(BgL_vsetz12z12_bglt);
	extern obj_t BGl_newz00zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozb2optimzd2otypezd2setz12z52zzcfa_info3z00
		(BgL_vallocz00_bglt, BgL_typez00_bglt);
	static obj_t BGl_z62prezd2valloczf2Cinfozd2exprza2z32zzcfa_info3z00(obj_t,
		obj_t);
	static BgL_approxz00_bglt
		BGl_z62getfieldzf2Cinfozd2approxz42zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62vlengthzf2Cinfozd2effectzd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62vrefzf2Cinfozd2effectz42zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62pragmazf2Cinfozd2keyz42zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_prezd2valloczf2Cinfozf3zd3zzcfa_info3z00(obj_t);
	extern obj_t BGl_vlengthz00zzast_nodez00;
	static BgL_newz00_bglt BGl_z62lambda1876z62zzcfa_info3z00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_newz00_bglt BGl_z62lambda1879z62zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62vlengthzf2Cinfozd2czd2formatz90zzcfa_info3z00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t
		BGl_vsetz12zf2Cinfozd2tvectorzf3zd2setz12z01zzcfa_info3z00
		(BgL_vsetz12z12_bglt, bool_t);
	BGL_EXPORTED_DECL BgL_vsetz12z12_bglt
		BGl_vsetz12zf2Cinfozd2nilz32zzcfa_info3z00(void);
	BGL_EXPORTED_DECL long
		BGl_valloczf2Cinfozb2optimzd2lostzd2stampz40zzcfa_info3z00
		(BgL_vallocz00_bglt);
	static obj_t BGl_z62vlengthzf2Cinfozd2effectz42zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31857ze3ze5zzcfa_info3z00(obj_t, obj_t);
	static BgL_newz00_bglt BGl_z62lambda1882z62zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62instanceofzf2Cinfozd2sidezd2effectz90zzcfa_info3z00(obj_t,
		obj_t);
	static obj_t BGl_z62newzf2Cinfozd2sidezd2effectz90zzcfa_info3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setfieldzf2Cinfozd2sidezd2effectzf2zzcfa_info3z00(BgL_setfieldz00_bglt);
	static obj_t BGl_z62valloczf2Cinfozd2keyz42zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_pragmazf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_info3z00
		(BgL_pragmaz00_bglt, obj_t);
	static obj_t BGl_z62vsetz12zf2Cinfozd2exprza2zf2zzcfa_info3z00(obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_z62pragmazf2Cinfozd2approxz42zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_vlengthzf2Cinfozd2exprza2zd2setz12z42zzcfa_info3z00(BgL_vlengthz00_bglt,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32111ze3ze5zzcfa_info3z00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zzcfa_info3z00(void);
	static obj_t BGl_z62valloczf2Cinfozb2optimzd2exprza2z52zzcfa_info3z00(obj_t,
		obj_t);
	static BgL_approxz00_bglt BGl_z62lambda1890z62zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62lambda1891z62zzcfa_info3z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_getfieldzf2Cinfozd2effectz20zzcfa_info3z00(BgL_getfieldz00_bglt);
	static BgL_instanceofz00_bglt BGl_z62lambda1899z62zzcfa_info3z00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_instanceofzf2Cinfozd2classzd2setz12ze0zzcfa_info3z00
		(BgL_instanceofz00_bglt, BgL_typez00_bglt);
	static obj_t BGl_z62vrefzf2Cinfozd2exprza2zd2setz12z20zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static BgL_vlengthz00_bglt
		BGl_z62vlengthzf2Cinfozd2nilz42zzcfa_info3z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_vsetz12zf2Cinfozd2locz32zzcfa_info3z00(BgL_vsetz12z12_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_vlengthzf2Cinfozd2effectz20zzcfa_info3z00(BgL_vlengthz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozb2optimzd2approxzd2setz12z52zzcfa_info3z00
		(BgL_vallocz00_bglt, BgL_approxz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_castzd2nullzf2Cinfozd2czd2formatz20zzcfa_info3z00
		(BgL_castzd2nullzd2_bglt);
	static obj_t BGl_genericzd2initzd2zzcfa_info3z00(void);
	BGL_EXPORTED_DECL bool_t BGl_valloczf2Cinfozf3z01zzcfa_info3z00(obj_t);
	static obj_t BGl_z62getfieldzf2Cinfozd2otypezd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_getfieldz00zzast_nodez00;
	BGL_EXPORTED_DECL BgL_pragmaz00_bglt
		BGl_pragmazf2Cinfozd2nilz20zzcfa_info3z00(void);
	static BgL_typez00_bglt
		BGl_z62prezd2valloczf2Cinfozd2typez90zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62vlengthzf2Cinfozd2keyzd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62castzd2nullzf2Cinfozd2effectz90zzcfa_info3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_prezd2valloczf2Cinfozd2otypezf2zzcfa_info3z00(BgL_vallocz00_bglt);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62valloczf2Cinfozd2ftypezd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31981ze3ze5zzcfa_info3z00(obj_t, obj_t);
	static BgL_getfieldz00_bglt
		BGl_z62getfieldzf2Cinfozd2nilz42zzcfa_info3z00(obj_t);
	static obj_t BGl_z62vlengthzf2Cinfozd2locz42zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32130ze3ze5zzcfa_info3z00(obj_t);
	BGL_EXPORTED_DECL BgL_vallocz00_bglt
		BGl_valloczf2Cinfozd2nilz20zzcfa_info3z00(void);
	static BgL_typez00_bglt
		BGl_z62instanceofzf2Cinfozd2classz42zzcfa_info3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozd2otypezd2setz12ze0zzcfa_info3z00(BgL_vallocz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_vlengthzf2Cinfozd2typezd2setz12ze0zzcfa_info3z00(BgL_vlengthz00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_z62vrefzf2Cinfozd2typezd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_pragmazf2Cinfozd2locz20zzcfa_info3z00(BgL_pragmaz00_bglt);
	extern obj_t BGl_typez00zztype_typez00;
	static BgL_typez00_bglt
		BGl_z62valloczf2Cinfozb2optimzd2typezf0zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32042ze3ze5zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62vrefzf2Cinfozd2effectzd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_newzf2Cinfozd2approxz20zzcfa_info3z00(BgL_newz00_bglt);
	static obj_t BGl_z62valloczf2Cinfozd2sidezd2effectz90zzcfa_info3z00(obj_t,
		obj_t);
	static obj_t BGl_z62getfieldzf2Cinfozd2locz42zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62getfieldzf2Cinfozd2exprza2ze0zzcfa_info3z00(obj_t, obj_t);
	static BgL_setfieldz00_bglt
		BGl_z62makezd2setfieldzf2Cinfoz42zzcfa_info3z00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_vrefzf2Cinfozd2unsafez20zzcfa_info3z00(BgL_vrefz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_castzd2nullzf2Cinfozd2keyzd2setz12z32zzcfa_info3z00
		(BgL_castzd2nullzd2_bglt, obj_t);
	static obj_t BGl_z62getfieldzf2Cinfozd2effectz42zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozb2optimzd2stackablezf3zd2setz12za1zzcfa_info3z00
		(BgL_vallocz00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_pragmazf2Cinfozd2keyzd2setz12ze0zzcfa_info3z00(BgL_pragmaz00_bglt,
		obj_t);
	static obj_t
		BGl_z62castzd2nullzf2Cinfozd2exprza2zd2setz12zf2zzcfa_info3z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozd2locz20zzcfa_info3z00(BgL_vallocz00_bglt);
	static obj_t BGl_z62vrefzf2Cinfozd2keyz42zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32027ze3ze5zzcfa_info3z00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_vrefzf2Cinfozf2zzcfa_info3z00 = BUNSPEC;
	static obj_t BGl_z62vrefzf2Cinfozf3z63zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_vrefzf2Cinfozd2typezd2setz12ze0zzcfa_info3z00(BgL_vrefz00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_z62valloczf2Cinfozd2exprza2ze0zzcfa_info3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_z62vsetz12zf2Cinfozf3z71zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setfieldzf2Cinfozd2otypezd2setz12ze0zzcfa_info3z00(BgL_setfieldz00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_z62getfieldzf2Cinfozf3z63zzcfa_info3z00(obj_t, obj_t);
	static obj_t
		BGl_z62valloczf2Cinfozb2optimzd2seenzf3zd2setz12zc3zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_instanceofzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_info3z00
		(BgL_instanceofz00_bglt, obj_t);
	static obj_t
		BGl_z62valloczf2Cinfozb2optimzd2lostzd2stampzd2setz12ze2zzcfa_info3z00
		(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62vsetz12zf2Cinfozd2ftypezd2setz12z90zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62pragmazf2Cinfozd2effectz42zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_getfieldzf2Cinfozd2exprza2z82zzcfa_info3z00(BgL_getfieldz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31969ze3ze5zzcfa_info3z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_prezd2valloczf2Cinfozd2sidezd2effectz20zzcfa_info3z00
		(BgL_vallocz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_vlengthzf2Cinfozd2tvectorzf3zd2setz12z13zzcfa_info3z00
		(BgL_vlengthz00_bglt, bool_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_pragmazf2Cinfozd2typez20zzcfa_info3z00(BgL_pragmaz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_valloczf2Cinfozb2optimzd2stackablezf3z61zzcfa_info3z00
		(BgL_vallocz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_getfieldzf2Cinfozd2typezd2setz12ze0zzcfa_info3z00(BgL_getfieldz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_pragmazf2Cinfozd2approxz20zzcfa_info3z00(BgL_pragmaz00_bglt);
	static obj_t
		BGl_z62castzd2nullzf2Cinfozd2typezd2setz12z50zzcfa_info3z00(obj_t, obj_t,
		obj_t);
	static BgL_castzd2nullzd2_bglt
		BGl_z62makezd2castzd2nullzf2Cinfoz90zzcfa_info3z00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozb2optimzd2exprza2zd2setz12zf0zzcfa_info3z00
		(BgL_vallocz00_bglt, obj_t);
	static BgL_pragmaz00_bglt BGl_z62makezd2pragmazf2Cinfoz42zzcfa_info3z00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62valloczf2Cinfozb2optimzd2stackzd2stampzd2setz12ze2zzcfa_info3z00
		(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozd2typezd2setz12ze0zzcfa_info3z00(BgL_vallocz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozb2optimzd2keyz92zzcfa_info3z00(BgL_vallocz00_bglt);
	static obj_t
		BGl_z62castzd2nullzf2Cinfozd2effectzd2setz12z50zzcfa_info3z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_prezd2valloczf2Cinfozd2otypezd2setz12z32zzcfa_info3z00
		(BgL_vallocz00_bglt, BgL_typez00_bglt);
	static BgL_vsetz12z12_bglt
		BGl_z62makezd2vsetz12zf2Cinfoz50zzcfa_info3z00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_newz00_bglt
		BGl_newzf2Cinfozd2nilz20zzcfa_info3z00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozb2optimzd2sidezd2effectzd2setz12z80zzcfa_info3z00
		(BgL_vallocz00_bglt, obj_t);
	static obj_t BGl_z62vlengthzf2Cinfozf3z63zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62prezd2valloczf2Cinfozd2czd2formatz42zzcfa_info3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_vrefzf2Cinfozd2exprza2z82zzcfa_info3z00(BgL_vrefz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_vlengthzf2Cinfozd2tvectorzf3zd3zzcfa_info3z00(BgL_vlengthz00_bglt);
	static obj_t BGl_z62pragmazf2Cinfozd2keyzd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62castzd2nullzf2Cinfozd2keyz90zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62castzd2nullzf2Cinfozf3zb1zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_castzd2nullzd2_bglt
		BGl_makezd2castzd2nullzf2Cinfozf2zzcfa_info3z00(obj_t, BgL_typez00_bglt,
		obj_t, obj_t, obj_t, obj_t, obj_t, BgL_approxz00_bglt);
	static BgL_vallocz00_bglt
		BGl_z62makezd2valloczf2Cinfozb2optimzf0zzcfa_info3z00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62valloczf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static BgL_vlengthz00_bglt BGl_z62lambda2004z62zzcfa_info3z00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32137ze3ze5zzcfa_info3z00(obj_t);
	static BgL_vlengthz00_bglt BGl_z62lambda2008z62zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62vsetz12zf2Cinfozd2sidezd2effectz82zzcfa_info3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_newzf2Cinfozd2locz20zzcfa_info3z00(BgL_newz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_newzf2Cinfozd2exprza2z82zzcfa_info3z00(BgL_newz00_bglt);
	static obj_t
		BGl_z62valloczf2Cinfozb2optimzd2czd2formatz22zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_newzf2Cinfozd2effectz20zzcfa_info3z00(BgL_newz00_bglt);
	static obj_t BGl_z62vrefzf2Cinfozd2tvectorzf3zd2setz12z71zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static BgL_vlengthz00_bglt BGl_z62lambda2011z62zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32065ze3ze5zzcfa_info3z00(obj_t, obj_t);
	static BgL_approxz00_bglt BGl_z62lambda2018z62zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62lambda2019z62zzcfa_info3z00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62getfieldzf2Cinfozd2exprza2zd2setz12z20zzcfa_info3z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_vrefzf2Cinfozd2approxz20zzcfa_info3z00(BgL_vrefz00_bglt);
	static obj_t BGl_z62vlengthzf2Cinfozd2vtypezd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static BgL_vlengthz00_bglt
		BGl_z62makezd2vlengthzf2Cinfoz42zzcfa_info3z00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_newzf2Cinfozf3z01zzcfa_info3z00(obj_t);
	static obj_t BGl_z62setfieldzf2Cinfozd2effectzd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_getfieldzf2Cinfozd2czd2formatzf2zzcfa_info3z00(BgL_getfieldz00_bglt);
	static obj_t
		BGl_z62prezd2valloczf2Cinfozd2keyzd2setz12z50zzcfa_info3z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda2025z62zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62lambda2026z62zzcfa_info3z00(obj_t, obj_t, obj_t);
	static BgL_typez00_bglt
		BGl_z62prezd2valloczf2Cinfozd2otypez90zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_setfieldz00_bglt
		BGl_makezd2setfieldzf2Cinfoz20zzcfa_info3z00(obj_t, BgL_typez00_bglt, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, BgL_typez00_bglt, BgL_typez00_bglt,
		BgL_approxz00_bglt);
	BGL_EXPORTED_DECL BgL_vrefz00_bglt
		BGl_makezd2vrefzf2Cinfoz20zzcfa_info3z00(obj_t, BgL_typez00_bglt, obj_t,
		obj_t, obj_t, obj_t, obj_t, BgL_typez00_bglt, BgL_typez00_bglt,
		BgL_typez00_bglt, bool_t, BgL_approxz00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_vsetz12zf2Cinfozd2vtypezd2setz12zf2zzcfa_info3z00(BgL_vsetz12z12_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_pragmazf2Cinfozd2exprza2zd2setz12z42zzcfa_info3z00(BgL_pragmaz00_bglt,
		obj_t);
	static obj_t BGl_z62setfieldzf2Cinfozd2fnamez42zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozd2exprza2zd2setz12z42zzcfa_info3z00(BgL_vallocz00_bglt,
		obj_t);
	static BgL_vallocz00_bglt BGl_z62lambda2037z62zzcfa_info3z00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_vsetz12zf2Cinfozd2otypezd2setz12zf2zzcfa_info3z00(BgL_vsetz12z12_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL BgL_vallocz00_bglt
		BGl_makezd2prezd2valloczf2Cinfozf2zzcfa_info3z00(obj_t, BgL_typez00_bglt,
		obj_t, obj_t, obj_t, obj_t, obj_t, BgL_typez00_bglt, BgL_typez00_bglt,
		BgL_variablez00_bglt);
	static BgL_typez00_bglt BGl_z62vsetz12zf2Cinfozd2otypez50zzcfa_info3z00(obj_t,
		obj_t);
	static obj_t
		BGl_z62valloczf2Cinfozb2optimzd2keyzd2setz12z30zzcfa_info3z00(obj_t, obj_t,
		obj_t);
	static BgL_vallocz00_bglt BGl_z62lambda2040z62zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62instanceofzf2Cinfozf3z63zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62newzf2Cinfozf3z63zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_setfieldzf2Cinfozf3z01zzcfa_info3z00(obj_t);
	static BgL_vallocz00_bglt BGl_z62lambda2043z62zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_pragmazf2Cinfozd2effectz20zzcfa_info3z00(BgL_pragmaz00_bglt);
	static BgL_typez00_bglt
		BGl_z62valloczf2Cinfozb2optimzd2otypezf0zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_valloczf2Cinfozb2optimzd2approxz92zzcfa_info3z00(BgL_vallocz00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcfa_info3z00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_infoz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_EXPORTED_DECL obj_t
		BGl_setfieldzf2Cinfozd2fnamez20zzcfa_info3z00(BgL_setfieldz00_bglt);
	static BgL_setfieldz00_bglt
		BGl_z62setfieldzf2Cinfozd2nilz42zzcfa_info3z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_pragmazf2Cinfozd2effectzd2setz12ze0zzcfa_info3z00(BgL_pragmaz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_valloczf2Cinfozb2optimzd2seenzf3z61zzcfa_info3z00(BgL_vallocz00_bglt);
	static BgL_variablez00_bglt BGl_z62lambda2050z62zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62lambda2051z62zzcfa_info3z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_getfieldzf2Cinfozd2keyzd2setz12ze0zzcfa_info3z00(BgL_getfieldz00_bglt,
		obj_t);
	static obj_t BGl_z62pragmazf2Cinfozf3z63zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_vrefzf2Cinfozd2sidezd2effectzf2zzcfa_info3z00(BgL_vrefz00_bglt);
	static obj_t
		BGl_z62instanceofzf2Cinfozd2exprza2zd2setz12z20zzcfa_info3z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62newzf2Cinfozd2exprza2zd2setz12z20zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setfieldzf2Cinfozd2exprza2zd2setz12z42zzcfa_info3z00
		(BgL_setfieldz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozd2czd2formatzf2zzcfa_info3z00(BgL_vallocz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_vsetz12zf2Cinfozd2typezd2setz12zf2zzcfa_info3z00(BgL_vsetz12z12_bglt,
		BgL_typez00_bglt);
	static BgL_vallocz00_bglt BGl_z62lambda2060z62zzcfa_info3z00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62prezd2valloczf2Cinfozd2keyz90zzcfa_info3z00(obj_t, obj_t);
	static BgL_vallocz00_bglt BGl_z62lambda2063z62zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_getfieldz00_bglt
		BGl_getfieldzf2Cinfozd2nilz20zzcfa_info3z00(void);
	extern obj_t BGl_vrefz00zzast_nodez00;
	static BgL_vallocz00_bglt BGl_z62lambda2066z62zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62setfieldzf2Cinfozd2locz42zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62setfieldzf2Cinfozd2ftypezd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_castzd2nullzf2Cinfozd2keyzf2zzcfa_info3z00(BgL_castzd2nullzd2_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_instanceofzf2Cinfozd2sidezd2effectzf2zzcfa_info3z00
		(BgL_instanceofz00_bglt);
	static BgL_approxz00_bglt BGl_z62lambda2075z62zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62lambda2076z62zzcfa_info3z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62vsetz12zf2Cinfozd2unsafez50zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_newzf2Cinfozd2exprza2zd2setz12z42zzcfa_info3z00(BgL_newz00_bglt, obj_t);
	static obj_t BGl_cnstzd2initzd2zzcfa_info3z00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcfa_info3z00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_prezd2valloczf2Cinfozd2exprza2zd2setz12z90zzcfa_info3z00
		(BgL_vallocz00_bglt, obj_t);
	static BgL_vallocz00_bglt
		BGl_z62makezd2prezd2valloczf2Cinfoz90zzcfa_info3z00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozb2optimzd2seenzf3zd2setz12za1zzcfa_info3z00
		(BgL_vallocz00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_prezd2valloczf2Cinfozd2keyzf2zzcfa_info3z00(BgL_vallocz00_bglt);
	static obj_t BGl_importedzd2moduleszd2initz00zzcfa_info3z00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_vrefzf2Cinfozd2effectz20zzcfa_info3z00(BgL_vrefz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_getfieldzf2Cinfozd2locz20zzcfa_info3z00(BgL_getfieldz00_bglt);
	static obj_t BGl_z62valloczf2Cinfozd2typezd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62valloczf2Cinfozb2optimzd2keyzf0zzcfa_info3z00(obj_t,
		obj_t);
	static obj_t BGl_z62pragmazf2Cinfozd2exprza2ze0zzcfa_info3z00(obj_t, obj_t);
	static BgL_vallocz00_bglt BGl_z62lambda2083z62zzcfa_info3z00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62instanceofzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62newzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_info3z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setfieldzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_info3z00
		(BgL_setfieldz00_bglt, obj_t);
	static BgL_vallocz00_bglt BGl_z62lambda2087z62zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_prezd2valloczf2Cinfozd2sidezd2effectzd2setz12ze0zzcfa_info3z00
		(BgL_vallocz00_bglt, obj_t);
	static obj_t
		BGl_z62setfieldzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static BgL_typez00_bglt
		BGl_z62getfieldzf2Cinfozd2otypez42zzcfa_info3z00(obj_t, obj_t);
	static BgL_typez00_bglt
		BGl_z62setfieldzf2Cinfozd2ftypez42zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_getfieldzf2Cinfozd2sidezd2effectzf2zzcfa_info3z00(BgL_getfieldz00_bglt);
	static obj_t BGl_z62vlengthzf2Cinfozd2tvectorzf3zb1zzcfa_info3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozd2keyzd2setz12ze0zzcfa_info3z00(BgL_vallocz00_bglt,
		obj_t);
	static BgL_vallocz00_bglt BGl_z62lambda2090z62zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozd2exprza2z82zzcfa_info3z00(BgL_vallocz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32089ze3ze5zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_vsetz12zf2Cinfozd2exprza2z90zzcfa_info3z00(BgL_vsetz12z12_bglt);
	static BgL_approxz00_bglt BGl_z62lambda2098z62zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62lambda2099z62zzcfa_info3z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62valloczf2Cinfozd2otypezd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62vrefzf2Cinfozd2czd2formatz90zzcfa_info3z00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62valloczf2Cinfozd2otypez42zzcfa_info3z00(obj_t,
		obj_t);
	static obj_t BGl_z62vlengthzf2Cinfozd2typezd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62vlengthzf2Cinfozd2sidezd2effectz90zzcfa_info3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_valloczf2Cinfozb2optimzd2ftypez92zzcfa_info3z00(BgL_vallocz00_bglt);
	extern obj_t BGl_instanceofz00zzast_nodez00;
	static obj_t BGl_z62valloczf2Cinfozd2czd2formatz90zzcfa_info3z00(obj_t,
		obj_t);
	static obj_t BGl_z62vsetz12zf2Cinfozd2czd2formatz82zzcfa_info3z00(obj_t,
		obj_t);
	static obj_t BGl_z62vsetz12zf2Cinfozd2typezd2setz12z90zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_getfieldzf2Cinfozd2ftypezd2setz12ze0zzcfa_info3z00(BgL_getfieldz00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_z62instanceofzf2Cinfozd2czd2formatz90zzcfa_info3z00(obj_t,
		obj_t);
	static obj_t BGl_z62newzf2Cinfozd2czd2formatz90zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setfieldzf2Cinfozd2czd2formatzf2zzcfa_info3z00(BgL_setfieldz00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_getfieldzf2Cinfozd2otypez20zzcfa_info3z00(BgL_getfieldz00_bglt);
	BGL_EXPORTED_DECL BgL_vrefz00_bglt
		BGl_vrefzf2Cinfozd2nilz20zzcfa_info3z00(void);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_setfieldzf2Cinfozd2ftypez20zzcfa_info3z00(BgL_setfieldz00_bglt);
	static BgL_typez00_bglt BGl_z62vrefzf2Cinfozd2typez42zzcfa_info3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_vlengthz00_bglt
		BGl_vlengthzf2Cinfozd2nilz20zzcfa_info3z00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozb2optimzd2effectz92zzcfa_info3z00(BgL_vallocz00_bglt);
	static obj_t BGl_z62vlengthzf2Cinfozd2ftypez42zzcfa_info3z00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62vsetz12zf2Cinfozd2typez50zzcfa_info3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_vsetz12zf2Cinfozd2keyz32zzcfa_info3z00(BgL_vsetz12z12_bglt);
	static BgL_typez00_bglt BGl_z62vlengthzf2Cinfozd2vtypez42zzcfa_info3z00(obj_t,
		obj_t);
	static obj_t BGl_z62valloczf2Cinfozf3z63zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_info3z00
		(BgL_vallocz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_vsetz12zf2Cinfozd2czd2formatze0zzcfa_info3z00(BgL_vsetz12z12_bglt);
	static obj_t BGl_z62vlengthzf2Cinfozd2exprza2zd2setz12z20zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_setfieldz00zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_vrefzf2Cinfozd2locz20zzcfa_info3z00(BgL_vrefz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_vlengthzf2Cinfozd2locz20zzcfa_info3z00(BgL_vlengthz00_bglt);
	static obj_t BGl_z62vsetz12zf2Cinfozd2vtypezd2setz12z90zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62vrefzf2Cinfozd2keyzd2setz12z82zzcfa_info3z00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62valloczf2Cinfozb2optimzd2stackablezf3zd2setz12zc3zzcfa_info3z00
		(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_vrefzf2Cinfozd2otypez20zzcfa_info3z00(BgL_vrefz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_vsetz12zf2Cinfozd2exprza2zd2setz12z50zzcfa_info3z00(BgL_vsetz12z12_bglt,
		obj_t);
	static obj_t BGl_z62vlengthzf2Cinfozd2keyz42zzcfa_info3z00(obj_t, obj_t);
	static obj_t BGl_z62valloczf2Cinfozd2keyzd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62vsetz12zf2Cinfozd2keyzd2setz12z90zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_castzd2nullzf2Cinfozd2sidezd2effectzd2setz12ze0zzcfa_info3z00
		(BgL_castzd2nullzd2_bglt, obj_t);
	static obj_t BGl_z62castzd2nullzf2Cinfozd2czd2formatz42zzcfa_info3z00(obj_t,
		obj_t);
	static obj_t BGl_z62instanceofzf2Cinfozd2keyzd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62newzf2Cinfozd2keyzd2setz12z82zzcfa_info3z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setfieldzf2Cinfozd2keyzd2setz12ze0zzcfa_info3z00(BgL_setfieldz00_bglt,
		obj_t);
	static obj_t BGl_z62vsetz12zf2Cinfozd2otypezd2setz12z90zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_pragmazf2Cinfozd2keyz20zzcfa_info3z00(BgL_pragmaz00_bglt);
	static BgL_approxz00_bglt
		BGl_z62vsetz12zf2Cinfozd2approxz50zzcfa_info3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozd2effectzd2setz12ze0zzcfa_info3z00(BgL_vallocz00_bglt,
		obj_t);
	static obj_t BGl_z62getfieldzf2Cinfozd2keyz42zzcfa_info3z00(obj_t, obj_t);
	static BgL_typez00_bglt
		BGl_z62castzd2nullzf2Cinfozd2typez90zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_newzf2Cinfozd2argszd2typezd2setz12z32zzcfa_info3z00(BgL_newz00_bglt,
		obj_t);
	static BgL_typez00_bglt BGl_z62vrefzf2Cinfozd2ftypez42zzcfa_info3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_setfieldzf2Cinfozf2zzcfa_info3z00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_vsetz12zf2Cinfozd2keyzd2setz12zf2zzcfa_info3z00(BgL_vsetz12z12_bglt,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31932ze3ze5zzcfa_info3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_newz00_bglt
		BGl_makezd2newzf2Cinfoz20zzcfa_info3z00(obj_t, BgL_typez00_bglt, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, BgL_approxz00_bglt);
	static BgL_instanceofz00_bglt BGl_z62lambda1903z62zzcfa_info3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_valloczf2Cinfozd2keyz20zzcfa_info3z00(BgL_vallocz00_bglt);
	static BgL_instanceofz00_bglt BGl_z62lambda1906z62zzcfa_info3z00(obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62getfieldzf2Cinfozd2typez42zzcfa_info3z00(obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62vrefzf2Cinfozd2vtypez42zzcfa_info3z00(obj_t,
		obj_t);
	static obj_t
		BGl_z62vrefzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_info3z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_vlengthz00_bglt
		BGl_makezd2vlengthzf2Cinfoz20zzcfa_info3z00(obj_t, BgL_typez00_bglt, obj_t,
		obj_t, obj_t, obj_t, obj_t, BgL_typez00_bglt, obj_t, BgL_approxz00_bglt,
		bool_t);
	static obj_t BGl_z62vrefzf2Cinfozd2sidezd2effectz90zzcfa_info3z00(obj_t,
		obj_t);
	static obj_t
		BGl_z62vsetz12zf2Cinfozd2sidezd2effectzd2setz12z42zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_instanceofzf2Cinfozd2approxz20zzcfa_info3z00(BgL_instanceofz00_bglt);
	BGL_EXPORTED_DEF obj_t BGl_valloczf2Cinfozb2optimz40zzcfa_info3z00 = BUNSPEC;
	static BgL_approxz00_bglt BGl_z62lambda1917z62zzcfa_info3z00(obj_t, obj_t);
	static BgL_pragmaz00_bglt BGl_z62lambda1756z62zzcfa_info3z00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1918z62zzcfa_info3z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_castzd2nullzf2Cinfozd2typezf2zzcfa_info3z00(BgL_castzd2nullzd2_bglt);
	static obj_t
		BGl_z62prezd2valloczf2Cinfozd2typezd2setz12z50zzcfa_info3z00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_vsetz12z12zzast_nodez00;
	static obj_t BGl_z62castzd2nullzf2Cinfozd2keyzd2setz12z50zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62vsetz12zf2Cinfozd2tvectorzf3zd2setz12z63zzcfa_info3z00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62instanceofzf2Cinfozd2typezd2setz12z82zzcfa_info3z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62newzf2Cinfozd2typezd2setz12z82zzcfa_info3z00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setfieldzf2Cinfozd2typezd2setz12ze0zzcfa_info3z00(BgL_setfieldz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_vrefzf2Cinfozd2czd2formatzf2zzcfa_info3z00(BgL_vrefz00_bglt);
	static BgL_pragmaz00_bglt BGl_z62lambda1763z62zzcfa_info3z00(obj_t, obj_t);
	static obj_t __cnst[26];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_vrefzf2Cinfozf3zd2envzd3zzcfa_info3z00,
		BgL_bgl_za762vrefza7f2cinfoza72368za7,
		BGl_z62vrefzf2Cinfozf3z63zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_instanceofzf2Cinfozd2typezd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762instanceofza7f22369z00,
		BGl_z62instanceofzf2Cinfozd2typezd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2nilzd2envz40zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2370z00,
		BGl_z62valloczf2Cinfozb2optimzd2nilzf0zzcfa_info3z00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_instanceofzf2Cinfozf3zd2envzd3zzcfa_info3z00,
		BgL_bgl_za762instanceofza7f22371z00,
		BGl_z62instanceofzf2Cinfozf3z63zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2valloczf2Cinfozd2ftypezd2envz20zzcfa_info3z00,
		BgL_bgl_za762preza7d2vallocza72372za7,
		BGl_z62prezd2valloczf2Cinfozd2ftypez90zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vrefzf2Cinfozd2effectzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762vrefza7f2cinfoza72373za7,
		BGl_z62vrefzf2Cinfozd2effectz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_instanceofzf2Cinfozd2sidezd2effectzd2setz12zd2envze0zzcfa_info3z00,
		BgL_bgl_za762instanceofza7f22374z00,
		BGl_z62instanceofzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setfieldzf2Cinfozd2approxzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762setfieldza7f2ci2375z00,
		BGl_z62setfieldzf2Cinfozd2approxz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_instanceofzf2Cinfozd2nilzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762instanceofza7f22376z00,
		BGl_z62instanceofzf2Cinfozd2nilz42zzcfa_info3z00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_castzd2nullzf2Cinfozd2nilzd2envz20zzcfa_info3z00,
		BgL_bgl_za762castza7d2nullza7f2377za7,
		BGl_z62castzd2nullzf2Cinfozd2nilz90zzcfa_info3z00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2valloczf2Cinfozd2ftypezd2setz12zd2envze0zzcfa_info3z00,
		BgL_bgl_za762preza7d2vallocza72378za7,
		BGl_z62prezd2valloczf2Cinfozd2ftypezd2setz12z50zzcfa_info3z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_valloczf2Cinfozd2nilzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2379z00,
		BGl_z62valloczf2Cinfozd2nilz42zzcfa_info3z00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vlengthzf2Cinfozd2typezd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762vlengthza7f2cin2380z00,
		BGl_z62vlengthzf2Cinfozd2typezd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vlengthzf2Cinfozd2sidezd2effectzd2envz20zzcfa_info3z00,
		BgL_bgl_za762vlengthza7f2cin2381z00,
		BGl_z62vlengthzf2Cinfozd2sidezd2effectz90zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vrefzf2Cinfozd2tvectorzf3zd2envz01zzcfa_info3z00,
		BgL_bgl_za762vrefza7f2cinfoza72382za7,
		BGl_z62vrefzf2Cinfozd2tvectorzf3zb1zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_newzf2Cinfozd2approxzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762newza7f2cinfoza7d2383za7,
		BGl_z62newzf2Cinfozd2approxz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vrefzf2Cinfozd2exprza2zd2setz12zd2envz90zzcfa_info3z00,
		BgL_bgl_za762vrefza7f2cinfoza72384za7,
		BGl_z62vrefzf2Cinfozd2exprza2zd2setz12z20zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vlengthzf2Cinfozd2effectzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762vlengthza7f2cin2385z00,
		BGl_z62vlengthzf2Cinfozd2effectz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_setfieldzf2Cinfozf3zd2envzd3zzcfa_info3z00,
		BgL_bgl_za762setfieldza7f2ci2386z00,
		BGl_z62setfieldzf2Cinfozf3z63zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_vlengthzf2Cinfozf3zd2envzd3zzcfa_info3z00,
		BgL_bgl_za762vlengthza7f2cin2387z00,
		BGl_z62vlengthzf2Cinfozf3z63zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getfieldzf2Cinfozd2fnamezd2envzf2zzcfa_info3z00,
		BgL_bgl_za762getfieldza7f2ci2388z00,
		BGl_z62getfieldzf2Cinfozd2fnamez42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setfieldzf2Cinfozd2loczd2envzf2zzcfa_info3z00,
		BgL_bgl_za762setfieldza7f2ci2389z00,
		BGl_z62setfieldzf2Cinfozd2locz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_newzf2Cinfozd2exprza2zd2envz50zzcfa_info3z00,
		BgL_bgl_za762newza7f2cinfoza7d2390za7,
		BGl_z62newzf2Cinfozd2exprza2ze0zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2valloczf2Cinfozd2sidezd2effectzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762preza7d2vallocza72391za7,
		BGl_z62prezd2valloczf2Cinfozd2sidezd2effectz42zzcfa_info3z00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setfieldzf2Cinfozd2keyzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762setfieldza7f2ci2392z00,
		BGl_z62setfieldzf2Cinfozd2keyz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2effectzd2setz12zd2envz80zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2393z00,
		BGl_z62valloczf2Cinfozb2optimzd2effectzd2setz12z30zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vsetz12zf2Cinfozd2nilzd2envze0zzcfa_info3z00,
		BgL_bgl_za762vsetza712za7f2cin2394za7,
		BGl_z62vsetz12zf2Cinfozd2nilz50zzcfa_info3z00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vrefzf2Cinfozd2sidezd2effectzd2envz20zzcfa_info3z00,
		BgL_bgl_za762vrefza7f2cinfoza72395za7,
		BGl_z62vrefzf2Cinfozd2sidezd2effectz90zzcfa_info3z00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2267z00zzcfa_info3z00,
		BgL_bgl_za762lambda1801za7622396z00, BGl_z62lambda1801z62zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2268z00zzcfa_info3z00,
		BgL_bgl_za762lambda1800za7622397z00, BGl_z62lambda1800z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2269z00zzcfa_info3z00,
		BgL_bgl_za762lambda1767za7622398z00, BGl_z62lambda1767z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vrefzf2Cinfozd2unsafezd2envzf2zzcfa_info3z00,
		BgL_bgl_za762vrefza7f2cinfoza72399za7,
		BGl_z62vrefzf2Cinfozd2unsafez42zzcfa_info3z00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2270z00zzcfa_info3z00,
		BgL_bgl_za762za7c3za704anonymo2400za7,
		BGl_z62zc3z04anonymousza31766ze3ze5zzcfa_info3z00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2271z00zzcfa_info3z00,
		BgL_bgl_za762lambda1763za7622401z00, BGl_z62lambda1763z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2272z00zzcfa_info3z00,
		BgL_bgl_za762lambda1756za7622402z00, BGl_z62lambda1756z62zzcfa_info3z00, 0L,
		BUNSPEC, 9);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_newzf2Cinfozd2argszd2typezd2setz12zd2envze0zzcfa_info3z00,
		BgL_bgl_za762newza7f2cinfoza7d2403za7,
		BGl_z62newzf2Cinfozd2argszd2typezd2setz12z50zzcfa_info3z00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2273z00zzcfa_info3z00,
		BgL_bgl_za762lambda1845za7622404z00, BGl_z62lambda1845z62zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2274z00zzcfa_info3z00,
		BgL_bgl_za762lambda1844za7622405z00, BGl_z62lambda1844z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2275z00zzcfa_info3z00,
		BgL_bgl_za762lambda1836za7622406z00, BGl_z62lambda1836z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2276z00zzcfa_info3z00,
		BgL_bgl_za762za7c3za704anonymo2407za7,
		BGl_z62zc3z04anonymousza31835ze3ze5zzcfa_info3z00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2364z00zzcfa_info3z00,
		BgL_bgl_string2364za700za7za7c2408za7, "", 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2277z00zzcfa_info3z00,
		BgL_bgl_za762lambda1833za7622409z00, BGl_z62lambda1833z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2365z00zzcfa_info3z00,
		BgL_bgl_string2365za700za7za7c2410za7, "cfa_info3", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2278z00zzcfa_info3z00,
		BgL_bgl_za762lambda1823za7622411z00, BGl_z62lambda1823z62zzcfa_info3z00, 0L,
		BUNSPEC, 11);
	      DEFINE_STRING(BGl_string2366z00zzcfa_info3z00,
		BgL_bgl_string2366za700za7za7c2412za7,
		"_ valloc/Cinfo+optim seen? obj stack-stamp stackable? long lost-stamp value-approx valloc/Cinfo pre-valloc/Cinfo owner vlength/Cinfo vset!/Cinfo vref/Cinfo bool tvector? cast-null/Cinfo instanceof/Cinfo new/Cinfo setfield/Cinfo getfield/Cinfo cfa_info3 pragma/Cinfo approx bigloo-c ",
		282);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2279z00zzcfa_info3z00,
		BgL_bgl_za762lambda1868za7622413z00, BGl_z62lambda1868z62zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_newzf2Cinfozd2keyzd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762newza7f2cinfoza7d2414za7,
		BGl_z62newzf2Cinfozd2keyzd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2prezd2valloczf2Cinfozd2envz20zzcfa_info3z00,
		BgL_bgl_za762makeza7d2preza7d22415za7,
		BGl_z62makezd2prezd2valloczf2Cinfoz90zzcfa_info3z00, 0L, BUNSPEC, 10);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2valloczf2Cinfozd2typezd2envz20zzcfa_info3z00,
		BgL_bgl_za762preza7d2vallocza72416za7,
		BGl_z62prezd2valloczf2Cinfozd2typez90zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vsetz12zf2Cinfozd2approxzd2envze0zzcfa_info3z00,
		BgL_bgl_za762vsetza712za7f2cin2417za7,
		BGl_z62vsetz12zf2Cinfozd2approxz50zzcfa_info3z00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2280z00zzcfa_info3z00,
		BgL_bgl_za762lambda1867za7622418z00, BGl_z62lambda1867z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2281z00zzcfa_info3z00,
		BgL_bgl_za762lambda1858za7622419z00, BGl_z62lambda1858z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2282z00zzcfa_info3z00,
		BgL_bgl_za762za7c3za704anonymo2420za7,
		BGl_z62zc3z04anonymousza31857ze3ze5zzcfa_info3z00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2283z00zzcfa_info3z00,
		BgL_bgl_za762lambda1855za7622421z00, BGl_z62lambda1855z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getfieldzf2Cinfozd2loczd2envzf2zzcfa_info3z00,
		BgL_bgl_za762getfieldza7f2ci2422z00,
		BGl_z62getfieldzf2Cinfozd2locz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2284z00zzcfa_info3z00,
		BgL_bgl_za762lambda1852za7622423z00, BGl_z62lambda1852z62zzcfa_info3z00, 0L,
		BUNSPEC, 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2285z00zzcfa_info3z00,
		BgL_bgl_za762lambda1891za7622424z00, BGl_z62lambda1891z62zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2286z00zzcfa_info3z00,
		BgL_bgl_za762lambda1890za7622425z00, BGl_z62lambda1890z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2287z00zzcfa_info3z00,
		BgL_bgl_za762lambda1882za7622426z00, BGl_z62lambda1882z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2valloczf2Cinfozd2otypezd2envz20zzcfa_info3z00,
		BgL_bgl_za762preza7d2vallocza72427za7,
		BGl_z62prezd2valloczf2Cinfozd2otypez90zzcfa_info3z00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2288z00zzcfa_info3z00,
		BgL_bgl_za762za7c3za704anonymo2428za7,
		BGl_z62zc3z04anonymousza31881ze3ze5zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2valloczf2Cinfozf3zd2envz01zzcfa_info3z00,
		BgL_bgl_za762preza7d2vallocza72429za7,
		BGl_z62prezd2valloczf2Cinfozf3zb1zzcfa_info3z00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2289z00zzcfa_info3z00,
		BgL_bgl_za762lambda1879za7622430z00, BGl_z62lambda1879z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getfieldzf2Cinfozd2keyzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762getfieldza7f2ci2431z00,
		BGl_z62getfieldzf2Cinfozd2keyz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2czd2formatzd2envz92zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2432z00,
		BGl_z62valloczf2Cinfozb2optimzd2czd2formatz22zzcfa_info3z00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_vrefzf2Cinfozd2loczd2envzf2zzcfa_info3z00,
		BgL_bgl_za762vrefza7f2cinfoza72433za7,
		BGl_z62vrefzf2Cinfozd2locz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_vrefzf2Cinfozd2keyzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762vrefza7f2cinfoza72434za7,
		BGl_z62vrefzf2Cinfozd2keyz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2290z00zzcfa_info3z00,
		BgL_bgl_za762lambda1876za7622435z00, BGl_z62lambda1876z62zzcfa_info3z00, 0L,
		BUNSPEC, 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2291z00zzcfa_info3z00,
		BgL_bgl_za762lambda1918za7622436z00, BGl_z62lambda1918z62zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2292z00zzcfa_info3z00,
		BgL_bgl_za762lambda1917za7622437z00, BGl_z62lambda1917z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2293z00zzcfa_info3z00,
		BgL_bgl_za762lambda1906za7622438z00, BGl_z62lambda1906z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2294z00zzcfa_info3z00,
		BgL_bgl_za762za7c3za704anonymo2439za7,
		BGl_z62zc3z04anonymousza31905ze3ze5zzcfa_info3z00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2295z00zzcfa_info3z00,
		BgL_bgl_za762lambda1903za7622440z00, BGl_z62lambda1903z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2296z00zzcfa_info3z00,
		BgL_bgl_za762lambda1899za7622441z00, BGl_z62lambda1899z62zzcfa_info3z00, 0L,
		BUNSPEC, 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2297z00zzcfa_info3z00,
		BgL_bgl_za762lambda1941za7622442z00, BGl_z62lambda1941z62zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2298z00zzcfa_info3z00,
		BgL_bgl_za762lambda1940za7622443z00, BGl_z62lambda1940z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2299z00zzcfa_info3z00,
		BgL_bgl_za762lambda1933za7622444z00, BGl_z62lambda1933z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_instanceofzf2Cinfozd2exprza2zd2setz12zd2envz90zzcfa_info3z00,
		BgL_bgl_za762instanceofza7f22445z00,
		BGl_z62instanceofzf2Cinfozd2exprza2zd2setz12z20zzcfa_info3z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setfieldzf2Cinfozd2exprza2zd2setz12zd2envz90zzcfa_info3z00,
		BgL_bgl_za762setfieldza7f2ci2446z00,
		BGl_z62setfieldzf2Cinfozd2exprza2zd2setz12z20zzcfa_info3z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_pragmazf2Cinfozd2loczd2envzf2zzcfa_info3z00,
		BgL_bgl_za762pragmaza7f2cinf2447z00,
		BGl_z62pragmazf2Cinfozd2locz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2valloczf2Cinfozd2nilzd2envz20zzcfa_info3z00,
		BgL_bgl_za762preza7d2vallocza72448za7,
		BGl_z62prezd2valloczf2Cinfozd2nilz90zzcfa_info3z00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vsetz12zf2Cinfozd2sidezd2effectzd2setz12zd2envzf2zzcfa_info3z00,
		BgL_bgl_za762vsetza712za7f2cin2449za7,
		BGl_z62vsetz12zf2Cinfozd2sidezd2effectzd2setz12z42zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_pragmazf2Cinfozd2keyzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762pragmaza7f2cinf2450z00,
		BGl_z62pragmazf2Cinfozd2keyz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_castzd2nullzf2Cinfozd2keyzd2setz12zd2envze0zzcfa_info3z00,
		BgL_bgl_za762castza7d2nullza7f2451za7,
		BGl_z62castzd2nullzf2Cinfozd2keyzd2setz12z50zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vlengthzf2Cinfozd2ftypezd2envzf2zzcfa_info3z00,
		BgL_bgl_za762vlengthza7f2cin2452z00,
		BGl_z62vlengthzf2Cinfozd2ftypez42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2effectzd2envz40zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2453z00,
		BGl_z62valloczf2Cinfozb2optimzd2effectzf0zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozd2exprza2zd2setz12zd2envz90zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2454z00,
		BGl_z62valloczf2Cinfozd2exprza2zd2setz12z20zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vlengthzf2Cinfozd2vtypezd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762vlengthza7f2cin2455z00,
		BGl_z62vlengthzf2Cinfozd2vtypezd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vlengthzf2Cinfozd2vtypezd2envzf2zzcfa_info3z00,
		BgL_bgl_za762vlengthza7f2cin2456z00,
		BGl_z62vlengthzf2Cinfozd2vtypez42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getfieldzf2Cinfozd2sidezd2effectzd2setz12zd2envze0zzcfa_info3z00,
		BgL_bgl_za762getfieldza7f2ci2457z00,
		BGl_z62getfieldzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vlengthzf2Cinfozd2effectzd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762vlengthza7f2cin2458z00,
		BGl_z62vlengthzf2Cinfozd2effectzd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_vrefzf2Cinfozd2ftypezd2envzf2zzcfa_info3z00,
		BgL_bgl_za762vrefza7f2cinfoza72459za7,
		BGl_z62vrefzf2Cinfozd2ftypez42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_castzd2nullzf2Cinfozd2exprza2zd2envz82zzcfa_info3z00,
		BgL_bgl_za762castza7d2nullza7f2460za7,
		BGl_z62castzd2nullzf2Cinfozd2exprza2z32zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getfieldzf2Cinfozd2sidezd2effectzd2envz20zzcfa_info3z00,
		BgL_bgl_za762getfieldza7f2ci2461z00,
		BGl_z62getfieldzf2Cinfozd2sidezd2effectz90zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_newzf2Cinfozd2sidezd2effectzd2envz20zzcfa_info3z00,
		BgL_bgl_za762newza7f2cinfoza7d2462za7,
		BGl_z62newzf2Cinfozd2sidezd2effectz90zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_vrefzf2Cinfozd2vtypezd2envzf2zzcfa_info3z00,
		BgL_bgl_za762vrefza7f2cinfoza72463za7,
		BGl_z62vrefzf2Cinfozd2vtypez42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2ownerzd2envz40zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2464z00,
		BGl_z62valloczf2Cinfozb2optimzd2ownerzf0zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getfieldzf2Cinfozd2ftypezd2envzf2zzcfa_info3z00,
		BgL_bgl_za762getfieldza7f2ci2465z00,
		BGl_z62getfieldzf2Cinfozd2ftypez42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vsetz12zf2Cinfozd2vtypezd2setz12zd2envz20zzcfa_info3z00,
		BgL_bgl_za762vsetza712za7f2cin2466za7,
		BGl_z62vsetz12zf2Cinfozd2vtypezd2setz12z90zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2valloczf2Cinfozd2effectzd2setz12zd2envze0zzcfa_info3z00,
		BgL_bgl_za762preza7d2vallocza72467za7,
		BGl_z62prezd2valloczf2Cinfozd2effectzd2setz12z50zzcfa_info3z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozd2ftypezd2envzf2zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2468z00,
		BGl_z62valloczf2Cinfozd2ftypez42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vrefzf2Cinfozd2sidezd2effectzd2setz12zd2envze0zzcfa_info3z00,
		BgL_bgl_za762vrefza7f2cinfoza72469za7,
		BGl_z62vrefzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_info3z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getfieldzf2Cinfozd2otypezd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762getfieldza7f2ci2470z00,
		BGl_z62getfieldzf2Cinfozd2otypezd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vlengthzf2Cinfozd2typezd2envzf2zzcfa_info3z00,
		BgL_bgl_za762vlengthza7f2cin2471z00,
		BGl_z62vlengthzf2Cinfozd2typez42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vrefzf2Cinfozd2effectzd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762vrefza7f2cinfoza72472za7,
		BGl_z62vrefzf2Cinfozd2effectzd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2exprza2zd2envze2zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2473z00,
		BGl_z62valloczf2Cinfozb2optimzd2exprza2z52zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_pragmazf2Cinfozd2effectzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762pragmaza7f2cinf2474z00,
		BGl_z62pragmazf2Cinfozd2effectz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozd2effectzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2475z00,
		BGl_z62valloczf2Cinfozd2effectz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozd2keyzd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2476z00,
		BGl_z62valloczf2Cinfozd2keyzd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2lostzd2stampzd2envz92zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2477z00,
		BGl_z62valloczf2Cinfozb2optimzd2lostzd2stampz22zzcfa_info3z00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_pragmazf2Cinfozf3zd2envzd3zzcfa_info3z00,
		BgL_bgl_za762pragmaza7f2cinf2478z00,
		BGl_z62pragmazf2Cinfozf3z63zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setfieldzf2Cinfozd2keyzd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762setfieldza7f2ci2479z00,
		BGl_z62setfieldzf2Cinfozd2keyzd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vlengthzf2Cinfozd2keyzd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762vlengthza7f2cin2480z00,
		BGl_z62vlengthzf2Cinfozd2keyzd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_pragmazf2Cinfozd2exprza2zd2envz50zzcfa_info3z00,
		BgL_bgl_za762pragmaza7f2cinf2481z00,
		BGl_z62pragmazf2Cinfozd2exprza2ze0zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2valloczf2Cinfozd2czd2formatzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762preza7d2vallocza72482za7,
		BGl_z62prezd2valloczf2Cinfozd2czd2formatz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2stackablezf3zd2setz12zd2envz73zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2483z00,
		BGl_z62valloczf2Cinfozb2optimzd2stackablezf3zd2setz12zc3zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setfieldzf2Cinfozd2effectzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762setfieldza7f2ci2484z00,
		BGl_z62setfieldzf2Cinfozd2effectz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2approxzd2setz12zd2envz80zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2485z00,
		BGl_z62valloczf2Cinfozb2optimzd2approxzd2setz12z30zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_instanceofzf2Cinfozd2typezd2envzf2zzcfa_info3z00,
		BgL_bgl_za762instanceofza7f22486z00,
		BGl_z62instanceofzf2Cinfozd2typez42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_castzd2nullzf2Cinfozd2approxzd2envz20zzcfa_info3z00,
		BgL_bgl_za762castza7d2nullza7f2487za7,
		BGl_z62castzd2nullzf2Cinfozd2approxz90zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2vsetz12zf2Cinfozd2envze0zzcfa_info3z00,
		BgL_bgl_za762makeza7d2vsetza712488za7,
		BGl_z62makezd2vsetz12zf2Cinfoz50zzcfa_info3z00, 0L, BUNSPEC, 13);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_newzf2Cinfozd2effectzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762newza7f2cinfoza7d2489za7,
		BGl_z62newzf2Cinfozd2effectz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vlengthzf2Cinfozd2tvectorzf3zd2setz12zd2envzc1zzcfa_info3z00,
		BgL_bgl_za762vlengthza7f2cin2490z00,
		BGl_z62vlengthzf2Cinfozd2tvectorzf3zd2setz12z71zzcfa_info3z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2valloczf2Cinfozd2effectzd2envz20zzcfa_info3z00,
		BgL_bgl_za762preza7d2vallocza72491za7,
		BGl_z62prezd2valloczf2Cinfozd2effectz90zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_vrefzf2Cinfozd2otypezd2envzf2zzcfa_info3z00,
		BgL_bgl_za762vrefza7f2cinfoza72492za7,
		BGl_z62vrefzf2Cinfozd2otypez42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_castzd2nullzf2Cinfozd2typezd2setz12zd2envze0zzcfa_info3z00,
		BgL_bgl_za762castza7d2nullza7f2493za7,
		BGl_z62castzd2nullzf2Cinfozd2typezd2setz12z50zzcfa_info3z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2exprza2zd2setz12zd2envz22zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2494z00,
		BGl_z62valloczf2Cinfozb2optimzd2exprza2zd2setz12z92zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setfieldzf2Cinfozd2nilzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762setfieldza7f2ci2495z00,
		BGl_z62setfieldzf2Cinfozd2nilz42zzcfa_info3z00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getfieldzf2Cinfozd2otypezd2envzf2zzcfa_info3z00,
		BgL_bgl_za762getfieldza7f2ci2496z00,
		BGl_z62getfieldzf2Cinfozd2otypez42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_instanceofzf2Cinfozd2classzd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762instanceofza7f22497z00,
		BGl_z62instanceofzf2Cinfozd2classzd2setz12z82zzcfa_info3z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vsetz12zf2Cinfozd2ftypezd2envze0zzcfa_info3z00,
		BgL_bgl_za762vsetza712za7f2cin2498za7,
		BGl_z62vsetz12zf2Cinfozd2ftypez50zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getfieldzf2Cinfozd2effectzd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762getfieldza7f2ci2499z00,
		BGl_z62getfieldzf2Cinfozd2effectzd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_newzf2Cinfozd2effectzd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762newza7f2cinfoza7d2500za7,
		BGl_z62newzf2Cinfozd2effectzd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozd2otypezd2envzf2zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2501z00,
		BGl_z62valloczf2Cinfozd2otypez42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vsetz12zf2Cinfozd2vtypezd2envze0zzcfa_info3z00,
		BgL_bgl_za762vsetza712za7f2cin2502za7,
		BGl_z62vsetz12zf2Cinfozd2vtypez50zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2valloczf2Cinfozd2ownerzd2envz20zzcfa_info3z00,
		BgL_bgl_za762preza7d2vallocza72503za7,
		BGl_z62prezd2valloczf2Cinfozd2ownerz90zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_instanceofzf2Cinfozd2approxzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762instanceofza7f22504z00,
		BGl_z62instanceofzf2Cinfozd2approxz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_newzf2Cinfozd2loczd2envzf2zzcfa_info3z00,
		BgL_bgl_za762newza7f2cinfoza7d2505za7,
		BGl_z62newzf2Cinfozd2locz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_newzf2Cinfozd2keyzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762newza7f2cinfoza7d2506za7,
		BGl_z62newzf2Cinfozd2keyz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setfieldzf2Cinfozd2fnamezd2envzf2zzcfa_info3z00,
		BgL_bgl_za762setfieldza7f2ci2507z00,
		BGl_z62setfieldzf2Cinfozd2fnamez42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2stackzd2stampzd2envz92zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2508z00,
		BGl_z62valloczf2Cinfozb2optimzd2stackzd2stampz22zzcfa_info3z00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vrefzf2Cinfozd2otypezd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762vrefza7f2cinfoza72509za7,
		BGl_z62vrefzf2Cinfozd2otypezd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getfieldzf2Cinfozd2keyzd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762getfieldza7f2ci2510z00,
		BGl_z62getfieldzf2Cinfozd2keyzd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vlengthzf2Cinfozd2exprza2zd2setz12zd2envz90zzcfa_info3z00,
		BgL_bgl_za762vlengthza7f2cin2511z00,
		BGl_z62vlengthzf2Cinfozd2exprza2zd2setz12z20zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2valloczf2Cinfozd2exprza2zd2envz82zzcfa_info3z00,
		BgL_bgl_za762preza7d2vallocza72512za7,
		BGl_z62prezd2valloczf2Cinfozd2exprza2z32zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getfieldzf2Cinfozd2ftypezd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762getfieldza7f2ci2513z00,
		BGl_z62getfieldzf2Cinfozd2ftypezd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getfieldzf2Cinfozd2nilzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762getfieldza7f2ci2514z00,
		BGl_z62getfieldzf2Cinfozd2nilz42zzcfa_info3z00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vsetz12zf2Cinfozd2effectzd2envze0zzcfa_info3z00,
		BgL_bgl_za762vsetza712za7f2cin2515za7,
		BGl_z62vsetz12zf2Cinfozd2effectz50zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_instanceofzf2Cinfozd2exprza2zd2envz50zzcfa_info3z00,
		BgL_bgl_za762instanceofza7f22516z00,
		BGl_z62instanceofzf2Cinfozd2exprza2ze0zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_vrefzf2Cinfozd2nilzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762vrefza7f2cinfoza72517za7,
		BGl_z62vrefzf2Cinfozd2nilz42zzcfa_info3z00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_pragmazf2Cinfozd2nilzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762pragmaza7f2cinf2518z00,
		BGl_z62pragmazf2Cinfozd2nilz42zzcfa_info3z00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozd2sidezd2effectzd2envz20zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2519z00,
		BGl_z62valloczf2Cinfozd2sidezd2effectz90zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_pragmazf2Cinfozd2formatzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762pragmaza7f2cinf2520z00,
		BGl_z62pragmazf2Cinfozd2formatz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_newzf2Cinfozd2czd2formatzd2envz20zzcfa_info3z00,
		BgL_bgl_za762newza7f2cinfoza7d2521za7,
		BGl_z62newzf2Cinfozd2czd2formatz90zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_castzd2nullzf2Cinfozd2typezd2envz20zzcfa_info3z00,
		BgL_bgl_za762castza7d2nullza7f2522za7,
		BGl_z62castzd2nullzf2Cinfozd2typez90zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vlengthzf2Cinfozd2loczd2envzf2zzcfa_info3z00,
		BgL_bgl_za762vlengthza7f2cin2523z00,
		BGl_z62vlengthzf2Cinfozd2locz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_pragmazf2Cinfozd2typezd2envzf2zzcfa_info3z00,
		BgL_bgl_za762pragmaza7f2cinf2524z00,
		BGl_z62pragmazf2Cinfozd2typez42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vlengthzf2Cinfozd2keyzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762vlengthza7f2cin2525z00,
		BGl_z62vlengthzf2Cinfozd2keyz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vsetz12zf2Cinfozd2exprza2zd2setz12zd2envz82zzcfa_info3z00,
		BgL_bgl_za762vsetza712za7f2cin2526za7,
		BGl_z62vsetz12zf2Cinfozd2exprza2zd2setz12z32zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_instanceofzf2Cinfozd2keyzd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762instanceofza7f22527z00,
		BGl_z62instanceofzf2Cinfozd2keyzd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_pragmazf2Cinfozd2keyzd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762pragmaza7f2cinf2528z00,
		BGl_z62pragmazf2Cinfozd2keyzd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vsetz12zf2Cinfozd2keyzd2setz12zd2envz20zzcfa_info3z00,
		BgL_bgl_za762vsetza712za7f2cin2529za7,
		BGl_z62vsetz12zf2Cinfozd2keyzd2setz12z90zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_castzd2nullzf2Cinfozd2czd2formatzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762castza7d2nullza7f2530za7,
		BGl_z62castzd2nullzf2Cinfozd2czd2formatz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vsetz12zf2Cinfozd2otypezd2envze0zzcfa_info3z00,
		BgL_bgl_za762vsetza712za7f2cin2531za7,
		BGl_z62vsetz12zf2Cinfozd2otypez50zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2vlengthzf2Cinfozd2envzf2zzcfa_info3z00,
		BgL_bgl_za762makeza7d2vlengt2532z00,
		BGl_z62makezd2vlengthzf2Cinfoz42zzcfa_info3z00, 0L, BUNSPEC, 11);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2valloczf2Cinfozd2exprza2zd2setz12zd2envz42zzcfa_info3z00,
		BgL_bgl_za762preza7d2vallocza72533za7,
		BGl_z62prezd2valloczf2Cinfozd2exprza2zd2setz12zf2zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setfieldzf2Cinfozd2otypezd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762setfieldza7f2ci2534z00,
		BGl_z62setfieldzf2Cinfozd2otypezd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getfieldzf2Cinfozd2approxzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762getfieldza7f2ci2535z00,
		BGl_z62getfieldzf2Cinfozd2approxz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2typezd2setz12zd2envz80zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2536z00,
		BGl_z62valloczf2Cinfozb2optimzd2typezd2setz12z30zzcfa_info3z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vsetz12zf2Cinfozd2unsafezd2envze0zzcfa_info3z00,
		BgL_bgl_za762vsetza712za7f2cin2537za7,
		BGl_z62vsetz12zf2Cinfozd2unsafez50zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozd2otypezd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2538z00,
		BGl_z62valloczf2Cinfozd2otypezd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2pragmazf2Cinfozd2envzf2zzcfa_info3z00,
		BgL_bgl_za762makeza7d2pragma2539z00,
		BGl_z62makezd2pragmazf2Cinfoz42zzcfa_info3z00, 0L, BUNSPEC, 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2300z00zzcfa_info3z00,
		BgL_bgl_za762za7c3za704anonymo2540za7,
		BGl_z62zc3z04anonymousza31932ze3ze5zzcfa_info3z00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2301z00zzcfa_info3z00,
		BgL_bgl_za762lambda1930za7622541z00, BGl_z62lambda1930z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2302z00zzcfa_info3z00,
		BgL_bgl_za762lambda1927za7622542z00, BGl_z62lambda1927z62zzcfa_info3z00, 0L,
		BUNSPEC, 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2303z00zzcfa_info3z00,
		BgL_bgl_za762lambda1962za7622543z00, BGl_z62lambda1962z62zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2304z00zzcfa_info3z00,
		BgL_bgl_za762lambda1961za7622544z00, BGl_z62lambda1961z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2305z00zzcfa_info3z00,
		BgL_bgl_za762za7c3za704anonymo2545za7,
		BGl_z62zc3z04anonymousza31969ze3ze5zzcfa_info3z00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2306z00zzcfa_info3z00,
		BgL_bgl_za762lambda1968za7622546z00, BGl_z62lambda1968z62zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2307z00zzcfa_info3z00,
		BgL_bgl_za762lambda1967za7622547z00, BGl_z62lambda1967z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2308z00zzcfa_info3z00,
		BgL_bgl_za762lambda1954za7622548z00, BGl_z62lambda1954z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2309z00zzcfa_info3z00,
		BgL_bgl_za762za7c3za704anonymo2549za7,
		BGl_z62zc3z04anonymousza31953ze3ze5zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setfieldzf2Cinfozd2ftypezd2envzf2zzcfa_info3z00,
		BgL_bgl_za762setfieldza7f2ci2550z00,
		BGl_z62setfieldzf2Cinfozd2ftypez42zzcfa_info3z00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2310z00zzcfa_info3z00,
		BgL_bgl_za762lambda1951za7622551z00, BGl_z62lambda1951z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2311z00zzcfa_info3z00,
		BgL_bgl_za762lambda1948za7622552z00, BGl_z62lambda1948z62zzcfa_info3z00, 0L,
		BUNSPEC, 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2312z00zzcfa_info3z00,
		BgL_bgl_za762lambda1990za7622553z00, BGl_z62lambda1990z62zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2313z00zzcfa_info3z00,
		BgL_bgl_za762lambda1989za7622554z00, BGl_z62lambda1989z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2314z00zzcfa_info3z00,
		BgL_bgl_za762za7c3za704anonymo2555za7,
		BGl_z62zc3z04anonymousza31997ze3ze5zzcfa_info3z00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2315z00zzcfa_info3z00,
		BgL_bgl_za762lambda1996za7622556z00, BGl_z62lambda1996z62zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2316z00zzcfa_info3z00,
		BgL_bgl_za762lambda1995za7622557z00, BGl_z62lambda1995z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2valloczf2Cinfozd2envzf2zzcfa_info3z00,
		BgL_bgl_za762makeza7d2valloc2558z00,
		BGl_z62makezd2valloczf2Cinfoz42zzcfa_info3z00, 0L, BUNSPEC, 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2317z00zzcfa_info3z00,
		BgL_bgl_za762lambda1982za7622559z00, BGl_z62lambda1982z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2318z00zzcfa_info3z00,
		BgL_bgl_za762za7c3za704anonymo2560za7,
		BGl_z62zc3z04anonymousza31981ze3ze5zzcfa_info3z00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2319z00zzcfa_info3z00,
		BgL_bgl_za762lambda1979za7622561z00, BGl_z62lambda1979z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_instanceofzf2Cinfozd2sidezd2effectzd2envz20zzcfa_info3z00,
		BgL_bgl_za762instanceofza7f22562z00,
		BGl_z62instanceofzf2Cinfozd2sidezd2effectz90zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vrefzf2Cinfozd2ftypezd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762vrefza7f2cinfoza72563za7,
		BGl_z62vrefzf2Cinfozd2ftypezd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2seenzf3zd2setz12zd2envz73zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2564z00,
		BGl_z62valloczf2Cinfozb2optimzd2seenzf3zd2setz12zc3zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2320z00zzcfa_info3z00,
		BgL_bgl_za762lambda1976za7622565z00, BGl_z62lambda1976z62zzcfa_info3z00, 0L,
		BUNSPEC, 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2321z00zzcfa_info3z00,
		BgL_bgl_za762lambda2019za7622566z00, BGl_z62lambda2019z62zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2322z00zzcfa_info3z00,
		BgL_bgl_za762lambda2018za7622567z00, BGl_z62lambda2018z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2323z00zzcfa_info3z00,
		BgL_bgl_za762za7c3za704anonymo2568za7,
		BGl_z62zc3z04anonymousza32027ze3ze5zzcfa_info3z00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2324z00zzcfa_info3z00,
		BgL_bgl_za762lambda2026za7622569z00, BGl_z62lambda2026z62zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2325z00zzcfa_info3z00,
		BgL_bgl_za762lambda2025za7622570z00, BGl_z62lambda2025z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2326z00zzcfa_info3z00,
		BgL_bgl_za762lambda2011za7622571z00, BGl_z62lambda2011z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2327z00zzcfa_info3z00,
		BgL_bgl_za762za7c3za704anonymo2572za7,
		BGl_z62zc3z04anonymousza32010ze3ze5zzcfa_info3z00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2328z00zzcfa_info3z00,
		BgL_bgl_za762lambda2008za7622573z00, BGl_z62lambda2008z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2329z00zzcfa_info3z00,
		BgL_bgl_za762lambda2004za7622574z00, BGl_z62lambda2004z62zzcfa_info3z00, 0L,
		BUNSPEC, 11);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setfieldzf2Cinfozd2sidezd2effectzd2setz12zd2envze0zzcfa_info3z00,
		BgL_bgl_za762setfieldza7f2ci2575z00,
		BGl_z62setfieldzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vlengthzf2Cinfozd2sidezd2effectzd2setz12zd2envze0zzcfa_info3z00,
		BgL_bgl_za762vlengthza7f2cin2576z00,
		BGl_z62vlengthzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vrefzf2Cinfozd2keyzd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762vrefza7f2cinfoza72577za7,
		BGl_z62vrefzf2Cinfozd2keyzd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2330z00zzcfa_info3z00,
		BgL_bgl_za762lambda2051za7622578z00, BGl_z62lambda2051z62zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2331z00zzcfa_info3z00,
		BgL_bgl_za762lambda2050za7622579z00, BGl_z62lambda2050z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2332z00zzcfa_info3z00,
		BgL_bgl_za762lambda2043za7622580z00, BGl_z62lambda2043z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2333z00zzcfa_info3z00,
		BgL_bgl_za762za7c3za704anonymo2581za7,
		BGl_z62zc3z04anonymousza32042ze3ze5zzcfa_info3z00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2334z00zzcfa_info3z00,
		BgL_bgl_za762lambda2040za7622582z00, BGl_z62lambda2040z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2335z00zzcfa_info3z00,
		BgL_bgl_za762lambda2037za7622583z00, BGl_z62lambda2037z62zzcfa_info3z00, 0L,
		BUNSPEC, 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2336z00zzcfa_info3z00,
		BgL_bgl_za762lambda2076za7622584z00, BGl_z62lambda2076z62zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vlengthzf2Cinfozd2exprza2zd2envz50zzcfa_info3z00,
		BgL_bgl_za762vlengthza7f2cin2585z00,
		BGl_z62vlengthzf2Cinfozd2exprza2ze0zzcfa_info3z00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2337z00zzcfa_info3z00,
		BgL_bgl_za762lambda2075za7622586z00, BGl_z62lambda2075z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2338z00zzcfa_info3z00,
		BgL_bgl_za762lambda2066za7622587z00, BGl_z62lambda2066z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2339z00zzcfa_info3z00,
		BgL_bgl_za762za7c3za704anonymo2588za7,
		BGl_z62zc3z04anonymousza32065ze3ze5zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_castzd2nullzf2Cinfozf3zd2envz01zzcfa_info3z00,
		BgL_bgl_za762castza7d2nullza7f2589za7,
		BGl_z62castzd2nullzf2Cinfozf3zb1zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozd2czd2formatzd2envz20zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2590z00,
		BGl_z62valloczf2Cinfozd2czd2formatz90zzcfa_info3z00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2340z00zzcfa_info3z00,
		BgL_bgl_za762lambda2063za7622591z00, BGl_z62lambda2063z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2341z00zzcfa_info3z00,
		BgL_bgl_za762lambda2060za7622592z00, BGl_z62lambda2060z62zzcfa_info3z00, 0L,
		BUNSPEC, 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2342z00zzcfa_info3z00,
		BgL_bgl_za762lambda2099za7622593z00, BGl_z62lambda2099z62zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2343z00zzcfa_info3z00,
		BgL_bgl_za762lambda2098za7622594z00, BGl_z62lambda2098z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2344z00zzcfa_info3z00,
		BgL_bgl_za762lambda2104za7622595z00, BGl_z62lambda2104z62zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_castzd2nullzf2Cinfozd2effectzd2envz20zzcfa_info3z00,
		BgL_bgl_za762castza7d2nullza7f2596za7,
		BGl_z62castzd2nullzf2Cinfozd2effectz90zzcfa_info3z00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2345z00zzcfa_info3z00,
		BgL_bgl_za762lambda2103za7622597z00, BGl_z62lambda2103z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2346z00zzcfa_info3z00,
		BgL_bgl_za762za7c3za704anonymo2598za7,
		BGl_z62zc3z04anonymousza32111ze3ze5zzcfa_info3z00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2getfieldzf2Cinfozd2envzf2zzcfa_info3z00,
		BgL_bgl_za762makeza7d2getfie2599z00,
		BGl_z62makezd2getfieldzf2Cinfoz42zzcfa_info3z00, 0L, BUNSPEC, 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2347z00zzcfa_info3z00,
		BgL_bgl_za762lambda2110za7622600z00, BGl_z62lambda2110z62zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2348z00zzcfa_info3z00,
		BgL_bgl_za762lambda2109za7622601z00, BGl_z62lambda2109z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2349z00zzcfa_info3z00,
		BgL_bgl_za762lambda2116za7622602z00, BGl_z62lambda2116z62zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozd2effectzd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2603z00,
		BGl_z62valloczf2Cinfozd2effectzd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setfieldzf2Cinfozd2czd2formatzd2envz20zzcfa_info3z00,
		BgL_bgl_za762setfieldza7f2ci2604z00,
		BGl_z62setfieldzf2Cinfozd2czd2formatz90zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vlengthzf2Cinfozd2czd2formatzd2envz20zzcfa_info3z00,
		BgL_bgl_za762vlengthza7f2cin2605z00,
		BGl_z62vlengthzf2Cinfozd2czd2formatz90zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_pragmazf2Cinfozd2exprza2zd2setz12zd2envz90zzcfa_info3z00,
		BgL_bgl_za762pragmaza7f2cinf2606z00,
		BGl_z62pragmazf2Cinfozd2exprza2zd2setz12z20zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vrefzf2Cinfozd2exprza2zd2envz50zzcfa_info3z00,
		BgL_bgl_za762vrefza7f2cinfoza72607za7,
		BGl_z62vrefzf2Cinfozd2exprza2ze0zzcfa_info3z00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2350z00zzcfa_info3z00,
		BgL_bgl_za762lambda2115za7622608z00, BGl_z62lambda2115z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2351z00zzcfa_info3z00,
		BgL_bgl_za762za7c3za704anonymo2609za7,
		BGl_z62zc3z04anonymousza32123ze3ze5zzcfa_info3z00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2352z00zzcfa_info3z00,
		BgL_bgl_za762lambda2122za7622610z00, BGl_z62lambda2122z62zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2353z00zzcfa_info3z00,
		BgL_bgl_za762lambda2121za7622611z00, BGl_z62lambda2121z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_instanceofzf2Cinfozd2classzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762instanceofza7f22612z00,
		BGl_z62instanceofzf2Cinfozd2classz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2354z00zzcfa_info3z00,
		BgL_bgl_za762za7c3za704anonymo2613za7,
		BGl_z62zc3z04anonymousza32130ze3ze5zzcfa_info3z00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2355z00zzcfa_info3z00,
		BgL_bgl_za762lambda2129za7622614z00, BGl_z62lambda2129z62zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2356z00zzcfa_info3z00,
		BgL_bgl_za762lambda2128za7622615z00, BGl_z62lambda2128z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2357z00zzcfa_info3z00,
		BgL_bgl_za762za7c3za704anonymo2616za7,
		BGl_z62zc3z04anonymousza32137ze3ze5zzcfa_info3z00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2358z00zzcfa_info3z00,
		BgL_bgl_za762lambda2136za7622617z00, BGl_z62lambda2136z62zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2359z00zzcfa_info3z00,
		BgL_bgl_za762lambda2135za7622618z00, BGl_z62lambda2135z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getfieldzf2Cinfozd2typezd2envzf2zzcfa_info3z00,
		BgL_bgl_za762getfieldza7f2ci2619z00,
		BGl_z62getfieldzf2Cinfozd2typez42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2newzf2Cinfozd2envzf2zzcfa_info3z00,
		BgL_bgl_za762makeza7d2newza7f22620za7,
		BGl_z62makezd2newzf2Cinfoz42zzcfa_info3z00, 0L, BUNSPEC, 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2360z00zzcfa_info3z00,
		BgL_bgl_za762lambda2090za7622621z00, BGl_z62lambda2090z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2361z00zzcfa_info3z00,
		BgL_bgl_za762za7c3za704anonymo2622za7,
		BGl_z62zc3z04anonymousza32089ze3ze5zzcfa_info3z00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2362z00zzcfa_info3z00,
		BgL_bgl_za762lambda2087za7622623z00, BGl_z62lambda2087z62zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2363z00zzcfa_info3z00,
		BgL_bgl_za762lambda2083za7622624z00, BGl_z62lambda2083z62zzcfa_info3z00, 0L,
		BUNSPEC, 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getfieldzf2Cinfozd2exprza2zd2envz50zzcfa_info3z00,
		BgL_bgl_za762getfieldza7f2ci2625z00,
		BGl_z62getfieldzf2Cinfozd2exprza2ze0zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_castzd2nullzf2Cinfozd2exprza2zd2setz12zd2envz42zzcfa_info3z00,
		BgL_bgl_za762castza7d2nullza7f2626za7,
		BGl_z62castzd2nullzf2Cinfozd2exprza2zd2setz12zf2zzcfa_info3z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_newzf2Cinfozd2typezd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762newza7f2cinfoza7d2627za7,
		BGl_z62newzf2Cinfozd2typezd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozd2exprza2zd2envz50zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2628z00,
		BGl_z62valloczf2Cinfozd2exprza2ze0zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setfieldzf2Cinfozd2sidezd2effectzd2envz20zzcfa_info3z00,
		BgL_bgl_za762setfieldza7f2ci2629z00,
		BGl_z62setfieldzf2Cinfozd2sidezd2effectz90zzcfa_info3z00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_newzf2Cinfozf3zd2envzd3zzcfa_info3z00,
		BgL_bgl_za762newza7f2cinfoza7f2630za7,
		BGl_z62newzf2Cinfozf3z63zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozd2typezd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2631z00,
		BGl_z62valloczf2Cinfozd2typezd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_newzf2Cinfozd2sidezd2effectzd2setz12zd2envze0zzcfa_info3z00,
		BgL_bgl_za762newza7f2cinfoza7d2632za7,
		BGl_z62newzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_info3z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2instanceofzf2Cinfozd2envzf2zzcfa_info3z00,
		BgL_bgl_za762makeza7d2instan2633z00,
		BGl_z62makezd2instanceofzf2Cinfoz42zzcfa_info3z00, 0L, BUNSPEC, 9);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_newzf2Cinfozd2nilzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762newza7f2cinfoza7d2634za7,
		BGl_z62newzf2Cinfozd2nilz42zzcfa_info3z00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2valloczf2Cinfozd2typezd2setz12zd2envze0zzcfa_info3z00,
		BgL_bgl_za762preza7d2vallocza72635za7,
		BGl_z62prezd2valloczf2Cinfozd2typezd2setz12z50zzcfa_info3z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setfieldzf2Cinfozd2ftypezd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762setfieldza7f2ci2636z00,
		BGl_z62setfieldzf2Cinfozd2ftypezd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_instanceofzf2Cinfozd2effectzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762instanceofza7f22637z00,
		BGl_z62instanceofzf2Cinfozd2effectz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setfieldzf2Cinfozd2otypezd2envzf2zzcfa_info3z00,
		BgL_bgl_za762setfieldza7f2ci2638z00,
		BGl_z62setfieldzf2Cinfozd2otypez42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozd2typezd2envzf2zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2639z00,
		BGl_z62valloczf2Cinfozd2typez42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_castzd2nullzf2Cinfozd2sidezd2effectzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762castza7d2nullza7f2640za7,
		BGl_z62castzd2nullzf2Cinfozd2sidezd2effectz42zzcfa_info3z00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_getfieldzf2Cinfozf3zd2envzd3zzcfa_info3z00,
		BgL_bgl_za762getfieldza7f2ci2641z00,
		BGl_z62getfieldzf2Cinfozf3z63zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozd2sidezd2effectzd2setz12zd2envze0zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2642z00,
		BGl_z62valloczf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2otypezd2setz12zd2envz80zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2643z00,
		BGl_z62valloczf2Cinfozb2optimzd2otypezd2setz12z30zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozd2ftypezd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2644z00,
		BGl_z62valloczf2Cinfozd2ftypezd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2stackablezf3zd2envzb3zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2645z00,
		BGl_z62valloczf2Cinfozb2optimzd2stackablezf3z03zzcfa_info3z00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vrefzf2Cinfozd2approxzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762vrefza7f2cinfoza72646za7,
		BGl_z62vrefzf2Cinfozd2approxz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_instanceofzf2Cinfozd2effectzd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762instanceofza7f22647z00,
		BGl_z62instanceofzf2Cinfozd2effectzd2setz12z82zzcfa_info3z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2loczd2envz40zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2648z00,
		BGl_z62valloczf2Cinfozb2optimzd2loczf0zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getfieldzf2Cinfozd2czd2formatzd2envz20zzcfa_info3z00,
		BgL_bgl_za762getfieldza7f2ci2649z00,
		BGl_z62getfieldzf2Cinfozd2czd2formatz90zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2keyzd2envz40zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2650z00,
		BGl_z62valloczf2Cinfozb2optimzd2keyzf0zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2lostzd2stampzd2setz12zd2envz52zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2651z00,
		BGl_z62valloczf2Cinfozb2optimzd2lostzd2stampzd2setz12ze2zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_instanceofzf2Cinfozd2loczd2envzf2zzcfa_info3z00,
		BgL_bgl_za762instanceofza7f22652z00,
		BGl_z62instanceofzf2Cinfozd2locz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2sidezd2effectzd2setz12zd2envz52zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2653z00,
		BGl_z62valloczf2Cinfozb2optimzd2sidezd2effectzd2setz12ze2zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_castzd2nullzf2Cinfozd2loczd2envz20zzcfa_info3z00,
		BgL_bgl_za762castza7d2nullza7f2654za7,
		BGl_z62castzd2nullzf2Cinfozd2locz90zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vrefzf2Cinfozd2tvectorzf3zd2setz12zd2envzc1zzcfa_info3z00,
		BgL_bgl_za762vrefza7f2cinfoza72655za7,
		BGl_z62vrefzf2Cinfozd2tvectorzf3zd2setz12z71zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_instanceofzf2Cinfozd2keyzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762instanceofza7f22656z00,
		BGl_z62instanceofzf2Cinfozd2keyz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_pragmazf2Cinfozd2typezd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762pragmaza7f2cinf2657z00,
		BGl_z62pragmazf2Cinfozd2typezd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_pragmazf2Cinfozd2sidezd2effectzd2envz20zzcfa_info3z00,
		BgL_bgl_za762pragmaza7f2cinf2658z00,
		BGl_z62pragmazf2Cinfozd2sidezd2effectz90zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vlengthzf2Cinfozd2nilzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762vlengthza7f2cin2659z00,
		BGl_z62vlengthzf2Cinfozd2nilz42zzcfa_info3z00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_castzd2nullzf2Cinfozd2keyzd2envz20zzcfa_info3z00,
		BgL_bgl_za762castza7d2nullza7f2660za7,
		BGl_z62castzd2nullzf2Cinfozd2keyz90zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vlengthzf2Cinfozd2tvectorzf3zd2envz01zzcfa_info3z00,
		BgL_bgl_za762vlengthza7f2cin2661z00,
		BGl_z62vlengthzf2Cinfozd2tvectorzf3zb1zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_valloczf2Cinfozd2loczd2envzf2zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2662z00,
		BGl_z62valloczf2Cinfozd2locz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_valloczf2Cinfozd2keyzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2663z00,
		BGl_z62valloczf2Cinfozd2keyz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vrefzf2Cinfozd2typezd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762vrefza7f2cinfoza72664za7,
		BGl_z62vrefzf2Cinfozd2typezd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setfieldzf2Cinfozd2typezd2envzf2zzcfa_info3z00,
		BgL_bgl_za762setfieldza7f2ci2665z00,
		BGl_z62setfieldzf2Cinfozd2typez42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vsetz12zf2Cinfozd2sidezd2effectzd2envz32zzcfa_info3z00,
		BgL_bgl_za762vsetza712za7f2cin2666za7,
		BGl_z62vsetz12zf2Cinfozd2sidezd2effectz82zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vlengthzf2Cinfozd2approxzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762vlengthza7f2cin2667z00,
		BGl_z62vlengthzf2Cinfozd2approxz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vsetz12zf2Cinfozd2typezd2envze0zzcfa_info3z00,
		BgL_bgl_za762vsetza712za7f2cin2668za7,
		BGl_z62vsetz12zf2Cinfozd2typez50zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vsetz12zf2Cinfozd2exprza2zd2envz42zzcfa_info3z00,
		BgL_bgl_za762vsetza712za7f2cin2669za7,
		BGl_z62vsetz12zf2Cinfozd2exprza2zf2zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_pragmazf2Cinfozd2sidezd2effectzd2setz12zd2envze0zzcfa_info3z00,
		BgL_bgl_za762pragmaza7f2cinf2670z00,
		BGl_z62pragmazf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2valuezd2approxzd2envz92zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2671z00,
		BGl_z62valloczf2Cinfozb2optimzd2valuezd2approxz22zzcfa_info3z00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_instanceofzf2Cinfozd2czd2formatzd2envz20zzcfa_info3z00,
		BgL_bgl_za762instanceofza7f22672z00,
		BGl_z62instanceofzf2Cinfozd2czd2formatz90zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2setfieldzf2Cinfozd2envzf2zzcfa_info3z00,
		BgL_bgl_za762makeza7d2setfie2673z00,
		BGl_z62makezd2setfieldzf2Cinfoz42zzcfa_info3z00, 0L, BUNSPEC, 11);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getfieldzf2Cinfozd2effectzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762getfieldza7f2ci2674z00,
		BGl_z62getfieldzf2Cinfozd2effectz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vsetz12zf2Cinfozd2loczd2envze0zzcfa_info3z00,
		BgL_bgl_za762vsetza712za7f2cin2675za7,
		BGl_z62vsetz12zf2Cinfozd2locz50zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vsetz12zf2Cinfozd2keyzd2envze0zzcfa_info3z00,
		BgL_bgl_za762vsetza712za7f2cin2676za7,
		BGl_z62vsetz12zf2Cinfozd2keyz50zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_newzf2Cinfozd2exprza2zd2setz12zd2envz90zzcfa_info3z00,
		BgL_bgl_za762newza7f2cinfoza7d2677za7,
		BGl_z62newzf2Cinfozd2exprza2zd2setz12z20zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vsetz12zf2Cinfozd2otypezd2setz12zd2envz20zzcfa_info3z00,
		BgL_bgl_za762vsetza712za7f2cin2678za7,
		BGl_z62vsetz12zf2Cinfozd2otypezd2setz12z90zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2valloczf2Cinfozb2optimzd2envz40zzcfa_info3z00,
		BgL_bgl_za762makeza7d2valloc2679z00,
		BGl_z62makezd2valloczf2Cinfozb2optimzf0zzcfa_info3z00, 0L, BUNSPEC, 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setfieldzf2Cinfozd2effectzd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762setfieldza7f2ci2680z00,
		BGl_z62setfieldzf2Cinfozd2effectzd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2keyzd2setz12zd2envz80zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2681z00,
		BGl_z62valloczf2Cinfozb2optimzd2keyzd2setz12z30zzcfa_info3z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vsetz12zf2Cinfozd2czd2formatzd2envz32zzcfa_info3z00,
		BgL_bgl_za762vsetza712za7f2cin2682za7,
		BGl_z62vsetz12zf2Cinfozd2czd2formatz82zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vrefzf2Cinfozd2vtypezd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762vrefza7f2cinfoza72683za7,
		BGl_z62vrefzf2Cinfozd2vtypezd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getfieldzf2Cinfozd2typezd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762getfieldza7f2ci2684z00,
		BGl_z62getfieldzf2Cinfozd2typezd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_castzd2nullzf2Cinfozd2sidezd2effectzd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762castza7d2nullza7f2685za7,
		BGl_z62castzd2nullzf2Cinfozd2sidezd2effectzd2setz12z82zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_castzd2nullzf2Cinfozd2effectzd2setz12zd2envze0zzcfa_info3z00,
		BgL_bgl_za762castza7d2nullza7f2686za7,
		BGl_z62castzd2nullzf2Cinfozd2effectzd2setz12z50zzcfa_info3z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2ftypezd2envz40zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2687z00,
		BGl_z62valloczf2Cinfozb2optimzd2ftypezf0zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2valloczf2Cinfozd2otypezd2setz12zd2envze0zzcfa_info3z00,
		BgL_bgl_za762preza7d2vallocza72688za7,
		BGl_z62prezd2valloczf2Cinfozd2otypezd2setz12z50zzcfa_info3z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_vrefzf2Cinfozd2typezd2envzf2zzcfa_info3z00,
		BgL_bgl_za762vrefza7f2cinfoza72689za7,
		BGl_z62vrefzf2Cinfozd2typez42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2ftypezd2setz12zd2envz80zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2690z00,
		BGl_z62valloczf2Cinfozb2optimzd2ftypezd2setz12z30zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2valloczf2Cinfozd2loczd2envz20zzcfa_info3z00,
		BgL_bgl_za762preza7d2vallocza72691za7,
		BGl_z62prezd2valloczf2Cinfozd2locz90zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2valloczf2Cinfozd2keyzd2envz20zzcfa_info3z00,
		BgL_bgl_za762preza7d2vallocza72692za7,
		BGl_z62prezd2valloczf2Cinfozd2keyz90zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2valloczf2Cinfozd2sidezd2effectzd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762preza7d2vallocza72693za7,
		BGl_z62prezd2valloczf2Cinfozd2sidezd2effectzd2setz12z82zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2approxzd2envz40zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2694z00,
		BGl_z62valloczf2Cinfozb2optimzd2approxzf0zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2seenzf3zd2envzb3zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2695z00,
		BGl_z62valloczf2Cinfozb2optimzd2seenzf3z03zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_pragmazf2Cinfozd2effectzd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762pragmaza7f2cinf2696z00,
		BGl_z62pragmazf2Cinfozd2effectzd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vsetz12zf2Cinfozd2tvectorzf3zd2setz12zd2envzd3zzcfa_info3z00,
		BgL_bgl_za762vsetza712za7f2cin2697za7,
		BGl_z62vsetz12zf2Cinfozd2tvectorzf3zd2setz12z63zzcfa_info3z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_newzf2Cinfozd2argszd2typezd2envz20zzcfa_info3z00,
		BgL_bgl_za762newza7f2cinfoza7d2698za7,
		BGl_z62newzf2Cinfozd2argszd2typez90zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getfieldzf2Cinfozd2exprza2zd2setz12zd2envz90zzcfa_info3z00,
		BgL_bgl_za762getfieldza7f2ci2699z00,
		BGl_z62getfieldzf2Cinfozd2exprza2zd2setz12z20zzcfa_info3z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vrefzf2Cinfozd2czd2formatzd2envz20zzcfa_info3z00,
		BgL_bgl_za762vrefza7f2cinfoza72700za7,
		BGl_z62vrefzf2Cinfozd2czd2formatz90zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setfieldzf2Cinfozd2typezd2setz12zd2envz32zzcfa_info3z00,
		BgL_bgl_za762setfieldza7f2ci2701z00,
		BGl_z62setfieldzf2Cinfozd2typezd2setz12z82zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_vsetz12zf2Cinfozf3zd2envzc1zzcfa_info3z00,
		BgL_bgl_za762vsetza712za7f2cin2702za7,
		BGl_z62vsetz12zf2Cinfozf3z71zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2stackzd2stampzd2setz12zd2envz52zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2703z00,
		BGl_z62valloczf2Cinfozb2optimzd2stackzd2stampzd2setz12ze2zzcfa_info3z00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vsetz12zf2Cinfozd2typezd2setz12zd2envz20zzcfa_info3z00,
		BgL_bgl_za762vsetza712za7f2cin2704za7,
		BGl_z62vsetz12zf2Cinfozd2typezd2setz12z90zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vsetz12zf2Cinfozd2effectzd2setz12zd2envz20zzcfa_info3z00,
		BgL_bgl_za762vsetza712za7f2cin2705za7,
		BGl_z62vsetz12zf2Cinfozd2effectzd2setz12z90zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2sidezd2effectzd2envz92zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2706z00,
		BGl_z62valloczf2Cinfozb2optimzd2sidezd2effectz22zzcfa_info3z00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2castzd2nullzf2Cinfozd2envz20zzcfa_info3z00,
		BgL_bgl_za762makeza7d2castza7d2707za7,
		BGl_z62makezd2castzd2nullzf2Cinfoz90zzcfa_info3z00, 0L, BUNSPEC, 8);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_valloczf2Cinfozf3zd2envzd3zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2708z00,
		BGl_z62valloczf2Cinfozf3z63zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vsetz12zf2Cinfozd2tvectorzf3zd2envz13zzcfa_info3z00,
		BgL_bgl_za762vsetza712za7f2cin2709za7,
		BGl_z62vsetz12zf2Cinfozd2tvectorzf3za3zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setfieldzf2Cinfozd2exprza2zd2envz50zzcfa_info3z00,
		BgL_bgl_za762setfieldza7f2ci2710z00,
		BGl_z62setfieldzf2Cinfozd2exprza2ze0zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_newzf2Cinfozd2typezd2envzf2zzcfa_info3z00,
		BgL_bgl_za762newza7f2cinfoza7d2711za7,
		BGl_z62newzf2Cinfozd2typez42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_pragmazf2Cinfozd2approxzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762pragmaza7f2cinf2712z00,
		BGl_z62pragmazf2Cinfozd2approxz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2typezd2envz40zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2713z00,
		BGl_z62valloczf2Cinfozb2optimzd2typezf0zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prezd2valloczf2Cinfozd2keyzd2setz12zd2envze0zzcfa_info3z00,
		BgL_bgl_za762preza7d2vallocza72714za7,
		BGl_z62prezd2valloczf2Cinfozd2keyzd2setz12z50zzcfa_info3z00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozd2approxzd2envzf2zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2715z00,
		BGl_z62valloczf2Cinfozd2approxz42zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vsetz12zf2Cinfozd2ftypezd2setz12zd2envz20zzcfa_info3z00,
		BgL_bgl_za762vsetza712za7f2cin2716za7,
		BGl_z62vsetz12zf2Cinfozd2ftypezd2setz12z90zzcfa_info3z00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzd2otypezd2envz40zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2717z00,
		BGl_z62valloczf2Cinfozb2optimzd2otypezf0zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_valloczf2Cinfozb2optimzf3zd2envz61zzcfa_info3z00,
		BgL_bgl_za762vallocza7f2cinf2718z00,
		BGl_z62valloczf2Cinfozb2optimzf3zd1zzcfa_info3z00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2vrefzf2Cinfozd2envzf2zzcfa_info3z00,
		BgL_bgl_za762makeza7d2vrefza7f2719za7,
		BGl_z62makezd2vrefzf2Cinfoz42zzcfa_info3z00, 0L, BUNSPEC, 13);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_newzf2Cinfozf2zzcfa_info3z00));
		     ADD_ROOT((void *) (&BGl_pragmazf2Cinfozf2zzcfa_info3z00));
		     ADD_ROOT((void *) (&BGl_prezd2valloczf2Cinfoz20zzcfa_info3z00));
		     ADD_ROOT((void *) (&BGl_castzd2nullzf2Cinfoz20zzcfa_info3z00));
		     ADD_ROOT((void *) (&BGl_vsetz12zf2Cinfoze0zzcfa_info3z00));
		     ADD_ROOT((void *) (&BGl_vlengthzf2Cinfozf2zzcfa_info3z00));
		     ADD_ROOT((void *) (&BGl_instanceofzf2Cinfozf2zzcfa_info3z00));
		     ADD_ROOT((void *) (&BGl_valloczf2Cinfozf2zzcfa_info3z00));
		     ADD_ROOT((void *) (&BGl_getfieldzf2Cinfozf2zzcfa_info3z00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcfa_info3z00));
		     ADD_ROOT((void *) (&BGl_vrefzf2Cinfozf2zzcfa_info3z00));
		     ADD_ROOT((void *) (&BGl_setfieldzf2Cinfozf2zzcfa_info3z00));
		     ADD_ROOT((void *) (&BGl_valloczf2Cinfozb2optimz40zzcfa_info3z00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcfa_info3z00(long
		BgL_checksumz00_6966, char *BgL_fromz00_6967)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcfa_info3z00))
				{
					BGl_requirezd2initializa7ationz75zzcfa_info3z00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcfa_info3z00();
					BGl_libraryzd2moduleszd2initz00zzcfa_info3z00();
					BGl_cnstzd2initzd2zzcfa_info3z00();
					BGl_importedzd2moduleszd2initz00zzcfa_info3z00();
					BGl_objectzd2initzd2zzcfa_info3z00();
					return BGl_toplevelzd2initzd2zzcfa_info3z00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcfa_info3z00(void)
	{
		{	/* Cfa/cinfo3.scm 16 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cfa_info3");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cfa_info3");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cfa_info3");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cfa_info3");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"cfa_info3");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "cfa_info3");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cfa_info3");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcfa_info3z00(void)
	{
		{	/* Cfa/cinfo3.scm 16 */
			{	/* Cfa/cinfo3.scm 16 */
				obj_t BgL_cportz00_6258;

				{	/* Cfa/cinfo3.scm 16 */
					obj_t BgL_stringz00_6265;

					BgL_stringz00_6265 = BGl_string2366z00zzcfa_info3z00;
					{	/* Cfa/cinfo3.scm 16 */
						obj_t BgL_startz00_6266;

						BgL_startz00_6266 = BINT(0L);
						{	/* Cfa/cinfo3.scm 16 */
							obj_t BgL_endz00_6267;

							BgL_endz00_6267 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_6265)));
							{	/* Cfa/cinfo3.scm 16 */

								BgL_cportz00_6258 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_6265, BgL_startz00_6266, BgL_endz00_6267);
				}}}}
				{
					long BgL_iz00_6259;

					BgL_iz00_6259 = 25L;
				BgL_loopz00_6260:
					if ((BgL_iz00_6259 == -1L))
						{	/* Cfa/cinfo3.scm 16 */
							return BUNSPEC;
						}
					else
						{	/* Cfa/cinfo3.scm 16 */
							{	/* Cfa/cinfo3.scm 16 */
								obj_t BgL_arg2367z00_6261;

								{	/* Cfa/cinfo3.scm 16 */

									{	/* Cfa/cinfo3.scm 16 */
										obj_t BgL_locationz00_6263;

										BgL_locationz00_6263 = BBOOL(((bool_t) 0));
										{	/* Cfa/cinfo3.scm 16 */

											BgL_arg2367z00_6261 =
												BGl_readz00zz__readerz00(BgL_cportz00_6258,
												BgL_locationz00_6263);
										}
									}
								}
								{	/* Cfa/cinfo3.scm 16 */
									int BgL_tmpz00_6993;

									BgL_tmpz00_6993 = (int) (BgL_iz00_6259);
									CNST_TABLE_SET(BgL_tmpz00_6993, BgL_arg2367z00_6261);
							}}
							{	/* Cfa/cinfo3.scm 16 */
								int BgL_auxz00_6264;

								BgL_auxz00_6264 = (int) ((BgL_iz00_6259 - 1L));
								{
									long BgL_iz00_6998;

									BgL_iz00_6998 = (long) (BgL_auxz00_6264);
									BgL_iz00_6259 = BgL_iz00_6998;
									goto BgL_loopz00_6260;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcfa_info3z00(void)
	{
		{	/* Cfa/cinfo3.scm 16 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcfa_info3z00(void)
	{
		{	/* Cfa/cinfo3.scm 16 */
			return BUNSPEC;
		}

	}



/* make-pragma/Cinfo */
	BGL_EXPORTED_DEF BgL_pragmaz00_bglt
		BGl_makezd2pragmazf2Cinfoz20zzcfa_info3z00(obj_t BgL_loc1471z00_3,
		BgL_typez00_bglt BgL_type1472z00_4, obj_t BgL_sidezd2effect1473zd2_5,
		obj_t BgL_key1474z00_6, obj_t BgL_exprza21475za2_7,
		obj_t BgL_effect1476z00_8, obj_t BgL_format1477z00_9,
		BgL_approxz00_bglt BgL_approx1478z00_10)
	{
		{	/* Cfa/cinfo3.sch 314 */
			{	/* Cfa/cinfo3.sch 314 */
				BgL_pragmaz00_bglt BgL_new1340z00_6269;

				{	/* Cfa/cinfo3.sch 314 */
					BgL_pragmaz00_bglt BgL_tmp1338z00_6270;
					BgL_pragmazf2cinfozf2_bglt BgL_wide1339z00_6271;

					{
						BgL_pragmaz00_bglt BgL_auxz00_7001;

						{	/* Cfa/cinfo3.sch 314 */
							BgL_pragmaz00_bglt BgL_new1337z00_6272;

							BgL_new1337z00_6272 =
								((BgL_pragmaz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_pragmaz00_bgl))));
							{	/* Cfa/cinfo3.sch 314 */
								long BgL_arg1705z00_6273;

								BgL_arg1705z00_6273 = BGL_CLASS_NUM(BGl_pragmaz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1337z00_6272),
									BgL_arg1705z00_6273);
							}
							{	/* Cfa/cinfo3.sch 314 */
								BgL_objectz00_bglt BgL_tmpz00_7006;

								BgL_tmpz00_7006 = ((BgL_objectz00_bglt) BgL_new1337z00_6272);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7006, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1337z00_6272);
							BgL_auxz00_7001 = BgL_new1337z00_6272;
						}
						BgL_tmp1338z00_6270 = ((BgL_pragmaz00_bglt) BgL_auxz00_7001);
					}
					BgL_wide1339z00_6271 =
						((BgL_pragmazf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_pragmazf2cinfozf2_bgl))));
					{	/* Cfa/cinfo3.sch 314 */
						obj_t BgL_auxz00_7014;
						BgL_objectz00_bglt BgL_tmpz00_7012;

						BgL_auxz00_7014 = ((obj_t) BgL_wide1339z00_6271);
						BgL_tmpz00_7012 = ((BgL_objectz00_bglt) BgL_tmp1338z00_6270);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7012, BgL_auxz00_7014);
					}
					((BgL_objectz00_bglt) BgL_tmp1338z00_6270);
					{	/* Cfa/cinfo3.sch 314 */
						long BgL_arg1703z00_6274;

						BgL_arg1703z00_6274 =
							BGL_CLASS_NUM(BGl_pragmazf2Cinfozf2zzcfa_info3z00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1338z00_6270), BgL_arg1703z00_6274);
					}
					BgL_new1340z00_6269 = ((BgL_pragmaz00_bglt) BgL_tmp1338z00_6270);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1340z00_6269)))->BgL_locz00) =
					((obj_t) BgL_loc1471z00_3), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1340z00_6269)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1472z00_4), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1340z00_6269)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1473zd2_5), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1340z00_6269)))->BgL_keyz00) =
					((obj_t) BgL_key1474z00_6), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1340z00_6269)))->BgL_exprza2za2) =
					((obj_t) BgL_exprza21475za2_7), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1340z00_6269)))->BgL_effectz00) =
					((obj_t) BgL_effect1476z00_8), BUNSPEC);
				((((BgL_pragmaz00_bglt) COBJECT(((BgL_pragmaz00_bglt)
									BgL_new1340z00_6269)))->BgL_formatz00) =
					((obj_t) BgL_format1477z00_9), BUNSPEC);
				((((BgL_pragmaz00_bglt) COBJECT(((BgL_pragmaz00_bglt)
									BgL_new1340z00_6269)))->BgL_srfi0z00) =
					((obj_t) CNST_TABLE_REF(0)), BUNSPEC);
				{
					BgL_pragmazf2cinfozf2_bglt BgL_auxz00_7039;

					{
						obj_t BgL_auxz00_7040;

						{	/* Cfa/cinfo3.sch 314 */
							BgL_objectz00_bglt BgL_tmpz00_7041;

							BgL_tmpz00_7041 = ((BgL_objectz00_bglt) BgL_new1340z00_6269);
							BgL_auxz00_7040 = BGL_OBJECT_WIDENING(BgL_tmpz00_7041);
						}
						BgL_auxz00_7039 = ((BgL_pragmazf2cinfozf2_bglt) BgL_auxz00_7040);
					}
					((((BgL_pragmazf2cinfozf2_bglt) COBJECT(BgL_auxz00_7039))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1478z00_10), BUNSPEC);
				}
				return BgL_new1340z00_6269;
			}
		}

	}



/* &make-pragma/Cinfo */
	BgL_pragmaz00_bglt BGl_z62makezd2pragmazf2Cinfoz42zzcfa_info3z00(obj_t
		BgL_envz00_5139, obj_t BgL_loc1471z00_5140, obj_t BgL_type1472z00_5141,
		obj_t BgL_sidezd2effect1473zd2_5142, obj_t BgL_key1474z00_5143,
		obj_t BgL_exprza21475za2_5144, obj_t BgL_effect1476z00_5145,
		obj_t BgL_format1477z00_5146, obj_t BgL_approx1478z00_5147)
	{
		{	/* Cfa/cinfo3.sch 314 */
			return
				BGl_makezd2pragmazf2Cinfoz20zzcfa_info3z00(BgL_loc1471z00_5140,
				((BgL_typez00_bglt) BgL_type1472z00_5141),
				BgL_sidezd2effect1473zd2_5142, BgL_key1474z00_5143,
				BgL_exprza21475za2_5144, BgL_effect1476z00_5145, BgL_format1477z00_5146,
				((BgL_approxz00_bglt) BgL_approx1478z00_5147));
		}

	}



/* pragma/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_pragmazf2Cinfozf3z01zzcfa_info3z00(obj_t
		BgL_objz00_11)
	{
		{	/* Cfa/cinfo3.sch 315 */
			{	/* Cfa/cinfo3.sch 315 */
				obj_t BgL_classz00_6275;

				BgL_classz00_6275 = BGl_pragmazf2Cinfozf2zzcfa_info3z00;
				if (BGL_OBJECTP(BgL_objz00_11))
					{	/* Cfa/cinfo3.sch 315 */
						BgL_objectz00_bglt BgL_arg1807z00_6276;

						BgL_arg1807z00_6276 = (BgL_objectz00_bglt) (BgL_objz00_11);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo3.sch 315 */
								long BgL_idxz00_6277;

								BgL_idxz00_6277 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6276);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6277 + 5L)) == BgL_classz00_6275);
							}
						else
							{	/* Cfa/cinfo3.sch 315 */
								bool_t BgL_res2255z00_6280;

								{	/* Cfa/cinfo3.sch 315 */
									obj_t BgL_oclassz00_6281;

									{	/* Cfa/cinfo3.sch 315 */
										obj_t BgL_arg1815z00_6282;
										long BgL_arg1816z00_6283;

										BgL_arg1815z00_6282 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo3.sch 315 */
											long BgL_arg1817z00_6284;

											BgL_arg1817z00_6284 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6276);
											BgL_arg1816z00_6283 = (BgL_arg1817z00_6284 - OBJECT_TYPE);
										}
										BgL_oclassz00_6281 =
											VECTOR_REF(BgL_arg1815z00_6282, BgL_arg1816z00_6283);
									}
									{	/* Cfa/cinfo3.sch 315 */
										bool_t BgL__ortest_1115z00_6285;

										BgL__ortest_1115z00_6285 =
											(BgL_classz00_6275 == BgL_oclassz00_6281);
										if (BgL__ortest_1115z00_6285)
											{	/* Cfa/cinfo3.sch 315 */
												BgL_res2255z00_6280 = BgL__ortest_1115z00_6285;
											}
										else
											{	/* Cfa/cinfo3.sch 315 */
												long BgL_odepthz00_6286;

												{	/* Cfa/cinfo3.sch 315 */
													obj_t BgL_arg1804z00_6287;

													BgL_arg1804z00_6287 = (BgL_oclassz00_6281);
													BgL_odepthz00_6286 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6287);
												}
												if ((5L < BgL_odepthz00_6286))
													{	/* Cfa/cinfo3.sch 315 */
														obj_t BgL_arg1802z00_6288;

														{	/* Cfa/cinfo3.sch 315 */
															obj_t BgL_arg1803z00_6289;

															BgL_arg1803z00_6289 = (BgL_oclassz00_6281);
															BgL_arg1802z00_6288 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6289,
																5L);
														}
														BgL_res2255z00_6280 =
															(BgL_arg1802z00_6288 == BgL_classz00_6275);
													}
												else
													{	/* Cfa/cinfo3.sch 315 */
														BgL_res2255z00_6280 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2255z00_6280;
							}
					}
				else
					{	/* Cfa/cinfo3.sch 315 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &pragma/Cinfo? */
	obj_t BGl_z62pragmazf2Cinfozf3z63zzcfa_info3z00(obj_t BgL_envz00_5148,
		obj_t BgL_objz00_5149)
	{
		{	/* Cfa/cinfo3.sch 315 */
			return BBOOL(BGl_pragmazf2Cinfozf3z01zzcfa_info3z00(BgL_objz00_5149));
		}

	}



/* pragma/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_pragmaz00_bglt
		BGl_pragmazf2Cinfozd2nilz20zzcfa_info3z00(void)
	{
		{	/* Cfa/cinfo3.sch 316 */
			{	/* Cfa/cinfo3.sch 316 */
				obj_t BgL_classz00_3979;

				BgL_classz00_3979 = BGl_pragmazf2Cinfozf2zzcfa_info3z00;
				{	/* Cfa/cinfo3.sch 316 */
					obj_t BgL__ortest_1117z00_3980;

					BgL__ortest_1117z00_3980 = BGL_CLASS_NIL(BgL_classz00_3979);
					if (CBOOL(BgL__ortest_1117z00_3980))
						{	/* Cfa/cinfo3.sch 316 */
							return ((BgL_pragmaz00_bglt) BgL__ortest_1117z00_3980);
						}
					else
						{	/* Cfa/cinfo3.sch 316 */
							return
								((BgL_pragmaz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3979));
						}
				}
			}
		}

	}



/* &pragma/Cinfo-nil */
	BgL_pragmaz00_bglt BGl_z62pragmazf2Cinfozd2nilz42zzcfa_info3z00(obj_t
		BgL_envz00_5150)
	{
		{	/* Cfa/cinfo3.sch 316 */
			return BGl_pragmazf2Cinfozd2nilz20zzcfa_info3z00();
		}

	}



/* pragma/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_pragmazf2Cinfozd2approxz20zzcfa_info3z00(BgL_pragmaz00_bglt BgL_oz00_12)
	{
		{	/* Cfa/cinfo3.sch 317 */
			{
				BgL_pragmazf2cinfozf2_bglt BgL_auxz00_7080;

				{
					obj_t BgL_auxz00_7081;

					{	/* Cfa/cinfo3.sch 317 */
						BgL_objectz00_bglt BgL_tmpz00_7082;

						BgL_tmpz00_7082 = ((BgL_objectz00_bglt) BgL_oz00_12);
						BgL_auxz00_7081 = BGL_OBJECT_WIDENING(BgL_tmpz00_7082);
					}
					BgL_auxz00_7080 = ((BgL_pragmazf2cinfozf2_bglt) BgL_auxz00_7081);
				}
				return
					(((BgL_pragmazf2cinfozf2_bglt) COBJECT(BgL_auxz00_7080))->
					BgL_approxz00);
			}
		}

	}



/* &pragma/Cinfo-approx */
	BgL_approxz00_bglt BGl_z62pragmazf2Cinfozd2approxz42zzcfa_info3z00(obj_t
		BgL_envz00_5151, obj_t BgL_oz00_5152)
	{
		{	/* Cfa/cinfo3.sch 317 */
			return
				BGl_pragmazf2Cinfozd2approxz20zzcfa_info3z00(
				((BgL_pragmaz00_bglt) BgL_oz00_5152));
		}

	}



/* pragma/Cinfo-format */
	BGL_EXPORTED_DEF obj_t
		BGl_pragmazf2Cinfozd2formatz20zzcfa_info3z00(BgL_pragmaz00_bglt BgL_oz00_15)
	{
		{	/* Cfa/cinfo3.sch 319 */
			return
				(((BgL_pragmaz00_bglt) COBJECT(
						((BgL_pragmaz00_bglt) BgL_oz00_15)))->BgL_formatz00);
		}

	}



/* &pragma/Cinfo-format */
	obj_t BGl_z62pragmazf2Cinfozd2formatz42zzcfa_info3z00(obj_t BgL_envz00_5153,
		obj_t BgL_oz00_5154)
	{
		{	/* Cfa/cinfo3.sch 319 */
			return
				BGl_pragmazf2Cinfozd2formatz20zzcfa_info3z00(
				((BgL_pragmaz00_bglt) BgL_oz00_5154));
		}

	}



/* pragma/Cinfo-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_pragmazf2Cinfozd2effectz20zzcfa_info3z00(BgL_pragmaz00_bglt BgL_oz00_18)
	{
		{	/* Cfa/cinfo3.sch 321 */
			return
				(((BgL_externz00_bglt) COBJECT(
						((BgL_externz00_bglt) BgL_oz00_18)))->BgL_effectz00);
		}

	}



/* &pragma/Cinfo-effect */
	obj_t BGl_z62pragmazf2Cinfozd2effectz42zzcfa_info3z00(obj_t BgL_envz00_5155,
		obj_t BgL_oz00_5156)
	{
		{	/* Cfa/cinfo3.sch 321 */
			return
				BGl_pragmazf2Cinfozd2effectz20zzcfa_info3z00(
				((BgL_pragmaz00_bglt) BgL_oz00_5156));
		}

	}



/* pragma/Cinfo-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_pragmazf2Cinfozd2effectzd2setz12ze0zzcfa_info3z00(BgL_pragmaz00_bglt
		BgL_oz00_19, obj_t BgL_vz00_20)
	{
		{	/* Cfa/cinfo3.sch 322 */
			return
				((((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_oz00_19)))->BgL_effectz00) =
				((obj_t) BgL_vz00_20), BUNSPEC);
		}

	}



/* &pragma/Cinfo-effect-set! */
	obj_t BGl_z62pragmazf2Cinfozd2effectzd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5157, obj_t BgL_oz00_5158, obj_t BgL_vz00_5159)
	{
		{	/* Cfa/cinfo3.sch 322 */
			return
				BGl_pragmazf2Cinfozd2effectzd2setz12ze0zzcfa_info3z00(
				((BgL_pragmaz00_bglt) BgL_oz00_5158), BgL_vz00_5159);
		}

	}



/* pragma/Cinfo-expr* */
	BGL_EXPORTED_DEF obj_t
		BGl_pragmazf2Cinfozd2exprza2z82zzcfa_info3z00(BgL_pragmaz00_bglt
		BgL_oz00_21)
	{
		{	/* Cfa/cinfo3.sch 323 */
			return
				(((BgL_externz00_bglt) COBJECT(
						((BgL_externz00_bglt) BgL_oz00_21)))->BgL_exprza2za2);
		}

	}



/* &pragma/Cinfo-expr* */
	obj_t BGl_z62pragmazf2Cinfozd2exprza2ze0zzcfa_info3z00(obj_t BgL_envz00_5160,
		obj_t BgL_oz00_5161)
	{
		{	/* Cfa/cinfo3.sch 323 */
			return
				BGl_pragmazf2Cinfozd2exprza2z82zzcfa_info3z00(
				((BgL_pragmaz00_bglt) BgL_oz00_5161));
		}

	}



/* pragma/Cinfo-expr*-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_pragmazf2Cinfozd2exprza2zd2setz12z42zzcfa_info3z00(BgL_pragmaz00_bglt
		BgL_oz00_22, obj_t BgL_vz00_23)
	{
		{	/* Cfa/cinfo3.sch 324 */
			return
				((((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_oz00_22)))->BgL_exprza2za2) =
				((obj_t) BgL_vz00_23), BUNSPEC);
		}

	}



/* &pragma/Cinfo-expr*-set! */
	obj_t BGl_z62pragmazf2Cinfozd2exprza2zd2setz12z20zzcfa_info3z00(obj_t
		BgL_envz00_5162, obj_t BgL_oz00_5163, obj_t BgL_vz00_5164)
	{
		{	/* Cfa/cinfo3.sch 324 */
			return
				BGl_pragmazf2Cinfozd2exprza2zd2setz12z42zzcfa_info3z00(
				((BgL_pragmaz00_bglt) BgL_oz00_5163), BgL_vz00_5164);
		}

	}



/* pragma/Cinfo-key */
	BGL_EXPORTED_DEF obj_t
		BGl_pragmazf2Cinfozd2keyz20zzcfa_info3z00(BgL_pragmaz00_bglt BgL_oz00_24)
	{
		{	/* Cfa/cinfo3.sch 325 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_24)))->BgL_keyz00);
		}

	}



/* &pragma/Cinfo-key */
	obj_t BGl_z62pragmazf2Cinfozd2keyz42zzcfa_info3z00(obj_t BgL_envz00_5165,
		obj_t BgL_oz00_5166)
	{
		{	/* Cfa/cinfo3.sch 325 */
			return
				BGl_pragmazf2Cinfozd2keyz20zzcfa_info3z00(
				((BgL_pragmaz00_bglt) BgL_oz00_5166));
		}

	}



/* pragma/Cinfo-key-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_pragmazf2Cinfozd2keyzd2setz12ze0zzcfa_info3z00(BgL_pragmaz00_bglt
		BgL_oz00_25, obj_t BgL_vz00_26)
	{
		{	/* Cfa/cinfo3.sch 326 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_25)))->BgL_keyz00) =
				((obj_t) BgL_vz00_26), BUNSPEC);
		}

	}



/* &pragma/Cinfo-key-set! */
	obj_t BGl_z62pragmazf2Cinfozd2keyzd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5167, obj_t BgL_oz00_5168, obj_t BgL_vz00_5169)
	{
		{	/* Cfa/cinfo3.sch 326 */
			return
				BGl_pragmazf2Cinfozd2keyzd2setz12ze0zzcfa_info3z00(
				((BgL_pragmaz00_bglt) BgL_oz00_5168), BgL_vz00_5169);
		}

	}



/* pragma/Cinfo-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_pragmazf2Cinfozd2sidezd2effectzf2zzcfa_info3z00(BgL_pragmaz00_bglt
		BgL_oz00_27)
	{
		{	/* Cfa/cinfo3.sch 327 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_27)))->BgL_sidezd2effectzd2);
		}

	}



/* &pragma/Cinfo-side-effect */
	obj_t BGl_z62pragmazf2Cinfozd2sidezd2effectz90zzcfa_info3z00(obj_t
		BgL_envz00_5170, obj_t BgL_oz00_5171)
	{
		{	/* Cfa/cinfo3.sch 327 */
			return
				BGl_pragmazf2Cinfozd2sidezd2effectzf2zzcfa_info3z00(
				((BgL_pragmaz00_bglt) BgL_oz00_5171));
		}

	}



/* pragma/Cinfo-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_pragmazf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_info3z00
		(BgL_pragmaz00_bglt BgL_oz00_28, obj_t BgL_vz00_29)
	{
		{	/* Cfa/cinfo3.sch 328 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_28)))->
					BgL_sidezd2effectzd2) = ((obj_t) BgL_vz00_29), BUNSPEC);
		}

	}



/* &pragma/Cinfo-side-effect-set! */
	obj_t BGl_z62pragmazf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_info3z00(obj_t
		BgL_envz00_5172, obj_t BgL_oz00_5173, obj_t BgL_vz00_5174)
	{
		{	/* Cfa/cinfo3.sch 328 */
			return
				BGl_pragmazf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_info3z00(
				((BgL_pragmaz00_bglt) BgL_oz00_5173), BgL_vz00_5174);
		}

	}



/* pragma/Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_pragmazf2Cinfozd2typez20zzcfa_info3z00(BgL_pragmaz00_bglt BgL_oz00_30)
	{
		{	/* Cfa/cinfo3.sch 329 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_30)))->BgL_typez00);
		}

	}



/* &pragma/Cinfo-type */
	BgL_typez00_bglt BGl_z62pragmazf2Cinfozd2typez42zzcfa_info3z00(obj_t
		BgL_envz00_5175, obj_t BgL_oz00_5176)
	{
		{	/* Cfa/cinfo3.sch 329 */
			return
				BGl_pragmazf2Cinfozd2typez20zzcfa_info3z00(
				((BgL_pragmaz00_bglt) BgL_oz00_5176));
		}

	}



/* pragma/Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_pragmazf2Cinfozd2typezd2setz12ze0zzcfa_info3z00(BgL_pragmaz00_bglt
		BgL_oz00_31, BgL_typez00_bglt BgL_vz00_32)
	{
		{	/* Cfa/cinfo3.sch 330 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_31)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_32), BUNSPEC);
		}

	}



/* &pragma/Cinfo-type-set! */
	obj_t BGl_z62pragmazf2Cinfozd2typezd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5177, obj_t BgL_oz00_5178, obj_t BgL_vz00_5179)
	{
		{	/* Cfa/cinfo3.sch 330 */
			return
				BGl_pragmazf2Cinfozd2typezd2setz12ze0zzcfa_info3z00(
				((BgL_pragmaz00_bglt) BgL_oz00_5178),
				((BgL_typez00_bglt) BgL_vz00_5179));
		}

	}



/* pragma/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_pragmazf2Cinfozd2locz20zzcfa_info3z00(BgL_pragmaz00_bglt BgL_oz00_33)
	{
		{	/* Cfa/cinfo3.sch 331 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_33)))->BgL_locz00);
		}

	}



/* &pragma/Cinfo-loc */
	obj_t BGl_z62pragmazf2Cinfozd2locz42zzcfa_info3z00(obj_t BgL_envz00_5180,
		obj_t BgL_oz00_5181)
	{
		{	/* Cfa/cinfo3.sch 331 */
			return
				BGl_pragmazf2Cinfozd2locz20zzcfa_info3z00(
				((BgL_pragmaz00_bglt) BgL_oz00_5181));
		}

	}



/* make-getfield/Cinfo */
	BGL_EXPORTED_DEF BgL_getfieldz00_bglt
		BGl_makezd2getfieldzf2Cinfoz20zzcfa_info3z00(obj_t BgL_loc1459z00_36,
		BgL_typez00_bglt BgL_type1460z00_37, obj_t BgL_sidezd2effect1461zd2_38,
		obj_t BgL_key1462z00_39, obj_t BgL_exprza21463za2_40,
		obj_t BgL_effect1464z00_41, obj_t BgL_czd2format1465zd2_42,
		obj_t BgL_fname1466z00_43, BgL_typez00_bglt BgL_ftype1467z00_44,
		BgL_typez00_bglt BgL_otype1468z00_45,
		BgL_approxz00_bglt BgL_approx1469z00_46)
	{
		{	/* Cfa/cinfo3.sch 335 */
			{	/* Cfa/cinfo3.sch 335 */
				BgL_getfieldz00_bglt BgL_new1345z00_6290;

				{	/* Cfa/cinfo3.sch 335 */
					BgL_getfieldz00_bglt BgL_tmp1342z00_6291;
					BgL_getfieldzf2cinfozf2_bglt BgL_wide1343z00_6292;

					{
						BgL_getfieldz00_bglt BgL_auxz00_7138;

						{	/* Cfa/cinfo3.sch 335 */
							BgL_getfieldz00_bglt BgL_new1341z00_6293;

							BgL_new1341z00_6293 =
								((BgL_getfieldz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_getfieldz00_bgl))));
							{	/* Cfa/cinfo3.sch 335 */
								long BgL_arg1709z00_6294;

								BgL_arg1709z00_6294 =
									BGL_CLASS_NUM(BGl_getfieldz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1341z00_6293),
									BgL_arg1709z00_6294);
							}
							{	/* Cfa/cinfo3.sch 335 */
								BgL_objectz00_bglt BgL_tmpz00_7143;

								BgL_tmpz00_7143 = ((BgL_objectz00_bglt) BgL_new1341z00_6293);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7143, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1341z00_6293);
							BgL_auxz00_7138 = BgL_new1341z00_6293;
						}
						BgL_tmp1342z00_6291 = ((BgL_getfieldz00_bglt) BgL_auxz00_7138);
					}
					BgL_wide1343z00_6292 =
						((BgL_getfieldzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_getfieldzf2cinfozf2_bgl))));
					{	/* Cfa/cinfo3.sch 335 */
						obj_t BgL_auxz00_7151;
						BgL_objectz00_bglt BgL_tmpz00_7149;

						BgL_auxz00_7151 = ((obj_t) BgL_wide1343z00_6292);
						BgL_tmpz00_7149 = ((BgL_objectz00_bglt) BgL_tmp1342z00_6291);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7149, BgL_auxz00_7151);
					}
					((BgL_objectz00_bglt) BgL_tmp1342z00_6291);
					{	/* Cfa/cinfo3.sch 335 */
						long BgL_arg1708z00_6295;

						BgL_arg1708z00_6295 =
							BGL_CLASS_NUM(BGl_getfieldzf2Cinfozf2zzcfa_info3z00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1342z00_6291), BgL_arg1708z00_6295);
					}
					BgL_new1345z00_6290 = ((BgL_getfieldz00_bglt) BgL_tmp1342z00_6291);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1345z00_6290)))->BgL_locz00) =
					((obj_t) BgL_loc1459z00_36), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1345z00_6290)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1460z00_37), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1345z00_6290)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1461zd2_38), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1345z00_6290)))->BgL_keyz00) =
					((obj_t) BgL_key1462z00_39), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1345z00_6290)))->BgL_exprza2za2) =
					((obj_t) BgL_exprza21463za2_40), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1345z00_6290)))->BgL_effectz00) =
					((obj_t) BgL_effect1464z00_41), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1345z00_6290)))->BgL_czd2formatzd2) =
					((obj_t) BgL_czd2format1465zd2_42), BUNSPEC);
				((((BgL_getfieldz00_bglt) COBJECT(((BgL_getfieldz00_bglt)
									BgL_new1345z00_6290)))->BgL_fnamez00) =
					((obj_t) BgL_fname1466z00_43), BUNSPEC);
				((((BgL_getfieldz00_bglt) COBJECT(((BgL_getfieldz00_bglt)
									BgL_new1345z00_6290)))->BgL_ftypez00) =
					((BgL_typez00_bglt) BgL_ftype1467z00_44), BUNSPEC);
				((((BgL_getfieldz00_bglt) COBJECT(((BgL_getfieldz00_bglt)
									BgL_new1345z00_6290)))->BgL_otypez00) =
					((BgL_typez00_bglt) BgL_otype1468z00_45), BUNSPEC);
				{
					BgL_getfieldzf2cinfozf2_bglt BgL_auxz00_7179;

					{
						obj_t BgL_auxz00_7180;

						{	/* Cfa/cinfo3.sch 335 */
							BgL_objectz00_bglt BgL_tmpz00_7181;

							BgL_tmpz00_7181 = ((BgL_objectz00_bglt) BgL_new1345z00_6290);
							BgL_auxz00_7180 = BGL_OBJECT_WIDENING(BgL_tmpz00_7181);
						}
						BgL_auxz00_7179 = ((BgL_getfieldzf2cinfozf2_bglt) BgL_auxz00_7180);
					}
					((((BgL_getfieldzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7179))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1469z00_46), BUNSPEC);
				}
				return BgL_new1345z00_6290;
			}
		}

	}



/* &make-getfield/Cinfo */
	BgL_getfieldz00_bglt BGl_z62makezd2getfieldzf2Cinfoz42zzcfa_info3z00(obj_t
		BgL_envz00_5182, obj_t BgL_loc1459z00_5183, obj_t BgL_type1460z00_5184,
		obj_t BgL_sidezd2effect1461zd2_5185, obj_t BgL_key1462z00_5186,
		obj_t BgL_exprza21463za2_5187, obj_t BgL_effect1464z00_5188,
		obj_t BgL_czd2format1465zd2_5189, obj_t BgL_fname1466z00_5190,
		obj_t BgL_ftype1467z00_5191, obj_t BgL_otype1468z00_5192,
		obj_t BgL_approx1469z00_5193)
	{
		{	/* Cfa/cinfo3.sch 335 */
			return
				BGl_makezd2getfieldzf2Cinfoz20zzcfa_info3z00(BgL_loc1459z00_5183,
				((BgL_typez00_bglt) BgL_type1460z00_5184),
				BgL_sidezd2effect1461zd2_5185, BgL_key1462z00_5186,
				BgL_exprza21463za2_5187, BgL_effect1464z00_5188,
				BgL_czd2format1465zd2_5189, BgL_fname1466z00_5190,
				((BgL_typez00_bglt) BgL_ftype1467z00_5191),
				((BgL_typez00_bglt) BgL_otype1468z00_5192),
				((BgL_approxz00_bglt) BgL_approx1469z00_5193));
		}

	}



/* getfield/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_getfieldzf2Cinfozf3z01zzcfa_info3z00(obj_t
		BgL_objz00_47)
	{
		{	/* Cfa/cinfo3.sch 336 */
			{	/* Cfa/cinfo3.sch 336 */
				obj_t BgL_classz00_6296;

				BgL_classz00_6296 = BGl_getfieldzf2Cinfozf2zzcfa_info3z00;
				if (BGL_OBJECTP(BgL_objz00_47))
					{	/* Cfa/cinfo3.sch 336 */
						BgL_objectz00_bglt BgL_arg1809z00_6297;
						long BgL_arg1810z00_6298;

						BgL_arg1809z00_6297 = (BgL_objectz00_bglt) (BgL_objz00_47);
						BgL_arg1810z00_6298 = BGL_CLASS_DEPTH(BgL_classz00_6296);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo3.sch 336 */
								long BgL_idxz00_6299;

								BgL_idxz00_6299 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1809z00_6297);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6299 + BgL_arg1810z00_6298)) ==
									BgL_classz00_6296);
							}
						else
							{	/* Cfa/cinfo3.sch 336 */
								bool_t BgL_res2256z00_6302;

								{	/* Cfa/cinfo3.sch 336 */
									obj_t BgL_oclassz00_6303;

									{	/* Cfa/cinfo3.sch 336 */
										obj_t BgL_arg1815z00_6304;
										long BgL_arg1816z00_6305;

										BgL_arg1815z00_6304 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo3.sch 336 */
											long BgL_arg1817z00_6306;

											BgL_arg1817z00_6306 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1809z00_6297);
											BgL_arg1816z00_6305 = (BgL_arg1817z00_6306 - OBJECT_TYPE);
										}
										BgL_oclassz00_6303 =
											VECTOR_REF(BgL_arg1815z00_6304, BgL_arg1816z00_6305);
									}
									{	/* Cfa/cinfo3.sch 336 */
										bool_t BgL__ortest_1115z00_6307;

										BgL__ortest_1115z00_6307 =
											(BgL_classz00_6296 == BgL_oclassz00_6303);
										if (BgL__ortest_1115z00_6307)
											{	/* Cfa/cinfo3.sch 336 */
												BgL_res2256z00_6302 = BgL__ortest_1115z00_6307;
											}
										else
											{	/* Cfa/cinfo3.sch 336 */
												long BgL_odepthz00_6308;

												{	/* Cfa/cinfo3.sch 336 */
													obj_t BgL_arg1804z00_6309;

													BgL_arg1804z00_6309 = (BgL_oclassz00_6303);
													BgL_odepthz00_6308 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6309);
												}
												if ((BgL_arg1810z00_6298 < BgL_odepthz00_6308))
													{	/* Cfa/cinfo3.sch 336 */
														obj_t BgL_arg1802z00_6310;

														{	/* Cfa/cinfo3.sch 336 */
															obj_t BgL_arg1803z00_6311;

															BgL_arg1803z00_6311 = (BgL_oclassz00_6303);
															BgL_arg1802z00_6310 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6311,
																BgL_arg1810z00_6298);
														}
														BgL_res2256z00_6302 =
															(BgL_arg1802z00_6310 == BgL_classz00_6296);
													}
												else
													{	/* Cfa/cinfo3.sch 336 */
														BgL_res2256z00_6302 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2256z00_6302;
							}
					}
				else
					{	/* Cfa/cinfo3.sch 336 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &getfield/Cinfo? */
	obj_t BGl_z62getfieldzf2Cinfozf3z63zzcfa_info3z00(obj_t BgL_envz00_5194,
		obj_t BgL_objz00_5195)
	{
		{	/* Cfa/cinfo3.sch 336 */
			return BBOOL(BGl_getfieldzf2Cinfozf3z01zzcfa_info3z00(BgL_objz00_5195));
		}

	}



/* getfield/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_getfieldz00_bglt
		BGl_getfieldzf2Cinfozd2nilz20zzcfa_info3z00(void)
	{
		{	/* Cfa/cinfo3.sch 337 */
			{	/* Cfa/cinfo3.sch 337 */
				obj_t BgL_classz00_4037;

				BgL_classz00_4037 = BGl_getfieldzf2Cinfozf2zzcfa_info3z00;
				{	/* Cfa/cinfo3.sch 337 */
					obj_t BgL__ortest_1117z00_4038;

					BgL__ortest_1117z00_4038 = BGL_CLASS_NIL(BgL_classz00_4037);
					if (CBOOL(BgL__ortest_1117z00_4038))
						{	/* Cfa/cinfo3.sch 337 */
							return ((BgL_getfieldz00_bglt) BgL__ortest_1117z00_4038);
						}
					else
						{	/* Cfa/cinfo3.sch 337 */
							return
								((BgL_getfieldz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4037));
						}
				}
			}
		}

	}



/* &getfield/Cinfo-nil */
	BgL_getfieldz00_bglt BGl_z62getfieldzf2Cinfozd2nilz42zzcfa_info3z00(obj_t
		BgL_envz00_5196)
	{
		{	/* Cfa/cinfo3.sch 337 */
			return BGl_getfieldzf2Cinfozd2nilz20zzcfa_info3z00();
		}

	}



/* getfield/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_getfieldzf2Cinfozd2approxz20zzcfa_info3z00(BgL_getfieldz00_bglt
		BgL_oz00_48)
	{
		{	/* Cfa/cinfo3.sch 338 */
			{
				BgL_getfieldzf2cinfozf2_bglt BgL_auxz00_7223;

				{
					obj_t BgL_auxz00_7224;

					{	/* Cfa/cinfo3.sch 338 */
						BgL_objectz00_bglt BgL_tmpz00_7225;

						BgL_tmpz00_7225 = ((BgL_objectz00_bglt) BgL_oz00_48);
						BgL_auxz00_7224 = BGL_OBJECT_WIDENING(BgL_tmpz00_7225);
					}
					BgL_auxz00_7223 = ((BgL_getfieldzf2cinfozf2_bglt) BgL_auxz00_7224);
				}
				return
					(((BgL_getfieldzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7223))->
					BgL_approxz00);
			}
		}

	}



/* &getfield/Cinfo-approx */
	BgL_approxz00_bglt BGl_z62getfieldzf2Cinfozd2approxz42zzcfa_info3z00(obj_t
		BgL_envz00_5197, obj_t BgL_oz00_5198)
	{
		{	/* Cfa/cinfo3.sch 338 */
			return
				BGl_getfieldzf2Cinfozd2approxz20zzcfa_info3z00(
				((BgL_getfieldz00_bglt) BgL_oz00_5198));
		}

	}



/* getfield/Cinfo-otype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_getfieldzf2Cinfozd2otypez20zzcfa_info3z00(BgL_getfieldz00_bglt
		BgL_oz00_51)
	{
		{	/* Cfa/cinfo3.sch 340 */
			return
				(((BgL_getfieldz00_bglt) COBJECT(
						((BgL_getfieldz00_bglt) BgL_oz00_51)))->BgL_otypez00);
		}

	}



/* &getfield/Cinfo-otype */
	BgL_typez00_bglt BGl_z62getfieldzf2Cinfozd2otypez42zzcfa_info3z00(obj_t
		BgL_envz00_5199, obj_t BgL_oz00_5200)
	{
		{	/* Cfa/cinfo3.sch 340 */
			return
				BGl_getfieldzf2Cinfozd2otypez20zzcfa_info3z00(
				((BgL_getfieldz00_bglt) BgL_oz00_5200));
		}

	}



/* getfield/Cinfo-otype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_getfieldzf2Cinfozd2otypezd2setz12ze0zzcfa_info3z00(BgL_getfieldz00_bglt
		BgL_oz00_52, BgL_typez00_bglt BgL_vz00_53)
	{
		{	/* Cfa/cinfo3.sch 341 */
			return
				((((BgL_getfieldz00_bglt) COBJECT(
							((BgL_getfieldz00_bglt) BgL_oz00_52)))->BgL_otypez00) =
				((BgL_typez00_bglt) BgL_vz00_53), BUNSPEC);
		}

	}



/* &getfield/Cinfo-otype-set! */
	obj_t BGl_z62getfieldzf2Cinfozd2otypezd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5201, obj_t BgL_oz00_5202, obj_t BgL_vz00_5203)
	{
		{	/* Cfa/cinfo3.sch 341 */
			return
				BGl_getfieldzf2Cinfozd2otypezd2setz12ze0zzcfa_info3z00(
				((BgL_getfieldz00_bglt) BgL_oz00_5202),
				((BgL_typez00_bglt) BgL_vz00_5203));
		}

	}



/* getfield/Cinfo-ftype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_getfieldzf2Cinfozd2ftypez20zzcfa_info3z00(BgL_getfieldz00_bglt
		BgL_oz00_54)
	{
		{	/* Cfa/cinfo3.sch 342 */
			return
				(((BgL_getfieldz00_bglt) COBJECT(
						((BgL_getfieldz00_bglt) BgL_oz00_54)))->BgL_ftypez00);
		}

	}



/* &getfield/Cinfo-ftype */
	BgL_typez00_bglt BGl_z62getfieldzf2Cinfozd2ftypez42zzcfa_info3z00(obj_t
		BgL_envz00_5204, obj_t BgL_oz00_5205)
	{
		{	/* Cfa/cinfo3.sch 342 */
			return
				BGl_getfieldzf2Cinfozd2ftypez20zzcfa_info3z00(
				((BgL_getfieldz00_bglt) BgL_oz00_5205));
		}

	}



/* getfield/Cinfo-ftype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_getfieldzf2Cinfozd2ftypezd2setz12ze0zzcfa_info3z00(BgL_getfieldz00_bglt
		BgL_oz00_55, BgL_typez00_bglt BgL_vz00_56)
	{
		{	/* Cfa/cinfo3.sch 343 */
			return
				((((BgL_getfieldz00_bglt) COBJECT(
							((BgL_getfieldz00_bglt) BgL_oz00_55)))->BgL_ftypez00) =
				((BgL_typez00_bglt) BgL_vz00_56), BUNSPEC);
		}

	}



/* &getfield/Cinfo-ftype-set! */
	obj_t BGl_z62getfieldzf2Cinfozd2ftypezd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5206, obj_t BgL_oz00_5207, obj_t BgL_vz00_5208)
	{
		{	/* Cfa/cinfo3.sch 343 */
			return
				BGl_getfieldzf2Cinfozd2ftypezd2setz12ze0zzcfa_info3z00(
				((BgL_getfieldz00_bglt) BgL_oz00_5207),
				((BgL_typez00_bglt) BgL_vz00_5208));
		}

	}



/* getfield/Cinfo-fname */
	BGL_EXPORTED_DEF obj_t
		BGl_getfieldzf2Cinfozd2fnamez20zzcfa_info3z00(BgL_getfieldz00_bglt
		BgL_oz00_57)
	{
		{	/* Cfa/cinfo3.sch 344 */
			return
				(((BgL_getfieldz00_bglt) COBJECT(
						((BgL_getfieldz00_bglt) BgL_oz00_57)))->BgL_fnamez00);
		}

	}



/* &getfield/Cinfo-fname */
	obj_t BGl_z62getfieldzf2Cinfozd2fnamez42zzcfa_info3z00(obj_t BgL_envz00_5209,
		obj_t BgL_oz00_5210)
	{
		{	/* Cfa/cinfo3.sch 344 */
			return
				BGl_getfieldzf2Cinfozd2fnamez20zzcfa_info3z00(
				((BgL_getfieldz00_bglt) BgL_oz00_5210));
		}

	}



/* getfield/Cinfo-c-format */
	BGL_EXPORTED_DEF obj_t
		BGl_getfieldzf2Cinfozd2czd2formatzf2zzcfa_info3z00(BgL_getfieldz00_bglt
		BgL_oz00_60)
	{
		{	/* Cfa/cinfo3.sch 346 */
			return
				(((BgL_privatez00_bglt) COBJECT(
						((BgL_privatez00_bglt) BgL_oz00_60)))->BgL_czd2formatzd2);
		}

	}



/* &getfield/Cinfo-c-format */
	obj_t BGl_z62getfieldzf2Cinfozd2czd2formatz90zzcfa_info3z00(obj_t
		BgL_envz00_5211, obj_t BgL_oz00_5212)
	{
		{	/* Cfa/cinfo3.sch 346 */
			return
				BGl_getfieldzf2Cinfozd2czd2formatzf2zzcfa_info3z00(
				((BgL_getfieldz00_bglt) BgL_oz00_5212));
		}

	}



/* getfield/Cinfo-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_getfieldzf2Cinfozd2effectz20zzcfa_info3z00(BgL_getfieldz00_bglt
		BgL_oz00_63)
	{
		{	/* Cfa/cinfo3.sch 348 */
			return
				(((BgL_externz00_bglt) COBJECT(
						((BgL_externz00_bglt) BgL_oz00_63)))->BgL_effectz00);
		}

	}



/* &getfield/Cinfo-effect */
	obj_t BGl_z62getfieldzf2Cinfozd2effectz42zzcfa_info3z00(obj_t BgL_envz00_5213,
		obj_t BgL_oz00_5214)
	{
		{	/* Cfa/cinfo3.sch 348 */
			return
				BGl_getfieldzf2Cinfozd2effectz20zzcfa_info3z00(
				((BgL_getfieldz00_bglt) BgL_oz00_5214));
		}

	}



/* getfield/Cinfo-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_getfieldzf2Cinfozd2effectzd2setz12ze0zzcfa_info3z00(BgL_getfieldz00_bglt
		BgL_oz00_64, obj_t BgL_vz00_65)
	{
		{	/* Cfa/cinfo3.sch 349 */
			return
				((((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_oz00_64)))->BgL_effectz00) =
				((obj_t) BgL_vz00_65), BUNSPEC);
		}

	}



/* &getfield/Cinfo-effect-set! */
	obj_t BGl_z62getfieldzf2Cinfozd2effectzd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5215, obj_t BgL_oz00_5216, obj_t BgL_vz00_5217)
	{
		{	/* Cfa/cinfo3.sch 349 */
			return
				BGl_getfieldzf2Cinfozd2effectzd2setz12ze0zzcfa_info3z00(
				((BgL_getfieldz00_bglt) BgL_oz00_5216), BgL_vz00_5217);
		}

	}



/* getfield/Cinfo-expr* */
	BGL_EXPORTED_DEF obj_t
		BGl_getfieldzf2Cinfozd2exprza2z82zzcfa_info3z00(BgL_getfieldz00_bglt
		BgL_oz00_66)
	{
		{	/* Cfa/cinfo3.sch 350 */
			return
				(((BgL_externz00_bglt) COBJECT(
						((BgL_externz00_bglt) BgL_oz00_66)))->BgL_exprza2za2);
		}

	}



/* &getfield/Cinfo-expr* */
	obj_t BGl_z62getfieldzf2Cinfozd2exprza2ze0zzcfa_info3z00(obj_t
		BgL_envz00_5218, obj_t BgL_oz00_5219)
	{
		{	/* Cfa/cinfo3.sch 350 */
			return
				BGl_getfieldzf2Cinfozd2exprza2z82zzcfa_info3z00(
				((BgL_getfieldz00_bglt) BgL_oz00_5219));
		}

	}



/* getfield/Cinfo-expr*-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_getfieldzf2Cinfozd2exprza2zd2setz12z42zzcfa_info3z00
		(BgL_getfieldz00_bglt BgL_oz00_67, obj_t BgL_vz00_68)
	{
		{	/* Cfa/cinfo3.sch 351 */
			return
				((((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_oz00_67)))->BgL_exprza2za2) =
				((obj_t) BgL_vz00_68), BUNSPEC);
		}

	}



/* &getfield/Cinfo-expr*-set! */
	obj_t BGl_z62getfieldzf2Cinfozd2exprza2zd2setz12z20zzcfa_info3z00(obj_t
		BgL_envz00_5220, obj_t BgL_oz00_5221, obj_t BgL_vz00_5222)
	{
		{	/* Cfa/cinfo3.sch 351 */
			return
				BGl_getfieldzf2Cinfozd2exprza2zd2setz12z42zzcfa_info3z00(
				((BgL_getfieldz00_bglt) BgL_oz00_5221), BgL_vz00_5222);
		}

	}



/* getfield/Cinfo-key */
	BGL_EXPORTED_DEF obj_t
		BGl_getfieldzf2Cinfozd2keyz20zzcfa_info3z00(BgL_getfieldz00_bglt
		BgL_oz00_69)
	{
		{	/* Cfa/cinfo3.sch 352 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_69)))->BgL_keyz00);
		}

	}



/* &getfield/Cinfo-key */
	obj_t BGl_z62getfieldzf2Cinfozd2keyz42zzcfa_info3z00(obj_t BgL_envz00_5223,
		obj_t BgL_oz00_5224)
	{
		{	/* Cfa/cinfo3.sch 352 */
			return
				BGl_getfieldzf2Cinfozd2keyz20zzcfa_info3z00(
				((BgL_getfieldz00_bglt) BgL_oz00_5224));
		}

	}



/* getfield/Cinfo-key-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_getfieldzf2Cinfozd2keyzd2setz12ze0zzcfa_info3z00(BgL_getfieldz00_bglt
		BgL_oz00_70, obj_t BgL_vz00_71)
	{
		{	/* Cfa/cinfo3.sch 353 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_70)))->BgL_keyz00) =
				((obj_t) BgL_vz00_71), BUNSPEC);
		}

	}



/* &getfield/Cinfo-key-set! */
	obj_t BGl_z62getfieldzf2Cinfozd2keyzd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5225, obj_t BgL_oz00_5226, obj_t BgL_vz00_5227)
	{
		{	/* Cfa/cinfo3.sch 353 */
			return
				BGl_getfieldzf2Cinfozd2keyzd2setz12ze0zzcfa_info3z00(
				((BgL_getfieldz00_bglt) BgL_oz00_5226), BgL_vz00_5227);
		}

	}



/* getfield/Cinfo-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_getfieldzf2Cinfozd2sidezd2effectzf2zzcfa_info3z00(BgL_getfieldz00_bglt
		BgL_oz00_72)
	{
		{	/* Cfa/cinfo3.sch 354 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_72)))->BgL_sidezd2effectzd2);
		}

	}



/* &getfield/Cinfo-side-effect */
	obj_t BGl_z62getfieldzf2Cinfozd2sidezd2effectz90zzcfa_info3z00(obj_t
		BgL_envz00_5228, obj_t BgL_oz00_5229)
	{
		{	/* Cfa/cinfo3.sch 354 */
			return
				BGl_getfieldzf2Cinfozd2sidezd2effectzf2zzcfa_info3z00(
				((BgL_getfieldz00_bglt) BgL_oz00_5229));
		}

	}



/* getfield/Cinfo-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_getfieldzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_info3z00
		(BgL_getfieldz00_bglt BgL_oz00_73, obj_t BgL_vz00_74)
	{
		{	/* Cfa/cinfo3.sch 355 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_73)))->
					BgL_sidezd2effectzd2) = ((obj_t) BgL_vz00_74), BUNSPEC);
		}

	}



/* &getfield/Cinfo-side-effect-set! */
	obj_t BGl_z62getfieldzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_info3z00(obj_t
		BgL_envz00_5230, obj_t BgL_oz00_5231, obj_t BgL_vz00_5232)
	{
		{	/* Cfa/cinfo3.sch 355 */
			return
				BGl_getfieldzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_info3z00(
				((BgL_getfieldz00_bglt) BgL_oz00_5231), BgL_vz00_5232);
		}

	}



/* getfield/Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_getfieldzf2Cinfozd2typez20zzcfa_info3z00(BgL_getfieldz00_bglt
		BgL_oz00_75)
	{
		{	/* Cfa/cinfo3.sch 356 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_75)))->BgL_typez00);
		}

	}



/* &getfield/Cinfo-type */
	BgL_typez00_bglt BGl_z62getfieldzf2Cinfozd2typez42zzcfa_info3z00(obj_t
		BgL_envz00_5233, obj_t BgL_oz00_5234)
	{
		{	/* Cfa/cinfo3.sch 356 */
			return
				BGl_getfieldzf2Cinfozd2typez20zzcfa_info3z00(
				((BgL_getfieldz00_bglt) BgL_oz00_5234));
		}

	}



/* getfield/Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_getfieldzf2Cinfozd2typezd2setz12ze0zzcfa_info3z00(BgL_getfieldz00_bglt
		BgL_oz00_76, BgL_typez00_bglt BgL_vz00_77)
	{
		{	/* Cfa/cinfo3.sch 357 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_76)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_77), BUNSPEC);
		}

	}



/* &getfield/Cinfo-type-set! */
	obj_t BGl_z62getfieldzf2Cinfozd2typezd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5235, obj_t BgL_oz00_5236, obj_t BgL_vz00_5237)
	{
		{	/* Cfa/cinfo3.sch 357 */
			return
				BGl_getfieldzf2Cinfozd2typezd2setz12ze0zzcfa_info3z00(
				((BgL_getfieldz00_bglt) BgL_oz00_5236),
				((BgL_typez00_bglt) BgL_vz00_5237));
		}

	}



/* getfield/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_getfieldzf2Cinfozd2locz20zzcfa_info3z00(BgL_getfieldz00_bglt
		BgL_oz00_78)
	{
		{	/* Cfa/cinfo3.sch 358 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_78)))->BgL_locz00);
		}

	}



/* &getfield/Cinfo-loc */
	obj_t BGl_z62getfieldzf2Cinfozd2locz42zzcfa_info3z00(obj_t BgL_envz00_5238,
		obj_t BgL_oz00_5239)
	{
		{	/* Cfa/cinfo3.sch 358 */
			return
				BGl_getfieldzf2Cinfozd2locz20zzcfa_info3z00(
				((BgL_getfieldz00_bglt) BgL_oz00_5239));
		}

	}



/* make-setfield/Cinfo */
	BGL_EXPORTED_DEF BgL_setfieldz00_bglt
		BGl_makezd2setfieldzf2Cinfoz20zzcfa_info3z00(obj_t BgL_loc1447z00_81,
		BgL_typez00_bglt BgL_type1448z00_82, obj_t BgL_sidezd2effect1449zd2_83,
		obj_t BgL_key1450z00_84, obj_t BgL_exprza21451za2_85,
		obj_t BgL_effect1452z00_86, obj_t BgL_czd2format1453zd2_87,
		obj_t BgL_fname1454z00_88, BgL_typez00_bglt BgL_ftype1455z00_89,
		BgL_typez00_bglt BgL_otype1456z00_90,
		BgL_approxz00_bglt BgL_approx1457z00_91)
	{
		{	/* Cfa/cinfo3.sch 362 */
			{	/* Cfa/cinfo3.sch 362 */
				BgL_setfieldz00_bglt BgL_new1349z00_6312;

				{	/* Cfa/cinfo3.sch 362 */
					BgL_setfieldz00_bglt BgL_tmp1347z00_6313;
					BgL_setfieldzf2cinfozf2_bglt BgL_wide1348z00_6314;

					{
						BgL_setfieldz00_bglt BgL_auxz00_7303;

						{	/* Cfa/cinfo3.sch 362 */
							BgL_setfieldz00_bglt BgL_new1346z00_6315;

							BgL_new1346z00_6315 =
								((BgL_setfieldz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_setfieldz00_bgl))));
							{	/* Cfa/cinfo3.sch 362 */
								long BgL_arg1711z00_6316;

								BgL_arg1711z00_6316 =
									BGL_CLASS_NUM(BGl_setfieldz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1346z00_6315),
									BgL_arg1711z00_6316);
							}
							{	/* Cfa/cinfo3.sch 362 */
								BgL_objectz00_bglt BgL_tmpz00_7308;

								BgL_tmpz00_7308 = ((BgL_objectz00_bglt) BgL_new1346z00_6315);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7308, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1346z00_6315);
							BgL_auxz00_7303 = BgL_new1346z00_6315;
						}
						BgL_tmp1347z00_6313 = ((BgL_setfieldz00_bglt) BgL_auxz00_7303);
					}
					BgL_wide1348z00_6314 =
						((BgL_setfieldzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_setfieldzf2cinfozf2_bgl))));
					{	/* Cfa/cinfo3.sch 362 */
						obj_t BgL_auxz00_7316;
						BgL_objectz00_bglt BgL_tmpz00_7314;

						BgL_auxz00_7316 = ((obj_t) BgL_wide1348z00_6314);
						BgL_tmpz00_7314 = ((BgL_objectz00_bglt) BgL_tmp1347z00_6313);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7314, BgL_auxz00_7316);
					}
					((BgL_objectz00_bglt) BgL_tmp1347z00_6313);
					{	/* Cfa/cinfo3.sch 362 */
						long BgL_arg1710z00_6317;

						BgL_arg1710z00_6317 =
							BGL_CLASS_NUM(BGl_setfieldzf2Cinfozf2zzcfa_info3z00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1347z00_6313), BgL_arg1710z00_6317);
					}
					BgL_new1349z00_6312 = ((BgL_setfieldz00_bglt) BgL_tmp1347z00_6313);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1349z00_6312)))->BgL_locz00) =
					((obj_t) BgL_loc1447z00_81), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1349z00_6312)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1448z00_82), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1349z00_6312)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1449zd2_83), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1349z00_6312)))->BgL_keyz00) =
					((obj_t) BgL_key1450z00_84), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1349z00_6312)))->BgL_exprza2za2) =
					((obj_t) BgL_exprza21451za2_85), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1349z00_6312)))->BgL_effectz00) =
					((obj_t) BgL_effect1452z00_86), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1349z00_6312)))->BgL_czd2formatzd2) =
					((obj_t) BgL_czd2format1453zd2_87), BUNSPEC);
				((((BgL_setfieldz00_bglt) COBJECT(((BgL_setfieldz00_bglt)
									BgL_new1349z00_6312)))->BgL_fnamez00) =
					((obj_t) BgL_fname1454z00_88), BUNSPEC);
				((((BgL_setfieldz00_bglt) COBJECT(((BgL_setfieldz00_bglt)
									BgL_new1349z00_6312)))->BgL_ftypez00) =
					((BgL_typez00_bglt) BgL_ftype1455z00_89), BUNSPEC);
				((((BgL_setfieldz00_bglt) COBJECT(((BgL_setfieldz00_bglt)
									BgL_new1349z00_6312)))->BgL_otypez00) =
					((BgL_typez00_bglt) BgL_otype1456z00_90), BUNSPEC);
				{
					BgL_setfieldzf2cinfozf2_bglt BgL_auxz00_7344;

					{
						obj_t BgL_auxz00_7345;

						{	/* Cfa/cinfo3.sch 362 */
							BgL_objectz00_bglt BgL_tmpz00_7346;

							BgL_tmpz00_7346 = ((BgL_objectz00_bglt) BgL_new1349z00_6312);
							BgL_auxz00_7345 = BGL_OBJECT_WIDENING(BgL_tmpz00_7346);
						}
						BgL_auxz00_7344 = ((BgL_setfieldzf2cinfozf2_bglt) BgL_auxz00_7345);
					}
					((((BgL_setfieldzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7344))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1457z00_91), BUNSPEC);
				}
				return BgL_new1349z00_6312;
			}
		}

	}



/* &make-setfield/Cinfo */
	BgL_setfieldz00_bglt BGl_z62makezd2setfieldzf2Cinfoz42zzcfa_info3z00(obj_t
		BgL_envz00_5240, obj_t BgL_loc1447z00_5241, obj_t BgL_type1448z00_5242,
		obj_t BgL_sidezd2effect1449zd2_5243, obj_t BgL_key1450z00_5244,
		obj_t BgL_exprza21451za2_5245, obj_t BgL_effect1452z00_5246,
		obj_t BgL_czd2format1453zd2_5247, obj_t BgL_fname1454z00_5248,
		obj_t BgL_ftype1455z00_5249, obj_t BgL_otype1456z00_5250,
		obj_t BgL_approx1457z00_5251)
	{
		{	/* Cfa/cinfo3.sch 362 */
			return
				BGl_makezd2setfieldzf2Cinfoz20zzcfa_info3z00(BgL_loc1447z00_5241,
				((BgL_typez00_bglt) BgL_type1448z00_5242),
				BgL_sidezd2effect1449zd2_5243, BgL_key1450z00_5244,
				BgL_exprza21451za2_5245, BgL_effect1452z00_5246,
				BgL_czd2format1453zd2_5247, BgL_fname1454z00_5248,
				((BgL_typez00_bglt) BgL_ftype1455z00_5249),
				((BgL_typez00_bglt) BgL_otype1456z00_5250),
				((BgL_approxz00_bglt) BgL_approx1457z00_5251));
		}

	}



/* setfield/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_setfieldzf2Cinfozf3z01zzcfa_info3z00(obj_t
		BgL_objz00_92)
	{
		{	/* Cfa/cinfo3.sch 363 */
			{	/* Cfa/cinfo3.sch 363 */
				obj_t BgL_classz00_6318;

				BgL_classz00_6318 = BGl_setfieldzf2Cinfozf2zzcfa_info3z00;
				if (BGL_OBJECTP(BgL_objz00_92))
					{	/* Cfa/cinfo3.sch 363 */
						BgL_objectz00_bglt BgL_arg1809z00_6319;
						long BgL_arg1810z00_6320;

						BgL_arg1809z00_6319 = (BgL_objectz00_bglt) (BgL_objz00_92);
						BgL_arg1810z00_6320 = BGL_CLASS_DEPTH(BgL_classz00_6318);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo3.sch 363 */
								long BgL_idxz00_6321;

								BgL_idxz00_6321 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1809z00_6319);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6321 + BgL_arg1810z00_6320)) ==
									BgL_classz00_6318);
							}
						else
							{	/* Cfa/cinfo3.sch 363 */
								bool_t BgL_res2257z00_6324;

								{	/* Cfa/cinfo3.sch 363 */
									obj_t BgL_oclassz00_6325;

									{	/* Cfa/cinfo3.sch 363 */
										obj_t BgL_arg1815z00_6326;
										long BgL_arg1816z00_6327;

										BgL_arg1815z00_6326 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo3.sch 363 */
											long BgL_arg1817z00_6328;

											BgL_arg1817z00_6328 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1809z00_6319);
											BgL_arg1816z00_6327 = (BgL_arg1817z00_6328 - OBJECT_TYPE);
										}
										BgL_oclassz00_6325 =
											VECTOR_REF(BgL_arg1815z00_6326, BgL_arg1816z00_6327);
									}
									{	/* Cfa/cinfo3.sch 363 */
										bool_t BgL__ortest_1115z00_6329;

										BgL__ortest_1115z00_6329 =
											(BgL_classz00_6318 == BgL_oclassz00_6325);
										if (BgL__ortest_1115z00_6329)
											{	/* Cfa/cinfo3.sch 363 */
												BgL_res2257z00_6324 = BgL__ortest_1115z00_6329;
											}
										else
											{	/* Cfa/cinfo3.sch 363 */
												long BgL_odepthz00_6330;

												{	/* Cfa/cinfo3.sch 363 */
													obj_t BgL_arg1804z00_6331;

													BgL_arg1804z00_6331 = (BgL_oclassz00_6325);
													BgL_odepthz00_6330 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6331);
												}
												if ((BgL_arg1810z00_6320 < BgL_odepthz00_6330))
													{	/* Cfa/cinfo3.sch 363 */
														obj_t BgL_arg1802z00_6332;

														{	/* Cfa/cinfo3.sch 363 */
															obj_t BgL_arg1803z00_6333;

															BgL_arg1803z00_6333 = (BgL_oclassz00_6325);
															BgL_arg1802z00_6332 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6333,
																BgL_arg1810z00_6320);
														}
														BgL_res2257z00_6324 =
															(BgL_arg1802z00_6332 == BgL_classz00_6318);
													}
												else
													{	/* Cfa/cinfo3.sch 363 */
														BgL_res2257z00_6324 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2257z00_6324;
							}
					}
				else
					{	/* Cfa/cinfo3.sch 363 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &setfield/Cinfo? */
	obj_t BGl_z62setfieldzf2Cinfozf3z63zzcfa_info3z00(obj_t BgL_envz00_5252,
		obj_t BgL_objz00_5253)
	{
		{	/* Cfa/cinfo3.sch 363 */
			return BBOOL(BGl_setfieldzf2Cinfozf3z01zzcfa_info3z00(BgL_objz00_5253));
		}

	}



/* setfield/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_setfieldz00_bglt
		BGl_setfieldzf2Cinfozd2nilz20zzcfa_info3z00(void)
	{
		{	/* Cfa/cinfo3.sch 364 */
			{	/* Cfa/cinfo3.sch 364 */
				obj_t BgL_classz00_4095;

				BgL_classz00_4095 = BGl_setfieldzf2Cinfozf2zzcfa_info3z00;
				{	/* Cfa/cinfo3.sch 364 */
					obj_t BgL__ortest_1117z00_4096;

					BgL__ortest_1117z00_4096 = BGL_CLASS_NIL(BgL_classz00_4095);
					if (CBOOL(BgL__ortest_1117z00_4096))
						{	/* Cfa/cinfo3.sch 364 */
							return ((BgL_setfieldz00_bglt) BgL__ortest_1117z00_4096);
						}
					else
						{	/* Cfa/cinfo3.sch 364 */
							return
								((BgL_setfieldz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4095));
						}
				}
			}
		}

	}



/* &setfield/Cinfo-nil */
	BgL_setfieldz00_bglt BGl_z62setfieldzf2Cinfozd2nilz42zzcfa_info3z00(obj_t
		BgL_envz00_5254)
	{
		{	/* Cfa/cinfo3.sch 364 */
			return BGl_setfieldzf2Cinfozd2nilz20zzcfa_info3z00();
		}

	}



/* setfield/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_setfieldzf2Cinfozd2approxz20zzcfa_info3z00(BgL_setfieldz00_bglt
		BgL_oz00_93)
	{
		{	/* Cfa/cinfo3.sch 365 */
			{
				BgL_setfieldzf2cinfozf2_bglt BgL_auxz00_7388;

				{
					obj_t BgL_auxz00_7389;

					{	/* Cfa/cinfo3.sch 365 */
						BgL_objectz00_bglt BgL_tmpz00_7390;

						BgL_tmpz00_7390 = ((BgL_objectz00_bglt) BgL_oz00_93);
						BgL_auxz00_7389 = BGL_OBJECT_WIDENING(BgL_tmpz00_7390);
					}
					BgL_auxz00_7388 = ((BgL_setfieldzf2cinfozf2_bglt) BgL_auxz00_7389);
				}
				return
					(((BgL_setfieldzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7388))->
					BgL_approxz00);
			}
		}

	}



/* &setfield/Cinfo-approx */
	BgL_approxz00_bglt BGl_z62setfieldzf2Cinfozd2approxz42zzcfa_info3z00(obj_t
		BgL_envz00_5255, obj_t BgL_oz00_5256)
	{
		{	/* Cfa/cinfo3.sch 365 */
			return
				BGl_setfieldzf2Cinfozd2approxz20zzcfa_info3z00(
				((BgL_setfieldz00_bglt) BgL_oz00_5256));
		}

	}



/* setfield/Cinfo-otype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_setfieldzf2Cinfozd2otypez20zzcfa_info3z00(BgL_setfieldz00_bglt
		BgL_oz00_96)
	{
		{	/* Cfa/cinfo3.sch 367 */
			return
				(((BgL_setfieldz00_bglt) COBJECT(
						((BgL_setfieldz00_bglt) BgL_oz00_96)))->BgL_otypez00);
		}

	}



/* &setfield/Cinfo-otype */
	BgL_typez00_bglt BGl_z62setfieldzf2Cinfozd2otypez42zzcfa_info3z00(obj_t
		BgL_envz00_5257, obj_t BgL_oz00_5258)
	{
		{	/* Cfa/cinfo3.sch 367 */
			return
				BGl_setfieldzf2Cinfozd2otypez20zzcfa_info3z00(
				((BgL_setfieldz00_bglt) BgL_oz00_5258));
		}

	}



/* setfield/Cinfo-otype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_setfieldzf2Cinfozd2otypezd2setz12ze0zzcfa_info3z00(BgL_setfieldz00_bglt
		BgL_oz00_97, BgL_typez00_bglt BgL_vz00_98)
	{
		{	/* Cfa/cinfo3.sch 368 */
			return
				((((BgL_setfieldz00_bglt) COBJECT(
							((BgL_setfieldz00_bglt) BgL_oz00_97)))->BgL_otypez00) =
				((BgL_typez00_bglt) BgL_vz00_98), BUNSPEC);
		}

	}



/* &setfield/Cinfo-otype-set! */
	obj_t BGl_z62setfieldzf2Cinfozd2otypezd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5259, obj_t BgL_oz00_5260, obj_t BgL_vz00_5261)
	{
		{	/* Cfa/cinfo3.sch 368 */
			return
				BGl_setfieldzf2Cinfozd2otypezd2setz12ze0zzcfa_info3z00(
				((BgL_setfieldz00_bglt) BgL_oz00_5260),
				((BgL_typez00_bglt) BgL_vz00_5261));
		}

	}



/* setfield/Cinfo-ftype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_setfieldzf2Cinfozd2ftypez20zzcfa_info3z00(BgL_setfieldz00_bglt
		BgL_oz00_99)
	{
		{	/* Cfa/cinfo3.sch 369 */
			return
				(((BgL_setfieldz00_bglt) COBJECT(
						((BgL_setfieldz00_bglt) BgL_oz00_99)))->BgL_ftypez00);
		}

	}



/* &setfield/Cinfo-ftype */
	BgL_typez00_bglt BGl_z62setfieldzf2Cinfozd2ftypez42zzcfa_info3z00(obj_t
		BgL_envz00_5262, obj_t BgL_oz00_5263)
	{
		{	/* Cfa/cinfo3.sch 369 */
			return
				BGl_setfieldzf2Cinfozd2ftypez20zzcfa_info3z00(
				((BgL_setfieldz00_bglt) BgL_oz00_5263));
		}

	}



/* setfield/Cinfo-ftype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_setfieldzf2Cinfozd2ftypezd2setz12ze0zzcfa_info3z00(BgL_setfieldz00_bglt
		BgL_oz00_100, BgL_typez00_bglt BgL_vz00_101)
	{
		{	/* Cfa/cinfo3.sch 370 */
			return
				((((BgL_setfieldz00_bglt) COBJECT(
							((BgL_setfieldz00_bglt) BgL_oz00_100)))->BgL_ftypez00) =
				((BgL_typez00_bglt) BgL_vz00_101), BUNSPEC);
		}

	}



/* &setfield/Cinfo-ftype-set! */
	obj_t BGl_z62setfieldzf2Cinfozd2ftypezd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5264, obj_t BgL_oz00_5265, obj_t BgL_vz00_5266)
	{
		{	/* Cfa/cinfo3.sch 370 */
			return
				BGl_setfieldzf2Cinfozd2ftypezd2setz12ze0zzcfa_info3z00(
				((BgL_setfieldz00_bglt) BgL_oz00_5265),
				((BgL_typez00_bglt) BgL_vz00_5266));
		}

	}



/* setfield/Cinfo-fname */
	BGL_EXPORTED_DEF obj_t
		BGl_setfieldzf2Cinfozd2fnamez20zzcfa_info3z00(BgL_setfieldz00_bglt
		BgL_oz00_102)
	{
		{	/* Cfa/cinfo3.sch 371 */
			return
				(((BgL_setfieldz00_bglt) COBJECT(
						((BgL_setfieldz00_bglt) BgL_oz00_102)))->BgL_fnamez00);
		}

	}



/* &setfield/Cinfo-fname */
	obj_t BGl_z62setfieldzf2Cinfozd2fnamez42zzcfa_info3z00(obj_t BgL_envz00_5267,
		obj_t BgL_oz00_5268)
	{
		{	/* Cfa/cinfo3.sch 371 */
			return
				BGl_setfieldzf2Cinfozd2fnamez20zzcfa_info3z00(
				((BgL_setfieldz00_bglt) BgL_oz00_5268));
		}

	}



/* setfield/Cinfo-c-format */
	BGL_EXPORTED_DEF obj_t
		BGl_setfieldzf2Cinfozd2czd2formatzf2zzcfa_info3z00(BgL_setfieldz00_bglt
		BgL_oz00_105)
	{
		{	/* Cfa/cinfo3.sch 373 */
			return
				(((BgL_privatez00_bglt) COBJECT(
						((BgL_privatez00_bglt) BgL_oz00_105)))->BgL_czd2formatzd2);
		}

	}



/* &setfield/Cinfo-c-format */
	obj_t BGl_z62setfieldzf2Cinfozd2czd2formatz90zzcfa_info3z00(obj_t
		BgL_envz00_5269, obj_t BgL_oz00_5270)
	{
		{	/* Cfa/cinfo3.sch 373 */
			return
				BGl_setfieldzf2Cinfozd2czd2formatzf2zzcfa_info3z00(
				((BgL_setfieldz00_bglt) BgL_oz00_5270));
		}

	}



/* setfield/Cinfo-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_setfieldzf2Cinfozd2effectz20zzcfa_info3z00(BgL_setfieldz00_bglt
		BgL_oz00_108)
	{
		{	/* Cfa/cinfo3.sch 375 */
			return
				(((BgL_externz00_bglt) COBJECT(
						((BgL_externz00_bglt) BgL_oz00_108)))->BgL_effectz00);
		}

	}



/* &setfield/Cinfo-effect */
	obj_t BGl_z62setfieldzf2Cinfozd2effectz42zzcfa_info3z00(obj_t BgL_envz00_5271,
		obj_t BgL_oz00_5272)
	{
		{	/* Cfa/cinfo3.sch 375 */
			return
				BGl_setfieldzf2Cinfozd2effectz20zzcfa_info3z00(
				((BgL_setfieldz00_bglt) BgL_oz00_5272));
		}

	}



/* setfield/Cinfo-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_setfieldzf2Cinfozd2effectzd2setz12ze0zzcfa_info3z00(BgL_setfieldz00_bglt
		BgL_oz00_109, obj_t BgL_vz00_110)
	{
		{	/* Cfa/cinfo3.sch 376 */
			return
				((((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_oz00_109)))->BgL_effectz00) =
				((obj_t) BgL_vz00_110), BUNSPEC);
		}

	}



/* &setfield/Cinfo-effect-set! */
	obj_t BGl_z62setfieldzf2Cinfozd2effectzd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5273, obj_t BgL_oz00_5274, obj_t BgL_vz00_5275)
	{
		{	/* Cfa/cinfo3.sch 376 */
			return
				BGl_setfieldzf2Cinfozd2effectzd2setz12ze0zzcfa_info3z00(
				((BgL_setfieldz00_bglt) BgL_oz00_5274), BgL_vz00_5275);
		}

	}



/* setfield/Cinfo-expr* */
	BGL_EXPORTED_DEF obj_t
		BGl_setfieldzf2Cinfozd2exprza2z82zzcfa_info3z00(BgL_setfieldz00_bglt
		BgL_oz00_111)
	{
		{	/* Cfa/cinfo3.sch 377 */
			return
				(((BgL_externz00_bglt) COBJECT(
						((BgL_externz00_bglt) BgL_oz00_111)))->BgL_exprza2za2);
		}

	}



/* &setfield/Cinfo-expr* */
	obj_t BGl_z62setfieldzf2Cinfozd2exprza2ze0zzcfa_info3z00(obj_t
		BgL_envz00_5276, obj_t BgL_oz00_5277)
	{
		{	/* Cfa/cinfo3.sch 377 */
			return
				BGl_setfieldzf2Cinfozd2exprza2z82zzcfa_info3z00(
				((BgL_setfieldz00_bglt) BgL_oz00_5277));
		}

	}



/* setfield/Cinfo-expr*-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_setfieldzf2Cinfozd2exprza2zd2setz12z42zzcfa_info3z00
		(BgL_setfieldz00_bglt BgL_oz00_112, obj_t BgL_vz00_113)
	{
		{	/* Cfa/cinfo3.sch 378 */
			return
				((((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_oz00_112)))->BgL_exprza2za2) =
				((obj_t) BgL_vz00_113), BUNSPEC);
		}

	}



/* &setfield/Cinfo-expr*-set! */
	obj_t BGl_z62setfieldzf2Cinfozd2exprza2zd2setz12z20zzcfa_info3z00(obj_t
		BgL_envz00_5278, obj_t BgL_oz00_5279, obj_t BgL_vz00_5280)
	{
		{	/* Cfa/cinfo3.sch 378 */
			return
				BGl_setfieldzf2Cinfozd2exprza2zd2setz12z42zzcfa_info3z00(
				((BgL_setfieldz00_bglt) BgL_oz00_5279), BgL_vz00_5280);
		}

	}



/* setfield/Cinfo-key */
	BGL_EXPORTED_DEF obj_t
		BGl_setfieldzf2Cinfozd2keyz20zzcfa_info3z00(BgL_setfieldz00_bglt
		BgL_oz00_114)
	{
		{	/* Cfa/cinfo3.sch 379 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_114)))->BgL_keyz00);
		}

	}



/* &setfield/Cinfo-key */
	obj_t BGl_z62setfieldzf2Cinfozd2keyz42zzcfa_info3z00(obj_t BgL_envz00_5281,
		obj_t BgL_oz00_5282)
	{
		{	/* Cfa/cinfo3.sch 379 */
			return
				BGl_setfieldzf2Cinfozd2keyz20zzcfa_info3z00(
				((BgL_setfieldz00_bglt) BgL_oz00_5282));
		}

	}



/* setfield/Cinfo-key-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_setfieldzf2Cinfozd2keyzd2setz12ze0zzcfa_info3z00(BgL_setfieldz00_bglt
		BgL_oz00_115, obj_t BgL_vz00_116)
	{
		{	/* Cfa/cinfo3.sch 380 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_115)))->BgL_keyz00) =
				((obj_t) BgL_vz00_116), BUNSPEC);
		}

	}



/* &setfield/Cinfo-key-set! */
	obj_t BGl_z62setfieldzf2Cinfozd2keyzd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5283, obj_t BgL_oz00_5284, obj_t BgL_vz00_5285)
	{
		{	/* Cfa/cinfo3.sch 380 */
			return
				BGl_setfieldzf2Cinfozd2keyzd2setz12ze0zzcfa_info3z00(
				((BgL_setfieldz00_bglt) BgL_oz00_5284), BgL_vz00_5285);
		}

	}



/* setfield/Cinfo-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_setfieldzf2Cinfozd2sidezd2effectzf2zzcfa_info3z00(BgL_setfieldz00_bglt
		BgL_oz00_117)
	{
		{	/* Cfa/cinfo3.sch 381 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_117)))->BgL_sidezd2effectzd2);
		}

	}



/* &setfield/Cinfo-side-effect */
	obj_t BGl_z62setfieldzf2Cinfozd2sidezd2effectz90zzcfa_info3z00(obj_t
		BgL_envz00_5286, obj_t BgL_oz00_5287)
	{
		{	/* Cfa/cinfo3.sch 381 */
			return
				BGl_setfieldzf2Cinfozd2sidezd2effectzf2zzcfa_info3z00(
				((BgL_setfieldz00_bglt) BgL_oz00_5287));
		}

	}



/* setfield/Cinfo-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_setfieldzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_info3z00
		(BgL_setfieldz00_bglt BgL_oz00_118, obj_t BgL_vz00_119)
	{
		{	/* Cfa/cinfo3.sch 382 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_118)))->
					BgL_sidezd2effectzd2) = ((obj_t) BgL_vz00_119), BUNSPEC);
		}

	}



/* &setfield/Cinfo-side-effect-set! */
	obj_t BGl_z62setfieldzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_info3z00(obj_t
		BgL_envz00_5288, obj_t BgL_oz00_5289, obj_t BgL_vz00_5290)
	{
		{	/* Cfa/cinfo3.sch 382 */
			return
				BGl_setfieldzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_info3z00(
				((BgL_setfieldz00_bglt) BgL_oz00_5289), BgL_vz00_5290);
		}

	}



/* setfield/Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_setfieldzf2Cinfozd2typez20zzcfa_info3z00(BgL_setfieldz00_bglt
		BgL_oz00_120)
	{
		{	/* Cfa/cinfo3.sch 383 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_120)))->BgL_typez00);
		}

	}



/* &setfield/Cinfo-type */
	BgL_typez00_bglt BGl_z62setfieldzf2Cinfozd2typez42zzcfa_info3z00(obj_t
		BgL_envz00_5291, obj_t BgL_oz00_5292)
	{
		{	/* Cfa/cinfo3.sch 383 */
			return
				BGl_setfieldzf2Cinfozd2typez20zzcfa_info3z00(
				((BgL_setfieldz00_bglt) BgL_oz00_5292));
		}

	}



/* setfield/Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_setfieldzf2Cinfozd2typezd2setz12ze0zzcfa_info3z00(BgL_setfieldz00_bglt
		BgL_oz00_121, BgL_typez00_bglt BgL_vz00_122)
	{
		{	/* Cfa/cinfo3.sch 384 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_121)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_122), BUNSPEC);
		}

	}



/* &setfield/Cinfo-type-set! */
	obj_t BGl_z62setfieldzf2Cinfozd2typezd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5293, obj_t BgL_oz00_5294, obj_t BgL_vz00_5295)
	{
		{	/* Cfa/cinfo3.sch 384 */
			return
				BGl_setfieldzf2Cinfozd2typezd2setz12ze0zzcfa_info3z00(
				((BgL_setfieldz00_bglt) BgL_oz00_5294),
				((BgL_typez00_bglt) BgL_vz00_5295));
		}

	}



/* setfield/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_setfieldzf2Cinfozd2locz20zzcfa_info3z00(BgL_setfieldz00_bglt
		BgL_oz00_123)
	{
		{	/* Cfa/cinfo3.sch 385 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_123)))->BgL_locz00);
		}

	}



/* &setfield/Cinfo-loc */
	obj_t BGl_z62setfieldzf2Cinfozd2locz42zzcfa_info3z00(obj_t BgL_envz00_5296,
		obj_t BgL_oz00_5297)
	{
		{	/* Cfa/cinfo3.sch 385 */
			return
				BGl_setfieldzf2Cinfozd2locz20zzcfa_info3z00(
				((BgL_setfieldz00_bglt) BgL_oz00_5297));
		}

	}



/* make-new/Cinfo */
	BGL_EXPORTED_DEF BgL_newz00_bglt BGl_makezd2newzf2Cinfoz20zzcfa_info3z00(obj_t
		BgL_loc1437z00_126, BgL_typez00_bglt BgL_type1438z00_127,
		obj_t BgL_sidezd2effect1439zd2_128, obj_t BgL_key1440z00_129,
		obj_t BgL_exprza21441za2_130, obj_t BgL_effect1442z00_131,
		obj_t BgL_czd2format1443zd2_132, obj_t BgL_argszd2type1444zd2_133,
		BgL_approxz00_bglt BgL_approx1445z00_134)
	{
		{	/* Cfa/cinfo3.sch 389 */
			{	/* Cfa/cinfo3.sch 389 */
				BgL_newz00_bglt BgL_new1353z00_6334;

				{	/* Cfa/cinfo3.sch 389 */
					BgL_newz00_bglt BgL_tmp1351z00_6335;
					BgL_newzf2cinfozf2_bglt BgL_wide1352z00_6336;

					{
						BgL_newz00_bglt BgL_auxz00_7468;

						{	/* Cfa/cinfo3.sch 389 */
							BgL_newz00_bglt BgL_new1350z00_6337;

							BgL_new1350z00_6337 =
								((BgL_newz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_newz00_bgl))));
							{	/* Cfa/cinfo3.sch 389 */
								long BgL_arg1717z00_6338;

								BgL_arg1717z00_6338 = BGL_CLASS_NUM(BGl_newz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1350z00_6337),
									BgL_arg1717z00_6338);
							}
							{	/* Cfa/cinfo3.sch 389 */
								BgL_objectz00_bglt BgL_tmpz00_7473;

								BgL_tmpz00_7473 = ((BgL_objectz00_bglt) BgL_new1350z00_6337);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7473, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1350z00_6337);
							BgL_auxz00_7468 = BgL_new1350z00_6337;
						}
						BgL_tmp1351z00_6335 = ((BgL_newz00_bglt) BgL_auxz00_7468);
					}
					BgL_wide1352z00_6336 =
						((BgL_newzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_newzf2cinfozf2_bgl))));
					{	/* Cfa/cinfo3.sch 389 */
						obj_t BgL_auxz00_7481;
						BgL_objectz00_bglt BgL_tmpz00_7479;

						BgL_auxz00_7481 = ((obj_t) BgL_wide1352z00_6336);
						BgL_tmpz00_7479 = ((BgL_objectz00_bglt) BgL_tmp1351z00_6335);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7479, BgL_auxz00_7481);
					}
					((BgL_objectz00_bglt) BgL_tmp1351z00_6335);
					{	/* Cfa/cinfo3.sch 389 */
						long BgL_arg1714z00_6339;

						BgL_arg1714z00_6339 =
							BGL_CLASS_NUM(BGl_newzf2Cinfozf2zzcfa_info3z00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1351z00_6335), BgL_arg1714z00_6339);
					}
					BgL_new1353z00_6334 = ((BgL_newz00_bglt) BgL_tmp1351z00_6335);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1353z00_6334)))->BgL_locz00) =
					((obj_t) BgL_loc1437z00_126), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1353z00_6334)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1438z00_127), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1353z00_6334)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1439zd2_128), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1353z00_6334)))->BgL_keyz00) =
					((obj_t) BgL_key1440z00_129), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1353z00_6334)))->BgL_exprza2za2) =
					((obj_t) BgL_exprza21441za2_130), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1353z00_6334)))->BgL_effectz00) =
					((obj_t) BgL_effect1442z00_131), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1353z00_6334)))->BgL_czd2formatzd2) =
					((obj_t) BgL_czd2format1443zd2_132), BUNSPEC);
				((((BgL_newz00_bglt) COBJECT(((BgL_newz00_bglt) BgL_new1353z00_6334)))->
						BgL_argszd2typezd2) =
					((obj_t) BgL_argszd2type1444zd2_133), BUNSPEC);
				{
					BgL_newzf2cinfozf2_bglt BgL_auxz00_7505;

					{
						obj_t BgL_auxz00_7506;

						{	/* Cfa/cinfo3.sch 389 */
							BgL_objectz00_bglt BgL_tmpz00_7507;

							BgL_tmpz00_7507 = ((BgL_objectz00_bglt) BgL_new1353z00_6334);
							BgL_auxz00_7506 = BGL_OBJECT_WIDENING(BgL_tmpz00_7507);
						}
						BgL_auxz00_7505 = ((BgL_newzf2cinfozf2_bglt) BgL_auxz00_7506);
					}
					((((BgL_newzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7505))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1445z00_134), BUNSPEC);
				}
				return BgL_new1353z00_6334;
			}
		}

	}



/* &make-new/Cinfo */
	BgL_newz00_bglt BGl_z62makezd2newzf2Cinfoz42zzcfa_info3z00(obj_t
		BgL_envz00_5298, obj_t BgL_loc1437z00_5299, obj_t BgL_type1438z00_5300,
		obj_t BgL_sidezd2effect1439zd2_5301, obj_t BgL_key1440z00_5302,
		obj_t BgL_exprza21441za2_5303, obj_t BgL_effect1442z00_5304,
		obj_t BgL_czd2format1443zd2_5305, obj_t BgL_argszd2type1444zd2_5306,
		obj_t BgL_approx1445z00_5307)
	{
		{	/* Cfa/cinfo3.sch 389 */
			return
				BGl_makezd2newzf2Cinfoz20zzcfa_info3z00(BgL_loc1437z00_5299,
				((BgL_typez00_bglt) BgL_type1438z00_5300),
				BgL_sidezd2effect1439zd2_5301, BgL_key1440z00_5302,
				BgL_exprza21441za2_5303, BgL_effect1442z00_5304,
				BgL_czd2format1443zd2_5305, BgL_argszd2type1444zd2_5306,
				((BgL_approxz00_bglt) BgL_approx1445z00_5307));
		}

	}



/* new/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_newzf2Cinfozf3z01zzcfa_info3z00(obj_t
		BgL_objz00_135)
	{
		{	/* Cfa/cinfo3.sch 390 */
			{	/* Cfa/cinfo3.sch 390 */
				obj_t BgL_classz00_6340;

				BgL_classz00_6340 = BGl_newzf2Cinfozf2zzcfa_info3z00;
				if (BGL_OBJECTP(BgL_objz00_135))
					{	/* Cfa/cinfo3.sch 390 */
						BgL_objectz00_bglt BgL_arg1809z00_6341;
						long BgL_arg1810z00_6342;

						BgL_arg1809z00_6341 = (BgL_objectz00_bglt) (BgL_objz00_135);
						BgL_arg1810z00_6342 = BGL_CLASS_DEPTH(BgL_classz00_6340);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo3.sch 390 */
								long BgL_idxz00_6343;

								BgL_idxz00_6343 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1809z00_6341);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6343 + BgL_arg1810z00_6342)) ==
									BgL_classz00_6340);
							}
						else
							{	/* Cfa/cinfo3.sch 390 */
								bool_t BgL_res2258z00_6346;

								{	/* Cfa/cinfo3.sch 390 */
									obj_t BgL_oclassz00_6347;

									{	/* Cfa/cinfo3.sch 390 */
										obj_t BgL_arg1815z00_6348;
										long BgL_arg1816z00_6349;

										BgL_arg1815z00_6348 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo3.sch 390 */
											long BgL_arg1817z00_6350;

											BgL_arg1817z00_6350 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1809z00_6341);
											BgL_arg1816z00_6349 = (BgL_arg1817z00_6350 - OBJECT_TYPE);
										}
										BgL_oclassz00_6347 =
											VECTOR_REF(BgL_arg1815z00_6348, BgL_arg1816z00_6349);
									}
									{	/* Cfa/cinfo3.sch 390 */
										bool_t BgL__ortest_1115z00_6351;

										BgL__ortest_1115z00_6351 =
											(BgL_classz00_6340 == BgL_oclassz00_6347);
										if (BgL__ortest_1115z00_6351)
											{	/* Cfa/cinfo3.sch 390 */
												BgL_res2258z00_6346 = BgL__ortest_1115z00_6351;
											}
										else
											{	/* Cfa/cinfo3.sch 390 */
												long BgL_odepthz00_6352;

												{	/* Cfa/cinfo3.sch 390 */
													obj_t BgL_arg1804z00_6353;

													BgL_arg1804z00_6353 = (BgL_oclassz00_6347);
													BgL_odepthz00_6352 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6353);
												}
												if ((BgL_arg1810z00_6342 < BgL_odepthz00_6352))
													{	/* Cfa/cinfo3.sch 390 */
														obj_t BgL_arg1802z00_6354;

														{	/* Cfa/cinfo3.sch 390 */
															obj_t BgL_arg1803z00_6355;

															BgL_arg1803z00_6355 = (BgL_oclassz00_6347);
															BgL_arg1802z00_6354 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6355,
																BgL_arg1810z00_6342);
														}
														BgL_res2258z00_6346 =
															(BgL_arg1802z00_6354 == BgL_classz00_6340);
													}
												else
													{	/* Cfa/cinfo3.sch 390 */
														BgL_res2258z00_6346 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2258z00_6346;
							}
					}
				else
					{	/* Cfa/cinfo3.sch 390 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &new/Cinfo? */
	obj_t BGl_z62newzf2Cinfozf3z63zzcfa_info3z00(obj_t BgL_envz00_5308,
		obj_t BgL_objz00_5309)
	{
		{	/* Cfa/cinfo3.sch 390 */
			return BBOOL(BGl_newzf2Cinfozf3z01zzcfa_info3z00(BgL_objz00_5309));
		}

	}



/* new/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_newz00_bglt BGl_newzf2Cinfozd2nilz20zzcfa_info3z00(void)
	{
		{	/* Cfa/cinfo3.sch 391 */
			{	/* Cfa/cinfo3.sch 391 */
				obj_t BgL_classz00_4153;

				BgL_classz00_4153 = BGl_newzf2Cinfozf2zzcfa_info3z00;
				{	/* Cfa/cinfo3.sch 391 */
					obj_t BgL__ortest_1117z00_4154;

					BgL__ortest_1117z00_4154 = BGL_CLASS_NIL(BgL_classz00_4153);
					if (CBOOL(BgL__ortest_1117z00_4154))
						{	/* Cfa/cinfo3.sch 391 */
							return ((BgL_newz00_bglt) BgL__ortest_1117z00_4154);
						}
					else
						{	/* Cfa/cinfo3.sch 391 */
							return
								((BgL_newz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4153));
						}
				}
			}
		}

	}



/* &new/Cinfo-nil */
	BgL_newz00_bglt BGl_z62newzf2Cinfozd2nilz42zzcfa_info3z00(obj_t
		BgL_envz00_5310)
	{
		{	/* Cfa/cinfo3.sch 391 */
			return BGl_newzf2Cinfozd2nilz20zzcfa_info3z00();
		}

	}



/* new/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_newzf2Cinfozd2approxz20zzcfa_info3z00(BgL_newz00_bglt BgL_oz00_136)
	{
		{	/* Cfa/cinfo3.sch 392 */
			{
				BgL_newzf2cinfozf2_bglt BgL_auxz00_7547;

				{
					obj_t BgL_auxz00_7548;

					{	/* Cfa/cinfo3.sch 392 */
						BgL_objectz00_bglt BgL_tmpz00_7549;

						BgL_tmpz00_7549 = ((BgL_objectz00_bglt) BgL_oz00_136);
						BgL_auxz00_7548 = BGL_OBJECT_WIDENING(BgL_tmpz00_7549);
					}
					BgL_auxz00_7547 = ((BgL_newzf2cinfozf2_bglt) BgL_auxz00_7548);
				}
				return
					(((BgL_newzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7547))->BgL_approxz00);
			}
		}

	}



/* &new/Cinfo-approx */
	BgL_approxz00_bglt BGl_z62newzf2Cinfozd2approxz42zzcfa_info3z00(obj_t
		BgL_envz00_5311, obj_t BgL_oz00_5312)
	{
		{	/* Cfa/cinfo3.sch 392 */
			return
				BGl_newzf2Cinfozd2approxz20zzcfa_info3z00(
				((BgL_newz00_bglt) BgL_oz00_5312));
		}

	}



/* new/Cinfo-args-type */
	BGL_EXPORTED_DEF obj_t
		BGl_newzf2Cinfozd2argszd2typezf2zzcfa_info3z00(BgL_newz00_bglt BgL_oz00_139)
	{
		{	/* Cfa/cinfo3.sch 394 */
			return
				(((BgL_newz00_bglt) COBJECT(
						((BgL_newz00_bglt) BgL_oz00_139)))->BgL_argszd2typezd2);
		}

	}



/* &new/Cinfo-args-type */
	obj_t BGl_z62newzf2Cinfozd2argszd2typez90zzcfa_info3z00(obj_t BgL_envz00_5313,
		obj_t BgL_oz00_5314)
	{
		{	/* Cfa/cinfo3.sch 394 */
			return
				BGl_newzf2Cinfozd2argszd2typezf2zzcfa_info3z00(
				((BgL_newz00_bglt) BgL_oz00_5314));
		}

	}



/* new/Cinfo-args-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_newzf2Cinfozd2argszd2typezd2setz12z32zzcfa_info3z00(BgL_newz00_bglt
		BgL_oz00_140, obj_t BgL_vz00_141)
	{
		{	/* Cfa/cinfo3.sch 395 */
			return
				((((BgL_newz00_bglt) COBJECT(
							((BgL_newz00_bglt) BgL_oz00_140)))->BgL_argszd2typezd2) =
				((obj_t) BgL_vz00_141), BUNSPEC);
		}

	}



/* &new/Cinfo-args-type-set! */
	obj_t BGl_z62newzf2Cinfozd2argszd2typezd2setz12z50zzcfa_info3z00(obj_t
		BgL_envz00_5315, obj_t BgL_oz00_5316, obj_t BgL_vz00_5317)
	{
		{	/* Cfa/cinfo3.sch 395 */
			return
				BGl_newzf2Cinfozd2argszd2typezd2setz12z32zzcfa_info3z00(
				((BgL_newz00_bglt) BgL_oz00_5316), BgL_vz00_5317);
		}

	}



/* new/Cinfo-c-format */
	BGL_EXPORTED_DEF obj_t
		BGl_newzf2Cinfozd2czd2formatzf2zzcfa_info3z00(BgL_newz00_bglt BgL_oz00_142)
	{
		{	/* Cfa/cinfo3.sch 396 */
			return
				(((BgL_privatez00_bglt) COBJECT(
						((BgL_privatez00_bglt) BgL_oz00_142)))->BgL_czd2formatzd2);
		}

	}



/* &new/Cinfo-c-format */
	obj_t BGl_z62newzf2Cinfozd2czd2formatz90zzcfa_info3z00(obj_t BgL_envz00_5318,
		obj_t BgL_oz00_5319)
	{
		{	/* Cfa/cinfo3.sch 396 */
			return
				BGl_newzf2Cinfozd2czd2formatzf2zzcfa_info3z00(
				((BgL_newz00_bglt) BgL_oz00_5319));
		}

	}



/* new/Cinfo-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_newzf2Cinfozd2effectz20zzcfa_info3z00(BgL_newz00_bglt BgL_oz00_145)
	{
		{	/* Cfa/cinfo3.sch 398 */
			return
				(((BgL_externz00_bglt) COBJECT(
						((BgL_externz00_bglt) BgL_oz00_145)))->BgL_effectz00);
		}

	}



/* &new/Cinfo-effect */
	obj_t BGl_z62newzf2Cinfozd2effectz42zzcfa_info3z00(obj_t BgL_envz00_5320,
		obj_t BgL_oz00_5321)
	{
		{	/* Cfa/cinfo3.sch 398 */
			return
				BGl_newzf2Cinfozd2effectz20zzcfa_info3z00(
				((BgL_newz00_bglt) BgL_oz00_5321));
		}

	}



/* new/Cinfo-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_newzf2Cinfozd2effectzd2setz12ze0zzcfa_info3z00(BgL_newz00_bglt
		BgL_oz00_146, obj_t BgL_vz00_147)
	{
		{	/* Cfa/cinfo3.sch 399 */
			return
				((((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_oz00_146)))->BgL_effectz00) =
				((obj_t) BgL_vz00_147), BUNSPEC);
		}

	}



/* &new/Cinfo-effect-set! */
	obj_t BGl_z62newzf2Cinfozd2effectzd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5322, obj_t BgL_oz00_5323, obj_t BgL_vz00_5324)
	{
		{	/* Cfa/cinfo3.sch 399 */
			return
				BGl_newzf2Cinfozd2effectzd2setz12ze0zzcfa_info3z00(
				((BgL_newz00_bglt) BgL_oz00_5323), BgL_vz00_5324);
		}

	}



/* new/Cinfo-expr* */
	BGL_EXPORTED_DEF obj_t
		BGl_newzf2Cinfozd2exprza2z82zzcfa_info3z00(BgL_newz00_bglt BgL_oz00_148)
	{
		{	/* Cfa/cinfo3.sch 400 */
			return
				(((BgL_externz00_bglt) COBJECT(
						((BgL_externz00_bglt) BgL_oz00_148)))->BgL_exprza2za2);
		}

	}



/* &new/Cinfo-expr* */
	obj_t BGl_z62newzf2Cinfozd2exprza2ze0zzcfa_info3z00(obj_t BgL_envz00_5325,
		obj_t BgL_oz00_5326)
	{
		{	/* Cfa/cinfo3.sch 400 */
			return
				BGl_newzf2Cinfozd2exprza2z82zzcfa_info3z00(
				((BgL_newz00_bglt) BgL_oz00_5326));
		}

	}



/* new/Cinfo-expr*-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_newzf2Cinfozd2exprza2zd2setz12z42zzcfa_info3z00(BgL_newz00_bglt
		BgL_oz00_149, obj_t BgL_vz00_150)
	{
		{	/* Cfa/cinfo3.sch 401 */
			return
				((((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_oz00_149)))->BgL_exprza2za2) =
				((obj_t) BgL_vz00_150), BUNSPEC);
		}

	}



/* &new/Cinfo-expr*-set! */
	obj_t BGl_z62newzf2Cinfozd2exprza2zd2setz12z20zzcfa_info3z00(obj_t
		BgL_envz00_5327, obj_t BgL_oz00_5328, obj_t BgL_vz00_5329)
	{
		{	/* Cfa/cinfo3.sch 401 */
			return
				BGl_newzf2Cinfozd2exprza2zd2setz12z42zzcfa_info3z00(
				((BgL_newz00_bglt) BgL_oz00_5328), BgL_vz00_5329);
		}

	}



/* new/Cinfo-key */
	BGL_EXPORTED_DEF obj_t BGl_newzf2Cinfozd2keyz20zzcfa_info3z00(BgL_newz00_bglt
		BgL_oz00_151)
	{
		{	/* Cfa/cinfo3.sch 402 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_151)))->BgL_keyz00);
		}

	}



/* &new/Cinfo-key */
	obj_t BGl_z62newzf2Cinfozd2keyz42zzcfa_info3z00(obj_t BgL_envz00_5330,
		obj_t BgL_oz00_5331)
	{
		{	/* Cfa/cinfo3.sch 402 */
			return
				BGl_newzf2Cinfozd2keyz20zzcfa_info3z00(
				((BgL_newz00_bglt) BgL_oz00_5331));
		}

	}



/* new/Cinfo-key-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_newzf2Cinfozd2keyzd2setz12ze0zzcfa_info3z00(BgL_newz00_bglt
		BgL_oz00_152, obj_t BgL_vz00_153)
	{
		{	/* Cfa/cinfo3.sch 403 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_152)))->BgL_keyz00) =
				((obj_t) BgL_vz00_153), BUNSPEC);
		}

	}



/* &new/Cinfo-key-set! */
	obj_t BGl_z62newzf2Cinfozd2keyzd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5332, obj_t BgL_oz00_5333, obj_t BgL_vz00_5334)
	{
		{	/* Cfa/cinfo3.sch 403 */
			return
				BGl_newzf2Cinfozd2keyzd2setz12ze0zzcfa_info3z00(
				((BgL_newz00_bglt) BgL_oz00_5333), BgL_vz00_5334);
		}

	}



/* new/Cinfo-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_newzf2Cinfozd2sidezd2effectzf2zzcfa_info3z00(BgL_newz00_bglt
		BgL_oz00_154)
	{
		{	/* Cfa/cinfo3.sch 404 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_154)))->BgL_sidezd2effectzd2);
		}

	}



/* &new/Cinfo-side-effect */
	obj_t BGl_z62newzf2Cinfozd2sidezd2effectz90zzcfa_info3z00(obj_t
		BgL_envz00_5335, obj_t BgL_oz00_5336)
	{
		{	/* Cfa/cinfo3.sch 404 */
			return
				BGl_newzf2Cinfozd2sidezd2effectzf2zzcfa_info3z00(
				((BgL_newz00_bglt) BgL_oz00_5336));
		}

	}



/* new/Cinfo-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_newzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_info3z00(BgL_newz00_bglt
		BgL_oz00_155, obj_t BgL_vz00_156)
	{
		{	/* Cfa/cinfo3.sch 405 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_155)))->
					BgL_sidezd2effectzd2) = ((obj_t) BgL_vz00_156), BUNSPEC);
		}

	}



/* &new/Cinfo-side-effect-set! */
	obj_t BGl_z62newzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_info3z00(obj_t
		BgL_envz00_5337, obj_t BgL_oz00_5338, obj_t BgL_vz00_5339)
	{
		{	/* Cfa/cinfo3.sch 405 */
			return
				BGl_newzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_info3z00(
				((BgL_newz00_bglt) BgL_oz00_5338), BgL_vz00_5339);
		}

	}



/* new/Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_newzf2Cinfozd2typez20zzcfa_info3z00(BgL_newz00_bglt BgL_oz00_157)
	{
		{	/* Cfa/cinfo3.sch 406 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_157)))->BgL_typez00);
		}

	}



/* &new/Cinfo-type */
	BgL_typez00_bglt BGl_z62newzf2Cinfozd2typez42zzcfa_info3z00(obj_t
		BgL_envz00_5340, obj_t BgL_oz00_5341)
	{
		{	/* Cfa/cinfo3.sch 406 */
			return
				BGl_newzf2Cinfozd2typez20zzcfa_info3z00(
				((BgL_newz00_bglt) BgL_oz00_5341));
		}

	}



/* new/Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_newzf2Cinfozd2typezd2setz12ze0zzcfa_info3z00(BgL_newz00_bglt
		BgL_oz00_158, BgL_typez00_bglt BgL_vz00_159)
	{
		{	/* Cfa/cinfo3.sch 407 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_158)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_159), BUNSPEC);
		}

	}



/* &new/Cinfo-type-set! */
	obj_t BGl_z62newzf2Cinfozd2typezd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5342, obj_t BgL_oz00_5343, obj_t BgL_vz00_5344)
	{
		{	/* Cfa/cinfo3.sch 407 */
			return
				BGl_newzf2Cinfozd2typezd2setz12ze0zzcfa_info3z00(
				((BgL_newz00_bglt) BgL_oz00_5343), ((BgL_typez00_bglt) BgL_vz00_5344));
		}

	}



/* new/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t BGl_newzf2Cinfozd2locz20zzcfa_info3z00(BgL_newz00_bglt
		BgL_oz00_160)
	{
		{	/* Cfa/cinfo3.sch 408 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_160)))->BgL_locz00);
		}

	}



/* &new/Cinfo-loc */
	obj_t BGl_z62newzf2Cinfozd2locz42zzcfa_info3z00(obj_t BgL_envz00_5345,
		obj_t BgL_oz00_5346)
	{
		{	/* Cfa/cinfo3.sch 408 */
			return
				BGl_newzf2Cinfozd2locz20zzcfa_info3z00(
				((BgL_newz00_bglt) BgL_oz00_5346));
		}

	}



/* make-instanceof/Cinfo */
	BGL_EXPORTED_DEF BgL_instanceofz00_bglt
		BGl_makezd2instanceofzf2Cinfoz20zzcfa_info3z00(obj_t BgL_loc1427z00_163,
		BgL_typez00_bglt BgL_type1428z00_164, obj_t BgL_sidezd2effect1429zd2_165,
		obj_t BgL_key1430z00_166, obj_t BgL_exprza21431za2_167,
		obj_t BgL_effect1432z00_168, obj_t BgL_czd2format1433zd2_169,
		BgL_typez00_bglt BgL_class1434z00_170,
		BgL_approxz00_bglt BgL_approx1435z00_171)
	{
		{	/* Cfa/cinfo3.sch 412 */
			{	/* Cfa/cinfo3.sch 412 */
				BgL_instanceofz00_bglt BgL_new1357z00_6356;

				{	/* Cfa/cinfo3.sch 412 */
					BgL_instanceofz00_bglt BgL_tmp1355z00_6357;
					BgL_instanceofzf2cinfozf2_bglt BgL_wide1356z00_6358;

					{
						BgL_instanceofz00_bglt BgL_auxz00_7613;

						{	/* Cfa/cinfo3.sch 412 */
							BgL_instanceofz00_bglt BgL_new1354z00_6359;

							BgL_new1354z00_6359 =
								((BgL_instanceofz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_instanceofz00_bgl))));
							{	/* Cfa/cinfo3.sch 412 */
								long BgL_arg1720z00_6360;

								BgL_arg1720z00_6360 =
									BGL_CLASS_NUM(BGl_instanceofz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1354z00_6359),
									BgL_arg1720z00_6360);
							}
							{	/* Cfa/cinfo3.sch 412 */
								BgL_objectz00_bglt BgL_tmpz00_7618;

								BgL_tmpz00_7618 = ((BgL_objectz00_bglt) BgL_new1354z00_6359);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7618, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1354z00_6359);
							BgL_auxz00_7613 = BgL_new1354z00_6359;
						}
						BgL_tmp1355z00_6357 = ((BgL_instanceofz00_bglt) BgL_auxz00_7613);
					}
					BgL_wide1356z00_6358 =
						((BgL_instanceofzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_instanceofzf2cinfozf2_bgl))));
					{	/* Cfa/cinfo3.sch 412 */
						obj_t BgL_auxz00_7626;
						BgL_objectz00_bglt BgL_tmpz00_7624;

						BgL_auxz00_7626 = ((obj_t) BgL_wide1356z00_6358);
						BgL_tmpz00_7624 = ((BgL_objectz00_bglt) BgL_tmp1355z00_6357);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7624, BgL_auxz00_7626);
					}
					((BgL_objectz00_bglt) BgL_tmp1355z00_6357);
					{	/* Cfa/cinfo3.sch 412 */
						long BgL_arg1718z00_6361;

						BgL_arg1718z00_6361 =
							BGL_CLASS_NUM(BGl_instanceofzf2Cinfozf2zzcfa_info3z00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1355z00_6357), BgL_arg1718z00_6361);
					}
					BgL_new1357z00_6356 = ((BgL_instanceofz00_bglt) BgL_tmp1355z00_6357);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1357z00_6356)))->BgL_locz00) =
					((obj_t) BgL_loc1427z00_163), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1357z00_6356)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1428z00_164), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1357z00_6356)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1429zd2_165), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1357z00_6356)))->BgL_keyz00) =
					((obj_t) BgL_key1430z00_166), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1357z00_6356)))->BgL_exprza2za2) =
					((obj_t) BgL_exprza21431za2_167), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1357z00_6356)))->BgL_effectz00) =
					((obj_t) BgL_effect1432z00_168), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1357z00_6356)))->BgL_czd2formatzd2) =
					((obj_t) BgL_czd2format1433zd2_169), BUNSPEC);
				((((BgL_instanceofz00_bglt) COBJECT(((BgL_instanceofz00_bglt)
									BgL_new1357z00_6356)))->BgL_classz00) =
					((BgL_typez00_bglt) BgL_class1434z00_170), BUNSPEC);
				{
					BgL_instanceofzf2cinfozf2_bglt BgL_auxz00_7650;

					{
						obj_t BgL_auxz00_7651;

						{	/* Cfa/cinfo3.sch 412 */
							BgL_objectz00_bglt BgL_tmpz00_7652;

							BgL_tmpz00_7652 = ((BgL_objectz00_bglt) BgL_new1357z00_6356);
							BgL_auxz00_7651 = BGL_OBJECT_WIDENING(BgL_tmpz00_7652);
						}
						BgL_auxz00_7650 =
							((BgL_instanceofzf2cinfozf2_bglt) BgL_auxz00_7651);
					}
					((((BgL_instanceofzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7650))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1435z00_171), BUNSPEC);
				}
				return BgL_new1357z00_6356;
			}
		}

	}



/* &make-instanceof/Cinfo */
	BgL_instanceofz00_bglt BGl_z62makezd2instanceofzf2Cinfoz42zzcfa_info3z00(obj_t
		BgL_envz00_5347, obj_t BgL_loc1427z00_5348, obj_t BgL_type1428z00_5349,
		obj_t BgL_sidezd2effect1429zd2_5350, obj_t BgL_key1430z00_5351,
		obj_t BgL_exprza21431za2_5352, obj_t BgL_effect1432z00_5353,
		obj_t BgL_czd2format1433zd2_5354, obj_t BgL_class1434z00_5355,
		obj_t BgL_approx1435z00_5356)
	{
		{	/* Cfa/cinfo3.sch 412 */
			return
				BGl_makezd2instanceofzf2Cinfoz20zzcfa_info3z00(BgL_loc1427z00_5348,
				((BgL_typez00_bglt) BgL_type1428z00_5349),
				BgL_sidezd2effect1429zd2_5350, BgL_key1430z00_5351,
				BgL_exprza21431za2_5352, BgL_effect1432z00_5353,
				BgL_czd2format1433zd2_5354, ((BgL_typez00_bglt) BgL_class1434z00_5355),
				((BgL_approxz00_bglt) BgL_approx1435z00_5356));
		}

	}



/* instanceof/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_instanceofzf2Cinfozf3z01zzcfa_info3z00(obj_t
		BgL_objz00_172)
	{
		{	/* Cfa/cinfo3.sch 413 */
			{	/* Cfa/cinfo3.sch 413 */
				obj_t BgL_classz00_6362;

				BgL_classz00_6362 = BGl_instanceofzf2Cinfozf2zzcfa_info3z00;
				if (BGL_OBJECTP(BgL_objz00_172))
					{	/* Cfa/cinfo3.sch 413 */
						BgL_objectz00_bglt BgL_arg1809z00_6363;
						long BgL_arg1810z00_6364;

						BgL_arg1809z00_6363 = (BgL_objectz00_bglt) (BgL_objz00_172);
						BgL_arg1810z00_6364 = BGL_CLASS_DEPTH(BgL_classz00_6362);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo3.sch 413 */
								long BgL_idxz00_6365;

								BgL_idxz00_6365 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1809z00_6363);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6365 + BgL_arg1810z00_6364)) ==
									BgL_classz00_6362);
							}
						else
							{	/* Cfa/cinfo3.sch 413 */
								bool_t BgL_res2259z00_6368;

								{	/* Cfa/cinfo3.sch 413 */
									obj_t BgL_oclassz00_6369;

									{	/* Cfa/cinfo3.sch 413 */
										obj_t BgL_arg1815z00_6370;
										long BgL_arg1816z00_6371;

										BgL_arg1815z00_6370 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo3.sch 413 */
											long BgL_arg1817z00_6372;

											BgL_arg1817z00_6372 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1809z00_6363);
											BgL_arg1816z00_6371 = (BgL_arg1817z00_6372 - OBJECT_TYPE);
										}
										BgL_oclassz00_6369 =
											VECTOR_REF(BgL_arg1815z00_6370, BgL_arg1816z00_6371);
									}
									{	/* Cfa/cinfo3.sch 413 */
										bool_t BgL__ortest_1115z00_6373;

										BgL__ortest_1115z00_6373 =
											(BgL_classz00_6362 == BgL_oclassz00_6369);
										if (BgL__ortest_1115z00_6373)
											{	/* Cfa/cinfo3.sch 413 */
												BgL_res2259z00_6368 = BgL__ortest_1115z00_6373;
											}
										else
											{	/* Cfa/cinfo3.sch 413 */
												long BgL_odepthz00_6374;

												{	/* Cfa/cinfo3.sch 413 */
													obj_t BgL_arg1804z00_6375;

													BgL_arg1804z00_6375 = (BgL_oclassz00_6369);
													BgL_odepthz00_6374 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6375);
												}
												if ((BgL_arg1810z00_6364 < BgL_odepthz00_6374))
													{	/* Cfa/cinfo3.sch 413 */
														obj_t BgL_arg1802z00_6376;

														{	/* Cfa/cinfo3.sch 413 */
															obj_t BgL_arg1803z00_6377;

															BgL_arg1803z00_6377 = (BgL_oclassz00_6369);
															BgL_arg1802z00_6376 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6377,
																BgL_arg1810z00_6364);
														}
														BgL_res2259z00_6368 =
															(BgL_arg1802z00_6376 == BgL_classz00_6362);
													}
												else
													{	/* Cfa/cinfo3.sch 413 */
														BgL_res2259z00_6368 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2259z00_6368;
							}
					}
				else
					{	/* Cfa/cinfo3.sch 413 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &instanceof/Cinfo? */
	obj_t BGl_z62instanceofzf2Cinfozf3z63zzcfa_info3z00(obj_t BgL_envz00_5357,
		obj_t BgL_objz00_5358)
	{
		{	/* Cfa/cinfo3.sch 413 */
			return BBOOL(BGl_instanceofzf2Cinfozf3z01zzcfa_info3z00(BgL_objz00_5358));
		}

	}



/* instanceof/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_instanceofz00_bglt
		BGl_instanceofzf2Cinfozd2nilz20zzcfa_info3z00(void)
	{
		{	/* Cfa/cinfo3.sch 414 */
			{	/* Cfa/cinfo3.sch 414 */
				obj_t BgL_classz00_4211;

				BgL_classz00_4211 = BGl_instanceofzf2Cinfozf2zzcfa_info3z00;
				{	/* Cfa/cinfo3.sch 414 */
					obj_t BgL__ortest_1117z00_4212;

					BgL__ortest_1117z00_4212 = BGL_CLASS_NIL(BgL_classz00_4211);
					if (CBOOL(BgL__ortest_1117z00_4212))
						{	/* Cfa/cinfo3.sch 414 */
							return ((BgL_instanceofz00_bglt) BgL__ortest_1117z00_4212);
						}
					else
						{	/* Cfa/cinfo3.sch 414 */
							return
								((BgL_instanceofz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4211));
						}
				}
			}
		}

	}



/* &instanceof/Cinfo-nil */
	BgL_instanceofz00_bglt BGl_z62instanceofzf2Cinfozd2nilz42zzcfa_info3z00(obj_t
		BgL_envz00_5359)
	{
		{	/* Cfa/cinfo3.sch 414 */
			return BGl_instanceofzf2Cinfozd2nilz20zzcfa_info3z00();
		}

	}



/* instanceof/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_instanceofzf2Cinfozd2approxz20zzcfa_info3z00(BgL_instanceofz00_bglt
		BgL_oz00_173)
	{
		{	/* Cfa/cinfo3.sch 415 */
			{
				BgL_instanceofzf2cinfozf2_bglt BgL_auxz00_7693;

				{
					obj_t BgL_auxz00_7694;

					{	/* Cfa/cinfo3.sch 415 */
						BgL_objectz00_bglt BgL_tmpz00_7695;

						BgL_tmpz00_7695 = ((BgL_objectz00_bglt) BgL_oz00_173);
						BgL_auxz00_7694 = BGL_OBJECT_WIDENING(BgL_tmpz00_7695);
					}
					BgL_auxz00_7693 = ((BgL_instanceofzf2cinfozf2_bglt) BgL_auxz00_7694);
				}
				return
					(((BgL_instanceofzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7693))->
					BgL_approxz00);
			}
		}

	}



/* &instanceof/Cinfo-approx */
	BgL_approxz00_bglt BGl_z62instanceofzf2Cinfozd2approxz42zzcfa_info3z00(obj_t
		BgL_envz00_5360, obj_t BgL_oz00_5361)
	{
		{	/* Cfa/cinfo3.sch 415 */
			return
				BGl_instanceofzf2Cinfozd2approxz20zzcfa_info3z00(
				((BgL_instanceofz00_bglt) BgL_oz00_5361));
		}

	}



/* instanceof/Cinfo-class */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_instanceofzf2Cinfozd2classz20zzcfa_info3z00(BgL_instanceofz00_bglt
		BgL_oz00_176)
	{
		{	/* Cfa/cinfo3.sch 417 */
			return
				(((BgL_instanceofz00_bglt) COBJECT(
						((BgL_instanceofz00_bglt) BgL_oz00_176)))->BgL_classz00);
		}

	}



/* &instanceof/Cinfo-class */
	BgL_typez00_bglt BGl_z62instanceofzf2Cinfozd2classz42zzcfa_info3z00(obj_t
		BgL_envz00_5362, obj_t BgL_oz00_5363)
	{
		{	/* Cfa/cinfo3.sch 417 */
			return
				BGl_instanceofzf2Cinfozd2classz20zzcfa_info3z00(
				((BgL_instanceofz00_bglt) BgL_oz00_5363));
		}

	}



/* instanceof/Cinfo-class-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_instanceofzf2Cinfozd2classzd2setz12ze0zzcfa_info3z00
		(BgL_instanceofz00_bglt BgL_oz00_177, BgL_typez00_bglt BgL_vz00_178)
	{
		{	/* Cfa/cinfo3.sch 418 */
			return
				((((BgL_instanceofz00_bglt) COBJECT(
							((BgL_instanceofz00_bglt) BgL_oz00_177)))->BgL_classz00) =
				((BgL_typez00_bglt) BgL_vz00_178), BUNSPEC);
		}

	}



/* &instanceof/Cinfo-class-set! */
	obj_t BGl_z62instanceofzf2Cinfozd2classzd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5364, obj_t BgL_oz00_5365, obj_t BgL_vz00_5366)
	{
		{	/* Cfa/cinfo3.sch 418 */
			return
				BGl_instanceofzf2Cinfozd2classzd2setz12ze0zzcfa_info3z00(
				((BgL_instanceofz00_bglt) BgL_oz00_5365),
				((BgL_typez00_bglt) BgL_vz00_5366));
		}

	}



/* instanceof/Cinfo-c-format */
	BGL_EXPORTED_DEF obj_t
		BGl_instanceofzf2Cinfozd2czd2formatzf2zzcfa_info3z00(BgL_instanceofz00_bglt
		BgL_oz00_179)
	{
		{	/* Cfa/cinfo3.sch 419 */
			return
				(((BgL_privatez00_bglt) COBJECT(
						((BgL_privatez00_bglt) BgL_oz00_179)))->BgL_czd2formatzd2);
		}

	}



/* &instanceof/Cinfo-c-format */
	obj_t BGl_z62instanceofzf2Cinfozd2czd2formatz90zzcfa_info3z00(obj_t
		BgL_envz00_5367, obj_t BgL_oz00_5368)
	{
		{	/* Cfa/cinfo3.sch 419 */
			return
				BGl_instanceofzf2Cinfozd2czd2formatzf2zzcfa_info3z00(
				((BgL_instanceofz00_bglt) BgL_oz00_5368));
		}

	}



/* instanceof/Cinfo-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_instanceofzf2Cinfozd2effectz20zzcfa_info3z00(BgL_instanceofz00_bglt
		BgL_oz00_182)
	{
		{	/* Cfa/cinfo3.sch 421 */
			return
				(((BgL_externz00_bglt) COBJECT(
						((BgL_externz00_bglt) BgL_oz00_182)))->BgL_effectz00);
		}

	}



/* &instanceof/Cinfo-effect */
	obj_t BGl_z62instanceofzf2Cinfozd2effectz42zzcfa_info3z00(obj_t
		BgL_envz00_5369, obj_t BgL_oz00_5370)
	{
		{	/* Cfa/cinfo3.sch 421 */
			return
				BGl_instanceofzf2Cinfozd2effectz20zzcfa_info3z00(
				((BgL_instanceofz00_bglt) BgL_oz00_5370));
		}

	}



/* instanceof/Cinfo-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_instanceofzf2Cinfozd2effectzd2setz12ze0zzcfa_info3z00
		(BgL_instanceofz00_bglt BgL_oz00_183, obj_t BgL_vz00_184)
	{
		{	/* Cfa/cinfo3.sch 422 */
			return
				((((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_oz00_183)))->BgL_effectz00) =
				((obj_t) BgL_vz00_184), BUNSPEC);
		}

	}



/* &instanceof/Cinfo-effect-set! */
	obj_t BGl_z62instanceofzf2Cinfozd2effectzd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5371, obj_t BgL_oz00_5372, obj_t BgL_vz00_5373)
	{
		{	/* Cfa/cinfo3.sch 422 */
			return
				BGl_instanceofzf2Cinfozd2effectzd2setz12ze0zzcfa_info3z00(
				((BgL_instanceofz00_bglt) BgL_oz00_5372), BgL_vz00_5373);
		}

	}



/* instanceof/Cinfo-expr* */
	BGL_EXPORTED_DEF obj_t
		BGl_instanceofzf2Cinfozd2exprza2z82zzcfa_info3z00(BgL_instanceofz00_bglt
		BgL_oz00_185)
	{
		{	/* Cfa/cinfo3.sch 423 */
			return
				(((BgL_externz00_bglt) COBJECT(
						((BgL_externz00_bglt) BgL_oz00_185)))->BgL_exprza2za2);
		}

	}



/* &instanceof/Cinfo-expr* */
	obj_t BGl_z62instanceofzf2Cinfozd2exprza2ze0zzcfa_info3z00(obj_t
		BgL_envz00_5374, obj_t BgL_oz00_5375)
	{
		{	/* Cfa/cinfo3.sch 423 */
			return
				BGl_instanceofzf2Cinfozd2exprza2z82zzcfa_info3z00(
				((BgL_instanceofz00_bglt) BgL_oz00_5375));
		}

	}



/* instanceof/Cinfo-expr*-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_instanceofzf2Cinfozd2exprza2zd2setz12z42zzcfa_info3z00
		(BgL_instanceofz00_bglt BgL_oz00_186, obj_t BgL_vz00_187)
	{
		{	/* Cfa/cinfo3.sch 424 */
			return
				((((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_oz00_186)))->BgL_exprza2za2) =
				((obj_t) BgL_vz00_187), BUNSPEC);
		}

	}



/* &instanceof/Cinfo-expr*-set! */
	obj_t BGl_z62instanceofzf2Cinfozd2exprza2zd2setz12z20zzcfa_info3z00(obj_t
		BgL_envz00_5376, obj_t BgL_oz00_5377, obj_t BgL_vz00_5378)
	{
		{	/* Cfa/cinfo3.sch 424 */
			return
				BGl_instanceofzf2Cinfozd2exprza2zd2setz12z42zzcfa_info3z00(
				((BgL_instanceofz00_bglt) BgL_oz00_5377), BgL_vz00_5378);
		}

	}



/* instanceof/Cinfo-key */
	BGL_EXPORTED_DEF obj_t
		BGl_instanceofzf2Cinfozd2keyz20zzcfa_info3z00(BgL_instanceofz00_bglt
		BgL_oz00_188)
	{
		{	/* Cfa/cinfo3.sch 425 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_188)))->BgL_keyz00);
		}

	}



/* &instanceof/Cinfo-key */
	obj_t BGl_z62instanceofzf2Cinfozd2keyz42zzcfa_info3z00(obj_t BgL_envz00_5379,
		obj_t BgL_oz00_5380)
	{
		{	/* Cfa/cinfo3.sch 425 */
			return
				BGl_instanceofzf2Cinfozd2keyz20zzcfa_info3z00(
				((BgL_instanceofz00_bglt) BgL_oz00_5380));
		}

	}



/* instanceof/Cinfo-key-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_instanceofzf2Cinfozd2keyzd2setz12ze0zzcfa_info3z00
		(BgL_instanceofz00_bglt BgL_oz00_189, obj_t BgL_vz00_190)
	{
		{	/* Cfa/cinfo3.sch 426 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_189)))->BgL_keyz00) =
				((obj_t) BgL_vz00_190), BUNSPEC);
		}

	}



/* &instanceof/Cinfo-key-set! */
	obj_t BGl_z62instanceofzf2Cinfozd2keyzd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5381, obj_t BgL_oz00_5382, obj_t BgL_vz00_5383)
	{
		{	/* Cfa/cinfo3.sch 426 */
			return
				BGl_instanceofzf2Cinfozd2keyzd2setz12ze0zzcfa_info3z00(
				((BgL_instanceofz00_bglt) BgL_oz00_5382), BgL_vz00_5383);
		}

	}



/* instanceof/Cinfo-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_instanceofzf2Cinfozd2sidezd2effectzf2zzcfa_info3z00
		(BgL_instanceofz00_bglt BgL_oz00_191)
	{
		{	/* Cfa/cinfo3.sch 427 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_191)))->BgL_sidezd2effectzd2);
		}

	}



/* &instanceof/Cinfo-side-effect */
	obj_t BGl_z62instanceofzf2Cinfozd2sidezd2effectz90zzcfa_info3z00(obj_t
		BgL_envz00_5384, obj_t BgL_oz00_5385)
	{
		{	/* Cfa/cinfo3.sch 427 */
			return
				BGl_instanceofzf2Cinfozd2sidezd2effectzf2zzcfa_info3z00(
				((BgL_instanceofz00_bglt) BgL_oz00_5385));
		}

	}



/* instanceof/Cinfo-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_instanceofzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_info3z00
		(BgL_instanceofz00_bglt BgL_oz00_192, obj_t BgL_vz00_193)
	{
		{	/* Cfa/cinfo3.sch 428 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_192)))->
					BgL_sidezd2effectzd2) = ((obj_t) BgL_vz00_193), BUNSPEC);
		}

	}



/* &instanceof/Cinfo-side-effect-set! */
	obj_t
		BGl_z62instanceofzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_info3z00(obj_t
		BgL_envz00_5386, obj_t BgL_oz00_5387, obj_t BgL_vz00_5388)
	{
		{	/* Cfa/cinfo3.sch 428 */
			return
				BGl_instanceofzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_info3z00(
				((BgL_instanceofz00_bglt) BgL_oz00_5387), BgL_vz00_5388);
		}

	}



/* instanceof/Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_instanceofzf2Cinfozd2typez20zzcfa_info3z00(BgL_instanceofz00_bglt
		BgL_oz00_194)
	{
		{	/* Cfa/cinfo3.sch 429 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_194)))->BgL_typez00);
		}

	}



/* &instanceof/Cinfo-type */
	BgL_typez00_bglt BGl_z62instanceofzf2Cinfozd2typez42zzcfa_info3z00(obj_t
		BgL_envz00_5389, obj_t BgL_oz00_5390)
	{
		{	/* Cfa/cinfo3.sch 429 */
			return
				BGl_instanceofzf2Cinfozd2typez20zzcfa_info3z00(
				((BgL_instanceofz00_bglt) BgL_oz00_5390));
		}

	}



/* instanceof/Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_instanceofzf2Cinfozd2typezd2setz12ze0zzcfa_info3z00
		(BgL_instanceofz00_bglt BgL_oz00_195, BgL_typez00_bglt BgL_vz00_196)
	{
		{	/* Cfa/cinfo3.sch 430 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_195)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_196), BUNSPEC);
		}

	}



/* &instanceof/Cinfo-type-set! */
	obj_t BGl_z62instanceofzf2Cinfozd2typezd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5391, obj_t BgL_oz00_5392, obj_t BgL_vz00_5393)
	{
		{	/* Cfa/cinfo3.sch 430 */
			return
				BGl_instanceofzf2Cinfozd2typezd2setz12ze0zzcfa_info3z00(
				((BgL_instanceofz00_bglt) BgL_oz00_5392),
				((BgL_typez00_bglt) BgL_vz00_5393));
		}

	}



/* instanceof/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_instanceofzf2Cinfozd2locz20zzcfa_info3z00(BgL_instanceofz00_bglt
		BgL_oz00_197)
	{
		{	/* Cfa/cinfo3.sch 431 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_197)))->BgL_locz00);
		}

	}



/* &instanceof/Cinfo-loc */
	obj_t BGl_z62instanceofzf2Cinfozd2locz42zzcfa_info3z00(obj_t BgL_envz00_5394,
		obj_t BgL_oz00_5395)
	{
		{	/* Cfa/cinfo3.sch 431 */
			return
				BGl_instanceofzf2Cinfozd2locz20zzcfa_info3z00(
				((BgL_instanceofz00_bglt) BgL_oz00_5395));
		}

	}



/* make-cast-null/Cinfo */
	BGL_EXPORTED_DEF BgL_castzd2nullzd2_bglt
		BGl_makezd2castzd2nullzf2Cinfozf2zzcfa_info3z00(obj_t BgL_loc1418z00_200,
		BgL_typez00_bglt BgL_type1419z00_201, obj_t BgL_sidezd2effect1420zd2_202,
		obj_t BgL_key1421z00_203, obj_t BgL_exprza21422za2_204,
		obj_t BgL_effect1423z00_205, obj_t BgL_czd2format1424zd2_206,
		BgL_approxz00_bglt BgL_approx1425z00_207)
	{
		{	/* Cfa/cinfo3.sch 435 */
			{	/* Cfa/cinfo3.sch 435 */
				BgL_castzd2nullzd2_bglt BgL_new1361z00_6378;

				{	/* Cfa/cinfo3.sch 435 */
					BgL_castzd2nullzd2_bglt BgL_tmp1359z00_6379;
					BgL_castzd2nullzf2cinfoz20_bglt BgL_wide1360z00_6380;

					{
						BgL_castzd2nullzd2_bglt BgL_auxz00_7760;

						{	/* Cfa/cinfo3.sch 435 */
							BgL_castzd2nullzd2_bglt BgL_new1358z00_6381;

							BgL_new1358z00_6381 =
								((BgL_castzd2nullzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_castzd2nullzd2_bgl))));
							{	/* Cfa/cinfo3.sch 435 */
								long BgL_arg1724z00_6382;

								BgL_arg1724z00_6382 =
									BGL_CLASS_NUM(BGl_castzd2nullzd2zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1358z00_6381),
									BgL_arg1724z00_6382);
							}
							{	/* Cfa/cinfo3.sch 435 */
								BgL_objectz00_bglt BgL_tmpz00_7765;

								BgL_tmpz00_7765 = ((BgL_objectz00_bglt) BgL_new1358z00_6381);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7765, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1358z00_6381);
							BgL_auxz00_7760 = BgL_new1358z00_6381;
						}
						BgL_tmp1359z00_6379 = ((BgL_castzd2nullzd2_bglt) BgL_auxz00_7760);
					}
					BgL_wide1360z00_6380 =
						((BgL_castzd2nullzf2cinfoz20_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_castzd2nullzf2cinfoz20_bgl))));
					{	/* Cfa/cinfo3.sch 435 */
						obj_t BgL_auxz00_7773;
						BgL_objectz00_bglt BgL_tmpz00_7771;

						BgL_auxz00_7773 = ((obj_t) BgL_wide1360z00_6380);
						BgL_tmpz00_7771 = ((BgL_objectz00_bglt) BgL_tmp1359z00_6379);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7771, BgL_auxz00_7773);
					}
					((BgL_objectz00_bglt) BgL_tmp1359z00_6379);
					{	/* Cfa/cinfo3.sch 435 */
						long BgL_arg1722z00_6383;

						BgL_arg1722z00_6383 =
							BGL_CLASS_NUM(BGl_castzd2nullzf2Cinfoz20zzcfa_info3z00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1359z00_6379), BgL_arg1722z00_6383);
					}
					BgL_new1361z00_6378 = ((BgL_castzd2nullzd2_bglt) BgL_tmp1359z00_6379);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1361z00_6378)))->BgL_locz00) =
					((obj_t) BgL_loc1418z00_200), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1361z00_6378)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1419z00_201), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1361z00_6378)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1420zd2_202), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1361z00_6378)))->BgL_keyz00) =
					((obj_t) BgL_key1421z00_203), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1361z00_6378)))->BgL_exprza2za2) =
					((obj_t) BgL_exprza21422za2_204), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1361z00_6378)))->BgL_effectz00) =
					((obj_t) BgL_effect1423z00_205), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1361z00_6378)))->BgL_czd2formatzd2) =
					((obj_t) BgL_czd2format1424zd2_206), BUNSPEC);
				{
					BgL_castzd2nullzf2cinfoz20_bglt BgL_auxz00_7795;

					{
						obj_t BgL_auxz00_7796;

						{	/* Cfa/cinfo3.sch 435 */
							BgL_objectz00_bglt BgL_tmpz00_7797;

							BgL_tmpz00_7797 = ((BgL_objectz00_bglt) BgL_new1361z00_6378);
							BgL_auxz00_7796 = BGL_OBJECT_WIDENING(BgL_tmpz00_7797);
						}
						BgL_auxz00_7795 =
							((BgL_castzd2nullzf2cinfoz20_bglt) BgL_auxz00_7796);
					}
					((((BgL_castzd2nullzf2cinfoz20_bglt) COBJECT(BgL_auxz00_7795))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1425z00_207), BUNSPEC);
				}
				return BgL_new1361z00_6378;
			}
		}

	}



/* &make-cast-null/Cinfo */
	BgL_castzd2nullzd2_bglt
		BGl_z62makezd2castzd2nullzf2Cinfoz90zzcfa_info3z00(obj_t BgL_envz00_5396,
		obj_t BgL_loc1418z00_5397, obj_t BgL_type1419z00_5398,
		obj_t BgL_sidezd2effect1420zd2_5399, obj_t BgL_key1421z00_5400,
		obj_t BgL_exprza21422za2_5401, obj_t BgL_effect1423z00_5402,
		obj_t BgL_czd2format1424zd2_5403, obj_t BgL_approx1425z00_5404)
	{
		{	/* Cfa/cinfo3.sch 435 */
			return
				BGl_makezd2castzd2nullzf2Cinfozf2zzcfa_info3z00(BgL_loc1418z00_5397,
				((BgL_typez00_bglt) BgL_type1419z00_5398),
				BgL_sidezd2effect1420zd2_5399, BgL_key1421z00_5400,
				BgL_exprza21422za2_5401, BgL_effect1423z00_5402,
				BgL_czd2format1424zd2_5403,
				((BgL_approxz00_bglt) BgL_approx1425z00_5404));
		}

	}



/* cast-null/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_castzd2nullzf2Cinfozf3zd3zzcfa_info3z00(obj_t
		BgL_objz00_208)
	{
		{	/* Cfa/cinfo3.sch 436 */
			{	/* Cfa/cinfo3.sch 436 */
				obj_t BgL_classz00_6384;

				BgL_classz00_6384 = BGl_castzd2nullzf2Cinfoz20zzcfa_info3z00;
				if (BGL_OBJECTP(BgL_objz00_208))
					{	/* Cfa/cinfo3.sch 436 */
						BgL_objectz00_bglt BgL_arg1809z00_6385;
						long BgL_arg1810z00_6386;

						BgL_arg1809z00_6385 = (BgL_objectz00_bglt) (BgL_objz00_208);
						BgL_arg1810z00_6386 = BGL_CLASS_DEPTH(BgL_classz00_6384);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo3.sch 436 */
								long BgL_idxz00_6387;

								BgL_idxz00_6387 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1809z00_6385);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6387 + BgL_arg1810z00_6386)) ==
									BgL_classz00_6384);
							}
						else
							{	/* Cfa/cinfo3.sch 436 */
								bool_t BgL_res2260z00_6390;

								{	/* Cfa/cinfo3.sch 436 */
									obj_t BgL_oclassz00_6391;

									{	/* Cfa/cinfo3.sch 436 */
										obj_t BgL_arg1815z00_6392;
										long BgL_arg1816z00_6393;

										BgL_arg1815z00_6392 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo3.sch 436 */
											long BgL_arg1817z00_6394;

											BgL_arg1817z00_6394 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1809z00_6385);
											BgL_arg1816z00_6393 = (BgL_arg1817z00_6394 - OBJECT_TYPE);
										}
										BgL_oclassz00_6391 =
											VECTOR_REF(BgL_arg1815z00_6392, BgL_arg1816z00_6393);
									}
									{	/* Cfa/cinfo3.sch 436 */
										bool_t BgL__ortest_1115z00_6395;

										BgL__ortest_1115z00_6395 =
											(BgL_classz00_6384 == BgL_oclassz00_6391);
										if (BgL__ortest_1115z00_6395)
											{	/* Cfa/cinfo3.sch 436 */
												BgL_res2260z00_6390 = BgL__ortest_1115z00_6395;
											}
										else
											{	/* Cfa/cinfo3.sch 436 */
												long BgL_odepthz00_6396;

												{	/* Cfa/cinfo3.sch 436 */
													obj_t BgL_arg1804z00_6397;

													BgL_arg1804z00_6397 = (BgL_oclassz00_6391);
													BgL_odepthz00_6396 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6397);
												}
												if ((BgL_arg1810z00_6386 < BgL_odepthz00_6396))
													{	/* Cfa/cinfo3.sch 436 */
														obj_t BgL_arg1802z00_6398;

														{	/* Cfa/cinfo3.sch 436 */
															obj_t BgL_arg1803z00_6399;

															BgL_arg1803z00_6399 = (BgL_oclassz00_6391);
															BgL_arg1802z00_6398 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6399,
																BgL_arg1810z00_6386);
														}
														BgL_res2260z00_6390 =
															(BgL_arg1802z00_6398 == BgL_classz00_6384);
													}
												else
													{	/* Cfa/cinfo3.sch 436 */
														BgL_res2260z00_6390 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2260z00_6390;
							}
					}
				else
					{	/* Cfa/cinfo3.sch 436 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &cast-null/Cinfo? */
	obj_t BGl_z62castzd2nullzf2Cinfozf3zb1zzcfa_info3z00(obj_t BgL_envz00_5405,
		obj_t BgL_objz00_5406)
	{
		{	/* Cfa/cinfo3.sch 436 */
			return
				BBOOL(BGl_castzd2nullzf2Cinfozf3zd3zzcfa_info3z00(BgL_objz00_5406));
		}

	}



/* cast-null/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_castzd2nullzd2_bglt
		BGl_castzd2nullzf2Cinfozd2nilzf2zzcfa_info3z00(void)
	{
		{	/* Cfa/cinfo3.sch 437 */
			{	/* Cfa/cinfo3.sch 437 */
				obj_t BgL_classz00_4269;

				BgL_classz00_4269 = BGl_castzd2nullzf2Cinfoz20zzcfa_info3z00;
				{	/* Cfa/cinfo3.sch 437 */
					obj_t BgL__ortest_1117z00_4270;

					BgL__ortest_1117z00_4270 = BGL_CLASS_NIL(BgL_classz00_4269);
					if (CBOOL(BgL__ortest_1117z00_4270))
						{	/* Cfa/cinfo3.sch 437 */
							return ((BgL_castzd2nullzd2_bglt) BgL__ortest_1117z00_4270);
						}
					else
						{	/* Cfa/cinfo3.sch 437 */
							return
								((BgL_castzd2nullzd2_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4269));
						}
				}
			}
		}

	}



/* &cast-null/Cinfo-nil */
	BgL_castzd2nullzd2_bglt
		BGl_z62castzd2nullzf2Cinfozd2nilz90zzcfa_info3z00(obj_t BgL_envz00_5407)
	{
		{	/* Cfa/cinfo3.sch 437 */
			return BGl_castzd2nullzf2Cinfozd2nilzf2zzcfa_info3z00();
		}

	}



/* cast-null/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_castzd2nullzf2Cinfozd2approxzf2zzcfa_info3z00(BgL_castzd2nullzd2_bglt
		BgL_oz00_209)
	{
		{	/* Cfa/cinfo3.sch 438 */
			{
				BgL_castzd2nullzf2cinfoz20_bglt BgL_auxz00_7837;

				{
					obj_t BgL_auxz00_7838;

					{	/* Cfa/cinfo3.sch 438 */
						BgL_objectz00_bglt BgL_tmpz00_7839;

						BgL_tmpz00_7839 = ((BgL_objectz00_bglt) BgL_oz00_209);
						BgL_auxz00_7838 = BGL_OBJECT_WIDENING(BgL_tmpz00_7839);
					}
					BgL_auxz00_7837 = ((BgL_castzd2nullzf2cinfoz20_bglt) BgL_auxz00_7838);
				}
				return
					(((BgL_castzd2nullzf2cinfoz20_bglt) COBJECT(BgL_auxz00_7837))->
					BgL_approxz00);
			}
		}

	}



/* &cast-null/Cinfo-approx */
	BgL_approxz00_bglt BGl_z62castzd2nullzf2Cinfozd2approxz90zzcfa_info3z00(obj_t
		BgL_envz00_5408, obj_t BgL_oz00_5409)
	{
		{	/* Cfa/cinfo3.sch 438 */
			return
				BGl_castzd2nullzf2Cinfozd2approxzf2zzcfa_info3z00(
				((BgL_castzd2nullzd2_bglt) BgL_oz00_5409));
		}

	}



/* cast-null/Cinfo-c-format */
	BGL_EXPORTED_DEF obj_t
		BGl_castzd2nullzf2Cinfozd2czd2formatz20zzcfa_info3z00
		(BgL_castzd2nullzd2_bglt BgL_oz00_212)
	{
		{	/* Cfa/cinfo3.sch 440 */
			return
				(((BgL_privatez00_bglt) COBJECT(
						((BgL_privatez00_bglt) BgL_oz00_212)))->BgL_czd2formatzd2);
		}

	}



/* &cast-null/Cinfo-c-format */
	obj_t BGl_z62castzd2nullzf2Cinfozd2czd2formatz42zzcfa_info3z00(obj_t
		BgL_envz00_5410, obj_t BgL_oz00_5411)
	{
		{	/* Cfa/cinfo3.sch 440 */
			return
				BGl_castzd2nullzf2Cinfozd2czd2formatz20zzcfa_info3z00(
				((BgL_castzd2nullzd2_bglt) BgL_oz00_5411));
		}

	}



/* cast-null/Cinfo-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_castzd2nullzf2Cinfozd2effectzf2zzcfa_info3z00(BgL_castzd2nullzd2_bglt
		BgL_oz00_215)
	{
		{	/* Cfa/cinfo3.sch 442 */
			return
				(((BgL_externz00_bglt) COBJECT(
						((BgL_externz00_bglt) BgL_oz00_215)))->BgL_effectz00);
		}

	}



/* &cast-null/Cinfo-effect */
	obj_t BGl_z62castzd2nullzf2Cinfozd2effectz90zzcfa_info3z00(obj_t
		BgL_envz00_5412, obj_t BgL_oz00_5413)
	{
		{	/* Cfa/cinfo3.sch 442 */
			return
				BGl_castzd2nullzf2Cinfozd2effectzf2zzcfa_info3z00(
				((BgL_castzd2nullzd2_bglt) BgL_oz00_5413));
		}

	}



/* cast-null/Cinfo-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_castzd2nullzf2Cinfozd2effectzd2setz12z32zzcfa_info3z00
		(BgL_castzd2nullzd2_bglt BgL_oz00_216, obj_t BgL_vz00_217)
	{
		{	/* Cfa/cinfo3.sch 443 */
			return
				((((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_oz00_216)))->BgL_effectz00) =
				((obj_t) BgL_vz00_217), BUNSPEC);
		}

	}



/* &cast-null/Cinfo-effect-set! */
	obj_t BGl_z62castzd2nullzf2Cinfozd2effectzd2setz12z50zzcfa_info3z00(obj_t
		BgL_envz00_5414, obj_t BgL_oz00_5415, obj_t BgL_vz00_5416)
	{
		{	/* Cfa/cinfo3.sch 443 */
			return
				BGl_castzd2nullzf2Cinfozd2effectzd2setz12z32zzcfa_info3z00(
				((BgL_castzd2nullzd2_bglt) BgL_oz00_5415), BgL_vz00_5416);
		}

	}



/* cast-null/Cinfo-expr* */
	BGL_EXPORTED_DEF obj_t
		BGl_castzd2nullzf2Cinfozd2exprza2z50zzcfa_info3z00(BgL_castzd2nullzd2_bglt
		BgL_oz00_218)
	{
		{	/* Cfa/cinfo3.sch 444 */
			return
				(((BgL_externz00_bglt) COBJECT(
						((BgL_externz00_bglt) BgL_oz00_218)))->BgL_exprza2za2);
		}

	}



/* &cast-null/Cinfo-expr* */
	obj_t BGl_z62castzd2nullzf2Cinfozd2exprza2z32zzcfa_info3z00(obj_t
		BgL_envz00_5417, obj_t BgL_oz00_5418)
	{
		{	/* Cfa/cinfo3.sch 444 */
			return
				BGl_castzd2nullzf2Cinfozd2exprza2z50zzcfa_info3z00(
				((BgL_castzd2nullzd2_bglt) BgL_oz00_5418));
		}

	}



/* cast-null/Cinfo-expr*-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_castzd2nullzf2Cinfozd2exprza2zd2setz12z90zzcfa_info3z00
		(BgL_castzd2nullzd2_bglt BgL_oz00_219, obj_t BgL_vz00_220)
	{
		{	/* Cfa/cinfo3.sch 445 */
			return
				((((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_oz00_219)))->BgL_exprza2za2) =
				((obj_t) BgL_vz00_220), BUNSPEC);
		}

	}



/* &cast-null/Cinfo-expr*-set! */
	obj_t BGl_z62castzd2nullzf2Cinfozd2exprza2zd2setz12zf2zzcfa_info3z00(obj_t
		BgL_envz00_5419, obj_t BgL_oz00_5420, obj_t BgL_vz00_5421)
	{
		{	/* Cfa/cinfo3.sch 445 */
			return
				BGl_castzd2nullzf2Cinfozd2exprza2zd2setz12z90zzcfa_info3z00(
				((BgL_castzd2nullzd2_bglt) BgL_oz00_5420), BgL_vz00_5421);
		}

	}



/* cast-null/Cinfo-key */
	BGL_EXPORTED_DEF obj_t
		BGl_castzd2nullzf2Cinfozd2keyzf2zzcfa_info3z00(BgL_castzd2nullzd2_bglt
		BgL_oz00_221)
	{
		{	/* Cfa/cinfo3.sch 446 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_221)))->BgL_keyz00);
		}

	}



/* &cast-null/Cinfo-key */
	obj_t BGl_z62castzd2nullzf2Cinfozd2keyz90zzcfa_info3z00(obj_t BgL_envz00_5422,
		obj_t BgL_oz00_5423)
	{
		{	/* Cfa/cinfo3.sch 446 */
			return
				BGl_castzd2nullzf2Cinfozd2keyzf2zzcfa_info3z00(
				((BgL_castzd2nullzd2_bglt) BgL_oz00_5423));
		}

	}



/* cast-null/Cinfo-key-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_castzd2nullzf2Cinfozd2keyzd2setz12z32zzcfa_info3z00
		(BgL_castzd2nullzd2_bglt BgL_oz00_222, obj_t BgL_vz00_223)
	{
		{	/* Cfa/cinfo3.sch 447 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_222)))->BgL_keyz00) =
				((obj_t) BgL_vz00_223), BUNSPEC);
		}

	}



/* &cast-null/Cinfo-key-set! */
	obj_t BGl_z62castzd2nullzf2Cinfozd2keyzd2setz12z50zzcfa_info3z00(obj_t
		BgL_envz00_5424, obj_t BgL_oz00_5425, obj_t BgL_vz00_5426)
	{
		{	/* Cfa/cinfo3.sch 447 */
			return
				BGl_castzd2nullzf2Cinfozd2keyzd2setz12z32zzcfa_info3z00(
				((BgL_castzd2nullzd2_bglt) BgL_oz00_5425), BgL_vz00_5426);
		}

	}



/* cast-null/Cinfo-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_castzd2nullzf2Cinfozd2sidezd2effectz20zzcfa_info3z00
		(BgL_castzd2nullzd2_bglt BgL_oz00_224)
	{
		{	/* Cfa/cinfo3.sch 448 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_224)))->BgL_sidezd2effectzd2);
		}

	}



/* &cast-null/Cinfo-side-effect */
	obj_t BGl_z62castzd2nullzf2Cinfozd2sidezd2effectz42zzcfa_info3z00(obj_t
		BgL_envz00_5427, obj_t BgL_oz00_5428)
	{
		{	/* Cfa/cinfo3.sch 448 */
			return
				BGl_castzd2nullzf2Cinfozd2sidezd2effectz20zzcfa_info3z00(
				((BgL_castzd2nullzd2_bglt) BgL_oz00_5428));
		}

	}



/* cast-null/Cinfo-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_castzd2nullzf2Cinfozd2sidezd2effectzd2setz12ze0zzcfa_info3z00
		(BgL_castzd2nullzd2_bglt BgL_oz00_225, obj_t BgL_vz00_226)
	{
		{	/* Cfa/cinfo3.sch 449 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_225)))->
					BgL_sidezd2effectzd2) = ((obj_t) BgL_vz00_226), BUNSPEC);
		}

	}



/* &cast-null/Cinfo-side-effect-set! */
	obj_t
		BGl_z62castzd2nullzf2Cinfozd2sidezd2effectzd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5429, obj_t BgL_oz00_5430, obj_t BgL_vz00_5431)
	{
		{	/* Cfa/cinfo3.sch 449 */
			return
				BGl_castzd2nullzf2Cinfozd2sidezd2effectzd2setz12ze0zzcfa_info3z00(
				((BgL_castzd2nullzd2_bglt) BgL_oz00_5430), BgL_vz00_5431);
		}

	}



/* cast-null/Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_castzd2nullzf2Cinfozd2typezf2zzcfa_info3z00(BgL_castzd2nullzd2_bglt
		BgL_oz00_227)
	{
		{	/* Cfa/cinfo3.sch 450 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_227)))->BgL_typez00);
		}

	}



/* &cast-null/Cinfo-type */
	BgL_typez00_bglt BGl_z62castzd2nullzf2Cinfozd2typez90zzcfa_info3z00(obj_t
		BgL_envz00_5432, obj_t BgL_oz00_5433)
	{
		{	/* Cfa/cinfo3.sch 450 */
			return
				BGl_castzd2nullzf2Cinfozd2typezf2zzcfa_info3z00(
				((BgL_castzd2nullzd2_bglt) BgL_oz00_5433));
		}

	}



/* cast-null/Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_castzd2nullzf2Cinfozd2typezd2setz12z32zzcfa_info3z00
		(BgL_castzd2nullzd2_bglt BgL_oz00_228, BgL_typez00_bglt BgL_vz00_229)
	{
		{	/* Cfa/cinfo3.sch 451 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_228)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_229), BUNSPEC);
		}

	}



/* &cast-null/Cinfo-type-set! */
	obj_t BGl_z62castzd2nullzf2Cinfozd2typezd2setz12z50zzcfa_info3z00(obj_t
		BgL_envz00_5434, obj_t BgL_oz00_5435, obj_t BgL_vz00_5436)
	{
		{	/* Cfa/cinfo3.sch 451 */
			return
				BGl_castzd2nullzf2Cinfozd2typezd2setz12z32zzcfa_info3z00(
				((BgL_castzd2nullzd2_bglt) BgL_oz00_5435),
				((BgL_typez00_bglt) BgL_vz00_5436));
		}

	}



/* cast-null/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_castzd2nullzf2Cinfozd2loczf2zzcfa_info3z00(BgL_castzd2nullzd2_bglt
		BgL_oz00_230)
	{
		{	/* Cfa/cinfo3.sch 452 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_230)))->BgL_locz00);
		}

	}



/* &cast-null/Cinfo-loc */
	obj_t BGl_z62castzd2nullzf2Cinfozd2locz90zzcfa_info3z00(obj_t BgL_envz00_5437,
		obj_t BgL_oz00_5438)
	{
		{	/* Cfa/cinfo3.sch 452 */
			return
				BGl_castzd2nullzf2Cinfozd2loczf2zzcfa_info3z00(
				((BgL_castzd2nullzd2_bglt) BgL_oz00_5438));
		}

	}



/* make-vref/Cinfo */
	BGL_EXPORTED_DEF BgL_vrefz00_bglt
		BGl_makezd2vrefzf2Cinfoz20zzcfa_info3z00(obj_t BgL_loc1404z00_233,
		BgL_typez00_bglt BgL_type1405z00_234, obj_t BgL_sidezd2effect1406zd2_235,
		obj_t BgL_key1407z00_236, obj_t BgL_exprza21408za2_237,
		obj_t BgL_effect1409z00_238, obj_t BgL_czd2format1410zd2_239,
		BgL_typez00_bglt BgL_ftype1411z00_240,
		BgL_typez00_bglt BgL_otype1412z00_241,
		BgL_typez00_bglt BgL_vtype1413z00_242, bool_t BgL_unsafe1414z00_243,
		BgL_approxz00_bglt BgL_approx1415z00_244, bool_t BgL_tvectorzf31416zf3_245)
	{
		{	/* Cfa/cinfo3.sch 456 */
			{	/* Cfa/cinfo3.sch 456 */
				BgL_vrefz00_bglt BgL_new1365z00_6400;

				{	/* Cfa/cinfo3.sch 456 */
					BgL_vrefz00_bglt BgL_tmp1363z00_6401;
					BgL_vrefzf2cinfozf2_bglt BgL_wide1364z00_6402;

					{
						BgL_vrefz00_bglt BgL_auxz00_7895;

						{	/* Cfa/cinfo3.sch 456 */
							BgL_vrefz00_bglt BgL_new1362z00_6403;

							BgL_new1362z00_6403 =
								((BgL_vrefz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_vrefz00_bgl))));
							{	/* Cfa/cinfo3.sch 456 */
								long BgL_arg1734z00_6404;

								BgL_arg1734z00_6404 = BGL_CLASS_NUM(BGl_vrefz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1362z00_6403),
									BgL_arg1734z00_6404);
							}
							{	/* Cfa/cinfo3.sch 456 */
								BgL_objectz00_bglt BgL_tmpz00_7900;

								BgL_tmpz00_7900 = ((BgL_objectz00_bglt) BgL_new1362z00_6403);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7900, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1362z00_6403);
							BgL_auxz00_7895 = BgL_new1362z00_6403;
						}
						BgL_tmp1363z00_6401 = ((BgL_vrefz00_bglt) BgL_auxz00_7895);
					}
					BgL_wide1364z00_6402 =
						((BgL_vrefzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_vrefzf2cinfozf2_bgl))));
					{	/* Cfa/cinfo3.sch 456 */
						obj_t BgL_auxz00_7908;
						BgL_objectz00_bglt BgL_tmpz00_7906;

						BgL_auxz00_7908 = ((obj_t) BgL_wide1364z00_6402);
						BgL_tmpz00_7906 = ((BgL_objectz00_bglt) BgL_tmp1363z00_6401);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7906, BgL_auxz00_7908);
					}
					((BgL_objectz00_bglt) BgL_tmp1363z00_6401);
					{	/* Cfa/cinfo3.sch 456 */
						long BgL_arg1733z00_6405;

						BgL_arg1733z00_6405 =
							BGL_CLASS_NUM(BGl_vrefzf2Cinfozf2zzcfa_info3z00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1363z00_6401), BgL_arg1733z00_6405);
					}
					BgL_new1365z00_6400 = ((BgL_vrefz00_bglt) BgL_tmp1363z00_6401);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1365z00_6400)))->BgL_locz00) =
					((obj_t) BgL_loc1404z00_233), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1365z00_6400)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1405z00_234), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1365z00_6400)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1406zd2_235), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1365z00_6400)))->BgL_keyz00) =
					((obj_t) BgL_key1407z00_236), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1365z00_6400)))->BgL_exprza2za2) =
					((obj_t) BgL_exprza21408za2_237), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1365z00_6400)))->BgL_effectz00) =
					((obj_t) BgL_effect1409z00_238), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1365z00_6400)))->BgL_czd2formatzd2) =
					((obj_t) BgL_czd2format1410zd2_239), BUNSPEC);
				((((BgL_vrefz00_bglt) COBJECT(((BgL_vrefz00_bglt)
									BgL_new1365z00_6400)))->BgL_ftypez00) =
					((BgL_typez00_bglt) BgL_ftype1411z00_240), BUNSPEC);
				((((BgL_vrefz00_bglt) COBJECT(((BgL_vrefz00_bglt)
									BgL_new1365z00_6400)))->BgL_otypez00) =
					((BgL_typez00_bglt) BgL_otype1412z00_241), BUNSPEC);
				((((BgL_vrefz00_bglt) COBJECT(((BgL_vrefz00_bglt)
									BgL_new1365z00_6400)))->BgL_vtypez00) =
					((BgL_typez00_bglt) BgL_vtype1413z00_242), BUNSPEC);
				((((BgL_vrefz00_bglt) COBJECT(((BgL_vrefz00_bglt)
									BgL_new1365z00_6400)))->BgL_unsafez00) =
					((bool_t) BgL_unsafe1414z00_243), BUNSPEC);
				{
					BgL_vrefzf2cinfozf2_bglt BgL_auxz00_7938;

					{
						obj_t BgL_auxz00_7939;

						{	/* Cfa/cinfo3.sch 456 */
							BgL_objectz00_bglt BgL_tmpz00_7940;

							BgL_tmpz00_7940 = ((BgL_objectz00_bglt) BgL_new1365z00_6400);
							BgL_auxz00_7939 = BGL_OBJECT_WIDENING(BgL_tmpz00_7940);
						}
						BgL_auxz00_7938 = ((BgL_vrefzf2cinfozf2_bglt) BgL_auxz00_7939);
					}
					((((BgL_vrefzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7938))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1415z00_244), BUNSPEC);
				}
				{
					BgL_vrefzf2cinfozf2_bglt BgL_auxz00_7945;

					{
						obj_t BgL_auxz00_7946;

						{	/* Cfa/cinfo3.sch 456 */
							BgL_objectz00_bglt BgL_tmpz00_7947;

							BgL_tmpz00_7947 = ((BgL_objectz00_bglt) BgL_new1365z00_6400);
							BgL_auxz00_7946 = BGL_OBJECT_WIDENING(BgL_tmpz00_7947);
						}
						BgL_auxz00_7945 = ((BgL_vrefzf2cinfozf2_bglt) BgL_auxz00_7946);
					}
					((((BgL_vrefzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7945))->
							BgL_tvectorzf3zf3) =
						((bool_t) BgL_tvectorzf31416zf3_245), BUNSPEC);
				}
				return BgL_new1365z00_6400;
			}
		}

	}



/* &make-vref/Cinfo */
	BgL_vrefz00_bglt BGl_z62makezd2vrefzf2Cinfoz42zzcfa_info3z00(obj_t
		BgL_envz00_5439, obj_t BgL_loc1404z00_5440, obj_t BgL_type1405z00_5441,
		obj_t BgL_sidezd2effect1406zd2_5442, obj_t BgL_key1407z00_5443,
		obj_t BgL_exprza21408za2_5444, obj_t BgL_effect1409z00_5445,
		obj_t BgL_czd2format1410zd2_5446, obj_t BgL_ftype1411z00_5447,
		obj_t BgL_otype1412z00_5448, obj_t BgL_vtype1413z00_5449,
		obj_t BgL_unsafe1414z00_5450, obj_t BgL_approx1415z00_5451,
		obj_t BgL_tvectorzf31416zf3_5452)
	{
		{	/* Cfa/cinfo3.sch 456 */
			return
				BGl_makezd2vrefzf2Cinfoz20zzcfa_info3z00(BgL_loc1404z00_5440,
				((BgL_typez00_bglt) BgL_type1405z00_5441),
				BgL_sidezd2effect1406zd2_5442, BgL_key1407z00_5443,
				BgL_exprza21408za2_5444, BgL_effect1409z00_5445,
				BgL_czd2format1410zd2_5446, ((BgL_typez00_bglt) BgL_ftype1411z00_5447),
				((BgL_typez00_bglt) BgL_otype1412z00_5448),
				((BgL_typez00_bglt) BgL_vtype1413z00_5449),
				CBOOL(BgL_unsafe1414z00_5450),
				((BgL_approxz00_bglt) BgL_approx1415z00_5451),
				CBOOL(BgL_tvectorzf31416zf3_5452));
		}

	}



/* vref/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_vrefzf2Cinfozf3z01zzcfa_info3z00(obj_t
		BgL_objz00_246)
	{
		{	/* Cfa/cinfo3.sch 457 */
			{	/* Cfa/cinfo3.sch 457 */
				obj_t BgL_classz00_6406;

				BgL_classz00_6406 = BGl_vrefzf2Cinfozf2zzcfa_info3z00;
				if (BGL_OBJECTP(BgL_objz00_246))
					{	/* Cfa/cinfo3.sch 457 */
						BgL_objectz00_bglt BgL_arg1809z00_6407;
						long BgL_arg1810z00_6408;

						BgL_arg1809z00_6407 = (BgL_objectz00_bglt) (BgL_objz00_246);
						BgL_arg1810z00_6408 = BGL_CLASS_DEPTH(BgL_classz00_6406);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo3.sch 457 */
								long BgL_idxz00_6409;

								BgL_idxz00_6409 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1809z00_6407);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6409 + BgL_arg1810z00_6408)) ==
									BgL_classz00_6406);
							}
						else
							{	/* Cfa/cinfo3.sch 457 */
								bool_t BgL_res2261z00_6412;

								{	/* Cfa/cinfo3.sch 457 */
									obj_t BgL_oclassz00_6413;

									{	/* Cfa/cinfo3.sch 457 */
										obj_t BgL_arg1815z00_6414;
										long BgL_arg1816z00_6415;

										BgL_arg1815z00_6414 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo3.sch 457 */
											long BgL_arg1817z00_6416;

											BgL_arg1817z00_6416 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1809z00_6407);
											BgL_arg1816z00_6415 = (BgL_arg1817z00_6416 - OBJECT_TYPE);
										}
										BgL_oclassz00_6413 =
											VECTOR_REF(BgL_arg1815z00_6414, BgL_arg1816z00_6415);
									}
									{	/* Cfa/cinfo3.sch 457 */
										bool_t BgL__ortest_1115z00_6417;

										BgL__ortest_1115z00_6417 =
											(BgL_classz00_6406 == BgL_oclassz00_6413);
										if (BgL__ortest_1115z00_6417)
											{	/* Cfa/cinfo3.sch 457 */
												BgL_res2261z00_6412 = BgL__ortest_1115z00_6417;
											}
										else
											{	/* Cfa/cinfo3.sch 457 */
												long BgL_odepthz00_6418;

												{	/* Cfa/cinfo3.sch 457 */
													obj_t BgL_arg1804z00_6419;

													BgL_arg1804z00_6419 = (BgL_oclassz00_6413);
													BgL_odepthz00_6418 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6419);
												}
												if ((BgL_arg1810z00_6408 < BgL_odepthz00_6418))
													{	/* Cfa/cinfo3.sch 457 */
														obj_t BgL_arg1802z00_6420;

														{	/* Cfa/cinfo3.sch 457 */
															obj_t BgL_arg1803z00_6421;

															BgL_arg1803z00_6421 = (BgL_oclassz00_6413);
															BgL_arg1802z00_6420 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6421,
																BgL_arg1810z00_6408);
														}
														BgL_res2261z00_6412 =
															(BgL_arg1802z00_6420 == BgL_classz00_6406);
													}
												else
													{	/* Cfa/cinfo3.sch 457 */
														BgL_res2261z00_6412 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2261z00_6412;
							}
					}
				else
					{	/* Cfa/cinfo3.sch 457 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &vref/Cinfo? */
	obj_t BGl_z62vrefzf2Cinfozf3z63zzcfa_info3z00(obj_t BgL_envz00_5453,
		obj_t BgL_objz00_5454)
	{
		{	/* Cfa/cinfo3.sch 457 */
			return BBOOL(BGl_vrefzf2Cinfozf3z01zzcfa_info3z00(BgL_objz00_5454));
		}

	}



/* vref/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_vrefz00_bglt
		BGl_vrefzf2Cinfozd2nilz20zzcfa_info3z00(void)
	{
		{	/* Cfa/cinfo3.sch 458 */
			{	/* Cfa/cinfo3.sch 458 */
				obj_t BgL_classz00_4328;

				BgL_classz00_4328 = BGl_vrefzf2Cinfozf2zzcfa_info3z00;
				{	/* Cfa/cinfo3.sch 458 */
					obj_t BgL__ortest_1117z00_4329;

					BgL__ortest_1117z00_4329 = BGL_CLASS_NIL(BgL_classz00_4328);
					if (CBOOL(BgL__ortest_1117z00_4329))
						{	/* Cfa/cinfo3.sch 458 */
							return ((BgL_vrefz00_bglt) BgL__ortest_1117z00_4329);
						}
					else
						{	/* Cfa/cinfo3.sch 458 */
							return
								((BgL_vrefz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4328));
						}
				}
			}
		}

	}



/* &vref/Cinfo-nil */
	BgL_vrefz00_bglt BGl_z62vrefzf2Cinfozd2nilz42zzcfa_info3z00(obj_t
		BgL_envz00_5455)
	{
		{	/* Cfa/cinfo3.sch 458 */
			return BGl_vrefzf2Cinfozd2nilz20zzcfa_info3z00();
		}

	}



/* vref/Cinfo-tvector? */
	BGL_EXPORTED_DEF bool_t
		BGl_vrefzf2Cinfozd2tvectorzf3zd3zzcfa_info3z00(BgL_vrefz00_bglt
		BgL_oz00_247)
	{
		{	/* Cfa/cinfo3.sch 459 */
			{
				BgL_vrefzf2cinfozf2_bglt BgL_auxz00_7992;

				{
					obj_t BgL_auxz00_7993;

					{	/* Cfa/cinfo3.sch 459 */
						BgL_objectz00_bglt BgL_tmpz00_7994;

						BgL_tmpz00_7994 = ((BgL_objectz00_bglt) BgL_oz00_247);
						BgL_auxz00_7993 = BGL_OBJECT_WIDENING(BgL_tmpz00_7994);
					}
					BgL_auxz00_7992 = ((BgL_vrefzf2cinfozf2_bglt) BgL_auxz00_7993);
				}
				return
					(((BgL_vrefzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7992))->
					BgL_tvectorzf3zf3);
			}
		}

	}



/* &vref/Cinfo-tvector? */
	obj_t BGl_z62vrefzf2Cinfozd2tvectorzf3zb1zzcfa_info3z00(obj_t BgL_envz00_5456,
		obj_t BgL_oz00_5457)
	{
		{	/* Cfa/cinfo3.sch 459 */
			return
				BBOOL(BGl_vrefzf2Cinfozd2tvectorzf3zd3zzcfa_info3z00(
					((BgL_vrefz00_bglt) BgL_oz00_5457)));
		}

	}



/* vref/Cinfo-tvector?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vrefzf2Cinfozd2tvectorzf3zd2setz12z13zzcfa_info3z00(BgL_vrefz00_bglt
		BgL_oz00_248, bool_t BgL_vz00_249)
	{
		{	/* Cfa/cinfo3.sch 460 */
			{
				BgL_vrefzf2cinfozf2_bglt BgL_auxz00_8002;

				{
					obj_t BgL_auxz00_8003;

					{	/* Cfa/cinfo3.sch 460 */
						BgL_objectz00_bglt BgL_tmpz00_8004;

						BgL_tmpz00_8004 = ((BgL_objectz00_bglt) BgL_oz00_248);
						BgL_auxz00_8003 = BGL_OBJECT_WIDENING(BgL_tmpz00_8004);
					}
					BgL_auxz00_8002 = ((BgL_vrefzf2cinfozf2_bglt) BgL_auxz00_8003);
				}
				return
					((((BgL_vrefzf2cinfozf2_bglt) COBJECT(BgL_auxz00_8002))->
						BgL_tvectorzf3zf3) = ((bool_t) BgL_vz00_249), BUNSPEC);
			}
		}

	}



/* &vref/Cinfo-tvector?-set! */
	obj_t BGl_z62vrefzf2Cinfozd2tvectorzf3zd2setz12z71zzcfa_info3z00(obj_t
		BgL_envz00_5458, obj_t BgL_oz00_5459, obj_t BgL_vz00_5460)
	{
		{	/* Cfa/cinfo3.sch 460 */
			return
				BGl_vrefzf2Cinfozd2tvectorzf3zd2setz12z13zzcfa_info3z00(
				((BgL_vrefz00_bglt) BgL_oz00_5459), CBOOL(BgL_vz00_5460));
		}

	}



/* vref/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_vrefzf2Cinfozd2approxz20zzcfa_info3z00(BgL_vrefz00_bglt BgL_oz00_250)
	{
		{	/* Cfa/cinfo3.sch 461 */
			{
				BgL_vrefzf2cinfozf2_bglt BgL_auxz00_8012;

				{
					obj_t BgL_auxz00_8013;

					{	/* Cfa/cinfo3.sch 461 */
						BgL_objectz00_bglt BgL_tmpz00_8014;

						BgL_tmpz00_8014 = ((BgL_objectz00_bglt) BgL_oz00_250);
						BgL_auxz00_8013 = BGL_OBJECT_WIDENING(BgL_tmpz00_8014);
					}
					BgL_auxz00_8012 = ((BgL_vrefzf2cinfozf2_bglt) BgL_auxz00_8013);
				}
				return
					(((BgL_vrefzf2cinfozf2_bglt) COBJECT(BgL_auxz00_8012))->
					BgL_approxz00);
			}
		}

	}



/* &vref/Cinfo-approx */
	BgL_approxz00_bglt BGl_z62vrefzf2Cinfozd2approxz42zzcfa_info3z00(obj_t
		BgL_envz00_5461, obj_t BgL_oz00_5462)
	{
		{	/* Cfa/cinfo3.sch 461 */
			return
				BGl_vrefzf2Cinfozd2approxz20zzcfa_info3z00(
				((BgL_vrefz00_bglt) BgL_oz00_5462));
		}

	}



/* vref/Cinfo-unsafe */
	BGL_EXPORTED_DEF bool_t
		BGl_vrefzf2Cinfozd2unsafez20zzcfa_info3z00(BgL_vrefz00_bglt BgL_oz00_253)
	{
		{	/* Cfa/cinfo3.sch 463 */
			return
				(((BgL_vrefz00_bglt) COBJECT(
						((BgL_vrefz00_bglt) BgL_oz00_253)))->BgL_unsafez00);
		}

	}



/* &vref/Cinfo-unsafe */
	obj_t BGl_z62vrefzf2Cinfozd2unsafez42zzcfa_info3z00(obj_t BgL_envz00_5463,
		obj_t BgL_oz00_5464)
	{
		{	/* Cfa/cinfo3.sch 463 */
			return
				BBOOL(BGl_vrefzf2Cinfozd2unsafez20zzcfa_info3z00(
					((BgL_vrefz00_bglt) BgL_oz00_5464)));
		}

	}



/* vref/Cinfo-vtype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_vrefzf2Cinfozd2vtypez20zzcfa_info3z00(BgL_vrefz00_bglt BgL_oz00_256)
	{
		{	/* Cfa/cinfo3.sch 465 */
			return
				(((BgL_vrefz00_bglt) COBJECT(
						((BgL_vrefz00_bglt) BgL_oz00_256)))->BgL_vtypez00);
		}

	}



/* &vref/Cinfo-vtype */
	BgL_typez00_bglt BGl_z62vrefzf2Cinfozd2vtypez42zzcfa_info3z00(obj_t
		BgL_envz00_5465, obj_t BgL_oz00_5466)
	{
		{	/* Cfa/cinfo3.sch 465 */
			return
				BGl_vrefzf2Cinfozd2vtypez20zzcfa_info3z00(
				((BgL_vrefz00_bglt) BgL_oz00_5466));
		}

	}



/* vref/Cinfo-vtype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vrefzf2Cinfozd2vtypezd2setz12ze0zzcfa_info3z00(BgL_vrefz00_bglt
		BgL_oz00_257, BgL_typez00_bglt BgL_vz00_258)
	{
		{	/* Cfa/cinfo3.sch 466 */
			return
				((((BgL_vrefz00_bglt) COBJECT(
							((BgL_vrefz00_bglt) BgL_oz00_257)))->BgL_vtypez00) =
				((BgL_typez00_bglt) BgL_vz00_258), BUNSPEC);
		}

	}



/* &vref/Cinfo-vtype-set! */
	obj_t BGl_z62vrefzf2Cinfozd2vtypezd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5467, obj_t BgL_oz00_5468, obj_t BgL_vz00_5469)
	{
		{	/* Cfa/cinfo3.sch 466 */
			return
				BGl_vrefzf2Cinfozd2vtypezd2setz12ze0zzcfa_info3z00(
				((BgL_vrefz00_bglt) BgL_oz00_5468), ((BgL_typez00_bglt) BgL_vz00_5469));
		}

	}



/* vref/Cinfo-otype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_vrefzf2Cinfozd2otypez20zzcfa_info3z00(BgL_vrefz00_bglt BgL_oz00_259)
	{
		{	/* Cfa/cinfo3.sch 467 */
			return
				(((BgL_vrefz00_bglt) COBJECT(
						((BgL_vrefz00_bglt) BgL_oz00_259)))->BgL_otypez00);
		}

	}



/* &vref/Cinfo-otype */
	BgL_typez00_bglt BGl_z62vrefzf2Cinfozd2otypez42zzcfa_info3z00(obj_t
		BgL_envz00_5470, obj_t BgL_oz00_5471)
	{
		{	/* Cfa/cinfo3.sch 467 */
			return
				BGl_vrefzf2Cinfozd2otypez20zzcfa_info3z00(
				((BgL_vrefz00_bglt) BgL_oz00_5471));
		}

	}



/* vref/Cinfo-otype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vrefzf2Cinfozd2otypezd2setz12ze0zzcfa_info3z00(BgL_vrefz00_bglt
		BgL_oz00_260, BgL_typez00_bglt BgL_vz00_261)
	{
		{	/* Cfa/cinfo3.sch 468 */
			return
				((((BgL_vrefz00_bglt) COBJECT(
							((BgL_vrefz00_bglt) BgL_oz00_260)))->BgL_otypez00) =
				((BgL_typez00_bglt) BgL_vz00_261), BUNSPEC);
		}

	}



/* &vref/Cinfo-otype-set! */
	obj_t BGl_z62vrefzf2Cinfozd2otypezd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5472, obj_t BgL_oz00_5473, obj_t BgL_vz00_5474)
	{
		{	/* Cfa/cinfo3.sch 468 */
			return
				BGl_vrefzf2Cinfozd2otypezd2setz12ze0zzcfa_info3z00(
				((BgL_vrefz00_bglt) BgL_oz00_5473), ((BgL_typez00_bglt) BgL_vz00_5474));
		}

	}



/* vref/Cinfo-ftype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_vrefzf2Cinfozd2ftypez20zzcfa_info3z00(BgL_vrefz00_bglt BgL_oz00_262)
	{
		{	/* Cfa/cinfo3.sch 469 */
			return
				(((BgL_vrefz00_bglt) COBJECT(
						((BgL_vrefz00_bglt) BgL_oz00_262)))->BgL_ftypez00);
		}

	}



/* &vref/Cinfo-ftype */
	BgL_typez00_bglt BGl_z62vrefzf2Cinfozd2ftypez42zzcfa_info3z00(obj_t
		BgL_envz00_5475, obj_t BgL_oz00_5476)
	{
		{	/* Cfa/cinfo3.sch 469 */
			return
				BGl_vrefzf2Cinfozd2ftypez20zzcfa_info3z00(
				((BgL_vrefz00_bglt) BgL_oz00_5476));
		}

	}



/* vref/Cinfo-ftype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vrefzf2Cinfozd2ftypezd2setz12ze0zzcfa_info3z00(BgL_vrefz00_bglt
		BgL_oz00_263, BgL_typez00_bglt BgL_vz00_264)
	{
		{	/* Cfa/cinfo3.sch 470 */
			return
				((((BgL_vrefz00_bglt) COBJECT(
							((BgL_vrefz00_bglt) BgL_oz00_263)))->BgL_ftypez00) =
				((BgL_typez00_bglt) BgL_vz00_264), BUNSPEC);
		}

	}



/* &vref/Cinfo-ftype-set! */
	obj_t BGl_z62vrefzf2Cinfozd2ftypezd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5477, obj_t BgL_oz00_5478, obj_t BgL_vz00_5479)
	{
		{	/* Cfa/cinfo3.sch 470 */
			return
				BGl_vrefzf2Cinfozd2ftypezd2setz12ze0zzcfa_info3z00(
				((BgL_vrefz00_bglt) BgL_oz00_5478), ((BgL_typez00_bglt) BgL_vz00_5479));
		}

	}



/* vref/Cinfo-c-format */
	BGL_EXPORTED_DEF obj_t
		BGl_vrefzf2Cinfozd2czd2formatzf2zzcfa_info3z00(BgL_vrefz00_bglt
		BgL_oz00_265)
	{
		{	/* Cfa/cinfo3.sch 471 */
			return
				(((BgL_privatez00_bglt) COBJECT(
						((BgL_privatez00_bglt) BgL_oz00_265)))->BgL_czd2formatzd2);
		}

	}



/* &vref/Cinfo-c-format */
	obj_t BGl_z62vrefzf2Cinfozd2czd2formatz90zzcfa_info3z00(obj_t BgL_envz00_5480,
		obj_t BgL_oz00_5481)
	{
		{	/* Cfa/cinfo3.sch 471 */
			return
				BGl_vrefzf2Cinfozd2czd2formatzf2zzcfa_info3z00(
				((BgL_vrefz00_bglt) BgL_oz00_5481));
		}

	}



/* vref/Cinfo-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_vrefzf2Cinfozd2effectz20zzcfa_info3z00(BgL_vrefz00_bglt BgL_oz00_268)
	{
		{	/* Cfa/cinfo3.sch 473 */
			return
				(((BgL_externz00_bglt) COBJECT(
						((BgL_externz00_bglt) BgL_oz00_268)))->BgL_effectz00);
		}

	}



/* &vref/Cinfo-effect */
	obj_t BGl_z62vrefzf2Cinfozd2effectz42zzcfa_info3z00(obj_t BgL_envz00_5482,
		obj_t BgL_oz00_5483)
	{
		{	/* Cfa/cinfo3.sch 473 */
			return
				BGl_vrefzf2Cinfozd2effectz20zzcfa_info3z00(
				((BgL_vrefz00_bglt) BgL_oz00_5483));
		}

	}



/* vref/Cinfo-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vrefzf2Cinfozd2effectzd2setz12ze0zzcfa_info3z00(BgL_vrefz00_bglt
		BgL_oz00_269, obj_t BgL_vz00_270)
	{
		{	/* Cfa/cinfo3.sch 474 */
			return
				((((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_oz00_269)))->BgL_effectz00) =
				((obj_t) BgL_vz00_270), BUNSPEC);
		}

	}



/* &vref/Cinfo-effect-set! */
	obj_t BGl_z62vrefzf2Cinfozd2effectzd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5484, obj_t BgL_oz00_5485, obj_t BgL_vz00_5486)
	{
		{	/* Cfa/cinfo3.sch 474 */
			return
				BGl_vrefzf2Cinfozd2effectzd2setz12ze0zzcfa_info3z00(
				((BgL_vrefz00_bglt) BgL_oz00_5485), BgL_vz00_5486);
		}

	}



/* vref/Cinfo-expr* */
	BGL_EXPORTED_DEF obj_t
		BGl_vrefzf2Cinfozd2exprza2z82zzcfa_info3z00(BgL_vrefz00_bglt BgL_oz00_271)
	{
		{	/* Cfa/cinfo3.sch 475 */
			return
				(((BgL_externz00_bglt) COBJECT(
						((BgL_externz00_bglt) BgL_oz00_271)))->BgL_exprza2za2);
		}

	}



/* &vref/Cinfo-expr* */
	obj_t BGl_z62vrefzf2Cinfozd2exprza2ze0zzcfa_info3z00(obj_t BgL_envz00_5487,
		obj_t BgL_oz00_5488)
	{
		{	/* Cfa/cinfo3.sch 475 */
			return
				BGl_vrefzf2Cinfozd2exprza2z82zzcfa_info3z00(
				((BgL_vrefz00_bglt) BgL_oz00_5488));
		}

	}



/* vref/Cinfo-expr*-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vrefzf2Cinfozd2exprza2zd2setz12z42zzcfa_info3z00(BgL_vrefz00_bglt
		BgL_oz00_272, obj_t BgL_vz00_273)
	{
		{	/* Cfa/cinfo3.sch 476 */
			return
				((((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_oz00_272)))->BgL_exprza2za2) =
				((obj_t) BgL_vz00_273), BUNSPEC);
		}

	}



/* &vref/Cinfo-expr*-set! */
	obj_t BGl_z62vrefzf2Cinfozd2exprza2zd2setz12z20zzcfa_info3z00(obj_t
		BgL_envz00_5489, obj_t BgL_oz00_5490, obj_t BgL_vz00_5491)
	{
		{	/* Cfa/cinfo3.sch 476 */
			return
				BGl_vrefzf2Cinfozd2exprza2zd2setz12z42zzcfa_info3z00(
				((BgL_vrefz00_bglt) BgL_oz00_5490), BgL_vz00_5491);
		}

	}



/* vref/Cinfo-key */
	BGL_EXPORTED_DEF obj_t
		BGl_vrefzf2Cinfozd2keyz20zzcfa_info3z00(BgL_vrefz00_bglt BgL_oz00_274)
	{
		{	/* Cfa/cinfo3.sch 477 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_274)))->BgL_keyz00);
		}

	}



/* &vref/Cinfo-key */
	obj_t BGl_z62vrefzf2Cinfozd2keyz42zzcfa_info3z00(obj_t BgL_envz00_5492,
		obj_t BgL_oz00_5493)
	{
		{	/* Cfa/cinfo3.sch 477 */
			return
				BGl_vrefzf2Cinfozd2keyz20zzcfa_info3z00(
				((BgL_vrefz00_bglt) BgL_oz00_5493));
		}

	}



/* vref/Cinfo-key-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vrefzf2Cinfozd2keyzd2setz12ze0zzcfa_info3z00(BgL_vrefz00_bglt
		BgL_oz00_275, obj_t BgL_vz00_276)
	{
		{	/* Cfa/cinfo3.sch 478 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_275)))->BgL_keyz00) =
				((obj_t) BgL_vz00_276), BUNSPEC);
		}

	}



/* &vref/Cinfo-key-set! */
	obj_t BGl_z62vrefzf2Cinfozd2keyzd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5494, obj_t BgL_oz00_5495, obj_t BgL_vz00_5496)
	{
		{	/* Cfa/cinfo3.sch 478 */
			return
				BGl_vrefzf2Cinfozd2keyzd2setz12ze0zzcfa_info3z00(
				((BgL_vrefz00_bglt) BgL_oz00_5495), BgL_vz00_5496);
		}

	}



/* vref/Cinfo-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_vrefzf2Cinfozd2sidezd2effectzf2zzcfa_info3z00(BgL_vrefz00_bglt
		BgL_oz00_277)
	{
		{	/* Cfa/cinfo3.sch 479 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_277)))->BgL_sidezd2effectzd2);
		}

	}



/* &vref/Cinfo-side-effect */
	obj_t BGl_z62vrefzf2Cinfozd2sidezd2effectz90zzcfa_info3z00(obj_t
		BgL_envz00_5497, obj_t BgL_oz00_5498)
	{
		{	/* Cfa/cinfo3.sch 479 */
			return
				BGl_vrefzf2Cinfozd2sidezd2effectzf2zzcfa_info3z00(
				((BgL_vrefz00_bglt) BgL_oz00_5498));
		}

	}



/* vref/Cinfo-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vrefzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_info3z00(BgL_vrefz00_bglt
		BgL_oz00_278, obj_t BgL_vz00_279)
	{
		{	/* Cfa/cinfo3.sch 480 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_278)))->
					BgL_sidezd2effectzd2) = ((obj_t) BgL_vz00_279), BUNSPEC);
		}

	}



/* &vref/Cinfo-side-effect-set! */
	obj_t BGl_z62vrefzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_info3z00(obj_t
		BgL_envz00_5499, obj_t BgL_oz00_5500, obj_t BgL_vz00_5501)
	{
		{	/* Cfa/cinfo3.sch 480 */
			return
				BGl_vrefzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_info3z00(
				((BgL_vrefz00_bglt) BgL_oz00_5500), BgL_vz00_5501);
		}

	}



/* vref/Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_vrefzf2Cinfozd2typez20zzcfa_info3z00(BgL_vrefz00_bglt BgL_oz00_280)
	{
		{	/* Cfa/cinfo3.sch 481 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_280)))->BgL_typez00);
		}

	}



/* &vref/Cinfo-type */
	BgL_typez00_bglt BGl_z62vrefzf2Cinfozd2typez42zzcfa_info3z00(obj_t
		BgL_envz00_5502, obj_t BgL_oz00_5503)
	{
		{	/* Cfa/cinfo3.sch 481 */
			return
				BGl_vrefzf2Cinfozd2typez20zzcfa_info3z00(
				((BgL_vrefz00_bglt) BgL_oz00_5503));
		}

	}



/* vref/Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vrefzf2Cinfozd2typezd2setz12ze0zzcfa_info3z00(BgL_vrefz00_bglt
		BgL_oz00_281, BgL_typez00_bglt BgL_vz00_282)
	{
		{	/* Cfa/cinfo3.sch 482 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_281)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_282), BUNSPEC);
		}

	}



/* &vref/Cinfo-type-set! */
	obj_t BGl_z62vrefzf2Cinfozd2typezd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5504, obj_t BgL_oz00_5505, obj_t BgL_vz00_5506)
	{
		{	/* Cfa/cinfo3.sch 482 */
			return
				BGl_vrefzf2Cinfozd2typezd2setz12ze0zzcfa_info3z00(
				((BgL_vrefz00_bglt) BgL_oz00_5505), ((BgL_typez00_bglt) BgL_vz00_5506));
		}

	}



/* vref/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_vrefzf2Cinfozd2locz20zzcfa_info3z00(BgL_vrefz00_bglt BgL_oz00_283)
	{
		{	/* Cfa/cinfo3.sch 483 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_283)))->BgL_locz00);
		}

	}



/* &vref/Cinfo-loc */
	obj_t BGl_z62vrefzf2Cinfozd2locz42zzcfa_info3z00(obj_t BgL_envz00_5507,
		obj_t BgL_oz00_5508)
	{
		{	/* Cfa/cinfo3.sch 483 */
			return
				BGl_vrefzf2Cinfozd2locz20zzcfa_info3z00(
				((BgL_vrefz00_bglt) BgL_oz00_5508));
		}

	}



/* make-vset!/Cinfo */
	BGL_EXPORTED_DEF BgL_vsetz12z12_bglt
		BGl_makezd2vsetz12zf2Cinfoz32zzcfa_info3z00(obj_t BgL_loc1390z00_286,
		BgL_typez00_bglt BgL_type1391z00_287, obj_t BgL_sidezd2effect1392zd2_288,
		obj_t BgL_key1393z00_289, obj_t BgL_exprza21394za2_290,
		obj_t BgL_effect1395z00_291, obj_t BgL_czd2format1396zd2_292,
		BgL_typez00_bglt BgL_ftype1397z00_293,
		BgL_typez00_bglt BgL_otype1398z00_294,
		BgL_typez00_bglt BgL_vtype1399z00_295, bool_t BgL_unsafe1400z00_296,
		BgL_approxz00_bglt BgL_approx1401z00_297, bool_t BgL_tvectorzf31402zf3_298)
	{
		{	/* Cfa/cinfo3.sch 487 */
			{	/* Cfa/cinfo3.sch 487 */
				BgL_vsetz12z12_bglt BgL_new1369z00_6422;

				{	/* Cfa/cinfo3.sch 487 */
					BgL_vsetz12z12_bglt BgL_tmp1367z00_6423;
					BgL_vsetz12zf2cinfoze0_bglt BgL_wide1368z00_6424;

					{
						BgL_vsetz12z12_bglt BgL_auxz00_8102;

						{	/* Cfa/cinfo3.sch 487 */
							BgL_vsetz12z12_bglt BgL_new1366z00_6425;

							BgL_new1366z00_6425 =
								((BgL_vsetz12z12_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_vsetz12z12_bgl))));
							{	/* Cfa/cinfo3.sch 487 */
								long BgL_arg1736z00_6426;

								BgL_arg1736z00_6426 =
									BGL_CLASS_NUM(BGl_vsetz12z12zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1366z00_6425),
									BgL_arg1736z00_6426);
							}
							{	/* Cfa/cinfo3.sch 487 */
								BgL_objectz00_bglt BgL_tmpz00_8107;

								BgL_tmpz00_8107 = ((BgL_objectz00_bglt) BgL_new1366z00_6425);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8107, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1366z00_6425);
							BgL_auxz00_8102 = BgL_new1366z00_6425;
						}
						BgL_tmp1367z00_6423 = ((BgL_vsetz12z12_bglt) BgL_auxz00_8102);
					}
					BgL_wide1368z00_6424 =
						((BgL_vsetz12zf2cinfoze0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_vsetz12zf2cinfoze0_bgl))));
					{	/* Cfa/cinfo3.sch 487 */
						obj_t BgL_auxz00_8115;
						BgL_objectz00_bglt BgL_tmpz00_8113;

						BgL_auxz00_8115 = ((obj_t) BgL_wide1368z00_6424);
						BgL_tmpz00_8113 = ((BgL_objectz00_bglt) BgL_tmp1367z00_6423);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8113, BgL_auxz00_8115);
					}
					((BgL_objectz00_bglt) BgL_tmp1367z00_6423);
					{	/* Cfa/cinfo3.sch 487 */
						long BgL_arg1735z00_6427;

						BgL_arg1735z00_6427 =
							BGL_CLASS_NUM(BGl_vsetz12zf2Cinfoze0zzcfa_info3z00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1367z00_6423), BgL_arg1735z00_6427);
					}
					BgL_new1369z00_6422 = ((BgL_vsetz12z12_bglt) BgL_tmp1367z00_6423);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1369z00_6422)))->BgL_locz00) =
					((obj_t) BgL_loc1390z00_286), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1369z00_6422)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1391z00_287), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1369z00_6422)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1392zd2_288), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1369z00_6422)))->BgL_keyz00) =
					((obj_t) BgL_key1393z00_289), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1369z00_6422)))->BgL_exprza2za2) =
					((obj_t) BgL_exprza21394za2_290), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1369z00_6422)))->BgL_effectz00) =
					((obj_t) BgL_effect1395z00_291), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1369z00_6422)))->BgL_czd2formatzd2) =
					((obj_t) BgL_czd2format1396zd2_292), BUNSPEC);
				((((BgL_vsetz12z12_bglt) COBJECT(((BgL_vsetz12z12_bglt)
									BgL_new1369z00_6422)))->BgL_ftypez00) =
					((BgL_typez00_bglt) BgL_ftype1397z00_293), BUNSPEC);
				((((BgL_vsetz12z12_bglt) COBJECT(((BgL_vsetz12z12_bglt)
									BgL_new1369z00_6422)))->BgL_otypez00) =
					((BgL_typez00_bglt) BgL_otype1398z00_294), BUNSPEC);
				((((BgL_vsetz12z12_bglt) COBJECT(((BgL_vsetz12z12_bglt)
									BgL_new1369z00_6422)))->BgL_vtypez00) =
					((BgL_typez00_bglt) BgL_vtype1399z00_295), BUNSPEC);
				((((BgL_vsetz12z12_bglt) COBJECT(((BgL_vsetz12z12_bglt)
									BgL_new1369z00_6422)))->BgL_unsafez00) =
					((bool_t) BgL_unsafe1400z00_296), BUNSPEC);
				{
					BgL_vsetz12zf2cinfoze0_bglt BgL_auxz00_8145;

					{
						obj_t BgL_auxz00_8146;

						{	/* Cfa/cinfo3.sch 487 */
							BgL_objectz00_bglt BgL_tmpz00_8147;

							BgL_tmpz00_8147 = ((BgL_objectz00_bglt) BgL_new1369z00_6422);
							BgL_auxz00_8146 = BGL_OBJECT_WIDENING(BgL_tmpz00_8147);
						}
						BgL_auxz00_8145 = ((BgL_vsetz12zf2cinfoze0_bglt) BgL_auxz00_8146);
					}
					((((BgL_vsetz12zf2cinfoze0_bglt) COBJECT(BgL_auxz00_8145))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1401z00_297), BUNSPEC);
				}
				{
					BgL_vsetz12zf2cinfoze0_bglt BgL_auxz00_8152;

					{
						obj_t BgL_auxz00_8153;

						{	/* Cfa/cinfo3.sch 487 */
							BgL_objectz00_bglt BgL_tmpz00_8154;

							BgL_tmpz00_8154 = ((BgL_objectz00_bglt) BgL_new1369z00_6422);
							BgL_auxz00_8153 = BGL_OBJECT_WIDENING(BgL_tmpz00_8154);
						}
						BgL_auxz00_8152 = ((BgL_vsetz12zf2cinfoze0_bglt) BgL_auxz00_8153);
					}
					((((BgL_vsetz12zf2cinfoze0_bglt) COBJECT(BgL_auxz00_8152))->
							BgL_tvectorzf3zf3) =
						((bool_t) BgL_tvectorzf31402zf3_298), BUNSPEC);
				}
				return BgL_new1369z00_6422;
			}
		}

	}



/* &make-vset!/Cinfo */
	BgL_vsetz12z12_bglt BGl_z62makezd2vsetz12zf2Cinfoz50zzcfa_info3z00(obj_t
		BgL_envz00_5509, obj_t BgL_loc1390z00_5510, obj_t BgL_type1391z00_5511,
		obj_t BgL_sidezd2effect1392zd2_5512, obj_t BgL_key1393z00_5513,
		obj_t BgL_exprza21394za2_5514, obj_t BgL_effect1395z00_5515,
		obj_t BgL_czd2format1396zd2_5516, obj_t BgL_ftype1397z00_5517,
		obj_t BgL_otype1398z00_5518, obj_t BgL_vtype1399z00_5519,
		obj_t BgL_unsafe1400z00_5520, obj_t BgL_approx1401z00_5521,
		obj_t BgL_tvectorzf31402zf3_5522)
	{
		{	/* Cfa/cinfo3.sch 487 */
			return
				BGl_makezd2vsetz12zf2Cinfoz32zzcfa_info3z00(BgL_loc1390z00_5510,
				((BgL_typez00_bglt) BgL_type1391z00_5511),
				BgL_sidezd2effect1392zd2_5512, BgL_key1393z00_5513,
				BgL_exprza21394za2_5514, BgL_effect1395z00_5515,
				BgL_czd2format1396zd2_5516, ((BgL_typez00_bglt) BgL_ftype1397z00_5517),
				((BgL_typez00_bglt) BgL_otype1398z00_5518),
				((BgL_typez00_bglt) BgL_vtype1399z00_5519),
				CBOOL(BgL_unsafe1400z00_5520),
				((BgL_approxz00_bglt) BgL_approx1401z00_5521),
				CBOOL(BgL_tvectorzf31402zf3_5522));
		}

	}



/* vset!/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_vsetz12zf2Cinfozf3z13zzcfa_info3z00(obj_t
		BgL_objz00_299)
	{
		{	/* Cfa/cinfo3.sch 488 */
			{	/* Cfa/cinfo3.sch 488 */
				obj_t BgL_classz00_6428;

				BgL_classz00_6428 = BGl_vsetz12zf2Cinfoze0zzcfa_info3z00;
				if (BGL_OBJECTP(BgL_objz00_299))
					{	/* Cfa/cinfo3.sch 488 */
						BgL_objectz00_bglt BgL_arg1809z00_6429;
						long BgL_arg1810z00_6430;

						BgL_arg1809z00_6429 = (BgL_objectz00_bglt) (BgL_objz00_299);
						BgL_arg1810z00_6430 = BGL_CLASS_DEPTH(BgL_classz00_6428);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo3.sch 488 */
								long BgL_idxz00_6431;

								BgL_idxz00_6431 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1809z00_6429);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6431 + BgL_arg1810z00_6430)) ==
									BgL_classz00_6428);
							}
						else
							{	/* Cfa/cinfo3.sch 488 */
								bool_t BgL_res2262z00_6434;

								{	/* Cfa/cinfo3.sch 488 */
									obj_t BgL_oclassz00_6435;

									{	/* Cfa/cinfo3.sch 488 */
										obj_t BgL_arg1815z00_6436;
										long BgL_arg1816z00_6437;

										BgL_arg1815z00_6436 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo3.sch 488 */
											long BgL_arg1817z00_6438;

											BgL_arg1817z00_6438 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1809z00_6429);
											BgL_arg1816z00_6437 = (BgL_arg1817z00_6438 - OBJECT_TYPE);
										}
										BgL_oclassz00_6435 =
											VECTOR_REF(BgL_arg1815z00_6436, BgL_arg1816z00_6437);
									}
									{	/* Cfa/cinfo3.sch 488 */
										bool_t BgL__ortest_1115z00_6439;

										BgL__ortest_1115z00_6439 =
											(BgL_classz00_6428 == BgL_oclassz00_6435);
										if (BgL__ortest_1115z00_6439)
											{	/* Cfa/cinfo3.sch 488 */
												BgL_res2262z00_6434 = BgL__ortest_1115z00_6439;
											}
										else
											{	/* Cfa/cinfo3.sch 488 */
												long BgL_odepthz00_6440;

												{	/* Cfa/cinfo3.sch 488 */
													obj_t BgL_arg1804z00_6441;

													BgL_arg1804z00_6441 = (BgL_oclassz00_6435);
													BgL_odepthz00_6440 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6441);
												}
												if ((BgL_arg1810z00_6430 < BgL_odepthz00_6440))
													{	/* Cfa/cinfo3.sch 488 */
														obj_t BgL_arg1802z00_6442;

														{	/* Cfa/cinfo3.sch 488 */
															obj_t BgL_arg1803z00_6443;

															BgL_arg1803z00_6443 = (BgL_oclassz00_6435);
															BgL_arg1802z00_6442 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6443,
																BgL_arg1810z00_6430);
														}
														BgL_res2262z00_6434 =
															(BgL_arg1802z00_6442 == BgL_classz00_6428);
													}
												else
													{	/* Cfa/cinfo3.sch 488 */
														BgL_res2262z00_6434 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2262z00_6434;
							}
					}
				else
					{	/* Cfa/cinfo3.sch 488 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &vset!/Cinfo? */
	obj_t BGl_z62vsetz12zf2Cinfozf3z71zzcfa_info3z00(obj_t BgL_envz00_5523,
		obj_t BgL_objz00_5524)
	{
		{	/* Cfa/cinfo3.sch 488 */
			return BBOOL(BGl_vsetz12zf2Cinfozf3z13zzcfa_info3z00(BgL_objz00_5524));
		}

	}



/* vset!/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_vsetz12z12_bglt
		BGl_vsetz12zf2Cinfozd2nilz32zzcfa_info3z00(void)
	{
		{	/* Cfa/cinfo3.sch 489 */
			{	/* Cfa/cinfo3.sch 489 */
				obj_t BgL_classz00_4389;

				BgL_classz00_4389 = BGl_vsetz12zf2Cinfoze0zzcfa_info3z00;
				{	/* Cfa/cinfo3.sch 489 */
					obj_t BgL__ortest_1117z00_4390;

					BgL__ortest_1117z00_4390 = BGL_CLASS_NIL(BgL_classz00_4389);
					if (CBOOL(BgL__ortest_1117z00_4390))
						{	/* Cfa/cinfo3.sch 489 */
							return ((BgL_vsetz12z12_bglt) BgL__ortest_1117z00_4390);
						}
					else
						{	/* Cfa/cinfo3.sch 489 */
							return
								((BgL_vsetz12z12_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4389));
						}
				}
			}
		}

	}



/* &vset!/Cinfo-nil */
	BgL_vsetz12z12_bglt BGl_z62vsetz12zf2Cinfozd2nilz50zzcfa_info3z00(obj_t
		BgL_envz00_5525)
	{
		{	/* Cfa/cinfo3.sch 489 */
			return BGl_vsetz12zf2Cinfozd2nilz32zzcfa_info3z00();
		}

	}



/* vset!/Cinfo-tvector? */
	BGL_EXPORTED_DEF bool_t
		BGl_vsetz12zf2Cinfozd2tvectorzf3zc1zzcfa_info3z00(BgL_vsetz12z12_bglt
		BgL_oz00_300)
	{
		{	/* Cfa/cinfo3.sch 490 */
			{
				BgL_vsetz12zf2cinfoze0_bglt BgL_auxz00_8199;

				{
					obj_t BgL_auxz00_8200;

					{	/* Cfa/cinfo3.sch 490 */
						BgL_objectz00_bglt BgL_tmpz00_8201;

						BgL_tmpz00_8201 = ((BgL_objectz00_bglt) BgL_oz00_300);
						BgL_auxz00_8200 = BGL_OBJECT_WIDENING(BgL_tmpz00_8201);
					}
					BgL_auxz00_8199 = ((BgL_vsetz12zf2cinfoze0_bglt) BgL_auxz00_8200);
				}
				return
					(((BgL_vsetz12zf2cinfoze0_bglt) COBJECT(BgL_auxz00_8199))->
					BgL_tvectorzf3zf3);
			}
		}

	}



/* &vset!/Cinfo-tvector? */
	obj_t BGl_z62vsetz12zf2Cinfozd2tvectorzf3za3zzcfa_info3z00(obj_t
		BgL_envz00_5526, obj_t BgL_oz00_5527)
	{
		{	/* Cfa/cinfo3.sch 490 */
			return
				BBOOL(BGl_vsetz12zf2Cinfozd2tvectorzf3zc1zzcfa_info3z00(
					((BgL_vsetz12z12_bglt) BgL_oz00_5527)));
		}

	}



/* vset!/Cinfo-tvector?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vsetz12zf2Cinfozd2tvectorzf3zd2setz12z01zzcfa_info3z00
		(BgL_vsetz12z12_bglt BgL_oz00_301, bool_t BgL_vz00_302)
	{
		{	/* Cfa/cinfo3.sch 491 */
			{
				BgL_vsetz12zf2cinfoze0_bglt BgL_auxz00_8209;

				{
					obj_t BgL_auxz00_8210;

					{	/* Cfa/cinfo3.sch 491 */
						BgL_objectz00_bglt BgL_tmpz00_8211;

						BgL_tmpz00_8211 = ((BgL_objectz00_bglt) BgL_oz00_301);
						BgL_auxz00_8210 = BGL_OBJECT_WIDENING(BgL_tmpz00_8211);
					}
					BgL_auxz00_8209 = ((BgL_vsetz12zf2cinfoze0_bglt) BgL_auxz00_8210);
				}
				return
					((((BgL_vsetz12zf2cinfoze0_bglt) COBJECT(BgL_auxz00_8209))->
						BgL_tvectorzf3zf3) = ((bool_t) BgL_vz00_302), BUNSPEC);
			}
		}

	}



/* &vset!/Cinfo-tvector?-set! */
	obj_t BGl_z62vsetz12zf2Cinfozd2tvectorzf3zd2setz12z63zzcfa_info3z00(obj_t
		BgL_envz00_5528, obj_t BgL_oz00_5529, obj_t BgL_vz00_5530)
	{
		{	/* Cfa/cinfo3.sch 491 */
			return
				BGl_vsetz12zf2Cinfozd2tvectorzf3zd2setz12z01zzcfa_info3z00(
				((BgL_vsetz12z12_bglt) BgL_oz00_5529), CBOOL(BgL_vz00_5530));
		}

	}



/* vset!/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_vsetz12zf2Cinfozd2approxz32zzcfa_info3z00(BgL_vsetz12z12_bglt
		BgL_oz00_303)
	{
		{	/* Cfa/cinfo3.sch 492 */
			{
				BgL_vsetz12zf2cinfoze0_bglt BgL_auxz00_8219;

				{
					obj_t BgL_auxz00_8220;

					{	/* Cfa/cinfo3.sch 492 */
						BgL_objectz00_bglt BgL_tmpz00_8221;

						BgL_tmpz00_8221 = ((BgL_objectz00_bglt) BgL_oz00_303);
						BgL_auxz00_8220 = BGL_OBJECT_WIDENING(BgL_tmpz00_8221);
					}
					BgL_auxz00_8219 = ((BgL_vsetz12zf2cinfoze0_bglt) BgL_auxz00_8220);
				}
				return
					(((BgL_vsetz12zf2cinfoze0_bglt) COBJECT(BgL_auxz00_8219))->
					BgL_approxz00);
			}
		}

	}



/* &vset!/Cinfo-approx */
	BgL_approxz00_bglt BGl_z62vsetz12zf2Cinfozd2approxz50zzcfa_info3z00(obj_t
		BgL_envz00_5531, obj_t BgL_oz00_5532)
	{
		{	/* Cfa/cinfo3.sch 492 */
			return
				BGl_vsetz12zf2Cinfozd2approxz32zzcfa_info3z00(
				((BgL_vsetz12z12_bglt) BgL_oz00_5532));
		}

	}



/* vset!/Cinfo-unsafe */
	BGL_EXPORTED_DEF bool_t
		BGl_vsetz12zf2Cinfozd2unsafez32zzcfa_info3z00(BgL_vsetz12z12_bglt
		BgL_oz00_306)
	{
		{	/* Cfa/cinfo3.sch 494 */
			return
				(((BgL_vsetz12z12_bglt) COBJECT(
						((BgL_vsetz12z12_bglt) BgL_oz00_306)))->BgL_unsafez00);
		}

	}



/* &vset!/Cinfo-unsafe */
	obj_t BGl_z62vsetz12zf2Cinfozd2unsafez50zzcfa_info3z00(obj_t BgL_envz00_5533,
		obj_t BgL_oz00_5534)
	{
		{	/* Cfa/cinfo3.sch 494 */
			return
				BBOOL(BGl_vsetz12zf2Cinfozd2unsafez32zzcfa_info3z00(
					((BgL_vsetz12z12_bglt) BgL_oz00_5534)));
		}

	}



/* vset!/Cinfo-vtype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_vsetz12zf2Cinfozd2vtypez32zzcfa_info3z00(BgL_vsetz12z12_bglt
		BgL_oz00_309)
	{
		{	/* Cfa/cinfo3.sch 496 */
			return
				(((BgL_vsetz12z12_bglt) COBJECT(
						((BgL_vsetz12z12_bglt) BgL_oz00_309)))->BgL_vtypez00);
		}

	}



/* &vset!/Cinfo-vtype */
	BgL_typez00_bglt BGl_z62vsetz12zf2Cinfozd2vtypez50zzcfa_info3z00(obj_t
		BgL_envz00_5535, obj_t BgL_oz00_5536)
	{
		{	/* Cfa/cinfo3.sch 496 */
			return
				BGl_vsetz12zf2Cinfozd2vtypez32zzcfa_info3z00(
				((BgL_vsetz12z12_bglt) BgL_oz00_5536));
		}

	}



/* vset!/Cinfo-vtype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vsetz12zf2Cinfozd2vtypezd2setz12zf2zzcfa_info3z00(BgL_vsetz12z12_bglt
		BgL_oz00_310, BgL_typez00_bglt BgL_vz00_311)
	{
		{	/* Cfa/cinfo3.sch 497 */
			return
				((((BgL_vsetz12z12_bglt) COBJECT(
							((BgL_vsetz12z12_bglt) BgL_oz00_310)))->BgL_vtypez00) =
				((BgL_typez00_bglt) BgL_vz00_311), BUNSPEC);
		}

	}



/* &vset!/Cinfo-vtype-set! */
	obj_t BGl_z62vsetz12zf2Cinfozd2vtypezd2setz12z90zzcfa_info3z00(obj_t
		BgL_envz00_5537, obj_t BgL_oz00_5538, obj_t BgL_vz00_5539)
	{
		{	/* Cfa/cinfo3.sch 497 */
			return
				BGl_vsetz12zf2Cinfozd2vtypezd2setz12zf2zzcfa_info3z00(
				((BgL_vsetz12z12_bglt) BgL_oz00_5538),
				((BgL_typez00_bglt) BgL_vz00_5539));
		}

	}



/* vset!/Cinfo-otype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_vsetz12zf2Cinfozd2otypez32zzcfa_info3z00(BgL_vsetz12z12_bglt
		BgL_oz00_312)
	{
		{	/* Cfa/cinfo3.sch 498 */
			return
				(((BgL_vsetz12z12_bglt) COBJECT(
						((BgL_vsetz12z12_bglt) BgL_oz00_312)))->BgL_otypez00);
		}

	}



/* &vset!/Cinfo-otype */
	BgL_typez00_bglt BGl_z62vsetz12zf2Cinfozd2otypez50zzcfa_info3z00(obj_t
		BgL_envz00_5540, obj_t BgL_oz00_5541)
	{
		{	/* Cfa/cinfo3.sch 498 */
			return
				BGl_vsetz12zf2Cinfozd2otypez32zzcfa_info3z00(
				((BgL_vsetz12z12_bglt) BgL_oz00_5541));
		}

	}



/* vset!/Cinfo-otype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vsetz12zf2Cinfozd2otypezd2setz12zf2zzcfa_info3z00(BgL_vsetz12z12_bglt
		BgL_oz00_313, BgL_typez00_bglt BgL_vz00_314)
	{
		{	/* Cfa/cinfo3.sch 499 */
			return
				((((BgL_vsetz12z12_bglt) COBJECT(
							((BgL_vsetz12z12_bglt) BgL_oz00_313)))->BgL_otypez00) =
				((BgL_typez00_bglt) BgL_vz00_314), BUNSPEC);
		}

	}



/* &vset!/Cinfo-otype-set! */
	obj_t BGl_z62vsetz12zf2Cinfozd2otypezd2setz12z90zzcfa_info3z00(obj_t
		BgL_envz00_5542, obj_t BgL_oz00_5543, obj_t BgL_vz00_5544)
	{
		{	/* Cfa/cinfo3.sch 499 */
			return
				BGl_vsetz12zf2Cinfozd2otypezd2setz12zf2zzcfa_info3z00(
				((BgL_vsetz12z12_bglt) BgL_oz00_5543),
				((BgL_typez00_bglt) BgL_vz00_5544));
		}

	}



/* vset!/Cinfo-ftype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_vsetz12zf2Cinfozd2ftypez32zzcfa_info3z00(BgL_vsetz12z12_bglt
		BgL_oz00_315)
	{
		{	/* Cfa/cinfo3.sch 500 */
			return
				(((BgL_vsetz12z12_bglt) COBJECT(
						((BgL_vsetz12z12_bglt) BgL_oz00_315)))->BgL_ftypez00);
		}

	}



/* &vset!/Cinfo-ftype */
	BgL_typez00_bglt BGl_z62vsetz12zf2Cinfozd2ftypez50zzcfa_info3z00(obj_t
		BgL_envz00_5545, obj_t BgL_oz00_5546)
	{
		{	/* Cfa/cinfo3.sch 500 */
			return
				BGl_vsetz12zf2Cinfozd2ftypez32zzcfa_info3z00(
				((BgL_vsetz12z12_bglt) BgL_oz00_5546));
		}

	}



/* vset!/Cinfo-ftype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vsetz12zf2Cinfozd2ftypezd2setz12zf2zzcfa_info3z00(BgL_vsetz12z12_bglt
		BgL_oz00_316, BgL_typez00_bglt BgL_vz00_317)
	{
		{	/* Cfa/cinfo3.sch 501 */
			return
				((((BgL_vsetz12z12_bglt) COBJECT(
							((BgL_vsetz12z12_bglt) BgL_oz00_316)))->BgL_ftypez00) =
				((BgL_typez00_bglt) BgL_vz00_317), BUNSPEC);
		}

	}



/* &vset!/Cinfo-ftype-set! */
	obj_t BGl_z62vsetz12zf2Cinfozd2ftypezd2setz12z90zzcfa_info3z00(obj_t
		BgL_envz00_5547, obj_t BgL_oz00_5548, obj_t BgL_vz00_5549)
	{
		{	/* Cfa/cinfo3.sch 501 */
			return
				BGl_vsetz12zf2Cinfozd2ftypezd2setz12zf2zzcfa_info3z00(
				((BgL_vsetz12z12_bglt) BgL_oz00_5548),
				((BgL_typez00_bglt) BgL_vz00_5549));
		}

	}



/* vset!/Cinfo-c-format */
	BGL_EXPORTED_DEF obj_t
		BGl_vsetz12zf2Cinfozd2czd2formatze0zzcfa_info3z00(BgL_vsetz12z12_bglt
		BgL_oz00_318)
	{
		{	/* Cfa/cinfo3.sch 502 */
			return
				(((BgL_privatez00_bglt) COBJECT(
						((BgL_privatez00_bglt) BgL_oz00_318)))->BgL_czd2formatzd2);
		}

	}



/* &vset!/Cinfo-c-format */
	obj_t BGl_z62vsetz12zf2Cinfozd2czd2formatz82zzcfa_info3z00(obj_t
		BgL_envz00_5550, obj_t BgL_oz00_5551)
	{
		{	/* Cfa/cinfo3.sch 502 */
			return
				BGl_vsetz12zf2Cinfozd2czd2formatze0zzcfa_info3z00(
				((BgL_vsetz12z12_bglt) BgL_oz00_5551));
		}

	}



/* vset!/Cinfo-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_vsetz12zf2Cinfozd2effectz32zzcfa_info3z00(BgL_vsetz12z12_bglt
		BgL_oz00_321)
	{
		{	/* Cfa/cinfo3.sch 504 */
			return
				(((BgL_externz00_bglt) COBJECT(
						((BgL_externz00_bglt) BgL_oz00_321)))->BgL_effectz00);
		}

	}



/* &vset!/Cinfo-effect */
	obj_t BGl_z62vsetz12zf2Cinfozd2effectz50zzcfa_info3z00(obj_t BgL_envz00_5552,
		obj_t BgL_oz00_5553)
	{
		{	/* Cfa/cinfo3.sch 504 */
			return
				BGl_vsetz12zf2Cinfozd2effectz32zzcfa_info3z00(
				((BgL_vsetz12z12_bglt) BgL_oz00_5553));
		}

	}



/* vset!/Cinfo-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vsetz12zf2Cinfozd2effectzd2setz12zf2zzcfa_info3z00(BgL_vsetz12z12_bglt
		BgL_oz00_322, obj_t BgL_vz00_323)
	{
		{	/* Cfa/cinfo3.sch 505 */
			return
				((((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_oz00_322)))->BgL_effectz00) =
				((obj_t) BgL_vz00_323), BUNSPEC);
		}

	}



/* &vset!/Cinfo-effect-set! */
	obj_t BGl_z62vsetz12zf2Cinfozd2effectzd2setz12z90zzcfa_info3z00(obj_t
		BgL_envz00_5554, obj_t BgL_oz00_5555, obj_t BgL_vz00_5556)
	{
		{	/* Cfa/cinfo3.sch 505 */
			return
				BGl_vsetz12zf2Cinfozd2effectzd2setz12zf2zzcfa_info3z00(
				((BgL_vsetz12z12_bglt) BgL_oz00_5555), BgL_vz00_5556);
		}

	}



/* vset!/Cinfo-expr* */
	BGL_EXPORTED_DEF obj_t
		BGl_vsetz12zf2Cinfozd2exprza2z90zzcfa_info3z00(BgL_vsetz12z12_bglt
		BgL_oz00_324)
	{
		{	/* Cfa/cinfo3.sch 506 */
			return
				(((BgL_externz00_bglt) COBJECT(
						((BgL_externz00_bglt) BgL_oz00_324)))->BgL_exprza2za2);
		}

	}



/* &vset!/Cinfo-expr* */
	obj_t BGl_z62vsetz12zf2Cinfozd2exprza2zf2zzcfa_info3z00(obj_t BgL_envz00_5557,
		obj_t BgL_oz00_5558)
	{
		{	/* Cfa/cinfo3.sch 506 */
			return
				BGl_vsetz12zf2Cinfozd2exprza2z90zzcfa_info3z00(
				((BgL_vsetz12z12_bglt) BgL_oz00_5558));
		}

	}



/* vset!/Cinfo-expr*-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vsetz12zf2Cinfozd2exprza2zd2setz12z50zzcfa_info3z00(BgL_vsetz12z12_bglt
		BgL_oz00_325, obj_t BgL_vz00_326)
	{
		{	/* Cfa/cinfo3.sch 507 */
			return
				((((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_oz00_325)))->BgL_exprza2za2) =
				((obj_t) BgL_vz00_326), BUNSPEC);
		}

	}



/* &vset!/Cinfo-expr*-set! */
	obj_t BGl_z62vsetz12zf2Cinfozd2exprza2zd2setz12z32zzcfa_info3z00(obj_t
		BgL_envz00_5559, obj_t BgL_oz00_5560, obj_t BgL_vz00_5561)
	{
		{	/* Cfa/cinfo3.sch 507 */
			return
				BGl_vsetz12zf2Cinfozd2exprza2zd2setz12z50zzcfa_info3z00(
				((BgL_vsetz12z12_bglt) BgL_oz00_5560), BgL_vz00_5561);
		}

	}



/* vset!/Cinfo-key */
	BGL_EXPORTED_DEF obj_t
		BGl_vsetz12zf2Cinfozd2keyz32zzcfa_info3z00(BgL_vsetz12z12_bglt BgL_oz00_327)
	{
		{	/* Cfa/cinfo3.sch 508 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_327)))->BgL_keyz00);
		}

	}



/* &vset!/Cinfo-key */
	obj_t BGl_z62vsetz12zf2Cinfozd2keyz50zzcfa_info3z00(obj_t BgL_envz00_5562,
		obj_t BgL_oz00_5563)
	{
		{	/* Cfa/cinfo3.sch 508 */
			return
				BGl_vsetz12zf2Cinfozd2keyz32zzcfa_info3z00(
				((BgL_vsetz12z12_bglt) BgL_oz00_5563));
		}

	}



/* vset!/Cinfo-key-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vsetz12zf2Cinfozd2keyzd2setz12zf2zzcfa_info3z00(BgL_vsetz12z12_bglt
		BgL_oz00_328, obj_t BgL_vz00_329)
	{
		{	/* Cfa/cinfo3.sch 509 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_328)))->BgL_keyz00) =
				((obj_t) BgL_vz00_329), BUNSPEC);
		}

	}



/* &vset!/Cinfo-key-set! */
	obj_t BGl_z62vsetz12zf2Cinfozd2keyzd2setz12z90zzcfa_info3z00(obj_t
		BgL_envz00_5564, obj_t BgL_oz00_5565, obj_t BgL_vz00_5566)
	{
		{	/* Cfa/cinfo3.sch 509 */
			return
				BGl_vsetz12zf2Cinfozd2keyzd2setz12zf2zzcfa_info3z00(
				((BgL_vsetz12z12_bglt) BgL_oz00_5565), BgL_vz00_5566);
		}

	}



/* vset!/Cinfo-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_vsetz12zf2Cinfozd2sidezd2effectze0zzcfa_info3z00(BgL_vsetz12z12_bglt
		BgL_oz00_330)
	{
		{	/* Cfa/cinfo3.sch 510 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_330)))->BgL_sidezd2effectzd2);
		}

	}



/* &vset!/Cinfo-side-effect */
	obj_t BGl_z62vsetz12zf2Cinfozd2sidezd2effectz82zzcfa_info3z00(obj_t
		BgL_envz00_5567, obj_t BgL_oz00_5568)
	{
		{	/* Cfa/cinfo3.sch 510 */
			return
				BGl_vsetz12zf2Cinfozd2sidezd2effectze0zzcfa_info3z00(
				((BgL_vsetz12z12_bglt) BgL_oz00_5568));
		}

	}



/* vset!/Cinfo-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vsetz12zf2Cinfozd2sidezd2effectzd2setz12z20zzcfa_info3z00
		(BgL_vsetz12z12_bglt BgL_oz00_331, obj_t BgL_vz00_332)
	{
		{	/* Cfa/cinfo3.sch 511 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_331)))->
					BgL_sidezd2effectzd2) = ((obj_t) BgL_vz00_332), BUNSPEC);
		}

	}



/* &vset!/Cinfo-side-effect-set! */
	obj_t BGl_z62vsetz12zf2Cinfozd2sidezd2effectzd2setz12z42zzcfa_info3z00(obj_t
		BgL_envz00_5569, obj_t BgL_oz00_5570, obj_t BgL_vz00_5571)
	{
		{	/* Cfa/cinfo3.sch 511 */
			return
				BGl_vsetz12zf2Cinfozd2sidezd2effectzd2setz12z20zzcfa_info3z00(
				((BgL_vsetz12z12_bglt) BgL_oz00_5570), BgL_vz00_5571);
		}

	}



/* vset!/Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_vsetz12zf2Cinfozd2typez32zzcfa_info3z00(BgL_vsetz12z12_bglt
		BgL_oz00_333)
	{
		{	/* Cfa/cinfo3.sch 512 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_333)))->BgL_typez00);
		}

	}



/* &vset!/Cinfo-type */
	BgL_typez00_bglt BGl_z62vsetz12zf2Cinfozd2typez50zzcfa_info3z00(obj_t
		BgL_envz00_5572, obj_t BgL_oz00_5573)
	{
		{	/* Cfa/cinfo3.sch 512 */
			return
				BGl_vsetz12zf2Cinfozd2typez32zzcfa_info3z00(
				((BgL_vsetz12z12_bglt) BgL_oz00_5573));
		}

	}



/* vset!/Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vsetz12zf2Cinfozd2typezd2setz12zf2zzcfa_info3z00(BgL_vsetz12z12_bglt
		BgL_oz00_334, BgL_typez00_bglt BgL_vz00_335)
	{
		{	/* Cfa/cinfo3.sch 513 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_334)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_335), BUNSPEC);
		}

	}



/* &vset!/Cinfo-type-set! */
	obj_t BGl_z62vsetz12zf2Cinfozd2typezd2setz12z90zzcfa_info3z00(obj_t
		BgL_envz00_5574, obj_t BgL_oz00_5575, obj_t BgL_vz00_5576)
	{
		{	/* Cfa/cinfo3.sch 513 */
			return
				BGl_vsetz12zf2Cinfozd2typezd2setz12zf2zzcfa_info3z00(
				((BgL_vsetz12z12_bglt) BgL_oz00_5575),
				((BgL_typez00_bglt) BgL_vz00_5576));
		}

	}



/* vset!/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_vsetz12zf2Cinfozd2locz32zzcfa_info3z00(BgL_vsetz12z12_bglt BgL_oz00_336)
	{
		{	/* Cfa/cinfo3.sch 514 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_336)))->BgL_locz00);
		}

	}



/* &vset!/Cinfo-loc */
	obj_t BGl_z62vsetz12zf2Cinfozd2locz50zzcfa_info3z00(obj_t BgL_envz00_5577,
		obj_t BgL_oz00_5578)
	{
		{	/* Cfa/cinfo3.sch 514 */
			return
				BGl_vsetz12zf2Cinfozd2locz32zzcfa_info3z00(
				((BgL_vsetz12z12_bglt) BgL_oz00_5578));
		}

	}



/* make-vlength/Cinfo */
	BGL_EXPORTED_DEF BgL_vlengthz00_bglt
		BGl_makezd2vlengthzf2Cinfoz20zzcfa_info3z00(obj_t BgL_loc1378z00_339,
		BgL_typez00_bglt BgL_type1379z00_340, obj_t BgL_sidezd2effect1380zd2_341,
		obj_t BgL_key1381z00_342, obj_t BgL_exprza21382za2_343,
		obj_t BgL_effect1383z00_344, obj_t BgL_czd2format1384zd2_345,
		BgL_typez00_bglt BgL_vtype1385z00_346, obj_t BgL_ftype1386z00_347,
		BgL_approxz00_bglt BgL_approx1387z00_348, bool_t BgL_tvectorzf31388zf3_349)
	{
		{	/* Cfa/cinfo3.sch 518 */
			{	/* Cfa/cinfo3.sch 518 */
				BgL_vlengthz00_bglt BgL_new1373z00_6444;

				{	/* Cfa/cinfo3.sch 518 */
					BgL_vlengthz00_bglt BgL_tmp1371z00_6445;
					BgL_vlengthzf2cinfozf2_bglt BgL_wide1372z00_6446;

					{
						BgL_vlengthz00_bglt BgL_auxz00_8309;

						{	/* Cfa/cinfo3.sch 518 */
							BgL_vlengthz00_bglt BgL_new1370z00_6447;

							BgL_new1370z00_6447 =
								((BgL_vlengthz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_vlengthz00_bgl))));
							{	/* Cfa/cinfo3.sch 518 */
								long BgL_arg1738z00_6448;

								BgL_arg1738z00_6448 =
									BGL_CLASS_NUM(BGl_vlengthz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1370z00_6447),
									BgL_arg1738z00_6448);
							}
							{	/* Cfa/cinfo3.sch 518 */
								BgL_objectz00_bglt BgL_tmpz00_8314;

								BgL_tmpz00_8314 = ((BgL_objectz00_bglt) BgL_new1370z00_6447);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8314, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1370z00_6447);
							BgL_auxz00_8309 = BgL_new1370z00_6447;
						}
						BgL_tmp1371z00_6445 = ((BgL_vlengthz00_bglt) BgL_auxz00_8309);
					}
					BgL_wide1372z00_6446 =
						((BgL_vlengthzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_vlengthzf2cinfozf2_bgl))));
					{	/* Cfa/cinfo3.sch 518 */
						obj_t BgL_auxz00_8322;
						BgL_objectz00_bglt BgL_tmpz00_8320;

						BgL_auxz00_8322 = ((obj_t) BgL_wide1372z00_6446);
						BgL_tmpz00_8320 = ((BgL_objectz00_bglt) BgL_tmp1371z00_6445);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8320, BgL_auxz00_8322);
					}
					((BgL_objectz00_bglt) BgL_tmp1371z00_6445);
					{	/* Cfa/cinfo3.sch 518 */
						long BgL_arg1737z00_6449;

						BgL_arg1737z00_6449 =
							BGL_CLASS_NUM(BGl_vlengthzf2Cinfozf2zzcfa_info3z00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1371z00_6445), BgL_arg1737z00_6449);
					}
					BgL_new1373z00_6444 = ((BgL_vlengthz00_bglt) BgL_tmp1371z00_6445);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1373z00_6444)))->BgL_locz00) =
					((obj_t) BgL_loc1378z00_339), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1373z00_6444)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1379z00_340), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1373z00_6444)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1380zd2_341), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1373z00_6444)))->BgL_keyz00) =
					((obj_t) BgL_key1381z00_342), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1373z00_6444)))->BgL_exprza2za2) =
					((obj_t) BgL_exprza21382za2_343), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1373z00_6444)))->BgL_effectz00) =
					((obj_t) BgL_effect1383z00_344), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1373z00_6444)))->BgL_czd2formatzd2) =
					((obj_t) BgL_czd2format1384zd2_345), BUNSPEC);
				((((BgL_vlengthz00_bglt) COBJECT(((BgL_vlengthz00_bglt)
									BgL_new1373z00_6444)))->BgL_vtypez00) =
					((BgL_typez00_bglt) BgL_vtype1385z00_346), BUNSPEC);
				((((BgL_vlengthz00_bglt) COBJECT(((BgL_vlengthz00_bglt)
									BgL_new1373z00_6444)))->BgL_ftypez00) =
					((obj_t) BgL_ftype1386z00_347), BUNSPEC);
				{
					BgL_vlengthzf2cinfozf2_bglt BgL_auxz00_8348;

					{
						obj_t BgL_auxz00_8349;

						{	/* Cfa/cinfo3.sch 518 */
							BgL_objectz00_bglt BgL_tmpz00_8350;

							BgL_tmpz00_8350 = ((BgL_objectz00_bglt) BgL_new1373z00_6444);
							BgL_auxz00_8349 = BGL_OBJECT_WIDENING(BgL_tmpz00_8350);
						}
						BgL_auxz00_8348 = ((BgL_vlengthzf2cinfozf2_bglt) BgL_auxz00_8349);
					}
					((((BgL_vlengthzf2cinfozf2_bglt) COBJECT(BgL_auxz00_8348))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1387z00_348), BUNSPEC);
				}
				{
					BgL_vlengthzf2cinfozf2_bglt BgL_auxz00_8355;

					{
						obj_t BgL_auxz00_8356;

						{	/* Cfa/cinfo3.sch 518 */
							BgL_objectz00_bglt BgL_tmpz00_8357;

							BgL_tmpz00_8357 = ((BgL_objectz00_bglt) BgL_new1373z00_6444);
							BgL_auxz00_8356 = BGL_OBJECT_WIDENING(BgL_tmpz00_8357);
						}
						BgL_auxz00_8355 = ((BgL_vlengthzf2cinfozf2_bglt) BgL_auxz00_8356);
					}
					((((BgL_vlengthzf2cinfozf2_bglt) COBJECT(BgL_auxz00_8355))->
							BgL_tvectorzf3zf3) =
						((bool_t) BgL_tvectorzf31388zf3_349), BUNSPEC);
				}
				return BgL_new1373z00_6444;
			}
		}

	}



/* &make-vlength/Cinfo */
	BgL_vlengthz00_bglt BGl_z62makezd2vlengthzf2Cinfoz42zzcfa_info3z00(obj_t
		BgL_envz00_5579, obj_t BgL_loc1378z00_5580, obj_t BgL_type1379z00_5581,
		obj_t BgL_sidezd2effect1380zd2_5582, obj_t BgL_key1381z00_5583,
		obj_t BgL_exprza21382za2_5584, obj_t BgL_effect1383z00_5585,
		obj_t BgL_czd2format1384zd2_5586, obj_t BgL_vtype1385z00_5587,
		obj_t BgL_ftype1386z00_5588, obj_t BgL_approx1387z00_5589,
		obj_t BgL_tvectorzf31388zf3_5590)
	{
		{	/* Cfa/cinfo3.sch 518 */
			return
				BGl_makezd2vlengthzf2Cinfoz20zzcfa_info3z00(BgL_loc1378z00_5580,
				((BgL_typez00_bglt) BgL_type1379z00_5581),
				BgL_sidezd2effect1380zd2_5582, BgL_key1381z00_5583,
				BgL_exprza21382za2_5584, BgL_effect1383z00_5585,
				BgL_czd2format1384zd2_5586, ((BgL_typez00_bglt) BgL_vtype1385z00_5587),
				BgL_ftype1386z00_5588, ((BgL_approxz00_bglt) BgL_approx1387z00_5589),
				CBOOL(BgL_tvectorzf31388zf3_5590));
		}

	}



/* vlength/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_vlengthzf2Cinfozf3z01zzcfa_info3z00(obj_t
		BgL_objz00_350)
	{
		{	/* Cfa/cinfo3.sch 519 */
			{	/* Cfa/cinfo3.sch 519 */
				obj_t BgL_classz00_6450;

				BgL_classz00_6450 = BGl_vlengthzf2Cinfozf2zzcfa_info3z00;
				if (BGL_OBJECTP(BgL_objz00_350))
					{	/* Cfa/cinfo3.sch 519 */
						BgL_objectz00_bglt BgL_arg1809z00_6451;
						long BgL_arg1810z00_6452;

						BgL_arg1809z00_6451 = (BgL_objectz00_bglt) (BgL_objz00_350);
						BgL_arg1810z00_6452 = BGL_CLASS_DEPTH(BgL_classz00_6450);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo3.sch 519 */
								long BgL_idxz00_6453;

								BgL_idxz00_6453 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1809z00_6451);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6453 + BgL_arg1810z00_6452)) ==
									BgL_classz00_6450);
							}
						else
							{	/* Cfa/cinfo3.sch 519 */
								bool_t BgL_res2263z00_6456;

								{	/* Cfa/cinfo3.sch 519 */
									obj_t BgL_oclassz00_6457;

									{	/* Cfa/cinfo3.sch 519 */
										obj_t BgL_arg1815z00_6458;
										long BgL_arg1816z00_6459;

										BgL_arg1815z00_6458 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo3.sch 519 */
											long BgL_arg1817z00_6460;

											BgL_arg1817z00_6460 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1809z00_6451);
											BgL_arg1816z00_6459 = (BgL_arg1817z00_6460 - OBJECT_TYPE);
										}
										BgL_oclassz00_6457 =
											VECTOR_REF(BgL_arg1815z00_6458, BgL_arg1816z00_6459);
									}
									{	/* Cfa/cinfo3.sch 519 */
										bool_t BgL__ortest_1115z00_6461;

										BgL__ortest_1115z00_6461 =
											(BgL_classz00_6450 == BgL_oclassz00_6457);
										if (BgL__ortest_1115z00_6461)
											{	/* Cfa/cinfo3.sch 519 */
												BgL_res2263z00_6456 = BgL__ortest_1115z00_6461;
											}
										else
											{	/* Cfa/cinfo3.sch 519 */
												long BgL_odepthz00_6462;

												{	/* Cfa/cinfo3.sch 519 */
													obj_t BgL_arg1804z00_6463;

													BgL_arg1804z00_6463 = (BgL_oclassz00_6457);
													BgL_odepthz00_6462 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6463);
												}
												if ((BgL_arg1810z00_6452 < BgL_odepthz00_6462))
													{	/* Cfa/cinfo3.sch 519 */
														obj_t BgL_arg1802z00_6464;

														{	/* Cfa/cinfo3.sch 519 */
															obj_t BgL_arg1803z00_6465;

															BgL_arg1803z00_6465 = (BgL_oclassz00_6457);
															BgL_arg1802z00_6464 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6465,
																BgL_arg1810z00_6452);
														}
														BgL_res2263z00_6456 =
															(BgL_arg1802z00_6464 == BgL_classz00_6450);
													}
												else
													{	/* Cfa/cinfo3.sch 519 */
														BgL_res2263z00_6456 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2263z00_6456;
							}
					}
				else
					{	/* Cfa/cinfo3.sch 519 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &vlength/Cinfo? */
	obj_t BGl_z62vlengthzf2Cinfozf3z63zzcfa_info3z00(obj_t BgL_envz00_5591,
		obj_t BgL_objz00_5592)
	{
		{	/* Cfa/cinfo3.sch 519 */
			return BBOOL(BGl_vlengthzf2Cinfozf3z01zzcfa_info3z00(BgL_objz00_5592));
		}

	}



/* vlength/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_vlengthz00_bglt
		BGl_vlengthzf2Cinfozd2nilz20zzcfa_info3z00(void)
	{
		{	/* Cfa/cinfo3.sch 520 */
			{	/* Cfa/cinfo3.sch 520 */
				obj_t BgL_classz00_4450;

				BgL_classz00_4450 = BGl_vlengthzf2Cinfozf2zzcfa_info3z00;
				{	/* Cfa/cinfo3.sch 520 */
					obj_t BgL__ortest_1117z00_4451;

					BgL__ortest_1117z00_4451 = BGL_CLASS_NIL(BgL_classz00_4450);
					if (CBOOL(BgL__ortest_1117z00_4451))
						{	/* Cfa/cinfo3.sch 520 */
							return ((BgL_vlengthz00_bglt) BgL__ortest_1117z00_4451);
						}
					else
						{	/* Cfa/cinfo3.sch 520 */
							return
								((BgL_vlengthz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4450));
						}
				}
			}
		}

	}



/* &vlength/Cinfo-nil */
	BgL_vlengthz00_bglt BGl_z62vlengthzf2Cinfozd2nilz42zzcfa_info3z00(obj_t
		BgL_envz00_5593)
	{
		{	/* Cfa/cinfo3.sch 520 */
			return BGl_vlengthzf2Cinfozd2nilz20zzcfa_info3z00();
		}

	}



/* vlength/Cinfo-tvector? */
	BGL_EXPORTED_DEF bool_t
		BGl_vlengthzf2Cinfozd2tvectorzf3zd3zzcfa_info3z00(BgL_vlengthz00_bglt
		BgL_oz00_351)
	{
		{	/* Cfa/cinfo3.sch 521 */
			{
				BgL_vlengthzf2cinfozf2_bglt BgL_auxz00_8399;

				{
					obj_t BgL_auxz00_8400;

					{	/* Cfa/cinfo3.sch 521 */
						BgL_objectz00_bglt BgL_tmpz00_8401;

						BgL_tmpz00_8401 = ((BgL_objectz00_bglt) BgL_oz00_351);
						BgL_auxz00_8400 = BGL_OBJECT_WIDENING(BgL_tmpz00_8401);
					}
					BgL_auxz00_8399 = ((BgL_vlengthzf2cinfozf2_bglt) BgL_auxz00_8400);
				}
				return
					(((BgL_vlengthzf2cinfozf2_bglt) COBJECT(BgL_auxz00_8399))->
					BgL_tvectorzf3zf3);
			}
		}

	}



/* &vlength/Cinfo-tvector? */
	obj_t BGl_z62vlengthzf2Cinfozd2tvectorzf3zb1zzcfa_info3z00(obj_t
		BgL_envz00_5594, obj_t BgL_oz00_5595)
	{
		{	/* Cfa/cinfo3.sch 521 */
			return
				BBOOL(BGl_vlengthzf2Cinfozd2tvectorzf3zd3zzcfa_info3z00(
					((BgL_vlengthz00_bglt) BgL_oz00_5595)));
		}

	}



/* vlength/Cinfo-tvector?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vlengthzf2Cinfozd2tvectorzf3zd2setz12z13zzcfa_info3z00
		(BgL_vlengthz00_bglt BgL_oz00_352, bool_t BgL_vz00_353)
	{
		{	/* Cfa/cinfo3.sch 522 */
			{
				BgL_vlengthzf2cinfozf2_bglt BgL_auxz00_8409;

				{
					obj_t BgL_auxz00_8410;

					{	/* Cfa/cinfo3.sch 522 */
						BgL_objectz00_bglt BgL_tmpz00_8411;

						BgL_tmpz00_8411 = ((BgL_objectz00_bglt) BgL_oz00_352);
						BgL_auxz00_8410 = BGL_OBJECT_WIDENING(BgL_tmpz00_8411);
					}
					BgL_auxz00_8409 = ((BgL_vlengthzf2cinfozf2_bglt) BgL_auxz00_8410);
				}
				return
					((((BgL_vlengthzf2cinfozf2_bglt) COBJECT(BgL_auxz00_8409))->
						BgL_tvectorzf3zf3) = ((bool_t) BgL_vz00_353), BUNSPEC);
			}
		}

	}



/* &vlength/Cinfo-tvector?-set! */
	obj_t BGl_z62vlengthzf2Cinfozd2tvectorzf3zd2setz12z71zzcfa_info3z00(obj_t
		BgL_envz00_5596, obj_t BgL_oz00_5597, obj_t BgL_vz00_5598)
	{
		{	/* Cfa/cinfo3.sch 522 */
			return
				BGl_vlengthzf2Cinfozd2tvectorzf3zd2setz12z13zzcfa_info3z00(
				((BgL_vlengthz00_bglt) BgL_oz00_5597), CBOOL(BgL_vz00_5598));
		}

	}



/* vlength/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_vlengthzf2Cinfozd2approxz20zzcfa_info3z00(BgL_vlengthz00_bglt
		BgL_oz00_354)
	{
		{	/* Cfa/cinfo3.sch 523 */
			{
				BgL_vlengthzf2cinfozf2_bglt BgL_auxz00_8419;

				{
					obj_t BgL_auxz00_8420;

					{	/* Cfa/cinfo3.sch 523 */
						BgL_objectz00_bglt BgL_tmpz00_8421;

						BgL_tmpz00_8421 = ((BgL_objectz00_bglt) BgL_oz00_354);
						BgL_auxz00_8420 = BGL_OBJECT_WIDENING(BgL_tmpz00_8421);
					}
					BgL_auxz00_8419 = ((BgL_vlengthzf2cinfozf2_bglt) BgL_auxz00_8420);
				}
				return
					(((BgL_vlengthzf2cinfozf2_bglt) COBJECT(BgL_auxz00_8419))->
					BgL_approxz00);
			}
		}

	}



/* &vlength/Cinfo-approx */
	BgL_approxz00_bglt BGl_z62vlengthzf2Cinfozd2approxz42zzcfa_info3z00(obj_t
		BgL_envz00_5599, obj_t BgL_oz00_5600)
	{
		{	/* Cfa/cinfo3.sch 523 */
			return
				BGl_vlengthzf2Cinfozd2approxz20zzcfa_info3z00(
				((BgL_vlengthz00_bglt) BgL_oz00_5600));
		}

	}



/* vlength/Cinfo-ftype */
	BGL_EXPORTED_DEF obj_t
		BGl_vlengthzf2Cinfozd2ftypez20zzcfa_info3z00(BgL_vlengthz00_bglt
		BgL_oz00_357)
	{
		{	/* Cfa/cinfo3.sch 525 */
			return
				(((BgL_vlengthz00_bglt) COBJECT(
						((BgL_vlengthz00_bglt) BgL_oz00_357)))->BgL_ftypez00);
		}

	}



/* &vlength/Cinfo-ftype */
	obj_t BGl_z62vlengthzf2Cinfozd2ftypez42zzcfa_info3z00(obj_t BgL_envz00_5601,
		obj_t BgL_oz00_5602)
	{
		{	/* Cfa/cinfo3.sch 525 */
			return
				BGl_vlengthzf2Cinfozd2ftypez20zzcfa_info3z00(
				((BgL_vlengthz00_bglt) BgL_oz00_5602));
		}

	}



/* vlength/Cinfo-vtype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_vlengthzf2Cinfozd2vtypez20zzcfa_info3z00(BgL_vlengthz00_bglt
		BgL_oz00_360)
	{
		{	/* Cfa/cinfo3.sch 527 */
			return
				(((BgL_vlengthz00_bglt) COBJECT(
						((BgL_vlengthz00_bglt) BgL_oz00_360)))->BgL_vtypez00);
		}

	}



/* &vlength/Cinfo-vtype */
	BgL_typez00_bglt BGl_z62vlengthzf2Cinfozd2vtypez42zzcfa_info3z00(obj_t
		BgL_envz00_5603, obj_t BgL_oz00_5604)
	{
		{	/* Cfa/cinfo3.sch 527 */
			return
				BGl_vlengthzf2Cinfozd2vtypez20zzcfa_info3z00(
				((BgL_vlengthz00_bglt) BgL_oz00_5604));
		}

	}



/* vlength/Cinfo-vtype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vlengthzf2Cinfozd2vtypezd2setz12ze0zzcfa_info3z00(BgL_vlengthz00_bglt
		BgL_oz00_361, BgL_typez00_bglt BgL_vz00_362)
	{
		{	/* Cfa/cinfo3.sch 528 */
			return
				((((BgL_vlengthz00_bglt) COBJECT(
							((BgL_vlengthz00_bglt) BgL_oz00_361)))->BgL_vtypez00) =
				((BgL_typez00_bglt) BgL_vz00_362), BUNSPEC);
		}

	}



/* &vlength/Cinfo-vtype-set! */
	obj_t BGl_z62vlengthzf2Cinfozd2vtypezd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5605, obj_t BgL_oz00_5606, obj_t BgL_vz00_5607)
	{
		{	/* Cfa/cinfo3.sch 528 */
			return
				BGl_vlengthzf2Cinfozd2vtypezd2setz12ze0zzcfa_info3z00(
				((BgL_vlengthz00_bglt) BgL_oz00_5606),
				((BgL_typez00_bglt) BgL_vz00_5607));
		}

	}



/* vlength/Cinfo-c-format */
	BGL_EXPORTED_DEF obj_t
		BGl_vlengthzf2Cinfozd2czd2formatzf2zzcfa_info3z00(BgL_vlengthz00_bglt
		BgL_oz00_363)
	{
		{	/* Cfa/cinfo3.sch 529 */
			return
				(((BgL_privatez00_bglt) COBJECT(
						((BgL_privatez00_bglt) BgL_oz00_363)))->BgL_czd2formatzd2);
		}

	}



/* &vlength/Cinfo-c-format */
	obj_t BGl_z62vlengthzf2Cinfozd2czd2formatz90zzcfa_info3z00(obj_t
		BgL_envz00_5608, obj_t BgL_oz00_5609)
	{
		{	/* Cfa/cinfo3.sch 529 */
			return
				BGl_vlengthzf2Cinfozd2czd2formatzf2zzcfa_info3z00(
				((BgL_vlengthz00_bglt) BgL_oz00_5609));
		}

	}



/* vlength/Cinfo-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_vlengthzf2Cinfozd2effectz20zzcfa_info3z00(BgL_vlengthz00_bglt
		BgL_oz00_366)
	{
		{	/* Cfa/cinfo3.sch 531 */
			return
				(((BgL_externz00_bglt) COBJECT(
						((BgL_externz00_bglt) BgL_oz00_366)))->BgL_effectz00);
		}

	}



/* &vlength/Cinfo-effect */
	obj_t BGl_z62vlengthzf2Cinfozd2effectz42zzcfa_info3z00(obj_t BgL_envz00_5610,
		obj_t BgL_oz00_5611)
	{
		{	/* Cfa/cinfo3.sch 531 */
			return
				BGl_vlengthzf2Cinfozd2effectz20zzcfa_info3z00(
				((BgL_vlengthz00_bglt) BgL_oz00_5611));
		}

	}



/* vlength/Cinfo-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vlengthzf2Cinfozd2effectzd2setz12ze0zzcfa_info3z00(BgL_vlengthz00_bglt
		BgL_oz00_367, obj_t BgL_vz00_368)
	{
		{	/* Cfa/cinfo3.sch 532 */
			return
				((((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_oz00_367)))->BgL_effectz00) =
				((obj_t) BgL_vz00_368), BUNSPEC);
		}

	}



/* &vlength/Cinfo-effect-set! */
	obj_t BGl_z62vlengthzf2Cinfozd2effectzd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5612, obj_t BgL_oz00_5613, obj_t BgL_vz00_5614)
	{
		{	/* Cfa/cinfo3.sch 532 */
			return
				BGl_vlengthzf2Cinfozd2effectzd2setz12ze0zzcfa_info3z00(
				((BgL_vlengthz00_bglt) BgL_oz00_5613), BgL_vz00_5614);
		}

	}



/* vlength/Cinfo-expr* */
	BGL_EXPORTED_DEF obj_t
		BGl_vlengthzf2Cinfozd2exprza2z82zzcfa_info3z00(BgL_vlengthz00_bglt
		BgL_oz00_369)
	{
		{	/* Cfa/cinfo3.sch 533 */
			return
				(((BgL_externz00_bglt) COBJECT(
						((BgL_externz00_bglt) BgL_oz00_369)))->BgL_exprza2za2);
		}

	}



/* &vlength/Cinfo-expr* */
	obj_t BGl_z62vlengthzf2Cinfozd2exprza2ze0zzcfa_info3z00(obj_t BgL_envz00_5615,
		obj_t BgL_oz00_5616)
	{
		{	/* Cfa/cinfo3.sch 533 */
			return
				BGl_vlengthzf2Cinfozd2exprza2z82zzcfa_info3z00(
				((BgL_vlengthz00_bglt) BgL_oz00_5616));
		}

	}



/* vlength/Cinfo-expr*-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vlengthzf2Cinfozd2exprza2zd2setz12z42zzcfa_info3z00(BgL_vlengthz00_bglt
		BgL_oz00_370, obj_t BgL_vz00_371)
	{
		{	/* Cfa/cinfo3.sch 534 */
			return
				((((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_oz00_370)))->BgL_exprza2za2) =
				((obj_t) BgL_vz00_371), BUNSPEC);
		}

	}



/* &vlength/Cinfo-expr*-set! */
	obj_t BGl_z62vlengthzf2Cinfozd2exprza2zd2setz12z20zzcfa_info3z00(obj_t
		BgL_envz00_5617, obj_t BgL_oz00_5618, obj_t BgL_vz00_5619)
	{
		{	/* Cfa/cinfo3.sch 534 */
			return
				BGl_vlengthzf2Cinfozd2exprza2zd2setz12z42zzcfa_info3z00(
				((BgL_vlengthz00_bglt) BgL_oz00_5618), BgL_vz00_5619);
		}

	}



/* vlength/Cinfo-key */
	BGL_EXPORTED_DEF obj_t
		BGl_vlengthzf2Cinfozd2keyz20zzcfa_info3z00(BgL_vlengthz00_bglt BgL_oz00_372)
	{
		{	/* Cfa/cinfo3.sch 535 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_372)))->BgL_keyz00);
		}

	}



/* &vlength/Cinfo-key */
	obj_t BGl_z62vlengthzf2Cinfozd2keyz42zzcfa_info3z00(obj_t BgL_envz00_5620,
		obj_t BgL_oz00_5621)
	{
		{	/* Cfa/cinfo3.sch 535 */
			return
				BGl_vlengthzf2Cinfozd2keyz20zzcfa_info3z00(
				((BgL_vlengthz00_bglt) BgL_oz00_5621));
		}

	}



/* vlength/Cinfo-key-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vlengthzf2Cinfozd2keyzd2setz12ze0zzcfa_info3z00(BgL_vlengthz00_bglt
		BgL_oz00_373, obj_t BgL_vz00_374)
	{
		{	/* Cfa/cinfo3.sch 536 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_373)))->BgL_keyz00) =
				((obj_t) BgL_vz00_374), BUNSPEC);
		}

	}



/* &vlength/Cinfo-key-set! */
	obj_t BGl_z62vlengthzf2Cinfozd2keyzd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5622, obj_t BgL_oz00_5623, obj_t BgL_vz00_5624)
	{
		{	/* Cfa/cinfo3.sch 536 */
			return
				BGl_vlengthzf2Cinfozd2keyzd2setz12ze0zzcfa_info3z00(
				((BgL_vlengthz00_bglt) BgL_oz00_5623), BgL_vz00_5624);
		}

	}



/* vlength/Cinfo-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_vlengthzf2Cinfozd2sidezd2effectzf2zzcfa_info3z00(BgL_vlengthz00_bglt
		BgL_oz00_375)
	{
		{	/* Cfa/cinfo3.sch 537 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_375)))->BgL_sidezd2effectzd2);
		}

	}



/* &vlength/Cinfo-side-effect */
	obj_t BGl_z62vlengthzf2Cinfozd2sidezd2effectz90zzcfa_info3z00(obj_t
		BgL_envz00_5625, obj_t BgL_oz00_5626)
	{
		{	/* Cfa/cinfo3.sch 537 */
			return
				BGl_vlengthzf2Cinfozd2sidezd2effectzf2zzcfa_info3z00(
				((BgL_vlengthz00_bglt) BgL_oz00_5626));
		}

	}



/* vlength/Cinfo-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vlengthzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_info3z00
		(BgL_vlengthz00_bglt BgL_oz00_376, obj_t BgL_vz00_377)
	{
		{	/* Cfa/cinfo3.sch 538 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_376)))->
					BgL_sidezd2effectzd2) = ((obj_t) BgL_vz00_377), BUNSPEC);
		}

	}



/* &vlength/Cinfo-side-effect-set! */
	obj_t BGl_z62vlengthzf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_info3z00(obj_t
		BgL_envz00_5627, obj_t BgL_oz00_5628, obj_t BgL_vz00_5629)
	{
		{	/* Cfa/cinfo3.sch 538 */
			return
				BGl_vlengthzf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_info3z00(
				((BgL_vlengthz00_bglt) BgL_oz00_5628), BgL_vz00_5629);
		}

	}



/* vlength/Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_vlengthzf2Cinfozd2typez20zzcfa_info3z00(BgL_vlengthz00_bglt
		BgL_oz00_378)
	{
		{	/* Cfa/cinfo3.sch 539 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_378)))->BgL_typez00);
		}

	}



/* &vlength/Cinfo-type */
	BgL_typez00_bglt BGl_z62vlengthzf2Cinfozd2typez42zzcfa_info3z00(obj_t
		BgL_envz00_5630, obj_t BgL_oz00_5631)
	{
		{	/* Cfa/cinfo3.sch 539 */
			return
				BGl_vlengthzf2Cinfozd2typez20zzcfa_info3z00(
				((BgL_vlengthz00_bglt) BgL_oz00_5631));
		}

	}



/* vlength/Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_vlengthzf2Cinfozd2typezd2setz12ze0zzcfa_info3z00(BgL_vlengthz00_bglt
		BgL_oz00_379, BgL_typez00_bglt BgL_vz00_380)
	{
		{	/* Cfa/cinfo3.sch 540 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_379)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_380), BUNSPEC);
		}

	}



/* &vlength/Cinfo-type-set! */
	obj_t BGl_z62vlengthzf2Cinfozd2typezd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5632, obj_t BgL_oz00_5633, obj_t BgL_vz00_5634)
	{
		{	/* Cfa/cinfo3.sch 540 */
			return
				BGl_vlengthzf2Cinfozd2typezd2setz12ze0zzcfa_info3z00(
				((BgL_vlengthz00_bglt) BgL_oz00_5633),
				((BgL_typez00_bglt) BgL_vz00_5634));
		}

	}



/* vlength/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_vlengthzf2Cinfozd2locz20zzcfa_info3z00(BgL_vlengthz00_bglt BgL_oz00_381)
	{
		{	/* Cfa/cinfo3.sch 541 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_381)))->BgL_locz00);
		}

	}



/* &vlength/Cinfo-loc */
	obj_t BGl_z62vlengthzf2Cinfozd2locz42zzcfa_info3z00(obj_t BgL_envz00_5635,
		obj_t BgL_oz00_5636)
	{
		{	/* Cfa/cinfo3.sch 541 */
			return
				BGl_vlengthzf2Cinfozd2locz20zzcfa_info3z00(
				((BgL_vlengthz00_bglt) BgL_oz00_5636));
		}

	}



/* make-pre-valloc/Cinfo */
	BGL_EXPORTED_DEF BgL_vallocz00_bglt
		BGl_makezd2prezd2valloczf2Cinfozf2zzcfa_info3z00(obj_t BgL_loc1367z00_384,
		BgL_typez00_bglt BgL_type1368z00_385, obj_t BgL_sidezd2effect1369zd2_386,
		obj_t BgL_key1370z00_387, obj_t BgL_exprza21371za2_388,
		obj_t BgL_effect1372z00_389, obj_t BgL_czd2format1373zd2_390,
		BgL_typez00_bglt BgL_ftype1374z00_391,
		BgL_typez00_bglt BgL_otype1375z00_392,
		BgL_variablez00_bglt BgL_owner1376z00_393)
	{
		{	/* Cfa/cinfo3.sch 545 */
			{	/* Cfa/cinfo3.sch 545 */
				BgL_vallocz00_bglt BgL_new1377z00_6466;

				{	/* Cfa/cinfo3.sch 545 */
					BgL_vallocz00_bglt BgL_tmp1375z00_6467;
					BgL_prezd2valloczf2cinfoz20_bglt BgL_wide1376z00_6468;

					{
						BgL_vallocz00_bglt BgL_auxz00_8490;

						{	/* Cfa/cinfo3.sch 545 */
							BgL_vallocz00_bglt BgL_new1374z00_6469;

							BgL_new1374z00_6469 =
								((BgL_vallocz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_vallocz00_bgl))));
							{	/* Cfa/cinfo3.sch 545 */
								long BgL_arg1740z00_6470;

								BgL_arg1740z00_6470 = BGL_CLASS_NUM(BGl_vallocz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1374z00_6469),
									BgL_arg1740z00_6470);
							}
							{	/* Cfa/cinfo3.sch 545 */
								BgL_objectz00_bglt BgL_tmpz00_8495;

								BgL_tmpz00_8495 = ((BgL_objectz00_bglt) BgL_new1374z00_6469);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8495, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1374z00_6469);
							BgL_auxz00_8490 = BgL_new1374z00_6469;
						}
						BgL_tmp1375z00_6467 = ((BgL_vallocz00_bglt) BgL_auxz00_8490);
					}
					BgL_wide1376z00_6468 =
						((BgL_prezd2valloczf2cinfoz20_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_prezd2valloczf2cinfoz20_bgl))));
					{	/* Cfa/cinfo3.sch 545 */
						obj_t BgL_auxz00_8503;
						BgL_objectz00_bglt BgL_tmpz00_8501;

						BgL_auxz00_8503 = ((obj_t) BgL_wide1376z00_6468);
						BgL_tmpz00_8501 = ((BgL_objectz00_bglt) BgL_tmp1375z00_6467);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8501, BgL_auxz00_8503);
					}
					((BgL_objectz00_bglt) BgL_tmp1375z00_6467);
					{	/* Cfa/cinfo3.sch 545 */
						long BgL_arg1739z00_6471;

						BgL_arg1739z00_6471 =
							BGL_CLASS_NUM(BGl_prezd2valloczf2Cinfoz20zzcfa_info3z00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1375z00_6467), BgL_arg1739z00_6471);
					}
					BgL_new1377z00_6466 = ((BgL_vallocz00_bglt) BgL_tmp1375z00_6467);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1377z00_6466)))->BgL_locz00) =
					((obj_t) BgL_loc1367z00_384), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1377z00_6466)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1368z00_385), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1377z00_6466)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1369zd2_386), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1377z00_6466)))->BgL_keyz00) =
					((obj_t) BgL_key1370z00_387), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1377z00_6466)))->BgL_exprza2za2) =
					((obj_t) BgL_exprza21371za2_388), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1377z00_6466)))->BgL_effectz00) =
					((obj_t) BgL_effect1372z00_389), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1377z00_6466)))->BgL_czd2formatzd2) =
					((obj_t) BgL_czd2format1373zd2_390), BUNSPEC);
				((((BgL_vallocz00_bglt) COBJECT(((BgL_vallocz00_bglt)
									BgL_new1377z00_6466)))->BgL_ftypez00) =
					((BgL_typez00_bglt) BgL_ftype1374z00_391), BUNSPEC);
				((((BgL_vallocz00_bglt) COBJECT(((BgL_vallocz00_bglt)
									BgL_new1377z00_6466)))->BgL_otypez00) =
					((BgL_typez00_bglt) BgL_otype1375z00_392), BUNSPEC);
				{
					BgL_prezd2valloczf2cinfoz20_bglt BgL_auxz00_8529;

					{
						obj_t BgL_auxz00_8530;

						{	/* Cfa/cinfo3.sch 545 */
							BgL_objectz00_bglt BgL_tmpz00_8531;

							BgL_tmpz00_8531 = ((BgL_objectz00_bglt) BgL_new1377z00_6466);
							BgL_auxz00_8530 = BGL_OBJECT_WIDENING(BgL_tmpz00_8531);
						}
						BgL_auxz00_8529 =
							((BgL_prezd2valloczf2cinfoz20_bglt) BgL_auxz00_8530);
					}
					((((BgL_prezd2valloczf2cinfoz20_bglt) COBJECT(BgL_auxz00_8529))->
							BgL_ownerz00) =
						((BgL_variablez00_bglt) BgL_owner1376z00_393), BUNSPEC);
				}
				return BgL_new1377z00_6466;
			}
		}

	}



/* &make-pre-valloc/Cinfo */
	BgL_vallocz00_bglt BGl_z62makezd2prezd2valloczf2Cinfoz90zzcfa_info3z00(obj_t
		BgL_envz00_5637, obj_t BgL_loc1367z00_5638, obj_t BgL_type1368z00_5639,
		obj_t BgL_sidezd2effect1369zd2_5640, obj_t BgL_key1370z00_5641,
		obj_t BgL_exprza21371za2_5642, obj_t BgL_effect1372z00_5643,
		obj_t BgL_czd2format1373zd2_5644, obj_t BgL_ftype1374z00_5645,
		obj_t BgL_otype1375z00_5646, obj_t BgL_owner1376z00_5647)
	{
		{	/* Cfa/cinfo3.sch 545 */
			return
				BGl_makezd2prezd2valloczf2Cinfozf2zzcfa_info3z00(BgL_loc1367z00_5638,
				((BgL_typez00_bglt) BgL_type1368z00_5639),
				BgL_sidezd2effect1369zd2_5640, BgL_key1370z00_5641,
				BgL_exprza21371za2_5642, BgL_effect1372z00_5643,
				BgL_czd2format1373zd2_5644, ((BgL_typez00_bglt) BgL_ftype1374z00_5645),
				((BgL_typez00_bglt) BgL_otype1375z00_5646),
				((BgL_variablez00_bglt) BgL_owner1376z00_5647));
		}

	}



/* pre-valloc/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_prezd2valloczf2Cinfozf3zd3zzcfa_info3z00(obj_t
		BgL_objz00_394)
	{
		{	/* Cfa/cinfo3.sch 546 */
			{	/* Cfa/cinfo3.sch 546 */
				obj_t BgL_classz00_6472;

				BgL_classz00_6472 = BGl_prezd2valloczf2Cinfoz20zzcfa_info3z00;
				if (BGL_OBJECTP(BgL_objz00_394))
					{	/* Cfa/cinfo3.sch 546 */
						BgL_objectz00_bglt BgL_arg1809z00_6473;
						long BgL_arg1810z00_6474;

						BgL_arg1809z00_6473 = (BgL_objectz00_bglt) (BgL_objz00_394);
						BgL_arg1810z00_6474 = BGL_CLASS_DEPTH(BgL_classz00_6472);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo3.sch 546 */
								long BgL_idxz00_6475;

								BgL_idxz00_6475 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1809z00_6473);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6475 + BgL_arg1810z00_6474)) ==
									BgL_classz00_6472);
							}
						else
							{	/* Cfa/cinfo3.sch 546 */
								bool_t BgL_res2264z00_6478;

								{	/* Cfa/cinfo3.sch 546 */
									obj_t BgL_oclassz00_6479;

									{	/* Cfa/cinfo3.sch 546 */
										obj_t BgL_arg1815z00_6480;
										long BgL_arg1816z00_6481;

										BgL_arg1815z00_6480 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo3.sch 546 */
											long BgL_arg1817z00_6482;

											BgL_arg1817z00_6482 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1809z00_6473);
											BgL_arg1816z00_6481 = (BgL_arg1817z00_6482 - OBJECT_TYPE);
										}
										BgL_oclassz00_6479 =
											VECTOR_REF(BgL_arg1815z00_6480, BgL_arg1816z00_6481);
									}
									{	/* Cfa/cinfo3.sch 546 */
										bool_t BgL__ortest_1115z00_6483;

										BgL__ortest_1115z00_6483 =
											(BgL_classz00_6472 == BgL_oclassz00_6479);
										if (BgL__ortest_1115z00_6483)
											{	/* Cfa/cinfo3.sch 546 */
												BgL_res2264z00_6478 = BgL__ortest_1115z00_6483;
											}
										else
											{	/* Cfa/cinfo3.sch 546 */
												long BgL_odepthz00_6484;

												{	/* Cfa/cinfo3.sch 546 */
													obj_t BgL_arg1804z00_6485;

													BgL_arg1804z00_6485 = (BgL_oclassz00_6479);
													BgL_odepthz00_6484 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6485);
												}
												if ((BgL_arg1810z00_6474 < BgL_odepthz00_6484))
													{	/* Cfa/cinfo3.sch 546 */
														obj_t BgL_arg1802z00_6486;

														{	/* Cfa/cinfo3.sch 546 */
															obj_t BgL_arg1803z00_6487;

															BgL_arg1803z00_6487 = (BgL_oclassz00_6479);
															BgL_arg1802z00_6486 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6487,
																BgL_arg1810z00_6474);
														}
														BgL_res2264z00_6478 =
															(BgL_arg1802z00_6486 == BgL_classz00_6472);
													}
												else
													{	/* Cfa/cinfo3.sch 546 */
														BgL_res2264z00_6478 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2264z00_6478;
							}
					}
				else
					{	/* Cfa/cinfo3.sch 546 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &pre-valloc/Cinfo? */
	obj_t BGl_z62prezd2valloczf2Cinfozf3zb1zzcfa_info3z00(obj_t BgL_envz00_5648,
		obj_t BgL_objz00_5649)
	{
		{	/* Cfa/cinfo3.sch 546 */
			return
				BBOOL(BGl_prezd2valloczf2Cinfozf3zd3zzcfa_info3z00(BgL_objz00_5649));
		}

	}



/* pre-valloc/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_vallocz00_bglt
		BGl_prezd2valloczf2Cinfozd2nilzf2zzcfa_info3z00(void)
	{
		{	/* Cfa/cinfo3.sch 547 */
			{	/* Cfa/cinfo3.sch 547 */
				obj_t BgL_classz00_4510;

				BgL_classz00_4510 = BGl_prezd2valloczf2Cinfoz20zzcfa_info3z00;
				{	/* Cfa/cinfo3.sch 547 */
					obj_t BgL__ortest_1117z00_4511;

					BgL__ortest_1117z00_4511 = BGL_CLASS_NIL(BgL_classz00_4510);
					if (CBOOL(BgL__ortest_1117z00_4511))
						{	/* Cfa/cinfo3.sch 547 */
							return ((BgL_vallocz00_bglt) BgL__ortest_1117z00_4511);
						}
					else
						{	/* Cfa/cinfo3.sch 547 */
							return
								((BgL_vallocz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4510));
						}
				}
			}
		}

	}



/* &pre-valloc/Cinfo-nil */
	BgL_vallocz00_bglt BGl_z62prezd2valloczf2Cinfozd2nilz90zzcfa_info3z00(obj_t
		BgL_envz00_5650)
	{
		{	/* Cfa/cinfo3.sch 547 */
			return BGl_prezd2valloczf2Cinfozd2nilzf2zzcfa_info3z00();
		}

	}



/* pre-valloc/Cinfo-owner */
	BGL_EXPORTED_DEF BgL_variablez00_bglt
		BGl_prezd2valloczf2Cinfozd2ownerzf2zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_395)
	{
		{	/* Cfa/cinfo3.sch 548 */
			{
				BgL_prezd2valloczf2cinfoz20_bglt BgL_auxz00_8573;

				{
					obj_t BgL_auxz00_8574;

					{	/* Cfa/cinfo3.sch 548 */
						BgL_objectz00_bglt BgL_tmpz00_8575;

						BgL_tmpz00_8575 = ((BgL_objectz00_bglt) BgL_oz00_395);
						BgL_auxz00_8574 = BGL_OBJECT_WIDENING(BgL_tmpz00_8575);
					}
					BgL_auxz00_8573 =
						((BgL_prezd2valloczf2cinfoz20_bglt) BgL_auxz00_8574);
				}
				return
					(((BgL_prezd2valloczf2cinfoz20_bglt) COBJECT(BgL_auxz00_8573))->
					BgL_ownerz00);
			}
		}

	}



/* &pre-valloc/Cinfo-owner */
	BgL_variablez00_bglt
		BGl_z62prezd2valloczf2Cinfozd2ownerz90zzcfa_info3z00(obj_t BgL_envz00_5651,
		obj_t BgL_oz00_5652)
	{
		{	/* Cfa/cinfo3.sch 548 */
			return
				BGl_prezd2valloczf2Cinfozd2ownerzf2zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5652));
		}

	}



/* pre-valloc/Cinfo-otype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_prezd2valloczf2Cinfozd2otypezf2zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_398)
	{
		{	/* Cfa/cinfo3.sch 550 */
			return
				(((BgL_vallocz00_bglt) COBJECT(
						((BgL_vallocz00_bglt) BgL_oz00_398)))->BgL_otypez00);
		}

	}



/* &pre-valloc/Cinfo-otype */
	BgL_typez00_bglt BGl_z62prezd2valloczf2Cinfozd2otypez90zzcfa_info3z00(obj_t
		BgL_envz00_5653, obj_t BgL_oz00_5654)
	{
		{	/* Cfa/cinfo3.sch 550 */
			return
				BGl_prezd2valloczf2Cinfozd2otypezf2zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5654));
		}

	}



/* pre-valloc/Cinfo-otype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_prezd2valloczf2Cinfozd2otypezd2setz12z32zzcfa_info3z00
		(BgL_vallocz00_bglt BgL_oz00_399, BgL_typez00_bglt BgL_vz00_400)
	{
		{	/* Cfa/cinfo3.sch 551 */
			return
				((((BgL_vallocz00_bglt) COBJECT(
							((BgL_vallocz00_bglt) BgL_oz00_399)))->BgL_otypez00) =
				((BgL_typez00_bglt) BgL_vz00_400), BUNSPEC);
		}

	}



/* &pre-valloc/Cinfo-otype-set! */
	obj_t BGl_z62prezd2valloczf2Cinfozd2otypezd2setz12z50zzcfa_info3z00(obj_t
		BgL_envz00_5655, obj_t BgL_oz00_5656, obj_t BgL_vz00_5657)
	{
		{	/* Cfa/cinfo3.sch 551 */
			return
				BGl_prezd2valloczf2Cinfozd2otypezd2setz12z32zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5656),
				((BgL_typez00_bglt) BgL_vz00_5657));
		}

	}



/* pre-valloc/Cinfo-ftype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_prezd2valloczf2Cinfozd2ftypezf2zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_401)
	{
		{	/* Cfa/cinfo3.sch 552 */
			return
				(((BgL_vallocz00_bglt) COBJECT(
						((BgL_vallocz00_bglt) BgL_oz00_401)))->BgL_ftypez00);
		}

	}



/* &pre-valloc/Cinfo-ftype */
	BgL_typez00_bglt BGl_z62prezd2valloczf2Cinfozd2ftypez90zzcfa_info3z00(obj_t
		BgL_envz00_5658, obj_t BgL_oz00_5659)
	{
		{	/* Cfa/cinfo3.sch 552 */
			return
				BGl_prezd2valloczf2Cinfozd2ftypezf2zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5659));
		}

	}



/* pre-valloc/Cinfo-ftype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_prezd2valloczf2Cinfozd2ftypezd2setz12z32zzcfa_info3z00
		(BgL_vallocz00_bglt BgL_oz00_402, BgL_typez00_bglt BgL_vz00_403)
	{
		{	/* Cfa/cinfo3.sch 553 */
			return
				((((BgL_vallocz00_bglt) COBJECT(
							((BgL_vallocz00_bglt) BgL_oz00_402)))->BgL_ftypez00) =
				((BgL_typez00_bglt) BgL_vz00_403), BUNSPEC);
		}

	}



/* &pre-valloc/Cinfo-ftype-set! */
	obj_t BGl_z62prezd2valloczf2Cinfozd2ftypezd2setz12z50zzcfa_info3z00(obj_t
		BgL_envz00_5660, obj_t BgL_oz00_5661, obj_t BgL_vz00_5662)
	{
		{	/* Cfa/cinfo3.sch 553 */
			return
				BGl_prezd2valloczf2Cinfozd2ftypezd2setz12z32zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5661),
				((BgL_typez00_bglt) BgL_vz00_5662));
		}

	}



/* pre-valloc/Cinfo-c-format */
	BGL_EXPORTED_DEF obj_t
		BGl_prezd2valloczf2Cinfozd2czd2formatz20zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_404)
	{
		{	/* Cfa/cinfo3.sch 554 */
			return
				(((BgL_privatez00_bglt) COBJECT(
						((BgL_privatez00_bglt) BgL_oz00_404)))->BgL_czd2formatzd2);
		}

	}



/* &pre-valloc/Cinfo-c-format */
	obj_t BGl_z62prezd2valloczf2Cinfozd2czd2formatz42zzcfa_info3z00(obj_t
		BgL_envz00_5663, obj_t BgL_oz00_5664)
	{
		{	/* Cfa/cinfo3.sch 554 */
			return
				BGl_prezd2valloczf2Cinfozd2czd2formatz20zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5664));
		}

	}



/* pre-valloc/Cinfo-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_prezd2valloczf2Cinfozd2effectzf2zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_407)
	{
		{	/* Cfa/cinfo3.sch 556 */
			return
				(((BgL_externz00_bglt) COBJECT(
						((BgL_externz00_bglt) BgL_oz00_407)))->BgL_effectz00);
		}

	}



/* &pre-valloc/Cinfo-effect */
	obj_t BGl_z62prezd2valloczf2Cinfozd2effectz90zzcfa_info3z00(obj_t
		BgL_envz00_5665, obj_t BgL_oz00_5666)
	{
		{	/* Cfa/cinfo3.sch 556 */
			return
				BGl_prezd2valloczf2Cinfozd2effectzf2zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5666));
		}

	}



/* pre-valloc/Cinfo-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_prezd2valloczf2Cinfozd2effectzd2setz12z32zzcfa_info3z00
		(BgL_vallocz00_bglt BgL_oz00_408, obj_t BgL_vz00_409)
	{
		{	/* Cfa/cinfo3.sch 557 */
			return
				((((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_oz00_408)))->BgL_effectz00) =
				((obj_t) BgL_vz00_409), BUNSPEC);
		}

	}



/* &pre-valloc/Cinfo-effect-set! */
	obj_t BGl_z62prezd2valloczf2Cinfozd2effectzd2setz12z50zzcfa_info3z00(obj_t
		BgL_envz00_5667, obj_t BgL_oz00_5668, obj_t BgL_vz00_5669)
	{
		{	/* Cfa/cinfo3.sch 557 */
			return
				BGl_prezd2valloczf2Cinfozd2effectzd2setz12z32zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5668), BgL_vz00_5669);
		}

	}



/* pre-valloc/Cinfo-expr* */
	BGL_EXPORTED_DEF obj_t
		BGl_prezd2valloczf2Cinfozd2exprza2z50zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_410)
	{
		{	/* Cfa/cinfo3.sch 558 */
			return
				(((BgL_externz00_bglt) COBJECT(
						((BgL_externz00_bglt) BgL_oz00_410)))->BgL_exprza2za2);
		}

	}



/* &pre-valloc/Cinfo-expr* */
	obj_t BGl_z62prezd2valloczf2Cinfozd2exprza2z32zzcfa_info3z00(obj_t
		BgL_envz00_5670, obj_t BgL_oz00_5671)
	{
		{	/* Cfa/cinfo3.sch 558 */
			return
				BGl_prezd2valloczf2Cinfozd2exprza2z50zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5671));
		}

	}



/* pre-valloc/Cinfo-expr*-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_prezd2valloczf2Cinfozd2exprza2zd2setz12z90zzcfa_info3z00
		(BgL_vallocz00_bglt BgL_oz00_411, obj_t BgL_vz00_412)
	{
		{	/* Cfa/cinfo3.sch 559 */
			return
				((((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_oz00_411)))->BgL_exprza2za2) =
				((obj_t) BgL_vz00_412), BUNSPEC);
		}

	}



/* &pre-valloc/Cinfo-expr*-set! */
	obj_t BGl_z62prezd2valloczf2Cinfozd2exprza2zd2setz12zf2zzcfa_info3z00(obj_t
		BgL_envz00_5672, obj_t BgL_oz00_5673, obj_t BgL_vz00_5674)
	{
		{	/* Cfa/cinfo3.sch 559 */
			return
				BGl_prezd2valloczf2Cinfozd2exprza2zd2setz12z90zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5673), BgL_vz00_5674);
		}

	}



/* pre-valloc/Cinfo-key */
	BGL_EXPORTED_DEF obj_t
		BGl_prezd2valloczf2Cinfozd2keyzf2zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_413)
	{
		{	/* Cfa/cinfo3.sch 560 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_413)))->BgL_keyz00);
		}

	}



/* &pre-valloc/Cinfo-key */
	obj_t BGl_z62prezd2valloczf2Cinfozd2keyz90zzcfa_info3z00(obj_t
		BgL_envz00_5675, obj_t BgL_oz00_5676)
	{
		{	/* Cfa/cinfo3.sch 560 */
			return
				BGl_prezd2valloczf2Cinfozd2keyzf2zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5676));
		}

	}



/* pre-valloc/Cinfo-key-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_prezd2valloczf2Cinfozd2keyzd2setz12z32zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_414, obj_t BgL_vz00_415)
	{
		{	/* Cfa/cinfo3.sch 561 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_414)))->BgL_keyz00) =
				((obj_t) BgL_vz00_415), BUNSPEC);
		}

	}



/* &pre-valloc/Cinfo-key-set! */
	obj_t BGl_z62prezd2valloczf2Cinfozd2keyzd2setz12z50zzcfa_info3z00(obj_t
		BgL_envz00_5677, obj_t BgL_oz00_5678, obj_t BgL_vz00_5679)
	{
		{	/* Cfa/cinfo3.sch 561 */
			return
				BGl_prezd2valloczf2Cinfozd2keyzd2setz12z32zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5678), BgL_vz00_5679);
		}

	}



/* pre-valloc/Cinfo-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_prezd2valloczf2Cinfozd2sidezd2effectz20zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_416)
	{
		{	/* Cfa/cinfo3.sch 562 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_416)))->BgL_sidezd2effectzd2);
		}

	}



/* &pre-valloc/Cinfo-side-effect */
	obj_t BGl_z62prezd2valloczf2Cinfozd2sidezd2effectz42zzcfa_info3z00(obj_t
		BgL_envz00_5680, obj_t BgL_oz00_5681)
	{
		{	/* Cfa/cinfo3.sch 562 */
			return
				BGl_prezd2valloczf2Cinfozd2sidezd2effectz20zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5681));
		}

	}



/* pre-valloc/Cinfo-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_prezd2valloczf2Cinfozd2sidezd2effectzd2setz12ze0zzcfa_info3z00
		(BgL_vallocz00_bglt BgL_oz00_417, obj_t BgL_vz00_418)
	{
		{	/* Cfa/cinfo3.sch 563 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_417)))->
					BgL_sidezd2effectzd2) = ((obj_t) BgL_vz00_418), BUNSPEC);
		}

	}



/* &pre-valloc/Cinfo-side-effect-set! */
	obj_t
		BGl_z62prezd2valloczf2Cinfozd2sidezd2effectzd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5682, obj_t BgL_oz00_5683, obj_t BgL_vz00_5684)
	{
		{	/* Cfa/cinfo3.sch 563 */
			return
				BGl_prezd2valloczf2Cinfozd2sidezd2effectzd2setz12ze0zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5683), BgL_vz00_5684);
		}

	}



/* pre-valloc/Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_prezd2valloczf2Cinfozd2typezf2zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_419)
	{
		{	/* Cfa/cinfo3.sch 564 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_419)))->BgL_typez00);
		}

	}



/* &pre-valloc/Cinfo-type */
	BgL_typez00_bglt BGl_z62prezd2valloczf2Cinfozd2typez90zzcfa_info3z00(obj_t
		BgL_envz00_5685, obj_t BgL_oz00_5686)
	{
		{	/* Cfa/cinfo3.sch 564 */
			return
				BGl_prezd2valloczf2Cinfozd2typezf2zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5686));
		}

	}



/* pre-valloc/Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_prezd2valloczf2Cinfozd2typezd2setz12z32zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_420, BgL_typez00_bglt BgL_vz00_421)
	{
		{	/* Cfa/cinfo3.sch 565 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_420)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_421), BUNSPEC);
		}

	}



/* &pre-valloc/Cinfo-type-set! */
	obj_t BGl_z62prezd2valloczf2Cinfozd2typezd2setz12z50zzcfa_info3z00(obj_t
		BgL_envz00_5687, obj_t BgL_oz00_5688, obj_t BgL_vz00_5689)
	{
		{	/* Cfa/cinfo3.sch 565 */
			return
				BGl_prezd2valloczf2Cinfozd2typezd2setz12z32zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5688),
				((BgL_typez00_bglt) BgL_vz00_5689));
		}

	}



/* pre-valloc/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_prezd2valloczf2Cinfozd2loczf2zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_422)
	{
		{	/* Cfa/cinfo3.sch 566 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_422)))->BgL_locz00);
		}

	}



/* &pre-valloc/Cinfo-loc */
	obj_t BGl_z62prezd2valloczf2Cinfozd2locz90zzcfa_info3z00(obj_t
		BgL_envz00_5690, obj_t BgL_oz00_5691)
	{
		{	/* Cfa/cinfo3.sch 566 */
			return
				BGl_prezd2valloczf2Cinfozd2loczf2zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5691));
		}

	}



/* make-valloc/Cinfo */
	BGL_EXPORTED_DEF BgL_vallocz00_bglt
		BGl_makezd2valloczf2Cinfoz20zzcfa_info3z00(obj_t BgL_loc1356z00_425,
		BgL_typez00_bglt BgL_type1357z00_426, obj_t BgL_sidezd2effect1358zd2_427,
		obj_t BgL_key1359z00_428, obj_t BgL_exprza21360za2_429,
		obj_t BgL_effect1361z00_430, obj_t BgL_czd2format1362zd2_431,
		BgL_typez00_bglt BgL_ftype1363z00_432,
		BgL_typez00_bglt BgL_otype1364z00_433,
		BgL_approxz00_bglt BgL_approx1365z00_434)
	{
		{	/* Cfa/cinfo3.sch 570 */
			{	/* Cfa/cinfo3.sch 570 */
				BgL_vallocz00_bglt BgL_new1381z00_6488;

				{	/* Cfa/cinfo3.sch 570 */
					BgL_vallocz00_bglt BgL_tmp1379z00_6489;
					BgL_valloczf2cinfozf2_bglt BgL_wide1380z00_6490;

					{
						BgL_vallocz00_bglt BgL_auxz00_8649;

						{	/* Cfa/cinfo3.sch 570 */
							BgL_vallocz00_bglt BgL_new1378z00_6491;

							BgL_new1378z00_6491 =
								((BgL_vallocz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_vallocz00_bgl))));
							{	/* Cfa/cinfo3.sch 570 */
								long BgL_arg1747z00_6492;

								BgL_arg1747z00_6492 = BGL_CLASS_NUM(BGl_vallocz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1378z00_6491),
									BgL_arg1747z00_6492);
							}
							{	/* Cfa/cinfo3.sch 570 */
								BgL_objectz00_bglt BgL_tmpz00_8654;

								BgL_tmpz00_8654 = ((BgL_objectz00_bglt) BgL_new1378z00_6491);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8654, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1378z00_6491);
							BgL_auxz00_8649 = BgL_new1378z00_6491;
						}
						BgL_tmp1379z00_6489 = ((BgL_vallocz00_bglt) BgL_auxz00_8649);
					}
					BgL_wide1380z00_6490 =
						((BgL_valloczf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_valloczf2cinfozf2_bgl))));
					{	/* Cfa/cinfo3.sch 570 */
						obj_t BgL_auxz00_8662;
						BgL_objectz00_bglt BgL_tmpz00_8660;

						BgL_auxz00_8662 = ((obj_t) BgL_wide1380z00_6490);
						BgL_tmpz00_8660 = ((BgL_objectz00_bglt) BgL_tmp1379z00_6489);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8660, BgL_auxz00_8662);
					}
					((BgL_objectz00_bglt) BgL_tmp1379z00_6489);
					{	/* Cfa/cinfo3.sch 570 */
						long BgL_arg1746z00_6493;

						BgL_arg1746z00_6493 =
							BGL_CLASS_NUM(BGl_valloczf2Cinfozf2zzcfa_info3z00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1379z00_6489), BgL_arg1746z00_6493);
					}
					BgL_new1381z00_6488 = ((BgL_vallocz00_bglt) BgL_tmp1379z00_6489);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1381z00_6488)))->BgL_locz00) =
					((obj_t) BgL_loc1356z00_425), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1381z00_6488)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1357z00_426), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1381z00_6488)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1358zd2_427), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1381z00_6488)))->BgL_keyz00) =
					((obj_t) BgL_key1359z00_428), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1381z00_6488)))->BgL_exprza2za2) =
					((obj_t) BgL_exprza21360za2_429), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1381z00_6488)))->BgL_effectz00) =
					((obj_t) BgL_effect1361z00_430), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1381z00_6488)))->BgL_czd2formatzd2) =
					((obj_t) BgL_czd2format1362zd2_431), BUNSPEC);
				((((BgL_vallocz00_bglt) COBJECT(((BgL_vallocz00_bglt)
									BgL_new1381z00_6488)))->BgL_ftypez00) =
					((BgL_typez00_bglt) BgL_ftype1363z00_432), BUNSPEC);
				((((BgL_vallocz00_bglt) COBJECT(((BgL_vallocz00_bglt)
									BgL_new1381z00_6488)))->BgL_otypez00) =
					((BgL_typez00_bglt) BgL_otype1364z00_433), BUNSPEC);
				{
					BgL_valloczf2cinfozf2_bglt BgL_auxz00_8688;

					{
						obj_t BgL_auxz00_8689;

						{	/* Cfa/cinfo3.sch 570 */
							BgL_objectz00_bglt BgL_tmpz00_8690;

							BgL_tmpz00_8690 = ((BgL_objectz00_bglt) BgL_new1381z00_6488);
							BgL_auxz00_8689 = BGL_OBJECT_WIDENING(BgL_tmpz00_8690);
						}
						BgL_auxz00_8688 = ((BgL_valloczf2cinfozf2_bglt) BgL_auxz00_8689);
					}
					((((BgL_valloczf2cinfozf2_bglt) COBJECT(BgL_auxz00_8688))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1365z00_434), BUNSPEC);
				}
				return BgL_new1381z00_6488;
			}
		}

	}



/* &make-valloc/Cinfo */
	BgL_vallocz00_bglt BGl_z62makezd2valloczf2Cinfoz42zzcfa_info3z00(obj_t
		BgL_envz00_5692, obj_t BgL_loc1356z00_5693, obj_t BgL_type1357z00_5694,
		obj_t BgL_sidezd2effect1358zd2_5695, obj_t BgL_key1359z00_5696,
		obj_t BgL_exprza21360za2_5697, obj_t BgL_effect1361z00_5698,
		obj_t BgL_czd2format1362zd2_5699, obj_t BgL_ftype1363z00_5700,
		obj_t BgL_otype1364z00_5701, obj_t BgL_approx1365z00_5702)
	{
		{	/* Cfa/cinfo3.sch 570 */
			return
				BGl_makezd2valloczf2Cinfoz20zzcfa_info3z00(BgL_loc1356z00_5693,
				((BgL_typez00_bglt) BgL_type1357z00_5694),
				BgL_sidezd2effect1358zd2_5695, BgL_key1359z00_5696,
				BgL_exprza21360za2_5697, BgL_effect1361z00_5698,
				BgL_czd2format1362zd2_5699, ((BgL_typez00_bglt) BgL_ftype1363z00_5700),
				((BgL_typez00_bglt) BgL_otype1364z00_5701),
				((BgL_approxz00_bglt) BgL_approx1365z00_5702));
		}

	}



/* valloc/Cinfo? */
	BGL_EXPORTED_DEF bool_t BGl_valloczf2Cinfozf3z01zzcfa_info3z00(obj_t
		BgL_objz00_435)
	{
		{	/* Cfa/cinfo3.sch 571 */
			{	/* Cfa/cinfo3.sch 571 */
				obj_t BgL_classz00_6494;

				BgL_classz00_6494 = BGl_valloczf2Cinfozf2zzcfa_info3z00;
				if (BGL_OBJECTP(BgL_objz00_435))
					{	/* Cfa/cinfo3.sch 571 */
						BgL_objectz00_bglt BgL_arg1809z00_6495;
						long BgL_arg1810z00_6496;

						BgL_arg1809z00_6495 = (BgL_objectz00_bglt) (BgL_objz00_435);
						BgL_arg1810z00_6496 = BGL_CLASS_DEPTH(BgL_classz00_6494);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo3.sch 571 */
								long BgL_idxz00_6497;

								BgL_idxz00_6497 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1809z00_6495);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6497 + BgL_arg1810z00_6496)) ==
									BgL_classz00_6494);
							}
						else
							{	/* Cfa/cinfo3.sch 571 */
								bool_t BgL_res2265z00_6500;

								{	/* Cfa/cinfo3.sch 571 */
									obj_t BgL_oclassz00_6501;

									{	/* Cfa/cinfo3.sch 571 */
										obj_t BgL_arg1815z00_6502;
										long BgL_arg1816z00_6503;

										BgL_arg1815z00_6502 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo3.sch 571 */
											long BgL_arg1817z00_6504;

											BgL_arg1817z00_6504 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1809z00_6495);
											BgL_arg1816z00_6503 = (BgL_arg1817z00_6504 - OBJECT_TYPE);
										}
										BgL_oclassz00_6501 =
											VECTOR_REF(BgL_arg1815z00_6502, BgL_arg1816z00_6503);
									}
									{	/* Cfa/cinfo3.sch 571 */
										bool_t BgL__ortest_1115z00_6505;

										BgL__ortest_1115z00_6505 =
											(BgL_classz00_6494 == BgL_oclassz00_6501);
										if (BgL__ortest_1115z00_6505)
											{	/* Cfa/cinfo3.sch 571 */
												BgL_res2265z00_6500 = BgL__ortest_1115z00_6505;
											}
										else
											{	/* Cfa/cinfo3.sch 571 */
												long BgL_odepthz00_6506;

												{	/* Cfa/cinfo3.sch 571 */
													obj_t BgL_arg1804z00_6507;

													BgL_arg1804z00_6507 = (BgL_oclassz00_6501);
													BgL_odepthz00_6506 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6507);
												}
												if ((BgL_arg1810z00_6496 < BgL_odepthz00_6506))
													{	/* Cfa/cinfo3.sch 571 */
														obj_t BgL_arg1802z00_6508;

														{	/* Cfa/cinfo3.sch 571 */
															obj_t BgL_arg1803z00_6509;

															BgL_arg1803z00_6509 = (BgL_oclassz00_6501);
															BgL_arg1802z00_6508 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6509,
																BgL_arg1810z00_6496);
														}
														BgL_res2265z00_6500 =
															(BgL_arg1802z00_6508 == BgL_classz00_6494);
													}
												else
													{	/* Cfa/cinfo3.sch 571 */
														BgL_res2265z00_6500 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2265z00_6500;
							}
					}
				else
					{	/* Cfa/cinfo3.sch 571 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &valloc/Cinfo? */
	obj_t BGl_z62valloczf2Cinfozf3z63zzcfa_info3z00(obj_t BgL_envz00_5703,
		obj_t BgL_objz00_5704)
	{
		{	/* Cfa/cinfo3.sch 571 */
			return BBOOL(BGl_valloczf2Cinfozf3z01zzcfa_info3z00(BgL_objz00_5704));
		}

	}



/* valloc/Cinfo-nil */
	BGL_EXPORTED_DEF BgL_vallocz00_bglt
		BGl_valloczf2Cinfozd2nilz20zzcfa_info3z00(void)
	{
		{	/* Cfa/cinfo3.sch 572 */
			{	/* Cfa/cinfo3.sch 572 */
				obj_t BgL_classz00_4568;

				BgL_classz00_4568 = BGl_valloczf2Cinfozf2zzcfa_info3z00;
				{	/* Cfa/cinfo3.sch 572 */
					obj_t BgL__ortest_1117z00_4569;

					BgL__ortest_1117z00_4569 = BGL_CLASS_NIL(BgL_classz00_4568);
					if (CBOOL(BgL__ortest_1117z00_4569))
						{	/* Cfa/cinfo3.sch 572 */
							return ((BgL_vallocz00_bglt) BgL__ortest_1117z00_4569);
						}
					else
						{	/* Cfa/cinfo3.sch 572 */
							return
								((BgL_vallocz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4568));
						}
				}
			}
		}

	}



/* &valloc/Cinfo-nil */
	BgL_vallocz00_bglt BGl_z62valloczf2Cinfozd2nilz42zzcfa_info3z00(obj_t
		BgL_envz00_5705)
	{
		{	/* Cfa/cinfo3.sch 572 */
			return BGl_valloczf2Cinfozd2nilz20zzcfa_info3z00();
		}

	}



/* valloc/Cinfo-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_valloczf2Cinfozd2approxz20zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_436)
	{
		{	/* Cfa/cinfo3.sch 573 */
			{
				BgL_valloczf2cinfozf2_bglt BgL_auxz00_8732;

				{
					obj_t BgL_auxz00_8733;

					{	/* Cfa/cinfo3.sch 573 */
						BgL_objectz00_bglt BgL_tmpz00_8734;

						BgL_tmpz00_8734 = ((BgL_objectz00_bglt) BgL_oz00_436);
						BgL_auxz00_8733 = BGL_OBJECT_WIDENING(BgL_tmpz00_8734);
					}
					BgL_auxz00_8732 = ((BgL_valloczf2cinfozf2_bglt) BgL_auxz00_8733);
				}
				return
					(((BgL_valloczf2cinfozf2_bglt) COBJECT(BgL_auxz00_8732))->
					BgL_approxz00);
			}
		}

	}



/* &valloc/Cinfo-approx */
	BgL_approxz00_bglt BGl_z62valloczf2Cinfozd2approxz42zzcfa_info3z00(obj_t
		BgL_envz00_5706, obj_t BgL_oz00_5707)
	{
		{	/* Cfa/cinfo3.sch 573 */
			return
				BGl_valloczf2Cinfozd2approxz20zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5707));
		}

	}



/* valloc/Cinfo-otype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_valloczf2Cinfozd2otypez20zzcfa_info3z00(BgL_vallocz00_bglt BgL_oz00_439)
	{
		{	/* Cfa/cinfo3.sch 575 */
			return
				(((BgL_vallocz00_bglt) COBJECT(
						((BgL_vallocz00_bglt) BgL_oz00_439)))->BgL_otypez00);
		}

	}



/* &valloc/Cinfo-otype */
	BgL_typez00_bglt BGl_z62valloczf2Cinfozd2otypez42zzcfa_info3z00(obj_t
		BgL_envz00_5708, obj_t BgL_oz00_5709)
	{
		{	/* Cfa/cinfo3.sch 575 */
			return
				BGl_valloczf2Cinfozd2otypez20zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5709));
		}

	}



/* valloc/Cinfo-otype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozd2otypezd2setz12ze0zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_440, BgL_typez00_bglt BgL_vz00_441)
	{
		{	/* Cfa/cinfo3.sch 576 */
			return
				((((BgL_vallocz00_bglt) COBJECT(
							((BgL_vallocz00_bglt) BgL_oz00_440)))->BgL_otypez00) =
				((BgL_typez00_bglt) BgL_vz00_441), BUNSPEC);
		}

	}



/* &valloc/Cinfo-otype-set! */
	obj_t BGl_z62valloczf2Cinfozd2otypezd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5710, obj_t BgL_oz00_5711, obj_t BgL_vz00_5712)
	{
		{	/* Cfa/cinfo3.sch 576 */
			return
				BGl_valloczf2Cinfozd2otypezd2setz12ze0zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5711),
				((BgL_typez00_bglt) BgL_vz00_5712));
		}

	}



/* valloc/Cinfo-ftype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_valloczf2Cinfozd2ftypez20zzcfa_info3z00(BgL_vallocz00_bglt BgL_oz00_442)
	{
		{	/* Cfa/cinfo3.sch 577 */
			return
				(((BgL_vallocz00_bglt) COBJECT(
						((BgL_vallocz00_bglt) BgL_oz00_442)))->BgL_ftypez00);
		}

	}



/* &valloc/Cinfo-ftype */
	BgL_typez00_bglt BGl_z62valloczf2Cinfozd2ftypez42zzcfa_info3z00(obj_t
		BgL_envz00_5713, obj_t BgL_oz00_5714)
	{
		{	/* Cfa/cinfo3.sch 577 */
			return
				BGl_valloczf2Cinfozd2ftypez20zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5714));
		}

	}



/* valloc/Cinfo-ftype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozd2ftypezd2setz12ze0zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_443, BgL_typez00_bglt BgL_vz00_444)
	{
		{	/* Cfa/cinfo3.sch 578 */
			return
				((((BgL_vallocz00_bglt) COBJECT(
							((BgL_vallocz00_bglt) BgL_oz00_443)))->BgL_ftypez00) =
				((BgL_typez00_bglt) BgL_vz00_444), BUNSPEC);
		}

	}



/* &valloc/Cinfo-ftype-set! */
	obj_t BGl_z62valloczf2Cinfozd2ftypezd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5715, obj_t BgL_oz00_5716, obj_t BgL_vz00_5717)
	{
		{	/* Cfa/cinfo3.sch 578 */
			return
				BGl_valloczf2Cinfozd2ftypezd2setz12ze0zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5716),
				((BgL_typez00_bglt) BgL_vz00_5717));
		}

	}



/* valloc/Cinfo-c-format */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozd2czd2formatzf2zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_445)
	{
		{	/* Cfa/cinfo3.sch 579 */
			return
				(((BgL_privatez00_bglt) COBJECT(
						((BgL_privatez00_bglt) BgL_oz00_445)))->BgL_czd2formatzd2);
		}

	}



/* &valloc/Cinfo-c-format */
	obj_t BGl_z62valloczf2Cinfozd2czd2formatz90zzcfa_info3z00(obj_t
		BgL_envz00_5718, obj_t BgL_oz00_5719)
	{
		{	/* Cfa/cinfo3.sch 579 */
			return
				BGl_valloczf2Cinfozd2czd2formatzf2zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5719));
		}

	}



/* valloc/Cinfo-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozd2effectz20zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_448)
	{
		{	/* Cfa/cinfo3.sch 581 */
			return
				(((BgL_externz00_bglt) COBJECT(
						((BgL_externz00_bglt) BgL_oz00_448)))->BgL_effectz00);
		}

	}



/* &valloc/Cinfo-effect */
	obj_t BGl_z62valloczf2Cinfozd2effectz42zzcfa_info3z00(obj_t BgL_envz00_5720,
		obj_t BgL_oz00_5721)
	{
		{	/* Cfa/cinfo3.sch 581 */
			return
				BGl_valloczf2Cinfozd2effectz20zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5721));
		}

	}



/* valloc/Cinfo-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozd2effectzd2setz12ze0zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_449, obj_t BgL_vz00_450)
	{
		{	/* Cfa/cinfo3.sch 582 */
			return
				((((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_oz00_449)))->BgL_effectz00) =
				((obj_t) BgL_vz00_450), BUNSPEC);
		}

	}



/* &valloc/Cinfo-effect-set! */
	obj_t BGl_z62valloczf2Cinfozd2effectzd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5722, obj_t BgL_oz00_5723, obj_t BgL_vz00_5724)
	{
		{	/* Cfa/cinfo3.sch 582 */
			return
				BGl_valloczf2Cinfozd2effectzd2setz12ze0zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5723), BgL_vz00_5724);
		}

	}



/* valloc/Cinfo-expr* */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozd2exprza2z82zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_451)
	{
		{	/* Cfa/cinfo3.sch 583 */
			return
				(((BgL_externz00_bglt) COBJECT(
						((BgL_externz00_bglt) BgL_oz00_451)))->BgL_exprza2za2);
		}

	}



/* &valloc/Cinfo-expr* */
	obj_t BGl_z62valloczf2Cinfozd2exprza2ze0zzcfa_info3z00(obj_t BgL_envz00_5725,
		obj_t BgL_oz00_5726)
	{
		{	/* Cfa/cinfo3.sch 583 */
			return
				BGl_valloczf2Cinfozd2exprza2z82zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5726));
		}

	}



/* valloc/Cinfo-expr*-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozd2exprza2zd2setz12z42zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_452, obj_t BgL_vz00_453)
	{
		{	/* Cfa/cinfo3.sch 584 */
			return
				((((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_oz00_452)))->BgL_exprza2za2) =
				((obj_t) BgL_vz00_453), BUNSPEC);
		}

	}



/* &valloc/Cinfo-expr*-set! */
	obj_t BGl_z62valloczf2Cinfozd2exprza2zd2setz12z20zzcfa_info3z00(obj_t
		BgL_envz00_5727, obj_t BgL_oz00_5728, obj_t BgL_vz00_5729)
	{
		{	/* Cfa/cinfo3.sch 584 */
			return
				BGl_valloczf2Cinfozd2exprza2zd2setz12z42zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5728), BgL_vz00_5729);
		}

	}



/* valloc/Cinfo-key */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozd2keyz20zzcfa_info3z00(BgL_vallocz00_bglt BgL_oz00_454)
	{
		{	/* Cfa/cinfo3.sch 585 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_454)))->BgL_keyz00);
		}

	}



/* &valloc/Cinfo-key */
	obj_t BGl_z62valloczf2Cinfozd2keyz42zzcfa_info3z00(obj_t BgL_envz00_5730,
		obj_t BgL_oz00_5731)
	{
		{	/* Cfa/cinfo3.sch 585 */
			return
				BGl_valloczf2Cinfozd2keyz20zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5731));
		}

	}



/* valloc/Cinfo-key-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozd2keyzd2setz12ze0zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_455, obj_t BgL_vz00_456)
	{
		{	/* Cfa/cinfo3.sch 586 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_455)))->BgL_keyz00) =
				((obj_t) BgL_vz00_456), BUNSPEC);
		}

	}



/* &valloc/Cinfo-key-set! */
	obj_t BGl_z62valloczf2Cinfozd2keyzd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5732, obj_t BgL_oz00_5733, obj_t BgL_vz00_5734)
	{
		{	/* Cfa/cinfo3.sch 586 */
			return
				BGl_valloczf2Cinfozd2keyzd2setz12ze0zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5733), BgL_vz00_5734);
		}

	}



/* valloc/Cinfo-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozd2sidezd2effectzf2zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_457)
	{
		{	/* Cfa/cinfo3.sch 587 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_457)))->BgL_sidezd2effectzd2);
		}

	}



/* &valloc/Cinfo-side-effect */
	obj_t BGl_z62valloczf2Cinfozd2sidezd2effectz90zzcfa_info3z00(obj_t
		BgL_envz00_5735, obj_t BgL_oz00_5736)
	{
		{	/* Cfa/cinfo3.sch 587 */
			return
				BGl_valloczf2Cinfozd2sidezd2effectzf2zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5736));
		}

	}



/* valloc/Cinfo-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_info3z00
		(BgL_vallocz00_bglt BgL_oz00_458, obj_t BgL_vz00_459)
	{
		{	/* Cfa/cinfo3.sch 588 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_458)))->
					BgL_sidezd2effectzd2) = ((obj_t) BgL_vz00_459), BUNSPEC);
		}

	}



/* &valloc/Cinfo-side-effect-set! */
	obj_t BGl_z62valloczf2Cinfozd2sidezd2effectzd2setz12z50zzcfa_info3z00(obj_t
		BgL_envz00_5737, obj_t BgL_oz00_5738, obj_t BgL_vz00_5739)
	{
		{	/* Cfa/cinfo3.sch 588 */
			return
				BGl_valloczf2Cinfozd2sidezd2effectzd2setz12z32zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5738), BgL_vz00_5739);
		}

	}



/* valloc/Cinfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_valloczf2Cinfozd2typez20zzcfa_info3z00(BgL_vallocz00_bglt BgL_oz00_460)
	{
		{	/* Cfa/cinfo3.sch 589 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_460)))->BgL_typez00);
		}

	}



/* &valloc/Cinfo-type */
	BgL_typez00_bglt BGl_z62valloczf2Cinfozd2typez42zzcfa_info3z00(obj_t
		BgL_envz00_5740, obj_t BgL_oz00_5741)
	{
		{	/* Cfa/cinfo3.sch 589 */
			return
				BGl_valloczf2Cinfozd2typez20zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5741));
		}

	}



/* valloc/Cinfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozd2typezd2setz12ze0zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_461, BgL_typez00_bglt BgL_vz00_462)
	{
		{	/* Cfa/cinfo3.sch 590 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_461)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_462), BUNSPEC);
		}

	}



/* &valloc/Cinfo-type-set! */
	obj_t BGl_z62valloczf2Cinfozd2typezd2setz12z82zzcfa_info3z00(obj_t
		BgL_envz00_5742, obj_t BgL_oz00_5743, obj_t BgL_vz00_5744)
	{
		{	/* Cfa/cinfo3.sch 590 */
			return
				BGl_valloczf2Cinfozd2typezd2setz12ze0zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5743),
				((BgL_typez00_bglt) BgL_vz00_5744));
		}

	}



/* valloc/Cinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozd2locz20zzcfa_info3z00(BgL_vallocz00_bglt BgL_oz00_463)
	{
		{	/* Cfa/cinfo3.sch 591 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_463)))->BgL_locz00);
		}

	}



/* &valloc/Cinfo-loc */
	obj_t BGl_z62valloczf2Cinfozd2locz42zzcfa_info3z00(obj_t BgL_envz00_5745,
		obj_t BgL_oz00_5746)
	{
		{	/* Cfa/cinfo3.sch 591 */
			return
				BGl_valloczf2Cinfozd2locz20zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5746));
		}

	}



/* make-valloc/Cinfo+optim */
	BGL_EXPORTED_DEF BgL_vallocz00_bglt
		BGl_makezd2valloczf2Cinfozb2optimz92zzcfa_info3z00(obj_t BgL_loc1339z00_466,
		BgL_typez00_bglt BgL_type1340z00_467, obj_t BgL_sidezd2effect1341zd2_468,
		obj_t BgL_key1342z00_469, obj_t BgL_exprza21343za2_470,
		obj_t BgL_effect1344z00_471, obj_t BgL_czd2format1345zd2_472,
		BgL_typez00_bglt BgL_ftype1346z00_473,
		BgL_typez00_bglt BgL_otype1347z00_474,
		BgL_approxz00_bglt BgL_approx1348z00_475,
		BgL_approxz00_bglt BgL_valuezd2approx1349zd2_476,
		long BgL_lostzd2stamp1350zd2_477, BgL_variablez00_bglt BgL_owner1351z00_478,
		bool_t BgL_stackablezf31352zf3_479, obj_t BgL_stackzd2stamp1353zd2_480,
		bool_t BgL_seenzf31354zf3_481)
	{
		{	/* Cfa/cinfo3.sch 595 */
			{	/* Cfa/cinfo3.sch 595 */
				BgL_vallocz00_bglt BgL_new1385z00_6510;

				{	/* Cfa/cinfo3.sch 595 */
					BgL_vallocz00_bglt BgL_tmp1383z00_6511;
					BgL_valloczf2cinfozb2optimz40_bglt BgL_wide1384z00_6512;

					{
						BgL_vallocz00_bglt BgL_auxz00_8808;

						{	/* Cfa/cinfo3.sch 595 */
							BgL_vallocz00_bglt BgL_new1382z00_6513;

							BgL_new1382z00_6513 =
								((BgL_vallocz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_vallocz00_bgl))));
							{	/* Cfa/cinfo3.sch 595 */
								long BgL_arg1749z00_6514;

								BgL_arg1749z00_6514 = BGL_CLASS_NUM(BGl_vallocz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1382z00_6513),
									BgL_arg1749z00_6514);
							}
							{	/* Cfa/cinfo3.sch 595 */
								BgL_objectz00_bglt BgL_tmpz00_8813;

								BgL_tmpz00_8813 = ((BgL_objectz00_bglt) BgL_new1382z00_6513);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8813, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1382z00_6513);
							BgL_auxz00_8808 = BgL_new1382z00_6513;
						}
						BgL_tmp1383z00_6511 = ((BgL_vallocz00_bglt) BgL_auxz00_8808);
					}
					BgL_wide1384z00_6512 =
						((BgL_valloczf2cinfozb2optimz40_bglt)
						BOBJECT(GC_MALLOC(sizeof(struct
									BgL_valloczf2cinfozb2optimz40_bgl))));
					{	/* Cfa/cinfo3.sch 595 */
						obj_t BgL_auxz00_8821;
						BgL_objectz00_bglt BgL_tmpz00_8819;

						BgL_auxz00_8821 = ((obj_t) BgL_wide1384z00_6512);
						BgL_tmpz00_8819 = ((BgL_objectz00_bglt) BgL_tmp1383z00_6511);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8819, BgL_auxz00_8821);
					}
					((BgL_objectz00_bglt) BgL_tmp1383z00_6511);
					{	/* Cfa/cinfo3.sch 595 */
						long BgL_arg1748z00_6515;

						BgL_arg1748z00_6515 =
							BGL_CLASS_NUM(BGl_valloczf2Cinfozb2optimz40zzcfa_info3z00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1383z00_6511), BgL_arg1748z00_6515);
					}
					BgL_new1385z00_6510 = ((BgL_vallocz00_bglt) BgL_tmp1383z00_6511);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1385z00_6510)))->BgL_locz00) =
					((obj_t) BgL_loc1339z00_466), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1385z00_6510)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1340z00_467), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1385z00_6510)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1341zd2_468), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1385z00_6510)))->BgL_keyz00) =
					((obj_t) BgL_key1342z00_469), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1385z00_6510)))->BgL_exprza2za2) =
					((obj_t) BgL_exprza21343za2_470), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1385z00_6510)))->BgL_effectz00) =
					((obj_t) BgL_effect1344z00_471), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1385z00_6510)))->BgL_czd2formatzd2) =
					((obj_t) BgL_czd2format1345zd2_472), BUNSPEC);
				((((BgL_vallocz00_bglt) COBJECT(((BgL_vallocz00_bglt)
									BgL_new1385z00_6510)))->BgL_ftypez00) =
					((BgL_typez00_bglt) BgL_ftype1346z00_473), BUNSPEC);
				((((BgL_vallocz00_bglt) COBJECT(((BgL_vallocz00_bglt)
									BgL_new1385z00_6510)))->BgL_otypez00) =
					((BgL_typez00_bglt) BgL_otype1347z00_474), BUNSPEC);
				{
					BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_8847;

					{
						obj_t BgL_auxz00_8848;

						{	/* Cfa/cinfo3.sch 595 */
							BgL_objectz00_bglt BgL_tmpz00_8849;

							BgL_tmpz00_8849 = ((BgL_objectz00_bglt) BgL_new1385z00_6510);
							BgL_auxz00_8848 = BGL_OBJECT_WIDENING(BgL_tmpz00_8849);
						}
						BgL_auxz00_8847 =
							((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_8848);
					}
					((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_8847))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_approx1348z00_475), BUNSPEC);
				}
				{
					BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_8854;

					{
						obj_t BgL_auxz00_8855;

						{	/* Cfa/cinfo3.sch 595 */
							BgL_objectz00_bglt BgL_tmpz00_8856;

							BgL_tmpz00_8856 = ((BgL_objectz00_bglt) BgL_new1385z00_6510);
							BgL_auxz00_8855 = BGL_OBJECT_WIDENING(BgL_tmpz00_8856);
						}
						BgL_auxz00_8854 =
							((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_8855);
					}
					((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_8854))->
							BgL_valuezd2approxzd2) =
						((BgL_approxz00_bglt) BgL_valuezd2approx1349zd2_476), BUNSPEC);
				}
				{
					BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_8861;

					{
						obj_t BgL_auxz00_8862;

						{	/* Cfa/cinfo3.sch 595 */
							BgL_objectz00_bglt BgL_tmpz00_8863;

							BgL_tmpz00_8863 = ((BgL_objectz00_bglt) BgL_new1385z00_6510);
							BgL_auxz00_8862 = BGL_OBJECT_WIDENING(BgL_tmpz00_8863);
						}
						BgL_auxz00_8861 =
							((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_8862);
					}
					((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_8861))->
							BgL_lostzd2stampzd2) =
						((long) BgL_lostzd2stamp1350zd2_477), BUNSPEC);
				}
				{
					BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_8868;

					{
						obj_t BgL_auxz00_8869;

						{	/* Cfa/cinfo3.sch 595 */
							BgL_objectz00_bglt BgL_tmpz00_8870;

							BgL_tmpz00_8870 = ((BgL_objectz00_bglt) BgL_new1385z00_6510);
							BgL_auxz00_8869 = BGL_OBJECT_WIDENING(BgL_tmpz00_8870);
						}
						BgL_auxz00_8868 =
							((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_8869);
					}
					((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_8868))->
							BgL_ownerz00) =
						((BgL_variablez00_bglt) BgL_owner1351z00_478), BUNSPEC);
				}
				{
					BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_8875;

					{
						obj_t BgL_auxz00_8876;

						{	/* Cfa/cinfo3.sch 595 */
							BgL_objectz00_bglt BgL_tmpz00_8877;

							BgL_tmpz00_8877 = ((BgL_objectz00_bglt) BgL_new1385z00_6510);
							BgL_auxz00_8876 = BGL_OBJECT_WIDENING(BgL_tmpz00_8877);
						}
						BgL_auxz00_8875 =
							((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_8876);
					}
					((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_8875))->
							BgL_stackablezf3zf3) =
						((bool_t) BgL_stackablezf31352zf3_479), BUNSPEC);
				}
				{
					BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_8882;

					{
						obj_t BgL_auxz00_8883;

						{	/* Cfa/cinfo3.sch 595 */
							BgL_objectz00_bglt BgL_tmpz00_8884;

							BgL_tmpz00_8884 = ((BgL_objectz00_bglt) BgL_new1385z00_6510);
							BgL_auxz00_8883 = BGL_OBJECT_WIDENING(BgL_tmpz00_8884);
						}
						BgL_auxz00_8882 =
							((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_8883);
					}
					((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_8882))->
							BgL_stackzd2stampzd2) =
						((obj_t) BgL_stackzd2stamp1353zd2_480), BUNSPEC);
				}
				{
					BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_8889;

					{
						obj_t BgL_auxz00_8890;

						{	/* Cfa/cinfo3.sch 595 */
							BgL_objectz00_bglt BgL_tmpz00_8891;

							BgL_tmpz00_8891 = ((BgL_objectz00_bglt) BgL_new1385z00_6510);
							BgL_auxz00_8890 = BGL_OBJECT_WIDENING(BgL_tmpz00_8891);
						}
						BgL_auxz00_8889 =
							((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_8890);
					}
					((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_8889))->
							BgL_seenzf3zf3) = ((bool_t) BgL_seenzf31354zf3_481), BUNSPEC);
				}
				return BgL_new1385z00_6510;
			}
		}

	}



/* &make-valloc/Cinfo+optim */
	BgL_vallocz00_bglt BGl_z62makezd2valloczf2Cinfozb2optimzf0zzcfa_info3z00(obj_t
		BgL_envz00_5747, obj_t BgL_loc1339z00_5748, obj_t BgL_type1340z00_5749,
		obj_t BgL_sidezd2effect1341zd2_5750, obj_t BgL_key1342z00_5751,
		obj_t BgL_exprza21343za2_5752, obj_t BgL_effect1344z00_5753,
		obj_t BgL_czd2format1345zd2_5754, obj_t BgL_ftype1346z00_5755,
		obj_t BgL_otype1347z00_5756, obj_t BgL_approx1348z00_5757,
		obj_t BgL_valuezd2approx1349zd2_5758, obj_t BgL_lostzd2stamp1350zd2_5759,
		obj_t BgL_owner1351z00_5760, obj_t BgL_stackablezf31352zf3_5761,
		obj_t BgL_stackzd2stamp1353zd2_5762, obj_t BgL_seenzf31354zf3_5763)
	{
		{	/* Cfa/cinfo3.sch 595 */
			return
				BGl_makezd2valloczf2Cinfozb2optimz92zzcfa_info3z00(BgL_loc1339z00_5748,
				((BgL_typez00_bglt) BgL_type1340z00_5749),
				BgL_sidezd2effect1341zd2_5750, BgL_key1342z00_5751,
				BgL_exprza21343za2_5752, BgL_effect1344z00_5753,
				BgL_czd2format1345zd2_5754, ((BgL_typez00_bglt) BgL_ftype1346z00_5755),
				((BgL_typez00_bglt) BgL_otype1347z00_5756),
				((BgL_approxz00_bglt) BgL_approx1348z00_5757),
				((BgL_approxz00_bglt) BgL_valuezd2approx1349zd2_5758),
				(long) CINT(BgL_lostzd2stamp1350zd2_5759),
				((BgL_variablez00_bglt) BgL_owner1351z00_5760),
				CBOOL(BgL_stackablezf31352zf3_5761), BgL_stackzd2stamp1353zd2_5762,
				CBOOL(BgL_seenzf31354zf3_5763));
		}

	}



/* valloc/Cinfo+optim? */
	BGL_EXPORTED_DEF bool_t BGl_valloczf2Cinfozb2optimzf3zb3zzcfa_info3z00(obj_t
		BgL_objz00_482)
	{
		{	/* Cfa/cinfo3.sch 596 */
			{	/* Cfa/cinfo3.sch 596 */
				obj_t BgL_classz00_6516;

				BgL_classz00_6516 = BGl_valloczf2Cinfozb2optimz40zzcfa_info3z00;
				if (BGL_OBJECTP(BgL_objz00_482))
					{	/* Cfa/cinfo3.sch 596 */
						BgL_objectz00_bglt BgL_arg1809z00_6517;
						long BgL_arg1810z00_6518;

						BgL_arg1809z00_6517 = (BgL_objectz00_bglt) (BgL_objz00_482);
						BgL_arg1810z00_6518 = BGL_CLASS_DEPTH(BgL_classz00_6516);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cfa/cinfo3.sch 596 */
								long BgL_idxz00_6519;

								BgL_idxz00_6519 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1809z00_6517);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6519 + BgL_arg1810z00_6518)) ==
									BgL_classz00_6516);
							}
						else
							{	/* Cfa/cinfo3.sch 596 */
								bool_t BgL_res2266z00_6522;

								{	/* Cfa/cinfo3.sch 596 */
									obj_t BgL_oclassz00_6523;

									{	/* Cfa/cinfo3.sch 596 */
										obj_t BgL_arg1815z00_6524;
										long BgL_arg1816z00_6525;

										BgL_arg1815z00_6524 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cfa/cinfo3.sch 596 */
											long BgL_arg1817z00_6526;

											BgL_arg1817z00_6526 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1809z00_6517);
											BgL_arg1816z00_6525 = (BgL_arg1817z00_6526 - OBJECT_TYPE);
										}
										BgL_oclassz00_6523 =
											VECTOR_REF(BgL_arg1815z00_6524, BgL_arg1816z00_6525);
									}
									{	/* Cfa/cinfo3.sch 596 */
										bool_t BgL__ortest_1115z00_6527;

										BgL__ortest_1115z00_6527 =
											(BgL_classz00_6516 == BgL_oclassz00_6523);
										if (BgL__ortest_1115z00_6527)
											{	/* Cfa/cinfo3.sch 596 */
												BgL_res2266z00_6522 = BgL__ortest_1115z00_6527;
											}
										else
											{	/* Cfa/cinfo3.sch 596 */
												long BgL_odepthz00_6528;

												{	/* Cfa/cinfo3.sch 596 */
													obj_t BgL_arg1804z00_6529;

													BgL_arg1804z00_6529 = (BgL_oclassz00_6523);
													BgL_odepthz00_6528 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6529);
												}
												if ((BgL_arg1810z00_6518 < BgL_odepthz00_6528))
													{	/* Cfa/cinfo3.sch 596 */
														obj_t BgL_arg1802z00_6530;

														{	/* Cfa/cinfo3.sch 596 */
															obj_t BgL_arg1803z00_6531;

															BgL_arg1803z00_6531 = (BgL_oclassz00_6523);
															BgL_arg1802z00_6530 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6531,
																BgL_arg1810z00_6518);
														}
														BgL_res2266z00_6522 =
															(BgL_arg1802z00_6530 == BgL_classz00_6516);
													}
												else
													{	/* Cfa/cinfo3.sch 596 */
														BgL_res2266z00_6522 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2266z00_6522;
							}
					}
				else
					{	/* Cfa/cinfo3.sch 596 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &valloc/Cinfo+optim? */
	obj_t BGl_z62valloczf2Cinfozb2optimzf3zd1zzcfa_info3z00(obj_t BgL_envz00_5764,
		obj_t BgL_objz00_5765)
	{
		{	/* Cfa/cinfo3.sch 596 */
			return
				BBOOL(BGl_valloczf2Cinfozb2optimzf3zb3zzcfa_info3z00(BgL_objz00_5765));
		}

	}



/* valloc/Cinfo+optim-nil */
	BGL_EXPORTED_DEF BgL_vallocz00_bglt
		BGl_valloczf2Cinfozb2optimzd2nilz92zzcfa_info3z00(void)
	{
		{	/* Cfa/cinfo3.sch 597 */
			{	/* Cfa/cinfo3.sch 597 */
				obj_t BgL_classz00_4632;

				BgL_classz00_4632 = BGl_valloczf2Cinfozb2optimz40zzcfa_info3z00;
				{	/* Cfa/cinfo3.sch 597 */
					obj_t BgL__ortest_1117z00_4633;

					BgL__ortest_1117z00_4633 = BGL_CLASS_NIL(BgL_classz00_4632);
					if (CBOOL(BgL__ortest_1117z00_4633))
						{	/* Cfa/cinfo3.sch 597 */
							return ((BgL_vallocz00_bglt) BgL__ortest_1117z00_4633);
						}
					else
						{	/* Cfa/cinfo3.sch 597 */
							return
								((BgL_vallocz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4632));
						}
				}
			}
		}

	}



/* &valloc/Cinfo+optim-nil */
	BgL_vallocz00_bglt BGl_z62valloczf2Cinfozb2optimzd2nilzf0zzcfa_info3z00(obj_t
		BgL_envz00_5766)
	{
		{	/* Cfa/cinfo3.sch 597 */
			return BGl_valloczf2Cinfozb2optimzd2nilz92zzcfa_info3z00();
		}

	}



/* valloc/Cinfo+optim-seen? */
	BGL_EXPORTED_DEF bool_t
		BGl_valloczf2Cinfozb2optimzd2seenzf3z61zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_483)
	{
		{	/* Cfa/cinfo3.sch 598 */
			{
				BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_8938;

				{
					obj_t BgL_auxz00_8939;

					{	/* Cfa/cinfo3.sch 598 */
						BgL_objectz00_bglt BgL_tmpz00_8940;

						BgL_tmpz00_8940 = ((BgL_objectz00_bglt) BgL_oz00_483);
						BgL_auxz00_8939 = BGL_OBJECT_WIDENING(BgL_tmpz00_8940);
					}
					BgL_auxz00_8938 =
						((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_8939);
				}
				return
					(((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_8938))->
					BgL_seenzf3zf3);
			}
		}

	}



/* &valloc/Cinfo+optim-seen? */
	obj_t BGl_z62valloczf2Cinfozb2optimzd2seenzf3z03zzcfa_info3z00(obj_t
		BgL_envz00_5767, obj_t BgL_oz00_5768)
	{
		{	/* Cfa/cinfo3.sch 598 */
			return
				BBOOL(BGl_valloczf2Cinfozb2optimzd2seenzf3z61zzcfa_info3z00(
					((BgL_vallocz00_bglt) BgL_oz00_5768)));
		}

	}



/* valloc/Cinfo+optim-seen?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozb2optimzd2seenzf3zd2setz12za1zzcfa_info3z00
		(BgL_vallocz00_bglt BgL_oz00_484, bool_t BgL_vz00_485)
	{
		{	/* Cfa/cinfo3.sch 599 */
			{
				BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_8948;

				{
					obj_t BgL_auxz00_8949;

					{	/* Cfa/cinfo3.sch 599 */
						BgL_objectz00_bglt BgL_tmpz00_8950;

						BgL_tmpz00_8950 = ((BgL_objectz00_bglt) BgL_oz00_484);
						BgL_auxz00_8949 = BGL_OBJECT_WIDENING(BgL_tmpz00_8950);
					}
					BgL_auxz00_8948 =
						((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_8949);
				}
				return
					((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_8948))->
						BgL_seenzf3zf3) = ((bool_t) BgL_vz00_485), BUNSPEC);
			}
		}

	}



/* &valloc/Cinfo+optim-seen?-set! */
	obj_t BGl_z62valloczf2Cinfozb2optimzd2seenzf3zd2setz12zc3zzcfa_info3z00(obj_t
		BgL_envz00_5769, obj_t BgL_oz00_5770, obj_t BgL_vz00_5771)
	{
		{	/* Cfa/cinfo3.sch 599 */
			return
				BGl_valloczf2Cinfozb2optimzd2seenzf3zd2setz12za1zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5770), CBOOL(BgL_vz00_5771));
		}

	}



/* valloc/Cinfo+optim-stack-stamp */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozb2optimzd2stackzd2stampz40zzcfa_info3z00
		(BgL_vallocz00_bglt BgL_oz00_486)
	{
		{	/* Cfa/cinfo3.sch 600 */
			{
				BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_8958;

				{
					obj_t BgL_auxz00_8959;

					{	/* Cfa/cinfo3.sch 600 */
						BgL_objectz00_bglt BgL_tmpz00_8960;

						BgL_tmpz00_8960 = ((BgL_objectz00_bglt) BgL_oz00_486);
						BgL_auxz00_8959 = BGL_OBJECT_WIDENING(BgL_tmpz00_8960);
					}
					BgL_auxz00_8958 =
						((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_8959);
				}
				return
					(((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_8958))->
					BgL_stackzd2stampzd2);
			}
		}

	}



/* &valloc/Cinfo+optim-stack-stamp */
	obj_t BGl_z62valloczf2Cinfozb2optimzd2stackzd2stampz22zzcfa_info3z00(obj_t
		BgL_envz00_5772, obj_t BgL_oz00_5773)
	{
		{	/* Cfa/cinfo3.sch 600 */
			return
				BGl_valloczf2Cinfozb2optimzd2stackzd2stampz40zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5773));
		}

	}



/* valloc/Cinfo+optim-stack-stamp-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozb2optimzd2stackzd2stampzd2setz12z80zzcfa_info3z00
		(BgL_vallocz00_bglt BgL_oz00_487, obj_t BgL_vz00_488)
	{
		{	/* Cfa/cinfo3.sch 601 */
			{
				BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_8967;

				{
					obj_t BgL_auxz00_8968;

					{	/* Cfa/cinfo3.sch 601 */
						BgL_objectz00_bglt BgL_tmpz00_8969;

						BgL_tmpz00_8969 = ((BgL_objectz00_bglt) BgL_oz00_487);
						BgL_auxz00_8968 = BGL_OBJECT_WIDENING(BgL_tmpz00_8969);
					}
					BgL_auxz00_8967 =
						((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_8968);
				}
				return
					((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_8967))->
						BgL_stackzd2stampzd2) = ((obj_t) BgL_vz00_488), BUNSPEC);
			}
		}

	}



/* &valloc/Cinfo+optim-stack-stamp-set! */
	obj_t
		BGl_z62valloczf2Cinfozb2optimzd2stackzd2stampzd2setz12ze2zzcfa_info3z00
		(obj_t BgL_envz00_5774, obj_t BgL_oz00_5775, obj_t BgL_vz00_5776)
	{
		{	/* Cfa/cinfo3.sch 601 */
			return
				BGl_valloczf2Cinfozb2optimzd2stackzd2stampzd2setz12z80zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5775), BgL_vz00_5776);
		}

	}



/* valloc/Cinfo+optim-stackable? */
	BGL_EXPORTED_DEF bool_t
		BGl_valloczf2Cinfozb2optimzd2stackablezf3z61zzcfa_info3z00
		(BgL_vallocz00_bglt BgL_oz00_489)
	{
		{	/* Cfa/cinfo3.sch 602 */
			{
				BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_8976;

				{
					obj_t BgL_auxz00_8977;

					{	/* Cfa/cinfo3.sch 602 */
						BgL_objectz00_bglt BgL_tmpz00_8978;

						BgL_tmpz00_8978 = ((BgL_objectz00_bglt) BgL_oz00_489);
						BgL_auxz00_8977 = BGL_OBJECT_WIDENING(BgL_tmpz00_8978);
					}
					BgL_auxz00_8976 =
						((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_8977);
				}
				return
					(((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_8976))->
					BgL_stackablezf3zf3);
			}
		}

	}



/* &valloc/Cinfo+optim-stackable? */
	obj_t BGl_z62valloczf2Cinfozb2optimzd2stackablezf3z03zzcfa_info3z00(obj_t
		BgL_envz00_5777, obj_t BgL_oz00_5778)
	{
		{	/* Cfa/cinfo3.sch 602 */
			return
				BBOOL(BGl_valloczf2Cinfozb2optimzd2stackablezf3z61zzcfa_info3z00(
					((BgL_vallocz00_bglt) BgL_oz00_5778)));
		}

	}



/* valloc/Cinfo+optim-stackable?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozb2optimzd2stackablezf3zd2setz12za1zzcfa_info3z00
		(BgL_vallocz00_bglt BgL_oz00_490, bool_t BgL_vz00_491)
	{
		{	/* Cfa/cinfo3.sch 603 */
			{
				BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_8986;

				{
					obj_t BgL_auxz00_8987;

					{	/* Cfa/cinfo3.sch 603 */
						BgL_objectz00_bglt BgL_tmpz00_8988;

						BgL_tmpz00_8988 = ((BgL_objectz00_bglt) BgL_oz00_490);
						BgL_auxz00_8987 = BGL_OBJECT_WIDENING(BgL_tmpz00_8988);
					}
					BgL_auxz00_8986 =
						((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_8987);
				}
				return
					((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_8986))->
						BgL_stackablezf3zf3) = ((bool_t) BgL_vz00_491), BUNSPEC);
			}
		}

	}



/* &valloc/Cinfo+optim-stackable?-set! */
	obj_t
		BGl_z62valloczf2Cinfozb2optimzd2stackablezf3zd2setz12zc3zzcfa_info3z00(obj_t
		BgL_envz00_5779, obj_t BgL_oz00_5780, obj_t BgL_vz00_5781)
	{
		{	/* Cfa/cinfo3.sch 603 */
			return
				BGl_valloczf2Cinfozb2optimzd2stackablezf3zd2setz12za1zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5780), CBOOL(BgL_vz00_5781));
		}

	}



/* valloc/Cinfo+optim-owner */
	BGL_EXPORTED_DEF BgL_variablez00_bglt
		BGl_valloczf2Cinfozb2optimzd2ownerz92zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_492)
	{
		{	/* Cfa/cinfo3.sch 604 */
			{
				BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_8996;

				{
					obj_t BgL_auxz00_8997;

					{	/* Cfa/cinfo3.sch 604 */
						BgL_objectz00_bglt BgL_tmpz00_8998;

						BgL_tmpz00_8998 = ((BgL_objectz00_bglt) BgL_oz00_492);
						BgL_auxz00_8997 = BGL_OBJECT_WIDENING(BgL_tmpz00_8998);
					}
					BgL_auxz00_8996 =
						((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_8997);
				}
				return
					(((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_8996))->
					BgL_ownerz00);
			}
		}

	}



/* &valloc/Cinfo+optim-owner */
	BgL_variablez00_bglt
		BGl_z62valloczf2Cinfozb2optimzd2ownerzf0zzcfa_info3z00(obj_t
		BgL_envz00_5782, obj_t BgL_oz00_5783)
	{
		{	/* Cfa/cinfo3.sch 604 */
			return
				BGl_valloczf2Cinfozb2optimzd2ownerz92zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5783));
		}

	}



/* valloc/Cinfo+optim-lost-stamp */
	BGL_EXPORTED_DEF long
		BGl_valloczf2Cinfozb2optimzd2lostzd2stampz40zzcfa_info3z00
		(BgL_vallocz00_bglt BgL_oz00_495)
	{
		{	/* Cfa/cinfo3.sch 606 */
			{
				BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9005;

				{
					obj_t BgL_auxz00_9006;

					{	/* Cfa/cinfo3.sch 606 */
						BgL_objectz00_bglt BgL_tmpz00_9007;

						BgL_tmpz00_9007 = ((BgL_objectz00_bglt) BgL_oz00_495);
						BgL_auxz00_9006 = BGL_OBJECT_WIDENING(BgL_tmpz00_9007);
					}
					BgL_auxz00_9005 =
						((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9006);
				}
				return
					(((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9005))->
					BgL_lostzd2stampzd2);
			}
		}

	}



/* &valloc/Cinfo+optim-lost-stamp */
	obj_t BGl_z62valloczf2Cinfozb2optimzd2lostzd2stampz22zzcfa_info3z00(obj_t
		BgL_envz00_5784, obj_t BgL_oz00_5785)
	{
		{	/* Cfa/cinfo3.sch 606 */
			return
				BINT(BGl_valloczf2Cinfozb2optimzd2lostzd2stampz40zzcfa_info3z00(
					((BgL_vallocz00_bglt) BgL_oz00_5785)));
		}

	}



/* valloc/Cinfo+optim-lost-stamp-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozb2optimzd2lostzd2stampzd2setz12z80zzcfa_info3z00
		(BgL_vallocz00_bglt BgL_oz00_496, long BgL_vz00_497)
	{
		{	/* Cfa/cinfo3.sch 607 */
			{
				BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9015;

				{
					obj_t BgL_auxz00_9016;

					{	/* Cfa/cinfo3.sch 607 */
						BgL_objectz00_bglt BgL_tmpz00_9017;

						BgL_tmpz00_9017 = ((BgL_objectz00_bglt) BgL_oz00_496);
						BgL_auxz00_9016 = BGL_OBJECT_WIDENING(BgL_tmpz00_9017);
					}
					BgL_auxz00_9015 =
						((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9016);
				}
				return
					((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9015))->
						BgL_lostzd2stampzd2) = ((long) BgL_vz00_497), BUNSPEC);
		}}

	}



/* &valloc/Cinfo+optim-lost-stamp-set! */
	obj_t
		BGl_z62valloczf2Cinfozb2optimzd2lostzd2stampzd2setz12ze2zzcfa_info3z00(obj_t
		BgL_envz00_5786, obj_t BgL_oz00_5787, obj_t BgL_vz00_5788)
	{
		{	/* Cfa/cinfo3.sch 607 */
			return
				BGl_valloczf2Cinfozb2optimzd2lostzd2stampzd2setz12z80zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5787), (long) CINT(BgL_vz00_5788));
		}

	}



/* valloc/Cinfo+optim-value-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_valloczf2Cinfozb2optimzd2valuezd2approxz40zzcfa_info3z00
		(BgL_vallocz00_bglt BgL_oz00_498)
	{
		{	/* Cfa/cinfo3.sch 608 */
			{
				BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9025;

				{
					obj_t BgL_auxz00_9026;

					{	/* Cfa/cinfo3.sch 608 */
						BgL_objectz00_bglt BgL_tmpz00_9027;

						BgL_tmpz00_9027 = ((BgL_objectz00_bglt) BgL_oz00_498);
						BgL_auxz00_9026 = BGL_OBJECT_WIDENING(BgL_tmpz00_9027);
					}
					BgL_auxz00_9025 =
						((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9026);
				}
				return
					(((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9025))->
					BgL_valuezd2approxzd2);
			}
		}

	}



/* &valloc/Cinfo+optim-value-approx */
	BgL_approxz00_bglt
		BGl_z62valloczf2Cinfozb2optimzd2valuezd2approxz22zzcfa_info3z00(obj_t
		BgL_envz00_5789, obj_t BgL_oz00_5790)
	{
		{	/* Cfa/cinfo3.sch 608 */
			return
				BGl_valloczf2Cinfozb2optimzd2valuezd2approxz40zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5790));
		}

	}



/* valloc/Cinfo+optim-approx */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_valloczf2Cinfozb2optimzd2approxz92zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_501)
	{
		{	/* Cfa/cinfo3.sch 610 */
			{
				BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9034;

				{
					obj_t BgL_auxz00_9035;

					{	/* Cfa/cinfo3.sch 610 */
						BgL_objectz00_bglt BgL_tmpz00_9036;

						BgL_tmpz00_9036 = ((BgL_objectz00_bglt) BgL_oz00_501);
						BgL_auxz00_9035 = BGL_OBJECT_WIDENING(BgL_tmpz00_9036);
					}
					BgL_auxz00_9034 =
						((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9035);
				}
				return
					(((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9034))->
					BgL_approxz00);
			}
		}

	}



/* &valloc/Cinfo+optim-approx */
	BgL_approxz00_bglt
		BGl_z62valloczf2Cinfozb2optimzd2approxzf0zzcfa_info3z00(obj_t
		BgL_envz00_5791, obj_t BgL_oz00_5792)
	{
		{	/* Cfa/cinfo3.sch 610 */
			return
				BGl_valloczf2Cinfozb2optimzd2approxz92zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5792));
		}

	}



/* valloc/Cinfo+optim-approx-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozb2optimzd2approxzd2setz12z52zzcfa_info3z00
		(BgL_vallocz00_bglt BgL_oz00_502, BgL_approxz00_bglt BgL_vz00_503)
	{
		{	/* Cfa/cinfo3.sch 611 */
			{
				BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9043;

				{
					obj_t BgL_auxz00_9044;

					{	/* Cfa/cinfo3.sch 611 */
						BgL_objectz00_bglt BgL_tmpz00_9045;

						BgL_tmpz00_9045 = ((BgL_objectz00_bglt) BgL_oz00_502);
						BgL_auxz00_9044 = BGL_OBJECT_WIDENING(BgL_tmpz00_9045);
					}
					BgL_auxz00_9043 =
						((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9044);
				}
				return
					((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9043))->
						BgL_approxz00) = ((BgL_approxz00_bglt) BgL_vz00_503), BUNSPEC);
			}
		}

	}



/* &valloc/Cinfo+optim-approx-set! */
	obj_t BGl_z62valloczf2Cinfozb2optimzd2approxzd2setz12z30zzcfa_info3z00(obj_t
		BgL_envz00_5793, obj_t BgL_oz00_5794, obj_t BgL_vz00_5795)
	{
		{	/* Cfa/cinfo3.sch 611 */
			return
				BGl_valloczf2Cinfozb2optimzd2approxzd2setz12z52zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5794),
				((BgL_approxz00_bglt) BgL_vz00_5795));
		}

	}



/* valloc/Cinfo+optim-otype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_valloczf2Cinfozb2optimzd2otypez92zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_504)
	{
		{	/* Cfa/cinfo3.sch 612 */
			return
				(((BgL_vallocz00_bglt) COBJECT(
						((BgL_vallocz00_bglt) BgL_oz00_504)))->BgL_otypez00);
		}

	}



/* &valloc/Cinfo+optim-otype */
	BgL_typez00_bglt BGl_z62valloczf2Cinfozb2optimzd2otypezf0zzcfa_info3z00(obj_t
		BgL_envz00_5796, obj_t BgL_oz00_5797)
	{
		{	/* Cfa/cinfo3.sch 612 */
			return
				BGl_valloczf2Cinfozb2optimzd2otypez92zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5797));
		}

	}



/* valloc/Cinfo+optim-otype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozb2optimzd2otypezd2setz12z52zzcfa_info3z00
		(BgL_vallocz00_bglt BgL_oz00_505, BgL_typez00_bglt BgL_vz00_506)
	{
		{	/* Cfa/cinfo3.sch 613 */
			return
				((((BgL_vallocz00_bglt) COBJECT(
							((BgL_vallocz00_bglt) BgL_oz00_505)))->BgL_otypez00) =
				((BgL_typez00_bglt) BgL_vz00_506), BUNSPEC);
		}

	}



/* &valloc/Cinfo+optim-otype-set! */
	obj_t BGl_z62valloczf2Cinfozb2optimzd2otypezd2setz12z30zzcfa_info3z00(obj_t
		BgL_envz00_5798, obj_t BgL_oz00_5799, obj_t BgL_vz00_5800)
	{
		{	/* Cfa/cinfo3.sch 613 */
			return
				BGl_valloczf2Cinfozb2optimzd2otypezd2setz12z52zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5799),
				((BgL_typez00_bglt) BgL_vz00_5800));
		}

	}



/* valloc/Cinfo+optim-ftype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_valloczf2Cinfozb2optimzd2ftypez92zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_507)
	{
		{	/* Cfa/cinfo3.sch 614 */
			return
				(((BgL_vallocz00_bglt) COBJECT(
						((BgL_vallocz00_bglt) BgL_oz00_507)))->BgL_ftypez00);
		}

	}



/* &valloc/Cinfo+optim-ftype */
	BgL_typez00_bglt BGl_z62valloczf2Cinfozb2optimzd2ftypezf0zzcfa_info3z00(obj_t
		BgL_envz00_5801, obj_t BgL_oz00_5802)
	{
		{	/* Cfa/cinfo3.sch 614 */
			return
				BGl_valloczf2Cinfozb2optimzd2ftypez92zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5802));
		}

	}



/* valloc/Cinfo+optim-ftype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozb2optimzd2ftypezd2setz12z52zzcfa_info3z00
		(BgL_vallocz00_bglt BgL_oz00_508, BgL_typez00_bglt BgL_vz00_509)
	{
		{	/* Cfa/cinfo3.sch 615 */
			return
				((((BgL_vallocz00_bglt) COBJECT(
							((BgL_vallocz00_bglt) BgL_oz00_508)))->BgL_ftypez00) =
				((BgL_typez00_bglt) BgL_vz00_509), BUNSPEC);
		}

	}



/* &valloc/Cinfo+optim-ftype-set! */
	obj_t BGl_z62valloczf2Cinfozb2optimzd2ftypezd2setz12z30zzcfa_info3z00(obj_t
		BgL_envz00_5803, obj_t BgL_oz00_5804, obj_t BgL_vz00_5805)
	{
		{	/* Cfa/cinfo3.sch 615 */
			return
				BGl_valloczf2Cinfozb2optimzd2ftypezd2setz12z52zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5804),
				((BgL_typez00_bglt) BgL_vz00_5805));
		}

	}



/* valloc/Cinfo+optim-c-format */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozb2optimzd2czd2formatz40zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_510)
	{
		{	/* Cfa/cinfo3.sch 616 */
			return
				(((BgL_privatez00_bglt) COBJECT(
						((BgL_privatez00_bglt) BgL_oz00_510)))->BgL_czd2formatzd2);
		}

	}



/* &valloc/Cinfo+optim-c-format */
	obj_t BGl_z62valloczf2Cinfozb2optimzd2czd2formatz22zzcfa_info3z00(obj_t
		BgL_envz00_5806, obj_t BgL_oz00_5807)
	{
		{	/* Cfa/cinfo3.sch 616 */
			return
				BGl_valloczf2Cinfozb2optimzd2czd2formatz40zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5807));
		}

	}



/* valloc/Cinfo+optim-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozb2optimzd2effectz92zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_513)
	{
		{	/* Cfa/cinfo3.sch 618 */
			return
				(((BgL_externz00_bglt) COBJECT(
						((BgL_externz00_bglt) BgL_oz00_513)))->BgL_effectz00);
		}

	}



/* &valloc/Cinfo+optim-effect */
	obj_t BGl_z62valloczf2Cinfozb2optimzd2effectzf0zzcfa_info3z00(obj_t
		BgL_envz00_5808, obj_t BgL_oz00_5809)
	{
		{	/* Cfa/cinfo3.sch 618 */
			return
				BGl_valloczf2Cinfozb2optimzd2effectz92zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5809));
		}

	}



/* valloc/Cinfo+optim-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozb2optimzd2effectzd2setz12z52zzcfa_info3z00
		(BgL_vallocz00_bglt BgL_oz00_514, obj_t BgL_vz00_515)
	{
		{	/* Cfa/cinfo3.sch 619 */
			return
				((((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_oz00_514)))->BgL_effectz00) =
				((obj_t) BgL_vz00_515), BUNSPEC);
		}

	}



/* &valloc/Cinfo+optim-effect-set! */
	obj_t BGl_z62valloczf2Cinfozb2optimzd2effectzd2setz12z30zzcfa_info3z00(obj_t
		BgL_envz00_5810, obj_t BgL_oz00_5811, obj_t BgL_vz00_5812)
	{
		{	/* Cfa/cinfo3.sch 619 */
			return
				BGl_valloczf2Cinfozb2optimzd2effectzd2setz12z52zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5811), BgL_vz00_5812);
		}

	}



/* valloc/Cinfo+optim-expr* */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozb2optimzd2exprza2z30zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_516)
	{
		{	/* Cfa/cinfo3.sch 620 */
			return
				(((BgL_externz00_bglt) COBJECT(
						((BgL_externz00_bglt) BgL_oz00_516)))->BgL_exprza2za2);
		}

	}



/* &valloc/Cinfo+optim-expr* */
	obj_t BGl_z62valloczf2Cinfozb2optimzd2exprza2z52zzcfa_info3z00(obj_t
		BgL_envz00_5813, obj_t BgL_oz00_5814)
	{
		{	/* Cfa/cinfo3.sch 620 */
			return
				BGl_valloczf2Cinfozb2optimzd2exprza2z30zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5814));
		}

	}



/* valloc/Cinfo+optim-expr*-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozb2optimzd2exprza2zd2setz12zf0zzcfa_info3z00
		(BgL_vallocz00_bglt BgL_oz00_517, obj_t BgL_vz00_518)
	{
		{	/* Cfa/cinfo3.sch 621 */
			return
				((((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_oz00_517)))->BgL_exprza2za2) =
				((obj_t) BgL_vz00_518), BUNSPEC);
		}

	}



/* &valloc/Cinfo+optim-expr*-set! */
	obj_t BGl_z62valloczf2Cinfozb2optimzd2exprza2zd2setz12z92zzcfa_info3z00(obj_t
		BgL_envz00_5815, obj_t BgL_oz00_5816, obj_t BgL_vz00_5817)
	{
		{	/* Cfa/cinfo3.sch 621 */
			return
				BGl_valloczf2Cinfozb2optimzd2exprza2zd2setz12zf0zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5816), BgL_vz00_5817);
		}

	}



/* valloc/Cinfo+optim-key */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozb2optimzd2keyz92zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_519)
	{
		{	/* Cfa/cinfo3.sch 622 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_519)))->BgL_keyz00);
		}

	}



/* &valloc/Cinfo+optim-key */
	obj_t BGl_z62valloczf2Cinfozb2optimzd2keyzf0zzcfa_info3z00(obj_t
		BgL_envz00_5818, obj_t BgL_oz00_5819)
	{
		{	/* Cfa/cinfo3.sch 622 */
			return
				BGl_valloczf2Cinfozb2optimzd2keyz92zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5819));
		}

	}



/* valloc/Cinfo+optim-key-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozb2optimzd2keyzd2setz12z52zzcfa_info3z00
		(BgL_vallocz00_bglt BgL_oz00_520, obj_t BgL_vz00_521)
	{
		{	/* Cfa/cinfo3.sch 623 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_520)))->BgL_keyz00) =
				((obj_t) BgL_vz00_521), BUNSPEC);
		}

	}



/* &valloc/Cinfo+optim-key-set! */
	obj_t BGl_z62valloczf2Cinfozb2optimzd2keyzd2setz12z30zzcfa_info3z00(obj_t
		BgL_envz00_5820, obj_t BgL_oz00_5821, obj_t BgL_vz00_5822)
	{
		{	/* Cfa/cinfo3.sch 623 */
			return
				BGl_valloczf2Cinfozb2optimzd2keyzd2setz12z52zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5821), BgL_vz00_5822);
		}

	}



/* valloc/Cinfo+optim-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozb2optimzd2sidezd2effectz40zzcfa_info3z00
		(BgL_vallocz00_bglt BgL_oz00_522)
	{
		{	/* Cfa/cinfo3.sch 624 */
			return
				(((BgL_nodezf2effectzf2_bglt) COBJECT(
						((BgL_nodezf2effectzf2_bglt) BgL_oz00_522)))->BgL_sidezd2effectzd2);
		}

	}



/* &valloc/Cinfo+optim-side-effect */
	obj_t BGl_z62valloczf2Cinfozb2optimzd2sidezd2effectz22zzcfa_info3z00(obj_t
		BgL_envz00_5823, obj_t BgL_oz00_5824)
	{
		{	/* Cfa/cinfo3.sch 624 */
			return
				BGl_valloczf2Cinfozb2optimzd2sidezd2effectz40zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5824));
		}

	}



/* valloc/Cinfo+optim-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozb2optimzd2sidezd2effectzd2setz12z80zzcfa_info3z00
		(BgL_vallocz00_bglt BgL_oz00_523, obj_t BgL_vz00_524)
	{
		{	/* Cfa/cinfo3.sch 625 */
			return
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_oz00_523)))->
					BgL_sidezd2effectzd2) = ((obj_t) BgL_vz00_524), BUNSPEC);
		}

	}



/* &valloc/Cinfo+optim-side-effect-set! */
	obj_t
		BGl_z62valloczf2Cinfozb2optimzd2sidezd2effectzd2setz12ze2zzcfa_info3z00
		(obj_t BgL_envz00_5825, obj_t BgL_oz00_5826, obj_t BgL_vz00_5827)
	{
		{	/* Cfa/cinfo3.sch 625 */
			return
				BGl_valloczf2Cinfozb2optimzd2sidezd2effectzd2setz12z80zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5826), BgL_vz00_5827);
		}

	}



/* valloc/Cinfo+optim-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_valloczf2Cinfozb2optimzd2typez92zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_525)
	{
		{	/* Cfa/cinfo3.sch 626 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_525)))->BgL_typez00);
		}

	}



/* &valloc/Cinfo+optim-type */
	BgL_typez00_bglt BGl_z62valloczf2Cinfozb2optimzd2typezf0zzcfa_info3z00(obj_t
		BgL_envz00_5828, obj_t BgL_oz00_5829)
	{
		{	/* Cfa/cinfo3.sch 626 */
			return
				BGl_valloczf2Cinfozb2optimzd2typez92zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5829));
		}

	}



/* valloc/Cinfo+optim-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozb2optimzd2typezd2setz12z52zzcfa_info3z00
		(BgL_vallocz00_bglt BgL_oz00_526, BgL_typez00_bglt BgL_vz00_527)
	{
		{	/* Cfa/cinfo3.sch 627 */
			return
				((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_oz00_526)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_527), BUNSPEC);
		}

	}



/* &valloc/Cinfo+optim-type-set! */
	obj_t BGl_z62valloczf2Cinfozb2optimzd2typezd2setz12z30zzcfa_info3z00(obj_t
		BgL_envz00_5830, obj_t BgL_oz00_5831, obj_t BgL_vz00_5832)
	{
		{	/* Cfa/cinfo3.sch 627 */
			return
				BGl_valloczf2Cinfozb2optimzd2typezd2setz12z52zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5831),
				((BgL_typez00_bglt) BgL_vz00_5832));
		}

	}



/* valloc/Cinfo+optim-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_valloczf2Cinfozb2optimzd2locz92zzcfa_info3z00(BgL_vallocz00_bglt
		BgL_oz00_528)
	{
		{	/* Cfa/cinfo3.sch 628 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt) BgL_oz00_528)))->BgL_locz00);
		}

	}



/* &valloc/Cinfo+optim-loc */
	obj_t BGl_z62valloczf2Cinfozb2optimzd2loczf0zzcfa_info3z00(obj_t
		BgL_envz00_5833, obj_t BgL_oz00_5834)
	{
		{	/* Cfa/cinfo3.sch 628 */
			return
				BGl_valloczf2Cinfozb2optimzd2locz92zzcfa_info3z00(
				((BgL_vallocz00_bglt) BgL_oz00_5834));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcfa_info3z00(void)
	{
		{	/* Cfa/cinfo3.scm 16 */
			{	/* Cfa/cinfo3.scm 27 */
				obj_t BgL_arg1754z00_2847;
				obj_t BgL_arg1755z00_2848;

				{	/* Cfa/cinfo3.scm 27 */
					obj_t BgL_v1676z00_2878;

					BgL_v1676z00_2878 = create_vector(1L);
					{	/* Cfa/cinfo3.scm 27 */
						obj_t BgL_arg1775z00_2879;

						BgL_arg1775z00_2879 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc2268z00zzcfa_info3z00, BGl_proc2267z00zzcfa_info3z00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_approxz00zzcfa_infoz00);
						VECTOR_SET(BgL_v1676z00_2878, 0L, BgL_arg1775z00_2879);
					}
					BgL_arg1754z00_2847 = BgL_v1676z00_2878;
				}
				{	/* Cfa/cinfo3.scm 27 */
					obj_t BgL_v1677z00_2889;

					BgL_v1677z00_2889 = create_vector(0L);
					BgL_arg1755z00_2848 = BgL_v1677z00_2889;
				}
				BGl_pragmazf2Cinfozf2zzcfa_info3z00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(2),
					CNST_TABLE_REF(3), BGl_pragmaz00zzast_nodez00, 57602L,
					BGl_proc2272z00zzcfa_info3z00, BGl_proc2271z00zzcfa_info3z00, BFALSE,
					BGl_proc2270z00zzcfa_info3z00, BGl_proc2269z00zzcfa_info3z00,
					BgL_arg1754z00_2847, BgL_arg1755z00_2848);
			}
			{	/* Cfa/cinfo3.scm 28 */
				obj_t BgL_arg1820z00_2898;
				obj_t BgL_arg1822z00_2899;

				{	/* Cfa/cinfo3.scm 28 */
					obj_t BgL_v1678z00_2931;

					BgL_v1678z00_2931 = create_vector(1L);
					{	/* Cfa/cinfo3.scm 28 */
						obj_t BgL_arg1840z00_2932;

						BgL_arg1840z00_2932 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc2274z00zzcfa_info3z00, BGl_proc2273z00zzcfa_info3z00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_approxz00zzcfa_infoz00);
						VECTOR_SET(BgL_v1678z00_2931, 0L, BgL_arg1840z00_2932);
					}
					BgL_arg1820z00_2898 = BgL_v1678z00_2931;
				}
				{	/* Cfa/cinfo3.scm 28 */
					obj_t BgL_v1679z00_2942;

					BgL_v1679z00_2942 = create_vector(0L);
					BgL_arg1822z00_2899 = BgL_v1679z00_2942;
				}
				BGl_getfieldzf2Cinfozf2zzcfa_info3z00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(4),
					CNST_TABLE_REF(3), BGl_getfieldz00zzast_nodez00, 59690L,
					BGl_proc2278z00zzcfa_info3z00, BGl_proc2277z00zzcfa_info3z00, BFALSE,
					BGl_proc2276z00zzcfa_info3z00, BGl_proc2275z00zzcfa_info3z00,
					BgL_arg1820z00_2898, BgL_arg1822z00_2899);
			}
			{	/* Cfa/cinfo3.scm 29 */
				obj_t BgL_arg1850z00_2951;
				obj_t BgL_arg1851z00_2952;

				{	/* Cfa/cinfo3.scm 29 */
					obj_t BgL_v1680z00_2984;

					BgL_v1680z00_2984 = create_vector(1L);
					{	/* Cfa/cinfo3.scm 29 */
						obj_t BgL_arg1863z00_2985;

						BgL_arg1863z00_2985 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc2280z00zzcfa_info3z00, BGl_proc2279z00zzcfa_info3z00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_approxz00zzcfa_infoz00);
						VECTOR_SET(BgL_v1680z00_2984, 0L, BgL_arg1863z00_2985);
					}
					BgL_arg1850z00_2951 = BgL_v1680z00_2984;
				}
				{	/* Cfa/cinfo3.scm 29 */
					obj_t BgL_v1681z00_2995;

					BgL_v1681z00_2995 = create_vector(0L);
					BgL_arg1851z00_2952 = BgL_v1681z00_2995;
				}
				BGl_setfieldzf2Cinfozf2zzcfa_info3z00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(5),
					CNST_TABLE_REF(3), BGl_setfieldz00zzast_nodez00, 23090L,
					BGl_proc2284z00zzcfa_info3z00, BGl_proc2283z00zzcfa_info3z00, BFALSE,
					BGl_proc2282z00zzcfa_info3z00, BGl_proc2281z00zzcfa_info3z00,
					BgL_arg1850z00_2951, BgL_arg1851z00_2952);
			}
			{	/* Cfa/cinfo3.scm 30 */
				obj_t BgL_arg1874z00_3004;
				obj_t BgL_arg1875z00_3005;

				{	/* Cfa/cinfo3.scm 30 */
					obj_t BgL_v1682z00_3035;

					BgL_v1682z00_3035 = create_vector(1L);
					{	/* Cfa/cinfo3.scm 30 */
						obj_t BgL_arg1887z00_3036;

						BgL_arg1887z00_3036 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc2286z00zzcfa_info3z00, BGl_proc2285z00zzcfa_info3z00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_approxz00zzcfa_infoz00);
						VECTOR_SET(BgL_v1682z00_3035, 0L, BgL_arg1887z00_3036);
					}
					BgL_arg1874z00_3004 = BgL_v1682z00_3035;
				}
				{	/* Cfa/cinfo3.scm 30 */
					obj_t BgL_v1683z00_3046;

					BgL_v1683z00_3046 = create_vector(0L);
					BgL_arg1875z00_3005 = BgL_v1683z00_3046;
				}
				BGl_newzf2Cinfozf2zzcfa_info3z00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(6),
					CNST_TABLE_REF(3), BGl_newz00zzast_nodez00, 32422L,
					BGl_proc2290z00zzcfa_info3z00, BGl_proc2289z00zzcfa_info3z00, BFALSE,
					BGl_proc2288z00zzcfa_info3z00, BGl_proc2287z00zzcfa_info3z00,
					BgL_arg1874z00_3004, BgL_arg1875z00_3005);
			}
			{	/* Cfa/cinfo3.scm 31 */
				obj_t BgL_arg1897z00_3055;
				obj_t BgL_arg1898z00_3056;

				{	/* Cfa/cinfo3.scm 31 */
					obj_t BgL_v1684z00_3086;

					BgL_v1684z00_3086 = create_vector(1L);
					{	/* Cfa/cinfo3.scm 31 */
						obj_t BgL_arg1913z00_3087;

						BgL_arg1913z00_3087 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc2292z00zzcfa_info3z00, BGl_proc2291z00zzcfa_info3z00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_approxz00zzcfa_infoz00);
						VECTOR_SET(BgL_v1684z00_3086, 0L, BgL_arg1913z00_3087);
					}
					BgL_arg1897z00_3055 = BgL_v1684z00_3086;
				}
				{	/* Cfa/cinfo3.scm 31 */
					obj_t BgL_v1685z00_3097;

					BgL_v1685z00_3097 = create_vector(0L);
					BgL_arg1898z00_3056 = BgL_v1685z00_3097;
				}
				BGl_instanceofzf2Cinfozf2zzcfa_info3z00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(7),
					CNST_TABLE_REF(3), BGl_instanceofz00zzast_nodez00, 4262L,
					BGl_proc2296z00zzcfa_info3z00, BGl_proc2295z00zzcfa_info3z00, BFALSE,
					BGl_proc2294z00zzcfa_info3z00, BGl_proc2293z00zzcfa_info3z00,
					BgL_arg1897z00_3055, BgL_arg1898z00_3056);
			}
			{	/* Cfa/cinfo3.scm 32 */
				obj_t BgL_arg1925z00_3106;
				obj_t BgL_arg1926z00_3107;

				{	/* Cfa/cinfo3.scm 32 */
					obj_t BgL_v1686z00_3136;

					BgL_v1686z00_3136 = create_vector(1L);
					{	/* Cfa/cinfo3.scm 32 */
						obj_t BgL_arg1937z00_3137;

						BgL_arg1937z00_3137 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc2298z00zzcfa_info3z00, BGl_proc2297z00zzcfa_info3z00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_approxz00zzcfa_infoz00);
						VECTOR_SET(BgL_v1686z00_3136, 0L, BgL_arg1937z00_3137);
					}
					BgL_arg1925z00_3106 = BgL_v1686z00_3136;
				}
				{	/* Cfa/cinfo3.scm 32 */
					obj_t BgL_v1687z00_3147;

					BgL_v1687z00_3147 = create_vector(0L);
					BgL_arg1926z00_3107 = BgL_v1687z00_3147;
				}
				BGl_castzd2nullzf2Cinfoz20zzcfa_info3z00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(8),
					CNST_TABLE_REF(3), BGl_castzd2nullzd2zzast_nodez00, 6484L,
					BGl_proc2302z00zzcfa_info3z00, BGl_proc2301z00zzcfa_info3z00, BFALSE,
					BGl_proc2300z00zzcfa_info3z00, BGl_proc2299z00zzcfa_info3z00,
					BgL_arg1925z00_3106, BgL_arg1926z00_3107);
			}
			{	/* Cfa/cinfo3.scm 35 */
				obj_t BgL_arg1946z00_3156;
				obj_t BgL_arg1947z00_3157;

				{	/* Cfa/cinfo3.scm 35 */
					obj_t BgL_v1688z00_3191;

					BgL_v1688z00_3191 = create_vector(2L);
					{	/* Cfa/cinfo3.scm 35 */
						obj_t BgL_arg1958z00_3192;

						BgL_arg1958z00_3192 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc2304z00zzcfa_info3z00, BGl_proc2303z00zzcfa_info3z00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_approxz00zzcfa_infoz00);
						VECTOR_SET(BgL_v1688z00_3191, 0L, BgL_arg1958z00_3192);
					}
					{	/* Cfa/cinfo3.scm 35 */
						obj_t BgL_arg1963z00_3202;

						BgL_arg1963z00_3202 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(9),
							BGl_proc2307z00zzcfa_info3z00, BGl_proc2306z00zzcfa_info3z00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2305z00zzcfa_info3z00,
							CNST_TABLE_REF(10));
						VECTOR_SET(BgL_v1688z00_3191, 1L, BgL_arg1963z00_3202);
					}
					BgL_arg1946z00_3156 = BgL_v1688z00_3191;
				}
				{	/* Cfa/cinfo3.scm 35 */
					obj_t BgL_v1689z00_3215;

					BgL_v1689z00_3215 = create_vector(0L);
					BgL_arg1947z00_3157 = BgL_v1689z00_3215;
				}
				BGl_vrefzf2Cinfozf2zzcfa_info3z00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(11),
					CNST_TABLE_REF(3), BGl_vrefz00zzast_nodez00, 64960L,
					BGl_proc2311z00zzcfa_info3z00, BGl_proc2310z00zzcfa_info3z00, BFALSE,
					BGl_proc2309z00zzcfa_info3z00, BGl_proc2308z00zzcfa_info3z00,
					BgL_arg1946z00_3156, BgL_arg1947z00_3157);
			}
			{	/* Cfa/cinfo3.scm 38 */
				obj_t BgL_arg1974z00_3224;
				obj_t BgL_arg1975z00_3225;

				{	/* Cfa/cinfo3.scm 38 */
					obj_t BgL_v1690z00_3259;

					BgL_v1690z00_3259 = create_vector(2L);
					{	/* Cfa/cinfo3.scm 38 */
						obj_t BgL_arg1986z00_3260;

						BgL_arg1986z00_3260 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc2313z00zzcfa_info3z00, BGl_proc2312z00zzcfa_info3z00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_approxz00zzcfa_infoz00);
						VECTOR_SET(BgL_v1690z00_3259, 0L, BgL_arg1986z00_3260);
					}
					{	/* Cfa/cinfo3.scm 38 */
						obj_t BgL_arg1991z00_3270;

						BgL_arg1991z00_3270 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(9),
							BGl_proc2316z00zzcfa_info3z00, BGl_proc2315z00zzcfa_info3z00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2314z00zzcfa_info3z00,
							CNST_TABLE_REF(10));
						VECTOR_SET(BgL_v1690z00_3259, 1L, BgL_arg1991z00_3270);
					}
					BgL_arg1974z00_3224 = BgL_v1690z00_3259;
				}
				{	/* Cfa/cinfo3.scm 38 */
					obj_t BgL_v1691z00_3283;

					BgL_v1691z00_3283 = create_vector(0L);
					BgL_arg1975z00_3225 = BgL_v1691z00_3283;
				}
				BGl_vsetz12zf2Cinfoze0zzcfa_info3z00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(12),
					CNST_TABLE_REF(3), BGl_vsetz12z12zzast_nodez00, 39936L,
					BGl_proc2320z00zzcfa_info3z00, BGl_proc2319z00zzcfa_info3z00, BFALSE,
					BGl_proc2318z00zzcfa_info3z00, BGl_proc2317z00zzcfa_info3z00,
					BgL_arg1974z00_3224, BgL_arg1975z00_3225);
			}
			{	/* Cfa/cinfo3.scm 41 */
				obj_t BgL_arg2002z00_3292;
				obj_t BgL_arg2003z00_3293;

				{	/* Cfa/cinfo3.scm 41 */
					obj_t BgL_v1692z00_3325;

					BgL_v1692z00_3325 = create_vector(2L);
					{	/* Cfa/cinfo3.scm 41 */
						obj_t BgL_arg2015z00_3326;

						BgL_arg2015z00_3326 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc2322z00zzcfa_info3z00, BGl_proc2321z00zzcfa_info3z00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_approxz00zzcfa_infoz00);
						VECTOR_SET(BgL_v1692z00_3325, 0L, BgL_arg2015z00_3326);
					}
					{	/* Cfa/cinfo3.scm 41 */
						obj_t BgL_arg2020z00_3336;

						BgL_arg2020z00_3336 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(9),
							BGl_proc2325z00zzcfa_info3z00, BGl_proc2324z00zzcfa_info3z00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2323z00zzcfa_info3z00,
							CNST_TABLE_REF(10));
						VECTOR_SET(BgL_v1692z00_3325, 1L, BgL_arg2020z00_3336);
					}
					BgL_arg2002z00_3292 = BgL_v1692z00_3325;
				}
				{	/* Cfa/cinfo3.scm 41 */
					obj_t BgL_v1693z00_3349;

					BgL_v1693z00_3349 = create_vector(0L);
					BgL_arg2003z00_3293 = BgL_v1693z00_3349;
				}
				BGl_vlengthzf2Cinfozf2zzcfa_info3z00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(13),
					CNST_TABLE_REF(3), BGl_vlengthz00zzast_nodez00, 33526L,
					BGl_proc2329z00zzcfa_info3z00, BGl_proc2328z00zzcfa_info3z00, BFALSE,
					BGl_proc2327z00zzcfa_info3z00, BGl_proc2326z00zzcfa_info3z00,
					BgL_arg2002z00_3292, BgL_arg2003z00_3293);
			}
			{	/* Cfa/cinfo3.scm 46 */
				obj_t BgL_arg2034z00_3358;
				obj_t BgL_arg2036z00_3359;

				{	/* Cfa/cinfo3.scm 46 */
					obj_t BgL_v1694z00_3390;

					BgL_v1694z00_3390 = create_vector(1L);
					{	/* Cfa/cinfo3.scm 46 */
						obj_t BgL_arg2047z00_3391;

						BgL_arg2047z00_3391 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(14),
							BGl_proc2331z00zzcfa_info3z00, BGl_proc2330z00zzcfa_info3z00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_variablez00zzast_varz00);
						VECTOR_SET(BgL_v1694z00_3390, 0L, BgL_arg2047z00_3391);
					}
					BgL_arg2034z00_3358 = BgL_v1694z00_3390;
				}
				{	/* Cfa/cinfo3.scm 46 */
					obj_t BgL_v1695z00_3401;

					BgL_v1695z00_3401 = create_vector(0L);
					BgL_arg2036z00_3359 = BgL_v1695z00_3401;
				}
				BGl_prezd2valloczf2Cinfoz20zzcfa_info3z00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(15),
					CNST_TABLE_REF(3), BGl_vallocz00zzast_nodez00, 33185L,
					BGl_proc2335z00zzcfa_info3z00, BGl_proc2334z00zzcfa_info3z00, BFALSE,
					BGl_proc2333z00zzcfa_info3z00, BGl_proc2332z00zzcfa_info3z00,
					BgL_arg2034z00_3358, BgL_arg2036z00_3359);
			}
			{	/* Cfa/cinfo3.scm 48 */
				obj_t BgL_arg2058z00_3410;
				obj_t BgL_arg2059z00_3411;

				{	/* Cfa/cinfo3.scm 48 */
					obj_t BgL_v1696z00_3442;

					BgL_v1696z00_3442 = create_vector(1L);
					{	/* Cfa/cinfo3.scm 48 */
						obj_t BgL_arg2070z00_3443;

						BgL_arg2070z00_3443 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc2337z00zzcfa_info3z00, BGl_proc2336z00zzcfa_info3z00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_approxz00zzcfa_infoz00);
						VECTOR_SET(BgL_v1696z00_3442, 0L, BgL_arg2070z00_3443);
					}
					BgL_arg2058z00_3410 = BgL_v1696z00_3442;
				}
				{	/* Cfa/cinfo3.scm 48 */
					obj_t BgL_v1697z00_3453;

					BgL_v1697z00_3453 = create_vector(0L);
					BgL_arg2059z00_3411 = BgL_v1697z00_3453;
				}
				BGl_valloczf2Cinfozf2zzcfa_info3z00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(16),
					CNST_TABLE_REF(3), BGl_vallocz00zzast_nodez00, 10896L,
					BGl_proc2341z00zzcfa_info3z00, BGl_proc2340z00zzcfa_info3z00, BFALSE,
					BGl_proc2339z00zzcfa_info3z00, BGl_proc2338z00zzcfa_info3z00,
					BgL_arg2058z00_3410, BgL_arg2059z00_3411);
			}
			{	/* Cfa/cinfo3.scm 50 */
				obj_t BgL_arg2081z00_3462;
				obj_t BgL_arg2082z00_3463;

				{	/* Cfa/cinfo3.scm 50 */
					obj_t BgL_v1698z00_3500;

					BgL_v1698z00_3500 = create_vector(7L);
					{	/* Cfa/cinfo3.scm 50 */
						obj_t BgL_arg2095z00_3501;

						BgL_arg2095z00_3501 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc2343z00zzcfa_info3z00, BGl_proc2342z00zzcfa_info3z00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_approxz00zzcfa_infoz00);
						VECTOR_SET(BgL_v1698z00_3500, 0L, BgL_arg2095z00_3501);
					}
					{	/* Cfa/cinfo3.scm 50 */
						obj_t BgL_arg2100z00_3511;

						BgL_arg2100z00_3511 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(17),
							BGl_proc2345z00zzcfa_info3z00, BGl_proc2344z00zzcfa_info3z00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_approxz00zzcfa_infoz00);
						VECTOR_SET(BgL_v1698z00_3500, 1L, BgL_arg2100z00_3511);
					}
					{	/* Cfa/cinfo3.scm 50 */
						obj_t BgL_arg2105z00_3521;

						BgL_arg2105z00_3521 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(18),
							BGl_proc2348z00zzcfa_info3z00, BGl_proc2347z00zzcfa_info3z00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2346z00zzcfa_info3z00,
							CNST_TABLE_REF(19));
						VECTOR_SET(BgL_v1698z00_3500, 2L, BgL_arg2105z00_3521);
					}
					{	/* Cfa/cinfo3.scm 50 */
						obj_t BgL_arg2112z00_3534;

						BgL_arg2112z00_3534 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(14),
							BGl_proc2350z00zzcfa_info3z00, BGl_proc2349z00zzcfa_info3z00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_variablez00zzast_varz00);
						VECTOR_SET(BgL_v1698z00_3500, 3L, BgL_arg2112z00_3534);
					}
					{	/* Cfa/cinfo3.scm 50 */
						obj_t BgL_arg2117z00_3544;

						BgL_arg2117z00_3544 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(20),
							BGl_proc2353z00zzcfa_info3z00, BGl_proc2352z00zzcfa_info3z00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2351z00zzcfa_info3z00,
							CNST_TABLE_REF(10));
						VECTOR_SET(BgL_v1698z00_3500, 4L, BgL_arg2117z00_3544);
					}
					{	/* Cfa/cinfo3.scm 50 */
						obj_t BgL_arg2124z00_3557;

						BgL_arg2124z00_3557 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(21),
							BGl_proc2356z00zzcfa_info3z00, BGl_proc2355z00zzcfa_info3z00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2354z00zzcfa_info3z00,
							CNST_TABLE_REF(22));
						VECTOR_SET(BgL_v1698z00_3500, 5L, BgL_arg2124z00_3557);
					}
					{	/* Cfa/cinfo3.scm 50 */
						obj_t BgL_arg2131z00_3570;

						BgL_arg2131z00_3570 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(23),
							BGl_proc2359z00zzcfa_info3z00, BGl_proc2358z00zzcfa_info3z00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2357z00zzcfa_info3z00,
							CNST_TABLE_REF(10));
						VECTOR_SET(BgL_v1698z00_3500, 6L, BgL_arg2131z00_3570);
					}
					BgL_arg2081z00_3462 = BgL_v1698z00_3500;
				}
				{	/* Cfa/cinfo3.scm 50 */
					obj_t BgL_v1699z00_3583;

					BgL_v1699z00_3583 = create_vector(0L);
					BgL_arg2082z00_3463 = BgL_v1699z00_3583;
				}
				return (BGl_valloczf2Cinfozb2optimz40zzcfa_info3z00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(24),
						CNST_TABLE_REF(3), BGl_vallocz00zzast_nodez00, 18419L,
						BGl_proc2363z00zzcfa_info3z00, BGl_proc2362z00zzcfa_info3z00,
						BFALSE, BGl_proc2361z00zzcfa_info3z00,
						BGl_proc2360z00zzcfa_info3z00, BgL_arg2081z00_3462,
						BgL_arg2082z00_3463), BUNSPEC);
			}
		}

	}



/* &lambda2090 */
	BgL_vallocz00_bglt BGl_z62lambda2090z62zzcfa_info3z00(obj_t BgL_envz00_5932,
		obj_t BgL_o1336z00_5933)
	{
		{	/* Cfa/cinfo3.scm 50 */
			{	/* Cfa/cinfo3.scm 50 */
				long BgL_arg2091z00_6533;

				{	/* Cfa/cinfo3.scm 50 */
					obj_t BgL_arg2093z00_6534;

					{	/* Cfa/cinfo3.scm 50 */
						obj_t BgL_arg2094z00_6535;

						{	/* Cfa/cinfo3.scm 50 */
							obj_t BgL_arg1815z00_6536;
							long BgL_arg1816z00_6537;

							BgL_arg1815z00_6536 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Cfa/cinfo3.scm 50 */
								long BgL_arg1817z00_6538;

								BgL_arg1817z00_6538 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_vallocz00_bglt) BgL_o1336z00_5933)));
								BgL_arg1816z00_6537 = (BgL_arg1817z00_6538 - OBJECT_TYPE);
							}
							BgL_arg2094z00_6535 =
								VECTOR_REF(BgL_arg1815z00_6536, BgL_arg1816z00_6537);
						}
						BgL_arg2093z00_6534 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2094z00_6535);
					}
					{	/* Cfa/cinfo3.scm 50 */
						obj_t BgL_tmpz00_9257;

						BgL_tmpz00_9257 = ((obj_t) BgL_arg2093z00_6534);
						BgL_arg2091z00_6533 = BGL_CLASS_NUM(BgL_tmpz00_9257);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_vallocz00_bglt) BgL_o1336z00_5933)), BgL_arg2091z00_6533);
			}
			{	/* Cfa/cinfo3.scm 50 */
				BgL_objectz00_bglt BgL_tmpz00_9263;

				BgL_tmpz00_9263 =
					((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_o1336z00_5933));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9263, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_o1336z00_5933));
			return ((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_o1336z00_5933));
		}

	}



/* &<@anonymous:2089> */
	obj_t BGl_z62zc3z04anonymousza32089ze3ze5zzcfa_info3z00(obj_t BgL_envz00_5934,
		obj_t BgL_new1335z00_5935)
	{
		{	/* Cfa/cinfo3.scm 50 */
			{
				BgL_vallocz00_bglt BgL_auxz00_9271;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_vallocz00_bglt) BgL_new1335z00_5935))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_9275;

					{	/* Cfa/cinfo3.scm 50 */
						obj_t BgL_classz00_6540;

						BgL_classz00_6540 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 50 */
							obj_t BgL__ortest_1117z00_6541;

							BgL__ortest_1117z00_6541 = BGL_CLASS_NIL(BgL_classz00_6540);
							if (CBOOL(BgL__ortest_1117z00_6541))
								{	/* Cfa/cinfo3.scm 50 */
									BgL_auxz00_9275 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6541);
								}
							else
								{	/* Cfa/cinfo3.scm 50 */
									BgL_auxz00_9275 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6540));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_vallocz00_bglt) BgL_new1335z00_5935))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_9275), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_vallocz00_bglt) BgL_new1335z00_5935))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_vallocz00_bglt)
										BgL_new1335z00_5935))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_vallocz00_bglt)
										BgL_new1335z00_5935))))->BgL_exprza2za2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_vallocz00_bglt)
										BgL_new1335z00_5935))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_privatez00_bglt)
							COBJECT(((BgL_privatez00_bglt) ((BgL_vallocz00_bglt)
										BgL_new1335z00_5935))))->BgL_czd2formatzd2) =
					((obj_t) BGl_string2364z00zzcfa_info3z00), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_9300;

					{	/* Cfa/cinfo3.scm 50 */
						obj_t BgL_classz00_6542;

						BgL_classz00_6542 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 50 */
							obj_t BgL__ortest_1117z00_6543;

							BgL__ortest_1117z00_6543 = BGL_CLASS_NIL(BgL_classz00_6542);
							if (CBOOL(BgL__ortest_1117z00_6543))
								{	/* Cfa/cinfo3.scm 50 */
									BgL_auxz00_9300 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6543);
								}
							else
								{	/* Cfa/cinfo3.scm 50 */
									BgL_auxz00_9300 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6542));
								}
						}
					}
					((((BgL_vallocz00_bglt) COBJECT(
									((BgL_vallocz00_bglt)
										((BgL_vallocz00_bglt) BgL_new1335z00_5935))))->
							BgL_ftypez00) = ((BgL_typez00_bglt) BgL_auxz00_9300), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_9310;

					{	/* Cfa/cinfo3.scm 50 */
						obj_t BgL_classz00_6544;

						BgL_classz00_6544 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 50 */
							obj_t BgL__ortest_1117z00_6545;

							BgL__ortest_1117z00_6545 = BGL_CLASS_NIL(BgL_classz00_6544);
							if (CBOOL(BgL__ortest_1117z00_6545))
								{	/* Cfa/cinfo3.scm 50 */
									BgL_auxz00_9310 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6545);
								}
							else
								{	/* Cfa/cinfo3.scm 50 */
									BgL_auxz00_9310 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6544));
								}
						}
					}
					((((BgL_vallocz00_bglt) COBJECT(
									((BgL_vallocz00_bglt)
										((BgL_vallocz00_bglt) BgL_new1335z00_5935))))->
							BgL_otypez00) = ((BgL_typez00_bglt) BgL_auxz00_9310), BUNSPEC);
				}
				{
					BgL_approxz00_bglt BgL_auxz00_9327;
					BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9320;

					{	/* Cfa/cinfo3.scm 50 */
						obj_t BgL_classz00_6546;

						BgL_classz00_6546 = BGl_approxz00zzcfa_infoz00;
						{	/* Cfa/cinfo3.scm 50 */
							obj_t BgL__ortest_1117z00_6547;

							BgL__ortest_1117z00_6547 = BGL_CLASS_NIL(BgL_classz00_6546);
							if (CBOOL(BgL__ortest_1117z00_6547))
								{	/* Cfa/cinfo3.scm 50 */
									BgL_auxz00_9327 =
										((BgL_approxz00_bglt) BgL__ortest_1117z00_6547);
								}
							else
								{	/* Cfa/cinfo3.scm 50 */
									BgL_auxz00_9327 =
										((BgL_approxz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6546));
								}
						}
					}
					{
						obj_t BgL_auxz00_9321;

						{	/* Cfa/cinfo3.scm 50 */
							BgL_objectz00_bglt BgL_tmpz00_9322;

							BgL_tmpz00_9322 =
								((BgL_objectz00_bglt)
								((BgL_vallocz00_bglt) BgL_new1335z00_5935));
							BgL_auxz00_9321 = BGL_OBJECT_WIDENING(BgL_tmpz00_9322);
						}
						BgL_auxz00_9320 =
							((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9321);
					}
					((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9320))->
							BgL_approxz00) = ((BgL_approxz00_bglt) BgL_auxz00_9327), BUNSPEC);
				}
				{
					BgL_approxz00_bglt BgL_auxz00_9342;
					BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9335;

					{	/* Cfa/cinfo3.scm 50 */
						obj_t BgL_classz00_6548;

						BgL_classz00_6548 = BGl_approxz00zzcfa_infoz00;
						{	/* Cfa/cinfo3.scm 50 */
							obj_t BgL__ortest_1117z00_6549;

							BgL__ortest_1117z00_6549 = BGL_CLASS_NIL(BgL_classz00_6548);
							if (CBOOL(BgL__ortest_1117z00_6549))
								{	/* Cfa/cinfo3.scm 50 */
									BgL_auxz00_9342 =
										((BgL_approxz00_bglt) BgL__ortest_1117z00_6549);
								}
							else
								{	/* Cfa/cinfo3.scm 50 */
									BgL_auxz00_9342 =
										((BgL_approxz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6548));
								}
						}
					}
					{
						obj_t BgL_auxz00_9336;

						{	/* Cfa/cinfo3.scm 50 */
							BgL_objectz00_bglt BgL_tmpz00_9337;

							BgL_tmpz00_9337 =
								((BgL_objectz00_bglt)
								((BgL_vallocz00_bglt) BgL_new1335z00_5935));
							BgL_auxz00_9336 = BGL_OBJECT_WIDENING(BgL_tmpz00_9337);
						}
						BgL_auxz00_9335 =
							((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9336);
					}
					((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9335))->
							BgL_valuezd2approxzd2) =
						((BgL_approxz00_bglt) BgL_auxz00_9342), BUNSPEC);
				}
				{
					BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9350;

					{
						obj_t BgL_auxz00_9351;

						{	/* Cfa/cinfo3.scm 50 */
							BgL_objectz00_bglt BgL_tmpz00_9352;

							BgL_tmpz00_9352 =
								((BgL_objectz00_bglt)
								((BgL_vallocz00_bglt) BgL_new1335z00_5935));
							BgL_auxz00_9351 = BGL_OBJECT_WIDENING(BgL_tmpz00_9352);
						}
						BgL_auxz00_9350 =
							((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9351);
					}
					((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9350))->
							BgL_lostzd2stampzd2) = ((long) 0L), BUNSPEC);
				}
				{
					BgL_variablez00_bglt BgL_auxz00_9365;
					BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9358;

					{	/* Cfa/cinfo3.scm 50 */
						obj_t BgL_classz00_6550;

						BgL_classz00_6550 = BGl_variablez00zzast_varz00;
						{	/* Cfa/cinfo3.scm 50 */
							obj_t BgL__ortest_1117z00_6551;

							BgL__ortest_1117z00_6551 = BGL_CLASS_NIL(BgL_classz00_6550);
							if (CBOOL(BgL__ortest_1117z00_6551))
								{	/* Cfa/cinfo3.scm 50 */
									BgL_auxz00_9365 =
										((BgL_variablez00_bglt) BgL__ortest_1117z00_6551);
								}
							else
								{	/* Cfa/cinfo3.scm 50 */
									BgL_auxz00_9365 =
										((BgL_variablez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6550));
								}
						}
					}
					{
						obj_t BgL_auxz00_9359;

						{	/* Cfa/cinfo3.scm 50 */
							BgL_objectz00_bglt BgL_tmpz00_9360;

							BgL_tmpz00_9360 =
								((BgL_objectz00_bglt)
								((BgL_vallocz00_bglt) BgL_new1335z00_5935));
							BgL_auxz00_9359 = BGL_OBJECT_WIDENING(BgL_tmpz00_9360);
						}
						BgL_auxz00_9358 =
							((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9359);
					}
					((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9358))->
							BgL_ownerz00) =
						((BgL_variablez00_bglt) BgL_auxz00_9365), BUNSPEC);
				}
				{
					BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9373;

					{
						obj_t BgL_auxz00_9374;

						{	/* Cfa/cinfo3.scm 50 */
							BgL_objectz00_bglt BgL_tmpz00_9375;

							BgL_tmpz00_9375 =
								((BgL_objectz00_bglt)
								((BgL_vallocz00_bglt) BgL_new1335z00_5935));
							BgL_auxz00_9374 = BGL_OBJECT_WIDENING(BgL_tmpz00_9375);
						}
						BgL_auxz00_9373 =
							((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9374);
					}
					((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9373))->
							BgL_stackablezf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				{
					BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9381;

					{
						obj_t BgL_auxz00_9382;

						{	/* Cfa/cinfo3.scm 50 */
							BgL_objectz00_bglt BgL_tmpz00_9383;

							BgL_tmpz00_9383 =
								((BgL_objectz00_bglt)
								((BgL_vallocz00_bglt) BgL_new1335z00_5935));
							BgL_auxz00_9382 = BGL_OBJECT_WIDENING(BgL_tmpz00_9383);
						}
						BgL_auxz00_9381 =
							((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9382);
					}
					((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9381))->
							BgL_stackzd2stampzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9389;

					{
						obj_t BgL_auxz00_9390;

						{	/* Cfa/cinfo3.scm 50 */
							BgL_objectz00_bglt BgL_tmpz00_9391;

							BgL_tmpz00_9391 =
								((BgL_objectz00_bglt)
								((BgL_vallocz00_bglt) BgL_new1335z00_5935));
							BgL_auxz00_9390 = BGL_OBJECT_WIDENING(BgL_tmpz00_9391);
						}
						BgL_auxz00_9389 =
							((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9390);
					}
					((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9389))->
							BgL_seenzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				BgL_auxz00_9271 = ((BgL_vallocz00_bglt) BgL_new1335z00_5935);
				return ((obj_t) BgL_auxz00_9271);
			}
		}

	}



/* &lambda2087 */
	BgL_vallocz00_bglt BGl_z62lambda2087z62zzcfa_info3z00(obj_t BgL_envz00_5936,
		obj_t BgL_o1332z00_5937)
	{
		{	/* Cfa/cinfo3.scm 50 */
			{	/* Cfa/cinfo3.scm 50 */
				BgL_valloczf2cinfozb2optimz40_bglt BgL_wide1334z00_6553;

				BgL_wide1334z00_6553 =
					((BgL_valloczf2cinfozb2optimz40_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_valloczf2cinfozb2optimz40_bgl))));
				{	/* Cfa/cinfo3.scm 50 */
					obj_t BgL_auxz00_9404;
					BgL_objectz00_bglt BgL_tmpz00_9400;

					BgL_auxz00_9404 = ((obj_t) BgL_wide1334z00_6553);
					BgL_tmpz00_9400 =
						((BgL_objectz00_bglt)
						((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_o1332z00_5937)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9400, BgL_auxz00_9404);
				}
				((BgL_objectz00_bglt)
					((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_o1332z00_5937)));
				{	/* Cfa/cinfo3.scm 50 */
					long BgL_arg2088z00_6554;

					BgL_arg2088z00_6554 =
						BGL_CLASS_NUM(BGl_valloczf2Cinfozb2optimz40zzcfa_info3z00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_vallocz00_bglt)
								((BgL_vallocz00_bglt) BgL_o1332z00_5937))),
						BgL_arg2088z00_6554);
				}
				return
					((BgL_vallocz00_bglt)
					((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_o1332z00_5937)));
			}
		}

	}



/* &lambda2083 */
	BgL_vallocz00_bglt BGl_z62lambda2083z62zzcfa_info3z00(obj_t BgL_envz00_5938,
		obj_t BgL_loc1316z00_5939, obj_t BgL_type1317z00_5940,
		obj_t BgL_sidezd2effect1318zd2_5941, obj_t BgL_key1319z00_5942,
		obj_t BgL_exprza21320za2_5943, obj_t BgL_effect1321z00_5944,
		obj_t BgL_czd2format1322zd2_5945, obj_t BgL_ftype1323z00_5946,
		obj_t BgL_otype1324z00_5947, obj_t BgL_approx1325z00_5948,
		obj_t BgL_valuezd2approx1326zd2_5949, obj_t BgL_lostzd2stamp1327zd2_5950,
		obj_t BgL_owner1328z00_5951, obj_t BgL_stackablezf31329zf3_5952,
		obj_t BgL_stackzd2stamp1330zd2_5953, obj_t BgL_seenzf31331zf3_5954)
	{
		{	/* Cfa/cinfo3.scm 50 */
			{	/* Cfa/cinfo3.scm 50 */
				long BgL_lostzd2stamp1327zd2_6562;
				bool_t BgL_stackablezf31329zf3_6564;
				bool_t BgL_seenzf31331zf3_6565;

				BgL_lostzd2stamp1327zd2_6562 =
					(long) CINT(BgL_lostzd2stamp1327zd2_5950);
				BgL_stackablezf31329zf3_6564 = CBOOL(BgL_stackablezf31329zf3_5952);
				BgL_seenzf31331zf3_6565 = CBOOL(BgL_seenzf31331zf3_5954);
				{	/* Cfa/cinfo3.scm 50 */
					BgL_vallocz00_bglt BgL_new1444z00_6566;

					{	/* Cfa/cinfo3.scm 50 */
						BgL_vallocz00_bglt BgL_tmp1442z00_6567;
						BgL_valloczf2cinfozb2optimz40_bglt BgL_wide1443z00_6568;

						{
							BgL_vallocz00_bglt BgL_auxz00_9421;

							{	/* Cfa/cinfo3.scm 50 */
								BgL_vallocz00_bglt BgL_new1441z00_6569;

								BgL_new1441z00_6569 =
									((BgL_vallocz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_vallocz00_bgl))));
								{	/* Cfa/cinfo3.scm 50 */
									long BgL_arg2086z00_6570;

									BgL_arg2086z00_6570 =
										BGL_CLASS_NUM(BGl_vallocz00zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1441z00_6569),
										BgL_arg2086z00_6570);
								}
								{	/* Cfa/cinfo3.scm 50 */
									BgL_objectz00_bglt BgL_tmpz00_9426;

									BgL_tmpz00_9426 = ((BgL_objectz00_bglt) BgL_new1441z00_6569);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9426, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1441z00_6569);
								BgL_auxz00_9421 = BgL_new1441z00_6569;
							}
							BgL_tmp1442z00_6567 = ((BgL_vallocz00_bglt) BgL_auxz00_9421);
						}
						BgL_wide1443z00_6568 =
							((BgL_valloczf2cinfozb2optimz40_bglt)
							BOBJECT(GC_MALLOC(sizeof(struct
										BgL_valloczf2cinfozb2optimz40_bgl))));
						{	/* Cfa/cinfo3.scm 50 */
							obj_t BgL_auxz00_9434;
							BgL_objectz00_bglt BgL_tmpz00_9432;

							BgL_auxz00_9434 = ((obj_t) BgL_wide1443z00_6568);
							BgL_tmpz00_9432 = ((BgL_objectz00_bglt) BgL_tmp1442z00_6567);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9432, BgL_auxz00_9434);
						}
						((BgL_objectz00_bglt) BgL_tmp1442z00_6567);
						{	/* Cfa/cinfo3.scm 50 */
							long BgL_arg2084z00_6571;

							BgL_arg2084z00_6571 =
								BGL_CLASS_NUM(BGl_valloczf2Cinfozb2optimz40zzcfa_info3z00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1442z00_6567),
								BgL_arg2084z00_6571);
						}
						BgL_new1444z00_6566 = ((BgL_vallocz00_bglt) BgL_tmp1442z00_6567);
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_new1444z00_6566)))->BgL_locz00) =
						((obj_t) BgL_loc1316z00_5939), BUNSPEC);
					((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_new1444z00_6566)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1317z00_5940)),
						BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1444z00_6566)))->BgL_sidezd2effectzd2) =
						((obj_t) BgL_sidezd2effect1318zd2_5941), BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1444z00_6566)))->BgL_keyz00) =
						((obj_t) BgL_key1319z00_5942), BUNSPEC);
					((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
										BgL_new1444z00_6566)))->BgL_exprza2za2) =
						((obj_t) ((obj_t) BgL_exprza21320za2_5943)), BUNSPEC);
					((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
										BgL_new1444z00_6566)))->BgL_effectz00) =
						((obj_t) BgL_effect1321z00_5944), BUNSPEC);
					((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
										BgL_new1444z00_6566)))->BgL_czd2formatzd2) =
						((obj_t) ((obj_t) BgL_czd2format1322zd2_5945)), BUNSPEC);
					((((BgL_vallocz00_bglt) COBJECT(((BgL_vallocz00_bglt)
										BgL_new1444z00_6566)))->BgL_ftypez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_ftype1323z00_5946)),
						BUNSPEC);
					((((BgL_vallocz00_bglt) COBJECT(((BgL_vallocz00_bglt)
										BgL_new1444z00_6566)))->BgL_otypez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_otype1324z00_5947)),
						BUNSPEC);
					{
						BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9465;

						{
							obj_t BgL_auxz00_9466;

							{	/* Cfa/cinfo3.scm 50 */
								BgL_objectz00_bglt BgL_tmpz00_9467;

								BgL_tmpz00_9467 = ((BgL_objectz00_bglt) BgL_new1444z00_6566);
								BgL_auxz00_9466 = BGL_OBJECT_WIDENING(BgL_tmpz00_9467);
							}
							BgL_auxz00_9465 =
								((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9466);
						}
						((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9465))->
								BgL_approxz00) =
							((BgL_approxz00_bglt) ((BgL_approxz00_bglt)
									BgL_approx1325z00_5948)), BUNSPEC);
					}
					{
						BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9473;

						{
							obj_t BgL_auxz00_9474;

							{	/* Cfa/cinfo3.scm 50 */
								BgL_objectz00_bglt BgL_tmpz00_9475;

								BgL_tmpz00_9475 = ((BgL_objectz00_bglt) BgL_new1444z00_6566);
								BgL_auxz00_9474 = BGL_OBJECT_WIDENING(BgL_tmpz00_9475);
							}
							BgL_auxz00_9473 =
								((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9474);
						}
						((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9473))->
								BgL_valuezd2approxzd2) =
							((BgL_approxz00_bglt) ((BgL_approxz00_bglt)
									BgL_valuezd2approx1326zd2_5949)), BUNSPEC);
					}
					{
						BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9481;

						{
							obj_t BgL_auxz00_9482;

							{	/* Cfa/cinfo3.scm 50 */
								BgL_objectz00_bglt BgL_tmpz00_9483;

								BgL_tmpz00_9483 = ((BgL_objectz00_bglt) BgL_new1444z00_6566);
								BgL_auxz00_9482 = BGL_OBJECT_WIDENING(BgL_tmpz00_9483);
							}
							BgL_auxz00_9481 =
								((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9482);
						}
						((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9481))->
								BgL_lostzd2stampzd2) =
							((long) BgL_lostzd2stamp1327zd2_6562), BUNSPEC);
					}
					{
						BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9488;

						{
							obj_t BgL_auxz00_9489;

							{	/* Cfa/cinfo3.scm 50 */
								BgL_objectz00_bglt BgL_tmpz00_9490;

								BgL_tmpz00_9490 = ((BgL_objectz00_bglt) BgL_new1444z00_6566);
								BgL_auxz00_9489 = BGL_OBJECT_WIDENING(BgL_tmpz00_9490);
							}
							BgL_auxz00_9488 =
								((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9489);
						}
						((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9488))->
								BgL_ownerz00) =
							((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
									BgL_owner1328z00_5951)), BUNSPEC);
					}
					{
						BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9496;

						{
							obj_t BgL_auxz00_9497;

							{	/* Cfa/cinfo3.scm 50 */
								BgL_objectz00_bglt BgL_tmpz00_9498;

								BgL_tmpz00_9498 = ((BgL_objectz00_bglt) BgL_new1444z00_6566);
								BgL_auxz00_9497 = BGL_OBJECT_WIDENING(BgL_tmpz00_9498);
							}
							BgL_auxz00_9496 =
								((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9497);
						}
						((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9496))->
								BgL_stackablezf3zf3) =
							((bool_t) BgL_stackablezf31329zf3_6564), BUNSPEC);
					}
					{
						BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9503;

						{
							obj_t BgL_auxz00_9504;

							{	/* Cfa/cinfo3.scm 50 */
								BgL_objectz00_bglt BgL_tmpz00_9505;

								BgL_tmpz00_9505 = ((BgL_objectz00_bglt) BgL_new1444z00_6566);
								BgL_auxz00_9504 = BGL_OBJECT_WIDENING(BgL_tmpz00_9505);
							}
							BgL_auxz00_9503 =
								((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9504);
						}
						((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9503))->
								BgL_stackzd2stampzd2) =
							((obj_t) BgL_stackzd2stamp1330zd2_5953), BUNSPEC);
					}
					{
						BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9510;

						{
							obj_t BgL_auxz00_9511;

							{	/* Cfa/cinfo3.scm 50 */
								BgL_objectz00_bglt BgL_tmpz00_9512;

								BgL_tmpz00_9512 = ((BgL_objectz00_bglt) BgL_new1444z00_6566);
								BgL_auxz00_9511 = BGL_OBJECT_WIDENING(BgL_tmpz00_9512);
							}
							BgL_auxz00_9510 =
								((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9511);
						}
						((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9510))->
								BgL_seenzf3zf3) = ((bool_t) BgL_seenzf31331zf3_6565), BUNSPEC);
					}
					return BgL_new1444z00_6566;
				}
			}
		}

	}



/* &<@anonymous:2137> */
	obj_t BGl_z62zc3z04anonymousza32137ze3ze5zzcfa_info3z00(obj_t BgL_envz00_5955)
	{
		{	/* Cfa/cinfo3.scm 50 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda2136 */
	obj_t BGl_z62lambda2136z62zzcfa_info3z00(obj_t BgL_envz00_5956,
		obj_t BgL_oz00_5957, obj_t BgL_vz00_5958)
	{
		{	/* Cfa/cinfo3.scm 50 */
			{	/* Cfa/cinfo3.scm 50 */
				bool_t BgL_vz00_6573;

				BgL_vz00_6573 = CBOOL(BgL_vz00_5958);
				{
					BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9519;

					{
						obj_t BgL_auxz00_9520;

						{	/* Cfa/cinfo3.scm 50 */
							BgL_objectz00_bglt BgL_tmpz00_9521;

							BgL_tmpz00_9521 =
								((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_oz00_5957));
							BgL_auxz00_9520 = BGL_OBJECT_WIDENING(BgL_tmpz00_9521);
						}
						BgL_auxz00_9519 =
							((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9520);
					}
					return
						((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9519))->
							BgL_seenzf3zf3) = ((bool_t) BgL_vz00_6573), BUNSPEC);
				}
			}
		}

	}



/* &lambda2135 */
	obj_t BGl_z62lambda2135z62zzcfa_info3z00(obj_t BgL_envz00_5959,
		obj_t BgL_oz00_5960)
	{
		{	/* Cfa/cinfo3.scm 50 */
			{	/* Cfa/cinfo3.scm 50 */
				bool_t BgL_tmpz00_9527;

				{
					BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9528;

					{
						obj_t BgL_auxz00_9529;

						{	/* Cfa/cinfo3.scm 50 */
							BgL_objectz00_bglt BgL_tmpz00_9530;

							BgL_tmpz00_9530 =
								((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_oz00_5960));
							BgL_auxz00_9529 = BGL_OBJECT_WIDENING(BgL_tmpz00_9530);
						}
						BgL_auxz00_9528 =
							((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9529);
					}
					BgL_tmpz00_9527 =
						(((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9528))->
						BgL_seenzf3zf3);
				}
				return BBOOL(BgL_tmpz00_9527);
			}
		}

	}



/* &<@anonymous:2130> */
	obj_t BGl_z62zc3z04anonymousza32130ze3ze5zzcfa_info3z00(obj_t BgL_envz00_5961)
	{
		{	/* Cfa/cinfo3.scm 50 */
			return BNIL;
		}

	}



/* &lambda2129 */
	obj_t BGl_z62lambda2129z62zzcfa_info3z00(obj_t BgL_envz00_5962,
		obj_t BgL_oz00_5963, obj_t BgL_vz00_5964)
	{
		{	/* Cfa/cinfo3.scm 50 */
			{
				BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9537;

				{
					obj_t BgL_auxz00_9538;

					{	/* Cfa/cinfo3.scm 50 */
						BgL_objectz00_bglt BgL_tmpz00_9539;

						BgL_tmpz00_9539 =
							((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_oz00_5963));
						BgL_auxz00_9538 = BGL_OBJECT_WIDENING(BgL_tmpz00_9539);
					}
					BgL_auxz00_9537 =
						((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9538);
				}
				return
					((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9537))->
						BgL_stackzd2stampzd2) = ((obj_t) BgL_vz00_5964), BUNSPEC);
			}
		}

	}



/* &lambda2128 */
	obj_t BGl_z62lambda2128z62zzcfa_info3z00(obj_t BgL_envz00_5965,
		obj_t BgL_oz00_5966)
	{
		{	/* Cfa/cinfo3.scm 50 */
			{
				BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9545;

				{
					obj_t BgL_auxz00_9546;

					{	/* Cfa/cinfo3.scm 50 */
						BgL_objectz00_bglt BgL_tmpz00_9547;

						BgL_tmpz00_9547 =
							((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_oz00_5966));
						BgL_auxz00_9546 = BGL_OBJECT_WIDENING(BgL_tmpz00_9547);
					}
					BgL_auxz00_9545 =
						((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9546);
				}
				return
					(((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9545))->
					BgL_stackzd2stampzd2);
			}
		}

	}



/* &<@anonymous:2123> */
	obj_t BGl_z62zc3z04anonymousza32123ze3ze5zzcfa_info3z00(obj_t BgL_envz00_5967)
	{
		{	/* Cfa/cinfo3.scm 50 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &lambda2122 */
	obj_t BGl_z62lambda2122z62zzcfa_info3z00(obj_t BgL_envz00_5968,
		obj_t BgL_oz00_5969, obj_t BgL_vz00_5970)
	{
		{	/* Cfa/cinfo3.scm 50 */
			{	/* Cfa/cinfo3.scm 50 */
				bool_t BgL_vz00_6578;

				BgL_vz00_6578 = CBOOL(BgL_vz00_5970);
				{
					BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9555;

					{
						obj_t BgL_auxz00_9556;

						{	/* Cfa/cinfo3.scm 50 */
							BgL_objectz00_bglt BgL_tmpz00_9557;

							BgL_tmpz00_9557 =
								((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_oz00_5969));
							BgL_auxz00_9556 = BGL_OBJECT_WIDENING(BgL_tmpz00_9557);
						}
						BgL_auxz00_9555 =
							((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9556);
					}
					return
						((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9555))->
							BgL_stackablezf3zf3) = ((bool_t) BgL_vz00_6578), BUNSPEC);
				}
			}
		}

	}



/* &lambda2121 */
	obj_t BGl_z62lambda2121z62zzcfa_info3z00(obj_t BgL_envz00_5971,
		obj_t BgL_oz00_5972)
	{
		{	/* Cfa/cinfo3.scm 50 */
			{	/* Cfa/cinfo3.scm 50 */
				bool_t BgL_tmpz00_9563;

				{
					BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9564;

					{
						obj_t BgL_auxz00_9565;

						{	/* Cfa/cinfo3.scm 50 */
							BgL_objectz00_bglt BgL_tmpz00_9566;

							BgL_tmpz00_9566 =
								((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_oz00_5972));
							BgL_auxz00_9565 = BGL_OBJECT_WIDENING(BgL_tmpz00_9566);
						}
						BgL_auxz00_9564 =
							((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9565);
					}
					BgL_tmpz00_9563 =
						(((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9564))->
						BgL_stackablezf3zf3);
				}
				return BBOOL(BgL_tmpz00_9563);
			}
		}

	}



/* &lambda2116 */
	obj_t BGl_z62lambda2116z62zzcfa_info3z00(obj_t BgL_envz00_5973,
		obj_t BgL_oz00_5974, obj_t BgL_vz00_5975)
	{
		{	/* Cfa/cinfo3.scm 50 */
			{
				BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9573;

				{
					obj_t BgL_auxz00_9574;

					{	/* Cfa/cinfo3.scm 50 */
						BgL_objectz00_bglt BgL_tmpz00_9575;

						BgL_tmpz00_9575 =
							((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_oz00_5974));
						BgL_auxz00_9574 = BGL_OBJECT_WIDENING(BgL_tmpz00_9575);
					}
					BgL_auxz00_9573 =
						((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9574);
				}
				return
					((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9573))->
						BgL_ownerz00) =
					((BgL_variablez00_bglt) ((BgL_variablez00_bglt) BgL_vz00_5975)),
					BUNSPEC);
			}
		}

	}



/* &lambda2115 */
	BgL_variablez00_bglt BGl_z62lambda2115z62zzcfa_info3z00(obj_t BgL_envz00_5976,
		obj_t BgL_oz00_5977)
	{
		{	/* Cfa/cinfo3.scm 50 */
			{
				BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9582;

				{
					obj_t BgL_auxz00_9583;

					{	/* Cfa/cinfo3.scm 50 */
						BgL_objectz00_bglt BgL_tmpz00_9584;

						BgL_tmpz00_9584 =
							((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_oz00_5977));
						BgL_auxz00_9583 = BGL_OBJECT_WIDENING(BgL_tmpz00_9584);
					}
					BgL_auxz00_9582 =
						((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9583);
				}
				return
					(((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9582))->
					BgL_ownerz00);
			}
		}

	}



/* &<@anonymous:2111> */
	obj_t BGl_z62zc3z04anonymousza32111ze3ze5zzcfa_info3z00(obj_t BgL_envz00_5978)
	{
		{	/* Cfa/cinfo3.scm 50 */
			return BINT(-1L);
		}

	}



/* &lambda2110 */
	obj_t BGl_z62lambda2110z62zzcfa_info3z00(obj_t BgL_envz00_5979,
		obj_t BgL_oz00_5980, obj_t BgL_vz00_5981)
	{
		{	/* Cfa/cinfo3.scm 50 */
			{	/* Cfa/cinfo3.scm 50 */
				long BgL_vz00_6584;

				BgL_vz00_6584 = (long) CINT(BgL_vz00_5981);
				{
					BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9592;

					{
						obj_t BgL_auxz00_9593;

						{	/* Cfa/cinfo3.scm 50 */
							BgL_objectz00_bglt BgL_tmpz00_9594;

							BgL_tmpz00_9594 =
								((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_oz00_5980));
							BgL_auxz00_9593 = BGL_OBJECT_WIDENING(BgL_tmpz00_9594);
						}
						BgL_auxz00_9592 =
							((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9593);
					}
					return
						((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9592))->
							BgL_lostzd2stampzd2) = ((long) BgL_vz00_6584), BUNSPEC);
		}}}

	}



/* &lambda2109 */
	obj_t BGl_z62lambda2109z62zzcfa_info3z00(obj_t BgL_envz00_5982,
		obj_t BgL_oz00_5983)
	{
		{	/* Cfa/cinfo3.scm 50 */
			{	/* Cfa/cinfo3.scm 50 */
				long BgL_tmpz00_9600;

				{
					BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9601;

					{
						obj_t BgL_auxz00_9602;

						{	/* Cfa/cinfo3.scm 50 */
							BgL_objectz00_bglt BgL_tmpz00_9603;

							BgL_tmpz00_9603 =
								((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_oz00_5983));
							BgL_auxz00_9602 = BGL_OBJECT_WIDENING(BgL_tmpz00_9603);
						}
						BgL_auxz00_9601 =
							((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9602);
					}
					BgL_tmpz00_9600 =
						(((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9601))->
						BgL_lostzd2stampzd2);
				}
				return BINT(BgL_tmpz00_9600);
			}
		}

	}



/* &lambda2104 */
	obj_t BGl_z62lambda2104z62zzcfa_info3z00(obj_t BgL_envz00_5984,
		obj_t BgL_oz00_5985, obj_t BgL_vz00_5986)
	{
		{	/* Cfa/cinfo3.scm 50 */
			{
				BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9610;

				{
					obj_t BgL_auxz00_9611;

					{	/* Cfa/cinfo3.scm 50 */
						BgL_objectz00_bglt BgL_tmpz00_9612;

						BgL_tmpz00_9612 =
							((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_oz00_5985));
						BgL_auxz00_9611 = BGL_OBJECT_WIDENING(BgL_tmpz00_9612);
					}
					BgL_auxz00_9610 =
						((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9611);
				}
				return
					((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9610))->
						BgL_valuezd2approxzd2) =
					((BgL_approxz00_bglt) ((BgL_approxz00_bglt) BgL_vz00_5986)), BUNSPEC);
			}
		}

	}



/* &lambda2103 */
	BgL_approxz00_bglt BGl_z62lambda2103z62zzcfa_info3z00(obj_t BgL_envz00_5987,
		obj_t BgL_oz00_5988)
	{
		{	/* Cfa/cinfo3.scm 50 */
			{
				BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9619;

				{
					obj_t BgL_auxz00_9620;

					{	/* Cfa/cinfo3.scm 50 */
						BgL_objectz00_bglt BgL_tmpz00_9621;

						BgL_tmpz00_9621 =
							((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_oz00_5988));
						BgL_auxz00_9620 = BGL_OBJECT_WIDENING(BgL_tmpz00_9621);
					}
					BgL_auxz00_9619 =
						((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9620);
				}
				return
					(((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9619))->
					BgL_valuezd2approxzd2);
			}
		}

	}



/* &lambda2099 */
	obj_t BGl_z62lambda2099z62zzcfa_info3z00(obj_t BgL_envz00_5989,
		obj_t BgL_oz00_5990, obj_t BgL_vz00_5991)
	{
		{	/* Cfa/cinfo3.scm 50 */
			{
				BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9627;

				{
					obj_t BgL_auxz00_9628;

					{	/* Cfa/cinfo3.scm 50 */
						BgL_objectz00_bglt BgL_tmpz00_9629;

						BgL_tmpz00_9629 =
							((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_oz00_5990));
						BgL_auxz00_9628 = BGL_OBJECT_WIDENING(BgL_tmpz00_9629);
					}
					BgL_auxz00_9627 =
						((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9628);
				}
				return
					((((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9627))->
						BgL_approxz00) =
					((BgL_approxz00_bglt) ((BgL_approxz00_bglt) BgL_vz00_5991)), BUNSPEC);
			}
		}

	}



/* &lambda2098 */
	BgL_approxz00_bglt BGl_z62lambda2098z62zzcfa_info3z00(obj_t BgL_envz00_5992,
		obj_t BgL_oz00_5993)
	{
		{	/* Cfa/cinfo3.scm 50 */
			{
				BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_9636;

				{
					obj_t BgL_auxz00_9637;

					{	/* Cfa/cinfo3.scm 50 */
						BgL_objectz00_bglt BgL_tmpz00_9638;

						BgL_tmpz00_9638 =
							((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_oz00_5993));
						BgL_auxz00_9637 = BGL_OBJECT_WIDENING(BgL_tmpz00_9638);
					}
					BgL_auxz00_9636 =
						((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_9637);
				}
				return
					(((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_9636))->
					BgL_approxz00);
			}
		}

	}



/* &lambda2066 */
	BgL_vallocz00_bglt BGl_z62lambda2066z62zzcfa_info3z00(obj_t BgL_envz00_5994,
		obj_t BgL_o1314z00_5995)
	{
		{	/* Cfa/cinfo3.scm 48 */
			{	/* Cfa/cinfo3.scm 48 */
				long BgL_arg2067z00_6593;

				{	/* Cfa/cinfo3.scm 48 */
					obj_t BgL_arg2068z00_6594;

					{	/* Cfa/cinfo3.scm 48 */
						obj_t BgL_arg2069z00_6595;

						{	/* Cfa/cinfo3.scm 48 */
							obj_t BgL_arg1815z00_6596;
							long BgL_arg1816z00_6597;

							BgL_arg1815z00_6596 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Cfa/cinfo3.scm 48 */
								long BgL_arg1817z00_6598;

								BgL_arg1817z00_6598 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_vallocz00_bglt) BgL_o1314z00_5995)));
								BgL_arg1816z00_6597 = (BgL_arg1817z00_6598 - OBJECT_TYPE);
							}
							BgL_arg2069z00_6595 =
								VECTOR_REF(BgL_arg1815z00_6596, BgL_arg1816z00_6597);
						}
						BgL_arg2068z00_6594 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2069z00_6595);
					}
					{	/* Cfa/cinfo3.scm 48 */
						obj_t BgL_tmpz00_9651;

						BgL_tmpz00_9651 = ((obj_t) BgL_arg2068z00_6594);
						BgL_arg2067z00_6593 = BGL_CLASS_NUM(BgL_tmpz00_9651);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_vallocz00_bglt) BgL_o1314z00_5995)), BgL_arg2067z00_6593);
			}
			{	/* Cfa/cinfo3.scm 48 */
				BgL_objectz00_bglt BgL_tmpz00_9657;

				BgL_tmpz00_9657 =
					((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_o1314z00_5995));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9657, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_o1314z00_5995));
			return ((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_o1314z00_5995));
		}

	}



/* &<@anonymous:2065> */
	obj_t BGl_z62zc3z04anonymousza32065ze3ze5zzcfa_info3z00(obj_t BgL_envz00_5996,
		obj_t BgL_new1313z00_5997)
	{
		{	/* Cfa/cinfo3.scm 48 */
			{
				BgL_vallocz00_bglt BgL_auxz00_9665;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_vallocz00_bglt) BgL_new1313z00_5997))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_9669;

					{	/* Cfa/cinfo3.scm 48 */
						obj_t BgL_classz00_6600;

						BgL_classz00_6600 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 48 */
							obj_t BgL__ortest_1117z00_6601;

							BgL__ortest_1117z00_6601 = BGL_CLASS_NIL(BgL_classz00_6600);
							if (CBOOL(BgL__ortest_1117z00_6601))
								{	/* Cfa/cinfo3.scm 48 */
									BgL_auxz00_9669 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6601);
								}
							else
								{	/* Cfa/cinfo3.scm 48 */
									BgL_auxz00_9669 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6600));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_vallocz00_bglt) BgL_new1313z00_5997))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_9669), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_vallocz00_bglt) BgL_new1313z00_5997))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_vallocz00_bglt)
										BgL_new1313z00_5997))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_vallocz00_bglt)
										BgL_new1313z00_5997))))->BgL_exprza2za2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_vallocz00_bglt)
										BgL_new1313z00_5997))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_privatez00_bglt)
							COBJECT(((BgL_privatez00_bglt) ((BgL_vallocz00_bglt)
										BgL_new1313z00_5997))))->BgL_czd2formatzd2) =
					((obj_t) BGl_string2364z00zzcfa_info3z00), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_9694;

					{	/* Cfa/cinfo3.scm 48 */
						obj_t BgL_classz00_6602;

						BgL_classz00_6602 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 48 */
							obj_t BgL__ortest_1117z00_6603;

							BgL__ortest_1117z00_6603 = BGL_CLASS_NIL(BgL_classz00_6602);
							if (CBOOL(BgL__ortest_1117z00_6603))
								{	/* Cfa/cinfo3.scm 48 */
									BgL_auxz00_9694 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6603);
								}
							else
								{	/* Cfa/cinfo3.scm 48 */
									BgL_auxz00_9694 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6602));
								}
						}
					}
					((((BgL_vallocz00_bglt) COBJECT(
									((BgL_vallocz00_bglt)
										((BgL_vallocz00_bglt) BgL_new1313z00_5997))))->
							BgL_ftypez00) = ((BgL_typez00_bglt) BgL_auxz00_9694), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_9704;

					{	/* Cfa/cinfo3.scm 48 */
						obj_t BgL_classz00_6604;

						BgL_classz00_6604 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 48 */
							obj_t BgL__ortest_1117z00_6605;

							BgL__ortest_1117z00_6605 = BGL_CLASS_NIL(BgL_classz00_6604);
							if (CBOOL(BgL__ortest_1117z00_6605))
								{	/* Cfa/cinfo3.scm 48 */
									BgL_auxz00_9704 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6605);
								}
							else
								{	/* Cfa/cinfo3.scm 48 */
									BgL_auxz00_9704 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6604));
								}
						}
					}
					((((BgL_vallocz00_bglt) COBJECT(
									((BgL_vallocz00_bglt)
										((BgL_vallocz00_bglt) BgL_new1313z00_5997))))->
							BgL_otypez00) = ((BgL_typez00_bglt) BgL_auxz00_9704), BUNSPEC);
				}
				{
					BgL_approxz00_bglt BgL_auxz00_9721;
					BgL_valloczf2cinfozf2_bglt BgL_auxz00_9714;

					{	/* Cfa/cinfo3.scm 48 */
						obj_t BgL_classz00_6606;

						BgL_classz00_6606 = BGl_approxz00zzcfa_infoz00;
						{	/* Cfa/cinfo3.scm 48 */
							obj_t BgL__ortest_1117z00_6607;

							BgL__ortest_1117z00_6607 = BGL_CLASS_NIL(BgL_classz00_6606);
							if (CBOOL(BgL__ortest_1117z00_6607))
								{	/* Cfa/cinfo3.scm 48 */
									BgL_auxz00_9721 =
										((BgL_approxz00_bglt) BgL__ortest_1117z00_6607);
								}
							else
								{	/* Cfa/cinfo3.scm 48 */
									BgL_auxz00_9721 =
										((BgL_approxz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6606));
								}
						}
					}
					{
						obj_t BgL_auxz00_9715;

						{	/* Cfa/cinfo3.scm 48 */
							BgL_objectz00_bglt BgL_tmpz00_9716;

							BgL_tmpz00_9716 =
								((BgL_objectz00_bglt)
								((BgL_vallocz00_bglt) BgL_new1313z00_5997));
							BgL_auxz00_9715 = BGL_OBJECT_WIDENING(BgL_tmpz00_9716);
						}
						BgL_auxz00_9714 = ((BgL_valloczf2cinfozf2_bglt) BgL_auxz00_9715);
					}
					((((BgL_valloczf2cinfozf2_bglt) COBJECT(BgL_auxz00_9714))->
							BgL_approxz00) = ((BgL_approxz00_bglt) BgL_auxz00_9721), BUNSPEC);
				}
				BgL_auxz00_9665 = ((BgL_vallocz00_bglt) BgL_new1313z00_5997);
				return ((obj_t) BgL_auxz00_9665);
			}
		}

	}



/* &lambda2063 */
	BgL_vallocz00_bglt BGl_z62lambda2063z62zzcfa_info3z00(obj_t BgL_envz00_5998,
		obj_t BgL_o1310z00_5999)
	{
		{	/* Cfa/cinfo3.scm 48 */
			{	/* Cfa/cinfo3.scm 48 */
				BgL_valloczf2cinfozf2_bglt BgL_wide1312z00_6609;

				BgL_wide1312z00_6609 =
					((BgL_valloczf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_valloczf2cinfozf2_bgl))));
				{	/* Cfa/cinfo3.scm 48 */
					obj_t BgL_auxz00_9736;
					BgL_objectz00_bglt BgL_tmpz00_9732;

					BgL_auxz00_9736 = ((obj_t) BgL_wide1312z00_6609);
					BgL_tmpz00_9732 =
						((BgL_objectz00_bglt)
						((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_o1310z00_5999)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9732, BgL_auxz00_9736);
				}
				((BgL_objectz00_bglt)
					((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_o1310z00_5999)));
				{	/* Cfa/cinfo3.scm 48 */
					long BgL_arg2064z00_6610;

					BgL_arg2064z00_6610 =
						BGL_CLASS_NUM(BGl_valloczf2Cinfozf2zzcfa_info3z00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_vallocz00_bglt)
								((BgL_vallocz00_bglt) BgL_o1310z00_5999))),
						BgL_arg2064z00_6610);
				}
				return
					((BgL_vallocz00_bglt)
					((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_o1310z00_5999)));
			}
		}

	}



/* &lambda2060 */
	BgL_vallocz00_bglt BGl_z62lambda2060z62zzcfa_info3z00(obj_t BgL_envz00_6000,
		obj_t BgL_loc1300z00_6001, obj_t BgL_type1301z00_6002,
		obj_t BgL_sidezd2effect1302zd2_6003, obj_t BgL_key1303z00_6004,
		obj_t BgL_exprza21304za2_6005, obj_t BgL_effect1305z00_6006,
		obj_t BgL_czd2format1306zd2_6007, obj_t BgL_ftype1307z00_6008,
		obj_t BgL_otype1308z00_6009, obj_t BgL_approx1309z00_6010)
	{
		{	/* Cfa/cinfo3.scm 48 */
			{	/* Cfa/cinfo3.scm 48 */
				BgL_vallocz00_bglt BgL_new1439z00_6617;

				{	/* Cfa/cinfo3.scm 48 */
					BgL_vallocz00_bglt BgL_tmp1437z00_6618;
					BgL_valloczf2cinfozf2_bglt BgL_wide1438z00_6619;

					{
						BgL_vallocz00_bglt BgL_auxz00_9750;

						{	/* Cfa/cinfo3.scm 48 */
							BgL_vallocz00_bglt BgL_new1436z00_6620;

							BgL_new1436z00_6620 =
								((BgL_vallocz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_vallocz00_bgl))));
							{	/* Cfa/cinfo3.scm 48 */
								long BgL_arg2062z00_6621;

								BgL_arg2062z00_6621 = BGL_CLASS_NUM(BGl_vallocz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1436z00_6620),
									BgL_arg2062z00_6621);
							}
							{	/* Cfa/cinfo3.scm 48 */
								BgL_objectz00_bglt BgL_tmpz00_9755;

								BgL_tmpz00_9755 = ((BgL_objectz00_bglt) BgL_new1436z00_6620);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9755, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1436z00_6620);
							BgL_auxz00_9750 = BgL_new1436z00_6620;
						}
						BgL_tmp1437z00_6618 = ((BgL_vallocz00_bglt) BgL_auxz00_9750);
					}
					BgL_wide1438z00_6619 =
						((BgL_valloczf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_valloczf2cinfozf2_bgl))));
					{	/* Cfa/cinfo3.scm 48 */
						obj_t BgL_auxz00_9763;
						BgL_objectz00_bglt BgL_tmpz00_9761;

						BgL_auxz00_9763 = ((obj_t) BgL_wide1438z00_6619);
						BgL_tmpz00_9761 = ((BgL_objectz00_bglt) BgL_tmp1437z00_6618);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9761, BgL_auxz00_9763);
					}
					((BgL_objectz00_bglt) BgL_tmp1437z00_6618);
					{	/* Cfa/cinfo3.scm 48 */
						long BgL_arg2061z00_6622;

						BgL_arg2061z00_6622 =
							BGL_CLASS_NUM(BGl_valloczf2Cinfozf2zzcfa_info3z00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1437z00_6618), BgL_arg2061z00_6622);
					}
					BgL_new1439z00_6617 = ((BgL_vallocz00_bglt) BgL_tmp1437z00_6618);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1439z00_6617)))->BgL_locz00) =
					((obj_t) BgL_loc1300z00_6001), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1439z00_6617)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1301z00_6002)),
					BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1439z00_6617)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1302zd2_6003), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1439z00_6617)))->BgL_keyz00) =
					((obj_t) BgL_key1303z00_6004), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1439z00_6617)))->BgL_exprza2za2) =
					((obj_t) ((obj_t) BgL_exprza21304za2_6005)), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1439z00_6617)))->BgL_effectz00) =
					((obj_t) BgL_effect1305z00_6006), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1439z00_6617)))->BgL_czd2formatzd2) =
					((obj_t) ((obj_t) BgL_czd2format1306zd2_6007)), BUNSPEC);
				((((BgL_vallocz00_bglt) COBJECT(((BgL_vallocz00_bglt)
									BgL_new1439z00_6617)))->BgL_ftypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_ftype1307z00_6008)),
					BUNSPEC);
				((((BgL_vallocz00_bglt) COBJECT(((BgL_vallocz00_bglt)
									BgL_new1439z00_6617)))->BgL_otypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_otype1308z00_6009)),
					BUNSPEC);
				{
					BgL_valloczf2cinfozf2_bglt BgL_auxz00_9794;

					{
						obj_t BgL_auxz00_9795;

						{	/* Cfa/cinfo3.scm 48 */
							BgL_objectz00_bglt BgL_tmpz00_9796;

							BgL_tmpz00_9796 = ((BgL_objectz00_bglt) BgL_new1439z00_6617);
							BgL_auxz00_9795 = BGL_OBJECT_WIDENING(BgL_tmpz00_9796);
						}
						BgL_auxz00_9794 = ((BgL_valloczf2cinfozf2_bglt) BgL_auxz00_9795);
					}
					((((BgL_valloczf2cinfozf2_bglt) COBJECT(BgL_auxz00_9794))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) ((BgL_approxz00_bglt)
								BgL_approx1309z00_6010)), BUNSPEC);
				}
				return BgL_new1439z00_6617;
			}
		}

	}



/* &lambda2076 */
	obj_t BGl_z62lambda2076z62zzcfa_info3z00(obj_t BgL_envz00_6011,
		obj_t BgL_oz00_6012, obj_t BgL_vz00_6013)
	{
		{	/* Cfa/cinfo3.scm 48 */
			{
				BgL_valloczf2cinfozf2_bglt BgL_auxz00_9802;

				{
					obj_t BgL_auxz00_9803;

					{	/* Cfa/cinfo3.scm 48 */
						BgL_objectz00_bglt BgL_tmpz00_9804;

						BgL_tmpz00_9804 =
							((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_oz00_6012));
						BgL_auxz00_9803 = BGL_OBJECT_WIDENING(BgL_tmpz00_9804);
					}
					BgL_auxz00_9802 = ((BgL_valloczf2cinfozf2_bglt) BgL_auxz00_9803);
				}
				return
					((((BgL_valloczf2cinfozf2_bglt) COBJECT(BgL_auxz00_9802))->
						BgL_approxz00) =
					((BgL_approxz00_bglt) ((BgL_approxz00_bglt) BgL_vz00_6013)), BUNSPEC);
			}
		}

	}



/* &lambda2075 */
	BgL_approxz00_bglt BGl_z62lambda2075z62zzcfa_info3z00(obj_t BgL_envz00_6014,
		obj_t BgL_oz00_6015)
	{
		{	/* Cfa/cinfo3.scm 48 */
			{
				BgL_valloczf2cinfozf2_bglt BgL_auxz00_9811;

				{
					obj_t BgL_auxz00_9812;

					{	/* Cfa/cinfo3.scm 48 */
						BgL_objectz00_bglt BgL_tmpz00_9813;

						BgL_tmpz00_9813 =
							((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_oz00_6015));
						BgL_auxz00_9812 = BGL_OBJECT_WIDENING(BgL_tmpz00_9813);
					}
					BgL_auxz00_9811 = ((BgL_valloczf2cinfozf2_bglt) BgL_auxz00_9812);
				}
				return
					(((BgL_valloczf2cinfozf2_bglt) COBJECT(BgL_auxz00_9811))->
					BgL_approxz00);
			}
		}

	}



/* &lambda2043 */
	BgL_vallocz00_bglt BGl_z62lambda2043z62zzcfa_info3z00(obj_t BgL_envz00_6016,
		obj_t BgL_o1298z00_6017)
	{
		{	/* Cfa/cinfo3.scm 46 */
			{	/* Cfa/cinfo3.scm 46 */
				long BgL_arg2044z00_6627;

				{	/* Cfa/cinfo3.scm 46 */
					obj_t BgL_arg2045z00_6628;

					{	/* Cfa/cinfo3.scm 46 */
						obj_t BgL_arg2046z00_6629;

						{	/* Cfa/cinfo3.scm 46 */
							obj_t BgL_arg1815z00_6630;
							long BgL_arg1816z00_6631;

							BgL_arg1815z00_6630 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Cfa/cinfo3.scm 46 */
								long BgL_arg1817z00_6632;

								BgL_arg1817z00_6632 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_vallocz00_bglt) BgL_o1298z00_6017)));
								BgL_arg1816z00_6631 = (BgL_arg1817z00_6632 - OBJECT_TYPE);
							}
							BgL_arg2046z00_6629 =
								VECTOR_REF(BgL_arg1815z00_6630, BgL_arg1816z00_6631);
						}
						BgL_arg2045z00_6628 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2046z00_6629);
					}
					{	/* Cfa/cinfo3.scm 46 */
						obj_t BgL_tmpz00_9826;

						BgL_tmpz00_9826 = ((obj_t) BgL_arg2045z00_6628);
						BgL_arg2044z00_6627 = BGL_CLASS_NUM(BgL_tmpz00_9826);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_vallocz00_bglt) BgL_o1298z00_6017)), BgL_arg2044z00_6627);
			}
			{	/* Cfa/cinfo3.scm 46 */
				BgL_objectz00_bglt BgL_tmpz00_9832;

				BgL_tmpz00_9832 =
					((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_o1298z00_6017));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9832, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_o1298z00_6017));
			return ((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_o1298z00_6017));
		}

	}



/* &<@anonymous:2042> */
	obj_t BGl_z62zc3z04anonymousza32042ze3ze5zzcfa_info3z00(obj_t BgL_envz00_6018,
		obj_t BgL_new1297z00_6019)
	{
		{	/* Cfa/cinfo3.scm 46 */
			{
				BgL_vallocz00_bglt BgL_auxz00_9840;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_vallocz00_bglt) BgL_new1297z00_6019))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_9844;

					{	/* Cfa/cinfo3.scm 46 */
						obj_t BgL_classz00_6634;

						BgL_classz00_6634 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 46 */
							obj_t BgL__ortest_1117z00_6635;

							BgL__ortest_1117z00_6635 = BGL_CLASS_NIL(BgL_classz00_6634);
							if (CBOOL(BgL__ortest_1117z00_6635))
								{	/* Cfa/cinfo3.scm 46 */
									BgL_auxz00_9844 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6635);
								}
							else
								{	/* Cfa/cinfo3.scm 46 */
									BgL_auxz00_9844 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6634));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_vallocz00_bglt) BgL_new1297z00_6019))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_9844), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_vallocz00_bglt) BgL_new1297z00_6019))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_vallocz00_bglt)
										BgL_new1297z00_6019))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_vallocz00_bglt)
										BgL_new1297z00_6019))))->BgL_exprza2za2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_vallocz00_bglt)
										BgL_new1297z00_6019))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_privatez00_bglt)
							COBJECT(((BgL_privatez00_bglt) ((BgL_vallocz00_bglt)
										BgL_new1297z00_6019))))->BgL_czd2formatzd2) =
					((obj_t) BGl_string2364z00zzcfa_info3z00), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_9869;

					{	/* Cfa/cinfo3.scm 46 */
						obj_t BgL_classz00_6636;

						BgL_classz00_6636 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 46 */
							obj_t BgL__ortest_1117z00_6637;

							BgL__ortest_1117z00_6637 = BGL_CLASS_NIL(BgL_classz00_6636);
							if (CBOOL(BgL__ortest_1117z00_6637))
								{	/* Cfa/cinfo3.scm 46 */
									BgL_auxz00_9869 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6637);
								}
							else
								{	/* Cfa/cinfo3.scm 46 */
									BgL_auxz00_9869 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6636));
								}
						}
					}
					((((BgL_vallocz00_bglt) COBJECT(
									((BgL_vallocz00_bglt)
										((BgL_vallocz00_bglt) BgL_new1297z00_6019))))->
							BgL_ftypez00) = ((BgL_typez00_bglt) BgL_auxz00_9869), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_9879;

					{	/* Cfa/cinfo3.scm 46 */
						obj_t BgL_classz00_6638;

						BgL_classz00_6638 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 46 */
							obj_t BgL__ortest_1117z00_6639;

							BgL__ortest_1117z00_6639 = BGL_CLASS_NIL(BgL_classz00_6638);
							if (CBOOL(BgL__ortest_1117z00_6639))
								{	/* Cfa/cinfo3.scm 46 */
									BgL_auxz00_9879 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6639);
								}
							else
								{	/* Cfa/cinfo3.scm 46 */
									BgL_auxz00_9879 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6638));
								}
						}
					}
					((((BgL_vallocz00_bglt) COBJECT(
									((BgL_vallocz00_bglt)
										((BgL_vallocz00_bglt) BgL_new1297z00_6019))))->
							BgL_otypez00) = ((BgL_typez00_bglt) BgL_auxz00_9879), BUNSPEC);
				}
				{
					BgL_variablez00_bglt BgL_auxz00_9896;
					BgL_prezd2valloczf2cinfoz20_bglt BgL_auxz00_9889;

					{	/* Cfa/cinfo3.scm 46 */
						obj_t BgL_classz00_6640;

						BgL_classz00_6640 = BGl_variablez00zzast_varz00;
						{	/* Cfa/cinfo3.scm 46 */
							obj_t BgL__ortest_1117z00_6641;

							BgL__ortest_1117z00_6641 = BGL_CLASS_NIL(BgL_classz00_6640);
							if (CBOOL(BgL__ortest_1117z00_6641))
								{	/* Cfa/cinfo3.scm 46 */
									BgL_auxz00_9896 =
										((BgL_variablez00_bglt) BgL__ortest_1117z00_6641);
								}
							else
								{	/* Cfa/cinfo3.scm 46 */
									BgL_auxz00_9896 =
										((BgL_variablez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6640));
								}
						}
					}
					{
						obj_t BgL_auxz00_9890;

						{	/* Cfa/cinfo3.scm 46 */
							BgL_objectz00_bglt BgL_tmpz00_9891;

							BgL_tmpz00_9891 =
								((BgL_objectz00_bglt)
								((BgL_vallocz00_bglt) BgL_new1297z00_6019));
							BgL_auxz00_9890 = BGL_OBJECT_WIDENING(BgL_tmpz00_9891);
						}
						BgL_auxz00_9889 =
							((BgL_prezd2valloczf2cinfoz20_bglt) BgL_auxz00_9890);
					}
					((((BgL_prezd2valloczf2cinfoz20_bglt) COBJECT(BgL_auxz00_9889))->
							BgL_ownerz00) =
						((BgL_variablez00_bglt) BgL_auxz00_9896), BUNSPEC);
				}
				BgL_auxz00_9840 = ((BgL_vallocz00_bglt) BgL_new1297z00_6019);
				return ((obj_t) BgL_auxz00_9840);
			}
		}

	}



/* &lambda2040 */
	BgL_vallocz00_bglt BGl_z62lambda2040z62zzcfa_info3z00(obj_t BgL_envz00_6020,
		obj_t BgL_o1294z00_6021)
	{
		{	/* Cfa/cinfo3.scm 46 */
			{	/* Cfa/cinfo3.scm 46 */
				BgL_prezd2valloczf2cinfoz20_bglt BgL_wide1296z00_6643;

				BgL_wide1296z00_6643 =
					((BgL_prezd2valloczf2cinfoz20_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_prezd2valloczf2cinfoz20_bgl))));
				{	/* Cfa/cinfo3.scm 46 */
					obj_t BgL_auxz00_9911;
					BgL_objectz00_bglt BgL_tmpz00_9907;

					BgL_auxz00_9911 = ((obj_t) BgL_wide1296z00_6643);
					BgL_tmpz00_9907 =
						((BgL_objectz00_bglt)
						((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_o1294z00_6021)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9907, BgL_auxz00_9911);
				}
				((BgL_objectz00_bglt)
					((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_o1294z00_6021)));
				{	/* Cfa/cinfo3.scm 46 */
					long BgL_arg2041z00_6644;

					BgL_arg2041z00_6644 =
						BGL_CLASS_NUM(BGl_prezd2valloczf2Cinfoz20zzcfa_info3z00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_vallocz00_bglt)
								((BgL_vallocz00_bglt) BgL_o1294z00_6021))),
						BgL_arg2041z00_6644);
				}
				return
					((BgL_vallocz00_bglt)
					((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_o1294z00_6021)));
			}
		}

	}



/* &lambda2037 */
	BgL_vallocz00_bglt BGl_z62lambda2037z62zzcfa_info3z00(obj_t BgL_envz00_6022,
		obj_t BgL_loc1283z00_6023, obj_t BgL_type1285z00_6024,
		obj_t BgL_sidezd2effect1286zd2_6025, obj_t BgL_key1287z00_6026,
		obj_t BgL_exprza21288za2_6027, obj_t BgL_effect1289z00_6028,
		obj_t BgL_czd2format1290zd2_6029, obj_t BgL_ftype1291z00_6030,
		obj_t BgL_otype1292z00_6031, obj_t BgL_owner1293z00_6032)
	{
		{	/* Cfa/cinfo3.scm 46 */
			{	/* Cfa/cinfo3.scm 46 */
				BgL_vallocz00_bglt BgL_new1434z00_6651;

				{	/* Cfa/cinfo3.scm 46 */
					BgL_vallocz00_bglt BgL_tmp1432z00_6652;
					BgL_prezd2valloczf2cinfoz20_bglt BgL_wide1433z00_6653;

					{
						BgL_vallocz00_bglt BgL_auxz00_9925;

						{	/* Cfa/cinfo3.scm 46 */
							BgL_vallocz00_bglt BgL_new1431z00_6654;

							BgL_new1431z00_6654 =
								((BgL_vallocz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_vallocz00_bgl))));
							{	/* Cfa/cinfo3.scm 46 */
								long BgL_arg2039z00_6655;

								BgL_arg2039z00_6655 = BGL_CLASS_NUM(BGl_vallocz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1431z00_6654),
									BgL_arg2039z00_6655);
							}
							{	/* Cfa/cinfo3.scm 46 */
								BgL_objectz00_bglt BgL_tmpz00_9930;

								BgL_tmpz00_9930 = ((BgL_objectz00_bglt) BgL_new1431z00_6654);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9930, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1431z00_6654);
							BgL_auxz00_9925 = BgL_new1431z00_6654;
						}
						BgL_tmp1432z00_6652 = ((BgL_vallocz00_bglt) BgL_auxz00_9925);
					}
					BgL_wide1433z00_6653 =
						((BgL_prezd2valloczf2cinfoz20_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_prezd2valloczf2cinfoz20_bgl))));
					{	/* Cfa/cinfo3.scm 46 */
						obj_t BgL_auxz00_9938;
						BgL_objectz00_bglt BgL_tmpz00_9936;

						BgL_auxz00_9938 = ((obj_t) BgL_wide1433z00_6653);
						BgL_tmpz00_9936 = ((BgL_objectz00_bglt) BgL_tmp1432z00_6652);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9936, BgL_auxz00_9938);
					}
					((BgL_objectz00_bglt) BgL_tmp1432z00_6652);
					{	/* Cfa/cinfo3.scm 46 */
						long BgL_arg2038z00_6656;

						BgL_arg2038z00_6656 =
							BGL_CLASS_NUM(BGl_prezd2valloczf2Cinfoz20zzcfa_info3z00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1432z00_6652), BgL_arg2038z00_6656);
					}
					BgL_new1434z00_6651 = ((BgL_vallocz00_bglt) BgL_tmp1432z00_6652);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1434z00_6651)))->BgL_locz00) =
					((obj_t) BgL_loc1283z00_6023), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1434z00_6651)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1285z00_6024)),
					BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1434z00_6651)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1286zd2_6025), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1434z00_6651)))->BgL_keyz00) =
					((obj_t) BgL_key1287z00_6026), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1434z00_6651)))->BgL_exprza2za2) =
					((obj_t) ((obj_t) BgL_exprza21288za2_6027)), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1434z00_6651)))->BgL_effectz00) =
					((obj_t) BgL_effect1289z00_6028), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1434z00_6651)))->BgL_czd2formatzd2) =
					((obj_t) ((obj_t) BgL_czd2format1290zd2_6029)), BUNSPEC);
				((((BgL_vallocz00_bglt) COBJECT(((BgL_vallocz00_bglt)
									BgL_new1434z00_6651)))->BgL_ftypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_ftype1291z00_6030)),
					BUNSPEC);
				((((BgL_vallocz00_bglt) COBJECT(((BgL_vallocz00_bglt)
									BgL_new1434z00_6651)))->BgL_otypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_otype1292z00_6031)),
					BUNSPEC);
				{
					BgL_prezd2valloczf2cinfoz20_bglt BgL_auxz00_9969;

					{
						obj_t BgL_auxz00_9970;

						{	/* Cfa/cinfo3.scm 46 */
							BgL_objectz00_bglt BgL_tmpz00_9971;

							BgL_tmpz00_9971 = ((BgL_objectz00_bglt) BgL_new1434z00_6651);
							BgL_auxz00_9970 = BGL_OBJECT_WIDENING(BgL_tmpz00_9971);
						}
						BgL_auxz00_9969 =
							((BgL_prezd2valloczf2cinfoz20_bglt) BgL_auxz00_9970);
					}
					((((BgL_prezd2valloczf2cinfoz20_bglt) COBJECT(BgL_auxz00_9969))->
							BgL_ownerz00) =
						((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
								BgL_owner1293z00_6032)), BUNSPEC);
				}
				return BgL_new1434z00_6651;
			}
		}

	}



/* &lambda2051 */
	obj_t BGl_z62lambda2051z62zzcfa_info3z00(obj_t BgL_envz00_6033,
		obj_t BgL_oz00_6034, obj_t BgL_vz00_6035)
	{
		{	/* Cfa/cinfo3.scm 46 */
			{
				BgL_prezd2valloczf2cinfoz20_bglt BgL_auxz00_9977;

				{
					obj_t BgL_auxz00_9978;

					{	/* Cfa/cinfo3.scm 46 */
						BgL_objectz00_bglt BgL_tmpz00_9979;

						BgL_tmpz00_9979 =
							((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_oz00_6034));
						BgL_auxz00_9978 = BGL_OBJECT_WIDENING(BgL_tmpz00_9979);
					}
					BgL_auxz00_9977 =
						((BgL_prezd2valloczf2cinfoz20_bglt) BgL_auxz00_9978);
				}
				return
					((((BgL_prezd2valloczf2cinfoz20_bglt) COBJECT(BgL_auxz00_9977))->
						BgL_ownerz00) =
					((BgL_variablez00_bglt) ((BgL_variablez00_bglt) BgL_vz00_6035)),
					BUNSPEC);
			}
		}

	}



/* &lambda2050 */
	BgL_variablez00_bglt BGl_z62lambda2050z62zzcfa_info3z00(obj_t BgL_envz00_6036,
		obj_t BgL_oz00_6037)
	{
		{	/* Cfa/cinfo3.scm 46 */
			{
				BgL_prezd2valloczf2cinfoz20_bglt BgL_auxz00_9986;

				{
					obj_t BgL_auxz00_9987;

					{	/* Cfa/cinfo3.scm 46 */
						BgL_objectz00_bglt BgL_tmpz00_9988;

						BgL_tmpz00_9988 =
							((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_oz00_6037));
						BgL_auxz00_9987 = BGL_OBJECT_WIDENING(BgL_tmpz00_9988);
					}
					BgL_auxz00_9986 =
						((BgL_prezd2valloczf2cinfoz20_bglt) BgL_auxz00_9987);
				}
				return
					(((BgL_prezd2valloczf2cinfoz20_bglt) COBJECT(BgL_auxz00_9986))->
					BgL_ownerz00);
			}
		}

	}



/* &lambda2011 */
	BgL_vlengthz00_bglt BGl_z62lambda2011z62zzcfa_info3z00(obj_t BgL_envz00_6038,
		obj_t BgL_o1281z00_6039)
	{
		{	/* Cfa/cinfo3.scm 41 */
			{	/* Cfa/cinfo3.scm 41 */
				long BgL_arg2012z00_6661;

				{	/* Cfa/cinfo3.scm 41 */
					obj_t BgL_arg2013z00_6662;

					{	/* Cfa/cinfo3.scm 41 */
						obj_t BgL_arg2014z00_6663;

						{	/* Cfa/cinfo3.scm 41 */
							obj_t BgL_arg1815z00_6664;
							long BgL_arg1816z00_6665;

							BgL_arg1815z00_6664 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Cfa/cinfo3.scm 41 */
								long BgL_arg1817z00_6666;

								BgL_arg1817z00_6666 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_vlengthz00_bglt) BgL_o1281z00_6039)));
								BgL_arg1816z00_6665 = (BgL_arg1817z00_6666 - OBJECT_TYPE);
							}
							BgL_arg2014z00_6663 =
								VECTOR_REF(BgL_arg1815z00_6664, BgL_arg1816z00_6665);
						}
						BgL_arg2013z00_6662 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2014z00_6663);
					}
					{	/* Cfa/cinfo3.scm 41 */
						obj_t BgL_tmpz00_10001;

						BgL_tmpz00_10001 = ((obj_t) BgL_arg2013z00_6662);
						BgL_arg2012z00_6661 = BGL_CLASS_NUM(BgL_tmpz00_10001);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_vlengthz00_bglt) BgL_o1281z00_6039)), BgL_arg2012z00_6661);
			}
			{	/* Cfa/cinfo3.scm 41 */
				BgL_objectz00_bglt BgL_tmpz00_10007;

				BgL_tmpz00_10007 =
					((BgL_objectz00_bglt) ((BgL_vlengthz00_bglt) BgL_o1281z00_6039));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10007, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_vlengthz00_bglt) BgL_o1281z00_6039));
			return ((BgL_vlengthz00_bglt) ((BgL_vlengthz00_bglt) BgL_o1281z00_6039));
		}

	}



/* &<@anonymous:2010> */
	obj_t BGl_z62zc3z04anonymousza32010ze3ze5zzcfa_info3z00(obj_t BgL_envz00_6040,
		obj_t BgL_new1280z00_6041)
	{
		{	/* Cfa/cinfo3.scm 41 */
			{
				BgL_vlengthz00_bglt BgL_auxz00_10015;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_vlengthz00_bglt) BgL_new1280z00_6041))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10019;

					{	/* Cfa/cinfo3.scm 41 */
						obj_t BgL_classz00_6668;

						BgL_classz00_6668 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 41 */
							obj_t BgL__ortest_1117z00_6669;

							BgL__ortest_1117z00_6669 = BGL_CLASS_NIL(BgL_classz00_6668);
							if (CBOOL(BgL__ortest_1117z00_6669))
								{	/* Cfa/cinfo3.scm 41 */
									BgL_auxz00_10019 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6669);
								}
							else
								{	/* Cfa/cinfo3.scm 41 */
									BgL_auxz00_10019 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6668));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_vlengthz00_bglt) BgL_new1280z00_6041))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_10019), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_vlengthz00_bglt) BgL_new1280z00_6041))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_vlengthz00_bglt)
										BgL_new1280z00_6041))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_vlengthz00_bglt)
										BgL_new1280z00_6041))))->BgL_exprza2za2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_vlengthz00_bglt)
										BgL_new1280z00_6041))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_privatez00_bglt)
							COBJECT(((BgL_privatez00_bglt) ((BgL_vlengthz00_bglt)
										BgL_new1280z00_6041))))->BgL_czd2formatzd2) =
					((obj_t) BGl_string2364z00zzcfa_info3z00), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10044;

					{	/* Cfa/cinfo3.scm 41 */
						obj_t BgL_classz00_6670;

						BgL_classz00_6670 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 41 */
							obj_t BgL__ortest_1117z00_6671;

							BgL__ortest_1117z00_6671 = BGL_CLASS_NIL(BgL_classz00_6670);
							if (CBOOL(BgL__ortest_1117z00_6671))
								{	/* Cfa/cinfo3.scm 41 */
									BgL_auxz00_10044 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6671);
								}
							else
								{	/* Cfa/cinfo3.scm 41 */
									BgL_auxz00_10044 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6670));
								}
						}
					}
					((((BgL_vlengthz00_bglt) COBJECT(
									((BgL_vlengthz00_bglt)
										((BgL_vlengthz00_bglt) BgL_new1280z00_6041))))->
							BgL_vtypez00) = ((BgL_typez00_bglt) BgL_auxz00_10044), BUNSPEC);
				}
				((((BgL_vlengthz00_bglt) COBJECT(
								((BgL_vlengthz00_bglt)
									((BgL_vlengthz00_bglt) BgL_new1280z00_6041))))->
						BgL_ftypez00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_approxz00_bglt BgL_auxz00_10064;
					BgL_vlengthzf2cinfozf2_bglt BgL_auxz00_10057;

					{	/* Cfa/cinfo3.scm 41 */
						obj_t BgL_classz00_6672;

						BgL_classz00_6672 = BGl_approxz00zzcfa_infoz00;
						{	/* Cfa/cinfo3.scm 41 */
							obj_t BgL__ortest_1117z00_6673;

							BgL__ortest_1117z00_6673 = BGL_CLASS_NIL(BgL_classz00_6672);
							if (CBOOL(BgL__ortest_1117z00_6673))
								{	/* Cfa/cinfo3.scm 41 */
									BgL_auxz00_10064 =
										((BgL_approxz00_bglt) BgL__ortest_1117z00_6673);
								}
							else
								{	/* Cfa/cinfo3.scm 41 */
									BgL_auxz00_10064 =
										((BgL_approxz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6672));
								}
						}
					}
					{
						obj_t BgL_auxz00_10058;

						{	/* Cfa/cinfo3.scm 41 */
							BgL_objectz00_bglt BgL_tmpz00_10059;

							BgL_tmpz00_10059 =
								((BgL_objectz00_bglt)
								((BgL_vlengthz00_bglt) BgL_new1280z00_6041));
							BgL_auxz00_10058 = BGL_OBJECT_WIDENING(BgL_tmpz00_10059);
						}
						BgL_auxz00_10057 = ((BgL_vlengthzf2cinfozf2_bglt) BgL_auxz00_10058);
					}
					((((BgL_vlengthzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10057))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_auxz00_10064), BUNSPEC);
				}
				{
					BgL_vlengthzf2cinfozf2_bglt BgL_auxz00_10072;

					{
						obj_t BgL_auxz00_10073;

						{	/* Cfa/cinfo3.scm 41 */
							BgL_objectz00_bglt BgL_tmpz00_10074;

							BgL_tmpz00_10074 =
								((BgL_objectz00_bglt)
								((BgL_vlengthz00_bglt) BgL_new1280z00_6041));
							BgL_auxz00_10073 = BGL_OBJECT_WIDENING(BgL_tmpz00_10074);
						}
						BgL_auxz00_10072 = ((BgL_vlengthzf2cinfozf2_bglt) BgL_auxz00_10073);
					}
					((((BgL_vlengthzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10072))->
							BgL_tvectorzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				BgL_auxz00_10015 = ((BgL_vlengthz00_bglt) BgL_new1280z00_6041);
				return ((obj_t) BgL_auxz00_10015);
			}
		}

	}



/* &lambda2008 */
	BgL_vlengthz00_bglt BGl_z62lambda2008z62zzcfa_info3z00(obj_t BgL_envz00_6042,
		obj_t BgL_o1277z00_6043)
	{
		{	/* Cfa/cinfo3.scm 41 */
			{	/* Cfa/cinfo3.scm 41 */
				BgL_vlengthzf2cinfozf2_bglt BgL_wide1279z00_6675;

				BgL_wide1279z00_6675 =
					((BgL_vlengthzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_vlengthzf2cinfozf2_bgl))));
				{	/* Cfa/cinfo3.scm 41 */
					obj_t BgL_auxz00_10087;
					BgL_objectz00_bglt BgL_tmpz00_10083;

					BgL_auxz00_10087 = ((obj_t) BgL_wide1279z00_6675);
					BgL_tmpz00_10083 =
						((BgL_objectz00_bglt)
						((BgL_vlengthz00_bglt) ((BgL_vlengthz00_bglt) BgL_o1277z00_6043)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10083, BgL_auxz00_10087);
				}
				((BgL_objectz00_bglt)
					((BgL_vlengthz00_bglt) ((BgL_vlengthz00_bglt) BgL_o1277z00_6043)));
				{	/* Cfa/cinfo3.scm 41 */
					long BgL_arg2009z00_6676;

					BgL_arg2009z00_6676 =
						BGL_CLASS_NUM(BGl_vlengthzf2Cinfozf2zzcfa_info3z00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_vlengthz00_bglt)
								((BgL_vlengthz00_bglt) BgL_o1277z00_6043))),
						BgL_arg2009z00_6676);
				}
				return
					((BgL_vlengthz00_bglt)
					((BgL_vlengthz00_bglt) ((BgL_vlengthz00_bglt) BgL_o1277z00_6043)));
			}
		}

	}



/* &lambda2004 */
	BgL_vlengthz00_bglt BGl_z62lambda2004z62zzcfa_info3z00(obj_t BgL_envz00_6044,
		obj_t BgL_loc1266z00_6045, obj_t BgL_type1267z00_6046,
		obj_t BgL_sidezd2effect1268zd2_6047, obj_t BgL_key1269z00_6048,
		obj_t BgL_exprza21270za2_6049, obj_t BgL_effect1271z00_6050,
		obj_t BgL_czd2format1272zd2_6051, obj_t BgL_vtype1273z00_6052,
		obj_t BgL_ftype1274z00_6053, obj_t BgL_approx1275z00_6054,
		obj_t BgL_tvectorzf31276zf3_6055)
	{
		{	/* Cfa/cinfo3.scm 41 */
			{	/* Cfa/cinfo3.scm 41 */
				bool_t BgL_tvectorzf31276zf3_6682;

				BgL_tvectorzf31276zf3_6682 = CBOOL(BgL_tvectorzf31276zf3_6055);
				{	/* Cfa/cinfo3.scm 41 */
					BgL_vlengthz00_bglt BgL_new1429z00_6683;

					{	/* Cfa/cinfo3.scm 41 */
						BgL_vlengthz00_bglt BgL_tmp1427z00_6684;
						BgL_vlengthzf2cinfozf2_bglt BgL_wide1428z00_6685;

						{
							BgL_vlengthz00_bglt BgL_auxz00_10102;

							{	/* Cfa/cinfo3.scm 41 */
								BgL_vlengthz00_bglt BgL_new1426z00_6686;

								BgL_new1426z00_6686 =
									((BgL_vlengthz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_vlengthz00_bgl))));
								{	/* Cfa/cinfo3.scm 41 */
									long BgL_arg2007z00_6687;

									BgL_arg2007z00_6687 =
										BGL_CLASS_NUM(BGl_vlengthz00zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1426z00_6686),
										BgL_arg2007z00_6687);
								}
								{	/* Cfa/cinfo3.scm 41 */
									BgL_objectz00_bglt BgL_tmpz00_10107;

									BgL_tmpz00_10107 = ((BgL_objectz00_bglt) BgL_new1426z00_6686);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10107, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1426z00_6686);
								BgL_auxz00_10102 = BgL_new1426z00_6686;
							}
							BgL_tmp1427z00_6684 = ((BgL_vlengthz00_bglt) BgL_auxz00_10102);
						}
						BgL_wide1428z00_6685 =
							((BgL_vlengthzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_vlengthzf2cinfozf2_bgl))));
						{	/* Cfa/cinfo3.scm 41 */
							obj_t BgL_auxz00_10115;
							BgL_objectz00_bglt BgL_tmpz00_10113;

							BgL_auxz00_10115 = ((obj_t) BgL_wide1428z00_6685);
							BgL_tmpz00_10113 = ((BgL_objectz00_bglt) BgL_tmp1427z00_6684);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10113, BgL_auxz00_10115);
						}
						((BgL_objectz00_bglt) BgL_tmp1427z00_6684);
						{	/* Cfa/cinfo3.scm 41 */
							long BgL_arg2006z00_6688;

							BgL_arg2006z00_6688 =
								BGL_CLASS_NUM(BGl_vlengthzf2Cinfozf2zzcfa_info3z00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1427z00_6684),
								BgL_arg2006z00_6688);
						}
						BgL_new1429z00_6683 = ((BgL_vlengthz00_bglt) BgL_tmp1427z00_6684);
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_new1429z00_6683)))->BgL_locz00) =
						((obj_t) BgL_loc1266z00_6045), BUNSPEC);
					((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_new1429z00_6683)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1267z00_6046)),
						BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1429z00_6683)))->BgL_sidezd2effectzd2) =
						((obj_t) BgL_sidezd2effect1268zd2_6047), BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1429z00_6683)))->BgL_keyz00) =
						((obj_t) BgL_key1269z00_6048), BUNSPEC);
					((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
										BgL_new1429z00_6683)))->BgL_exprza2za2) =
						((obj_t) ((obj_t) BgL_exprza21270za2_6049)), BUNSPEC);
					((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
										BgL_new1429z00_6683)))->BgL_effectz00) =
						((obj_t) BgL_effect1271z00_6050), BUNSPEC);
					((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
										BgL_new1429z00_6683)))->BgL_czd2formatzd2) =
						((obj_t) ((obj_t) BgL_czd2format1272zd2_6051)), BUNSPEC);
					((((BgL_vlengthz00_bglt) COBJECT(((BgL_vlengthz00_bglt)
										BgL_new1429z00_6683)))->BgL_vtypez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vtype1273z00_6052)),
						BUNSPEC);
					((((BgL_vlengthz00_bglt) COBJECT(((BgL_vlengthz00_bglt)
										BgL_new1429z00_6683)))->BgL_ftypez00) =
						((obj_t) BgL_ftype1274z00_6053), BUNSPEC);
					{
						BgL_vlengthzf2cinfozf2_bglt BgL_auxz00_10145;

						{
							obj_t BgL_auxz00_10146;

							{	/* Cfa/cinfo3.scm 41 */
								BgL_objectz00_bglt BgL_tmpz00_10147;

								BgL_tmpz00_10147 = ((BgL_objectz00_bglt) BgL_new1429z00_6683);
								BgL_auxz00_10146 = BGL_OBJECT_WIDENING(BgL_tmpz00_10147);
							}
							BgL_auxz00_10145 =
								((BgL_vlengthzf2cinfozf2_bglt) BgL_auxz00_10146);
						}
						((((BgL_vlengthzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10145))->
								BgL_approxz00) =
							((BgL_approxz00_bglt) ((BgL_approxz00_bglt)
									BgL_approx1275z00_6054)), BUNSPEC);
					}
					{
						BgL_vlengthzf2cinfozf2_bglt BgL_auxz00_10153;

						{
							obj_t BgL_auxz00_10154;

							{	/* Cfa/cinfo3.scm 41 */
								BgL_objectz00_bglt BgL_tmpz00_10155;

								BgL_tmpz00_10155 = ((BgL_objectz00_bglt) BgL_new1429z00_6683);
								BgL_auxz00_10154 = BGL_OBJECT_WIDENING(BgL_tmpz00_10155);
							}
							BgL_auxz00_10153 =
								((BgL_vlengthzf2cinfozf2_bglt) BgL_auxz00_10154);
						}
						((((BgL_vlengthzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10153))->
								BgL_tvectorzf3zf3) =
							((bool_t) BgL_tvectorzf31276zf3_6682), BUNSPEC);
					}
					return BgL_new1429z00_6683;
				}
			}
		}

	}



/* &<@anonymous:2027> */
	obj_t BGl_z62zc3z04anonymousza32027ze3ze5zzcfa_info3z00(obj_t BgL_envz00_6056)
	{
		{	/* Cfa/cinfo3.scm 41 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda2026 */
	obj_t BGl_z62lambda2026z62zzcfa_info3z00(obj_t BgL_envz00_6057,
		obj_t BgL_oz00_6058, obj_t BgL_vz00_6059)
	{
		{	/* Cfa/cinfo3.scm 41 */
			{	/* Cfa/cinfo3.scm 41 */
				bool_t BgL_vz00_6690;

				BgL_vz00_6690 = CBOOL(BgL_vz00_6059);
				{
					BgL_vlengthzf2cinfozf2_bglt BgL_auxz00_10162;

					{
						obj_t BgL_auxz00_10163;

						{	/* Cfa/cinfo3.scm 41 */
							BgL_objectz00_bglt BgL_tmpz00_10164;

							BgL_tmpz00_10164 =
								((BgL_objectz00_bglt) ((BgL_vlengthz00_bglt) BgL_oz00_6058));
							BgL_auxz00_10163 = BGL_OBJECT_WIDENING(BgL_tmpz00_10164);
						}
						BgL_auxz00_10162 = ((BgL_vlengthzf2cinfozf2_bglt) BgL_auxz00_10163);
					}
					return
						((((BgL_vlengthzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10162))->
							BgL_tvectorzf3zf3) = ((bool_t) BgL_vz00_6690), BUNSPEC);
				}
			}
		}

	}



/* &lambda2025 */
	obj_t BGl_z62lambda2025z62zzcfa_info3z00(obj_t BgL_envz00_6060,
		obj_t BgL_oz00_6061)
	{
		{	/* Cfa/cinfo3.scm 41 */
			{	/* Cfa/cinfo3.scm 41 */
				bool_t BgL_tmpz00_10170;

				{
					BgL_vlengthzf2cinfozf2_bglt BgL_auxz00_10171;

					{
						obj_t BgL_auxz00_10172;

						{	/* Cfa/cinfo3.scm 41 */
							BgL_objectz00_bglt BgL_tmpz00_10173;

							BgL_tmpz00_10173 =
								((BgL_objectz00_bglt) ((BgL_vlengthz00_bglt) BgL_oz00_6061));
							BgL_auxz00_10172 = BGL_OBJECT_WIDENING(BgL_tmpz00_10173);
						}
						BgL_auxz00_10171 = ((BgL_vlengthzf2cinfozf2_bglt) BgL_auxz00_10172);
					}
					BgL_tmpz00_10170 =
						(((BgL_vlengthzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10171))->
						BgL_tvectorzf3zf3);
				}
				return BBOOL(BgL_tmpz00_10170);
			}
		}

	}



/* &lambda2019 */
	obj_t BGl_z62lambda2019z62zzcfa_info3z00(obj_t BgL_envz00_6062,
		obj_t BgL_oz00_6063, obj_t BgL_vz00_6064)
	{
		{	/* Cfa/cinfo3.scm 41 */
			{
				BgL_vlengthzf2cinfozf2_bglt BgL_auxz00_10180;

				{
					obj_t BgL_auxz00_10181;

					{	/* Cfa/cinfo3.scm 41 */
						BgL_objectz00_bglt BgL_tmpz00_10182;

						BgL_tmpz00_10182 =
							((BgL_objectz00_bglt) ((BgL_vlengthz00_bglt) BgL_oz00_6063));
						BgL_auxz00_10181 = BGL_OBJECT_WIDENING(BgL_tmpz00_10182);
					}
					BgL_auxz00_10180 = ((BgL_vlengthzf2cinfozf2_bglt) BgL_auxz00_10181);
				}
				return
					((((BgL_vlengthzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10180))->
						BgL_approxz00) =
					((BgL_approxz00_bglt) ((BgL_approxz00_bglt) BgL_vz00_6064)), BUNSPEC);
			}
		}

	}



/* &lambda2018 */
	BgL_approxz00_bglt BGl_z62lambda2018z62zzcfa_info3z00(obj_t BgL_envz00_6065,
		obj_t BgL_oz00_6066)
	{
		{	/* Cfa/cinfo3.scm 41 */
			{
				BgL_vlengthzf2cinfozf2_bglt BgL_auxz00_10189;

				{
					obj_t BgL_auxz00_10190;

					{	/* Cfa/cinfo3.scm 41 */
						BgL_objectz00_bglt BgL_tmpz00_10191;

						BgL_tmpz00_10191 =
							((BgL_objectz00_bglt) ((BgL_vlengthz00_bglt) BgL_oz00_6066));
						BgL_auxz00_10190 = BGL_OBJECT_WIDENING(BgL_tmpz00_10191);
					}
					BgL_auxz00_10189 = ((BgL_vlengthzf2cinfozf2_bglt) BgL_auxz00_10190);
				}
				return
					(((BgL_vlengthzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10189))->
					BgL_approxz00);
			}
		}

	}



/* &lambda1982 */
	BgL_vsetz12z12_bglt BGl_z62lambda1982z62zzcfa_info3z00(obj_t BgL_envz00_6067,
		obj_t BgL_o1264z00_6068)
	{
		{	/* Cfa/cinfo3.scm 38 */
			{	/* Cfa/cinfo3.scm 38 */
				long BgL_arg1983z00_6696;

				{	/* Cfa/cinfo3.scm 38 */
					obj_t BgL_arg1984z00_6697;

					{	/* Cfa/cinfo3.scm 38 */
						obj_t BgL_arg1985z00_6698;

						{	/* Cfa/cinfo3.scm 38 */
							obj_t BgL_arg1815z00_6699;
							long BgL_arg1816z00_6700;

							BgL_arg1815z00_6699 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Cfa/cinfo3.scm 38 */
								long BgL_arg1817z00_6701;

								BgL_arg1817z00_6701 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_vsetz12z12_bglt) BgL_o1264z00_6068)));
								BgL_arg1816z00_6700 = (BgL_arg1817z00_6701 - OBJECT_TYPE);
							}
							BgL_arg1985z00_6698 =
								VECTOR_REF(BgL_arg1815z00_6699, BgL_arg1816z00_6700);
						}
						BgL_arg1984z00_6697 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1985z00_6698);
					}
					{	/* Cfa/cinfo3.scm 38 */
						obj_t BgL_tmpz00_10204;

						BgL_tmpz00_10204 = ((obj_t) BgL_arg1984z00_6697);
						BgL_arg1983z00_6696 = BGL_CLASS_NUM(BgL_tmpz00_10204);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_vsetz12z12_bglt) BgL_o1264z00_6068)), BgL_arg1983z00_6696);
			}
			{	/* Cfa/cinfo3.scm 38 */
				BgL_objectz00_bglt BgL_tmpz00_10210;

				BgL_tmpz00_10210 =
					((BgL_objectz00_bglt) ((BgL_vsetz12z12_bglt) BgL_o1264z00_6068));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10210, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_vsetz12z12_bglt) BgL_o1264z00_6068));
			return ((BgL_vsetz12z12_bglt) ((BgL_vsetz12z12_bglt) BgL_o1264z00_6068));
		}

	}



/* &<@anonymous:1981> */
	obj_t BGl_z62zc3z04anonymousza31981ze3ze5zzcfa_info3z00(obj_t BgL_envz00_6069,
		obj_t BgL_new1263z00_6070)
	{
		{	/* Cfa/cinfo3.scm 38 */
			{
				BgL_vsetz12z12_bglt BgL_auxz00_10218;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_vsetz12z12_bglt) BgL_new1263z00_6070))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10222;

					{	/* Cfa/cinfo3.scm 38 */
						obj_t BgL_classz00_6703;

						BgL_classz00_6703 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 38 */
							obj_t BgL__ortest_1117z00_6704;

							BgL__ortest_1117z00_6704 = BGL_CLASS_NIL(BgL_classz00_6703);
							if (CBOOL(BgL__ortest_1117z00_6704))
								{	/* Cfa/cinfo3.scm 38 */
									BgL_auxz00_10222 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6704);
								}
							else
								{	/* Cfa/cinfo3.scm 38 */
									BgL_auxz00_10222 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6703));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_vsetz12z12_bglt) BgL_new1263z00_6070))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_10222), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_vsetz12z12_bglt) BgL_new1263z00_6070))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_vsetz12z12_bglt)
										BgL_new1263z00_6070))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_vsetz12z12_bglt)
										BgL_new1263z00_6070))))->BgL_exprza2za2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_vsetz12z12_bglt)
										BgL_new1263z00_6070))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_privatez00_bglt)
							COBJECT(((BgL_privatez00_bglt) ((BgL_vsetz12z12_bglt)
										BgL_new1263z00_6070))))->BgL_czd2formatzd2) =
					((obj_t) BGl_string2364z00zzcfa_info3z00), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10247;

					{	/* Cfa/cinfo3.scm 38 */
						obj_t BgL_classz00_6705;

						BgL_classz00_6705 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 38 */
							obj_t BgL__ortest_1117z00_6706;

							BgL__ortest_1117z00_6706 = BGL_CLASS_NIL(BgL_classz00_6705);
							if (CBOOL(BgL__ortest_1117z00_6706))
								{	/* Cfa/cinfo3.scm 38 */
									BgL_auxz00_10247 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6706);
								}
							else
								{	/* Cfa/cinfo3.scm 38 */
									BgL_auxz00_10247 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6705));
								}
						}
					}
					((((BgL_vsetz12z12_bglt) COBJECT(
									((BgL_vsetz12z12_bglt)
										((BgL_vsetz12z12_bglt) BgL_new1263z00_6070))))->
							BgL_ftypez00) = ((BgL_typez00_bglt) BgL_auxz00_10247), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_10257;

					{	/* Cfa/cinfo3.scm 38 */
						obj_t BgL_classz00_6707;

						BgL_classz00_6707 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 38 */
							obj_t BgL__ortest_1117z00_6708;

							BgL__ortest_1117z00_6708 = BGL_CLASS_NIL(BgL_classz00_6707);
							if (CBOOL(BgL__ortest_1117z00_6708))
								{	/* Cfa/cinfo3.scm 38 */
									BgL_auxz00_10257 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6708);
								}
							else
								{	/* Cfa/cinfo3.scm 38 */
									BgL_auxz00_10257 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6707));
								}
						}
					}
					((((BgL_vsetz12z12_bglt) COBJECT(
									((BgL_vsetz12z12_bglt)
										((BgL_vsetz12z12_bglt) BgL_new1263z00_6070))))->
							BgL_otypez00) = ((BgL_typez00_bglt) BgL_auxz00_10257), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_10267;

					{	/* Cfa/cinfo3.scm 38 */
						obj_t BgL_classz00_6709;

						BgL_classz00_6709 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 38 */
							obj_t BgL__ortest_1117z00_6710;

							BgL__ortest_1117z00_6710 = BGL_CLASS_NIL(BgL_classz00_6709);
							if (CBOOL(BgL__ortest_1117z00_6710))
								{	/* Cfa/cinfo3.scm 38 */
									BgL_auxz00_10267 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6710);
								}
							else
								{	/* Cfa/cinfo3.scm 38 */
									BgL_auxz00_10267 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6709));
								}
						}
					}
					((((BgL_vsetz12z12_bglt) COBJECT(
									((BgL_vsetz12z12_bglt)
										((BgL_vsetz12z12_bglt) BgL_new1263z00_6070))))->
							BgL_vtypez00) = ((BgL_typez00_bglt) BgL_auxz00_10267), BUNSPEC);
				}
				((((BgL_vsetz12z12_bglt) COBJECT(
								((BgL_vsetz12z12_bglt)
									((BgL_vsetz12z12_bglt) BgL_new1263z00_6070))))->
						BgL_unsafez00) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				{
					BgL_approxz00_bglt BgL_auxz00_10287;
					BgL_vsetz12zf2cinfoze0_bglt BgL_auxz00_10280;

					{	/* Cfa/cinfo3.scm 38 */
						obj_t BgL_classz00_6711;

						BgL_classz00_6711 = BGl_approxz00zzcfa_infoz00;
						{	/* Cfa/cinfo3.scm 38 */
							obj_t BgL__ortest_1117z00_6712;

							BgL__ortest_1117z00_6712 = BGL_CLASS_NIL(BgL_classz00_6711);
							if (CBOOL(BgL__ortest_1117z00_6712))
								{	/* Cfa/cinfo3.scm 38 */
									BgL_auxz00_10287 =
										((BgL_approxz00_bglt) BgL__ortest_1117z00_6712);
								}
							else
								{	/* Cfa/cinfo3.scm 38 */
									BgL_auxz00_10287 =
										((BgL_approxz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6711));
								}
						}
					}
					{
						obj_t BgL_auxz00_10281;

						{	/* Cfa/cinfo3.scm 38 */
							BgL_objectz00_bglt BgL_tmpz00_10282;

							BgL_tmpz00_10282 =
								((BgL_objectz00_bglt)
								((BgL_vsetz12z12_bglt) BgL_new1263z00_6070));
							BgL_auxz00_10281 = BGL_OBJECT_WIDENING(BgL_tmpz00_10282);
						}
						BgL_auxz00_10280 = ((BgL_vsetz12zf2cinfoze0_bglt) BgL_auxz00_10281);
					}
					((((BgL_vsetz12zf2cinfoze0_bglt) COBJECT(BgL_auxz00_10280))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_auxz00_10287), BUNSPEC);
				}
				{
					BgL_vsetz12zf2cinfoze0_bglt BgL_auxz00_10295;

					{
						obj_t BgL_auxz00_10296;

						{	/* Cfa/cinfo3.scm 38 */
							BgL_objectz00_bglt BgL_tmpz00_10297;

							BgL_tmpz00_10297 =
								((BgL_objectz00_bglt)
								((BgL_vsetz12z12_bglt) BgL_new1263z00_6070));
							BgL_auxz00_10296 = BGL_OBJECT_WIDENING(BgL_tmpz00_10297);
						}
						BgL_auxz00_10295 = ((BgL_vsetz12zf2cinfoze0_bglt) BgL_auxz00_10296);
					}
					((((BgL_vsetz12zf2cinfoze0_bglt) COBJECT(BgL_auxz00_10295))->
							BgL_tvectorzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				BgL_auxz00_10218 = ((BgL_vsetz12z12_bglt) BgL_new1263z00_6070);
				return ((obj_t) BgL_auxz00_10218);
			}
		}

	}



/* &lambda1979 */
	BgL_vsetz12z12_bglt BGl_z62lambda1979z62zzcfa_info3z00(obj_t BgL_envz00_6071,
		obj_t BgL_o1260z00_6072)
	{
		{	/* Cfa/cinfo3.scm 38 */
			{	/* Cfa/cinfo3.scm 38 */
				BgL_vsetz12zf2cinfoze0_bglt BgL_wide1262z00_6714;

				BgL_wide1262z00_6714 =
					((BgL_vsetz12zf2cinfoze0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_vsetz12zf2cinfoze0_bgl))));
				{	/* Cfa/cinfo3.scm 38 */
					obj_t BgL_auxz00_10310;
					BgL_objectz00_bglt BgL_tmpz00_10306;

					BgL_auxz00_10310 = ((obj_t) BgL_wide1262z00_6714);
					BgL_tmpz00_10306 =
						((BgL_objectz00_bglt)
						((BgL_vsetz12z12_bglt) ((BgL_vsetz12z12_bglt) BgL_o1260z00_6072)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10306, BgL_auxz00_10310);
				}
				((BgL_objectz00_bglt)
					((BgL_vsetz12z12_bglt) ((BgL_vsetz12z12_bglt) BgL_o1260z00_6072)));
				{	/* Cfa/cinfo3.scm 38 */
					long BgL_arg1980z00_6715;

					BgL_arg1980z00_6715 =
						BGL_CLASS_NUM(BGl_vsetz12zf2Cinfoze0zzcfa_info3z00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_vsetz12z12_bglt)
								((BgL_vsetz12z12_bglt) BgL_o1260z00_6072))),
						BgL_arg1980z00_6715);
				}
				return
					((BgL_vsetz12z12_bglt)
					((BgL_vsetz12z12_bglt) ((BgL_vsetz12z12_bglt) BgL_o1260z00_6072)));
			}
		}

	}



/* &lambda1976 */
	BgL_vsetz12z12_bglt BGl_z62lambda1976z62zzcfa_info3z00(obj_t BgL_envz00_6073,
		obj_t BgL_loc1247z00_6074, obj_t BgL_type1248z00_6075,
		obj_t BgL_sidezd2effect1249zd2_6076, obj_t BgL_key1250z00_6077,
		obj_t BgL_exprza21251za2_6078, obj_t BgL_effect1252z00_6079,
		obj_t BgL_czd2format1253zd2_6080, obj_t BgL_ftype1254z00_6081,
		obj_t BgL_otype1255z00_6082, obj_t BgL_vtype1256z00_6083,
		obj_t BgL_unsafe1257z00_6084, obj_t BgL_approx1258z00_6085,
		obj_t BgL_tvectorzf31259zf3_6086)
	{
		{	/* Cfa/cinfo3.scm 38 */
			{	/* Cfa/cinfo3.scm 38 */
				bool_t BgL_unsafe1257z00_6722;
				bool_t BgL_tvectorzf31259zf3_6724;

				BgL_unsafe1257z00_6722 = CBOOL(BgL_unsafe1257z00_6084);
				BgL_tvectorzf31259zf3_6724 = CBOOL(BgL_tvectorzf31259zf3_6086);
				{	/* Cfa/cinfo3.scm 38 */
					BgL_vsetz12z12_bglt BgL_new1424z00_6725;

					{	/* Cfa/cinfo3.scm 38 */
						BgL_vsetz12z12_bglt BgL_tmp1422z00_6726;
						BgL_vsetz12zf2cinfoze0_bglt BgL_wide1423z00_6727;

						{
							BgL_vsetz12z12_bglt BgL_auxz00_10326;

							{	/* Cfa/cinfo3.scm 38 */
								BgL_vsetz12z12_bglt BgL_new1421z00_6728;

								BgL_new1421z00_6728 =
									((BgL_vsetz12z12_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_vsetz12z12_bgl))));
								{	/* Cfa/cinfo3.scm 38 */
									long BgL_arg1978z00_6729;

									BgL_arg1978z00_6729 =
										BGL_CLASS_NUM(BGl_vsetz12z12zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1421z00_6728),
										BgL_arg1978z00_6729);
								}
								{	/* Cfa/cinfo3.scm 38 */
									BgL_objectz00_bglt BgL_tmpz00_10331;

									BgL_tmpz00_10331 = ((BgL_objectz00_bglt) BgL_new1421z00_6728);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10331, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1421z00_6728);
								BgL_auxz00_10326 = BgL_new1421z00_6728;
							}
							BgL_tmp1422z00_6726 = ((BgL_vsetz12z12_bglt) BgL_auxz00_10326);
						}
						BgL_wide1423z00_6727 =
							((BgL_vsetz12zf2cinfoze0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_vsetz12zf2cinfoze0_bgl))));
						{	/* Cfa/cinfo3.scm 38 */
							obj_t BgL_auxz00_10339;
							BgL_objectz00_bglt BgL_tmpz00_10337;

							BgL_auxz00_10339 = ((obj_t) BgL_wide1423z00_6727);
							BgL_tmpz00_10337 = ((BgL_objectz00_bglt) BgL_tmp1422z00_6726);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10337, BgL_auxz00_10339);
						}
						((BgL_objectz00_bglt) BgL_tmp1422z00_6726);
						{	/* Cfa/cinfo3.scm 38 */
							long BgL_arg1977z00_6730;

							BgL_arg1977z00_6730 =
								BGL_CLASS_NUM(BGl_vsetz12zf2Cinfoze0zzcfa_info3z00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1422z00_6726),
								BgL_arg1977z00_6730);
						}
						BgL_new1424z00_6725 = ((BgL_vsetz12z12_bglt) BgL_tmp1422z00_6726);
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_new1424z00_6725)))->BgL_locz00) =
						((obj_t) BgL_loc1247z00_6074), BUNSPEC);
					((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_new1424z00_6725)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1248z00_6075)),
						BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1424z00_6725)))->BgL_sidezd2effectzd2) =
						((obj_t) BgL_sidezd2effect1249zd2_6076), BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1424z00_6725)))->BgL_keyz00) =
						((obj_t) BgL_key1250z00_6077), BUNSPEC);
					((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
										BgL_new1424z00_6725)))->BgL_exprza2za2) =
						((obj_t) ((obj_t) BgL_exprza21251za2_6078)), BUNSPEC);
					((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
										BgL_new1424z00_6725)))->BgL_effectz00) =
						((obj_t) BgL_effect1252z00_6079), BUNSPEC);
					((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
										BgL_new1424z00_6725)))->BgL_czd2formatzd2) =
						((obj_t) ((obj_t) BgL_czd2format1253zd2_6080)), BUNSPEC);
					((((BgL_vsetz12z12_bglt) COBJECT(((BgL_vsetz12z12_bglt)
										BgL_new1424z00_6725)))->BgL_ftypez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_ftype1254z00_6081)),
						BUNSPEC);
					((((BgL_vsetz12z12_bglt) COBJECT(((BgL_vsetz12z12_bglt)
										BgL_new1424z00_6725)))->BgL_otypez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_otype1255z00_6082)),
						BUNSPEC);
					((((BgL_vsetz12z12_bglt) COBJECT(((BgL_vsetz12z12_bglt)
										BgL_new1424z00_6725)))->BgL_vtypez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vtype1256z00_6083)),
						BUNSPEC);
					((((BgL_vsetz12z12_bglt) COBJECT(((BgL_vsetz12z12_bglt)
										BgL_new1424z00_6725)))->BgL_unsafez00) =
						((bool_t) BgL_unsafe1257z00_6722), BUNSPEC);
					{
						BgL_vsetz12zf2cinfoze0_bglt BgL_auxz00_10375;

						{
							obj_t BgL_auxz00_10376;

							{	/* Cfa/cinfo3.scm 38 */
								BgL_objectz00_bglt BgL_tmpz00_10377;

								BgL_tmpz00_10377 = ((BgL_objectz00_bglt) BgL_new1424z00_6725);
								BgL_auxz00_10376 = BGL_OBJECT_WIDENING(BgL_tmpz00_10377);
							}
							BgL_auxz00_10375 =
								((BgL_vsetz12zf2cinfoze0_bglt) BgL_auxz00_10376);
						}
						((((BgL_vsetz12zf2cinfoze0_bglt) COBJECT(BgL_auxz00_10375))->
								BgL_approxz00) =
							((BgL_approxz00_bglt) ((BgL_approxz00_bglt)
									BgL_approx1258z00_6085)), BUNSPEC);
					}
					{
						BgL_vsetz12zf2cinfoze0_bglt BgL_auxz00_10383;

						{
							obj_t BgL_auxz00_10384;

							{	/* Cfa/cinfo3.scm 38 */
								BgL_objectz00_bglt BgL_tmpz00_10385;

								BgL_tmpz00_10385 = ((BgL_objectz00_bglt) BgL_new1424z00_6725);
								BgL_auxz00_10384 = BGL_OBJECT_WIDENING(BgL_tmpz00_10385);
							}
							BgL_auxz00_10383 =
								((BgL_vsetz12zf2cinfoze0_bglt) BgL_auxz00_10384);
						}
						((((BgL_vsetz12zf2cinfoze0_bglt) COBJECT(BgL_auxz00_10383))->
								BgL_tvectorzf3zf3) =
							((bool_t) BgL_tvectorzf31259zf3_6724), BUNSPEC);
					}
					return BgL_new1424z00_6725;
				}
			}
		}

	}



/* &<@anonymous:1997> */
	obj_t BGl_z62zc3z04anonymousza31997ze3ze5zzcfa_info3z00(obj_t BgL_envz00_6087)
	{
		{	/* Cfa/cinfo3.scm 38 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1996 */
	obj_t BGl_z62lambda1996z62zzcfa_info3z00(obj_t BgL_envz00_6088,
		obj_t BgL_oz00_6089, obj_t BgL_vz00_6090)
	{
		{	/* Cfa/cinfo3.scm 38 */
			{	/* Cfa/cinfo3.scm 38 */
				bool_t BgL_vz00_6732;

				BgL_vz00_6732 = CBOOL(BgL_vz00_6090);
				{
					BgL_vsetz12zf2cinfoze0_bglt BgL_auxz00_10392;

					{
						obj_t BgL_auxz00_10393;

						{	/* Cfa/cinfo3.scm 38 */
							BgL_objectz00_bglt BgL_tmpz00_10394;

							BgL_tmpz00_10394 =
								((BgL_objectz00_bglt) ((BgL_vsetz12z12_bglt) BgL_oz00_6089));
							BgL_auxz00_10393 = BGL_OBJECT_WIDENING(BgL_tmpz00_10394);
						}
						BgL_auxz00_10392 = ((BgL_vsetz12zf2cinfoze0_bglt) BgL_auxz00_10393);
					}
					return
						((((BgL_vsetz12zf2cinfoze0_bglt) COBJECT(BgL_auxz00_10392))->
							BgL_tvectorzf3zf3) = ((bool_t) BgL_vz00_6732), BUNSPEC);
				}
			}
		}

	}



/* &lambda1995 */
	obj_t BGl_z62lambda1995z62zzcfa_info3z00(obj_t BgL_envz00_6091,
		obj_t BgL_oz00_6092)
	{
		{	/* Cfa/cinfo3.scm 38 */
			{	/* Cfa/cinfo3.scm 38 */
				bool_t BgL_tmpz00_10400;

				{
					BgL_vsetz12zf2cinfoze0_bglt BgL_auxz00_10401;

					{
						obj_t BgL_auxz00_10402;

						{	/* Cfa/cinfo3.scm 38 */
							BgL_objectz00_bglt BgL_tmpz00_10403;

							BgL_tmpz00_10403 =
								((BgL_objectz00_bglt) ((BgL_vsetz12z12_bglt) BgL_oz00_6092));
							BgL_auxz00_10402 = BGL_OBJECT_WIDENING(BgL_tmpz00_10403);
						}
						BgL_auxz00_10401 = ((BgL_vsetz12zf2cinfoze0_bglt) BgL_auxz00_10402);
					}
					BgL_tmpz00_10400 =
						(((BgL_vsetz12zf2cinfoze0_bglt) COBJECT(BgL_auxz00_10401))->
						BgL_tvectorzf3zf3);
				}
				return BBOOL(BgL_tmpz00_10400);
			}
		}

	}



/* &lambda1990 */
	obj_t BGl_z62lambda1990z62zzcfa_info3z00(obj_t BgL_envz00_6093,
		obj_t BgL_oz00_6094, obj_t BgL_vz00_6095)
	{
		{	/* Cfa/cinfo3.scm 38 */
			{
				BgL_vsetz12zf2cinfoze0_bglt BgL_auxz00_10410;

				{
					obj_t BgL_auxz00_10411;

					{	/* Cfa/cinfo3.scm 38 */
						BgL_objectz00_bglt BgL_tmpz00_10412;

						BgL_tmpz00_10412 =
							((BgL_objectz00_bglt) ((BgL_vsetz12z12_bglt) BgL_oz00_6094));
						BgL_auxz00_10411 = BGL_OBJECT_WIDENING(BgL_tmpz00_10412);
					}
					BgL_auxz00_10410 = ((BgL_vsetz12zf2cinfoze0_bglt) BgL_auxz00_10411);
				}
				return
					((((BgL_vsetz12zf2cinfoze0_bglt) COBJECT(BgL_auxz00_10410))->
						BgL_approxz00) =
					((BgL_approxz00_bglt) ((BgL_approxz00_bglt) BgL_vz00_6095)), BUNSPEC);
			}
		}

	}



/* &lambda1989 */
	BgL_approxz00_bglt BGl_z62lambda1989z62zzcfa_info3z00(obj_t BgL_envz00_6096,
		obj_t BgL_oz00_6097)
	{
		{	/* Cfa/cinfo3.scm 38 */
			{
				BgL_vsetz12zf2cinfoze0_bglt BgL_auxz00_10419;

				{
					obj_t BgL_auxz00_10420;

					{	/* Cfa/cinfo3.scm 38 */
						BgL_objectz00_bglt BgL_tmpz00_10421;

						BgL_tmpz00_10421 =
							((BgL_objectz00_bglt) ((BgL_vsetz12z12_bglt) BgL_oz00_6097));
						BgL_auxz00_10420 = BGL_OBJECT_WIDENING(BgL_tmpz00_10421);
					}
					BgL_auxz00_10419 = ((BgL_vsetz12zf2cinfoze0_bglt) BgL_auxz00_10420);
				}
				return
					(((BgL_vsetz12zf2cinfoze0_bglt) COBJECT(BgL_auxz00_10419))->
					BgL_approxz00);
			}
		}

	}



/* &lambda1954 */
	BgL_vrefz00_bglt BGl_z62lambda1954z62zzcfa_info3z00(obj_t BgL_envz00_6098,
		obj_t BgL_o1245z00_6099)
	{
		{	/* Cfa/cinfo3.scm 35 */
			{	/* Cfa/cinfo3.scm 35 */
				long BgL_arg1955z00_6738;

				{	/* Cfa/cinfo3.scm 35 */
					obj_t BgL_arg1956z00_6739;

					{	/* Cfa/cinfo3.scm 35 */
						obj_t BgL_arg1957z00_6740;

						{	/* Cfa/cinfo3.scm 35 */
							obj_t BgL_arg1815z00_6741;
							long BgL_arg1816z00_6742;

							BgL_arg1815z00_6741 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Cfa/cinfo3.scm 35 */
								long BgL_arg1817z00_6743;

								BgL_arg1817z00_6743 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_vrefz00_bglt) BgL_o1245z00_6099)));
								BgL_arg1816z00_6742 = (BgL_arg1817z00_6743 - OBJECT_TYPE);
							}
							BgL_arg1957z00_6740 =
								VECTOR_REF(BgL_arg1815z00_6741, BgL_arg1816z00_6742);
						}
						BgL_arg1956z00_6739 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1957z00_6740);
					}
					{	/* Cfa/cinfo3.scm 35 */
						obj_t BgL_tmpz00_10434;

						BgL_tmpz00_10434 = ((obj_t) BgL_arg1956z00_6739);
						BgL_arg1955z00_6738 = BGL_CLASS_NUM(BgL_tmpz00_10434);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_vrefz00_bglt) BgL_o1245z00_6099)), BgL_arg1955z00_6738);
			}
			{	/* Cfa/cinfo3.scm 35 */
				BgL_objectz00_bglt BgL_tmpz00_10440;

				BgL_tmpz00_10440 =
					((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_o1245z00_6099));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10440, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_o1245z00_6099));
			return ((BgL_vrefz00_bglt) ((BgL_vrefz00_bglt) BgL_o1245z00_6099));
		}

	}



/* &<@anonymous:1953> */
	obj_t BGl_z62zc3z04anonymousza31953ze3ze5zzcfa_info3z00(obj_t BgL_envz00_6100,
		obj_t BgL_new1244z00_6101)
	{
		{	/* Cfa/cinfo3.scm 35 */
			{
				BgL_vrefz00_bglt BgL_auxz00_10448;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_vrefz00_bglt) BgL_new1244z00_6101))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10452;

					{	/* Cfa/cinfo3.scm 35 */
						obj_t BgL_classz00_6745;

						BgL_classz00_6745 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 35 */
							obj_t BgL__ortest_1117z00_6746;

							BgL__ortest_1117z00_6746 = BGL_CLASS_NIL(BgL_classz00_6745);
							if (CBOOL(BgL__ortest_1117z00_6746))
								{	/* Cfa/cinfo3.scm 35 */
									BgL_auxz00_10452 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6746);
								}
							else
								{	/* Cfa/cinfo3.scm 35 */
									BgL_auxz00_10452 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6745));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_vrefz00_bglt) BgL_new1244z00_6101))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_10452), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_vrefz00_bglt) BgL_new1244z00_6101))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_vrefz00_bglt)
										BgL_new1244z00_6101))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_vrefz00_bglt)
										BgL_new1244z00_6101))))->BgL_exprza2za2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_vrefz00_bglt)
										BgL_new1244z00_6101))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_privatez00_bglt)
							COBJECT(((BgL_privatez00_bglt) ((BgL_vrefz00_bglt)
										BgL_new1244z00_6101))))->BgL_czd2formatzd2) =
					((obj_t) BGl_string2364z00zzcfa_info3z00), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10477;

					{	/* Cfa/cinfo3.scm 35 */
						obj_t BgL_classz00_6747;

						BgL_classz00_6747 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 35 */
							obj_t BgL__ortest_1117z00_6748;

							BgL__ortest_1117z00_6748 = BGL_CLASS_NIL(BgL_classz00_6747);
							if (CBOOL(BgL__ortest_1117z00_6748))
								{	/* Cfa/cinfo3.scm 35 */
									BgL_auxz00_10477 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6748);
								}
							else
								{	/* Cfa/cinfo3.scm 35 */
									BgL_auxz00_10477 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6747));
								}
						}
					}
					((((BgL_vrefz00_bglt) COBJECT(
									((BgL_vrefz00_bglt)
										((BgL_vrefz00_bglt) BgL_new1244z00_6101))))->BgL_ftypez00) =
						((BgL_typez00_bglt) BgL_auxz00_10477), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_10487;

					{	/* Cfa/cinfo3.scm 35 */
						obj_t BgL_classz00_6749;

						BgL_classz00_6749 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 35 */
							obj_t BgL__ortest_1117z00_6750;

							BgL__ortest_1117z00_6750 = BGL_CLASS_NIL(BgL_classz00_6749);
							if (CBOOL(BgL__ortest_1117z00_6750))
								{	/* Cfa/cinfo3.scm 35 */
									BgL_auxz00_10487 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6750);
								}
							else
								{	/* Cfa/cinfo3.scm 35 */
									BgL_auxz00_10487 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6749));
								}
						}
					}
					((((BgL_vrefz00_bglt) COBJECT(
									((BgL_vrefz00_bglt)
										((BgL_vrefz00_bglt) BgL_new1244z00_6101))))->BgL_otypez00) =
						((BgL_typez00_bglt) BgL_auxz00_10487), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_10497;

					{	/* Cfa/cinfo3.scm 35 */
						obj_t BgL_classz00_6751;

						BgL_classz00_6751 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 35 */
							obj_t BgL__ortest_1117z00_6752;

							BgL__ortest_1117z00_6752 = BGL_CLASS_NIL(BgL_classz00_6751);
							if (CBOOL(BgL__ortest_1117z00_6752))
								{	/* Cfa/cinfo3.scm 35 */
									BgL_auxz00_10497 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6752);
								}
							else
								{	/* Cfa/cinfo3.scm 35 */
									BgL_auxz00_10497 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6751));
								}
						}
					}
					((((BgL_vrefz00_bglt) COBJECT(
									((BgL_vrefz00_bglt)
										((BgL_vrefz00_bglt) BgL_new1244z00_6101))))->BgL_vtypez00) =
						((BgL_typez00_bglt) BgL_auxz00_10497), BUNSPEC);
				}
				((((BgL_vrefz00_bglt) COBJECT(
								((BgL_vrefz00_bglt)
									((BgL_vrefz00_bglt) BgL_new1244z00_6101))))->BgL_unsafez00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				{
					BgL_approxz00_bglt BgL_auxz00_10517;
					BgL_vrefzf2cinfozf2_bglt BgL_auxz00_10510;

					{	/* Cfa/cinfo3.scm 35 */
						obj_t BgL_classz00_6753;

						BgL_classz00_6753 = BGl_approxz00zzcfa_infoz00;
						{	/* Cfa/cinfo3.scm 35 */
							obj_t BgL__ortest_1117z00_6754;

							BgL__ortest_1117z00_6754 = BGL_CLASS_NIL(BgL_classz00_6753);
							if (CBOOL(BgL__ortest_1117z00_6754))
								{	/* Cfa/cinfo3.scm 35 */
									BgL_auxz00_10517 =
										((BgL_approxz00_bglt) BgL__ortest_1117z00_6754);
								}
							else
								{	/* Cfa/cinfo3.scm 35 */
									BgL_auxz00_10517 =
										((BgL_approxz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6753));
								}
						}
					}
					{
						obj_t BgL_auxz00_10511;

						{	/* Cfa/cinfo3.scm 35 */
							BgL_objectz00_bglt BgL_tmpz00_10512;

							BgL_tmpz00_10512 =
								((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_new1244z00_6101));
							BgL_auxz00_10511 = BGL_OBJECT_WIDENING(BgL_tmpz00_10512);
						}
						BgL_auxz00_10510 = ((BgL_vrefzf2cinfozf2_bglt) BgL_auxz00_10511);
					}
					((((BgL_vrefzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10510))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_auxz00_10517), BUNSPEC);
				}
				{
					BgL_vrefzf2cinfozf2_bglt BgL_auxz00_10525;

					{
						obj_t BgL_auxz00_10526;

						{	/* Cfa/cinfo3.scm 35 */
							BgL_objectz00_bglt BgL_tmpz00_10527;

							BgL_tmpz00_10527 =
								((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_new1244z00_6101));
							BgL_auxz00_10526 = BGL_OBJECT_WIDENING(BgL_tmpz00_10527);
						}
						BgL_auxz00_10525 = ((BgL_vrefzf2cinfozf2_bglt) BgL_auxz00_10526);
					}
					((((BgL_vrefzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10525))->
							BgL_tvectorzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				BgL_auxz00_10448 = ((BgL_vrefz00_bglt) BgL_new1244z00_6101);
				return ((obj_t) BgL_auxz00_10448);
			}
		}

	}



/* &lambda1951 */
	BgL_vrefz00_bglt BGl_z62lambda1951z62zzcfa_info3z00(obj_t BgL_envz00_6102,
		obj_t BgL_o1241z00_6103)
	{
		{	/* Cfa/cinfo3.scm 35 */
			{	/* Cfa/cinfo3.scm 35 */
				BgL_vrefzf2cinfozf2_bglt BgL_wide1243z00_6756;

				BgL_wide1243z00_6756 =
					((BgL_vrefzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_vrefzf2cinfozf2_bgl))));
				{	/* Cfa/cinfo3.scm 35 */
					obj_t BgL_auxz00_10540;
					BgL_objectz00_bglt BgL_tmpz00_10536;

					BgL_auxz00_10540 = ((obj_t) BgL_wide1243z00_6756);
					BgL_tmpz00_10536 =
						((BgL_objectz00_bglt)
						((BgL_vrefz00_bglt) ((BgL_vrefz00_bglt) BgL_o1241z00_6103)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10536, BgL_auxz00_10540);
				}
				((BgL_objectz00_bglt)
					((BgL_vrefz00_bglt) ((BgL_vrefz00_bglt) BgL_o1241z00_6103)));
				{	/* Cfa/cinfo3.scm 35 */
					long BgL_arg1952z00_6757;

					BgL_arg1952z00_6757 =
						BGL_CLASS_NUM(BGl_vrefzf2Cinfozf2zzcfa_info3z00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_vrefz00_bglt)
								((BgL_vrefz00_bglt) BgL_o1241z00_6103))), BgL_arg1952z00_6757);
				}
				return
					((BgL_vrefz00_bglt)
					((BgL_vrefz00_bglt) ((BgL_vrefz00_bglt) BgL_o1241z00_6103)));
			}
		}

	}



/* &lambda1948 */
	BgL_vrefz00_bglt BGl_z62lambda1948z62zzcfa_info3z00(obj_t BgL_envz00_6104,
		obj_t BgL_loc1228z00_6105, obj_t BgL_type1229z00_6106,
		obj_t BgL_sidezd2effect1230zd2_6107, obj_t BgL_key1231z00_6108,
		obj_t BgL_exprza21232za2_6109, obj_t BgL_effect1233z00_6110,
		obj_t BgL_czd2format1234zd2_6111, obj_t BgL_ftype1235z00_6112,
		obj_t BgL_otype1236z00_6113, obj_t BgL_vtype1237z00_6114,
		obj_t BgL_unsafe1238z00_6115, obj_t BgL_approx1239z00_6116,
		obj_t BgL_tvectorzf31240zf3_6117)
	{
		{	/* Cfa/cinfo3.scm 35 */
			{	/* Cfa/cinfo3.scm 35 */
				bool_t BgL_unsafe1238z00_6764;
				bool_t BgL_tvectorzf31240zf3_6766;

				BgL_unsafe1238z00_6764 = CBOOL(BgL_unsafe1238z00_6115);
				BgL_tvectorzf31240zf3_6766 = CBOOL(BgL_tvectorzf31240zf3_6117);
				{	/* Cfa/cinfo3.scm 35 */
					BgL_vrefz00_bglt BgL_new1419z00_6767;

					{	/* Cfa/cinfo3.scm 35 */
						BgL_vrefz00_bglt BgL_tmp1417z00_6768;
						BgL_vrefzf2cinfozf2_bglt BgL_wide1418z00_6769;

						{
							BgL_vrefz00_bglt BgL_auxz00_10556;

							{	/* Cfa/cinfo3.scm 35 */
								BgL_vrefz00_bglt BgL_new1416z00_6770;

								BgL_new1416z00_6770 =
									((BgL_vrefz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_vrefz00_bgl))));
								{	/* Cfa/cinfo3.scm 35 */
									long BgL_arg1950z00_6771;

									BgL_arg1950z00_6771 = BGL_CLASS_NUM(BGl_vrefz00zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1416z00_6770),
										BgL_arg1950z00_6771);
								}
								{	/* Cfa/cinfo3.scm 35 */
									BgL_objectz00_bglt BgL_tmpz00_10561;

									BgL_tmpz00_10561 = ((BgL_objectz00_bglt) BgL_new1416z00_6770);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10561, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1416z00_6770);
								BgL_auxz00_10556 = BgL_new1416z00_6770;
							}
							BgL_tmp1417z00_6768 = ((BgL_vrefz00_bglt) BgL_auxz00_10556);
						}
						BgL_wide1418z00_6769 =
							((BgL_vrefzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_vrefzf2cinfozf2_bgl))));
						{	/* Cfa/cinfo3.scm 35 */
							obj_t BgL_auxz00_10569;
							BgL_objectz00_bglt BgL_tmpz00_10567;

							BgL_auxz00_10569 = ((obj_t) BgL_wide1418z00_6769);
							BgL_tmpz00_10567 = ((BgL_objectz00_bglt) BgL_tmp1417z00_6768);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10567, BgL_auxz00_10569);
						}
						((BgL_objectz00_bglt) BgL_tmp1417z00_6768);
						{	/* Cfa/cinfo3.scm 35 */
							long BgL_arg1949z00_6772;

							BgL_arg1949z00_6772 =
								BGL_CLASS_NUM(BGl_vrefzf2Cinfozf2zzcfa_info3z00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1417z00_6768),
								BgL_arg1949z00_6772);
						}
						BgL_new1419z00_6767 = ((BgL_vrefz00_bglt) BgL_tmp1417z00_6768);
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_new1419z00_6767)))->BgL_locz00) =
						((obj_t) BgL_loc1228z00_6105), BUNSPEC);
					((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_new1419z00_6767)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1229z00_6106)),
						BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1419z00_6767)))->BgL_sidezd2effectzd2) =
						((obj_t) BgL_sidezd2effect1230zd2_6107), BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1419z00_6767)))->BgL_keyz00) =
						((obj_t) BgL_key1231z00_6108), BUNSPEC);
					((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
										BgL_new1419z00_6767)))->BgL_exprza2za2) =
						((obj_t) ((obj_t) BgL_exprza21232za2_6109)), BUNSPEC);
					((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
										BgL_new1419z00_6767)))->BgL_effectz00) =
						((obj_t) BgL_effect1233z00_6110), BUNSPEC);
					((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
										BgL_new1419z00_6767)))->BgL_czd2formatzd2) =
						((obj_t) ((obj_t) BgL_czd2format1234zd2_6111)), BUNSPEC);
					((((BgL_vrefz00_bglt) COBJECT(((BgL_vrefz00_bglt)
										BgL_new1419z00_6767)))->BgL_ftypez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_ftype1235z00_6112)),
						BUNSPEC);
					((((BgL_vrefz00_bglt) COBJECT(((BgL_vrefz00_bglt)
										BgL_new1419z00_6767)))->BgL_otypez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_otype1236z00_6113)),
						BUNSPEC);
					((((BgL_vrefz00_bglt) COBJECT(((BgL_vrefz00_bglt)
										BgL_new1419z00_6767)))->BgL_vtypez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vtype1237z00_6114)),
						BUNSPEC);
					((((BgL_vrefz00_bglt) COBJECT(((BgL_vrefz00_bglt)
										BgL_new1419z00_6767)))->BgL_unsafez00) =
						((bool_t) BgL_unsafe1238z00_6764), BUNSPEC);
					{
						BgL_vrefzf2cinfozf2_bglt BgL_auxz00_10605;

						{
							obj_t BgL_auxz00_10606;

							{	/* Cfa/cinfo3.scm 35 */
								BgL_objectz00_bglt BgL_tmpz00_10607;

								BgL_tmpz00_10607 = ((BgL_objectz00_bglt) BgL_new1419z00_6767);
								BgL_auxz00_10606 = BGL_OBJECT_WIDENING(BgL_tmpz00_10607);
							}
							BgL_auxz00_10605 = ((BgL_vrefzf2cinfozf2_bglt) BgL_auxz00_10606);
						}
						((((BgL_vrefzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10605))->
								BgL_approxz00) =
							((BgL_approxz00_bglt) ((BgL_approxz00_bglt)
									BgL_approx1239z00_6116)), BUNSPEC);
					}
					{
						BgL_vrefzf2cinfozf2_bglt BgL_auxz00_10613;

						{
							obj_t BgL_auxz00_10614;

							{	/* Cfa/cinfo3.scm 35 */
								BgL_objectz00_bglt BgL_tmpz00_10615;

								BgL_tmpz00_10615 = ((BgL_objectz00_bglt) BgL_new1419z00_6767);
								BgL_auxz00_10614 = BGL_OBJECT_WIDENING(BgL_tmpz00_10615);
							}
							BgL_auxz00_10613 = ((BgL_vrefzf2cinfozf2_bglt) BgL_auxz00_10614);
						}
						((((BgL_vrefzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10613))->
								BgL_tvectorzf3zf3) =
							((bool_t) BgL_tvectorzf31240zf3_6766), BUNSPEC);
					}
					return BgL_new1419z00_6767;
				}
			}
		}

	}



/* &<@anonymous:1969> */
	obj_t BGl_z62zc3z04anonymousza31969ze3ze5zzcfa_info3z00(obj_t BgL_envz00_6118)
	{
		{	/* Cfa/cinfo3.scm 35 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1968 */
	obj_t BGl_z62lambda1968z62zzcfa_info3z00(obj_t BgL_envz00_6119,
		obj_t BgL_oz00_6120, obj_t BgL_vz00_6121)
	{
		{	/* Cfa/cinfo3.scm 35 */
			{	/* Cfa/cinfo3.scm 35 */
				bool_t BgL_vz00_6774;

				BgL_vz00_6774 = CBOOL(BgL_vz00_6121);
				{
					BgL_vrefzf2cinfozf2_bglt BgL_auxz00_10622;

					{
						obj_t BgL_auxz00_10623;

						{	/* Cfa/cinfo3.scm 35 */
							BgL_objectz00_bglt BgL_tmpz00_10624;

							BgL_tmpz00_10624 =
								((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_oz00_6120));
							BgL_auxz00_10623 = BGL_OBJECT_WIDENING(BgL_tmpz00_10624);
						}
						BgL_auxz00_10622 = ((BgL_vrefzf2cinfozf2_bglt) BgL_auxz00_10623);
					}
					return
						((((BgL_vrefzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10622))->
							BgL_tvectorzf3zf3) = ((bool_t) BgL_vz00_6774), BUNSPEC);
				}
			}
		}

	}



/* &lambda1967 */
	obj_t BGl_z62lambda1967z62zzcfa_info3z00(obj_t BgL_envz00_6122,
		obj_t BgL_oz00_6123)
	{
		{	/* Cfa/cinfo3.scm 35 */
			{	/* Cfa/cinfo3.scm 35 */
				bool_t BgL_tmpz00_10630;

				{
					BgL_vrefzf2cinfozf2_bglt BgL_auxz00_10631;

					{
						obj_t BgL_auxz00_10632;

						{	/* Cfa/cinfo3.scm 35 */
							BgL_objectz00_bglt BgL_tmpz00_10633;

							BgL_tmpz00_10633 =
								((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_oz00_6123));
							BgL_auxz00_10632 = BGL_OBJECT_WIDENING(BgL_tmpz00_10633);
						}
						BgL_auxz00_10631 = ((BgL_vrefzf2cinfozf2_bglt) BgL_auxz00_10632);
					}
					BgL_tmpz00_10630 =
						(((BgL_vrefzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10631))->
						BgL_tvectorzf3zf3);
				}
				return BBOOL(BgL_tmpz00_10630);
			}
		}

	}



/* &lambda1962 */
	obj_t BGl_z62lambda1962z62zzcfa_info3z00(obj_t BgL_envz00_6124,
		obj_t BgL_oz00_6125, obj_t BgL_vz00_6126)
	{
		{	/* Cfa/cinfo3.scm 35 */
			{
				BgL_vrefzf2cinfozf2_bglt BgL_auxz00_10640;

				{
					obj_t BgL_auxz00_10641;

					{	/* Cfa/cinfo3.scm 35 */
						BgL_objectz00_bglt BgL_tmpz00_10642;

						BgL_tmpz00_10642 =
							((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_oz00_6125));
						BgL_auxz00_10641 = BGL_OBJECT_WIDENING(BgL_tmpz00_10642);
					}
					BgL_auxz00_10640 = ((BgL_vrefzf2cinfozf2_bglt) BgL_auxz00_10641);
				}
				return
					((((BgL_vrefzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10640))->
						BgL_approxz00) =
					((BgL_approxz00_bglt) ((BgL_approxz00_bglt) BgL_vz00_6126)), BUNSPEC);
			}
		}

	}



/* &lambda1961 */
	BgL_approxz00_bglt BGl_z62lambda1961z62zzcfa_info3z00(obj_t BgL_envz00_6127,
		obj_t BgL_oz00_6128)
	{
		{	/* Cfa/cinfo3.scm 35 */
			{
				BgL_vrefzf2cinfozf2_bglt BgL_auxz00_10649;

				{
					obj_t BgL_auxz00_10650;

					{	/* Cfa/cinfo3.scm 35 */
						BgL_objectz00_bglt BgL_tmpz00_10651;

						BgL_tmpz00_10651 =
							((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_oz00_6128));
						BgL_auxz00_10650 = BGL_OBJECT_WIDENING(BgL_tmpz00_10651);
					}
					BgL_auxz00_10649 = ((BgL_vrefzf2cinfozf2_bglt) BgL_auxz00_10650);
				}
				return
					(((BgL_vrefzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10649))->
					BgL_approxz00);
			}
		}

	}



/* &lambda1933 */
	BgL_castzd2nullzd2_bglt BGl_z62lambda1933z62zzcfa_info3z00(obj_t
		BgL_envz00_6129, obj_t BgL_o1226z00_6130)
	{
		{	/* Cfa/cinfo3.scm 32 */
			{	/* Cfa/cinfo3.scm 32 */
				long BgL_arg1934z00_6780;

				{	/* Cfa/cinfo3.scm 32 */
					obj_t BgL_arg1935z00_6781;

					{	/* Cfa/cinfo3.scm 32 */
						obj_t BgL_arg1936z00_6782;

						{	/* Cfa/cinfo3.scm 32 */
							obj_t BgL_arg1815z00_6783;
							long BgL_arg1816z00_6784;

							BgL_arg1815z00_6783 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Cfa/cinfo3.scm 32 */
								long BgL_arg1817z00_6785;

								BgL_arg1817z00_6785 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_castzd2nullzd2_bglt) BgL_o1226z00_6130)));
								BgL_arg1816z00_6784 = (BgL_arg1817z00_6785 - OBJECT_TYPE);
							}
							BgL_arg1936z00_6782 =
								VECTOR_REF(BgL_arg1815z00_6783, BgL_arg1816z00_6784);
						}
						BgL_arg1935z00_6781 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1936z00_6782);
					}
					{	/* Cfa/cinfo3.scm 32 */
						obj_t BgL_tmpz00_10664;

						BgL_tmpz00_10664 = ((obj_t) BgL_arg1935z00_6781);
						BgL_arg1934z00_6780 = BGL_CLASS_NUM(BgL_tmpz00_10664);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_castzd2nullzd2_bglt) BgL_o1226z00_6130)),
					BgL_arg1934z00_6780);
			}
			{	/* Cfa/cinfo3.scm 32 */
				BgL_objectz00_bglt BgL_tmpz00_10670;

				BgL_tmpz00_10670 =
					((BgL_objectz00_bglt) ((BgL_castzd2nullzd2_bglt) BgL_o1226z00_6130));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10670, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_castzd2nullzd2_bglt) BgL_o1226z00_6130));
			return
				((BgL_castzd2nullzd2_bglt)
				((BgL_castzd2nullzd2_bglt) BgL_o1226z00_6130));
		}

	}



/* &<@anonymous:1932> */
	obj_t BGl_z62zc3z04anonymousza31932ze3ze5zzcfa_info3z00(obj_t BgL_envz00_6131,
		obj_t BgL_new1225z00_6132)
	{
		{	/* Cfa/cinfo3.scm 32 */
			{
				BgL_castzd2nullzd2_bglt BgL_auxz00_10678;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_castzd2nullzd2_bglt) BgL_new1225z00_6132))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10682;

					{	/* Cfa/cinfo3.scm 32 */
						obj_t BgL_classz00_6787;

						BgL_classz00_6787 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 32 */
							obj_t BgL__ortest_1117z00_6788;

							BgL__ortest_1117z00_6788 = BGL_CLASS_NIL(BgL_classz00_6787);
							if (CBOOL(BgL__ortest_1117z00_6788))
								{	/* Cfa/cinfo3.scm 32 */
									BgL_auxz00_10682 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6788);
								}
							else
								{	/* Cfa/cinfo3.scm 32 */
									BgL_auxz00_10682 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6787));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_castzd2nullzd2_bglt) BgL_new1225z00_6132))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_10682), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_castzd2nullzd2_bglt) BgL_new1225z00_6132))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_castzd2nullzd2_bglt)
										BgL_new1225z00_6132))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_castzd2nullzd2_bglt)
										BgL_new1225z00_6132))))->BgL_exprza2za2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_castzd2nullzd2_bglt)
										BgL_new1225z00_6132))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_privatez00_bglt)
							COBJECT(((BgL_privatez00_bglt) ((BgL_castzd2nullzd2_bglt)
										BgL_new1225z00_6132))))->BgL_czd2formatzd2) =
					((obj_t) BGl_string2364z00zzcfa_info3z00), BUNSPEC);
				{
					BgL_approxz00_bglt BgL_auxz00_10714;
					BgL_castzd2nullzf2cinfoz20_bglt BgL_auxz00_10707;

					{	/* Cfa/cinfo3.scm 32 */
						obj_t BgL_classz00_6789;

						BgL_classz00_6789 = BGl_approxz00zzcfa_infoz00;
						{	/* Cfa/cinfo3.scm 32 */
							obj_t BgL__ortest_1117z00_6790;

							BgL__ortest_1117z00_6790 = BGL_CLASS_NIL(BgL_classz00_6789);
							if (CBOOL(BgL__ortest_1117z00_6790))
								{	/* Cfa/cinfo3.scm 32 */
									BgL_auxz00_10714 =
										((BgL_approxz00_bglt) BgL__ortest_1117z00_6790);
								}
							else
								{	/* Cfa/cinfo3.scm 32 */
									BgL_auxz00_10714 =
										((BgL_approxz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6789));
								}
						}
					}
					{
						obj_t BgL_auxz00_10708;

						{	/* Cfa/cinfo3.scm 32 */
							BgL_objectz00_bglt BgL_tmpz00_10709;

							BgL_tmpz00_10709 =
								((BgL_objectz00_bglt)
								((BgL_castzd2nullzd2_bglt) BgL_new1225z00_6132));
							BgL_auxz00_10708 = BGL_OBJECT_WIDENING(BgL_tmpz00_10709);
						}
						BgL_auxz00_10707 =
							((BgL_castzd2nullzf2cinfoz20_bglt) BgL_auxz00_10708);
					}
					((((BgL_castzd2nullzf2cinfoz20_bglt) COBJECT(BgL_auxz00_10707))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_auxz00_10714), BUNSPEC);
				}
				BgL_auxz00_10678 = ((BgL_castzd2nullzd2_bglt) BgL_new1225z00_6132);
				return ((obj_t) BgL_auxz00_10678);
			}
		}

	}



/* &lambda1930 */
	BgL_castzd2nullzd2_bglt BGl_z62lambda1930z62zzcfa_info3z00(obj_t
		BgL_envz00_6133, obj_t BgL_o1222z00_6134)
	{
		{	/* Cfa/cinfo3.scm 32 */
			{	/* Cfa/cinfo3.scm 32 */
				BgL_castzd2nullzf2cinfoz20_bglt BgL_wide1224z00_6792;

				BgL_wide1224z00_6792 =
					((BgL_castzd2nullzf2cinfoz20_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_castzd2nullzf2cinfoz20_bgl))));
				{	/* Cfa/cinfo3.scm 32 */
					obj_t BgL_auxz00_10729;
					BgL_objectz00_bglt BgL_tmpz00_10725;

					BgL_auxz00_10729 = ((obj_t) BgL_wide1224z00_6792);
					BgL_tmpz00_10725 =
						((BgL_objectz00_bglt)
						((BgL_castzd2nullzd2_bglt)
							((BgL_castzd2nullzd2_bglt) BgL_o1222z00_6134)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10725, BgL_auxz00_10729);
				}
				((BgL_objectz00_bglt)
					((BgL_castzd2nullzd2_bglt)
						((BgL_castzd2nullzd2_bglt) BgL_o1222z00_6134)));
				{	/* Cfa/cinfo3.scm 32 */
					long BgL_arg1931z00_6793;

					BgL_arg1931z00_6793 =
						BGL_CLASS_NUM(BGl_castzd2nullzf2Cinfoz20zzcfa_info3z00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_castzd2nullzd2_bglt)
								((BgL_castzd2nullzd2_bglt) BgL_o1222z00_6134))),
						BgL_arg1931z00_6793);
				}
				return
					((BgL_castzd2nullzd2_bglt)
					((BgL_castzd2nullzd2_bglt)
						((BgL_castzd2nullzd2_bglt) BgL_o1222z00_6134)));
			}
		}

	}



/* &lambda1927 */
	BgL_castzd2nullzd2_bglt BGl_z62lambda1927z62zzcfa_info3z00(obj_t
		BgL_envz00_6135, obj_t BgL_loc1214z00_6136, obj_t BgL_type1215z00_6137,
		obj_t BgL_sidezd2effect1216zd2_6138, obj_t BgL_key1217z00_6139,
		obj_t BgL_exprza21218za2_6140, obj_t BgL_effect1219z00_6141,
		obj_t BgL_czd2format1220zd2_6142, obj_t BgL_approx1221z00_6143)
	{
		{	/* Cfa/cinfo3.scm 32 */
			{	/* Cfa/cinfo3.scm 32 */
				BgL_castzd2nullzd2_bglt BgL_new1414z00_6798;

				{	/* Cfa/cinfo3.scm 32 */
					BgL_castzd2nullzd2_bglt BgL_tmp1412z00_6799;
					BgL_castzd2nullzf2cinfoz20_bglt BgL_wide1413z00_6800;

					{
						BgL_castzd2nullzd2_bglt BgL_auxz00_10743;

						{	/* Cfa/cinfo3.scm 32 */
							BgL_castzd2nullzd2_bglt BgL_new1411z00_6801;

							BgL_new1411z00_6801 =
								((BgL_castzd2nullzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_castzd2nullzd2_bgl))));
							{	/* Cfa/cinfo3.scm 32 */
								long BgL_arg1929z00_6802;

								BgL_arg1929z00_6802 =
									BGL_CLASS_NUM(BGl_castzd2nullzd2zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1411z00_6801),
									BgL_arg1929z00_6802);
							}
							{	/* Cfa/cinfo3.scm 32 */
								BgL_objectz00_bglt BgL_tmpz00_10748;

								BgL_tmpz00_10748 = ((BgL_objectz00_bglt) BgL_new1411z00_6801);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10748, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1411z00_6801);
							BgL_auxz00_10743 = BgL_new1411z00_6801;
						}
						BgL_tmp1412z00_6799 = ((BgL_castzd2nullzd2_bglt) BgL_auxz00_10743);
					}
					BgL_wide1413z00_6800 =
						((BgL_castzd2nullzf2cinfoz20_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_castzd2nullzf2cinfoz20_bgl))));
					{	/* Cfa/cinfo3.scm 32 */
						obj_t BgL_auxz00_10756;
						BgL_objectz00_bglt BgL_tmpz00_10754;

						BgL_auxz00_10756 = ((obj_t) BgL_wide1413z00_6800);
						BgL_tmpz00_10754 = ((BgL_objectz00_bglt) BgL_tmp1412z00_6799);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10754, BgL_auxz00_10756);
					}
					((BgL_objectz00_bglt) BgL_tmp1412z00_6799);
					{	/* Cfa/cinfo3.scm 32 */
						long BgL_arg1928z00_6803;

						BgL_arg1928z00_6803 =
							BGL_CLASS_NUM(BGl_castzd2nullzf2Cinfoz20zzcfa_info3z00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1412z00_6799), BgL_arg1928z00_6803);
					}
					BgL_new1414z00_6798 = ((BgL_castzd2nullzd2_bglt) BgL_tmp1412z00_6799);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1414z00_6798)))->BgL_locz00) =
					((obj_t) BgL_loc1214z00_6136), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1414z00_6798)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1215z00_6137)),
					BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1414z00_6798)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1216zd2_6138), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1414z00_6798)))->BgL_keyz00) =
					((obj_t) BgL_key1217z00_6139), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1414z00_6798)))->BgL_exprza2za2) =
					((obj_t) ((obj_t) BgL_exprza21218za2_6140)), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1414z00_6798)))->BgL_effectz00) =
					((obj_t) BgL_effect1219z00_6141), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1414z00_6798)))->BgL_czd2formatzd2) =
					((obj_t) ((obj_t) BgL_czd2format1220zd2_6142)), BUNSPEC);
				{
					BgL_castzd2nullzf2cinfoz20_bglt BgL_auxz00_10781;

					{
						obj_t BgL_auxz00_10782;

						{	/* Cfa/cinfo3.scm 32 */
							BgL_objectz00_bglt BgL_tmpz00_10783;

							BgL_tmpz00_10783 = ((BgL_objectz00_bglt) BgL_new1414z00_6798);
							BgL_auxz00_10782 = BGL_OBJECT_WIDENING(BgL_tmpz00_10783);
						}
						BgL_auxz00_10781 =
							((BgL_castzd2nullzf2cinfoz20_bglt) BgL_auxz00_10782);
					}
					((((BgL_castzd2nullzf2cinfoz20_bglt) COBJECT(BgL_auxz00_10781))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) ((BgL_approxz00_bglt)
								BgL_approx1221z00_6143)), BUNSPEC);
				}
				return BgL_new1414z00_6798;
			}
		}

	}



/* &lambda1941 */
	obj_t BGl_z62lambda1941z62zzcfa_info3z00(obj_t BgL_envz00_6144,
		obj_t BgL_oz00_6145, obj_t BgL_vz00_6146)
	{
		{	/* Cfa/cinfo3.scm 32 */
			{
				BgL_castzd2nullzf2cinfoz20_bglt BgL_auxz00_10789;

				{
					obj_t BgL_auxz00_10790;

					{	/* Cfa/cinfo3.scm 32 */
						BgL_objectz00_bglt BgL_tmpz00_10791;

						BgL_tmpz00_10791 =
							((BgL_objectz00_bglt) ((BgL_castzd2nullzd2_bglt) BgL_oz00_6145));
						BgL_auxz00_10790 = BGL_OBJECT_WIDENING(BgL_tmpz00_10791);
					}
					BgL_auxz00_10789 =
						((BgL_castzd2nullzf2cinfoz20_bglt) BgL_auxz00_10790);
				}
				return
					((((BgL_castzd2nullzf2cinfoz20_bglt) COBJECT(BgL_auxz00_10789))->
						BgL_approxz00) =
					((BgL_approxz00_bglt) ((BgL_approxz00_bglt) BgL_vz00_6146)), BUNSPEC);
			}
		}

	}



/* &lambda1940 */
	BgL_approxz00_bglt BGl_z62lambda1940z62zzcfa_info3z00(obj_t BgL_envz00_6147,
		obj_t BgL_oz00_6148)
	{
		{	/* Cfa/cinfo3.scm 32 */
			{
				BgL_castzd2nullzf2cinfoz20_bglt BgL_auxz00_10798;

				{
					obj_t BgL_auxz00_10799;

					{	/* Cfa/cinfo3.scm 32 */
						BgL_objectz00_bglt BgL_tmpz00_10800;

						BgL_tmpz00_10800 =
							((BgL_objectz00_bglt) ((BgL_castzd2nullzd2_bglt) BgL_oz00_6148));
						BgL_auxz00_10799 = BGL_OBJECT_WIDENING(BgL_tmpz00_10800);
					}
					BgL_auxz00_10798 =
						((BgL_castzd2nullzf2cinfoz20_bglt) BgL_auxz00_10799);
				}
				return
					(((BgL_castzd2nullzf2cinfoz20_bglt) COBJECT(BgL_auxz00_10798))->
					BgL_approxz00);
			}
		}

	}



/* &lambda1906 */
	BgL_instanceofz00_bglt BGl_z62lambda1906z62zzcfa_info3z00(obj_t
		BgL_envz00_6149, obj_t BgL_o1212z00_6150)
	{
		{	/* Cfa/cinfo3.scm 31 */
			{	/* Cfa/cinfo3.scm 31 */
				long BgL_arg1910z00_6808;

				{	/* Cfa/cinfo3.scm 31 */
					obj_t BgL_arg1911z00_6809;

					{	/* Cfa/cinfo3.scm 31 */
						obj_t BgL_arg1912z00_6810;

						{	/* Cfa/cinfo3.scm 31 */
							obj_t BgL_arg1815z00_6811;
							long BgL_arg1816z00_6812;

							BgL_arg1815z00_6811 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Cfa/cinfo3.scm 31 */
								long BgL_arg1817z00_6813;

								BgL_arg1817z00_6813 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_instanceofz00_bglt) BgL_o1212z00_6150)));
								BgL_arg1816z00_6812 = (BgL_arg1817z00_6813 - OBJECT_TYPE);
							}
							BgL_arg1912z00_6810 =
								VECTOR_REF(BgL_arg1815z00_6811, BgL_arg1816z00_6812);
						}
						BgL_arg1911z00_6809 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1912z00_6810);
					}
					{	/* Cfa/cinfo3.scm 31 */
						obj_t BgL_tmpz00_10813;

						BgL_tmpz00_10813 = ((obj_t) BgL_arg1911z00_6809);
						BgL_arg1910z00_6808 = BGL_CLASS_NUM(BgL_tmpz00_10813);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_instanceofz00_bglt) BgL_o1212z00_6150)), BgL_arg1910z00_6808);
			}
			{	/* Cfa/cinfo3.scm 31 */
				BgL_objectz00_bglt BgL_tmpz00_10819;

				BgL_tmpz00_10819 =
					((BgL_objectz00_bglt) ((BgL_instanceofz00_bglt) BgL_o1212z00_6150));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10819, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_instanceofz00_bglt) BgL_o1212z00_6150));
			return
				((BgL_instanceofz00_bglt) ((BgL_instanceofz00_bglt) BgL_o1212z00_6150));
		}

	}



/* &<@anonymous:1905> */
	obj_t BGl_z62zc3z04anonymousza31905ze3ze5zzcfa_info3z00(obj_t BgL_envz00_6151,
		obj_t BgL_new1211z00_6152)
	{
		{	/* Cfa/cinfo3.scm 31 */
			{
				BgL_instanceofz00_bglt BgL_auxz00_10827;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_instanceofz00_bglt) BgL_new1211z00_6152))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10831;

					{	/* Cfa/cinfo3.scm 31 */
						obj_t BgL_classz00_6815;

						BgL_classz00_6815 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 31 */
							obj_t BgL__ortest_1117z00_6816;

							BgL__ortest_1117z00_6816 = BGL_CLASS_NIL(BgL_classz00_6815);
							if (CBOOL(BgL__ortest_1117z00_6816))
								{	/* Cfa/cinfo3.scm 31 */
									BgL_auxz00_10831 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6816);
								}
							else
								{	/* Cfa/cinfo3.scm 31 */
									BgL_auxz00_10831 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6815));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_instanceofz00_bglt) BgL_new1211z00_6152))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_10831), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_instanceofz00_bglt) BgL_new1211z00_6152))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_instanceofz00_bglt)
										BgL_new1211z00_6152))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_instanceofz00_bglt)
										BgL_new1211z00_6152))))->BgL_exprza2za2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_instanceofz00_bglt)
										BgL_new1211z00_6152))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_privatez00_bglt)
							COBJECT(((BgL_privatez00_bglt) ((BgL_instanceofz00_bglt)
										BgL_new1211z00_6152))))->BgL_czd2formatzd2) =
					((obj_t) BGl_string2364z00zzcfa_info3z00), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10856;

					{	/* Cfa/cinfo3.scm 31 */
						obj_t BgL_classz00_6817;

						BgL_classz00_6817 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 31 */
							obj_t BgL__ortest_1117z00_6818;

							BgL__ortest_1117z00_6818 = BGL_CLASS_NIL(BgL_classz00_6817);
							if (CBOOL(BgL__ortest_1117z00_6818))
								{	/* Cfa/cinfo3.scm 31 */
									BgL_auxz00_10856 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6818);
								}
							else
								{	/* Cfa/cinfo3.scm 31 */
									BgL_auxz00_10856 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6817));
								}
						}
					}
					((((BgL_instanceofz00_bglt) COBJECT(
									((BgL_instanceofz00_bglt)
										((BgL_instanceofz00_bglt) BgL_new1211z00_6152))))->
							BgL_classz00) = ((BgL_typez00_bglt) BgL_auxz00_10856), BUNSPEC);
				}
				{
					BgL_approxz00_bglt BgL_auxz00_10873;
					BgL_instanceofzf2cinfozf2_bglt BgL_auxz00_10866;

					{	/* Cfa/cinfo3.scm 31 */
						obj_t BgL_classz00_6819;

						BgL_classz00_6819 = BGl_approxz00zzcfa_infoz00;
						{	/* Cfa/cinfo3.scm 31 */
							obj_t BgL__ortest_1117z00_6820;

							BgL__ortest_1117z00_6820 = BGL_CLASS_NIL(BgL_classz00_6819);
							if (CBOOL(BgL__ortest_1117z00_6820))
								{	/* Cfa/cinfo3.scm 31 */
									BgL_auxz00_10873 =
										((BgL_approxz00_bglt) BgL__ortest_1117z00_6820);
								}
							else
								{	/* Cfa/cinfo3.scm 31 */
									BgL_auxz00_10873 =
										((BgL_approxz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6819));
								}
						}
					}
					{
						obj_t BgL_auxz00_10867;

						{	/* Cfa/cinfo3.scm 31 */
							BgL_objectz00_bglt BgL_tmpz00_10868;

							BgL_tmpz00_10868 =
								((BgL_objectz00_bglt)
								((BgL_instanceofz00_bglt) BgL_new1211z00_6152));
							BgL_auxz00_10867 = BGL_OBJECT_WIDENING(BgL_tmpz00_10868);
						}
						BgL_auxz00_10866 =
							((BgL_instanceofzf2cinfozf2_bglt) BgL_auxz00_10867);
					}
					((((BgL_instanceofzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10866))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_auxz00_10873), BUNSPEC);
				}
				BgL_auxz00_10827 = ((BgL_instanceofz00_bglt) BgL_new1211z00_6152);
				return ((obj_t) BgL_auxz00_10827);
			}
		}

	}



/* &lambda1903 */
	BgL_instanceofz00_bglt BGl_z62lambda1903z62zzcfa_info3z00(obj_t
		BgL_envz00_6153, obj_t BgL_o1208z00_6154)
	{
		{	/* Cfa/cinfo3.scm 31 */
			{	/* Cfa/cinfo3.scm 31 */
				BgL_instanceofzf2cinfozf2_bglt BgL_wide1210z00_6822;

				BgL_wide1210z00_6822 =
					((BgL_instanceofzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_instanceofzf2cinfozf2_bgl))));
				{	/* Cfa/cinfo3.scm 31 */
					obj_t BgL_auxz00_10888;
					BgL_objectz00_bglt BgL_tmpz00_10884;

					BgL_auxz00_10888 = ((obj_t) BgL_wide1210z00_6822);
					BgL_tmpz00_10884 =
						((BgL_objectz00_bglt)
						((BgL_instanceofz00_bglt)
							((BgL_instanceofz00_bglt) BgL_o1208z00_6154)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10884, BgL_auxz00_10888);
				}
				((BgL_objectz00_bglt)
					((BgL_instanceofz00_bglt)
						((BgL_instanceofz00_bglt) BgL_o1208z00_6154)));
				{	/* Cfa/cinfo3.scm 31 */
					long BgL_arg1904z00_6823;

					BgL_arg1904z00_6823 =
						BGL_CLASS_NUM(BGl_instanceofzf2Cinfozf2zzcfa_info3z00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_instanceofz00_bglt)
								((BgL_instanceofz00_bglt) BgL_o1208z00_6154))),
						BgL_arg1904z00_6823);
				}
				return
					((BgL_instanceofz00_bglt)
					((BgL_instanceofz00_bglt)
						((BgL_instanceofz00_bglt) BgL_o1208z00_6154)));
			}
		}

	}



/* &lambda1899 */
	BgL_instanceofz00_bglt BGl_z62lambda1899z62zzcfa_info3z00(obj_t
		BgL_envz00_6155, obj_t BgL_loc1198z00_6156, obj_t BgL_type1199z00_6157,
		obj_t BgL_sidezd2effect1201zd2_6158, obj_t BgL_key1202z00_6159,
		obj_t BgL_exprza21203za2_6160, obj_t BgL_effect1204z00_6161,
		obj_t BgL_czd2format1205zd2_6162, obj_t BgL_class1206z00_6163,
		obj_t BgL_approx1207z00_6164)
	{
		{	/* Cfa/cinfo3.scm 31 */
			{	/* Cfa/cinfo3.scm 31 */
				BgL_instanceofz00_bglt BgL_new1409z00_6829;

				{	/* Cfa/cinfo3.scm 31 */
					BgL_instanceofz00_bglt BgL_tmp1407z00_6830;
					BgL_instanceofzf2cinfozf2_bglt BgL_wide1408z00_6831;

					{
						BgL_instanceofz00_bglt BgL_auxz00_10902;

						{	/* Cfa/cinfo3.scm 31 */
							BgL_instanceofz00_bglt BgL_new1406z00_6832;

							BgL_new1406z00_6832 =
								((BgL_instanceofz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_instanceofz00_bgl))));
							{	/* Cfa/cinfo3.scm 31 */
								long BgL_arg1902z00_6833;

								BgL_arg1902z00_6833 =
									BGL_CLASS_NUM(BGl_instanceofz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1406z00_6832),
									BgL_arg1902z00_6833);
							}
							{	/* Cfa/cinfo3.scm 31 */
								BgL_objectz00_bglt BgL_tmpz00_10907;

								BgL_tmpz00_10907 = ((BgL_objectz00_bglt) BgL_new1406z00_6832);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10907, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1406z00_6832);
							BgL_auxz00_10902 = BgL_new1406z00_6832;
						}
						BgL_tmp1407z00_6830 = ((BgL_instanceofz00_bglt) BgL_auxz00_10902);
					}
					BgL_wide1408z00_6831 =
						((BgL_instanceofzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_instanceofzf2cinfozf2_bgl))));
					{	/* Cfa/cinfo3.scm 31 */
						obj_t BgL_auxz00_10915;
						BgL_objectz00_bglt BgL_tmpz00_10913;

						BgL_auxz00_10915 = ((obj_t) BgL_wide1408z00_6831);
						BgL_tmpz00_10913 = ((BgL_objectz00_bglt) BgL_tmp1407z00_6830);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10913, BgL_auxz00_10915);
					}
					((BgL_objectz00_bglt) BgL_tmp1407z00_6830);
					{	/* Cfa/cinfo3.scm 31 */
						long BgL_arg1901z00_6834;

						BgL_arg1901z00_6834 =
							BGL_CLASS_NUM(BGl_instanceofzf2Cinfozf2zzcfa_info3z00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1407z00_6830), BgL_arg1901z00_6834);
					}
					BgL_new1409z00_6829 = ((BgL_instanceofz00_bglt) BgL_tmp1407z00_6830);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1409z00_6829)))->BgL_locz00) =
					((obj_t) BgL_loc1198z00_6156), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1409z00_6829)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1199z00_6157)),
					BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1409z00_6829)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1201zd2_6158), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1409z00_6829)))->BgL_keyz00) =
					((obj_t) BgL_key1202z00_6159), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1409z00_6829)))->BgL_exprza2za2) =
					((obj_t) ((obj_t) BgL_exprza21203za2_6160)), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1409z00_6829)))->BgL_effectz00) =
					((obj_t) BgL_effect1204z00_6161), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1409z00_6829)))->BgL_czd2formatzd2) =
					((obj_t) ((obj_t) BgL_czd2format1205zd2_6162)), BUNSPEC);
				((((BgL_instanceofz00_bglt) COBJECT(((BgL_instanceofz00_bglt)
									BgL_new1409z00_6829)))->BgL_classz00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_class1206z00_6163)),
					BUNSPEC);
				{
					BgL_instanceofzf2cinfozf2_bglt BgL_auxz00_10943;

					{
						obj_t BgL_auxz00_10944;

						{	/* Cfa/cinfo3.scm 31 */
							BgL_objectz00_bglt BgL_tmpz00_10945;

							BgL_tmpz00_10945 = ((BgL_objectz00_bglt) BgL_new1409z00_6829);
							BgL_auxz00_10944 = BGL_OBJECT_WIDENING(BgL_tmpz00_10945);
						}
						BgL_auxz00_10943 =
							((BgL_instanceofzf2cinfozf2_bglt) BgL_auxz00_10944);
					}
					((((BgL_instanceofzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10943))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) ((BgL_approxz00_bglt)
								BgL_approx1207z00_6164)), BUNSPEC);
				}
				return BgL_new1409z00_6829;
			}
		}

	}



/* &lambda1918 */
	obj_t BGl_z62lambda1918z62zzcfa_info3z00(obj_t BgL_envz00_6165,
		obj_t BgL_oz00_6166, obj_t BgL_vz00_6167)
	{
		{	/* Cfa/cinfo3.scm 31 */
			{
				BgL_instanceofzf2cinfozf2_bglt BgL_auxz00_10951;

				{
					obj_t BgL_auxz00_10952;

					{	/* Cfa/cinfo3.scm 31 */
						BgL_objectz00_bglt BgL_tmpz00_10953;

						BgL_tmpz00_10953 =
							((BgL_objectz00_bglt) ((BgL_instanceofz00_bglt) BgL_oz00_6166));
						BgL_auxz00_10952 = BGL_OBJECT_WIDENING(BgL_tmpz00_10953);
					}
					BgL_auxz00_10951 =
						((BgL_instanceofzf2cinfozf2_bglt) BgL_auxz00_10952);
				}
				return
					((((BgL_instanceofzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10951))->
						BgL_approxz00) =
					((BgL_approxz00_bglt) ((BgL_approxz00_bglt) BgL_vz00_6167)), BUNSPEC);
			}
		}

	}



/* &lambda1917 */
	BgL_approxz00_bglt BGl_z62lambda1917z62zzcfa_info3z00(obj_t BgL_envz00_6168,
		obj_t BgL_oz00_6169)
	{
		{	/* Cfa/cinfo3.scm 31 */
			{
				BgL_instanceofzf2cinfozf2_bglt BgL_auxz00_10960;

				{
					obj_t BgL_auxz00_10961;

					{	/* Cfa/cinfo3.scm 31 */
						BgL_objectz00_bglt BgL_tmpz00_10962;

						BgL_tmpz00_10962 =
							((BgL_objectz00_bglt) ((BgL_instanceofz00_bglt) BgL_oz00_6169));
						BgL_auxz00_10961 = BGL_OBJECT_WIDENING(BgL_tmpz00_10962);
					}
					BgL_auxz00_10960 =
						((BgL_instanceofzf2cinfozf2_bglt) BgL_auxz00_10961);
				}
				return
					(((BgL_instanceofzf2cinfozf2_bglt) COBJECT(BgL_auxz00_10960))->
					BgL_approxz00);
			}
		}

	}



/* &lambda1882 */
	BgL_newz00_bglt BGl_z62lambda1882z62zzcfa_info3z00(obj_t BgL_envz00_6170,
		obj_t BgL_o1196z00_6171)
	{
		{	/* Cfa/cinfo3.scm 30 */
			{	/* Cfa/cinfo3.scm 30 */
				long BgL_arg1883z00_6839;

				{	/* Cfa/cinfo3.scm 30 */
					obj_t BgL_arg1884z00_6840;

					{	/* Cfa/cinfo3.scm 30 */
						obj_t BgL_arg1885z00_6841;

						{	/* Cfa/cinfo3.scm 30 */
							obj_t BgL_arg1815z00_6842;
							long BgL_arg1816z00_6843;

							BgL_arg1815z00_6842 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Cfa/cinfo3.scm 30 */
								long BgL_arg1817z00_6844;

								BgL_arg1817z00_6844 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_o1196z00_6171)));
								BgL_arg1816z00_6843 = (BgL_arg1817z00_6844 - OBJECT_TYPE);
							}
							BgL_arg1885z00_6841 =
								VECTOR_REF(BgL_arg1815z00_6842, BgL_arg1816z00_6843);
						}
						BgL_arg1884z00_6840 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1885z00_6841);
					}
					{	/* Cfa/cinfo3.scm 30 */
						obj_t BgL_tmpz00_10975;

						BgL_tmpz00_10975 = ((obj_t) BgL_arg1884z00_6840);
						BgL_arg1883z00_6839 = BGL_CLASS_NUM(BgL_tmpz00_10975);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_newz00_bglt) BgL_o1196z00_6171)), BgL_arg1883z00_6839);
			}
			{	/* Cfa/cinfo3.scm 30 */
				BgL_objectz00_bglt BgL_tmpz00_10981;

				BgL_tmpz00_10981 =
					((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_o1196z00_6171));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10981, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_o1196z00_6171));
			return ((BgL_newz00_bglt) ((BgL_newz00_bglt) BgL_o1196z00_6171));
		}

	}



/* &<@anonymous:1881> */
	obj_t BGl_z62zc3z04anonymousza31881ze3ze5zzcfa_info3z00(obj_t BgL_envz00_6172,
		obj_t BgL_new1195z00_6173)
	{
		{	/* Cfa/cinfo3.scm 30 */
			{
				BgL_newz00_bglt BgL_auxz00_10989;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_newz00_bglt) BgL_new1195z00_6173))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10993;

					{	/* Cfa/cinfo3.scm 30 */
						obj_t BgL_classz00_6846;

						BgL_classz00_6846 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 30 */
							obj_t BgL__ortest_1117z00_6847;

							BgL__ortest_1117z00_6847 = BGL_CLASS_NIL(BgL_classz00_6846);
							if (CBOOL(BgL__ortest_1117z00_6847))
								{	/* Cfa/cinfo3.scm 30 */
									BgL_auxz00_10993 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6847);
								}
							else
								{	/* Cfa/cinfo3.scm 30 */
									BgL_auxz00_10993 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6846));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_newz00_bglt) BgL_new1195z00_6173))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_10993), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_newz00_bglt) BgL_new1195z00_6173))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_newz00_bglt)
										BgL_new1195z00_6173))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt) ((BgL_newz00_bglt)
										BgL_new1195z00_6173))))->BgL_exprza2za2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt) ((BgL_newz00_bglt)
										BgL_new1195z00_6173))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_privatez00_bglt)
							COBJECT(((BgL_privatez00_bglt) ((BgL_newz00_bglt)
										BgL_new1195z00_6173))))->BgL_czd2formatzd2) =
					((obj_t) BGl_string2364z00zzcfa_info3z00), BUNSPEC);
				((((BgL_newz00_bglt) COBJECT(((BgL_newz00_bglt) ((BgL_newz00_bglt)
										BgL_new1195z00_6173))))->BgL_argszd2typezd2) =
					((obj_t) BNIL), BUNSPEC);
				{
					BgL_approxz00_bglt BgL_auxz00_11028;
					BgL_newzf2cinfozf2_bglt BgL_auxz00_11021;

					{	/* Cfa/cinfo3.scm 30 */
						obj_t BgL_classz00_6848;

						BgL_classz00_6848 = BGl_approxz00zzcfa_infoz00;
						{	/* Cfa/cinfo3.scm 30 */
							obj_t BgL__ortest_1117z00_6849;

							BgL__ortest_1117z00_6849 = BGL_CLASS_NIL(BgL_classz00_6848);
							if (CBOOL(BgL__ortest_1117z00_6849))
								{	/* Cfa/cinfo3.scm 30 */
									BgL_auxz00_11028 =
										((BgL_approxz00_bglt) BgL__ortest_1117z00_6849);
								}
							else
								{	/* Cfa/cinfo3.scm 30 */
									BgL_auxz00_11028 =
										((BgL_approxz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6848));
								}
						}
					}
					{
						obj_t BgL_auxz00_11022;

						{	/* Cfa/cinfo3.scm 30 */
							BgL_objectz00_bglt BgL_tmpz00_11023;

							BgL_tmpz00_11023 =
								((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_new1195z00_6173));
							BgL_auxz00_11022 = BGL_OBJECT_WIDENING(BgL_tmpz00_11023);
						}
						BgL_auxz00_11021 = ((BgL_newzf2cinfozf2_bglt) BgL_auxz00_11022);
					}
					((((BgL_newzf2cinfozf2_bglt) COBJECT(BgL_auxz00_11021))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_auxz00_11028), BUNSPEC);
				}
				BgL_auxz00_10989 = ((BgL_newz00_bglt) BgL_new1195z00_6173);
				return ((obj_t) BgL_auxz00_10989);
			}
		}

	}



/* &lambda1879 */
	BgL_newz00_bglt BGl_z62lambda1879z62zzcfa_info3z00(obj_t BgL_envz00_6174,
		obj_t BgL_o1192z00_6175)
	{
		{	/* Cfa/cinfo3.scm 30 */
			{	/* Cfa/cinfo3.scm 30 */
				BgL_newzf2cinfozf2_bglt BgL_wide1194z00_6851;

				BgL_wide1194z00_6851 =
					((BgL_newzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_newzf2cinfozf2_bgl))));
				{	/* Cfa/cinfo3.scm 30 */
					obj_t BgL_auxz00_11043;
					BgL_objectz00_bglt BgL_tmpz00_11039;

					BgL_auxz00_11043 = ((obj_t) BgL_wide1194z00_6851);
					BgL_tmpz00_11039 =
						((BgL_objectz00_bglt)
						((BgL_newz00_bglt) ((BgL_newz00_bglt) BgL_o1192z00_6175)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11039, BgL_auxz00_11043);
				}
				((BgL_objectz00_bglt)
					((BgL_newz00_bglt) ((BgL_newz00_bglt) BgL_o1192z00_6175)));
				{	/* Cfa/cinfo3.scm 30 */
					long BgL_arg1880z00_6852;

					BgL_arg1880z00_6852 = BGL_CLASS_NUM(BGl_newzf2Cinfozf2zzcfa_info3z00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_newz00_bglt)
								((BgL_newz00_bglt) BgL_o1192z00_6175))), BgL_arg1880z00_6852);
				}
				return
					((BgL_newz00_bglt)
					((BgL_newz00_bglt) ((BgL_newz00_bglt) BgL_o1192z00_6175)));
			}
		}

	}



/* &lambda1876 */
	BgL_newz00_bglt BGl_z62lambda1876z62zzcfa_info3z00(obj_t BgL_envz00_6176,
		obj_t BgL_loc1183z00_6177, obj_t BgL_type1184z00_6178,
		obj_t BgL_sidezd2effect1185zd2_6179, obj_t BgL_key1186z00_6180,
		obj_t BgL_exprza21187za2_6181, obj_t BgL_effect1188z00_6182,
		obj_t BgL_czd2format1189zd2_6183, obj_t BgL_argszd2type1190zd2_6184,
		obj_t BgL_approx1191z00_6185)
	{
		{	/* Cfa/cinfo3.scm 30 */
			{	/* Cfa/cinfo3.scm 30 */
				BgL_newz00_bglt BgL_new1404z00_6858;

				{	/* Cfa/cinfo3.scm 30 */
					BgL_newz00_bglt BgL_tmp1402z00_6859;
					BgL_newzf2cinfozf2_bglt BgL_wide1403z00_6860;

					{
						BgL_newz00_bglt BgL_auxz00_11057;

						{	/* Cfa/cinfo3.scm 30 */
							BgL_newz00_bglt BgL_new1401z00_6861;

							BgL_new1401z00_6861 =
								((BgL_newz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_newz00_bgl))));
							{	/* Cfa/cinfo3.scm 30 */
								long BgL_arg1878z00_6862;

								BgL_arg1878z00_6862 = BGL_CLASS_NUM(BGl_newz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1401z00_6861),
									BgL_arg1878z00_6862);
							}
							{	/* Cfa/cinfo3.scm 30 */
								BgL_objectz00_bglt BgL_tmpz00_11062;

								BgL_tmpz00_11062 = ((BgL_objectz00_bglt) BgL_new1401z00_6861);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11062, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1401z00_6861);
							BgL_auxz00_11057 = BgL_new1401z00_6861;
						}
						BgL_tmp1402z00_6859 = ((BgL_newz00_bglt) BgL_auxz00_11057);
					}
					BgL_wide1403z00_6860 =
						((BgL_newzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_newzf2cinfozf2_bgl))));
					{	/* Cfa/cinfo3.scm 30 */
						obj_t BgL_auxz00_11070;
						BgL_objectz00_bglt BgL_tmpz00_11068;

						BgL_auxz00_11070 = ((obj_t) BgL_wide1403z00_6860);
						BgL_tmpz00_11068 = ((BgL_objectz00_bglt) BgL_tmp1402z00_6859);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11068, BgL_auxz00_11070);
					}
					((BgL_objectz00_bglt) BgL_tmp1402z00_6859);
					{	/* Cfa/cinfo3.scm 30 */
						long BgL_arg1877z00_6863;

						BgL_arg1877z00_6863 =
							BGL_CLASS_NUM(BGl_newzf2Cinfozf2zzcfa_info3z00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1402z00_6859), BgL_arg1877z00_6863);
					}
					BgL_new1404z00_6858 = ((BgL_newz00_bglt) BgL_tmp1402z00_6859);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1404z00_6858)))->BgL_locz00) =
					((obj_t) BgL_loc1183z00_6177), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1404z00_6858)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1184z00_6178)),
					BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1404z00_6858)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1185zd2_6179), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1404z00_6858)))->BgL_keyz00) =
					((obj_t) BgL_key1186z00_6180), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1404z00_6858)))->BgL_exprza2za2) =
					((obj_t) ((obj_t) BgL_exprza21187za2_6181)), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1404z00_6858)))->BgL_effectz00) =
					((obj_t) BgL_effect1188z00_6182), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1404z00_6858)))->BgL_czd2formatzd2) =
					((obj_t) ((obj_t) BgL_czd2format1189zd2_6183)), BUNSPEC);
				((((BgL_newz00_bglt) COBJECT(((BgL_newz00_bglt) BgL_new1404z00_6858)))->
						BgL_argszd2typezd2) =
					((obj_t) ((obj_t) BgL_argszd2type1190zd2_6184)), BUNSPEC);
				{
					BgL_newzf2cinfozf2_bglt BgL_auxz00_11098;

					{
						obj_t BgL_auxz00_11099;

						{	/* Cfa/cinfo3.scm 30 */
							BgL_objectz00_bglt BgL_tmpz00_11100;

							BgL_tmpz00_11100 = ((BgL_objectz00_bglt) BgL_new1404z00_6858);
							BgL_auxz00_11099 = BGL_OBJECT_WIDENING(BgL_tmpz00_11100);
						}
						BgL_auxz00_11098 = ((BgL_newzf2cinfozf2_bglt) BgL_auxz00_11099);
					}
					((((BgL_newzf2cinfozf2_bglt) COBJECT(BgL_auxz00_11098))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) ((BgL_approxz00_bglt)
								BgL_approx1191z00_6185)), BUNSPEC);
				}
				return BgL_new1404z00_6858;
			}
		}

	}



/* &lambda1891 */
	obj_t BGl_z62lambda1891z62zzcfa_info3z00(obj_t BgL_envz00_6186,
		obj_t BgL_oz00_6187, obj_t BgL_vz00_6188)
	{
		{	/* Cfa/cinfo3.scm 30 */
			{
				BgL_newzf2cinfozf2_bglt BgL_auxz00_11106;

				{
					obj_t BgL_auxz00_11107;

					{	/* Cfa/cinfo3.scm 30 */
						BgL_objectz00_bglt BgL_tmpz00_11108;

						BgL_tmpz00_11108 =
							((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_oz00_6187));
						BgL_auxz00_11107 = BGL_OBJECT_WIDENING(BgL_tmpz00_11108);
					}
					BgL_auxz00_11106 = ((BgL_newzf2cinfozf2_bglt) BgL_auxz00_11107);
				}
				return
					((((BgL_newzf2cinfozf2_bglt) COBJECT(BgL_auxz00_11106))->
						BgL_approxz00) =
					((BgL_approxz00_bglt) ((BgL_approxz00_bglt) BgL_vz00_6188)), BUNSPEC);
			}
		}

	}



/* &lambda1890 */
	BgL_approxz00_bglt BGl_z62lambda1890z62zzcfa_info3z00(obj_t BgL_envz00_6189,
		obj_t BgL_oz00_6190)
	{
		{	/* Cfa/cinfo3.scm 30 */
			{
				BgL_newzf2cinfozf2_bglt BgL_auxz00_11115;

				{
					obj_t BgL_auxz00_11116;

					{	/* Cfa/cinfo3.scm 30 */
						BgL_objectz00_bglt BgL_tmpz00_11117;

						BgL_tmpz00_11117 =
							((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_oz00_6190));
						BgL_auxz00_11116 = BGL_OBJECT_WIDENING(BgL_tmpz00_11117);
					}
					BgL_auxz00_11115 = ((BgL_newzf2cinfozf2_bglt) BgL_auxz00_11116);
				}
				return
					(((BgL_newzf2cinfozf2_bglt) COBJECT(BgL_auxz00_11115))->
					BgL_approxz00);
			}
		}

	}



/* &lambda1858 */
	BgL_setfieldz00_bglt BGl_z62lambda1858z62zzcfa_info3z00(obj_t BgL_envz00_6191,
		obj_t BgL_o1181z00_6192)
	{
		{	/* Cfa/cinfo3.scm 29 */
			{	/* Cfa/cinfo3.scm 29 */
				long BgL_arg1859z00_6868;

				{	/* Cfa/cinfo3.scm 29 */
					obj_t BgL_arg1860z00_6869;

					{	/* Cfa/cinfo3.scm 29 */
						obj_t BgL_arg1862z00_6870;

						{	/* Cfa/cinfo3.scm 29 */
							obj_t BgL_arg1815z00_6871;
							long BgL_arg1816z00_6872;

							BgL_arg1815z00_6871 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Cfa/cinfo3.scm 29 */
								long BgL_arg1817z00_6873;

								BgL_arg1817z00_6873 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_setfieldz00_bglt) BgL_o1181z00_6192)));
								BgL_arg1816z00_6872 = (BgL_arg1817z00_6873 - OBJECT_TYPE);
							}
							BgL_arg1862z00_6870 =
								VECTOR_REF(BgL_arg1815z00_6871, BgL_arg1816z00_6872);
						}
						BgL_arg1860z00_6869 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1862z00_6870);
					}
					{	/* Cfa/cinfo3.scm 29 */
						obj_t BgL_tmpz00_11130;

						BgL_tmpz00_11130 = ((obj_t) BgL_arg1860z00_6869);
						BgL_arg1859z00_6868 = BGL_CLASS_NUM(BgL_tmpz00_11130);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_setfieldz00_bglt) BgL_o1181z00_6192)), BgL_arg1859z00_6868);
			}
			{	/* Cfa/cinfo3.scm 29 */
				BgL_objectz00_bglt BgL_tmpz00_11136;

				BgL_tmpz00_11136 =
					((BgL_objectz00_bglt) ((BgL_setfieldz00_bglt) BgL_o1181z00_6192));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11136, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_setfieldz00_bglt) BgL_o1181z00_6192));
			return
				((BgL_setfieldz00_bglt) ((BgL_setfieldz00_bglt) BgL_o1181z00_6192));
		}

	}



/* &<@anonymous:1857> */
	obj_t BGl_z62zc3z04anonymousza31857ze3ze5zzcfa_info3z00(obj_t BgL_envz00_6193,
		obj_t BgL_new1180z00_6194)
	{
		{	/* Cfa/cinfo3.scm 29 */
			{
				BgL_setfieldz00_bglt BgL_auxz00_11144;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_setfieldz00_bglt) BgL_new1180z00_6194))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_11148;

					{	/* Cfa/cinfo3.scm 29 */
						obj_t BgL_classz00_6875;

						BgL_classz00_6875 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 29 */
							obj_t BgL__ortest_1117z00_6876;

							BgL__ortest_1117z00_6876 = BGL_CLASS_NIL(BgL_classz00_6875);
							if (CBOOL(BgL__ortest_1117z00_6876))
								{	/* Cfa/cinfo3.scm 29 */
									BgL_auxz00_11148 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6876);
								}
							else
								{	/* Cfa/cinfo3.scm 29 */
									BgL_auxz00_11148 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6875));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_setfieldz00_bglt) BgL_new1180z00_6194))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_11148), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_setfieldz00_bglt) BgL_new1180z00_6194))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_setfieldz00_bglt)
										BgL_new1180z00_6194))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_setfieldz00_bglt)
										BgL_new1180z00_6194))))->BgL_exprza2za2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_setfieldz00_bglt)
										BgL_new1180z00_6194))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_privatez00_bglt)
							COBJECT(((BgL_privatez00_bglt) ((BgL_setfieldz00_bglt)
										BgL_new1180z00_6194))))->BgL_czd2formatzd2) =
					((obj_t) BGl_string2364z00zzcfa_info3z00), BUNSPEC);
				((((BgL_setfieldz00_bglt)
							COBJECT(((BgL_setfieldz00_bglt) ((BgL_setfieldz00_bglt)
										BgL_new1180z00_6194))))->BgL_fnamez00) =
					((obj_t) BGl_string2364z00zzcfa_info3z00), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_11176;

					{	/* Cfa/cinfo3.scm 29 */
						obj_t BgL_classz00_6877;

						BgL_classz00_6877 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 29 */
							obj_t BgL__ortest_1117z00_6878;

							BgL__ortest_1117z00_6878 = BGL_CLASS_NIL(BgL_classz00_6877);
							if (CBOOL(BgL__ortest_1117z00_6878))
								{	/* Cfa/cinfo3.scm 29 */
									BgL_auxz00_11176 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6878);
								}
							else
								{	/* Cfa/cinfo3.scm 29 */
									BgL_auxz00_11176 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6877));
								}
						}
					}
					((((BgL_setfieldz00_bglt) COBJECT(
									((BgL_setfieldz00_bglt)
										((BgL_setfieldz00_bglt) BgL_new1180z00_6194))))->
							BgL_ftypez00) = ((BgL_typez00_bglt) BgL_auxz00_11176), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_11186;

					{	/* Cfa/cinfo3.scm 29 */
						obj_t BgL_classz00_6879;

						BgL_classz00_6879 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 29 */
							obj_t BgL__ortest_1117z00_6880;

							BgL__ortest_1117z00_6880 = BGL_CLASS_NIL(BgL_classz00_6879);
							if (CBOOL(BgL__ortest_1117z00_6880))
								{	/* Cfa/cinfo3.scm 29 */
									BgL_auxz00_11186 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6880);
								}
							else
								{	/* Cfa/cinfo3.scm 29 */
									BgL_auxz00_11186 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6879));
								}
						}
					}
					((((BgL_setfieldz00_bglt) COBJECT(
									((BgL_setfieldz00_bglt)
										((BgL_setfieldz00_bglt) BgL_new1180z00_6194))))->
							BgL_otypez00) = ((BgL_typez00_bglt) BgL_auxz00_11186), BUNSPEC);
				}
				{
					BgL_approxz00_bglt BgL_auxz00_11203;
					BgL_setfieldzf2cinfozf2_bglt BgL_auxz00_11196;

					{	/* Cfa/cinfo3.scm 29 */
						obj_t BgL_classz00_6881;

						BgL_classz00_6881 = BGl_approxz00zzcfa_infoz00;
						{	/* Cfa/cinfo3.scm 29 */
							obj_t BgL__ortest_1117z00_6882;

							BgL__ortest_1117z00_6882 = BGL_CLASS_NIL(BgL_classz00_6881);
							if (CBOOL(BgL__ortest_1117z00_6882))
								{	/* Cfa/cinfo3.scm 29 */
									BgL_auxz00_11203 =
										((BgL_approxz00_bglt) BgL__ortest_1117z00_6882);
								}
							else
								{	/* Cfa/cinfo3.scm 29 */
									BgL_auxz00_11203 =
										((BgL_approxz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6881));
								}
						}
					}
					{
						obj_t BgL_auxz00_11197;

						{	/* Cfa/cinfo3.scm 29 */
							BgL_objectz00_bglt BgL_tmpz00_11198;

							BgL_tmpz00_11198 =
								((BgL_objectz00_bglt)
								((BgL_setfieldz00_bglt) BgL_new1180z00_6194));
							BgL_auxz00_11197 = BGL_OBJECT_WIDENING(BgL_tmpz00_11198);
						}
						BgL_auxz00_11196 =
							((BgL_setfieldzf2cinfozf2_bglt) BgL_auxz00_11197);
					}
					((((BgL_setfieldzf2cinfozf2_bglt) COBJECT(BgL_auxz00_11196))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_auxz00_11203), BUNSPEC);
				}
				BgL_auxz00_11144 = ((BgL_setfieldz00_bglt) BgL_new1180z00_6194);
				return ((obj_t) BgL_auxz00_11144);
			}
		}

	}



/* &lambda1855 */
	BgL_setfieldz00_bglt BGl_z62lambda1855z62zzcfa_info3z00(obj_t BgL_envz00_6195,
		obj_t BgL_o1177z00_6196)
	{
		{	/* Cfa/cinfo3.scm 29 */
			{	/* Cfa/cinfo3.scm 29 */
				BgL_setfieldzf2cinfozf2_bglt BgL_wide1179z00_6884;

				BgL_wide1179z00_6884 =
					((BgL_setfieldzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_setfieldzf2cinfozf2_bgl))));
				{	/* Cfa/cinfo3.scm 29 */
					obj_t BgL_auxz00_11218;
					BgL_objectz00_bglt BgL_tmpz00_11214;

					BgL_auxz00_11218 = ((obj_t) BgL_wide1179z00_6884);
					BgL_tmpz00_11214 =
						((BgL_objectz00_bglt)
						((BgL_setfieldz00_bglt)
							((BgL_setfieldz00_bglt) BgL_o1177z00_6196)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11214, BgL_auxz00_11218);
				}
				((BgL_objectz00_bglt)
					((BgL_setfieldz00_bglt) ((BgL_setfieldz00_bglt) BgL_o1177z00_6196)));
				{	/* Cfa/cinfo3.scm 29 */
					long BgL_arg1856z00_6885;

					BgL_arg1856z00_6885 =
						BGL_CLASS_NUM(BGl_setfieldzf2Cinfozf2zzcfa_info3z00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_setfieldz00_bglt)
								((BgL_setfieldz00_bglt) BgL_o1177z00_6196))),
						BgL_arg1856z00_6885);
				}
				return
					((BgL_setfieldz00_bglt)
					((BgL_setfieldz00_bglt) ((BgL_setfieldz00_bglt) BgL_o1177z00_6196)));
			}
		}

	}



/* &lambda1852 */
	BgL_setfieldz00_bglt BGl_z62lambda1852z62zzcfa_info3z00(obj_t BgL_envz00_6197,
		obj_t BgL_loc1166z00_6198, obj_t BgL_type1167z00_6199,
		obj_t BgL_sidezd2effect1168zd2_6200, obj_t BgL_key1169z00_6201,
		obj_t BgL_exprza21170za2_6202, obj_t BgL_effect1171z00_6203,
		obj_t BgL_czd2format1172zd2_6204, obj_t BgL_fname1173z00_6205,
		obj_t BgL_ftype1174z00_6206, obj_t BgL_otype1175z00_6207,
		obj_t BgL_approx1176z00_6208)
	{
		{	/* Cfa/cinfo3.scm 29 */
			{	/* Cfa/cinfo3.scm 29 */
				BgL_setfieldz00_bglt BgL_new1399z00_6893;

				{	/* Cfa/cinfo3.scm 29 */
					BgL_setfieldz00_bglt BgL_tmp1397z00_6894;
					BgL_setfieldzf2cinfozf2_bglt BgL_wide1398z00_6895;

					{
						BgL_setfieldz00_bglt BgL_auxz00_11232;

						{	/* Cfa/cinfo3.scm 29 */
							BgL_setfieldz00_bglt BgL_new1396z00_6896;

							BgL_new1396z00_6896 =
								((BgL_setfieldz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_setfieldz00_bgl))));
							{	/* Cfa/cinfo3.scm 29 */
								long BgL_arg1854z00_6897;

								BgL_arg1854z00_6897 =
									BGL_CLASS_NUM(BGl_setfieldz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1396z00_6896),
									BgL_arg1854z00_6897);
							}
							{	/* Cfa/cinfo3.scm 29 */
								BgL_objectz00_bglt BgL_tmpz00_11237;

								BgL_tmpz00_11237 = ((BgL_objectz00_bglt) BgL_new1396z00_6896);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11237, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1396z00_6896);
							BgL_auxz00_11232 = BgL_new1396z00_6896;
						}
						BgL_tmp1397z00_6894 = ((BgL_setfieldz00_bglt) BgL_auxz00_11232);
					}
					BgL_wide1398z00_6895 =
						((BgL_setfieldzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_setfieldzf2cinfozf2_bgl))));
					{	/* Cfa/cinfo3.scm 29 */
						obj_t BgL_auxz00_11245;
						BgL_objectz00_bglt BgL_tmpz00_11243;

						BgL_auxz00_11245 = ((obj_t) BgL_wide1398z00_6895);
						BgL_tmpz00_11243 = ((BgL_objectz00_bglt) BgL_tmp1397z00_6894);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11243, BgL_auxz00_11245);
					}
					((BgL_objectz00_bglt) BgL_tmp1397z00_6894);
					{	/* Cfa/cinfo3.scm 29 */
						long BgL_arg1853z00_6898;

						BgL_arg1853z00_6898 =
							BGL_CLASS_NUM(BGl_setfieldzf2Cinfozf2zzcfa_info3z00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1397z00_6894), BgL_arg1853z00_6898);
					}
					BgL_new1399z00_6893 = ((BgL_setfieldz00_bglt) BgL_tmp1397z00_6894);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1399z00_6893)))->BgL_locz00) =
					((obj_t) BgL_loc1166z00_6198), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1399z00_6893)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1167z00_6199)),
					BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1399z00_6893)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1168zd2_6200), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1399z00_6893)))->BgL_keyz00) =
					((obj_t) BgL_key1169z00_6201), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1399z00_6893)))->BgL_exprza2za2) =
					((obj_t) ((obj_t) BgL_exprza21170za2_6202)), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1399z00_6893)))->BgL_effectz00) =
					((obj_t) BgL_effect1171z00_6203), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1399z00_6893)))->BgL_czd2formatzd2) =
					((obj_t) ((obj_t) BgL_czd2format1172zd2_6204)), BUNSPEC);
				((((BgL_setfieldz00_bglt) COBJECT(((BgL_setfieldz00_bglt)
									BgL_new1399z00_6893)))->BgL_fnamez00) =
					((obj_t) ((obj_t) BgL_fname1173z00_6205)), BUNSPEC);
				((((BgL_setfieldz00_bglt) COBJECT(((BgL_setfieldz00_bglt)
									BgL_new1399z00_6893)))->BgL_ftypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_ftype1174z00_6206)),
					BUNSPEC);
				((((BgL_setfieldz00_bglt) COBJECT(((BgL_setfieldz00_bglt)
									BgL_new1399z00_6893)))->BgL_otypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_otype1175z00_6207)),
					BUNSPEC);
				{
					BgL_setfieldzf2cinfozf2_bglt BgL_auxz00_11279;

					{
						obj_t BgL_auxz00_11280;

						{	/* Cfa/cinfo3.scm 29 */
							BgL_objectz00_bglt BgL_tmpz00_11281;

							BgL_tmpz00_11281 = ((BgL_objectz00_bglt) BgL_new1399z00_6893);
							BgL_auxz00_11280 = BGL_OBJECT_WIDENING(BgL_tmpz00_11281);
						}
						BgL_auxz00_11279 =
							((BgL_setfieldzf2cinfozf2_bglt) BgL_auxz00_11280);
					}
					((((BgL_setfieldzf2cinfozf2_bglt) COBJECT(BgL_auxz00_11279))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) ((BgL_approxz00_bglt)
								BgL_approx1176z00_6208)), BUNSPEC);
				}
				return BgL_new1399z00_6893;
			}
		}

	}



/* &lambda1868 */
	obj_t BGl_z62lambda1868z62zzcfa_info3z00(obj_t BgL_envz00_6209,
		obj_t BgL_oz00_6210, obj_t BgL_vz00_6211)
	{
		{	/* Cfa/cinfo3.scm 29 */
			{
				BgL_setfieldzf2cinfozf2_bglt BgL_auxz00_11287;

				{
					obj_t BgL_auxz00_11288;

					{	/* Cfa/cinfo3.scm 29 */
						BgL_objectz00_bglt BgL_tmpz00_11289;

						BgL_tmpz00_11289 =
							((BgL_objectz00_bglt) ((BgL_setfieldz00_bglt) BgL_oz00_6210));
						BgL_auxz00_11288 = BGL_OBJECT_WIDENING(BgL_tmpz00_11289);
					}
					BgL_auxz00_11287 = ((BgL_setfieldzf2cinfozf2_bglt) BgL_auxz00_11288);
				}
				return
					((((BgL_setfieldzf2cinfozf2_bglt) COBJECT(BgL_auxz00_11287))->
						BgL_approxz00) =
					((BgL_approxz00_bglt) ((BgL_approxz00_bglt) BgL_vz00_6211)), BUNSPEC);
			}
		}

	}



/* &lambda1867 */
	BgL_approxz00_bglt BGl_z62lambda1867z62zzcfa_info3z00(obj_t BgL_envz00_6212,
		obj_t BgL_oz00_6213)
	{
		{	/* Cfa/cinfo3.scm 29 */
			{
				BgL_setfieldzf2cinfozf2_bglt BgL_auxz00_11296;

				{
					obj_t BgL_auxz00_11297;

					{	/* Cfa/cinfo3.scm 29 */
						BgL_objectz00_bglt BgL_tmpz00_11298;

						BgL_tmpz00_11298 =
							((BgL_objectz00_bglt) ((BgL_setfieldz00_bglt) BgL_oz00_6213));
						BgL_auxz00_11297 = BGL_OBJECT_WIDENING(BgL_tmpz00_11298);
					}
					BgL_auxz00_11296 = ((BgL_setfieldzf2cinfozf2_bglt) BgL_auxz00_11297);
				}
				return
					(((BgL_setfieldzf2cinfozf2_bglt) COBJECT(BgL_auxz00_11296))->
					BgL_approxz00);
			}
		}

	}



/* &lambda1836 */
	BgL_getfieldz00_bglt BGl_z62lambda1836z62zzcfa_info3z00(obj_t BgL_envz00_6214,
		obj_t BgL_o1164z00_6215)
	{
		{	/* Cfa/cinfo3.scm 28 */
			{	/* Cfa/cinfo3.scm 28 */
				long BgL_arg1837z00_6903;

				{	/* Cfa/cinfo3.scm 28 */
					obj_t BgL_arg1838z00_6904;

					{	/* Cfa/cinfo3.scm 28 */
						obj_t BgL_arg1839z00_6905;

						{	/* Cfa/cinfo3.scm 28 */
							obj_t BgL_arg1815z00_6906;
							long BgL_arg1816z00_6907;

							BgL_arg1815z00_6906 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Cfa/cinfo3.scm 28 */
								long BgL_arg1817z00_6908;

								BgL_arg1817z00_6908 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_getfieldz00_bglt) BgL_o1164z00_6215)));
								BgL_arg1816z00_6907 = (BgL_arg1817z00_6908 - OBJECT_TYPE);
							}
							BgL_arg1839z00_6905 =
								VECTOR_REF(BgL_arg1815z00_6906, BgL_arg1816z00_6907);
						}
						BgL_arg1838z00_6904 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1839z00_6905);
					}
					{	/* Cfa/cinfo3.scm 28 */
						obj_t BgL_tmpz00_11311;

						BgL_tmpz00_11311 = ((obj_t) BgL_arg1838z00_6904);
						BgL_arg1837z00_6903 = BGL_CLASS_NUM(BgL_tmpz00_11311);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_getfieldz00_bglt) BgL_o1164z00_6215)), BgL_arg1837z00_6903);
			}
			{	/* Cfa/cinfo3.scm 28 */
				BgL_objectz00_bglt BgL_tmpz00_11317;

				BgL_tmpz00_11317 =
					((BgL_objectz00_bglt) ((BgL_getfieldz00_bglt) BgL_o1164z00_6215));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11317, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_getfieldz00_bglt) BgL_o1164z00_6215));
			return
				((BgL_getfieldz00_bglt) ((BgL_getfieldz00_bglt) BgL_o1164z00_6215));
		}

	}



/* &<@anonymous:1835> */
	obj_t BGl_z62zc3z04anonymousza31835ze3ze5zzcfa_info3z00(obj_t BgL_envz00_6216,
		obj_t BgL_new1163z00_6217)
	{
		{	/* Cfa/cinfo3.scm 28 */
			{
				BgL_getfieldz00_bglt BgL_auxz00_11325;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_getfieldz00_bglt) BgL_new1163z00_6217))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_11329;

					{	/* Cfa/cinfo3.scm 28 */
						obj_t BgL_classz00_6910;

						BgL_classz00_6910 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 28 */
							obj_t BgL__ortest_1117z00_6911;

							BgL__ortest_1117z00_6911 = BGL_CLASS_NIL(BgL_classz00_6910);
							if (CBOOL(BgL__ortest_1117z00_6911))
								{	/* Cfa/cinfo3.scm 28 */
									BgL_auxz00_11329 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6911);
								}
							else
								{	/* Cfa/cinfo3.scm 28 */
									BgL_auxz00_11329 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6910));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_getfieldz00_bglt) BgL_new1163z00_6217))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_11329), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_getfieldz00_bglt) BgL_new1163z00_6217))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_getfieldz00_bglt)
										BgL_new1163z00_6217))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_getfieldz00_bglt)
										BgL_new1163z00_6217))))->BgL_exprza2za2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_getfieldz00_bglt)
										BgL_new1163z00_6217))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_privatez00_bglt)
							COBJECT(((BgL_privatez00_bglt) ((BgL_getfieldz00_bglt)
										BgL_new1163z00_6217))))->BgL_czd2formatzd2) =
					((obj_t) BGl_string2364z00zzcfa_info3z00), BUNSPEC);
				((((BgL_getfieldz00_bglt)
							COBJECT(((BgL_getfieldz00_bglt) ((BgL_getfieldz00_bglt)
										BgL_new1163z00_6217))))->BgL_fnamez00) =
					((obj_t) BGl_string2364z00zzcfa_info3z00), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_11357;

					{	/* Cfa/cinfo3.scm 28 */
						obj_t BgL_classz00_6912;

						BgL_classz00_6912 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 28 */
							obj_t BgL__ortest_1117z00_6913;

							BgL__ortest_1117z00_6913 = BGL_CLASS_NIL(BgL_classz00_6912);
							if (CBOOL(BgL__ortest_1117z00_6913))
								{	/* Cfa/cinfo3.scm 28 */
									BgL_auxz00_11357 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6913);
								}
							else
								{	/* Cfa/cinfo3.scm 28 */
									BgL_auxz00_11357 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6912));
								}
						}
					}
					((((BgL_getfieldz00_bglt) COBJECT(
									((BgL_getfieldz00_bglt)
										((BgL_getfieldz00_bglt) BgL_new1163z00_6217))))->
							BgL_ftypez00) = ((BgL_typez00_bglt) BgL_auxz00_11357), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_11367;

					{	/* Cfa/cinfo3.scm 28 */
						obj_t BgL_classz00_6914;

						BgL_classz00_6914 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 28 */
							obj_t BgL__ortest_1117z00_6915;

							BgL__ortest_1117z00_6915 = BGL_CLASS_NIL(BgL_classz00_6914);
							if (CBOOL(BgL__ortest_1117z00_6915))
								{	/* Cfa/cinfo3.scm 28 */
									BgL_auxz00_11367 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6915);
								}
							else
								{	/* Cfa/cinfo3.scm 28 */
									BgL_auxz00_11367 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6914));
								}
						}
					}
					((((BgL_getfieldz00_bglt) COBJECT(
									((BgL_getfieldz00_bglt)
										((BgL_getfieldz00_bglt) BgL_new1163z00_6217))))->
							BgL_otypez00) = ((BgL_typez00_bglt) BgL_auxz00_11367), BUNSPEC);
				}
				{
					BgL_approxz00_bglt BgL_auxz00_11384;
					BgL_getfieldzf2cinfozf2_bglt BgL_auxz00_11377;

					{	/* Cfa/cinfo3.scm 28 */
						obj_t BgL_classz00_6916;

						BgL_classz00_6916 = BGl_approxz00zzcfa_infoz00;
						{	/* Cfa/cinfo3.scm 28 */
							obj_t BgL__ortest_1117z00_6917;

							BgL__ortest_1117z00_6917 = BGL_CLASS_NIL(BgL_classz00_6916);
							if (CBOOL(BgL__ortest_1117z00_6917))
								{	/* Cfa/cinfo3.scm 28 */
									BgL_auxz00_11384 =
										((BgL_approxz00_bglt) BgL__ortest_1117z00_6917);
								}
							else
								{	/* Cfa/cinfo3.scm 28 */
									BgL_auxz00_11384 =
										((BgL_approxz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6916));
								}
						}
					}
					{
						obj_t BgL_auxz00_11378;

						{	/* Cfa/cinfo3.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_11379;

							BgL_tmpz00_11379 =
								((BgL_objectz00_bglt)
								((BgL_getfieldz00_bglt) BgL_new1163z00_6217));
							BgL_auxz00_11378 = BGL_OBJECT_WIDENING(BgL_tmpz00_11379);
						}
						BgL_auxz00_11377 =
							((BgL_getfieldzf2cinfozf2_bglt) BgL_auxz00_11378);
					}
					((((BgL_getfieldzf2cinfozf2_bglt) COBJECT(BgL_auxz00_11377))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_auxz00_11384), BUNSPEC);
				}
				BgL_auxz00_11325 = ((BgL_getfieldz00_bglt) BgL_new1163z00_6217);
				return ((obj_t) BgL_auxz00_11325);
			}
		}

	}



/* &lambda1833 */
	BgL_getfieldz00_bglt BGl_z62lambda1833z62zzcfa_info3z00(obj_t BgL_envz00_6218,
		obj_t BgL_o1160z00_6219)
	{
		{	/* Cfa/cinfo3.scm 28 */
			{	/* Cfa/cinfo3.scm 28 */
				BgL_getfieldzf2cinfozf2_bglt BgL_wide1162z00_6919;

				BgL_wide1162z00_6919 =
					((BgL_getfieldzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_getfieldzf2cinfozf2_bgl))));
				{	/* Cfa/cinfo3.scm 28 */
					obj_t BgL_auxz00_11399;
					BgL_objectz00_bglt BgL_tmpz00_11395;

					BgL_auxz00_11399 = ((obj_t) BgL_wide1162z00_6919);
					BgL_tmpz00_11395 =
						((BgL_objectz00_bglt)
						((BgL_getfieldz00_bglt)
							((BgL_getfieldz00_bglt) BgL_o1160z00_6219)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11395, BgL_auxz00_11399);
				}
				((BgL_objectz00_bglt)
					((BgL_getfieldz00_bglt) ((BgL_getfieldz00_bglt) BgL_o1160z00_6219)));
				{	/* Cfa/cinfo3.scm 28 */
					long BgL_arg1834z00_6920;

					BgL_arg1834z00_6920 =
						BGL_CLASS_NUM(BGl_getfieldzf2Cinfozf2zzcfa_info3z00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_getfieldz00_bglt)
								((BgL_getfieldz00_bglt) BgL_o1160z00_6219))),
						BgL_arg1834z00_6920);
				}
				return
					((BgL_getfieldz00_bglt)
					((BgL_getfieldz00_bglt) ((BgL_getfieldz00_bglt) BgL_o1160z00_6219)));
			}
		}

	}



/* &lambda1823 */
	BgL_getfieldz00_bglt BGl_z62lambda1823z62zzcfa_info3z00(obj_t BgL_envz00_6220,
		obj_t BgL_loc1149z00_6221, obj_t BgL_type1150z00_6222,
		obj_t BgL_sidezd2effect1151zd2_6223, obj_t BgL_key1152z00_6224,
		obj_t BgL_exprza21153za2_6225, obj_t BgL_effect1154z00_6226,
		obj_t BgL_czd2format1155zd2_6227, obj_t BgL_fname1156z00_6228,
		obj_t BgL_ftype1157z00_6229, obj_t BgL_otype1158z00_6230,
		obj_t BgL_approx1159z00_6231)
	{
		{	/* Cfa/cinfo3.scm 28 */
			{	/* Cfa/cinfo3.scm 28 */
				BgL_getfieldz00_bglt BgL_new1394z00_6928;

				{	/* Cfa/cinfo3.scm 28 */
					BgL_getfieldz00_bglt BgL_tmp1392z00_6929;
					BgL_getfieldzf2cinfozf2_bglt BgL_wide1393z00_6930;

					{
						BgL_getfieldz00_bglt BgL_auxz00_11413;

						{	/* Cfa/cinfo3.scm 28 */
							BgL_getfieldz00_bglt BgL_new1391z00_6931;

							BgL_new1391z00_6931 =
								((BgL_getfieldz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_getfieldz00_bgl))));
							{	/* Cfa/cinfo3.scm 28 */
								long BgL_arg1832z00_6932;

								BgL_arg1832z00_6932 =
									BGL_CLASS_NUM(BGl_getfieldz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1391z00_6931),
									BgL_arg1832z00_6932);
							}
							{	/* Cfa/cinfo3.scm 28 */
								BgL_objectz00_bglt BgL_tmpz00_11418;

								BgL_tmpz00_11418 = ((BgL_objectz00_bglt) BgL_new1391z00_6931);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11418, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1391z00_6931);
							BgL_auxz00_11413 = BgL_new1391z00_6931;
						}
						BgL_tmp1392z00_6929 = ((BgL_getfieldz00_bglt) BgL_auxz00_11413);
					}
					BgL_wide1393z00_6930 =
						((BgL_getfieldzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_getfieldzf2cinfozf2_bgl))));
					{	/* Cfa/cinfo3.scm 28 */
						obj_t BgL_auxz00_11426;
						BgL_objectz00_bglt BgL_tmpz00_11424;

						BgL_auxz00_11426 = ((obj_t) BgL_wide1393z00_6930);
						BgL_tmpz00_11424 = ((BgL_objectz00_bglt) BgL_tmp1392z00_6929);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11424, BgL_auxz00_11426);
					}
					((BgL_objectz00_bglt) BgL_tmp1392z00_6929);
					{	/* Cfa/cinfo3.scm 28 */
						long BgL_arg1831z00_6933;

						BgL_arg1831z00_6933 =
							BGL_CLASS_NUM(BGl_getfieldzf2Cinfozf2zzcfa_info3z00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1392z00_6929), BgL_arg1831z00_6933);
					}
					BgL_new1394z00_6928 = ((BgL_getfieldz00_bglt) BgL_tmp1392z00_6929);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1394z00_6928)))->BgL_locz00) =
					((obj_t) BgL_loc1149z00_6221), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1394z00_6928)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1150z00_6222)),
					BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1394z00_6928)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1151zd2_6223), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1394z00_6928)))->BgL_keyz00) =
					((obj_t) BgL_key1152z00_6224), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1394z00_6928)))->BgL_exprza2za2) =
					((obj_t) ((obj_t) BgL_exprza21153za2_6225)), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1394z00_6928)))->BgL_effectz00) =
					((obj_t) BgL_effect1154z00_6226), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1394z00_6928)))->BgL_czd2formatzd2) =
					((obj_t) ((obj_t) BgL_czd2format1155zd2_6227)), BUNSPEC);
				((((BgL_getfieldz00_bglt) COBJECT(((BgL_getfieldz00_bglt)
									BgL_new1394z00_6928)))->BgL_fnamez00) =
					((obj_t) ((obj_t) BgL_fname1156z00_6228)), BUNSPEC);
				((((BgL_getfieldz00_bglt) COBJECT(((BgL_getfieldz00_bglt)
									BgL_new1394z00_6928)))->BgL_ftypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_ftype1157z00_6229)),
					BUNSPEC);
				((((BgL_getfieldz00_bglt) COBJECT(((BgL_getfieldz00_bglt)
									BgL_new1394z00_6928)))->BgL_otypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_otype1158z00_6230)),
					BUNSPEC);
				{
					BgL_getfieldzf2cinfozf2_bglt BgL_auxz00_11460;

					{
						obj_t BgL_auxz00_11461;

						{	/* Cfa/cinfo3.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_11462;

							BgL_tmpz00_11462 = ((BgL_objectz00_bglt) BgL_new1394z00_6928);
							BgL_auxz00_11461 = BGL_OBJECT_WIDENING(BgL_tmpz00_11462);
						}
						BgL_auxz00_11460 =
							((BgL_getfieldzf2cinfozf2_bglt) BgL_auxz00_11461);
					}
					((((BgL_getfieldzf2cinfozf2_bglt) COBJECT(BgL_auxz00_11460))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) ((BgL_approxz00_bglt)
								BgL_approx1159z00_6231)), BUNSPEC);
				}
				return BgL_new1394z00_6928;
			}
		}

	}



/* &lambda1845 */
	obj_t BGl_z62lambda1845z62zzcfa_info3z00(obj_t BgL_envz00_6232,
		obj_t BgL_oz00_6233, obj_t BgL_vz00_6234)
	{
		{	/* Cfa/cinfo3.scm 28 */
			{
				BgL_getfieldzf2cinfozf2_bglt BgL_auxz00_11468;

				{
					obj_t BgL_auxz00_11469;

					{	/* Cfa/cinfo3.scm 28 */
						BgL_objectz00_bglt BgL_tmpz00_11470;

						BgL_tmpz00_11470 =
							((BgL_objectz00_bglt) ((BgL_getfieldz00_bglt) BgL_oz00_6233));
						BgL_auxz00_11469 = BGL_OBJECT_WIDENING(BgL_tmpz00_11470);
					}
					BgL_auxz00_11468 = ((BgL_getfieldzf2cinfozf2_bglt) BgL_auxz00_11469);
				}
				return
					((((BgL_getfieldzf2cinfozf2_bglt) COBJECT(BgL_auxz00_11468))->
						BgL_approxz00) =
					((BgL_approxz00_bglt) ((BgL_approxz00_bglt) BgL_vz00_6234)), BUNSPEC);
			}
		}

	}



/* &lambda1844 */
	BgL_approxz00_bglt BGl_z62lambda1844z62zzcfa_info3z00(obj_t BgL_envz00_6235,
		obj_t BgL_oz00_6236)
	{
		{	/* Cfa/cinfo3.scm 28 */
			{
				BgL_getfieldzf2cinfozf2_bglt BgL_auxz00_11477;

				{
					obj_t BgL_auxz00_11478;

					{	/* Cfa/cinfo3.scm 28 */
						BgL_objectz00_bglt BgL_tmpz00_11479;

						BgL_tmpz00_11479 =
							((BgL_objectz00_bglt) ((BgL_getfieldz00_bglt) BgL_oz00_6236));
						BgL_auxz00_11478 = BGL_OBJECT_WIDENING(BgL_tmpz00_11479);
					}
					BgL_auxz00_11477 = ((BgL_getfieldzf2cinfozf2_bglt) BgL_auxz00_11478);
				}
				return
					(((BgL_getfieldzf2cinfozf2_bglt) COBJECT(BgL_auxz00_11477))->
					BgL_approxz00);
			}
		}

	}



/* &lambda1767 */
	BgL_pragmaz00_bglt BGl_z62lambda1767z62zzcfa_info3z00(obj_t BgL_envz00_6237,
		obj_t BgL_o1147z00_6238)
	{
		{	/* Cfa/cinfo3.scm 27 */
			{	/* Cfa/cinfo3.scm 27 */
				long BgL_arg1770z00_6938;

				{	/* Cfa/cinfo3.scm 27 */
					obj_t BgL_arg1771z00_6939;

					{	/* Cfa/cinfo3.scm 27 */
						obj_t BgL_arg1773z00_6940;

						{	/* Cfa/cinfo3.scm 27 */
							obj_t BgL_arg1815z00_6941;
							long BgL_arg1816z00_6942;

							BgL_arg1815z00_6941 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Cfa/cinfo3.scm 27 */
								long BgL_arg1817z00_6943;

								BgL_arg1817z00_6943 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_pragmaz00_bglt) BgL_o1147z00_6238)));
								BgL_arg1816z00_6942 = (BgL_arg1817z00_6943 - OBJECT_TYPE);
							}
							BgL_arg1773z00_6940 =
								VECTOR_REF(BgL_arg1815z00_6941, BgL_arg1816z00_6942);
						}
						BgL_arg1771z00_6939 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1773z00_6940);
					}
					{	/* Cfa/cinfo3.scm 27 */
						obj_t BgL_tmpz00_11492;

						BgL_tmpz00_11492 = ((obj_t) BgL_arg1771z00_6939);
						BgL_arg1770z00_6938 = BGL_CLASS_NUM(BgL_tmpz00_11492);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_pragmaz00_bglt) BgL_o1147z00_6238)), BgL_arg1770z00_6938);
			}
			{	/* Cfa/cinfo3.scm 27 */
				BgL_objectz00_bglt BgL_tmpz00_11498;

				BgL_tmpz00_11498 =
					((BgL_objectz00_bglt) ((BgL_pragmaz00_bglt) BgL_o1147z00_6238));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11498, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_pragmaz00_bglt) BgL_o1147z00_6238));
			return ((BgL_pragmaz00_bglt) ((BgL_pragmaz00_bglt) BgL_o1147z00_6238));
		}

	}



/* &<@anonymous:1766> */
	obj_t BGl_z62zc3z04anonymousza31766ze3ze5zzcfa_info3z00(obj_t BgL_envz00_6239,
		obj_t BgL_new1146z00_6240)
	{
		{	/* Cfa/cinfo3.scm 27 */
			{
				BgL_pragmaz00_bglt BgL_auxz00_11506;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_pragmaz00_bglt) BgL_new1146z00_6240))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_11510;

					{	/* Cfa/cinfo3.scm 27 */
						obj_t BgL_classz00_6945;

						BgL_classz00_6945 = BGl_typez00zztype_typez00;
						{	/* Cfa/cinfo3.scm 27 */
							obj_t BgL__ortest_1117z00_6946;

							BgL__ortest_1117z00_6946 = BGL_CLASS_NIL(BgL_classz00_6945);
							if (CBOOL(BgL__ortest_1117z00_6946))
								{	/* Cfa/cinfo3.scm 27 */
									BgL_auxz00_11510 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6946);
								}
							else
								{	/* Cfa/cinfo3.scm 27 */
									BgL_auxz00_11510 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6945));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_pragmaz00_bglt) BgL_new1146z00_6240))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_11510), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_pragmaz00_bglt) BgL_new1146z00_6240))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_pragmaz00_bglt)
										BgL_new1146z00_6240))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_pragmaz00_bglt)
										BgL_new1146z00_6240))))->BgL_exprza2za2) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_externz00_bglt)
							COBJECT(((BgL_externz00_bglt) ((BgL_pragmaz00_bglt)
										BgL_new1146z00_6240))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_pragmaz00_bglt)
							COBJECT(((BgL_pragmaz00_bglt) ((BgL_pragmaz00_bglt)
										BgL_new1146z00_6240))))->BgL_formatz00) =
					((obj_t) BGl_string2364z00zzcfa_info3z00), BUNSPEC);
				((((BgL_pragmaz00_bglt)
							COBJECT(((BgL_pragmaz00_bglt) ((BgL_pragmaz00_bglt)
										BgL_new1146z00_6240))))->BgL_srfi0z00) =
					((obj_t) CNST_TABLE_REF(25)), BUNSPEC);
				{
					BgL_approxz00_bglt BgL_auxz00_11546;
					BgL_pragmazf2cinfozf2_bglt BgL_auxz00_11539;

					{	/* Cfa/cinfo3.scm 27 */
						obj_t BgL_classz00_6947;

						BgL_classz00_6947 = BGl_approxz00zzcfa_infoz00;
						{	/* Cfa/cinfo3.scm 27 */
							obj_t BgL__ortest_1117z00_6948;

							BgL__ortest_1117z00_6948 = BGL_CLASS_NIL(BgL_classz00_6947);
							if (CBOOL(BgL__ortest_1117z00_6948))
								{	/* Cfa/cinfo3.scm 27 */
									BgL_auxz00_11546 =
										((BgL_approxz00_bglt) BgL__ortest_1117z00_6948);
								}
							else
								{	/* Cfa/cinfo3.scm 27 */
									BgL_auxz00_11546 =
										((BgL_approxz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6947));
								}
						}
					}
					{
						obj_t BgL_auxz00_11540;

						{	/* Cfa/cinfo3.scm 27 */
							BgL_objectz00_bglt BgL_tmpz00_11541;

							BgL_tmpz00_11541 =
								((BgL_objectz00_bglt)
								((BgL_pragmaz00_bglt) BgL_new1146z00_6240));
							BgL_auxz00_11540 = BGL_OBJECT_WIDENING(BgL_tmpz00_11541);
						}
						BgL_auxz00_11539 = ((BgL_pragmazf2cinfozf2_bglt) BgL_auxz00_11540);
					}
					((((BgL_pragmazf2cinfozf2_bglt) COBJECT(BgL_auxz00_11539))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) BgL_auxz00_11546), BUNSPEC);
				}
				BgL_auxz00_11506 = ((BgL_pragmaz00_bglt) BgL_new1146z00_6240);
				return ((obj_t) BgL_auxz00_11506);
			}
		}

	}



/* &lambda1763 */
	BgL_pragmaz00_bglt BGl_z62lambda1763z62zzcfa_info3z00(obj_t BgL_envz00_6241,
		obj_t BgL_o1143z00_6242)
	{
		{	/* Cfa/cinfo3.scm 27 */
			{	/* Cfa/cinfo3.scm 27 */
				BgL_pragmazf2cinfozf2_bglt BgL_wide1145z00_6950;

				BgL_wide1145z00_6950 =
					((BgL_pragmazf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_pragmazf2cinfozf2_bgl))));
				{	/* Cfa/cinfo3.scm 27 */
					obj_t BgL_auxz00_11561;
					BgL_objectz00_bglt BgL_tmpz00_11557;

					BgL_auxz00_11561 = ((obj_t) BgL_wide1145z00_6950);
					BgL_tmpz00_11557 =
						((BgL_objectz00_bglt)
						((BgL_pragmaz00_bglt) ((BgL_pragmaz00_bglt) BgL_o1143z00_6242)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11557, BgL_auxz00_11561);
				}
				((BgL_objectz00_bglt)
					((BgL_pragmaz00_bglt) ((BgL_pragmaz00_bglt) BgL_o1143z00_6242)));
				{	/* Cfa/cinfo3.scm 27 */
					long BgL_arg1765z00_6951;

					BgL_arg1765z00_6951 =
						BGL_CLASS_NUM(BGl_pragmazf2Cinfozf2zzcfa_info3z00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_pragmaz00_bglt)
								((BgL_pragmaz00_bglt) BgL_o1143z00_6242))),
						BgL_arg1765z00_6951);
				}
				return
					((BgL_pragmaz00_bglt)
					((BgL_pragmaz00_bglt) ((BgL_pragmaz00_bglt) BgL_o1143z00_6242)));
			}
		}

	}



/* &lambda1756 */
	BgL_pragmaz00_bglt BGl_z62lambda1756z62zzcfa_info3z00(obj_t BgL_envz00_6243,
		obj_t BgL_loc1134z00_6244, obj_t BgL_type1135z00_6245,
		obj_t BgL_sidezd2effect1136zd2_6246, obj_t BgL_key1137z00_6247,
		obj_t BgL_exprza21138za2_6248, obj_t BgL_effect1139z00_6249,
		obj_t BgL_format1140z00_6250, obj_t BgL_srfi01141z00_6251,
		obj_t BgL_approx1142z00_6252)
	{
		{	/* Cfa/cinfo3.scm 27 */
			{	/* Cfa/cinfo3.scm 27 */
				BgL_pragmaz00_bglt BgL_new1389z00_6957;

				{	/* Cfa/cinfo3.scm 27 */
					BgL_pragmaz00_bglt BgL_tmp1387z00_6958;
					BgL_pragmazf2cinfozf2_bglt BgL_wide1388z00_6959;

					{
						BgL_pragmaz00_bglt BgL_auxz00_11575;

						{	/* Cfa/cinfo3.scm 27 */
							BgL_pragmaz00_bglt BgL_new1386z00_6960;

							BgL_new1386z00_6960 =
								((BgL_pragmaz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_pragmaz00_bgl))));
							{	/* Cfa/cinfo3.scm 27 */
								long BgL_arg1762z00_6961;

								BgL_arg1762z00_6961 = BGL_CLASS_NUM(BGl_pragmaz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1386z00_6960),
									BgL_arg1762z00_6961);
							}
							{	/* Cfa/cinfo3.scm 27 */
								BgL_objectz00_bglt BgL_tmpz00_11580;

								BgL_tmpz00_11580 = ((BgL_objectz00_bglt) BgL_new1386z00_6960);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11580, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1386z00_6960);
							BgL_auxz00_11575 = BgL_new1386z00_6960;
						}
						BgL_tmp1387z00_6958 = ((BgL_pragmaz00_bglt) BgL_auxz00_11575);
					}
					BgL_wide1388z00_6959 =
						((BgL_pragmazf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_pragmazf2cinfozf2_bgl))));
					{	/* Cfa/cinfo3.scm 27 */
						obj_t BgL_auxz00_11588;
						BgL_objectz00_bglt BgL_tmpz00_11586;

						BgL_auxz00_11588 = ((obj_t) BgL_wide1388z00_6959);
						BgL_tmpz00_11586 = ((BgL_objectz00_bglt) BgL_tmp1387z00_6958);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11586, BgL_auxz00_11588);
					}
					((BgL_objectz00_bglt) BgL_tmp1387z00_6958);
					{	/* Cfa/cinfo3.scm 27 */
						long BgL_arg1761z00_6962;

						BgL_arg1761z00_6962 =
							BGL_CLASS_NUM(BGl_pragmazf2Cinfozf2zzcfa_info3z00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1387z00_6958), BgL_arg1761z00_6962);
					}
					BgL_new1389z00_6957 = ((BgL_pragmaz00_bglt) BgL_tmp1387z00_6958);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1389z00_6957)))->BgL_locz00) =
					((obj_t) BgL_loc1134z00_6244), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1389z00_6957)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1135z00_6245)),
					BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1389z00_6957)))->BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1136zd2_6246), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1389z00_6957)))->BgL_keyz00) =
					((obj_t) BgL_key1137z00_6247), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1389z00_6957)))->BgL_exprza2za2) =
					((obj_t) ((obj_t) BgL_exprza21138za2_6248)), BUNSPEC);
				((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
									BgL_new1389z00_6957)))->BgL_effectz00) =
					((obj_t) BgL_effect1139z00_6249), BUNSPEC);
				((((BgL_pragmaz00_bglt) COBJECT(((BgL_pragmaz00_bglt)
									BgL_new1389z00_6957)))->BgL_formatz00) =
					((obj_t) ((obj_t) BgL_format1140z00_6250)), BUNSPEC);
				((((BgL_pragmaz00_bglt) COBJECT(((BgL_pragmaz00_bglt)
									BgL_new1389z00_6957)))->BgL_srfi0z00) =
					((obj_t) ((obj_t) BgL_srfi01141z00_6251)), BUNSPEC);
				{
					BgL_pragmazf2cinfozf2_bglt BgL_auxz00_11616;

					{
						obj_t BgL_auxz00_11617;

						{	/* Cfa/cinfo3.scm 27 */
							BgL_objectz00_bglt BgL_tmpz00_11618;

							BgL_tmpz00_11618 = ((BgL_objectz00_bglt) BgL_new1389z00_6957);
							BgL_auxz00_11617 = BGL_OBJECT_WIDENING(BgL_tmpz00_11618);
						}
						BgL_auxz00_11616 = ((BgL_pragmazf2cinfozf2_bglt) BgL_auxz00_11617);
					}
					((((BgL_pragmazf2cinfozf2_bglt) COBJECT(BgL_auxz00_11616))->
							BgL_approxz00) =
						((BgL_approxz00_bglt) ((BgL_approxz00_bglt)
								BgL_approx1142z00_6252)), BUNSPEC);
				}
				return BgL_new1389z00_6957;
			}
		}

	}



/* &lambda1801 */
	obj_t BGl_z62lambda1801z62zzcfa_info3z00(obj_t BgL_envz00_6253,
		obj_t BgL_oz00_6254, obj_t BgL_vz00_6255)
	{
		{	/* Cfa/cinfo3.scm 27 */
			{
				BgL_pragmazf2cinfozf2_bglt BgL_auxz00_11624;

				{
					obj_t BgL_auxz00_11625;

					{	/* Cfa/cinfo3.scm 27 */
						BgL_objectz00_bglt BgL_tmpz00_11626;

						BgL_tmpz00_11626 =
							((BgL_objectz00_bglt) ((BgL_pragmaz00_bglt) BgL_oz00_6254));
						BgL_auxz00_11625 = BGL_OBJECT_WIDENING(BgL_tmpz00_11626);
					}
					BgL_auxz00_11624 = ((BgL_pragmazf2cinfozf2_bglt) BgL_auxz00_11625);
				}
				return
					((((BgL_pragmazf2cinfozf2_bglt) COBJECT(BgL_auxz00_11624))->
						BgL_approxz00) =
					((BgL_approxz00_bglt) ((BgL_approxz00_bglt) BgL_vz00_6255)), BUNSPEC);
			}
		}

	}



/* &lambda1800 */
	BgL_approxz00_bglt BGl_z62lambda1800z62zzcfa_info3z00(obj_t BgL_envz00_6256,
		obj_t BgL_oz00_6257)
	{
		{	/* Cfa/cinfo3.scm 27 */
			{
				BgL_pragmazf2cinfozf2_bglt BgL_auxz00_11633;

				{
					obj_t BgL_auxz00_11634;

					{	/* Cfa/cinfo3.scm 27 */
						BgL_objectz00_bglt BgL_tmpz00_11635;

						BgL_tmpz00_11635 =
							((BgL_objectz00_bglt) ((BgL_pragmaz00_bglt) BgL_oz00_6257));
						BgL_auxz00_11634 = BGL_OBJECT_WIDENING(BgL_tmpz00_11635);
					}
					BgL_auxz00_11633 = ((BgL_pragmazf2cinfozf2_bglt) BgL_auxz00_11634);
				}
				return
					(((BgL_pragmazf2cinfozf2_bglt) COBJECT(BgL_auxz00_11633))->
					BgL_approxz00);
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcfa_info3z00(void)
	{
		{	/* Cfa/cinfo3.scm 16 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcfa_info3z00(void)
	{
		{	/* Cfa/cinfo3.scm 16 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcfa_info3z00(void)
	{
		{	/* Cfa/cinfo3.scm 16 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2365z00zzcfa_info3z00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2365z00zzcfa_info3z00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2365z00zzcfa_info3z00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2365z00zzcfa_info3z00));
			return
				BGl_modulezd2initializa7ationz75zzcfa_infoz00(3788352L,
				BSTRING_TO_STRING(BGl_string2365z00zzcfa_info3z00));
		}

	}

#ifdef __cplusplus
}
#endif
