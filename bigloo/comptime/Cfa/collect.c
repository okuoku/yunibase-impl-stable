/*===========================================================================*/
/*   (Cfa/collect.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cfa/collect.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CFA_COLLECT_TYPE_DEFINITIONS
#define BGL_CFA_COLLECT_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_scnstz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_nodez00;
		obj_t BgL_classz00;
		obj_t BgL_locz00;
	}               *BgL_scnstz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_vallocz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                *BgL_vallocz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_kwotezf2nodezf2_bgl
	{
		struct BgL_nodez00_bgl *BgL_nodez00;
	}                      *BgL_kwotezf2nodezf2_bglt;

	typedef struct BgL_prezd2makezd2boxz00_bgl
	{
	}                          *BgL_prezd2makezd2boxz00_bglt;

	typedef struct BgL_prezd2arithmeticzd2appz00_bgl
	{
		obj_t BgL_speczd2typeszd2;
	}                                *BgL_prezd2arithmeticzd2appz00_bglt;

	typedef struct BgL_prezd2makezd2procedurezd2appzd2_bgl
	{
		struct BgL_variablez00_bgl *BgL_ownerz00;
	}                                     
		*BgL_prezd2makezd2procedurezd2appzd2_bglt;

	typedef struct BgL_prezd2procedurezd2refzd2appzd2_bgl
	{
	}                                    
		*BgL_prezd2procedurezd2refzd2appzd2_bglt;

	typedef struct BgL_prezd2procedurezd2setz12zd2appzc0_bgl
	{
	}                                       
		*BgL_prezd2procedurezd2setz12zd2appzc0_bglt;

	typedef struct BgL_prezd2makezd2vectorzd2appzd2_bgl
	{
		struct BgL_variablez00_bgl *BgL_ownerz00;
	}                                   *BgL_prezd2makezd2vectorzd2appzd2_bglt;

	typedef struct BgL_prezd2conszd2appz00_bgl
	{
		struct BgL_variablez00_bgl *BgL_ownerz00;
	}                          *BgL_prezd2conszd2appz00_bglt;

	typedef struct BgL_prezd2conszd2refzd2appzd2_bgl
	{
		obj_t BgL_getz00;
	}                                *BgL_prezd2conszd2refzd2appzd2_bglt;

	typedef struct BgL_prezd2conszd2setz12zd2appzc0_bgl
	{
		obj_t BgL_getz00;
	}                                   *BgL_prezd2conszd2setz12zd2appzc0_bglt;

	typedef struct BgL_prezd2makezd2structzd2appzd2_bgl
	{
		struct BgL_variablez00_bgl *BgL_ownerz00;
	}                                   *BgL_prezd2makezd2structzd2appzd2_bglt;

	typedef struct BgL_prezd2structzd2refzd2appzd2_bgl
	{
	}                                  *BgL_prezd2structzd2refzd2appzd2_bglt;

	typedef struct BgL_prezd2structzd2setz12zd2appzc0_bgl
	{
	}                                    
		*BgL_prezd2structzd2setz12zd2appzc0_bglt;

	typedef struct BgL_prezd2valloczf2cinfoz20_bgl
	{
		struct BgL_variablez00_bgl *BgL_ownerz00;
	}                              *BgL_prezd2valloczf2cinfoz20_bglt;


#endif													// BGL_CFA_COLLECT_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62nodezd2collectz121651za2zzcfa_collectz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	static obj_t BGl_z62nodezd2collectz12zd2funcal1670z70zzcfa_collectz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	extern obj_t
		BGl_za2optimzd2cfazd2fixnumzd2arithmeticzf3za2z21zzengine_paramz00;
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_objectzd2initzd2zzcfa_collectz00(void);
	extern obj_t BGl_addzd2funcallz12zc0zzcfa_closurez00(BgL_nodez00_bglt);
	static bool_t BGl_nodezd2collectza2z12z62zzcfa_collectz00(obj_t, obj_t);
	extern obj_t BGl_prezd2makezd2boxz00zzcfa_infoz00;
	static obj_t BGl_z62nodezd2collectz12zd2switch1682z70zzcfa_collectz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	extern obj_t BGl_prezd2valloczf2Cinfoz20zzcfa_info3z00;
	extern obj_t BGl_pairzd2optimzd2quotezd2maxlenzd2zzcfa_pairz00(void);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	static obj_t BGl_methodzd2initzd2zzcfa_collectz00(void);
	extern obj_t BGl_externz00zzast_nodez00;
	extern obj_t BGl_prezd2makezd2vectorzd2appzd2zzcfa_info2z00;
	extern bool_t BGl_vectorzd2optimzf3z21zzcfa_vectorz00(void);
	static obj_t BGl_z62nodezd2collectz12zd2makezd2b1692za2zzcfa_collectz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_prezd2conszd2refzd2appzd2zzcfa_info2z00;
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62nodezd2collectz12zd2jumpzd2e1690za2zzcfa_collectz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd2collectz12zd2app1664z70zzcfa_collectz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd2collectz12za2zzcfa_collectz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_cfunz00zzast_varz00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t BGl_getzd2atypeze70z35zzcfa_collectz00(obj_t);
	extern obj_t BGl_prezd2structzd2refzd2appzd2zzcfa_info2z00;
	BGL_IMPORT bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzcfa_collectz00(void);
	extern obj_t BGl_kwotezf2nodezf2zzcfa_infoz00;
	extern obj_t BGl_vallocz00zzast_nodez00;
	extern obj_t BGl_prezd2conszd2setz12zd2appzc0zzcfa_info2z00;
	extern obj_t BGl_prezd2procedurezd2setz12zd2appzc0zzcfa_info2z00;
	extern obj_t BGl_prezd2procedurezd2refzd2appzd2zzcfa_info2z00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_loopze70ze7zzcfa_collectz00(obj_t);
	static obj_t BGl_z62nodezd2collectz12zd2cast1674z70zzcfa_collectz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd2collectz12zd2letzd2va1686za2zzcfa_collectz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd2collectz12zd2letzd2fu1684za2zzcfa_collectz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t BGl_za2usedzd2allocza2zd2zzcfa_collectz00 = BUNSPEC;
	static obj_t BGl_z62nodezd2collectz12zd2valloc1666z70zzcfa_collectz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd2collectz12zd2setq1676z70zzcfa_collectz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62collectzd2allzd2approxz12z70zzcfa_collectz00(obj_t,
		obj_t);
	extern obj_t BGl_za2optimza2z00zzengine_paramz00;
	extern obj_t BGl_setqz00zzast_nodez00;
	BGL_IMPORT int BGl_bigloozd2warningzd2zz__paramz00(void);
	extern obj_t
		BGl_arithmeticzd2operatorzf3z21zzcfa_specializa7eza7(BgL_globalz00_bglt);
	static obj_t BGl_requirezd2initializa7ationz75zzcfa_collectz00 = BUNSPEC;
	static obj_t BGl_z62nodezd2collectz12zd2appzd2ly1668za2zzcfa_collectz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd2collectz12zd2setzd2ex1688za2zzcfa_collectz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	extern bool_t BGl_pairzd2optimzf3z21zzcfa_pairz00(void);
	static obj_t BGl_toplevelzd2initzd2zzcfa_collectz00(void);
	extern obj_t BGl_prezd2makezd2structzd2appzd2zzcfa_info2z00;
	static obj_t BGl_genericzd2initzd2zzcfa_collectz00(void);
	static obj_t BGl_z62nodezd2collectz12zd2extern1672z70zzcfa_collectz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd2collectz12zd2condit1678z70zzcfa_collectz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t
		BGl_za2optimzd2cfazd2flonumzd2arithmeticzf3za2z21zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t
		BGl_collectzd2allzd2approxz12z12zzcfa_collectz00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd2collectz12zd2boxzd2re1696za2zzcfa_collectz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd2collectz12zd2var1658z70zzcfa_collectz00(obj_t,
		obj_t, obj_t);
	extern BgL_nodez00_bglt
		BGl_topzd2levelzd2sexpzd2ze3nodez31zzast_sexpz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_bigloozd2warningzd2setz12z12zz__paramz00(int);
	extern obj_t BGl_varz00zzast_nodez00;
	static obj_t BGl_z62nodezd2collectz12zd2atom1654z70zzcfa_collectz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	extern obj_t BGl_prezd2structzd2setz12zd2appzc0zzcfa_info2z00;
	static obj_t BGl_z62nodezd2collectz12zd2boxzd2se1694za2zzcfa_collectz00(obj_t,
		obj_t, obj_t);
	extern obj_t
		BGl_arithmeticzd2speczd2typesz00zzcfa_specializa7eza7(BgL_globalz00_bglt);
	static obj_t BGl_z62nodezd2collectz12zd2kwote1656z70zzcfa_collectz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcfa_collectz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_closurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_boxz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_structz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_pairz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_vectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_procedurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_specializa7eza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_arithmeticz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info2z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_infoz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	BGL_EXPORTED_DECL obj_t BGl_getzd2allocszd2zzcfa_collectz00(void);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	static obj_t BGl_z62nodezd2collectz12zd2sequen1660z70zzcfa_collectz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzcfa_collectz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcfa_collectz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzcfa_collectz00(void);
	extern obj_t BGl_scnstz00zzast_varz00;
	static obj_t BGl_z62getzd2allocszb0zzcfa_collectz00(obj_t);
	extern obj_t BGl_prezd2arithmeticzd2appz00zzcfa_info2z00;
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t BGl_prezd2conszd2appz00zzcfa_info2z00;
	extern obj_t BGl_prezd2makezd2procedurezd2appzd2zzcfa_info2z00;
	static obj_t BGl_nodezd2collectz12zc0zzcfa_collectz00(BgL_nodez00_bglt,
		BgL_variablez00_bglt);
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t BGl_z62nodezd2collectz12zd2sync1662z70zzcfa_collectz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd2collectz12zd2fail1680z70zzcfa_collectz00(obj_t,
		obj_t, obj_t);
	static obj_t __cnst[23];


	   
		 
		DEFINE_STRING(BGl_string2162z00zzcfa_collectz00,
		BgL_bgl_string2162za700za7za7c2190za7, "node-collect!1651", 17);
	      DEFINE_STRING(BGl_string2163z00zzcfa_collectz00,
		BgL_bgl_string2163za700za7za7c2191za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string2165z00zzcfa_collectz00,
		BgL_bgl_string2165za700za7za7c2192za7, "node-collect!", 13);
	     
		DEFINE_STATIC_BGL_GENERIC(BGl_nodezd2collectz12zd2envz12zzcfa_collectz00,
		BgL_bgl_za762nodeza7d2collec2193z00,
		BGl_z62nodezd2collectz12za2zzcfa_collectz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2187z00zzcfa_collectz00,
		BgL_bgl_string2187za700za7za7c2194za7, "cfa_collect", 11);
	      DEFINE_STRING(BGl_string2188z00zzcfa_collectz00,
		BgL_bgl_string2188za700za7za7c2195za7,
		"real string boolean char integer quote (quote ()) (pragma::obj \"\") $set-cdr! $set-car! $cdr $car $cons $struct-set! $struct-ref $make-struct $make-vector procedure-set! procedure-ref make-va-procedure make-fx-procedure c-eq? node-collect!1651 ",
		243);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_collectzd2allzd2approxz12zd2envzc0zzcfa_collectz00,
		BgL_bgl_za762collectza7d2all2196z00,
		BGl_z62collectzd2allzd2approxz12z70zzcfa_collectz00, 0L, BUNSPEC, 1);
	BGL_IMPORT obj_t BGl_carzd2envzd2zz__r4_pairs_and_lists_6_3z00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2161z00zzcfa_collectz00,
		BgL_bgl_za762nodeza7d2collec2197z00,
		BGl_z62nodezd2collectz121651za2zzcfa_collectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2164z00zzcfa_collectz00,
		BgL_bgl_za762nodeza7d2collec2198z00,
		BGl_z62nodezd2collectz12zd2atom1654z70zzcfa_collectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2166z00zzcfa_collectz00,
		BgL_bgl_za762nodeza7d2collec2199z00,
		BGl_z62nodezd2collectz12zd2kwote1656z70zzcfa_collectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2167z00zzcfa_collectz00,
		BgL_bgl_za762nodeza7d2collec2200z00,
		BGl_z62nodezd2collectz12zd2var1658z70zzcfa_collectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2168z00zzcfa_collectz00,
		BgL_bgl_za762nodeza7d2collec2201z00,
		BGl_z62nodezd2collectz12zd2sequen1660z70zzcfa_collectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2169z00zzcfa_collectz00,
		BgL_bgl_za762nodeza7d2collec2202z00,
		BGl_z62nodezd2collectz12zd2sync1662z70zzcfa_collectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2170z00zzcfa_collectz00,
		BgL_bgl_za762nodeza7d2collec2203z00,
		BGl_z62nodezd2collectz12zd2app1664z70zzcfa_collectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2171z00zzcfa_collectz00,
		BgL_bgl_za762nodeza7d2collec2204z00,
		BGl_z62nodezd2collectz12zd2valloc1666z70zzcfa_collectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2172z00zzcfa_collectz00,
		BgL_bgl_za762nodeza7d2collec2205z00,
		BGl_z62nodezd2collectz12zd2appzd2ly1668za2zzcfa_collectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2173z00zzcfa_collectz00,
		BgL_bgl_za762nodeza7d2collec2206z00,
		BGl_z62nodezd2collectz12zd2funcal1670z70zzcfa_collectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2174z00zzcfa_collectz00,
		BgL_bgl_za762nodeza7d2collec2207z00,
		BGl_z62nodezd2collectz12zd2extern1672z70zzcfa_collectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2175z00zzcfa_collectz00,
		BgL_bgl_za762nodeza7d2collec2208z00,
		BGl_z62nodezd2collectz12zd2cast1674z70zzcfa_collectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2176z00zzcfa_collectz00,
		BgL_bgl_za762nodeza7d2collec2209z00,
		BGl_z62nodezd2collectz12zd2setq1676z70zzcfa_collectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2177z00zzcfa_collectz00,
		BgL_bgl_za762nodeza7d2collec2210z00,
		BGl_z62nodezd2collectz12zd2condit1678z70zzcfa_collectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2178z00zzcfa_collectz00,
		BgL_bgl_za762nodeza7d2collec2211z00,
		BGl_z62nodezd2collectz12zd2fail1680z70zzcfa_collectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2179z00zzcfa_collectz00,
		BgL_bgl_za762nodeza7d2collec2212z00,
		BGl_z62nodezd2collectz12zd2switch1682z70zzcfa_collectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2180z00zzcfa_collectz00,
		BgL_bgl_za762nodeza7d2collec2213z00,
		BGl_z62nodezd2collectz12zd2letzd2fu1684za2zzcfa_collectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2181z00zzcfa_collectz00,
		BgL_bgl_za762nodeza7d2collec2214z00,
		BGl_z62nodezd2collectz12zd2letzd2va1686za2zzcfa_collectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2182z00zzcfa_collectz00,
		BgL_bgl_za762nodeza7d2collec2215z00,
		BGl_z62nodezd2collectz12zd2setzd2ex1688za2zzcfa_collectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2183z00zzcfa_collectz00,
		BgL_bgl_za762nodeza7d2collec2216z00,
		BGl_z62nodezd2collectz12zd2jumpzd2e1690za2zzcfa_collectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2184z00zzcfa_collectz00,
		BgL_bgl_za762nodeza7d2collec2217z00,
		BGl_z62nodezd2collectz12zd2makezd2b1692za2zzcfa_collectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2185z00zzcfa_collectz00,
		BgL_bgl_za762nodeza7d2collec2218z00,
		BGl_z62nodezd2collectz12zd2boxzd2se1694za2zzcfa_collectz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2186z00zzcfa_collectz00,
		BgL_bgl_za762nodeza7d2collec2219z00,
		BGl_z62nodezd2collectz12zd2boxzd2re1696za2zzcfa_collectz00, 0L, BUNSPEC, 2);
	BGL_IMPORT obj_t BGl_cdrzd2envzd2zz__r4_pairs_and_lists_6_3z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_getzd2allocszd2envz00zzcfa_collectz00,
		BgL_bgl_za762getza7d2allocsza72220za7,
		BGl_z62getzd2allocszb0zzcfa_collectz00, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_za2usedzd2allocza2zd2zzcfa_collectz00));
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcfa_collectz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcfa_collectz00(long
		BgL_checksumz00_5342, char *BgL_fromz00_5343)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcfa_collectz00))
				{
					BGl_requirezd2initializa7ationz75zzcfa_collectz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcfa_collectz00();
					BGl_libraryzd2moduleszd2initz00zzcfa_collectz00();
					BGl_cnstzd2initzd2zzcfa_collectz00();
					BGl_importedzd2moduleszd2initz00zzcfa_collectz00();
					BGl_genericzd2initzd2zzcfa_collectz00();
					BGl_methodzd2initzd2zzcfa_collectz00();
					return BGl_toplevelzd2initzd2zzcfa_collectz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcfa_collectz00(void)
	{
		{	/* Cfa/collect.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cfa_collect");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "cfa_collect");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cfa_collect");
			BGl_modulezd2initializa7ationz75zz__paramz00(0L, "cfa_collect");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cfa_collect");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cfa_collect");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"cfa_collect");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(0L,
				"cfa_collect");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cfa_collect");
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(0L, "cfa_collect");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L,
				"cfa_collect");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "cfa_collect");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "cfa_collect");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cfa_collect");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcfa_collectz00(void)
	{
		{	/* Cfa/collect.scm 15 */
			{	/* Cfa/collect.scm 15 */
				obj_t BgL_cportz00_5128;

				{	/* Cfa/collect.scm 15 */
					obj_t BgL_stringz00_5135;

					BgL_stringz00_5135 = BGl_string2188z00zzcfa_collectz00;
					{	/* Cfa/collect.scm 15 */
						obj_t BgL_startz00_5136;

						BgL_startz00_5136 = BINT(0L);
						{	/* Cfa/collect.scm 15 */
							obj_t BgL_endz00_5137;

							BgL_endz00_5137 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_5135)));
							{	/* Cfa/collect.scm 15 */

								BgL_cportz00_5128 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_5135, BgL_startz00_5136, BgL_endz00_5137);
				}}}}
				{
					long BgL_iz00_5129;

					BgL_iz00_5129 = 22L;
				BgL_loopz00_5130:
					if ((BgL_iz00_5129 == -1L))
						{	/* Cfa/collect.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Cfa/collect.scm 15 */
							{	/* Cfa/collect.scm 15 */
								obj_t BgL_arg2189z00_5131;

								{	/* Cfa/collect.scm 15 */

									{	/* Cfa/collect.scm 15 */
										obj_t BgL_locationz00_5133;

										BgL_locationz00_5133 = BBOOL(((bool_t) 0));
										{	/* Cfa/collect.scm 15 */

											BgL_arg2189z00_5131 =
												BGl_readz00zz__readerz00(BgL_cportz00_5128,
												BgL_locationz00_5133);
										}
									}
								}
								{	/* Cfa/collect.scm 15 */
									int BgL_tmpz00_5377;

									BgL_tmpz00_5377 = (int) (BgL_iz00_5129);
									CNST_TABLE_SET(BgL_tmpz00_5377, BgL_arg2189z00_5131);
							}}
							{	/* Cfa/collect.scm 15 */
								int BgL_auxz00_5134;

								BgL_auxz00_5134 = (int) ((BgL_iz00_5129 - 1L));
								{
									long BgL_iz00_5382;

									BgL_iz00_5382 = (long) (BgL_auxz00_5134);
									BgL_iz00_5129 = BgL_iz00_5382;
									goto BgL_loopz00_5130;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcfa_collectz00(void)
	{
		{	/* Cfa/collect.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcfa_collectz00(void)
	{
		{	/* Cfa/collect.scm 15 */
			return (BGl_za2usedzd2allocza2zd2zzcfa_collectz00 = BNIL, BUNSPEC);
		}

	}



/* collect-all-approx! */
	BGL_EXPORTED_DEF obj_t BGl_collectzd2allzd2approxz12z12zzcfa_collectz00(obj_t
		BgL_globalsz00_3)
	{
		{	/* Cfa/collect.scm 46 */
			{
				obj_t BgL_l1638z00_3569;

				{	/* Cfa/collect.scm 48 */
					bool_t BgL_tmpz00_5385;

					BgL_l1638z00_3569 = BgL_globalsz00_3;
				BgL_zc3z04anonymousza31701ze3z87_3570:
					if (PAIRP(BgL_l1638z00_3569))
						{	/* Cfa/collect.scm 48 */
							{	/* Cfa/collect.scm 48 */
								obj_t BgL_globalz00_3572;

								BgL_globalz00_3572 = CAR(BgL_l1638z00_3569);
								{	/* Cfa/collect.scm 48 */
									BgL_valuez00_bglt BgL_arg1703z00_3573;

									BgL_arg1703z00_3573 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_globalz00_3572))))->
										BgL_valuez00);
									{	/* Cfa/collect.scm 55 */
										obj_t BgL_arg1708z00_4571;

										BgL_arg1708z00_4571 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_arg1703z00_3573)))->
											BgL_bodyz00);
										BGl_nodezd2collectz12zc0zzcfa_collectz00(((BgL_nodez00_bglt)
												BgL_arg1708z00_4571),
											((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
													BgL_globalz00_3572)));
									}
								}
							}
							{
								obj_t BgL_l1638z00_5398;

								BgL_l1638z00_5398 = CDR(BgL_l1638z00_3569);
								BgL_l1638z00_3569 = BgL_l1638z00_5398;
								goto BgL_zc3z04anonymousza31701ze3z87_3570;
							}
						}
					else
						{	/* Cfa/collect.scm 48 */
							BgL_tmpz00_5385 = ((bool_t) 1);
						}
					return BBOOL(BgL_tmpz00_5385);
				}
			}
		}

	}



/* &collect-all-approx! */
	obj_t BGl_z62collectzd2allzd2approxz12z70zzcfa_collectz00(obj_t
		BgL_envz00_5026, obj_t BgL_globalsz00_5027)
	{
		{	/* Cfa/collect.scm 46 */
			return
				BGl_collectzd2allzd2approxz12z12zzcfa_collectz00(BgL_globalsz00_5027);
		}

	}



/* node-collect*! */
	bool_t BGl_nodezd2collectza2z12z62zzcfa_collectz00(obj_t BgL_nodeza2za2_52,
		obj_t BgL_ownerz00_53)
	{
		{	/* Cfa/collect.scm 393 */
			{
				obj_t BgL_l1649z00_3578;

				BgL_l1649z00_3578 = BgL_nodeza2za2_52;
			BgL_zc3z04anonymousza31709ze3z87_3579:
				if (PAIRP(BgL_l1649z00_3578))
					{	/* Cfa/collect.scm 394 */
						{	/* Cfa/collect.scm 394 */
							obj_t BgL_nodez00_3581;

							BgL_nodez00_3581 = CAR(BgL_l1649z00_3578);
							BGl_nodezd2collectz12zc0zzcfa_collectz00(
								((BgL_nodez00_bglt) BgL_nodez00_3581),
								((BgL_variablez00_bglt) BgL_ownerz00_53));
						}
						{
							obj_t BgL_l1649z00_5408;

							BgL_l1649z00_5408 = CDR(BgL_l1649z00_3578);
							BgL_l1649z00_3578 = BgL_l1649z00_5408;
							goto BgL_zc3z04anonymousza31709ze3z87_3579;
						}
					}
				else
					{	/* Cfa/collect.scm 394 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* get-allocs */
	BGL_EXPORTED_DEF obj_t BGl_getzd2allocszd2zzcfa_collectz00(void)
	{
		{	/* Cfa/collect.scm 416 */
			return BGl_za2usedzd2allocza2zd2zzcfa_collectz00;
		}

	}



/* &get-allocs */
	obj_t BGl_z62getzd2allocszb0zzcfa_collectz00(obj_t BgL_envz00_5028)
	{
		{	/* Cfa/collect.scm 416 */
			return BGl_getzd2allocszd2zzcfa_collectz00();
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcfa_collectz00(void)
	{
		{	/* Cfa/collect.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcfa_collectz00(void)
	{
		{	/* Cfa/collect.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_nodezd2collectz12zd2envz12zzcfa_collectz00,
				BGl_proc2161z00zzcfa_collectz00, BGl_nodez00zzast_nodez00,
				BGl_string2162z00zzcfa_collectz00);
		}

	}



/* &node-collect!1651 */
	obj_t BGl_z62nodezd2collectz121651za2zzcfa_collectz00(obj_t BgL_envz00_5030,
		obj_t BgL_nodez00_5031, obj_t BgL_ownerz00_5032)
	{
		{	/* Cfa/collect.scm 60 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(0),
				BGl_string2163z00zzcfa_collectz00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_5031)));
		}

	}



/* node-collect! */
	obj_t BGl_nodezd2collectz12zc0zzcfa_collectz00(BgL_nodez00_bglt BgL_nodez00_6,
		BgL_variablez00_bglt BgL_ownerz00_7)
	{
		{	/* Cfa/collect.scm 60 */
			{	/* Cfa/collect.scm 60 */
				obj_t BgL_method1652z00_3589;

				{	/* Cfa/collect.scm 60 */
					obj_t BgL_res2153z00_4608;

					{	/* Cfa/collect.scm 60 */
						long BgL_objzd2classzd2numz00_4579;

						BgL_objzd2classzd2numz00_4579 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_6));
						{	/* Cfa/collect.scm 60 */
							obj_t BgL_arg1811z00_4580;

							BgL_arg1811z00_4580 =
								PROCEDURE_REF(BGl_nodezd2collectz12zd2envz12zzcfa_collectz00,
								(int) (1L));
							{	/* Cfa/collect.scm 60 */
								int BgL_offsetz00_4583;

								BgL_offsetz00_4583 = (int) (BgL_objzd2classzd2numz00_4579);
								{	/* Cfa/collect.scm 60 */
									long BgL_offsetz00_4584;

									BgL_offsetz00_4584 =
										((long) (BgL_offsetz00_4583) - OBJECT_TYPE);
									{	/* Cfa/collect.scm 60 */
										long BgL_modz00_4585;

										BgL_modz00_4585 =
											(BgL_offsetz00_4584 >> (int) ((long) ((int) (4L))));
										{	/* Cfa/collect.scm 60 */
											long BgL_restz00_4587;

											BgL_restz00_4587 =
												(BgL_offsetz00_4584 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Cfa/collect.scm 60 */

												{	/* Cfa/collect.scm 60 */
													obj_t BgL_bucketz00_4589;

													BgL_bucketz00_4589 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_4580), BgL_modz00_4585);
													BgL_res2153z00_4608 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_4589), BgL_restz00_4587);
					}}}}}}}}
					BgL_method1652z00_3589 = BgL_res2153z00_4608;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1652z00_3589,
					((obj_t) BgL_nodez00_6), ((obj_t) BgL_ownerz00_7));
			}
		}

	}



/* &node-collect! */
	obj_t BGl_z62nodezd2collectz12za2zzcfa_collectz00(obj_t BgL_envz00_5033,
		obj_t BgL_nodez00_5034, obj_t BgL_ownerz00_5035)
	{
		{	/* Cfa/collect.scm 60 */
			return
				BGl_nodezd2collectz12zc0zzcfa_collectz00(
				((BgL_nodez00_bglt) BgL_nodez00_5034),
				((BgL_variablez00_bglt) BgL_ownerz00_5035));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcfa_collectz00(void)
	{
		{	/* Cfa/collect.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2collectz12zd2envz12zzcfa_collectz00,
				BGl_atomz00zzast_nodez00, BGl_proc2164z00zzcfa_collectz00,
				BGl_string2165z00zzcfa_collectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2collectz12zd2envz12zzcfa_collectz00,
				BGl_kwotez00zzast_nodez00, BGl_proc2166z00zzcfa_collectz00,
				BGl_string2165z00zzcfa_collectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2collectz12zd2envz12zzcfa_collectz00,
				BGl_varz00zzast_nodez00, BGl_proc2167z00zzcfa_collectz00,
				BGl_string2165z00zzcfa_collectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2collectz12zd2envz12zzcfa_collectz00,
				BGl_sequencez00zzast_nodez00, BGl_proc2168z00zzcfa_collectz00,
				BGl_string2165z00zzcfa_collectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2collectz12zd2envz12zzcfa_collectz00,
				BGl_syncz00zzast_nodez00, BGl_proc2169z00zzcfa_collectz00,
				BGl_string2165z00zzcfa_collectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2collectz12zd2envz12zzcfa_collectz00,
				BGl_appz00zzast_nodez00, BGl_proc2170z00zzcfa_collectz00,
				BGl_string2165z00zzcfa_collectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2collectz12zd2envz12zzcfa_collectz00,
				BGl_vallocz00zzast_nodez00, BGl_proc2171z00zzcfa_collectz00,
				BGl_string2165z00zzcfa_collectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2collectz12zd2envz12zzcfa_collectz00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc2172z00zzcfa_collectz00,
				BGl_string2165z00zzcfa_collectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2collectz12zd2envz12zzcfa_collectz00,
				BGl_funcallz00zzast_nodez00, BGl_proc2173z00zzcfa_collectz00,
				BGl_string2165z00zzcfa_collectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2collectz12zd2envz12zzcfa_collectz00,
				BGl_externz00zzast_nodez00, BGl_proc2174z00zzcfa_collectz00,
				BGl_string2165z00zzcfa_collectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2collectz12zd2envz12zzcfa_collectz00,
				BGl_castz00zzast_nodez00, BGl_proc2175z00zzcfa_collectz00,
				BGl_string2165z00zzcfa_collectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2collectz12zd2envz12zzcfa_collectz00,
				BGl_setqz00zzast_nodez00, BGl_proc2176z00zzcfa_collectz00,
				BGl_string2165z00zzcfa_collectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2collectz12zd2envz12zzcfa_collectz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc2177z00zzcfa_collectz00,
				BGl_string2165z00zzcfa_collectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2collectz12zd2envz12zzcfa_collectz00,
				BGl_failz00zzast_nodez00, BGl_proc2178z00zzcfa_collectz00,
				BGl_string2165z00zzcfa_collectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2collectz12zd2envz12zzcfa_collectz00,
				BGl_switchz00zzast_nodez00, BGl_proc2179z00zzcfa_collectz00,
				BGl_string2165z00zzcfa_collectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2collectz12zd2envz12zzcfa_collectz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc2180z00zzcfa_collectz00,
				BGl_string2165z00zzcfa_collectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2collectz12zd2envz12zzcfa_collectz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc2181z00zzcfa_collectz00,
				BGl_string2165z00zzcfa_collectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2collectz12zd2envz12zzcfa_collectz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc2182z00zzcfa_collectz00,
				BGl_string2165z00zzcfa_collectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2collectz12zd2envz12zzcfa_collectz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc2183z00zzcfa_collectz00,
				BGl_string2165z00zzcfa_collectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2collectz12zd2envz12zzcfa_collectz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc2184z00zzcfa_collectz00,
				BGl_string2165z00zzcfa_collectz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2collectz12zd2envz12zzcfa_collectz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc2185z00zzcfa_collectz00,
				BGl_string2165z00zzcfa_collectz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2collectz12zd2envz12zzcfa_collectz00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc2186z00zzcfa_collectz00,
				BGl_string2165z00zzcfa_collectz00);
		}

	}



/* &node-collect!-box-re1696 */
	obj_t BGl_z62nodezd2collectz12zd2boxzd2re1696za2zzcfa_collectz00(obj_t
		BgL_envz00_5062, obj_t BgL_nodez00_5063, obj_t BgL_ownerz00_5064)
	{
		{	/* Cfa/collect.scm 386 */
			{	/* Cfa/collect.scm 388 */
				BgL_varz00_bglt BgL_arg1950z00_5142;

				BgL_arg1950z00_5142 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_nodez00_5063)))->BgL_varz00);
				return
					BGl_nodezd2collectz12zc0zzcfa_collectz00(
					((BgL_nodez00_bglt) BgL_arg1950z00_5142),
					((BgL_variablez00_bglt) BgL_ownerz00_5064));
			}
		}

	}



/* &node-collect!-box-se1694 */
	obj_t BGl_z62nodezd2collectz12zd2boxzd2se1694za2zzcfa_collectz00(obj_t
		BgL_envz00_5065, obj_t BgL_nodez00_5066, obj_t BgL_ownerz00_5067)
	{
		{	/* Cfa/collect.scm 378 */
			{	/* Cfa/collect.scm 380 */
				BgL_varz00_bglt BgL_arg1948z00_5144;

				BgL_arg1948z00_5144 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_5066)))->BgL_varz00);
				BGl_nodezd2collectz12zc0zzcfa_collectz00(
					((BgL_nodez00_bglt) BgL_arg1948z00_5144),
					((BgL_variablez00_bglt) BgL_ownerz00_5067));
			}
			{	/* Cfa/collect.scm 381 */
				BgL_nodez00_bglt BgL_arg1949z00_5145;

				BgL_arg1949z00_5145 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_5066)))->BgL_valuez00);
				return
					BGl_nodezd2collectz12zc0zzcfa_collectz00(BgL_arg1949z00_5145,
					((BgL_variablez00_bglt) BgL_ownerz00_5067));
			}
		}

	}



/* &node-collect!-make-b1692 */
	obj_t BGl_z62nodezd2collectz12zd2makezd2b1692za2zzcfa_collectz00(obj_t
		BgL_envz00_5068, obj_t BgL_nodez00_5069, obj_t BgL_ownerz00_5070)
	{
		{	/* Cfa/collect.scm 368 */
			{	/* Cfa/collect.scm 369 */
				BgL_nodez00_bglt BgL_arg1945z00_5147;

				BgL_arg1945z00_5147 =
					(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_5069)))->BgL_valuez00);
				BGl_nodezd2collectz12zc0zzcfa_collectz00(BgL_arg1945z00_5147,
					((BgL_variablez00_bglt) BgL_ownerz00_5070));
			}
			if (((long) CINT(BGl_za2optimza2z00zzengine_paramz00) >= 1L))
				{	/* Cfa/collect.scm 370 */
					BGl_za2usedzd2allocza2zd2zzcfa_collectz00 =
						MAKE_YOUNG_PAIR(
						((obj_t)
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_5069)),
						BGl_za2usedzd2allocza2zd2zzcfa_collectz00);
					{	/* Cfa/collect.scm 373 */
						BgL_prezd2makezd2boxz00_bglt BgL_wide1264z00_5148;

						BgL_wide1264z00_5148 =
							((BgL_prezd2makezd2boxz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_prezd2makezd2boxz00_bgl))));
						{	/* Cfa/collect.scm 373 */
							obj_t BgL_auxz00_5502;
							BgL_objectz00_bglt BgL_tmpz00_5498;

							BgL_auxz00_5502 = ((obj_t) BgL_wide1264z00_5148);
							BgL_tmpz00_5498 =
								((BgL_objectz00_bglt)
								((BgL_makezd2boxzd2_bglt)
									((BgL_makezd2boxzd2_bglt) BgL_nodez00_5069)));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5498, BgL_auxz00_5502);
						}
						((BgL_objectz00_bglt)
							((BgL_makezd2boxzd2_bglt)
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_5069)));
						{	/* Cfa/collect.scm 373 */
							long BgL_arg1947z00_5149;

							BgL_arg1947z00_5149 =
								BGL_CLASS_NUM(BGl_prezd2makezd2boxz00zzcfa_infoz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_makezd2boxzd2_bglt)
										((BgL_makezd2boxzd2_bglt) BgL_nodez00_5069))),
								BgL_arg1947z00_5149);
						}
						((BgL_makezd2boxzd2_bglt)
							((BgL_makezd2boxzd2_bglt)
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_5069)));
					}
					return
						((obj_t)
						((BgL_makezd2boxzd2_bglt)
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_5069)));
				}
			else
				{	/* Cfa/collect.scm 370 */
					return BFALSE;
				}
		}

	}



/* &node-collect!-jump-e1690 */
	obj_t BGl_z62nodezd2collectz12zd2jumpzd2e1690za2zzcfa_collectz00(obj_t
		BgL_envz00_5071, obj_t BgL_nodez00_5072, obj_t BgL_ownerz00_5073)
	{
		{	/* Cfa/collect.scm 360 */
			{	/* Cfa/collect.scm 362 */
				BgL_nodez00_bglt BgL_arg1943z00_5151;

				BgL_arg1943z00_5151 =
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_5072)))->BgL_exitz00);
				BGl_nodezd2collectz12zc0zzcfa_collectz00(BgL_arg1943z00_5151,
					((BgL_variablez00_bglt) BgL_ownerz00_5073));
			}
			{	/* Cfa/collect.scm 363 */
				BgL_nodez00_bglt BgL_arg1944z00_5152;

				BgL_arg1944z00_5152 =
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_5072)))->BgL_valuez00);
				return
					BGl_nodezd2collectz12zc0zzcfa_collectz00(BgL_arg1944z00_5152,
					((BgL_variablez00_bglt) BgL_ownerz00_5073));
			}
		}

	}



/* &node-collect!-set-ex1688 */
	obj_t BGl_z62nodezd2collectz12zd2setzd2ex1688za2zzcfa_collectz00(obj_t
		BgL_envz00_5074, obj_t BgL_nodez00_5075, obj_t BgL_ownerz00_5076)
	{
		{	/* Cfa/collect.scm 352 */
			{	/* Cfa/collect.scm 354 */
				BgL_nodez00_bglt BgL_arg1941z00_5154;

				BgL_arg1941z00_5154 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_5075)))->BgL_onexitz00);
				BGl_nodezd2collectz12zc0zzcfa_collectz00(BgL_arg1941z00_5154,
					((BgL_variablez00_bglt) BgL_ownerz00_5076));
			}
			{	/* Cfa/collect.scm 355 */
				BgL_nodez00_bglt BgL_arg1942z00_5155;

				BgL_arg1942z00_5155 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_5075)))->BgL_bodyz00);
				return
					BGl_nodezd2collectz12zc0zzcfa_collectz00(BgL_arg1942z00_5155,
					((BgL_variablez00_bglt) BgL_ownerz00_5076));
			}
		}

	}



/* &node-collect!-let-va1686 */
	obj_t BGl_z62nodezd2collectz12zd2letzd2va1686za2zzcfa_collectz00(obj_t
		BgL_envz00_5077, obj_t BgL_nodez00_5078, obj_t BgL_ownerz00_5079)
	{
		{	/* Cfa/collect.scm 342 */
			{	/* Cfa/collect.scm 344 */
				obj_t BgL_g1648z00_5157;

				BgL_g1648z00_5157 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_5078)))->BgL_bindingsz00);
				{
					obj_t BgL_l1646z00_5159;

					BgL_l1646z00_5159 = BgL_g1648z00_5157;
				BgL_zc3z04anonymousza31936ze3z87_5158:
					if (PAIRP(BgL_l1646z00_5159))
						{	/* Cfa/collect.scm 344 */
							{	/* Cfa/collect.scm 345 */
								obj_t BgL_bindingz00_5160;

								BgL_bindingz00_5160 = CAR(BgL_l1646z00_5159);
								{	/* Cfa/collect.scm 345 */
									obj_t BgL_arg1938z00_5161;

									BgL_arg1938z00_5161 = CDR(((obj_t) BgL_bindingz00_5160));
									BGl_nodezd2collectz12zc0zzcfa_collectz00(
										((BgL_nodez00_bglt) BgL_arg1938z00_5161),
										((BgL_variablez00_bglt) BgL_ownerz00_5079));
								}
							}
							{
								obj_t BgL_l1646z00_5545;

								BgL_l1646z00_5545 = CDR(BgL_l1646z00_5159);
								BgL_l1646z00_5159 = BgL_l1646z00_5545;
								goto BgL_zc3z04anonymousza31936ze3z87_5158;
							}
						}
					else
						{	/* Cfa/collect.scm 344 */
							((bool_t) 1);
						}
				}
			}
			{	/* Cfa/collect.scm 347 */
				BgL_nodez00_bglt BgL_arg1940z00_5162;

				BgL_arg1940z00_5162 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_5078)))->BgL_bodyz00);
				return
					BGl_nodezd2collectz12zc0zzcfa_collectz00(BgL_arg1940z00_5162,
					((BgL_variablez00_bglt) BgL_ownerz00_5079));
			}
		}

	}



/* &node-collect!-let-fu1684 */
	obj_t BGl_z62nodezd2collectz12zd2letzd2fu1684za2zzcfa_collectz00(obj_t
		BgL_envz00_5080, obj_t BgL_nodez00_5081, obj_t BgL_ownerz00_5082)
	{
		{	/* Cfa/collect.scm 331 */
			{	/* Cfa/collect.scm 333 */
				obj_t BgL_g1645z00_5164;

				BgL_g1645z00_5164 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_5081)))->BgL_localsz00);
				{
					obj_t BgL_l1643z00_5166;

					BgL_l1643z00_5166 = BgL_g1645z00_5164;
				BgL_zc3z04anonymousza31931ze3z87_5165:
					if (PAIRP(BgL_l1643z00_5166))
						{	/* Cfa/collect.scm 333 */
							{	/* Cfa/collect.scm 334 */
								obj_t BgL_lz00_5167;

								BgL_lz00_5167 = CAR(BgL_l1643z00_5166);
								{	/* Cfa/collect.scm 334 */
									BgL_valuez00_bglt BgL_fz00_5168;

									BgL_fz00_5168 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_lz00_5167))))->BgL_valuez00);
									{	/* Cfa/collect.scm 335 */
										obj_t BgL_arg1933z00_5169;

										BgL_arg1933z00_5169 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_fz00_5168)))->BgL_bodyz00);
										BGl_nodezd2collectz12zc0zzcfa_collectz00(
											((BgL_nodez00_bglt) BgL_arg1933z00_5169),
											((BgL_variablez00_bglt) BgL_lz00_5167));
									}
								}
							}
							{
								obj_t BgL_l1643z00_5564;

								BgL_l1643z00_5564 = CDR(BgL_l1643z00_5166);
								BgL_l1643z00_5166 = BgL_l1643z00_5564;
								goto BgL_zc3z04anonymousza31931ze3z87_5165;
							}
						}
					else
						{	/* Cfa/collect.scm 333 */
							((bool_t) 1);
						}
				}
			}
			{	/* Cfa/collect.scm 337 */
				BgL_nodez00_bglt BgL_arg1935z00_5170;

				BgL_arg1935z00_5170 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_5081)))->BgL_bodyz00);
				return
					BGl_nodezd2collectz12zc0zzcfa_collectz00(BgL_arg1935z00_5170,
					((BgL_variablez00_bglt) BgL_ownerz00_5082));
			}
		}

	}



/* &node-collect!-switch1682 */
	obj_t BGl_z62nodezd2collectz12zd2switch1682z70zzcfa_collectz00(obj_t
		BgL_envz00_5083, obj_t BgL_nodez00_5084, obj_t BgL_ownerz00_5085)
	{
		{	/* Cfa/collect.scm 321 */
			{	/* Cfa/collect.scm 322 */
				bool_t BgL_tmpz00_5570;

				{	/* Cfa/collect.scm 323 */
					BgL_nodez00_bglt BgL_arg1926z00_5172;

					BgL_arg1926z00_5172 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_5084)))->BgL_testz00);
					BGl_nodezd2collectz12zc0zzcfa_collectz00(BgL_arg1926z00_5172,
						((BgL_variablez00_bglt) BgL_ownerz00_5085));
				}
				{	/* Cfa/collect.scm 324 */
					obj_t BgL_g1642z00_5173;

					BgL_g1642z00_5173 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_5084)))->BgL_clausesz00);
					{
						obj_t BgL_l1640z00_5175;

						BgL_l1640z00_5175 = BgL_g1642z00_5173;
					BgL_zc3z04anonymousza31927ze3z87_5174:
						if (PAIRP(BgL_l1640z00_5175))
							{	/* Cfa/collect.scm 324 */
								{	/* Cfa/collect.scm 325 */
									obj_t BgL_clausez00_5176;

									BgL_clausez00_5176 = CAR(BgL_l1640z00_5175);
									{	/* Cfa/collect.scm 325 */
										obj_t BgL_arg1929z00_5177;

										BgL_arg1929z00_5177 = CDR(((obj_t) BgL_clausez00_5176));
										BGl_nodezd2collectz12zc0zzcfa_collectz00(
											((BgL_nodez00_bglt) BgL_arg1929z00_5177),
											((BgL_variablez00_bglt) BgL_ownerz00_5085));
									}
								}
								{
									obj_t BgL_l1640z00_5585;

									BgL_l1640z00_5585 = CDR(BgL_l1640z00_5175);
									BgL_l1640z00_5175 = BgL_l1640z00_5585;
									goto BgL_zc3z04anonymousza31927ze3z87_5174;
								}
							}
						else
							{	/* Cfa/collect.scm 324 */
								BgL_tmpz00_5570 = ((bool_t) 1);
							}
					}
				}
				return BBOOL(BgL_tmpz00_5570);
			}
		}

	}



/* &node-collect!-fail1680 */
	obj_t BGl_z62nodezd2collectz12zd2fail1680z70zzcfa_collectz00(obj_t
		BgL_envz00_5086, obj_t BgL_nodez00_5087, obj_t BgL_ownerz00_5088)
	{
		{	/* Cfa/collect.scm 312 */
			{	/* Cfa/collect.scm 314 */
				BgL_nodez00_bglt BgL_arg1923z00_5179;

				BgL_arg1923z00_5179 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_5087)))->BgL_procz00);
				BGl_nodezd2collectz12zc0zzcfa_collectz00(BgL_arg1923z00_5179,
					((BgL_variablez00_bglt) BgL_ownerz00_5088));
			}
			{	/* Cfa/collect.scm 315 */
				BgL_nodez00_bglt BgL_arg1924z00_5180;

				BgL_arg1924z00_5180 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_5087)))->BgL_msgz00);
				BGl_nodezd2collectz12zc0zzcfa_collectz00(BgL_arg1924z00_5180,
					((BgL_variablez00_bglt) BgL_ownerz00_5088));
			}
			{	/* Cfa/collect.scm 316 */
				BgL_nodez00_bglt BgL_arg1925z00_5181;

				BgL_arg1925z00_5181 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_5087)))->BgL_objz00);
				return
					BGl_nodezd2collectz12zc0zzcfa_collectz00(BgL_arg1925z00_5181,
					((BgL_variablez00_bglt) BgL_ownerz00_5088));
			}
		}

	}



/* &node-collect!-condit1678 */
	obj_t BGl_z62nodezd2collectz12zd2condit1678z70zzcfa_collectz00(obj_t
		BgL_envz00_5089, obj_t BgL_nodez00_5090, obj_t BgL_ownerz00_5091)
	{
		{	/* Cfa/collect.scm 303 */
			{	/* Cfa/collect.scm 305 */
				BgL_nodez00_bglt BgL_arg1918z00_5183;

				BgL_arg1918z00_5183 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_5090)))->BgL_testz00);
				BGl_nodezd2collectz12zc0zzcfa_collectz00(BgL_arg1918z00_5183,
					((BgL_variablez00_bglt) BgL_ownerz00_5091));
			}
			{	/* Cfa/collect.scm 306 */
				BgL_nodez00_bglt BgL_arg1919z00_5184;

				BgL_arg1919z00_5184 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_5090)))->BgL_truez00);
				BGl_nodezd2collectz12zc0zzcfa_collectz00(BgL_arg1919z00_5184,
					((BgL_variablez00_bglt) BgL_ownerz00_5091));
			}
			{	/* Cfa/collect.scm 307 */
				BgL_nodez00_bglt BgL_arg1920z00_5185;

				BgL_arg1920z00_5185 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_5090)))->BgL_falsez00);
				return
					BGl_nodezd2collectz12zc0zzcfa_collectz00(BgL_arg1920z00_5185,
					((BgL_variablez00_bglt) BgL_ownerz00_5091));
			}
		}

	}



/* &node-collect!-setq1676 */
	obj_t BGl_z62nodezd2collectz12zd2setq1676z70zzcfa_collectz00(obj_t
		BgL_envz00_5092, obj_t BgL_nodez00_5093, obj_t BgL_ownerz00_5094)
	{
		{	/* Cfa/collect.scm 296 */
			{	/* Cfa/collect.scm 298 */
				BgL_nodez00_bglt BgL_arg1917z00_5187;

				BgL_arg1917z00_5187 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_5093)))->BgL_valuez00);
				return
					BGl_nodezd2collectz12zc0zzcfa_collectz00(BgL_arg1917z00_5187,
					((BgL_variablez00_bglt) BgL_ownerz00_5094));
			}
		}

	}



/* &node-collect!-cast1674 */
	obj_t BGl_z62nodezd2collectz12zd2cast1674z70zzcfa_collectz00(obj_t
		BgL_envz00_5095, obj_t BgL_nodez00_5096, obj_t BgL_ownerz00_5097)
	{
		{	/* Cfa/collect.scm 289 */
			{	/* Cfa/collect.scm 291 */
				BgL_nodez00_bglt BgL_arg1916z00_5189;

				BgL_arg1916z00_5189 =
					(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_5096)))->BgL_argz00);
				return
					BGl_nodezd2collectz12zc0zzcfa_collectz00(BgL_arg1916z00_5189,
					((BgL_variablez00_bglt) BgL_ownerz00_5097));
			}
		}

	}



/* &node-collect!-extern1672 */
	obj_t BGl_z62nodezd2collectz12zd2extern1672z70zzcfa_collectz00(obj_t
		BgL_envz00_5098, obj_t BgL_nodez00_5099, obj_t BgL_ownerz00_5100)
	{
		{	/* Cfa/collect.scm 282 */
			return
				BBOOL(BGl_nodezd2collectza2z12z62zzcfa_collectz00(
					(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_nodez00_5099)))->BgL_exprza2za2),
					BgL_ownerz00_5100));
		}

	}



/* &node-collect!-funcal1670 */
	obj_t BGl_z62nodezd2collectz12zd2funcal1670z70zzcfa_collectz00(obj_t
		BgL_envz00_5101, obj_t BgL_nodez00_5102, obj_t BgL_ownerz00_5103)
	{
		{	/* Cfa/collect.scm 273 */
			{	/* Cfa/collect.scm 274 */
				bool_t BgL_tmpz00_5624;

				BGl_addzd2funcallz12zc0zzcfa_closurez00(
					((BgL_nodez00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_5102)));
				{	/* Cfa/collect.scm 276 */
					BgL_nodez00_bglt BgL_arg1912z00_5192;

					BgL_arg1912z00_5192 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_5102)))->BgL_funz00);
					BGl_nodezd2collectz12zc0zzcfa_collectz00(BgL_arg1912z00_5192,
						((BgL_variablez00_bglt) BgL_ownerz00_5103));
				}
				BgL_tmpz00_5624 =
					BGl_nodezd2collectza2z12z62zzcfa_collectz00(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_5102)))->BgL_argsz00),
					BgL_ownerz00_5103);
				return BBOOL(BgL_tmpz00_5624);
			}
		}

	}



/* &node-collect!-app-ly1668 */
	obj_t BGl_z62nodezd2collectz12zd2appzd2ly1668za2zzcfa_collectz00(obj_t
		BgL_envz00_5104, obj_t BgL_nodez00_5105, obj_t BgL_ownerz00_5106)
	{
		{	/* Cfa/collect.scm 265 */
			{	/* Cfa/collect.scm 267 */
				BgL_nodez00_bglt BgL_arg1910z00_5194;

				BgL_arg1910z00_5194 =
					(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_5105)))->BgL_funz00);
				BGl_nodezd2collectz12zc0zzcfa_collectz00(BgL_arg1910z00_5194,
					((BgL_variablez00_bglt) BgL_ownerz00_5106));
			}
			{	/* Cfa/collect.scm 268 */
				BgL_nodez00_bglt BgL_arg1911z00_5195;

				BgL_arg1911z00_5195 =
					(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_5105)))->BgL_argz00);
				return
					BGl_nodezd2collectz12zc0zzcfa_collectz00(BgL_arg1911z00_5195,
					((BgL_variablez00_bglt) BgL_ownerz00_5106));
			}
		}

	}



/* &node-collect!-valloc1666 */
	obj_t BGl_z62nodezd2collectz12zd2valloc1666z70zzcfa_collectz00(obj_t
		BgL_envz00_5107, obj_t BgL_nodez00_5108, obj_t BgL_ownerz00_5109)
	{
		{	/* Cfa/collect.scm 255 */
			{	/* Cfa/collect.scm 257 */
				obj_t BgL_arg1904z00_5197;

				BgL_arg1904z00_5197 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_vallocz00_bglt) BgL_nodez00_5108))))->BgL_exprza2za2);
				BGl_nodezd2collectza2z12z62zzcfa_collectz00(BgL_arg1904z00_5197,
					BgL_ownerz00_5109);
			}
			if (BGl_vectorzd2optimzf3z21zzcfa_vectorz00())
				{	/* Cfa/collect.scm 258 */
					BGl_za2usedzd2allocza2zd2zzcfa_collectz00 =
						MAKE_YOUNG_PAIR(
						((obj_t)
							((BgL_vallocz00_bglt) BgL_nodez00_5108)),
						BGl_za2usedzd2allocza2zd2zzcfa_collectz00);
					{	/* Cfa/collect.scm 260 */
						BgL_prezd2valloczf2cinfoz20_bglt BgL_wide1248z00_5198;

						BgL_wide1248z00_5198 =
							((BgL_prezd2valloczf2cinfoz20_bglt)
							BOBJECT(GC_MALLOC(sizeof(struct
										BgL_prezd2valloczf2cinfoz20_bgl))));
						{	/* Cfa/collect.scm 260 */
							obj_t BgL_auxz00_5658;
							BgL_objectz00_bglt BgL_tmpz00_5654;

							BgL_auxz00_5658 = ((obj_t) BgL_wide1248z00_5198);
							BgL_tmpz00_5654 =
								((BgL_objectz00_bglt)
								((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_nodez00_5108)));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5654, BgL_auxz00_5658);
						}
						((BgL_objectz00_bglt)
							((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_nodez00_5108)));
						{	/* Cfa/collect.scm 260 */
							long BgL_arg1906z00_5199;

							BgL_arg1906z00_5199 =
								BGL_CLASS_NUM(BGl_prezd2valloczf2Cinfoz20zzcfa_info3z00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_vallocz00_bglt)
										((BgL_vallocz00_bglt) BgL_nodez00_5108))),
								BgL_arg1906z00_5199);
						}
						((BgL_vallocz00_bglt)
							((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_nodez00_5108)));
					}
					{
						BgL_prezd2valloczf2cinfoz20_bglt BgL_auxz00_5672;

						{
							obj_t BgL_auxz00_5673;

							{	/* Cfa/collect.scm 260 */
								BgL_objectz00_bglt BgL_tmpz00_5674;

								BgL_tmpz00_5674 =
									((BgL_objectz00_bglt)
									((BgL_vallocz00_bglt)
										((BgL_vallocz00_bglt) BgL_nodez00_5108)));
								BgL_auxz00_5673 = BGL_OBJECT_WIDENING(BgL_tmpz00_5674);
							}
							BgL_auxz00_5672 =
								((BgL_prezd2valloczf2cinfoz20_bglt) BgL_auxz00_5673);
						}
						((((BgL_prezd2valloczf2cinfoz20_bglt) COBJECT(BgL_auxz00_5672))->
								BgL_ownerz00) =
							((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
									BgL_ownerz00_5109)), BUNSPEC);
					}
					return
						((obj_t)
						((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_nodez00_5108)));
				}
			else
				{	/* Cfa/collect.scm 258 */
					return BFALSE;
				}
		}

	}



/* &node-collect!-app1664 */
	obj_t BGl_z62nodezd2collectz12zd2app1664z70zzcfa_collectz00(obj_t
		BgL_envz00_5110, obj_t BgL_nodez00_5111, obj_t BgL_ownerz00_5112)
	{
		{	/* Cfa/collect.scm 181 */
			BGl_nodezd2collectza2z12z62zzcfa_collectz00(
				(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_5111)))->BgL_argsz00),
				BgL_ownerz00_5112);
			{	/* Cfa/collect.scm 184 */
				BgL_varz00_bglt BgL_arg1856z00_5201;

				BgL_arg1856z00_5201 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_5111)))->BgL_funz00);
				BGl_nodezd2collectz12zc0zzcfa_collectz00(
					((BgL_nodez00_bglt) BgL_arg1856z00_5201),
					((BgL_variablez00_bglt) BgL_ownerz00_5112));
			}
			{	/* Cfa/collect.scm 185 */
				BgL_variablez00_bglt BgL_vz00_5202;

				BgL_vz00_5202 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_nodez00_5111)))->BgL_funz00)))->
					BgL_variablez00);
				{	/* Cfa/collect.scm 186 */
					bool_t BgL_test2230z00_5696;

					{	/* Cfa/collect.scm 186 */
						obj_t BgL_classz00_5203;

						BgL_classz00_5203 = BGl_globalz00zzast_varz00;
						{	/* Cfa/collect.scm 186 */
							BgL_objectz00_bglt BgL_arg1807z00_5204;

							{	/* Cfa/collect.scm 186 */
								obj_t BgL_tmpz00_5697;

								BgL_tmpz00_5697 =
									((obj_t) ((BgL_objectz00_bglt) BgL_vz00_5202));
								BgL_arg1807z00_5204 = (BgL_objectz00_bglt) (BgL_tmpz00_5697);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Cfa/collect.scm 186 */
									long BgL_idxz00_5205;

									BgL_idxz00_5205 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5204);
									BgL_test2230z00_5696 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_5205 + 2L)) == BgL_classz00_5203);
								}
							else
								{	/* Cfa/collect.scm 186 */
									bool_t BgL_res2156z00_5208;

									{	/* Cfa/collect.scm 186 */
										obj_t BgL_oclassz00_5209;

										{	/* Cfa/collect.scm 186 */
											obj_t BgL_arg1815z00_5210;
											long BgL_arg1816z00_5211;

											BgL_arg1815z00_5210 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Cfa/collect.scm 186 */
												long BgL_arg1817z00_5212;

												BgL_arg1817z00_5212 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5204);
												BgL_arg1816z00_5211 =
													(BgL_arg1817z00_5212 - OBJECT_TYPE);
											}
											BgL_oclassz00_5209 =
												VECTOR_REF(BgL_arg1815z00_5210, BgL_arg1816z00_5211);
										}
										{	/* Cfa/collect.scm 186 */
											bool_t BgL__ortest_1115z00_5213;

											BgL__ortest_1115z00_5213 =
												(BgL_classz00_5203 == BgL_oclassz00_5209);
											if (BgL__ortest_1115z00_5213)
												{	/* Cfa/collect.scm 186 */
													BgL_res2156z00_5208 = BgL__ortest_1115z00_5213;
												}
											else
												{	/* Cfa/collect.scm 186 */
													long BgL_odepthz00_5214;

													{	/* Cfa/collect.scm 186 */
														obj_t BgL_arg1804z00_5215;

														BgL_arg1804z00_5215 = (BgL_oclassz00_5209);
														BgL_odepthz00_5214 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_5215);
													}
													if ((2L < BgL_odepthz00_5214))
														{	/* Cfa/collect.scm 186 */
															obj_t BgL_arg1802z00_5216;

															{	/* Cfa/collect.scm 186 */
																obj_t BgL_arg1803z00_5217;

																BgL_arg1803z00_5217 = (BgL_oclassz00_5209);
																BgL_arg1802z00_5216 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5217,
																	2L);
															}
															BgL_res2156z00_5208 =
																(BgL_arg1802z00_5216 == BgL_classz00_5203);
														}
													else
														{	/* Cfa/collect.scm 186 */
															BgL_res2156z00_5208 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2230z00_5696 = BgL_res2156z00_5208;
								}
						}
					}
					if (BgL_test2230z00_5696)
						{	/* Cfa/collect.scm 187 */
							bool_t BgL_test2234z00_5720;

							{	/* Cfa/collect.scm 187 */
								BgL_valuez00_bglt BgL_arg1902z00_5218;

								BgL_arg1902z00_5218 =
									(((BgL_variablez00_bglt) COBJECT(BgL_vz00_5202))->
									BgL_valuez00);
								{	/* Cfa/collect.scm 187 */
									obj_t BgL_classz00_5219;

									BgL_classz00_5219 = BGl_cfunz00zzast_varz00;
									{	/* Cfa/collect.scm 187 */
										BgL_objectz00_bglt BgL_arg1807z00_5220;

										{	/* Cfa/collect.scm 187 */
											obj_t BgL_tmpz00_5722;

											BgL_tmpz00_5722 =
												((obj_t) ((BgL_objectz00_bglt) BgL_arg1902z00_5218));
											BgL_arg1807z00_5220 =
												(BgL_objectz00_bglt) (BgL_tmpz00_5722);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Cfa/collect.scm 187 */
												long BgL_idxz00_5221;

												BgL_idxz00_5221 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5220);
												BgL_test2234z00_5720 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_5221 + 3L)) == BgL_classz00_5219);
											}
										else
											{	/* Cfa/collect.scm 187 */
												bool_t BgL_res2157z00_5224;

												{	/* Cfa/collect.scm 187 */
													obj_t BgL_oclassz00_5225;

													{	/* Cfa/collect.scm 187 */
														obj_t BgL_arg1815z00_5226;
														long BgL_arg1816z00_5227;

														BgL_arg1815z00_5226 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Cfa/collect.scm 187 */
															long BgL_arg1817z00_5228;

															BgL_arg1817z00_5228 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5220);
															BgL_arg1816z00_5227 =
																(BgL_arg1817z00_5228 - OBJECT_TYPE);
														}
														BgL_oclassz00_5225 =
															VECTOR_REF(BgL_arg1815z00_5226,
															BgL_arg1816z00_5227);
													}
													{	/* Cfa/collect.scm 187 */
														bool_t BgL__ortest_1115z00_5229;

														BgL__ortest_1115z00_5229 =
															(BgL_classz00_5219 == BgL_oclassz00_5225);
														if (BgL__ortest_1115z00_5229)
															{	/* Cfa/collect.scm 187 */
																BgL_res2157z00_5224 = BgL__ortest_1115z00_5229;
															}
														else
															{	/* Cfa/collect.scm 187 */
																long BgL_odepthz00_5230;

																{	/* Cfa/collect.scm 187 */
																	obj_t BgL_arg1804z00_5231;

																	BgL_arg1804z00_5231 = (BgL_oclassz00_5225);
																	BgL_odepthz00_5230 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_5231);
																}
																if ((3L < BgL_odepthz00_5230))
																	{	/* Cfa/collect.scm 187 */
																		obj_t BgL_arg1802z00_5232;

																		{	/* Cfa/collect.scm 187 */
																			obj_t BgL_arg1803z00_5233;

																			BgL_arg1803z00_5233 =
																				(BgL_oclassz00_5225);
																			BgL_arg1802z00_5232 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_5233, 3L);
																		}
																		BgL_res2157z00_5224 =
																			(BgL_arg1802z00_5232 ==
																			BgL_classz00_5219);
																	}
																else
																	{	/* Cfa/collect.scm 187 */
																		BgL_res2157z00_5224 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2234z00_5720 = BgL_res2157z00_5224;
											}
									}
								}
							}
							if (BgL_test2234z00_5720)
								{	/* Cfa/collect.scm 193 */
									obj_t BgL_casezd2valuezd2_5234;

									BgL_casezd2valuezd2_5234 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_vz00_5202))))->BgL_idz00);
									if ((BgL_casezd2valuezd2_5234 == CNST_TABLE_REF(1)))
										{	/* Cfa/collect.scm 195 */
											bool_t BgL_test2239z00_5751;

											if (CBOOL
												(BGl_za2optimzd2cfazd2fixnumzd2arithmeticzf3za2z21zzengine_paramz00))
												{	/* Cfa/collect.scm 195 */
													BgL_test2239z00_5751 = ((bool_t) 1);
												}
											else
												{	/* Cfa/collect.scm 195 */
													BgL_test2239z00_5751 =
														CBOOL
														(BGl_za2optimzd2cfazd2flonumzd2arithmeticzf3za2z21zzengine_paramz00);
												}
											if (BgL_test2239z00_5751)
												{	/* Cfa/collect.scm 195 */
													{	/* Cfa/collect.scm 197 */
														BgL_prezd2arithmeticzd2appz00_bglt
															BgL_wide1186z00_5235;
														BgL_wide1186z00_5235 =
															((BgL_prezd2arithmeticzd2appz00_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_prezd2arithmeticzd2appz00_bgl))));
														{	/* Cfa/collect.scm 197 */
															obj_t BgL_auxz00_5760;
															BgL_objectz00_bglt BgL_tmpz00_5756;

															BgL_auxz00_5760 = ((obj_t) BgL_wide1186z00_5235);
															BgL_tmpz00_5756 =
																((BgL_objectz00_bglt)
																((BgL_appz00_bglt)
																	((BgL_appz00_bglt) BgL_nodez00_5111)));
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5756,
																BgL_auxz00_5760);
														}
														((BgL_objectz00_bglt)
															((BgL_appz00_bglt)
																((BgL_appz00_bglt) BgL_nodez00_5111)));
														{	/* Cfa/collect.scm 197 */
															long BgL_arg1862z00_5236;

															BgL_arg1862z00_5236 =
																BGL_CLASS_NUM
																(BGl_prezd2arithmeticzd2appz00zzcfa_info2z00);
															BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
																		(BgL_appz00_bglt) ((BgL_appz00_bglt)
																			BgL_nodez00_5111))), BgL_arg1862z00_5236);
														}
														((BgL_appz00_bglt)
															((BgL_appz00_bglt)
																((BgL_appz00_bglt) BgL_nodez00_5111)));
													}
													{
														BgL_prezd2arithmeticzd2appz00_bglt BgL_auxz00_5774;

														{
															obj_t BgL_auxz00_5775;

															{	/* Cfa/collect.scm 198 */
																BgL_objectz00_bglt BgL_tmpz00_5776;

																BgL_tmpz00_5776 =
																	((BgL_objectz00_bglt)
																	((BgL_appz00_bglt)
																		((BgL_appz00_bglt) BgL_nodez00_5111)));
																BgL_auxz00_5775 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5776);
															}
															BgL_auxz00_5774 =
																((BgL_prezd2arithmeticzd2appz00_bglt)
																BgL_auxz00_5775);
														}
														((((BgL_prezd2arithmeticzd2appz00_bglt)
																	COBJECT(BgL_auxz00_5774))->
																BgL_speczd2typeszd2) =
															((obj_t)
																BGl_arithmeticzd2speczd2typesz00zzcfa_specializa7eza7
																(((BgL_globalz00_bglt) BgL_vz00_5202))),
															BUNSPEC);
													}
													return
														((obj_t)
														((BgL_appz00_bglt)
															((BgL_appz00_bglt) BgL_nodez00_5111)));
												}
											else
												{	/* Cfa/collect.scm 195 */
													return BFALSE;
												}
										}
									else
										{	/* Cfa/collect.scm 193 */
											if ((BgL_casezd2valuezd2_5234 == CNST_TABLE_REF(2)))
												{	/* Cfa/collect.scm 193 */
													BGl_za2usedzd2allocza2zd2zzcfa_collectz00 =
														MAKE_YOUNG_PAIR(
														((obj_t)
															((BgL_appz00_bglt) BgL_nodez00_5111)),
														BGl_za2usedzd2allocza2zd2zzcfa_collectz00);
													{	/* Cfa/collect.scm 201 */
														BgL_prezd2makezd2procedurezd2appzd2_bglt
															BgL_wide1190z00_5237;
														BgL_wide1190z00_5237 =
															((BgL_prezd2makezd2procedurezd2appzd2_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_prezd2makezd2procedurezd2appzd2_bgl))));
														{	/* Cfa/collect.scm 201 */
															obj_t BgL_auxz00_5799;
															BgL_objectz00_bglt BgL_tmpz00_5795;

															BgL_auxz00_5799 = ((obj_t) BgL_wide1190z00_5237);
															BgL_tmpz00_5795 =
																((BgL_objectz00_bglt)
																((BgL_appz00_bglt)
																	((BgL_appz00_bglt) BgL_nodez00_5111)));
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5795,
																BgL_auxz00_5799);
														}
														((BgL_objectz00_bglt)
															((BgL_appz00_bglt)
																((BgL_appz00_bglt) BgL_nodez00_5111)));
														{	/* Cfa/collect.scm 201 */
															long BgL_arg1864z00_5238;

															BgL_arg1864z00_5238 =
																BGL_CLASS_NUM
																(BGl_prezd2makezd2procedurezd2appzd2zzcfa_info2z00);
															BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
																		(BgL_appz00_bglt) ((BgL_appz00_bglt)
																			BgL_nodez00_5111))), BgL_arg1864z00_5238);
														}
														((BgL_appz00_bglt)
															((BgL_appz00_bglt)
																((BgL_appz00_bglt) BgL_nodez00_5111)));
													}
													{
														BgL_prezd2makezd2procedurezd2appzd2_bglt
															BgL_auxz00_5813;
														{
															obj_t BgL_auxz00_5814;

															{	/* Cfa/collect.scm 201 */
																BgL_objectz00_bglt BgL_tmpz00_5815;

																BgL_tmpz00_5815 =
																	((BgL_objectz00_bglt)
																	((BgL_appz00_bglt)
																		((BgL_appz00_bglt) BgL_nodez00_5111)));
																BgL_auxz00_5814 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5815);
															}
															BgL_auxz00_5813 =
																((BgL_prezd2makezd2procedurezd2appzd2_bglt)
																BgL_auxz00_5814);
														}
														((((BgL_prezd2makezd2procedurezd2appzd2_bglt)
																	COBJECT(BgL_auxz00_5813))->BgL_ownerz00) =
															((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
																	BgL_ownerz00_5112)), BUNSPEC);
													}
													return
														((obj_t)
														((BgL_appz00_bglt)
															((BgL_appz00_bglt) BgL_nodez00_5111)));
												}
											else
												{	/* Cfa/collect.scm 193 */
													if ((BgL_casezd2valuezd2_5234 == CNST_TABLE_REF(3)))
														{	/* Cfa/collect.scm 193 */
															BGl_za2usedzd2allocza2zd2zzcfa_collectz00 =
																MAKE_YOUNG_PAIR(
																((obj_t)
																	((BgL_appz00_bglt) BgL_nodez00_5111)),
																BGl_za2usedzd2allocza2zd2zzcfa_collectz00);
															{	/* Cfa/collect.scm 204 */
																BgL_prezd2makezd2procedurezd2appzd2_bglt
																	BgL_wide1194z00_5239;
																BgL_wide1194z00_5239 =
																	((BgL_prezd2makezd2procedurezd2appzd2_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_prezd2makezd2procedurezd2appzd2_bgl))));
																{	/* Cfa/collect.scm 204 */
																	obj_t BgL_auxz00_5837;
																	BgL_objectz00_bglt BgL_tmpz00_5833;

																	BgL_auxz00_5837 =
																		((obj_t) BgL_wide1194z00_5239);
																	BgL_tmpz00_5833 =
																		((BgL_objectz00_bglt)
																		((BgL_appz00_bglt)
																			((BgL_appz00_bglt) BgL_nodez00_5111)));
																	BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5833,
																		BgL_auxz00_5837);
																}
																((BgL_objectz00_bglt)
																	((BgL_appz00_bglt)
																		((BgL_appz00_bglt) BgL_nodez00_5111)));
																{	/* Cfa/collect.scm 204 */
																	long BgL_arg1866z00_5240;

																	BgL_arg1866z00_5240 =
																		BGL_CLASS_NUM
																		(BGl_prezd2makezd2procedurezd2appzd2zzcfa_info2z00);
																	BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																			((BgL_appz00_bglt) ((BgL_appz00_bglt)
																					BgL_nodez00_5111))),
																		BgL_arg1866z00_5240);
																}
																((BgL_appz00_bglt)
																	((BgL_appz00_bglt)
																		((BgL_appz00_bglt) BgL_nodez00_5111)));
															}
															{
																BgL_prezd2makezd2procedurezd2appzd2_bglt
																	BgL_auxz00_5851;
																{
																	obj_t BgL_auxz00_5852;

																	{	/* Cfa/collect.scm 204 */
																		BgL_objectz00_bglt BgL_tmpz00_5853;

																		BgL_tmpz00_5853 =
																			((BgL_objectz00_bglt)
																			((BgL_appz00_bglt)
																				((BgL_appz00_bglt) BgL_nodez00_5111)));
																		BgL_auxz00_5852 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_5853);
																	}
																	BgL_auxz00_5851 =
																		((BgL_prezd2makezd2procedurezd2appzd2_bglt)
																		BgL_auxz00_5852);
																}
																((((BgL_prezd2makezd2procedurezd2appzd2_bglt)
																			COBJECT(BgL_auxz00_5851))->BgL_ownerz00) =
																	((BgL_variablez00_bglt) (
																			(BgL_variablez00_bglt)
																			BgL_ownerz00_5112)), BUNSPEC);
															}
															return
																((obj_t)
																((BgL_appz00_bglt)
																	((BgL_appz00_bglt) BgL_nodez00_5111)));
														}
													else
														{	/* Cfa/collect.scm 193 */
															if (
																(BgL_casezd2valuezd2_5234 == CNST_TABLE_REF(4)))
																{	/* Cfa/collect.scm 193 */
																	{	/* Cfa/collect.scm 206 */
																		BgL_prezd2procedurezd2refzd2appzd2_bglt
																			BgL_wide1198z00_5241;
																		BgL_wide1198z00_5241 =
																			((BgL_prezd2procedurezd2refzd2appzd2_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_prezd2procedurezd2refzd2appzd2_bgl))));
																		{	/* Cfa/collect.scm 206 */
																			obj_t BgL_auxz00_5872;
																			BgL_objectz00_bglt BgL_tmpz00_5868;

																			BgL_auxz00_5872 =
																				((obj_t) BgL_wide1198z00_5241);
																			BgL_tmpz00_5868 =
																				((BgL_objectz00_bglt)
																				((BgL_appz00_bglt)
																					((BgL_appz00_bglt)
																						BgL_nodez00_5111)));
																			BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5868,
																				BgL_auxz00_5872);
																		}
																		((BgL_objectz00_bglt)
																			((BgL_appz00_bglt)
																				((BgL_appz00_bglt) BgL_nodez00_5111)));
																		{	/* Cfa/collect.scm 206 */
																			long BgL_arg1868z00_5242;

																			BgL_arg1868z00_5242 =
																				BGL_CLASS_NUM
																				(BGl_prezd2procedurezd2refzd2appzd2zzcfa_info2z00);
																			BGL_OBJECT_CLASS_NUM_SET((
																					(BgL_objectz00_bglt) (
																						(BgL_appz00_bglt) ((BgL_appz00_bglt)
																							BgL_nodez00_5111))),
																				BgL_arg1868z00_5242);
																		}
																		((BgL_appz00_bglt)
																			((BgL_appz00_bglt)
																				((BgL_appz00_bglt) BgL_nodez00_5111)));
																	}
																	return
																		((obj_t)
																		((BgL_appz00_bglt)
																			((BgL_appz00_bglt) BgL_nodez00_5111)));
																}
															else
																{	/* Cfa/collect.scm 193 */
																	if (
																		(BgL_casezd2valuezd2_5234 ==
																			CNST_TABLE_REF(5)))
																		{	/* Cfa/collect.scm 193 */
																			{	/* Cfa/collect.scm 208 */
																				BgL_prezd2procedurezd2setz12zd2appzc0_bglt
																					BgL_wide1203z00_5243;
																				BgL_wide1203z00_5243 =
																					(
																					(BgL_prezd2procedurezd2setz12zd2appzc0_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_prezd2procedurezd2setz12zd2appzc0_bgl))));
																				{	/* Cfa/collect.scm 208 */
																					obj_t BgL_auxz00_5897;
																					BgL_objectz00_bglt BgL_tmpz00_5893;

																					BgL_auxz00_5897 =
																						((obj_t) BgL_wide1203z00_5243);
																					BgL_tmpz00_5893 =
																						((BgL_objectz00_bglt)
																						((BgL_appz00_bglt)
																							((BgL_appz00_bglt)
																								BgL_nodez00_5111)));
																					BGL_OBJECT_WIDENING_SET
																						(BgL_tmpz00_5893, BgL_auxz00_5897);
																				}
																				((BgL_objectz00_bglt)
																					((BgL_appz00_bglt)
																						((BgL_appz00_bglt)
																							BgL_nodez00_5111)));
																				{	/* Cfa/collect.scm 208 */
																					long BgL_arg1870z00_5244;

																					BgL_arg1870z00_5244 =
																						BGL_CLASS_NUM
																						(BGl_prezd2procedurezd2setz12zd2appzc0zzcfa_info2z00);
																					BGL_OBJECT_CLASS_NUM_SET((
																							(BgL_objectz00_bglt) (
																								(BgL_appz00_bglt) (
																									(BgL_appz00_bglt)
																									BgL_nodez00_5111))),
																						BgL_arg1870z00_5244);
																				}
																				((BgL_appz00_bglt)
																					((BgL_appz00_bglt)
																						((BgL_appz00_bglt)
																							BgL_nodez00_5111)));
																			}
																			return
																				((obj_t)
																				((BgL_appz00_bglt)
																					((BgL_appz00_bglt)
																						BgL_nodez00_5111)));
																		}
																	else
																		{	/* Cfa/collect.scm 193 */
																			if (
																				(BgL_casezd2valuezd2_5234 ==
																					CNST_TABLE_REF(6)))
																				{	/* Cfa/collect.scm 193 */
																					if (BGl_vectorzd2optimzf3z21zzcfa_vectorz00())
																						{	/* Cfa/collect.scm 210 */
																							BGl_za2usedzd2allocza2zd2zzcfa_collectz00
																								=
																								MAKE_YOUNG_PAIR(((obj_t) (
																										(BgL_appz00_bglt)
																										BgL_nodez00_5111)),
																								BGl_za2usedzd2allocza2zd2zzcfa_collectz00);
																							{	/* Cfa/collect.scm 212 */
																								BgL_prezd2makezd2vectorzd2appzd2_bglt
																									BgL_wide1207z00_5245;
																								BgL_wide1207z00_5245 =
																									(
																									(BgL_prezd2makezd2vectorzd2appzd2_bglt)
																									BOBJECT(GC_MALLOC(sizeof
																											(struct
																												BgL_prezd2makezd2vectorzd2appzd2_bgl))));
																								{	/* Cfa/collect.scm 212 */
																									obj_t BgL_auxz00_5927;
																									BgL_objectz00_bglt
																										BgL_tmpz00_5923;
																									BgL_auxz00_5927 =
																										((obj_t)
																										BgL_wide1207z00_5245);
																									BgL_tmpz00_5923 =
																										((BgL_objectz00_bglt) (
																											(BgL_appz00_bglt) (
																												(BgL_appz00_bglt)
																												BgL_nodez00_5111)));
																									BGL_OBJECT_WIDENING_SET
																										(BgL_tmpz00_5923,
																										BgL_auxz00_5927);
																								}
																								((BgL_objectz00_bglt)
																									((BgL_appz00_bglt)
																										((BgL_appz00_bglt)
																											BgL_nodez00_5111)));
																								{	/* Cfa/collect.scm 212 */
																									long BgL_arg1873z00_5246;

																									BgL_arg1873z00_5246 =
																										BGL_CLASS_NUM
																										(BGl_prezd2makezd2vectorzd2appzd2zzcfa_info2z00);
																									BGL_OBJECT_CLASS_NUM_SET((
																											(BgL_objectz00_bglt) (
																												(BgL_appz00_bglt) (
																													(BgL_appz00_bglt)
																													BgL_nodez00_5111))),
																										BgL_arg1873z00_5246);
																								}
																								((BgL_appz00_bglt)
																									((BgL_appz00_bglt)
																										((BgL_appz00_bglt)
																											BgL_nodez00_5111)));
																							}
																							{
																								BgL_prezd2makezd2vectorzd2appzd2_bglt
																									BgL_auxz00_5941;
																								{
																									obj_t BgL_auxz00_5942;

																									{	/* Cfa/collect.scm 212 */
																										BgL_objectz00_bglt
																											BgL_tmpz00_5943;
																										BgL_tmpz00_5943 =
																											((BgL_objectz00_bglt) (
																												(BgL_appz00_bglt) (
																													(BgL_appz00_bglt)
																													BgL_nodez00_5111)));
																										BgL_auxz00_5942 =
																											BGL_OBJECT_WIDENING
																											(BgL_tmpz00_5943);
																									}
																									BgL_auxz00_5941 =
																										(
																										(BgL_prezd2makezd2vectorzd2appzd2_bglt)
																										BgL_auxz00_5942);
																								}
																								((((BgL_prezd2makezd2vectorzd2appzd2_bglt) COBJECT(BgL_auxz00_5941))->BgL_ownerz00) = ((BgL_variablez00_bglt) ((BgL_variablez00_bglt) BgL_ownerz00_5112)), BUNSPEC);
																							}
																							return
																								((obj_t)
																								((BgL_appz00_bglt)
																									((BgL_appz00_bglt)
																										BgL_nodez00_5111)));
																						}
																					else
																						{	/* Cfa/collect.scm 210 */
																							return BFALSE;
																						}
																				}
																			else
																				{	/* Cfa/collect.scm 193 */
																					if (
																						(BgL_casezd2valuezd2_5234 ==
																							CNST_TABLE_REF(7)))
																						{	/* Cfa/collect.scm 193 */
																							if (
																								((long)
																									CINT
																									(BGl_za2optimza2z00zzengine_paramz00)
																									>= 2L))
																								{	/* Cfa/collect.scm 214 */
																									BGl_za2usedzd2allocza2zd2zzcfa_collectz00
																										=
																										MAKE_YOUNG_PAIR(((obj_t) (
																												(BgL_appz00_bglt)
																												BgL_nodez00_5111)),
																										BGl_za2usedzd2allocza2zd2zzcfa_collectz00);
																									{	/* Cfa/collect.scm 216 */
																										BgL_prezd2makezd2structzd2appzd2_bglt
																											BgL_wide1211z00_5247;
																										BgL_wide1211z00_5247 =
																											(
																											(BgL_prezd2makezd2structzd2appzd2_bglt)
																											BOBJECT(GC_MALLOC(sizeof
																													(struct
																														BgL_prezd2makezd2structzd2appzd2_bgl))));
																										{	/* Cfa/collect.scm 216 */
																											obj_t BgL_auxz00_5968;
																											BgL_objectz00_bglt
																												BgL_tmpz00_5964;
																											BgL_auxz00_5968 =
																												((obj_t)
																												BgL_wide1211z00_5247);
																											BgL_tmpz00_5964 =
																												((BgL_objectz00_bglt) (
																													(BgL_appz00_bglt) (
																														(BgL_appz00_bglt)
																														BgL_nodez00_5111)));
																											BGL_OBJECT_WIDENING_SET
																												(BgL_tmpz00_5964,
																												BgL_auxz00_5968);
																										}
																										((BgL_objectz00_bglt)
																											((BgL_appz00_bglt)
																												((BgL_appz00_bglt)
																													BgL_nodez00_5111)));
																										{	/* Cfa/collect.scm 216 */
																											long BgL_arg1876z00_5248;

																											BgL_arg1876z00_5248 =
																												BGL_CLASS_NUM
																												(BGl_prezd2makezd2structzd2appzd2zzcfa_info2z00);
																											BGL_OBJECT_CLASS_NUM_SET((
																													(BgL_objectz00_bglt) (
																														(BgL_appz00_bglt) (
																															(BgL_appz00_bglt)
																															BgL_nodez00_5111))),
																												BgL_arg1876z00_5248);
																										}
																										((BgL_appz00_bglt)
																											((BgL_appz00_bglt)
																												((BgL_appz00_bglt)
																													BgL_nodez00_5111)));
																									}
																									{
																										BgL_prezd2makezd2structzd2appzd2_bglt
																											BgL_auxz00_5982;
																										{
																											obj_t BgL_auxz00_5983;

																											{	/* Cfa/collect.scm 216 */
																												BgL_objectz00_bglt
																													BgL_tmpz00_5984;
																												BgL_tmpz00_5984 =
																													((BgL_objectz00_bglt)
																													((BgL_appz00_bglt) (
																															(BgL_appz00_bglt)
																															BgL_nodez00_5111)));
																												BgL_auxz00_5983 =
																													BGL_OBJECT_WIDENING
																													(BgL_tmpz00_5984);
																											}
																											BgL_auxz00_5982 =
																												(
																												(BgL_prezd2makezd2structzd2appzd2_bglt)
																												BgL_auxz00_5983);
																										}
																										((((BgL_prezd2makezd2structzd2appzd2_bglt) COBJECT(BgL_auxz00_5982))->BgL_ownerz00) = ((BgL_variablez00_bglt) ((BgL_variablez00_bglt) BgL_ownerz00_5112)), BUNSPEC);
																									}
																									return
																										((obj_t)
																										((BgL_appz00_bglt)
																											((BgL_appz00_bglt)
																												BgL_nodez00_5111)));
																								}
																							else
																								{	/* Cfa/collect.scm 214 */
																									return BFALSE;
																								}
																						}
																					else
																						{	/* Cfa/collect.scm 193 */
																							if (
																								(BgL_casezd2valuezd2_5234 ==
																									CNST_TABLE_REF(8)))
																								{	/* Cfa/collect.scm 193 */
																									if (
																										((long)
																											CINT
																											(BGl_za2optimza2z00zzengine_paramz00)
																											>= 2L))
																										{	/* Cfa/collect.scm 218 */
																											{	/* Cfa/collect.scm 219 */
																												BgL_prezd2structzd2refzd2appzd2_bglt
																													BgL_wide1215z00_5249;
																												BgL_wide1215z00_5249 =
																													(
																													(BgL_prezd2structzd2refzd2appzd2_bglt)
																													BOBJECT(GC_MALLOC
																														(sizeof(struct
																																BgL_prezd2structzd2refzd2appzd2_bgl))));
																												{	/* Cfa/collect.scm 219 */
																													obj_t BgL_auxz00_6006;
																													BgL_objectz00_bglt
																														BgL_tmpz00_6002;
																													BgL_auxz00_6006 =
																														((obj_t)
																														BgL_wide1215z00_5249);
																													BgL_tmpz00_6002 =
																														(
																														(BgL_objectz00_bglt)
																														((BgL_appz00_bglt) (
																																(BgL_appz00_bglt)
																																BgL_nodez00_5111)));
																													BGL_OBJECT_WIDENING_SET
																														(BgL_tmpz00_6002,
																														BgL_auxz00_6006);
																												}
																												((BgL_objectz00_bglt)
																													((BgL_appz00_bglt)
																														((BgL_appz00_bglt)
																															BgL_nodez00_5111)));
																												{	/* Cfa/collect.scm 219 */
																													long
																														BgL_arg1879z00_5250;
																													BgL_arg1879z00_5250 =
																														BGL_CLASS_NUM
																														(BGl_prezd2structzd2refzd2appzd2zzcfa_info2z00);
																													BGL_OBJECT_CLASS_NUM_SET
																														(((BgL_objectz00_bglt) ((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111))), BgL_arg1879z00_5250);
																												}
																												((BgL_appz00_bglt)
																													((BgL_appz00_bglt)
																														((BgL_appz00_bglt)
																															BgL_nodez00_5111)));
																											}
																											return
																												((obj_t)
																												((BgL_appz00_bglt)
																													((BgL_appz00_bglt)
																														BgL_nodez00_5111)));
																										}
																									else
																										{	/* Cfa/collect.scm 218 */
																											return BFALSE;
																										}
																								}
																							else
																								{	/* Cfa/collect.scm 193 */
																									if (
																										(BgL_casezd2valuezd2_5234 ==
																											CNST_TABLE_REF(9)))
																										{	/* Cfa/collect.scm 193 */
																											if (
																												((long)
																													CINT
																													(BGl_za2optimza2z00zzengine_paramz00)
																													>= 2L))
																												{	/* Cfa/collect.scm 221 */
																													{	/* Cfa/collect.scm 222 */
																														BgL_prezd2structzd2setz12zd2appzc0_bglt
																															BgL_wide1219z00_5251;
																														BgL_wide1219z00_5251
																															=
																															(
																															(BgL_prezd2structzd2setz12zd2appzc0_bglt)
																															BOBJECT(GC_MALLOC
																																(sizeof(struct
																																		BgL_prezd2structzd2setz12zd2appzc0_bgl))));
																														{	/* Cfa/collect.scm 222 */
																															obj_t
																																BgL_auxz00_6034;
																															BgL_objectz00_bglt
																																BgL_tmpz00_6030;
																															BgL_auxz00_6034 =
																																((obj_t)
																																BgL_wide1219z00_5251);
																															BgL_tmpz00_6030 =
																																(
																																(BgL_objectz00_bglt)
																																((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111)));
																															BGL_OBJECT_WIDENING_SET
																																(BgL_tmpz00_6030,
																																BgL_auxz00_6034);
																														}
																														((BgL_objectz00_bglt) ((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111)));
																														{	/* Cfa/collect.scm 222 */
																															long
																																BgL_arg1882z00_5252;
																															BgL_arg1882z00_5252
																																=
																																BGL_CLASS_NUM
																																(BGl_prezd2structzd2setz12zd2appzc0zzcfa_info2z00);
																															BGL_OBJECT_CLASS_NUM_SET
																																(((BgL_objectz00_bglt) ((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111))), BgL_arg1882z00_5252);
																														}
																														((BgL_appz00_bglt)
																															((BgL_appz00_bglt)
																																((BgL_appz00_bglt) BgL_nodez00_5111)));
																													}
																													return
																														((obj_t)
																														((BgL_appz00_bglt)
																															((BgL_appz00_bglt)
																																BgL_nodez00_5111)));
																												}
																											else
																												{	/* Cfa/collect.scm 221 */
																													return BFALSE;
																												}
																										}
																									else
																										{	/* Cfa/collect.scm 193 */
																											if (
																												(BgL_casezd2valuezd2_5234
																													==
																													CNST_TABLE_REF(10)))
																												{	/* Cfa/collect.scm 193 */
																													if (BGl_pairzd2optimzf3z21zzcfa_pairz00())
																														{	/* Cfa/collect.scm 224 */
																															BGl_za2usedzd2allocza2zd2zzcfa_collectz00
																																=
																																MAKE_YOUNG_PAIR(
																																((obj_t) (
																																		(BgL_appz00_bglt)
																																		BgL_nodez00_5111)),
																																BGl_za2usedzd2allocza2zd2zzcfa_collectz00);
																															{	/* Cfa/collect.scm 226 */
																																BgL_prezd2conszd2appz00_bglt
																																	BgL_wide1223z00_5253;
																																BgL_wide1223z00_5253
																																	=
																																	(
																																	(BgL_prezd2conszd2appz00_bglt)
																																	BOBJECT
																																	(GC_MALLOC
																																		(sizeof
																																			(struct
																																				BgL_prezd2conszd2appz00_bgl))));
																																{	/* Cfa/collect.scm 226 */
																																	obj_t
																																		BgL_auxz00_6064;
																																	BgL_objectz00_bglt
																																		BgL_tmpz00_6060;
																																	BgL_auxz00_6064
																																		=
																																		((obj_t)
																																		BgL_wide1223z00_5253);
																																	BgL_tmpz00_6060
																																		=
																																		(
																																		(BgL_objectz00_bglt)
																																		((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111)));
																																	BGL_OBJECT_WIDENING_SET
																																		(BgL_tmpz00_6060,
																																		BgL_auxz00_6064);
																																}
																																((BgL_objectz00_bglt) ((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111)));
																																{	/* Cfa/collect.scm 226 */
																																	long
																																		BgL_arg1885z00_5254;
																																	BgL_arg1885z00_5254
																																		=
																																		BGL_CLASS_NUM
																																		(BGl_prezd2conszd2appz00zzcfa_info2z00);
																																	BGL_OBJECT_CLASS_NUM_SET
																																		(((BgL_objectz00_bglt) ((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111))), BgL_arg1885z00_5254);
																																}
																																((BgL_appz00_bglt) ((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111)));
																															}
																															{
																																BgL_prezd2conszd2appz00_bglt
																																	BgL_auxz00_6078;
																																{
																																	obj_t
																																		BgL_auxz00_6079;
																																	{	/* Cfa/collect.scm 226 */
																																		BgL_objectz00_bglt
																																			BgL_tmpz00_6080;
																																		BgL_tmpz00_6080
																																			=
																																			(
																																			(BgL_objectz00_bglt)
																																			((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111)));
																																		BgL_auxz00_6079
																																			=
																																			BGL_OBJECT_WIDENING
																																			(BgL_tmpz00_6080);
																																	}
																																	BgL_auxz00_6078
																																		=
																																		(
																																		(BgL_prezd2conszd2appz00_bglt)
																																		BgL_auxz00_6079);
																																}
																																((((BgL_prezd2conszd2appz00_bglt) COBJECT(BgL_auxz00_6078))->BgL_ownerz00) = ((BgL_variablez00_bglt) ((BgL_variablez00_bglt) BgL_ownerz00_5112)), BUNSPEC);
																															}
																															return
																																((obj_t)
																																((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111)));
																														}
																													else
																														{	/* Cfa/collect.scm 224 */
																															return BFALSE;
																														}
																												}
																											else
																												{	/* Cfa/collect.scm 193 */
																													if (
																														(BgL_casezd2valuezd2_5234
																															==
																															CNST_TABLE_REF
																															(11)))
																														{	/* Cfa/collect.scm 193 */
																															if (BGl_pairzd2optimzf3z21zzcfa_pairz00())
																																{	/* Cfa/collect.scm 228 */
																																	{	/* Cfa/collect.scm 229 */
																																		BgL_prezd2conszd2refzd2appzd2_bglt
																																			BgL_wide1227z00_5255;
																																		BgL_wide1227z00_5255
																																			=
																																			(
																																			(BgL_prezd2conszd2refzd2appzd2_bglt)
																																			BOBJECT
																																			(GC_MALLOC
																																				(sizeof
																																					(struct
																																						BgL_prezd2conszd2refzd2appzd2_bgl))));
																																		{	/* Cfa/collect.scm 229 */
																																			obj_t
																																				BgL_auxz00_6101;
																																			BgL_objectz00_bglt
																																				BgL_tmpz00_6097;
																																			BgL_auxz00_6101
																																				=
																																				((obj_t)
																																				BgL_wide1227z00_5255);
																																			BgL_tmpz00_6097
																																				=
																																				(
																																				(BgL_objectz00_bglt)
																																				((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111)));
																																			BGL_OBJECT_WIDENING_SET
																																				(BgL_tmpz00_6097,
																																				BgL_auxz00_6101);
																																		}
																																		((BgL_objectz00_bglt) ((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111)));
																																		{	/* Cfa/collect.scm 229 */
																																			long
																																				BgL_arg1888z00_5256;
																																			BgL_arg1888z00_5256
																																				=
																																				BGL_CLASS_NUM
																																				(BGl_prezd2conszd2refzd2appzd2zzcfa_info2z00);
																																			BGL_OBJECT_CLASS_NUM_SET
																																				(((BgL_objectz00_bglt) ((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111))), BgL_arg1888z00_5256);
																																		}
																																		((BgL_appz00_bglt) ((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111)));
																																	}
																																	{
																																		BgL_prezd2conszd2refzd2appzd2_bglt
																																			BgL_auxz00_6115;
																																		{
																																			obj_t
																																				BgL_auxz00_6116;
																																			{	/* Cfa/collect.scm 230 */
																																				BgL_objectz00_bglt
																																					BgL_tmpz00_6117;
																																				BgL_tmpz00_6117
																																					=
																																					(
																																					(BgL_objectz00_bglt)
																																					((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111)));
																																				BgL_auxz00_6116
																																					=
																																					BGL_OBJECT_WIDENING
																																					(BgL_tmpz00_6117);
																																			}
																																			BgL_auxz00_6115
																																				=
																																				(
																																				(BgL_prezd2conszd2refzd2appzd2_bglt)
																																				BgL_auxz00_6116);
																																		}
																																		((((BgL_prezd2conszd2refzd2appzd2_bglt) COBJECT(BgL_auxz00_6115))->BgL_getz00) = ((obj_t) BGl_carzd2envzd2zz__r4_pairs_and_lists_6_3z00), BUNSPEC);
																																	}
																																	return
																																		((obj_t)
																																		((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111)));
																																}
																															else
																																{	/* Cfa/collect.scm 228 */
																																	return BFALSE;
																																}
																														}
																													else
																														{	/* Cfa/collect.scm 193 */
																															if (
																																(BgL_casezd2valuezd2_5234
																																	==
																																	CNST_TABLE_REF
																																	(12)))
																																{	/* Cfa/collect.scm 193 */
																																	if (BGl_pairzd2optimzf3z21zzcfa_pairz00())
																																		{	/* Cfa/collect.scm 232 */
																																			{	/* Cfa/collect.scm 233 */
																																				BgL_prezd2conszd2refzd2appzd2_bglt
																																					BgL_wide1231z00_5257;
																																				BgL_wide1231z00_5257
																																					=
																																					(
																																					(BgL_prezd2conszd2refzd2appzd2_bglt)
																																					BOBJECT
																																					(GC_MALLOC
																																						(sizeof
																																							(struct
																																								BgL_prezd2conszd2refzd2appzd2_bgl))));
																																				{	/* Cfa/collect.scm 233 */
																																					obj_t
																																						BgL_auxz00_6137;
																																					BgL_objectz00_bglt
																																						BgL_tmpz00_6133;
																																					BgL_auxz00_6137
																																						=
																																						(
																																						(obj_t)
																																						BgL_wide1231z00_5257);
																																					BgL_tmpz00_6133
																																						=
																																						(
																																						(BgL_objectz00_bglt)
																																						((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111)));
																																					BGL_OBJECT_WIDENING_SET
																																						(BgL_tmpz00_6133,
																																						BgL_auxz00_6137);
																																				}
																																				((BgL_objectz00_bglt) ((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111)));
																																				{	/* Cfa/collect.scm 233 */
																																					long
																																						BgL_arg1891z00_5258;
																																					BgL_arg1891z00_5258
																																						=
																																						BGL_CLASS_NUM
																																						(BGl_prezd2conszd2refzd2appzd2zzcfa_info2z00);
																																					BGL_OBJECT_CLASS_NUM_SET
																																						(((BgL_objectz00_bglt) ((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111))), BgL_arg1891z00_5258);
																																				}
																																				((BgL_appz00_bglt) ((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111)));
																																			}
																																			{
																																				BgL_prezd2conszd2refzd2appzd2_bglt
																																					BgL_auxz00_6151;
																																				{
																																					obj_t
																																						BgL_auxz00_6152;
																																					{	/* Cfa/collect.scm 234 */
																																						BgL_objectz00_bglt
																																							BgL_tmpz00_6153;
																																						BgL_tmpz00_6153
																																							=
																																							(
																																							(BgL_objectz00_bglt)
																																							((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111)));
																																						BgL_auxz00_6152
																																							=
																																							BGL_OBJECT_WIDENING
																																							(BgL_tmpz00_6153);
																																					}
																																					BgL_auxz00_6151
																																						=
																																						(
																																						(BgL_prezd2conszd2refzd2appzd2_bglt)
																																						BgL_auxz00_6152);
																																				}
																																				((((BgL_prezd2conszd2refzd2appzd2_bglt) COBJECT(BgL_auxz00_6151))->BgL_getz00) = ((obj_t) BGl_cdrzd2envzd2zz__r4_pairs_and_lists_6_3z00), BUNSPEC);
																																			}
																																			return
																																				((obj_t)
																																				((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111)));
																																		}
																																	else
																																		{	/* Cfa/collect.scm 232 */
																																			return
																																				BFALSE;
																																		}
																																}
																															else
																																{	/* Cfa/collect.scm 193 */
																																	if (
																																		(BgL_casezd2valuezd2_5234
																																			==
																																			CNST_TABLE_REF
																																			(13)))
																																		{	/* Cfa/collect.scm 193 */
																																			if (BGl_pairzd2optimzf3z21zzcfa_pairz00())
																																				{	/* Cfa/collect.scm 236 */
																																					{	/* Cfa/collect.scm 237 */
																																						BgL_prezd2conszd2setz12zd2appzc0_bglt
																																							BgL_wide1235z00_5259;
																																						BgL_wide1235z00_5259
																																							=
																																							(
																																							(BgL_prezd2conszd2setz12zd2appzc0_bglt)
																																							BOBJECT
																																							(GC_MALLOC
																																								(sizeof
																																									(struct
																																										BgL_prezd2conszd2setz12zd2appzc0_bgl))));
																																						{	/* Cfa/collect.scm 237 */
																																							obj_t
																																								BgL_auxz00_6173;
																																							BgL_objectz00_bglt
																																								BgL_tmpz00_6169;
																																							BgL_auxz00_6173
																																								=
																																								(
																																								(obj_t)
																																								BgL_wide1235z00_5259);
																																							BgL_tmpz00_6169
																																								=
																																								(
																																								(BgL_objectz00_bglt)
																																								((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111)));
																																							BGL_OBJECT_WIDENING_SET
																																								(BgL_tmpz00_6169,
																																								BgL_auxz00_6173);
																																						}
																																						((BgL_objectz00_bglt) ((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111)));
																																						{	/* Cfa/collect.scm 237 */
																																							long
																																								BgL_arg1894z00_5260;
																																							BgL_arg1894z00_5260
																																								=
																																								BGL_CLASS_NUM
																																								(BGl_prezd2conszd2setz12zd2appzc0zzcfa_info2z00);
																																							BGL_OBJECT_CLASS_NUM_SET
																																								(
																																								((BgL_objectz00_bglt) ((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111))), BgL_arg1894z00_5260);
																																						}
																																						((BgL_appz00_bglt) ((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111)));
																																					}
																																					{
																																						BgL_prezd2conszd2setz12zd2appzc0_bglt
																																							BgL_auxz00_6187;
																																						{
																																							obj_t
																																								BgL_auxz00_6188;
																																							{	/* Cfa/collect.scm 238 */
																																								BgL_objectz00_bglt
																																									BgL_tmpz00_6189;
																																								BgL_tmpz00_6189
																																									=
																																									(
																																									(BgL_objectz00_bglt)
																																									((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111)));
																																								BgL_auxz00_6188
																																									=
																																									BGL_OBJECT_WIDENING
																																									(BgL_tmpz00_6189);
																																							}
																																							BgL_auxz00_6187
																																								=
																																								(
																																								(BgL_prezd2conszd2setz12zd2appzc0_bglt)
																																								BgL_auxz00_6188);
																																						}
																																						((((BgL_prezd2conszd2setz12zd2appzc0_bglt) COBJECT(BgL_auxz00_6187))->BgL_getz00) = ((obj_t) BGl_carzd2envzd2zz__r4_pairs_and_lists_6_3z00), BUNSPEC);
																																					}
																																					return
																																						(
																																						(obj_t)
																																						((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111)));
																																				}
																																			else
																																				{	/* Cfa/collect.scm 236 */
																																					return
																																						BFALSE;
																																				}
																																		}
																																	else
																																		{	/* Cfa/collect.scm 193 */
																																			if (
																																				(BgL_casezd2valuezd2_5234
																																					==
																																					CNST_TABLE_REF
																																					(14)))
																																				{	/* Cfa/collect.scm 193 */
																																					if (BGl_pairzd2optimzf3z21zzcfa_pairz00())
																																						{	/* Cfa/collect.scm 240 */
																																							{	/* Cfa/collect.scm 241 */
																																								BgL_prezd2conszd2setz12zd2appzc0_bglt
																																									BgL_wide1239z00_5261;
																																								BgL_wide1239z00_5261
																																									=
																																									(
																																									(BgL_prezd2conszd2setz12zd2appzc0_bglt)
																																									BOBJECT
																																									(GC_MALLOC
																																										(sizeof
																																											(struct
																																												BgL_prezd2conszd2setz12zd2appzc0_bgl))));
																																								{	/* Cfa/collect.scm 241 */
																																									obj_t
																																										BgL_auxz00_6209;
																																									BgL_objectz00_bglt
																																										BgL_tmpz00_6205;
																																									BgL_auxz00_6209
																																										=
																																										(
																																										(obj_t)
																																										BgL_wide1239z00_5261);
																																									BgL_tmpz00_6205
																																										=
																																										(
																																										(BgL_objectz00_bglt)
																																										((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111)));
																																									BGL_OBJECT_WIDENING_SET
																																										(BgL_tmpz00_6205,
																																										BgL_auxz00_6209);
																																								}
																																								((BgL_objectz00_bglt) ((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111)));
																																								{	/* Cfa/collect.scm 241 */
																																									long
																																										BgL_arg1897z00_5262;
																																									BgL_arg1897z00_5262
																																										=
																																										BGL_CLASS_NUM
																																										(BGl_prezd2conszd2setz12zd2appzc0zzcfa_info2z00);
																																									BGL_OBJECT_CLASS_NUM_SET
																																										(
																																										((BgL_objectz00_bglt) ((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111))), BgL_arg1897z00_5262);
																																								}
																																								((BgL_appz00_bglt) ((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111)));
																																							}
																																							{
																																								BgL_prezd2conszd2setz12zd2appzc0_bglt
																																									BgL_auxz00_6223;
																																								{
																																									obj_t
																																										BgL_auxz00_6224;
																																									{	/* Cfa/collect.scm 242 */
																																										BgL_objectz00_bglt
																																											BgL_tmpz00_6225;
																																										BgL_tmpz00_6225
																																											=
																																											(
																																											(BgL_objectz00_bglt)
																																											((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111)));
																																										BgL_auxz00_6224
																																											=
																																											BGL_OBJECT_WIDENING
																																											(BgL_tmpz00_6225);
																																									}
																																									BgL_auxz00_6223
																																										=
																																										(
																																										(BgL_prezd2conszd2setz12zd2appzc0_bglt)
																																										BgL_auxz00_6224);
																																								}
																																								((((BgL_prezd2conszd2setz12zd2appzc0_bglt) COBJECT(BgL_auxz00_6223))->BgL_getz00) = ((obj_t) BGl_cdrzd2envzd2zz__r4_pairs_and_lists_6_3z00), BUNSPEC);
																																							}
																																							return
																																								(
																																								(obj_t)
																																								((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_5111)));
																																						}
																																					else
																																						{	/* Cfa/collect.scm 240 */
																																							return
																																								BFALSE;
																																						}
																																				}
																																			else
																																				{	/* Cfa/collect.scm 193 */
																																					return
																																						BUNSPEC;
																																				}
																																		}
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
							else
								{	/* Cfa/collect.scm 244 */
									bool_t BgL_test2263z00_6235;

									if (CBOOL
										(BGl_za2optimzd2cfazd2fixnumzd2arithmeticzf3za2z21zzengine_paramz00))
										{	/* Cfa/collect.scm 244 */
											BgL_test2263z00_6235 = ((bool_t) 1);
										}
									else
										{	/* Cfa/collect.scm 244 */
											BgL_test2263z00_6235 =
												CBOOL
												(BGl_za2optimzd2cfazd2flonumzd2arithmeticzf3za2z21zzengine_paramz00);
										}
									if (BgL_test2263z00_6235)
										{	/* Cfa/collect.scm 244 */
											if (CBOOL
												(BGl_arithmeticzd2operatorzf3z21zzcfa_specializa7eza7((
															(BgL_globalz00_bglt) BgL_vz00_5202))))
												{	/* Cfa/collect.scm 247 */
													obj_t BgL_typesz00_5263;

													BgL_typesz00_5263 =
														BGl_arithmeticzd2speczd2typesz00zzcfa_specializa7eza7
														(((BgL_globalz00_bglt) BgL_vz00_5202));
													if (PAIRP(BgL_typesz00_5263))
														{	/* Cfa/collect.scm 248 */
															{	/* Cfa/collect.scm 249 */
																BgL_prezd2arithmeticzd2appz00_bglt
																	BgL_wide1243z00_5264;
																BgL_wide1243z00_5264 =
																	((BgL_prezd2arithmeticzd2appz00_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_prezd2arithmeticzd2appz00_bgl))));
																{	/* Cfa/collect.scm 249 */
																	obj_t BgL_auxz00_6252;
																	BgL_objectz00_bglt BgL_tmpz00_6248;

																	BgL_auxz00_6252 =
																		((obj_t) BgL_wide1243z00_5264);
																	BgL_tmpz00_6248 =
																		((BgL_objectz00_bglt)
																		((BgL_appz00_bglt)
																			((BgL_appz00_bglt) BgL_nodez00_5111)));
																	BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6248,
																		BgL_auxz00_6252);
																}
																((BgL_objectz00_bglt)
																	((BgL_appz00_bglt)
																		((BgL_appz00_bglt) BgL_nodez00_5111)));
																{	/* Cfa/collect.scm 249 */
																	long BgL_arg1901z00_5265;

																	BgL_arg1901z00_5265 =
																		BGL_CLASS_NUM
																		(BGl_prezd2arithmeticzd2appz00zzcfa_info2z00);
																	BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																			((BgL_appz00_bglt) ((BgL_appz00_bglt)
																					BgL_nodez00_5111))),
																		BgL_arg1901z00_5265);
																}
																((BgL_appz00_bglt)
																	((BgL_appz00_bglt)
																		((BgL_appz00_bglt) BgL_nodez00_5111)));
															}
															{
																BgL_prezd2arithmeticzd2appz00_bglt
																	BgL_auxz00_6266;
																{
																	obj_t BgL_auxz00_6267;

																	{	/* Cfa/collect.scm 250 */
																		BgL_objectz00_bglt BgL_tmpz00_6268;

																		BgL_tmpz00_6268 =
																			((BgL_objectz00_bglt)
																			((BgL_appz00_bglt)
																				((BgL_appz00_bglt) BgL_nodez00_5111)));
																		BgL_auxz00_6267 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_6268);
																	}
																	BgL_auxz00_6266 =
																		((BgL_prezd2arithmeticzd2appz00_bglt)
																		BgL_auxz00_6267);
																}
																((((BgL_prezd2arithmeticzd2appz00_bglt)
																			COBJECT(BgL_auxz00_6266))->
																		BgL_speczd2typeszd2) =
																	((obj_t) BgL_typesz00_5263), BUNSPEC);
															}
															return
																((obj_t)
																((BgL_appz00_bglt)
																	((BgL_appz00_bglt) BgL_nodez00_5111)));
														}
													else
														{	/* Cfa/collect.scm 248 */
															return BFALSE;
														}
												}
											else
												{	/* Cfa/collect.scm 246 */
													return BFALSE;
												}
										}
									else
										{	/* Cfa/collect.scm 244 */
											return BFALSE;
										}
								}
						}
					else
						{	/* Cfa/collect.scm 186 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &node-collect!-sync1662 */
	obj_t BGl_z62nodezd2collectz12zd2sync1662z70zzcfa_collectz00(obj_t
		BgL_envz00_5113, obj_t BgL_nodez00_5114, obj_t BgL_ownerz00_5115)
	{
		{	/* Cfa/collect.scm 173 */
			{	/* Cfa/collect.scm 174 */
				BgL_nodez00_bglt BgL_arg1851z00_5267;

				BgL_arg1851z00_5267 =
					(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_5114)))->BgL_mutexz00);
				BGl_nodezd2collectz12zc0zzcfa_collectz00(BgL_arg1851z00_5267,
					((BgL_variablez00_bglt) BgL_ownerz00_5115));
			}
			{	/* Cfa/collect.scm 175 */
				BgL_nodez00_bglt BgL_arg1852z00_5268;

				BgL_arg1852z00_5268 =
					(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_5114)))->BgL_prelockz00);
				BGl_nodezd2collectz12zc0zzcfa_collectz00(BgL_arg1852z00_5268,
					((BgL_variablez00_bglt) BgL_ownerz00_5115));
			}
			{	/* Cfa/collect.scm 176 */
				BgL_nodez00_bglt BgL_arg1853z00_5269;

				BgL_arg1853z00_5269 =
					(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_5114)))->BgL_bodyz00);
				return
					BGl_nodezd2collectz12zc0zzcfa_collectz00(BgL_arg1853z00_5269,
					((BgL_variablez00_bglt) BgL_ownerz00_5115));
			}
		}

	}



/* &node-collect!-sequen1660 */
	obj_t BGl_z62nodezd2collectz12zd2sequen1660z70zzcfa_collectz00(obj_t
		BgL_envz00_5116, obj_t BgL_nodez00_5117, obj_t BgL_ownerz00_5118)
	{
		{	/* Cfa/collect.scm 167 */
			return
				BBOOL(BGl_nodezd2collectza2z12z62zzcfa_collectz00(
					(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_5117)))->BgL_nodesz00),
					BgL_ownerz00_5118));
		}

	}



/* &node-collect!-var1658 */
	obj_t BGl_z62nodezd2collectz12zd2var1658z70zzcfa_collectz00(obj_t
		BgL_envz00_5119, obj_t BgL_nodez00_5120, obj_t BgL_ownerz00_5121)
	{
		{	/* Cfa/collect.scm 150 */
			{	/* Cfa/collect.scm 151 */
				BgL_variablez00_bglt BgL_vz00_5272;

				BgL_vz00_5272 =
					(((BgL_varz00_bglt) COBJECT(
							((BgL_varz00_bglt) BgL_nodez00_5120)))->BgL_variablez00);
				{	/* Cfa/collect.scm 152 */
					bool_t BgL_test2267z00_6296;

					{	/* Cfa/collect.scm 152 */
						bool_t BgL_test2268z00_6297;

						{	/* Cfa/collect.scm 152 */
							obj_t BgL_classz00_5273;

							BgL_classz00_5273 = BGl_globalz00zzast_varz00;
							{	/* Cfa/collect.scm 152 */
								BgL_objectz00_bglt BgL_arg1807z00_5274;

								{	/* Cfa/collect.scm 152 */
									obj_t BgL_tmpz00_6298;

									BgL_tmpz00_6298 =
										((obj_t) ((BgL_objectz00_bglt) BgL_vz00_5272));
									BgL_arg1807z00_5274 = (BgL_objectz00_bglt) (BgL_tmpz00_6298);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cfa/collect.scm 152 */
										long BgL_idxz00_5275;

										BgL_idxz00_5275 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5274);
										BgL_test2268z00_6297 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_5275 + 2L)) == BgL_classz00_5273);
									}
								else
									{	/* Cfa/collect.scm 152 */
										bool_t BgL_res2154z00_5278;

										{	/* Cfa/collect.scm 152 */
											obj_t BgL_oclassz00_5279;

											{	/* Cfa/collect.scm 152 */
												obj_t BgL_arg1815z00_5280;
												long BgL_arg1816z00_5281;

												BgL_arg1815z00_5280 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cfa/collect.scm 152 */
													long BgL_arg1817z00_5282;

													BgL_arg1817z00_5282 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5274);
													BgL_arg1816z00_5281 =
														(BgL_arg1817z00_5282 - OBJECT_TYPE);
												}
												BgL_oclassz00_5279 =
													VECTOR_REF(BgL_arg1815z00_5280, BgL_arg1816z00_5281);
											}
											{	/* Cfa/collect.scm 152 */
												bool_t BgL__ortest_1115z00_5283;

												BgL__ortest_1115z00_5283 =
													(BgL_classz00_5273 == BgL_oclassz00_5279);
												if (BgL__ortest_1115z00_5283)
													{	/* Cfa/collect.scm 152 */
														BgL_res2154z00_5278 = BgL__ortest_1115z00_5283;
													}
												else
													{	/* Cfa/collect.scm 152 */
														long BgL_odepthz00_5284;

														{	/* Cfa/collect.scm 152 */
															obj_t BgL_arg1804z00_5285;

															BgL_arg1804z00_5285 = (BgL_oclassz00_5279);
															BgL_odepthz00_5284 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_5285);
														}
														if ((2L < BgL_odepthz00_5284))
															{	/* Cfa/collect.scm 152 */
																obj_t BgL_arg1802z00_5286;

																{	/* Cfa/collect.scm 152 */
																	obj_t BgL_arg1803z00_5287;

																	BgL_arg1803z00_5287 = (BgL_oclassz00_5279);
																	BgL_arg1802z00_5286 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5287,
																		2L);
																}
																BgL_res2154z00_5278 =
																	(BgL_arg1802z00_5286 == BgL_classz00_5273);
															}
														else
															{	/* Cfa/collect.scm 152 */
																BgL_res2154z00_5278 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2268z00_6297 = BgL_res2154z00_5278;
									}
							}
						}
						if (BgL_test2268z00_6297)
							{	/* Cfa/collect.scm 153 */
								bool_t BgL_test2272z00_6321;

								{	/* Cfa/collect.scm 153 */
									obj_t BgL_arg1849z00_5288;

									BgL_arg1849z00_5288 =
										(((BgL_globalz00_bglt) COBJECT(
												((BgL_globalz00_bglt) BgL_vz00_5272)))->BgL_modulez00);
									BgL_test2272z00_6321 =
										(BgL_arg1849z00_5288 ==
										BGl_za2moduleza2z00zzmodule_modulez00);
								}
								if (BgL_test2272z00_6321)
									{	/* Cfa/collect.scm 154 */
										bool_t BgL_test2273z00_6325;

										{	/* Cfa/collect.scm 154 */
											BgL_valuez00_bglt BgL_arg1848z00_5289;

											BgL_arg1848z00_5289 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_globalz00_bglt) BgL_vz00_5272))))->
												BgL_valuez00);
											{	/* Cfa/collect.scm 154 */
												obj_t BgL_classz00_5290;

												BgL_classz00_5290 = BGl_scnstz00zzast_varz00;
												{	/* Cfa/collect.scm 154 */
													BgL_objectz00_bglt BgL_arg1807z00_5291;

													{	/* Cfa/collect.scm 154 */
														obj_t BgL_tmpz00_6329;

														BgL_tmpz00_6329 =
															((obj_t)
															((BgL_objectz00_bglt) BgL_arg1848z00_5289));
														BgL_arg1807z00_5291 =
															(BgL_objectz00_bglt) (BgL_tmpz00_6329);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Cfa/collect.scm 154 */
															long BgL_idxz00_5292;

															BgL_idxz00_5292 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5291);
															BgL_test2273z00_6325 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_5292 + 2L)) == BgL_classz00_5290);
														}
													else
														{	/* Cfa/collect.scm 154 */
															bool_t BgL_res2155z00_5295;

															{	/* Cfa/collect.scm 154 */
																obj_t BgL_oclassz00_5296;

																{	/* Cfa/collect.scm 154 */
																	obj_t BgL_arg1815z00_5297;
																	long BgL_arg1816z00_5298;

																	BgL_arg1815z00_5297 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Cfa/collect.scm 154 */
																		long BgL_arg1817z00_5299;

																		BgL_arg1817z00_5299 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5291);
																		BgL_arg1816z00_5298 =
																			(BgL_arg1817z00_5299 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_5296 =
																		VECTOR_REF(BgL_arg1815z00_5297,
																		BgL_arg1816z00_5298);
																}
																{	/* Cfa/collect.scm 154 */
																	bool_t BgL__ortest_1115z00_5300;

																	BgL__ortest_1115z00_5300 =
																		(BgL_classz00_5290 == BgL_oclassz00_5296);
																	if (BgL__ortest_1115z00_5300)
																		{	/* Cfa/collect.scm 154 */
																			BgL_res2155z00_5295 =
																				BgL__ortest_1115z00_5300;
																		}
																	else
																		{	/* Cfa/collect.scm 154 */
																			long BgL_odepthz00_5301;

																			{	/* Cfa/collect.scm 154 */
																				obj_t BgL_arg1804z00_5302;

																				BgL_arg1804z00_5302 =
																					(BgL_oclassz00_5296);
																				BgL_odepthz00_5301 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_5302);
																			}
																			if ((2L < BgL_odepthz00_5301))
																				{	/* Cfa/collect.scm 154 */
																					obj_t BgL_arg1802z00_5303;

																					{	/* Cfa/collect.scm 154 */
																						obj_t BgL_arg1803z00_5304;

																						BgL_arg1803z00_5304 =
																							(BgL_oclassz00_5296);
																						BgL_arg1802z00_5303 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_5304, 2L);
																					}
																					BgL_res2155z00_5295 =
																						(BgL_arg1802z00_5303 ==
																						BgL_classz00_5290);
																				}
																			else
																				{	/* Cfa/collect.scm 154 */
																					BgL_res2155z00_5295 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2273z00_6325 = BgL_res2155z00_5295;
														}
												}
											}
										}
										if (BgL_test2273z00_6325)
											{	/* Cfa/collect.scm 155 */
												bool_t BgL_test2277z00_6352;

												{	/* Cfa/collect.scm 155 */
													obj_t BgL_arg1846z00_5305;

													BgL_arg1846z00_5305 =
														(((BgL_scnstz00_bglt) COBJECT(
																((BgL_scnstz00_bglt)
																	(((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					((BgL_globalz00_bglt)
																						BgL_vz00_5272))))->
																		BgL_valuez00))))->BgL_nodez00);
													BgL_test2277z00_6352 =
														CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
														(BgL_arg1846z00_5305,
															BGl_za2usedzd2allocza2zd2zzcfa_collectz00));
												}
												if (BgL_test2277z00_6352)
													{	/* Cfa/collect.scm 155 */
														BgL_test2267z00_6296 = ((bool_t) 0);
													}
												else
													{	/* Cfa/collect.scm 155 */
														BgL_test2267z00_6296 = ((bool_t) 1);
													}
											}
										else
											{	/* Cfa/collect.scm 154 */
												BgL_test2267z00_6296 = ((bool_t) 0);
											}
									}
								else
									{	/* Cfa/collect.scm 153 */
										BgL_test2267z00_6296 = ((bool_t) 0);
									}
							}
						else
							{	/* Cfa/collect.scm 152 */
								BgL_test2267z00_6296 = ((bool_t) 0);
							}
					}
					if (BgL_test2267z00_6296)
						{	/* Cfa/collect.scm 152 */
							{	/* Cfa/collect.scm 161 */
								obj_t BgL_arg1844z00_5306;

								BgL_arg1844z00_5306 =
									(((BgL_scnstz00_bglt) COBJECT(
											((BgL_scnstz00_bglt)
												(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_globalz00_bglt) BgL_vz00_5272))))->
													BgL_valuez00))))->BgL_nodez00);
								BGl_nodezd2collectz12zc0zzcfa_collectz00(((BgL_nodez00_bglt)
										BgL_arg1844z00_5306),
									((BgL_variablez00_bglt) BgL_ownerz00_5121));
							}
						}
					else
						{	/* Cfa/collect.scm 152 */
							BFALSE;
						}
				}
			}
			return BUNSPEC;
		}

	}



/* &node-collect!-kwote1656 */
	obj_t BGl_z62nodezd2collectz12zd2kwote1656z70zzcfa_collectz00(obj_t
		BgL_envz00_5122, obj_t BgL_nodez00_5123, obj_t BgL_ownerz00_5124)
	{
		{	/* Cfa/collect.scm 71 */
			{
				obj_t BgL_valuez00_5328;
				obj_t BgL_valuez00_5315;
				obj_t BgL_vectorz00_5311;

				{	/* Cfa/collect.scm 137 */
					obj_t BgL_valuez00_5338;

					BgL_valuez00_5338 =
						(((BgL_kwotez00_bglt) COBJECT(
								((BgL_kwotez00_bglt) BgL_nodez00_5123)))->BgL_valuez00);
					{	/* Cfa/collect.scm 139 */
						bool_t BgL_test2278z00_6370;

						if (VECTORP(BgL_valuez00_5338))
							{	/* Cfa/collect.scm 139 */
								BgL_test2278z00_6370 =
									BGl_vectorzd2optimzf3z21zzcfa_vectorz00();
							}
						else
							{	/* Cfa/collect.scm 139 */
								BgL_test2278z00_6370 = ((bool_t) 0);
							}
						if (BgL_test2278z00_6370)
							{	/* Cfa/collect.scm 139 */
								BgL_valuez00_5315 = BgL_valuez00_5338;
								{	/* Cfa/collect.scm 101 */
									int BgL_warningz00_5316;

									{	/* Cfa/collect.scm 101 */
										int BgL_wanz00_5317;

										BgL_wanz00_5317 = BGl_bigloozd2warningzd2zz__paramz00();
										BGl_bigloozd2warningzd2setz12z12zz__paramz00((int) (0L));
										BgL_warningz00_5316 = BgL_wanz00_5317;
									}
									{	/* Cfa/collect.scm 101 */
										obj_t BgL_backendz00_5318;

										BgL_backendz00_5318 =
											BGl_thezd2backendzd2zzbackend_backendz00();
										{	/* Cfa/collect.scm 104 */
											bool_t BgL_pragmazf3zf3_5319;

											{	/* Cfa/collect.scm 105 */
												bool_t BgL_tgtz00_5320;

												BgL_tgtz00_5320 =
													(((BgL_backendz00_bglt) COBJECT(
															((BgL_backendz00_bglt) BgL_backendz00_5318)))->
													BgL_pragmazd2supportzd2);
												((((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt)
																	BgL_backendz00_5318)))->
														BgL_pragmazd2supportzd2) =
													((bool_t) ((bool_t) 1)), BUNSPEC);
												BgL_pragmazf3zf3_5319 = BgL_tgtz00_5320;
											}
											{	/* Cfa/collect.scm 105 */
												BgL_nodez00_bglt BgL_dummyz00_5321;

												{	/* Cfa/collect.scm 109 */
													obj_t BgL_arg1767z00_5322;

													{	/* Cfa/collect.scm 109 */
														obj_t BgL_arg1770z00_5323;

														{	/* Cfa/collect.scm 109 */
															obj_t BgL_arg1773z00_5324;

															{	/* Cfa/collect.scm 110 */
																obj_t BgL_arg1775z00_5325;

																{	/* Cfa/collect.scm 110 */
																	bool_t BgL_test2280z00_6382;

																	BgL_vectorz00_5311 = BgL_valuez00_5315;
																	if ((VECTOR_LENGTH(BgL_vectorz00_5311) == 0L))
																		{	/* Cfa/collect.scm 83 */
																			BgL_test2280z00_6382 = ((bool_t) 0);
																		}
																	else
																		{	/* Cfa/collect.scm 85 */
																			obj_t BgL_atypez00_5312;

																			BgL_atypez00_5312 =
																				BGl_getzd2atypeze70z35zzcfa_collectz00
																				(VECTOR_REF(BgL_vectorz00_5311, 0L));
																			{
																				long BgL_iz00_5314;

																				BgL_iz00_5314 = 1L;
																			BgL_loopz00_5313:
																				if (CBOOL(BgL_atypez00_5312))
																					{	/* Cfa/collect.scm 88 */
																						if (
																							(BgL_iz00_5314 ==
																								VECTOR_LENGTH
																								(BgL_vectorz00_5311)))
																							{	/* Cfa/collect.scm 90 */
																								BgL_test2280z00_6382 =
																									((bool_t) 1);
																							}
																						else
																							{	/* Cfa/collect.scm 90 */
																								if (
																									(BGl_getzd2atypeze70z35zzcfa_collectz00
																										(VECTOR_REF
																											(BgL_vectorz00_5311,
																												BgL_iz00_5314)) ==
																										BgL_atypez00_5312))
																									{
																										long BgL_iz00_6397;

																										BgL_iz00_6397 =
																											(BgL_iz00_5314 + 1L);
																										BgL_iz00_5314 =
																											BgL_iz00_6397;
																										goto BgL_loopz00_5313;
																									}
																								else
																									{	/* Cfa/collect.scm 92 */
																										BgL_test2280z00_6382 =
																											((bool_t) 0);
																									}
																							}
																					}
																				else
																					{	/* Cfa/collect.scm 88 */
																						BgL_test2280z00_6382 = ((bool_t) 0);
																					}
																			}
																		}
																	if (BgL_test2280z00_6382)
																		{	/* Cfa/collect.scm 110 */
																			BgL_arg1775z00_5325 =
																				VECTOR_REF(BgL_valuez00_5315, 0L);
																		}
																	else
																		{	/* Cfa/collect.scm 110 */
																			BgL_arg1775z00_5325 = CNST_TABLE_REF(15);
																		}
																}
																BgL_arg1773z00_5324 =
																	MAKE_YOUNG_PAIR(BgL_arg1775z00_5325, BNIL);
															}
															BgL_arg1770z00_5323 =
																MAKE_YOUNG_PAIR(BINT(VECTOR_LENGTH
																	(BgL_valuez00_5315)), BgL_arg1773z00_5324);
														}
														BgL_arg1767z00_5322 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
															BgL_arg1770z00_5323);
													}
													BgL_dummyz00_5321 =
														BGl_topzd2levelzd2sexpzd2ze3nodez31zzast_sexpz00
														(BgL_arg1767z00_5322, BFALSE);
												}
												{	/* Cfa/collect.scm 108 */

													((((BgL_backendz00_bglt) COBJECT(
																	((BgL_backendz00_bglt)
																		BgL_backendz00_5318)))->
															BgL_pragmazd2supportzd2) =
														((bool_t) BgL_pragmazf3zf3_5319), BUNSPEC);
													BGl_bigloozd2warningzd2setz12z12zz__paramz00
														(BgL_warningz00_5316);
													{	/* Cfa/collect.scm 116 */
														BgL_kwotezf2nodezf2_bglt BgL_wide1176z00_5326;

														BgL_wide1176z00_5326 =
															((BgL_kwotezf2nodezf2_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_kwotezf2nodezf2_bgl))));
														{	/* Cfa/collect.scm 116 */
															obj_t BgL_auxz00_6416;
															BgL_objectz00_bglt BgL_tmpz00_6412;

															BgL_auxz00_6416 = ((obj_t) BgL_wide1176z00_5326);
															BgL_tmpz00_6412 =
																((BgL_objectz00_bglt)
																((BgL_kwotez00_bglt)
																	((BgL_kwotez00_bglt) BgL_nodez00_5123)));
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6412,
																BgL_auxz00_6416);
														}
														((BgL_objectz00_bglt)
															((BgL_kwotez00_bglt)
																((BgL_kwotez00_bglt) BgL_nodez00_5123)));
														{	/* Cfa/collect.scm 116 */
															long BgL_arg1765z00_5327;

															BgL_arg1765z00_5327 =
																BGL_CLASS_NUM(BGl_kwotezf2nodezf2zzcfa_infoz00);
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt)
																	((BgL_kwotez00_bglt)
																		((BgL_kwotez00_bglt) BgL_nodez00_5123))),
																BgL_arg1765z00_5327);
														}
														((BgL_kwotez00_bglt)
															((BgL_kwotez00_bglt)
																((BgL_kwotez00_bglt) BgL_nodez00_5123)));
													}
													{
														BgL_kwotezf2nodezf2_bglt BgL_auxz00_6430;

														{
															obj_t BgL_auxz00_6431;

															{	/* Cfa/collect.scm 116 */
																BgL_objectz00_bglt BgL_tmpz00_6432;

																BgL_tmpz00_6432 =
																	((BgL_objectz00_bglt)
																	((BgL_kwotez00_bglt)
																		((BgL_kwotez00_bglt) BgL_nodez00_5123)));
																BgL_auxz00_6431 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_6432);
															}
															BgL_auxz00_6430 =
																((BgL_kwotezf2nodezf2_bglt) BgL_auxz00_6431);
														}
														((((BgL_kwotezf2nodezf2_bglt)
																	COBJECT(BgL_auxz00_6430))->BgL_nodez00) =
															((BgL_nodez00_bglt) BgL_dummyz00_5321), BUNSPEC);
													}
													((BgL_kwotez00_bglt)
														((BgL_kwotez00_bglt) BgL_nodez00_5123));
													return
														BGl_nodezd2collectz12zc0zzcfa_collectz00
														(BgL_dummyz00_5321,
														((BgL_variablez00_bglt) BgL_ownerz00_5124));
												}
											}
										}
									}
								}
							}
						else
							{	/* Cfa/collect.scm 142 */
								bool_t BgL_test2285z00_6443;

								if (PAIRP(BgL_valuez00_5338))
									{	/* Cfa/collect.scm 142 */
										if (BGl_pairzd2optimzf3z21zzcfa_pairz00())
											{	/* Cfa/collect.scm 142 */
												if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
													(BgL_valuez00_5338))
													{	/* Cfa/collect.scm 143 */
														long BgL_arg1735z00_5339;
														obj_t BgL_arg1736z00_5340;

														BgL_arg1735z00_5339 =
															bgl_list_length(BgL_valuez00_5338);
														BgL_arg1736z00_5340 =
															BGl_pairzd2optimzd2quotezd2maxlenzd2zzcfa_pairz00
															();
														BgL_test2285z00_6443 =
															(BgL_arg1735z00_5339 <
															(long) CINT(BgL_arg1736z00_5340));
													}
												else
													{	/* Cfa/collect.scm 142 */
														BgL_test2285z00_6443 = ((bool_t) 0);
													}
											}
										else
											{	/* Cfa/collect.scm 142 */
												BgL_test2285z00_6443 = ((bool_t) 0);
											}
									}
								else
									{	/* Cfa/collect.scm 142 */
										BgL_test2285z00_6443 = ((bool_t) 0);
									}
								if (BgL_test2285z00_6443)
									{	/* Cfa/collect.scm 142 */
										BgL_valuez00_5328 = BgL_valuez00_5338;
										{	/* Cfa/collect.scm 120 */
											int BgL_warningz00_5329;

											{	/* Cfa/collect.scm 120 */
												int BgL_wanz00_5330;

												BgL_wanz00_5330 = BGl_bigloozd2warningzd2zz__paramz00();
												BGl_bigloozd2warningzd2setz12z12zz__paramz00(
													(int) (0L));
												BgL_warningz00_5329 = BgL_wanz00_5330;
											}
											{	/* Cfa/collect.scm 120 */
												obj_t BgL_backendz00_5331;

												BgL_backendz00_5331 =
													BGl_thezd2backendzd2zzbackend_backendz00();
												{	/* Cfa/collect.scm 123 */
													bool_t BgL_pragmazf3zf3_5332;

													{	/* Cfa/collect.scm 124 */
														bool_t BgL_tgtz00_5333;

														BgL_tgtz00_5333 =
															(((BgL_backendz00_bglt) COBJECT(
																	((BgL_backendz00_bglt)
																		BgL_backendz00_5331)))->
															BgL_pragmazd2supportzd2);
														((((BgL_backendz00_bglt)
																	COBJECT(((BgL_backendz00_bglt)
																			BgL_backendz00_5331)))->
																BgL_pragmazd2supportzd2) =
															((bool_t) ((bool_t) 1)), BUNSPEC);
														BgL_pragmazf3zf3_5332 = BgL_tgtz00_5333;
													}
													{	/* Cfa/collect.scm 124 */
														obj_t BgL_expz00_5334;

														BgL_expz00_5334 =
															BGl_loopze70ze7zzcfa_collectz00
															(BgL_valuez00_5328);
														{	/* Cfa/collect.scm 127 */
															BgL_nodez00_bglt BgL_dummyz00_5335;

															BgL_dummyz00_5335 =
																BGl_topzd2levelzd2sexpzd2ze3nodez31zzast_sexpz00
																(BgL_expz00_5334, BFALSE);
															{	/* Cfa/collect.scm 131 */

																((((BgL_backendz00_bglt) COBJECT(
																				((BgL_backendz00_bglt)
																					BgL_backendz00_5331)))->
																		BgL_pragmazd2supportzd2) =
																	((bool_t) BgL_pragmazf3zf3_5332), BUNSPEC);
																BGl_bigloozd2warningzd2setz12z12zz__paramz00
																	(BgL_warningz00_5329);
																{	/* Cfa/collect.scm 134 */
																	BgL_kwotezf2nodezf2_bglt BgL_wide1180z00_5336;

																	BgL_wide1180z00_5336 =
																		((BgL_kwotezf2nodezf2_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_kwotezf2nodezf2_bgl))));
																	{	/* Cfa/collect.scm 134 */
																		obj_t BgL_auxz00_6472;
																		BgL_objectz00_bglt BgL_tmpz00_6468;

																		BgL_auxz00_6472 =
																			((obj_t) BgL_wide1180z00_5336);
																		BgL_tmpz00_6468 =
																			((BgL_objectz00_bglt)
																			((BgL_kwotez00_bglt)
																				((BgL_kwotez00_bglt)
																					BgL_nodez00_5123)));
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6468,
																			BgL_auxz00_6472);
																	}
																	((BgL_objectz00_bglt)
																		((BgL_kwotez00_bglt)
																			((BgL_kwotez00_bglt) BgL_nodez00_5123)));
																	{	/* Cfa/collect.scm 134 */
																		long BgL_arg1798z00_5337;

																		BgL_arg1798z00_5337 =
																			BGL_CLASS_NUM
																			(BGl_kwotezf2nodezf2zzcfa_infoz00);
																		BGL_OBJECT_CLASS_NUM_SET((
																				(BgL_objectz00_bglt) (
																					(BgL_kwotez00_bglt) (
																						(BgL_kwotez00_bglt)
																						BgL_nodez00_5123))),
																			BgL_arg1798z00_5337);
																	}
																	((BgL_kwotez00_bglt)
																		((BgL_kwotez00_bglt)
																			((BgL_kwotez00_bglt) BgL_nodez00_5123)));
																}
																{
																	BgL_kwotezf2nodezf2_bglt BgL_auxz00_6486;

																	{
																		obj_t BgL_auxz00_6487;

																		{	/* Cfa/collect.scm 134 */
																			BgL_objectz00_bglt BgL_tmpz00_6488;

																			BgL_tmpz00_6488 =
																				((BgL_objectz00_bglt)
																				((BgL_kwotez00_bglt)
																					((BgL_kwotez00_bglt)
																						BgL_nodez00_5123)));
																			BgL_auxz00_6487 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_6488);
																		}
																		BgL_auxz00_6486 =
																			((BgL_kwotezf2nodezf2_bglt)
																			BgL_auxz00_6487);
																	}
																	((((BgL_kwotezf2nodezf2_bglt)
																				COBJECT(BgL_auxz00_6486))->
																			BgL_nodez00) =
																		((BgL_nodez00_bglt) BgL_dummyz00_5335),
																		BUNSPEC);
																}
																((BgL_kwotez00_bglt)
																	((BgL_kwotez00_bglt) BgL_nodez00_5123));
																return
																	BGl_nodezd2collectz12zc0zzcfa_collectz00
																	(BgL_dummyz00_5335,
																	((BgL_variablez00_bglt) BgL_ownerz00_5124));
															}
														}
													}
												}
											}
										}
									}
								else
									{	/* Cfa/collect.scm 142 */
										return BFALSE;
									}
							}
					}
				}
			}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zzcfa_collectz00(obj_t BgL_vz00_3679)
	{
		{	/* Cfa/collect.scm 127 */
			if (NULLP(BgL_vz00_3679))
				{	/* Cfa/collect.scm 128 */
					return CNST_TABLE_REF(16);
				}
			else
				{	/* Cfa/collect.scm 130 */
					obj_t BgL_arg1805z00_3682;

					{	/* Cfa/collect.scm 130 */
						obj_t BgL_arg1806z00_3683;
						obj_t BgL_arg1808z00_3684;

						{	/* Cfa/collect.scm 130 */
							obj_t BgL_arg1812z00_3685;

							{	/* Cfa/collect.scm 130 */
								obj_t BgL_arg1820z00_3686;

								BgL_arg1820z00_3686 = CAR(((obj_t) BgL_vz00_3679));
								BgL_arg1812z00_3685 =
									MAKE_YOUNG_PAIR(BgL_arg1820z00_3686, BNIL);
							}
							BgL_arg1806z00_3683 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(17), BgL_arg1812z00_3685);
						}
						{	/* Cfa/collect.scm 130 */
							obj_t BgL_arg1822z00_3687;

							{	/* Cfa/collect.scm 130 */
								obj_t BgL_arg1823z00_3688;

								BgL_arg1823z00_3688 = CDR(((obj_t) BgL_vz00_3679));
								BgL_arg1822z00_3687 =
									BGl_loopze70ze7zzcfa_collectz00(BgL_arg1823z00_3688);
							}
							BgL_arg1808z00_3684 = MAKE_YOUNG_PAIR(BgL_arg1822z00_3687, BNIL);
						}
						BgL_arg1805z00_3682 =
							MAKE_YOUNG_PAIR(BgL_arg1806z00_3683, BgL_arg1808z00_3684);
					}
					return MAKE_YOUNG_PAIR(CNST_TABLE_REF(10), BgL_arg1805z00_3682);
				}
		}

	}



/* get-atype~0 */
	obj_t BGl_getzd2atypeze70z35zzcfa_collectz00(obj_t BgL_valuez00_3636)
	{
		{	/* Cfa/collect.scm 81 */
			if (INTEGERP(BgL_valuez00_3636))
				{	/* Cfa/collect.scm 76 */
					return CNST_TABLE_REF(18);
				}
			else
				{	/* Cfa/collect.scm 76 */
					if (CHARP(BgL_valuez00_3636))
						{	/* Cfa/collect.scm 77 */
							return CNST_TABLE_REF(19);
						}
					else
						{	/* Cfa/collect.scm 77 */
							if (BOOLEANP(BgL_valuez00_3636))
								{	/* Cfa/collect.scm 78 */
									return CNST_TABLE_REF(20);
								}
							else
								{	/* Cfa/collect.scm 78 */
									if (STRINGP(BgL_valuez00_3636))
										{	/* Cfa/collect.scm 79 */
											return CNST_TABLE_REF(21);
										}
									else
										{	/* Cfa/collect.scm 80 */
											bool_t BgL_test2294z00_6526;

											if (INTEGERP(BgL_valuez00_3636))
												{	/* Cfa/collect.scm 80 */
													BgL_test2294z00_6526 = ((bool_t) 1);
												}
											else
												{	/* Cfa/collect.scm 80 */
													BgL_test2294z00_6526 = REALP(BgL_valuez00_3636);
												}
											if (BgL_test2294z00_6526)
												{	/* Cfa/collect.scm 80 */
													return CNST_TABLE_REF(22);
												}
											else
												{	/* Cfa/collect.scm 80 */
													return BFALSE;
												}
										}
								}
						}
				}
		}

	}



/* &node-collect!-atom1654 */
	obj_t BGl_z62nodezd2collectz12zd2atom1654z70zzcfa_collectz00(obj_t
		BgL_envz00_5125, obj_t BgL_nodez00_5126, obj_t BgL_ownerz00_5127)
	{
		{	/* Cfa/collect.scm 65 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcfa_collectz00(void)
	{
		{	/* Cfa/collect.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2187z00zzcfa_collectz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2187z00zzcfa_collectz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2187z00zzcfa_collectz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2187z00zzcfa_collectz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2187z00zzcfa_collectz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string2187z00zzcfa_collectz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2187z00zzcfa_collectz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2187z00zzcfa_collectz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2187z00zzcfa_collectz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2187z00zzcfa_collectz00));
			BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string2187z00zzcfa_collectz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2187z00zzcfa_collectz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string2187z00zzcfa_collectz00));
			BGl_modulezd2initializa7ationz75zzcfa_infoz00(3788352L,
				BSTRING_TO_STRING(BGl_string2187z00zzcfa_collectz00));
			BGl_modulezd2initializa7ationz75zzcfa_info2z00(0L,
				BSTRING_TO_STRING(BGl_string2187z00zzcfa_collectz00));
			BGl_modulezd2initializa7ationz75zzcfa_info3z00(0L,
				BSTRING_TO_STRING(BGl_string2187z00zzcfa_collectz00));
			BGl_modulezd2initializa7ationz75zzcfa_arithmeticz00(185547682L,
				BSTRING_TO_STRING(BGl_string2187z00zzcfa_collectz00));
			BGl_modulezd2initializa7ationz75zzcfa_specializa7eza7(374743292L,
				BSTRING_TO_STRING(BGl_string2187z00zzcfa_collectz00));
			BGl_modulezd2initializa7ationz75zzcfa_procedurez00(227655313L,
				BSTRING_TO_STRING(BGl_string2187z00zzcfa_collectz00));
			BGl_modulezd2initializa7ationz75zzcfa_vectorz00(146804578L,
				BSTRING_TO_STRING(BGl_string2187z00zzcfa_collectz00));
			BGl_modulezd2initializa7ationz75zzcfa_pairz00(37668556L,
				BSTRING_TO_STRING(BGl_string2187z00zzcfa_collectz00));
			BGl_modulezd2initializa7ationz75zzcfa_structz00(306923863L,
				BSTRING_TO_STRING(BGl_string2187z00zzcfa_collectz00));
			BGl_modulezd2initializa7ationz75zzcfa_boxz00(370344659L,
				BSTRING_TO_STRING(BGl_string2187z00zzcfa_collectz00));
			return
				BGl_modulezd2initializa7ationz75zzcfa_closurez00(189402713L,
				BSTRING_TO_STRING(BGl_string2187z00zzcfa_collectz00));
		}

	}

#ifdef __cplusplus
}
#endif
