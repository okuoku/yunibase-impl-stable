/*===========================================================================*/
/*   (Cfa/pair.scm)                                                          */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cfa/pair.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CFA_PAIR_TYPE_DEFINITIONS
#define BGL_CFA_PAIR_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_approxz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		bool_t BgL_typezd2lockedzf3z21;
		obj_t BgL_allocsz00;
		bool_t BgL_topzf3zf3;
		long BgL_lostzd2stampzd2;
		obj_t BgL_dupz00;
		bool_t BgL_haszd2procedurezf3z21;
	}                *BgL_approxz00_bglt;

	typedef struct BgL_prezd2conszd2appz00_bgl
	{
		struct BgL_variablez00_bgl *BgL_ownerz00;
	}                          *BgL_prezd2conszd2appz00_bglt;

	typedef struct BgL_prezd2conszd2refzd2appzd2_bgl
	{
		obj_t BgL_getz00;
	}                                *BgL_prezd2conszd2refzd2appzd2_bglt;

	typedef struct BgL_prezd2conszd2setz12zd2appzc0_bgl
	{
		obj_t BgL_getz00;
	}                                   *BgL_prezd2conszd2setz12zd2appzc0_bglt;

	typedef struct BgL_conszd2appzd2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		obj_t BgL_approxesz00;
		long BgL_lostzd2stampzd2;
		struct BgL_variablez00_bgl *BgL_ownerz00;
		obj_t BgL_stackzd2stampzd2;
		bool_t BgL_seenzf3zf3;
	}                    *BgL_conszd2appzd2_bglt;

	typedef struct BgL_conszd2refzd2appz00_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		obj_t BgL_getz00;
	}                          *BgL_conszd2refzd2appz00_bglt;

	typedef struct BgL_conszd2setz12zd2appz12_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		obj_t BgL_getz00;
	}                             *BgL_conszd2setz12zd2appz12_bglt;


#endif													// BGL_CFA_PAIR_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t
		BGl_za2optimzd2cfazd2pairzd2quotezd2maxzd2lengthza2zd2zzengine_paramz00;
	static obj_t BGl_z62unpatchzd2pairzd2setz12z70zzcfa_pairz00(obj_t);
	static obj_t BGl_z62patchzd2pairzd2setz12z70zzcfa_pairz00(obj_t);
	static obj_t BGl_objectzd2initzd2zzcfa_pairz00(void);
	extern obj_t BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00(obj_t,
		BgL_approxz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_pairzd2optimzd2quotezd2maxlenzd2zzcfa_pairz00(void);
	static obj_t BGl_methodzd2initzd2zzcfa_pairz00(void);
	extern BgL_typez00_bglt
		BGl_getzd2bigloozd2typez00zztype_cachez00(BgL_typez00_bglt);
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2conszd2refzd2app1584za2zzcfa_pairz00(obj_t, obj_t);
	extern obj_t BGl_prezd2conszd2refzd2appzd2zzcfa_info2z00;
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	static obj_t BGl_z62nodezd2setupz12zd2prezd2cons1576za2zzcfa_pairz00(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2setupz12zd2prezd2cons1578za2zzcfa_pairz00(obj_t,
		obj_t);
	static obj_t BGl_z62loosezd2allocz12zd2conszd2ap1588za2zzcfa_pairz00(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2setupz12zd2prezd2cons1580za2zzcfa_pairz00(obj_t,
		obj_t);
	extern obj_t BGl_cfunz00zzast_varz00;
	extern BgL_approxz00_bglt
		BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_approxz00_bglt,
		BgL_approxz00_bglt);
	static obj_t BGl_gczd2rootszd2initz00zzcfa_pairz00(void);
	extern BgL_approxz00_bglt BGl_makezd2emptyzd2approxz00zzcfa_approxz00(void);
	BGL_EXPORTED_DECL obj_t BGl_unpatchzd2pairzd2setz12z12zzcfa_pairz00(void);
	extern obj_t BGl_prezd2conszd2setz12zd2appzc0zzcfa_info2z00;
	static obj_t BGl_z62pairzd2optimzd2quotezd2maxlenzb0zzcfa_pairz00(obj_t);
	extern obj_t BGl_conszd2setz12zd2appz12zzcfa_info2z00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2cfazd2stampza2zd2zzcfa_iteratez00;
	extern obj_t BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_approxz00_bglt);
	extern obj_t BGl_za2optimzd2cfazd2pairzf3za2zf3zzengine_paramz00;
	extern obj_t BGl_za2optimza2z00zzengine_paramz00;
	extern obj_t BGl_nodezd2setupza2z12z62zzcfa_setupz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzcfa_pairz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31766ze3ze5zzcfa_pairz00(obj_t, obj_t);
	extern obj_t BGl_conszd2appzd2zzcfa_info2z00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_EXPORTED_DECL bool_t BGl_pairzd2optimzf3z21zzcfa_pairz00(void);
	BGL_EXPORTED_DECL obj_t BGl_patchzd2pairzd2setz12z12zzcfa_pairz00(void);
	static obj_t BGl_toplevelzd2initzd2zzcfa_pairz00(void);
	static obj_t BGl_genericzd2initzd2zzcfa_pairz00(void);
	extern BgL_typez00_bglt BGl_getzd2defaultzd2typez00zztype_cachez00(void);
	extern obj_t BGl_conszd2refzd2appz00zzcfa_info2z00;
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern BgL_approxz00_bglt BGl_loosez12z12zzcfa_loosez00(BgL_approxz00_bglt,
		obj_t);
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2conszd2app1582z70zzcfa_pairz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62pairzd2optimzf3z43zzcfa_pairz00(obj_t);
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	extern BgL_approxz00_bglt
		BGl_makezd2typezd2alloczd2approxzd2zzcfa_approxz00(BgL_typez00_bglt,
		BgL_nodez00_bglt);
	extern obj_t BGl_za2pairza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcfa_pairz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_approxz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_setupz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_cfaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_iteratez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_loosez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info2z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_infoz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	static obj_t BGl_cnstzd2initzd2zzcfa_pairz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcfa_pairz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzcfa_pairz00(void);
	extern obj_t BGl_findzd2globalzf2modulez20zzast_envz00(obj_t, obj_t);
	extern BgL_approxz00_bglt
		BGl_makezd2typezd2approxz00zzcfa_approxz00(BgL_typez00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31823ze3ze5zzcfa_pairz00(obj_t, obj_t);
	extern obj_t BGl_za2unspecza2z00zztype_cachez00;
	extern obj_t BGl_prezd2conszd2appz00zzcfa_info2z00;
	extern BgL_approxz00_bglt BGl_cfaz12z12zzcfa_cfaz00(BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	extern obj_t BGl_approxzd2setzd2typez12z12zzcfa_approxz00(BgL_approxz00_bglt,
		BgL_typez00_bglt);
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2conszd2setz12zd2app1586zb0zzcfa_pairz00(obj_t, obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t __cnst[4];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2073z00zzcfa_pairz00,
		BgL_bgl_za762nodeza7d2setupza72087za7,
		BGl_z62nodezd2setupz12zd2prezd2cons1576za2zzcfa_pairz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2075z00zzcfa_pairz00,
		BgL_bgl_za762nodeza7d2setupza72088za7,
		BGl_z62nodezd2setupz12zd2prezd2cons1578za2zzcfa_pairz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2076z00zzcfa_pairz00,
		BgL_bgl_za762nodeza7d2setupza72089za7,
		BGl_z62nodezd2setupz12zd2prezd2cons1580za2zzcfa_pairz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2077z00zzcfa_pairz00,
		BgL_bgl_za762cfaza712za7d2cons2090za7,
		BGl_z62cfaz12zd2conszd2app1582z70zzcfa_pairz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2079z00zzcfa_pairz00,
		BgL_bgl_za762cfaza712za7d2cons2091za7,
		BGl_z62cfaz12zd2conszd2refzd2app1584za2zzcfa_pairz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2080z00zzcfa_pairz00,
		BgL_bgl_za762cfaza712za7d2cons2092za7,
		BGl_z62cfaz12zd2conszd2setz12zd2app1586zb0zzcfa_pairz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2081z00zzcfa_pairz00,
		BgL_bgl_za762looseza7d2alloc2093z00,
		BGl_z62loosezd2allocz12zd2conszd2ap1588za2zzcfa_pairz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_pairzd2optimzf3zd2envzf3zzcfa_pairz00,
		BgL_bgl_za762pairza7d2optimza72094za7,
		BGl_z62pairzd2optimzf3z43zzcfa_pairz00, 0L, BUNSPEC, 0);
	extern obj_t BGl_cfaz12zd2envzc0zzcfa_cfaz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_unpatchzd2pairzd2setz12zd2envzc0zzcfa_pairz00,
		BgL_bgl_za762unpatchza7d2pai2095z00,
		BGl_z62unpatchzd2pairzd2setz12z70zzcfa_pairz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_patchzd2pairzd2setz12zd2envzc0zzcfa_pairz00,
		BgL_bgl_za762patchza7d2pairza72096za7,
		BGl_z62patchzd2pairzd2setz12z70zzcfa_pairz00, 0L, BUNSPEC, 0);
	extern obj_t BGl_nodezd2setupz12zd2envz12zzcfa_setupz00;
	   
		 
		DEFINE_STRING(BGl_string2074z00zzcfa_pairz00,
		BgL_bgl_string2074za700za7za7c2097za7, "node-setup!", 11);
	      DEFINE_STRING(BGl_string2078z00zzcfa_pairz00,
		BgL_bgl_string2078za700za7za7c2098za7, "cfa!::approx", 12);
	      DEFINE_STRING(BGl_string2082z00zzcfa_pairz00,
		BgL_bgl_string2082za700za7za7c2099za7, "loose-alloc!", 12);
	      DEFINE_STRING(BGl_string2083z00zzcfa_pairz00,
		BgL_bgl_string2083za700za7za7c2100za7, "cfa_pair", 8);
	      DEFINE_STRING(BGl_string2084z00zzcfa_pairz00,
		BgL_bgl_string2084za700za7za7c2101za7,
		"all pair? $pair? ((@ cons __r4_pairs_and_lists_6_3) (@ set-car! __r4_pairs_and_lists_6_3) (@ set-cdr! __r4_pairs_and_lists_6_3) (@ $cons foreign) (@ $set-car! foreign) (@ $set-cdr! foreign)) ",
		191);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_pairzd2optimzd2quotezd2maxlenzd2envz00zzcfa_pairz00,
		BgL_bgl_za762pairza7d2optimza72102za7,
		BGl_z62pairzd2optimzd2quotezd2maxlenzb0zzcfa_pairz00, 0L, BUNSPEC, 0);
	extern obj_t BGl_loosezd2allocz12zd2envz12zzcfa_loosez00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcfa_pairz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcfa_pairz00(long
		BgL_checksumz00_4906, char *BgL_fromz00_4907)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcfa_pairz00))
				{
					BGl_requirezd2initializa7ationz75zzcfa_pairz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcfa_pairz00();
					BGl_libraryzd2moduleszd2initz00zzcfa_pairz00();
					BGl_cnstzd2initzd2zzcfa_pairz00();
					BGl_importedzd2moduleszd2initz00zzcfa_pairz00();
					BGl_methodzd2initzd2zzcfa_pairz00();
					return BGl_toplevelzd2initzd2zzcfa_pairz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcfa_pairz00(void)
	{
		{	/* Cfa/pair.scm 17 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cfa_pair");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cfa_pair");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cfa_pair");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cfa_pair");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L, "cfa_pair");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "cfa_pair");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cfa_pair");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cfa_pair");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcfa_pairz00(void)
	{
		{	/* Cfa/pair.scm 17 */
			{	/* Cfa/pair.scm 17 */
				obj_t BgL_cportz00_4760;

				{	/* Cfa/pair.scm 17 */
					obj_t BgL_stringz00_4767;

					BgL_stringz00_4767 = BGl_string2084z00zzcfa_pairz00;
					{	/* Cfa/pair.scm 17 */
						obj_t BgL_startz00_4768;

						BgL_startz00_4768 = BINT(0L);
						{	/* Cfa/pair.scm 17 */
							obj_t BgL_endz00_4769;

							BgL_endz00_4769 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_4767)));
							{	/* Cfa/pair.scm 17 */

								BgL_cportz00_4760 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_4767, BgL_startz00_4768, BgL_endz00_4769);
				}}}}
				{
					long BgL_iz00_4761;

					BgL_iz00_4761 = 3L;
				BgL_loopz00_4762:
					if ((BgL_iz00_4761 == -1L))
						{	/* Cfa/pair.scm 17 */
							return BUNSPEC;
						}
					else
						{	/* Cfa/pair.scm 17 */
							{	/* Cfa/pair.scm 17 */
								obj_t BgL_arg2086z00_4763;

								{	/* Cfa/pair.scm 17 */

									{	/* Cfa/pair.scm 17 */
										obj_t BgL_locationz00_4765;

										BgL_locationz00_4765 = BBOOL(((bool_t) 0));
										{	/* Cfa/pair.scm 17 */

											BgL_arg2086z00_4763 =
												BGl_readz00zz__readerz00(BgL_cportz00_4760,
												BgL_locationz00_4765);
										}
									}
								}
								{	/* Cfa/pair.scm 17 */
									int BgL_tmpz00_4934;

									BgL_tmpz00_4934 = (int) (BgL_iz00_4761);
									CNST_TABLE_SET(BgL_tmpz00_4934, BgL_arg2086z00_4763);
							}}
							{	/* Cfa/pair.scm 17 */
								int BgL_auxz00_4766;

								BgL_auxz00_4766 = (int) ((BgL_iz00_4761 - 1L));
								{
									long BgL_iz00_4939;

									BgL_iz00_4939 = (long) (BgL_auxz00_4766);
									BgL_iz00_4761 = BgL_iz00_4939;
									goto BgL_loopz00_4762;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcfa_pairz00(void)
	{
		{	/* Cfa/pair.scm 17 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcfa_pairz00(void)
	{
		{	/* Cfa/pair.scm 17 */
			return BUNSPEC;
		}

	}



/* pair-optim? */
	BGL_EXPORTED_DEF bool_t BGl_pairzd2optimzf3z21zzcfa_pairz00(void)
	{
		{	/* Cfa/pair.scm 44 */
			if (((long) CINT(BGl_za2optimza2z00zzengine_paramz00) >= 2L))
				{	/* Cfa/pair.scm 45 */
					return CBOOL(BGl_za2optimzd2cfazd2pairzf3za2zf3zzengine_paramz00);
				}
			else
				{	/* Cfa/pair.scm 45 */
					return ((bool_t) 0);
				}
		}

	}



/* &pair-optim? */
	obj_t BGl_z62pairzd2optimzf3z43zzcfa_pairz00(obj_t BgL_envz00_4720)
	{
		{	/* Cfa/pair.scm 44 */
			return BBOOL(BGl_pairzd2optimzf3z21zzcfa_pairz00());
		}

	}



/* pair-optim-quote-maxlen */
	BGL_EXPORTED_DEF obj_t BGl_pairzd2optimzd2quotezd2maxlenzd2zzcfa_pairz00(void)
	{
		{	/* Cfa/pair.scm 50 */
			return
				BGl_za2optimzd2cfazd2pairzd2quotezd2maxzd2lengthza2zd2zzengine_paramz00;
		}

	}



/* &pair-optim-quote-maxlen */
	obj_t BGl_z62pairzd2optimzd2quotezd2maxlenzb0zzcfa_pairz00(obj_t
		BgL_envz00_4721)
	{
		{	/* Cfa/pair.scm 50 */
			return BGl_pairzd2optimzd2quotezd2maxlenzd2zzcfa_pairz00();
		}

	}



/* patch-pair-set! */
	BGL_EXPORTED_DEF obj_t BGl_patchzd2pairzd2setz12z12zzcfa_pairz00(void)
	{
		{	/* Cfa/pair.scm 59 */
			if (BGl_pairzd2optimzf3z21zzcfa_pairz00())
				{	/* Cfa/pair.scm 62 */
					{	/* Cfa/pair.scm 63 */
						obj_t BgL_g1571z00_3421;

						BgL_g1571z00_3421 = CNST_TABLE_REF(0);
						{
							obj_t BgL_l1569z00_3423;

							BgL_l1569z00_3423 = BgL_g1571z00_3421;
						BgL_zc3z04anonymousza31595ze3z87_3424:
							if (PAIRP(BgL_l1569z00_3423))
								{	/* Cfa/pair.scm 74 */
									{	/* Cfa/pair.scm 64 */
										obj_t BgL_idz00_3426;

										BgL_idz00_3426 = CAR(BgL_l1569z00_3423);
										{	/* Cfa/pair.scm 64 */
											obj_t BgL_gz00_3427;

											{	/* Cfa/pair.scm 64 */
												obj_t BgL_arg1625z00_3440;
												obj_t BgL_arg1626z00_3441;

												{	/* Cfa/pair.scm 64 */
													obj_t BgL_pairz00_4183;

													BgL_pairz00_4183 = CDR(((obj_t) BgL_idz00_3426));
													BgL_arg1625z00_3440 = CAR(BgL_pairz00_4183);
												}
												{	/* Cfa/pair.scm 64 */
													obj_t BgL_pairz00_4189;

													{	/* Cfa/pair.scm 64 */
														obj_t BgL_pairz00_4188;

														BgL_pairz00_4188 = CDR(((obj_t) BgL_idz00_3426));
														BgL_pairz00_4189 = CDR(BgL_pairz00_4188);
													}
													BgL_arg1626z00_3441 = CAR(BgL_pairz00_4189);
												}
												BgL_gz00_3427 =
													BGl_findzd2globalzf2modulez20zzast_envz00
													(BgL_arg1625z00_3440, BgL_arg1626z00_3441);
											}
											{	/* Cfa/pair.scm 64 */
												BgL_valuez00_bglt BgL_funz00_3428;

												BgL_funz00_3428 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_globalz00_bglt) BgL_gz00_3427))))->
													BgL_valuez00);
												{	/* Cfa/pair.scm 65 */

													{	/* Cfa/pair.scm 66 */
														BgL_valuez00_bglt BgL_arg1602z00_3429;

														BgL_arg1602z00_3429 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_globalz00_bglt) BgL_gz00_3427))))->
															BgL_valuez00);
														((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																			BgL_arg1602z00_3429)))->BgL_topzf3zf3) =
															((bool_t) ((bool_t) 0)), BUNSPEC);
													}
													{	/* Cfa/pair.scm 67 */
														bool_t BgL_test2108z00_4971;

														{	/* Cfa/pair.scm 67 */
															obj_t BgL_classz00_4194;

															BgL_classz00_4194 = BGl_cfunz00zzast_varz00;
															{	/* Cfa/pair.scm 67 */
																BgL_objectz00_bglt BgL_arg1807z00_4196;

																{	/* Cfa/pair.scm 67 */
																	obj_t BgL_tmpz00_4972;

																	BgL_tmpz00_4972 =
																		((obj_t)
																		((BgL_objectz00_bglt) BgL_funz00_3428));
																	BgL_arg1807z00_4196 =
																		(BgL_objectz00_bglt) (BgL_tmpz00_4972);
																}
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Cfa/pair.scm 67 */
																		long BgL_idxz00_4202;

																		BgL_idxz00_4202 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_4196);
																		BgL_test2108z00_4971 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_4202 + 3L)) ==
																			BgL_classz00_4194);
																	}
																else
																	{	/* Cfa/pair.scm 67 */
																		bool_t BgL_res2060z00_4227;

																		{	/* Cfa/pair.scm 67 */
																			obj_t BgL_oclassz00_4210;

																			{	/* Cfa/pair.scm 67 */
																				obj_t BgL_arg1815z00_4218;
																				long BgL_arg1816z00_4219;

																				BgL_arg1815z00_4218 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Cfa/pair.scm 67 */
																					long BgL_arg1817z00_4220;

																					BgL_arg1817z00_4220 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_4196);
																					BgL_arg1816z00_4219 =
																						(BgL_arg1817z00_4220 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_4210 =
																					VECTOR_REF(BgL_arg1815z00_4218,
																					BgL_arg1816z00_4219);
																			}
																			{	/* Cfa/pair.scm 67 */
																				bool_t BgL__ortest_1115z00_4211;

																				BgL__ortest_1115z00_4211 =
																					(BgL_classz00_4194 ==
																					BgL_oclassz00_4210);
																				if (BgL__ortest_1115z00_4211)
																					{	/* Cfa/pair.scm 67 */
																						BgL_res2060z00_4227 =
																							BgL__ortest_1115z00_4211;
																					}
																				else
																					{	/* Cfa/pair.scm 67 */
																						long BgL_odepthz00_4212;

																						{	/* Cfa/pair.scm 67 */
																							obj_t BgL_arg1804z00_4213;

																							BgL_arg1804z00_4213 =
																								(BgL_oclassz00_4210);
																							BgL_odepthz00_4212 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_4213);
																						}
																						if ((3L < BgL_odepthz00_4212))
																							{	/* Cfa/pair.scm 67 */
																								obj_t BgL_arg1802z00_4215;

																								{	/* Cfa/pair.scm 67 */
																									obj_t BgL_arg1803z00_4216;

																									BgL_arg1803z00_4216 =
																										(BgL_oclassz00_4210);
																									BgL_arg1802z00_4215 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_4216, 3L);
																								}
																								BgL_res2060z00_4227 =
																									(BgL_arg1802z00_4215 ==
																									BgL_classz00_4194);
																							}
																						else
																							{	/* Cfa/pair.scm 67 */
																								BgL_res2060z00_4227 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2108z00_4971 = BgL_res2060z00_4227;
																	}
															}
														}
														if (BgL_test2108z00_4971)
															{	/* Cfa/pair.scm 68 */
																obj_t BgL_argsz00_3431;

																BgL_argsz00_3431 =
																	(((BgL_cfunz00_bglt) COBJECT(
																			((BgL_cfunz00_bglt) BgL_funz00_3428)))->
																	BgL_argszd2typezd2);
																{	/* Cfa/pair.scm 69 */
																	BgL_typez00_bglt BgL_arg1605z00_3432;

																	BgL_arg1605z00_3432 =
																		BGl_getzd2defaultzd2typez00zztype_cachez00
																		();
																	{	/* Cfa/pair.scm 69 */
																		obj_t BgL_auxz00_5000;
																		obj_t BgL_tmpz00_4998;

																		BgL_auxz00_5000 =
																			((obj_t) BgL_arg1605z00_3432);
																		BgL_tmpz00_4998 =
																			((obj_t) BgL_argsz00_3431);
																		SET_CAR(BgL_tmpz00_4998, BgL_auxz00_5000);
																	}
																}
																{	/* Cfa/pair.scm 70 */
																	obj_t BgL_arg1606z00_3433;
																	BgL_typez00_bglt BgL_arg1609z00_3434;

																	BgL_arg1606z00_3433 =
																		CDR(((obj_t) BgL_argsz00_3431));
																	BgL_arg1609z00_3434 =
																		BGl_getzd2defaultzd2typez00zztype_cachez00
																		();
																	{	/* Cfa/pair.scm 70 */
																		obj_t BgL_auxz00_5008;
																		obj_t BgL_tmpz00_5006;

																		BgL_auxz00_5008 =
																			((obj_t) BgL_arg1609z00_3434);
																		BgL_tmpz00_5006 =
																			((obj_t) BgL_arg1606z00_3433);
																		SET_CAR(BgL_tmpz00_5006, BgL_auxz00_5008);
																	}
																}
															}
														else
															{	/* Cfa/pair.scm 71 */
																obj_t BgL_argsz00_3435;

																BgL_argsz00_3435 =
																	(((BgL_sfunz00_bglt) COBJECT(
																			((BgL_sfunz00_bglt) BgL_funz00_3428)))->
																	BgL_argsz00);
																{	/* Cfa/pair.scm 72 */
																	obj_t BgL_arg1611z00_3436;
																	BgL_typez00_bglt BgL_arg1613z00_3437;

																	BgL_arg1611z00_3436 =
																		CAR(((obj_t) BgL_argsz00_3435));
																	BgL_arg1613z00_3437 =
																		BGl_getzd2defaultzd2typez00zztype_cachez00
																		();
																	((((BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt) (
																							(BgL_localz00_bglt)
																							BgL_arg1611z00_3436))))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) BgL_arg1613z00_3437),
																		BUNSPEC);
																}
																{	/* Cfa/pair.scm 73 */
																	obj_t BgL_arg1615z00_3438;
																	BgL_typez00_bglt BgL_arg1616z00_3439;

																	{	/* Cfa/pair.scm 73 */
																		obj_t BgL_pairz00_4239;

																		BgL_pairz00_4239 =
																			CDR(((obj_t) BgL_argsz00_3435));
																		BgL_arg1615z00_3438 = CAR(BgL_pairz00_4239);
																	}
																	BgL_arg1616z00_3439 =
																		BGl_getzd2defaultzd2typez00zztype_cachez00
																		();
																	((((BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt) (
																							(BgL_localz00_bglt)
																							BgL_arg1615z00_3438))))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) BgL_arg1616z00_3439),
																		BUNSPEC);
																}
															}
													}
												}
											}
										}
									}
									{
										obj_t BgL_l1569z00_5026;

										BgL_l1569z00_5026 = CDR(BgL_l1569z00_3423);
										BgL_l1569z00_3423 = BgL_l1569z00_5026;
										goto BgL_zc3z04anonymousza31595ze3z87_3424;
									}
								}
							else
								{	/* Cfa/pair.scm 74 */
									((bool_t) 1);
								}
						}
					}
					{	/* Cfa/pair.scm 80 */
						obj_t BgL_gz00_3444;

						BgL_gz00_3444 =
							BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(1), BNIL);
						{	/* Cfa/pair.scm 81 */
							bool_t BgL_test2112z00_5030;

							{	/* Cfa/pair.scm 81 */
								obj_t BgL_classz00_4243;

								BgL_classz00_4243 = BGl_globalz00zzast_varz00;
								if (BGL_OBJECTP(BgL_gz00_3444))
									{	/* Cfa/pair.scm 81 */
										BgL_objectz00_bglt BgL_arg1807z00_4245;

										BgL_arg1807z00_4245 = (BgL_objectz00_bglt) (BgL_gz00_3444);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Cfa/pair.scm 81 */
												long BgL_idxz00_4251;

												BgL_idxz00_4251 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4245);
												BgL_test2112z00_5030 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_4251 + 2L)) == BgL_classz00_4243);
											}
										else
											{	/* Cfa/pair.scm 81 */
												bool_t BgL_res2061z00_4276;

												{	/* Cfa/pair.scm 81 */
													obj_t BgL_oclassz00_4259;

													{	/* Cfa/pair.scm 81 */
														obj_t BgL_arg1815z00_4267;
														long BgL_arg1816z00_4268;

														BgL_arg1815z00_4267 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Cfa/pair.scm 81 */
															long BgL_arg1817z00_4269;

															BgL_arg1817z00_4269 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4245);
															BgL_arg1816z00_4268 =
																(BgL_arg1817z00_4269 - OBJECT_TYPE);
														}
														BgL_oclassz00_4259 =
															VECTOR_REF(BgL_arg1815z00_4267,
															BgL_arg1816z00_4268);
													}
													{	/* Cfa/pair.scm 81 */
														bool_t BgL__ortest_1115z00_4260;

														BgL__ortest_1115z00_4260 =
															(BgL_classz00_4243 == BgL_oclassz00_4259);
														if (BgL__ortest_1115z00_4260)
															{	/* Cfa/pair.scm 81 */
																BgL_res2061z00_4276 = BgL__ortest_1115z00_4260;
															}
														else
															{	/* Cfa/pair.scm 81 */
																long BgL_odepthz00_4261;

																{	/* Cfa/pair.scm 81 */
																	obj_t BgL_arg1804z00_4262;

																	BgL_arg1804z00_4262 = (BgL_oclassz00_4259);
																	BgL_odepthz00_4261 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_4262);
																}
																if ((2L < BgL_odepthz00_4261))
																	{	/* Cfa/pair.scm 81 */
																		obj_t BgL_arg1802z00_4264;

																		{	/* Cfa/pair.scm 81 */
																			obj_t BgL_arg1803z00_4265;

																			BgL_arg1803z00_4265 =
																				(BgL_oclassz00_4259);
																			BgL_arg1802z00_4264 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_4265, 2L);
																		}
																		BgL_res2061z00_4276 =
																			(BgL_arg1802z00_4264 ==
																			BgL_classz00_4243);
																	}
																else
																	{	/* Cfa/pair.scm 81 */
																		BgL_res2061z00_4276 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2112z00_5030 = BgL_res2061z00_4276;
											}
									}
								else
									{	/* Cfa/pair.scm 81 */
										BgL_test2112z00_5030 = ((bool_t) 0);
									}
							}
							if (BgL_test2112z00_5030)
								{	/* Cfa/pair.scm 82 */
									BgL_valuez00_bglt BgL_fz00_3446;

									BgL_fz00_3446 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_gz00_3444))))->
										BgL_valuez00);
									{	/* Cfa/pair.scm 83 */
										obj_t BgL_arg1629z00_3447;
										BgL_typez00_bglt BgL_arg1630z00_3448;

										BgL_arg1629z00_3447 =
											(((BgL_cfunz00_bglt) COBJECT(
													((BgL_cfunz00_bglt) BgL_fz00_3446)))->
											BgL_argszd2typezd2);
										BgL_arg1630z00_3448 =
											BGl_getzd2defaultzd2typez00zztype_cachez00();
										{	/* Cfa/pair.scm 83 */
											obj_t BgL_auxz00_5061;
											obj_t BgL_tmpz00_5059;

											BgL_auxz00_5061 = ((obj_t) BgL_arg1630z00_3448);
											BgL_tmpz00_5059 = ((obj_t) BgL_arg1629z00_3447);
											return SET_CAR(BgL_tmpz00_5059, BgL_auxz00_5061);
										}
									}
								}
							else
								{	/* Cfa/pair.scm 81 */
									return BFALSE;
								}
						}
					}
				}
			else
				{	/* Cfa/pair.scm 62 */
					return BFALSE;
				}
		}

	}



/* &patch-pair-set! */
	obj_t BGl_z62patchzd2pairzd2setz12z70zzcfa_pairz00(obj_t BgL_envz00_4722)
	{
		{	/* Cfa/pair.scm 59 */
			return BGl_patchzd2pairzd2setz12z12zzcfa_pairz00();
		}

	}



/* unpatch-pair-set! */
	BGL_EXPORTED_DEF obj_t BGl_unpatchzd2pairzd2setz12z12zzcfa_pairz00(void)
	{
		{	/* Cfa/pair.scm 88 */
			if (BGl_pairzd2optimzf3z21zzcfa_pairz00())
				{	/* Cfa/pair.scm 89 */
					{	/* Cfa/pair.scm 90 */
						obj_t BgL_g1574z00_3451;

						BgL_g1574z00_3451 = CNST_TABLE_REF(0);
						{
							obj_t BgL_l1572z00_3453;

							BgL_l1572z00_3453 = BgL_g1574z00_3451;
						BgL_zc3z04anonymousza31634ze3z87_3454:
							if (PAIRP(BgL_l1572z00_3453))
								{	/* Cfa/pair.scm 100 */
									{	/* Cfa/pair.scm 91 */
										obj_t BgL_idz00_3456;

										BgL_idz00_3456 = CAR(BgL_l1572z00_3453);
										{	/* Cfa/pair.scm 91 */
											obj_t BgL_gz00_3457;

											{	/* Cfa/pair.scm 91 */
												obj_t BgL_arg1651z00_3465;
												obj_t BgL_arg1654z00_3466;

												{	/* Cfa/pair.scm 91 */
													obj_t BgL_pairz00_4284;

													BgL_pairz00_4284 = CDR(((obj_t) BgL_idz00_3456));
													BgL_arg1651z00_3465 = CAR(BgL_pairz00_4284);
												}
												{	/* Cfa/pair.scm 91 */
													obj_t BgL_pairz00_4290;

													{	/* Cfa/pair.scm 91 */
														obj_t BgL_pairz00_4289;

														BgL_pairz00_4289 = CDR(((obj_t) BgL_idz00_3456));
														BgL_pairz00_4290 = CDR(BgL_pairz00_4289);
													}
													BgL_arg1654z00_3466 = CAR(BgL_pairz00_4290);
												}
												BgL_gz00_3457 =
													BGl_findzd2globalzf2modulez20zzast_envz00
													(BgL_arg1651z00_3465, BgL_arg1654z00_3466);
											}
											{	/* Cfa/pair.scm 91 */
												BgL_valuez00_bglt BgL_funz00_3458;

												BgL_funz00_3458 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_globalz00_bglt) BgL_gz00_3457))))->
													BgL_valuez00);
												{	/* Cfa/pair.scm 92 */

													{	/* Cfa/pair.scm 93 */
														bool_t BgL_test2119z00_5082;

														{	/* Cfa/pair.scm 93 */
															obj_t BgL_classz00_4292;

															BgL_classz00_4292 = BGl_cfunz00zzast_varz00;
															{	/* Cfa/pair.scm 93 */
																BgL_objectz00_bglt BgL_arg1807z00_4294;

																{	/* Cfa/pair.scm 93 */
																	obj_t BgL_tmpz00_5083;

																	BgL_tmpz00_5083 =
																		((obj_t)
																		((BgL_objectz00_bglt) BgL_funz00_3458));
																	BgL_arg1807z00_4294 =
																		(BgL_objectz00_bglt) (BgL_tmpz00_5083);
																}
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Cfa/pair.scm 93 */
																		long BgL_idxz00_4300;

																		BgL_idxz00_4300 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_4294);
																		BgL_test2119z00_5082 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_4300 + 3L)) ==
																			BgL_classz00_4292);
																	}
																else
																	{	/* Cfa/pair.scm 93 */
																		bool_t BgL_res2062z00_4325;

																		{	/* Cfa/pair.scm 93 */
																			obj_t BgL_oclassz00_4308;

																			{	/* Cfa/pair.scm 93 */
																				obj_t BgL_arg1815z00_4316;
																				long BgL_arg1816z00_4317;

																				BgL_arg1815z00_4316 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Cfa/pair.scm 93 */
																					long BgL_arg1817z00_4318;

																					BgL_arg1817z00_4318 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_4294);
																					BgL_arg1816z00_4317 =
																						(BgL_arg1817z00_4318 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_4308 =
																					VECTOR_REF(BgL_arg1815z00_4316,
																					BgL_arg1816z00_4317);
																			}
																			{	/* Cfa/pair.scm 93 */
																				bool_t BgL__ortest_1115z00_4309;

																				BgL__ortest_1115z00_4309 =
																					(BgL_classz00_4292 ==
																					BgL_oclassz00_4308);
																				if (BgL__ortest_1115z00_4309)
																					{	/* Cfa/pair.scm 93 */
																						BgL_res2062z00_4325 =
																							BgL__ortest_1115z00_4309;
																					}
																				else
																					{	/* Cfa/pair.scm 93 */
																						long BgL_odepthz00_4310;

																						{	/* Cfa/pair.scm 93 */
																							obj_t BgL_arg1804z00_4311;

																							BgL_arg1804z00_4311 =
																								(BgL_oclassz00_4308);
																							BgL_odepthz00_4310 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_4311);
																						}
																						if ((3L < BgL_odepthz00_4310))
																							{	/* Cfa/pair.scm 93 */
																								obj_t BgL_arg1802z00_4313;

																								{	/* Cfa/pair.scm 93 */
																									obj_t BgL_arg1803z00_4314;

																									BgL_arg1803z00_4314 =
																										(BgL_oclassz00_4308);
																									BgL_arg1802z00_4313 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_4314, 3L);
																								}
																								BgL_res2062z00_4325 =
																									(BgL_arg1802z00_4313 ==
																									BgL_classz00_4292);
																							}
																						else
																							{	/* Cfa/pair.scm 93 */
																								BgL_res2062z00_4325 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2119z00_5082 = BgL_res2062z00_4325;
																	}
															}
														}
														if (BgL_test2119z00_5082)
															{	/* Cfa/pair.scm 94 */
																obj_t BgL_argsz00_3460;

																BgL_argsz00_3460 =
																	(((BgL_cfunz00_bglt) COBJECT(
																			((BgL_cfunz00_bglt) BgL_funz00_3458)))->
																	BgL_argszd2typezd2);
																{	/* Cfa/pair.scm 95 */
																	obj_t BgL_objz00_4328;

																	BgL_objz00_4328 =
																		BGl_za2objza2z00zztype_cachez00;
																	{	/* Cfa/pair.scm 95 */
																		obj_t BgL_tmpz00_5108;

																		BgL_tmpz00_5108 =
																			((obj_t) BgL_argsz00_3460);
																		SET_CAR(BgL_tmpz00_5108, BgL_objz00_4328);
																	}
																}
																{	/* Cfa/pair.scm 96 */
																	obj_t BgL_arg1642z00_3461;

																	BgL_arg1642z00_3461 =
																		CDR(((obj_t) BgL_argsz00_3460));
																	{	/* Cfa/pair.scm 96 */
																		obj_t BgL_objz00_4331;

																		BgL_objz00_4331 =
																			BGl_za2objza2z00zztype_cachez00;
																		{	/* Cfa/pair.scm 96 */
																			obj_t BgL_tmpz00_5113;

																			BgL_tmpz00_5113 =
																				((obj_t) BgL_arg1642z00_3461);
																			SET_CAR(BgL_tmpz00_5113, BgL_objz00_4331);
																		}
																	}
																}
															}
														else
															{	/* Cfa/pair.scm 97 */
																obj_t BgL_argsz00_3462;

																BgL_argsz00_3462 =
																	(((BgL_sfunz00_bglt) COBJECT(
																			((BgL_sfunz00_bglt) BgL_funz00_3458)))->
																	BgL_argsz00);
																{	/* Cfa/pair.scm 98 */
																	obj_t BgL_arg1646z00_3463;

																	BgL_arg1646z00_3463 =
																		CAR(((obj_t) BgL_argsz00_3462));
																	{	/* Cfa/pair.scm 98 */
																		BgL_typez00_bglt BgL_vz00_4335;

																		BgL_vz00_4335 =
																			((BgL_typez00_bglt)
																			BGl_za2objza2z00zztype_cachez00);
																		((((BgL_variablez00_bglt)
																					COBJECT(((BgL_variablez00_bglt) (
																								(BgL_localz00_bglt)
																								BgL_arg1646z00_3463))))->
																				BgL_typez00) =
																			((BgL_typez00_bglt) BgL_vz00_4335),
																			BUNSPEC);
																	}
																}
																{	/* Cfa/pair.scm 99 */
																	obj_t BgL_arg1650z00_3464;

																	{	/* Cfa/pair.scm 99 */
																		obj_t BgL_pairz00_4339;

																		BgL_pairz00_4339 =
																			CDR(((obj_t) BgL_argsz00_3462));
																		BgL_arg1650z00_3464 = CAR(BgL_pairz00_4339);
																	}
																	{	/* Cfa/pair.scm 99 */
																		BgL_typez00_bglt BgL_vz00_4341;

																		BgL_vz00_4341 =
																			((BgL_typez00_bglt)
																			BGl_za2objza2z00zztype_cachez00);
																		((((BgL_variablez00_bglt)
																					COBJECT(((BgL_variablez00_bglt) (
																								(BgL_localz00_bglt)
																								BgL_arg1650z00_3464))))->
																				BgL_typez00) =
																			((BgL_typez00_bglt) BgL_vz00_4341),
																			BUNSPEC);
																	}
																}
															}
													}
												}
											}
										}
									}
									{
										obj_t BgL_l1572z00_5131;

										BgL_l1572z00_5131 = CDR(BgL_l1572z00_3453);
										BgL_l1572z00_3453 = BgL_l1572z00_5131;
										goto BgL_zc3z04anonymousza31634ze3z87_3454;
									}
								}
							else
								{	/* Cfa/pair.scm 100 */
									((bool_t) 1);
								}
						}
					}
					{	/* Cfa/pair.scm 106 */
						obj_t BgL_gz00_3469;

						BgL_gz00_3469 =
							BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(1), BNIL);
						{	/* Cfa/pair.scm 107 */
							bool_t BgL_test2123z00_5135;

							{	/* Cfa/pair.scm 107 */
								obj_t BgL_classz00_4343;

								BgL_classz00_4343 = BGl_globalz00zzast_varz00;
								if (BGL_OBJECTP(BgL_gz00_3469))
									{	/* Cfa/pair.scm 107 */
										BgL_objectz00_bglt BgL_arg1807z00_4345;

										BgL_arg1807z00_4345 = (BgL_objectz00_bglt) (BgL_gz00_3469);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Cfa/pair.scm 107 */
												long BgL_idxz00_4351;

												BgL_idxz00_4351 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4345);
												BgL_test2123z00_5135 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_4351 + 2L)) == BgL_classz00_4343);
											}
										else
											{	/* Cfa/pair.scm 107 */
												bool_t BgL_res2063z00_4376;

												{	/* Cfa/pair.scm 107 */
													obj_t BgL_oclassz00_4359;

													{	/* Cfa/pair.scm 107 */
														obj_t BgL_arg1815z00_4367;
														long BgL_arg1816z00_4368;

														BgL_arg1815z00_4367 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Cfa/pair.scm 107 */
															long BgL_arg1817z00_4369;

															BgL_arg1817z00_4369 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4345);
															BgL_arg1816z00_4368 =
																(BgL_arg1817z00_4369 - OBJECT_TYPE);
														}
														BgL_oclassz00_4359 =
															VECTOR_REF(BgL_arg1815z00_4367,
															BgL_arg1816z00_4368);
													}
													{	/* Cfa/pair.scm 107 */
														bool_t BgL__ortest_1115z00_4360;

														BgL__ortest_1115z00_4360 =
															(BgL_classz00_4343 == BgL_oclassz00_4359);
														if (BgL__ortest_1115z00_4360)
															{	/* Cfa/pair.scm 107 */
																BgL_res2063z00_4376 = BgL__ortest_1115z00_4360;
															}
														else
															{	/* Cfa/pair.scm 107 */
																long BgL_odepthz00_4361;

																{	/* Cfa/pair.scm 107 */
																	obj_t BgL_arg1804z00_4362;

																	BgL_arg1804z00_4362 = (BgL_oclassz00_4359);
																	BgL_odepthz00_4361 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_4362);
																}
																if ((2L < BgL_odepthz00_4361))
																	{	/* Cfa/pair.scm 107 */
																		obj_t BgL_arg1802z00_4364;

																		{	/* Cfa/pair.scm 107 */
																			obj_t BgL_arg1803z00_4365;

																			BgL_arg1803z00_4365 =
																				(BgL_oclassz00_4359);
																			BgL_arg1802z00_4364 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_4365, 2L);
																		}
																		BgL_res2063z00_4376 =
																			(BgL_arg1802z00_4364 ==
																			BgL_classz00_4343);
																	}
																else
																	{	/* Cfa/pair.scm 107 */
																		BgL_res2063z00_4376 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2123z00_5135 = BgL_res2063z00_4376;
											}
									}
								else
									{	/* Cfa/pair.scm 107 */
										BgL_test2123z00_5135 = ((bool_t) 0);
									}
							}
							if (BgL_test2123z00_5135)
								{	/* Cfa/pair.scm 108 */
									BgL_valuez00_bglt BgL_fz00_3471;

									BgL_fz00_3471 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_gz00_3469))))->
										BgL_valuez00);
									{	/* Cfa/pair.scm 109 */
										obj_t BgL_arg1675z00_3472;

										BgL_arg1675z00_3472 =
											(((BgL_cfunz00_bglt) COBJECT(
													((BgL_cfunz00_bglt) BgL_fz00_3471)))->
											BgL_argszd2typezd2);
										{	/* Cfa/pair.scm 109 */
											obj_t BgL_objz00_4380;

											BgL_objz00_4380 = BGl_za2objza2z00zztype_cachez00;
											{	/* Cfa/pair.scm 109 */
												obj_t BgL_tmpz00_5163;

												BgL_tmpz00_5163 = ((obj_t) BgL_arg1675z00_3472);
												SET_CAR(BgL_tmpz00_5163, BgL_objz00_4380);
											}
										}
									}
								}
							else
								{	/* Cfa/pair.scm 107 */
									BFALSE;
								}
						}
					}
					{	/* Cfa/pair.scm 110 */
						obj_t BgL_gz00_3474;

						BgL_gz00_3474 =
							BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(2), BNIL);
						{	/* Cfa/pair.scm 111 */
							bool_t BgL_test2128z00_5168;

							{	/* Cfa/pair.scm 111 */
								obj_t BgL_classz00_4381;

								BgL_classz00_4381 = BGl_globalz00zzast_varz00;
								if (BGL_OBJECTP(BgL_gz00_3474))
									{	/* Cfa/pair.scm 111 */
										BgL_objectz00_bglt BgL_arg1807z00_4383;

										BgL_arg1807z00_4383 = (BgL_objectz00_bglt) (BgL_gz00_3474);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Cfa/pair.scm 111 */
												long BgL_idxz00_4389;

												BgL_idxz00_4389 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4383);
												BgL_test2128z00_5168 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_4389 + 2L)) == BgL_classz00_4381);
											}
										else
											{	/* Cfa/pair.scm 111 */
												bool_t BgL_res2064z00_4414;

												{	/* Cfa/pair.scm 111 */
													obj_t BgL_oclassz00_4397;

													{	/* Cfa/pair.scm 111 */
														obj_t BgL_arg1815z00_4405;
														long BgL_arg1816z00_4406;

														BgL_arg1815z00_4405 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Cfa/pair.scm 111 */
															long BgL_arg1817z00_4407;

															BgL_arg1817z00_4407 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4383);
															BgL_arg1816z00_4406 =
																(BgL_arg1817z00_4407 - OBJECT_TYPE);
														}
														BgL_oclassz00_4397 =
															VECTOR_REF(BgL_arg1815z00_4405,
															BgL_arg1816z00_4406);
													}
													{	/* Cfa/pair.scm 111 */
														bool_t BgL__ortest_1115z00_4398;

														BgL__ortest_1115z00_4398 =
															(BgL_classz00_4381 == BgL_oclassz00_4397);
														if (BgL__ortest_1115z00_4398)
															{	/* Cfa/pair.scm 111 */
																BgL_res2064z00_4414 = BgL__ortest_1115z00_4398;
															}
														else
															{	/* Cfa/pair.scm 111 */
																long BgL_odepthz00_4399;

																{	/* Cfa/pair.scm 111 */
																	obj_t BgL_arg1804z00_4400;

																	BgL_arg1804z00_4400 = (BgL_oclassz00_4397);
																	BgL_odepthz00_4399 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_4400);
																}
																if ((2L < BgL_odepthz00_4399))
																	{	/* Cfa/pair.scm 111 */
																		obj_t BgL_arg1802z00_4402;

																		{	/* Cfa/pair.scm 111 */
																			obj_t BgL_arg1803z00_4403;

																			BgL_arg1803z00_4403 =
																				(BgL_oclassz00_4397);
																			BgL_arg1802z00_4402 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_4403, 2L);
																		}
																		BgL_res2064z00_4414 =
																			(BgL_arg1802z00_4402 ==
																			BgL_classz00_4381);
																	}
																else
																	{	/* Cfa/pair.scm 111 */
																		BgL_res2064z00_4414 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2128z00_5168 = BgL_res2064z00_4414;
											}
									}
								else
									{	/* Cfa/pair.scm 111 */
										BgL_test2128z00_5168 = ((bool_t) 0);
									}
							}
							if (BgL_test2128z00_5168)
								{	/* Cfa/pair.scm 112 */
									BgL_valuez00_bglt BgL_fz00_3476;

									BgL_fz00_3476 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_gz00_3474))))->
										BgL_valuez00);
									{	/* Cfa/pair.scm 113 */
										obj_t BgL_arg1678z00_3477;

										{	/* Cfa/pair.scm 113 */
											obj_t BgL_pairz00_4417;

											BgL_pairz00_4417 =
												(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_fz00_3476)))->BgL_argsz00);
											BgL_arg1678z00_3477 = CAR(BgL_pairz00_4417);
										}
										{	/* Cfa/pair.scm 113 */
											BgL_typez00_bglt BgL_vz00_4419;

											BgL_vz00_4419 =
												((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00);
											((((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_localz00_bglt) BgL_arg1678z00_3477))))->
													BgL_typez00) =
												((BgL_typez00_bglt) BgL_vz00_4419), BUNSPEC);
										}
									}
								}
							else
								{	/* Cfa/pair.scm 111 */
									BFALSE;
								}
						}
					}
				}
			else
				{	/* Cfa/pair.scm 89 */
					BFALSE;
				}
			return BUNSPEC;
		}

	}



/* &unpatch-pair-set! */
	obj_t BGl_z62unpatchzd2pairzd2setz12z70zzcfa_pairz00(obj_t BgL_envz00_4723)
	{
		{	/* Cfa/pair.scm 88 */
			return BGl_unpatchzd2pairzd2setz12z12zzcfa_pairz00();
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcfa_pairz00(void)
	{
		{	/* Cfa/pair.scm 17 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcfa_pairz00(void)
	{
		{	/* Cfa/pair.scm 17 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcfa_pairz00(void)
	{
		{	/* Cfa/pair.scm 17 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_prezd2conszd2appz00zzcfa_info2z00, BGl_proc2073z00zzcfa_pairz00,
				BGl_string2074z00zzcfa_pairz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_prezd2conszd2refzd2appzd2zzcfa_info2z00,
				BGl_proc2075z00zzcfa_pairz00, BGl_string2074z00zzcfa_pairz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_prezd2conszd2setz12zd2appzc0zzcfa_info2z00,
				BGl_proc2076z00zzcfa_pairz00, BGl_string2074z00zzcfa_pairz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_conszd2appzd2zzcfa_info2z00,
				BGl_proc2077z00zzcfa_pairz00, BGl_string2078z00zzcfa_pairz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_conszd2refzd2appz00zzcfa_info2z00,
				BGl_proc2079z00zzcfa_pairz00, BGl_string2078z00zzcfa_pairz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00,
				BGl_conszd2setz12zd2appz12zzcfa_info2z00, BGl_proc2080z00zzcfa_pairz00,
				BGl_string2078z00zzcfa_pairz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_loosezd2allocz12zd2envz12zzcfa_loosez00,
				BGl_conszd2appzd2zzcfa_info2z00, BGl_proc2081z00zzcfa_pairz00,
				BGl_string2082z00zzcfa_pairz00);
		}

	}



/* &loose-alloc!-cons-ap1588 */
	obj_t BGl_z62loosezd2allocz12zd2conszd2ap1588za2zzcfa_pairz00(obj_t
		BgL_envz00_4735, obj_t BgL_allocz00_4736)
	{
		{	/* Cfa/pair.scm 245 */
			{	/* Cfa/pair.scm 247 */
				bool_t BgL_test2133z00_5209;

				{	/* Cfa/pair.scm 247 */
					long BgL_arg1853z00_4772;

					{
						BgL_conszd2appzd2_bglt BgL_auxz00_5210;

						{
							obj_t BgL_auxz00_5211;

							{	/* Cfa/pair.scm 247 */
								BgL_objectz00_bglt BgL_tmpz00_5212;

								BgL_tmpz00_5212 =
									((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_allocz00_4736));
								BgL_auxz00_5211 = BGL_OBJECT_WIDENING(BgL_tmpz00_5212);
							}
							BgL_auxz00_5210 = ((BgL_conszd2appzd2_bglt) BgL_auxz00_5211);
						}
						BgL_arg1853z00_4772 =
							(((BgL_conszd2appzd2_bglt) COBJECT(BgL_auxz00_5210))->
							BgL_lostzd2stampzd2);
					}
					BgL_test2133z00_5209 =
						(BgL_arg1853z00_4772 ==
						(long) CINT(BGl_za2cfazd2stampza2zd2zzcfa_iteratez00));
				}
				if (BgL_test2133z00_5209)
					{	/* Cfa/pair.scm 247 */
						return BFALSE;
					}
				else
					{	/* Cfa/pair.scm 247 */
						{
							BgL_conszd2appzd2_bglt BgL_auxz00_5220;

							{
								obj_t BgL_auxz00_5221;

								{	/* Cfa/pair.scm 250 */
									BgL_objectz00_bglt BgL_tmpz00_5222;

									BgL_tmpz00_5222 =
										((BgL_objectz00_bglt)
										((BgL_appz00_bglt) BgL_allocz00_4736));
									BgL_auxz00_5221 = BGL_OBJECT_WIDENING(BgL_tmpz00_5222);
								}
								BgL_auxz00_5220 = ((BgL_conszd2appzd2_bglt) BgL_auxz00_5221);
							}
							((((BgL_conszd2appzd2_bglt) COBJECT(BgL_auxz00_5220))->
									BgL_lostzd2stampzd2) =
								((long) (long) CINT(BGl_za2cfazd2stampza2zd2zzcfa_iteratez00)),
								BUNSPEC);
						}
						{	/* Cfa/pair.scm 251 */
							obj_t BgL_arg1840z00_4773;

							{	/* Cfa/pair.scm 251 */
								obj_t BgL_arg1842z00_4774;

								{
									BgL_conszd2appzd2_bglt BgL_auxz00_5229;

									{
										obj_t BgL_auxz00_5230;

										{	/* Cfa/pair.scm 251 */
											BgL_objectz00_bglt BgL_tmpz00_5231;

											BgL_tmpz00_5231 =
												((BgL_objectz00_bglt)
												((BgL_appz00_bglt) BgL_allocz00_4736));
											BgL_auxz00_5230 = BGL_OBJECT_WIDENING(BgL_tmpz00_5231);
										}
										BgL_auxz00_5229 =
											((BgL_conszd2appzd2_bglt) BgL_auxz00_5230);
									}
									BgL_arg1842z00_4774 =
										(((BgL_conszd2appzd2_bglt) COBJECT(BgL_auxz00_5229))->
										BgL_approxesz00);
								}
								BgL_arg1840z00_4773 = CAR(BgL_arg1842z00_4774);
							}
							BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00
								(BGl_loosezd2allocz12zd2envz12zzcfa_loosez00,
								((BgL_approxz00_bglt) BgL_arg1840z00_4773));
						}
						{	/* Cfa/pair.scm 252 */
							obj_t BgL_arg1843z00_4775;

							{	/* Cfa/pair.scm 252 */
								obj_t BgL_arg1844z00_4776;

								{
									BgL_conszd2appzd2_bglt BgL_auxz00_5240;

									{
										obj_t BgL_auxz00_5241;

										{	/* Cfa/pair.scm 252 */
											BgL_objectz00_bglt BgL_tmpz00_5242;

											BgL_tmpz00_5242 =
												((BgL_objectz00_bglt)
												((BgL_appz00_bglt) BgL_allocz00_4736));
											BgL_auxz00_5241 = BGL_OBJECT_WIDENING(BgL_tmpz00_5242);
										}
										BgL_auxz00_5240 =
											((BgL_conszd2appzd2_bglt) BgL_auxz00_5241);
									}
									BgL_arg1844z00_4776 =
										(((BgL_conszd2appzd2_bglt) COBJECT(BgL_auxz00_5240))->
										BgL_approxesz00);
								}
								BgL_arg1843z00_4775 = CDR(BgL_arg1844z00_4776);
							}
							BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00
								(BGl_loosezd2allocz12zd2envz12zzcfa_loosez00,
								((BgL_approxz00_bglt) BgL_arg1843z00_4775));
						}
						{	/* Cfa/pair.scm 253 */
							obj_t BgL_arg1845z00_4777;

							{	/* Cfa/pair.scm 253 */
								obj_t BgL_arg1846z00_4778;

								{
									BgL_conszd2appzd2_bglt BgL_auxz00_5251;

									{
										obj_t BgL_auxz00_5252;

										{	/* Cfa/pair.scm 253 */
											BgL_objectz00_bglt BgL_tmpz00_5253;

											BgL_tmpz00_5253 =
												((BgL_objectz00_bglt)
												((BgL_appz00_bglt) BgL_allocz00_4736));
											BgL_auxz00_5252 = BGL_OBJECT_WIDENING(BgL_tmpz00_5253);
										}
										BgL_auxz00_5251 =
											((BgL_conszd2appzd2_bglt) BgL_auxz00_5252);
									}
									BgL_arg1846z00_4778 =
										(((BgL_conszd2appzd2_bglt) COBJECT(BgL_auxz00_5251))->
										BgL_approxesz00);
								}
								BgL_arg1845z00_4777 = CAR(BgL_arg1846z00_4778);
							}
							BGl_approxzd2setzd2typez12z12zzcfa_approxz00(
								((BgL_approxz00_bglt) BgL_arg1845z00_4777),
								((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00));
						}
						{	/* Cfa/pair.scm 254 */
							obj_t BgL_arg1847z00_4779;

							{	/* Cfa/pair.scm 254 */
								obj_t BgL_arg1848z00_4780;

								{
									BgL_conszd2appzd2_bglt BgL_auxz00_5263;

									{
										obj_t BgL_auxz00_5264;

										{	/* Cfa/pair.scm 254 */
											BgL_objectz00_bglt BgL_tmpz00_5265;

											BgL_tmpz00_5265 =
												((BgL_objectz00_bglt)
												((BgL_appz00_bglt) BgL_allocz00_4736));
											BgL_auxz00_5264 = BGL_OBJECT_WIDENING(BgL_tmpz00_5265);
										}
										BgL_auxz00_5263 =
											((BgL_conszd2appzd2_bglt) BgL_auxz00_5264);
									}
									BgL_arg1848z00_4780 =
										(((BgL_conszd2appzd2_bglt) COBJECT(BgL_auxz00_5263))->
										BgL_approxesz00);
								}
								BgL_arg1847z00_4779 = CDR(BgL_arg1848z00_4780);
							}
							BGl_approxzd2setzd2typez12z12zzcfa_approxz00(
								((BgL_approxz00_bglt) BgL_arg1847z00_4779),
								((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00));
						}
						{	/* Cfa/pair.scm 255 */
							obj_t BgL_arg1849z00_4781;

							{	/* Cfa/pair.scm 255 */
								obj_t BgL_arg1850z00_4782;

								{
									BgL_conszd2appzd2_bglt BgL_auxz00_5275;

									{
										obj_t BgL_auxz00_5276;

										{	/* Cfa/pair.scm 255 */
											BgL_objectz00_bglt BgL_tmpz00_5277;

											BgL_tmpz00_5277 =
												((BgL_objectz00_bglt)
												((BgL_appz00_bglt) BgL_allocz00_4736));
											BgL_auxz00_5276 = BGL_OBJECT_WIDENING(BgL_tmpz00_5277);
										}
										BgL_auxz00_5275 =
											((BgL_conszd2appzd2_bglt) BgL_auxz00_5276);
									}
									BgL_arg1850z00_4782 =
										(((BgL_conszd2appzd2_bglt) COBJECT(BgL_auxz00_5275))->
										BgL_approxesz00);
								}
								BgL_arg1849z00_4781 = CAR(BgL_arg1850z00_4782);
							}
							BGl_approxzd2setzd2topz12z12zzcfa_approxz00(
								((BgL_approxz00_bglt) BgL_arg1849z00_4781));
						}
						{	/* Cfa/pair.scm 256 */
							obj_t BgL_arg1851z00_4783;

							{	/* Cfa/pair.scm 256 */
								obj_t BgL_arg1852z00_4784;

								{
									BgL_conszd2appzd2_bglt BgL_auxz00_5286;

									{
										obj_t BgL_auxz00_5287;

										{	/* Cfa/pair.scm 256 */
											BgL_objectz00_bglt BgL_tmpz00_5288;

											BgL_tmpz00_5288 =
												((BgL_objectz00_bglt)
												((BgL_appz00_bglt) BgL_allocz00_4736));
											BgL_auxz00_5287 = BGL_OBJECT_WIDENING(BgL_tmpz00_5288);
										}
										BgL_auxz00_5286 =
											((BgL_conszd2appzd2_bglt) BgL_auxz00_5287);
									}
									BgL_arg1852z00_4784 =
										(((BgL_conszd2appzd2_bglt) COBJECT(BgL_auxz00_5286))->
										BgL_approxesz00);
								}
								BgL_arg1851z00_4783 = CDR(BgL_arg1852z00_4784);
							}
							return
								BGl_approxzd2setzd2topz12z12zzcfa_approxz00(
								((BgL_approxz00_bglt) BgL_arg1851z00_4783));
						}
					}
			}
		}

	}



/* &cfa!-cons-set!-app1586 */
	BgL_approxz00_bglt
		BGl_z62cfaz12zd2conszd2setz12zd2app1586zb0zzcfa_pairz00(obj_t
		BgL_envz00_4737, obj_t BgL_nodez00_4738)
	{
		{	/* Cfa/pair.scm 212 */
			{	/* Cfa/pair.scm 214 */
				BgL_approxz00_bglt BgL_conszd2approxzd2_4786;
				BgL_approxz00_bglt BgL_valzd2approxzd2_4787;

				{	/* Cfa/pair.scm 214 */
					obj_t BgL_arg1834z00_4788;

					{	/* Cfa/pair.scm 214 */
						obj_t BgL_pairz00_4789;

						BgL_pairz00_4789 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt)
										((BgL_appz00_bglt) BgL_nodez00_4738))))->BgL_argsz00);
						BgL_arg1834z00_4788 = CAR(BgL_pairz00_4789);
					}
					BgL_conszd2approxzd2_4786 =
						BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1834z00_4788));
				}
				{	/* Cfa/pair.scm 215 */
					obj_t BgL_arg1836z00_4790;

					{	/* Cfa/pair.scm 215 */
						obj_t BgL_pairz00_4791;

						BgL_pairz00_4791 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt)
										((BgL_appz00_bglt) BgL_nodez00_4738))))->BgL_argsz00);
						BgL_arg1836z00_4790 = CAR(CDR(BgL_pairz00_4791));
					}
					BgL_valzd2approxzd2_4787 =
						BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1836z00_4790));
				}
				{	/* Cfa/pair.scm 220 */
					bool_t BgL_test2134z00_5310;

					{	/* Cfa/pair.scm 220 */
						BgL_typez00_bglt BgL_arg1820z00_4792;

						BgL_arg1820z00_4792 =
							(((BgL_approxz00_bglt) COBJECT(BgL_conszd2approxzd2_4786))->
							BgL_typez00);
						BgL_test2134z00_5310 =
							(((obj_t) BgL_arg1820z00_4792) ==
							BGl_za2pairza2z00zztype_cachez00);
					}
					if (BgL_test2134z00_5310)
						{	/* Cfa/pair.scm 220 */
							BFALSE;
						}
					else
						{	/* Cfa/pair.scm 220 */
							BGl_approxzd2setzd2typez12z12zzcfa_approxz00
								(BgL_valzd2approxzd2_4787,
								((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00));
						}
				}
				if (
					(((BgL_approxz00_bglt) COBJECT(BgL_conszd2approxzd2_4786))->
						BgL_topzf3zf3))
					{	/* Cfa/pair.scm 223 */
						((obj_t)
							BGl_loosez12z12zzcfa_loosez00(BgL_valzd2approxzd2_4787,
								CNST_TABLE_REF(3)));
					}
				else
					{	/* Cfa/pair.scm 229 */
						obj_t BgL_zc3z04anonymousza31823ze3z87_4793;

						BgL_zc3z04anonymousza31823ze3z87_4793 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31823ze3ze5zzcfa_pairz00, (int) (1L),
							(int) (2L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31823ze3z87_4793, (int) (0L),
							((obj_t) ((BgL_appz00_bglt) BgL_nodez00_4738)));
						PROCEDURE_SET(BgL_zc3z04anonymousza31823ze3z87_4793, (int) (1L),
							((obj_t) BgL_valzd2approxzd2_4787));
						BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00
							(BgL_zc3z04anonymousza31823ze3z87_4793,
							BgL_conszd2approxzd2_4786);
			}}
			{
				BgL_conszd2setz12zd2appz12_bglt BgL_auxz00_5332;

				{
					obj_t BgL_auxz00_5333;

					{	/* Cfa/pair.scm 236 */
						BgL_objectz00_bglt BgL_tmpz00_5334;

						BgL_tmpz00_5334 =
							((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4738));
						BgL_auxz00_5333 = BGL_OBJECT_WIDENING(BgL_tmpz00_5334);
					}
					BgL_auxz00_5332 = ((BgL_conszd2setz12zd2appz12_bglt) BgL_auxz00_5333);
				}
				return
					(((BgL_conszd2setz12zd2appz12_bglt) COBJECT(BgL_auxz00_5332))->
					BgL_approxz00);
			}
		}

	}



/* &<@anonymous:1823> */
	obj_t BGl_z62zc3z04anonymousza31823ze3ze5zzcfa_pairz00(obj_t BgL_envz00_4739,
		obj_t BgL_appz00_4742)
	{
		{	/* Cfa/pair.scm 228 */
			{	/* Cfa/pair.scm 229 */
				BgL_appz00_bglt BgL_i1194z00_4740;
				BgL_approxz00_bglt BgL_valzd2approxzd2_4741;

				BgL_i1194z00_4740 =
					((BgL_appz00_bglt) PROCEDURE_REF(BgL_envz00_4739, (int) (0L)));
				BgL_valzd2approxzd2_4741 =
					((BgL_approxz00_bglt) PROCEDURE_REF(BgL_envz00_4739, (int) (1L)));
				{	/* Cfa/pair.scm 229 */
					bool_t BgL_test2136z00_5346;

					{	/* Cfa/pair.scm 229 */
						obj_t BgL_classz00_4794;

						BgL_classz00_4794 = BGl_conszd2appzd2zzcfa_info2z00;
						if (BGL_OBJECTP(BgL_appz00_4742))
							{	/* Cfa/pair.scm 229 */
								BgL_objectz00_bglt BgL_arg1807z00_4795;

								BgL_arg1807z00_4795 = (BgL_objectz00_bglt) (BgL_appz00_4742);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cfa/pair.scm 229 */
										long BgL_idxz00_4796;

										BgL_idxz00_4796 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4795);
										BgL_test2136z00_5346 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_4796 + 4L)) == BgL_classz00_4794);
									}
								else
									{	/* Cfa/pair.scm 229 */
										bool_t BgL_res2066z00_4799;

										{	/* Cfa/pair.scm 229 */
											obj_t BgL_oclassz00_4800;

											{	/* Cfa/pair.scm 229 */
												obj_t BgL_arg1815z00_4801;
												long BgL_arg1816z00_4802;

												BgL_arg1815z00_4801 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cfa/pair.scm 229 */
													long BgL_arg1817z00_4803;

													BgL_arg1817z00_4803 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4795);
													BgL_arg1816z00_4802 =
														(BgL_arg1817z00_4803 - OBJECT_TYPE);
												}
												BgL_oclassz00_4800 =
													VECTOR_REF(BgL_arg1815z00_4801, BgL_arg1816z00_4802);
											}
											{	/* Cfa/pair.scm 229 */
												bool_t BgL__ortest_1115z00_4804;

												BgL__ortest_1115z00_4804 =
													(BgL_classz00_4794 == BgL_oclassz00_4800);
												if (BgL__ortest_1115z00_4804)
													{	/* Cfa/pair.scm 229 */
														BgL_res2066z00_4799 = BgL__ortest_1115z00_4804;
													}
												else
													{	/* Cfa/pair.scm 229 */
														long BgL_odepthz00_4805;

														{	/* Cfa/pair.scm 229 */
															obj_t BgL_arg1804z00_4806;

															BgL_arg1804z00_4806 = (BgL_oclassz00_4800);
															BgL_odepthz00_4805 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_4806);
														}
														if ((4L < BgL_odepthz00_4805))
															{	/* Cfa/pair.scm 229 */
																obj_t BgL_arg1802z00_4807;

																{	/* Cfa/pair.scm 229 */
																	obj_t BgL_arg1803z00_4808;

																	BgL_arg1803z00_4808 = (BgL_oclassz00_4800);
																	BgL_arg1802z00_4807 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4808,
																		4L);
																}
																BgL_res2066z00_4799 =
																	(BgL_arg1802z00_4807 == BgL_classz00_4794);
															}
														else
															{	/* Cfa/pair.scm 229 */
																BgL_res2066z00_4799 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2136z00_5346 = BgL_res2066z00_4799;
									}
							}
						else
							{	/* Cfa/pair.scm 229 */
								BgL_test2136z00_5346 = ((bool_t) 0);
							}
					}
					if (BgL_test2136z00_5346)
						{	/* Cfa/pair.scm 229 */
							{
								BgL_conszd2appzd2_bglt BgL_auxz00_5369;

								{
									obj_t BgL_auxz00_5370;

									{	/* Cfa/pair.scm 231 */
										BgL_objectz00_bglt BgL_tmpz00_5371;

										BgL_tmpz00_5371 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) BgL_appz00_4742));
										BgL_auxz00_5370 = BGL_OBJECT_WIDENING(BgL_tmpz00_5371);
									}
									BgL_auxz00_5369 = ((BgL_conszd2appzd2_bglt) BgL_auxz00_5370);
								}
								((((BgL_conszd2appzd2_bglt) COBJECT(BgL_auxz00_5369))->
										BgL_seenzf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
							}
							{	/* Cfa/pair.scm 232 */
								obj_t BgL_arg1831z00_4809;

								{	/* Cfa/pair.scm 232 */
									obj_t BgL_fun1833z00_4810;

									{
										BgL_conszd2setz12zd2appz12_bglt BgL_auxz00_5377;

										{
											obj_t BgL_auxz00_5378;

											{	/* Cfa/pair.scm 232 */
												BgL_objectz00_bglt BgL_tmpz00_5379;

												BgL_tmpz00_5379 =
													((BgL_objectz00_bglt) BgL_i1194z00_4740);
												BgL_auxz00_5378 = BGL_OBJECT_WIDENING(BgL_tmpz00_5379);
											}
											BgL_auxz00_5377 =
												((BgL_conszd2setz12zd2appz12_bglt) BgL_auxz00_5378);
										}
										BgL_fun1833z00_4810 =
											(((BgL_conszd2setz12zd2appz12_bglt)
												COBJECT(BgL_auxz00_5377))->BgL_getz00);
									}
									{	/* Cfa/pair.scm 232 */
										obj_t BgL_arg1832z00_4811;

										{
											BgL_conszd2appzd2_bglt BgL_auxz00_5384;

											{
												obj_t BgL_auxz00_5385;

												{	/* Cfa/pair.scm 232 */
													BgL_objectz00_bglt BgL_tmpz00_5386;

													BgL_tmpz00_5386 =
														((BgL_objectz00_bglt)
														((BgL_appz00_bglt) BgL_appz00_4742));
													BgL_auxz00_5385 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_5386);
												}
												BgL_auxz00_5384 =
													((BgL_conszd2appzd2_bglt) BgL_auxz00_5385);
											}
											BgL_arg1832z00_4811 =
												(((BgL_conszd2appzd2_bglt) COBJECT(BgL_auxz00_5384))->
												BgL_approxesz00);
										}
										BgL_arg1831z00_4809 =
											BGL_PROCEDURE_CALL1(BgL_fun1833z00_4810,
											BgL_arg1832z00_4811);
									}
								}
								return
									((obj_t)
									BGl_unionzd2approxz12zc0zzcfa_approxz00(
										((BgL_approxz00_bglt) BgL_arg1831z00_4809),
										BgL_valzd2approxzd2_4741));
							}
						}
					else
						{	/* Cfa/pair.scm 229 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &cfa!-cons-ref-app1584 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2conszd2refzd2app1584za2zzcfa_pairz00(obj_t
		BgL_envz00_4743, obj_t BgL_nodez00_4744)
	{
		{	/* Cfa/pair.scm 183 */
			{	/* Cfa/pair.scm 185 */
				BgL_approxz00_bglt BgL_conszd2approxzd2_4813;

				{	/* Cfa/pair.scm 185 */
					obj_t BgL_arg1806z00_4814;

					{	/* Cfa/pair.scm 185 */
						obj_t BgL_pairz00_4815;

						BgL_pairz00_4815 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt)
										((BgL_appz00_bglt) BgL_nodez00_4744))))->BgL_argsz00);
						BgL_arg1806z00_4814 = CAR(BgL_pairz00_4815);
					}
					BgL_conszd2approxzd2_4813 =
						BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1806z00_4814));
				}
				if (
					(((BgL_approxz00_bglt) COBJECT(BgL_conszd2approxzd2_4813))->
						BgL_topzf3zf3))
					{	/* Cfa/pair.scm 191 */
						{	/* Cfa/pair.scm 192 */
							BgL_approxz00_bglt BgL_arg1761z00_4816;

							{
								BgL_conszd2refzd2appz00_bglt BgL_auxz00_5407;

								{
									obj_t BgL_auxz00_5408;

									{	/* Cfa/pair.scm 192 */
										BgL_objectz00_bglt BgL_tmpz00_5409;

										BgL_tmpz00_5409 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) BgL_nodez00_4744));
										BgL_auxz00_5408 = BGL_OBJECT_WIDENING(BgL_tmpz00_5409);
									}
									BgL_auxz00_5407 =
										((BgL_conszd2refzd2appz00_bglt) BgL_auxz00_5408);
								}
								BgL_arg1761z00_4816 =
									(((BgL_conszd2refzd2appz00_bglt) COBJECT(BgL_auxz00_5407))->
									BgL_approxz00);
							}
							BGl_approxzd2setzd2typez12z12zzcfa_approxz00(BgL_arg1761z00_4816,
								((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00));
						}
						{	/* Cfa/pair.scm 193 */
							BgL_approxz00_bglt BgL_arg1762z00_4817;

							{
								BgL_conszd2refzd2appz00_bglt BgL_auxz00_5417;

								{
									obj_t BgL_auxz00_5418;

									{	/* Cfa/pair.scm 193 */
										BgL_objectz00_bglt BgL_tmpz00_5419;

										BgL_tmpz00_5419 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) BgL_nodez00_4744));
										BgL_auxz00_5418 = BGL_OBJECT_WIDENING(BgL_tmpz00_5419);
									}
									BgL_auxz00_5417 =
										((BgL_conszd2refzd2appz00_bglt) BgL_auxz00_5418);
								}
								BgL_arg1762z00_4817 =
									(((BgL_conszd2refzd2appz00_bglt) COBJECT(BgL_auxz00_5417))->
									BgL_approxz00);
							}
							BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_arg1762z00_4817);
						}
					}
				else
					{	/* Cfa/pair.scm 191 */
						BFALSE;
					}
				{	/* Cfa/pair.scm 197 */
					obj_t BgL_zc3z04anonymousza31766ze3z87_4818;

					BgL_zc3z04anonymousza31766ze3z87_4818 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31766ze3ze5zzcfa_pairz00,
						(int) (1L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31766ze3z87_4818,
						(int) (0L), ((obj_t) ((BgL_appz00_bglt) BgL_nodez00_4744)));
					BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00
						(BgL_zc3z04anonymousza31766ze3z87_4818, BgL_conszd2approxzd2_4813);
			}}
			{
				BgL_conszd2refzd2appz00_bglt BgL_auxz00_5434;

				{
					obj_t BgL_auxz00_5435;

					{	/* Cfa/pair.scm 207 */
						BgL_objectz00_bglt BgL_tmpz00_5436;

						BgL_tmpz00_5436 =
							((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4744));
						BgL_auxz00_5435 = BGL_OBJECT_WIDENING(BgL_tmpz00_5436);
					}
					BgL_auxz00_5434 = ((BgL_conszd2refzd2appz00_bglt) BgL_auxz00_5435);
				}
				return
					(((BgL_conszd2refzd2appz00_bglt) COBJECT(BgL_auxz00_5434))->
					BgL_approxz00);
			}
		}

	}



/* &<@anonymous:1766> */
	obj_t BGl_z62zc3z04anonymousza31766ze3ze5zzcfa_pairz00(obj_t BgL_envz00_4745,
		obj_t BgL_appz00_4747)
	{
		{	/* Cfa/pair.scm 196 */
			{	/* Cfa/pair.scm 197 */
				BgL_appz00_bglt BgL_i1192z00_4746;

				BgL_i1192z00_4746 =
					((BgL_appz00_bglt) PROCEDURE_REF(BgL_envz00_4745, (int) (0L)));
				{	/* Cfa/pair.scm 197 */
					bool_t BgL_test2142z00_5445;

					{	/* Cfa/pair.scm 197 */
						obj_t BgL_classz00_4819;

						BgL_classz00_4819 = BGl_conszd2appzd2zzcfa_info2z00;
						if (BGL_OBJECTP(BgL_appz00_4747))
							{	/* Cfa/pair.scm 197 */
								BgL_objectz00_bglt BgL_arg1807z00_4820;

								BgL_arg1807z00_4820 = (BgL_objectz00_bglt) (BgL_appz00_4747);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cfa/pair.scm 197 */
										long BgL_idxz00_4821;

										BgL_idxz00_4821 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4820);
										BgL_test2142z00_5445 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_4821 + 4L)) == BgL_classz00_4819);
									}
								else
									{	/* Cfa/pair.scm 197 */
										bool_t BgL_res2065z00_4824;

										{	/* Cfa/pair.scm 197 */
											obj_t BgL_oclassz00_4825;

											{	/* Cfa/pair.scm 197 */
												obj_t BgL_arg1815z00_4826;
												long BgL_arg1816z00_4827;

												BgL_arg1815z00_4826 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cfa/pair.scm 197 */
													long BgL_arg1817z00_4828;

													BgL_arg1817z00_4828 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4820);
													BgL_arg1816z00_4827 =
														(BgL_arg1817z00_4828 - OBJECT_TYPE);
												}
												BgL_oclassz00_4825 =
													VECTOR_REF(BgL_arg1815z00_4826, BgL_arg1816z00_4827);
											}
											{	/* Cfa/pair.scm 197 */
												bool_t BgL__ortest_1115z00_4829;

												BgL__ortest_1115z00_4829 =
													(BgL_classz00_4819 == BgL_oclassz00_4825);
												if (BgL__ortest_1115z00_4829)
													{	/* Cfa/pair.scm 197 */
														BgL_res2065z00_4824 = BgL__ortest_1115z00_4829;
													}
												else
													{	/* Cfa/pair.scm 197 */
														long BgL_odepthz00_4830;

														{	/* Cfa/pair.scm 197 */
															obj_t BgL_arg1804z00_4831;

															BgL_arg1804z00_4831 = (BgL_oclassz00_4825);
															BgL_odepthz00_4830 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_4831);
														}
														if ((4L < BgL_odepthz00_4830))
															{	/* Cfa/pair.scm 197 */
																obj_t BgL_arg1802z00_4832;

																{	/* Cfa/pair.scm 197 */
																	obj_t BgL_arg1803z00_4833;

																	BgL_arg1803z00_4833 = (BgL_oclassz00_4825);
																	BgL_arg1802z00_4832 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4833,
																		4L);
																}
																BgL_res2065z00_4824 =
																	(BgL_arg1802z00_4832 == BgL_classz00_4819);
															}
														else
															{	/* Cfa/pair.scm 197 */
																BgL_res2065z00_4824 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2142z00_5445 = BgL_res2065z00_4824;
									}
							}
						else
							{	/* Cfa/pair.scm 197 */
								BgL_test2142z00_5445 = ((bool_t) 0);
							}
					}
					if (BgL_test2142z00_5445)
						{	/* Cfa/pair.scm 197 */
							{
								BgL_conszd2appzd2_bglt BgL_auxz00_5468;

								{
									obj_t BgL_auxz00_5469;

									{	/* Cfa/pair.scm 199 */
										BgL_objectz00_bglt BgL_tmpz00_5470;

										BgL_tmpz00_5470 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) BgL_appz00_4747));
										BgL_auxz00_5469 = BGL_OBJECT_WIDENING(BgL_tmpz00_5470);
									}
									BgL_auxz00_5468 = ((BgL_conszd2appzd2_bglt) BgL_auxz00_5469);
								}
								((((BgL_conszd2appzd2_bglt) COBJECT(BgL_auxz00_5468))->
										BgL_seenzf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
							}
							{	/* Cfa/pair.scm 202 */
								BgL_approxz00_bglt BgL_arg1770z00_4834;
								obj_t BgL_arg1771z00_4835;

								{
									BgL_conszd2refzd2appz00_bglt BgL_auxz00_5476;

									{
										obj_t BgL_auxz00_5477;

										{	/* Cfa/pair.scm 202 */
											BgL_objectz00_bglt BgL_tmpz00_5478;

											BgL_tmpz00_5478 =
												((BgL_objectz00_bglt) BgL_i1192z00_4746);
											BgL_auxz00_5477 = BGL_OBJECT_WIDENING(BgL_tmpz00_5478);
										}
										BgL_auxz00_5476 =
											((BgL_conszd2refzd2appz00_bglt) BgL_auxz00_5477);
									}
									BgL_arg1770z00_4834 =
										(((BgL_conszd2refzd2appz00_bglt) COBJECT(BgL_auxz00_5476))->
										BgL_approxz00);
								}
								{	/* Cfa/pair.scm 202 */
									obj_t BgL_fun1774z00_4836;

									{
										BgL_conszd2refzd2appz00_bglt BgL_auxz00_5483;

										{
											obj_t BgL_auxz00_5484;

											{	/* Cfa/pair.scm 202 */
												BgL_objectz00_bglt BgL_tmpz00_5485;

												BgL_tmpz00_5485 =
													((BgL_objectz00_bglt) BgL_i1192z00_4746);
												BgL_auxz00_5484 = BGL_OBJECT_WIDENING(BgL_tmpz00_5485);
											}
											BgL_auxz00_5483 =
												((BgL_conszd2refzd2appz00_bglt) BgL_auxz00_5484);
										}
										BgL_fun1774z00_4836 =
											(((BgL_conszd2refzd2appz00_bglt)
												COBJECT(BgL_auxz00_5483))->BgL_getz00);
									}
									{	/* Cfa/pair.scm 202 */
										obj_t BgL_arg1773z00_4837;

										{
											BgL_conszd2appzd2_bglt BgL_auxz00_5490;

											{
												obj_t BgL_auxz00_5491;

												{	/* Cfa/pair.scm 202 */
													BgL_objectz00_bglt BgL_tmpz00_5492;

													BgL_tmpz00_5492 =
														((BgL_objectz00_bglt)
														((BgL_appz00_bglt) BgL_appz00_4747));
													BgL_auxz00_5491 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_5492);
												}
												BgL_auxz00_5490 =
													((BgL_conszd2appzd2_bglt) BgL_auxz00_5491);
											}
											BgL_arg1773z00_4837 =
												(((BgL_conszd2appzd2_bglt) COBJECT(BgL_auxz00_5490))->
												BgL_approxesz00);
										}
										BgL_arg1771z00_4835 =
											BGL_PROCEDURE_CALL1(BgL_fun1774z00_4836,
											BgL_arg1773z00_4837);
									}
								}
								BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_arg1770z00_4834,
									((BgL_approxz00_bglt) BgL_arg1771z00_4835));
							}
							{	/* Cfa/pair.scm 203 */
								obj_t BgL_arg1775z00_4838;
								BgL_typez00_bglt BgL_arg1798z00_4839;

								{	/* Cfa/pair.scm 203 */
									obj_t BgL_fun1800z00_4840;

									{
										BgL_conszd2refzd2appz00_bglt BgL_auxz00_5504;

										{
											obj_t BgL_auxz00_5505;

											{	/* Cfa/pair.scm 203 */
												BgL_objectz00_bglt BgL_tmpz00_5506;

												BgL_tmpz00_5506 =
													((BgL_objectz00_bglt) BgL_i1192z00_4746);
												BgL_auxz00_5505 = BGL_OBJECT_WIDENING(BgL_tmpz00_5506);
											}
											BgL_auxz00_5504 =
												((BgL_conszd2refzd2appz00_bglt) BgL_auxz00_5505);
										}
										BgL_fun1800z00_4840 =
											(((BgL_conszd2refzd2appz00_bglt)
												COBJECT(BgL_auxz00_5504))->BgL_getz00);
									}
									{	/* Cfa/pair.scm 203 */
										obj_t BgL_arg1799z00_4841;

										{
											BgL_conszd2appzd2_bglt BgL_auxz00_5511;

											{
												obj_t BgL_auxz00_5512;

												{	/* Cfa/pair.scm 203 */
													BgL_objectz00_bglt BgL_tmpz00_5513;

													BgL_tmpz00_5513 =
														((BgL_objectz00_bglt)
														((BgL_appz00_bglt) BgL_appz00_4747));
													BgL_auxz00_5512 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_5513);
												}
												BgL_auxz00_5511 =
													((BgL_conszd2appzd2_bglt) BgL_auxz00_5512);
											}
											BgL_arg1799z00_4841 =
												(((BgL_conszd2appzd2_bglt) COBJECT(BgL_auxz00_5511))->
												BgL_approxesz00);
										}
										BgL_arg1775z00_4838 =
											BGL_PROCEDURE_CALL1(BgL_fun1800z00_4840,
											BgL_arg1799z00_4841);
									}
								}
								{	/* Cfa/pair.scm 203 */
									BgL_approxz00_bglt BgL_arg1805z00_4842;

									{
										BgL_conszd2refzd2appz00_bglt BgL_auxz00_5523;

										{
											obj_t BgL_auxz00_5524;

											{	/* Cfa/pair.scm 203 */
												BgL_objectz00_bglt BgL_tmpz00_5525;

												BgL_tmpz00_5525 =
													((BgL_objectz00_bglt) BgL_i1192z00_4746);
												BgL_auxz00_5524 = BGL_OBJECT_WIDENING(BgL_tmpz00_5525);
											}
											BgL_auxz00_5523 =
												((BgL_conszd2refzd2appz00_bglt) BgL_auxz00_5524);
										}
										BgL_arg1805z00_4842 =
											(((BgL_conszd2refzd2appz00_bglt)
												COBJECT(BgL_auxz00_5523))->BgL_approxz00);
									}
									BgL_arg1798z00_4839 =
										(((BgL_approxz00_bglt) COBJECT(BgL_arg1805z00_4842))->
										BgL_typez00);
								}
								return
									BGl_approxzd2setzd2typez12z12zzcfa_approxz00(
									((BgL_approxz00_bglt) BgL_arg1775z00_4838),
									BgL_arg1798z00_4839);
							}
						}
					else
						{	/* Cfa/pair.scm 197 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &cfa!-cons-app1582 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2conszd2app1582z70zzcfa_pairz00(obj_t
		BgL_envz00_4748, obj_t BgL_nodez00_4749)
	{
		{	/* Cfa/pair.scm 159 */
			{	/* Cfa/pair.scm 162 */
				BgL_approxz00_bglt BgL_caraz00_4844;
				BgL_approxz00_bglt BgL_cdraz00_4845;

				{	/* Cfa/pair.scm 162 */
					obj_t BgL_arg1752z00_4846;

					{	/* Cfa/pair.scm 162 */
						obj_t BgL_pairz00_4847;

						BgL_pairz00_4847 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt)
										((BgL_appz00_bglt) BgL_nodez00_4749))))->BgL_argsz00);
						BgL_arg1752z00_4846 = CAR(BgL_pairz00_4847);
					}
					BgL_caraz00_4844 =
						BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1752z00_4846));
				}
				{	/* Cfa/pair.scm 163 */
					obj_t BgL_arg1754z00_4848;

					{	/* Cfa/pair.scm 163 */
						obj_t BgL_pairz00_4849;

						BgL_pairz00_4849 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt)
										((BgL_appz00_bglt) BgL_nodez00_4749))))->BgL_argsz00);
						BgL_arg1754z00_4848 = CAR(CDR(BgL_pairz00_4849));
					}
					BgL_cdraz00_4845 =
						BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1754z00_4848));
				}
				{	/* Cfa/pair.scm 164 */
					obj_t BgL_arg1722z00_4850;

					{	/* Cfa/pair.scm 164 */
						obj_t BgL_arg1724z00_4851;

						{
							BgL_conszd2appzd2_bglt BgL_auxz00_5546;

							{
								obj_t BgL_auxz00_5547;

								{	/* Cfa/pair.scm 164 */
									BgL_objectz00_bglt BgL_tmpz00_5548;

									BgL_tmpz00_5548 =
										((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4749));
									BgL_auxz00_5547 = BGL_OBJECT_WIDENING(BgL_tmpz00_5548);
								}
								BgL_auxz00_5546 = ((BgL_conszd2appzd2_bglt) BgL_auxz00_5547);
							}
							BgL_arg1724z00_4851 =
								(((BgL_conszd2appzd2_bglt) COBJECT(BgL_auxz00_5546))->
								BgL_approxesz00);
						}
						BgL_arg1722z00_4850 = CAR(BgL_arg1724z00_4851);
					}
					BGl_unionzd2approxz12zc0zzcfa_approxz00(
						((BgL_approxz00_bglt) BgL_arg1722z00_4850), BgL_caraz00_4844);
				}
				{	/* Cfa/pair.scm 165 */
					obj_t BgL_arg1733z00_4852;

					{	/* Cfa/pair.scm 165 */
						obj_t BgL_arg1734z00_4853;

						{
							BgL_conszd2appzd2_bglt BgL_auxz00_5557;

							{
								obj_t BgL_auxz00_5558;

								{	/* Cfa/pair.scm 165 */
									BgL_objectz00_bglt BgL_tmpz00_5559;

									BgL_tmpz00_5559 =
										((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4749));
									BgL_auxz00_5558 = BGL_OBJECT_WIDENING(BgL_tmpz00_5559);
								}
								BgL_auxz00_5557 = ((BgL_conszd2appzd2_bglt) BgL_auxz00_5558);
							}
							BgL_arg1734z00_4853 =
								(((BgL_conszd2appzd2_bglt) COBJECT(BgL_auxz00_5557))->
								BgL_approxesz00);
						}
						BgL_arg1733z00_4852 = CDR(BgL_arg1734z00_4853);
					}
					BGl_unionzd2approxz12zc0zzcfa_approxz00(
						((BgL_approxz00_bglt) BgL_arg1733z00_4852), BgL_cdraz00_4845);
				}
				{	/* Cfa/pair.scm 172 */
					obj_t BgL_arg1735z00_4854;
					BgL_typez00_bglt BgL_arg1736z00_4855;

					{	/* Cfa/pair.scm 172 */
						obj_t BgL_arg1737z00_4856;

						{
							BgL_conszd2appzd2_bglt BgL_auxz00_5568;

							{
								obj_t BgL_auxz00_5569;

								{	/* Cfa/pair.scm 172 */
									BgL_objectz00_bglt BgL_tmpz00_5570;

									BgL_tmpz00_5570 =
										((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4749));
									BgL_auxz00_5569 = BGL_OBJECT_WIDENING(BgL_tmpz00_5570);
								}
								BgL_auxz00_5568 = ((BgL_conszd2appzd2_bglt) BgL_auxz00_5569);
							}
							BgL_arg1737z00_4856 =
								(((BgL_conszd2appzd2_bglt) COBJECT(BgL_auxz00_5568))->
								BgL_approxesz00);
						}
						BgL_arg1735z00_4854 = CAR(BgL_arg1737z00_4856);
					}
					{	/* Cfa/pair.scm 173 */
						BgL_typez00_bglt BgL_arg1738z00_4857;

						{	/* Cfa/pair.scm 173 */
							obj_t BgL_arg1739z00_4858;

							{	/* Cfa/pair.scm 173 */
								obj_t BgL_arg1740z00_4859;

								{
									BgL_conszd2appzd2_bglt BgL_auxz00_5577;

									{
										obj_t BgL_auxz00_5578;

										{	/* Cfa/pair.scm 173 */
											BgL_objectz00_bglt BgL_tmpz00_5579;

											BgL_tmpz00_5579 =
												((BgL_objectz00_bglt)
												((BgL_appz00_bglt) BgL_nodez00_4749));
											BgL_auxz00_5578 = BGL_OBJECT_WIDENING(BgL_tmpz00_5579);
										}
										BgL_auxz00_5577 =
											((BgL_conszd2appzd2_bglt) BgL_auxz00_5578);
									}
									BgL_arg1740z00_4859 =
										(((BgL_conszd2appzd2_bglt) COBJECT(BgL_auxz00_5577))->
										BgL_approxesz00);
								}
								BgL_arg1739z00_4858 = CAR(BgL_arg1740z00_4859);
							}
							BgL_arg1738z00_4857 =
								(((BgL_approxz00_bglt) COBJECT(
										((BgL_approxz00_bglt) BgL_arg1739z00_4858)))->BgL_typez00);
						}
						BgL_arg1736z00_4855 =
							BGl_getzd2bigloozd2typez00zztype_cachez00(BgL_arg1738z00_4857);
					}
					BGl_approxzd2setzd2typez12z12zzcfa_approxz00(
						((BgL_approxz00_bglt) BgL_arg1735z00_4854), BgL_arg1736z00_4855);
				}
				{	/* Cfa/pair.scm 174 */
					obj_t BgL_arg1746z00_4860;
					BgL_typez00_bglt BgL_arg1747z00_4861;

					{	/* Cfa/pair.scm 174 */
						obj_t BgL_arg1748z00_4862;

						{
							BgL_conszd2appzd2_bglt BgL_auxz00_5591;

							{
								obj_t BgL_auxz00_5592;

								{	/* Cfa/pair.scm 174 */
									BgL_objectz00_bglt BgL_tmpz00_5593;

									BgL_tmpz00_5593 =
										((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4749));
									BgL_auxz00_5592 = BGL_OBJECT_WIDENING(BgL_tmpz00_5593);
								}
								BgL_auxz00_5591 = ((BgL_conszd2appzd2_bglt) BgL_auxz00_5592);
							}
							BgL_arg1748z00_4862 =
								(((BgL_conszd2appzd2_bglt) COBJECT(BgL_auxz00_5591))->
								BgL_approxesz00);
						}
						BgL_arg1746z00_4860 = CDR(BgL_arg1748z00_4862);
					}
					{	/* Cfa/pair.scm 175 */
						BgL_typez00_bglt BgL_arg1749z00_4863;

						{	/* Cfa/pair.scm 175 */
							obj_t BgL_arg1750z00_4864;

							{	/* Cfa/pair.scm 175 */
								obj_t BgL_arg1751z00_4865;

								{
									BgL_conszd2appzd2_bglt BgL_auxz00_5600;

									{
										obj_t BgL_auxz00_5601;

										{	/* Cfa/pair.scm 175 */
											BgL_objectz00_bglt BgL_tmpz00_5602;

											BgL_tmpz00_5602 =
												((BgL_objectz00_bglt)
												((BgL_appz00_bglt) BgL_nodez00_4749));
											BgL_auxz00_5601 = BGL_OBJECT_WIDENING(BgL_tmpz00_5602);
										}
										BgL_auxz00_5600 =
											((BgL_conszd2appzd2_bglt) BgL_auxz00_5601);
									}
									BgL_arg1751z00_4865 =
										(((BgL_conszd2appzd2_bglt) COBJECT(BgL_auxz00_5600))->
										BgL_approxesz00);
								}
								BgL_arg1750z00_4864 = CDR(BgL_arg1751z00_4865);
							}
							BgL_arg1749z00_4863 =
								(((BgL_approxz00_bglt) COBJECT(
										((BgL_approxz00_bglt) BgL_arg1750z00_4864)))->BgL_typez00);
						}
						BgL_arg1747z00_4861 =
							BGl_getzd2bigloozd2typez00zztype_cachez00(BgL_arg1749z00_4863);
					}
					BGl_approxzd2setzd2typez12z12zzcfa_approxz00(
						((BgL_approxz00_bglt) BgL_arg1746z00_4860), BgL_arg1747z00_4861);
				}
				{
					BgL_conszd2appzd2_bglt BgL_auxz00_5614;

					{
						obj_t BgL_auxz00_5615;

						{	/* Cfa/pair.scm 178 */
							BgL_objectz00_bglt BgL_tmpz00_5616;

							BgL_tmpz00_5616 =
								((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4749));
							BgL_auxz00_5615 = BGL_OBJECT_WIDENING(BgL_tmpz00_5616);
						}
						BgL_auxz00_5614 = ((BgL_conszd2appzd2_bglt) BgL_auxz00_5615);
					}
					return
						(((BgL_conszd2appzd2_bglt) COBJECT(BgL_auxz00_5614))->
						BgL_approxz00);
				}
			}
		}

	}



/* &node-setup!-pre-cons1580 */
	obj_t BGl_z62nodezd2setupz12zd2prezd2cons1580za2zzcfa_pairz00(obj_t
		BgL_envz00_4750, obj_t BgL_nodez00_4751)
	{
		{	/* Cfa/pair.scm 147 */
			{	/* Cfa/pair.scm 149 */
				obj_t BgL_arg1711z00_4867;

				BgL_arg1711z00_4867 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt)
								((BgL_appz00_bglt) BgL_nodez00_4751))))->BgL_argsz00);
				BGl_nodezd2setupza2z12z62zzcfa_setupz00(BgL_arg1711z00_4867);
			}
			{	/* Cfa/pair.scm 150 */
				obj_t BgL_cgetz00_4868;

				{
					BgL_prezd2conszd2setz12zd2appzc0_bglt BgL_auxz00_5626;

					{
						obj_t BgL_auxz00_5627;

						{	/* Cfa/pair.scm 150 */
							BgL_objectz00_bglt BgL_tmpz00_5628;

							BgL_tmpz00_5628 =
								((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4751));
							BgL_auxz00_5627 = BGL_OBJECT_WIDENING(BgL_tmpz00_5628);
						}
						BgL_auxz00_5626 =
							((BgL_prezd2conszd2setz12zd2appzc0_bglt) BgL_auxz00_5627);
					}
					BgL_cgetz00_4868 =
						(((BgL_prezd2conszd2setz12zd2appzc0_bglt)
							COBJECT(BgL_auxz00_5626))->BgL_getz00);
				}
				{	/* Cfa/pair.scm 150 */
					BgL_appz00_bglt BgL_nodez00_4869;

					{	/* Cfa/pair.scm 151 */
						long BgL_arg1717z00_4870;

						{	/* Cfa/pair.scm 151 */
							obj_t BgL_arg1718z00_4871;

							{	/* Cfa/pair.scm 151 */
								obj_t BgL_arg1720z00_4872;

								{	/* Cfa/pair.scm 151 */
									obj_t BgL_arg1815z00_4873;
									long BgL_arg1816z00_4874;

									BgL_arg1815z00_4873 = (BGl_za2classesza2z00zz__objectz00);
									{	/* Cfa/pair.scm 151 */
										long BgL_arg1817z00_4875;

										BgL_arg1817z00_4875 =
											BGL_OBJECT_CLASS_NUM(
											((BgL_objectz00_bglt)
												((BgL_appz00_bglt) BgL_nodez00_4751)));
										BgL_arg1816z00_4874 = (BgL_arg1817z00_4875 - OBJECT_TYPE);
									}
									BgL_arg1720z00_4872 =
										VECTOR_REF(BgL_arg1815z00_4873, BgL_arg1816z00_4874);
								}
								BgL_arg1718z00_4871 =
									BGl_classzd2superzd2zz__objectz00(BgL_arg1720z00_4872);
							}
							{	/* Cfa/pair.scm 151 */
								obj_t BgL_tmpz00_5641;

								BgL_tmpz00_5641 = ((obj_t) BgL_arg1718z00_4871);
								BgL_arg1717z00_4870 = BGL_CLASS_NUM(BgL_tmpz00_5641);
						}}
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt)
								((BgL_appz00_bglt) BgL_nodez00_4751)), BgL_arg1717z00_4870);
					}
					{	/* Cfa/pair.scm 151 */
						BgL_objectz00_bglt BgL_tmpz00_5647;

						BgL_tmpz00_5647 =
							((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4751));
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5647, BFALSE);
					}
					((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4751));
					BgL_nodez00_4869 = ((BgL_appz00_bglt) BgL_nodez00_4751);
					{	/* Cfa/pair.scm 151 */

						{	/* Cfa/pair.scm 152 */
							BgL_conszd2setz12zd2appz12_bglt BgL_wide1189z00_4876;

							BgL_wide1189z00_4876 =
								((BgL_conszd2setz12zd2appz12_bglt)
								BOBJECT(GC_MALLOC(sizeof(struct
											BgL_conszd2setz12zd2appz12_bgl))));
							{	/* Cfa/pair.scm 152 */
								obj_t BgL_auxz00_5659;
								BgL_objectz00_bglt BgL_tmpz00_5655;

								BgL_auxz00_5659 = ((obj_t) BgL_wide1189z00_4876);
								BgL_tmpz00_5655 =
									((BgL_objectz00_bglt)
									((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4869)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5655, BgL_auxz00_5659);
							}
							((BgL_objectz00_bglt)
								((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4869)));
							{	/* Cfa/pair.scm 152 */
								long BgL_arg1714z00_4877;

								BgL_arg1714z00_4877 =
									BGL_CLASS_NUM(BGl_conszd2setz12zd2appz12zzcfa_info2z00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_appz00_bglt)
											((BgL_appz00_bglt) BgL_nodez00_4869))),
									BgL_arg1714z00_4877);
							}
							((BgL_appz00_bglt)
								((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4869)));
						}
						{
							BgL_conszd2setz12zd2appz12_bglt BgL_auxz00_5673;

							{
								obj_t BgL_auxz00_5674;

								{	/* Cfa/pair.scm 154 */
									BgL_objectz00_bglt BgL_tmpz00_5675;

									BgL_tmpz00_5675 =
										((BgL_objectz00_bglt)
										((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4869)));
									BgL_auxz00_5674 = BGL_OBJECT_WIDENING(BgL_tmpz00_5675);
								}
								BgL_auxz00_5673 =
									((BgL_conszd2setz12zd2appz12_bglt) BgL_auxz00_5674);
							}
							((((BgL_conszd2setz12zd2appz12_bglt) COBJECT(BgL_auxz00_5673))->
									BgL_approxz00) =
								((BgL_approxz00_bglt)
									BGl_makezd2typezd2approxz00zzcfa_approxz00(((BgL_typez00_bglt)
											BGl_za2unspecza2z00zztype_cachez00))), BUNSPEC);
						}
						{
							BgL_conszd2setz12zd2appz12_bglt BgL_auxz00_5684;

							{
								obj_t BgL_auxz00_5685;

								{	/* Cfa/pair.scm 153 */
									BgL_objectz00_bglt BgL_tmpz00_5686;

									BgL_tmpz00_5686 =
										((BgL_objectz00_bglt)
										((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4869)));
									BgL_auxz00_5685 = BGL_OBJECT_WIDENING(BgL_tmpz00_5686);
								}
								BgL_auxz00_5684 =
									((BgL_conszd2setz12zd2appz12_bglt) BgL_auxz00_5685);
							}
							((((BgL_conszd2setz12zd2appz12_bglt) COBJECT(BgL_auxz00_5684))->
									BgL_getz00) = ((obj_t) BgL_cgetz00_4868), BUNSPEC);
						}
						return
							((obj_t)
							((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4869)));
					}
				}
			}
		}

	}



/* &node-setup!-pre-cons1578 */
	obj_t BGl_z62nodezd2setupz12zd2prezd2cons1578za2zzcfa_pairz00(obj_t
		BgL_envz00_4752, obj_t BgL_nodez00_4753)
	{
		{	/* Cfa/pair.scm 134 */
			{	/* Tools/trace.sch 53 */
				obj_t BgL_arg1703z00_4879;

				BgL_arg1703z00_4879 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt)
								((BgL_appz00_bglt) BgL_nodez00_4753))))->BgL_argsz00);
				BGl_nodezd2setupza2z12z62zzcfa_setupz00(BgL_arg1703z00_4879);
			}
			{	/* Tools/trace.sch 53 */
				obj_t BgL_cgetz00_4880;

				{
					BgL_prezd2conszd2refzd2appzd2_bglt BgL_auxz00_5700;

					{
						obj_t BgL_auxz00_5701;

						{	/* Tools/trace.sch 53 */
							BgL_objectz00_bglt BgL_tmpz00_5702;

							BgL_tmpz00_5702 =
								((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4753));
							BgL_auxz00_5701 = BGL_OBJECT_WIDENING(BgL_tmpz00_5702);
						}
						BgL_auxz00_5700 =
							((BgL_prezd2conszd2refzd2appzd2_bglt) BgL_auxz00_5701);
					}
					BgL_cgetz00_4880 =
						(((BgL_prezd2conszd2refzd2appzd2_bglt) COBJECT(BgL_auxz00_5700))->
						BgL_getz00);
				}
				{	/* Tools/trace.sch 53 */
					BgL_appz00_bglt BgL_nodez00_4881;

					{	/* Tools/trace.sch 53 */
						long BgL_arg1708z00_4882;

						{	/* Tools/trace.sch 53 */
							obj_t BgL_arg1709z00_4883;

							{	/* Tools/trace.sch 53 */
								obj_t BgL_arg1710z00_4884;

								{	/* Tools/trace.sch 53 */
									obj_t BgL_arg1815z00_4885;
									long BgL_arg1816z00_4886;

									BgL_arg1815z00_4885 = (BGl_za2classesza2z00zz__objectz00);
									{	/* Tools/trace.sch 53 */
										long BgL_arg1817z00_4887;

										BgL_arg1817z00_4887 =
											BGL_OBJECT_CLASS_NUM(
											((BgL_objectz00_bglt)
												((BgL_appz00_bglt) BgL_nodez00_4753)));
										BgL_arg1816z00_4886 = (BgL_arg1817z00_4887 - OBJECT_TYPE);
									}
									BgL_arg1710z00_4884 =
										VECTOR_REF(BgL_arg1815z00_4885, BgL_arg1816z00_4886);
								}
								BgL_arg1709z00_4883 =
									BGl_classzd2superzd2zz__objectz00(BgL_arg1710z00_4884);
							}
							{	/* Tools/trace.sch 53 */
								obj_t BgL_tmpz00_5715;

								BgL_tmpz00_5715 = ((obj_t) BgL_arg1709z00_4883);
								BgL_arg1708z00_4882 = BGL_CLASS_NUM(BgL_tmpz00_5715);
						}}
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt)
								((BgL_appz00_bglt) BgL_nodez00_4753)), BgL_arg1708z00_4882);
					}
					{	/* Tools/trace.sch 53 */
						BgL_objectz00_bglt BgL_tmpz00_5721;

						BgL_tmpz00_5721 =
							((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4753));
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5721, BFALSE);
					}
					((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4753));
					BgL_nodez00_4881 = ((BgL_appz00_bglt) BgL_nodez00_4753);
					{	/* Tools/trace.sch 53 */

						{	/* Tools/trace.sch 53 */
							BgL_conszd2refzd2appz00_bglt BgL_wide1183z00_4888;

							BgL_wide1183z00_4888 =
								((BgL_conszd2refzd2appz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_conszd2refzd2appz00_bgl))));
							{	/* Tools/trace.sch 53 */
								obj_t BgL_auxz00_5733;
								BgL_objectz00_bglt BgL_tmpz00_5729;

								BgL_auxz00_5733 = ((obj_t) BgL_wide1183z00_4888);
								BgL_tmpz00_5729 =
									((BgL_objectz00_bglt)
									((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4881)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5729, BgL_auxz00_5733);
							}
							((BgL_objectz00_bglt)
								((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4881)));
							{	/* Tools/trace.sch 53 */
								long BgL_arg1705z00_4889;

								BgL_arg1705z00_4889 =
									BGL_CLASS_NUM(BGl_conszd2refzd2appz00zzcfa_info2z00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_appz00_bglt)
											((BgL_appz00_bglt) BgL_nodez00_4881))),
									BgL_arg1705z00_4889);
							}
							((BgL_appz00_bglt)
								((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4881)));
						}
						{
							BgL_conszd2refzd2appz00_bglt BgL_auxz00_5747;

							{
								obj_t BgL_auxz00_5748;

								{	/* Tools/trace.sch 53 */
									BgL_objectz00_bglt BgL_tmpz00_5749;

									BgL_tmpz00_5749 =
										((BgL_objectz00_bglt)
										((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4881)));
									BgL_auxz00_5748 = BGL_OBJECT_WIDENING(BgL_tmpz00_5749);
								}
								BgL_auxz00_5747 =
									((BgL_conszd2refzd2appz00_bglt) BgL_auxz00_5748);
							}
							((((BgL_conszd2refzd2appz00_bglt) COBJECT(BgL_auxz00_5747))->
									BgL_approxz00) =
								((BgL_approxz00_bglt)
									BGl_makezd2emptyzd2approxz00zzcfa_approxz00()), BUNSPEC);
						}
						{
							BgL_conszd2refzd2appz00_bglt BgL_auxz00_5757;

							{
								obj_t BgL_auxz00_5758;

								{	/* Tools/trace.sch 53 */
									BgL_objectz00_bglt BgL_tmpz00_5759;

									BgL_tmpz00_5759 =
										((BgL_objectz00_bglt)
										((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4881)));
									BgL_auxz00_5758 = BGL_OBJECT_WIDENING(BgL_tmpz00_5759);
								}
								BgL_auxz00_5757 =
									((BgL_conszd2refzd2appz00_bglt) BgL_auxz00_5758);
							}
							((((BgL_conszd2refzd2appz00_bglt) COBJECT(BgL_auxz00_5757))->
									BgL_getz00) = ((obj_t) BgL_cgetz00_4880), BUNSPEC);
						}
						return
							((obj_t)
							((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4881)));
					}
				}
			}
		}

	}



/* &node-setup!-pre-cons1576 */
	obj_t BGl_z62nodezd2setupz12zd2prezd2cons1576za2zzcfa_pairz00(obj_t
		BgL_envz00_4754, obj_t BgL_nodez00_4755)
	{
		{	/* Cfa/pair.scm 119 */
			{	/* Cfa/pair.scm 121 */
				obj_t BgL_arg1688z00_4891;

				BgL_arg1688z00_4891 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt)
								((BgL_appz00_bglt) BgL_nodez00_4755))))->BgL_argsz00);
				BGl_nodezd2setupza2z12z62zzcfa_setupz00(BgL_arg1688z00_4891);
			}
			{	/* Cfa/pair.scm 122 */
				BgL_variablez00_bglt BgL_ownerz00_4892;

				{
					BgL_prezd2conszd2appz00_bglt BgL_auxz00_5773;

					{
						obj_t BgL_auxz00_5774;

						{	/* Cfa/pair.scm 122 */
							BgL_objectz00_bglt BgL_tmpz00_5775;

							BgL_tmpz00_5775 =
								((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4755));
							BgL_auxz00_5774 = BGL_OBJECT_WIDENING(BgL_tmpz00_5775);
						}
						BgL_auxz00_5773 = ((BgL_prezd2conszd2appz00_bglt) BgL_auxz00_5774);
					}
					BgL_ownerz00_4892 =
						(((BgL_prezd2conszd2appz00_bglt) COBJECT(BgL_auxz00_5773))->
						BgL_ownerz00);
				}
				{	/* Cfa/pair.scm 122 */
					BgL_appz00_bglt BgL_nodez00_4893;

					{	/* Cfa/pair.scm 123 */
						long BgL_arg1700z00_4894;

						{	/* Cfa/pair.scm 123 */
							obj_t BgL_arg1701z00_4895;

							{	/* Cfa/pair.scm 123 */
								obj_t BgL_arg1702z00_4896;

								{	/* Cfa/pair.scm 123 */
									obj_t BgL_arg1815z00_4897;
									long BgL_arg1816z00_4898;

									BgL_arg1815z00_4897 = (BGl_za2classesza2z00zz__objectz00);
									{	/* Cfa/pair.scm 123 */
										long BgL_arg1817z00_4899;

										BgL_arg1817z00_4899 =
											BGL_OBJECT_CLASS_NUM(
											((BgL_objectz00_bglt)
												((BgL_appz00_bglt) BgL_nodez00_4755)));
										BgL_arg1816z00_4898 = (BgL_arg1817z00_4899 - OBJECT_TYPE);
									}
									BgL_arg1702z00_4896 =
										VECTOR_REF(BgL_arg1815z00_4897, BgL_arg1816z00_4898);
								}
								BgL_arg1701z00_4895 =
									BGl_classzd2superzd2zz__objectz00(BgL_arg1702z00_4896);
							}
							{	/* Cfa/pair.scm 123 */
								obj_t BgL_tmpz00_5788;

								BgL_tmpz00_5788 = ((obj_t) BgL_arg1701z00_4895);
								BgL_arg1700z00_4894 = BGL_CLASS_NUM(BgL_tmpz00_5788);
						}}
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt)
								((BgL_appz00_bglt) BgL_nodez00_4755)), BgL_arg1700z00_4894);
					}
					{	/* Cfa/pair.scm 123 */
						BgL_objectz00_bglt BgL_tmpz00_5794;

						BgL_tmpz00_5794 =
							((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4755));
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5794, BFALSE);
					}
					((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4755));
					BgL_nodez00_4893 = ((BgL_appz00_bglt) BgL_nodez00_4755);
					{	/* Cfa/pair.scm 123 */
						BgL_appz00_bglt BgL_wnodez00_4900;

						{	/* Cfa/pair.scm 124 */
							BgL_conszd2appzd2_bglt BgL_wide1177z00_4901;

							BgL_wide1177z00_4901 =
								((BgL_conszd2appzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_conszd2appzd2_bgl))));
							{	/* Cfa/pair.scm 124 */
								obj_t BgL_auxz00_5806;
								BgL_objectz00_bglt BgL_tmpz00_5802;

								BgL_auxz00_5806 = ((obj_t) BgL_wide1177z00_4901);
								BgL_tmpz00_5802 =
									((BgL_objectz00_bglt)
									((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4893)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5802, BgL_auxz00_5806);
							}
							((BgL_objectz00_bglt)
								((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4893)));
							{	/* Cfa/pair.scm 124 */
								long BgL_arg1691z00_4902;

								BgL_arg1691z00_4902 =
									BGL_CLASS_NUM(BGl_conszd2appzd2zzcfa_info2z00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_appz00_bglt)
											((BgL_appz00_bglt) BgL_nodez00_4893))),
									BgL_arg1691z00_4902);
							}
							((BgL_appz00_bglt)
								((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4893)));
						}
						{
							BgL_conszd2appzd2_bglt BgL_auxz00_5820;

							{
								obj_t BgL_auxz00_5821;

								{	/* Cfa/pair.scm 127 */
									BgL_objectz00_bglt BgL_tmpz00_5822;

									BgL_tmpz00_5822 =
										((BgL_objectz00_bglt)
										((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4893)));
									BgL_auxz00_5821 = BGL_OBJECT_WIDENING(BgL_tmpz00_5822);
								}
								BgL_auxz00_5820 = ((BgL_conszd2appzd2_bglt) BgL_auxz00_5821);
							}
							((((BgL_conszd2appzd2_bglt) COBJECT(BgL_auxz00_5820))->
									BgL_approxz00) =
								((BgL_approxz00_bglt)
									BGl_makezd2emptyzd2approxz00zzcfa_approxz00()), BUNSPEC);
						}
						{
							obj_t BgL_auxz00_5838;
							BgL_conszd2appzd2_bglt BgL_auxz00_5830;

							{	/* Cfa/pair.scm 126 */
								BgL_approxz00_bglt BgL_arg1692z00_4903;
								BgL_approxz00_bglt BgL_arg1699z00_4904;

								BgL_arg1692z00_4903 =
									BGl_makezd2emptyzd2approxz00zzcfa_approxz00();
								BgL_arg1699z00_4904 =
									BGl_makezd2emptyzd2approxz00zzcfa_approxz00();
								BgL_auxz00_5838 =
									MAKE_YOUNG_PAIR(
									((obj_t) BgL_arg1692z00_4903), ((obj_t) BgL_arg1699z00_4904));
							}
							{
								obj_t BgL_auxz00_5831;

								{	/* Cfa/pair.scm 126 */
									BgL_objectz00_bglt BgL_tmpz00_5832;

									BgL_tmpz00_5832 =
										((BgL_objectz00_bglt)
										((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4893)));
									BgL_auxz00_5831 = BGL_OBJECT_WIDENING(BgL_tmpz00_5832);
								}
								BgL_auxz00_5830 = ((BgL_conszd2appzd2_bglt) BgL_auxz00_5831);
							}
							((((BgL_conszd2appzd2_bglt) COBJECT(BgL_auxz00_5830))->
									BgL_approxesz00) = ((obj_t) BgL_auxz00_5838), BUNSPEC);
						}
						{
							BgL_conszd2appzd2_bglt BgL_auxz00_5845;

							{
								obj_t BgL_auxz00_5846;

								{	/* Cfa/pair.scm 122 */
									BgL_objectz00_bglt BgL_tmpz00_5847;

									BgL_tmpz00_5847 =
										((BgL_objectz00_bglt)
										((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4893)));
									BgL_auxz00_5846 = BGL_OBJECT_WIDENING(BgL_tmpz00_5847);
								}
								BgL_auxz00_5845 = ((BgL_conszd2appzd2_bglt) BgL_auxz00_5846);
							}
							((((BgL_conszd2appzd2_bglt) COBJECT(BgL_auxz00_5845))->
									BgL_lostzd2stampzd2) = ((long) -1L), BUNSPEC);
						}
						{
							BgL_conszd2appzd2_bglt BgL_auxz00_5854;

							{
								obj_t BgL_auxz00_5855;

								{	/* Cfa/pair.scm 125 */
									BgL_objectz00_bglt BgL_tmpz00_5856;

									BgL_tmpz00_5856 =
										((BgL_objectz00_bglt)
										((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4893)));
									BgL_auxz00_5855 = BGL_OBJECT_WIDENING(BgL_tmpz00_5856);
								}
								BgL_auxz00_5854 = ((BgL_conszd2appzd2_bglt) BgL_auxz00_5855);
							}
							((((BgL_conszd2appzd2_bglt) COBJECT(BgL_auxz00_5854))->
									BgL_ownerz00) =
								((BgL_variablez00_bglt) BgL_ownerz00_4892), BUNSPEC);
						}
						{
							BgL_conszd2appzd2_bglt BgL_auxz00_5863;

							{
								obj_t BgL_auxz00_5864;

								{	/* Cfa/pair.scm 122 */
									BgL_objectz00_bglt BgL_tmpz00_5865;

									BgL_tmpz00_5865 =
										((BgL_objectz00_bglt)
										((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4893)));
									BgL_auxz00_5864 = BGL_OBJECT_WIDENING(BgL_tmpz00_5865);
								}
								BgL_auxz00_5863 = ((BgL_conszd2appzd2_bglt) BgL_auxz00_5864);
							}
							((((BgL_conszd2appzd2_bglt) COBJECT(BgL_auxz00_5863))->
									BgL_stackzd2stampzd2) = ((obj_t) BNIL), BUNSPEC);
						}
						{
							BgL_conszd2appzd2_bglt BgL_auxz00_5872;

							{
								obj_t BgL_auxz00_5873;

								{	/* Cfa/pair.scm 122 */
									BgL_objectz00_bglt BgL_tmpz00_5874;

									BgL_tmpz00_5874 =
										((BgL_objectz00_bglt)
										((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4893)));
									BgL_auxz00_5873 = BGL_OBJECT_WIDENING(BgL_tmpz00_5874);
								}
								BgL_auxz00_5872 = ((BgL_conszd2appzd2_bglt) BgL_auxz00_5873);
							}
							((((BgL_conszd2appzd2_bglt) COBJECT(BgL_auxz00_5872))->
									BgL_seenzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
						}
						BgL_wnodez00_4900 =
							((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4893));
						{	/* Cfa/pair.scm 124 */

							{	/* Cfa/pair.scm 129 */
								BgL_approxz00_bglt BgL_arg1689z00_4905;

								BgL_arg1689z00_4905 =
									BGl_makezd2typezd2alloczd2approxzd2zzcfa_approxz00(
									((BgL_typez00_bglt) BGl_za2pairza2z00zztype_cachez00),
									((BgL_nodez00_bglt) BgL_nodez00_4893));
								{
									BgL_conszd2appzd2_bglt BgL_auxz00_5886;

									{
										obj_t BgL_auxz00_5887;

										{	/* Cfa/pair.scm 129 */
											BgL_objectz00_bglt BgL_tmpz00_5888;

											BgL_tmpz00_5888 =
												((BgL_objectz00_bglt) BgL_wnodez00_4900);
											BgL_auxz00_5887 = BGL_OBJECT_WIDENING(BgL_tmpz00_5888);
										}
										BgL_auxz00_5886 =
											((BgL_conszd2appzd2_bglt) BgL_auxz00_5887);
									}
									return
										((((BgL_conszd2appzd2_bglt) COBJECT(BgL_auxz00_5886))->
											BgL_approxz00) =
										((BgL_approxz00_bglt) BgL_arg1689z00_4905), BUNSPEC);
								}
							}
						}
					}
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcfa_pairz00(void)
	{
		{	/* Cfa/pair.scm 17 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2083z00zzcfa_pairz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2083z00zzcfa_pairz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2083z00zzcfa_pairz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2083z00zzcfa_pairz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2083z00zzcfa_pairz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2083z00zzcfa_pairz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2083z00zzcfa_pairz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2083z00zzcfa_pairz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2083z00zzcfa_pairz00));
			BGl_modulezd2initializa7ationz75zzcfa_infoz00(3788352L,
				BSTRING_TO_STRING(BGl_string2083z00zzcfa_pairz00));
			BGl_modulezd2initializa7ationz75zzcfa_info2z00(0L,
				BSTRING_TO_STRING(BGl_string2083z00zzcfa_pairz00));
			BGl_modulezd2initializa7ationz75zzcfa_info3z00(0L,
				BSTRING_TO_STRING(BGl_string2083z00zzcfa_pairz00));
			BGl_modulezd2initializa7ationz75zzcfa_loosez00(471177480L,
				BSTRING_TO_STRING(BGl_string2083z00zzcfa_pairz00));
			BGl_modulezd2initializa7ationz75zzcfa_iteratez00(131412063L,
				BSTRING_TO_STRING(BGl_string2083z00zzcfa_pairz00));
			BGl_modulezd2initializa7ationz75zzcfa_cfaz00(400853817L,
				BSTRING_TO_STRING(BGl_string2083z00zzcfa_pairz00));
			BGl_modulezd2initializa7ationz75zzcfa_setupz00(168272122L,
				BSTRING_TO_STRING(BGl_string2083z00zzcfa_pairz00));
			BGl_modulezd2initializa7ationz75zzcfa_approxz00(285971837L,
				BSTRING_TO_STRING(BGl_string2083z00zzcfa_pairz00));
			return
				BGl_modulezd2initializa7ationz75zzcfa_tvectorz00(324810626L,
				BSTRING_TO_STRING(BGl_string2083z00zzcfa_pairz00));
		}

	}

#ifdef __cplusplus
}
#endif
