/*===========================================================================*/
/*   (Flop/walk.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Flop/walk.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_FLOP_WALK_TYPE_DEFINITIONS
#define BGL_FLOP_WALK_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_literalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}                 *BgL_literalz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;


#endif													// BGL_FLOP_WALK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_za2optimzd2specializa7ezd2flonumzf3za2z54zzengine_paramz00;
	static obj_t BGl_requirezd2initializa7ationz75zzflop_walkz00 = BUNSPEC;
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzflop_walkz00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static obj_t BGl_z62nodezd2varzd2typezd2ref1296zb0zzflop_walkz00(obj_t,
		obj_t);
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzflop_walkz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	extern obj_t BGl_leavezd2functionzd2zztools_errorz00(void);
	static obj_t BGl_za2fixnumzf3za2zf3zzflop_walkz00 = BUNSPEC;
	static BgL_nodez00_bglt BGl_z62flopz12zd2letzd2var1288z70zzflop_walkz00(obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zzflop_walkz00(void);
	extern obj_t BGl_za2boolza2z00zztype_cachez00;
	static obj_t BGl_z62flopz121285z70zzflop_walkz00(obj_t, obj_t);
	static obj_t BGl_nodezd2varzd2typez00zzflop_walkz00(BgL_nodez00_bglt);
	static obj_t BGl_z62clearzd2flopzd2cachez12z70zzflop_walkz00(obj_t);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_za2flopsza2z00zzflop_walkz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_z62nodezd2varzd2type1293z62zzflop_walkz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzflop_walkz00(void);
	static obj_t BGl_za2flonumzf3za2zf3zzflop_walkz00 = BUNSPEC;
	static obj_t BGl_za2czd2fixnumzf3za2z21zzflop_walkz00 = BUNSPEC;
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	extern obj_t BGl_za2realza2z00zztype_cachez00;
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	static bool_t BGl_iszd2booleanzf3z21zzflop_walkz00(BgL_nodez00_bglt, bool_t);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	static bool_t BGl_isnumberzf3zf3zzflop_walkz00(BgL_nodez00_bglt, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzflop_walkz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_effectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_lvtypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_privatez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_enterzd2functionzd2zztools_errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_initzd2flopzd2cachez12z12zzflop_walkz00(void);
	extern obj_t BGl_nodez00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzflop_walkz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzflop_walkz00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	static BgL_nodez00_bglt
		BGl_z62flopz12zd2conditional1292za2zzflop_walkz00(obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzflop_walkz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzflop_walkz00(void);
	extern bool_t BGl_sidezd2effectzf3z21zzeffect_effectz00(BgL_nodez00_bglt);
	extern BgL_nodez00_bglt BGl_walk0z12z12zzast_walkz00(BgL_nodez00_bglt, obj_t);
	static obj_t BGl_za2toflonumza2z00zzflop_walkz00 = BUNSPEC;
	static BgL_nodez00_bglt BGl_z62flopz12zd2app1290za2zzflop_walkz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_flopzd2walkz12zc0zzflop_walkz00(obj_t);
	static BgL_nodez00_bglt BGl_flopz12z12zzflop_walkz00(BgL_nodez00_bglt);
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	static obj_t BGl_z62initzd2flopzd2cachez12z70zzflop_walkz00(obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_flopzd2funz12zc0zzflop_walkz00(obj_t);
	static obj_t BGl_za2z42flonumzf3za2zb1zzflop_walkz00 = BUNSPEC;
	extern obj_t BGl_literalz00zzast_nodez00;
	static obj_t BGl_z62nodezd2varzd2typez62zzflop_walkz00(obj_t, obj_t);
	static obj_t BGl_z62flopzd2walkz12za2zzflop_walkz00(obj_t, obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static BgL_nodez00_bglt BGl_z62flopz12z70zzflop_walkz00(obj_t, obj_t);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t __cnst[16];


	   
		 
		DEFINE_STRING(BGl_string2042z00zzflop_walkz00,
		BgL_bgl_string2042za700za7za7f2063za7, "Flop", 4);
	      DEFINE_STRING(BGl_string2043z00zzflop_walkz00,
		BgL_bgl_string2043za700za7za7f2064za7, "   . ", 5);
	      DEFINE_STRING(BGl_string2044z00zzflop_walkz00,
		BgL_bgl_string2044za700za7za7f2065za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string2045z00zzflop_walkz00,
		BgL_bgl_string2045za700za7za7f2066za7, " error", 6);
	      DEFINE_STRING(BGl_string2046z00zzflop_walkz00,
		BgL_bgl_string2046za700za7za7f2067za7, "s", 1);
	      DEFINE_STRING(BGl_string2047z00zzflop_walkz00,
		BgL_bgl_string2047za700za7za7f2068za7, "", 0);
	      DEFINE_STRING(BGl_string2048z00zzflop_walkz00,
		BgL_bgl_string2048za700za7za7f2069za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string2049z00zzflop_walkz00,
		BgL_bgl_string2049za700za7za7f2070za7, "failure during postlude hook", 28);
	      DEFINE_STATIC_BGL_GENERIC(BGl_flopz12zd2envzc0zzflop_walkz00,
		BgL_bgl_za762flopza712za770za7za7f2071za7, BGl_z62flopz12z70zzflop_walkz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2051z00zzflop_walkz00,
		BgL_bgl_string2051za700za7za7f2072za7, "flop!1285", 9);
	      DEFINE_STRING(BGl_string2053z00zzflop_walkz00,
		BgL_bgl_string2053za700za7za7f2073za7, "node-var-type1293", 17);
	      DEFINE_STRING(BGl_string2055z00zzflop_walkz00,
		BgL_bgl_string2055za700za7za7f2074za7, "flop!::node", 11);
	      DEFINE_STRING(BGl_string2059z00zzflop_walkz00,
		BgL_bgl_string2059za700za7za7f2075za7, "node-var-type", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2050z00zzflop_walkz00,
		BgL_bgl_za762flopza7121285za772076za7, BGl_z62flopz121285z70zzflop_walkz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2052z00zzflop_walkz00,
		BgL_bgl_za762nodeza7d2varza7d22077za7,
		BGl_z62nodezd2varzd2type1293z62zzflop_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2060z00zzflop_walkz00,
		BgL_bgl_string2060za700za7za7f2078za7, "flop_walk", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2054z00zzflop_walkz00,
		BgL_bgl_za762flopza712za7d2let2079za7,
		BGl_z62flopz12zd2letzd2var1288z70zzflop_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2061z00zzflop_walkz00,
		BgL_bgl_string2061za700za7za7f2080za7,
		"read fl |2| (+ - / *) number->flonum __r4_numbers_6_5 fixnum? __r4_numbers_6_5_fixnum c-fixnum? flonum? __r4_numbers_6_5_flonum $flonum? foreign (clear-flop-cache!) pass-started (init-flop-cache!) ",
		197);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2056z00zzflop_walkz00,
		BgL_bgl_za762flopza712za7d2app2081za7,
		BGl_z62flopz12zd2app1290za2zzflop_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2057z00zzflop_walkz00,
		BgL_bgl_za762flopza712za7d2con2082za7,
		BGl_z62flopz12zd2conditional1292za2zzflop_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2058z00zzflop_walkz00,
		BgL_bgl_za762nodeza7d2varza7d22083za7,
		BGl_z62nodezd2varzd2typezd2ref1296zb0zzflop_walkz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_initzd2flopzd2cachez12zd2envzc0zzflop_walkz00,
		BgL_bgl_za762initza7d2flopza7d2084za7,
		BGl_z62initzd2flopzd2cachez12z70zzflop_walkz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_flopzd2walkz12zd2envz12zzflop_walkz00,
		BgL_bgl_za762flopza7d2walkza712085za7,
		BGl_z62flopzd2walkz12za2zzflop_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_GENERIC(BGl_nodezd2varzd2typezd2envzd2zzflop_walkz00,
		BgL_bgl_za762nodeza7d2varza7d22086za7,
		BGl_z62nodezd2varzd2typez62zzflop_walkz00, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_clearzd2flopzd2cachez12zd2envzc0zzflop_walkz00,
		BgL_bgl_za762clearza7d2flopza72087za7,
		BGl_z62clearzd2flopzd2cachez12z70zzflop_walkz00, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzflop_walkz00));
		     ADD_ROOT((void *) (&BGl_za2fixnumzf3za2zf3zzflop_walkz00));
		     ADD_ROOT((void *) (&BGl_za2flopsza2z00zzflop_walkz00));
		     ADD_ROOT((void *) (&BGl_za2flonumzf3za2zf3zzflop_walkz00));
		     ADD_ROOT((void *) (&BGl_za2czd2fixnumzf3za2z21zzflop_walkz00));
		     ADD_ROOT((void *) (&BGl_za2toflonumza2z00zzflop_walkz00));
		     ADD_ROOT((void *) (&BGl_za2z42flonumzf3za2zb1zzflop_walkz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzflop_walkz00(long
		BgL_checksumz00_3135, char *BgL_fromz00_3136)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzflop_walkz00))
				{
					BGl_requirezd2initializa7ationz75zzflop_walkz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzflop_walkz00();
					BGl_libraryzd2moduleszd2initz00zzflop_walkz00();
					BGl_cnstzd2initzd2zzflop_walkz00();
					BGl_importedzd2moduleszd2initz00zzflop_walkz00();
					BGl_genericzd2initzd2zzflop_walkz00();
					BGl_methodzd2initzd2zzflop_walkz00();
					return BGl_toplevelzd2initzd2zzflop_walkz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzflop_walkz00(void)
	{
		{	/* Flop/walk.scm 19 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "flop_walk");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "flop_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"flop_walk");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"flop_walk");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "flop_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "flop_walk");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "flop_walk");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "flop_walk");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "flop_walk");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "flop_walk");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"flop_walk");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "flop_walk");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "flop_walk");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "flop_walk");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzflop_walkz00(void)
	{
		{	/* Flop/walk.scm 19 */
			{	/* Flop/walk.scm 19 */
				obj_t BgL_cportz00_2945;

				{	/* Flop/walk.scm 19 */
					obj_t BgL_stringz00_2952;

					BgL_stringz00_2952 = BGl_string2061z00zzflop_walkz00;
					{	/* Flop/walk.scm 19 */
						obj_t BgL_startz00_2953;

						BgL_startz00_2953 = BINT(0L);
						{	/* Flop/walk.scm 19 */
							obj_t BgL_endz00_2954;

							BgL_endz00_2954 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2952)));
							{	/* Flop/walk.scm 19 */

								BgL_cportz00_2945 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2952, BgL_startz00_2953, BgL_endz00_2954);
				}}}}
				{
					long BgL_iz00_2946;

					BgL_iz00_2946 = 15L;
				BgL_loopz00_2947:
					if ((BgL_iz00_2946 == -1L))
						{	/* Flop/walk.scm 19 */
							return BUNSPEC;
						}
					else
						{	/* Flop/walk.scm 19 */
							{	/* Flop/walk.scm 19 */
								obj_t BgL_arg2062z00_2948;

								{	/* Flop/walk.scm 19 */

									{	/* Flop/walk.scm 19 */
										obj_t BgL_locationz00_2950;

										BgL_locationz00_2950 = BBOOL(((bool_t) 0));
										{	/* Flop/walk.scm 19 */

											BgL_arg2062z00_2948 =
												BGl_readz00zz__readerz00(BgL_cportz00_2945,
												BgL_locationz00_2950);
										}
									}
								}
								{	/* Flop/walk.scm 19 */
									int BgL_tmpz00_3170;

									BgL_tmpz00_3170 = (int) (BgL_iz00_2946);
									CNST_TABLE_SET(BgL_tmpz00_3170, BgL_arg2062z00_2948);
							}}
							{	/* Flop/walk.scm 19 */
								int BgL_auxz00_2951;

								BgL_auxz00_2951 = (int) ((BgL_iz00_2946 - 1L));
								{
									long BgL_iz00_3175;

									BgL_iz00_3175 = (long) (BgL_auxz00_2951);
									BgL_iz00_2946 = BgL_iz00_3175;
									goto BgL_loopz00_2947;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzflop_walkz00(void)
	{
		{	/* Flop/walk.scm 19 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzflop_walkz00(void)
	{
		{	/* Flop/walk.scm 19 */
			BGl_za2flopsza2z00zzflop_walkz00 = BNIL;
			BGl_za2flonumzf3za2zf3zzflop_walkz00 = BFALSE;
			BGl_za2z42flonumzf3za2zb1zzflop_walkz00 = BFALSE;
			BGl_za2fixnumzf3za2zf3zzflop_walkz00 = BFALSE;
			BGl_za2czd2fixnumzf3za2z21zzflop_walkz00 = BFALSE;
			BGl_za2toflonumza2z00zzflop_walkz00 = BFALSE;
			return BUNSPEC;
		}

	}



/* flop-walk! */
	BGL_EXPORTED_DEF obj_t BGl_flopzd2walkz12zc0zzflop_walkz00(obj_t
		BgL_globalsz00_17)
	{
		{	/* Flop/walk.scm 44 */
			{	/* Flop/walk.scm 45 */
				obj_t BgL_list1318z00_1492;

				{	/* Flop/walk.scm 45 */
					obj_t BgL_arg1319z00_1493;

					{	/* Flop/walk.scm 45 */
						obj_t BgL_arg1320z00_1494;

						BgL_arg1320z00_1494 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1319z00_1493 =
							MAKE_YOUNG_PAIR(BGl_string2042z00zzflop_walkz00,
							BgL_arg1320z00_1494);
					}
					BgL_list1318z00_1492 =
						MAKE_YOUNG_PAIR(BGl_string2043z00zzflop_walkz00,
						BgL_arg1319z00_1493);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1318z00_1492);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string2042z00zzflop_walkz00;
			{	/* Flop/walk.scm 45 */
				obj_t BgL_g1117z00_1495;
				obj_t BgL_g1118z00_1496;

				{	/* Flop/walk.scm 45 */
					obj_t BgL_list1329z00_1509;

					BgL_list1329z00_1509 =
						MAKE_YOUNG_PAIR(BGl_initzd2flopzd2cachez12zd2envzc0zzflop_walkz00,
						BNIL);
					BgL_g1117z00_1495 = BgL_list1329z00_1509;
				}
				BgL_g1118z00_1496 = CNST_TABLE_REF(0);
				{
					obj_t BgL_hooksz00_1498;
					obj_t BgL_hnamesz00_1499;

					BgL_hooksz00_1498 = BgL_g1117z00_1495;
					BgL_hnamesz00_1499 = BgL_g1118z00_1496;
				BgL_zc3z04anonymousza31321ze3z87_1500:
					if (NULLP(BgL_hooksz00_1498))
						{	/* Flop/walk.scm 45 */
							CNST_TABLE_REF(1);
						}
					else
						{	/* Flop/walk.scm 45 */
							bool_t BgL_test2091z00_3190;

							{	/* Flop/walk.scm 45 */
								obj_t BgL_fun1328z00_1507;

								BgL_fun1328z00_1507 = CAR(((obj_t) BgL_hooksz00_1498));
								BgL_test2091z00_3190 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1328z00_1507));
							}
							if (BgL_test2091z00_3190)
								{	/* Flop/walk.scm 45 */
									obj_t BgL_arg1325z00_1504;
									obj_t BgL_arg1326z00_1505;

									BgL_arg1325z00_1504 = CDR(((obj_t) BgL_hooksz00_1498));
									BgL_arg1326z00_1505 = CDR(((obj_t) BgL_hnamesz00_1499));
									{
										obj_t BgL_hnamesz00_3202;
										obj_t BgL_hooksz00_3201;

										BgL_hooksz00_3201 = BgL_arg1325z00_1504;
										BgL_hnamesz00_3202 = BgL_arg1326z00_1505;
										BgL_hnamesz00_1499 = BgL_hnamesz00_3202;
										BgL_hooksz00_1498 = BgL_hooksz00_3201;
										goto BgL_zc3z04anonymousza31321ze3z87_1500;
									}
								}
							else
								{	/* Flop/walk.scm 45 */
									obj_t BgL_arg1327z00_1506;

									BgL_arg1327z00_1506 = CAR(((obj_t) BgL_hnamesz00_1499));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string2042z00zzflop_walkz00,
										BGl_string2044z00zzflop_walkz00, BgL_arg1327z00_1506);
								}
						}
				}
			}
			if (CBOOL(BGl_za2optimzd2specializa7ezd2flonumzf3za2z54zzengine_paramz00))
				{
					obj_t BgL_l1270z00_1511;

					BgL_l1270z00_1511 = BgL_globalsz00_17;
				BgL_zc3z04anonymousza31330ze3z87_1512:
					if (PAIRP(BgL_l1270z00_1511))
						{	/* Flop/walk.scm 47 */
							BGl_flopzd2funz12zc0zzflop_walkz00(CAR(BgL_l1270z00_1511));
							{
								obj_t BgL_l1270z00_3212;

								BgL_l1270z00_3212 = CDR(BgL_l1270z00_1511);
								BgL_l1270z00_1511 = BgL_l1270z00_3212;
								goto BgL_zc3z04anonymousza31330ze3z87_1512;
							}
						}
					else
						{	/* Flop/walk.scm 47 */
							((bool_t) 1);
						}
				}
			else
				{	/* Flop/walk.scm 46 */
					((bool_t) 0);
				}
			if (
				((long) CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) > 0L))
				{	/* Flop/walk.scm 48 */
					{	/* Flop/walk.scm 48 */
						obj_t BgL_port1272z00_1519;

						{	/* Flop/walk.scm 48 */
							obj_t BgL_tmpz00_3217;

							BgL_tmpz00_3217 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_port1272z00_1519 =
								BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_3217);
						}
						bgl_display_obj(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
							BgL_port1272z00_1519);
						bgl_display_string(BGl_string2045z00zzflop_walkz00,
							BgL_port1272z00_1519);
						{	/* Flop/walk.scm 48 */
							obj_t BgL_arg1335z00_1520;

							{	/* Flop/walk.scm 48 */
								bool_t BgL_test2095z00_3222;

								if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
									(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
									{	/* Flop/walk.scm 48 */
										if (INTEGERP
											(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
											{	/* Flop/walk.scm 48 */
												BgL_test2095z00_3222 =
													(
													(long)
													CINT
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
													> 1L);
											}
										else
											{	/* Flop/walk.scm 48 */
												BgL_test2095z00_3222 =
													BGl_2ze3ze3zz__r4_numbers_6_5z00
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
													BINT(1L));
											}
									}
								else
									{	/* Flop/walk.scm 48 */
										BgL_test2095z00_3222 = ((bool_t) 0);
									}
								if (BgL_test2095z00_3222)
									{	/* Flop/walk.scm 48 */
										BgL_arg1335z00_1520 = BGl_string2046z00zzflop_walkz00;
									}
								else
									{	/* Flop/walk.scm 48 */
										BgL_arg1335z00_1520 = BGl_string2047z00zzflop_walkz00;
									}
							}
							bgl_display_obj(BgL_arg1335z00_1520, BgL_port1272z00_1519);
						}
						bgl_display_string(BGl_string2048z00zzflop_walkz00,
							BgL_port1272z00_1519);
						bgl_display_char(((unsigned char) 10), BgL_port1272z00_1519);
					}
					{	/* Flop/walk.scm 48 */
						obj_t BgL_list1338z00_1524;

						BgL_list1338z00_1524 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
						BGL_TAIL return BGl_exitz00zz__errorz00(BgL_list1338z00_1524);
					}
				}
			else
				{	/* Flop/walk.scm 48 */
					obj_t BgL_g1119z00_1525;
					obj_t BgL_g1120z00_1526;

					{	/* Flop/walk.scm 48 */
						obj_t BgL_list1352z00_1539;

						BgL_list1352z00_1539 =
							MAKE_YOUNG_PAIR
							(BGl_clearzd2flopzd2cachez12zd2envzc0zzflop_walkz00, BNIL);
						BgL_g1119z00_1525 = BgL_list1352z00_1539;
					}
					BgL_g1120z00_1526 = CNST_TABLE_REF(2);
					{
						obj_t BgL_hooksz00_1528;
						obj_t BgL_hnamesz00_1529;

						BgL_hooksz00_1528 = BgL_g1119z00_1525;
						BgL_hnamesz00_1529 = BgL_g1120z00_1526;
					BgL_zc3z04anonymousza31339ze3z87_1530:
						if (NULLP(BgL_hooksz00_1528))
							{	/* Flop/walk.scm 48 */
								return BgL_globalsz00_17;
							}
						else
							{	/* Flop/walk.scm 48 */
								bool_t BgL_test2099z00_3241;

								{	/* Flop/walk.scm 48 */
									obj_t BgL_fun1351z00_1537;

									BgL_fun1351z00_1537 = CAR(((obj_t) BgL_hooksz00_1528));
									BgL_test2099z00_3241 =
										CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1351z00_1537));
								}
								if (BgL_test2099z00_3241)
									{	/* Flop/walk.scm 48 */
										obj_t BgL_arg1346z00_1534;
										obj_t BgL_arg1348z00_1535;

										BgL_arg1346z00_1534 = CDR(((obj_t) BgL_hooksz00_1528));
										BgL_arg1348z00_1535 = CDR(((obj_t) BgL_hnamesz00_1529));
										{
											obj_t BgL_hnamesz00_3253;
											obj_t BgL_hooksz00_3252;

											BgL_hooksz00_3252 = BgL_arg1346z00_1534;
											BgL_hnamesz00_3253 = BgL_arg1348z00_1535;
											BgL_hnamesz00_1529 = BgL_hnamesz00_3253;
											BgL_hooksz00_1528 = BgL_hooksz00_3252;
											goto BgL_zc3z04anonymousza31339ze3z87_1530;
										}
									}
								else
									{	/* Flop/walk.scm 48 */
										obj_t BgL_arg1349z00_1536;

										BgL_arg1349z00_1536 = CAR(((obj_t) BgL_hnamesz00_1529));
										return
											BGl_internalzd2errorzd2zztools_errorz00
											(BGl_za2currentzd2passza2zd2zzengine_passz00,
											BGl_string2049z00zzflop_walkz00, BgL_arg1349z00_1536);
									}
							}
					}
				}
		}

	}



/* &flop-walk! */
	obj_t BGl_z62flopzd2walkz12za2zzflop_walkz00(obj_t BgL_envz00_2918,
		obj_t BgL_globalsz00_2919)
	{
		{	/* Flop/walk.scm 44 */
			return BGl_flopzd2walkz12zc0zzflop_walkz00(BgL_globalsz00_2919);
		}

	}



/* init-flop-cache! */
	BGL_EXPORTED_DEF obj_t BGl_initzd2flopzd2cachez12z12zzflop_walkz00(void)
	{
		{	/* Flop/walk.scm 63 */
			if (PAIRP(BGl_za2flopsza2z00zzflop_walkz00))
				{	/* Flop/walk.scm 64 */
					BFALSE;
				}
			else
				{	/* Flop/walk.scm 64 */
					{	/* Flop/walk.scm 65 */
						obj_t BgL_list1355z00_1541;

						BgL_list1355z00_1541 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
						BGl_za2z42flonumzf3za2zb1zzflop_walkz00 =
							BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(4),
							BgL_list1355z00_1541);
					}
					{	/* Flop/walk.scm 66 */
						obj_t BgL_list1356z00_1542;

						BgL_list1356z00_1542 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(5), BNIL);
						BGl_za2flonumzf3za2zf3zzflop_walkz00 =
							BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(6),
							BgL_list1356z00_1542);
					}
					{	/* Flop/walk.scm 67 */
						obj_t BgL_list1357z00_1543;

						BgL_list1357z00_1543 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
						BGl_za2czd2fixnumzf3za2z21zzflop_walkz00 =
							BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(7),
							BgL_list1357z00_1543);
					}
					{	/* Flop/walk.scm 68 */
						obj_t BgL_list1358z00_1544;

						BgL_list1358z00_1544 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(8), BNIL);
						BGl_za2fixnumzf3za2zf3zzflop_walkz00 =
							BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(9),
							BgL_list1358z00_1544);
					}
					{	/* Flop/walk.scm 69 */
						obj_t BgL_list1359z00_1545;

						BgL_list1359z00_1545 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(10), BNIL);
						BGl_za2toflonumza2z00zzflop_walkz00 =
							BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(11),
							BgL_list1359z00_1545);
					}
					{	/* Flop/walk.scm 71 */
						obj_t BgL_l1273z00_1546;

						BgL_l1273z00_1546 = CNST_TABLE_REF(12);
						{	/* Flop/walk.scm 71 */
							obj_t BgL_head1275z00_1548;

							BgL_head1275z00_1548 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1273z00_1550;
								obj_t BgL_tail1276z00_1551;

								BgL_l1273z00_1550 = BgL_l1273z00_1546;
								BgL_tail1276z00_1551 = BgL_head1275z00_1548;
							BgL_zc3z04anonymousza31361ze3z87_1552:
								if (NULLP(BgL_l1273z00_1550))
									{	/* Flop/walk.scm 71 */
										BGl_za2flopsza2z00zzflop_walkz00 =
											CDR(BgL_head1275z00_1548);
									}
								else
									{	/* Flop/walk.scm 71 */
										obj_t BgL_newtail1277z00_1554;

										{	/* Flop/walk.scm 71 */
											obj_t BgL_arg1367z00_1556;

											{	/* Flop/walk.scm 71 */
												obj_t BgL_opz00_1557;

												BgL_opz00_1557 = CAR(((obj_t) BgL_l1273z00_1550));
												{	/* Flop/walk.scm 72 */
													obj_t BgL_arg1370z00_1558;
													obj_t BgL_arg1371z00_1559;

													{	/* Flop/walk.scm 72 */
														obj_t BgL_arg1375z00_1560;

														{	/* Flop/walk.scm 72 */
															obj_t BgL_arg1377z00_1562;

															{	/* Flop/walk.scm 72 */
																obj_t BgL_arg1378z00_1563;
																obj_t BgL_arg1379z00_1564;

																{	/* Flop/walk.scm 72 */
																	obj_t BgL_symbolz00_2166;

																	BgL_symbolz00_2166 = CNST_TABLE_REF(13);
																	{	/* Flop/walk.scm 72 */
																		obj_t BgL_arg1455z00_2167;

																		BgL_arg1455z00_2167 =
																			SYMBOL_TO_STRING(BgL_symbolz00_2166);
																		BgL_arg1378z00_1563 =
																			BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																			(BgL_arg1455z00_2167);
																	}
																}
																{	/* Flop/walk.scm 72 */
																	obj_t BgL_arg1455z00_2169;

																	BgL_arg1455z00_2169 =
																		SYMBOL_TO_STRING(((obj_t) BgL_opz00_1557));
																	BgL_arg1379z00_1564 =
																		BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																		(BgL_arg1455z00_2169);
																}
																BgL_arg1377z00_1562 =
																	string_append(BgL_arg1378z00_1563,
																	BgL_arg1379z00_1564);
															}
															BgL_arg1375z00_1560 =
																bstring_to_symbol(BgL_arg1377z00_1562);
														}
														{	/* Flop/walk.scm 72 */
															obj_t BgL_list1376z00_1561;

															BgL_list1376z00_1561 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(10), BNIL);
															BgL_arg1370z00_1558 =
																BGl_findzd2globalzd2zzast_envz00
																(BgL_arg1375z00_1560, BgL_list1376z00_1561);
														}
													}
													{	/* Flop/walk.scm 73 */
														obj_t BgL_arg1380z00_1565;

														{	/* Flop/walk.scm 73 */
															obj_t BgL_arg1408z00_1567;

															{	/* Flop/walk.scm 73 */
																obj_t BgL_arg1410z00_1568;
																obj_t BgL_arg1421z00_1569;

																{	/* Flop/walk.scm 73 */
																	obj_t BgL_arg1455z00_2172;

																	BgL_arg1455z00_2172 =
																		SYMBOL_TO_STRING(((obj_t) BgL_opz00_1557));
																	BgL_arg1410z00_1568 =
																		BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																		(BgL_arg1455z00_2172);
																}
																{	/* Flop/walk.scm 73 */
																	obj_t BgL_symbolz00_2173;

																	BgL_symbolz00_2173 = CNST_TABLE_REF(14);
																	{	/* Flop/walk.scm 73 */
																		obj_t BgL_arg1455z00_2174;

																		BgL_arg1455z00_2174 =
																			SYMBOL_TO_STRING(BgL_symbolz00_2173);
																		BgL_arg1421z00_1569 =
																			BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																			(BgL_arg1455z00_2174);
																	}
																}
																BgL_arg1408z00_1567 =
																	string_append(BgL_arg1410z00_1568,
																	BgL_arg1421z00_1569);
															}
															BgL_arg1380z00_1565 =
																bstring_to_symbol(BgL_arg1408z00_1567);
														}
														{	/* Flop/walk.scm 73 */
															obj_t BgL_list1381z00_1566;

															BgL_list1381z00_1566 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(5), BNIL);
															BgL_arg1371z00_1559 =
																BGl_findzd2globalzd2zzast_envz00
																(BgL_arg1380z00_1565, BgL_list1381z00_1566);
														}
													}
													BgL_arg1367z00_1556 =
														MAKE_YOUNG_PAIR(BgL_arg1370z00_1558,
														BgL_arg1371z00_1559);
												}
											}
											BgL_newtail1277z00_1554 =
												MAKE_YOUNG_PAIR(BgL_arg1367z00_1556, BNIL);
										}
										SET_CDR(BgL_tail1276z00_1551, BgL_newtail1277z00_1554);
										{	/* Flop/walk.scm 71 */
											obj_t BgL_arg1364z00_1555;

											BgL_arg1364z00_1555 = CDR(((obj_t) BgL_l1273z00_1550));
											{
												obj_t BgL_tail1276z00_3315;
												obj_t BgL_l1273z00_3314;

												BgL_l1273z00_3314 = BgL_arg1364z00_1555;
												BgL_tail1276z00_3315 = BgL_newtail1277z00_1554;
												BgL_tail1276z00_1551 = BgL_tail1276z00_3315;
												BgL_l1273z00_1550 = BgL_l1273z00_3314;
												goto BgL_zc3z04anonymousza31361ze3z87_1552;
											}
										}
									}
							}
						}
					}
				}
			return BUNSPEC;
		}

	}



/* &init-flop-cache! */
	obj_t BGl_z62initzd2flopzd2cachez12z70zzflop_walkz00(obj_t BgL_envz00_2920)
	{
		{	/* Flop/walk.scm 63 */
			return BGl_initzd2flopzd2cachez12z12zzflop_walkz00();
		}

	}



/* &clear-flop-cache! */
	obj_t BGl_z62clearzd2flopzd2cachez12z70zzflop_walkz00(obj_t BgL_envz00_2921)
	{
		{	/* Flop/walk.scm 80 */
			BGl_za2flonumzf3za2zf3zzflop_walkz00 = BFALSE;
			BGl_za2z42flonumzf3za2zb1zzflop_walkz00 = BFALSE;
			BGl_za2fixnumzf3za2zf3zzflop_walkz00 = BFALSE;
			BGl_za2czd2fixnumzf3za2z21zzflop_walkz00 = BFALSE;
			return (BGl_za2flopsza2z00zzflop_walkz00 = BNIL, BUNSPEC);
		}

	}



/* flop-fun! */
	obj_t BGl_flopzd2funz12zc0zzflop_walkz00(obj_t BgL_varz00_18)
	{
		{	/* Flop/walk.scm 90 */
			{	/* Flop/walk.scm 91 */
				obj_t BgL_arg1422z00_1571;

				BgL_arg1422z00_1571 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_varz00_18)))->BgL_idz00);
				BGl_enterzd2functionzd2zztools_errorz00(BgL_arg1422z00_1571);
			}
			{	/* Flop/walk.scm 92 */
				BgL_valuez00_bglt BgL_funz00_1572;

				BgL_funz00_1572 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_varz00_18)))->BgL_valuez00);
				{	/* Flop/walk.scm 93 */
					BgL_nodez00_bglt BgL_arg1434z00_1573;

					{	/* Flop/walk.scm 93 */
						obj_t BgL_arg1437z00_1574;

						BgL_arg1437z00_1574 =
							(((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_funz00_1572)))->BgL_bodyz00);
						BgL_arg1434z00_1573 =
							BGl_flopz12z12zzflop_walkz00(
							((BgL_nodez00_bglt) BgL_arg1437z00_1574));
					}
					((((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_funz00_1572)))->BgL_bodyz00) =
						((obj_t) ((obj_t) BgL_arg1434z00_1573)), BUNSPEC);
				}
				BGl_leavezd2functionzd2zztools_errorz00();
				return BgL_varz00_18;
			}
		}

	}



/* is-boolean? */
	bool_t BGl_iszd2booleanzf3z21zzflop_walkz00(BgL_nodez00_bglt BgL_nodez00_23,
		bool_t BgL_valz00_24)
	{
		{	/* Flop/walk.scm 253 */
			{	/* Flop/walk.scm 254 */
				bool_t BgL_test2102z00_3330;

				{	/* Flop/walk.scm 254 */
					obj_t BgL_classz00_2182;

					BgL_classz00_2182 = BGl_literalz00zzast_nodez00;
					{	/* Flop/walk.scm 254 */
						BgL_objectz00_bglt BgL_arg1807z00_2184;

						{	/* Flop/walk.scm 254 */
							obj_t BgL_tmpz00_3331;

							BgL_tmpz00_3331 = ((obj_t) ((BgL_objectz00_bglt) BgL_nodez00_23));
							BgL_arg1807z00_2184 = (BgL_objectz00_bglt) (BgL_tmpz00_3331);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Flop/walk.scm 254 */
								long BgL_idxz00_2190;

								BgL_idxz00_2190 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2184);
								BgL_test2102z00_3330 =
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_2190 + 3L)) == BgL_classz00_2182);
							}
						else
							{	/* Flop/walk.scm 254 */
								bool_t BgL_res2018z00_2215;

								{	/* Flop/walk.scm 254 */
									obj_t BgL_oclassz00_2198;

									{	/* Flop/walk.scm 254 */
										obj_t BgL_arg1815z00_2206;
										long BgL_arg1816z00_2207;

										BgL_arg1815z00_2206 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Flop/walk.scm 254 */
											long BgL_arg1817z00_2208;

											BgL_arg1817z00_2208 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2184);
											BgL_arg1816z00_2207 = (BgL_arg1817z00_2208 - OBJECT_TYPE);
										}
										BgL_oclassz00_2198 =
											VECTOR_REF(BgL_arg1815z00_2206, BgL_arg1816z00_2207);
									}
									{	/* Flop/walk.scm 254 */
										bool_t BgL__ortest_1115z00_2199;

										BgL__ortest_1115z00_2199 =
											(BgL_classz00_2182 == BgL_oclassz00_2198);
										if (BgL__ortest_1115z00_2199)
											{	/* Flop/walk.scm 254 */
												BgL_res2018z00_2215 = BgL__ortest_1115z00_2199;
											}
										else
											{	/* Flop/walk.scm 254 */
												long BgL_odepthz00_2200;

												{	/* Flop/walk.scm 254 */
													obj_t BgL_arg1804z00_2201;

													BgL_arg1804z00_2201 = (BgL_oclassz00_2198);
													BgL_odepthz00_2200 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_2201);
												}
												if ((3L < BgL_odepthz00_2200))
													{	/* Flop/walk.scm 254 */
														obj_t BgL_arg1802z00_2203;

														{	/* Flop/walk.scm 254 */
															obj_t BgL_arg1803z00_2204;

															BgL_arg1803z00_2204 = (BgL_oclassz00_2198);
															BgL_arg1802z00_2203 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2204,
																3L);
														}
														BgL_res2018z00_2215 =
															(BgL_arg1802z00_2203 == BgL_classz00_2182);
													}
												else
													{	/* Flop/walk.scm 254 */
														BgL_res2018z00_2215 = ((bool_t) 0);
													}
											}
									}
								}
								BgL_test2102z00_3330 = BgL_res2018z00_2215;
							}
					}
				}
				if (BgL_test2102z00_3330)
					{	/* Flop/walk.scm 254 */
						return
							(
							(((BgL_atomz00_bglt) COBJECT(
										((BgL_atomz00_bglt)
											((BgL_literalz00_bglt) BgL_nodez00_23))))->
								BgL_valuez00) == BBOOL(BgL_valz00_24));
					}
				else
					{	/* Flop/walk.scm 254 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* isnumber? */
	bool_t BGl_isnumberzf3zf3zzflop_walkz00(BgL_nodez00_bglt BgL_exprz00_27,
		obj_t BgL_predz00_28, obj_t BgL_z42predz42_29)
	{
		{	/* Flop/walk.scm 269 */
			{
				BgL_letzd2varzd2_bglt BgL_exprz00_1589;
				obj_t BgL_predz00_1590;
				obj_t BgL_z42predz42_1591;

				{	/* Flop/walk.scm 286 */
					bool_t BgL_test2106z00_3359;

					{	/* Flop/walk.scm 286 */
						obj_t BgL_classz00_2378;

						BgL_classz00_2378 = BGl_appz00zzast_nodez00;
						{	/* Flop/walk.scm 286 */
							BgL_objectz00_bglt BgL_arg1807z00_2380;

							{	/* Flop/walk.scm 286 */
								obj_t BgL_tmpz00_3360;

								BgL_tmpz00_3360 = ((obj_t) BgL_exprz00_27);
								BgL_arg1807z00_2380 = (BgL_objectz00_bglt) (BgL_tmpz00_3360);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Flop/walk.scm 286 */
									long BgL_idxz00_2386;

									BgL_idxz00_2386 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2380);
									BgL_test2106z00_3359 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2386 + 3L)) == BgL_classz00_2378);
								}
							else
								{	/* Flop/walk.scm 286 */
									bool_t BgL_res2021z00_2411;

									{	/* Flop/walk.scm 286 */
										obj_t BgL_oclassz00_2394;

										{	/* Flop/walk.scm 286 */
											obj_t BgL_arg1815z00_2402;
											long BgL_arg1816z00_2403;

											BgL_arg1815z00_2402 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Flop/walk.scm 286 */
												long BgL_arg1817z00_2404;

												BgL_arg1817z00_2404 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2380);
												BgL_arg1816z00_2403 =
													(BgL_arg1817z00_2404 - OBJECT_TYPE);
											}
											BgL_oclassz00_2394 =
												VECTOR_REF(BgL_arg1815z00_2402, BgL_arg1816z00_2403);
										}
										{	/* Flop/walk.scm 286 */
											bool_t BgL__ortest_1115z00_2395;

											BgL__ortest_1115z00_2395 =
												(BgL_classz00_2378 == BgL_oclassz00_2394);
											if (BgL__ortest_1115z00_2395)
												{	/* Flop/walk.scm 286 */
													BgL_res2021z00_2411 = BgL__ortest_1115z00_2395;
												}
											else
												{	/* Flop/walk.scm 286 */
													long BgL_odepthz00_2396;

													{	/* Flop/walk.scm 286 */
														obj_t BgL_arg1804z00_2397;

														BgL_arg1804z00_2397 = (BgL_oclassz00_2394);
														BgL_odepthz00_2396 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2397);
													}
													if ((3L < BgL_odepthz00_2396))
														{	/* Flop/walk.scm 286 */
															obj_t BgL_arg1802z00_2399;

															{	/* Flop/walk.scm 286 */
																obj_t BgL_arg1803z00_2400;

																BgL_arg1803z00_2400 = (BgL_oclassz00_2394);
																BgL_arg1802z00_2399 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2400,
																	3L);
															}
															BgL_res2021z00_2411 =
																(BgL_arg1802z00_2399 == BgL_classz00_2378);
														}
													else
														{	/* Flop/walk.scm 286 */
															BgL_res2021z00_2411 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2106z00_3359 = BgL_res2021z00_2411;
								}
						}
					}
					if (BgL_test2106z00_3359)
						{	/* Flop/walk.scm 272 */
							BgL_variablez00_bglt BgL_vz00_2413;

							BgL_vz00_2413 =
								(((BgL_varz00_bglt) COBJECT(
										(((BgL_appz00_bglt) COBJECT(
													((BgL_appz00_bglt) BgL_exprz00_27)))->BgL_funz00)))->
								BgL_variablez00);
							{	/* Flop/walk.scm 273 */
								bool_t BgL__ortest_1150z00_2415;

								BgL__ortest_1150z00_2415 =
									(((obj_t) BgL_vz00_2413) == BgL_predz00_28);
								if (BgL__ortest_1150z00_2415)
									{	/* Flop/walk.scm 273 */
										return BgL__ortest_1150z00_2415;
									}
								else
									{	/* Flop/walk.scm 273 */
										return (((obj_t) BgL_vz00_2413) == BgL_z42predz42_29);
									}
							}
						}
					else
						{	/* Flop/walk.scm 287 */
							bool_t BgL_test2111z00_3390;

							{	/* Flop/walk.scm 287 */
								obj_t BgL_classz00_2418;

								BgL_classz00_2418 = BGl_letzd2varzd2zzast_nodez00;
								{	/* Flop/walk.scm 287 */
									BgL_objectz00_bglt BgL_arg1807z00_2420;

									{	/* Flop/walk.scm 287 */
										obj_t BgL_tmpz00_3391;

										BgL_tmpz00_3391 = ((obj_t) BgL_exprz00_27);
										BgL_arg1807z00_2420 =
											(BgL_objectz00_bglt) (BgL_tmpz00_3391);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Flop/walk.scm 287 */
											long BgL_idxz00_2426;

											BgL_idxz00_2426 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2420);
											BgL_test2111z00_3390 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2426 + 3L)) == BgL_classz00_2418);
										}
									else
										{	/* Flop/walk.scm 287 */
											bool_t BgL_res2022z00_2451;

											{	/* Flop/walk.scm 287 */
												obj_t BgL_oclassz00_2434;

												{	/* Flop/walk.scm 287 */
													obj_t BgL_arg1815z00_2442;
													long BgL_arg1816z00_2443;

													BgL_arg1815z00_2442 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Flop/walk.scm 287 */
														long BgL_arg1817z00_2444;

														BgL_arg1817z00_2444 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2420);
														BgL_arg1816z00_2443 =
															(BgL_arg1817z00_2444 - OBJECT_TYPE);
													}
													BgL_oclassz00_2434 =
														VECTOR_REF(BgL_arg1815z00_2442,
														BgL_arg1816z00_2443);
												}
												{	/* Flop/walk.scm 287 */
													bool_t BgL__ortest_1115z00_2435;

													BgL__ortest_1115z00_2435 =
														(BgL_classz00_2418 == BgL_oclassz00_2434);
													if (BgL__ortest_1115z00_2435)
														{	/* Flop/walk.scm 287 */
															BgL_res2022z00_2451 = BgL__ortest_1115z00_2435;
														}
													else
														{	/* Flop/walk.scm 287 */
															long BgL_odepthz00_2436;

															{	/* Flop/walk.scm 287 */
																obj_t BgL_arg1804z00_2437;

																BgL_arg1804z00_2437 = (BgL_oclassz00_2434);
																BgL_odepthz00_2436 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2437);
															}
															if ((3L < BgL_odepthz00_2436))
																{	/* Flop/walk.scm 287 */
																	obj_t BgL_arg1802z00_2439;

																	{	/* Flop/walk.scm 287 */
																		obj_t BgL_arg1803z00_2440;

																		BgL_arg1803z00_2440 = (BgL_oclassz00_2434);
																		BgL_arg1802z00_2439 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2440, 3L);
																	}
																	BgL_res2022z00_2451 =
																		(BgL_arg1802z00_2439 == BgL_classz00_2418);
																}
															else
																{	/* Flop/walk.scm 287 */
																	BgL_res2022z00_2451 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2111z00_3390 = BgL_res2022z00_2451;
										}
								}
							}
							if (BgL_test2111z00_3390)
								{	/* Flop/walk.scm 287 */
									BgL_exprz00_1589 = ((BgL_letzd2varzd2_bglt) BgL_exprz00_27);
									BgL_predz00_1590 = BgL_predz00_28;
									BgL_z42predz42_1591 = BgL_z42predz42_29;
									if (
										(bgl_list_length(
												(((BgL_letzd2varzd2_bglt) COBJECT(BgL_exprz00_1589))->
													BgL_bindingsz00)) == 1L))
										{	/* Flop/walk.scm 278 */
											bool_t BgL_test2116z00_3417;

											{	/* Flop/walk.scm 272 */
												BgL_variablez00_bglt BgL_vz00_2301;

												{
													BgL_varz00_bglt BgL_auxz00_3418;

													{
														BgL_appz00_bglt BgL_auxz00_3419;

														{
															obj_t BgL_auxz00_3420;

															{	/* Flop/walk.scm 278 */
																obj_t BgL_pairz00_2296;

																BgL_pairz00_2296 =
																	(((BgL_letzd2varzd2_bglt)
																		COBJECT(BgL_exprz00_1589))->
																	BgL_bindingsz00);
																{	/* Flop/walk.scm 278 */
																	obj_t BgL_pairz00_2299;

																	BgL_pairz00_2299 = CAR(BgL_pairz00_2296);
																	BgL_auxz00_3420 = CDR(BgL_pairz00_2299);
																}
															}
															BgL_auxz00_3419 =
																((BgL_appz00_bglt) BgL_auxz00_3420);
														}
														BgL_auxz00_3418 =
															(((BgL_appz00_bglt) COBJECT(BgL_auxz00_3419))->
															BgL_funz00);
													}
													BgL_vz00_2301 =
														(((BgL_varz00_bglt) COBJECT(BgL_auxz00_3418))->
														BgL_variablez00);
												}
												{	/* Flop/walk.scm 273 */
													bool_t BgL__ortest_1150z00_2303;

													BgL__ortest_1150z00_2303 =
														(((obj_t) BgL_vz00_2301) == BgL_predz00_1590);
													if (BgL__ortest_1150z00_2303)
														{	/* Flop/walk.scm 273 */
															BgL_test2116z00_3417 = BgL__ortest_1150z00_2303;
														}
													else
														{	/* Flop/walk.scm 273 */
															BgL_test2116z00_3417 =
																(
																((obj_t) BgL_vz00_2301) == BgL_z42predz42_1591);
														}
												}
											}
											if (BgL_test2116z00_3417)
												{	/* Flop/walk.scm 279 */
													bool_t BgL_test2118z00_3432;

													{	/* Flop/walk.scm 279 */
														BgL_nodez00_bglt BgL_arg1540z00_1610;

														BgL_arg1540z00_1610 =
															(((BgL_letzd2varzd2_bglt)
																COBJECT(BgL_exprz00_1589))->BgL_bodyz00);
														{	/* Flop/walk.scm 279 */
															obj_t BgL_classz00_2306;

															BgL_classz00_2306 =
																BGl_conditionalz00zzast_nodez00;
															{	/* Flop/walk.scm 279 */
																BgL_objectz00_bglt BgL_arg1807z00_2308;

																{	/* Flop/walk.scm 279 */
																	obj_t BgL_tmpz00_3434;

																	BgL_tmpz00_3434 =
																		((obj_t)
																		((BgL_objectz00_bglt) BgL_arg1540z00_1610));
																	BgL_arg1807z00_2308 =
																		(BgL_objectz00_bglt) (BgL_tmpz00_3434);
																}
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Flop/walk.scm 279 */
																		long BgL_idxz00_2314;

																		BgL_idxz00_2314 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_2308);
																		BgL_test2118z00_3432 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_2314 + 3L)) ==
																			BgL_classz00_2306);
																	}
																else
																	{	/* Flop/walk.scm 279 */
																		bool_t BgL_res2019z00_2339;

																		{	/* Flop/walk.scm 279 */
																			obj_t BgL_oclassz00_2322;

																			{	/* Flop/walk.scm 279 */
																				obj_t BgL_arg1815z00_2330;
																				long BgL_arg1816z00_2331;

																				BgL_arg1815z00_2330 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Flop/walk.scm 279 */
																					long BgL_arg1817z00_2332;

																					BgL_arg1817z00_2332 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_2308);
																					BgL_arg1816z00_2331 =
																						(BgL_arg1817z00_2332 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_2322 =
																					VECTOR_REF(BgL_arg1815z00_2330,
																					BgL_arg1816z00_2331);
																			}
																			{	/* Flop/walk.scm 279 */
																				bool_t BgL__ortest_1115z00_2323;

																				BgL__ortest_1115z00_2323 =
																					(BgL_classz00_2306 ==
																					BgL_oclassz00_2322);
																				if (BgL__ortest_1115z00_2323)
																					{	/* Flop/walk.scm 279 */
																						BgL_res2019z00_2339 =
																							BgL__ortest_1115z00_2323;
																					}
																				else
																					{	/* Flop/walk.scm 279 */
																						long BgL_odepthz00_2324;

																						{	/* Flop/walk.scm 279 */
																							obj_t BgL_arg1804z00_2325;

																							BgL_arg1804z00_2325 =
																								(BgL_oclassz00_2322);
																							BgL_odepthz00_2324 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_2325);
																						}
																						if ((3L < BgL_odepthz00_2324))
																							{	/* Flop/walk.scm 279 */
																								obj_t BgL_arg1802z00_2327;

																								{	/* Flop/walk.scm 279 */
																									obj_t BgL_arg1803z00_2328;

																									BgL_arg1803z00_2328 =
																										(BgL_oclassz00_2322);
																									BgL_arg1802z00_2327 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_2328, 3L);
																								}
																								BgL_res2019z00_2339 =
																									(BgL_arg1802z00_2327 ==
																									BgL_classz00_2306);
																							}
																						else
																							{	/* Flop/walk.scm 279 */
																								BgL_res2019z00_2339 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2118z00_3432 = BgL_res2019z00_2339;
																	}
															}
														}
													}
													if (BgL_test2118z00_3432)
														{	/* Flop/walk.scm 280 */
															BgL_conditionalz00_bglt BgL_i1152z00_1602;

															BgL_i1152z00_1602 =
																((BgL_conditionalz00_bglt)
																(((BgL_letzd2varzd2_bglt)
																		COBJECT(BgL_exprz00_1589))->BgL_bodyz00));
															{	/* Flop/walk.scm 281 */
																bool_t BgL_test2122z00_3459;

																{	/* Flop/walk.scm 281 */
																	BgL_nodez00_bglt BgL_arg1535z00_1609;

																	BgL_arg1535z00_1609 =
																		(((BgL_conditionalz00_bglt)
																			COBJECT(BgL_i1152z00_1602))->BgL_testz00);
																	{	/* Flop/walk.scm 281 */
																		obj_t BgL_classz00_2340;

																		BgL_classz00_2340 = BGl_varz00zzast_nodez00;
																		{	/* Flop/walk.scm 281 */
																			BgL_objectz00_bglt BgL_arg1807z00_2342;

																			{	/* Flop/walk.scm 281 */
																				obj_t BgL_tmpz00_3461;

																				BgL_tmpz00_3461 =
																					((obj_t)
																					((BgL_objectz00_bglt)
																						BgL_arg1535z00_1609));
																				BgL_arg1807z00_2342 =
																					(BgL_objectz00_bglt)
																					(BgL_tmpz00_3461);
																			}
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Flop/walk.scm 281 */
																					long BgL_idxz00_2348;

																					BgL_idxz00_2348 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_2342);
																					BgL_test2122z00_3459 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_2348 + 2L)) ==
																						BgL_classz00_2340);
																				}
																			else
																				{	/* Flop/walk.scm 281 */
																					bool_t BgL_res2020z00_2373;

																					{	/* Flop/walk.scm 281 */
																						obj_t BgL_oclassz00_2356;

																						{	/* Flop/walk.scm 281 */
																							obj_t BgL_arg1815z00_2364;
																							long BgL_arg1816z00_2365;

																							BgL_arg1815z00_2364 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Flop/walk.scm 281 */
																								long BgL_arg1817z00_2366;

																								BgL_arg1817z00_2366 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_2342);
																								BgL_arg1816z00_2365 =
																									(BgL_arg1817z00_2366 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_2356 =
																								VECTOR_REF(BgL_arg1815z00_2364,
																								BgL_arg1816z00_2365);
																						}
																						{	/* Flop/walk.scm 281 */
																							bool_t BgL__ortest_1115z00_2357;

																							BgL__ortest_1115z00_2357 =
																								(BgL_classz00_2340 ==
																								BgL_oclassz00_2356);
																							if (BgL__ortest_1115z00_2357)
																								{	/* Flop/walk.scm 281 */
																									BgL_res2020z00_2373 =
																										BgL__ortest_1115z00_2357;
																								}
																							else
																								{	/* Flop/walk.scm 281 */
																									long BgL_odepthz00_2358;

																									{	/* Flop/walk.scm 281 */
																										obj_t BgL_arg1804z00_2359;

																										BgL_arg1804z00_2359 =
																											(BgL_oclassz00_2356);
																										BgL_odepthz00_2358 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_2359);
																									}
																									if ((2L < BgL_odepthz00_2358))
																										{	/* Flop/walk.scm 281 */
																											obj_t BgL_arg1802z00_2361;

																											{	/* Flop/walk.scm 281 */
																												obj_t
																													BgL_arg1803z00_2362;
																												BgL_arg1803z00_2362 =
																													(BgL_oclassz00_2356);
																												BgL_arg1802z00_2361 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_2362,
																													2L);
																											}
																											BgL_res2020z00_2373 =
																												(BgL_arg1802z00_2361 ==
																												BgL_classz00_2340);
																										}
																									else
																										{	/* Flop/walk.scm 281 */
																											BgL_res2020z00_2373 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test2122z00_3459 =
																						BgL_res2020z00_2373;
																				}
																		}
																	}
																}
																if (BgL_test2122z00_3459)
																	{	/* Flop/walk.scm 283 */
																		obj_t BgL_tmpz00_3484;

																		{	/* Flop/walk.scm 283 */
																			obj_t BgL_pairz00_2374;

																			BgL_pairz00_2374 =
																				(((BgL_letzd2varzd2_bglt)
																					COBJECT(BgL_exprz00_1589))->
																				BgL_bindingsz00);
																			BgL_tmpz00_3484 =
																				CAR(CAR(BgL_pairz00_2374));
																		}
																		return
																			(
																			((obj_t)
																				(((BgL_varz00_bglt) COBJECT(
																							((BgL_varz00_bglt)
																								(((BgL_conditionalz00_bglt)
																										COBJECT
																										(BgL_i1152z00_1602))->
																									BgL_testz00))))->
																					BgL_variablez00)) == BgL_tmpz00_3484);
																	}
																else
																	{	/* Flop/walk.scm 281 */
																		return ((bool_t) 0);
																	}
															}
														}
													else
														{	/* Flop/walk.scm 279 */
															return ((bool_t) 0);
														}
												}
											else
												{	/* Flop/walk.scm 278 */
													return ((bool_t) 0);
												}
										}
									else
										{	/* Flop/walk.scm 277 */
											return ((bool_t) 0);
										}
								}
							else
								{	/* Flop/walk.scm 287 */
									return ((bool_t) 0);
								}
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzflop_walkz00(void)
	{
		{	/* Flop/walk.scm 19 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzflop_walkz00(void)
	{
		{	/* Flop/walk.scm 19 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_flopz12zd2envzc0zzflop_walkz00, BGl_proc2050z00zzflop_walkz00,
				BGl_nodez00zzast_nodez00, BGl_string2051z00zzflop_walkz00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_nodezd2varzd2typezd2envzd2zzflop_walkz00,
				BGl_proc2052z00zzflop_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string2053z00zzflop_walkz00);
		}

	}



/* &node-var-type1293 */
	obj_t BGl_z62nodezd2varzd2type1293z62zzflop_walkz00(obj_t BgL_envz00_2926,
		obj_t BgL_nodez00_2927)
	{
		{	/* Flop/walk.scm 296 */
			return
				((obj_t)
				(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_nodez00_2927)))->BgL_typez00));
		}

	}



/* &flop!1285 */
	obj_t BGl_z62flopz121285z70zzflop_walkz00(obj_t BgL_envz00_2928,
		obj_t BgL_nodez00_2929)
	{
		{	/* Flop/walk.scm 100 */
			return
				((obj_t)
				BGl_walk0z12z12zzast_walkz00(
					((BgL_nodez00_bglt) BgL_nodez00_2929),
					BGl_flopz12zd2envzc0zzflop_walkz00));
		}

	}



/* flop! */
	BgL_nodez00_bglt BGl_flopz12z12zzflop_walkz00(BgL_nodez00_bglt BgL_nodez00_19)
	{
		{	/* Flop/walk.scm 100 */
			{	/* Flop/walk.scm 100 */
				obj_t BgL_method1286z00_1636;

				{	/* Flop/walk.scm 100 */
					obj_t BgL_res2027z00_2484;

					{	/* Flop/walk.scm 100 */
						long BgL_objzd2classzd2numz00_2455;

						BgL_objzd2classzd2numz00_2455 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_19));
						{	/* Flop/walk.scm 100 */
							obj_t BgL_arg1811z00_2456;

							BgL_arg1811z00_2456 =
								PROCEDURE_REF(BGl_flopz12zd2envzc0zzflop_walkz00, (int) (1L));
							{	/* Flop/walk.scm 100 */
								int BgL_offsetz00_2459;

								BgL_offsetz00_2459 = (int) (BgL_objzd2classzd2numz00_2455);
								{	/* Flop/walk.scm 100 */
									long BgL_offsetz00_2460;

									BgL_offsetz00_2460 =
										((long) (BgL_offsetz00_2459) - OBJECT_TYPE);
									{	/* Flop/walk.scm 100 */
										long BgL_modz00_2461;

										BgL_modz00_2461 =
											(BgL_offsetz00_2460 >> (int) ((long) ((int) (4L))));
										{	/* Flop/walk.scm 100 */
											long BgL_restz00_2463;

											BgL_restz00_2463 =
												(BgL_offsetz00_2460 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Flop/walk.scm 100 */

												{	/* Flop/walk.scm 100 */
													obj_t BgL_bucketz00_2465;

													BgL_bucketz00_2465 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2456), BgL_modz00_2461);
													BgL_res2027z00_2484 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2465), BgL_restz00_2463);
					}}}}}}}}
					BgL_method1286z00_1636 = BgL_res2027z00_2484;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL1(BgL_method1286z00_1636,
						((obj_t) BgL_nodez00_19)));
			}
		}

	}



/* &flop! */
	BgL_nodez00_bglt BGl_z62flopz12z70zzflop_walkz00(obj_t BgL_envz00_2924,
		obj_t BgL_nodez00_2925)
	{
		{	/* Flop/walk.scm 100 */
			return
				BGl_flopz12z12zzflop_walkz00(((BgL_nodez00_bglt) BgL_nodez00_2925));
		}

	}



/* node-var-type */
	obj_t BGl_nodezd2varzd2typez00zzflop_walkz00(BgL_nodez00_bglt BgL_nodez00_32)
	{
		{	/* Flop/walk.scm 296 */
			{	/* Flop/walk.scm 296 */
				obj_t BgL_method1294z00_1637;

				{	/* Flop/walk.scm 296 */
					obj_t BgL_res2032z00_2515;

					{	/* Flop/walk.scm 296 */
						long BgL_objzd2classzd2numz00_2486;

						BgL_objzd2classzd2numz00_2486 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_32));
						{	/* Flop/walk.scm 296 */
							obj_t BgL_arg1811z00_2487;

							BgL_arg1811z00_2487 =
								PROCEDURE_REF(BGl_nodezd2varzd2typezd2envzd2zzflop_walkz00,
								(int) (1L));
							{	/* Flop/walk.scm 296 */
								int BgL_offsetz00_2490;

								BgL_offsetz00_2490 = (int) (BgL_objzd2classzd2numz00_2486);
								{	/* Flop/walk.scm 296 */
									long BgL_offsetz00_2491;

									BgL_offsetz00_2491 =
										((long) (BgL_offsetz00_2490) - OBJECT_TYPE);
									{	/* Flop/walk.scm 296 */
										long BgL_modz00_2492;

										BgL_modz00_2492 =
											(BgL_offsetz00_2491 >> (int) ((long) ((int) (4L))));
										{	/* Flop/walk.scm 296 */
											long BgL_restz00_2494;

											BgL_restz00_2494 =
												(BgL_offsetz00_2491 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Flop/walk.scm 296 */

												{	/* Flop/walk.scm 296 */
													obj_t BgL_bucketz00_2496;

													BgL_bucketz00_2496 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2487), BgL_modz00_2492);
													BgL_res2032z00_2515 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2496), BgL_restz00_2494);
					}}}}}}}}
					BgL_method1294z00_1637 = BgL_res2032z00_2515;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1294z00_1637, ((obj_t) BgL_nodez00_32));
			}
		}

	}



/* &node-var-type */
	obj_t BGl_z62nodezd2varzd2typez62zzflop_walkz00(obj_t BgL_envz00_2930,
		obj_t BgL_nodez00_2931)
	{
		{	/* Flop/walk.scm 296 */
			return
				BGl_nodezd2varzd2typez00zzflop_walkz00(
				((BgL_nodez00_bglt) BgL_nodez00_2931));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzflop_walkz00(void)
	{
		{	/* Flop/walk.scm 19 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_flopz12zd2envzc0zzflop_walkz00, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc2054z00zzflop_walkz00, BGl_string2055z00zzflop_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_flopz12zd2envzc0zzflop_walkz00, BGl_appz00zzast_nodez00,
				BGl_proc2056z00zzflop_walkz00, BGl_string2055z00zzflop_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_flopz12zd2envzc0zzflop_walkz00, BGl_conditionalz00zzast_nodez00,
				BGl_proc2057z00zzflop_walkz00, BGl_string2055z00zzflop_walkz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2varzd2typezd2envzd2zzflop_walkz00, BGl_refz00zzast_nodez00,
				BGl_proc2058z00zzflop_walkz00, BGl_string2059z00zzflop_walkz00);
		}

	}



/* &node-var-type-ref1296 */
	obj_t BGl_z62nodezd2varzd2typezd2ref1296zb0zzflop_walkz00(obj_t
		BgL_envz00_2936, obj_t BgL_nodez00_2937)
	{
		{	/* Flop/walk.scm 302 */
			return
				((obj_t)
				(((BgL_variablez00_bglt) COBJECT(
							(((BgL_varz00_bglt) COBJECT(
										((BgL_varz00_bglt)
											((BgL_refz00_bglt) BgL_nodez00_2937))))->
								BgL_variablez00)))->BgL_typez00));
		}

	}



/* &flop!-conditional1292 */
	BgL_nodez00_bglt BGl_z62flopz12zd2conditional1292za2zzflop_walkz00(obj_t
		BgL_envz00_2938, obj_t BgL_nodez00_2939)
	{
		{	/* Flop/walk.scm 232 */
			BGl_walk0z12z12zzast_walkz00(
				((BgL_nodez00_bglt)
					((BgL_conditionalz00_bglt) BgL_nodez00_2939)),
				BGl_flopz12zd2envzc0zzflop_walkz00);
			{	/* Flop/walk.scm 236 */
				bool_t BgL_test2126z00_3579;

				if (BGl_iszd2booleanzf3z21zzflop_walkz00(
						(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_2939)))->BgL_truez00),
						((bool_t) 1)))
					{	/* Flop/walk.scm 236 */
						BgL_test2126z00_3579 =
							BGl_iszd2booleanzf3z21zzflop_walkz00(
							(((BgL_conditionalz00_bglt) COBJECT(
										((BgL_conditionalz00_bglt) BgL_nodez00_2939)))->
								BgL_falsez00), ((bool_t) 1));
					}
				else
					{	/* Flop/walk.scm 236 */
						BgL_test2126z00_3579 = ((bool_t) 0);
					}
				if (BgL_test2126z00_3579)
					{	/* Flop/walk.scm 237 */
						BgL_literalz00_bglt BgL_new1146z00_2960;

						{	/* Flop/walk.scm 237 */
							BgL_literalz00_bglt BgL_new1145z00_2961;

							BgL_new1145z00_2961 =
								((BgL_literalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_literalz00_bgl))));
							{	/* Flop/walk.scm 237 */
								long BgL_arg1936z00_2962;

								BgL_arg1936z00_2962 =
									BGL_CLASS_NUM(BGl_literalz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1145z00_2961),
									BgL_arg1936z00_2962);
							}
							{	/* Flop/walk.scm 237 */
								BgL_objectz00_bglt BgL_tmpz00_3591;

								BgL_tmpz00_3591 = ((BgL_objectz00_bglt) BgL_new1145z00_2961);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3591, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1145z00_2961);
							BgL_new1146z00_2960 = BgL_new1145z00_2961;
						}
						((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt) BgL_new1146z00_2960)))->BgL_locz00) =
							((obj_t) BFALSE), BUNSPEC);
						((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
											BgL_new1146z00_2960)))->BgL_typez00) =
							((BgL_typez00_bglt) ((BgL_typez00_bglt)
									BGl_za2boolza2z00zztype_cachez00)), BUNSPEC);
						((((BgL_atomz00_bglt) COBJECT(((BgL_atomz00_bglt)
											BgL_new1146z00_2960)))->BgL_valuez00) =
							((obj_t) BTRUE), BUNSPEC);
						return ((BgL_nodez00_bglt) BgL_new1146z00_2960);
					}
				else
					{	/* Flop/walk.scm 240 */
						bool_t BgL_test2128z00_3603;

						if (BGl_iszd2booleanzf3z21zzflop_walkz00(
								(((BgL_conditionalz00_bglt) COBJECT(
											((BgL_conditionalz00_bglt) BgL_nodez00_2939)))->
									BgL_truez00), ((bool_t) 0)))
							{	/* Flop/walk.scm 240 */
								BgL_test2128z00_3603 =
									BGl_iszd2booleanzf3z21zzflop_walkz00(
									(((BgL_conditionalz00_bglt) COBJECT(
												((BgL_conditionalz00_bglt) BgL_nodez00_2939)))->
										BgL_falsez00), ((bool_t) 0));
							}
						else
							{	/* Flop/walk.scm 240 */
								BgL_test2128z00_3603 = ((bool_t) 0);
							}
						if (BgL_test2128z00_3603)
							{	/* Flop/walk.scm 241 */
								BgL_literalz00_bglt BgL_new1148z00_2963;

								{	/* Flop/walk.scm 241 */
									BgL_literalz00_bglt BgL_new1147z00_2964;

									BgL_new1147z00_2964 =
										((BgL_literalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_literalz00_bgl))));
									{	/* Flop/walk.scm 241 */
										long BgL_arg1942z00_2965;

										BgL_arg1942z00_2965 =
											BGL_CLASS_NUM(BGl_literalz00zzast_nodez00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1147z00_2964),
											BgL_arg1942z00_2965);
									}
									{	/* Flop/walk.scm 241 */
										BgL_objectz00_bglt BgL_tmpz00_3615;

										BgL_tmpz00_3615 =
											((BgL_objectz00_bglt) BgL_new1147z00_2964);
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3615, BFALSE);
									}
									((BgL_objectz00_bglt) BgL_new1147z00_2964);
									BgL_new1148z00_2963 = BgL_new1147z00_2964;
								}
								((((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt) BgL_new1148z00_2963)))->
										BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
								((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
													BgL_new1148z00_2963)))->BgL_typez00) =
									((BgL_typez00_bglt) ((BgL_typez00_bglt)
											BGl_za2boolza2z00zztype_cachez00)), BUNSPEC);
								((((BgL_atomz00_bglt) COBJECT(((BgL_atomz00_bglt)
													BgL_new1148z00_2963)))->BgL_valuez00) =
									((obj_t) BFALSE), BUNSPEC);
								return ((BgL_nodez00_bglt) BgL_new1148z00_2963);
							}
						else
							{	/* Flop/walk.scm 240 */
								if (
									(BGl_nodezd2varzd2typez00zzflop_walkz00(
											(((BgL_conditionalz00_bglt) COBJECT(
														((BgL_conditionalz00_bglt) BgL_nodez00_2939)))->
												BgL_truez00)) ==
										BGl_nodezd2varzd2typez00zzflop_walkz00(((
													(BgL_conditionalz00_bglt)
													COBJECT(((BgL_conditionalz00_bglt)
															BgL_nodez00_2939)))->BgL_falsez00))))
									{	/* Flop/walk.scm 244 */
										{
											BgL_typez00_bglt BgL_auxz00_3635;

											{	/* Flop/walk.scm 245 */
												BgL_nodez00_bglt BgL_arg1948z00_2966;

												BgL_arg1948z00_2966 =
													(((BgL_conditionalz00_bglt) COBJECT(
															((BgL_conditionalz00_bglt) BgL_nodez00_2939)))->
													BgL_truez00);
												BgL_auxz00_3635 =
													((BgL_typez00_bglt)
													BGl_nodezd2varzd2typez00zzflop_walkz00
													(BgL_arg1948z00_2966));
											}
											((((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt)
																((BgL_conditionalz00_bglt)
																	BgL_nodez00_2939))))->BgL_typez00) =
												((BgL_typez00_bglt) BgL_auxz00_3635), BUNSPEC);
										}
										return
											((BgL_nodez00_bglt)
											((BgL_conditionalz00_bglt) BgL_nodez00_2939));
									}
								else
									{	/* Flop/walk.scm 244 */
										return
											((BgL_nodez00_bglt)
											((BgL_conditionalz00_bglt) BgL_nodez00_2939));
									}
							}
					}
			}
		}

	}



/* &flop!-app1290 */
	BgL_nodez00_bglt BGl_z62flopz12zd2app1290za2zzflop_walkz00(obj_t
		BgL_envz00_2940, obj_t BgL_nodez00_2941)
	{
		{	/* Flop/walk.scm 169 */
			{
				obj_t BgL_opz00_2987;
				BgL_appz00_bglt BgL_nodez00_2988;
				BgL_varz00_bglt BgL_funz00_2989;
				obj_t BgL_argsz00_2990;
				obj_t BgL_argsz00_2970;
				bool_t BgL_boolz00_2971;

				if (
					(((obj_t)
							(((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_appz00_bglt) BgL_nodez00_2941))))->BgL_typez00)) ==
						((obj_t)
							(((BgL_variablez00_bglt) COBJECT(
										(((BgL_varz00_bglt) COBJECT(
													(((BgL_appz00_bglt) COBJECT(
																((BgL_appz00_bglt) BgL_nodez00_2941)))->
														BgL_funz00)))->BgL_variablez00)))->BgL_typez00))))
					{	/* Flop/walk.scm 201 */
						BFALSE;
					}
				else
					{	/* Flop/walk.scm 201 */
						((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_appz00_bglt) BgL_nodez00_2941))))->BgL_typez00) =
							((BgL_typez00_bglt) (((BgL_variablez00_bglt)
										COBJECT((((BgL_varz00_bglt) COBJECT((((BgL_appz00_bglt)
																COBJECT(((BgL_appz00_bglt) BgL_nodez00_2941)))->
															BgL_funz00)))->BgL_variablez00)))->BgL_typez00)),
							BUNSPEC);
					}
				{	/* Flop/walk.scm 203 */
					obj_t BgL_opz00_3005;

					{	/* Flop/walk.scm 203 */
						BgL_variablez00_bglt BgL_arg1898z00_3006;

						BgL_arg1898z00_3006 =
							(((BgL_varz00_bglt) COBJECT(
									(((BgL_appz00_bglt) COBJECT(
												((BgL_appz00_bglt) BgL_nodez00_2941)))->BgL_funz00)))->
							BgL_variablez00);
						BgL_opz00_3005 =
							BGl_assqz00zz__r4_pairs_and_lists_6_3z00(((obj_t)
								BgL_arg1898z00_3006), BGl_za2flopsza2z00zzflop_walkz00);
					}
					if (BGl_isnumberzf3zf3zzflop_walkz00(
							((BgL_nodez00_bglt)
								((BgL_appz00_bglt) BgL_nodez00_2941)),
							BGl_za2flonumzf3za2zf3zzflop_walkz00,
							BGl_za2z42flonumzf3za2zb1zzflop_walkz00))
						{	/* Flop/walk.scm 205 */
							BGl_walk0z12z12zzast_walkz00(
								((BgL_nodez00_bglt)
									((BgL_appz00_bglt) BgL_nodez00_2941)),
								BGl_flopz12zd2envzc0zzflop_walkz00);
							{	/* Flop/walk.scm 208 */
								bool_t BgL_test2133z00_3677;

								{	/* Flop/walk.scm 208 */
									obj_t BgL_arg1862z00_3007;

									{	/* Flop/walk.scm 208 */
										obj_t BgL_arg1863z00_3008;

										BgL_arg1863z00_3008 =
											CAR(
											(((BgL_appz00_bglt) COBJECT(
														((BgL_appz00_bglt) BgL_nodez00_2941)))->
												BgL_argsz00));
										BgL_arg1862z00_3007 =
											BGl_nodezd2varzd2typez00zzflop_walkz00(((BgL_nodez00_bglt)
												BgL_arg1863z00_3008));
									}
									BgL_test2133z00_3677 =
										(BgL_arg1862z00_3007 == BGl_za2realza2z00zztype_cachez00);
								}
								if (BgL_test2133z00_3677)
									{	/* Flop/walk.scm 208 */
										BgL_argsz00_2970 =
											(((BgL_appz00_bglt) COBJECT(
													((BgL_appz00_bglt) BgL_nodez00_2941)))->BgL_argsz00);
										BgL_boolz00_2971 = ((bool_t) 1);
									BgL_condzd2ze3boolz31_2968:
										{	/* Flop/walk.scm 189 */
											bool_t BgL_test2134z00_3684;

											{	/* Flop/walk.scm 189 */
												obj_t BgL_arg1930z00_2972;

												BgL_arg1930z00_2972 = CAR(((obj_t) BgL_argsz00_2970));
												BgL_test2134z00_3684 =
													BGl_sidezd2effectzf3z21zzeffect_effectz00(
													((BgL_nodez00_bglt) BgL_arg1930z00_2972));
											}
											if (BgL_test2134z00_3684)
												{	/* Flop/walk.scm 190 */
													BgL_sequencez00_bglt BgL_new1138z00_2973;

													{	/* Flop/walk.scm 190 */
														BgL_sequencez00_bglt BgL_new1137z00_2974;

														BgL_new1137z00_2974 =
															((BgL_sequencez00_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_sequencez00_bgl))));
														{	/* Flop/walk.scm 190 */
															long BgL_arg1928z00_2975;

															BgL_arg1928z00_2975 =
																BGL_CLASS_NUM(BGl_sequencez00zzast_nodez00);
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_new1137z00_2974),
																BgL_arg1928z00_2975);
														}
														{	/* Flop/walk.scm 190 */
															BgL_objectz00_bglt BgL_tmpz00_3693;

															BgL_tmpz00_3693 =
																((BgL_objectz00_bglt) BgL_new1137z00_2974);
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3693, BFALSE);
														}
														((BgL_objectz00_bglt) BgL_new1137z00_2974);
														BgL_new1138z00_2973 = BgL_new1137z00_2974;
													}
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_new1138z00_2973)))->
															BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
													((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																		BgL_new1138z00_2973)))->BgL_typez00) =
														((BgL_typez00_bglt) ((BgL_typez00_bglt)
																BGl_za2boolza2z00zztype_cachez00)), BUNSPEC);
													((((BgL_nodezf2effectzf2_bglt)
																COBJECT(((BgL_nodezf2effectzf2_bglt)
																		BgL_new1138z00_2973)))->
															BgL_sidezd2effectzd2) =
														((obj_t) BUNSPEC), BUNSPEC);
													((((BgL_nodezf2effectzf2_bglt)
																COBJECT(((BgL_nodezf2effectzf2_bglt)
																		BgL_new1138z00_2973)))->BgL_keyz00) =
														((obj_t) BINT(-1L)), BUNSPEC);
													{
														obj_t BgL_auxz00_3707;

														{	/* Flop/walk.scm 192 */
															obj_t BgL_arg1923z00_2976;
															BgL_literalz00_bglt BgL_arg1924z00_2977;

															BgL_arg1923z00_2976 =
																CAR(((obj_t) BgL_argsz00_2970));
															{	/* Flop/walk.scm 193 */
																BgL_literalz00_bglt BgL_new1140z00_2978;

																{	/* Flop/walk.scm 193 */
																	BgL_literalz00_bglt BgL_new1139z00_2979;

																	BgL_new1139z00_2979 =
																		((BgL_literalz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_literalz00_bgl))));
																	{	/* Flop/walk.scm 193 */
																		long BgL_arg1927z00_2980;

																		{	/* Flop/walk.scm 193 */
																			obj_t BgL_classz00_2981;

																			BgL_classz00_2981 =
																				BGl_literalz00zzast_nodez00;
																			BgL_arg1927z00_2980 =
																				BGL_CLASS_NUM(BgL_classz00_2981);
																		}
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1139z00_2979),
																			BgL_arg1927z00_2980);
																	}
																	{	/* Flop/walk.scm 193 */
																		BgL_objectz00_bglt BgL_tmpz00_3714;

																		BgL_tmpz00_3714 =
																			((BgL_objectz00_bglt)
																			BgL_new1139z00_2979);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3714,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1139z00_2979);
																	BgL_new1140z00_2978 = BgL_new1139z00_2979;
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1140z00_2978)))->BgL_locz00) =
																	((obj_t) BFALSE), BUNSPEC);
																((((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt)
																					BgL_new1140z00_2978)))->BgL_typez00) =
																	((BgL_typez00_bglt) ((BgL_typez00_bglt)
																			BGl_za2boolza2z00zztype_cachez00)),
																	BUNSPEC);
																((((BgL_atomz00_bglt)
																			COBJECT(((BgL_atomz00_bglt)
																					BgL_new1140z00_2978)))->
																		BgL_valuez00) =
																	((obj_t) BBOOL(BgL_boolz00_2971)), BUNSPEC);
																BgL_arg1924z00_2977 = BgL_new1140z00_2978;
															}
															{	/* Flop/walk.scm 192 */
																obj_t BgL_list1925z00_2982;

																{	/* Flop/walk.scm 192 */
																	obj_t BgL_arg1926z00_2983;

																	BgL_arg1926z00_2983 =
																		MAKE_YOUNG_PAIR(
																		((obj_t) BgL_arg1924z00_2977), BNIL);
																	BgL_list1925z00_2982 =
																		MAKE_YOUNG_PAIR(BgL_arg1923z00_2976,
																		BgL_arg1926z00_2983);
																}
																BgL_auxz00_3707 = BgL_list1925z00_2982;
														}}
														((((BgL_sequencez00_bglt)
																	COBJECT(BgL_new1138z00_2973))->BgL_nodesz00) =
															((obj_t) BgL_auxz00_3707), BUNSPEC);
													}
													((((BgL_sequencez00_bglt)
																COBJECT(BgL_new1138z00_2973))->BgL_unsafez00) =
														((bool_t) ((bool_t) 0)), BUNSPEC);
													((((BgL_sequencez00_bglt)
																COBJECT(BgL_new1138z00_2973))->BgL_metaz00) =
														((obj_t) BNIL), BUNSPEC);
													return ((BgL_nodez00_bglt) BgL_new1138z00_2973);
												}
											else
												{	/* Flop/walk.scm 196 */
													BgL_literalz00_bglt BgL_new1142z00_2984;

													{	/* Flop/walk.scm 196 */
														BgL_literalz00_bglt BgL_new1141z00_2985;

														BgL_new1141z00_2985 =
															((BgL_literalz00_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_literalz00_bgl))));
														{	/* Flop/walk.scm 196 */
															long BgL_arg1929z00_2986;

															BgL_arg1929z00_2986 =
																BGL_CLASS_NUM(BGl_literalz00zzast_nodez00);
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_new1141z00_2985),
																BgL_arg1929z00_2986);
														}
														{	/* Flop/walk.scm 196 */
															BgL_objectz00_bglt BgL_tmpz00_3737;

															BgL_tmpz00_3737 =
																((BgL_objectz00_bglt) BgL_new1141z00_2985);
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3737, BFALSE);
														}
														((BgL_objectz00_bglt) BgL_new1141z00_2985);
														BgL_new1142z00_2984 = BgL_new1141z00_2985;
													}
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_new1142z00_2984)))->
															BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
													((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																		BgL_new1142z00_2984)))->BgL_typez00) =
														((BgL_typez00_bglt) ((BgL_typez00_bglt)
																BGl_za2boolza2z00zztype_cachez00)), BUNSPEC);
													((((BgL_atomz00_bglt) COBJECT(((BgL_atomz00_bglt)
																		BgL_new1142z00_2984)))->BgL_valuez00) =
														((obj_t) BBOOL(BgL_boolz00_2971)), BUNSPEC);
													return ((BgL_nodez00_bglt) BgL_new1142z00_2984);
												}
										}
									}
								else
									{	/* Flop/walk.scm 210 */
										bool_t BgL_test2135z00_3752;

										{	/* Flop/walk.scm 210 */
											bool_t BgL_test2136z00_3753;

											{	/* Flop/walk.scm 210 */
												obj_t BgL_arg1858z00_3009;

												{	/* Flop/walk.scm 210 */
													obj_t BgL_arg1859z00_3010;

													BgL_arg1859z00_3010 =
														CAR(
														(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt) BgL_nodez00_2941)))->
															BgL_argsz00));
													BgL_arg1858z00_3009 =
														BGl_nodezd2varzd2typez00zzflop_walkz00((
															(BgL_nodez00_bglt) BgL_arg1859z00_3010));
												}
												BgL_test2136z00_3753 =
													(BgL_arg1858z00_3009 ==
													BGl_za2_za2z00zztype_cachez00);
											}
											if (BgL_test2136z00_3753)
												{	/* Flop/walk.scm 210 */
													BgL_test2135z00_3752 = ((bool_t) 0);
												}
											else
												{	/* Flop/walk.scm 211 */
													bool_t BgL_test2137z00_3760;

													{	/* Flop/walk.scm 211 */
														obj_t BgL_arg1854z00_3011;

														{	/* Flop/walk.scm 211 */
															obj_t BgL_arg1856z00_3012;

															BgL_arg1856z00_3012 =
																CAR(
																(((BgL_appz00_bglt) COBJECT(
																			((BgL_appz00_bglt) BgL_nodez00_2941)))->
																	BgL_argsz00));
															BgL_arg1854z00_3011 =
																BGl_nodezd2varzd2typez00zzflop_walkz00((
																	(BgL_nodez00_bglt) BgL_arg1856z00_3012));
														}
														BgL_test2137z00_3760 =
															(BgL_arg1854z00_3011 ==
															BGl_za2objza2z00zztype_cachez00);
													}
													if (BgL_test2137z00_3760)
														{	/* Flop/walk.scm 211 */
															BgL_test2135z00_3752 = ((bool_t) 0);
														}
													else
														{	/* Flop/walk.scm 211 */
															BgL_test2135z00_3752 = ((bool_t) 1);
														}
												}
										}
										if (BgL_test2135z00_3752)
											{
												bool_t BgL_boolz00_3770;
												obj_t BgL_argsz00_3767;

												BgL_argsz00_3767 =
													(((BgL_appz00_bglt) COBJECT(
															((BgL_appz00_bglt) BgL_nodez00_2941)))->
													BgL_argsz00);
												BgL_boolz00_3770 = ((bool_t) 0);
												BgL_boolz00_2971 = BgL_boolz00_3770;
												BgL_argsz00_2970 = BgL_argsz00_3767;
												goto BgL_condzd2ze3boolz31_2968;
											}
										else
											{	/* Flop/walk.scm 210 */
												return
													((BgL_nodez00_bglt)
													((BgL_appz00_bglt) BgL_nodez00_2941));
											}
									}
							}
						}
					else
						{	/* Flop/walk.scm 205 */
							if (BGl_isnumberzf3zf3zzflop_walkz00(
									((BgL_nodez00_bglt)
										((BgL_appz00_bglt) BgL_nodez00_2941)),
									BGl_za2fixnumzf3za2zf3zzflop_walkz00,
									BGl_za2czd2fixnumzf3za2z21zzflop_walkz00))
								{	/* Flop/walk.scm 215 */
									BGl_walk0z12z12zzast_walkz00(
										((BgL_nodez00_bglt)
											((BgL_appz00_bglt) BgL_nodez00_2941)),
										BGl_flopz12zd2envzc0zzflop_walkz00);
									{	/* Flop/walk.scm 218 */
										bool_t BgL_test2139z00_3780;

										{	/* Flop/walk.scm 218 */
											obj_t BgL_arg1873z00_3013;

											{	/* Flop/walk.scm 218 */
												obj_t BgL_arg1874z00_3014;

												BgL_arg1874z00_3014 =
													CAR(
													(((BgL_appz00_bglt) COBJECT(
																((BgL_appz00_bglt) BgL_nodez00_2941)))->
														BgL_argsz00));
												BgL_arg1873z00_3013 =
													BGl_nodezd2varzd2typez00zzflop_walkz00((
														(BgL_nodez00_bglt) BgL_arg1874z00_3014));
											}
											BgL_test2139z00_3780 =
												(BgL_arg1873z00_3013 ==
												BGl_za2realza2z00zztype_cachez00);
										}
										if (BgL_test2139z00_3780)
											{
												bool_t BgL_boolz00_3790;
												obj_t BgL_argsz00_3787;

												BgL_argsz00_3787 =
													(((BgL_appz00_bglt) COBJECT(
															((BgL_appz00_bglt) BgL_nodez00_2941)))->
													BgL_argsz00);
												BgL_boolz00_3790 = ((bool_t) 0);
												BgL_boolz00_2971 = BgL_boolz00_3790;
												BgL_argsz00_2970 = BgL_argsz00_3787;
												goto BgL_condzd2ze3boolz31_2968;
											}
										else
											{	/* Flop/walk.scm 218 */
												return
													((BgL_nodez00_bglt)
													((BgL_appz00_bglt) BgL_nodez00_2941));
											}
									}
								}
							else
								{	/* Flop/walk.scm 215 */
									if (CBOOL(BgL_opz00_3005))
										{	/* Flop/walk.scm 222 */
											bool_t BgL_test2141z00_3795;

											{	/* Flop/walk.scm 222 */
												obj_t BgL_arg1894z00_3015;

												{	/* Flop/walk.scm 222 */
													obj_t BgL_arg1896z00_3016;

													BgL_arg1896z00_3016 =
														CAR(
														(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt) BgL_nodez00_2941)))->
															BgL_argsz00));
													BgL_arg1894z00_3015 =
														BGl_nodezd2varzd2typez00zzflop_walkz00((
															(BgL_nodez00_bglt) BgL_arg1896z00_3016));
												}
												BgL_test2141z00_3795 =
													(BgL_arg1894z00_3015 ==
													BGl_za2realza2z00zztype_cachez00);
											}
											if (BgL_test2141z00_3795)
												{	/* Flop/walk.scm 223 */
													BgL_varz00_bglt BgL_arg1880z00_3017;
													obj_t BgL_arg1882z00_3018;

													BgL_arg1880z00_3017 =
														(((BgL_appz00_bglt) COBJECT(
																((BgL_appz00_bglt) BgL_nodez00_2941)))->
														BgL_funz00);
													BgL_arg1882z00_3018 =
														CDR((((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt)
																		BgL_nodez00_2941)))->BgL_argsz00));
													{
														BgL_appz00_bglt BgL_auxz00_3807;

														BgL_opz00_2987 = BgL_opz00_3005;
														BgL_nodez00_2988 =
															((BgL_appz00_bglt) BgL_nodez00_2941);
														BgL_funz00_2989 = BgL_arg1880z00_3017;
														BgL_argsz00_2990 = BgL_arg1882z00_3018;
													BgL_patchz12z12_2969:
														{	/* Flop/walk.scm 172 */
															BgL_typez00_bglt BgL_vz00_2991;

															BgL_vz00_2991 =
																((BgL_typez00_bglt)
																BGl_za2realza2z00zztype_cachez00);
															((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																				BgL_nodez00_2988)))->BgL_typez00) =
																((BgL_typez00_bglt) BgL_vz00_2991), BUNSPEC);
														}
														((((BgL_varz00_bglt) COBJECT(BgL_funz00_2989))->
																BgL_variablez00) =
															((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
																	CDR(((obj_t) BgL_opz00_2987)))), BUNSPEC);
														{	/* Flop/walk.scm 177 */
															obj_t BgL_arg1904z00_2992;

															BgL_arg1904z00_2992 =
																CAR(((obj_t) BgL_argsz00_2990));
															BGl_flopz12z12zzflop_walkz00(
																((BgL_nodez00_bglt) BgL_arg1904z00_2992));
														}
														{	/* Flop/walk.scm 178 */
															bool_t BgL_test2142z00_3819;

															{	/* Flop/walk.scm 178 */
																obj_t BgL_arg1917z00_2993;

																{	/* Flop/walk.scm 178 */
																	obj_t BgL_arg1918z00_2994;

																	BgL_arg1918z00_2994 =
																		CAR(((obj_t) BgL_argsz00_2990));
																	BgL_arg1917z00_2993 =
																		BGl_nodezd2varzd2typez00zzflop_walkz00(
																		((BgL_nodez00_bglt) BgL_arg1918z00_2994));
																}
																BgL_test2142z00_3819 =
																	(BgL_arg1917z00_2993 ==
																	BGl_za2realza2z00zztype_cachez00);
															}
															if (BgL_test2142z00_3819)
																{	/* Flop/walk.scm 178 */
																	BFALSE;
																}
															else
																{	/* Flop/walk.scm 180 */
																	BgL_appz00_bglt BgL_arg1912z00_2995;

																	{	/* Flop/walk.scm 180 */
																		BgL_appz00_bglt BgL_new1134z00_2996;

																		{	/* Flop/walk.scm 180 */
																			BgL_appz00_bglt BgL_new1133z00_2997;

																			BgL_new1133z00_2997 =
																				((BgL_appz00_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_appz00_bgl))));
																			{	/* Flop/walk.scm 180 */
																				long BgL_arg1916z00_2998;

																				BgL_arg1916z00_2998 =
																					BGL_CLASS_NUM
																					(BGl_appz00zzast_nodez00);
																				BGL_OBJECT_CLASS_NUM_SET((
																						(BgL_objectz00_bglt)
																						BgL_new1133z00_2997),
																					BgL_arg1916z00_2998);
																			}
																			{	/* Flop/walk.scm 180 */
																				BgL_objectz00_bglt BgL_tmpz00_3829;

																				BgL_tmpz00_3829 =
																					((BgL_objectz00_bglt)
																					BgL_new1133z00_2997);
																				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3829,
																					BFALSE);
																			}
																			((BgL_objectz00_bglt)
																				BgL_new1133z00_2997);
																			BgL_new1134z00_2996 = BgL_new1133z00_2997;
																		}
																		((((BgL_nodez00_bglt) COBJECT(
																						((BgL_nodez00_bglt)
																							BgL_new1134z00_2996)))->
																				BgL_locz00) =
																			((obj_t) BFALSE), BUNSPEC);
																		((((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt)
																							BgL_new1134z00_2996)))->
																				BgL_typez00) =
																			((BgL_typez00_bglt) ((BgL_typez00_bglt)
																					BGl_za2realza2z00zztype_cachez00)),
																			BUNSPEC);
																		((((BgL_nodezf2effectzf2_bglt)
																					COBJECT(((BgL_nodezf2effectzf2_bglt)
																							BgL_new1134z00_2996)))->
																				BgL_sidezd2effectzd2) =
																			((obj_t) BUNSPEC), BUNSPEC);
																		((((BgL_nodezf2effectzf2_bglt)
																					COBJECT(((BgL_nodezf2effectzf2_bglt)
																							BgL_new1134z00_2996)))->
																				BgL_keyz00) =
																			((obj_t) BINT(-1L)), BUNSPEC);
																		{
																			BgL_varz00_bglt BgL_auxz00_3843;

																			{	/* Flop/walk.scm 182 */
																				BgL_refz00_bglt BgL_new1136z00_2999;

																				{	/* Flop/walk.scm 182 */
																					BgL_refz00_bglt BgL_new1135z00_3000;

																					BgL_new1135z00_3000 =
																						((BgL_refz00_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_refz00_bgl))));
																					{	/* Flop/walk.scm 182 */
																						long BgL_arg1913z00_3001;

																						{	/* Flop/walk.scm 182 */
																							obj_t BgL_classz00_3002;

																							BgL_classz00_3002 =
																								BGl_refz00zzast_nodez00;
																							BgL_arg1913z00_3001 =
																								BGL_CLASS_NUM
																								(BgL_classz00_3002);
																						}
																						BGL_OBJECT_CLASS_NUM_SET(
																							((BgL_objectz00_bglt)
																								BgL_new1135z00_3000),
																							BgL_arg1913z00_3001);
																					}
																					{	/* Flop/walk.scm 182 */
																						BgL_objectz00_bglt BgL_tmpz00_3848;

																						BgL_tmpz00_3848 =
																							((BgL_objectz00_bglt)
																							BgL_new1135z00_3000);
																						BGL_OBJECT_WIDENING_SET
																							(BgL_tmpz00_3848, BFALSE);
																					}
																					((BgL_objectz00_bglt)
																						BgL_new1135z00_3000);
																					BgL_new1136z00_2999 =
																						BgL_new1135z00_3000;
																				}
																				((((BgL_nodez00_bglt) COBJECT(
																								((BgL_nodez00_bglt)
																									BgL_new1136z00_2999)))->
																						BgL_locz00) =
																					((obj_t) BFALSE), BUNSPEC);
																				((((BgL_nodez00_bglt)
																							COBJECT(((BgL_nodez00_bglt)
																									BgL_new1136z00_2999)))->
																						BgL_typez00) =
																					((BgL_typez00_bglt) (
																							(BgL_typez00_bglt)
																							BGl_za2realza2z00zztype_cachez00)),
																					BUNSPEC);
																				((((BgL_varz00_bglt)
																							COBJECT(((BgL_varz00_bglt)
																									BgL_new1136z00_2999)))->
																						BgL_variablez00) =
																					((BgL_variablez00_bglt) (
																							(BgL_variablez00_bglt)
																							BGl_za2toflonumza2z00zzflop_walkz00)),
																					BUNSPEC);
																				BgL_auxz00_3843 =
																					((BgL_varz00_bglt)
																					BgL_new1136z00_2999);
																			}
																			((((BgL_appz00_bglt)
																						COBJECT(BgL_new1134z00_2996))->
																					BgL_funz00) =
																				((BgL_varz00_bglt) BgL_auxz00_3843),
																				BUNSPEC);
																		}
																		{
																			obj_t BgL_auxz00_3862;

																			{	/* Flop/walk.scm 185 */
																				obj_t BgL_arg1914z00_3003;

																				BgL_arg1914z00_3003 =
																					CAR(((obj_t) BgL_argsz00_2990));
																				{	/* Flop/walk.scm 185 */
																					obj_t BgL_list1915z00_3004;

																					BgL_list1915z00_3004 =
																						MAKE_YOUNG_PAIR(BgL_arg1914z00_3003,
																						BNIL);
																					BgL_auxz00_3862 =
																						BgL_list1915z00_3004;
																			}}
																			((((BgL_appz00_bglt)
																						COBJECT(BgL_new1134z00_2996))->
																					BgL_argsz00) =
																				((obj_t) BgL_auxz00_3862), BUNSPEC);
																		}
																		((((BgL_appz00_bglt)
																					COBJECT(BgL_new1134z00_2996))->
																				BgL_stackablez00) =
																			((obj_t) BFALSE), BUNSPEC);
																		BgL_arg1912z00_2995 = BgL_new1134z00_2996;
																	}
																	{	/* Flop/walk.scm 179 */
																		obj_t BgL_auxz00_3870;
																		obj_t BgL_tmpz00_3868;

																		BgL_auxz00_3870 =
																			((obj_t) BgL_arg1912z00_2995);
																		BgL_tmpz00_3868 =
																			((obj_t) BgL_argsz00_2990);
																		SET_CAR(BgL_tmpz00_3868, BgL_auxz00_3870);
														}}}
														BgL_auxz00_3807 = BgL_nodez00_2988;
														return ((BgL_nodez00_bglt) BgL_auxz00_3807);
													}
												}
											else
												{	/* Flop/walk.scm 224 */
													bool_t BgL_test2143z00_3875;

													{	/* Flop/walk.scm 224 */
														obj_t BgL_arg1891z00_3019;

														{	/* Flop/walk.scm 224 */
															obj_t BgL_arg1892z00_3020;

															{	/* Flop/walk.scm 224 */
																obj_t BgL_pairz00_3021;

																BgL_pairz00_3021 =
																	(((BgL_appz00_bglt) COBJECT(
																			((BgL_appz00_bglt) BgL_nodez00_2941)))->
																	BgL_argsz00);
																BgL_arg1892z00_3020 =
																	CAR(CDR(BgL_pairz00_3021));
															}
															BgL_arg1891z00_3019 =
																BGl_nodezd2varzd2typez00zzflop_walkz00(
																((BgL_nodez00_bglt) BgL_arg1892z00_3020));
														}
														BgL_test2143z00_3875 =
															(BgL_arg1891z00_3019 ==
															BGl_za2realza2z00zztype_cachez00);
													}
													if (BgL_test2143z00_3875)
														{	/* Flop/walk.scm 225 */
															BgL_varz00_bglt BgL_arg1889z00_3022;
															obj_t BgL_arg1890z00_3023;

															BgL_arg1889z00_3022 =
																(((BgL_appz00_bglt) COBJECT(
																		((BgL_appz00_bglt) BgL_nodez00_2941)))->
																BgL_funz00);
															BgL_arg1890z00_3023 =
																(((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt)
																			BgL_nodez00_2941)))->BgL_argsz00);
															{
																BgL_appz00_bglt BgL_auxz00_3887;

																{
																	obj_t BgL_argsz00_3892;
																	BgL_varz00_bglt BgL_funz00_3891;
																	BgL_appz00_bglt BgL_nodez00_3889;
																	obj_t BgL_opz00_3888;

																	BgL_opz00_3888 = BgL_opz00_3005;
																	BgL_nodez00_3889 =
																		((BgL_appz00_bglt) BgL_nodez00_2941);
																	BgL_funz00_3891 = BgL_arg1889z00_3022;
																	BgL_argsz00_3892 = BgL_arg1890z00_3023;
																	BgL_argsz00_2990 = BgL_argsz00_3892;
																	BgL_funz00_2989 = BgL_funz00_3891;
																	BgL_nodez00_2988 = BgL_nodez00_3889;
																	BgL_opz00_2987 = BgL_opz00_3888;
																	goto BgL_patchz12z12_2969;
																}
																return ((BgL_nodez00_bglt) BgL_auxz00_3887);
															}
														}
													else
														{	/* Flop/walk.scm 224 */
															return
																BGl_walk0z12z12zzast_walkz00(
																((BgL_nodez00_bglt)
																	((BgL_appz00_bglt) BgL_nodez00_2941)),
																BGl_flopz12zd2envzc0zzflop_walkz00);
														}
												}
										}
									else
										{	/* Flop/walk.scm 220 */
											return
												BGl_walk0z12z12zzast_walkz00(
												((BgL_nodez00_bglt)
													((BgL_appz00_bglt) BgL_nodez00_2941)),
												BGl_flopz12zd2envzc0zzflop_walkz00);
										}
								}
						}
				}
			}
		}

	}



/* &flop!-let-var1288 */
	BgL_nodez00_bglt BGl_z62flopz12zd2letzd2var1288z70zzflop_walkz00(obj_t
		BgL_envz00_2942, obj_t BgL_nodez00_2943)
	{
		{	/* Flop/walk.scm 106 */
			{
				BgL_letzd2varzd2_bglt BgL_nodez00_3026;

				{	/* Flop/walk.scm 144 */
					obj_t BgL_g1280z00_3083;

					BgL_g1280z00_3083 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2943)))->BgL_bindingsz00);
					{
						obj_t BgL_l1278z00_3085;

						BgL_l1278z00_3085 = BgL_g1280z00_3083;
					BgL_zc3z04anonymousza31565ze3z87_3084:
						if (PAIRP(BgL_l1278z00_3085))
							{	/* Flop/walk.scm 144 */
								{	/* Flop/walk.scm 145 */
									obj_t BgL_bindingz00_3086;

									BgL_bindingz00_3086 = CAR(BgL_l1278z00_3085);
									{	/* Flop/walk.scm 145 */
										BgL_nodez00_bglt BgL_arg1571z00_3087;

										{	/* Flop/walk.scm 145 */
											obj_t BgL_arg1573z00_3088;

											BgL_arg1573z00_3088 = CDR(((obj_t) BgL_bindingz00_3086));
											BgL_arg1571z00_3087 =
												BGl_flopz12z12zzflop_walkz00(
												((BgL_nodez00_bglt) BgL_arg1573z00_3088));
										}
										{	/* Flop/walk.scm 145 */
											obj_t BgL_auxz00_3911;
											obj_t BgL_tmpz00_3909;

											BgL_auxz00_3911 = ((obj_t) BgL_arg1571z00_3087);
											BgL_tmpz00_3909 = ((obj_t) BgL_bindingz00_3086);
											SET_CDR(BgL_tmpz00_3909, BgL_auxz00_3911);
										}
									}
									if (
										((((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															CAR(
																((obj_t) BgL_bindingz00_3086)))))->
												BgL_accessz00) == CNST_TABLE_REF(15)))
										{	/* Flop/walk.scm 147 */
											bool_t BgL_test2146z00_3921;

											{	/* Flop/walk.scm 147 */
												obj_t BgL_arg1591z00_3089;

												{	/* Flop/walk.scm 147 */
													obj_t BgL_arg1593z00_3090;

													BgL_arg1593z00_3090 =
														CDR(((obj_t) BgL_bindingz00_3086));
													BgL_arg1591z00_3089 =
														BGl_nodezd2varzd2typez00zzflop_walkz00(
														((BgL_nodez00_bglt) BgL_arg1593z00_3090));
												}
												BgL_test2146z00_3921 =
													(BgL_arg1591z00_3089 ==
													BGl_za2realza2z00zztype_cachez00);
											}
											if (BgL_test2146z00_3921)
												{	/* Flop/walk.scm 148 */
													obj_t BgL_arg1589z00_3091;

													BgL_arg1589z00_3091 =
														CAR(((obj_t) BgL_bindingz00_3086));
													{	/* Flop/walk.scm 148 */
														BgL_typez00_bglt BgL_vz00_3092;

														BgL_vz00_3092 =
															((BgL_typez00_bglt)
															BGl_za2realza2z00zztype_cachez00);
														((((BgL_variablez00_bglt)
																	COBJECT(((BgL_variablez00_bglt)
																			BgL_arg1589z00_3091)))->BgL_typez00) =
															((BgL_typez00_bglt) BgL_vz00_3092), BUNSPEC);
													}
												}
											else
												{	/* Flop/walk.scm 147 */
													BFALSE;
												}
										}
									else
										{	/* Flop/walk.scm 146 */
											BFALSE;
										}
								}
								{
									obj_t BgL_l1278z00_3932;

									BgL_l1278z00_3932 = CDR(BgL_l1278z00_3085);
									BgL_l1278z00_3085 = BgL_l1278z00_3932;
									goto BgL_zc3z04anonymousza31565ze3z87_3084;
								}
							}
						else
							{	/* Flop/walk.scm 144 */
								((bool_t) 1);
							}
					}
				}
				{
					BgL_nodez00_bglt BgL_auxz00_3934;

					{	/* Flop/walk.scm 150 */
						BgL_nodez00_bglt BgL_arg1605z00_3093;

						BgL_arg1605z00_3093 =
							(((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_nodez00_2943)))->BgL_bodyz00);
						BgL_auxz00_3934 = BGl_flopz12z12zzflop_walkz00(BgL_arg1605z00_3093);
					}
					((((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_nodez00_2943)))->BgL_bodyz00) =
						((BgL_nodez00_bglt) BgL_auxz00_3934), BUNSPEC);
				}
				{	/* Flop/walk.scm 152 */
					bool_t BgL_test2147z00_3940;

					{	/* Flop/walk.scm 152 */
						obj_t BgL_arg1654z00_3094;

						BgL_arg1654z00_3094 =
							BGl_nodezd2varzd2typez00zzflop_walkz00(
							(((BgL_letzd2varzd2_bglt) COBJECT(
										((BgL_letzd2varzd2_bglt) BgL_nodez00_2943)))->BgL_bodyz00));
						BgL_test2147z00_3940 =
							(BgL_arg1654z00_3094 == BGl_za2realza2z00zztype_cachez00);
					}
					if (BgL_test2147z00_3940)
						{	/* Flop/walk.scm 152 */
							{
								BgL_typez00_bglt BgL_auxz00_3945;

								{	/* Flop/walk.scm 153 */
									BgL_nodez00_bglt BgL_arg1611z00_3095;

									BgL_arg1611z00_3095 =
										(((BgL_letzd2varzd2_bglt) COBJECT(
												((BgL_letzd2varzd2_bglt) BgL_nodez00_2943)))->
										BgL_bodyz00);
									BgL_auxz00_3945 =
										((BgL_typez00_bglt)
										BGl_nodezd2varzd2typez00zzflop_walkz00
										(BgL_arg1611z00_3095));
								}
								((((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt)
													((BgL_letzd2varzd2_bglt) BgL_nodez00_2943))))->
										BgL_typez00) =
									((BgL_typez00_bglt) BgL_auxz00_3945), BUNSPEC);
							}
							return
								((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nodez00_2943));
						}
					else
						{	/* Flop/walk.scm 155 */
							bool_t BgL_test2148z00_3955;

							{	/* Flop/walk.scm 155 */
								bool_t BgL_test2149z00_3956;

								{	/* Flop/walk.scm 155 */
									BgL_nodez00_bglt BgL_arg1651z00_3096;

									BgL_arg1651z00_3096 =
										(((BgL_letzd2varzd2_bglt) COBJECT(
												((BgL_letzd2varzd2_bglt) BgL_nodez00_2943)))->
										BgL_bodyz00);
									{	/* Flop/walk.scm 254 */
										bool_t BgL_test2150z00_3959;

										{	/* Flop/walk.scm 254 */
											obj_t BgL_classz00_3097;

											BgL_classz00_3097 = BGl_literalz00zzast_nodez00;
											{	/* Flop/walk.scm 254 */
												BgL_objectz00_bglt BgL_arg1807z00_3098;

												{	/* Flop/walk.scm 254 */
													obj_t BgL_tmpz00_3960;

													BgL_tmpz00_3960 =
														((obj_t)
														((BgL_objectz00_bglt) BgL_arg1651z00_3096));
													BgL_arg1807z00_3098 =
														(BgL_objectz00_bglt) (BgL_tmpz00_3960);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Flop/walk.scm 254 */
														long BgL_idxz00_3099;

														BgL_idxz00_3099 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3098);
														BgL_test2150z00_3959 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3099 + 3L)) == BgL_classz00_3097);
													}
												else
													{	/* Flop/walk.scm 254 */
														bool_t BgL_res2018z00_3102;

														{	/* Flop/walk.scm 254 */
															obj_t BgL_oclassz00_3103;

															{	/* Flop/walk.scm 254 */
																obj_t BgL_arg1815z00_3104;
																long BgL_arg1816z00_3105;

																BgL_arg1815z00_3104 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Flop/walk.scm 254 */
																	long BgL_arg1817z00_3106;

																	BgL_arg1817z00_3106 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3098);
																	BgL_arg1816z00_3105 =
																		(BgL_arg1817z00_3106 - OBJECT_TYPE);
																}
																BgL_oclassz00_3103 =
																	VECTOR_REF(BgL_arg1815z00_3104,
																	BgL_arg1816z00_3105);
															}
															{	/* Flop/walk.scm 254 */
																bool_t BgL__ortest_1115z00_3107;

																BgL__ortest_1115z00_3107 =
																	(BgL_classz00_3097 == BgL_oclassz00_3103);
																if (BgL__ortest_1115z00_3107)
																	{	/* Flop/walk.scm 254 */
																		BgL_res2018z00_3102 =
																			BgL__ortest_1115z00_3107;
																	}
																else
																	{	/* Flop/walk.scm 254 */
																		long BgL_odepthz00_3108;

																		{	/* Flop/walk.scm 254 */
																			obj_t BgL_arg1804z00_3109;

																			BgL_arg1804z00_3109 =
																				(BgL_oclassz00_3103);
																			BgL_odepthz00_3108 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3109);
																		}
																		if ((3L < BgL_odepthz00_3108))
																			{	/* Flop/walk.scm 254 */
																				obj_t BgL_arg1802z00_3110;

																				{	/* Flop/walk.scm 254 */
																					obj_t BgL_arg1803z00_3111;

																					BgL_arg1803z00_3111 =
																						(BgL_oclassz00_3103);
																					BgL_arg1802z00_3110 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3111, 3L);
																				}
																				BgL_res2018z00_3102 =
																					(BgL_arg1802z00_3110 ==
																					BgL_classz00_3097);
																			}
																		else
																			{	/* Flop/walk.scm 254 */
																				BgL_res2018z00_3102 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2150z00_3959 = BgL_res2018z00_3102;
													}
											}
										}
										if (BgL_test2150z00_3959)
											{	/* Flop/walk.scm 254 */
												BgL_test2149z00_3956 =
													(
													(((BgL_atomz00_bglt) COBJECT(
																((BgL_atomz00_bglt)
																	((BgL_literalz00_bglt)
																		BgL_arg1651z00_3096))))->BgL_valuez00) ==
													BTRUE);
											}
										else
											{	/* Flop/walk.scm 254 */
												BgL_test2149z00_3956 = ((bool_t) 0);
											}
									}
								}
								if (BgL_test2149z00_3956)
									{	/* Flop/walk.scm 155 */
										BgL_test2148z00_3955 = ((bool_t) 1);
									}
								else
									{	/* Flop/walk.scm 155 */
										BgL_nodez00_bglt BgL_arg1650z00_3112;

										BgL_arg1650z00_3112 =
											(((BgL_letzd2varzd2_bglt) COBJECT(
													((BgL_letzd2varzd2_bglt) BgL_nodez00_2943)))->
											BgL_bodyz00);
										{	/* Flop/walk.scm 254 */
											bool_t BgL_test2154z00_3989;

											{	/* Flop/walk.scm 254 */
												obj_t BgL_classz00_3113;

												BgL_classz00_3113 = BGl_literalz00zzast_nodez00;
												{	/* Flop/walk.scm 254 */
													BgL_objectz00_bglt BgL_arg1807z00_3114;

													{	/* Flop/walk.scm 254 */
														obj_t BgL_tmpz00_3990;

														BgL_tmpz00_3990 =
															((obj_t)
															((BgL_objectz00_bglt) BgL_arg1650z00_3112));
														BgL_arg1807z00_3114 =
															(BgL_objectz00_bglt) (BgL_tmpz00_3990);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Flop/walk.scm 254 */
															long BgL_idxz00_3115;

															BgL_idxz00_3115 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3114);
															BgL_test2154z00_3989 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_3115 + 3L)) == BgL_classz00_3113);
														}
													else
														{	/* Flop/walk.scm 254 */
															bool_t BgL_res2018z00_3118;

															{	/* Flop/walk.scm 254 */
																obj_t BgL_oclassz00_3119;

																{	/* Flop/walk.scm 254 */
																	obj_t BgL_arg1815z00_3120;
																	long BgL_arg1816z00_3121;

																	BgL_arg1815z00_3120 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Flop/walk.scm 254 */
																		long BgL_arg1817z00_3122;

																		BgL_arg1817z00_3122 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3114);
																		BgL_arg1816z00_3121 =
																			(BgL_arg1817z00_3122 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_3119 =
																		VECTOR_REF(BgL_arg1815z00_3120,
																		BgL_arg1816z00_3121);
																}
																{	/* Flop/walk.scm 254 */
																	bool_t BgL__ortest_1115z00_3123;

																	BgL__ortest_1115z00_3123 =
																		(BgL_classz00_3113 == BgL_oclassz00_3119);
																	if (BgL__ortest_1115z00_3123)
																		{	/* Flop/walk.scm 254 */
																			BgL_res2018z00_3118 =
																				BgL__ortest_1115z00_3123;
																		}
																	else
																		{	/* Flop/walk.scm 254 */
																			long BgL_odepthz00_3124;

																			{	/* Flop/walk.scm 254 */
																				obj_t BgL_arg1804z00_3125;

																				BgL_arg1804z00_3125 =
																					(BgL_oclassz00_3119);
																				BgL_odepthz00_3124 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_3125);
																			}
																			if ((3L < BgL_odepthz00_3124))
																				{	/* Flop/walk.scm 254 */
																					obj_t BgL_arg1802z00_3126;

																					{	/* Flop/walk.scm 254 */
																						obj_t BgL_arg1803z00_3127;

																						BgL_arg1803z00_3127 =
																							(BgL_oclassz00_3119);
																						BgL_arg1802z00_3126 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_3127, 3L);
																					}
																					BgL_res2018z00_3118 =
																						(BgL_arg1802z00_3126 ==
																						BgL_classz00_3113);
																				}
																			else
																				{	/* Flop/walk.scm 254 */
																					BgL_res2018z00_3118 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2154z00_3989 = BgL_res2018z00_3118;
														}
												}
											}
											if (BgL_test2154z00_3989)
												{	/* Flop/walk.scm 254 */
													BgL_test2148z00_3955 =
														(
														(((BgL_atomz00_bglt) COBJECT(
																	((BgL_atomz00_bglt)
																		((BgL_literalz00_bglt)
																			BgL_arg1650z00_3112))))->BgL_valuez00) ==
														BFALSE);
												}
											else
												{	/* Flop/walk.scm 254 */
													BgL_test2148z00_3955 = ((bool_t) 0);
												}
										}
									}
							}
							if (BgL_test2148z00_3955)
								{	/* Flop/walk.scm 156 */
									bool_t BgL_test2158z00_4017;

									{	/* Flop/walk.scm 156 */
										obj_t BgL_g1283z00_3128;

										BgL_g1283z00_3128 =
											(((BgL_letzd2varzd2_bglt) COBJECT(
													((BgL_letzd2varzd2_bglt) BgL_nodez00_2943)))->
											BgL_bindingsz00);
										{
											obj_t BgL_l1281z00_3130;

											BgL_l1281z00_3130 = BgL_g1283z00_3128;
										BgL_zc3z04anonymousza31631ze3z87_3129:
											if (NULLP(BgL_l1281z00_3130))
												{	/* Flop/walk.scm 156 */
													BgL_test2158z00_4017 = ((bool_t) 1);
												}
											else
												{	/* Flop/walk.scm 156 */
													bool_t BgL_test2160z00_4022;

													{	/* Flop/walk.scm 156 */
														bool_t BgL_test2161z00_4023;

														{	/* Flop/walk.scm 156 */
															obj_t BgL_arg1646z00_3131;

															{	/* Flop/walk.scm 156 */
																obj_t BgL_pairz00_3132;

																BgL_pairz00_3132 =
																	CAR(((obj_t) BgL_l1281z00_3130));
																BgL_arg1646z00_3131 = CDR(BgL_pairz00_3132);
															}
															BgL_test2161z00_4023 =
																BGl_sidezd2effectzf3z21zzeffect_effectz00(
																((BgL_nodez00_bglt) BgL_arg1646z00_3131));
														}
														if (BgL_test2161z00_4023)
															{	/* Flop/walk.scm 156 */
																BgL_test2160z00_4022 = ((bool_t) 0);
															}
														else
															{	/* Flop/walk.scm 156 */
																BgL_test2160z00_4022 = ((bool_t) 1);
															}
													}
													if (BgL_test2160z00_4022)
														{	/* Flop/walk.scm 156 */
															obj_t BgL_arg1642z00_3133;

															BgL_arg1642z00_3133 =
																CDR(((obj_t) BgL_l1281z00_3130));
															{
																obj_t BgL_l1281z00_4031;

																BgL_l1281z00_4031 = BgL_arg1642z00_3133;
																BgL_l1281z00_3130 = BgL_l1281z00_4031;
																goto BgL_zc3z04anonymousza31631ze3z87_3129;
															}
														}
													else
														{	/* Flop/walk.scm 156 */
															BgL_test2158z00_4017 = ((bool_t) 0);
														}
												}
										}
									}
									if (BgL_test2158z00_4017)
										{	/* Flop/walk.scm 156 */
											return
												(((BgL_letzd2varzd2_bglt) COBJECT(
														((BgL_letzd2varzd2_bglt) BgL_nodez00_2943)))->
												BgL_bodyz00);
										}
									else
										{	/* Flop/walk.scm 156 */
											return
												((BgL_nodez00_bglt)
												((BgL_letzd2varzd2_bglt) BgL_nodez00_2943));
										}
								}
							else
								{	/* Flop/walk.scm 159 */
									obj_t BgL_g1129z00_3134;

									BgL_nodez00_3026 = ((BgL_letzd2varzd2_bglt) BgL_nodez00_2943);
									{	/* Flop/walk.scm 128 */
										bool_t BgL_test2162z00_4036;

										{	/* Flop/walk.scm 128 */
											bool_t BgL_test2163z00_4037;

											{	/* Flop/walk.scm 128 */
												obj_t BgL_tmpz00_4038;

												BgL_tmpz00_4038 =
													(((BgL_letzd2varzd2_bglt) COBJECT(BgL_nodez00_3026))->
													BgL_bindingsz00);
												BgL_test2163z00_4037 = PAIRP(BgL_tmpz00_4038);
											}
											if (BgL_test2163z00_4037)
												{	/* Flop/walk.scm 128 */
													if (NULLP(CDR(
																(((BgL_letzd2varzd2_bglt)
																		COBJECT(BgL_nodez00_3026))->
																	BgL_bindingsz00))))
														{	/* Flop/walk.scm 130 */
															bool_t BgL_test2165z00_4045;

															{	/* Flop/walk.scm 130 */
																BgL_typez00_bglt BgL_arg1762z00_3027;

																{
																	BgL_variablez00_bglt BgL_auxz00_4046;

																	{
																		obj_t BgL_auxz00_4047;

																		{	/* Flop/walk.scm 130 */
																			obj_t BgL_pairz00_3028;

																			BgL_pairz00_3028 =
																				(((BgL_letzd2varzd2_bglt)
																					COBJECT(BgL_nodez00_3026))->
																				BgL_bindingsz00);
																			{	/* Flop/walk.scm 130 */
																				obj_t BgL_pairz00_3029;

																				BgL_pairz00_3029 =
																					CAR(BgL_pairz00_3028);
																				BgL_auxz00_4047 = CAR(BgL_pairz00_3029);
																			}
																		}
																		BgL_auxz00_4046 =
																			((BgL_variablez00_bglt) BgL_auxz00_4047);
																	}
																	BgL_arg1762z00_3027 =
																		(((BgL_variablez00_bglt)
																			COBJECT(BgL_auxz00_4046))->BgL_typez00);
																}
																BgL_test2165z00_4045 =
																	(
																	((obj_t) BgL_arg1762z00_3027) ==
																	BGl_za2boolza2z00zztype_cachez00);
															}
															if (BgL_test2165z00_4045)
																{	/* Flop/walk.scm 131 */
																	bool_t BgL_test2166z00_4055;

																	{	/* Flop/walk.scm 131 */
																		obj_t BgL_arg1755z00_3030;

																		{	/* Flop/walk.scm 131 */
																			obj_t BgL_pairz00_3031;

																			BgL_pairz00_3031 =
																				(((BgL_letzd2varzd2_bglt)
																					COBJECT(BgL_nodez00_3026))->
																				BgL_bindingsz00);
																			BgL_arg1755z00_3030 =
																				CDR(CAR(BgL_pairz00_3031));
																		}
																		{	/* Flop/walk.scm 131 */
																			obj_t BgL_classz00_3032;

																			BgL_classz00_3032 =
																				BGl_literalz00zzast_nodez00;
																			if (BGL_OBJECTP(BgL_arg1755z00_3030))
																				{	/* Flop/walk.scm 131 */
																					BgL_objectz00_bglt
																						BgL_arg1807z00_3033;
																					BgL_arg1807z00_3033 =
																						(BgL_objectz00_bglt)
																						(BgL_arg1755z00_3030);
																					if (BGL_CONDEXPAND_ISA_ARCH64())
																						{	/* Flop/walk.scm 131 */
																							long BgL_idxz00_3034;

																							BgL_idxz00_3034 =
																								BGL_OBJECT_INHERITANCE_NUM
																								(BgL_arg1807z00_3033);
																							BgL_test2166z00_4055 =
																								(VECTOR_REF
																								(BGl_za2inheritancesza2z00zz__objectz00,
																									(BgL_idxz00_3034 + 3L)) ==
																								BgL_classz00_3032);
																						}
																					else
																						{	/* Flop/walk.scm 131 */
																							bool_t BgL_res2033z00_3037;

																							{	/* Flop/walk.scm 131 */
																								obj_t BgL_oclassz00_3038;

																								{	/* Flop/walk.scm 131 */
																									obj_t BgL_arg1815z00_3039;
																									long BgL_arg1816z00_3040;

																									BgL_arg1815z00_3039 =
																										(BGl_za2classesza2z00zz__objectz00);
																									{	/* Flop/walk.scm 131 */
																										long BgL_arg1817z00_3041;

																										BgL_arg1817z00_3041 =
																											BGL_OBJECT_CLASS_NUM
																											(BgL_arg1807z00_3033);
																										BgL_arg1816z00_3040 =
																											(BgL_arg1817z00_3041 -
																											OBJECT_TYPE);
																									}
																									BgL_oclassz00_3038 =
																										VECTOR_REF
																										(BgL_arg1815z00_3039,
																										BgL_arg1816z00_3040);
																								}
																								{	/* Flop/walk.scm 131 */
																									bool_t
																										BgL__ortest_1115z00_3042;
																									BgL__ortest_1115z00_3042 =
																										(BgL_classz00_3032 ==
																										BgL_oclassz00_3038);
																									if (BgL__ortest_1115z00_3042)
																										{	/* Flop/walk.scm 131 */
																											BgL_res2033z00_3037 =
																												BgL__ortest_1115z00_3042;
																										}
																									else
																										{	/* Flop/walk.scm 131 */
																											long BgL_odepthz00_3043;

																											{	/* Flop/walk.scm 131 */
																												obj_t
																													BgL_arg1804z00_3044;
																												BgL_arg1804z00_3044 =
																													(BgL_oclassz00_3038);
																												BgL_odepthz00_3043 =
																													BGL_CLASS_DEPTH
																													(BgL_arg1804z00_3044);
																											}
																											if (
																												(3L <
																													BgL_odepthz00_3043))
																												{	/* Flop/walk.scm 131 */
																													obj_t
																														BgL_arg1802z00_3045;
																													{	/* Flop/walk.scm 131 */
																														obj_t
																															BgL_arg1803z00_3046;
																														BgL_arg1803z00_3046
																															=
																															(BgL_oclassz00_3038);
																														BgL_arg1802z00_3045
																															=
																															BGL_CLASS_ANCESTORS_REF
																															(BgL_arg1803z00_3046,
																															3L);
																													}
																													BgL_res2033z00_3037 =
																														(BgL_arg1802z00_3045
																														==
																														BgL_classz00_3032);
																												}
																											else
																												{	/* Flop/walk.scm 131 */
																													BgL_res2033z00_3037 =
																														((bool_t) 0);
																												}
																										}
																								}
																							}
																							BgL_test2166z00_4055 =
																								BgL_res2033z00_3037;
																						}
																				}
																			else
																				{	/* Flop/walk.scm 131 */
																					BgL_test2166z00_4055 = ((bool_t) 0);
																				}
																		}
																	}
																	if (BgL_test2166z00_4055)
																		{	/* Flop/walk.scm 132 */
																			BgL_nodez00_bglt BgL_arg1754z00_3047;

																			BgL_arg1754z00_3047 =
																				(((BgL_letzd2varzd2_bglt)
																					COBJECT(BgL_nodez00_3026))->
																				BgL_bodyz00);
																			{	/* Flop/walk.scm 132 */
																				obj_t BgL_classz00_3048;

																				BgL_classz00_3048 =
																					BGl_conditionalz00zzast_nodez00;
																				{	/* Flop/walk.scm 132 */
																					BgL_objectz00_bglt
																						BgL_arg1807z00_3049;
																					{	/* Flop/walk.scm 132 */
																						obj_t BgL_tmpz00_4082;

																						BgL_tmpz00_4082 =
																							((obj_t)
																							((BgL_objectz00_bglt)
																								BgL_arg1754z00_3047));
																						BgL_arg1807z00_3049 =
																							(BgL_objectz00_bglt)
																							(BgL_tmpz00_4082);
																					}
																					if (BGL_CONDEXPAND_ISA_ARCH64())
																						{	/* Flop/walk.scm 132 */
																							long BgL_idxz00_3050;

																							BgL_idxz00_3050 =
																								BGL_OBJECT_INHERITANCE_NUM
																								(BgL_arg1807z00_3049);
																							BgL_test2162z00_4036 =
																								(VECTOR_REF
																								(BGl_za2inheritancesza2z00zz__objectz00,
																									(BgL_idxz00_3050 + 3L)) ==
																								BgL_classz00_3048);
																						}
																					else
																						{	/* Flop/walk.scm 132 */
																							bool_t BgL_res2034z00_3053;

																							{	/* Flop/walk.scm 132 */
																								obj_t BgL_oclassz00_3054;

																								{	/* Flop/walk.scm 132 */
																									obj_t BgL_arg1815z00_3055;
																									long BgL_arg1816z00_3056;

																									BgL_arg1815z00_3055 =
																										(BGl_za2classesza2z00zz__objectz00);
																									{	/* Flop/walk.scm 132 */
																										long BgL_arg1817z00_3057;

																										BgL_arg1817z00_3057 =
																											BGL_OBJECT_CLASS_NUM
																											(BgL_arg1807z00_3049);
																										BgL_arg1816z00_3056 =
																											(BgL_arg1817z00_3057 -
																											OBJECT_TYPE);
																									}
																									BgL_oclassz00_3054 =
																										VECTOR_REF
																										(BgL_arg1815z00_3055,
																										BgL_arg1816z00_3056);
																								}
																								{	/* Flop/walk.scm 132 */
																									bool_t
																										BgL__ortest_1115z00_3058;
																									BgL__ortest_1115z00_3058 =
																										(BgL_classz00_3048 ==
																										BgL_oclassz00_3054);
																									if (BgL__ortest_1115z00_3058)
																										{	/* Flop/walk.scm 132 */
																											BgL_res2034z00_3053 =
																												BgL__ortest_1115z00_3058;
																										}
																									else
																										{	/* Flop/walk.scm 132 */
																											long BgL_odepthz00_3059;

																											{	/* Flop/walk.scm 132 */
																												obj_t
																													BgL_arg1804z00_3060;
																												BgL_arg1804z00_3060 =
																													(BgL_oclassz00_3054);
																												BgL_odepthz00_3059 =
																													BGL_CLASS_DEPTH
																													(BgL_arg1804z00_3060);
																											}
																											if (
																												(3L <
																													BgL_odepthz00_3059))
																												{	/* Flop/walk.scm 132 */
																													obj_t
																														BgL_arg1802z00_3061;
																													{	/* Flop/walk.scm 132 */
																														obj_t
																															BgL_arg1803z00_3062;
																														BgL_arg1803z00_3062
																															=
																															(BgL_oclassz00_3054);
																														BgL_arg1802z00_3061
																															=
																															BGL_CLASS_ANCESTORS_REF
																															(BgL_arg1803z00_3062,
																															3L);
																													}
																													BgL_res2034z00_3053 =
																														(BgL_arg1802z00_3061
																														==
																														BgL_classz00_3048);
																												}
																											else
																												{	/* Flop/walk.scm 132 */
																													BgL_res2034z00_3053 =
																														((bool_t) 0);
																												}
																										}
																								}
																							}
																							BgL_test2162z00_4036 =
																								BgL_res2034z00_3053;
																						}
																				}
																			}
																		}
																	else
																		{	/* Flop/walk.scm 131 */
																			BgL_test2162z00_4036 = ((bool_t) 0);
																		}
																}
															else
																{	/* Flop/walk.scm 130 */
																	BgL_test2162z00_4036 = ((bool_t) 0);
																}
														}
													else
														{	/* Flop/walk.scm 129 */
															BgL_test2162z00_4036 = ((bool_t) 0);
														}
												}
											else
												{	/* Flop/walk.scm 128 */
													BgL_test2162z00_4036 = ((bool_t) 0);
												}
										}
										if (BgL_test2162z00_4036)
											{	/* Flop/walk.scm 133 */
												BgL_conditionalz00_bglt BgL_i1125z00_3063;

												BgL_i1125z00_3063 =
													((BgL_conditionalz00_bglt)
													(((BgL_letzd2varzd2_bglt) COBJECT(BgL_nodez00_3026))->
														BgL_bodyz00));
												{	/* Flop/walk.scm 134 */
													bool_t BgL_test2174z00_4107;

													{	/* Flop/walk.scm 134 */
														BgL_nodez00_bglt BgL_arg1753z00_3064;

														BgL_arg1753z00_3064 =
															(((BgL_conditionalz00_bglt)
																COBJECT(BgL_i1125z00_3063))->BgL_testz00);
														{	/* Flop/walk.scm 134 */
															obj_t BgL_classz00_3065;

															BgL_classz00_3065 = BGl_refz00zzast_nodez00;
															{	/* Flop/walk.scm 134 */
																BgL_objectz00_bglt BgL_arg1807z00_3066;

																{	/* Flop/walk.scm 134 */
																	obj_t BgL_tmpz00_4109;

																	BgL_tmpz00_4109 =
																		((obj_t)
																		((BgL_objectz00_bglt) BgL_arg1753z00_3064));
																	BgL_arg1807z00_3066 =
																		(BgL_objectz00_bglt) (BgL_tmpz00_4109);
																}
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Flop/walk.scm 134 */
																		long BgL_idxz00_3067;

																		BgL_idxz00_3067 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_3066);
																		BgL_test2174z00_4107 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_3067 + 3L)) ==
																			BgL_classz00_3065);
																	}
																else
																	{	/* Flop/walk.scm 134 */
																		bool_t BgL_res2035z00_3070;

																		{	/* Flop/walk.scm 134 */
																			obj_t BgL_oclassz00_3071;

																			{	/* Flop/walk.scm 134 */
																				obj_t BgL_arg1815z00_3072;
																				long BgL_arg1816z00_3073;

																				BgL_arg1815z00_3072 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Flop/walk.scm 134 */
																					long BgL_arg1817z00_3074;

																					BgL_arg1817z00_3074 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_3066);
																					BgL_arg1816z00_3073 =
																						(BgL_arg1817z00_3074 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_3071 =
																					VECTOR_REF(BgL_arg1815z00_3072,
																					BgL_arg1816z00_3073);
																			}
																			{	/* Flop/walk.scm 134 */
																				bool_t BgL__ortest_1115z00_3075;

																				BgL__ortest_1115z00_3075 =
																					(BgL_classz00_3065 ==
																					BgL_oclassz00_3071);
																				if (BgL__ortest_1115z00_3075)
																					{	/* Flop/walk.scm 134 */
																						BgL_res2035z00_3070 =
																							BgL__ortest_1115z00_3075;
																					}
																				else
																					{	/* Flop/walk.scm 134 */
																						long BgL_odepthz00_3076;

																						{	/* Flop/walk.scm 134 */
																							obj_t BgL_arg1804z00_3077;

																							BgL_arg1804z00_3077 =
																								(BgL_oclassz00_3071);
																							BgL_odepthz00_3076 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_3077);
																						}
																						if ((3L < BgL_odepthz00_3076))
																							{	/* Flop/walk.scm 134 */
																								obj_t BgL_arg1802z00_3078;

																								{	/* Flop/walk.scm 134 */
																									obj_t BgL_arg1803z00_3079;

																									BgL_arg1803z00_3079 =
																										(BgL_oclassz00_3071);
																									BgL_arg1802z00_3078 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_3079, 3L);
																								}
																								BgL_res2035z00_3070 =
																									(BgL_arg1802z00_3078 ==
																									BgL_classz00_3065);
																							}
																						else
																							{	/* Flop/walk.scm 134 */
																								BgL_res2035z00_3070 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2174z00_4107 = BgL_res2035z00_3070;
																	}
															}
														}
													}
													if (BgL_test2174z00_4107)
														{	/* Flop/walk.scm 136 */
															bool_t BgL_test2178z00_4132;

															{	/* Flop/walk.scm 136 */
																obj_t BgL_tmpz00_4133;

																{	/* Flop/walk.scm 136 */
																	obj_t BgL_pairz00_3080;

																	BgL_pairz00_3080 =
																		(((BgL_letzd2varzd2_bglt)
																			COBJECT(BgL_nodez00_3026))->
																		BgL_bindingsz00);
																	BgL_tmpz00_4133 = CAR(CAR(BgL_pairz00_3080));
																}
																BgL_test2178z00_4132 =
																	(
																	((obj_t)
																		(((BgL_varz00_bglt) COBJECT(
																					((BgL_varz00_bglt)
																						((BgL_refz00_bglt)
																							(((BgL_conditionalz00_bglt)
																									COBJECT(BgL_i1125z00_3063))->
																								BgL_testz00)))))->
																			BgL_variablez00)) == BgL_tmpz00_4133);
															}
															if (BgL_test2178z00_4132)
																{	/* Flop/walk.scm 139 */
																	bool_t BgL_test2179z00_4143;

																	{	/* Flop/walk.scm 139 */
																		obj_t BgL_tmpz00_4144;

																		{
																			BgL_atomz00_bglt BgL_auxz00_4145;

																			{
																				BgL_literalz00_bglt BgL_auxz00_4146;

																				{	/* Flop/walk.scm 138 */
																					obj_t BgL_pairz00_3081;

																					BgL_pairz00_3081 =
																						(((BgL_letzd2varzd2_bglt)
																							COBJECT(BgL_nodez00_3026))->
																						BgL_bindingsz00);
																					{	/* Flop/walk.scm 138 */
																						obj_t BgL_pairz00_3082;

																						BgL_pairz00_3082 =
																							CAR(BgL_pairz00_3081);
																						BgL_auxz00_4146 =
																							((BgL_literalz00_bglt)
																							CDR(BgL_pairz00_3082));
																					}
																				}
																				BgL_auxz00_4145 =
																					((BgL_atomz00_bglt) BgL_auxz00_4146);
																			}
																			BgL_tmpz00_4144 =
																				(((BgL_atomz00_bglt)
																					COBJECT(BgL_auxz00_4145))->
																				BgL_valuez00);
																		}
																		BgL_test2179z00_4143 =
																			CBOOL(BgL_tmpz00_4144);
																	}
																	if (BgL_test2179z00_4143)
																		{	/* Flop/walk.scm 139 */
																			BgL_g1129z00_3134 =
																				((obj_t)
																				(((BgL_conditionalz00_bglt)
																						COBJECT(BgL_i1125z00_3063))->
																					BgL_truez00));
																		}
																	else
																		{	/* Flop/walk.scm 139 */
																			BgL_g1129z00_3134 =
																				((obj_t)
																				(((BgL_conditionalz00_bglt)
																						COBJECT(BgL_i1125z00_3063))->
																					BgL_falsez00));
																		}
																}
															else
																{	/* Flop/walk.scm 136 */
																	BgL_g1129z00_3134 = BFALSE;
																}
														}
													else
														{	/* Flop/walk.scm 134 */
															BgL_g1129z00_3134 = BFALSE;
														}
												}
											}
										else
											{	/* Flop/walk.scm 128 */
												BgL_g1129z00_3134 = BFALSE;
											}
									}
									if (CBOOL(BgL_g1129z00_3134))
										{	/* Flop/walk.scm 159 */
											return ((BgL_nodez00_bglt) BgL_g1129z00_3134);
										}
									else
										{	/* Flop/walk.scm 159 */
											return
												((BgL_nodez00_bglt)
												((BgL_letzd2varzd2_bglt) BgL_nodez00_2943));
										}
								}
						}
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzflop_walkz00(void)
	{
		{	/* Flop/walk.scm 19 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string2060z00zzflop_walkz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2060z00zzflop_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string2060z00zzflop_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2060z00zzflop_walkz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2060z00zzflop_walkz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2060z00zzflop_walkz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2060z00zzflop_walkz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string2060z00zzflop_walkz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2060z00zzflop_walkz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string2060z00zzflop_walkz00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string2060z00zzflop_walkz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2060z00zzflop_walkz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string2060z00zzflop_walkz00));
			BGl_modulezd2initializa7ationz75zzast_privatez00(135263837L,
				BSTRING_TO_STRING(BGl_string2060z00zzflop_walkz00));
			BGl_modulezd2initializa7ationz75zzast_lvtypez00(189769752L,
				BSTRING_TO_STRING(BGl_string2060z00zzflop_walkz00));
			BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string2060z00zzflop_walkz00));
			BGl_modulezd2initializa7ationz75zzast_walkz00(343174225L,
				BSTRING_TO_STRING(BGl_string2060z00zzflop_walkz00));
			BGl_modulezd2initializa7ationz75zzeffect_effectz00(460136018L,
				BSTRING_TO_STRING(BGl_string2060z00zzflop_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2060z00zzflop_walkz00));
			return
				BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2060z00zzflop_walkz00));
		}

	}

#ifdef __cplusplus
}
#endif
