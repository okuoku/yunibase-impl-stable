/*===========================================================================*/
/*   (Ast/substitute.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/substitute.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_AST_SUBSTITUTE_TYPE_DEFINITIONS
#define BGL_AST_SUBSTITUTE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;


#endif													// BGL_AST_SUBSTITUTE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t BGl_requirezd2initializa7ationz75zzast_substitutez00 = BUNSPEC;
	static BgL_nodez00_bglt
		BGl_dozd2substitutez12zc0zzast_substitutez00(BgL_nodez00_bglt, obj_t);
	extern obj_t BGl_closurez00zzast_nodez00;
	extern obj_t BGl_userzd2errorzf2locationz20zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_funz00zzast_varz00;
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_toplevelzd2initzd2zzast_substitutez00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2seque1265z70zzast_substitutez00(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2makezd21295za2zzast_substitutez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzast_substitutez00(void);
	static obj_t BGl_objectzd2initzd2zzast_substitutez00(void);
	static BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2var1261z70zzast_substitutez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_castz00zzast_nodez00;
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2switc1285z70zzast_substitutez00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzast_substitutez00(void);
	static BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2atom1259z70zzast_substitutez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_externz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2app1269z70zzast_substitutez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_dozd2substituteza2z12z62zzast_substitutez00(obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	static obj_t BGl_z62dozd2substitutez121256za2zzast_substitutez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2appzd2l1271za2zzast_substitutez00(obj_t, obj_t,
		obj_t);
	extern bool_t BGl_globalzd2optionalzf3z21zzast_varz00(obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2setzd2e1291za2zzast_substitutez00(obj_t, obj_t,
		obj_t);
	extern BgL_typez00_bglt
		BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_za2procedureza2z00zztype_cachez00;
	extern BgL_nodez00_bglt BGl_makezd2appzd2nodez00zzast_appz00(obj_t, obj_t,
		obj_t, BgL_varz00_bglt, obj_t);
	static BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2exter1275z70zzast_substitutez00(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2funca1273z70zzast_substitutez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2kwote1263z70zzast_substitutez00(obj_t, obj_t,
		obj_t);
	extern bool_t BGl_globalzd2keyzf3z21zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzast_substitutez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_appz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_applyz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_substitutez12z12zzast_substitutez00(obj_t, obj_t, BgL_nodez00_bglt,
		obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2condi1281z70zzast_substitutez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzast_substitutez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_substitutez00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzast_substitutez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzast_substitutez00(void);
	static BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2cast1277z70zzast_substitutez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern BgL_nodez00_bglt BGl_knownzd2appzd2lyzd2ze3nodez31zzast_applyz00(obj_t,
		obj_t, BgL_nodez00_bglt, BgL_nodez00_bglt, obj_t);
	static BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2boxzd2r1297za2zzast_substitutez00(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2letzd2f1287za2zzast_substitutez00(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2setq1279z70zzast_substitutez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_usezd2variablez12zc0zzast_sexpz00(BgL_variablez00_bglt,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_variablez00zzast_varz00;
	static BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2letzd2v1289za2zzast_substitutez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2sync1267z70zzast_substitutez00(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt BGl_z62dozd2substitutez12za2zzast_substitutez00(obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62substitutez12z70zzast_substitutez00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2fail1283z70zzast_substitutez00(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2jumpzd21293za2zzast_substitutez00(obj_t, obj_t,
		obj_t);
	extern bool_t
		BGl_correctzd2arityzd2appzf3zf3zzast_appz00(BgL_variablez00_bglt, obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2boxzd2s1299za2zzast_substitutez00(obj_t, obj_t,
		obj_t);
	static obj_t __cnst[7];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_substitutez12zd2envzc0zzast_substitutez00,
		BgL_bgl_za762substituteza7121890z00,
		BGl_z62substitutez12z70zzast_substitutez00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string1859z00zzast_substitutez00,
		BgL_bgl_string1859za700za7za7a1891za7, "do-substitute!1256", 18);
	      DEFINE_STRING(BGl_string1860z00zzast_substitutez00,
		BgL_bgl_string1860za700za7za7a1892za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string1862z00zzast_substitutez00,
		BgL_bgl_string1862za700za7za7a1893za7, "do-substitute!", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1858z00zzast_substitutez00,
		BgL_bgl_za762doza7d2substitu1894z00,
		BGl_z62dozd2substitutez121256za2zzast_substitutez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1861z00zzast_substitutez00,
		BgL_bgl_za762doza7d2substitu1895z00,
		BGl_z62dozd2substitutez12zd2atom1259z70zzast_substitutez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1863z00zzast_substitutez00,
		BgL_bgl_za762doza7d2substitu1896z00,
		BGl_z62dozd2substitutez12zd2var1261z70zzast_substitutez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1864z00zzast_substitutez00,
		BgL_bgl_za762doza7d2substitu1897z00,
		BGl_z62dozd2substitutez12zd2kwote1263z70zzast_substitutez00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1865z00zzast_substitutez00,
		BgL_bgl_za762doza7d2substitu1898z00,
		BGl_z62dozd2substitutez12zd2seque1265z70zzast_substitutez00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1866z00zzast_substitutez00,
		BgL_bgl_za762doza7d2substitu1899z00,
		BGl_z62dozd2substitutez12zd2sync1267z70zzast_substitutez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1867z00zzast_substitutez00,
		BgL_bgl_za762doza7d2substitu1900z00,
		BGl_z62dozd2substitutez12zd2app1269z70zzast_substitutez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1868z00zzast_substitutez00,
		BgL_bgl_za762doza7d2substitu1901z00,
		BGl_z62dozd2substitutez12zd2appzd2l1271za2zzast_substitutez00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1869z00zzast_substitutez00,
		BgL_bgl_za762doza7d2substitu1902z00,
		BGl_z62dozd2substitutez12zd2funca1273z70zzast_substitutez00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1870z00zzast_substitutez00,
		BgL_bgl_za762doza7d2substitu1903z00,
		BGl_z62dozd2substitutez12zd2exter1275z70zzast_substitutez00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1871z00zzast_substitutez00,
		BgL_bgl_za762doza7d2substitu1904z00,
		BGl_z62dozd2substitutez12zd2cast1277z70zzast_substitutez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1872z00zzast_substitutez00,
		BgL_bgl_za762doza7d2substitu1905z00,
		BGl_z62dozd2substitutez12zd2setq1279z70zzast_substitutez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1873z00zzast_substitutez00,
		BgL_bgl_za762doza7d2substitu1906z00,
		BGl_z62dozd2substitutez12zd2condi1281z70zzast_substitutez00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1874z00zzast_substitutez00,
		BgL_bgl_za762doza7d2substitu1907z00,
		BGl_z62dozd2substitutez12zd2fail1283z70zzast_substitutez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1875z00zzast_substitutez00,
		BgL_bgl_za762doza7d2substitu1908z00,
		BGl_z62dozd2substitutez12zd2switc1285z70zzast_substitutez00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1876z00zzast_substitutez00,
		BgL_bgl_za762doza7d2substitu1909z00,
		BGl_z62dozd2substitutez12zd2letzd2f1287za2zzast_substitutez00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string1883z00zzast_substitutez00,
		BgL_bgl_string1883za700za7za7a1910za7, "Illegal application", 19);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1877z00zzast_substitutez00,
		BgL_bgl_za762doza7d2substitu1911z00,
		BGl_z62dozd2substitutez12zd2letzd2v1289za2zzast_substitutez00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string1884z00zzast_substitutez00,
		BgL_bgl_string1884za700za7za7a1912za7, "wrong number of argument(s)", 27);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1878z00zzast_substitutez00,
		BgL_bgl_za762doza7d2substitu1913z00,
		BGl_z62dozd2substitutez12zd2setzd2e1291za2zzast_substitutez00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string1885z00zzast_substitutez00,
		BgL_bgl_string1885za700za7za7a1914za7, "duplicate", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1879z00zzast_substitutez00,
		BgL_bgl_za762doza7d2substitu1915z00,
		BGl_z62dozd2substitutez12zd2jumpzd21293za2zzast_substitutez00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string1886z00zzast_substitutez00,
		BgL_bgl_string1886za700za7za7a1916za7, "Illegal substitution", 20);
	      DEFINE_STRING(BGl_string1887z00zzast_substitutez00,
		BgL_bgl_string1887za700za7za7a1917za7, "ast_substitute", 14);
	      DEFINE_STRING(BGl_string1888z00zzast_substitutez00,
		BgL_bgl_string1888za700za7za7a1918za7,
		"apply funcall set! app do-substitute!1256 value done ", 53);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1880z00zzast_substitutez00,
		BgL_bgl_za762doza7d2substitu1919z00,
		BGl_z62dozd2substitutez12zd2makezd21295za2zzast_substitutez00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1881z00zzast_substitutez00,
		BgL_bgl_za762doza7d2substitu1920z00,
		BGl_z62dozd2substitutez12zd2boxzd2r1297za2zzast_substitutez00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1882z00zzast_substitutez00,
		BgL_bgl_za762doza7d2substitu1921z00,
		BGl_z62dozd2substitutez12zd2boxzd2s1299za2zzast_substitutez00, 0L, BUNSPEC,
		2);
	     
		DEFINE_STATIC_BGL_GENERIC
		(BGl_dozd2substitutez12zd2envz12zzast_substitutez00,
		BgL_bgl_za762doza7d2substitu1922z00,
		BGl_z62dozd2substitutez12za2zzast_substitutez00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzast_substitutez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzast_substitutez00(long
		BgL_checksumz00_2794, char *BgL_fromz00_2795)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_substitutez00))
				{
					BGl_requirezd2initializa7ationz75zzast_substitutez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_substitutez00();
					BGl_libraryzd2moduleszd2initz00zzast_substitutez00();
					BGl_cnstzd2initzd2zzast_substitutez00();
					BGl_importedzd2moduleszd2initz00zzast_substitutez00();
					BGl_genericzd2initzd2zzast_substitutez00();
					BGl_methodzd2initzd2zzast_substitutez00();
					return BGl_toplevelzd2initzd2zzast_substitutez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_substitutez00(void)
	{
		{	/* Ast/substitute.scm 14 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "ast_substitute");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_substitute");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"ast_substitute");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "ast_substitute");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"ast_substitute");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"ast_substitute");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"ast_substitute");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "ast_substitute");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"ast_substitute");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_substitute");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_substitutez00(void)
	{
		{	/* Ast/substitute.scm 14 */
			{	/* Ast/substitute.scm 14 */
				obj_t BgL_cportz00_2552;

				{	/* Ast/substitute.scm 14 */
					obj_t BgL_stringz00_2559;

					BgL_stringz00_2559 = BGl_string1888z00zzast_substitutez00;
					{	/* Ast/substitute.scm 14 */
						obj_t BgL_startz00_2560;

						BgL_startz00_2560 = BINT(0L);
						{	/* Ast/substitute.scm 14 */
							obj_t BgL_endz00_2561;

							BgL_endz00_2561 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2559)));
							{	/* Ast/substitute.scm 14 */

								BgL_cportz00_2552 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2559, BgL_startz00_2560, BgL_endz00_2561);
				}}}}
				{
					long BgL_iz00_2553;

					BgL_iz00_2553 = 6L;
				BgL_loopz00_2554:
					if ((BgL_iz00_2553 == -1L))
						{	/* Ast/substitute.scm 14 */
							return BUNSPEC;
						}
					else
						{	/* Ast/substitute.scm 14 */
							{	/* Ast/substitute.scm 14 */
								obj_t BgL_arg1889z00_2555;

								{	/* Ast/substitute.scm 14 */

									{	/* Ast/substitute.scm 14 */
										obj_t BgL_locationz00_2557;

										BgL_locationz00_2557 = BBOOL(((bool_t) 0));
										{	/* Ast/substitute.scm 14 */

											BgL_arg1889z00_2555 =
												BGl_readz00zz__readerz00(BgL_cportz00_2552,
												BgL_locationz00_2557);
										}
									}
								}
								{	/* Ast/substitute.scm 14 */
									int BgL_tmpz00_2825;

									BgL_tmpz00_2825 = (int) (BgL_iz00_2553);
									CNST_TABLE_SET(BgL_tmpz00_2825, BgL_arg1889z00_2555);
							}}
							{	/* Ast/substitute.scm 14 */
								int BgL_auxz00_2558;

								BgL_auxz00_2558 = (int) ((BgL_iz00_2553 - 1L));
								{
									long BgL_iz00_2830;

									BgL_iz00_2830 = (long) (BgL_auxz00_2558);
									BgL_iz00_2553 = BgL_iz00_2830;
									goto BgL_loopz00_2554;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_substitutez00(void)
	{
		{	/* Ast/substitute.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzast_substitutez00(void)
	{
		{	/* Ast/substitute.scm 14 */
			return BUNSPEC;
		}

	}



/* substitute! */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_substitutez12z12zzast_substitutez00(obj_t BgL_whatza2za2_3,
		obj_t BgL_byza2za2_4, BgL_nodez00_bglt BgL_nodez00_5, obj_t BgL_sitez00_6)
	{
		{	/* Ast/substitute.scm 34 */
			{
				obj_t BgL_ll1237z00_1403;
				obj_t BgL_ll1238z00_1404;

				BgL_ll1237z00_1403 = BgL_whatza2za2_3;
				BgL_ll1238z00_1404 = BgL_byza2za2_4;
			BgL_zc3z04anonymousza31306ze3z87_1405:
				if (NULLP(BgL_ll1237z00_1403))
					{	/* Ast/substitute.scm 37 */
						((bool_t) 1);
					}
				else
					{	/* Ast/substitute.scm 37 */
						{	/* Ast/substitute.scm 39 */
							obj_t BgL_whatz00_1407;
							obj_t BgL_byz00_1408;

							BgL_whatz00_1407 = CAR(((obj_t) BgL_ll1237z00_1403));
							BgL_byz00_1408 = CAR(((obj_t) BgL_ll1238z00_1404));
							((((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_whatz00_1407)))->
									BgL_fastzd2alphazd2) = ((obj_t) BgL_byz00_1408), BUNSPEC);
						}
						{	/* Ast/substitute.scm 37 */
							obj_t BgL_arg1308z00_1409;
							obj_t BgL_arg1310z00_1410;

							BgL_arg1308z00_1409 = CDR(((obj_t) BgL_ll1237z00_1403));
							BgL_arg1310z00_1410 = CDR(((obj_t) BgL_ll1238z00_1404));
							{
								obj_t BgL_ll1238z00_2846;
								obj_t BgL_ll1237z00_2845;

								BgL_ll1237z00_2845 = BgL_arg1308z00_1409;
								BgL_ll1238z00_2846 = BgL_arg1310z00_1410;
								BgL_ll1238z00_1404 = BgL_ll1238z00_2846;
								BgL_ll1237z00_1403 = BgL_ll1237z00_2845;
								goto BgL_zc3z04anonymousza31306ze3z87_1405;
							}
						}
					}
			}
			{	/* Ast/substitute.scm 42 */
				BgL_nodez00_bglt BgL_resz00_1412;

				BgL_resz00_1412 =
					BGl_dozd2substitutez12zc0zzast_substitutez00(BgL_nodez00_5,
					BgL_sitez00_6);
				{
					obj_t BgL_l1240z00_1414;

					BgL_l1240z00_1414 = BgL_whatza2za2_3;
				BgL_zc3z04anonymousza31311ze3z87_1415:
					if (PAIRP(BgL_l1240z00_1414))
						{	/* Ast/substitute.scm 44 */
							{	/* Ast/substitute.scm 45 */
								obj_t BgL_whatz00_1417;

								BgL_whatz00_1417 = CAR(BgL_l1240z00_1414);
								((((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_whatz00_1417)))->
										BgL_fastzd2alphazd2) = ((obj_t) BUNSPEC), BUNSPEC);
							}
							{
								obj_t BgL_l1240z00_2853;

								BgL_l1240z00_2853 = CDR(BgL_l1240z00_1414);
								BgL_l1240z00_1414 = BgL_l1240z00_2853;
								goto BgL_zc3z04anonymousza31311ze3z87_1415;
							}
						}
					else
						{	/* Ast/substitute.scm 44 */
							((bool_t) 1);
						}
				}
				return BgL_resz00_1412;
			}
		}

	}



/* &substitute! */
	BgL_nodez00_bglt BGl_z62substitutez12z70zzast_substitutez00(obj_t
		BgL_envz00_2456, obj_t BgL_whatza2za2_2457, obj_t BgL_byza2za2_2458,
		obj_t BgL_nodez00_2459, obj_t BgL_sitez00_2460)
	{
		{	/* Ast/substitute.scm 34 */
			return
				BGl_substitutez12z12zzast_substitutez00(BgL_whatza2za2_2457,
				BgL_byza2za2_2458, ((BgL_nodez00_bglt) BgL_nodez00_2459),
				BgL_sitez00_2460);
		}

	}



/* do-substitute*! */
	obj_t BGl_dozd2substituteza2z12z62zzast_substitutez00(obj_t BgL_nodeza2za2_51,
		obj_t BgL_sitez00_52)
	{
		{	/* Ast/substitute.scm 275 */
		BGl_dozd2substituteza2z12z62zzast_substitutez00:
			if (NULLP(BgL_nodeza2za2_51))
				{	/* Ast/substitute.scm 277 */
					return CNST_TABLE_REF(0);
				}
			else
				{	/* Ast/substitute.scm 277 */
					if (NULLP(CDR(((obj_t) BgL_nodeza2za2_51))))
						{	/* Ast/substitute.scm 279 */
							{	/* Ast/substitute.scm 280 */
								BgL_nodez00_bglt BgL_arg1318z00_1423;

								{	/* Ast/substitute.scm 280 */
									obj_t BgL_arg1319z00_1424;

									BgL_arg1319z00_1424 = CAR(((obj_t) BgL_nodeza2za2_51));
									BgL_arg1318z00_1423 =
										BGl_dozd2substitutez12zc0zzast_substitutez00(
										((BgL_nodez00_bglt) BgL_arg1319z00_1424), BgL_sitez00_52);
								}
								{	/* Ast/substitute.scm 280 */
									obj_t BgL_auxz00_2870;
									obj_t BgL_tmpz00_2868;

									BgL_auxz00_2870 = ((obj_t) BgL_arg1318z00_1423);
									BgL_tmpz00_2868 = ((obj_t) BgL_nodeza2za2_51);
									SET_CAR(BgL_tmpz00_2868, BgL_auxz00_2870);
								}
							}
							return CNST_TABLE_REF(0);
						}
					else
						{	/* Ast/substitute.scm 279 */
							{	/* Ast/substitute.scm 283 */
								BgL_nodez00_bglt BgL_arg1320z00_1425;

								{	/* Ast/substitute.scm 283 */
									obj_t BgL_arg1321z00_1426;

									BgL_arg1321z00_1426 = CAR(((obj_t) BgL_nodeza2za2_51));
									BgL_arg1320z00_1425 =
										BGl_dozd2substitutez12zc0zzast_substitutez00(
										((BgL_nodez00_bglt) BgL_arg1321z00_1426),
										CNST_TABLE_REF(1));
								}
								{	/* Ast/substitute.scm 283 */
									obj_t BgL_auxz00_2881;
									obj_t BgL_tmpz00_2879;

									BgL_auxz00_2881 = ((obj_t) BgL_arg1320z00_1425);
									BgL_tmpz00_2879 = ((obj_t) BgL_nodeza2za2_51);
									SET_CAR(BgL_tmpz00_2879, BgL_auxz00_2881);
								}
							}
							{	/* Ast/substitute.scm 284 */
								obj_t BgL_arg1322z00_1427;

								BgL_arg1322z00_1427 = CDR(((obj_t) BgL_nodeza2za2_51));
								{
									obj_t BgL_nodeza2za2_2886;

									BgL_nodeza2za2_2886 = BgL_arg1322z00_1427;
									BgL_nodeza2za2_51 = BgL_nodeza2za2_2886;
									goto BGl_dozd2substituteza2z12z62zzast_substitutez00;
								}
							}
						}
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_substitutez00(void)
	{
		{	/* Ast/substitute.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_substitutez00(void)
	{
		{	/* Ast/substitute.scm 14 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_dozd2substitutez12zd2envz12zzast_substitutez00,
				BGl_proc1858z00zzast_substitutez00, BGl_nodez00zzast_nodez00,
				BGl_string1859z00zzast_substitutez00);
		}

	}



/* &do-substitute!1256 */
	obj_t BGl_z62dozd2substitutez121256za2zzast_substitutez00(obj_t
		BgL_envz00_2462, obj_t BgL_nodez00_2463, obj_t BgL_sitez00_2464)
	{
		{	/* Ast/substitute.scm 52 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(2),
				BGl_string1860z00zzast_substitutez00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_2463)));
		}

	}



/* do-substitute! */
	BgL_nodez00_bglt BGl_dozd2substitutez12zc0zzast_substitutez00(BgL_nodez00_bglt
		BgL_nodez00_7, obj_t BgL_sitez00_8)
	{
		{	/* Ast/substitute.scm 52 */
			{	/* Ast/substitute.scm 52 */
				obj_t BgL_method1257z00_1434;

				{	/* Ast/substitute.scm 52 */
					obj_t BgL_res1844z00_1906;

					{	/* Ast/substitute.scm 52 */
						long BgL_objzd2classzd2numz00_1877;

						BgL_objzd2classzd2numz00_1877 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_7));
						{	/* Ast/substitute.scm 52 */
							obj_t BgL_arg1811z00_1878;

							BgL_arg1811z00_1878 =
								PROCEDURE_REF
								(BGl_dozd2substitutez12zd2envz12zzast_substitutez00,
								(int) (1L));
							{	/* Ast/substitute.scm 52 */
								int BgL_offsetz00_1881;

								BgL_offsetz00_1881 = (int) (BgL_objzd2classzd2numz00_1877);
								{	/* Ast/substitute.scm 52 */
									long BgL_offsetz00_1882;

									BgL_offsetz00_1882 =
										((long) (BgL_offsetz00_1881) - OBJECT_TYPE);
									{	/* Ast/substitute.scm 52 */
										long BgL_modz00_1883;

										BgL_modz00_1883 =
											(BgL_offsetz00_1882 >> (int) ((long) ((int) (4L))));
										{	/* Ast/substitute.scm 52 */
											long BgL_restz00_1885;

											BgL_restz00_1885 =
												(BgL_offsetz00_1882 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/substitute.scm 52 */

												{	/* Ast/substitute.scm 52 */
													obj_t BgL_bucketz00_1887;

													BgL_bucketz00_1887 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_1878), BgL_modz00_1883);
													BgL_res1844z00_1906 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_1887), BgL_restz00_1885);
					}}}}}}}}
					BgL_method1257z00_1434 = BgL_res1844z00_1906;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL2(BgL_method1257z00_1434,
						((obj_t) BgL_nodez00_7), BgL_sitez00_8));
			}
		}

	}



/* &do-substitute! */
	BgL_nodez00_bglt BGl_z62dozd2substitutez12za2zzast_substitutez00(obj_t
		BgL_envz00_2465, obj_t BgL_nodez00_2466, obj_t BgL_sitez00_2467)
	{
		{	/* Ast/substitute.scm 52 */
			return
				BGl_dozd2substitutez12zc0zzast_substitutez00(
				((BgL_nodez00_bglt) BgL_nodez00_2466), BgL_sitez00_2467);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_substitutez00(void)
	{
		{	/* Ast/substitute.scm 14 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2substitutez12zd2envz12zzast_substitutez00,
				BGl_atomz00zzast_nodez00, BGl_proc1861z00zzast_substitutez00,
				BGl_string1862z00zzast_substitutez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2substitutez12zd2envz12zzast_substitutez00,
				BGl_varz00zzast_nodez00, BGl_proc1863z00zzast_substitutez00,
				BGl_string1862z00zzast_substitutez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2substitutez12zd2envz12zzast_substitutez00,
				BGl_kwotez00zzast_nodez00, BGl_proc1864z00zzast_substitutez00,
				BGl_string1862z00zzast_substitutez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2substitutez12zd2envz12zzast_substitutez00,
				BGl_sequencez00zzast_nodez00, BGl_proc1865z00zzast_substitutez00,
				BGl_string1862z00zzast_substitutez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2substitutez12zd2envz12zzast_substitutez00,
				BGl_syncz00zzast_nodez00, BGl_proc1866z00zzast_substitutez00,
				BGl_string1862z00zzast_substitutez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2substitutez12zd2envz12zzast_substitutez00,
				BGl_appz00zzast_nodez00, BGl_proc1867z00zzast_substitutez00,
				BGl_string1862z00zzast_substitutez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2substitutez12zd2envz12zzast_substitutez00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1868z00zzast_substitutez00,
				BGl_string1862z00zzast_substitutez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2substitutez12zd2envz12zzast_substitutez00,
				BGl_funcallz00zzast_nodez00, BGl_proc1869z00zzast_substitutez00,
				BGl_string1862z00zzast_substitutez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2substitutez12zd2envz12zzast_substitutez00,
				BGl_externz00zzast_nodez00, BGl_proc1870z00zzast_substitutez00,
				BGl_string1862z00zzast_substitutez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2substitutez12zd2envz12zzast_substitutez00,
				BGl_castz00zzast_nodez00, BGl_proc1871z00zzast_substitutez00,
				BGl_string1862z00zzast_substitutez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2substitutez12zd2envz12zzast_substitutez00,
				BGl_setqz00zzast_nodez00, BGl_proc1872z00zzast_substitutez00,
				BGl_string1862z00zzast_substitutez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2substitutez12zd2envz12zzast_substitutez00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1873z00zzast_substitutez00,
				BGl_string1862z00zzast_substitutez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2substitutez12zd2envz12zzast_substitutez00,
				BGl_failz00zzast_nodez00, BGl_proc1874z00zzast_substitutez00,
				BGl_string1862z00zzast_substitutez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2substitutez12zd2envz12zzast_substitutez00,
				BGl_switchz00zzast_nodez00, BGl_proc1875z00zzast_substitutez00,
				BGl_string1862z00zzast_substitutez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2substitutez12zd2envz12zzast_substitutez00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1876z00zzast_substitutez00,
				BGl_string1862z00zzast_substitutez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2substitutez12zd2envz12zzast_substitutez00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1877z00zzast_substitutez00,
				BGl_string1862z00zzast_substitutez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2substitutez12zd2envz12zzast_substitutez00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc1878z00zzast_substitutez00,
				BGl_string1862z00zzast_substitutez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2substitutez12zd2envz12zzast_substitutez00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc1879z00zzast_substitutez00,
				BGl_string1862z00zzast_substitutez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2substitutez12zd2envz12zzast_substitutez00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1880z00zzast_substitutez00,
				BGl_string1862z00zzast_substitutez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2substitutez12zd2envz12zzast_substitutez00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1881z00zzast_substitutez00,
				BGl_string1862z00zzast_substitutez00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2substitutez12zd2envz12zzast_substitutez00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1882z00zzast_substitutez00,
				BGl_string1862z00zzast_substitutez00);
		}

	}



/* &do-substitute!-box-s1299 */
	BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2boxzd2s1299za2zzast_substitutez00(obj_t
		BgL_envz00_2489, obj_t BgL_nodez00_2490, obj_t BgL_sitez00_2491)
	{
		{	/* Ast/substitute.scm 267 */
			{	/* Ast/substitute.scm 268 */
				BgL_nodez00_bglt BgL_arg1702z00_2565;

				{	/* Ast/substitute.scm 268 */
					BgL_varz00_bglt BgL_arg1703z00_2566;

					BgL_arg1703z00_2566 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2490)))->BgL_varz00);
					BgL_arg1702z00_2565 =
						BGl_dozd2substitutez12zc0zzast_substitutez00(
						((BgL_nodez00_bglt) BgL_arg1703z00_2566), CNST_TABLE_REF(1));
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2490)))->BgL_varz00) =
					((BgL_varz00_bglt) ((BgL_varz00_bglt) BgL_arg1702z00_2565)), BUNSPEC);
			}
			{	/* Ast/substitute.scm 269 */
				BgL_nodez00_bglt BgL_arg1705z00_2567;

				{	/* Ast/substitute.scm 269 */
					BgL_nodez00_bglt BgL_arg1708z00_2568;

					BgL_arg1708z00_2568 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2490)))->BgL_valuez00);
					BgL_arg1705z00_2567 =
						BGl_dozd2substitutez12zc0zzast_substitutez00(BgL_arg1708z00_2568,
						CNST_TABLE_REF(1));
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2490)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_arg1705z00_2567), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2490));
		}

	}



/* &do-substitute!-box-r1297 */
	BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2boxzd2r1297za2zzast_substitutez00(obj_t
		BgL_envz00_2492, obj_t BgL_nodez00_2493, obj_t BgL_sitez00_2494)
	{
		{	/* Ast/substitute.scm 260 */
			{	/* Ast/substitute.scm 261 */
				BgL_nodez00_bglt BgL_arg1700z00_2570;

				{	/* Ast/substitute.scm 261 */
					BgL_varz00_bglt BgL_arg1701z00_2571;

					BgL_arg1701z00_2571 =
						(((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_2493)))->BgL_varz00);
					BgL_arg1700z00_2570 =
						BGl_dozd2substitutez12zc0zzast_substitutez00(
						((BgL_nodez00_bglt) BgL_arg1701z00_2571), CNST_TABLE_REF(1));
				}
				((((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_2493)))->BgL_varz00) =
					((BgL_varz00_bglt) ((BgL_varz00_bglt) BgL_arg1700z00_2570)), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_2493));
		}

	}



/* &do-substitute!-make-1295 */
	BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2makezd21295za2zzast_substitutez00(obj_t
		BgL_envz00_2495, obj_t BgL_nodez00_2496, obj_t BgL_sitez00_2497)
	{
		{	/* Ast/substitute.scm 253 */
			{	/* Ast/substitute.scm 254 */
				BgL_nodez00_bglt BgL_arg1692z00_2573;

				{	/* Ast/substitute.scm 254 */
					BgL_nodez00_bglt BgL_arg1699z00_2574;

					BgL_arg1699z00_2574 =
						(((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_2496)))->BgL_valuez00);
					BgL_arg1692z00_2573 =
						BGl_dozd2substitutez12zc0zzast_substitutez00(BgL_arg1699z00_2574,
						CNST_TABLE_REF(1));
				}
				((((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_2496)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_arg1692z00_2573), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_2496));
		}

	}



/* &do-substitute!-jump-1293 */
	BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2jumpzd21293za2zzast_substitutez00(obj_t
		BgL_envz00_2498, obj_t BgL_nodez00_2499, obj_t BgL_sitez00_2500)
	{
		{	/* Ast/substitute.scm 245 */
			{	/* Ast/substitute.scm 246 */
				BgL_nodez00_bglt BgL_arg1681z00_2576;

				{	/* Ast/substitute.scm 246 */
					BgL_nodez00_bglt BgL_arg1688z00_2577;

					BgL_arg1688z00_2577 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2499)))->BgL_exitz00);
					BgL_arg1681z00_2576 =
						BGl_dozd2substitutez12zc0zzast_substitutez00(BgL_arg1688z00_2577,
						CNST_TABLE_REF(3));
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2499)))->
						BgL_exitz00) = ((BgL_nodez00_bglt) BgL_arg1681z00_2576), BUNSPEC);
			}
			{	/* Ast/substitute.scm 247 */
				BgL_nodez00_bglt BgL_arg1689z00_2578;

				{	/* Ast/substitute.scm 247 */
					BgL_nodez00_bglt BgL_arg1691z00_2579;

					BgL_arg1691z00_2579 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2499)))->
						BgL_valuez00);
					BgL_arg1689z00_2578 =
						BGl_dozd2substitutez12zc0zzast_substitutez00(BgL_arg1691z00_2579,
						CNST_TABLE_REF(1));
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2499)))->
						BgL_valuez00) = ((BgL_nodez00_bglt) BgL_arg1689z00_2578), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2499));
		}

	}



/* &do-substitute!-set-e1291 */
	BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2setzd2e1291za2zzast_substitutez00(obj_t
		BgL_envz00_2501, obj_t BgL_nodez00_2502, obj_t BgL_sitez00_2503)
	{
		{	/* Ast/substitute.scm 237 */
			((((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2502)))->BgL_bodyz00) =
				((BgL_nodez00_bglt)
					BGl_dozd2substitutez12zc0zzast_substitutez00(((
								(BgL_setzd2exzd2itz00_bglt) COBJECT(((BgL_setzd2exzd2itz00_bglt)
										BgL_nodez00_2502)))->BgL_bodyz00), BgL_sitez00_2503)),
				BUNSPEC);
			((((BgL_setzd2exzd2itz00_bglt) COBJECT(((BgL_setzd2exzd2itz00_bglt)
								BgL_nodez00_2502)))->BgL_onexitz00) =
				((BgL_nodez00_bglt)
					BGl_dozd2substitutez12zc0zzast_substitutez00(((
								(BgL_setzd2exzd2itz00_bglt) COBJECT(((BgL_setzd2exzd2itz00_bglt)
										BgL_nodez00_2502)))->BgL_onexitz00), BgL_sitez00_2503)),
				BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt)
					BgL_nodez00_2502));
		}

	}



/* &do-substitute!-let-v1289 */
	BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2letzd2v1289za2zzast_substitutez00(obj_t
		BgL_envz00_2504, obj_t BgL_nodez00_2505, obj_t BgL_sitez00_2506)
	{
		{	/* Ast/substitute.scm 227 */
			{	/* Ast/substitute.scm 228 */
				obj_t BgL_g1255z00_2582;

				BgL_g1255z00_2582 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2505)))->BgL_bindingsz00);
				{
					obj_t BgL_l1253z00_2584;

					BgL_l1253z00_2584 = BgL_g1255z00_2582;
				BgL_zc3z04anonymousza31630ze3z87_2583:
					if (PAIRP(BgL_l1253z00_2584))
						{	/* Ast/substitute.scm 230 */
							{	/* Ast/substitute.scm 229 */
								obj_t BgL_bindingz00_2585;

								BgL_bindingz00_2585 = CAR(BgL_l1253z00_2584);
								{	/* Ast/substitute.scm 229 */
									BgL_nodez00_bglt BgL_arg1642z00_2586;

									{	/* Ast/substitute.scm 229 */
										obj_t BgL_arg1646z00_2587;

										BgL_arg1646z00_2587 = CDR(((obj_t) BgL_bindingz00_2585));
										BgL_arg1642z00_2586 =
											BGl_dozd2substitutez12zc0zzast_substitutez00(
											((BgL_nodez00_bglt) BgL_arg1646z00_2587),
											CNST_TABLE_REF(1));
									}
									{	/* Ast/substitute.scm 229 */
										obj_t BgL_auxz00_3019;
										obj_t BgL_tmpz00_3017;

										BgL_auxz00_3019 = ((obj_t) BgL_arg1642z00_2586);
										BgL_tmpz00_3017 = ((obj_t) BgL_bindingz00_2585);
										SET_CDR(BgL_tmpz00_3017, BgL_auxz00_3019);
									}
								}
							}
							{
								obj_t BgL_l1253z00_3022;

								BgL_l1253z00_3022 = CDR(BgL_l1253z00_2584);
								BgL_l1253z00_2584 = BgL_l1253z00_3022;
								goto BgL_zc3z04anonymousza31630ze3z87_2583;
							}
						}
					else
						{	/* Ast/substitute.scm 230 */
							((bool_t) 1);
						}
				}
			}
			((((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2505)))->BgL_bodyz00) =
				((BgL_nodez00_bglt)
					BGl_dozd2substitutez12zc0zzast_substitutez00((((BgL_letzd2varzd2_bglt)
								COBJECT(((BgL_letzd2varzd2_bglt) BgL_nodez00_2505)))->
							BgL_bodyz00), BgL_sitez00_2506)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nodez00_2505));
		}

	}



/* &do-substitute!-let-f1287 */
	BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2letzd2f1287za2zzast_substitutez00(obj_t
		BgL_envz00_2507, obj_t BgL_nodez00_2508, obj_t BgL_sitez00_2509)
	{
		{	/* Ast/substitute.scm 215 */
			{	/* Ast/substitute.scm 216 */
				obj_t BgL_g1252z00_2589;

				BgL_g1252z00_2589 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2508)))->BgL_localsz00);
				{
					obj_t BgL_l1250z00_2591;

					BgL_l1250z00_2591 = BgL_g1252z00_2589;
				BgL_zc3z04anonymousza31614ze3z87_2590:
					if (PAIRP(BgL_l1250z00_2591))
						{	/* Ast/substitute.scm 220 */
							{	/* Ast/substitute.scm 217 */
								obj_t BgL_localz00_2592;

								BgL_localz00_2592 = CAR(BgL_l1250z00_2591);
								{	/* Ast/substitute.scm 217 */
									BgL_valuez00_bglt BgL_funz00_2593;

									BgL_funz00_2593 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_localz00_2592))))->
										BgL_valuez00);
									{	/* Ast/substitute.scm 218 */
										BgL_nodez00_bglt BgL_arg1616z00_2594;

										{	/* Ast/substitute.scm 218 */
											obj_t BgL_arg1625z00_2595;

											BgL_arg1625z00_2595 =
												(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_funz00_2593)))->
												BgL_bodyz00);
											BgL_arg1616z00_2594 =
												BGl_dozd2substitutez12zc0zzast_substitutez00((
													(BgL_nodez00_bglt) BgL_arg1625z00_2595),
												CNST_TABLE_REF(1));
										}
										((((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_funz00_2593)))->
												BgL_bodyz00) =
											((obj_t) ((obj_t) BgL_arg1616z00_2594)), BUNSPEC);
									}
								}
							}
							{
								obj_t BgL_l1250z00_3047;

								BgL_l1250z00_3047 = CDR(BgL_l1250z00_2591);
								BgL_l1250z00_2591 = BgL_l1250z00_3047;
								goto BgL_zc3z04anonymousza31614ze3z87_2590;
							}
						}
					else
						{	/* Ast/substitute.scm 220 */
							((bool_t) 1);
						}
				}
			}
			((((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2508)))->BgL_bodyz00) =
				((BgL_nodez00_bglt)
					BGl_dozd2substitutez12zc0zzast_substitutez00((((BgL_letzd2funzd2_bglt)
								COBJECT(((BgL_letzd2funzd2_bglt) BgL_nodez00_2508)))->
							BgL_bodyz00), BgL_sitez00_2509)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nodez00_2508));
		}

	}



/* &do-substitute!-switc1285 */
	BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2switc1285z70zzast_substitutez00(obj_t
		BgL_envz00_2510, obj_t BgL_nodez00_2511, obj_t BgL_sitez00_2512)
	{
		{	/* Ast/substitute.scm 205 */
			{	/* Ast/substitute.scm 206 */
				BgL_nodez00_bglt BgL_arg1605z00_2597;

				{	/* Ast/substitute.scm 206 */
					BgL_nodez00_bglt BgL_arg1606z00_2598;

					BgL_arg1606z00_2598 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2511)))->BgL_testz00);
					BgL_arg1605z00_2597 =
						BGl_dozd2substitutez12zc0zzast_substitutez00(BgL_arg1606z00_2598,
						CNST_TABLE_REF(1));
				}
				((((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2511)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_arg1605z00_2597), BUNSPEC);
			}
			{	/* Ast/substitute.scm 207 */
				obj_t BgL_g1249z00_2599;

				BgL_g1249z00_2599 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_2511)))->BgL_clausesz00);
				{
					obj_t BgL_l1247z00_2601;

					BgL_l1247z00_2601 = BgL_g1249z00_2599;
				BgL_zc3z04anonymousza31607ze3z87_2600:
					if (PAIRP(BgL_l1247z00_2601))
						{	/* Ast/substitute.scm 209 */
							{	/* Ast/substitute.scm 208 */
								obj_t BgL_clausez00_2602;

								BgL_clausez00_2602 = CAR(BgL_l1247z00_2601);
								{	/* Ast/substitute.scm 208 */
									BgL_nodez00_bglt BgL_arg1609z00_2603;

									{	/* Ast/substitute.scm 208 */
										obj_t BgL_arg1611z00_2604;

										BgL_arg1611z00_2604 = CDR(((obj_t) BgL_clausez00_2602));
										BgL_arg1609z00_2603 =
											BGl_dozd2substitutez12zc0zzast_substitutez00(
											((BgL_nodez00_bglt) BgL_arg1611z00_2604),
											BgL_sitez00_2512);
									}
									{	/* Ast/substitute.scm 208 */
										obj_t BgL_auxz00_3073;
										obj_t BgL_tmpz00_3071;

										BgL_auxz00_3073 = ((obj_t) BgL_arg1609z00_2603);
										BgL_tmpz00_3071 = ((obj_t) BgL_clausez00_2602);
										SET_CDR(BgL_tmpz00_3071, BgL_auxz00_3073);
									}
								}
							}
							{
								obj_t BgL_l1247z00_3076;

								BgL_l1247z00_3076 = CDR(BgL_l1247z00_2601);
								BgL_l1247z00_2601 = BgL_l1247z00_3076;
								goto BgL_zc3z04anonymousza31607ze3z87_2600;
							}
						}
					else
						{	/* Ast/substitute.scm 209 */
							((bool_t) 1);
						}
				}
			}
			return ((BgL_nodez00_bglt) ((BgL_switchz00_bglt) BgL_nodez00_2511));
		}

	}



/* &do-substitute!-fail1283 */
	BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2fail1283z70zzast_substitutez00(obj_t
		BgL_envz00_2513, obj_t BgL_nodez00_2514, obj_t BgL_sitez00_2515)
	{
		{	/* Ast/substitute.scm 195 */
			{
				BgL_nodez00_bglt BgL_auxz00_3080;

				{	/* Ast/substitute.scm 197 */
					BgL_nodez00_bglt BgL_arg1594z00_2606;

					BgL_arg1594z00_2606 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2514)))->BgL_procz00);
					BgL_auxz00_3080 =
						BGl_dozd2substitutez12zc0zzast_substitutez00(BgL_arg1594z00_2606,
						CNST_TABLE_REF(1));
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2514)))->BgL_procz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3080), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3087;

				{	/* Ast/substitute.scm 198 */
					BgL_nodez00_bglt BgL_arg1595z00_2607;

					BgL_arg1595z00_2607 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2514)))->BgL_msgz00);
					BgL_auxz00_3087 =
						BGl_dozd2substitutez12zc0zzast_substitutez00(BgL_arg1595z00_2607,
						CNST_TABLE_REF(1));
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2514)))->BgL_msgz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3087), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3094;

				{	/* Ast/substitute.scm 199 */
					BgL_nodez00_bglt BgL_arg1602z00_2608;

					BgL_arg1602z00_2608 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2514)))->BgL_objz00);
					BgL_auxz00_3094 =
						BGl_dozd2substitutez12zc0zzast_substitutez00(BgL_arg1602z00_2608,
						CNST_TABLE_REF(1));
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2514)))->BgL_objz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3094), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_failz00_bglt) BgL_nodez00_2514));
		}

	}



/* &do-substitute!-condi1281 */
	BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2condi1281z70zzast_substitutez00(obj_t
		BgL_envz00_2516, obj_t BgL_nodez00_2517, obj_t BgL_sitez00_2518)
	{
		{	/* Ast/substitute.scm 185 */
			{
				BgL_nodez00_bglt BgL_auxz00_3103;

				{	/* Ast/substitute.scm 187 */
					BgL_nodez00_bglt BgL_arg1589z00_2610;

					BgL_arg1589z00_2610 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2517)))->BgL_testz00);
					BgL_auxz00_3103 =
						BGl_dozd2substitutez12zc0zzast_substitutez00(BgL_arg1589z00_2610,
						CNST_TABLE_REF(1));
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2517)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3103), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3110;

				{	/* Ast/substitute.scm 188 */
					BgL_nodez00_bglt BgL_arg1591z00_2611;

					BgL_arg1591z00_2611 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2517)))->BgL_truez00);
					BgL_auxz00_3110 =
						BGl_dozd2substitutez12zc0zzast_substitutez00(BgL_arg1591z00_2611,
						BgL_sitez00_2518);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2517)))->BgL_truez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3110), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3116;

				{	/* Ast/substitute.scm 189 */
					BgL_nodez00_bglt BgL_arg1593z00_2612;

					BgL_arg1593z00_2612 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2517)))->BgL_falsez00);
					BgL_auxz00_3116 =
						BGl_dozd2substitutez12zc0zzast_substitutez00(BgL_arg1593z00_2612,
						BgL_sitez00_2518);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2517)))->BgL_falsez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3116), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt) BgL_nodez00_2517));
		}

	}



/* &do-substitute!-setq1279 */
	BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2setq1279z70zzast_substitutez00(obj_t
		BgL_envz00_2519, obj_t BgL_nodez00_2520, obj_t BgL_sitez00_2521)
	{
		{	/* Ast/substitute.scm 176 */
			{
				BgL_varz00_bglt BgL_auxz00_3124;

				{	/* Ast/substitute.scm 178 */
					BgL_varz00_bglt BgL_arg1584z00_2614;

					BgL_arg1584z00_2614 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2520)))->BgL_varz00);
					BgL_auxz00_3124 =
						((BgL_varz00_bglt)
						BGl_dozd2substitutez12zc0zzast_substitutez00(
							((BgL_nodez00_bglt) BgL_arg1584z00_2614), CNST_TABLE_REF(4)));
				}
				((((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2520)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_3124), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3133;

				{	/* Ast/substitute.scm 179 */
					BgL_nodez00_bglt BgL_arg1585z00_2615;

					BgL_arg1585z00_2615 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2520)))->BgL_valuez00);
					BgL_auxz00_3133 =
						BGl_dozd2substitutez12zc0zzast_substitutez00(BgL_arg1585z00_2615,
						BgL_sitez00_2521);
				}
				((((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2520)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3133), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_setqz00_bglt) BgL_nodez00_2520));
		}

	}



/* &do-substitute!-cast1277 */
	BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2cast1277z70zzast_substitutez00(obj_t
		BgL_envz00_2522, obj_t BgL_nodez00_2523, obj_t BgL_sitez00_2524)
	{
		{	/* Ast/substitute.scm 169 */
			((((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_2523)))->BgL_argz00) =
				((BgL_nodez00_bglt)
					BGl_dozd2substitutez12zc0zzast_substitutez00((((BgL_castz00_bglt)
								COBJECT(((BgL_castz00_bglt) BgL_nodez00_2523)))->BgL_argz00),
						BgL_sitez00_2524)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_castz00_bglt) BgL_nodez00_2523));
		}

	}



/* &do-substitute!-exter1275 */
	BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2exter1275z70zzast_substitutez00(obj_t
		BgL_envz00_2525, obj_t BgL_nodez00_2526, obj_t BgL_sitez00_2527)
	{
		{	/* Ast/substitute.scm 162 */
			BGl_dozd2substituteza2z12z62zzast_substitutez00(
				(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_2526)))->BgL_exprza2za2),
				BgL_sitez00_2527);
			return ((BgL_nodez00_bglt) ((BgL_externz00_bglt) BgL_nodez00_2526));
		}

	}



/* &do-substitute!-funca1273 */
	BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2funca1273z70zzast_substitutez00(obj_t
		BgL_envz00_2528, obj_t BgL_nodez00_2529, obj_t BgL_sitez00_2530)
	{
		{	/* Ast/substitute.scm 140 */
			{	/* Ast/substitute.scm 142 */
				BgL_nodez00_bglt BgL_nfunz00_2619;
				obj_t BgL_nargsz00_2620;

				{	/* Ast/substitute.scm 142 */
					BgL_nodez00_bglt BgL_arg1561z00_2621;

					BgL_arg1561z00_2621 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2529)))->BgL_funz00);
					BgL_nfunz00_2619 =
						BGl_dozd2substitutez12zc0zzast_substitutez00(BgL_arg1561z00_2621,
						CNST_TABLE_REF(1));
				}
				{	/* Ast/substitute.scm 143 */
					obj_t BgL_l1242z00_2622;

					BgL_l1242z00_2622 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2529)))->BgL_argsz00);
					if (NULLP(BgL_l1242z00_2622))
						{	/* Ast/substitute.scm 143 */
							BgL_nargsz00_2620 = BNIL;
						}
					else
						{	/* Ast/substitute.scm 143 */
							obj_t BgL_head1244z00_2623;

							BgL_head1244z00_2623 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1242z00_2625;
								obj_t BgL_tail1245z00_2626;

								BgL_l1242z00_2625 = BgL_l1242z00_2622;
								BgL_tail1245z00_2626 = BgL_head1244z00_2623;
							BgL_zc3z04anonymousza31563ze3z87_2624:
								if (NULLP(BgL_l1242z00_2625))
									{	/* Ast/substitute.scm 143 */
										BgL_nargsz00_2620 = CDR(BgL_head1244z00_2623);
									}
								else
									{	/* Ast/substitute.scm 143 */
										obj_t BgL_newtail1246z00_2627;

										{	/* Ast/substitute.scm 143 */
											BgL_nodez00_bglt BgL_arg1571z00_2628;

											{	/* Ast/substitute.scm 143 */
												obj_t BgL_az00_2629;

												BgL_az00_2629 = CAR(((obj_t) BgL_l1242z00_2625));
												BgL_arg1571z00_2628 =
													BGl_dozd2substitutez12zc0zzast_substitutez00(
													((BgL_nodez00_bglt) BgL_az00_2629),
													CNST_TABLE_REF(1));
											}
											BgL_newtail1246z00_2627 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg1571z00_2628), BNIL);
										}
										SET_CDR(BgL_tail1245z00_2626, BgL_newtail1246z00_2627);
										{	/* Ast/substitute.scm 143 */
											obj_t BgL_arg1565z00_2630;

											BgL_arg1565z00_2630 = CDR(((obj_t) BgL_l1242z00_2625));
											{
												obj_t BgL_tail1245z00_3176;
												obj_t BgL_l1242z00_3175;

												BgL_l1242z00_3175 = BgL_arg1565z00_2630;
												BgL_tail1245z00_3176 = BgL_newtail1246z00_2627;
												BgL_tail1245z00_2626 = BgL_tail1245z00_3176;
												BgL_l1242z00_2625 = BgL_l1242z00_3175;
												goto BgL_zc3z04anonymousza31563ze3z87_2624;
											}
										}
									}
							}
						}
				}
				{	/* Ast/substitute.scm 144 */
					bool_t BgL_test1934z00_3177;

					{	/* Ast/substitute.scm 144 */
						bool_t BgL_test1935z00_3178;

						{	/* Ast/substitute.scm 144 */
							obj_t BgL_classz00_2631;

							BgL_classz00_2631 = BGl_closurez00zzast_nodez00;
							{	/* Ast/substitute.scm 144 */
								BgL_objectz00_bglt BgL_arg1807z00_2632;

								{	/* Ast/substitute.scm 144 */
									obj_t BgL_tmpz00_3179;

									BgL_tmpz00_3179 =
										((obj_t) ((BgL_objectz00_bglt) BgL_nfunz00_2619));
									BgL_arg1807z00_2632 = (BgL_objectz00_bglt) (BgL_tmpz00_3179);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Ast/substitute.scm 144 */
										long BgL_idxz00_2633;

										BgL_idxz00_2633 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2632);
										BgL_test1935z00_3178 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2633 + 3L)) == BgL_classz00_2631);
									}
								else
									{	/* Ast/substitute.scm 144 */
										bool_t BgL_res1850z00_2636;

										{	/* Ast/substitute.scm 144 */
											obj_t BgL_oclassz00_2637;

											{	/* Ast/substitute.scm 144 */
												obj_t BgL_arg1815z00_2638;
												long BgL_arg1816z00_2639;

												BgL_arg1815z00_2638 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Ast/substitute.scm 144 */
													long BgL_arg1817z00_2640;

													BgL_arg1817z00_2640 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2632);
													BgL_arg1816z00_2639 =
														(BgL_arg1817z00_2640 - OBJECT_TYPE);
												}
												BgL_oclassz00_2637 =
													VECTOR_REF(BgL_arg1815z00_2638, BgL_arg1816z00_2639);
											}
											{	/* Ast/substitute.scm 144 */
												bool_t BgL__ortest_1115z00_2641;

												BgL__ortest_1115z00_2641 =
													(BgL_classz00_2631 == BgL_oclassz00_2637);
												if (BgL__ortest_1115z00_2641)
													{	/* Ast/substitute.scm 144 */
														BgL_res1850z00_2636 = BgL__ortest_1115z00_2641;
													}
												else
													{	/* Ast/substitute.scm 144 */
														long BgL_odepthz00_2642;

														{	/* Ast/substitute.scm 144 */
															obj_t BgL_arg1804z00_2643;

															BgL_arg1804z00_2643 = (BgL_oclassz00_2637);
															BgL_odepthz00_2642 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2643);
														}
														if ((3L < BgL_odepthz00_2642))
															{	/* Ast/substitute.scm 144 */
																obj_t BgL_arg1802z00_2644;

																{	/* Ast/substitute.scm 144 */
																	obj_t BgL_arg1803z00_2645;

																	BgL_arg1803z00_2645 = (BgL_oclassz00_2637);
																	BgL_arg1802z00_2644 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2645,
																		3L);
																}
																BgL_res1850z00_2636 =
																	(BgL_arg1802z00_2644 == BgL_classz00_2631);
															}
														else
															{	/* Ast/substitute.scm 144 */
																BgL_res1850z00_2636 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1935z00_3178 = BgL_res1850z00_2636;
									}
							}
						}
						if (BgL_test1935z00_3178)
							{	/* Ast/substitute.scm 144 */
								BgL_test1934z00_3177 = ((bool_t) 1);
							}
						else
							{	/* Ast/substitute.scm 145 */
								bool_t BgL_test1939z00_3202;

								{	/* Ast/substitute.scm 145 */
									obj_t BgL_classz00_2646;

									BgL_classz00_2646 = BGl_varz00zzast_nodez00;
									{	/* Ast/substitute.scm 145 */
										BgL_objectz00_bglt BgL_arg1807z00_2647;

										{	/* Ast/substitute.scm 145 */
											obj_t BgL_tmpz00_3203;

											BgL_tmpz00_3203 =
												((obj_t) ((BgL_objectz00_bglt) BgL_nfunz00_2619));
											BgL_arg1807z00_2647 =
												(BgL_objectz00_bglt) (BgL_tmpz00_3203);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Ast/substitute.scm 145 */
												long BgL_idxz00_2648;

												BgL_idxz00_2648 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2647);
												BgL_test1939z00_3202 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2648 + 2L)) == BgL_classz00_2646);
											}
										else
											{	/* Ast/substitute.scm 145 */
												bool_t BgL_res1851z00_2651;

												{	/* Ast/substitute.scm 145 */
													obj_t BgL_oclassz00_2652;

													{	/* Ast/substitute.scm 145 */
														obj_t BgL_arg1815z00_2653;
														long BgL_arg1816z00_2654;

														BgL_arg1815z00_2653 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Ast/substitute.scm 145 */
															long BgL_arg1817z00_2655;

															BgL_arg1817z00_2655 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2647);
															BgL_arg1816z00_2654 =
																(BgL_arg1817z00_2655 - OBJECT_TYPE);
														}
														BgL_oclassz00_2652 =
															VECTOR_REF(BgL_arg1815z00_2653,
															BgL_arg1816z00_2654);
													}
													{	/* Ast/substitute.scm 145 */
														bool_t BgL__ortest_1115z00_2656;

														BgL__ortest_1115z00_2656 =
															(BgL_classz00_2646 == BgL_oclassz00_2652);
														if (BgL__ortest_1115z00_2656)
															{	/* Ast/substitute.scm 145 */
																BgL_res1851z00_2651 = BgL__ortest_1115z00_2656;
															}
														else
															{	/* Ast/substitute.scm 145 */
																long BgL_odepthz00_2657;

																{	/* Ast/substitute.scm 145 */
																	obj_t BgL_arg1804z00_2658;

																	BgL_arg1804z00_2658 = (BgL_oclassz00_2652);
																	BgL_odepthz00_2657 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2658);
																}
																if ((2L < BgL_odepthz00_2657))
																	{	/* Ast/substitute.scm 145 */
																		obj_t BgL_arg1802z00_2659;

																		{	/* Ast/substitute.scm 145 */
																			obj_t BgL_arg1803z00_2660;

																			BgL_arg1803z00_2660 =
																				(BgL_oclassz00_2652);
																			BgL_arg1802z00_2659 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2660, 2L);
																		}
																		BgL_res1851z00_2651 =
																			(BgL_arg1802z00_2659 ==
																			BgL_classz00_2646);
																	}
																else
																	{	/* Ast/substitute.scm 145 */
																		BgL_res1851z00_2651 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test1939z00_3202 = BgL_res1851z00_2651;
											}
									}
								}
								if (BgL_test1939z00_3202)
									{	/* Ast/substitute.scm 146 */
										BgL_valuez00_bglt BgL_arg1553z00_2661;

										BgL_arg1553z00_2661 =
											(((BgL_variablez00_bglt) COBJECT(
													(((BgL_varz00_bglt) COBJECT(
																((BgL_varz00_bglt) BgL_nfunz00_2619)))->
														BgL_variablez00)))->BgL_valuez00);
										{	/* Ast/substitute.scm 146 */
											obj_t BgL_classz00_2662;

											BgL_classz00_2662 = BGl_funz00zzast_varz00;
											{	/* Ast/substitute.scm 146 */
												BgL_objectz00_bglt BgL_arg1807z00_2663;

												{	/* Ast/substitute.scm 146 */
													obj_t BgL_tmpz00_3229;

													BgL_tmpz00_3229 =
														((obj_t)
														((BgL_objectz00_bglt) BgL_arg1553z00_2661));
													BgL_arg1807z00_2663 =
														(BgL_objectz00_bglt) (BgL_tmpz00_3229);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Ast/substitute.scm 146 */
														long BgL_idxz00_2664;

														BgL_idxz00_2664 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2663);
														BgL_test1934z00_3177 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2664 + 2L)) == BgL_classz00_2662);
													}
												else
													{	/* Ast/substitute.scm 146 */
														bool_t BgL_res1852z00_2667;

														{	/* Ast/substitute.scm 146 */
															obj_t BgL_oclassz00_2668;

															{	/* Ast/substitute.scm 146 */
																obj_t BgL_arg1815z00_2669;
																long BgL_arg1816z00_2670;

																BgL_arg1815z00_2669 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Ast/substitute.scm 146 */
																	long BgL_arg1817z00_2671;

																	BgL_arg1817z00_2671 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2663);
																	BgL_arg1816z00_2670 =
																		(BgL_arg1817z00_2671 - OBJECT_TYPE);
																}
																BgL_oclassz00_2668 =
																	VECTOR_REF(BgL_arg1815z00_2669,
																	BgL_arg1816z00_2670);
															}
															{	/* Ast/substitute.scm 146 */
																bool_t BgL__ortest_1115z00_2672;

																BgL__ortest_1115z00_2672 =
																	(BgL_classz00_2662 == BgL_oclassz00_2668);
																if (BgL__ortest_1115z00_2672)
																	{	/* Ast/substitute.scm 146 */
																		BgL_res1852z00_2667 =
																			BgL__ortest_1115z00_2672;
																	}
																else
																	{	/* Ast/substitute.scm 146 */
																		long BgL_odepthz00_2673;

																		{	/* Ast/substitute.scm 146 */
																			obj_t BgL_arg1804z00_2674;

																			BgL_arg1804z00_2674 =
																				(BgL_oclassz00_2668);
																			BgL_odepthz00_2673 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2674);
																		}
																		if ((2L < BgL_odepthz00_2673))
																			{	/* Ast/substitute.scm 146 */
																				obj_t BgL_arg1802z00_2675;

																				{	/* Ast/substitute.scm 146 */
																					obj_t BgL_arg1803z00_2676;

																					BgL_arg1803z00_2676 =
																						(BgL_oclassz00_2668);
																					BgL_arg1802z00_2675 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2676, 2L);
																				}
																				BgL_res1852z00_2667 =
																					(BgL_arg1802z00_2675 ==
																					BgL_classz00_2662);
																			}
																		else
																			{	/* Ast/substitute.scm 146 */
																				BgL_res1852z00_2667 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test1934z00_3177 = BgL_res1852z00_2667;
													}
											}
										}
									}
								else
									{	/* Ast/substitute.scm 145 */
										BgL_test1934z00_3177 = ((bool_t) 0);
									}
							}
					}
					if (BgL_test1934z00_3177)
						{	/* Ast/substitute.scm 147 */
							bool_t BgL_test1946z00_3252;

							{	/* Ast/substitute.scm 147 */
								BgL_variablez00_bglt BgL_arg1546z00_2677;
								obj_t BgL_arg1552z00_2678;

								BgL_arg1546z00_2677 =
									(((BgL_varz00_bglt) COBJECT(
											((BgL_varz00_bglt) BgL_nfunz00_2619)))->BgL_variablez00);
								BgL_arg1552z00_2678 = CDR(((obj_t) BgL_nargsz00_2620));
								BgL_test1946z00_3252 =
									BGl_correctzd2arityzd2appzf3zf3zzast_appz00
									(BgL_arg1546z00_2677, BgL_arg1552z00_2678);
							}
							if (BgL_test1946z00_3252)
								{	/* Ast/substitute.scm 149 */
									obj_t BgL_arg1516z00_2679;
									obj_t BgL_arg1535z00_2680;

									BgL_arg1516z00_2679 =
										(((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt)
													((BgL_funcallz00_bglt) BgL_nodez00_2529))))->
										BgL_locz00);
									BgL_arg1535z00_2680 = CDR(((obj_t) BgL_nargsz00_2620));
									return
										BGl_makezd2appzd2nodez00zzast_appz00(BNIL,
										BgL_arg1516z00_2679, CNST_TABLE_REF(5),
										((BgL_varz00_bglt) BgL_nfunz00_2619), BgL_arg1535z00_2680);
								}
							else
								{	/* Ast/substitute.scm 150 */
									obj_t BgL_arg1540z00_2681;
									obj_t BgL_arg1544z00_2682;

									BgL_arg1540z00_2681 =
										(((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt)
													((BgL_funcallz00_bglt) BgL_nodez00_2529))))->
										BgL_locz00);
									BgL_arg1544z00_2682 =
										BGl_shapez00zztools_shapez00(((obj_t) ((BgL_funcallz00_bglt)
												BgL_nodez00_2529)));
									return ((BgL_nodez00_bglt)
										BGl_userzd2errorzf2locationz20zztools_errorz00
										(BgL_arg1540z00_2681, BGl_string1883z00zzast_substitutez00,
											BGl_string1884z00zzast_substitutez00, BgL_arg1544z00_2682,
											BNIL));
								}
						}
					else
						{	/* Ast/substitute.scm 144 */
							((((BgL_funcallz00_bglt) COBJECT(
											((BgL_funcallz00_bglt) BgL_nodez00_2529)))->BgL_funz00) =
								((BgL_nodez00_bglt) BgL_nfunz00_2619), BUNSPEC);
							((((BgL_funcallz00_bglt) COBJECT(((BgL_funcallz00_bglt)
												BgL_nodez00_2529)))->BgL_argsz00) =
								((obj_t) BgL_nargsz00_2620), BUNSPEC);
							return ((BgL_nodez00_bglt) ((BgL_funcallz00_bglt)
									BgL_nodez00_2529));
						}
				}
			}
		}

	}



/* &do-substitute!-app-l1271 */
	BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2appzd2l1271za2zzast_substitutez00(obj_t
		BgL_envz00_2531, obj_t BgL_nodez00_2532, obj_t BgL_sitez00_2533)
	{
		{	/* Ast/substitute.scm 121 */
			{	/* Ast/substitute.scm 123 */
				BgL_nodez00_bglt BgL_nfunz00_2684;
				BgL_nodez00_bglt BgL_nargz00_2685;

				{	/* Ast/substitute.scm 123 */
					BgL_nodez00_bglt BgL_arg1485z00_2686;

					BgL_arg1485z00_2686 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2532)))->BgL_funz00);
					BgL_nfunz00_2684 =
						BGl_dozd2substitutez12zc0zzast_substitutez00(BgL_arg1485z00_2686,
						CNST_TABLE_REF(6));
				}
				{	/* Ast/substitute.scm 124 */
					BgL_nodez00_bglt BgL_arg1489z00_2687;

					BgL_arg1489z00_2687 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2532)))->BgL_argz00);
					BgL_nargz00_2685 =
						BGl_dozd2substitutez12zc0zzast_substitutez00(BgL_arg1489z00_2687,
						CNST_TABLE_REF(1));
				}
				{	/* Ast/substitute.scm 125 */
					bool_t BgL_test1947z00_3288;

					{	/* Ast/substitute.scm 125 */
						bool_t BgL_test1948z00_3289;

						{	/* Ast/substitute.scm 125 */
							obj_t BgL_classz00_2688;

							BgL_classz00_2688 = BGl_closurez00zzast_nodez00;
							{	/* Ast/substitute.scm 125 */
								BgL_objectz00_bglt BgL_arg1807z00_2689;

								{	/* Ast/substitute.scm 125 */
									obj_t BgL_tmpz00_3290;

									BgL_tmpz00_3290 =
										((obj_t) ((BgL_objectz00_bglt) BgL_nfunz00_2684));
									BgL_arg1807z00_2689 = (BgL_objectz00_bglt) (BgL_tmpz00_3290);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Ast/substitute.scm 125 */
										long BgL_idxz00_2690;

										BgL_idxz00_2690 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2689);
										BgL_test1948z00_3289 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2690 + 3L)) == BgL_classz00_2688);
									}
								else
									{	/* Ast/substitute.scm 125 */
										bool_t BgL_res1849z00_2693;

										{	/* Ast/substitute.scm 125 */
											obj_t BgL_oclassz00_2694;

											{	/* Ast/substitute.scm 125 */
												obj_t BgL_arg1815z00_2695;
												long BgL_arg1816z00_2696;

												BgL_arg1815z00_2695 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Ast/substitute.scm 125 */
													long BgL_arg1817z00_2697;

													BgL_arg1817z00_2697 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2689);
													BgL_arg1816z00_2696 =
														(BgL_arg1817z00_2697 - OBJECT_TYPE);
												}
												BgL_oclassz00_2694 =
													VECTOR_REF(BgL_arg1815z00_2695, BgL_arg1816z00_2696);
											}
											{	/* Ast/substitute.scm 125 */
												bool_t BgL__ortest_1115z00_2698;

												BgL__ortest_1115z00_2698 =
													(BgL_classz00_2688 == BgL_oclassz00_2694);
												if (BgL__ortest_1115z00_2698)
													{	/* Ast/substitute.scm 125 */
														BgL_res1849z00_2693 = BgL__ortest_1115z00_2698;
													}
												else
													{	/* Ast/substitute.scm 125 */
														long BgL_odepthz00_2699;

														{	/* Ast/substitute.scm 125 */
															obj_t BgL_arg1804z00_2700;

															BgL_arg1804z00_2700 = (BgL_oclassz00_2694);
															BgL_odepthz00_2699 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2700);
														}
														if ((3L < BgL_odepthz00_2699))
															{	/* Ast/substitute.scm 125 */
																obj_t BgL_arg1802z00_2701;

																{	/* Ast/substitute.scm 125 */
																	obj_t BgL_arg1803z00_2702;

																	BgL_arg1803z00_2702 = (BgL_oclassz00_2694);
																	BgL_arg1802z00_2701 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2702,
																		3L);
																}
																BgL_res1849z00_2693 =
																	(BgL_arg1802z00_2701 == BgL_classz00_2688);
															}
														else
															{	/* Ast/substitute.scm 125 */
																BgL_res1849z00_2693 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1948z00_3289 = BgL_res1849z00_2693;
									}
							}
						}
						if (BgL_test1948z00_3289)
							{	/* Ast/substitute.scm 126 */
								bool_t BgL_test1952z00_3313;

								{	/* Ast/substitute.scm 126 */
									BgL_variablez00_bglt BgL_arg1473z00_2703;

									BgL_arg1473z00_2703 =
										(((BgL_varz00_bglt) COBJECT(
												((BgL_varz00_bglt) BgL_nfunz00_2684)))->
										BgL_variablez00);
									BgL_test1952z00_3313 =
										BGl_globalzd2optionalzf3z21zzast_varz00(((obj_t)
											BgL_arg1473z00_2703));
								}
								if (BgL_test1952z00_3313)
									{	/* Ast/substitute.scm 126 */
										BgL_test1947z00_3288 = ((bool_t) 0);
									}
								else
									{	/* Ast/substitute.scm 127 */
										bool_t BgL_test1953z00_3318;

										{	/* Ast/substitute.scm 127 */
											BgL_variablez00_bglt BgL_arg1472z00_2704;

											BgL_arg1472z00_2704 =
												(((BgL_varz00_bglt) COBJECT(
														((BgL_varz00_bglt) BgL_nfunz00_2684)))->
												BgL_variablez00);
											BgL_test1953z00_3318 =
												BGl_globalzd2keyzf3z21zzast_varz00(((obj_t)
													BgL_arg1472z00_2704));
										}
										if (BgL_test1953z00_3318)
											{	/* Ast/substitute.scm 127 */
												BgL_test1947z00_3288 = ((bool_t) 0);
											}
										else
											{	/* Ast/substitute.scm 127 */
												BgL_test1947z00_3288 = ((bool_t) 1);
											}
									}
							}
						else
							{	/* Ast/substitute.scm 125 */
								BgL_test1947z00_3288 = ((bool_t) 0);
							}
					}
					if (BgL_test1947z00_3288)
						{	/* Ast/substitute.scm 128 */
							obj_t BgL_arg1448z00_2705;
							BgL_refz00_bglt BgL_arg1453z00_2706;

							BgL_arg1448z00_2705 =
								(((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_appzd2lyzd2_bglt) BgL_nodez00_2532))))->BgL_locz00);
							{	/* Ast/substitute.scm 128 */
								BgL_refz00_bglt BgL_new1111z00_2707;

								{	/* Ast/substitute.scm 128 */
									BgL_refz00_bglt BgL_new1115z00_2708;

									BgL_new1115z00_2708 =
										((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_refz00_bgl))));
									{	/* Ast/substitute.scm 128 */
										long BgL_arg1454z00_2709;

										BgL_arg1454z00_2709 =
											BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1115z00_2708),
											BgL_arg1454z00_2709);
									}
									{	/* Ast/substitute.scm 128 */
										BgL_objectz00_bglt BgL_tmpz00_3330;

										BgL_tmpz00_3330 =
											((BgL_objectz00_bglt) BgL_new1115z00_2708);
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3330, BFALSE);
									}
									((BgL_objectz00_bglt) BgL_new1115z00_2708);
									BgL_new1111z00_2707 = BgL_new1115z00_2708;
								}
								((((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt) BgL_new1111z00_2707)))->
										BgL_locz00) =
									((obj_t) (((BgL_nodez00_bglt) COBJECT(BgL_nfunz00_2684))->
											BgL_locz00)), BUNSPEC);
								((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
													BgL_new1111z00_2707)))->BgL_typez00) =
									((BgL_typez00_bglt) (((BgL_nodez00_bglt)
												COBJECT(BgL_nfunz00_2684))->BgL_typez00)), BUNSPEC);
								((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
													BgL_new1111z00_2707)))->BgL_variablez00) =
									((BgL_variablez00_bglt) (((BgL_varz00_bglt)
												COBJECT(((BgL_varz00_bglt) BgL_nfunz00_2684)))->
											BgL_variablez00)), BUNSPEC);
								BgL_arg1453z00_2706 = BgL_new1111z00_2707;
							}
							return
								BGl_knownzd2appzd2lyzd2ze3nodez31zzast_applyz00(BNIL,
								BgL_arg1448z00_2705, ((BgL_nodez00_bglt) BgL_arg1453z00_2706),
								BgL_nargz00_2685, BgL_sitez00_2533);
						}
					else
						{	/* Ast/substitute.scm 125 */
							((((BgL_appzd2lyzd2_bglt) COBJECT(
											((BgL_appzd2lyzd2_bglt) BgL_nodez00_2532)))->BgL_funz00) =
								((BgL_nodez00_bglt) BgL_nfunz00_2684), BUNSPEC);
							((((BgL_appzd2lyzd2_bglt) COBJECT(((BgL_appzd2lyzd2_bglt)
												BgL_nodez00_2532)))->BgL_argz00) =
								((BgL_nodez00_bglt) BgL_nargz00_2685), BUNSPEC);
							return ((BgL_nodez00_bglt) ((BgL_appzd2lyzd2_bglt)
									BgL_nodez00_2532));
						}
				}
			}
		}

	}



/* &do-substitute!-app1269 */
	BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2app1269z70zzast_substitutez00(obj_t
		BgL_envz00_2534, obj_t BgL_nodez00_2535, obj_t BgL_sitez00_2536)
	{
		{	/* Ast/substitute.scm 113 */
			{	/* Ast/substitute.scm 114 */
				BgL_nodez00_bglt BgL_arg1370z00_2711;

				{	/* Ast/substitute.scm 114 */
					BgL_varz00_bglt BgL_arg1371z00_2712;

					BgL_arg1371z00_2712 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_2535)))->BgL_funz00);
					BgL_arg1370z00_2711 =
						BGl_dozd2substitutez12zc0zzast_substitutez00(
						((BgL_nodez00_bglt) BgL_arg1371z00_2712), CNST_TABLE_REF(3));
				}
				((((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_2535)))->BgL_funz00) =
					((BgL_varz00_bglt) ((BgL_varz00_bglt) BgL_arg1370z00_2711)), BUNSPEC);
			}
			{	/* Ast/substitute.scm 115 */
				obj_t BgL_arg1375z00_2713;

				BgL_arg1375z00_2713 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_2535)))->BgL_argsz00);
				BGl_dozd2substituteza2z12z62zzast_substitutez00(BgL_arg1375z00_2713,
					CNST_TABLE_REF(1));
			}
			return ((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_2535));
		}

	}



/* &do-substitute!-sync1267 */
	BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2sync1267z70zzast_substitutez00(obj_t
		BgL_envz00_2537, obj_t BgL_nodez00_2538, obj_t BgL_sitez00_2539)
	{
		{	/* Ast/substitute.scm 104 */
			((((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2538)))->BgL_mutexz00) =
				((BgL_nodez00_bglt)
					BGl_dozd2substitutez12zc0zzast_substitutez00((((BgL_syncz00_bglt)
								COBJECT(((BgL_syncz00_bglt) BgL_nodez00_2538)))->BgL_mutexz00),
						BgL_sitez00_2539)), BUNSPEC);
			((((BgL_syncz00_bglt) COBJECT(((BgL_syncz00_bglt) BgL_nodez00_2538)))->
					BgL_prelockz00) =
				((BgL_nodez00_bglt)
					BGl_dozd2substitutez12zc0zzast_substitutez00((((BgL_syncz00_bglt)
								COBJECT(((BgL_syncz00_bglt) BgL_nodez00_2538)))->
							BgL_prelockz00), BgL_sitez00_2539)), BUNSPEC);
			((((BgL_syncz00_bglt) COBJECT(((BgL_syncz00_bglt) BgL_nodez00_2538)))->
					BgL_bodyz00) =
				((BgL_nodez00_bglt)
					BGl_dozd2substitutez12zc0zzast_substitutez00((((BgL_syncz00_bglt)
								COBJECT(((BgL_syncz00_bglt) BgL_nodez00_2538)))->BgL_bodyz00),
						BgL_sitez00_2539)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_syncz00_bglt) BgL_nodez00_2538));
		}

	}



/* &do-substitute!-seque1265 */
	BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2seque1265z70zzast_substitutez00(obj_t
		BgL_envz00_2540, obj_t BgL_nodez00_2541, obj_t BgL_sitez00_2542)
	{
		{	/* Ast/substitute.scm 97 */
			BGl_dozd2substituteza2z12z62zzast_substitutez00(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_2541)))->BgL_nodesz00),
				BgL_sitez00_2542);
			return ((BgL_nodez00_bglt) ((BgL_sequencez00_bglt) BgL_nodez00_2541));
		}

	}



/* &do-substitute!-kwote1263 */
	BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2kwote1263z70zzast_substitutez00(obj_t
		BgL_envz00_2543, obj_t BgL_nodez00_2544, obj_t BgL_sitez00_2545)
	{
		{	/* Ast/substitute.scm 91 */
			return ((BgL_nodez00_bglt) ((BgL_kwotez00_bglt) BgL_nodez00_2544));
		}

	}



/* &do-substitute!-var1261 */
	BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2var1261z70zzast_substitutez00(obj_t
		BgL_envz00_2546, obj_t BgL_nodez00_2547, obj_t BgL_sitez00_2548)
	{
		{	/* Ast/substitute.scm 63 */
			{	/* Ast/substitute.scm 64 */
				BgL_variablez00_bglt BgL_varz00_2718;

				BgL_varz00_2718 =
					(((BgL_varz00_bglt) COBJECT(
							((BgL_varz00_bglt) BgL_nodez00_2547)))->BgL_variablez00);
				{	/* Ast/substitute.scm 64 */
					obj_t BgL_alphaz00_2719;

					BgL_alphaz00_2719 =
						(((BgL_variablez00_bglt) COBJECT(BgL_varz00_2718))->
						BgL_fastzd2alphazd2);
					{	/* Ast/substitute.scm 65 */

						{
							obj_t BgL_alphaz00_2721;

							{
								obj_t BgL_auxz00_3393;

								BgL_alphaz00_2721 = BgL_alphaz00_2719;
							BgL_loopz00_2720:
								if ((BgL_alphaz00_2721 == BUNSPEC))
									{	/* Ast/substitute.scm 68 */
										BgL_auxz00_3393 =
											((obj_t) ((BgL_varz00_bglt) BgL_nodez00_2547));
									}
								else
									{	/* Ast/substitute.scm 70 */
										bool_t BgL_test1955z00_3398;

										{	/* Ast/substitute.scm 70 */
											obj_t BgL_classz00_2722;

											BgL_classz00_2722 = BGl_varz00zzast_nodez00;
											if (BGL_OBJECTP(BgL_alphaz00_2721))
												{	/* Ast/substitute.scm 70 */
													BgL_objectz00_bglt BgL_arg1807z00_2723;

													BgL_arg1807z00_2723 =
														(BgL_objectz00_bglt) (BgL_alphaz00_2721);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Ast/substitute.scm 70 */
															long BgL_idxz00_2724;

															BgL_idxz00_2724 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2723);
															BgL_test1955z00_3398 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_2724 + 2L)) == BgL_classz00_2722);
														}
													else
														{	/* Ast/substitute.scm 70 */
															bool_t BgL_res1845z00_2727;

															{	/* Ast/substitute.scm 70 */
																obj_t BgL_oclassz00_2728;

																{	/* Ast/substitute.scm 70 */
																	obj_t BgL_arg1815z00_2729;
																	long BgL_arg1816z00_2730;

																	BgL_arg1815z00_2729 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Ast/substitute.scm 70 */
																		long BgL_arg1817z00_2731;

																		BgL_arg1817z00_2731 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2723);
																		BgL_arg1816z00_2730 =
																			(BgL_arg1817z00_2731 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_2728 =
																		VECTOR_REF(BgL_arg1815z00_2729,
																		BgL_arg1816z00_2730);
																}
																{	/* Ast/substitute.scm 70 */
																	bool_t BgL__ortest_1115z00_2732;

																	BgL__ortest_1115z00_2732 =
																		(BgL_classz00_2722 == BgL_oclassz00_2728);
																	if (BgL__ortest_1115z00_2732)
																		{	/* Ast/substitute.scm 70 */
																			BgL_res1845z00_2727 =
																				BgL__ortest_1115z00_2732;
																		}
																	else
																		{	/* Ast/substitute.scm 70 */
																			long BgL_odepthz00_2733;

																			{	/* Ast/substitute.scm 70 */
																				obj_t BgL_arg1804z00_2734;

																				BgL_arg1804z00_2734 =
																					(BgL_oclassz00_2728);
																				BgL_odepthz00_2733 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_2734);
																			}
																			if ((2L < BgL_odepthz00_2733))
																				{	/* Ast/substitute.scm 70 */
																					obj_t BgL_arg1802z00_2735;

																					{	/* Ast/substitute.scm 70 */
																						obj_t BgL_arg1803z00_2736;

																						BgL_arg1803z00_2736 =
																							(BgL_oclassz00_2728);
																						BgL_arg1802z00_2735 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_2736, 2L);
																					}
																					BgL_res1845z00_2727 =
																						(BgL_arg1802z00_2735 ==
																						BgL_classz00_2722);
																				}
																			else
																				{	/* Ast/substitute.scm 70 */
																					BgL_res1845z00_2727 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test1955z00_3398 = BgL_res1845z00_2727;
														}
												}
											else
												{	/* Ast/substitute.scm 70 */
													BgL_test1955z00_3398 = ((bool_t) 0);
												}
										}
										if (BgL_test1955z00_3398)
											{	/* Ast/substitute.scm 71 */
												BgL_variablez00_bglt BgL_arg1328z00_2737;

												BgL_arg1328z00_2737 =
													(((BgL_varz00_bglt) COBJECT(
															((BgL_varz00_bglt) BgL_alphaz00_2721)))->
													BgL_variablez00);
												{
													obj_t BgL_alphaz00_3423;

													BgL_alphaz00_3423 = ((obj_t) BgL_arg1328z00_2737);
													BgL_alphaz00_2721 = BgL_alphaz00_3423;
													goto BgL_loopz00_2720;
												}
											}
										else
											{	/* Ast/substitute.scm 72 */
												bool_t BgL_test1960z00_3425;

												{	/* Ast/substitute.scm 72 */
													obj_t BgL_classz00_2738;

													BgL_classz00_2738 = BGl_variablez00zzast_varz00;
													if (BGL_OBJECTP(BgL_alphaz00_2721))
														{	/* Ast/substitute.scm 72 */
															BgL_objectz00_bglt BgL_arg1807z00_2739;

															BgL_arg1807z00_2739 =
																(BgL_objectz00_bglt) (BgL_alphaz00_2721);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Ast/substitute.scm 72 */
																	long BgL_idxz00_2740;

																	BgL_idxz00_2740 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_2739);
																	BgL_test1960z00_3425 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_2740 + 1L)) ==
																		BgL_classz00_2738);
																}
															else
																{	/* Ast/substitute.scm 72 */
																	bool_t BgL_res1846z00_2743;

																	{	/* Ast/substitute.scm 72 */
																		obj_t BgL_oclassz00_2744;

																		{	/* Ast/substitute.scm 72 */
																			obj_t BgL_arg1815z00_2745;
																			long BgL_arg1816z00_2746;

																			BgL_arg1815z00_2745 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Ast/substitute.scm 72 */
																				long BgL_arg1817z00_2747;

																				BgL_arg1817z00_2747 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_2739);
																				BgL_arg1816z00_2746 =
																					(BgL_arg1817z00_2747 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_2744 =
																				VECTOR_REF(BgL_arg1815z00_2745,
																				BgL_arg1816z00_2746);
																		}
																		{	/* Ast/substitute.scm 72 */
																			bool_t BgL__ortest_1115z00_2748;

																			BgL__ortest_1115z00_2748 =
																				(BgL_classz00_2738 ==
																				BgL_oclassz00_2744);
																			if (BgL__ortest_1115z00_2748)
																				{	/* Ast/substitute.scm 72 */
																					BgL_res1846z00_2743 =
																						BgL__ortest_1115z00_2748;
																				}
																			else
																				{	/* Ast/substitute.scm 72 */
																					long BgL_odepthz00_2749;

																					{	/* Ast/substitute.scm 72 */
																						obj_t BgL_arg1804z00_2750;

																						BgL_arg1804z00_2750 =
																							(BgL_oclassz00_2744);
																						BgL_odepthz00_2749 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_2750);
																					}
																					if ((1L < BgL_odepthz00_2749))
																						{	/* Ast/substitute.scm 72 */
																							obj_t BgL_arg1802z00_2751;

																							{	/* Ast/substitute.scm 72 */
																								obj_t BgL_arg1803z00_2752;

																								BgL_arg1803z00_2752 =
																									(BgL_oclassz00_2744);
																								BgL_arg1802z00_2751 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_2752, 1L);
																							}
																							BgL_res1846z00_2743 =
																								(BgL_arg1802z00_2751 ==
																								BgL_classz00_2738);
																						}
																					else
																						{	/* Ast/substitute.scm 72 */
																							BgL_res1846z00_2743 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test1960z00_3425 = BgL_res1846z00_2743;
																}
														}
													else
														{	/* Ast/substitute.scm 72 */
															BgL_test1960z00_3425 = ((bool_t) 0);
														}
												}
												if (BgL_test1960z00_3425)
													{	/* Ast/substitute.scm 72 */
														{	/* Ast/substitute.scm 73 */
															obj_t BgL_arg1331z00_2753;

															BgL_arg1331z00_2753 =
																(((BgL_nodez00_bglt) COBJECT(
																		((BgL_nodez00_bglt)
																			((BgL_varz00_bglt) BgL_nodez00_2547))))->
																BgL_locz00);
															BGl_usezd2variablez12zc0zzast_sexpz00((
																	(BgL_variablez00_bglt) BgL_alphaz00_2721),
																BgL_arg1331z00_2753, BgL_sitez00_2548);
														}
														{	/* Ast/substitute.scm 74 */
															bool_t BgL_test1965z00_3453;

															{	/* Ast/substitute.scm 74 */
																bool_t BgL_test1966z00_3454;

																{	/* Ast/substitute.scm 74 */
																	BgL_valuez00_bglt BgL_arg1343z00_2754;

																	BgL_arg1343z00_2754 =
																		(((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					BgL_alphaz00_2721)))->BgL_valuez00);
																	{	/* Ast/substitute.scm 74 */
																		obj_t BgL_classz00_2755;

																		BgL_classz00_2755 = BGl_funz00zzast_varz00;
																		{	/* Ast/substitute.scm 74 */
																			BgL_objectz00_bglt BgL_arg1807z00_2756;

																			{	/* Ast/substitute.scm 74 */
																				obj_t BgL_tmpz00_3457;

																				BgL_tmpz00_3457 =
																					((obj_t)
																					((BgL_objectz00_bglt)
																						BgL_arg1343z00_2754));
																				BgL_arg1807z00_2756 =
																					(BgL_objectz00_bglt)
																					(BgL_tmpz00_3457);
																			}
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Ast/substitute.scm 74 */
																					long BgL_idxz00_2757;

																					BgL_idxz00_2757 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_2756);
																					BgL_test1966z00_3454 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_2757 + 2L)) ==
																						BgL_classz00_2755);
																				}
																			else
																				{	/* Ast/substitute.scm 74 */
																					bool_t BgL_res1847z00_2760;

																					{	/* Ast/substitute.scm 74 */
																						obj_t BgL_oclassz00_2761;

																						{	/* Ast/substitute.scm 74 */
																							obj_t BgL_arg1815z00_2762;
																							long BgL_arg1816z00_2763;

																							BgL_arg1815z00_2762 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Ast/substitute.scm 74 */
																								long BgL_arg1817z00_2764;

																								BgL_arg1817z00_2764 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_2756);
																								BgL_arg1816z00_2763 =
																									(BgL_arg1817z00_2764 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_2761 =
																								VECTOR_REF(BgL_arg1815z00_2762,
																								BgL_arg1816z00_2763);
																						}
																						{	/* Ast/substitute.scm 74 */
																							bool_t BgL__ortest_1115z00_2765;

																							BgL__ortest_1115z00_2765 =
																								(BgL_classz00_2755 ==
																								BgL_oclassz00_2761);
																							if (BgL__ortest_1115z00_2765)
																								{	/* Ast/substitute.scm 74 */
																									BgL_res1847z00_2760 =
																										BgL__ortest_1115z00_2765;
																								}
																							else
																								{	/* Ast/substitute.scm 74 */
																									long BgL_odepthz00_2766;

																									{	/* Ast/substitute.scm 74 */
																										obj_t BgL_arg1804z00_2767;

																										BgL_arg1804z00_2767 =
																											(BgL_oclassz00_2761);
																										BgL_odepthz00_2766 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_2767);
																									}
																									if ((2L < BgL_odepthz00_2766))
																										{	/* Ast/substitute.scm 74 */
																											obj_t BgL_arg1802z00_2768;

																											{	/* Ast/substitute.scm 74 */
																												obj_t
																													BgL_arg1803z00_2769;
																												BgL_arg1803z00_2769 =
																													(BgL_oclassz00_2761);
																												BgL_arg1802z00_2768 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_2769,
																													2L);
																											}
																											BgL_res1847z00_2760 =
																												(BgL_arg1802z00_2768 ==
																												BgL_classz00_2755);
																										}
																									else
																										{	/* Ast/substitute.scm 74 */
																											BgL_res1847z00_2760 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test1966z00_3454 =
																						BgL_res1847z00_2760;
																				}
																		}
																	}
																}
																if (BgL_test1966z00_3454)
																	{	/* Ast/substitute.scm 74 */
																		if ((BgL_sitez00_2548 == CNST_TABLE_REF(3)))
																			{	/* Ast/substitute.scm 74 */
																				BgL_test1965z00_3453 = ((bool_t) 0);
																			}
																		else
																			{	/* Ast/substitute.scm 74 */
																				BgL_test1965z00_3453 = ((bool_t) 1);
																			}
																	}
																else
																	{	/* Ast/substitute.scm 74 */
																		BgL_test1965z00_3453 = ((bool_t) 0);
																	}
															}
															if (BgL_test1965z00_3453)
																{	/* Ast/substitute.scm 75 */
																	BgL_closurez00_bglt BgL_new1107z00_2770;

																	{	/* Ast/substitute.scm 76 */
																		BgL_closurez00_bglt BgL_new1106z00_2771;

																		BgL_new1106z00_2771 =
																			((BgL_closurez00_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_closurez00_bgl))));
																		{	/* Ast/substitute.scm 76 */
																			long BgL_arg1340z00_2772;

																			BgL_arg1340z00_2772 =
																				BGL_CLASS_NUM
																				(BGl_closurez00zzast_nodez00);
																			BGL_OBJECT_CLASS_NUM_SET((
																					(BgL_objectz00_bglt)
																					BgL_new1106z00_2771),
																				BgL_arg1340z00_2772);
																		}
																		{	/* Ast/substitute.scm 76 */
																			BgL_objectz00_bglt BgL_tmpz00_3487;

																			BgL_tmpz00_3487 =
																				((BgL_objectz00_bglt)
																				BgL_new1106z00_2771);
																			BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3487,
																				BFALSE);
																		}
																		((BgL_objectz00_bglt) BgL_new1106z00_2771);
																		BgL_new1107z00_2770 = BgL_new1106z00_2771;
																	}
																	((((BgL_nodez00_bglt) COBJECT(
																					((BgL_nodez00_bglt)
																						BgL_new1107z00_2770)))->
																			BgL_locz00) =
																		((obj_t) (((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt) (
																								(BgL_varz00_bglt)
																								BgL_nodez00_2547))))->
																				BgL_locz00)), BUNSPEC);
																	{
																		BgL_typez00_bglt BgL_auxz00_3496;

																		{	/* Ast/substitute.scm 77 */
																			BgL_typez00_bglt BgL_arg1339z00_2773;

																			BgL_arg1339z00_2773 =
																				(((BgL_nodez00_bglt) COBJECT(
																						((BgL_nodez00_bglt)
																							((BgL_varz00_bglt)
																								BgL_nodez00_2547))))->
																				BgL_typez00);
																			BgL_auxz00_3496 =
																				BGl_strictzd2nodezd2typez00zzast_nodez00
																				(((BgL_typez00_bglt)
																					BGl_za2procedureza2z00zztype_cachez00),
																				BgL_arg1339z00_2773);
																		}
																		((((BgL_nodez00_bglt) COBJECT(
																						((BgL_nodez00_bglt)
																							BgL_new1107z00_2770)))->
																				BgL_typez00) =
																			((BgL_typez00_bglt) BgL_auxz00_3496),
																			BUNSPEC);
																	}
																	((((BgL_varz00_bglt) COBJECT(
																					((BgL_varz00_bglt)
																						BgL_new1107z00_2770)))->
																			BgL_variablez00) =
																		((BgL_variablez00_bglt) (
																				(BgL_variablez00_bglt)
																				BgL_alphaz00_2721)), BUNSPEC);
																	BgL_auxz00_3393 =
																		((obj_t) BgL_new1107z00_2770);
																}
															else
																{	/* Ast/substitute.scm 79 */
																	BgL_refz00_bglt BgL_new1109z00_2774;

																	{	/* Ast/substitute.scm 80 */
																		BgL_refz00_bglt BgL_new1108z00_2775;

																		BgL_new1108z00_2775 =
																			((BgL_refz00_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_refz00_bgl))));
																		{	/* Ast/substitute.scm 80 */
																			long BgL_arg1342z00_2776;

																			BgL_arg1342z00_2776 =
																				BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
																			BGL_OBJECT_CLASS_NUM_SET(
																				((BgL_objectz00_bglt)
																					BgL_new1108z00_2775),
																				BgL_arg1342z00_2776);
																		}
																		{	/* Ast/substitute.scm 80 */
																			BgL_objectz00_bglt BgL_tmpz00_3512;

																			BgL_tmpz00_3512 =
																				((BgL_objectz00_bglt)
																				BgL_new1108z00_2775);
																			BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3512,
																				BFALSE);
																		}
																		((BgL_objectz00_bglt) BgL_new1108z00_2775);
																		BgL_new1109z00_2774 = BgL_new1108z00_2775;
																	}
																	((((BgL_nodez00_bglt) COBJECT(
																					((BgL_nodez00_bglt)
																						BgL_new1109z00_2774)))->
																			BgL_locz00) =
																		((obj_t) (((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt) (
																								(BgL_varz00_bglt)
																								BgL_nodez00_2547))))->
																				BgL_locz00)), BUNSPEC);
																	((((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt)
																						BgL_new1109z00_2774)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt) (
																								(BgL_varz00_bglt)
																								BgL_nodez00_2547))))->
																				BgL_typez00)), BUNSPEC);
																	((((BgL_varz00_bglt)
																				COBJECT(((BgL_varz00_bglt)
																						BgL_new1109z00_2774)))->
																			BgL_variablez00) =
																		((BgL_variablez00_bglt) (
																				(BgL_variablez00_bglt)
																				BgL_alphaz00_2721)), BUNSPEC);
																	BgL_auxz00_3393 =
																		((obj_t) BgL_new1109z00_2774);
													}}}
												else
													{	/* Ast/substitute.scm 83 */
														bool_t BgL_test1971z00_3530;

														{	/* Ast/substitute.scm 83 */
															obj_t BgL_classz00_2777;

															BgL_classz00_2777 = BGl_atomz00zzast_nodez00;
															if (BGL_OBJECTP(BgL_alphaz00_2721))
																{	/* Ast/substitute.scm 83 */
																	BgL_objectz00_bglt BgL_arg1807z00_2778;

																	BgL_arg1807z00_2778 =
																		(BgL_objectz00_bglt) (BgL_alphaz00_2721);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Ast/substitute.scm 83 */
																			long BgL_idxz00_2779;

																			BgL_idxz00_2779 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_2778);
																			BgL_test1971z00_3530 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_2779 + 2L)) ==
																				BgL_classz00_2777);
																		}
																	else
																		{	/* Ast/substitute.scm 83 */
																			bool_t BgL_res1848z00_2782;

																			{	/* Ast/substitute.scm 83 */
																				obj_t BgL_oclassz00_2783;

																				{	/* Ast/substitute.scm 83 */
																					obj_t BgL_arg1815z00_2784;
																					long BgL_arg1816z00_2785;

																					BgL_arg1815z00_2784 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Ast/substitute.scm 83 */
																						long BgL_arg1817z00_2786;

																						BgL_arg1817z00_2786 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_2778);
																						BgL_arg1816z00_2785 =
																							(BgL_arg1817z00_2786 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_2783 =
																						VECTOR_REF(BgL_arg1815z00_2784,
																						BgL_arg1816z00_2785);
																				}
																				{	/* Ast/substitute.scm 83 */
																					bool_t BgL__ortest_1115z00_2787;

																					BgL__ortest_1115z00_2787 =
																						(BgL_classz00_2777 ==
																						BgL_oclassz00_2783);
																					if (BgL__ortest_1115z00_2787)
																						{	/* Ast/substitute.scm 83 */
																							BgL_res1848z00_2782 =
																								BgL__ortest_1115z00_2787;
																						}
																					else
																						{	/* Ast/substitute.scm 83 */
																							long BgL_odepthz00_2788;

																							{	/* Ast/substitute.scm 83 */
																								obj_t BgL_arg1804z00_2789;

																								BgL_arg1804z00_2789 =
																									(BgL_oclassz00_2783);
																								BgL_odepthz00_2788 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_2789);
																							}
																							if ((2L < BgL_odepthz00_2788))
																								{	/* Ast/substitute.scm 83 */
																									obj_t BgL_arg1802z00_2790;

																									{	/* Ast/substitute.scm 83 */
																										obj_t BgL_arg1803z00_2791;

																										BgL_arg1803z00_2791 =
																											(BgL_oclassz00_2783);
																										BgL_arg1802z00_2790 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_2791, 2L);
																									}
																									BgL_res1848z00_2782 =
																										(BgL_arg1802z00_2790 ==
																										BgL_classz00_2777);
																								}
																							else
																								{	/* Ast/substitute.scm 83 */
																									BgL_res1848z00_2782 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test1971z00_3530 =
																				BgL_res1848z00_2782;
																		}
																}
															else
																{	/* Ast/substitute.scm 83 */
																	BgL_test1971z00_3530 = ((bool_t) 0);
																}
														}
														if (BgL_test1971z00_3530)
															{	/* Ast/substitute.scm 83 */
																BgL_auxz00_3393 = BgL_alphaz00_2721;
															}
														else
															{	/* Ast/substitute.scm 86 */
																obj_t BgL_arg1346z00_2792;

																BgL_arg1346z00_2792 =
																	BGl_shapez00zztools_shapez00(
																	((obj_t)
																		((BgL_varz00_bglt) BgL_nodez00_2547)));
																BgL_auxz00_3393 =
																	BGl_internalzd2errorzd2zztools_errorz00
																	(BGl_string1885z00zzast_substitutez00,
																	BGl_string1886z00zzast_substitutez00,
																	BgL_arg1346z00_2792);
															}
													}
											}
									}
								return ((BgL_nodez00_bglt) BgL_auxz00_3393);
							}
						}
					}
				}
			}
		}

	}



/* &do-substitute!-atom1259 */
	BgL_nodez00_bglt
		BGl_z62dozd2substitutez12zd2atom1259z70zzast_substitutez00(obj_t
		BgL_envz00_2549, obj_t BgL_nodez00_2550, obj_t BgL_sitez00_2551)
	{
		{	/* Ast/substitute.scm 57 */
			return ((BgL_nodez00_bglt) ((BgL_atomz00_bglt) BgL_nodez00_2550));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_substitutez00(void)
	{
		{	/* Ast/substitute.scm 14 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1887z00zzast_substitutez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1887z00zzast_substitutez00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1887z00zzast_substitutez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1887z00zzast_substitutez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1887z00zzast_substitutez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1887z00zzast_substitutez00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1887z00zzast_substitutez00));
			BGl_modulezd2initializa7ationz75zzast_applyz00(277780830L,
				BSTRING_TO_STRING(BGl_string1887z00zzast_substitutez00));
			BGl_modulezd2initializa7ationz75zzast_appz00(449859288L,
				BSTRING_TO_STRING(BGl_string1887z00zzast_substitutez00));
			return
				BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1887z00zzast_substitutez00));
		}

	}

#ifdef __cplusplus
}
#endif
