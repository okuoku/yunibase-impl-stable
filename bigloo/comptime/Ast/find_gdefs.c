/*===========================================================================*/
/*   (Ast/find_gdefs.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/find_gdefs.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_AST_FINDzd2GDEFSzd2_TYPE_DEFINITIONS
#define BGL_BgL_AST_FINDzd2GDEFSzd2_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;


#endif													// BGL_BgL_AST_FINDzd2GDEFSzd2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t BGl_putpropz12z12zz__r4_symbols_6_4z00(obj_t, obj_t, obj_t);
	static obj_t BGl_bindzd2globalzd2defz12z12zzast_findzd2gdefszd2(obj_t, obj_t);
	static obj_t BGl_pushzd2argszd2zzast_findzd2gdefszd2(obj_t, obj_t, obj_t);
	static obj_t BGl_za2allzd2definedzd2idza2z00zzast_findzd2gdefszd2 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zzast_findzd2gdefszd2 = BUNSPEC;
	extern obj_t BGl_userzd2errorzf2locationz20zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_defszd2ze3listz31zzast_findzd2gdefszd2(void);
	static obj_t BGl_toplevelzd2initzd2zzast_findzd2gdefszd2(void);
	static obj_t BGl_z62findzd2globalzd2defsz62zzast_findzd2gdefszd2(obj_t,
		obj_t);
	static obj_t BGl_genericzd2initzd2zzast_findzd2gdefszd2(void);
	static obj_t BGl_objectzd2initzd2zzast_findzd2gdefszd2(void);
	static obj_t BGl_za2tozd2bezd2defineza2z00zzast_findzd2gdefszd2 = BUNSPEC;
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	static obj_t BGl_za2gdefszd2listza2zd2zzast_findzd2gdefszd2 = BUNSPEC;
	extern obj_t BGl_dssslzd2defaultzd2formalz00zztools_dssslz00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_rempropz12z12zz__r4_symbols_6_4z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzast_findzd2gdefszd2(void);
	static obj_t BGl_z62zc3z04anonymousza31332ze3ze5zzast_findzd2gdefszd2(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_dssslzd2defaultedzd2formalzf3zf3zztools_dssslz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_checkzd2tozd2bezd2definezd2zzast_findzd2gdefszd2(void);
	extern obj_t BGl_idzd2ofzd2idz00zzast_identz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31336ze3ze5zzast_findzd2gdefszd2(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31337ze3ze5zzast_findzd2gdefszd2(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	static obj_t BGl_z62tozd2bezd2definez12z70zzast_findzd2gdefszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzast_findzd2gdefszd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_argsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_dssslz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__dssslz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	static bool_t BGl_findzd2mutationsz12zc0zzast_findzd2gdefszd2(obj_t, obj_t);
	BGL_IMPORT obj_t create_struct(obj_t, int);
	static obj_t BGl_cnstzd2initzd2zzast_findzd2gdefszd2(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_findzd2gdefszd2(void);
	extern long BGl_globalzd2arityzd2zztools_argsz00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzast_findzd2gdefszd2(void);
	static obj_t BGl_gczd2rootszd2initz00zzast_findzd2gdefszd2(void);
	extern obj_t BGl_parsezd2idzd2zzast_identz00(obj_t, obj_t);
	static obj_t BGl_za2gdefzd2keyza2zd2zzast_findzd2gdefszd2 = BUNSPEC;
	BGL_IMPORT obj_t BGl_getpropz00zz__r4_symbols_6_4z00(obj_t, obj_t);
	static obj_t BGl_findzd21zd2mutationsz12z12zzast_findzd2gdefszd2(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_loopze70ze7zzast_findzd2gdefszd2(obj_t, obj_t, obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62checkzd2tozd2bezd2definezb0zzast_findzd2gdefszd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tozd2bezd2definez12z12zzast_findzd2gdefszd2(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_findzd2globalzd2defsz00zzast_findzd2gdefszd2(obj_t);
	static obj_t BGl_z62definezd2globalzb0zzast_findzd2gdefszd2(obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT bool_t BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(obj_t);
	static obj_t __cnst[25];


	   
		 
		DEFINE_STRING(BGl_string2045z00zzast_findzd2gdefszd2,
		BgL_bgl_string2045za700za7za7a2064za7, "Can't find global definition", 28);
	      DEFINE_STRING(BGl_string2050z00zzast_findzd2gdefszd2,
		BgL_bgl_string2050za700za7za7a2065za7, "Illegal duplicated definition", 29);
	      DEFINE_STRING(BGl_string2051z00zzast_findzd2gdefszd2,
		BgL_bgl_string2051za700za7za7a2066za7, "let", 3);
	      DEFINE_STRING(BGl_string2052z00zzast_findzd2gdefszd2,
		BgL_bgl_string2052za700za7za7a2067za7, "Illegal bindings", 16);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc2046z00zzast_findzd2gdefszd2,
		BgL_bgl_za762defineza7d2glob2068z00,
		BGl_z62definezd2globalzb0zzast_findzd2gdefszd2);
	      DEFINE_STRING(BGl_string2053z00zzast_findzd2gdefszd2,
		BgL_bgl_string2053za700za7za7a2069za7, "letrec", 6);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc2047z00zzast_findzd2gdefszd2,
		BgL_bgl_za762za7c3za704anonymo2070za7,
		BGl_z62zc3z04anonymousza31332ze3ze5zzast_findzd2gdefszd2);
	      DEFINE_STRING(BGl_string2054z00zzast_findzd2gdefszd2,
		BgL_bgl_string2054za700za7za7a2071za7, "lambda", 6);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc2048z00zzast_findzd2gdefszd2,
		BgL_bgl_za762za7c3za704anonymo2072za7,
		BGl_z62zc3z04anonymousza31337ze3ze5zzast_findzd2gdefszd2);
	      DEFINE_STRING(BGl_string2055z00zzast_findzd2gdefszd2,
		BgL_bgl_string2055za700za7za7a2073za7, "Illegal `lambda' form", 21);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc2049z00zzast_findzd2gdefszd2,
		BgL_bgl_za762za7c3za704anonymo2074za7,
		BGl_z62zc3z04anonymousza31336ze3ze5zzast_findzd2gdefszd2);
	      DEFINE_STRING(BGl_string2056z00zzast_findzd2gdefszd2,
		BgL_bgl_string2056za700za7za7a2075za7, "Illegal `define-inline' form", 28);
	      DEFINE_STRING(BGl_string2057z00zzast_findzd2gdefszd2,
		BgL_bgl_string2057za700za7za7a2076za7, "Illegal `define-generic' form", 29);
	      DEFINE_STRING(BGl_string2058z00zzast_findzd2gdefszd2,
		BgL_bgl_string2058za700za7za7a2077za7,
		"Illegal formal parameter, symbol or named constant expected", 59);
	      DEFINE_STRING(BGl_string2059z00zzast_findzd2gdefszd2,
		BgL_bgl_string2059za700za7za7a2078za7,
		"Illegal formal parameter, symbol expected", 41);
	      DEFINE_STRING(BGl_string2060z00zzast_findzd2gdefszd2,
		BgL_bgl_string2060za700za7za7a2079za7,
		"Can't use both DSSSL named constant, and `.' notation", 53);
	      DEFINE_STRING(BGl_string2061z00zzast_findzd2gdefszd2,
		BgL_bgl_string2061za700za7za7a2080za7, "ast_find-gdefs", 14);
	      DEFINE_STRING(BGl_string2062z00zzast_findzd2gdefszd2,
		BgL_bgl_string2062za700za7za7a2081za7,
		"if case apply bind-exit labels letrec let set! assert quote lambda free-pragma pragma write @ nothing done define-method define-generic define-inline define begin read def find-gdefs ",
		183);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_checkzd2tozd2bezd2definezd2envz00zzast_findzd2gdefszd2,
		BgL_bgl_za762checkza7d2toza7d22082za7,
		BGl_z62checkzd2tozd2bezd2definezb0zzast_findzd2gdefszd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_findzd2globalzd2defszd2envzd2zzast_findzd2gdefszd2,
		BgL_bgl_za762findza7d2global2083z00,
		BGl_z62findzd2globalzd2defsz62zzast_findzd2gdefszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tozd2bezd2definez12zd2envzc0zzast_findzd2gdefszd2,
		BgL_bgl_za762toza7d2beza7d2def2084za7,
		BGl_z62tozd2bezd2definez12z70zzast_findzd2gdefszd2, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_za2allzd2definedzd2idza2z00zzast_findzd2gdefszd2));
		     ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzast_findzd2gdefszd2));
		     ADD_ROOT((void
				*) (&BGl_za2tozd2bezd2defineza2z00zzast_findzd2gdefszd2));
		     ADD_ROOT((void *) (&BGl_za2gdefszd2listza2zd2zzast_findzd2gdefszd2));
		     ADD_ROOT((void *) (&BGl_za2gdefzd2keyza2zd2zzast_findzd2gdefszd2));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzast_findzd2gdefszd2(long
		BgL_checksumz00_2431, char *BgL_fromz00_2432)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_findzd2gdefszd2))
				{
					BGl_requirezd2initializa7ationz75zzast_findzd2gdefszd2 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_findzd2gdefszd2();
					BGl_libraryzd2moduleszd2initz00zzast_findzd2gdefszd2();
					BGl_cnstzd2initzd2zzast_findzd2gdefszd2();
					BGl_importedzd2moduleszd2initz00zzast_findzd2gdefszd2();
					return BGl_toplevelzd2initzd2zzast_findzd2gdefszd2();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_findzd2gdefszd2(void)
	{
		{	/* Ast/find_gdefs.scm 23 */
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"ast_find-gdefs");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_find-gdefs");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_find-gdefs");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"ast_find-gdefs");
			BGl_modulezd2initializa7ationz75zz__dssslz00(0L, "ast_find-gdefs");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"ast_find-gdefs");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"ast_find-gdefs");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "ast_find-gdefs");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "ast_find-gdefs");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_findzd2gdefszd2(void)
	{
		{	/* Ast/find_gdefs.scm 23 */
			{	/* Ast/find_gdefs.scm 23 */
				obj_t BgL_cportz00_2409;

				{	/* Ast/find_gdefs.scm 23 */
					obj_t BgL_stringz00_2416;

					BgL_stringz00_2416 = BGl_string2062z00zzast_findzd2gdefszd2;
					{	/* Ast/find_gdefs.scm 23 */
						obj_t BgL_startz00_2417;

						BgL_startz00_2417 = BINT(0L);
						{	/* Ast/find_gdefs.scm 23 */
							obj_t BgL_endz00_2418;

							BgL_endz00_2418 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2416)));
							{	/* Ast/find_gdefs.scm 23 */

								BgL_cportz00_2409 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2416, BgL_startz00_2417, BgL_endz00_2418);
				}}}}
				{
					long BgL_iz00_2410;

					BgL_iz00_2410 = 24L;
				BgL_loopz00_2411:
					if ((BgL_iz00_2410 == -1L))
						{	/* Ast/find_gdefs.scm 23 */
							return BUNSPEC;
						}
					else
						{	/* Ast/find_gdefs.scm 23 */
							{	/* Ast/find_gdefs.scm 23 */
								obj_t BgL_arg2063z00_2412;

								{	/* Ast/find_gdefs.scm 23 */

									{	/* Ast/find_gdefs.scm 23 */
										obj_t BgL_locationz00_2414;

										BgL_locationz00_2414 = BBOOL(((bool_t) 0));
										{	/* Ast/find_gdefs.scm 23 */

											BgL_arg2063z00_2412 =
												BGl_readz00zz__readerz00(BgL_cportz00_2409,
												BgL_locationz00_2414);
										}
									}
								}
								{	/* Ast/find_gdefs.scm 23 */
									int BgL_tmpz00_2459;

									BgL_tmpz00_2459 = (int) (BgL_iz00_2410);
									CNST_TABLE_SET(BgL_tmpz00_2459, BgL_arg2063z00_2412);
							}}
							{	/* Ast/find_gdefs.scm 23 */
								int BgL_auxz00_2415;

								BgL_auxz00_2415 = (int) ((BgL_iz00_2410 - 1L));
								{
									long BgL_iz00_2464;

									BgL_iz00_2464 = (long) (BgL_auxz00_2415);
									BgL_iz00_2410 = BgL_iz00_2464;
									goto BgL_loopz00_2411;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_findzd2gdefszd2(void)
	{
		{	/* Ast/find_gdefs.scm 23 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzast_findzd2gdefszd2(void)
	{
		{	/* Ast/find_gdefs.scm 23 */
			BGl_za2tozd2bezd2defineza2z00zzast_findzd2gdefszd2 = BNIL;
			BGl_za2gdefzd2keyza2zd2zzast_findzd2gdefszd2 =
				BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(0));
			BGl_za2gdefszd2listza2zd2zzast_findzd2gdefszd2 = BNIL;
			return (BGl_za2allzd2definedzd2idza2z00zzast_findzd2gdefszd2 =
				BNIL, BUNSPEC);
		}

	}



/* to-be-define! */
	BGL_EXPORTED_DEF obj_t
		BGl_tozd2bezd2definez12z12zzast_findzd2gdefszd2(BgL_globalz00_bglt
		BgL_globalz00_17)
	{
		{	/* Ast/find_gdefs.scm 54 */
			return (BGl_za2tozd2bezd2defineza2z00zzast_findzd2gdefszd2 =
				MAKE_YOUNG_PAIR(
					((obj_t) BgL_globalz00_17),
					BGl_za2tozd2bezd2defineza2z00zzast_findzd2gdefszd2), BUNSPEC);
		}

	}



/* &to-be-define! */
	obj_t BGl_z62tozd2bezd2definez12z70zzast_findzd2gdefszd2(obj_t
		BgL_envz00_2381, obj_t BgL_globalz00_2382)
	{
		{	/* Ast/find_gdefs.scm 54 */
			return
				BGl_tozd2bezd2definez12z12zzast_findzd2gdefszd2(
				((BgL_globalz00_bglt) BgL_globalz00_2382));
		}

	}



/* check-to-be-define */
	BGL_EXPORTED_DEF obj_t
		BGl_checkzd2tozd2bezd2definezd2zzast_findzd2gdefszd2(void)
	{
		{	/* Ast/find_gdefs.scm 63 */
			{
				obj_t BgL_l1230z00_1400;

				BgL_l1230z00_1400 = BGl_za2tozd2bezd2defineza2z00zzast_findzd2gdefszd2;
			BgL_zc3z04anonymousza31307ze3z87_1401:
				if (PAIRP(BgL_l1230z00_1400))
					{	/* Ast/find_gdefs.scm 64 */
						{	/* Ast/find_gdefs.scm 65 */
							obj_t BgL_globalz00_1403;

							BgL_globalz00_1403 = CAR(BgL_l1230z00_1400);
							{	/* Ast/find_gdefs.scm 65 */
								obj_t BgL_defz00_1404;

								{	/* Ast/find_gdefs.scm 65 */
									obj_t BgL_arg1315z00_1410;

									BgL_arg1315z00_1410 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_globalz00_1403))))->
										BgL_idz00);
									BgL_defz00_1404 =
										BGl_getpropz00zz__r4_symbols_6_4z00(BgL_arg1315z00_1410,
										BGl_za2gdefzd2keyza2zd2zzast_findzd2gdefszd2);
								}
								{	/* Ast/find_gdefs.scm 66 */
									bool_t BgL_test2088z00_2480;

									if (STRUCTP(BgL_defz00_1404))
										{	/* Ast/find_gdefs.scm 44 */
											BgL_test2088z00_2480 =
												(STRUCT_KEY(BgL_defz00_1404) == CNST_TABLE_REF(1));
										}
									else
										{	/* Ast/find_gdefs.scm 44 */
											BgL_test2088z00_2480 = ((bool_t) 0);
										}
									if (BgL_test2088z00_2480)
										{	/* Ast/find_gdefs.scm 66 */
											BFALSE;
										}
									else
										{	/* Ast/find_gdefs.scm 67 */
											obj_t BgL_arg1310z00_1406;
											obj_t BgL_arg1311z00_1407;

											BgL_arg1310z00_1406 =
												BGl_shapez00zztools_shapez00(BgL_globalz00_1403);
											{	/* Ast/find_gdefs.scm 69 */
												obj_t BgL_arg1314z00_1409;

												BgL_arg1314z00_1409 =
													(((BgL_globalz00_bglt) COBJECT(
															((BgL_globalz00_bglt) BgL_globalz00_1403)))->
													BgL_srcz00);
												BgL_arg1311z00_1407 =
													BGl_shapez00zztools_shapez00(BgL_arg1314z00_1409);
											}
											{	/* Ast/find_gdefs.scm 67 */
												obj_t BgL_list1312z00_1408;

												BgL_list1312z00_1408 = MAKE_YOUNG_PAIR(BNIL, BNIL);
												BGl_userzd2errorzd2zztools_errorz00(BgL_arg1310z00_1406,
													BGl_string2045z00zzast_findzd2gdefszd2,
													BgL_arg1311z00_1407, BgL_list1312z00_1408);
											}
										}
								}
							}
						}
						{
							obj_t BgL_l1230z00_2492;

							BgL_l1230z00_2492 = CDR(BgL_l1230z00_1400);
							BgL_l1230z00_1400 = BgL_l1230z00_2492;
							goto BgL_zc3z04anonymousza31307ze3z87_1401;
						}
					}
				else
					{	/* Ast/find_gdefs.scm 64 */
						((bool_t) 1);
					}
			}
			{
				obj_t BgL_l1232z00_1414;

				BgL_l1232z00_1414 =
					BGl_za2allzd2definedzd2idza2z00zzast_findzd2gdefszd2;
			BgL_zc3z04anonymousza31317ze3z87_1415:
				if (PAIRP(BgL_l1232z00_1414))
					{	/* Ast/find_gdefs.scm 73 */
						{	/* Ast/find_gdefs.scm 73 */
							obj_t BgL_idz00_1417;

							BgL_idz00_1417 = CAR(BgL_l1232z00_1414);
							BGl_rempropz12z12zz__r4_symbols_6_4z00(BgL_idz00_1417,
								BGl_za2gdefzd2keyza2zd2zzast_findzd2gdefszd2);
						}
						{
							obj_t BgL_l1232z00_2498;

							BgL_l1232z00_2498 = CDR(BgL_l1232z00_1414);
							BgL_l1232z00_1414 = BgL_l1232z00_2498;
							goto BgL_zc3z04anonymousza31317ze3z87_1415;
						}
					}
				else
					{	/* Ast/find_gdefs.scm 73 */
						((bool_t) 1);
					}
			}
			BGl_za2allzd2definedzd2idza2z00zzast_findzd2gdefszd2 = BNIL;
			return (BGl_za2tozd2bezd2defineza2z00zzast_findzd2gdefszd2 =
				BNIL, BUNSPEC);
		}

	}



/* &check-to-be-define */
	obj_t BGl_z62checkzd2tozd2bezd2definezb0zzast_findzd2gdefszd2(obj_t
		BgL_envz00_2383)
	{
		{	/* Ast/find_gdefs.scm 63 */
			return BGl_checkzd2tozd2bezd2definezd2zzast_findzd2gdefszd2();
		}

	}



/* bind-global-def! */
	obj_t BGl_bindzd2globalzd2defz12z12zzast_findzd2gdefszd2(obj_t BgL_idz00_19,
		obj_t BgL_arityz00_20)
	{
		{	/* Ast/find_gdefs.scm 102 */
			{	/* Ast/find_gdefs.scm 103 */
				obj_t BgL_defz00_1420;

				{	/* Ast/find_gdefs.scm 103 */
					obj_t BgL_accessz00_2147;

					BgL_accessz00_2147 = CNST_TABLE_REF(2);
					{	/* Ast/find_gdefs.scm 44 */
						obj_t BgL_newz00_2148;

						BgL_newz00_2148 = create_struct(CNST_TABLE_REF(1), (int) (3L));
						{	/* Ast/find_gdefs.scm 44 */
							int BgL_tmpz00_2505;

							BgL_tmpz00_2505 = (int) (2L);
							STRUCT_SET(BgL_newz00_2148, BgL_tmpz00_2505, BgL_arityz00_20);
						}
						{	/* Ast/find_gdefs.scm 44 */
							int BgL_tmpz00_2508;

							BgL_tmpz00_2508 = (int) (1L);
							STRUCT_SET(BgL_newz00_2148, BgL_tmpz00_2508, BgL_accessz00_2147);
						}
						{	/* Ast/find_gdefs.scm 44 */
							int BgL_tmpz00_2511;

							BgL_tmpz00_2511 = (int) (0L);
							STRUCT_SET(BgL_newz00_2148, BgL_tmpz00_2511, BgL_idz00_19);
						}
						BgL_defz00_1420 = BgL_newz00_2148;
				}}
				BGl_za2gdefszd2listza2zd2zzast_findzd2gdefszd2 =
					MAKE_YOUNG_PAIR(BgL_defz00_1420,
					BGl_za2gdefszd2listza2zd2zzast_findzd2gdefszd2);
				BGl_za2allzd2definedzd2idza2z00zzast_findzd2gdefszd2 =
					MAKE_YOUNG_PAIR(BgL_idz00_19,
					BGl_za2allzd2definedzd2idza2z00zzast_findzd2gdefszd2);
				return BGl_putpropz12z12zz__r4_symbols_6_4z00(BgL_idz00_19,
					BGl_za2gdefzd2keyza2zd2zzast_findzd2gdefszd2, BgL_defz00_1420);
			}
		}

	}



/* defs->list */
	obj_t BGl_defszd2ze3listz31zzast_findzd2gdefszd2(void)
	{
		{	/* Ast/find_gdefs.scm 114 */
			{
				obj_t BgL_defsz00_1423;
				obj_t BgL_resz00_1424;

				BgL_defsz00_1423 = BGl_za2gdefszd2listza2zd2zzast_findzd2gdefszd2;
				BgL_resz00_1424 = BNIL;
			BgL_zc3z04anonymousza31320ze3z87_1425:
				if (NULLP(BgL_defsz00_1423))
					{	/* Ast/find_gdefs.scm 117 */
						BGl_za2gdefszd2listza2zd2zzast_findzd2gdefszd2 = BNIL;
						return BgL_resz00_1424;
					}
				else
					{	/* Ast/find_gdefs.scm 121 */
						obj_t BgL_defz00_1427;

						BgL_defz00_1427 = CAR(((obj_t) BgL_defsz00_1423));
						{	/* Ast/find_gdefs.scm 122 */
							obj_t BgL_arg1322z00_1428;
							obj_t BgL_arg1323z00_1429;

							BgL_arg1322z00_1428 = CDR(((obj_t) BgL_defsz00_1423));
							{	/* Ast/find_gdefs.scm 123 */
								obj_t BgL_arg1325z00_1430;

								{	/* Ast/find_gdefs.scm 123 */
									obj_t BgL_arg1326z00_1431;
									obj_t BgL_arg1327z00_1432;

									BgL_arg1326z00_1431 =
										STRUCT_REF(((obj_t) BgL_defz00_1427), (int) (0L));
									{	/* Ast/find_gdefs.scm 123 */
										obj_t BgL_arg1328z00_1433;
										obj_t BgL_arg1329z00_1434;

										BgL_arg1328z00_1433 =
											STRUCT_REF(((obj_t) BgL_defz00_1427), (int) (1L));
										BgL_arg1329z00_1434 =
											STRUCT_REF(((obj_t) BgL_defz00_1427), (int) (2L));
										BgL_arg1327z00_1432 =
											MAKE_YOUNG_PAIR(BgL_arg1328z00_1433, BgL_arg1329z00_1434);
									}
									BgL_arg1325z00_1430 =
										MAKE_YOUNG_PAIR(BgL_arg1326z00_1431, BgL_arg1327z00_1432);
								}
								BgL_arg1323z00_1429 =
									MAKE_YOUNG_PAIR(BgL_arg1325z00_1430, BgL_resz00_1424);
							}
							{
								obj_t BgL_resz00_2536;
								obj_t BgL_defsz00_2535;

								BgL_defsz00_2535 = BgL_arg1322z00_1428;
								BgL_resz00_2536 = BgL_arg1323z00_1429;
								BgL_resz00_1424 = BgL_resz00_2536;
								BgL_defsz00_1423 = BgL_defsz00_2535;
								goto BgL_zc3z04anonymousza31320ze3z87_1425;
							}
						}
					}
			}
		}

	}



/* find-global-defs */
	BGL_EXPORTED_DEF obj_t BGl_findzd2globalzd2defsz00zzast_findzd2gdefszd2(obj_t
		BgL_sexpza2za2_21)
	{
		{	/* Ast/find_gdefs.scm 139 */
			BGl_za2gdefszd2listza2zd2zzast_findzd2gdefszd2 = BNIL;
			BGl_loopze70ze7zzast_findzd2gdefszd2(BGl_proc2046z00zzast_findzd2gdefszd2,
				BGl_proc2047z00zzast_findzd2gdefszd2, BgL_sexpza2za2_21);
			BGl_loopze70ze7zzast_findzd2gdefszd2(BGl_proc2049z00zzast_findzd2gdefszd2,
				BGl_proc2048z00zzast_findzd2gdefszd2, BgL_sexpza2za2_21);
			return BGl_defszd2ze3listz31zzast_findzd2gdefszd2();
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zzast_findzd2gdefszd2(obj_t BgL_actionzd2definezd2_2407,
		obj_t BgL_actionzd2bodyzd2_2406, obj_t BgL_sexpza2za2_1488)
	{
		{	/* Ast/find_gdefs.scm 155 */
		BGl_loopze70ze7zzast_findzd2gdefszd2:
			if (PAIRP(BgL_sexpza2za2_1488))
				{	/* Ast/find_gdefs.scm 158 */
					obj_t BgL_sexpz00_1491;

					BgL_sexpz00_1491 = CAR(BgL_sexpza2za2_1488);
					{
						obj_t BgL_varz00_1494;
						obj_t BgL_argsz00_1495;
						obj_t BgL_expz00_1496;
						obj_t BgL_varz00_1502;
						obj_t BgL_expz00_1503;

						if (PAIRP(BgL_sexpz00_1491))
							{	/* Ast/find_gdefs.scm 159 */
								if ((CAR(((obj_t) BgL_sexpz00_1491)) == CNST_TABLE_REF(3)))
									{	/* Ast/find_gdefs.scm 159 */
										obj_t BgL_arg1375z00_1510;

										BgL_arg1375z00_1510 = CDR(((obj_t) BgL_sexpz00_1491));
										BGl_loopze70ze7zzast_findzd2gdefszd2
											(BgL_actionzd2definezd2_2407, BgL_actionzd2bodyzd2_2406,
											BgL_arg1375z00_1510);
										{
											obj_t BgL_sexpza2za2_2553;

											BgL_sexpza2za2_2553 = CDR(BgL_sexpza2za2_1488);
											BgL_sexpza2za2_1488 = BgL_sexpza2za2_2553;
											goto BGl_loopze70ze7zzast_findzd2gdefszd2;
										}
									}
								else
									{	/* Ast/find_gdefs.scm 159 */
										obj_t BgL_cdrzd2404zd2_1511;

										BgL_cdrzd2404zd2_1511 = CDR(((obj_t) BgL_sexpz00_1491));
										if ((CAR(((obj_t) BgL_sexpz00_1491)) == CNST_TABLE_REF(4)))
											{	/* Ast/find_gdefs.scm 159 */
												if (PAIRP(BgL_cdrzd2404zd2_1511))
													{	/* Ast/find_gdefs.scm 159 */
														obj_t BgL_carzd2408zd2_1515;

														BgL_carzd2408zd2_1515 = CAR(BgL_cdrzd2404zd2_1511);
														if (PAIRP(BgL_carzd2408zd2_1515))
															{	/* Ast/find_gdefs.scm 159 */
																BgL_varz00_1494 = CAR(BgL_carzd2408zd2_1515);
																BgL_argsz00_1495 = CDR(BgL_carzd2408zd2_1515);
																BgL_expz00_1496 = CDR(BgL_cdrzd2404zd2_1511);
															BgL_tagzd2380zd2_1497:
																{	/* Ast/find_gdefs.scm 166 */
																	long BgL_arg1559z00_1556;

																	BgL_arg1559z00_1556 =
																		BGl_globalzd2arityzd2zztools_argsz00
																		(BgL_argsz00_1495);
																	((obj_t(*)(obj_t, obj_t, obj_t,
																				obj_t))
																		PROCEDURE_L_ENTRY
																		(BgL_actionzd2definezd2_2407))
																		(BgL_actionzd2definezd2_2407,
																		BgL_varz00_1494, BINT(BgL_arg1559z00_1556),
																		BgL_sexpz00_1491);
																}
																((obj_t(*)(obj_t, obj_t, obj_t,
																			obj_t))
																	PROCEDURE_L_ENTRY(BgL_actionzd2bodyzd2_2406))
																	(BgL_actionzd2bodyzd2_2406, BgL_argsz00_1495,
																	BgL_expz00_1496, BgL_sexpz00_1491);
																{	/* Ast/find_gdefs.scm 168 */
																	obj_t BgL_arg1561z00_1557;

																	BgL_arg1561z00_1557 =
																		CDR(((obj_t) BgL_sexpza2za2_1488));
																	{
																		obj_t BgL_sexpza2za2_2583;

																		BgL_sexpza2za2_2583 = BgL_arg1561z00_1557;
																		BgL_sexpza2za2_1488 = BgL_sexpza2za2_2583;
																		goto BGl_loopze70ze7zzast_findzd2gdefszd2;
																	}
																}
															}
														else
															{	/* Ast/find_gdefs.scm 159 */
																obj_t BgL_arg1422z00_1521;
																obj_t BgL_arg1434z00_1522;

																BgL_arg1422z00_1521 =
																	CAR(((obj_t) BgL_cdrzd2404zd2_1511));
																BgL_arg1434z00_1522 =
																	CDR(((obj_t) BgL_cdrzd2404zd2_1511));
																BgL_varz00_1502 = BgL_arg1422z00_1521;
																BgL_expz00_1503 = BgL_arg1434z00_1522;
																((obj_t(*)(obj_t, obj_t, obj_t,
																			obj_t))
																	PROCEDURE_L_ENTRY
																	(BgL_actionzd2definezd2_2407))
																	(BgL_actionzd2definezd2_2407, BgL_varz00_1502,
																	BFALSE, BgL_sexpz00_1491);
																((obj_t(*)(obj_t, obj_t, obj_t,
																			obj_t))
																	PROCEDURE_L_ENTRY(BgL_actionzd2bodyzd2_2406))
																	(BgL_actionzd2bodyzd2_2406, BNIL,
																	BgL_expz00_1503, BgL_sexpz00_1491);
																{	/* Ast/find_gdefs.scm 175 */
																	obj_t BgL_arg1565z00_1559;

																	BgL_arg1565z00_1559 =
																		CDR(((obj_t) BgL_sexpza2za2_1488));
																	{
																		obj_t BgL_sexpza2za2_2605;

																		BgL_sexpza2za2_2605 = BgL_arg1565z00_1559;
																		BgL_sexpza2za2_1488 = BgL_sexpza2za2_2605;
																		goto BGl_loopze70ze7zzast_findzd2gdefszd2;
																	}
																}
															}
													}
												else
													{	/* Ast/find_gdefs.scm 159 */
													BgL_tagzd2383zd2_1505:
														{	/* Ast/find_gdefs.scm 177 */
															obj_t BgL_arg1571z00_1560;

															{	/* Ast/find_gdefs.scm 177 */
																obj_t BgL_list1572z00_1561;

																BgL_list1572z00_1561 =
																	MAKE_YOUNG_PAIR(BgL_sexpz00_1491, BNIL);
																BgL_arg1571z00_1560 = BgL_list1572z00_1561;
															}
															((obj_t(*)(obj_t, obj_t, obj_t,
																		obj_t))
																PROCEDURE_L_ENTRY(BgL_actionzd2bodyzd2_2406))
																(BgL_actionzd2bodyzd2_2406, BNIL,
																BgL_arg1571z00_1560, BgL_sexpz00_1491);
														}
														{	/* Ast/find_gdefs.scm 178 */
															obj_t BgL_arg1573z00_1562;

															BgL_arg1573z00_1562 =
																CDR(((obj_t) BgL_sexpza2za2_1488));
															{
																obj_t BgL_sexpza2za2_2615;

																BgL_sexpza2za2_2615 = BgL_arg1573z00_1562;
																BgL_sexpza2za2_1488 = BgL_sexpza2za2_2615;
																goto BGl_loopze70ze7zzast_findzd2gdefszd2;
															}
														}
													}
											}
										else
											{	/* Ast/find_gdefs.scm 159 */
												if (
													(CAR(
															((obj_t) BgL_sexpz00_1491)) == CNST_TABLE_REF(5)))
													{	/* Ast/find_gdefs.scm 159 */
														if (PAIRP(BgL_cdrzd2404zd2_1511))
															{	/* Ast/find_gdefs.scm 159 */
																obj_t BgL_carzd2477zd2_1527;

																BgL_carzd2477zd2_1527 =
																	CAR(BgL_cdrzd2404zd2_1511);
																if (PAIRP(BgL_carzd2477zd2_1527))
																	{
																		obj_t BgL_expz00_2630;
																		obj_t BgL_argsz00_2628;
																		obj_t BgL_varz00_2626;

																		BgL_varz00_2626 =
																			CAR(BgL_carzd2477zd2_1527);
																		BgL_argsz00_2628 =
																			CDR(BgL_carzd2477zd2_1527);
																		BgL_expz00_2630 =
																			CDR(BgL_cdrzd2404zd2_1511);
																		BgL_expz00_1496 = BgL_expz00_2630;
																		BgL_argsz00_1495 = BgL_argsz00_2628;
																		BgL_varz00_1494 = BgL_varz00_2626;
																		goto BgL_tagzd2380zd2_1497;
																	}
																else
																	{	/* Ast/find_gdefs.scm 159 */
																		goto BgL_tagzd2383zd2_1505;
																	}
															}
														else
															{	/* Ast/find_gdefs.scm 159 */
																goto BgL_tagzd2383zd2_1505;
															}
													}
												else
													{	/* Ast/find_gdefs.scm 159 */
														obj_t BgL_cdrzd2516zd2_1532;

														BgL_cdrzd2516zd2_1532 =
															CDR(((obj_t) BgL_sexpz00_1491));
														if (
															(CAR(
																	((obj_t) BgL_sexpz00_1491)) ==
																CNST_TABLE_REF(6)))
															{	/* Ast/find_gdefs.scm 159 */
																if (PAIRP(BgL_cdrzd2516zd2_1532))
																	{	/* Ast/find_gdefs.scm 159 */
																		obj_t BgL_carzd2520zd2_1536;

																		BgL_carzd2520zd2_1536 =
																			CAR(BgL_cdrzd2516zd2_1532);
																		if (PAIRP(BgL_carzd2520zd2_1536))
																			{
																				obj_t BgL_expz00_2648;
																				obj_t BgL_argsz00_2646;
																				obj_t BgL_varz00_2644;

																				BgL_varz00_2644 =
																					CAR(BgL_carzd2520zd2_1536);
																				BgL_argsz00_2646 =
																					CDR(BgL_carzd2520zd2_1536);
																				BgL_expz00_2648 =
																					CDR(BgL_cdrzd2516zd2_1532);
																				BgL_expz00_1496 = BgL_expz00_2648;
																				BgL_argsz00_1495 = BgL_argsz00_2646;
																				BgL_varz00_1494 = BgL_varz00_2644;
																				goto BgL_tagzd2380zd2_1497;
																			}
																		else
																			{	/* Ast/find_gdefs.scm 159 */
																				goto BgL_tagzd2383zd2_1505;
																			}
																	}
																else
																	{	/* Ast/find_gdefs.scm 159 */
																		goto BgL_tagzd2383zd2_1505;
																	}
															}
														else
															{	/* Ast/find_gdefs.scm 159 */
																if (
																	(CAR(
																			((obj_t) BgL_sexpz00_1491)) ==
																		CNST_TABLE_REF(7)))
																	{	/* Ast/find_gdefs.scm 159 */
																		if (PAIRP(BgL_cdrzd2516zd2_1532))
																			{	/* Ast/find_gdefs.scm 159 */
																				obj_t BgL_carzd2560zd2_1545;

																				BgL_carzd2560zd2_1545 =
																					CAR(BgL_cdrzd2516zd2_1532);
																				if (PAIRP(BgL_carzd2560zd2_1545))
																					{	/* Ast/find_gdefs.scm 159 */
																						obj_t BgL_arg1514z00_1548;
																						obj_t BgL_arg1516z00_1549;

																						BgL_arg1514z00_1548 =
																							CDR(BgL_carzd2560zd2_1545);
																						BgL_arg1516z00_1549 =
																							CDR(BgL_cdrzd2516zd2_1532);
																						((obj_t(*)(obj_t, obj_t, obj_t,
																									obj_t))
																							PROCEDURE_L_ENTRY
																							(BgL_actionzd2bodyzd2_2406))
																							(BgL_actionzd2bodyzd2_2406,
																							BgL_arg1514z00_1548,
																							BgL_arg1516z00_1549,
																							BgL_sexpz00_1491);
																						{
																							obj_t BgL_sexpza2za2_2668;

																							BgL_sexpza2za2_2668 =
																								CDR(BgL_sexpza2za2_1488);
																							BgL_sexpza2za2_1488 =
																								BgL_sexpza2za2_2668;
																							goto
																								BGl_loopze70ze7zzast_findzd2gdefszd2;
																						}
																					}
																				else
																					{	/* Ast/find_gdefs.scm 159 */
																						goto BgL_tagzd2383zd2_1505;
																					}
																			}
																		else
																			{	/* Ast/find_gdefs.scm 159 */
																				goto BgL_tagzd2383zd2_1505;
																			}
																	}
																else
																	{	/* Ast/find_gdefs.scm 159 */
																		goto BgL_tagzd2383zd2_1505;
																	}
															}
													}
											}
									}
							}
						else
							{	/* Ast/find_gdefs.scm 159 */
								goto BgL_tagzd2383zd2_1505;
							}
					}
				}
			else
				{	/* Ast/find_gdefs.scm 156 */
					return CNST_TABLE_REF(8);
				}
		}

	}



/* &find-global-defs */
	obj_t BGl_z62findzd2globalzd2defsz62zzast_findzd2gdefszd2(obj_t
		BgL_envz00_2388, obj_t BgL_sexpza2za2_2389)
	{
		{	/* Ast/find_gdefs.scm 139 */
			return
				BGl_findzd2globalzd2defsz00zzast_findzd2gdefszd2(BgL_sexpza2za2_2389);
		}

	}



/* &<@anonymous:1337> */
	obj_t BGl_z62zc3z04anonymousza31337ze3ze5zzast_findzd2gdefszd2(obj_t
		BgL_envz00_2390, obj_t BgL_argsz00_2391, obj_t BgL_expz00_2392,
		obj_t BgL_defz00_2393)
	{
		{	/* Ast/find_gdefs.scm 187 */
			return
				BBOOL(BGl_findzd2mutationsz12zc0zzast_findzd2gdefszd2(BgL_expz00_2392,
					BGl_pushzd2argszd2zzast_findzd2gdefszd2(BgL_argsz00_2391, BNIL,
						BGl_findzd2locationzd2zztools_locationz00(BgL_defz00_2393))));
		}

	}



/* &<@anonymous:1336> */
	obj_t BGl_z62zc3z04anonymousza31336ze3ze5zzast_findzd2gdefszd2(obj_t
		BgL_envz00_2394, obj_t BgL_xz00_2395, obj_t BgL_yz00_2396,
		obj_t BgL_expz00_2397)
	{
		{	/* Ast/find_gdefs.scm 185 */
			return CNST_TABLE_REF(9);
		}

	}



/* &<@anonymous:1332> */
	obj_t BGl_z62zc3z04anonymousza31332ze3ze5zzast_findzd2gdefszd2(obj_t
		BgL_envz00_2398, obj_t BgL_argsz00_2399, obj_t BgL_expz00_2400,
		obj_t BgL_defz00_2401)
	{
		{	/* Ast/find_gdefs.scm 182 */
			return CNST_TABLE_REF(9);
		}

	}



/* &define-global */
	obj_t BGl_z62definezd2globalzb0zzast_findzd2gdefszd2(obj_t BgL_envz00_2402,
		obj_t BgL_varz00_2403, obj_t BgL_arityz00_2404, obj_t BgL_expz00_2405)
	{
		{	/* Ast/find_gdefs.scm 152 */
			{
				obj_t BgL_prezd2idzd2_2425;

				if (PAIRP(BgL_varz00_2403))
					{	/* Ast/find_gdefs.scm 152 */
						obj_t BgL_cdrzd2367zd2_2429;

						BgL_cdrzd2367zd2_2429 = CDR(((obj_t) BgL_varz00_2403));
						if ((CAR(((obj_t) BgL_varz00_2403)) == CNST_TABLE_REF(10)))
							{	/* Ast/find_gdefs.scm 152 */
								if (PAIRP(BgL_cdrzd2367zd2_2429))
									{	/* Ast/find_gdefs.scm 152 */
										obj_t BgL_cdrzd2370zd2_2430;

										BgL_cdrzd2370zd2_2430 = CDR(BgL_cdrzd2367zd2_2429);
										if (PAIRP(BgL_cdrzd2370zd2_2430))
											{	/* Ast/find_gdefs.scm 152 */
												if (NULLP(CDR(BgL_cdrzd2370zd2_2430)))
													{	/* Ast/find_gdefs.scm 152 */
														BgL_prezd2idzd2_2425 = CAR(BgL_cdrzd2367zd2_2429);
													BgL_tagzd2359zd2_2424:
														{	/* Ast/find_gdefs.scm 146 */
															obj_t BgL_idz00_2426;

															BgL_idz00_2426 =
																BGl_idzd2ofzd2idz00zzast_identz00
																(BgL_prezd2idzd2_2425,
																BGl_findzd2locationzd2zztools_locationz00
																(BgL_expz00_2405));
															{	/* Ast/find_gdefs.scm 146 */
																obj_t BgL_oldzd2defzd2_2427;

																BgL_oldzd2defzd2_2427 =
																	BGl_getpropz00zz__r4_symbols_6_4z00
																	(BgL_idz00_2426,
																	BGl_za2gdefzd2keyza2zd2zzast_findzd2gdefszd2);
																{	/* Ast/find_gdefs.scm 147 */

																	{	/* Ast/find_gdefs.scm 148 */
																		bool_t BgL_test2112z00_2698;

																		if (STRUCTP(BgL_oldzd2defzd2_2427))
																			{	/* Ast/find_gdefs.scm 44 */
																				BgL_test2112z00_2698 =
																					(STRUCT_KEY(BgL_oldzd2defzd2_2427) ==
																					CNST_TABLE_REF(1));
																			}
																		else
																			{	/* Ast/find_gdefs.scm 44 */
																				BgL_test2112z00_2698 = ((bool_t) 0);
																			}
																		if (BgL_test2112z00_2698)
																			{	/* Ast/find_gdefs.scm 149 */
																				obj_t BgL_list1363z00_2428;

																				BgL_list1363z00_2428 =
																					MAKE_YOUNG_PAIR(BNIL, BNIL);
																				return
																					BGl_userzd2errorzd2zztools_errorz00
																					(BgL_varz00_2403,
																					BGl_string2050z00zzast_findzd2gdefszd2,
																					BgL_expz00_2405,
																					BgL_list1363z00_2428);
																			}
																		else
																			{	/* Ast/find_gdefs.scm 148 */
																				return
																					BGl_bindzd2globalzd2defz12z12zzast_findzd2gdefszd2
																					(BgL_idz00_2426, BgL_arityz00_2404);
																			}
																	}
																}
															}
														}
													}
												else
													{
														obj_t BgL_prezd2idzd2_2708;

														BgL_prezd2idzd2_2708 = BgL_varz00_2403;
														BgL_prezd2idzd2_2425 = BgL_prezd2idzd2_2708;
														goto BgL_tagzd2359zd2_2424;
													}
											}
										else
											{
												obj_t BgL_prezd2idzd2_2709;

												BgL_prezd2idzd2_2709 = BgL_varz00_2403;
												BgL_prezd2idzd2_2425 = BgL_prezd2idzd2_2709;
												goto BgL_tagzd2359zd2_2424;
											}
									}
								else
									{
										obj_t BgL_prezd2idzd2_2710;

										BgL_prezd2idzd2_2710 = BgL_varz00_2403;
										BgL_prezd2idzd2_2425 = BgL_prezd2idzd2_2710;
										goto BgL_tagzd2359zd2_2424;
									}
							}
						else
							{
								obj_t BgL_prezd2idzd2_2711;

								BgL_prezd2idzd2_2711 = BgL_varz00_2403;
								BgL_prezd2idzd2_2425 = BgL_prezd2idzd2_2711;
								goto BgL_tagzd2359zd2_2424;
							}
					}
				else
					{
						obj_t BgL_prezd2idzd2_2712;

						BgL_prezd2idzd2_2712 = BgL_varz00_2403;
						BgL_prezd2idzd2_2425 = BgL_prezd2idzd2_2712;
						goto BgL_tagzd2359zd2_2424;
					}
			}
		}

	}



/* find-mutations! */
	bool_t BGl_findzd2mutationsz12zc0zzast_findzd2gdefszd2(obj_t BgL_expza2za2_22,
		obj_t BgL_stackz00_23)
	{
		{	/* Ast/find_gdefs.scm 195 */
			{
				obj_t BgL_l1234z00_1567;

				BgL_l1234z00_1567 = BgL_expza2za2_22;
			BgL_zc3z04anonymousza31574ze3z87_1568:
				if (PAIRP(BgL_l1234z00_1567))
					{	/* Ast/find_gdefs.scm 196 */
						BGl_findzd21zd2mutationsz12z12zzast_findzd2gdefszd2(CAR
							(BgL_l1234z00_1567), BgL_stackz00_23);
						{
							obj_t BgL_l1234z00_2717;

							BgL_l1234z00_2717 = CDR(BgL_l1234z00_1567);
							BgL_l1234z00_1567 = BgL_l1234z00_2717;
							goto BgL_zc3z04anonymousza31574ze3z87_1568;
						}
					}
				else
					{	/* Ast/find_gdefs.scm 196 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* find-1-mutations! */
	obj_t BGl_findzd21zd2mutationsz12z12zzast_findzd2gdefszd2(obj_t BgL_expz00_24,
		obj_t BgL_stackz00_25)
	{
		{	/* Ast/find_gdefs.scm 201 */
		BGl_findzd21zd2mutationsz12z12zzast_findzd2gdefszd2:
			{
				obj_t BgL_valz00_1608;
				obj_t BgL_clausesz00_1609;
				obj_t BgL_bindingsz00_1596;
				obj_t BgL_bodyz00_1597;
				obj_t BgL_bindingsz00_1593;
				obj_t BgL_bodyz00_1594;
				obj_t BgL_bindingsz00_1590;
				obj_t BgL_bodyz00_1591;
				obj_t BgL_idz00_1586;
				obj_t BgL_modulez00_1587;
				obj_t BgL_valz00_1588;
				obj_t BgL_idz00_1583;
				obj_t BgL_valz00_1584;

				if (PAIRP(BgL_expz00_24))
					{	/* Ast/find_gdefs.scm 202 */
						obj_t BgL_cdrzd2663zd2_1628;

						BgL_cdrzd2663zd2_1628 = CDR(((obj_t) BgL_expz00_24));
						if ((CAR(((obj_t) BgL_expz00_24)) == CNST_TABLE_REF(15)))
							{	/* Ast/find_gdefs.scm 202 */
								if (PAIRP(BgL_cdrzd2663zd2_1628))
									{	/* Ast/find_gdefs.scm 202 */
										if (NULLP(CDR(BgL_cdrzd2663zd2_1628)))
											{	/* Ast/find_gdefs.scm 202 */
												return CNST_TABLE_REF(8);
											}
										else
											{	/* Ast/find_gdefs.scm 202 */
											BgL_tagzd2631zd2_1625:
												{	/* Ast/find_gdefs.scm 300 */
													obj_t BgL_callerz00_1901;
													obj_t BgL_locz00_1902;

													BgL_callerz00_1901 = CAR(((obj_t) BgL_expz00_24));
													BgL_locz00_1902 =
														BGl_findzd2locationzd2zztools_locationz00
														(BgL_expz00_24);
													if (SYMBOLP(BgL_callerz00_1901))
														{	/* Ast/find_gdefs.scm 304 */
															obj_t BgL_pidz00_1904;

															BgL_pidz00_1904 =
																BGl_parsezd2idzd2zzast_identz00
																(BgL_callerz00_1901, BgL_locz00_1902);
															{	/* Ast/find_gdefs.scm 304 */
																obj_t BgL_idz00_1905;

																BgL_idz00_1905 = CAR(BgL_pidz00_1904);
																{	/* Ast/find_gdefs.scm 306 */

																	if ((BgL_idz00_1905 == CNST_TABLE_REF(12)))
																		{	/* Ast/find_gdefs.scm 307 */
																			return CNST_TABLE_REF(8);
																		}
																	else
																		{	/* Ast/find_gdefs.scm 307 */
																			if (
																				(BgL_idz00_1905 == CNST_TABLE_REF(13)))
																				{	/* Ast/find_gdefs.scm 307 */
																					return CNST_TABLE_REF(8);
																				}
																			else
																				{	/* Ast/find_gdefs.scm 307 */
																					if (
																						(BgL_idz00_1905 ==
																							CNST_TABLE_REF(14)))
																						{

																							if (PAIRP(BgL_expz00_24))
																								{	/* Ast/find_gdefs.scm 313 */
																									obj_t BgL_cdrzd23454zd2_1917;

																									BgL_cdrzd23454zd2_1917 =
																										CDR(
																										((obj_t) BgL_expz00_24));
																									if (PAIRP
																										(BgL_cdrzd23454zd2_1917))
																										{	/* Ast/find_gdefs.scm 313 */
																											obj_t BgL_arg1945z00_1919;
																											obj_t BgL_arg1946z00_1920;

																											BgL_arg1945z00_1919 =
																												CAR
																												(BgL_cdrzd23454zd2_1917);
																											BgL_arg1946z00_1920 =
																												CDR
																												(BgL_cdrzd23454zd2_1917);
																											{	/* Ast/find_gdefs.scm 315 */
																												obj_t
																													BgL_arg1947z00_2282;
																												BgL_arg1947z00_2282 =
																													BGl_pushzd2argszd2zzast_findzd2gdefszd2
																													(BgL_arg1945z00_1919,
																													BgL_stackz00_25,
																													BgL_locz00_1902);
																												return
																													BBOOL
																													(BGl_findzd2mutationsz12zc0zzast_findzd2gdefszd2
																													(BgL_arg1946z00_1920,
																														BgL_arg1947z00_2282));
																											}
																										}
																									else
																										{	/* Ast/find_gdefs.scm 313 */
																										BgL_tagzd23447zd2_1914:
																											{	/* Ast/find_gdefs.scm 317 */
																												obj_t
																													BgL_list1948z00_1922;
																												BgL_list1948z00_1922 =
																													MAKE_YOUNG_PAIR(BNIL,
																													BNIL);
																												return
																													BGl_userzd2errorzd2zztools_errorz00
																													(BGl_string2054z00zzast_findzd2gdefszd2,
																													BGl_string2055z00zzast_findzd2gdefszd2,
																													BgL_expz00_24,
																													BgL_list1948z00_1922);
																											}
																										}
																								}
																							else
																								{	/* Ast/find_gdefs.scm 313 */
																									goto BgL_tagzd23447zd2_1914;
																								}
																						}
																					else
																						{	/* Ast/find_gdefs.scm 307 */
																							return
																								BBOOL
																								(BGl_findzd2mutationsz12zc0zzast_findzd2gdefszd2
																								(BgL_expz00_24,
																									BgL_stackz00_25));
																						}
																				}
																		}
																}
															}
														}
													else
														{	/* Ast/find_gdefs.scm 302 */
															return
																BBOOL
																(BGl_findzd2mutationsz12zc0zzast_findzd2gdefszd2
																(BgL_expz00_24, BgL_stackz00_25));
														}
												}
											}
									}
								else
									{	/* Ast/find_gdefs.scm 202 */
										goto BgL_tagzd2631zd2_1625;
									}
							}
						else
							{	/* Ast/find_gdefs.scm 202 */
								if ((CAR(((obj_t) BgL_expz00_24)) == CNST_TABLE_REF(12)))
									{	/* Ast/find_gdefs.scm 202 */
										if (PAIRP(BgL_cdrzd2663zd2_1628))
											{	/* Ast/find_gdefs.scm 202 */
												if (NULLP(CDR(BgL_cdrzd2663zd2_1628)))
													{	/* Ast/find_gdefs.scm 202 */
														return CNST_TABLE_REF(8);
													}
												else
													{	/* Ast/find_gdefs.scm 202 */
														goto BgL_tagzd2631zd2_1625;
													}
											}
										else
											{	/* Ast/find_gdefs.scm 202 */
												goto BgL_tagzd2631zd2_1625;
											}
									}
								else
									{	/* Ast/find_gdefs.scm 202 */
										obj_t BgL_cdrzd21347zd2_1642;

										BgL_cdrzd21347zd2_1642 = CDR(((obj_t) BgL_expz00_24));
										if ((CAR(((obj_t) BgL_expz00_24)) == CNST_TABLE_REF(13)))
											{	/* Ast/find_gdefs.scm 202 */
												if (PAIRP(BgL_cdrzd21347zd2_1642))
													{	/* Ast/find_gdefs.scm 202 */
														if (NULLP(CDR(BgL_cdrzd21347zd2_1642)))
															{	/* Ast/find_gdefs.scm 202 */
																return CNST_TABLE_REF(8);
															}
														else
															{	/* Ast/find_gdefs.scm 202 */
																goto BgL_tagzd2631zd2_1625;
															}
													}
												else
													{	/* Ast/find_gdefs.scm 202 */
														goto BgL_tagzd2631zd2_1625;
													}
											}
										else
											{	/* Ast/find_gdefs.scm 202 */
												if (
													(CAR(((obj_t) BgL_expz00_24)) == CNST_TABLE_REF(16)))
													{	/* Ast/find_gdefs.scm 202 */
														if (PAIRP(BgL_cdrzd21347zd2_1642))
															{	/* Ast/find_gdefs.scm 202 */
																obj_t BgL_cdrzd21659zd2_1653;

																BgL_cdrzd21659zd2_1653 =
																	CDR(BgL_cdrzd21347zd2_1642);
																if (PAIRP(BgL_cdrzd21659zd2_1653))
																	{	/* Ast/find_gdefs.scm 202 */
																		obj_t BgL_arg1626z00_1656;

																		BgL_arg1626z00_1656 =
																			CDR(BgL_cdrzd21659zd2_1653);
																		return
																			BBOOL
																			(BGl_findzd2mutationsz12zc0zzast_findzd2gdefszd2
																			(BgL_arg1626z00_1656, BgL_stackz00_25));
																	}
																else
																	{	/* Ast/find_gdefs.scm 202 */
																		goto BgL_tagzd2631zd2_1625;
																	}
															}
														else
															{	/* Ast/find_gdefs.scm 202 */
																goto BgL_tagzd2631zd2_1625;
															}
													}
												else
													{	/* Ast/find_gdefs.scm 202 */
														if (
															(CAR(
																	((obj_t) BgL_expz00_24)) ==
																CNST_TABLE_REF(3)))
															{	/* Ast/find_gdefs.scm 202 */
																obj_t BgL_arg1629z00_1659;

																BgL_arg1629z00_1659 =
																	CDR(((obj_t) BgL_expz00_24));
																return
																	BBOOL
																	(BGl_findzd2mutationsz12zc0zzast_findzd2gdefszd2
																	(BgL_arg1629z00_1659, BgL_stackz00_25));
															}
														else
															{	/* Ast/find_gdefs.scm 202 */
																obj_t BgL_cdrzd21957zd2_1660;

																BgL_cdrzd21957zd2_1660 =
																	CDR(((obj_t) BgL_expz00_24));
																if (
																	(CAR(
																			((obj_t) BgL_expz00_24)) ==
																		CNST_TABLE_REF(17)))
																	{	/* Ast/find_gdefs.scm 202 */
																		if (PAIRP(BgL_cdrzd21957zd2_1660))
																			{	/* Ast/find_gdefs.scm 202 */
																				obj_t BgL_carzd21960zd2_1664;

																				BgL_carzd21960zd2_1664 =
																					CAR(BgL_cdrzd21957zd2_1660);
																				if (SYMBOLP(BgL_carzd21960zd2_1664))
																					{	/* Ast/find_gdefs.scm 202 */
																						BgL_idz00_1583 =
																							BgL_carzd21960zd2_1664;
																						BgL_valz00_1584 =
																							CDR(BgL_cdrzd21957zd2_1660);
																						BGl_findzd2mutationsz12zc0zzast_findzd2gdefszd2
																							(BgL_valz00_1584,
																							BgL_stackz00_25);
																						if (CBOOL
																							(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																								(BgL_idz00_1583,
																									BgL_stackz00_25)))
																							{	/* Ast/find_gdefs.scm 217 */
																								return BFALSE;
																							}
																						else
																							{	/* Ast/find_gdefs.scm 218 */
																								obj_t BgL_defz00_1785;

																								BgL_defz00_1785 =
																									BGl_getpropz00zz__r4_symbols_6_4z00
																									(BgL_idz00_1583,
																									BGl_za2gdefzd2keyza2zd2zzast_findzd2gdefszd2);
																								{	/* Ast/find_gdefs.scm 219 */
																									bool_t BgL_test2139z00_2832;

																									if (STRUCTP(BgL_defz00_1785))
																										{	/* Ast/find_gdefs.scm 44 */
																											BgL_test2139z00_2832 =
																												(STRUCT_KEY
																												(BgL_defz00_1785) ==
																												CNST_TABLE_REF(1));
																										}
																									else
																										{	/* Ast/find_gdefs.scm 44 */
																											BgL_test2139z00_2832 =
																												((bool_t) 0);
																										}
																									if (BgL_test2139z00_2832)
																										{	/* Ast/find_gdefs.scm 220 */
																											obj_t BgL_vz00_2216;

																											BgL_vz00_2216 =
																												CNST_TABLE_REF(11);
																											{	/* Ast/find_gdefs.scm 44 */
																												int BgL_tmpz00_2839;

																												BgL_tmpz00_2839 =
																													(int) (1L);
																												return
																													STRUCT_SET
																													(BgL_defz00_1785,
																													BgL_tmpz00_2839,
																													BgL_vz00_2216);
																											}
																										}
																									else
																										{	/* Ast/find_gdefs.scm 219 */
																											return BFALSE;
																										}
																								}
																							}
																					}
																				else
																					{	/* Ast/find_gdefs.scm 202 */
																						obj_t BgL_carzd21985zd2_1668;

																						BgL_carzd21985zd2_1668 =
																							CAR(
																							((obj_t) BgL_cdrzd21957zd2_1660));
																						if (PAIRP(BgL_carzd21985zd2_1668))
																							{	/* Ast/find_gdefs.scm 202 */
																								obj_t BgL_cdrzd21990zd2_1670;

																								BgL_cdrzd21990zd2_1670 =
																									CDR(BgL_carzd21985zd2_1668);
																								if (
																									(CAR(BgL_carzd21985zd2_1668)
																										== CNST_TABLE_REF(10)))
																									{	/* Ast/find_gdefs.scm 202 */
																										if (PAIRP
																											(BgL_cdrzd21990zd2_1670))
																											{	/* Ast/find_gdefs.scm 202 */
																												obj_t
																													BgL_carzd21993zd2_1674;
																												obj_t
																													BgL_cdrzd21994zd2_1675;
																												BgL_carzd21993zd2_1674 =
																													CAR
																													(BgL_cdrzd21990zd2_1670);
																												BgL_cdrzd21994zd2_1675 =
																													CDR
																													(BgL_cdrzd21990zd2_1670);
																												if (SYMBOLP
																													(BgL_carzd21993zd2_1674))
																													{	/* Ast/find_gdefs.scm 202 */
																														if (PAIRP
																															(BgL_cdrzd21994zd2_1675))
																															{	/* Ast/find_gdefs.scm 202 */
																																obj_t
																																	BgL_carzd21999zd2_1678;
																																BgL_carzd21999zd2_1678
																																	=
																																	CAR
																																	(BgL_cdrzd21994zd2_1675);
																																if (SYMBOLP
																																	(BgL_carzd21999zd2_1678))
																																	{	/* Ast/find_gdefs.scm 202 */
																																		if (NULLP
																																			(CDR
																																				(BgL_cdrzd21994zd2_1675)))
																																			{	/* Ast/find_gdefs.scm 202 */
																																				obj_t
																																					BgL_arg1661z00_1682;
																																				BgL_arg1661z00_1682
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_cdrzd21957zd2_1660));
																																				BgL_idz00_1586
																																					=
																																					BgL_carzd21993zd2_1674;
																																				BgL_modulez00_1587
																																					=
																																					BgL_carzd21999zd2_1678;
																																				BgL_valz00_1588
																																					=
																																					BgL_arg1661z00_1682;
																																				BGl_findzd2mutationsz12zc0zzast_findzd2gdefszd2
																																					(BgL_valz00_1588,
																																					BgL_stackz00_25);
																																				if (
																																					(BgL_modulez00_1587
																																						==
																																						BGl_za2moduleza2z00zzmodule_modulez00))
																																					{	/* Ast/find_gdefs.scm 224 */
																																						obj_t
																																							BgL_defz00_1787;
																																						BgL_defz00_1787
																																							=
																																							BGl_getpropz00zz__r4_symbols_6_4z00
																																							(BgL_idz00_1586,
																																							BGl_za2gdefzd2keyza2zd2zzast_findzd2gdefszd2);
																																						{	/* Ast/find_gdefs.scm 225 */
																																							bool_t
																																								BgL_test2149z00_2872;
																																							if (STRUCTP(BgL_defz00_1787))
																																								{	/* Ast/find_gdefs.scm 44 */
																																									BgL_test2149z00_2872
																																										=
																																										(STRUCT_KEY
																																										(BgL_defz00_1787)
																																										==
																																										CNST_TABLE_REF
																																										(1));
																																								}
																																							else
																																								{	/* Ast/find_gdefs.scm 44 */
																																									BgL_test2149z00_2872
																																										=
																																										(
																																										(bool_t)
																																										0);
																																								}
																																							if (BgL_test2149z00_2872)
																																								{	/* Ast/find_gdefs.scm 226 */
																																									obj_t
																																										BgL_vz00_2222;
																																									BgL_vz00_2222
																																										=
																																										CNST_TABLE_REF
																																										(11);
																																									{	/* Ast/find_gdefs.scm 44 */
																																										int
																																											BgL_tmpz00_2879;
																																										BgL_tmpz00_2879
																																											=
																																											(int)
																																											(1L);
																																										return
																																											STRUCT_SET
																																											(BgL_defz00_1787,
																																											BgL_tmpz00_2879,
																																											BgL_vz00_2222);
																																									}
																																								}
																																							else
																																								{	/* Ast/find_gdefs.scm 225 */
																																									return
																																										BFALSE;
																																								}
																																						}
																																					}
																																				else
																																					{	/* Ast/find_gdefs.scm 223 */
																																						return
																																							BFALSE;
																																					}
																																			}
																																		else
																																			{	/* Ast/find_gdefs.scm 202 */
																																				goto
																																					BgL_tagzd2631zd2_1625;
																																			}
																																	}
																																else
																																	{	/* Ast/find_gdefs.scm 202 */
																																		goto
																																			BgL_tagzd2631zd2_1625;
																																	}
																															}
																														else
																															{	/* Ast/find_gdefs.scm 202 */
																																goto
																																	BgL_tagzd2631zd2_1625;
																															}
																													}
																												else
																													{	/* Ast/find_gdefs.scm 202 */
																														goto
																															BgL_tagzd2631zd2_1625;
																													}
																											}
																										else
																											{	/* Ast/find_gdefs.scm 202 */
																												goto
																													BgL_tagzd2631zd2_1625;
																											}
																									}
																								else
																									{	/* Ast/find_gdefs.scm 202 */
																										goto BgL_tagzd2631zd2_1625;
																									}
																							}
																						else
																							{	/* Ast/find_gdefs.scm 202 */
																								goto BgL_tagzd2631zd2_1625;
																							}
																					}
																			}
																		else
																			{	/* Ast/find_gdefs.scm 202 */
																				goto BgL_tagzd2631zd2_1625;
																			}
																	}
																else
																	{	/* Ast/find_gdefs.scm 202 */
																		if (
																			(CAR(
																					((obj_t) BgL_expz00_24)) ==
																				CNST_TABLE_REF(18)))
																			{	/* Ast/find_gdefs.scm 202 */
																				if (PAIRP(BgL_cdrzd21957zd2_1660))
																					{	/* Ast/find_gdefs.scm 202 */
																						obj_t BgL_arg1688z00_1689;
																						obj_t BgL_arg1689z00_1690;

																						BgL_arg1688z00_1689 =
																							CAR(BgL_cdrzd21957zd2_1660);
																						BgL_arg1689z00_1690 =
																							CDR(BgL_cdrzd21957zd2_1660);
																						{	/* Ast/find_gdefs.scm 202 */
																							bool_t BgL_tmpz00_2891;

																							BgL_bindingsz00_1590 =
																								BgL_arg1688z00_1689;
																							BgL_bodyz00_1591 =
																								BgL_arg1689z00_1690;
																							{	/* Ast/find_gdefs.scm 228 */
																								obj_t BgL_newzd2stackzd2_1789;

																								{
																									obj_t BgL_stackz00_1800;
																									obj_t BgL_bindingsz00_1801;

																									BgL_stackz00_1800 =
																										BgL_stackz00_25;
																									BgL_bindingsz00_1801 =
																										BgL_bindingsz00_1590;
																								BgL_zc3z04anonymousza31860ze3z87_1802:
																									if (NULLP
																										(BgL_bindingsz00_1801))
																										{	/* Ast/find_gdefs.scm 231 */
																											BgL_newzd2stackzd2_1789 =
																												BgL_stackz00_1800;
																										}
																									else
																										{	/* Ast/find_gdefs.scm 231 */
																											if (PAIRP
																												(BgL_bindingsz00_1801))
																												{	/* Ast/find_gdefs.scm 235 */
																													bool_t
																														BgL_test2155z00_2896;
																													{	/* Ast/find_gdefs.scm 235 */
																														obj_t
																															BgL_tmpz00_2897;
																														BgL_tmpz00_2897 =
																															CAR
																															(BgL_bindingsz00_1801);
																														BgL_test2155z00_2896
																															=
																															PAIRP
																															(BgL_tmpz00_2897);
																													}
																													if (BgL_test2155z00_2896)
																														{	/* Ast/find_gdefs.scm 241 */
																															obj_t
																																BgL_arg1866z00_1807;
																															obj_t
																																BgL_arg1868z00_1808;
																															BgL_arg1866z00_1807
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_idzd2ofzd2idz00zzast_identz00
																																(CAR(CAR
																																		(BgL_bindingsz00_1801)),
																																	BGl_findzd2locationzd2zztools_locationz00
																																	(BgL_expz00_24)),
																																BgL_stackz00_1800);
																															BgL_arg1868z00_1808
																																=
																																CDR
																																(BgL_bindingsz00_1801);
																															{
																																obj_t
																																	BgL_bindingsz00_2907;
																																obj_t
																																	BgL_stackz00_2906;
																																BgL_stackz00_2906
																																	=
																																	BgL_arg1866z00_1807;
																																BgL_bindingsz00_2907
																																	=
																																	BgL_arg1868z00_1808;
																																BgL_bindingsz00_1801
																																	=
																																	BgL_bindingsz00_2907;
																																BgL_stackz00_1800
																																	=
																																	BgL_stackz00_2906;
																																goto
																																	BgL_zc3z04anonymousza31860ze3z87_1802;
																															}
																														}
																													else
																														{	/* Ast/find_gdefs.scm 236 */
																															obj_t
																																BgL_arg1874z00_1813;
																															obj_t
																																BgL_arg1875z00_1814;
																															BgL_arg1874z00_1813
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_idzd2ofzd2idz00zzast_identz00
																																(CAR
																																	(BgL_bindingsz00_1801),
																																	BGl_findzd2locationzd2zztools_locationz00
																																	(BgL_expz00_24)),
																																BgL_stackz00_1800);
																															BgL_arg1875z00_1814
																																=
																																CDR
																																(BgL_bindingsz00_1801);
																															{
																																obj_t
																																	BgL_bindingsz00_2914;
																																obj_t
																																	BgL_stackz00_2913;
																																BgL_stackz00_2913
																																	=
																																	BgL_arg1874z00_1813;
																																BgL_bindingsz00_2914
																																	=
																																	BgL_arg1875z00_1814;
																																BgL_bindingsz00_1801
																																	=
																																	BgL_bindingsz00_2914;
																																BgL_stackz00_1800
																																	=
																																	BgL_stackz00_2913;
																																goto
																																	BgL_zc3z04anonymousza31860ze3z87_1802;
																															}
																														}
																												}
																											else
																												{	/* Ast/find_gdefs.scm 234 */
																													obj_t
																														BgL_list1880z00_1819;
																													BgL_list1880z00_1819 =
																														MAKE_YOUNG_PAIR
																														(BNIL, BNIL);
																													BgL_newzd2stackzd2_1789
																														=
																														BGl_userzd2errorzd2zztools_errorz00
																														(BGl_string2051z00zzast_findzd2gdefszd2,
																														BGl_string2052z00zzast_findzd2gdefszd2,
																														BgL_expz00_24,
																														BgL_list1880z00_1819);
																												}
																										}
																								}
																								BGl_findzd2mutationsz12zc0zzast_findzd2gdefszd2
																									(BgL_bodyz00_1591,
																									BgL_newzd2stackzd2_1789);
																								if (PAIRP(BgL_bindingsz00_1590))
																									{
																										obj_t BgL_l1236z00_1792;

																										BgL_l1236z00_1792 =
																											BgL_bindingsz00_1590;
																									BgL_zc3z04anonymousza31856ze3z87_1793:
																										if (PAIRP
																											(BgL_l1236z00_1792))
																											{	/* Ast/find_gdefs.scm 247 */
																												{	/* Ast/find_gdefs.scm 247 */
																													obj_t BgL_bz00_1795;

																													BgL_bz00_1795 =
																														CAR
																														(BgL_l1236z00_1792);
																													{	/* Ast/find_gdefs.scm 247 */
																														obj_t
																															BgL_arg1858z00_1796;
																														BgL_arg1858z00_1796
																															=
																															CDR(((obj_t)
																																BgL_bz00_1795));
																														BGl_findzd2mutationsz12zc0zzast_findzd2gdefszd2
																															(BgL_arg1858z00_1796,
																															BgL_stackz00_25);
																													}
																												}
																												{
																													obj_t
																														BgL_l1236z00_2926;
																													BgL_l1236z00_2926 =
																														CDR
																														(BgL_l1236z00_1792);
																													BgL_l1236z00_1792 =
																														BgL_l1236z00_2926;
																													goto
																														BgL_zc3z04anonymousza31856ze3z87_1793;
																												}
																											}
																										else
																											{	/* Ast/find_gdefs.scm 247 */
																												BgL_tmpz00_2891 =
																													((bool_t) 1);
																											}
																									}
																								else
																									{	/* Ast/find_gdefs.scm 246 */
																										BgL_tmpz00_2891 =
																											((bool_t) 0);
																									}
																							}
																							return BBOOL(BgL_tmpz00_2891);
																						}
																					}
																				else
																					{	/* Ast/find_gdefs.scm 202 */
																						goto BgL_tagzd2631zd2_1625;
																					}
																			}
																		else
																			{	/* Ast/find_gdefs.scm 202 */
																				obj_t BgL_cdrzd22881zd2_1691;

																				BgL_cdrzd22881zd2_1691 =
																					CDR(((obj_t) BgL_expz00_24));
																				if (
																					(CAR(
																							((obj_t) BgL_expz00_24)) ==
																						CNST_TABLE_REF(19)))
																					{	/* Ast/find_gdefs.scm 202 */
																						if (PAIRP(BgL_cdrzd22881zd2_1691))
																							{	/* Ast/find_gdefs.scm 202 */
																								obj_t BgL_arg1699z00_1695;
																								obj_t BgL_arg1700z00_1696;

																								BgL_arg1699z00_1695 =
																									CAR(BgL_cdrzd22881zd2_1691);
																								BgL_arg1700z00_1696 =
																									CDR(BgL_cdrzd22881zd2_1691);
																								{	/* Ast/find_gdefs.scm 202 */
																									bool_t BgL_tmpz00_2940;

																									BgL_bindingsz00_1593 =
																										BgL_arg1699z00_1695;
																									BgL_bodyz00_1594 =
																										BgL_arg1700z00_1696;
																									{	/* Ast/find_gdefs.scm 249 */
																										obj_t
																											BgL_newzd2stackzd2_1821;
																										{
																											obj_t BgL_stackz00_1832;
																											obj_t
																												BgL_bindingsz00_1833;
																											BgL_stackz00_1832 =
																												BgL_stackz00_25;
																											BgL_bindingsz00_1833 =
																												BgL_bindingsz00_1593;
																										BgL_zc3z04anonymousza31886ze3z87_1834:
																											if (NULLP
																												(BgL_bindingsz00_1833))
																												{	/* Ast/find_gdefs.scm 252 */
																													BgL_newzd2stackzd2_1821
																														= BgL_stackz00_1832;
																												}
																											else
																												{	/* Ast/find_gdefs.scm 252 */
																													if (PAIRP
																														(BgL_bindingsz00_1833))
																														{	/* Ast/find_gdefs.scm 256 */
																															bool_t
																																BgL_test2162z00_2945;
																															{	/* Ast/find_gdefs.scm 256 */
																																obj_t
																																	BgL_tmpz00_2946;
																																BgL_tmpz00_2946
																																	=
																																	CAR
																																	(BgL_bindingsz00_1833);
																																BgL_test2162z00_2945
																																	=
																																	PAIRP
																																	(BgL_tmpz00_2946);
																															}
																															if (BgL_test2162z00_2945)
																																{	/* Ast/find_gdefs.scm 262 */
																																	obj_t
																																		BgL_arg1891z00_1839;
																																	obj_t
																																		BgL_arg1892z00_1840;
																																	BgL_arg1891z00_1839
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_idzd2ofzd2idz00zzast_identz00
																																		(CAR(CAR
																																				(BgL_bindingsz00_1833)),
																																			BGl_findzd2locationzd2zztools_locationz00
																																			(BgL_expz00_24)),
																																		BgL_stackz00_1832);
																																	BgL_arg1892z00_1840
																																		=
																																		CDR
																																		(BgL_bindingsz00_1833);
																																	{
																																		obj_t
																																			BgL_bindingsz00_2956;
																																		obj_t
																																			BgL_stackz00_2955;
																																		BgL_stackz00_2955
																																			=
																																			BgL_arg1891z00_1839;
																																		BgL_bindingsz00_2956
																																			=
																																			BgL_arg1892z00_1840;
																																		BgL_bindingsz00_1833
																																			=
																																			BgL_bindingsz00_2956;
																																		BgL_stackz00_1832
																																			=
																																			BgL_stackz00_2955;
																																		goto
																																			BgL_zc3z04anonymousza31886ze3z87_1834;
																																	}
																																}
																															else
																																{	/* Ast/find_gdefs.scm 257 */
																																	obj_t
																																		BgL_arg1898z00_1845;
																																	obj_t
																																		BgL_arg1899z00_1846;
																																	BgL_arg1898z00_1845
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_idzd2ofzd2idz00zzast_identz00
																																		(CAR
																																			(BgL_bindingsz00_1833),
																																			BGl_findzd2locationzd2zztools_locationz00
																																			(BgL_expz00_24)),
																																		BgL_stackz00_1832);
																																	BgL_arg1899z00_1846
																																		=
																																		CDR
																																		(BgL_bindingsz00_1833);
																																	{
																																		obj_t
																																			BgL_bindingsz00_2963;
																																		obj_t
																																			BgL_stackz00_2962;
																																		BgL_stackz00_2962
																																			=
																																			BgL_arg1898z00_1845;
																																		BgL_bindingsz00_2963
																																			=
																																			BgL_arg1899z00_1846;
																																		BgL_bindingsz00_1833
																																			=
																																			BgL_bindingsz00_2963;
																																		BgL_stackz00_1832
																																			=
																																			BgL_stackz00_2962;
																																		goto
																																			BgL_zc3z04anonymousza31886ze3z87_1834;
																																	}
																																}
																														}
																													else
																														{	/* Ast/find_gdefs.scm 255 */
																															obj_t
																																BgL_list1905z00_1851;
																															BgL_list1905z00_1851
																																=
																																MAKE_YOUNG_PAIR
																																(BNIL, BNIL);
																															BgL_newzd2stackzd2_1821
																																=
																																BGl_userzd2errorzd2zztools_errorz00
																																(BGl_string2053z00zzast_findzd2gdefszd2,
																																BGl_string2052z00zzast_findzd2gdefszd2,
																																BgL_expz00_24,
																																BgL_list1905z00_1851);
																														}
																												}
																										}
																										BGl_findzd2mutationsz12zc0zzast_findzd2gdefszd2
																											(BgL_bodyz00_1594,
																											BgL_newzd2stackzd2_1821);
																										if (PAIRP
																											(BgL_bindingsz00_1593))
																											{
																												obj_t BgL_l1238z00_1824;

																												BgL_l1238z00_1824 =
																													BgL_bindingsz00_1593;
																											BgL_zc3z04anonymousza31882ze3z87_1825:
																												if (PAIRP
																													(BgL_l1238z00_1824))
																													{	/* Ast/find_gdefs.scm 268 */
																														{	/* Ast/find_gdefs.scm 268 */
																															obj_t
																																BgL_bz00_1827;
																															BgL_bz00_1827 =
																																CAR
																																(BgL_l1238z00_1824);
																															{	/* Ast/find_gdefs.scm 268 */
																																obj_t
																																	BgL_arg1884z00_1828;
																																BgL_arg1884z00_1828
																																	=
																																	CDR(((obj_t)
																																		BgL_bz00_1827));
																																BGl_findzd2mutationsz12zc0zzast_findzd2gdefszd2
																																	(BgL_arg1884z00_1828,
																																	BgL_newzd2stackzd2_1821);
																															}
																														}
																														{
																															obj_t
																																BgL_l1238z00_2975;
																															BgL_l1238z00_2975
																																=
																																CDR
																																(BgL_l1238z00_1824);
																															BgL_l1238z00_1824
																																=
																																BgL_l1238z00_2975;
																															goto
																																BgL_zc3z04anonymousza31882ze3z87_1825;
																														}
																													}
																												else
																													{	/* Ast/find_gdefs.scm 268 */
																														BgL_tmpz00_2940 =
																															((bool_t) 1);
																													}
																											}
																										else
																											{	/* Ast/find_gdefs.scm 267 */
																												BgL_tmpz00_2940 =
																													((bool_t) 0);
																											}
																									}
																									return BBOOL(BgL_tmpz00_2940);
																								}
																							}
																						else
																							{	/* Ast/find_gdefs.scm 202 */
																								goto BgL_tagzd2631zd2_1625;
																							}
																					}
																				else
																					{	/* Ast/find_gdefs.scm 202 */
																						if (
																							(CAR(
																									((obj_t) BgL_expz00_24)) ==
																								CNST_TABLE_REF(20)))
																							{	/* Ast/find_gdefs.scm 202 */
																								if (PAIRP
																									(BgL_cdrzd22881zd2_1691))
																									{	/* Ast/find_gdefs.scm 202 */
																										obj_t BgL_arg1705z00_1701;
																										obj_t BgL_arg1708z00_1702;

																										BgL_arg1705z00_1701 =
																											CAR
																											(BgL_cdrzd22881zd2_1691);
																										BgL_arg1708z00_1702 =
																											CDR
																											(BgL_cdrzd22881zd2_1691);
																										{	/* Ast/find_gdefs.scm 202 */
																											bool_t BgL_tmpz00_2987;

																											BgL_bindingsz00_1596 =
																												BgL_arg1705z00_1701;
																											BgL_bodyz00_1597 =
																												BgL_arg1708z00_1702;
																											{	/* Ast/find_gdefs.scm 271 */
																												obj_t BgL_locz00_1853;

																												BgL_locz00_1853 =
																													BGl_findzd2locationzd2zztools_locationz00
																													(BgL_expz00_24);
																												{	/* Ast/find_gdefs.scm 271 */
																													obj_t
																														BgL_newzd2stackzd2_1854;
																													{	/* Ast/find_gdefs.scm 272 */
																														obj_t
																															BgL_arg1914z00_1866;
																														if (NULLP
																															(BgL_bindingsz00_1596))
																															{	/* Ast/find_gdefs.scm 272 */
																																BgL_arg1914z00_1866
																																	= BNIL;
																															}
																														else
																															{	/* Ast/find_gdefs.scm 272 */
																																obj_t
																																	BgL_head1242z00_1869;
																																{	/* Ast/find_gdefs.scm 272 */
																																	obj_t
																																		BgL_arg1923z00_1881;
																																	{	/* Ast/find_gdefs.scm 272 */
																																		obj_t
																																			BgL_pairz00_2243;
																																		BgL_pairz00_2243
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_bindingsz00_1596));
																																		BgL_arg1923z00_1881
																																			=
																																			CAR
																																			(BgL_pairz00_2243);
																																	}
																																	BgL_head1242z00_1869
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1923z00_1881,
																																		BNIL);
																																}
																																{	/* Ast/find_gdefs.scm 272 */
																																	obj_t
																																		BgL_g1245z00_1870;
																																	BgL_g1245z00_1870
																																		=
																																		CDR(((obj_t)
																																			BgL_bindingsz00_1596));
																																	{
																																		obj_t
																																			BgL_l1240z00_1872;
																																		obj_t
																																			BgL_tail1243z00_1873;
																																		BgL_l1240z00_1872
																																			=
																																			BgL_g1245z00_1870;
																																		BgL_tail1243z00_1873
																																			=
																																			BgL_head1242z00_1869;
																																	BgL_zc3z04anonymousza31916ze3z87_1874:
																																		if (NULLP
																																			(BgL_l1240z00_1872))
																																			{	/* Ast/find_gdefs.scm 272 */
																																				BgL_arg1914z00_1866
																																					=
																																					BgL_head1242z00_1869;
																																			}
																																		else
																																			{	/* Ast/find_gdefs.scm 272 */
																																				obj_t
																																					BgL_newtail1244z00_1876;
																																				{	/* Ast/find_gdefs.scm 272 */
																																					obj_t
																																						BgL_arg1919z00_1878;
																																					{	/* Ast/find_gdefs.scm 272 */
																																						obj_t
																																							BgL_pairz00_2246;
																																						BgL_pairz00_2246
																																							=
																																							CAR
																																							(((obj_t) BgL_l1240z00_1872));
																																						BgL_arg1919z00_1878
																																							=
																																							CAR
																																							(BgL_pairz00_2246);
																																					}
																																					BgL_newtail1244z00_1876
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1919z00_1878,
																																						BNIL);
																																				}
																																				SET_CDR
																																					(BgL_tail1243z00_1873,
																																					BgL_newtail1244z00_1876);
																																				{	/* Ast/find_gdefs.scm 272 */
																																					obj_t
																																						BgL_arg1918z00_1877;
																																					BgL_arg1918z00_1877
																																						=
																																						CDR(
																																						((obj_t) BgL_l1240z00_1872));
																																					{
																																						obj_t
																																							BgL_tail1243z00_3007;
																																						obj_t
																																							BgL_l1240z00_3006;
																																						BgL_l1240z00_3006
																																							=
																																							BgL_arg1918z00_1877;
																																						BgL_tail1243z00_3007
																																							=
																																							BgL_newtail1244z00_1876;
																																						BgL_tail1243z00_1873
																																							=
																																							BgL_tail1243z00_3007;
																																						BgL_l1240z00_1872
																																							=
																																							BgL_l1240z00_3006;
																																						goto
																																							BgL_zc3z04anonymousza31916ze3z87_1874;
																																					}
																																				}
																																			}
																																	}
																																}
																															}
																														BgL_newzd2stackzd2_1854
																															=
																															BGl_pushzd2argszd2zzast_findzd2gdefszd2
																															(BgL_arg1914z00_1866,
																															BgL_stackz00_25,
																															BgL_locz00_1853);
																													}
																													{	/* Ast/find_gdefs.scm 272 */

																														BGl_findzd2mutationsz12zc0zzast_findzd2gdefszd2
																															(BgL_bodyz00_1597,
																															BgL_newzd2stackzd2_1854);
																														if (PAIRP
																															(BgL_bindingsz00_1596))
																															{
																																obj_t
																																	BgL_l1246z00_1857;
																																BgL_l1246z00_1857
																																	=
																																	BgL_bindingsz00_1596;
																															BgL_zc3z04anonymousza31907ze3z87_1858:
																																if (PAIRP
																																	(BgL_l1246z00_1857))
																																	{	/* Ast/find_gdefs.scm 275 */
																																		{	/* Ast/find_gdefs.scm 276 */
																																			obj_t
																																				BgL_bz00_1860;
																																			BgL_bz00_1860
																																				=
																																				CAR
																																				(BgL_l1246z00_1857);
																																			{	/* Ast/find_gdefs.scm 276 */
																																				obj_t
																																					BgL_arg1910z00_1861;
																																				obj_t
																																					BgL_arg1911z00_1862;
																																				{	/* Ast/find_gdefs.scm 276 */
																																					obj_t
																																						BgL_pairz00_2253;
																																					BgL_pairz00_2253
																																						=
																																						CDR(
																																						((obj_t) BgL_bz00_1860));
																																					BgL_arg1910z00_1861
																																						=
																																						CDR
																																						(BgL_pairz00_2253);
																																				}
																																				{	/* Ast/find_gdefs.scm 277 */
																																					obj_t
																																						BgL_arg1912z00_1863;
																																					{	/* Ast/find_gdefs.scm 277 */
																																						obj_t
																																							BgL_pairz00_2257;
																																						BgL_pairz00_2257
																																							=
																																							CDR
																																							(((obj_t) BgL_bz00_1860));
																																						BgL_arg1912z00_1863
																																							=
																																							CAR
																																							(BgL_pairz00_2257);
																																					}
																																					BgL_arg1911z00_1862
																																						=
																																						BGl_pushzd2argszd2zzast_findzd2gdefszd2
																																						(BgL_arg1912z00_1863,
																																						BgL_newzd2stackzd2_1854,
																																						BgL_locz00_1853);
																																				}
																																				BGl_findzd2mutationsz12zc0zzast_findzd2gdefszd2
																																					(BgL_arg1910z00_1861,
																																					BgL_arg1911z00_1862);
																																			}
																																		}
																																		{
																																			obj_t
																																				BgL_l1246z00_3023;
																																			BgL_l1246z00_3023
																																				=
																																				CDR
																																				(BgL_l1246z00_1857);
																																			BgL_l1246z00_1857
																																				=
																																				BgL_l1246z00_3023;
																																			goto
																																				BgL_zc3z04anonymousza31907ze3z87_1858;
																																		}
																																	}
																																else
																																	{	/* Ast/find_gdefs.scm 275 */
																																		BgL_tmpz00_2987
																																			=
																																			((bool_t)
																																			1);
																																	}
																															}
																														else
																															{	/* Ast/find_gdefs.scm 274 */
																																BgL_tmpz00_2987
																																	=
																																	((bool_t) 0);
																															}
																													}
																												}
																											}
																											return
																												BBOOL(BgL_tmpz00_2987);
																										}
																									}
																								else
																									{	/* Ast/find_gdefs.scm 202 */
																										goto BgL_tagzd2631zd2_1625;
																									}
																							}
																						else
																							{	/* Ast/find_gdefs.scm 202 */
																								obj_t BgL_cdrzd23047zd2_1703;

																								BgL_cdrzd23047zd2_1703 =
																									CDR(((obj_t) BgL_expz00_24));
																								if (
																									(CAR(
																											((obj_t) BgL_expz00_24))
																										== CNST_TABLE_REF(14)))
																									{	/* Ast/find_gdefs.scm 202 */
																										if (PAIRP
																											(BgL_cdrzd23047zd2_1703))
																											{	/* Ast/find_gdefs.scm 202 */
																												obj_t
																													BgL_arg1714z00_1707;
																												obj_t
																													BgL_arg1717z00_1708;
																												BgL_arg1714z00_1707 =
																													CAR
																													(BgL_cdrzd23047zd2_1703);
																												BgL_arg1717z00_1708 =
																													CDR
																													(BgL_cdrzd23047zd2_1703);
																												{	/* Ast/find_gdefs.scm 280 */
																													obj_t
																														BgL_arg1925z00_2328;
																													BgL_arg1925z00_2328 =
																														BGl_pushzd2argszd2zzast_findzd2gdefszd2
																														(BgL_arg1714z00_1707,
																														BgL_stackz00_25,
																														BGl_findzd2locationzd2zztools_locationz00
																														(BgL_expz00_24));
																													return
																														BBOOL
																														(BGl_findzd2mutationsz12zc0zzast_findzd2gdefszd2
																														(BgL_arg1717z00_1708,
																															BgL_arg1925z00_2328));
																												}
																											}
																										else
																											{	/* Ast/find_gdefs.scm 202 */
																												goto
																													BgL_tagzd2631zd2_1625;
																											}
																									}
																								else
																									{	/* Ast/find_gdefs.scm 202 */
																										if (
																											(CAR(
																													((obj_t)
																														BgL_expz00_24)) ==
																												CNST_TABLE_REF(21)))
																											{	/* Ast/find_gdefs.scm 202 */
																												if (PAIRP
																													(BgL_cdrzd23047zd2_1703))
																													{	/* Ast/find_gdefs.scm 202 */
																														obj_t
																															BgL_arg1722z00_1713;
																														obj_t
																															BgL_arg1724z00_1714;
																														BgL_arg1722z00_1713
																															=
																															CAR
																															(BgL_cdrzd23047zd2_1703);
																														BgL_arg1724z00_1714
																															=
																															CDR
																															(BgL_cdrzd23047zd2_1703);
																														{	/* Ast/find_gdefs.scm 282 */
																															obj_t
																																BgL_arg1927z00_2334;
																															BgL_arg1927z00_2334
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_idzd2ofzd2idz00zzast_identz00
																																(BgL_arg1722z00_1713,
																																	BGl_findzd2locationzd2zztools_locationz00
																																	(BgL_expz00_24)),
																																BgL_stackz00_25);
																															return
																																BBOOL
																																(BGl_findzd2mutationsz12zc0zzast_findzd2gdefszd2
																																(BgL_arg1724z00_1714,
																																	BgL_arg1927z00_2334));
																														}
																													}
																												else
																													{	/* Ast/find_gdefs.scm 202 */
																														goto
																															BgL_tagzd2631zd2_1625;
																													}
																											}
																										else
																											{	/* Ast/find_gdefs.scm 202 */
																												obj_t
																													BgL_cdrzd23170zd2_1715;
																												BgL_cdrzd23170zd2_1715 =
																													CDR(((obj_t)
																														BgL_expz00_24));
																												if ((CAR(((obj_t)
																																BgL_expz00_24))
																														==
																														CNST_TABLE_REF(22)))
																													{	/* Ast/find_gdefs.scm 202 */
																														if (PAIRP
																															(BgL_cdrzd23170zd2_1715))
																															{	/* Ast/find_gdefs.scm 202 */
																																obj_t
																																	BgL_cdrzd23174zd2_1719;
																																BgL_cdrzd23174zd2_1719
																																	=
																																	CDR
																																	(BgL_cdrzd23170zd2_1715);
																																if (PAIRP
																																	(BgL_cdrzd23174zd2_1719))
																																	{	/* Ast/find_gdefs.scm 202 */
																																		if (NULLP
																																			(CDR
																																				(BgL_cdrzd23174zd2_1719)))
																																			{	/* Ast/find_gdefs.scm 202 */
																																				obj_t
																																					BgL_arg1740z00_1723;
																																				obj_t
																																					BgL_arg1746z00_1724;
																																				BgL_arg1740z00_1723
																																					=
																																					CAR
																																					(BgL_cdrzd23170zd2_1715);
																																				BgL_arg1746z00_1724
																																					=
																																					CAR
																																					(BgL_cdrzd23174zd2_1719);
																																				BGl_findzd21zd2mutationsz12z12zzast_findzd2gdefszd2
																																					(BgL_arg1740z00_1723,
																																					BgL_stackz00_25);
																																				{
																																					obj_t
																																						BgL_expz00_3073;
																																					BgL_expz00_3073
																																						=
																																						BgL_arg1746z00_1724;
																																					BgL_expz00_24
																																						=
																																						BgL_expz00_3073;
																																					goto
																																						BGl_findzd21zd2mutationsz12z12zzast_findzd2gdefszd2;
																																				}
																																			}
																																		else
																																			{	/* Ast/find_gdefs.scm 202 */
																																				goto
																																					BgL_tagzd2631zd2_1625;
																																			}
																																	}
																																else
																																	{	/* Ast/find_gdefs.scm 202 */
																																		goto
																																			BgL_tagzd2631zd2_1625;
																																	}
																															}
																														else
																															{	/* Ast/find_gdefs.scm 202 */
																																goto
																																	BgL_tagzd2631zd2_1625;
																															}
																													}
																												else
																													{	/* Ast/find_gdefs.scm 202 */
																														if (
																															(CAR(
																																	((obj_t)
																																		BgL_expz00_24))
																																==
																																CNST_TABLE_REF
																																(23)))
																															{	/* Ast/find_gdefs.scm 202 */
																																if (PAIRP
																																	(BgL_cdrzd23170zd2_1715))
																																	{	/* Ast/find_gdefs.scm 202 */
																																		obj_t
																																			BgL_cdrzd23282zd2_1730;
																																		BgL_cdrzd23282zd2_1730
																																			=
																																			CDR
																																			(BgL_cdrzd23170zd2_1715);
																																		if (PAIRP
																																			(BgL_cdrzd23282zd2_1730))
																																			{	/* Ast/find_gdefs.scm 202 */
																																				if (NULLP(CDR(BgL_cdrzd23282zd2_1730)))
																																					{	/* Ast/find_gdefs.scm 202 */
																																						obj_t
																																							BgL_arg1755z00_1734;
																																						obj_t
																																							BgL_arg1761z00_1735;
																																						BgL_arg1755z00_1734
																																							=
																																							CAR
																																							(BgL_cdrzd23170zd2_1715);
																																						BgL_arg1761z00_1735
																																							=
																																							CAR
																																							(BgL_cdrzd23282zd2_1730);
																																						{	/* Ast/find_gdefs.scm 202 */
																																							bool_t
																																								BgL_tmpz00_3089;
																																							BgL_valz00_1608
																																								=
																																								BgL_arg1755z00_1734;
																																							BgL_clausesz00_1609
																																								=
																																								BgL_arg1761z00_1735;
																																							BGl_findzd2mutationsz12zc0zzast_findzd2gdefszd2
																																								(BgL_valz00_1608,
																																								BgL_stackz00_25);
																																							{
																																								obj_t
																																									BgL_l1248z00_1889;
																																								BgL_l1248z00_1889
																																									=
																																									BgL_clausesz00_1609;
																																							BgL_zc3z04anonymousza31930ze3z87_1890:
																																								if (PAIRP(BgL_l1248z00_1889))
																																									{	/* Ast/find_gdefs.scm 288 */
																																										{	/* Ast/find_gdefs.scm 288 */
																																											obj_t
																																												BgL_cz00_1892;
																																											BgL_cz00_1892
																																												=
																																												CAR
																																												(BgL_l1248z00_1889);
																																											{	/* Ast/find_gdefs.scm 288 */
																																												obj_t
																																													BgL_arg1932z00_1893;
																																												BgL_arg1932z00_1893
																																													=
																																													CDR
																																													(
																																													((obj_t) BgL_cz00_1892));
																																												BGl_findzd2mutationsz12zc0zzast_findzd2gdefszd2
																																													(BgL_arg1932z00_1893,
																																													BgL_stackz00_25);
																																											}
																																										}
																																										{
																																											obj_t
																																												BgL_l1248z00_3097;
																																											BgL_l1248z00_3097
																																												=
																																												CDR
																																												(BgL_l1248z00_1889);
																																											BgL_l1248z00_1889
																																												=
																																												BgL_l1248z00_3097;
																																											goto
																																												BgL_zc3z04anonymousza31930ze3z87_1890;
																																										}
																																									}
																																								else
																																									{	/* Ast/find_gdefs.scm 288 */
																																										BgL_tmpz00_3089
																																											=
																																											(
																																											(bool_t)
																																											1);
																																									}
																																							}
																																							return
																																								BBOOL
																																								(BgL_tmpz00_3089);
																																						}
																																					}
																																				else
																																					{	/* Ast/find_gdefs.scm 202 */
																																						goto
																																							BgL_tagzd2631zd2_1625;
																																					}
																																			}
																																		else
																																			{	/* Ast/find_gdefs.scm 202 */
																																				goto
																																					BgL_tagzd2631zd2_1625;
																																			}
																																	}
																																else
																																	{	/* Ast/find_gdefs.scm 202 */
																																		goto
																																			BgL_tagzd2631zd2_1625;
																																	}
																															}
																														else
																															{	/* Ast/find_gdefs.scm 202 */
																																if (
																																	(CAR(
																																			((obj_t)
																																				BgL_expz00_24))
																																		==
																																		CNST_TABLE_REF
																																		(24)))
																																	{	/* Ast/find_gdefs.scm 202 */
																																		obj_t
																																			BgL_arg1767z00_1739;
																																		BgL_arg1767z00_1739
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_expz00_24));
																																		return
																																			BBOOL
																																			(BGl_findzd2mutationsz12zc0zzast_findzd2gdefszd2
																																			(BgL_arg1767z00_1739,
																																				BgL_stackz00_25));
																																	}
																																else
																																	{	/* Ast/find_gdefs.scm 202 */
																																		obj_t
																																			BgL_cdrzd23370zd2_1740;
																																		BgL_cdrzd23370zd2_1740
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_expz00_24));
																																		if ((CAR((
																																						(obj_t)
																																						BgL_expz00_24))
																																				==
																																				CNST_TABLE_REF
																																				(4)))
																																			{	/* Ast/find_gdefs.scm 202 */
																																				if (PAIRP(BgL_cdrzd23370zd2_1740))
																																					{	/* Ast/find_gdefs.scm 202 */
																																						obj_t
																																							BgL_arg1773z00_1744;
																																						obj_t
																																							BgL_arg1775z00_1745;
																																						BgL_arg1773z00_1744
																																							=
																																							CAR
																																							(BgL_cdrzd23370zd2_1740);
																																						BgL_arg1775z00_1745
																																							=
																																							CDR
																																							(BgL_cdrzd23370zd2_1740);
																																						{	/* Ast/find_gdefs.scm 292 */
																																							obj_t
																																								BgL_arg1934z00_2355;
																																							BgL_arg1934z00_2355
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg1773z00_1744,
																																								BgL_stackz00_25);
																																							return
																																								BBOOL
																																								(BGl_findzd2mutationsz12zc0zzast_findzd2gdefszd2
																																								(BgL_arg1775z00_1745,
																																									BgL_arg1934z00_2355));
																																						}
																																					}
																																				else
																																					{	/* Ast/find_gdefs.scm 202 */
																																						goto
																																							BgL_tagzd2631zd2_1625;
																																					}
																																			}
																																		else
																																			{	/* Ast/find_gdefs.scm 202 */
																																				if (
																																					(CAR(
																																							((obj_t) BgL_expz00_24)) == CNST_TABLE_REF(5)))
																																					{	/* Ast/find_gdefs.scm 202 */
																																						if (PAIRP(BgL_cdrzd23370zd2_1740))
																																							{	/* Ast/find_gdefs.scm 202 */
																																								obj_t
																																									BgL_arg1805z00_1750;
																																								obj_t
																																									BgL_arg1806z00_1751;
																																								BgL_arg1805z00_1750
																																									=
																																									CAR
																																									(BgL_cdrzd23370zd2_1740);
																																								BgL_arg1806z00_1751
																																									=
																																									CDR
																																									(BgL_cdrzd23370zd2_1740);
																																								{	/* Ast/find_gdefs.scm 294 */
																																									obj_t
																																										BgL_list1935z00_2360;
																																									BgL_list1935z00_2360
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BNIL,
																																										BNIL);
																																									return
																																										BGl_userzd2errorzd2zztools_errorz00
																																										(BgL_arg1805z00_1750,
																																										BGl_string2056z00zzast_findzd2gdefszd2,
																																										BgL_arg1806z00_1751,
																																										BgL_list1935z00_2360);
																																								}
																																							}
																																						else
																																							{	/* Ast/find_gdefs.scm 202 */
																																								goto
																																									BgL_tagzd2631zd2_1625;
																																							}
																																					}
																																				else
																																					{	/* Ast/find_gdefs.scm 202 */
																																						obj_t
																																							BgL_cdrzd23419zd2_1752;
																																						BgL_cdrzd23419zd2_1752
																																							=
																																							CDR
																																							(((obj_t) BgL_expz00_24));
																																						if (
																																							(CAR
																																								(((obj_t) BgL_expz00_24)) == CNST_TABLE_REF(6)))
																																							{	/* Ast/find_gdefs.scm 202 */
																																								if (PAIRP(BgL_cdrzd23419zd2_1752))
																																									{	/* Ast/find_gdefs.scm 202 */
																																										obj_t
																																											BgL_arg1812z00_1756;
																																										obj_t
																																											BgL_arg1820z00_1757;
																																										BgL_arg1812z00_1756
																																											=
																																											CAR
																																											(BgL_cdrzd23419zd2_1752);
																																										BgL_arg1820z00_1757
																																											=
																																											CDR
																																											(BgL_cdrzd23419zd2_1752);
																																										{	/* Ast/find_gdefs.scm 296 */
																																											obj_t
																																												BgL_list1936z00_2365;
																																											BgL_list1936z00_2365
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BNIL,
																																												BNIL);
																																											return
																																												BGl_userzd2errorzd2zztools_errorz00
																																												(BgL_arg1812z00_1756,
																																												BGl_string2057z00zzast_findzd2gdefszd2,
																																												BgL_arg1820z00_1757,
																																												BgL_list1936z00_2365);
																																										}
																																									}
																																								else
																																									{	/* Ast/find_gdefs.scm 202 */
																																										goto
																																											BgL_tagzd2631zd2_1625;
																																									}
																																							}
																																						else
																																							{	/* Ast/find_gdefs.scm 202 */
																																								if ((CAR(((obj_t) BgL_expz00_24)) == CNST_TABLE_REF(7)))
																																									{	/* Ast/find_gdefs.scm 202 */
																																										if (PAIRP(BgL_cdrzd23419zd2_1752))
																																											{	/* Ast/find_gdefs.scm 202 */
																																												obj_t
																																													BgL_carzd23438zd2_1762;
																																												BgL_carzd23438zd2_1762
																																													=
																																													CAR
																																													(BgL_cdrzd23419zd2_1752);
																																												if (PAIRP(BgL_carzd23438zd2_1762))
																																													{	/* Ast/find_gdefs.scm 202 */
																																														obj_t
																																															BgL_arg1831z00_1764;
																																														obj_t
																																															BgL_arg1832z00_1765;
																																														BgL_arg1831z00_1764
																																															=
																																															CDR
																																															(BgL_carzd23438zd2_1762);
																																														BgL_arg1832z00_1765
																																															=
																																															CDR
																																															(BgL_cdrzd23419zd2_1752);
																																														{	/* Ast/find_gdefs.scm 298 */
																																															obj_t
																																																BgL_arg1937z00_2371;
																																															BgL_arg1937z00_2371
																																																=
																																																BGl_pushzd2argszd2zzast_findzd2gdefszd2
																																																(BgL_arg1831z00_1764,
																																																BgL_stackz00_25,
																																																BGl_findzd2locationzd2zztools_locationz00
																																																(BgL_expz00_24));
																																															return
																																																BBOOL
																																																(BGl_findzd2mutationsz12zc0zzast_findzd2gdefszd2
																																																(BgL_arg1832z00_1765,
																																																	BgL_arg1937z00_2371));
																																														}
																																													}
																																												else
																																													{	/* Ast/find_gdefs.scm 202 */
																																														goto
																																															BgL_tagzd2631zd2_1625;
																																													}
																																											}
																																										else
																																											{	/* Ast/find_gdefs.scm 202 */
																																												goto
																																													BgL_tagzd2631zd2_1625;
																																											}
																																									}
																																								else
																																									{	/* Ast/find_gdefs.scm 202 */
																																										goto
																																											BgL_tagzd2631zd2_1625;
																																									}
																																							}
																																					}
																																			}
																																	}
																															}
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
				else
					{	/* Ast/find_gdefs.scm 202 */
						return CNST_TABLE_REF(8);
					}
			}
		}

	}



/* push-args */
	obj_t BGl_pushzd2argszd2zzast_findzd2gdefszd2(obj_t BgL_expr0z00_26,
		obj_t BgL_listz00_27, obj_t BgL_locz00_28)
	{
		{	/* Ast/find_gdefs.scm 331 */
			{
				obj_t BgL_exprz00_1924;
				obj_t BgL_listz00_1925;
				bool_t BgL_dssslz00_1926;

				BgL_exprz00_1924 = BgL_expr0z00_26;
				BgL_listz00_1925 = BgL_listz00_27;
				BgL_dssslz00_1926 = ((bool_t) 0);
			BgL_zc3z04anonymousza31949ze3z87_1927:
				if (NULLP(BgL_exprz00_1924))
					{	/* Ast/find_gdefs.scm 336 */
						return BgL_listz00_1925;
					}
				else
					{	/* Ast/find_gdefs.scm 336 */
						if (PAIRP(BgL_exprz00_1924))
							{	/* Ast/find_gdefs.scm 352 */
								bool_t BgL_test2196z00_3168;

								{	/* Ast/find_gdefs.scm 352 */
									obj_t BgL_tmpz00_3169;

									BgL_tmpz00_3169 = CAR(BgL_exprz00_1924);
									BgL_test2196z00_3168 = SYMBOLP(BgL_tmpz00_3169);
								}
								if (BgL_test2196z00_3168)
									{	/* Ast/find_gdefs.scm 372 */
										obj_t BgL_arg1954z00_1932;
										obj_t BgL_arg1955z00_1933;

										BgL_arg1954z00_1932 = CDR(BgL_exprz00_1924);
										BgL_arg1955z00_1933 =
											MAKE_YOUNG_PAIR(BGl_idzd2ofzd2idz00zzast_identz00(CAR
												(BgL_exprz00_1924), BgL_locz00_28), BgL_listz00_1925);
										{
											obj_t BgL_listz00_3177;
											obj_t BgL_exprz00_3176;

											BgL_exprz00_3176 = BgL_arg1954z00_1932;
											BgL_listz00_3177 = BgL_arg1955z00_1933;
											BgL_listz00_1925 = BgL_listz00_3177;
											BgL_exprz00_1924 = BgL_exprz00_3176;
											goto BgL_zc3z04anonymousza31949ze3z87_1927;
										}
									}
								else
									{	/* Ast/find_gdefs.scm 352 */
										if (BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(CAR
												(BgL_exprz00_1924)))
											{
												bool_t BgL_dssslz00_3183;
												obj_t BgL_exprz00_3181;

												BgL_exprz00_3181 = CDR(BgL_exprz00_1924);
												BgL_dssslz00_3183 = ((bool_t) 1);
												BgL_dssslz00_1926 = BgL_dssslz00_3183;
												BgL_exprz00_1924 = BgL_exprz00_3181;
												goto BgL_zc3z04anonymousza31949ze3z87_1927;
											}
										else
											{	/* Ast/find_gdefs.scm 354 */
												if (BgL_dssslz00_1926)
													{	/* Ast/find_gdefs.scm 361 */
														bool_t BgL_test2199z00_3185;

														{	/* Ast/find_gdefs.scm 361 */
															obj_t BgL_arg1970z00_1949;

															BgL_arg1970z00_1949 = CAR(BgL_exprz00_1924);
															BgL_test2199z00_3185 =
																CBOOL
																(BGl_dssslzd2defaultedzd2formalzf3zf3zztools_dssslz00
																(BgL_arg1970z00_1949));
														}
														if (BgL_test2199z00_3185)
															{	/* Ast/find_gdefs.scm 362 */
																obj_t BgL_arg1963z00_1941;
																obj_t BgL_arg1964z00_1942;

																BgL_arg1963z00_1941 = CDR(BgL_exprz00_1924);
																BgL_arg1964z00_1942 =
																	MAKE_YOUNG_PAIR
																	(BGl_idzd2ofzd2idz00zzast_identz00
																	(BGl_dssslzd2defaultzd2formalz00zztools_dssslz00
																		(CAR(BgL_exprz00_1924)), BgL_locz00_28),
																	BgL_listz00_1925);
																{
																	bool_t BgL_dssslz00_3196;
																	obj_t BgL_listz00_3195;
																	obj_t BgL_exprz00_3194;

																	BgL_exprz00_3194 = BgL_arg1963z00_1941;
																	BgL_listz00_3195 = BgL_arg1964z00_1942;
																	BgL_dssslz00_3196 = ((bool_t) 1);
																	BgL_dssslz00_1926 = BgL_dssslz00_3196;
																	BgL_listz00_1925 = BgL_listz00_3195;
																	BgL_exprz00_1924 = BgL_exprz00_3194;
																	goto BgL_zc3z04anonymousza31949ze3z87_1927;
																}
															}
														else
															{	/* Ast/find_gdefs.scm 367 */
																obj_t BgL_arg1968z00_1946;

																{	/* Ast/find_gdefs.scm 367 */
																	obj_t BgL__ortest_1109z00_1948;

																	BgL__ortest_1109z00_1948 =
																		BGl_findzd2locationzd2zztools_locationz00
																		(BgL_exprz00_1924);
																	if (CBOOL(BgL__ortest_1109z00_1948))
																		{	/* Ast/find_gdefs.scm 367 */
																			BgL_arg1968z00_1946 =
																				BgL__ortest_1109z00_1948;
																		}
																	else
																		{	/* Ast/find_gdefs.scm 367 */
																			BgL_arg1968z00_1946 = BgL_locz00_28;
																		}
																}
																return
																	BGl_userzd2errorzf2locationz20zztools_errorz00
																	(BgL_arg1968z00_1946, CNST_TABLE_REF(14),
																	BGl_string2058z00zzast_findzd2gdefszd2,
																	BgL_exprz00_1924, BNIL);
															}
													}
												else
													{	/* Ast/find_gdefs.scm 357 */
														obj_t BgL_arg1971z00_1950;

														{	/* Ast/find_gdefs.scm 357 */
															obj_t BgL__ortest_1108z00_1952;

															BgL__ortest_1108z00_1952 =
																BGl_findzd2locationzd2zztools_locationz00
																(BgL_exprz00_1924);
															if (CBOOL(BgL__ortest_1108z00_1952))
																{	/* Ast/find_gdefs.scm 357 */
																	BgL_arg1971z00_1950 =
																		BgL__ortest_1108z00_1952;
																}
															else
																{	/* Ast/find_gdefs.scm 357 */
																	BgL_arg1971z00_1950 = BgL_locz00_28;
																}
														}
														return
															BGl_userzd2errorzf2locationz20zztools_errorz00
															(BgL_arg1971z00_1950, CNST_TABLE_REF(14),
															BGl_string2059z00zzast_findzd2gdefszd2,
															BgL_exprz00_1924, BNIL);
													}
											}
									}
							}
						else
							{	/* Ast/find_gdefs.scm 338 */
								if (BgL_dssslz00_1926)
									{	/* Ast/find_gdefs.scm 340 */
										return
											BGl_userzd2errorzf2locationz20zztools_errorz00
											(BgL_locz00_28, CNST_TABLE_REF(14),
											BGl_string2060z00zzast_findzd2gdefszd2, BgL_exprz00_1924,
											BNIL);
									}
								else
									{	/* Ast/find_gdefs.scm 340 */
										if (SYMBOLP(BgL_exprz00_1924))
											{	/* Ast/find_gdefs.scm 345 */
												return
													MAKE_YOUNG_PAIR(BGl_idzd2ofzd2idz00zzast_identz00
													(BgL_exprz00_1924, BgL_locz00_28), BgL_listz00_1925);
											}
										else
											{	/* Ast/find_gdefs.scm 345 */
												return
													BGl_userzd2errorzf2locationz20zztools_errorz00
													(BgL_locz00_28, CNST_TABLE_REF(14),
													BGl_string2059z00zzast_findzd2gdefszd2,
													BgL_exprz00_1924, BNIL);
											}
									}
							}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_findzd2gdefszd2(void)
	{
		{	/* Ast/find_gdefs.scm 23 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_findzd2gdefszd2(void)
	{
		{	/* Ast/find_gdefs.scm 23 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_findzd2gdefszd2(void)
	{
		{	/* Ast/find_gdefs.scm 23 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_findzd2gdefszd2(void)
	{
		{	/* Ast/find_gdefs.scm 23 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2061z00zzast_findzd2gdefszd2));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2061z00zzast_findzd2gdefszd2));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2061z00zzast_findzd2gdefszd2));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2061z00zzast_findzd2gdefszd2));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string2061z00zzast_findzd2gdefszd2));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2061z00zzast_findzd2gdefszd2));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2061z00zzast_findzd2gdefszd2));
			BGl_modulezd2initializa7ationz75zztools_dssslz00(275867955L,
				BSTRING_TO_STRING(BGl_string2061z00zzast_findzd2gdefszd2));
			BGl_modulezd2initializa7ationz75zztools_argsz00(47102372L,
				BSTRING_TO_STRING(BGl_string2061z00zzast_findzd2gdefszd2));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2061z00zzast_findzd2gdefszd2));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string2061z00zzast_findzd2gdefszd2));
			return
				BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string2061z00zzast_findzd2gdefszd2));
		}

	}

#ifdef __cplusplus
}
#endif
