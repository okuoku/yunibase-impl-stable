/*===========================================================================*/
/*   (Ast/occur.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/occur.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_AST_OCCUR_TYPE_DEFINITIONS
#define BGL_AST_OCCUR_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_scnstz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_nodez00;
		obj_t BgL_classz00;
		obj_t BgL_locz00;
	}               *BgL_scnstz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_patchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
		struct BgL_varz00_bgl *BgL_refz00;
		long BgL_indexz00;
		obj_t BgL_patchidz00;
	}               *BgL_patchz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_pragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_formatz00;
		obj_t BgL_srfi0z00;
	}                *BgL_pragmaz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_retblockz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                  *BgL_retblockz00_bglt;

	typedef struct BgL_returnz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_retblockz00_bgl *BgL_blockz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                *BgL_returnz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;


#endif													// BGL_AST_OCCUR_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62occurzd2nodezd2inz12z70zzast_occurz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62occurzd2nodez12zd2jumpzd2exzd21305z70zzast_occurz00(obj_t,
		obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	extern obj_t BGl_returnz00zzast_nodez00;
	static obj_t BGl_requirezd2initializa7ationz75zzast_occurz00 = BUNSPEC;
	static obj_t BGl_za2globalza2z00zzast_occurz00 = BUNSPEC;
	static obj_t BGl_z62occurzd2nodez121268za2zzast_occurz00(obj_t, obj_t);
	static obj_t BGl_z62occurzd2nodez12zd2extern1285z70zzast_occurz00(obj_t,
		obj_t);
	static obj_t BGl_z62occurzd2nodez12za2zzast_occurz00(obj_t, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62occurzd2nodez12zd2switch1297z70zzast_occurz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzast_occurz00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_occurzd2nodezd2sfunz12z12zzast_occurz00(BgL_sfunz00_bglt,
		BgL_globalz00_bglt);
	extern obj_t BGl_forzd2eachzd2globalz12z12zzast_envz00(obj_t, obj_t);
	static obj_t BGl_z62occurzd2nodez12zd2conditio1293z70zzast_occurz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_occurzd2nodezd2inz12z12zzast_occurz00(BgL_nodez00_bglt,
		BgL_globalz00_bglt);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzast_occurz00(void);
	static obj_t BGl_z62occurzd2nodez12zd2var1271z70zzast_occurz00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzast_occurz00(void);
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static obj_t BGl_z62zc3z04anonymousza31321ze3ze5zzast_occurz00(obj_t, obj_t);
	static obj_t BGl_z62occurzd2nodez12zd2letzd2fun1299za2zzast_occurz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62occurzd2nodezd2sfunz12z70zzast_occurz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zzast_occurz00(void);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t BGl_z62occurzd2nodez12zd2app1279z70zzast_occurz00(obj_t, obj_t);
	static obj_t BGl_z62occurzd2nodez12zd2cast1289z70zzast_occurz00(obj_t, obj_t);
	static obj_t BGl_z62occurzd2nodez12zd2sequence1275z70zzast_occurz00(obj_t,
		obj_t);
	static obj_t BGl_z62occurzd2nodez12zd2letzd2var1301za2zzast_occurz00(obj_t,
		obj_t);
	static obj_t BGl_z62occurzd2nodez12zd2retblock1307z70zzast_occurz00(obj_t,
		obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	static obj_t BGl_z62occurzd2nodez12zd2pragma1287z70zzast_occurz00(obj_t,
		obj_t);
	static obj_t BGl_z62occurzd2nodez12zd2setq1291z70zzast_occurz00(obj_t, obj_t);
	static obj_t BGl_z62occurzd2nodez12zd2sync1277z70zzast_occurz00(obj_t, obj_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t BGl_z62occurzd2nodez12zd2fail1295z70zzast_occurz00(obj_t, obj_t);
	extern obj_t BGl_pragmaz00zzast_nodez00;
	static obj_t BGl_z62occurzd2nodez12zd2boxzd2ref1313za2zzast_occurz00(obj_t,
		obj_t);
	static obj_t BGl_z62occurzd2varzb0zzast_occurz00(obj_t, obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzast_occurz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t BGl_z62occurzd2nodez12zd2funcall1283z70zzast_occurz00(obj_t,
		obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	BGL_EXPORTED_DECL obj_t BGl_occurzd2varzd2zzast_occurz00(obj_t);
	static obj_t BGl_cnstzd2initzd2zzast_occurz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_occurz00(void);
	extern obj_t BGl_scnstz00zzast_varz00;
	static obj_t BGl_importedzd2moduleszd2initz00zzast_occurz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzast_occurz00(void);
	extern obj_t BGl_retblockz00zzast_nodez00;
	static obj_t BGl_z62occurzd2nodez12zd2return1309z70zzast_occurz00(obj_t,
		obj_t);
	static bool_t BGl_occurzd2nodeza2z12z62zzast_occurz00(obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	static obj_t BGl_z62occurzd2nodez12zd2patch1273z70zzast_occurz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_occurzd2nodez12zc0zzast_occurz00(BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t BGl_z62occurzd2nodez12zd2setzd2exzd2i1303z70zzast_occurz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t
		BGl_findzd2superzd2classzd2methodzd2zz__objectz00(BgL_objectz00_bglt, obj_t,
		obj_t);
	static obj_t BGl_z62occurzd2nodez12zd2boxzd2setz121315zb0zzast_occurz00(obj_t,
		obj_t);
	static obj_t BGl_z62occurzd2nodez12zd2makezd2box1311za2zzast_occurz00(obj_t,
		obj_t);
	static obj_t BGl_z62occurzd2nodez12zd2appzd2ly1281za2zzast_occurz00(obj_t,
		obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_patchz00zzast_nodez00;
	static obj_t __cnst[3];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_occurzd2nodezd2sfunz12zd2envzc0zzast_occurz00,
		BgL_bgl_za762occurza7d2nodeza71865za7,
		BGl_z62occurzd2nodezd2sfunz12z70zzast_occurz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1837z00zzast_occurz00,
		BgL_bgl_string1837za700za7za7a1866za7, "occur-node!1268", 15);
	      DEFINE_STRING(BGl_string1839z00zzast_occurz00,
		BgL_bgl_string1839za700za7za7a1867za7, "occur-node!", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1835z00zzast_occurz00,
		BgL_bgl_za762za7c3za704anonymo1868za7,
		BGl_z62zc3z04anonymousza31321ze3ze5zzast_occurz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1836z00zzast_occurz00,
		BgL_bgl_za762occurza7d2nodeza71869za7,
		BGl_z62occurzd2nodez121268za2zzast_occurz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1838z00zzast_occurz00,
		BgL_bgl_za762occurza7d2nodeza71870za7,
		BGl_z62occurzd2nodez12zd2var1271z70zzast_occurz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1840z00zzast_occurz00,
		BgL_bgl_za762occurza7d2nodeza71871za7,
		BGl_z62occurzd2nodez12zd2patch1273z70zzast_occurz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1841z00zzast_occurz00,
		BgL_bgl_za762occurza7d2nodeza71872za7,
		BGl_z62occurzd2nodez12zd2sequence1275z70zzast_occurz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1842z00zzast_occurz00,
		BgL_bgl_za762occurza7d2nodeza71873za7,
		BGl_z62occurzd2nodez12zd2sync1277z70zzast_occurz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1843z00zzast_occurz00,
		BgL_bgl_za762occurza7d2nodeza71874za7,
		BGl_z62occurzd2nodez12zd2app1279z70zzast_occurz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1844z00zzast_occurz00,
		BgL_bgl_za762occurza7d2nodeza71875za7,
		BGl_z62occurzd2nodez12zd2appzd2ly1281za2zzast_occurz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1845z00zzast_occurz00,
		BgL_bgl_za762occurza7d2nodeza71876za7,
		BGl_z62occurzd2nodez12zd2funcall1283z70zzast_occurz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1846z00zzast_occurz00,
		BgL_bgl_za762occurza7d2nodeza71877za7,
		BGl_z62occurzd2nodez12zd2extern1285z70zzast_occurz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1847z00zzast_occurz00,
		BgL_bgl_za762occurza7d2nodeza71878za7,
		BGl_z62occurzd2nodez12zd2pragma1287z70zzast_occurz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1848z00zzast_occurz00,
		BgL_bgl_za762occurza7d2nodeza71879za7,
		BGl_z62occurzd2nodez12zd2cast1289z70zzast_occurz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1849z00zzast_occurz00,
		BgL_bgl_za762occurza7d2nodeza71880za7,
		BGl_z62occurzd2nodez12zd2setq1291z70zzast_occurz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1850z00zzast_occurz00,
		BgL_bgl_za762occurza7d2nodeza71881za7,
		BGl_z62occurzd2nodez12zd2conditio1293z70zzast_occurz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1851z00zzast_occurz00,
		BgL_bgl_za762occurza7d2nodeza71882za7,
		BGl_z62occurzd2nodez12zd2fail1295z70zzast_occurz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1852z00zzast_occurz00,
		BgL_bgl_za762occurza7d2nodeza71883za7,
		BGl_z62occurzd2nodez12zd2switch1297z70zzast_occurz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1853z00zzast_occurz00,
		BgL_bgl_za762occurza7d2nodeza71884za7,
		BGl_z62occurzd2nodez12zd2letzd2fun1299za2zzast_occurz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1854z00zzast_occurz00,
		BgL_bgl_za762occurza7d2nodeza71885za7,
		BGl_z62occurzd2nodez12zd2letzd2var1301za2zzast_occurz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1855z00zzast_occurz00,
		BgL_bgl_za762occurza7d2nodeza71886za7,
		BGl_z62occurzd2nodez12zd2setzd2exzd2i1303z70zzast_occurz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1862z00zzast_occurz00,
		BgL_bgl_string1862za700za7za7a1887za7, "ast_occur", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1856z00zzast_occurz00,
		BgL_bgl_za762occurza7d2nodeza71888za7,
		BGl_z62occurzd2nodez12zd2jumpzd2exzd21305z70zzast_occurz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1863z00zzast_occurz00,
		BgL_bgl_string1863za700za7za7a1889za7, "read write done ", 16);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1857z00zzast_occurz00,
		BgL_bgl_za762occurza7d2nodeza71890za7,
		BGl_z62occurzd2nodez12zd2retblock1307z70zzast_occurz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1858z00zzast_occurz00,
		BgL_bgl_za762occurza7d2nodeza71891za7,
		BGl_z62occurzd2nodez12zd2return1309z70zzast_occurz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1859z00zzast_occurz00,
		BgL_bgl_za762occurza7d2nodeza71892za7,
		BGl_z62occurzd2nodez12zd2makezd2box1311za2zzast_occurz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1860z00zzast_occurz00,
		BgL_bgl_za762occurza7d2nodeza71893za7,
		BGl_z62occurzd2nodez12zd2boxzd2ref1313za2zzast_occurz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1861z00zzast_occurz00,
		BgL_bgl_za762occurza7d2nodeza71894za7,
		BGl_z62occurzd2nodez12zd2boxzd2setz121315zb0zzast_occurz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_occurzd2nodezd2inz12zd2envzc0zzast_occurz00,
		BgL_bgl_za762occurza7d2nodeza71895za7,
		BGl_z62occurzd2nodezd2inz12z70zzast_occurz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_occurzd2nodez12zd2envz12zzast_occurz00,
		BgL_bgl_za762occurza7d2nodeza71896za7,
		BGl_z62occurzd2nodez12za2zzast_occurz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_occurzd2varzd2envz00zzast_occurz00,
		BgL_bgl_za762occurza7d2varza7b1897za7, BGl_z62occurzd2varzb0zzast_occurz00,
		0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzast_occurz00));
		     ADD_ROOT((void *) (&BGl_za2globalza2z00zzast_occurz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzast_occurz00(long
		BgL_checksumz00_2496, char *BgL_fromz00_2497)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_occurz00))
				{
					BGl_requirezd2initializa7ationz75zzast_occurz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_occurz00();
					BGl_libraryzd2moduleszd2initz00zzast_occurz00();
					BGl_cnstzd2initzd2zzast_occurz00();
					BGl_importedzd2moduleszd2initz00zzast_occurz00();
					BGl_genericzd2initzd2zzast_occurz00();
					BGl_methodzd2initzd2zzast_occurz00();
					return BGl_toplevelzd2initzd2zzast_occurz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_occurz00(void)
	{
		{	/* Ast/occur.scm 17 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "ast_occur");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_occur");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "ast_occur");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "ast_occur");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"ast_occur");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "ast_occur");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "ast_occur");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"ast_occur");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_occur");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_occurz00(void)
	{
		{	/* Ast/occur.scm 17 */
			{	/* Ast/occur.scm 17 */
				obj_t BgL_cportz00_2330;

				{	/* Ast/occur.scm 17 */
					obj_t BgL_stringz00_2337;

					BgL_stringz00_2337 = BGl_string1863z00zzast_occurz00;
					{	/* Ast/occur.scm 17 */
						obj_t BgL_startz00_2338;

						BgL_startz00_2338 = BINT(0L);
						{	/* Ast/occur.scm 17 */
							obj_t BgL_endz00_2339;

							BgL_endz00_2339 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2337)));
							{	/* Ast/occur.scm 17 */

								BgL_cportz00_2330 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2337, BgL_startz00_2338, BgL_endz00_2339);
				}}}}
				{
					long BgL_iz00_2331;

					BgL_iz00_2331 = 2L;
				BgL_loopz00_2332:
					if ((BgL_iz00_2331 == -1L))
						{	/* Ast/occur.scm 17 */
							return BUNSPEC;
						}
					else
						{	/* Ast/occur.scm 17 */
							{	/* Ast/occur.scm 17 */
								obj_t BgL_arg1864z00_2333;

								{	/* Ast/occur.scm 17 */

									{	/* Ast/occur.scm 17 */
										obj_t BgL_locationz00_2335;

										BgL_locationz00_2335 = BBOOL(((bool_t) 0));
										{	/* Ast/occur.scm 17 */

											BgL_arg1864z00_2333 =
												BGl_readz00zz__readerz00(BgL_cportz00_2330,
												BgL_locationz00_2335);
										}
									}
								}
								{	/* Ast/occur.scm 17 */
									int BgL_tmpz00_2526;

									BgL_tmpz00_2526 = (int) (BgL_iz00_2331);
									CNST_TABLE_SET(BgL_tmpz00_2526, BgL_arg1864z00_2333);
							}}
							{	/* Ast/occur.scm 17 */
								int BgL_auxz00_2336;

								BgL_auxz00_2336 = (int) ((BgL_iz00_2331 - 1L));
								{
									long BgL_iz00_2531;

									BgL_iz00_2531 = (long) (BgL_auxz00_2336);
									BgL_iz00_2331 = BgL_iz00_2531;
									goto BgL_loopz00_2332;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_occurz00(void)
	{
		{	/* Ast/occur.scm 17 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzast_occurz00(void)
	{
		{	/* Ast/occur.scm 17 */
			BGl_za2globalza2z00zzast_occurz00 = BUNSPEC;
			return BUNSPEC;
		}

	}



/* occur-var */
	BGL_EXPORTED_DEF obj_t BGl_occurzd2varzd2zzast_occurz00(obj_t
		BgL_globalsz00_3)
	{
		{	/* Ast/occur.scm 33 */
			BGl_forzd2eachzd2globalz12z12zzast_envz00(BGl_proc1835z00zzast_occurz00,
				BNIL);
			{
				obj_t BgL_l1240z00_1388;

				BgL_l1240z00_1388 = BgL_globalsz00_3;
			BgL_zc3z04anonymousza31322ze3z87_1389:
				if (PAIRP(BgL_l1240z00_1388))
					{	/* Ast/occur.scm 39 */
						{	/* Ast/occur.scm 40 */
							obj_t BgL_globalz00_1391;

							BgL_globalz00_1391 = CAR(BgL_l1240z00_1388);
							{	/* Ast/occur.scm 40 */
								BgL_valuez00_bglt BgL_arg1325z00_1392;

								BgL_arg1325z00_1392 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_globalz00_1391))))->
									BgL_valuez00);
								BGl_occurzd2nodezd2sfunz12z12zzast_occurz00(((BgL_sfunz00_bglt)
										BgL_arg1325z00_1392),
									((BgL_globalz00_bglt) BgL_globalz00_1391));
							}
						}
						{
							obj_t BgL_l1240z00_2544;

							BgL_l1240z00_2544 = CDR(BgL_l1240z00_1388);
							BgL_l1240z00_1388 = BgL_l1240z00_2544;
							goto BgL_zc3z04anonymousza31322ze3z87_1389;
						}
					}
				else
					{	/* Ast/occur.scm 39 */
						((bool_t) 1);
					}
			}
			return BgL_globalsz00_3;
		}

	}



/* &occur-var */
	obj_t BGl_z62occurzd2varzb0zzast_occurz00(obj_t BgL_envz00_2246,
		obj_t BgL_globalsz00_2247)
	{
		{	/* Ast/occur.scm 33 */
			return BGl_occurzd2varzd2zzast_occurz00(BgL_globalsz00_2247);
		}

	}



/* &<@anonymous:1321> */
	obj_t BGl_z62zc3z04anonymousza31321ze3ze5zzast_occurz00(obj_t BgL_envz00_2248,
		obj_t BgL_globalz00_2249)
	{
		{	/* Ast/occur.scm 35 */
			((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt)
								((BgL_globalz00_bglt) BgL_globalz00_2249))))->
					BgL_occurrencez00) = ((long) 0L), BUNSPEC);
			return ((((BgL_variablez00_bglt)
						COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
									BgL_globalz00_2249))))->BgL_occurrencewz00) =
				((long) 0L), BUNSPEC);
		}

	}



/* occur-node-sfun! */
	BGL_EXPORTED_DEF obj_t
		BGl_occurzd2nodezd2sfunz12z12zzast_occurz00(BgL_sfunz00_bglt BgL_fz00_4,
		BgL_globalz00_bglt BgL_globalz00_5)
	{
		{	/* Ast/occur.scm 47 */
			{	/* Ast/occur.scm 49 */
				obj_t BgL_g1244z00_1396;

				BgL_g1244z00_1396 =
					(((BgL_sfunz00_bglt) COBJECT(BgL_fz00_4))->BgL_argsz00);
				{
					obj_t BgL_l1242z00_1398;

					BgL_l1242z00_1398 = BgL_g1244z00_1396;
				BgL_zc3z04anonymousza31327ze3z87_1399:
					if (PAIRP(BgL_l1242z00_1398))
						{	/* Ast/occur.scm 49 */
							{	/* Ast/occur.scm 49 */
								obj_t BgL_az00_1401;

								BgL_az00_1401 = CAR(BgL_l1242z00_1398);
								((((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_az00_1401))))->
										BgL_occurrencez00) = ((long) 0L), BUNSPEC);
							}
							{
								obj_t BgL_l1242z00_2560;

								BgL_l1242z00_2560 = CDR(BgL_l1242z00_1398);
								BgL_l1242z00_1398 = BgL_l1242z00_2560;
								goto BgL_zc3z04anonymousza31327ze3z87_1399;
							}
						}
					else
						{	/* Ast/occur.scm 49 */
							((bool_t) 1);
						}
				}
			}
			{	/* Ast/occur.scm 50 */
				obj_t BgL_arg1331z00_1404;

				BgL_arg1331z00_1404 =
					(((BgL_sfunz00_bglt) COBJECT(BgL_fz00_4))->BgL_bodyz00);
				BGl_za2globalza2z00zzast_occurz00 = ((obj_t) BgL_globalz00_5);
				return
					BGl_occurzd2nodez12zc0zzast_occurz00(
					((BgL_nodez00_bglt) BgL_arg1331z00_1404));
			}
		}

	}



/* &occur-node-sfun! */
	obj_t BGl_z62occurzd2nodezd2sfunz12z70zzast_occurz00(obj_t BgL_envz00_2250,
		obj_t BgL_fz00_2251, obj_t BgL_globalz00_2252)
	{
		{	/* Ast/occur.scm 47 */
			return
				BGl_occurzd2nodezd2sfunz12z12zzast_occurz00(
				((BgL_sfunz00_bglt) BgL_fz00_2251),
				((BgL_globalz00_bglt) BgL_globalz00_2252));
		}

	}



/* occur-node-in! */
	BGL_EXPORTED_DEF obj_t
		BGl_occurzd2nodezd2inz12z12zzast_occurz00(BgL_nodez00_bglt BgL_nodez00_6,
		BgL_globalz00_bglt BgL_globalz00_7)
	{
		{	/* Ast/occur.scm 55 */
			BGl_za2globalza2z00zzast_occurz00 = ((obj_t) BgL_globalz00_7);
			return BGl_occurzd2nodez12zc0zzast_occurz00(BgL_nodez00_6);
		}

	}



/* &occur-node-in! */
	obj_t BGl_z62occurzd2nodezd2inz12z70zzast_occurz00(obj_t BgL_envz00_2253,
		obj_t BgL_nodez00_2254, obj_t BgL_globalz00_2255)
	{
		{	/* Ast/occur.scm 55 */
			return
				BGl_occurzd2nodezd2inz12z12zzast_occurz00(
				((BgL_nodez00_bglt) BgL_nodez00_2254),
				((BgL_globalz00_bglt) BgL_globalz00_2255));
		}

	}



/* occur-node*! */
	bool_t BGl_occurzd2nodeza2z12z62zzast_occurz00(obj_t BgL_nodeza2za2_32)
	{
		{	/* Ast/occur.scm 294 */
			{
				obj_t BgL_l1266z00_1406;

				BgL_l1266z00_1406 = BgL_nodeza2za2_32;
			BgL_zc3z04anonymousza31332ze3z87_1407:
				if (PAIRP(BgL_l1266z00_1406))
					{	/* Ast/occur.scm 295 */
						{	/* Ast/occur.scm 295 */
							obj_t BgL_arg1335z00_1409;

							BgL_arg1335z00_1409 = CAR(BgL_l1266z00_1406);
							BGl_occurzd2nodez12zc0zzast_occurz00(
								((BgL_nodez00_bglt) BgL_arg1335z00_1409));
						}
						{
							obj_t BgL_l1266z00_2579;

							BgL_l1266z00_2579 = CDR(BgL_l1266z00_1406);
							BgL_l1266z00_1406 = BgL_l1266z00_2579;
							goto BgL_zc3z04anonymousza31332ze3z87_1407;
						}
					}
				else
					{	/* Ast/occur.scm 295 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_occurz00(void)
	{
		{	/* Ast/occur.scm 17 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_occurz00(void)
	{
		{	/* Ast/occur.scm 17 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_occurz00,
				BGl_proc1836z00zzast_occurz00, BGl_nodez00zzast_nodez00,
				BGl_string1837z00zzast_occurz00);
		}

	}



/* &occur-node!1268 */
	obj_t BGl_z62occurzd2nodez121268za2zzast_occurz00(obj_t BgL_envz00_2257,
		obj_t BgL_nodez00_2258)
	{
		{	/* Ast/occur.scm 67 */
			return CNST_TABLE_REF(0);
		}

	}



/* occur-node! */
	BGL_EXPORTED_DEF obj_t BGl_occurzd2nodez12zc0zzast_occurz00(BgL_nodez00_bglt
		BgL_nodez00_8)
	{
		{	/* Ast/occur.scm 67 */
			{	/* Ast/occur.scm 67 */
				obj_t BgL_method1269z00_1416;

				{	/* Ast/occur.scm 67 */
					obj_t BgL_res1825z00_1869;

					{	/* Ast/occur.scm 67 */
						long BgL_objzd2classzd2numz00_1840;

						BgL_objzd2classzd2numz00_1840 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_8));
						{	/* Ast/occur.scm 67 */
							obj_t BgL_arg1811z00_1841;

							BgL_arg1811z00_1841 =
								PROCEDURE_REF(BGl_occurzd2nodez12zd2envz12zzast_occurz00,
								(int) (1L));
							{	/* Ast/occur.scm 67 */
								int BgL_offsetz00_1844;

								BgL_offsetz00_1844 = (int) (BgL_objzd2classzd2numz00_1840);
								{	/* Ast/occur.scm 67 */
									long BgL_offsetz00_1845;

									BgL_offsetz00_1845 =
										((long) (BgL_offsetz00_1844) - OBJECT_TYPE);
									{	/* Ast/occur.scm 67 */
										long BgL_modz00_1846;

										BgL_modz00_1846 =
											(BgL_offsetz00_1845 >> (int) ((long) ((int) (4L))));
										{	/* Ast/occur.scm 67 */
											long BgL_restz00_1848;

											BgL_restz00_1848 =
												(BgL_offsetz00_1845 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/occur.scm 67 */

												{	/* Ast/occur.scm 67 */
													obj_t BgL_bucketz00_1850;

													BgL_bucketz00_1850 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_1841), BgL_modz00_1846);
													BgL_res1825z00_1869 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_1850), BgL_restz00_1848);
					}}}}}}}}
					BgL_method1269z00_1416 = BgL_res1825z00_1869;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1269z00_1416, ((obj_t) BgL_nodez00_8));
			}
		}

	}



/* &occur-node! */
	obj_t BGl_z62occurzd2nodez12za2zzast_occurz00(obj_t BgL_envz00_2259,
		obj_t BgL_nodez00_2260)
	{
		{	/* Ast/occur.scm 67 */
			return
				BGl_occurzd2nodez12zc0zzast_occurz00(
				((BgL_nodez00_bglt) BgL_nodez00_2260));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_occurz00(void)
	{
		{	/* Ast/occur.scm 17 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_occurz00, BGl_varz00zzast_nodez00,
				BGl_proc1838z00zzast_occurz00, BGl_string1839z00zzast_occurz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_occurz00, BGl_patchz00zzast_nodez00,
				BGl_proc1840z00zzast_occurz00, BGl_string1839z00zzast_occurz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_occurz00,
				BGl_sequencez00zzast_nodez00, BGl_proc1841z00zzast_occurz00,
				BGl_string1839z00zzast_occurz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_occurz00, BGl_syncz00zzast_nodez00,
				BGl_proc1842z00zzast_occurz00, BGl_string1839z00zzast_occurz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_occurz00, BGl_appz00zzast_nodez00,
				BGl_proc1843z00zzast_occurz00, BGl_string1839z00zzast_occurz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_occurz00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1844z00zzast_occurz00,
				BGl_string1839z00zzast_occurz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_occurz00,
				BGl_funcallz00zzast_nodez00, BGl_proc1845z00zzast_occurz00,
				BGl_string1839z00zzast_occurz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_occurz00, BGl_externz00zzast_nodez00,
				BGl_proc1846z00zzast_occurz00, BGl_string1839z00zzast_occurz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_occurz00, BGl_pragmaz00zzast_nodez00,
				BGl_proc1847z00zzast_occurz00, BGl_string1839z00zzast_occurz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_occurz00, BGl_castz00zzast_nodez00,
				BGl_proc1848z00zzast_occurz00, BGl_string1839z00zzast_occurz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_occurz00, BGl_setqz00zzast_nodez00,
				BGl_proc1849z00zzast_occurz00, BGl_string1839z00zzast_occurz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_occurz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1850z00zzast_occurz00,
				BGl_string1839z00zzast_occurz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_occurz00, BGl_failz00zzast_nodez00,
				BGl_proc1851z00zzast_occurz00, BGl_string1839z00zzast_occurz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_occurz00, BGl_switchz00zzast_nodez00,
				BGl_proc1852z00zzast_occurz00, BGl_string1839z00zzast_occurz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_occurz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1853z00zzast_occurz00,
				BGl_string1839z00zzast_occurz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_occurz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1854z00zzast_occurz00,
				BGl_string1839z00zzast_occurz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_occurz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc1855z00zzast_occurz00,
				BGl_string1839z00zzast_occurz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_occurz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc1856z00zzast_occurz00,
				BGl_string1839z00zzast_occurz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_occurz00,
				BGl_retblockz00zzast_nodez00, BGl_proc1857z00zzast_occurz00,
				BGl_string1839z00zzast_occurz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_occurz00, BGl_returnz00zzast_nodez00,
				BGl_proc1858z00zzast_occurz00, BGl_string1839z00zzast_occurz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_occurz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1859z00zzast_occurz00,
				BGl_string1839z00zzast_occurz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_occurz00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1860z00zzast_occurz00,
				BGl_string1839z00zzast_occurz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_occurz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1861z00zzast_occurz00,
				BGl_string1839z00zzast_occurz00);
		}

	}



/* &occur-node!-box-set!1315 */
	obj_t BGl_z62occurzd2nodez12zd2boxzd2setz121315zb0zzast_occurz00(obj_t
		BgL_envz00_2284, obj_t BgL_nodez00_2285)
	{
		{	/* Ast/occur.scm 286 */
			{	/* Ast/occur.scm 288 */
				BgL_varz00_bglt BgL_arg1678z00_2343;

				BgL_arg1678z00_2343 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2285)))->BgL_varz00);
				BGl_occurzd2nodez12zc0zzast_occurz00(
					((BgL_nodez00_bglt) BgL_arg1678z00_2343));
			}
			return
				BGl_occurzd2nodez12zc0zzast_occurz00(
				(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2285)))->BgL_valuez00));
		}

	}



/* &occur-node!-box-ref1313 */
	obj_t BGl_z62occurzd2nodez12zd2boxzd2ref1313za2zzast_occurz00(obj_t
		BgL_envz00_2286, obj_t BgL_nodez00_2287)
	{
		{	/* Ast/occur.scm 280 */
			{	/* Ast/occur.scm 281 */
				BgL_varz00_bglt BgL_arg1675z00_2345;

				BgL_arg1675z00_2345 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_nodez00_2287)))->BgL_varz00);
				return
					BGl_occurzd2nodez12zc0zzast_occurz00(
					((BgL_nodez00_bglt) BgL_arg1675z00_2345));
			}
		}

	}



/* &occur-node!-make-box1311 */
	obj_t BGl_z62occurzd2nodez12zd2makezd2box1311za2zzast_occurz00(obj_t
		BgL_envz00_2288, obj_t BgL_nodez00_2289)
	{
		{	/* Ast/occur.scm 274 */
			return
				BGl_occurzd2nodez12zc0zzast_occurz00(
				(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_2289)))->BgL_valuez00));
		}

	}



/* &occur-node!-return1309 */
	obj_t BGl_z62occurzd2nodez12zd2return1309z70zzast_occurz00(obj_t
		BgL_envz00_2290, obj_t BgL_nodez00_2291)
	{
		{	/* Ast/occur.scm 267 */
			return
				BGl_occurzd2nodez12zc0zzast_occurz00(
				(((BgL_returnz00_bglt) COBJECT(
							((BgL_returnz00_bglt) BgL_nodez00_2291)))->BgL_valuez00));
		}

	}



/* &occur-node!-retblock1307 */
	obj_t BGl_z62occurzd2nodez12zd2retblock1307z70zzast_occurz00(obj_t
		BgL_envz00_2292, obj_t BgL_nodez00_2293)
	{
		{	/* Ast/occur.scm 260 */
			return
				BGl_occurzd2nodez12zc0zzast_occurz00(
				(((BgL_retblockz00_bglt) COBJECT(
							((BgL_retblockz00_bglt) BgL_nodez00_2293)))->BgL_bodyz00));
		}

	}



/* &occur-node!-jump-ex-1305 */
	obj_t BGl_z62occurzd2nodez12zd2jumpzd2exzd21305z70zzast_occurz00(obj_t
		BgL_envz00_2294, obj_t BgL_nodez00_2295)
	{
		{	/* Ast/occur.scm 252 */
			BGl_occurzd2nodez12zc0zzast_occurz00(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2295)))->BgL_exitz00));
			return
				BGl_occurzd2nodez12zc0zzast_occurz00(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2295)))->BgL_valuez00));
		}

	}



/* &occur-node!-set-ex-i1303 */
	obj_t BGl_z62occurzd2nodez12zd2setzd2exzd2i1303z70zzast_occurz00(obj_t
		BgL_envz00_2296, obj_t BgL_nodez00_2297)
	{
		{	/* Ast/occur.scm 240 */
			{	/* Ast/occur.scm 242 */
				BgL_localz00_bglt BgL_i1120z00_2351;

				BgL_i1120z00_2351 =
					((BgL_localz00_bglt)
					(((BgL_varz00_bglt) COBJECT(
								(((BgL_setzd2exzd2itz00_bglt) COBJECT(
											((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2297)))->
									BgL_varz00)))->BgL_variablez00));
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_i1120z00_2351)))->BgL_occurrencez00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_i1120z00_2351)))->BgL_occurrencewz00) =
					((long) 0L), BUNSPEC);
				if (((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_i1120z00_2351)))->BgL_accessz00) == CNST_TABLE_REF(1)))
					{	/* Ast/occur.scm 245 */
						((((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_i1120z00_2351)))->
								BgL_accessz00) = ((obj_t) CNST_TABLE_REF(2)), BUNSPEC);
					}
				else
					{	/* Ast/occur.scm 245 */
						BFALSE;
					}
			}
			BGl_occurzd2nodez12zc0zzast_occurz00(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2297)))->BgL_onexitz00));
			return
				BGl_occurzd2nodez12zc0zzast_occurz00(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2297)))->BgL_bodyz00));
		}

	}



/* &occur-node!-let-var1301 */
	obj_t BGl_z62occurzd2nodez12zd2letzd2var1301za2zzast_occurz00(obj_t
		BgL_envz00_2298, obj_t BgL_nodez00_2299)
	{
		{	/* Ast/occur.scm 224 */
			{	/* Ast/occur.scm 226 */
				obj_t BgL_g1262z00_2353;

				BgL_g1262z00_2353 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2299)))->BgL_bindingsz00);
				{
					obj_t BgL_l1260z00_2355;

					BgL_l1260z00_2355 = BgL_g1262z00_2353;
				BgL_zc3z04anonymousza31610ze3z87_2354:
					if (PAIRP(BgL_l1260z00_2355))
						{	/* Ast/occur.scm 226 */
							{	/* Ast/occur.scm 227 */
								obj_t BgL_bindingz00_2356;

								BgL_bindingz00_2356 = CAR(BgL_l1260z00_2355);
								{	/* Ast/occur.scm 227 */
									BgL_localz00_bglt BgL_i1118z00_2357;

									BgL_i1118z00_2357 =
										((BgL_localz00_bglt) CAR(((obj_t) BgL_bindingz00_2356)));
									((((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_i1118z00_2357)))->
											BgL_occurrencez00) = ((long) 0L), BUNSPEC);
									((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
														BgL_i1118z00_2357)))->BgL_occurrencewz00) =
										((long) 0L), BUNSPEC);
									((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
														BgL_i1118z00_2357)))->BgL_accessz00) =
										((obj_t) CNST_TABLE_REF(2)), BUNSPEC);
							}}
							{
								obj_t BgL_l1260z00_2701;

								BgL_l1260z00_2701 = CDR(BgL_l1260z00_2355);
								BgL_l1260z00_2355 = BgL_l1260z00_2701;
								goto BgL_zc3z04anonymousza31610ze3z87_2354;
							}
						}
					else
						{	/* Ast/occur.scm 226 */
							((bool_t) 1);
						}
				}
			}
			{	/* Ast/occur.scm 232 */
				obj_t BgL_g1265z00_2358;

				BgL_g1265z00_2358 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2299)))->BgL_bindingsz00);
				{
					obj_t BgL_l1263z00_2360;

					BgL_l1263z00_2360 = BgL_g1265z00_2358;
				BgL_zc3z04anonymousza31614ze3z87_2359:
					if (PAIRP(BgL_l1263z00_2360))
						{	/* Ast/occur.scm 232 */
							{	/* Ast/occur.scm 233 */
								obj_t BgL_bindingz00_2361;

								BgL_bindingz00_2361 = CAR(BgL_l1263z00_2360);
								{	/* Ast/occur.scm 233 */
									obj_t BgL_arg1616z00_2362;

									BgL_arg1616z00_2362 = CDR(((obj_t) BgL_bindingz00_2361));
									BGl_occurzd2nodez12zc0zzast_occurz00(
										((BgL_nodez00_bglt) BgL_arg1616z00_2362));
								}
							}
							{
								obj_t BgL_l1263z00_2712;

								BgL_l1263z00_2712 = CDR(BgL_l1263z00_2360);
								BgL_l1263z00_2360 = BgL_l1263z00_2712;
								goto BgL_zc3z04anonymousza31614ze3z87_2359;
							}
						}
					else
						{	/* Ast/occur.scm 232 */
							((bool_t) 1);
						}
				}
			}
			return
				BGl_occurzd2nodez12zc0zzast_occurz00(
				(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2299)))->BgL_bodyz00));
		}

	}



/* &occur-node!-let-fun1299 */
	obj_t BGl_z62occurzd2nodez12zd2letzd2fun1299za2zzast_occurz00(obj_t
		BgL_envz00_2300, obj_t BgL_nodez00_2301)
	{
		{	/* Ast/occur.scm 202 */
			{	/* Ast/occur.scm 205 */
				obj_t BgL_g1256z00_2364;

				BgL_g1256z00_2364 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2301)))->BgL_localsz00);
				{
					obj_t BgL_l1254z00_2366;

					BgL_l1254z00_2366 = BgL_g1256z00_2364;
				BgL_zc3z04anonymousza31577ze3z87_2365:
					if (PAIRP(BgL_l1254z00_2366))
						{	/* Ast/occur.scm 205 */
							{	/* Ast/occur.scm 210 */
								obj_t BgL_localz00_2367;

								BgL_localz00_2367 = CAR(BgL_l1254z00_2366);
								((((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_localz00_2367))))->
										BgL_occurrencez00) = ((long) 0L), BUNSPEC);
								((((BgL_variablez00_bglt)
											COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
														BgL_localz00_2367))))->BgL_occurrencewz00) =
									((long) 0L), BUNSPEC);
								if (((((BgL_variablez00_bglt)
												COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
															BgL_localz00_2367))))->BgL_accessz00) ==
										CNST_TABLE_REF(1)))
									{	/* Ast/occur.scm 210 */
										((((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_localz00_bglt) BgL_localz00_2367))))->
												BgL_accessz00) = ((obj_t) CNST_TABLE_REF(2)), BUNSPEC);
									}
								else
									{	/* Ast/occur.scm 210 */
										BFALSE;
									}
								{	/* Ast/occur.scm 211 */
									obj_t BgL_g1253z00_2368;

									BgL_g1253z00_2368 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt)
													(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt) BgL_localz00_2367))))->
														BgL_valuez00))))->BgL_argsz00);
									{
										obj_t BgL_l1251z00_2370;

										BgL_l1251z00_2370 = BgL_g1253z00_2368;
									BgL_zc3z04anonymousza31590ze3z87_2369:
										if (PAIRP(BgL_l1251z00_2370))
											{	/* Ast/occur.scm 212 */
												{	/* Ast/occur.scm 211 */
													obj_t BgL_az00_2371;

													BgL_az00_2371 = CAR(BgL_l1251z00_2370);
													((((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt) BgL_az00_2371))))->
															BgL_occurrencez00) = ((long) 0L), BUNSPEC);
												}
												{
													obj_t BgL_l1251z00_2749;

													BgL_l1251z00_2749 = CDR(BgL_l1251z00_2370);
													BgL_l1251z00_2370 = BgL_l1251z00_2749;
													goto BgL_zc3z04anonymousza31590ze3z87_2369;
												}
											}
										else
											{	/* Ast/occur.scm 212 */
												((bool_t) 1);
											}
									}
								}
							}
							{
								obj_t BgL_l1254z00_2751;

								BgL_l1254z00_2751 = CDR(BgL_l1254z00_2366);
								BgL_l1254z00_2366 = BgL_l1254z00_2751;
								goto BgL_zc3z04anonymousza31577ze3z87_2365;
							}
						}
					else
						{	/* Ast/occur.scm 205 */
							((bool_t) 1);
						}
				}
			}
			{	/* Ast/occur.scm 214 */
				obj_t BgL_g1259z00_2372;

				BgL_g1259z00_2372 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2301)))->BgL_localsz00);
				{
					obj_t BgL_l1257z00_2374;

					BgL_l1257z00_2374 = BgL_g1259z00_2372;
				BgL_zc3z04anonymousza31596ze3z87_2373:
					if (PAIRP(BgL_l1257z00_2374))
						{	/* Ast/occur.scm 214 */
							{	/* Ast/occur.scm 215 */
								obj_t BgL_localz00_2375;

								BgL_localz00_2375 = CAR(BgL_l1257z00_2374);
								{	/* Ast/occur.scm 215 */
									long BgL_oldz00_2376;

									BgL_oldz00_2376 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_localz00_2375))))->
										BgL_occurrencez00);
									{	/* Ast/occur.scm 216 */
										obj_t BgL_arg1602z00_2377;

										BgL_arg1602z00_2377 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt)
														(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt) BgL_localz00_2375))))->
															BgL_valuez00))))->BgL_bodyz00);
										BGl_occurzd2nodez12zc0zzast_occurz00(((BgL_nodez00_bglt)
												BgL_arg1602z00_2377));
									}
									((((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt) BgL_localz00_2375))))->
											BgL_occurrencez00) = ((long) BgL_oldz00_2376), BUNSPEC);
							}}
							{
								obj_t BgL_l1257z00_2771;

								BgL_l1257z00_2771 = CDR(BgL_l1257z00_2374);
								BgL_l1257z00_2374 = BgL_l1257z00_2771;
								goto BgL_zc3z04anonymousza31596ze3z87_2373;
							}
						}
					else
						{	/* Ast/occur.scm 214 */
							((bool_t) 1);
						}
				}
			}
			return
				BGl_occurzd2nodez12zc0zzast_occurz00(
				(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2301)))->BgL_bodyz00));
		}

	}



/* &occur-node!-switch1297 */
	obj_t BGl_z62occurzd2nodez12zd2switch1297z70zzast_occurz00(obj_t
		BgL_envz00_2302, obj_t BgL_nodez00_2303)
	{
		{	/* Ast/occur.scm 192 */
			BGl_occurzd2nodez12zc0zzast_occurz00(
				(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_2303)))->BgL_testz00));
			{	/* Ast/occur.scm 195 */
				obj_t BgL_g1250z00_2379;

				BgL_g1250z00_2379 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_2303)))->BgL_clausesz00);
				{
					obj_t BgL_l1248z00_2381;

					{	/* Ast/occur.scm 195 */
						bool_t BgL_tmpz00_2781;

						BgL_l1248z00_2381 = BgL_g1250z00_2379;
					BgL_zc3z04anonymousza31572ze3z87_2380:
						if (PAIRP(BgL_l1248z00_2381))
							{	/* Ast/occur.scm 195 */
								{	/* Ast/occur.scm 196 */
									obj_t BgL_clausez00_2382;

									BgL_clausez00_2382 = CAR(BgL_l1248z00_2381);
									{	/* Ast/occur.scm 196 */
										obj_t BgL_arg1575z00_2383;

										BgL_arg1575z00_2383 = CDR(((obj_t) BgL_clausez00_2382));
										BGl_occurzd2nodez12zc0zzast_occurz00(
											((BgL_nodez00_bglt) BgL_arg1575z00_2383));
									}
								}
								{
									obj_t BgL_l1248z00_2789;

									BgL_l1248z00_2789 = CDR(BgL_l1248z00_2381);
									BgL_l1248z00_2381 = BgL_l1248z00_2789;
									goto BgL_zc3z04anonymousza31572ze3z87_2380;
								}
							}
						else
							{	/* Ast/occur.scm 195 */
								BgL_tmpz00_2781 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_2781);
					}
				}
			}
		}

	}



/* &occur-node!-fail1295 */
	obj_t BGl_z62occurzd2nodez12zd2fail1295z70zzast_occurz00(obj_t
		BgL_envz00_2304, obj_t BgL_nodez00_2305)
	{
		{	/* Ast/occur.scm 183 */
			BGl_occurzd2nodez12zc0zzast_occurz00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2305)))->BgL_procz00));
			BGl_occurzd2nodez12zc0zzast_occurz00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2305)))->BgL_msgz00));
			return
				BGl_occurzd2nodez12zc0zzast_occurz00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2305)))->BgL_objz00));
		}

	}



/* &occur-node!-conditio1293 */
	obj_t BGl_z62occurzd2nodez12zd2conditio1293z70zzast_occurz00(obj_t
		BgL_envz00_2306, obj_t BgL_nodez00_2307)
	{
		{	/* Ast/occur.scm 174 */
			BGl_occurzd2nodez12zc0zzast_occurz00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2307)))->BgL_testz00));
			BGl_occurzd2nodez12zc0zzast_occurz00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2307)))->BgL_truez00));
			return
				BGl_occurzd2nodez12zc0zzast_occurz00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2307)))->BgL_falsez00));
		}

	}



/* &occur-node!-setq1291 */
	obj_t BGl_z62occurzd2nodez12zd2setq1291z70zzast_occurz00(obj_t
		BgL_envz00_2308, obj_t BgL_nodez00_2309)
	{
		{	/* Ast/occur.scm 163 */
			{	/* Ast/occur.scm 165 */
				BgL_variablez00_bglt BgL_i1111z00_2387;

				BgL_i1111z00_2387 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_setqz00_bglt) COBJECT(
										((BgL_setqz00_bglt) BgL_nodez00_2309)))->BgL_varz00)))->
					BgL_variablez00);
				if (((((BgL_variablez00_bglt) COBJECT(BgL_i1111z00_2387))->
							BgL_accessz00) == CNST_TABLE_REF(2)))
					{	/* Ast/occur.scm 166 */
						((((BgL_variablez00_bglt) COBJECT(BgL_i1111z00_2387))->
								BgL_accessz00) = ((obj_t) CNST_TABLE_REF(1)), BUNSPEC);
					}
				else
					{	/* Ast/occur.scm 166 */
						BFALSE;
					}
				((((BgL_variablez00_bglt) COBJECT(BgL_i1111z00_2387))->
						BgL_occurrencewz00) =
					((long) (1L +
							(((BgL_variablez00_bglt) COBJECT(BgL_i1111z00_2387))->
								BgL_occurrencewz00))), BUNSPEC);
			}
			{	/* Ast/occur.scm 168 */
				BgL_varz00_bglt BgL_arg1544z00_2388;

				BgL_arg1544z00_2388 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_2309)))->BgL_varz00);
				BGl_occurzd2nodez12zc0zzast_occurz00(
					((BgL_nodez00_bglt) BgL_arg1544z00_2388));
			}
			return
				BGl_occurzd2nodez12zc0zzast_occurz00(
				(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_2309)))->BgL_valuez00));
		}

	}



/* &occur-node!-cast1289 */
	obj_t BGl_z62occurzd2nodez12zd2cast1289z70zzast_occurz00(obj_t
		BgL_envz00_2310, obj_t BgL_nodez00_2311)
	{
		{	/* Ast/occur.scm 157 */
			return
				BGl_occurzd2nodez12zc0zzast_occurz00(
				(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_2311)))->BgL_argz00));
		}

	}



/* &occur-node!-pragma1287 */
	obj_t BGl_z62occurzd2nodez12zd2pragma1287z70zzast_occurz00(obj_t
		BgL_envz00_2312, obj_t BgL_nodez00_2313)
	{
		{	/* Ast/occur.scm 143 */
			{

				if (CBOOL(
						(((BgL_nodezf2effectzf2_bglt) COBJECT(
									((BgL_nodezf2effectzf2_bglt)
										((BgL_pragmaz00_bglt) BgL_nodez00_2313))))->
							BgL_sidezd2effectzd2)))
					{	/* Ast/occur.scm 146 */
						obj_t BgL_g1247z00_2393;

						BgL_g1247z00_2393 =
							(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt)
										((BgL_pragmaz00_bglt) BgL_nodez00_2313))))->BgL_exprza2za2);
						{
							obj_t BgL_l1245z00_2395;

							BgL_l1245z00_2395 = BgL_g1247z00_2393;
						BgL_zc3z04anonymousza31450ze3z87_2394:
							if (PAIRP(BgL_l1245z00_2395))
								{	/* Ast/occur.scm 151 */
									{	/* Ast/occur.scm 147 */
										obj_t BgL_nz00_2396;

										BgL_nz00_2396 = CAR(BgL_l1245z00_2395);
										{	/* Ast/occur.scm 147 */
											bool_t BgL_test1914z00_2843;

											{	/* Ast/occur.scm 147 */
												bool_t BgL_test1915z00_2844;

												{	/* Ast/occur.scm 147 */
													obj_t BgL_classz00_2397;

													BgL_classz00_2397 = BGl_varz00zzast_nodez00;
													if (BGL_OBJECTP(BgL_nz00_2396))
														{	/* Ast/occur.scm 147 */
															BgL_objectz00_bglt BgL_arg1807z00_2398;

															BgL_arg1807z00_2398 =
																(BgL_objectz00_bglt) (BgL_nz00_2396);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Ast/occur.scm 147 */
																	long BgL_idxz00_2399;

																	BgL_idxz00_2399 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_2398);
																	BgL_test1915z00_2844 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_2399 + 2L)) ==
																		BgL_classz00_2397);
																}
															else
																{	/* Ast/occur.scm 147 */
																	bool_t BgL_res1829z00_2402;

																	{	/* Ast/occur.scm 147 */
																		obj_t BgL_oclassz00_2403;

																		{	/* Ast/occur.scm 147 */
																			obj_t BgL_arg1815z00_2404;
																			long BgL_arg1816z00_2405;

																			BgL_arg1815z00_2404 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Ast/occur.scm 147 */
																				long BgL_arg1817z00_2406;

																				BgL_arg1817z00_2406 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_2398);
																				BgL_arg1816z00_2405 =
																					(BgL_arg1817z00_2406 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_2403 =
																				VECTOR_REF(BgL_arg1815z00_2404,
																				BgL_arg1816z00_2405);
																		}
																		{	/* Ast/occur.scm 147 */
																			bool_t BgL__ortest_1115z00_2407;

																			BgL__ortest_1115z00_2407 =
																				(BgL_classz00_2397 ==
																				BgL_oclassz00_2403);
																			if (BgL__ortest_1115z00_2407)
																				{	/* Ast/occur.scm 147 */
																					BgL_res1829z00_2402 =
																						BgL__ortest_1115z00_2407;
																				}
																			else
																				{	/* Ast/occur.scm 147 */
																					long BgL_odepthz00_2408;

																					{	/* Ast/occur.scm 147 */
																						obj_t BgL_arg1804z00_2409;

																						BgL_arg1804z00_2409 =
																							(BgL_oclassz00_2403);
																						BgL_odepthz00_2408 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_2409);
																					}
																					if ((2L < BgL_odepthz00_2408))
																						{	/* Ast/occur.scm 147 */
																							obj_t BgL_arg1802z00_2410;

																							{	/* Ast/occur.scm 147 */
																								obj_t BgL_arg1803z00_2411;

																								BgL_arg1803z00_2411 =
																									(BgL_oclassz00_2403);
																								BgL_arg1802z00_2410 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_2411, 2L);
																							}
																							BgL_res1829z00_2402 =
																								(BgL_arg1802z00_2410 ==
																								BgL_classz00_2397);
																						}
																					else
																						{	/* Ast/occur.scm 147 */
																							BgL_res1829z00_2402 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test1915z00_2844 = BgL_res1829z00_2402;
																}
														}
													else
														{	/* Ast/occur.scm 147 */
															BgL_test1915z00_2844 = ((bool_t) 0);
														}
												}
												if (BgL_test1915z00_2844)
													{	/* Ast/occur.scm 147 */
														BgL_variablez00_bglt BgL_arg1489z00_2412;

														BgL_arg1489z00_2412 =
															(((BgL_varz00_bglt) COBJECT(
																	((BgL_varz00_bglt) BgL_nz00_2396)))->
															BgL_variablez00);
														{	/* Ast/occur.scm 147 */
															obj_t BgL_classz00_2413;

															BgL_classz00_2413 = BGl_localz00zzast_varz00;
															{	/* Ast/occur.scm 147 */
																BgL_objectz00_bglt BgL_arg1807z00_2414;

																{	/* Ast/occur.scm 147 */
																	obj_t BgL_tmpz00_2869;

																	BgL_tmpz00_2869 =
																		((obj_t)
																		((BgL_objectz00_bglt) BgL_arg1489z00_2412));
																	BgL_arg1807z00_2414 =
																		(BgL_objectz00_bglt) (BgL_tmpz00_2869);
																}
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Ast/occur.scm 147 */
																		long BgL_idxz00_2415;

																		BgL_idxz00_2415 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_2414);
																		BgL_test1914z00_2843 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_2415 + 2L)) ==
																			BgL_classz00_2413);
																	}
																else
																	{	/* Ast/occur.scm 147 */
																		bool_t BgL_res1830z00_2418;

																		{	/* Ast/occur.scm 147 */
																			obj_t BgL_oclassz00_2419;

																			{	/* Ast/occur.scm 147 */
																				obj_t BgL_arg1815z00_2420;
																				long BgL_arg1816z00_2421;

																				BgL_arg1815z00_2420 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Ast/occur.scm 147 */
																					long BgL_arg1817z00_2422;

																					BgL_arg1817z00_2422 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_2414);
																					BgL_arg1816z00_2421 =
																						(BgL_arg1817z00_2422 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_2419 =
																					VECTOR_REF(BgL_arg1815z00_2420,
																					BgL_arg1816z00_2421);
																			}
																			{	/* Ast/occur.scm 147 */
																				bool_t BgL__ortest_1115z00_2423;

																				BgL__ortest_1115z00_2423 =
																					(BgL_classz00_2413 ==
																					BgL_oclassz00_2419);
																				if (BgL__ortest_1115z00_2423)
																					{	/* Ast/occur.scm 147 */
																						BgL_res1830z00_2418 =
																							BgL__ortest_1115z00_2423;
																					}
																				else
																					{	/* Ast/occur.scm 147 */
																						long BgL_odepthz00_2424;

																						{	/* Ast/occur.scm 147 */
																							obj_t BgL_arg1804z00_2425;

																							BgL_arg1804z00_2425 =
																								(BgL_oclassz00_2419);
																							BgL_odepthz00_2424 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_2425);
																						}
																						if ((2L < BgL_odepthz00_2424))
																							{	/* Ast/occur.scm 147 */
																								obj_t BgL_arg1802z00_2426;

																								{	/* Ast/occur.scm 147 */
																									obj_t BgL_arg1803z00_2427;

																									BgL_arg1803z00_2427 =
																										(BgL_oclassz00_2419);
																									BgL_arg1802z00_2426 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_2427, 2L);
																								}
																								BgL_res1830z00_2418 =
																									(BgL_arg1802z00_2426 ==
																									BgL_classz00_2413);
																							}
																						else
																							{	/* Ast/occur.scm 147 */
																								BgL_res1830z00_2418 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test1914z00_2843 = BgL_res1830z00_2418;
																	}
															}
														}
													}
												else
													{	/* Ast/occur.scm 147 */
														BgL_test1914z00_2843 = ((bool_t) 0);
													}
											}
											if (BgL_test1914z00_2843)
												{	/* Ast/occur.scm 148 */
													BgL_localz00_bglt BgL_i1109z00_2428;

													BgL_i1109z00_2428 =
														((BgL_localz00_bglt)
														(((BgL_varz00_bglt) COBJECT(
																	((BgL_varz00_bglt) BgL_nz00_2396)))->
															BgL_variablez00));
													if (((((BgL_variablez00_bglt)
																	COBJECT(((BgL_variablez00_bglt)
																			BgL_i1109z00_2428)))->BgL_accessz00) ==
															CNST_TABLE_REF(2)))
														{	/* Ast/occur.scm 149 */
															((((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				BgL_i1109z00_2428)))->BgL_accessz00) =
																((obj_t) CNST_TABLE_REF(1)), BUNSPEC);
														}
													else
														{	/* Ast/occur.scm 149 */
															BFALSE;
														}
												}
											else
												{	/* Ast/occur.scm 147 */
													BFALSE;
												}
										}
									}
									{
										obj_t BgL_l1245z00_2903;

										BgL_l1245z00_2903 = CDR(BgL_l1245z00_2395);
										BgL_l1245z00_2395 = BgL_l1245z00_2903;
										goto BgL_zc3z04anonymousza31450ze3z87_2394;
									}
								}
							else
								{	/* Ast/occur.scm 151 */
									((bool_t) 1);
								}
						}
					}
				else
					{	/* Ast/occur.scm 145 */
						((bool_t) 0);
					}
				{	/* Ast/occur.scm 143 */
					obj_t BgL_nextzd2method1286zd2_2392;

					BgL_nextzd2method1286zd2_2392 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_pragmaz00_bglt) BgL_nodez00_2313)),
						BGl_occurzd2nodez12zd2envz12zzast_occurz00,
						BGl_pragmaz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1286zd2_2392,
						((obj_t) ((BgL_pragmaz00_bglt) BgL_nodez00_2313)));
				}
			}
		}

	}



/* &occur-node!-extern1285 */
	obj_t BGl_z62occurzd2nodez12zd2extern1285z70zzast_occurz00(obj_t
		BgL_envz00_2314, obj_t BgL_nodez00_2315)
	{
		{	/* Ast/occur.scm 135 */
			{	/* Ast/occur.scm 137 */
				obj_t BgL_nodesz00_2430;

				BgL_nodesz00_2430 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_2315)))->BgL_exprza2za2);
				return
					BBOOL(BGl_occurzd2nodeza2z12z62zzast_occurz00(BgL_nodesz00_2430));
			}
		}

	}



/* &occur-node!-funcall1283 */
	obj_t BGl_z62occurzd2nodez12zd2funcall1283z70zzast_occurz00(obj_t
		BgL_envz00_2316, obj_t BgL_nodez00_2317)
	{
		{	/* Ast/occur.scm 127 */
			BGl_occurzd2nodez12zc0zzast_occurz00(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_2317)))->BgL_funz00));
			{	/* Ast/occur.scm 130 */
				obj_t BgL_arg1448z00_2432;

				BgL_arg1448z00_2432 =
					(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_2317)))->BgL_argsz00);
				return
					BBOOL(BGl_occurzd2nodeza2z12z62zzast_occurz00(BgL_arg1448z00_2432));
			}
		}

	}



/* &occur-node!-app-ly1281 */
	obj_t BGl_z62occurzd2nodez12zd2appzd2ly1281za2zzast_occurz00(obj_t
		BgL_envz00_2318, obj_t BgL_nodez00_2319)
	{
		{	/* Ast/occur.scm 119 */
			BGl_occurzd2nodez12zc0zzast_occurz00(
				(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_2319)))->BgL_funz00));
			return
				BGl_occurzd2nodez12zc0zzast_occurz00(
				(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_2319)))->BgL_argz00));
		}

	}



/* &occur-node!-app1279 */
	obj_t BGl_z62occurzd2nodez12zd2app1279z70zzast_occurz00(obj_t BgL_envz00_2320,
		obj_t BgL_nodez00_2321)
	{
		{	/* Ast/occur.scm 111 */
			{	/* Ast/occur.scm 113 */
				BgL_varz00_bglt BgL_arg1410z00_2435;

				BgL_arg1410z00_2435 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_2321)))->BgL_funz00);
				BGl_occurzd2nodez12zc0zzast_occurz00(
					((BgL_nodez00_bglt) BgL_arg1410z00_2435));
			}
			{	/* Ast/occur.scm 114 */
				obj_t BgL_arg1421z00_2436;

				BgL_arg1421z00_2436 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_2321)))->BgL_argsz00);
				return
					BBOOL(BGl_occurzd2nodeza2z12z62zzast_occurz00(BgL_arg1421z00_2436));
			}
		}

	}



/* &occur-node!-sync1277 */
	obj_t BGl_z62occurzd2nodez12zd2sync1277z70zzast_occurz00(obj_t
		BgL_envz00_2322, obj_t BgL_nodez00_2323)
	{
		{	/* Ast/occur.scm 103 */
			BGl_occurzd2nodez12zc0zzast_occurz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2323)))->BgL_mutexz00));
			BGl_occurzd2nodez12zc0zzast_occurz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2323)))->BgL_prelockz00));
			return
				BGl_occurzd2nodez12zc0zzast_occurz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2323)))->BgL_bodyz00));
		}

	}



/* &occur-node!-sequence1275 */
	obj_t BGl_z62occurzd2nodez12zd2sequence1275z70zzast_occurz00(obj_t
		BgL_envz00_2324, obj_t BgL_nodez00_2325)
	{
		{	/* Ast/occur.scm 97 */
			{	/* Ast/occur.scm 98 */
				obj_t BgL_arg1378z00_2439;

				BgL_arg1378z00_2439 =
					(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_2325)))->BgL_nodesz00);
				return
					BBOOL(BGl_occurzd2nodeza2z12z62zzast_occurz00(BgL_arg1378z00_2439));
			}
		}

	}



/* &occur-node!-patch1273 */
	obj_t BGl_z62occurzd2nodez12zd2patch1273z70zzast_occurz00(obj_t
		BgL_envz00_2326, obj_t BgL_nodez00_2327)
	{
		{	/* Ast/occur.scm 84 */
			{	/* Ast/occur.scm 86 */
				bool_t BgL_test1924z00_2952;

				{	/* Ast/occur.scm 86 */
					obj_t BgL_arg1376z00_2441;

					BgL_arg1376z00_2441 =
						(((BgL_atomz00_bglt) COBJECT(
								((BgL_atomz00_bglt)
									((BgL_patchz00_bglt) BgL_nodez00_2327))))->BgL_valuez00);
					{	/* Ast/occur.scm 86 */
						obj_t BgL_classz00_2442;

						BgL_classz00_2442 = BGl_varz00zzast_nodez00;
						if (BGL_OBJECTP(BgL_arg1376z00_2441))
							{	/* Ast/occur.scm 86 */
								BgL_objectz00_bglt BgL_arg1807z00_2443;

								BgL_arg1807z00_2443 =
									(BgL_objectz00_bglt) (BgL_arg1376z00_2441);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Ast/occur.scm 86 */
										long BgL_idxz00_2444;

										BgL_idxz00_2444 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2443);
										BgL_test1924z00_2952 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2444 + 2L)) == BgL_classz00_2442);
									}
								else
									{	/* Ast/occur.scm 86 */
										bool_t BgL_res1828z00_2447;

										{	/* Ast/occur.scm 86 */
											obj_t BgL_oclassz00_2448;

											{	/* Ast/occur.scm 86 */
												obj_t BgL_arg1815z00_2449;
												long BgL_arg1816z00_2450;

												BgL_arg1815z00_2449 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Ast/occur.scm 86 */
													long BgL_arg1817z00_2451;

													BgL_arg1817z00_2451 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2443);
													BgL_arg1816z00_2450 =
														(BgL_arg1817z00_2451 - OBJECT_TYPE);
												}
												BgL_oclassz00_2448 =
													VECTOR_REF(BgL_arg1815z00_2449, BgL_arg1816z00_2450);
											}
											{	/* Ast/occur.scm 86 */
												bool_t BgL__ortest_1115z00_2452;

												BgL__ortest_1115z00_2452 =
													(BgL_classz00_2442 == BgL_oclassz00_2448);
												if (BgL__ortest_1115z00_2452)
													{	/* Ast/occur.scm 86 */
														BgL_res1828z00_2447 = BgL__ortest_1115z00_2452;
													}
												else
													{	/* Ast/occur.scm 86 */
														long BgL_odepthz00_2453;

														{	/* Ast/occur.scm 86 */
															obj_t BgL_arg1804z00_2454;

															BgL_arg1804z00_2454 = (BgL_oclassz00_2448);
															BgL_odepthz00_2453 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2454);
														}
														if ((2L < BgL_odepthz00_2453))
															{	/* Ast/occur.scm 86 */
																obj_t BgL_arg1802z00_2455;

																{	/* Ast/occur.scm 86 */
																	obj_t BgL_arg1803z00_2456;

																	BgL_arg1803z00_2456 = (BgL_oclassz00_2448);
																	BgL_arg1802z00_2455 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2456,
																		2L);
																}
																BgL_res1828z00_2447 =
																	(BgL_arg1802z00_2455 == BgL_classz00_2442);
															}
														else
															{	/* Ast/occur.scm 86 */
																BgL_res1828z00_2447 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1924z00_2952 = BgL_res1828z00_2447;
									}
							}
						else
							{	/* Ast/occur.scm 86 */
								BgL_test1924z00_2952 = ((bool_t) 0);
							}
					}
				}
				if (BgL_test1924z00_2952)
					{	/* Ast/occur.scm 87 */
						BgL_variablez00_bglt BgL_vz00_2457;

						BgL_vz00_2457 =
							(((BgL_varz00_bglt) COBJECT(
									((BgL_varz00_bglt)
										(((BgL_atomz00_bglt) COBJECT(
													((BgL_atomz00_bglt)
														((BgL_patchz00_bglt) BgL_nodez00_2327))))->
											BgL_valuez00))))->BgL_variablez00);
						((((BgL_variablez00_bglt) COBJECT(BgL_vz00_2457))->
								BgL_occurrencewz00) =
							((long) ((((BgL_variablez00_bglt) COBJECT(BgL_vz00_2457))->
										BgL_occurrencewz00) + 1L)), BUNSPEC);
						((((BgL_variablez00_bglt) COBJECT(BgL_vz00_2457))->
								BgL_occurrencez00) =
							((long) ((((BgL_variablez00_bglt) COBJECT(BgL_vz00_2457))->
										BgL_occurrencez00) + 1L)), BUNSPEC);
						{	/* Ast/occur.scm 90 */
							obj_t BgL_vz00_2458;

							BgL_vz00_2458 = CNST_TABLE_REF(1);
							((((BgL_variablez00_bglt) COBJECT(BgL_vz00_2457))->
									BgL_accessz00) = ((obj_t) BgL_vz00_2458), BUNSPEC);
					}}
				else
					{	/* Ast/occur.scm 91 */
						obj_t BgL_arg1375z00_2459;

						BgL_arg1375z00_2459 =
							(((BgL_atomz00_bglt) COBJECT(
									((BgL_atomz00_bglt)
										((BgL_patchz00_bglt) BgL_nodez00_2327))))->BgL_valuez00);
						BGl_occurzd2nodez12zc0zzast_occurz00(
							((BgL_nodez00_bglt) BgL_arg1375z00_2459));
					}
			}
			{	/* Ast/occur.scm 92 */
				BgL_varz00_bglt BgL_arg1377z00_2460;

				BgL_arg1377z00_2460 =
					(((BgL_patchz00_bglt) COBJECT(
							((BgL_patchz00_bglt) BgL_nodez00_2327)))->BgL_refz00);
				return
					BGl_occurzd2nodez12zc0zzast_occurz00(
					((BgL_nodez00_bglt) BgL_arg1377z00_2460));
			}
		}

	}



/* &occur-node!-var1271 */
	obj_t BGl_z62occurzd2nodez12zd2var1271z70zzast_occurz00(obj_t BgL_envz00_2328,
		obj_t BgL_nodez00_2329)
	{
		{	/* Ast/occur.scm 73 */
			{	/* Ast/occur.scm 74 */
				BgL_variablez00_bglt BgL_vz00_2462;

				BgL_vz00_2462 =
					(((BgL_varz00_bglt) COBJECT(
							((BgL_varz00_bglt) BgL_nodez00_2329)))->BgL_variablez00);
				{	/* Ast/occur.scm 75 */
					BgL_valuez00_bglt BgL_valuez00_2463;

					BgL_valuez00_2463 =
						(((BgL_variablez00_bglt) COBJECT(BgL_vz00_2462))->BgL_valuez00);
					{	/* Ast/occur.scm 76 */
						bool_t BgL_test1929z00_3003;

						{	/* Ast/occur.scm 76 */
							bool_t BgL_test1930z00_3004;

							{	/* Ast/occur.scm 76 */
								obj_t BgL_classz00_2464;

								BgL_classz00_2464 = BGl_scnstz00zzast_varz00;
								{	/* Ast/occur.scm 76 */
									BgL_objectz00_bglt BgL_arg1807z00_2465;

									{	/* Ast/occur.scm 76 */
										obj_t BgL_tmpz00_3005;

										BgL_tmpz00_3005 =
											((obj_t) ((BgL_objectz00_bglt) BgL_valuez00_2463));
										BgL_arg1807z00_2465 =
											(BgL_objectz00_bglt) (BgL_tmpz00_3005);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Ast/occur.scm 76 */
											long BgL_idxz00_2466;

											BgL_idxz00_2466 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2465);
											BgL_test1930z00_3004 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2466 + 2L)) == BgL_classz00_2464);
										}
									else
										{	/* Ast/occur.scm 76 */
											bool_t BgL_res1826z00_2469;

											{	/* Ast/occur.scm 76 */
												obj_t BgL_oclassz00_2470;

												{	/* Ast/occur.scm 76 */
													obj_t BgL_arg1815z00_2471;
													long BgL_arg1816z00_2472;

													BgL_arg1815z00_2471 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Ast/occur.scm 76 */
														long BgL_arg1817z00_2473;

														BgL_arg1817z00_2473 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2465);
														BgL_arg1816z00_2472 =
															(BgL_arg1817z00_2473 - OBJECT_TYPE);
													}
													BgL_oclassz00_2470 =
														VECTOR_REF(BgL_arg1815z00_2471,
														BgL_arg1816z00_2472);
												}
												{	/* Ast/occur.scm 76 */
													bool_t BgL__ortest_1115z00_2474;

													BgL__ortest_1115z00_2474 =
														(BgL_classz00_2464 == BgL_oclassz00_2470);
													if (BgL__ortest_1115z00_2474)
														{	/* Ast/occur.scm 76 */
															BgL_res1826z00_2469 = BgL__ortest_1115z00_2474;
														}
													else
														{	/* Ast/occur.scm 76 */
															long BgL_odepthz00_2475;

															{	/* Ast/occur.scm 76 */
																obj_t BgL_arg1804z00_2476;

																BgL_arg1804z00_2476 = (BgL_oclassz00_2470);
																BgL_odepthz00_2475 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2476);
															}
															if ((2L < BgL_odepthz00_2475))
																{	/* Ast/occur.scm 76 */
																	obj_t BgL_arg1802z00_2477;

																	{	/* Ast/occur.scm 76 */
																		obj_t BgL_arg1803z00_2478;

																		BgL_arg1803z00_2478 = (BgL_oclassz00_2470);
																		BgL_arg1802z00_2477 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2478, 2L);
																	}
																	BgL_res1826z00_2469 =
																		(BgL_arg1802z00_2477 == BgL_classz00_2464);
																}
															else
																{	/* Ast/occur.scm 76 */
																	BgL_res1826z00_2469 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test1930z00_3004 = BgL_res1826z00_2469;
										}
								}
							}
							if (BgL_test1930z00_3004)
								{	/* Ast/occur.scm 76 */
									obj_t BgL_arg1348z00_2479;

									BgL_arg1348z00_2479 =
										(((BgL_scnstz00_bglt) COBJECT(
												((BgL_scnstz00_bglt) BgL_valuez00_2463)))->BgL_nodez00);
									{	/* Ast/occur.scm 76 */
										obj_t BgL_classz00_2480;

										BgL_classz00_2480 = BGl_nodez00zzast_nodez00;
										if (BGL_OBJECTP(BgL_arg1348z00_2479))
											{	/* Ast/occur.scm 76 */
												BgL_objectz00_bglt BgL_arg1807z00_2481;

												BgL_arg1807z00_2481 =
													(BgL_objectz00_bglt) (BgL_arg1348z00_2479);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Ast/occur.scm 76 */
														long BgL_idxz00_2482;

														BgL_idxz00_2482 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2481);
														BgL_test1929z00_3003 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2482 + 1L)) == BgL_classz00_2480);
													}
												else
													{	/* Ast/occur.scm 76 */
														bool_t BgL_res1827z00_2485;

														{	/* Ast/occur.scm 76 */
															obj_t BgL_oclassz00_2486;

															{	/* Ast/occur.scm 76 */
																obj_t BgL_arg1815z00_2487;
																long BgL_arg1816z00_2488;

																BgL_arg1815z00_2487 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Ast/occur.scm 76 */
																	long BgL_arg1817z00_2489;

																	BgL_arg1817z00_2489 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2481);
																	BgL_arg1816z00_2488 =
																		(BgL_arg1817z00_2489 - OBJECT_TYPE);
																}
																BgL_oclassz00_2486 =
																	VECTOR_REF(BgL_arg1815z00_2487,
																	BgL_arg1816z00_2488);
															}
															{	/* Ast/occur.scm 76 */
																bool_t BgL__ortest_1115z00_2490;

																BgL__ortest_1115z00_2490 =
																	(BgL_classz00_2480 == BgL_oclassz00_2486);
																if (BgL__ortest_1115z00_2490)
																	{	/* Ast/occur.scm 76 */
																		BgL_res1827z00_2485 =
																			BgL__ortest_1115z00_2490;
																	}
																else
																	{	/* Ast/occur.scm 76 */
																		long BgL_odepthz00_2491;

																		{	/* Ast/occur.scm 76 */
																			obj_t BgL_arg1804z00_2492;

																			BgL_arg1804z00_2492 =
																				(BgL_oclassz00_2486);
																			BgL_odepthz00_2491 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2492);
																		}
																		if ((1L < BgL_odepthz00_2491))
																			{	/* Ast/occur.scm 76 */
																				obj_t BgL_arg1802z00_2493;

																				{	/* Ast/occur.scm 76 */
																					obj_t BgL_arg1803z00_2494;

																					BgL_arg1803z00_2494 =
																						(BgL_oclassz00_2486);
																					BgL_arg1802z00_2493 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2494, 1L);
																				}
																				BgL_res1827z00_2485 =
																					(BgL_arg1802z00_2493 ==
																					BgL_classz00_2480);
																			}
																		else
																			{	/* Ast/occur.scm 76 */
																				BgL_res1827z00_2485 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test1929z00_3003 = BgL_res1827z00_2485;
													}
											}
										else
											{	/* Ast/occur.scm 76 */
												BgL_test1929z00_3003 = ((bool_t) 0);
											}
									}
								}
							else
								{	/* Ast/occur.scm 76 */
									BgL_test1929z00_3003 = ((bool_t) 0);
								}
						}
						if (BgL_test1929z00_3003)
							{	/* Ast/occur.scm 77 */
								obj_t BgL_arg1346z00_2495;

								BgL_arg1346z00_2495 =
									(((BgL_scnstz00_bglt) COBJECT(
											((BgL_scnstz00_bglt) BgL_valuez00_2463)))->BgL_nodez00);
								BGl_occurzd2nodez12zc0zzast_occurz00(
									((BgL_nodez00_bglt) BgL_arg1346z00_2495));
							}
						else
							{	/* Ast/occur.scm 76 */
								BFALSE;
							}
					}
				}
				if ((((obj_t) BgL_vz00_2462) == BGl_za2globalza2z00zzast_occurz00))
					{	/* Ast/occur.scm 78 */
						return BFALSE;
					}
				else
					{	/* Ast/occur.scm 78 */
						return
							((((BgL_variablez00_bglt) COBJECT(BgL_vz00_2462))->
								BgL_occurrencez00) =
							((long) ((((BgL_variablez00_bglt) COBJECT(BgL_vz00_2462))->
										BgL_occurrencez00) + 1L)), BUNSPEC);
		}}}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_occurz00(void)
	{
		{	/* Ast/occur.scm 17 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1862z00zzast_occurz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1862z00zzast_occurz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1862z00zzast_occurz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1862z00zzast_occurz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1862z00zzast_occurz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1862z00zzast_occurz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1862z00zzast_occurz00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string1862z00zzast_occurz00));
			return
				BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string1862z00zzast_occurz00));
		}

	}

#ifdef __cplusplus
}
#endif
