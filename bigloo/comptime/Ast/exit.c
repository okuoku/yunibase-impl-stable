/*===========================================================================*/
/*   (Ast/exit.scm)                                                          */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/exit.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_AST_EXIT_TYPE_DEFINITIONS
#define BGL_AST_EXIT_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_sexitz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_handlerz00;
		bool_t BgL_detachedzf3zf3;
	}               *BgL_sexitz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_pragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_formatz00;
		obj_t BgL_srfi0z00;
	}                *BgL_pragmaz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;


#endif													// BGL_AST_EXIT_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_normaliza7ezd2prognz75zztools_prognz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzast_exitz00 = BUNSPEC;
	extern obj_t BGl_findzd2locationzf2locz20zztools_locationz00(obj_t, obj_t);
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzast_exitz00(void);
	extern obj_t BGl_sexitz00zzast_varz00;
	static obj_t BGl_objectzd2initzd2zzast_exitz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	extern obj_t BGl_za2exitza2z00zztype_cachez00;
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzast_exitz00(void);
	static BgL_letzd2funzd2_bglt
		BGl_z62setzd2exitzd2ze3nodez81zzast_exitz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	extern BgL_localz00_bglt BGl_makezd2localzd2sexitz00zzast_localz00(obj_t,
		BgL_typez00_bglt, BgL_sexitz00_bglt);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	extern obj_t BGl_pragmaz00zzast_nodez00;
	extern BgL_typez00_bglt
		BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	extern obj_t BGl_makezd2anonymouszd2namez00zzast_sexpz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_letzd2funzd2_bglt
		BGl_setzd2exitzd2ze3nodeze3zzast_exitz00(obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzast_exitz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_EXPORTED_DECL BgL_jumpzd2exzd2itz00_bglt
		BGl_jumpzd2exitzd2ze3nodeze3zzast_exitz00(obj_t, obj_t, obj_t, obj_t);
	static BgL_jumpzd2exzd2itz00_bglt
		BGl_z62jumpzd2exitzd2ze3nodez81zzast_exitz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzast_exitz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_exitz00(void);
	extern BgL_nodez00_bglt BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzast_exitz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzast_exitz00(void);
	extern BgL_nodez00_bglt BGl_sexpzd2ze3nodez31zzast_sexpz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2unspecza2z00zztype_cachez00;
	extern obj_t BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00(obj_t);
	static BgL_localz00_bglt BGl_makezd2localzd2exitze70ze7zzast_exitz00(obj_t,
		obj_t);
	static obj_t __cnst[5];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_setzd2exitzd2ze3nodezd2envz31zzast_exitz00,
		BgL_bgl_za762setza7d2exitza7d21662za7,
		BGl_z62setzd2exitzd2ze3nodez81zzast_exitz00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_jumpzd2exitzd2ze3nodezd2envz31zzast_exitz00,
		BgL_bgl_za762jumpza7d2exitza7d1663za7,
		BGl_z62jumpzd2exitzd2ze3nodez81zzast_exitz00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string1655z00zzast_exitz00,
		BgL_bgl_string1655za700za7za7a1664za7, "Illegal `set-exit' form", 23);
	      DEFINE_STRING(BGl_string1656z00zzast_exitz00,
		BgL_bgl_string1656za700za7za7a1665za7, "exit", 4);
	      DEFINE_STRING(BGl_string1657z00zzast_exitz00,
		BgL_bgl_string1657za700za7za7a1666za7, "BGL_EXIT_VALUE()", 16);
	      DEFINE_STRING(BGl_string1658z00zzast_exitz00,
		BgL_bgl_string1658za700za7za7a1667za7, "Illegal `jump-exit' form", 24);
	      DEFINE_STRING(BGl_string1659z00zzast_exitz00,
		BgL_bgl_string1659za700za7za7a1668za7, "ast_exit", 8);
	      DEFINE_STRING(BGl_string1660z00zzast_exitz00,
		BgL_bgl_string1660za700za7za7a1669za7,
		":onexit bigloo-c snifun value labels ", 37);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzast_exitz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzast_exitz00(long
		BgL_checksumz00_1730, char *BgL_fromz00_1731)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_exitz00))
				{
					BGl_requirezd2initializa7ationz75zzast_exitz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_exitz00();
					BGl_libraryzd2moduleszd2initz00zzast_exitz00();
					BGl_cnstzd2initzd2zzast_exitz00();
					BGl_importedzd2moduleszd2initz00zzast_exitz00();
					return BGl_methodzd2initzd2zzast_exitz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_exitz00(void)
	{
		{	/* Ast/exit.scm 14 */
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_exit");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "ast_exit");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "ast_exit");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_exit");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "ast_exit");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_exitz00(void)
	{
		{	/* Ast/exit.scm 14 */
			{	/* Ast/exit.scm 14 */
				obj_t BgL_cportz00_1719;

				{	/* Ast/exit.scm 14 */
					obj_t BgL_stringz00_1726;

					BgL_stringz00_1726 = BGl_string1660z00zzast_exitz00;
					{	/* Ast/exit.scm 14 */
						obj_t BgL_startz00_1727;

						BgL_startz00_1727 = BINT(0L);
						{	/* Ast/exit.scm 14 */
							obj_t BgL_endz00_1728;

							BgL_endz00_1728 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1726)));
							{	/* Ast/exit.scm 14 */

								BgL_cportz00_1719 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1726, BgL_startz00_1727, BgL_endz00_1728);
				}}}}
				{
					long BgL_iz00_1720;

					BgL_iz00_1720 = 4L;
				BgL_loopz00_1721:
					if ((BgL_iz00_1720 == -1L))
						{	/* Ast/exit.scm 14 */
							return BUNSPEC;
						}
					else
						{	/* Ast/exit.scm 14 */
							{	/* Ast/exit.scm 14 */
								obj_t BgL_arg1661z00_1722;

								{	/* Ast/exit.scm 14 */

									{	/* Ast/exit.scm 14 */
										obj_t BgL_locationz00_1724;

										BgL_locationz00_1724 = BBOOL(((bool_t) 0));
										{	/* Ast/exit.scm 14 */

											BgL_arg1661z00_1722 =
												BGl_readz00zz__readerz00(BgL_cportz00_1719,
												BgL_locationz00_1724);
										}
									}
								}
								{	/* Ast/exit.scm 14 */
									int BgL_tmpz00_1754;

									BgL_tmpz00_1754 = (int) (BgL_iz00_1720);
									CNST_TABLE_SET(BgL_tmpz00_1754, BgL_arg1661z00_1722);
							}}
							{	/* Ast/exit.scm 14 */
								int BgL_auxz00_1725;

								BgL_auxz00_1725 = (int) ((BgL_iz00_1720 - 1L));
								{
									long BgL_iz00_1759;

									BgL_iz00_1759 = (long) (BgL_auxz00_1725);
									BgL_iz00_1720 = BgL_iz00_1759;
									goto BgL_loopz00_1721;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_exitz00(void)
	{
		{	/* Ast/exit.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* set-exit->node */
	BGL_EXPORTED_DEF BgL_letzd2funzd2_bglt
		BGl_setzd2exitzd2ze3nodeze3zzast_exitz00(obj_t BgL_expz00_3,
		obj_t BgL_stackz00_4, obj_t BgL_locz00_5, obj_t BgL_sitez00_6)
	{
		{	/* Ast/exit.scm 33 */
			{	/* Ast/exit.scm 38 */
				obj_t BgL_locz00_1361;

				BgL_locz00_1361 =
					BGl_findzd2locationzf2locz20zztools_locationz00(BgL_expz00_3,
					BgL_locz00_5);
				{
					obj_t BgL_exitz00_1362;
					obj_t BgL_bodyz00_1363;
					obj_t BgL_exitz00_1365;
					obj_t BgL_bodyz00_1366;
					obj_t BgL_onexitz00_1367;

					if (PAIRP(BgL_expz00_3))
						{	/* Ast/exit.scm 39 */
							obj_t BgL_cdrzd2369zd2_1372;

							BgL_cdrzd2369zd2_1372 = CDR(((obj_t) BgL_expz00_3));
							if (PAIRP(BgL_cdrzd2369zd2_1372))
								{	/* Ast/exit.scm 39 */
									obj_t BgL_carzd2372zd2_1374;
									obj_t BgL_cdrzd2373zd2_1375;

									BgL_carzd2372zd2_1374 = CAR(BgL_cdrzd2369zd2_1372);
									BgL_cdrzd2373zd2_1375 = CDR(BgL_cdrzd2369zd2_1372);
									if (PAIRP(BgL_carzd2372zd2_1374))
										{	/* Ast/exit.scm 39 */
											if (NULLP(CDR(BgL_carzd2372zd2_1374)))
												{	/* Ast/exit.scm 39 */
													if (PAIRP(BgL_cdrzd2373zd2_1375))
														{	/* Ast/exit.scm 39 */
															if (NULLP(CDR(BgL_cdrzd2373zd2_1375)))
																{	/* Ast/exit.scm 39 */
																	obj_t BgL_arg1268z00_1382;
																	obj_t BgL_arg1272z00_1383;

																	BgL_arg1268z00_1382 =
																		CAR(BgL_carzd2372zd2_1374);
																	BgL_arg1272z00_1383 =
																		CAR(BgL_cdrzd2373zd2_1375);
																	{
																		BgL_nodez00_bglt BgL_auxz00_1783;

																		BgL_exitz00_1362 = BgL_arg1268z00_1382;
																		BgL_bodyz00_1363 = BgL_arg1272z00_1383;
																		{	/* Ast/exit.scm 41 */
																			obj_t BgL_hdlgzd2namezd2_1402;

																			{	/* Ast/exit.scm 42 */
																				obj_t BgL_arg1332z00_1430;

																				{	/* Ast/exit.scm 42 */
																					obj_t BgL_list1333z00_1431;

																					BgL_list1333z00_1431 =
																						MAKE_YOUNG_PAIR
																						(BGl_string1656z00zzast_exitz00,
																						BNIL);
																					BgL_arg1332z00_1430 =
																						BGl_makezd2anonymouszd2namez00zzast_sexpz00
																						(BgL_locz00_1361,
																						BgL_list1333z00_1431);
																				}
																				BgL_hdlgzd2namezd2_1402 =
																					BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																					(BgL_arg1332z00_1430);
																			}
																			{	/* Ast/exit.scm 41 */
																				obj_t BgL_hdlgzd2sexpzd2_1403;

																				{	/* Ast/exit.scm 43 */
																					obj_t BgL_arg1323z00_1423;

																					{	/* Ast/exit.scm 43 */
																						obj_t BgL_arg1325z00_1424;
																						obj_t BgL_arg1326z00_1425;

																						{	/* Ast/exit.scm 43 */
																							obj_t BgL_arg1327z00_1426;

																							{	/* Ast/exit.scm 43 */
																								obj_t BgL_arg1328z00_1427;

																								{	/* Ast/exit.scm 43 */
																									obj_t BgL_arg1329z00_1428;

																									BgL_arg1329z00_1428 =
																										MAKE_YOUNG_PAIR(BUNSPEC,
																										BNIL);
																									BgL_arg1328z00_1427 =
																										MAKE_YOUNG_PAIR(BNIL,
																										BgL_arg1329z00_1428);
																								}
																								BgL_arg1327z00_1426 =
																									MAKE_YOUNG_PAIR
																									(BgL_hdlgzd2namezd2_1402,
																									BgL_arg1328z00_1427);
																							}
																							BgL_arg1325z00_1424 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1327z00_1426, BNIL);
																						}
																						{	/* Ast/exit.scm 44 */
																							obj_t BgL_arg1331z00_1429;

																							BgL_arg1331z00_1429 =
																								MAKE_YOUNG_PAIR
																								(BgL_hdlgzd2namezd2_1402, BNIL);
																							BgL_arg1326z00_1425 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1331z00_1429, BNIL);
																						}
																						BgL_arg1323z00_1423 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1325z00_1424,
																							BgL_arg1326z00_1425);
																					}
																					BgL_hdlgzd2sexpzd2_1403 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
																						BgL_arg1323z00_1423);
																				}
																				{	/* Ast/exit.scm 43 */
																					BgL_nodez00_bglt
																						BgL_hdlgzd2nodezd2_1404;
																					BgL_hdlgzd2nodezd2_1404 =
																						BGl_sexpzd2ze3nodez31zzast_sexpz00
																						(BgL_hdlgzd2sexpzd2_1403,
																						BgL_stackz00_4, BgL_locz00_1361,
																						BgL_sitez00_6);
																					{	/* Ast/exit.scm 45 */
																						obj_t BgL_hdlgzd2funzd2_1405;

																						{	/* Ast/exit.scm 46 */
																							obj_t BgL_pairz00_1648;

																							BgL_pairz00_1648 =
																								(((BgL_letzd2funzd2_bglt)
																									COBJECT((
																											(BgL_letzd2funzd2_bglt)
																											BgL_hdlgzd2nodezd2_1404)))->
																								BgL_localsz00);
																							BgL_hdlgzd2funzd2_1405 =
																								CAR(BgL_pairz00_1648);
																						}
																						{	/* Ast/exit.scm 46 */
																							BgL_localz00_bglt
																								BgL_exitz00_1406;
																							BgL_exitz00_1406 =
																								BGl_makezd2localzd2exitze70ze7zzast_exitz00
																								(BgL_exitz00_1362,
																								BgL_hdlgzd2funzd2_1405);
																							{	/* Ast/exit.scm 47 */
																								BgL_nodez00_bglt
																									BgL_bodyz00_1407;
																								{	/* Ast/exit.scm 48 */
																									obj_t BgL_arg1321z00_1421;

																									BgL_arg1321z00_1421 =
																										MAKE_YOUNG_PAIR(
																										((obj_t) BgL_exitz00_1406),
																										BgL_stackz00_4);
																									BgL_bodyz00_1407 =
																										BGl_sexpzd2ze3nodez31zzast_sexpz00
																										(BgL_bodyz00_1363,
																										BgL_arg1321z00_1421,
																										BgL_locz00_1361,
																										CNST_TABLE_REF(1));
																								}
																								{	/* Ast/exit.scm 48 */
																									BgL_pragmaz00_bglt
																										BgL_onexitz00_1408;
																									{	/* Ast/exit.scm 49 */
																										BgL_pragmaz00_bglt
																											BgL_new1109z00_1418;
																										{	/* Ast/exit.scm 50 */
																											BgL_pragmaz00_bglt
																												BgL_new1108z00_1419;
																											BgL_new1108z00_1419 =
																												((BgL_pragmaz00_bglt)
																												BOBJECT(GC_MALLOC(sizeof
																														(struct
																															BgL_pragmaz00_bgl))));
																											{	/* Ast/exit.scm 50 */
																												long
																													BgL_arg1320z00_1420;
																												BgL_arg1320z00_1420 =
																													BGL_CLASS_NUM
																													(BGl_pragmaz00zzast_nodez00);
																												BGL_OBJECT_CLASS_NUM_SET
																													(((BgL_objectz00_bglt)
																														BgL_new1108z00_1419),
																													BgL_arg1320z00_1420);
																											}
																											{	/* Ast/exit.scm 50 */
																												BgL_objectz00_bglt
																													BgL_tmpz00_1809;
																												BgL_tmpz00_1809 =
																													((BgL_objectz00_bglt)
																													BgL_new1108z00_1419);
																												BGL_OBJECT_WIDENING_SET
																													(BgL_tmpz00_1809,
																													BFALSE);
																											}
																											((BgL_objectz00_bglt)
																												BgL_new1108z00_1419);
																											BgL_new1109z00_1418 =
																												BgL_new1108z00_1419;
																										}
																										((((BgL_nodez00_bglt)
																													COBJECT((
																															(BgL_nodez00_bglt)
																															BgL_new1109z00_1418)))->
																												BgL_locz00) =
																											((obj_t) BgL_locz00_1361),
																											BUNSPEC);
																										((((BgL_nodez00_bglt)
																													COBJECT((
																															(BgL_nodez00_bglt)
																															BgL_new1109z00_1418)))->
																												BgL_typez00) =
																											((BgL_typez00_bglt)
																												BGl_strictzd2nodezd2typez00zzast_nodez00
																												(((BgL_typez00_bglt)
																														BGl_za2objza2z00zztype_cachez00),
																													((BgL_typez00_bglt)
																														BGl_za2_za2z00zztype_cachez00))),
																											BUNSPEC);
																										((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1109z00_1418)))->BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
																										((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1109z00_1418)))->BgL_keyz00) = ((obj_t) BINT(-1L)), BUNSPEC);
																										((((BgL_externz00_bglt)
																													COBJECT((
																															(BgL_externz00_bglt)
																															BgL_new1109z00_1418)))->
																												BgL_exprza2za2) =
																											((obj_t) BNIL), BUNSPEC);
																										((((BgL_externz00_bglt)
																													COBJECT((
																															(BgL_externz00_bglt)
																															BgL_new1109z00_1418)))->
																												BgL_effectz00) =
																											((obj_t) BUNSPEC),
																											BUNSPEC);
																										((((BgL_pragmaz00_bglt)
																													COBJECT
																													(BgL_new1109z00_1418))->
																												BgL_formatz00) =
																											((obj_t)
																												BGl_string1657z00zzast_exitz00),
																											BUNSPEC);
																										((((BgL_pragmaz00_bglt)
																													COBJECT
																													(BgL_new1109z00_1418))->
																												BgL_srfi0z00) =
																											((obj_t)
																												CNST_TABLE_REF(3)),
																											BUNSPEC);
																										BgL_onexitz00_1408 =
																											BgL_new1109z00_1418;
																									}
																									{	/* Ast/exit.scm 49 */
																										BgL_setzd2exzd2itz00_bglt
																											BgL_exitzd2bodyzd2_1409;
																										{	/* Ast/exit.scm 54 */
																											BgL_setzd2exzd2itz00_bglt
																												BgL_new1111z00_1412;
																											{	/* Ast/exit.scm 55 */
																												BgL_setzd2exzd2itz00_bglt
																													BgL_new1110z00_1416;
																												BgL_new1110z00_1416 =
																													(
																													(BgL_setzd2exzd2itz00_bglt)
																													BOBJECT(GC_MALLOC
																														(sizeof(struct
																																BgL_setzd2exzd2itz00_bgl))));
																												{	/* Ast/exit.scm 55 */
																													long
																														BgL_arg1319z00_1417;
																													BgL_arg1319z00_1417 =
																														BGL_CLASS_NUM
																														(BGl_setzd2exzd2itz00zzast_nodez00);
																													BGL_OBJECT_CLASS_NUM_SET
																														(((BgL_objectz00_bglt) BgL_new1110z00_1416), BgL_arg1319z00_1417);
																												}
																												{	/* Ast/exit.scm 55 */
																													BgL_objectz00_bglt
																														BgL_tmpz00_1836;
																													BgL_tmpz00_1836 =
																														(
																														(BgL_objectz00_bglt)
																														BgL_new1110z00_1416);
																													BGL_OBJECT_WIDENING_SET
																														(BgL_tmpz00_1836,
																														BFALSE);
																												}
																												((BgL_objectz00_bglt)
																													BgL_new1110z00_1416);
																												BgL_new1111z00_1412 =
																													BgL_new1110z00_1416;
																											}
																											((((BgL_nodez00_bglt)
																														COBJECT((
																																(BgL_nodez00_bglt)
																																BgL_new1111z00_1412)))->
																													BgL_locz00) =
																												((obj_t)
																													BgL_locz00_1361),
																												BUNSPEC);
																											((((BgL_nodez00_bglt)
																														COBJECT((
																																(BgL_nodez00_bglt)
																																BgL_new1111z00_1412)))->
																													BgL_typez00) =
																												((BgL_typez00_bglt)
																													BGl_strictzd2nodezd2typez00zzast_nodez00
																													(((BgL_typez00_bglt)
																															BGl_za2objza2z00zztype_cachez00),
																														((BgL_typez00_bglt)
																															BGl_za2_za2z00zztype_cachez00))),
																												BUNSPEC);
																											{
																												BgL_varz00_bglt
																													BgL_auxz00_1847;
																												{	/* Ast/exit.scm 57 */
																													BgL_refz00_bglt
																														BgL_new1114z00_1413;
																													{	/* Ast/exit.scm 59 */
																														BgL_refz00_bglt
																															BgL_new1113z00_1414;
																														BgL_new1113z00_1414
																															=
																															((BgL_refz00_bglt)
																															BOBJECT(GC_MALLOC
																																(sizeof(struct
																																		BgL_refz00_bgl))));
																														{	/* Ast/exit.scm 59 */
																															long
																																BgL_arg1318z00_1415;
																															{	/* Ast/exit.scm 59 */
																																obj_t
																																	BgL_classz00_1657;
																																BgL_classz00_1657
																																	=
																																	BGl_refz00zzast_nodez00;
																																BgL_arg1318z00_1415
																																	=
																																	BGL_CLASS_NUM
																																	(BgL_classz00_1657);
																															}
																															BGL_OBJECT_CLASS_NUM_SET
																																(((BgL_objectz00_bglt) BgL_new1113z00_1414), BgL_arg1318z00_1415);
																														}
																														{	/* Ast/exit.scm 59 */
																															BgL_objectz00_bglt
																																BgL_tmpz00_1852;
																															BgL_tmpz00_1852 =
																																(
																																(BgL_objectz00_bglt)
																																BgL_new1113z00_1414);
																															BGL_OBJECT_WIDENING_SET
																																(BgL_tmpz00_1852,
																																BFALSE);
																														}
																														((BgL_objectz00_bglt) BgL_new1113z00_1414);
																														BgL_new1114z00_1413
																															=
																															BgL_new1113z00_1414;
																													}
																													((((BgL_nodez00_bglt)
																																COBJECT((
																																		(BgL_nodez00_bglt)
																																		BgL_new1114z00_1413)))->
																															BgL_locz00) =
																														((obj_t)
																															BgL_locz00_1361),
																														BUNSPEC);
																													((((BgL_nodez00_bglt)
																																COBJECT((
																																		(BgL_nodez00_bglt)
																																		BgL_new1114z00_1413)))->
																															BgL_typez00) =
																														((BgL_typez00_bglt)
																															BGl_strictzd2nodezd2typez00zzast_nodez00
																															(((BgL_typez00_bglt) BGl_za2_za2z00zztype_cachez00), ((BgL_typez00_bglt) BGl_za2exitza2z00zztype_cachez00))), BUNSPEC);
																													((((BgL_varz00_bglt)
																																COBJECT((
																																		(BgL_varz00_bglt)
																																		BgL_new1114z00_1413)))->
																															BgL_variablez00) =
																														((BgL_variablez00_bglt) ((BgL_variablez00_bglt) BgL_exitz00_1406)), BUNSPEC);
																													BgL_auxz00_1847 =
																														((BgL_varz00_bglt)
																														BgL_new1114z00_1413);
																												}
																												((((BgL_setzd2exzd2itz00_bglt) COBJECT(BgL_new1111z00_1412))->BgL_varz00) = ((BgL_varz00_bglt) BgL_auxz00_1847), BUNSPEC);
																											}
																											((((BgL_setzd2exzd2itz00_bglt) COBJECT(BgL_new1111z00_1412))->BgL_bodyz00) = ((BgL_nodez00_bglt) BgL_bodyz00_1407), BUNSPEC);
																											((((BgL_setzd2exzd2itz00_bglt) COBJECT(BgL_new1111z00_1412))->BgL_onexitz00) = ((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_onexitz00_1408)), BUNSPEC);
																											BgL_exitzd2bodyzd2_1409 =
																												BgL_new1111z00_1412;
																										}
																										{	/* Ast/exit.scm 54 */

																											((((BgL_variablez00_bglt)
																														COBJECT((
																																(BgL_variablez00_bglt)
																																((BgL_localz00_bglt) BgL_hdlgzd2funzd2_1405))))->BgL_userzf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
																											{	/* Ast/exit.scm 69 */
																												BgL_valuez00_bglt
																													BgL_arg1316z00_1410;
																												BgL_arg1316z00_1410 =
																													(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt) BgL_hdlgzd2funzd2_1405))))->BgL_valuez00);
																												{	/* Ast/exit.scm 69 */
																													obj_t BgL_vz00_1665;

																													BgL_vz00_1665 =
																														CNST_TABLE_REF(2);
																													((((BgL_sfunz00_bglt)
																																COBJECT((
																																		(BgL_sfunz00_bglt)
																																		BgL_arg1316z00_1410)))->
																															BgL_classz00) =
																														((obj_t)
																															BgL_vz00_1665),
																														BUNSPEC);
																											}}
																											{	/* Ast/exit.scm 70 */
																												BgL_valuez00_bglt
																													BgL_arg1317z00_1411;
																												BgL_arg1317z00_1411 =
																													(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt) BgL_hdlgzd2funzd2_1405))))->BgL_valuez00);
																												((((BgL_sfunz00_bglt)
																															COBJECT((
																																	(BgL_sfunz00_bglt)
																																	BgL_arg1317z00_1411)))->
																														BgL_bodyz00) =
																													((obj_t) ((obj_t)
																															BgL_exitzd2bodyzd2_1409)),
																													BUNSPEC);
																											}
																											BgL_auxz00_1783 =
																												BgL_hdlgzd2nodezd2_1404;
																		}}}}}}}}}
																		return
																			((BgL_letzd2funzd2_bglt) BgL_auxz00_1783);
																	}
																}
															else
																{	/* Ast/exit.scm 39 */
																	obj_t BgL_cdrzd2398zd2_1385;

																	BgL_cdrzd2398zd2_1385 =
																		CDR(((obj_t) BgL_cdrzd2369zd2_1372));
																	{	/* Ast/exit.scm 39 */
																		obj_t BgL_cdrzd2409zd2_1386;

																		BgL_cdrzd2409zd2_1386 =
																			CDR(((obj_t) BgL_cdrzd2398zd2_1385));
																		if (PAIRP(BgL_cdrzd2409zd2_1386))
																			{	/* Ast/exit.scm 39 */
																				obj_t BgL_cdrzd2414zd2_1388;

																				BgL_cdrzd2414zd2_1388 =
																					CDR(BgL_cdrzd2409zd2_1386);
																				if (
																					(CAR(BgL_cdrzd2409zd2_1386) ==
																						CNST_TABLE_REF(4)))
																					{	/* Ast/exit.scm 39 */
																						if (PAIRP(BgL_cdrzd2414zd2_1388))
																							{	/* Ast/exit.scm 39 */
																								if (NULLP(CDR
																										(BgL_cdrzd2414zd2_1388)))
																									{	/* Ast/exit.scm 39 */
																										obj_t BgL_arg1306z00_1394;
																										obj_t BgL_arg1307z00_1395;
																										obj_t BgL_arg1308z00_1396;

																										{	/* Ast/exit.scm 39 */
																											obj_t BgL_pairz00_1699;

																											BgL_pairz00_1699 =
																												CAR(
																												((obj_t)
																													BgL_cdrzd2369zd2_1372));
																											BgL_arg1306z00_1394 =
																												CAR(BgL_pairz00_1699);
																										}
																										BgL_arg1307z00_1395 =
																											CAR(
																											((obj_t)
																												BgL_cdrzd2398zd2_1385));
																										BgL_arg1308z00_1396 =
																											CAR
																											(BgL_cdrzd2414zd2_1388);
																										{
																											BgL_nodez00_bglt
																												BgL_auxz00_1909;
																											BgL_exitz00_1365 =
																												BgL_arg1306z00_1394;
																											BgL_bodyz00_1366 =
																												BgL_arg1307z00_1395;
																											BgL_onexitz00_1367 =
																												BgL_arg1308z00_1396;
																											{	/* Ast/exit.scm 74 */
																												obj_t
																													BgL_hdlgzd2namezd2_1432;
																												{	/* Ast/exit.scm 75 */
																													obj_t
																														BgL_arg1370z00_1457;
																													{	/* Ast/exit.scm 75 */
																														obj_t
																															BgL_list1371z00_1458;
																														BgL_list1371z00_1458
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_string1656z00zzast_exitz00,
																															BNIL);
																														BgL_arg1370z00_1457
																															=
																															BGl_makezd2anonymouszd2namez00zzast_sexpz00
																															(BgL_locz00_1361,
																															BgL_list1371z00_1458);
																													}
																													BgL_hdlgzd2namezd2_1432
																														=
																														BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																														(BgL_arg1370z00_1457);
																												}
																												{	/* Ast/exit.scm 74 */
																													obj_t
																														BgL_hdlgzd2sexpzd2_1433;
																													{	/* Ast/exit.scm 76 */
																														obj_t
																															BgL_arg1348z00_1450;
																														{	/* Ast/exit.scm 76 */
																															obj_t
																																BgL_arg1349z00_1451;
																															obj_t
																																BgL_arg1351z00_1452;
																															{	/* Ast/exit.scm 76 */
																																obj_t
																																	BgL_arg1352z00_1453;
																																{	/* Ast/exit.scm 76 */
																																	obj_t
																																		BgL_arg1361z00_1454;
																																	{	/* Ast/exit.scm 76 */
																																		obj_t
																																			BgL_arg1364z00_1455;
																																		BgL_arg1364z00_1455
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BUNSPEC,
																																			BNIL);
																																		BgL_arg1361z00_1454
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BNIL,
																																			BgL_arg1364z00_1455);
																																	}
																																	BgL_arg1352z00_1453
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_hdlgzd2namezd2_1432,
																																		BgL_arg1361z00_1454);
																																}
																																BgL_arg1349z00_1451
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1352z00_1453,
																																	BNIL);
																															}
																															{	/* Ast/exit.scm 77 */
																																obj_t
																																	BgL_arg1367z00_1456;
																																BgL_arg1367z00_1456
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_hdlgzd2namezd2_1432,
																																	BNIL);
																																BgL_arg1351z00_1452
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1367z00_1456,
																																	BNIL);
																															}
																															BgL_arg1348z00_1450
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1349z00_1451,
																																BgL_arg1351z00_1452);
																														}
																														BgL_hdlgzd2sexpzd2_1433
																															=
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(0),
																															BgL_arg1348z00_1450);
																													}
																													{	/* Ast/exit.scm 76 */
																														BgL_nodez00_bglt
																															BgL_hdlgzd2nodezd2_1434;
																														BgL_hdlgzd2nodezd2_1434
																															=
																															BGl_sexpzd2ze3nodez31zzast_sexpz00
																															(BgL_hdlgzd2sexpzd2_1433,
																															BgL_stackz00_4,
																															BgL_locz00_1361,
																															BgL_sitez00_6);
																														{	/* Ast/exit.scm 78 */
																															obj_t
																																BgL_hdlgzd2funzd2_1435;
																															{	/* Ast/exit.scm 79 */
																																obj_t
																																	BgL_pairz00_1669;
																																BgL_pairz00_1669
																																	=
																																	(((BgL_letzd2funzd2_bglt) COBJECT(((BgL_letzd2funzd2_bglt) BgL_hdlgzd2nodezd2_1434)))->BgL_localsz00);
																																BgL_hdlgzd2funzd2_1435
																																	=
																																	CAR
																																	(BgL_pairz00_1669);
																															}
																															{	/* Ast/exit.scm 79 */
																																BgL_localz00_bglt
																																	BgL_exitz00_1436;
																																BgL_exitz00_1436
																																	=
																																	BGl_makezd2localzd2exitze70ze7zzast_exitz00
																																	(BgL_exitz00_1365,
																																	BgL_hdlgzd2funzd2_1435);
																																{	/* Ast/exit.scm 80 */
																																	BgL_nodez00_bglt
																																		BgL_bodyz00_1437;
																																	{	/* Ast/exit.scm 81 */
																																		obj_t
																																			BgL_arg1343z00_1448;
																																		BgL_arg1343z00_1448
																																			=
																																			MAKE_YOUNG_PAIR
																																			(((obj_t)
																																				BgL_exitz00_1436),
																																			BgL_stackz00_4);
																																		BgL_bodyz00_1437
																																			=
																																			BGl_sexpzd2ze3nodez31zzast_sexpz00
																																			(BgL_bodyz00_1366,
																																			BgL_arg1343z00_1448,
																																			BgL_locz00_1361,
																																			CNST_TABLE_REF
																																			(1));
																																	}
																																	{	/* Ast/exit.scm 81 */
																																		BgL_nodez00_bglt
																																			BgL_onexitz00_1438;
																																		BgL_onexitz00_1438
																																			=
																																			BGl_sexpzd2ze3nodez31zzast_sexpz00
																																			(BgL_onexitz00_1367,
																																			BgL_stackz00_4,
																																			BgL_locz00_1361,
																																			CNST_TABLE_REF
																																			(1));
																																		{	/* Ast/exit.scm 82 */
																																			BgL_setzd2exzd2itz00_bglt
																																				BgL_exitzd2bodyzd2_1439;
																																			{	/* Ast/exit.scm 83 */
																																				BgL_setzd2exzd2itz00_bglt
																																					BgL_new1116z00_1442;
																																				{	/* Ast/exit.scm 84 */
																																					BgL_setzd2exzd2itz00_bglt
																																						BgL_new1115z00_1446;
																																					BgL_new1115z00_1446
																																						=
																																						(
																																						(BgL_setzd2exzd2itz00_bglt)
																																						BOBJECT
																																						(GC_MALLOC
																																							(sizeof
																																								(struct
																																									BgL_setzd2exzd2itz00_bgl))));
																																					{	/* Ast/exit.scm 84 */
																																						long
																																							BgL_arg1342z00_1447;
																																						BgL_arg1342z00_1447
																																							=
																																							BGL_CLASS_NUM
																																							(BGl_setzd2exzd2itz00zzast_nodez00);
																																						BGL_OBJECT_CLASS_NUM_SET
																																							(((BgL_objectz00_bglt) BgL_new1115z00_1446), BgL_arg1342z00_1447);
																																					}
																																					{	/* Ast/exit.scm 84 */
																																						BgL_objectz00_bglt
																																							BgL_tmpz00_1937;
																																						BgL_tmpz00_1937
																																							=
																																							(
																																							(BgL_objectz00_bglt)
																																							BgL_new1115z00_1446);
																																						BGL_OBJECT_WIDENING_SET
																																							(BgL_tmpz00_1937,
																																							BFALSE);
																																					}
																																					((BgL_objectz00_bglt) BgL_new1115z00_1446);
																																					BgL_new1116z00_1442
																																						=
																																						BgL_new1115z00_1446;
																																				}
																																				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1116z00_1442)))->BgL_locz00) = ((obj_t) BgL_locz00_1361), BUNSPEC);
																																				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1116z00_1442)))->BgL_typez00) = ((BgL_typez00_bglt) BGl_strictzd2nodezd2typez00zzast_nodez00(((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00), ((BgL_typez00_bglt) BGl_za2_za2z00zztype_cachez00))), BUNSPEC);
																																				{
																																					BgL_varz00_bglt
																																						BgL_auxz00_1948;
																																					{	/* Ast/exit.scm 86 */
																																						BgL_refz00_bglt
																																							BgL_new1119z00_1443;
																																						{	/* Ast/exit.scm 88 */
																																							BgL_refz00_bglt
																																								BgL_new1118z00_1444;
																																							BgL_new1118z00_1444
																																								=
																																								(
																																								(BgL_refz00_bglt)
																																								BOBJECT
																																								(GC_MALLOC
																																									(sizeof
																																										(struct
																																											BgL_refz00_bgl))));
																																							{	/* Ast/exit.scm 88 */
																																								long
																																									BgL_arg1340z00_1445;
																																								{	/* Ast/exit.scm 88 */
																																									obj_t
																																										BgL_classz00_1674;
																																									BgL_classz00_1674
																																										=
																																										BGl_refz00zzast_nodez00;
																																									BgL_arg1340z00_1445
																																										=
																																										BGL_CLASS_NUM
																																										(BgL_classz00_1674);
																																								}
																																								BGL_OBJECT_CLASS_NUM_SET
																																									(
																																									((BgL_objectz00_bglt) BgL_new1118z00_1444), BgL_arg1340z00_1445);
																																							}
																																							{	/* Ast/exit.scm 88 */
																																								BgL_objectz00_bglt
																																									BgL_tmpz00_1953;
																																								BgL_tmpz00_1953
																																									=
																																									(
																																									(BgL_objectz00_bglt)
																																									BgL_new1118z00_1444);
																																								BGL_OBJECT_WIDENING_SET
																																									(BgL_tmpz00_1953,
																																									BFALSE);
																																							}
																																							((BgL_objectz00_bglt) BgL_new1118z00_1444);
																																							BgL_new1119z00_1443
																																								=
																																								BgL_new1118z00_1444;
																																						}
																																						((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1119z00_1443)))->BgL_locz00) = ((obj_t) BgL_locz00_1361), BUNSPEC);
																																						((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1119z00_1443)))->BgL_typez00) = ((BgL_typez00_bglt) BGl_strictzd2nodezd2typez00zzast_nodez00(((BgL_typez00_bglt) BGl_za2_za2z00zztype_cachez00), ((BgL_typez00_bglt) BGl_za2exitza2z00zztype_cachez00))), BUNSPEC);
																																						((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt) BgL_new1119z00_1443)))->BgL_variablez00) = ((BgL_variablez00_bglt) ((BgL_variablez00_bglt) BgL_exitz00_1436)), BUNSPEC);
																																						BgL_auxz00_1948
																																							=
																																							(
																																							(BgL_varz00_bglt)
																																							BgL_new1119z00_1443);
																																					}
																																					((((BgL_setzd2exzd2itz00_bglt) COBJECT(BgL_new1116z00_1442))->BgL_varz00) = ((BgL_varz00_bglt) BgL_auxz00_1948), BUNSPEC);
																																				}
																																				((((BgL_setzd2exzd2itz00_bglt) COBJECT(BgL_new1116z00_1442))->BgL_bodyz00) = ((BgL_nodez00_bglt) BgL_bodyz00_1437), BUNSPEC);
																																				((((BgL_setzd2exzd2itz00_bglt) COBJECT(BgL_new1116z00_1442))->BgL_onexitz00) = ((BgL_nodez00_bglt) BgL_onexitz00_1438), BUNSPEC);
																																				BgL_exitzd2bodyzd2_1439
																																					=
																																					BgL_new1116z00_1442;
																																			}
																																			{	/* Ast/exit.scm 83 */

																																				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt) BgL_hdlgzd2funzd2_1435))))->BgL_userzf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
																																				{	/* Ast/exit.scm 98 */
																																					BgL_valuez00_bglt
																																						BgL_arg1335z00_1440;
																																					BgL_arg1335z00_1440
																																						=
																																						(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt) BgL_hdlgzd2funzd2_1435))))->BgL_valuez00);
																																					{	/* Ast/exit.scm 98 */
																																						obj_t
																																							BgL_vz00_1682;
																																						BgL_vz00_1682
																																							=
																																							CNST_TABLE_REF
																																							(2);
																																						((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) BgL_arg1335z00_1440)))->BgL_classz00) = ((obj_t) BgL_vz00_1682), BUNSPEC);
																																				}}
																																				{	/* Ast/exit.scm 99 */
																																					BgL_valuez00_bglt
																																						BgL_arg1339z00_1441;
																																					BgL_arg1339z00_1441
																																						=
																																						(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt) BgL_hdlgzd2funzd2_1435))))->BgL_valuez00);
																																					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) BgL_arg1339z00_1441)))->BgL_bodyz00) = ((obj_t) ((obj_t) BgL_exitzd2bodyzd2_1439)), BUNSPEC);
																																				}
																																				BgL_auxz00_1909
																																					=
																																					BgL_hdlgzd2nodezd2_1434;
																											}}}}}}}}}
																											return
																												((BgL_letzd2funzd2_bglt)
																												BgL_auxz00_1909);
																										}
																									}
																								else
																									{
																										BgL_nodez00_bglt
																											BgL_auxz00_1987;
																									BgL_tagzd2361zd2_1369:
																										BgL_auxz00_1987 =
																											BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00
																											(BGl_string1655z00zzast_exitz00,
																											BgL_expz00_3,
																											BgL_locz00_1361);
																										return (
																											(BgL_letzd2funzd2_bglt)
																											BgL_auxz00_1987);
																									}
																							}
																						else
																							{
																								BgL_nodez00_bglt
																									BgL_auxz00_1990;
																								goto BgL_tagzd2361zd2_1369;
																								return
																									((BgL_letzd2funzd2_bglt)
																									BgL_auxz00_1990);
																							}
																					}
																				else
																					{
																						BgL_nodez00_bglt BgL_auxz00_1992;

																						goto BgL_tagzd2361zd2_1369;
																						return
																							((BgL_letzd2funzd2_bglt)
																							BgL_auxz00_1992);
																					}
																			}
																		else
																			{
																				BgL_nodez00_bglt BgL_auxz00_1994;

																				goto BgL_tagzd2361zd2_1369;
																				return
																					((BgL_letzd2funzd2_bglt)
																					BgL_auxz00_1994);
																			}
																	}
																}
														}
													else
														{
															BgL_nodez00_bglt BgL_auxz00_1996;

															goto BgL_tagzd2361zd2_1369;
															return ((BgL_letzd2funzd2_bglt) BgL_auxz00_1996);
														}
												}
											else
												{
													BgL_nodez00_bglt BgL_auxz00_1998;

													goto BgL_tagzd2361zd2_1369;
													return ((BgL_letzd2funzd2_bglt) BgL_auxz00_1998);
												}
										}
									else
										{
											BgL_nodez00_bglt BgL_auxz00_2000;

											goto BgL_tagzd2361zd2_1369;
											return ((BgL_letzd2funzd2_bglt) BgL_auxz00_2000);
										}
								}
							else
								{
									BgL_nodez00_bglt BgL_auxz00_2002;

									goto BgL_tagzd2361zd2_1369;
									return ((BgL_letzd2funzd2_bglt) BgL_auxz00_2002);
								}
						}
					else
						{
							BgL_nodez00_bglt BgL_auxz00_2004;

							goto BgL_tagzd2361zd2_1369;
							return ((BgL_letzd2funzd2_bglt) BgL_auxz00_2004);
						}
				}
			}
		}

	}



/* make-local-exit~0 */
	BgL_localz00_bglt BGl_makezd2localzd2exitze70ze7zzast_exitz00(obj_t
		BgL_exitz00_1459, obj_t BgL_handlerz00_1460)
	{
		{	/* Ast/exit.scm 36 */
			{	/* Ast/exit.scm 36 */
				BgL_sexitz00_bglt BgL_arg1375z00_1462;

				{	/* Ast/exit.scm 36 */
					BgL_sexitz00_bglt BgL_new1107z00_1463;

					{	/* Ast/exit.scm 36 */
						BgL_sexitz00_bglt BgL_new1106z00_1464;

						BgL_new1106z00_1464 =
							((BgL_sexitz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_sexitz00_bgl))));
						{	/* Ast/exit.scm 36 */
							long BgL_arg1376z00_1465;

							BgL_arg1376z00_1465 = BGL_CLASS_NUM(BGl_sexitz00zzast_varz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1106z00_1464),
								BgL_arg1376z00_1465);
						}
						{	/* Ast/exit.scm 36 */
							BgL_objectz00_bglt BgL_tmpz00_2010;

							BgL_tmpz00_2010 = ((BgL_objectz00_bglt) BgL_new1106z00_1464);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2010, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_new1106z00_1464);
						BgL_new1107z00_1463 = BgL_new1106z00_1464;
					}
					((((BgL_sexitz00_bglt) COBJECT(BgL_new1107z00_1463))->
							BgL_handlerz00) = ((obj_t) BgL_handlerz00_1460), BUNSPEC);
					((((BgL_sexitz00_bglt) COBJECT(BgL_new1107z00_1463))->
							BgL_detachedzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
					BgL_arg1375z00_1462 = BgL_new1107z00_1463;
				}
				return
					BGl_makezd2localzd2sexitz00zzast_localz00(BgL_exitz00_1459,
					((BgL_typez00_bglt) BGl_za2exitza2z00zztype_cachez00),
					BgL_arg1375z00_1462);
			}
		}

	}



/* &set-exit->node */
	BgL_letzd2funzd2_bglt BGl_z62setzd2exitzd2ze3nodez81zzast_exitz00(obj_t
		BgL_envz00_1709, obj_t BgL_expz00_1710, obj_t BgL_stackz00_1711,
		obj_t BgL_locz00_1712, obj_t BgL_sitez00_1713)
	{
		{	/* Ast/exit.scm 33 */
			return
				BGl_setzd2exitzd2ze3nodeze3zzast_exitz00(BgL_expz00_1710,
				BgL_stackz00_1711, BgL_locz00_1712, BgL_sitez00_1713);
		}

	}



/* jump-exit->node */
	BGL_EXPORTED_DEF BgL_jumpzd2exzd2itz00_bglt
		BGl_jumpzd2exitzd2ze3nodeze3zzast_exitz00(obj_t BgL_expz00_7,
		obj_t BgL_stackz00_8, obj_t BgL_locz00_9, obj_t BgL_sitez00_10)
	{
		{	/* Ast/exit.scm 107 */
			{	/* Ast/exit.scm 108 */
				obj_t BgL_locz00_1467;

				BgL_locz00_1467 =
					BGl_findzd2locationzf2locz20zztools_locationz00(BgL_expz00_7,
					BgL_locz00_9);
				{
					obj_t BgL_exitz00_1468;
					obj_t BgL_valuez00_1469;

					if (PAIRP(BgL_expz00_7))
						{	/* Ast/exit.scm 109 */
							obj_t BgL_cdrzd2459zd2_1474;

							BgL_cdrzd2459zd2_1474 = CDR(((obj_t) BgL_expz00_7));
							if (PAIRP(BgL_cdrzd2459zd2_1474))
								{	/* Ast/exit.scm 109 */
									BgL_exitz00_1468 = CAR(BgL_cdrzd2459zd2_1474);
									BgL_valuez00_1469 = CDR(BgL_cdrzd2459zd2_1474);
									{	/* Ast/exit.scm 111 */
										BgL_nodez00_bglt BgL_valuez00_1478;
										BgL_nodez00_bglt BgL_exitz00_1479;

										{	/* Ast/exit.scm 111 */
											obj_t BgL_arg1422z00_1483;

											BgL_arg1422z00_1483 =
												BGl_normaliza7ezd2prognz75zztools_prognz00
												(BgL_valuez00_1469);
											BgL_valuez00_1478 =
												BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_arg1422z00_1483,
												BgL_stackz00_8, BgL_locz00_1467, CNST_TABLE_REF(1));
										}
										BgL_exitz00_1479 =
											BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_exitz00_1468,
											BgL_stackz00_8, BgL_locz00_1467, CNST_TABLE_REF(1));
										{	/* Ast/exit.scm 113 */
											BgL_jumpzd2exzd2itz00_bglt BgL_new1122z00_1480;

											{	/* Ast/exit.scm 114 */
												BgL_jumpzd2exzd2itz00_bglt BgL_new1120z00_1481;

												BgL_new1120z00_1481 =
													((BgL_jumpzd2exzd2itz00_bglt)
													BOBJECT(GC_MALLOC(sizeof(struct
																BgL_jumpzd2exzd2itz00_bgl))));
												{	/* Ast/exit.scm 114 */
													long BgL_arg1421z00_1482;

													BgL_arg1421z00_1482 =
														BGL_CLASS_NUM(BGl_jumpzd2exzd2itz00zzast_nodez00);
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt) BgL_new1120z00_1481),
														BgL_arg1421z00_1482);
												}
												{	/* Ast/exit.scm 114 */
													BgL_objectz00_bglt BgL_tmpz00_2035;

													BgL_tmpz00_2035 =
														((BgL_objectz00_bglt) BgL_new1120z00_1481);
													BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2035, BFALSE);
												}
												((BgL_objectz00_bglt) BgL_new1120z00_1481);
												BgL_new1122z00_1480 = BgL_new1120z00_1481;
											}
											((((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt) BgL_new1122z00_1480)))->
													BgL_locz00) = ((obj_t) BgL_locz00_1467), BUNSPEC);
											((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																BgL_new1122z00_1480)))->BgL_typez00) =
												((BgL_typez00_bglt)
													BGl_strictzd2nodezd2typez00zzast_nodez00((
															(BgL_typez00_bglt)
															BGl_za2unspecza2z00zztype_cachez00),
														((BgL_typez00_bglt)
															BGl_za2_za2z00zztype_cachez00))), BUNSPEC);
											((((BgL_jumpzd2exzd2itz00_bglt)
														COBJECT(BgL_new1122z00_1480))->BgL_exitz00) =
												((BgL_nodez00_bglt) BgL_exitz00_1479), BUNSPEC);
											((((BgL_jumpzd2exzd2itz00_bglt)
														COBJECT(BgL_new1122z00_1480))->BgL_valuez00) =
												((BgL_nodez00_bglt) BgL_valuez00_1478), BUNSPEC);
											return BgL_new1122z00_1480;
										}
									}
								}
							else
								{
									BgL_nodez00_bglt BgL_auxz00_2050;

								BgL_tagzd2452zd2_1471:
									BgL_auxz00_2050 =
										BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00
										(BGl_string1658z00zzast_exitz00, BgL_expz00_7,
										BgL_locz00_1467);
									return ((BgL_jumpzd2exzd2itz00_bglt) BgL_auxz00_2050);
								}
						}
					else
						{
							BgL_nodez00_bglt BgL_auxz00_2053;

							goto BgL_tagzd2452zd2_1471;
							return ((BgL_jumpzd2exzd2itz00_bglt) BgL_auxz00_2053);
						}
				}
			}
		}

	}



/* &jump-exit->node */
	BgL_jumpzd2exzd2itz00_bglt BGl_z62jumpzd2exitzd2ze3nodez81zzast_exitz00(obj_t
		BgL_envz00_1714, obj_t BgL_expz00_1715, obj_t BgL_stackz00_1716,
		obj_t BgL_locz00_1717, obj_t BgL_sitez00_1718)
	{
		{	/* Ast/exit.scm 107 */
			return
				BGl_jumpzd2exitzd2ze3nodeze3zzast_exitz00(BgL_expz00_1715,
				BgL_stackz00_1716, BgL_locz00_1717, BgL_sitez00_1718);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_exitz00(void)
	{
		{	/* Ast/exit.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_exitz00(void)
	{
		{	/* Ast/exit.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_exitz00(void)
	{
		{	/* Ast/exit.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_exitz00(void)
	{
		{	/* Ast/exit.scm 14 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1659z00zzast_exitz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1659z00zzast_exitz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1659z00zzast_exitz00));
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1659z00zzast_exitz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1659z00zzast_exitz00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string1659z00zzast_exitz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1659z00zzast_exitz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1659z00zzast_exitz00));
			BGl_modulezd2initializa7ationz75zztools_prognz00(301998274L,
				BSTRING_TO_STRING(BGl_string1659z00zzast_exitz00));
			return
				BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1659z00zzast_exitz00));
		}

	}

#ifdef __cplusplus
}
#endif
