/*===========================================================================*/
/*   (Ast/build.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/build.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_AST_BUILD_TYPE_DEFINITIONS
#define BGL_AST_BUILD_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;


#endif													// BGL_AST_BUILD_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzast_buildz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_appendzd2astzd2zzast_buildz00(obj_t, obj_t);
	extern obj_t BGl_findzd2locationzf2locz20zztools_locationz00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	static obj_t BGl_z62appendzd2astzb0zzast_buildz00(obj_t, obj_t, obj_t);
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzast_buildz00(void);
	extern obj_t BGl_leavezd2functionzd2zztools_errorz00(void);
	static obj_t BGl_objectzd2initzd2zzast_buildz00(void);
	BGL_IMPORT obj_t BGl_appendz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	static BgL_globalz00_bglt
		BGl_sfunzd2defzd2ze3astze3zzast_buildz00(BgL_globalz00_bglt);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzast_buildz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzast_buildz00(void);
	extern obj_t BGl_checkzd2tozd2bezd2definezd2zzast_findzd2gdefszd2(void);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	static obj_t BGl_z62buildzd2astzb0zzast_buildz00(obj_t, obj_t);
	extern obj_t BGl_removezd2varzd2zzast_removez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_buildzd2astzd2sanszd2removezd2zzast_buildz00(obj_t);
	extern obj_t BGl_unitzd2ze3defsz31zzast_unitz00(obj_t);
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_buildzd2astzd2zzast_buildz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzast_buildz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_argsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_removez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_findzd2gdefszd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_unitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	extern obj_t BGl_enterzd2functionzd2zztools_errorz00(obj_t);
	static obj_t BGl_z62buildzd2astzd2sanszd2removezb0zzast_buildz00(obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zzast_buildz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_buildz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzast_buildz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzast_buildz00(void);
	extern BgL_nodez00_bglt BGl_sexpzd2ze3nodez31zzast_sexpz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31377ze3ze5zzast_buildz00(obj_t);
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t __cnst[3];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_buildzd2astzd2envz00zzast_buildz00,
		BgL_bgl_za762buildza7d2astza7b1643za7, BGl_z62buildzd2astzb0zzast_buildz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1631z00zzast_buildz00,
		BgL_bgl_string1631za700za7za7a1644za7, "Ast", 3);
	      DEFINE_STRING(BGl_string1632z00zzast_buildz00,
		BgL_bgl_string1632za700za7za7a1645za7, "   . ", 5);
	      DEFINE_STRING(BGl_string1633z00zzast_buildz00,
		BgL_bgl_string1633za700za7za7a1646za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string1634z00zzast_buildz00,
		BgL_bgl_string1634za700za7za7a1647za7, " error", 6);
	      DEFINE_STRING(BGl_string1635z00zzast_buildz00,
		BgL_bgl_string1635za700za7za7a1648za7, "s", 1);
	      DEFINE_STRING(BGl_string1636z00zzast_buildz00,
		BgL_bgl_string1636za700za7za7a1649za7, "", 0);
	      DEFINE_STRING(BGl_string1637z00zzast_buildz00,
		BgL_bgl_string1637za700za7za7a1650za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string1638z00zzast_buildz00,
		BgL_bgl_string1638za700za7za7a1651za7, "failure during postlude hook", 28);
	      DEFINE_STRING(BGl_string1640z00zzast_buildz00,
		BgL_bgl_string1640za700za7za7a1652za7, "ast_build", 9);
	      DEFINE_STRING(BGl_string1641z00zzast_buildz00,
		BgL_bgl_string1641za700za7za7a1653za7, "value pass-started ast ", 23);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1639z00zzast_buildz00,
		BgL_bgl_za762za7c3za704anonymo1654za7,
		BGl_z62zc3z04anonymousza31377ze3ze5zzast_buildz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_appendzd2astzd2envz00zzast_buildz00,
		BgL_bgl_za762appendza7d2astza71655za7, BGl_z62appendzd2astzb0zzast_buildz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_buildzd2astzd2sanszd2removezd2envz00zzast_buildz00,
		BgL_bgl_za762buildza7d2astza7d1656za7,
		BGl_z62buildzd2astzd2sanszd2removezb0zzast_buildz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzast_buildz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzast_buildz00(long
		BgL_checksumz00_1712, char *BgL_fromz00_1713)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_buildz00))
				{
					BGl_requirezd2initializa7ationz75zzast_buildz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_buildz00();
					BGl_libraryzd2moduleszd2initz00zzast_buildz00();
					BGl_cnstzd2initzd2zzast_buildz00();
					BGl_importedzd2moduleszd2initz00zzast_buildz00();
					return BGl_methodzd2initzd2zzast_buildz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_buildz00(void)
	{
		{	/* Ast/build.scm 14 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "ast_build");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"ast_build");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_build");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "ast_build");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_build");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "ast_build");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "ast_build");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "ast_build");
			BGl_modulezd2initializa7ationz75zz__bexitz00(0L, "ast_build");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_buildz00(void)
	{
		{	/* Ast/build.scm 14 */
			{	/* Ast/build.scm 14 */
				obj_t BgL_cportz00_1701;

				{	/* Ast/build.scm 14 */
					obj_t BgL_stringz00_1708;

					BgL_stringz00_1708 = BGl_string1641z00zzast_buildz00;
					{	/* Ast/build.scm 14 */
						obj_t BgL_startz00_1709;

						BgL_startz00_1709 = BINT(0L);
						{	/* Ast/build.scm 14 */
							obj_t BgL_endz00_1710;

							BgL_endz00_1710 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1708)));
							{	/* Ast/build.scm 14 */

								BgL_cportz00_1701 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1708, BgL_startz00_1709, BgL_endz00_1710);
				}}}}
				{
					long BgL_iz00_1702;

					BgL_iz00_1702 = 2L;
				BgL_loopz00_1703:
					if ((BgL_iz00_1702 == -1L))
						{	/* Ast/build.scm 14 */
							return BUNSPEC;
						}
					else
						{	/* Ast/build.scm 14 */
							{	/* Ast/build.scm 14 */
								obj_t BgL_arg1642z00_1704;

								{	/* Ast/build.scm 14 */

									{	/* Ast/build.scm 14 */
										obj_t BgL_locationz00_1706;

										BgL_locationz00_1706 = BBOOL(((bool_t) 0));
										{	/* Ast/build.scm 14 */

											BgL_arg1642z00_1704 =
												BGl_readz00zz__readerz00(BgL_cportz00_1701,
												BgL_locationz00_1706);
										}
									}
								}
								{	/* Ast/build.scm 14 */
									int BgL_tmpz00_1740;

									BgL_tmpz00_1740 = (int) (BgL_iz00_1702);
									CNST_TABLE_SET(BgL_tmpz00_1740, BgL_arg1642z00_1704);
							}}
							{	/* Ast/build.scm 14 */
								int BgL_auxz00_1707;

								BgL_auxz00_1707 = (int) ((BgL_iz00_1702 - 1L));
								{
									long BgL_iz00_1745;

									BgL_iz00_1745 = (long) (BgL_auxz00_1707);
									BgL_iz00_1702 = BgL_iz00_1745;
									goto BgL_loopz00_1703;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_buildz00(void)
	{
		{	/* Ast/build.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzast_buildz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1349;

				BgL_headz00_1349 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1350;
					obj_t BgL_tailz00_1351;

					BgL_prevz00_1350 = BgL_headz00_1349;
					BgL_tailz00_1351 = BgL_l1z00_1;
				BgL_loopz00_1352:
					if (PAIRP(BgL_tailz00_1351))
						{
							obj_t BgL_newzd2prevzd2_1354;

							BgL_newzd2prevzd2_1354 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1351), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1350, BgL_newzd2prevzd2_1354);
							{
								obj_t BgL_tailz00_1755;
								obj_t BgL_prevz00_1754;

								BgL_prevz00_1754 = BgL_newzd2prevzd2_1354;
								BgL_tailz00_1755 = CDR(BgL_tailz00_1351);
								BgL_tailz00_1351 = BgL_tailz00_1755;
								BgL_prevz00_1350 = BgL_prevz00_1754;
								goto BgL_loopz00_1352;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1349);
				}
			}
		}

	}



/* append-ast */
	BGL_EXPORTED_DEF obj_t BGl_appendzd2astzd2zzast_buildz00(obj_t BgL_a1z00_3,
		obj_t BgL_a2z00_4)
	{
		{	/* Ast/build.scm 35 */
			BGL_TAIL return
				BGl_appendzd221011zd2zzast_buildz00(BgL_a1z00_3, BgL_a2z00_4);
		}

	}



/* &append-ast */
	obj_t BGl_z62appendzd2astzb0zzast_buildz00(obj_t BgL_envz00_1692,
		obj_t BgL_a1z00_1693, obj_t BgL_a2z00_1694)
	{
		{	/* Ast/build.scm 35 */
			return BGl_appendzd2astzd2zzast_buildz00(BgL_a1z00_1693, BgL_a2z00_1694);
		}

	}



/* build-ast */
	BGL_EXPORTED_DEF obj_t BGl_buildzd2astzd2zzast_buildz00(obj_t BgL_unitsz00_5)
	{
		{	/* Ast/build.scm 43 */
			{	/* Ast/build.scm 44 */
				obj_t BgL_arg1252z00_1635;

				BgL_arg1252z00_1635 =
					BGl_buildzd2astzd2sanszd2removezd2zzast_buildz00(BgL_unitsz00_5);
				return
					BGl_removezd2varzd2zzast_removez00(CNST_TABLE_REF(0),
					BgL_arg1252z00_1635);
			}
		}

	}



/* &build-ast */
	obj_t BGl_z62buildzd2astzb0zzast_buildz00(obj_t BgL_envz00_1695,
		obj_t BgL_unitsz00_1696)
	{
		{	/* Ast/build.scm 43 */
			return BGl_buildzd2astzd2zzast_buildz00(BgL_unitsz00_1696);
		}

	}



/* build-ast-sans-remove */
	BGL_EXPORTED_DEF obj_t BGl_buildzd2astzd2sanszd2removezd2zzast_buildz00(obj_t
		BgL_unitsz00_6)
	{
		{	/* Ast/build.scm 51 */
			{	/* Ast/build.scm 52 */
				obj_t BgL_list1253z00_1358;

				{	/* Ast/build.scm 52 */
					obj_t BgL_arg1268z00_1359;

					{	/* Ast/build.scm 52 */
						obj_t BgL_arg1272z00_1360;

						BgL_arg1272z00_1360 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1268z00_1359 =
							MAKE_YOUNG_PAIR(BGl_string1631z00zzast_buildz00,
							BgL_arg1272z00_1360);
					}
					BgL_list1253z00_1358 =
						MAKE_YOUNG_PAIR(BGl_string1632z00zzast_buildz00,
						BgL_arg1268z00_1359);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1253z00_1358);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string1631z00zzast_buildz00;
			{	/* Ast/build.scm 52 */
				obj_t BgL_g1106z00_1361;

				BgL_g1106z00_1361 = BNIL;
				{
					obj_t BgL_hooksz00_1364;
					obj_t BgL_hnamesz00_1365;

					BgL_hooksz00_1364 = BgL_g1106z00_1361;
					BgL_hnamesz00_1365 = BNIL;
				BgL_zc3z04anonymousza31273ze3z87_1366:
					if (NULLP(BgL_hooksz00_1364))
						{	/* Ast/build.scm 52 */
							CNST_TABLE_REF(1);
						}
					else
						{	/* Ast/build.scm 52 */
							bool_t BgL_test1661z00_1774;

							{	/* Ast/build.scm 52 */
								obj_t BgL_fun1306z00_1373;

								BgL_fun1306z00_1373 = CAR(((obj_t) BgL_hooksz00_1364));
								BgL_test1661z00_1774 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1306z00_1373));
							}
							if (BgL_test1661z00_1774)
								{	/* Ast/build.scm 52 */
									obj_t BgL_arg1284z00_1370;
									obj_t BgL_arg1304z00_1371;

									BgL_arg1284z00_1370 = CDR(((obj_t) BgL_hooksz00_1364));
									BgL_arg1304z00_1371 = CDR(((obj_t) BgL_hnamesz00_1365));
									{
										obj_t BgL_hnamesz00_1786;
										obj_t BgL_hooksz00_1785;

										BgL_hooksz00_1785 = BgL_arg1284z00_1370;
										BgL_hnamesz00_1786 = BgL_arg1304z00_1371;
										BgL_hnamesz00_1365 = BgL_hnamesz00_1786;
										BgL_hooksz00_1364 = BgL_hooksz00_1785;
										goto BgL_zc3z04anonymousza31273ze3z87_1366;
									}
								}
							else
								{	/* Ast/build.scm 52 */
									obj_t BgL_arg1305z00_1372;

									BgL_arg1305z00_1372 = CAR(((obj_t) BgL_hnamesz00_1365));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string1631z00zzast_buildz00,
										BGl_string1633z00zzast_buildz00, BgL_arg1305z00_1372);
								}
						}
				}
			}
			{	/* Ast/build.scm 56 */
				obj_t BgL_nberrz00_1376;

				BgL_nberrz00_1376 = BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
				{	/* Ast/build.scm 56 */
					obj_t BgL_defsz00_1377;

					{	/* Ast/build.scm 57 */
						obj_t BgL_runner1372z00_1458;

						if (NULLP(BgL_unitsz00_6))
							{	/* Ast/build.scm 57 */
								BgL_runner1372z00_1458 = BNIL;
							}
						else
							{	/* Ast/build.scm 57 */
								obj_t BgL_head1235z00_1444;

								{	/* Ast/build.scm 57 */
									obj_t BgL_arg1370z00_1456;

									{	/* Ast/build.scm 57 */
										obj_t BgL_arg1371z00_1457;

										BgL_arg1371z00_1457 = CAR(((obj_t) BgL_unitsz00_6));
										BgL_arg1370z00_1456 =
											BGl_unitzd2ze3defsz31zzast_unitz00(BgL_arg1371z00_1457);
									}
									BgL_head1235z00_1444 =
										MAKE_YOUNG_PAIR(BgL_arg1370z00_1456, BNIL);
								}
								{	/* Ast/build.scm 57 */
									obj_t BgL_g1238z00_1445;

									BgL_g1238z00_1445 = CDR(((obj_t) BgL_unitsz00_6));
									{
										obj_t BgL_l1233z00_1447;
										obj_t BgL_tail1236z00_1448;

										BgL_l1233z00_1447 = BgL_g1238z00_1445;
										BgL_tail1236z00_1448 = BgL_head1235z00_1444;
									BgL_zc3z04anonymousza31351ze3z87_1449:
										if (NULLP(BgL_l1233z00_1447))
											{	/* Ast/build.scm 57 */
												BgL_runner1372z00_1458 = BgL_head1235z00_1444;
											}
										else
											{	/* Ast/build.scm 57 */
												obj_t BgL_newtail1237z00_1451;

												{	/* Ast/build.scm 57 */
													obj_t BgL_arg1364z00_1453;

													{	/* Ast/build.scm 57 */
														obj_t BgL_arg1367z00_1454;

														BgL_arg1367z00_1454 =
															CAR(((obj_t) BgL_l1233z00_1447));
														BgL_arg1364z00_1453 =
															BGl_unitzd2ze3defsz31zzast_unitz00
															(BgL_arg1367z00_1454);
													}
													BgL_newtail1237z00_1451 =
														MAKE_YOUNG_PAIR(BgL_arg1364z00_1453, BNIL);
												}
												SET_CDR(BgL_tail1236z00_1448, BgL_newtail1237z00_1451);
												{	/* Ast/build.scm 57 */
													obj_t BgL_arg1361z00_1452;

													BgL_arg1361z00_1452 =
														CDR(((obj_t) BgL_l1233z00_1447));
													{
														obj_t BgL_tail1236z00_1808;
														obj_t BgL_l1233z00_1807;

														BgL_l1233z00_1807 = BgL_arg1361z00_1452;
														BgL_tail1236z00_1808 = BgL_newtail1237z00_1451;
														BgL_tail1236z00_1448 = BgL_tail1236z00_1808;
														BgL_l1233z00_1447 = BgL_l1233z00_1807;
														goto BgL_zc3z04anonymousza31351ze3z87_1449;
													}
												}
											}
									}
								}
							}
						BgL_defsz00_1377 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00
							(BgL_runner1372z00_1458);
					}
					{	/* Ast/build.scm 57 */

						if (
							((long) CINT(BgL_nberrz00_1376) ==
								(long)
								CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)))
							{	/* Ast/build.scm 58 */
								BGl_checkzd2tozd2bezd2definezd2zzast_findzd2gdefszd2();
								{	/* Ast/build.scm 63 */
									obj_t BgL_astz00_1379;

									if (NULLP(BgL_defsz00_1377))
										{	/* Ast/build.scm 63 */
											BgL_astz00_1379 = BNIL;
										}
									else
										{	/* Ast/build.scm 63 */
											obj_t BgL_head1241z00_1405;

											{	/* Ast/build.scm 63 */
												BgL_globalz00_bglt BgL_arg1329z00_1417;

												{	/* Ast/build.scm 63 */
													obj_t BgL_arg1331z00_1418;

													BgL_arg1331z00_1418 = CAR(((obj_t) BgL_defsz00_1377));
													BgL_arg1329z00_1417 =
														BGl_sfunzd2defzd2ze3astze3zzast_buildz00(
														((BgL_globalz00_bglt) BgL_arg1331z00_1418));
												}
												BgL_head1241z00_1405 =
													MAKE_YOUNG_PAIR(((obj_t) BgL_arg1329z00_1417), BNIL);
											}
											{	/* Ast/build.scm 63 */
												obj_t BgL_g1244z00_1406;

												BgL_g1244z00_1406 = CDR(((obj_t) BgL_defsz00_1377));
												{
													obj_t BgL_l1239z00_1408;
													obj_t BgL_tail1242z00_1409;

													BgL_l1239z00_1408 = BgL_g1244z00_1406;
													BgL_tail1242z00_1409 = BgL_head1241z00_1405;
												BgL_zc3z04anonymousza31324ze3z87_1410:
													if (NULLP(BgL_l1239z00_1408))
														{	/* Ast/build.scm 63 */
															BgL_astz00_1379 = BgL_head1241z00_1405;
														}
													else
														{	/* Ast/build.scm 63 */
															obj_t BgL_newtail1243z00_1412;

															{	/* Ast/build.scm 63 */
																BgL_globalz00_bglt BgL_arg1327z00_1414;

																{	/* Ast/build.scm 63 */
																	obj_t BgL_arg1328z00_1415;

																	BgL_arg1328z00_1415 =
																		CAR(((obj_t) BgL_l1239z00_1408));
																	BgL_arg1327z00_1414 =
																		BGl_sfunzd2defzd2ze3astze3zzast_buildz00(
																		((BgL_globalz00_bglt) BgL_arg1328z00_1415));
																}
																BgL_newtail1243z00_1412 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_arg1327z00_1414), BNIL);
															}
															SET_CDR(BgL_tail1242z00_1409,
																BgL_newtail1243z00_1412);
															{	/* Ast/build.scm 63 */
																obj_t BgL_arg1326z00_1413;

																BgL_arg1326z00_1413 =
																	CDR(((obj_t) BgL_l1239z00_1408));
																{
																	obj_t BgL_tail1242z00_1837;
																	obj_t BgL_l1239z00_1836;

																	BgL_l1239z00_1836 = BgL_arg1326z00_1413;
																	BgL_tail1242z00_1837 =
																		BgL_newtail1243z00_1412;
																	BgL_tail1242z00_1409 = BgL_tail1242z00_1837;
																	BgL_l1239z00_1408 = BgL_l1239z00_1836;
																	goto BgL_zc3z04anonymousza31324ze3z87_1410;
																}
															}
														}
												}
											}
										}
									if (
										((long)
											CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
											> 0L))
										{	/* Ast/build.scm 65 */
											{	/* Ast/build.scm 65 */
												obj_t BgL_port1245z00_1382;

												{	/* Ast/build.scm 65 */
													obj_t BgL_tmpz00_1841;

													BgL_tmpz00_1841 = BGL_CURRENT_DYNAMIC_ENV();
													BgL_port1245z00_1382 =
														BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_1841);
												}
												bgl_display_obj
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
													BgL_port1245z00_1382);
												bgl_display_string(BGl_string1634z00zzast_buildz00,
													BgL_port1245z00_1382);
												{	/* Ast/build.scm 65 */
													obj_t BgL_arg1310z00_1383;

													{	/* Ast/build.scm 65 */
														bool_t BgL_test1669z00_1846;

														if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
															(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
															{	/* Ast/build.scm 65 */
																if (INTEGERP
																	(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
																	{	/* Ast/build.scm 65 */
																		BgL_test1669z00_1846 =
																			(
																			(long)
																			CINT
																			(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
																			> 1L);
																	}
																else
																	{	/* Ast/build.scm 65 */
																		BgL_test1669z00_1846 =
																			BGl_2ze3ze3zz__r4_numbers_6_5z00
																			(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
																			BINT(1L));
																	}
															}
														else
															{	/* Ast/build.scm 65 */
																BgL_test1669z00_1846 = ((bool_t) 0);
															}
														if (BgL_test1669z00_1846)
															{	/* Ast/build.scm 65 */
																BgL_arg1310z00_1383 =
																	BGl_string1635z00zzast_buildz00;
															}
														else
															{	/* Ast/build.scm 65 */
																BgL_arg1310z00_1383 =
																	BGl_string1636z00zzast_buildz00;
															}
													}
													bgl_display_obj(BgL_arg1310z00_1383,
														BgL_port1245z00_1382);
												}
												bgl_display_string(BGl_string1637z00zzast_buildz00,
													BgL_port1245z00_1382);
												bgl_display_char(((unsigned char) 10),
													BgL_port1245z00_1382);
											}
											{	/* Ast/build.scm 65 */
												obj_t BgL_list1313z00_1387;

												BgL_list1313z00_1387 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
												BGL_TAIL return
													BGl_exitz00zz__errorz00(BgL_list1313z00_1387);
											}
										}
									else
										{	/* Ast/build.scm 65 */
											obj_t BgL_g1108z00_1388;

											BgL_g1108z00_1388 = BNIL;
											{
												obj_t BgL_hooksz00_1391;
												obj_t BgL_hnamesz00_1392;

												BgL_hooksz00_1391 = BgL_g1108z00_1388;
												BgL_hnamesz00_1392 = BNIL;
											BgL_zc3z04anonymousza31314ze3z87_1393:
												if (NULLP(BgL_hooksz00_1391))
													{	/* Ast/build.scm 65 */
														return BgL_astz00_1379;
													}
												else
													{	/* Ast/build.scm 65 */
														bool_t BgL_test1673z00_1863;

														{	/* Ast/build.scm 65 */
															obj_t BgL_fun1321z00_1400;

															BgL_fun1321z00_1400 =
																CAR(((obj_t) BgL_hooksz00_1391));
															BgL_test1673z00_1863 =
																CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1321z00_1400));
														}
														if (BgL_test1673z00_1863)
															{	/* Ast/build.scm 65 */
																obj_t BgL_arg1318z00_1397;
																obj_t BgL_arg1319z00_1398;

																BgL_arg1318z00_1397 =
																	CDR(((obj_t) BgL_hooksz00_1391));
																BgL_arg1319z00_1398 =
																	CDR(((obj_t) BgL_hnamesz00_1392));
																{
																	obj_t BgL_hnamesz00_1875;
																	obj_t BgL_hooksz00_1874;

																	BgL_hooksz00_1874 = BgL_arg1318z00_1397;
																	BgL_hnamesz00_1875 = BgL_arg1319z00_1398;
																	BgL_hnamesz00_1392 = BgL_hnamesz00_1875;
																	BgL_hooksz00_1391 = BgL_hooksz00_1874;
																	goto BgL_zc3z04anonymousza31314ze3z87_1393;
																}
															}
														else
															{	/* Ast/build.scm 65 */
																obj_t BgL_arg1320z00_1399;

																BgL_arg1320z00_1399 =
																	CAR(((obj_t) BgL_hnamesz00_1392));
																return
																	BGl_internalzd2errorzd2zztools_errorz00
																	(BGl_za2currentzd2passza2zd2zzengine_passz00,
																	BGl_string1638z00zzast_buildz00,
																	BgL_arg1320z00_1399);
															}
													}
											}
										}
								}
							}
						else
							{	/* Ast/build.scm 58 */
								if (
									((long)
										CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) >
										0L))
									{	/* Ast/build.scm 66 */
										{	/* Ast/build.scm 66 */
											obj_t BgL_port1246z00_1421;

											{	/* Ast/build.scm 66 */
												obj_t BgL_tmpz00_1882;

												BgL_tmpz00_1882 = BGL_CURRENT_DYNAMIC_ENV();
												BgL_port1246z00_1421 =
													BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_1882);
											}
											bgl_display_obj
												(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
												BgL_port1246z00_1421);
											bgl_display_string(BGl_string1634z00zzast_buildz00,
												BgL_port1246z00_1421);
											{	/* Ast/build.scm 66 */
												obj_t BgL_arg1333z00_1422;

												{	/* Ast/build.scm 66 */
													bool_t BgL_test1675z00_1887;

													if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
														(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
														{	/* Ast/build.scm 66 */
															if (INTEGERP
																(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
																{	/* Ast/build.scm 66 */
																	BgL_test1675z00_1887 =
																		(
																		(long)
																		CINT
																		(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
																		> 1L);
																}
															else
																{	/* Ast/build.scm 66 */
																	BgL_test1675z00_1887 =
																		BGl_2ze3ze3zz__r4_numbers_6_5z00
																		(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
																		BINT(1L));
																}
														}
													else
														{	/* Ast/build.scm 66 */
															BgL_test1675z00_1887 = ((bool_t) 0);
														}
													if (BgL_test1675z00_1887)
														{	/* Ast/build.scm 66 */
															BgL_arg1333z00_1422 =
																BGl_string1635z00zzast_buildz00;
														}
													else
														{	/* Ast/build.scm 66 */
															BgL_arg1333z00_1422 =
																BGl_string1636z00zzast_buildz00;
														}
												}
												bgl_display_obj(BgL_arg1333z00_1422,
													BgL_port1246z00_1421);
											}
											bgl_display_string(BGl_string1637z00zzast_buildz00,
												BgL_port1246z00_1421);
											bgl_display_char(((unsigned char) 10),
												BgL_port1246z00_1421);
										}
										{	/* Ast/build.scm 66 */
											obj_t BgL_list1336z00_1426;

											BgL_list1336z00_1426 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
											BGL_TAIL return
												BGl_exitz00zz__errorz00(BgL_list1336z00_1426);
										}
									}
								else
									{	/* Ast/build.scm 66 */
										obj_t BgL_g1110z00_1427;

										BgL_g1110z00_1427 = BNIL;
										{
											obj_t BgL_hooksz00_1430;
											obj_t BgL_hnamesz00_1431;

											BgL_hooksz00_1430 = BgL_g1110z00_1427;
											BgL_hnamesz00_1431 = BNIL;
										BgL_zc3z04anonymousza31337ze3z87_1432:
											if (NULLP(BgL_hooksz00_1430))
												{	/* Ast/build.scm 66 */
													return BNIL;
												}
											else
												{	/* Ast/build.scm 66 */
													bool_t BgL_test1681z00_1904;

													{	/* Ast/build.scm 66 */
														obj_t BgL_fun1347z00_1439;

														BgL_fun1347z00_1439 =
															CAR(((obj_t) BgL_hooksz00_1430));
														BgL_test1681z00_1904 =
															CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1347z00_1439));
													}
													if (BgL_test1681z00_1904)
														{	/* Ast/build.scm 66 */
															obj_t BgL_arg1342z00_1436;
															obj_t BgL_arg1343z00_1437;

															BgL_arg1342z00_1436 =
																CDR(((obj_t) BgL_hooksz00_1430));
															BgL_arg1343z00_1437 =
																CDR(((obj_t) BgL_hnamesz00_1431));
															{
																obj_t BgL_hnamesz00_1916;
																obj_t BgL_hooksz00_1915;

																BgL_hooksz00_1915 = BgL_arg1342z00_1436;
																BgL_hnamesz00_1916 = BgL_arg1343z00_1437;
																BgL_hnamesz00_1431 = BgL_hnamesz00_1916;
																BgL_hooksz00_1430 = BgL_hooksz00_1915;
																goto BgL_zc3z04anonymousza31337ze3z87_1432;
															}
														}
													else
														{	/* Ast/build.scm 66 */
															obj_t BgL_arg1346z00_1438;

															BgL_arg1346z00_1438 =
																CAR(((obj_t) BgL_hnamesz00_1431));
															return
																BGl_internalzd2errorzd2zztools_errorz00
																(BGl_za2currentzd2passza2zd2zzengine_passz00,
																BGl_string1638z00zzast_buildz00,
																BgL_arg1346z00_1438);
														}
												}
										}
									}
							}
					}
				}
			}
		}

	}



/* &build-ast-sans-remove */
	obj_t BGl_z62buildzd2astzd2sanszd2removezb0zzast_buildz00(obj_t
		BgL_envz00_1697, obj_t BgL_unitsz00_1698)
	{
		{	/* Ast/build.scm 51 */
			return
				BGl_buildzd2astzd2sanszd2removezd2zzast_buildz00(BgL_unitsz00_1698);
		}

	}



/* sfun-def->ast */
	BgL_globalz00_bglt BGl_sfunzd2defzd2ze3astze3zzast_buildz00(BgL_globalz00_bglt
		BgL_defz00_7)
	{
		{	/* Ast/build.scm 71 */
			{	/* Ast/build.scm 72 */
				obj_t BgL_arg1375z00_1459;

				BgL_arg1375z00_1459 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_defz00_7)))->BgL_idz00);
				BGl_enterzd2functionzd2zztools_errorz00(BgL_arg1375z00_1459);
			}
			{	/* Ast/build.scm 73 */
				obj_t BgL_exitd1112z00_1460;

				BgL_exitd1112z00_1460 = BGL_EXITD_TOP_AS_OBJ();
				{	/* Ast/build.scm 73 */
					obj_t BgL_arg1828z00_1680;

					{	/* Ast/build.scm 73 */
						obj_t BgL_arg1829z00_1681;

						BgL_arg1829z00_1681 = BGL_EXITD_PROTECT(BgL_exitd1112z00_1460);
						BgL_arg1828z00_1680 =
							MAKE_YOUNG_PAIR(BGl_proc1639z00zzast_buildz00,
							BgL_arg1829z00_1681);
					}
					BGL_EXITD_PROTECT_SET(BgL_exitd1112z00_1460, BgL_arg1828z00_1680);
					BUNSPEC;
				}
				{	/* Ast/build.scm 74 */
					obj_t BgL_tmp1114z00_1462;

					{	/* Ast/build.scm 74 */
						BgL_valuez00_bglt BgL_sfunz00_1463;

						BgL_sfunz00_1463 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_defz00_7)))->BgL_valuez00);
						{	/* Ast/build.scm 74 */
							obj_t BgL_sfunzd2argszd2_1464;

							BgL_sfunzd2argszd2_1464 =
								(((BgL_sfunz00_bglt) COBJECT(
										((BgL_sfunz00_bglt) BgL_sfunz00_1463)))->BgL_argsz00);
							{	/* Ast/build.scm 75 */
								obj_t BgL_sfunzd2bodyzd2expz00_1465;

								BgL_sfunzd2bodyzd2expz00_1465 =
									(((BgL_sfunz00_bglt) COBJECT(
											((BgL_sfunz00_bglt) BgL_sfunz00_1463)))->BgL_bodyz00);
								{	/* Ast/build.scm 76 */
									obj_t BgL_defzd2loczd2_1466;

									BgL_defzd2loczd2_1466 =
										BGl_findzd2locationzd2zztools_locationz00(
										(((BgL_globalz00_bglt) COBJECT(BgL_defz00_7))->BgL_srcz00));
									{	/* Ast/build.scm 77 */
										obj_t BgL_locz00_1467;

										BgL_locz00_1467 =
											BGl_findzd2locationzf2locz20zztools_locationz00
											(BgL_sfunzd2bodyzd2expz00_1465, BgL_defzd2loczd2_1466);
										{	/* Ast/build.scm 78 */
											BgL_nodez00_bglt BgL_bodyz00_1468;

											BgL_bodyz00_1468 =
												BGl_sexpzd2ze3nodez31zzast_sexpz00
												(BgL_sfunzd2bodyzd2expz00_1465, BgL_sfunzd2argszd2_1464,
												BgL_locz00_1467, CNST_TABLE_REF(2));
											{	/* Ast/build.scm 79 */

												BgL_tmp1114z00_1462 =
													((((BgL_sfunz00_bglt) COBJECT(
																((BgL_sfunz00_bglt) BgL_sfunz00_1463)))->
														BgL_bodyz00) =
													((obj_t) ((obj_t) BgL_bodyz00_1468)), BUNSPEC);
											}
										}
									}
								}
							}
						}
					}
					{	/* Ast/build.scm 73 */
						bool_t BgL_test1682z00_1942;

						{	/* Ast/build.scm 73 */
							obj_t BgL_arg1827z00_1688;

							BgL_arg1827z00_1688 = BGL_EXITD_PROTECT(BgL_exitd1112z00_1460);
							BgL_test1682z00_1942 = PAIRP(BgL_arg1827z00_1688);
						}
						if (BgL_test1682z00_1942)
							{	/* Ast/build.scm 73 */
								obj_t BgL_arg1825z00_1689;

								{	/* Ast/build.scm 73 */
									obj_t BgL_arg1826z00_1690;

									BgL_arg1826z00_1690 =
										BGL_EXITD_PROTECT(BgL_exitd1112z00_1460);
									BgL_arg1825z00_1689 = CDR(((obj_t) BgL_arg1826z00_1690));
								}
								BGL_EXITD_PROTECT_SET(BgL_exitd1112z00_1460,
									BgL_arg1825z00_1689);
								BUNSPEC;
							}
						else
							{	/* Ast/build.scm 73 */
								BFALSE;
							}
					}
					BGl_leavezd2functionzd2zztools_errorz00();
					BgL_tmp1114z00_1462;
				}
			}
			return BgL_defz00_7;
		}

	}



/* &<@anonymous:1377> */
	obj_t BGl_z62zc3z04anonymousza31377ze3ze5zzast_buildz00(obj_t BgL_envz00_1700)
	{
		{	/* Ast/build.scm 73 */
			return BGl_leavezd2functionzd2zztools_errorz00();
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_buildz00(void)
	{
		{	/* Ast/build.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_buildz00(void)
	{
		{	/* Ast/build.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_buildz00(void)
	{
		{	/* Ast/build.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_buildz00(void)
	{
		{	/* Ast/build.scm 14 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1640z00zzast_buildz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1640z00zzast_buildz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1640z00zzast_buildz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1640z00zzast_buildz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1640z00zzast_buildz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1640z00zzast_buildz00));
			BGl_modulezd2initializa7ationz75zzast_unitz00(234044111L,
				BSTRING_TO_STRING(BGl_string1640z00zzast_buildz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1640z00zzast_buildz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1640z00zzast_buildz00));
			BGl_modulezd2initializa7ationz75zzast_findzd2gdefszd2(502577483L,
				BSTRING_TO_STRING(BGl_string1640z00zzast_buildz00));
			BGl_modulezd2initializa7ationz75zzast_removez00(383247627L,
				BSTRING_TO_STRING(BGl_string1640z00zzast_buildz00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string1640z00zzast_buildz00));
			BGl_modulezd2initializa7ationz75zztools_argsz00(47102372L,
				BSTRING_TO_STRING(BGl_string1640z00zzast_buildz00));
			BGl_modulezd2initializa7ationz75zztools_prognz00(301998274L,
				BSTRING_TO_STRING(BGl_string1640z00zzast_buildz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1640z00zzast_buildz00));
			return
				BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1640z00zzast_buildz00));
		}

	}

#ifdef __cplusplus
}
#endif
