/*===========================================================================*/
/*   (Ast/hrtype.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/hrtype.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_AST_HRTYPE_TYPE_DEFINITIONS
#define BGL_AST_HRTYPE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_getfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_getfieldz00_bglt;

	typedef struct BgL_setfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_setfieldz00_bglt;

	typedef struct BgL_wideningz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_wideningz00_bglt;

	typedef struct BgL_newz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_argszd2typezd2;
	}             *BgL_newz00_bglt;

	typedef struct BgL_vallocz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                *BgL_vallocz00_bglt;

	typedef struct BgL_vrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}              *BgL_vrefz00_bglt;

	typedef struct BgL_vsetz12z12_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}                 *BgL_vsetz12z12_bglt;

	typedef struct BgL_vlengthz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_ftypez00;
	}                 *BgL_vlengthz00_bglt;

	typedef struct BgL_instanceofz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_classz00;
	}                    *BgL_instanceofz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;


#endif													// BGL_AST_HRTYPE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t BGl_requirezd2initializa7ationz75zzast_hrtypez00 = BUNSPEC;
	extern obj_t BGl_newz00zzast_nodez00;
	static obj_t BGl_z62hrtypezd2nodez12zd2vlength1297z70zzast_hrtypez00(obj_t,
		obj_t);
	static obj_t BGl_z62hrtypezd2nodez12zd2cast1301z70zzast_hrtypez00(obj_t,
		obj_t);
	extern obj_t BGl_vlengthz00zzast_nodez00;
	extern obj_t BGl_syncz00zzast_nodez00;
	static obj_t BGl_z62hrtypezd2nodez12zd2var1269z70zzast_hrtypez00(obj_t,
		obj_t);
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_alreadyzd2restoredzf3z21zzast_envz00(obj_t);
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t BGl_z62hrtypezd2nodez12zd2setq1303z70zzast_hrtypez00(obj_t,
		obj_t);
	static obj_t BGl_toplevelzd2initzd2zzast_hrtypez00(void);
	static obj_t BGl_z62hrtypezd2nodez12zd2vsetz121295z62zzast_hrtypez00(obj_t,
		obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static obj_t BGl_z62hrtypezd2nodez12zd2fail1307z70zzast_hrtypez00(obj_t,
		obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzast_hrtypez00(void);
	extern obj_t BGl_getfieldz00zzast_nodez00;
	static obj_t BGl_objectzd2initzd2zzast_hrtypez00(void);
	extern obj_t BGl_castz00zzast_nodez00;
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	extern obj_t BGl_typez00zztype_typez00;
	static obj_t
		BGl_z62hrtypezd2nodez12zd2setzd2exzd21315z70zzast_hrtypez00(obj_t, obj_t);
	static obj_t BGl_z62hrtypezd2nodez12zd2app1275z70zzast_hrtypez00(obj_t,
		obj_t);
	static obj_t BGl_z62hrtypezd2nodez12zd2conditi1305z70zzast_hrtypez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzast_hrtypez00(void);
	static obj_t BGl_z62hrtypezd2nodez12zd2letzd2var1313za2zzast_hrtypez00(obj_t,
		obj_t);
	static obj_t BGl_z62hrtypezd2nodez12zd2sync1273z70zzast_hrtypez00(obj_t,
		obj_t);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t BGl_z62hrtypezd2nodez12zd2boxzd2set1321za2zzast_hrtypez00(obj_t,
		obj_t);
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	static obj_t BGl_z62hrtypezd2nodez12zd2boxzd2ref1323za2zzast_hrtypez00(obj_t,
		obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	extern obj_t BGl_restorezd2globalz12zc0zzast_envz00(obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62hrtypezd2nodez12zd2funcall1279z70zzast_hrtypez00(obj_t,
		obj_t);
	static bool_t BGl_hrtypezd2nodeza2z12z62zzast_hrtypez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_hrtypezd2nodez12zc0zzast_hrtypez00(BgL_nodez00_bglt);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzast_hrtypez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_cfunz00zzast_varz00;
	static obj_t BGl_z62hrtypezd2nodez12zd2makezd2bo1319za2zzast_hrtypez00(obj_t,
		obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	extern obj_t BGl_wideningz00zzast_nodez00;
	static obj_t BGl_z62hrtypezd2nodez12zd2widenin1287z70zzast_hrtypez00(obj_t,
		obj_t);
	extern obj_t BGl_vrefz00zzast_nodez00;
	static obj_t BGl_z62hrtypezd2nodez12zd2setfiel1285z70zzast_hrtypez00(obj_t,
		obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzast_hrtypez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_hrtypez00(void);
	extern obj_t BGl_globalzd2bucketzd2positionz00zzast_envz00(obj_t, obj_t);
	static obj_t BGl_z62hrtypezd2nodez12zd2sequenc1271z70zzast_hrtypez00(obj_t,
		obj_t);
	static obj_t BGl_z62hrtypezd2nodez12zd2instanc1299z70zzast_hrtypez00(obj_t,
		obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzast_hrtypez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzast_hrtypez00(void);
	extern obj_t BGl_vallocz00zzast_nodez00;
	static obj_t BGl_z62hrtypezd2nodez12zd2vref1293z70zzast_hrtypez00(obj_t,
		obj_t);
	static obj_t BGl_z62hrtypezd2nodez12zd2valloc1291z70zzast_hrtypez00(obj_t,
		obj_t);
	extern obj_t BGl_findzd2globalzf2modulez20zzast_envz00(obj_t, obj_t);
	extern obj_t BGl_instanceofz00zzast_nodez00;
	static obj_t BGl_z62hrtypezd2nodez12zd2switch1309z70zzast_hrtypez00(obj_t,
		obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	static obj_t BGl_z62hrtypezd2nodez12zd2appzd2ly1277za2zzast_hrtypez00(obj_t,
		obj_t);
	extern obj_t BGl_setfieldz00zzast_nodez00;
	extern BgL_typez00_bglt BGl_findzd2typezd2zztype_envz00(obj_t);
	static obj_t BGl_z62hrtypezd2nodez12zd2jumpzd2ex1317za2zzast_hrtypez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_variablez00zzast_varz00;
	extern obj_t BGl_switchz00zzast_nodez00;
	static obj_t BGl_z62hrtypezd2nodez12zd2getfiel1283z70zzast_hrtypez00(obj_t,
		obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t BGl_z62hrtypezd2nodez12zd2new1289z70zzast_hrtypez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t
		BGl_findzd2superzd2classzd2methodzd2zz__objectz00(BgL_objectz00_bglt, obj_t,
		obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_globalz00zzast_varz00;
	extern obj_t BGl_vsetz12z12zzast_nodez00;
	static obj_t BGl_z62hrtypezd2nodez12zd2letzd2fun1311za2zzast_hrtypez00(obj_t,
		obj_t);
	static obj_t BGl_z62hrtypezd2nodez12zd2extern1281z70zzast_hrtypez00(obj_t,
		obj_t);
	static obj_t BGl_z62hrtypezd2nodez121266za2zzast_hrtypez00(obj_t, obj_t);
	static obj_t
		BGl_restorezd2variablezd2typez12z12zzast_hrtypez00(BgL_variablez00_bglt);
	static obj_t BGl_z62hrtypezd2nodez12za2zzast_hrtypez00(obj_t, obj_t);
	static obj_t __cnst[2];


	   
		 
		DEFINE_STRING(BGl_string1911z00zzast_hrtypez00,
		BgL_bgl_string1911za700za7za7a1947za7, "hrtype-node!1266", 16);
	      DEFINE_STRING(BGl_string1913z00zzast_hrtypez00,
		BgL_bgl_string1913za700za7za7a1948za7, "hrtype-node!", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1910z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1949z00,
		BGl_z62hrtypezd2nodez121266za2zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1912z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1950z00,
		BGl_z62hrtypezd2nodez12zd2var1269z70zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1914z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1951z00,
		BGl_z62hrtypezd2nodez12zd2sequenc1271z70zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1915z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1952z00,
		BGl_z62hrtypezd2nodez12zd2sync1273z70zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1916z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1953z00,
		BGl_z62hrtypezd2nodez12zd2app1275z70zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1917z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1954z00,
		BGl_z62hrtypezd2nodez12zd2appzd2ly1277za2zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1918z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1955z00,
		BGl_z62hrtypezd2nodez12zd2funcall1279z70zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1919z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1956z00,
		BGl_z62hrtypezd2nodez12zd2extern1281z70zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1920z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1957z00,
		BGl_z62hrtypezd2nodez12zd2getfiel1283z70zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1921z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1958z00,
		BGl_z62hrtypezd2nodez12zd2setfiel1285z70zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1922z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1959z00,
		BGl_z62hrtypezd2nodez12zd2widenin1287z70zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1923z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1960z00,
		BGl_z62hrtypezd2nodez12zd2new1289z70zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1924z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1961z00,
		BGl_z62hrtypezd2nodez12zd2valloc1291z70zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1925z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1962z00,
		BGl_z62hrtypezd2nodez12zd2vref1293z70zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1926z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1963z00,
		BGl_z62hrtypezd2nodez12zd2vsetz121295z62zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1927z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1964z00,
		BGl_z62hrtypezd2nodez12zd2vlength1297z70zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1928z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1965z00,
		BGl_z62hrtypezd2nodez12zd2instanc1299z70zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1929z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1966z00,
		BGl_z62hrtypezd2nodez12zd2cast1301z70zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1930z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1967z00,
		BGl_z62hrtypezd2nodez12zd2setq1303z70zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1931z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1968z00,
		BGl_z62hrtypezd2nodez12zd2conditi1305z70zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1932z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1969z00,
		BGl_z62hrtypezd2nodez12zd2fail1307z70zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1933z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1970z00,
		BGl_z62hrtypezd2nodez12zd2switch1309z70zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1934z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1971z00,
		BGl_z62hrtypezd2nodez12zd2letzd2fun1311za2zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1941z00zzast_hrtypez00,
		BgL_bgl_string1941za700za7za7a1972za7, "Illegal argument", 16);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1935z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1973z00,
		BGl_z62hrtypezd2nodez12zd2letzd2var1313za2zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1942z00zzast_hrtypez00,
		BgL_bgl_string1942za700za7za7a1974za7, "heap", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1936z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1975z00,
		BGl_z62hrtypezd2nodez12zd2setzd2exzd21315z70zzast_hrtypez00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string1943z00zzast_hrtypez00,
		BgL_bgl_string1943za700za7za7a1976za7, "Can't find library variable", 27);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1937z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1977z00,
		BGl_z62hrtypezd2nodez12zd2jumpzd2ex1317za2zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1944z00zzast_hrtypez00,
		BgL_bgl_string1944za700za7za7a1978za7, "ast_hrtype", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1938z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1979z00,
		BGl_z62hrtypezd2nodez12zd2makezd2bo1319za2zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1945z00zzast_hrtypez00,
		BgL_bgl_string1945za700za7za7a1980za7, "static sifun ", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1939z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1981z00,
		BGl_z62hrtypezd2nodez12zd2boxzd2set1321za2zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1940z00zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1982z00,
		BGl_z62hrtypezd2nodez12zd2boxzd2ref1323za2zzast_hrtypez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
		BgL_bgl_za762hrtypeza7d2node1983z00,
		BGl_z62hrtypezd2nodez12za2zzast_hrtypez00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzast_hrtypez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzast_hrtypez00(long
		BgL_checksumz00_2975, char *BgL_fromz00_2976)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_hrtypez00))
				{
					BGl_requirezd2initializa7ationz75zzast_hrtypez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_hrtypez00();
					BGl_libraryzd2moduleszd2initz00zzast_hrtypez00();
					BGl_cnstzd2initzd2zzast_hrtypez00();
					BGl_importedzd2moduleszd2initz00zzast_hrtypez00();
					BGl_genericzd2initzd2zzast_hrtypez00();
					BGl_methodzd2initzd2zzast_hrtypez00();
					return BGl_toplevelzd2initzd2zzast_hrtypez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_hrtypez00(void)
	{
		{	/* Ast/hrtype.scm 17 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "ast_hrtype");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_hrtype");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "ast_hrtype");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "ast_hrtype");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "ast_hrtype");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "ast_hrtype");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"ast_hrtype");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "ast_hrtype");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"ast_hrtype");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_hrtype");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_hrtypez00(void)
	{
		{	/* Ast/hrtype.scm 17 */
			{	/* Ast/hrtype.scm 17 */
				obj_t BgL_cportz00_2653;

				{	/* Ast/hrtype.scm 17 */
					obj_t BgL_stringz00_2660;

					BgL_stringz00_2660 = BGl_string1945z00zzast_hrtypez00;
					{	/* Ast/hrtype.scm 17 */
						obj_t BgL_startz00_2661;

						BgL_startz00_2661 = BINT(0L);
						{	/* Ast/hrtype.scm 17 */
							obj_t BgL_endz00_2662;

							BgL_endz00_2662 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2660)));
							{	/* Ast/hrtype.scm 17 */

								BgL_cportz00_2653 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2660, BgL_startz00_2661, BgL_endz00_2662);
				}}}}
				{
					long BgL_iz00_2654;

					BgL_iz00_2654 = 1L;
				BgL_loopz00_2655:
					if ((BgL_iz00_2654 == -1L))
						{	/* Ast/hrtype.scm 17 */
							return BUNSPEC;
						}
					else
						{	/* Ast/hrtype.scm 17 */
							{	/* Ast/hrtype.scm 17 */
								obj_t BgL_arg1946z00_2656;

								{	/* Ast/hrtype.scm 17 */

									{	/* Ast/hrtype.scm 17 */
										obj_t BgL_locationz00_2658;

										BgL_locationz00_2658 = BBOOL(((bool_t) 0));
										{	/* Ast/hrtype.scm 17 */

											BgL_arg1946z00_2656 =
												BGl_readz00zz__readerz00(BgL_cportz00_2653,
												BgL_locationz00_2658);
										}
									}
								}
								{	/* Ast/hrtype.scm 17 */
									int BgL_tmpz00_3006;

									BgL_tmpz00_3006 = (int) (BgL_iz00_2654);
									CNST_TABLE_SET(BgL_tmpz00_3006, BgL_arg1946z00_2656);
							}}
							{	/* Ast/hrtype.scm 17 */
								int BgL_auxz00_2659;

								BgL_auxz00_2659 = (int) ((BgL_iz00_2654 - 1L));
								{
									long BgL_iz00_3011;

									BgL_iz00_3011 = (long) (BgL_auxz00_2659);
									BgL_iz00_2654 = BgL_iz00_3011;
									goto BgL_loopz00_2655;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_hrtypez00(void)
	{
		{	/* Ast/hrtype.scm 17 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzast_hrtypez00(void)
	{
		{	/* Ast/hrtype.scm 17 */
			return BUNSPEC;
		}

	}



/* hrtype-node*! */
	bool_t BGl_hrtypezd2nodeza2z12z62zzast_hrtypez00(obj_t BgL_nodeza2za2_32)
	{
		{	/* Ast/hrtype.scm 385 */
			{
				obj_t BgL_l1264z00_1393;

				BgL_l1264z00_1393 = BgL_nodeza2za2_32;
			BgL_zc3z04anonymousza31327ze3z87_1394:
				if (PAIRP(BgL_l1264z00_1393))
					{	/* Ast/hrtype.scm 386 */
						{	/* Ast/hrtype.scm 386 */
							obj_t BgL_arg1329z00_1396;

							BgL_arg1329z00_1396 = CAR(BgL_l1264z00_1393);
							BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
								((BgL_nodez00_bglt) BgL_arg1329z00_1396));
						}
						{
							obj_t BgL_l1264z00_3019;

							BgL_l1264z00_3019 = CDR(BgL_l1264z00_1393);
							BgL_l1264z00_1393 = BgL_l1264z00_3019;
							goto BgL_zc3z04anonymousza31327ze3z87_1394;
						}
					}
				else
					{	/* Ast/hrtype.scm 386 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* restore-variable-type! */
	obj_t BGl_restorezd2variablezd2typez12z12zzast_hrtypez00(BgL_variablez00_bglt
		BgL_variablez00_33)
	{
		{	/* Ast/hrtype.scm 391 */
			{	/* Ast/hrtype.scm 392 */
				BgL_typez00_bglt BgL_typez00_1399;

				BgL_typez00_1399 =
					(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_33))->BgL_typez00);
				{	/* Ast/hrtype.scm 393 */
					bool_t BgL_test1987z00_3022;

					{	/* Ast/hrtype.scm 393 */
						obj_t BgL_classz00_1901;

						BgL_classz00_1901 = BGl_typez00zztype_typez00;
						{	/* Ast/hrtype.scm 393 */
							BgL_objectz00_bglt BgL_arg1807z00_1903;

							{	/* Ast/hrtype.scm 393 */
								obj_t BgL_tmpz00_3023;

								BgL_tmpz00_3023 =
									((obj_t) ((BgL_objectz00_bglt) BgL_typez00_1399));
								BgL_arg1807z00_1903 = (BgL_objectz00_bglt) (BgL_tmpz00_3023);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Ast/hrtype.scm 393 */
									long BgL_idxz00_1909;

									BgL_idxz00_1909 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1903);
									BgL_test1987z00_3022 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_1909 + 1L)) == BgL_classz00_1901);
								}
							else
								{	/* Ast/hrtype.scm 393 */
									bool_t BgL_res1888z00_1934;

									{	/* Ast/hrtype.scm 393 */
										obj_t BgL_oclassz00_1917;

										{	/* Ast/hrtype.scm 393 */
											obj_t BgL_arg1815z00_1925;
											long BgL_arg1816z00_1926;

											BgL_arg1815z00_1925 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Ast/hrtype.scm 393 */
												long BgL_arg1817z00_1927;

												BgL_arg1817z00_1927 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1903);
												BgL_arg1816z00_1926 =
													(BgL_arg1817z00_1927 - OBJECT_TYPE);
											}
											BgL_oclassz00_1917 =
												VECTOR_REF(BgL_arg1815z00_1925, BgL_arg1816z00_1926);
										}
										{	/* Ast/hrtype.scm 393 */
											bool_t BgL__ortest_1115z00_1918;

											BgL__ortest_1115z00_1918 =
												(BgL_classz00_1901 == BgL_oclassz00_1917);
											if (BgL__ortest_1115z00_1918)
												{	/* Ast/hrtype.scm 393 */
													BgL_res1888z00_1934 = BgL__ortest_1115z00_1918;
												}
											else
												{	/* Ast/hrtype.scm 393 */
													long BgL_odepthz00_1919;

													{	/* Ast/hrtype.scm 393 */
														obj_t BgL_arg1804z00_1920;

														BgL_arg1804z00_1920 = (BgL_oclassz00_1917);
														BgL_odepthz00_1919 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_1920);
													}
													if ((1L < BgL_odepthz00_1919))
														{	/* Ast/hrtype.scm 393 */
															obj_t BgL_arg1802z00_1922;

															{	/* Ast/hrtype.scm 393 */
																obj_t BgL_arg1803z00_1923;

																BgL_arg1803z00_1923 = (BgL_oclassz00_1917);
																BgL_arg1802z00_1922 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_1923,
																	1L);
															}
															BgL_res1888z00_1934 =
																(BgL_arg1802z00_1922 == BgL_classz00_1901);
														}
													else
														{	/* Ast/hrtype.scm 393 */
															BgL_res1888z00_1934 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1987z00_3022 = BgL_res1888z00_1934;
								}
						}
					}
					if (BgL_test1987z00_3022)
						{	/* Ast/hrtype.scm 393 */
							return
								((((BgL_variablez00_bglt) COBJECT(BgL_variablez00_33))->
									BgL_typez00) =
								((BgL_typez00_bglt)
									BGl_findzd2typezd2zztype_envz00((((BgL_typez00_bglt)
												COBJECT(BgL_typez00_1399))->BgL_idz00))), BUNSPEC);
						}
					else
						{	/* Ast/hrtype.scm 393 */
							return BFALSE;
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_hrtypez00(void)
	{
		{	/* Ast/hrtype.scm 17 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_hrtypez00(void)
	{
		{	/* Ast/hrtype.scm 17 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
				BGl_proc1910z00zzast_hrtypez00, BGl_nodez00zzast_nodez00,
				BGl_string1911z00zzast_hrtypez00);
		}

	}



/* &hrtype-node!1266 */
	obj_t BGl_z62hrtypezd2nodez121266za2zzast_hrtypez00(obj_t BgL_envz00_2565,
		obj_t BgL_nodez00_2566)
	{
		{	/* Ast/hrtype.scm 32 */
			{	/* Ast/hrtype.scm 34 */
				bool_t BgL_test1991z00_3050;

				{	/* Ast/hrtype.scm 34 */
					BgL_typez00_bglt BgL_arg1346z00_2665;

					BgL_arg1346z00_2665 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_nodez00_2566)))->BgL_typez00);
					{	/* Ast/hrtype.scm 34 */
						obj_t BgL_classz00_2666;

						BgL_classz00_2666 = BGl_typez00zztype_typez00;
						{	/* Ast/hrtype.scm 34 */
							BgL_objectz00_bglt BgL_arg1807z00_2667;

							{	/* Ast/hrtype.scm 34 */
								obj_t BgL_tmpz00_3053;

								BgL_tmpz00_3053 =
									((obj_t) ((BgL_objectz00_bglt) BgL_arg1346z00_2665));
								BgL_arg1807z00_2667 = (BgL_objectz00_bglt) (BgL_tmpz00_3053);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Ast/hrtype.scm 34 */
									long BgL_idxz00_2668;

									BgL_idxz00_2668 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2667);
									BgL_test1991z00_3050 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2668 + 1L)) == BgL_classz00_2666);
								}
							else
								{	/* Ast/hrtype.scm 34 */
									bool_t BgL_res1889z00_2671;

									{	/* Ast/hrtype.scm 34 */
										obj_t BgL_oclassz00_2672;

										{	/* Ast/hrtype.scm 34 */
											obj_t BgL_arg1815z00_2673;
											long BgL_arg1816z00_2674;

											BgL_arg1815z00_2673 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Ast/hrtype.scm 34 */
												long BgL_arg1817z00_2675;

												BgL_arg1817z00_2675 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2667);
												BgL_arg1816z00_2674 =
													(BgL_arg1817z00_2675 - OBJECT_TYPE);
											}
											BgL_oclassz00_2672 =
												VECTOR_REF(BgL_arg1815z00_2673, BgL_arg1816z00_2674);
										}
										{	/* Ast/hrtype.scm 34 */
											bool_t BgL__ortest_1115z00_2676;

											BgL__ortest_1115z00_2676 =
												(BgL_classz00_2666 == BgL_oclassz00_2672);
											if (BgL__ortest_1115z00_2676)
												{	/* Ast/hrtype.scm 34 */
													BgL_res1889z00_2671 = BgL__ortest_1115z00_2676;
												}
											else
												{	/* Ast/hrtype.scm 34 */
													long BgL_odepthz00_2677;

													{	/* Ast/hrtype.scm 34 */
														obj_t BgL_arg1804z00_2678;

														BgL_arg1804z00_2678 = (BgL_oclassz00_2672);
														BgL_odepthz00_2677 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2678);
													}
													if ((1L < BgL_odepthz00_2677))
														{	/* Ast/hrtype.scm 34 */
															obj_t BgL_arg1802z00_2679;

															{	/* Ast/hrtype.scm 34 */
																obj_t BgL_arg1803z00_2680;

																BgL_arg1803z00_2680 = (BgL_oclassz00_2672);
																BgL_arg1802z00_2679 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2680,
																	1L);
															}
															BgL_res1889z00_2671 =
																(BgL_arg1802z00_2679 == BgL_classz00_2666);
														}
													else
														{	/* Ast/hrtype.scm 34 */
															BgL_res1889z00_2671 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1991z00_3050 = BgL_res1889z00_2671;
								}
						}
					}
				}
				if (BgL_test1991z00_3050)
					{
						BgL_typez00_bglt BgL_auxz00_3076;

						{	/* Ast/hrtype.scm 35 */
							obj_t BgL_arg1342z00_2681;

							BgL_arg1342z00_2681 =
								(((BgL_typez00_bglt) COBJECT(
										(((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_nodez00_2566)))->
											BgL_typez00)))->BgL_idz00);
							BgL_auxz00_3076 =
								BGl_findzd2typezd2zztype_envz00(BgL_arg1342z00_2681);
						}
						return
							((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt) BgL_nodez00_2566)))->BgL_typez00) =
							((BgL_typez00_bglt) BgL_auxz00_3076), BUNSPEC);
					}
				else
					{	/* Ast/hrtype.scm 34 */
						return BFALSE;
					}
			}
		}

	}



/* hrtype-node! */
	BGL_EXPORTED_DEF obj_t BGl_hrtypezd2nodez12zc0zzast_hrtypez00(BgL_nodez00_bglt
		BgL_nodez00_3)
	{
		{	/* Ast/hrtype.scm 32 */
			{	/* Ast/hrtype.scm 32 */
				obj_t BgL_method1267z00_1413;

				{	/* Ast/hrtype.scm 32 */
					obj_t BgL_res1894z00_2003;

					{	/* Ast/hrtype.scm 32 */
						long BgL_objzd2classzd2numz00_1974;

						BgL_objzd2classzd2numz00_1974 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_3));
						{	/* Ast/hrtype.scm 32 */
							obj_t BgL_arg1811z00_1975;

							BgL_arg1811z00_1975 =
								PROCEDURE_REF(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
								(int) (1L));
							{	/* Ast/hrtype.scm 32 */
								int BgL_offsetz00_1978;

								BgL_offsetz00_1978 = (int) (BgL_objzd2classzd2numz00_1974);
								{	/* Ast/hrtype.scm 32 */
									long BgL_offsetz00_1979;

									BgL_offsetz00_1979 =
										((long) (BgL_offsetz00_1978) - OBJECT_TYPE);
									{	/* Ast/hrtype.scm 32 */
										long BgL_modz00_1980;

										BgL_modz00_1980 =
											(BgL_offsetz00_1979 >> (int) ((long) ((int) (4L))));
										{	/* Ast/hrtype.scm 32 */
											long BgL_restz00_1982;

											BgL_restz00_1982 =
												(BgL_offsetz00_1979 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/hrtype.scm 32 */

												{	/* Ast/hrtype.scm 32 */
													obj_t BgL_bucketz00_1984;

													BgL_bucketz00_1984 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_1975), BgL_modz00_1980);
													BgL_res1894z00_2003 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_1984), BgL_restz00_1982);
					}}}}}}}}
					BgL_method1267z00_1413 = BgL_res1894z00_2003;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1267z00_1413, ((obj_t) BgL_nodez00_3));
			}
		}

	}



/* &hrtype-node! */
	obj_t BGl_z62hrtypezd2nodez12za2zzast_hrtypez00(obj_t BgL_envz00_2567,
		obj_t BgL_nodez00_2568)
	{
		{	/* Ast/hrtype.scm 32 */
			return
				BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
				((BgL_nodez00_bglt) BgL_nodez00_2568));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_hrtypez00(void)
	{
		{	/* Ast/hrtype.scm 17 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00, BGl_varz00zzast_nodez00,
				BGl_proc1912z00zzast_hrtypez00, BGl_string1913z00zzast_hrtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
				BGl_sequencez00zzast_nodez00, BGl_proc1914z00zzast_hrtypez00,
				BGl_string1913z00zzast_hrtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00, BGl_syncz00zzast_nodez00,
				BGl_proc1915z00zzast_hrtypez00, BGl_string1913z00zzast_hrtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00, BGl_appz00zzast_nodez00,
				BGl_proc1916z00zzast_hrtypez00, BGl_string1913z00zzast_hrtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1917z00zzast_hrtypez00,
				BGl_string1913z00zzast_hrtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
				BGl_funcallz00zzast_nodez00, BGl_proc1918z00zzast_hrtypez00,
				BGl_string1913z00zzast_hrtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
				BGl_externz00zzast_nodez00, BGl_proc1919z00zzast_hrtypez00,
				BGl_string1913z00zzast_hrtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
				BGl_getfieldz00zzast_nodez00, BGl_proc1920z00zzast_hrtypez00,
				BGl_string1913z00zzast_hrtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
				BGl_setfieldz00zzast_nodez00, BGl_proc1921z00zzast_hrtypez00,
				BGl_string1913z00zzast_hrtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
				BGl_wideningz00zzast_nodez00, BGl_proc1922z00zzast_hrtypez00,
				BGl_string1913z00zzast_hrtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00, BGl_newz00zzast_nodez00,
				BGl_proc1923z00zzast_hrtypez00, BGl_string1913z00zzast_hrtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
				BGl_vallocz00zzast_nodez00, BGl_proc1924z00zzast_hrtypez00,
				BGl_string1913z00zzast_hrtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00, BGl_vrefz00zzast_nodez00,
				BGl_proc1925z00zzast_hrtypez00, BGl_string1913z00zzast_hrtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
				BGl_vsetz12z12zzast_nodez00, BGl_proc1926z00zzast_hrtypez00,
				BGl_string1913z00zzast_hrtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
				BGl_vlengthz00zzast_nodez00, BGl_proc1927z00zzast_hrtypez00,
				BGl_string1913z00zzast_hrtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
				BGl_instanceofz00zzast_nodez00, BGl_proc1928z00zzast_hrtypez00,
				BGl_string1913z00zzast_hrtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00, BGl_castz00zzast_nodez00,
				BGl_proc1929z00zzast_hrtypez00, BGl_string1913z00zzast_hrtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00, BGl_setqz00zzast_nodez00,
				BGl_proc1930z00zzast_hrtypez00, BGl_string1913z00zzast_hrtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1931z00zzast_hrtypez00,
				BGl_string1913z00zzast_hrtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00, BGl_failz00zzast_nodez00,
				BGl_proc1932z00zzast_hrtypez00, BGl_string1913z00zzast_hrtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
				BGl_switchz00zzast_nodez00, BGl_proc1933z00zzast_hrtypez00,
				BGl_string1913z00zzast_hrtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1934z00zzast_hrtypez00,
				BGl_string1913z00zzast_hrtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1935z00zzast_hrtypez00,
				BGl_string1913z00zzast_hrtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc1936z00zzast_hrtypez00,
				BGl_string1913z00zzast_hrtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc1937z00zzast_hrtypez00,
				BGl_string1913z00zzast_hrtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1938z00zzast_hrtypez00,
				BGl_string1913z00zzast_hrtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1939z00zzast_hrtypez00,
				BGl_string1913z00zzast_hrtypez00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1940z00zzast_hrtypez00,
				BGl_string1913z00zzast_hrtypez00);
		}

	}



/* &hrtype-node!-box-ref1323 */
	obj_t BGl_z62hrtypezd2nodez12zd2boxzd2ref1323za2zzast_hrtypez00(obj_t
		BgL_envz00_2597, obj_t BgL_nodez00_2598)
	{
		{	/* Ast/hrtype.scm 377 */
			{

				{	/* Ast/hrtype.scm 379 */
					BgL_varz00_bglt BgL_arg1806z00_2685;

					BgL_arg1806z00_2685 =
						(((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_2598)))->BgL_varz00);
					BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
						((BgL_nodez00_bglt) BgL_arg1806z00_2685));
				}
				{	/* Ast/hrtype.scm 377 */
					obj_t BgL_nextzd2method1322zd2_2684;

					BgL_nextzd2method1322zd2_2684 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_boxzd2refzd2_bglt) BgL_nodez00_2598)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_boxzd2refzd2zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1322zd2_2684,
						((obj_t) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_2598)));
				}
			}
		}

	}



/* &hrtype-node!-box-set1321 */
	obj_t BGl_z62hrtypezd2nodez12zd2boxzd2set1321za2zzast_hrtypez00(obj_t
		BgL_envz00_2599, obj_t BgL_nodez00_2600)
	{
		{	/* Ast/hrtype.scm 368 */
			{

				{	/* Ast/hrtype.scm 370 */
					BgL_varz00_bglt BgL_arg1799z00_2689;

					BgL_arg1799z00_2689 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2600)))->BgL_varz00);
					BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
						((BgL_nodez00_bglt) BgL_arg1799z00_2689));
				}
				BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2600)))->BgL_valuez00));
				{	/* Ast/hrtype.scm 368 */
					obj_t BgL_nextzd2method1320zd2_2688;

					BgL_nextzd2method1320zd2_2688 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2600)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_boxzd2setz12zc0zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1320zd2_2688,
						((obj_t) ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2600)));
				}
			}
		}

	}



/* &hrtype-node!-make-bo1319 */
	obj_t BGl_z62hrtypezd2nodez12zd2makezd2bo1319za2zzast_hrtypez00(obj_t
		BgL_envz00_2601, obj_t BgL_nodez00_2602)
	{
		{	/* Ast/hrtype.scm 360 */
			{

				BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
					(((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_2602)))->BgL_valuez00));
				{	/* Ast/hrtype.scm 360 */
					obj_t BgL_nextzd2method1318zd2_2692;

					BgL_nextzd2method1318zd2_2692 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_2602)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_makezd2boxzd2zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1318zd2_2692,
						((obj_t) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_2602)));
				}
			}
		}

	}



/* &hrtype-node!-jump-ex1317 */
	obj_t BGl_z62hrtypezd2nodez12zd2jumpzd2ex1317za2zzast_hrtypez00(obj_t
		BgL_envz00_2603, obj_t BgL_nodez00_2604)
	{
		{	/* Ast/hrtype.scm 351 */
			{

				BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2604)))->
						BgL_exitz00));
				BGl_hrtypezd2nodez12zc0zzast_hrtypez00((((BgL_jumpzd2exzd2itz00_bglt)
							COBJECT(((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2604)))->
						BgL_valuez00));
				{	/* Ast/hrtype.scm 351 */
					obj_t BgL_nextzd2method1316zd2_2695;

					BgL_nextzd2method1316zd2_2695 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2604)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_jumpzd2exzd2itz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1316zd2_2695,
						((obj_t) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2604)));
				}
			}
		}

	}



/* &hrtype-node!-set-ex-1315 */
	obj_t BGl_z62hrtypezd2nodez12zd2setzd2exzd21315z70zzast_hrtypez00(obj_t
		BgL_envz00_2605, obj_t BgL_nodez00_2606)
	{
		{	/* Ast/hrtype.scm 340 */
			{

				BGl_restorezd2variablezd2typez12z12zzast_hrtypez00(
					(((BgL_varz00_bglt) COBJECT(
								(((BgL_setzd2exzd2itz00_bglt) COBJECT(
											((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2606)))->
									BgL_varz00)))->BgL_variablez00));
				BGl_hrtypezd2nodez12zc0zzast_hrtypez00((((BgL_setzd2exzd2itz00_bglt)
							COBJECT(((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2606)))->
						BgL_bodyz00));
				BGl_hrtypezd2nodez12zc0zzast_hrtypez00((((BgL_setzd2exzd2itz00_bglt)
							COBJECT(((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2606)))->
						BgL_onexitz00));
				{	/* Ast/hrtype.scm 345 */
					BgL_varz00_bglt BgL_arg1771z00_2699;

					BgL_arg1771z00_2699 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2606)))->BgL_varz00);
					BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
						((BgL_nodez00_bglt) BgL_arg1771z00_2699));
				}
				{	/* Ast/hrtype.scm 340 */
					obj_t BgL_nextzd2method1314zd2_2698;

					BgL_nextzd2method1314zd2_2698 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2606)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_setzd2exzd2itz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1314zd2_2698,
						((obj_t) ((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2606)));
				}
			}
		}

	}



/* &hrtype-node!-let-var1313 */
	obj_t BGl_z62hrtypezd2nodez12zd2letzd2var1313za2zzast_hrtypez00(obj_t
		BgL_envz00_2607, obj_t BgL_nodez00_2608)
	{
		{	/* Ast/hrtype.scm 326 */
			{

				{	/* Ast/hrtype.scm 328 */
					obj_t BgL_g1263z00_2703;

					BgL_g1263z00_2703 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2608)))->BgL_bindingsz00);
					{
						obj_t BgL_l1261z00_2705;

						BgL_l1261z00_2705 = BgL_g1263z00_2703;
					BgL_zc3z04anonymousza31753ze3z87_2704:
						if (PAIRP(BgL_l1261z00_2705))
							{	/* Ast/hrtype.scm 328 */
								{	/* Ast/hrtype.scm 329 */
									obj_t BgL_bindingz00_2706;

									BgL_bindingz00_2706 = CAR(BgL_l1261z00_2705);
									{	/* Ast/hrtype.scm 329 */
										obj_t BgL_varz00_2707;
										obj_t BgL_valz00_2708;

										BgL_varz00_2707 = CAR(((obj_t) BgL_bindingz00_2706));
										BgL_valz00_2708 = CDR(((obj_t) BgL_bindingz00_2706));
										BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
											((BgL_nodez00_bglt) BgL_valz00_2708));
										BGl_restorezd2variablezd2typez12z12zzast_hrtypez00(
											((BgL_variablez00_bglt) BgL_varz00_2707));
									}
								}
								{
									obj_t BgL_l1261z00_3235;

									BgL_l1261z00_3235 = CDR(BgL_l1261z00_2705);
									BgL_l1261z00_2705 = BgL_l1261z00_3235;
									goto BgL_zc3z04anonymousza31753ze3z87_2704;
								}
							}
						else
							{	/* Ast/hrtype.scm 328 */
								((bool_t) 1);
							}
					}
				}
				BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
					(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2608)))->BgL_bodyz00));
				{	/* Ast/hrtype.scm 326 */
					obj_t BgL_nextzd2method1312zd2_2702;

					BgL_nextzd2method1312zd2_2702 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2608)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_letzd2varzd2zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1312zd2_2702,
						((obj_t) ((BgL_letzd2varzd2_bglt) BgL_nodez00_2608)));
				}
			}
		}

	}



/* &hrtype-node!-let-fun1311 */
	obj_t BGl_z62hrtypezd2nodez12zd2letzd2fun1311za2zzast_hrtypez00(obj_t
		BgL_envz00_2609, obj_t BgL_nodez00_2610)
	{
		{	/* Ast/hrtype.scm 300 */
			{

				{	/* Ast/hrtype.scm 302 */
					obj_t BgL_g1260z00_2712;

					BgL_g1260z00_2712 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2610)))->BgL_localsz00);
					{
						obj_t BgL_l1258z00_2714;

						BgL_l1258z00_2714 = BgL_g1260z00_2712;
					BgL_zc3z04anonymousza31735ze3z87_2713:
						if (PAIRP(BgL_l1258z00_2714))
							{	/* Ast/hrtype.scm 302 */
								{	/* Ast/hrtype.scm 303 */
									obj_t BgL_localz00_2715;

									BgL_localz00_2715 = CAR(BgL_l1258z00_2714);
									{	/* Ast/hrtype.scm 303 */
										BgL_valuez00_bglt BgL_sfunz00_2716;

										BgL_sfunz00_2716 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt) BgL_localz00_2715))))->
											BgL_valuez00);
										{	/* Ast/hrtype.scm 304 */
											obj_t BgL_g1129z00_2717;

											BgL_g1129z00_2717 =
												(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_sfunz00_2716)))->
												BgL_argsz00);
											{
												obj_t BgL_argsz00_2719;

												BgL_argsz00_2719 = BgL_g1129z00_2717;
											BgL_loopz00_2718:
												if (PAIRP(BgL_argsz00_2719))
													{	/* Ast/hrtype.scm 306 */
														obj_t BgL_argz00_2720;

														BgL_argz00_2720 = CAR(BgL_argsz00_2719);
														{	/* Ast/hrtype.scm 308 */
															bool_t BgL_test1998z00_3262;

															{	/* Ast/hrtype.scm 308 */
																obj_t BgL_classz00_2721;

																BgL_classz00_2721 = BGl_typez00zztype_typez00;
																if (BGL_OBJECTP(BgL_argz00_2720))
																	{	/* Ast/hrtype.scm 308 */
																		BgL_objectz00_bglt BgL_arg1807z00_2722;

																		BgL_arg1807z00_2722 =
																			(BgL_objectz00_bglt) (BgL_argz00_2720);
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Ast/hrtype.scm 308 */
																				long BgL_idxz00_2723;

																				BgL_idxz00_2723 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_2722);
																				BgL_test1998z00_3262 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_2723 + 1L)) ==
																					BgL_classz00_2721);
																			}
																		else
																			{	/* Ast/hrtype.scm 308 */
																				bool_t BgL_res1902z00_2726;

																				{	/* Ast/hrtype.scm 308 */
																					obj_t BgL_oclassz00_2727;

																					{	/* Ast/hrtype.scm 308 */
																						obj_t BgL_arg1815z00_2728;
																						long BgL_arg1816z00_2729;

																						BgL_arg1815z00_2728 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Ast/hrtype.scm 308 */
																							long BgL_arg1817z00_2730;

																							BgL_arg1817z00_2730 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_2722);
																							BgL_arg1816z00_2729 =
																								(BgL_arg1817z00_2730 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_2727 =
																							VECTOR_REF(BgL_arg1815z00_2728,
																							BgL_arg1816z00_2729);
																					}
																					{	/* Ast/hrtype.scm 308 */
																						bool_t BgL__ortest_1115z00_2731;

																						BgL__ortest_1115z00_2731 =
																							(BgL_classz00_2721 ==
																							BgL_oclassz00_2727);
																						if (BgL__ortest_1115z00_2731)
																							{	/* Ast/hrtype.scm 308 */
																								BgL_res1902z00_2726 =
																									BgL__ortest_1115z00_2731;
																							}
																						else
																							{	/* Ast/hrtype.scm 308 */
																								long BgL_odepthz00_2732;

																								{	/* Ast/hrtype.scm 308 */
																									obj_t BgL_arg1804z00_2733;

																									BgL_arg1804z00_2733 =
																										(BgL_oclassz00_2727);
																									BgL_odepthz00_2732 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_2733);
																								}
																								if ((1L < BgL_odepthz00_2732))
																									{	/* Ast/hrtype.scm 308 */
																										obj_t BgL_arg1802z00_2734;

																										{	/* Ast/hrtype.scm 308 */
																											obj_t BgL_arg1803z00_2735;

																											BgL_arg1803z00_2735 =
																												(BgL_oclassz00_2727);
																											BgL_arg1802z00_2734 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_2735,
																												1L);
																										}
																										BgL_res1902z00_2726 =
																											(BgL_arg1802z00_2734 ==
																											BgL_classz00_2721);
																									}
																								else
																									{	/* Ast/hrtype.scm 308 */
																										BgL_res1902z00_2726 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test1998z00_3262 =
																					BgL_res1902z00_2726;
																			}
																	}
																else
																	{	/* Ast/hrtype.scm 308 */
																		BgL_test1998z00_3262 = ((bool_t) 0);
																	}
															}
															if (BgL_test1998z00_3262)
																{	/* Ast/hrtype.scm 309 */
																	BgL_typez00_bglt BgL_arg1740z00_2736;

																	{	/* Ast/hrtype.scm 309 */
																		obj_t BgL_arg1746z00_2737;

																		BgL_arg1746z00_2737 =
																			(((BgL_typez00_bglt) COBJECT(
																					((BgL_typez00_bglt)
																						BgL_argz00_2720)))->BgL_idz00);
																		BgL_arg1740z00_2736 =
																			BGl_findzd2typezd2zztype_envz00
																			(BgL_arg1746z00_2737);
																	}
																	{	/* Ast/hrtype.scm 309 */
																		obj_t BgL_tmpz00_3288;

																		BgL_tmpz00_3288 =
																			((obj_t) BgL_arg1740z00_2736);
																		SET_CAR(BgL_argsz00_2719, BgL_tmpz00_3288);
																	}
																}
															else
																{	/* Ast/hrtype.scm 310 */
																	bool_t BgL_test2003z00_3291;

																	{	/* Ast/hrtype.scm 310 */
																		obj_t BgL_classz00_2738;

																		BgL_classz00_2738 =
																			BGl_localz00zzast_varz00;
																		if (BGL_OBJECTP(BgL_argz00_2720))
																			{	/* Ast/hrtype.scm 310 */
																				BgL_objectz00_bglt BgL_arg1807z00_2739;

																				BgL_arg1807z00_2739 =
																					(BgL_objectz00_bglt)
																					(BgL_argz00_2720);
																				if (BGL_CONDEXPAND_ISA_ARCH64())
																					{	/* Ast/hrtype.scm 310 */
																						long BgL_idxz00_2740;

																						BgL_idxz00_2740 =
																							BGL_OBJECT_INHERITANCE_NUM
																							(BgL_arg1807z00_2739);
																						BgL_test2003z00_3291 =
																							(VECTOR_REF
																							(BGl_za2inheritancesza2z00zz__objectz00,
																								(BgL_idxz00_2740 + 2L)) ==
																							BgL_classz00_2738);
																					}
																				else
																					{	/* Ast/hrtype.scm 310 */
																						bool_t BgL_res1903z00_2743;

																						{	/* Ast/hrtype.scm 310 */
																							obj_t BgL_oclassz00_2744;

																							{	/* Ast/hrtype.scm 310 */
																								obj_t BgL_arg1815z00_2745;
																								long BgL_arg1816z00_2746;

																								BgL_arg1815z00_2745 =
																									(BGl_za2classesza2z00zz__objectz00);
																								{	/* Ast/hrtype.scm 310 */
																									long BgL_arg1817z00_2747;

																									BgL_arg1817z00_2747 =
																										BGL_OBJECT_CLASS_NUM
																										(BgL_arg1807z00_2739);
																									BgL_arg1816z00_2746 =
																										(BgL_arg1817z00_2747 -
																										OBJECT_TYPE);
																								}
																								BgL_oclassz00_2744 =
																									VECTOR_REF
																									(BgL_arg1815z00_2745,
																									BgL_arg1816z00_2746);
																							}
																							{	/* Ast/hrtype.scm 310 */
																								bool_t BgL__ortest_1115z00_2748;

																								BgL__ortest_1115z00_2748 =
																									(BgL_classz00_2738 ==
																									BgL_oclassz00_2744);
																								if (BgL__ortest_1115z00_2748)
																									{	/* Ast/hrtype.scm 310 */
																										BgL_res1903z00_2743 =
																											BgL__ortest_1115z00_2748;
																									}
																								else
																									{	/* Ast/hrtype.scm 310 */
																										long BgL_odepthz00_2749;

																										{	/* Ast/hrtype.scm 310 */
																											obj_t BgL_arg1804z00_2750;

																											BgL_arg1804z00_2750 =
																												(BgL_oclassz00_2744);
																											BgL_odepthz00_2749 =
																												BGL_CLASS_DEPTH
																												(BgL_arg1804z00_2750);
																										}
																										if (
																											(2L < BgL_odepthz00_2749))
																											{	/* Ast/hrtype.scm 310 */
																												obj_t
																													BgL_arg1802z00_2751;
																												{	/* Ast/hrtype.scm 310 */
																													obj_t
																														BgL_arg1803z00_2752;
																													BgL_arg1803z00_2752 =
																														(BgL_oclassz00_2744);
																													BgL_arg1802z00_2751 =
																														BGL_CLASS_ANCESTORS_REF
																														(BgL_arg1803z00_2752,
																														2L);
																												}
																												BgL_res1903z00_2743 =
																													(BgL_arg1802z00_2751
																													== BgL_classz00_2738);
																											}
																										else
																											{	/* Ast/hrtype.scm 310 */
																												BgL_res1903z00_2743 =
																													((bool_t) 0);
																											}
																									}
																							}
																						}
																						BgL_test2003z00_3291 =
																							BgL_res1903z00_2743;
																					}
																			}
																		else
																			{	/* Ast/hrtype.scm 310 */
																				BgL_test2003z00_3291 = ((bool_t) 0);
																			}
																	}
																	if (BgL_test2003z00_3291)
																		{	/* Ast/hrtype.scm 310 */
																			BGl_restorezd2variablezd2typez12z12zzast_hrtypez00
																				(((BgL_variablez00_bglt)
																					BgL_argz00_2720));
																		}
																	else
																		{	/* Ast/hrtype.scm 310 */
																			BGl_errorz00zz__errorz00
																				(BGl_string1913z00zzast_hrtypez00,
																				BGl_string1941z00zzast_hrtypez00,
																				BGl_shapez00zztools_shapez00
																				(BgL_argz00_2720));
																		}
																}
														}
														{
															obj_t BgL_argsz00_3318;

															BgL_argsz00_3318 = CDR(BgL_argsz00_2719);
															BgL_argsz00_2719 = BgL_argsz00_3318;
															goto BgL_loopz00_2718;
														}
													}
												else
													{	/* Ast/hrtype.scm 305 */
														((bool_t) 0);
													}
											}
										}
										BGl_restorezd2variablezd2typez12z12zzast_hrtypez00(
											((BgL_variablez00_bglt) BgL_localz00_2715));
										{	/* Ast/hrtype.scm 318 */
											obj_t BgL_arg1750z00_2753;

											BgL_arg1750z00_2753 =
												(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_sfunz00_2716)))->
												BgL_bodyz00);
											BGl_hrtypezd2nodez12zc0zzast_hrtypez00(((BgL_nodez00_bglt)
													BgL_arg1750z00_2753));
										}
									}
								}
								{
									obj_t BgL_l1258z00_3326;

									BgL_l1258z00_3326 = CDR(BgL_l1258z00_2714);
									BgL_l1258z00_2714 = BgL_l1258z00_3326;
									goto BgL_zc3z04anonymousza31735ze3z87_2713;
								}
							}
						else
							{	/* Ast/hrtype.scm 302 */
								((bool_t) 1);
							}
					}
				}
				BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
					(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2610)))->BgL_bodyz00));
				{	/* Ast/hrtype.scm 300 */
					obj_t BgL_nextzd2method1310zd2_2711;

					BgL_nextzd2method1310zd2_2711 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2610)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_letzd2funzd2zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1310zd2_2711,
						((obj_t) ((BgL_letzd2funzd2_bglt) BgL_nodez00_2610)));
				}
			}
		}

	}



/* &hrtype-node!-switch1309 */
	obj_t BGl_z62hrtypezd2nodez12zd2switch1309z70zzast_hrtypez00(obj_t
		BgL_envz00_2611, obj_t BgL_nodez00_2612)
	{
		{	/* Ast/hrtype.scm 288 */
			{

				{
					BgL_typez00_bglt BgL_auxz00_3340;

					{	/* Ast/hrtype.scm 290 */
						obj_t BgL_arg1720z00_2757;

						BgL_arg1720z00_2757 =
							(((BgL_typez00_bglt) COBJECT(
									(((BgL_switchz00_bglt) COBJECT(
												((BgL_switchz00_bglt) BgL_nodez00_2612)))->
										BgL_itemzd2typezd2)))->BgL_idz00);
						BgL_auxz00_3340 =
							BGl_findzd2typezd2zztype_envz00(BgL_arg1720z00_2757);
					}
					((((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt) BgL_nodez00_2612)))->
							BgL_itemzd2typezd2) =
						((BgL_typez00_bglt) BgL_auxz00_3340), BUNSPEC);
				}
				BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
					(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2612)))->BgL_testz00));
				{	/* Ast/hrtype.scm 292 */
					obj_t BgL_g1257z00_2758;

					BgL_g1257z00_2758 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2612)))->BgL_clausesz00);
					{
						obj_t BgL_l1255z00_2760;

						BgL_l1255z00_2760 = BgL_g1257z00_2758;
					BgL_zc3z04anonymousza31725ze3z87_2759:
						if (PAIRP(BgL_l1255z00_2760))
							{	/* Ast/hrtype.scm 292 */
								{	/* Ast/hrtype.scm 293 */
									obj_t BgL_clausez00_2761;

									BgL_clausez00_2761 = CAR(BgL_l1255z00_2760);
									{	/* Ast/hrtype.scm 293 */
										obj_t BgL_arg1733z00_2762;

										BgL_arg1733z00_2762 = CDR(((obj_t) BgL_clausez00_2761));
										BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
											((BgL_nodez00_bglt) BgL_arg1733z00_2762));
									}
								}
								{
									obj_t BgL_l1255z00_3359;

									BgL_l1255z00_3359 = CDR(BgL_l1255z00_2760);
									BgL_l1255z00_2760 = BgL_l1255z00_3359;
									goto BgL_zc3z04anonymousza31725ze3z87_2759;
								}
							}
						else
							{	/* Ast/hrtype.scm 292 */
								((bool_t) 1);
							}
					}
				}
				{	/* Ast/hrtype.scm 288 */
					obj_t BgL_nextzd2method1308zd2_2756;

					BgL_nextzd2method1308zd2_2756 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_switchz00_bglt) BgL_nodez00_2612)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_switchz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1308zd2_2756,
						((obj_t) ((BgL_switchz00_bglt) BgL_nodez00_2612)));
				}
			}
		}

	}



/* &hrtype-node!-fail1307 */
	obj_t BGl_z62hrtypezd2nodez12zd2fail1307z70zzast_hrtypez00(obj_t
		BgL_envz00_2613, obj_t BgL_nodez00_2614)
	{
		{	/* Ast/hrtype.scm 278 */
			{

				BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
					(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2614)))->BgL_procz00));
				BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
					(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2614)))->BgL_msgz00));
				BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
					(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2614)))->BgL_objz00));
				{	/* Ast/hrtype.scm 278 */
					obj_t BgL_nextzd2method1306zd2_2765;

					BgL_nextzd2method1306zd2_2765 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_failz00_bglt) BgL_nodez00_2614)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_failz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1306zd2_2765,
						((obj_t) ((BgL_failz00_bglt) BgL_nodez00_2614)));
				}
			}
		}

	}



/* &hrtype-node!-conditi1305 */
	obj_t BGl_z62hrtypezd2nodez12zd2conditi1305z70zzast_hrtypez00(obj_t
		BgL_envz00_2615, obj_t BgL_nodez00_2616)
	{
		{	/* Ast/hrtype.scm 268 */
			{

				BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
					(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2616)))->BgL_testz00));
				BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
					(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2616)))->BgL_truez00));
				BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
					(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2616)))->BgL_falsez00));
				{	/* Ast/hrtype.scm 268 */
					obj_t BgL_nextzd2method1304zd2_2768;

					BgL_nextzd2method1304zd2_2768 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_conditionalz00_bglt) BgL_nodez00_2616)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_conditionalz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1304zd2_2768,
						((obj_t) ((BgL_conditionalz00_bglt) BgL_nodez00_2616)));
				}
			}
		}

	}



/* &hrtype-node!-setq1303 */
	obj_t BGl_z62hrtypezd2nodez12zd2setq1303z70zzast_hrtypez00(obj_t
		BgL_envz00_2617, obj_t BgL_nodez00_2618)
	{
		{	/* Ast/hrtype.scm 259 */
			{

				BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
					(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2618)))->BgL_valuez00));
				{	/* Ast/hrtype.scm 262 */
					BgL_varz00_bglt BgL_arg1708z00_2772;

					BgL_arg1708z00_2772 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2618)))->BgL_varz00);
					BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
						((BgL_nodez00_bglt) BgL_arg1708z00_2772));
				}
				{	/* Ast/hrtype.scm 259 */
					obj_t BgL_nextzd2method1302zd2_2771;

					BgL_nextzd2method1302zd2_2771 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_setqz00_bglt) BgL_nodez00_2618)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_setqz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1302zd2_2771,
						((obj_t) ((BgL_setqz00_bglt) BgL_nodez00_2618)));
				}
			}
		}

	}



/* &hrtype-node!-cast1301 */
	obj_t BGl_z62hrtypezd2nodez12zd2cast1301z70zzast_hrtypez00(obj_t
		BgL_envz00_2619, obj_t BgL_nodez00_2620)
	{
		{	/* Ast/hrtype.scm 251 */
			{

				BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
					(((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nodez00_2620)))->BgL_argz00));
				{	/* Ast/hrtype.scm 251 */
					obj_t BgL_nextzd2method1300zd2_2775;

					BgL_nextzd2method1300zd2_2775 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_castz00_bglt) BgL_nodez00_2620)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_castz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1300zd2_2775,
						((obj_t) ((BgL_castz00_bglt) BgL_nodez00_2620)));
				}
			}
		}

	}



/* &hrtype-node!-instanc1299 */
	obj_t BGl_z62hrtypezd2nodez12zd2instanc1299z70zzast_hrtypez00(obj_t
		BgL_envz00_2621, obj_t BgL_nodez00_2622)
	{
		{	/* Ast/hrtype.scm 243 */
			{

				{
					BgL_typez00_bglt BgL_auxz00_3434;

					{	/* Ast/hrtype.scm 245 */
						obj_t BgL_arg1701z00_2779;

						BgL_arg1701z00_2779 =
							(((BgL_typez00_bglt) COBJECT(
									(((BgL_instanceofz00_bglt) COBJECT(
												((BgL_instanceofz00_bglt) BgL_nodez00_2622)))->
										BgL_classz00)))->BgL_idz00);
						BgL_auxz00_3434 =
							BGl_findzd2typezd2zztype_envz00(BgL_arg1701z00_2779);
					}
					((((BgL_instanceofz00_bglt) COBJECT(
									((BgL_instanceofz00_bglt) BgL_nodez00_2622)))->BgL_classz00) =
						((BgL_typez00_bglt) BgL_auxz00_3434), BUNSPEC);
				}
				{	/* Ast/hrtype.scm 243 */
					obj_t BgL_nextzd2method1298zd2_2778;

					BgL_nextzd2method1298zd2_2778 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_instanceofz00_bglt) BgL_nodez00_2622)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_instanceofz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1298zd2_2778,
						((obj_t) ((BgL_instanceofz00_bglt) BgL_nodez00_2622)));
				}
			}
		}

	}



/* &hrtype-node!-vlength1297 */
	obj_t BGl_z62hrtypezd2nodez12zd2vlength1297z70zzast_hrtypez00(obj_t
		BgL_envz00_2623, obj_t BgL_nodez00_2624)
	{
		{	/* Ast/hrtype.scm 235 */
			{

				{
					BgL_typez00_bglt BgL_auxz00_3450;

					{	/* Ast/hrtype.scm 237 */
						obj_t BgL_arg1699z00_2783;

						BgL_arg1699z00_2783 =
							(((BgL_typez00_bglt) COBJECT(
									(((BgL_vlengthz00_bglt) COBJECT(
												((BgL_vlengthz00_bglt) BgL_nodez00_2624)))->
										BgL_vtypez00)))->BgL_idz00);
						BgL_auxz00_3450 =
							BGl_findzd2typezd2zztype_envz00(BgL_arg1699z00_2783);
					}
					((((BgL_vlengthz00_bglt) COBJECT(
									((BgL_vlengthz00_bglt) BgL_nodez00_2624)))->BgL_vtypez00) =
						((BgL_typez00_bglt) BgL_auxz00_3450), BUNSPEC);
				}
				{	/* Ast/hrtype.scm 235 */
					obj_t BgL_nextzd2method1296zd2_2782;

					BgL_nextzd2method1296zd2_2782 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_vlengthz00_bglt) BgL_nodez00_2624)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_vlengthz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1296zd2_2782,
						((obj_t) ((BgL_vlengthz00_bglt) BgL_nodez00_2624)));
				}
			}
		}

	}



/* &hrtype-node!-vset!1295 */
	obj_t BGl_z62hrtypezd2nodez12zd2vsetz121295z62zzast_hrtypez00(obj_t
		BgL_envz00_2625, obj_t BgL_nodez00_2626)
	{
		{	/* Ast/hrtype.scm 225 */
			{

				{
					BgL_typez00_bglt BgL_auxz00_3466;

					{	/* Ast/hrtype.scm 227 */
						obj_t BgL_arg1678z00_2787;

						BgL_arg1678z00_2787 =
							(((BgL_typez00_bglt) COBJECT(
									(((BgL_vsetz12z12_bglt) COBJECT(
												((BgL_vsetz12z12_bglt) BgL_nodez00_2626)))->
										BgL_ftypez00)))->BgL_idz00);
						BgL_auxz00_3466 =
							BGl_findzd2typezd2zztype_envz00(BgL_arg1678z00_2787);
					}
					((((BgL_vsetz12z12_bglt) COBJECT(
									((BgL_vsetz12z12_bglt) BgL_nodez00_2626)))->BgL_ftypez00) =
						((BgL_typez00_bglt) BgL_auxz00_3466), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_3473;

					{	/* Ast/hrtype.scm 228 */
						obj_t BgL_arg1688z00_2788;

						BgL_arg1688z00_2788 =
							(((BgL_typez00_bglt) COBJECT(
									(((BgL_vsetz12z12_bglt) COBJECT(
												((BgL_vsetz12z12_bglt) BgL_nodez00_2626)))->
										BgL_otypez00)))->BgL_idz00);
						BgL_auxz00_3473 =
							BGl_findzd2typezd2zztype_envz00(BgL_arg1688z00_2788);
					}
					((((BgL_vsetz12z12_bglt) COBJECT(
									((BgL_vsetz12z12_bglt) BgL_nodez00_2626)))->BgL_otypez00) =
						((BgL_typez00_bglt) BgL_auxz00_3473), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_3480;

					{	/* Ast/hrtype.scm 229 */
						obj_t BgL_arg1691z00_2789;

						BgL_arg1691z00_2789 =
							(((BgL_typez00_bglt) COBJECT(
									(((BgL_vsetz12z12_bglt) COBJECT(
												((BgL_vsetz12z12_bglt) BgL_nodez00_2626)))->
										BgL_vtypez00)))->BgL_idz00);
						BgL_auxz00_3480 =
							BGl_findzd2typezd2zztype_envz00(BgL_arg1691z00_2789);
					}
					((((BgL_vsetz12z12_bglt) COBJECT(
									((BgL_vsetz12z12_bglt) BgL_nodez00_2626)))->BgL_vtypez00) =
						((BgL_typez00_bglt) BgL_auxz00_3480), BUNSPEC);
				}
				{	/* Ast/hrtype.scm 225 */
					obj_t BgL_nextzd2method1294zd2_2786;

					BgL_nextzd2method1294zd2_2786 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_vsetz12z12_bglt) BgL_nodez00_2626)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_vsetz12z12zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1294zd2_2786,
						((obj_t) ((BgL_vsetz12z12_bglt) BgL_nodez00_2626)));
				}
			}
		}

	}



/* &hrtype-node!-vref1293 */
	obj_t BGl_z62hrtypezd2nodez12zd2vref1293z70zzast_hrtypez00(obj_t
		BgL_envz00_2627, obj_t BgL_nodez00_2628)
	{
		{	/* Ast/hrtype.scm 215 */
			{

				{
					BgL_typez00_bglt BgL_auxz00_3496;

					{	/* Ast/hrtype.scm 217 */
						obj_t BgL_arg1650z00_2793;

						BgL_arg1650z00_2793 =
							(((BgL_typez00_bglt) COBJECT(
									(((BgL_vrefz00_bglt) COBJECT(
												((BgL_vrefz00_bglt) BgL_nodez00_2628)))->
										BgL_ftypez00)))->BgL_idz00);
						BgL_auxz00_3496 =
							BGl_findzd2typezd2zztype_envz00(BgL_arg1650z00_2793);
					}
					((((BgL_vrefz00_bglt) COBJECT(
									((BgL_vrefz00_bglt) BgL_nodez00_2628)))->BgL_ftypez00) =
						((BgL_typez00_bglt) BgL_auxz00_3496), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_3503;

					{	/* Ast/hrtype.scm 218 */
						obj_t BgL_arg1654z00_2794;

						BgL_arg1654z00_2794 =
							(((BgL_typez00_bglt) COBJECT(
									(((BgL_vrefz00_bglt) COBJECT(
												((BgL_vrefz00_bglt) BgL_nodez00_2628)))->
										BgL_otypez00)))->BgL_idz00);
						BgL_auxz00_3503 =
							BGl_findzd2typezd2zztype_envz00(BgL_arg1654z00_2794);
					}
					((((BgL_vrefz00_bglt) COBJECT(
									((BgL_vrefz00_bglt) BgL_nodez00_2628)))->BgL_otypez00) =
						((BgL_typez00_bglt) BgL_auxz00_3503), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_3510;

					{	/* Ast/hrtype.scm 219 */
						obj_t BgL_arg1663z00_2795;

						BgL_arg1663z00_2795 =
							(((BgL_typez00_bglt) COBJECT(
									(((BgL_vrefz00_bglt) COBJECT(
												((BgL_vrefz00_bglt) BgL_nodez00_2628)))->
										BgL_vtypez00)))->BgL_idz00);
						BgL_auxz00_3510 =
							BGl_findzd2typezd2zztype_envz00(BgL_arg1663z00_2795);
					}
					((((BgL_vrefz00_bglt) COBJECT(
									((BgL_vrefz00_bglt) BgL_nodez00_2628)))->BgL_vtypez00) =
						((BgL_typez00_bglt) BgL_auxz00_3510), BUNSPEC);
				}
				{	/* Ast/hrtype.scm 215 */
					obj_t BgL_nextzd2method1292zd2_2792;

					BgL_nextzd2method1292zd2_2792 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_vrefz00_bglt) BgL_nodez00_2628)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_vrefz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1292zd2_2792,
						((obj_t) ((BgL_vrefz00_bglt) BgL_nodez00_2628)));
				}
			}
		}

	}



/* &hrtype-node!-valloc1291 */
	obj_t BGl_z62hrtypezd2nodez12zd2valloc1291z70zzast_hrtypez00(obj_t
		BgL_envz00_2629, obj_t BgL_nodez00_2630)
	{
		{	/* Ast/hrtype.scm 206 */
			{

				{
					BgL_typez00_bglt BgL_auxz00_3526;

					{	/* Ast/hrtype.scm 208 */
						obj_t BgL_arg1629z00_2799;

						BgL_arg1629z00_2799 =
							(((BgL_typez00_bglt) COBJECT(
									(((BgL_vallocz00_bglt) COBJECT(
												((BgL_vallocz00_bglt) BgL_nodez00_2630)))->
										BgL_ftypez00)))->BgL_idz00);
						BgL_auxz00_3526 =
							BGl_findzd2typezd2zztype_envz00(BgL_arg1629z00_2799);
					}
					((((BgL_vallocz00_bglt) COBJECT(
									((BgL_vallocz00_bglt) BgL_nodez00_2630)))->BgL_ftypez00) =
						((BgL_typez00_bglt) BgL_auxz00_3526), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_3533;

					{	/* Ast/hrtype.scm 209 */
						obj_t BgL_arg1642z00_2800;

						BgL_arg1642z00_2800 =
							(((BgL_typez00_bglt) COBJECT(
									(((BgL_vallocz00_bglt) COBJECT(
												((BgL_vallocz00_bglt) BgL_nodez00_2630)))->
										BgL_otypez00)))->BgL_idz00);
						BgL_auxz00_3533 =
							BGl_findzd2typezd2zztype_envz00(BgL_arg1642z00_2800);
					}
					((((BgL_vallocz00_bglt) COBJECT(
									((BgL_vallocz00_bglt) BgL_nodez00_2630)))->BgL_otypez00) =
						((BgL_typez00_bglt) BgL_auxz00_3533), BUNSPEC);
				}
				{	/* Ast/hrtype.scm 206 */
					obj_t BgL_nextzd2method1290zd2_2798;

					BgL_nextzd2method1290zd2_2798 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_vallocz00_bglt) BgL_nodez00_2630)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_vallocz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1290zd2_2798,
						((obj_t) ((BgL_vallocz00_bglt) BgL_nodez00_2630)));
				}
			}
		}

	}



/* &hrtype-node!-new1289 */
	obj_t BGl_z62hrtypezd2nodez12zd2new1289z70zzast_hrtypez00(obj_t
		BgL_envz00_2631, obj_t BgL_nodez00_2632)
	{
		{	/* Ast/hrtype.scm 198 */
			{

				{
					obj_t BgL_auxz00_3549;

					{	/* Ast/hrtype.scm 200 */
						obj_t BgL_l01254z00_2804;

						BgL_l01254z00_2804 =
							(((BgL_newz00_bglt) COBJECT(
									((BgL_newz00_bglt) BgL_nodez00_2632)))->BgL_argszd2typezd2);
						{
							obj_t BgL_l1253z00_2806;

							BgL_l1253z00_2806 = BgL_l01254z00_2804;
						BgL_zc3z04anonymousza31617ze3z87_2805:
							if (NULLP(BgL_l1253z00_2806))
								{	/* Ast/hrtype.scm 200 */
									BgL_auxz00_3549 = BgL_l01254z00_2804;
								}
							else
								{	/* Ast/hrtype.scm 200 */
									{	/* Ast/hrtype.scm 200 */
										BgL_typez00_bglt BgL_arg1625z00_2807;

										{	/* Ast/hrtype.scm 200 */
											obj_t BgL_tz00_2808;

											BgL_tz00_2808 = CAR(((obj_t) BgL_l1253z00_2806));
											{	/* Ast/hrtype.scm 200 */
												obj_t BgL_arg1626z00_2809;

												BgL_arg1626z00_2809 =
													(((BgL_typez00_bglt) COBJECT(
															((BgL_typez00_bglt) BgL_tz00_2808)))->BgL_idz00);
												BgL_arg1625z00_2807 =
													BGl_findzd2typezd2zztype_envz00(BgL_arg1626z00_2809);
											}
										}
										{	/* Ast/hrtype.scm 200 */
											obj_t BgL_auxz00_3562;
											obj_t BgL_tmpz00_3560;

											BgL_auxz00_3562 = ((obj_t) BgL_arg1625z00_2807);
											BgL_tmpz00_3560 = ((obj_t) BgL_l1253z00_2806);
											SET_CAR(BgL_tmpz00_3560, BgL_auxz00_3562);
										}
									}
									{	/* Ast/hrtype.scm 200 */
										obj_t BgL_arg1627z00_2810;

										BgL_arg1627z00_2810 = CDR(((obj_t) BgL_l1253z00_2806));
										{
											obj_t BgL_l1253z00_3567;

											BgL_l1253z00_3567 = BgL_arg1627z00_2810;
											BgL_l1253z00_2806 = BgL_l1253z00_3567;
											goto BgL_zc3z04anonymousza31617ze3z87_2805;
										}
									}
								}
						}
					}
					((((BgL_newz00_bglt) COBJECT(
									((BgL_newz00_bglt) BgL_nodez00_2632)))->BgL_argszd2typezd2) =
						((obj_t) BgL_auxz00_3549), BUNSPEC);
				}
				{	/* Ast/hrtype.scm 198 */
					obj_t BgL_nextzd2method1288zd2_2803;

					BgL_nextzd2method1288zd2_2803 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_newz00_bglt) BgL_nodez00_2632)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_newz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1288zd2_2803,
						((obj_t) ((BgL_newz00_bglt) BgL_nodez00_2632)));
				}
			}
		}

	}



/* &hrtype-node!-widenin1287 */
	obj_t BGl_z62hrtypezd2nodez12zd2widenin1287z70zzast_hrtypez00(obj_t
		BgL_envz00_2633, obj_t BgL_nodez00_2634)
	{
		{	/* Ast/hrtype.scm 190 */
			{

				{
					BgL_typez00_bglt BgL_auxz00_3578;

					{	/* Ast/hrtype.scm 192 */
						obj_t BgL_arg1615z00_2814;

						BgL_arg1615z00_2814 =
							(((BgL_typez00_bglt) COBJECT(
									(((BgL_wideningz00_bglt) COBJECT(
												((BgL_wideningz00_bglt) BgL_nodez00_2634)))->
										BgL_otypez00)))->BgL_idz00);
						BgL_auxz00_3578 =
							BGl_findzd2typezd2zztype_envz00(BgL_arg1615z00_2814);
					}
					((((BgL_wideningz00_bglt) COBJECT(
									((BgL_wideningz00_bglt) BgL_nodez00_2634)))->BgL_otypez00) =
						((BgL_typez00_bglt) BgL_auxz00_3578), BUNSPEC);
				}
				{	/* Ast/hrtype.scm 190 */
					obj_t BgL_nextzd2method1286zd2_2813;

					BgL_nextzd2method1286zd2_2813 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_wideningz00_bglt) BgL_nodez00_2634)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_wideningz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1286zd2_2813,
						((obj_t) ((BgL_wideningz00_bglt) BgL_nodez00_2634)));
				}
			}
		}

	}



/* &hrtype-node!-setfiel1285 */
	obj_t BGl_z62hrtypezd2nodez12zd2setfiel1285z70zzast_hrtypez00(obj_t
		BgL_envz00_2635, obj_t BgL_nodez00_2636)
	{
		{	/* Ast/hrtype.scm 181 */
			{

				{
					BgL_typez00_bglt BgL_auxz00_3594;

					{	/* Ast/hrtype.scm 183 */
						obj_t BgL_arg1606z00_2818;

						BgL_arg1606z00_2818 =
							(((BgL_typez00_bglt) COBJECT(
									(((BgL_setfieldz00_bglt) COBJECT(
												((BgL_setfieldz00_bglt) BgL_nodez00_2636)))->
										BgL_ftypez00)))->BgL_idz00);
						BgL_auxz00_3594 =
							BGl_findzd2typezd2zztype_envz00(BgL_arg1606z00_2818);
					}
					((((BgL_setfieldz00_bglt) COBJECT(
									((BgL_setfieldz00_bglt) BgL_nodez00_2636)))->BgL_ftypez00) =
						((BgL_typez00_bglt) BgL_auxz00_3594), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_3601;

					{	/* Ast/hrtype.scm 184 */
						obj_t BgL_arg1611z00_2819;

						BgL_arg1611z00_2819 =
							(((BgL_typez00_bglt) COBJECT(
									(((BgL_setfieldz00_bglt) COBJECT(
												((BgL_setfieldz00_bglt) BgL_nodez00_2636)))->
										BgL_otypez00)))->BgL_idz00);
						BgL_auxz00_3601 =
							BGl_findzd2typezd2zztype_envz00(BgL_arg1611z00_2819);
					}
					((((BgL_setfieldz00_bglt) COBJECT(
									((BgL_setfieldz00_bglt) BgL_nodez00_2636)))->BgL_otypez00) =
						((BgL_typez00_bglt) BgL_auxz00_3601), BUNSPEC);
				}
				{	/* Ast/hrtype.scm 181 */
					obj_t BgL_nextzd2method1284zd2_2817;

					BgL_nextzd2method1284zd2_2817 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_setfieldz00_bglt) BgL_nodez00_2636)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_setfieldz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1284zd2_2817,
						((obj_t) ((BgL_setfieldz00_bglt) BgL_nodez00_2636)));
				}
			}
		}

	}



/* &hrtype-node!-getfiel1283 */
	obj_t BGl_z62hrtypezd2nodez12zd2getfiel1283z70zzast_hrtypez00(obj_t
		BgL_envz00_2637, obj_t BgL_nodez00_2638)
	{
		{	/* Ast/hrtype.scm 172 */
			{

				{
					BgL_typez00_bglt BgL_auxz00_3617;

					{	/* Ast/hrtype.scm 174 */
						obj_t BgL_arg1594z00_2823;

						BgL_arg1594z00_2823 =
							(((BgL_typez00_bglt) COBJECT(
									(((BgL_getfieldz00_bglt) COBJECT(
												((BgL_getfieldz00_bglt) BgL_nodez00_2638)))->
										BgL_ftypez00)))->BgL_idz00);
						BgL_auxz00_3617 =
							BGl_findzd2typezd2zztype_envz00(BgL_arg1594z00_2823);
					}
					((((BgL_getfieldz00_bglt) COBJECT(
									((BgL_getfieldz00_bglt) BgL_nodez00_2638)))->BgL_ftypez00) =
						((BgL_typez00_bglt) BgL_auxz00_3617), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_3624;

					{	/* Ast/hrtype.scm 175 */
						obj_t BgL_arg1602z00_2824;

						BgL_arg1602z00_2824 =
							(((BgL_typez00_bglt) COBJECT(
									(((BgL_getfieldz00_bglt) COBJECT(
												((BgL_getfieldz00_bglt) BgL_nodez00_2638)))->
										BgL_otypez00)))->BgL_idz00);
						BgL_auxz00_3624 =
							BGl_findzd2typezd2zztype_envz00(BgL_arg1602z00_2824);
					}
					((((BgL_getfieldz00_bglt) COBJECT(
									((BgL_getfieldz00_bglt) BgL_nodez00_2638)))->BgL_otypez00) =
						((BgL_typez00_bglt) BgL_auxz00_3624), BUNSPEC);
				}
				{	/* Ast/hrtype.scm 172 */
					obj_t BgL_nextzd2method1282zd2_2822;

					BgL_nextzd2method1282zd2_2822 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_getfieldz00_bglt) BgL_nodez00_2638)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_getfieldz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1282zd2_2822,
						((obj_t) ((BgL_getfieldz00_bglt) BgL_nodez00_2638)));
				}
			}
		}

	}



/* &hrtype-node!-extern1281 */
	obj_t BGl_z62hrtypezd2nodez12zd2extern1281z70zzast_hrtypez00(obj_t
		BgL_envz00_2639, obj_t BgL_nodez00_2640)
	{
		{	/* Ast/hrtype.scm 164 */
			{

				BGl_hrtypezd2nodeza2z12z62zzast_hrtypez00(
					(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_nodez00_2640)))->BgL_exprza2za2));
				{	/* Ast/hrtype.scm 164 */
					obj_t BgL_nextzd2method1280zd2_2827;

					BgL_nextzd2method1280zd2_2827 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_externz00_bglt) BgL_nodez00_2640)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_externz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1280zd2_2827,
						((obj_t) ((BgL_externz00_bglt) BgL_nodez00_2640)));
				}
			}
		}

	}



/* &hrtype-node!-funcall1279 */
	obj_t BGl_z62hrtypezd2nodez12zd2funcall1279z70zzast_hrtypez00(obj_t
		BgL_envz00_2641, obj_t BgL_nodez00_2642)
	{
		{	/* Ast/hrtype.scm 155 */
			{

				BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2642)))->BgL_funz00));
				BGl_hrtypezd2nodeza2z12z62zzast_hrtypez00(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2642)))->BgL_argsz00));
				{	/* Ast/hrtype.scm 155 */
					obj_t BgL_nextzd2method1278zd2_2830;

					BgL_nextzd2method1278zd2_2830 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_funcallz00_bglt) BgL_nodez00_2642)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_funcallz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1278zd2_2830,
						((obj_t) ((BgL_funcallz00_bglt) BgL_nodez00_2642)));
				}
			}
		}

	}



/* &hrtype-node!-app-ly1277 */
	obj_t BGl_z62hrtypezd2nodez12zd2appzd2ly1277za2zzast_hrtypez00(obj_t
		BgL_envz00_2643, obj_t BgL_nodez00_2644)
	{
		{	/* Ast/hrtype.scm 146 */
			{

				BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
					(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2644)))->BgL_funz00));
				BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
					(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2644)))->BgL_argz00));
				{	/* Ast/hrtype.scm 146 */
					obj_t BgL_nextzd2method1276zd2_2833;

					BgL_nextzd2method1276zd2_2833 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_2644)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_appzd2lyzd2zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1276zd2_2833,
						((obj_t) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_2644)));
				}
			}
		}

	}



/* &hrtype-node!-app1275 */
	obj_t BGl_z62hrtypezd2nodez12zd2app1275z70zzast_hrtypez00(obj_t
		BgL_envz00_2645, obj_t BgL_nodez00_2646)
	{
		{	/* Ast/hrtype.scm 95 */
			{

				{	/* Ast/hrtype.scm 97 */
					BgL_varz00_bglt BgL_i1108z00_2837;

					BgL_i1108z00_2837 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_2646)))->BgL_funz00);
					{	/* Ast/hrtype.scm 98 */
						bool_t BgL_test2010z00_3684;

						{	/* Ast/hrtype.scm 98 */
							BgL_variablez00_bglt BgL_arg1575z00_2838;

							BgL_arg1575z00_2838 =
								(((BgL_varz00_bglt) COBJECT(BgL_i1108z00_2837))->
								BgL_variablez00);
							{	/* Ast/hrtype.scm 98 */
								obj_t BgL_classz00_2839;

								BgL_classz00_2839 = BGl_globalz00zzast_varz00;
								{	/* Ast/hrtype.scm 98 */
									BgL_objectz00_bglt BgL_arg1807z00_2840;

									{	/* Ast/hrtype.scm 98 */
										obj_t BgL_tmpz00_3686;

										BgL_tmpz00_3686 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg1575z00_2838));
										BgL_arg1807z00_2840 =
											(BgL_objectz00_bglt) (BgL_tmpz00_3686);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Ast/hrtype.scm 98 */
											long BgL_idxz00_2841;

											BgL_idxz00_2841 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2840);
											BgL_test2010z00_3684 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2841 + 2L)) == BgL_classz00_2839);
										}
									else
										{	/* Ast/hrtype.scm 98 */
											bool_t BgL_res1897z00_2844;

											{	/* Ast/hrtype.scm 98 */
												obj_t BgL_oclassz00_2845;

												{	/* Ast/hrtype.scm 98 */
													obj_t BgL_arg1815z00_2846;
													long BgL_arg1816z00_2847;

													BgL_arg1815z00_2846 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Ast/hrtype.scm 98 */
														long BgL_arg1817z00_2848;

														BgL_arg1817z00_2848 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2840);
														BgL_arg1816z00_2847 =
															(BgL_arg1817z00_2848 - OBJECT_TYPE);
													}
													BgL_oclassz00_2845 =
														VECTOR_REF(BgL_arg1815z00_2846,
														BgL_arg1816z00_2847);
												}
												{	/* Ast/hrtype.scm 98 */
													bool_t BgL__ortest_1115z00_2849;

													BgL__ortest_1115z00_2849 =
														(BgL_classz00_2839 == BgL_oclassz00_2845);
													if (BgL__ortest_1115z00_2849)
														{	/* Ast/hrtype.scm 98 */
															BgL_res1897z00_2844 = BgL__ortest_1115z00_2849;
														}
													else
														{	/* Ast/hrtype.scm 98 */
															long BgL_odepthz00_2850;

															{	/* Ast/hrtype.scm 98 */
																obj_t BgL_arg1804z00_2851;

																BgL_arg1804z00_2851 = (BgL_oclassz00_2845);
																BgL_odepthz00_2850 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2851);
															}
															if ((2L < BgL_odepthz00_2850))
																{	/* Ast/hrtype.scm 98 */
																	obj_t BgL_arg1802z00_2852;

																	{	/* Ast/hrtype.scm 98 */
																		obj_t BgL_arg1803z00_2853;

																		BgL_arg1803z00_2853 = (BgL_oclassz00_2845);
																		BgL_arg1802z00_2852 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2853, 2L);
																	}
																	BgL_res1897z00_2844 =
																		(BgL_arg1802z00_2852 == BgL_classz00_2839);
																}
															else
																{	/* Ast/hrtype.scm 98 */
																	BgL_res1897z00_2844 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2010z00_3684 = BgL_res1897z00_2844;
										}
								}
							}
						}
						if (BgL_test2010z00_3684)
							{	/* Ast/hrtype.scm 99 */
								BgL_valuez00_bglt BgL_valuez00_2854;

								BgL_valuez00_2854 =
									(((BgL_variablez00_bglt) COBJECT(
											(((BgL_varz00_bglt) COBJECT(BgL_i1108z00_2837))->
												BgL_variablez00)))->BgL_valuez00);
								{	/* Ast/hrtype.scm 101 */
									bool_t BgL_test2014z00_3711;

									{	/* Ast/hrtype.scm 101 */
										obj_t BgL_classz00_2855;

										BgL_classz00_2855 = BGl_cfunz00zzast_varz00;
										{	/* Ast/hrtype.scm 101 */
											BgL_objectz00_bglt BgL_arg1807z00_2856;

											{	/* Ast/hrtype.scm 101 */
												obj_t BgL_tmpz00_3712;

												BgL_tmpz00_3712 =
													((obj_t) ((BgL_objectz00_bglt) BgL_valuez00_2854));
												BgL_arg1807z00_2856 =
													(BgL_objectz00_bglt) (BgL_tmpz00_3712);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Ast/hrtype.scm 101 */
													long BgL_idxz00_2857;

													BgL_idxz00_2857 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2856);
													BgL_test2014z00_3711 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_2857 + 3L)) == BgL_classz00_2855);
												}
											else
												{	/* Ast/hrtype.scm 101 */
													bool_t BgL_res1898z00_2860;

													{	/* Ast/hrtype.scm 101 */
														obj_t BgL_oclassz00_2861;

														{	/* Ast/hrtype.scm 101 */
															obj_t BgL_arg1815z00_2862;
															long BgL_arg1816z00_2863;

															BgL_arg1815z00_2862 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Ast/hrtype.scm 101 */
																long BgL_arg1817z00_2864;

																BgL_arg1817z00_2864 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2856);
																BgL_arg1816z00_2863 =
																	(BgL_arg1817z00_2864 - OBJECT_TYPE);
															}
															BgL_oclassz00_2861 =
																VECTOR_REF(BgL_arg1815z00_2862,
																BgL_arg1816z00_2863);
														}
														{	/* Ast/hrtype.scm 101 */
															bool_t BgL__ortest_1115z00_2865;

															BgL__ortest_1115z00_2865 =
																(BgL_classz00_2855 == BgL_oclassz00_2861);
															if (BgL__ortest_1115z00_2865)
																{	/* Ast/hrtype.scm 101 */
																	BgL_res1898z00_2860 =
																		BgL__ortest_1115z00_2865;
																}
															else
																{	/* Ast/hrtype.scm 101 */
																	long BgL_odepthz00_2866;

																	{	/* Ast/hrtype.scm 101 */
																		obj_t BgL_arg1804z00_2867;

																		BgL_arg1804z00_2867 = (BgL_oclassz00_2861);
																		BgL_odepthz00_2866 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_2867);
																	}
																	if ((3L < BgL_odepthz00_2866))
																		{	/* Ast/hrtype.scm 101 */
																			obj_t BgL_arg1802z00_2868;

																			{	/* Ast/hrtype.scm 101 */
																				obj_t BgL_arg1803z00_2869;

																				BgL_arg1803z00_2869 =
																					(BgL_oclassz00_2861);
																				BgL_arg1802z00_2868 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_2869, 3L);
																			}
																			BgL_res1898z00_2860 =
																				(BgL_arg1802z00_2868 ==
																				BgL_classz00_2855);
																		}
																	else
																		{	/* Ast/hrtype.scm 101 */
																			BgL_res1898z00_2860 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2014z00_3711 = BgL_res1898z00_2860;
												}
										}
									}
									if (BgL_test2014z00_3711)
										{	/* Ast/hrtype.scm 102 */
											obj_t BgL_gz00_2870;

											{	/* Ast/hrtype.scm 102 */
												obj_t BgL_arg1453z00_2871;
												obj_t BgL_arg1454z00_2872;

												BgL_arg1453z00_2871 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_globalz00_bglt)
																	(((BgL_varz00_bglt)
																			COBJECT(BgL_i1108z00_2837))->
																		BgL_variablez00)))))->BgL_idz00);
												BgL_arg1454z00_2872 =
													(((BgL_globalz00_bglt)
														COBJECT(((BgL_globalz00_bglt) (((BgL_varz00_bglt)
																		COBJECT(BgL_i1108z00_2837))->
																	BgL_variablez00))))->BgL_modulez00);
												{	/* Ast/hrtype.scm 102 */
													obj_t BgL_list1455z00_2873;

													BgL_list1455z00_2873 =
														MAKE_YOUNG_PAIR(BgL_arg1454z00_2872, BNIL);
													BgL_gz00_2870 =
														BGl_findzd2globalzd2zzast_envz00
														(BgL_arg1453z00_2871, BgL_list1455z00_2873);
												}
											}
											if (CBOOL(BgL_gz00_2870))
												{	/* Ast/hrtype.scm 112 */
													((((BgL_varz00_bglt) COBJECT(BgL_i1108z00_2837))->
															BgL_variablez00) =
														((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
																BgL_gz00_2870)), BUNSPEC);
												}
											else
												{	/* Ast/hrtype.scm 114 */
													BgL_variablez00_bglt BgL_arg1448z00_2874;

													BgL_arg1448z00_2874 =
														(((BgL_varz00_bglt) COBJECT(BgL_i1108z00_2837))->
														BgL_variablez00);
													BGl_restorezd2globalz12zc0zzast_envz00(((obj_t)
															BgL_arg1448z00_2874));
												}
										}
									else
										{	/* Ast/hrtype.scm 115 */
											bool_t BgL_test2019z00_3751;

											{	/* Ast/hrtype.scm 115 */
												obj_t BgL_classz00_2875;

												BgL_classz00_2875 = BGl_sfunz00zzast_varz00;
												{	/* Ast/hrtype.scm 115 */
													BgL_objectz00_bglt BgL_arg1807z00_2876;

													{	/* Ast/hrtype.scm 115 */
														obj_t BgL_tmpz00_3752;

														BgL_tmpz00_3752 =
															((obj_t)
															((BgL_objectz00_bglt) BgL_valuez00_2854));
														BgL_arg1807z00_2876 =
															(BgL_objectz00_bglt) (BgL_tmpz00_3752);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Ast/hrtype.scm 115 */
															long BgL_idxz00_2877;

															BgL_idxz00_2877 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2876);
															BgL_test2019z00_3751 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_2877 + 3L)) == BgL_classz00_2875);
														}
													else
														{	/* Ast/hrtype.scm 115 */
															bool_t BgL_res1899z00_2880;

															{	/* Ast/hrtype.scm 115 */
																obj_t BgL_oclassz00_2881;

																{	/* Ast/hrtype.scm 115 */
																	obj_t BgL_arg1815z00_2882;
																	long BgL_arg1816z00_2883;

																	BgL_arg1815z00_2882 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Ast/hrtype.scm 115 */
																		long BgL_arg1817z00_2884;

																		BgL_arg1817z00_2884 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2876);
																		BgL_arg1816z00_2883 =
																			(BgL_arg1817z00_2884 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_2881 =
																		VECTOR_REF(BgL_arg1815z00_2882,
																		BgL_arg1816z00_2883);
																}
																{	/* Ast/hrtype.scm 115 */
																	bool_t BgL__ortest_1115z00_2885;

																	BgL__ortest_1115z00_2885 =
																		(BgL_classz00_2875 == BgL_oclassz00_2881);
																	if (BgL__ortest_1115z00_2885)
																		{	/* Ast/hrtype.scm 115 */
																			BgL_res1899z00_2880 =
																				BgL__ortest_1115z00_2885;
																		}
																	else
																		{	/* Ast/hrtype.scm 115 */
																			long BgL_odepthz00_2886;

																			{	/* Ast/hrtype.scm 115 */
																				obj_t BgL_arg1804z00_2887;

																				BgL_arg1804z00_2887 =
																					(BgL_oclassz00_2881);
																				BgL_odepthz00_2886 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_2887);
																			}
																			if ((3L < BgL_odepthz00_2886))
																				{	/* Ast/hrtype.scm 115 */
																					obj_t BgL_arg1802z00_2888;

																					{	/* Ast/hrtype.scm 115 */
																						obj_t BgL_arg1803z00_2889;

																						BgL_arg1803z00_2889 =
																							(BgL_oclassz00_2881);
																						BgL_arg1802z00_2888 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_2889, 3L);
																					}
																					BgL_res1899z00_2880 =
																						(BgL_arg1802z00_2888 ==
																						BgL_classz00_2875);
																				}
																			else
																				{	/* Ast/hrtype.scm 115 */
																					BgL_res1899z00_2880 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2019z00_3751 = BgL_res1899z00_2880;
														}
												}
											}
											if (BgL_test2019z00_3751)
												{	/* Ast/hrtype.scm 116 */
													bool_t BgL_test2023z00_3775;

													if (
														((((BgL_sfunz00_bglt) COBJECT(
																		((BgL_sfunz00_bglt) BgL_valuez00_2854)))->
																BgL_classz00) == CNST_TABLE_REF(0)))
														{	/* Ast/hrtype.scm 118 */
															bool_t BgL_test2025z00_3781;

															{	/* Ast/hrtype.scm 118 */
																BgL_variablez00_bglt BgL_arg1565z00_2890;

																BgL_arg1565z00_2890 =
																	(((BgL_varz00_bglt)
																		COBJECT(BgL_i1108z00_2837))->
																	BgL_variablez00);
																BgL_test2025z00_3781 =
																	CBOOL(BGl_alreadyzd2restoredzf3z21zzast_envz00
																	(((obj_t) BgL_arg1565z00_2890)));
															}
															if (BgL_test2025z00_3781)
																{	/* Ast/hrtype.scm 118 */
																	BgL_test2023z00_3775 = ((bool_t) 0);
																}
															else
																{	/* Ast/hrtype.scm 118 */
																	BgL_test2023z00_3775 = ((bool_t) 1);
																}
														}
													else
														{	/* Ast/hrtype.scm 116 */
															BgL_test2023z00_3775 = ((bool_t) 0);
														}
													if (BgL_test2023z00_3775)
														{	/* Ast/hrtype.scm 122 */
															BgL_variablez00_bglt BgL_arg1513z00_2891;

															BgL_arg1513z00_2891 =
																(((BgL_varz00_bglt)
																	COBJECT(BgL_i1108z00_2837))->BgL_variablez00);
															BGl_restorezd2globalz12zc0zzast_envz00(((obj_t)
																	BgL_arg1513z00_2891));
														}
													else
														{	/* Ast/hrtype.scm 128 */
															obj_t BgL_gz00_2892;

															{	/* Ast/hrtype.scm 128 */
																obj_t BgL_arg1553z00_2893;
																obj_t BgL_arg1559z00_2894;

																BgL_arg1553z00_2893 =
																	(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_globalz00_bglt)
																					(((BgL_varz00_bglt)
																							COBJECT(BgL_i1108z00_2837))->
																						BgL_variablez00)))))->BgL_idz00);
																BgL_arg1559z00_2894 =
																	(((BgL_globalz00_bglt)
																		COBJECT(((BgL_globalz00_bglt) ((
																						(BgL_varz00_bglt)
																						COBJECT(BgL_i1108z00_2837))->
																					BgL_variablez00))))->BgL_modulez00);
																{	/* Ast/hrtype.scm 128 */
																	obj_t BgL_list1560z00_2895;

																	BgL_list1560z00_2895 =
																		MAKE_YOUNG_PAIR(BgL_arg1559z00_2894, BNIL);
																	BgL_gz00_2892 =
																		BGl_findzd2globalzd2zzast_envz00
																		(BgL_arg1553z00_2893, BgL_list1560z00_2895);
																}
															}
															{	/* Ast/hrtype.scm 130 */
																bool_t BgL_test2026z00_3798;

																{	/* Ast/hrtype.scm 130 */
																	obj_t BgL_classz00_2896;

																	BgL_classz00_2896 =
																		BGl_variablez00zzast_varz00;
																	if (BGL_OBJECTP(BgL_gz00_2892))
																		{	/* Ast/hrtype.scm 130 */
																			BgL_objectz00_bglt BgL_arg1807z00_2897;

																			BgL_arg1807z00_2897 =
																				(BgL_objectz00_bglt) (BgL_gz00_2892);
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Ast/hrtype.scm 130 */
																					long BgL_idxz00_2898;

																					BgL_idxz00_2898 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_2897);
																					BgL_test2026z00_3798 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_2898 + 1L)) ==
																						BgL_classz00_2896);
																				}
																			else
																				{	/* Ast/hrtype.scm 130 */
																					bool_t BgL_res1900z00_2901;

																					{	/* Ast/hrtype.scm 130 */
																						obj_t BgL_oclassz00_2902;

																						{	/* Ast/hrtype.scm 130 */
																							obj_t BgL_arg1815z00_2903;
																							long BgL_arg1816z00_2904;

																							BgL_arg1815z00_2903 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Ast/hrtype.scm 130 */
																								long BgL_arg1817z00_2905;

																								BgL_arg1817z00_2905 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_2897);
																								BgL_arg1816z00_2904 =
																									(BgL_arg1817z00_2905 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_2902 =
																								VECTOR_REF(BgL_arg1815z00_2903,
																								BgL_arg1816z00_2904);
																						}
																						{	/* Ast/hrtype.scm 130 */
																							bool_t BgL__ortest_1115z00_2906;

																							BgL__ortest_1115z00_2906 =
																								(BgL_classz00_2896 ==
																								BgL_oclassz00_2902);
																							if (BgL__ortest_1115z00_2906)
																								{	/* Ast/hrtype.scm 130 */
																									BgL_res1900z00_2901 =
																										BgL__ortest_1115z00_2906;
																								}
																							else
																								{	/* Ast/hrtype.scm 130 */
																									long BgL_odepthz00_2907;

																									{	/* Ast/hrtype.scm 130 */
																										obj_t BgL_arg1804z00_2908;

																										BgL_arg1804z00_2908 =
																											(BgL_oclassz00_2902);
																										BgL_odepthz00_2907 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_2908);
																									}
																									if ((1L < BgL_odepthz00_2907))
																										{	/* Ast/hrtype.scm 130 */
																											obj_t BgL_arg1802z00_2909;

																											{	/* Ast/hrtype.scm 130 */
																												obj_t
																													BgL_arg1803z00_2910;
																												BgL_arg1803z00_2910 =
																													(BgL_oclassz00_2902);
																												BgL_arg1802z00_2909 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_2910,
																													1L);
																											}
																											BgL_res1900z00_2901 =
																												(BgL_arg1802z00_2909 ==
																												BgL_classz00_2896);
																										}
																									else
																										{	/* Ast/hrtype.scm 130 */
																											BgL_res1900z00_2901 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test2026z00_3798 =
																						BgL_res1900z00_2901;
																				}
																		}
																	else
																		{	/* Ast/hrtype.scm 130 */
																			BgL_test2026z00_3798 = ((bool_t) 0);
																		}
																}
																if (BgL_test2026z00_3798)
																	{	/* Ast/hrtype.scm 130 */
																		((((BgL_varz00_bglt)
																					COBJECT(BgL_i1108z00_2837))->
																				BgL_variablez00) =
																			((BgL_variablez00_bglt) (
																					(BgL_variablez00_bglt)
																					BgL_gz00_2892)), BUNSPEC);
																	}
																else
																	{	/* Ast/hrtype.scm 131 */
																		obj_t BgL_newzd2gzd2_2911;

																		{	/* Ast/hrtype.scm 131 */
																			obj_t BgL_arg1546z00_2912;

																			BgL_arg1546z00_2912 =
																				(((BgL_variablez00_bglt) COBJECT(
																						((BgL_variablez00_bglt)
																							((BgL_globalz00_bglt)
																								(((BgL_varz00_bglt)
																										COBJECT
																										(BgL_i1108z00_2837))->
																									BgL_variablez00)))))->
																				BgL_idz00);
																			{	/* Ast/hrtype.scm 131 */
																				obj_t BgL_list1547z00_2913;

																				BgL_list1547z00_2913 =
																					MAKE_YOUNG_PAIR
																					(BGl_za2moduleza2z00zzmodule_modulez00,
																					BNIL);
																				BgL_newzd2gzd2_2911 =
																					BGl_findzd2globalzd2zzast_envz00
																					(BgL_arg1546z00_2912,
																					BgL_list1547z00_2913);
																			}
																		}
																		{	/* Ast/hrtype.scm 133 */
																			bool_t BgL_test2031z00_3829;

																			{	/* Ast/hrtype.scm 133 */
																				bool_t BgL_test2032z00_3830;

																				{	/* Ast/hrtype.scm 133 */
																					obj_t BgL_classz00_2914;

																					BgL_classz00_2914 =
																						BGl_globalz00zzast_varz00;
																					if (BGL_OBJECTP(BgL_newzd2gzd2_2911))
																						{	/* Ast/hrtype.scm 133 */
																							BgL_objectz00_bglt
																								BgL_arg1807z00_2915;
																							BgL_arg1807z00_2915 =
																								(BgL_objectz00_bglt)
																								(BgL_newzd2gzd2_2911);
																							if (BGL_CONDEXPAND_ISA_ARCH64())
																								{	/* Ast/hrtype.scm 133 */
																									long BgL_idxz00_2916;

																									BgL_idxz00_2916 =
																										BGL_OBJECT_INHERITANCE_NUM
																										(BgL_arg1807z00_2915);
																									BgL_test2032z00_3830 =
																										(VECTOR_REF
																										(BGl_za2inheritancesza2z00zz__objectz00,
																											(BgL_idxz00_2916 + 2L)) ==
																										BgL_classz00_2914);
																								}
																							else
																								{	/* Ast/hrtype.scm 133 */
																									bool_t BgL_res1901z00_2919;

																									{	/* Ast/hrtype.scm 133 */
																										obj_t BgL_oclassz00_2920;

																										{	/* Ast/hrtype.scm 133 */
																											obj_t BgL_arg1815z00_2921;
																											long BgL_arg1816z00_2922;

																											BgL_arg1815z00_2921 =
																												(BGl_za2classesza2z00zz__objectz00);
																											{	/* Ast/hrtype.scm 133 */
																												long
																													BgL_arg1817z00_2923;
																												BgL_arg1817z00_2923 =
																													BGL_OBJECT_CLASS_NUM
																													(BgL_arg1807z00_2915);
																												BgL_arg1816z00_2922 =
																													(BgL_arg1817z00_2923 -
																													OBJECT_TYPE);
																											}
																											BgL_oclassz00_2920 =
																												VECTOR_REF
																												(BgL_arg1815z00_2921,
																												BgL_arg1816z00_2922);
																										}
																										{	/* Ast/hrtype.scm 133 */
																											bool_t
																												BgL__ortest_1115z00_2924;
																											BgL__ortest_1115z00_2924 =
																												(BgL_classz00_2914 ==
																												BgL_oclassz00_2920);
																											if (BgL__ortest_1115z00_2924)
																												{	/* Ast/hrtype.scm 133 */
																													BgL_res1901z00_2919 =
																														BgL__ortest_1115z00_2924;
																												}
																											else
																												{	/* Ast/hrtype.scm 133 */
																													long
																														BgL_odepthz00_2925;
																													{	/* Ast/hrtype.scm 133 */
																														obj_t
																															BgL_arg1804z00_2926;
																														BgL_arg1804z00_2926
																															=
																															(BgL_oclassz00_2920);
																														BgL_odepthz00_2925 =
																															BGL_CLASS_DEPTH
																															(BgL_arg1804z00_2926);
																													}
																													if (
																														(2L <
																															BgL_odepthz00_2925))
																														{	/* Ast/hrtype.scm 133 */
																															obj_t
																																BgL_arg1802z00_2927;
																															{	/* Ast/hrtype.scm 133 */
																																obj_t
																																	BgL_arg1803z00_2928;
																																BgL_arg1803z00_2928
																																	=
																																	(BgL_oclassz00_2920);
																																BgL_arg1802z00_2927
																																	=
																																	BGL_CLASS_ANCESTORS_REF
																																	(BgL_arg1803z00_2928,
																																	2L);
																															}
																															BgL_res1901z00_2919
																																=
																																(BgL_arg1802z00_2927
																																==
																																BgL_classz00_2914);
																														}
																													else
																														{	/* Ast/hrtype.scm 133 */
																															BgL_res1901z00_2919
																																= ((bool_t) 0);
																														}
																												}
																										}
																									}
																									BgL_test2032z00_3830 =
																										BgL_res1901z00_2919;
																								}
																						}
																					else
																						{	/* Ast/hrtype.scm 133 */
																							BgL_test2032z00_3830 =
																								((bool_t) 0);
																						}
																				}
																				if (BgL_test2032z00_3830)
																					{	/* Ast/hrtype.scm 133 */
																						BgL_test2031z00_3829 =
																							(
																							(((BgL_globalz00_bglt) COBJECT(
																										((BgL_globalz00_bglt)
																											BgL_newzd2gzd2_2911)))->
																								BgL_importz00) ==
																							CNST_TABLE_REF(1));
																					}
																				else
																					{	/* Ast/hrtype.scm 133 */
																						BgL_test2031z00_3829 = ((bool_t) 0);
																					}
																			}
																			if (BgL_test2031z00_3829)
																				{	/* Ast/hrtype.scm 133 */
																					((((BgL_varz00_bglt)
																								COBJECT(BgL_i1108z00_2837))->
																							BgL_variablez00) =
																						((BgL_variablez00_bglt) (
																								(BgL_variablez00_bglt)
																								BgL_newzd2gzd2_2911)), BUNSPEC);
																				}
																			else
																				{	/* Ast/hrtype.scm 137 */
																					obj_t BgL_arg1535z00_2929;

																					BgL_arg1535z00_2929 =
																						(((BgL_variablez00_bglt) COBJECT(
																								((BgL_variablez00_bglt)
																									((BgL_globalz00_bglt)
																										(((BgL_varz00_bglt)
																												COBJECT
																												(BgL_i1108z00_2837))->
																											BgL_variablez00)))))->
																						BgL_idz00);
																					BGl_errorz00zz__errorz00
																						(BGl_string1942z00zzast_hrtypez00,
																						BGl_string1943z00zzast_hrtypez00,
																						BgL_arg1535z00_2929);
																				}
																		}
																	}
															}
														}
												}
											else
												{	/* Ast/hrtype.scm 115 */
													BFALSE;
												}
										}
								}
							}
						else
							{	/* Ast/hrtype.scm 98 */
								BFALSE;
							}
					}
				}
				BGl_hrtypezd2nodeza2z12z62zzast_hrtypez00(
					(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_2646)))->BgL_argsz00));
				{	/* Ast/hrtype.scm 95 */
					obj_t BgL_nextzd2method1274zd2_2836;

					BgL_nextzd2method1274zd2_2836 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_appz00_bglt) BgL_nodez00_2646)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_appz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1274zd2_2836,
						((obj_t) ((BgL_appz00_bglt) BgL_nodez00_2646)));
				}
			}
		}

	}



/* &hrtype-node!-sync1273 */
	obj_t BGl_z62hrtypezd2nodez12zd2sync1273z70zzast_hrtypez00(obj_t
		BgL_envz00_2647, obj_t BgL_nodez00_2648)
	{
		{	/* Ast/hrtype.scm 85 */
			{

				BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
					(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2648)))->BgL_mutexz00));
				BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
					(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2648)))->BgL_prelockz00));
				BGl_hrtypezd2nodez12zc0zzast_hrtypez00(
					(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2648)))->BgL_bodyz00));
				{	/* Ast/hrtype.scm 85 */
					obj_t BgL_nextzd2method1272zd2_2932;

					BgL_nextzd2method1272zd2_2932 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_syncz00_bglt) BgL_nodez00_2648)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_syncz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1272zd2_2932,
						((obj_t) ((BgL_syncz00_bglt) BgL_nodez00_2648)));
				}
			}
		}

	}



/* &hrtype-node!-sequenc1271 */
	obj_t BGl_z62hrtypezd2nodez12zd2sequenc1271z70zzast_hrtypez00(obj_t
		BgL_envz00_2649, obj_t BgL_nodez00_2650)
	{
		{	/* Ast/hrtype.scm 77 */
			{

				BGl_hrtypezd2nodeza2z12z62zzast_hrtypez00(
					(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_2650)))->BgL_nodesz00));
				{	/* Ast/hrtype.scm 77 */
					obj_t BgL_nextzd2method1270zd2_2935;

					BgL_nextzd2method1270zd2_2935 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_sequencez00_bglt) BgL_nodez00_2650)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_sequencez00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1270zd2_2935,
						((obj_t) ((BgL_sequencez00_bglt) BgL_nodez00_2650)));
				}
			}
		}

	}



/* &hrtype-node!-var1269 */
	obj_t BGl_z62hrtypezd2nodez12zd2var1269z70zzast_hrtypez00(obj_t
		BgL_envz00_2651, obj_t BgL_nodez00_2652)
	{
		{	/* Ast/hrtype.scm 40 */
			{

				{	/* Ast/hrtype.scm 65 */
					bool_t BgL_test2037z00_3906;

					{	/* Ast/hrtype.scm 65 */
						bool_t BgL_test2038z00_3907;

						{	/* Ast/hrtype.scm 65 */
							BgL_variablez00_bglt BgL_arg1408z00_2939;

							BgL_arg1408z00_2939 =
								(((BgL_varz00_bglt) COBJECT(
										((BgL_varz00_bglt) BgL_nodez00_2652)))->BgL_variablez00);
							{	/* Ast/hrtype.scm 65 */
								obj_t BgL_classz00_2940;

								BgL_classz00_2940 = BGl_globalz00zzast_varz00;
								{	/* Ast/hrtype.scm 65 */
									BgL_objectz00_bglt BgL_arg1807z00_2941;

									{	/* Ast/hrtype.scm 65 */
										obj_t BgL_tmpz00_3910;

										BgL_tmpz00_3910 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg1408z00_2939));
										BgL_arg1807z00_2941 =
											(BgL_objectz00_bglt) (BgL_tmpz00_3910);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Ast/hrtype.scm 65 */
											long BgL_idxz00_2942;

											BgL_idxz00_2942 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2941);
											BgL_test2038z00_3907 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2942 + 2L)) == BgL_classz00_2940);
										}
									else
										{	/* Ast/hrtype.scm 65 */
											bool_t BgL_res1895z00_2945;

											{	/* Ast/hrtype.scm 65 */
												obj_t BgL_oclassz00_2946;

												{	/* Ast/hrtype.scm 65 */
													obj_t BgL_arg1815z00_2947;
													long BgL_arg1816z00_2948;

													BgL_arg1815z00_2947 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Ast/hrtype.scm 65 */
														long BgL_arg1817z00_2949;

														BgL_arg1817z00_2949 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2941);
														BgL_arg1816z00_2948 =
															(BgL_arg1817z00_2949 - OBJECT_TYPE);
													}
													BgL_oclassz00_2946 =
														VECTOR_REF(BgL_arg1815z00_2947,
														BgL_arg1816z00_2948);
												}
												{	/* Ast/hrtype.scm 65 */
													bool_t BgL__ortest_1115z00_2950;

													BgL__ortest_1115z00_2950 =
														(BgL_classz00_2940 == BgL_oclassz00_2946);
													if (BgL__ortest_1115z00_2950)
														{	/* Ast/hrtype.scm 65 */
															BgL_res1895z00_2945 = BgL__ortest_1115z00_2950;
														}
													else
														{	/* Ast/hrtype.scm 65 */
															long BgL_odepthz00_2951;

															{	/* Ast/hrtype.scm 65 */
																obj_t BgL_arg1804z00_2952;

																BgL_arg1804z00_2952 = (BgL_oclassz00_2946);
																BgL_odepthz00_2951 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2952);
															}
															if ((2L < BgL_odepthz00_2951))
																{	/* Ast/hrtype.scm 65 */
																	obj_t BgL_arg1802z00_2953;

																	{	/* Ast/hrtype.scm 65 */
																		obj_t BgL_arg1803z00_2954;

																		BgL_arg1803z00_2954 = (BgL_oclassz00_2946);
																		BgL_arg1802z00_2953 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2954, 2L);
																	}
																	BgL_res1895z00_2945 =
																		(BgL_arg1802z00_2953 == BgL_classz00_2940);
																}
															else
																{	/* Ast/hrtype.scm 65 */
																	BgL_res1895z00_2945 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2038z00_3907 = BgL_res1895z00_2945;
										}
								}
							}
						}
						if (BgL_test2038z00_3907)
							{	/* Ast/hrtype.scm 66 */
								obj_t BgL_arg1377z00_2955;
								obj_t BgL_arg1378z00_2956;

								BgL_arg1377z00_2955 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt)
													(((BgL_varz00_bglt) COBJECT(
																((BgL_varz00_bglt) BgL_nodez00_2652)))->
														BgL_variablez00)))))->BgL_idz00);
								BgL_arg1378z00_2956 =
									(((BgL_globalz00_bglt)
										COBJECT(((BgL_globalz00_bglt) (((BgL_varz00_bglt)
														COBJECT(((BgL_varz00_bglt) BgL_nodez00_2652)))->
													BgL_variablez00))))->BgL_modulez00);
								BgL_test2037z00_3906 =
									CBOOL(BGl_globalzd2bucketzd2positionz00zzast_envz00
									(BgL_arg1377z00_2955, BgL_arg1378z00_2956));
							}
						else
							{	/* Ast/hrtype.scm 65 */
								BgL_test2037z00_3906 = ((bool_t) 0);
							}
					}
					if (BgL_test2037z00_3906)
						{	/* Ast/hrtype.scm 68 */
							obj_t BgL_nz00_2957;

							{	/* Ast/hrtype.scm 68 */
								obj_t BgL_arg1370z00_2958;
								obj_t BgL_arg1371z00_2959;

								BgL_arg1370z00_2958 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt)
													(((BgL_varz00_bglt) COBJECT(
																((BgL_varz00_bglt) BgL_nodez00_2652)))->
														BgL_variablez00)))))->BgL_idz00);
								BgL_arg1371z00_2959 =
									(((BgL_globalz00_bglt)
										COBJECT(((BgL_globalz00_bglt) (((BgL_varz00_bglt)
														COBJECT(((BgL_varz00_bglt) BgL_nodez00_2652)))->
													BgL_variablez00))))->BgL_modulez00);
								BgL_nz00_2957 =
									BGl_findzd2globalzf2modulez20zzast_envz00(BgL_arg1370z00_2958,
									BgL_arg1371z00_2959);
							}
							{	/* Ast/hrtype.scm 70 */
								bool_t BgL_test2042z00_3954;

								{	/* Ast/hrtype.scm 70 */
									obj_t BgL_classz00_2960;

									BgL_classz00_2960 = BGl_globalz00zzast_varz00;
									if (BGL_OBJECTP(BgL_nz00_2957))
										{	/* Ast/hrtype.scm 70 */
											BgL_objectz00_bglt BgL_arg1807z00_2961;

											BgL_arg1807z00_2961 =
												(BgL_objectz00_bglt) (BgL_nz00_2957);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Ast/hrtype.scm 70 */
													long BgL_idxz00_2962;

													BgL_idxz00_2962 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2961);
													BgL_test2042z00_3954 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_2962 + 2L)) == BgL_classz00_2960);
												}
											else
												{	/* Ast/hrtype.scm 70 */
													bool_t BgL_res1896z00_2965;

													{	/* Ast/hrtype.scm 70 */
														obj_t BgL_oclassz00_2966;

														{	/* Ast/hrtype.scm 70 */
															obj_t BgL_arg1815z00_2967;
															long BgL_arg1816z00_2968;

															BgL_arg1815z00_2967 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Ast/hrtype.scm 70 */
																long BgL_arg1817z00_2969;

																BgL_arg1817z00_2969 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2961);
																BgL_arg1816z00_2968 =
																	(BgL_arg1817z00_2969 - OBJECT_TYPE);
															}
															BgL_oclassz00_2966 =
																VECTOR_REF(BgL_arg1815z00_2967,
																BgL_arg1816z00_2968);
														}
														{	/* Ast/hrtype.scm 70 */
															bool_t BgL__ortest_1115z00_2970;

															BgL__ortest_1115z00_2970 =
																(BgL_classz00_2960 == BgL_oclassz00_2966);
															if (BgL__ortest_1115z00_2970)
																{	/* Ast/hrtype.scm 70 */
																	BgL_res1896z00_2965 =
																		BgL__ortest_1115z00_2970;
																}
															else
																{	/* Ast/hrtype.scm 70 */
																	long BgL_odepthz00_2971;

																	{	/* Ast/hrtype.scm 70 */
																		obj_t BgL_arg1804z00_2972;

																		BgL_arg1804z00_2972 = (BgL_oclassz00_2966);
																		BgL_odepthz00_2971 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_2972);
																	}
																	if ((2L < BgL_odepthz00_2971))
																		{	/* Ast/hrtype.scm 70 */
																			obj_t BgL_arg1802z00_2973;

																			{	/* Ast/hrtype.scm 70 */
																				obj_t BgL_arg1803z00_2974;

																				BgL_arg1803z00_2974 =
																					(BgL_oclassz00_2966);
																				BgL_arg1802z00_2973 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_2974, 2L);
																			}
																			BgL_res1896z00_2965 =
																				(BgL_arg1802z00_2973 ==
																				BgL_classz00_2960);
																		}
																	else
																		{	/* Ast/hrtype.scm 70 */
																			BgL_res1896z00_2965 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2042z00_3954 = BgL_res1896z00_2965;
												}
										}
									else
										{	/* Ast/hrtype.scm 70 */
											BgL_test2042z00_3954 = ((bool_t) 0);
										}
								}
								if (BgL_test2042z00_3954)
									{	/* Ast/hrtype.scm 70 */
										((((BgL_varz00_bglt) COBJECT(
														((BgL_varz00_bglt) BgL_nodez00_2652)))->
												BgL_variablez00) =
											((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
													BgL_nz00_2957)), BUNSPEC);
									}
								else
									{	/* Ast/hrtype.scm 70 */
										BFALSE;
									}
							}
						}
					else
						{	/* Ast/hrtype.scm 65 */
							BFALSE;
						}
				}
				{	/* Ast/hrtype.scm 40 */
					obj_t BgL_nextzd2method1268zd2_2938;

					BgL_nextzd2method1268zd2_2938 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_varz00_bglt) BgL_nodez00_2652)),
						BGl_hrtypezd2nodez12zd2envz12zzast_hrtypez00,
						BGl_varz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1268zd2_2938,
						((obj_t) ((BgL_varz00_bglt) BgL_nodez00_2652)));
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_hrtypez00(void)
	{
		{	/* Ast/hrtype.scm 17 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1944z00zzast_hrtypez00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1944z00zzast_hrtypez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1944z00zzast_hrtypez00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1944z00zzast_hrtypez00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1944z00zzast_hrtypez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1944z00zzast_hrtypez00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1944z00zzast_hrtypez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1944z00zzast_hrtypez00));
			return
				BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string1944z00zzast_hrtypez00));
		}

	}

#ifdef __cplusplus
}
#endif
