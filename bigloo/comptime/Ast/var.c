/*===========================================================================*/
/*   (Ast/var.scm)                                                           */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/var.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_AST_VAR_TYPE_DEFINITIONS
#define BGL_AST_VAR_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_scnstz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_nodez00;
		obj_t BgL_classz00;
		obj_t BgL_locz00;
	}               *BgL_scnstz00_bglt;

	typedef struct BgL_cvarz00_bgl
	{
		header_t header;
		obj_t widening;
		bool_t BgL_macrozf3zf3;
	}              *BgL_cvarz00_bglt;

	typedef struct BgL_sexitz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_handlerz00;
		bool_t BgL_detachedzf3zf3;
	}               *BgL_sexitz00_bglt;

	typedef struct BgL_feffectz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_readz00;
		obj_t BgL_writez00;
	}                 *BgL_feffectz00_bglt;


#endif													// BGL_AST_VAR_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static BgL_funz00_bglt BGl_z62lambda1846z62zzast_varz00(obj_t);
	static BgL_sfunz00_bglt BGl_z62lambda1929z62zzast_varz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2modulezd2setz12z12zzast_varz00(BgL_globalz00_bglt, obj_t);
	static obj_t BGl_z62lambda1689z62zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sfunzd2propertyzd2zzast_varz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62globalzd2initzd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	static BgL_sfunz00_bglt BGl_z62lambda1931z62zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzd2removablezd2zzast_varz00(BgL_localz00_bglt);
	static obj_t BGl_z62lambda1690z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1852z62zzast_varz00(obj_t, obj_t);
	static BgL_variablez00_bglt BGl_z62makezd2variablezb0zzast_varz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1853z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31579ze3ze5zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_localzd2namezd2zzast_varz00(BgL_localz00_bglt);
	static BgL_localz00_bglt BGl_z62lambda1774z62zzast_varz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62localzd2removablezb0zzast_varz00(obj_t, obj_t);
	static BgL_localz00_bglt BGl_z62lambda1776z62zzast_varz00(obj_t);
	static obj_t BGl_z62lambda1938z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1939z62zzast_varz00(obj_t, obj_t, obj_t);
	static BgL_cvarz00_bglt BGl_z62makezd2cvarzb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1859z62zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_funzd2argszd2noescapez00zzast_varz00(BgL_funz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_svarzd2loczd2setz12z12zzast_varz00(BgL_svarz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_globalzd2srczd2zzast_varz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_sfunzd2loczd2zzast_varz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62funzd2topzf3zd2setz12z83zzast_varz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_z62cvarzd2macrozf3z43zzast_varz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzast_varz00 = BUNSPEC;
	static BgL_svarz00_bglt BGl_z62svarzd2nilzb0zzast_varz00(obj_t);
	static obj_t BGl_z62sfunzd2stackablezd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzd2fastzd2alphazd2setz12zc0zzast_varz00(BgL_localz00_bglt, obj_t);
	static obj_t BGl_z62lambda1860z62zzast_varz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzd2sidezd2effectz00zzast_varz00(BgL_cfunz00_bglt);
	static obj_t BGl_z62lambda1944z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1945z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62variablezd2namezb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62globalzd2valuezd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62globalzd2removablezd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzd2occurrencezd2setz12z12zzast_varz00(BgL_localz00_bglt, long);
	static obj_t BGl_z62lambda1867z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1868z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1949z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62localzd2occurrencewzb0zzast_varz00(obj_t, obj_t);
	static BgL_funz00_bglt BGl_z62funzd2nilzb0zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sfunzd2classzd2zzast_varz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2evalzf3zd2setz12ze1zzast_varz00(BgL_globalz00_bglt, bool_t);
	static obj_t BGl_z62funzd2stackzd2allocatorz62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62cfunzd2argszd2noescapezd2setz12za2zzast_varz00(obj_t,
		obj_t, obj_t);
	static BgL_globalz00_bglt BGl_z62makezd2globalzb0zzast_varz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_sexitz00_bglt BGl_z62sexitzd2nilzb0zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzd2accesszd2setz12z12zzast_varz00(BgL_localz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_variablezd2accesszd2zzast_varz00(BgL_variablez00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32012ze3ze5zzast_varz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32004ze3ze5zzast_varz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_funz00zzast_varz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzd2argszd2retescapezd2setz12zc0zzast_varz00(BgL_cfunz00_bglt,
		obj_t);
	static obj_t BGl_z62variablezd2accesszb0zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzd2predicatezd2ofz00zzast_varz00(BgL_cfunz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_sfunzd2optionalzf3z21zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_variablezd2namezd2setz12z12zzast_varz00(BgL_variablez00_bglt, obj_t);
	static obj_t BGl_z62sfunzd2argszd2noescapezd2setz12za2zzast_varz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_variablezd2fastzd2alphaz00zzast_varz00(BgL_variablez00_bglt);
	static obj_t BGl_z62lambda1950z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62sfunzd2effectzd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62globalzd2pragmazd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31848ze3ze5zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1955z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1875z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1956z62zzast_varz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzd2predicatezd2ofz00zzast_varz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62lambda1876z62zzast_varz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzd2failsafezd2setz12z12zzast_varz00(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62localzd2namezb0zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_localzd2keyzd2zzast_varz00(BgL_localz00_bglt);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62globalzd2srczb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62svarzd2loczb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62globalzd2occurrencezb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62funzd2failsafezd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62localzd2valzd2noescapezd2setz12za2zzast_varz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda1961z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzd2strengthzd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda1962z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62globalzd2libraryzb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1883z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1884z62zzast_varz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_sfunz00zzast_varz00 = BUNSPEC;
	static obj_t BGl_z62lambda1967z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1968z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62funzd2thezd2closurezd2setz12za2zzast_varz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sfunzd2optionalszd2zzast_varz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62sfunzd2classzb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32022ze3ze5zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62sexitzd2detachedzf3zd2setz12z83zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62variablezf3z91zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62sexitzf3z91zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62cfunzd2failsafezd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62funzd2sidezd2effectzd2setz12za2zzast_varz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_cvarz00_bglt BGl_makezd2cvarzd2zzast_varz00(bool_t);
	static obj_t BGl_z62lambda1891z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1892z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1974z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1975z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62sfunzd2dssslzd2keywordsz62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62globalzd2typezd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda1899z62zzast_varz00(obj_t, obj_t);
	static BgL_cfunz00_bglt BGl_z62cfunzd2nilzb0zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzd2thezd2closurez00zzast_varz00(BgL_cfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_scnstzd2classzd2setz12z12zzast_varz00(BgL_scnstz00_bglt, obj_t);
	static obj_t BGl_z62cfunzd2effectzb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62funzd2argszd2retescapezd2setz12za2zzast_varz00(obj_t,
		obj_t, obj_t);
	static BgL_sfunz00_bglt BGl_z62makezd2sfunzb0zzast_varz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62globalzd2accesszd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62variablezd2removablezd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62feffectzd2writezb0zzast_varz00(obj_t, obj_t);
	static obj_t
		BGl_z62sfunzd2thezd2closurezd2globalzd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_localz00_bglt BGl_makezd2localzd2zzast_varz00(obj_t,
		obj_t, BgL_typez00_bglt, BgL_valuez00_bglt, obj_t, obj_t, obj_t, long, long,
		bool_t, long, obj_t);
	static obj_t BGl_z62feffectzf3z91zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62localzd2fastzd2alphazd2setz12za2zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda1981z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1982z62zzast_varz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_funzd2thezd2closurezd2setz12zc0zzast_varz00(BgL_funz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2removablezd2zzast_varz00(BgL_globalz00_bglt);
	static obj_t BGl_z62sfunzd2effectzb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62globalzd2pragmazb0zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_sexitz00_bglt BGl_sexitzd2nilzd2zzast_varz00(void);
	static obj_t BGl_genericzd2initzd2zzast_varz00(void);
	static obj_t BGl_z62cfunzd2methodzb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1988z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62localzd2occurrencezd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda1989z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62funzd2arityzb0zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sexitzd2detachedzf3zd2setz12ze1zzast_varz00(BgL_sexitz00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2evaluablezf3zd2setz12ze1zzast_varz00(BgL_globalz00_bglt,
		bool_t);
	static obj_t BGl_z62localzd2idzb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62globalzd2evalzf3z43zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_cfunzf3zf3zzast_varz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_sexitz00zzast_varz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzd2argszd2retescapez00zzast_varz00(BgL_cfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_funzd2sidezd2effectzd2setz12zc0zzast_varz00(BgL_funz00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62globalzd2accesszb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62globalzf3z91zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62variablezd2removablezb0zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_scnstz00_bglt BGl_makezd2scnstzd2zzast_varz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62sfunzd2dssslzd2keywordszd2setz12za2zzast_varz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzast_varz00(void);
	static obj_t BGl_z62zc3z04anonymousza31957ze3ze5zzast_varz00(obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_localzd2typezd2zzast_varz00(BgL_localz00_bglt);
	static obj_t BGl_z62sfunzd2topzf3zd2setz12z83zzast_varz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_sfunzf3zf3zzast_varz00(obj_t);
	static obj_t BGl_z62localzd2valzd2noescapez62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62globalzd2userzf3zd2setz12z83zzast_varz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL long
		BGl_globalzd2occurrencewzd2zzast_varz00(BgL_globalz00_bglt);
	static obj_t BGl_z62lambda1995z62zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzd2argszd2retescapez00zzast_varz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62lambda1996z62zzast_varz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_variablezd2idzd2zzast_varz00(BgL_variablez00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32041ze3ze5zzast_varz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_localzf3zf3zzast_varz00(obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62globalzd2idzb0zzast_varz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62zc3z04anonymousza31990ze3ze5zzast_varz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31893ze3ze5zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_funzd2argszd2noescapezd2setz12zc0zzast_varz00(BgL_funz00_bglt, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31885ze3ze5zzast_varz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31877ze3ze5zzast_varz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31869ze3ze5zzast_varz00(obj_t);
	static BgL_typez00_bglt BGl_z62variablezd2typezb0zzast_varz00(obj_t, obj_t);
	static BgL_localz00_bglt BGl_z62makezd2localzb0zzast_varz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62cfunzd2sidezd2effectz62zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_globalzd2namezd2zzast_varz00(BgL_globalz00_bglt);
	static obj_t BGl_z62valuezf3z91zzast_varz00(obj_t, obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	BGL_EXPORTED_DECL obj_t
		BGl_funzd2predicatezd2ofzd2setz12zc0zzast_varz00(BgL_funz00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_variablezf3zf3zzast_varz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_scnstzf3zf3zzast_varz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32123ze3ze5zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_funzd2argszd2retescapez00zzast_varz00(BgL_funz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sexitzd2handlerzd2setz12z12zzast_varz00(BgL_sexitz00_bglt, obj_t);
	static obj_t BGl_z62scnstzd2loczd2setz12z70zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31983ze3ze5zzast_varz00(obj_t);
	static obj_t BGl_z62localzd2occurrencewzd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sfunzd2strengthzd2zzast_varz00(BgL_sfunz00_bglt);
	static BgL_scnstz00_bglt BGl_z62makezd2scnstzb0zzast_varz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cfunzd2stackzd2allocatorzd2setz12za2zzast_varz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62sfunzd2argszd2retescapezd2setz12za2zzast_varz00(obj_t,
		obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62localzd2typezb0zzast_varz00(obj_t, obj_t);
	static BgL_localz00_bglt BGl_z62localzd2nilzb0zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2aliaszd2setz12z12zzast_varz00(BgL_globalz00_bglt, obj_t);
	static obj_t BGl_z62cfunzd2stackzd2allocatorz62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzd2loczd2setz12z70zzast_varz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_sfunz00_bglt BGl_makezd2sfunzd2zzast_varz00(long, obj_t,
		obj_t, obj_t, bool_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62sfunzd2stackzd2allocatorzd2setz12za2zzast_varz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzd2stackablezd2setz12z12zzast_varz00(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32108ze3ze5zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzd2propertyzd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2libraryzd2zzast_varz00(BgL_globalz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31976ze3ze5zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_funzd2stackzd2allocatorz00zzast_varz00(BgL_funz00_bglt);
	static obj_t BGl_z62sfunzd2predicatezd2ofzd2setz12za2zzast_varz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzd2thezd2closurezd2setz12zc0zzast_varz00(BgL_cfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL long BGl_funzd2arityzd2zzast_varz00(BgL_funz00_bglt);
	static BgL_scnstz00_bglt BGl_z62scnstzd2nilzb0zzast_varz00(obj_t);
	static BgL_feffectz00_bglt BGl_z62makezd2feffectzb0zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62variablezd2accesszd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_valuez00_bglt BGl_makezd2valuezd2zzast_varz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_funzd2predicatezd2ofz00zzast_varz00(BgL_funz00_bglt);
	static obj_t BGl_z62funzd2topzf3z43zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzd2thezd2closurezd2setz12zc0zzast_varz00(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzd2valuezd2setz12z12zzast_varz00(BgL_localz00_bglt,
		BgL_valuez00_bglt);
	static obj_t BGl_z62cfunzf3z91zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzd2sidezd2effectzd2setz12zc0zzast_varz00(BgL_cfunz00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62sfunzd2keyzf3z43zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2fastzd2alphazd2setz12zc0zzast_varz00(BgL_globalz00_bglt,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31969ze3ze5zzast_varz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31799ze3ze5zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzd2stackzd2allocatorz00zzast_varz00(BgL_cfunz00_bglt);
	static obj_t BGl_z62sfunzf3z91zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2occurrencezd2setz12z12zzast_varz00(BgL_globalz00_bglt, long);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzd2sidezd2effectzd2setz12zc0zzast_varz00(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_methodzd2initzd2zzast_varz00(void);
	static obj_t BGl_z62globalzd2fastzd2alphaz62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62cfunzd2thezd2closurez62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62feffectzd2writezd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_globalzd2idzd2zzast_varz00(BgL_globalz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32061ze3ze5zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_svarz00_bglt BGl_svarzd2nilzd2zzast_varz00(void);
	static obj_t BGl_z62scnstzd2classzd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62variablezd2occurrencewzd2setz12z70zzast_varz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2importzd2setz12z12zzast_varz00(BgL_globalz00_bglt, obj_t);
	static obj_t BGl_z62funzd2optionalzd2arityz62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62cfunzd2argszd2typez62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62scnstzd2loczb0zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_variablezd2accesszd2setz12z12zzast_varz00(BgL_variablez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzd2effectzd2setz12z12zzast_varz00(BgL_cfunz00_bglt, obj_t);
	static BgL_valuez00_bglt BGl_z62globalzd2valuezb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62globalzd2removablezb0zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_localzd2occurrencezd2zzast_varz00(BgL_localz00_bglt);
	static obj_t BGl_z62globalzd2userzf3z43zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62funzd2predicatezd2ofzd2setz12za2zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62sfunzd2keyszb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32135ze3ze5zzast_varz00(obj_t);
	static BgL_valuez00_bglt BGl_z62makezd2valuezb0zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzd2infixzf3zd2setz12ze1zzast_varz00(BgL_cfunz00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzd2argszd2noescapez00zzast_varz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL BgL_variablez00_bglt
		BGl_makezd2variablezd2zzast_varz00(obj_t, obj_t, BgL_typez00_bglt,
		BgL_valuez00_bglt, obj_t, obj_t, obj_t, long, long, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzd2thezd2closurezd2globalzd2zzast_varz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzd2topzf3zd2setz12ze1zzast_varz00(BgL_sfunz00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_funzd2sidezd2effectz00zzast_varz00(BgL_funz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzd2stackzd2allocatorzd2setz12zc0zzast_varz00(BgL_cfunz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzd2dssslzd2keywordsz00zzast_varz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62funzd2sidezd2effectz62zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_localz00_bglt BGl_localzd2nilzd2zzast_varz00(void);
	static BgL_variablez00_bglt BGl_z62variablezd2nilzb0zzast_varz00(obj_t);
	BGL_EXPORTED_DECL long BGl_cfunzd2arityzd2zzast_varz00(BgL_cfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_funzd2topzf3zd2setz12ze1zzast_varz00(BgL_funz00_bglt, bool_t);
	BGL_EXPORTED_DECL BgL_feffectz00_bglt BGl_feffectzd2nilzd2zzast_varz00(void);
	static obj_t BGl_z62funzd2argszd2noescapez62zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_svarzd2loczd2zzast_varz00(BgL_svarz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzd2stackzd2allocatorzd2setz12zc0zzast_varz00(BgL_sfunz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_cfunzd2macrozf3z21zzast_varz00(BgL_cfunz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32160ze3ze5zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_variablezd2typezd2setz12z12zzast_varz00(BgL_variablez00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_z62localzd2accesszb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62variablezd2fastzd2alphazd2setz12za2zzast_varz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_variablezd2namezd2zzast_varz00(BgL_variablez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_feffectzd2readzd2setz12z12zzast_varz00(BgL_feffectz00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_cfunzd2infixzf3z21zzast_varz00(BgL_cfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2occurrencewzd2setz12z12zzast_varz00(BgL_globalz00_bglt, long);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzd2classzd2setz12z12zzast_varz00(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_scnstz00_bglt BGl_scnstzd2nilzd2zzast_varz00(void);
	static obj_t BGl_z62variablezd2occurrencezd2setz12z70zzast_varz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62sfunzd2classzd2setz12z70zzast_varz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_globalzd2typezd2zzast_varz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL BgL_globalz00_bglt BGl_globalzd2nilzd2zzast_varz00(void);
	static obj_t BGl_z62cfunzd2thezd2closurezd2setz12za2zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62globalzd2evalzf3zd2setz12z83zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62cfunzd2argszd2noescapez62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62localzd2accesszd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda2002z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2003z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32080ze3ze5zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32153ze3ze5zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_scnstzd2classzd2zzast_varz00(BgL_scnstz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_scnstzd2loczd2setz12z12zzast_varz00(BgL_scnstz00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_sfunzd2keyzf3z21zzast_varz00(obj_t);
	static obj_t BGl_z62cfunzd2predicatezd2ofz62zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzd2namezd2setz12z12zzast_varz00(BgL_localz00_bglt, obj_t);
	static obj_t BGl_z62sfunzd2thezd2closurezd2setz12za2zzast_varz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_globalzd2argszd2safezf3zf3zzast_varz00(BgL_globalz00_bglt);
	static obj_t BGl_z62cfunzd2sidezd2effectzd2setz12za2zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62localzd2namezd2setz12z70zzast_varz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_cfunz00_bglt BGl_cfunzd2nilzd2zzast_varz00(void);
	static obj_t BGl_z62zc3z04anonymousza31997ze3ze5zzast_varz00(obj_t);
	static obj_t BGl_z62sfunzd2predicatezd2ofz62zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_globalzd2optionalzf3z21zzast_varz00(obj_t);
	static obj_t BGl_z62variablezd2idzb0zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzd2predicatezd2ofzd2setz12zc0zzast_varz00(BgL_cfunz00_bglt, obj_t);
	static obj_t BGl_z62cfunzd2arityzb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzd2sidezd2effectzd2setz12za2zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda2010z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62globalzd2modulezb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2011z62zzast_varz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_variablezd2occurrencewzd2setz12z12zzast_varz00(BgL_variablez00_bglt,
		long);
	BGL_EXPORTED_DECL bool_t
		BGl_globalzd2evaluablezf3z21zzast_varz00(BgL_globalz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32146ze3ze5zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzd2userzf3zd2setz12ze1zzast_varz00(BgL_localz00_bglt, bool_t);
	static obj_t BGl_z62zc3z04anonymousza32049ze3ze5zzast_varz00(obj_t);
	static BgL_cfunz00_bglt BGl_z62lambda2018z62zzast_varz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_feffectzf3zf3zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_globalzd2initzd2zzast_varz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_scnstzd2loczd2zzast_varz00(BgL_scnstz00_bglt);
	BGL_EXPORTED_DEF obj_t BGl_svarz00zzast_varz00 = BUNSPEC;
	static obj_t BGl_z62sfunzd2bodyzb0zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2srczd2setz12z12zzast_varz00(BgL_globalz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_funzd2thezd2closurez00zzast_varz00(BgL_funz00_bglt);
	static obj_t BGl_z62globalzd2namezb0zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sfunzd2keyszd2zzast_varz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_localzd2idzd2zzast_varz00(BgL_localz00_bglt);
	static obj_t BGl_z62globalzd2srczd2setz12z70zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62funzd2thezd2closurez62zzast_varz00(obj_t, obj_t);
	static BgL_globalz00_bglt BGl_z62globalzd2nilzb0zzast_varz00(obj_t);
	static obj_t BGl_z62globalzd2jvmzd2typezd2namezd2setz12z70zzast_varz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_globalzd2aliaszd2zzast_varz00(BgL_globalz00_bglt);
	static BgL_cfunz00_bglt BGl_z62lambda2020z62zzast_varz00(obj_t);
	static BgL_cvarz00_bglt BGl_z62lambda2104z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62funzf3z91zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62globalzd2evaluablezf3zd2setz12z83zzast_varz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62funzd2predicatezd2ofz62zzast_varz00(obj_t, obj_t);
	static BgL_cvarz00_bglt BGl_z62lambda2106z62zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2pragmazd2setz12z12zzast_varz00(BgL_globalz00_bglt, obj_t);
	static obj_t BGl_z62cfunzd2topzf3z43zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzd2dssslzd2keywordszd2setz12zc0zzast_varz00(BgL_sfunz00_bglt,
		obj_t);
	static obj_t BGl_z62lambda2027z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2028z62zzast_varz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzd2methodzd2setz12z12zzast_varz00(BgL_cfunz00_bglt, obj_t);
	static obj_t BGl_z62localzd2valuezd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_valuez00_bglt
		BGl_localzd2valuezd2zzast_varz00(BgL_localz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_localzd2userzf3z21zzast_varz00(BgL_localz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_feffectzd2readzd2zzast_varz00(BgL_feffectz00_bglt);
	static obj_t BGl_z62sfunzd2topzf3z43zzast_varz00(obj_t, obj_t);
	static BgL_cvarz00_bglt BGl_z62cvarzd2nilzb0zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_feffectzd2writezd2zzast_varz00(BgL_feffectz00_bglt);
	static obj_t BGl_z62lambda2112z62zzast_varz00(obj_t, obj_t);
	static BgL_svarz00_bglt BGl_z62makezd2svarzb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2032z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2113z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2033z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62localzf3z91zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62variablezd2valuezd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_funzd2failsafezd2zzast_varz00(BgL_funz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzd2strengthzd2setz12z12zzast_varz00(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62globalzd2libraryzd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	static BgL_sexitz00_bglt BGl_z62lambda2119z62zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda2039z62zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzd2argszd2typez00zzast_varz00(BgL_cfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2pragmazd2zzast_varz00(BgL_globalz00_bglt);
	static obj_t BGl_z62sfunzd2bodyzd2setz12z70zzast_varz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_funz00_bglt BGl_funzd2nilzd2zzast_varz00(void);
	static obj_t BGl_z62cfunzd2failsafezb0zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzd2argszd2noescapezd2setz12zc0zzast_varz00(BgL_cfunz00_bglt, obj_t);
	static obj_t BGl_z62funzd2argszd2noescapezd2setz12za2zzast_varz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62globalzd2importzb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62globalzd2modulezd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62cfunzd2effectzd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_globalzd2keyzf3z21zzast_varz00(obj_t);
	static obj_t BGl_z62localzd2occurrencezb0zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzd2sidezd2effectz00zzast_varz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_funzf3zf3zzast_varz00(obj_t);
	static obj_t BGl_z62sfunzd2failsafezb0zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_cvarzf3zf3zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzd2argszd2noescapezd2setz12zc0zzast_varz00(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31630ze3ze5zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2040z62zzast_varz00(obj_t, obj_t, obj_t);
	static BgL_sexitz00_bglt BGl_z62lambda2121z62zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzd2failsafezd2setz12z12zzast_varz00(BgL_cfunz00_bglt, obj_t);
	static obj_t BGl_z62scnstzf3z91zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2accesszd2zzast_varz00(BgL_globalz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32068ze3ze5zzast_varz00(obj_t);
	static obj_t BGl_z62lambda2127z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2047z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2128z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2048z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62cfunzd2infixzf3zd2setz12z83zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62sfunzd2argszd2setz12z70zzast_varz00(obj_t, obj_t, obj_t);
	static BgL_valuez00_bglt BGl_z62variablezd2valuezb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62variablezd2userzf3z43zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzd2argszd2retescapezd2setz12zc0zzast_varz00(BgL_sfunz00_bglt,
		obj_t);
	static obj_t BGl_z62funzd2stackzd2allocatorzd2setz12za2zzast_varz00(obj_t,
		obj_t, obj_t);
	static BgL_funz00_bglt BGl_z62makezd2funzb0zzast_varz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_svarzf3zf3zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	static obj_t BGl_z62variablezd2occurrencezb0zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_globalzd2readzd2onlyzf3zf3zzast_varz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2accesszd2setz12z12zzast_varz00(BgL_globalz00_bglt, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_cfunz00zzast_varz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31704ze3ze5zzast_varz00(obj_t);
	static obj_t BGl_z62lambda2133z62zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzd2valzd2noescapezd2setz12zc0zzast_varz00(BgL_localz00_bglt,
		obj_t);
	static obj_t BGl_z62lambda2134z62zzast_varz00(obj_t, obj_t, obj_t);
	static BgL_svarz00_bglt BGl_z62lambda2057z62zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_variablezd2typezd2zzast_varz00(BgL_variablez00_bglt);
	static BgL_svarz00_bglt BGl_z62lambda2059z62zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sfunzd2bodyzd2zzast_varz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62sfunzd2stackablezb0zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzd2fastzd2alphaz00zzast_varz00(BgL_localz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2namezd2setz12z12zzast_varz00(BgL_globalz00_bglt, obj_t);
	static obj_t BGl_z62sexitzd2handlerzb0zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2jvmzd2typezd2namezd2setz12z12zzast_varz00(BgL_globalz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_funzd2topzf3z21zzast_varz00(BgL_funz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_cfunzd2topzf3z21zzast_varz00(BgL_cfunz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31713ze3ze5zzast_varz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31381ze3ze5zzast_varz00(obj_t, obj_t);
	static BgL_feffectz00_bglt BGl_z62lambda2142z62zzast_varz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_variablezd2removablezd2zzast_varz00(BgL_variablez00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31365ze3ze5zzast_varz00(obj_t, obj_t);
	static BgL_feffectz00_bglt BGl_z62lambda2144z62zzast_varz00(obj_t);
	static obj_t BGl_z62sfunzd2propertyzb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62variablezd2namezd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda2066z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2067z62zzast_varz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzd2valzd2noescapez00zzast_varz00(BgL_localz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_sfunzd2topzf3z21zzast_varz00(BgL_sfunz00_bglt);
	static BgL_cfunz00_bglt BGl_z62makezd2cfunzb0zzast_varz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_scnstzd2nodezd2zzast_varz00(BgL_scnstz00_bglt);
	static obj_t BGl_z62globalzd2fastzd2alphazd2setz12za2zzast_varz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzd2loczd2setz12z12zzast_varz00(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62sfunzd2failsafezd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_funzd2argszd2retescapezd2setz12zc0zzast_varz00(BgL_funz00_bglt, obj_t);
	static obj_t BGl_z62svarzd2loczd2setz12z70zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62globalzd2occurrencezd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_svarz00_bglt BGl_makezd2svarzd2zzast_varz00(obj_t);
	static obj_t BGl_z62globalzd2aliaszd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62globalzd2optionalzf3z43zzast_varz00(obj_t, obj_t);
	static BgL_feffectz00_bglt BGl_z62feffectzd2nilzb0zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_variablezd2removablezd2setz12z12zzast_varz00(BgL_variablez00_bglt,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_localz00zzast_varz00 = BUNSPEC;
	static obj_t BGl_z62lambda1422z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2151z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62cfunzd2macrozf3z43zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1423z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2152z62zzast_varz00(obj_t, obj_t, obj_t);
	static BgL_sfunz00_bglt BGl_z62sfunzd2nilzb0zzast_varz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31609ze3ze5zzast_varz00(obj_t);
	static BgL_valuez00_bglt BGl_z62valuezd2nilzb0zzast_varz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_feffectz00zzast_varz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzd2thezd2closurez00zzast_varz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2valuezd2setz12z12zzast_varz00(BgL_globalz00_bglt,
		BgL_valuez00_bglt);
	static BgL_scnstz00_bglt BGl_z62lambda2076z62zzast_varz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62localzd2userzf3zd2setz12z83zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda2158z62zzast_varz00(obj_t, obj_t);
	static BgL_scnstz00_bglt BGl_z62lambda2078z62zzast_varz00(obj_t);
	static obj_t BGl_z62lambda2159z62zzast_varz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_localzd2occurrencewzd2zzast_varz00(BgL_localz00_bglt);
	static obj_t BGl_cnstzd2initzd2zzast_varz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_varz00(void);
	static obj_t BGl_z62cfunzd2infixzf3z43zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzd2argszb0zzast_varz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62globalzd2typezb0zzast_varz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62sfunzd2argszd2namez62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62cfunzd2topzf3zd2setz12z83zzast_varz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_scnstz00zzast_varz00 = BUNSPEC;
	static obj_t BGl_importedzd2moduleszd2initz00zzast_varz00(void);
	static BgL_valuez00_bglt BGl_z62lambda1510z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31901ze3ze5zzast_varz00(obj_t);
	static obj_t BGl_z62lambda1511z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzast_varz00(void);
	static obj_t BGl_z62zc3z04anonymousza31723ze3ze5zzast_varz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_globalzf3zf3zzast_varz00(obj_t);
	static obj_t BGl_z62cvarzf3z91zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62variablezd2userzf3zd2setz12z83zzast_varz00(obj_t, obj_t,
		obj_t);
	static BgL_valuez00_bglt BGl_z62lambda1353z62zzast_varz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31456ze3ze5zzast_varz00(obj_t);
	static obj_t BGl_z62scnstzd2classzb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2084z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2085z62zzast_varz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_sexitz00_bglt BGl_makezd2sexitzd2zzast_varz00(obj_t,
		bool_t);
	static obj_t BGl_z62lambda2089z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62cfunzd2methodzd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_funzd2effectzd2setz12z12zzast_varz00(BgL_funz00_bglt, obj_t);
	static obj_t BGl_z62svarzf3z91zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62funzd2effectzd2setz12z70zzast_varz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_funzd2effectzd2zzast_varz00(BgL_funz00_bglt);
	static obj_t BGl_z62scnstzd2nodezb0zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_globalzd2occurrencezd2zzast_varz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzd2propertyzd2setz12z12zzast_varz00(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_funzd2failsafezd2setz12z12zzast_varz00(BgL_funz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzd2thezd2closurezd2globalzd2setz12z12zzast_varz00(BgL_sfunz00_bglt,
		obj_t);
	static obj_t BGl_z62sfunzd2loczb0zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_variablezd2fastzd2alphazd2setz12zc0zzast_varz00(BgL_variablez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzd2bodyzd2setz12z12zzast_varz00(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62lambda2090z62zzast_varz00(obj_t, obj_t, obj_t);
	static BgL_valuez00_bglt BGl_z62lambda1362z62zzast_varz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31538ze3ze5zzast_varz00(obj_t);
	static obj_t BGl_z62lambda1607z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1608z62zzast_varz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_variablezd2occurrencezd2setz12z12zzast_varz00(BgL_variablez00_bglt,
		long);
	BGL_IMPORT obj_t BGl_objectz00zz__objectz00;
	static obj_t BGl_z62lambda2096z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62globalzd2initzb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2097z62zzast_varz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_globalzd2evalzf3z21zzast_varz00(BgL_globalz00_bglt);
	static obj_t BGl_z62sfunzd2sidezd2effectz62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62cfunzd2argszd2retescapezd2setz12za2zzast_varz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzd2argszd2setz12z12zzast_varz00(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_valuez00_bglt
		BGl_variablezd2valuezd2zzast_varz00(BgL_variablez00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_variablezd2userzf3z21zzast_varz00(BgL_variablez00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31555ze3ze5zzast_varz00(obj_t);
	static obj_t BGl_z62lambda1454z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62localzd2removablezd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda1455z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1536z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32098ze3ze5zzast_varz00(obj_t);
	static obj_t BGl_z62lambda1537z62zzast_varz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_cfunz00_bglt BGl_makezd2cfunzd2zzast_varz00(long, obj_t,
		obj_t, obj_t, bool_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, bool_t,
		bool_t, obj_t);
	static BgL_variablez00_bglt BGl_z62lambda1377z62zzast_varz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_variablez00_bglt BGl_z62lambda1379z62zzast_varz00(obj_t);
	static BgL_valuez00_bglt BGl_z62localzd2valuezb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62localzd2userzf3z43zzast_varz00(obj_t, obj_t);
	static BgL_sexitz00_bglt BGl_z62makezd2sexitzb0zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62globalzd2occurrencewzb0zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_feffectzd2writezd2setz12z12zzast_varz00(BgL_feffectz00_bglt, obj_t);
	static obj_t BGl_z62funzd2effectzb0zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_cvarzd2macrozf3z21zzast_varz00(BgL_cvarz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_variablezd2valuezd2setz12z12zzast_varz00(BgL_variablez00_bglt,
		BgL_valuez00_bglt);
	static obj_t BGl_z62cfunzd2predicatezd2ofzd2setz12za2zzast_varz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2libraryzd2setz12z12zzast_varz00(BgL_globalz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_valuez00_bglt BGl_valuezd2nilzd2zzast_varz00(void);
	static obj_t BGl_z62globalzd2jvmzd2typezd2namezb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1702z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1703z62zzast_varz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_funzd2optionalzd2arityz00zzast_varz00(BgL_funz00_bglt);
	static obj_t BGl_z62sfunzd2stackzd2allocatorz62zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sfunzd2stackablezd2zzast_varz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_localzd2typezd2setz12z12zzast_varz00(BgL_localz00_bglt,
		BgL_typez00_bglt);
	static BgL_globalz00_bglt BGl_z62lambda1626z62zzast_varz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sfunzd2argszd2zzast_varz00(BgL_sfunz00_bglt);
	static BgL_globalz00_bglt BGl_z62lambda1628z62zzast_varz00(obj_t);
	static obj_t BGl_z62localzd2typezd2setz12z70zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62feffectzd2readzd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_globalz00_bglt BGl_makezd2globalzd2zzast_varz00(obj_t,
		obj_t, BgL_typez00_bglt, BgL_valuez00_bglt, obj_t, obj_t, obj_t, long, long,
		bool_t, obj_t, obj_t, bool_t, bool_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_valuez00zzast_varz00 = BUNSPEC;
	static obj_t BGl_z62localzd2fastzd2alphaz62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62globalzd2namezd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62globalzd2keyzf3z43zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_sexitzf3zf3zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2initzd2setz12z12zzast_varz00(BgL_globalz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzd2removablezd2setz12z12zzast_varz00(BgL_localz00_bglt, obj_t);
	static obj_t BGl_z62lambda1711z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1712z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62funzd2argszd2retescapez62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1553z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1554z62zzast_varz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2userzf3zd2setz12ze1zzast_varz00(BgL_globalz00_bglt, bool_t);
	static obj_t BGl_z62sexitzd2handlerzd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62variablezd2fastzd2alphaz62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62globalzd2argszd2safezf3z91zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_cvarz00_bglt BGl_cvarzd2nilzd2zzast_varz00(void);
	BGL_EXPORTED_DECL long
		BGl_variablezd2occurrencezd2zzast_varz00(BgL_variablez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2setzd2readzd2onlyz12zc0zzast_varz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2modulezd2zzast_varz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DEF obj_t BGl_variablez00zzast_varz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2jvmzd2typezd2namezd2zzast_varz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzd2stackzd2allocatorz00zzast_varz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62lambda1721z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31752ze3ze5zzast_varz00(obj_t);
	static obj_t BGl_z62lambda1722z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62globalzd2evaluablezf3z43zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1566z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1809z62zzast_varz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda1486z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1567z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62sfunzd2thezd2closurez62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1487z62zzast_varz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_feffectz00_bglt BGl_makezd2feffectzd2zzast_varz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzd2argszd2noescapez00zzast_varz00(BgL_cfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzd2argszd2namez00zzast_varz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunzd2topzf3zd2setz12ze1zzast_varz00(BgL_cfunz00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzd2effectzd2setz12z12zzast_varz00(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62lambda1810z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31923ze3ze5zzast_varz00(obj_t);
	static obj_t BGl_z62lambda1651z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31826ze3ze5zzast_varz00(obj_t);
	static obj_t BGl_z62sfunzd2optionalzf3z43zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_funzd2stackzd2allocatorzd2setz12zc0zzast_varz00(BgL_funz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_funz00_bglt BGl_makezd2funzd2zzast_varz00(long, obj_t,
		obj_t, obj_t, bool_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1652z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1735z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1736z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62sfunzd2thezd2closurezd2globalzb0zzast_varz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1577z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1578z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62sfunzd2strengthzb0zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cfunzd2effectzd2zzast_varz00(BgL_cfunz00_bglt);
	static obj_t BGl_z62lambda1900z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31940ze3ze5zzast_varz00(obj_t);
	BGL_EXPORTED_DECL long BGl_sfunzd2arityzd2zzast_varz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62lambda1740z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1741z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31916ze3ze5zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cfunzd2failsafezd2zzast_varz00(BgL_cfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2removablezd2setz12z12zzast_varz00(BgL_globalz00_bglt, obj_t);
	static obj_t BGl_z62lambda1824z62zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sfunzd2effectzd2zzast_varz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31568ze3ze5zzast_varz00(obj_t);
	static obj_t BGl_z62lambda1825z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1664z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1907z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1665z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1908z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62globalzd2importzd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62variablezd2occurrencewzb0zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2importzd2zzast_varz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_cfunzd2methodzd2zzast_varz00(BgL_cfunz00_bglt);
	BGL_EXPORTED_DECL BgL_valuez00_bglt
		BGl_globalzd2valuezd2zzast_varz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_globalzd2userzf3z21zzast_varz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_valuezf3zf3zzast_varz00(obj_t);
	static obj_t BGl_z62globalzd2occurrencewzd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sfunzd2failsafezd2zzast_varz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62sfunzd2optionalszb0zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_sexitzd2detachedzf3z21zzast_varz00(BgL_sexitz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_variablezd2userzf3zd2setz12ze1zzast_varz00(BgL_variablez00_bglt,
		bool_t);
	static obj_t BGl_z62globalzd2aliaszb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62sexitzd2detachedzf3z43zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_globalz00zzast_varz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_localzd2occurrencewzd2setz12z12zzast_varz00(BgL_localz00_bglt, long);
	static obj_t BGl_z62zc3z04anonymousza31933ze3ze5zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1750z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1751z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62cfunzd2argszd2retescapez62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31909ze3ze5zzast_varz00(obj_t);
	static obj_t BGl_z62lambda1914z62zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_variablez00_bglt
		BGl_variablezd2nilzd2zzast_varz00(void);
	static obj_t BGl_z62lambda1915z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62sfunzd2argszd2noescapez62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1592z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1835z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1593z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1836z62zzast_varz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2typezd2setz12z12zzast_varz00(BgL_globalz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzd2fastzd2alphaz00zzast_varz00(BgL_globalz00_bglt);
	static obj_t BGl_z62feffectzd2readzb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzd2argszd2retescapez62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62localzd2keyzb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62globalzd2setzd2readzd2onlyz12za2zzast_varz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_localzd2accesszd2zzast_varz00(BgL_localz00_bglt);
	BGL_EXPORTED_DEF obj_t BGl_cvarz00zzast_varz00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_sfunz00_bglt BGl_sfunzd2nilzd2zzast_varz00(void);
	static obj_t BGl_z62globalzd2readzd2onlyzf3z91zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31861ze3ze5zzast_varz00(obj_t);
	BGL_EXPORTED_DECL long
		BGl_variablezd2occurrencewzd2zzast_varz00(BgL_variablez00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31691ze3ze5zzast_varz00(obj_t);
	static obj_t BGl_z62lambda1921z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31764ze3ze5zzast_varz00(obj_t);
	static obj_t BGl_z62lambda1922z62zzast_varz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62variablezd2typezd2setz12z70zzast_varz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31594ze3ze5zzast_varz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31837ze3ze5zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzd2predicatezd2ofzd2setz12zc0zzast_varz00(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62lambda1762z62zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62funzd2failsafezb0zzast_varz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1763z62zzast_varz00(obj_t, obj_t, obj_t);
	static BgL_funz00_bglt BGl_z62lambda1844z62zzast_varz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62sfunzd2arityzb0zzast_varz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sexitzd2handlerzd2zzast_varz00(BgL_sexitz00_bglt);
	static obj_t __cnst[77];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cvarzd2macrozf3zd2envzf3zzast_varz00,
		BgL_bgl_za762cvarza7d2macroza72376za7,
		BGl_z62cvarzd2macrozf3z43zzast_varz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_variablezd2valuezd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762variableza7d2va2377z00,
		BGl_z62variablezd2valuezd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_funzd2optionalzd2arityzd2envzd2zzast_varz00,
		BgL_bgl_za762funza7d2optiona2378z00,
		BGl_z62funzd2optionalzd2arityz62zzast_varz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfunzd2sidezd2effectzd2envzd2zzast_varz00,
		BgL_bgl_za762cfunza7d2sideza7d2379za7,
		BGl_z62cfunzd2sidezd2effectz62zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2importzd2envz00zzast_varz00,
		BgL_bgl_za762globalza7d2impo2380z00, BGl_z62globalzd2importzb0zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzd2thezd2closurezd2setz12zd2envz12zzast_varz00,
		BgL_bgl_za762cfunza7d2theza7d22381za7,
		BGl_z62cfunzd2thezd2closurezd2setz12za2zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2200z00zzast_varz00,
		BgL_bgl_za762lambda1566za7622382z00, BGl_z62lambda1566z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2201z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2383za7,
		BGl_z62zc3z04anonymousza31579ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2202z00zzast_varz00,
		BgL_bgl_za762lambda1578za7622384z00, BGl_z62lambda1578z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2203z00zzast_varz00,
		BgL_bgl_za762lambda1577za7622385z00, BGl_z62lambda1577z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2204z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2386za7,
		BGl_z62zc3z04anonymousza31594ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2205z00zzast_varz00,
		BgL_bgl_za762lambda1593za7622387z00, BGl_z62lambda1593z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2206z00zzast_varz00,
		BgL_bgl_za762lambda1592za7622388z00, BGl_z62lambda1592z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2207z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2389za7,
		BGl_z62zc3z04anonymousza31609ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2208z00zzast_varz00,
		BgL_bgl_za762lambda1608za7622390z00, BGl_z62lambda1608z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2209z00zzast_varz00,
		BgL_bgl_za762lambda1607za7622391z00, BGl_z62lambda1607z62zzast_varz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzd2thezd2closurezd2setz12zd2envz12zzast_varz00,
		BgL_bgl_za762sfunza7d2theza7d22392za7,
		BGl_z62sfunzd2thezd2closurezd2setz12za2zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzd2argszd2retescapezd2envzd2zzast_varz00,
		BgL_bgl_za762cfunza7d2argsza7d2393za7,
		BGl_z62cfunzd2argszd2retescapez62zzast_varz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_funzd2sidezd2effectzd2setz12zd2envz12zzast_varz00,
		BgL_bgl_za762funza7d2sideza7d22394za7,
		BGl_z62funzd2sidezd2effectzd2setz12za2zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzd2argszd2retescapezd2envzd2zzast_varz00,
		BgL_bgl_za762sfunza7d2argsza7d2395za7,
		BGl_z62sfunzd2argszd2retescapez62zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2210z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2396za7,
		BGl_z62zc3z04anonymousza31381ze3ze5zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_funzd2effectzd2envz00zzast_varz00,
		BgL_bgl_za762funza7d2effectza72397za7, BGl_z62funzd2effectzb0zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2211z00zzast_varz00,
		BgL_bgl_za762lambda1379za7622398z00, BGl_z62lambda1379z62zzast_varz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2212z00zzast_varz00,
		BgL_bgl_za762lambda1377za7622399z00, BGl_z62lambda1377z62zzast_varz00, 0L,
		BUNSPEC, 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2213z00zzast_varz00,
		BgL_bgl_za762lambda1652za7622400z00, BGl_z62lambda1652z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2214z00zzast_varz00,
		BgL_bgl_za762lambda1651za7622401z00, BGl_z62lambda1651z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2215z00zzast_varz00,
		BgL_bgl_za762lambda1665za7622402z00, BGl_z62lambda1665z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2216z00zzast_varz00,
		BgL_bgl_za762lambda1664za7622403z00, BGl_z62lambda1664z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2217z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2404za7,
		BGl_z62zc3z04anonymousza31691ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2218z00zzast_varz00,
		BgL_bgl_za762lambda1690za7622405z00, BGl_z62lambda1690z62zzast_varz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_funzd2failsafezd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762funza7d2failsaf2406z00,
		BGl_z62funzd2failsafezd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2219z00zzast_varz00,
		BgL_bgl_za762lambda1689za7622407z00, BGl_z62lambda1689z62zzast_varz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_variablezd2occurrencewzd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762variableza7d2oc2408z00,
		BGl_z62variablezd2occurrencewzd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2namezd2envz00zzast_varz00,
		BgL_bgl_za762globalza7d2name2409z00, BGl_z62globalzd2namezb0zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2300z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2410za7,
		BGl_z62zc3z04anonymousza31976ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2301z00zzast_varz00,
		BgL_bgl_za762lambda1975za7622411z00, BGl_z62lambda1975z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2220z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2412za7,
		BGl_z62zc3z04anonymousza31704ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2302z00zzast_varz00,
		BgL_bgl_za762lambda1974za7622413z00, BGl_z62lambda1974z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2221z00zzast_varz00,
		BgL_bgl_za762lambda1703za7622414z00, BGl_z62lambda1703z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzd2keyszd2envz00zzast_varz00,
		BgL_bgl_za762sfunza7d2keysza7b2415za7, BGl_z62sfunzd2keyszb0zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_localzd2idzd2envz00zzast_varz00,
		BgL_bgl_za762localza7d2idza7b02416za7, BGl_z62localzd2idzb0zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2303z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2417za7,
		BGl_z62zc3z04anonymousza31983ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2222z00zzast_varz00,
		BgL_bgl_za762lambda1702za7622418z00, BGl_z62lambda1702z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2304z00zzast_varz00,
		BgL_bgl_za762lambda1982za7622419z00, BGl_z62lambda1982z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2223z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2420za7,
		BGl_z62zc3z04anonymousza31713ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2305z00zzast_varz00,
		BgL_bgl_za762lambda1981za7622421z00, BGl_z62lambda1981z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2224z00zzast_varz00,
		BgL_bgl_za762lambda1712za7622422z00, BGl_z62lambda1712z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2306z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2423za7,
		BGl_z62zc3z04anonymousza31990ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2225z00zzast_varz00,
		BgL_bgl_za762lambda1711za7622424z00, BGl_z62lambda1711z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2307z00zzast_varz00,
		BgL_bgl_za762lambda1989za7622425z00, BGl_z62lambda1989z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2226z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2426za7,
		BGl_z62zc3z04anonymousza31723ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2308z00zzast_varz00,
		BgL_bgl_za762lambda1988za7622427z00, BGl_z62lambda1988z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2227z00zzast_varz00,
		BgL_bgl_za762lambda1722za7622428z00, BGl_z62lambda1722z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2309z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2429za7,
		BGl_z62zc3z04anonymousza31997ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2228z00zzast_varz00,
		BgL_bgl_za762lambda1721za7622430z00, BGl_z62lambda1721z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2229z00zzast_varz00,
		BgL_bgl_za762lambda1736za7622431z00, BGl_z62lambda1736z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_localzd2valuezd2envz00zzast_varz00,
		BgL_bgl_za762localza7d2value2432z00, BGl_z62localzd2valuezb0zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_localzd2namezd2envz00zzast_varz00,
		BgL_bgl_za762localza7d2nameza72433za7, BGl_z62localzd2namezb0zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_svarzd2loczd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762svarza7d2locza7d22434za7,
		BGl_z62svarzd2loczd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2310z00zzast_varz00,
		BgL_bgl_za762lambda1996za7622435z00, BGl_z62lambda1996z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2311z00zzast_varz00,
		BgL_bgl_za762lambda1995za7622436z00, BGl_z62lambda1995z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2230z00zzast_varz00,
		BgL_bgl_za762lambda1735za7622437z00, BGl_z62lambda1735z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2312z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2438za7,
		BGl_z62zc3z04anonymousza32004ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2231z00zzast_varz00,
		BgL_bgl_za762lambda1741za7622439z00, BGl_z62lambda1741z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2313z00zzast_varz00,
		BgL_bgl_za762lambda2003za7622440z00, BGl_z62lambda2003z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2232z00zzast_varz00,
		BgL_bgl_za762lambda1740za7622441z00, BGl_z62lambda1740z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2sfunzd2envz00zzast_varz00,
		BgL_bgl_za762makeza7d2sfunza7b2442za7, BGl_z62makezd2sfunzb0zzast_varz00,
		0L, BUNSPEC, 22);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2314z00zzast_varz00,
		BgL_bgl_za762lambda2002za7622443z00, BGl_z62lambda2002z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2233z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2444za7,
		BGl_z62zc3z04anonymousza31752ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_scnstzd2classzd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762scnstza7d2class2445z00,
		BGl_z62scnstzd2classzd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2315z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2446za7,
		BGl_z62zc3z04anonymousza32012ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2234z00zzast_varz00,
		BgL_bgl_za762lambda1751za7622447z00, BGl_z62lambda1751z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2316z00zzast_varz00,
		BgL_bgl_za762lambda2011za7622448z00, BGl_z62lambda2011z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2235z00zzast_varz00,
		BgL_bgl_za762lambda1750za7622449z00, BGl_z62lambda1750z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2317z00zzast_varz00,
		BgL_bgl_za762lambda2010za7622450z00, BGl_z62lambda2010z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2236z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2451za7,
		BGl_z62zc3z04anonymousza31764ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2318z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2452za7,
		BGl_z62zc3z04anonymousza31933ze3ze5zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2237z00zzast_varz00,
		BgL_bgl_za762lambda1763za7622453z00, BGl_z62lambda1763z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2319z00zzast_varz00,
		BgL_bgl_za762lambda1931za7622454z00, BGl_z62lambda1931z62zzast_varz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2238z00zzast_varz00,
		BgL_bgl_za762lambda1762za7622455z00, BGl_z62lambda1762z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2239z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2456za7,
		BGl_z62zc3z04anonymousza31630ze3ze5zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzd2arityzd2envz00zzast_varz00,
		BgL_bgl_za762sfunza7d2arityza72457za7, BGl_z62sfunzd2arityzb0zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzd2stackablezd2envz00zzast_varz00,
		BgL_bgl_za762sfunza7d2stacka2458z00, BGl_z62sfunzd2stackablezb0zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzd2thezd2closurezd2envzd2zzast_varz00,
		BgL_bgl_za762sfunza7d2theza7d22459za7,
		BGl_z62sfunzd2thezd2closurez62zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfunzd2argszd2typezd2envzd2zzast_varz00,
		BgL_bgl_za762cfunza7d2argsza7d2460za7,
		BGl_z62cfunzd2argszd2typez62zzast_varz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_feffectzd2readzd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762feffectza7d2rea2461z00,
		BGl_z62feffectzd2readzd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfunzd2infixzf3zd2envzf3zzast_varz00,
		BgL_bgl_za762cfunza7d2infixza72462za7,
		BGl_z62cfunzd2infixzf3z43zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfunzd2methodzd2envz00zzast_varz00,
		BgL_bgl_za762cfunza7d2method2463z00, BGl_z62cfunzd2methodzb0zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_funzd2arityzd2envz00zzast_varz00,
		BgL_bgl_za762funza7d2arityza7b2464za7, BGl_z62funzd2arityzb0zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2320z00zzast_varz00,
		BgL_bgl_za762lambda1929za7622465z00, BGl_z62lambda1929z62zzast_varz00, 0L,
		BUNSPEC, 22);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2321z00zzast_varz00,
		BgL_bgl_za762lambda2028za7622466z00, BGl_z62lambda2028z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2240z00zzast_varz00,
		BgL_bgl_za762lambda1628za7622467z00, BGl_z62lambda1628z62zzast_varz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2322z00zzast_varz00,
		BgL_bgl_za762lambda2027za7622468z00, BGl_z62lambda2027z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2241z00zzast_varz00,
		BgL_bgl_za762lambda1626za7622469z00, BGl_z62lambda1626z62zzast_varz00, 0L,
		BUNSPEC, 20);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2323z00zzast_varz00,
		BgL_bgl_za762lambda2033za7622470z00, BGl_z62lambda2033z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2242z00zzast_varz00,
		BgL_bgl_za762lambda1810za7622471z00, BGl_z62lambda1810z62zzast_varz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzd2evaluablezf3zd2setz12zd2envz33zzast_varz00,
		BgL_bgl_za762globalza7d2eval2472z00,
		BGl_z62globalzd2evaluablezf3zd2setz12z83zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2324z00zzast_varz00,
		BgL_bgl_za762lambda2032za7622473z00, BGl_z62lambda2032z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2243z00zzast_varz00,
		BgL_bgl_za762lambda1809za7622474z00, BGl_z62lambda1809z62zzast_varz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzd2argszd2retescapezd2setz12zd2envz12zzast_varz00,
		BgL_bgl_za762cfunza7d2argsza7d2475za7,
		BGl_z62cfunzd2argszd2retescapezd2setz12za2zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2325z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2476za7,
		BGl_z62zc3z04anonymousza32041ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2244z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2477za7,
		BGl_z62zc3z04anonymousza31826ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2326z00zzast_varz00,
		BgL_bgl_za762lambda2040za7622478z00, BGl_z62lambda2040z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2245z00zzast_varz00,
		BgL_bgl_za762lambda1825za7622479z00, BGl_z62lambda1825z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2327z00zzast_varz00,
		BgL_bgl_za762lambda2039za7622480z00, BGl_z62lambda2039z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2246z00zzast_varz00,
		BgL_bgl_za762lambda1824za7622481z00, BGl_z62lambda1824z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2328z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2482za7,
		BGl_z62zc3z04anonymousza32049ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2247z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2483za7,
		BGl_z62zc3z04anonymousza31837ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2329z00zzast_varz00,
		BgL_bgl_za762lambda2048za7622484z00, BGl_z62lambda2048z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2248z00zzast_varz00,
		BgL_bgl_za762lambda1836za7622485z00, BGl_z62lambda1836z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_scnstzd2classzd2envz00zzast_varz00,
		BgL_bgl_za762scnstza7d2class2486z00, BGl_z62scnstzd2classzb0zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2249z00zzast_varz00,
		BgL_bgl_za762lambda1835za7622487z00, BGl_z62lambda1835z62zzast_varz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_localzd2valuezd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762localza7d2value2488z00,
		BGl_z62localzd2valuezd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzd2occurrencewzd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762localza7d2occur2489z00,
		BGl_z62localzd2occurrencewzd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzd2argszd2safezf3zd2envz21zzast_varz00,
		BgL_bgl_za762globalza7d2args2490z00,
		BGl_z62globalzd2argszd2safezf3z91zzast_varz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2typezd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762globalza7d2type2491z00,
		BGl_z62globalzd2typezd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_variablezd2idzd2envz00zzast_varz00,
		BgL_bgl_za762variableza7d2id2492z00, BGl_z62variablezd2idzb0zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_localzd2keyzd2envz00zzast_varz00,
		BgL_bgl_za762localza7d2keyza7b2493za7, BGl_z62localzd2keyzb0zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzd2setzd2readzd2onlyz12zd2envz12zzast_varz00,
		BgL_bgl_za762globalza7d2setza72494za7,
		BGl_z62globalzd2setzd2readzd2onlyz12za2zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2330z00zzast_varz00,
		BgL_bgl_za762lambda2047za7622495z00, BGl_z62lambda2047z62zzast_varz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzd2predicatezd2ofzd2setz12zd2envz12zzast_varz00,
		BgL_bgl_za762cfunza7d2predic2496z00,
		BGl_z62cfunzd2predicatezd2ofzd2setz12za2zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2331z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2497za7,
		BGl_z62zc3z04anonymousza32022ze3ze5zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2250z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2498za7,
		BGl_z62zc3z04anonymousza31799ze3ze5zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2332z00zzast_varz00,
		BgL_bgl_za762lambda2020za7622499z00, BGl_z62lambda2020z62zzast_varz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2251z00zzast_varz00,
		BgL_bgl_za762lambda1776za7622500z00, BGl_z62lambda1776z62zzast_varz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2333z00zzast_varz00,
		BgL_bgl_za762lambda2018za7622501z00, BGl_z62lambda2018z62zzast_varz00, 0L,
		BUNSPEC, 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2252z00zzast_varz00,
		BgL_bgl_za762lambda1774za7622502z00, BGl_z62lambda1774z62zzast_varz00, 0L,
		BUNSPEC, 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2334z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2503za7,
		BGl_z62zc3z04anonymousza32068ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2253z00zzast_varz00,
		BgL_bgl_za762lambda1853za7622504z00, BGl_z62lambda1853z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_localzd2accesszd2envz00zzast_varz00,
		BgL_bgl_za762localza7d2acces2505z00, BGl_z62localzd2accesszb0zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2335z00zzast_varz00,
		BgL_bgl_za762lambda2067za7622506z00, BGl_z62lambda2067z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2254z00zzast_varz00,
		BgL_bgl_za762lambda1852za7622507z00, BGl_z62lambda1852z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2336z00zzast_varz00,
		BgL_bgl_za762lambda2066za7622508z00, BGl_z62lambda2066z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2255z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2509za7,
		BGl_z62zc3z04anonymousza31861ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzd2thezd2closurezd2globalzd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762sfunza7d2theza7d22510za7,
		BGl_z62sfunzd2thezd2closurezd2globalzd2setz12z70zzast_varz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2337z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2511za7,
		BGl_z62zc3z04anonymousza32061ze3ze5zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2256z00zzast_varz00,
		BgL_bgl_za762lambda1860za7622512z00, BGl_z62lambda1860z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2338z00zzast_varz00,
		BgL_bgl_za762lambda2059za7622513z00, BGl_z62lambda2059z62zzast_varz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2257z00zzast_varz00,
		BgL_bgl_za762lambda1859za7622514z00, BGl_z62lambda1859z62zzast_varz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2evaluablezf3zd2envzf3zzast_varz00,
		BgL_bgl_za762globalza7d2eval2515z00,
		BGl_z62globalzd2evaluablezf3z43zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2339z00zzast_varz00,
		BgL_bgl_za762lambda2057za7622516z00, BGl_z62lambda2057z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2258z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2517za7,
		BGl_z62zc3z04anonymousza31869ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2259z00zzast_varz00,
		BgL_bgl_za762lambda1868za7622518z00, BGl_z62lambda1868z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_variablezd2typezd2envz00zzast_varz00,
		BgL_bgl_za762variableza7d2ty2519z00, BGl_z62variablezd2typezb0zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_scnstzd2loczd2envz00zzast_varz00,
		BgL_bgl_za762scnstza7d2locza7b2520za7, BGl_z62scnstzd2loczb0zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_funzd2argszd2noescapezd2setz12zd2envz12zzast_varz00,
		BgL_bgl_za762funza7d2argsza7d22521za7,
		BGl_z62funzd2argszd2noescapezd2setz12za2zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_variablezd2accesszd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762variableza7d2ac2522z00,
		BGl_z62variablezd2accesszd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2340z00zzast_varz00,
		BgL_bgl_za762lambda2085za7622523z00, BGl_z62lambda2085z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2341z00zzast_varz00,
		BgL_bgl_za762lambda2084za7622524z00, BGl_z62lambda2084z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2260z00zzast_varz00,
		BgL_bgl_za762lambda1867za7622525z00, BGl_z62lambda1867z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2342z00zzast_varz00,
		BgL_bgl_za762lambda2090za7622526z00, BGl_z62lambda2090z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2261z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2527za7,
		BGl_z62zc3z04anonymousza31877ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2180z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2528za7,
		BGl_z62zc3z04anonymousza31365ze3ze5zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2343z00zzast_varz00,
		BgL_bgl_za762lambda2089za7622529z00, BGl_z62lambda2089z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2262z00zzast_varz00,
		BgL_bgl_za762lambda1876za7622530z00, BGl_z62lambda1876z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2181z00zzast_varz00,
		BgL_bgl_za762lambda1362za7622531z00, BGl_z62lambda1362z62zzast_varz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2344z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2532za7,
		BGl_z62zc3z04anonymousza32098ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2263z00zzast_varz00,
		BgL_bgl_za762lambda1875za7622533z00, BGl_z62lambda1875z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2182z00zzast_varz00,
		BgL_bgl_za762lambda1353za7622534z00, BGl_z62lambda1353z62zzast_varz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2345z00zzast_varz00,
		BgL_bgl_za762lambda2097za7622535z00, BGl_z62lambda2097z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2264z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2536za7,
		BGl_z62zc3z04anonymousza31885ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2183z00zzast_varz00,
		BgL_bgl_za762lambda1423za7622537z00, BGl_z62lambda1423z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2346z00zzast_varz00,
		BgL_bgl_za762lambda2096za7622538z00, BGl_z62lambda2096z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2265z00zzast_varz00,
		BgL_bgl_za762lambda1884za7622539z00, BGl_z62lambda1884z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2184z00zzast_varz00,
		BgL_bgl_za762lambda1422za7622540z00, BGl_z62lambda1422z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2347z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2541za7,
		BGl_z62zc3z04anonymousza32080ze3ze5zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2266z00zzast_varz00,
		BgL_bgl_za762lambda1883za7622542z00, BGl_z62lambda1883z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2185z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2543za7,
		BGl_z62zc3z04anonymousza31456ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2348z00zzast_varz00,
		BgL_bgl_za762lambda2078za7622544z00, BGl_z62lambda2078z62zzast_varz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2267z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2545za7,
		BGl_z62zc3z04anonymousza31893ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2186z00zzast_varz00,
		BgL_bgl_za762lambda1455za7622546z00, BGl_z62lambda1455z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2349z00zzast_varz00,
		BgL_bgl_za762lambda2076za7622547z00, BGl_z62lambda2076z62zzast_varz00, 0L,
		BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2268z00zzast_varz00,
		BgL_bgl_za762lambda1892za7622548z00, BGl_z62lambda1892z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2187z00zzast_varz00,
		BgL_bgl_za762lambda1454za7622549z00, BGl_z62lambda1454z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2269z00zzast_varz00,
		BgL_bgl_za762lambda1891za7622550z00, BGl_z62lambda1891z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2188z00zzast_varz00,
		BgL_bgl_za762lambda1487za7622551z00, BGl_z62lambda1487z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2189z00zzast_varz00,
		BgL_bgl_za762lambda1486za7622552z00, BGl_z62lambda1486z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_localzd2userzf3zd2envzf3zzast_varz00,
		BgL_bgl_za762localza7d2userza72553za7,
		BGl_z62localzd2userzf3z43zzast_varz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzd2stackzd2allocatorzd2setz12zd2envz12zzast_varz00,
		BgL_bgl_za762cfunza7d2stackza72554za7,
		BGl_z62cfunzd2stackzd2allocatorzd2setz12za2zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzd2dssslzd2keywordszd2envzd2zzast_varz00,
		BgL_bgl_za762sfunza7d2dssslza72555za7,
		BGl_z62sfunzd2dssslzd2keywordsz62zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzd2strengthzd2envz00zzast_varz00,
		BgL_bgl_za762sfunza7d2streng2556z00, BGl_z62sfunzd2strengthzb0zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_svarzd2loczd2envz00zzast_varz00,
		BgL_bgl_za762svarza7d2locza7b02557za7, BGl_z62svarzd2loczb0zzast_varz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzd2stackzd2allocatorzd2setz12zd2envz12zzast_varz00,
		BgL_bgl_za762sfunza7d2stackza72558za7,
		BGl_z62sfunzd2stackzd2allocatorzd2setz12za2zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2350z00zzast_varz00,
		BgL_bgl_za762lambda2113za7622559z00, BGl_z62lambda2113z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2351z00zzast_varz00,
		BgL_bgl_za762lambda2112za7622560z00, BGl_z62lambda2112z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2270z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2561za7,
		BGl_z62zc3z04anonymousza31901ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2352z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2562za7,
		BGl_z62zc3z04anonymousza32108ze3ze5zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2271z00zzast_varz00,
		BgL_bgl_za762lambda1900za7622563z00, BGl_z62lambda1900z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2190z00zzast_varz00,
		BgL_bgl_za762lambda1511za7622564z00, BGl_z62lambda1511z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2353z00zzast_varz00,
		BgL_bgl_za762lambda2106za7622565z00, BGl_z62lambda2106z62zzast_varz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2272z00zzast_varz00,
		BgL_bgl_za762lambda1899za7622566z00, BGl_z62lambda1899z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2191z00zzast_varz00,
		BgL_bgl_za762lambda1510za7622567z00, BGl_z62lambda1510z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2354z00zzast_varz00,
		BgL_bgl_za762lambda2104za7622568z00, BGl_z62lambda2104z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2273z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2569za7,
		BGl_z62zc3z04anonymousza31909ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2192z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2570za7,
		BGl_z62zc3z04anonymousza31538ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2355z00zzast_varz00,
		BgL_bgl_za762lambda2128za7622571z00, BGl_z62lambda2128z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2274z00zzast_varz00,
		BgL_bgl_za762lambda1908za7622572z00, BGl_z62lambda1908z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2193z00zzast_varz00,
		BgL_bgl_za762lambda1537za7622573z00, BGl_z62lambda1537z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2356z00zzast_varz00,
		BgL_bgl_za762lambda2127za7622574z00, BGl_z62lambda2127z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2275z00zzast_varz00,
		BgL_bgl_za762lambda1907za7622575z00, BGl_z62lambda1907z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2194z00zzast_varz00,
		BgL_bgl_za762lambda1536za7622576z00, BGl_z62lambda1536z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2357z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2577za7,
		BGl_z62zc3z04anonymousza32135ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2276z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2578za7,
		BGl_z62zc3z04anonymousza31916ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2195z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2579za7,
		BGl_z62zc3z04anonymousza31555ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_variablezd2typezd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762variableza7d2ty2580z00,
		BGl_z62variablezd2typezd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2358z00zzast_varz00,
		BgL_bgl_za762lambda2134za7622581z00, BGl_z62lambda2134z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2277z00zzast_varz00,
		BgL_bgl_za762lambda1915za7622582z00, BGl_z62lambda1915z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2196z00zzast_varz00,
		BgL_bgl_za762lambda1554za7622583z00, BGl_z62lambda1554z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2359z00zzast_varz00,
		BgL_bgl_za762lambda2133za7622584z00, BGl_z62lambda2133z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2278z00zzast_varz00,
		BgL_bgl_za762lambda1914za7622585z00, BGl_z62lambda1914z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2197z00zzast_varz00,
		BgL_bgl_za762lambda1553za7622586z00, BGl_z62lambda1553z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2279z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2587za7,
		BGl_z62zc3z04anonymousza31923ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2198z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2588za7,
		BGl_z62zc3z04anonymousza31568ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2199z00zzast_varz00,
		BgL_bgl_za762lambda1567za7622589z00, BGl_z62lambda1567z62zzast_varz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_localzd2typezd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762localza7d2typeza72590za7,
		BGl_z62localzd2typezd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzd2valuezd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762globalza7d2valu2591z00,
		BGl_z62globalzd2valuezd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2pragmazd2envz00zzast_varz00,
		BgL_bgl_za762globalza7d2prag2592z00, BGl_z62globalzd2pragmazb0zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2valuezd2envz00zzast_varz00,
		BgL_bgl_za762globalza7d2valu2593z00, BGl_z62globalzd2valuezb0zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2globalzd2envz00zzast_varz00,
		BgL_bgl_za762makeza7d2global2594z00, BGl_z62makezd2globalzb0zzast_varz00,
		0L, BUNSPEC, 20);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2keyzf3zd2envzf3zzast_varz00,
		BgL_bgl_za762globalza7d2keyza72595za7,
		BGl_z62globalzd2keyzf3z43zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2valuezd2envz00zzast_varz00,
		BgL_bgl_za762makeza7d2valueza72596za7, BGl_z62makezd2valuezb0zzast_varz00,
		0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2360z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2597za7,
		BGl_z62zc3z04anonymousza32123ze3ze5zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2361z00zzast_varz00,
		BgL_bgl_za762lambda2121za7622598z00, BGl_z62lambda2121z62zzast_varz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2280z00zzast_varz00,
		BgL_bgl_za762lambda1922za7622599z00, BGl_z62lambda1922z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2362z00zzast_varz00,
		BgL_bgl_za762lambda2119za7622600z00, BGl_z62lambda2119z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2281z00zzast_varz00,
		BgL_bgl_za762lambda1921za7622601z00, BGl_z62lambda1921z62zzast_varz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sexitzd2detachedzf3zd2envzf3zzast_varz00,
		BgL_bgl_za762sexitza7d2detac2602z00,
		BGl_z62sexitzd2detachedzf3z43zzast_varz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_variablezd2userzf3zd2setz12zd2envz33zzast_varz00,
		BgL_bgl_za762variableza7d2us2603z00,
		BGl_z62variablezd2userzf3zd2setz12z83zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2363z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2604za7,
		BGl_z62zc3z04anonymousza32153ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2282z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2605za7,
		BGl_z62zc3z04anonymousza31848ze3ze5zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sexitzf3zd2envz21zzast_varz00,
		BgL_bgl_za762sexitza7f3za791za7za72606za7, BGl_z62sexitzf3z91zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2initzd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762globalza7d2init2607z00,
		BGl_z62globalzd2initzd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2364z00zzast_varz00,
		BgL_bgl_za762lambda2152za7622608z00, BGl_z62lambda2152z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2283z00zzast_varz00,
		BgL_bgl_za762lambda1846za7622609z00, BGl_z62lambda1846z62zzast_varz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2365z00zzast_varz00,
		BgL_bgl_za762lambda2151za7622610z00, BGl_z62lambda2151z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2284z00zzast_varz00,
		BgL_bgl_za762lambda1844za7622611z00, BGl_z62lambda1844z62zzast_varz00, 0L,
		BUNSPEC, 10);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzd2failsafezd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762cfunza7d2failsa2612z00,
		BGl_z62cfunzd2failsafezd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2372z00zzast_varz00,
		BgL_bgl_string2372za700za7za7a2613za7, "", 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2366z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2614za7,
		BGl_z62zc3z04anonymousza32160ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2285z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2615za7,
		BGl_z62zc3z04anonymousza31940ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2373z00zzast_varz00,
		BgL_bgl_string2373za700za7za7a2616za7, "ast_var", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2367z00zzast_varz00,
		BgL_bgl_za762lambda2159za7622617z00, BGl_z62lambda2159z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2286z00zzast_varz00,
		BgL_bgl_za762lambda1939za7622618z00, BGl_z62lambda1939z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2374z00zzast_varz00,
		BgL_bgl_string2374za700za7za7a2619za7,
		"now \077\077? _ feffect write read sexit detached? handler cvar scnst node svar cfun pair-nil method infix? macro? args-type sfun stackable strength the-closure-global keys optionals loc dsssl-keywords class body args-name args property fun args-retescape args-noescape failsafe effect the-closure top? stack-allocator predicate-of side-effect arity local volatile val-noescape key global alias init bstring jvm-type-name src pragma library eval? evaluable? import module variable bool user? occurrencew long occurrence removable fast-alpha access type obj name symbol id ast_var value args-safe read-only ",
		600);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2368z00zzast_varz00,
		BgL_bgl_za762lambda2158za7622620z00, BGl_z62lambda2158z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2287z00zzast_varz00,
		BgL_bgl_za762lambda1938za7622621z00, BGl_z62lambda1938z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2369z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2622za7,
		BGl_z62zc3z04anonymousza32146ze3ze5zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2288z00zzast_varz00,
		BgL_bgl_za762lambda1945za7622623z00, BGl_z62lambda1945z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2289z00zzast_varz00,
		BgL_bgl_za762lambda1944za7622624z00, BGl_z62lambda1944z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cvarzd2nilzd2envz00zzast_varz00,
		BgL_bgl_za762cvarza7d2nilza7b02625za7, BGl_z62cvarzd2nilzb0zzast_varz00, 0L,
		BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_variablezd2occurrencezd2envz00zzast_varz00,
		BgL_bgl_za762variableza7d2oc2626z00,
		BGl_z62variablezd2occurrencezb0zzast_varz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_funzd2effectzd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762funza7d2effectza72627za7,
		BGl_z62funzd2effectzd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2modulezd2envz00zzast_varz00,
		BgL_bgl_za762globalza7d2modu2628z00, BGl_z62globalzd2modulezb0zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2370z00zzast_varz00,
		BgL_bgl_za762lambda2144za7622629z00, BGl_z62lambda2144z62zzast_varz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2371z00zzast_varz00,
		BgL_bgl_za762lambda2142za7622630z00, BGl_z62lambda2142z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2290z00zzast_varz00,
		BgL_bgl_za762lambda1950za7622631z00, BGl_z62lambda1950z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2291z00zzast_varz00,
		BgL_bgl_za762lambda1949za7622632z00, BGl_z62lambda1949z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2292z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2633za7,
		BGl_z62zc3z04anonymousza31957ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2293z00zzast_varz00,
		BgL_bgl_za762lambda1956za7622634z00, BGl_z62lambda1956z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2294z00zzast_varz00,
		BgL_bgl_za762lambda1955za7622635z00, BGl_z62lambda1955z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2295z00zzast_varz00,
		BgL_bgl_za762lambda1962za7622636z00, BGl_z62lambda1962z62zzast_varz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2296z00zzast_varz00,
		BgL_bgl_za762lambda1961za7622637z00, BGl_z62lambda1961z62zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2297z00zzast_varz00,
		BgL_bgl_za762za7c3za704anonymo2638za7,
		BGl_z62zc3z04anonymousza31969ze3ze5zzast_varz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2298z00zzast_varz00,
		BgL_bgl_za762lambda1968za7622639z00, BGl_z62lambda1968z62zzast_varz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_variablezd2occurrencewzd2envz00zzast_varz00,
		BgL_bgl_za762variableza7d2oc2640z00,
		BGl_z62variablezd2occurrencewzb0zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2299z00zzast_varz00,
		BgL_bgl_za762lambda1967za7622641z00, BGl_z62lambda1967z62zzast_varz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfunzd2effectzd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762cfunza7d2effect2642z00,
		BGl_z62cfunzd2effectzd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzd2optionalszd2envz00zzast_varz00,
		BgL_bgl_za762sfunza7d2option2643z00, BGl_z62sfunzd2optionalszb0zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzd2argszd2namezd2envzd2zzast_varz00,
		BgL_bgl_za762sfunza7d2argsza7d2644za7,
		BGl_z62sfunzd2argszd2namez62zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2evalzf3zd2envzf3zzast_varz00,
		BgL_bgl_za762globalza7d2eval2645z00, BGl_z62globalzd2evalzf3z43zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzd2infixzf3zd2setz12zd2envz33zzast_varz00,
		BgL_bgl_za762cfunza7d2infixza72646za7,
		BGl_z62cfunzd2infixzf3zd2setz12z83zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_funzd2stackzd2allocatorzd2setz12zd2envz12zzast_varz00,
		BgL_bgl_za762funza7d2stackza7d2647za7,
		BGl_z62funzd2stackzd2allocatorzd2setz12za2zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2funzd2envz00zzast_varz00,
		BgL_bgl_za762makeza7d2funza7b02648za7, BGl_z62makezd2funzb0zzast_varz00, 0L,
		BUNSPEC, 10);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_localzd2valzd2noescapezd2envzd2zzast_varz00,
		BgL_bgl_za762localza7d2valza7d2649za7,
		BGl_z62localzd2valzd2noescapez62zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_scnstzd2nodezd2envz00zzast_varz00,
		BgL_bgl_za762scnstza7d2nodeza72650za7, BGl_z62scnstzd2nodezb0zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_feffectzd2writezd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762feffectza7d2wri2651z00,
		BGl_z62feffectzd2writezd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_funzd2argszd2retescapezd2setz12zd2envz12zzast_varz00,
		BgL_bgl_za762funza7d2argsza7d22652za7,
		BGl_z62funzd2argszd2retescapezd2setz12za2zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzd2libraryzd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762globalza7d2libr2653z00,
		BGl_z62globalzd2libraryzd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_valuezd2nilzd2envz00zzast_varz00,
		BgL_bgl_za762valueza7d2nilza7b2654za7, BGl_z62valuezd2nilzb0zzast_varz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzd2fastzd2alphazd2setz12zd2envz12zzast_varz00,
		BgL_bgl_za762localza7d2fastza72655za7,
		BGl_z62localzd2fastzd2alphazd2setz12za2zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfunzd2failsafezd2envz00zzast_varz00,
		BgL_bgl_za762cfunza7d2failsa2656z00, BGl_z62cfunzd2failsafezb0zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzd2occurrencewzd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762globalza7d2occu2657z00,
		BGl_z62globalzd2occurrencewzd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzd2occurrencezd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762localza7d2occur2658z00,
		BGl_z62localzd2occurrencezd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2userzf3zd2envzf3zzast_varz00,
		BgL_bgl_za762globalza7d2user2659z00, BGl_z62globalzd2userzf3z43zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_valuezf3zd2envz21zzast_varz00,
		BgL_bgl_za762valueza7f3za791za7za72660za7, BGl_z62valuezf3z91zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2typezd2envz00zzast_varz00,
		BgL_bgl_za762globalza7d2type2661z00, BGl_z62globalzd2typezb0zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzd2failsafezd2envz00zzast_varz00,
		BgL_bgl_za762sfunza7d2failsa2662z00, BGl_z62sfunzd2failsafezb0zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_funzd2predicatezd2ofzd2envzd2zzast_varz00,
		BgL_bgl_za762funza7d2predica2663z00,
		BGl_z62funzd2predicatezd2ofz62zzast_varz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_scnstzd2loczd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762scnstza7d2locza7d2664za7,
		BGl_z62scnstzd2loczd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzd2fastzd2alphazd2setz12zd2envz12zzast_varz00,
		BgL_bgl_za762globalza7d2fast2665z00,
		BGl_z62globalzd2fastzd2alphazd2setz12za2zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_localzd2typezd2envz00zzast_varz00,
		BgL_bgl_za762localza7d2typeza72666za7, BGl_z62localzd2typezb0zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzd2occurrencezd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762globalza7d2occu2667z00,
		BGl_z62globalzd2occurrencezd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_localzf3zd2envz21zzast_varz00,
		BgL_bgl_za762localza7f3za791za7za72668za7, BGl_z62localzf3z91zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzd2nilzd2envz00zzast_varz00,
		BgL_bgl_za762sfunza7d2nilza7b02669za7, BGl_z62sfunzd2nilzb0zzast_varz00, 0L,
		BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzd2stackzd2allocatorzd2envzd2zzast_varz00,
		BgL_bgl_za762sfunza7d2stackza72670za7,
		BGl_z62sfunzd2stackzd2allocatorz62zzast_varz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_variablezd2fastzd2alphazd2setz12zd2envz12zzast_varz00,
		BgL_bgl_za762variableza7d2fa2671z00,
		BGl_z62variablezd2fastzd2alphazd2setz12za2zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzd2userzf3zd2setz12zd2envz33zzast_varz00,
		BgL_bgl_za762localza7d2userza72672za7,
		BGl_z62localzd2userzf3zd2setz12z83zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_variablezd2occurrencezd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762variableza7d2oc2673z00,
		BGl_z62variablezd2occurrencezd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzd2bodyzd2envz00zzast_varz00,
		BgL_bgl_za762sfunza7d2bodyza7b2674za7, BGl_z62sfunzd2bodyzb0zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2srczd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762globalza7d2srcza72675za7,
		BGl_z62globalzd2srczd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_localzd2occurrencezd2envz00zzast_varz00,
		BgL_bgl_za762localza7d2occur2676z00,
		BGl_z62localzd2occurrencezb0zzast_varz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfunzd2topzf3zd2setz12zd2envz33zzast_varz00,
		BgL_bgl_za762cfunza7d2topza7f32677za7,
		BGl_z62cfunzd2topzf3zd2setz12z83zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2aliaszd2envz00zzast_varz00,
		BgL_bgl_za762globalza7d2alia2678z00, BGl_z62globalzd2aliaszb0zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_scnstzf3zd2envz21zzast_varz00,
		BgL_bgl_za762scnstza7f3za791za7za72679za7, BGl_z62scnstzf3z91zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzd2pragmazd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762globalza7d2prag2680z00,
		BGl_z62globalzd2pragmazd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzd2argszd2noescapezd2envzd2zzast_varz00,
		BgL_bgl_za762sfunza7d2argsza7d2681za7,
		BGl_z62sfunzd2argszd2noescapez62zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2variablezd2envz00zzast_varz00,
		BgL_bgl_za762makeza7d2variab2682z00, BGl_z62makezd2variablezb0zzast_varz00,
		0L, BUNSPEC, 10);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzd2handlerzd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762sexitza7d2handl2683z00,
		BGl_z62sexitzd2handlerzd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzd2aliaszd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762globalza7d2alia2684z00,
		BGl_z62globalzd2aliaszd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2svarzd2envz00zzast_varz00,
		BgL_bgl_za762makeza7d2svarza7b2685za7, BGl_z62makezd2svarzb0zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzd2removablezd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762globalza7d2remo2686z00,
		BGl_z62globalzd2removablezd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzd2argszd2envz00zzast_varz00,
		BgL_bgl_za762sfunza7d2argsza7b2687za7, BGl_z62sfunzd2argszb0zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzd2classzd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762sfunza7d2classza72688za7,
		BGl_z62sfunzd2classzd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_funzd2stackzd2allocatorzd2envzd2zzast_varz00,
		BgL_bgl_za762funza7d2stackza7d2689za7,
		BGl_z62funzd2stackzd2allocatorz62zzast_varz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_funzd2thezd2closurezd2setz12zd2envz12zzast_varz00,
		BgL_bgl_za762funza7d2theza7d2c2690za7,
		BGl_z62funzd2thezd2closurezd2setz12za2zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2removablezd2envz00zzast_varz00,
		BgL_bgl_za762globalza7d2remo2691z00,
		BGl_z62globalzd2removablezb0zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2nilzd2envz00zzast_varz00,
		BgL_bgl_za762globalza7d2nilza72692za7, BGl_z62globalzd2nilzb0zzast_varz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzd2accesszd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762localza7d2acces2693z00,
		BGl_z62localzd2accesszd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzd2sidezd2effectzd2envzd2zzast_varz00,
		BgL_bgl_za762sfunza7d2sideza7d2694za7,
		BGl_z62sfunzd2sidezd2effectz62zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_funzf3zd2envz21zzast_varz00,
		BgL_bgl_za762funza7f3za791za7za7as2695za7, BGl_z62funzf3z91zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfunzf3zd2envz21zzast_varz00,
		BgL_bgl_za762cfunza7f3za791za7za7a2696za7, BGl_z62cfunzf3z91zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfunzd2predicatezd2ofzd2envzd2zzast_varz00,
		BgL_bgl_za762cfunza7d2predic2697z00,
		BGl_z62cfunzd2predicatezd2ofz62zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2accesszd2envz00zzast_varz00,
		BgL_bgl_za762globalza7d2acce2698z00, BGl_z62globalzd2accesszb0zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzd2keyzf3zd2envzf3zzast_varz00,
		BgL_bgl_za762sfunza7d2keyza7f32699za7, BGl_z62sfunzd2keyzf3z43zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzd2sidezd2effectzd2setz12zd2envz12zzast_varz00,
		BgL_bgl_za762cfunza7d2sideza7d2700za7,
		BGl_z62cfunzd2sidezd2effectzd2setz12za2zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_variablezd2fastzd2alphazd2envzd2zzast_varz00,
		BgL_bgl_za762variableza7d2fa2701z00,
		BGl_z62variablezd2fastzd2alphaz62zzast_varz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzd2userzf3zd2setz12zd2envz33zzast_varz00,
		BgL_bgl_za762globalza7d2user2702z00,
		BGl_z62globalzd2userzf3zd2setz12z83zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzf3zd2envz21zzast_varz00,
		BgL_bgl_za762sfunza7f3za791za7za7a2703za7, BGl_z62sfunzf3z91zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzd2predicatezd2ofzd2envzd2zzast_varz00,
		BgL_bgl_za762sfunza7d2predic2704z00,
		BGl_z62sfunzd2predicatezd2ofz62zzast_varz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzd2sidezd2effectzd2setz12zd2envz12zzast_varz00,
		BgL_bgl_za762sfunza7d2sideza7d2705za7,
		BGl_z62sfunzd2sidezd2effectzd2setz12za2zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2occurrencezd2envz00zzast_varz00,
		BgL_bgl_za762globalza7d2occu2706z00,
		BGl_z62globalzd2occurrencezb0zzast_varz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzd2readzd2onlyzf3zd2envz21zzast_varz00,
		BgL_bgl_za762globalza7d2read2707z00,
		BGl_z62globalzd2readzd2onlyzf3z91zzast_varz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzd2valzd2noescapezd2setz12zd2envz12zzast_varz00,
		BgL_bgl_za762localza7d2valza7d2708za7,
		BGl_z62localzd2valzd2noescapezd2setz12za2zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2feffectzd2envz00zzast_varz00,
		BgL_bgl_za762makeza7d2feffec2709z00, BGl_z62makezd2feffectzb0zzast_varz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_funzd2thezd2closurezd2envzd2zzast_varz00,
		BgL_bgl_za762funza7d2theza7d2c2710za7,
		BGl_z62funzd2thezd2closurez62zzast_varz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_funzd2predicatezd2ofzd2setz12zd2envz12zzast_varz00,
		BgL_bgl_za762funza7d2predica2711z00,
		BGl_z62funzd2predicatezd2ofzd2setz12za2zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_funzd2topzf3zd2envzf3zzast_varz00,
		BgL_bgl_za762funza7d2topza7f3za72712z00, BGl_z62funzd2topzf3z43zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_variablezd2removablezd2envz00zzast_varz00,
		BgL_bgl_za762variableza7d2re2713z00,
		BGl_z62variablezd2removablezb0zzast_varz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzd2dssslzd2keywordszd2setz12zd2envz12zzast_varz00,
		BgL_bgl_za762sfunza7d2dssslza72714za7,
		BGl_z62sfunzd2dssslzd2keywordszd2setz12za2zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzd2thezd2closurezd2globalzd2envz00zzast_varz00,
		BgL_bgl_za762sfunza7d2theza7d22715za7,
		BGl_z62sfunzd2thezd2closurezd2globalzb0zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_localzd2removablezd2envz00zzast_varz00,
		BgL_bgl_za762localza7d2remov2716z00,
		BGl_z62localzd2removablezb0zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2cfunzd2envz00zzast_varz00,
		BgL_bgl_za762makeza7d2cfunza7b2717za7, BGl_z62makezd2cfunzb0zzast_varz00,
		0L, BUNSPEC, 14);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfunzd2arityzd2envz00zzast_varz00,
		BgL_bgl_za762cfunza7d2arityza72718za7, BGl_z62cfunzd2arityzb0zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_funzd2topzf3zd2setz12zd2envz33zzast_varz00,
		BgL_bgl_za762funza7d2topza7f3za72719z00,
		BGl_z62funzd2topzf3zd2setz12z83zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfunzd2thezd2closurezd2envzd2zzast_varz00,
		BgL_bgl_za762cfunza7d2theza7d22720za7,
		BGl_z62cfunzd2thezd2closurez62zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfunzd2effectzd2envz00zzast_varz00,
		BgL_bgl_za762cfunza7d2effect2721z00, BGl_z62cfunzd2effectzb0zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfunzd2macrozf3zd2envzf3zzast_varz00,
		BgL_bgl_za762cfunza7d2macroza72722za7,
		BGl_z62cfunzd2macrozf3z43zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_funzd2failsafezd2envz00zzast_varz00,
		BgL_bgl_za762funza7d2failsaf2723z00, BGl_z62funzd2failsafezb0zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2libraryzd2envz00zzast_varz00,
		BgL_bgl_za762globalza7d2libr2724z00, BGl_z62globalzd2libraryzb0zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzd2effectzd2envz00zzast_varz00,
		BgL_bgl_za762sfunza7d2effect2725z00, BGl_z62sfunzd2effectzb0zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_localzd2occurrencewzd2envz00zzast_varz00,
		BgL_bgl_za762localza7d2occur2726z00,
		BGl_z62localzd2occurrencewzb0zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_funzd2nilzd2envz00zzast_varz00,
		BgL_bgl_za762funza7d2nilza7b0za72727z00, BGl_z62funzd2nilzb0zzast_varz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_variablezd2accesszd2envz00zzast_varz00,
		BgL_bgl_za762variableza7d2ac2728z00,
		BGl_z62variablezd2accesszb0zzast_varz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzd2removablezd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762localza7d2remov2729z00,
		BGl_z62localzd2removablezd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzf3zd2envz21zzast_varz00,
		BgL_bgl_za762globalza7f3za791za72730z00, BGl_z62globalzf3z91zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_variablezd2nilzd2envz00zzast_varz00,
		BgL_bgl_za762variableza7d2ni2731z00, BGl_z62variablezd2nilzb0zzast_varz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2sexitzd2envz00zzast_varz00,
		BgL_bgl_za762makeza7d2sexitza72732za7, BGl_z62makezd2sexitzb0zzast_varz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzd2argszd2retescapezd2setz12zd2envz12zzast_varz00,
		BgL_bgl_za762sfunza7d2argsza7d2733za7,
		BGl_z62sfunzd2argszd2retescapezd2setz12za2zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzd2propertyzd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762sfunza7d2proper2734z00,
		BGl_z62sfunzd2propertyzd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2idzd2envz00zzast_varz00,
		BgL_bgl_za762globalza7d2idza7b2735za7, BGl_z62globalzd2idzb0zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_feffectzf3zd2envz21zzast_varz00,
		BgL_bgl_za762feffectza7f3za7912736za7, BGl_z62feffectzf3z91zzast_varz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2initzd2envz00zzast_varz00,
		BgL_bgl_za762globalza7d2init2737z00, BGl_z62globalzd2initzb0zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzd2importzd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762globalza7d2impo2738z00,
		BGl_z62globalzd2importzd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzd2predicatezd2ofzd2setz12zd2envz12zzast_varz00,
		BgL_bgl_za762sfunza7d2predic2739z00,
		BGl_z62sfunzd2predicatezd2ofzd2setz12za2zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_localzd2fastzd2alphazd2envzd2zzast_varz00,
		BgL_bgl_za762localza7d2fastza72740za7,
		BGl_z62localzd2fastzd2alphaz62zzast_varz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2namezd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762globalza7d2name2741z00,
		BGl_z62globalzd2namezd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzd2modulezd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762globalza7d2modu2742z00,
		BGl_z62globalzd2modulezd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_funzd2argszd2retescapezd2envzd2zzast_varz00,
		BgL_bgl_za762funza7d2argsza7d22743za7,
		BGl_z62funzd2argszd2retescapez62zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzd2propertyzd2envz00zzast_varz00,
		BgL_bgl_za762sfunza7d2proper2744z00, BGl_z62sfunzd2propertyzb0zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzd2argszd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762sfunza7d2argsza7d2745za7,
		BGl_z62sfunzd2argszd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_variablezd2valuezd2envz00zzast_varz00,
		BgL_bgl_za762variableza7d2va2746z00, BGl_z62variablezd2valuezb0zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_localzd2nilzd2envz00zzast_varz00,
		BgL_bgl_za762localza7d2nilza7b2747za7, BGl_z62localzd2nilzb0zzast_varz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_funzd2argszd2noescapezd2envzd2zzast_varz00,
		BgL_bgl_za762funza7d2argsza7d22748za7,
		BGl_z62funzd2argszd2noescapez62zzast_varz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzd2loczd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762sfunza7d2locza7d22749za7,
		BGl_z62sfunzd2loczd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_feffectzd2nilzd2envz00zzast_varz00,
		BgL_bgl_za762feffectza7d2nil2750z00, BGl_z62feffectzd2nilzb0zzast_varz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2srczd2envz00zzast_varz00,
		BgL_bgl_za762globalza7d2srcza72751za7, BGl_z62globalzd2srczb0zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzd2strengthzd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762sfunza7d2streng2752z00,
		BGl_z62sfunzd2strengthzd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_scnstzd2nilzd2envz00zzast_varz00,
		BgL_bgl_za762scnstza7d2nilza7b2753za7, BGl_z62scnstzd2nilzb0zzast_varz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sexitzd2nilzd2envz00zzast_varz00,
		BgL_bgl_za762sexitza7d2nilza7b2754za7, BGl_z62sexitzd2nilzb0zzast_varz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzd2detachedzf3zd2setz12zd2envz33zzast_varz00,
		BgL_bgl_za762sexitza7d2detac2755z00,
		BGl_z62sexitzd2detachedzf3zd2setz12z83zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzd2optionalzf3zd2envzf3zzast_varz00,
		BgL_bgl_za762sfunza7d2option2756z00,
		BGl_z62sfunzd2optionalzf3z43zzast_varz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_variablezd2namezd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762variableza7d2na2757z00,
		BGl_z62variablezd2namezd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_localzd2namezd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762localza7d2nameza72758za7,
		BGl_z62localzd2namezd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzd2failsafezd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762sfunza7d2failsa2759z00,
		BGl_z62sfunzd2failsafezd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2fastzd2alphazd2envzd2zzast_varz00,
		BgL_bgl_za762globalza7d2fast2760z00,
		BGl_z62globalzd2fastzd2alphaz62zzast_varz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2optionalzf3zd2envzf3zzast_varz00,
		BgL_bgl_za762globalza7d2opti2761z00,
		BGl_z62globalzd2optionalzf3z43zzast_varz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzd2jvmzd2typezd2namezd2envz00zzast_varz00,
		BgL_bgl_za762globalza7d2jvmza72762za7,
		BGl_z62globalzd2jvmzd2typezd2namezb0zzast_varz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzd2accesszd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762globalza7d2acce2763z00,
		BGl_z62globalzd2accesszd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_svarzd2nilzd2envz00zzast_varz00,
		BgL_bgl_za762svarza7d2nilza7b02764za7, BGl_z62svarzd2nilzb0zzast_varz00, 0L,
		BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzd2bodyzd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762sfunza7d2bodyza7d2765za7,
		BGl_z62sfunzd2bodyzd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzd2jvmzd2typezd2namezd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762globalza7d2jvmza72766za7,
		BGl_z62globalzd2jvmzd2typezd2namezd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_variablezf3zd2envz21zzast_varz00,
		BgL_bgl_za762variableza7f3za792767za7, BGl_z62variablezf3z91zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzd2effectzd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762sfunza7d2effect2768z00,
		BGl_z62sfunzd2effectzd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_variablezd2userzf3zd2envzf3zzast_varz00,
		BgL_bgl_za762variableza7d2us2769z00,
		BGl_z62variablezd2userzf3z43zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzd2loczd2envz00zzast_varz00,
		BgL_bgl_za762sfunza7d2locza7b02770za7, BGl_z62sfunzd2loczb0zzast_varz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_variablezd2removablezd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762variableza7d2re2771z00,
		BGl_z62variablezd2removablezd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_feffectzd2writezd2envz00zzast_varz00,
		BgL_bgl_za762feffectza7d2wri2772z00, BGl_z62feffectzd2writezb0zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzd2stackablezd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762sfunza7d2stacka2773z00,
		BGl_z62sfunzd2stackablezd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_variablezd2namezd2envz00zzast_varz00,
		BgL_bgl_za762variableza7d2na2774z00, BGl_z62variablezd2namezb0zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2localzd2envz00zzast_varz00,
		BgL_bgl_za762makeza7d2localza72775za7, BGl_z62makezd2localzb0zzast_varz00,
		0L, BUNSPEC, 12);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzd2argszd2noescapezd2setz12zd2envz12zzast_varz00,
		BgL_bgl_za762cfunza7d2argsza7d2776za7,
		BGl_z62cfunzd2argszd2noescapezd2setz12za2zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzd2classzd2envz00zzast_varz00,
		BgL_bgl_za762sfunza7d2classza72777za7, BGl_z62sfunzd2classzb0zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzd2evalzf3zd2setz12zd2envz33zzast_varz00,
		BgL_bgl_za762globalza7d2eval2778z00,
		BGl_z62globalzd2evalzf3zd2setz12z83zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cvarzf3zd2envz21zzast_varz00,
		BgL_bgl_za762cvarza7f3za791za7za7a2779za7, BGl_z62cvarzf3z91zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzd2argszd2noescapezd2setz12zd2envz12zzast_varz00,
		BgL_bgl_za762sfunza7d2argsza7d2780za7,
		BGl_z62sfunzd2argszd2noescapezd2setz12za2zzast_varz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2scnstzd2envz00zzast_varz00,
		BgL_bgl_za762makeza7d2scnstza72781za7, BGl_z62makezd2scnstzb0zzast_varz00,
		0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_svarzf3zd2envz21zzast_varz00,
		BgL_bgl_za762svarza7f3za791za7za7a2782za7, BGl_z62svarzf3z91zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfunzd2nilzd2envz00zzast_varz00,
		BgL_bgl_za762cfunza7d2nilza7b02783za7, BGl_z62cfunzd2nilzb0zzast_varz00, 0L,
		BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunzd2stackzd2allocatorzd2envzd2zzast_varz00,
		BgL_bgl_za762cfunza7d2stackza72784za7,
		BGl_z62cfunzd2stackzd2allocatorz62zzast_varz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2occurrencewzd2envz00zzast_varz00,
		BgL_bgl_za762globalza7d2occu2785z00,
		BGl_z62globalzd2occurrencewzb0zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sexitzd2handlerzd2envz00zzast_varz00,
		BgL_bgl_za762sexitza7d2handl2786z00, BGl_z62sexitzd2handlerzb0zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfunzd2argszd2noescapezd2envzd2zzast_varz00,
		BgL_bgl_za762cfunza7d2argsza7d2787za7,
		BGl_z62cfunzd2argszd2noescapez62zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfunzd2topzf3zd2envzf3zzast_varz00,
		BgL_bgl_za762cfunza7d2topza7f32788za7, BGl_z62cfunzd2topzf3z43zzast_varz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfunzd2methodzd2setz12zd2envzc0zzast_varz00,
		BgL_bgl_za762cfunza7d2method2789z00,
		BGl_z62cfunzd2methodzd2setz12z70zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzd2topzf3zd2setz12zd2envz33zzast_varz00,
		BgL_bgl_za762sfunza7d2topza7f32790za7,
		BGl_z62sfunzd2topzf3zd2setz12z83zzast_varz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_funzd2sidezd2effectzd2envzd2zzast_varz00,
		BgL_bgl_za762funza7d2sideza7d22791za7,
		BGl_z62funzd2sidezd2effectz62zzast_varz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2cvarzd2envz00zzast_varz00,
		BgL_bgl_za762makeza7d2cvarza7b2792za7, BGl_z62makezd2cvarzb0zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzd2topzf3zd2envzf3zzast_varz00,
		BgL_bgl_za762sfunza7d2topza7f32793za7, BGl_z62sfunzd2topzf3z43zzast_varz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_feffectzd2readzd2envz00zzast_varz00,
		BgL_bgl_za762feffectza7d2rea2794z00, BGl_z62feffectzd2readzb0zzast_varz00,
		0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzast_varz00));
		     ADD_ROOT((void *) (&BGl_funz00zzast_varz00));
		     ADD_ROOT((void *) (&BGl_sfunz00zzast_varz00));
		     ADD_ROOT((void *) (&BGl_sexitz00zzast_varz00));
		     ADD_ROOT((void *) (&BGl_svarz00zzast_varz00));
		     ADD_ROOT((void *) (&BGl_cfunz00zzast_varz00));
		     ADD_ROOT((void *) (&BGl_localz00zzast_varz00));
		     ADD_ROOT((void *) (&BGl_feffectz00zzast_varz00));
		     ADD_ROOT((void *) (&BGl_scnstz00zzast_varz00));
		     ADD_ROOT((void *) (&BGl_valuez00zzast_varz00));
		     ADD_ROOT((void *) (&BGl_variablez00zzast_varz00));
		     ADD_ROOT((void *) (&BGl_globalz00zzast_varz00));
		     ADD_ROOT((void *) (&BGl_cvarz00zzast_varz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long
		BgL_checksumz00_4226, char *BgL_fromz00_4227)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_varz00))
				{
					BGl_requirezd2initializa7ationz75zzast_varz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_varz00();
					BGl_libraryzd2moduleszd2initz00zzast_varz00();
					BGl_cnstzd2initzd2zzast_varz00();
					BGl_importedzd2moduleszd2initz00zzast_varz00();
					BGl_objectzd2initzd2zzast_varz00();
					return BGl_methodzd2initzd2zzast_varz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_varz00(void)
	{
		{	/* Ast/var.scm 14 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "ast_var");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_var");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_var");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "ast_var");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "ast_var");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L, "ast_var");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "ast_var");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"ast_var");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_varz00(void)
	{
		{	/* Ast/var.scm 14 */
			{	/* Ast/var.scm 14 */
				obj_t BgL_cportz00_3743;

				{	/* Ast/var.scm 14 */
					obj_t BgL_stringz00_3750;

					BgL_stringz00_3750 = BGl_string2374z00zzast_varz00;
					{	/* Ast/var.scm 14 */
						obj_t BgL_startz00_3751;

						BgL_startz00_3751 = BINT(0L);
						{	/* Ast/var.scm 14 */
							obj_t BgL_endz00_3752;

							BgL_endz00_3752 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3750)));
							{	/* Ast/var.scm 14 */

								BgL_cportz00_3743 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3750, BgL_startz00_3751, BgL_endz00_3752);
				}}}}
				{
					long BgL_iz00_3744;

					BgL_iz00_3744 = 76L;
				BgL_loopz00_3745:
					if ((BgL_iz00_3744 == -1L))
						{	/* Ast/var.scm 14 */
							return BUNSPEC;
						}
					else
						{	/* Ast/var.scm 14 */
							{	/* Ast/var.scm 14 */
								obj_t BgL_arg2375z00_3746;

								{	/* Ast/var.scm 14 */

									{	/* Ast/var.scm 14 */
										obj_t BgL_locationz00_3748;

										BgL_locationz00_3748 = BBOOL(((bool_t) 0));
										{	/* Ast/var.scm 14 */

											BgL_arg2375z00_3746 =
												BGl_readz00zz__readerz00(BgL_cportz00_3743,
												BgL_locationz00_3748);
										}
									}
								}
								{	/* Ast/var.scm 14 */
									int BgL_tmpz00_4254;

									BgL_tmpz00_4254 = (int) (BgL_iz00_3744);
									CNST_TABLE_SET(BgL_tmpz00_4254, BgL_arg2375z00_3746);
							}}
							{	/* Ast/var.scm 14 */
								int BgL_auxz00_3749;

								BgL_auxz00_3749 = (int) ((BgL_iz00_3744 - 1L));
								{
									long BgL_iz00_4259;

									BgL_iz00_4259 = (long) (BgL_auxz00_3749);
									BgL_iz00_3744 = BgL_iz00_4259;
									goto BgL_loopz00_3745;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_varz00(void)
	{
		{	/* Ast/var.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* make-value */
	BGL_EXPORTED_DEF BgL_valuez00_bglt BGl_makezd2valuezd2zzast_varz00(void)
	{
		{	/* Ast/var.sch 278 */
			{	/* Ast/var.sch 278 */
				BgL_valuez00_bglt BgL_new1184z00_3754;

				{	/* Ast/var.sch 278 */
					BgL_valuez00_bglt BgL_new1183z00_3755;

					BgL_new1183z00_3755 =
						((BgL_valuez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_valuez00_bgl))));
					{	/* Ast/var.sch 278 */
						long BgL_arg1305z00_3756;

						BgL_arg1305z00_3756 = BGL_CLASS_NUM(BGl_valuez00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1183z00_3755), BgL_arg1305z00_3756);
					}
					BgL_new1184z00_3754 = BgL_new1183z00_3755;
				}
				return BgL_new1184z00_3754;
			}
		}

	}



/* &make-value */
	BgL_valuez00_bglt BGl_z62makezd2valuezb0zzast_varz00(obj_t BgL_envz00_2471)
	{
		{	/* Ast/var.sch 278 */
			return BGl_makezd2valuezd2zzast_varz00();
		}

	}



/* value? */
	BGL_EXPORTED_DEF bool_t BGl_valuezf3zf3zzast_varz00(obj_t BgL_objz00_3)
	{
		{	/* Ast/var.sch 279 */
			{	/* Ast/var.sch 279 */
				obj_t BgL_classz00_3757;

				BgL_classz00_3757 = BGl_valuez00zzast_varz00;
				if (BGL_OBJECTP(BgL_objz00_3))
					{	/* Ast/var.sch 279 */
						BgL_objectz00_bglt BgL_arg1807z00_3758;

						BgL_arg1807z00_3758 = (BgL_objectz00_bglt) (BgL_objz00_3);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Ast/var.sch 279 */
								long BgL_idxz00_3759;

								BgL_idxz00_3759 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3758);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3759 + 1L)) == BgL_classz00_3757);
							}
						else
							{	/* Ast/var.sch 279 */
								bool_t BgL_res2162z00_3762;

								{	/* Ast/var.sch 279 */
									obj_t BgL_oclassz00_3763;

									{	/* Ast/var.sch 279 */
										obj_t BgL_arg1815z00_3764;
										long BgL_arg1816z00_3765;

										BgL_arg1815z00_3764 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Ast/var.sch 279 */
											long BgL_arg1817z00_3766;

											BgL_arg1817z00_3766 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3758);
											BgL_arg1816z00_3765 = (BgL_arg1817z00_3766 - OBJECT_TYPE);
										}
										BgL_oclassz00_3763 =
											VECTOR_REF(BgL_arg1815z00_3764, BgL_arg1816z00_3765);
									}
									{	/* Ast/var.sch 279 */
										bool_t BgL__ortest_1115z00_3767;

										BgL__ortest_1115z00_3767 =
											(BgL_classz00_3757 == BgL_oclassz00_3763);
										if (BgL__ortest_1115z00_3767)
											{	/* Ast/var.sch 279 */
												BgL_res2162z00_3762 = BgL__ortest_1115z00_3767;
											}
										else
											{	/* Ast/var.sch 279 */
												long BgL_odepthz00_3768;

												{	/* Ast/var.sch 279 */
													obj_t BgL_arg1804z00_3769;

													BgL_arg1804z00_3769 = (BgL_oclassz00_3763);
													BgL_odepthz00_3768 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3769);
												}
												if ((1L < BgL_odepthz00_3768))
													{	/* Ast/var.sch 279 */
														obj_t BgL_arg1802z00_3770;

														{	/* Ast/var.sch 279 */
															obj_t BgL_arg1803z00_3771;

															BgL_arg1803z00_3771 = (BgL_oclassz00_3763);
															BgL_arg1802z00_3770 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3771,
																1L);
														}
														BgL_res2162z00_3762 =
															(BgL_arg1802z00_3770 == BgL_classz00_3757);
													}
												else
													{	/* Ast/var.sch 279 */
														BgL_res2162z00_3762 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2162z00_3762;
							}
					}
				else
					{	/* Ast/var.sch 279 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &value? */
	obj_t BGl_z62valuezf3z91zzast_varz00(obj_t BgL_envz00_2472,
		obj_t BgL_objz00_2473)
	{
		{	/* Ast/var.sch 279 */
			return BBOOL(BGl_valuezf3zf3zzast_varz00(BgL_objz00_2473));
		}

	}



/* value-nil */
	BGL_EXPORTED_DEF BgL_valuez00_bglt BGl_valuezd2nilzd2zzast_varz00(void)
	{
		{	/* Ast/var.sch 280 */
			{	/* Ast/var.sch 280 */
				obj_t BgL_classz00_1613;

				BgL_classz00_1613 = BGl_valuez00zzast_varz00;
				{	/* Ast/var.sch 280 */
					obj_t BgL__ortest_1117z00_1614;

					BgL__ortest_1117z00_1614 = BGL_CLASS_NIL(BgL_classz00_1613);
					if (CBOOL(BgL__ortest_1117z00_1614))
						{	/* Ast/var.sch 280 */
							return ((BgL_valuez00_bglt) BgL__ortest_1117z00_1614);
						}
					else
						{	/* Ast/var.sch 280 */
							return
								((BgL_valuez00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1613));
						}
				}
			}
		}

	}



/* &value-nil */
	BgL_valuez00_bglt BGl_z62valuezd2nilzb0zzast_varz00(obj_t BgL_envz00_2474)
	{
		{	/* Ast/var.sch 280 */
			return BGl_valuezd2nilzd2zzast_varz00();
		}

	}



/* make-variable */
	BGL_EXPORTED_DEF BgL_variablez00_bglt BGl_makezd2variablezd2zzast_varz00(obj_t
		BgL_id1282z00_4, obj_t BgL_name1283z00_5,
		BgL_typez00_bglt BgL_type1284z00_6, BgL_valuez00_bglt BgL_value1285z00_7,
		obj_t BgL_access1286z00_8, obj_t BgL_fastzd2alpha1287zd2_9,
		obj_t BgL_removable1288z00_10, long BgL_occurrence1289z00_11,
		long BgL_occurrencew1290z00_12, bool_t BgL_userzf31291zf3_13)
	{
		{	/* Ast/var.sch 283 */
			{	/* Ast/var.sch 283 */
				BgL_variablez00_bglt BgL_new1186z00_3772;

				{	/* Ast/var.sch 283 */
					BgL_variablez00_bglt BgL_new1185z00_3773;

					BgL_new1185z00_3773 =
						((BgL_variablez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_variablez00_bgl))));
					{	/* Ast/var.sch 283 */
						long BgL_arg1306z00_3774;

						BgL_arg1306z00_3774 = BGL_CLASS_NUM(BGl_variablez00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1185z00_3773), BgL_arg1306z00_3774);
					}
					BgL_new1186z00_3772 = BgL_new1185z00_3773;
				}
				((((BgL_variablez00_bglt) COBJECT(BgL_new1186z00_3772))->BgL_idz00) =
					((obj_t) BgL_id1282z00_4), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(BgL_new1186z00_3772))->BgL_namez00) =
					((obj_t) BgL_name1283z00_5), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(BgL_new1186z00_3772))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1284z00_6), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(BgL_new1186z00_3772))->BgL_valuez00) =
					((BgL_valuez00_bglt) BgL_value1285z00_7), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(BgL_new1186z00_3772))->
						BgL_accessz00) = ((obj_t) BgL_access1286z00_8), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(BgL_new1186z00_3772))->
						BgL_fastzd2alphazd2) =
					((obj_t) BgL_fastzd2alpha1287zd2_9), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(BgL_new1186z00_3772))->
						BgL_removablez00) = ((obj_t) BgL_removable1288z00_10), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(BgL_new1186z00_3772))->
						BgL_occurrencez00) = ((long) BgL_occurrence1289z00_11), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(BgL_new1186z00_3772))->
						BgL_occurrencewz00) = ((long) BgL_occurrencew1290z00_12), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(BgL_new1186z00_3772))->
						BgL_userzf3zf3) = ((bool_t) BgL_userzf31291zf3_13), BUNSPEC);
				return BgL_new1186z00_3772;
			}
		}

	}



/* &make-variable */
	BgL_variablez00_bglt BGl_z62makezd2variablezb0zzast_varz00(obj_t
		BgL_envz00_2475, obj_t BgL_id1282z00_2476, obj_t BgL_name1283z00_2477,
		obj_t BgL_type1284z00_2478, obj_t BgL_value1285z00_2479,
		obj_t BgL_access1286z00_2480, obj_t BgL_fastzd2alpha1287zd2_2481,
		obj_t BgL_removable1288z00_2482, obj_t BgL_occurrence1289z00_2483,
		obj_t BgL_occurrencew1290z00_2484, obj_t BgL_userzf31291zf3_2485)
	{
		{	/* Ast/var.sch 283 */
			return
				BGl_makezd2variablezd2zzast_varz00(BgL_id1282z00_2476,
				BgL_name1283z00_2477, ((BgL_typez00_bglt) BgL_type1284z00_2478),
				((BgL_valuez00_bglt) BgL_value1285z00_2479), BgL_access1286z00_2480,
				BgL_fastzd2alpha1287zd2_2481, BgL_removable1288z00_2482,
				(long) CINT(BgL_occurrence1289z00_2483),
				(long) CINT(BgL_occurrencew1290z00_2484),
				CBOOL(BgL_userzf31291zf3_2485));
		}

	}



/* variable? */
	BGL_EXPORTED_DEF bool_t BGl_variablezf3zf3zzast_varz00(obj_t BgL_objz00_14)
	{
		{	/* Ast/var.sch 284 */
			{	/* Ast/var.sch 284 */
				obj_t BgL_classz00_3775;

				BgL_classz00_3775 = BGl_variablez00zzast_varz00;
				if (BGL_OBJECTP(BgL_objz00_14))
					{	/* Ast/var.sch 284 */
						BgL_objectz00_bglt BgL_arg1807z00_3776;

						BgL_arg1807z00_3776 = (BgL_objectz00_bglt) (BgL_objz00_14);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Ast/var.sch 284 */
								long BgL_idxz00_3777;

								BgL_idxz00_3777 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3776);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3777 + 1L)) == BgL_classz00_3775);
							}
						else
							{	/* Ast/var.sch 284 */
								bool_t BgL_res2163z00_3780;

								{	/* Ast/var.sch 284 */
									obj_t BgL_oclassz00_3781;

									{	/* Ast/var.sch 284 */
										obj_t BgL_arg1815z00_3782;
										long BgL_arg1816z00_3783;

										BgL_arg1815z00_3782 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Ast/var.sch 284 */
											long BgL_arg1817z00_3784;

											BgL_arg1817z00_3784 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3776);
											BgL_arg1816z00_3783 = (BgL_arg1817z00_3784 - OBJECT_TYPE);
										}
										BgL_oclassz00_3781 =
											VECTOR_REF(BgL_arg1815z00_3782, BgL_arg1816z00_3783);
									}
									{	/* Ast/var.sch 284 */
										bool_t BgL__ortest_1115z00_3785;

										BgL__ortest_1115z00_3785 =
											(BgL_classz00_3775 == BgL_oclassz00_3781);
										if (BgL__ortest_1115z00_3785)
											{	/* Ast/var.sch 284 */
												BgL_res2163z00_3780 = BgL__ortest_1115z00_3785;
											}
										else
											{	/* Ast/var.sch 284 */
												long BgL_odepthz00_3786;

												{	/* Ast/var.sch 284 */
													obj_t BgL_arg1804z00_3787;

													BgL_arg1804z00_3787 = (BgL_oclassz00_3781);
													BgL_odepthz00_3786 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3787);
												}
												if ((1L < BgL_odepthz00_3786))
													{	/* Ast/var.sch 284 */
														obj_t BgL_arg1802z00_3788;

														{	/* Ast/var.sch 284 */
															obj_t BgL_arg1803z00_3789;

															BgL_arg1803z00_3789 = (BgL_oclassz00_3781);
															BgL_arg1802z00_3788 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3789,
																1L);
														}
														BgL_res2163z00_3780 =
															(BgL_arg1802z00_3788 == BgL_classz00_3775);
													}
												else
													{	/* Ast/var.sch 284 */
														BgL_res2163z00_3780 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2163z00_3780;
							}
					}
				else
					{	/* Ast/var.sch 284 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &variable? */
	obj_t BGl_z62variablezf3z91zzast_varz00(obj_t BgL_envz00_2486,
		obj_t BgL_objz00_2487)
	{
		{	/* Ast/var.sch 284 */
			return BBOOL(BGl_variablezf3zf3zzast_varz00(BgL_objz00_2487));
		}

	}



/* variable-nil */
	BGL_EXPORTED_DEF BgL_variablez00_bglt BGl_variablezd2nilzd2zzast_varz00(void)
	{
		{	/* Ast/var.sch 285 */
			{	/* Ast/var.sch 285 */
				obj_t BgL_classz00_1655;

				BgL_classz00_1655 = BGl_variablez00zzast_varz00;
				{	/* Ast/var.sch 285 */
					obj_t BgL__ortest_1117z00_1656;

					BgL__ortest_1117z00_1656 = BGL_CLASS_NIL(BgL_classz00_1655);
					if (CBOOL(BgL__ortest_1117z00_1656))
						{	/* Ast/var.sch 285 */
							return ((BgL_variablez00_bglt) BgL__ortest_1117z00_1656);
						}
					else
						{	/* Ast/var.sch 285 */
							return
								((BgL_variablez00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1655));
						}
				}
			}
		}

	}



/* &variable-nil */
	BgL_variablez00_bglt BGl_z62variablezd2nilzb0zzast_varz00(obj_t
		BgL_envz00_2488)
	{
		{	/* Ast/var.sch 285 */
			return BGl_variablezd2nilzd2zzast_varz00();
		}

	}



/* variable-user? */
	BGL_EXPORTED_DEF bool_t
		BGl_variablezd2userzf3z21zzast_varz00(BgL_variablez00_bglt BgL_oz00_15)
	{
		{	/* Ast/var.sch 286 */
			return (((BgL_variablez00_bglt) COBJECT(BgL_oz00_15))->BgL_userzf3zf3);
		}

	}



/* &variable-user? */
	obj_t BGl_z62variablezd2userzf3z43zzast_varz00(obj_t BgL_envz00_2489,
		obj_t BgL_oz00_2490)
	{
		{	/* Ast/var.sch 286 */
			return
				BBOOL(BGl_variablezd2userzf3z21zzast_varz00(
					((BgL_variablez00_bglt) BgL_oz00_2490)));
		}

	}



/* variable-user?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_variablezd2userzf3zd2setz12ze1zzast_varz00(BgL_variablez00_bglt
		BgL_oz00_16, bool_t BgL_vz00_17)
	{
		{	/* Ast/var.sch 287 */
			return
				((((BgL_variablez00_bglt) COBJECT(BgL_oz00_16))->BgL_userzf3zf3) =
				((bool_t) BgL_vz00_17), BUNSPEC);
		}

	}



/* &variable-user?-set! */
	obj_t BGl_z62variablezd2userzf3zd2setz12z83zzast_varz00(obj_t BgL_envz00_2491,
		obj_t BgL_oz00_2492, obj_t BgL_vz00_2493)
	{
		{	/* Ast/var.sch 287 */
			return
				BGl_variablezd2userzf3zd2setz12ze1zzast_varz00(
				((BgL_variablez00_bglt) BgL_oz00_2492), CBOOL(BgL_vz00_2493));
		}

	}



/* variable-occurrencew */
	BGL_EXPORTED_DEF long
		BGl_variablezd2occurrencewzd2zzast_varz00(BgL_variablez00_bglt BgL_oz00_18)
	{
		{	/* Ast/var.sch 288 */
			return
				(((BgL_variablez00_bglt) COBJECT(BgL_oz00_18))->BgL_occurrencewz00);
		}

	}



/* &variable-occurrencew */
	obj_t BGl_z62variablezd2occurrencewzb0zzast_varz00(obj_t BgL_envz00_2494,
		obj_t BgL_oz00_2495)
	{
		{	/* Ast/var.sch 288 */
			return
				BINT(BGl_variablezd2occurrencewzd2zzast_varz00(
					((BgL_variablez00_bglt) BgL_oz00_2495)));
		}

	}



/* variable-occurrencew-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_variablezd2occurrencewzd2setz12z12zzast_varz00(BgL_variablez00_bglt
		BgL_oz00_19, long BgL_vz00_20)
	{
		{	/* Ast/var.sch 289 */
			return
				((((BgL_variablez00_bglt) COBJECT(BgL_oz00_19))->BgL_occurrencewz00) =
				((long) BgL_vz00_20), BUNSPEC);
		}

	}



/* &variable-occurrencew-set! */
	obj_t BGl_z62variablezd2occurrencewzd2setz12z70zzast_varz00(obj_t
		BgL_envz00_2496, obj_t BgL_oz00_2497, obj_t BgL_vz00_2498)
	{
		{	/* Ast/var.sch 289 */
			return
				BGl_variablezd2occurrencewzd2setz12z12zzast_varz00(
				((BgL_variablez00_bglt) BgL_oz00_2497), (long) CINT(BgL_vz00_2498));
		}

	}



/* variable-occurrence */
	BGL_EXPORTED_DEF long
		BGl_variablezd2occurrencezd2zzast_varz00(BgL_variablez00_bglt BgL_oz00_21)
	{
		{	/* Ast/var.sch 290 */
			return (((BgL_variablez00_bglt) COBJECT(BgL_oz00_21))->BgL_occurrencez00);
		}

	}



/* &variable-occurrence */
	obj_t BGl_z62variablezd2occurrencezb0zzast_varz00(obj_t BgL_envz00_2499,
		obj_t BgL_oz00_2500)
	{
		{	/* Ast/var.sch 290 */
			return
				BINT(BGl_variablezd2occurrencezd2zzast_varz00(
					((BgL_variablez00_bglt) BgL_oz00_2500)));
		}

	}



/* variable-occurrence-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_variablezd2occurrencezd2setz12z12zzast_varz00(BgL_variablez00_bglt
		BgL_oz00_22, long BgL_vz00_23)
	{
		{	/* Ast/var.sch 291 */
			return
				((((BgL_variablez00_bglt) COBJECT(BgL_oz00_22))->BgL_occurrencez00) =
				((long) BgL_vz00_23), BUNSPEC);
		}

	}



/* &variable-occurrence-set! */
	obj_t BGl_z62variablezd2occurrencezd2setz12z70zzast_varz00(obj_t
		BgL_envz00_2501, obj_t BgL_oz00_2502, obj_t BgL_vz00_2503)
	{
		{	/* Ast/var.sch 291 */
			return
				BGl_variablezd2occurrencezd2setz12z12zzast_varz00(
				((BgL_variablez00_bglt) BgL_oz00_2502), (long) CINT(BgL_vz00_2503));
		}

	}



/* variable-removable */
	BGL_EXPORTED_DEF obj_t
		BGl_variablezd2removablezd2zzast_varz00(BgL_variablez00_bglt BgL_oz00_24)
	{
		{	/* Ast/var.sch 292 */
			return (((BgL_variablez00_bglt) COBJECT(BgL_oz00_24))->BgL_removablez00);
		}

	}



/* &variable-removable */
	obj_t BGl_z62variablezd2removablezb0zzast_varz00(obj_t BgL_envz00_2504,
		obj_t BgL_oz00_2505)
	{
		{	/* Ast/var.sch 292 */
			return
				BGl_variablezd2removablezd2zzast_varz00(
				((BgL_variablez00_bglt) BgL_oz00_2505));
		}

	}



/* variable-removable-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_variablezd2removablezd2setz12z12zzast_varz00(BgL_variablez00_bglt
		BgL_oz00_25, obj_t BgL_vz00_26)
	{
		{	/* Ast/var.sch 293 */
			return
				((((BgL_variablez00_bglt) COBJECT(BgL_oz00_25))->BgL_removablez00) =
				((obj_t) BgL_vz00_26), BUNSPEC);
		}

	}



/* &variable-removable-set! */
	obj_t BGl_z62variablezd2removablezd2setz12z70zzast_varz00(obj_t
		BgL_envz00_2506, obj_t BgL_oz00_2507, obj_t BgL_vz00_2508)
	{
		{	/* Ast/var.sch 293 */
			return
				BGl_variablezd2removablezd2setz12z12zzast_varz00(
				((BgL_variablez00_bglt) BgL_oz00_2507), BgL_vz00_2508);
		}

	}



/* variable-fast-alpha */
	BGL_EXPORTED_DEF obj_t
		BGl_variablezd2fastzd2alphaz00zzast_varz00(BgL_variablez00_bglt BgL_oz00_27)
	{
		{	/* Ast/var.sch 294 */
			return
				(((BgL_variablez00_bglt) COBJECT(BgL_oz00_27))->BgL_fastzd2alphazd2);
		}

	}



/* &variable-fast-alpha */
	obj_t BGl_z62variablezd2fastzd2alphaz62zzast_varz00(obj_t BgL_envz00_2509,
		obj_t BgL_oz00_2510)
	{
		{	/* Ast/var.sch 294 */
			return
				BGl_variablezd2fastzd2alphaz00zzast_varz00(
				((BgL_variablez00_bglt) BgL_oz00_2510));
		}

	}



/* variable-fast-alpha-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_variablezd2fastzd2alphazd2setz12zc0zzast_varz00(BgL_variablez00_bglt
		BgL_oz00_28, obj_t BgL_vz00_29)
	{
		{	/* Ast/var.sch 295 */
			return
				((((BgL_variablez00_bglt) COBJECT(BgL_oz00_28))->BgL_fastzd2alphazd2) =
				((obj_t) BgL_vz00_29), BUNSPEC);
		}

	}



/* &variable-fast-alpha-set! */
	obj_t BGl_z62variablezd2fastzd2alphazd2setz12za2zzast_varz00(obj_t
		BgL_envz00_2511, obj_t BgL_oz00_2512, obj_t BgL_vz00_2513)
	{
		{	/* Ast/var.sch 295 */
			return
				BGl_variablezd2fastzd2alphazd2setz12zc0zzast_varz00(
				((BgL_variablez00_bglt) BgL_oz00_2512), BgL_vz00_2513);
		}

	}



/* variable-access */
	BGL_EXPORTED_DEF obj_t
		BGl_variablezd2accesszd2zzast_varz00(BgL_variablez00_bglt BgL_oz00_30)
	{
		{	/* Ast/var.sch 296 */
			return (((BgL_variablez00_bglt) COBJECT(BgL_oz00_30))->BgL_accessz00);
		}

	}



/* &variable-access */
	obj_t BGl_z62variablezd2accesszb0zzast_varz00(obj_t BgL_envz00_2514,
		obj_t BgL_oz00_2515)
	{
		{	/* Ast/var.sch 296 */
			return
				BGl_variablezd2accesszd2zzast_varz00(
				((BgL_variablez00_bglt) BgL_oz00_2515));
		}

	}



/* variable-access-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_variablezd2accesszd2setz12z12zzast_varz00(BgL_variablez00_bglt
		BgL_oz00_31, obj_t BgL_vz00_32)
	{
		{	/* Ast/var.sch 297 */
			return
				((((BgL_variablez00_bglt) COBJECT(BgL_oz00_31))->BgL_accessz00) =
				((obj_t) BgL_vz00_32), BUNSPEC);
		}

	}



/* &variable-access-set! */
	obj_t BGl_z62variablezd2accesszd2setz12z70zzast_varz00(obj_t BgL_envz00_2516,
		obj_t BgL_oz00_2517, obj_t BgL_vz00_2518)
	{
		{	/* Ast/var.sch 297 */
			return
				BGl_variablezd2accesszd2setz12z12zzast_varz00(
				((BgL_variablez00_bglt) BgL_oz00_2517), BgL_vz00_2518);
		}

	}



/* variable-value */
	BGL_EXPORTED_DEF BgL_valuez00_bglt
		BGl_variablezd2valuezd2zzast_varz00(BgL_variablez00_bglt BgL_oz00_33)
	{
		{	/* Ast/var.sch 298 */
			return (((BgL_variablez00_bglt) COBJECT(BgL_oz00_33))->BgL_valuez00);
		}

	}



/* &variable-value */
	BgL_valuez00_bglt BGl_z62variablezd2valuezb0zzast_varz00(obj_t
		BgL_envz00_2519, obj_t BgL_oz00_2520)
	{
		{	/* Ast/var.sch 298 */
			return
				BGl_variablezd2valuezd2zzast_varz00(
				((BgL_variablez00_bglt) BgL_oz00_2520));
		}

	}



/* variable-value-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_variablezd2valuezd2setz12z12zzast_varz00(BgL_variablez00_bglt
		BgL_oz00_34, BgL_valuez00_bglt BgL_vz00_35)
	{
		{	/* Ast/var.sch 299 */
			return
				((((BgL_variablez00_bglt) COBJECT(BgL_oz00_34))->BgL_valuez00) =
				((BgL_valuez00_bglt) BgL_vz00_35), BUNSPEC);
		}

	}



/* &variable-value-set! */
	obj_t BGl_z62variablezd2valuezd2setz12z70zzast_varz00(obj_t BgL_envz00_2521,
		obj_t BgL_oz00_2522, obj_t BgL_vz00_2523)
	{
		{	/* Ast/var.sch 299 */
			return
				BGl_variablezd2valuezd2setz12z12zzast_varz00(
				((BgL_variablez00_bglt) BgL_oz00_2522),
				((BgL_valuez00_bglt) BgL_vz00_2523));
		}

	}



/* variable-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_variablezd2typezd2zzast_varz00(BgL_variablez00_bglt BgL_oz00_36)
	{
		{	/* Ast/var.sch 300 */
			return (((BgL_variablez00_bglt) COBJECT(BgL_oz00_36))->BgL_typez00);
		}

	}



/* &variable-type */
	BgL_typez00_bglt BGl_z62variablezd2typezb0zzast_varz00(obj_t BgL_envz00_2524,
		obj_t BgL_oz00_2525)
	{
		{	/* Ast/var.sch 300 */
			return
				BGl_variablezd2typezd2zzast_varz00(
				((BgL_variablez00_bglt) BgL_oz00_2525));
		}

	}



/* variable-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_variablezd2typezd2setz12z12zzast_varz00(BgL_variablez00_bglt
		BgL_oz00_37, BgL_typez00_bglt BgL_vz00_38)
	{
		{	/* Ast/var.sch 301 */
			return
				((((BgL_variablez00_bglt) COBJECT(BgL_oz00_37))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_38), BUNSPEC);
		}

	}



/* &variable-type-set! */
	obj_t BGl_z62variablezd2typezd2setz12z70zzast_varz00(obj_t BgL_envz00_2526,
		obj_t BgL_oz00_2527, obj_t BgL_vz00_2528)
	{
		{	/* Ast/var.sch 301 */
			return
				BGl_variablezd2typezd2setz12z12zzast_varz00(
				((BgL_variablez00_bglt) BgL_oz00_2527),
				((BgL_typez00_bglt) BgL_vz00_2528));
		}

	}



/* variable-name */
	BGL_EXPORTED_DEF obj_t BGl_variablezd2namezd2zzast_varz00(BgL_variablez00_bglt
		BgL_oz00_39)
	{
		{	/* Ast/var.sch 302 */
			return (((BgL_variablez00_bglt) COBJECT(BgL_oz00_39))->BgL_namez00);
		}

	}



/* &variable-name */
	obj_t BGl_z62variablezd2namezb0zzast_varz00(obj_t BgL_envz00_2529,
		obj_t BgL_oz00_2530)
	{
		{	/* Ast/var.sch 302 */
			return
				BGl_variablezd2namezd2zzast_varz00(
				((BgL_variablez00_bglt) BgL_oz00_2530));
		}

	}



/* variable-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_variablezd2namezd2setz12z12zzast_varz00(BgL_variablez00_bglt
		BgL_oz00_40, obj_t BgL_vz00_41)
	{
		{	/* Ast/var.sch 303 */
			return
				((((BgL_variablez00_bglt) COBJECT(BgL_oz00_40))->BgL_namez00) =
				((obj_t) BgL_vz00_41), BUNSPEC);
		}

	}



/* &variable-name-set! */
	obj_t BGl_z62variablezd2namezd2setz12z70zzast_varz00(obj_t BgL_envz00_2531,
		obj_t BgL_oz00_2532, obj_t BgL_vz00_2533)
	{
		{	/* Ast/var.sch 303 */
			return
				BGl_variablezd2namezd2setz12z12zzast_varz00(
				((BgL_variablez00_bglt) BgL_oz00_2532), BgL_vz00_2533);
		}

	}



/* variable-id */
	BGL_EXPORTED_DEF obj_t BGl_variablezd2idzd2zzast_varz00(BgL_variablez00_bglt
		BgL_oz00_42)
	{
		{	/* Ast/var.sch 304 */
			return (((BgL_variablez00_bglt) COBJECT(BgL_oz00_42))->BgL_idz00);
		}

	}



/* &variable-id */
	obj_t BGl_z62variablezd2idzb0zzast_varz00(obj_t BgL_envz00_2534,
		obj_t BgL_oz00_2535)
	{
		{	/* Ast/var.sch 304 */
			return
				BGl_variablezd2idzd2zzast_varz00(
				((BgL_variablez00_bglt) BgL_oz00_2535));
		}

	}



/* make-global */
	BGL_EXPORTED_DEF BgL_globalz00_bglt BGl_makezd2globalzd2zzast_varz00(obj_t
		BgL_id1261z00_45, obj_t BgL_name1262z00_46,
		BgL_typez00_bglt BgL_type1263z00_47, BgL_valuez00_bglt BgL_value1264z00_48,
		obj_t BgL_access1265z00_49, obj_t BgL_fastzd2alpha1266zd2_50,
		obj_t BgL_removable1267z00_51, long BgL_occurrence1268z00_52,
		long BgL_occurrencew1269z00_53, bool_t BgL_userzf31270zf3_54,
		obj_t BgL_module1271z00_55, obj_t BgL_import1272z00_56,
		bool_t BgL_evaluablezf31273zf3_57, bool_t BgL_evalzf31274zf3_58,
		obj_t BgL_library1275z00_59, obj_t BgL_pragma1276z00_60,
		obj_t BgL_src1277z00_61, obj_t BgL_jvmzd2typezd2name1278z00_62,
		obj_t BgL_init1279z00_63, obj_t BgL_alias1280z00_64)
	{
		{	/* Ast/var.sch 308 */
			{	/* Ast/var.sch 308 */
				BgL_globalz00_bglt BgL_new1189z00_3790;

				{	/* Ast/var.sch 308 */
					BgL_globalz00_bglt BgL_new1187z00_3791;

					BgL_new1187z00_3791 =
						((BgL_globalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_globalz00_bgl))));
					{	/* Ast/var.sch 308 */
						long BgL_arg1307z00_3792;

						BgL_arg1307z00_3792 = BGL_CLASS_NUM(BGl_globalz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1187z00_3791), BgL_arg1307z00_3792);
					}
					{	/* Ast/var.sch 308 */
						BgL_objectz00_bglt BgL_tmpz00_4418;

						BgL_tmpz00_4418 = ((BgL_objectz00_bglt) BgL_new1187z00_3791);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4418, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1187z00_3791);
					BgL_new1189z00_3790 = BgL_new1187z00_3791;
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_new1189z00_3790)))->BgL_idz00) =
					((obj_t) BgL_id1261z00_45), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1189z00_3790)))->BgL_namez00) =
					((obj_t) BgL_name1262z00_46), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1189z00_3790)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1263z00_47), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1189z00_3790)))->BgL_valuez00) =
					((BgL_valuez00_bglt) BgL_value1264z00_48), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1189z00_3790)))->BgL_accessz00) =
					((obj_t) BgL_access1265z00_49), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1189z00_3790)))->BgL_fastzd2alphazd2) =
					((obj_t) BgL_fastzd2alpha1266zd2_50), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1189z00_3790)))->BgL_removablez00) =
					((obj_t) BgL_removable1267z00_51), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1189z00_3790)))->BgL_occurrencez00) =
					((long) BgL_occurrence1268z00_52), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1189z00_3790)))->BgL_occurrencewz00) =
					((long) BgL_occurrencew1269z00_53), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1189z00_3790)))->BgL_userzf3zf3) =
					((bool_t) BgL_userzf31270zf3_54), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(BgL_new1189z00_3790))->BgL_modulez00) =
					((obj_t) BgL_module1271z00_55), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(BgL_new1189z00_3790))->BgL_importz00) =
					((obj_t) BgL_import1272z00_56), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(BgL_new1189z00_3790))->
						BgL_evaluablezf3zf3) =
					((bool_t) BgL_evaluablezf31273zf3_57), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(BgL_new1189z00_3790))->BgL_evalzf3zf3) =
					((bool_t) BgL_evalzf31274zf3_58), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(BgL_new1189z00_3790))->BgL_libraryz00) =
					((obj_t) BgL_library1275z00_59), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(BgL_new1189z00_3790))->BgL_pragmaz00) =
					((obj_t) BgL_pragma1276z00_60), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(BgL_new1189z00_3790))->BgL_srcz00) =
					((obj_t) BgL_src1277z00_61), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(BgL_new1189z00_3790))->
						BgL_jvmzd2typezd2namez00) =
					((obj_t) BgL_jvmzd2typezd2name1278z00_62), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(BgL_new1189z00_3790))->BgL_initz00) =
					((obj_t) BgL_init1279z00_63), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(BgL_new1189z00_3790))->BgL_aliasz00) =
					((obj_t) BgL_alias1280z00_64), BUNSPEC);
				return BgL_new1189z00_3790;
			}
		}

	}



/* &make-global */
	BgL_globalz00_bglt BGl_z62makezd2globalzb0zzast_varz00(obj_t BgL_envz00_2536,
		obj_t BgL_id1261z00_2537, obj_t BgL_name1262z00_2538,
		obj_t BgL_type1263z00_2539, obj_t BgL_value1264z00_2540,
		obj_t BgL_access1265z00_2541, obj_t BgL_fastzd2alpha1266zd2_2542,
		obj_t BgL_removable1267z00_2543, obj_t BgL_occurrence1268z00_2544,
		obj_t BgL_occurrencew1269z00_2545, obj_t BgL_userzf31270zf3_2546,
		obj_t BgL_module1271z00_2547, obj_t BgL_import1272z00_2548,
		obj_t BgL_evaluablezf31273zf3_2549, obj_t BgL_evalzf31274zf3_2550,
		obj_t BgL_library1275z00_2551, obj_t BgL_pragma1276z00_2552,
		obj_t BgL_src1277z00_2553, obj_t BgL_jvmzd2typezd2name1278z00_2554,
		obj_t BgL_init1279z00_2555, obj_t BgL_alias1280z00_2556)
	{
		{	/* Ast/var.sch 308 */
			return
				BGl_makezd2globalzd2zzast_varz00(BgL_id1261z00_2537,
				BgL_name1262z00_2538, ((BgL_typez00_bglt) BgL_type1263z00_2539),
				((BgL_valuez00_bglt) BgL_value1264z00_2540), BgL_access1265z00_2541,
				BgL_fastzd2alpha1266zd2_2542, BgL_removable1267z00_2543,
				(long) CINT(BgL_occurrence1268z00_2544),
				(long) CINT(BgL_occurrencew1269z00_2545),
				CBOOL(BgL_userzf31270zf3_2546), BgL_module1271z00_2547,
				BgL_import1272z00_2548, CBOOL(BgL_evaluablezf31273zf3_2549),
				CBOOL(BgL_evalzf31274zf3_2550), BgL_library1275z00_2551,
				BgL_pragma1276z00_2552, BgL_src1277z00_2553,
				BgL_jvmzd2typezd2name1278z00_2554, BgL_init1279z00_2555,
				BgL_alias1280z00_2556);
		}

	}



/* global? */
	BGL_EXPORTED_DEF bool_t BGl_globalzf3zf3zzast_varz00(obj_t BgL_objz00_65)
	{
		{	/* Ast/var.sch 309 */
			{	/* Ast/var.sch 309 */
				obj_t BgL_classz00_3793;

				BgL_classz00_3793 = BGl_globalz00zzast_varz00;
				if (BGL_OBJECTP(BgL_objz00_65))
					{	/* Ast/var.sch 309 */
						BgL_objectz00_bglt BgL_arg1807z00_3794;

						BgL_arg1807z00_3794 = (BgL_objectz00_bglt) (BgL_objz00_65);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Ast/var.sch 309 */
								long BgL_idxz00_3795;

								BgL_idxz00_3795 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3794);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3795 + 2L)) == BgL_classz00_3793);
							}
						else
							{	/* Ast/var.sch 309 */
								bool_t BgL_res2164z00_3798;

								{	/* Ast/var.sch 309 */
									obj_t BgL_oclassz00_3799;

									{	/* Ast/var.sch 309 */
										obj_t BgL_arg1815z00_3800;
										long BgL_arg1816z00_3801;

										BgL_arg1815z00_3800 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Ast/var.sch 309 */
											long BgL_arg1817z00_3802;

											BgL_arg1817z00_3802 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3794);
											BgL_arg1816z00_3801 = (BgL_arg1817z00_3802 - OBJECT_TYPE);
										}
										BgL_oclassz00_3799 =
											VECTOR_REF(BgL_arg1815z00_3800, BgL_arg1816z00_3801);
									}
									{	/* Ast/var.sch 309 */
										bool_t BgL__ortest_1115z00_3803;

										BgL__ortest_1115z00_3803 =
											(BgL_classz00_3793 == BgL_oclassz00_3799);
										if (BgL__ortest_1115z00_3803)
											{	/* Ast/var.sch 309 */
												BgL_res2164z00_3798 = BgL__ortest_1115z00_3803;
											}
										else
											{	/* Ast/var.sch 309 */
												long BgL_odepthz00_3804;

												{	/* Ast/var.sch 309 */
													obj_t BgL_arg1804z00_3805;

													BgL_arg1804z00_3805 = (BgL_oclassz00_3799);
													BgL_odepthz00_3804 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3805);
												}
												if ((2L < BgL_odepthz00_3804))
													{	/* Ast/var.sch 309 */
														obj_t BgL_arg1802z00_3806;

														{	/* Ast/var.sch 309 */
															obj_t BgL_arg1803z00_3807;

															BgL_arg1803z00_3807 = (BgL_oclassz00_3799);
															BgL_arg1802z00_3806 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3807,
																2L);
														}
														BgL_res2164z00_3798 =
															(BgL_arg1802z00_3806 == BgL_classz00_3793);
													}
												else
													{	/* Ast/var.sch 309 */
														BgL_res2164z00_3798 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2164z00_3798;
							}
					}
				else
					{	/* Ast/var.sch 309 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &global? */
	obj_t BGl_z62globalzf3z91zzast_varz00(obj_t BgL_envz00_2557,
		obj_t BgL_objz00_2558)
	{
		{	/* Ast/var.sch 309 */
			return BBOOL(BGl_globalzf3zf3zzast_varz00(BgL_objz00_2558));
		}

	}



/* global-nil */
	BGL_EXPORTED_DEF BgL_globalz00_bglt BGl_globalzd2nilzd2zzast_varz00(void)
	{
		{	/* Ast/var.sch 310 */
			{	/* Ast/var.sch 310 */
				obj_t BgL_classz00_1698;

				BgL_classz00_1698 = BGl_globalz00zzast_varz00;
				{	/* Ast/var.sch 310 */
					obj_t BgL__ortest_1117z00_1699;

					BgL__ortest_1117z00_1699 = BGL_CLASS_NIL(BgL_classz00_1698);
					if (CBOOL(BgL__ortest_1117z00_1699))
						{	/* Ast/var.sch 310 */
							return ((BgL_globalz00_bglt) BgL__ortest_1117z00_1699);
						}
					else
						{	/* Ast/var.sch 310 */
							return
								((BgL_globalz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1698));
						}
				}
			}
		}

	}



/* &global-nil */
	BgL_globalz00_bglt BGl_z62globalzd2nilzb0zzast_varz00(obj_t BgL_envz00_2559)
	{
		{	/* Ast/var.sch 310 */
			return BGl_globalzd2nilzd2zzast_varz00();
		}

	}



/* global-alias */
	BGL_EXPORTED_DEF obj_t BGl_globalzd2aliaszd2zzast_varz00(BgL_globalz00_bglt
		BgL_oz00_66)
	{
		{	/* Ast/var.sch 311 */
			return (((BgL_globalz00_bglt) COBJECT(BgL_oz00_66))->BgL_aliasz00);
		}

	}



/* &global-alias */
	obj_t BGl_z62globalzd2aliaszb0zzast_varz00(obj_t BgL_envz00_2560,
		obj_t BgL_oz00_2561)
	{
		{	/* Ast/var.sch 311 */
			return
				BGl_globalzd2aliaszd2zzast_varz00(((BgL_globalz00_bglt) BgL_oz00_2561));
		}

	}



/* global-alias-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzd2aliaszd2setz12z12zzast_varz00(BgL_globalz00_bglt BgL_oz00_67,
		obj_t BgL_vz00_68)
	{
		{	/* Ast/var.sch 312 */
			return
				((((BgL_globalz00_bglt) COBJECT(BgL_oz00_67))->BgL_aliasz00) =
				((obj_t) BgL_vz00_68), BUNSPEC);
		}

	}



/* &global-alias-set! */
	obj_t BGl_z62globalzd2aliaszd2setz12z70zzast_varz00(obj_t BgL_envz00_2562,
		obj_t BgL_oz00_2563, obj_t BgL_vz00_2564)
	{
		{	/* Ast/var.sch 312 */
			return
				BGl_globalzd2aliaszd2setz12z12zzast_varz00(
				((BgL_globalz00_bglt) BgL_oz00_2563), BgL_vz00_2564);
		}

	}



/* global-init */
	BGL_EXPORTED_DEF obj_t BGl_globalzd2initzd2zzast_varz00(BgL_globalz00_bglt
		BgL_oz00_69)
	{
		{	/* Ast/var.sch 313 */
			return (((BgL_globalz00_bglt) COBJECT(BgL_oz00_69))->BgL_initz00);
		}

	}



/* &global-init */
	obj_t BGl_z62globalzd2initzb0zzast_varz00(obj_t BgL_envz00_2565,
		obj_t BgL_oz00_2566)
	{
		{	/* Ast/var.sch 313 */
			return
				BGl_globalzd2initzd2zzast_varz00(((BgL_globalz00_bglt) BgL_oz00_2566));
		}

	}



/* global-init-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzd2initzd2setz12z12zzast_varz00(BgL_globalz00_bglt BgL_oz00_70,
		obj_t BgL_vz00_71)
	{
		{	/* Ast/var.sch 314 */
			return
				((((BgL_globalz00_bglt) COBJECT(BgL_oz00_70))->BgL_initz00) =
				((obj_t) BgL_vz00_71), BUNSPEC);
		}

	}



/* &global-init-set! */
	obj_t BGl_z62globalzd2initzd2setz12z70zzast_varz00(obj_t BgL_envz00_2567,
		obj_t BgL_oz00_2568, obj_t BgL_vz00_2569)
	{
		{	/* Ast/var.sch 314 */
			return
				BGl_globalzd2initzd2setz12z12zzast_varz00(
				((BgL_globalz00_bglt) BgL_oz00_2568), BgL_vz00_2569);
		}

	}



/* global-jvm-type-name */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzd2jvmzd2typezd2namezd2zzast_varz00(BgL_globalz00_bglt
		BgL_oz00_72)
	{
		{	/* Ast/var.sch 315 */
			return
				(((BgL_globalz00_bglt) COBJECT(BgL_oz00_72))->BgL_jvmzd2typezd2namez00);
		}

	}



/* &global-jvm-type-name */
	obj_t BGl_z62globalzd2jvmzd2typezd2namezb0zzast_varz00(obj_t BgL_envz00_2570,
		obj_t BgL_oz00_2571)
	{
		{	/* Ast/var.sch 315 */
			return
				BGl_globalzd2jvmzd2typezd2namezd2zzast_varz00(
				((BgL_globalz00_bglt) BgL_oz00_2571));
		}

	}



/* global-jvm-type-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzd2jvmzd2typezd2namezd2setz12z12zzast_varz00(BgL_globalz00_bglt
		BgL_oz00_73, obj_t BgL_vz00_74)
	{
		{	/* Ast/var.sch 316 */
			return
				((((BgL_globalz00_bglt) COBJECT(BgL_oz00_73))->
					BgL_jvmzd2typezd2namez00) = ((obj_t) BgL_vz00_74), BUNSPEC);
		}

	}



/* &global-jvm-type-name-set! */
	obj_t BGl_z62globalzd2jvmzd2typezd2namezd2setz12z70zzast_varz00(obj_t
		BgL_envz00_2572, obj_t BgL_oz00_2573, obj_t BgL_vz00_2574)
	{
		{	/* Ast/var.sch 316 */
			return
				BGl_globalzd2jvmzd2typezd2namezd2setz12z12zzast_varz00(
				((BgL_globalz00_bglt) BgL_oz00_2573), BgL_vz00_2574);
		}

	}



/* global-src */
	BGL_EXPORTED_DEF obj_t BGl_globalzd2srczd2zzast_varz00(BgL_globalz00_bglt
		BgL_oz00_75)
	{
		{	/* Ast/var.sch 317 */
			return (((BgL_globalz00_bglt) COBJECT(BgL_oz00_75))->BgL_srcz00);
		}

	}



/* &global-src */
	obj_t BGl_z62globalzd2srczb0zzast_varz00(obj_t BgL_envz00_2575,
		obj_t BgL_oz00_2576)
	{
		{	/* Ast/var.sch 317 */
			return
				BGl_globalzd2srczd2zzast_varz00(((BgL_globalz00_bglt) BgL_oz00_2576));
		}

	}



/* global-src-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzd2srczd2setz12z12zzast_varz00(BgL_globalz00_bglt BgL_oz00_76,
		obj_t BgL_vz00_77)
	{
		{	/* Ast/var.sch 318 */
			return
				((((BgL_globalz00_bglt) COBJECT(BgL_oz00_76))->BgL_srcz00) =
				((obj_t) BgL_vz00_77), BUNSPEC);
		}

	}



/* &global-src-set! */
	obj_t BGl_z62globalzd2srczd2setz12z70zzast_varz00(obj_t BgL_envz00_2577,
		obj_t BgL_oz00_2578, obj_t BgL_vz00_2579)
	{
		{	/* Ast/var.sch 318 */
			return
				BGl_globalzd2srczd2setz12z12zzast_varz00(
				((BgL_globalz00_bglt) BgL_oz00_2578), BgL_vz00_2579);
		}

	}



/* global-pragma */
	BGL_EXPORTED_DEF obj_t BGl_globalzd2pragmazd2zzast_varz00(BgL_globalz00_bglt
		BgL_oz00_78)
	{
		{	/* Ast/var.sch 319 */
			return (((BgL_globalz00_bglt) COBJECT(BgL_oz00_78))->BgL_pragmaz00);
		}

	}



/* &global-pragma */
	obj_t BGl_z62globalzd2pragmazb0zzast_varz00(obj_t BgL_envz00_2580,
		obj_t BgL_oz00_2581)
	{
		{	/* Ast/var.sch 319 */
			return
				BGl_globalzd2pragmazd2zzast_varz00(
				((BgL_globalz00_bglt) BgL_oz00_2581));
		}

	}



/* global-pragma-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzd2pragmazd2setz12z12zzast_varz00(BgL_globalz00_bglt BgL_oz00_79,
		obj_t BgL_vz00_80)
	{
		{	/* Ast/var.sch 320 */
			return
				((((BgL_globalz00_bglt) COBJECT(BgL_oz00_79))->BgL_pragmaz00) =
				((obj_t) BgL_vz00_80), BUNSPEC);
		}

	}



/* &global-pragma-set! */
	obj_t BGl_z62globalzd2pragmazd2setz12z70zzast_varz00(obj_t BgL_envz00_2582,
		obj_t BgL_oz00_2583, obj_t BgL_vz00_2584)
	{
		{	/* Ast/var.sch 320 */
			return
				BGl_globalzd2pragmazd2setz12z12zzast_varz00(
				((BgL_globalz00_bglt) BgL_oz00_2583), BgL_vz00_2584);
		}

	}



/* global-library */
	BGL_EXPORTED_DEF obj_t BGl_globalzd2libraryzd2zzast_varz00(BgL_globalz00_bglt
		BgL_oz00_81)
	{
		{	/* Ast/var.sch 321 */
			return (((BgL_globalz00_bglt) COBJECT(BgL_oz00_81))->BgL_libraryz00);
		}

	}



/* &global-library */
	obj_t BGl_z62globalzd2libraryzb0zzast_varz00(obj_t BgL_envz00_2585,
		obj_t BgL_oz00_2586)
	{
		{	/* Ast/var.sch 321 */
			return
				BGl_globalzd2libraryzd2zzast_varz00(
				((BgL_globalz00_bglt) BgL_oz00_2586));
		}

	}



/* global-library-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzd2libraryzd2setz12z12zzast_varz00(BgL_globalz00_bglt BgL_oz00_82,
		obj_t BgL_vz00_83)
	{
		{	/* Ast/var.sch 322 */
			return
				((((BgL_globalz00_bglt) COBJECT(BgL_oz00_82))->BgL_libraryz00) =
				((obj_t) BgL_vz00_83), BUNSPEC);
		}

	}



/* &global-library-set! */
	obj_t BGl_z62globalzd2libraryzd2setz12z70zzast_varz00(obj_t BgL_envz00_2587,
		obj_t BgL_oz00_2588, obj_t BgL_vz00_2589)
	{
		{	/* Ast/var.sch 322 */
			return
				BGl_globalzd2libraryzd2setz12z12zzast_varz00(
				((BgL_globalz00_bglt) BgL_oz00_2588), BgL_vz00_2589);
		}

	}



/* global-eval? */
	BGL_EXPORTED_DEF bool_t BGl_globalzd2evalzf3z21zzast_varz00(BgL_globalz00_bglt
		BgL_oz00_84)
	{
		{	/* Ast/var.sch 323 */
			return (((BgL_globalz00_bglt) COBJECT(BgL_oz00_84))->BgL_evalzf3zf3);
		}

	}



/* &global-eval? */
	obj_t BGl_z62globalzd2evalzf3z43zzast_varz00(obj_t BgL_envz00_2590,
		obj_t BgL_oz00_2591)
	{
		{	/* Ast/var.sch 323 */
			return
				BBOOL(BGl_globalzd2evalzf3z21zzast_varz00(
					((BgL_globalz00_bglt) BgL_oz00_2591)));
		}

	}



/* global-eval?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzd2evalzf3zd2setz12ze1zzast_varz00(BgL_globalz00_bglt BgL_oz00_85,
		bool_t BgL_vz00_86)
	{
		{	/* Ast/var.sch 324 */
			return
				((((BgL_globalz00_bglt) COBJECT(BgL_oz00_85))->BgL_evalzf3zf3) =
				((bool_t) BgL_vz00_86), BUNSPEC);
		}

	}



/* &global-eval?-set! */
	obj_t BGl_z62globalzd2evalzf3zd2setz12z83zzast_varz00(obj_t BgL_envz00_2592,
		obj_t BgL_oz00_2593, obj_t BgL_vz00_2594)
	{
		{	/* Ast/var.sch 324 */
			return
				BGl_globalzd2evalzf3zd2setz12ze1zzast_varz00(
				((BgL_globalz00_bglt) BgL_oz00_2593), CBOOL(BgL_vz00_2594));
		}

	}



/* global-evaluable? */
	BGL_EXPORTED_DEF bool_t
		BGl_globalzd2evaluablezf3z21zzast_varz00(BgL_globalz00_bglt BgL_oz00_87)
	{
		{	/* Ast/var.sch 325 */
			return (((BgL_globalz00_bglt) COBJECT(BgL_oz00_87))->BgL_evaluablezf3zf3);
		}

	}



/* &global-evaluable? */
	obj_t BGl_z62globalzd2evaluablezf3z43zzast_varz00(obj_t BgL_envz00_2595,
		obj_t BgL_oz00_2596)
	{
		{	/* Ast/var.sch 325 */
			return
				BBOOL(BGl_globalzd2evaluablezf3z21zzast_varz00(
					((BgL_globalz00_bglt) BgL_oz00_2596)));
		}

	}



/* global-evaluable?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzd2evaluablezf3zd2setz12ze1zzast_varz00(BgL_globalz00_bglt
		BgL_oz00_88, bool_t BgL_vz00_89)
	{
		{	/* Ast/var.sch 326 */
			return
				((((BgL_globalz00_bglt) COBJECT(BgL_oz00_88))->BgL_evaluablezf3zf3) =
				((bool_t) BgL_vz00_89), BUNSPEC);
		}

	}



/* &global-evaluable?-set! */
	obj_t BGl_z62globalzd2evaluablezf3zd2setz12z83zzast_varz00(obj_t
		BgL_envz00_2597, obj_t BgL_oz00_2598, obj_t BgL_vz00_2599)
	{
		{	/* Ast/var.sch 326 */
			return
				BGl_globalzd2evaluablezf3zd2setz12ze1zzast_varz00(
				((BgL_globalz00_bglt) BgL_oz00_2598), CBOOL(BgL_vz00_2599));
		}

	}



/* global-import */
	BGL_EXPORTED_DEF obj_t BGl_globalzd2importzd2zzast_varz00(BgL_globalz00_bglt
		BgL_oz00_90)
	{
		{	/* Ast/var.sch 327 */
			return (((BgL_globalz00_bglt) COBJECT(BgL_oz00_90))->BgL_importz00);
		}

	}



/* &global-import */
	obj_t BGl_z62globalzd2importzb0zzast_varz00(obj_t BgL_envz00_2600,
		obj_t BgL_oz00_2601)
	{
		{	/* Ast/var.sch 327 */
			return
				BGl_globalzd2importzd2zzast_varz00(
				((BgL_globalz00_bglt) BgL_oz00_2601));
		}

	}



/* global-import-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzd2importzd2setz12z12zzast_varz00(BgL_globalz00_bglt BgL_oz00_91,
		obj_t BgL_vz00_92)
	{
		{	/* Ast/var.sch 328 */
			return
				((((BgL_globalz00_bglt) COBJECT(BgL_oz00_91))->BgL_importz00) =
				((obj_t) BgL_vz00_92), BUNSPEC);
		}

	}



/* &global-import-set! */
	obj_t BGl_z62globalzd2importzd2setz12z70zzast_varz00(obj_t BgL_envz00_2602,
		obj_t BgL_oz00_2603, obj_t BgL_vz00_2604)
	{
		{	/* Ast/var.sch 328 */
			return
				BGl_globalzd2importzd2setz12z12zzast_varz00(
				((BgL_globalz00_bglt) BgL_oz00_2603), BgL_vz00_2604);
		}

	}



/* global-module */
	BGL_EXPORTED_DEF obj_t BGl_globalzd2modulezd2zzast_varz00(BgL_globalz00_bglt
		BgL_oz00_93)
	{
		{	/* Ast/var.sch 329 */
			return (((BgL_globalz00_bglt) COBJECT(BgL_oz00_93))->BgL_modulez00);
		}

	}



/* &global-module */
	obj_t BGl_z62globalzd2modulezb0zzast_varz00(obj_t BgL_envz00_2605,
		obj_t BgL_oz00_2606)
	{
		{	/* Ast/var.sch 329 */
			return
				BGl_globalzd2modulezd2zzast_varz00(
				((BgL_globalz00_bglt) BgL_oz00_2606));
		}

	}



/* global-module-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzd2modulezd2setz12z12zzast_varz00(BgL_globalz00_bglt BgL_oz00_94,
		obj_t BgL_vz00_95)
	{
		{	/* Ast/var.sch 330 */
			return
				((((BgL_globalz00_bglt) COBJECT(BgL_oz00_94))->BgL_modulez00) =
				((obj_t) BgL_vz00_95), BUNSPEC);
		}

	}



/* &global-module-set! */
	obj_t BGl_z62globalzd2modulezd2setz12z70zzast_varz00(obj_t BgL_envz00_2607,
		obj_t BgL_oz00_2608, obj_t BgL_vz00_2609)
	{
		{	/* Ast/var.sch 330 */
			return
				BGl_globalzd2modulezd2setz12z12zzast_varz00(
				((BgL_globalz00_bglt) BgL_oz00_2608), BgL_vz00_2609);
		}

	}



/* global-user? */
	BGL_EXPORTED_DEF bool_t BGl_globalzd2userzf3z21zzast_varz00(BgL_globalz00_bglt
		BgL_oz00_96)
	{
		{	/* Ast/var.sch 331 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_96)))->BgL_userzf3zf3);
		}

	}



/* &global-user? */
	obj_t BGl_z62globalzd2userzf3z43zzast_varz00(obj_t BgL_envz00_2610,
		obj_t BgL_oz00_2611)
	{
		{	/* Ast/var.sch 331 */
			return
				BBOOL(BGl_globalzd2userzf3z21zzast_varz00(
					((BgL_globalz00_bglt) BgL_oz00_2611)));
		}

	}



/* global-user?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzd2userzf3zd2setz12ze1zzast_varz00(BgL_globalz00_bglt BgL_oz00_97,
		bool_t BgL_vz00_98)
	{
		{	/* Ast/var.sch 332 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_97)))->BgL_userzf3zf3) =
				((bool_t) BgL_vz00_98), BUNSPEC);
		}

	}



/* &global-user?-set! */
	obj_t BGl_z62globalzd2userzf3zd2setz12z83zzast_varz00(obj_t BgL_envz00_2612,
		obj_t BgL_oz00_2613, obj_t BgL_vz00_2614)
	{
		{	/* Ast/var.sch 332 */
			return
				BGl_globalzd2userzf3zd2setz12ze1zzast_varz00(
				((BgL_globalz00_bglt) BgL_oz00_2613), CBOOL(BgL_vz00_2614));
		}

	}



/* global-occurrencew */
	BGL_EXPORTED_DEF long
		BGl_globalzd2occurrencewzd2zzast_varz00(BgL_globalz00_bglt BgL_oz00_99)
	{
		{	/* Ast/var.sch 333 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_99)))->BgL_occurrencewz00);
		}

	}



/* &global-occurrencew */
	obj_t BGl_z62globalzd2occurrencewzb0zzast_varz00(obj_t BgL_envz00_2615,
		obj_t BgL_oz00_2616)
	{
		{	/* Ast/var.sch 333 */
			return
				BINT(BGl_globalzd2occurrencewzd2zzast_varz00(
					((BgL_globalz00_bglt) BgL_oz00_2616)));
		}

	}



/* global-occurrencew-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzd2occurrencewzd2setz12z12zzast_varz00(BgL_globalz00_bglt
		BgL_oz00_100, long BgL_vz00_101)
	{
		{	/* Ast/var.sch 334 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_100)))->BgL_occurrencewz00) =
				((long) BgL_vz00_101), BUNSPEC);
		}

	}



/* &global-occurrencew-set! */
	obj_t BGl_z62globalzd2occurrencewzd2setz12z70zzast_varz00(obj_t
		BgL_envz00_2617, obj_t BgL_oz00_2618, obj_t BgL_vz00_2619)
	{
		{	/* Ast/var.sch 334 */
			return
				BGl_globalzd2occurrencewzd2setz12z12zzast_varz00(
				((BgL_globalz00_bglt) BgL_oz00_2618), (long) CINT(BgL_vz00_2619));
		}

	}



/* global-occurrence */
	BGL_EXPORTED_DEF long
		BGl_globalzd2occurrencezd2zzast_varz00(BgL_globalz00_bglt BgL_oz00_102)
	{
		{	/* Ast/var.sch 335 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_102)))->BgL_occurrencez00);
		}

	}



/* &global-occurrence */
	obj_t BGl_z62globalzd2occurrencezb0zzast_varz00(obj_t BgL_envz00_2620,
		obj_t BgL_oz00_2621)
	{
		{	/* Ast/var.sch 335 */
			return
				BINT(BGl_globalzd2occurrencezd2zzast_varz00(
					((BgL_globalz00_bglt) BgL_oz00_2621)));
		}

	}



/* global-occurrence-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzd2occurrencezd2setz12z12zzast_varz00(BgL_globalz00_bglt
		BgL_oz00_103, long BgL_vz00_104)
	{
		{	/* Ast/var.sch 336 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_103)))->BgL_occurrencez00) =
				((long) BgL_vz00_104), BUNSPEC);
		}

	}



/* &global-occurrence-set! */
	obj_t BGl_z62globalzd2occurrencezd2setz12z70zzast_varz00(obj_t
		BgL_envz00_2622, obj_t BgL_oz00_2623, obj_t BgL_vz00_2624)
	{
		{	/* Ast/var.sch 336 */
			return
				BGl_globalzd2occurrencezd2setz12z12zzast_varz00(
				((BgL_globalz00_bglt) BgL_oz00_2623), (long) CINT(BgL_vz00_2624));
		}

	}



/* global-removable */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzd2removablezd2zzast_varz00(BgL_globalz00_bglt BgL_oz00_105)
	{
		{	/* Ast/var.sch 337 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_105)))->BgL_removablez00);
		}

	}



/* &global-removable */
	obj_t BGl_z62globalzd2removablezb0zzast_varz00(obj_t BgL_envz00_2625,
		obj_t BgL_oz00_2626)
	{
		{	/* Ast/var.sch 337 */
			return
				BGl_globalzd2removablezd2zzast_varz00(
				((BgL_globalz00_bglt) BgL_oz00_2626));
		}

	}



/* global-removable-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzd2removablezd2setz12z12zzast_varz00(BgL_globalz00_bglt
		BgL_oz00_106, obj_t BgL_vz00_107)
	{
		{	/* Ast/var.sch 338 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_106)))->BgL_removablez00) =
				((obj_t) BgL_vz00_107), BUNSPEC);
		}

	}



/* &global-removable-set! */
	obj_t BGl_z62globalzd2removablezd2setz12z70zzast_varz00(obj_t BgL_envz00_2627,
		obj_t BgL_oz00_2628, obj_t BgL_vz00_2629)
	{
		{	/* Ast/var.sch 338 */
			return
				BGl_globalzd2removablezd2setz12z12zzast_varz00(
				((BgL_globalz00_bglt) BgL_oz00_2628), BgL_vz00_2629);
		}

	}



/* global-fast-alpha */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzd2fastzd2alphaz00zzast_varz00(BgL_globalz00_bglt BgL_oz00_108)
	{
		{	/* Ast/var.sch 339 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_108)))->BgL_fastzd2alphazd2);
		}

	}



/* &global-fast-alpha */
	obj_t BGl_z62globalzd2fastzd2alphaz62zzast_varz00(obj_t BgL_envz00_2630,
		obj_t BgL_oz00_2631)
	{
		{	/* Ast/var.sch 339 */
			return
				BGl_globalzd2fastzd2alphaz00zzast_varz00(
				((BgL_globalz00_bglt) BgL_oz00_2631));
		}

	}



/* global-fast-alpha-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzd2fastzd2alphazd2setz12zc0zzast_varz00(BgL_globalz00_bglt
		BgL_oz00_109, obj_t BgL_vz00_110)
	{
		{	/* Ast/var.sch 340 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_109)))->BgL_fastzd2alphazd2) =
				((obj_t) BgL_vz00_110), BUNSPEC);
		}

	}



/* &global-fast-alpha-set! */
	obj_t BGl_z62globalzd2fastzd2alphazd2setz12za2zzast_varz00(obj_t
		BgL_envz00_2632, obj_t BgL_oz00_2633, obj_t BgL_vz00_2634)
	{
		{	/* Ast/var.sch 340 */
			return
				BGl_globalzd2fastzd2alphazd2setz12zc0zzast_varz00(
				((BgL_globalz00_bglt) BgL_oz00_2633), BgL_vz00_2634);
		}

	}



/* global-access */
	BGL_EXPORTED_DEF obj_t BGl_globalzd2accesszd2zzast_varz00(BgL_globalz00_bglt
		BgL_oz00_111)
	{
		{	/* Ast/var.sch 341 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_111)))->BgL_accessz00);
		}

	}



/* &global-access */
	obj_t BGl_z62globalzd2accesszb0zzast_varz00(obj_t BgL_envz00_2635,
		obj_t BgL_oz00_2636)
	{
		{	/* Ast/var.sch 341 */
			return
				BGl_globalzd2accesszd2zzast_varz00(
				((BgL_globalz00_bglt) BgL_oz00_2636));
		}

	}



/* global-access-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzd2accesszd2setz12z12zzast_varz00(BgL_globalz00_bglt BgL_oz00_112,
		obj_t BgL_vz00_113)
	{
		{	/* Ast/var.sch 342 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_112)))->BgL_accessz00) =
				((obj_t) BgL_vz00_113), BUNSPEC);
		}

	}



/* &global-access-set! */
	obj_t BGl_z62globalzd2accesszd2setz12z70zzast_varz00(obj_t BgL_envz00_2637,
		obj_t BgL_oz00_2638, obj_t BgL_vz00_2639)
	{
		{	/* Ast/var.sch 342 */
			return
				BGl_globalzd2accesszd2setz12z12zzast_varz00(
				((BgL_globalz00_bglt) BgL_oz00_2638), BgL_vz00_2639);
		}

	}



/* global-value */
	BGL_EXPORTED_DEF BgL_valuez00_bglt
		BGl_globalzd2valuezd2zzast_varz00(BgL_globalz00_bglt BgL_oz00_114)
	{
		{	/* Ast/var.sch 343 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_114)))->BgL_valuez00);
		}

	}



/* &global-value */
	BgL_valuez00_bglt BGl_z62globalzd2valuezb0zzast_varz00(obj_t BgL_envz00_2640,
		obj_t BgL_oz00_2641)
	{
		{	/* Ast/var.sch 343 */
			return
				BGl_globalzd2valuezd2zzast_varz00(((BgL_globalz00_bglt) BgL_oz00_2641));
		}

	}



/* global-value-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzd2valuezd2setz12z12zzast_varz00(BgL_globalz00_bglt BgL_oz00_115,
		BgL_valuez00_bglt BgL_vz00_116)
	{
		{	/* Ast/var.sch 344 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_115)))->BgL_valuez00) =
				((BgL_valuez00_bglt) BgL_vz00_116), BUNSPEC);
		}

	}



/* &global-value-set! */
	obj_t BGl_z62globalzd2valuezd2setz12z70zzast_varz00(obj_t BgL_envz00_2642,
		obj_t BgL_oz00_2643, obj_t BgL_vz00_2644)
	{
		{	/* Ast/var.sch 344 */
			return
				BGl_globalzd2valuezd2setz12z12zzast_varz00(
				((BgL_globalz00_bglt) BgL_oz00_2643),
				((BgL_valuez00_bglt) BgL_vz00_2644));
		}

	}



/* global-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_globalzd2typezd2zzast_varz00(BgL_globalz00_bglt BgL_oz00_117)
	{
		{	/* Ast/var.sch 345 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_117)))->BgL_typez00);
		}

	}



/* &global-type */
	BgL_typez00_bglt BGl_z62globalzd2typezb0zzast_varz00(obj_t BgL_envz00_2645,
		obj_t BgL_oz00_2646)
	{
		{	/* Ast/var.sch 345 */
			return
				BGl_globalzd2typezd2zzast_varz00(((BgL_globalz00_bglt) BgL_oz00_2646));
		}

	}



/* global-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzd2typezd2setz12z12zzast_varz00(BgL_globalz00_bglt BgL_oz00_118,
		BgL_typez00_bglt BgL_vz00_119)
	{
		{	/* Ast/var.sch 346 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_118)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_119), BUNSPEC);
		}

	}



/* &global-type-set! */
	obj_t BGl_z62globalzd2typezd2setz12z70zzast_varz00(obj_t BgL_envz00_2647,
		obj_t BgL_oz00_2648, obj_t BgL_vz00_2649)
	{
		{	/* Ast/var.sch 346 */
			return
				BGl_globalzd2typezd2setz12z12zzast_varz00(
				((BgL_globalz00_bglt) BgL_oz00_2648),
				((BgL_typez00_bglt) BgL_vz00_2649));
		}

	}



/* global-name */
	BGL_EXPORTED_DEF obj_t BGl_globalzd2namezd2zzast_varz00(BgL_globalz00_bglt
		BgL_oz00_120)
	{
		{	/* Ast/var.sch 347 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_120)))->BgL_namez00);
		}

	}



/* &global-name */
	obj_t BGl_z62globalzd2namezb0zzast_varz00(obj_t BgL_envz00_2650,
		obj_t BgL_oz00_2651)
	{
		{	/* Ast/var.sch 347 */
			return
				BGl_globalzd2namezd2zzast_varz00(((BgL_globalz00_bglt) BgL_oz00_2651));
		}

	}



/* global-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzd2namezd2setz12z12zzast_varz00(BgL_globalz00_bglt BgL_oz00_121,
		obj_t BgL_vz00_122)
	{
		{	/* Ast/var.sch 348 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_121)))->BgL_namez00) =
				((obj_t) BgL_vz00_122), BUNSPEC);
		}

	}



/* &global-name-set! */
	obj_t BGl_z62globalzd2namezd2setz12z70zzast_varz00(obj_t BgL_envz00_2652,
		obj_t BgL_oz00_2653, obj_t BgL_vz00_2654)
	{
		{	/* Ast/var.sch 348 */
			return
				BGl_globalzd2namezd2setz12z12zzast_varz00(
				((BgL_globalz00_bglt) BgL_oz00_2653), BgL_vz00_2654);
		}

	}



/* global-id */
	BGL_EXPORTED_DEF obj_t BGl_globalzd2idzd2zzast_varz00(BgL_globalz00_bglt
		BgL_oz00_123)
	{
		{	/* Ast/var.sch 349 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_123)))->BgL_idz00);
		}

	}



/* &global-id */
	obj_t BGl_z62globalzd2idzb0zzast_varz00(obj_t BgL_envz00_2655,
		obj_t BgL_oz00_2656)
	{
		{	/* Ast/var.sch 349 */
			return
				BGl_globalzd2idzd2zzast_varz00(((BgL_globalz00_bglt) BgL_oz00_2656));
		}

	}



/* make-local */
	BGL_EXPORTED_DEF BgL_localz00_bglt BGl_makezd2localzd2zzast_varz00(obj_t
		BgL_id1248z00_126, obj_t BgL_name1249z00_127,
		BgL_typez00_bglt BgL_type1250z00_128,
		BgL_valuez00_bglt BgL_value1251z00_129, obj_t BgL_access1252z00_130,
		obj_t BgL_fastzd2alpha1253zd2_131, obj_t BgL_removable1254z00_132,
		long BgL_occurrence1255z00_133, long BgL_occurrencew1256z00_134,
		bool_t BgL_userzf31257zf3_135, long BgL_key1258z00_136,
		obj_t BgL_valzd2noescape1259zd2_137)
	{
		{	/* Ast/var.sch 353 */
			{	/* Ast/var.sch 353 */
				BgL_localz00_bglt BgL_new1191z00_3808;

				{	/* Ast/var.sch 353 */
					BgL_localz00_bglt BgL_new1190z00_3809;

					BgL_new1190z00_3809 =
						((BgL_localz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_localz00_bgl))));
					{	/* Ast/var.sch 353 */
						long BgL_arg1308z00_3810;

						BgL_arg1308z00_3810 = BGL_CLASS_NUM(BGl_localz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1190z00_3809), BgL_arg1308z00_3810);
					}
					{	/* Ast/var.sch 353 */
						BgL_objectz00_bglt BgL_tmpz00_4643;

						BgL_tmpz00_4643 = ((BgL_objectz00_bglt) BgL_new1190z00_3809);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4643, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1190z00_3809);
					BgL_new1191z00_3808 = BgL_new1190z00_3809;
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_new1191z00_3808)))->BgL_idz00) =
					((obj_t) BgL_id1248z00_126), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1191z00_3808)))->BgL_namez00) =
					((obj_t) BgL_name1249z00_127), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1191z00_3808)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1250z00_128), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1191z00_3808)))->BgL_valuez00) =
					((BgL_valuez00_bglt) BgL_value1251z00_129), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1191z00_3808)))->BgL_accessz00) =
					((obj_t) BgL_access1252z00_130), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1191z00_3808)))->BgL_fastzd2alphazd2) =
					((obj_t) BgL_fastzd2alpha1253zd2_131), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1191z00_3808)))->BgL_removablez00) =
					((obj_t) BgL_removable1254z00_132), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1191z00_3808)))->BgL_occurrencez00) =
					((long) BgL_occurrence1255z00_133), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1191z00_3808)))->BgL_occurrencewz00) =
					((long) BgL_occurrencew1256z00_134), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1191z00_3808)))->BgL_userzf3zf3) =
					((bool_t) BgL_userzf31257zf3_135), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(BgL_new1191z00_3808))->BgL_keyz00) =
					((long) BgL_key1258z00_136), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(BgL_new1191z00_3808))->
						BgL_valzd2noescapezd2) =
					((obj_t) BgL_valzd2noescape1259zd2_137), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(BgL_new1191z00_3808))->BgL_volatilez00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				return BgL_new1191z00_3808;
			}
		}

	}



/* &make-local */
	BgL_localz00_bglt BGl_z62makezd2localzb0zzast_varz00(obj_t BgL_envz00_2657,
		obj_t BgL_id1248z00_2658, obj_t BgL_name1249z00_2659,
		obj_t BgL_type1250z00_2660, obj_t BgL_value1251z00_2661,
		obj_t BgL_access1252z00_2662, obj_t BgL_fastzd2alpha1253zd2_2663,
		obj_t BgL_removable1254z00_2664, obj_t BgL_occurrence1255z00_2665,
		obj_t BgL_occurrencew1256z00_2666, obj_t BgL_userzf31257zf3_2667,
		obj_t BgL_key1258z00_2668, obj_t BgL_valzd2noescape1259zd2_2669)
	{
		{	/* Ast/var.sch 353 */
			return
				BGl_makezd2localzd2zzast_varz00(BgL_id1248z00_2658,
				BgL_name1249z00_2659, ((BgL_typez00_bglt) BgL_type1250z00_2660),
				((BgL_valuez00_bglt) BgL_value1251z00_2661), BgL_access1252z00_2662,
				BgL_fastzd2alpha1253zd2_2663, BgL_removable1254z00_2664,
				(long) CINT(BgL_occurrence1255z00_2665),
				(long) CINT(BgL_occurrencew1256z00_2666),
				CBOOL(BgL_userzf31257zf3_2667), (long) CINT(BgL_key1258z00_2668),
				BgL_valzd2noescape1259zd2_2669);
		}

	}



/* local? */
	BGL_EXPORTED_DEF bool_t BGl_localzf3zf3zzast_varz00(obj_t BgL_objz00_138)
	{
		{	/* Ast/var.sch 354 */
			{	/* Ast/var.sch 354 */
				obj_t BgL_classz00_3811;

				BgL_classz00_3811 = BGl_localz00zzast_varz00;
				if (BGL_OBJECTP(BgL_objz00_138))
					{	/* Ast/var.sch 354 */
						BgL_objectz00_bglt BgL_arg1807z00_3812;

						BgL_arg1807z00_3812 = (BgL_objectz00_bglt) (BgL_objz00_138);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Ast/var.sch 354 */
								long BgL_idxz00_3813;

								BgL_idxz00_3813 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3812);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3813 + 2L)) == BgL_classz00_3811);
							}
						else
							{	/* Ast/var.sch 354 */
								bool_t BgL_res2165z00_3816;

								{	/* Ast/var.sch 354 */
									obj_t BgL_oclassz00_3817;

									{	/* Ast/var.sch 354 */
										obj_t BgL_arg1815z00_3818;
										long BgL_arg1816z00_3819;

										BgL_arg1815z00_3818 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Ast/var.sch 354 */
											long BgL_arg1817z00_3820;

											BgL_arg1817z00_3820 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3812);
											BgL_arg1816z00_3819 = (BgL_arg1817z00_3820 - OBJECT_TYPE);
										}
										BgL_oclassz00_3817 =
											VECTOR_REF(BgL_arg1815z00_3818, BgL_arg1816z00_3819);
									}
									{	/* Ast/var.sch 354 */
										bool_t BgL__ortest_1115z00_3821;

										BgL__ortest_1115z00_3821 =
											(BgL_classz00_3811 == BgL_oclassz00_3817);
										if (BgL__ortest_1115z00_3821)
											{	/* Ast/var.sch 354 */
												BgL_res2165z00_3816 = BgL__ortest_1115z00_3821;
											}
										else
											{	/* Ast/var.sch 354 */
												long BgL_odepthz00_3822;

												{	/* Ast/var.sch 354 */
													obj_t BgL_arg1804z00_3823;

													BgL_arg1804z00_3823 = (BgL_oclassz00_3817);
													BgL_odepthz00_3822 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3823);
												}
												if ((2L < BgL_odepthz00_3822))
													{	/* Ast/var.sch 354 */
														obj_t BgL_arg1802z00_3824;

														{	/* Ast/var.sch 354 */
															obj_t BgL_arg1803z00_3825;

															BgL_arg1803z00_3825 = (BgL_oclassz00_3817);
															BgL_arg1802z00_3824 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3825,
																2L);
														}
														BgL_res2165z00_3816 =
															(BgL_arg1802z00_3824 == BgL_classz00_3811);
													}
												else
													{	/* Ast/var.sch 354 */
														BgL_res2165z00_3816 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2165z00_3816;
							}
					}
				else
					{	/* Ast/var.sch 354 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &local? */
	obj_t BGl_z62localzf3z91zzast_varz00(obj_t BgL_envz00_2670,
		obj_t BgL_objz00_2671)
	{
		{	/* Ast/var.sch 354 */
			return BBOOL(BGl_localzf3zf3zzast_varz00(BgL_objz00_2671));
		}

	}



/* local-nil */
	BGL_EXPORTED_DEF BgL_localz00_bglt BGl_localzd2nilzd2zzast_varz00(void)
	{
		{	/* Ast/var.sch 355 */
			{	/* Ast/var.sch 355 */
				obj_t BgL_classz00_1741;

				BgL_classz00_1741 = BGl_localz00zzast_varz00;
				{	/* Ast/var.sch 355 */
					obj_t BgL__ortest_1117z00_1742;

					BgL__ortest_1117z00_1742 = BGL_CLASS_NIL(BgL_classz00_1741);
					if (CBOOL(BgL__ortest_1117z00_1742))
						{	/* Ast/var.sch 355 */
							return ((BgL_localz00_bglt) BgL__ortest_1117z00_1742);
						}
					else
						{	/* Ast/var.sch 355 */
							return
								((BgL_localz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1741));
						}
				}
			}
		}

	}



/* &local-nil */
	BgL_localz00_bglt BGl_z62localzd2nilzb0zzast_varz00(obj_t BgL_envz00_2672)
	{
		{	/* Ast/var.sch 355 */
			return BGl_localzd2nilzd2zzast_varz00();
		}

	}



/* local-val-noescape */
	BGL_EXPORTED_DEF obj_t
		BGl_localzd2valzd2noescapez00zzast_varz00(BgL_localz00_bglt BgL_oz00_139)
	{
		{	/* Ast/var.sch 356 */
			return
				(((BgL_localz00_bglt) COBJECT(BgL_oz00_139))->BgL_valzd2noescapezd2);
		}

	}



/* &local-val-noescape */
	obj_t BGl_z62localzd2valzd2noescapez62zzast_varz00(obj_t BgL_envz00_2673,
		obj_t BgL_oz00_2674)
	{
		{	/* Ast/var.sch 356 */
			return
				BGl_localzd2valzd2noescapez00zzast_varz00(
				((BgL_localz00_bglt) BgL_oz00_2674));
		}

	}



/* local-val-noescape-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzd2valzd2noescapezd2setz12zc0zzast_varz00(BgL_localz00_bglt
		BgL_oz00_140, obj_t BgL_vz00_141)
	{
		{	/* Ast/var.sch 357 */
			return
				((((BgL_localz00_bglt) COBJECT(BgL_oz00_140))->BgL_valzd2noescapezd2) =
				((obj_t) BgL_vz00_141), BUNSPEC);
		}

	}



/* &local-val-noescape-set! */
	obj_t BGl_z62localzd2valzd2noescapezd2setz12za2zzast_varz00(obj_t
		BgL_envz00_2675, obj_t BgL_oz00_2676, obj_t BgL_vz00_2677)
	{
		{	/* Ast/var.sch 357 */
			return
				BGl_localzd2valzd2noescapezd2setz12zc0zzast_varz00(
				((BgL_localz00_bglt) BgL_oz00_2676), BgL_vz00_2677);
		}

	}



/* local-key */
	BGL_EXPORTED_DEF long BGl_localzd2keyzd2zzast_varz00(BgL_localz00_bglt
		BgL_oz00_142)
	{
		{	/* Ast/var.sch 358 */
			return (((BgL_localz00_bglt) COBJECT(BgL_oz00_142))->BgL_keyz00);
		}

	}



/* &local-key */
	obj_t BGl_z62localzd2keyzb0zzast_varz00(obj_t BgL_envz00_2678,
		obj_t BgL_oz00_2679)
	{
		{	/* Ast/var.sch 358 */
			return
				BINT(BGl_localzd2keyzd2zzast_varz00(
					((BgL_localz00_bglt) BgL_oz00_2679)));
		}

	}



/* local-user? */
	BGL_EXPORTED_DEF bool_t BGl_localzd2userzf3z21zzast_varz00(BgL_localz00_bglt
		BgL_oz00_145)
	{
		{	/* Ast/var.sch 360 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_145)))->BgL_userzf3zf3);
		}

	}



/* &local-user? */
	obj_t BGl_z62localzd2userzf3z43zzast_varz00(obj_t BgL_envz00_2680,
		obj_t BgL_oz00_2681)
	{
		{	/* Ast/var.sch 360 */
			return
				BBOOL(BGl_localzd2userzf3z21zzast_varz00(
					((BgL_localz00_bglt) BgL_oz00_2681)));
		}

	}



/* local-user?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzd2userzf3zd2setz12ze1zzast_varz00(BgL_localz00_bglt BgL_oz00_146,
		bool_t BgL_vz00_147)
	{
		{	/* Ast/var.sch 361 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_146)))->BgL_userzf3zf3) =
				((bool_t) BgL_vz00_147), BUNSPEC);
		}

	}



/* &local-user?-set! */
	obj_t BGl_z62localzd2userzf3zd2setz12z83zzast_varz00(obj_t BgL_envz00_2682,
		obj_t BgL_oz00_2683, obj_t BgL_vz00_2684)
	{
		{	/* Ast/var.sch 361 */
			return
				BGl_localzd2userzf3zd2setz12ze1zzast_varz00(
				((BgL_localz00_bglt) BgL_oz00_2683), CBOOL(BgL_vz00_2684));
		}

	}



/* local-occurrencew */
	BGL_EXPORTED_DEF long BGl_localzd2occurrencewzd2zzast_varz00(BgL_localz00_bglt
		BgL_oz00_148)
	{
		{	/* Ast/var.sch 362 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_148)))->BgL_occurrencewz00);
		}

	}



/* &local-occurrencew */
	obj_t BGl_z62localzd2occurrencewzb0zzast_varz00(obj_t BgL_envz00_2685,
		obj_t BgL_oz00_2686)
	{
		{	/* Ast/var.sch 362 */
			return
				BINT(BGl_localzd2occurrencewzd2zzast_varz00(
					((BgL_localz00_bglt) BgL_oz00_2686)));
		}

	}



/* local-occurrencew-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzd2occurrencewzd2setz12z12zzast_varz00(BgL_localz00_bglt
		BgL_oz00_149, long BgL_vz00_150)
	{
		{	/* Ast/var.sch 363 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_149)))->BgL_occurrencewz00) =
				((long) BgL_vz00_150), BUNSPEC);
		}

	}



/* &local-occurrencew-set! */
	obj_t BGl_z62localzd2occurrencewzd2setz12z70zzast_varz00(obj_t
		BgL_envz00_2687, obj_t BgL_oz00_2688, obj_t BgL_vz00_2689)
	{
		{	/* Ast/var.sch 363 */
			return
				BGl_localzd2occurrencewzd2setz12z12zzast_varz00(
				((BgL_localz00_bglt) BgL_oz00_2688), (long) CINT(BgL_vz00_2689));
		}

	}



/* local-occurrence */
	BGL_EXPORTED_DEF long BGl_localzd2occurrencezd2zzast_varz00(BgL_localz00_bglt
		BgL_oz00_151)
	{
		{	/* Ast/var.sch 364 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_151)))->BgL_occurrencez00);
		}

	}



/* &local-occurrence */
	obj_t BGl_z62localzd2occurrencezb0zzast_varz00(obj_t BgL_envz00_2690,
		obj_t BgL_oz00_2691)
	{
		{	/* Ast/var.sch 364 */
			return
				BINT(BGl_localzd2occurrencezd2zzast_varz00(
					((BgL_localz00_bglt) BgL_oz00_2691)));
		}

	}



/* local-occurrence-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzd2occurrencezd2setz12z12zzast_varz00(BgL_localz00_bglt
		BgL_oz00_152, long BgL_vz00_153)
	{
		{	/* Ast/var.sch 365 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_152)))->BgL_occurrencez00) =
				((long) BgL_vz00_153), BUNSPEC);
		}

	}



/* &local-occurrence-set! */
	obj_t BGl_z62localzd2occurrencezd2setz12z70zzast_varz00(obj_t BgL_envz00_2692,
		obj_t BgL_oz00_2693, obj_t BgL_vz00_2694)
	{
		{	/* Ast/var.sch 365 */
			return
				BGl_localzd2occurrencezd2setz12z12zzast_varz00(
				((BgL_localz00_bglt) BgL_oz00_2693), (long) CINT(BgL_vz00_2694));
		}

	}



/* local-removable */
	BGL_EXPORTED_DEF obj_t BGl_localzd2removablezd2zzast_varz00(BgL_localz00_bglt
		BgL_oz00_154)
	{
		{	/* Ast/var.sch 366 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_154)))->BgL_removablez00);
		}

	}



/* &local-removable */
	obj_t BGl_z62localzd2removablezb0zzast_varz00(obj_t BgL_envz00_2695,
		obj_t BgL_oz00_2696)
	{
		{	/* Ast/var.sch 366 */
			return
				BGl_localzd2removablezd2zzast_varz00(
				((BgL_localz00_bglt) BgL_oz00_2696));
		}

	}



/* local-removable-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzd2removablezd2setz12z12zzast_varz00(BgL_localz00_bglt
		BgL_oz00_155, obj_t BgL_vz00_156)
	{
		{	/* Ast/var.sch 367 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_155)))->BgL_removablez00) =
				((obj_t) BgL_vz00_156), BUNSPEC);
		}

	}



/* &local-removable-set! */
	obj_t BGl_z62localzd2removablezd2setz12z70zzast_varz00(obj_t BgL_envz00_2697,
		obj_t BgL_oz00_2698, obj_t BgL_vz00_2699)
	{
		{	/* Ast/var.sch 367 */
			return
				BGl_localzd2removablezd2setz12z12zzast_varz00(
				((BgL_localz00_bglt) BgL_oz00_2698), BgL_vz00_2699);
		}

	}



/* local-fast-alpha */
	BGL_EXPORTED_DEF obj_t
		BGl_localzd2fastzd2alphaz00zzast_varz00(BgL_localz00_bglt BgL_oz00_157)
	{
		{	/* Ast/var.sch 368 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_157)))->BgL_fastzd2alphazd2);
		}

	}



/* &local-fast-alpha */
	obj_t BGl_z62localzd2fastzd2alphaz62zzast_varz00(obj_t BgL_envz00_2700,
		obj_t BgL_oz00_2701)
	{
		{	/* Ast/var.sch 368 */
			return
				BGl_localzd2fastzd2alphaz00zzast_varz00(
				((BgL_localz00_bglt) BgL_oz00_2701));
		}

	}



/* local-fast-alpha-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzd2fastzd2alphazd2setz12zc0zzast_varz00(BgL_localz00_bglt
		BgL_oz00_158, obj_t BgL_vz00_159)
	{
		{	/* Ast/var.sch 369 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_158)))->BgL_fastzd2alphazd2) =
				((obj_t) BgL_vz00_159), BUNSPEC);
		}

	}



/* &local-fast-alpha-set! */
	obj_t BGl_z62localzd2fastzd2alphazd2setz12za2zzast_varz00(obj_t
		BgL_envz00_2702, obj_t BgL_oz00_2703, obj_t BgL_vz00_2704)
	{
		{	/* Ast/var.sch 369 */
			return
				BGl_localzd2fastzd2alphazd2setz12zc0zzast_varz00(
				((BgL_localz00_bglt) BgL_oz00_2703), BgL_vz00_2704);
		}

	}



/* local-access */
	BGL_EXPORTED_DEF obj_t BGl_localzd2accesszd2zzast_varz00(BgL_localz00_bglt
		BgL_oz00_160)
	{
		{	/* Ast/var.sch 370 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_160)))->BgL_accessz00);
		}

	}



/* &local-access */
	obj_t BGl_z62localzd2accesszb0zzast_varz00(obj_t BgL_envz00_2705,
		obj_t BgL_oz00_2706)
	{
		{	/* Ast/var.sch 370 */
			return
				BGl_localzd2accesszd2zzast_varz00(((BgL_localz00_bglt) BgL_oz00_2706));
		}

	}



/* local-access-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzd2accesszd2setz12z12zzast_varz00(BgL_localz00_bglt BgL_oz00_161,
		obj_t BgL_vz00_162)
	{
		{	/* Ast/var.sch 371 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_161)))->BgL_accessz00) =
				((obj_t) BgL_vz00_162), BUNSPEC);
		}

	}



/* &local-access-set! */
	obj_t BGl_z62localzd2accesszd2setz12z70zzast_varz00(obj_t BgL_envz00_2707,
		obj_t BgL_oz00_2708, obj_t BgL_vz00_2709)
	{
		{	/* Ast/var.sch 371 */
			return
				BGl_localzd2accesszd2setz12z12zzast_varz00(
				((BgL_localz00_bglt) BgL_oz00_2708), BgL_vz00_2709);
		}

	}



/* local-value */
	BGL_EXPORTED_DEF BgL_valuez00_bglt
		BGl_localzd2valuezd2zzast_varz00(BgL_localz00_bglt BgL_oz00_163)
	{
		{	/* Ast/var.sch 372 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_163)))->BgL_valuez00);
		}

	}



/* &local-value */
	BgL_valuez00_bglt BGl_z62localzd2valuezb0zzast_varz00(obj_t BgL_envz00_2710,
		obj_t BgL_oz00_2711)
	{
		{	/* Ast/var.sch 372 */
			return
				BGl_localzd2valuezd2zzast_varz00(((BgL_localz00_bglt) BgL_oz00_2711));
		}

	}



/* local-value-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzd2valuezd2setz12z12zzast_varz00(BgL_localz00_bglt BgL_oz00_164,
		BgL_valuez00_bglt BgL_vz00_165)
	{
		{	/* Ast/var.sch 373 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_164)))->BgL_valuez00) =
				((BgL_valuez00_bglt) BgL_vz00_165), BUNSPEC);
		}

	}



/* &local-value-set! */
	obj_t BGl_z62localzd2valuezd2setz12z70zzast_varz00(obj_t BgL_envz00_2712,
		obj_t BgL_oz00_2713, obj_t BgL_vz00_2714)
	{
		{	/* Ast/var.sch 373 */
			return
				BGl_localzd2valuezd2setz12z12zzast_varz00(
				((BgL_localz00_bglt) BgL_oz00_2713),
				((BgL_valuez00_bglt) BgL_vz00_2714));
		}

	}



/* local-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_localzd2typezd2zzast_varz00(BgL_localz00_bglt BgL_oz00_166)
	{
		{	/* Ast/var.sch 374 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_166)))->BgL_typez00);
		}

	}



/* &local-type */
	BgL_typez00_bglt BGl_z62localzd2typezb0zzast_varz00(obj_t BgL_envz00_2715,
		obj_t BgL_oz00_2716)
	{
		{	/* Ast/var.sch 374 */
			return
				BGl_localzd2typezd2zzast_varz00(((BgL_localz00_bglt) BgL_oz00_2716));
		}

	}



/* local-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzd2typezd2setz12z12zzast_varz00(BgL_localz00_bglt BgL_oz00_167,
		BgL_typez00_bglt BgL_vz00_168)
	{
		{	/* Ast/var.sch 375 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_167)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_168), BUNSPEC);
		}

	}



/* &local-type-set! */
	obj_t BGl_z62localzd2typezd2setz12z70zzast_varz00(obj_t BgL_envz00_2717,
		obj_t BgL_oz00_2718, obj_t BgL_vz00_2719)
	{
		{	/* Ast/var.sch 375 */
			return
				BGl_localzd2typezd2setz12z12zzast_varz00(
				((BgL_localz00_bglt) BgL_oz00_2718),
				((BgL_typez00_bglt) BgL_vz00_2719));
		}

	}



/* local-name */
	BGL_EXPORTED_DEF obj_t BGl_localzd2namezd2zzast_varz00(BgL_localz00_bglt
		BgL_oz00_169)
	{
		{	/* Ast/var.sch 376 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_169)))->BgL_namez00);
		}

	}



/* &local-name */
	obj_t BGl_z62localzd2namezb0zzast_varz00(obj_t BgL_envz00_2720,
		obj_t BgL_oz00_2721)
	{
		{	/* Ast/var.sch 376 */
			return
				BGl_localzd2namezd2zzast_varz00(((BgL_localz00_bglt) BgL_oz00_2721));
		}

	}



/* local-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzd2namezd2setz12z12zzast_varz00(BgL_localz00_bglt BgL_oz00_170,
		obj_t BgL_vz00_171)
	{
		{	/* Ast/var.sch 377 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_170)))->BgL_namez00) =
				((obj_t) BgL_vz00_171), BUNSPEC);
		}

	}



/* &local-name-set! */
	obj_t BGl_z62localzd2namezd2setz12z70zzast_varz00(obj_t BgL_envz00_2722,
		obj_t BgL_oz00_2723, obj_t BgL_vz00_2724)
	{
		{	/* Ast/var.sch 377 */
			return
				BGl_localzd2namezd2setz12z12zzast_varz00(
				((BgL_localz00_bglt) BgL_oz00_2723), BgL_vz00_2724);
		}

	}



/* local-id */
	BGL_EXPORTED_DEF obj_t BGl_localzd2idzd2zzast_varz00(BgL_localz00_bglt
		BgL_oz00_172)
	{
		{	/* Ast/var.sch 378 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_172)))->BgL_idz00);
		}

	}



/* &local-id */
	obj_t BGl_z62localzd2idzb0zzast_varz00(obj_t BgL_envz00_2725,
		obj_t BgL_oz00_2726)
	{
		{	/* Ast/var.sch 378 */
			return BGl_localzd2idzd2zzast_varz00(((BgL_localz00_bglt) BgL_oz00_2726));
		}

	}



/* make-fun */
	BGL_EXPORTED_DEF BgL_funz00_bglt BGl_makezd2funzd2zzast_varz00(long
		BgL_arity1237z00_175, obj_t BgL_sidezd2effect1238zd2_176,
		obj_t BgL_predicatezd2of1239zd2_177, obj_t BgL_stackzd2allocator1240zd2_178,
		bool_t BgL_topzf31241zf3_179, obj_t BgL_thezd2closure1242zd2_180,
		obj_t BgL_effect1243z00_181, obj_t BgL_failsafe1244z00_182,
		obj_t BgL_argszd2noescape1245zd2_183, obj_t BgL_argszd2retescape1246zd2_184)
	{
		{	/* Ast/var.sch 382 */
			{	/* Ast/var.sch 382 */
				BgL_funz00_bglt BgL_new1193z00_3826;

				{	/* Ast/var.sch 382 */
					BgL_funz00_bglt BgL_new1192z00_3827;

					BgL_new1192z00_3827 =
						((BgL_funz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_funz00_bgl))));
					{	/* Ast/var.sch 382 */
						long BgL_arg1310z00_3828;

						BgL_arg1310z00_3828 = BGL_CLASS_NUM(BGl_funz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1192z00_3827), BgL_arg1310z00_3828);
					}
					BgL_new1193z00_3826 = BgL_new1192z00_3827;
				}
				((((BgL_funz00_bglt) COBJECT(BgL_new1193z00_3826))->BgL_arityz00) =
					((long) BgL_arity1237z00_175), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(BgL_new1193z00_3826))->
						BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1238zd2_176), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(BgL_new1193z00_3826))->
						BgL_predicatezd2ofzd2) =
					((obj_t) BgL_predicatezd2of1239zd2_177), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(BgL_new1193z00_3826))->
						BgL_stackzd2allocatorzd2) =
					((obj_t) BgL_stackzd2allocator1240zd2_178), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(BgL_new1193z00_3826))->BgL_topzf3zf3) =
					((bool_t) BgL_topzf31241zf3_179), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(BgL_new1193z00_3826))->
						BgL_thezd2closurezd2) =
					((obj_t) BgL_thezd2closure1242zd2_180), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(BgL_new1193z00_3826))->BgL_effectz00) =
					((obj_t) BgL_effect1243z00_181), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(BgL_new1193z00_3826))->BgL_failsafez00) =
					((obj_t) BgL_failsafe1244z00_182), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(BgL_new1193z00_3826))->
						BgL_argszd2noescapezd2) =
					((obj_t) BgL_argszd2noescape1245zd2_183), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(BgL_new1193z00_3826))->
						BgL_argszd2retescapezd2) =
					((obj_t) BgL_argszd2retescape1246zd2_184), BUNSPEC);
				return BgL_new1193z00_3826;
			}
		}

	}



/* &make-fun */
	BgL_funz00_bglt BGl_z62makezd2funzb0zzast_varz00(obj_t BgL_envz00_2727,
		obj_t BgL_arity1237z00_2728, obj_t BgL_sidezd2effect1238zd2_2729,
		obj_t BgL_predicatezd2of1239zd2_2730,
		obj_t BgL_stackzd2allocator1240zd2_2731, obj_t BgL_topzf31241zf3_2732,
		obj_t BgL_thezd2closure1242zd2_2733, obj_t BgL_effect1243z00_2734,
		obj_t BgL_failsafe1244z00_2735, obj_t BgL_argszd2noescape1245zd2_2736,
		obj_t BgL_argszd2retescape1246zd2_2737)
	{
		{	/* Ast/var.sch 382 */
			return
				BGl_makezd2funzd2zzast_varz00(
				(long) CINT(BgL_arity1237z00_2728), BgL_sidezd2effect1238zd2_2729,
				BgL_predicatezd2of1239zd2_2730, BgL_stackzd2allocator1240zd2_2731,
				CBOOL(BgL_topzf31241zf3_2732), BgL_thezd2closure1242zd2_2733,
				BgL_effect1243z00_2734, BgL_failsafe1244z00_2735,
				BgL_argszd2noescape1245zd2_2736, BgL_argszd2retescape1246zd2_2737);
		}

	}



/* fun? */
	BGL_EXPORTED_DEF bool_t BGl_funzf3zf3zzast_varz00(obj_t BgL_objz00_185)
	{
		{	/* Ast/var.sch 383 */
			{	/* Ast/var.sch 383 */
				obj_t BgL_classz00_3829;

				BgL_classz00_3829 = BGl_funz00zzast_varz00;
				if (BGL_OBJECTP(BgL_objz00_185))
					{	/* Ast/var.sch 383 */
						BgL_objectz00_bglt BgL_arg1807z00_3830;

						BgL_arg1807z00_3830 = (BgL_objectz00_bglt) (BgL_objz00_185);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Ast/var.sch 383 */
								long BgL_idxz00_3831;

								BgL_idxz00_3831 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3830);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3831 + 2L)) == BgL_classz00_3829);
							}
						else
							{	/* Ast/var.sch 383 */
								bool_t BgL_res2166z00_3834;

								{	/* Ast/var.sch 383 */
									obj_t BgL_oclassz00_3835;

									{	/* Ast/var.sch 383 */
										obj_t BgL_arg1815z00_3836;
										long BgL_arg1816z00_3837;

										BgL_arg1815z00_3836 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Ast/var.sch 383 */
											long BgL_arg1817z00_3838;

											BgL_arg1817z00_3838 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3830);
											BgL_arg1816z00_3837 = (BgL_arg1817z00_3838 - OBJECT_TYPE);
										}
										BgL_oclassz00_3835 =
											VECTOR_REF(BgL_arg1815z00_3836, BgL_arg1816z00_3837);
									}
									{	/* Ast/var.sch 383 */
										bool_t BgL__ortest_1115z00_3839;

										BgL__ortest_1115z00_3839 =
											(BgL_classz00_3829 == BgL_oclassz00_3835);
										if (BgL__ortest_1115z00_3839)
											{	/* Ast/var.sch 383 */
												BgL_res2166z00_3834 = BgL__ortest_1115z00_3839;
											}
										else
											{	/* Ast/var.sch 383 */
												long BgL_odepthz00_3840;

												{	/* Ast/var.sch 383 */
													obj_t BgL_arg1804z00_3841;

													BgL_arg1804z00_3841 = (BgL_oclassz00_3835);
													BgL_odepthz00_3840 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3841);
												}
												if ((2L < BgL_odepthz00_3840))
													{	/* Ast/var.sch 383 */
														obj_t BgL_arg1802z00_3842;

														{	/* Ast/var.sch 383 */
															obj_t BgL_arg1803z00_3843;

															BgL_arg1803z00_3843 = (BgL_oclassz00_3835);
															BgL_arg1802z00_3842 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3843,
																2L);
														}
														BgL_res2166z00_3834 =
															(BgL_arg1802z00_3842 == BgL_classz00_3829);
													}
												else
													{	/* Ast/var.sch 383 */
														BgL_res2166z00_3834 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2166z00_3834;
							}
					}
				else
					{	/* Ast/var.sch 383 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &fun? */
	obj_t BGl_z62funzf3z91zzast_varz00(obj_t BgL_envz00_2738,
		obj_t BgL_objz00_2739)
	{
		{	/* Ast/var.sch 383 */
			return BBOOL(BGl_funzf3zf3zzast_varz00(BgL_objz00_2739));
		}

	}



/* fun-nil */
	BGL_EXPORTED_DEF BgL_funz00_bglt BGl_funzd2nilzd2zzast_varz00(void)
	{
		{	/* Ast/var.sch 384 */
			{	/* Ast/var.sch 384 */
				obj_t BgL_classz00_1783;

				BgL_classz00_1783 = BGl_funz00zzast_varz00;
				{	/* Ast/var.sch 384 */
					obj_t BgL__ortest_1117z00_1784;

					BgL__ortest_1117z00_1784 = BGL_CLASS_NIL(BgL_classz00_1783);
					if (CBOOL(BgL__ortest_1117z00_1784))
						{	/* Ast/var.sch 384 */
							return ((BgL_funz00_bglt) BgL__ortest_1117z00_1784);
						}
					else
						{	/* Ast/var.sch 384 */
							return
								((BgL_funz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1783));
						}
				}
			}
		}

	}



/* &fun-nil */
	BgL_funz00_bglt BGl_z62funzd2nilzb0zzast_varz00(obj_t BgL_envz00_2740)
	{
		{	/* Ast/var.sch 384 */
			return BGl_funzd2nilzd2zzast_varz00();
		}

	}



/* fun-args-retescape */
	BGL_EXPORTED_DEF obj_t
		BGl_funzd2argszd2retescapez00zzast_varz00(BgL_funz00_bglt BgL_oz00_186)
	{
		{	/* Ast/var.sch 385 */
			return
				(((BgL_funz00_bglt) COBJECT(BgL_oz00_186))->BgL_argszd2retescapezd2);
		}

	}



/* &fun-args-retescape */
	obj_t BGl_z62funzd2argszd2retescapez62zzast_varz00(obj_t BgL_envz00_2741,
		obj_t BgL_oz00_2742)
	{
		{	/* Ast/var.sch 385 */
			return
				BGl_funzd2argszd2retescapez00zzast_varz00(
				((BgL_funz00_bglt) BgL_oz00_2742));
		}

	}



/* fun-args-retescape-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_funzd2argszd2retescapezd2setz12zc0zzast_varz00(BgL_funz00_bglt
		BgL_oz00_187, obj_t BgL_vz00_188)
	{
		{	/* Ast/var.sch 386 */
			return
				((((BgL_funz00_bglt) COBJECT(BgL_oz00_187))->BgL_argszd2retescapezd2) =
				((obj_t) BgL_vz00_188), BUNSPEC);
		}

	}



/* &fun-args-retescape-set! */
	obj_t BGl_z62funzd2argszd2retescapezd2setz12za2zzast_varz00(obj_t
		BgL_envz00_2743, obj_t BgL_oz00_2744, obj_t BgL_vz00_2745)
	{
		{	/* Ast/var.sch 386 */
			return
				BGl_funzd2argszd2retescapezd2setz12zc0zzast_varz00(
				((BgL_funz00_bglt) BgL_oz00_2744), BgL_vz00_2745);
		}

	}



/* fun-args-noescape */
	BGL_EXPORTED_DEF obj_t
		BGl_funzd2argszd2noescapez00zzast_varz00(BgL_funz00_bglt BgL_oz00_189)
	{
		{	/* Ast/var.sch 387 */
			return
				(((BgL_funz00_bglt) COBJECT(BgL_oz00_189))->BgL_argszd2noescapezd2);
		}

	}



/* &fun-args-noescape */
	obj_t BGl_z62funzd2argszd2noescapez62zzast_varz00(obj_t BgL_envz00_2746,
		obj_t BgL_oz00_2747)
	{
		{	/* Ast/var.sch 387 */
			return
				BGl_funzd2argszd2noescapez00zzast_varz00(
				((BgL_funz00_bglt) BgL_oz00_2747));
		}

	}



/* fun-args-noescape-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_funzd2argszd2noescapezd2setz12zc0zzast_varz00(BgL_funz00_bglt
		BgL_oz00_190, obj_t BgL_vz00_191)
	{
		{	/* Ast/var.sch 388 */
			return
				((((BgL_funz00_bglt) COBJECT(BgL_oz00_190))->BgL_argszd2noescapezd2) =
				((obj_t) BgL_vz00_191), BUNSPEC);
		}

	}



/* &fun-args-noescape-set! */
	obj_t BGl_z62funzd2argszd2noescapezd2setz12za2zzast_varz00(obj_t
		BgL_envz00_2748, obj_t BgL_oz00_2749, obj_t BgL_vz00_2750)
	{
		{	/* Ast/var.sch 388 */
			return
				BGl_funzd2argszd2noescapezd2setz12zc0zzast_varz00(
				((BgL_funz00_bglt) BgL_oz00_2749), BgL_vz00_2750);
		}

	}



/* fun-failsafe */
	BGL_EXPORTED_DEF obj_t BGl_funzd2failsafezd2zzast_varz00(BgL_funz00_bglt
		BgL_oz00_192)
	{
		{	/* Ast/var.sch 389 */
			return (((BgL_funz00_bglt) COBJECT(BgL_oz00_192))->BgL_failsafez00);
		}

	}



/* &fun-failsafe */
	obj_t BGl_z62funzd2failsafezb0zzast_varz00(obj_t BgL_envz00_2751,
		obj_t BgL_oz00_2752)
	{
		{	/* Ast/var.sch 389 */
			return
				BGl_funzd2failsafezd2zzast_varz00(((BgL_funz00_bglt) BgL_oz00_2752));
		}

	}



/* fun-failsafe-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_funzd2failsafezd2setz12z12zzast_varz00(BgL_funz00_bglt BgL_oz00_193,
		obj_t BgL_vz00_194)
	{
		{	/* Ast/var.sch 390 */
			return
				((((BgL_funz00_bglt) COBJECT(BgL_oz00_193))->BgL_failsafez00) =
				((obj_t) BgL_vz00_194), BUNSPEC);
		}

	}



/* &fun-failsafe-set! */
	obj_t BGl_z62funzd2failsafezd2setz12z70zzast_varz00(obj_t BgL_envz00_2753,
		obj_t BgL_oz00_2754, obj_t BgL_vz00_2755)
	{
		{	/* Ast/var.sch 390 */
			return
				BGl_funzd2failsafezd2setz12z12zzast_varz00(
				((BgL_funz00_bglt) BgL_oz00_2754), BgL_vz00_2755);
		}

	}



/* fun-effect */
	BGL_EXPORTED_DEF obj_t BGl_funzd2effectzd2zzast_varz00(BgL_funz00_bglt
		BgL_oz00_195)
	{
		{	/* Ast/var.sch 391 */
			return (((BgL_funz00_bglt) COBJECT(BgL_oz00_195))->BgL_effectz00);
		}

	}



/* &fun-effect */
	obj_t BGl_z62funzd2effectzb0zzast_varz00(obj_t BgL_envz00_2756,
		obj_t BgL_oz00_2757)
	{
		{	/* Ast/var.sch 391 */
			return BGl_funzd2effectzd2zzast_varz00(((BgL_funz00_bglt) BgL_oz00_2757));
		}

	}



/* fun-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_funzd2effectzd2setz12z12zzast_varz00(BgL_funz00_bglt BgL_oz00_196,
		obj_t BgL_vz00_197)
	{
		{	/* Ast/var.sch 392 */
			return
				((((BgL_funz00_bglt) COBJECT(BgL_oz00_196))->BgL_effectz00) =
				((obj_t) BgL_vz00_197), BUNSPEC);
		}

	}



/* &fun-effect-set! */
	obj_t BGl_z62funzd2effectzd2setz12z70zzast_varz00(obj_t BgL_envz00_2758,
		obj_t BgL_oz00_2759, obj_t BgL_vz00_2760)
	{
		{	/* Ast/var.sch 392 */
			return
				BGl_funzd2effectzd2setz12z12zzast_varz00(
				((BgL_funz00_bglt) BgL_oz00_2759), BgL_vz00_2760);
		}

	}



/* fun-the-closure */
	BGL_EXPORTED_DEF obj_t BGl_funzd2thezd2closurez00zzast_varz00(BgL_funz00_bglt
		BgL_oz00_198)
	{
		{	/* Ast/var.sch 393 */
			return (((BgL_funz00_bglt) COBJECT(BgL_oz00_198))->BgL_thezd2closurezd2);
		}

	}



/* &fun-the-closure */
	obj_t BGl_z62funzd2thezd2closurez62zzast_varz00(obj_t BgL_envz00_2761,
		obj_t BgL_oz00_2762)
	{
		{	/* Ast/var.sch 393 */
			return
				BGl_funzd2thezd2closurez00zzast_varz00(
				((BgL_funz00_bglt) BgL_oz00_2762));
		}

	}



/* fun-the-closure-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_funzd2thezd2closurezd2setz12zc0zzast_varz00(BgL_funz00_bglt
		BgL_oz00_199, obj_t BgL_vz00_200)
	{
		{	/* Ast/var.sch 394 */
			return
				((((BgL_funz00_bglt) COBJECT(BgL_oz00_199))->BgL_thezd2closurezd2) =
				((obj_t) BgL_vz00_200), BUNSPEC);
		}

	}



/* &fun-the-closure-set! */
	obj_t BGl_z62funzd2thezd2closurezd2setz12za2zzast_varz00(obj_t
		BgL_envz00_2763, obj_t BgL_oz00_2764, obj_t BgL_vz00_2765)
	{
		{	/* Ast/var.sch 394 */
			return
				BGl_funzd2thezd2closurezd2setz12zc0zzast_varz00(
				((BgL_funz00_bglt) BgL_oz00_2764), BgL_vz00_2765);
		}

	}



/* fun-top? */
	BGL_EXPORTED_DEF bool_t BGl_funzd2topzf3z21zzast_varz00(BgL_funz00_bglt
		BgL_oz00_201)
	{
		{	/* Ast/var.sch 395 */
			return (((BgL_funz00_bglt) COBJECT(BgL_oz00_201))->BgL_topzf3zf3);
		}

	}



/* &fun-top? */
	obj_t BGl_z62funzd2topzf3z43zzast_varz00(obj_t BgL_envz00_2766,
		obj_t BgL_oz00_2767)
	{
		{	/* Ast/var.sch 395 */
			return
				BBOOL(BGl_funzd2topzf3z21zzast_varz00(
					((BgL_funz00_bglt) BgL_oz00_2767)));
		}

	}



/* fun-top?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_funzd2topzf3zd2setz12ze1zzast_varz00(BgL_funz00_bglt BgL_oz00_202,
		bool_t BgL_vz00_203)
	{
		{	/* Ast/var.sch 396 */
			return
				((((BgL_funz00_bglt) COBJECT(BgL_oz00_202))->BgL_topzf3zf3) =
				((bool_t) BgL_vz00_203), BUNSPEC);
		}

	}



/* &fun-top?-set! */
	obj_t BGl_z62funzd2topzf3zd2setz12z83zzast_varz00(obj_t BgL_envz00_2768,
		obj_t BgL_oz00_2769, obj_t BgL_vz00_2770)
	{
		{	/* Ast/var.sch 396 */
			return
				BGl_funzd2topzf3zd2setz12ze1zzast_varz00(
				((BgL_funz00_bglt) BgL_oz00_2769), CBOOL(BgL_vz00_2770));
		}

	}



/* fun-stack-allocator */
	BGL_EXPORTED_DEF obj_t
		BGl_funzd2stackzd2allocatorz00zzast_varz00(BgL_funz00_bglt BgL_oz00_204)
	{
		{	/* Ast/var.sch 397 */
			return
				(((BgL_funz00_bglt) COBJECT(BgL_oz00_204))->BgL_stackzd2allocatorzd2);
		}

	}



/* &fun-stack-allocator */
	obj_t BGl_z62funzd2stackzd2allocatorz62zzast_varz00(obj_t BgL_envz00_2771,
		obj_t BgL_oz00_2772)
	{
		{	/* Ast/var.sch 397 */
			return
				BGl_funzd2stackzd2allocatorz00zzast_varz00(
				((BgL_funz00_bglt) BgL_oz00_2772));
		}

	}



/* fun-stack-allocator-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_funzd2stackzd2allocatorzd2setz12zc0zzast_varz00(BgL_funz00_bglt
		BgL_oz00_205, obj_t BgL_vz00_206)
	{
		{	/* Ast/var.sch 398 */
			return
				((((BgL_funz00_bglt) COBJECT(BgL_oz00_205))->BgL_stackzd2allocatorzd2) =
				((obj_t) BgL_vz00_206), BUNSPEC);
		}

	}



/* &fun-stack-allocator-set! */
	obj_t BGl_z62funzd2stackzd2allocatorzd2setz12za2zzast_varz00(obj_t
		BgL_envz00_2773, obj_t BgL_oz00_2774, obj_t BgL_vz00_2775)
	{
		{	/* Ast/var.sch 398 */
			return
				BGl_funzd2stackzd2allocatorzd2setz12zc0zzast_varz00(
				((BgL_funz00_bglt) BgL_oz00_2774), BgL_vz00_2775);
		}

	}



/* fun-predicate-of */
	BGL_EXPORTED_DEF obj_t BGl_funzd2predicatezd2ofz00zzast_varz00(BgL_funz00_bglt
		BgL_oz00_207)
	{
		{	/* Ast/var.sch 399 */
			return (((BgL_funz00_bglt) COBJECT(BgL_oz00_207))->BgL_predicatezd2ofzd2);
		}

	}



/* &fun-predicate-of */
	obj_t BGl_z62funzd2predicatezd2ofz62zzast_varz00(obj_t BgL_envz00_2776,
		obj_t BgL_oz00_2777)
	{
		{	/* Ast/var.sch 399 */
			return
				BGl_funzd2predicatezd2ofz00zzast_varz00(
				((BgL_funz00_bglt) BgL_oz00_2777));
		}

	}



/* fun-predicate-of-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_funzd2predicatezd2ofzd2setz12zc0zzast_varz00(BgL_funz00_bglt
		BgL_oz00_208, obj_t BgL_vz00_209)
	{
		{	/* Ast/var.sch 400 */
			return
				((((BgL_funz00_bglt) COBJECT(BgL_oz00_208))->BgL_predicatezd2ofzd2) =
				((obj_t) BgL_vz00_209), BUNSPEC);
		}

	}



/* &fun-predicate-of-set! */
	obj_t BGl_z62funzd2predicatezd2ofzd2setz12za2zzast_varz00(obj_t
		BgL_envz00_2778, obj_t BgL_oz00_2779, obj_t BgL_vz00_2780)
	{
		{	/* Ast/var.sch 400 */
			return
				BGl_funzd2predicatezd2ofzd2setz12zc0zzast_varz00(
				((BgL_funz00_bglt) BgL_oz00_2779), BgL_vz00_2780);
		}

	}



/* fun-side-effect */
	BGL_EXPORTED_DEF obj_t BGl_funzd2sidezd2effectz00zzast_varz00(BgL_funz00_bglt
		BgL_oz00_210)
	{
		{	/* Ast/var.sch 401 */
			return (((BgL_funz00_bglt) COBJECT(BgL_oz00_210))->BgL_sidezd2effectzd2);
		}

	}



/* &fun-side-effect */
	obj_t BGl_z62funzd2sidezd2effectz62zzast_varz00(obj_t BgL_envz00_2781,
		obj_t BgL_oz00_2782)
	{
		{	/* Ast/var.sch 401 */
			return
				BGl_funzd2sidezd2effectz00zzast_varz00(
				((BgL_funz00_bglt) BgL_oz00_2782));
		}

	}



/* fun-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_funzd2sidezd2effectzd2setz12zc0zzast_varz00(BgL_funz00_bglt
		BgL_oz00_211, obj_t BgL_vz00_212)
	{
		{	/* Ast/var.sch 402 */
			return
				((((BgL_funz00_bglt) COBJECT(BgL_oz00_211))->BgL_sidezd2effectzd2) =
				((obj_t) BgL_vz00_212), BUNSPEC);
		}

	}



/* &fun-side-effect-set! */
	obj_t BGl_z62funzd2sidezd2effectzd2setz12za2zzast_varz00(obj_t
		BgL_envz00_2783, obj_t BgL_oz00_2784, obj_t BgL_vz00_2785)
	{
		{	/* Ast/var.sch 402 */
			return
				BGl_funzd2sidezd2effectzd2setz12zc0zzast_varz00(
				((BgL_funz00_bglt) BgL_oz00_2784), BgL_vz00_2785);
		}

	}



/* fun-arity */
	BGL_EXPORTED_DEF long BGl_funzd2arityzd2zzast_varz00(BgL_funz00_bglt
		BgL_oz00_213)
	{
		{	/* Ast/var.sch 403 */
			return (((BgL_funz00_bglt) COBJECT(BgL_oz00_213))->BgL_arityz00);
		}

	}



/* &fun-arity */
	obj_t BGl_z62funzd2arityzb0zzast_varz00(obj_t BgL_envz00_2786,
		obj_t BgL_oz00_2787)
	{
		{	/* Ast/var.sch 403 */
			return
				BINT(BGl_funzd2arityzd2zzast_varz00(((BgL_funz00_bglt) BgL_oz00_2787)));
		}

	}



/* make-sfun */
	BGL_EXPORTED_DEF BgL_sfunz00_bglt BGl_makezd2sfunzd2zzast_varz00(long
		BgL_arity1214z00_216, obj_t BgL_sidezd2effect1215zd2_217,
		obj_t BgL_predicatezd2of1216zd2_218, obj_t BgL_stackzd2allocator1217zd2_219,
		bool_t BgL_topzf31218zf3_220, obj_t BgL_thezd2closure1219zd2_221,
		obj_t BgL_effect1220z00_222, obj_t BgL_failsafe1221z00_223,
		obj_t BgL_argszd2noescape1222zd2_224, obj_t BgL_argszd2retescape1223zd2_225,
		obj_t BgL_property1224z00_226, obj_t BgL_args1225z00_227,
		obj_t BgL_argszd2name1226zd2_228, obj_t BgL_body1227z00_229,
		obj_t BgL_class1228z00_230, obj_t BgL_dssslzd2keywords1229zd2_231,
		obj_t BgL_loc1230z00_232, obj_t BgL_optionals1231z00_233,
		obj_t BgL_keys1232z00_234, obj_t BgL_thezd2closurezd2global1233z00_235,
		obj_t BgL_strength1234z00_236, obj_t BgL_stackable1235z00_237)
	{
		{	/* Ast/var.sch 407 */
			{	/* Ast/var.sch 407 */
				BgL_sfunz00_bglt BgL_new1195z00_3844;

				{	/* Ast/var.sch 407 */
					BgL_sfunz00_bglt BgL_new1194z00_3845;

					BgL_new1194z00_3845 =
						((BgL_sfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_sfunz00_bgl))));
					{	/* Ast/var.sch 407 */
						long BgL_arg1311z00_3846;

						BgL_arg1311z00_3846 = BGL_CLASS_NUM(BGl_sfunz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1194z00_3845), BgL_arg1311z00_3846);
					}
					{	/* Ast/var.sch 407 */
						BgL_objectz00_bglt BgL_tmpz00_4914;

						BgL_tmpz00_4914 = ((BgL_objectz00_bglt) BgL_new1194z00_3845);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4914, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1194z00_3845);
					BgL_new1195z00_3844 = BgL_new1194z00_3845;
				}
				((((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt) BgL_new1195z00_3844)))->BgL_arityz00) =
					((long) BgL_arity1214z00_216), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1195z00_3844)))->
						BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1215zd2_217), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1195z00_3844)))->
						BgL_predicatezd2ofzd2) =
					((obj_t) BgL_predicatezd2of1216zd2_218), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1195z00_3844)))->
						BgL_stackzd2allocatorzd2) =
					((obj_t) BgL_stackzd2allocator1217zd2_219), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1195z00_3844)))->
						BgL_topzf3zf3) = ((bool_t) BgL_topzf31218zf3_220), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1195z00_3844)))->
						BgL_thezd2closurezd2) =
					((obj_t) BgL_thezd2closure1219zd2_221), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1195z00_3844)))->
						BgL_effectz00) = ((obj_t) BgL_effect1220z00_222), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1195z00_3844)))->
						BgL_failsafez00) = ((obj_t) BgL_failsafe1221z00_223), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1195z00_3844)))->
						BgL_argszd2noescapezd2) =
					((obj_t) BgL_argszd2noescape1222zd2_224), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1195z00_3844)))->
						BgL_argszd2retescapezd2) =
					((obj_t) BgL_argszd2retescape1223zd2_225), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(BgL_new1195z00_3844))->BgL_propertyz00) =
					((obj_t) BgL_property1224z00_226), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(BgL_new1195z00_3844))->BgL_argsz00) =
					((obj_t) BgL_args1225z00_227), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(BgL_new1195z00_3844))->
						BgL_argszd2namezd2) =
					((obj_t) BgL_argszd2name1226zd2_228), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(BgL_new1195z00_3844))->BgL_bodyz00) =
					((obj_t) BgL_body1227z00_229), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(BgL_new1195z00_3844))->BgL_classz00) =
					((obj_t) BgL_class1228z00_230), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(BgL_new1195z00_3844))->
						BgL_dssslzd2keywordszd2) =
					((obj_t) BgL_dssslzd2keywords1229zd2_231), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(BgL_new1195z00_3844))->BgL_locz00) =
					((obj_t) BgL_loc1230z00_232), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(BgL_new1195z00_3844))->BgL_optionalsz00) =
					((obj_t) BgL_optionals1231z00_233), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(BgL_new1195z00_3844))->BgL_keysz00) =
					((obj_t) BgL_keys1232z00_234), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(BgL_new1195z00_3844))->
						BgL_thezd2closurezd2globalz00) =
					((obj_t) BgL_thezd2closurezd2global1233z00_235), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(BgL_new1195z00_3844))->BgL_strengthz00) =
					((obj_t) BgL_strength1234z00_236), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(BgL_new1195z00_3844))->BgL_stackablez00) =
					((obj_t) BgL_stackable1235z00_237), BUNSPEC);
				return BgL_new1195z00_3844;
			}
		}

	}



/* &make-sfun */
	BgL_sfunz00_bglt BGl_z62makezd2sfunzb0zzast_varz00(obj_t BgL_envz00_2788,
		obj_t BgL_arity1214z00_2789, obj_t BgL_sidezd2effect1215zd2_2790,
		obj_t BgL_predicatezd2of1216zd2_2791,
		obj_t BgL_stackzd2allocator1217zd2_2792, obj_t BgL_topzf31218zf3_2793,
		obj_t BgL_thezd2closure1219zd2_2794, obj_t BgL_effect1220z00_2795,
		obj_t BgL_failsafe1221z00_2796, obj_t BgL_argszd2noescape1222zd2_2797,
		obj_t BgL_argszd2retescape1223zd2_2798, obj_t BgL_property1224z00_2799,
		obj_t BgL_args1225z00_2800, obj_t BgL_argszd2name1226zd2_2801,
		obj_t BgL_body1227z00_2802, obj_t BgL_class1228z00_2803,
		obj_t BgL_dssslzd2keywords1229zd2_2804, obj_t BgL_loc1230z00_2805,
		obj_t BgL_optionals1231z00_2806, obj_t BgL_keys1232z00_2807,
		obj_t BgL_thezd2closurezd2global1233z00_2808,
		obj_t BgL_strength1234z00_2809, obj_t BgL_stackable1235z00_2810)
	{
		{	/* Ast/var.sch 407 */
			return
				BGl_makezd2sfunzd2zzast_varz00(
				(long) CINT(BgL_arity1214z00_2789), BgL_sidezd2effect1215zd2_2790,
				BgL_predicatezd2of1216zd2_2791, BgL_stackzd2allocator1217zd2_2792,
				CBOOL(BgL_topzf31218zf3_2793), BgL_thezd2closure1219zd2_2794,
				BgL_effect1220z00_2795, BgL_failsafe1221z00_2796,
				BgL_argszd2noescape1222zd2_2797, BgL_argszd2retescape1223zd2_2798,
				BgL_property1224z00_2799, BgL_args1225z00_2800,
				BgL_argszd2name1226zd2_2801, BgL_body1227z00_2802,
				BgL_class1228z00_2803, BgL_dssslzd2keywords1229zd2_2804,
				BgL_loc1230z00_2805, BgL_optionals1231z00_2806, BgL_keys1232z00_2807,
				BgL_thezd2closurezd2global1233z00_2808, BgL_strength1234z00_2809,
				BgL_stackable1235z00_2810);
		}

	}



/* sfun? */
	BGL_EXPORTED_DEF bool_t BGl_sfunzf3zf3zzast_varz00(obj_t BgL_objz00_238)
	{
		{	/* Ast/var.sch 408 */
			{	/* Ast/var.sch 408 */
				obj_t BgL_classz00_3847;

				BgL_classz00_3847 = BGl_sfunz00zzast_varz00;
				if (BGL_OBJECTP(BgL_objz00_238))
					{	/* Ast/var.sch 408 */
						BgL_objectz00_bglt BgL_arg1807z00_3848;

						BgL_arg1807z00_3848 = (BgL_objectz00_bglt) (BgL_objz00_238);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Ast/var.sch 408 */
								long BgL_idxz00_3849;

								BgL_idxz00_3849 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3848);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3849 + 3L)) == BgL_classz00_3847);
							}
						else
							{	/* Ast/var.sch 408 */
								bool_t BgL_res2167z00_3852;

								{	/* Ast/var.sch 408 */
									obj_t BgL_oclassz00_3853;

									{	/* Ast/var.sch 408 */
										obj_t BgL_arg1815z00_3854;
										long BgL_arg1816z00_3855;

										BgL_arg1815z00_3854 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Ast/var.sch 408 */
											long BgL_arg1817z00_3856;

											BgL_arg1817z00_3856 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3848);
											BgL_arg1816z00_3855 = (BgL_arg1817z00_3856 - OBJECT_TYPE);
										}
										BgL_oclassz00_3853 =
											VECTOR_REF(BgL_arg1815z00_3854, BgL_arg1816z00_3855);
									}
									{	/* Ast/var.sch 408 */
										bool_t BgL__ortest_1115z00_3857;

										BgL__ortest_1115z00_3857 =
											(BgL_classz00_3847 == BgL_oclassz00_3853);
										if (BgL__ortest_1115z00_3857)
											{	/* Ast/var.sch 408 */
												BgL_res2167z00_3852 = BgL__ortest_1115z00_3857;
											}
										else
											{	/* Ast/var.sch 408 */
												long BgL_odepthz00_3858;

												{	/* Ast/var.sch 408 */
													obj_t BgL_arg1804z00_3859;

													BgL_arg1804z00_3859 = (BgL_oclassz00_3853);
													BgL_odepthz00_3858 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3859);
												}
												if ((3L < BgL_odepthz00_3858))
													{	/* Ast/var.sch 408 */
														obj_t BgL_arg1802z00_3860;

														{	/* Ast/var.sch 408 */
															obj_t BgL_arg1803z00_3861;

															BgL_arg1803z00_3861 = (BgL_oclassz00_3853);
															BgL_arg1802z00_3860 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3861,
																3L);
														}
														BgL_res2167z00_3852 =
															(BgL_arg1802z00_3860 == BgL_classz00_3847);
													}
												else
													{	/* Ast/var.sch 408 */
														BgL_res2167z00_3852 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2167z00_3852;
							}
					}
				else
					{	/* Ast/var.sch 408 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &sfun? */
	obj_t BGl_z62sfunzf3z91zzast_varz00(obj_t BgL_envz00_2811,
		obj_t BgL_objz00_2812)
	{
		{	/* Ast/var.sch 408 */
			return BBOOL(BGl_sfunzf3zf3zzast_varz00(BgL_objz00_2812));
		}

	}



/* sfun-nil */
	BGL_EXPORTED_DEF BgL_sfunz00_bglt BGl_sfunzd2nilzd2zzast_varz00(void)
	{
		{	/* Ast/var.sch 409 */
			{	/* Ast/var.sch 409 */
				obj_t BgL_classz00_1826;

				BgL_classz00_1826 = BGl_sfunz00zzast_varz00;
				{	/* Ast/var.sch 409 */
					obj_t BgL__ortest_1117z00_1827;

					BgL__ortest_1117z00_1827 = BGL_CLASS_NIL(BgL_classz00_1826);
					if (CBOOL(BgL__ortest_1117z00_1827))
						{	/* Ast/var.sch 409 */
							return ((BgL_sfunz00_bglt) BgL__ortest_1117z00_1827);
						}
					else
						{	/* Ast/var.sch 409 */
							return
								((BgL_sfunz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1826));
						}
				}
			}
		}

	}



/* &sfun-nil */
	BgL_sfunz00_bglt BGl_z62sfunzd2nilzb0zzast_varz00(obj_t BgL_envz00_2813)
	{
		{	/* Ast/var.sch 409 */
			return BGl_sfunzd2nilzd2zzast_varz00();
		}

	}



/* sfun-stackable */
	BGL_EXPORTED_DEF obj_t BGl_sfunzd2stackablezd2zzast_varz00(BgL_sfunz00_bglt
		BgL_oz00_239)
	{
		{	/* Ast/var.sch 410 */
			return (((BgL_sfunz00_bglt) COBJECT(BgL_oz00_239))->BgL_stackablez00);
		}

	}



/* &sfun-stackable */
	obj_t BGl_z62sfunzd2stackablezb0zzast_varz00(obj_t BgL_envz00_2814,
		obj_t BgL_oz00_2815)
	{
		{	/* Ast/var.sch 410 */
			return
				BGl_sfunzd2stackablezd2zzast_varz00(((BgL_sfunz00_bglt) BgL_oz00_2815));
		}

	}



/* sfun-stackable-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzd2stackablezd2setz12z12zzast_varz00(BgL_sfunz00_bglt BgL_oz00_240,
		obj_t BgL_vz00_241)
	{
		{	/* Ast/var.sch 411 */
			return
				((((BgL_sfunz00_bglt) COBJECT(BgL_oz00_240))->BgL_stackablez00) =
				((obj_t) BgL_vz00_241), BUNSPEC);
		}

	}



/* &sfun-stackable-set! */
	obj_t BGl_z62sfunzd2stackablezd2setz12z70zzast_varz00(obj_t BgL_envz00_2816,
		obj_t BgL_oz00_2817, obj_t BgL_vz00_2818)
	{
		{	/* Ast/var.sch 411 */
			return
				BGl_sfunzd2stackablezd2setz12z12zzast_varz00(
				((BgL_sfunz00_bglt) BgL_oz00_2817), BgL_vz00_2818);
		}

	}



/* sfun-strength */
	BGL_EXPORTED_DEF obj_t BGl_sfunzd2strengthzd2zzast_varz00(BgL_sfunz00_bglt
		BgL_oz00_242)
	{
		{	/* Ast/var.sch 412 */
			return (((BgL_sfunz00_bglt) COBJECT(BgL_oz00_242))->BgL_strengthz00);
		}

	}



/* &sfun-strength */
	obj_t BGl_z62sfunzd2strengthzb0zzast_varz00(obj_t BgL_envz00_2819,
		obj_t BgL_oz00_2820)
	{
		{	/* Ast/var.sch 412 */
			return
				BGl_sfunzd2strengthzd2zzast_varz00(((BgL_sfunz00_bglt) BgL_oz00_2820));
		}

	}



/* sfun-strength-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzd2strengthzd2setz12z12zzast_varz00(BgL_sfunz00_bglt BgL_oz00_243,
		obj_t BgL_vz00_244)
	{
		{	/* Ast/var.sch 413 */
			return
				((((BgL_sfunz00_bglt) COBJECT(BgL_oz00_243))->BgL_strengthz00) =
				((obj_t) BgL_vz00_244), BUNSPEC);
		}

	}



/* &sfun-strength-set! */
	obj_t BGl_z62sfunzd2strengthzd2setz12z70zzast_varz00(obj_t BgL_envz00_2821,
		obj_t BgL_oz00_2822, obj_t BgL_vz00_2823)
	{
		{	/* Ast/var.sch 413 */
			return
				BGl_sfunzd2strengthzd2setz12z12zzast_varz00(
				((BgL_sfunz00_bglt) BgL_oz00_2822), BgL_vz00_2823);
		}

	}



/* sfun-the-closure-global */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzd2thezd2closurezd2globalzd2zzast_varz00(BgL_sfunz00_bglt
		BgL_oz00_245)
	{
		{	/* Ast/var.sch 414 */
			return
				(((BgL_sfunz00_bglt) COBJECT(BgL_oz00_245))->
				BgL_thezd2closurezd2globalz00);
		}

	}



/* &sfun-the-closure-global */
	obj_t BGl_z62sfunzd2thezd2closurezd2globalzb0zzast_varz00(obj_t
		BgL_envz00_2824, obj_t BgL_oz00_2825)
	{
		{	/* Ast/var.sch 414 */
			return
				BGl_sfunzd2thezd2closurezd2globalzd2zzast_varz00(
				((BgL_sfunz00_bglt) BgL_oz00_2825));
		}

	}



/* sfun-the-closure-global-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzd2thezd2closurezd2globalzd2setz12z12zzast_varz00(BgL_sfunz00_bglt
		BgL_oz00_246, obj_t BgL_vz00_247)
	{
		{	/* Ast/var.sch 415 */
			return
				((((BgL_sfunz00_bglt) COBJECT(BgL_oz00_246))->
					BgL_thezd2closurezd2globalz00) = ((obj_t) BgL_vz00_247), BUNSPEC);
		}

	}



/* &sfun-the-closure-global-set! */
	obj_t BGl_z62sfunzd2thezd2closurezd2globalzd2setz12z70zzast_varz00(obj_t
		BgL_envz00_2826, obj_t BgL_oz00_2827, obj_t BgL_vz00_2828)
	{
		{	/* Ast/var.sch 415 */
			return
				BGl_sfunzd2thezd2closurezd2globalzd2setz12z12zzast_varz00(
				((BgL_sfunz00_bglt) BgL_oz00_2827), BgL_vz00_2828);
		}

	}



/* sfun-keys */
	BGL_EXPORTED_DEF obj_t BGl_sfunzd2keyszd2zzast_varz00(BgL_sfunz00_bglt
		BgL_oz00_248)
	{
		{	/* Ast/var.sch 416 */
			return (((BgL_sfunz00_bglt) COBJECT(BgL_oz00_248))->BgL_keysz00);
		}

	}



/* &sfun-keys */
	obj_t BGl_z62sfunzd2keyszb0zzast_varz00(obj_t BgL_envz00_2829,
		obj_t BgL_oz00_2830)
	{
		{	/* Ast/var.sch 416 */
			return BGl_sfunzd2keyszd2zzast_varz00(((BgL_sfunz00_bglt) BgL_oz00_2830));
		}

	}



/* sfun-optionals */
	BGL_EXPORTED_DEF obj_t BGl_sfunzd2optionalszd2zzast_varz00(BgL_sfunz00_bglt
		BgL_oz00_251)
	{
		{	/* Ast/var.sch 418 */
			return (((BgL_sfunz00_bglt) COBJECT(BgL_oz00_251))->BgL_optionalsz00);
		}

	}



/* &sfun-optionals */
	obj_t BGl_z62sfunzd2optionalszb0zzast_varz00(obj_t BgL_envz00_2831,
		obj_t BgL_oz00_2832)
	{
		{	/* Ast/var.sch 418 */
			return
				BGl_sfunzd2optionalszd2zzast_varz00(((BgL_sfunz00_bglt) BgL_oz00_2832));
		}

	}



/* sfun-loc */
	BGL_EXPORTED_DEF obj_t BGl_sfunzd2loczd2zzast_varz00(BgL_sfunz00_bglt
		BgL_oz00_254)
	{
		{	/* Ast/var.sch 420 */
			return (((BgL_sfunz00_bglt) COBJECT(BgL_oz00_254))->BgL_locz00);
		}

	}



/* &sfun-loc */
	obj_t BGl_z62sfunzd2loczb0zzast_varz00(obj_t BgL_envz00_2833,
		obj_t BgL_oz00_2834)
	{
		{	/* Ast/var.sch 420 */
			return BGl_sfunzd2loczd2zzast_varz00(((BgL_sfunz00_bglt) BgL_oz00_2834));
		}

	}



/* sfun-loc-set! */
	BGL_EXPORTED_DEF obj_t BGl_sfunzd2loczd2setz12z12zzast_varz00(BgL_sfunz00_bglt
		BgL_oz00_255, obj_t BgL_vz00_256)
	{
		{	/* Ast/var.sch 421 */
			return
				((((BgL_sfunz00_bglt) COBJECT(BgL_oz00_255))->BgL_locz00) =
				((obj_t) BgL_vz00_256), BUNSPEC);
		}

	}



/* &sfun-loc-set! */
	obj_t BGl_z62sfunzd2loczd2setz12z70zzast_varz00(obj_t BgL_envz00_2835,
		obj_t BgL_oz00_2836, obj_t BgL_vz00_2837)
	{
		{	/* Ast/var.sch 421 */
			return
				BGl_sfunzd2loczd2setz12z12zzast_varz00(
				((BgL_sfunz00_bglt) BgL_oz00_2836), BgL_vz00_2837);
		}

	}



/* sfun-dsssl-keywords */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzd2dssslzd2keywordsz00zzast_varz00(BgL_sfunz00_bglt BgL_oz00_257)
	{
		{	/* Ast/var.sch 422 */
			return
				(((BgL_sfunz00_bglt) COBJECT(BgL_oz00_257))->BgL_dssslzd2keywordszd2);
		}

	}



/* &sfun-dsssl-keywords */
	obj_t BGl_z62sfunzd2dssslzd2keywordsz62zzast_varz00(obj_t BgL_envz00_2838,
		obj_t BgL_oz00_2839)
	{
		{	/* Ast/var.sch 422 */
			return
				BGl_sfunzd2dssslzd2keywordsz00zzast_varz00(
				((BgL_sfunz00_bglt) BgL_oz00_2839));
		}

	}



/* sfun-dsssl-keywords-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzd2dssslzd2keywordszd2setz12zc0zzast_varz00(BgL_sfunz00_bglt
		BgL_oz00_258, obj_t BgL_vz00_259)
	{
		{	/* Ast/var.sch 423 */
			return
				((((BgL_sfunz00_bglt) COBJECT(BgL_oz00_258))->BgL_dssslzd2keywordszd2) =
				((obj_t) BgL_vz00_259), BUNSPEC);
		}

	}



/* &sfun-dsssl-keywords-set! */
	obj_t BGl_z62sfunzd2dssslzd2keywordszd2setz12za2zzast_varz00(obj_t
		BgL_envz00_2840, obj_t BgL_oz00_2841, obj_t BgL_vz00_2842)
	{
		{	/* Ast/var.sch 423 */
			return
				BGl_sfunzd2dssslzd2keywordszd2setz12zc0zzast_varz00(
				((BgL_sfunz00_bglt) BgL_oz00_2841), BgL_vz00_2842);
		}

	}



/* sfun-class */
	BGL_EXPORTED_DEF obj_t BGl_sfunzd2classzd2zzast_varz00(BgL_sfunz00_bglt
		BgL_oz00_260)
	{
		{	/* Ast/var.sch 424 */
			return (((BgL_sfunz00_bglt) COBJECT(BgL_oz00_260))->BgL_classz00);
		}

	}



/* &sfun-class */
	obj_t BGl_z62sfunzd2classzb0zzast_varz00(obj_t BgL_envz00_2843,
		obj_t BgL_oz00_2844)
	{
		{	/* Ast/var.sch 424 */
			return
				BGl_sfunzd2classzd2zzast_varz00(((BgL_sfunz00_bglt) BgL_oz00_2844));
		}

	}



/* sfun-class-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzd2classzd2setz12z12zzast_varz00(BgL_sfunz00_bglt BgL_oz00_261,
		obj_t BgL_vz00_262)
	{
		{	/* Ast/var.sch 425 */
			return
				((((BgL_sfunz00_bglt) COBJECT(BgL_oz00_261))->BgL_classz00) =
				((obj_t) BgL_vz00_262), BUNSPEC);
		}

	}



/* &sfun-class-set! */
	obj_t BGl_z62sfunzd2classzd2setz12z70zzast_varz00(obj_t BgL_envz00_2845,
		obj_t BgL_oz00_2846, obj_t BgL_vz00_2847)
	{
		{	/* Ast/var.sch 425 */
			return
				BGl_sfunzd2classzd2setz12z12zzast_varz00(
				((BgL_sfunz00_bglt) BgL_oz00_2846), BgL_vz00_2847);
		}

	}



/* sfun-body */
	BGL_EXPORTED_DEF obj_t BGl_sfunzd2bodyzd2zzast_varz00(BgL_sfunz00_bglt
		BgL_oz00_263)
	{
		{	/* Ast/var.sch 426 */
			return (((BgL_sfunz00_bglt) COBJECT(BgL_oz00_263))->BgL_bodyz00);
		}

	}



/* &sfun-body */
	obj_t BGl_z62sfunzd2bodyzb0zzast_varz00(obj_t BgL_envz00_2848,
		obj_t BgL_oz00_2849)
	{
		{	/* Ast/var.sch 426 */
			return BGl_sfunzd2bodyzd2zzast_varz00(((BgL_sfunz00_bglt) BgL_oz00_2849));
		}

	}



/* sfun-body-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzd2bodyzd2setz12z12zzast_varz00(BgL_sfunz00_bglt BgL_oz00_264,
		obj_t BgL_vz00_265)
	{
		{	/* Ast/var.sch 427 */
			return
				((((BgL_sfunz00_bglt) COBJECT(BgL_oz00_264))->BgL_bodyz00) =
				((obj_t) BgL_vz00_265), BUNSPEC);
		}

	}



/* &sfun-body-set! */
	obj_t BGl_z62sfunzd2bodyzd2setz12z70zzast_varz00(obj_t BgL_envz00_2850,
		obj_t BgL_oz00_2851, obj_t BgL_vz00_2852)
	{
		{	/* Ast/var.sch 427 */
			return
				BGl_sfunzd2bodyzd2setz12z12zzast_varz00(
				((BgL_sfunz00_bglt) BgL_oz00_2851), BgL_vz00_2852);
		}

	}



/* sfun-args-name */
	BGL_EXPORTED_DEF obj_t BGl_sfunzd2argszd2namez00zzast_varz00(BgL_sfunz00_bglt
		BgL_oz00_266)
	{
		{	/* Ast/var.sch 428 */
			return (((BgL_sfunz00_bglt) COBJECT(BgL_oz00_266))->BgL_argszd2namezd2);
		}

	}



/* &sfun-args-name */
	obj_t BGl_z62sfunzd2argszd2namez62zzast_varz00(obj_t BgL_envz00_2853,
		obj_t BgL_oz00_2854)
	{
		{	/* Ast/var.sch 428 */
			return
				BGl_sfunzd2argszd2namez00zzast_varz00(
				((BgL_sfunz00_bglt) BgL_oz00_2854));
		}

	}



/* sfun-args */
	BGL_EXPORTED_DEF obj_t BGl_sfunzd2argszd2zzast_varz00(BgL_sfunz00_bglt
		BgL_oz00_269)
	{
		{	/* Ast/var.sch 430 */
			return (((BgL_sfunz00_bglt) COBJECT(BgL_oz00_269))->BgL_argsz00);
		}

	}



/* &sfun-args */
	obj_t BGl_z62sfunzd2argszb0zzast_varz00(obj_t BgL_envz00_2855,
		obj_t BgL_oz00_2856)
	{
		{	/* Ast/var.sch 430 */
			return BGl_sfunzd2argszd2zzast_varz00(((BgL_sfunz00_bglt) BgL_oz00_2856));
		}

	}



/* sfun-args-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzd2argszd2setz12z12zzast_varz00(BgL_sfunz00_bglt BgL_oz00_270,
		obj_t BgL_vz00_271)
	{
		{	/* Ast/var.sch 431 */
			return
				((((BgL_sfunz00_bglt) COBJECT(BgL_oz00_270))->BgL_argsz00) =
				((obj_t) BgL_vz00_271), BUNSPEC);
		}

	}



/* &sfun-args-set! */
	obj_t BGl_z62sfunzd2argszd2setz12z70zzast_varz00(obj_t BgL_envz00_2857,
		obj_t BgL_oz00_2858, obj_t BgL_vz00_2859)
	{
		{	/* Ast/var.sch 431 */
			return
				BGl_sfunzd2argszd2setz12z12zzast_varz00(
				((BgL_sfunz00_bglt) BgL_oz00_2858), BgL_vz00_2859);
		}

	}



/* sfun-property */
	BGL_EXPORTED_DEF obj_t BGl_sfunzd2propertyzd2zzast_varz00(BgL_sfunz00_bglt
		BgL_oz00_272)
	{
		{	/* Ast/var.sch 432 */
			return (((BgL_sfunz00_bglt) COBJECT(BgL_oz00_272))->BgL_propertyz00);
		}

	}



/* &sfun-property */
	obj_t BGl_z62sfunzd2propertyzb0zzast_varz00(obj_t BgL_envz00_2860,
		obj_t BgL_oz00_2861)
	{
		{	/* Ast/var.sch 432 */
			return
				BGl_sfunzd2propertyzd2zzast_varz00(((BgL_sfunz00_bglt) BgL_oz00_2861));
		}

	}



/* sfun-property-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzd2propertyzd2setz12z12zzast_varz00(BgL_sfunz00_bglt BgL_oz00_273,
		obj_t BgL_vz00_274)
	{
		{	/* Ast/var.sch 433 */
			return
				((((BgL_sfunz00_bglt) COBJECT(BgL_oz00_273))->BgL_propertyz00) =
				((obj_t) BgL_vz00_274), BUNSPEC);
		}

	}



/* &sfun-property-set! */
	obj_t BGl_z62sfunzd2propertyzd2setz12z70zzast_varz00(obj_t BgL_envz00_2862,
		obj_t BgL_oz00_2863, obj_t BgL_vz00_2864)
	{
		{	/* Ast/var.sch 433 */
			return
				BGl_sfunzd2propertyzd2setz12z12zzast_varz00(
				((BgL_sfunz00_bglt) BgL_oz00_2863), BgL_vz00_2864);
		}

	}



/* sfun-args-retescape */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzd2argszd2retescapez00zzast_varz00(BgL_sfunz00_bglt BgL_oz00_275)
	{
		{	/* Ast/var.sch 434 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_275)))->BgL_argszd2retescapezd2);
		}

	}



/* &sfun-args-retescape */
	obj_t BGl_z62sfunzd2argszd2retescapez62zzast_varz00(obj_t BgL_envz00_2865,
		obj_t BgL_oz00_2866)
	{
		{	/* Ast/var.sch 434 */
			return
				BGl_sfunzd2argszd2retescapez00zzast_varz00(
				((BgL_sfunz00_bglt) BgL_oz00_2866));
		}

	}



/* sfun-args-retescape-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzd2argszd2retescapezd2setz12zc0zzast_varz00(BgL_sfunz00_bglt
		BgL_oz00_276, obj_t BgL_vz00_277)
	{
		{	/* Ast/var.sch 435 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_276)))->BgL_argszd2retescapezd2) =
				((obj_t) BgL_vz00_277), BUNSPEC);
		}

	}



/* &sfun-args-retescape-set! */
	obj_t BGl_z62sfunzd2argszd2retescapezd2setz12za2zzast_varz00(obj_t
		BgL_envz00_2867, obj_t BgL_oz00_2868, obj_t BgL_vz00_2869)
	{
		{	/* Ast/var.sch 435 */
			return
				BGl_sfunzd2argszd2retescapezd2setz12zc0zzast_varz00(
				((BgL_sfunz00_bglt) BgL_oz00_2868), BgL_vz00_2869);
		}

	}



/* sfun-args-noescape */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzd2argszd2noescapez00zzast_varz00(BgL_sfunz00_bglt BgL_oz00_278)
	{
		{	/* Ast/var.sch 436 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_278)))->BgL_argszd2noescapezd2);
		}

	}



/* &sfun-args-noescape */
	obj_t BGl_z62sfunzd2argszd2noescapez62zzast_varz00(obj_t BgL_envz00_2870,
		obj_t BgL_oz00_2871)
	{
		{	/* Ast/var.sch 436 */
			return
				BGl_sfunzd2argszd2noescapez00zzast_varz00(
				((BgL_sfunz00_bglt) BgL_oz00_2871));
		}

	}



/* sfun-args-noescape-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzd2argszd2noescapezd2setz12zc0zzast_varz00(BgL_sfunz00_bglt
		BgL_oz00_279, obj_t BgL_vz00_280)
	{
		{	/* Ast/var.sch 437 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_279)))->BgL_argszd2noescapezd2) =
				((obj_t) BgL_vz00_280), BUNSPEC);
		}

	}



/* &sfun-args-noescape-set! */
	obj_t BGl_z62sfunzd2argszd2noescapezd2setz12za2zzast_varz00(obj_t
		BgL_envz00_2872, obj_t BgL_oz00_2873, obj_t BgL_vz00_2874)
	{
		{	/* Ast/var.sch 437 */
			return
				BGl_sfunzd2argszd2noescapezd2setz12zc0zzast_varz00(
				((BgL_sfunz00_bglt) BgL_oz00_2873), BgL_vz00_2874);
		}

	}



/* sfun-failsafe */
	BGL_EXPORTED_DEF obj_t BGl_sfunzd2failsafezd2zzast_varz00(BgL_sfunz00_bglt
		BgL_oz00_281)
	{
		{	/* Ast/var.sch 438 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_281)))->BgL_failsafez00);
		}

	}



/* &sfun-failsafe */
	obj_t BGl_z62sfunzd2failsafezb0zzast_varz00(obj_t BgL_envz00_2875,
		obj_t BgL_oz00_2876)
	{
		{	/* Ast/var.sch 438 */
			return
				BGl_sfunzd2failsafezd2zzast_varz00(((BgL_sfunz00_bglt) BgL_oz00_2876));
		}

	}



/* sfun-failsafe-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzd2failsafezd2setz12z12zzast_varz00(BgL_sfunz00_bglt BgL_oz00_282,
		obj_t BgL_vz00_283)
	{
		{	/* Ast/var.sch 439 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_282)))->BgL_failsafez00) =
				((obj_t) BgL_vz00_283), BUNSPEC);
		}

	}



/* &sfun-failsafe-set! */
	obj_t BGl_z62sfunzd2failsafezd2setz12z70zzast_varz00(obj_t BgL_envz00_2877,
		obj_t BgL_oz00_2878, obj_t BgL_vz00_2879)
	{
		{	/* Ast/var.sch 439 */
			return
				BGl_sfunzd2failsafezd2setz12z12zzast_varz00(
				((BgL_sfunz00_bglt) BgL_oz00_2878), BgL_vz00_2879);
		}

	}



/* sfun-effect */
	BGL_EXPORTED_DEF obj_t BGl_sfunzd2effectzd2zzast_varz00(BgL_sfunz00_bglt
		BgL_oz00_284)
	{
		{	/* Ast/var.sch 440 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_284)))->BgL_effectz00);
		}

	}



/* &sfun-effect */
	obj_t BGl_z62sfunzd2effectzb0zzast_varz00(obj_t BgL_envz00_2880,
		obj_t BgL_oz00_2881)
	{
		{	/* Ast/var.sch 440 */
			return
				BGl_sfunzd2effectzd2zzast_varz00(((BgL_sfunz00_bglt) BgL_oz00_2881));
		}

	}



/* sfun-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzd2effectzd2setz12z12zzast_varz00(BgL_sfunz00_bglt BgL_oz00_285,
		obj_t BgL_vz00_286)
	{
		{	/* Ast/var.sch 441 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_285)))->BgL_effectz00) =
				((obj_t) BgL_vz00_286), BUNSPEC);
		}

	}



/* &sfun-effect-set! */
	obj_t BGl_z62sfunzd2effectzd2setz12z70zzast_varz00(obj_t BgL_envz00_2882,
		obj_t BgL_oz00_2883, obj_t BgL_vz00_2884)
	{
		{	/* Ast/var.sch 441 */
			return
				BGl_sfunzd2effectzd2setz12z12zzast_varz00(
				((BgL_sfunz00_bglt) BgL_oz00_2883), BgL_vz00_2884);
		}

	}



/* sfun-the-closure */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzd2thezd2closurez00zzast_varz00(BgL_sfunz00_bglt BgL_oz00_287)
	{
		{	/* Ast/var.sch 442 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_287)))->BgL_thezd2closurezd2);
		}

	}



/* &sfun-the-closure */
	obj_t BGl_z62sfunzd2thezd2closurez62zzast_varz00(obj_t BgL_envz00_2885,
		obj_t BgL_oz00_2886)
	{
		{	/* Ast/var.sch 442 */
			return
				BGl_sfunzd2thezd2closurez00zzast_varz00(
				((BgL_sfunz00_bglt) BgL_oz00_2886));
		}

	}



/* sfun-the-closure-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzd2thezd2closurezd2setz12zc0zzast_varz00(BgL_sfunz00_bglt
		BgL_oz00_288, obj_t BgL_vz00_289)
	{
		{	/* Ast/var.sch 443 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_288)))->BgL_thezd2closurezd2) =
				((obj_t) BgL_vz00_289), BUNSPEC);
		}

	}



/* &sfun-the-closure-set! */
	obj_t BGl_z62sfunzd2thezd2closurezd2setz12za2zzast_varz00(obj_t
		BgL_envz00_2887, obj_t BgL_oz00_2888, obj_t BgL_vz00_2889)
	{
		{	/* Ast/var.sch 443 */
			return
				BGl_sfunzd2thezd2closurezd2setz12zc0zzast_varz00(
				((BgL_sfunz00_bglt) BgL_oz00_2888), BgL_vz00_2889);
		}

	}



/* sfun-top? */
	BGL_EXPORTED_DEF bool_t BGl_sfunzd2topzf3z21zzast_varz00(BgL_sfunz00_bglt
		BgL_oz00_290)
	{
		{	/* Ast/var.sch 444 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_290)))->BgL_topzf3zf3);
		}

	}



/* &sfun-top? */
	obj_t BGl_z62sfunzd2topzf3z43zzast_varz00(obj_t BgL_envz00_2890,
		obj_t BgL_oz00_2891)
	{
		{	/* Ast/var.sch 444 */
			return
				BBOOL(BGl_sfunzd2topzf3z21zzast_varz00(
					((BgL_sfunz00_bglt) BgL_oz00_2891)));
		}

	}



/* sfun-top?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzd2topzf3zd2setz12ze1zzast_varz00(BgL_sfunz00_bglt BgL_oz00_291,
		bool_t BgL_vz00_292)
	{
		{	/* Ast/var.sch 445 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_291)))->BgL_topzf3zf3) =
				((bool_t) BgL_vz00_292), BUNSPEC);
		}

	}



/* &sfun-top?-set! */
	obj_t BGl_z62sfunzd2topzf3zd2setz12z83zzast_varz00(obj_t BgL_envz00_2892,
		obj_t BgL_oz00_2893, obj_t BgL_vz00_2894)
	{
		{	/* Ast/var.sch 445 */
			return
				BGl_sfunzd2topzf3zd2setz12ze1zzast_varz00(
				((BgL_sfunz00_bglt) BgL_oz00_2893), CBOOL(BgL_vz00_2894));
		}

	}



/* sfun-stack-allocator */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzd2stackzd2allocatorz00zzast_varz00(BgL_sfunz00_bglt BgL_oz00_293)
	{
		{	/* Ast/var.sch 446 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_293)))->BgL_stackzd2allocatorzd2);
		}

	}



/* &sfun-stack-allocator */
	obj_t BGl_z62sfunzd2stackzd2allocatorz62zzast_varz00(obj_t BgL_envz00_2895,
		obj_t BgL_oz00_2896)
	{
		{	/* Ast/var.sch 446 */
			return
				BGl_sfunzd2stackzd2allocatorz00zzast_varz00(
				((BgL_sfunz00_bglt) BgL_oz00_2896));
		}

	}



/* sfun-stack-allocator-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzd2stackzd2allocatorzd2setz12zc0zzast_varz00(BgL_sfunz00_bglt
		BgL_oz00_294, obj_t BgL_vz00_295)
	{
		{	/* Ast/var.sch 447 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_294)))->BgL_stackzd2allocatorzd2) =
				((obj_t) BgL_vz00_295), BUNSPEC);
		}

	}



/* &sfun-stack-allocator-set! */
	obj_t BGl_z62sfunzd2stackzd2allocatorzd2setz12za2zzast_varz00(obj_t
		BgL_envz00_2897, obj_t BgL_oz00_2898, obj_t BgL_vz00_2899)
	{
		{	/* Ast/var.sch 447 */
			return
				BGl_sfunzd2stackzd2allocatorzd2setz12zc0zzast_varz00(
				((BgL_sfunz00_bglt) BgL_oz00_2898), BgL_vz00_2899);
		}

	}



/* sfun-predicate-of */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzd2predicatezd2ofz00zzast_varz00(BgL_sfunz00_bglt BgL_oz00_296)
	{
		{	/* Ast/var.sch 448 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_296)))->BgL_predicatezd2ofzd2);
		}

	}



/* &sfun-predicate-of */
	obj_t BGl_z62sfunzd2predicatezd2ofz62zzast_varz00(obj_t BgL_envz00_2900,
		obj_t BgL_oz00_2901)
	{
		{	/* Ast/var.sch 448 */
			return
				BGl_sfunzd2predicatezd2ofz00zzast_varz00(
				((BgL_sfunz00_bglt) BgL_oz00_2901));
		}

	}



/* sfun-predicate-of-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzd2predicatezd2ofzd2setz12zc0zzast_varz00(BgL_sfunz00_bglt
		BgL_oz00_297, obj_t BgL_vz00_298)
	{
		{	/* Ast/var.sch 449 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_297)))->BgL_predicatezd2ofzd2) =
				((obj_t) BgL_vz00_298), BUNSPEC);
		}

	}



/* &sfun-predicate-of-set! */
	obj_t BGl_z62sfunzd2predicatezd2ofzd2setz12za2zzast_varz00(obj_t
		BgL_envz00_2902, obj_t BgL_oz00_2903, obj_t BgL_vz00_2904)
	{
		{	/* Ast/var.sch 449 */
			return
				BGl_sfunzd2predicatezd2ofzd2setz12zc0zzast_varz00(
				((BgL_sfunz00_bglt) BgL_oz00_2903), BgL_vz00_2904);
		}

	}



/* sfun-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzd2sidezd2effectz00zzast_varz00(BgL_sfunz00_bglt BgL_oz00_299)
	{
		{	/* Ast/var.sch 450 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_299)))->BgL_sidezd2effectzd2);
		}

	}



/* &sfun-side-effect */
	obj_t BGl_z62sfunzd2sidezd2effectz62zzast_varz00(obj_t BgL_envz00_2905,
		obj_t BgL_oz00_2906)
	{
		{	/* Ast/var.sch 450 */
			return
				BGl_sfunzd2sidezd2effectz00zzast_varz00(
				((BgL_sfunz00_bglt) BgL_oz00_2906));
		}

	}



/* sfun-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzd2sidezd2effectzd2setz12zc0zzast_varz00(BgL_sfunz00_bglt
		BgL_oz00_300, obj_t BgL_vz00_301)
	{
		{	/* Ast/var.sch 451 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_300)))->BgL_sidezd2effectzd2) =
				((obj_t) BgL_vz00_301), BUNSPEC);
		}

	}



/* &sfun-side-effect-set! */
	obj_t BGl_z62sfunzd2sidezd2effectzd2setz12za2zzast_varz00(obj_t
		BgL_envz00_2907, obj_t BgL_oz00_2908, obj_t BgL_vz00_2909)
	{
		{	/* Ast/var.sch 451 */
			return
				BGl_sfunzd2sidezd2effectzd2setz12zc0zzast_varz00(
				((BgL_sfunz00_bglt) BgL_oz00_2908), BgL_vz00_2909);
		}

	}



/* sfun-arity */
	BGL_EXPORTED_DEF long BGl_sfunzd2arityzd2zzast_varz00(BgL_sfunz00_bglt
		BgL_oz00_302)
	{
		{	/* Ast/var.sch 452 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_302)))->BgL_arityz00);
		}

	}



/* &sfun-arity */
	obj_t BGl_z62sfunzd2arityzb0zzast_varz00(obj_t BgL_envz00_2910,
		obj_t BgL_oz00_2911)
	{
		{	/* Ast/var.sch 452 */
			return
				BINT(BGl_sfunzd2arityzd2zzast_varz00(
					((BgL_sfunz00_bglt) BgL_oz00_2911)));
		}

	}



/* make-cfun */
	BGL_EXPORTED_DEF BgL_cfunz00_bglt BGl_makezd2cfunzd2zzast_varz00(long
		BgL_arity1199z00_305, obj_t BgL_sidezd2effect1200zd2_306,
		obj_t BgL_predicatezd2of1201zd2_307, obj_t BgL_stackzd2allocator1202zd2_308,
		bool_t BgL_topzf31203zf3_309, obj_t BgL_thezd2closure1204zd2_310,
		obj_t BgL_effect1205z00_311, obj_t BgL_failsafe1206z00_312,
		obj_t BgL_argszd2noescape1207zd2_313, obj_t BgL_argszd2retescape1208zd2_314,
		obj_t BgL_argszd2type1209zd2_315, bool_t BgL_macrozf31210zf3_316,
		bool_t BgL_infixzf31211zf3_317, obj_t BgL_method1212z00_318)
	{
		{	/* Ast/var.sch 456 */
			{	/* Ast/var.sch 456 */
				BgL_cfunz00_bglt BgL_new1197z00_3862;

				{	/* Ast/var.sch 456 */
					BgL_cfunz00_bglt BgL_new1196z00_3863;

					BgL_new1196z00_3863 =
						((BgL_cfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cfunz00_bgl))));
					{	/* Ast/var.sch 456 */
						long BgL_arg1312z00_3864;

						BgL_arg1312z00_3864 = BGL_CLASS_NUM(BGl_cfunz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1196z00_3863), BgL_arg1312z00_3864);
					}
					{	/* Ast/var.sch 456 */
						BgL_objectz00_bglt BgL_tmpz00_5130;

						BgL_tmpz00_5130 = ((BgL_objectz00_bglt) BgL_new1196z00_3863);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5130, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1196z00_3863);
					BgL_new1197z00_3862 = BgL_new1196z00_3863;
				}
				((((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt) BgL_new1197z00_3862)))->BgL_arityz00) =
					((long) BgL_arity1199z00_305), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1197z00_3862)))->
						BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1200zd2_306), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1197z00_3862)))->
						BgL_predicatezd2ofzd2) =
					((obj_t) BgL_predicatezd2of1201zd2_307), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1197z00_3862)))->
						BgL_stackzd2allocatorzd2) =
					((obj_t) BgL_stackzd2allocator1202zd2_308), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1197z00_3862)))->
						BgL_topzf3zf3) = ((bool_t) BgL_topzf31203zf3_309), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1197z00_3862)))->
						BgL_thezd2closurezd2) =
					((obj_t) BgL_thezd2closure1204zd2_310), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1197z00_3862)))->
						BgL_effectz00) = ((obj_t) BgL_effect1205z00_311), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1197z00_3862)))->
						BgL_failsafez00) = ((obj_t) BgL_failsafe1206z00_312), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1197z00_3862)))->
						BgL_argszd2noescapezd2) =
					((obj_t) BgL_argszd2noescape1207zd2_313), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1197z00_3862)))->
						BgL_argszd2retescapezd2) =
					((obj_t) BgL_argszd2retescape1208zd2_314), BUNSPEC);
				((((BgL_cfunz00_bglt) COBJECT(BgL_new1197z00_3862))->
						BgL_argszd2typezd2) =
					((obj_t) BgL_argszd2type1209zd2_315), BUNSPEC);
				((((BgL_cfunz00_bglt) COBJECT(BgL_new1197z00_3862))->BgL_macrozf3zf3) =
					((bool_t) BgL_macrozf31210zf3_316), BUNSPEC);
				((((BgL_cfunz00_bglt) COBJECT(BgL_new1197z00_3862))->BgL_infixzf3zf3) =
					((bool_t) BgL_infixzf31211zf3_317), BUNSPEC);
				((((BgL_cfunz00_bglt) COBJECT(BgL_new1197z00_3862))->BgL_methodz00) =
					((obj_t) BgL_method1212z00_318), BUNSPEC);
				return BgL_new1197z00_3862;
			}
		}

	}



/* &make-cfun */
	BgL_cfunz00_bglt BGl_z62makezd2cfunzb0zzast_varz00(obj_t BgL_envz00_2912,
		obj_t BgL_arity1199z00_2913, obj_t BgL_sidezd2effect1200zd2_2914,
		obj_t BgL_predicatezd2of1201zd2_2915,
		obj_t BgL_stackzd2allocator1202zd2_2916, obj_t BgL_topzf31203zf3_2917,
		obj_t BgL_thezd2closure1204zd2_2918, obj_t BgL_effect1205z00_2919,
		obj_t BgL_failsafe1206z00_2920, obj_t BgL_argszd2noescape1207zd2_2921,
		obj_t BgL_argszd2retescape1208zd2_2922, obj_t BgL_argszd2type1209zd2_2923,
		obj_t BgL_macrozf31210zf3_2924, obj_t BgL_infixzf31211zf3_2925,
		obj_t BgL_method1212z00_2926)
	{
		{	/* Ast/var.sch 456 */
			return
				BGl_makezd2cfunzd2zzast_varz00(
				(long) CINT(BgL_arity1199z00_2913), BgL_sidezd2effect1200zd2_2914,
				BgL_predicatezd2of1201zd2_2915, BgL_stackzd2allocator1202zd2_2916,
				CBOOL(BgL_topzf31203zf3_2917), BgL_thezd2closure1204zd2_2918,
				BgL_effect1205z00_2919, BgL_failsafe1206z00_2920,
				BgL_argszd2noescape1207zd2_2921, BgL_argszd2retescape1208zd2_2922,
				BgL_argszd2type1209zd2_2923, CBOOL(BgL_macrozf31210zf3_2924),
				CBOOL(BgL_infixzf31211zf3_2925), BgL_method1212z00_2926);
		}

	}



/* cfun? */
	BGL_EXPORTED_DEF bool_t BGl_cfunzf3zf3zzast_varz00(obj_t BgL_objz00_319)
	{
		{	/* Ast/var.sch 457 */
			{	/* Ast/var.sch 457 */
				obj_t BgL_classz00_3865;

				BgL_classz00_3865 = BGl_cfunz00zzast_varz00;
				if (BGL_OBJECTP(BgL_objz00_319))
					{	/* Ast/var.sch 457 */
						BgL_objectz00_bglt BgL_arg1807z00_3866;

						BgL_arg1807z00_3866 = (BgL_objectz00_bglt) (BgL_objz00_319);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Ast/var.sch 457 */
								long BgL_idxz00_3867;

								BgL_idxz00_3867 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3866);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3867 + 3L)) == BgL_classz00_3865);
							}
						else
							{	/* Ast/var.sch 457 */
								bool_t BgL_res2168z00_3870;

								{	/* Ast/var.sch 457 */
									obj_t BgL_oclassz00_3871;

									{	/* Ast/var.sch 457 */
										obj_t BgL_arg1815z00_3872;
										long BgL_arg1816z00_3873;

										BgL_arg1815z00_3872 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Ast/var.sch 457 */
											long BgL_arg1817z00_3874;

											BgL_arg1817z00_3874 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3866);
											BgL_arg1816z00_3873 = (BgL_arg1817z00_3874 - OBJECT_TYPE);
										}
										BgL_oclassz00_3871 =
											VECTOR_REF(BgL_arg1815z00_3872, BgL_arg1816z00_3873);
									}
									{	/* Ast/var.sch 457 */
										bool_t BgL__ortest_1115z00_3875;

										BgL__ortest_1115z00_3875 =
											(BgL_classz00_3865 == BgL_oclassz00_3871);
										if (BgL__ortest_1115z00_3875)
											{	/* Ast/var.sch 457 */
												BgL_res2168z00_3870 = BgL__ortest_1115z00_3875;
											}
										else
											{	/* Ast/var.sch 457 */
												long BgL_odepthz00_3876;

												{	/* Ast/var.sch 457 */
													obj_t BgL_arg1804z00_3877;

													BgL_arg1804z00_3877 = (BgL_oclassz00_3871);
													BgL_odepthz00_3876 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3877);
												}
												if ((3L < BgL_odepthz00_3876))
													{	/* Ast/var.sch 457 */
														obj_t BgL_arg1802z00_3878;

														{	/* Ast/var.sch 457 */
															obj_t BgL_arg1803z00_3879;

															BgL_arg1803z00_3879 = (BgL_oclassz00_3871);
															BgL_arg1802z00_3878 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3879,
																3L);
														}
														BgL_res2168z00_3870 =
															(BgL_arg1802z00_3878 == BgL_classz00_3865);
													}
												else
													{	/* Ast/var.sch 457 */
														BgL_res2168z00_3870 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2168z00_3870;
							}
					}
				else
					{	/* Ast/var.sch 457 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &cfun? */
	obj_t BGl_z62cfunzf3z91zzast_varz00(obj_t BgL_envz00_2927,
		obj_t BgL_objz00_2928)
	{
		{	/* Ast/var.sch 457 */
			return BBOOL(BGl_cfunzf3zf3zzast_varz00(BgL_objz00_2928));
		}

	}



/* cfun-nil */
	BGL_EXPORTED_DEF BgL_cfunz00_bglt BGl_cfunzd2nilzd2zzast_varz00(void)
	{
		{	/* Ast/var.sch 458 */
			{	/* Ast/var.sch 458 */
				obj_t BgL_classz00_1869;

				BgL_classz00_1869 = BGl_cfunz00zzast_varz00;
				{	/* Ast/var.sch 458 */
					obj_t BgL__ortest_1117z00_1870;

					BgL__ortest_1117z00_1870 = BGL_CLASS_NIL(BgL_classz00_1869);
					if (CBOOL(BgL__ortest_1117z00_1870))
						{	/* Ast/var.sch 458 */
							return ((BgL_cfunz00_bglt) BgL__ortest_1117z00_1870);
						}
					else
						{	/* Ast/var.sch 458 */
							return
								((BgL_cfunz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1869));
						}
				}
			}
		}

	}



/* &cfun-nil */
	BgL_cfunz00_bglt BGl_z62cfunzd2nilzb0zzast_varz00(obj_t BgL_envz00_2929)
	{
		{	/* Ast/var.sch 458 */
			return BGl_cfunzd2nilzd2zzast_varz00();
		}

	}



/* cfun-method */
	BGL_EXPORTED_DEF obj_t BGl_cfunzd2methodzd2zzast_varz00(BgL_cfunz00_bglt
		BgL_oz00_320)
	{
		{	/* Ast/var.sch 459 */
			return (((BgL_cfunz00_bglt) COBJECT(BgL_oz00_320))->BgL_methodz00);
		}

	}



/* &cfun-method */
	obj_t BGl_z62cfunzd2methodzb0zzast_varz00(obj_t BgL_envz00_2930,
		obj_t BgL_oz00_2931)
	{
		{	/* Ast/var.sch 459 */
			return
				BGl_cfunzd2methodzd2zzast_varz00(((BgL_cfunz00_bglt) BgL_oz00_2931));
		}

	}



/* cfun-method-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzd2methodzd2setz12z12zzast_varz00(BgL_cfunz00_bglt BgL_oz00_321,
		obj_t BgL_vz00_322)
	{
		{	/* Ast/var.sch 460 */
			return
				((((BgL_cfunz00_bglt) COBJECT(BgL_oz00_321))->BgL_methodz00) =
				((obj_t) BgL_vz00_322), BUNSPEC);
		}

	}



/* &cfun-method-set! */
	obj_t BGl_z62cfunzd2methodzd2setz12z70zzast_varz00(obj_t BgL_envz00_2932,
		obj_t BgL_oz00_2933, obj_t BgL_vz00_2934)
	{
		{	/* Ast/var.sch 460 */
			return
				BGl_cfunzd2methodzd2setz12z12zzast_varz00(
				((BgL_cfunz00_bglt) BgL_oz00_2933), BgL_vz00_2934);
		}

	}



/* cfun-infix? */
	BGL_EXPORTED_DEF bool_t BGl_cfunzd2infixzf3z21zzast_varz00(BgL_cfunz00_bglt
		BgL_oz00_323)
	{
		{	/* Ast/var.sch 461 */
			return (((BgL_cfunz00_bglt) COBJECT(BgL_oz00_323))->BgL_infixzf3zf3);
		}

	}



/* &cfun-infix? */
	obj_t BGl_z62cfunzd2infixzf3z43zzast_varz00(obj_t BgL_envz00_2935,
		obj_t BgL_oz00_2936)
	{
		{	/* Ast/var.sch 461 */
			return
				BBOOL(BGl_cfunzd2infixzf3z21zzast_varz00(
					((BgL_cfunz00_bglt) BgL_oz00_2936)));
		}

	}



/* cfun-infix?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzd2infixzf3zd2setz12ze1zzast_varz00(BgL_cfunz00_bglt BgL_oz00_324,
		bool_t BgL_vz00_325)
	{
		{	/* Ast/var.sch 462 */
			return
				((((BgL_cfunz00_bglt) COBJECT(BgL_oz00_324))->BgL_infixzf3zf3) =
				((bool_t) BgL_vz00_325), BUNSPEC);
		}

	}



/* &cfun-infix?-set! */
	obj_t BGl_z62cfunzd2infixzf3zd2setz12z83zzast_varz00(obj_t BgL_envz00_2937,
		obj_t BgL_oz00_2938, obj_t BgL_vz00_2939)
	{
		{	/* Ast/var.sch 462 */
			return
				BGl_cfunzd2infixzf3zd2setz12ze1zzast_varz00(
				((BgL_cfunz00_bglt) BgL_oz00_2938), CBOOL(BgL_vz00_2939));
		}

	}



/* cfun-macro? */
	BGL_EXPORTED_DEF bool_t BGl_cfunzd2macrozf3z21zzast_varz00(BgL_cfunz00_bglt
		BgL_oz00_326)
	{
		{	/* Ast/var.sch 463 */
			return (((BgL_cfunz00_bglt) COBJECT(BgL_oz00_326))->BgL_macrozf3zf3);
		}

	}



/* &cfun-macro? */
	obj_t BGl_z62cfunzd2macrozf3z43zzast_varz00(obj_t BgL_envz00_2940,
		obj_t BgL_oz00_2941)
	{
		{	/* Ast/var.sch 463 */
			return
				BBOOL(BGl_cfunzd2macrozf3z21zzast_varz00(
					((BgL_cfunz00_bglt) BgL_oz00_2941)));
		}

	}



/* cfun-args-type */
	BGL_EXPORTED_DEF obj_t BGl_cfunzd2argszd2typez00zzast_varz00(BgL_cfunz00_bglt
		BgL_oz00_329)
	{
		{	/* Ast/var.sch 465 */
			return (((BgL_cfunz00_bglt) COBJECT(BgL_oz00_329))->BgL_argszd2typezd2);
		}

	}



/* &cfun-args-type */
	obj_t BGl_z62cfunzd2argszd2typez62zzast_varz00(obj_t BgL_envz00_2942,
		obj_t BgL_oz00_2943)
	{
		{	/* Ast/var.sch 465 */
			return
				BGl_cfunzd2argszd2typez00zzast_varz00(
				((BgL_cfunz00_bglt) BgL_oz00_2943));
		}

	}



/* cfun-args-retescape */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzd2argszd2retescapez00zzast_varz00(BgL_cfunz00_bglt BgL_oz00_332)
	{
		{	/* Ast/var.sch 467 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_332)))->BgL_argszd2retescapezd2);
		}

	}



/* &cfun-args-retescape */
	obj_t BGl_z62cfunzd2argszd2retescapez62zzast_varz00(obj_t BgL_envz00_2944,
		obj_t BgL_oz00_2945)
	{
		{	/* Ast/var.sch 467 */
			return
				BGl_cfunzd2argszd2retescapez00zzast_varz00(
				((BgL_cfunz00_bglt) BgL_oz00_2945));
		}

	}



/* cfun-args-retescape-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzd2argszd2retescapezd2setz12zc0zzast_varz00(BgL_cfunz00_bglt
		BgL_oz00_333, obj_t BgL_vz00_334)
	{
		{	/* Ast/var.sch 468 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_333)))->BgL_argszd2retescapezd2) =
				((obj_t) BgL_vz00_334), BUNSPEC);
		}

	}



/* &cfun-args-retescape-set! */
	obj_t BGl_z62cfunzd2argszd2retescapezd2setz12za2zzast_varz00(obj_t
		BgL_envz00_2946, obj_t BgL_oz00_2947, obj_t BgL_vz00_2948)
	{
		{	/* Ast/var.sch 468 */
			return
				BGl_cfunzd2argszd2retescapezd2setz12zc0zzast_varz00(
				((BgL_cfunz00_bglt) BgL_oz00_2947), BgL_vz00_2948);
		}

	}



/* cfun-args-noescape */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzd2argszd2noescapez00zzast_varz00(BgL_cfunz00_bglt BgL_oz00_335)
	{
		{	/* Ast/var.sch 469 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_335)))->BgL_argszd2noescapezd2);
		}

	}



/* &cfun-args-noescape */
	obj_t BGl_z62cfunzd2argszd2noescapez62zzast_varz00(obj_t BgL_envz00_2949,
		obj_t BgL_oz00_2950)
	{
		{	/* Ast/var.sch 469 */
			return
				BGl_cfunzd2argszd2noescapez00zzast_varz00(
				((BgL_cfunz00_bglt) BgL_oz00_2950));
		}

	}



/* cfun-args-noescape-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzd2argszd2noescapezd2setz12zc0zzast_varz00(BgL_cfunz00_bglt
		BgL_oz00_336, obj_t BgL_vz00_337)
	{
		{	/* Ast/var.sch 470 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_336)))->BgL_argszd2noescapezd2) =
				((obj_t) BgL_vz00_337), BUNSPEC);
		}

	}



/* &cfun-args-noescape-set! */
	obj_t BGl_z62cfunzd2argszd2noescapezd2setz12za2zzast_varz00(obj_t
		BgL_envz00_2951, obj_t BgL_oz00_2952, obj_t BgL_vz00_2953)
	{
		{	/* Ast/var.sch 470 */
			return
				BGl_cfunzd2argszd2noescapezd2setz12zc0zzast_varz00(
				((BgL_cfunz00_bglt) BgL_oz00_2952), BgL_vz00_2953);
		}

	}



/* cfun-failsafe */
	BGL_EXPORTED_DEF obj_t BGl_cfunzd2failsafezd2zzast_varz00(BgL_cfunz00_bglt
		BgL_oz00_338)
	{
		{	/* Ast/var.sch 471 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_338)))->BgL_failsafez00);
		}

	}



/* &cfun-failsafe */
	obj_t BGl_z62cfunzd2failsafezb0zzast_varz00(obj_t BgL_envz00_2954,
		obj_t BgL_oz00_2955)
	{
		{	/* Ast/var.sch 471 */
			return
				BGl_cfunzd2failsafezd2zzast_varz00(((BgL_cfunz00_bglt) BgL_oz00_2955));
		}

	}



/* cfun-failsafe-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzd2failsafezd2setz12z12zzast_varz00(BgL_cfunz00_bglt BgL_oz00_339,
		obj_t BgL_vz00_340)
	{
		{	/* Ast/var.sch 472 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_339)))->BgL_failsafez00) =
				((obj_t) BgL_vz00_340), BUNSPEC);
		}

	}



/* &cfun-failsafe-set! */
	obj_t BGl_z62cfunzd2failsafezd2setz12z70zzast_varz00(obj_t BgL_envz00_2956,
		obj_t BgL_oz00_2957, obj_t BgL_vz00_2958)
	{
		{	/* Ast/var.sch 472 */
			return
				BGl_cfunzd2failsafezd2setz12z12zzast_varz00(
				((BgL_cfunz00_bglt) BgL_oz00_2957), BgL_vz00_2958);
		}

	}



/* cfun-effect */
	BGL_EXPORTED_DEF obj_t BGl_cfunzd2effectzd2zzast_varz00(BgL_cfunz00_bglt
		BgL_oz00_341)
	{
		{	/* Ast/var.sch 473 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_341)))->BgL_effectz00);
		}

	}



/* &cfun-effect */
	obj_t BGl_z62cfunzd2effectzb0zzast_varz00(obj_t BgL_envz00_2959,
		obj_t BgL_oz00_2960)
	{
		{	/* Ast/var.sch 473 */
			return
				BGl_cfunzd2effectzd2zzast_varz00(((BgL_cfunz00_bglt) BgL_oz00_2960));
		}

	}



/* cfun-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzd2effectzd2setz12z12zzast_varz00(BgL_cfunz00_bglt BgL_oz00_342,
		obj_t BgL_vz00_343)
	{
		{	/* Ast/var.sch 474 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_342)))->BgL_effectz00) =
				((obj_t) BgL_vz00_343), BUNSPEC);
		}

	}



/* &cfun-effect-set! */
	obj_t BGl_z62cfunzd2effectzd2setz12z70zzast_varz00(obj_t BgL_envz00_2961,
		obj_t BgL_oz00_2962, obj_t BgL_vz00_2963)
	{
		{	/* Ast/var.sch 474 */
			return
				BGl_cfunzd2effectzd2setz12z12zzast_varz00(
				((BgL_cfunz00_bglt) BgL_oz00_2962), BgL_vz00_2963);
		}

	}



/* cfun-the-closure */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzd2thezd2closurez00zzast_varz00(BgL_cfunz00_bglt BgL_oz00_344)
	{
		{	/* Ast/var.sch 475 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_344)))->BgL_thezd2closurezd2);
		}

	}



/* &cfun-the-closure */
	obj_t BGl_z62cfunzd2thezd2closurez62zzast_varz00(obj_t BgL_envz00_2964,
		obj_t BgL_oz00_2965)
	{
		{	/* Ast/var.sch 475 */
			return
				BGl_cfunzd2thezd2closurez00zzast_varz00(
				((BgL_cfunz00_bglt) BgL_oz00_2965));
		}

	}



/* cfun-the-closure-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzd2thezd2closurezd2setz12zc0zzast_varz00(BgL_cfunz00_bglt
		BgL_oz00_345, obj_t BgL_vz00_346)
	{
		{	/* Ast/var.sch 476 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_345)))->BgL_thezd2closurezd2) =
				((obj_t) BgL_vz00_346), BUNSPEC);
		}

	}



/* &cfun-the-closure-set! */
	obj_t BGl_z62cfunzd2thezd2closurezd2setz12za2zzast_varz00(obj_t
		BgL_envz00_2966, obj_t BgL_oz00_2967, obj_t BgL_vz00_2968)
	{
		{	/* Ast/var.sch 476 */
			return
				BGl_cfunzd2thezd2closurezd2setz12zc0zzast_varz00(
				((BgL_cfunz00_bglt) BgL_oz00_2967), BgL_vz00_2968);
		}

	}



/* cfun-top? */
	BGL_EXPORTED_DEF bool_t BGl_cfunzd2topzf3z21zzast_varz00(BgL_cfunz00_bglt
		BgL_oz00_347)
	{
		{	/* Ast/var.sch 477 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_347)))->BgL_topzf3zf3);
		}

	}



/* &cfun-top? */
	obj_t BGl_z62cfunzd2topzf3z43zzast_varz00(obj_t BgL_envz00_2969,
		obj_t BgL_oz00_2970)
	{
		{	/* Ast/var.sch 477 */
			return
				BBOOL(BGl_cfunzd2topzf3z21zzast_varz00(
					((BgL_cfunz00_bglt) BgL_oz00_2970)));
		}

	}



/* cfun-top?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzd2topzf3zd2setz12ze1zzast_varz00(BgL_cfunz00_bglt BgL_oz00_348,
		bool_t BgL_vz00_349)
	{
		{	/* Ast/var.sch 478 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_348)))->BgL_topzf3zf3) =
				((bool_t) BgL_vz00_349), BUNSPEC);
		}

	}



/* &cfun-top?-set! */
	obj_t BGl_z62cfunzd2topzf3zd2setz12z83zzast_varz00(obj_t BgL_envz00_2971,
		obj_t BgL_oz00_2972, obj_t BgL_vz00_2973)
	{
		{	/* Ast/var.sch 478 */
			return
				BGl_cfunzd2topzf3zd2setz12ze1zzast_varz00(
				((BgL_cfunz00_bglt) BgL_oz00_2972), CBOOL(BgL_vz00_2973));
		}

	}



/* cfun-stack-allocator */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzd2stackzd2allocatorz00zzast_varz00(BgL_cfunz00_bglt BgL_oz00_350)
	{
		{	/* Ast/var.sch 479 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_350)))->BgL_stackzd2allocatorzd2);
		}

	}



/* &cfun-stack-allocator */
	obj_t BGl_z62cfunzd2stackzd2allocatorz62zzast_varz00(obj_t BgL_envz00_2974,
		obj_t BgL_oz00_2975)
	{
		{	/* Ast/var.sch 479 */
			return
				BGl_cfunzd2stackzd2allocatorz00zzast_varz00(
				((BgL_cfunz00_bglt) BgL_oz00_2975));
		}

	}



/* cfun-stack-allocator-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzd2stackzd2allocatorzd2setz12zc0zzast_varz00(BgL_cfunz00_bglt
		BgL_oz00_351, obj_t BgL_vz00_352)
	{
		{	/* Ast/var.sch 480 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_351)))->BgL_stackzd2allocatorzd2) =
				((obj_t) BgL_vz00_352), BUNSPEC);
		}

	}



/* &cfun-stack-allocator-set! */
	obj_t BGl_z62cfunzd2stackzd2allocatorzd2setz12za2zzast_varz00(obj_t
		BgL_envz00_2976, obj_t BgL_oz00_2977, obj_t BgL_vz00_2978)
	{
		{	/* Ast/var.sch 480 */
			return
				BGl_cfunzd2stackzd2allocatorzd2setz12zc0zzast_varz00(
				((BgL_cfunz00_bglt) BgL_oz00_2977), BgL_vz00_2978);
		}

	}



/* cfun-predicate-of */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzd2predicatezd2ofz00zzast_varz00(BgL_cfunz00_bglt BgL_oz00_353)
	{
		{	/* Ast/var.sch 481 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_353)))->BgL_predicatezd2ofzd2);
		}

	}



/* &cfun-predicate-of */
	obj_t BGl_z62cfunzd2predicatezd2ofz62zzast_varz00(obj_t BgL_envz00_2979,
		obj_t BgL_oz00_2980)
	{
		{	/* Ast/var.sch 481 */
			return
				BGl_cfunzd2predicatezd2ofz00zzast_varz00(
				((BgL_cfunz00_bglt) BgL_oz00_2980));
		}

	}



/* cfun-predicate-of-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzd2predicatezd2ofzd2setz12zc0zzast_varz00(BgL_cfunz00_bglt
		BgL_oz00_354, obj_t BgL_vz00_355)
	{
		{	/* Ast/var.sch 482 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_354)))->BgL_predicatezd2ofzd2) =
				((obj_t) BgL_vz00_355), BUNSPEC);
		}

	}



/* &cfun-predicate-of-set! */
	obj_t BGl_z62cfunzd2predicatezd2ofzd2setz12za2zzast_varz00(obj_t
		BgL_envz00_2981, obj_t BgL_oz00_2982, obj_t BgL_vz00_2983)
	{
		{	/* Ast/var.sch 482 */
			return
				BGl_cfunzd2predicatezd2ofzd2setz12zc0zzast_varz00(
				((BgL_cfunz00_bglt) BgL_oz00_2982), BgL_vz00_2983);
		}

	}



/* cfun-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzd2sidezd2effectz00zzast_varz00(BgL_cfunz00_bglt BgL_oz00_356)
	{
		{	/* Ast/var.sch 483 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_356)))->BgL_sidezd2effectzd2);
		}

	}



/* &cfun-side-effect */
	obj_t BGl_z62cfunzd2sidezd2effectz62zzast_varz00(obj_t BgL_envz00_2984,
		obj_t BgL_oz00_2985)
	{
		{	/* Ast/var.sch 483 */
			return
				BGl_cfunzd2sidezd2effectz00zzast_varz00(
				((BgL_cfunz00_bglt) BgL_oz00_2985));
		}

	}



/* cfun-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunzd2sidezd2effectzd2setz12zc0zzast_varz00(BgL_cfunz00_bglt
		BgL_oz00_357, obj_t BgL_vz00_358)
	{
		{	/* Ast/var.sch 484 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_357)))->BgL_sidezd2effectzd2) =
				((obj_t) BgL_vz00_358), BUNSPEC);
		}

	}



/* &cfun-side-effect-set! */
	obj_t BGl_z62cfunzd2sidezd2effectzd2setz12za2zzast_varz00(obj_t
		BgL_envz00_2986, obj_t BgL_oz00_2987, obj_t BgL_vz00_2988)
	{
		{	/* Ast/var.sch 484 */
			return
				BGl_cfunzd2sidezd2effectzd2setz12zc0zzast_varz00(
				((BgL_cfunz00_bglt) BgL_oz00_2987), BgL_vz00_2988);
		}

	}



/* cfun-arity */
	BGL_EXPORTED_DEF long BGl_cfunzd2arityzd2zzast_varz00(BgL_cfunz00_bglt
		BgL_oz00_359)
	{
		{	/* Ast/var.sch 485 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_359)))->BgL_arityz00);
		}

	}



/* &cfun-arity */
	obj_t BGl_z62cfunzd2arityzb0zzast_varz00(obj_t BgL_envz00_2989,
		obj_t BgL_oz00_2990)
	{
		{	/* Ast/var.sch 485 */
			return
				BINT(BGl_cfunzd2arityzd2zzast_varz00(
					((BgL_cfunz00_bglt) BgL_oz00_2990)));
		}

	}



/* make-svar */
	BGL_EXPORTED_DEF BgL_svarz00_bglt BGl_makezd2svarzd2zzast_varz00(obj_t
		BgL_loc1197z00_362)
	{
		{	/* Ast/var.sch 489 */
			{	/* Ast/var.sch 489 */
				BgL_svarz00_bglt BgL_new1200z00_3880;

				{	/* Ast/var.sch 489 */
					BgL_svarz00_bglt BgL_new1198z00_3881;

					BgL_new1198z00_3881 =
						((BgL_svarz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_svarz00_bgl))));
					{	/* Ast/var.sch 489 */
						long BgL_arg1314z00_3882;

						BgL_arg1314z00_3882 = BGL_CLASS_NUM(BGl_svarz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1198z00_3881), BgL_arg1314z00_3882);
					}
					{	/* Ast/var.sch 489 */
						BgL_objectz00_bglt BgL_tmpz00_5298;

						BgL_tmpz00_5298 = ((BgL_objectz00_bglt) BgL_new1198z00_3881);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5298, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1198z00_3881);
					BgL_new1200z00_3880 = BgL_new1198z00_3881;
				}
				((((BgL_svarz00_bglt) COBJECT(BgL_new1200z00_3880))->BgL_locz00) =
					((obj_t) BgL_loc1197z00_362), BUNSPEC);
				return BgL_new1200z00_3880;
			}
		}

	}



/* &make-svar */
	BgL_svarz00_bglt BGl_z62makezd2svarzb0zzast_varz00(obj_t BgL_envz00_2991,
		obj_t BgL_loc1197z00_2992)
	{
		{	/* Ast/var.sch 489 */
			return BGl_makezd2svarzd2zzast_varz00(BgL_loc1197z00_2992);
		}

	}



/* svar? */
	BGL_EXPORTED_DEF bool_t BGl_svarzf3zf3zzast_varz00(obj_t BgL_objz00_363)
	{
		{	/* Ast/var.sch 490 */
			{	/* Ast/var.sch 490 */
				obj_t BgL_classz00_3883;

				BgL_classz00_3883 = BGl_svarz00zzast_varz00;
				if (BGL_OBJECTP(BgL_objz00_363))
					{	/* Ast/var.sch 490 */
						BgL_objectz00_bglt BgL_arg1807z00_3884;

						BgL_arg1807z00_3884 = (BgL_objectz00_bglt) (BgL_objz00_363);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Ast/var.sch 490 */
								long BgL_idxz00_3885;

								BgL_idxz00_3885 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3884);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3885 + 2L)) == BgL_classz00_3883);
							}
						else
							{	/* Ast/var.sch 490 */
								bool_t BgL_res2169z00_3888;

								{	/* Ast/var.sch 490 */
									obj_t BgL_oclassz00_3889;

									{	/* Ast/var.sch 490 */
										obj_t BgL_arg1815z00_3890;
										long BgL_arg1816z00_3891;

										BgL_arg1815z00_3890 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Ast/var.sch 490 */
											long BgL_arg1817z00_3892;

											BgL_arg1817z00_3892 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3884);
											BgL_arg1816z00_3891 = (BgL_arg1817z00_3892 - OBJECT_TYPE);
										}
										BgL_oclassz00_3889 =
											VECTOR_REF(BgL_arg1815z00_3890, BgL_arg1816z00_3891);
									}
									{	/* Ast/var.sch 490 */
										bool_t BgL__ortest_1115z00_3893;

										BgL__ortest_1115z00_3893 =
											(BgL_classz00_3883 == BgL_oclassz00_3889);
										if (BgL__ortest_1115z00_3893)
											{	/* Ast/var.sch 490 */
												BgL_res2169z00_3888 = BgL__ortest_1115z00_3893;
											}
										else
											{	/* Ast/var.sch 490 */
												long BgL_odepthz00_3894;

												{	/* Ast/var.sch 490 */
													obj_t BgL_arg1804z00_3895;

													BgL_arg1804z00_3895 = (BgL_oclassz00_3889);
													BgL_odepthz00_3894 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3895);
												}
												if ((2L < BgL_odepthz00_3894))
													{	/* Ast/var.sch 490 */
														obj_t BgL_arg1802z00_3896;

														{	/* Ast/var.sch 490 */
															obj_t BgL_arg1803z00_3897;

															BgL_arg1803z00_3897 = (BgL_oclassz00_3889);
															BgL_arg1802z00_3896 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3897,
																2L);
														}
														BgL_res2169z00_3888 =
															(BgL_arg1802z00_3896 == BgL_classz00_3883);
													}
												else
													{	/* Ast/var.sch 490 */
														BgL_res2169z00_3888 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2169z00_3888;
							}
					}
				else
					{	/* Ast/var.sch 490 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &svar? */
	obj_t BGl_z62svarzf3z91zzast_varz00(obj_t BgL_envz00_2993,
		obj_t BgL_objz00_2994)
	{
		{	/* Ast/var.sch 490 */
			return BBOOL(BGl_svarzf3zf3zzast_varz00(BgL_objz00_2994));
		}

	}



/* svar-nil */
	BGL_EXPORTED_DEF BgL_svarz00_bglt BGl_svarzd2nilzd2zzast_varz00(void)
	{
		{	/* Ast/var.sch 491 */
			{	/* Ast/var.sch 491 */
				obj_t BgL_classz00_1912;

				BgL_classz00_1912 = BGl_svarz00zzast_varz00;
				{	/* Ast/var.sch 491 */
					obj_t BgL__ortest_1117z00_1913;

					BgL__ortest_1117z00_1913 = BGL_CLASS_NIL(BgL_classz00_1912);
					if (CBOOL(BgL__ortest_1117z00_1913))
						{	/* Ast/var.sch 491 */
							return ((BgL_svarz00_bglt) BgL__ortest_1117z00_1913);
						}
					else
						{	/* Ast/var.sch 491 */
							return
								((BgL_svarz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1912));
						}
				}
			}
		}

	}



/* &svar-nil */
	BgL_svarz00_bglt BGl_z62svarzd2nilzb0zzast_varz00(obj_t BgL_envz00_2995)
	{
		{	/* Ast/var.sch 491 */
			return BGl_svarzd2nilzd2zzast_varz00();
		}

	}



/* svar-loc */
	BGL_EXPORTED_DEF obj_t BGl_svarzd2loczd2zzast_varz00(BgL_svarz00_bglt
		BgL_oz00_364)
	{
		{	/* Ast/var.sch 492 */
			return (((BgL_svarz00_bglt) COBJECT(BgL_oz00_364))->BgL_locz00);
		}

	}



/* &svar-loc */
	obj_t BGl_z62svarzd2loczb0zzast_varz00(obj_t BgL_envz00_2996,
		obj_t BgL_oz00_2997)
	{
		{	/* Ast/var.sch 492 */
			return BGl_svarzd2loczd2zzast_varz00(((BgL_svarz00_bglt) BgL_oz00_2997));
		}

	}



/* svar-loc-set! */
	BGL_EXPORTED_DEF obj_t BGl_svarzd2loczd2setz12z12zzast_varz00(BgL_svarz00_bglt
		BgL_oz00_365, obj_t BgL_vz00_366)
	{
		{	/* Ast/var.sch 493 */
			return
				((((BgL_svarz00_bglt) COBJECT(BgL_oz00_365))->BgL_locz00) =
				((obj_t) BgL_vz00_366), BUNSPEC);
		}

	}



/* &svar-loc-set! */
	obj_t BGl_z62svarzd2loczd2setz12z70zzast_varz00(obj_t BgL_envz00_2998,
		obj_t BgL_oz00_2999, obj_t BgL_vz00_3000)
	{
		{	/* Ast/var.sch 493 */
			return
				BGl_svarzd2loczd2setz12z12zzast_varz00(
				((BgL_svarz00_bglt) BgL_oz00_2999), BgL_vz00_3000);
		}

	}



/* make-scnst */
	BGL_EXPORTED_DEF BgL_scnstz00_bglt BGl_makezd2scnstzd2zzast_varz00(obj_t
		BgL_node1193z00_367, obj_t BgL_class1194z00_368, obj_t BgL_loc1195z00_369)
	{
		{	/* Ast/var.sch 496 */
			{	/* Ast/var.sch 496 */
				BgL_scnstz00_bglt BgL_new1202z00_3898;

				{	/* Ast/var.sch 496 */
					BgL_scnstz00_bglt BgL_new1201z00_3899;

					BgL_new1201z00_3899 =
						((BgL_scnstz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_scnstz00_bgl))));
					{	/* Ast/var.sch 496 */
						long BgL_arg1315z00_3900;

						BgL_arg1315z00_3900 = BGL_CLASS_NUM(BGl_scnstz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1201z00_3899), BgL_arg1315z00_3900);
					}
					{	/* Ast/var.sch 496 */
						BgL_objectz00_bglt BgL_tmpz00_5345;

						BgL_tmpz00_5345 = ((BgL_objectz00_bglt) BgL_new1201z00_3899);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5345, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1201z00_3899);
					BgL_new1202z00_3898 = BgL_new1201z00_3899;
				}
				((((BgL_scnstz00_bglt) COBJECT(BgL_new1202z00_3898))->BgL_nodez00) =
					((obj_t) BgL_node1193z00_367), BUNSPEC);
				((((BgL_scnstz00_bglt) COBJECT(BgL_new1202z00_3898))->BgL_classz00) =
					((obj_t) BgL_class1194z00_368), BUNSPEC);
				((((BgL_scnstz00_bglt) COBJECT(BgL_new1202z00_3898))->BgL_locz00) =
					((obj_t) BgL_loc1195z00_369), BUNSPEC);
				return BgL_new1202z00_3898;
			}
		}

	}



/* &make-scnst */
	BgL_scnstz00_bglt BGl_z62makezd2scnstzb0zzast_varz00(obj_t BgL_envz00_3001,
		obj_t BgL_node1193z00_3002, obj_t BgL_class1194z00_3003,
		obj_t BgL_loc1195z00_3004)
	{
		{	/* Ast/var.sch 496 */
			return
				BGl_makezd2scnstzd2zzast_varz00(BgL_node1193z00_3002,
				BgL_class1194z00_3003, BgL_loc1195z00_3004);
		}

	}



/* scnst? */
	BGL_EXPORTED_DEF bool_t BGl_scnstzf3zf3zzast_varz00(obj_t BgL_objz00_370)
	{
		{	/* Ast/var.sch 497 */
			{	/* Ast/var.sch 497 */
				obj_t BgL_classz00_3901;

				BgL_classz00_3901 = BGl_scnstz00zzast_varz00;
				if (BGL_OBJECTP(BgL_objz00_370))
					{	/* Ast/var.sch 497 */
						BgL_objectz00_bglt BgL_arg1807z00_3902;

						BgL_arg1807z00_3902 = (BgL_objectz00_bglt) (BgL_objz00_370);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Ast/var.sch 497 */
								long BgL_idxz00_3903;

								BgL_idxz00_3903 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3902);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3903 + 2L)) == BgL_classz00_3901);
							}
						else
							{	/* Ast/var.sch 497 */
								bool_t BgL_res2170z00_3906;

								{	/* Ast/var.sch 497 */
									obj_t BgL_oclassz00_3907;

									{	/* Ast/var.sch 497 */
										obj_t BgL_arg1815z00_3908;
										long BgL_arg1816z00_3909;

										BgL_arg1815z00_3908 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Ast/var.sch 497 */
											long BgL_arg1817z00_3910;

											BgL_arg1817z00_3910 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3902);
											BgL_arg1816z00_3909 = (BgL_arg1817z00_3910 - OBJECT_TYPE);
										}
										BgL_oclassz00_3907 =
											VECTOR_REF(BgL_arg1815z00_3908, BgL_arg1816z00_3909);
									}
									{	/* Ast/var.sch 497 */
										bool_t BgL__ortest_1115z00_3911;

										BgL__ortest_1115z00_3911 =
											(BgL_classz00_3901 == BgL_oclassz00_3907);
										if (BgL__ortest_1115z00_3911)
											{	/* Ast/var.sch 497 */
												BgL_res2170z00_3906 = BgL__ortest_1115z00_3911;
											}
										else
											{	/* Ast/var.sch 497 */
												long BgL_odepthz00_3912;

												{	/* Ast/var.sch 497 */
													obj_t BgL_arg1804z00_3913;

													BgL_arg1804z00_3913 = (BgL_oclassz00_3907);
													BgL_odepthz00_3912 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3913);
												}
												if ((2L < BgL_odepthz00_3912))
													{	/* Ast/var.sch 497 */
														obj_t BgL_arg1802z00_3914;

														{	/* Ast/var.sch 497 */
															obj_t BgL_arg1803z00_3915;

															BgL_arg1803z00_3915 = (BgL_oclassz00_3907);
															BgL_arg1802z00_3914 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3915,
																2L);
														}
														BgL_res2170z00_3906 =
															(BgL_arg1802z00_3914 == BgL_classz00_3901);
													}
												else
													{	/* Ast/var.sch 497 */
														BgL_res2170z00_3906 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2170z00_3906;
							}
					}
				else
					{	/* Ast/var.sch 497 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &scnst? */
	obj_t BGl_z62scnstzf3z91zzast_varz00(obj_t BgL_envz00_3005,
		obj_t BgL_objz00_3006)
	{
		{	/* Ast/var.sch 497 */
			return BBOOL(BGl_scnstzf3zf3zzast_varz00(BgL_objz00_3006));
		}

	}



/* scnst-nil */
	BGL_EXPORTED_DEF BgL_scnstz00_bglt BGl_scnstzd2nilzd2zzast_varz00(void)
	{
		{	/* Ast/var.sch 498 */
			{	/* Ast/var.sch 498 */
				obj_t BgL_classz00_1955;

				BgL_classz00_1955 = BGl_scnstz00zzast_varz00;
				{	/* Ast/var.sch 498 */
					obj_t BgL__ortest_1117z00_1956;

					BgL__ortest_1117z00_1956 = BGL_CLASS_NIL(BgL_classz00_1955);
					if (CBOOL(BgL__ortest_1117z00_1956))
						{	/* Ast/var.sch 498 */
							return ((BgL_scnstz00_bglt) BgL__ortest_1117z00_1956);
						}
					else
						{	/* Ast/var.sch 498 */
							return
								((BgL_scnstz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1955));
						}
				}
			}
		}

	}



/* &scnst-nil */
	BgL_scnstz00_bglt BGl_z62scnstzd2nilzb0zzast_varz00(obj_t BgL_envz00_3007)
	{
		{	/* Ast/var.sch 498 */
			return BGl_scnstzd2nilzd2zzast_varz00();
		}

	}



/* scnst-loc */
	BGL_EXPORTED_DEF obj_t BGl_scnstzd2loczd2zzast_varz00(BgL_scnstz00_bglt
		BgL_oz00_371)
	{
		{	/* Ast/var.sch 499 */
			return (((BgL_scnstz00_bglt) COBJECT(BgL_oz00_371))->BgL_locz00);
		}

	}



/* &scnst-loc */
	obj_t BGl_z62scnstzd2loczb0zzast_varz00(obj_t BgL_envz00_3008,
		obj_t BgL_oz00_3009)
	{
		{	/* Ast/var.sch 499 */
			return
				BGl_scnstzd2loczd2zzast_varz00(((BgL_scnstz00_bglt) BgL_oz00_3009));
		}

	}



/* scnst-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_scnstzd2loczd2setz12z12zzast_varz00(BgL_scnstz00_bglt BgL_oz00_372,
		obj_t BgL_vz00_373)
	{
		{	/* Ast/var.sch 500 */
			return
				((((BgL_scnstz00_bglt) COBJECT(BgL_oz00_372))->BgL_locz00) =
				((obj_t) BgL_vz00_373), BUNSPEC);
		}

	}



/* &scnst-loc-set! */
	obj_t BGl_z62scnstzd2loczd2setz12z70zzast_varz00(obj_t BgL_envz00_3010,
		obj_t BgL_oz00_3011, obj_t BgL_vz00_3012)
	{
		{	/* Ast/var.sch 500 */
			return
				BGl_scnstzd2loczd2setz12z12zzast_varz00(
				((BgL_scnstz00_bglt) BgL_oz00_3011), BgL_vz00_3012);
		}

	}



/* scnst-class */
	BGL_EXPORTED_DEF obj_t BGl_scnstzd2classzd2zzast_varz00(BgL_scnstz00_bglt
		BgL_oz00_374)
	{
		{	/* Ast/var.sch 501 */
			return (((BgL_scnstz00_bglt) COBJECT(BgL_oz00_374))->BgL_classz00);
		}

	}



/* &scnst-class */
	obj_t BGl_z62scnstzd2classzb0zzast_varz00(obj_t BgL_envz00_3013,
		obj_t BgL_oz00_3014)
	{
		{	/* Ast/var.sch 501 */
			return
				BGl_scnstzd2classzd2zzast_varz00(((BgL_scnstz00_bglt) BgL_oz00_3014));
		}

	}



/* scnst-class-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_scnstzd2classzd2setz12z12zzast_varz00(BgL_scnstz00_bglt BgL_oz00_375,
		obj_t BgL_vz00_376)
	{
		{	/* Ast/var.sch 502 */
			return
				((((BgL_scnstz00_bglt) COBJECT(BgL_oz00_375))->BgL_classz00) =
				((obj_t) BgL_vz00_376), BUNSPEC);
		}

	}



/* &scnst-class-set! */
	obj_t BGl_z62scnstzd2classzd2setz12z70zzast_varz00(obj_t BgL_envz00_3015,
		obj_t BgL_oz00_3016, obj_t BgL_vz00_3017)
	{
		{	/* Ast/var.sch 502 */
			return
				BGl_scnstzd2classzd2setz12z12zzast_varz00(
				((BgL_scnstz00_bglt) BgL_oz00_3016), BgL_vz00_3017);
		}

	}



/* scnst-node */
	BGL_EXPORTED_DEF obj_t BGl_scnstzd2nodezd2zzast_varz00(BgL_scnstz00_bglt
		BgL_oz00_377)
	{
		{	/* Ast/var.sch 503 */
			return (((BgL_scnstz00_bglt) COBJECT(BgL_oz00_377))->BgL_nodez00);
		}

	}



/* &scnst-node */
	obj_t BGl_z62scnstzd2nodezb0zzast_varz00(obj_t BgL_envz00_3018,
		obj_t BgL_oz00_3019)
	{
		{	/* Ast/var.sch 503 */
			return
				BGl_scnstzd2nodezd2zzast_varz00(((BgL_scnstz00_bglt) BgL_oz00_3019));
		}

	}



/* make-cvar */
	BGL_EXPORTED_DEF BgL_cvarz00_bglt BGl_makezd2cvarzd2zzast_varz00(bool_t
		BgL_macrozf31191zf3_380)
	{
		{	/* Ast/var.sch 507 */
			{	/* Ast/var.sch 507 */
				BgL_cvarz00_bglt BgL_new1205z00_3916;

				{	/* Ast/var.sch 507 */
					BgL_cvarz00_bglt BgL_new1204z00_3917;

					BgL_new1204z00_3917 =
						((BgL_cvarz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cvarz00_bgl))));
					{	/* Ast/var.sch 507 */
						long BgL_arg1316z00_3918;

						BgL_arg1316z00_3918 = BGL_CLASS_NUM(BGl_cvarz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1204z00_3917), BgL_arg1316z00_3918);
					}
					{	/* Ast/var.sch 507 */
						BgL_objectz00_bglt BgL_tmpz00_5403;

						BgL_tmpz00_5403 = ((BgL_objectz00_bglt) BgL_new1204z00_3917);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5403, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1204z00_3917);
					BgL_new1205z00_3916 = BgL_new1204z00_3917;
				}
				((((BgL_cvarz00_bglt) COBJECT(BgL_new1205z00_3916))->BgL_macrozf3zf3) =
					((bool_t) BgL_macrozf31191zf3_380), BUNSPEC);
				return BgL_new1205z00_3916;
			}
		}

	}



/* &make-cvar */
	BgL_cvarz00_bglt BGl_z62makezd2cvarzb0zzast_varz00(obj_t BgL_envz00_3020,
		obj_t BgL_macrozf31191zf3_3021)
	{
		{	/* Ast/var.sch 507 */
			return BGl_makezd2cvarzd2zzast_varz00(CBOOL(BgL_macrozf31191zf3_3021));
		}

	}



/* cvar? */
	BGL_EXPORTED_DEF bool_t BGl_cvarzf3zf3zzast_varz00(obj_t BgL_objz00_381)
	{
		{	/* Ast/var.sch 508 */
			{	/* Ast/var.sch 508 */
				obj_t BgL_classz00_3919;

				BgL_classz00_3919 = BGl_cvarz00zzast_varz00;
				if (BGL_OBJECTP(BgL_objz00_381))
					{	/* Ast/var.sch 508 */
						BgL_objectz00_bglt BgL_arg1807z00_3920;

						BgL_arg1807z00_3920 = (BgL_objectz00_bglt) (BgL_objz00_381);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Ast/var.sch 508 */
								long BgL_idxz00_3921;

								BgL_idxz00_3921 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3920);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3921 + 2L)) == BgL_classz00_3919);
							}
						else
							{	/* Ast/var.sch 508 */
								bool_t BgL_res2171z00_3924;

								{	/* Ast/var.sch 508 */
									obj_t BgL_oclassz00_3925;

									{	/* Ast/var.sch 508 */
										obj_t BgL_arg1815z00_3926;
										long BgL_arg1816z00_3927;

										BgL_arg1815z00_3926 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Ast/var.sch 508 */
											long BgL_arg1817z00_3928;

											BgL_arg1817z00_3928 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3920);
											BgL_arg1816z00_3927 = (BgL_arg1817z00_3928 - OBJECT_TYPE);
										}
										BgL_oclassz00_3925 =
											VECTOR_REF(BgL_arg1815z00_3926, BgL_arg1816z00_3927);
									}
									{	/* Ast/var.sch 508 */
										bool_t BgL__ortest_1115z00_3929;

										BgL__ortest_1115z00_3929 =
											(BgL_classz00_3919 == BgL_oclassz00_3925);
										if (BgL__ortest_1115z00_3929)
											{	/* Ast/var.sch 508 */
												BgL_res2171z00_3924 = BgL__ortest_1115z00_3929;
											}
										else
											{	/* Ast/var.sch 508 */
												long BgL_odepthz00_3930;

												{	/* Ast/var.sch 508 */
													obj_t BgL_arg1804z00_3931;

													BgL_arg1804z00_3931 = (BgL_oclassz00_3925);
													BgL_odepthz00_3930 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3931);
												}
												if ((2L < BgL_odepthz00_3930))
													{	/* Ast/var.sch 508 */
														obj_t BgL_arg1802z00_3932;

														{	/* Ast/var.sch 508 */
															obj_t BgL_arg1803z00_3933;

															BgL_arg1803z00_3933 = (BgL_oclassz00_3925);
															BgL_arg1802z00_3932 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3933,
																2L);
														}
														BgL_res2171z00_3924 =
															(BgL_arg1802z00_3932 == BgL_classz00_3919);
													}
												else
													{	/* Ast/var.sch 508 */
														BgL_res2171z00_3924 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2171z00_3924;
							}
					}
				else
					{	/* Ast/var.sch 508 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &cvar? */
	obj_t BGl_z62cvarzf3z91zzast_varz00(obj_t BgL_envz00_3022,
		obj_t BgL_objz00_3023)
	{
		{	/* Ast/var.sch 508 */
			return BBOOL(BGl_cvarzf3zf3zzast_varz00(BgL_objz00_3023));
		}

	}



/* cvar-nil */
	BGL_EXPORTED_DEF BgL_cvarz00_bglt BGl_cvarzd2nilzd2zzast_varz00(void)
	{
		{	/* Ast/var.sch 509 */
			{	/* Ast/var.sch 509 */
				obj_t BgL_classz00_1998;

				BgL_classz00_1998 = BGl_cvarz00zzast_varz00;
				{	/* Ast/var.sch 509 */
					obj_t BgL__ortest_1117z00_1999;

					BgL__ortest_1117z00_1999 = BGL_CLASS_NIL(BgL_classz00_1998);
					if (CBOOL(BgL__ortest_1117z00_1999))
						{	/* Ast/var.sch 509 */
							return ((BgL_cvarz00_bglt) BgL__ortest_1117z00_1999);
						}
					else
						{	/* Ast/var.sch 509 */
							return
								((BgL_cvarz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1998));
						}
				}
			}
		}

	}



/* &cvar-nil */
	BgL_cvarz00_bglt BGl_z62cvarzd2nilzb0zzast_varz00(obj_t BgL_envz00_3024)
	{
		{	/* Ast/var.sch 509 */
			return BGl_cvarzd2nilzd2zzast_varz00();
		}

	}



/* cvar-macro? */
	BGL_EXPORTED_DEF bool_t BGl_cvarzd2macrozf3z21zzast_varz00(BgL_cvarz00_bglt
		BgL_oz00_382)
	{
		{	/* Ast/var.sch 510 */
			return (((BgL_cvarz00_bglt) COBJECT(BgL_oz00_382))->BgL_macrozf3zf3);
		}

	}



/* &cvar-macro? */
	obj_t BGl_z62cvarzd2macrozf3z43zzast_varz00(obj_t BgL_envz00_3025,
		obj_t BgL_oz00_3026)
	{
		{	/* Ast/var.sch 510 */
			return
				BBOOL(BGl_cvarzd2macrozf3z21zzast_varz00(
					((BgL_cvarz00_bglt) BgL_oz00_3026)));
		}

	}



/* make-sexit */
	BGL_EXPORTED_DEF BgL_sexitz00_bglt BGl_makezd2sexitzd2zzast_varz00(obj_t
		BgL_handler1188z00_385, bool_t BgL_detachedzf31189zf3_386)
	{
		{	/* Ast/var.sch 514 */
			{	/* Ast/var.sch 514 */
				BgL_sexitz00_bglt BgL_new1207z00_3934;

				{	/* Ast/var.sch 514 */
					BgL_sexitz00_bglt BgL_new1206z00_3935;

					BgL_new1206z00_3935 =
						((BgL_sexitz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_sexitz00_bgl))));
					{	/* Ast/var.sch 514 */
						long BgL_arg1317z00_3936;

						BgL_arg1317z00_3936 = BGL_CLASS_NUM(BGl_sexitz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1206z00_3935), BgL_arg1317z00_3936);
					}
					{	/* Ast/var.sch 514 */
						BgL_objectz00_bglt BgL_tmpz00_5449;

						BgL_tmpz00_5449 = ((BgL_objectz00_bglt) BgL_new1206z00_3935);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5449, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1206z00_3935);
					BgL_new1207z00_3934 = BgL_new1206z00_3935;
				}
				((((BgL_sexitz00_bglt) COBJECT(BgL_new1207z00_3934))->BgL_handlerz00) =
					((obj_t) BgL_handler1188z00_385), BUNSPEC);
				((((BgL_sexitz00_bglt) COBJECT(BgL_new1207z00_3934))->
						BgL_detachedzf3zf3) =
					((bool_t) BgL_detachedzf31189zf3_386), BUNSPEC);
				return BgL_new1207z00_3934;
			}
		}

	}



/* &make-sexit */
	BgL_sexitz00_bglt BGl_z62makezd2sexitzb0zzast_varz00(obj_t BgL_envz00_3027,
		obj_t BgL_handler1188z00_3028, obj_t BgL_detachedzf31189zf3_3029)
	{
		{	/* Ast/var.sch 514 */
			return
				BGl_makezd2sexitzd2zzast_varz00(BgL_handler1188z00_3028,
				CBOOL(BgL_detachedzf31189zf3_3029));
		}

	}



/* sexit? */
	BGL_EXPORTED_DEF bool_t BGl_sexitzf3zf3zzast_varz00(obj_t BgL_objz00_387)
	{
		{	/* Ast/var.sch 515 */
			{	/* Ast/var.sch 515 */
				obj_t BgL_classz00_3937;

				BgL_classz00_3937 = BGl_sexitz00zzast_varz00;
				if (BGL_OBJECTP(BgL_objz00_387))
					{	/* Ast/var.sch 515 */
						BgL_objectz00_bglt BgL_arg1807z00_3938;

						BgL_arg1807z00_3938 = (BgL_objectz00_bglt) (BgL_objz00_387);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Ast/var.sch 515 */
								long BgL_idxz00_3939;

								BgL_idxz00_3939 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3938);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3939 + 2L)) == BgL_classz00_3937);
							}
						else
							{	/* Ast/var.sch 515 */
								bool_t BgL_res2172z00_3942;

								{	/* Ast/var.sch 515 */
									obj_t BgL_oclassz00_3943;

									{	/* Ast/var.sch 515 */
										obj_t BgL_arg1815z00_3944;
										long BgL_arg1816z00_3945;

										BgL_arg1815z00_3944 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Ast/var.sch 515 */
											long BgL_arg1817z00_3946;

											BgL_arg1817z00_3946 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3938);
											BgL_arg1816z00_3945 = (BgL_arg1817z00_3946 - OBJECT_TYPE);
										}
										BgL_oclassz00_3943 =
											VECTOR_REF(BgL_arg1815z00_3944, BgL_arg1816z00_3945);
									}
									{	/* Ast/var.sch 515 */
										bool_t BgL__ortest_1115z00_3947;

										BgL__ortest_1115z00_3947 =
											(BgL_classz00_3937 == BgL_oclassz00_3943);
										if (BgL__ortest_1115z00_3947)
											{	/* Ast/var.sch 515 */
												BgL_res2172z00_3942 = BgL__ortest_1115z00_3947;
											}
										else
											{	/* Ast/var.sch 515 */
												long BgL_odepthz00_3948;

												{	/* Ast/var.sch 515 */
													obj_t BgL_arg1804z00_3949;

													BgL_arg1804z00_3949 = (BgL_oclassz00_3943);
													BgL_odepthz00_3948 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3949);
												}
												if ((2L < BgL_odepthz00_3948))
													{	/* Ast/var.sch 515 */
														obj_t BgL_arg1802z00_3950;

														{	/* Ast/var.sch 515 */
															obj_t BgL_arg1803z00_3951;

															BgL_arg1803z00_3951 = (BgL_oclassz00_3943);
															BgL_arg1802z00_3950 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3951,
																2L);
														}
														BgL_res2172z00_3942 =
															(BgL_arg1802z00_3950 == BgL_classz00_3937);
													}
												else
													{	/* Ast/var.sch 515 */
														BgL_res2172z00_3942 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2172z00_3942;
							}
					}
				else
					{	/* Ast/var.sch 515 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &sexit? */
	obj_t BGl_z62sexitzf3z91zzast_varz00(obj_t BgL_envz00_3030,
		obj_t BgL_objz00_3031)
	{
		{	/* Ast/var.sch 515 */
			return BBOOL(BGl_sexitzf3zf3zzast_varz00(BgL_objz00_3031));
		}

	}



/* sexit-nil */
	BGL_EXPORTED_DEF BgL_sexitz00_bglt BGl_sexitzd2nilzd2zzast_varz00(void)
	{
		{	/* Ast/var.sch 516 */
			{	/* Ast/var.sch 516 */
				obj_t BgL_classz00_2041;

				BgL_classz00_2041 = BGl_sexitz00zzast_varz00;
				{	/* Ast/var.sch 516 */
					obj_t BgL__ortest_1117z00_2042;

					BgL__ortest_1117z00_2042 = BGL_CLASS_NIL(BgL_classz00_2041);
					if (CBOOL(BgL__ortest_1117z00_2042))
						{	/* Ast/var.sch 516 */
							return ((BgL_sexitz00_bglt) BgL__ortest_1117z00_2042);
						}
					else
						{	/* Ast/var.sch 516 */
							return
								((BgL_sexitz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2041));
						}
				}
			}
		}

	}



/* &sexit-nil */
	BgL_sexitz00_bglt BGl_z62sexitzd2nilzb0zzast_varz00(obj_t BgL_envz00_3032)
	{
		{	/* Ast/var.sch 516 */
			return BGl_sexitzd2nilzd2zzast_varz00();
		}

	}



/* sexit-detached? */
	BGL_EXPORTED_DEF bool_t
		BGl_sexitzd2detachedzf3z21zzast_varz00(BgL_sexitz00_bglt BgL_oz00_388)
	{
		{	/* Ast/var.sch 517 */
			return (((BgL_sexitz00_bglt) COBJECT(BgL_oz00_388))->BgL_detachedzf3zf3);
		}

	}



/* &sexit-detached? */
	obj_t BGl_z62sexitzd2detachedzf3z43zzast_varz00(obj_t BgL_envz00_3033,
		obj_t BgL_oz00_3034)
	{
		{	/* Ast/var.sch 517 */
			return
				BBOOL(BGl_sexitzd2detachedzf3z21zzast_varz00(
					((BgL_sexitz00_bglt) BgL_oz00_3034)));
		}

	}



/* sexit-detached?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sexitzd2detachedzf3zd2setz12ze1zzast_varz00(BgL_sexitz00_bglt
		BgL_oz00_389, bool_t BgL_vz00_390)
	{
		{	/* Ast/var.sch 518 */
			return
				((((BgL_sexitz00_bglt) COBJECT(BgL_oz00_389))->BgL_detachedzf3zf3) =
				((bool_t) BgL_vz00_390), BUNSPEC);
		}

	}



/* &sexit-detached?-set! */
	obj_t BGl_z62sexitzd2detachedzf3zd2setz12z83zzast_varz00(obj_t
		BgL_envz00_3035, obj_t BgL_oz00_3036, obj_t BgL_vz00_3037)
	{
		{	/* Ast/var.sch 518 */
			return
				BGl_sexitzd2detachedzf3zd2setz12ze1zzast_varz00(
				((BgL_sexitz00_bglt) BgL_oz00_3036), CBOOL(BgL_vz00_3037));
		}

	}



/* sexit-handler */
	BGL_EXPORTED_DEF obj_t BGl_sexitzd2handlerzd2zzast_varz00(BgL_sexitz00_bglt
		BgL_oz00_391)
	{
		{	/* Ast/var.sch 519 */
			return (((BgL_sexitz00_bglt) COBJECT(BgL_oz00_391))->BgL_handlerz00);
		}

	}



/* &sexit-handler */
	obj_t BGl_z62sexitzd2handlerzb0zzast_varz00(obj_t BgL_envz00_3038,
		obj_t BgL_oz00_3039)
	{
		{	/* Ast/var.sch 519 */
			return
				BGl_sexitzd2handlerzd2zzast_varz00(((BgL_sexitz00_bglt) BgL_oz00_3039));
		}

	}



/* sexit-handler-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sexitzd2handlerzd2setz12z12zzast_varz00(BgL_sexitz00_bglt BgL_oz00_392,
		obj_t BgL_vz00_393)
	{
		{	/* Ast/var.sch 520 */
			return
				((((BgL_sexitz00_bglt) COBJECT(BgL_oz00_392))->BgL_handlerz00) =
				((obj_t) BgL_vz00_393), BUNSPEC);
		}

	}



/* &sexit-handler-set! */
	obj_t BGl_z62sexitzd2handlerzd2setz12z70zzast_varz00(obj_t BgL_envz00_3040,
		obj_t BgL_oz00_3041, obj_t BgL_vz00_3042)
	{
		{	/* Ast/var.sch 520 */
			return
				BGl_sexitzd2handlerzd2setz12z12zzast_varz00(
				((BgL_sexitz00_bglt) BgL_oz00_3041), BgL_vz00_3042);
		}

	}



/* make-feffect */
	BGL_EXPORTED_DEF BgL_feffectz00_bglt BGl_makezd2feffectzd2zzast_varz00(obj_t
		BgL_read1185z00_394, obj_t BgL_write1186z00_395)
	{
		{	/* Ast/var.sch 523 */
			{	/* Ast/var.sch 523 */
				BgL_feffectz00_bglt BgL_new1209z00_3952;

				{	/* Ast/var.sch 523 */
					BgL_feffectz00_bglt BgL_new1208z00_3953;

					BgL_new1208z00_3953 =
						((BgL_feffectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_feffectz00_bgl))));
					{	/* Ast/var.sch 523 */
						long BgL_arg1318z00_3954;

						BgL_arg1318z00_3954 = BGL_CLASS_NUM(BGl_feffectz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1208z00_3953), BgL_arg1318z00_3954);
					}
					BgL_new1209z00_3952 = BgL_new1208z00_3953;
				}
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1209z00_3952))->BgL_readz00) =
					((obj_t) BgL_read1185z00_394), BUNSPEC);
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1209z00_3952))->BgL_writez00) =
					((obj_t) BgL_write1186z00_395), BUNSPEC);
				return BgL_new1209z00_3952;
			}
		}

	}



/* &make-feffect */
	BgL_feffectz00_bglt BGl_z62makezd2feffectzb0zzast_varz00(obj_t
		BgL_envz00_3043, obj_t BgL_read1185z00_3044, obj_t BgL_write1186z00_3045)
	{
		{	/* Ast/var.sch 523 */
			return
				BGl_makezd2feffectzd2zzast_varz00(BgL_read1185z00_3044,
				BgL_write1186z00_3045);
		}

	}



/* feffect? */
	BGL_EXPORTED_DEF bool_t BGl_feffectzf3zf3zzast_varz00(obj_t BgL_objz00_396)
	{
		{	/* Ast/var.sch 524 */
			{	/* Ast/var.sch 524 */
				obj_t BgL_classz00_3955;

				BgL_classz00_3955 = BGl_feffectz00zzast_varz00;
				if (BGL_OBJECTP(BgL_objz00_396))
					{	/* Ast/var.sch 524 */
						BgL_objectz00_bglt BgL_arg1807z00_3956;

						BgL_arg1807z00_3956 = (BgL_objectz00_bglt) (BgL_objz00_396);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Ast/var.sch 524 */
								long BgL_idxz00_3957;

								BgL_idxz00_3957 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3956);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3957 + 1L)) == BgL_classz00_3955);
							}
						else
							{	/* Ast/var.sch 524 */
								bool_t BgL_res2173z00_3960;

								{	/* Ast/var.sch 524 */
									obj_t BgL_oclassz00_3961;

									{	/* Ast/var.sch 524 */
										obj_t BgL_arg1815z00_3962;
										long BgL_arg1816z00_3963;

										BgL_arg1815z00_3962 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Ast/var.sch 524 */
											long BgL_arg1817z00_3964;

											BgL_arg1817z00_3964 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3956);
											BgL_arg1816z00_3963 = (BgL_arg1817z00_3964 - OBJECT_TYPE);
										}
										BgL_oclassz00_3961 =
											VECTOR_REF(BgL_arg1815z00_3962, BgL_arg1816z00_3963);
									}
									{	/* Ast/var.sch 524 */
										bool_t BgL__ortest_1115z00_3965;

										BgL__ortest_1115z00_3965 =
											(BgL_classz00_3955 == BgL_oclassz00_3961);
										if (BgL__ortest_1115z00_3965)
											{	/* Ast/var.sch 524 */
												BgL_res2173z00_3960 = BgL__ortest_1115z00_3965;
											}
										else
											{	/* Ast/var.sch 524 */
												long BgL_odepthz00_3966;

												{	/* Ast/var.sch 524 */
													obj_t BgL_arg1804z00_3967;

													BgL_arg1804z00_3967 = (BgL_oclassz00_3961);
													BgL_odepthz00_3966 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3967);
												}
												if ((1L < BgL_odepthz00_3966))
													{	/* Ast/var.sch 524 */
														obj_t BgL_arg1802z00_3968;

														{	/* Ast/var.sch 524 */
															obj_t BgL_arg1803z00_3969;

															BgL_arg1803z00_3969 = (BgL_oclassz00_3961);
															BgL_arg1802z00_3968 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3969,
																1L);
														}
														BgL_res2173z00_3960 =
															(BgL_arg1802z00_3968 == BgL_classz00_3955);
													}
												else
													{	/* Ast/var.sch 524 */
														BgL_res2173z00_3960 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2173z00_3960;
							}
					}
				else
					{	/* Ast/var.sch 524 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &feffect? */
	obj_t BGl_z62feffectzf3z91zzast_varz00(obj_t BgL_envz00_3046,
		obj_t BgL_objz00_3047)
	{
		{	/* Ast/var.sch 524 */
			return BBOOL(BGl_feffectzf3zf3zzast_varz00(BgL_objz00_3047));
		}

	}



/* feffect-nil */
	BGL_EXPORTED_DEF BgL_feffectz00_bglt BGl_feffectzd2nilzd2zzast_varz00(void)
	{
		{	/* Ast/var.sch 525 */
			{	/* Ast/var.sch 525 */
				obj_t BgL_classz00_2083;

				BgL_classz00_2083 = BGl_feffectz00zzast_varz00;
				{	/* Ast/var.sch 525 */
					obj_t BgL__ortest_1117z00_2084;

					BgL__ortest_1117z00_2084 = BGL_CLASS_NIL(BgL_classz00_2083);
					if (CBOOL(BgL__ortest_1117z00_2084))
						{	/* Ast/var.sch 525 */
							return ((BgL_feffectz00_bglt) BgL__ortest_1117z00_2084);
						}
					else
						{	/* Ast/var.sch 525 */
							return
								((BgL_feffectz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2083));
						}
				}
			}
		}

	}



/* &feffect-nil */
	BgL_feffectz00_bglt BGl_z62feffectzd2nilzb0zzast_varz00(obj_t BgL_envz00_3048)
	{
		{	/* Ast/var.sch 525 */
			return BGl_feffectzd2nilzd2zzast_varz00();
		}

	}



/* feffect-write */
	BGL_EXPORTED_DEF obj_t BGl_feffectzd2writezd2zzast_varz00(BgL_feffectz00_bglt
		BgL_oz00_397)
	{
		{	/* Ast/var.sch 526 */
			return (((BgL_feffectz00_bglt) COBJECT(BgL_oz00_397))->BgL_writez00);
		}

	}



/* &feffect-write */
	obj_t BGl_z62feffectzd2writezb0zzast_varz00(obj_t BgL_envz00_3049,
		obj_t BgL_oz00_3050)
	{
		{	/* Ast/var.sch 526 */
			return
				BGl_feffectzd2writezd2zzast_varz00(
				((BgL_feffectz00_bglt) BgL_oz00_3050));
		}

	}



/* feffect-write-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_feffectzd2writezd2setz12z12zzast_varz00(BgL_feffectz00_bglt
		BgL_oz00_398, obj_t BgL_vz00_399)
	{
		{	/* Ast/var.sch 527 */
			return
				((((BgL_feffectz00_bglt) COBJECT(BgL_oz00_398))->BgL_writez00) =
				((obj_t) BgL_vz00_399), BUNSPEC);
		}

	}



/* &feffect-write-set! */
	obj_t BGl_z62feffectzd2writezd2setz12z70zzast_varz00(obj_t BgL_envz00_3051,
		obj_t BgL_oz00_3052, obj_t BgL_vz00_3053)
	{
		{	/* Ast/var.sch 527 */
			return
				BGl_feffectzd2writezd2setz12z12zzast_varz00(
				((BgL_feffectz00_bglt) BgL_oz00_3052), BgL_vz00_3053);
		}

	}



/* feffect-read */
	BGL_EXPORTED_DEF obj_t BGl_feffectzd2readzd2zzast_varz00(BgL_feffectz00_bglt
		BgL_oz00_400)
	{
		{	/* Ast/var.sch 528 */
			return (((BgL_feffectz00_bglt) COBJECT(BgL_oz00_400))->BgL_readz00);
		}

	}



/* &feffect-read */
	obj_t BGl_z62feffectzd2readzb0zzast_varz00(obj_t BgL_envz00_3054,
		obj_t BgL_oz00_3055)
	{
		{	/* Ast/var.sch 528 */
			return
				BGl_feffectzd2readzd2zzast_varz00(
				((BgL_feffectz00_bglt) BgL_oz00_3055));
		}

	}



/* feffect-read-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_feffectzd2readzd2setz12z12zzast_varz00(BgL_feffectz00_bglt BgL_oz00_401,
		obj_t BgL_vz00_402)
	{
		{	/* Ast/var.sch 529 */
			return
				((((BgL_feffectz00_bglt) COBJECT(BgL_oz00_401))->BgL_readz00) =
				((obj_t) BgL_vz00_402), BUNSPEC);
		}

	}



/* &feffect-read-set! */
	obj_t BGl_z62feffectzd2readzd2setz12z70zzast_varz00(obj_t BgL_envz00_3056,
		obj_t BgL_oz00_3057, obj_t BgL_vz00_3058)
	{
		{	/* Ast/var.sch 529 */
			return
				BGl_feffectzd2readzd2setz12z12zzast_varz00(
				((BgL_feffectz00_bglt) BgL_oz00_3057), BgL_vz00_3058);
		}

	}



/* global-read-only? */
	BGL_EXPORTED_DEF bool_t
		BGl_globalzd2readzd2onlyzf3zf3zzast_varz00(BgL_globalz00_bglt
		BgL_globalz00_403)
	{
		{	/* Ast/var.scm 194 */
			{	/* Ast/var.scm 196 */
				obj_t BgL_tmpz00_5552;

				BgL_tmpz00_5552 =
					BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CNST_TABLE_REF(0),
					(((BgL_globalz00_bglt) COBJECT(BgL_globalz00_403))->BgL_pragmaz00));
				return PAIRP(BgL_tmpz00_5552);
			}
		}

	}



/* &global-read-only? */
	obj_t BGl_z62globalzd2readzd2onlyzf3z91zzast_varz00(obj_t BgL_envz00_3059,
		obj_t BgL_globalz00_3060)
	{
		{	/* Ast/var.scm 194 */
			return
				BBOOL(BGl_globalzd2readzd2onlyzf3zf3zzast_varz00(
					((BgL_globalz00_bglt) BgL_globalz00_3060)));
		}

	}



/* global-set-read-only! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzd2setzd2readzd2onlyz12zc0zzast_varz00(BgL_globalz00_bglt
		BgL_globalz00_404)
	{
		{	/* Ast/var.scm 201 */
			{
				obj_t BgL_auxz00_5560;

				{	/* Ast/var.scm 203 */
					obj_t BgL_arg1321z00_518;

					BgL_arg1321z00_518 =
						(((BgL_globalz00_bglt) COBJECT(BgL_globalz00_404))->BgL_pragmaz00);
					BgL_auxz00_5560 =
						MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BgL_arg1321z00_518);
				}
				return
					((((BgL_globalz00_bglt) COBJECT(BgL_globalz00_404))->BgL_pragmaz00) =
					((obj_t) BgL_auxz00_5560), BUNSPEC);
			}
		}

	}



/* &global-set-read-only! */
	obj_t BGl_z62globalzd2setzd2readzd2onlyz12za2zzast_varz00(obj_t
		BgL_envz00_3061, obj_t BgL_globalz00_3062)
	{
		{	/* Ast/var.scm 201 */
			return
				BGl_globalzd2setzd2readzd2onlyz12zc0zzast_varz00(
				((BgL_globalz00_bglt) BgL_globalz00_3062));
		}

	}



/* global-args-safe? */
	BGL_EXPORTED_DEF bool_t
		BGl_globalzd2argszd2safezf3zf3zzast_varz00(BgL_globalz00_bglt
		BgL_globalz00_405)
	{
		{	/* Ast/var.scm 208 */
			{	/* Ast/var.scm 210 */
				bool_t BgL__ortest_1213z00_520;

				{	/* Ast/var.scm 210 */
					bool_t BgL_test2857z00_5567;

					{	/* Ast/var.scm 210 */
						BgL_valuez00_bglt BgL_arg1328z00_528;

						BgL_arg1328z00_528 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_globalz00_405)))->BgL_valuez00);
						{	/* Ast/var.scm 210 */
							obj_t BgL_classz00_2085;

							BgL_classz00_2085 = BGl_cfunz00zzast_varz00;
							{	/* Ast/var.scm 210 */
								BgL_objectz00_bglt BgL_arg1807z00_2087;

								{	/* Ast/var.scm 210 */
									obj_t BgL_tmpz00_5570;

									BgL_tmpz00_5570 =
										((obj_t) ((BgL_objectz00_bglt) BgL_arg1328z00_528));
									BgL_arg1807z00_2087 = (BgL_objectz00_bglt) (BgL_tmpz00_5570);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Ast/var.scm 210 */
										long BgL_idxz00_2093;

										BgL_idxz00_2093 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2087);
										BgL_test2857z00_5567 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2093 + 3L)) == BgL_classz00_2085);
									}
								else
									{	/* Ast/var.scm 210 */
										bool_t BgL_res2174z00_2118;

										{	/* Ast/var.scm 210 */
											obj_t BgL_oclassz00_2101;

											{	/* Ast/var.scm 210 */
												obj_t BgL_arg1815z00_2109;
												long BgL_arg1816z00_2110;

												BgL_arg1815z00_2109 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Ast/var.scm 210 */
													long BgL_arg1817z00_2111;

													BgL_arg1817z00_2111 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2087);
													BgL_arg1816z00_2110 =
														(BgL_arg1817z00_2111 - OBJECT_TYPE);
												}
												BgL_oclassz00_2101 =
													VECTOR_REF(BgL_arg1815z00_2109, BgL_arg1816z00_2110);
											}
											{	/* Ast/var.scm 210 */
												bool_t BgL__ortest_1115z00_2102;

												BgL__ortest_1115z00_2102 =
													(BgL_classz00_2085 == BgL_oclassz00_2101);
												if (BgL__ortest_1115z00_2102)
													{	/* Ast/var.scm 210 */
														BgL_res2174z00_2118 = BgL__ortest_1115z00_2102;
													}
												else
													{	/* Ast/var.scm 210 */
														long BgL_odepthz00_2103;

														{	/* Ast/var.scm 210 */
															obj_t BgL_arg1804z00_2104;

															BgL_arg1804z00_2104 = (BgL_oclassz00_2101);
															BgL_odepthz00_2103 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2104);
														}
														if ((3L < BgL_odepthz00_2103))
															{	/* Ast/var.scm 210 */
																obj_t BgL_arg1802z00_2106;

																{	/* Ast/var.scm 210 */
																	obj_t BgL_arg1803z00_2107;

																	BgL_arg1803z00_2107 = (BgL_oclassz00_2101);
																	BgL_arg1802z00_2106 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2107,
																		3L);
																}
																BgL_res2174z00_2118 =
																	(BgL_arg1802z00_2106 == BgL_classz00_2085);
															}
														else
															{	/* Ast/var.scm 210 */
																BgL_res2174z00_2118 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2857z00_5567 = BgL_res2174z00_2118;
									}
							}
						}
					}
					if (BgL_test2857z00_5567)
						{	/* Ast/var.scm 210 */
							BgL__ortest_1213z00_520 = ((bool_t) 0);
						}
					else
						{	/* Ast/var.scm 210 */
							BgL__ortest_1213z00_520 = ((bool_t) 1);
						}
				}
				if (BgL__ortest_1213z00_520)
					{	/* Ast/var.scm 210 */
						return BgL__ortest_1213z00_520;
					}
				else
					{	/* Ast/var.scm 211 */
						bool_t BgL__ortest_1214z00_521;

						if (
							(((BgL_cfunz00_bglt) COBJECT(
										((BgL_cfunz00_bglt)
											(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_globalz00_405)))->
												BgL_valuez00))))->BgL_macrozf3zf3))
							{	/* Ast/var.scm 211 */
								BgL__ortest_1214z00_521 = ((bool_t) 0);
							}
						else
							{	/* Ast/var.scm 211 */
								BgL__ortest_1214z00_521 = ((bool_t) 1);
							}
						if (BgL__ortest_1214z00_521)
							{	/* Ast/var.scm 211 */
								return BgL__ortest_1214z00_521;
							}
						else
							{	/* Ast/var.scm 211 */
								return
									CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CNST_TABLE_REF
										(1),
										(((BgL_globalz00_bglt) COBJECT(BgL_globalz00_405))->
											BgL_pragmaz00)));
							}
					}
			}
		}

	}



/* &global-args-safe? */
	obj_t BGl_z62globalzd2argszd2safezf3z91zzast_varz00(obj_t BgL_envz00_3063,
		obj_t BgL_globalz00_3064)
	{
		{	/* Ast/var.scm 208 */
			return
				BBOOL(BGl_globalzd2argszd2safezf3zf3zzast_varz00(
					((BgL_globalz00_bglt) BgL_globalz00_3064)));
		}

	}



/* fun-optional-arity */
	BGL_EXPORTED_DEF int BGl_funzd2optionalzd2arityz00zzast_varz00(BgL_funz00_bglt
		BgL_funz00_406)
	{
		{	/* Ast/var.scm 217 */
			{	/* Ast/var.scm 219 */
				bool_t BgL_test2864z00_5607;

				{	/* Ast/var.scm 219 */
					obj_t BgL_classz00_2120;

					BgL_classz00_2120 = BGl_sfunz00zzast_varz00;
					{	/* Ast/var.scm 219 */
						BgL_objectz00_bglt BgL_arg1807z00_2122;

						{	/* Ast/var.scm 219 */
							obj_t BgL_tmpz00_5608;

							BgL_tmpz00_5608 = ((obj_t) BgL_funz00_406);
							BgL_arg1807z00_2122 = (BgL_objectz00_bglt) (BgL_tmpz00_5608);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Ast/var.scm 219 */
								long BgL_idxz00_2128;

								BgL_idxz00_2128 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2122);
								BgL_test2864z00_5607 =
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_2128 + 3L)) == BgL_classz00_2120);
							}
						else
							{	/* Ast/var.scm 219 */
								bool_t BgL_res2175z00_2153;

								{	/* Ast/var.scm 219 */
									obj_t BgL_oclassz00_2136;

									{	/* Ast/var.scm 219 */
										obj_t BgL_arg1815z00_2144;
										long BgL_arg1816z00_2145;

										BgL_arg1815z00_2144 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Ast/var.scm 219 */
											long BgL_arg1817z00_2146;

											BgL_arg1817z00_2146 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2122);
											BgL_arg1816z00_2145 = (BgL_arg1817z00_2146 - OBJECT_TYPE);
										}
										BgL_oclassz00_2136 =
											VECTOR_REF(BgL_arg1815z00_2144, BgL_arg1816z00_2145);
									}
									{	/* Ast/var.scm 219 */
										bool_t BgL__ortest_1115z00_2137;

										BgL__ortest_1115z00_2137 =
											(BgL_classz00_2120 == BgL_oclassz00_2136);
										if (BgL__ortest_1115z00_2137)
											{	/* Ast/var.scm 219 */
												BgL_res2175z00_2153 = BgL__ortest_1115z00_2137;
											}
										else
											{	/* Ast/var.scm 219 */
												long BgL_odepthz00_2138;

												{	/* Ast/var.scm 219 */
													obj_t BgL_arg1804z00_2139;

													BgL_arg1804z00_2139 = (BgL_oclassz00_2136);
													BgL_odepthz00_2138 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_2139);
												}
												if ((3L < BgL_odepthz00_2138))
													{	/* Ast/var.scm 219 */
														obj_t BgL_arg1802z00_2141;

														{	/* Ast/var.scm 219 */
															obj_t BgL_arg1803z00_2142;

															BgL_arg1803z00_2142 = (BgL_oclassz00_2136);
															BgL_arg1802z00_2141 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2142,
																3L);
														}
														BgL_res2175z00_2153 =
															(BgL_arg1802z00_2141 == BgL_classz00_2120);
													}
												else
													{	/* Ast/var.scm 219 */
														BgL_res2175z00_2153 = ((bool_t) 0);
													}
											}
									}
								}
								BgL_test2864z00_5607 = BgL_res2175z00_2153;
							}
					}
				}
				if (BgL_test2864z00_5607)
					{	/* Ast/var.scm 221 */
						bool_t BgL_test2868z00_5630;

						{	/* Ast/var.scm 221 */
							obj_t BgL_tmpz00_5631;

							BgL_tmpz00_5631 =
								(((BgL_sfunz00_bglt) COBJECT(
										((BgL_sfunz00_bglt) BgL_funz00_406)))->BgL_optionalsz00);
							BgL_test2868z00_5630 = PAIRP(BgL_tmpz00_5631);
						}
						if (BgL_test2868z00_5630)
							{	/* Ast/var.scm 221 */
								return
									(int) (
									((((BgL_funz00_bglt) COBJECT(BgL_funz00_406))->BgL_arityz00) +
										1L));
							}
						else
							{	/* Ast/var.scm 221 */
								return
									(int) (
									(((BgL_funz00_bglt) COBJECT(BgL_funz00_406))->BgL_arityz00));
					}}
				else
					{	/* Ast/var.scm 219 */
						return
							(int) (
							(((BgL_funz00_bglt) COBJECT(BgL_funz00_406))->BgL_arityz00));
		}}}

	}



/* &fun-optional-arity */
	obj_t BGl_z62funzd2optionalzd2arityz62zzast_varz00(obj_t BgL_envz00_3065,
		obj_t BgL_funz00_3066)
	{
		{	/* Ast/var.scm 217 */
			return
				BINT(BGl_funzd2optionalzd2arityz00zzast_varz00(
					((BgL_funz00_bglt) BgL_funz00_3066)));
		}

	}



/* global-optional? */
	BGL_EXPORTED_DEF bool_t BGl_globalzd2optionalzf3z21zzast_varz00(obj_t
		BgL_gz00_407)
	{
		{	/* Ast/var.scm 229 */
			{	/* Ast/var.scm 230 */
				bool_t BgL_test2869z00_5645;

				{	/* Ast/var.scm 230 */
					obj_t BgL_classz00_2155;

					BgL_classz00_2155 = BGl_globalz00zzast_varz00;
					if (BGL_OBJECTP(BgL_gz00_407))
						{	/* Ast/var.scm 230 */
							BgL_objectz00_bglt BgL_arg1807z00_2157;

							BgL_arg1807z00_2157 = (BgL_objectz00_bglt) (BgL_gz00_407);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Ast/var.scm 230 */
									long BgL_idxz00_2163;

									BgL_idxz00_2163 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2157);
									BgL_test2869z00_5645 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2163 + 2L)) == BgL_classz00_2155);
								}
							else
								{	/* Ast/var.scm 230 */
									bool_t BgL_res2176z00_2188;

									{	/* Ast/var.scm 230 */
										obj_t BgL_oclassz00_2171;

										{	/* Ast/var.scm 230 */
											obj_t BgL_arg1815z00_2179;
											long BgL_arg1816z00_2180;

											BgL_arg1815z00_2179 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Ast/var.scm 230 */
												long BgL_arg1817z00_2181;

												BgL_arg1817z00_2181 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2157);
												BgL_arg1816z00_2180 =
													(BgL_arg1817z00_2181 - OBJECT_TYPE);
											}
											BgL_oclassz00_2171 =
												VECTOR_REF(BgL_arg1815z00_2179, BgL_arg1816z00_2180);
										}
										{	/* Ast/var.scm 230 */
											bool_t BgL__ortest_1115z00_2172;

											BgL__ortest_1115z00_2172 =
												(BgL_classz00_2155 == BgL_oclassz00_2171);
											if (BgL__ortest_1115z00_2172)
												{	/* Ast/var.scm 230 */
													BgL_res2176z00_2188 = BgL__ortest_1115z00_2172;
												}
											else
												{	/* Ast/var.scm 230 */
													long BgL_odepthz00_2173;

													{	/* Ast/var.scm 230 */
														obj_t BgL_arg1804z00_2174;

														BgL_arg1804z00_2174 = (BgL_oclassz00_2171);
														BgL_odepthz00_2173 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2174);
													}
													if ((2L < BgL_odepthz00_2173))
														{	/* Ast/var.scm 230 */
															obj_t BgL_arg1802z00_2176;

															{	/* Ast/var.scm 230 */
																obj_t BgL_arg1803z00_2177;

																BgL_arg1803z00_2177 = (BgL_oclassz00_2171);
																BgL_arg1802z00_2176 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2177,
																	2L);
															}
															BgL_res2176z00_2188 =
																(BgL_arg1802z00_2176 == BgL_classz00_2155);
														}
													else
														{	/* Ast/var.scm 230 */
															BgL_res2176z00_2188 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2869z00_5645 = BgL_res2176z00_2188;
								}
						}
					else
						{	/* Ast/var.scm 230 */
							BgL_test2869z00_5645 = ((bool_t) 0);
						}
				}
				if (BgL_test2869z00_5645)
					{	/* Ast/var.scm 230 */
						BGL_TAIL return
							BGl_sfunzd2optionalzf3z21zzast_varz00(
							((obj_t)
								(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_gz00_407))))->BgL_valuez00)));
					}
				else
					{	/* Ast/var.scm 230 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &global-optional? */
	obj_t BGl_z62globalzd2optionalzf3z43zzast_varz00(obj_t BgL_envz00_3067,
		obj_t BgL_gz00_3068)
	{
		{	/* Ast/var.scm 229 */
			return BBOOL(BGl_globalzd2optionalzf3z21zzast_varz00(BgL_gz00_3068));
		}

	}



/* sfun-optional? */
	BGL_EXPORTED_DEF bool_t BGl_sfunzd2optionalzf3z21zzast_varz00(obj_t
		BgL_funz00_408)
	{
		{	/* Ast/var.scm 237 */
			{	/* Ast/var.scm 238 */
				bool_t BgL_test2874z00_5675;

				{	/* Ast/var.scm 238 */
					obj_t BgL_classz00_2190;

					BgL_classz00_2190 = BGl_sfunz00zzast_varz00;
					if (BGL_OBJECTP(BgL_funz00_408))
						{	/* Ast/var.scm 238 */
							BgL_objectz00_bglt BgL_arg1807z00_2192;

							BgL_arg1807z00_2192 = (BgL_objectz00_bglt) (BgL_funz00_408);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Ast/var.scm 238 */
									long BgL_idxz00_2198;

									BgL_idxz00_2198 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2192);
									BgL_test2874z00_5675 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2198 + 3L)) == BgL_classz00_2190);
								}
							else
								{	/* Ast/var.scm 238 */
									bool_t BgL_res2177z00_2223;

									{	/* Ast/var.scm 238 */
										obj_t BgL_oclassz00_2206;

										{	/* Ast/var.scm 238 */
											obj_t BgL_arg1815z00_2214;
											long BgL_arg1816z00_2215;

											BgL_arg1815z00_2214 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Ast/var.scm 238 */
												long BgL_arg1817z00_2216;

												BgL_arg1817z00_2216 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2192);
												BgL_arg1816z00_2215 =
													(BgL_arg1817z00_2216 - OBJECT_TYPE);
											}
											BgL_oclassz00_2206 =
												VECTOR_REF(BgL_arg1815z00_2214, BgL_arg1816z00_2215);
										}
										{	/* Ast/var.scm 238 */
											bool_t BgL__ortest_1115z00_2207;

											BgL__ortest_1115z00_2207 =
												(BgL_classz00_2190 == BgL_oclassz00_2206);
											if (BgL__ortest_1115z00_2207)
												{	/* Ast/var.scm 238 */
													BgL_res2177z00_2223 = BgL__ortest_1115z00_2207;
												}
											else
												{	/* Ast/var.scm 238 */
													long BgL_odepthz00_2208;

													{	/* Ast/var.scm 238 */
														obj_t BgL_arg1804z00_2209;

														BgL_arg1804z00_2209 = (BgL_oclassz00_2206);
														BgL_odepthz00_2208 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2209);
													}
													if ((3L < BgL_odepthz00_2208))
														{	/* Ast/var.scm 238 */
															obj_t BgL_arg1802z00_2211;

															{	/* Ast/var.scm 238 */
																obj_t BgL_arg1803z00_2212;

																BgL_arg1803z00_2212 = (BgL_oclassz00_2206);
																BgL_arg1802z00_2211 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2212,
																	3L);
															}
															BgL_res2177z00_2223 =
																(BgL_arg1802z00_2211 == BgL_classz00_2190);
														}
													else
														{	/* Ast/var.scm 238 */
															BgL_res2177z00_2223 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2874z00_5675 = BgL_res2177z00_2223;
								}
						}
					else
						{	/* Ast/var.scm 238 */
							BgL_test2874z00_5675 = ((bool_t) 0);
						}
				}
				if (BgL_test2874z00_5675)
					{	/* Ast/var.scm 238 */
						obj_t BgL_tmpz00_5698;

						BgL_tmpz00_5698 =
							(((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_funz00_408)))->BgL_optionalsz00);
						return PAIRP(BgL_tmpz00_5698);
					}
				else
					{	/* Ast/var.scm 238 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &sfun-optional? */
	obj_t BGl_z62sfunzd2optionalzf3z43zzast_varz00(obj_t BgL_envz00_3069,
		obj_t BgL_funz00_3070)
	{
		{	/* Ast/var.scm 237 */
			return BBOOL(BGl_sfunzd2optionalzf3z21zzast_varz00(BgL_funz00_3070));
		}

	}



/* global-key? */
	BGL_EXPORTED_DEF bool_t BGl_globalzd2keyzf3z21zzast_varz00(obj_t BgL_gz00_409)
	{
		{	/* Ast/var.scm 243 */
			{	/* Ast/var.scm 244 */
				bool_t BgL_test2879z00_5704;

				{	/* Ast/var.scm 244 */
					obj_t BgL_classz00_2225;

					BgL_classz00_2225 = BGl_globalz00zzast_varz00;
					if (BGL_OBJECTP(BgL_gz00_409))
						{	/* Ast/var.scm 244 */
							BgL_objectz00_bglt BgL_arg1807z00_2227;

							BgL_arg1807z00_2227 = (BgL_objectz00_bglt) (BgL_gz00_409);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Ast/var.scm 244 */
									long BgL_idxz00_2233;

									BgL_idxz00_2233 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2227);
									BgL_test2879z00_5704 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2233 + 2L)) == BgL_classz00_2225);
								}
							else
								{	/* Ast/var.scm 244 */
									bool_t BgL_res2178z00_2258;

									{	/* Ast/var.scm 244 */
										obj_t BgL_oclassz00_2241;

										{	/* Ast/var.scm 244 */
											obj_t BgL_arg1815z00_2249;
											long BgL_arg1816z00_2250;

											BgL_arg1815z00_2249 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Ast/var.scm 244 */
												long BgL_arg1817z00_2251;

												BgL_arg1817z00_2251 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2227);
												BgL_arg1816z00_2250 =
													(BgL_arg1817z00_2251 - OBJECT_TYPE);
											}
											BgL_oclassz00_2241 =
												VECTOR_REF(BgL_arg1815z00_2249, BgL_arg1816z00_2250);
										}
										{	/* Ast/var.scm 244 */
											bool_t BgL__ortest_1115z00_2242;

											BgL__ortest_1115z00_2242 =
												(BgL_classz00_2225 == BgL_oclassz00_2241);
											if (BgL__ortest_1115z00_2242)
												{	/* Ast/var.scm 244 */
													BgL_res2178z00_2258 = BgL__ortest_1115z00_2242;
												}
											else
												{	/* Ast/var.scm 244 */
													long BgL_odepthz00_2243;

													{	/* Ast/var.scm 244 */
														obj_t BgL_arg1804z00_2244;

														BgL_arg1804z00_2244 = (BgL_oclassz00_2241);
														BgL_odepthz00_2243 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2244);
													}
													if ((2L < BgL_odepthz00_2243))
														{	/* Ast/var.scm 244 */
															obj_t BgL_arg1802z00_2246;

															{	/* Ast/var.scm 244 */
																obj_t BgL_arg1803z00_2247;

																BgL_arg1803z00_2247 = (BgL_oclassz00_2241);
																BgL_arg1802z00_2246 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2247,
																	2L);
															}
															BgL_res2178z00_2258 =
																(BgL_arg1802z00_2246 == BgL_classz00_2225);
														}
													else
														{	/* Ast/var.scm 244 */
															BgL_res2178z00_2258 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2879z00_5704 = BgL_res2178z00_2258;
								}
						}
					else
						{	/* Ast/var.scm 244 */
							BgL_test2879z00_5704 = ((bool_t) 0);
						}
				}
				if (BgL_test2879z00_5704)
					{	/* Ast/var.scm 244 */
						BGL_TAIL return
							BGl_sfunzd2keyzf3z21zzast_varz00(
							((obj_t)
								(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_gz00_409))))->BgL_valuez00)));
					}
				else
					{	/* Ast/var.scm 244 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &global-key? */
	obj_t BGl_z62globalzd2keyzf3z43zzast_varz00(obj_t BgL_envz00_3071,
		obj_t BgL_gz00_3072)
	{
		{	/* Ast/var.scm 243 */
			return BBOOL(BGl_globalzd2keyzf3z21zzast_varz00(BgL_gz00_3072));
		}

	}



/* sfun-key? */
	BGL_EXPORTED_DEF bool_t BGl_sfunzd2keyzf3z21zzast_varz00(obj_t BgL_funz00_410)
	{
		{	/* Ast/var.scm 251 */
			{	/* Ast/var.scm 252 */
				bool_t BgL_test2884z00_5734;

				{	/* Ast/var.scm 252 */
					obj_t BgL_classz00_2260;

					BgL_classz00_2260 = BGl_sfunz00zzast_varz00;
					if (BGL_OBJECTP(BgL_funz00_410))
						{	/* Ast/var.scm 252 */
							BgL_objectz00_bglt BgL_arg1807z00_2262;

							BgL_arg1807z00_2262 = (BgL_objectz00_bglt) (BgL_funz00_410);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Ast/var.scm 252 */
									long BgL_idxz00_2268;

									BgL_idxz00_2268 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2262);
									BgL_test2884z00_5734 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2268 + 3L)) == BgL_classz00_2260);
								}
							else
								{	/* Ast/var.scm 252 */
									bool_t BgL_res2179z00_2293;

									{	/* Ast/var.scm 252 */
										obj_t BgL_oclassz00_2276;

										{	/* Ast/var.scm 252 */
											obj_t BgL_arg1815z00_2284;
											long BgL_arg1816z00_2285;

											BgL_arg1815z00_2284 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Ast/var.scm 252 */
												long BgL_arg1817z00_2286;

												BgL_arg1817z00_2286 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2262);
												BgL_arg1816z00_2285 =
													(BgL_arg1817z00_2286 - OBJECT_TYPE);
											}
											BgL_oclassz00_2276 =
												VECTOR_REF(BgL_arg1815z00_2284, BgL_arg1816z00_2285);
										}
										{	/* Ast/var.scm 252 */
											bool_t BgL__ortest_1115z00_2277;

											BgL__ortest_1115z00_2277 =
												(BgL_classz00_2260 == BgL_oclassz00_2276);
											if (BgL__ortest_1115z00_2277)
												{	/* Ast/var.scm 252 */
													BgL_res2179z00_2293 = BgL__ortest_1115z00_2277;
												}
											else
												{	/* Ast/var.scm 252 */
													long BgL_odepthz00_2278;

													{	/* Ast/var.scm 252 */
														obj_t BgL_arg1804z00_2279;

														BgL_arg1804z00_2279 = (BgL_oclassz00_2276);
														BgL_odepthz00_2278 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2279);
													}
													if ((3L < BgL_odepthz00_2278))
														{	/* Ast/var.scm 252 */
															obj_t BgL_arg1802z00_2281;

															{	/* Ast/var.scm 252 */
																obj_t BgL_arg1803z00_2282;

																BgL_arg1803z00_2282 = (BgL_oclassz00_2276);
																BgL_arg1802z00_2281 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2282,
																	3L);
															}
															BgL_res2179z00_2293 =
																(BgL_arg1802z00_2281 == BgL_classz00_2260);
														}
													else
														{	/* Ast/var.scm 252 */
															BgL_res2179z00_2293 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2884z00_5734 = BgL_res2179z00_2293;
								}
						}
					else
						{	/* Ast/var.scm 252 */
							BgL_test2884z00_5734 = ((bool_t) 0);
						}
				}
				if (BgL_test2884z00_5734)
					{	/* Ast/var.scm 252 */
						obj_t BgL_tmpz00_5757;

						BgL_tmpz00_5757 =
							(((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_funz00_410)))->BgL_keysz00);
						return PAIRP(BgL_tmpz00_5757);
					}
				else
					{	/* Ast/var.scm 252 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &sfun-key? */
	obj_t BGl_z62sfunzd2keyzf3z43zzast_varz00(obj_t BgL_envz00_3073,
		obj_t BgL_funz00_3074)
	{
		{	/* Ast/var.scm 251 */
			return BBOOL(BGl_sfunzd2keyzf3z21zzast_varz00(BgL_funz00_3074));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_varz00(void)
	{
		{	/* Ast/var.scm 14 */
			{	/* Ast/var.scm 21 */
				obj_t BgL_arg1351z00_547;
				obj_t BgL_arg1352z00_548;

				{	/* Ast/var.scm 21 */
					obj_t BgL_v1249z00_558;

					BgL_v1249z00_558 = create_vector(0L);
					BgL_arg1351z00_547 = BgL_v1249z00_558;
				}
				{	/* Ast/var.scm 21 */
					obj_t BgL_v1250z00_559;

					BgL_v1250z00_559 = create_vector(0L);
					BgL_arg1352z00_548 = BgL_v1250z00_559;
				}
				BGl_valuez00zzast_varz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(2),
					CNST_TABLE_REF(3), BGl_objectz00zz__objectz00, 3894L,
					BGl_proc2182z00zzast_varz00, BGl_proc2181z00zzast_varz00, BFALSE,
					BGl_proc2180z00zzast_varz00, BFALSE, BgL_arg1351z00_547,
					BgL_arg1352z00_548);
			}
			{	/* Ast/var.scm 23 */
				obj_t BgL_arg1375z00_566;
				obj_t BgL_arg1376z00_567;

				{	/* Ast/var.scm 23 */
					obj_t BgL_v1251z00_587;

					BgL_v1251z00_587 = create_vector(10L);
					{	/* Ast/var.scm 23 */
						obj_t BgL_arg1408z00_588;

						BgL_arg1408z00_588 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc2184z00zzast_varz00, BGl_proc2183z00zzast_varz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1251z00_587, 0L, BgL_arg1408z00_588);
					}
					{	/* Ast/var.scm 23 */
						obj_t BgL_arg1434z00_598;

						BgL_arg1434z00_598 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc2187z00zzast_varz00, BGl_proc2186z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2185z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1251z00_587, 1L, BgL_arg1434z00_598);
					}
					{	/* Ast/var.scm 23 */
						obj_t BgL_arg1472z00_611;

						BgL_arg1472z00_611 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc2189z00zzast_varz00, BGl_proc2188z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1251z00_587, 2L, BgL_arg1472z00_611);
					}
					{	/* Ast/var.scm 23 */
						obj_t BgL_arg1489z00_621;

						BgL_arg1489z00_621 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(2),
							BGl_proc2191z00zzast_varz00, BGl_proc2190z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_valuez00zzast_varz00);
						VECTOR_SET(BgL_v1251z00_587, 3L, BgL_arg1489z00_621);
					}
					{	/* Ast/var.scm 23 */
						obj_t BgL_arg1513z00_631;

						BgL_arg1513z00_631 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(9),
							BGl_proc2194z00zzast_varz00, BGl_proc2193z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2192z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1251z00_587, 4L, BgL_arg1513z00_631);
					}
					{	/* Ast/var.scm 23 */
						obj_t BgL_arg1540z00_644;

						BgL_arg1540z00_644 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(10),
							BGl_proc2197z00zzast_varz00, BGl_proc2196z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2195z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1251z00_587, 5L, BgL_arg1540z00_644);
					}
					{	/* Ast/var.scm 23 */
						obj_t BgL_arg1559z00_657;

						BgL_arg1559z00_657 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(11),
							BGl_proc2200z00zzast_varz00, BGl_proc2199z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2198z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1251z00_587, 6L, BgL_arg1559z00_657);
					}
					{	/* Ast/var.scm 23 */
						obj_t BgL_arg1571z00_670;

						BgL_arg1571z00_670 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(12),
							BGl_proc2203z00zzast_varz00, BGl_proc2202z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2201z00zzast_varz00,
							CNST_TABLE_REF(13));
						VECTOR_SET(BgL_v1251z00_587, 7L, BgL_arg1571z00_670);
					}
					{	/* Ast/var.scm 23 */
						obj_t BgL_arg1584z00_683;

						BgL_arg1584z00_683 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(14),
							BGl_proc2206z00zzast_varz00, BGl_proc2205z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2204z00zzast_varz00,
							CNST_TABLE_REF(13));
						VECTOR_SET(BgL_v1251z00_587, 8L, BgL_arg1584z00_683);
					}
					{	/* Ast/var.scm 23 */
						obj_t BgL_arg1595z00_696;

						BgL_arg1595z00_696 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(15),
							BGl_proc2209z00zzast_varz00, BGl_proc2208z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2207z00zzast_varz00,
							CNST_TABLE_REF(16));
						VECTOR_SET(BgL_v1251z00_587, 9L, BgL_arg1595z00_696);
					}
					BgL_arg1375z00_566 = BgL_v1251z00_587;
				}
				{	/* Ast/var.scm 23 */
					obj_t BgL_v1252z00_709;

					BgL_v1252z00_709 = create_vector(0L);
					BgL_arg1376z00_567 = BgL_v1252z00_709;
				}
				BGl_variablez00zzast_varz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(17),
					CNST_TABLE_REF(3), BGl_objectz00zz__objectz00, 41390L,
					BGl_proc2212z00zzast_varz00, BGl_proc2211z00zzast_varz00, BFALSE,
					BGl_proc2210z00zzast_varz00, BFALSE, BgL_arg1375z00_566,
					BgL_arg1376z00_567);
			}
			{	/* Ast/var.scm 46 */
				obj_t BgL_arg1616z00_716;
				obj_t BgL_arg1625z00_717;

				{	/* Ast/var.scm 46 */
					obj_t BgL_v1253z00_747;

					BgL_v1253z00_747 = create_vector(10L);
					{	/* Ast/var.scm 46 */
						obj_t BgL_arg1642z00_748;

						BgL_arg1642z00_748 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(18),
							BGl_proc2214z00zzast_varz00, BGl_proc2213z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1253z00_747, 0L, BgL_arg1642z00_748);
					}
					{	/* Ast/var.scm 46 */
						obj_t BgL_arg1654z00_758;

						BgL_arg1654z00_758 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(19),
							BGl_proc2216z00zzast_varz00, BGl_proc2215z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1253z00_747, 1L, BgL_arg1654z00_758);
					}
					{	/* Ast/var.scm 46 */
						obj_t BgL_arg1675z00_768;

						BgL_arg1675z00_768 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(20),
							BGl_proc2219z00zzast_varz00, BGl_proc2218z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2217z00zzast_varz00,
							CNST_TABLE_REF(16));
						VECTOR_SET(BgL_v1253z00_747, 2L, BgL_arg1675z00_768);
					}
					{	/* Ast/var.scm 46 */
						obj_t BgL_arg1692z00_781;

						BgL_arg1692z00_781 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(21),
							BGl_proc2222z00zzast_varz00, BGl_proc2221z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2220z00zzast_varz00,
							CNST_TABLE_REF(16));
						VECTOR_SET(BgL_v1253z00_747, 3L, BgL_arg1692z00_781);
					}
					{	/* Ast/var.scm 46 */
						obj_t BgL_arg1705z00_794;

						BgL_arg1705z00_794 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(22),
							BGl_proc2225z00zzast_varz00, BGl_proc2224z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2223z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1253z00_747, 4L, BgL_arg1705z00_794);
					}
					{	/* Ast/var.scm 46 */
						obj_t BgL_arg1714z00_807;

						BgL_arg1714z00_807 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(23),
							BGl_proc2228z00zzast_varz00, BGl_proc2227z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2226z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1253z00_747, 5L, BgL_arg1714z00_807);
					}
					{	/* Ast/var.scm 46 */
						obj_t BgL_arg1724z00_820;

						BgL_arg1724z00_820 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(24),
							BGl_proc2230z00zzast_varz00, BGl_proc2229z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1253z00_747, 6L, BgL_arg1724z00_820);
					}
					{	/* Ast/var.scm 46 */
						obj_t BgL_arg1737z00_830;

						BgL_arg1737z00_830 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(25),
							BGl_proc2232z00zzast_varz00, BGl_proc2231z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(26));
						VECTOR_SET(BgL_v1253z00_747, 7L, BgL_arg1737z00_830);
					}
					{	/* Ast/var.scm 46 */
						obj_t BgL_arg1746z00_840;

						BgL_arg1746z00_840 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(27),
							BGl_proc2235z00zzast_varz00, BGl_proc2234z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2233z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1253z00_747, 8L, BgL_arg1746z00_840);
					}
					{	/* Ast/var.scm 46 */
						obj_t BgL_arg1753z00_853;

						BgL_arg1753z00_853 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(28),
							BGl_proc2238z00zzast_varz00, BGl_proc2237z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2236z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1253z00_747, 9L, BgL_arg1753z00_853);
					}
					BgL_arg1616z00_716 = BgL_v1253z00_747;
				}
				{	/* Ast/var.scm 46 */
					obj_t BgL_v1254z00_866;

					BgL_v1254z00_866 = create_vector(0L);
					BgL_arg1625z00_717 = BgL_v1254z00_866;
				}
				BGl_globalz00zzast_varz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(29),
					CNST_TABLE_REF(3), BGl_variablez00zzast_varz00, 49842L,
					BGl_proc2241z00zzast_varz00, BGl_proc2240z00zzast_varz00, BFALSE,
					BGl_proc2239z00zzast_varz00, BFALSE, BgL_arg1616z00_716,
					BgL_arg1625z00_717);
			}
			{	/* Ast/var.scm 75 */
				obj_t BgL_arg1771z00_873;
				obj_t BgL_arg1773z00_874;

				{	/* Ast/var.scm 75 */
					obj_t BgL_v1255z00_897;

					BgL_v1255z00_897 = create_vector(3L);
					{	/* Ast/var.scm 75 */
						obj_t BgL_arg1805z00_898;

						BgL_arg1805z00_898 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(30),
							BGl_proc2243z00zzast_varz00, BGl_proc2242z00zzast_varz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(13));
						VECTOR_SET(BgL_v1255z00_897, 0L, BgL_arg1805z00_898);
					}
					{	/* Ast/var.scm 75 */
						obj_t BgL_arg1812z00_908;

						BgL_arg1812z00_908 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(31),
							BGl_proc2246z00zzast_varz00, BGl_proc2245z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2244z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1255z00_897, 1L, BgL_arg1812z00_908);
					}
					{	/* Ast/var.scm 75 */
						obj_t BgL_arg1831z00_921;

						BgL_arg1831z00_921 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(32),
							BGl_proc2249z00zzast_varz00, BGl_proc2248z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2247z00zzast_varz00,
							CNST_TABLE_REF(16));
						VECTOR_SET(BgL_v1255z00_897, 2L, BgL_arg1831z00_921);
					}
					BgL_arg1771z00_873 = BgL_v1255z00_897;
				}
				{	/* Ast/var.scm 75 */
					obj_t BgL_v1256z00_934;

					BgL_v1256z00_934 = create_vector(0L);
					BgL_arg1773z00_874 = BgL_v1256z00_934;
				}
				BGl_localz00zzast_varz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(33),
					CNST_TABLE_REF(3), BGl_variablez00zzast_varz00, 825L,
					BGl_proc2252z00zzast_varz00, BGl_proc2251z00zzast_varz00, BFALSE,
					BGl_proc2250z00zzast_varz00, BFALSE, BgL_arg1771z00_873,
					BgL_arg1773z00_874);
			}
			{	/* Ast/var.scm 84 */
				obj_t BgL_arg1842z00_941;
				obj_t BgL_arg1843z00_942;

				{	/* Ast/var.scm 84 */
					obj_t BgL_v1257z00_962;

					BgL_v1257z00_962 = create_vector(10L);
					{	/* Ast/var.scm 84 */
						obj_t BgL_arg1849z00_963;

						BgL_arg1849z00_963 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(34),
							BGl_proc2254z00zzast_varz00, BGl_proc2253z00zzast_varz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(13));
						VECTOR_SET(BgL_v1257z00_962, 0L, BgL_arg1849z00_963);
					}
					{	/* Ast/var.scm 84 */
						obj_t BgL_arg1854z00_973;

						BgL_arg1854z00_973 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(35),
							BGl_proc2257z00zzast_varz00, BGl_proc2256z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2255z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1257z00_962, 1L, BgL_arg1854z00_973);
					}
					{	/* Ast/var.scm 84 */
						obj_t BgL_arg1862z00_986;

						BgL_arg1862z00_986 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(36),
							BGl_proc2260z00zzast_varz00, BGl_proc2259z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2258z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1257z00_962, 2L, BgL_arg1862z00_986);
					}
					{	/* Ast/var.scm 84 */
						obj_t BgL_arg1870z00_999;

						BgL_arg1870z00_999 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(37),
							BGl_proc2263z00zzast_varz00, BGl_proc2262z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2261z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1257z00_962, 3L, BgL_arg1870z00_999);
					}
					{	/* Ast/var.scm 84 */
						obj_t BgL_arg1878z00_1012;

						BgL_arg1878z00_1012 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(38),
							BGl_proc2266z00zzast_varz00, BGl_proc2265z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2264z00zzast_varz00,
							CNST_TABLE_REF(16));
						VECTOR_SET(BgL_v1257z00_962, 4L, BgL_arg1878z00_1012);
					}
					{	/* Ast/var.scm 84 */
						obj_t BgL_arg1887z00_1025;

						BgL_arg1887z00_1025 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(39),
							BGl_proc2269z00zzast_varz00, BGl_proc2268z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2267z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1257z00_962, 5L, BgL_arg1887z00_1025);
					}
					{	/* Ast/var.scm 84 */
						obj_t BgL_arg1894z00_1038;

						BgL_arg1894z00_1038 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(40),
							BGl_proc2272z00zzast_varz00, BGl_proc2271z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2270z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1257z00_962, 6L, BgL_arg1894z00_1038);
					}
					{	/* Ast/var.scm 84 */
						obj_t BgL_arg1902z00_1051;

						BgL_arg1902z00_1051 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(41),
							BGl_proc2275z00zzast_varz00, BGl_proc2274z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2273z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1257z00_962, 7L, BgL_arg1902z00_1051);
					}
					{	/* Ast/var.scm 84 */
						obj_t BgL_arg1910z00_1064;

						BgL_arg1910z00_1064 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(42),
							BGl_proc2278z00zzast_varz00, BGl_proc2277z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2276z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1257z00_962, 8L, BgL_arg1910z00_1064);
					}
					{	/* Ast/var.scm 84 */
						obj_t BgL_arg1917z00_1077;

						BgL_arg1917z00_1077 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(43),
							BGl_proc2281z00zzast_varz00, BGl_proc2280z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2279z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1257z00_962, 9L, BgL_arg1917z00_1077);
					}
					BgL_arg1842z00_941 = BgL_v1257z00_962;
				}
				{	/* Ast/var.scm 84 */
					obj_t BgL_v1258z00_1090;

					BgL_v1258z00_1090 = create_vector(0L);
					BgL_arg1843z00_942 = BgL_v1258z00_1090;
				}
				BGl_funz00zzast_varz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(44),
					CNST_TABLE_REF(3), BGl_valuez00zzast_varz00, 55727L,
					BGl_proc2284z00zzast_varz00, BGl_proc2283z00zzast_varz00, BFALSE,
					BGl_proc2282z00zzast_varz00, BFALSE, BgL_arg1842z00_941,
					BgL_arg1843z00_942);
			}
			{	/* Ast/var.scm 116 */
				obj_t BgL_arg1927z00_1097;
				obj_t BgL_arg1928z00_1098;

				{	/* Ast/var.scm 116 */
					obj_t BgL_v1259z00_1130;

					BgL_v1259z00_1130 = create_vector(12L);
					{	/* Ast/var.scm 116 */
						obj_t BgL_arg1934z00_1131;

						BgL_arg1934z00_1131 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(45),
							BGl_proc2287z00zzast_varz00, BGl_proc2286z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2285z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1259z00_1130, 0L, BgL_arg1934z00_1131);
					}
					{	/* Ast/var.scm 116 */
						obj_t BgL_arg1941z00_1144;

						BgL_arg1941z00_1144 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(46),
							BGl_proc2289z00zzast_varz00, BGl_proc2288z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1259z00_1130, 1L, BgL_arg1941z00_1144);
					}
					{	/* Ast/var.scm 116 */
						obj_t BgL_arg1946z00_1154;

						BgL_arg1946z00_1154 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(47),
							BGl_proc2291z00zzast_varz00, BGl_proc2290z00zzast_varz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1259z00_1130, 2L, BgL_arg1946z00_1154);
					}
					{	/* Ast/var.scm 116 */
						obj_t BgL_arg1951z00_1164;

						BgL_arg1951z00_1164 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(48),
							BGl_proc2294z00zzast_varz00, BGl_proc2293z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2292z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1259z00_1130, 3L, BgL_arg1951z00_1164);
					}
					{	/* Ast/var.scm 116 */
						obj_t BgL_arg1958z00_1177;

						BgL_arg1958z00_1177 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(49),
							BGl_proc2296z00zzast_varz00, BGl_proc2295z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1259z00_1130, 4L, BgL_arg1958z00_1177);
					}
					{	/* Ast/var.scm 116 */
						obj_t BgL_arg1963z00_1187;

						BgL_arg1963z00_1187 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(50),
							BGl_proc2299z00zzast_varz00, BGl_proc2298z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2297z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1259z00_1130, 5L, BgL_arg1963z00_1187);
					}
					{	/* Ast/var.scm 116 */
						obj_t BgL_arg1970z00_1200;

						BgL_arg1970z00_1200 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(51),
							BGl_proc2302z00zzast_varz00, BGl_proc2301z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2300z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1259z00_1130, 6L, BgL_arg1970z00_1200);
					}
					{	/* Ast/var.scm 116 */
						obj_t BgL_arg1977z00_1213;

						BgL_arg1977z00_1213 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(52),
							BGl_proc2305z00zzast_varz00, BGl_proc2304z00zzast_varz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BGl_proc2303z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1259z00_1130, 7L, BgL_arg1977z00_1213);
					}
					{	/* Ast/var.scm 116 */
						obj_t BgL_arg1984z00_1226;

						BgL_arg1984z00_1226 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(53),
							BGl_proc2308z00zzast_varz00, BGl_proc2307z00zzast_varz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BGl_proc2306z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1259z00_1130, 8L, BgL_arg1984z00_1226);
					}
					{	/* Ast/var.scm 116 */
						obj_t BgL_arg1991z00_1239;

						BgL_arg1991z00_1239 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(54),
							BGl_proc2311z00zzast_varz00, BGl_proc2310z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2309z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1259z00_1130, 9L, BgL_arg1991z00_1239);
					}
					{	/* Ast/var.scm 116 */
						obj_t BgL_arg1998z00_1252;

						BgL_arg1998z00_1252 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(55),
							BGl_proc2314z00zzast_varz00, BGl_proc2313z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2312z00zzast_varz00,
							CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1259z00_1130, 10L, BgL_arg1998z00_1252);
					}
					{	/* Ast/var.scm 116 */
						obj_t BgL_arg2006z00_1265;

						BgL_arg2006z00_1265 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(56),
							BGl_proc2317z00zzast_varz00, BGl_proc2316z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2315z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1259z00_1130, 11L, BgL_arg2006z00_1265);
					}
					BgL_arg1927z00_1097 = BgL_v1259z00_1130;
				}
				{	/* Ast/var.scm 116 */
					obj_t BgL_v1260z00_1278;

					BgL_v1260z00_1278 = create_vector(0L);
					BgL_arg1928z00_1098 = BgL_v1260z00_1278;
				}
				BGl_sfunz00zzast_varz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(57),
					CNST_TABLE_REF(3), BGl_funz00zzast_varz00, 64266L,
					BGl_proc2320z00zzast_varz00, BGl_proc2319z00zzast_varz00, BFALSE,
					BGl_proc2318z00zzast_varz00, BFALSE, BgL_arg1927z00_1097,
					BgL_arg1928z00_1098);
			}
			{	/* Ast/var.scm 146 */
				obj_t BgL_arg2016z00_1285;
				obj_t BgL_arg2017z00_1286;

				{	/* Ast/var.scm 146 */
					obj_t BgL_v1261z00_1310;

					BgL_v1261z00_1310 = create_vector(4L);
					{	/* Ast/var.scm 146 */
						obj_t BgL_arg2024z00_1311;

						BgL_arg2024z00_1311 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(58),
							BGl_proc2322z00zzast_varz00, BGl_proc2321z00zzast_varz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1261z00_1310, 0L, BgL_arg2024z00_1311);
					}
					{	/* Ast/var.scm 146 */
						obj_t BgL_arg2029z00_1321;

						BgL_arg2029z00_1321 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(59),
							BGl_proc2324z00zzast_varz00, BGl_proc2323z00zzast_varz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(16));
						VECTOR_SET(BgL_v1261z00_1310, 1L, BgL_arg2029z00_1321);
					}
					{	/* Ast/var.scm 146 */
						obj_t BgL_arg2034z00_1331;

						BgL_arg2034z00_1331 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(60),
							BGl_proc2327z00zzast_varz00, BGl_proc2326z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2325z00zzast_varz00,
							CNST_TABLE_REF(16));
						VECTOR_SET(BgL_v1261z00_1310, 2L, BgL_arg2034z00_1331);
					}
					{	/* Ast/var.scm 146 */
						obj_t BgL_arg2042z00_1344;

						BgL_arg2042z00_1344 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(61),
							BGl_proc2330z00zzast_varz00, BGl_proc2329z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2328z00zzast_varz00,
							CNST_TABLE_REF(62));
						VECTOR_SET(BgL_v1261z00_1310, 3L, BgL_arg2042z00_1344);
					}
					BgL_arg2016z00_1285 = BgL_v1261z00_1310;
				}
				{	/* Ast/var.scm 146 */
					obj_t BgL_v1262z00_1357;

					BgL_v1262z00_1357 = create_vector(0L);
					BgL_arg2017z00_1286 = BgL_v1262z00_1357;
				}
				BGl_cfunz00zzast_varz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(63),
					CNST_TABLE_REF(3), BGl_funz00zzast_varz00, 20445L,
					BGl_proc2333z00zzast_varz00, BGl_proc2332z00zzast_varz00, BFALSE,
					BGl_proc2331z00zzast_varz00, BFALSE, BgL_arg2016z00_1285,
					BgL_arg2017z00_1286);
			}
			{	/* Ast/var.scm 156 */
				obj_t BgL_arg2055z00_1364;
				obj_t BgL_arg2056z00_1365;

				{	/* Ast/var.scm 156 */
					obj_t BgL_v1263z00_1376;

					BgL_v1263z00_1376 = create_vector(1L);
					{	/* Ast/var.scm 156 */
						obj_t BgL_arg2062z00_1377;

						BgL_arg2062z00_1377 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(51),
							BGl_proc2336z00zzast_varz00, BGl_proc2335z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2334z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1263z00_1376, 0L, BgL_arg2062z00_1377);
					}
					BgL_arg2055z00_1364 = BgL_v1263z00_1376;
				}
				{	/* Ast/var.scm 156 */
					obj_t BgL_v1264z00_1390;

					BgL_v1264z00_1390 = create_vector(0L);
					BgL_arg2056z00_1365 = BgL_v1264z00_1390;
				}
				BGl_svarz00zzast_varz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(64),
					CNST_TABLE_REF(3), BGl_valuez00zzast_varz00, 12471L,
					BGl_proc2339z00zzast_varz00, BGl_proc2338z00zzast_varz00, BFALSE,
					BGl_proc2337z00zzast_varz00, BFALSE, BgL_arg2055z00_1364,
					BgL_arg2056z00_1365);
			}
			{	/* Ast/var.scm 160 */
				obj_t BgL_arg2074z00_1397;
				obj_t BgL_arg2075z00_1398;

				{	/* Ast/var.scm 160 */
					obj_t BgL_v1265z00_1411;

					BgL_v1265z00_1411 = create_vector(3L);
					{	/* Ast/var.scm 160 */
						obj_t BgL_arg2081z00_1412;

						BgL_arg2081z00_1412 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(65),
							BGl_proc2341z00zzast_varz00, BGl_proc2340z00zzast_varz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1265z00_1411, 0L, BgL_arg2081z00_1412);
					}
					{	/* Ast/var.scm 160 */
						obj_t BgL_arg2086z00_1422;

						BgL_arg2086z00_1422 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(49),
							BGl_proc2343z00zzast_varz00, BGl_proc2342z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1265z00_1411, 1L, BgL_arg2086z00_1422);
					}
					{	/* Ast/var.scm 160 */
						obj_t BgL_arg2091z00_1432;

						BgL_arg2091z00_1432 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(51),
							BGl_proc2346z00zzast_varz00, BGl_proc2345z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2344z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1265z00_1411, 2L, BgL_arg2091z00_1432);
					}
					BgL_arg2074z00_1397 = BgL_v1265z00_1411;
				}
				{	/* Ast/var.scm 160 */
					obj_t BgL_v1266z00_1445;

					BgL_v1266z00_1445 = create_vector(0L);
					BgL_arg2075z00_1398 = BgL_v1266z00_1445;
				}
				BGl_scnstz00zzast_varz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(66),
					CNST_TABLE_REF(3), BGl_valuez00zzast_varz00, 22055L,
					BGl_proc2349z00zzast_varz00, BGl_proc2348z00zzast_varz00, BFALSE,
					BGl_proc2347z00zzast_varz00, BFALSE, BgL_arg2074z00_1397,
					BgL_arg2075z00_1398);
			}
			{	/* Ast/var.scm 168 */
				obj_t BgL_arg2102z00_1452;
				obj_t BgL_arg2103z00_1453;

				{	/* Ast/var.scm 168 */
					obj_t BgL_v1267z00_1464;

					BgL_v1267z00_1464 = create_vector(1L);
					{	/* Ast/var.scm 168 */
						obj_t BgL_arg2109z00_1465;

						BgL_arg2109z00_1465 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(59),
							BGl_proc2351z00zzast_varz00, BGl_proc2350z00zzast_varz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(16));
						VECTOR_SET(BgL_v1267z00_1464, 0L, BgL_arg2109z00_1465);
					}
					BgL_arg2102z00_1452 = BgL_v1267z00_1464;
				}
				{	/* Ast/var.scm 168 */
					obj_t BgL_v1268z00_1475;

					BgL_v1268z00_1475 = create_vector(0L);
					BgL_arg2103z00_1453 = BgL_v1268z00_1475;
				}
				BGl_cvarz00zzast_varz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(67),
					CNST_TABLE_REF(3), BGl_valuez00zzast_varz00, 53457L,
					BGl_proc2354z00zzast_varz00, BGl_proc2353z00zzast_varz00, BFALSE,
					BGl_proc2352z00zzast_varz00, BFALSE, BgL_arg2102z00_1452,
					BgL_arg2103z00_1453);
			}
			{	/* Ast/var.scm 172 */
				obj_t BgL_arg2117z00_1482;
				obj_t BgL_arg2118z00_1483;

				{	/* Ast/var.scm 172 */
					obj_t BgL_v1269z00_1495;

					BgL_v1269z00_1495 = create_vector(2L);
					{	/* Ast/var.scm 172 */
						obj_t BgL_arg2124z00_1496;

						BgL_arg2124z00_1496 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(68),
							BGl_proc2356z00zzast_varz00, BGl_proc2355z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1269z00_1495, 0L, BgL_arg2124z00_1496);
					}
					{	/* Ast/var.scm 172 */
						obj_t BgL_arg2129z00_1506;

						BgL_arg2129z00_1506 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(69),
							BGl_proc2359z00zzast_varz00, BGl_proc2358z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2357z00zzast_varz00,
							CNST_TABLE_REF(16));
						VECTOR_SET(BgL_v1269z00_1495, 1L, BgL_arg2129z00_1506);
					}
					BgL_arg2117z00_1482 = BgL_v1269z00_1495;
				}
				{	/* Ast/var.scm 172 */
					obj_t BgL_v1270z00_1519;

					BgL_v1270z00_1519 = create_vector(0L);
					BgL_arg2118z00_1483 = BgL_v1270z00_1519;
				}
				BGl_sexitz00zzast_varz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(70),
					CNST_TABLE_REF(3), BGl_valuez00zzast_varz00, 19916L,
					BGl_proc2362z00zzast_varz00, BGl_proc2361z00zzast_varz00, BFALSE,
					BGl_proc2360z00zzast_varz00, BFALSE, BgL_arg2117z00_1482,
					BgL_arg2118z00_1483);
			}
			{	/* Ast/var.scm 178 */
				obj_t BgL_arg2139z00_1526;
				obj_t BgL_arg2141z00_1527;

				{	/* Ast/var.scm 178 */
					obj_t BgL_v1271z00_1539;

					BgL_v1271z00_1539 = create_vector(2L);
					{	/* Ast/var.scm 178 */
						obj_t BgL_arg2147z00_1540;

						BgL_arg2147z00_1540 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(71),
							BGl_proc2365z00zzast_varz00, BGl_proc2364z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2363z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1271z00_1539, 0L, BgL_arg2147z00_1540);
					}
					{	/* Ast/var.scm 178 */
						obj_t BgL_arg2154z00_1553;

						BgL_arg2154z00_1553 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(72),
							BGl_proc2368z00zzast_varz00, BGl_proc2367z00zzast_varz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2366z00zzast_varz00,
							CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1271z00_1539, 1L, BgL_arg2154z00_1553);
					}
					BgL_arg2139z00_1526 = BgL_v1271z00_1539;
				}
				{	/* Ast/var.scm 178 */
					obj_t BgL_v1272z00_1566;

					BgL_v1272z00_1566 = create_vector(0L);
					BgL_arg2141z00_1527 = BgL_v1272z00_1566;
				}
				return (BGl_feffectz00zzast_varz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(73),
						CNST_TABLE_REF(3), BGl_objectz00zz__objectz00, 59103L,
						BGl_proc2371z00zzast_varz00, BGl_proc2370z00zzast_varz00, BFALSE,
						BGl_proc2369z00zzast_varz00, BFALSE, BgL_arg2139z00_1526,
						BgL_arg2141z00_1527), BUNSPEC);
			}
		}

	}



/* &<@anonymous:2146> */
	obj_t BGl_z62zc3z04anonymousza32146ze3ze5zzast_varz00(obj_t BgL_envz00_3267,
		obj_t BgL_new1182z00_3268)
	{
		{	/* Ast/var.scm 178 */
			{
				BgL_feffectz00_bglt BgL_auxz00_6053;

				((((BgL_feffectz00_bglt) COBJECT(
								((BgL_feffectz00_bglt) BgL_new1182z00_3268)))->BgL_readz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_feffectz00_bglt) COBJECT(((BgL_feffectz00_bglt)
									BgL_new1182z00_3268)))->BgL_writez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_6053 = ((BgL_feffectz00_bglt) BgL_new1182z00_3268);
				return ((obj_t) BgL_auxz00_6053);
			}
		}

	}



/* &lambda2144 */
	BgL_feffectz00_bglt BGl_z62lambda2144z62zzast_varz00(obj_t BgL_envz00_3269)
	{
		{	/* Ast/var.scm 178 */
			{	/* Ast/var.scm 178 */
				BgL_feffectz00_bglt BgL_new1180z00_3971;

				BgL_new1180z00_3971 =
					((BgL_feffectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_feffectz00_bgl))));
				{	/* Ast/var.scm 178 */
					long BgL_arg2145z00_3972;

					BgL_arg2145z00_3972 = BGL_CLASS_NUM(BGl_feffectz00zzast_varz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1180z00_3971), BgL_arg2145z00_3972);
				}
				return BgL_new1180z00_3971;
			}
		}

	}



/* &lambda2142 */
	BgL_feffectz00_bglt BGl_z62lambda2142z62zzast_varz00(obj_t BgL_envz00_3270,
		obj_t BgL_read1178z00_3271, obj_t BgL_write1179z00_3272)
	{
		{	/* Ast/var.scm 178 */
			{	/* Ast/var.scm 178 */
				BgL_feffectz00_bglt BgL_new1246z00_3973;

				{	/* Ast/var.scm 178 */
					BgL_feffectz00_bglt BgL_new1245z00_3974;

					BgL_new1245z00_3974 =
						((BgL_feffectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_feffectz00_bgl))));
					{	/* Ast/var.scm 178 */
						long BgL_arg2143z00_3975;

						BgL_arg2143z00_3975 = BGL_CLASS_NUM(BGl_feffectz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1245z00_3974), BgL_arg2143z00_3975);
					}
					BgL_new1246z00_3973 = BgL_new1245z00_3974;
				}
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1246z00_3973))->BgL_readz00) =
					((obj_t) BgL_read1178z00_3271), BUNSPEC);
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1246z00_3973))->BgL_writez00) =
					((obj_t) BgL_write1179z00_3272), BUNSPEC);
				return BgL_new1246z00_3973;
			}
		}

	}



/* &<@anonymous:2160> */
	obj_t BGl_z62zc3z04anonymousza32160ze3ze5zzast_varz00(obj_t BgL_envz00_3273)
	{
		{	/* Ast/var.scm 178 */
			return BNIL;
		}

	}



/* &lambda2159 */
	obj_t BGl_z62lambda2159z62zzast_varz00(obj_t BgL_envz00_3274,
		obj_t BgL_oz00_3275, obj_t BgL_vz00_3276)
	{
		{	/* Ast/var.scm 178 */
			return
				((((BgL_feffectz00_bglt) COBJECT(
							((BgL_feffectz00_bglt) BgL_oz00_3275)))->BgL_writez00) =
				((obj_t) BgL_vz00_3276), BUNSPEC);
		}

	}



/* &lambda2158 */
	obj_t BGl_z62lambda2158z62zzast_varz00(obj_t BgL_envz00_3277,
		obj_t BgL_oz00_3278)
	{
		{	/* Ast/var.scm 178 */
			return
				(((BgL_feffectz00_bglt) COBJECT(
						((BgL_feffectz00_bglt) BgL_oz00_3278)))->BgL_writez00);
		}

	}



/* &<@anonymous:2153> */
	obj_t BGl_z62zc3z04anonymousza32153ze3ze5zzast_varz00(obj_t BgL_envz00_3279)
	{
		{	/* Ast/var.scm 178 */
			return BNIL;
		}

	}



/* &lambda2152 */
	obj_t BGl_z62lambda2152z62zzast_varz00(obj_t BgL_envz00_3280,
		obj_t BgL_oz00_3281, obj_t BgL_vz00_3282)
	{
		{	/* Ast/var.scm 178 */
			return
				((((BgL_feffectz00_bglt) COBJECT(
							((BgL_feffectz00_bglt) BgL_oz00_3281)))->BgL_readz00) =
				((obj_t) BgL_vz00_3282), BUNSPEC);
		}

	}



/* &lambda2151 */
	obj_t BGl_z62lambda2151z62zzast_varz00(obj_t BgL_envz00_3283,
		obj_t BgL_oz00_3284)
	{
		{	/* Ast/var.scm 178 */
			return
				(((BgL_feffectz00_bglt) COBJECT(
						((BgL_feffectz00_bglt) BgL_oz00_3284)))->BgL_readz00);
		}

	}



/* &<@anonymous:2123> */
	obj_t BGl_z62zc3z04anonymousza32123ze3ze5zzast_varz00(obj_t BgL_envz00_3285,
		obj_t BgL_new1176z00_3286)
	{
		{	/* Ast/var.scm 172 */
			{
				BgL_sexitz00_bglt BgL_auxz00_6078;

				((((BgL_sexitz00_bglt) COBJECT(
								((BgL_sexitz00_bglt) BgL_new1176z00_3286)))->BgL_handlerz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sexitz00_bglt) COBJECT(((BgL_sexitz00_bglt)
									BgL_new1176z00_3286)))->BgL_detachedzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				BgL_auxz00_6078 = ((BgL_sexitz00_bglt) BgL_new1176z00_3286);
				return ((obj_t) BgL_auxz00_6078);
			}
		}

	}



/* &lambda2121 */
	BgL_sexitz00_bglt BGl_z62lambda2121z62zzast_varz00(obj_t BgL_envz00_3287)
	{
		{	/* Ast/var.scm 172 */
			{	/* Ast/var.scm 172 */
				BgL_sexitz00_bglt BgL_new1175z00_3981;

				BgL_new1175z00_3981 =
					((BgL_sexitz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_sexitz00_bgl))));
				{	/* Ast/var.scm 172 */
					long BgL_arg2122z00_3982;

					BgL_arg2122z00_3982 = BGL_CLASS_NUM(BGl_sexitz00zzast_varz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1175z00_3981), BgL_arg2122z00_3982);
				}
				{	/* Ast/var.scm 172 */
					BgL_objectz00_bglt BgL_tmpz00_6089;

					BgL_tmpz00_6089 = ((BgL_objectz00_bglt) BgL_new1175z00_3981);
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6089, BFALSE);
				}
				((BgL_objectz00_bglt) BgL_new1175z00_3981);
				return BgL_new1175z00_3981;
			}
		}

	}



/* &lambda2119 */
	BgL_sexitz00_bglt BGl_z62lambda2119z62zzast_varz00(obj_t BgL_envz00_3288,
		obj_t BgL_handler1173z00_3289, obj_t BgL_detachedzf31174zf3_3290)
	{
		{	/* Ast/var.scm 172 */
			{	/* Ast/var.scm 172 */
				bool_t BgL_detachedzf31174zf3_3983;

				BgL_detachedzf31174zf3_3983 = CBOOL(BgL_detachedzf31174zf3_3290);
				{	/* Ast/var.scm 172 */
					BgL_sexitz00_bglt BgL_new1244z00_3984;

					{	/* Ast/var.scm 172 */
						BgL_sexitz00_bglt BgL_new1243z00_3985;

						BgL_new1243z00_3985 =
							((BgL_sexitz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_sexitz00_bgl))));
						{	/* Ast/var.scm 172 */
							long BgL_arg2120z00_3986;

							BgL_arg2120z00_3986 = BGL_CLASS_NUM(BGl_sexitz00zzast_varz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1243z00_3985),
								BgL_arg2120z00_3986);
						}
						{	/* Ast/var.scm 172 */
							BgL_objectz00_bglt BgL_tmpz00_6098;

							BgL_tmpz00_6098 = ((BgL_objectz00_bglt) BgL_new1243z00_3985);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6098, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_new1243z00_3985);
						BgL_new1244z00_3984 = BgL_new1243z00_3985;
					}
					((((BgL_sexitz00_bglt) COBJECT(BgL_new1244z00_3984))->
							BgL_handlerz00) = ((obj_t) BgL_handler1173z00_3289), BUNSPEC);
					((((BgL_sexitz00_bglt) COBJECT(BgL_new1244z00_3984))->
							BgL_detachedzf3zf3) =
						((bool_t) BgL_detachedzf31174zf3_3983), BUNSPEC);
					return BgL_new1244z00_3984;
				}
			}
		}

	}



/* &<@anonymous:2135> */
	obj_t BGl_z62zc3z04anonymousza32135ze3ze5zzast_varz00(obj_t BgL_envz00_3291)
	{
		{	/* Ast/var.scm 172 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda2134 */
	obj_t BGl_z62lambda2134z62zzast_varz00(obj_t BgL_envz00_3292,
		obj_t BgL_oz00_3293, obj_t BgL_vz00_3294)
	{
		{	/* Ast/var.scm 172 */
			{	/* Ast/var.scm 172 */
				bool_t BgL_vz00_3988;

				BgL_vz00_3988 = CBOOL(BgL_vz00_3294);
				return
					((((BgL_sexitz00_bglt) COBJECT(
								((BgL_sexitz00_bglt) BgL_oz00_3293)))->BgL_detachedzf3zf3) =
					((bool_t) BgL_vz00_3988), BUNSPEC);
			}
		}

	}



/* &lambda2133 */
	obj_t BGl_z62lambda2133z62zzast_varz00(obj_t BgL_envz00_3295,
		obj_t BgL_oz00_3296)
	{
		{	/* Ast/var.scm 172 */
			return
				BBOOL(
				(((BgL_sexitz00_bglt) COBJECT(
							((BgL_sexitz00_bglt) BgL_oz00_3296)))->BgL_detachedzf3zf3));
		}

	}



/* &lambda2128 */
	obj_t BGl_z62lambda2128z62zzast_varz00(obj_t BgL_envz00_3297,
		obj_t BgL_oz00_3298, obj_t BgL_vz00_3299)
	{
		{	/* Ast/var.scm 172 */
			return
				((((BgL_sexitz00_bglt) COBJECT(
							((BgL_sexitz00_bglt) BgL_oz00_3298)))->BgL_handlerz00) =
				((obj_t) BgL_vz00_3299), BUNSPEC);
		}

	}



/* &lambda2127 */
	obj_t BGl_z62lambda2127z62zzast_varz00(obj_t BgL_envz00_3300,
		obj_t BgL_oz00_3301)
	{
		{	/* Ast/var.scm 172 */
			return
				(((BgL_sexitz00_bglt) COBJECT(
						((BgL_sexitz00_bglt) BgL_oz00_3301)))->BgL_handlerz00);
		}

	}



/* &<@anonymous:2108> */
	obj_t BGl_z62zc3z04anonymousza32108ze3ze5zzast_varz00(obj_t BgL_envz00_3302,
		obj_t BgL_new1171z00_3303)
	{
		{	/* Ast/var.scm 168 */
			{
				BgL_cvarz00_bglt BgL_auxz00_6115;

				((((BgL_cvarz00_bglt) COBJECT(
								((BgL_cvarz00_bglt) BgL_new1171z00_3303)))->BgL_macrozf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				BgL_auxz00_6115 = ((BgL_cvarz00_bglt) BgL_new1171z00_3303);
				return ((obj_t) BgL_auxz00_6115);
			}
		}

	}



/* &lambda2106 */
	BgL_cvarz00_bglt BGl_z62lambda2106z62zzast_varz00(obj_t BgL_envz00_3304)
	{
		{	/* Ast/var.scm 168 */
			{	/* Ast/var.scm 168 */
				BgL_cvarz00_bglt BgL_new1170z00_3993;

				BgL_new1170z00_3993 =
					((BgL_cvarz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_cvarz00_bgl))));
				{	/* Ast/var.scm 168 */
					long BgL_arg2107z00_3994;

					BgL_arg2107z00_3994 = BGL_CLASS_NUM(BGl_cvarz00zzast_varz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1170z00_3993), BgL_arg2107z00_3994);
				}
				{	/* Ast/var.scm 168 */
					BgL_objectz00_bglt BgL_tmpz00_6124;

					BgL_tmpz00_6124 = ((BgL_objectz00_bglt) BgL_new1170z00_3993);
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6124, BFALSE);
				}
				((BgL_objectz00_bglt) BgL_new1170z00_3993);
				return BgL_new1170z00_3993;
			}
		}

	}



/* &lambda2104 */
	BgL_cvarz00_bglt BGl_z62lambda2104z62zzast_varz00(obj_t BgL_envz00_3305,
		obj_t BgL_macrozf31169zf3_3306)
	{
		{	/* Ast/var.scm 168 */
			{	/* Ast/var.scm 168 */
				bool_t BgL_macrozf31169zf3_3995;

				BgL_macrozf31169zf3_3995 = CBOOL(BgL_macrozf31169zf3_3306);
				{	/* Ast/var.scm 168 */
					BgL_cvarz00_bglt BgL_new1242z00_3996;

					{	/* Ast/var.scm 168 */
						BgL_cvarz00_bglt BgL_new1240z00_3997;

						BgL_new1240z00_3997 =
							((BgL_cvarz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_cvarz00_bgl))));
						{	/* Ast/var.scm 168 */
							long BgL_arg2105z00_3998;

							BgL_arg2105z00_3998 = BGL_CLASS_NUM(BGl_cvarz00zzast_varz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1240z00_3997),
								BgL_arg2105z00_3998);
						}
						{	/* Ast/var.scm 168 */
							BgL_objectz00_bglt BgL_tmpz00_6133;

							BgL_tmpz00_6133 = ((BgL_objectz00_bglt) BgL_new1240z00_3997);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6133, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_new1240z00_3997);
						BgL_new1242z00_3996 = BgL_new1240z00_3997;
					}
					((((BgL_cvarz00_bglt) COBJECT(BgL_new1242z00_3996))->
							BgL_macrozf3zf3) = ((bool_t) BgL_macrozf31169zf3_3995), BUNSPEC);
					return BgL_new1242z00_3996;
				}
			}
		}

	}



/* &lambda2113 */
	obj_t BGl_z62lambda2113z62zzast_varz00(obj_t BgL_envz00_3307,
		obj_t BgL_oz00_3308, obj_t BgL_vz00_3309)
	{
		{	/* Ast/var.scm 168 */
			{	/* Ast/var.scm 168 */
				bool_t BgL_vz00_4000;

				BgL_vz00_4000 = CBOOL(BgL_vz00_3309);
				return
					((((BgL_cvarz00_bglt) COBJECT(
								((BgL_cvarz00_bglt) BgL_oz00_3308)))->BgL_macrozf3zf3) =
					((bool_t) BgL_vz00_4000), BUNSPEC);
			}
		}

	}



/* &lambda2112 */
	obj_t BGl_z62lambda2112z62zzast_varz00(obj_t BgL_envz00_3310,
		obj_t BgL_oz00_3311)
	{
		{	/* Ast/var.scm 168 */
			return
				BBOOL(
				(((BgL_cvarz00_bglt) COBJECT(
							((BgL_cvarz00_bglt) BgL_oz00_3311)))->BgL_macrozf3zf3));
		}

	}



/* &<@anonymous:2080> */
	obj_t BGl_z62zc3z04anonymousza32080ze3ze5zzast_varz00(obj_t BgL_envz00_3312,
		obj_t BgL_new1167z00_3313)
	{
		{	/* Ast/var.scm 160 */
			{
				BgL_scnstz00_bglt BgL_auxz00_6144;

				((((BgL_scnstz00_bglt) COBJECT(
								((BgL_scnstz00_bglt) BgL_new1167z00_3313)))->BgL_nodez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_scnstz00_bglt) COBJECT(((BgL_scnstz00_bglt)
									BgL_new1167z00_3313)))->BgL_classz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_scnstz00_bglt) COBJECT(((BgL_scnstz00_bglt)
									BgL_new1167z00_3313)))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_6144 = ((BgL_scnstz00_bglt) BgL_new1167z00_3313);
				return ((obj_t) BgL_auxz00_6144);
			}
		}

	}



/* &lambda2078 */
	BgL_scnstz00_bglt BGl_z62lambda2078z62zzast_varz00(obj_t BgL_envz00_3314)
	{
		{	/* Ast/var.scm 160 */
			{	/* Ast/var.scm 160 */
				BgL_scnstz00_bglt BgL_new1166z00_4003;

				BgL_new1166z00_4003 =
					((BgL_scnstz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_scnstz00_bgl))));
				{	/* Ast/var.scm 160 */
					long BgL_arg2079z00_4004;

					BgL_arg2079z00_4004 = BGL_CLASS_NUM(BGl_scnstz00zzast_varz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1166z00_4003), BgL_arg2079z00_4004);
				}
				{	/* Ast/var.scm 160 */
					BgL_objectz00_bglt BgL_tmpz00_6157;

					BgL_tmpz00_6157 = ((BgL_objectz00_bglt) BgL_new1166z00_4003);
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6157, BFALSE);
				}
				((BgL_objectz00_bglt) BgL_new1166z00_4003);
				return BgL_new1166z00_4003;
			}
		}

	}



/* &lambda2076 */
	BgL_scnstz00_bglt BGl_z62lambda2076z62zzast_varz00(obj_t BgL_envz00_3315,
		obj_t BgL_node1163z00_3316, obj_t BgL_class1164z00_3317,
		obj_t BgL_loc1165z00_3318)
	{
		{	/* Ast/var.scm 160 */
			{	/* Ast/var.scm 160 */
				BgL_scnstz00_bglt BgL_new1239z00_4005;

				{	/* Ast/var.scm 160 */
					BgL_scnstz00_bglt BgL_new1238z00_4006;

					BgL_new1238z00_4006 =
						((BgL_scnstz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_scnstz00_bgl))));
					{	/* Ast/var.scm 160 */
						long BgL_arg2077z00_4007;

						BgL_arg2077z00_4007 = BGL_CLASS_NUM(BGl_scnstz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1238z00_4006), BgL_arg2077z00_4007);
					}
					{	/* Ast/var.scm 160 */
						BgL_objectz00_bglt BgL_tmpz00_6165;

						BgL_tmpz00_6165 = ((BgL_objectz00_bglt) BgL_new1238z00_4006);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6165, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1238z00_4006);
					BgL_new1239z00_4005 = BgL_new1238z00_4006;
				}
				((((BgL_scnstz00_bglt) COBJECT(BgL_new1239z00_4005))->BgL_nodez00) =
					((obj_t) BgL_node1163z00_3316), BUNSPEC);
				((((BgL_scnstz00_bglt) COBJECT(BgL_new1239z00_4005))->BgL_classz00) =
					((obj_t) BgL_class1164z00_3317), BUNSPEC);
				((((BgL_scnstz00_bglt) COBJECT(BgL_new1239z00_4005))->BgL_locz00) =
					((obj_t) BgL_loc1165z00_3318), BUNSPEC);
				return BgL_new1239z00_4005;
			}
		}

	}



/* &<@anonymous:2098> */
	obj_t BGl_z62zc3z04anonymousza32098ze3ze5zzast_varz00(obj_t BgL_envz00_3319)
	{
		{	/* Ast/var.scm 160 */
			return BUNSPEC;
		}

	}



/* &lambda2097 */
	obj_t BGl_z62lambda2097z62zzast_varz00(obj_t BgL_envz00_3320,
		obj_t BgL_oz00_3321, obj_t BgL_vz00_3322)
	{
		{	/* Ast/var.scm 160 */
			return
				((((BgL_scnstz00_bglt) COBJECT(
							((BgL_scnstz00_bglt) BgL_oz00_3321)))->BgL_locz00) =
				((obj_t) BgL_vz00_3322), BUNSPEC);
		}

	}



/* &lambda2096 */
	obj_t BGl_z62lambda2096z62zzast_varz00(obj_t BgL_envz00_3323,
		obj_t BgL_oz00_3324)
	{
		{	/* Ast/var.scm 160 */
			return
				(((BgL_scnstz00_bglt) COBJECT(
						((BgL_scnstz00_bglt) BgL_oz00_3324)))->BgL_locz00);
		}

	}



/* &lambda2090 */
	obj_t BGl_z62lambda2090z62zzast_varz00(obj_t BgL_envz00_3325,
		obj_t BgL_oz00_3326, obj_t BgL_vz00_3327)
	{
		{	/* Ast/var.scm 160 */
			return
				((((BgL_scnstz00_bglt) COBJECT(
							((BgL_scnstz00_bglt) BgL_oz00_3326)))->BgL_classz00) =
				((obj_t) BgL_vz00_3327), BUNSPEC);
		}

	}



/* &lambda2089 */
	obj_t BGl_z62lambda2089z62zzast_varz00(obj_t BgL_envz00_3328,
		obj_t BgL_oz00_3329)
	{
		{	/* Ast/var.scm 160 */
			return
				(((BgL_scnstz00_bglt) COBJECT(
						((BgL_scnstz00_bglt) BgL_oz00_3329)))->BgL_classz00);
		}

	}



/* &lambda2085 */
	obj_t BGl_z62lambda2085z62zzast_varz00(obj_t BgL_envz00_3330,
		obj_t BgL_oz00_3331, obj_t BgL_vz00_3332)
	{
		{	/* Ast/var.scm 160 */
			return
				((((BgL_scnstz00_bglt) COBJECT(
							((BgL_scnstz00_bglt) BgL_oz00_3331)))->BgL_nodez00) =
				((obj_t) BgL_vz00_3332), BUNSPEC);
		}

	}



/* &lambda2084 */
	obj_t BGl_z62lambda2084z62zzast_varz00(obj_t BgL_envz00_3333,
		obj_t BgL_oz00_3334)
	{
		{	/* Ast/var.scm 160 */
			return
				(((BgL_scnstz00_bglt) COBJECT(
						((BgL_scnstz00_bglt) BgL_oz00_3334)))->BgL_nodez00);
		}

	}



/* &<@anonymous:2061> */
	obj_t BGl_z62zc3z04anonymousza32061ze3ze5zzast_varz00(obj_t BgL_envz00_3335,
		obj_t BgL_new1161z00_3336)
	{
		{	/* Ast/var.scm 156 */
			{
				BgL_svarz00_bglt BgL_auxz00_6184;

				((((BgL_svarz00_bglt) COBJECT(
								((BgL_svarz00_bglt) BgL_new1161z00_3336)))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_6184 = ((BgL_svarz00_bglt) BgL_new1161z00_3336);
				return ((obj_t) BgL_auxz00_6184);
			}
		}

	}



/* &lambda2059 */
	BgL_svarz00_bglt BGl_z62lambda2059z62zzast_varz00(obj_t BgL_envz00_3337)
	{
		{	/* Ast/var.scm 156 */
			{	/* Ast/var.scm 156 */
				BgL_svarz00_bglt BgL_new1160z00_4015;

				BgL_new1160z00_4015 =
					((BgL_svarz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_svarz00_bgl))));
				{	/* Ast/var.scm 156 */
					long BgL_arg2060z00_4016;

					BgL_arg2060z00_4016 = BGL_CLASS_NUM(BGl_svarz00zzast_varz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1160z00_4015), BgL_arg2060z00_4016);
				}
				{	/* Ast/var.scm 156 */
					BgL_objectz00_bglt BgL_tmpz00_6193;

					BgL_tmpz00_6193 = ((BgL_objectz00_bglt) BgL_new1160z00_4015);
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6193, BFALSE);
				}
				((BgL_objectz00_bglt) BgL_new1160z00_4015);
				return BgL_new1160z00_4015;
			}
		}

	}



/* &lambda2057 */
	BgL_svarz00_bglt BGl_z62lambda2057z62zzast_varz00(obj_t BgL_envz00_3338,
		obj_t BgL_loc1159z00_3339)
	{
		{	/* Ast/var.scm 156 */
			{	/* Ast/var.scm 156 */
				BgL_svarz00_bglt BgL_new1237z00_4017;

				{	/* Ast/var.scm 156 */
					BgL_svarz00_bglt BgL_new1236z00_4018;

					BgL_new1236z00_4018 =
						((BgL_svarz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_svarz00_bgl))));
					{	/* Ast/var.scm 156 */
						long BgL_arg2058z00_4019;

						BgL_arg2058z00_4019 = BGL_CLASS_NUM(BGl_svarz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1236z00_4018), BgL_arg2058z00_4019);
					}
					{	/* Ast/var.scm 156 */
						BgL_objectz00_bglt BgL_tmpz00_6201;

						BgL_tmpz00_6201 = ((BgL_objectz00_bglt) BgL_new1236z00_4018);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6201, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1236z00_4018);
					BgL_new1237z00_4017 = BgL_new1236z00_4018;
				}
				((((BgL_svarz00_bglt) COBJECT(BgL_new1237z00_4017))->BgL_locz00) =
					((obj_t) BgL_loc1159z00_3339), BUNSPEC);
				return BgL_new1237z00_4017;
			}
		}

	}



/* &<@anonymous:2068> */
	obj_t BGl_z62zc3z04anonymousza32068ze3ze5zzast_varz00(obj_t BgL_envz00_3340)
	{
		{	/* Ast/var.scm 156 */
			return BUNSPEC;
		}

	}



/* &lambda2067 */
	obj_t BGl_z62lambda2067z62zzast_varz00(obj_t BgL_envz00_3341,
		obj_t BgL_oz00_3342, obj_t BgL_vz00_3343)
	{
		{	/* Ast/var.scm 156 */
			return
				((((BgL_svarz00_bglt) COBJECT(
							((BgL_svarz00_bglt) BgL_oz00_3342)))->BgL_locz00) =
				((obj_t) BgL_vz00_3343), BUNSPEC);
		}

	}



/* &lambda2066 */
	obj_t BGl_z62lambda2066z62zzast_varz00(obj_t BgL_envz00_3344,
		obj_t BgL_oz00_3345)
	{
		{	/* Ast/var.scm 156 */
			return
				(((BgL_svarz00_bglt) COBJECT(
						((BgL_svarz00_bglt) BgL_oz00_3345)))->BgL_locz00);
		}

	}



/* &<@anonymous:2022> */
	obj_t BGl_z62zc3z04anonymousza32022ze3ze5zzast_varz00(obj_t BgL_envz00_3346,
		obj_t BgL_new1157z00_3347)
	{
		{	/* Ast/var.scm 146 */
			{
				BgL_cfunz00_bglt BgL_auxz00_6210;

				((((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt)
									((BgL_cfunz00_bglt) BgL_new1157z00_3347))))->BgL_arityz00) =
					((long) 0L), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_cfunz00_bglt)
										BgL_new1157z00_3347))))->BgL_sidezd2effectzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_cfunz00_bglt)
										BgL_new1157z00_3347))))->BgL_predicatezd2ofzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_cfunz00_bglt)
										BgL_new1157z00_3347))))->BgL_stackzd2allocatorzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_cfunz00_bglt)
										BgL_new1157z00_3347))))->BgL_topzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_cfunz00_bglt)
										BgL_new1157z00_3347))))->BgL_thezd2closurezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_cfunz00_bglt)
										BgL_new1157z00_3347))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_cfunz00_bglt)
										BgL_new1157z00_3347))))->BgL_failsafez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_cfunz00_bglt)
										BgL_new1157z00_3347))))->BgL_argszd2noescapezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_cfunz00_bglt)
										BgL_new1157z00_3347))))->BgL_argszd2retescapezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_cfunz00_bglt) COBJECT(((BgL_cfunz00_bglt)
									BgL_new1157z00_3347)))->BgL_argszd2typezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_cfunz00_bglt) COBJECT(((BgL_cfunz00_bglt)
									BgL_new1157z00_3347)))->BgL_macrozf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_cfunz00_bglt) COBJECT(((BgL_cfunz00_bglt)
									BgL_new1157z00_3347)))->BgL_infixzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_cfunz00_bglt) COBJECT(((BgL_cfunz00_bglt)
									BgL_new1157z00_3347)))->BgL_methodz00) =
					((obj_t) BNIL), BUNSPEC);
				BgL_auxz00_6210 = ((BgL_cfunz00_bglt) BgL_new1157z00_3347);
				return ((obj_t) BgL_auxz00_6210);
			}
		}

	}



/* &lambda2020 */
	BgL_cfunz00_bglt BGl_z62lambda2020z62zzast_varz00(obj_t BgL_envz00_3348)
	{
		{	/* Ast/var.scm 146 */
			{	/* Ast/var.scm 146 */
				BgL_cfunz00_bglt BgL_new1156z00_4023;

				BgL_new1156z00_4023 =
					((BgL_cfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_cfunz00_bgl))));
				{	/* Ast/var.scm 146 */
					long BgL_arg2021z00_4024;

					BgL_arg2021z00_4024 = BGL_CLASS_NUM(BGl_cfunz00zzast_varz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1156z00_4023), BgL_arg2021z00_4024);
				}
				{	/* Ast/var.scm 146 */
					BgL_objectz00_bglt BgL_tmpz00_6255;

					BgL_tmpz00_6255 = ((BgL_objectz00_bglt) BgL_new1156z00_4023);
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6255, BFALSE);
				}
				((BgL_objectz00_bglt) BgL_new1156z00_4023);
				return BgL_new1156z00_4023;
			}
		}

	}



/* &lambda2018 */
	BgL_cfunz00_bglt BGl_z62lambda2018z62zzast_varz00(obj_t BgL_envz00_3349,
		obj_t BgL_arity1142z00_3350, obj_t BgL_sidezd2effect1143zd2_3351,
		obj_t BgL_predicatezd2of1144zd2_3352,
		obj_t BgL_stackzd2allocator1145zd2_3353, obj_t BgL_topzf31146zf3_3354,
		obj_t BgL_thezd2closure1147zd2_3355, obj_t BgL_effect1148z00_3356,
		obj_t BgL_failsafe1149z00_3357, obj_t BgL_argszd2noescape1150zd2_3358,
		obj_t BgL_argszd2retescape1151zd2_3359, obj_t BgL_argszd2type1152zd2_3360,
		obj_t BgL_macrozf31153zf3_3361, obj_t BgL_infixzf31154zf3_3362,
		obj_t BgL_method1155z00_3363)
	{
		{	/* Ast/var.scm 146 */
			{	/* Ast/var.scm 146 */
				long BgL_arity1142z00_4025;
				bool_t BgL_topzf31146zf3_4026;
				bool_t BgL_macrozf31153zf3_4027;
				bool_t BgL_infixzf31154zf3_4028;

				BgL_arity1142z00_4025 = (long) CINT(BgL_arity1142z00_3350);
				BgL_topzf31146zf3_4026 = CBOOL(BgL_topzf31146zf3_3354);
				BgL_macrozf31153zf3_4027 = CBOOL(BgL_macrozf31153zf3_3361);
				BgL_infixzf31154zf3_4028 = CBOOL(BgL_infixzf31154zf3_3362);
				{	/* Ast/var.scm 146 */
					BgL_cfunz00_bglt BgL_new1235z00_4030;

					{	/* Ast/var.scm 146 */
						BgL_cfunz00_bglt BgL_new1234z00_4031;

						BgL_new1234z00_4031 =
							((BgL_cfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_cfunz00_bgl))));
						{	/* Ast/var.scm 146 */
							long BgL_arg2019z00_4032;

							BgL_arg2019z00_4032 = BGL_CLASS_NUM(BGl_cfunz00zzast_varz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1234z00_4031),
								BgL_arg2019z00_4032);
						}
						{	/* Ast/var.scm 146 */
							BgL_objectz00_bglt BgL_tmpz00_6267;

							BgL_tmpz00_6267 = ((BgL_objectz00_bglt) BgL_new1234z00_4031);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6267, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_new1234z00_4031);
						BgL_new1235z00_4030 = BgL_new1234z00_4031;
					}
					((((BgL_funz00_bglt) COBJECT(
									((BgL_funz00_bglt) BgL_new1235z00_4030)))->BgL_arityz00) =
						((long) BgL_arity1142z00_4025), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1235z00_4030)))->BgL_sidezd2effectzd2) =
						((obj_t) BgL_sidezd2effect1143zd2_3351), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1235z00_4030)))->BgL_predicatezd2ofzd2) =
						((obj_t) BgL_predicatezd2of1144zd2_3352), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1235z00_4030)))->BgL_stackzd2allocatorzd2) =
						((obj_t) BgL_stackzd2allocator1145zd2_3353), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1235z00_4030)))->BgL_topzf3zf3) =
						((bool_t) BgL_topzf31146zf3_4026), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1235z00_4030)))->BgL_thezd2closurezd2) =
						((obj_t) BgL_thezd2closure1147zd2_3355), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1235z00_4030)))->BgL_effectz00) =
						((obj_t) BgL_effect1148z00_3356), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1235z00_4030)))->BgL_failsafez00) =
						((obj_t) BgL_failsafe1149z00_3357), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1235z00_4030)))->BgL_argszd2noescapezd2) =
						((obj_t) BgL_argszd2noescape1150zd2_3358), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1235z00_4030)))->BgL_argszd2retescapezd2) =
						((obj_t) BgL_argszd2retescape1151zd2_3359), BUNSPEC);
					((((BgL_cfunz00_bglt) COBJECT(BgL_new1235z00_4030))->
							BgL_argszd2typezd2) =
						((obj_t) BgL_argszd2type1152zd2_3360), BUNSPEC);
					((((BgL_cfunz00_bglt) COBJECT(BgL_new1235z00_4030))->
							BgL_macrozf3zf3) = ((bool_t) BgL_macrozf31153zf3_4027), BUNSPEC);
					((((BgL_cfunz00_bglt) COBJECT(BgL_new1235z00_4030))->
							BgL_infixzf3zf3) = ((bool_t) BgL_infixzf31154zf3_4028), BUNSPEC);
					((((BgL_cfunz00_bglt) COBJECT(BgL_new1235z00_4030))->BgL_methodz00) =
						((obj_t) ((obj_t) BgL_method1155z00_3363)), BUNSPEC);
					return BgL_new1235z00_4030;
				}
			}
		}

	}



/* &<@anonymous:2049> */
	obj_t BGl_z62zc3z04anonymousza32049ze3ze5zzast_varz00(obj_t BgL_envz00_3364)
	{
		{	/* Ast/var.scm 146 */
			return BNIL;
		}

	}



/* &lambda2048 */
	obj_t BGl_z62lambda2048z62zzast_varz00(obj_t BgL_envz00_3365,
		obj_t BgL_oz00_3366, obj_t BgL_vz00_3367)
	{
		{	/* Ast/var.scm 146 */
			return
				((((BgL_cfunz00_bglt) COBJECT(
							((BgL_cfunz00_bglt) BgL_oz00_3366)))->BgL_methodz00) = ((obj_t)
					((obj_t) BgL_vz00_3367)), BUNSPEC);
		}

	}



/* &lambda2047 */
	obj_t BGl_z62lambda2047z62zzast_varz00(obj_t BgL_envz00_3368,
		obj_t BgL_oz00_3369)
	{
		{	/* Ast/var.scm 146 */
			return
				(((BgL_cfunz00_bglt) COBJECT(
						((BgL_cfunz00_bglt) BgL_oz00_3369)))->BgL_methodz00);
		}

	}



/* &<@anonymous:2041> */
	obj_t BGl_z62zc3z04anonymousza32041ze3ze5zzast_varz00(obj_t BgL_envz00_3370)
	{
		{	/* Ast/var.scm 146 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda2040 */
	obj_t BGl_z62lambda2040z62zzast_varz00(obj_t BgL_envz00_3371,
		obj_t BgL_oz00_3372, obj_t BgL_vz00_3373)
	{
		{	/* Ast/var.scm 146 */
			{	/* Ast/var.scm 146 */
				bool_t BgL_vz00_4037;

				BgL_vz00_4037 = CBOOL(BgL_vz00_3373);
				return
					((((BgL_cfunz00_bglt) COBJECT(
								((BgL_cfunz00_bglt) BgL_oz00_3372)))->BgL_infixzf3zf3) =
					((bool_t) BgL_vz00_4037), BUNSPEC);
			}
		}

	}



/* &lambda2039 */
	obj_t BGl_z62lambda2039z62zzast_varz00(obj_t BgL_envz00_3374,
		obj_t BgL_oz00_3375)
	{
		{	/* Ast/var.scm 146 */
			return
				BBOOL(
				(((BgL_cfunz00_bglt) COBJECT(
							((BgL_cfunz00_bglt) BgL_oz00_3375)))->BgL_infixzf3zf3));
		}

	}



/* &lambda2033 */
	obj_t BGl_z62lambda2033z62zzast_varz00(obj_t BgL_envz00_3376,
		obj_t BgL_oz00_3377, obj_t BgL_vz00_3378)
	{
		{	/* Ast/var.scm 146 */
			{	/* Ast/var.scm 146 */
				bool_t BgL_vz00_4040;

				BgL_vz00_4040 = CBOOL(BgL_vz00_3378);
				return
					((((BgL_cfunz00_bglt) COBJECT(
								((BgL_cfunz00_bglt) BgL_oz00_3377)))->BgL_macrozf3zf3) =
					((bool_t) BgL_vz00_4040), BUNSPEC);
			}
		}

	}



/* &lambda2032 */
	obj_t BGl_z62lambda2032z62zzast_varz00(obj_t BgL_envz00_3379,
		obj_t BgL_oz00_3380)
	{
		{	/* Ast/var.scm 146 */
			return
				BBOOL(
				(((BgL_cfunz00_bglt) COBJECT(
							((BgL_cfunz00_bglt) BgL_oz00_3380)))->BgL_macrozf3zf3));
		}

	}



/* &lambda2028 */
	obj_t BGl_z62lambda2028z62zzast_varz00(obj_t BgL_envz00_3381,
		obj_t BgL_oz00_3382, obj_t BgL_vz00_3383)
	{
		{	/* Ast/var.scm 146 */
			return
				((((BgL_cfunz00_bglt) COBJECT(
							((BgL_cfunz00_bglt) BgL_oz00_3382)))->BgL_argszd2typezd2) =
				((obj_t) BgL_vz00_3383), BUNSPEC);
		}

	}



/* &lambda2027 */
	obj_t BGl_z62lambda2027z62zzast_varz00(obj_t BgL_envz00_3384,
		obj_t BgL_oz00_3385)
	{
		{	/* Ast/var.scm 146 */
			return
				(((BgL_cfunz00_bglt) COBJECT(
						((BgL_cfunz00_bglt) BgL_oz00_3385)))->BgL_argszd2typezd2);
		}

	}



/* &<@anonymous:1933> */
	obj_t BGl_z62zc3z04anonymousza31933ze3ze5zzast_varz00(obj_t BgL_envz00_3386,
		obj_t BgL_new1140z00_3387)
	{
		{	/* Ast/var.scm 116 */
			{
				BgL_sfunz00_bglt BgL_auxz00_6318;

				((((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt)
									((BgL_sfunz00_bglt) BgL_new1140z00_3387))))->BgL_arityz00) =
					((long) 0L), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1140z00_3387))))->BgL_sidezd2effectzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1140z00_3387))))->BgL_predicatezd2ofzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1140z00_3387))))->BgL_stackzd2allocatorzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1140z00_3387))))->BgL_topzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1140z00_3387))))->BgL_thezd2closurezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1140z00_3387))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1140z00_3387))))->BgL_failsafez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1140z00_3387))))->BgL_argszd2noescapezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1140z00_3387))))->BgL_argszd2retescapezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1140z00_3387)))->BgL_propertyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1140z00_3387)))->BgL_argsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1140z00_3387)))->BgL_argszd2namezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1140z00_3387)))->BgL_bodyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1140z00_3387)))->BgL_classz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1140z00_3387)))->BgL_dssslzd2keywordszd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1140z00_3387)))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1140z00_3387)))->BgL_optionalsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1140z00_3387)))->BgL_keysz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1140z00_3387)))->BgL_thezd2closurezd2globalz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1140z00_3387)))->BgL_strengthz00) =
					((obj_t) CNST_TABLE_REF(74)), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1140z00_3387)))->BgL_stackablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_6318 = ((BgL_sfunz00_bglt) BgL_new1140z00_3387);
				return ((obj_t) BgL_auxz00_6318);
			}
		}

	}



/* &lambda1931 */
	BgL_sfunz00_bglt BGl_z62lambda1931z62zzast_varz00(obj_t BgL_envz00_3388)
	{
		{	/* Ast/var.scm 116 */
			{	/* Ast/var.scm 116 */
				BgL_sfunz00_bglt BgL_new1139z00_4045;

				BgL_new1139z00_4045 =
					((BgL_sfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_sfunz00_bgl))));
				{	/* Ast/var.scm 116 */
					long BgL_arg1932z00_4046;

					BgL_arg1932z00_4046 = BGL_CLASS_NUM(BGl_sfunz00zzast_varz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1139z00_4045), BgL_arg1932z00_4046);
				}
				{	/* Ast/var.scm 116 */
					BgL_objectz00_bglt BgL_tmpz00_6380;

					BgL_tmpz00_6380 = ((BgL_objectz00_bglt) BgL_new1139z00_4045);
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6380, BFALSE);
				}
				((BgL_objectz00_bglt) BgL_new1139z00_4045);
				return BgL_new1139z00_4045;
			}
		}

	}



/* &lambda1929 */
	BgL_sfunz00_bglt BGl_z62lambda1929z62zzast_varz00(obj_t BgL_envz00_3389,
		obj_t BgL_arity1117z00_3390, obj_t BgL_sidezd2effect1118zd2_3391,
		obj_t BgL_predicatezd2of1119zd2_3392,
		obj_t BgL_stackzd2allocator1120zd2_3393, obj_t BgL_topzf31121zf3_3394,
		obj_t BgL_thezd2closure1122zd2_3395, obj_t BgL_effect1123z00_3396,
		obj_t BgL_failsafe1124z00_3397, obj_t BgL_argszd2noescape1125zd2_3398,
		obj_t BgL_argszd2retescape1126zd2_3399, obj_t BgL_property1127z00_3400,
		obj_t BgL_args1128z00_3401, obj_t BgL_argszd2name1129zd2_3402,
		obj_t BgL_body1130z00_3403, obj_t BgL_class1131z00_3404,
		obj_t BgL_dssslzd2keywords1132zd2_3405, obj_t BgL_loc1133z00_3406,
		obj_t BgL_optionals1134z00_3407, obj_t BgL_keys1135z00_3408,
		obj_t BgL_thezd2closurezd2global1136z00_3409,
		obj_t BgL_strength1137z00_3410, obj_t BgL_stackable1138z00_3411)
	{
		{	/* Ast/var.scm 116 */
			{	/* Ast/var.scm 116 */
				long BgL_arity1117z00_4047;
				bool_t BgL_topzf31121zf3_4048;

				BgL_arity1117z00_4047 = (long) CINT(BgL_arity1117z00_3390);
				BgL_topzf31121zf3_4048 = CBOOL(BgL_topzf31121zf3_3394);
				{	/* Ast/var.scm 116 */
					BgL_sfunz00_bglt BgL_new1233z00_4050;

					{	/* Ast/var.scm 116 */
						BgL_sfunz00_bglt BgL_new1232z00_4051;

						BgL_new1232z00_4051 =
							((BgL_sfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_sfunz00_bgl))));
						{	/* Ast/var.scm 116 */
							long BgL_arg1930z00_4052;

							BgL_arg1930z00_4052 = BGL_CLASS_NUM(BGl_sfunz00zzast_varz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1232z00_4051),
								BgL_arg1930z00_4052);
						}
						{	/* Ast/var.scm 116 */
							BgL_objectz00_bglt BgL_tmpz00_6390;

							BgL_tmpz00_6390 = ((BgL_objectz00_bglt) BgL_new1232z00_4051);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6390, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_new1232z00_4051);
						BgL_new1233z00_4050 = BgL_new1232z00_4051;
					}
					((((BgL_funz00_bglt) COBJECT(
									((BgL_funz00_bglt) BgL_new1233z00_4050)))->BgL_arityz00) =
						((long) BgL_arity1117z00_4047), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1233z00_4050)))->BgL_sidezd2effectzd2) =
						((obj_t) BgL_sidezd2effect1118zd2_3391), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1233z00_4050)))->BgL_predicatezd2ofzd2) =
						((obj_t) BgL_predicatezd2of1119zd2_3392), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1233z00_4050)))->BgL_stackzd2allocatorzd2) =
						((obj_t) BgL_stackzd2allocator1120zd2_3393), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1233z00_4050)))->BgL_topzf3zf3) =
						((bool_t) BgL_topzf31121zf3_4048), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1233z00_4050)))->BgL_thezd2closurezd2) =
						((obj_t) BgL_thezd2closure1122zd2_3395), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1233z00_4050)))->BgL_effectz00) =
						((obj_t) BgL_effect1123z00_3396), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1233z00_4050)))->BgL_failsafez00) =
						((obj_t) BgL_failsafe1124z00_3397), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1233z00_4050)))->BgL_argszd2noescapezd2) =
						((obj_t) BgL_argszd2noescape1125zd2_3398), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1233z00_4050)))->BgL_argszd2retescapezd2) =
						((obj_t) BgL_argszd2retescape1126zd2_3399), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(BgL_new1233z00_4050))->
							BgL_propertyz00) = ((obj_t) BgL_property1127z00_3400), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(BgL_new1233z00_4050))->BgL_argsz00) =
						((obj_t) BgL_args1128z00_3401), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(BgL_new1233z00_4050))->
							BgL_argszd2namezd2) =
						((obj_t) BgL_argszd2name1129zd2_3402), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(BgL_new1233z00_4050))->BgL_bodyz00) =
						((obj_t) BgL_body1130z00_3403), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(BgL_new1233z00_4050))->BgL_classz00) =
						((obj_t) BgL_class1131z00_3404), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(BgL_new1233z00_4050))->
							BgL_dssslzd2keywordszd2) =
						((obj_t) BgL_dssslzd2keywords1132zd2_3405), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(BgL_new1233z00_4050))->BgL_locz00) =
						((obj_t) BgL_loc1133z00_3406), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(BgL_new1233z00_4050))->
							BgL_optionalsz00) = ((obj_t) BgL_optionals1134z00_3407), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(BgL_new1233z00_4050))->BgL_keysz00) =
						((obj_t) BgL_keys1135z00_3408), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(BgL_new1233z00_4050))->
							BgL_thezd2closurezd2globalz00) =
						((obj_t) BgL_thezd2closurezd2global1136z00_3409), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(BgL_new1233z00_4050))->
							BgL_strengthz00) =
						((obj_t) ((obj_t) BgL_strength1137z00_3410)), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(BgL_new1233z00_4050))->
							BgL_stackablez00) = ((obj_t) BgL_stackable1138z00_3411), BUNSPEC);
					return BgL_new1233z00_4050;
				}
			}
		}

	}



/* &<@anonymous:2012> */
	obj_t BGl_z62zc3z04anonymousza32012ze3ze5zzast_varz00(obj_t BgL_envz00_3412)
	{
		{	/* Ast/var.scm 116 */
			return BUNSPEC;
		}

	}



/* &lambda2011 */
	obj_t BGl_z62lambda2011z62zzast_varz00(obj_t BgL_envz00_3413,
		obj_t BgL_oz00_3414, obj_t BgL_vz00_3415)
	{
		{	/* Ast/var.scm 116 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_3414)))->BgL_stackablez00) =
				((obj_t) BgL_vz00_3415), BUNSPEC);
		}

	}



/* &lambda2010 */
	obj_t BGl_z62lambda2010z62zzast_varz00(obj_t BgL_envz00_3416,
		obj_t BgL_oz00_3417)
	{
		{	/* Ast/var.scm 116 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_3417)))->BgL_stackablez00);
		}

	}



/* &<@anonymous:2004> */
	obj_t BGl_z62zc3z04anonymousza32004ze3ze5zzast_varz00(obj_t BgL_envz00_3418)
	{
		{	/* Ast/var.scm 116 */
			return CNST_TABLE_REF(75);
		}

	}



/* &lambda2003 */
	obj_t BGl_z62lambda2003z62zzast_varz00(obj_t BgL_envz00_3419,
		obj_t BgL_oz00_3420, obj_t BgL_vz00_3421)
	{
		{	/* Ast/var.scm 116 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_3420)))->BgL_strengthz00) = ((obj_t)
					((obj_t) BgL_vz00_3421)), BUNSPEC);
		}

	}



/* &lambda2002 */
	obj_t BGl_z62lambda2002z62zzast_varz00(obj_t BgL_envz00_3422,
		obj_t BgL_oz00_3423)
	{
		{	/* Ast/var.scm 116 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_3423)))->BgL_strengthz00);
		}

	}



/* &<@anonymous:1997> */
	obj_t BGl_z62zc3z04anonymousza31997ze3ze5zzast_varz00(obj_t BgL_envz00_3424)
	{
		{	/* Ast/var.scm 116 */
			return BUNSPEC;
		}

	}



/* &lambda1996 */
	obj_t BGl_z62lambda1996z62zzast_varz00(obj_t BgL_envz00_3425,
		obj_t BgL_oz00_3426, obj_t BgL_vz00_3427)
	{
		{	/* Ast/var.scm 116 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_3426)))->
					BgL_thezd2closurezd2globalz00) = ((obj_t) BgL_vz00_3427), BUNSPEC);
		}

	}



/* &lambda1995 */
	obj_t BGl_z62lambda1995z62zzast_varz00(obj_t BgL_envz00_3428,
		obj_t BgL_oz00_3429)
	{
		{	/* Ast/var.scm 116 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_3429)))->
				BgL_thezd2closurezd2globalz00);
		}

	}



/* &<@anonymous:1990> */
	obj_t BGl_z62zc3z04anonymousza31990ze3ze5zzast_varz00(obj_t BgL_envz00_3430)
	{
		{	/* Ast/var.scm 116 */
			return BNIL;
		}

	}



/* &lambda1989 */
	obj_t BGl_z62lambda1989z62zzast_varz00(obj_t BgL_envz00_3431,
		obj_t BgL_oz00_3432, obj_t BgL_vz00_3433)
	{
		{	/* Ast/var.scm 116 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_3432)))->BgL_keysz00) =
				((obj_t) BgL_vz00_3433), BUNSPEC);
		}

	}



/* &lambda1988 */
	obj_t BGl_z62lambda1988z62zzast_varz00(obj_t BgL_envz00_3434,
		obj_t BgL_oz00_3435)
	{
		{	/* Ast/var.scm 116 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_3435)))->BgL_keysz00);
		}

	}



/* &<@anonymous:1983> */
	obj_t BGl_z62zc3z04anonymousza31983ze3ze5zzast_varz00(obj_t BgL_envz00_3436)
	{
		{	/* Ast/var.scm 116 */
			return BNIL;
		}

	}



/* &lambda1982 */
	obj_t BGl_z62lambda1982z62zzast_varz00(obj_t BgL_envz00_3437,
		obj_t BgL_oz00_3438, obj_t BgL_vz00_3439)
	{
		{	/* Ast/var.scm 116 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_3438)))->BgL_optionalsz00) =
				((obj_t) BgL_vz00_3439), BUNSPEC);
		}

	}



/* &lambda1981 */
	obj_t BGl_z62lambda1981z62zzast_varz00(obj_t BgL_envz00_3440,
		obj_t BgL_oz00_3441)
	{
		{	/* Ast/var.scm 116 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_3441)))->BgL_optionalsz00);
		}

	}



/* &<@anonymous:1976> */
	obj_t BGl_z62zc3z04anonymousza31976ze3ze5zzast_varz00(obj_t BgL_envz00_3442)
	{
		{	/* Ast/var.scm 116 */
			return BUNSPEC;
		}

	}



/* &lambda1975 */
	obj_t BGl_z62lambda1975z62zzast_varz00(obj_t BgL_envz00_3443,
		obj_t BgL_oz00_3444, obj_t BgL_vz00_3445)
	{
		{	/* Ast/var.scm 116 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_3444)))->BgL_locz00) =
				((obj_t) BgL_vz00_3445), BUNSPEC);
		}

	}



/* &lambda1974 */
	obj_t BGl_z62lambda1974z62zzast_varz00(obj_t BgL_envz00_3446,
		obj_t BgL_oz00_3447)
	{
		{	/* Ast/var.scm 116 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_3447)))->BgL_locz00);
		}

	}



/* &<@anonymous:1969> */
	obj_t BGl_z62zc3z04anonymousza31969ze3ze5zzast_varz00(obj_t BgL_envz00_3448)
	{
		{	/* Ast/var.scm 116 */
			return BNIL;
		}

	}



/* &lambda1968 */
	obj_t BGl_z62lambda1968z62zzast_varz00(obj_t BgL_envz00_3449,
		obj_t BgL_oz00_3450, obj_t BgL_vz00_3451)
	{
		{	/* Ast/var.scm 116 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_3450)))->BgL_dssslzd2keywordszd2) =
				((obj_t) BgL_vz00_3451), BUNSPEC);
		}

	}



/* &lambda1967 */
	obj_t BGl_z62lambda1967z62zzast_varz00(obj_t BgL_envz00_3452,
		obj_t BgL_oz00_3453)
	{
		{	/* Ast/var.scm 116 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_3453)))->BgL_dssslzd2keywordszd2);
		}

	}



/* &lambda1962 */
	obj_t BGl_z62lambda1962z62zzast_varz00(obj_t BgL_envz00_3454,
		obj_t BgL_oz00_3455, obj_t BgL_vz00_3456)
	{
		{	/* Ast/var.scm 116 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_3455)))->BgL_classz00) =
				((obj_t) BgL_vz00_3456), BUNSPEC);
		}

	}



/* &lambda1961 */
	obj_t BGl_z62lambda1961z62zzast_varz00(obj_t BgL_envz00_3457,
		obj_t BgL_oz00_3458)
	{
		{	/* Ast/var.scm 116 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_3458)))->BgL_classz00);
		}

	}



/* &<@anonymous:1957> */
	obj_t BGl_z62zc3z04anonymousza31957ze3ze5zzast_varz00(obj_t BgL_envz00_3459)
	{
		{	/* Ast/var.scm 116 */
			return BUNSPEC;
		}

	}



/* &lambda1956 */
	obj_t BGl_z62lambda1956z62zzast_varz00(obj_t BgL_envz00_3460,
		obj_t BgL_oz00_3461, obj_t BgL_vz00_3462)
	{
		{	/* Ast/var.scm 116 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_3461)))->BgL_bodyz00) =
				((obj_t) BgL_vz00_3462), BUNSPEC);
		}

	}



/* &lambda1955 */
	obj_t BGl_z62lambda1955z62zzast_varz00(obj_t BgL_envz00_3463,
		obj_t BgL_oz00_3464)
	{
		{	/* Ast/var.scm 116 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_3464)))->BgL_bodyz00);
		}

	}



/* &lambda1950 */
	obj_t BGl_z62lambda1950z62zzast_varz00(obj_t BgL_envz00_3465,
		obj_t BgL_oz00_3466, obj_t BgL_vz00_3467)
	{
		{	/* Ast/var.scm 116 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_3466)))->BgL_argszd2namezd2) =
				((obj_t) BgL_vz00_3467), BUNSPEC);
		}

	}



/* &lambda1949 */
	obj_t BGl_z62lambda1949z62zzast_varz00(obj_t BgL_envz00_3468,
		obj_t BgL_oz00_3469)
	{
		{	/* Ast/var.scm 116 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_3469)))->BgL_argszd2namezd2);
		}

	}



/* &lambda1945 */
	obj_t BGl_z62lambda1945z62zzast_varz00(obj_t BgL_envz00_3470,
		obj_t BgL_oz00_3471, obj_t BgL_vz00_3472)
	{
		{	/* Ast/var.scm 116 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_3471)))->BgL_argsz00) =
				((obj_t) BgL_vz00_3472), BUNSPEC);
		}

	}



/* &lambda1944 */
	obj_t BGl_z62lambda1944z62zzast_varz00(obj_t BgL_envz00_3473,
		obj_t BgL_oz00_3474)
	{
		{	/* Ast/var.scm 116 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_3474)))->BgL_argsz00);
		}

	}



/* &<@anonymous:1940> */
	obj_t BGl_z62zc3z04anonymousza31940ze3ze5zzast_varz00(obj_t BgL_envz00_3475)
	{
		{	/* Ast/var.scm 116 */
			return BNIL;
		}

	}



/* &lambda1939 */
	obj_t BGl_z62lambda1939z62zzast_varz00(obj_t BgL_envz00_3476,
		obj_t BgL_oz00_3477, obj_t BgL_vz00_3478)
	{
		{	/* Ast/var.scm 116 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_3477)))->BgL_propertyz00) =
				((obj_t) BgL_vz00_3478), BUNSPEC);
		}

	}



/* &lambda1938 */
	obj_t BGl_z62lambda1938z62zzast_varz00(obj_t BgL_envz00_3479,
		obj_t BgL_oz00_3480)
	{
		{	/* Ast/var.scm 116 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_3480)))->BgL_propertyz00);
		}

	}



/* &<@anonymous:1848> */
	obj_t BGl_z62zc3z04anonymousza31848ze3ze5zzast_varz00(obj_t BgL_envz00_3481,
		obj_t BgL_new1115z00_3482)
	{
		{	/* Ast/var.scm 84 */
			{
				BgL_funz00_bglt BgL_auxz00_6477;

				((((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt) BgL_new1115z00_3482)))->BgL_arityz00) =
					((long) 0L), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1115z00_3482)))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1115z00_3482)))->
						BgL_predicatezd2ofzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1115z00_3482)))->
						BgL_stackzd2allocatorzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1115z00_3482)))->
						BgL_topzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1115z00_3482)))->
						BgL_thezd2closurezd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1115z00_3482)))->
						BgL_effectz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1115z00_3482)))->
						BgL_failsafez00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1115z00_3482)))->
						BgL_argszd2noescapezd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1115z00_3482)))->
						BgL_argszd2retescapezd2) = ((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_6477 = ((BgL_funz00_bglt) BgL_new1115z00_3482);
				return ((obj_t) BgL_auxz00_6477);
			}
		}

	}



/* &lambda1846 */
	BgL_funz00_bglt BGl_z62lambda1846z62zzast_varz00(obj_t BgL_envz00_3483)
	{
		{	/* Ast/var.scm 84 */
			{	/* Ast/var.scm 84 */
				BgL_funz00_bglt BgL_new1114z00_4079;

				BgL_new1114z00_4079 =
					((BgL_funz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct BgL_funz00_bgl))));
				{	/* Ast/var.scm 84 */
					long BgL_arg1847z00_4080;

					BgL_arg1847z00_4080 = BGL_CLASS_NUM(BGl_funz00zzast_varz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1114z00_4079), BgL_arg1847z00_4080);
				}
				return BgL_new1114z00_4079;
			}
		}

	}



/* &lambda1844 */
	BgL_funz00_bglt BGl_z62lambda1844z62zzast_varz00(obj_t BgL_envz00_3484,
		obj_t BgL_arity1104z00_3485, obj_t BgL_sidezd2effect1105zd2_3486,
		obj_t BgL_predicatezd2of1106zd2_3487,
		obj_t BgL_stackzd2allocator1107zd2_3488, obj_t BgL_topzf31108zf3_3489,
		obj_t BgL_thezd2closure1109zd2_3490, obj_t BgL_effect1110z00_3491,
		obj_t BgL_failsafe1111z00_3492, obj_t BgL_argszd2noescape1112zd2_3493,
		obj_t BgL_argszd2retescape1113zd2_3494)
	{
		{	/* Ast/var.scm 84 */
			{	/* Ast/var.scm 84 */
				long BgL_arity1104z00_4081;
				bool_t BgL_topzf31108zf3_4082;

				BgL_arity1104z00_4081 = (long) CINT(BgL_arity1104z00_3485);
				BgL_topzf31108zf3_4082 = CBOOL(BgL_topzf31108zf3_3489);
				{	/* Ast/var.scm 84 */
					BgL_funz00_bglt BgL_new1231z00_4083;

					{	/* Ast/var.scm 84 */
						BgL_funz00_bglt BgL_new1230z00_4084;

						BgL_new1230z00_4084 =
							((BgL_funz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_funz00_bgl))));
						{	/* Ast/var.scm 84 */
							long BgL_arg1845z00_4085;

							BgL_arg1845z00_4085 = BGL_CLASS_NUM(BGl_funz00zzast_varz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1230z00_4084),
								BgL_arg1845z00_4085);
						}
						BgL_new1231z00_4083 = BgL_new1230z00_4084;
					}
					((((BgL_funz00_bglt) COBJECT(BgL_new1231z00_4083))->BgL_arityz00) =
						((long) BgL_arity1104z00_4081), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(BgL_new1231z00_4083))->
							BgL_sidezd2effectzd2) =
						((obj_t) BgL_sidezd2effect1105zd2_3486), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(BgL_new1231z00_4083))->
							BgL_predicatezd2ofzd2) =
						((obj_t) BgL_predicatezd2of1106zd2_3487), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(BgL_new1231z00_4083))->
							BgL_stackzd2allocatorzd2) =
						((obj_t) BgL_stackzd2allocator1107zd2_3488), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(BgL_new1231z00_4083))->BgL_topzf3zf3) =
						((bool_t) BgL_topzf31108zf3_4082), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(BgL_new1231z00_4083))->
							BgL_thezd2closurezd2) =
						((obj_t) BgL_thezd2closure1109zd2_3490), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(BgL_new1231z00_4083))->BgL_effectz00) =
						((obj_t) BgL_effect1110z00_3491), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(BgL_new1231z00_4083))->BgL_failsafez00) =
						((obj_t) BgL_failsafe1111z00_3492), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(BgL_new1231z00_4083))->
							BgL_argszd2noescapezd2) =
						((obj_t) BgL_argszd2noescape1112zd2_3493), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(BgL_new1231z00_4083))->
							BgL_argszd2retescapezd2) =
						((obj_t) BgL_argszd2retescape1113zd2_3494), BUNSPEC);
					return BgL_new1231z00_4083;
				}
			}
		}

	}



/* &<@anonymous:1923> */
	obj_t BGl_z62zc3z04anonymousza31923ze3ze5zzast_varz00(obj_t BgL_envz00_3495)
	{
		{	/* Ast/var.scm 84 */
			return BNIL;
		}

	}



/* &lambda1922 */
	obj_t BGl_z62lambda1922z62zzast_varz00(obj_t BgL_envz00_3496,
		obj_t BgL_oz00_3497, obj_t BgL_vz00_3498)
	{
		{	/* Ast/var.scm 84 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_3497)))->BgL_argszd2retescapezd2) =
				((obj_t) BgL_vz00_3498), BUNSPEC);
		}

	}



/* &lambda1921 */
	obj_t BGl_z62lambda1921z62zzast_varz00(obj_t BgL_envz00_3499,
		obj_t BgL_oz00_3500)
	{
		{	/* Ast/var.scm 84 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_3500)))->BgL_argszd2retescapezd2);
		}

	}



/* &<@anonymous:1916> */
	obj_t BGl_z62zc3z04anonymousza31916ze3ze5zzast_varz00(obj_t BgL_envz00_3501)
	{
		{	/* Ast/var.scm 84 */
			return BNIL;
		}

	}



/* &lambda1915 */
	obj_t BGl_z62lambda1915z62zzast_varz00(obj_t BgL_envz00_3502,
		obj_t BgL_oz00_3503, obj_t BgL_vz00_3504)
	{
		{	/* Ast/var.scm 84 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_3503)))->BgL_argszd2noescapezd2) =
				((obj_t) BgL_vz00_3504), BUNSPEC);
		}

	}



/* &lambda1914 */
	obj_t BGl_z62lambda1914z62zzast_varz00(obj_t BgL_envz00_3505,
		obj_t BgL_oz00_3506)
	{
		{	/* Ast/var.scm 84 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_3506)))->BgL_argszd2noescapezd2);
		}

	}



/* &<@anonymous:1909> */
	obj_t BGl_z62zc3z04anonymousza31909ze3ze5zzast_varz00(obj_t BgL_envz00_3507)
	{
		{	/* Ast/var.scm 84 */
			return BUNSPEC;
		}

	}



/* &lambda1908 */
	obj_t BGl_z62lambda1908z62zzast_varz00(obj_t BgL_envz00_3508,
		obj_t BgL_oz00_3509, obj_t BgL_vz00_3510)
	{
		{	/* Ast/var.scm 84 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_3509)))->BgL_failsafez00) =
				((obj_t) BgL_vz00_3510), BUNSPEC);
		}

	}



/* &lambda1907 */
	obj_t BGl_z62lambda1907z62zzast_varz00(obj_t BgL_envz00_3511,
		obj_t BgL_oz00_3512)
	{
		{	/* Ast/var.scm 84 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_3512)))->BgL_failsafez00);
		}

	}



/* &<@anonymous:1901> */
	obj_t BGl_z62zc3z04anonymousza31901ze3ze5zzast_varz00(obj_t BgL_envz00_3513)
	{
		{	/* Ast/var.scm 84 */
			return BUNSPEC;
		}

	}



/* &lambda1900 */
	obj_t BGl_z62lambda1900z62zzast_varz00(obj_t BgL_envz00_3514,
		obj_t BgL_oz00_3515, obj_t BgL_vz00_3516)
	{
		{	/* Ast/var.scm 84 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_3515)))->BgL_effectz00) =
				((obj_t) BgL_vz00_3516), BUNSPEC);
		}

	}



/* &lambda1899 */
	obj_t BGl_z62lambda1899z62zzast_varz00(obj_t BgL_envz00_3517,
		obj_t BgL_oz00_3518)
	{
		{	/* Ast/var.scm 84 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_3518)))->BgL_effectz00);
		}

	}



/* &<@anonymous:1893> */
	obj_t BGl_z62zc3z04anonymousza31893ze3ze5zzast_varz00(obj_t BgL_envz00_3519)
	{
		{	/* Ast/var.scm 84 */
			return BUNSPEC;
		}

	}



/* &lambda1892 */
	obj_t BGl_z62lambda1892z62zzast_varz00(obj_t BgL_envz00_3520,
		obj_t BgL_oz00_3521, obj_t BgL_vz00_3522)
	{
		{	/* Ast/var.scm 84 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_3521)))->BgL_thezd2closurezd2) =
				((obj_t) BgL_vz00_3522), BUNSPEC);
		}

	}



/* &lambda1891 */
	obj_t BGl_z62lambda1891z62zzast_varz00(obj_t BgL_envz00_3523,
		obj_t BgL_oz00_3524)
	{
		{	/* Ast/var.scm 84 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_3524)))->BgL_thezd2closurezd2);
		}

	}



/* &<@anonymous:1885> */
	obj_t BGl_z62zc3z04anonymousza31885ze3ze5zzast_varz00(obj_t BgL_envz00_3525)
	{
		{	/* Ast/var.scm 84 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &lambda1884 */
	obj_t BGl_z62lambda1884z62zzast_varz00(obj_t BgL_envz00_3526,
		obj_t BgL_oz00_3527, obj_t BgL_vz00_3528)
	{
		{	/* Ast/var.scm 84 */
			{	/* Ast/var.scm 84 */
				bool_t BgL_vz00_4097;

				BgL_vz00_4097 = CBOOL(BgL_vz00_3528);
				return
					((((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt) BgL_oz00_3527)))->BgL_topzf3zf3) =
					((bool_t) BgL_vz00_4097), BUNSPEC);
			}
		}

	}



/* &lambda1883 */
	obj_t BGl_z62lambda1883z62zzast_varz00(obj_t BgL_envz00_3529,
		obj_t BgL_oz00_3530)
	{
		{	/* Ast/var.scm 84 */
			return
				BBOOL(
				(((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_3530)))->BgL_topzf3zf3));
		}

	}



/* &<@anonymous:1877> */
	obj_t BGl_z62zc3z04anonymousza31877ze3ze5zzast_varz00(obj_t BgL_envz00_3531)
	{
		{	/* Ast/var.scm 84 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1876 */
	obj_t BGl_z62lambda1876z62zzast_varz00(obj_t BgL_envz00_3532,
		obj_t BgL_oz00_3533, obj_t BgL_vz00_3534)
	{
		{	/* Ast/var.scm 84 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_3533)))->BgL_stackzd2allocatorzd2) =
				((obj_t) BgL_vz00_3534), BUNSPEC);
		}

	}



/* &lambda1875 */
	obj_t BGl_z62lambda1875z62zzast_varz00(obj_t BgL_envz00_3535,
		obj_t BgL_oz00_3536)
	{
		{	/* Ast/var.scm 84 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_3536)))->BgL_stackzd2allocatorzd2);
		}

	}



/* &<@anonymous:1869> */
	obj_t BGl_z62zc3z04anonymousza31869ze3ze5zzast_varz00(obj_t BgL_envz00_3537)
	{
		{	/* Ast/var.scm 84 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1868 */
	obj_t BGl_z62lambda1868z62zzast_varz00(obj_t BgL_envz00_3538,
		obj_t BgL_oz00_3539, obj_t BgL_vz00_3540)
	{
		{	/* Ast/var.scm 84 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_3539)))->BgL_predicatezd2ofzd2) =
				((obj_t) BgL_vz00_3540), BUNSPEC);
		}

	}



/* &lambda1867 */
	obj_t BGl_z62lambda1867z62zzast_varz00(obj_t BgL_envz00_3541,
		obj_t BgL_oz00_3542)
	{
		{	/* Ast/var.scm 84 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_3542)))->BgL_predicatezd2ofzd2);
		}

	}



/* &<@anonymous:1861> */
	obj_t BGl_z62zc3z04anonymousza31861ze3ze5zzast_varz00(obj_t BgL_envz00_3543)
	{
		{	/* Ast/var.scm 84 */
			return BUNSPEC;
		}

	}



/* &lambda1860 */
	obj_t BGl_z62lambda1860z62zzast_varz00(obj_t BgL_envz00_3544,
		obj_t BgL_oz00_3545, obj_t BgL_vz00_3546)
	{
		{	/* Ast/var.scm 84 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_3545)))->BgL_sidezd2effectzd2) =
				((obj_t) BgL_vz00_3546), BUNSPEC);
		}

	}



/* &lambda1859 */
	obj_t BGl_z62lambda1859z62zzast_varz00(obj_t BgL_envz00_3547,
		obj_t BgL_oz00_3548)
	{
		{	/* Ast/var.scm 84 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_3548)))->BgL_sidezd2effectzd2);
		}

	}



/* &lambda1853 */
	obj_t BGl_z62lambda1853z62zzast_varz00(obj_t BgL_envz00_3549,
		obj_t BgL_oz00_3550, obj_t BgL_vz00_3551)
	{
		{	/* Ast/var.scm 84 */
			{	/* Ast/var.scm 84 */
				long BgL_vz00_4106;

				BgL_vz00_4106 = (long) CINT(BgL_vz00_3551);
				return
					((((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt) BgL_oz00_3550)))->BgL_arityz00) =
					((long) BgL_vz00_4106), BUNSPEC);
		}}

	}



/* &lambda1852 */
	obj_t BGl_z62lambda1852z62zzast_varz00(obj_t BgL_envz00_3552,
		obj_t BgL_oz00_3553)
	{
		{	/* Ast/var.scm 84 */
			return
				BINT(
				(((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_3553)))->BgL_arityz00));
		}

	}



/* &<@anonymous:1799> */
	obj_t BGl_z62zc3z04anonymousza31799ze3ze5zzast_varz00(obj_t BgL_envz00_3554,
		obj_t BgL_new1102z00_3555)
	{
		{	/* Ast/var.scm 75 */
			{
				BgL_localz00_bglt BgL_auxz00_6567;

				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_new1102z00_3555))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(74)), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1102z00_3555))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_6575;

					{	/* Ast/var.scm 75 */
						obj_t BgL_classz00_4109;

						BgL_classz00_4109 = BGl_typez00zztype_typez00;
						{	/* Ast/var.scm 75 */
							obj_t BgL__ortest_1117z00_4110;

							BgL__ortest_1117z00_4110 = BGL_CLASS_NIL(BgL_classz00_4109);
							if (CBOOL(BgL__ortest_1117z00_4110))
								{	/* Ast/var.scm 75 */
									BgL_auxz00_6575 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_4110);
								}
							else
								{	/* Ast/var.scm 75 */
									BgL_auxz00_6575 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4109));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_new1102z00_3555))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_6575), BUNSPEC);
				}
				{
					BgL_valuez00_bglt BgL_auxz00_6585;

					{	/* Ast/var.scm 75 */
						obj_t BgL_classz00_4111;

						BgL_classz00_4111 = BGl_valuez00zzast_varz00;
						{	/* Ast/var.scm 75 */
							obj_t BgL__ortest_1117z00_4112;

							BgL__ortest_1117z00_4112 = BGL_CLASS_NIL(BgL_classz00_4111);
							if (CBOOL(BgL__ortest_1117z00_4112))
								{	/* Ast/var.scm 75 */
									BgL_auxz00_6585 =
										((BgL_valuez00_bglt) BgL__ortest_1117z00_4112);
								}
							else
								{	/* Ast/var.scm 75 */
									BgL_auxz00_6585 =
										((BgL_valuez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4111));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_new1102z00_3555))))->
							BgL_valuez00) = ((BgL_valuez00_bglt) BgL_auxz00_6585), BUNSPEC);
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_new1102z00_3555))))->BgL_accessz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1102z00_3555))))->BgL_fastzd2alphazd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1102z00_3555))))->BgL_removablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1102z00_3555))))->BgL_occurrencez00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1102z00_3555))))->BgL_occurrencewz00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1102z00_3555))))->BgL_userzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
									BgL_new1102z00_3555)))->BgL_keyz00) = ((long) 0L), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
									BgL_new1102z00_3555)))->BgL_valzd2noescapezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
									BgL_new1102z00_3555)))->BgL_volatilez00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				BgL_auxz00_6567 = ((BgL_localz00_bglt) BgL_new1102z00_3555);
				return ((obj_t) BgL_auxz00_6567);
			}
		}

	}



/* &lambda1776 */
	BgL_localz00_bglt BGl_z62lambda1776z62zzast_varz00(obj_t BgL_envz00_3556)
	{
		{	/* Ast/var.scm 75 */
			{	/* Ast/var.scm 75 */
				BgL_localz00_bglt BgL_new1101z00_4113;

				BgL_new1101z00_4113 =
					((BgL_localz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_localz00_bgl))));
				{	/* Ast/var.scm 75 */
					long BgL_arg1798z00_4114;

					BgL_arg1798z00_4114 = BGL_CLASS_NUM(BGl_localz00zzast_varz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1101z00_4113), BgL_arg1798z00_4114);
				}
				{	/* Ast/var.scm 75 */
					BgL_objectz00_bglt BgL_tmpz00_6625;

					BgL_tmpz00_6625 = ((BgL_objectz00_bglt) BgL_new1101z00_4113);
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6625, BFALSE);
				}
				((BgL_objectz00_bglt) BgL_new1101z00_4113);
				return BgL_new1101z00_4113;
			}
		}

	}



/* &lambda1774 */
	BgL_localz00_bglt BGl_z62lambda1774z62zzast_varz00(obj_t BgL_envz00_3557,
		obj_t BgL_id1088z00_3558, obj_t BgL_name1089z00_3559,
		obj_t BgL_type1090z00_3560, obj_t BgL_value1091z00_3561,
		obj_t BgL_access1092z00_3562, obj_t BgL_fastzd2alpha1093zd2_3563,
		obj_t BgL_removable1094z00_3564, obj_t BgL_occurrence1095z00_3565,
		obj_t BgL_occurrencew1096z00_3566, obj_t BgL_userzf31097zf3_3567,
		obj_t BgL_key1098z00_3568, obj_t BgL_valzd2noescape1099zd2_3569,
		obj_t BgL_volatile1100z00_3570)
	{
		{	/* Ast/var.scm 75 */
			{	/* Ast/var.scm 75 */
				long BgL_occurrence1095z00_4118;
				long BgL_occurrencew1096z00_4119;
				bool_t BgL_userzf31097zf3_4120;
				long BgL_key1098z00_4121;
				bool_t BgL_volatile1100z00_4122;

				BgL_occurrence1095z00_4118 = (long) CINT(BgL_occurrence1095z00_3565);
				BgL_occurrencew1096z00_4119 = (long) CINT(BgL_occurrencew1096z00_3566);
				BgL_userzf31097zf3_4120 = CBOOL(BgL_userzf31097zf3_3567);
				BgL_key1098z00_4121 = (long) CINT(BgL_key1098z00_3568);
				BgL_volatile1100z00_4122 = CBOOL(BgL_volatile1100z00_3570);
				{	/* Ast/var.scm 75 */
					BgL_localz00_bglt BgL_new1229z00_4123;

					{	/* Ast/var.scm 75 */
						BgL_localz00_bglt BgL_new1228z00_4124;

						BgL_new1228z00_4124 =
							((BgL_localz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_localz00_bgl))));
						{	/* Ast/var.scm 75 */
							long BgL_arg1775z00_4125;

							BgL_arg1775z00_4125 = BGL_CLASS_NUM(BGl_localz00zzast_varz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1228z00_4124),
								BgL_arg1775z00_4125);
						}
						{	/* Ast/var.scm 75 */
							BgL_objectz00_bglt BgL_tmpz00_6638;

							BgL_tmpz00_6638 = ((BgL_objectz00_bglt) BgL_new1228z00_4124);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6638, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_new1228z00_4124);
						BgL_new1229z00_4123 = BgL_new1228z00_4124;
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_new1229z00_4123)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1088z00_3558)), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1229z00_4123)))->BgL_namez00) =
						((obj_t) BgL_name1089z00_3559), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1229z00_4123)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1090z00_3560)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1229z00_4123)))->BgL_valuez00) =
						((BgL_valuez00_bglt) ((BgL_valuez00_bglt) BgL_value1091z00_3561)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1229z00_4123)))->BgL_accessz00) =
						((obj_t) BgL_access1092z00_3562), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1229z00_4123)))->BgL_fastzd2alphazd2) =
						((obj_t) BgL_fastzd2alpha1093zd2_3563), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1229z00_4123)))->BgL_removablez00) =
						((obj_t) BgL_removable1094z00_3564), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1229z00_4123)))->BgL_occurrencez00) =
						((long) BgL_occurrence1095z00_4118), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1229z00_4123)))->BgL_occurrencewz00) =
						((long) BgL_occurrencew1096z00_4119), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1229z00_4123)))->BgL_userzf3zf3) =
						((bool_t) BgL_userzf31097zf3_4120), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(BgL_new1229z00_4123))->BgL_keyz00) =
						((long) BgL_key1098z00_4121), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(BgL_new1229z00_4123))->
							BgL_valzd2noescapezd2) =
						((obj_t) BgL_valzd2noescape1099zd2_3569), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(BgL_new1229z00_4123))->
							BgL_volatilez00) = ((bool_t) BgL_volatile1100z00_4122), BUNSPEC);
					return BgL_new1229z00_4123;
				}
			}
		}

	}



/* &<@anonymous:1837> */
	obj_t BGl_z62zc3z04anonymousza31837ze3ze5zzast_varz00(obj_t BgL_envz00_3571)
	{
		{	/* Ast/var.scm 75 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1836 */
	obj_t BGl_z62lambda1836z62zzast_varz00(obj_t BgL_envz00_3572,
		obj_t BgL_oz00_3573, obj_t BgL_vz00_3574)
	{
		{	/* Ast/var.scm 75 */
			{	/* Ast/var.scm 75 */
				bool_t BgL_vz00_4127;

				BgL_vz00_4127 = CBOOL(BgL_vz00_3574);
				return
					((((BgL_localz00_bglt) COBJECT(
								((BgL_localz00_bglt) BgL_oz00_3573)))->BgL_volatilez00) =
					((bool_t) BgL_vz00_4127), BUNSPEC);
			}
		}

	}



/* &lambda1835 */
	obj_t BGl_z62lambda1835z62zzast_varz00(obj_t BgL_envz00_3575,
		obj_t BgL_oz00_3576)
	{
		{	/* Ast/var.scm 75 */
			return
				BBOOL(
				(((BgL_localz00_bglt) COBJECT(
							((BgL_localz00_bglt) BgL_oz00_3576)))->BgL_volatilez00));
		}

	}



/* &<@anonymous:1826> */
	obj_t BGl_z62zc3z04anonymousza31826ze3ze5zzast_varz00(obj_t BgL_envz00_3577)
	{
		{	/* Ast/var.scm 75 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &lambda1825 */
	obj_t BGl_z62lambda1825z62zzast_varz00(obj_t BgL_envz00_3578,
		obj_t BgL_oz00_3579, obj_t BgL_vz00_3580)
	{
		{	/* Ast/var.scm 75 */
			return
				((((BgL_localz00_bglt) COBJECT(
							((BgL_localz00_bglt) BgL_oz00_3579)))->BgL_valzd2noescapezd2) =
				((obj_t) BgL_vz00_3580), BUNSPEC);
		}

	}



/* &lambda1824 */
	obj_t BGl_z62lambda1824z62zzast_varz00(obj_t BgL_envz00_3581,
		obj_t BgL_oz00_3582)
	{
		{	/* Ast/var.scm 75 */
			return
				(((BgL_localz00_bglt) COBJECT(
						((BgL_localz00_bglt) BgL_oz00_3582)))->BgL_valzd2noescapezd2);
		}

	}



/* &lambda1810 */
	obj_t BGl_z62lambda1810z62zzast_varz00(obj_t BgL_envz00_3583,
		obj_t BgL_oz00_3584, obj_t BgL_vz00_3585)
	{
		{	/* Ast/var.scm 75 */
			{	/* Ast/var.scm 75 */
				long BgL_vz00_4132;

				BgL_vz00_4132 = (long) CINT(BgL_vz00_3585);
				return
					((((BgL_localz00_bglt) COBJECT(
								((BgL_localz00_bglt) BgL_oz00_3584)))->BgL_keyz00) =
					((long) BgL_vz00_4132), BUNSPEC);
		}}

	}



/* &lambda1809 */
	obj_t BGl_z62lambda1809z62zzast_varz00(obj_t BgL_envz00_3586,
		obj_t BgL_oz00_3587)
	{
		{	/* Ast/var.scm 75 */
			return
				BINT(
				(((BgL_localz00_bglt) COBJECT(
							((BgL_localz00_bglt) BgL_oz00_3587)))->BgL_keyz00));
		}

	}



/* &<@anonymous:1630> */
	obj_t BGl_z62zc3z04anonymousza31630ze3ze5zzast_varz00(obj_t BgL_envz00_3588,
		obj_t BgL_new1086z00_3589)
	{
		{	/* Ast/var.scm 46 */
			{
				BgL_globalz00_bglt BgL_auxz00_6686;

				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_globalz00_bglt) BgL_new1086z00_3589))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(74)), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1086z00_3589))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_6694;

					{	/* Ast/var.scm 46 */
						obj_t BgL_classz00_4135;

						BgL_classz00_4135 = BGl_typez00zztype_typez00;
						{	/* Ast/var.scm 46 */
							obj_t BgL__ortest_1117z00_4136;

							BgL__ortest_1117z00_4136 = BGL_CLASS_NIL(BgL_classz00_4135);
							if (CBOOL(BgL__ortest_1117z00_4136))
								{	/* Ast/var.scm 46 */
									BgL_auxz00_6694 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_4136);
								}
							else
								{	/* Ast/var.scm 46 */
									BgL_auxz00_6694 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4135));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt) BgL_new1086z00_3589))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_6694), BUNSPEC);
				}
				{
					BgL_valuez00_bglt BgL_auxz00_6704;

					{	/* Ast/var.scm 46 */
						obj_t BgL_classz00_4137;

						BgL_classz00_4137 = BGl_valuez00zzast_varz00;
						{	/* Ast/var.scm 46 */
							obj_t BgL__ortest_1117z00_4138;

							BgL__ortest_1117z00_4138 = BGL_CLASS_NIL(BgL_classz00_4137);
							if (CBOOL(BgL__ortest_1117z00_4138))
								{	/* Ast/var.scm 46 */
									BgL_auxz00_6704 =
										((BgL_valuez00_bglt) BgL__ortest_1117z00_4138);
								}
							else
								{	/* Ast/var.scm 46 */
									BgL_auxz00_6704 =
										((BgL_valuez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4137));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt) BgL_new1086z00_3589))))->
							BgL_valuez00) = ((BgL_valuez00_bglt) BgL_auxz00_6704), BUNSPEC);
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_globalz00_bglt) BgL_new1086z00_3589))))->
						BgL_accessz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1086z00_3589))))->BgL_fastzd2alphazd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1086z00_3589))))->BgL_removablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1086z00_3589))))->BgL_occurrencez00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1086z00_3589))))->BgL_occurrencewz00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1086z00_3589))))->BgL_userzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1086z00_3589)))->BgL_modulez00) =
					((obj_t) CNST_TABLE_REF(74)), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1086z00_3589)))->BgL_importz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1086z00_3589)))->BgL_evaluablezf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1086z00_3589)))->BgL_evalzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1086z00_3589)))->BgL_libraryz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1086z00_3589)))->BgL_pragmaz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1086z00_3589)))->BgL_srcz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1086z00_3589)))->BgL_jvmzd2typezd2namez00) =
					((obj_t) BGl_string2372z00zzast_varz00), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1086z00_3589)))->BgL_initz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1086z00_3589)))->BgL_aliasz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_6686 = ((BgL_globalz00_bglt) BgL_new1086z00_3589);
				return ((obj_t) BgL_auxz00_6686);
			}
		}

	}



/* &lambda1628 */
	BgL_globalz00_bglt BGl_z62lambda1628z62zzast_varz00(obj_t BgL_envz00_3590)
	{
		{	/* Ast/var.scm 46 */
			{	/* Ast/var.scm 46 */
				BgL_globalz00_bglt BgL_new1085z00_4139;

				BgL_new1085z00_4139 =
					((BgL_globalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_globalz00_bgl))));
				{	/* Ast/var.scm 46 */
					long BgL_arg1629z00_4140;

					BgL_arg1629z00_4140 = BGL_CLASS_NUM(BGl_globalz00zzast_varz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1085z00_4139), BgL_arg1629z00_4140);
				}
				{	/* Ast/var.scm 46 */
					BgL_objectz00_bglt BgL_tmpz00_6759;

					BgL_tmpz00_6759 = ((BgL_objectz00_bglt) BgL_new1085z00_4139);
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6759, BFALSE);
				}
				((BgL_objectz00_bglt) BgL_new1085z00_4139);
				return BgL_new1085z00_4139;
			}
		}

	}



/* &lambda1626 */
	BgL_globalz00_bglt BGl_z62lambda1626z62zzast_varz00(obj_t BgL_envz00_3591,
		obj_t BgL_id1065z00_3592, obj_t BgL_name1066z00_3593,
		obj_t BgL_type1067z00_3594, obj_t BgL_value1068z00_3595,
		obj_t BgL_access1069z00_3596, obj_t BgL_fastzd2alpha1070zd2_3597,
		obj_t BgL_removable1071z00_3598, obj_t BgL_occurrence1072z00_3599,
		obj_t BgL_occurrencew1073z00_3600, obj_t BgL_userzf31074zf3_3601,
		obj_t BgL_module1075z00_3602, obj_t BgL_import1076z00_3603,
		obj_t BgL_evaluablezf31077zf3_3604, obj_t BgL_evalzf31078zf3_3605,
		obj_t BgL_library1079z00_3606, obj_t BgL_pragma1080z00_3607,
		obj_t BgL_src1081z00_3608, obj_t BgL_jvmzd2typezd2name1082z00_3609,
		obj_t BgL_init1083z00_3610, obj_t BgL_alias1084z00_3611)
	{
		{	/* Ast/var.scm 46 */
			{	/* Ast/var.scm 46 */
				long BgL_occurrence1072z00_4144;
				long BgL_occurrencew1073z00_4145;
				bool_t BgL_userzf31074zf3_4146;
				bool_t BgL_evaluablezf31077zf3_4148;
				bool_t BgL_evalzf31078zf3_4149;

				BgL_occurrence1072z00_4144 = (long) CINT(BgL_occurrence1072z00_3599);
				BgL_occurrencew1073z00_4145 = (long) CINT(BgL_occurrencew1073z00_3600);
				BgL_userzf31074zf3_4146 = CBOOL(BgL_userzf31074zf3_3601);
				BgL_evaluablezf31077zf3_4148 = CBOOL(BgL_evaluablezf31077zf3_3604);
				BgL_evalzf31078zf3_4149 = CBOOL(BgL_evalzf31078zf3_3605);
				{	/* Ast/var.scm 46 */
					BgL_globalz00_bglt BgL_new1226z00_4151;

					{	/* Ast/var.scm 46 */
						BgL_globalz00_bglt BgL_new1225z00_4152;

						BgL_new1225z00_4152 =
							((BgL_globalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_globalz00_bgl))));
						{	/* Ast/var.scm 46 */
							long BgL_arg1627z00_4153;

							BgL_arg1627z00_4153 = BGL_CLASS_NUM(BGl_globalz00zzast_varz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1225z00_4152),
								BgL_arg1627z00_4153);
						}
						{	/* Ast/var.scm 46 */
							BgL_objectz00_bglt BgL_tmpz00_6772;

							BgL_tmpz00_6772 = ((BgL_objectz00_bglt) BgL_new1225z00_4152);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6772, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_new1225z00_4152);
						BgL_new1226z00_4151 = BgL_new1225z00_4152;
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_new1226z00_4151)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1065z00_3592)), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1226z00_4151)))->BgL_namez00) =
						((obj_t) BgL_name1066z00_3593), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1226z00_4151)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1067z00_3594)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1226z00_4151)))->BgL_valuez00) =
						((BgL_valuez00_bglt) ((BgL_valuez00_bglt) BgL_value1068z00_3595)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1226z00_4151)))->BgL_accessz00) =
						((obj_t) BgL_access1069z00_3596), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1226z00_4151)))->BgL_fastzd2alphazd2) =
						((obj_t) BgL_fastzd2alpha1070zd2_3597), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1226z00_4151)))->BgL_removablez00) =
						((obj_t) BgL_removable1071z00_3598), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1226z00_4151)))->BgL_occurrencez00) =
						((long) BgL_occurrence1072z00_4144), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1226z00_4151)))->BgL_occurrencewz00) =
						((long) BgL_occurrencew1073z00_4145), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1226z00_4151)))->BgL_userzf3zf3) =
						((bool_t) BgL_userzf31074zf3_4146), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(BgL_new1226z00_4151))->
							BgL_modulez00) =
						((obj_t) ((obj_t) BgL_module1075z00_3602)), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(BgL_new1226z00_4151))->
							BgL_importz00) = ((obj_t) BgL_import1076z00_3603), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(BgL_new1226z00_4151))->
							BgL_evaluablezf3zf3) =
						((bool_t) BgL_evaluablezf31077zf3_4148), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(BgL_new1226z00_4151))->
							BgL_evalzf3zf3) = ((bool_t) BgL_evalzf31078zf3_4149), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(BgL_new1226z00_4151))->
							BgL_libraryz00) = ((obj_t) BgL_library1079z00_3606), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(BgL_new1226z00_4151))->
							BgL_pragmaz00) = ((obj_t) BgL_pragma1080z00_3607), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(BgL_new1226z00_4151))->BgL_srcz00) =
						((obj_t) BgL_src1081z00_3608), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(BgL_new1226z00_4151))->
							BgL_jvmzd2typezd2namez00) =
						((obj_t) ((obj_t) BgL_jvmzd2typezd2name1082z00_3609)), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(BgL_new1226z00_4151))->BgL_initz00) =
						((obj_t) BgL_init1083z00_3610), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(BgL_new1226z00_4151))->BgL_aliasz00) =
						((obj_t) BgL_alias1084z00_3611), BUNSPEC);
					return BgL_new1226z00_4151;
				}
			}
		}

	}



/* &<@anonymous:1764> */
	obj_t BGl_z62zc3z04anonymousza31764ze3ze5zzast_varz00(obj_t BgL_envz00_3612)
	{
		{	/* Ast/var.scm 46 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1763 */
	obj_t BGl_z62lambda1763z62zzast_varz00(obj_t BgL_envz00_3613,
		obj_t BgL_oz00_3614, obj_t BgL_vz00_3615)
	{
		{	/* Ast/var.scm 46 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_3614)))->BgL_aliasz00) =
				((obj_t) BgL_vz00_3615), BUNSPEC);
		}

	}



/* &lambda1762 */
	obj_t BGl_z62lambda1762z62zzast_varz00(obj_t BgL_envz00_3616,
		obj_t BgL_oz00_3617)
	{
		{	/* Ast/var.scm 46 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_3617)))->BgL_aliasz00);
		}

	}



/* &<@anonymous:1752> */
	obj_t BGl_z62zc3z04anonymousza31752ze3ze5zzast_varz00(obj_t BgL_envz00_3618)
	{
		{	/* Ast/var.scm 46 */
			return BUNSPEC;
		}

	}



/* &lambda1751 */
	obj_t BGl_z62lambda1751z62zzast_varz00(obj_t BgL_envz00_3619,
		obj_t BgL_oz00_3620, obj_t BgL_vz00_3621)
	{
		{	/* Ast/var.scm 46 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_3620)))->BgL_initz00) =
				((obj_t) BgL_vz00_3621), BUNSPEC);
		}

	}



/* &lambda1750 */
	obj_t BGl_z62lambda1750z62zzast_varz00(obj_t BgL_envz00_3622,
		obj_t BgL_oz00_3623)
	{
		{	/* Ast/var.scm 46 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_3623)))->BgL_initz00);
		}

	}



/* &lambda1741 */
	obj_t BGl_z62lambda1741z62zzast_varz00(obj_t BgL_envz00_3624,
		obj_t BgL_oz00_3625, obj_t BgL_vz00_3626)
	{
		{	/* Ast/var.scm 46 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_3625)))->
					BgL_jvmzd2typezd2namez00) =
				((obj_t) ((obj_t) BgL_vz00_3626)), BUNSPEC);
		}

	}



/* &lambda1740 */
	obj_t BGl_z62lambda1740z62zzast_varz00(obj_t BgL_envz00_3627,
		obj_t BgL_oz00_3628)
	{
		{	/* Ast/var.scm 46 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_3628)))->BgL_jvmzd2typezd2namez00);
		}

	}



/* &lambda1736 */
	obj_t BGl_z62lambda1736z62zzast_varz00(obj_t BgL_envz00_3629,
		obj_t BgL_oz00_3630, obj_t BgL_vz00_3631)
	{
		{	/* Ast/var.scm 46 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_3630)))->BgL_srcz00) =
				((obj_t) BgL_vz00_3631), BUNSPEC);
		}

	}



/* &lambda1735 */
	obj_t BGl_z62lambda1735z62zzast_varz00(obj_t BgL_envz00_3632,
		obj_t BgL_oz00_3633)
	{
		{	/* Ast/var.scm 46 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_3633)))->BgL_srcz00);
		}

	}



/* &<@anonymous:1723> */
	obj_t BGl_z62zc3z04anonymousza31723ze3ze5zzast_varz00(obj_t BgL_envz00_3634)
	{
		{	/* Ast/var.scm 46 */
			return BNIL;
		}

	}



/* &lambda1722 */
	obj_t BGl_z62lambda1722z62zzast_varz00(obj_t BgL_envz00_3635,
		obj_t BgL_oz00_3636, obj_t BgL_vz00_3637)
	{
		{	/* Ast/var.scm 46 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_3636)))->BgL_pragmaz00) =
				((obj_t) BgL_vz00_3637), BUNSPEC);
		}

	}



/* &lambda1721 */
	obj_t BGl_z62lambda1721z62zzast_varz00(obj_t BgL_envz00_3638,
		obj_t BgL_oz00_3639)
	{
		{	/* Ast/var.scm 46 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_3639)))->BgL_pragmaz00);
		}

	}



/* &<@anonymous:1713> */
	obj_t BGl_z62zc3z04anonymousza31713ze3ze5zzast_varz00(obj_t BgL_envz00_3640)
	{
		{	/* Ast/var.scm 46 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1712 */
	obj_t BGl_z62lambda1712z62zzast_varz00(obj_t BgL_envz00_3641,
		obj_t BgL_oz00_3642, obj_t BgL_vz00_3643)
	{
		{	/* Ast/var.scm 46 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_3642)))->BgL_libraryz00) =
				((obj_t) BgL_vz00_3643), BUNSPEC);
		}

	}



/* &lambda1711 */
	obj_t BGl_z62lambda1711z62zzast_varz00(obj_t BgL_envz00_3644,
		obj_t BgL_oz00_3645)
	{
		{	/* Ast/var.scm 46 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_3645)))->BgL_libraryz00);
		}

	}



/* &<@anonymous:1704> */
	obj_t BGl_z62zc3z04anonymousza31704ze3ze5zzast_varz00(obj_t BgL_envz00_3646)
	{
		{	/* Ast/var.scm 46 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1703 */
	obj_t BGl_z62lambda1703z62zzast_varz00(obj_t BgL_envz00_3647,
		obj_t BgL_oz00_3648, obj_t BgL_vz00_3649)
	{
		{	/* Ast/var.scm 46 */
			{	/* Ast/var.scm 46 */
				bool_t BgL_vz00_4168;

				BgL_vz00_4168 = CBOOL(BgL_vz00_3649);
				return
					((((BgL_globalz00_bglt) COBJECT(
								((BgL_globalz00_bglt) BgL_oz00_3648)))->BgL_evalzf3zf3) =
					((bool_t) BgL_vz00_4168), BUNSPEC);
			}
		}

	}



/* &lambda1702 */
	obj_t BGl_z62lambda1702z62zzast_varz00(obj_t BgL_envz00_3650,
		obj_t BgL_oz00_3651)
	{
		{	/* Ast/var.scm 46 */
			return
				BBOOL(
				(((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_3651)))->BgL_evalzf3zf3));
		}

	}



/* &<@anonymous:1691> */
	obj_t BGl_z62zc3z04anonymousza31691ze3ze5zzast_varz00(obj_t BgL_envz00_3652)
	{
		{	/* Ast/var.scm 46 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &lambda1690 */
	obj_t BGl_z62lambda1690z62zzast_varz00(obj_t BgL_envz00_3653,
		obj_t BgL_oz00_3654, obj_t BgL_vz00_3655)
	{
		{	/* Ast/var.scm 46 */
			{	/* Ast/var.scm 46 */
				bool_t BgL_vz00_4171;

				BgL_vz00_4171 = CBOOL(BgL_vz00_3655);
				return
					((((BgL_globalz00_bglt) COBJECT(
								((BgL_globalz00_bglt) BgL_oz00_3654)))->BgL_evaluablezf3zf3) =
					((bool_t) BgL_vz00_4171), BUNSPEC);
			}
		}

	}



/* &lambda1689 */
	obj_t BGl_z62lambda1689z62zzast_varz00(obj_t BgL_envz00_3656,
		obj_t BgL_oz00_3657)
	{
		{	/* Ast/var.scm 46 */
			return
				BBOOL(
				(((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_3657)))->BgL_evaluablezf3zf3));
		}

	}



/* &lambda1665 */
	obj_t BGl_z62lambda1665z62zzast_varz00(obj_t BgL_envz00_3658,
		obj_t BgL_oz00_3659, obj_t BgL_vz00_3660)
	{
		{	/* Ast/var.scm 46 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_3659)))->BgL_importz00) =
				((obj_t) BgL_vz00_3660), BUNSPEC);
		}

	}



/* &lambda1664 */
	obj_t BGl_z62lambda1664z62zzast_varz00(obj_t BgL_envz00_3661,
		obj_t BgL_oz00_3662)
	{
		{	/* Ast/var.scm 46 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_3662)))->BgL_importz00);
		}

	}



/* &lambda1652 */
	obj_t BGl_z62lambda1652z62zzast_varz00(obj_t BgL_envz00_3663,
		obj_t BgL_oz00_3664, obj_t BgL_vz00_3665)
	{
		{	/* Ast/var.scm 46 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_3664)))->BgL_modulez00) = ((obj_t)
					((obj_t) BgL_vz00_3665)), BUNSPEC);
		}

	}



/* &lambda1651 */
	obj_t BGl_z62lambda1651z62zzast_varz00(obj_t BgL_envz00_3666,
		obj_t BgL_oz00_3667)
	{
		{	/* Ast/var.scm 46 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_3667)))->BgL_modulez00);
		}

	}



/* &<@anonymous:1381> */
	obj_t BGl_z62zc3z04anonymousza31381ze3ze5zzast_varz00(obj_t BgL_envz00_3668,
		obj_t BgL_new1063z00_3669)
	{
		{	/* Ast/var.scm 23 */
			{
				BgL_variablez00_bglt BgL_auxz00_6861;

				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_new1063z00_3669)))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(74)), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1063z00_3669)))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_6867;

					{	/* Ast/var.scm 23 */
						obj_t BgL_classz00_4179;

						BgL_classz00_4179 = BGl_typez00zztype_typez00;
						{	/* Ast/var.scm 23 */
							obj_t BgL__ortest_1117z00_4180;

							BgL__ortest_1117z00_4180 = BGL_CLASS_NIL(BgL_classz00_4179);
							if (CBOOL(BgL__ortest_1117z00_4180))
								{	/* Ast/var.scm 23 */
									BgL_auxz00_6867 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_4180);
								}
							else
								{	/* Ast/var.scm 23 */
									BgL_auxz00_6867 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4179));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_new1063z00_3669)))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_6867), BUNSPEC);
				}
				{
					BgL_valuez00_bglt BgL_auxz00_6876;

					{	/* Ast/var.scm 23 */
						obj_t BgL_classz00_4181;

						BgL_classz00_4181 = BGl_valuez00zzast_varz00;
						{	/* Ast/var.scm 23 */
							obj_t BgL__ortest_1117z00_4182;

							BgL__ortest_1117z00_4182 = BGL_CLASS_NIL(BgL_classz00_4181);
							if (CBOOL(BgL__ortest_1117z00_4182))
								{	/* Ast/var.scm 23 */
									BgL_auxz00_6876 =
										((BgL_valuez00_bglt) BgL__ortest_1117z00_4182);
								}
							else
								{	/* Ast/var.scm 23 */
									BgL_auxz00_6876 =
										((BgL_valuez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4181));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_new1063z00_3669)))->
							BgL_valuez00) = ((BgL_valuez00_bglt) BgL_auxz00_6876), BUNSPEC);
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_new1063z00_3669)))->BgL_accessz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1063z00_3669)))->BgL_fastzd2alphazd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1063z00_3669)))->BgL_removablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1063z00_3669)))->BgL_occurrencez00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1063z00_3669)))->BgL_occurrencewz00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1063z00_3669)))->BgL_userzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				BgL_auxz00_6861 = ((BgL_variablez00_bglt) BgL_new1063z00_3669);
				return ((obj_t) BgL_auxz00_6861);
			}
		}

	}



/* &lambda1379 */
	BgL_variablez00_bglt BGl_z62lambda1379z62zzast_varz00(obj_t BgL_envz00_3670)
	{
		{	/* Ast/var.scm 23 */
			{	/* Ast/var.scm 23 */
				BgL_variablez00_bglt BgL_new1062z00_4183;

				BgL_new1062z00_4183 =
					((BgL_variablez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_variablez00_bgl))));
				{	/* Ast/var.scm 23 */
					long BgL_arg1380z00_4184;

					BgL_arg1380z00_4184 = BGL_CLASS_NUM(BGl_variablez00zzast_varz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1062z00_4183), BgL_arg1380z00_4184);
				}
				return BgL_new1062z00_4183;
			}
		}

	}



/* &lambda1377 */
	BgL_variablez00_bglt BGl_z62lambda1377z62zzast_varz00(obj_t BgL_envz00_3671,
		obj_t BgL_id1052z00_3672, obj_t BgL_name1053z00_3673,
		obj_t BgL_type1054z00_3674, obj_t BgL_value1055z00_3675,
		obj_t BgL_access1056z00_3676, obj_t BgL_fastzd2alpha1057zd2_3677,
		obj_t BgL_removable1058z00_3678, obj_t BgL_occurrence1059z00_3679,
		obj_t BgL_occurrencew1060z00_3680, obj_t BgL_userzf31061zf3_3681)
	{
		{	/* Ast/var.scm 23 */
			{	/* Ast/var.scm 23 */
				long BgL_occurrence1059z00_4188;
				long BgL_occurrencew1060z00_4189;
				bool_t BgL_userzf31061zf3_4190;

				BgL_occurrence1059z00_4188 = (long) CINT(BgL_occurrence1059z00_3679);
				BgL_occurrencew1060z00_4189 = (long) CINT(BgL_occurrencew1060z00_3680);
				BgL_userzf31061zf3_4190 = CBOOL(BgL_userzf31061zf3_3681);
				{	/* Ast/var.scm 23 */
					BgL_variablez00_bglt BgL_new1224z00_4191;

					{	/* Ast/var.scm 23 */
						BgL_variablez00_bglt BgL_new1223z00_4192;

						BgL_new1223z00_4192 =
							((BgL_variablez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_variablez00_bgl))));
						{	/* Ast/var.scm 23 */
							long BgL_arg1378z00_4193;

							BgL_arg1378z00_4193 = BGL_CLASS_NUM(BGl_variablez00zzast_varz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1223z00_4192),
								BgL_arg1378z00_4193);
						}
						BgL_new1224z00_4191 = BgL_new1223z00_4192;
					}
					((((BgL_variablez00_bglt) COBJECT(BgL_new1224z00_4191))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1052z00_3672)), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(BgL_new1224z00_4191))->
							BgL_namez00) = ((obj_t) BgL_name1053z00_3673), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(BgL_new1224z00_4191))->
							BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1054z00_3674)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(BgL_new1224z00_4191))->
							BgL_valuez00) =
						((BgL_valuez00_bglt) ((BgL_valuez00_bglt) BgL_value1055z00_3675)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(BgL_new1224z00_4191))->
							BgL_accessz00) = ((obj_t) BgL_access1056z00_3676), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(BgL_new1224z00_4191))->
							BgL_fastzd2alphazd2) =
						((obj_t) BgL_fastzd2alpha1057zd2_3677), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(BgL_new1224z00_4191))->
							BgL_removablez00) = ((obj_t) BgL_removable1058z00_3678), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(BgL_new1224z00_4191))->
							BgL_occurrencez00) =
						((long) BgL_occurrence1059z00_4188), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(BgL_new1224z00_4191))->
							BgL_occurrencewz00) =
						((long) BgL_occurrencew1060z00_4189), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(BgL_new1224z00_4191))->
							BgL_userzf3zf3) = ((bool_t) BgL_userzf31061zf3_4190), BUNSPEC);
					return BgL_new1224z00_4191;
				}
			}
		}

	}



/* &<@anonymous:1609> */
	obj_t BGl_z62zc3z04anonymousza31609ze3ze5zzast_varz00(obj_t BgL_envz00_3682)
	{
		{	/* Ast/var.scm 23 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1608 */
	obj_t BGl_z62lambda1608z62zzast_varz00(obj_t BgL_envz00_3683,
		obj_t BgL_oz00_3684, obj_t BgL_vz00_3685)
	{
		{	/* Ast/var.scm 23 */
			{	/* Ast/var.scm 23 */
				bool_t BgL_vz00_4195;

				BgL_vz00_4195 = CBOOL(BgL_vz00_3685);
				return
					((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_oz00_3684)))->BgL_userzf3zf3) =
					((bool_t) BgL_vz00_4195), BUNSPEC);
			}
		}

	}



/* &lambda1607 */
	obj_t BGl_z62lambda1607z62zzast_varz00(obj_t BgL_envz00_3686,
		obj_t BgL_oz00_3687)
	{
		{	/* Ast/var.scm 23 */
			return
				BBOOL(
				(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_3687)))->BgL_userzf3zf3));
		}

	}



/* &<@anonymous:1594> */
	obj_t BGl_z62zc3z04anonymousza31594ze3ze5zzast_varz00(obj_t BgL_envz00_3688)
	{
		{	/* Ast/var.scm 23 */
			return BINT(0L);
		}

	}



/* &lambda1593 */
	obj_t BGl_z62lambda1593z62zzast_varz00(obj_t BgL_envz00_3689,
		obj_t BgL_oz00_3690, obj_t BgL_vz00_3691)
	{
		{	/* Ast/var.scm 23 */
			{	/* Ast/var.scm 23 */
				long BgL_vz00_4198;

				BgL_vz00_4198 = (long) CINT(BgL_vz00_3691);
				return
					((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_oz00_3690)))->BgL_occurrencewz00) =
					((long) BgL_vz00_4198), BUNSPEC);
		}}

	}



/* &lambda1592 */
	obj_t BGl_z62lambda1592z62zzast_varz00(obj_t BgL_envz00_3692,
		obj_t BgL_oz00_3693)
	{
		{	/* Ast/var.scm 23 */
			return
				BINT(
				(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_3693)))->BgL_occurrencewz00));
		}

	}



/* &<@anonymous:1579> */
	obj_t BGl_z62zc3z04anonymousza31579ze3ze5zzast_varz00(obj_t BgL_envz00_3694)
	{
		{	/* Ast/var.scm 23 */
			return BINT(0L);
		}

	}



/* &lambda1578 */
	obj_t BGl_z62lambda1578z62zzast_varz00(obj_t BgL_envz00_3695,
		obj_t BgL_oz00_3696, obj_t BgL_vz00_3697)
	{
		{	/* Ast/var.scm 23 */
			{	/* Ast/var.scm 23 */
				long BgL_vz00_4201;

				BgL_vz00_4201 = (long) CINT(BgL_vz00_3697);
				return
					((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_oz00_3696)))->BgL_occurrencez00) =
					((long) BgL_vz00_4201), BUNSPEC);
		}}

	}



/* &lambda1577 */
	obj_t BGl_z62lambda1577z62zzast_varz00(obj_t BgL_envz00_3698,
		obj_t BgL_oz00_3699)
	{
		{	/* Ast/var.scm 23 */
			return
				BINT(
				(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_3699)))->BgL_occurrencez00));
		}

	}



/* &<@anonymous:1568> */
	obj_t BGl_z62zc3z04anonymousza31568ze3ze5zzast_varz00(obj_t BgL_envz00_3700)
	{
		{	/* Ast/var.scm 23 */
			return CNST_TABLE_REF(76);
		}

	}



/* &lambda1567 */
	obj_t BGl_z62lambda1567z62zzast_varz00(obj_t BgL_envz00_3701,
		obj_t BgL_oz00_3702, obj_t BgL_vz00_3703)
	{
		{	/* Ast/var.scm 23 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_3702)))->BgL_removablez00) =
				((obj_t) BgL_vz00_3703), BUNSPEC);
		}

	}



/* &lambda1566 */
	obj_t BGl_z62lambda1566z62zzast_varz00(obj_t BgL_envz00_3704,
		obj_t BgL_oz00_3705)
	{
		{	/* Ast/var.scm 23 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_3705)))->BgL_removablez00);
		}

	}



/* &<@anonymous:1555> */
	obj_t BGl_z62zc3z04anonymousza31555ze3ze5zzast_varz00(obj_t BgL_envz00_3706)
	{
		{	/* Ast/var.scm 23 */
			return BUNSPEC;
		}

	}



/* &lambda1554 */
	obj_t BGl_z62lambda1554z62zzast_varz00(obj_t BgL_envz00_3707,
		obj_t BgL_oz00_3708, obj_t BgL_vz00_3709)
	{
		{	/* Ast/var.scm 23 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_3708)))->BgL_fastzd2alphazd2) =
				((obj_t) BgL_vz00_3709), BUNSPEC);
		}

	}



/* &lambda1553 */
	obj_t BGl_z62lambda1553z62zzast_varz00(obj_t BgL_envz00_3710,
		obj_t BgL_oz00_3711)
	{
		{	/* Ast/var.scm 23 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_3711)))->BgL_fastzd2alphazd2);
		}

	}



/* &<@anonymous:1538> */
	obj_t BGl_z62zc3z04anonymousza31538ze3ze5zzast_varz00(obj_t BgL_envz00_3712)
	{
		{	/* Ast/var.scm 23 */
			return CNST_TABLE_REF(71);
		}

	}



/* &lambda1537 */
	obj_t BGl_z62lambda1537z62zzast_varz00(obj_t BgL_envz00_3713,
		obj_t BgL_oz00_3714, obj_t BgL_vz00_3715)
	{
		{	/* Ast/var.scm 23 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_3714)))->BgL_accessz00) =
				((obj_t) BgL_vz00_3715), BUNSPEC);
		}

	}



/* &lambda1536 */
	obj_t BGl_z62lambda1536z62zzast_varz00(obj_t BgL_envz00_3716,
		obj_t BgL_oz00_3717)
	{
		{	/* Ast/var.scm 23 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_3717)))->BgL_accessz00);
		}

	}



/* &lambda1511 */
	obj_t BGl_z62lambda1511z62zzast_varz00(obj_t BgL_envz00_3718,
		obj_t BgL_oz00_3719, obj_t BgL_vz00_3720)
	{
		{	/* Ast/var.scm 23 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_3719)))->BgL_valuez00) =
				((BgL_valuez00_bglt) ((BgL_valuez00_bglt) BgL_vz00_3720)), BUNSPEC);
		}

	}



/* &lambda1510 */
	BgL_valuez00_bglt BGl_z62lambda1510z62zzast_varz00(obj_t BgL_envz00_3721,
		obj_t BgL_oz00_3722)
	{
		{	/* Ast/var.scm 23 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_3722)))->BgL_valuez00);
		}

	}



/* &lambda1487 */
	obj_t BGl_z62lambda1487z62zzast_varz00(obj_t BgL_envz00_3723,
		obj_t BgL_oz00_3724, obj_t BgL_vz00_3725)
	{
		{	/* Ast/var.scm 23 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_3724)))->BgL_typez00) =
				((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_3725)), BUNSPEC);
		}

	}



/* &lambda1486 */
	BgL_typez00_bglt BGl_z62lambda1486z62zzast_varz00(obj_t BgL_envz00_3726,
		obj_t BgL_oz00_3727)
	{
		{	/* Ast/var.scm 23 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_3727)))->BgL_typez00);
		}

	}



/* &<@anonymous:1456> */
	obj_t BGl_z62zc3z04anonymousza31456ze3ze5zzast_varz00(obj_t BgL_envz00_3728)
	{
		{	/* Ast/var.scm 23 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1455 */
	obj_t BGl_z62lambda1455z62zzast_varz00(obj_t BgL_envz00_3729,
		obj_t BgL_oz00_3730, obj_t BgL_vz00_3731)
	{
		{	/* Ast/var.scm 23 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_3730)))->BgL_namez00) =
				((obj_t) BgL_vz00_3731), BUNSPEC);
		}

	}



/* &lambda1454 */
	obj_t BGl_z62lambda1454z62zzast_varz00(obj_t BgL_envz00_3732,
		obj_t BgL_oz00_3733)
	{
		{	/* Ast/var.scm 23 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_3733)))->BgL_namez00);
		}

	}



/* &lambda1423 */
	obj_t BGl_z62lambda1423z62zzast_varz00(obj_t BgL_envz00_3734,
		obj_t BgL_oz00_3735, obj_t BgL_vz00_3736)
	{
		{	/* Ast/var.scm 23 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_3735)))->BgL_idz00) = ((obj_t)
					((obj_t) BgL_vz00_3736)), BUNSPEC);
		}

	}



/* &lambda1422 */
	obj_t BGl_z62lambda1422z62zzast_varz00(obj_t BgL_envz00_3737,
		obj_t BgL_oz00_3738)
	{
		{	/* Ast/var.scm 23 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_3738)))->BgL_idz00);
		}

	}



/* &<@anonymous:1365> */
	obj_t BGl_z62zc3z04anonymousza31365ze3ze5zzast_varz00(obj_t BgL_envz00_3739,
		obj_t BgL_new1050z00_3740)
	{
		{	/* Ast/var.scm 21 */
			return ((obj_t) ((BgL_valuez00_bglt) BgL_new1050z00_3740));
		}

	}



/* &lambda1362 */
	BgL_valuez00_bglt BGl_z62lambda1362z62zzast_varz00(obj_t BgL_envz00_3741)
	{
		{	/* Ast/var.scm 21 */
			{	/* Ast/var.scm 21 */
				BgL_valuez00_bglt BgL_new1049z00_4221;

				BgL_new1049z00_4221 =
					((BgL_valuez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_valuez00_bgl))));
				{	/* Ast/var.scm 21 */
					long BgL_arg1364z00_4222;

					BgL_arg1364z00_4222 = BGL_CLASS_NUM(BGl_valuez00zzast_varz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1049z00_4221), BgL_arg1364z00_4222);
				}
				return BgL_new1049z00_4221;
			}
		}

	}



/* &lambda1353 */
	BgL_valuez00_bglt BGl_z62lambda1353z62zzast_varz00(obj_t BgL_envz00_3742)
	{
		{	/* Ast/var.scm 21 */
			{	/* Ast/var.scm 21 */
				BgL_valuez00_bglt BgL_new1222z00_4223;

				{	/* Ast/var.scm 21 */
					BgL_valuez00_bglt BgL_new1221z00_4224;

					BgL_new1221z00_4224 =
						((BgL_valuez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_valuez00_bgl))));
					{	/* Ast/var.scm 21 */
						long BgL_arg1361z00_4225;

						BgL_arg1361z00_4225 = BGL_CLASS_NUM(BGl_valuez00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1221z00_4224), BgL_arg1361z00_4225);
					}
					BgL_new1222z00_4223 = BgL_new1221z00_4224;
				}
				return BgL_new1222z00_4223;
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_varz00(void)
	{
		{	/* Ast/var.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_varz00(void)
	{
		{	/* Ast/var.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_varz00(void)
	{
		{	/* Ast/var.scm 14 */
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2373z00zzast_varz00));
			return
				BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2373z00zzast_varz00));
		}

	}

#ifdef __cplusplus
}
#endif
