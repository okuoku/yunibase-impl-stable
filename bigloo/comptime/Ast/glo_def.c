/*===========================================================================*/
/*   (Ast/glo_def.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/glo_def.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_AST_GLOzd2DEFzd2_TYPE_DEFINITIONS
#define BGL_BgL_AST_GLOzd2DEFzd2_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;


#endif													// BGL_BgL_AST_GLOzd2DEFzd2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_defzd2globalzd2sfunzd2nozd2warningz12z12zzast_glozd2defzd2(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_defzd2globalzd2svarz12z12zzast_glozd2defzd2(obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT int BGl_bigloozd2warningzd2zz__paramz00(void);
	static obj_t BGl_requirezd2initializa7ationz75zzast_glozd2defzd2 = BUNSPEC;
	extern obj_t BGl_dssslzd2keyszd2zztools_dssslz00(obj_t);
	BGL_IMPORT bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_tclassz00zzobject_classz00;
	extern obj_t BGl_sfunz00zzast_varz00;
	extern obj_t BGl_dssslzd2formalszd2zztools_dssslz00(obj_t);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzast_glozd2defzd2(void);
	extern obj_t BGl_leavezd2functionzd2zztools_errorz00(void);
	static obj_t BGl_objectzd2initzd2zzast_glozd2defzd2(void);
	static BgL_globalz00_bglt
		BGl_z62defzd2globalzd2svarz12z70zzast_glozd2defzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern BgL_globalz00_bglt
		BGl_declarezd2globalzd2scnstz12z12zzast_glozd2declzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	static BgL_globalz00_bglt
		BGl_z62defzd2globalzd2sfunzd2nozd2warningz12z70zzast_glozd2defzd2(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	extern bool_t BGl_dssslzd2keyzd2onlyzd2prototypezf3z21zztools_dssslz00(obj_t);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static BgL_globalz00_bglt
		BGl_checkzd2sfunzd2definitionz00zzast_glozd2defzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern BgL_globalz00_bglt
		BGl_declarezd2globalzd2sfunz12z12zzast_glozd2declzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzast_glozd2defzd2(void);
	BGL_IMPORT obj_t BGl_bigloozd2warningzd2setz12z12zz__paramz00(int);
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	static obj_t BGl_z62checkzd2methodzd2definitionz62zzast_glozd2defzd2(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_za2bdbzd2debugza2zd2zzengine_paramz00;
	extern obj_t BGl_removezd2varzd2fromz12z12zzast_removez00(obj_t,
		BgL_variablez00_bglt);
	extern obj_t BGl_idzd2ofzd2idz00zzast_identz00(obj_t, obj_t);
	static BgL_globalz00_bglt
		BGl_mismatchzd2errorzd2zzast_glozd2defzd2(BgL_globalz00_bglt, obj_t, obj_t);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	extern obj_t BGl_svarz00zzast_varz00;
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_defzd2globalzd2scnstz12z12zzast_glozd2defzd2(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	extern bool_t
		BGl_dssslzd2optionalzd2onlyzd2prototypezf3z21zztools_dssslz00(obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzast_glozd2defzd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_dssslz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_argsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_removez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_glozd2declzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	extern bool_t BGl_typezd2subclasszf3z21zzobject_classz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_enterzd2functionzd2zztools_errorz00(obj_t);
	extern BgL_typez00_bglt BGl_typezd2ofzd2idz00zzast_identz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_defzd2globalzd2sfunz12z12zzast_glozd2defzd2(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_localz00zzast_varz00;
	static obj_t BGl_cnstzd2initzd2zzast_glozd2defzd2(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_glozd2defzd2(void);
	static BgL_globalz00_bglt
		BGl_checkzd2svarzd2definitionz00zzast_glozd2defzd2(obj_t, obj_t, obj_t);
	BGL_IMPORT long bgl_list_length(obj_t);
	extern long BGl_globalzd2arityzd2zztools_argsz00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzast_glozd2defzd2(void);
	extern obj_t BGl_dssslzd2optionalszd2zztools_dssslz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzast_glozd2defzd2(void);
	extern obj_t BGl_parsezd2idzd2zzast_identz00(obj_t, obj_t);
	extern BgL_globalz00_bglt
		BGl_declarezd2globalzd2svarz12z12zzast_glozd2declzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_findzd2globalzf2modulez20zzast_envz00(obj_t, obj_t);
	static bool_t BGl_compatiblezd2typezf3z21zzast_glozd2defzd2(bool_t,
		BgL_typez00_bglt, BgL_typez00_bglt);
	static BgL_globalz00_bglt
		BGl_z62defzd2globalzd2sfunz12z70zzast_glozd2defzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_globalz00_bglt
		BGl_z62defzd2globalzd2scnstz12z70zzast_glozd2defzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_checkzd2methodzd2definitionz00zzast_glozd2defzd2(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	extern bool_t BGl_dssslzd2prototypezf3z21zztools_dssslz00(obj_t);
	static obj_t __cnst[6];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_checkzd2methodzd2definitionzd2envzd2zzast_glozd2defzd2,
		BgL_bgl_za762checkza7d2metho1905z00,
		BGl_z62checkzd2methodzd2definitionz62zzast_glozd2defzd2, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_defzd2globalzd2scnstz12zd2envzc0zzast_glozd2defzd2,
		BgL_bgl_za762defza7d2globalza71906za7,
		BGl_z62defzd2globalzd2scnstz12z70zzast_glozd2defzd2, 0L, BUNSPEC, 5);
	      DEFINE_STRING(BGl_string1900z00zzast_glozd2defzd2,
		BgL_bgl_string1900za700za7za7a1907za7, "argument missing", 16);
	      DEFINE_STRING(BGl_string1901z00zzast_glozd2defzd2,
		BgL_bgl_string1901za700za7za7a1908za7, "(generic not found for method)",
		30);
	      DEFINE_STRING(BGl_string1902z00zzast_glozd2defzd2,
		BgL_bgl_string1902za700za7za7a1909za7, "ast_glo-def", 11);
	      DEFINE_STRING(BGl_string1903z00zzast_glozd2defzd2,
		BgL_bgl_string1903za700za7za7a1910za7,
		"bigloo now sgfun static export bdb ", 35);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_defzd2globalzd2sfunzd2nozd2warningz12zd2envzc0zzast_glozd2defzd2,
		BgL_bgl_za762defza7d2globalza71911za7,
		BGl_z62defzd2globalzd2sfunzd2nozd2warningz12z70zzast_glozd2defzd2, 0L,
		BUNSPEC, 8);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_defzd2globalzd2sfunz12zd2envzc0zzast_glozd2defzd2,
		BgL_bgl_za762defza7d2globalza71912za7,
		BGl_z62defzd2globalzd2sfunz12z70zzast_glozd2defzd2, 0L, BUNSPEC, 8);
	      DEFINE_STRING(BGl_string1885z00zzast_glozd2defzd2,
		BgL_bgl_string1885za700za7za7a1913za7, "check-method-definition", 23);
	      DEFINE_STRING(BGl_string1886z00zzast_glozd2defzd2,
		BgL_bgl_string1886za700za7za7a1914za7, "unexpected generic arg", 22);
	      DEFINE_STRING(BGl_string1887z00zzast_glozd2defzd2,
		BgL_bgl_string1887za700za7za7a1915za7,
		"(incompatible DSSSL #!optional prototype)", 41);
	      DEFINE_STRING(BGl_string1888z00zzast_glozd2defzd2,
		BgL_bgl_string1888za700za7za7a1916za7,
		"(incompatible DSSSL #!key prototype)", 36);
	      DEFINE_STRING(BGl_string1889z00zzast_glozd2defzd2,
		BgL_bgl_string1889za700za7za7a1917za7, "(incompatible Dsssl prototype)",
		30);
	      DEFINE_STRING(BGl_string1890z00zzast_glozd2defzd2,
		BgL_bgl_string1890za700za7za7a1918za7, "(arity differs)", 15);
	      DEFINE_STRING(BGl_string1891z00zzast_glozd2defzd2,
		BgL_bgl_string1891za700za7za7a1919za7, "(incompatible formal type)", 26);
	      DEFINE_STRING(BGl_string1892z00zzast_glozd2defzd2,
		BgL_bgl_string1892za700za7za7a1920za7,
		"(incompatible function type result)", 35);
	      DEFINE_STRING(BGl_string1893z00zzast_glozd2defzd2,
		BgL_bgl_string1893za700za7za7a1921za7,
		"(declared as function of another class (~a/~a))", 47);
	      DEFINE_STRING(BGl_string1894z00zzast_glozd2defzd2,
		BgL_bgl_string1894za700za7za7a1922za7, "(not declared as function)", 26);
	      DEFINE_STRING(BGl_string1895z00zzast_glozd2defzd2,
		BgL_bgl_string1895za700za7za7a1923za7, "Illegal type for global variable",
		32);
	      DEFINE_STRING(BGl_string1896z00zzast_glozd2defzd2,
		BgL_bgl_string1896za700za7za7a1924za7, "(incompatible variable type)", 28);
	      DEFINE_STRING(BGl_string1897z00zzast_glozd2defzd2,
		BgL_bgl_string1897za700za7za7a1925za7, "(not declared as a variable)", 28);
	      DEFINE_STRING(BGl_string1898z00zzast_glozd2defzd2,
		BgL_bgl_string1898za700za7za7a1926za7,
		"Prototype and definition don't match", 36);
	      DEFINE_STRING(BGl_string1899z00zzast_glozd2defzd2,
		BgL_bgl_string1899za700za7za7a1927za7, " ", 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_defzd2globalzd2svarz12zd2envzc0zzast_glozd2defzd2,
		BgL_bgl_za762defza7d2globalza71928za7,
		BGl_z62defzd2globalzd2svarz12z70zzast_glozd2defzd2, 0L, BUNSPEC, 4);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzast_glozd2defzd2));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzast_glozd2defzd2(long
		BgL_checksumz00_3022, char *BgL_fromz00_3023)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_glozd2defzd2))
				{
					BGl_requirezd2initializa7ationz75zzast_glozd2defzd2 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_glozd2defzd2();
					BGl_libraryzd2moduleszd2initz00zzast_glozd2defzd2();
					BGl_cnstzd2initzd2zzast_glozd2defzd2();
					BGl_importedzd2moduleszd2initz00zzast_glozd2defzd2();
					return BGl_methodzd2initzd2zzast_glozd2defzd2();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_glozd2defzd2(void)
	{
		{	/* Ast/glo_def.scm 15 */
			BGl_modulezd2initializa7ationz75zz__paramz00(0L, "ast_glo-def");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"ast_glo-def");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "ast_glo-def");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"ast_glo-def");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_glo-def");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_glo-def");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "ast_glo-def");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "ast_glo-def");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "ast_glo-def");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"ast_glo-def");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_glozd2defzd2(void)
	{
		{	/* Ast/glo_def.scm 15 */
			{	/* Ast/glo_def.scm 15 */
				obj_t BgL_cportz00_3011;

				{	/* Ast/glo_def.scm 15 */
					obj_t BgL_stringz00_3018;

					BgL_stringz00_3018 = BGl_string1903z00zzast_glozd2defzd2;
					{	/* Ast/glo_def.scm 15 */
						obj_t BgL_startz00_3019;

						BgL_startz00_3019 = BINT(0L);
						{	/* Ast/glo_def.scm 15 */
							obj_t BgL_endz00_3020;

							BgL_endz00_3020 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3018)));
							{	/* Ast/glo_def.scm 15 */

								BgL_cportz00_3011 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3018, BgL_startz00_3019, BgL_endz00_3020);
				}}}}
				{
					long BgL_iz00_3012;

					BgL_iz00_3012 = 5L;
				BgL_loopz00_3013:
					if ((BgL_iz00_3012 == -1L))
						{	/* Ast/glo_def.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Ast/glo_def.scm 15 */
							{	/* Ast/glo_def.scm 15 */
								obj_t BgL_arg1904z00_3014;

								{	/* Ast/glo_def.scm 15 */

									{	/* Ast/glo_def.scm 15 */
										obj_t BgL_locationz00_3016;

										BgL_locationz00_3016 = BBOOL(((bool_t) 0));
										{	/* Ast/glo_def.scm 15 */

											BgL_arg1904z00_3014 =
												BGl_readz00zz__readerz00(BgL_cportz00_3011,
												BgL_locationz00_3016);
										}
									}
								}
								{	/* Ast/glo_def.scm 15 */
									int BgL_tmpz00_3051;

									BgL_tmpz00_3051 = (int) (BgL_iz00_3012);
									CNST_TABLE_SET(BgL_tmpz00_3051, BgL_arg1904z00_3014);
							}}
							{	/* Ast/glo_def.scm 15 */
								int BgL_auxz00_3017;

								BgL_auxz00_3017 = (int) ((BgL_iz00_3012 - 1L));
								{
									long BgL_iz00_3056;

									BgL_iz00_3056 = (long) (BgL_auxz00_3017);
									BgL_iz00_3012 = BgL_iz00_3056;
									goto BgL_loopz00_3013;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_glozd2defzd2(void)
	{
		{	/* Ast/glo_def.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* def-global-sfun-no-warning! */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_defzd2globalzd2sfunzd2nozd2warningz12z12zzast_glozd2defzd2(obj_t
		BgL_idz00_3, obj_t BgL_argsz00_4, obj_t BgL_locz00_5, obj_t BgL_modz00_6,
		obj_t BgL_classz00_7, obj_t BgL_srczd2expzd2_8, obj_t BgL_remz00_9,
		obj_t BgL_nodez00_10)
	{
		{	/* Ast/glo_def.scm 67 */
			{	/* Ast/glo_def.scm 68 */
				int BgL_warningz00_2157;

				BgL_warningz00_2157 = BGl_bigloozd2warningzd2zz__paramz00();
				BGl_bigloozd2warningzd2setz12z12zz__paramz00((int) (0L));
				{	/* Ast/glo_def.scm 70 */
					BgL_globalz00_bglt BgL_funz00_2158;

					BgL_funz00_2158 =
						BGl_defzd2globalzd2sfunz12z12zzast_glozd2defzd2(BgL_idz00_3,
						BgL_argsz00_4, BgL_locz00_5, BgL_modz00_6, BgL_classz00_7,
						BgL_srczd2expzd2_8, BgL_remz00_9, BgL_nodez00_10);
					BGl_bigloozd2warningzd2setz12z12zz__paramz00(BgL_warningz00_2157);
					return BgL_funz00_2158;
				}
			}
		}

	}



/* &def-global-sfun-no-warning! */
	BgL_globalz00_bglt
		BGl_z62defzd2globalzd2sfunzd2nozd2warningz12z70zzast_glozd2defzd2(obj_t
		BgL_envz00_2977, obj_t BgL_idz00_2978, obj_t BgL_argsz00_2979,
		obj_t BgL_locz00_2980, obj_t BgL_modz00_2981, obj_t BgL_classz00_2982,
		obj_t BgL_srczd2expzd2_2983, obj_t BgL_remz00_2984, obj_t BgL_nodez00_2985)
	{
		{	/* Ast/glo_def.scm 67 */
			return
				BGl_defzd2globalzd2sfunzd2nozd2warningz12z12zzast_glozd2defzd2
				(BgL_idz00_2978, BgL_argsz00_2979, BgL_locz00_2980, BgL_modz00_2981,
				BgL_classz00_2982, BgL_srczd2expzd2_2983, BgL_remz00_2984,
				BgL_nodez00_2985);
		}

	}



/* def-global-sfun! */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_defzd2globalzd2sfunz12z12zzast_glozd2defzd2(obj_t BgL_idz00_11,
		obj_t BgL_argsz00_12, obj_t BgL_localsz00_13, obj_t BgL_modulez00_14,
		obj_t BgL_classz00_15, obj_t BgL_srczd2expzd2_16, obj_t BgL_remz00_17,
		obj_t BgL_nodez00_18)
	{
		{	/* Ast/glo_def.scm 80 */
			BGl_enterzd2functionzd2zztools_errorz00(BgL_idz00_11);
			{	/* Ast/glo_def.scm 86 */
				obj_t BgL_locz00_1722;

				BgL_locz00_1722 =
					BGl_findzd2locationzd2zztools_locationz00(BgL_srczd2expzd2_16);
				{	/* Ast/glo_def.scm 86 */
					obj_t BgL_idzd2typezd2_1723;

					BgL_idzd2typezd2_1723 =
						BGl_parsezd2idzd2zzast_identz00(BgL_idz00_11, BgL_locz00_1722);
					{	/* Ast/glo_def.scm 87 */
						obj_t BgL_typezd2reszd2_1724;

						BgL_typezd2reszd2_1724 = CDR(BgL_idzd2typezd2_1723);
						{	/* Ast/glo_def.scm 88 */
							obj_t BgL_idz00_1725;

							BgL_idz00_1725 = CAR(BgL_idzd2typezd2_1723);
							{	/* Ast/glo_def.scm 89 */
								obj_t BgL_importz00_1726;

								{	/* Ast/glo_def.scm 90 */
									bool_t BgL_test1931z00_3070;

									if (
										((long) CINT(BGl_za2bdbzd2debugza2zd2zzengine_paramz00) >=
											3L))
										{	/* Ast/glo_def.scm 91 */
											obj_t BgL_arg1348z00_1779;

											{	/* Ast/glo_def.scm 91 */
												obj_t BgL_arg1349z00_1780;

												BgL_arg1349z00_1780 =
													BGl_thezd2backendzd2zzbackend_backendz00();
												BgL_arg1348z00_1779 =
													(((BgL_backendz00_bglt) COBJECT(
															((BgL_backendz00_bglt) BgL_arg1349z00_1780)))->
													BgL_debugzd2supportzd2);
											}
											BgL_test1931z00_3070 =
												CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
												(CNST_TABLE_REF(0), BgL_arg1348z00_1779));
										}
									else
										{	/* Ast/glo_def.scm 90 */
											BgL_test1931z00_3070 = ((bool_t) 0);
										}
									if (BgL_test1931z00_3070)
										{	/* Ast/glo_def.scm 90 */
											BgL_importz00_1726 = CNST_TABLE_REF(1);
										}
									else
										{	/* Ast/glo_def.scm 90 */
											BgL_importz00_1726 = CNST_TABLE_REF(2);
										}
								}
								{	/* Ast/glo_def.scm 90 */
									obj_t BgL_oldzd2globalzd2_1727;

									BgL_oldzd2globalzd2_1727 =
										BGl_findzd2globalzf2modulez20zzast_envz00(BgL_idz00_1725,
										BgL_modulez00_14);
									{	/* Ast/glo_def.scm 94 */
										BgL_globalz00_bglt BgL_globalz00_1728;

										{	/* Ast/glo_def.scm 96 */
											bool_t BgL_test1933z00_3083;

											{	/* Ast/glo_def.scm 96 */
												obj_t BgL_classz00_2163;

												BgL_classz00_2163 = BGl_globalz00zzast_varz00;
												if (BGL_OBJECTP(BgL_oldzd2globalzd2_1727))
													{	/* Ast/glo_def.scm 96 */
														BgL_objectz00_bglt BgL_arg1807z00_2165;

														BgL_arg1807z00_2165 =
															(BgL_objectz00_bglt) (BgL_oldzd2globalzd2_1727);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Ast/glo_def.scm 96 */
																long BgL_idxz00_2171;

																BgL_idxz00_2171 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_2165);
																BgL_test1933z00_3083 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_2171 + 2L)) ==
																	BgL_classz00_2163);
															}
														else
															{	/* Ast/glo_def.scm 96 */
																bool_t BgL_res1864z00_2196;

																{	/* Ast/glo_def.scm 96 */
																	obj_t BgL_oclassz00_2179;

																	{	/* Ast/glo_def.scm 96 */
																		obj_t BgL_arg1815z00_2187;
																		long BgL_arg1816z00_2188;

																		BgL_arg1815z00_2187 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Ast/glo_def.scm 96 */
																			long BgL_arg1817z00_2189;

																			BgL_arg1817z00_2189 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_2165);
																			BgL_arg1816z00_2188 =
																				(BgL_arg1817z00_2189 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_2179 =
																			VECTOR_REF(BgL_arg1815z00_2187,
																			BgL_arg1816z00_2188);
																	}
																	{	/* Ast/glo_def.scm 96 */
																		bool_t BgL__ortest_1115z00_2180;

																		BgL__ortest_1115z00_2180 =
																			(BgL_classz00_2163 == BgL_oclassz00_2179);
																		if (BgL__ortest_1115z00_2180)
																			{	/* Ast/glo_def.scm 96 */
																				BgL_res1864z00_2196 =
																					BgL__ortest_1115z00_2180;
																			}
																		else
																			{	/* Ast/glo_def.scm 96 */
																				long BgL_odepthz00_2181;

																				{	/* Ast/glo_def.scm 96 */
																					obj_t BgL_arg1804z00_2182;

																					BgL_arg1804z00_2182 =
																						(BgL_oclassz00_2179);
																					BgL_odepthz00_2181 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_2182);
																				}
																				if ((2L < BgL_odepthz00_2181))
																					{	/* Ast/glo_def.scm 96 */
																						obj_t BgL_arg1802z00_2184;

																						{	/* Ast/glo_def.scm 96 */
																							obj_t BgL_arg1803z00_2185;

																							BgL_arg1803z00_2185 =
																								(BgL_oclassz00_2179);
																							BgL_arg1802z00_2184 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_2185, 2L);
																						}
																						BgL_res1864z00_2196 =
																							(BgL_arg1802z00_2184 ==
																							BgL_classz00_2163);
																					}
																				else
																					{	/* Ast/glo_def.scm 96 */
																						BgL_res1864z00_2196 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test1933z00_3083 = BgL_res1864z00_2196;
															}
													}
												else
													{	/* Ast/glo_def.scm 96 */
														BgL_test1933z00_3083 = ((bool_t) 0);
													}
											}
											if (BgL_test1933z00_3083)
												{	/* Ast/glo_def.scm 96 */
													BgL_globalz00_1728 =
														BGl_checkzd2sfunzd2definitionz00zzast_glozd2defzd2
														(BgL_oldzd2globalzd2_1727, BgL_typezd2reszd2_1724,
														BgL_argsz00_12, BgL_localsz00_13, BgL_classz00_15,
														BgL_srczd2expzd2_16);
												}
											else
												{	/* Ast/glo_def.scm 96 */
													BgL_globalz00_1728 =
														BGl_declarezd2globalzd2sfunz12z12zzast_glozd2declzd2
														(BgL_idz00_1725, BFALSE, BgL_argsz00_12,
														BgL_modulez00_14, BgL_importz00_1726,
														BgL_classz00_15, BgL_srczd2expzd2_16, BFALSE);
												}
										}
										{	/* Ast/glo_def.scm 95 */
											obj_t BgL_defzd2loczd2_1729;

											BgL_defzd2loczd2_1729 =
												BGl_findzd2locationzd2zztools_locationz00
												(BgL_srczd2expzd2_16);
											{	/* Ast/glo_def.scm 102 */

												{	/* Ast/glo_def.scm 103 */
													bool_t BgL_test1938z00_3109;

													{	/* Ast/glo_def.scm 103 */
														BgL_valuez00_bglt BgL_arg1339z00_1772;

														BgL_arg1339z00_1772 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		BgL_globalz00_1728)))->BgL_valuez00);
														{	/* Ast/glo_def.scm 103 */
															obj_t BgL_classz00_2198;

															BgL_classz00_2198 = BGl_sfunz00zzast_varz00;
															{	/* Ast/glo_def.scm 103 */
																BgL_objectz00_bglt BgL_arg1807z00_2200;

																{	/* Ast/glo_def.scm 103 */
																	obj_t BgL_tmpz00_3112;

																	BgL_tmpz00_3112 =
																		((obj_t)
																		((BgL_objectz00_bglt) BgL_arg1339z00_1772));
																	BgL_arg1807z00_2200 =
																		(BgL_objectz00_bglt) (BgL_tmpz00_3112);
																}
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Ast/glo_def.scm 103 */
																		long BgL_idxz00_2206;

																		BgL_idxz00_2206 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_2200);
																		BgL_test1938z00_3109 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_2206 + 3L)) ==
																			BgL_classz00_2198);
																	}
																else
																	{	/* Ast/glo_def.scm 103 */
																		bool_t BgL_res1865z00_2231;

																		{	/* Ast/glo_def.scm 103 */
																			obj_t BgL_oclassz00_2214;

																			{	/* Ast/glo_def.scm 103 */
																				obj_t BgL_arg1815z00_2222;
																				long BgL_arg1816z00_2223;

																				BgL_arg1815z00_2222 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Ast/glo_def.scm 103 */
																					long BgL_arg1817z00_2224;

																					BgL_arg1817z00_2224 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_2200);
																					BgL_arg1816z00_2223 =
																						(BgL_arg1817z00_2224 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_2214 =
																					VECTOR_REF(BgL_arg1815z00_2222,
																					BgL_arg1816z00_2223);
																			}
																			{	/* Ast/glo_def.scm 103 */
																				bool_t BgL__ortest_1115z00_2215;

																				BgL__ortest_1115z00_2215 =
																					(BgL_classz00_2198 ==
																					BgL_oclassz00_2214);
																				if (BgL__ortest_1115z00_2215)
																					{	/* Ast/glo_def.scm 103 */
																						BgL_res1865z00_2231 =
																							BgL__ortest_1115z00_2215;
																					}
																				else
																					{	/* Ast/glo_def.scm 103 */
																						long BgL_odepthz00_2216;

																						{	/* Ast/glo_def.scm 103 */
																							obj_t BgL_arg1804z00_2217;

																							BgL_arg1804z00_2217 =
																								(BgL_oclassz00_2214);
																							BgL_odepthz00_2216 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_2217);
																						}
																						if ((3L < BgL_odepthz00_2216))
																							{	/* Ast/glo_def.scm 103 */
																								obj_t BgL_arg1802z00_2219;

																								{	/* Ast/glo_def.scm 103 */
																									obj_t BgL_arg1803z00_2220;

																									BgL_arg1803z00_2220 =
																										(BgL_oclassz00_2214);
																									BgL_arg1802z00_2219 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_2220, 3L);
																								}
																								BgL_res1865z00_2231 =
																									(BgL_arg1802z00_2219 ==
																									BgL_classz00_2198);
																							}
																						else
																							{	/* Ast/glo_def.scm 103 */
																								BgL_res1865z00_2231 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test1938z00_3109 = BgL_res1865z00_2231;
																	}
															}
														}
													}
													if (BgL_test1938z00_3109)
														{	/* Ast/glo_def.scm 103 */
															{	/* Ast/glo_def.scm 300 */
																BgL_typez00_bglt BgL_oldzd2typezd2_2234;

																BgL_oldzd2typezd2_2234 =
																	(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				BgL_globalz00_1728)))->BgL_typez00);
																if ((((obj_t) BgL_oldzd2typezd2_2234) ==
																		BGl_za2_za2z00zztype_cachez00))
																	{	/* Ast/glo_def.scm 301 */
																		((((BgL_variablez00_bglt) COBJECT(
																						((BgL_variablez00_bglt)
																							BgL_globalz00_1728)))->
																				BgL_typez00) =
																			((BgL_typez00_bglt) ((BgL_typez00_bglt)
																					BgL_typezd2reszd2_1724)), BUNSPEC);
																	}
																else
																	{	/* Ast/glo_def.scm 301 */
																		BFALSE;
																	}
															}
															if (
																(bgl_list_length(BgL_localsz00_13) ==
																	bgl_list_length(
																		(((BgL_sfunz00_bglt) COBJECT(
																					((BgL_sfunz00_bglt)
																						(((BgL_variablez00_bglt) COBJECT(
																									((BgL_variablez00_bglt)
																										BgL_globalz00_1728)))->
																							BgL_valuez00))))->BgL_argsz00))))
																{	/* Ast/glo_def.scm 114 */
																	obj_t BgL_typesz00_1737;

																	{	/* Ast/glo_def.scm 114 */
																		obj_t BgL_l1247z00_1748;

																		BgL_l1247z00_1748 =
																			(((BgL_sfunz00_bglt) COBJECT(
																					((BgL_sfunz00_bglt)
																						(((BgL_variablez00_bglt) COBJECT(
																									((BgL_variablez00_bglt)
																										BgL_globalz00_1728)))->
																							BgL_valuez00))))->BgL_argsz00);
																		if (NULLP(BgL_l1247z00_1748))
																			{	/* Ast/glo_def.scm 114 */
																				BgL_typesz00_1737 = BNIL;
																			}
																		else
																			{	/* Ast/glo_def.scm 114 */
																				obj_t BgL_head1249z00_1750;

																				BgL_head1249z00_1750 =
																					MAKE_YOUNG_PAIR(BNIL, BNIL);
																				{
																					obj_t BgL_l1247z00_1752;
																					obj_t BgL_tail1250z00_1753;

																					BgL_l1247z00_1752 = BgL_l1247z00_1748;
																					BgL_tail1250z00_1753 =
																						BgL_head1249z00_1750;
																				BgL_zc3z04anonymousza31319ze3z87_1754:
																					if (NULLP(BgL_l1247z00_1752))
																						{	/* Ast/glo_def.scm 114 */
																							BgL_typesz00_1737 =
																								CDR(BgL_head1249z00_1750);
																						}
																					else
																						{	/* Ast/glo_def.scm 114 */
																							obj_t BgL_newtail1251z00_1756;

																							{	/* Ast/glo_def.scm 114 */
																								obj_t BgL_arg1322z00_1758;

																								{	/* Ast/glo_def.scm 114 */
																									obj_t BgL_az00_1759;

																									BgL_az00_1759 =
																										CAR(
																										((obj_t)
																											BgL_l1247z00_1752));
																									{	/* Ast/glo_def.scm 116 */
																										bool_t BgL_test1946z00_3163;

																										{	/* Ast/glo_def.scm 116 */
																											obj_t BgL_classz00_2246;

																											BgL_classz00_2246 =
																												BGl_localz00zzast_varz00;
																											if (BGL_OBJECTP
																												(BgL_az00_1759))
																												{	/* Ast/glo_def.scm 116 */
																													BgL_objectz00_bglt
																														BgL_arg1807z00_2248;
																													BgL_arg1807z00_2248 =
																														(BgL_objectz00_bglt)
																														(BgL_az00_1759);
																													if (BGL_CONDEXPAND_ISA_ARCH64())
																														{	/* Ast/glo_def.scm 116 */
																															long
																																BgL_idxz00_2254;
																															BgL_idxz00_2254 =
																																BGL_OBJECT_INHERITANCE_NUM
																																(BgL_arg1807z00_2248);
																															BgL_test1946z00_3163
																																=
																																(VECTOR_REF
																																(BGl_za2inheritancesza2z00zz__objectz00,
																																	(BgL_idxz00_2254
																																		+ 2L)) ==
																																BgL_classz00_2246);
																														}
																													else
																														{	/* Ast/glo_def.scm 116 */
																															bool_t
																																BgL_res1866z00_2279;
																															{	/* Ast/glo_def.scm 116 */
																																obj_t
																																	BgL_oclassz00_2262;
																																{	/* Ast/glo_def.scm 116 */
																																	obj_t
																																		BgL_arg1815z00_2270;
																																	long
																																		BgL_arg1816z00_2271;
																																	BgL_arg1815z00_2270
																																		=
																																		(BGl_za2classesza2z00zz__objectz00);
																																	{	/* Ast/glo_def.scm 116 */
																																		long
																																			BgL_arg1817z00_2272;
																																		BgL_arg1817z00_2272
																																			=
																																			BGL_OBJECT_CLASS_NUM
																																			(BgL_arg1807z00_2248);
																																		BgL_arg1816z00_2271
																																			=
																																			(BgL_arg1817z00_2272
																																			-
																																			OBJECT_TYPE);
																																	}
																																	BgL_oclassz00_2262
																																		=
																																		VECTOR_REF
																																		(BgL_arg1815z00_2270,
																																		BgL_arg1816z00_2271);
																																}
																																{	/* Ast/glo_def.scm 116 */
																																	bool_t
																																		BgL__ortest_1115z00_2263;
																																	BgL__ortest_1115z00_2263
																																		=
																																		(BgL_classz00_2246
																																		==
																																		BgL_oclassz00_2262);
																																	if (BgL__ortest_1115z00_2263)
																																		{	/* Ast/glo_def.scm 116 */
																																			BgL_res1866z00_2279
																																				=
																																				BgL__ortest_1115z00_2263;
																																		}
																																	else
																																		{	/* Ast/glo_def.scm 116 */
																																			long
																																				BgL_odepthz00_2264;
																																			{	/* Ast/glo_def.scm 116 */
																																				obj_t
																																					BgL_arg1804z00_2265;
																																				BgL_arg1804z00_2265
																																					=
																																					(BgL_oclassz00_2262);
																																				BgL_odepthz00_2264
																																					=
																																					BGL_CLASS_DEPTH
																																					(BgL_arg1804z00_2265);
																																			}
																																			if (
																																				(2L <
																																					BgL_odepthz00_2264))
																																				{	/* Ast/glo_def.scm 116 */
																																					obj_t
																																						BgL_arg1802z00_2267;
																																					{	/* Ast/glo_def.scm 116 */
																																						obj_t
																																							BgL_arg1803z00_2268;
																																						BgL_arg1803z00_2268
																																							=
																																							(BgL_oclassz00_2262);
																																						BgL_arg1802z00_2267
																																							=
																																							BGL_CLASS_ANCESTORS_REF
																																							(BgL_arg1803z00_2268,
																																							2L);
																																					}
																																					BgL_res1866z00_2279
																																						=
																																						(BgL_arg1802z00_2267
																																						==
																																						BgL_classz00_2246);
																																				}
																																			else
																																				{	/* Ast/glo_def.scm 116 */
																																					BgL_res1866z00_2279
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																}
																															}
																															BgL_test1946z00_3163
																																=
																																BgL_res1866z00_2279;
																														}
																												}
																											else
																												{	/* Ast/glo_def.scm 116 */
																													BgL_test1946z00_3163 =
																														((bool_t) 0);
																												}
																										}
																										if (BgL_test1946z00_3163)
																											{	/* Ast/glo_def.scm 116 */
																												BgL_arg1322z00_1758 =
																													((obj_t)
																													(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt) BgL_az00_1759))))->BgL_typez00));
																											}
																										else
																											{	/* Ast/glo_def.scm 118 */
																												bool_t
																													BgL_test1951z00_3190;
																												{	/* Ast/glo_def.scm 118 */
																													obj_t
																														BgL_classz00_2281;
																													BgL_classz00_2281 =
																														BGl_typez00zztype_typez00;
																													if (BGL_OBJECTP
																														(BgL_az00_1759))
																														{	/* Ast/glo_def.scm 118 */
																															BgL_objectz00_bglt
																																BgL_arg1807z00_2283;
																															BgL_arg1807z00_2283
																																=
																																(BgL_objectz00_bglt)
																																(BgL_az00_1759);
																															if (BGL_CONDEXPAND_ISA_ARCH64())
																																{	/* Ast/glo_def.scm 118 */
																																	long
																																		BgL_idxz00_2289;
																																	BgL_idxz00_2289
																																		=
																																		BGL_OBJECT_INHERITANCE_NUM
																																		(BgL_arg1807z00_2283);
																																	BgL_test1951z00_3190
																																		=
																																		(VECTOR_REF
																																		(BGl_za2inheritancesza2z00zz__objectz00,
																																			(BgL_idxz00_2289
																																				+
																																				1L)) ==
																																		BgL_classz00_2281);
																																}
																															else
																																{	/* Ast/glo_def.scm 118 */
																																	bool_t
																																		BgL_res1867z00_2314;
																																	{	/* Ast/glo_def.scm 118 */
																																		obj_t
																																			BgL_oclassz00_2297;
																																		{	/* Ast/glo_def.scm 118 */
																																			obj_t
																																				BgL_arg1815z00_2305;
																																			long
																																				BgL_arg1816z00_2306;
																																			BgL_arg1815z00_2305
																																				=
																																				(BGl_za2classesza2z00zz__objectz00);
																																			{	/* Ast/glo_def.scm 118 */
																																				long
																																					BgL_arg1817z00_2307;
																																				BgL_arg1817z00_2307
																																					=
																																					BGL_OBJECT_CLASS_NUM
																																					(BgL_arg1807z00_2283);
																																				BgL_arg1816z00_2306
																																					=
																																					(BgL_arg1817z00_2307
																																					-
																																					OBJECT_TYPE);
																																			}
																																			BgL_oclassz00_2297
																																				=
																																				VECTOR_REF
																																				(BgL_arg1815z00_2305,
																																				BgL_arg1816z00_2306);
																																		}
																																		{	/* Ast/glo_def.scm 118 */
																																			bool_t
																																				BgL__ortest_1115z00_2298;
																																			BgL__ortest_1115z00_2298
																																				=
																																				(BgL_classz00_2281
																																				==
																																				BgL_oclassz00_2297);
																																			if (BgL__ortest_1115z00_2298)
																																				{	/* Ast/glo_def.scm 118 */
																																					BgL_res1867z00_2314
																																						=
																																						BgL__ortest_1115z00_2298;
																																				}
																																			else
																																				{	/* Ast/glo_def.scm 118 */
																																					long
																																						BgL_odepthz00_2299;
																																					{	/* Ast/glo_def.scm 118 */
																																						obj_t
																																							BgL_arg1804z00_2300;
																																						BgL_arg1804z00_2300
																																							=
																																							(BgL_oclassz00_2297);
																																						BgL_odepthz00_2299
																																							=
																																							BGL_CLASS_DEPTH
																																							(BgL_arg1804z00_2300);
																																					}
																																					if (
																																						(1L
																																							<
																																							BgL_odepthz00_2299))
																																						{	/* Ast/glo_def.scm 118 */
																																							obj_t
																																								BgL_arg1802z00_2302;
																																							{	/* Ast/glo_def.scm 118 */
																																								obj_t
																																									BgL_arg1803z00_2303;
																																								BgL_arg1803z00_2303
																																									=
																																									(BgL_oclassz00_2297);
																																								BgL_arg1802z00_2302
																																									=
																																									BGL_CLASS_ANCESTORS_REF
																																									(BgL_arg1803z00_2303,
																																									1L);
																																							}
																																							BgL_res1867z00_2314
																																								=
																																								(BgL_arg1802z00_2302
																																								==
																																								BgL_classz00_2281);
																																						}
																																					else
																																						{	/* Ast/glo_def.scm 118 */
																																							BgL_res1867z00_2314
																																								=
																																								(
																																								(bool_t)
																																								0);
																																						}
																																				}
																																		}
																																	}
																																	BgL_test1951z00_3190
																																		=
																																		BgL_res1867z00_2314;
																																}
																														}
																													else
																														{	/* Ast/glo_def.scm 118 */
																															BgL_test1951z00_3190
																																= ((bool_t) 0);
																														}
																												}
																												if (BgL_test1951z00_3190)
																													{	/* Ast/glo_def.scm 118 */
																														BgL_arg1322z00_1758
																															= BgL_az00_1759;
																													}
																												else
																													{	/* Ast/glo_def.scm 118 */
																														BgL_arg1322z00_1758
																															=
																															BGl_internalzd2errorzd2zztools_errorz00
																															(BGl_string1885z00zzast_glozd2defzd2,
																															BGl_string1886z00zzast_glozd2defzd2,
																															BGl_shapez00zztools_shapez00
																															(BgL_az00_1759));
																													}
																											}
																									}
																								}
																								BgL_newtail1251z00_1756 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1322z00_1758, BNIL);
																							}
																							SET_CDR(BgL_tail1250z00_1753,
																								BgL_newtail1251z00_1756);
																							{	/* Ast/glo_def.scm 114 */
																								obj_t BgL_arg1321z00_1757;

																								BgL_arg1321z00_1757 =
																									CDR(
																									((obj_t) BgL_l1247z00_1752));
																								{
																									obj_t BgL_tail1250z00_3220;
																									obj_t BgL_l1247z00_3219;

																									BgL_l1247z00_3219 =
																										BgL_arg1321z00_1757;
																									BgL_tail1250z00_3220 =
																										BgL_newtail1251z00_1756;
																									BgL_tail1250z00_1753 =
																										BgL_tail1250z00_3220;
																									BgL_l1247z00_1752 =
																										BgL_l1247z00_3219;
																									goto
																										BgL_zc3z04anonymousza31319ze3z87_1754;
																								}
																							}
																						}
																				}
																			}
																	}
																	{
																		obj_t BgL_ll1252z00_1739;
																		obj_t BgL_ll1253z00_1740;

																		BgL_ll1252z00_1739 = BgL_localsz00_13;
																		BgL_ll1253z00_1740 = BgL_typesz00_1737;
																	BgL_zc3z04anonymousza31312ze3z87_1741:
																		if (NULLP(BgL_ll1252z00_1739))
																			{	/* Ast/glo_def.scm 126 */
																				((bool_t) 1);
																			}
																		else
																			{	/* Ast/glo_def.scm 126 */
																				{	/* Ast/glo_def.scm 126 */
																					obj_t BgL_arg1314z00_1743;
																					obj_t BgL_arg1315z00_1744;

																					BgL_arg1314z00_1743 =
																						CAR(((obj_t) BgL_ll1252z00_1739));
																					BgL_arg1315z00_1744 =
																						CAR(((obj_t) BgL_ll1253z00_1740));
																					{	/* Ast/glo_def.scm 300 */
																						BgL_typez00_bglt
																							BgL_oldzd2typezd2_2321;
																						BgL_oldzd2typezd2_2321 =
																							(((BgL_variablez00_bglt)
																								COBJECT(((BgL_variablez00_bglt)
																										BgL_arg1314z00_1743)))->
																							BgL_typez00);
																						if ((((obj_t)
																									BgL_oldzd2typezd2_2321) ==
																								BGl_za2_za2z00zztype_cachez00))
																							{	/* Ast/glo_def.scm 301 */
																								((((BgL_variablez00_bglt)
																											COBJECT((
																													(BgL_variablez00_bglt)
																													BgL_arg1314z00_1743)))->
																										BgL_typez00) =
																									((BgL_typez00_bglt) (
																											(BgL_typez00_bglt)
																											BgL_arg1315z00_1744)),
																									BUNSPEC);
																							}
																						else
																							{	/* Ast/glo_def.scm 301 */
																								BFALSE;
																							}
																					}
																				}
																				{	/* Ast/glo_def.scm 126 */
																					obj_t BgL_arg1316z00_1745;
																					obj_t BgL_arg1317z00_1746;

																					BgL_arg1316z00_1745 =
																						CDR(((obj_t) BgL_ll1252z00_1739));
																					BgL_arg1317z00_1746 =
																						CDR(((obj_t) BgL_ll1253z00_1740));
																					{
																						obj_t BgL_ll1253z00_3240;
																						obj_t BgL_ll1252z00_3239;

																						BgL_ll1252z00_3239 =
																							BgL_arg1316z00_1745;
																						BgL_ll1253z00_3240 =
																							BgL_arg1317z00_1746;
																						BgL_ll1253z00_1740 =
																							BgL_ll1253z00_3240;
																						BgL_ll1252z00_1739 =
																							BgL_ll1252z00_3239;
																						goto
																							BgL_zc3z04anonymousza31312ze3z87_1741;
																					}
																				}
																			}
																	}
																}
															else
																{	/* Ast/glo_def.scm 112 */
																	((bool_t) 0);
																}
															BGl_removezd2varzd2fromz12z12zzast_removez00
																(BgL_remz00_17,
																((BgL_variablez00_bglt) BgL_globalz00_1728));
															{	/* Ast/glo_def.scm 130 */
																BgL_valuez00_bglt BgL_arg1332z00_1769;

																BgL_arg1332z00_1769 =
																	(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				BgL_globalz00_1728)))->BgL_valuez00);
																((((BgL_sfunz00_bglt)
																			COBJECT(((BgL_sfunz00_bglt)
																					BgL_arg1332z00_1769)))->BgL_bodyz00) =
																	((obj_t) BgL_nodez00_18), BUNSPEC);
															}
															{	/* Ast/glo_def.scm 132 */
																BgL_valuez00_bglt BgL_arg1333z00_1770;

																BgL_arg1333z00_1770 =
																	(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				BgL_globalz00_1728)))->BgL_valuez00);
																((((BgL_sfunz00_bglt)
																			COBJECT(((BgL_sfunz00_bglt)
																					BgL_arg1333z00_1770)))->BgL_argsz00) =
																	((obj_t) BgL_localsz00_13), BUNSPEC);
															}
															{	/* Ast/glo_def.scm 134 */
																BgL_valuez00_bglt BgL_arg1335z00_1771;

																BgL_arg1335z00_1771 =
																	(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				BgL_globalz00_1728)))->BgL_valuez00);
																((((BgL_sfunz00_bglt)
																			COBJECT(((BgL_sfunz00_bglt)
																					BgL_arg1335z00_1771)))->BgL_locz00) =
																	((obj_t) BgL_defzd2loczd2_1729), BUNSPEC);
															}
															BGl_leavezd2functionzd2zztools_errorz00();
														}
													else
														{	/* Ast/glo_def.scm 103 */
															BFALSE;
														}
												}
												return BgL_globalz00_1728;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &def-global-sfun! */
	BgL_globalz00_bglt BGl_z62defzd2globalzd2sfunz12z70zzast_glozd2defzd2(obj_t
		BgL_envz00_2986, obj_t BgL_idz00_2987, obj_t BgL_argsz00_2988,
		obj_t BgL_localsz00_2989, obj_t BgL_modulez00_2990, obj_t BgL_classz00_2991,
		obj_t BgL_srczd2expzd2_2992, obj_t BgL_remz00_2993, obj_t BgL_nodez00_2994)
	{
		{	/* Ast/glo_def.scm 80 */
			return
				BGl_defzd2globalzd2sfunz12z12zzast_glozd2defzd2(BgL_idz00_2987,
				BgL_argsz00_2988, BgL_localsz00_2989, BgL_modulez00_2990,
				BgL_classz00_2991, BgL_srczd2expzd2_2992, BgL_remz00_2993,
				BgL_nodez00_2994);
		}

	}



/* check-sfun-definition */
	BgL_globalz00_bglt BGl_checkzd2sfunzd2definitionz00zzast_glozd2defzd2(obj_t
		BgL_oldz00_19, obj_t BgL_typezd2reszd2_20, obj_t BgL_argsz00_21,
		obj_t BgL_localsz00_22, obj_t BgL_classz00_23, obj_t BgL_srczd2expzd2_24)
	{
		{	/* Ast/glo_def.scm 142 */
			{	/* Ast/glo_def.scm 145 */
				BgL_valuez00_bglt BgL_oldzd2valuezd2_1781;

				BgL_oldzd2valuezd2_1781 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt)
								((BgL_globalz00_bglt) BgL_oldz00_19))))->BgL_valuez00);
				{	/* Ast/glo_def.scm 147 */
					bool_t BgL_test1958z00_3260;

					{	/* Ast/glo_def.scm 147 */
						obj_t BgL_classz00_2334;

						BgL_classz00_2334 = BGl_sfunz00zzast_varz00;
						{	/* Ast/glo_def.scm 147 */
							BgL_objectz00_bglt BgL_arg1807z00_2336;

							{	/* Ast/glo_def.scm 147 */
								obj_t BgL_tmpz00_3261;

								BgL_tmpz00_3261 =
									((obj_t) ((BgL_objectz00_bglt) BgL_oldzd2valuezd2_1781));
								BgL_arg1807z00_2336 = (BgL_objectz00_bglt) (BgL_tmpz00_3261);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Ast/glo_def.scm 147 */
									long BgL_idxz00_2342;

									BgL_idxz00_2342 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2336);
									BgL_test1958z00_3260 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2342 + 3L)) == BgL_classz00_2334);
								}
							else
								{	/* Ast/glo_def.scm 147 */
									bool_t BgL_res1868z00_2367;

									{	/* Ast/glo_def.scm 147 */
										obj_t BgL_oclassz00_2350;

										{	/* Ast/glo_def.scm 147 */
											obj_t BgL_arg1815z00_2358;
											long BgL_arg1816z00_2359;

											BgL_arg1815z00_2358 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Ast/glo_def.scm 147 */
												long BgL_arg1817z00_2360;

												BgL_arg1817z00_2360 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2336);
												BgL_arg1816z00_2359 =
													(BgL_arg1817z00_2360 - OBJECT_TYPE);
											}
											BgL_oclassz00_2350 =
												VECTOR_REF(BgL_arg1815z00_2358, BgL_arg1816z00_2359);
										}
										{	/* Ast/glo_def.scm 147 */
											bool_t BgL__ortest_1115z00_2351;

											BgL__ortest_1115z00_2351 =
												(BgL_classz00_2334 == BgL_oclassz00_2350);
											if (BgL__ortest_1115z00_2351)
												{	/* Ast/glo_def.scm 147 */
													BgL_res1868z00_2367 = BgL__ortest_1115z00_2351;
												}
											else
												{	/* Ast/glo_def.scm 147 */
													long BgL_odepthz00_2352;

													{	/* Ast/glo_def.scm 147 */
														obj_t BgL_arg1804z00_2353;

														BgL_arg1804z00_2353 = (BgL_oclassz00_2350);
														BgL_odepthz00_2352 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2353);
													}
													if ((3L < BgL_odepthz00_2352))
														{	/* Ast/glo_def.scm 147 */
															obj_t BgL_arg1802z00_2355;

															{	/* Ast/glo_def.scm 147 */
																obj_t BgL_arg1803z00_2356;

																BgL_arg1803z00_2356 = (BgL_oclassz00_2350);
																BgL_arg1802z00_2355 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2356,
																	3L);
															}
															BgL_res1868z00_2367 =
																(BgL_arg1802z00_2355 == BgL_classz00_2334);
														}
													else
														{	/* Ast/glo_def.scm 147 */
															BgL_res1868z00_2367 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1958z00_3260 = BgL_res1868z00_2367;
								}
						}
					}
					if (BgL_test1958z00_3260)
						{	/* Ast/glo_def.scm 147 */
							if (
								((((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt) BgL_oldzd2valuezd2_1781)))->
										BgL_classz00) == BgL_classz00_23))
								{	/* Ast/glo_def.scm 154 */
									bool_t BgL_test1963z00_3288;

									{	/* Ast/glo_def.scm 154 */
										long BgL_arg1575z00_1858;
										long BgL_arg1576z00_1859;

										BgL_arg1575z00_1858 =
											(((BgL_funz00_bglt) COBJECT(
													((BgL_funz00_bglt)
														((BgL_sfunz00_bglt) BgL_oldzd2valuezd2_1781))))->
											BgL_arityz00);
										BgL_arg1576z00_1859 =
											BGl_globalzd2arityzd2zztools_argsz00(BgL_argsz00_21);
										BgL_test1963z00_3288 =
											(BgL_arg1575z00_1858 == BgL_arg1576z00_1859);
									}
									if (BgL_test1963z00_3288)
										{	/* Ast/glo_def.scm 156 */
											bool_t BgL_test1964z00_3294;

											{	/* Ast/glo_def.scm 156 */
												bool_t BgL_arg1571z00_1855;
												BgL_typez00_bglt BgL_arg1573z00_1856;

												BgL_arg1571z00_1855 =
													(CNST_TABLE_REF(3) == BgL_classz00_23);
												BgL_arg1573z00_1856 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_globalz00_bglt) BgL_oldz00_19))))->
													BgL_typez00);
												BgL_test1964z00_3294 =
													BGl_compatiblezd2typezf3z21zzast_glozd2defzd2
													(BgL_arg1571z00_1855,
													((BgL_typez00_bglt) BgL_typezd2reszd2_20),
													BgL_arg1573z00_1856);
											}
											if (BgL_test1964z00_3294)
												{	/* Ast/glo_def.scm 156 */
													if (BGl_dssslzd2prototypezf3z21zztools_dssslz00
														(BgL_argsz00_21))
														{	/* Ast/glo_def.scm 162 */
															if (BGl_dssslzd2optionalzd2onlyzd2prototypezf3z21zztools_dssslz00(BgL_argsz00_21))
																{	/* Ast/glo_def.scm 165 */
																	bool_t BgL_test1967z00_3306;

																	{	/* Ast/glo_def.scm 165 */
																		obj_t BgL_arg1408z00_1797;
																		obj_t BgL_arg1410z00_1798;

																		BgL_arg1408z00_1797 =
																			BGl_dssslzd2optionalszd2zztools_dssslz00
																			(BgL_argsz00_21);
																		BgL_arg1410z00_1798 =
																			(((BgL_sfunz00_bglt)
																				COBJECT(((BgL_sfunz00_bglt)
																						BgL_oldzd2valuezd2_1781)))->
																			BgL_optionalsz00);
																		BgL_test1967z00_3306 =
																			BGl_equalzf3zf3zz__r4_equivalence_6_2z00
																			(BgL_arg1408z00_1797,
																			BgL_arg1410z00_1798);
																	}
																	if (BgL_test1967z00_3306)
																		{	/* Ast/glo_def.scm 165 */
																			BgL_oldz00_19;
																		}
																	else
																		{	/* Ast/glo_def.scm 167 */
																			obj_t BgL_list1383z00_1796;

																			BgL_list1383z00_1796 =
																				MAKE_YOUNG_PAIR
																				(BGl_string1887z00zzast_glozd2defzd2,
																				BNIL);
																			((obj_t)
																				BGl_mismatchzd2errorzd2zzast_glozd2defzd2
																				(((BgL_globalz00_bglt) BgL_oldz00_19),
																					BgL_srczd2expzd2_24,
																					BgL_list1383z00_1796));
																		}
																}
															else
																{	/* Ast/glo_def.scm 164 */
																	if (BGl_dssslzd2keyzd2onlyzd2prototypezf3z21zztools_dssslz00(BgL_argsz00_21))
																		{	/* Ast/glo_def.scm 171 */
																			bool_t BgL_test1969z00_3317;

																			{	/* Ast/glo_def.scm 171 */
																				obj_t BgL_arg1434z00_1804;
																				obj_t BgL_arg1437z00_1805;

																				BgL_arg1434z00_1804 =
																					BGl_dssslzd2keyszd2zztools_dssslz00
																					(BgL_argsz00_21);
																				BgL_arg1437z00_1805 =
																					(((BgL_sfunz00_bglt)
																						COBJECT(((BgL_sfunz00_bglt)
																								BgL_oldzd2valuezd2_1781)))->
																					BgL_keysz00);
																				BgL_test1969z00_3317 =
																					BGl_equalzf3zf3zz__r4_equivalence_6_2z00
																					(BgL_arg1434z00_1804,
																					BgL_arg1437z00_1805);
																			}
																			if (BgL_test1969z00_3317)
																				{	/* Ast/glo_def.scm 171 */
																					BgL_oldz00_19;
																				}
																			else
																				{	/* Ast/glo_def.scm 173 */
																					obj_t BgL_list1424z00_1803;

																					BgL_list1424z00_1803 =
																						MAKE_YOUNG_PAIR
																						(BGl_string1888z00zzast_glozd2defzd2,
																						BNIL);
																					((obj_t)
																						BGl_mismatchzd2errorzd2zzast_glozd2defzd2
																						(((BgL_globalz00_bglt)
																								BgL_oldz00_19),
																							BgL_srczd2expzd2_24,
																							BgL_list1424z00_1803));
																				}
																		}
																	else
																		{	/* Ast/glo_def.scm 176 */
																			bool_t BgL_test1970z00_3326;

																			{	/* Ast/glo_def.scm 176 */
																				obj_t BgL_arg1472z00_1810;
																				obj_t BgL_arg1473z00_1811;

																				BgL_arg1472z00_1810 =
																					(((BgL_sfunz00_bglt) COBJECT(
																							((BgL_sfunz00_bglt)
																								BgL_oldzd2valuezd2_1781)))->
																					BgL_dssslzd2keywordszd2);
																				BgL_arg1473z00_1811 =
																					BGl_dssslzd2formalszd2zztools_dssslz00
																					(BgL_argsz00_21);
																				BgL_test1970z00_3326 =
																					BGl_equalzf3zf3zz__r4_equivalence_6_2z00
																					(BgL_arg1472z00_1810,
																					BgL_arg1473z00_1811);
																			}
																			if (BgL_test1970z00_3326)
																				{	/* Ast/glo_def.scm 176 */
																					BFALSE;
																				}
																			else
																				{	/* Ast/glo_def.scm 178 */
																					obj_t BgL_list1456z00_1809;

																					BgL_list1456z00_1809 =
																						MAKE_YOUNG_PAIR
																						(BGl_string1889z00zzast_glozd2defzd2,
																						BNIL);
																					((obj_t)
																						BGl_mismatchzd2errorzd2zzast_glozd2defzd2
																						(((BgL_globalz00_bglt)
																								BgL_oldz00_19),
																							BgL_srczd2expzd2_24,
																							BgL_list1456z00_1809));
																				}
																		}
																}
														}
													else
														{	/* Ast/glo_def.scm 180 */
															obj_t BgL_g1110z00_1812;

															{	/* Ast/glo_def.scm 181 */
																obj_t BgL_l1255z00_1838;

																BgL_l1255z00_1838 =
																	(((BgL_sfunz00_bglt) COBJECT(
																			((BgL_sfunz00_bglt)
																				BgL_oldzd2valuezd2_1781)))->
																	BgL_argsz00);
																if (NULLP(BgL_l1255z00_1838))
																	{	/* Ast/glo_def.scm 181 */
																		BgL_g1110z00_1812 = BNIL;
																	}
																else
																	{	/* Ast/glo_def.scm 181 */
																		obj_t BgL_head1257z00_1840;

																		BgL_head1257z00_1840 =
																			MAKE_YOUNG_PAIR(BNIL, BNIL);
																		{
																			obj_t BgL_l1255z00_1842;
																			obj_t BgL_tail1258z00_1843;

																			BgL_l1255z00_1842 = BgL_l1255z00_1838;
																			BgL_tail1258z00_1843 =
																				BgL_head1257z00_1840;
																		BgL_zc3z04anonymousza31555ze3z87_1844:
																			if (NULLP(BgL_l1255z00_1842))
																				{	/* Ast/glo_def.scm 181 */
																					BgL_g1110z00_1812 =
																						CDR(BgL_head1257z00_1840);
																				}
																			else
																				{	/* Ast/glo_def.scm 181 */
																					obj_t BgL_newtail1259z00_1846;

																					{	/* Ast/glo_def.scm 181 */
																						obj_t BgL_arg1561z00_1848;

																						{	/* Ast/glo_def.scm 181 */
																							obj_t BgL_az00_1849;

																							BgL_az00_1849 =
																								CAR(
																								((obj_t) BgL_l1255z00_1842));
																							{	/* Ast/glo_def.scm 183 */
																								bool_t BgL_test1973z00_3345;

																								{	/* Ast/glo_def.scm 183 */
																									obj_t BgL_classz00_2379;

																									BgL_classz00_2379 =
																										BGl_localz00zzast_varz00;
																									if (BGL_OBJECTP
																										(BgL_az00_1849))
																										{	/* Ast/glo_def.scm 183 */
																											BgL_objectz00_bglt
																												BgL_arg1807z00_2381;
																											BgL_arg1807z00_2381 =
																												(BgL_objectz00_bglt)
																												(BgL_az00_1849);
																											if (BGL_CONDEXPAND_ISA_ARCH64())
																												{	/* Ast/glo_def.scm 183 */
																													long BgL_idxz00_2387;

																													BgL_idxz00_2387 =
																														BGL_OBJECT_INHERITANCE_NUM
																														(BgL_arg1807z00_2381);
																													BgL_test1973z00_3345 =
																														(VECTOR_REF
																														(BGl_za2inheritancesza2z00zz__objectz00,
																															(BgL_idxz00_2387 +
																																2L)) ==
																														BgL_classz00_2379);
																												}
																											else
																												{	/* Ast/glo_def.scm 183 */
																													bool_t
																														BgL_res1869z00_2412;
																													{	/* Ast/glo_def.scm 183 */
																														obj_t
																															BgL_oclassz00_2395;
																														{	/* Ast/glo_def.scm 183 */
																															obj_t
																																BgL_arg1815z00_2403;
																															long
																																BgL_arg1816z00_2404;
																															BgL_arg1815z00_2403
																																=
																																(BGl_za2classesza2z00zz__objectz00);
																															{	/* Ast/glo_def.scm 183 */
																																long
																																	BgL_arg1817z00_2405;
																																BgL_arg1817z00_2405
																																	=
																																	BGL_OBJECT_CLASS_NUM
																																	(BgL_arg1807z00_2381);
																																BgL_arg1816z00_2404
																																	=
																																	(BgL_arg1817z00_2405
																																	-
																																	OBJECT_TYPE);
																															}
																															BgL_oclassz00_2395
																																=
																																VECTOR_REF
																																(BgL_arg1815z00_2403,
																																BgL_arg1816z00_2404);
																														}
																														{	/* Ast/glo_def.scm 183 */
																															bool_t
																																BgL__ortest_1115z00_2396;
																															BgL__ortest_1115z00_2396
																																=
																																(BgL_classz00_2379
																																==
																																BgL_oclassz00_2395);
																															if (BgL__ortest_1115z00_2396)
																																{	/* Ast/glo_def.scm 183 */
																																	BgL_res1869z00_2412
																																		=
																																		BgL__ortest_1115z00_2396;
																																}
																															else
																																{	/* Ast/glo_def.scm 183 */
																																	long
																																		BgL_odepthz00_2397;
																																	{	/* Ast/glo_def.scm 183 */
																																		obj_t
																																			BgL_arg1804z00_2398;
																																		BgL_arg1804z00_2398
																																			=
																																			(BgL_oclassz00_2395);
																																		BgL_odepthz00_2397
																																			=
																																			BGL_CLASS_DEPTH
																																			(BgL_arg1804z00_2398);
																																	}
																																	if (
																																		(2L <
																																			BgL_odepthz00_2397))
																																		{	/* Ast/glo_def.scm 183 */
																																			obj_t
																																				BgL_arg1802z00_2400;
																																			{	/* Ast/glo_def.scm 183 */
																																				obj_t
																																					BgL_arg1803z00_2401;
																																				BgL_arg1803z00_2401
																																					=
																																					(BgL_oclassz00_2395);
																																				BgL_arg1802z00_2400
																																					=
																																					BGL_CLASS_ANCESTORS_REF
																																					(BgL_arg1803z00_2401,
																																					2L);
																																			}
																																			BgL_res1869z00_2412
																																				=
																																				(BgL_arg1802z00_2400
																																				==
																																				BgL_classz00_2379);
																																		}
																																	else
																																		{	/* Ast/glo_def.scm 183 */
																																			BgL_res1869z00_2412
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																														}
																													}
																													BgL_test1973z00_3345 =
																														BgL_res1869z00_2412;
																												}
																										}
																									else
																										{	/* Ast/glo_def.scm 183 */
																											BgL_test1973z00_3345 =
																												((bool_t) 0);
																										}
																								}
																								if (BgL_test1973z00_3345)
																									{	/* Ast/glo_def.scm 183 */
																										BgL_arg1561z00_1848 =
																											((obj_t)
																											(((BgL_variablez00_bglt)
																													COBJECT((
																															(BgL_variablez00_bglt)
																															((BgL_localz00_bglt) BgL_az00_1849))))->BgL_typez00));
																									}
																								else
																									{	/* Ast/glo_def.scm 185 */
																										bool_t BgL_test1978z00_3372;

																										{	/* Ast/glo_def.scm 185 */
																											obj_t BgL_classz00_2414;

																											BgL_classz00_2414 =
																												BGl_typez00zztype_typez00;
																											if (BGL_OBJECTP
																												(BgL_az00_1849))
																												{	/* Ast/glo_def.scm 185 */
																													BgL_objectz00_bglt
																														BgL_arg1807z00_2416;
																													BgL_arg1807z00_2416 =
																														(BgL_objectz00_bglt)
																														(BgL_az00_1849);
																													if (BGL_CONDEXPAND_ISA_ARCH64())
																														{	/* Ast/glo_def.scm 185 */
																															long
																																BgL_idxz00_2422;
																															BgL_idxz00_2422 =
																																BGL_OBJECT_INHERITANCE_NUM
																																(BgL_arg1807z00_2416);
																															BgL_test1978z00_3372
																																=
																																(VECTOR_REF
																																(BGl_za2inheritancesza2z00zz__objectz00,
																																	(BgL_idxz00_2422
																																		+ 1L)) ==
																																BgL_classz00_2414);
																														}
																													else
																														{	/* Ast/glo_def.scm 185 */
																															bool_t
																																BgL_res1870z00_2447;
																															{	/* Ast/glo_def.scm 185 */
																																obj_t
																																	BgL_oclassz00_2430;
																																{	/* Ast/glo_def.scm 185 */
																																	obj_t
																																		BgL_arg1815z00_2438;
																																	long
																																		BgL_arg1816z00_2439;
																																	BgL_arg1815z00_2438
																																		=
																																		(BGl_za2classesza2z00zz__objectz00);
																																	{	/* Ast/glo_def.scm 185 */
																																		long
																																			BgL_arg1817z00_2440;
																																		BgL_arg1817z00_2440
																																			=
																																			BGL_OBJECT_CLASS_NUM
																																			(BgL_arg1807z00_2416);
																																		BgL_arg1816z00_2439
																																			=
																																			(BgL_arg1817z00_2440
																																			-
																																			OBJECT_TYPE);
																																	}
																																	BgL_oclassz00_2430
																																		=
																																		VECTOR_REF
																																		(BgL_arg1815z00_2438,
																																		BgL_arg1816z00_2439);
																																}
																																{	/* Ast/glo_def.scm 185 */
																																	bool_t
																																		BgL__ortest_1115z00_2431;
																																	BgL__ortest_1115z00_2431
																																		=
																																		(BgL_classz00_2414
																																		==
																																		BgL_oclassz00_2430);
																																	if (BgL__ortest_1115z00_2431)
																																		{	/* Ast/glo_def.scm 185 */
																																			BgL_res1870z00_2447
																																				=
																																				BgL__ortest_1115z00_2431;
																																		}
																																	else
																																		{	/* Ast/glo_def.scm 185 */
																																			long
																																				BgL_odepthz00_2432;
																																			{	/* Ast/glo_def.scm 185 */
																																				obj_t
																																					BgL_arg1804z00_2433;
																																				BgL_arg1804z00_2433
																																					=
																																					(BgL_oclassz00_2430);
																																				BgL_odepthz00_2432
																																					=
																																					BGL_CLASS_DEPTH
																																					(BgL_arg1804z00_2433);
																																			}
																																			if (
																																				(1L <
																																					BgL_odepthz00_2432))
																																				{	/* Ast/glo_def.scm 185 */
																																					obj_t
																																						BgL_arg1802z00_2435;
																																					{	/* Ast/glo_def.scm 185 */
																																						obj_t
																																							BgL_arg1803z00_2436;
																																						BgL_arg1803z00_2436
																																							=
																																							(BgL_oclassz00_2430);
																																						BgL_arg1802z00_2435
																																							=
																																							BGL_CLASS_ANCESTORS_REF
																																							(BgL_arg1803z00_2436,
																																							1L);
																																					}
																																					BgL_res1870z00_2447
																																						=
																																						(BgL_arg1802z00_2435
																																						==
																																						BgL_classz00_2414);
																																				}
																																			else
																																				{	/* Ast/glo_def.scm 185 */
																																					BgL_res1870z00_2447
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																}
																															}
																															BgL_test1978z00_3372
																																=
																																BgL_res1870z00_2447;
																														}
																												}
																											else
																												{	/* Ast/glo_def.scm 185 */
																													BgL_test1978z00_3372 =
																														((bool_t) 0);
																												}
																										}
																										if (BgL_test1978z00_3372)
																											{	/* Ast/glo_def.scm 185 */
																												BgL_arg1561z00_1848 =
																													BgL_az00_1849;
																											}
																										else
																											{	/* Ast/glo_def.scm 185 */
																												BgL_arg1561z00_1848 =
																													BGl_internalzd2errorzd2zztools_errorz00
																													(BGl_string1885z00zzast_glozd2defzd2,
																													BGl_string1886z00zzast_glozd2defzd2,
																													BGl_shapez00zztools_shapez00
																													(BgL_az00_1849));
																											}
																									}
																							}
																						}
																						BgL_newtail1259z00_1846 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1561z00_1848, BNIL);
																					}
																					SET_CDR(BgL_tail1258z00_1843,
																						BgL_newtail1259z00_1846);
																					{	/* Ast/glo_def.scm 181 */
																						obj_t BgL_arg1559z00_1847;

																						BgL_arg1559z00_1847 =
																							CDR(((obj_t) BgL_l1255z00_1842));
																						{
																							obj_t BgL_tail1258z00_3402;
																							obj_t BgL_l1255z00_3401;

																							BgL_l1255z00_3401 =
																								BgL_arg1559z00_1847;
																							BgL_tail1258z00_3402 =
																								BgL_newtail1259z00_1846;
																							BgL_tail1258z00_1843 =
																								BgL_tail1258z00_3402;
																							BgL_l1255z00_1842 =
																								BgL_l1255z00_3401;
																							goto
																								BgL_zc3z04anonymousza31555ze3z87_1844;
																						}
																					}
																				}
																		}
																	}
															}
															{
																obj_t BgL_localsz00_1814;
																obj_t BgL_typesz00_1815;

																BgL_localsz00_1814 = BgL_localsz00_22;
																BgL_typesz00_1815 = BgL_g1110z00_1812;
															BgL_zc3z04anonymousza31474ze3z87_1816:
																if (NULLP(BgL_localsz00_1814))
																	{	/* Ast/glo_def.scm 193 */
																		if (NULLP(BgL_typesz00_1815))
																			{	/* Ast/glo_def.scm 196 */
																				((((BgL_globalz00_bglt) COBJECT(
																								((BgL_globalz00_bglt)
																									BgL_oldz00_19)))->
																						BgL_srcz00) =
																					((obj_t) BgL_srczd2expzd2_24),
																					BUNSPEC);
																			}
																		else
																			{	/* Ast/glo_def.scm 198 */
																				obj_t BgL_list1477z00_1819;

																				BgL_list1477z00_1819 =
																					MAKE_YOUNG_PAIR
																					(BGl_string1890z00zzast_glozd2defzd2,
																					BNIL);
																				((obj_t)
																					BGl_mismatchzd2errorzd2zzast_glozd2defzd2
																					(((BgL_globalz00_bglt) BgL_oldz00_19),
																						BgL_srczd2expzd2_24,
																						BgL_list1477z00_1819));
																			}
																	}
																else
																	{	/* Ast/glo_def.scm 199 */
																		bool_t BgL_test1985z00_3413;

																		if (NULLP(BgL_typesz00_1815))
																			{	/* Ast/glo_def.scm 199 */
																				BgL_test1985z00_3413 = ((bool_t) 1);
																			}
																		else
																			{	/* Ast/glo_def.scm 200 */
																				bool_t BgL_test1987z00_3416;

																				{	/* Ast/glo_def.scm 201 */
																					BgL_typez00_bglt BgL_arg1544z00_1834;
																					obj_t BgL_arg1546z00_1835;

																					BgL_arg1544z00_1834 =
																						(((BgL_variablez00_bglt) COBJECT(
																								((BgL_variablez00_bglt)
																									((BgL_localz00_bglt)
																										CAR(
																											((obj_t)
																												BgL_localsz00_1814))))))->
																						BgL_typez00);
																					BgL_arg1546z00_1835 =
																						CAR(((obj_t) BgL_typesz00_1815));
																					BgL_test1987z00_3416 =
																						BGl_compatiblezd2typezf3z21zzast_glozd2defzd2
																						(((bool_t) 0), BgL_arg1544z00_1834,
																						((BgL_typez00_bglt)
																							BgL_arg1546z00_1835));
																				}
																				if (BgL_test1987z00_3416)
																					{	/* Ast/glo_def.scm 200 */
																						BgL_test1985z00_3413 = ((bool_t) 0);
																					}
																				else
																					{	/* Ast/glo_def.scm 200 */
																						BgL_test1985z00_3413 = ((bool_t) 1);
																					}
																			}
																		if (BgL_test1985z00_3413)
																			{	/* Ast/glo_def.scm 203 */
																				obj_t BgL_list1516z00_1829;

																				BgL_list1516z00_1829 =
																					MAKE_YOUNG_PAIR
																					(BGl_string1891z00zzast_glozd2defzd2,
																					BNIL);
																				((obj_t)
																					BGl_mismatchzd2errorzd2zzast_glozd2defzd2
																					(((BgL_globalz00_bglt) BgL_oldz00_19),
																						BgL_srczd2expzd2_24,
																						BgL_list1516z00_1829));
																			}
																		else
																			{	/* Ast/glo_def.scm 205 */
																				obj_t BgL_arg1535z00_1830;
																				obj_t BgL_arg1540z00_1831;

																				BgL_arg1535z00_1830 =
																					CDR(((obj_t) BgL_localsz00_1814));
																				BgL_arg1540z00_1831 =
																					CDR(((obj_t) BgL_typesz00_1815));
																				{
																					obj_t BgL_typesz00_3435;
																					obj_t BgL_localsz00_3434;

																					BgL_localsz00_3434 =
																						BgL_arg1535z00_1830;
																					BgL_typesz00_3435 =
																						BgL_arg1540z00_1831;
																					BgL_typesz00_1815 = BgL_typesz00_3435;
																					BgL_localsz00_1814 =
																						BgL_localsz00_3434;
																					goto
																						BgL_zc3z04anonymousza31474ze3z87_1816;
																				}
																			}
																	}
															}
														}
												}
											else
												{	/* Ast/glo_def.scm 159 */
													obj_t BgL_list1565z00_1854;

													BgL_list1565z00_1854 =
														MAKE_YOUNG_PAIR(BGl_string1892z00zzast_glozd2defzd2,
														BNIL);
													((obj_t)
														BGl_mismatchzd2errorzd2zzast_glozd2defzd2((
																(BgL_globalz00_bglt) BgL_oldz00_19),
															BgL_srczd2expzd2_24, BgL_list1565z00_1854));
												}
										}
									else
										{	/* Ast/glo_def.scm 155 */
											obj_t BgL_list1574z00_1857;

											BgL_list1574z00_1857 =
												MAKE_YOUNG_PAIR(BGl_string1890z00zzast_glozd2defzd2,
												BNIL);
											((obj_t)
												BGl_mismatchzd2errorzd2zzast_glozd2defzd2((
														(BgL_globalz00_bglt) BgL_oldz00_19),
													BgL_srczd2expzd2_24, BgL_list1574z00_1857));
										}
								}
							else
								{	/* Ast/glo_def.scm 153 */
									obj_t BgL_arg1584z00_1860;

									{	/* Ast/glo_def.scm 153 */
										obj_t BgL_arg1589z00_1862;

										BgL_arg1589z00_1862 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_oldzd2valuezd2_1781)))->
											BgL_classz00);
										{	/* Ast/glo_def.scm 152 */
											obj_t BgL_list1590z00_1863;

											{	/* Ast/glo_def.scm 152 */
												obj_t BgL_arg1591z00_1864;

												BgL_arg1591z00_1864 =
													MAKE_YOUNG_PAIR(BgL_classz00_23, BNIL);
												BgL_list1590z00_1863 =
													MAKE_YOUNG_PAIR(BgL_arg1589z00_1862,
													BgL_arg1591z00_1864);
											}
											BgL_arg1584z00_1860 =
												BGl_formatz00zz__r4_output_6_10_3z00
												(BGl_string1893z00zzast_glozd2defzd2,
												BgL_list1590z00_1863);
										}
									}
									{	/* Ast/glo_def.scm 151 */
										obj_t BgL_list1585z00_1861;

										BgL_list1585z00_1861 =
											MAKE_YOUNG_PAIR(BgL_arg1584z00_1860, BNIL);
										((obj_t)
											BGl_mismatchzd2errorzd2zzast_glozd2defzd2(
												((BgL_globalz00_bglt) BgL_oldz00_19),
												BgL_srczd2expzd2_24, BgL_list1585z00_1861));
									}
								}
						}
					else
						{	/* Ast/glo_def.scm 148 */
							obj_t BgL_list1594z00_1866;

							BgL_list1594z00_1866 =
								MAKE_YOUNG_PAIR(BGl_string1894z00zzast_glozd2defzd2, BNIL);
							((obj_t)
								BGl_mismatchzd2errorzd2zzast_glozd2defzd2(
									((BgL_globalz00_bglt) BgL_oldz00_19), BgL_srczd2expzd2_24,
									BgL_list1594z00_1866));
						}
				}
				return ((BgL_globalz00_bglt) BgL_oldz00_19);
			}
		}

	}



/* def-global-scnst! */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_defzd2globalzd2scnstz12z12zzast_glozd2defzd2(obj_t BgL_idz00_25,
		obj_t BgL_modulez00_26, obj_t BgL_nodez00_27, obj_t BgL_classz00_28,
		obj_t BgL_locz00_29)
	{
		{	/* Ast/glo_def.scm 211 */
			BGl_enterzd2functionzd2zztools_errorz00(BgL_idz00_25);
			{	/* Ast/glo_def.scm 213 */
				obj_t BgL_idzd2typezd2_2457;

				BgL_idzd2typezd2_2457 =
					BGl_parsezd2idzd2zzast_identz00(BgL_idz00_25, BgL_locz00_29);
				{	/* Ast/glo_def.scm 213 */
					obj_t BgL_idzd2idzd2_2458;

					BgL_idzd2idzd2_2458 = CAR(BgL_idzd2typezd2_2457);
					{	/* Ast/glo_def.scm 214 */
						obj_t BgL_oldzd2globalzd2_2459;

						BgL_oldzd2globalzd2_2459 =
							BGl_findzd2globalzf2modulez20zzast_envz00(BgL_idzd2idzd2_2458,
							BgL_modulez00_26);
						{	/* Ast/glo_def.scm 215 */
							BgL_globalz00_bglt BgL_globalz00_2460;

							BgL_globalz00_2460 =
								BGl_declarezd2globalzd2scnstz12z12zzast_glozd2declzd2
								(BgL_idz00_25, BFALSE, BgL_modulez00_26, CNST_TABLE_REF(2),
								BgL_nodez00_27, BgL_classz00_28, BgL_locz00_29);
							{	/* Ast/glo_def.scm 216 */

								BGl_removezd2varzd2fromz12z12zzast_removez00(CNST_TABLE_REF(4),
									((BgL_variablez00_bglt) BgL_globalz00_2460));
								BGl_leavezd2functionzd2zztools_errorz00();
								return BgL_globalz00_2460;
							}
						}
					}
				}
			}
		}

	}



/* &def-global-scnst! */
	BgL_globalz00_bglt BGl_z62defzd2globalzd2scnstz12z70zzast_glozd2defzd2(obj_t
		BgL_envz00_2995, obj_t BgL_idz00_2996, obj_t BgL_modulez00_2997,
		obj_t BgL_nodez00_2998, obj_t BgL_classz00_2999, obj_t BgL_locz00_3000)
	{
		{	/* Ast/glo_def.scm 211 */
			return
				BGl_defzd2globalzd2scnstz12z12zzast_glozd2defzd2(BgL_idz00_2996,
				BgL_modulez00_2997, BgL_nodez00_2998, BgL_classz00_2999,
				BgL_locz00_3000);
		}

	}



/* def-global-svar! */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_defzd2globalzd2svarz12z12zzast_glozd2defzd2(obj_t BgL_idz00_30,
		obj_t BgL_modulez00_31, obj_t BgL_srczd2expzd2_32, obj_t BgL_remz00_33)
	{
		{	/* Ast/glo_def.scm 227 */
			{	/* Ast/glo_def.scm 228 */
				obj_t BgL_locz00_1871;

				BgL_locz00_1871 =
					BGl_findzd2locationzd2zztools_locationz00(BgL_srczd2expzd2_32);
				{	/* Ast/glo_def.scm 228 */
					obj_t BgL_idzd2typezd2_1872;

					BgL_idzd2typezd2_1872 =
						BGl_parsezd2idzd2zzast_identz00(BgL_idz00_30, BgL_locz00_1871);
					{	/* Ast/glo_def.scm 229 */
						obj_t BgL_idzd2idzd2_1873;

						BgL_idzd2idzd2_1873 = CAR(BgL_idzd2typezd2_1872);
						{	/* Ast/glo_def.scm 230 */
							obj_t BgL_oldzd2globalzd2_1874;

							BgL_oldzd2globalzd2_1874 =
								BGl_findzd2globalzf2modulez20zzast_envz00(BgL_idzd2idzd2_1873,
								BgL_modulez00_31);
							{	/* Ast/glo_def.scm 231 */
								obj_t BgL_importz00_1875;

								{	/* Ast/glo_def.scm 232 */
									bool_t BgL_test1988z00_3473;

									if (
										((long) CINT(BGl_za2bdbzd2debugza2zd2zzengine_paramz00) >=
											3L))
										{	/* Ast/glo_def.scm 233 */
											obj_t BgL_arg1625z00_1895;

											{	/* Ast/glo_def.scm 233 */
												obj_t BgL_arg1626z00_1896;

												BgL_arg1626z00_1896 =
													BGl_thezd2backendzd2zzbackend_backendz00();
												BgL_arg1625z00_1895 =
													(((BgL_backendz00_bglt) COBJECT(
															((BgL_backendz00_bglt) BgL_arg1626z00_1896)))->
													BgL_debugzd2supportzd2);
											}
											BgL_test1988z00_3473 =
												CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
												(CNST_TABLE_REF(0), BgL_arg1625z00_1895));
										}
									else
										{	/* Ast/glo_def.scm 232 */
											BgL_test1988z00_3473 = ((bool_t) 0);
										}
									if (BgL_test1988z00_3473)
										{	/* Ast/glo_def.scm 232 */
											BgL_importz00_1875 = CNST_TABLE_REF(1);
										}
									else
										{	/* Ast/glo_def.scm 232 */
											BgL_importz00_1875 = CNST_TABLE_REF(2);
										}
								}
								{	/* Ast/glo_def.scm 232 */
									obj_t BgL_typez00_1876;

									{	/* Ast/glo_def.scm 236 */
										obj_t BgL_typez00_1884;

										BgL_typez00_1884 = CDR(BgL_idzd2typezd2_1872);
										if (
											((((BgL_typez00_bglt) COBJECT(
															((BgL_typez00_bglt) BgL_typez00_1884)))->
													BgL_classz00) == CNST_TABLE_REF(5)))
											{	/* Ast/glo_def.scm 239 */
												BgL_typez00_1876 = BgL_typez00_1884;
											}
										else
											{	/* Ast/glo_def.scm 239 */
												BgL_typez00_1876 =
													BGl_userzd2errorzd2zztools_errorz00
													(BgL_idzd2idzd2_1873,
													BGl_string1895z00zzast_glozd2defzd2,
													BGl_shapez00zztools_shapez00(BgL_typez00_1884), BNIL);
											}
									}
									{	/* Ast/glo_def.scm 236 */
										BgL_globalz00_bglt BgL_globalz00_1877;

										{	/* Ast/glo_def.scm 245 */
											bool_t BgL_test1991z00_3493;

											{	/* Ast/glo_def.scm 245 */
												obj_t BgL_classz00_2467;

												BgL_classz00_2467 = BGl_globalz00zzast_varz00;
												if (BGL_OBJECTP(BgL_oldzd2globalzd2_1874))
													{	/* Ast/glo_def.scm 245 */
														BgL_objectz00_bglt BgL_arg1807z00_2469;

														BgL_arg1807z00_2469 =
															(BgL_objectz00_bglt) (BgL_oldzd2globalzd2_1874);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Ast/glo_def.scm 245 */
																long BgL_idxz00_2475;

																BgL_idxz00_2475 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_2469);
																BgL_test1991z00_3493 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_2475 + 2L)) ==
																	BgL_classz00_2467);
															}
														else
															{	/* Ast/glo_def.scm 245 */
																bool_t BgL_res1871z00_2500;

																{	/* Ast/glo_def.scm 245 */
																	obj_t BgL_oclassz00_2483;

																	{	/* Ast/glo_def.scm 245 */
																		obj_t BgL_arg1815z00_2491;
																		long BgL_arg1816z00_2492;

																		BgL_arg1815z00_2491 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Ast/glo_def.scm 245 */
																			long BgL_arg1817z00_2493;

																			BgL_arg1817z00_2493 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_2469);
																			BgL_arg1816z00_2492 =
																				(BgL_arg1817z00_2493 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_2483 =
																			VECTOR_REF(BgL_arg1815z00_2491,
																			BgL_arg1816z00_2492);
																	}
																	{	/* Ast/glo_def.scm 245 */
																		bool_t BgL__ortest_1115z00_2484;

																		BgL__ortest_1115z00_2484 =
																			(BgL_classz00_2467 == BgL_oclassz00_2483);
																		if (BgL__ortest_1115z00_2484)
																			{	/* Ast/glo_def.scm 245 */
																				BgL_res1871z00_2500 =
																					BgL__ortest_1115z00_2484;
																			}
																		else
																			{	/* Ast/glo_def.scm 245 */
																				long BgL_odepthz00_2485;

																				{	/* Ast/glo_def.scm 245 */
																					obj_t BgL_arg1804z00_2486;

																					BgL_arg1804z00_2486 =
																						(BgL_oclassz00_2483);
																					BgL_odepthz00_2485 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_2486);
																				}
																				if ((2L < BgL_odepthz00_2485))
																					{	/* Ast/glo_def.scm 245 */
																						obj_t BgL_arg1802z00_2488;

																						{	/* Ast/glo_def.scm 245 */
																							obj_t BgL_arg1803z00_2489;

																							BgL_arg1803z00_2489 =
																								(BgL_oclassz00_2483);
																							BgL_arg1802z00_2488 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_2489, 2L);
																						}
																						BgL_res1871z00_2500 =
																							(BgL_arg1802z00_2488 ==
																							BgL_classz00_2467);
																					}
																				else
																					{	/* Ast/glo_def.scm 245 */
																						BgL_res1871z00_2500 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test1991z00_3493 = BgL_res1871z00_2500;
															}
													}
												else
													{	/* Ast/glo_def.scm 245 */
														BgL_test1991z00_3493 = ((bool_t) 0);
													}
											}
											if (BgL_test1991z00_3493)
												{	/* Ast/glo_def.scm 245 */
													BgL_globalz00_1877 =
														BGl_checkzd2svarzd2definitionz00zzast_glozd2defzd2
														(BgL_oldzd2globalzd2_1874, BgL_typez00_1876,
														BgL_srczd2expzd2_32);
												}
											else
												{	/* Ast/glo_def.scm 245 */
													BgL_globalz00_1877 =
														BGl_declarezd2globalzd2svarz12z12zzast_glozd2declzd2
														(BgL_idz00_30, BFALSE, BgL_modulez00_31,
														BgL_importz00_1875, BgL_srczd2expzd2_32, BFALSE);
												}
										}
										{	/* Ast/glo_def.scm 244 */
											obj_t BgL_defzd2loczd2_1878;

											BgL_defzd2loczd2_1878 =
												BGl_findzd2locationzd2zztools_locationz00
												(BgL_srczd2expzd2_32);
											{	/* Ast/glo_def.scm 250 */

												{	/* Ast/glo_def.scm 300 */
													BgL_typez00_bglt BgL_oldzd2typezd2_2503;

													BgL_oldzd2typezd2_2503 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt) BgL_globalz00_1877)))->
														BgL_typez00);
													if ((((obj_t) BgL_oldzd2typezd2_2503) ==
															BGl_za2_za2z00zztype_cachez00))
														{	/* Ast/glo_def.scm 301 */
															((((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				BgL_globalz00_1877)))->BgL_typez00) =
																((BgL_typez00_bglt) ((BgL_typez00_bglt)
																		BgL_typez00_1876)), BUNSPEC);
														}
													else
														{	/* Ast/glo_def.scm 301 */
															BFALSE;
														}
												}
												{	/* Ast/glo_def.scm 254 */
													bool_t BgL_test1997z00_3527;

													{	/* Ast/glo_def.scm 254 */
														BgL_valuez00_bglt BgL_arg1605z00_1882;

														BgL_arg1605z00_1882 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		BgL_globalz00_1877)))->BgL_valuez00);
														{	/* Ast/glo_def.scm 254 */
															obj_t BgL_classz00_2508;

															BgL_classz00_2508 = BGl_svarz00zzast_varz00;
															{	/* Ast/glo_def.scm 254 */
																BgL_objectz00_bglt BgL_arg1807z00_2510;

																{	/* Ast/glo_def.scm 254 */
																	obj_t BgL_tmpz00_3530;

																	BgL_tmpz00_3530 =
																		((obj_t)
																		((BgL_objectz00_bglt) BgL_arg1605z00_1882));
																	BgL_arg1807z00_2510 =
																		(BgL_objectz00_bglt) (BgL_tmpz00_3530);
																}
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Ast/glo_def.scm 254 */
																		long BgL_idxz00_2516;

																		BgL_idxz00_2516 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_2510);
																		BgL_test1997z00_3527 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_2516 + 2L)) ==
																			BgL_classz00_2508);
																	}
																else
																	{	/* Ast/glo_def.scm 254 */
																		bool_t BgL_res1872z00_2541;

																		{	/* Ast/glo_def.scm 254 */
																			obj_t BgL_oclassz00_2524;

																			{	/* Ast/glo_def.scm 254 */
																				obj_t BgL_arg1815z00_2532;
																				long BgL_arg1816z00_2533;

																				BgL_arg1815z00_2532 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Ast/glo_def.scm 254 */
																					long BgL_arg1817z00_2534;

																					BgL_arg1817z00_2534 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_2510);
																					BgL_arg1816z00_2533 =
																						(BgL_arg1817z00_2534 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_2524 =
																					VECTOR_REF(BgL_arg1815z00_2532,
																					BgL_arg1816z00_2533);
																			}
																			{	/* Ast/glo_def.scm 254 */
																				bool_t BgL__ortest_1115z00_2525;

																				BgL__ortest_1115z00_2525 =
																					(BgL_classz00_2508 ==
																					BgL_oclassz00_2524);
																				if (BgL__ortest_1115z00_2525)
																					{	/* Ast/glo_def.scm 254 */
																						BgL_res1872z00_2541 =
																							BgL__ortest_1115z00_2525;
																					}
																				else
																					{	/* Ast/glo_def.scm 254 */
																						long BgL_odepthz00_2526;

																						{	/* Ast/glo_def.scm 254 */
																							obj_t BgL_arg1804z00_2527;

																							BgL_arg1804z00_2527 =
																								(BgL_oclassz00_2524);
																							BgL_odepthz00_2526 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_2527);
																						}
																						if ((2L < BgL_odepthz00_2526))
																							{	/* Ast/glo_def.scm 254 */
																								obj_t BgL_arg1802z00_2529;

																								{	/* Ast/glo_def.scm 254 */
																									obj_t BgL_arg1803z00_2530;

																									BgL_arg1803z00_2530 =
																										(BgL_oclassz00_2524);
																									BgL_arg1802z00_2529 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_2530, 2L);
																								}
																								BgL_res1872z00_2541 =
																									(BgL_arg1802z00_2529 ==
																									BgL_classz00_2508);
																							}
																						else
																							{	/* Ast/glo_def.scm 254 */
																								BgL_res1872z00_2541 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test1997z00_3527 = BgL_res1872z00_2541;
																	}
															}
														}
													}
													if (BgL_test1997z00_3527)
														{	/* Ast/glo_def.scm 256 */
															BgL_valuez00_bglt BgL_arg1602z00_1881;

															BgL_arg1602z00_1881 =
																(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			BgL_globalz00_1877)))->BgL_valuez00);
															((((BgL_svarz00_bglt) COBJECT(((BgL_svarz00_bglt)
																				BgL_arg1602z00_1881)))->BgL_locz00) =
																((obj_t) BgL_defzd2loczd2_1878), BUNSPEC);
														}
													else
														{	/* Ast/glo_def.scm 254 */
															BFALSE;
														}
												}
												BGl_removezd2varzd2fromz12z12zzast_removez00
													(BgL_remz00_33,
													((BgL_variablez00_bglt) BgL_globalz00_1877));
												return BgL_globalz00_1877;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &def-global-svar! */
	BgL_globalz00_bglt BGl_z62defzd2globalzd2svarz12z70zzast_glozd2defzd2(obj_t
		BgL_envz00_3001, obj_t BgL_idz00_3002, obj_t BgL_modulez00_3003,
		obj_t BgL_srczd2expzd2_3004, obj_t BgL_remz00_3005)
	{
		{	/* Ast/glo_def.scm 227 */
			return
				BGl_defzd2globalzd2svarz12z12zzast_glozd2defzd2(BgL_idz00_3002,
				BgL_modulez00_3003, BgL_srczd2expzd2_3004, BgL_remz00_3005);
		}

	}



/* check-svar-definition */
	BgL_globalz00_bglt BGl_checkzd2svarzd2definitionz00zzast_glozd2defzd2(obj_t
		BgL_oldz00_34, obj_t BgL_typez00_35, obj_t BgL_srczd2expzd2_36)
	{
		{	/* Ast/glo_def.scm 264 */
			{	/* Ast/glo_def.scm 265 */
				BgL_valuez00_bglt BgL_oldzd2valuezd2_1897;

				BgL_oldzd2valuezd2_1897 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt)
								((BgL_globalz00_bglt) BgL_oldz00_34))))->BgL_valuez00);
				{	/* Ast/glo_def.scm 267 */
					bool_t BgL_test2001z00_3563;

					{	/* Ast/glo_def.scm 267 */
						obj_t BgL_classz00_2545;

						BgL_classz00_2545 = BGl_svarz00zzast_varz00;
						{	/* Ast/glo_def.scm 267 */
							BgL_objectz00_bglt BgL_arg1807z00_2547;

							{	/* Ast/glo_def.scm 267 */
								obj_t BgL_tmpz00_3564;

								BgL_tmpz00_3564 =
									((obj_t) ((BgL_objectz00_bglt) BgL_oldzd2valuezd2_1897));
								BgL_arg1807z00_2547 = (BgL_objectz00_bglt) (BgL_tmpz00_3564);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Ast/glo_def.scm 267 */
									long BgL_idxz00_2553;

									BgL_idxz00_2553 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2547);
									BgL_test2001z00_3563 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2553 + 2L)) == BgL_classz00_2545);
								}
							else
								{	/* Ast/glo_def.scm 267 */
									bool_t BgL_res1873z00_2578;

									{	/* Ast/glo_def.scm 267 */
										obj_t BgL_oclassz00_2561;

										{	/* Ast/glo_def.scm 267 */
											obj_t BgL_arg1815z00_2569;
											long BgL_arg1816z00_2570;

											BgL_arg1815z00_2569 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Ast/glo_def.scm 267 */
												long BgL_arg1817z00_2571;

												BgL_arg1817z00_2571 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2547);
												BgL_arg1816z00_2570 =
													(BgL_arg1817z00_2571 - OBJECT_TYPE);
											}
											BgL_oclassz00_2561 =
												VECTOR_REF(BgL_arg1815z00_2569, BgL_arg1816z00_2570);
										}
										{	/* Ast/glo_def.scm 267 */
											bool_t BgL__ortest_1115z00_2562;

											BgL__ortest_1115z00_2562 =
												(BgL_classz00_2545 == BgL_oclassz00_2561);
											if (BgL__ortest_1115z00_2562)
												{	/* Ast/glo_def.scm 267 */
													BgL_res1873z00_2578 = BgL__ortest_1115z00_2562;
												}
											else
												{	/* Ast/glo_def.scm 267 */
													long BgL_odepthz00_2563;

													{	/* Ast/glo_def.scm 267 */
														obj_t BgL_arg1804z00_2564;

														BgL_arg1804z00_2564 = (BgL_oclassz00_2561);
														BgL_odepthz00_2563 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2564);
													}
													if ((2L < BgL_odepthz00_2563))
														{	/* Ast/glo_def.scm 267 */
															obj_t BgL_arg1802z00_2566;

															{	/* Ast/glo_def.scm 267 */
																obj_t BgL_arg1803z00_2567;

																BgL_arg1803z00_2567 = (BgL_oclassz00_2561);
																BgL_arg1802z00_2566 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2567,
																	2L);
															}
															BgL_res1873z00_2578 =
																(BgL_arg1802z00_2566 == BgL_classz00_2545);
														}
													else
														{	/* Ast/glo_def.scm 267 */
															BgL_res1873z00_2578 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2001z00_3563 = BgL_res1873z00_2578;
								}
						}
					}
					if (BgL_test2001z00_3563)
						{	/* Ast/glo_def.scm 269 */
							bool_t BgL_test2005z00_3587;

							{	/* Ast/glo_def.scm 269 */
								BgL_typez00_bglt BgL_arg1642z00_1902;

								BgL_arg1642z00_1902 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_oldz00_34))))->BgL_typez00);
								BgL_test2005z00_3587 =
									BGl_compatiblezd2typezf3z21zzast_glozd2defzd2(((bool_t) 0),
									((BgL_typez00_bglt) BgL_typez00_35), BgL_arg1642z00_1902);
							}
							if (BgL_test2005z00_3587)
								{	/* Ast/glo_def.scm 269 */
									return ((BgL_globalz00_bglt) BgL_oldz00_34);
								}
							else
								{	/* Ast/glo_def.scm 270 */
									obj_t BgL_list1631z00_1901;

									BgL_list1631z00_1901 =
										MAKE_YOUNG_PAIR(BGl_string1896z00zzast_glozd2defzd2, BNIL);
									return
										BGl_mismatchzd2errorzd2zzast_glozd2defzd2(
										((BgL_globalz00_bglt) BgL_oldz00_34), BgL_srczd2expzd2_36,
										BgL_list1631z00_1901);
								}
						}
					else
						{	/* Ast/glo_def.scm 268 */
							obj_t BgL_list1643z00_1903;

							BgL_list1643z00_1903 =
								MAKE_YOUNG_PAIR(BGl_string1897z00zzast_glozd2defzd2, BNIL);
							return
								BGl_mismatchzd2errorzd2zzast_glozd2defzd2(
								((BgL_globalz00_bglt) BgL_oldz00_34), BgL_srczd2expzd2_36,
								BgL_list1643z00_1903);
						}
				}
			}
		}

	}



/* compatible-type? */
	bool_t BGl_compatiblezd2typezf3z21zzast_glozd2defzd2(bool_t BgL_subzf3zf3_37,
		BgL_typez00_bglt BgL_newz00_38, BgL_typez00_bglt BgL_oldz00_39)
	{
		{	/* Ast/glo_def.scm 277 */
			{	/* Ast/glo_def.scm 278 */
				bool_t BgL__ortest_1111z00_1904;

				BgL__ortest_1111z00_1904 =
					(((obj_t) BgL_newz00_38) == BGl_za2_za2z00zztype_cachez00);
				if (BgL__ortest_1111z00_1904)
					{	/* Ast/glo_def.scm 278 */
						return BgL__ortest_1111z00_1904;
					}
				else
					{	/* Ast/glo_def.scm 279 */
						bool_t BgL__ortest_1112z00_1905;

						BgL__ortest_1112z00_1905 =
							(((obj_t) BgL_oldz00_39) == ((obj_t) BgL_newz00_38));
						if (BgL__ortest_1112z00_1905)
							{	/* Ast/glo_def.scm 279 */
								return BgL__ortest_1112z00_1905;
							}
						else
							{	/* Ast/glo_def.scm 279 */
								if (BgL_subzf3zf3_37)
									{	/* Ast/glo_def.scm 281 */
										bool_t BgL__ortest_1114z00_1907;

										BgL__ortest_1114z00_1907 =
											BGl_typezd2subclasszf3z21zzobject_classz00(BgL_newz00_38,
											BgL_oldz00_39);
										if (BgL__ortest_1114z00_1907)
											{	/* Ast/glo_def.scm 281 */
												return BgL__ortest_1114z00_1907;
											}
										else
											{	/* Ast/glo_def.scm 282 */
												bool_t BgL_test2010z00_3610;

												{	/* Ast/glo_def.scm 282 */
													obj_t BgL_classz00_2580;

													BgL_classz00_2580 = BGl_tclassz00zzobject_classz00;
													{	/* Ast/glo_def.scm 282 */
														BgL_objectz00_bglt BgL_arg1807z00_2582;

														{	/* Ast/glo_def.scm 282 */
															obj_t BgL_tmpz00_3611;

															BgL_tmpz00_3611 = ((obj_t) BgL_newz00_38);
															BgL_arg1807z00_2582 =
																(BgL_objectz00_bglt) (BgL_tmpz00_3611);
														}
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Ast/glo_def.scm 282 */
																long BgL_idxz00_2588;

																BgL_idxz00_2588 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_2582);
																BgL_test2010z00_3610 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_2588 + 2L)) ==
																	BgL_classz00_2580);
															}
														else
															{	/* Ast/glo_def.scm 282 */
																bool_t BgL_res1874z00_2613;

																{	/* Ast/glo_def.scm 282 */
																	obj_t BgL_oclassz00_2596;

																	{	/* Ast/glo_def.scm 282 */
																		obj_t BgL_arg1815z00_2604;
																		long BgL_arg1816z00_2605;

																		BgL_arg1815z00_2604 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Ast/glo_def.scm 282 */
																			long BgL_arg1817z00_2606;

																			BgL_arg1817z00_2606 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_2582);
																			BgL_arg1816z00_2605 =
																				(BgL_arg1817z00_2606 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_2596 =
																			VECTOR_REF(BgL_arg1815z00_2604,
																			BgL_arg1816z00_2605);
																	}
																	{	/* Ast/glo_def.scm 282 */
																		bool_t BgL__ortest_1115z00_2597;

																		BgL__ortest_1115z00_2597 =
																			(BgL_classz00_2580 == BgL_oclassz00_2596);
																		if (BgL__ortest_1115z00_2597)
																			{	/* Ast/glo_def.scm 282 */
																				BgL_res1874z00_2613 =
																					BgL__ortest_1115z00_2597;
																			}
																		else
																			{	/* Ast/glo_def.scm 282 */
																				long BgL_odepthz00_2598;

																				{	/* Ast/glo_def.scm 282 */
																					obj_t BgL_arg1804z00_2599;

																					BgL_arg1804z00_2599 =
																						(BgL_oclassz00_2596);
																					BgL_odepthz00_2598 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_2599);
																				}
																				if ((2L < BgL_odepthz00_2598))
																					{	/* Ast/glo_def.scm 282 */
																						obj_t BgL_arg1802z00_2601;

																						{	/* Ast/glo_def.scm 282 */
																							obj_t BgL_arg1803z00_2602;

																							BgL_arg1803z00_2602 =
																								(BgL_oclassz00_2596);
																							BgL_arg1802z00_2601 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_2602, 2L);
																						}
																						BgL_res1874z00_2613 =
																							(BgL_arg1802z00_2601 ==
																							BgL_classz00_2580);
																					}
																				else
																					{	/* Ast/glo_def.scm 282 */
																						BgL_res1874z00_2613 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2010z00_3610 = BgL_res1874z00_2613;
															}
													}
												}
												if (BgL_test2010z00_3610)
													{	/* Ast/glo_def.scm 282 */
														return
															(
															((obj_t) BgL_oldz00_39) ==
															BGl_za2objza2z00zztype_cachez00);
													}
												else
													{	/* Ast/glo_def.scm 282 */
														return ((bool_t) 0);
													}
											}
									}
								else
									{	/* Ast/glo_def.scm 280 */
										return ((bool_t) 0);
									}
							}
					}
			}
		}

	}



/* mismatch-error */
	BgL_globalz00_bglt
		BGl_mismatchzd2errorzd2zzast_glozd2defzd2(BgL_globalz00_bglt
		BgL_globalz00_40, obj_t BgL_srczd2expzd2_41, obj_t BgL_addzd2msgzd2_42)
	{
		{	/* Ast/glo_def.scm 287 */
			{	/* Ast/glo_def.scm 289 */
				obj_t BgL_arg1646z00_1910;
				obj_t BgL_arg1650z00_1911;

				if (PAIRP(BgL_addzd2msgzd2_42))
					{	/* Ast/glo_def.scm 289 */
						BgL_arg1646z00_1910 =
							string_append_3(BGl_string1898z00zzast_glozd2defzd2,
							BGl_string1899z00zzast_glozd2defzd2, CAR(BgL_addzd2msgzd2_42));
					}
				else
					{	/* Ast/glo_def.scm 289 */
						BgL_arg1646z00_1910 = BGl_string1898z00zzast_glozd2defzd2;
					}
				BgL_arg1650z00_1911 =
					BGl_shapez00zztools_shapez00(
					(((BgL_globalz00_bglt) COBJECT(BgL_globalz00_40))->BgL_srcz00));
				{	/* Ast/glo_def.scm 289 */
					obj_t BgL_list1651z00_1912;

					BgL_list1651z00_1912 =
						MAKE_YOUNG_PAIR(((obj_t) BgL_globalz00_40), BNIL);
					return
						((BgL_globalz00_bglt)
						BGl_userzd2errorzd2zztools_errorz00(BgL_arg1646z00_1910,
							BgL_arg1650z00_1911, BgL_srczd2expzd2_41, BgL_list1651z00_1912));
				}
			}
		}

	}



/* check-method-definition */
	BGL_EXPORTED_DEF bool_t
		BGl_checkzd2methodzd2definitionz00zzast_glozd2defzd2(obj_t BgL_idz00_45,
		obj_t BgL_argsz00_46, obj_t BgL_localsz00_47, obj_t BgL_srcz00_48)
	{
		{	/* Ast/glo_def.scm 307 */
			{	/* Ast/glo_def.scm 308 */
				obj_t BgL_locz00_1917;

				BgL_locz00_1917 =
					BGl_findzd2locationzd2zztools_locationz00(BgL_srcz00_48);
				{	/* Ast/glo_def.scm 308 */
					BgL_typez00_bglt BgL_typezd2reszd2_1918;

					BgL_typezd2reszd2_1918 =
						BGl_typezd2ofzd2idz00zzast_identz00(BgL_idz00_45, BgL_locz00_1917);
					{	/* Ast/glo_def.scm 309 */
						obj_t BgL_methodzd2idzd2_1919;

						BgL_methodzd2idzd2_1919 =
							BGl_idzd2ofzd2idz00zzast_identz00(BgL_idz00_45, BgL_locz00_1917);
						{	/* Ast/glo_def.scm 310 */
							obj_t BgL_genericz00_1920;

							BgL_genericz00_1920 =
								BGl_findzd2globalzd2zzast_envz00(BgL_idz00_45, BNIL);
							{	/* Ast/glo_def.scm 311 */

								if (NULLP(BgL_argsz00_46))
									{	/* Ast/glo_def.scm 313 */
										BGl_userzd2errorzd2zztools_errorz00(BgL_idz00_45,
											BGl_shapez00zztools_shapez00(BgL_srcz00_48),
											BGl_string1900z00zzast_glozd2defzd2, BNIL);
										return ((bool_t) 1);
									}
								else
									{	/* Ast/glo_def.scm 316 */
										bool_t BgL_test2016z00_3653;

										{	/* Ast/glo_def.scm 316 */
											obj_t BgL_classz00_2620;

											BgL_classz00_2620 = BGl_globalz00zzast_varz00;
											if (BGL_OBJECTP(BgL_genericz00_1920))
												{	/* Ast/glo_def.scm 316 */
													BgL_objectz00_bglt BgL_arg1807z00_2622;

													BgL_arg1807z00_2622 =
														(BgL_objectz00_bglt) (BgL_genericz00_1920);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Ast/glo_def.scm 316 */
															long BgL_idxz00_2628;

															BgL_idxz00_2628 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2622);
															BgL_test2016z00_3653 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_2628 + 2L)) == BgL_classz00_2620);
														}
													else
														{	/* Ast/glo_def.scm 316 */
															bool_t BgL_res1875z00_2653;

															{	/* Ast/glo_def.scm 316 */
																obj_t BgL_oclassz00_2636;

																{	/* Ast/glo_def.scm 316 */
																	obj_t BgL_arg1815z00_2644;
																	long BgL_arg1816z00_2645;

																	BgL_arg1815z00_2644 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Ast/glo_def.scm 316 */
																		long BgL_arg1817z00_2646;

																		BgL_arg1817z00_2646 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2622);
																		BgL_arg1816z00_2645 =
																			(BgL_arg1817z00_2646 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_2636 =
																		VECTOR_REF(BgL_arg1815z00_2644,
																		BgL_arg1816z00_2645);
																}
																{	/* Ast/glo_def.scm 316 */
																	bool_t BgL__ortest_1115z00_2637;

																	BgL__ortest_1115z00_2637 =
																		(BgL_classz00_2620 == BgL_oclassz00_2636);
																	if (BgL__ortest_1115z00_2637)
																		{	/* Ast/glo_def.scm 316 */
																			BgL_res1875z00_2653 =
																				BgL__ortest_1115z00_2637;
																		}
																	else
																		{	/* Ast/glo_def.scm 316 */
																			long BgL_odepthz00_2638;

																			{	/* Ast/glo_def.scm 316 */
																				obj_t BgL_arg1804z00_2639;

																				BgL_arg1804z00_2639 =
																					(BgL_oclassz00_2636);
																				BgL_odepthz00_2638 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_2639);
																			}
																			if ((2L < BgL_odepthz00_2638))
																				{	/* Ast/glo_def.scm 316 */
																					obj_t BgL_arg1802z00_2641;

																					{	/* Ast/glo_def.scm 316 */
																						obj_t BgL_arg1803z00_2642;

																						BgL_arg1803z00_2642 =
																							(BgL_oclassz00_2636);
																						BgL_arg1802z00_2641 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_2642, 2L);
																					}
																					BgL_res1875z00_2653 =
																						(BgL_arg1802z00_2641 ==
																						BgL_classz00_2620);
																				}
																			else
																				{	/* Ast/glo_def.scm 316 */
																					BgL_res1875z00_2653 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2016z00_3653 = BgL_res1875z00_2653;
														}
												}
											else
												{	/* Ast/glo_def.scm 316 */
													BgL_test2016z00_3653 = ((bool_t) 0);
												}
										}
										if (BgL_test2016z00_3653)
											{	/* Ast/glo_def.scm 321 */
												BgL_valuez00_bglt BgL_genericzd2valuezd2_1925;

												BgL_genericzd2valuezd2_1925 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_globalz00_bglt) BgL_genericz00_1920))))->
													BgL_valuez00);
												{	/* Ast/glo_def.scm 323 */
													bool_t BgL_test2021z00_3679;

													{	/* Ast/glo_def.scm 323 */
														obj_t BgL_classz00_2655;

														BgL_classz00_2655 = BGl_sfunz00zzast_varz00;
														{	/* Ast/glo_def.scm 323 */
															BgL_objectz00_bglt BgL_arg1807z00_2657;

															{	/* Ast/glo_def.scm 323 */
																obj_t BgL_tmpz00_3680;

																BgL_tmpz00_3680 =
																	((obj_t)
																	((BgL_objectz00_bglt)
																		BgL_genericzd2valuezd2_1925));
																BgL_arg1807z00_2657 =
																	(BgL_objectz00_bglt) (BgL_tmpz00_3680);
															}
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Ast/glo_def.scm 323 */
																	long BgL_idxz00_2663;

																	BgL_idxz00_2663 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_2657);
																	BgL_test2021z00_3679 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_2663 + 3L)) ==
																		BgL_classz00_2655);
																}
															else
																{	/* Ast/glo_def.scm 323 */
																	bool_t BgL_res1876z00_2688;

																	{	/* Ast/glo_def.scm 323 */
																		obj_t BgL_oclassz00_2671;

																		{	/* Ast/glo_def.scm 323 */
																			obj_t BgL_arg1815z00_2679;
																			long BgL_arg1816z00_2680;

																			BgL_arg1815z00_2679 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Ast/glo_def.scm 323 */
																				long BgL_arg1817z00_2681;

																				BgL_arg1817z00_2681 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_2657);
																				BgL_arg1816z00_2680 =
																					(BgL_arg1817z00_2681 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_2671 =
																				VECTOR_REF(BgL_arg1815z00_2679,
																				BgL_arg1816z00_2680);
																		}
																		{	/* Ast/glo_def.scm 323 */
																			bool_t BgL__ortest_1115z00_2672;

																			BgL__ortest_1115z00_2672 =
																				(BgL_classz00_2655 ==
																				BgL_oclassz00_2671);
																			if (BgL__ortest_1115z00_2672)
																				{	/* Ast/glo_def.scm 323 */
																					BgL_res1876z00_2688 =
																						BgL__ortest_1115z00_2672;
																				}
																			else
																				{	/* Ast/glo_def.scm 323 */
																					long BgL_odepthz00_2673;

																					{	/* Ast/glo_def.scm 323 */
																						obj_t BgL_arg1804z00_2674;

																						BgL_arg1804z00_2674 =
																							(BgL_oclassz00_2671);
																						BgL_odepthz00_2673 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_2674);
																					}
																					if ((3L < BgL_odepthz00_2673))
																						{	/* Ast/glo_def.scm 323 */
																							obj_t BgL_arg1802z00_2676;

																							{	/* Ast/glo_def.scm 323 */
																								obj_t BgL_arg1803z00_2677;

																								BgL_arg1803z00_2677 =
																									(BgL_oclassz00_2671);
																								BgL_arg1802z00_2676 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_2677, 3L);
																							}
																							BgL_res1876z00_2688 =
																								(BgL_arg1802z00_2676 ==
																								BgL_classz00_2655);
																						}
																					else
																						{	/* Ast/glo_def.scm 323 */
																							BgL_res1876z00_2688 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2021z00_3679 = BgL_res1876z00_2688;
																}
														}
													}
													if (BgL_test2021z00_3679)
														{	/* Ast/glo_def.scm 323 */
															if (
																((((BgL_sfunz00_bglt) COBJECT(
																				((BgL_sfunz00_bglt)
																					BgL_genericzd2valuezd2_1925)))->
																		BgL_classz00) == CNST_TABLE_REF(3)))
																{	/* Ast/glo_def.scm 329 */
																	bool_t BgL_test2026z00_3708;

																	{	/* Ast/glo_def.scm 329 */
																		long BgL_arg1733z00_1974;
																		long BgL_arg1734z00_1975;

																		BgL_arg1733z00_1974 =
																			(((BgL_funz00_bglt) COBJECT(
																					((BgL_funz00_bglt)
																						((BgL_sfunz00_bglt)
																							BgL_genericzd2valuezd2_1925))))->
																			BgL_arityz00);
																		BgL_arg1734z00_1975 =
																			BGl_globalzd2arityzd2zztools_argsz00
																			(BgL_argsz00_46);
																		BgL_test2026z00_3708 =
																			(BgL_arg1733z00_1974 ==
																			BgL_arg1734z00_1975);
																	}
																	if (BgL_test2026z00_3708)
																		{	/* Ast/glo_def.scm 332 */
																			bool_t BgL_test2027z00_3714;

																			{	/* Ast/glo_def.scm 332 */
																				BgL_typez00_bglt BgL_arg1724z00_1972;

																				BgL_arg1724z00_1972 =
																					(((BgL_variablez00_bglt) COBJECT(
																							((BgL_variablez00_bglt)
																								((BgL_globalz00_bglt)
																									BgL_genericz00_1920))))->
																					BgL_typez00);
																				BgL_test2027z00_3714 =
																					BGl_compatiblezd2typezf3z21zzast_glozd2defzd2
																					(((bool_t) 1), BgL_typezd2reszd2_1918,
																					BgL_arg1724z00_1972);
																			}
																			if (BgL_test2027z00_3714)
																				{	/* Ast/glo_def.scm 337 */
																					obj_t BgL__ortest_1116z00_1934;

																					{	/* Ast/glo_def.scm 337 */
																						obj_t BgL_g1117z00_1935;

																						{	/* Ast/glo_def.scm 338 */
																							obj_t BgL_l1260z00_1955;

																							BgL_l1260z00_1955 =
																								(((BgL_sfunz00_bglt) COBJECT(
																										((BgL_sfunz00_bglt)
																											BgL_genericzd2valuezd2_1925)))->
																								BgL_argsz00);
																							if (NULLP(BgL_l1260z00_1955))
																								{	/* Ast/glo_def.scm 338 */
																									BgL_g1117z00_1935 = BNIL;
																								}
																							else
																								{	/* Ast/glo_def.scm 338 */
																									obj_t BgL_head1262z00_1957;

																									BgL_head1262z00_1957 =
																										MAKE_YOUNG_PAIR(BNIL, BNIL);
																									{
																										obj_t BgL_l1260z00_1959;
																										obj_t BgL_tail1263z00_1960;

																										BgL_l1260z00_1959 =
																											BgL_l1260z00_1955;
																										BgL_tail1263z00_1960 =
																											BgL_head1262z00_1957;
																									BgL_zc3z04anonymousza31712ze3z87_1961:
																										if (NULLP
																											(BgL_l1260z00_1959))
																											{	/* Ast/glo_def.scm 338 */
																												BgL_g1117z00_1935 =
																													CDR
																													(BgL_head1262z00_1957);
																											}
																										else
																											{	/* Ast/glo_def.scm 338 */
																												obj_t
																													BgL_newtail1264z00_1963;
																												{	/* Ast/glo_def.scm 338 */
																													obj_t
																														BgL_arg1718z00_1965;
																													{	/* Ast/glo_def.scm 338 */
																														obj_t BgL_az00_1966;

																														BgL_az00_1966 =
																															CAR(
																															((obj_t)
																																BgL_l1260z00_1959));
																														{	/* Ast/glo_def.scm 340 */
																															bool_t
																																BgL_test2030z00_3729;
																															{	/* Ast/glo_def.scm 340 */
																																obj_t
																																	BgL_classz00_2697;
																																BgL_classz00_2697
																																	=
																																	BGl_localz00zzast_varz00;
																																if (BGL_OBJECTP
																																	(BgL_az00_1966))
																																	{	/* Ast/glo_def.scm 340 */
																																		BgL_objectz00_bglt
																																			BgL_arg1807z00_2699;
																																		BgL_arg1807z00_2699
																																			=
																																			(BgL_objectz00_bglt)
																																			(BgL_az00_1966);
																																		if (BGL_CONDEXPAND_ISA_ARCH64())
																																			{	/* Ast/glo_def.scm 340 */
																																				long
																																					BgL_idxz00_2705;
																																				BgL_idxz00_2705
																																					=
																																					BGL_OBJECT_INHERITANCE_NUM
																																					(BgL_arg1807z00_2699);
																																				BgL_test2030z00_3729
																																					=
																																					(VECTOR_REF
																																					(BGl_za2inheritancesza2z00zz__objectz00,
																																						(BgL_idxz00_2705
																																							+
																																							2L))
																																					==
																																					BgL_classz00_2697);
																																			}
																																		else
																																			{	/* Ast/glo_def.scm 340 */
																																				bool_t
																																					BgL_res1877z00_2730;
																																				{	/* Ast/glo_def.scm 340 */
																																					obj_t
																																						BgL_oclassz00_2713;
																																					{	/* Ast/glo_def.scm 340 */
																																						obj_t
																																							BgL_arg1815z00_2721;
																																						long
																																							BgL_arg1816z00_2722;
																																						BgL_arg1815z00_2721
																																							=
																																							(BGl_za2classesza2z00zz__objectz00);
																																						{	/* Ast/glo_def.scm 340 */
																																							long
																																								BgL_arg1817z00_2723;
																																							BgL_arg1817z00_2723
																																								=
																																								BGL_OBJECT_CLASS_NUM
																																								(BgL_arg1807z00_2699);
																																							BgL_arg1816z00_2722
																																								=
																																								(BgL_arg1817z00_2723
																																								-
																																								OBJECT_TYPE);
																																						}
																																						BgL_oclassz00_2713
																																							=
																																							VECTOR_REF
																																							(BgL_arg1815z00_2721,
																																							BgL_arg1816z00_2722);
																																					}
																																					{	/* Ast/glo_def.scm 340 */
																																						bool_t
																																							BgL__ortest_1115z00_2714;
																																						BgL__ortest_1115z00_2714
																																							=
																																							(BgL_classz00_2697
																																							==
																																							BgL_oclassz00_2713);
																																						if (BgL__ortest_1115z00_2714)
																																							{	/* Ast/glo_def.scm 340 */
																																								BgL_res1877z00_2730
																																									=
																																									BgL__ortest_1115z00_2714;
																																							}
																																						else
																																							{	/* Ast/glo_def.scm 340 */
																																								long
																																									BgL_odepthz00_2715;
																																								{	/* Ast/glo_def.scm 340 */
																																									obj_t
																																										BgL_arg1804z00_2716;
																																									BgL_arg1804z00_2716
																																										=
																																										(BgL_oclassz00_2713);
																																									BgL_odepthz00_2715
																																										=
																																										BGL_CLASS_DEPTH
																																										(BgL_arg1804z00_2716);
																																								}
																																								if ((2L < BgL_odepthz00_2715))
																																									{	/* Ast/glo_def.scm 340 */
																																										obj_t
																																											BgL_arg1802z00_2718;
																																										{	/* Ast/glo_def.scm 340 */
																																											obj_t
																																												BgL_arg1803z00_2719;
																																											BgL_arg1803z00_2719
																																												=
																																												(BgL_oclassz00_2713);
																																											BgL_arg1802z00_2718
																																												=
																																												BGL_CLASS_ANCESTORS_REF
																																												(BgL_arg1803z00_2719,
																																												2L);
																																										}
																																										BgL_res1877z00_2730
																																											=
																																											(BgL_arg1802z00_2718
																																											==
																																											BgL_classz00_2697);
																																									}
																																								else
																																									{	/* Ast/glo_def.scm 340 */
																																										BgL_res1877z00_2730
																																											=
																																											(
																																											(bool_t)
																																											0);
																																									}
																																							}
																																					}
																																				}
																																				BgL_test2030z00_3729
																																					=
																																					BgL_res1877z00_2730;
																																			}
																																	}
																																else
																																	{	/* Ast/glo_def.scm 340 */
																																		BgL_test2030z00_3729
																																			=
																																			((bool_t)
																																			0);
																																	}
																															}
																															if (BgL_test2030z00_3729)
																																{	/* Ast/glo_def.scm 340 */
																																	BgL_arg1718z00_1965
																																		=
																																		((obj_t) ((
																																				(BgL_variablez00_bglt)
																																				COBJECT(
																																					((BgL_variablez00_bglt) ((BgL_localz00_bglt) BgL_az00_1966))))->BgL_typez00));
																																}
																															else
																																{	/* Ast/glo_def.scm 342 */
																																	bool_t
																																		BgL_test2035z00_3756;
																																	{	/* Ast/glo_def.scm 342 */
																																		obj_t
																																			BgL_classz00_2732;
																																		BgL_classz00_2732
																																			=
																																			BGl_typez00zztype_typez00;
																																		if (BGL_OBJECTP(BgL_az00_1966))
																																			{	/* Ast/glo_def.scm 342 */
																																				BgL_objectz00_bglt
																																					BgL_arg1807z00_2734;
																																				BgL_arg1807z00_2734
																																					=
																																					(BgL_objectz00_bglt)
																																					(BgL_az00_1966);
																																				if (BGL_CONDEXPAND_ISA_ARCH64())
																																					{	/* Ast/glo_def.scm 342 */
																																						long
																																							BgL_idxz00_2740;
																																						BgL_idxz00_2740
																																							=
																																							BGL_OBJECT_INHERITANCE_NUM
																																							(BgL_arg1807z00_2734);
																																						BgL_test2035z00_3756
																																							=
																																							(VECTOR_REF
																																							(BGl_za2inheritancesza2z00zz__objectz00,
																																								(BgL_idxz00_2740
																																									+
																																									1L))
																																							==
																																							BgL_classz00_2732);
																																					}
																																				else
																																					{	/* Ast/glo_def.scm 342 */
																																						bool_t
																																							BgL_res1878z00_2765;
																																						{	/* Ast/glo_def.scm 342 */
																																							obj_t
																																								BgL_oclassz00_2748;
																																							{	/* Ast/glo_def.scm 342 */
																																								obj_t
																																									BgL_arg1815z00_2756;
																																								long
																																									BgL_arg1816z00_2757;
																																								BgL_arg1815z00_2756
																																									=
																																									(BGl_za2classesza2z00zz__objectz00);
																																								{	/* Ast/glo_def.scm 342 */
																																									long
																																										BgL_arg1817z00_2758;
																																									BgL_arg1817z00_2758
																																										=
																																										BGL_OBJECT_CLASS_NUM
																																										(BgL_arg1807z00_2734);
																																									BgL_arg1816z00_2757
																																										=
																																										(BgL_arg1817z00_2758
																																										-
																																										OBJECT_TYPE);
																																								}
																																								BgL_oclassz00_2748
																																									=
																																									VECTOR_REF
																																									(BgL_arg1815z00_2756,
																																									BgL_arg1816z00_2757);
																																							}
																																							{	/* Ast/glo_def.scm 342 */
																																								bool_t
																																									BgL__ortest_1115z00_2749;
																																								BgL__ortest_1115z00_2749
																																									=
																																									(BgL_classz00_2732
																																									==
																																									BgL_oclassz00_2748);
																																								if (BgL__ortest_1115z00_2749)
																																									{	/* Ast/glo_def.scm 342 */
																																										BgL_res1878z00_2765
																																											=
																																											BgL__ortest_1115z00_2749;
																																									}
																																								else
																																									{	/* Ast/glo_def.scm 342 */
																																										long
																																											BgL_odepthz00_2750;
																																										{	/* Ast/glo_def.scm 342 */
																																											obj_t
																																												BgL_arg1804z00_2751;
																																											BgL_arg1804z00_2751
																																												=
																																												(BgL_oclassz00_2748);
																																											BgL_odepthz00_2750
																																												=
																																												BGL_CLASS_DEPTH
																																												(BgL_arg1804z00_2751);
																																										}
																																										if ((1L < BgL_odepthz00_2750))
																																											{	/* Ast/glo_def.scm 342 */
																																												obj_t
																																													BgL_arg1802z00_2753;
																																												{	/* Ast/glo_def.scm 342 */
																																													obj_t
																																														BgL_arg1803z00_2754;
																																													BgL_arg1803z00_2754
																																														=
																																														(BgL_oclassz00_2748);
																																													BgL_arg1802z00_2753
																																														=
																																														BGL_CLASS_ANCESTORS_REF
																																														(BgL_arg1803z00_2754,
																																														1L);
																																												}
																																												BgL_res1878z00_2765
																																													=
																																													(BgL_arg1802z00_2753
																																													==
																																													BgL_classz00_2732);
																																											}
																																										else
																																											{	/* Ast/glo_def.scm 342 */
																																												BgL_res1878z00_2765
																																													=
																																													(
																																													(bool_t)
																																													0);
																																											}
																																									}
																																							}
																																						}
																																						BgL_test2035z00_3756
																																							=
																																							BgL_res1878z00_2765;
																																					}
																																			}
																																		else
																																			{	/* Ast/glo_def.scm 342 */
																																				BgL_test2035z00_3756
																																					=
																																					(
																																					(bool_t)
																																					0);
																																			}
																																	}
																																	if (BgL_test2035z00_3756)
																																		{	/* Ast/glo_def.scm 342 */
																																			BgL_arg1718z00_1965
																																				=
																																				BgL_az00_1966;
																																		}
																																	else
																																		{	/* Ast/glo_def.scm 342 */
																																			BgL_arg1718z00_1965
																																				=
																																				BGl_internalzd2errorzd2zztools_errorz00
																																				(BGl_string1885z00zzast_glozd2defzd2,
																																				BGl_string1886z00zzast_glozd2defzd2,
																																				BGl_shapez00zztools_shapez00
																																				(BgL_az00_1966));
																																		}
																																}
																														}
																													}
																													BgL_newtail1264z00_1963
																														=
																														MAKE_YOUNG_PAIR
																														(BgL_arg1718z00_1965,
																														BNIL);
																												}
																												SET_CDR
																													(BgL_tail1263z00_1960,
																													BgL_newtail1264z00_1963);
																												{	/* Ast/glo_def.scm 338 */
																													obj_t
																														BgL_arg1717z00_1964;
																													BgL_arg1717z00_1964 =
																														CDR(((obj_t)
																															BgL_l1260z00_1959));
																													{
																														obj_t
																															BgL_tail1263z00_3786;
																														obj_t
																															BgL_l1260z00_3785;
																														BgL_l1260z00_3785 =
																															BgL_arg1717z00_1964;
																														BgL_tail1263z00_3786
																															=
																															BgL_newtail1264z00_1963;
																														BgL_tail1263z00_1960
																															=
																															BgL_tail1263z00_3786;
																														BgL_l1260z00_1959 =
																															BgL_l1260z00_3785;
																														goto
																															BgL_zc3z04anonymousza31712ze3z87_1961;
																													}
																												}
																											}
																									}
																								}
																						}
																						{
																							obj_t BgL_localsz00_1937;
																							obj_t BgL_typesz00_1938;
																							bool_t BgL_subzf3zf3_1939;

																							BgL_localsz00_1937 =
																								BgL_localsz00_47;
																							BgL_typesz00_1938 =
																								BgL_g1117z00_1935;
																							BgL_subzf3zf3_1939 = ((bool_t) 1);
																						BgL_zc3z04anonymousza31693ze3z87_1940:
																							if (NULLP
																								(BgL_localsz00_1937))
																								{	/* Ast/glo_def.scm 352 */
																									BgL__ortest_1116z00_1934 =
																										BTRUE;
																								}
																							else
																								{	/* Ast/glo_def.scm 352 */
																									if (NULLP(BgL_typesz00_1938))
																										{	/* Ast/glo_def.scm 355 */
																											obj_t
																												BgL_list1696z00_1943;
																											BgL_list1696z00_1943 =
																												MAKE_YOUNG_PAIR
																												(BGl_string1891z00zzast_glozd2defzd2,
																												BNIL);
																											BgL__ortest_1116z00_1934 =
																												((obj_t)
																												BGl_mismatchzd2errorzd2zzast_glozd2defzd2
																												(((BgL_globalz00_bglt)
																														BgL_genericz00_1920),
																													BgL_srcz00_48,
																													BgL_list1696z00_1943));
																										}
																									else
																										{	/* Ast/glo_def.scm 358 */
																											bool_t
																												BgL_test2042z00_3795;
																											{	/* Ast/glo_def.scm 359 */
																												BgL_typez00_bglt
																													BgL_arg1708z00_1951;
																												obj_t
																													BgL_arg1709z00_1952;
																												BgL_arg1708z00_1951 =
																													(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt) CAR(((obj_t) BgL_localsz00_1937))))))->BgL_typez00);
																												BgL_arg1709z00_1952 =
																													CAR(((obj_t)
																														BgL_typesz00_1938));
																												BgL_test2042z00_3795 =
																													BGl_compatiblezd2typezf3z21zzast_glozd2defzd2
																													(BgL_subzf3zf3_1939,
																													BgL_arg1708z00_1951,
																													((BgL_typez00_bglt)
																														BgL_arg1709z00_1952));
																											}
																											if (BgL_test2042z00_3795)
																												{	/* Ast/glo_def.scm 366 */
																													obj_t
																														BgL_arg1703z00_1948;
																													obj_t
																														BgL_arg1705z00_1949;
																													BgL_arg1703z00_1948 =
																														CDR(((obj_t)
																															BgL_localsz00_1937));
																													BgL_arg1705z00_1949 =
																														CDR(((obj_t)
																															BgL_typesz00_1938));
																													{
																														bool_t
																															BgL_subzf3zf3_3811;
																														obj_t
																															BgL_typesz00_3810;
																														obj_t
																															BgL_localsz00_3809;
																														BgL_localsz00_3809 =
																															BgL_arg1703z00_1948;
																														BgL_typesz00_3810 =
																															BgL_arg1705z00_1949;
																														BgL_subzf3zf3_3811 =
																															((bool_t) 0);
																														BgL_subzf3zf3_1939 =
																															BgL_subzf3zf3_3811;
																														BgL_typesz00_1938 =
																															BgL_typesz00_3810;
																														BgL_localsz00_1937 =
																															BgL_localsz00_3809;
																														goto
																															BgL_zc3z04anonymousza31693ze3z87_1940;
																													}
																												}
																											else
																												{	/* Ast/glo_def.scm 358 */
																													{	/* Ast/glo_def.scm 361 */
																														obj_t
																															BgL_list1706z00_1950;
																														BgL_list1706z00_1950
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_string1891z00zzast_glozd2defzd2,
																															BNIL);
																														BGl_mismatchzd2errorzd2zzast_glozd2defzd2
																															(((BgL_globalz00_bglt) BgL_genericz00_1920), BgL_srcz00_48, BgL_list1706z00_1950);
																													}
																													BgL__ortest_1116z00_1934
																														= BFALSE;
																												}
																										}
																								}
																						}
																					}
																					if (CBOOL(BgL__ortest_1116z00_1934))
																						{	/* Ast/glo_def.scm 337 */
																							return
																								CBOOL(BgL__ortest_1116z00_1934);
																						}
																					else
																						{	/* Ast/glo_def.scm 337 */
																							return ((bool_t) 1);
																						}
																				}
																			else
																				{	/* Ast/glo_def.scm 332 */
																					{	/* Ast/glo_def.scm 333 */
																						obj_t BgL_list1723z00_1971;

																						BgL_list1723z00_1971 =
																							MAKE_YOUNG_PAIR
																							(BGl_string1892z00zzast_glozd2defzd2,
																							BNIL);
																						BGl_mismatchzd2errorzd2zzast_glozd2defzd2
																							(((BgL_globalz00_bglt)
																								BgL_genericz00_1920),
																							BgL_srcz00_48,
																							BgL_list1723z00_1971);
																					}
																					return ((bool_t) 0);
																				}
																		}
																	else
																		{	/* Ast/glo_def.scm 329 */
																			{	/* Ast/glo_def.scm 330 */
																				obj_t BgL_list1725z00_1973;

																				BgL_list1725z00_1973 =
																					MAKE_YOUNG_PAIR
																					(BGl_string1890z00zzast_glozd2defzd2,
																					BNIL);
																				BGl_mismatchzd2errorzd2zzast_glozd2defzd2
																					(((BgL_globalz00_bglt)
																						BgL_genericz00_1920), BgL_srcz00_48,
																					BgL_list1725z00_1973);
																			}
																			return ((bool_t) 0);
																		}
																}
															else
																{	/* Ast/glo_def.scm 326 */
																	{	/* Ast/glo_def.scm 327 */
																		obj_t BgL_list1735z00_1976;

																		BgL_list1735z00_1976 =
																			MAKE_YOUNG_PAIR
																			(BGl_string1901z00zzast_glozd2defzd2,
																			BNIL);
																		BGl_mismatchzd2errorzd2zzast_glozd2defzd2((
																				(BgL_globalz00_bglt)
																				BgL_genericz00_1920), BgL_srcz00_48,
																			BgL_list1735z00_1976);
																	}
																	return ((bool_t) 0);
																}
														}
													else
														{	/* Ast/glo_def.scm 323 */
															{	/* Ast/glo_def.scm 324 */
																obj_t BgL_list1737z00_1978;

																BgL_list1737z00_1978 =
																	MAKE_YOUNG_PAIR
																	(BGl_string1901z00zzast_glozd2defzd2, BNIL);
																BGl_mismatchzd2errorzd2zzast_glozd2defzd2((
																		(BgL_globalz00_bglt) BgL_genericz00_1920),
																	BgL_srcz00_48, BgL_list1737z00_1978);
															}
															return ((bool_t) 0);
														}
												}
											}
										else
											{	/* Ast/glo_def.scm 316 */
												return ((bool_t) 1);
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* &check-method-definition */
	obj_t BGl_z62checkzd2methodzd2definitionz62zzast_glozd2defzd2(obj_t
		BgL_envz00_3006, obj_t BgL_idz00_3007, obj_t BgL_argsz00_3008,
		obj_t BgL_localsz00_3009, obj_t BgL_srcz00_3010)
	{
		{	/* Ast/glo_def.scm 307 */
			return
				BBOOL(BGl_checkzd2methodzd2definitionz00zzast_glozd2defzd2
				(BgL_idz00_3007, BgL_argsz00_3008, BgL_localsz00_3009,
					BgL_srcz00_3010));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_glozd2defzd2(void)
	{
		{	/* Ast/glo_def.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_glozd2defzd2(void)
	{
		{	/* Ast/glo_def.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_glozd2defzd2(void)
	{
		{	/* Ast/glo_def.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_glozd2defzd2(void)
	{
		{	/* Ast/glo_def.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1902z00zzast_glozd2defzd2));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1902z00zzast_glozd2defzd2));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1902z00zzast_glozd2defzd2));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1902z00zzast_glozd2defzd2));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1902z00zzast_glozd2defzd2));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1902z00zzast_glozd2defzd2));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1902z00zzast_glozd2defzd2));
			BGl_modulezd2initializa7ationz75zzast_glozd2declzd2(374700232L,
				BSTRING_TO_STRING(BGl_string1902z00zzast_glozd2defzd2));
			BGl_modulezd2initializa7ationz75zzast_removez00(383247627L,
				BSTRING_TO_STRING(BGl_string1902z00zzast_glozd2defzd2));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1902z00zzast_glozd2defzd2));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1902z00zzast_glozd2defzd2));
			BGl_modulezd2initializa7ationz75zztools_argsz00(47102372L,
				BSTRING_TO_STRING(BGl_string1902z00zzast_glozd2defzd2));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1902z00zzast_glozd2defzd2));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1902z00zzast_glozd2defzd2));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1902z00zzast_glozd2defzd2));
			BGl_modulezd2initializa7ationz75zztools_dssslz00(275867955L,
				BSTRING_TO_STRING(BGl_string1902z00zzast_glozd2defzd2));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1902z00zzast_glozd2defzd2));
			return
				BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1902z00zzast_glozd2defzd2));
		}

	}

#ifdef __cplusplus
}
#endif
