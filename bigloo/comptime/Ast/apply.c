/*===========================================================================*/
/*   (Ast/apply.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/apply.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_AST_APPLY_TYPE_DEFINITIONS
#define BGL_AST_APPLY_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;


#endif													// BGL_AST_APPLY_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_za2unsafezd2arityza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_z62makezd2funzd2framezd2sfun1279zb0zzast_applyz00(obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzast_applyz00 = BUNSPEC;
	extern obj_t BGl_funz00zzast_varz00;
	static obj_t BGl_makezd2funzd2framez00zzast_applyz00(BgL_funz00_bglt);
	extern obj_t BGl_findzd2locationzf2locz20zztools_locationz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern BgL_nodez00_bglt BGl_applicationzd2ze3nodez31zzast_appz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_applycationzd2ze3nodez31zzast_applyz00(obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t BGl_toplevelzd2initzd2zzast_applyz00(void);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzast_applyz00(void);
	static BgL_letzd2varzd2_bglt
		BGl_vazd2knownzd2appzd2lyzd2ze3nodeze3zzast_applyz00(obj_t, obj_t,
		BgL_nodez00_bglt, BgL_nodez00_bglt, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzast_applyz00(void);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzast_applyz00(void);
	static BgL_nodez00_bglt
		BGl_z62knownzd2appzd2lyzd2ze3nodez53zzast_applyz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62makezd2funzd2frame1275z62zzast_applyz00(obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern bool_t BGl_globalzd2optionalzf3z21zzast_varz00(obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	static BgL_nodez00_bglt BGl_z62applycationzd2ze3nodez53zzast_applyz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern BgL_typez00_bglt
		BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_z62makezd2funzd2framezd2cfun1281zb0zzast_applyz00(obj_t,
		obj_t);
	extern bool_t BGl_globalzd2keyzf3z21zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzast_applyz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_appz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_cfunz00zzast_varz00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzast_applyz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_applyz00(void);
	extern BgL_localz00_bglt BGl_makezd2localzd2svarz00zzast_localz00(obj_t,
		BgL_typez00_bglt);
	extern BgL_nodez00_bglt BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzast_applyz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzast_applyz00(void);
	extern BgL_nodez00_bglt BGl_sexpzd2ze3nodez31zzast_sexpz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_knownzd2appzd2lyzd2ze3nodez31zzast_applyz00(obj_t, obj_t,
		BgL_nodez00_bglt, BgL_nodez00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_letzd2varzd2_bglt
		BGl_fxzd2knownzd2appzd2lyzd2ze3nodeze3zzast_applyz00(obj_t, obj_t,
		BgL_nodez00_bglt, BgL_nodez00_bglt, obj_t, obj_t);
	extern obj_t BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00(obj_t);
	static BgL_nodezf2effectzf2_bglt
		BGl_loopze70ze7zzast_applyz00(BgL_nodez00_bglt, obj_t, obj_t,
		BgL_localz00_bglt, BgL_typez00_bglt, obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_loopze71ze7zzast_applyz00(obj_t, BgL_nodez00_bglt,
		obj_t, obj_t, BgL_localz00_bglt, BgL_typez00_bglt, obj_t, obj_t);
	static obj_t BGl_z62makezd2funzd2framez62zzast_applyz00(obj_t, obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t __cnst[14];


	   
		 
		DEFINE_STRING(BGl_string1918z00zzast_applyz00,
		BgL_bgl_string1918za700za7za7a1930za7, "Illegal `apply' form", 20);
	      DEFINE_STRING(BGl_string1919z00zzast_applyz00,
		BgL_bgl_string1919za700za7za7a1931za7, "Too many arguments provided", 27);
	      DEFINE_STRING(BGl_string1920z00zzast_applyz00,
		BgL_bgl_string1920za700za7za7a1932za7, "apply", 5);
	      DEFINE_STRING(BGl_string1922z00zzast_applyz00,
		BgL_bgl_string1922za700za7za7a1933za7, "make-fun-frame1275", 18);
	      DEFINE_STRING(BGl_string1923z00zzast_applyz00,
		BgL_bgl_string1923za700za7za7a1934za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string1925z00zzast_applyz00,
		BgL_bgl_string1925za700za7za7a1935za7, "make-fun-frame", 14);
	      DEFINE_STRING(BGl_string1927z00zzast_applyz00,
		BgL_bgl_string1927za700za7za7a1936za7, "ast_apply", 9);
	      DEFINE_STRING(BGl_string1928z00zzast_applyz00,
		BgL_bgl_string1928za700za7za7a1937za7,
		"aux make-fun-frame1275 begin set! car if failure quote null? cdr write runner value apply ",
		90);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1921z00zzast_applyz00,
		BgL_bgl_za762makeza7d2funza7d21938za7,
		BGl_z62makezd2funzd2frame1275z62zzast_applyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1924z00zzast_applyz00,
		BgL_bgl_za762makeza7d2funza7d21939za7,
		BGl_z62makezd2funzd2framezd2sfun1279zb0zzast_applyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1926z00zzast_applyz00,
		BgL_bgl_za762makeza7d2funza7d21940za7,
		BGl_z62makezd2funzd2framezd2cfun1281zb0zzast_applyz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_knownzd2appzd2lyzd2ze3nodezd2envze3zzast_applyz00,
		BgL_bgl_za762knownza7d2appza7d1941za7,
		BGl_z62knownzd2appzd2lyzd2ze3nodez53zzast_applyz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_GENERIC(BGl_makezd2funzd2framezd2envzd2zzast_applyz00,
		BgL_bgl_za762makeza7d2funza7d21942za7,
		BGl_z62makezd2funzd2framez62zzast_applyz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_applycationzd2ze3nodezd2envze3zzast_applyz00,
		BgL_bgl_za762applycationza7d1943z00,
		BGl_z62applycationzd2ze3nodez53zzast_applyz00, 0L, BUNSPEC, 4);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzast_applyz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzast_applyz00(long
		BgL_checksumz00_2368, char *BgL_fromz00_2369)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_applyz00))
				{
					BGl_requirezd2initializa7ationz75zzast_applyz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_applyz00();
					BGl_libraryzd2moduleszd2initz00zzast_applyz00();
					BGl_cnstzd2initzd2zzast_applyz00();
					BGl_importedzd2moduleszd2initz00zzast_applyz00();
					BGl_genericzd2initzd2zzast_applyz00();
					BGl_methodzd2initzd2zzast_applyz00();
					return BGl_toplevelzd2initzd2zzast_applyz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_applyz00(void)
	{
		{	/* Ast/apply.scm 14 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_apply");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "ast_apply");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_apply");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "ast_apply");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "ast_apply");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "ast_apply");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "ast_apply");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "ast_apply");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"ast_apply");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "ast_apply");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"ast_apply");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_applyz00(void)
	{
		{	/* Ast/apply.scm 14 */
			{	/* Ast/apply.scm 14 */
				obj_t BgL_cportz00_2318;

				{	/* Ast/apply.scm 14 */
					obj_t BgL_stringz00_2325;

					BgL_stringz00_2325 = BGl_string1928z00zzast_applyz00;
					{	/* Ast/apply.scm 14 */
						obj_t BgL_startz00_2326;

						BgL_startz00_2326 = BINT(0L);
						{	/* Ast/apply.scm 14 */
							obj_t BgL_endz00_2327;

							BgL_endz00_2327 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2325)));
							{	/* Ast/apply.scm 14 */

								BgL_cportz00_2318 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2325, BgL_startz00_2326, BgL_endz00_2327);
				}}}}
				{
					long BgL_iz00_2319;

					BgL_iz00_2319 = 13L;
				BgL_loopz00_2320:
					if ((BgL_iz00_2319 == -1L))
						{	/* Ast/apply.scm 14 */
							return BUNSPEC;
						}
					else
						{	/* Ast/apply.scm 14 */
							{	/* Ast/apply.scm 14 */
								obj_t BgL_arg1929z00_2321;

								{	/* Ast/apply.scm 14 */

									{	/* Ast/apply.scm 14 */
										obj_t BgL_locationz00_2323;

										BgL_locationz00_2323 = BBOOL(((bool_t) 0));
										{	/* Ast/apply.scm 14 */

											BgL_arg1929z00_2321 =
												BGl_readz00zz__readerz00(BgL_cportz00_2318,
												BgL_locationz00_2323);
										}
									}
								}
								{	/* Ast/apply.scm 14 */
									int BgL_tmpz00_2400;

									BgL_tmpz00_2400 = (int) (BgL_iz00_2319);
									CNST_TABLE_SET(BgL_tmpz00_2400, BgL_arg1929z00_2321);
							}}
							{	/* Ast/apply.scm 14 */
								int BgL_auxz00_2324;

								BgL_auxz00_2324 = (int) ((BgL_iz00_2319 - 1L));
								{
									long BgL_iz00_2405;

									BgL_iz00_2405 = (long) (BgL_auxz00_2324);
									BgL_iz00_2319 = BgL_iz00_2405;
									goto BgL_loopz00_2320;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_applyz00(void)
	{
		{	/* Ast/apply.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzast_applyz00(void)
	{
		{	/* Ast/apply.scm 14 */
			return BUNSPEC;
		}

	}



/* applycation->node */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_applycationzd2ze3nodez31zzast_applyz00(obj_t BgL_expz00_3,
		obj_t BgL_stackz00_4, obj_t BgL_locz00_5, obj_t BgL_sitez00_6)
	{
		{	/* Ast/apply.scm 31 */
			{
				obj_t BgL_procz00_1382;
				obj_t BgL_argz00_1383;

				if (PAIRP(BgL_expz00_3))
					{	/* Ast/apply.scm 32 */
						obj_t BgL_cdrzd2109zd2_1388;

						BgL_cdrzd2109zd2_1388 = CDR(((obj_t) BgL_expz00_3));
						if ((CAR(((obj_t) BgL_expz00_3)) == CNST_TABLE_REF(0)))
							{	/* Ast/apply.scm 32 */
								if (PAIRP(BgL_cdrzd2109zd2_1388))
									{	/* Ast/apply.scm 32 */
										obj_t BgL_cdrzd2113zd2_1392;

										BgL_cdrzd2113zd2_1392 = CDR(BgL_cdrzd2109zd2_1388);
										if (PAIRP(BgL_cdrzd2113zd2_1392))
											{	/* Ast/apply.scm 32 */
												if (NULLP(CDR(BgL_cdrzd2113zd2_1392)))
													{	/* Ast/apply.scm 32 */
														BgL_procz00_1382 = CAR(BgL_cdrzd2109zd2_1388);
														BgL_argz00_1383 = CAR(BgL_cdrzd2113zd2_1392);
														{	/* Ast/apply.scm 34 */
															obj_t BgL_locz00_1400;

															BgL_locz00_1400 =
																BGl_findzd2locationzf2locz20zztools_locationz00
																(BgL_expz00_3, BgL_locz00_5);
															{	/* Ast/apply.scm 34 */
																BgL_nodez00_bglt BgL_procz00_1401;

																{	/* Ast/apply.scm 37 */
																	obj_t BgL_arg1348z00_1434;

																	BgL_arg1348z00_1434 =
																		BGl_findzd2locationzf2locz20zztools_locationz00
																		(BgL_procz00_1382, BgL_locz00_1400);
																	BgL_procz00_1401 =
																		BGl_sexpzd2ze3nodez31zzast_sexpz00
																		(BgL_procz00_1382, BgL_stackz00_4,
																		BgL_arg1348z00_1434, CNST_TABLE_REF(0));
																}
																{	/* Ast/apply.scm 35 */
																	BgL_nodez00_bglt BgL_argz00_1402;

																	{	/* Ast/apply.scm 41 */
																		obj_t BgL_arg1346z00_1433;

																		BgL_arg1346z00_1433 =
																			BGl_findzd2locationzf2locz20zztools_locationz00
																			(BgL_argz00_1383, BgL_locz00_1400);
																		BgL_argz00_1402 =
																			BGl_sexpzd2ze3nodez31zzast_sexpz00
																			(BgL_argz00_1383, BgL_stackz00_4,
																			BgL_arg1346z00_1433, CNST_TABLE_REF(1));
																	}
																	{	/* Ast/apply.scm 39 */

																		{	/* Ast/apply.scm 43 */
																			bool_t BgL_test1951z00_2432;

																			{	/* Ast/apply.scm 43 */
																				bool_t BgL_test1952z00_2433;

																				{	/* Ast/apply.scm 43 */
																					obj_t BgL_classz00_1838;

																					BgL_classz00_1838 =
																						BGl_varz00zzast_nodez00;
																					{	/* Ast/apply.scm 43 */
																						BgL_objectz00_bglt
																							BgL_arg1807z00_1840;
																						{	/* Ast/apply.scm 43 */
																							obj_t BgL_tmpz00_2434;

																							BgL_tmpz00_2434 =
																								((obj_t)
																								((BgL_objectz00_bglt)
																									BgL_procz00_1401));
																							BgL_arg1807z00_1840 =
																								(BgL_objectz00_bglt)
																								(BgL_tmpz00_2434);
																						}
																						if (BGL_CONDEXPAND_ISA_ARCH64())
																							{	/* Ast/apply.scm 43 */
																								long BgL_idxz00_1846;

																								BgL_idxz00_1846 =
																									BGL_OBJECT_INHERITANCE_NUM
																									(BgL_arg1807z00_1840);
																								BgL_test1952z00_2433 =
																									(VECTOR_REF
																									(BGl_za2inheritancesza2z00zz__objectz00,
																										(BgL_idxz00_1846 + 2L)) ==
																									BgL_classz00_1838);
																							}
																						else
																							{	/* Ast/apply.scm 43 */
																								bool_t BgL_res1898z00_1871;

																								{	/* Ast/apply.scm 43 */
																									obj_t BgL_oclassz00_1854;

																									{	/* Ast/apply.scm 43 */
																										obj_t BgL_arg1815z00_1862;
																										long BgL_arg1816z00_1863;

																										BgL_arg1815z00_1862 =
																											(BGl_za2classesza2z00zz__objectz00);
																										{	/* Ast/apply.scm 43 */
																											long BgL_arg1817z00_1864;

																											BgL_arg1817z00_1864 =
																												BGL_OBJECT_CLASS_NUM
																												(BgL_arg1807z00_1840);
																											BgL_arg1816z00_1863 =
																												(BgL_arg1817z00_1864 -
																												OBJECT_TYPE);
																										}
																										BgL_oclassz00_1854 =
																											VECTOR_REF
																											(BgL_arg1815z00_1862,
																											BgL_arg1816z00_1863);
																									}
																									{	/* Ast/apply.scm 43 */
																										bool_t
																											BgL__ortest_1115z00_1855;
																										BgL__ortest_1115z00_1855 =
																											(BgL_classz00_1838 ==
																											BgL_oclassz00_1854);
																										if (BgL__ortest_1115z00_1855)
																											{	/* Ast/apply.scm 43 */
																												BgL_res1898z00_1871 =
																													BgL__ortest_1115z00_1855;
																											}
																										else
																											{	/* Ast/apply.scm 43 */
																												long BgL_odepthz00_1856;

																												{	/* Ast/apply.scm 43 */
																													obj_t
																														BgL_arg1804z00_1857;
																													BgL_arg1804z00_1857 =
																														(BgL_oclassz00_1854);
																													BgL_odepthz00_1856 =
																														BGL_CLASS_DEPTH
																														(BgL_arg1804z00_1857);
																												}
																												if (
																													(2L <
																														BgL_odepthz00_1856))
																													{	/* Ast/apply.scm 43 */
																														obj_t
																															BgL_arg1802z00_1859;
																														{	/* Ast/apply.scm 43 */
																															obj_t
																																BgL_arg1803z00_1860;
																															BgL_arg1803z00_1860
																																=
																																(BgL_oclassz00_1854);
																															BgL_arg1802z00_1859
																																=
																																BGL_CLASS_ANCESTORS_REF
																																(BgL_arg1803z00_1860,
																																2L);
																														}
																														BgL_res1898z00_1871
																															=
																															(BgL_arg1802z00_1859
																															==
																															BgL_classz00_1838);
																													}
																												else
																													{	/* Ast/apply.scm 43 */
																														BgL_res1898z00_1871
																															= ((bool_t) 0);
																													}
																											}
																									}
																								}
																								BgL_test1952z00_2433 =
																									BgL_res1898z00_1871;
																							}
																					}
																				}
																				if (BgL_test1952z00_2433)
																					{	/* Ast/apply.scm 44 */
																						bool_t BgL_test1956z00_2457;

																						{	/* Ast/apply.scm 44 */
																							BgL_valuez00_bglt
																								BgL_arg1342z00_1431;
																							BgL_arg1342z00_1431 =
																								(((BgL_variablez00_bglt)
																									COBJECT((((BgL_varz00_bglt)
																												COBJECT((
																														(BgL_varz00_bglt)
																														BgL_procz00_1401)))->
																											BgL_variablez00)))->
																								BgL_valuez00);
																							{	/* Ast/apply.scm 44 */
																								obj_t BgL_classz00_1874;

																								BgL_classz00_1874 =
																									BGl_funz00zzast_varz00;
																								{	/* Ast/apply.scm 44 */
																									BgL_objectz00_bglt
																										BgL_arg1807z00_1876;
																									{	/* Ast/apply.scm 44 */
																										obj_t BgL_tmpz00_2461;

																										BgL_tmpz00_2461 =
																											((obj_t)
																											((BgL_objectz00_bglt)
																												BgL_arg1342z00_1431));
																										BgL_arg1807z00_1876 =
																											(BgL_objectz00_bglt)
																											(BgL_tmpz00_2461);
																									}
																									if (BGL_CONDEXPAND_ISA_ARCH64
																										())
																										{	/* Ast/apply.scm 44 */
																											long BgL_idxz00_1882;

																											BgL_idxz00_1882 =
																												BGL_OBJECT_INHERITANCE_NUM
																												(BgL_arg1807z00_1876);
																											BgL_test1956z00_2457 =
																												(VECTOR_REF
																												(BGl_za2inheritancesza2z00zz__objectz00,
																													(BgL_idxz00_1882 +
																														2L)) ==
																												BgL_classz00_1874);
																										}
																									else
																										{	/* Ast/apply.scm 44 */
																											bool_t
																												BgL_res1899z00_1907;
																											{	/* Ast/apply.scm 44 */
																												obj_t
																													BgL_oclassz00_1890;
																												{	/* Ast/apply.scm 44 */
																													obj_t
																														BgL_arg1815z00_1898;
																													long
																														BgL_arg1816z00_1899;
																													BgL_arg1815z00_1898 =
																														(BGl_za2classesza2z00zz__objectz00);
																													{	/* Ast/apply.scm 44 */
																														long
																															BgL_arg1817z00_1900;
																														BgL_arg1817z00_1900
																															=
																															BGL_OBJECT_CLASS_NUM
																															(BgL_arg1807z00_1876);
																														BgL_arg1816z00_1899
																															=
																															(BgL_arg1817z00_1900
																															- OBJECT_TYPE);
																													}
																													BgL_oclassz00_1890 =
																														VECTOR_REF
																														(BgL_arg1815z00_1898,
																														BgL_arg1816z00_1899);
																												}
																												{	/* Ast/apply.scm 44 */
																													bool_t
																														BgL__ortest_1115z00_1891;
																													BgL__ortest_1115z00_1891
																														=
																														(BgL_classz00_1874
																														==
																														BgL_oclassz00_1890);
																													if (BgL__ortest_1115z00_1891)
																														{	/* Ast/apply.scm 44 */
																															BgL_res1899z00_1907
																																=
																																BgL__ortest_1115z00_1891;
																														}
																													else
																														{	/* Ast/apply.scm 44 */
																															long
																																BgL_odepthz00_1892;
																															{	/* Ast/apply.scm 44 */
																																obj_t
																																	BgL_arg1804z00_1893;
																																BgL_arg1804z00_1893
																																	=
																																	(BgL_oclassz00_1890);
																																BgL_odepthz00_1892
																																	=
																																	BGL_CLASS_DEPTH
																																	(BgL_arg1804z00_1893);
																															}
																															if (
																																(2L <
																																	BgL_odepthz00_1892))
																																{	/* Ast/apply.scm 44 */
																																	obj_t
																																		BgL_arg1802z00_1895;
																																	{	/* Ast/apply.scm 44 */
																																		obj_t
																																			BgL_arg1803z00_1896;
																																		BgL_arg1803z00_1896
																																			=
																																			(BgL_oclassz00_1890);
																																		BgL_arg1802z00_1895
																																			=
																																			BGL_CLASS_ANCESTORS_REF
																																			(BgL_arg1803z00_1896,
																																			2L);
																																	}
																																	BgL_res1899z00_1907
																																		=
																																		(BgL_arg1802z00_1895
																																		==
																																		BgL_classz00_1874);
																																}
																															else
																																{	/* Ast/apply.scm 44 */
																																	BgL_res1899z00_1907
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																												}
																											}
																											BgL_test1956z00_2457 =
																												BgL_res1899z00_1907;
																										}
																								}
																							}
																						}
																						if (BgL_test1956z00_2457)
																							{	/* Ast/apply.scm 45 */
																								bool_t BgL__ortest_1102z00_1424;

																								{	/* Ast/apply.scm 45 */
																									bool_t BgL_test1960z00_2484;

																									{	/* Ast/apply.scm 45 */
																										BgL_variablez00_bglt
																											BgL_arg1340z00_1430;
																										BgL_arg1340z00_1430 =
																											(((BgL_varz00_bglt)
																												COBJECT((
																														(BgL_varz00_bglt)
																														BgL_procz00_1401)))->
																											BgL_variablez00);
																										{	/* Ast/apply.scm 45 */
																											obj_t BgL_classz00_1909;

																											BgL_classz00_1909 =
																												BGl_globalz00zzast_varz00;
																											{	/* Ast/apply.scm 45 */
																												BgL_objectz00_bglt
																													BgL_arg1807z00_1911;
																												{	/* Ast/apply.scm 45 */
																													obj_t BgL_tmpz00_2487;

																													BgL_tmpz00_2487 =
																														((obj_t)
																														((BgL_objectz00_bglt) BgL_arg1340z00_1430));
																													BgL_arg1807z00_1911 =
																														(BgL_objectz00_bglt)
																														(BgL_tmpz00_2487);
																												}
																												if (BGL_CONDEXPAND_ISA_ARCH64())
																													{	/* Ast/apply.scm 45 */
																														long
																															BgL_idxz00_1917;
																														BgL_idxz00_1917 =
																															BGL_OBJECT_INHERITANCE_NUM
																															(BgL_arg1807z00_1911);
																														BgL_test1960z00_2484
																															=
																															(VECTOR_REF
																															(BGl_za2inheritancesza2z00zz__objectz00,
																																(BgL_idxz00_1917
																																	+ 2L)) ==
																															BgL_classz00_1909);
																													}
																												else
																													{	/* Ast/apply.scm 45 */
																														bool_t
																															BgL_res1900z00_1942;
																														{	/* Ast/apply.scm 45 */
																															obj_t
																																BgL_oclassz00_1925;
																															{	/* Ast/apply.scm 45 */
																																obj_t
																																	BgL_arg1815z00_1933;
																																long
																																	BgL_arg1816z00_1934;
																																BgL_arg1815z00_1933
																																	=
																																	(BGl_za2classesza2z00zz__objectz00);
																																{	/* Ast/apply.scm 45 */
																																	long
																																		BgL_arg1817z00_1935;
																																	BgL_arg1817z00_1935
																																		=
																																		BGL_OBJECT_CLASS_NUM
																																		(BgL_arg1807z00_1911);
																																	BgL_arg1816z00_1934
																																		=
																																		(BgL_arg1817z00_1935
																																		-
																																		OBJECT_TYPE);
																																}
																																BgL_oclassz00_1925
																																	=
																																	VECTOR_REF
																																	(BgL_arg1815z00_1933,
																																	BgL_arg1816z00_1934);
																															}
																															{	/* Ast/apply.scm 45 */
																																bool_t
																																	BgL__ortest_1115z00_1926;
																																BgL__ortest_1115z00_1926
																																	=
																																	(BgL_classz00_1909
																																	==
																																	BgL_oclassz00_1925);
																																if (BgL__ortest_1115z00_1926)
																																	{	/* Ast/apply.scm 45 */
																																		BgL_res1900z00_1942
																																			=
																																			BgL__ortest_1115z00_1926;
																																	}
																																else
																																	{	/* Ast/apply.scm 45 */
																																		long
																																			BgL_odepthz00_1927;
																																		{	/* Ast/apply.scm 45 */
																																			obj_t
																																				BgL_arg1804z00_1928;
																																			BgL_arg1804z00_1928
																																				=
																																				(BgL_oclassz00_1925);
																																			BgL_odepthz00_1927
																																				=
																																				BGL_CLASS_DEPTH
																																				(BgL_arg1804z00_1928);
																																		}
																																		if (
																																			(2L <
																																				BgL_odepthz00_1927))
																																			{	/* Ast/apply.scm 45 */
																																				obj_t
																																					BgL_arg1802z00_1930;
																																				{	/* Ast/apply.scm 45 */
																																					obj_t
																																						BgL_arg1803z00_1931;
																																					BgL_arg1803z00_1931
																																						=
																																						(BgL_oclassz00_1925);
																																					BgL_arg1802z00_1930
																																						=
																																						BGL_CLASS_ANCESTORS_REF
																																						(BgL_arg1803z00_1931,
																																						2L);
																																				}
																																				BgL_res1900z00_1942
																																					=
																																					(BgL_arg1802z00_1930
																																					==
																																					BgL_classz00_1909);
																																			}
																																		else
																																			{	/* Ast/apply.scm 45 */
																																				BgL_res1900z00_1942
																																					=
																																					(
																																					(bool_t)
																																					0);
																																			}
																																	}
																															}
																														}
																														BgL_test1960z00_2484
																															=
																															BgL_res1900z00_1942;
																													}
																											}
																										}
																									}
																									if (BgL_test1960z00_2484)
																										{	/* Ast/apply.scm 45 */
																											BgL__ortest_1102z00_1424 =
																												((bool_t) 0);
																										}
																									else
																										{	/* Ast/apply.scm 45 */
																											BgL__ortest_1102z00_1424 =
																												((bool_t) 1);
																										}
																								}
																								if (BgL__ortest_1102z00_1424)
																									{	/* Ast/apply.scm 45 */
																										BgL_test1951z00_2432 =
																											BgL__ortest_1102z00_1424;
																									}
																								else
																									{	/* Ast/apply.scm 46 */
																										bool_t BgL_test1965z00_2511;

																										{	/* Ast/apply.scm 46 */
																											BgL_variablez00_bglt
																												BgL_arg1339z00_1428;
																											BgL_arg1339z00_1428 =
																												(((BgL_varz00_bglt)
																													COBJECT((
																															(BgL_varz00_bglt)
																															BgL_procz00_1401)))->
																												BgL_variablez00);
																											BgL_test1965z00_2511 =
																												BGl_globalzd2optionalzf3z21zzast_varz00
																												(((obj_t)
																													BgL_arg1339z00_1428));
																										}
																										if (BgL_test1965z00_2511)
																											{	/* Ast/apply.scm 46 */
																												BgL_test1951z00_2432 =
																													((bool_t) 0);
																											}
																										else
																											{	/* Ast/apply.scm 47 */
																												bool_t
																													BgL_test1966z00_2516;
																												{	/* Ast/apply.scm 47 */
																													BgL_variablez00_bglt
																														BgL_arg1335z00_1427;
																													BgL_arg1335z00_1427 =
																														(((BgL_varz00_bglt)
																															COBJECT((
																																	(BgL_varz00_bglt)
																																	BgL_procz00_1401)))->
																														BgL_variablez00);
																													BgL_test1966z00_2516 =
																														BGl_globalzd2keyzf3z21zzast_varz00
																														(((obj_t)
																															BgL_arg1335z00_1427));
																												}
																												if (BgL_test1966z00_2516)
																													{	/* Ast/apply.scm 47 */
																														BgL_test1951z00_2432
																															= ((bool_t) 0);
																													}
																												else
																													{	/* Ast/apply.scm 47 */
																														BgL_test1951z00_2432
																															= ((bool_t) 1);
																													}
																											}
																									}
																							}
																						else
																							{	/* Ast/apply.scm 44 */
																								BgL_test1951z00_2432 =
																									((bool_t) 0);
																							}
																					}
																				else
																					{	/* Ast/apply.scm 43 */
																						BgL_test1951z00_2432 = ((bool_t) 0);
																					}
																			}
																			if (BgL_test1951z00_2432)
																				{	/* Ast/apply.scm 43 */
																					return
																						BGl_knownzd2appzd2lyzd2ze3nodez31zzast_applyz00
																						(BgL_stackz00_4, BgL_locz00_1400,
																						BgL_procz00_1401, BgL_argz00_1402,
																						BgL_sitez00_6);
																				}
																			else
																				{	/* Ast/apply.scm 49 */
																					BgL_appzd2lyzd2_bglt
																						BgL_new1105z00_1419;
																					{	/* Ast/apply.scm 50 */
																						BgL_appzd2lyzd2_bglt
																							BgL_new1104z00_1420;
																						BgL_new1104z00_1420 =
																							((BgL_appzd2lyzd2_bglt)
																							BOBJECT(GC_MALLOC(sizeof(struct
																										BgL_appzd2lyzd2_bgl))));
																						{	/* Ast/apply.scm 50 */
																							long BgL_arg1333z00_1421;

																							BgL_arg1333z00_1421 =
																								BGL_CLASS_NUM
																								(BGl_appzd2lyzd2zzast_nodez00);
																							BGL_OBJECT_CLASS_NUM_SET((
																									(BgL_objectz00_bglt)
																									BgL_new1104z00_1420),
																								BgL_arg1333z00_1421);
																						}
																						{	/* Ast/apply.scm 50 */
																							BgL_objectz00_bglt
																								BgL_tmpz00_2526;
																							BgL_tmpz00_2526 =
																								((BgL_objectz00_bglt)
																								BgL_new1104z00_1420);
																							BGL_OBJECT_WIDENING_SET
																								(BgL_tmpz00_2526, BFALSE);
																						}
																						((BgL_objectz00_bglt)
																							BgL_new1104z00_1420);
																						BgL_new1105z00_1419 =
																							BgL_new1104z00_1420;
																					}
																					((((BgL_nodez00_bglt) COBJECT(
																									((BgL_nodez00_bglt)
																										BgL_new1105z00_1419)))->
																							BgL_locz00) =
																						((obj_t) BgL_locz00_1400), BUNSPEC);
																					((((BgL_nodez00_bglt)
																								COBJECT(((BgL_nodez00_bglt)
																										BgL_new1105z00_1419)))->
																							BgL_typez00) =
																						((BgL_typez00_bglt) (
																								(BgL_typez00_bglt)
																								BGl_za2_za2z00zztype_cachez00)),
																						BUNSPEC);
																					((((BgL_appzd2lyzd2_bglt)
																								COBJECT(BgL_new1105z00_1419))->
																							BgL_funz00) =
																						((BgL_nodez00_bglt)
																							BgL_procz00_1401), BUNSPEC);
																					((((BgL_appzd2lyzd2_bglt)
																								COBJECT(BgL_new1105z00_1419))->
																							BgL_argz00) =
																						((BgL_nodez00_bglt)
																							BgL_argz00_1402), BUNSPEC);
																					return ((BgL_nodez00_bglt)
																						BgL_new1105z00_1419);
																				}
																		}
																	}
																}
															}
														}
													}
												else
													{	/* Ast/apply.scm 32 */
													BgL_tagzd2102zd2_1385:
														return
															BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00
															(BGl_string1918z00zzast_applyz00, BgL_expz00_3,
															BGl_findzd2locationzf2locz20zztools_locationz00
															(BgL_expz00_3, BgL_locz00_5));
													}
											}
										else
											{	/* Ast/apply.scm 32 */
												goto BgL_tagzd2102zd2_1385;
											}
									}
								else
									{	/* Ast/apply.scm 32 */
										goto BgL_tagzd2102zd2_1385;
									}
							}
						else
							{	/* Ast/apply.scm 32 */
								goto BgL_tagzd2102zd2_1385;
							}
					}
				else
					{	/* Ast/apply.scm 32 */
						goto BgL_tagzd2102zd2_1385;
					}
			}
		}

	}



/* &applycation->node */
	BgL_nodez00_bglt BGl_z62applycationzd2ze3nodez53zzast_applyz00(obj_t
		BgL_envz00_2281, obj_t BgL_expz00_2282, obj_t BgL_stackz00_2283,
		obj_t BgL_locz00_2284, obj_t BgL_sitez00_2285)
	{
		{	/* Ast/apply.scm 31 */
			return
				BGl_applycationzd2ze3nodez31zzast_applyz00(BgL_expz00_2282,
				BgL_stackz00_2283, BgL_locz00_2284, BgL_sitez00_2285);
		}

	}



/* known-app-ly->node */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_knownzd2appzd2lyzd2ze3nodez31zzast_applyz00(obj_t BgL_stackz00_10,
		obj_t BgL_locz00_11, BgL_nodez00_bglt BgL_procz00_12,
		BgL_nodez00_bglt BgL_argz00_13, obj_t BgL_sitez00_14)
	{
		{	/* Ast/apply.scm 107 */
			{	/* Ast/apply.scm 108 */
				BgL_valuez00_bglt BgL_funz00_1436;

				BgL_funz00_1436 =
					(((BgL_variablez00_bglt) COBJECT(
							(((BgL_varz00_bglt) COBJECT(
										((BgL_varz00_bglt) BgL_procz00_12)))->BgL_variablez00)))->
					BgL_valuez00);
				{	/* Ast/apply.scm 108 */
					long BgL_arityz00_1437;

					BgL_arityz00_1437 =
						(((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt) BgL_funz00_1436)))->BgL_arityz00);
					{	/* Ast/apply.scm 109 */
						obj_t BgL_framez00_1438;

						BgL_framez00_1438 =
							BGl_makezd2funzd2framez00zzast_applyz00(
							((BgL_funz00_bglt) BgL_funz00_1436));
						{	/* Ast/apply.scm 110 */

							if ((BgL_arityz00_1437 > 0L))
								{	/* Ast/apply.scm 115 */
									return
										((BgL_nodez00_bglt)
										BGl_fxzd2knownzd2appzd2lyzd2ze3nodeze3zzast_applyz00
										(BgL_stackz00_10, BgL_locz00_11, BgL_procz00_12,
											BgL_argz00_13, BgL_framez00_1438, BgL_sitez00_14));
								}
							else
								{	/* Ast/apply.scm 115 */
									if ((BgL_arityz00_1437 == 0L))
										{	/* Ast/apply.scm 126 */
											obj_t BgL_arg1364z00_1960;

											{	/* Ast/apply.scm 126 */
												obj_t BgL_list1365z00_1961;

												BgL_list1365z00_1961 =
													MAKE_YOUNG_PAIR(((obj_t) BgL_procz00_12), BNIL);
												BgL_arg1364z00_1960 = BgL_list1365z00_1961;
											}
											return
												BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_arg1364z00_1960,
												BgL_stackz00_10, BgL_locz00_11, BgL_sitez00_14);
										}
									else
										{	/* Ast/apply.scm 117 */
											return
												((BgL_nodez00_bglt)
												BGl_vazd2knownzd2appzd2lyzd2ze3nodeze3zzast_applyz00
												(BgL_stackz00_10, BgL_locz00_11, BgL_procz00_12,
													BgL_argz00_13, BgL_framez00_1438, BgL_sitez00_14));
										}
								}
						}
					}
				}
			}
		}

	}



/* &known-app-ly->node */
	BgL_nodez00_bglt BGl_z62knownzd2appzd2lyzd2ze3nodez53zzast_applyz00(obj_t
		BgL_envz00_2286, obj_t BgL_stackz00_2287, obj_t BgL_locz00_2288,
		obj_t BgL_procz00_2289, obj_t BgL_argz00_2290, obj_t BgL_sitez00_2291)
	{
		{	/* Ast/apply.scm 107 */
			return
				BGl_knownzd2appzd2lyzd2ze3nodez31zzast_applyz00(BgL_stackz00_2287,
				BgL_locz00_2288, ((BgL_nodez00_bglt) BgL_procz00_2289),
				((BgL_nodez00_bglt) BgL_argz00_2290), BgL_sitez00_2291);
		}

	}



/* fx-known-app-ly->node */
	BgL_letzd2varzd2_bglt
		BGl_fxzd2knownzd2appzd2lyzd2ze3nodeze3zzast_applyz00(obj_t BgL_stackz00_21,
		obj_t BgL_locz00_22, BgL_nodez00_bglt BgL_procz00_23,
		BgL_nodez00_bglt BgL_argz00_24, obj_t BgL_framez00_25, obj_t BgL_sitez00_26)
	{
		{	/* Ast/apply.scm 147 */
			{	/* Ast/apply.scm 148 */
				BgL_localz00_bglt BgL_runnerz00_1444;
				BgL_typez00_bglt BgL_typez00_1445;

				{	/* Ast/apply.scm 148 */
					obj_t BgL_arg1642z00_1536;

					BgL_arg1642z00_1536 =
						BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
						(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(2)));
					BgL_runnerz00_1444 =
						BGl_makezd2localzd2svarz00zzast_localz00(BgL_arg1642z00_1536,
						((BgL_typez00_bglt) BGl_za2_za2z00zztype_cachez00));
				}
				BgL_typez00_1445 =
					(((BgL_nodez00_bglt) COBJECT(BgL_procz00_23))->BgL_typez00);
				{	/* Ast/apply.scm 151 */
					obj_t BgL_vz00_1968;

					BgL_vz00_1968 = CNST_TABLE_REF(3);
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_runnerz00_1444)))->
							BgL_accessz00) = ((obj_t) BgL_vz00_1968), BUNSPEC);
				}
				{	/* Ast/apply.scm 152 */
					BgL_letzd2varzd2_bglt BgL_new1111z00_1446;

					{	/* Ast/apply.scm 153 */
						BgL_letzd2varzd2_bglt BgL_new1110z00_1534;

						BgL_new1110z00_1534 =
							((BgL_letzd2varzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_letzd2varzd2_bgl))));
						{	/* Ast/apply.scm 153 */
							long BgL_arg1630z00_1535;

							BgL_arg1630z00_1535 =
								BGL_CLASS_NUM(BGl_letzd2varzd2zzast_nodez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1110z00_1534),
								BgL_arg1630z00_1535);
						}
						{	/* Ast/apply.scm 153 */
							BgL_objectz00_bglt BgL_tmpz00_2577;

							BgL_tmpz00_2577 = ((BgL_objectz00_bglt) BgL_new1110z00_1534);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2577, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_new1110z00_1534);
						BgL_new1111z00_1446 = BgL_new1110z00_1534;
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_new1111z00_1446)))->BgL_locz00) =
						((obj_t) BgL_locz00_22), BUNSPEC);
					((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_new1111z00_1446)))->BgL_typez00) =
						((BgL_typez00_bglt)
							BGl_strictzd2nodezd2typez00zzast_nodez00(((BgL_typez00_bglt)
									BGl_za2_za2z00zztype_cachez00), BgL_typez00_1445)), BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1111z00_1446)))->BgL_sidezd2effectzd2) =
						((obj_t) BUNSPEC), BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1111z00_1446)))->BgL_keyz00) =
						((obj_t) BINT(-1L)), BUNSPEC);
					{
						obj_t BgL_auxz00_2592;

						{	/* Ast/apply.scm 155 */
							obj_t BgL_arg1367z00_1447;

							BgL_arg1367z00_1447 =
								MAKE_YOUNG_PAIR(
								((obj_t) BgL_runnerz00_1444), ((obj_t) BgL_argz00_24));
							{	/* Ast/apply.scm 155 */
								obj_t BgL_list1368z00_1448;

								BgL_list1368z00_1448 =
									MAKE_YOUNG_PAIR(BgL_arg1367z00_1447, BNIL);
								BgL_auxz00_2592 = BgL_list1368z00_1448;
						}}
						((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1111z00_1446))->
								BgL_bindingsz00) = ((obj_t) BgL_auxz00_2592), BUNSPEC);
					}
					((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1111z00_1446))->
							BgL_bodyz00) =
						((BgL_nodez00_bglt) BGl_loopze71ze7zzast_applyz00(BgL_sitez00_26,
								BgL_procz00_23, BgL_framez00_25, BgL_stackz00_21,
								BgL_runnerz00_1444, BgL_typez00_1445, BgL_locz00_22,
								BgL_framez00_25)), BUNSPEC);
					((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1111z00_1446))->
							BgL_removablezf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
					return BgL_new1111z00_1446;
				}
			}
		}

	}



/* loop~1 */
	BgL_nodez00_bglt BGl_loopze71ze7zzast_applyz00(obj_t BgL_sitez00_2317,
		BgL_nodez00_bglt BgL_procz00_2316, obj_t BgL_framez00_2315,
		obj_t BgL_stackz00_2314, BgL_localz00_bglt BgL_runnerz00_2313,
		BgL_typez00_bglt BgL_typez00_2312, obj_t BgL_locz00_2311,
		obj_t BgL_localsz00_1450)
	{
		{	/* Ast/apply.scm 156 */
			if (NULLP(BgL_localsz00_1450))
				{	/* Ast/apply.scm 158 */
					BgL_nodez00_bglt BgL_appz00_1453;

					{	/* Ast/apply.scm 160 */
						obj_t BgL_arg1502z00_1475;

						{	/* Ast/apply.scm 160 */
							obj_t BgL_arg1509z00_1476;

							{	/* Ast/apply.scm 160 */
								obj_t BgL_arg1513z00_1477;

								if (NULLP(BgL_framez00_2315))
									{	/* Ast/apply.scm 160 */
										BgL_arg1513z00_1477 = BNIL;
									}
								else
									{	/* Ast/apply.scm 160 */
										obj_t BgL_head1267z00_1480;

										BgL_head1267z00_1480 = MAKE_YOUNG_PAIR(BNIL, BNIL);
										{
											obj_t BgL_l1265z00_1482;
											obj_t BgL_tail1268z00_1483;

											BgL_l1265z00_1482 = BgL_framez00_2315;
											BgL_tail1268z00_1483 = BgL_head1267z00_1480;
										BgL_zc3z04anonymousza31515ze3z87_1484:
											if (NULLP(BgL_l1265z00_1482))
												{	/* Ast/apply.scm 160 */
													BgL_arg1513z00_1477 = CDR(BgL_head1267z00_1480);
												}
											else
												{	/* Ast/apply.scm 160 */
													obj_t BgL_newtail1269z00_1486;

													{	/* Ast/apply.scm 160 */
														BgL_refz00_bglt BgL_arg1540z00_1488;

														{	/* Ast/apply.scm 160 */
															obj_t BgL_localz00_1489;

															BgL_localz00_1489 =
																CAR(((obj_t) BgL_l1265z00_1482));
															{	/* Ast/apply.scm 161 */
																BgL_refz00_bglt BgL_new1114z00_1490;

																{	/* Ast/apply.scm 164 */
																	BgL_refz00_bglt BgL_new1113z00_1492;

																	BgL_new1113z00_1492 =
																		((BgL_refz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_refz00_bgl))));
																	{	/* Ast/apply.scm 164 */
																		long BgL_arg1546z00_1493;

																		BgL_arg1546z00_1493 =
																			BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1113z00_1492),
																			BgL_arg1546z00_1493);
																	}
																	{	/* Ast/apply.scm 164 */
																		BgL_objectz00_bglt BgL_tmpz00_2615;

																		BgL_tmpz00_2615 =
																			((BgL_objectz00_bglt)
																			BgL_new1113z00_1492);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2615,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1113z00_1492);
																	BgL_new1114z00_1490 = BgL_new1113z00_1492;
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1114z00_1490)))->BgL_locz00) =
																	((obj_t) BgL_locz00_2311), BUNSPEC);
																{
																	BgL_typez00_bglt BgL_auxz00_2621;

																	{	/* Ast/apply.scm 163 */
																		BgL_typez00_bglt BgL_arg1544z00_1491;

																		BgL_arg1544z00_1491 =
																			(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						((BgL_localz00_bglt)
																							BgL_localz00_1489))))->
																			BgL_typez00);
																		BgL_auxz00_2621 =
																			BGl_strictzd2nodezd2typez00zzast_nodez00((
																				(BgL_typez00_bglt)
																				BGl_za2_za2z00zztype_cachez00),
																			BgL_arg1544z00_1491);
																	}
																	((((BgL_nodez00_bglt) COBJECT(
																					((BgL_nodez00_bglt)
																						BgL_new1114z00_1490)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) BgL_auxz00_2621),
																		BUNSPEC);
																}
																((((BgL_varz00_bglt) COBJECT(
																				((BgL_varz00_bglt)
																					BgL_new1114z00_1490)))->
																		BgL_variablez00) =
																	((BgL_variablez00_bglt) (
																			(BgL_variablez00_bglt)
																			BgL_localz00_1489)), BUNSPEC);
																BgL_arg1540z00_1488 = BgL_new1114z00_1490;
														}}
														BgL_newtail1269z00_1486 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_arg1540z00_1488), BNIL);
													}
													SET_CDR(BgL_tail1268z00_1483,
														BgL_newtail1269z00_1486);
													{	/* Ast/apply.scm 160 */
														obj_t BgL_arg1535z00_1487;

														BgL_arg1535z00_1487 =
															CDR(((obj_t) BgL_l1265z00_1482));
														{
															obj_t BgL_tail1268z00_2638;
															obj_t BgL_l1265z00_2637;

															BgL_l1265z00_2637 = BgL_arg1535z00_1487;
															BgL_tail1268z00_2638 = BgL_newtail1269z00_1486;
															BgL_tail1268z00_1483 = BgL_tail1268z00_2638;
															BgL_l1265z00_1482 = BgL_l1265z00_2637;
															goto BgL_zc3z04anonymousza31515ze3z87_1484;
														}
													}
												}
										}
									}
								BgL_arg1509z00_1476 =
									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
									(BgL_arg1513z00_1477, BNIL);
							}
							BgL_arg1502z00_1475 =
								MAKE_YOUNG_PAIR(
								((obj_t) BgL_procz00_2316), BgL_arg1509z00_1476);
						}
						BgL_appz00_1453 =
							BGl_applicationzd2ze3nodez31zzast_appz00(BgL_arg1502z00_1475,
							BgL_stackz00_2314, BgL_locz00_2311, CNST_TABLE_REF(1));
					}
					if (CBOOL(BGl_za2unsafezd2arityza2zd2zzengine_paramz00))
						{	/* Ast/apply.scm 169 */
							return BgL_appz00_1453;
						}
					else
						{	/* Ast/apply.scm 173 */
							obj_t BgL_arg1375z00_1454;

							{	/* Ast/apply.scm 173 */
								obj_t BgL_arg1376z00_1455;

								{	/* Ast/apply.scm 173 */
									obj_t BgL_arg1377z00_1456;
									obj_t BgL_arg1378z00_1457;

									{	/* Ast/apply.scm 173 */
										obj_t BgL_arg1379z00_1458;

										{	/* Ast/apply.scm 173 */
											obj_t BgL_arg1380z00_1459;

											{	/* Ast/apply.scm 173 */
												obj_t BgL_arg1408z00_1460;

												{	/* Ast/apply.scm 173 */
													BgL_refz00_bglt BgL_arg1410z00_1461;

													{	/* Ast/apply.scm 173 */
														BgL_refz00_bglt BgL_new1116z00_1462;

														{	/* Ast/apply.scm 176 */
															BgL_refz00_bglt BgL_new1115z00_1464;

															BgL_new1115z00_1464 =
																((BgL_refz00_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_refz00_bgl))));
															{	/* Ast/apply.scm 176 */
																long BgL_arg1422z00_1465;

																BgL_arg1422z00_1465 =
																	BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1115z00_1464),
																	BgL_arg1422z00_1465);
															}
															{	/* Ast/apply.scm 176 */
																BgL_objectz00_bglt BgL_tmpz00_2650;

																BgL_tmpz00_2650 =
																	((BgL_objectz00_bglt) BgL_new1115z00_1464);
																BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2650,
																	BFALSE);
															}
															((BgL_objectz00_bglt) BgL_new1115z00_1464);
															BgL_new1116z00_1462 = BgL_new1115z00_1464;
														}
														((((BgL_nodez00_bglt) COBJECT(
																		((BgL_nodez00_bglt) BgL_new1116z00_1462)))->
																BgL_locz00) =
															((obj_t) BgL_locz00_2311), BUNSPEC);
														{
															BgL_typez00_bglt BgL_auxz00_2656;

															{	/* Ast/apply.scm 175 */
																BgL_typez00_bglt BgL_arg1421z00_1463;

																BgL_arg1421z00_1463 =
																	(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				BgL_runnerz00_2313)))->BgL_typez00);
																BgL_auxz00_2656 =
																	BGl_strictzd2nodezd2typez00zzast_nodez00((
																		(BgL_typez00_bglt)
																		BGl_za2_za2z00zztype_cachez00),
																	BgL_arg1421z00_1463);
															}
															((((BgL_nodez00_bglt) COBJECT(
																			((BgL_nodez00_bglt)
																				BgL_new1116z00_1462)))->BgL_typez00) =
																((BgL_typez00_bglt) BgL_auxz00_2656), BUNSPEC);
														}
														((((BgL_varz00_bglt) COBJECT(
																		((BgL_varz00_bglt) BgL_new1116z00_1462)))->
																BgL_variablez00) =
															((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
																	BgL_runnerz00_2313)), BUNSPEC);
														BgL_arg1410z00_1461 = BgL_new1116z00_1462;
													}
													BgL_arg1408z00_1460 =
														MAKE_YOUNG_PAIR(
														((obj_t) BgL_arg1410z00_1461), BNIL);
												}
												BgL_arg1380z00_1459 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(4),
													BgL_arg1408z00_1460);
											}
											BgL_arg1379z00_1458 =
												MAKE_YOUNG_PAIR(BgL_arg1380z00_1459, BNIL);
										}
										BgL_arg1377z00_1456 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(5), BgL_arg1379z00_1458);
									}
									{	/* Ast/apply.scm 180 */
										obj_t BgL_arg1434z00_1466;

										{	/* Ast/apply.scm 180 */
											obj_t BgL_arg1437z00_1467;

											{	/* Ast/apply.scm 180 */
												obj_t BgL_arg1448z00_1468;

												{	/* Ast/apply.scm 180 */
													obj_t BgL_arg1453z00_1469;

													{	/* Ast/apply.scm 180 */
														obj_t BgL_arg1454z00_1470;

														{	/* Ast/apply.scm 180 */
															obj_t BgL_arg1472z00_1471;

															{	/* Ast/apply.scm 180 */
																obj_t BgL_arg1473z00_1472;

																{	/* Ast/apply.scm 180 */
																	obj_t BgL_arg1485z00_1473;

																	{	/* Ast/apply.scm 180 */
																		BgL_variablez00_bglt BgL_arg1489z00_1474;

																		BgL_arg1489z00_1474 =
																			(((BgL_varz00_bglt) COBJECT(
																					((BgL_varz00_bglt)
																						BgL_procz00_2316)))->
																			BgL_variablez00);
																		BgL_arg1485z00_1473 =
																			BGl_shapez00zztools_shapez00(((obj_t)
																				BgL_arg1489z00_1474));
																	}
																	BgL_arg1473z00_1472 =
																		MAKE_YOUNG_PAIR(BgL_arg1485z00_1473, BNIL);
																}
																BgL_arg1472z00_1471 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
																	BgL_arg1473z00_1472);
															}
															BgL_arg1454z00_1470 =
																MAKE_YOUNG_PAIR(BgL_arg1472z00_1471, BNIL);
														}
														BgL_arg1453z00_1469 =
															MAKE_YOUNG_PAIR(BGl_string1919z00zzast_applyz00,
															BgL_arg1454z00_1470);
													}
													BgL_arg1448z00_1468 =
														MAKE_YOUNG_PAIR(BGl_string1920z00zzast_applyz00,
														BgL_arg1453z00_1469);
												}
												BgL_arg1437z00_1467 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(7),
													BgL_arg1448z00_1468);
											}
											BgL_arg1434z00_1466 =
												MAKE_YOUNG_PAIR(BgL_arg1437z00_1467, BNIL);
										}
										BgL_arg1378z00_1457 =
											MAKE_YOUNG_PAIR(
											((obj_t) BgL_appz00_1453), BgL_arg1434z00_1466);
									}
									BgL_arg1376z00_1455 =
										MAKE_YOUNG_PAIR(BgL_arg1377z00_1456, BgL_arg1378z00_1457);
								}
								BgL_arg1375z00_1454 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(8), BgL_arg1376z00_1455);
							}
							return
								BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_arg1375z00_1454,
								BgL_stackz00_2314, BgL_locz00_2311, BgL_sitez00_2317);
						}
				}
			else
				{	/* Ast/apply.scm 184 */
					BgL_letzd2varzd2_bglt BgL_new1119z00_1495;

					{	/* Ast/apply.scm 185 */
						BgL_letzd2varzd2_bglt BgL_new1118z00_1531;

						BgL_new1118z00_1531 =
							((BgL_letzd2varzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_letzd2varzd2_bgl))));
						{	/* Ast/apply.scm 185 */
							long BgL_arg1629z00_1532;

							BgL_arg1629z00_1532 =
								BGL_CLASS_NUM(BGl_letzd2varzd2zzast_nodez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1118z00_1531),
								BgL_arg1629z00_1532);
						}
						{	/* Ast/apply.scm 185 */
							BgL_objectz00_bglt BgL_tmpz00_2696;

							BgL_tmpz00_2696 = ((BgL_objectz00_bglt) BgL_new1118z00_1531);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2696, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_new1118z00_1531);
						BgL_new1119z00_1495 = BgL_new1118z00_1531;
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_new1119z00_1495)))->BgL_locz00) =
						((obj_t) BgL_locz00_2311), BUNSPEC);
					((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_new1119z00_1495)))->BgL_typez00) =
						((BgL_typez00_bglt)
							BGl_strictzd2nodezd2typez00zzast_nodez00(((BgL_typez00_bglt)
									BGl_za2_za2z00zztype_cachez00), BgL_typez00_2312)), BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1119z00_1495)))->BgL_sidezd2effectzd2) =
						((obj_t) BUNSPEC), BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1119z00_1495)))->BgL_keyz00) =
						((obj_t) BINT(-1L)), BUNSPEC);
					{
						obj_t BgL_auxz00_2711;

						{	/* Ast/apply.scm 188 */
							obj_t BgL_arg1552z00_1496;

							{	/* Ast/apply.scm 188 */
								obj_t BgL_arg1559z00_1498;
								BgL_nodez00_bglt BgL_arg1561z00_1499;

								BgL_arg1559z00_1498 = CAR(((obj_t) BgL_localsz00_1450));
								{	/* Ast/apply.scm 191 */
									obj_t BgL_arg1564z00_1500;

									{	/* Ast/apply.scm 191 */
										obj_t BgL_arg1565z00_1501;

										{	/* Ast/apply.scm 191 */
											BgL_refz00_bglt BgL_arg1571z00_1502;

											{	/* Ast/apply.scm 191 */
												BgL_refz00_bglt BgL_new1122z00_1503;

												{	/* Ast/apply.scm 194 */
													BgL_refz00_bglt BgL_new1120z00_1505;

													BgL_new1120z00_1505 =
														((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_refz00_bgl))));
													{	/* Ast/apply.scm 194 */
														long BgL_arg1575z00_1506;

														{	/* Ast/apply.scm 194 */
															obj_t BgL_classz00_1994;

															BgL_classz00_1994 = BGl_refz00zzast_nodez00;
															BgL_arg1575z00_1506 =
																BGL_CLASS_NUM(BgL_classz00_1994);
														}
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1120z00_1505),
															BgL_arg1575z00_1506);
													}
													{	/* Ast/apply.scm 194 */
														BgL_objectz00_bglt BgL_tmpz00_2718;

														BgL_tmpz00_2718 =
															((BgL_objectz00_bglt) BgL_new1120z00_1505);
														BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2718, BFALSE);
													}
													((BgL_objectz00_bglt) BgL_new1120z00_1505);
													BgL_new1122z00_1503 = BgL_new1120z00_1505;
												}
												((((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt) BgL_new1122z00_1503)))->
														BgL_locz00) = ((obj_t) BgL_locz00_2311), BUNSPEC);
												{
													BgL_typez00_bglt BgL_auxz00_2724;

													{	/* Ast/apply.scm 193 */
														BgL_typez00_bglt BgL_arg1573z00_1504;

														BgL_arg1573z00_1504 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		BgL_runnerz00_2313)))->BgL_typez00);
														BgL_auxz00_2724 =
															BGl_strictzd2nodezd2typez00zzast_nodez00((
																(BgL_typez00_bglt)
																BGl_za2_za2z00zztype_cachez00),
															BgL_arg1573z00_1504);
													}
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_new1122z00_1503)))->
															BgL_typez00) =
														((BgL_typez00_bglt) BgL_auxz00_2724), BUNSPEC);
												}
												((((BgL_varz00_bglt) COBJECT(
																((BgL_varz00_bglt) BgL_new1122z00_1503)))->
														BgL_variablez00) =
													((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
															BgL_runnerz00_2313)), BUNSPEC);
												BgL_arg1571z00_1502 = BgL_new1122z00_1503;
											}
											BgL_arg1565z00_1501 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg1571z00_1502), BNIL);
										}
										BgL_arg1564z00_1500 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(9), BgL_arg1565z00_1501);
									}
									BgL_arg1561z00_1499 =
										BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_arg1564z00_1500,
										BgL_stackz00_2314, BgL_locz00_2311, CNST_TABLE_REF(1));
								}
								BgL_arg1552z00_1496 =
									MAKE_YOUNG_PAIR(BgL_arg1559z00_1498,
									((obj_t) BgL_arg1561z00_1499));
							}
							{	/* Ast/apply.scm 187 */
								obj_t BgL_list1553z00_1497;

								BgL_list1553z00_1497 =
									MAKE_YOUNG_PAIR(BgL_arg1552z00_1496, BNIL);
								BgL_auxz00_2711 = BgL_list1553z00_1497;
						}}
						((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1119z00_1495))->
								BgL_bindingsz00) = ((obj_t) BgL_auxz00_2711), BUNSPEC);
					}
					{
						BgL_nodez00_bglt BgL_auxz00_2744;

						if (NULLP(CDR(((obj_t) BgL_localsz00_1450))))
							{	/* Ast/apply.scm 199 */
								obj_t BgL_arg1584z00_1509;

								BgL_arg1584z00_1509 = CDR(((obj_t) BgL_localsz00_1450));
								BgL_auxz00_2744 =
									BGl_loopze71ze7zzast_applyz00(BgL_sitez00_2317,
									BgL_procz00_2316, BgL_framez00_2315, BgL_stackz00_2314,
									BgL_runnerz00_2313, BgL_typez00_2312, BgL_locz00_2311,
									BgL_arg1584z00_1509);
							}
						else
							{	/* Ast/apply.scm 202 */
								obj_t BgL_arg1585z00_1510;

								{	/* Ast/apply.scm 202 */
									obj_t BgL_arg1589z00_1511;

									{	/* Ast/apply.scm 202 */
										obj_t BgL_arg1591z00_1512;
										obj_t BgL_arg1593z00_1513;

										{	/* Ast/apply.scm 202 */
											obj_t BgL_arg1594z00_1514;

											{	/* Ast/apply.scm 202 */
												BgL_refz00_bglt BgL_arg1595z00_1515;
												obj_t BgL_arg1602z00_1516;

												{	/* Ast/apply.scm 202 */
													BgL_refz00_bglt BgL_new1124z00_1517;

													{	/* Ast/apply.scm 205 */
														BgL_refz00_bglt BgL_new1123z00_1519;

														BgL_new1123z00_1519 =
															((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_refz00_bgl))));
														{	/* Ast/apply.scm 205 */
															long BgL_arg1606z00_1520;

															{	/* Ast/apply.scm 205 */
																obj_t BgL_classz00_2002;

																BgL_classz00_2002 = BGl_refz00zzast_nodez00;
																BgL_arg1606z00_1520 =
																	BGL_CLASS_NUM(BgL_classz00_2002);
															}
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_new1123z00_1519),
																BgL_arg1606z00_1520);
														}
														{	/* Ast/apply.scm 205 */
															BgL_objectz00_bglt BgL_tmpz00_2756;

															BgL_tmpz00_2756 =
																((BgL_objectz00_bglt) BgL_new1123z00_1519);
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2756, BFALSE);
														}
														((BgL_objectz00_bglt) BgL_new1123z00_1519);
														BgL_new1124z00_1517 = BgL_new1123z00_1519;
													}
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_new1124z00_1517)))->
															BgL_locz00) = ((obj_t) BgL_locz00_2311), BUNSPEC);
													{
														BgL_typez00_bglt BgL_auxz00_2762;

														{	/* Ast/apply.scm 204 */
															BgL_typez00_bglt BgL_arg1605z00_1518;

															BgL_arg1605z00_1518 =
																(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			BgL_runnerz00_2313)))->BgL_typez00);
															BgL_auxz00_2762 =
																BGl_strictzd2nodezd2typez00zzast_nodez00((
																	(BgL_typez00_bglt)
																	BGl_za2_za2z00zztype_cachez00),
																BgL_arg1605z00_1518);
														}
														((((BgL_nodez00_bglt) COBJECT(
																		((BgL_nodez00_bglt) BgL_new1124z00_1517)))->
																BgL_typez00) =
															((BgL_typez00_bglt) BgL_auxz00_2762), BUNSPEC);
													}
													((((BgL_varz00_bglt) COBJECT(
																	((BgL_varz00_bglt) BgL_new1124z00_1517)))->
															BgL_variablez00) =
														((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
																BgL_runnerz00_2313)), BUNSPEC);
													BgL_arg1595z00_1515 = BgL_new1124z00_1517;
												}
												{	/* Ast/apply.scm 207 */
													obj_t BgL_arg1609z00_1521;

													{	/* Ast/apply.scm 207 */
														obj_t BgL_arg1611z00_1522;

														{	/* Ast/apply.scm 207 */
															BgL_refz00_bglt BgL_arg1613z00_1523;

															{	/* Ast/apply.scm 207 */
																BgL_refz00_bglt BgL_new1127z00_1524;

																{	/* Ast/apply.scm 210 */
																	BgL_refz00_bglt BgL_new1125z00_1526;

																	BgL_new1125z00_1526 =
																		((BgL_refz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_refz00_bgl))));
																	{	/* Ast/apply.scm 210 */
																		long BgL_arg1616z00_1527;

																		{	/* Ast/apply.scm 210 */
																			obj_t BgL_classz00_2007;

																			BgL_classz00_2007 =
																				BGl_refz00zzast_nodez00;
																			BgL_arg1616z00_1527 =
																				BGL_CLASS_NUM(BgL_classz00_2007);
																		}
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1125z00_1526),
																			BgL_arg1616z00_1527);
																	}
																	{	/* Ast/apply.scm 210 */
																		BgL_objectz00_bglt BgL_tmpz00_2776;

																		BgL_tmpz00_2776 =
																			((BgL_objectz00_bglt)
																			BgL_new1125z00_1526);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2776,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1125z00_1526);
																	BgL_new1127z00_1524 = BgL_new1125z00_1526;
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1127z00_1524)))->BgL_locz00) =
																	((obj_t) BgL_locz00_2311), BUNSPEC);
																{
																	BgL_typez00_bglt BgL_auxz00_2782;

																	{	/* Ast/apply.scm 209 */
																		BgL_typez00_bglt BgL_arg1615z00_1525;

																		BgL_arg1615z00_1525 =
																			(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						BgL_runnerz00_2313)))->BgL_typez00);
																		BgL_auxz00_2782 =
																			BGl_strictzd2nodezd2typez00zzast_nodez00((
																				(BgL_typez00_bglt)
																				BGl_za2_za2z00zztype_cachez00),
																			BgL_arg1615z00_1525);
																	}
																	((((BgL_nodez00_bglt) COBJECT(
																					((BgL_nodez00_bglt)
																						BgL_new1127z00_1524)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) BgL_auxz00_2782),
																		BUNSPEC);
																}
																((((BgL_varz00_bglt) COBJECT(
																				((BgL_varz00_bglt)
																					BgL_new1127z00_1524)))->
																		BgL_variablez00) =
																	((BgL_variablez00_bglt) (
																			(BgL_variablez00_bglt)
																			BgL_runnerz00_2313)), BUNSPEC);
																BgL_arg1613z00_1523 = BgL_new1127z00_1524;
															}
															BgL_arg1611z00_1522 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_arg1613z00_1523), BNIL);
														}
														BgL_arg1609z00_1521 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(4),
															BgL_arg1611z00_1522);
													}
													BgL_arg1602z00_1516 =
														MAKE_YOUNG_PAIR(BgL_arg1609z00_1521, BNIL);
												}
												BgL_arg1594z00_1514 =
													MAKE_YOUNG_PAIR(
													((obj_t) BgL_arg1595z00_1515), BgL_arg1602z00_1516);
											}
											BgL_arg1591z00_1512 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
												BgL_arg1594z00_1514);
										}
										{	/* Ast/apply.scm 211 */
											BgL_nodez00_bglt BgL_arg1625z00_1528;

											{	/* Ast/apply.scm 211 */
												obj_t BgL_arg1626z00_1529;

												BgL_arg1626z00_1529 = CDR(((obj_t) BgL_localsz00_1450));
												BgL_arg1625z00_1528 =
													BGl_loopze71ze7zzast_applyz00(BgL_sitez00_2317,
													BgL_procz00_2316, BgL_framez00_2315,
													BgL_stackz00_2314, BgL_runnerz00_2313,
													BgL_typez00_2312, BgL_locz00_2311,
													BgL_arg1626z00_1529);
											}
											BgL_arg1593z00_1513 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg1625z00_1528), BNIL);
										}
										BgL_arg1589z00_1511 =
											MAKE_YOUNG_PAIR(BgL_arg1591z00_1512, BgL_arg1593z00_1513);
									}
									BgL_arg1585z00_1510 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(11), BgL_arg1589z00_1511);
								}
								BgL_auxz00_2744 =
									BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_arg1585z00_1510,
									BgL_stackz00_2314, BgL_locz00_2311, CNST_TABLE_REF(1));
							}
						((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1119z00_1495))->
								BgL_bodyz00) = ((BgL_nodez00_bglt) BgL_auxz00_2744), BUNSPEC);
					}
					((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1119z00_1495))->
							BgL_removablezf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
					return ((BgL_nodez00_bglt) BgL_new1119z00_1495);
				}
		}

	}



/* va-known-app-ly->node */
	BgL_letzd2varzd2_bglt
		BGl_vazd2knownzd2appzd2lyzd2ze3nodeze3zzast_applyz00(obj_t BgL_stackz00_27,
		obj_t BgL_locz00_28, BgL_nodez00_bglt BgL_procz00_29,
		BgL_nodez00_bglt BgL_argz00_30, obj_t BgL_framez00_31, obj_t BgL_sitez00_32)
	{
		{	/* Ast/apply.scm 232 */
			{	/* Ast/apply.scm 233 */
				struct bgl_cell BgL_box1974_2310z00;
				obj_t BgL_framez00_2310;

				BgL_framez00_2310 =
					MAKE_CELL_STACK(BgL_framez00_31, BgL_box1974_2310z00);
				{	/* Ast/apply.scm 233 */
					BgL_localz00_bglt BgL_runnerz00_1538;
					BgL_typez00_bglt BgL_typez00_1539;

					{	/* Ast/apply.scm 233 */
						obj_t BgL_arg1750z00_1617;

						BgL_arg1750z00_1617 =
							BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
							(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(2)));
						BgL_runnerz00_1538 =
							BGl_makezd2localzd2svarz00zzast_localz00(BgL_arg1750z00_1617,
							((BgL_typez00_bglt) BGl_za2_za2z00zztype_cachez00));
					}
					{	/* Ast/apply.scm 234 */
						BgL_typez00_bglt BgL_arg1752z00_1619;

						BgL_arg1752z00_1619 =
							(((BgL_nodez00_bglt) COBJECT(BgL_procz00_29))->BgL_typez00);
						BgL_typez00_1539 =
							BGl_strictzd2nodezd2typez00zzast_nodez00(
							((BgL_typez00_bglt) BGl_za2_za2z00zztype_cachez00),
							BgL_arg1752z00_1619);
					}
					{	/* Ast/apply.scm 235 */
						bool_t BgL_test1975z00_2822;

						{	/* Ast/apply.scm 235 */
							obj_t BgL_objz00_2014;

							BgL_objz00_2014 = CELL_REF(BgL_framez00_2310);
							BgL_test1975z00_2822 = PAIRP(BgL_objz00_2014);
						}
						if (BgL_test1975z00_2822)
							{	/* Ast/apply.scm 236 */
								obj_t BgL_vz00_2016;

								BgL_vz00_2016 = CNST_TABLE_REF(3);
								((((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_runnerz00_1538)))->
										BgL_accessz00) = ((obj_t) BgL_vz00_2016), BUNSPEC);
							}
						else
							{	/* Ast/apply.scm 235 */
								BFALSE;
							}
					}
					{	/* Ast/apply.scm 237 */
						BgL_letzd2varzd2_bglt BgL_new1130z00_1541;

						{	/* Ast/apply.scm 238 */
							BgL_letzd2varzd2_bglt BgL_new1129z00_1615;

							BgL_new1129z00_1615 =
								((BgL_letzd2varzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_letzd2varzd2_bgl))));
							{	/* Ast/apply.scm 238 */
								long BgL_arg1749z00_1616;

								BgL_arg1749z00_1616 =
									BGL_CLASS_NUM(BGl_letzd2varzd2zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1129z00_1615),
									BgL_arg1749z00_1616);
							}
							{	/* Ast/apply.scm 238 */
								BgL_objectz00_bglt BgL_tmpz00_2831;

								BgL_tmpz00_2831 = ((BgL_objectz00_bglt) BgL_new1129z00_1615);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2831, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1129z00_1615);
							BgL_new1130z00_1541 = BgL_new1129z00_1615;
						}
						((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt) BgL_new1130z00_1541)))->BgL_locz00) =
							((obj_t) BgL_locz00_28), BUNSPEC);
						((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
											BgL_new1130z00_1541)))->BgL_typez00) =
							((BgL_typez00_bglt)
								BGl_strictzd2nodezd2typez00zzast_nodez00(((BgL_typez00_bglt)
										BGl_za2_za2z00zztype_cachez00), BgL_typez00_1539)),
							BUNSPEC);
						((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
											BgL_new1130z00_1541)))->BgL_sidezd2effectzd2) =
							((obj_t) BUNSPEC), BUNSPEC);
						((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
											BgL_new1130z00_1541)))->BgL_keyz00) =
							((obj_t) BINT(-1L)), BUNSPEC);
						{
							obj_t BgL_auxz00_2846;

							{	/* Ast/apply.scm 240 */
								obj_t BgL_arg1650z00_1542;

								BgL_arg1650z00_1542 =
									MAKE_YOUNG_PAIR(
									((obj_t) BgL_runnerz00_1538), ((obj_t) BgL_argz00_30));
								{	/* Ast/apply.scm 240 */
									obj_t BgL_list1651z00_1543;

									BgL_list1651z00_1543 =
										MAKE_YOUNG_PAIR(BgL_arg1650z00_1542, BNIL);
									BgL_auxz00_2846 = BgL_list1651z00_1543;
							}}
							((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1130z00_1541))->
									BgL_bindingsz00) = ((obj_t) BgL_auxz00_2846), BUNSPEC);
						}
						((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1130z00_1541))->
								BgL_bodyz00) =
							((BgL_nodez00_bglt) ((BgL_nodez00_bglt)
									BGl_loopze70ze7zzast_applyz00(BgL_procz00_29,
										BgL_framez00_2310, BgL_stackz00_27, BgL_runnerz00_1538,
										BgL_typez00_1539, BgL_locz00_28,
										CELL_REF(BgL_framez00_2310), BNIL))), BUNSPEC);
						((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1130z00_1541))->
								BgL_removablezf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
						return BgL_new1130z00_1541;
					}
				}
			}
		}

	}



/* loop~0 */
	BgL_nodezf2effectzf2_bglt BGl_loopze70ze7zzast_applyz00(BgL_nodez00_bglt
		BgL_procz00_2308, obj_t BgL_framez00_2307, obj_t BgL_stackz00_2306,
		BgL_localz00_bglt BgL_runnerz00_2305, BgL_typez00_bglt BgL_typez00_2304,
		obj_t BgL_locz00_2303, obj_t BgL_localsz00_1546, obj_t BgL_oldz00_1547)
	{
		{	/* Ast/apply.scm 241 */
			if (NULLP(BgL_localsz00_1546))
				{	/* Ast/apply.scm 243 */
					if (NULLP(BgL_oldz00_1547))
						{	/* Ast/apply.scm 246 */
							obj_t BgL_auxz00_2309;

							{	/* Ast/apply.scm 246 */
								obj_t BgL_list1655z00_1551;

								BgL_list1655z00_1551 =
									MAKE_YOUNG_PAIR(((obj_t) BgL_runnerz00_2305), BNIL);
								BgL_auxz00_2309 = BgL_list1655z00_1551;
							}
							CELL_SET(BgL_framez00_2307, BgL_auxz00_2309);
						}
					else
						{	/* Ast/apply.scm 247 */
							obj_t BgL_arg1661z00_1552;

							BgL_arg1661z00_1552 =
								MAKE_YOUNG_PAIR(((obj_t) BgL_runnerz00_2305), BNIL);
							{	/* Ast/apply.scm 247 */
								obj_t BgL_tmpz00_2864;

								BgL_tmpz00_2864 = ((obj_t) BgL_oldz00_1547);
								SET_CDR(BgL_tmpz00_2864, BgL_arg1661z00_1552);
							}
						}
					{	/* Ast/apply.scm 248 */
						BgL_appz00_bglt BgL_new1134z00_1553;

						{	/* Ast/apply.scm 249 */
							BgL_appz00_bglt BgL_new1133z00_1578;

							BgL_new1133z00_1578 =
								((BgL_appz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_appz00_bgl))));
							{	/* Ast/apply.scm 249 */
								long BgL_arg1699z00_1579;

								BgL_arg1699z00_1579 = BGL_CLASS_NUM(BGl_appz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1133z00_1578),
									BgL_arg1699z00_1579);
							}
							{	/* Ast/apply.scm 249 */
								BgL_objectz00_bglt BgL_tmpz00_2871;

								BgL_tmpz00_2871 = ((BgL_objectz00_bglt) BgL_new1133z00_1578);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2871, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1133z00_1578);
							BgL_new1134z00_1553 = BgL_new1133z00_1578;
						}
						((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt) BgL_new1134z00_1553)))->BgL_locz00) =
							((obj_t) BgL_locz00_2303), BUNSPEC);
						{
							BgL_typez00_bglt BgL_auxz00_2877;

							{	/* Ast/apply.scm 250 */
								BgL_typez00_bglt BgL_arg1663z00_1554;

								BgL_arg1663z00_1554 =
									(((BgL_variablez00_bglt) COBJECT(
											(((BgL_varz00_bglt) COBJECT(
														((BgL_varz00_bglt) BgL_procz00_2308)))->
												BgL_variablez00)))->BgL_typez00);
								BgL_auxz00_2877 =
									BGl_strictzd2nodezd2typez00zzast_nodez00(((BgL_typez00_bglt)
										BGl_za2_za2z00zztype_cachez00), BgL_arg1663z00_1554);
							}
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt) BgL_new1134z00_1553)))->BgL_typez00) =
								((BgL_typez00_bglt) BgL_auxz00_2877), BUNSPEC);
						}
						((((BgL_nodezf2effectzf2_bglt) COBJECT(
										((BgL_nodezf2effectzf2_bglt) BgL_new1134z00_1553)))->
								BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
						((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
											BgL_new1134z00_1553)))->BgL_keyz00) =
							((obj_t) BINT(-1L)), BUNSPEC);
						{
							BgL_varz00_bglt BgL_auxz00_2890;

							{	/* Ast/apply.scm 251 */
								BgL_refz00_bglt BgL_new1135z00_1557;

								{	/* Ast/apply.scm 251 */
									BgL_refz00_bglt BgL_new1139z00_1559;

									BgL_new1139z00_1559 =
										((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_refz00_bgl))));
									{	/* Ast/apply.scm 251 */
										long BgL_arg1678z00_1560;

										{	/* Ast/apply.scm 251 */
											obj_t BgL_classz00_2030;

											BgL_classz00_2030 = BGl_refz00zzast_nodez00;
											BgL_arg1678z00_1560 = BGL_CLASS_NUM(BgL_classz00_2030);
										}
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1139z00_1559),
											BgL_arg1678z00_1560);
									}
									{	/* Ast/apply.scm 251 */
										BgL_objectz00_bglt BgL_tmpz00_2895;

										BgL_tmpz00_2895 =
											((BgL_objectz00_bglt) BgL_new1139z00_1559);
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2895, BFALSE);
									}
									((BgL_objectz00_bglt) BgL_new1139z00_1559);
									BgL_new1135z00_1557 = BgL_new1139z00_1559;
								}
								((((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt) BgL_new1135z00_1557)))->
										BgL_locz00) =
									((obj_t) (((BgL_nodez00_bglt) COBJECT(BgL_procz00_2308))->
											BgL_locz00)), BUNSPEC);
								((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
													BgL_new1135z00_1557)))->BgL_typez00) =
									((BgL_typez00_bglt) (((BgL_nodez00_bglt)
												COBJECT(BgL_procz00_2308))->BgL_typez00)), BUNSPEC);
								((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
													BgL_new1135z00_1557)))->BgL_variablez00) =
									((BgL_variablez00_bglt) (((BgL_varz00_bglt)
												COBJECT(((BgL_varz00_bglt) BgL_procz00_2308)))->
											BgL_variablez00)), BUNSPEC);
								BgL_auxz00_2890 = ((BgL_varz00_bglt) BgL_new1135z00_1557);
							}
							((((BgL_appz00_bglt) COBJECT(BgL_new1134z00_1553))->BgL_funz00) =
								((BgL_varz00_bglt) BgL_auxz00_2890), BUNSPEC);
						}
						{
							obj_t BgL_auxz00_2911;

							{	/* Ast/apply.scm 252 */
								obj_t BgL_l1270z00_1561;

								BgL_l1270z00_1561 = CELL_REF(BgL_framez00_2307);
								if (NULLP(BgL_l1270z00_1561))
									{	/* Ast/apply.scm 252 */
										BgL_auxz00_2911 = BNIL;
									}
								else
									{	/* Ast/apply.scm 252 */
										obj_t BgL_head1272z00_1563;

										BgL_head1272z00_1563 = MAKE_YOUNG_PAIR(BNIL, BNIL);
										{
											obj_t BgL_l1270z00_1565;
											obj_t BgL_tail1273z00_1566;

											BgL_l1270z00_1565 = BgL_l1270z00_1561;
											BgL_tail1273z00_1566 = BgL_head1272z00_1563;
										BgL_zc3z04anonymousza31680ze3z87_1567:
											if (NULLP(BgL_l1270z00_1565))
												{	/* Ast/apply.scm 252 */
													BgL_auxz00_2911 = CDR(BgL_head1272z00_1563);
												}
											else
												{	/* Ast/apply.scm 252 */
													obj_t BgL_newtail1274z00_1569;

													{	/* Ast/apply.scm 252 */
														BgL_refz00_bglt BgL_arg1689z00_1571;

														{	/* Ast/apply.scm 252 */
															obj_t BgL_localz00_1572;

															BgL_localz00_1572 =
																CAR(((obj_t) BgL_l1270z00_1565));
															{	/* Ast/apply.scm 253 */
																BgL_refz00_bglt BgL_new1141z00_1573;

																{	/* Ast/apply.scm 256 */
																	BgL_refz00_bglt BgL_new1140z00_1575;

																	BgL_new1140z00_1575 =
																		((BgL_refz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_refz00_bgl))));
																	{	/* Ast/apply.scm 256 */
																		long BgL_arg1692z00_1576;

																		{	/* Ast/apply.scm 256 */
																			obj_t BgL_classz00_2036;

																			BgL_classz00_2036 =
																				BGl_refz00zzast_nodez00;
																			BgL_arg1692z00_1576 =
																				BGL_CLASS_NUM(BgL_classz00_2036);
																		}
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1140z00_1575),
																			BgL_arg1692z00_1576);
																	}
																	{	/* Ast/apply.scm 256 */
																		BgL_objectz00_bglt BgL_tmpz00_2924;

																		BgL_tmpz00_2924 =
																			((BgL_objectz00_bglt)
																			BgL_new1140z00_1575);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2924,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1140z00_1575);
																	BgL_new1141z00_1573 = BgL_new1140z00_1575;
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1141z00_1573)))->BgL_locz00) =
																	((obj_t) BgL_locz00_2303), BUNSPEC);
																{
																	BgL_typez00_bglt BgL_auxz00_2930;

																	{	/* Ast/apply.scm 255 */
																		BgL_typez00_bglt BgL_arg1691z00_1574;

																		BgL_arg1691z00_1574 =
																			(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						((BgL_localz00_bglt)
																							BgL_localz00_1572))))->
																			BgL_typez00);
																		BgL_auxz00_2930 =
																			BGl_strictzd2nodezd2typez00zzast_nodez00((
																				(BgL_typez00_bglt)
																				BGl_za2_za2z00zztype_cachez00),
																			BgL_arg1691z00_1574);
																	}
																	((((BgL_nodez00_bglt) COBJECT(
																					((BgL_nodez00_bglt)
																						BgL_new1141z00_1573)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) BgL_auxz00_2930),
																		BUNSPEC);
																}
																((((BgL_varz00_bglt) COBJECT(
																				((BgL_varz00_bglt)
																					BgL_new1141z00_1573)))->
																		BgL_variablez00) =
																	((BgL_variablez00_bglt) (
																			(BgL_variablez00_bglt)
																			BgL_localz00_1572)), BUNSPEC);
																BgL_arg1689z00_1571 = BgL_new1141z00_1573;
														}}
														BgL_newtail1274z00_1569 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_arg1689z00_1571), BNIL);
													}
													SET_CDR(BgL_tail1273z00_1566,
														BgL_newtail1274z00_1569);
													{	/* Ast/apply.scm 252 */
														obj_t BgL_arg1688z00_1570;

														BgL_arg1688z00_1570 =
															CDR(((obj_t) BgL_l1270z00_1565));
														{
															obj_t BgL_tail1273z00_2947;
															obj_t BgL_l1270z00_2946;

															BgL_l1270z00_2946 = BgL_arg1688z00_1570;
															BgL_tail1273z00_2947 = BgL_newtail1274z00_1569;
															BgL_tail1273z00_1566 = BgL_tail1273z00_2947;
															BgL_l1270z00_1565 = BgL_l1270z00_2946;
															goto BgL_zc3z04anonymousza31680ze3z87_1567;
														}
													}
												}
										}
									}
							}
							((((BgL_appz00_bglt) COBJECT(BgL_new1134z00_1553))->BgL_argsz00) =
								((obj_t) BgL_auxz00_2911), BUNSPEC);
						}
						((((BgL_appz00_bglt) COBJECT(BgL_new1134z00_1553))->
								BgL_stackablez00) = ((obj_t) BFALSE), BUNSPEC);
						return ((BgL_nodezf2effectzf2_bglt) BgL_new1134z00_1553);
					}
				}
			else
				{	/* Ast/apply.scm 258 */
					BgL_letzd2varzd2_bglt BgL_new1143z00_1580;

					{	/* Ast/apply.scm 259 */
						BgL_letzd2varzd2_bglt BgL_new1142z00_1612;

						BgL_new1142z00_1612 =
							((BgL_letzd2varzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_letzd2varzd2_bgl))));
						{	/* Ast/apply.scm 259 */
							long BgL_arg1748z00_1613;

							BgL_arg1748z00_1613 =
								BGL_CLASS_NUM(BGl_letzd2varzd2zzast_nodez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1142z00_1612),
								BgL_arg1748z00_1613);
						}
						{	/* Ast/apply.scm 259 */
							BgL_objectz00_bglt BgL_tmpz00_2955;

							BgL_tmpz00_2955 = ((BgL_objectz00_bglt) BgL_new1142z00_1612);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2955, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_new1142z00_1612);
						BgL_new1143z00_1580 = BgL_new1142z00_1612;
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_new1143z00_1580)))->BgL_locz00) =
						((obj_t) BgL_locz00_2303), BUNSPEC);
					((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_new1143z00_1580)))->BgL_typez00) =
						((BgL_typez00_bglt)
							BGl_strictzd2nodezd2typez00zzast_nodez00(((BgL_typez00_bglt)
									BGl_za2_za2z00zztype_cachez00), BgL_typez00_2304)), BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1143z00_1580)))->BgL_sidezd2effectzd2) =
						((obj_t) BUNSPEC), BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1143z00_1580)))->BgL_keyz00) =
						((obj_t) BINT(-1L)), BUNSPEC);
					{
						obj_t BgL_auxz00_2970;

						{	/* Ast/apply.scm 262 */
							obj_t BgL_arg1700z00_1581;

							{	/* Ast/apply.scm 262 */
								obj_t BgL_arg1702z00_1583;
								BgL_nodez00_bglt BgL_arg1703z00_1584;

								BgL_arg1702z00_1583 = CAR(((obj_t) BgL_localsz00_1546));
								{	/* Ast/apply.scm 265 */
									obj_t BgL_arg1705z00_1585;

									{	/* Ast/apply.scm 265 */
										obj_t BgL_arg1708z00_1586;

										{	/* Ast/apply.scm 265 */
											BgL_refz00_bglt BgL_arg1709z00_1587;

											{	/* Ast/apply.scm 265 */
												BgL_refz00_bglt BgL_new1145z00_1588;

												{	/* Ast/apply.scm 268 */
													BgL_refz00_bglt BgL_new1144z00_1590;

													BgL_new1144z00_1590 =
														((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_refz00_bgl))));
													{	/* Ast/apply.scm 268 */
														long BgL_arg1711z00_1591;

														{	/* Ast/apply.scm 268 */
															obj_t BgL_classz00_2048;

															BgL_classz00_2048 = BGl_refz00zzast_nodez00;
															BgL_arg1711z00_1591 =
																BGL_CLASS_NUM(BgL_classz00_2048);
														}
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1144z00_1590),
															BgL_arg1711z00_1591);
													}
													{	/* Ast/apply.scm 268 */
														BgL_objectz00_bglt BgL_tmpz00_2977;

														BgL_tmpz00_2977 =
															((BgL_objectz00_bglt) BgL_new1144z00_1590);
														BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2977, BFALSE);
													}
													((BgL_objectz00_bglt) BgL_new1144z00_1590);
													BgL_new1145z00_1588 = BgL_new1144z00_1590;
												}
												((((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt) BgL_new1145z00_1588)))->
														BgL_locz00) = ((obj_t) BgL_locz00_2303), BUNSPEC);
												{
													BgL_typez00_bglt BgL_auxz00_2983;

													{	/* Ast/apply.scm 267 */
														BgL_typez00_bglt BgL_arg1710z00_1589;

														BgL_arg1710z00_1589 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		BgL_runnerz00_2305)))->BgL_typez00);
														BgL_auxz00_2983 =
															BGl_strictzd2nodezd2typez00zzast_nodez00((
																(BgL_typez00_bglt)
																BGl_za2_za2z00zztype_cachez00),
															BgL_arg1710z00_1589);
													}
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_new1145z00_1588)))->
															BgL_typez00) =
														((BgL_typez00_bglt) BgL_auxz00_2983), BUNSPEC);
												}
												((((BgL_varz00_bglt) COBJECT(
																((BgL_varz00_bglt) BgL_new1145z00_1588)))->
														BgL_variablez00) =
													((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
															BgL_runnerz00_2305)), BUNSPEC);
												BgL_arg1709z00_1587 = BgL_new1145z00_1588;
											}
											BgL_arg1708z00_1586 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg1709z00_1587), BNIL);
										}
										BgL_arg1705z00_1585 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(9), BgL_arg1708z00_1586);
									}
									BgL_arg1703z00_1584 =
										BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_arg1705z00_1585,
										BgL_stackz00_2306, BgL_locz00_2303, CNST_TABLE_REF(1));
								}
								BgL_arg1700z00_1581 =
									MAKE_YOUNG_PAIR(BgL_arg1702z00_1583,
									((obj_t) BgL_arg1703z00_1584));
							}
							{	/* Ast/apply.scm 261 */
								obj_t BgL_list1701z00_1582;

								BgL_list1701z00_1582 =
									MAKE_YOUNG_PAIR(BgL_arg1700z00_1581, BNIL);
								BgL_auxz00_2970 = BgL_list1701z00_1582;
						}}
						((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1143z00_1580))->
								BgL_bindingsz00) = ((obj_t) BgL_auxz00_2970), BUNSPEC);
					}
					{
						BgL_nodez00_bglt BgL_auxz00_3003;

						{	/* Ast/apply.scm 274 */
							obj_t BgL_arg1714z00_1592;

							{	/* Ast/apply.scm 274 */
								obj_t BgL_arg1717z00_1593;

								{	/* Ast/apply.scm 274 */
									obj_t BgL_arg1718z00_1594;
									obj_t BgL_arg1720z00_1595;

									{	/* Ast/apply.scm 274 */
										obj_t BgL_arg1722z00_1596;

										{	/* Ast/apply.scm 274 */
											BgL_refz00_bglt BgL_arg1724z00_1597;
											obj_t BgL_arg1733z00_1598;

											{	/* Ast/apply.scm 274 */
												BgL_refz00_bglt BgL_new1147z00_1599;

												{	/* Ast/apply.scm 277 */
													BgL_refz00_bglt BgL_new1146z00_1601;

													BgL_new1146z00_1601 =
														((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_refz00_bgl))));
													{	/* Ast/apply.scm 277 */
														long BgL_arg1735z00_1602;

														{	/* Ast/apply.scm 277 */
															obj_t BgL_classz00_2054;

															BgL_classz00_2054 = BGl_refz00zzast_nodez00;
															BgL_arg1735z00_1602 =
																BGL_CLASS_NUM(BgL_classz00_2054);
														}
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1146z00_1601),
															BgL_arg1735z00_1602);
													}
													{	/* Ast/apply.scm 277 */
														BgL_objectz00_bglt BgL_tmpz00_3008;

														BgL_tmpz00_3008 =
															((BgL_objectz00_bglt) BgL_new1146z00_1601);
														BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3008, BFALSE);
													}
													((BgL_objectz00_bglt) BgL_new1146z00_1601);
													BgL_new1147z00_1599 = BgL_new1146z00_1601;
												}
												((((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt) BgL_new1147z00_1599)))->
														BgL_locz00) = ((obj_t) BgL_locz00_2303), BUNSPEC);
												{
													BgL_typez00_bglt BgL_auxz00_3014;

													{	/* Ast/apply.scm 276 */
														BgL_typez00_bglt BgL_arg1734z00_1600;

														BgL_arg1734z00_1600 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		BgL_runnerz00_2305)))->BgL_typez00);
														BgL_auxz00_3014 =
															BGl_strictzd2nodezd2typez00zzast_nodez00((
																(BgL_typez00_bglt)
																BGl_za2_za2z00zztype_cachez00),
															BgL_arg1734z00_1600);
													}
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_new1147z00_1599)))->
															BgL_typez00) =
														((BgL_typez00_bglt) BgL_auxz00_3014), BUNSPEC);
												}
												((((BgL_varz00_bglt) COBJECT(
																((BgL_varz00_bglt) BgL_new1147z00_1599)))->
														BgL_variablez00) =
													((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
															BgL_runnerz00_2305)), BUNSPEC);
												BgL_arg1724z00_1597 = BgL_new1147z00_1599;
											}
											{	/* Ast/apply.scm 278 */
												obj_t BgL_arg1736z00_1603;

												{	/* Ast/apply.scm 278 */
													obj_t BgL_arg1737z00_1604;

													{	/* Ast/apply.scm 278 */
														BgL_refz00_bglt BgL_arg1738z00_1605;

														{	/* Ast/apply.scm 278 */
															BgL_refz00_bglt BgL_new1149z00_1606;

															{	/* Ast/apply.scm 281 */
																BgL_refz00_bglt BgL_new1148z00_1608;

																BgL_new1148z00_1608 =
																	((BgL_refz00_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_refz00_bgl))));
																{	/* Ast/apply.scm 281 */
																	long BgL_arg1740z00_1609;

																	{	/* Ast/apply.scm 281 */
																		obj_t BgL_classz00_2059;

																		BgL_classz00_2059 = BGl_refz00zzast_nodez00;
																		BgL_arg1740z00_1609 =
																			BGL_CLASS_NUM(BgL_classz00_2059);
																	}
																	BGL_OBJECT_CLASS_NUM_SET(
																		((BgL_objectz00_bglt) BgL_new1148z00_1608),
																		BgL_arg1740z00_1609);
																}
																{	/* Ast/apply.scm 281 */
																	BgL_objectz00_bglt BgL_tmpz00_3028;

																	BgL_tmpz00_3028 =
																		((BgL_objectz00_bglt) BgL_new1148z00_1608);
																	BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3028,
																		BFALSE);
																}
																((BgL_objectz00_bglt) BgL_new1148z00_1608);
																BgL_new1149z00_1606 = BgL_new1148z00_1608;
															}
															((((BgL_nodez00_bglt) COBJECT(
																			((BgL_nodez00_bglt)
																				BgL_new1149z00_1606)))->BgL_locz00) =
																((obj_t) BgL_locz00_2303), BUNSPEC);
															{
																BgL_typez00_bglt BgL_auxz00_3034;

																{	/* Ast/apply.scm 280 */
																	BgL_typez00_bglt BgL_arg1739z00_1607;

																	BgL_arg1739z00_1607 =
																		(((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					BgL_runnerz00_2305)))->BgL_typez00);
																	BgL_auxz00_3034 =
																		BGl_strictzd2nodezd2typez00zzast_nodez00((
																			(BgL_typez00_bglt)
																			BGl_za2_za2z00zztype_cachez00),
																		BgL_arg1739z00_1607);
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1149z00_1606)))->BgL_typez00) =
																	((BgL_typez00_bglt) BgL_auxz00_3034),
																	BUNSPEC);
															}
															((((BgL_varz00_bglt) COBJECT(
																			((BgL_varz00_bglt)
																				BgL_new1149z00_1606)))->
																	BgL_variablez00) =
																((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
																		BgL_runnerz00_2305)), BUNSPEC);
															BgL_arg1738z00_1605 = BgL_new1149z00_1606;
														}
														BgL_arg1737z00_1604 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_arg1738z00_1605), BNIL);
													}
													BgL_arg1736z00_1603 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(4),
														BgL_arg1737z00_1604);
												}
												BgL_arg1733z00_1598 =
													MAKE_YOUNG_PAIR(BgL_arg1736z00_1603, BNIL);
											}
											BgL_arg1722z00_1596 =
												MAKE_YOUNG_PAIR(
												((obj_t) BgL_arg1724z00_1597), BgL_arg1733z00_1598);
										}
										BgL_arg1718z00_1594 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(10), BgL_arg1722z00_1596);
									}
									{	/* Ast/apply.scm 282 */
										BgL_nodezf2effectzf2_bglt BgL_arg1746z00_1610;

										{	/* Ast/apply.scm 282 */
											obj_t BgL_arg1747z00_1611;

											BgL_arg1747z00_1611 = CDR(((obj_t) BgL_localsz00_1546));
											BgL_arg1746z00_1610 =
												BGl_loopze70ze7zzast_applyz00(BgL_procz00_2308,
												BgL_framez00_2307, BgL_stackz00_2306,
												BgL_runnerz00_2305, BgL_typez00_2304, BgL_locz00_2303,
												BgL_arg1747z00_1611, BgL_localsz00_1546);
										}
										BgL_arg1720z00_1595 =
											MAKE_YOUNG_PAIR(((obj_t) BgL_arg1746z00_1610), BNIL);
									}
									BgL_arg1717z00_1593 =
										MAKE_YOUNG_PAIR(BgL_arg1718z00_1594, BgL_arg1720z00_1595);
								}
								BgL_arg1714z00_1592 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(11), BgL_arg1717z00_1593);
							}
							BgL_auxz00_3003 =
								BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_arg1714z00_1592,
								BgL_stackz00_2306, BgL_locz00_2303, CNST_TABLE_REF(1));
						}
						((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1143z00_1580))->
								BgL_bodyz00) = ((BgL_nodez00_bglt) BgL_auxz00_3003), BUNSPEC);
					}
					((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1143z00_1580))->
							BgL_removablezf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
					return ((BgL_nodezf2effectzf2_bglt) BgL_new1143z00_1580);
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_applyz00(void)
	{
		{	/* Ast/apply.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_applyz00(void)
	{
		{	/* Ast/apply.scm 14 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_makezd2funzd2framezd2envzd2zzast_applyz00,
				BGl_proc1921z00zzast_applyz00, BGl_funz00zzast_varz00,
				BGl_string1922z00zzast_applyz00);
		}

	}



/* &make-fun-frame1275 */
	obj_t BGl_z62makezd2funzd2frame1275z62zzast_applyz00(obj_t BgL_envz00_2293,
		obj_t BgL_funz00_2294)
	{
		{	/* Ast/apply.scm 61 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(12),
				BGl_string1923z00zzast_applyz00,
				((obj_t) ((BgL_funz00_bglt) BgL_funz00_2294)));
		}

	}



/* make-fun-frame */
	obj_t BGl_makezd2funzd2framez00zzast_applyz00(BgL_funz00_bglt BgL_funz00_7)
	{
		{	/* Ast/apply.scm 61 */
			{	/* Ast/apply.scm 61 */
				obj_t BgL_method1276z00_1624;

				{	/* Ast/apply.scm 61 */
					obj_t BgL_res1912z00_2095;

					{	/* Ast/apply.scm 61 */
						long BgL_objzd2classzd2numz00_2066;

						BgL_objzd2classzd2numz00_2066 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_funz00_7));
						{	/* Ast/apply.scm 61 */
							obj_t BgL_arg1811z00_2067;

							BgL_arg1811z00_2067 =
								PROCEDURE_REF(BGl_makezd2funzd2framezd2envzd2zzast_applyz00,
								(int) (1L));
							{	/* Ast/apply.scm 61 */
								int BgL_offsetz00_2070;

								BgL_offsetz00_2070 = (int) (BgL_objzd2classzd2numz00_2066);
								{	/* Ast/apply.scm 61 */
									long BgL_offsetz00_2071;

									BgL_offsetz00_2071 =
										((long) (BgL_offsetz00_2070) - OBJECT_TYPE);
									{	/* Ast/apply.scm 61 */
										long BgL_modz00_2072;

										BgL_modz00_2072 =
											(BgL_offsetz00_2071 >> (int) ((long) ((int) (4L))));
										{	/* Ast/apply.scm 61 */
											long BgL_restz00_2074;

											BgL_restz00_2074 =
												(BgL_offsetz00_2071 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/apply.scm 61 */

												{	/* Ast/apply.scm 61 */
													obj_t BgL_bucketz00_2076;

													BgL_bucketz00_2076 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2067), BgL_modz00_2072);
													BgL_res1912z00_2095 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2076), BgL_restz00_2074);
					}}}}}}}}
					BgL_method1276z00_1624 = BgL_res1912z00_2095;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1276z00_1624, ((obj_t) BgL_funz00_7));
			}
		}

	}



/* &make-fun-frame */
	obj_t BGl_z62makezd2funzd2framez62zzast_applyz00(obj_t BgL_envz00_2295,
		obj_t BgL_funz00_2296)
	{
		{	/* Ast/apply.scm 61 */
			return
				BGl_makezd2funzd2framez00zzast_applyz00(
				((BgL_funz00_bglt) BgL_funz00_2296));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_applyz00(void)
	{
		{	/* Ast/apply.scm 14 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_makezd2funzd2framezd2envzd2zzast_applyz00, BGl_sfunz00zzast_varz00,
				BGl_proc1924z00zzast_applyz00, BGl_string1925z00zzast_applyz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_makezd2funzd2framezd2envzd2zzast_applyz00, BGl_cfunz00zzast_varz00,
				BGl_proc1926z00zzast_applyz00, BGl_string1925z00zzast_applyz00);
		}

	}



/* &make-fun-frame-cfun1281 */
	obj_t BGl_z62makezd2funzd2framezd2cfun1281zb0zzast_applyz00(obj_t
		BgL_envz00_2299, obj_t BgL_funz00_2300)
	{
		{	/* Ast/apply.scm 86 */
			{	/* Ast/apply.scm 87 */
				long BgL_arityz00_2331;

				BgL_arityz00_2331 =
					(((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt)
								((BgL_cfunz00_bglt) BgL_funz00_2300))))->BgL_arityz00);
				{	/* Ast/apply.scm 88 */
					obj_t BgL_g1108z00_2332;

					BgL_g1108z00_2332 =
						(((BgL_cfunz00_bglt) COBJECT(
								((BgL_cfunz00_bglt) BgL_funz00_2300)))->BgL_argszd2typezd2);
					{
						obj_t BgL_typesz00_2334;
						obj_t BgL_localsz00_2335;

						BgL_typesz00_2334 = BgL_g1108z00_2332;
						BgL_localsz00_2335 = BNIL;
					BgL_loopz00_2333:
						if (NULLP(BgL_typesz00_2334))
							{	/* Ast/apply.scm 91 */
								return bgl_reverse_bang(BgL_localsz00_2335);
							}
						else
							{	/* Ast/apply.scm 93 */
								bool_t BgL_test1981z00_3113;

								if (NULLP(CDR(((obj_t) BgL_typesz00_2334))))
									{	/* Ast/apply.scm 93 */
										BgL_test1981z00_3113 = (BgL_arityz00_2331 < 0L);
									}
								else
									{	/* Ast/apply.scm 93 */
										BgL_test1981z00_3113 = ((bool_t) 0);
									}
								if (BgL_test1981z00_3113)
									{	/* Ast/apply.scm 93 */
										return bgl_reverse_bang(BgL_localsz00_2335);
									}
								else
									{	/* Ast/apply.scm 96 */
										obj_t BgL_arg1822z00_2336;
										obj_t BgL_arg1823z00_2337;

										BgL_arg1822z00_2336 = CDR(((obj_t) BgL_typesz00_2334));
										{	/* Ast/apply.scm 97 */
											BgL_localz00_bglt BgL_arg1831z00_2338;

											{	/* Ast/apply.scm 97 */
												obj_t BgL_arg1832z00_2339;
												obj_t BgL_arg1833z00_2340;

												BgL_arg1832z00_2339 =
													BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
													(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF
														(13)));
												BgL_arg1833z00_2340 = CAR(((obj_t) BgL_typesz00_2334));
												BgL_arg1831z00_2338 =
													BGl_makezd2localzd2svarz00zzast_localz00
													(BgL_arg1832z00_2339,
													((BgL_typez00_bglt) BgL_arg1833z00_2340));
											}
											BgL_arg1823z00_2337 =
												MAKE_YOUNG_PAIR(
												((obj_t) BgL_arg1831z00_2338), BgL_localsz00_2335);
										}
										{
											obj_t BgL_localsz00_3132;
											obj_t BgL_typesz00_3131;

											BgL_typesz00_3131 = BgL_arg1822z00_2336;
											BgL_localsz00_3132 = BgL_arg1823z00_2337;
											BgL_localsz00_2335 = BgL_localsz00_3132;
											BgL_typesz00_2334 = BgL_typesz00_3131;
											goto BgL_loopz00_2333;
										}
									}
							}
					}
				}
			}
		}

	}



/* &make-fun-frame-sfun1279 */
	obj_t BGl_z62makezd2funzd2framezd2sfun1279zb0zzast_applyz00(obj_t
		BgL_envz00_2301, obj_t BgL_funz00_2302)
	{
		{	/* Ast/apply.scm 66 */
			{	/* Ast/apply.scm 67 */
				long BgL_arityz00_2342;

				BgL_arityz00_2342 =
					(((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt)
								((BgL_sfunz00_bglt) BgL_funz00_2302))))->BgL_arityz00);
				{	/* Ast/apply.scm 68 */
					obj_t BgL_g1106z00_2343;

					BgL_g1106z00_2343 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt) BgL_funz00_2302)))->BgL_argsz00);
					{
						obj_t BgL_formalsz00_2345;
						obj_t BgL_localsz00_2346;

						BgL_formalsz00_2345 = BgL_g1106z00_2343;
						BgL_localsz00_2346 = BNIL;
					BgL_loopz00_2344:
						if (NULLP(BgL_formalsz00_2345))
							{	/* Ast/apply.scm 71 */
								return bgl_reverse_bang(BgL_localsz00_2346);
							}
						else
							{	/* Ast/apply.scm 73 */
								bool_t BgL_test1984z00_3141;

								if (NULLP(CDR(((obj_t) BgL_formalsz00_2345))))
									{	/* Ast/apply.scm 73 */
										BgL_test1984z00_3141 = (BgL_arityz00_2342 < 0L);
									}
								else
									{	/* Ast/apply.scm 73 */
										BgL_test1984z00_3141 = ((bool_t) 0);
									}
								if (BgL_test1984z00_3141)
									{	/* Ast/apply.scm 73 */
										return bgl_reverse_bang(BgL_localsz00_2346);
									}
								else
									{	/* Ast/apply.scm 76 */
										obj_t BgL_arg1767z00_2347;
										obj_t BgL_arg1770z00_2348;

										BgL_arg1767z00_2347 = CDR(((obj_t) BgL_formalsz00_2345));
										{	/* Ast/apply.scm 77 */
											BgL_localz00_bglt BgL_arg1771z00_2349;

											{	/* Ast/apply.scm 77 */
												obj_t BgL_arg1773z00_2350;
												obj_t BgL_arg1775z00_2351;

												BgL_arg1773z00_2350 =
													BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
													(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF
														(13)));
												{	/* Ast/apply.scm 78 */
													bool_t BgL_test1986z00_3153;

													{	/* Ast/apply.scm 78 */
														obj_t BgL_arg1806z00_2352;

														BgL_arg1806z00_2352 =
															CAR(((obj_t) BgL_formalsz00_2345));
														{	/* Ast/apply.scm 78 */
															obj_t BgL_classz00_2353;

															BgL_classz00_2353 = BGl_typez00zztype_typez00;
															if (BGL_OBJECTP(BgL_arg1806z00_2352))
																{	/* Ast/apply.scm 78 */
																	BgL_objectz00_bglt BgL_arg1807z00_2354;

																	BgL_arg1807z00_2354 =
																		(BgL_objectz00_bglt) (BgL_arg1806z00_2352);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Ast/apply.scm 78 */
																			long BgL_idxz00_2355;

																			BgL_idxz00_2355 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_2354);
																			BgL_test1986z00_3153 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_2355 + 1L)) ==
																				BgL_classz00_2353);
																		}
																	else
																		{	/* Ast/apply.scm 78 */
																			bool_t BgL_res1913z00_2358;

																			{	/* Ast/apply.scm 78 */
																				obj_t BgL_oclassz00_2359;

																				{	/* Ast/apply.scm 78 */
																					obj_t BgL_arg1815z00_2360;
																					long BgL_arg1816z00_2361;

																					BgL_arg1815z00_2360 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Ast/apply.scm 78 */
																						long BgL_arg1817z00_2362;

																						BgL_arg1817z00_2362 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_2354);
																						BgL_arg1816z00_2361 =
																							(BgL_arg1817z00_2362 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_2359 =
																						VECTOR_REF(BgL_arg1815z00_2360,
																						BgL_arg1816z00_2361);
																				}
																				{	/* Ast/apply.scm 78 */
																					bool_t BgL__ortest_1115z00_2363;

																					BgL__ortest_1115z00_2363 =
																						(BgL_classz00_2353 ==
																						BgL_oclassz00_2359);
																					if (BgL__ortest_1115z00_2363)
																						{	/* Ast/apply.scm 78 */
																							BgL_res1913z00_2358 =
																								BgL__ortest_1115z00_2363;
																						}
																					else
																						{	/* Ast/apply.scm 78 */
																							long BgL_odepthz00_2364;

																							{	/* Ast/apply.scm 78 */
																								obj_t BgL_arg1804z00_2365;

																								BgL_arg1804z00_2365 =
																									(BgL_oclassz00_2359);
																								BgL_odepthz00_2364 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_2365);
																							}
																							if ((1L < BgL_odepthz00_2364))
																								{	/* Ast/apply.scm 78 */
																									obj_t BgL_arg1802z00_2366;

																									{	/* Ast/apply.scm 78 */
																										obj_t BgL_arg1803z00_2367;

																										BgL_arg1803z00_2367 =
																											(BgL_oclassz00_2359);
																										BgL_arg1802z00_2366 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_2367, 1L);
																									}
																									BgL_res1913z00_2358 =
																										(BgL_arg1802z00_2366 ==
																										BgL_classz00_2353);
																								}
																							else
																								{	/* Ast/apply.scm 78 */
																									BgL_res1913z00_2358 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test1986z00_3153 =
																				BgL_res1913z00_2358;
																		}
																}
															else
																{	/* Ast/apply.scm 78 */
																	BgL_test1986z00_3153 = ((bool_t) 0);
																}
														}
													}
													if (BgL_test1986z00_3153)
														{	/* Ast/apply.scm 78 */
															BgL_arg1775z00_2351 =
																CAR(((obj_t) BgL_formalsz00_2345));
														}
													else
														{	/* Ast/apply.scm 78 */
															BgL_arg1775z00_2351 =
																((obj_t)
																(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_localz00_bglt)
																					CAR(
																						((obj_t) BgL_formalsz00_2345))))))->
																	BgL_typez00));
														}
												}
												BgL_arg1771z00_2349 =
													BGl_makezd2localzd2svarz00zzast_localz00
													(BgL_arg1773z00_2350,
													((BgL_typez00_bglt) BgL_arg1775z00_2351));
											}
											BgL_arg1770z00_2348 =
												MAKE_YOUNG_PAIR(
												((obj_t) BgL_arg1771z00_2349), BgL_localsz00_2346);
										}
										{
											obj_t BgL_localsz00_3191;
											obj_t BgL_formalsz00_3190;

											BgL_formalsz00_3190 = BgL_arg1767z00_2347;
											BgL_localsz00_3191 = BgL_arg1770z00_2348;
											BgL_localsz00_2346 = BgL_localsz00_3191;
											BgL_formalsz00_2345 = BgL_formalsz00_3190;
											goto BgL_loopz00_2344;
										}
									}
							}
					}
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_applyz00(void)
	{
		{	/* Ast/apply.scm 14 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1927z00zzast_applyz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1927z00zzast_applyz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1927z00zzast_applyz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1927z00zzast_applyz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1927z00zzast_applyz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1927z00zzast_applyz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1927z00zzast_applyz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1927z00zzast_applyz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1927z00zzast_applyz00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string1927z00zzast_applyz00));
			BGl_modulezd2initializa7ationz75zzast_appz00(449859288L,
				BSTRING_TO_STRING(BGl_string1927z00zzast_applyz00));
			return
				BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1927z00zzast_applyz00));
		}

	}

#ifdef __cplusplus
}
#endif
