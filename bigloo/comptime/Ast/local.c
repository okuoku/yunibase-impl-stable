/*===========================================================================*/
/*   (Ast/local.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/local.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_AST_LOCAL_TYPE_DEFINITIONS
#define BGL_AST_LOCAL_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_sexitz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_handlerz00;
		bool_t BgL_detachedzf3zf3;
	}               *BgL_sexitz00_bglt;


#endif													// BGL_AST_LOCAL_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzast_localz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zzast_localz00(void);
	static obj_t BGl_genericzd2initzd2zzast_localz00(void);
	static obj_t BGl_objectzd2initzd2zzast_localz00(void);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_localz00_bglt
		BGl_clonezd2localzd2zzast_localz00(BgL_localz00_bglt, BgL_valuez00_bglt);
	static BgL_localz00_bglt
		BGl_z62makezd2userzd2localzd2sfunzb0zzast_localz00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_localz00_bglt
		BGl_makezd2localzd2sfunz00zzast_localz00(obj_t, BgL_typez00_bglt,
		BgL_sfunz00_bglt);
	static BgL_localz00_bglt BGl_z62makezd2localzd2sfunz62zzast_localz00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_localz00_bglt
		BGl_makezd2userzd2localzd2sfunzd2zzast_localz00(obj_t, BgL_typez00_bglt,
		BgL_sfunz00_bglt);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzast_localz00(void);
	extern obj_t BGl_localzd2idzd2ze3nameze3zzast_identz00(obj_t);
	static BgL_localz00_bglt BGl_makezd2newzd2localz00zzast_localz00(obj_t,
		BgL_typez00_bglt, BgL_valuez00_bglt, bool_t);
	BGL_EXPORTED_DECL BgL_localz00_bglt
		BGl_makezd2localzd2sexitz00zzast_localz00(obj_t, BgL_typez00_bglt,
		BgL_sexitz00_bglt);
	extern obj_t BGl_idzd2ze3namez31zzast_identz00(obj_t);
	extern obj_t BGl_svarz00zzast_varz00;
	static BgL_localz00_bglt BGl_z62clonezd2localzb0zzast_localz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		long);
	static BgL_localz00_bglt
		BGl_z62makezd2userzd2localzd2svarzb0zzast_localz00(obj_t, obj_t, obj_t);
	static long BGl_za2localzd2keyza2zd2zzast_localz00 = 0L;
	extern obj_t BGl_localz00zzast_varz00;
	static BgL_localz00_bglt BGl_z62makezd2localzd2sexitz62zzast_localz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzast_localz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_localz00(void);
	BGL_EXPORTED_DECL BgL_localz00_bglt
		BGl_makezd2localzd2svarz00zzast_localz00(obj_t, BgL_typez00_bglt);
	static BgL_localz00_bglt BGl_z62makezd2localzd2svarz62zzast_localz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_localz00_bglt
		BGl_makezd2userzd2localzd2svarzd2zzast_localz00(obj_t, BgL_typez00_bglt);
	static obj_t BGl_importedzd2moduleszd2initz00zzast_localz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzast_localz00(void);
	static long BGl_getzd2newzd2keyz00zzast_localz00(void);
	static obj_t __cnst[2];


	   
		 
		DEFINE_STRING(BGl_string1158z00zzast_localz00,
		BgL_bgl_string1158za700za7za7a1163za7, "_", 1);
	      DEFINE_STRING(BGl_string1159z00zzast_localz00,
		BgL_bgl_string1159za700za7za7a1164za7, "ast_local", 9);
	      DEFINE_STRING(BGl_string1160z00zzast_localz00,
		BgL_bgl_string1160za700za7za7a1165za7, "now read ", 9);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2userzd2localzd2svarzd2envz00zzast_localz00,
		BgL_bgl_za762makeza7d2userza7d1166za7,
		BGl_z62makezd2userzd2localzd2svarzb0zzast_localz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2localzd2sfunzd2envzd2zzast_localz00,
		BgL_bgl_za762makeza7d2localza71167za7,
		BGl_z62makezd2localzd2sfunz62zzast_localz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_clonezd2localzd2envz00zzast_localz00,
		BgL_bgl_za762cloneza7d2local1168z00, BGl_z62clonezd2localzb0zzast_localz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2localzd2sexitzd2envzd2zzast_localz00,
		BgL_bgl_za762makeza7d2localza71169za7,
		BGl_z62makezd2localzd2sexitz62zzast_localz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2localzd2svarzd2envzd2zzast_localz00,
		BgL_bgl_za762makeza7d2localza71170za7,
		BGl_z62makezd2localzd2svarz62zzast_localz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2userzd2localzd2sfunzd2envz00zzast_localz00,
		BgL_bgl_za762makeza7d2userza7d1171za7,
		BGl_z62makezd2userzd2localzd2sfunzb0zzast_localz00, 0L, BUNSPEC, 3);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzast_localz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long
		BgL_checksumz00_578, char *BgL_fromz00_579)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_localz00))
				{
					BGl_requirezd2initializa7ationz75zzast_localz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_localz00();
					BGl_libraryzd2moduleszd2initz00zzast_localz00();
					BGl_cnstzd2initzd2zzast_localz00();
					BGl_importedzd2moduleszd2initz00zzast_localz00();
					return BGl_toplevelzd2initzd2zzast_localz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_localz00(void)
	{
		{	/* Ast/local.scm 14 */
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_local");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "ast_local");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"ast_local");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "ast_local");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "ast_local");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_localz00(void)
	{
		{	/* Ast/local.scm 14 */
			{	/* Ast/local.scm 14 */
				obj_t BgL_cportz00_567;

				{	/* Ast/local.scm 14 */
					obj_t BgL_stringz00_574;

					BgL_stringz00_574 = BGl_string1160z00zzast_localz00;
					{	/* Ast/local.scm 14 */
						obj_t BgL_startz00_575;

						BgL_startz00_575 = BINT(0L);
						{	/* Ast/local.scm 14 */
							obj_t BgL_endz00_576;

							BgL_endz00_576 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_574)));
							{	/* Ast/local.scm 14 */

								BgL_cportz00_567 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_574, BgL_startz00_575, BgL_endz00_576);
				}}}}
				{
					long BgL_iz00_568;

					BgL_iz00_568 = 1L;
				BgL_loopz00_569:
					if ((BgL_iz00_568 == -1L))
						{	/* Ast/local.scm 14 */
							return BUNSPEC;
						}
					else
						{	/* Ast/local.scm 14 */
							{	/* Ast/local.scm 14 */
								obj_t BgL_arg1162z00_570;

								{	/* Ast/local.scm 14 */

									{	/* Ast/local.scm 14 */
										obj_t BgL_locationz00_572;

										BgL_locationz00_572 = BBOOL(((bool_t) 0));
										{	/* Ast/local.scm 14 */

											BgL_arg1162z00_570 =
												BGl_readz00zz__readerz00(BgL_cportz00_567,
												BgL_locationz00_572);
										}
									}
								}
								{	/* Ast/local.scm 14 */
									int BgL_tmpz00_602;

									BgL_tmpz00_602 = (int) (BgL_iz00_568);
									CNST_TABLE_SET(BgL_tmpz00_602, BgL_arg1162z00_570);
							}}
							{	/* Ast/local.scm 14 */
								int BgL_auxz00_573;

								BgL_auxz00_573 = (int) ((BgL_iz00_568 - 1L));
								{
									long BgL_iz00_607;

									BgL_iz00_607 = (long) (BgL_auxz00_573);
									BgL_iz00_568 = BgL_iz00_607;
									goto BgL_loopz00_569;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_localz00(void)
	{
		{	/* Ast/local.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzast_localz00(void)
	{
		{	/* Ast/local.scm 14 */
			return (BGl_za2localzd2keyza2zd2zzast_localz00 = 0L, BUNSPEC);
		}

	}



/* get-new-key */
	long BGl_getzd2newzd2keyz00zzast_localz00(void)
	{
		{	/* Ast/local.scm 35 */
			BGl_za2localzd2keyza2zd2zzast_localz00 =
				(BGl_za2localzd2keyza2zd2zzast_localz00 + 1L);
			return BGl_za2localzd2keyza2zd2zzast_localz00;
		}

	}



/* make-new-local */
	BgL_localz00_bglt BGl_makezd2newzd2localz00zzast_localz00(obj_t BgL_idz00_3,
		BgL_typez00_bglt BgL_typez00_4, BgL_valuez00_bglt BgL_valuez00_5,
		bool_t BgL_userzf3zf3_6)
	{
		{	/* Ast/local.scm 44 */
			{	/* Ast/local.scm 45 */
				long BgL_keyz00_461;

				BgL_keyz00_461 = BGl_getzd2newzd2keyz00zzast_localz00();
				{	/* Ast/local.scm 46 */
					BgL_localz00_bglt BgL_new1062z00_462;

					{	/* Ast/local.scm 48 */
						BgL_localz00_bglt BgL_new1061z00_467;

						BgL_new1061z00_467 =
							((BgL_localz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_localz00_bgl))));
						{	/* Ast/local.scm 48 */
							long BgL_arg1115z00_468;

							BgL_arg1115z00_468 = BGL_CLASS_NUM(BGl_localz00zzast_varz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1061z00_467), BgL_arg1115z00_468);
						}
						{	/* Ast/local.scm 48 */
							BgL_objectz00_bglt BgL_tmpz00_616;

							BgL_tmpz00_616 = ((BgL_objectz00_bglt) BgL_new1061z00_467);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_616, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_new1061z00_467);
						BgL_new1062z00_462 = BgL_new1061z00_467;
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_new1062z00_462)))->BgL_idz00) =
						((obj_t) BgL_idz00_3), BUNSPEC);
					{
						obj_t BgL_auxz00_622;

						{	/* Ast/local.scm 49 */
							obj_t BgL_arg1104z00_463;
							obj_t BgL_arg1114z00_464;

							BgL_arg1104z00_463 =
								BGl_localzd2idzd2ze3nameze3zzast_identz00(BgL_idz00_3);
							{	/* Ast/local.scm 49 */

								BgL_arg1114z00_464 =
									BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
									(BgL_keyz00_461, 10L);
							}
							BgL_auxz00_622 =
								string_append_3(BgL_arg1104z00_463,
								BGl_string1158z00zzast_localz00, BgL_arg1114z00_464);
						}
						((((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_new1062z00_462)))->
								BgL_namez00) = ((obj_t) BgL_auxz00_622), BUNSPEC);
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_new1062z00_462)))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_typez00_4), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1062z00_462)))->BgL_valuez00) =
						((BgL_valuez00_bglt) BgL_valuez00_5), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1062z00_462)))->BgL_accessz00) =
						((obj_t) CNST_TABLE_REF(0)), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1062z00_462)))->BgL_fastzd2alphazd2) =
						((obj_t) BUNSPEC), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1062z00_462)))->BgL_removablez00) =
						((obj_t) CNST_TABLE_REF(1)), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1062z00_462)))->BgL_occurrencez00) =
						((long) 0L), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1062z00_462)))->BgL_occurrencewz00) =
						((long) 0L), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1062z00_462)))->BgL_userzf3zf3) =
						((bool_t) BgL_userzf3zf3_6), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(BgL_new1062z00_462))->BgL_keyz00) =
						((long) BgL_keyz00_461), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(BgL_new1062z00_462))->
							BgL_valzd2noescapezd2) = ((obj_t) BTRUE), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(BgL_new1062z00_462))->
							BgL_volatilez00) = ((bool_t) ((bool_t) 0)), BUNSPEC);
					return BgL_new1062z00_462;
				}
			}
		}

	}



/* clone-local */
	BGL_EXPORTED_DEF BgL_localz00_bglt
		BGl_clonezd2localzd2zzast_localz00(BgL_localz00_bglt BgL_localz00_7,
		BgL_valuez00_bglt BgL_valuez00_8)
	{
		{	/* Ast/local.scm 57 */
			{	/* Ast/local.scm 58 */
				long BgL_keyz00_469;

				BgL_keyz00_469 = BGl_getzd2newzd2keyz00zzast_localz00();
				{	/* Ast/local.scm 60 */
					BgL_localz00_bglt BgL_new1064z00_472;

					{	/* Ast/local.scm 60 */
						BgL_localz00_bglt BgL_new1069z00_480;

						BgL_new1069z00_480 =
							((BgL_localz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_localz00_bgl))));
						{	/* Ast/local.scm 60 */
							long BgL_arg1126z00_481;

							BgL_arg1126z00_481 = BGL_CLASS_NUM(BGl_localz00zzast_varz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1069z00_480), BgL_arg1126z00_481);
						}
						{	/* Ast/local.scm 60 */
							BgL_objectz00_bglt BgL_tmpz00_654;

							BgL_tmpz00_654 = ((BgL_objectz00_bglt) BgL_new1069z00_480);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_654, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_new1069z00_480);
						BgL_new1064z00_472 = BgL_new1069z00_480;
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_new1064z00_472)))->BgL_idz00) =
						((obj_t) (((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
											BgL_localz00_7)))->BgL_idz00)), BUNSPEC);
					{
						obj_t BgL_auxz00_662;

						{	/* Ast/local.scm 62 */
							obj_t BgL_arg1122z00_473;
							obj_t BgL_arg1123z00_474;

							{	/* Ast/local.scm 62 */
								obj_t BgL_arg1125z00_475;

								BgL_arg1125z00_475 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_localz00_7)))->BgL_idz00);
								BgL_arg1122z00_473 =
									BGl_idzd2ze3namez31zzast_identz00(BgL_arg1125z00_475);
							}
							{	/* Ast/local.scm 62 */

								BgL_arg1123z00_474 =
									BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
									(BgL_keyz00_469, 10L);
							}
							BgL_auxz00_662 =
								string_append_3(BgL_arg1122z00_473,
								BGl_string1158z00zzast_localz00, BgL_arg1123z00_474);
						}
						((((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_new1064z00_472)))->
								BgL_namez00) = ((obj_t) BgL_auxz00_662), BUNSPEC);
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_new1064z00_472)))->BgL_typez00) =
						((BgL_typez00_bglt) (((BgL_variablez00_bglt)
									COBJECT(((BgL_variablez00_bglt) BgL_localz00_7)))->
								BgL_typez00)), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1064z00_472)))->BgL_valuez00) =
						((BgL_valuez00_bglt) BgL_valuez00_8), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1064z00_472)))->BgL_accessz00) =
						((obj_t) (((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
											BgL_localz00_7)))->BgL_accessz00)), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1064z00_472)))->BgL_fastzd2alphazd2) =
						((obj_t) (((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
											BgL_localz00_7)))->BgL_fastzd2alphazd2)), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1064z00_472)))->BgL_removablez00) =
						((obj_t) (((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
											BgL_localz00_7)))->BgL_removablez00)), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1064z00_472)))->BgL_occurrencez00) =
						((long) (((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
											BgL_localz00_7)))->BgL_occurrencez00)), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1064z00_472)))->BgL_occurrencewz00) =
						((long) (((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
											BgL_localz00_7)))->BgL_occurrencewz00)), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1064z00_472)))->BgL_userzf3zf3) =
						((bool_t) (((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
											BgL_localz00_7)))->BgL_userzf3zf3)), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(BgL_new1064z00_472))->BgL_keyz00) =
						((long) BgL_keyz00_469), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(BgL_new1064z00_472))->
							BgL_valzd2noescapezd2) =
						((obj_t) (((BgL_localz00_bglt)
									COBJECT(((BgL_localz00_bglt) ((BgL_variablez00_bglt)
												BgL_localz00_7))))->BgL_valzd2noescapezd2)), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(BgL_new1064z00_472))->
							BgL_volatilez00) =
						((bool_t) (((BgL_localz00_bglt)
									COBJECT(((BgL_localz00_bglt) ((BgL_variablez00_bglt)
												BgL_localz00_7))))->BgL_volatilez00)), BUNSPEC);
					return BgL_new1064z00_472;
				}
			}
		}

	}



/* &clone-local */
	BgL_localz00_bglt BGl_z62clonezd2localzb0zzast_localz00(obj_t BgL_envz00_546,
		obj_t BgL_localz00_547, obj_t BgL_valuez00_548)
	{
		{	/* Ast/local.scm 57 */
			return
				BGl_clonezd2localzd2zzast_localz00(
				((BgL_localz00_bglt) BgL_localz00_547),
				((BgL_valuez00_bglt) BgL_valuez00_548));
		}

	}



/* make-local-svar */
	BGL_EXPORTED_DEF BgL_localz00_bglt
		BGl_makezd2localzd2svarz00zzast_localz00(obj_t BgL_idz00_9,
		BgL_typez00_bglt BgL_typez00_10)
	{
		{	/* Ast/local.scm 68 */
			{	/* Ast/local.scm 69 */
				BgL_svarz00_bglt BgL_arg1127z00_482;

				{	/* Ast/local.scm 69 */
					BgL_svarz00_bglt BgL_new1071z00_483;

					{	/* Ast/local.scm 69 */
						BgL_svarz00_bglt BgL_new1070z00_484;

						BgL_new1070z00_484 =
							((BgL_svarz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_svarz00_bgl))));
						{	/* Ast/local.scm 69 */
							long BgL_arg1129z00_485;

							BgL_arg1129z00_485 = BGL_CLASS_NUM(BGl_svarz00zzast_varz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1070z00_484), BgL_arg1129z00_485);
						}
						{	/* Ast/local.scm 69 */
							BgL_objectz00_bglt BgL_tmpz00_716;

							BgL_tmpz00_716 = ((BgL_objectz00_bglt) BgL_new1070z00_484);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_716, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_new1070z00_484);
						BgL_new1071z00_483 = BgL_new1070z00_484;
					}
					((((BgL_svarz00_bglt) COBJECT(BgL_new1071z00_483))->BgL_locz00) =
						((obj_t) BUNSPEC), BUNSPEC);
					BgL_arg1127z00_482 = BgL_new1071z00_483;
				}
				return
					BGl_makezd2newzd2localz00zzast_localz00(BgL_idz00_9, BgL_typez00_10,
					((BgL_valuez00_bglt) BgL_arg1127z00_482), ((bool_t) 0));
			}
		}

	}



/* &make-local-svar */
	BgL_localz00_bglt BGl_z62makezd2localzd2svarz62zzast_localz00(obj_t
		BgL_envz00_549, obj_t BgL_idz00_550, obj_t BgL_typez00_551)
	{
		{	/* Ast/local.scm 68 */
			return
				BGl_makezd2localzd2svarz00zzast_localz00(BgL_idz00_550,
				((BgL_typez00_bglt) BgL_typez00_551));
		}

	}



/* make-user-local-svar */
	BGL_EXPORTED_DEF BgL_localz00_bglt
		BGl_makezd2userzd2localzd2svarzd2zzast_localz00(obj_t BgL_idz00_11,
		BgL_typez00_bglt BgL_typez00_12)
	{
		{	/* Ast/local.scm 74 */
			{	/* Ast/local.scm 75 */
				BgL_svarz00_bglt BgL_arg1131z00_486;

				{	/* Ast/local.scm 75 */
					BgL_svarz00_bglt BgL_new1073z00_487;

					{	/* Ast/local.scm 75 */
						BgL_svarz00_bglt BgL_new1072z00_488;

						BgL_new1072z00_488 =
							((BgL_svarz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_svarz00_bgl))));
						{	/* Ast/local.scm 75 */
							long BgL_arg1132z00_489;

							BgL_arg1132z00_489 = BGL_CLASS_NUM(BGl_svarz00zzast_varz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1072z00_488), BgL_arg1132z00_489);
						}
						{	/* Ast/local.scm 75 */
							BgL_objectz00_bglt BgL_tmpz00_729;

							BgL_tmpz00_729 = ((BgL_objectz00_bglt) BgL_new1072z00_488);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_729, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_new1072z00_488);
						BgL_new1073z00_487 = BgL_new1072z00_488;
					}
					((((BgL_svarz00_bglt) COBJECT(BgL_new1073z00_487))->BgL_locz00) =
						((obj_t) BUNSPEC), BUNSPEC);
					BgL_arg1131z00_486 = BgL_new1073z00_487;
				}
				return
					BGl_makezd2newzd2localz00zzast_localz00(BgL_idz00_11, BgL_typez00_12,
					((BgL_valuez00_bglt) BgL_arg1131z00_486), ((bool_t) 1));
			}
		}

	}



/* &make-user-local-svar */
	BgL_localz00_bglt BGl_z62makezd2userzd2localzd2svarzb0zzast_localz00(obj_t
		BgL_envz00_552, obj_t BgL_idz00_553, obj_t BgL_typez00_554)
	{
		{	/* Ast/local.scm 74 */
			return
				BGl_makezd2userzd2localzd2svarzd2zzast_localz00(BgL_idz00_553,
				((BgL_typez00_bglt) BgL_typez00_554));
		}

	}



/* make-local-sexit */
	BGL_EXPORTED_DEF BgL_localz00_bglt
		BGl_makezd2localzd2sexitz00zzast_localz00(obj_t BgL_idz00_13,
		BgL_typez00_bglt BgL_typez00_14, BgL_sexitz00_bglt BgL_sexitz00_15)
	{
		{	/* Ast/local.scm 80 */
			return
				BGl_makezd2newzd2localz00zzast_localz00(BgL_idz00_13, BgL_typez00_14,
				((BgL_valuez00_bglt) BgL_sexitz00_15), ((bool_t) 0));
		}

	}



/* &make-local-sexit */
	BgL_localz00_bglt BGl_z62makezd2localzd2sexitz62zzast_localz00(obj_t
		BgL_envz00_555, obj_t BgL_idz00_556, obj_t BgL_typez00_557,
		obj_t BgL_sexitz00_558)
	{
		{	/* Ast/local.scm 80 */
			return
				BGl_makezd2localzd2sexitz00zzast_localz00(BgL_idz00_556,
				((BgL_typez00_bglt) BgL_typez00_557),
				((BgL_sexitz00_bglt) BgL_sexitz00_558));
		}

	}



/* make-local-sfun */
	BGL_EXPORTED_DEF BgL_localz00_bglt
		BGl_makezd2localzd2sfunz00zzast_localz00(obj_t BgL_idz00_16,
		BgL_typez00_bglt BgL_typez00_17, BgL_sfunz00_bglt BgL_sfunz00_18)
	{
		{	/* Ast/local.scm 88 */
			return
				BGl_makezd2newzd2localz00zzast_localz00(BgL_idz00_16, BgL_typez00_17,
				((BgL_valuez00_bglt) BgL_sfunz00_18), ((bool_t) 0));
		}

	}



/* &make-local-sfun */
	BgL_localz00_bglt BGl_z62makezd2localzd2sfunz62zzast_localz00(obj_t
		BgL_envz00_559, obj_t BgL_idz00_560, obj_t BgL_typez00_561,
		obj_t BgL_sfunz00_562)
	{
		{	/* Ast/local.scm 88 */
			return
				BGl_makezd2localzd2sfunz00zzast_localz00(BgL_idz00_560,
				((BgL_typez00_bglt) BgL_typez00_561),
				((BgL_sfunz00_bglt) BgL_sfunz00_562));
		}

	}



/* make-user-local-sfun */
	BGL_EXPORTED_DEF BgL_localz00_bglt
		BGl_makezd2userzd2localzd2sfunzd2zzast_localz00(obj_t BgL_idz00_19,
		BgL_typez00_bglt BgL_typez00_20, BgL_sfunz00_bglt BgL_sfunz00_21)
	{
		{	/* Ast/local.scm 96 */
			return
				BGl_makezd2newzd2localz00zzast_localz00(BgL_idz00_19, BgL_typez00_20,
				((BgL_valuez00_bglt) BgL_sfunz00_21), ((bool_t) 1));
		}

	}



/* &make-user-local-sfun */
	BgL_localz00_bglt BGl_z62makezd2userzd2localzd2sfunzb0zzast_localz00(obj_t
		BgL_envz00_563, obj_t BgL_idz00_564, obj_t BgL_typez00_565,
		obj_t BgL_sfunz00_566)
	{
		{	/* Ast/local.scm 96 */
			return
				BGl_makezd2userzd2localzd2sfunzd2zzast_localz00(BgL_idz00_564,
				((BgL_typez00_bglt) BgL_typez00_565),
				((BgL_sfunz00_bglt) BgL_sfunz00_566));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_localz00(void)
	{
		{	/* Ast/local.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_localz00(void)
	{
		{	/* Ast/local.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_localz00(void)
	{
		{	/* Ast/local.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_localz00(void)
	{
		{	/* Ast/local.scm 14 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1159z00zzast_localz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1159z00zzast_localz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1159z00zzast_localz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1159z00zzast_localz00));
			return
				BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1159z00zzast_localz00));
		}

	}

#ifdef __cplusplus
}
#endif
