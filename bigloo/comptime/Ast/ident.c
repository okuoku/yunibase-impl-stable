/*===========================================================================*/
/*   (Ast/ident.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/ident.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_AST_IDENT_TYPE_DEFINITIONS
#define BGL_AST_IDENT_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;


#endif													// BGL_AST_IDENT_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t BGl_putpropz12z12zz__r4_symbols_6_4z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_parsezd2dssslzd2zzast_identz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzast_identz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_makezd2typedzd2identz00zzast_identz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_fastzd2idzd2ofzd2idzd2zzast_identz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_parsezd2idzf2importzd2locationzf2zzast_identz00(obj_t, obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzast_identz00(void);
	BGL_IMPORT obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_z62idzd2ofzd2idz62zzast_identz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzast_identz00(void);
	static obj_t BGl_z62idzd2ze3namez53zzast_identz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	extern BgL_typez00_bglt BGl_getzd2defaultzd2typez00zztype_cachez00(void);
	static obj_t BGl_objectzd2initzd2zzast_identz00(void);
	static obj_t BGl_z62makezd2typedzd2identz62zzast_identz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62userzd2symbolzf3z43zzast_identz00(obj_t, obj_t);
	static obj_t BGl_z62localzd2idzd2ze3namez81zzast_identz00(obj_t, obj_t);
	static obj_t BGl_z62parsezd2dssslzb0zzast_identz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2typedzd2formalz00zzast_identz00(obj_t);
	static obj_t BGl_methodzd2initzd2zzast_identz00(void);
	extern obj_t BGl_dssslzd2defaultedzd2formalzf3zf3zztools_dssslz00(obj_t);
	static obj_t BGl_z62checkzd2idzb0zzast_identz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62parsezd2idzb0zzast_identz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_localzd2idzd2ze3nameze3zzast_identz00(obj_t);
	extern BgL_typez00_bglt
		BGl_usezd2typezf2importzd2locz12ze0zztype_envz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_idzd2ofzd2idz00zzast_identz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_idzd2ze3namez31zzast_identz00(obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_typezd2ofzd2idzf2importzd2locationz20zzast_identz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_dssslz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__dssslz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static BgL_typez00_bglt
		BGl_z62typezd2ofzd2idzf2importzd2locationz42zzast_identz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt BGl_typezd2ofzd2idz00zzast_identz00(obj_t,
		obj_t);
	static obj_t BGl_parsezd2idzf2usez20zzast_identz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_checkzd2idzd2zzast_identz00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzast_identz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_identz00(void);
	static BgL_typez00_bglt BGl_z62typezd2ofzd2idz62zzast_identz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzast_identz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzast_identz00(void);
	BGL_EXPORTED_DECL obj_t BGl_parsezd2idzd2zzast_identz00(obj_t, obj_t);
	BGL_IMPORT obj_t bigloo_mangle(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_typezd2identzf3z21zzast_identz00(obj_t);
	static obj_t BGl_z62makezd2typedzd2formalz62zzast_identz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31199ze31223ze5zzast_identz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62fastzd2idzd2ofzd2idzb0zzast_identz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31199ze3ze5zzast_identz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_getpropz00zz__r4_symbols_6_4z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_userzd2symbolzf3z21zzast_identz00(obj_t);
	BGL_IMPORT obj_t c_substring(obj_t, long, long);
	static obj_t BGl_z62parsezd2idzf2importzd2locationz90zzast_identz00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00(obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_4dotsz00zzast_identz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_stringzd2downcasezd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_z62typezd2identzf3z43zzast_identz00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(obj_t);
	static obj_t BGl_z62markzd2symbolzd2nonzd2userz12za2zzast_identz00(obj_t,
		obj_t);
	static obj_t __cnst[3];


	   
		 
		DEFINE_STRING(BGl_string1230z00zzast_identz00,
		BgL_bgl_string1230za700za7za7a1237za7, "Illegal formal identifier", 25);
	      DEFINE_STRING(BGl_string1231z00zzast_identz00,
		BgL_bgl_string1231za700za7za7a1238za7, "`' (~a)", 7);
	      DEFINE_STRING(BGl_string1232z00zzast_identz00,
		BgL_bgl_string1232za700za7za7a1239za7, "Illegal empty identifier", 24);
	      DEFINE_STRING(BGl_string1233z00zzast_identz00,
		BgL_bgl_string1233za700za7za7a1240za7, "Illegal formal parameter", 24);
	      DEFINE_STRING(BGl_string1234z00zzast_identz00,
		BgL_bgl_string1234za700za7za7a1241za7, "ast_ident", 9);
	      DEFINE_STRING(BGl_string1235z00zzast_identz00,
		BgL_bgl_string1235za700za7za7a1242za7, "non-user error-ident dsssl ", 27);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_parsezd2dssslzd2envz00zzast_identz00,
		BgL_bgl_za762parseza7d2dsssl1243z00, BGl_z62parsezd2dssslzb0zzast_identz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2typedzd2formalzd2envzd2zzast_identz00,
		BgL_bgl_za762makeza7d2typedza71244za7,
		BGl_z62makezd2typedzd2formalz62zzast_identz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_markzd2symbolzd2nonzd2userz12zd2envz12zzast_identz00,
		BgL_bgl_za762markza7d2symbol1245z00,
		BGl_z62markzd2symbolzd2nonzd2userz12za2zzast_identz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_typezd2ofzd2idzf2importzd2locationzd2envzf2zzast_identz00,
		BgL_bgl_za762typeza7d2ofza7d2i1246za7,
		BGl_z62typezd2ofzd2idzf2importzd2locationz42zzast_identz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2typedzd2identzd2envzd2zzast_identz00,
		BgL_bgl_za762makeza7d2typedza71247za7,
		BGl_z62makezd2typedzd2identz62zzast_identz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_parsezd2idzd2envz00zzast_identz00,
		BgL_bgl_za762parseza7d2idza7b01248za7, BGl_z62parsezd2idzb0zzast_identz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_typezd2ofzd2idzd2envzd2zzast_identz00,
		BgL_bgl_za762typeza7d2ofza7d2i1249za7,
		BGl_z62typezd2ofzd2idz62zzast_identz00, 0L, BUNSPEC, 2);
	extern obj_t BGl_usezd2typez12zd2envz12zztype_envz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_localzd2idzd2ze3namezd2envz31zzast_identz00,
		BgL_bgl_za762localza7d2idza7d21250za7,
		BGl_z62localzd2idzd2ze3namez81zzast_identz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_idzd2ofzd2idzd2envzd2zzast_identz00,
		BgL_bgl_za762idza7d2ofza7d2idza71251z00,
		BGl_z62idzd2ofzd2idz62zzast_identz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_idzd2ze3namezd2envze3zzast_identz00,
		BgL_bgl_za762idza7d2za7e3nameza71252z00,
		BGl_z62idzd2ze3namez53zzast_identz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_fastzd2idzd2ofzd2idzd2envz00zzast_identz00,
		BgL_bgl_za762fastza7d2idza7d2o1253za7,
		BGl_z62fastzd2idzd2ofzd2idzb0zzast_identz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_checkzd2idzd2envz00zzast_identz00,
		BgL_bgl_za762checkza7d2idza7b01254za7, BGl_z62checkzd2idzb0zzast_identz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_parsezd2idzf2importzd2locationzd2envz20zzast_identz00,
		BgL_bgl_za762parseza7d2idza7f21255za7,
		BGl_z62parsezd2idzf2importzd2locationz90zzast_identz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_userzd2symbolzf3zd2envzf3zzast_identz00,
		BgL_bgl_za762userza7d2symbol1256z00,
		BGl_z62userzd2symbolzf3z43zzast_identz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_typezd2identzf3zd2envzf3zzast_identz00,
		BgL_bgl_za762typeza7d2identza71257za7,
		BGl_z62typezd2identzf3z43zzast_identz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1224z00zzast_identz00,
		BgL_bgl_string1224za700za7za7a1258za7, "::", 2);
	      DEFINE_STRING(BGl_string1225z00zzast_identz00,
		BgL_bgl_string1225za700za7za7a1259za7, "Illegal identifier", 18);
	      DEFINE_STRING(BGl_string1226z00zzast_identz00,
		BgL_bgl_string1226za700za7za7a1260za7, "`'", 2);
	      DEFINE_STRING(BGl_string1227z00zzast_identz00,
		BgL_bgl_string1227za700za7za7a1261za7, "parse", 5);
	      DEFINE_STRING(BGl_string1228z00zzast_identz00,
		BgL_bgl_string1228za700za7za7a1262za7, "", 0);
	      DEFINE_STRING(BGl_string1229z00zzast_identz00,
		BgL_bgl_string1229za700za7za7a1263za7, "type-of-id", 10);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzast_identz00));
		     ADD_ROOT((void *) (&BGl_4dotsz00zzast_identz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long
		BgL_checksumz00_404, char *BgL_fromz00_405)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_identz00))
				{
					BGl_requirezd2initializa7ationz75zzast_identz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_identz00();
					BGl_libraryzd2moduleszd2initz00zzast_identz00();
					BGl_cnstzd2initzd2zzast_identz00();
					BGl_importedzd2moduleszd2initz00zzast_identz00();
					return BGl_toplevelzd2initzd2zzast_identz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_identz00(void)
	{
		{	/* Ast/ident.scm 14 */
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "ast_ident");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "ast_ident");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "ast_ident");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_ident");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "ast_ident");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "ast_ident");
			BGl_modulezd2initializa7ationz75zz__dssslz00(0L, "ast_ident");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_ident");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L, "ast_ident");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"ast_ident");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_identz00(void)
	{
		{	/* Ast/ident.scm 14 */
			{	/* Ast/ident.scm 14 */
				obj_t BgL_cportz00_391;

				{	/* Ast/ident.scm 14 */
					obj_t BgL_stringz00_398;

					BgL_stringz00_398 = BGl_string1235z00zzast_identz00;
					{	/* Ast/ident.scm 14 */
						obj_t BgL_startz00_399;

						BgL_startz00_399 = BINT(0L);
						{	/* Ast/ident.scm 14 */
							obj_t BgL_endz00_400;

							BgL_endz00_400 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_398)));
							{	/* Ast/ident.scm 14 */

								BgL_cportz00_391 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_398, BgL_startz00_399, BgL_endz00_400);
				}}}}
				{
					long BgL_iz00_392;

					BgL_iz00_392 = 2L;
				BgL_loopz00_393:
					if ((BgL_iz00_392 == -1L))
						{	/* Ast/ident.scm 14 */
							return BUNSPEC;
						}
					else
						{	/* Ast/ident.scm 14 */
							{	/* Ast/ident.scm 14 */
								obj_t BgL_arg1236z00_394;

								{	/* Ast/ident.scm 14 */

									{	/* Ast/ident.scm 14 */
										obj_t BgL_locationz00_396;

										BgL_locationz00_396 = BBOOL(((bool_t) 0));
										{	/* Ast/ident.scm 14 */

											BgL_arg1236z00_394 =
												BGl_readz00zz__readerz00(BgL_cportz00_391,
												BgL_locationz00_396);
										}
									}
								}
								{	/* Ast/ident.scm 14 */
									int BgL_tmpz00_433;

									BgL_tmpz00_433 = (int) (BgL_iz00_392);
									CNST_TABLE_SET(BgL_tmpz00_433, BgL_arg1236z00_394);
							}}
							{	/* Ast/ident.scm 14 */
								int BgL_auxz00_397;

								BgL_auxz00_397 = (int) ((BgL_iz00_392 - 1L));
								{
									long BgL_iz00_438;

									BgL_iz00_438 = (long) (BgL_auxz00_397);
									BgL_iz00_392 = BgL_iz00_438;
									goto BgL_loopz00_393;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_identz00(void)
	{
		{	/* Ast/ident.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzast_identz00(void)
	{
		{	/* Ast/ident.scm 14 */
			return (BGl_4dotsz00zzast_identz00 =
				bstring_to_symbol(BGl_string1224z00zzast_identz00), BUNSPEC);
		}

	}



/* type-ident? */
	BGL_EXPORTED_DEF bool_t BGl_typezd2identzf3z21zzast_identz00(obj_t
		BgL_symz00_3)
	{
		{	/* Ast/ident.scm 44 */
			{	/* Ast/ident.scm 45 */
				obj_t BgL_strz00_98;

				{	/* Ast/ident.scm 45 */
					obj_t BgL_arg1455z00_233;

					BgL_arg1455z00_233 = SYMBOL_TO_STRING(BgL_symz00_3);
					BgL_strz00_98 =
						BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_233);
				}
				if ((STRING_LENGTH(BgL_strz00_98) > 2L))
					{	/* Ast/ident.scm 46 */
						if ((STRING_REF(BgL_strz00_98, 0L) == ((unsigned char) ':')))
							{	/* Ast/ident.scm 47 */
								return (STRING_REF(BgL_strz00_98, 1L) == ((unsigned char) ':'));
							}
						else
							{	/* Ast/ident.scm 47 */
								return ((bool_t) 0);
							}
					}
				else
					{	/* Ast/ident.scm 46 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &type-ident? */
	obj_t BGl_z62typezd2identzf3z43zzast_identz00(obj_t BgL_envz00_338,
		obj_t BgL_symz00_339)
	{
		{	/* Ast/ident.scm 44 */
			return BBOOL(BGl_typezd2identzf3z21zzast_identz00(BgL_symz00_339));
		}

	}



/* make-typed-ident */
	BGL_EXPORTED_DEF obj_t BGl_makezd2typedzd2identz00zzast_identz00(obj_t
		BgL_sym1z00_4, obj_t BgL_sym2z00_5)
	{
		{	/* Ast/ident.scm 53 */
			{	/* Ast/ident.scm 54 */
				obj_t BgL_list1058z00_242;

				{	/* Ast/ident.scm 54 */
					obj_t BgL_arg1059z00_243;

					{	/* Ast/ident.scm 54 */
						obj_t BgL_arg1060z00_244;

						BgL_arg1060z00_244 = MAKE_YOUNG_PAIR(BgL_sym2z00_5, BNIL);
						BgL_arg1059z00_243 =
							MAKE_YOUNG_PAIR(BGl_4dotsz00zzast_identz00, BgL_arg1060z00_244);
					}
					BgL_list1058z00_242 =
						MAKE_YOUNG_PAIR(BgL_sym1z00_4, BgL_arg1059z00_243);
				}
				return BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(BgL_list1058z00_242);
			}
		}

	}



/* &make-typed-ident */
	obj_t BGl_z62makezd2typedzd2identz62zzast_identz00(obj_t BgL_envz00_340,
		obj_t BgL_sym1z00_341, obj_t BgL_sym2z00_342)
	{
		{	/* Ast/ident.scm 53 */
			return
				BGl_makezd2typedzd2identz00zzast_identz00(BgL_sym1z00_341,
				BgL_sym2z00_342);
		}

	}



/* make-typed-formal */
	BGL_EXPORTED_DEF obj_t BGl_makezd2typedzd2formalz00zzast_identz00(obj_t
		BgL_sym2z00_6)
	{
		{	/* Ast/ident.scm 59 */
			{	/* Ast/ident.scm 60 */
				obj_t BgL_arg1062z00_107;

				{	/* Ast/ident.scm 60 */
					obj_t BgL_arg1063z00_108;
					obj_t BgL_arg1065z00_109;

					{	/* Ast/ident.scm 60 */
						obj_t BgL_symbolz00_245;

						BgL_symbolz00_245 = BGl_4dotsz00zzast_identz00;
						{	/* Ast/ident.scm 60 */
							obj_t BgL_arg1455z00_246;

							BgL_arg1455z00_246 = SYMBOL_TO_STRING(BgL_symbolz00_245);
							BgL_arg1063z00_108 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_246);
						}
					}
					{	/* Ast/ident.scm 60 */
						obj_t BgL_arg1455z00_248;

						BgL_arg1455z00_248 = SYMBOL_TO_STRING(BgL_sym2z00_6);
						BgL_arg1065z00_109 =
							BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_248);
					}
					BgL_arg1062z00_107 =
						string_append(BgL_arg1063z00_108, BgL_arg1065z00_109);
				}
				return bstring_to_symbol(BgL_arg1062z00_107);
			}
		}

	}



/* &make-typed-formal */
	obj_t BGl_z62makezd2typedzd2formalz62zzast_identz00(obj_t BgL_envz00_343,
		obj_t BgL_sym2z00_344)
	{
		{	/* Ast/ident.scm 59 */
			return BGl_makezd2typedzd2formalz00zzast_identz00(BgL_sym2z00_344);
		}

	}



/* type-of-id */
	BGL_EXPORTED_DEF BgL_typez00_bglt BGl_typezd2ofzd2idz00zzast_identz00(obj_t
		BgL_idz00_7, obj_t BgL_locz00_8)
	{
		{	/* Ast/ident.scm 65 */
			if (SYMBOLP(BgL_idz00_7))
				{	/* Ast/ident.scm 68 */
					obj_t BgL_arg1068z00_111;

					BgL_arg1068z00_111 =
						BGl_parsezd2idzf2usez20zzast_identz00(BgL_idz00_7, BgL_locz00_8,
						BGl_usezd2typez12zd2envz12zztype_envz00);
					return ((BgL_typez00_bglt) CDR(BgL_arg1068z00_111));
				}
			else
				{	/* Ast/ident.scm 66 */
					return
						((BgL_typez00_bglt)
						BGl_userzd2errorzd2zztools_errorz00(BGl_string1225z00zzast_identz00,
							BGl_string1226z00zzast_identz00, BgL_idz00_7, BNIL));
				}
		}

	}



/* &type-of-id */
	BgL_typez00_bglt BGl_z62typezd2ofzd2idz62zzast_identz00(obj_t BgL_envz00_345,
		obj_t BgL_idz00_346, obj_t BgL_locz00_347)
	{
		{	/* Ast/ident.scm 65 */
			return BGl_typezd2ofzd2idz00zzast_identz00(BgL_idz00_346, BgL_locz00_347);
		}

	}



/* type-of-id/import-location */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_typezd2ofzd2idzf2importzd2locationz20zzast_identz00(obj_t BgL_idz00_9,
		obj_t BgL_locz00_10, obj_t BgL_lociz00_11)
	{
		{	/* Ast/ident.scm 73 */
			if (SYMBOLP(BgL_idz00_9))
				{	/* Ast/ident.scm 76 */
					obj_t BgL_arg1074z00_114;

					{	/* Ast/ident.scm 165 */
						obj_t BgL_zc3z04anonymousza31199ze3z87_351;

						BgL_zc3z04anonymousza31199ze3z87_351 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31199ze3ze5zzast_identz00, (int) (2L),
							(int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31199ze3z87_351, (int) (0L),
							BgL_lociz00_11);
						BgL_arg1074z00_114 =
							BGl_parsezd2idzf2usez20zzast_identz00(BgL_idz00_9, BgL_locz00_10,
							BgL_zc3z04anonymousza31199ze3z87_351);
					}
					return ((BgL_typez00_bglt) CDR(BgL_arg1074z00_114));
				}
			else
				{	/* Ast/ident.scm 74 */
					return
						((BgL_typez00_bglt)
						BGl_userzd2errorzd2zztools_errorz00(BGl_string1225z00zzast_identz00,
							BGl_string1226z00zzast_identz00, BgL_idz00_9, BNIL));
				}
		}

	}



/* &type-of-id/import-location */
	BgL_typez00_bglt
		BGl_z62typezd2ofzd2idzf2importzd2locationz42zzast_identz00(obj_t
		BgL_envz00_352, obj_t BgL_idz00_353, obj_t BgL_locz00_354,
		obj_t BgL_lociz00_355)
	{
		{	/* Ast/ident.scm 73 */
			return
				BGl_typezd2ofzd2idzf2importzd2locationz20zzast_identz00(BgL_idz00_353,
				BgL_locz00_354, BgL_lociz00_355);
		}

	}



/* &<@anonymous:1199> */
	obj_t BGl_z62zc3z04anonymousza31199ze3ze5zzast_identz00(obj_t BgL_envz00_356,
		obj_t BgL_tidz00_358, obj_t BgL_locz00_359)
	{
		{	/* Ast/ident.scm 165 */
			{	/* Ast/ident.scm 165 */
				obj_t BgL_lociz00_357;

				BgL_lociz00_357 = PROCEDURE_REF(BgL_envz00_356, (int) (0L));
				return
					((obj_t)
					BGl_usezd2typezf2importzd2locz12ze0zztype_envz00(BgL_tidz00_358,
						BgL_locz00_359, BgL_lociz00_357));
			}
		}

	}



/* id-of-id */
	BGL_EXPORTED_DEF obj_t BGl_idzd2ofzd2idz00zzast_identz00(obj_t BgL_idz00_12,
		obj_t BgL_locz00_13)
	{
		{	/* Ast/ident.scm 81 */
			if (SYMBOLP(BgL_idz00_12))
				{	/* Ast/ident.scm 82 */
					return
						CAR(BGl_parsezd2idzf2usez20zzast_identz00(BgL_idz00_12,
							BgL_locz00_13, BGl_usezd2typez12zd2envz12zztype_envz00));
				}
			else
				{	/* Ast/ident.scm 82 */
					return
						BGl_userzd2errorzd2zztools_errorz00(BGl_string1227z00zzast_identz00,
						BGl_string1225z00zzast_identz00, BgL_idz00_12, BNIL);
				}
		}

	}



/* &id-of-id */
	obj_t BGl_z62idzd2ofzd2idz62zzast_identz00(obj_t BgL_envz00_360,
		obj_t BgL_idz00_361, obj_t BgL_locz00_362)
	{
		{	/* Ast/ident.scm 81 */
			return BGl_idzd2ofzd2idz00zzast_identz00(BgL_idz00_361, BgL_locz00_362);
		}

	}



/* fast-id-of-id */
	BGL_EXPORTED_DEF obj_t BGl_fastzd2idzd2ofzd2idzd2zzast_identz00(obj_t
		BgL_idz00_14, obj_t BgL_locz00_15)
	{
		{	/* Ast/ident.scm 89 */
			{
				obj_t BgL_idz00_129;

				if (BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(BgL_idz00_14))
					{	/* Ast/ident.scm 104 */
						return BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(0));
					}
				else
					{	/* Ast/ident.scm 106 */
						bool_t BgL_test1277z00_501;

						if (PAIRP(BgL_idz00_14))
							{	/* Ast/ident.scm 106 */
								obj_t BgL_tmpz00_504;

								BgL_tmpz00_504 = CAR(BgL_idz00_14);
								BgL_test1277z00_501 = SYMBOLP(BgL_tmpz00_504);
							}
						else
							{	/* Ast/ident.scm 106 */
								BgL_test1277z00_501 = ((bool_t) 0);
							}
						if (BgL_test1277z00_501)
							{	/* Ast/ident.scm 106 */
								BgL_idz00_129 = CAR(BgL_idz00_14);
							BgL_zc3z04anonymousza31088ze3z87_130:
								{	/* Ast/ident.scm 91 */
									obj_t BgL_stringz00_131;

									{	/* Ast/ident.scm 91 */
										obj_t BgL_arg1455z00_257;

										BgL_arg1455z00_257 =
											SYMBOL_TO_STRING(((obj_t) BgL_idz00_129));
										BgL_stringz00_131 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_257);
									}
									{	/* Ast/ident.scm 91 */
										long BgL_lenz00_132;

										BgL_lenz00_132 = STRING_LENGTH(BgL_stringz00_131);
										{	/* Ast/ident.scm 92 */

											{
												long BgL_walkerz00_134;

												BgL_walkerz00_134 = 0L;
											BgL_zc3z04anonymousza31089ze3z87_135:
												if ((BgL_walkerz00_134 == BgL_lenz00_132))
													{	/* Ast/ident.scm 95 */
														return BgL_idz00_129;
													}
												else
													{	/* Ast/ident.scm 97 */
														bool_t BgL_test1283z00_513;

														if (
															(STRING_REF(BgL_stringz00_131,
																	BgL_walkerz00_134) == ((unsigned char) ':')))
															{	/* Ast/ident.scm 97 */
																if ((BgL_walkerz00_134 < (BgL_lenz00_132 - 1L)))
																	{	/* Ast/ident.scm 98 */
																		BgL_test1283z00_513 =
																			(STRING_REF(BgL_stringz00_131,
																				(BgL_walkerz00_134 + 1L)) ==
																			((unsigned char) ':'));
																	}
																else
																	{	/* Ast/ident.scm 98 */
																		BgL_test1283z00_513 = ((bool_t) 0);
																	}
															}
														else
															{	/* Ast/ident.scm 97 */
																BgL_test1283z00_513 = ((bool_t) 0);
															}
														if (BgL_test1283z00_513)
															{	/* Ast/ident.scm 97 */
																return
																	bstring_to_symbol(c_substring
																	(BgL_stringz00_131, 0L, BgL_walkerz00_134));
															}
														else
															{
																long BgL_walkerz00_525;

																BgL_walkerz00_525 = (BgL_walkerz00_134 + 1L);
																BgL_walkerz00_134 = BgL_walkerz00_525;
																goto BgL_zc3z04anonymousza31089ze3z87_135;
															}
													}
											}
										}
									}
								}
							}
						else
							{	/* Ast/ident.scm 106 */
								if (SYMBOLP(BgL_idz00_14))
									{
										obj_t BgL_idz00_530;

										BgL_idz00_530 = BgL_idz00_14;
										BgL_idz00_129 = BgL_idz00_530;
										goto BgL_zc3z04anonymousza31088ze3z87_130;
									}
								else
									{	/* Ast/ident.scm 108 */
										return
											BGl_userzd2errorzd2zztools_errorz00
											(BGl_string1227z00zzast_identz00,
											BGl_string1225z00zzast_identz00, BgL_idz00_14, BNIL);
									}
							}
					}
			}
		}

	}



/* &fast-id-of-id */
	obj_t BGl_z62fastzd2idzd2ofzd2idzb0zzast_identz00(obj_t BgL_envz00_363,
		obj_t BgL_idz00_364, obj_t BgL_locz00_365)
	{
		{	/* Ast/ident.scm 89 */
			return
				BGl_fastzd2idzd2ofzd2idzd2zzast_identz00(BgL_idz00_364, BgL_locz00_365);
		}

	}



/* parse-id/use */
	obj_t BGl_parsezd2idzf2usez20zzast_identz00(obj_t BgL_idz00_16,
		obj_t BgL_locz00_17, obj_t BgL_usezd2typezd2_18)
	{
		{	/* Ast/ident.scm 116 */
			if (SYMBOLP(BgL_idz00_16))
				{	/* Ast/ident.scm 119 */
					obj_t BgL_stringz00_157;

					{	/* Ast/ident.scm 119 */
						obj_t BgL_arg1455z00_280;

						BgL_arg1455z00_280 = SYMBOL_TO_STRING(BgL_idz00_16);
						BgL_stringz00_157 =
							BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_280);
					}
					{	/* Ast/ident.scm 119 */
						long BgL_lenz00_158;

						BgL_lenz00_158 = STRING_LENGTH(BgL_stringz00_157);
						{	/* Ast/ident.scm 120 */

							{
								long BgL_walkerz00_160;
								long BgL_idzd2stopzd2_161;
								long BgL_typezd2startzd2_162;

								BgL_walkerz00_160 = 0L;
								BgL_idzd2stopzd2_161 = 0L;
								BgL_typezd2startzd2_162 = 0L;
							BgL_zc3z04anonymousza31131ze3z87_163:
								if ((BgL_walkerz00_160 == BgL_lenz00_158))
									{	/* Ast/ident.scm 127 */
										bool_t BgL_test1289z00_540;

										if ((BgL_idzd2stopzd2_161 == 0L))
											{	/* Ast/ident.scm 127 */
												BgL_test1289z00_540 = (BgL_typezd2startzd2_162 > 0L);
											}
										else
											{	/* Ast/ident.scm 127 */
												BgL_test1289z00_540 = ((bool_t) 0);
											}
										if (BgL_test1289z00_540)
											{	/* Ast/ident.scm 130 */
												obj_t BgL_idz00_167;
												obj_t BgL_tidz00_168;

												BgL_idz00_167 =
													bstring_to_symbol(BGl_string1228z00zzast_identz00);
												BgL_tidz00_168 =
													bstring_to_symbol(c_substring(BgL_stringz00_157,
														BgL_typezd2startzd2_162, BgL_lenz00_158));
												{	/* Ast/ident.scm 133 */
													obj_t BgL_arg1137z00_169;

													BgL_arg1137z00_169 =
														BGL_PROCEDURE_CALL2(BgL_usezd2typezd2_18,
														BgL_tidz00_168, BgL_locz00_17);
													return MAKE_YOUNG_PAIR(BgL_idz00_167,
														BgL_arg1137z00_169);
												}
											}
										else
											{	/* Ast/ident.scm 127 */
												if ((BgL_idzd2stopzd2_161 == 0L))
													{	/* Ast/ident.scm 135 */
														BgL_typez00_bglt BgL_arg1140z00_172;

														BgL_arg1140z00_172 =
															BGl_getzd2defaultzd2typez00zztype_cachez00();
														return
															MAKE_YOUNG_PAIR(BgL_idz00_16,
															((obj_t) BgL_arg1140z00_172));
													}
												else
													{	/* Ast/ident.scm 134 */
														if ((BgL_typezd2startzd2_162 == BgL_lenz00_158))
															{	/* Ast/ident.scm 141 */
																obj_t BgL_arg1142z00_174;

																{	/* Ast/ident.scm 141 */
																	BgL_typez00_bglt BgL_arg1145z00_176;

																	BgL_arg1145z00_176 =
																		BGl_getzd2defaultzd2typez00zztype_cachez00
																		();
																	BgL_arg1142z00_174 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(1),
																		((obj_t) BgL_arg1145z00_176));
																}
																{	/* Ast/ident.scm 138 */
																	obj_t BgL_list1143z00_175;

																	BgL_list1143z00_175 =
																		MAKE_YOUNG_PAIR(BgL_arg1142z00_174, BNIL);
																	return
																		BGl_userzd2errorzd2zztools_errorz00
																		(BGl_string1229z00zzast_identz00,
																		BGl_string1230z00zzast_identz00,
																		BgL_idz00_16, BgL_list1143z00_175);
																}
															}
														else
															{	/* Ast/ident.scm 143 */
																obj_t BgL_idz00_177;
																obj_t BgL_tidz00_178;

																BgL_idz00_177 =
																	bstring_to_symbol(c_substring
																	(BgL_stringz00_157, 0L,
																		BgL_idzd2stopzd2_161));
																BgL_tidz00_178 =
																	bstring_to_symbol(c_substring
																	(BgL_stringz00_157, BgL_typezd2startzd2_162,
																		BgL_lenz00_158));
																{	/* Ast/ident.scm 146 */
																	obj_t BgL_arg1148z00_179;

																	BgL_arg1148z00_179 =
																		BGL_PROCEDURE_CALL2(BgL_usezd2typezd2_18,
																		BgL_tidz00_178, BgL_locz00_17);
																	return MAKE_YOUNG_PAIR(BgL_idz00_177,
																		BgL_arg1148z00_179);
																}
															}
													}
											}
									}
								else
									{	/* Ast/ident.scm 147 */
										bool_t BgL_test1293z00_576;

										if (
											(STRING_REF(BgL_stringz00_157,
													BgL_walkerz00_160) == ((unsigned char) ':')))
											{	/* Ast/ident.scm 147 */
												if ((BgL_walkerz00_160 < (BgL_lenz00_158 - 1L)))
													{	/* Ast/ident.scm 148 */
														BgL_test1293z00_576 =
															(STRING_REF(BgL_stringz00_157,
																(BgL_walkerz00_160 + 1L)) ==
															((unsigned char) ':'));
													}
												else
													{	/* Ast/ident.scm 148 */
														BgL_test1293z00_576 = ((bool_t) 0);
													}
											}
										else
											{	/* Ast/ident.scm 147 */
												BgL_test1293z00_576 = ((bool_t) 0);
											}
										if (BgL_test1293z00_576)
											{	/* Ast/ident.scm 147 */
												if ((BgL_typezd2startzd2_162 > 0L))
													{	/* Ast/ident.scm 154 */
														obj_t BgL_arg1182z00_193;

														{	/* Ast/ident.scm 154 */
															BgL_typez00_bglt BgL_arg1187z00_195;

															BgL_arg1187z00_195 =
																BGl_getzd2defaultzd2typez00zztype_cachez00();
															BgL_arg1182z00_193 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(1),
																((obj_t) BgL_arg1187z00_195));
														}
														{	/* Ast/ident.scm 151 */
															obj_t BgL_list1183z00_194;

															BgL_list1183z00_194 =
																MAKE_YOUNG_PAIR(BgL_arg1182z00_193, BNIL);
															return
																BGl_userzd2errorzd2zztools_errorz00
																(BGl_string1229z00zzast_identz00,
																BGl_string1230z00zzast_identz00, BgL_idz00_16,
																BgL_list1183z00_194);
														}
													}
												else
													{
														long BgL_typezd2startzd2_597;
														long BgL_idzd2stopzd2_596;
														long BgL_walkerz00_594;

														BgL_walkerz00_594 = (BgL_walkerz00_160 + 2L);
														BgL_idzd2stopzd2_596 = BgL_walkerz00_160;
														BgL_typezd2startzd2_597 = (BgL_walkerz00_160 + 2L);
														BgL_typezd2startzd2_162 = BgL_typezd2startzd2_597;
														BgL_idzd2stopzd2_161 = BgL_idzd2stopzd2_596;
														BgL_walkerz00_160 = BgL_walkerz00_594;
														goto BgL_zc3z04anonymousza31131ze3z87_163;
													}
											}
										else
											{
												long BgL_walkerz00_599;

												BgL_walkerz00_599 = (BgL_walkerz00_160 + 1L);
												BgL_walkerz00_160 = BgL_walkerz00_599;
												goto BgL_zc3z04anonymousza31131ze3z87_163;
											}
									}
							}
						}
					}
				}
			else
				{	/* Ast/ident.scm 117 */
					return
						BGl_userzd2errorzd2zztools_errorz00(BGl_string1227z00zzast_identz00,
						BGl_string1225z00zzast_identz00, BgL_idz00_16, BNIL);
				}
		}

	}



/* parse-id/import-location */
	BGL_EXPORTED_DEF obj_t
		BGl_parsezd2idzf2importzd2locationzf2zzast_identz00(obj_t BgL_idz00_19,
		obj_t BgL_locz00_20, obj_t BgL_lociz00_21)
	{
		{	/* Ast/ident.scm 162 */
			{	/* Ast/ident.scm 165 */
				obj_t BgL_zc3z04anonymousza31199ze3z87_366;

				BgL_zc3z04anonymousza31199ze3z87_366 =
					MAKE_FX_PROCEDURE
					(BGl_z62zc3z04anonymousza31199ze31223ze5zzast_identz00, (int) (2L),
					(int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31199ze3z87_366, (int) (0L),
					BgL_lociz00_21);
				return BGl_parsezd2idzf2usez20zzast_identz00(BgL_idz00_19,
					BgL_locz00_20, BgL_zc3z04anonymousza31199ze3z87_366);
			}
		}

	}



/* &parse-id/import-location */
	obj_t BGl_z62parsezd2idzf2importzd2locationz90zzast_identz00(obj_t
		BgL_envz00_367, obj_t BgL_idz00_368, obj_t BgL_locz00_369,
		obj_t BgL_lociz00_370)
	{
		{	/* Ast/ident.scm 162 */
			return
				BGl_parsezd2idzf2importzd2locationzf2zzast_identz00(BgL_idz00_368,
				BgL_locz00_369, BgL_lociz00_370);
		}

	}



/* &<@anonymous:1199>1223 */
	obj_t BGl_z62zc3z04anonymousza31199ze31223ze5zzast_identz00(obj_t
		BgL_envz00_371, obj_t BgL_tidz00_373, obj_t BgL_locz00_374)
	{
		{	/* Ast/ident.scm 165 */
			{	/* Ast/ident.scm 165 */
				obj_t BgL_lociz00_372;

				BgL_lociz00_372 = PROCEDURE_REF(BgL_envz00_371, (int) (0L));
				return
					((obj_t)
					BGl_usezd2typezf2importzd2locz12ze0zztype_envz00(BgL_tidz00_373,
						BgL_locz00_374, BgL_lociz00_372));
			}
		}

	}



/* parse-id */
	BGL_EXPORTED_DEF obj_t BGl_parsezd2idzd2zzast_identz00(obj_t BgL_idz00_22,
		obj_t BgL_locz00_23)
	{
		{	/* Ast/ident.scm 170 */
			return
				BGl_parsezd2idzf2usez20zzast_identz00(BgL_idz00_22, BgL_locz00_23,
				BGl_usezd2typez12zd2envz12zztype_envz00);
		}

	}



/* &parse-id */
	obj_t BGl_z62parsezd2idzb0zzast_identz00(obj_t BgL_envz00_375,
		obj_t BgL_idz00_376, obj_t BgL_locz00_377)
	{
		{	/* Ast/ident.scm 170 */
			return BGl_parsezd2idzd2zzast_identz00(BgL_idz00_376, BgL_locz00_377);
		}

	}



/* id->name */
	BGL_EXPORTED_DEF obj_t BGl_idzd2ze3namez31zzast_identz00(obj_t BgL_idz00_24)
	{
		{	/* Ast/ident.scm 176 */
			{	/* Ast/ident.scm 177 */
				obj_t BgL_namez00_320;

				{	/* Ast/ident.scm 177 */
					obj_t BgL_arg1200z00_321;

					{	/* Ast/ident.scm 177 */
						obj_t BgL_arg1455z00_323;

						BgL_arg1455z00_323 = SYMBOL_TO_STRING(((obj_t) BgL_idz00_24));
						BgL_arg1200z00_321 =
							BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_323);
					}
					BgL_namez00_320 =
						BGl_stringzd2downcasezd2zz__r4_strings_6_7z00(BgL_arg1200z00_321);
				}
				return bigloo_mangle(BgL_namez00_320);
			}
		}

	}



/* &id->name */
	obj_t BGl_z62idzd2ze3namez53zzast_identz00(obj_t BgL_envz00_378,
		obj_t BgL_idz00_379)
	{
		{	/* Ast/ident.scm 176 */
			return BGl_idzd2ze3namez31zzast_identz00(BgL_idz00_379);
		}

	}



/* local-id->name */
	BGL_EXPORTED_DEF obj_t BGl_localzd2idzd2ze3nameze3zzast_identz00(obj_t
		BgL_idz00_25)
	{
		{	/* Ast/ident.scm 183 */
			{	/* Ast/ident.scm 177 */
				obj_t BgL_namez00_324;

				{	/* Ast/ident.scm 177 */
					obj_t BgL_arg1200z00_325;

					{	/* Ast/ident.scm 177 */
						obj_t BgL_arg1455z00_327;

						BgL_arg1455z00_327 = SYMBOL_TO_STRING(BgL_idz00_25);
						BgL_arg1200z00_325 =
							BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_327);
					}
					BgL_namez00_324 =
						BGl_stringzd2downcasezd2zz__r4_strings_6_7z00(BgL_arg1200z00_325);
				}
				return bigloo_mangle(BgL_namez00_324);
			}
		}

	}



/* &local-id->name */
	obj_t BGl_z62localzd2idzd2ze3namez81zzast_identz00(obj_t BgL_envz00_380,
		obj_t BgL_idz00_381)
	{
		{	/* Ast/ident.scm 183 */
			return BGl_localzd2idzd2ze3nameze3zzast_identz00(BgL_idz00_381);
		}

	}



/* check-id */
	BGL_EXPORTED_DEF obj_t BGl_checkzd2idzd2zzast_identz00(obj_t BgL_idz00_26,
		obj_t BgL_srcz00_27)
	{
		{	/* Ast/ident.scm 189 */
			{	/* Ast/ident.scm 190 */
				bool_t BgL_test1301z00_626;

				{	/* Ast/ident.scm 190 */
					obj_t BgL_arg1212z00_222;

					{	/* Ast/ident.scm 190 */
						obj_t BgL_arg1215z00_223;

						BgL_arg1215z00_223 = CAR(BgL_idz00_26);
						{	/* Ast/ident.scm 190 */
							obj_t BgL_arg1455z00_330;

							BgL_arg1455z00_330 =
								SYMBOL_TO_STRING(((obj_t) BgL_arg1215z00_223));
							BgL_arg1212z00_222 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_330);
						}
					}
					BgL_test1301z00_626 = (STRING_LENGTH(BgL_arg1212z00_222) == 0L);
				}
				if (BgL_test1301z00_626)
					{	/* Ast/ident.scm 192 */
						obj_t BgL_arg1206z00_217;

						{	/* Ast/ident.scm 192 */
							obj_t BgL_arg1208z00_219;

							BgL_arg1208z00_219 =
								(((BgL_typez00_bglt) COBJECT(
										((BgL_typez00_bglt) CDR(BgL_idz00_26))))->BgL_idz00);
							{	/* Ast/ident.scm 192 */
								obj_t BgL_list1209z00_220;

								BgL_list1209z00_220 = MAKE_YOUNG_PAIR(BgL_arg1208z00_219, BNIL);
								BgL_arg1206z00_217 =
									BGl_formatz00zz__r4_output_6_10_3z00
									(BGl_string1231z00zzast_identz00, BgL_list1209z00_220);
							}
						}
						return
							BGl_userzd2errorzd2zztools_errorz00
							(BGl_string1232z00zzast_identz00, BgL_arg1206z00_217,
							BgL_srcz00_27, BNIL);
					}
				else
					{	/* Ast/ident.scm 190 */
						return BgL_idz00_26;
					}
			}
		}

	}



/* &check-id */
	obj_t BGl_z62checkzd2idzb0zzast_identz00(obj_t BgL_envz00_382,
		obj_t BgL_idz00_383, obj_t BgL_srcz00_384)
	{
		{	/* Ast/ident.scm 189 */
			return BGl_checkzd2idzd2zzast_identz00(BgL_idz00_383, BgL_srcz00_384);
		}

	}



/* parse-dsssl */
	BGL_EXPORTED_DEF obj_t BGl_parsezd2dssslzd2zzast_identz00(obj_t BgL_objz00_28)
	{
		{	/* Ast/ident.scm 198 */
			if (BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(BgL_objz00_28))
				{	/* Ast/ident.scm 200 */
					return
						MAKE_YOUNG_PAIR(BgL_objz00_28, BGl_za2objza2z00zztype_cachez00);
				}
			else
				{	/* Ast/ident.scm 200 */
					if (CBOOL(BGl_dssslzd2defaultedzd2formalzf3zf3zztools_dssslz00
							(BgL_objz00_28)))
						{	/* Ast/ident.scm 202 */
							return
								MAKE_YOUNG_PAIR(BgL_objz00_28, BGl_za2objza2z00zztype_cachez00);
						}
					else
						{	/* Ast/ident.scm 202 */
							return
								BGl_userzd2errorzd2zztools_errorz00
								(BGl_string1233z00zzast_identz00,
								BGl_string1228z00zzast_identz00, BgL_objz00_28, BNIL);
						}
				}
		}

	}



/* &parse-dsssl */
	obj_t BGl_z62parsezd2dssslzb0zzast_identz00(obj_t BgL_envz00_385,
		obj_t BgL_objz00_386)
	{
		{	/* Ast/ident.scm 198 */
			return BGl_parsezd2dssslzd2zzast_identz00(BgL_objz00_386);
		}

	}



/* mark-symbol-non-user! */
	BGL_EXPORTED_DEF obj_t
		BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00(obj_t BgL_symz00_29)
	{
		{	/* Ast/ident.scm 217 */
			BGl_putpropz12z12zz__r4_symbols_6_4z00(BgL_symz00_29,
				CNST_TABLE_REF(2), BTRUE);
			return BgL_symz00_29;
		}

	}



/* &mark-symbol-non-user! */
	obj_t BGl_z62markzd2symbolzd2nonzd2userz12za2zzast_identz00(obj_t
		BgL_envz00_387, obj_t BgL_symz00_388)
	{
		{	/* Ast/ident.scm 217 */
			return BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00(BgL_symz00_388);
		}

	}



/* user-symbol? */
	BGL_EXPORTED_DEF bool_t BGl_userzd2symbolzf3z21zzast_identz00(obj_t
		BgL_symbolz00_30)
	{
		{	/* Ast/ident.scm 226 */
			if (CBOOL(BGl_getpropz00zz__r4_symbols_6_4z00(BgL_symbolz00_30,
						CNST_TABLE_REF(2))))
				{	/* Ast/ident.scm 227 */
					return ((bool_t) 0);
				}
			else
				{	/* Ast/ident.scm 227 */
					return ((bool_t) 1);
				}
		}

	}



/* &user-symbol? */
	obj_t BGl_z62userzd2symbolzf3z43zzast_identz00(obj_t BgL_envz00_389,
		obj_t BgL_symbolz00_390)
	{
		{	/* Ast/ident.scm 226 */
			return BBOOL(BGl_userzd2symbolzf3z21zzast_identz00(BgL_symbolz00_390));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_identz00(void)
	{
		{	/* Ast/ident.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_identz00(void)
	{
		{	/* Ast/ident.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_identz00(void)
	{
		{	/* Ast/ident.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_identz00(void)
	{
		{	/* Ast/ident.scm 14 */
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1234z00zzast_identz00));
			BGl_modulezd2initializa7ationz75zztools_dssslz00(275867955L,
				BSTRING_TO_STRING(BGl_string1234z00zzast_identz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1234z00zzast_identz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1234z00zzast_identz00));
			return
				BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1234z00zzast_identz00));
		}

	}

#ifdef __cplusplus
}
#endif
