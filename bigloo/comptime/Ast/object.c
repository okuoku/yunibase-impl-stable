/*===========================================================================*/
/*   (Ast/object.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/object.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_AST_OBJECT_TYPE_DEFINITIONS
#define BGL_AST_OBJECT_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_slotz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_srcz00;
		obj_t BgL_classzd2ownerzd2;
		long BgL_indexz00;
		obj_t BgL_typez00;
		bool_t BgL_readzd2onlyzf3z21;
		obj_t BgL_defaultzd2valuezd2;
		obj_t BgL_virtualzd2numzd2;
		bool_t BgL_virtualzd2overridezd2;
		obj_t BgL_getterz00;
		obj_t BgL_setterz00;
		obj_t BgL_userzd2infozd2;
	}              *BgL_slotz00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;


#endif													// BGL_AST_OBJECT_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl___bigloo_wallow__z00zzast_objectz00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_fieldzd2refzd2ze3nodeze3zzast_objectz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt BGl_z62fieldzd2refzd2ze3nodez81zzast_objectz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzast_objectz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_tclassz00zzobject_classz00;
	static obj_t BGl_toplevelzd2initzd2zzast_objectz00(void);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzast_objectz00(void);
	static obj_t BGl_makezd2fieldzd2setz12z12zzast_objectz00(obj_t, obj_t,
		BgL_nodez00_bglt, obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzast_objectz00(void);
	extern obj_t BGl_privatezd2nodezd2zzast_privatez00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_IMPORT bool_t BGl_2zd3zd3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_findzd2classzd2slotz00zzobject_toolsz00(BgL_typez00_bglt,
		obj_t);
	static obj_t BGl___bigloo__z00zzast_objectz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzast_objectz00(void);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	extern obj_t BGl_varz00zzast_nodez00;
	static obj_t BGl__fieldzd2accesszd2zzast_objectz00(obj_t, obj_t);
	extern obj_t BGl_jclassz00zzobject_classz00;
	extern obj_t BGl_wclassz00zzobject_classz00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzast_objectz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_feffectz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_privatez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_applyz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_appz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_exitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_letz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_labelsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_pragmaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_buildz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_dssslz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_EXPORTED_DECL obj_t BGl_fieldzd2accesszd2zzast_objectz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zzast_objectz00(void);
	static obj_t BGl_makezd2fieldzd2refz00zzast_objectz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_objectz00(void);
	static obj_t BGl_sourcezd2ze3fieldz31zzast_objectz00(obj_t);
	extern BgL_nodez00_bglt BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzast_objectz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzast_objectz00(void);
	extern BgL_nodez00_bglt BGl_sexpzd2ze3nodez31zzast_sexpz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_makezd2classzd2refz00zzobject_toolsz00(BgL_typez00_bglt,
		BgL_slotz00_bglt, obj_t);
	extern obj_t BGl_makezd2classzd2setz12z12zzobject_toolsz00(BgL_typez00_bglt,
		BgL_slotz00_bglt, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_fieldzd2setzd2ze3nodeze3zzast_objectz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62fieldzd2setzd2ze3nodez81zzast_objectz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t __cnst[6];


	   
		 
		DEFINE_STRING(BGl_string1760z00zzast_objectz00,
		BgL_bgl_string1760za700za7za7a1771za7, "#!bigloo", 8);
	      DEFINE_STRING(BGl_string1761z00zzast_objectz00,
		BgL_bgl_string1761za700za7za7a1772za7, "#!bigloo_wallow", 15);
	      DEFINE_STRING(BGl_string1762z00zzast_objectz00,
		BgL_bgl_string1762za700za7za7a1773za7, "Class \"~a\" has not field \"~a\"",
		29);
	      DEFINE_STRING(BGl_string1763z00zzast_objectz00,
		BgL_bgl_string1763za700za7za7a1774za7, "Static type not a class", 23);
	      DEFINE_STRING(BGl_string1764z00zzast_objectz00,
		BgL_bgl_string1764za700za7za7a1775za7, "Unbound variable", 16);
	      DEFINE_STRING(BGl_string1765z00zzast_objectz00,
		BgL_bgl_string1765za700za7za7a1776za7, "Field read-only \"~a\"", 20);
	      DEFINE_STRING(BGl_string1766z00zzast_objectz00,
		BgL_bgl_string1766za700za7za7a1777za7, "Class \"~a\" has no field \"~a\"",
		28);
	      DEFINE_STRING(BGl_string1767z00zzast_objectz00,
		BgL_bgl_string1767za700za7za7a1778za7, "ast_object", 10);
	      DEFINE_STRING(BGl_string1768z00zzast_objectz00,
		BgL_bgl_string1768za700za7za7a1779za7,
		"call-virtual-setter @ call-virtual-getter __object set! -> ", 59);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2setzd2ze3nodezd2envz31zzast_objectz00,
		BgL_bgl_za762fieldza7d2setza7d1780za7,
		BGl_z62fieldzd2setzd2ze3nodez81zzast_objectz00, 0L, BUNSPEC, 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fieldzd2refzd2ze3nodezd2envz31zzast_objectz00,
		BgL_bgl_za762fieldza7d2refza7d1781za7,
		BGl_z62fieldzd2refzd2ze3nodez81zzast_objectz00, 0L, BUNSPEC, 5);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_fieldzd2accesszd2envz00zzast_objectz00,
		BgL_bgl__fieldza7d2accessza71782z00, opt_generic_entry,
		BGl__fieldzd2accesszd2zzast_objectz00, BFALSE, -1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl___bigloo_wallow__z00zzast_objectz00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzast_objectz00));
		     ADD_ROOT((void *) (&BGl___bigloo__z00zzast_objectz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzast_objectz00(long
		BgL_checksumz00_2534, char *BgL_fromz00_2535)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_objectz00))
				{
					BGl_requirezd2initializa7ationz75zzast_objectz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_objectz00();
					BGl_libraryzd2moduleszd2initz00zzast_objectz00();
					BGl_cnstzd2initzd2zzast_objectz00();
					BGl_importedzd2moduleszd2initz00zzast_objectz00();
					return BGl_toplevelzd2initzd2zzast_objectz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_objectz00(void)
	{
		{	/* Ast/object.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "ast_object");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "ast_object");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "ast_object");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_object");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "ast_object");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "ast_object");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"ast_object");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "ast_object");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"ast_object");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_object");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "ast_object");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "ast_object");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_objectz00(void)
	{
		{	/* Ast/object.scm 15 */
			{	/* Ast/object.scm 15 */
				obj_t BgL_cportz00_2523;

				{	/* Ast/object.scm 15 */
					obj_t BgL_stringz00_2530;

					BgL_stringz00_2530 = BGl_string1768z00zzast_objectz00;
					{	/* Ast/object.scm 15 */
						obj_t BgL_startz00_2531;

						BgL_startz00_2531 = BINT(0L);
						{	/* Ast/object.scm 15 */
							obj_t BgL_endz00_2532;

							BgL_endz00_2532 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2530)));
							{	/* Ast/object.scm 15 */

								BgL_cportz00_2523 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2530, BgL_startz00_2531, BgL_endz00_2532);
				}}}}
				{
					long BgL_iz00_2524;

					BgL_iz00_2524 = 5L;
				BgL_loopz00_2525:
					if ((BgL_iz00_2524 == -1L))
						{	/* Ast/object.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Ast/object.scm 15 */
							{	/* Ast/object.scm 15 */
								obj_t BgL_arg1770z00_2526;

								{	/* Ast/object.scm 15 */

									{	/* Ast/object.scm 15 */
										obj_t BgL_locationz00_2528;

										BgL_locationz00_2528 = BBOOL(((bool_t) 0));
										{	/* Ast/object.scm 15 */

											BgL_arg1770z00_2526 =
												BGl_readz00zz__readerz00(BgL_cportz00_2523,
												BgL_locationz00_2528);
										}
									}
								}
								{	/* Ast/object.scm 15 */
									int BgL_tmpz00_2565;

									BgL_tmpz00_2565 = (int) (BgL_iz00_2524);
									CNST_TABLE_SET(BgL_tmpz00_2565, BgL_arg1770z00_2526);
							}}
							{	/* Ast/object.scm 15 */
								int BgL_auxz00_2529;

								BgL_auxz00_2529 = (int) ((BgL_iz00_2524 - 1L));
								{
									long BgL_iz00_2570;

									BgL_iz00_2570 = (long) (BgL_auxz00_2529);
									BgL_iz00_2524 = BgL_iz00_2570;
									goto BgL_loopz00_2525;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_objectz00(void)
	{
		{	/* Ast/object.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzast_objectz00(void)
	{
		{	/* Ast/object.scm 15 */
			BGl___bigloo__z00zzast_objectz00 =
				bstring_to_symbol(BGl_string1760z00zzast_objectz00);
			return (BGl___bigloo_wallow__z00zzast_objectz00 =
				bstring_to_symbol(BGl_string1761z00zzast_objectz00), BUNSPEC);
		}

	}



/* _field-access */
	obj_t BGl__fieldzd2accesszd2zzast_objectz00(obj_t BgL_env1250z00_21,
		obj_t BgL_opt1249z00_20)
	{
		{	/* Ast/object.scm 65 */
			{	/* Ast/object.scm 65 */
				obj_t BgL_g1251z00_1761;
				obj_t BgL_g1252z00_1762;

				BgL_g1251z00_1761 = VECTOR_REF(BgL_opt1249z00_20, 0L);
				BgL_g1252z00_1762 = VECTOR_REF(BgL_opt1249z00_20, 1L);
				switch (VECTOR_LENGTH(BgL_opt1249z00_20))
					{
					case 2L:

						{	/* Ast/object.scm 65 */

							return
								BGl_fieldzd2accesszd2zzast_objectz00(BgL_g1251z00_1761,
								BgL_g1252z00_1762, BFALSE);
						}
						break;
					case 3L:

						{	/* Ast/object.scm 65 */
							obj_t BgL_writezd2allowzd2_1766;

							BgL_writezd2allowzd2_1766 = VECTOR_REF(BgL_opt1249z00_20, 2L);
							{	/* Ast/object.scm 65 */

								return
									BGl_fieldzd2accesszd2zzast_objectz00(BgL_g1251z00_1761,
									BgL_g1252z00_1762, BgL_writezd2allowzd2_1766);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* field-access */
	BGL_EXPORTED_DEF obj_t BGl_fieldzd2accesszd2zzast_objectz00(obj_t
		BgL_varz00_17, obj_t BgL_fieldz00_18, obj_t BgL_writezd2allowzd2_19)
	{
		{	/* Ast/object.scm 65 */
			{	/* Ast/object.scm 66 */
				obj_t BgL_arg1314z00_1767;

				{	/* Ast/object.scm 66 */
					obj_t BgL_arg1315z00_1768;
					obj_t BgL_arg1316z00_1769;

					if (CBOOL(BgL_writezd2allowzd2_19))
						{	/* Ast/object.scm 66 */
							BgL_arg1315z00_1768 = BGl___bigloo_wallow__z00zzast_objectz00;
						}
					else
						{	/* Ast/object.scm 66 */
							BgL_arg1315z00_1768 = BGl___bigloo__z00zzast_objectz00;
						}
					{	/* Ast/object.scm 66 */
						obj_t BgL_arg1317z00_1770;

						BgL_arg1317z00_1770 = MAKE_YOUNG_PAIR(BgL_fieldz00_18, BNIL);
						BgL_arg1316z00_1769 =
							MAKE_YOUNG_PAIR(BgL_varz00_17, BgL_arg1317z00_1770);
					}
					BgL_arg1314z00_1767 =
						MAKE_YOUNG_PAIR(BgL_arg1315z00_1768, BgL_arg1316z00_1769);
				}
				return MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BgL_arg1314z00_1767);
			}
		}

	}



/* source->field */
	obj_t BGl_sourcezd2ze3fieldz31zzast_objectz00(obj_t BgL_lz00_22)
	{
		{	/* Ast/object.scm 71 */
			{	/* Ast/object.scm 72 */
				bool_t BgL_test1786z00_2589;

				{	/* Ast/object.scm 72 */
					bool_t BgL_test1787z00_2590;

					{	/* Ast/object.scm 72 */
						obj_t BgL_arg1325z00_1778;

						BgL_arg1325z00_1778 = CAR(((obj_t) BgL_lz00_22));
						BgL_test1787z00_2590 =
							(BgL_arg1325z00_1778 == BGl___bigloo__z00zzast_objectz00);
					}
					if (BgL_test1787z00_2590)
						{	/* Ast/object.scm 72 */
							BgL_test1786z00_2589 = ((bool_t) 1);
						}
					else
						{	/* Ast/object.scm 72 */
							obj_t BgL_arg1323z00_1777;

							BgL_arg1323z00_1777 = CAR(((obj_t) BgL_lz00_22));
							BgL_test1786z00_2589 =
								(BgL_arg1323z00_1777 ==
								BGl___bigloo_wallow__z00zzast_objectz00);
						}
				}
				if (BgL_test1786z00_2589)
					{	/* Ast/object.scm 72 */
						return CDR(((obj_t) BgL_lz00_22));
					}
				else
					{	/* Ast/object.scm 72 */
						return BgL_lz00_22;
					}
			}
		}

	}



/* field-ref->node */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_fieldzd2refzd2ze3nodeze3zzast_objectz00(obj_t BgL_lz00_23,
		obj_t BgL_expz00_24, obj_t BgL_stackz00_25, obj_t BgL_locz00_26,
		obj_t BgL_sitez00_27)
	{
		{	/* Ast/object.scm 79 */
			{	/* Ast/object.scm 80 */
				obj_t BgL_l2z00_1779;

				BgL_l2z00_1779 = BGl_sourcezd2ze3fieldz31zzast_objectz00(BgL_lz00_23);
				{	/* Ast/object.scm 80 */
					BgL_nodez00_bglt BgL_varz00_1780;

					{	/* Ast/object.scm 81 */
						obj_t BgL_arg1351z00_1808;

						BgL_arg1351z00_1808 = CAR(((obj_t) BgL_l2z00_1779));
						BgL_varz00_1780 =
							BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_arg1351z00_1808,
							BgL_stackz00_25, BgL_locz00_26, BgL_sitez00_27);
					}
					{	/* Ast/object.scm 81 */

						{	/* Ast/object.scm 83 */
							bool_t BgL_test1788z00_2603;

							{	/* Ast/object.scm 83 */
								obj_t BgL_classz00_2066;

								BgL_classz00_2066 = BGl_varz00zzast_nodez00;
								{	/* Ast/object.scm 83 */
									BgL_objectz00_bglt BgL_arg1807z00_2068;

									{	/* Ast/object.scm 83 */
										obj_t BgL_tmpz00_2604;

										BgL_tmpz00_2604 =
											((obj_t) ((BgL_objectz00_bglt) BgL_varz00_1780));
										BgL_arg1807z00_2068 =
											(BgL_objectz00_bglt) (BgL_tmpz00_2604);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Ast/object.scm 83 */
											long BgL_idxz00_2074;

											BgL_idxz00_2074 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2068);
											BgL_test1788z00_2603 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2074 + 2L)) == BgL_classz00_2066);
										}
									else
										{	/* Ast/object.scm 83 */
											bool_t BgL_res1748z00_2099;

											{	/* Ast/object.scm 83 */
												obj_t BgL_oclassz00_2082;

												{	/* Ast/object.scm 83 */
													obj_t BgL_arg1815z00_2090;
													long BgL_arg1816z00_2091;

													BgL_arg1815z00_2090 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Ast/object.scm 83 */
														long BgL_arg1817z00_2092;

														BgL_arg1817z00_2092 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2068);
														BgL_arg1816z00_2091 =
															(BgL_arg1817z00_2092 - OBJECT_TYPE);
													}
													BgL_oclassz00_2082 =
														VECTOR_REF(BgL_arg1815z00_2090,
														BgL_arg1816z00_2091);
												}
												{	/* Ast/object.scm 83 */
													bool_t BgL__ortest_1115z00_2083;

													BgL__ortest_1115z00_2083 =
														(BgL_classz00_2066 == BgL_oclassz00_2082);
													if (BgL__ortest_1115z00_2083)
														{	/* Ast/object.scm 83 */
															BgL_res1748z00_2099 = BgL__ortest_1115z00_2083;
														}
													else
														{	/* Ast/object.scm 83 */
															long BgL_odepthz00_2084;

															{	/* Ast/object.scm 83 */
																obj_t BgL_arg1804z00_2085;

																BgL_arg1804z00_2085 = (BgL_oclassz00_2082);
																BgL_odepthz00_2084 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2085);
															}
															if ((2L < BgL_odepthz00_2084))
																{	/* Ast/object.scm 83 */
																	obj_t BgL_arg1802z00_2087;

																	{	/* Ast/object.scm 83 */
																		obj_t BgL_arg1803z00_2088;

																		BgL_arg1803z00_2088 = (BgL_oclassz00_2082);
																		BgL_arg1802z00_2087 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2088, 2L);
																	}
																	BgL_res1748z00_2099 =
																		(BgL_arg1802z00_2087 == BgL_classz00_2066);
																}
															else
																{	/* Ast/object.scm 83 */
																	BgL_res1748z00_2099 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test1788z00_2603 = BgL_res1748z00_2099;
										}
								}
							}
							if (BgL_test1788z00_2603)
								{	/* Ast/object.scm 84 */
									BgL_variablez00_bglt BgL_i1112z00_1782;

									BgL_i1112z00_1782 =
										(((BgL_varz00_bglt) COBJECT(
												((BgL_varz00_bglt) BgL_varz00_1780)))->BgL_variablez00);
									{	/* Ast/object.scm 85 */
										obj_t BgL_g1113z00_1783;

										BgL_g1113z00_1783 = CDR(((obj_t) BgL_l2z00_1779));
										{
											obj_t BgL_nodez00_1786;
											obj_t BgL_klassz00_1787;
											obj_t BgL_slotsz00_1788;

											{	/* Ast/object.scm 85 */
												BgL_typez00_bglt BgL_arg1327z00_1785;

												BgL_arg1327z00_1785 =
													(((BgL_variablez00_bglt) COBJECT(BgL_i1112z00_1782))->
													BgL_typez00);
												{
													obj_t BgL_auxz00_2632;

													BgL_nodez00_1786 = ((obj_t) BgL_varz00_1780);
													BgL_klassz00_1787 = ((obj_t) BgL_arg1327z00_1785);
													BgL_slotsz00_1788 = BgL_g1113z00_1783;
												BgL_zc3z04anonymousza31328ze3z87_1789:
													if (NULLP(BgL_slotsz00_1788))
														{	/* Ast/object.scm 89 */
															BgL_auxz00_2632 = BgL_nodez00_1786;
														}
													else
														{	/* Ast/object.scm 91 */
															bool_t BgL_test1793z00_2635;

															{	/* Ast/object.scm 91 */
																bool_t BgL_test1794z00_2636;

																{	/* Ast/object.scm 91 */
																	obj_t BgL_classz00_2102;

																	BgL_classz00_2102 =
																		BGl_tclassz00zzobject_classz00;
																	if (BGL_OBJECTP(BgL_klassz00_1787))
																		{	/* Ast/object.scm 91 */
																			BgL_objectz00_bglt BgL_arg1807z00_2104;

																			BgL_arg1807z00_2104 =
																				(BgL_objectz00_bglt)
																				(BgL_klassz00_1787);
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Ast/object.scm 91 */
																					long BgL_idxz00_2110;

																					BgL_idxz00_2110 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_2104);
																					BgL_test1794z00_2636 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_2110 + 2L)) ==
																						BgL_classz00_2102);
																				}
																			else
																				{	/* Ast/object.scm 91 */
																					bool_t BgL_res1749z00_2135;

																					{	/* Ast/object.scm 91 */
																						obj_t BgL_oclassz00_2118;

																						{	/* Ast/object.scm 91 */
																							obj_t BgL_arg1815z00_2126;
																							long BgL_arg1816z00_2127;

																							BgL_arg1815z00_2126 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Ast/object.scm 91 */
																								long BgL_arg1817z00_2128;

																								BgL_arg1817z00_2128 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_2104);
																								BgL_arg1816z00_2127 =
																									(BgL_arg1817z00_2128 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_2118 =
																								VECTOR_REF(BgL_arg1815z00_2126,
																								BgL_arg1816z00_2127);
																						}
																						{	/* Ast/object.scm 91 */
																							bool_t BgL__ortest_1115z00_2119;

																							BgL__ortest_1115z00_2119 =
																								(BgL_classz00_2102 ==
																								BgL_oclassz00_2118);
																							if (BgL__ortest_1115z00_2119)
																								{	/* Ast/object.scm 91 */
																									BgL_res1749z00_2135 =
																										BgL__ortest_1115z00_2119;
																								}
																							else
																								{	/* Ast/object.scm 91 */
																									long BgL_odepthz00_2120;

																									{	/* Ast/object.scm 91 */
																										obj_t BgL_arg1804z00_2121;

																										BgL_arg1804z00_2121 =
																											(BgL_oclassz00_2118);
																										BgL_odepthz00_2120 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_2121);
																									}
																									if ((2L < BgL_odepthz00_2120))
																										{	/* Ast/object.scm 91 */
																											obj_t BgL_arg1802z00_2123;

																											{	/* Ast/object.scm 91 */
																												obj_t
																													BgL_arg1803z00_2124;
																												BgL_arg1803z00_2124 =
																													(BgL_oclassz00_2118);
																												BgL_arg1802z00_2123 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_2124,
																													2L);
																											}
																											BgL_res1749z00_2135 =
																												(BgL_arg1802z00_2123 ==
																												BgL_classz00_2102);
																										}
																									else
																										{	/* Ast/object.scm 91 */
																											BgL_res1749z00_2135 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test1794z00_2636 =
																						BgL_res1749z00_2135;
																				}
																		}
																	else
																		{	/* Ast/object.scm 91 */
																			BgL_test1794z00_2636 = ((bool_t) 0);
																		}
																}
																if (BgL_test1794z00_2636)
																	{	/* Ast/object.scm 91 */
																		BgL_test1793z00_2635 = ((bool_t) 1);
																	}
																else
																	{	/* Ast/object.scm 91 */
																		bool_t BgL_test1801z00_2659;

																		{	/* Ast/object.scm 91 */
																			obj_t BgL_classz00_2136;

																			BgL_classz00_2136 =
																				BGl_jclassz00zzobject_classz00;
																			if (BGL_OBJECTP(BgL_klassz00_1787))
																				{	/* Ast/object.scm 91 */
																					BgL_objectz00_bglt
																						BgL_arg1807z00_2138;
																					BgL_arg1807z00_2138 =
																						(BgL_objectz00_bglt)
																						(BgL_klassz00_1787);
																					if (BGL_CONDEXPAND_ISA_ARCH64())
																						{	/* Ast/object.scm 91 */
																							long BgL_idxz00_2144;

																							BgL_idxz00_2144 =
																								BGL_OBJECT_INHERITANCE_NUM
																								(BgL_arg1807z00_2138);
																							BgL_test1801z00_2659 =
																								(VECTOR_REF
																								(BGl_za2inheritancesza2z00zz__objectz00,
																									(BgL_idxz00_2144 + 2L)) ==
																								BgL_classz00_2136);
																						}
																					else
																						{	/* Ast/object.scm 91 */
																							bool_t BgL_res1750z00_2169;

																							{	/* Ast/object.scm 91 */
																								obj_t BgL_oclassz00_2152;

																								{	/* Ast/object.scm 91 */
																									obj_t BgL_arg1815z00_2160;
																									long BgL_arg1816z00_2161;

																									BgL_arg1815z00_2160 =
																										(BGl_za2classesza2z00zz__objectz00);
																									{	/* Ast/object.scm 91 */
																										long BgL_arg1817z00_2162;

																										BgL_arg1817z00_2162 =
																											BGL_OBJECT_CLASS_NUM
																											(BgL_arg1807z00_2138);
																										BgL_arg1816z00_2161 =
																											(BgL_arg1817z00_2162 -
																											OBJECT_TYPE);
																									}
																									BgL_oclassz00_2152 =
																										VECTOR_REF
																										(BgL_arg1815z00_2160,
																										BgL_arg1816z00_2161);
																								}
																								{	/* Ast/object.scm 91 */
																									bool_t
																										BgL__ortest_1115z00_2153;
																									BgL__ortest_1115z00_2153 =
																										(BgL_classz00_2136 ==
																										BgL_oclassz00_2152);
																									if (BgL__ortest_1115z00_2153)
																										{	/* Ast/object.scm 91 */
																											BgL_res1750z00_2169 =
																												BgL__ortest_1115z00_2153;
																										}
																									else
																										{	/* Ast/object.scm 91 */
																											long BgL_odepthz00_2154;

																											{	/* Ast/object.scm 91 */
																												obj_t
																													BgL_arg1804z00_2155;
																												BgL_arg1804z00_2155 =
																													(BgL_oclassz00_2152);
																												BgL_odepthz00_2154 =
																													BGL_CLASS_DEPTH
																													(BgL_arg1804z00_2155);
																											}
																											if (
																												(2L <
																													BgL_odepthz00_2154))
																												{	/* Ast/object.scm 91 */
																													obj_t
																														BgL_arg1802z00_2157;
																													{	/* Ast/object.scm 91 */
																														obj_t
																															BgL_arg1803z00_2158;
																														BgL_arg1803z00_2158
																															=
																															(BgL_oclassz00_2152);
																														BgL_arg1802z00_2157
																															=
																															BGL_CLASS_ANCESTORS_REF
																															(BgL_arg1803z00_2158,
																															2L);
																													}
																													BgL_res1750z00_2169 =
																														(BgL_arg1802z00_2157
																														==
																														BgL_classz00_2136);
																												}
																											else
																												{	/* Ast/object.scm 91 */
																													BgL_res1750z00_2169 =
																														((bool_t) 0);
																												}
																										}
																								}
																							}
																							BgL_test1801z00_2659 =
																								BgL_res1750z00_2169;
																						}
																				}
																			else
																				{	/* Ast/object.scm 91 */
																					BgL_test1801z00_2659 = ((bool_t) 0);
																				}
																		}
																		if (BgL_test1801z00_2659)
																			{	/* Ast/object.scm 91 */
																				BgL_test1793z00_2635 = ((bool_t) 1);
																			}
																		else
																			{	/* Ast/object.scm 91 */
																				obj_t BgL_classz00_2170;

																				BgL_classz00_2170 =
																					BGl_wclassz00zzobject_classz00;
																				if (BGL_OBJECTP(BgL_klassz00_1787))
																					{	/* Ast/object.scm 91 */
																						BgL_objectz00_bglt
																							BgL_arg1807z00_2172;
																						BgL_arg1807z00_2172 =
																							(BgL_objectz00_bglt)
																							(BgL_klassz00_1787);
																						if (BGL_CONDEXPAND_ISA_ARCH64())
																							{	/* Ast/object.scm 91 */
																								long BgL_idxz00_2178;

																								BgL_idxz00_2178 =
																									BGL_OBJECT_INHERITANCE_NUM
																									(BgL_arg1807z00_2172);
																								BgL_test1793z00_2635 =
																									(VECTOR_REF
																									(BGl_za2inheritancesza2z00zz__objectz00,
																										(BgL_idxz00_2178 + 2L)) ==
																									BgL_classz00_2170);
																							}
																						else
																							{	/* Ast/object.scm 91 */
																								bool_t BgL_res1751z00_2203;

																								{	/* Ast/object.scm 91 */
																									obj_t BgL_oclassz00_2186;

																									{	/* Ast/object.scm 91 */
																										obj_t BgL_arg1815z00_2194;
																										long BgL_arg1816z00_2195;

																										BgL_arg1815z00_2194 =
																											(BGl_za2classesza2z00zz__objectz00);
																										{	/* Ast/object.scm 91 */
																											long BgL_arg1817z00_2196;

																											BgL_arg1817z00_2196 =
																												BGL_OBJECT_CLASS_NUM
																												(BgL_arg1807z00_2172);
																											BgL_arg1816z00_2195 =
																												(BgL_arg1817z00_2196 -
																												OBJECT_TYPE);
																										}
																										BgL_oclassz00_2186 =
																											VECTOR_REF
																											(BgL_arg1815z00_2194,
																											BgL_arg1816z00_2195);
																									}
																									{	/* Ast/object.scm 91 */
																										bool_t
																											BgL__ortest_1115z00_2187;
																										BgL__ortest_1115z00_2187 =
																											(BgL_classz00_2170 ==
																											BgL_oclassz00_2186);
																										if (BgL__ortest_1115z00_2187)
																											{	/* Ast/object.scm 91 */
																												BgL_res1751z00_2203 =
																													BgL__ortest_1115z00_2187;
																											}
																										else
																											{	/* Ast/object.scm 91 */
																												long BgL_odepthz00_2188;

																												{	/* Ast/object.scm 91 */
																													obj_t
																														BgL_arg1804z00_2189;
																													BgL_arg1804z00_2189 =
																														(BgL_oclassz00_2186);
																													BgL_odepthz00_2188 =
																														BGL_CLASS_DEPTH
																														(BgL_arg1804z00_2189);
																												}
																												if (
																													(2L <
																														BgL_odepthz00_2188))
																													{	/* Ast/object.scm 91 */
																														obj_t
																															BgL_arg1802z00_2191;
																														{	/* Ast/object.scm 91 */
																															obj_t
																																BgL_arg1803z00_2192;
																															BgL_arg1803z00_2192
																																=
																																(BgL_oclassz00_2186);
																															BgL_arg1802z00_2191
																																=
																																BGL_CLASS_ANCESTORS_REF
																																(BgL_arg1803z00_2192,
																																2L);
																														}
																														BgL_res1751z00_2203
																															=
																															(BgL_arg1802z00_2191
																															==
																															BgL_classz00_2170);
																													}
																												else
																													{	/* Ast/object.scm 91 */
																														BgL_res1751z00_2203
																															= ((bool_t) 0);
																													}
																											}
																									}
																								}
																								BgL_test1793z00_2635 =
																									BgL_res1751z00_2203;
																							}
																					}
																				else
																					{	/* Ast/object.scm 91 */
																						BgL_test1793z00_2635 = ((bool_t) 0);
																					}
																			}
																	}
															}
															if (BgL_test1793z00_2635)
																{	/* Ast/object.scm 94 */
																	obj_t BgL_slotz00_1794;

																	{	/* Ast/object.scm 94 */
																		obj_t BgL_arg1348z00_1803;

																		BgL_arg1348z00_1803 =
																			CAR(((obj_t) BgL_slotsz00_1788));
																		BgL_slotz00_1794 =
																			BGl_findzd2classzd2slotz00zzobject_toolsz00
																			(((BgL_typez00_bglt) BgL_klassz00_1787),
																			BgL_arg1348z00_1803);
																	}
																	if (CBOOL(BgL_slotz00_1794))
																		{	/* Ast/object.scm 100 */
																			obj_t BgL_nodez00_1795;

																			BgL_nodez00_1795 =
																				BGl_makezd2fieldzd2refz00zzast_objectz00
																				(BgL_slotz00_1794, BgL_nodez00_1786,
																				BgL_stackz00_25, BgL_locz00_26,
																				BgL_sitez00_27);
																			{	/* Ast/object.scm 101 */
																				obj_t BgL_arg1333z00_1796;
																				obj_t BgL_arg1335z00_1797;

																				BgL_arg1333z00_1796 =
																					(((BgL_slotz00_bglt) COBJECT(
																							((BgL_slotz00_bglt)
																								BgL_slotz00_1794)))->
																					BgL_typez00);
																				BgL_arg1335z00_1797 =
																					CDR(((obj_t) BgL_slotsz00_1788));
																				{
																					obj_t BgL_slotsz00_2717;
																					obj_t BgL_klassz00_2716;
																					obj_t BgL_nodez00_2715;

																					BgL_nodez00_2715 = BgL_nodez00_1795;
																					BgL_klassz00_2716 =
																						BgL_arg1333z00_1796;
																					BgL_slotsz00_2717 =
																						BgL_arg1335z00_1797;
																					BgL_slotsz00_1788 = BgL_slotsz00_2717;
																					BgL_klassz00_1787 = BgL_klassz00_2716;
																					BgL_nodez00_1786 = BgL_nodez00_2715;
																					goto
																						BgL_zc3z04anonymousza31328ze3z87_1789;
																				}
																			}
																		}
																	else
																		{	/* Ast/object.scm 98 */
																			obj_t BgL_arg1339z00_1798;

																			{	/* Ast/object.scm 98 */
																				obj_t BgL_arg1340z00_1799;
																				obj_t BgL_arg1342z00_1800;

																				BgL_arg1340z00_1799 =
																					(((BgL_typez00_bglt) COBJECT(
																							((BgL_typez00_bglt)
																								BgL_klassz00_1787)))->
																					BgL_idz00);
																				BgL_arg1342z00_1800 =
																					CAR(((obj_t) BgL_slotsz00_1788));
																				{	/* Ast/object.scm 97 */
																					obj_t BgL_list1343z00_1801;

																					{	/* Ast/object.scm 97 */
																						obj_t BgL_arg1346z00_1802;

																						BgL_arg1346z00_1802 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1342z00_1800, BNIL);
																						BgL_list1343z00_1801 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1340z00_1799,
																							BgL_arg1346z00_1802);
																					}
																					BgL_arg1339z00_1798 =
																						BGl_formatz00zz__r4_output_6_10_3z00
																						(BGl_string1762z00zzast_objectz00,
																						BgL_list1343z00_1801);
																				}
																			}
																			BgL_auxz00_2632 =
																				((obj_t)
																				BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00
																				(BgL_arg1339z00_1798, BgL_expz00_24,
																					BgL_locz00_26));
																		}
																}
															else
																{	/* Ast/object.scm 91 */
																	BgL_auxz00_2632 =
																		((obj_t)
																		BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00
																		(BGl_string1763z00zzast_objectz00,
																			BgL_expz00_24, BgL_locz00_26));
																}
														}
													return ((BgL_nodez00_bglt) BgL_auxz00_2632);
												}
											}
										}
									}
								}
							else
								{	/* Ast/object.scm 102 */
									bool_t BgL_test1814z00_2732;

									if (INTEGERP
										(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
										{	/* Ast/object.scm 102 */
											BgL_test1814z00_2732 =
												(
												(long)
												CINT
												(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) ==
												0L);
										}
									else
										{	/* Ast/object.scm 102 */
											BgL_test1814z00_2732 =
												BGl_2zd3zd3zz__r4_numbers_6_5z00
												(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
												BINT(0L));
										}
									if (BgL_test1814z00_2732)
										{	/* Ast/object.scm 102 */
											return
												BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00
												(BGl_string1764z00zzast_objectz00, BgL_expz00_24,
												BgL_locz00_26);
										}
									else
										{	/* Ast/object.scm 102 */
											return BgL_varz00_1780;
										}
								}
						}
					}
				}
			}
		}

	}



/* &field-ref->node */
	BgL_nodez00_bglt BGl_z62fieldzd2refzd2ze3nodez81zzast_objectz00(obj_t
		BgL_envz00_2509, obj_t BgL_lz00_2510, obj_t BgL_expz00_2511,
		obj_t BgL_stackz00_2512, obj_t BgL_locz00_2513, obj_t BgL_sitez00_2514)
	{
		{	/* Ast/object.scm 79 */
			return
				BGl_fieldzd2refzd2ze3nodeze3zzast_objectz00(BgL_lz00_2510,
				BgL_expz00_2511, BgL_stackz00_2512, BgL_locz00_2513, BgL_sitez00_2514);
		}

	}



/* field-set->node */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_fieldzd2setzd2ze3nodeze3zzast_objectz00(obj_t BgL_lz00_28,
		obj_t BgL_valz00_29, obj_t BgL_expz00_30, obj_t BgL_stackz00_31,
		obj_t BgL_locz00_32, obj_t BgL_sitez00_33)
	{
		{	/* Ast/object.scm 110 */
			{	/* Ast/object.scm 111 */
				obj_t BgL_l2z00_1809;

				BgL_l2z00_1809 = BGl_sourcezd2ze3fieldz31zzast_objectz00(BgL_lz00_28);
				{	/* Ast/object.scm 111 */
					BgL_nodez00_bglt BgL_varz00_1810;

					{	/* Ast/object.scm 112 */
						obj_t BgL_arg1516z00_1857;

						BgL_arg1516z00_1857 = CAR(((obj_t) BgL_l2z00_1809));
						BgL_varz00_1810 =
							BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_arg1516z00_1857,
							BgL_stackz00_31, BgL_locz00_32, BgL_sitez00_33);
					}
					{	/* Ast/object.scm 112 */
						BgL_nodez00_bglt BgL_valz00_1811;

						BgL_valz00_1811 =
							BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_valz00_29, BgL_stackz00_31,
							BgL_locz00_32, BgL_sitez00_33);
						{	/* Ast/object.scm 113 */

							{	/* Ast/object.scm 115 */
								bool_t BgL_test1816z00_2746;

								{	/* Ast/object.scm 115 */
									obj_t BgL_classz00_2211;

									BgL_classz00_2211 = BGl_varz00zzast_nodez00;
									{	/* Ast/object.scm 115 */
										BgL_objectz00_bglt BgL_arg1807z00_2213;

										{	/* Ast/object.scm 115 */
											obj_t BgL_tmpz00_2747;

											BgL_tmpz00_2747 =
												((obj_t) ((BgL_objectz00_bglt) BgL_varz00_1810));
											BgL_arg1807z00_2213 =
												(BgL_objectz00_bglt) (BgL_tmpz00_2747);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Ast/object.scm 115 */
												long BgL_idxz00_2219;

												BgL_idxz00_2219 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2213);
												BgL_test1816z00_2746 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2219 + 2L)) == BgL_classz00_2211);
											}
										else
											{	/* Ast/object.scm 115 */
												bool_t BgL_res1752z00_2244;

												{	/* Ast/object.scm 115 */
													obj_t BgL_oclassz00_2227;

													{	/* Ast/object.scm 115 */
														obj_t BgL_arg1815z00_2235;
														long BgL_arg1816z00_2236;

														BgL_arg1815z00_2235 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Ast/object.scm 115 */
															long BgL_arg1817z00_2237;

															BgL_arg1817z00_2237 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2213);
															BgL_arg1816z00_2236 =
																(BgL_arg1817z00_2237 - OBJECT_TYPE);
														}
														BgL_oclassz00_2227 =
															VECTOR_REF(BgL_arg1815z00_2235,
															BgL_arg1816z00_2236);
													}
													{	/* Ast/object.scm 115 */
														bool_t BgL__ortest_1115z00_2228;

														BgL__ortest_1115z00_2228 =
															(BgL_classz00_2211 == BgL_oclassz00_2227);
														if (BgL__ortest_1115z00_2228)
															{	/* Ast/object.scm 115 */
																BgL_res1752z00_2244 = BgL__ortest_1115z00_2228;
															}
														else
															{	/* Ast/object.scm 115 */
																long BgL_odepthz00_2229;

																{	/* Ast/object.scm 115 */
																	obj_t BgL_arg1804z00_2230;

																	BgL_arg1804z00_2230 = (BgL_oclassz00_2227);
																	BgL_odepthz00_2229 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2230);
																}
																if ((2L < BgL_odepthz00_2229))
																	{	/* Ast/object.scm 115 */
																		obj_t BgL_arg1802z00_2232;

																		{	/* Ast/object.scm 115 */
																			obj_t BgL_arg1803z00_2233;

																			BgL_arg1803z00_2233 =
																				(BgL_oclassz00_2227);
																			BgL_arg1802z00_2232 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2233, 2L);
																		}
																		BgL_res1752z00_2244 =
																			(BgL_arg1802z00_2232 ==
																			BgL_classz00_2211);
																	}
																else
																	{	/* Ast/object.scm 115 */
																		BgL_res1752z00_2244 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test1816z00_2746 = BgL_res1752z00_2244;
											}
									}
								}
								if (BgL_test1816z00_2746)
									{	/* Ast/object.scm 116 */
										BgL_variablez00_bglt BgL_i1114z00_1813;

										BgL_i1114z00_1813 =
											(((BgL_varz00_bglt) COBJECT(
													((BgL_varz00_bglt) BgL_varz00_1810)))->
											BgL_variablez00);
										{	/* Ast/object.scm 117 */
											obj_t BgL_g1115z00_1814;

											BgL_g1115z00_1814 = CDR(((obj_t) BgL_l2z00_1809));
											{
												obj_t BgL_nodez00_1817;
												obj_t BgL_klassz00_1818;
												obj_t BgL_slotsz00_1819;

												{	/* Ast/object.scm 117 */
													BgL_typez00_bglt BgL_arg1361z00_1816;

													BgL_arg1361z00_1816 =
														(((BgL_variablez00_bglt)
															COBJECT(BgL_i1114z00_1813))->BgL_typez00);
													{
														obj_t BgL_auxz00_2775;

														BgL_nodez00_1817 = ((obj_t) BgL_varz00_1810);
														BgL_klassz00_1818 = ((obj_t) BgL_arg1361z00_1816);
														BgL_slotsz00_1819 = BgL_g1115z00_1814;
													BgL_zc3z04anonymousza31362ze3z87_1820:
														{	/* Ast/object.scm 120 */
															bool_t BgL_test1821z00_2776;

															{	/* Ast/object.scm 120 */
																bool_t BgL_test1822z00_2777;

																{	/* Ast/object.scm 120 */
																	obj_t BgL_classz00_2247;

																	BgL_classz00_2247 =
																		BGl_tclassz00zzobject_classz00;
																	if (BGL_OBJECTP(BgL_klassz00_1818))
																		{	/* Ast/object.scm 120 */
																			BgL_objectz00_bglt BgL_arg1807z00_2249;

																			BgL_arg1807z00_2249 =
																				(BgL_objectz00_bglt)
																				(BgL_klassz00_1818);
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Ast/object.scm 120 */
																					long BgL_idxz00_2255;

																					BgL_idxz00_2255 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_2249);
																					BgL_test1822z00_2777 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_2255 + 2L)) ==
																						BgL_classz00_2247);
																				}
																			else
																				{	/* Ast/object.scm 120 */
																					bool_t BgL_res1753z00_2280;

																					{	/* Ast/object.scm 120 */
																						obj_t BgL_oclassz00_2263;

																						{	/* Ast/object.scm 120 */
																							obj_t BgL_arg1815z00_2271;
																							long BgL_arg1816z00_2272;

																							BgL_arg1815z00_2271 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Ast/object.scm 120 */
																								long BgL_arg1817z00_2273;

																								BgL_arg1817z00_2273 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_2249);
																								BgL_arg1816z00_2272 =
																									(BgL_arg1817z00_2273 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_2263 =
																								VECTOR_REF(BgL_arg1815z00_2271,
																								BgL_arg1816z00_2272);
																						}
																						{	/* Ast/object.scm 120 */
																							bool_t BgL__ortest_1115z00_2264;

																							BgL__ortest_1115z00_2264 =
																								(BgL_classz00_2247 ==
																								BgL_oclassz00_2263);
																							if (BgL__ortest_1115z00_2264)
																								{	/* Ast/object.scm 120 */
																									BgL_res1753z00_2280 =
																										BgL__ortest_1115z00_2264;
																								}
																							else
																								{	/* Ast/object.scm 120 */
																									long BgL_odepthz00_2265;

																									{	/* Ast/object.scm 120 */
																										obj_t BgL_arg1804z00_2266;

																										BgL_arg1804z00_2266 =
																											(BgL_oclassz00_2263);
																										BgL_odepthz00_2265 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_2266);
																									}
																									if ((2L < BgL_odepthz00_2265))
																										{	/* Ast/object.scm 120 */
																											obj_t BgL_arg1802z00_2268;

																											{	/* Ast/object.scm 120 */
																												obj_t
																													BgL_arg1803z00_2269;
																												BgL_arg1803z00_2269 =
																													(BgL_oclassz00_2263);
																												BgL_arg1802z00_2268 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_2269,
																													2L);
																											}
																											BgL_res1753z00_2280 =
																												(BgL_arg1802z00_2268 ==
																												BgL_classz00_2247);
																										}
																									else
																										{	/* Ast/object.scm 120 */
																											BgL_res1753z00_2280 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test1822z00_2777 =
																						BgL_res1753z00_2280;
																				}
																		}
																	else
																		{	/* Ast/object.scm 120 */
																			BgL_test1822z00_2777 = ((bool_t) 0);
																		}
																}
																if (BgL_test1822z00_2777)
																	{	/* Ast/object.scm 120 */
																		BgL_test1821z00_2776 = ((bool_t) 1);
																	}
																else
																	{	/* Ast/object.scm 120 */
																		bool_t BgL_test1828z00_2800;

																		{	/* Ast/object.scm 120 */
																			obj_t BgL_classz00_2281;

																			BgL_classz00_2281 =
																				BGl_jclassz00zzobject_classz00;
																			if (BGL_OBJECTP(BgL_klassz00_1818))
																				{	/* Ast/object.scm 120 */
																					BgL_objectz00_bglt
																						BgL_arg1807z00_2283;
																					BgL_arg1807z00_2283 =
																						(BgL_objectz00_bglt)
																						(BgL_klassz00_1818);
																					if (BGL_CONDEXPAND_ISA_ARCH64())
																						{	/* Ast/object.scm 120 */
																							long BgL_idxz00_2289;

																							BgL_idxz00_2289 =
																								BGL_OBJECT_INHERITANCE_NUM
																								(BgL_arg1807z00_2283);
																							BgL_test1828z00_2800 =
																								(VECTOR_REF
																								(BGl_za2inheritancesza2z00zz__objectz00,
																									(BgL_idxz00_2289 + 2L)) ==
																								BgL_classz00_2281);
																						}
																					else
																						{	/* Ast/object.scm 120 */
																							bool_t BgL_res1754z00_2314;

																							{	/* Ast/object.scm 120 */
																								obj_t BgL_oclassz00_2297;

																								{	/* Ast/object.scm 120 */
																									obj_t BgL_arg1815z00_2305;
																									long BgL_arg1816z00_2306;

																									BgL_arg1815z00_2305 =
																										(BGl_za2classesza2z00zz__objectz00);
																									{	/* Ast/object.scm 120 */
																										long BgL_arg1817z00_2307;

																										BgL_arg1817z00_2307 =
																											BGL_OBJECT_CLASS_NUM
																											(BgL_arg1807z00_2283);
																										BgL_arg1816z00_2306 =
																											(BgL_arg1817z00_2307 -
																											OBJECT_TYPE);
																									}
																									BgL_oclassz00_2297 =
																										VECTOR_REF
																										(BgL_arg1815z00_2305,
																										BgL_arg1816z00_2306);
																								}
																								{	/* Ast/object.scm 120 */
																									bool_t
																										BgL__ortest_1115z00_2298;
																									BgL__ortest_1115z00_2298 =
																										(BgL_classz00_2281 ==
																										BgL_oclassz00_2297);
																									if (BgL__ortest_1115z00_2298)
																										{	/* Ast/object.scm 120 */
																											BgL_res1754z00_2314 =
																												BgL__ortest_1115z00_2298;
																										}
																									else
																										{	/* Ast/object.scm 120 */
																											long BgL_odepthz00_2299;

																											{	/* Ast/object.scm 120 */
																												obj_t
																													BgL_arg1804z00_2300;
																												BgL_arg1804z00_2300 =
																													(BgL_oclassz00_2297);
																												BgL_odepthz00_2299 =
																													BGL_CLASS_DEPTH
																													(BgL_arg1804z00_2300);
																											}
																											if (
																												(2L <
																													BgL_odepthz00_2299))
																												{	/* Ast/object.scm 120 */
																													obj_t
																														BgL_arg1802z00_2302;
																													{	/* Ast/object.scm 120 */
																														obj_t
																															BgL_arg1803z00_2303;
																														BgL_arg1803z00_2303
																															=
																															(BgL_oclassz00_2297);
																														BgL_arg1802z00_2302
																															=
																															BGL_CLASS_ANCESTORS_REF
																															(BgL_arg1803z00_2303,
																															2L);
																													}
																													BgL_res1754z00_2314 =
																														(BgL_arg1802z00_2302
																														==
																														BgL_classz00_2281);
																												}
																											else
																												{	/* Ast/object.scm 120 */
																													BgL_res1754z00_2314 =
																														((bool_t) 0);
																												}
																										}
																								}
																							}
																							BgL_test1828z00_2800 =
																								BgL_res1754z00_2314;
																						}
																				}
																			else
																				{	/* Ast/object.scm 120 */
																					BgL_test1828z00_2800 = ((bool_t) 0);
																				}
																		}
																		if (BgL_test1828z00_2800)
																			{	/* Ast/object.scm 120 */
																				BgL_test1821z00_2776 = ((bool_t) 1);
																			}
																		else
																			{	/* Ast/object.scm 120 */
																				obj_t BgL_classz00_2315;

																				BgL_classz00_2315 =
																					BGl_wclassz00zzobject_classz00;
																				if (BGL_OBJECTP(BgL_klassz00_1818))
																					{	/* Ast/object.scm 120 */
																						BgL_objectz00_bglt
																							BgL_arg1807z00_2317;
																						BgL_arg1807z00_2317 =
																							(BgL_objectz00_bglt)
																							(BgL_klassz00_1818);
																						if (BGL_CONDEXPAND_ISA_ARCH64())
																							{	/* Ast/object.scm 120 */
																								long BgL_idxz00_2323;

																								BgL_idxz00_2323 =
																									BGL_OBJECT_INHERITANCE_NUM
																									(BgL_arg1807z00_2317);
																								BgL_test1821z00_2776 =
																									(VECTOR_REF
																									(BGl_za2inheritancesza2z00zz__objectz00,
																										(BgL_idxz00_2323 + 2L)) ==
																									BgL_classz00_2315);
																							}
																						else
																							{	/* Ast/object.scm 120 */
																								bool_t BgL_res1755z00_2348;

																								{	/* Ast/object.scm 120 */
																									obj_t BgL_oclassz00_2331;

																									{	/* Ast/object.scm 120 */
																										obj_t BgL_arg1815z00_2339;
																										long BgL_arg1816z00_2340;

																										BgL_arg1815z00_2339 =
																											(BGl_za2classesza2z00zz__objectz00);
																										{	/* Ast/object.scm 120 */
																											long BgL_arg1817z00_2341;

																											BgL_arg1817z00_2341 =
																												BGL_OBJECT_CLASS_NUM
																												(BgL_arg1807z00_2317);
																											BgL_arg1816z00_2340 =
																												(BgL_arg1817z00_2341 -
																												OBJECT_TYPE);
																										}
																										BgL_oclassz00_2331 =
																											VECTOR_REF
																											(BgL_arg1815z00_2339,
																											BgL_arg1816z00_2340);
																									}
																									{	/* Ast/object.scm 120 */
																										bool_t
																											BgL__ortest_1115z00_2332;
																										BgL__ortest_1115z00_2332 =
																											(BgL_classz00_2315 ==
																											BgL_oclassz00_2331);
																										if (BgL__ortest_1115z00_2332)
																											{	/* Ast/object.scm 120 */
																												BgL_res1755z00_2348 =
																													BgL__ortest_1115z00_2332;
																											}
																										else
																											{	/* Ast/object.scm 120 */
																												long BgL_odepthz00_2333;

																												{	/* Ast/object.scm 120 */
																													obj_t
																														BgL_arg1804z00_2334;
																													BgL_arg1804z00_2334 =
																														(BgL_oclassz00_2331);
																													BgL_odepthz00_2333 =
																														BGL_CLASS_DEPTH
																														(BgL_arg1804z00_2334);
																												}
																												if (
																													(2L <
																														BgL_odepthz00_2333))
																													{	/* Ast/object.scm 120 */
																														obj_t
																															BgL_arg1802z00_2336;
																														{	/* Ast/object.scm 120 */
																															obj_t
																																BgL_arg1803z00_2337;
																															BgL_arg1803z00_2337
																																=
																																(BgL_oclassz00_2331);
																															BgL_arg1802z00_2336
																																=
																																BGL_CLASS_ANCESTORS_REF
																																(BgL_arg1803z00_2337,
																																2L);
																														}
																														BgL_res1755z00_2348
																															=
																															(BgL_arg1802z00_2336
																															==
																															BgL_classz00_2315);
																													}
																												else
																													{	/* Ast/object.scm 120 */
																														BgL_res1755z00_2348
																															= ((bool_t) 0);
																													}
																											}
																									}
																								}
																								BgL_test1821z00_2776 =
																									BgL_res1755z00_2348;
																							}
																					}
																				else
																					{	/* Ast/object.scm 120 */
																						BgL_test1821z00_2776 = ((bool_t) 0);
																					}
																			}
																	}
															}
															if (BgL_test1821z00_2776)
																{	/* Ast/object.scm 122 */
																	obj_t BgL_slotz00_1824;

																	{	/* Ast/object.scm 122 */
																		obj_t BgL_arg1514z00_1852;

																		BgL_arg1514z00_1852 =
																			CAR(((obj_t) BgL_slotsz00_1819));
																		BgL_slotz00_1824 =
																			BGl_findzd2classzd2slotz00zzobject_toolsz00
																			(((BgL_typez00_bglt) BgL_klassz00_1818),
																			BgL_arg1514z00_1852);
																	}
																	if (CBOOL(BgL_slotz00_1824))
																		{	/* Ast/object.scm 124 */
																			if (NULLP(CDR(
																						((obj_t) BgL_slotsz00_1819))))
																				{	/* Ast/object.scm 130 */
																					bool_t BgL_test1840z00_2855;

																					if (
																						(((BgL_slotz00_bglt) COBJECT(
																									((BgL_slotz00_bglt)
																										BgL_slotz00_1824)))->
																							BgL_readzd2onlyzf3z21))
																						{	/* Ast/object.scm 131 */
																							bool_t BgL_test1842z00_2859;

																							{	/* Ast/object.scm 131 */
																								obj_t BgL_arg1454z00_1842;

																								BgL_arg1454z00_1842 =
																									CAR(((obj_t) BgL_lz00_28));
																								BgL_test1842z00_2859 =
																									(BgL_arg1454z00_1842 ==
																									BGl___bigloo_wallow__z00zzast_objectz00);
																							}
																							if (BgL_test1842z00_2859)
																								{	/* Ast/object.scm 131 */
																									BgL_test1840z00_2855 =
																										((bool_t) 0);
																								}
																							else
																								{	/* Ast/object.scm 131 */
																									BgL_test1840z00_2855 =
																										((bool_t) 1);
																								}
																						}
																					else
																						{	/* Ast/object.scm 130 */
																							BgL_test1840z00_2855 =
																								((bool_t) 0);
																						}
																					if (BgL_test1840z00_2855)
																						{	/* Ast/object.scm 133 */
																							obj_t BgL_arg1408z00_1832;
																							obj_t BgL_arg1410z00_1833;

																							{	/* Ast/object.scm 133 */
																								obj_t BgL_arg1421z00_1834;

																								{	/* Ast/object.scm 133 */
																									obj_t BgL_pairz00_2356;

																									BgL_pairz00_2356 =
																										CDR(
																										((obj_t) BgL_l2z00_1809));
																									BgL_arg1421z00_1834 =
																										CAR(BgL_pairz00_2356);
																								}
																								{	/* Ast/object.scm 133 */
																									obj_t BgL_list1422z00_1835;

																									BgL_list1422z00_1835 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1421z00_1834, BNIL);
																									BgL_arg1408z00_1832 =
																										BGl_formatz00zz__r4_output_6_10_3z00
																										(BGl_string1765z00zzast_objectz00,
																										BgL_list1422z00_1835);
																								}
																							}
																							{	/* Ast/object.scm 134 */
																								obj_t BgL_arg1434z00_1836;

																								{	/* Ast/object.scm 134 */
																									obj_t BgL_arg1437z00_1837;
																									obj_t BgL_arg1448z00_1838;

																									BgL_arg1437z00_1837 =
																										CAR(
																										((obj_t) BgL_l2z00_1809));
																									{	/* Ast/object.scm 134 */
																										obj_t BgL_arg1453z00_1839;

																										{	/* Ast/object.scm 134 */
																											obj_t BgL_pairz00_2361;

																											BgL_pairz00_2361 =
																												CDR(
																												((obj_t)
																													BgL_l2z00_1809));
																											BgL_arg1453z00_1839 =
																												CAR(BgL_pairz00_2361);
																										}
																										BgL_arg1448z00_1838 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1453z00_1839,
																											BNIL);
																									}
																									BgL_arg1434z00_1836 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1437z00_1837,
																										BgL_arg1448z00_1838);
																								}
																								BgL_arg1410z00_1833 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(1), BgL_arg1434z00_1836);
																							}
																							BgL_auxz00_2775 =
																								((obj_t)
																								BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00
																								(BgL_arg1408z00_1832,
																									BgL_arg1410z00_1833,
																									BgL_locz00_32));
																						}
																					else
																						{	/* Ast/object.scm 130 */
																							BgL_auxz00_2775 =
																								BGl_makezd2fieldzd2setz12z12zzast_objectz00
																								(BgL_slotz00_1824,
																								BgL_nodez00_1817,
																								BgL_valz00_1811,
																								BgL_stackz00_31, BgL_locz00_32,
																								BgL_sitez00_33);
																						}
																				}
																			else
																				{	/* Ast/object.scm 137 */
																					obj_t BgL_nodez00_1843;

																					BgL_nodez00_1843 =
																						BGl_makezd2fieldzd2refz00zzast_objectz00
																						(BgL_slotz00_1824, BgL_nodez00_1817,
																						BgL_stackz00_31, BgL_locz00_32,
																						BgL_sitez00_33);
																					{	/* Ast/object.scm 138 */
																						obj_t BgL_arg1472z00_1844;
																						obj_t BgL_arg1473z00_1845;

																						BgL_arg1472z00_1844 =
																							(((BgL_slotz00_bglt) COBJECT(
																									((BgL_slotz00_bglt)
																										BgL_slotz00_1824)))->
																							BgL_typez00);
																						BgL_arg1473z00_1845 =
																							CDR(((obj_t) BgL_slotsz00_1819));
																						{
																							obj_t BgL_slotsz00_2887;
																							obj_t BgL_klassz00_2886;
																							obj_t BgL_nodez00_2885;

																							BgL_nodez00_2885 =
																								BgL_nodez00_1843;
																							BgL_klassz00_2886 =
																								BgL_arg1472z00_1844;
																							BgL_slotsz00_2887 =
																								BgL_arg1473z00_1845;
																							BgL_slotsz00_1819 =
																								BgL_slotsz00_2887;
																							BgL_klassz00_1818 =
																								BgL_klassz00_2886;
																							BgL_nodez00_1817 =
																								BgL_nodez00_2885;
																							goto
																								BgL_zc3z04anonymousza31362ze3z87_1820;
																						}
																					}
																				}
																		}
																	else
																		{	/* Ast/object.scm 127 */
																			obj_t BgL_arg1489z00_1847;

																			{	/* Ast/object.scm 127 */
																				obj_t BgL_arg1502z00_1848;
																				obj_t BgL_arg1509z00_1849;

																				BgL_arg1502z00_1848 =
																					(((BgL_typez00_bglt) COBJECT(
																							((BgL_typez00_bglt)
																								BgL_klassz00_1818)))->
																					BgL_idz00);
																				BgL_arg1509z00_1849 =
																					CAR(((obj_t) BgL_slotsz00_1819));
																				{	/* Ast/object.scm 126 */
																					obj_t BgL_list1510z00_1850;

																					{	/* Ast/object.scm 126 */
																						obj_t BgL_arg1513z00_1851;

																						BgL_arg1513z00_1851 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1509z00_1849, BNIL);
																						BgL_list1510z00_1850 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1502z00_1848,
																							BgL_arg1513z00_1851);
																					}
																					BgL_arg1489z00_1847 =
																						BGl_formatz00zz__r4_output_6_10_3z00
																						(BGl_string1766z00zzast_objectz00,
																						BgL_list1510z00_1850);
																				}
																			}
																			BgL_auxz00_2775 =
																				((obj_t)
																				BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00
																				(BgL_arg1489z00_1847, BgL_expz00_30,
																					BgL_locz00_32));
																		}
																}
															else
																{	/* Ast/object.scm 120 */
																	BgL_auxz00_2775 =
																		((obj_t)
																		BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00
																		(BGl_string1763z00zzast_objectz00,
																			BgL_expz00_30, BgL_locz00_32));
																}
														}
														return ((BgL_nodez00_bglt) BgL_auxz00_2775);
													}
												}
											}
										}
									}
								else
									{	/* Ast/object.scm 139 */
										bool_t BgL_test1843z00_2902;

										if (INTEGERP
											(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
											{	/* Ast/object.scm 139 */
												BgL_test1843z00_2902 =
													(
													(long)
													CINT
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
													== 0L);
											}
										else
											{	/* Ast/object.scm 139 */
												BgL_test1843z00_2902 =
													BGl_2zd3zd3zz__r4_numbers_6_5z00
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
													BINT(0L));
											}
										if (BgL_test1843z00_2902)
											{	/* Ast/object.scm 139 */
												return
													BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00
													(BGl_string1764z00zzast_objectz00, BgL_expz00_30,
													BgL_locz00_32);
											}
										else
											{	/* Ast/object.scm 139 */
												return BgL_varz00_1810;
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* &field-set->node */
	BgL_nodez00_bglt BGl_z62fieldzd2setzd2ze3nodez81zzast_objectz00(obj_t
		BgL_envz00_2515, obj_t BgL_lz00_2516, obj_t BgL_valz00_2517,
		obj_t BgL_expz00_2518, obj_t BgL_stackz00_2519, obj_t BgL_locz00_2520,
		obj_t BgL_sitez00_2521)
	{
		{	/* Ast/object.scm 110 */
			return
				BGl_fieldzd2setzd2ze3nodeze3zzast_objectz00(BgL_lz00_2516,
				BgL_valz00_2517, BgL_expz00_2518, BgL_stackz00_2519, BgL_locz00_2520,
				BgL_sitez00_2521);
		}

	}



/* make-field-ref */
	obj_t BGl_makezd2fieldzd2refz00zzast_objectz00(obj_t BgL_slotz00_34,
		obj_t BgL_objz00_35, obj_t BgL_stackz00_36, obj_t BgL_locz00_37,
		obj_t BgL_sitez00_38)
	{
		{	/* Ast/object.scm 147 */
			if (CBOOL(
					(((BgL_slotz00_bglt) COBJECT(
								((BgL_slotz00_bglt) BgL_slotz00_34)))->BgL_getterz00)))
				{	/* Ast/object.scm 149 */
					obj_t BgL_vnumz00_1859;

					BgL_vnumz00_1859 =
						(((BgL_slotz00_bglt) COBJECT(
								((BgL_slotz00_bglt) BgL_slotz00_34)))->BgL_virtualzd2numzd2);
					{	/* Ast/object.scm 149 */
						obj_t BgL_expz00_1860;

						{	/* Ast/object.scm 150 */
							obj_t BgL_arg1535z00_1861;
							obj_t BgL_arg1540z00_1862;

							{	/* Ast/object.scm 150 */
								obj_t BgL_arg1544z00_1863;

								{	/* Ast/object.scm 150 */
									obj_t BgL_arg1546z00_1864;

									BgL_arg1546z00_1864 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(2), BNIL);
									BgL_arg1544z00_1863 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BgL_arg1546z00_1864);
								}
								BgL_arg1535z00_1861 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(4), BgL_arg1544z00_1863);
							}
							{	/* Ast/object.scm 150 */
								obj_t BgL_arg1552z00_1865;

								BgL_arg1552z00_1865 = MAKE_YOUNG_PAIR(BgL_vnumz00_1859, BNIL);
								BgL_arg1540z00_1862 =
									MAKE_YOUNG_PAIR(BgL_objz00_35, BgL_arg1552z00_1865);
							}
							BgL_expz00_1860 =
								MAKE_YOUNG_PAIR(BgL_arg1535z00_1861, BgL_arg1540z00_1862);
						}
						{	/* Ast/object.scm 150 */

							return
								((obj_t)
								BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_expz00_1860,
									BgL_stackz00_36, BgL_locz00_37, BgL_sitez00_38));
						}
					}
				}
			else
				{	/* Ast/object.scm 152 */
					obj_t BgL_privz00_1866;

					{	/* Ast/object.scm 152 */
						obj_t BgL_arg1553z00_1867;

						BgL_arg1553z00_1867 =
							(((BgL_slotz00_bglt) COBJECT(
									((BgL_slotz00_bglt) BgL_slotz00_34)))->BgL_classzd2ownerzd2);
						BgL_privz00_1866 =
							BGl_makezd2classzd2refz00zzobject_toolsz00(
							((BgL_typez00_bglt) BgL_arg1553z00_1867),
							((BgL_slotz00_bglt) BgL_slotz00_34), BgL_objz00_35);
					}
					return
						BGl_privatezd2nodezd2zzast_privatez00(BgL_privz00_1866,
						BgL_stackz00_36, BgL_locz00_37, BgL_sitez00_38);
				}
		}

	}



/* make-field-set! */
	obj_t BGl_makezd2fieldzd2setz12z12zzast_objectz00(obj_t BgL_slotz00_39,
		obj_t BgL_objz00_40, BgL_nodez00_bglt BgL_valz00_41, obj_t BgL_stackz00_42,
		obj_t BgL_locz00_43, obj_t BgL_sitez00_44)
	{
		{	/* Ast/object.scm 160 */
			if (CBOOL(
					(((BgL_slotz00_bglt) COBJECT(
								((BgL_slotz00_bglt) BgL_slotz00_39)))->BgL_setterz00)))
				{	/* Ast/object.scm 163 */
					obj_t BgL_vnumz00_1869;

					BgL_vnumz00_1869 =
						(((BgL_slotz00_bglt) COBJECT(
								((BgL_slotz00_bglt) BgL_slotz00_39)))->BgL_virtualzd2numzd2);
					{	/* Ast/object.scm 163 */
						obj_t BgL_expz00_1870;

						{	/* Ast/object.scm 164 */
							obj_t BgL_arg1559z00_1871;
							obj_t BgL_arg1561z00_1872;

							{	/* Ast/object.scm 164 */
								obj_t BgL_arg1564z00_1873;

								{	/* Ast/object.scm 164 */
									obj_t BgL_arg1565z00_1874;

									BgL_arg1565z00_1874 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(2), BNIL);
									BgL_arg1564z00_1873 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(5), BgL_arg1565z00_1874);
								}
								BgL_arg1559z00_1871 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(4), BgL_arg1564z00_1873);
							}
							{	/* Ast/object.scm 164 */
								obj_t BgL_arg1571z00_1875;

								{	/* Ast/object.scm 164 */
									obj_t BgL_arg1573z00_1876;

									BgL_arg1573z00_1876 =
										MAKE_YOUNG_PAIR(((obj_t) BgL_valz00_41), BNIL);
									BgL_arg1571z00_1875 =
										MAKE_YOUNG_PAIR(BgL_vnumz00_1869, BgL_arg1573z00_1876);
								}
								BgL_arg1561z00_1872 =
									MAKE_YOUNG_PAIR(BgL_objz00_40, BgL_arg1571z00_1875);
							}
							BgL_expz00_1870 =
								MAKE_YOUNG_PAIR(BgL_arg1559z00_1871, BgL_arg1561z00_1872);
						}
						{	/* Ast/object.scm 164 */

							return
								((obj_t)
								BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_expz00_1870,
									BgL_stackz00_42, BgL_locz00_43, BgL_sitez00_44));
						}
					}
				}
			else
				{	/* Ast/object.scm 167 */
					obj_t BgL_privz00_1877;

					{	/* Ast/object.scm 167 */
						obj_t BgL_arg1575z00_1878;

						BgL_arg1575z00_1878 =
							(((BgL_slotz00_bglt) COBJECT(
									((BgL_slotz00_bglt) BgL_slotz00_39)))->BgL_classzd2ownerzd2);
						BgL_privz00_1877 =
							BGl_makezd2classzd2setz12z12zzobject_toolsz00(
							((BgL_typez00_bglt) BgL_arg1575z00_1878),
							((BgL_slotz00_bglt) BgL_slotz00_39), BgL_objz00_40,
							((obj_t) BgL_valz00_41));
					}
					return
						BGl_privatezd2nodezd2zzast_privatez00(BgL_privz00_1877,
						BgL_stackz00_42, BgL_locz00_43, BgL_sitez00_44);
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_objectz00(void)
	{
		{	/* Ast/object.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_objectz00(void)
	{
		{	/* Ast/object.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_objectz00(void)
	{
		{	/* Ast/object.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_objectz00(void)
	{
		{	/* Ast/object.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zztools_prognz00(301998274L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zztools_dssslz00(275867955L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zzobject_toolsz00(196511171L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zzast_buildz00(428035925L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zzast_pragmaz00(156774043L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zzast_labelsz00(129879154L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zzast_letz00(469204197L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zzast_exitz00(419066125L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zzast_appz00(449859288L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zzast_applyz00(277780830L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zzast_privatez00(135263837L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			BGl_modulezd2initializa7ationz75zzeffect_feffectz00(516374368L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
			return
				BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1767z00zzast_objectz00));
		}

	}

#ifdef __cplusplus
}
#endif
