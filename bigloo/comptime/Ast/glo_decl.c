/*===========================================================================*/
/*   (Ast/glo_decl.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/glo_decl.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_AST_GLOzd2DECLzd2_TYPE_DEFINITIONS
#define BGL_BgL_AST_GLOzd2DECLzd2_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_scnstz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_nodez00;
		obj_t BgL_classz00;
		obj_t BgL_locz00;
	}               *BgL_scnstz00_bglt;

	typedef struct BgL_cvarz00_bgl
	{
		header_t header;
		obj_t widening;
		bool_t BgL_macrozf3zf3;
	}              *BgL_cvarz00_bglt;


#endif													// BGL_BgL_AST_GLOzd2DECLzd2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_symbol1768z00zzast_glozd2declzd2 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zzast_glozd2declzd2 = BUNSPEC;
	extern obj_t BGl_dssslzd2keyszd2zztools_dssslz00(obj_t);
	extern obj_t BGl_za2pairzd2nilza2zd2zztype_cachez00;
	extern obj_t BGl_findzd2locationzf2locz20zztools_locationz00(obj_t, obj_t);
	extern obj_t BGl_sfunz00zzast_varz00;
	extern obj_t BGl_fastzd2idzd2ofzd2idzd2zzast_identz00(obj_t, obj_t);
	extern obj_t BGl_parsezd2idzf2importzd2locationzf2zzast_identz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_dssslzd2formalszd2zztools_dssslz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern BgL_globalz00_bglt BGl_bindzd2globalz12zc0zzast_envz00(obj_t, obj_t,
		obj_t, BgL_valuez00_bglt, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzast_glozd2declzd2(void);
	static obj_t BGl_objectzd2initzd2zzast_glozd2declzd2(void);
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_declarezd2globalzd2scnstz12z12zzast_glozd2declzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	static BgL_globalz00_bglt
		BGl_z62declarezd2globalzd2cfunz12z70zzast_glozd2declzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	extern bool_t BGl_dssslzd2keyzd2onlyzd2prototypezf3z21zztools_dssslz00(obj_t);
	BGL_IMPORT obj_t BGl_dropz00zz__r4_pairs_and_lists_6_3z00(obj_t, long);
	static BgL_globalz00_bglt
		BGl_z62declarezd2globalzd2sfunz12z70zzast_glozd2declzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_declarezd2globalzd2cfunz12z12zzast_glozd2declzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, bool_t, bool_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_declarezd2globalzd2sfunz12z12zzast_glozd2declzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzast_glozd2declzd2(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzast_glozd2declzd2(void);
	static BgL_globalz00_bglt
		BGl_z62declarezd2globalzd2scnstz12z70zzast_glozd2declzd2(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	extern obj_t BGl_za2bdbzd2debugza2zd2zzengine_paramz00;
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	extern obj_t BGl_svarz00zzast_varz00;
	extern bool_t
		BGl_dssslzd2optionalzd2onlyzd2prototypezf3z21zztools_dssslz00(obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	extern BgL_typez00_bglt
		BGl_typezd2ofzd2idzf2importzd2locationz20zzast_identz00(obj_t, obj_t,
		obj_t);
	static BgL_globalz00_bglt
		BGl_declarezd2globalzd2nooptzd2sfunz12zc0zzast_glozd2declzd2(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzast_glozd2declzd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_dssslz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_argsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__dssslz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	extern obj_t BGl_cfunz00zzast_varz00;
	static BgL_globalz00_bglt
		BGl_z62declarezd2globalzd2cvarz12z70zzast_glozd2declzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_globalz00_bglt
		BGl_z62declarezd2globalzd2svarz12z70zzast_glozd2declzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzast_glozd2declzd2(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_glozd2declzd2(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	extern long BGl_globalzd2arityzd2zztools_argsz00(obj_t);
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_declarezd2globalzd2cvarz12z12zzast_glozd2declzd2(obj_t, obj_t, obj_t,
		obj_t, bool_t, obj_t, obj_t);
	extern obj_t BGl_scnstz00zzast_varz00;
	static obj_t BGl_importedzd2moduleszd2initz00zzast_glozd2declzd2(void);
	static BgL_globalz00_bglt
		BGl_declarezd2globalzd2dssslzd2sfunz12zc0zzast_glozd2declzd2(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_dssslzd2optionalszd2zztools_dssslz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzast_glozd2declzd2(void);
	extern obj_t BGl_parsezd2idzd2zzast_identz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_declarezd2globalzd2svarz12z12zzast_glozd2declzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern BgL_typez00_bglt
		BGl_usezd2foreignzd2typezf2importzd2locz12z32zztype_envz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	extern obj_t BGl_argsza2zd2ze3argszd2listz41zztools_argsz00(obj_t);
	static obj_t BGl_loopze70ze7zzast_glozd2declzd2(obj_t, obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_cvarz00zzast_varz00;
	BGL_IMPORT bool_t BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(obj_t);
	static obj_t __cnst[11];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_declarezd2globalzd2scnstz12zd2envzc0zzast_glozd2declzd2,
		BgL_bgl_za762declareza7d2glo1776z00,
		BGl_z62declarezd2globalzd2scnstz12z70zzast_glozd2declzd2, 0L, BUNSPEC, 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_declarezd2globalzd2cfunz12zd2envzc0zzast_glozd2declzd2,
		BgL_bgl_za762declareza7d2glo1777z00,
		BGl_z62declarezd2globalzd2cfunz12z70zzast_glozd2declzd2, 0L, BUNSPEC, 10);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_declarezd2globalzd2sfunz12zd2envzc0zzast_glozd2declzd2,
		BgL_bgl_za762declareza7d2glo1778z00,
		BGl_z62declarezd2globalzd2sfunz12z70zzast_glozd2declzd2, 0L, BUNSPEC, 8);
	      DEFINE_STRING(BGl_string1769z00zzast_glozd2declzd2,
		BgL_bgl_string1769za700za7za7a1779za7, "", 0);
	      DEFINE_STRING(BGl_string1770z00zzast_glozd2declzd2,
		BgL_bgl_string1770za700za7za7a1780za7, "Illegal nary argument type", 26);
	      DEFINE_STRING(BGl_string1771z00zzast_glozd2declzd2,
		BgL_bgl_string1771za700za7za7a1781za7, "Illegal type for global variable",
		32);
	      DEFINE_STRING(BGl_string1772z00zzast_glozd2declzd2,
		BgL_bgl_string1772za700za7za7a1782za7, "ast_glo-decl", 12);
	      DEFINE_STRING(BGl_string1773z00zzast_glozd2declzd2,
		BgL_bgl_string1773za700za7za7a1783za7,
		"foreign a-cnst write read (export import) bigloo sgfun \077\077? export bdb static ",
		77);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_declarezd2globalzd2cvarz12zd2envzc0zzast_glozd2declzd2,
		BgL_bgl_za762declareza7d2glo1784z00,
		BGl_z62declarezd2globalzd2cvarz12z70zzast_glozd2declzd2, 0L, BUNSPEC, 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_declarezd2globalzd2svarz12zd2envzc0zzast_glozd2declzd2,
		BgL_bgl_za762declareza7d2glo1785z00,
		BGl_z62declarezd2globalzd2svarz12z70zzast_glozd2declzd2, 0L, BUNSPEC, 6);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_symbol1768z00zzast_glozd2declzd2));
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzast_glozd2declzd2));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzast_glozd2declzd2(long
		BgL_checksumz00_2111, char *BgL_fromz00_2112)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_glozd2declzd2))
				{
					BGl_requirezd2initializa7ationz75zzast_glozd2declzd2 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_glozd2declzd2();
					BGl_libraryzd2moduleszd2initz00zzast_glozd2declzd2();
					BGl_cnstzd2initzd2zzast_glozd2declzd2();
					BGl_importedzd2moduleszd2initz00zzast_glozd2declzd2();
					return BGl_methodzd2initzd2zzast_glozd2declzd2();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_glozd2declzd2(void)
	{
		{	/* Ast/glo_decl.scm 17 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_glo-decl");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_glo-decl");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"ast_glo-decl");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "ast_glo-decl");
			BGl_modulezd2initializa7ationz75zz__dssslz00(0L, "ast_glo-decl");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "ast_glo-decl");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "ast_glo-decl");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"ast_glo-decl");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_glozd2declzd2(void)
	{
		{	/* Ast/glo_decl.scm 17 */
			{	/* Ast/glo_decl.scm 17 */
				obj_t BgL_cportz00_2099;

				{	/* Ast/glo_decl.scm 17 */
					obj_t BgL_stringz00_2106;

					BgL_stringz00_2106 = BGl_string1773z00zzast_glozd2declzd2;
					{	/* Ast/glo_decl.scm 17 */
						obj_t BgL_startz00_2107;

						BgL_startz00_2107 = BINT(0L);
						{	/* Ast/glo_decl.scm 17 */
							obj_t BgL_endz00_2108;

							BgL_endz00_2108 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2106)));
							{	/* Ast/glo_decl.scm 17 */

								BgL_cportz00_2099 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2106, BgL_startz00_2107, BgL_endz00_2108);
				}}}}
				{
					long BgL_iz00_2100;

					BgL_iz00_2100 = 10L;
				BgL_loopz00_2101:
					if ((BgL_iz00_2100 == -1L))
						{	/* Ast/glo_decl.scm 17 */
							BUNSPEC;
						}
					else
						{	/* Ast/glo_decl.scm 17 */
							{	/* Ast/glo_decl.scm 17 */
								obj_t BgL_arg1775z00_2102;

								{	/* Ast/glo_decl.scm 17 */

									{	/* Ast/glo_decl.scm 17 */
										obj_t BgL_locationz00_2104;

										BgL_locationz00_2104 = BBOOL(((bool_t) 0));
										{	/* Ast/glo_decl.scm 17 */

											BgL_arg1775z00_2102 =
												BGl_readz00zz__readerz00(BgL_cportz00_2099,
												BgL_locationz00_2104);
										}
									}
								}
								{	/* Ast/glo_decl.scm 17 */
									int BgL_tmpz00_2138;

									BgL_tmpz00_2138 = (int) (BgL_iz00_2100);
									CNST_TABLE_SET(BgL_tmpz00_2138, BgL_arg1775z00_2102);
							}}
							{	/* Ast/glo_decl.scm 17 */
								int BgL_auxz00_2105;

								BgL_auxz00_2105 = (int) ((BgL_iz00_2100 - 1L));
								{
									long BgL_iz00_2143;

									BgL_iz00_2143 = (long) (BgL_auxz00_2105);
									BgL_iz00_2100 = BgL_iz00_2143;
									goto BgL_loopz00_2101;
								}
							}
						}
				}
			}
			return (BGl_symbol1768z00zzast_glozd2declzd2 =
				bstring_to_symbol(BGl_string1769z00zzast_glozd2declzd2), BUNSPEC);
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_glozd2declzd2(void)
	{
		{	/* Ast/glo_decl.scm 17 */
			return bgl_gc_roots_register();
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzast_glozd2declzd2(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1522;

				BgL_headz00_1522 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1523;
					obj_t BgL_tailz00_1524;

					BgL_prevz00_1523 = BgL_headz00_1522;
					BgL_tailz00_1524 = BgL_l1z00_1;
				BgL_loopz00_1525:
					if (PAIRP(BgL_tailz00_1524))
						{
							obj_t BgL_newzd2prevzd2_1527;

							BgL_newzd2prevzd2_1527 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1524), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1523, BgL_newzd2prevzd2_1527);
							{
								obj_t BgL_tailz00_2154;
								obj_t BgL_prevz00_2153;

								BgL_prevz00_2153 = BgL_newzd2prevzd2_1527;
								BgL_tailz00_2154 = CDR(BgL_tailz00_1524);
								BgL_tailz00_1524 = BgL_tailz00_2154;
								BgL_prevz00_1523 = BgL_prevz00_2153;
								goto BgL_loopz00_1525;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1522);
				}
			}
		}

	}



/* declare-global-sfun! */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_declarezd2globalzd2sfunz12z12zzast_glozd2declzd2(obj_t BgL_idz00_3,
		obj_t BgL_aliasz00_4, obj_t BgL_argsz00_5, obj_t BgL_modulez00_6,
		obj_t BgL_importz00_7, obj_t BgL_classz00_8, obj_t BgL_srcez00_9,
		obj_t BgL_srciz00_10)
	{
		{	/* Ast/glo_decl.scm 56 */
			if (BGl_dssslzd2optionalzd2onlyzd2prototypezf3z21zztools_dssslz00
				(BgL_argsz00_5))
				{	/* Ast/glo_decl.scm 60 */
					return
						BGl_declarezd2globalzd2dssslzd2sfunz12zc0zzast_glozd2declzd2
						(BGl_dssslzd2optionalszd2zztools_dssslz00(BgL_argsz00_5), BNIL,
						BgL_idz00_3, BgL_aliasz00_4, BgL_argsz00_5, BgL_modulez00_6,
						BgL_importz00_7, BgL_classz00_8, BgL_srcez00_9, BgL_srciz00_10);
				}
			else
				{	/* Ast/glo_decl.scm 60 */
					if (BGl_dssslzd2keyzd2onlyzd2prototypezf3z21zztools_dssslz00
						(BgL_argsz00_5))
						{	/* Ast/glo_decl.scm 62 */
							return
								BGl_declarezd2globalzd2dssslzd2sfunz12zc0zzast_glozd2declzd2
								(BNIL, BGl_dssslzd2keyszd2zztools_dssslz00(BgL_argsz00_5),
								BgL_idz00_3, BgL_aliasz00_4, BgL_argsz00_5, BgL_modulez00_6,
								BgL_importz00_7, BgL_classz00_8, BgL_srcez00_9, BgL_srciz00_10);
						}
					else
						{	/* Ast/glo_decl.scm 62 */
							BGL_TAIL return
								BGl_declarezd2globalzd2nooptzd2sfunz12zc0zzast_glozd2declzd2
								(BgL_idz00_3, BgL_aliasz00_4, BgL_argsz00_5, BgL_modulez00_6,
								BgL_importz00_7, BgL_classz00_8, BgL_srcez00_9, BgL_srciz00_10);
						}
				}
		}

	}



/* &declare-global-sfun! */
	BgL_globalz00_bglt
		BGl_z62declarezd2globalzd2sfunz12z70zzast_glozd2declzd2(obj_t
		BgL_envz00_2055, obj_t BgL_idz00_2056, obj_t BgL_aliasz00_2057,
		obj_t BgL_argsz00_2058, obj_t BgL_modulez00_2059, obj_t BgL_importz00_2060,
		obj_t BgL_classz00_2061, obj_t BgL_srcez00_2062, obj_t BgL_srciz00_2063)
	{
		{	/* Ast/glo_decl.scm 56 */
			return
				BGl_declarezd2globalzd2sfunz12z12zzast_glozd2declzd2(BgL_idz00_2056,
				BgL_aliasz00_2057, BgL_argsz00_2058, BgL_modulez00_2059,
				BgL_importz00_2060, BgL_classz00_2061, BgL_srcez00_2062,
				BgL_srciz00_2063);
		}

	}



/* declare-global-dsssl-sfun! */
	BgL_globalz00_bglt
		BGl_declarezd2globalzd2dssslzd2sfunz12zc0zzast_glozd2declzd2(obj_t
		BgL_optsz00_11, obj_t BgL_keysz00_12, obj_t BgL_idz00_13,
		obj_t BgL_aliasz00_14, obj_t BgL_argsz00_15, obj_t BgL_modulez00_16,
		obj_t BgL_importz00_17, obj_t BgL_classz00_18, obj_t BgL_srcez00_19,
		obj_t BgL_srciz00_20)
	{
		{	/* Ast/glo_decl.scm 70 */
			{	/* Ast/glo_decl.scm 73 */
				long BgL_arityz00_1532;

				BgL_arityz00_1532 =
					BGl_globalzd2arityzd2zztools_argsz00(BgL_argsz00_15);
				{	/* Ast/glo_decl.scm 73 */
					bool_t BgL_exportzf3zf3_1533;

					{	/* Ast/glo_decl.scm 74 */
						bool_t BgL__ortest_1108z00_1617;

						if ((BgL_importz00_17 == CNST_TABLE_REF(0)))
							{	/* Ast/glo_decl.scm 74 */
								BgL__ortest_1108z00_1617 = ((bool_t) 0);
							}
						else
							{	/* Ast/glo_decl.scm 74 */
								BgL__ortest_1108z00_1617 = ((bool_t) 1);
							}
						if (BgL__ortest_1108z00_1617)
							{	/* Ast/glo_decl.scm 74 */
								BgL_exportzf3zf3_1533 = BgL__ortest_1108z00_1617;
							}
						else
							{	/* Ast/glo_decl.scm 75 */
								obj_t BgL__andtest_1109z00_1618;

								{	/* Ast/glo_decl.scm 75 */
									obj_t BgL_arg1375z00_1619;

									{	/* Ast/glo_decl.scm 75 */
										obj_t BgL_arg1376z00_1620;

										BgL_arg1376z00_1620 =
											BGl_thezd2backendzd2zzbackend_backendz00();
										BgL_arg1375z00_1619 =
											(((BgL_backendz00_bglt) COBJECT(
													((BgL_backendz00_bglt) BgL_arg1376z00_1620)))->
											BgL_debugzd2supportzd2);
									}
									BgL__andtest_1109z00_1618 =
										BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CNST_TABLE_REF(1),
										BgL_arg1375z00_1619);
								}
								if (CBOOL(BgL__andtest_1109z00_1618))
									{	/* Ast/glo_decl.scm 75 */
										BgL_exportzf3zf3_1533 =
											(
											(long) CINT(BGl_za2bdbzd2debugza2zd2zzengine_paramz00) >=
											3L);
									}
								else
									{	/* Ast/glo_decl.scm 75 */
										BgL_exportzf3zf3_1533 = ((bool_t) 0);
									}
							}
					}
					{	/* Ast/glo_decl.scm 74 */
						obj_t BgL_importz00_1534;

						{	/* Ast/glo_decl.scm 77 */
							bool_t BgL_test1794z00_2181;

							if ((BgL_importz00_17 == CNST_TABLE_REF(0)))
								{	/* Ast/glo_decl.scm 78 */
									bool_t BgL_test1796z00_2185;

									{	/* Ast/glo_decl.scm 78 */
										obj_t BgL_arg1370z00_1615;

										{	/* Ast/glo_decl.scm 78 */
											obj_t BgL_arg1371z00_1616;

											BgL_arg1371z00_1616 =
												BGl_thezd2backendzd2zzbackend_backendz00();
											BgL_arg1370z00_1615 =
												(((BgL_backendz00_bglt) COBJECT(
														((BgL_backendz00_bglt) BgL_arg1371z00_1616)))->
												BgL_debugzd2supportzd2);
										}
										BgL_test1796z00_2185 =
											CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
											(CNST_TABLE_REF(1), BgL_arg1370z00_1615));
									}
									if (BgL_test1796z00_2185)
										{	/* Ast/glo_decl.scm 78 */
											BgL_test1794z00_2181 =
												(
												(long) CINT(BGl_za2bdbzd2debugza2zd2zzengine_paramz00)
												>= 3L);
										}
									else
										{	/* Ast/glo_decl.scm 78 */
											BgL_test1794z00_2181 = ((bool_t) 0);
										}
								}
							else
								{	/* Ast/glo_decl.scm 77 */
									BgL_test1794z00_2181 = ((bool_t) 0);
								}
							if (BgL_test1794z00_2181)
								{	/* Ast/glo_decl.scm 77 */
									BgL_importz00_1534 = CNST_TABLE_REF(2);
								}
							else
								{	/* Ast/glo_decl.scm 77 */
									BgL_importz00_1534 = BgL_importz00_17;
								}
						}
						{	/* Ast/glo_decl.scm 77 */
							obj_t BgL_locz00_1535;

							BgL_locz00_1535 =
								BGl_findzd2locationzd2zztools_locationz00(BgL_srcez00_19);
							{	/* Ast/glo_decl.scm 82 */
								obj_t BgL_lociz00_1536;

								BgL_lociz00_1536 =
									BGl_findzd2locationzf2locz20zztools_locationz00
									(BgL_srciz00_20, BgL_locz00_1535);
								{	/* Ast/glo_decl.scm 83 */
									obj_t BgL_argsz00_1537;

									if (NULLP(BgL_keysz00_12))
										{	/* Ast/glo_decl.scm 84 */
											BgL_argsz00_1537 = BgL_argsz00_15;
										}
									else
										{	/* Ast/glo_decl.scm 84 */
											BgL_argsz00_1537 =
												BGl_loopze70ze7zzast_glozd2declzd2(BgL_keysz00_12,
												BgL_argsz00_15);
										}
									{	/* Ast/glo_decl.scm 84 */
										obj_t BgL_argszd2typezd2_1538;

										{
											obj_t BgL_argsz00_1572;
											obj_t BgL_resz00_1573;

											BgL_argsz00_1572 = BgL_argsz00_1537;
											BgL_resz00_1573 = BNIL;
										BgL_zc3z04anonymousza31315ze3z87_1574:
											if (NULLP(BgL_argsz00_1572))
												{	/* Ast/glo_decl.scm 102 */
													BgL_argszd2typezd2_1538 =
														bgl_reverse_bang(BgL_resz00_1573);
												}
											else
												{	/* Ast/glo_decl.scm 104 */
													bool_t BgL_test1801z00_2203;

													{	/* Ast/glo_decl.scm 104 */
														obj_t BgL_arg1326z00_1588;

														BgL_arg1326z00_1588 =
															CAR(((obj_t) BgL_argsz00_1572));
														BgL_test1801z00_2203 =
															BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00
															(BgL_arg1326z00_1588);
													}
													if (BgL_test1801z00_2203)
														{	/* Ast/glo_decl.scm 105 */
															obj_t BgL_arg1319z00_1578;

															BgL_arg1319z00_1578 =
																CDR(((obj_t) BgL_argsz00_1572));
															{
																obj_t BgL_argsz00_2209;

																BgL_argsz00_2209 = BgL_arg1319z00_1578;
																BgL_argsz00_1572 = BgL_argsz00_2209;
																goto BgL_zc3z04anonymousza31315ze3z87_1574;
															}
														}
													else
														{	/* Ast/glo_decl.scm 107 */
															obj_t BgL_az00_1579;

															{	/* Ast/glo_decl.scm 107 */
																bool_t BgL_test1802z00_2210;

																{	/* Ast/glo_decl.scm 107 */
																	obj_t BgL_tmpz00_2211;

																	BgL_tmpz00_2211 =
																		CAR(((obj_t) BgL_argsz00_1572));
																	BgL_test1802z00_2210 =
																		SYMBOLP(BgL_tmpz00_2211);
																}
																if (BgL_test1802z00_2210)
																	{	/* Ast/glo_decl.scm 107 */
																		BgL_az00_1579 =
																			CAR(((obj_t) BgL_argsz00_1572));
																	}
																else
																	{	/* Ast/glo_decl.scm 109 */
																		obj_t BgL_pairz00_1958;

																		BgL_pairz00_1958 =
																			CAR(((obj_t) BgL_argsz00_1572));
																		BgL_az00_1579 = CAR(BgL_pairz00_1958);
																	}
															}
															{	/* Ast/glo_decl.scm 107 */
																obj_t BgL_tyz00_1580;

																{	/* Ast/glo_decl.scm 110 */
																	BgL_typez00_bglt BgL_tz00_1583;

																	BgL_tz00_1583 =
																		BGl_typezd2ofzd2idzf2importzd2locationz20zzast_identz00
																		(BgL_az00_1579, BgL_locz00_1535,
																		BgL_lociz00_1536);
																	{	/* Ast/glo_decl.scm 112 */
																		bool_t BgL_test1803z00_2221;

																		if (
																			(((obj_t) BgL_tz00_1583) ==
																				BGl_za2_za2z00zztype_cachez00))
																			{	/* Ast/glo_decl.scm 112 */
																				BgL_test1803z00_2221 =
																					BgL_exportzf3zf3_1533;
																			}
																		else
																			{	/* Ast/glo_decl.scm 112 */
																				BgL_test1803z00_2221 = ((bool_t) 0);
																			}
																		if (BgL_test1803z00_2221)
																			{	/* Ast/glo_decl.scm 112 */
																				BgL_tyz00_1580 =
																					BGl_za2objza2z00zztype_cachez00;
																			}
																		else
																			{	/* Ast/glo_decl.scm 112 */
																				BgL_tyz00_1580 =
																					((obj_t) BgL_tz00_1583);
																			}
																	}
																}
																{	/* Ast/glo_decl.scm 110 */

																	{	/* Ast/glo_decl.scm 115 */
																		obj_t BgL_arg1320z00_1581;
																		obj_t BgL_arg1321z00_1582;

																		BgL_arg1320z00_1581 =
																			CDR(((obj_t) BgL_argsz00_1572));
																		BgL_arg1321z00_1582 =
																			MAKE_YOUNG_PAIR(BgL_tyz00_1580,
																			BgL_resz00_1573);
																		{
																			obj_t BgL_resz00_2230;
																			obj_t BgL_argsz00_2229;

																			BgL_argsz00_2229 = BgL_arg1320z00_1581;
																			BgL_resz00_2230 = BgL_arg1321z00_1582;
																			BgL_resz00_1573 = BgL_resz00_2230;
																			BgL_argsz00_1572 = BgL_argsz00_2229;
																			goto
																				BgL_zc3z04anonymousza31315ze3z87_1574;
																		}
																	}
																}
															}
														}
												}
										}
										{	/* Ast/glo_decl.scm 99 */
											obj_t BgL_argszd2namezd2_1539;

											{
												obj_t BgL_argsz00_1552;
												obj_t BgL_resz00_1553;

												BgL_argsz00_1552 = BgL_argsz00_1537;
												BgL_resz00_1553 = BNIL;
											BgL_zc3z04anonymousza31285ze3z87_1554:
												if (NULLP(BgL_argsz00_1552))
													{	/* Ast/glo_decl.scm 119 */
														BgL_argszd2namezd2_1539 =
															bgl_reverse_bang(BgL_resz00_1553);
													}
												else
													{	/* Ast/glo_decl.scm 121 */
														bool_t BgL_test1809z00_2234;

														{	/* Ast/glo_decl.scm 121 */
															obj_t BgL_arg1314z00_1568;

															BgL_arg1314z00_1568 =
																CAR(((obj_t) BgL_argsz00_1552));
															BgL_test1809z00_2234 =
																BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00
																(BgL_arg1314z00_1568);
														}
														if (BgL_test1809z00_2234)
															{	/* Ast/glo_decl.scm 122 */
																obj_t BgL_arg1306z00_1558;

																BgL_arg1306z00_1558 =
																	CDR(((obj_t) BgL_argsz00_1552));
																{
																	obj_t BgL_argsz00_2240;

																	BgL_argsz00_2240 = BgL_arg1306z00_1558;
																	BgL_argsz00_1552 = BgL_argsz00_2240;
																	goto BgL_zc3z04anonymousza31285ze3z87_1554;
																}
															}
														else
															{	/* Ast/glo_decl.scm 124 */
																obj_t BgL_az00_1559;

																{	/* Ast/glo_decl.scm 124 */
																	bool_t BgL_test1810z00_2241;

																	{	/* Ast/glo_decl.scm 124 */
																		obj_t BgL_tmpz00_2242;

																		BgL_tmpz00_2242 =
																			CAR(((obj_t) BgL_argsz00_1552));
																		BgL_test1810z00_2241 =
																			SYMBOLP(BgL_tmpz00_2242);
																	}
																	if (BgL_test1810z00_2241)
																		{	/* Ast/glo_decl.scm 124 */
																			BgL_az00_1559 =
																				CAR(((obj_t) BgL_argsz00_1552));
																		}
																	else
																		{	/* Ast/glo_decl.scm 126 */
																			obj_t BgL_pairz00_1967;

																			BgL_pairz00_1967 =
																				CAR(((obj_t) BgL_argsz00_1552));
																			BgL_az00_1559 = CAR(BgL_pairz00_1967);
																		}
																}
																{	/* Ast/glo_decl.scm 124 */
																	obj_t BgL_vidz00_1560;

																	BgL_vidz00_1560 =
																		BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																		(BgL_az00_1559, BgL_locz00_1535);
																	{	/* Ast/glo_decl.scm 127 */
																		obj_t BgL_varz00_1561;

																		if (
																			(BgL_vidz00_1560 ==
																				BGl_symbol1768z00zzast_glozd2declzd2))
																			{	/* Ast/glo_decl.scm 129 */

																				{	/* Ast/glo_decl.scm 129 */

																					BgL_varz00_1561 =
																						BGl_gensymz00zz__r4_symbols_6_4z00
																						(BFALSE);
																				}
																			}
																		else
																			{	/* Ast/glo_decl.scm 128 */
																				BgL_varz00_1561 = BgL_vidz00_1560;
																			}
																		{	/* Ast/glo_decl.scm 128 */

																			{	/* Ast/glo_decl.scm 131 */
																				obj_t BgL_arg1307z00_1562;
																				obj_t BgL_arg1308z00_1563;

																				BgL_arg1307z00_1562 =
																					CDR(((obj_t) BgL_argsz00_1552));
																				BgL_arg1308z00_1563 =
																					MAKE_YOUNG_PAIR(BgL_varz00_1561,
																					BgL_resz00_1553);
																				{
																					obj_t BgL_resz00_2259;
																					obj_t BgL_argsz00_2258;

																					BgL_argsz00_2258 =
																						BgL_arg1307z00_1562;
																					BgL_resz00_2259 = BgL_arg1308z00_1563;
																					BgL_resz00_1553 = BgL_resz00_2259;
																					BgL_argsz00_1552 = BgL_argsz00_2258;
																					goto
																						BgL_zc3z04anonymousza31285ze3z87_1554;
																				}
																			}
																		}
																	}
																}
															}
													}
											}
											{	/* Ast/glo_decl.scm 116 */
												obj_t BgL_idzd2typezd2_1540;

												BgL_idzd2typezd2_1540 =
													BGl_parsezd2idzf2importzd2locationzf2zzast_identz00
													(BgL_idz00_13, BgL_locz00_1535, BgL_lociz00_1536);
												{	/* Ast/glo_decl.scm 132 */
													obj_t BgL_typezd2reszd2_1541;

													BgL_typezd2reszd2_1541 = CDR(BgL_idzd2typezd2_1540);
													{	/* Ast/glo_decl.scm 133 */
														obj_t BgL_idz00_1542;

														BgL_idz00_1542 = CAR(BgL_idzd2typezd2_1540);
														{	/* Ast/glo_decl.scm 134 */
															BgL_sfunz00_bglt BgL_sfunz00_1543;

															{	/* Ast/glo_decl.scm 135 */
																BgL_sfunz00_bglt BgL_new1114z00_1547;

																{	/* Ast/glo_decl.scm 136 */
																	BgL_sfunz00_bglt BgL_new1113z00_1548;

																	BgL_new1113z00_1548 =
																		((BgL_sfunz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_sfunz00_bgl))));
																	{	/* Ast/glo_decl.scm 136 */
																		long BgL_arg1284z00_1549;

																		BgL_arg1284z00_1549 =
																			BGL_CLASS_NUM(BGl_sfunz00zzast_varz00);
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1113z00_1548),
																			BgL_arg1284z00_1549);
																	}
																	{	/* Ast/glo_decl.scm 136 */
																		BgL_objectz00_bglt BgL_tmpz00_2267;

																		BgL_tmpz00_2267 =
																			((BgL_objectz00_bglt)
																			BgL_new1113z00_1548);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2267,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1113z00_1548);
																	BgL_new1114z00_1547 = BgL_new1113z00_1548;
																}
																((((BgL_funz00_bglt) COBJECT(
																				((BgL_funz00_bglt)
																					BgL_new1114z00_1547)))->
																		BgL_arityz00) =
																	((long) BgL_arityz00_1532), BUNSPEC);
																((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																					BgL_new1114z00_1547)))->
																		BgL_sidezd2effectzd2) =
																	((obj_t) BUNSPEC), BUNSPEC);
																((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																					BgL_new1114z00_1547)))->
																		BgL_predicatezd2ofzd2) =
																	((obj_t) BFALSE), BUNSPEC);
																((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																					BgL_new1114z00_1547)))->
																		BgL_stackzd2allocatorzd2) =
																	((obj_t) BFALSE), BUNSPEC);
																((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																					BgL_new1114z00_1547)))->
																		BgL_topzf3zf3) =
																	((bool_t) ((bool_t) 1)), BUNSPEC);
																((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																					BgL_new1114z00_1547)))->
																		BgL_thezd2closurezd2) =
																	((obj_t) BUNSPEC), BUNSPEC);
																((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																					BgL_new1114z00_1547)))->
																		BgL_effectz00) =
																	((obj_t) BUNSPEC), BUNSPEC);
																((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																					BgL_new1114z00_1547)))->
																		BgL_failsafez00) =
																	((obj_t) BUNSPEC), BUNSPEC);
																((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																					BgL_new1114z00_1547)))->
																		BgL_argszd2noescapezd2) =
																	((obj_t) BNIL), BUNSPEC);
																((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																					BgL_new1114z00_1547)))->
																		BgL_argszd2retescapezd2) =
																	((obj_t) BNIL), BUNSPEC);
																((((BgL_sfunz00_bglt)
																			COBJECT(BgL_new1114z00_1547))->
																		BgL_propertyz00) = ((obj_t) BNIL), BUNSPEC);
																((((BgL_sfunz00_bglt)
																			COBJECT(BgL_new1114z00_1547))->
																		BgL_argsz00) =
																	((obj_t) BgL_argszd2typezd2_1538), BUNSPEC);
																((((BgL_sfunz00_bglt)
																			COBJECT(BgL_new1114z00_1547))->
																		BgL_argszd2namezd2) =
																	((obj_t) BgL_argszd2namezd2_1539), BUNSPEC);
																((((BgL_sfunz00_bglt)
																			COBJECT(BgL_new1114z00_1547))->
																		BgL_bodyz00) = ((obj_t) BUNSPEC), BUNSPEC);
																((((BgL_sfunz00_bglt)
																			COBJECT(BgL_new1114z00_1547))->
																		BgL_classz00) =
																	((obj_t) BgL_classz00_18), BUNSPEC);
																((((BgL_sfunz00_bglt)
																			COBJECT(BgL_new1114z00_1547))->
																		BgL_dssslzd2keywordszd2) =
																	((obj_t) BNIL), BUNSPEC);
																((((BgL_sfunz00_bglt)
																			COBJECT(BgL_new1114z00_1547))->
																		BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
																((((BgL_sfunz00_bglt)
																			COBJECT(BgL_new1114z00_1547))->
																		BgL_optionalsz00) =
																	((obj_t) BgL_optsz00_11), BUNSPEC);
																((((BgL_sfunz00_bglt)
																			COBJECT(BgL_new1114z00_1547))->
																		BgL_keysz00) =
																	((obj_t) BgL_keysz00_12), BUNSPEC);
																((((BgL_sfunz00_bglt)
																			COBJECT(BgL_new1114z00_1547))->
																		BgL_thezd2closurezd2globalz00) =
																	((obj_t) BUNSPEC), BUNSPEC);
																((((BgL_sfunz00_bglt)
																			COBJECT(BgL_new1114z00_1547))->
																		BgL_strengthz00) =
																	((obj_t) CNST_TABLE_REF(3)), BUNSPEC);
																((((BgL_sfunz00_bglt)
																			COBJECT(BgL_new1114z00_1547))->
																		BgL_stackablez00) =
																	((obj_t) BUNSPEC), BUNSPEC);
																BgL_sfunz00_1543 = BgL_new1114z00_1547;
															}
															{	/* Ast/glo_decl.scm 135 */
																obj_t BgL_oldz00_1544;

																BgL_oldz00_1544 =
																	BGl_findzd2globalzd2zzast_envz00
																	(BgL_idz00_1542, BNIL);
																{	/* Ast/glo_decl.scm 142 */
																	BgL_globalz00_bglt BgL_globalz00_1545;

																	BgL_globalz00_1545 =
																		BGl_bindzd2globalz12zc0zzast_envz00
																		(BgL_idz00_1542, BgL_aliasz00_14,
																		BgL_modulez00_16,
																		((BgL_valuez00_bglt) BgL_sfunz00_1543),
																		BgL_importz00_1534, BgL_srcez00_19);
																	{	/* Ast/glo_decl.scm 143 */

																		if (
																			(BgL_typezd2reszd2_1541 ==
																				BGl_za2_za2z00zztype_cachez00))
																			{	/* Ast/glo_decl.scm 151 */
																				if (BgL_exportzf3zf3_1533)
																					{	/* Ast/glo_decl.scm 154 */
																						BgL_typez00_bglt BgL_vz00_1976;

																						BgL_vz00_1976 =
																							((BgL_typez00_bglt)
																							BGl_za2objza2z00zztype_cachez00);
																						((((BgL_variablez00_bglt)
																									COBJECT((
																											(BgL_variablez00_bglt)
																											BgL_globalz00_1545)))->
																								BgL_typez00) =
																							((BgL_typez00_bglt)
																								BgL_vz00_1976), BUNSPEC);
																					}
																				else
																					{	/* Ast/glo_decl.scm 153 */
																						((((BgL_variablez00_bglt) COBJECT(
																										((BgL_variablez00_bglt)
																											BgL_globalz00_1545)))->
																								BgL_typez00) =
																							((BgL_typez00_bglt) (
																									(BgL_typez00_bglt)
																									BgL_typezd2reszd2_1541)),
																							BUNSPEC);
																					}
																			}
																		else
																			{	/* Ast/glo_decl.scm 151 */
																				((((BgL_variablez00_bglt) COBJECT(
																								((BgL_variablez00_bglt)
																									BgL_globalz00_1545)))->
																						BgL_typez00) =
																					((BgL_typez00_bglt) (
																							(BgL_typez00_bglt)
																							BgL_typezd2reszd2_1541)),
																					BUNSPEC);
																			}
																		return BgL_globalz00_1545;
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zzast_glozd2declzd2(obj_t BgL_keysz00_2098,
		obj_t BgL_argsz00_1592)
	{
		{	/* Ast/glo_decl.scm 87 */
			if (NULLP(BgL_argsz00_1592))
				{	/* Ast/glo_decl.scm 89 */
					return BNIL;
				}
			else
				{	/* Ast/glo_decl.scm 89 */
					if ((CAR(((obj_t) BgL_argsz00_1592)) == (BKEY)))
						{	/* Ast/glo_decl.scm 92 */
							obj_t BgL_arg1333z00_1597;
							obj_t BgL_arg1335z00_1598;

							BgL_arg1333z00_1597 = CAR(((obj_t) BgL_argsz00_1592));
							{	/* Ast/glo_decl.scm 95 */
								obj_t BgL_arg1339z00_1599;

								{	/* Ast/glo_decl.scm 95 */
									obj_t BgL_arg1340z00_1600;

									BgL_arg1340z00_1600 =
										BGl_dropz00zz__r4_pairs_and_lists_6_3z00(CDR(
											((obj_t) BgL_argsz00_1592)),
										bgl_list_length(BgL_keysz00_2098));
									BgL_arg1339z00_1599 =
										BGl_loopze70ze7zzast_glozd2declzd2(BgL_keysz00_2098,
										BgL_arg1340z00_1600);
								}
								BgL_arg1335z00_1598 =
									BGl_appendzd221011zd2zzast_glozd2declzd2(BgL_keysz00_2098,
									BgL_arg1339z00_1599);
							}
							return MAKE_YOUNG_PAIR(BgL_arg1333z00_1597, BgL_arg1335z00_1598);
						}
					else
						{	/* Ast/glo_decl.scm 97 */
							obj_t BgL_arg1346z00_1603;
							obj_t BgL_arg1348z00_1604;

							BgL_arg1346z00_1603 = CAR(((obj_t) BgL_argsz00_1592));
							{	/* Ast/glo_decl.scm 97 */
								obj_t BgL_arg1349z00_1605;

								BgL_arg1349z00_1605 = CDR(((obj_t) BgL_argsz00_1592));
								BgL_arg1348z00_1604 =
									BGl_loopze70ze7zzast_glozd2declzd2(BgL_keysz00_2098,
									BgL_arg1349z00_1605);
							}
							return MAKE_YOUNG_PAIR(BgL_arg1346z00_1603, BgL_arg1348z00_1604);
						}
				}
		}

	}



/* declare-global-noopt-sfun! */
	BgL_globalz00_bglt
		BGl_declarezd2globalzd2nooptzd2sfunz12zc0zzast_glozd2declzd2(obj_t
		BgL_idz00_37, obj_t BgL_aliasz00_38, obj_t BgL_argsz00_39,
		obj_t BgL_modulez00_40, obj_t BgL_importz00_41, obj_t BgL_classz00_42,
		obj_t BgL_srcez00_43, obj_t BgL_srciz00_44)
	{
		{	/* Ast/glo_decl.scm 177 */
			{	/* Ast/glo_decl.scm 178 */
				long BgL_arityz00_1623;

				BgL_arityz00_1623 =
					BGl_globalzd2arityzd2zztools_argsz00(BgL_argsz00_39);
				{	/* Ast/glo_decl.scm 178 */
					obj_t BgL_argsz00_1624;

					BgL_argsz00_1624 =
						BGl_argsza2zd2ze3argszd2listz41zztools_argsz00(BgL_argsz00_39);
					{	/* Ast/glo_decl.scm 179 */
						bool_t BgL_exportzf3zf3_1625;

						{	/* Ast/glo_decl.scm 180 */
							bool_t BgL__ortest_1116z00_1693;

							if ((BgL_importz00_41 == CNST_TABLE_REF(0)))
								{	/* Ast/glo_decl.scm 180 */
									BgL__ortest_1116z00_1693 = ((bool_t) 0);
								}
							else
								{	/* Ast/glo_decl.scm 180 */
									BgL__ortest_1116z00_1693 = ((bool_t) 1);
								}
							if (BgL__ortest_1116z00_1693)
								{	/* Ast/glo_decl.scm 180 */
									BgL_exportzf3zf3_1625 = BgL__ortest_1116z00_1693;
								}
							else
								{	/* Ast/glo_decl.scm 181 */
									obj_t BgL__andtest_1117z00_1694;

									{	/* Ast/glo_decl.scm 181 */
										obj_t BgL_arg1559z00_1695;

										{	/* Ast/glo_decl.scm 181 */
											obj_t BgL_arg1561z00_1696;

											BgL_arg1561z00_1696 =
												BGl_thezd2backendzd2zzbackend_backendz00();
											BgL_arg1559z00_1695 =
												(((BgL_backendz00_bglt) COBJECT(
														((BgL_backendz00_bglt) BgL_arg1561z00_1696)))->
												BgL_debugzd2supportzd2);
										}
										BgL__andtest_1117z00_1694 =
											BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CNST_TABLE_REF
											(1), BgL_arg1559z00_1695);
									}
									if (CBOOL(BgL__andtest_1117z00_1694))
										{	/* Ast/glo_decl.scm 181 */
											BgL_exportzf3zf3_1625 =
												(
												(long) CINT(BGl_za2bdbzd2debugza2zd2zzengine_paramz00)
												>= 3L);
										}
									else
										{	/* Ast/glo_decl.scm 181 */
											BgL_exportzf3zf3_1625 = ((bool_t) 0);
										}
								}
						}
						{	/* Ast/glo_decl.scm 180 */
							obj_t BgL_importz00_1626;

							{	/* Ast/glo_decl.scm 183 */
								bool_t BgL_test1819z00_2355;

								if ((BgL_importz00_41 == CNST_TABLE_REF(0)))
									{	/* Ast/glo_decl.scm 184 */
										bool_t BgL_test1822z00_2359;

										{	/* Ast/glo_decl.scm 184 */
											obj_t BgL_arg1552z00_1691;

											{	/* Ast/glo_decl.scm 184 */
												obj_t BgL_arg1553z00_1692;

												BgL_arg1553z00_1692 =
													BGl_thezd2backendzd2zzbackend_backendz00();
												BgL_arg1552z00_1691 =
													(((BgL_backendz00_bglt) COBJECT(
															((BgL_backendz00_bglt) BgL_arg1553z00_1692)))->
													BgL_debugzd2supportzd2);
											}
											BgL_test1822z00_2359 =
												CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
												(CNST_TABLE_REF(1), BgL_arg1552z00_1691));
										}
										if (BgL_test1822z00_2359)
											{	/* Ast/glo_decl.scm 184 */
												BgL_test1819z00_2355 =
													(
													(long) CINT(BGl_za2bdbzd2debugza2zd2zzengine_paramz00)
													>= 3L);
											}
										else
											{	/* Ast/glo_decl.scm 184 */
												BgL_test1819z00_2355 = ((bool_t) 0);
											}
									}
								else
									{	/* Ast/glo_decl.scm 183 */
										BgL_test1819z00_2355 = ((bool_t) 0);
									}
								if (BgL_test1819z00_2355)
									{	/* Ast/glo_decl.scm 183 */
										BgL_importz00_1626 = CNST_TABLE_REF(2);
									}
								else
									{	/* Ast/glo_decl.scm 183 */
										BgL_importz00_1626 = BgL_importz00_41;
									}
							}
							{	/* Ast/glo_decl.scm 183 */
								obj_t BgL_locz00_1627;

								BgL_locz00_1627 =
									BGl_findzd2locationzd2zztools_locationz00(BgL_srcez00_43);
								{	/* Ast/glo_decl.scm 188 */
									obj_t BgL_lociz00_1628;

									BgL_lociz00_1628 =
										BGl_findzd2locationzf2locz20zztools_locationz00
										(BgL_srciz00_44, BgL_locz00_1627);
									{	/* Ast/glo_decl.scm 189 */
										obj_t BgL_argszd2typezd2_1629;

										{	/* Ast/glo_decl.scm 190 */
											bool_t BgL_g1119z00_1657;

											BgL_g1119z00_1657 =
												(BgL_classz00_42 == CNST_TABLE_REF(4));
											{
												obj_t BgL_argsz00_1659;
												obj_t BgL_resz00_1660;
												bool_t BgL_sgfunzf3zf3_1661;

												BgL_argsz00_1659 = BgL_argsz00_1624;
												BgL_resz00_1660 = BNIL;
												BgL_sgfunzf3zf3_1661 = BgL_g1119z00_1657;
											BgL_zc3z04anonymousza31449ze3z87_1662:
												if (NULLP(BgL_argsz00_1659))
													{	/* Ast/glo_decl.scm 194 */
														if ((BgL_arityz00_1623 >= 0L))
															{	/* Ast/glo_decl.scm 195 */
																BgL_argszd2typezd2_1629 =
																	bgl_reverse_bang(BgL_resz00_1660);
															}
														else
															{	/* Ast/glo_decl.scm 197 */
																obj_t BgL_typez00_1665;

																BgL_typez00_1665 =
																	CAR(((obj_t) BgL_resz00_1660));
																{	/* Ast/glo_decl.scm 199 */
																	bool_t BgL_test1826z00_2380;

																	if (
																		(BgL_typez00_1665 ==
																			BGl_za2objza2z00zztype_cachez00))
																		{	/* Ast/glo_decl.scm 199 */
																			BgL_test1826z00_2380 = ((bool_t) 1);
																		}
																	else
																		{	/* Ast/glo_decl.scm 199 */
																			BgL_test1826z00_2380 =
																				(BgL_typez00_1665 ==
																				BGl_za2pairzd2nilza2zd2zztype_cachez00);
																		}
																	if (BgL_test1826z00_2380)
																		{	/* Ast/glo_decl.scm 199 */
																			BgL_argszd2typezd2_1629 =
																				bgl_reverse_bang(BgL_resz00_1660);
																		}
																	else
																		{	/* Ast/glo_decl.scm 199 */
																			if (
																				(BgL_typez00_1665 ==
																					BGl_za2_za2z00zztype_cachez00))
																				{	/* Ast/glo_decl.scm 203 */
																					obj_t BgL_arg1453z00_1667;

																					{	/* Ast/glo_decl.scm 203 */
																						obj_t BgL_arg1454z00_1668;

																						BgL_arg1454z00_1668 =
																							CDR(((obj_t) BgL_resz00_1660));
																						BgL_arg1453z00_1667 =
																							MAKE_YOUNG_PAIR
																							(BGl_za2objza2z00zztype_cachez00,
																							BgL_arg1454z00_1668);
																					}
																					BgL_argszd2typezd2_1629 =
																						bgl_reverse_bang
																						(BgL_arg1453z00_1667);
																				}
																			else
																				{	/* Ast/glo_decl.scm 202 */
																					BgL_argszd2typezd2_1629 =
																						BGl_userzd2errorzd2zztools_errorz00
																						(BgL_idz00_37,
																						BGl_string1770z00zzast_glozd2declzd2,
																						BGl_shapez00zztools_shapez00
																						(BgL_typez00_1665), BNIL);
																				}
																		}
																}
															}
													}
												else
													{	/* Ast/glo_decl.scm 208 */
														bool_t BgL_test1829z00_2393;

														{	/* Ast/glo_decl.scm 208 */
															obj_t BgL_arg1514z00_1682;

															BgL_arg1514z00_1682 =
																CAR(((obj_t) BgL_argsz00_1659));
															BgL_test1829z00_2393 =
																BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00
																(BgL_arg1514z00_1682);
														}
														if (BgL_test1829z00_2393)
															{	/* Ast/glo_decl.scm 209 */
																obj_t BgL_arg1489z00_1673;

																BgL_arg1489z00_1673 =
																	MAKE_YOUNG_PAIR
																	(BGl_za2objza2z00zztype_cachez00,
																	BgL_resz00_1660);
																BgL_argszd2typezd2_1629 =
																	bgl_reverse_bang(BgL_arg1489z00_1673);
															}
														else
															{	/* Ast/glo_decl.scm 211 */
																obj_t BgL_typez00_1674;

																{	/* Ast/glo_decl.scm 211 */
																	BgL_typez00_bglt BgL_tz00_1677;

																	{	/* Ast/glo_decl.scm 212 */
																		obj_t BgL_arg1513z00_1681;

																		BgL_arg1513z00_1681 =
																			CAR(((obj_t) BgL_argsz00_1659));
																		BgL_tz00_1677 =
																			BGl_typezd2ofzd2idzf2importzd2locationz20zzast_identz00
																			(BgL_arg1513z00_1681, BgL_locz00_1627,
																			BgL_lociz00_1628);
																	}
																	{	/* Ast/glo_decl.scm 213 */
																		bool_t BgL_test1831z00_2402;

																		if (
																			(((obj_t) BgL_tz00_1677) ==
																				BGl_za2_za2z00zztype_cachez00))
																			{	/* Ast/glo_decl.scm 213 */
																				if (BgL_exportzf3zf3_1625)
																					{	/* Ast/glo_decl.scm 214 */
																						BgL_test1831z00_2402 =
																							BgL_exportzf3zf3_1625;
																					}
																				else
																					{	/* Ast/glo_decl.scm 214 */
																						BgL_test1831z00_2402 =
																							BgL_sgfunzf3zf3_1661;
																					}
																			}
																		else
																			{	/* Ast/glo_decl.scm 213 */
																				BgL_test1831z00_2402 = ((bool_t) 0);
																			}
																		if (BgL_test1831z00_2402)
																			{	/* Ast/glo_decl.scm 213 */
																				BgL_typez00_1674 =
																					BGl_za2objza2z00zztype_cachez00;
																			}
																		else
																			{	/* Ast/glo_decl.scm 213 */
																				BgL_typez00_1674 =
																					((obj_t) BgL_tz00_1677);
																			}
																	}
																}
																{	/* Ast/glo_decl.scm 217 */
																	obj_t BgL_arg1502z00_1675;
																	obj_t BgL_arg1509z00_1676;

																	BgL_arg1502z00_1675 =
																		CDR(((obj_t) BgL_argsz00_1659));
																	BgL_arg1509z00_1676 =
																		MAKE_YOUNG_PAIR(BgL_typez00_1674,
																		BgL_resz00_1660);
																	{
																		bool_t BgL_sgfunzf3zf3_2413;
																		obj_t BgL_resz00_2412;
																		obj_t BgL_argsz00_2411;

																		BgL_argsz00_2411 = BgL_arg1502z00_1675;
																		BgL_resz00_2412 = BgL_arg1509z00_1676;
																		BgL_sgfunzf3zf3_2413 = ((bool_t) 0);
																		BgL_sgfunzf3zf3_1661 = BgL_sgfunzf3zf3_2413;
																		BgL_resz00_1660 = BgL_resz00_2412;
																		BgL_argsz00_1659 = BgL_argsz00_2411;
																		goto BgL_zc3z04anonymousza31449ze3z87_1662;
																	}
																}
															}
													}
											}
										}
										{	/* Ast/glo_decl.scm 190 */
											obj_t BgL_argszd2namezd2_1630;

											{
												obj_t BgL_argsz00_1643;
												obj_t BgL_resz00_1644;

												BgL_argsz00_1643 = BgL_argsz00_1624;
												BgL_resz00_1644 = BNIL;
											BgL_zc3z04anonymousza31381ze3z87_1645:
												if (NULLP(BgL_argsz00_1643))
													{	/* Ast/glo_decl.scm 223 */
														BgL_argszd2namezd2_1630 =
															bgl_reverse_bang(BgL_resz00_1644);
													}
												else
													{	/* Ast/glo_decl.scm 225 */
														bool_t BgL_test1835z00_2417;

														{	/* Ast/glo_decl.scm 225 */
															obj_t BgL_arg1448z00_1654;

															BgL_arg1448z00_1654 =
																CAR(((obj_t) BgL_argsz00_1643));
															BgL_test1835z00_2417 =
																BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00
																(BgL_arg1448z00_1654);
														}
														if (BgL_test1835z00_2417)
															{	/* Ast/glo_decl.scm 226 */
																obj_t BgL_arg1421z00_1649;

																BgL_arg1421z00_1649 =
																	CDR(((obj_t) BgL_argsz00_1643));
																{
																	obj_t BgL_argsz00_2423;

																	BgL_argsz00_2423 = BgL_arg1421z00_1649;
																	BgL_argsz00_1643 = BgL_argsz00_2423;
																	goto BgL_zc3z04anonymousza31381ze3z87_1645;
																}
															}
														else
															{	/* Ast/glo_decl.scm 228 */
																obj_t BgL_az00_1650;

																{	/* Ast/glo_decl.scm 228 */
																	obj_t BgL_arg1437z00_1653;

																	BgL_arg1437z00_1653 =
																		CAR(((obj_t) BgL_argsz00_1643));
																	BgL_az00_1650 =
																		BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																		(BgL_arg1437z00_1653, BgL_locz00_1627);
																}
																{	/* Ast/glo_decl.scm 229 */
																	obj_t BgL_arg1422z00_1651;
																	obj_t BgL_arg1434z00_1652;

																	BgL_arg1422z00_1651 =
																		CDR(((obj_t) BgL_argsz00_1643));
																	BgL_arg1434z00_1652 =
																		MAKE_YOUNG_PAIR(BgL_az00_1650,
																		BgL_resz00_1644);
																	{
																		obj_t BgL_resz00_2431;
																		obj_t BgL_argsz00_2430;

																		BgL_argsz00_2430 = BgL_arg1422z00_1651;
																		BgL_resz00_2431 = BgL_arg1434z00_1652;
																		BgL_resz00_1644 = BgL_resz00_2431;
																		BgL_argsz00_1643 = BgL_argsz00_2430;
																		goto BgL_zc3z04anonymousza31381ze3z87_1645;
																	}
																}
															}
													}
											}
											{	/* Ast/glo_decl.scm 220 */
												obj_t BgL_idzd2typezd2_1631;

												BgL_idzd2typezd2_1631 =
													BGl_parsezd2idzf2importzd2locationzf2zzast_identz00
													(BgL_idz00_37, BgL_locz00_1627, BgL_lociz00_1628);
												{	/* Ast/glo_decl.scm 230 */
													obj_t BgL_typezd2reszd2_1632;

													BgL_typezd2reszd2_1632 = CDR(BgL_idzd2typezd2_1631);
													{	/* Ast/glo_decl.scm 231 */
														obj_t BgL_idz00_1633;

														BgL_idz00_1633 = CAR(BgL_idzd2typezd2_1631);
														{	/* Ast/glo_decl.scm 232 */
															BgL_sfunz00_bglt BgL_sfunz00_1634;

															{	/* Ast/glo_decl.scm 233 */
																BgL_sfunz00_bglt BgL_new1123z00_1638;

																{	/* Ast/glo_decl.scm 234 */
																	BgL_sfunz00_bglt BgL_new1122z00_1639;

																	BgL_new1122z00_1639 =
																		((BgL_sfunz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_sfunz00_bgl))));
																	{	/* Ast/glo_decl.scm 234 */
																		long BgL_arg1380z00_1640;

																		BgL_arg1380z00_1640 =
																			BGL_CLASS_NUM(BGl_sfunz00zzast_varz00);
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1122z00_1639),
																			BgL_arg1380z00_1640);
																	}
																	{	/* Ast/glo_decl.scm 234 */
																		BgL_objectz00_bglt BgL_tmpz00_2439;

																		BgL_tmpz00_2439 =
																			((BgL_objectz00_bglt)
																			BgL_new1122z00_1639);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2439,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1122z00_1639);
																	BgL_new1123z00_1638 = BgL_new1122z00_1639;
																}
																((((BgL_funz00_bglt) COBJECT(
																				((BgL_funz00_bglt)
																					BgL_new1123z00_1638)))->
																		BgL_arityz00) =
																	((long) BgL_arityz00_1623), BUNSPEC);
																((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																					BgL_new1123z00_1638)))->
																		BgL_sidezd2effectzd2) =
																	((obj_t) BUNSPEC), BUNSPEC);
																((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																					BgL_new1123z00_1638)))->
																		BgL_predicatezd2ofzd2) =
																	((obj_t) BFALSE), BUNSPEC);
																((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																					BgL_new1123z00_1638)))->
																		BgL_stackzd2allocatorzd2) =
																	((obj_t) BFALSE), BUNSPEC);
																((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																					BgL_new1123z00_1638)))->
																		BgL_topzf3zf3) =
																	((bool_t) ((bool_t) 1)), BUNSPEC);
																((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																					BgL_new1123z00_1638)))->
																		BgL_thezd2closurezd2) =
																	((obj_t) BUNSPEC), BUNSPEC);
																((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																					BgL_new1123z00_1638)))->
																		BgL_effectz00) =
																	((obj_t) BUNSPEC), BUNSPEC);
																((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																					BgL_new1123z00_1638)))->
																		BgL_failsafez00) =
																	((obj_t) BUNSPEC), BUNSPEC);
																((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																					BgL_new1123z00_1638)))->
																		BgL_argszd2noescapezd2) =
																	((obj_t) BNIL), BUNSPEC);
																((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																					BgL_new1123z00_1638)))->
																		BgL_argszd2retescapezd2) =
																	((obj_t) BNIL), BUNSPEC);
																((((BgL_sfunz00_bglt)
																			COBJECT(BgL_new1123z00_1638))->
																		BgL_propertyz00) = ((obj_t) BNIL), BUNSPEC);
																((((BgL_sfunz00_bglt)
																			COBJECT(BgL_new1123z00_1638))->
																		BgL_argsz00) =
																	((obj_t) BgL_argszd2typezd2_1629), BUNSPEC);
																((((BgL_sfunz00_bglt)
																			COBJECT(BgL_new1123z00_1638))->
																		BgL_argszd2namezd2) =
																	((obj_t) BgL_argszd2namezd2_1630), BUNSPEC);
																((((BgL_sfunz00_bglt)
																			COBJECT(BgL_new1123z00_1638))->
																		BgL_bodyz00) = ((obj_t) BUNSPEC), BUNSPEC);
																((((BgL_sfunz00_bglt)
																			COBJECT(BgL_new1123z00_1638))->
																		BgL_classz00) =
																	((obj_t) BgL_classz00_42), BUNSPEC);
																((((BgL_sfunz00_bglt)
																			COBJECT(BgL_new1123z00_1638))->
																		BgL_dssslzd2keywordszd2) =
																	((obj_t)
																		BGl_dssslzd2formalszd2zztools_dssslz00
																		(BgL_argsz00_1624)), BUNSPEC);
																((((BgL_sfunz00_bglt)
																			COBJECT(BgL_new1123z00_1638))->
																		BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
																((((BgL_sfunz00_bglt)
																			COBJECT(BgL_new1123z00_1638))->
																		BgL_optionalsz00) =
																	((obj_t) BNIL), BUNSPEC);
																((((BgL_sfunz00_bglt)
																			COBJECT(BgL_new1123z00_1638))->
																		BgL_keysz00) = ((obj_t) BNIL), BUNSPEC);
																((((BgL_sfunz00_bglt)
																			COBJECT(BgL_new1123z00_1638))->
																		BgL_thezd2closurezd2globalz00) =
																	((obj_t) BUNSPEC), BUNSPEC);
																((((BgL_sfunz00_bglt)
																			COBJECT(BgL_new1123z00_1638))->
																		BgL_strengthz00) =
																	((obj_t) CNST_TABLE_REF(3)), BUNSPEC);
																((((BgL_sfunz00_bglt)
																			COBJECT(BgL_new1123z00_1638))->
																		BgL_stackablez00) =
																	((obj_t) BUNSPEC), BUNSPEC);
																BgL_sfunz00_1634 = BgL_new1123z00_1638;
															}
															{	/* Ast/glo_decl.scm 233 */
																obj_t BgL_oldz00_1635;

																BgL_oldz00_1635 =
																	BGl_findzd2globalzd2zzast_envz00
																	(BgL_idz00_1633, BNIL);
																{	/* Ast/glo_decl.scm 239 */
																	BgL_globalz00_bglt BgL_globalz00_1636;

																	BgL_globalz00_1636 =
																		BGl_bindzd2globalz12zc0zzast_envz00
																		(BgL_idz00_1633, BgL_aliasz00_38,
																		BgL_modulez00_40,
																		((BgL_valuez00_bglt) BgL_sfunz00_1634),
																		BgL_importz00_1626, BgL_srcez00_43);
																	{	/* Ast/glo_decl.scm 240 */

																		if (
																			(BgL_typezd2reszd2_1632 ==
																				BGl_za2_za2z00zztype_cachez00))
																			{	/* Ast/glo_decl.scm 248 */
																				if (BgL_exportzf3zf3_1625)
																					{	/* Ast/glo_decl.scm 251 */
																						BgL_typez00_bglt BgL_vz00_2004;

																						BgL_vz00_2004 =
																							((BgL_typez00_bglt)
																							BGl_za2objza2z00zztype_cachez00);
																						((((BgL_variablez00_bglt)
																									COBJECT((
																											(BgL_variablez00_bglt)
																											BgL_globalz00_1636)))->
																								BgL_typez00) =
																							((BgL_typez00_bglt)
																								BgL_vz00_2004), BUNSPEC);
																					}
																				else
																					{	/* Ast/glo_decl.scm 250 */
																						((((BgL_variablez00_bglt) COBJECT(
																										((BgL_variablez00_bglt)
																											BgL_globalz00_1636)))->
																								BgL_typez00) =
																							((BgL_typez00_bglt) (
																									(BgL_typez00_bglt)
																									BgL_typezd2reszd2_1632)),
																							BUNSPEC);
																					}
																			}
																		else
																			{	/* Ast/glo_decl.scm 248 */
																				((((BgL_variablez00_bglt) COBJECT(
																								((BgL_variablez00_bglt)
																									BgL_globalz00_1636)))->
																						BgL_typez00) =
																					((BgL_typez00_bglt) (
																							(BgL_typez00_bglt)
																							BgL_typezd2reszd2_1632)),
																					BUNSPEC);
																			}
																		return BgL_globalz00_1636;
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* declare-global-svar! */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_declarezd2globalzd2svarz12z12zzast_glozd2declzd2(obj_t BgL_idz00_45,
		obj_t BgL_aliasz00_46, obj_t BgL_modulez00_47, obj_t BgL_importz00_48,
		obj_t BgL_srcez00_49, obj_t BgL_srciz00_50)
	{
		{	/* Ast/glo_decl.scm 260 */
			{	/* Ast/glo_decl.scm 261 */
				obj_t BgL_locz00_1697;

				BgL_locz00_1697 =
					BGl_findzd2locationzd2zztools_locationz00(BgL_srcez00_49);
				{	/* Ast/glo_decl.scm 261 */
					obj_t BgL_lociz00_1698;

					BgL_lociz00_1698 =
						BGl_findzd2locationzf2locz20zztools_locationz00(BgL_srciz00_50,
						BgL_locz00_1697);
					{	/* Ast/glo_decl.scm 262 */
						obj_t BgL_idzd2typezd2_1699;

						BgL_idzd2typezd2_1699 =
							BGl_parsezd2idzf2importzd2locationzf2zzast_identz00(BgL_idz00_45,
							BgL_locz00_1697, BgL_lociz00_1698);
						{	/* Ast/glo_decl.scm 263 */
							obj_t BgL_typez00_1700;

							{	/* Ast/glo_decl.scm 264 */
								obj_t BgL_typez00_1718;

								BgL_typez00_1718 = CDR(BgL_idzd2typezd2_1699);
								if (
									((((BgL_typez00_bglt) COBJECT(
													((BgL_typez00_bglt) BgL_typez00_1718)))->
											BgL_classz00) == CNST_TABLE_REF(5)))
									{	/* Ast/glo_decl.scm 273 */
										bool_t BgL_test1839z00_2501;

										if ((BgL_typez00_1718 == BGl_za2_za2z00zztype_cachez00))
											{	/* Ast/glo_decl.scm 274 */
												obj_t BgL__ortest_1124z00_1726;

												BgL__ortest_1124z00_1726 =
													BGl_memqz00zz__r4_pairs_and_lists_6_3z00
													(BgL_importz00_48, CNST_TABLE_REF(6));
												if (CBOOL(BgL__ortest_1124z00_1726))
													{	/* Ast/glo_decl.scm 274 */
														BgL_test1839z00_2501 =
															CBOOL(BgL__ortest_1124z00_1726);
													}
												else
													{	/* Ast/glo_decl.scm 275 */
														obj_t BgL__andtest_1125z00_1727;

														{	/* Ast/glo_decl.scm 276 */
															obj_t BgL_arg1602z00_1728;

															{	/* Ast/glo_decl.scm 276 */
																obj_t BgL_arg1605z00_1729;

																BgL_arg1605z00_1729 =
																	BGl_thezd2backendzd2zzbackend_backendz00();
																BgL_arg1602z00_1728 =
																	(((BgL_backendz00_bglt) COBJECT(
																			((BgL_backendz00_bglt)
																				BgL_arg1605z00_1729)))->
																	BgL_debugzd2supportzd2);
															}
															BgL__andtest_1125z00_1727 =
																BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																(CNST_TABLE_REF(1), BgL_arg1602z00_1728);
														}
														if (CBOOL(BgL__andtest_1125z00_1727))
															{	/* Ast/glo_decl.scm 275 */
																BgL_test1839z00_2501 =
																	(
																	(long)
																	CINT
																	(BGl_za2bdbzd2debugza2zd2zzengine_paramz00) >
																	0L);
															}
														else
															{	/* Ast/glo_decl.scm 275 */
																BgL_test1839z00_2501 = ((bool_t) 0);
															}
													}
											}
										else
											{	/* Ast/glo_decl.scm 273 */
												BgL_test1839z00_2501 = ((bool_t) 0);
											}
										if (BgL_test1839z00_2501)
											{	/* Ast/glo_decl.scm 273 */
												BgL_typez00_1700 = BGl_za2objza2z00zztype_cachez00;
											}
										else
											{	/* Ast/glo_decl.scm 273 */
												BgL_typez00_1700 = BgL_typez00_1718;
											}
									}
								else
									{	/* Ast/glo_decl.scm 271 */
										obj_t BgL_arg1606z00_1730;

										BgL_arg1606z00_1730 =
											BGl_shapez00zztools_shapez00(BgL_typez00_1718);
										{	/* Ast/glo_decl.scm 269 */
											obj_t BgL_list1607z00_1731;

											BgL_list1607z00_1731 =
												MAKE_YOUNG_PAIR(BGl_za2_za2z00zztype_cachez00, BNIL);
											BgL_typez00_1700 =
												BGl_userzd2errorzd2zztools_errorz00(BgL_idz00_45,
												BGl_string1771z00zzast_glozd2declzd2,
												BgL_arg1606z00_1730, BgL_list1607z00_1731);
										}
									}
							}
							{	/* Ast/glo_decl.scm 264 */
								obj_t BgL_importz00_1701;

								{	/* Ast/glo_decl.scm 281 */
									bool_t BgL_test1843z00_2521;

									if ((BgL_importz00_48 == CNST_TABLE_REF(0)))
										{	/* Ast/glo_decl.scm 282 */
											bool_t BgL_test1845z00_2525;

											{	/* Ast/glo_decl.scm 282 */
												obj_t BgL_arg1584z00_1716;

												{	/* Ast/glo_decl.scm 282 */
													obj_t BgL_arg1585z00_1717;

													BgL_arg1585z00_1717 =
														BGl_thezd2backendzd2zzbackend_backendz00();
													BgL_arg1584z00_1716 =
														(((BgL_backendz00_bglt) COBJECT(
																((BgL_backendz00_bglt) BgL_arg1585z00_1717)))->
														BgL_debugzd2supportzd2);
												}
												BgL_test1845z00_2525 =
													CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
													(CNST_TABLE_REF(1), BgL_arg1584z00_1716));
											}
											if (BgL_test1845z00_2525)
												{	/* Ast/glo_decl.scm 282 */
													BgL_test1843z00_2521 =
														(
														(long)
														CINT(BGl_za2bdbzd2debugza2zd2zzengine_paramz00) >=
														3L);
												}
											else
												{	/* Ast/glo_decl.scm 282 */
													BgL_test1843z00_2521 = ((bool_t) 0);
												}
										}
									else
										{	/* Ast/glo_decl.scm 281 */
											BgL_test1843z00_2521 = ((bool_t) 0);
										}
									if (BgL_test1843z00_2521)
										{	/* Ast/glo_decl.scm 281 */
											BgL_importz00_1701 = CNST_TABLE_REF(2);
										}
									else
										{	/* Ast/glo_decl.scm 281 */
											BgL_importz00_1701 = BgL_importz00_48;
										}
								}
								{	/* Ast/glo_decl.scm 281 */
									obj_t BgL_idz00_1702;

									BgL_idz00_1702 = CAR(BgL_idzd2typezd2_1699);
									{	/* Ast/glo_decl.scm 286 */
										BgL_svarz00_bglt BgL_svarz00_1703;

										{	/* Ast/glo_decl.scm 287 */
											BgL_svarz00_bglt BgL_new1129z00_1706;

											{	/* Ast/glo_decl.scm 287 */
												BgL_svarz00_bglt BgL_new1127z00_1707;

												BgL_new1127z00_1707 =
													((BgL_svarz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																BgL_svarz00_bgl))));
												{	/* Ast/glo_decl.scm 287 */
													long BgL_arg1565z00_1708;

													BgL_arg1565z00_1708 =
														BGL_CLASS_NUM(BGl_svarz00zzast_varz00);
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt) BgL_new1127z00_1707),
														BgL_arg1565z00_1708);
												}
												{	/* Ast/glo_decl.scm 287 */
													BgL_objectz00_bglt BgL_tmpz00_2540;

													BgL_tmpz00_2540 =
														((BgL_objectz00_bglt) BgL_new1127z00_1707);
													BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2540, BFALSE);
												}
												((BgL_objectz00_bglt) BgL_new1127z00_1707);
												BgL_new1129z00_1706 = BgL_new1127z00_1707;
											}
											((((BgL_svarz00_bglt) COBJECT(BgL_new1129z00_1706))->
													BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
											BgL_svarz00_1703 = BgL_new1129z00_1706;
										}
										{	/* Ast/glo_decl.scm 287 */
											BgL_globalz00_bglt BgL_globalz00_1704;

											BgL_globalz00_1704 =
												BGl_bindzd2globalz12zc0zzast_envz00(BgL_idz00_1702,
												BgL_aliasz00_46, BgL_modulez00_47,
												((BgL_valuez00_bglt) BgL_svarz00_1703),
												BgL_importz00_1701, BgL_srcez00_49);
											{	/* Ast/glo_decl.scm 288 */

												((((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt) BgL_globalz00_1704)))->
														BgL_typez00) =
													((BgL_typez00_bglt) ((BgL_typez00_bglt)
															BgL_typez00_1700)), BUNSPEC);
												{	/* Ast/glo_decl.scm 292 */
													obj_t BgL_arg1564z00_1705;

													if ((BgL_importz00_1701 == CNST_TABLE_REF(0)))
														{	/* Ast/glo_decl.scm 292 */
															BgL_arg1564z00_1705 = CNST_TABLE_REF(7);
														}
													else
														{	/* Ast/glo_decl.scm 292 */
															BgL_arg1564z00_1705 = CNST_TABLE_REF(8);
														}
													((((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		BgL_globalz00_1704)))->BgL_accessz00) =
														((obj_t) BgL_arg1564z00_1705), BUNSPEC);
												}
												return BgL_globalz00_1704;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &declare-global-svar! */
	BgL_globalz00_bglt
		BGl_z62declarezd2globalzd2svarz12z70zzast_glozd2declzd2(obj_t
		BgL_envz00_2064, obj_t BgL_idz00_2065, obj_t BgL_aliasz00_2066,
		obj_t BgL_modulez00_2067, obj_t BgL_importz00_2068, obj_t BgL_srcez00_2069,
		obj_t BgL_srciz00_2070)
	{
		{	/* Ast/glo_decl.scm 260 */
			return
				BGl_declarezd2globalzd2svarz12z12zzast_glozd2declzd2(BgL_idz00_2065,
				BgL_aliasz00_2066, BgL_modulez00_2067, BgL_importz00_2068,
				BgL_srcez00_2069, BgL_srciz00_2070);
		}

	}



/* declare-global-scnst! */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_declarezd2globalzd2scnstz12z12zzast_glozd2declzd2(obj_t BgL_idz00_51,
		obj_t BgL_aliasz00_52, obj_t BgL_modulez00_53, obj_t BgL_importz00_54,
		obj_t BgL_nodez00_55, obj_t BgL_classz00_56, obj_t BgL_locz00_57)
	{
		{	/* Ast/glo_decl.scm 299 */
			{	/* Ast/glo_decl.scm 300 */
				obj_t BgL_idzd2typezd2_1733;

				BgL_idzd2typezd2_1733 =
					BGl_parsezd2idzd2zzast_identz00(BgL_idz00_51, BgL_locz00_57);
				{	/* Ast/glo_decl.scm 300 */
					obj_t BgL_typez00_1734;

					{	/* Ast/glo_decl.scm 301 */
						obj_t BgL_typez00_1741;

						BgL_typez00_1741 = CDR(BgL_idzd2typezd2_1733);
						if ((BgL_typez00_1741 == BGl_za2_za2z00zztype_cachez00))
							{	/* Ast/glo_decl.scm 305 */
								BgL_typez00_1734 =
									BGl_internalzd2errorzd2zztools_errorz00(BgL_idz00_51,
									BGl_string1771z00zzast_glozd2declzd2,
									BGl_shapez00zztools_shapez00(BgL_typez00_1741));
							}
						else
							{	/* Ast/glo_decl.scm 305 */
								BgL_typez00_1734 = BgL_typez00_1741;
							}
					}
					{	/* Ast/glo_decl.scm 301 */
						obj_t BgL_idz00_1735;

						BgL_idz00_1735 = CAR(BgL_idzd2typezd2_1733);
						{	/* Ast/glo_decl.scm 311 */
							BgL_scnstz00_bglt BgL_scnstz00_1736;

							{	/* Ast/glo_decl.scm 312 */
								BgL_scnstz00_bglt BgL_new1131z00_1738;

								{	/* Ast/glo_decl.scm 314 */
									BgL_scnstz00_bglt BgL_new1130z00_1739;

									BgL_new1130z00_1739 =
										((BgL_scnstz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_scnstz00_bgl))));
									{	/* Ast/glo_decl.scm 314 */
										long BgL_arg1611z00_1740;

										BgL_arg1611z00_1740 =
											BGL_CLASS_NUM(BGl_scnstz00zzast_varz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1130z00_1739),
											BgL_arg1611z00_1740);
									}
									{	/* Ast/glo_decl.scm 314 */
										BgL_objectz00_bglt BgL_tmpz00_2569;

										BgL_tmpz00_2569 =
											((BgL_objectz00_bglt) BgL_new1130z00_1739);
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2569, BFALSE);
									}
									((BgL_objectz00_bglt) BgL_new1130z00_1739);
									BgL_new1131z00_1738 = BgL_new1130z00_1739;
								}
								((((BgL_scnstz00_bglt) COBJECT(BgL_new1131z00_1738))->
										BgL_nodez00) = ((obj_t) BgL_nodez00_55), BUNSPEC);
								((((BgL_scnstz00_bglt) COBJECT(BgL_new1131z00_1738))->
										BgL_classz00) = ((obj_t) BgL_classz00_56), BUNSPEC);
								((((BgL_scnstz00_bglt) COBJECT(BgL_new1131z00_1738))->
										BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
								BgL_scnstz00_1736 = BgL_new1131z00_1738;
							}
							{	/* Ast/glo_decl.scm 312 */
								BgL_globalz00_bglt BgL_globalz00_1737;

								BgL_globalz00_1737 =
									BGl_bindzd2globalz12zc0zzast_envz00(BgL_idz00_1735,
									BgL_aliasz00_52, BgL_modulez00_53,
									((BgL_valuez00_bglt) BgL_scnstz00_1736), BgL_importz00_54,
									CNST_TABLE_REF(9));
								{	/* Ast/glo_decl.scm 315 */

									((((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_globalz00_1737)))->
											BgL_typez00) =
										((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_typez00_1734)),
										BUNSPEC);
									{	/* Ast/glo_decl.scm 319 */
										obj_t BgL_vz00_2032;

										BgL_vz00_2032 = CNST_TABLE_REF(7);
										((((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_globalz00_1737)))->
												BgL_accessz00) = ((obj_t) BgL_vz00_2032), BUNSPEC);
									}
									return BgL_globalz00_1737;
								}
							}
						}
					}
				}
			}
		}

	}



/* &declare-global-scnst! */
	BgL_globalz00_bglt
		BGl_z62declarezd2globalzd2scnstz12z70zzast_glozd2declzd2(obj_t
		BgL_envz00_2071, obj_t BgL_idz00_2072, obj_t BgL_aliasz00_2073,
		obj_t BgL_modulez00_2074, obj_t BgL_importz00_2075, obj_t BgL_nodez00_2076,
		obj_t BgL_classz00_2077, obj_t BgL_locz00_2078)
	{
		{	/* Ast/glo_decl.scm 299 */
			return
				BGl_declarezd2globalzd2scnstz12z12zzast_glozd2declzd2(BgL_idz00_2072,
				BgL_aliasz00_2073, BgL_modulez00_2074, BgL_importz00_2075,
				BgL_nodez00_2076, BgL_classz00_2077, BgL_locz00_2078);
		}

	}



/* declare-global-cfun! */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_declarezd2globalzd2cfunz12z12zzast_glozd2declzd2(obj_t BgL_idz00_58,
		obj_t BgL_aliasz00_59, obj_t BgL_modulez00_60, obj_t BgL_namez00_61,
		obj_t BgL_treszd2idzd2_62, obj_t BgL_targszd2idzd2_63,
		bool_t BgL_infixzf3zf3_64, bool_t BgL_macrozf3zf3_65, obj_t BgL_srcez00_66,
		obj_t BgL_srciz00_67)
	{
		{	/* Ast/glo_decl.scm 326 */
			{	/* Ast/glo_decl.scm 327 */
				long BgL_arityz00_1743;

				BgL_arityz00_1743 =
					BGl_globalzd2arityzd2zztools_argsz00(BgL_targszd2idzd2_63);
				{	/* Ast/glo_decl.scm 327 */
					obj_t BgL_locz00_1744;

					BgL_locz00_1744 =
						BGl_findzd2locationzd2zztools_locationz00(BgL_srcez00_66);
					{	/* Ast/glo_decl.scm 328 */
						obj_t BgL_lociz00_1745;

						BgL_lociz00_1745 =
							BGl_findzd2locationzf2locz20zztools_locationz00(BgL_srciz00_67,
							BgL_locz00_1744);
						{	/* Ast/glo_decl.scm 329 */
							BgL_typez00_bglt BgL_typezd2reszd2_1746;

							BgL_typezd2reszd2_1746 =
								BGl_usezd2foreignzd2typezf2importzd2locz12z32zztype_envz00
								(BgL_treszd2idzd2_62, BgL_locz00_1744, BgL_lociz00_1745);
							{	/* Ast/glo_decl.scm 330 */
								obj_t BgL_typezd2argszd2_1747;

								{	/* Ast/glo_decl.scm 331 */
									obj_t BgL_l1253z00_1753;

									BgL_l1253z00_1753 =
										BGl_argsza2zd2ze3argszd2listz41zztools_argsz00
										(BgL_targszd2idzd2_63);
									if (NULLP(BgL_l1253z00_1753))
										{	/* Ast/glo_decl.scm 331 */
											BgL_typezd2argszd2_1747 = BNIL;
										}
									else
										{	/* Ast/glo_decl.scm 331 */
											obj_t BgL_head1255z00_1755;

											BgL_head1255z00_1755 = MAKE_YOUNG_PAIR(BNIL, BNIL);
											{
												obj_t BgL_l1253z00_1757;
												obj_t BgL_tail1256z00_1758;

												BgL_l1253z00_1757 = BgL_l1253z00_1753;
												BgL_tail1256z00_1758 = BgL_head1255z00_1755;
											BgL_zc3z04anonymousza31618ze3z87_1759:
												if (NULLP(BgL_l1253z00_1757))
													{	/* Ast/glo_decl.scm 331 */
														BgL_typezd2argszd2_1747 = CDR(BgL_head1255z00_1755);
													}
												else
													{	/* Ast/glo_decl.scm 331 */
														obj_t BgL_newtail1257z00_1761;

														{	/* Ast/glo_decl.scm 331 */
															BgL_typez00_bglt BgL_arg1626z00_1763;

															{	/* Ast/glo_decl.scm 331 */
																obj_t BgL_tz00_1764;

																BgL_tz00_1764 =
																	CAR(((obj_t) BgL_l1253z00_1757));
																BgL_arg1626z00_1763 =
																	BGl_usezd2foreignzd2typezf2importzd2locz12z32zztype_envz00
																	(BgL_tz00_1764, BgL_locz00_1744,
																	BgL_lociz00_1745);
															}
															BgL_newtail1257z00_1761 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_arg1626z00_1763), BNIL);
														}
														SET_CDR(BgL_tail1256z00_1758,
															BgL_newtail1257z00_1761);
														{	/* Ast/glo_decl.scm 331 */
															obj_t BgL_arg1625z00_1762;

															BgL_arg1625z00_1762 =
																CDR(((obj_t) BgL_l1253z00_1757));
															{
																obj_t BgL_tail1256z00_2606;
																obj_t BgL_l1253z00_2605;

																BgL_l1253z00_2605 = BgL_arg1625z00_1762;
																BgL_tail1256z00_2606 = BgL_newtail1257z00_1761;
																BgL_tail1256z00_1758 = BgL_tail1256z00_2606;
																BgL_l1253z00_1757 = BgL_l1253z00_2605;
																goto BgL_zc3z04anonymousza31618ze3z87_1759;
															}
														}
													}
											}
										}
								}
								{	/* Ast/glo_decl.scm 331 */
									BgL_cfunz00_bglt BgL_cfunz00_1748;

									{	/* Ast/glo_decl.scm 334 */
										BgL_cfunz00_bglt BgL_new1134z00_1750;

										{	/* Ast/glo_decl.scm 334 */
											BgL_cfunz00_bglt BgL_new1133z00_1751;

											BgL_new1133z00_1751 =
												((BgL_cfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_cfunz00_bgl))));
											{	/* Ast/glo_decl.scm 334 */
												long BgL_arg1615z00_1752;

												BgL_arg1615z00_1752 =
													BGL_CLASS_NUM(BGl_cfunz00zzast_varz00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1133z00_1751),
													BgL_arg1615z00_1752);
											}
											{	/* Ast/glo_decl.scm 334 */
												BgL_objectz00_bglt BgL_tmpz00_2611;

												BgL_tmpz00_2611 =
													((BgL_objectz00_bglt) BgL_new1133z00_1751);
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2611, BFALSE);
											}
											((BgL_objectz00_bglt) BgL_new1133z00_1751);
											BgL_new1134z00_1750 = BgL_new1133z00_1751;
										}
										((((BgL_funz00_bglt) COBJECT(
														((BgL_funz00_bglt) BgL_new1134z00_1750)))->
												BgL_arityz00) = ((long) BgL_arityz00_1743), BUNSPEC);
										((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_new1134z00_1750)))->BgL_sidezd2effectzd2) =
											((obj_t) BUNSPEC), BUNSPEC);
										((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_new1134z00_1750)))->BgL_predicatezd2ofzd2) =
											((obj_t) BFALSE), BUNSPEC);
										((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_new1134z00_1750)))->
												BgL_stackzd2allocatorzd2) = ((obj_t) BFALSE), BUNSPEC);
										((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_new1134z00_1750)))->BgL_topzf3zf3) =
											((bool_t) ((bool_t) 1)), BUNSPEC);
										((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_new1134z00_1750)))->BgL_thezd2closurezd2) =
											((obj_t) BUNSPEC), BUNSPEC);
										((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_new1134z00_1750)))->BgL_effectz00) =
											((obj_t) BUNSPEC), BUNSPEC);
										((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_new1134z00_1750)))->BgL_failsafez00) =
											((obj_t) BUNSPEC), BUNSPEC);
										((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_new1134z00_1750)))->BgL_argszd2noescapezd2) =
											((obj_t) BNIL), BUNSPEC);
										((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_new1134z00_1750)))->BgL_argszd2retescapezd2) =
											((obj_t) BNIL), BUNSPEC);
										((((BgL_cfunz00_bglt) COBJECT(BgL_new1134z00_1750))->
												BgL_argszd2typezd2) =
											((obj_t) BgL_typezd2argszd2_1747), BUNSPEC);
										((((BgL_cfunz00_bglt) COBJECT(BgL_new1134z00_1750))->
												BgL_macrozf3zf3) =
											((bool_t) BgL_macrozf3zf3_65), BUNSPEC);
										((((BgL_cfunz00_bglt) COBJECT(BgL_new1134z00_1750))->
												BgL_infixzf3zf3) =
											((bool_t) BgL_infixzf3zf3_64), BUNSPEC);
										((((BgL_cfunz00_bglt) COBJECT(BgL_new1134z00_1750))->
												BgL_methodz00) = ((obj_t) BNIL), BUNSPEC);
										BgL_cfunz00_1748 = BgL_new1134z00_1750;
									}
									{	/* Ast/glo_decl.scm 334 */
										BgL_globalz00_bglt BgL_globalz00_1749;

										BgL_globalz00_1749 =
											BGl_bindzd2globalz12zc0zzast_envz00(BgL_idz00_58,
											BgL_aliasz00_59, BgL_modulez00_60,
											((BgL_valuez00_bglt) BgL_cfunz00_1748),
											CNST_TABLE_REF(10), BgL_srcez00_66);
										{	/* Ast/glo_decl.scm 338 */

											((((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt) BgL_globalz00_1749)))->
													BgL_namez00) = ((obj_t) BgL_namez00_61), BUNSPEC);
											((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
																BgL_globalz00_1749)))->BgL_typez00) =
												((BgL_typez00_bglt) BgL_typezd2reszd2_1746), BUNSPEC);
											((((BgL_globalz00_bglt) COBJECT(BgL_globalz00_1749))->
													BgL_evaluablezf3zf3) =
												((bool_t) ((bool_t) 0)), BUNSPEC);
											return BgL_globalz00_1749;
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &declare-global-cfun! */
	BgL_globalz00_bglt
		BGl_z62declarezd2globalzd2cfunz12z70zzast_glozd2declzd2(obj_t
		BgL_envz00_2079, obj_t BgL_idz00_2080, obj_t BgL_aliasz00_2081,
		obj_t BgL_modulez00_2082, obj_t BgL_namez00_2083,
		obj_t BgL_treszd2idzd2_2084, obj_t BgL_targszd2idzd2_2085,
		obj_t BgL_infixzf3zf3_2086, obj_t BgL_macrozf3zf3_2087,
		obj_t BgL_srcez00_2088, obj_t BgL_srciz00_2089)
	{
		{	/* Ast/glo_decl.scm 326 */
			return
				BGl_declarezd2globalzd2cfunz12z12zzast_glozd2declzd2(BgL_idz00_2080,
				BgL_aliasz00_2081, BgL_modulez00_2082, BgL_namez00_2083,
				BgL_treszd2idzd2_2084, BgL_targszd2idzd2_2085,
				CBOOL(BgL_infixzf3zf3_2086), CBOOL(BgL_macrozf3zf3_2087),
				BgL_srcez00_2088, BgL_srciz00_2089);
		}

	}



/* declare-global-cvar! */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_declarezd2globalzd2cvarz12z12zzast_glozd2declzd2(obj_t BgL_idz00_68,
		obj_t BgL_aliasz00_69, obj_t BgL_namez00_70, obj_t BgL_typezd2idzd2_71,
		bool_t BgL_macrozf3zf3_72, obj_t BgL_srcez00_73, obj_t BgL_srciz00_74)
	{
		{	/* Ast/glo_decl.scm 351 */
			{	/* Ast/glo_decl.scm 352 */
				obj_t BgL_locz00_1766;

				BgL_locz00_1766 =
					BGl_findzd2locationzd2zztools_locationz00(BgL_srcez00_73);
				{	/* Ast/glo_decl.scm 352 */
					obj_t BgL_lociz00_1767;

					BgL_lociz00_1767 =
						BGl_findzd2locationzf2locz20zztools_locationz00(BgL_srciz00_74,
						BgL_locz00_1766);
					{	/* Ast/glo_decl.scm 353 */
						BgL_typez00_bglt BgL_typez00_1768;

						BgL_typez00_1768 =
							BGl_usezd2foreignzd2typezf2importzd2locz12z32zztype_envz00
							(BgL_typezd2idzd2_71, BgL_locz00_1766, BgL_lociz00_1767);
						{	/* Ast/glo_decl.scm 354 */
							BgL_cvarz00_bglt BgL_cvarz00_1769;

							{	/* Ast/glo_decl.scm 355 */
								BgL_cvarz00_bglt BgL_new1136z00_1771;

								{	/* Ast/glo_decl.scm 355 */
									BgL_cvarz00_bglt BgL_new1135z00_1772;

									BgL_new1135z00_1772 =
										((BgL_cvarz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_cvarz00_bgl))));
									{	/* Ast/glo_decl.scm 355 */
										long BgL_arg1627z00_1773;

										BgL_arg1627z00_1773 =
											BGL_CLASS_NUM(BGl_cvarz00zzast_varz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1135z00_1772),
											BgL_arg1627z00_1773);
									}
									{	/* Ast/glo_decl.scm 355 */
										BgL_objectz00_bglt BgL_tmpz00_2657;

										BgL_tmpz00_2657 =
											((BgL_objectz00_bglt) BgL_new1135z00_1772);
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2657, BFALSE);
									}
									((BgL_objectz00_bglt) BgL_new1135z00_1772);
									BgL_new1136z00_1771 = BgL_new1135z00_1772;
								}
								((((BgL_cvarz00_bglt) COBJECT(BgL_new1136z00_1771))->
										BgL_macrozf3zf3) = ((bool_t) BgL_macrozf3zf3_72), BUNSPEC);
								BgL_cvarz00_1769 = BgL_new1136z00_1771;
							}
							{	/* Ast/glo_decl.scm 355 */
								BgL_globalz00_bglt BgL_globalz00_1770;

								BgL_globalz00_1770 =
									BGl_bindzd2globalz12zc0zzast_envz00(BgL_idz00_68,
									BgL_aliasz00_69, CNST_TABLE_REF(10),
									((BgL_valuez00_bglt) BgL_cvarz00_1769), CNST_TABLE_REF(10),
									BgL_srcez00_73);
								{	/* Ast/glo_decl.scm 356 */

									((((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_globalz00_1770)))->
											BgL_namez00) = ((obj_t) BgL_namez00_70), BUNSPEC);
									((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
														BgL_globalz00_1770)))->BgL_typez00) =
										((BgL_typez00_bglt) BgL_typez00_1768), BUNSPEC);
									((((BgL_globalz00_bglt) COBJECT(BgL_globalz00_1770))->
											BgL_evaluablezf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
									return BgL_globalz00_1770;
								}
							}
						}
					}
				}
			}
		}

	}



/* &declare-global-cvar! */
	BgL_globalz00_bglt
		BGl_z62declarezd2globalzd2cvarz12z70zzast_glozd2declzd2(obj_t
		BgL_envz00_2090, obj_t BgL_idz00_2091, obj_t BgL_aliasz00_2092,
		obj_t BgL_namez00_2093, obj_t BgL_typezd2idzd2_2094,
		obj_t BgL_macrozf3zf3_2095, obj_t BgL_srcez00_2096, obj_t BgL_srciz00_2097)
	{
		{	/* Ast/glo_decl.scm 351 */
			return
				BGl_declarezd2globalzd2cvarz12z12zzast_glozd2declzd2(BgL_idz00_2091,
				BgL_aliasz00_2092, BgL_namez00_2093, BgL_typezd2idzd2_2094,
				CBOOL(BgL_macrozf3zf3_2095), BgL_srcez00_2096, BgL_srciz00_2097);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_glozd2declzd2(void)
	{
		{	/* Ast/glo_decl.scm 17 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_glozd2declzd2(void)
	{
		{	/* Ast/glo_decl.scm 17 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_glozd2declzd2(void)
	{
		{	/* Ast/glo_decl.scm 17 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_glozd2declzd2(void)
	{
		{	/* Ast/glo_decl.scm 17 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1772z00zzast_glozd2declzd2));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1772z00zzast_glozd2declzd2));
			BGl_modulezd2initializa7ationz75zztools_argsz00(47102372L,
				BSTRING_TO_STRING(BGl_string1772z00zzast_glozd2declzd2));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1772z00zzast_glozd2declzd2));
			BGl_modulezd2initializa7ationz75zztools_dssslz00(275867955L,
				BSTRING_TO_STRING(BGl_string1772z00zzast_glozd2declzd2));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1772z00zzast_glozd2declzd2));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1772z00zzast_glozd2declzd2));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1772z00zzast_glozd2declzd2));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1772z00zzast_glozd2declzd2));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1772z00zzast_glozd2declzd2));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1772z00zzast_glozd2declzd2));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1772z00zzast_glozd2declzd2));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1772z00zzast_glozd2declzd2));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1772z00zzast_glozd2declzd2));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1772z00zzast_glozd2declzd2));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1772z00zzast_glozd2declzd2));
			return
				BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string1772z00zzast_glozd2declzd2));
		}

	}

#ifdef __cplusplus
}
#endif
