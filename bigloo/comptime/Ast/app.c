/*===========================================================================*/
/*   (Ast/app.scm)                                                           */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/app.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_AST_APP_TYPE_DEFINITIONS
#define BGL_AST_APP_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_literalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}                 *BgL_literalz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_privatez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
	}                 *BgL_privatez00_bglt;

	typedef struct BgL_vallocz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                *BgL_vallocz00_bglt;

	typedef struct BgL_vrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}              *BgL_vrefz00_bglt;

	typedef struct BgL_vsetz12z12_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}                 *BgL_vsetz12z12_bglt;

	typedef struct BgL_vlengthz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_ftypez00;
	}                 *BgL_vlengthz00_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_tvecz00_bgl
	{
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}              *BgL_tvecz00_bglt;


#endif													// BGL_AST_APP_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	extern obj_t BGl_dssslzd2keyzd2argszd2sortzd2zztools_dssslz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzast_appz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_listzd2tailzd2zz__r4_pairs_and_lists_6_3z00(obj_t, long);
	BGL_IMPORT bool_t BGl_stringzd2suffixzf3z21zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_closurez00zzast_nodez00;
	extern obj_t BGl_userzd2errorzf2locationz20zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_funz00zzast_varz00;
	extern bool_t BGl_sfunzd2optionalzf3z21zzast_varz00(obj_t);
	extern obj_t BGl_findzd2locationzf2locz20zztools_locationz00(obj_t, obj_t);
	extern obj_t BGl_vlengthz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_applicationzd2ze3nodez31zzast_appz00(obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_za2voidza2z00zztype_cachez00;
	extern obj_t BGl_atomz00zzast_nodez00;
	extern obj_t BGl_sfunz00zzast_varz00;
	extern obj_t BGl_fastzd2idzd2ofzd2idzd2zzast_identz00(obj_t, obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static BgL_nodez00_bglt BGl_z62makezd2appzd2nodez62zzast_appz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_makezd2fxzd2appzd2nodezd2zzast_appz00(obj_t, BgL_varz00_bglt,
		obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzast_appz00(void);
	static obj_t BGl_objectzd2initzd2zzast_appz00(void);
	BGL_IMPORT obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_bignum_mul(obj_t, obj_t);
	extern obj_t BGl_comptimezd2expandzd2zzexpand_epsz00(obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	static bool_t BGl_atomiczf3ze70z14zzast_appz00(obj_t);
	BGL_IMPORT obj_t BGl_dropz00zz__r4_pairs_and_lists_6_3z00(obj_t, long);
	BGL_IMPORT obj_t bgl_bignum_add(obj_t, obj_t);
	static obj_t BGl_makezd2argszd2listze70ze7zzast_appz00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzast_appz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzast_appz00(void);
	BGL_IMPORT obj_t BGl_keywordzd2ze3symbolz31zz__r4_symbols_6_4z00(obj_t);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	static obj_t BGl_makezd2specialzd2appzd2nodezd2zzast_appz00(obj_t,
		BgL_varz00_bglt, obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_long_to_bignum(long);
	extern obj_t BGl_varz00zzast_nodez00;
	extern bool_t BGl_sfunzd2keyzf3z21zzast_varz00(obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62applicationzd2ze3nodez53zzast_appz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern BgL_typez00_bglt BGl_getzd2typezd2atomz00zztype_typeofz00(obj_t);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	static obj_t BGl_z62correctzd2arityzd2appzf3z91zzast_appz00(obj_t, obj_t,
		obj_t);
	extern BgL_typez00_bglt
		BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	BGL_EXPORTED_DECL BgL_nodez00_bglt BGl_makezd2appzd2nodez00zzast_appz00(obj_t,
		obj_t, obj_t, BgL_varz00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_takez00zz__r4_pairs_and_lists_6_3z00(obj_t, long);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzast_appz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_epsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_dssslz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_privatez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_letz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztvector_tvectorz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_cfunz00zzast_varz00;
	BGL_IMPORT obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	extern obj_t BGl_vrefz00zzast_nodez00;
	static obj_t BGl_makezd2thezd2appze70ze7zzast_appz00(obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzast_appz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_appz00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	static BgL_nodez00_bglt BGl_makezd2vazd2appzd2nodezd2zzast_appz00(long, obj_t,
		obj_t, BgL_varz00_bglt, obj_t);
	extern BgL_nodez00_bglt BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00(obj_t,
		obj_t, obj_t);
	extern BgL_localz00_bglt
		BGl_makezd2userzd2localzd2svarzd2zzast_localz00(obj_t, BgL_typez00_bglt);
	static obj_t BGl_importedzd2moduleszd2initz00zzast_appz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzast_appz00(void);
	extern obj_t BGl_tvecz00zztvector_tvectorz00;
	extern obj_t BGl_parsezd2idzd2zzast_identz00(obj_t, obj_t);
	extern obj_t BGl_vallocz00zzast_nodez00;
	static BgL_nodez00_bglt BGl_makezd2keyszd2appzd2nodezd2zzast_appz00(obj_t,
		obj_t, obj_t, BgL_varz00_bglt, obj_t);
	extern obj_t BGl_letzd2symzd2zzast_letz00(void);
	extern BgL_nodez00_bglt BGl_sexpzd2ze3nodez31zzast_sexpz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2unspecza2z00zztype_cachez00;
	extern obj_t BGl_userzd2warningzf2locationz20zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static BgL_nodez00_bglt
		BGl_makezd2optionalszd2appzd2nodezd2zzast_appz00(obj_t, obj_t, obj_t,
		BgL_varz00_bglt, obj_t);
	extern obj_t BGl_literalz00zzast_nodez00;
	extern obj_t BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00(obj_t);
	static BgL_nodez00_bglt
		BGl_cleanzd2userzd2nodez12z12zzast_appz00(BgL_nodez00_bglt);
	static obj_t BGl_loopze70ze7zzast_appz00(obj_t, BgL_variablez00_bglt, obj_t);
	static BgL_nodezf2effectzf2_bglt BGl_loopze71ze7zzast_appz00(obj_t,
		BgL_sfunz00_bglt, BgL_varz00_bglt, BgL_variablez00_bglt, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_correctzd2arityzd2appzf3zf3zzast_appz00(BgL_variablez00_bglt, obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_globalz00zzast_varz00;
	static long BGl_checkzd2userzd2appz00zzast_appz00(BgL_nodez00_bglt, obj_t);
	extern obj_t BGl_vsetz12z12zzast_nodez00;
	extern obj_t BGl_compilezd2expandzd2zzexpand_epsz00(obj_t);
	static obj_t __cnst[22];


	   
		 
		DEFINE_STRING(BGl_string2334z00zzast_appz00,
		BgL_bgl_string2334za700za7za7a2353za7, "] args expected, ", 17);
	      DEFINE_STRING(BGl_string2335z00zzast_appz00,
		BgL_bgl_string2335za700za7za7a2354za7, "..", 2);
	      DEFINE_STRING(BGl_string2336z00zzast_appz00,
		BgL_bgl_string2336za700za7za7a2355za7, "[", 1);
	      DEFINE_STRING(BGl_string2337z00zzast_appz00,
		BgL_bgl_string2337za700za7za7a2356za7, " arg(s) expected, ", 18);
	      DEFINE_STRING(BGl_string2338z00zzast_appz00,
		BgL_bgl_string2338za700za7za7a2357za7, " or more arg(s) expected, ", 26);
	      DEFINE_STRING(BGl_string2339z00zzast_appz00,
		BgL_bgl_string2339za700za7za7a2358za7, " provided", 9);
	      DEFINE_STRING(BGl_string2340z00zzast_appz00,
		BgL_bgl_string2340za700za7za7a2359za7, "Illegal application: ", 21);
	      DEFINE_STRING(BGl_string2341z00zzast_appz00,
		BgL_bgl_string2341za700za7za7a2360za7, "Illegal duplicated key", 22);
	      DEFINE_STRING(BGl_string2342z00zzast_appz00,
		BgL_bgl_string2342za700za7za7a2361za7, "Illegal keyword(s) argument(s)",
		30);
	      DEFINE_STRING(BGl_string2343z00zzast_appz00,
		BgL_bgl_string2343za700za7za7a2362za7, "Illegal keyword application", 27);
	      DEFINE_STRING(BGl_string2344z00zzast_appz00,
		BgL_bgl_string2344za700za7za7a2363za7, "($1)", 4);
	      DEFINE_STRING(BGl_string2345z00zzast_appz00,
		BgL_bgl_string2345za700za7za7a2364za7, "($1,$2)", 7);
	      DEFINE_STRING(BGl_string2346z00zzast_appz00,
		BgL_bgl_string2346za700za7za7a2365za7, "-ur", 3);
	      DEFINE_STRING(BGl_string2347z00zzast_appz00,
		BgL_bgl_string2347za700za7za7a2366za7, "($1,$2,$3)", 10);
	      DEFINE_STRING(BGl_string2348z00zzast_appz00,
		BgL_bgl_string2348za700za7za7a2367za7, "make-special-app-node", 21);
	      DEFINE_STRING(BGl_string2349z00zzast_appz00,
		BgL_bgl_string2349za700za7za7a2368za7, "Illegal application", 19);
	      DEFINE_STRING(BGl_string2350z00zzast_appz00,
		BgL_bgl_string2350za700za7za7a2369za7, "ast_app", 7);
	      DEFINE_STRING(BGl_string2351z00zzast_appz00,
		BgL_bgl_string2351za700za7za7a2370za7,
		"$create-vector $vector-set-ur! $vector-set! $vector-ref-ur $vector-ref $vector-length $vector? c-eq? $cons foreign list \077\077? (c-eq? $vector? $vector-length $vector-ref $vector-ref-ur $vector-set! $vector-set-ur! $create-vector) let* @ quote arg (@ list __r4_pairs_and_lists_6_3) fun value (quote ()) app ",
		303);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2appzd2nodezd2envzd2zzast_appz00,
		BgL_bgl_za762makeza7d2appza7d22371za7,
		BGl_z62makezd2appzd2nodez62zzast_appz00, 0L, BUNSPEC, 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_correctzd2arityzd2appzf3zd2envz21zzast_appz00,
		BgL_bgl_za762correctza7d2ari2372z00,
		BGl_z62correctzd2arityzd2appzf3z91zzast_appz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_applicationzd2ze3nodezd2envze3zzast_appz00,
		BgL_bgl_za762applicationza7d2373z00,
		BGl_z62applicationzd2ze3nodez53zzast_appz00, 0L, BUNSPEC, 4);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzast_appz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzast_appz00(long
		BgL_checksumz00_4184, char *BgL_fromz00_4185)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_appz00))
				{
					BGl_requirezd2initializa7ationz75zzast_appz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_appz00();
					BGl_libraryzd2moduleszd2initz00zzast_appz00();
					BGl_cnstzd2initzd2zzast_appz00();
					BGl_importedzd2moduleszd2initz00zzast_appz00();
					return BGl_methodzd2initzd2zzast_appz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_appz00(void)
	{
		{	/* Ast/app.scm 14 */
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "ast_app");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_app");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "ast_app");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "ast_app");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_app");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "ast_app");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "ast_app");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "ast_app");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L, "ast_app");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "ast_app");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"ast_app");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_appz00(void)
	{
		{	/* Ast/app.scm 14 */
			{	/* Ast/app.scm 14 */
				obj_t BgL_cportz00_4173;

				{	/* Ast/app.scm 14 */
					obj_t BgL_stringz00_4180;

					BgL_stringz00_4180 = BGl_string2351z00zzast_appz00;
					{	/* Ast/app.scm 14 */
						obj_t BgL_startz00_4181;

						BgL_startz00_4181 = BINT(0L);
						{	/* Ast/app.scm 14 */
							obj_t BgL_endz00_4182;

							BgL_endz00_4182 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_4180)));
							{	/* Ast/app.scm 14 */

								BgL_cportz00_4173 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_4180, BgL_startz00_4181, BgL_endz00_4182);
				}}}}
				{
					long BgL_iz00_4174;

					BgL_iz00_4174 = 21L;
				BgL_loopz00_4175:
					if ((BgL_iz00_4174 == -1L))
						{	/* Ast/app.scm 14 */
							return BUNSPEC;
						}
					else
						{	/* Ast/app.scm 14 */
							{	/* Ast/app.scm 14 */
								obj_t BgL_arg2352z00_4176;

								{	/* Ast/app.scm 14 */

									{	/* Ast/app.scm 14 */
										obj_t BgL_locationz00_4178;

										BgL_locationz00_4178 = BBOOL(((bool_t) 0));
										{	/* Ast/app.scm 14 */

											BgL_arg2352z00_4176 =
												BGl_readz00zz__readerz00(BgL_cportz00_4173,
												BgL_locationz00_4178);
										}
									}
								}
								{	/* Ast/app.scm 14 */
									int BgL_tmpz00_4214;

									BgL_tmpz00_4214 = (int) (BgL_iz00_4174);
									CNST_TABLE_SET(BgL_tmpz00_4214, BgL_arg2352z00_4176);
							}}
							{	/* Ast/app.scm 14 */
								int BgL_auxz00_4179;

								BgL_auxz00_4179 = (int) ((BgL_iz00_4174 - 1L));
								{
									long BgL_iz00_4219;

									BgL_iz00_4219 = (long) (BgL_auxz00_4179);
									BgL_iz00_4174 = BgL_iz00_4219;
									goto BgL_loopz00_4175;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_appz00(void)
	{
		{	/* Ast/app.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzast_appz00(obj_t BgL_l1z00_1, obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1441;

				BgL_headz00_1441 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1442;
					obj_t BgL_tailz00_1443;

					BgL_prevz00_1442 = BgL_headz00_1441;
					BgL_tailz00_1443 = BgL_l1z00_1;
				BgL_loopz00_1444:
					if (PAIRP(BgL_tailz00_1443))
						{
							obj_t BgL_newzd2prevzd2_1446;

							BgL_newzd2prevzd2_1446 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1443), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1442, BgL_newzd2prevzd2_1446);
							{
								obj_t BgL_tailz00_4229;
								obj_t BgL_prevz00_4228;

								BgL_prevz00_4228 = BgL_newzd2prevzd2_1446;
								BgL_tailz00_4229 = CDR(BgL_tailz00_1443);
								BgL_tailz00_1443 = BgL_tailz00_4229;
								BgL_prevz00_1442 = BgL_prevz00_4228;
								goto BgL_loopz00_1444;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1441);
				}
			}
		}

	}



/* correct-arity-app? */
	BGL_EXPORTED_DEF bool_t
		BGl_correctzd2arityzd2appzf3zf3zzast_appz00(BgL_variablez00_bglt
		BgL_varz00_3, obj_t BgL_argsz00_4)
	{
		{	/* Ast/app.scm 40 */
			{	/* Ast/app.scm 41 */
				long BgL_nbzd2argszd2_1450;

				BgL_nbzd2argszd2_1450 = bgl_list_length(BgL_argsz00_4);
				{	/* Ast/app.scm 42 */
					long BgL_arityz00_1451;

					BgL_arityz00_1451 =
						(((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt)
									(((BgL_variablez00_bglt) COBJECT(BgL_varz00_3))->
										BgL_valuez00))))->BgL_arityz00);
					{	/* Ast/app.scm 43 */

						if ((BgL_arityz00_1451 == -1L))
							{	/* Ast/app.scm 45 */
								return ((bool_t) 1);
							}
						else
							{	/* Ast/app.scm 45 */
								if ((BgL_arityz00_1451 >= 0L))
									{	/* Ast/app.scm 47 */
										return (BgL_arityz00_1451 == BgL_nbzd2argszd2_1450);
									}
								else
									{	/* Ast/app.scm 47 */
										return
											(
											(NEG(BgL_arityz00_1451) -
												(BgL_nbzd2argszd2_1450 + 1L)) <= 0L);
									}
							}
					}
				}
			}
		}

	}



/* &correct-arity-app? */
	obj_t BGl_z62correctzd2arityzd2appzf3z91zzast_appz00(obj_t BgL_envz00_4149,
		obj_t BgL_varz00_4150, obj_t BgL_argsz00_4151)
	{
		{	/* Ast/app.scm 40 */
			return
				BBOOL(BGl_correctzd2arityzd2appzf3zf3zzast_appz00(
					((BgL_variablez00_bglt) BgL_varz00_4150), BgL_argsz00_4151));
		}

	}



/* clean-user-node! */
	BgL_nodez00_bglt BGl_cleanzd2userzd2nodez12z12zzast_appz00(BgL_nodez00_bglt
		BgL_nodez00_5)
	{
		{	/* Ast/app.scm 58 */
			{
				BgL_nodez00_bglt BgL_walkz00_1459;

				BgL_walkz00_1459 = BgL_nodez00_5;
			BgL_zc3z04anonymousza31378ze3z87_1460:
				{	/* Ast/app.scm 60 */
					bool_t BgL_test2379z00_4248;

					{	/* Ast/app.scm 60 */
						obj_t BgL_classz00_2546;

						BgL_classz00_2546 = BGl_letzd2varzd2zzast_nodez00;
						{	/* Ast/app.scm 60 */
							BgL_objectz00_bglt BgL_arg1807z00_2548;

							{	/* Ast/app.scm 60 */
								obj_t BgL_tmpz00_4249;

								BgL_tmpz00_4249 =
									((obj_t) ((BgL_objectz00_bglt) BgL_walkz00_1459));
								BgL_arg1807z00_2548 = (BgL_objectz00_bglt) (BgL_tmpz00_4249);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Ast/app.scm 60 */
									long BgL_idxz00_2554;

									BgL_idxz00_2554 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2548);
									BgL_test2379z00_4248 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2554 + 3L)) == BgL_classz00_2546);
								}
							else
								{	/* Ast/app.scm 60 */
									bool_t BgL_res2288z00_2579;

									{	/* Ast/app.scm 60 */
										obj_t BgL_oclassz00_2562;

										{	/* Ast/app.scm 60 */
											obj_t BgL_arg1815z00_2570;
											long BgL_arg1816z00_2571;

											BgL_arg1815z00_2570 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Ast/app.scm 60 */
												long BgL_arg1817z00_2572;

												BgL_arg1817z00_2572 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2548);
												BgL_arg1816z00_2571 =
													(BgL_arg1817z00_2572 - OBJECT_TYPE);
											}
											BgL_oclassz00_2562 =
												VECTOR_REF(BgL_arg1815z00_2570, BgL_arg1816z00_2571);
										}
										{	/* Ast/app.scm 60 */
											bool_t BgL__ortest_1115z00_2563;

											BgL__ortest_1115z00_2563 =
												(BgL_classz00_2546 == BgL_oclassz00_2562);
											if (BgL__ortest_1115z00_2563)
												{	/* Ast/app.scm 60 */
													BgL_res2288z00_2579 = BgL__ortest_1115z00_2563;
												}
											else
												{	/* Ast/app.scm 60 */
													long BgL_odepthz00_2564;

													{	/* Ast/app.scm 60 */
														obj_t BgL_arg1804z00_2565;

														BgL_arg1804z00_2565 = (BgL_oclassz00_2562);
														BgL_odepthz00_2564 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2565);
													}
													if ((3L < BgL_odepthz00_2564))
														{	/* Ast/app.scm 60 */
															obj_t BgL_arg1802z00_2567;

															{	/* Ast/app.scm 60 */
																obj_t BgL_arg1803z00_2568;

																BgL_arg1803z00_2568 = (BgL_oclassz00_2562);
																BgL_arg1802z00_2567 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2568,
																	3L);
															}
															BgL_res2288z00_2579 =
																(BgL_arg1802z00_2567 == BgL_classz00_2546);
														}
													else
														{	/* Ast/app.scm 60 */
															BgL_res2288z00_2579 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2379z00_4248 = BgL_res2288z00_2579;
								}
						}
					}
					if (BgL_test2379z00_4248)
						{	/* Ast/app.scm 60 */
							{	/* Ast/app.scm 62 */
								obj_t BgL_g1296z00_1462;

								BgL_g1296z00_1462 =
									(((BgL_letzd2varzd2_bglt) COBJECT(
											((BgL_letzd2varzd2_bglt) BgL_walkz00_1459)))->
									BgL_bindingsz00);
								{
									obj_t BgL_l1294z00_1464;

									BgL_l1294z00_1464 = BgL_g1296z00_1462;
								BgL_zc3z04anonymousza31383ze3z87_1465:
									if (PAIRP(BgL_l1294z00_1464))
										{	/* Ast/app.scm 64 */
											{	/* Ast/app.scm 63 */
												obj_t BgL_bindingz00_1467;

												BgL_bindingz00_1467 = CAR(BgL_l1294z00_1464);
												{	/* Ast/app.scm 63 */
													obj_t BgL_arg1408z00_1468;

													BgL_arg1408z00_1468 =
														CAR(((obj_t) BgL_bindingz00_1467));
													((((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt)
																			BgL_arg1408z00_1468))))->BgL_userzf3zf3) =
														((bool_t) ((bool_t) 0)), BUNSPEC);
												}
											}
											{
												obj_t BgL_l1294z00_4282;

												BgL_l1294z00_4282 = CDR(BgL_l1294z00_1464);
												BgL_l1294z00_1464 = BgL_l1294z00_4282;
												goto BgL_zc3z04anonymousza31383ze3z87_1465;
											}
										}
									else
										{	/* Ast/app.scm 64 */
											((bool_t) 1);
										}
								}
							}
							{	/* Ast/app.scm 65 */
								BgL_nodez00_bglt BgL_arg1421z00_1471;

								BgL_arg1421z00_1471 =
									(((BgL_letzd2varzd2_bglt) COBJECT(
											((BgL_letzd2varzd2_bglt) BgL_walkz00_1459)))->
									BgL_bodyz00);
								{
									BgL_nodez00_bglt BgL_walkz00_4286;

									BgL_walkz00_4286 = BgL_arg1421z00_1471;
									BgL_walkz00_1459 = BgL_walkz00_4286;
									goto BgL_zc3z04anonymousza31378ze3z87_1460;
								}
							}
						}
					else
						{	/* Ast/app.scm 60 */
							return BgL_nodez00_5;
						}
				}
			}
		}

	}



/* application->node */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_applicationzd2ze3nodez31zzast_appz00(obj_t BgL_expz00_6,
		obj_t BgL_stackz00_7, obj_t BgL_locz00_8, obj_t BgL_sitez00_9)
	{
		{	/* Ast/app.scm 74 */
			{
				obj_t BgL_expz00_1581;
				obj_t BgL_expz00_1597;
				obj_t BgL_locz00_1598;
				BgL_nodez00_bglt BgL_funz00_1599;
				obj_t BgL_argsz00_1600;

				{	/* Ast/app.scm 142 */
					obj_t BgL_locz00_1476;

					BgL_locz00_1476 =
						BGl_findzd2locationzf2locz20zztools_locationz00(BgL_expz00_6,
						BgL_locz00_8);
					{	/* Ast/app.scm 142 */
						obj_t BgL_errzd2nbzd2_1477;

						BgL_errzd2nbzd2_1477 =
							BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
						{	/* Ast/app.scm 143 */
							obj_t BgL_debugstampz00_1478;

							{	/* Ast/app.scm 144 */

								{	/* Ast/app.scm 144 */

									BgL_debugstampz00_1478 =
										BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
								}
							}
							{	/* Ast/app.scm 144 */
								BgL_nodez00_bglt BgL_funz00_1479;

								{	/* Ast/app.scm 145 */
									obj_t BgL_arg1595z00_1548;

									BgL_arg1595z00_1548 = CAR(((obj_t) BgL_expz00_6));
									BgL_funz00_1479 =
										BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_arg1595z00_1548,
										BgL_stackz00_7, BgL_locz00_1476, CNST_TABLE_REF(0));
								}
								{	/* Ast/app.scm 145 */
									bool_t BgL_funzd2errzf3z21_1480;

									BgL_funzd2errzf3z21_1480 =
										(
										(long)
										CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) >
										(long) CINT(BgL_errzd2nbzd2_1477));
									{	/* Ast/app.scm 146 */

										{	/* Ast/app.scm 147 */
											bool_t BgL_test2384z00_4296;

											{	/* Ast/app.scm 147 */
												bool_t BgL_test2385z00_4297;

												BgL_expz00_1581 = BgL_expz00_6;
												{
													obj_t BgL_expz00_1584;

													BgL_expz00_1584 = BgL_expz00_1581;
												BgL_zc3z04anonymousza31632ze3z87_1585:
													if (NULLP(BgL_expz00_1584))
														{	/* Ast/app.scm 94 */
															BgL_test2385z00_4297 = ((bool_t) 1);
														}
													else
														{	/* Ast/app.scm 94 */
															if (BGl_atomiczf3ze70z14zzast_appz00(CAR(
																		((obj_t) BgL_expz00_1584))))
																{	/* Ast/app.scm 97 */
																	obj_t BgL_arg1646z00_1589;

																	BgL_arg1646z00_1589 =
																		CDR(((obj_t) BgL_expz00_1584));
																	{
																		obj_t BgL_expz00_4306;

																		BgL_expz00_4306 = BgL_arg1646z00_1589;
																		BgL_expz00_1584 = BgL_expz00_4306;
																		goto BgL_zc3z04anonymousza31632ze3z87_1585;
																	}
																}
															else
																{	/* Ast/app.scm 99 */
																	bool_t BgL_test2388z00_4307;

																	{	/* Ast/app.scm 99 */
																		bool_t BgL__ortest_1108z00_1592;

																		{	/* Ast/app.scm 99 */
																			obj_t BgL_arg1654z00_1594;

																			BgL_arg1654z00_1594 =
																				CAR(((obj_t) BgL_expz00_1584));
																			{	/* Ast/app.scm 99 */
																				obj_t BgL_classz00_2597;

																				BgL_classz00_2597 =
																					BGl_atomz00zzast_nodez00;
																				if (BGL_OBJECTP(BgL_arg1654z00_1594))
																					{	/* Ast/app.scm 99 */
																						BgL_objectz00_bglt
																							BgL_arg1807z00_2599;
																						BgL_arg1807z00_2599 =
																							(BgL_objectz00_bglt)
																							(BgL_arg1654z00_1594);
																						if (BGL_CONDEXPAND_ISA_ARCH64())
																							{	/* Ast/app.scm 99 */
																								long BgL_idxz00_2605;

																								BgL_idxz00_2605 =
																									BGL_OBJECT_INHERITANCE_NUM
																									(BgL_arg1807z00_2599);
																								BgL__ortest_1108z00_1592 =
																									(VECTOR_REF
																									(BGl_za2inheritancesza2z00zz__objectz00,
																										(BgL_idxz00_2605 + 2L)) ==
																									BgL_classz00_2597);
																							}
																						else
																							{	/* Ast/app.scm 99 */
																								bool_t BgL_res2289z00_2630;

																								{	/* Ast/app.scm 99 */
																									obj_t BgL_oclassz00_2613;

																									{	/* Ast/app.scm 99 */
																										obj_t BgL_arg1815z00_2621;
																										long BgL_arg1816z00_2622;

																										BgL_arg1815z00_2621 =
																											(BGl_za2classesza2z00zz__objectz00);
																										{	/* Ast/app.scm 99 */
																											long BgL_arg1817z00_2623;

																											BgL_arg1817z00_2623 =
																												BGL_OBJECT_CLASS_NUM
																												(BgL_arg1807z00_2599);
																											BgL_arg1816z00_2622 =
																												(BgL_arg1817z00_2623 -
																												OBJECT_TYPE);
																										}
																										BgL_oclassz00_2613 =
																											VECTOR_REF
																											(BgL_arg1815z00_2621,
																											BgL_arg1816z00_2622);
																									}
																									{	/* Ast/app.scm 99 */
																										bool_t
																											BgL__ortest_1115z00_2614;
																										BgL__ortest_1115z00_2614 =
																											(BgL_classz00_2597 ==
																											BgL_oclassz00_2613);
																										if (BgL__ortest_1115z00_2614)
																											{	/* Ast/app.scm 99 */
																												BgL_res2289z00_2630 =
																													BgL__ortest_1115z00_2614;
																											}
																										else
																											{	/* Ast/app.scm 99 */
																												long BgL_odepthz00_2615;

																												{	/* Ast/app.scm 99 */
																													obj_t
																														BgL_arg1804z00_2616;
																													BgL_arg1804z00_2616 =
																														(BgL_oclassz00_2613);
																													BgL_odepthz00_2615 =
																														BGL_CLASS_DEPTH
																														(BgL_arg1804z00_2616);
																												}
																												if (
																													(2L <
																														BgL_odepthz00_2615))
																													{	/* Ast/app.scm 99 */
																														obj_t
																															BgL_arg1802z00_2618;
																														{	/* Ast/app.scm 99 */
																															obj_t
																																BgL_arg1803z00_2619;
																															BgL_arg1803z00_2619
																																=
																																(BgL_oclassz00_2613);
																															BgL_arg1802z00_2618
																																=
																																BGL_CLASS_ANCESTORS_REF
																																(BgL_arg1803z00_2619,
																																2L);
																														}
																														BgL_res2289z00_2630
																															=
																															(BgL_arg1802z00_2618
																															==
																															BgL_classz00_2597);
																													}
																												else
																													{	/* Ast/app.scm 99 */
																														BgL_res2289z00_2630
																															= ((bool_t) 0);
																													}
																											}
																									}
																								}
																								BgL__ortest_1108z00_1592 =
																									BgL_res2289z00_2630;
																							}
																					}
																				else
																					{	/* Ast/app.scm 99 */
																						BgL__ortest_1108z00_1592 =
																							((bool_t) 0);
																					}
																			}
																		}
																		if (BgL__ortest_1108z00_1592)
																			{	/* Ast/app.scm 99 */
																				BgL_test2388z00_4307 =
																					BgL__ortest_1108z00_1592;
																			}
																		else
																			{	/* Ast/app.scm 99 */
																				obj_t BgL_arg1651z00_1593;

																				BgL_arg1651z00_1593 =
																					CAR(((obj_t) BgL_expz00_1584));
																				{	/* Ast/app.scm 99 */
																					obj_t BgL_classz00_2632;

																					BgL_classz00_2632 =
																						BGl_varz00zzast_nodez00;
																					if (BGL_OBJECTP(BgL_arg1651z00_1593))
																						{	/* Ast/app.scm 99 */
																							BgL_objectz00_bglt
																								BgL_arg1807z00_2634;
																							BgL_arg1807z00_2634 =
																								(BgL_objectz00_bglt)
																								(BgL_arg1651z00_1593);
																							if (BGL_CONDEXPAND_ISA_ARCH64())
																								{	/* Ast/app.scm 99 */
																									long BgL_idxz00_2640;

																									BgL_idxz00_2640 =
																										BGL_OBJECT_INHERITANCE_NUM
																										(BgL_arg1807z00_2634);
																									BgL_test2388z00_4307 =
																										(VECTOR_REF
																										(BGl_za2inheritancesza2z00zz__objectz00,
																											(BgL_idxz00_2640 + 2L)) ==
																										BgL_classz00_2632);
																								}
																							else
																								{	/* Ast/app.scm 99 */
																									bool_t BgL_res2290z00_2665;

																									{	/* Ast/app.scm 99 */
																										obj_t BgL_oclassz00_2648;

																										{	/* Ast/app.scm 99 */
																											obj_t BgL_arg1815z00_2656;
																											long BgL_arg1816z00_2657;

																											BgL_arg1815z00_2656 =
																												(BGl_za2classesza2z00zz__objectz00);
																											{	/* Ast/app.scm 99 */
																												long
																													BgL_arg1817z00_2658;
																												BgL_arg1817z00_2658 =
																													BGL_OBJECT_CLASS_NUM
																													(BgL_arg1807z00_2634);
																												BgL_arg1816z00_2657 =
																													(BgL_arg1817z00_2658 -
																													OBJECT_TYPE);
																											}
																											BgL_oclassz00_2648 =
																												VECTOR_REF
																												(BgL_arg1815z00_2656,
																												BgL_arg1816z00_2657);
																										}
																										{	/* Ast/app.scm 99 */
																											bool_t
																												BgL__ortest_1115z00_2649;
																											BgL__ortest_1115z00_2649 =
																												(BgL_classz00_2632 ==
																												BgL_oclassz00_2648);
																											if (BgL__ortest_1115z00_2649)
																												{	/* Ast/app.scm 99 */
																													BgL_res2290z00_2665 =
																														BgL__ortest_1115z00_2649;
																												}
																											else
																												{	/* Ast/app.scm 99 */
																													long
																														BgL_odepthz00_2650;
																													{	/* Ast/app.scm 99 */
																														obj_t
																															BgL_arg1804z00_2651;
																														BgL_arg1804z00_2651
																															=
																															(BgL_oclassz00_2648);
																														BgL_odepthz00_2650 =
																															BGL_CLASS_DEPTH
																															(BgL_arg1804z00_2651);
																													}
																													if (
																														(2L <
																															BgL_odepthz00_2650))
																														{	/* Ast/app.scm 99 */
																															obj_t
																																BgL_arg1802z00_2653;
																															{	/* Ast/app.scm 99 */
																																obj_t
																																	BgL_arg1803z00_2654;
																																BgL_arg1803z00_2654
																																	=
																																	(BgL_oclassz00_2648);
																																BgL_arg1802z00_2653
																																	=
																																	BGL_CLASS_ANCESTORS_REF
																																	(BgL_arg1803z00_2654,
																																	2L);
																															}
																															BgL_res2290z00_2665
																																=
																																(BgL_arg1802z00_2653
																																==
																																BgL_classz00_2632);
																														}
																													else
																														{	/* Ast/app.scm 99 */
																															BgL_res2290z00_2665
																																= ((bool_t) 0);
																														}
																												}
																										}
																									}
																									BgL_test2388z00_4307 =
																										BgL_res2290z00_2665;
																								}
																						}
																					else
																						{	/* Ast/app.scm 99 */
																							BgL_test2388z00_4307 =
																								((bool_t) 0);
																						}
																				}
																			}
																	}
																	if (BgL_test2388z00_4307)
																		{	/* Ast/app.scm 100 */
																			obj_t BgL_arg1650z00_1591;

																			BgL_arg1650z00_1591 =
																				CDR(((obj_t) BgL_expz00_1584));
																			{
																				obj_t BgL_expz00_4359;

																				BgL_expz00_4359 = BgL_arg1650z00_1591;
																				BgL_expz00_1584 = BgL_expz00_4359;
																				goto
																					BgL_zc3z04anonymousza31632ze3z87_1585;
																			}
																		}
																	else
																		{	/* Ast/app.scm 99 */
																			BgL_test2385z00_4297 = ((bool_t) 0);
																		}
																}
														}
												}
												if (BgL_test2385z00_4297)
													{	/* Ast/app.scm 147 */
														obj_t BgL_classz00_2742;

														BgL_classz00_2742 = BGl_varz00zzast_nodez00;
														{	/* Ast/app.scm 147 */
															BgL_objectz00_bglt BgL_arg1807z00_2744;

															{	/* Ast/app.scm 147 */
																obj_t BgL_tmpz00_4360;

																BgL_tmpz00_4360 =
																	((obj_t)
																	((BgL_objectz00_bglt) BgL_funz00_1479));
																BgL_arg1807z00_2744 =
																	(BgL_objectz00_bglt) (BgL_tmpz00_4360);
															}
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Ast/app.scm 147 */
																	long BgL_idxz00_2750;

																	BgL_idxz00_2750 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_2744);
																	BgL_test2384z00_4296 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_2750 + 2L)) ==
																		BgL_classz00_2742);
																}
															else
																{	/* Ast/app.scm 147 */
																	bool_t BgL_res2292z00_2775;

																	{	/* Ast/app.scm 147 */
																		obj_t BgL_oclassz00_2758;

																		{	/* Ast/app.scm 147 */
																			obj_t BgL_arg1815z00_2766;
																			long BgL_arg1816z00_2767;

																			BgL_arg1815z00_2766 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Ast/app.scm 147 */
																				long BgL_arg1817z00_2768;

																				BgL_arg1817z00_2768 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_2744);
																				BgL_arg1816z00_2767 =
																					(BgL_arg1817z00_2768 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_2758 =
																				VECTOR_REF(BgL_arg1815z00_2766,
																				BgL_arg1816z00_2767);
																		}
																		{	/* Ast/app.scm 147 */
																			bool_t BgL__ortest_1115z00_2759;

																			BgL__ortest_1115z00_2759 =
																				(BgL_classz00_2742 ==
																				BgL_oclassz00_2758);
																			if (BgL__ortest_1115z00_2759)
																				{	/* Ast/app.scm 147 */
																					BgL_res2292z00_2775 =
																						BgL__ortest_1115z00_2759;
																				}
																			else
																				{	/* Ast/app.scm 147 */
																					long BgL_odepthz00_2760;

																					{	/* Ast/app.scm 147 */
																						obj_t BgL_arg1804z00_2761;

																						BgL_arg1804z00_2761 =
																							(BgL_oclassz00_2758);
																						BgL_odepthz00_2760 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_2761);
																					}
																					if ((2L < BgL_odepthz00_2760))
																						{	/* Ast/app.scm 147 */
																							obj_t BgL_arg1802z00_2763;

																							{	/* Ast/app.scm 147 */
																								obj_t BgL_arg1803z00_2764;

																								BgL_arg1803z00_2764 =
																									(BgL_oclassz00_2758);
																								BgL_arg1802z00_2763 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_2764, 2L);
																							}
																							BgL_res2292z00_2775 =
																								(BgL_arg1802z00_2763 ==
																								BgL_classz00_2742);
																						}
																					else
																						{	/* Ast/app.scm 147 */
																							BgL_res2292z00_2775 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2384z00_4296 = BgL_res2292z00_2775;
																}
														}
													}
												else
													{	/* Ast/app.scm 147 */
														BgL_test2384z00_4296 = ((bool_t) 0);
													}
											}
											if (BgL_test2384z00_4296)
												{	/* Ast/app.scm 148 */
													obj_t BgL_argsz00_1483;

													BgL_argsz00_1483 = CDR(((obj_t) BgL_expz00_6));
													{	/* Ast/app.scm 148 */
														long BgL_deltaz00_1484;

														BgL_deltaz00_1484 =
															BGl_checkzd2userzd2appz00zzast_appz00
															(BgL_funz00_1479, BgL_argsz00_1483);
														{	/* Ast/app.scm 149 */

															{	/* Ast/app.scm 151 */
																bool_t BgL_test2401z00_4386;

																{	/* Ast/app.scm 151 */
																	obj_t BgL_classz00_2777;

																	BgL_classz00_2777 = BGl_varz00zzast_nodez00;
																	{	/* Ast/app.scm 151 */
																		BgL_objectz00_bglt BgL_arg1807z00_2779;

																		{	/* Ast/app.scm 151 */
																			obj_t BgL_tmpz00_4387;

																			BgL_tmpz00_4387 =
																				((obj_t)
																				((BgL_objectz00_bglt) BgL_funz00_1479));
																			BgL_arg1807z00_2779 =
																				(BgL_objectz00_bglt) (BgL_tmpz00_4387);
																		}
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Ast/app.scm 151 */
																				long BgL_idxz00_2785;

																				BgL_idxz00_2785 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_2779);
																				BgL_test2401z00_4386 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_2785 + 2L)) ==
																					BgL_classz00_2777);
																			}
																		else
																			{	/* Ast/app.scm 151 */
																				bool_t BgL_res2293z00_2810;

																				{	/* Ast/app.scm 151 */
																					obj_t BgL_oclassz00_2793;

																					{	/* Ast/app.scm 151 */
																						obj_t BgL_arg1815z00_2801;
																						long BgL_arg1816z00_2802;

																						BgL_arg1815z00_2801 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Ast/app.scm 151 */
																							long BgL_arg1817z00_2803;

																							BgL_arg1817z00_2803 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_2779);
																							BgL_arg1816z00_2802 =
																								(BgL_arg1817z00_2803 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_2793 =
																							VECTOR_REF(BgL_arg1815z00_2801,
																							BgL_arg1816z00_2802);
																					}
																					{	/* Ast/app.scm 151 */
																						bool_t BgL__ortest_1115z00_2794;

																						BgL__ortest_1115z00_2794 =
																							(BgL_classz00_2777 ==
																							BgL_oclassz00_2793);
																						if (BgL__ortest_1115z00_2794)
																							{	/* Ast/app.scm 151 */
																								BgL_res2293z00_2810 =
																									BgL__ortest_1115z00_2794;
																							}
																						else
																							{	/* Ast/app.scm 151 */
																								long BgL_odepthz00_2795;

																								{	/* Ast/app.scm 151 */
																									obj_t BgL_arg1804z00_2796;

																									BgL_arg1804z00_2796 =
																										(BgL_oclassz00_2793);
																									BgL_odepthz00_2795 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_2796);
																								}
																								if ((2L < BgL_odepthz00_2795))
																									{	/* Ast/app.scm 151 */
																										obj_t BgL_arg1802z00_2798;

																										{	/* Ast/app.scm 151 */
																											obj_t BgL_arg1803z00_2799;

																											BgL_arg1803z00_2799 =
																												(BgL_oclassz00_2793);
																											BgL_arg1802z00_2798 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_2799,
																												2L);
																										}
																										BgL_res2293z00_2810 =
																											(BgL_arg1802z00_2798 ==
																											BgL_classz00_2777);
																									}
																								else
																									{	/* Ast/app.scm 151 */
																										BgL_res2293z00_2810 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test2401z00_4386 =
																					BgL_res2293z00_2810;
																			}
																	}
																}
																if (BgL_test2401z00_4386)
																	{	/* Ast/app.scm 151 */
																		if ((BgL_deltaz00_1484 == 0L))
																			{	/* Ast/app.scm 153 */
																				return
																					BGl_makezd2appzd2nodez00zzast_appz00
																					(BgL_stackz00_7, BgL_locz00_1476,
																					BgL_sitez00_9,
																					((BgL_varz00_bglt) BgL_funz00_1479),
																					BgL_argsz00_1483);
																			}
																		else
																			{	/* Ast/app.scm 153 */
																				BgL_expz00_1597 = BgL_expz00_6;
																				BgL_locz00_1598 = BgL_locz00_1476;
																				BgL_funz00_1599 = BgL_funz00_1479;
																				BgL_argsz00_1600 = BgL_argsz00_1483;
																				{	/* Ast/app.scm 103 */
																					BgL_variablez00_bglt BgL_varz00_1602;

																					BgL_varz00_1602 =
																						(((BgL_varz00_bglt) COBJECT(
																								((BgL_varz00_bglt)
																									BgL_funz00_1599)))->
																						BgL_variablez00);
																					{	/* Ast/app.scm 103 */
																						BgL_valuez00_bglt BgL_funz00_1603;

																						BgL_funz00_1603 =
																							(((BgL_variablez00_bglt)
																								COBJECT(BgL_varz00_1602))->
																							BgL_valuez00);
																						{	/* Ast/app.scm 105 */
																							long BgL_arityz00_1605;

																							{	/* Ast/app.scm 107 */
																								bool_t BgL_test2406z00_4417;

																								{	/* Ast/app.scm 107 */
																									obj_t BgL_classz00_2669;

																									BgL_classz00_2669 =
																										BGl_funz00zzast_varz00;
																									{	/* Ast/app.scm 107 */
																										BgL_objectz00_bglt
																											BgL_arg1807z00_2671;
																										{	/* Ast/app.scm 107 */
																											obj_t BgL_tmpz00_4418;

																											BgL_tmpz00_4418 =
																												((obj_t)
																												((BgL_objectz00_bglt)
																													BgL_funz00_1603));
																											BgL_arg1807z00_2671 =
																												(BgL_objectz00_bglt)
																												(BgL_tmpz00_4418);
																										}
																										if (BGL_CONDEXPAND_ISA_ARCH64())
																											{	/* Ast/app.scm 107 */
																												long BgL_idxz00_2677;

																												BgL_idxz00_2677 =
																													BGL_OBJECT_INHERITANCE_NUM
																													(BgL_arg1807z00_2671);
																												BgL_test2406z00_4417 =
																													(VECTOR_REF
																													(BGl_za2inheritancesza2z00zz__objectz00,
																														(BgL_idxz00_2677 +
																															2L)) ==
																													BgL_classz00_2669);
																											}
																										else
																											{	/* Ast/app.scm 107 */
																												bool_t
																													BgL_res2291z00_2702;
																												{	/* Ast/app.scm 107 */
																													obj_t
																														BgL_oclassz00_2685;
																													{	/* Ast/app.scm 107 */
																														obj_t
																															BgL_arg1815z00_2693;
																														long
																															BgL_arg1816z00_2694;
																														BgL_arg1815z00_2693
																															=
																															(BGl_za2classesza2z00zz__objectz00);
																														{	/* Ast/app.scm 107 */
																															long
																																BgL_arg1817z00_2695;
																															BgL_arg1817z00_2695
																																=
																																BGL_OBJECT_CLASS_NUM
																																(BgL_arg1807z00_2671);
																															BgL_arg1816z00_2694
																																=
																																(BgL_arg1817z00_2695
																																- OBJECT_TYPE);
																														}
																														BgL_oclassz00_2685 =
																															VECTOR_REF
																															(BgL_arg1815z00_2693,
																															BgL_arg1816z00_2694);
																													}
																													{	/* Ast/app.scm 107 */
																														bool_t
																															BgL__ortest_1115z00_2686;
																														BgL__ortest_1115z00_2686
																															=
																															(BgL_classz00_2669
																															==
																															BgL_oclassz00_2685);
																														if (BgL__ortest_1115z00_2686)
																															{	/* Ast/app.scm 107 */
																																BgL_res2291z00_2702
																																	=
																																	BgL__ortest_1115z00_2686;
																															}
																														else
																															{	/* Ast/app.scm 107 */
																																long
																																	BgL_odepthz00_2687;
																																{	/* Ast/app.scm 107 */
																																	obj_t
																																		BgL_arg1804z00_2688;
																																	BgL_arg1804z00_2688
																																		=
																																		(BgL_oclassz00_2685);
																																	BgL_odepthz00_2687
																																		=
																																		BGL_CLASS_DEPTH
																																		(BgL_arg1804z00_2688);
																																}
																																if (
																																	(2L <
																																		BgL_odepthz00_2687))
																																	{	/* Ast/app.scm 107 */
																																		obj_t
																																			BgL_arg1802z00_2690;
																																		{	/* Ast/app.scm 107 */
																																			obj_t
																																				BgL_arg1803z00_2691;
																																			BgL_arg1803z00_2691
																																				=
																																				(BgL_oclassz00_2685);
																																			BgL_arg1802z00_2690
																																				=
																																				BGL_CLASS_ANCESTORS_REF
																																				(BgL_arg1803z00_2691,
																																				2L);
																																		}
																																		BgL_res2291z00_2702
																																			=
																																			(BgL_arg1802z00_2690
																																			==
																																			BgL_classz00_2669);
																																	}
																																else
																																	{	/* Ast/app.scm 107 */
																																		BgL_res2291z00_2702
																																			=
																																			((bool_t)
																																			0);
																																	}
																															}
																													}
																												}
																												BgL_test2406z00_4417 =
																													BgL_res2291z00_2702;
																											}
																									}
																								}
																								if (BgL_test2406z00_4417)
																									{	/* Ast/app.scm 107 */
																										BgL_arityz00_1605 =
																											(((BgL_funz00_bglt)
																												COBJECT((
																														(BgL_funz00_bglt)
																														BgL_funz00_1603)))->
																											BgL_arityz00);
																									}
																								else
																									{	/* Ast/app.scm 107 */
																										BgL_arityz00_1605 = -1L;
																									}
																							}
																							{	/* Ast/app.scm 106 */
																								obj_t BgL_expectz00_1606;

																								if ((BgL_arityz00_1605 >= 0L))
																									{	/* Ast/app.scm 112 */
																										if (BGl_sfunzd2optionalzf3z21zzast_varz00(((obj_t) BgL_funz00_1603)))
																											{	/* Ast/app.scm 116 */
																												obj_t
																													BgL_arg1688z00_1616;
																												obj_t
																													BgL_arg1689z00_1617;
																												{	/* Ast/app.scm 116 */

																													BgL_arg1688z00_1616 =
																														BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
																														(BINT
																														(BgL_arityz00_1605),
																														BINT(10L));
																												}
																												{	/* Ast/app.scm 119 */
																													obj_t
																														BgL_arg1701z00_1625;
																													{	/* Ast/app.scm 119 */
																														long
																															BgL_b1297z00_1628;
																														BgL_b1297z00_1628 =
																															bgl_list_length(((
																																	(BgL_sfunz00_bglt)
																																	COBJECT((
																																			(BgL_sfunz00_bglt)
																																			BgL_funz00_1603)))->
																																BgL_optionalsz00));
																														{	/* Ast/app.scm 119 */
																															obj_t
																																BgL_za71za7_2706;
																															obj_t
																																BgL_za72za7_2707;
																															BgL_za71za7_2706 =
																																BINT
																																(BgL_arityz00_1605);
																															BgL_za72za7_2707 =
																																BINT
																																(BgL_b1297z00_1628);
																															{	/* Ast/app.scm 119 */
																																obj_t
																																	BgL_tmpz00_2708;
																																BgL_tmpz00_2708
																																	= BINT(0L);
																																if (BGL_ADDFX_OV
																																	(BgL_za71za7_2706,
																																		BgL_za72za7_2707,
																																		BgL_tmpz00_2708))
																																	{	/* Ast/app.scm 119 */
																																		BgL_arg1701z00_1625
																																			=
																																			bgl_bignum_add
																																			(bgl_long_to_bignum
																																			((long)
																																				CINT
																																				(BgL_za71za7_2706)),
																																			bgl_long_to_bignum
																																			((long)
																																				CINT
																																				(BgL_za72za7_2707)));
																																	}
																																else
																																	{	/* Ast/app.scm 119 */
																																		BgL_arg1701z00_1625
																																			=
																																			BgL_tmpz00_2708;
																																	}
																															}
																														}
																													}
																													{	/* Ast/app.scm 118 */

																														BgL_arg1689z00_1617
																															=
																															BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
																															(BgL_arg1701z00_1625,
																															BINT(10L));
																													}
																												}
																												{	/* Ast/app.scm 115 */
																													obj_t
																														BgL_list1690z00_1618;
																													{	/* Ast/app.scm 115 */
																														obj_t
																															BgL_arg1691z00_1619;
																														{	/* Ast/app.scm 115 */
																															obj_t
																																BgL_arg1692z00_1620;
																															{	/* Ast/app.scm 115 */
																																obj_t
																																	BgL_arg1699z00_1621;
																																{	/* Ast/app.scm 115 */
																																	obj_t
																																		BgL_arg1700z00_1622;
																																	BgL_arg1700z00_1622
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_string2334z00zzast_appz00,
																																		BNIL);
																																	BgL_arg1699z00_1621
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1689z00_1617,
																																		BgL_arg1700z00_1622);
																																}
																																BgL_arg1692z00_1620
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_string2335z00zzast_appz00,
																																	BgL_arg1699z00_1621);
																															}
																															BgL_arg1691z00_1619
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1688z00_1616,
																																BgL_arg1692z00_1620);
																														}
																														BgL_list1690z00_1618
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_string2336z00zzast_appz00,
																															BgL_arg1691z00_1619);
																													}
																													BgL_expectz00_1606 =
																														BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																														(BgL_list1690z00_1618);
																												}
																											}
																										else
																											{	/* Ast/app.scm 114 */
																												if (BGl_sfunzd2keyzf3z21zzast_varz00(((obj_t) BgL_funz00_1603)))
																													{	/* Ast/app.scm 124 */
																														obj_t
																															BgL_arg1705z00_1632;
																														obj_t
																															BgL_arg1708z00_1633;
																														{	/* Ast/app.scm 124 */

																															BgL_arg1705z00_1632
																																=
																																BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
																																(BINT
																																(BgL_arityz00_1605),
																																BINT(10L));
																														}
																														{	/* Ast/app.scm 127 */
																															obj_t
																																BgL_arg1718z00_1641;
																															{	/* Ast/app.scm 127 */
																																obj_t
																																	BgL_b1300z00_1644;
																																{	/* Ast/app.scm 128 */
																																	long
																																		BgL_b1299z00_1647;
																																	BgL_b1299z00_1647
																																		=
																																		bgl_list_length
																																		((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) BgL_funz00_1603)))->BgL_keysz00));
																																	{	/* Ast/app.scm 128 */

																																		{	/* Ast/app.scm 128 */
																																			obj_t
																																				BgL_za71za7_2717;
																																			obj_t
																																				BgL_za72za7_2718;
																																			BgL_za71za7_2717
																																				=
																																				BINT
																																				(2L);
																																			BgL_za72za7_2718
																																				=
																																				BINT
																																				(BgL_b1299z00_1647);
																																			{	/* Ast/app.scm 128 */
																																				obj_t
																																					BgL_tmpz00_2719;
																																				BgL_tmpz00_2719
																																					=
																																					BINT
																																					(0L);
																																				{	/* Ast/app.scm 128 */
																																					bool_t
																																						BgL_test2414z00_4484;
																																					{	/* Ast/app.scm 128 */
																																						long
																																							BgL_tmpz00_4485;
																																						BgL_tmpz00_4485
																																							=
																																							(long)
																																							CINT
																																							(BgL_za72za7_2718);
																																						BgL_test2414z00_4484
																																							=
																																							BGL_MULFX_OV
																																							(BgL_za71za7_2717,
																																							BgL_tmpz00_4485,
																																							BgL_tmpz00_2719);
																																					}
																																					if (BgL_test2414z00_4484)
																																						{	/* Ast/app.scm 128 */
																																							BgL_b1300z00_1644
																																								=
																																								bgl_bignum_mul
																																								(bgl_long_to_bignum
																																								((long) CINT(BgL_za71za7_2717)), bgl_long_to_bignum((long) CINT(BgL_za72za7_2718)));
																																						}
																																					else
																																						{	/* Ast/app.scm 128 */
																																							BgL_b1300z00_1644
																																								=
																																								BgL_tmpz00_2719;
																																						}
																																				}
																																			}
																																		}
																																	}
																																}
																																if (INTEGERP
																																	(BgL_b1300z00_1644))
																																	{	/* Ast/app.scm 127 */
																																		obj_t
																																			BgL_za71za7_2727;
																																		BgL_za71za7_2727
																																			=
																																			BINT
																																			(BgL_arityz00_1605);
																																		{	/* Ast/app.scm 127 */
																																			obj_t
																																				BgL_tmpz00_2729;
																																			BgL_tmpz00_2729
																																				=
																																				BINT
																																				(0L);
																																			if (BGL_ADDFX_OV(BgL_za71za7_2727, BgL_b1300z00_1644, BgL_tmpz00_2729))
																																				{	/* Ast/app.scm 127 */
																																					BgL_arg1718z00_1641
																																						=
																																						bgl_bignum_add
																																						(bgl_long_to_bignum
																																						((long) CINT(BgL_za71za7_2727)), bgl_long_to_bignum((long) CINT(BgL_b1300z00_1644)));
																																				}
																																			else
																																				{	/* Ast/app.scm 127 */
																																					BgL_arg1718z00_1641
																																						=
																																						BgL_tmpz00_2729;
																																				}
																																		}
																																	}
																																else
																																	{	/* Ast/app.scm 127 */
																																		BgL_arg1718z00_1641
																																			=
																																			BGl_2zb2zb2zz__r4_numbers_6_5z00
																																			(BINT
																																			(BgL_arityz00_1605),
																																			BgL_b1300z00_1644);
																																	}
																															}
																															{	/* Ast/app.scm 126 */

																																BgL_arg1708z00_1633
																																	=
																																	BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
																																	(BgL_arg1718z00_1641,
																																	BINT(10L));
																															}
																														}
																														{	/* Ast/app.scm 123 */
																															obj_t
																																BgL_list1709z00_1634;
																															{	/* Ast/app.scm 123 */
																																obj_t
																																	BgL_arg1710z00_1635;
																																{	/* Ast/app.scm 123 */
																																	obj_t
																																		BgL_arg1711z00_1636;
																																	{	/* Ast/app.scm 123 */
																																		obj_t
																																			BgL_arg1714z00_1637;
																																		{	/* Ast/app.scm 123 */
																																			obj_t
																																				BgL_arg1717z00_1638;
																																			BgL_arg1717z00_1638
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_string2334z00zzast_appz00,
																																				BNIL);
																																			BgL_arg1714z00_1637
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1708z00_1633,
																																				BgL_arg1717z00_1638);
																																		}
																																		BgL_arg1711z00_1636
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_string2335z00zzast_appz00,
																																			BgL_arg1714z00_1637);
																																	}
																																	BgL_arg1710z00_1635
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1705z00_1632,
																																		BgL_arg1711z00_1636);
																																}
																																BgL_list1709z00_1634
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_string2336z00zzast_appz00,
																																	BgL_arg1710z00_1635);
																															}
																															BgL_expectz00_1606
																																=
																																BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																																(BgL_list1709z00_1634);
																														}
																													}
																												else
																													{	/* Ast/app.scm 131 */
																														obj_t
																															BgL_arg1724z00_1650;
																														{	/* Ast/app.scm 131 */

																															BgL_arg1724z00_1650
																																=
																																BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
																																(BINT
																																(BgL_arityz00_1605),
																																BINT(10L));
																														}
																														BgL_expectz00_1606 =
																															string_append
																															(BgL_arg1724z00_1650,
																															BGl_string2337z00zzast_appz00);
																													}
																											}
																									}
																								else
																									{	/* Ast/app.scm 134 */
																										obj_t BgL_arg1733z00_1653;

																										{	/* Ast/app.scm 134 */

																											BgL_arg1733z00_1653 =
																												BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
																												(BINT(NEG(
																														(BgL_arityz00_1605 +
																															1L))), BINT(10L));
																										}
																										BgL_expectz00_1606 =
																											string_append
																											(BgL_arg1733z00_1653,
																											BGl_string2338z00zzast_appz00);
																									}
																								{	/* Ast/app.scm 111 */
																									obj_t BgL_providez00_1607;

																									{	/* Ast/app.scm 136 */
																										obj_t BgL_arg1678z00_1610;

																										{	/* Ast/app.scm 136 */

																											BgL_arg1678z00_1610 =
																												BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
																												(BINT(bgl_list_length
																													(BgL_argsz00_1600)),
																												BINT(10L));
																										}
																										BgL_providez00_1607 =
																											string_append
																											(BgL_arg1678z00_1610,
																											BGl_string2339z00zzast_appz00);
																									}
																									{	/* Ast/app.scm 136 */

																										return
																											BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00
																											(string_append_3
																											(BGl_string2340z00zzast_appz00,
																												BgL_expectz00_1606,
																												BgL_providez00_1607),
																											BGl_shapez00zztools_shapez00
																											(BgL_expz00_1597),
																											BgL_locz00_1598);
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																	}
																else
																	{	/* Ast/app.scm 151 */
																		BGL_TAIL return
																			BGl_sexpzd2ze3nodez31zzast_sexpz00
																			(CNST_TABLE_REF(1), BgL_stackz00_7,
																			BgL_locz00_1476, CNST_TABLE_REF(2));
																	}
															}
														}
													}
												}
											else
												{	/* Ast/app.scm 157 */
													obj_t BgL_g1109z00_1487;

													BgL_g1109z00_1487 = CDR(((obj_t) BgL_expz00_6));
													{
														obj_t BgL_oldzd2argszd2_1491;
														obj_t BgL_newzd2argszd2_1492;
														obj_t BgL_bindingsz00_1493;

														BgL_oldzd2argszd2_1491 = BgL_g1109z00_1487;
														BgL_newzd2argszd2_1492 = BNIL;
														BgL_bindingsz00_1493 = BNIL;
													BgL_zc3z04anonymousza31426ze3z87_1494:
														if (NULLP(BgL_oldzd2argszd2_1491))
															{	/* Ast/app.scm 168 */
																bool_t BgL_test2418z00_4539;

																{	/* Ast/app.scm 168 */
																	obj_t BgL_classz00_2814;

																	BgL_classz00_2814 = BGl_varz00zzast_nodez00;
																	{	/* Ast/app.scm 168 */
																		BgL_objectz00_bglt BgL_arg1807z00_2816;

																		{	/* Ast/app.scm 168 */
																			obj_t BgL_tmpz00_4540;

																			BgL_tmpz00_4540 =
																				((obj_t)
																				((BgL_objectz00_bglt) BgL_funz00_1479));
																			BgL_arg1807z00_2816 =
																				(BgL_objectz00_bglt) (BgL_tmpz00_4540);
																		}
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Ast/app.scm 168 */
																				long BgL_idxz00_2822;

																				BgL_idxz00_2822 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_2816);
																				BgL_test2418z00_4539 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_2822 + 2L)) ==
																					BgL_classz00_2814);
																			}
																		else
																			{	/* Ast/app.scm 168 */
																				bool_t BgL_res2294z00_2847;

																				{	/* Ast/app.scm 168 */
																					obj_t BgL_oclassz00_2830;

																					{	/* Ast/app.scm 168 */
																						obj_t BgL_arg1815z00_2838;
																						long BgL_arg1816z00_2839;

																						BgL_arg1815z00_2838 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Ast/app.scm 168 */
																							long BgL_arg1817z00_2840;

																							BgL_arg1817z00_2840 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_2816);
																							BgL_arg1816z00_2839 =
																								(BgL_arg1817z00_2840 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_2830 =
																							VECTOR_REF(BgL_arg1815z00_2838,
																							BgL_arg1816z00_2839);
																					}
																					{	/* Ast/app.scm 168 */
																						bool_t BgL__ortest_1115z00_2831;

																						BgL__ortest_1115z00_2831 =
																							(BgL_classz00_2814 ==
																							BgL_oclassz00_2830);
																						if (BgL__ortest_1115z00_2831)
																							{	/* Ast/app.scm 168 */
																								BgL_res2294z00_2847 =
																									BgL__ortest_1115z00_2831;
																							}
																						else
																							{	/* Ast/app.scm 168 */
																								long BgL_odepthz00_2832;

																								{	/* Ast/app.scm 168 */
																									obj_t BgL_arg1804z00_2833;

																									BgL_arg1804z00_2833 =
																										(BgL_oclassz00_2830);
																									BgL_odepthz00_2832 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_2833);
																								}
																								if ((2L < BgL_odepthz00_2832))
																									{	/* Ast/app.scm 168 */
																										obj_t BgL_arg1802z00_2835;

																										{	/* Ast/app.scm 168 */
																											obj_t BgL_arg1803z00_2836;

																											BgL_arg1803z00_2836 =
																												(BgL_oclassz00_2830);
																											BgL_arg1802z00_2835 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_2836,
																												2L);
																										}
																										BgL_res2294z00_2847 =
																											(BgL_arg1802z00_2835 ==
																											BgL_classz00_2814);
																									}
																								else
																									{	/* Ast/app.scm 168 */
																										BgL_res2294z00_2847 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test2418z00_4539 =
																					BgL_res2294z00_2847;
																			}
																	}
																}
																if (BgL_test2418z00_4539)
																	{	/* Ast/app.scm 170 */
																		BgL_nodez00_bglt BgL_nodez00_1499;

																		{	/* Ast/app.scm 170 */
																			obj_t BgL_arg1434z00_1500;

																			BgL_arg1434z00_1500 =
																				BGl_makezd2thezd2appze70ze7zzast_appz00
																				(BgL_bindingsz00_1493,
																				BgL_newzd2argszd2_1492,
																				((obj_t) BgL_funz00_1479));
																			BgL_nodez00_1499 =
																				BGl_sexpzd2ze3nodez31zzast_sexpz00
																				(BgL_arg1434z00_1500, BgL_stackz00_7,
																				BgL_locz00_1476, BgL_sitez00_9);
																		}
																		return
																			BGl_cleanzd2userzd2nodez12z12zzast_appz00
																			(BgL_nodez00_1499);
																	}
																else
																	{	/* Ast/app.scm 175 */
																		obj_t BgL_newzd2funzd2_1501;

																		BgL_newzd2funzd2_1501 =
																			BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																			(BGl_gensymz00zz__r4_symbols_6_4z00
																			(CNST_TABLE_REF(3)));
																		{	/* Ast/app.scm 175 */
																			obj_t BgL_lexpz00_1502;

																			{	/* Ast/app.scm 176 */
																				obj_t BgL_arg1437z00_1504;
																				obj_t BgL_arg1448z00_1505;

																				BgL_arg1437z00_1504 =
																					BGl_letzd2symzd2zzast_letz00();
																				{	/* Ast/app.scm 176 */
																					obj_t BgL_arg1453z00_1506;
																					obj_t BgL_arg1454z00_1507;

																					{	/* Ast/app.scm 176 */
																						obj_t BgL_arg1472z00_1508;

																						{	/* Ast/app.scm 176 */
																							obj_t BgL_arg1473z00_1509;

																							{	/* Ast/app.scm 176 */
																								obj_t BgL_arg1485z00_1510;

																								if (BgL_funzd2errzf3z21_1480)
																									{	/* Ast/app.scm 176 */
																										BgL_arg1485z00_1510 =
																											CNST_TABLE_REF(4);
																									}
																								else
																									{	/* Ast/app.scm 176 */
																										BgL_arg1485z00_1510 =
																											((obj_t) BgL_funz00_1479);
																									}
																								BgL_arg1473z00_1509 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1485z00_1510, BNIL);
																							}
																							BgL_arg1472z00_1508 =
																								MAKE_YOUNG_PAIR
																								(BgL_newzd2funzd2_1501,
																								BgL_arg1473z00_1509);
																						}
																						BgL_arg1453z00_1506 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1472z00_1508, BNIL);
																					}
																					BgL_arg1454z00_1507 =
																						MAKE_YOUNG_PAIR
																						(BGl_makezd2thezd2appze70ze7zzast_appz00
																						(BgL_bindingsz00_1493,
																							BgL_newzd2argszd2_1492,
																							BgL_newzd2funzd2_1501), BNIL);
																					BgL_arg1448z00_1505 =
																						MAKE_YOUNG_PAIR(BgL_arg1453z00_1506,
																						BgL_arg1454z00_1507);
																				}
																				BgL_lexpz00_1502 =
																					MAKE_YOUNG_PAIR(BgL_arg1437z00_1504,
																					BgL_arg1448z00_1505);
																			}
																			{	/* Ast/app.scm 176 */
																				BgL_nodez00_bglt BgL_nodez00_1503;

																				BgL_nodez00_1503 =
																					BGl_sexpzd2ze3nodez31zzast_sexpz00
																					(BgL_lexpz00_1502, BgL_stackz00_7,
																					BgL_locz00_1476, BgL_sitez00_9);
																				{	/* Ast/app.scm 180 */

																					return
																						BGl_cleanzd2userzd2nodez12z12zzast_appz00
																						(BgL_nodez00_1503);
																				}
																			}
																		}
																	}
															}
														else
															{	/* Ast/app.scm 161 */
																if (BGl_atomiczf3ze70z14zzast_appz00(CAR(
																			((obj_t) BgL_oldzd2argszd2_1491))))
																	{	/* Ast/app.scm 183 */
																		obj_t BgL_arg1559z00_1528;
																		obj_t BgL_arg1561z00_1529;

																		BgL_arg1559z00_1528 =
																			CDR(((obj_t) BgL_oldzd2argszd2_1491));
																		if (EPAIRP(BgL_oldzd2argszd2_1491))
																			{	/* Ast/app.scm 185 */
																				obj_t BgL_arg1564z00_1531;
																				obj_t BgL_arg1565z00_1532;

																				BgL_arg1564z00_1531 =
																					CAR(((obj_t) BgL_oldzd2argszd2_1491));
																				BgL_arg1565z00_1532 =
																					CER(((obj_t) BgL_oldzd2argszd2_1491));
																				{	/* Ast/app.scm 185 */
																					obj_t BgL_res2295z00_2852;

																					BgL_res2295z00_2852 =
																						MAKE_YOUNG_EPAIR
																						(BgL_arg1564z00_1531,
																						BgL_newzd2argszd2_1492,
																						BgL_arg1565z00_1532);
																					BgL_arg1561z00_1529 =
																						BgL_res2295z00_2852;
																				}
																			}
																		else
																			{	/* Ast/app.scm 186 */
																				obj_t BgL_arg1571z00_1533;

																				BgL_arg1571z00_1533 =
																					CAR(((obj_t) BgL_oldzd2argszd2_1491));
																				BgL_arg1561z00_1529 =
																					MAKE_YOUNG_PAIR(BgL_arg1571z00_1533,
																					BgL_newzd2argszd2_1492);
																			}
																		{
																			obj_t BgL_newzd2argszd2_4600;
																			obj_t BgL_oldzd2argszd2_4599;

																			BgL_oldzd2argszd2_4599 =
																				BgL_arg1559z00_1528;
																			BgL_newzd2argszd2_4600 =
																				BgL_arg1561z00_1529;
																			BgL_newzd2argszd2_1492 =
																				BgL_newzd2argszd2_4600;
																			BgL_oldzd2argszd2_1491 =
																				BgL_oldzd2argszd2_4599;
																			goto
																				BgL_zc3z04anonymousza31426ze3z87_1494;
																		}
																	}
																else
																	{	/* Ast/app.scm 189 */
																		obj_t BgL_newzd2argzd2_1534;

																		BgL_newzd2argzd2_1534 =
																			BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																			(BGl_gensymz00zz__r4_symbols_6_4z00
																			(CNST_TABLE_REF(5)));
																		{	/* Ast/app.scm 190 */
																			obj_t BgL_arg1573z00_1535;
																			obj_t BgL_arg1575z00_1536;
																			obj_t BgL_arg1576z00_1537;

																			BgL_arg1573z00_1535 =
																				CDR(((obj_t) BgL_oldzd2argszd2_1491));
																			if (EPAIRP(BgL_oldzd2argszd2_1491))
																				{	/* Ast/app.scm 192 */
																					obj_t BgL_arg1584z00_1539;

																					BgL_arg1584z00_1539 =
																						CER(
																						((obj_t) BgL_oldzd2argszd2_1491));
																					{	/* Ast/app.scm 192 */
																						obj_t BgL_res2296z00_2856;

																						BgL_res2296z00_2856 =
																							MAKE_YOUNG_EPAIR
																							(BgL_newzd2argzd2_1534,
																							BgL_newzd2argszd2_1492,
																							BgL_arg1584z00_1539);
																						BgL_arg1575z00_1536 =
																							BgL_res2296z00_2856;
																					}
																				}
																			else
																				{	/* Ast/app.scm 191 */
																					BgL_arg1575z00_1536 =
																						MAKE_YOUNG_PAIR
																						(BgL_newzd2argzd2_1534,
																						BgL_newzd2argszd2_1492);
																				}
																			{	/* Ast/app.scm 194 */
																				obj_t BgL_arg1585z00_1540;

																				{	/* Ast/app.scm 194 */
																					obj_t BgL_arg1589z00_1541;

																					BgL_arg1589z00_1541 =
																						CAR(
																						((obj_t) BgL_oldzd2argszd2_1491));
																					{	/* Ast/app.scm 194 */
																						obj_t BgL_list1590z00_1542;

																						{	/* Ast/app.scm 194 */
																							obj_t BgL_arg1591z00_1543;

																							BgL_arg1591z00_1543 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1589z00_1541, BNIL);
																							BgL_list1590z00_1542 =
																								MAKE_YOUNG_PAIR
																								(BgL_newzd2argzd2_1534,
																								BgL_arg1591z00_1543);
																						}
																						BgL_arg1585z00_1540 =
																							BgL_list1590z00_1542;
																					}
																				}
																				BgL_arg1576z00_1537 =
																					MAKE_YOUNG_PAIR(BgL_arg1585z00_1540,
																					BgL_bindingsz00_1493);
																			}
																			{
																				obj_t BgL_bindingsz00_4619;
																				obj_t BgL_newzd2argszd2_4618;
																				obj_t BgL_oldzd2argszd2_4617;

																				BgL_oldzd2argszd2_4617 =
																					BgL_arg1573z00_1535;
																				BgL_newzd2argszd2_4618 =
																					BgL_arg1575z00_1536;
																				BgL_bindingsz00_4619 =
																					BgL_arg1576z00_1537;
																				BgL_bindingsz00_1493 =
																					BgL_bindingsz00_4619;
																				BgL_newzd2argszd2_1492 =
																					BgL_newzd2argszd2_4618;
																				BgL_oldzd2argszd2_1491 =
																					BgL_oldzd2argszd2_4617;
																				goto
																					BgL_zc3z04anonymousza31426ze3z87_1494;
																			}
																		}
																	}
															}
													}
												}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* atomic?~0 */
	bool_t BGl_atomiczf3ze70z14zzast_appz00(obj_t BgL_expz00_1550)
	{
		{	/* Ast/app.scm 89 */
			if (PAIRP(BgL_expz00_1550))
				{	/* Ast/app.scm 89 */
					if ((CAR(((obj_t) BgL_expz00_1550)) == CNST_TABLE_REF(6)))
						{	/* Ast/app.scm 89 */
							return ((bool_t) 1);
						}
					else
						{	/* Ast/app.scm 89 */
							obj_t BgL_cdrzd2371zd2_1564;

							BgL_cdrzd2371zd2_1564 = CDR(((obj_t) BgL_expz00_1550));
							if ((CAR(((obj_t) BgL_expz00_1550)) == CNST_TABLE_REF(7)))
								{	/* Ast/app.scm 89 */
									if (PAIRP(BgL_cdrzd2371zd2_1564))
										{	/* Ast/app.scm 89 */
											obj_t BgL_cdrzd2373zd2_1568;

											BgL_cdrzd2373zd2_1568 = CDR(BgL_cdrzd2371zd2_1564);
											{	/* Ast/app.scm 89 */
												bool_t BgL_test2430z00_4637;

												{	/* Ast/app.scm 89 */
													obj_t BgL_tmpz00_4638;

													BgL_tmpz00_4638 = CAR(BgL_cdrzd2371zd2_1564);
													BgL_test2430z00_4637 = SYMBOLP(BgL_tmpz00_4638);
												}
												if (BgL_test2430z00_4637)
													{	/* Ast/app.scm 89 */
														if (PAIRP(BgL_cdrzd2373zd2_1568))
															{	/* Ast/app.scm 89 */
																bool_t BgL_test2432z00_4643;

																{	/* Ast/app.scm 89 */
																	obj_t BgL_tmpz00_4644;

																	BgL_tmpz00_4644 = CAR(BgL_cdrzd2373zd2_1568);
																	BgL_test2432z00_4643 =
																		SYMBOLP(BgL_tmpz00_4644);
																}
																if (BgL_test2432z00_4643)
																	{	/* Ast/app.scm 89 */
																		return NULLP(CDR(BgL_cdrzd2373zd2_1568));
																	}
																else
																	{	/* Ast/app.scm 89 */
																		return ((bool_t) 0);
																	}
															}
														else
															{	/* Ast/app.scm 89 */
																return ((bool_t) 0);
															}
													}
												else
													{	/* Ast/app.scm 89 */
														return ((bool_t) 0);
													}
											}
										}
									else
										{	/* Ast/app.scm 89 */
											return ((bool_t) 0);
										}
								}
							else
								{	/* Ast/app.scm 89 */
									return ((bool_t) 0);
								}
						}
				}
			else
				{	/* Ast/app.scm 89 */
					return ((bool_t) 1);
				}
		}

	}



/* make-the-app~0 */
	obj_t BGl_makezd2thezd2appze70ze7zzast_appz00(obj_t BgL_bindingsz00_4172,
		obj_t BgL_newzd2argszd2_4171, obj_t BgL_funz00_1513)
	{
		{	/* Ast/app.scm 163 */
			if (NULLP(BgL_bindingsz00_4172))
				{	/* Ast/app.scm 164 */
					return
						MAKE_YOUNG_PAIR(BgL_funz00_1513,
						BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(bgl_reverse_bang
							(BgL_newzd2argszd2_4171), BNIL));
				}
			else
				{	/* Ast/app.scm 165 */
					obj_t BgL_arg1509z00_1516;
					obj_t BgL_arg1513z00_1517;

					BgL_arg1509z00_1516 = BGl_letzd2symzd2zzast_letz00();
					{	/* Ast/app.scm 165 */
						obj_t BgL_arg1514z00_1518;
						obj_t BgL_arg1516z00_1519;

						BgL_arg1514z00_1518 = bgl_reverse_bang(BgL_bindingsz00_4172);
						{	/* Ast/app.scm 166 */
							obj_t BgL_arg1535z00_1520;

							BgL_arg1535z00_1520 =
								MAKE_YOUNG_PAIR(BgL_funz00_1513,
								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(bgl_reverse_bang
									(BgL_newzd2argszd2_4171), BNIL));
							BgL_arg1516z00_1519 = MAKE_YOUNG_PAIR(BgL_arg1535z00_1520, BNIL);
						}
						BgL_arg1513z00_1517 =
							MAKE_YOUNG_PAIR(BgL_arg1514z00_1518, BgL_arg1516z00_1519);
					}
					return MAKE_YOUNG_PAIR(BgL_arg1509z00_1516, BgL_arg1513z00_1517);
				}
		}

	}



/* &application->node */
	BgL_nodez00_bglt BGl_z62applicationzd2ze3nodez53zzast_appz00(obj_t
		BgL_envz00_4152, obj_t BgL_expz00_4153, obj_t BgL_stackz00_4154,
		obj_t BgL_locz00_4155, obj_t BgL_sitez00_4156)
	{
		{	/* Ast/app.scm 74 */
			return
				BGl_applicationzd2ze3nodez31zzast_appz00(BgL_expz00_4153,
				BgL_stackz00_4154, BgL_locz00_4155, BgL_sitez00_4156);
		}

	}



/* check-user-app */
	long BGl_checkzd2userzd2appz00zzast_appz00(BgL_nodez00_bglt BgL_funz00_10,
		obj_t BgL_argsz00_11)
	{
		{	/* Ast/app.scm 205 */
			{	/* Ast/app.scm 206 */
				bool_t BgL_test2434z00_4663;

				{	/* Ast/app.scm 206 */
					obj_t BgL_classz00_2859;

					BgL_classz00_2859 = BGl_varz00zzast_nodez00;
					{	/* Ast/app.scm 206 */
						BgL_objectz00_bglt BgL_arg1807z00_2861;

						{	/* Ast/app.scm 206 */
							obj_t BgL_tmpz00_4664;

							BgL_tmpz00_4664 = ((obj_t) ((BgL_objectz00_bglt) BgL_funz00_10));
							BgL_arg1807z00_2861 = (BgL_objectz00_bglt) (BgL_tmpz00_4664);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Ast/app.scm 206 */
								long BgL_idxz00_2867;

								BgL_idxz00_2867 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2861);
								BgL_test2434z00_4663 =
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_2867 + 2L)) == BgL_classz00_2859);
							}
						else
							{	/* Ast/app.scm 206 */
								bool_t BgL_res2298z00_2892;

								{	/* Ast/app.scm 206 */
									obj_t BgL_oclassz00_2875;

									{	/* Ast/app.scm 206 */
										obj_t BgL_arg1815z00_2883;
										long BgL_arg1816z00_2884;

										BgL_arg1815z00_2883 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Ast/app.scm 206 */
											long BgL_arg1817z00_2885;

											BgL_arg1817z00_2885 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2861);
											BgL_arg1816z00_2884 = (BgL_arg1817z00_2885 - OBJECT_TYPE);
										}
										BgL_oclassz00_2875 =
											VECTOR_REF(BgL_arg1815z00_2883, BgL_arg1816z00_2884);
									}
									{	/* Ast/app.scm 206 */
										bool_t BgL__ortest_1115z00_2876;

										BgL__ortest_1115z00_2876 =
											(BgL_classz00_2859 == BgL_oclassz00_2875);
										if (BgL__ortest_1115z00_2876)
											{	/* Ast/app.scm 206 */
												BgL_res2298z00_2892 = BgL__ortest_1115z00_2876;
											}
										else
											{	/* Ast/app.scm 206 */
												long BgL_odepthz00_2877;

												{	/* Ast/app.scm 206 */
													obj_t BgL_arg1804z00_2878;

													BgL_arg1804z00_2878 = (BgL_oclassz00_2875);
													BgL_odepthz00_2877 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_2878);
												}
												if ((2L < BgL_odepthz00_2877))
													{	/* Ast/app.scm 206 */
														obj_t BgL_arg1802z00_2880;

														{	/* Ast/app.scm 206 */
															obj_t BgL_arg1803z00_2881;

															BgL_arg1803z00_2881 = (BgL_oclassz00_2875);
															BgL_arg1802z00_2880 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2881,
																2L);
														}
														BgL_res2298z00_2892 =
															(BgL_arg1802z00_2880 == BgL_classz00_2859);
													}
												else
													{	/* Ast/app.scm 206 */
														BgL_res2298z00_2892 = ((bool_t) 0);
													}
											}
									}
								}
								BgL_test2434z00_4663 = BgL_res2298z00_2892;
							}
					}
				}
				if (BgL_test2434z00_4663)
					{	/* Ast/app.scm 212 */
						BgL_variablez00_bglt BgL_varz00_1663;

						BgL_varz00_1663 =
							(((BgL_varz00_bglt) COBJECT(
									((BgL_varz00_bglt) BgL_funz00_10)))->BgL_variablez00);
						{	/* Ast/app.scm 212 */
							BgL_valuez00_bglt BgL_funz00_1664;

							BgL_funz00_1664 =
								(((BgL_variablez00_bglt) COBJECT(BgL_varz00_1663))->
								BgL_valuez00);
							{	/* Ast/app.scm 213 */
								long BgL_nbzd2argszd2_1665;

								BgL_nbzd2argszd2_1665 = bgl_list_length(BgL_argsz00_11);
								{	/* Ast/app.scm 214 */
									long BgL_arityz00_1666;

									{	/* Ast/app.scm 216 */
										bool_t BgL_test2438z00_4691;

										{	/* Ast/app.scm 216 */
											obj_t BgL_classz00_2895;

											BgL_classz00_2895 = BGl_funz00zzast_varz00;
											{	/* Ast/app.scm 216 */
												BgL_objectz00_bglt BgL_arg1807z00_2897;

												{	/* Ast/app.scm 216 */
													obj_t BgL_tmpz00_4692;

													BgL_tmpz00_4692 =
														((obj_t) ((BgL_objectz00_bglt) BgL_funz00_1664));
													BgL_arg1807z00_2897 =
														(BgL_objectz00_bglt) (BgL_tmpz00_4692);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Ast/app.scm 216 */
														long BgL_idxz00_2903;

														BgL_idxz00_2903 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2897);
														BgL_test2438z00_4691 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2903 + 2L)) == BgL_classz00_2895);
													}
												else
													{	/* Ast/app.scm 216 */
														bool_t BgL_res2299z00_2928;

														{	/* Ast/app.scm 216 */
															obj_t BgL_oclassz00_2911;

															{	/* Ast/app.scm 216 */
																obj_t BgL_arg1815z00_2919;
																long BgL_arg1816z00_2920;

																BgL_arg1815z00_2919 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Ast/app.scm 216 */
																	long BgL_arg1817z00_2921;

																	BgL_arg1817z00_2921 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2897);
																	BgL_arg1816z00_2920 =
																		(BgL_arg1817z00_2921 - OBJECT_TYPE);
																}
																BgL_oclassz00_2911 =
																	VECTOR_REF(BgL_arg1815z00_2919,
																	BgL_arg1816z00_2920);
															}
															{	/* Ast/app.scm 216 */
																bool_t BgL__ortest_1115z00_2912;

																BgL__ortest_1115z00_2912 =
																	(BgL_classz00_2895 == BgL_oclassz00_2911);
																if (BgL__ortest_1115z00_2912)
																	{	/* Ast/app.scm 216 */
																		BgL_res2299z00_2928 =
																			BgL__ortest_1115z00_2912;
																	}
																else
																	{	/* Ast/app.scm 216 */
																		long BgL_odepthz00_2913;

																		{	/* Ast/app.scm 216 */
																			obj_t BgL_arg1804z00_2914;

																			BgL_arg1804z00_2914 =
																				(BgL_oclassz00_2911);
																			BgL_odepthz00_2913 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2914);
																		}
																		if ((2L < BgL_odepthz00_2913))
																			{	/* Ast/app.scm 216 */
																				obj_t BgL_arg1802z00_2916;

																				{	/* Ast/app.scm 216 */
																					obj_t BgL_arg1803z00_2917;

																					BgL_arg1803z00_2917 =
																						(BgL_oclassz00_2911);
																					BgL_arg1802z00_2916 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2917, 2L);
																				}
																				BgL_res2299z00_2928 =
																					(BgL_arg1802z00_2916 ==
																					BgL_classz00_2895);
																			}
																		else
																			{	/* Ast/app.scm 216 */
																				BgL_res2299z00_2928 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2438z00_4691 = BgL_res2299z00_2928;
													}
											}
										}
										if (BgL_test2438z00_4691)
											{	/* Ast/app.scm 216 */
												BgL_arityz00_1666 =
													(((BgL_funz00_bglt) COBJECT(
															((BgL_funz00_bglt) BgL_funz00_1664)))->
													BgL_arityz00);
											}
										else
											{	/* Ast/app.scm 216 */
												BgL_arityz00_1666 = -1L;
											}
									}
									{	/* Ast/app.scm 215 */

										if ((BgL_arityz00_1666 == -1L))
											{	/* Ast/app.scm 221 */
												return 0L;
											}
										else
											{	/* Ast/app.scm 221 */
												if ((BgL_arityz00_1666 >= 0L))
													{	/* Ast/app.scm 223 */
														if (BGl_sfunzd2optionalzf3z21zzast_varz00(
																((obj_t) BgL_funz00_1664)))
															{	/* Ast/app.scm 225 */
																if ((BgL_nbzd2argszd2_1665 < BgL_arityz00_1666))
																	{	/* Ast/app.scm 227 */
																		return
																			(BgL_arityz00_1666 -
																			BgL_nbzd2argszd2_1665);
																	}
																else
																	{	/* Ast/app.scm 227 */
																		if (
																			(BgL_nbzd2argszd2_1665 >
																				(BgL_arityz00_1666 +
																					bgl_list_length(
																						(((BgL_sfunz00_bglt) COBJECT(
																									((BgL_sfunz00_bglt)
																										BgL_funz00_1664)))->
																							BgL_optionalsz00)))))
																			{	/* Ast/app.scm 229 */
																				return
																					(
																					(BgL_arityz00_1666 +
																						bgl_list_length(
																							(((BgL_sfunz00_bglt) COBJECT(
																										((BgL_sfunz00_bglt)
																											BgL_funz00_1664)))->
																								BgL_optionalsz00))) -
																					BgL_nbzd2argszd2_1665);
																			}
																		else
																			{	/* Ast/app.scm 229 */
																				return 0L;
																			}
																	}
															}
														else
															{	/* Ast/app.scm 225 */
																if (BGl_sfunzd2keyzf3z21zzast_varz00(
																		((obj_t) BgL_funz00_1664)))
																	{	/* Ast/app.scm 234 */
																		long BgL_klz00_1686;

																		BgL_klz00_1686 =
																			bgl_list_length(
																			(((BgL_sfunz00_bglt) COBJECT(
																						((BgL_sfunz00_bglt)
																							BgL_funz00_1664)))->BgL_keysz00));
																		if ((BgL_nbzd2argszd2_1665 <
																				BgL_arityz00_1666))
																			{	/* Ast/app.scm 236 */
																				return
																					(BgL_arityz00_1666 -
																					BgL_nbzd2argszd2_1665);
																			}
																		else
																			{	/* Ast/app.scm 236 */
																				if (
																					(BgL_nbzd2argszd2_1665 >
																						(BgL_arityz00_1666 +
																							(BgL_klz00_1686 * 2L))))
																					{	/* Ast/app.scm 238 */
																						return
																							(
																							(BgL_arityz00_1666 +
																								(BgL_klz00_1686 * 2L)) -
																							BgL_nbzd2argszd2_1665);
																					}
																				else
																					{	/* Ast/app.scm 238 */
																						return 0L;
																					}
																			}
																	}
																else
																	{	/* Ast/app.scm 233 */
																		return
																			(BgL_arityz00_1666 -
																			BgL_nbzd2argszd2_1665);
																	}
															}
													}
												else
													{	/* Ast/app.scm 223 */
														if (
															((NEG(BgL_arityz00_1666) -
																	(BgL_nbzd2argszd2_1665 + 1L)) <= 0L))
															{	/* Ast/app.scm 245 */
																return 0L;
															}
														else
															{	/* Ast/app.scm 245 */
																return 1L;
															}
													}
											}
									}
								}
							}
						}
					}
				else
					{	/* Ast/app.scm 206 */
						return 0L;
					}
			}
		}

	}



/* make-app-node */
	BGL_EXPORTED_DEF BgL_nodez00_bglt BGl_makezd2appzd2nodez00zzast_appz00(obj_t
		BgL_stackz00_12, obj_t BgL_locz00_13, obj_t BgL_sitez00_14,
		BgL_varz00_bglt BgL_varz00_15, obj_t BgL_argsz00_16)
	{
		{	/* Ast/app.scm 252 */
			{	/* Ast/app.scm 253 */
				BgL_valuez00_bglt BgL_funz00_1708;

				BgL_funz00_1708 =
					(((BgL_variablez00_bglt) COBJECT(
							(((BgL_varz00_bglt) COBJECT(BgL_varz00_15))->BgL_variablez00)))->
					BgL_valuez00);
				{	/* Ast/app.scm 255 */
					bool_t BgL_test2451z00_4762;

					{	/* Ast/app.scm 255 */
						bool_t BgL_test2452z00_4763;

						{	/* Ast/app.scm 255 */
							obj_t BgL_classz00_2970;

							BgL_classz00_2970 = BGl_sfunz00zzast_varz00;
							{	/* Ast/app.scm 255 */
								BgL_objectz00_bglt BgL_arg1807z00_2972;

								{	/* Ast/app.scm 255 */
									obj_t BgL_tmpz00_4764;

									BgL_tmpz00_4764 =
										((obj_t) ((BgL_objectz00_bglt) BgL_funz00_1708));
									BgL_arg1807z00_2972 = (BgL_objectz00_bglt) (BgL_tmpz00_4764);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Ast/app.scm 255 */
										long BgL_idxz00_2978;

										BgL_idxz00_2978 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2972);
										BgL_test2452z00_4763 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2978 + 3L)) == BgL_classz00_2970);
									}
								else
									{	/* Ast/app.scm 255 */
										bool_t BgL_res2300z00_3003;

										{	/* Ast/app.scm 255 */
											obj_t BgL_oclassz00_2986;

											{	/* Ast/app.scm 255 */
												obj_t BgL_arg1815z00_2994;
												long BgL_arg1816z00_2995;

												BgL_arg1815z00_2994 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Ast/app.scm 255 */
													long BgL_arg1817z00_2996;

													BgL_arg1817z00_2996 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2972);
													BgL_arg1816z00_2995 =
														(BgL_arg1817z00_2996 - OBJECT_TYPE);
												}
												BgL_oclassz00_2986 =
													VECTOR_REF(BgL_arg1815z00_2994, BgL_arg1816z00_2995);
											}
											{	/* Ast/app.scm 255 */
												bool_t BgL__ortest_1115z00_2987;

												BgL__ortest_1115z00_2987 =
													(BgL_classz00_2970 == BgL_oclassz00_2986);
												if (BgL__ortest_1115z00_2987)
													{	/* Ast/app.scm 255 */
														BgL_res2300z00_3003 = BgL__ortest_1115z00_2987;
													}
												else
													{	/* Ast/app.scm 255 */
														long BgL_odepthz00_2988;

														{	/* Ast/app.scm 255 */
															obj_t BgL_arg1804z00_2989;

															BgL_arg1804z00_2989 = (BgL_oclassz00_2986);
															BgL_odepthz00_2988 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2989);
														}
														if ((3L < BgL_odepthz00_2988))
															{	/* Ast/app.scm 255 */
																obj_t BgL_arg1802z00_2991;

																{	/* Ast/app.scm 255 */
																	obj_t BgL_arg1803z00_2992;

																	BgL_arg1803z00_2992 = (BgL_oclassz00_2986);
																	BgL_arg1802z00_2991 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2992,
																		3L);
																}
																BgL_res2300z00_3003 =
																	(BgL_arg1802z00_2991 == BgL_classz00_2970);
															}
														else
															{	/* Ast/app.scm 255 */
																BgL_res2300z00_3003 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2452z00_4763 = BgL_res2300z00_3003;
									}
							}
						}
						if (BgL_test2452z00_4763)
							{	/* Ast/app.scm 255 */
								obj_t BgL_tmpz00_4787;

								BgL_tmpz00_4787 =
									(((BgL_sfunz00_bglt) COBJECT(
											((BgL_sfunz00_bglt) BgL_funz00_1708)))->BgL_optionalsz00);
								BgL_test2451z00_4762 = PAIRP(BgL_tmpz00_4787);
							}
						else
							{	/* Ast/app.scm 255 */
								BgL_test2451z00_4762 = ((bool_t) 0);
							}
					}
					if (BgL_test2451z00_4762)
						{	/* Ast/app.scm 256 */
							obj_t BgL_argsz00_1712;

							{
								obj_t BgL_argsz00_1715;
								obj_t BgL_resz00_1716;

								BgL_argsz00_1715 = BgL_argsz00_16;
								BgL_resz00_1716 = BNIL;
							BgL_zc3z04anonymousza31814ze3z87_1717:
								if (NULLP(BgL_argsz00_1715))
									{	/* Ast/app.scm 258 */
										BgL_argsz00_1712 = bgl_reverse_bang(BgL_resz00_1716);
									}
								else
									{	/* Ast/app.scm 260 */
										obj_t BgL_az00_1719;
										obj_t BgL_locz00_1720;

										BgL_az00_1719 = CAR(((obj_t) BgL_argsz00_1715));
										BgL_locz00_1720 =
											BGl_findzd2locationzf2locz20zztools_locationz00
											(BgL_argsz00_1715, BgL_locz00_13);
										{	/* Ast/app.scm 262 */
											obj_t BgL_arg1820z00_1721;
											obj_t BgL_arg1822z00_1722;

											BgL_arg1820z00_1721 = CDR(((obj_t) BgL_argsz00_1715));
											{	/* Ast/app.scm 263 */
												BgL_nodez00_bglt BgL_arg1823z00_1723;

												BgL_arg1823z00_1723 =
													BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_az00_1719,
													BgL_stackz00_12, BgL_locz00_1720, CNST_TABLE_REF(2));
												BgL_arg1822z00_1722 =
													MAKE_YOUNG_PAIR(((obj_t) BgL_arg1823z00_1723),
													BgL_resz00_1716);
											}
											{
												obj_t BgL_resz00_4804;
												obj_t BgL_argsz00_4803;

												BgL_argsz00_4803 = BgL_arg1820z00_1721;
												BgL_resz00_4804 = BgL_arg1822z00_1722;
												BgL_resz00_1716 = BgL_resz00_4804;
												BgL_argsz00_1715 = BgL_argsz00_4803;
												goto BgL_zc3z04anonymousza31814ze3z87_1717;
											}
										}
									}
							}
							return
								BGl_makezd2optionalszd2appzd2nodezd2zzast_appz00
								(BgL_stackz00_12, BgL_locz00_13, BgL_sitez00_14, BgL_varz00_15,
								BgL_argsz00_1712);
						}
					else
						{	/* Ast/app.scm 266 */
							bool_t BgL_test2457z00_4806;

							{	/* Ast/app.scm 266 */
								bool_t BgL_test2458z00_4807;

								{	/* Ast/app.scm 266 */
									obj_t BgL_classz00_3007;

									BgL_classz00_3007 = BGl_sfunz00zzast_varz00;
									{	/* Ast/app.scm 266 */
										BgL_objectz00_bglt BgL_arg1807z00_3009;

										{	/* Ast/app.scm 266 */
											obj_t BgL_tmpz00_4808;

											BgL_tmpz00_4808 =
												((obj_t) ((BgL_objectz00_bglt) BgL_funz00_1708));
											BgL_arg1807z00_3009 =
												(BgL_objectz00_bglt) (BgL_tmpz00_4808);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Ast/app.scm 266 */
												long BgL_idxz00_3015;

												BgL_idxz00_3015 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3009);
												BgL_test2458z00_4807 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_3015 + 3L)) == BgL_classz00_3007);
											}
										else
											{	/* Ast/app.scm 266 */
												bool_t BgL_res2301z00_3040;

												{	/* Ast/app.scm 266 */
													obj_t BgL_oclassz00_3023;

													{	/* Ast/app.scm 266 */
														obj_t BgL_arg1815z00_3031;
														long BgL_arg1816z00_3032;

														BgL_arg1815z00_3031 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Ast/app.scm 266 */
															long BgL_arg1817z00_3033;

															BgL_arg1817z00_3033 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3009);
															BgL_arg1816z00_3032 =
																(BgL_arg1817z00_3033 - OBJECT_TYPE);
														}
														BgL_oclassz00_3023 =
															VECTOR_REF(BgL_arg1815z00_3031,
															BgL_arg1816z00_3032);
													}
													{	/* Ast/app.scm 266 */
														bool_t BgL__ortest_1115z00_3024;

														BgL__ortest_1115z00_3024 =
															(BgL_classz00_3007 == BgL_oclassz00_3023);
														if (BgL__ortest_1115z00_3024)
															{	/* Ast/app.scm 266 */
																BgL_res2301z00_3040 = BgL__ortest_1115z00_3024;
															}
														else
															{	/* Ast/app.scm 266 */
																long BgL_odepthz00_3025;

																{	/* Ast/app.scm 266 */
																	obj_t BgL_arg1804z00_3026;

																	BgL_arg1804z00_3026 = (BgL_oclassz00_3023);
																	BgL_odepthz00_3025 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_3026);
																}
																if ((3L < BgL_odepthz00_3025))
																	{	/* Ast/app.scm 266 */
																		obj_t BgL_arg1802z00_3028;

																		{	/* Ast/app.scm 266 */
																			obj_t BgL_arg1803z00_3029;

																			BgL_arg1803z00_3029 =
																				(BgL_oclassz00_3023);
																			BgL_arg1802z00_3028 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_3029, 3L);
																		}
																		BgL_res2301z00_3040 =
																			(BgL_arg1802z00_3028 ==
																			BgL_classz00_3007);
																	}
																else
																	{	/* Ast/app.scm 266 */
																		BgL_res2301z00_3040 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2458z00_4807 = BgL_res2301z00_3040;
											}
									}
								}
								if (BgL_test2458z00_4807)
									{	/* Ast/app.scm 266 */
										obj_t BgL_tmpz00_4831;

										BgL_tmpz00_4831 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_funz00_1708)))->BgL_keysz00);
										BgL_test2457z00_4806 = PAIRP(BgL_tmpz00_4831);
									}
								else
									{	/* Ast/app.scm 266 */
										BgL_test2457z00_4806 = ((bool_t) 0);
									}
							}
							if (BgL_test2457z00_4806)
								{	/* Ast/app.scm 267 */
									obj_t BgL_argsz00_1728;

									{
										obj_t BgL_argsz00_1731;
										obj_t BgL_resz00_1732;

										BgL_argsz00_1731 = BgL_argsz00_16;
										BgL_resz00_1732 = BNIL;
									BgL_zc3z04anonymousza31833ze3z87_1733:
										if (NULLP(BgL_argsz00_1731))
											{	/* Ast/app.scm 269 */
												BgL_argsz00_1728 = bgl_reverse_bang(BgL_resz00_1732);
											}
										else
											{	/* Ast/app.scm 271 */
												obj_t BgL_az00_1735;
												obj_t BgL_locz00_1736;

												BgL_az00_1735 = CAR(((obj_t) BgL_argsz00_1731));
												BgL_locz00_1736 =
													BGl_findzd2locationzf2locz20zztools_locationz00
													(BgL_argsz00_1731, BgL_locz00_13);
												{	/* Ast/app.scm 273 */
													obj_t BgL_arg1835z00_1737;
													obj_t BgL_arg1836z00_1738;

													BgL_arg1835z00_1737 = CDR(((obj_t) BgL_argsz00_1731));
													{	/* Ast/app.scm 274 */
														BgL_nodez00_bglt BgL_arg1837z00_1739;

														BgL_arg1837z00_1739 =
															BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_az00_1735,
															BgL_stackz00_12, BgL_locz00_1736,
															CNST_TABLE_REF(2));
														BgL_arg1836z00_1738 =
															MAKE_YOUNG_PAIR(((obj_t) BgL_arg1837z00_1739),
															BgL_resz00_1732);
													}
													{
														obj_t BgL_resz00_4848;
														obj_t BgL_argsz00_4847;

														BgL_argsz00_4847 = BgL_arg1835z00_1737;
														BgL_resz00_4848 = BgL_arg1836z00_1738;
														BgL_resz00_1732 = BgL_resz00_4848;
														BgL_argsz00_1731 = BgL_argsz00_4847;
														goto BgL_zc3z04anonymousza31833ze3z87_1733;
													}
												}
											}
									}
									return
										BGl_makezd2keyszd2appzd2nodezd2zzast_appz00(BgL_stackz00_12,
										BgL_locz00_13, BgL_sitez00_14, BgL_varz00_15,
										BgL_argsz00_1728);
								}
							else
								{	/* Ast/app.scm 277 */
									bool_t BgL_test2463z00_4850;

									{	/* Ast/app.scm 277 */
										bool_t BgL_test2464z00_4851;

										{	/* Ast/app.scm 277 */
											obj_t BgL_classz00_3044;

											BgL_classz00_3044 = BGl_funz00zzast_varz00;
											{	/* Ast/app.scm 277 */
												BgL_objectz00_bglt BgL_arg1807z00_3046;

												{	/* Ast/app.scm 277 */
													obj_t BgL_tmpz00_4852;

													BgL_tmpz00_4852 =
														((obj_t) ((BgL_objectz00_bglt) BgL_funz00_1708));
													BgL_arg1807z00_3046 =
														(BgL_objectz00_bglt) (BgL_tmpz00_4852);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Ast/app.scm 277 */
														long BgL_idxz00_3052;

														BgL_idxz00_3052 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3046);
														BgL_test2464z00_4851 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3052 + 2L)) == BgL_classz00_3044);
													}
												else
													{	/* Ast/app.scm 277 */
														bool_t BgL_res2302z00_3077;

														{	/* Ast/app.scm 277 */
															obj_t BgL_oclassz00_3060;

															{	/* Ast/app.scm 277 */
																obj_t BgL_arg1815z00_3068;
																long BgL_arg1816z00_3069;

																BgL_arg1815z00_3068 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Ast/app.scm 277 */
																	long BgL_arg1817z00_3070;

																	BgL_arg1817z00_3070 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3046);
																	BgL_arg1816z00_3069 =
																		(BgL_arg1817z00_3070 - OBJECT_TYPE);
																}
																BgL_oclassz00_3060 =
																	VECTOR_REF(BgL_arg1815z00_3068,
																	BgL_arg1816z00_3069);
															}
															{	/* Ast/app.scm 277 */
																bool_t BgL__ortest_1115z00_3061;

																BgL__ortest_1115z00_3061 =
																	(BgL_classz00_3044 == BgL_oclassz00_3060);
																if (BgL__ortest_1115z00_3061)
																	{	/* Ast/app.scm 277 */
																		BgL_res2302z00_3077 =
																			BgL__ortest_1115z00_3061;
																	}
																else
																	{	/* Ast/app.scm 277 */
																		long BgL_odepthz00_3062;

																		{	/* Ast/app.scm 277 */
																			obj_t BgL_arg1804z00_3063;

																			BgL_arg1804z00_3063 =
																				(BgL_oclassz00_3060);
																			BgL_odepthz00_3062 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3063);
																		}
																		if ((2L < BgL_odepthz00_3062))
																			{	/* Ast/app.scm 277 */
																				obj_t BgL_arg1802z00_3065;

																				{	/* Ast/app.scm 277 */
																					obj_t BgL_arg1803z00_3066;

																					BgL_arg1803z00_3066 =
																						(BgL_oclassz00_3060);
																					BgL_arg1802z00_3065 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3066, 2L);
																				}
																				BgL_res2302z00_3077 =
																					(BgL_arg1802z00_3065 ==
																					BgL_classz00_3044);
																			}
																		else
																			{	/* Ast/app.scm 277 */
																				BgL_res2302z00_3077 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2464z00_4851 = BgL_res2302z00_3077;
													}
											}
										}
										if (BgL_test2464z00_4851)
											{	/* Ast/app.scm 277 */
												if (
													((((BgL_funz00_bglt) COBJECT(
																	((BgL_funz00_bglt) BgL_funz00_1708)))->
															BgL_arityz00) >= 0L))
													{	/* Ast/app.scm 277 */
														BgL_test2463z00_4850 = ((bool_t) 1);
													}
												else
													{	/* Ast/app.scm 277 */
														obj_t BgL_classz00_3080;

														BgL_classz00_3080 = BGl_cfunz00zzast_varz00;
														{	/* Ast/app.scm 277 */
															BgL_objectz00_bglt BgL_arg1807z00_3082;

															{	/* Ast/app.scm 277 */
																obj_t BgL_tmpz00_4879;

																BgL_tmpz00_4879 =
																	((obj_t)
																	((BgL_objectz00_bglt) BgL_funz00_1708));
																BgL_arg1807z00_3082 =
																	(BgL_objectz00_bglt) (BgL_tmpz00_4879);
															}
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Ast/app.scm 277 */
																	long BgL_idxz00_3088;

																	BgL_idxz00_3088 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_3082);
																	BgL_test2463z00_4850 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_3088 + 3L)) ==
																		BgL_classz00_3080);
																}
															else
																{	/* Ast/app.scm 277 */
																	bool_t BgL_res2303z00_3113;

																	{	/* Ast/app.scm 277 */
																		obj_t BgL_oclassz00_3096;

																		{	/* Ast/app.scm 277 */
																			obj_t BgL_arg1815z00_3104;
																			long BgL_arg1816z00_3105;

																			BgL_arg1815z00_3104 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Ast/app.scm 277 */
																				long BgL_arg1817z00_3106;

																				BgL_arg1817z00_3106 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_3082);
																				BgL_arg1816z00_3105 =
																					(BgL_arg1817z00_3106 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_3096 =
																				VECTOR_REF(BgL_arg1815z00_3104,
																				BgL_arg1816z00_3105);
																		}
																		{	/* Ast/app.scm 277 */
																			bool_t BgL__ortest_1115z00_3097;

																			BgL__ortest_1115z00_3097 =
																				(BgL_classz00_3080 ==
																				BgL_oclassz00_3096);
																			if (BgL__ortest_1115z00_3097)
																				{	/* Ast/app.scm 277 */
																					BgL_res2303z00_3113 =
																						BgL__ortest_1115z00_3097;
																				}
																			else
																				{	/* Ast/app.scm 277 */
																					long BgL_odepthz00_3098;

																					{	/* Ast/app.scm 277 */
																						obj_t BgL_arg1804z00_3099;

																						BgL_arg1804z00_3099 =
																							(BgL_oclassz00_3096);
																						BgL_odepthz00_3098 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_3099);
																					}
																					if ((3L < BgL_odepthz00_3098))
																						{	/* Ast/app.scm 277 */
																							obj_t BgL_arg1802z00_3101;

																							{	/* Ast/app.scm 277 */
																								obj_t BgL_arg1803z00_3102;

																								BgL_arg1803z00_3102 =
																									(BgL_oclassz00_3096);
																								BgL_arg1802z00_3101 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_3102, 3L);
																							}
																							BgL_res2303z00_3113 =
																								(BgL_arg1802z00_3101 ==
																								BgL_classz00_3080);
																						}
																					else
																						{	/* Ast/app.scm 277 */
																							BgL_res2303z00_3113 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2463z00_4850 = BgL_res2303z00_3113;
																}
														}
													}
											}
										else
											{	/* Ast/app.scm 277 */
												BgL_test2463z00_4850 = ((bool_t) 1);
											}
									}
									if (BgL_test2463z00_4850)
										{	/* Ast/app.scm 278 */
											obj_t BgL_argsz00_1746;

											{
												obj_t BgL_argsz00_1749;
												obj_t BgL_resz00_1750;

												BgL_argsz00_1749 = BgL_argsz00_16;
												BgL_resz00_1750 = BNIL;
											BgL_zc3z04anonymousza31844ze3z87_1751:
												if (NULLP(BgL_argsz00_1749))
													{	/* Ast/app.scm 280 */
														BgL_argsz00_1746 =
															bgl_reverse_bang(BgL_resz00_1750);
													}
												else
													{	/* Ast/app.scm 282 */
														obj_t BgL_az00_1753;
														obj_t BgL_locz00_1754;

														BgL_az00_1753 = CAR(((obj_t) BgL_argsz00_1749));
														BgL_locz00_1754 =
															BGl_findzd2locationzf2locz20zztools_locationz00
															(BgL_argsz00_1749, BgL_locz00_13);
														{	/* Ast/app.scm 284 */
															obj_t BgL_arg1846z00_1755;
															obj_t BgL_arg1847z00_1756;

															BgL_arg1846z00_1755 =
																CDR(((obj_t) BgL_argsz00_1749));
															{	/* Ast/app.scm 285 */
																BgL_nodez00_bglt BgL_arg1848z00_1757;

																BgL_arg1848z00_1757 =
																	BGl_sexpzd2ze3nodez31zzast_sexpz00
																	(BgL_az00_1753, BgL_stackz00_12,
																	BgL_locz00_1754, CNST_TABLE_REF(2));
																BgL_arg1847z00_1756 =
																	MAKE_YOUNG_PAIR(((obj_t) BgL_arg1848z00_1757),
																	BgL_resz00_1750);
															}
															{
																obj_t BgL_resz00_4915;
																obj_t BgL_argsz00_4914;

																BgL_argsz00_4914 = BgL_arg1846z00_1755;
																BgL_resz00_4915 = BgL_arg1847z00_1756;
																BgL_resz00_1750 = BgL_resz00_4915;
																BgL_argsz00_1749 = BgL_argsz00_4914;
																goto BgL_zc3z04anonymousza31844ze3z87_1751;
															}
														}
													}
											}
											return
												((BgL_nodez00_bglt)
												BGl_makezd2fxzd2appzd2nodezd2zzast_appz00(BgL_locz00_13,
													BgL_varz00_15, BgL_argsz00_1746));
										}
									else
										{	/* Ast/app.scm 289 */
											long BgL_arg1849z00_1759;

											BgL_arg1849z00_1759 =
												(((BgL_funz00_bglt) COBJECT(
														((BgL_funz00_bglt) BgL_funz00_1708)))->
												BgL_arityz00);
											return
												BGl_makezd2vazd2appzd2nodezd2zzast_appz00
												(BgL_arg1849z00_1759, BgL_stackz00_12, BgL_locz00_13,
												BgL_varz00_15, BgL_argsz00_16);
										}
								}
						}
				}
			}
		}

	}



/* &make-app-node */
	BgL_nodez00_bglt BGl_z62makezd2appzd2nodez62zzast_appz00(obj_t
		BgL_envz00_4157, obj_t BgL_stackz00_4158, obj_t BgL_locz00_4159,
		obj_t BgL_sitez00_4160, obj_t BgL_varz00_4161, obj_t BgL_argsz00_4162)
	{
		{	/* Ast/app.scm 252 */
			return
				BGl_makezd2appzd2nodez00zzast_appz00(BgL_stackz00_4158, BgL_locz00_4159,
				BgL_sitez00_4160, ((BgL_varz00_bglt) BgL_varz00_4161),
				BgL_argsz00_4162);
		}

	}



/* make-optionals-app-node */
	BgL_nodez00_bglt BGl_makezd2optionalszd2appzd2nodezd2zzast_appz00(obj_t
		BgL_stackz00_17, obj_t BgL_locz00_18, obj_t BgL_sitez00_19,
		BgL_varz00_bglt BgL_varz00_20, obj_t BgL_actualsz00_21)
	{
		{	/* Ast/app.scm 294 */
			{	/* Ast/app.scm 295 */
				BgL_variablez00_bglt BgL_vz00_1768;
				long BgL_lenz00_1769;

				BgL_vz00_1768 =
					(((BgL_varz00_bglt) COBJECT(BgL_varz00_20))->BgL_variablez00);
				BgL_lenz00_1769 = bgl_list_length(BgL_actualsz00_21);
				{	/* Ast/app.scm 297 */
					BgL_sfunz00_bglt BgL_i1115z00_1770;

					BgL_i1115z00_1770 =
						((BgL_sfunz00_bglt)
						(((BgL_variablez00_bglt) COBJECT(BgL_vz00_1768))->BgL_valuez00));
					if (
						(BgL_lenz00_1769 ==
							((((BgL_funz00_bglt) COBJECT(
											((BgL_funz00_bglt) BgL_i1115z00_1770)))->BgL_arityz00) +
								bgl_list_length(
									(((BgL_sfunz00_bglt) COBJECT(BgL_i1115z00_1770))->
										BgL_optionalsz00)))))
						{	/* Ast/app.scm 300 */
							BgL_appz00_bglt BgL_new1118z00_1776;

							{	/* Ast/app.scm 301 */
								BgL_appz00_bglt BgL_new1116z00_1784;

								BgL_new1116z00_1784 =
									((BgL_appz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_appz00_bgl))));
								{	/* Ast/app.scm 301 */
									long BgL_arg1863z00_1785;

									BgL_arg1863z00_1785 = BGL_CLASS_NUM(BGl_appz00zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1116z00_1784),
										BgL_arg1863z00_1785);
								}
								{	/* Ast/app.scm 301 */
									BgL_objectz00_bglt BgL_tmpz00_4938;

									BgL_tmpz00_4938 = ((BgL_objectz00_bglt) BgL_new1116z00_1784);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4938, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1116z00_1784);
								BgL_new1118z00_1776 = BgL_new1116z00_1784;
							}
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt) BgL_new1118z00_1776)))->BgL_locz00) =
								((obj_t) BgL_locz00_18), BUNSPEC);
							{
								BgL_typez00_bglt BgL_auxz00_4944;

								{	/* Ast/app.scm 302 */
									BgL_typez00_bglt BgL_arg1860z00_1777;

									BgL_arg1860z00_1777 =
										(((BgL_variablez00_bglt) COBJECT(BgL_vz00_1768))->
										BgL_typez00);
									BgL_auxz00_4944 =
										BGl_strictzd2nodezd2typez00zzast_nodez00(((BgL_typez00_bglt)
											BGl_za2_za2z00zztype_cachez00), BgL_arg1860z00_1777);
								}
								((((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt) BgL_new1118z00_1776)))->
										BgL_typez00) =
									((BgL_typez00_bglt) BgL_auxz00_4944), BUNSPEC);
							}
							((((BgL_nodezf2effectzf2_bglt) COBJECT(
											((BgL_nodezf2effectzf2_bglt) BgL_new1118z00_1776)))->
									BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
							((((BgL_nodezf2effectzf2_bglt)
										COBJECT(((BgL_nodezf2effectzf2_bglt)
												BgL_new1118z00_1776)))->BgL_keyz00) =
								((obj_t) BINT(-1L)), BUNSPEC);
							{
								BgL_varz00_bglt BgL_auxz00_4955;

								{	/* Ast/app.scm 303 */
									bool_t BgL_test2474z00_4956;

									{	/* Ast/app.scm 303 */
										obj_t BgL_classz00_3128;

										BgL_classz00_3128 = BGl_closurez00zzast_nodez00;
										{	/* Ast/app.scm 303 */
											BgL_objectz00_bglt BgL_arg1807z00_3130;

											{	/* Ast/app.scm 303 */
												obj_t BgL_tmpz00_4957;

												BgL_tmpz00_4957 =
													((obj_t) ((BgL_objectz00_bglt) BgL_varz00_20));
												BgL_arg1807z00_3130 =
													(BgL_objectz00_bglt) (BgL_tmpz00_4957);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Ast/app.scm 303 */
													long BgL_idxz00_3136;

													BgL_idxz00_3136 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3130);
													{	/* Ast/app.scm 303 */
														obj_t BgL_arg1800z00_3137;

														{	/* Ast/app.scm 303 */
															long BgL_arg1801z00_3138;

															BgL_arg1801z00_3138 = (BgL_idxz00_3136 + 3L);
															{	/* Ast/app.scm 303 */
																obj_t BgL_vectorz00_3140;

																BgL_vectorz00_3140 =
																	BGl_za2inheritancesza2z00zz__objectz00;
																BgL_arg1800z00_3137 =
																	VECTOR_REF(BgL_vectorz00_3140,
																	BgL_arg1801z00_3138);
														}}
														BgL_test2474z00_4956 =
															(BgL_arg1800z00_3137 == BgL_classz00_3128);
												}}
											else
												{	/* Ast/app.scm 303 */
													bool_t BgL_res2304z00_3161;

													{	/* Ast/app.scm 303 */
														obj_t BgL_oclassz00_3144;

														{	/* Ast/app.scm 303 */
															obj_t BgL_arg1815z00_3152;
															long BgL_arg1816z00_3153;

															BgL_arg1815z00_3152 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Ast/app.scm 303 */
																long BgL_arg1817z00_3154;

																BgL_arg1817z00_3154 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3130);
																BgL_arg1816z00_3153 =
																	(BgL_arg1817z00_3154 - OBJECT_TYPE);
															}
															BgL_oclassz00_3144 =
																VECTOR_REF(BgL_arg1815z00_3152,
																BgL_arg1816z00_3153);
														}
														{	/* Ast/app.scm 303 */
															bool_t BgL__ortest_1115z00_3145;

															BgL__ortest_1115z00_3145 =
																(BgL_classz00_3128 == BgL_oclassz00_3144);
															if (BgL__ortest_1115z00_3145)
																{	/* Ast/app.scm 303 */
																	BgL_res2304z00_3161 =
																		BgL__ortest_1115z00_3145;
																}
															else
																{	/* Ast/app.scm 303 */
																	long BgL_odepthz00_3146;

																	{	/* Ast/app.scm 303 */
																		obj_t BgL_arg1804z00_3147;

																		BgL_arg1804z00_3147 = (BgL_oclassz00_3144);
																		BgL_odepthz00_3146 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_3147);
																	}
																	if ((3L < BgL_odepthz00_3146))
																		{	/* Ast/app.scm 303 */
																			obj_t BgL_arg1802z00_3149;

																			{	/* Ast/app.scm 303 */
																				obj_t BgL_arg1803z00_3150;

																				BgL_arg1803z00_3150 =
																					(BgL_oclassz00_3144);
																				BgL_arg1802z00_3149 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_3150, 3L);
																			}
																			BgL_res2304z00_3161 =
																				(BgL_arg1802z00_3149 ==
																				BgL_classz00_3128);
																		}
																	else
																		{	/* Ast/app.scm 303 */
																			BgL_res2304z00_3161 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2474z00_4956 = BgL_res2304z00_3161;
												}
										}
									}
									if (BgL_test2474z00_4956)
										{	/* Ast/app.scm 303 */
											BgL_refz00_bglt BgL_new1119z00_1780;

											{	/* Ast/app.scm 303 */
												BgL_refz00_bglt BgL_new1123z00_1782;

												BgL_new1123z00_1782 =
													((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																BgL_refz00_bgl))));
												{	/* Ast/app.scm 303 */
													long BgL_arg1862z00_1783;

													{	/* Ast/app.scm 303 */
														obj_t BgL_classz00_3162;

														BgL_classz00_3162 = BGl_refz00zzast_nodez00;
														BgL_arg1862z00_1783 =
															BGL_CLASS_NUM(BgL_classz00_3162);
													}
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt) BgL_new1123z00_1782),
														BgL_arg1862z00_1783);
												}
												{	/* Ast/app.scm 303 */
													BgL_objectz00_bglt BgL_tmpz00_4984;

													BgL_tmpz00_4984 =
														((BgL_objectz00_bglt) BgL_new1123z00_1782);
													BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4984, BFALSE);
												}
												((BgL_objectz00_bglt) BgL_new1123z00_1782);
												BgL_new1119z00_1780 = BgL_new1123z00_1782;
											}
											((((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt) BgL_new1119z00_1780)))->
													BgL_locz00) =
												((obj_t) (((BgL_nodez00_bglt)
															COBJECT(((BgL_nodez00_bglt) BgL_varz00_20)))->
														BgL_locz00)), BUNSPEC);
											((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																BgL_new1119z00_1780)))->BgL_typez00) =
												((BgL_typez00_bglt) (((BgL_nodez00_bglt)
															COBJECT(((BgL_nodez00_bglt) BgL_varz00_20)))->
														BgL_typez00)), BUNSPEC);
											((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																BgL_new1119z00_1780)))->BgL_variablez00) =
												((BgL_variablez00_bglt) (((BgL_varz00_bglt)
															COBJECT(((BgL_varz00_bglt) ((BgL_nodez00_bglt)
																		BgL_varz00_20))))->BgL_variablez00)),
												BUNSPEC);
											BgL_auxz00_4955 = ((BgL_varz00_bglt) BgL_new1119z00_1780);
										}
									else
										{	/* Ast/app.scm 303 */
											BgL_auxz00_4955 = BgL_varz00_20;
										}
								}
								((((BgL_appz00_bglt) COBJECT(BgL_new1118z00_1776))->
										BgL_funz00) = ((BgL_varz00_bglt) BgL_auxz00_4955), BUNSPEC);
							}
							((((BgL_appz00_bglt) COBJECT(BgL_new1118z00_1776))->BgL_argsz00) =
								((obj_t) BgL_actualsz00_21), BUNSPEC);
							((((BgL_appz00_bglt) COBJECT(BgL_new1118z00_1776))->
									BgL_stackablez00) = ((obj_t) BFALSE), BUNSPEC);
							return ((BgL_nodez00_bglt) BgL_new1118z00_1776);
						}
					else
						{	/* Ast/app.scm 306 */
							obj_t BgL_reqsz00_1786;

							BgL_reqsz00_1786 =
								BGl_takez00zz__r4_pairs_and_lists_6_3z00(
								(((BgL_sfunz00_bglt) COBJECT(BgL_i1115z00_1770))->
									BgL_argszd2namezd2),
								(((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
												BgL_i1115z00_1770)))->BgL_arityz00));
							{	/* Ast/app.scm 306 */
								obj_t BgL_provsz00_1787;

								{	/* Ast/app.scm 307 */
									obj_t BgL_l1303z00_1892;

									BgL_l1303z00_1892 =
										BGl_takez00zz__r4_pairs_and_lists_6_3z00(
										(((BgL_sfunz00_bglt) COBJECT(BgL_i1115z00_1770))->
											BgL_optionalsz00),
										(BgL_lenz00_1769 -
											(((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_i1115z00_1770)))->BgL_arityz00)));
									if (NULLP(BgL_l1303z00_1892))
										{	/* Ast/app.scm 307 */
											BgL_provsz00_1787 = BNIL;
										}
									else
										{	/* Ast/app.scm 307 */
											obj_t BgL_head1305z00_1894;

											BgL_head1305z00_1894 =
												MAKE_YOUNG_PAIR(CAR(CAR(BgL_l1303z00_1892)), BNIL);
											{	/* Ast/app.scm 307 */
												obj_t BgL_g1308z00_1895;

												BgL_g1308z00_1895 = CDR(BgL_l1303z00_1892);
												{
													obj_t BgL_l1303z00_1897;
													obj_t BgL_tail1306z00_1898;

													BgL_l1303z00_1897 = BgL_g1308z00_1895;
													BgL_tail1306z00_1898 = BgL_head1305z00_1894;
												BgL_zc3z04anonymousza31940ze3z87_1899:
													if (NULLP(BgL_l1303z00_1897))
														{	/* Ast/app.scm 307 */
															BgL_provsz00_1787 = BgL_head1305z00_1894;
														}
													else
														{	/* Ast/app.scm 307 */
															obj_t BgL_newtail1307z00_1901;

															{	/* Ast/app.scm 307 */
																obj_t BgL_arg1943z00_1903;

																{	/* Ast/app.scm 307 */
																	obj_t BgL_pairz00_3172;

																	BgL_pairz00_3172 =
																		CAR(((obj_t) BgL_l1303z00_1897));
																	BgL_arg1943z00_1903 = CAR(BgL_pairz00_3172);
																}
																BgL_newtail1307z00_1901 =
																	MAKE_YOUNG_PAIR(BgL_arg1943z00_1903, BNIL);
															}
															SET_CDR(BgL_tail1306z00_1898,
																BgL_newtail1307z00_1901);
															{	/* Ast/app.scm 307 */
																obj_t BgL_arg1942z00_1902;

																BgL_arg1942z00_1902 =
																	CDR(((obj_t) BgL_l1303z00_1897));
																{
																	obj_t BgL_tail1306z00_5031;
																	obj_t BgL_l1303z00_5030;

																	BgL_l1303z00_5030 = BgL_arg1942z00_1902;
																	BgL_tail1306z00_5031 =
																		BgL_newtail1307z00_1901;
																	BgL_tail1306z00_1898 = BgL_tail1306z00_5031;
																	BgL_l1303z00_1897 = BgL_l1303z00_5030;
																	goto BgL_zc3z04anonymousza31940ze3z87_1899;
																}
															}
														}
												}
											}
										}
								}
								{	/* Ast/app.scm 307 */
									obj_t BgL_optsz00_1788;

									BgL_optsz00_1788 =
										BGl_listzd2tailzd2zz__r4_pairs_and_lists_6_3z00(
										(((BgL_sfunz00_bglt) COBJECT(BgL_i1115z00_1770))->
											BgL_optionalsz00),
										(BgL_lenz00_1769 -
											(((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_i1115z00_1770)))->BgL_arityz00)));
									{	/* Ast/app.scm 308 */
										obj_t BgL_expz00_1789;

										{	/* Ast/app.scm 309 */
											obj_t BgL_arg1868z00_1792;
											obj_t BgL_arg1869z00_1793;

											BgL_arg1868z00_1792 = BGl_letzd2symzd2zzast_letz00();
											{	/* Ast/app.scm 309 */
												obj_t BgL_arg1870z00_1794;
												obj_t BgL_arg1872z00_1795;

												{	/* Ast/app.scm 309 */
													obj_t BgL_ll1310z00_1797;

													BgL_ll1310z00_1797 =
														BGl_takez00zz__r4_pairs_and_lists_6_3z00
														(BgL_actualsz00_21,
														(((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																		BgL_i1115z00_1770)))->BgL_arityz00));
													if (NULLP(BgL_reqsz00_1786))
														{	/* Ast/app.scm 309 */
															BgL_arg1870z00_1794 = BNIL;
														}
													else
														{	/* Ast/app.scm 309 */
															obj_t BgL_head1311z00_1799;

															{	/* Ast/app.scm 309 */
																obj_t BgL_arg1883z00_1817;

																{	/* Ast/app.scm 309 */
																	obj_t BgL_arg1884z00_1818;
																	obj_t BgL_arg1885z00_1819;

																	BgL_arg1884z00_1818 =
																		CAR(((obj_t) BgL_reqsz00_1786));
																	BgL_arg1885z00_1819 =
																		CAR(((obj_t) BgL_ll1310z00_1797));
																	{	/* Ast/app.scm 309 */
																		obj_t BgL_list1886z00_1820;

																		{	/* Ast/app.scm 309 */
																			obj_t BgL_arg1887z00_1821;

																			BgL_arg1887z00_1821 =
																				MAKE_YOUNG_PAIR(BgL_arg1885z00_1819,
																				BNIL);
																			BgL_list1886z00_1820 =
																				MAKE_YOUNG_PAIR(BgL_arg1884z00_1818,
																				BgL_arg1887z00_1821);
																		}
																		BgL_arg1883z00_1817 = BgL_list1886z00_1820;
																	}
																}
																BgL_head1311z00_1799 =
																	MAKE_YOUNG_PAIR(BgL_arg1883z00_1817, BNIL);
															}
															{	/* Ast/app.scm 309 */
																obj_t BgL_g1315z00_1800;
																obj_t BgL_g1316z00_1801;

																BgL_g1315z00_1800 =
																	CDR(((obj_t) BgL_reqsz00_1786));
																BgL_g1316z00_1801 =
																	CDR(((obj_t) BgL_ll1310z00_1797));
																{
																	obj_t BgL_ll1309z00_1803;
																	obj_t BgL_ll1310z00_1804;
																	obj_t BgL_tail1312z00_1805;

																	BgL_ll1309z00_1803 = BgL_g1315z00_1800;
																	BgL_ll1310z00_1804 = BgL_g1316z00_1801;
																	BgL_tail1312z00_1805 = BgL_head1311z00_1799;
																BgL_zc3z04anonymousza31874ze3z87_1806:
																	if (NULLP(BgL_ll1309z00_1803))
																		{	/* Ast/app.scm 309 */
																			BgL_arg1870z00_1794 =
																				BgL_head1311z00_1799;
																		}
																	else
																		{	/* Ast/app.scm 309 */
																			obj_t BgL_newtail1313z00_1808;

																			{	/* Ast/app.scm 309 */
																				obj_t BgL_arg1878z00_1811;

																				{	/* Ast/app.scm 309 */
																					obj_t BgL_arg1879z00_1812;
																					obj_t BgL_arg1880z00_1813;

																					BgL_arg1879z00_1812 =
																						CAR(((obj_t) BgL_ll1309z00_1803));
																					BgL_arg1880z00_1813 =
																						CAR(((obj_t) BgL_ll1310z00_1804));
																					{	/* Ast/app.scm 309 */
																						obj_t BgL_list1881z00_1814;

																						{	/* Ast/app.scm 309 */
																							obj_t BgL_arg1882z00_1815;

																							BgL_arg1882z00_1815 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1880z00_1813, BNIL);
																							BgL_list1881z00_1814 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1879z00_1812,
																								BgL_arg1882z00_1815);
																						}
																						BgL_arg1878z00_1811 =
																							BgL_list1881z00_1814;
																					}
																				}
																				BgL_newtail1313z00_1808 =
																					MAKE_YOUNG_PAIR(BgL_arg1878z00_1811,
																					BNIL);
																			}
																			SET_CDR(BgL_tail1312z00_1805,
																				BgL_newtail1313z00_1808);
																			{	/* Ast/app.scm 309 */
																				obj_t BgL_arg1876z00_1809;
																				obj_t BgL_arg1877z00_1810;

																				BgL_arg1876z00_1809 =
																					CDR(((obj_t) BgL_ll1309z00_1803));
																				BgL_arg1877z00_1810 =
																					CDR(((obj_t) BgL_ll1310z00_1804));
																				{
																					obj_t BgL_tail1312z00_5070;
																					obj_t BgL_ll1310z00_5069;
																					obj_t BgL_ll1309z00_5068;

																					BgL_ll1309z00_5068 =
																						BgL_arg1876z00_1809;
																					BgL_ll1310z00_5069 =
																						BgL_arg1877z00_1810;
																					BgL_tail1312z00_5070 =
																						BgL_newtail1313z00_1808;
																					BgL_tail1312z00_1805 =
																						BgL_tail1312z00_5070;
																					BgL_ll1310z00_1804 =
																						BgL_ll1310z00_5069;
																					BgL_ll1309z00_1803 =
																						BgL_ll1309z00_5068;
																					goto
																						BgL_zc3z04anonymousza31874ze3z87_1806;
																				}
																			}
																		}
																}
															}
														}
												}
												{	/* Ast/app.scm 310 */
													obj_t BgL_arg1889z00_1823;

													{	/* Ast/app.scm 310 */
														obj_t BgL_arg1890z00_1824;

														{	/* Ast/app.scm 310 */
															obj_t BgL_arg1891z00_1825;
															obj_t BgL_arg1892z00_1826;

															{	/* Ast/app.scm 310 */
																obj_t BgL_arg1893z00_1827;
																obj_t BgL_arg1894z00_1828;

																{	/* Ast/app.scm 310 */
																	obj_t BgL_ll1318z00_1830;

																	BgL_ll1318z00_1830 =
																		BGl_dropz00zz__r4_pairs_and_lists_6_3z00
																		(BgL_actualsz00_21,
																		(((BgL_funz00_bglt)
																				COBJECT(((BgL_funz00_bglt)
																						BgL_i1115z00_1770)))->
																			BgL_arityz00));
																	if (NULLP(BgL_provsz00_1787))
																		{	/* Ast/app.scm 310 */
																			BgL_arg1893z00_1827 = BNIL;
																		}
																	else
																		{	/* Ast/app.scm 310 */
																			obj_t BgL_head1319z00_1832;

																			{	/* Ast/app.scm 310 */
																				obj_t BgL_arg1910z00_1850;

																				{	/* Ast/app.scm 310 */
																					obj_t BgL_arg1911z00_1851;
																					obj_t BgL_arg1912z00_1852;

																					BgL_arg1911z00_1851 =
																						CAR(((obj_t) BgL_provsz00_1787));
																					BgL_arg1912z00_1852 =
																						CAR(((obj_t) BgL_ll1318z00_1830));
																					{	/* Ast/app.scm 310 */
																						obj_t BgL_list1913z00_1853;

																						{	/* Ast/app.scm 310 */
																							obj_t BgL_arg1914z00_1854;

																							BgL_arg1914z00_1854 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1912z00_1852, BNIL);
																							BgL_list1913z00_1853 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1911z00_1851,
																								BgL_arg1914z00_1854);
																						}
																						BgL_arg1910z00_1850 =
																							BgL_list1913z00_1853;
																					}
																				}
																				BgL_head1319z00_1832 =
																					MAKE_YOUNG_PAIR(BgL_arg1910z00_1850,
																					BNIL);
																			}
																			{	/* Ast/app.scm 310 */
																				obj_t BgL_g1323z00_1833;
																				obj_t BgL_g1324z00_1834;

																				BgL_g1323z00_1833 =
																					CDR(((obj_t) BgL_provsz00_1787));
																				BgL_g1324z00_1834 =
																					CDR(((obj_t) BgL_ll1318z00_1830));
																				{
																					obj_t BgL_ll1317z00_1836;
																					obj_t BgL_ll1318z00_1837;
																					obj_t BgL_tail1320z00_1838;

																					BgL_ll1317z00_1836 =
																						BgL_g1323z00_1833;
																					BgL_ll1318z00_1837 =
																						BgL_g1324z00_1834;
																					BgL_tail1320z00_1838 =
																						BgL_head1319z00_1832;
																				BgL_zc3z04anonymousza31896ze3z87_1839:
																					if (NULLP(BgL_ll1317z00_1836))
																						{	/* Ast/app.scm 310 */
																							BgL_arg1893z00_1827 =
																								BgL_head1319z00_1832;
																						}
																					else
																						{	/* Ast/app.scm 310 */
																							obj_t BgL_newtail1321z00_1841;

																							{	/* Ast/app.scm 310 */
																								obj_t BgL_arg1901z00_1844;

																								{	/* Ast/app.scm 310 */
																									obj_t BgL_arg1902z00_1845;
																									obj_t BgL_arg1903z00_1846;

																									BgL_arg1902z00_1845 =
																										CAR(
																										((obj_t)
																											BgL_ll1317z00_1836));
																									BgL_arg1903z00_1846 =
																										CAR(((obj_t)
																											BgL_ll1318z00_1837));
																									{	/* Ast/app.scm 310 */
																										obj_t BgL_list1904z00_1847;

																										{	/* Ast/app.scm 310 */
																											obj_t BgL_arg1906z00_1848;

																											BgL_arg1906z00_1848 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1903z00_1846,
																												BNIL);
																											BgL_list1904z00_1847 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1902z00_1845,
																												BgL_arg1906z00_1848);
																										}
																										BgL_arg1901z00_1844 =
																											BgL_list1904z00_1847;
																									}
																								}
																								BgL_newtail1321z00_1841 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1901z00_1844, BNIL);
																							}
																							SET_CDR(BgL_tail1320z00_1838,
																								BgL_newtail1321z00_1841);
																							{	/* Ast/app.scm 310 */
																								obj_t BgL_arg1898z00_1842;
																								obj_t BgL_arg1899z00_1843;

																								BgL_arg1898z00_1842 =
																									CDR(
																									((obj_t) BgL_ll1317z00_1836));
																								BgL_arg1899z00_1843 =
																									CDR(
																									((obj_t) BgL_ll1318z00_1837));
																								{
																									obj_t BgL_tail1320z00_5103;
																									obj_t BgL_ll1318z00_5102;
																									obj_t BgL_ll1317z00_5101;

																									BgL_ll1317z00_5101 =
																										BgL_arg1898z00_1842;
																									BgL_ll1318z00_5102 =
																										BgL_arg1899z00_1843;
																									BgL_tail1320z00_5103 =
																										BgL_newtail1321z00_1841;
																									BgL_tail1320z00_1838 =
																										BgL_tail1320z00_5103;
																									BgL_ll1318z00_1837 =
																										BgL_ll1318z00_5102;
																									BgL_ll1317z00_1836 =
																										BgL_ll1317z00_5101;
																									goto
																										BgL_zc3z04anonymousza31896ze3z87_1839;
																								}
																							}
																						}
																				}
																			}
																		}
																}
																BgL_arg1894z00_1828 =
																	BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																	(BgL_optsz00_1788, BNIL);
																BgL_arg1891z00_1825 =
																	BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																	(BgL_arg1893z00_1827, BgL_arg1894z00_1828);
															}
															{	/* Ast/app.scm 313 */
																obj_t BgL_arg1917z00_1856;

																{	/* Ast/app.scm 313 */
																	obj_t BgL_arg1918z00_1857;

																	{	/* Ast/app.scm 313 */
																		obj_t BgL_arg1919z00_1858;

																		{	/* Ast/app.scm 313 */
																			obj_t BgL_arg1920z00_1859;
																			obj_t BgL_arg1923z00_1860;

																			if (NULLP(BgL_provsz00_1787))
																				{	/* Ast/app.scm 313 */
																					BgL_arg1920z00_1859 = BNIL;
																				}
																			else
																				{	/* Ast/app.scm 313 */
																					obj_t BgL_head1327z00_1863;

																					BgL_head1327z00_1863 =
																						MAKE_YOUNG_PAIR(BNIL, BNIL);
																					{
																						obj_t BgL_l1325z00_1865;
																						obj_t BgL_tail1328z00_1866;

																						BgL_l1325z00_1865 =
																							BgL_provsz00_1787;
																						BgL_tail1328z00_1866 =
																							BgL_head1327z00_1863;
																					BgL_zc3z04anonymousza31925ze3z87_1867:
																						if (NULLP
																							(BgL_l1325z00_1865))
																							{	/* Ast/app.scm 313 */
																								BgL_arg1920z00_1859 =
																									CDR(BgL_head1327z00_1863);
																							}
																						else
																							{	/* Ast/app.scm 313 */
																								obj_t BgL_newtail1329z00_1869;

																								{	/* Ast/app.scm 313 */
																									obj_t BgL_arg1928z00_1871;

																									{	/* Ast/app.scm 313 */
																										obj_t BgL_iz00_1872;

																										BgL_iz00_1872 =
																											CAR(
																											((obj_t)
																												BgL_l1325z00_1865));
																										BgL_arg1928z00_1871 =
																											BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																											(BgL_iz00_1872,
																											BgL_locz00_18);
																									}
																									BgL_newtail1329z00_1869 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1928z00_1871, BNIL);
																								}
																								SET_CDR(BgL_tail1328z00_1866,
																									BgL_newtail1329z00_1869);
																								{	/* Ast/app.scm 313 */
																									obj_t BgL_arg1927z00_1870;

																									BgL_arg1927z00_1870 =
																										CDR(
																										((obj_t)
																											BgL_l1325z00_1865));
																									{
																										obj_t BgL_tail1328z00_5120;
																										obj_t BgL_l1325z00_5119;

																										BgL_l1325z00_5119 =
																											BgL_arg1927z00_1870;
																										BgL_tail1328z00_5120 =
																											BgL_newtail1329z00_1869;
																										BgL_tail1328z00_1866 =
																											BgL_tail1328z00_5120;
																										BgL_l1325z00_1865 =
																											BgL_l1325z00_5119;
																										goto
																											BgL_zc3z04anonymousza31925ze3z87_1867;
																									}
																								}
																							}
																					}
																				}
																			{	/* Ast/app.scm 316 */
																				obj_t BgL_arg1929z00_1874;

																				if (NULLP(BgL_optsz00_1788))
																					{	/* Ast/app.scm 316 */
																						BgL_arg1929z00_1874 = BNIL;
																					}
																				else
																					{	/* Ast/app.scm 316 */
																						obj_t BgL_head1332z00_1877;

																						BgL_head1332z00_1877 =
																							MAKE_YOUNG_PAIR(BNIL, BNIL);
																						{
																							obj_t BgL_l1330z00_1879;
																							obj_t BgL_tail1333z00_1880;

																							BgL_l1330z00_1879 =
																								BgL_optsz00_1788;
																							BgL_tail1333z00_1880 =
																								BgL_head1332z00_1877;
																						BgL_zc3z04anonymousza31931ze3z87_1881:
																							if (NULLP
																								(BgL_l1330z00_1879))
																								{	/* Ast/app.scm 316 */
																									BgL_arg1929z00_1874 =
																										CDR(BgL_head1332z00_1877);
																								}
																							else
																								{	/* Ast/app.scm 316 */
																									obj_t BgL_newtail1334z00_1883;

																									{	/* Ast/app.scm 316 */
																										obj_t BgL_arg1934z00_1885;

																										{	/* Ast/app.scm 316 */
																											obj_t BgL_oz00_1886;

																											BgL_oz00_1886 =
																												CAR(
																												((obj_t)
																													BgL_l1330z00_1879));
																											{	/* Ast/app.scm 317 */
																												obj_t
																													BgL_arg1935z00_1887;
																												BgL_arg1935z00_1887 =
																													CAR(((obj_t)
																														BgL_oz00_1886));
																												BgL_arg1934z00_1885 =
																													BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																													(BgL_arg1935z00_1887,
																													BgL_locz00_18);
																											}
																										}
																										BgL_newtail1334z00_1883 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1934z00_1885,
																											BNIL);
																									}
																									SET_CDR(BgL_tail1333z00_1880,
																										BgL_newtail1334z00_1883);
																									{	/* Ast/app.scm 316 */
																										obj_t BgL_arg1933z00_1884;

																										BgL_arg1933z00_1884 =
																											CDR(
																											((obj_t)
																												BgL_l1330z00_1879));
																										{
																											obj_t
																												BgL_tail1333z00_5137;
																											obj_t BgL_l1330z00_5136;

																											BgL_l1330z00_5136 =
																												BgL_arg1933z00_1884;
																											BgL_tail1333z00_5137 =
																												BgL_newtail1334z00_1883;
																											BgL_tail1333z00_1880 =
																												BgL_tail1333z00_5137;
																											BgL_l1330z00_1879 =
																												BgL_l1330z00_5136;
																											goto
																												BgL_zc3z04anonymousza31931ze3z87_1881;
																										}
																									}
																								}
																						}
																					}
																				BgL_arg1923z00_1860 =
																					BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																					(BgL_arg1929z00_1874, BNIL);
																			}
																			BgL_arg1919z00_1858 =
																				BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																				(BgL_arg1920z00_1859,
																				BgL_arg1923z00_1860);
																		}
																		BgL_arg1918z00_1857 =
																			BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																			(BgL_reqsz00_1786, BgL_arg1919z00_1858);
																	}
																	BgL_arg1917z00_1856 =
																		MAKE_YOUNG_PAIR(
																		((obj_t) BgL_varz00_20),
																		BgL_arg1918z00_1857);
																}
																BgL_arg1892z00_1826 =
																	MAKE_YOUNG_PAIR(BgL_arg1917z00_1856, BNIL);
															}
															BgL_arg1890z00_1824 =
																MAKE_YOUNG_PAIR(BgL_arg1891z00_1825,
																BgL_arg1892z00_1826);
														}
														BgL_arg1889z00_1823 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(8),
															BgL_arg1890z00_1824);
													}
													BgL_arg1872z00_1795 =
														MAKE_YOUNG_PAIR(BgL_arg1889z00_1823, BNIL);
												}
												BgL_arg1869z00_1793 =
													MAKE_YOUNG_PAIR(BgL_arg1870z00_1794,
													BgL_arg1872z00_1795);
											}
											BgL_expz00_1789 =
												MAKE_YOUNG_PAIR(BgL_arg1868z00_1792,
												BgL_arg1869z00_1793);
										}
										{	/* Ast/app.scm 309 */

											return
												BGl_sexpzd2ze3nodez31zzast_sexpz00
												(BGl_compilezd2expandzd2zzexpand_epsz00
												(BGl_comptimezd2expandzd2zzexpand_epsz00
													(BgL_expz00_1789)), BNIL, BgL_locz00_18,
												BgL_sitez00_19);
										}
									}
								}
							}
						}
				}
			}
		}

	}



/* make-keys-app-node */
	BgL_nodez00_bglt BGl_makezd2keyszd2appzd2nodezd2zzast_appz00(obj_t
		BgL_stackz00_22, obj_t BgL_locz00_23, obj_t BgL_sitez00_24,
		BgL_varz00_bglt BgL_varz00_25, obj_t BgL_argsz00_26)
	{
		{	/* Ast/app.scm 327 */
			{	/* Ast/app.scm 328 */
				BgL_variablez00_bglt BgL_vz00_1917;

				BgL_vz00_1917 =
					(((BgL_varz00_bglt) COBJECT(BgL_varz00_25))->BgL_variablez00);
				{	/* Ast/app.scm 329 */
					BgL_sfunz00_bglt BgL_i1124z00_1918;

					BgL_i1124z00_1918 =
						((BgL_sfunz00_bglt)
						(((BgL_variablez00_bglt) COBJECT(BgL_vz00_1917))->BgL_valuez00));
					{	/* Ast/app.scm 330 */
						obj_t BgL_collectedz00_1919;

						{	/* Ast/app.scm 330 */
							obj_t BgL_g1125z00_2163;

							BgL_g1125z00_2163 =
								BGl_listzd2tailzd2zz__r4_pairs_and_lists_6_3z00(BgL_argsz00_26,
								(((BgL_funz00_bglt) COBJECT(
											((BgL_funz00_bglt) BgL_i1124z00_1918)))->BgL_arityz00));
							BgL_collectedz00_1919 =
								BGl_loopze70ze7zzast_appz00(BgL_locz00_23, BgL_vz00_1917,
								BgL_g1125z00_2163);
						}
						{
							obj_t BgL_valsz00_2040;
							obj_t BgL_keysz00_2067;
							obj_t BgL_valsz00_2068;
							obj_t BgL_stackz00_2069;

							{	/* Ast/app.scm 418 */
								bool_t BgL_test2488z00_5160;

								{
									obj_t BgL_l1340z00_2017;

									BgL_l1340z00_2017 = BgL_collectedz00_1919;
								BgL_zc3z04anonymousza32007ze3z87_2018:
									if (NULLP(BgL_l1340z00_2017))
										{	/* Ast/app.scm 418 */
											BgL_test2488z00_5160 = ((bool_t) 0);
										}
									else
										{	/* Ast/app.scm 419 */
											bool_t BgL__ortest_1342z00_2020;

											{	/* Ast/app.scm 419 */
												obj_t BgL_az00_2023;

												{	/* Ast/app.scm 419 */
													obj_t BgL_pairz00_3319;

													BgL_pairz00_3319 = CAR(((obj_t) BgL_l1340z00_2017));
													BgL_az00_2023 = CAR(BgL_pairz00_3319);
												}
												{	/* Ast/app.scm 420 */
													bool_t BgL_test2490z00_5166;

													{	/* Ast/app.scm 420 */
														bool_t BgL_test2491z00_5167;

														{	/* Ast/app.scm 420 */
															obj_t BgL_classz00_3320;

															BgL_classz00_3320 = BGl_atomz00zzast_nodez00;
															if (BGL_OBJECTP(BgL_az00_2023))
																{	/* Ast/app.scm 420 */
																	BgL_objectz00_bglt BgL_arg1807z00_3322;

																	BgL_arg1807z00_3322 =
																		(BgL_objectz00_bglt) (BgL_az00_2023);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Ast/app.scm 420 */
																			long BgL_idxz00_3328;

																			BgL_idxz00_3328 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_3322);
																			BgL_test2491z00_5167 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_3328 + 2L)) ==
																				BgL_classz00_3320);
																		}
																	else
																		{	/* Ast/app.scm 420 */
																			bool_t BgL_res2313z00_3353;

																			{	/* Ast/app.scm 420 */
																				obj_t BgL_oclassz00_3336;

																				{	/* Ast/app.scm 420 */
																					obj_t BgL_arg1815z00_3344;
																					long BgL_arg1816z00_3345;

																					BgL_arg1815z00_3344 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Ast/app.scm 420 */
																						long BgL_arg1817z00_3346;

																						BgL_arg1817z00_3346 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_3322);
																						BgL_arg1816z00_3345 =
																							(BgL_arg1817z00_3346 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_3336 =
																						VECTOR_REF(BgL_arg1815z00_3344,
																						BgL_arg1816z00_3345);
																				}
																				{	/* Ast/app.scm 420 */
																					bool_t BgL__ortest_1115z00_3337;

																					BgL__ortest_1115z00_3337 =
																						(BgL_classz00_3320 ==
																						BgL_oclassz00_3336);
																					if (BgL__ortest_1115z00_3337)
																						{	/* Ast/app.scm 420 */
																							BgL_res2313z00_3353 =
																								BgL__ortest_1115z00_3337;
																						}
																					else
																						{	/* Ast/app.scm 420 */
																							long BgL_odepthz00_3338;

																							{	/* Ast/app.scm 420 */
																								obj_t BgL_arg1804z00_3339;

																								BgL_arg1804z00_3339 =
																									(BgL_oclassz00_3336);
																								BgL_odepthz00_3338 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_3339);
																							}
																							if ((2L < BgL_odepthz00_3338))
																								{	/* Ast/app.scm 420 */
																									obj_t BgL_arg1802z00_3341;

																									{	/* Ast/app.scm 420 */
																										obj_t BgL_arg1803z00_3342;

																										BgL_arg1803z00_3342 =
																											(BgL_oclassz00_3336);
																										BgL_arg1802z00_3341 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_3342, 2L);
																									}
																									BgL_res2313z00_3353 =
																										(BgL_arg1802z00_3341 ==
																										BgL_classz00_3320);
																								}
																							else
																								{	/* Ast/app.scm 420 */
																									BgL_res2313z00_3353 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test2491z00_5167 =
																				BgL_res2313z00_3353;
																		}
																}
															else
																{	/* Ast/app.scm 420 */
																	BgL_test2491z00_5167 = ((bool_t) 0);
																}
														}
														if (BgL_test2491z00_5167)
															{	/* Ast/app.scm 420 */
																obj_t BgL_tmpz00_5190;

																BgL_tmpz00_5190 =
																	(((BgL_atomz00_bglt) COBJECT(
																			((BgL_atomz00_bglt) BgL_az00_2023)))->
																	BgL_valuez00);
																BgL_test2490z00_5166 =
																	KEYWORDP(BgL_tmpz00_5190);
															}
														else
															{	/* Ast/app.scm 420 */
																BgL_test2490z00_5166 = ((bool_t) 0);
															}
													}
													if (BgL_test2490z00_5166)
														{	/* Ast/app.scm 420 */
															BgL__ortest_1342z00_2020 = ((bool_t) 0);
														}
													else
														{	/* Ast/app.scm 420 */
															BgL__ortest_1342z00_2020 = ((bool_t) 1);
														}
												}
											}
											if (BgL__ortest_1342z00_2020)
												{	/* Ast/app.scm 418 */
													BgL_test2488z00_5160 = BgL__ortest_1342z00_2020;
												}
											else
												{	/* Ast/app.scm 418 */
													obj_t BgL_arg2008z00_2021;

													BgL_arg2008z00_2021 =
														CDR(((obj_t) BgL_l1340z00_2017));
													{
														obj_t BgL_l1340z00_5197;

														BgL_l1340z00_5197 = BgL_arg2008z00_2021;
														BgL_l1340z00_2017 = BgL_l1340z00_5197;
														goto BgL_zc3z04anonymousza32007ze3z87_2018;
													}
												}
										}
								}
								if (BgL_test2488z00_5160)
									{	/* Ast/app.scm 418 */
										{	/* Ast/app.scm 343 */
											obj_t BgL_fz00_2029;

											BgL_fz00_2029 =
												BGl_gensymz00zz__r4_symbols_6_4z00(
												(((BgL_variablez00_bglt) COBJECT(BgL_vz00_1917))->
													BgL_idz00));
											{	/* Ast/app.scm 348 */
												obj_t BgL_expz00_2030;

												{	/* Ast/app.scm 348 */
													obj_t BgL_arg2011z00_2031;
													obj_t BgL_arg2012z00_2032;

													BgL_arg2011z00_2031 = BGl_letzd2symzd2zzast_letz00();
													{	/* Ast/app.scm 348 */
														obj_t BgL_arg2013z00_2033;
														obj_t BgL_arg2014z00_2034;

														{	/* Ast/app.scm 348 */
															obj_t BgL_arg2015z00_2035;

															{	/* Ast/app.scm 348 */
																obj_t BgL_arg2016z00_2036;

																BgL_arg2016z00_2036 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_vz00_1917), BNIL);
																BgL_arg2015z00_2035 =
																	MAKE_YOUNG_PAIR(BgL_fz00_2029,
																	BgL_arg2016z00_2036);
															}
															BgL_arg2013z00_2033 =
																MAKE_YOUNG_PAIR(BgL_arg2015z00_2035, BNIL);
														}
														{	/* Ast/app.scm 348 */
															obj_t BgL_arg2017z00_2037;

															BgL_arg2017z00_2037 =
																MAKE_YOUNG_PAIR(BgL_fz00_2029,
																BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																(BgL_argsz00_26, BNIL));
															BgL_arg2014z00_2034 =
																MAKE_YOUNG_PAIR(BgL_arg2017z00_2037, BNIL);
														}
														BgL_arg2012z00_2032 =
															MAKE_YOUNG_PAIR(BgL_arg2013z00_2033,
															BgL_arg2014z00_2034);
													}
													BgL_expz00_2030 =
														MAKE_YOUNG_PAIR(BgL_arg2011z00_2031,
														BgL_arg2012z00_2032);
												}
												return
													BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_expz00_2030,
													BgL_stackz00_22, BgL_locz00_23, BgL_sitez00_24);
											}
										}
									}
								else
									{	/* Ast/app.scm 423 */
										obj_t BgL_valsz00_1938;

										{	/* Ast/app.scm 425 */
											obj_t BgL_arg1993z00_1995;

											{	/* Ast/app.scm 425 */
												obj_t BgL_arg1994z00_1996;

												if (NULLP(BgL_collectedz00_1919))
													{	/* Ast/app.scm 425 */
														BgL_arg1994z00_1996 = BNIL;
													}
												else
													{	/* Ast/app.scm 425 */
														obj_t BgL_head1345z00_1999;

														BgL_head1345z00_1999 = MAKE_YOUNG_PAIR(BNIL, BNIL);
														{
															obj_t BgL_l1343z00_2001;
															obj_t BgL_tail1346z00_2002;

															BgL_l1343z00_2001 = BgL_collectedz00_1919;
															BgL_tail1346z00_2002 = BgL_head1345z00_1999;
														BgL_zc3z04anonymousza31996ze3z87_2003:
															if (NULLP(BgL_l1343z00_2001))
																{	/* Ast/app.scm 425 */
																	BgL_arg1994z00_1996 =
																		CDR(BgL_head1345z00_1999);
																}
															else
																{	/* Ast/app.scm 425 */
																	obj_t BgL_newtail1347z00_2005;

																	{	/* Ast/app.scm 425 */
																		obj_t BgL_arg1999z00_2007;

																		{	/* Ast/app.scm 425 */
																			obj_t BgL_vz00_2008;

																			BgL_vz00_2008 =
																				CAR(((obj_t) BgL_l1343z00_2001));
																			{	/* Ast/app.scm 427 */
																				obj_t BgL_arg2000z00_2009;
																				obj_t BgL_arg2001z00_2010;

																				{	/* Ast/app.scm 427 */
																					obj_t BgL_arg2004z00_2013;

																					BgL_arg2004z00_2013 =
																						(((BgL_atomz00_bglt) COBJECT(
																								((BgL_atomz00_bglt)
																									CAR(
																										((obj_t)
																											BgL_vz00_2008)))))->
																						BgL_valuez00);
																					BgL_arg2000z00_2009 =
																						BGl_keywordzd2ze3symbolz31zz__r4_symbols_6_4z00
																						(BgL_arg2004z00_2013);
																				}
																				{	/* Ast/app.scm 428 */
																					obj_t BgL_pairz00_3363;

																					BgL_pairz00_3363 =
																						CDR(((obj_t) BgL_vz00_2008));
																					BgL_arg2001z00_2010 =
																						CAR(BgL_pairz00_3363);
																				}
																				{	/* Ast/app.scm 426 */
																					obj_t BgL_list2002z00_2011;

																					{	/* Ast/app.scm 426 */
																						obj_t BgL_arg2003z00_2012;

																						BgL_arg2003z00_2012 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2001z00_2010, BNIL);
																						BgL_list2002z00_2011 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2000z00_2009,
																							BgL_arg2003z00_2012);
																					}
																					BgL_arg1999z00_2007 =
																						BgL_list2002z00_2011;
																				}
																			}
																		}
																		BgL_newtail1347z00_2005 =
																			MAKE_YOUNG_PAIR(BgL_arg1999z00_2007,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1346z00_2002,
																		BgL_newtail1347z00_2005);
																	{	/* Ast/app.scm 425 */
																		obj_t BgL_arg1998z00_2006;

																		BgL_arg1998z00_2006 =
																			CDR(((obj_t) BgL_l1343z00_2001));
																		{
																			obj_t BgL_tail1346z00_5234;
																			obj_t BgL_l1343z00_5233;

																			BgL_l1343z00_5233 = BgL_arg1998z00_2006;
																			BgL_tail1346z00_5234 =
																				BgL_newtail1347z00_2005;
																			BgL_tail1346z00_2002 =
																				BgL_tail1346z00_5234;
																			BgL_l1343z00_2001 = BgL_l1343z00_5233;
																			goto
																				BgL_zc3z04anonymousza31996ze3z87_2003;
																		}
																	}
																}
														}
													}
												BgL_arg1993z00_1995 =
													BGl_dssslzd2keyzd2argszd2sortzd2zztools_dssslz00
													(BgL_arg1994z00_1996);
											}
											BgL_valsz00_2040 = BgL_arg1993z00_1995;
											{
												obj_t BgL_vsz00_2044;
												obj_t BgL_nvalsz00_2045;

												BgL_vsz00_2044 = BgL_valsz00_2040;
												BgL_nvalsz00_2045 = BNIL;
											BgL_zc3z04anonymousza32021ze3z87_2046:
												if (NULLP(BgL_vsz00_2044))
													{	/* Ast/app.scm 354 */
														BgL_valsz00_1938 =
															bgl_reverse_bang(BgL_nvalsz00_2045);
													}
												else
													{	/* Ast/app.scm 354 */
														if (NULLP(CDR(((obj_t) BgL_vsz00_2044))))
															{	/* Ast/app.scm 357 */
																obj_t BgL_arg2026z00_2050;

																{	/* Ast/app.scm 357 */
																	obj_t BgL_arg2027z00_2051;

																	BgL_arg2027z00_2051 =
																		CAR(((obj_t) BgL_vsz00_2044));
																	BgL_arg2026z00_2050 =
																		MAKE_YOUNG_PAIR(BgL_arg2027z00_2051,
																		BgL_nvalsz00_2045);
																}
																BgL_valsz00_1938 =
																	bgl_reverse_bang(BgL_arg2026z00_2050);
															}
														else
															{	/* Ast/app.scm 358 */
																bool_t BgL_test2501z00_5247;

																{	/* Ast/app.scm 358 */
																	obj_t BgL_auxz00_5252;
																	obj_t BgL_tmpz00_5248;

																	{	/* Ast/app.scm 358 */
																		obj_t BgL_pairz00_3234;

																		{	/* Ast/app.scm 358 */
																			obj_t BgL_pairz00_3233;

																			BgL_pairz00_3233 =
																				CDR(((obj_t) BgL_vsz00_2044));
																			BgL_pairz00_3234 = CAR(BgL_pairz00_3233);
																		}
																		BgL_auxz00_5252 = CAR(BgL_pairz00_3234);
																	}
																	{	/* Ast/app.scm 358 */
																		obj_t BgL_pairz00_3229;

																		BgL_pairz00_3229 =
																			CAR(((obj_t) BgL_vsz00_2044));
																		BgL_tmpz00_5248 = CAR(BgL_pairz00_3229);
																	}
																	BgL_test2501z00_5247 =
																		(BgL_tmpz00_5248 == BgL_auxz00_5252);
																}
																if (BgL_test2501z00_5247)
																	{	/* Ast/app.scm 358 */
																		{	/* Ast/app.scm 360 */
																			obj_t BgL_arg2033z00_2056;
																			obj_t BgL_arg2034z00_2057;

																			BgL_arg2033z00_2056 =
																				(((BgL_variablez00_bglt)
																					COBJECT(BgL_vz00_1917))->BgL_idz00);
																			{	/* Ast/app.scm 361 */
																				obj_t BgL_pairz00_3239;

																				BgL_pairz00_3239 =
																					CAR(((obj_t) BgL_vsz00_2044));
																				BgL_arg2034z00_2057 =
																					CAR(BgL_pairz00_3239);
																			}
																			BGl_userzd2warningzf2locationz20zztools_errorz00
																				(BgL_locz00_23, BgL_arg2033z00_2056,
																				BGl_string2341z00zzast_appz00,
																				BgL_arg2034z00_2057);
																		}
																		{	/* Ast/app.scm 362 */
																			obj_t BgL_arg2036z00_2058;

																			BgL_arg2036z00_2058 =
																				CDR(((obj_t) BgL_vsz00_2044));
																			{
																				obj_t BgL_vsz00_5265;

																				BgL_vsz00_5265 = BgL_arg2036z00_2058;
																				BgL_vsz00_2044 = BgL_vsz00_5265;
																				goto
																					BgL_zc3z04anonymousza32021ze3z87_2046;
																			}
																		}
																	}
																else
																	{	/* Ast/app.scm 364 */
																		obj_t BgL_arg2037z00_2059;
																		obj_t BgL_arg2038z00_2060;

																		BgL_arg2037z00_2059 =
																			CDR(((obj_t) BgL_vsz00_2044));
																		{	/* Ast/app.scm 364 */
																			obj_t BgL_arg2039z00_2061;

																			BgL_arg2039z00_2061 =
																				CAR(((obj_t) BgL_vsz00_2044));
																			BgL_arg2038z00_2060 =
																				MAKE_YOUNG_PAIR(BgL_arg2039z00_2061,
																				BgL_nvalsz00_2045);
																		}
																		{
																			obj_t BgL_nvalsz00_5272;
																			obj_t BgL_vsz00_5271;

																			BgL_vsz00_5271 = BgL_arg2037z00_2059;
																			BgL_nvalsz00_5272 = BgL_arg2038z00_2060;
																			BgL_nvalsz00_2045 = BgL_nvalsz00_5272;
																			BgL_vsz00_2044 = BgL_vsz00_5271;
																			goto
																				BgL_zc3z04anonymousza32021ze3z87_2046;
																		}
																	}
															}
													}
											}
										}
										{	/* Ast/app.scm 431 */
											obj_t BgL_fkeysz00_1939;

											{	/* Ast/app.scm 431 */
												obj_t BgL_l1348z00_1980;

												BgL_l1348z00_1980 =
													(((BgL_sfunz00_bglt) COBJECT(BgL_i1124z00_1918))->
													BgL_keysz00);
												if (NULLP(BgL_l1348z00_1980))
													{	/* Ast/app.scm 431 */
														BgL_fkeysz00_1939 = BNIL;
													}
												else
													{	/* Ast/app.scm 431 */
														obj_t BgL_head1350z00_1982;

														BgL_head1350z00_1982 = MAKE_YOUNG_PAIR(BNIL, BNIL);
														{
															obj_t BgL_l1348z00_1984;
															obj_t BgL_tail1351z00_1985;

															BgL_l1348z00_1984 = BgL_l1348z00_1980;
															BgL_tail1351z00_1985 = BgL_head1350z00_1982;
														BgL_zc3z04anonymousza31987ze3z87_1986:
															if (NULLP(BgL_l1348z00_1984))
																{	/* Ast/app.scm 431 */
																	BgL_fkeysz00_1939 = CDR(BgL_head1350z00_1982);
																}
															else
																{	/* Ast/app.scm 431 */
																	obj_t BgL_newtail1352z00_1988;

																	{	/* Ast/app.scm 431 */
																		obj_t BgL_arg1990z00_1990;

																		{	/* Ast/app.scm 431 */
																			obj_t BgL_kz00_1991;

																			BgL_kz00_1991 =
																				CAR(((obj_t) BgL_l1348z00_1984));
																			{	/* Ast/app.scm 432 */
																				obj_t BgL_arg1991z00_1992;

																				BgL_arg1991z00_1992 =
																					CAR(((obj_t) BgL_kz00_1991));
																				BgL_arg1990z00_1990 =
																					BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																					(BgL_arg1991z00_1992, BgL_locz00_23);
																			}
																		}
																		BgL_newtail1352z00_1988 =
																			MAKE_YOUNG_PAIR(BgL_arg1990z00_1990,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1351z00_1985,
																		BgL_newtail1352z00_1988);
																	{	/* Ast/app.scm 431 */
																		obj_t BgL_arg1989z00_1989;

																		BgL_arg1989z00_1989 =
																			CDR(((obj_t) BgL_l1348z00_1984));
																		{
																			obj_t BgL_tail1351z00_5290;
																			obj_t BgL_l1348z00_5289;

																			BgL_l1348z00_5289 = BgL_arg1989z00_1989;
																			BgL_tail1351z00_5290 =
																				BgL_newtail1352z00_1988;
																			BgL_tail1351z00_1985 =
																				BgL_tail1351z00_5290;
																			BgL_l1348z00_1984 = BgL_l1348z00_5289;
																			goto
																				BgL_zc3z04anonymousza31987ze3z87_1986;
																		}
																	}
																}
														}
													}
											}
											{	/* Ast/app.scm 431 */
												obj_t BgL_errz00_1940;

												{	/* Ast/app.scm 434 */
													obj_t BgL_hook1357z00_1961;

													BgL_hook1357z00_1961 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
													{
														obj_t BgL_l1354z00_1963;
														obj_t BgL_h1355z00_1964;

														BgL_l1354z00_1963 = BgL_valsz00_1938;
														BgL_h1355z00_1964 = BgL_hook1357z00_1961;
													BgL_zc3z04anonymousza31976ze3z87_1965:
														if (NULLP(BgL_l1354z00_1963))
															{	/* Ast/app.scm 434 */
																BgL_errz00_1940 = CDR(BgL_hook1357z00_1961);
															}
														else
															{	/* Ast/app.scm 434 */
																bool_t BgL_test2505z00_5295;

																{	/* Ast/app.scm 435 */
																	bool_t BgL_test2506z00_5296;

																	{	/* Ast/app.scm 435 */
																		obj_t BgL_tmpz00_5297;

																		{	/* Ast/app.scm 435 */
																			obj_t BgL_auxz00_5298;

																			{	/* Ast/app.scm 435 */
																				obj_t BgL_pairz00_3374;

																				BgL_pairz00_3374 =
																					CAR(((obj_t) BgL_l1354z00_1963));
																				BgL_auxz00_5298 = CAR(BgL_pairz00_3374);
																			}
																			BgL_tmpz00_5297 =
																				BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																				(BgL_auxz00_5298, BgL_fkeysz00_1939);
																		}
																		BgL_test2506z00_5296 =
																			CBOOL(BgL_tmpz00_5297);
																	}
																	if (BgL_test2506z00_5296)
																		{	/* Ast/app.scm 435 */
																			BgL_test2505z00_5295 = ((bool_t) 0);
																		}
																	else
																		{	/* Ast/app.scm 435 */
																			BgL_test2505z00_5295 = ((bool_t) 1);
																		}
																}
																if (BgL_test2505z00_5295)
																	{	/* Ast/app.scm 434 */
																		obj_t BgL_nh1356z00_1972;

																		{	/* Ast/app.scm 434 */
																			obj_t BgL_arg1983z00_1974;

																			BgL_arg1983z00_1974 =
																				CAR(((obj_t) BgL_l1354z00_1963));
																			BgL_nh1356z00_1972 =
																				MAKE_YOUNG_PAIR(BgL_arg1983z00_1974,
																				BNIL);
																		}
																		SET_CDR(BgL_h1355z00_1964,
																			BgL_nh1356z00_1972);
																		{	/* Ast/app.scm 434 */
																			obj_t BgL_arg1982z00_1973;

																			BgL_arg1982z00_1973 =
																				CDR(((obj_t) BgL_l1354z00_1963));
																			{
																				obj_t BgL_h1355z00_5311;
																				obj_t BgL_l1354z00_5310;

																				BgL_l1354z00_5310 = BgL_arg1982z00_1973;
																				BgL_h1355z00_5311 = BgL_nh1356z00_1972;
																				BgL_h1355z00_1964 = BgL_h1355z00_5311;
																				BgL_l1354z00_1963 = BgL_l1354z00_5310;
																				goto
																					BgL_zc3z04anonymousza31976ze3z87_1965;
																			}
																		}
																	}
																else
																	{	/* Ast/app.scm 434 */
																		obj_t BgL_arg1984z00_1975;

																		BgL_arg1984z00_1975 =
																			CDR(((obj_t) BgL_l1354z00_1963));
																		{
																			obj_t BgL_l1354z00_5314;

																			BgL_l1354z00_5314 = BgL_arg1984z00_1975;
																			BgL_l1354z00_1963 = BgL_l1354z00_5314;
																			goto
																				BgL_zc3z04anonymousza31976ze3z87_1965;
																		}
																	}
															}
													}
												}
												{	/* Ast/app.scm 434 */

													if (PAIRP(BgL_errz00_1940))
														{	/* Ast/app.scm 439 */
															obj_t BgL_arg1965z00_1942;
															obj_t BgL_arg1966z00_1943;

															BgL_arg1965z00_1942 =
																(((BgL_variablez00_bglt)
																	COBJECT(BgL_vz00_1917))->BgL_idz00);
															{	/* Ast/app.scm 440 */
																obj_t BgL_head1360z00_1947;

																BgL_head1360z00_1947 =
																	MAKE_YOUNG_PAIR(CAR(CAR(BgL_errz00_1940)),
																	BNIL);
																{	/* Ast/app.scm 440 */
																	obj_t BgL_g1363z00_1948;

																	BgL_g1363z00_1948 = CDR(BgL_errz00_1940);
																	{
																		obj_t BgL_l1358z00_1950;
																		obj_t BgL_tail1361z00_1951;

																		BgL_l1358z00_1950 = BgL_g1363z00_1948;
																		BgL_tail1361z00_1951 = BgL_head1360z00_1947;
																	BgL_zc3z04anonymousza31969ze3z87_1952:
																		if (NULLP(BgL_l1358z00_1950))
																			{	/* Ast/app.scm 440 */
																				BgL_arg1966z00_1943 =
																					BgL_head1360z00_1947;
																			}
																		else
																			{	/* Ast/app.scm 440 */
																				obj_t BgL_newtail1362z00_1954;

																				{	/* Ast/app.scm 440 */
																					obj_t BgL_arg1972z00_1956;

																					{	/* Ast/app.scm 440 */
																						obj_t BgL_pairz00_3384;

																						BgL_pairz00_3384 =
																							CAR(((obj_t) BgL_l1358z00_1950));
																						BgL_arg1972z00_1956 =
																							CAR(BgL_pairz00_3384);
																					}
																					BgL_newtail1362z00_1954 =
																						MAKE_YOUNG_PAIR(BgL_arg1972z00_1956,
																						BNIL);
																				}
																				SET_CDR(BgL_tail1361z00_1951,
																					BgL_newtail1362z00_1954);
																				{	/* Ast/app.scm 440 */
																					obj_t BgL_arg1971z00_1955;

																					BgL_arg1971z00_1955 =
																						CDR(((obj_t) BgL_l1358z00_1950));
																					{
																						obj_t BgL_tail1361z00_5332;
																						obj_t BgL_l1358z00_5331;

																						BgL_l1358z00_5331 =
																							BgL_arg1971z00_1955;
																						BgL_tail1361z00_5332 =
																							BgL_newtail1362z00_1954;
																						BgL_tail1361z00_1951 =
																							BgL_tail1361z00_5332;
																						BgL_l1358z00_1950 =
																							BgL_l1358z00_5331;
																						goto
																							BgL_zc3z04anonymousza31969ze3z87_1952;
																					}
																				}
																			}
																	}
																}
															}
															BGl_userzd2errorzf2locationz20zztools_errorz00
																(BgL_locz00_23, BgL_arg1965z00_1942,
																BGl_string2342z00zzast_appz00,
																BgL_arg1966z00_1943, BNIL);
														}
													else
														{	/* Ast/app.scm 437 */
															BFALSE;
														}
												}
											}
										}
										{	/* Ast/app.scm 441 */
											obj_t BgL_arg1992z00_1994;

											BgL_arg1992z00_1994 =
												(((BgL_sfunz00_bglt) COBJECT(BgL_i1124z00_1918))->
												BgL_keysz00);
											{
												BgL_nodezf2effectzf2_bglt BgL_auxz00_5335;

												BgL_keysz00_2067 = BgL_arg1992z00_1994;
												BgL_valsz00_2068 = BgL_valsz00_1938;
												BgL_stackz00_2069 = BgL_stackz00_22;
												BgL_auxz00_5335 =
													BGl_loopze71ze7zzast_appz00(BgL_argsz00_26,
													BgL_i1124z00_1918, BgL_varz00_25, BgL_vz00_1917,
													BgL_sitez00_24, BgL_locz00_23, BgL_keysz00_2067,
													BgL_valsz00_2068, BNIL, BgL_stackz00_2069);
												return ((BgL_nodez00_bglt) BgL_auxz00_5335);
											}
										}
									}
							}
						}
					}
				}
			}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zzast_appz00(obj_t BgL_locz00_4164,
		BgL_variablez00_bglt BgL_vz00_4163, obj_t BgL_lz00_2165)
	{
		{	/* Ast/app.scm 330 */
			if (NULLP(BgL_lz00_2165))
				{	/* Ast/app.scm 332 */
					return BNIL;
				}
			else
				{	/* Ast/app.scm 334 */
					bool_t BgL_test2510z00_5340;

					{	/* Ast/app.scm 334 */
						obj_t BgL_tmpz00_5341;

						BgL_tmpz00_5341 = CDR(((obj_t) BgL_lz00_2165));
						BgL_test2510z00_5340 = PAIRP(BgL_tmpz00_5341);
					}
					if (BgL_test2510z00_5340)
						{	/* Ast/app.scm 335 */
							obj_t BgL_arg2102z00_2170;
							obj_t BgL_arg2103z00_2171;

							{	/* Ast/app.scm 335 */
								obj_t BgL_arg2104z00_2172;
								obj_t BgL_arg2105z00_2173;

								BgL_arg2104z00_2172 = CAR(((obj_t) BgL_lz00_2165));
								{	/* Ast/app.scm 335 */
									obj_t BgL_pairz00_3215;

									BgL_pairz00_3215 = CDR(((obj_t) BgL_lz00_2165));
									BgL_arg2105z00_2173 = CAR(BgL_pairz00_3215);
								}
								{	/* Ast/app.scm 335 */
									obj_t BgL_list2106z00_2174;

									{	/* Ast/app.scm 335 */
										obj_t BgL_arg2107z00_2175;

										BgL_arg2107z00_2175 =
											MAKE_YOUNG_PAIR(BgL_arg2105z00_2173, BNIL);
										BgL_list2106z00_2174 =
											MAKE_YOUNG_PAIR(BgL_arg2104z00_2172, BgL_arg2107z00_2175);
									}
									BgL_arg2102z00_2170 = BgL_list2106z00_2174;
								}
							}
							{	/* Ast/app.scm 336 */
								obj_t BgL_arg2108z00_2176;

								{	/* Ast/app.scm 336 */
									obj_t BgL_pairz00_3220;

									BgL_pairz00_3220 = CDR(((obj_t) BgL_lz00_2165));
									BgL_arg2108z00_2176 = CDR(BgL_pairz00_3220);
								}
								BgL_arg2103z00_2171 =
									BGl_loopze70ze7zzast_appz00(BgL_locz00_4164, BgL_vz00_4163,
									BgL_arg2108z00_2176);
							}
							return MAKE_YOUNG_PAIR(BgL_arg2102z00_2170, BgL_arg2103z00_2171);
						}
					else
						{	/* Ast/app.scm 339 */
							obj_t BgL_arg2109z00_2177;
							obj_t BgL_arg2110z00_2178;

							BgL_arg2109z00_2177 =
								(((BgL_variablez00_bglt) COBJECT(BgL_vz00_4163))->BgL_idz00);
							{	/* Ast/app.scm 341 */
								obj_t BgL_arg2112z00_2180;

								BgL_arg2112z00_2180 = CAR(((obj_t) BgL_lz00_2165));
								BgL_arg2110z00_2178 =
									BGl_shapez00zztools_shapez00(BgL_arg2112z00_2180);
							}
							return
								BGl_userzd2errorzf2locationz20zztools_errorz00(BgL_locz00_4164,
								BgL_arg2109z00_2177, BGl_string2343z00zzast_appz00,
								BgL_arg2110z00_2178, BNIL);
						}
				}
		}

	}



/* loop~1 */
	BgL_nodezf2effectzf2_bglt BGl_loopze71ze7zzast_appz00(obj_t BgL_argsz00_4170,
		BgL_sfunz00_bglt BgL_i1124z00_4169, BgL_varz00_bglt BgL_varz00_4168,
		BgL_variablez00_bglt BgL_vz00_4167, obj_t BgL_sitez00_4166,
		obj_t BgL_locz00_4165, obj_t BgL_keysz00_2073, obj_t BgL_valsz00_2074,
		obj_t BgL_envz00_2075, obj_t BgL_stackz00_2076)
	{
		{	/* Ast/app.scm 367 */
			if (NULLP(BgL_keysz00_2073))
				{	/* Ast/app.scm 373 */
					BgL_appz00_bglt BgL_new1133z00_2079;

					{	/* Ast/app.scm 374 */
						BgL_appz00_bglt BgL_new1131z00_2097;

						BgL_new1131z00_2097 =
							((BgL_appz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_appz00_bgl))));
						{	/* Ast/app.scm 374 */
							long BgL_arg2057z00_2098;

							BgL_arg2057z00_2098 = BGL_CLASS_NUM(BGl_appz00zzast_nodez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1131z00_2097),
								BgL_arg2057z00_2098);
						}
						{	/* Ast/app.scm 374 */
							BgL_objectz00_bglt BgL_tmpz00_5368;

							BgL_tmpz00_5368 = ((BgL_objectz00_bglt) BgL_new1131z00_2097);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5368, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_new1131z00_2097);
						BgL_new1133z00_2079 = BgL_new1131z00_2097;
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_new1133z00_2079)))->BgL_locz00) =
						((obj_t) BgL_locz00_4165), BUNSPEC);
					{
						BgL_typez00_bglt BgL_auxz00_5374;

						{	/* Ast/app.scm 375 */
							BgL_typez00_bglt BgL_arg2048z00_2080;

							BgL_arg2048z00_2080 =
								(((BgL_variablez00_bglt) COBJECT(BgL_vz00_4167))->BgL_typez00);
							BgL_auxz00_5374 =
								BGl_strictzd2nodezd2typez00zzast_nodez00(
								((BgL_typez00_bglt) BGl_za2_za2z00zztype_cachez00),
								BgL_arg2048z00_2080);
						}
						((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt) BgL_new1133z00_2079)))->BgL_typez00) =
							((BgL_typez00_bglt) BgL_auxz00_5374), BUNSPEC);
					}
					((((BgL_nodezf2effectzf2_bglt) COBJECT(
									((BgL_nodezf2effectzf2_bglt) BgL_new1133z00_2079)))->
							BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1133z00_2079)))->BgL_keyz00) =
						((obj_t) BINT(-1L)), BUNSPEC);
					((((BgL_appz00_bglt) COBJECT(BgL_new1133z00_2079))->BgL_funz00) =
						((BgL_varz00_bglt) BgL_varz00_4168), BUNSPEC);
					{
						obj_t BgL_auxz00_5386;

						{	/* Ast/app.scm 378 */
							obj_t BgL_arg2049z00_2081;
							obj_t BgL_arg2050z00_2082;

							BgL_arg2049z00_2081 =
								BGl_takez00zz__r4_pairs_and_lists_6_3z00(BgL_argsz00_4170,
								(((BgL_funz00_bglt) COBJECT(
											((BgL_funz00_bglt) BgL_i1124z00_4169)))->BgL_arityz00));
							{	/* Ast/app.scm 379 */
								obj_t BgL_l1335z00_2084;

								BgL_l1335z00_2084 = bgl_reverse_bang(BgL_envz00_2075);
								if (NULLP(BgL_l1335z00_2084))
									{	/* Ast/app.scm 379 */
										BgL_arg2050z00_2082 = BNIL;
									}
								else
									{	/* Ast/app.scm 379 */
										obj_t BgL_head1337z00_2086;

										BgL_head1337z00_2086 = MAKE_YOUNG_PAIR(BNIL, BNIL);
										{
											obj_t BgL_l1335z00_2088;
											obj_t BgL_tail1338z00_2089;

											BgL_l1335z00_2088 = BgL_l1335z00_2084;
											BgL_tail1338z00_2089 = BgL_head1337z00_2086;
										BgL_zc3z04anonymousza32053ze3z87_2090:
											if (NULLP(BgL_l1335z00_2088))
												{	/* Ast/app.scm 379 */
													BgL_arg2050z00_2082 = CDR(BgL_head1337z00_2086);
												}
											else
												{	/* Ast/app.scm 379 */
													obj_t BgL_newtail1339z00_2092;

													{	/* Ast/app.scm 379 */
														BgL_nodez00_bglt BgL_arg2056z00_2094;

														{	/* Ast/app.scm 379 */
															obj_t BgL_vz00_2095;

															BgL_vz00_2095 = CAR(((obj_t) BgL_l1335z00_2088));
															BgL_arg2056z00_2094 =
																BGl_sexpzd2ze3nodez31zzast_sexpz00
																(BgL_vz00_2095, BgL_stackz00_2076,
																BgL_locz00_4165, BgL_sitez00_4166);
														}
														BgL_newtail1339z00_2092 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_arg2056z00_2094), BNIL);
													}
													SET_CDR(BgL_tail1338z00_2089,
														BgL_newtail1339z00_2092);
													{	/* Ast/app.scm 379 */
														obj_t BgL_arg2055z00_2093;

														BgL_arg2055z00_2093 =
															CDR(((obj_t) BgL_l1335z00_2088));
														{
															obj_t BgL_tail1338z00_5406;
															obj_t BgL_l1335z00_5405;

															BgL_l1335z00_5405 = BgL_arg2055z00_2093;
															BgL_tail1338z00_5406 = BgL_newtail1339z00_2092;
															BgL_tail1338z00_2089 = BgL_tail1338z00_5406;
															BgL_l1335z00_2088 = BgL_l1335z00_5405;
															goto BgL_zc3z04anonymousza32053ze3z87_2090;
														}
													}
												}
										}
									}
							}
							BgL_auxz00_5386 =
								BGl_appendzd221011zd2zzast_appz00(BgL_arg2049z00_2081,
								BgL_arg2050z00_2082);
						}
						((((BgL_appz00_bglt) COBJECT(BgL_new1133z00_2079))->BgL_argsz00) =
							((obj_t) BgL_auxz00_5386), BUNSPEC);
					}
					((((BgL_appz00_bglt) COBJECT(BgL_new1133z00_2079))->
							BgL_stackablez00) = ((obj_t) BFALSE), BUNSPEC);
					return ((BgL_nodezf2effectzf2_bglt) BgL_new1133z00_2079);
				}
			else
				{	/* Ast/app.scm 372 */
					if (NULLP(BgL_valsz00_2074))
						{	/* Ast/app.scm 383 */
							obj_t BgL_itz00_2100;

							{	/* Ast/app.scm 383 */
								obj_t BgL_arg2068z00_2116;

								{	/* Ast/app.scm 383 */
									obj_t BgL_pairz00_3255;

									BgL_pairz00_3255 = CAR(((obj_t) BgL_keysz00_2073));
									BgL_arg2068z00_2116 = CAR(BgL_pairz00_3255);
								}
								BgL_itz00_2100 =
									BGl_parsezd2idzd2zzast_identz00(BgL_arg2068z00_2116,
									BgL_locz00_4165);
							}
							{	/* Ast/app.scm 383 */
								BgL_localz00_bglt BgL_varz00_2101;

								{	/* Ast/app.scm 384 */
									obj_t BgL_arg2065z00_2114;
									obj_t BgL_arg2067z00_2115;

									BgL_arg2065z00_2114 = CAR(BgL_itz00_2100);
									BgL_arg2067z00_2115 = CDR(BgL_itz00_2100);
									BgL_varz00_2101 =
										BGl_makezd2userzd2localzd2svarzd2zzast_localz00
										(BgL_arg2065z00_2114,
										((BgL_typez00_bglt) BgL_arg2067z00_2115));
								}
								{	/* Ast/app.scm 384 */
									BgL_nodez00_bglt BgL_valz00_2102;

									{	/* Ast/app.scm 385 */
										obj_t BgL_arg2064z00_2113;

										{	/* Ast/app.scm 385 */
											obj_t BgL_pairz00_3263;

											{	/* Ast/app.scm 385 */
												obj_t BgL_pairz00_3262;

												BgL_pairz00_3262 = CAR(((obj_t) BgL_keysz00_2073));
												BgL_pairz00_3263 = CDR(BgL_pairz00_3262);
											}
											BgL_arg2064z00_2113 = CAR(BgL_pairz00_3263);
										}
										BgL_valz00_2102 =
											BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_arg2064z00_2113,
											BgL_stackz00_2076, BgL_locz00_4165, BgL_sitez00_4166);
									}
									{	/* Ast/app.scm 385 */
										obj_t BgL_nenvz00_2103;

										BgL_nenvz00_2103 =
											MAKE_YOUNG_PAIR(
											((obj_t) BgL_varz00_2101), BgL_envz00_2075);
										{	/* Ast/app.scm 386 */
											obj_t BgL_nstackz00_2104;

											BgL_nstackz00_2104 =
												MAKE_YOUNG_PAIR(
												((obj_t) BgL_varz00_2101), BgL_stackz00_2076);
											{	/* Ast/app.scm 387 */
												BgL_nodezf2effectzf2_bglt BgL_bodyz00_2105;

												{	/* Ast/app.scm 388 */
													obj_t BgL_arg2063z00_2112;

													BgL_arg2063z00_2112 = CDR(((obj_t) BgL_keysz00_2073));
													BgL_bodyz00_2105 =
														BGl_loopze71ze7zzast_appz00(BgL_argsz00_4170,
														BgL_i1124z00_4169, BgL_varz00_4168, BgL_vz00_4167,
														BgL_sitez00_4166, BgL_locz00_4165,
														BgL_arg2063z00_2112, BNIL, BgL_nenvz00_2103,
														BgL_nstackz00_2104);
												}
												{	/* Ast/app.scm 388 */

													{	/* Ast/app.scm 389 */
														BgL_letzd2varzd2_bglt BgL_new1135z00_2106;

														{	/* Ast/app.scm 390 */
															BgL_letzd2varzd2_bglt BgL_new1134z00_2110;

															BgL_new1134z00_2110 =
																((BgL_letzd2varzd2_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_letzd2varzd2_bgl))));
															{	/* Ast/app.scm 390 */
																long BgL_arg2062z00_2111;

																BgL_arg2062z00_2111 =
																	BGL_CLASS_NUM(BGl_letzd2varzd2zzast_nodez00);
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1134z00_2110),
																	BgL_arg2062z00_2111);
															}
															{	/* Ast/app.scm 390 */
																BgL_objectz00_bglt BgL_tmpz00_5437;

																BgL_tmpz00_5437 =
																	((BgL_objectz00_bglt) BgL_new1134z00_2110);
																BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5437,
																	BFALSE);
															}
															((BgL_objectz00_bglt) BgL_new1134z00_2110);
															BgL_new1135z00_2106 = BgL_new1134z00_2110;
														}
														((((BgL_nodez00_bglt) COBJECT(
																		((BgL_nodez00_bglt) BgL_new1135z00_2106)))->
																BgL_locz00) =
															((obj_t) BgL_locz00_4165), BUNSPEC);
														{
															BgL_typez00_bglt BgL_auxz00_5443;

															{	/* Ast/app.scm 391 */
																BgL_typez00_bglt BgL_arg2059z00_2107;

																BgL_arg2059z00_2107 =
																	(((BgL_variablez00_bglt)
																		COBJECT(BgL_vz00_4167))->BgL_typez00);
																BgL_auxz00_5443 =
																	BGl_strictzd2nodezd2typez00zzast_nodez00((
																		(BgL_typez00_bglt)
																		BGl_za2_za2z00zztype_cachez00),
																	BgL_arg2059z00_2107);
															}
															((((BgL_nodez00_bglt) COBJECT(
																			((BgL_nodez00_bglt)
																				BgL_new1135z00_2106)))->BgL_typez00) =
																((BgL_typez00_bglt) BgL_auxz00_5443), BUNSPEC);
														}
														((((BgL_nodezf2effectzf2_bglt) COBJECT(
																		((BgL_nodezf2effectzf2_bglt)
																			BgL_new1135z00_2106)))->
																BgL_sidezd2effectzd2) =
															((obj_t) BUNSPEC), BUNSPEC);
														((((BgL_nodezf2effectzf2_bglt)
																	COBJECT(((BgL_nodezf2effectzf2_bglt)
																			BgL_new1135z00_2106)))->BgL_keyz00) =
															((obj_t) BINT(-1L)), BUNSPEC);
														{
															obj_t BgL_auxz00_5454;

															{	/* Ast/app.scm 392 */
																obj_t BgL_arg2060z00_2108;

																BgL_arg2060z00_2108 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_varz00_2101),
																	((obj_t) BgL_valz00_2102));
																{	/* Ast/app.scm 392 */
																	obj_t BgL_list2061z00_2109;

																	BgL_list2061z00_2109 =
																		MAKE_YOUNG_PAIR(BgL_arg2060z00_2108, BNIL);
																	BgL_auxz00_5454 = BgL_list2061z00_2109;
															}}
															((((BgL_letzd2varzd2_bglt)
																		COBJECT(BgL_new1135z00_2106))->
																	BgL_bindingsz00) =
																((obj_t) BgL_auxz00_5454), BUNSPEC);
														}
														((((BgL_letzd2varzd2_bglt)
																	COBJECT(BgL_new1135z00_2106))->BgL_bodyz00) =
															((BgL_nodez00_bglt) ((BgL_nodez00_bglt)
																	BgL_bodyz00_2105)), BUNSPEC);
														((((BgL_letzd2varzd2_bglt)
																	COBJECT(BgL_new1135z00_2106))->
																BgL_removablezf3zf3) =
															((bool_t) ((bool_t) 1)), BUNSPEC);
														return ((BgL_nodezf2effectzf2_bglt)
															BgL_new1135z00_2106);
													}
												}
											}
										}
									}
								}
							}
						}
					else
						{	/* Ast/app.scm 394 */
							bool_t BgL_test2515z00_5464;

							{	/* Ast/app.scm 394 */
								obj_t BgL_arg2095z00_2156;
								obj_t BgL_arg2096z00_2157;

								{	/* Ast/app.scm 394 */
									obj_t BgL_arg2097z00_2158;

									{	/* Ast/app.scm 394 */
										obj_t BgL_pairz00_3274;

										BgL_pairz00_3274 = CAR(((obj_t) BgL_keysz00_2073));
										BgL_arg2097z00_2158 = CAR(BgL_pairz00_3274);
									}
									BgL_arg2095z00_2156 =
										BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
										(BgL_arg2097z00_2158, BgL_locz00_4165);
								}
								{	/* Ast/app.scm 394 */
									obj_t BgL_pairz00_3278;

									BgL_pairz00_3278 = CAR(((obj_t) BgL_valsz00_2074));
									BgL_arg2096z00_2157 = CAR(BgL_pairz00_3278);
								}
								BgL_test2515z00_5464 =
									(BgL_arg2095z00_2156 == BgL_arg2096z00_2157);
							}
							if (BgL_test2515z00_5464)
								{	/* Ast/app.scm 395 */
									obj_t BgL_itz00_2121;

									{	/* Ast/app.scm 395 */
										obj_t BgL_arg2083z00_2138;

										{	/* Ast/app.scm 395 */
											obj_t BgL_pairz00_3282;

											BgL_pairz00_3282 = CAR(((obj_t) BgL_valsz00_2074));
											BgL_arg2083z00_2138 = CAR(BgL_pairz00_3282);
										}
										BgL_itz00_2121 =
											BGl_parsezd2idzd2zzast_identz00(BgL_arg2083z00_2138,
											BgL_locz00_4165);
									}
									{	/* Ast/app.scm 395 */
										BgL_localz00_bglt BgL_varz00_2122;

										{	/* Ast/app.scm 396 */
											obj_t BgL_arg2081z00_2136;
											obj_t BgL_arg2082z00_2137;

											BgL_arg2081z00_2136 = CAR(BgL_itz00_2121);
											BgL_arg2082z00_2137 = CDR(BgL_itz00_2121);
											BgL_varz00_2122 =
												BGl_makezd2userzd2localzd2svarzd2zzast_localz00
												(BgL_arg2081z00_2136,
												((BgL_typez00_bglt) BgL_arg2082z00_2137));
										}
										{	/* Ast/app.scm 396 */
											BgL_nodez00_bglt BgL_valz00_2123;

											{	/* Ast/app.scm 397 */
												obj_t BgL_arg2080z00_2135;

												{	/* Ast/app.scm 397 */
													obj_t BgL_pairz00_3290;

													{	/* Ast/app.scm 397 */
														obj_t BgL_pairz00_3289;

														BgL_pairz00_3289 = CAR(((obj_t) BgL_valsz00_2074));
														BgL_pairz00_3290 = CDR(BgL_pairz00_3289);
													}
													BgL_arg2080z00_2135 = CAR(BgL_pairz00_3290);
												}
												BgL_valz00_2123 =
													BGl_sexpzd2ze3nodez31zzast_sexpz00
													(BgL_arg2080z00_2135, BgL_stackz00_2076,
													BgL_locz00_4165, BgL_sitez00_4166);
											}
											{	/* Ast/app.scm 397 */
												obj_t BgL_nenvz00_2124;

												BgL_nenvz00_2124 =
													MAKE_YOUNG_PAIR(
													((obj_t) BgL_varz00_2122), BgL_envz00_2075);
												{	/* Ast/app.scm 398 */
													obj_t BgL_nstackz00_2125;

													BgL_nstackz00_2125 =
														MAKE_YOUNG_PAIR(
														((obj_t) BgL_varz00_2122), BgL_stackz00_2076);
													{	/* Ast/app.scm 399 */
														BgL_nodezf2effectzf2_bglt BgL_bodyz00_2126;

														{	/* Ast/app.scm 400 */
															obj_t BgL_arg2078z00_2133;
															obj_t BgL_arg2079z00_2134;

															BgL_arg2078z00_2133 =
																CDR(((obj_t) BgL_keysz00_2073));
															BgL_arg2079z00_2134 =
																CDR(((obj_t) BgL_valsz00_2074));
															BgL_bodyz00_2126 =
																BGl_loopze71ze7zzast_appz00(BgL_argsz00_4170,
																BgL_i1124z00_4169, BgL_varz00_4168,
																BgL_vz00_4167, BgL_sitez00_4166,
																BgL_locz00_4165, BgL_arg2078z00_2133,
																BgL_arg2079z00_2134, BgL_nenvz00_2124,
																BgL_nstackz00_2125);
														}
														{	/* Ast/app.scm 400 */

															{	/* Ast/app.scm 401 */
																BgL_letzd2varzd2_bglt BgL_new1137z00_2127;

																{	/* Ast/app.scm 402 */
																	BgL_letzd2varzd2_bglt BgL_new1136z00_2131;

																	BgL_new1136z00_2131 =
																		((BgL_letzd2varzd2_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_letzd2varzd2_bgl))));
																	{	/* Ast/app.scm 402 */
																		long BgL_arg2077z00_2132;

																		BgL_arg2077z00_2132 =
																			BGL_CLASS_NUM
																			(BGl_letzd2varzd2zzast_nodez00);
																		BGL_OBJECT_CLASS_NUM_SET((
																				(BgL_objectz00_bglt)
																				BgL_new1136z00_2131),
																			BgL_arg2077z00_2132);
																	}
																	{	/* Ast/app.scm 402 */
																		BgL_objectz00_bglt BgL_tmpz00_5499;

																		BgL_tmpz00_5499 =
																			((BgL_objectz00_bglt)
																			BgL_new1136z00_2131);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5499,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1136z00_2131);
																	BgL_new1137z00_2127 = BgL_new1136z00_2131;
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1137z00_2127)))->BgL_locz00) =
																	((obj_t) BgL_locz00_4165), BUNSPEC);
																{
																	BgL_typez00_bglt BgL_auxz00_5505;

																	{	/* Ast/app.scm 403 */
																		BgL_typez00_bglt BgL_arg2074z00_2128;

																		BgL_arg2074z00_2128 =
																			(((BgL_variablez00_bglt)
																				COBJECT(BgL_vz00_4167))->BgL_typez00);
																		BgL_auxz00_5505 =
																			BGl_strictzd2nodezd2typez00zzast_nodez00((
																				(BgL_typez00_bglt)
																				BGl_za2_za2z00zztype_cachez00),
																			BgL_arg2074z00_2128);
																	}
																	((((BgL_nodez00_bglt) COBJECT(
																					((BgL_nodez00_bglt)
																						BgL_new1137z00_2127)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) BgL_auxz00_5505),
																		BUNSPEC);
																}
																((((BgL_nodezf2effectzf2_bglt) COBJECT(
																				((BgL_nodezf2effectzf2_bglt)
																					BgL_new1137z00_2127)))->
																		BgL_sidezd2effectzd2) =
																	((obj_t) BUNSPEC), BUNSPEC);
																((((BgL_nodezf2effectzf2_bglt)
																			COBJECT(((BgL_nodezf2effectzf2_bglt)
																					BgL_new1137z00_2127)))->BgL_keyz00) =
																	((obj_t) BINT(-1L)), BUNSPEC);
																{
																	obj_t BgL_auxz00_5516;

																	{	/* Ast/app.scm 404 */
																		obj_t BgL_arg2075z00_2129;

																		BgL_arg2075z00_2129 =
																			MAKE_YOUNG_PAIR(
																			((obj_t) BgL_varz00_2122),
																			((obj_t) BgL_valz00_2123));
																		{	/* Ast/app.scm 404 */
																			obj_t BgL_list2076z00_2130;

																			BgL_list2076z00_2130 =
																				MAKE_YOUNG_PAIR(BgL_arg2075z00_2129,
																				BNIL);
																			BgL_auxz00_5516 = BgL_list2076z00_2130;
																	}}
																	((((BgL_letzd2varzd2_bglt)
																				COBJECT(BgL_new1137z00_2127))->
																			BgL_bindingsz00) =
																		((obj_t) BgL_auxz00_5516), BUNSPEC);
																}
																((((BgL_letzd2varzd2_bglt)
																			COBJECT(BgL_new1137z00_2127))->
																		BgL_bodyz00) =
																	((BgL_nodez00_bglt) ((BgL_nodez00_bglt)
																			BgL_bodyz00_2126)), BUNSPEC);
																((((BgL_letzd2varzd2_bglt)
																			COBJECT(BgL_new1137z00_2127))->
																		BgL_removablezf3zf3) =
																	((bool_t) ((bool_t) 1)), BUNSPEC);
																return ((BgL_nodezf2effectzf2_bglt)
																	BgL_new1137z00_2127);
															}
														}
													}
												}
											}
										}
									}
								}
							else
								{	/* Ast/app.scm 407 */
									obj_t BgL_itz00_2139;

									{	/* Ast/app.scm 407 */
										obj_t BgL_arg2094z00_2155;

										{	/* Ast/app.scm 407 */
											obj_t BgL_pairz00_3302;

											BgL_pairz00_3302 = CAR(((obj_t) BgL_keysz00_2073));
											BgL_arg2094z00_2155 = CAR(BgL_pairz00_3302);
										}
										BgL_itz00_2139 =
											BGl_parsezd2idzd2zzast_identz00(BgL_arg2094z00_2155,
											BgL_locz00_4165);
									}
									{	/* Ast/app.scm 407 */
										BgL_localz00_bglt BgL_varz00_2140;

										{	/* Ast/app.scm 408 */
											obj_t BgL_arg2091z00_2153;
											obj_t BgL_arg2093z00_2154;

											BgL_arg2091z00_2153 = CAR(BgL_itz00_2139);
											BgL_arg2093z00_2154 = CDR(BgL_itz00_2139);
											BgL_varz00_2140 =
												BGl_makezd2userzd2localzd2svarzd2zzast_localz00
												(BgL_arg2091z00_2153,
												((BgL_typez00_bglt) BgL_arg2093z00_2154));
										}
										{	/* Ast/app.scm 408 */
											BgL_nodez00_bglt BgL_valz00_2141;

											{	/* Ast/app.scm 409 */
												obj_t BgL_arg2090z00_2152;

												{	/* Ast/app.scm 409 */
													obj_t BgL_pairz00_3310;

													{	/* Ast/app.scm 409 */
														obj_t BgL_pairz00_3309;

														BgL_pairz00_3309 = CAR(((obj_t) BgL_keysz00_2073));
														BgL_pairz00_3310 = CDR(BgL_pairz00_3309);
													}
													BgL_arg2090z00_2152 = CAR(BgL_pairz00_3310);
												}
												BgL_valz00_2141 =
													BGl_sexpzd2ze3nodez31zzast_sexpz00
													(BgL_arg2090z00_2152, BgL_stackz00_2076,
													BgL_locz00_4165, BgL_sitez00_4166);
											}
											{	/* Ast/app.scm 409 */
												obj_t BgL_nenvz00_2142;

												BgL_nenvz00_2142 =
													MAKE_YOUNG_PAIR(
													((obj_t) BgL_varz00_2140), BgL_envz00_2075);
												{	/* Ast/app.scm 410 */
													obj_t BgL_nstackz00_2143;

													BgL_nstackz00_2143 =
														MAKE_YOUNG_PAIR(
														((obj_t) BgL_varz00_2140), BgL_stackz00_2076);
													{	/* Ast/app.scm 411 */
														BgL_nodezf2effectzf2_bglt BgL_bodyz00_2144;

														{	/* Ast/app.scm 412 */
															obj_t BgL_arg2089z00_2151;

															BgL_arg2089z00_2151 =
																CDR(((obj_t) BgL_keysz00_2073));
															BgL_bodyz00_2144 =
																BGl_loopze71ze7zzast_appz00(BgL_argsz00_4170,
																BgL_i1124z00_4169, BgL_varz00_4168,
																BgL_vz00_4167, BgL_sitez00_4166,
																BgL_locz00_4165, BgL_arg2089z00_2151,
																BgL_valsz00_2074, BgL_nenvz00_2142,
																BgL_nstackz00_2143);
														}
														{	/* Ast/app.scm 412 */

															{	/* Ast/app.scm 413 */
																BgL_letzd2varzd2_bglt BgL_new1139z00_2145;

																{	/* Ast/app.scm 414 */
																	BgL_letzd2varzd2_bglt BgL_new1138z00_2149;

																	BgL_new1138z00_2149 =
																		((BgL_letzd2varzd2_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_letzd2varzd2_bgl))));
																	{	/* Ast/app.scm 414 */
																		long BgL_arg2088z00_2150;

																		BgL_arg2088z00_2150 =
																			BGL_CLASS_NUM
																			(BGl_letzd2varzd2zzast_nodez00);
																		BGL_OBJECT_CLASS_NUM_SET((
																				(BgL_objectz00_bglt)
																				BgL_new1138z00_2149),
																			BgL_arg2088z00_2150);
																	}
																	{	/* Ast/app.scm 414 */
																		BgL_objectz00_bglt BgL_tmpz00_5550;

																		BgL_tmpz00_5550 =
																			((BgL_objectz00_bglt)
																			BgL_new1138z00_2149);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5550,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1138z00_2149);
																	BgL_new1139z00_2145 = BgL_new1138z00_2149;
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1139z00_2145)))->BgL_locz00) =
																	((obj_t) BgL_locz00_4165), BUNSPEC);
																{
																	BgL_typez00_bglt BgL_auxz00_5556;

																	{	/* Ast/app.scm 415 */
																		BgL_typez00_bglt BgL_arg2084z00_2146;

																		BgL_arg2084z00_2146 =
																			(((BgL_variablez00_bglt)
																				COBJECT(BgL_vz00_4167))->BgL_typez00);
																		BgL_auxz00_5556 =
																			BGl_strictzd2nodezd2typez00zzast_nodez00((
																				(BgL_typez00_bglt)
																				BGl_za2_za2z00zztype_cachez00),
																			BgL_arg2084z00_2146);
																	}
																	((((BgL_nodez00_bglt) COBJECT(
																					((BgL_nodez00_bglt)
																						BgL_new1139z00_2145)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) BgL_auxz00_5556),
																		BUNSPEC);
																}
																((((BgL_nodezf2effectzf2_bglt) COBJECT(
																				((BgL_nodezf2effectzf2_bglt)
																					BgL_new1139z00_2145)))->
																		BgL_sidezd2effectzd2) =
																	((obj_t) BUNSPEC), BUNSPEC);
																((((BgL_nodezf2effectzf2_bglt)
																			COBJECT(((BgL_nodezf2effectzf2_bglt)
																					BgL_new1139z00_2145)))->BgL_keyz00) =
																	((obj_t) BINT(-1L)), BUNSPEC);
																{
																	obj_t BgL_auxz00_5567;

																	{	/* Ast/app.scm 416 */
																		obj_t BgL_arg2086z00_2147;

																		BgL_arg2086z00_2147 =
																			MAKE_YOUNG_PAIR(
																			((obj_t) BgL_varz00_2140),
																			((obj_t) BgL_valz00_2141));
																		{	/* Ast/app.scm 416 */
																			obj_t BgL_list2087z00_2148;

																			BgL_list2087z00_2148 =
																				MAKE_YOUNG_PAIR(BgL_arg2086z00_2147,
																				BNIL);
																			BgL_auxz00_5567 = BgL_list2087z00_2148;
																	}}
																	((((BgL_letzd2varzd2_bglt)
																				COBJECT(BgL_new1139z00_2145))->
																			BgL_bindingsz00) =
																		((obj_t) BgL_auxz00_5567), BUNSPEC);
																}
																((((BgL_letzd2varzd2_bglt)
																			COBJECT(BgL_new1139z00_2145))->
																		BgL_bodyz00) =
																	((BgL_nodez00_bglt) ((BgL_nodez00_bglt)
																			BgL_bodyz00_2144)), BUNSPEC);
																((((BgL_letzd2varzd2_bglt)
																			COBJECT(BgL_new1139z00_2145))->
																		BgL_removablezf3zf3) =
																	((bool_t) ((bool_t) 1)), BUNSPEC);
																return ((BgL_nodezf2effectzf2_bglt)
																	BgL_new1139z00_2145);
															}
														}
													}
												}
											}
										}
									}
								}
						}
				}
		}

	}



/* make-fx-app-node */
	obj_t BGl_makezd2fxzd2appzd2nodezd2zzast_appz00(obj_t BgL_locz00_27,
		BgL_varz00_bglt BgL_varz00_28, obj_t BgL_argsz00_29)
	{
		{	/* Ast/app.scm 452 */
			{	/* Ast/app.scm 453 */
				BgL_variablez00_bglt BgL_vz00_2184;

				BgL_vz00_2184 =
					(((BgL_varz00_bglt) COBJECT(BgL_varz00_28))->BgL_variablez00);
				{	/* Ast/app.scm 455 */
					bool_t BgL_test2516z00_5578;

					{	/* Ast/app.scm 455 */
						bool_t BgL_test2517z00_5579;

						{	/* Ast/app.scm 455 */
							BgL_valuez00_bglt BgL_arg2138z00_2226;

							BgL_arg2138z00_2226 =
								(((BgL_variablez00_bglt) COBJECT(BgL_vz00_2184))->BgL_valuez00);
							{	/* Ast/app.scm 455 */
								obj_t BgL_classz00_3389;

								BgL_classz00_3389 = BGl_cfunz00zzast_varz00;
								{	/* Ast/app.scm 455 */
									BgL_objectz00_bglt BgL_arg1807z00_3391;

									{	/* Ast/app.scm 455 */
										obj_t BgL_tmpz00_5581;

										BgL_tmpz00_5581 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg2138z00_2226));
										BgL_arg1807z00_3391 =
											(BgL_objectz00_bglt) (BgL_tmpz00_5581);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Ast/app.scm 455 */
											long BgL_idxz00_3397;

											BgL_idxz00_3397 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3391);
											BgL_test2517z00_5579 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_3397 + 3L)) == BgL_classz00_3389);
										}
									else
										{	/* Ast/app.scm 455 */
											bool_t BgL_res2315z00_3422;

											{	/* Ast/app.scm 455 */
												obj_t BgL_oclassz00_3405;

												{	/* Ast/app.scm 455 */
													obj_t BgL_arg1815z00_3413;
													long BgL_arg1816z00_3414;

													BgL_arg1815z00_3413 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Ast/app.scm 455 */
														long BgL_arg1817z00_3415;

														BgL_arg1817z00_3415 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3391);
														BgL_arg1816z00_3414 =
															(BgL_arg1817z00_3415 - OBJECT_TYPE);
													}
													BgL_oclassz00_3405 =
														VECTOR_REF(BgL_arg1815z00_3413,
														BgL_arg1816z00_3414);
												}
												{	/* Ast/app.scm 455 */
													bool_t BgL__ortest_1115z00_3406;

													BgL__ortest_1115z00_3406 =
														(BgL_classz00_3389 == BgL_oclassz00_3405);
													if (BgL__ortest_1115z00_3406)
														{	/* Ast/app.scm 455 */
															BgL_res2315z00_3422 = BgL__ortest_1115z00_3406;
														}
													else
														{	/* Ast/app.scm 455 */
															long BgL_odepthz00_3407;

															{	/* Ast/app.scm 455 */
																obj_t BgL_arg1804z00_3408;

																BgL_arg1804z00_3408 = (BgL_oclassz00_3405);
																BgL_odepthz00_3407 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_3408);
															}
															if ((3L < BgL_odepthz00_3407))
																{	/* Ast/app.scm 455 */
																	obj_t BgL_arg1802z00_3410;

																	{	/* Ast/app.scm 455 */
																		obj_t BgL_arg1803z00_3411;

																		BgL_arg1803z00_3411 = (BgL_oclassz00_3405);
																		BgL_arg1802z00_3410 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_3411, 3L);
																	}
																	BgL_res2315z00_3422 =
																		(BgL_arg1802z00_3410 == BgL_classz00_3389);
																}
															else
																{	/* Ast/app.scm 455 */
																	BgL_res2315z00_3422 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2517z00_5579 = BgL_res2315z00_3422;
										}
								}
							}
						}
						if (BgL_test2517z00_5579)
							{	/* Ast/app.scm 455 */
								BgL_test2516z00_5578 =
									CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
										(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_globalz00_bglt) BgL_vz00_2184))))->BgL_idz00),
										CNST_TABLE_REF(9)));
							}
						else
							{	/* Ast/app.scm 455 */
								BgL_test2516z00_5578 = ((bool_t) 0);
							}
					}
					if (BgL_test2516z00_5578)
						{	/* Ast/app.scm 455 */
							BGL_TAIL return
								BGl_makezd2specialzd2appzd2nodezd2zzast_appz00(BgL_locz00_27,
								BgL_varz00_28, BgL_argsz00_29);
						}
					else
						{	/* Ast/app.scm 459 */
							bool_t BgL_test2521z00_5611;

							{	/* Ast/app.scm 459 */
								BgL_valuez00_bglt BgL_arg2137z00_2224;

								BgL_arg2137z00_2224 =
									(((BgL_variablez00_bglt) COBJECT(BgL_vz00_2184))->
									BgL_valuez00);
								{	/* Ast/app.scm 459 */
									obj_t BgL_classz00_3426;

									BgL_classz00_3426 = BGl_funz00zzast_varz00;
									{	/* Ast/app.scm 459 */
										BgL_objectz00_bglt BgL_arg1807z00_3428;

										{	/* Ast/app.scm 459 */
											obj_t BgL_tmpz00_5613;

											BgL_tmpz00_5613 =
												((obj_t) ((BgL_objectz00_bglt) BgL_arg2137z00_2224));
											BgL_arg1807z00_3428 =
												(BgL_objectz00_bglt) (BgL_tmpz00_5613);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Ast/app.scm 459 */
												long BgL_idxz00_3434;

												BgL_idxz00_3434 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3428);
												BgL_test2521z00_5611 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_3434 + 2L)) == BgL_classz00_3426);
											}
										else
											{	/* Ast/app.scm 459 */
												bool_t BgL_res2316z00_3459;

												{	/* Ast/app.scm 459 */
													obj_t BgL_oclassz00_3442;

													{	/* Ast/app.scm 459 */
														obj_t BgL_arg1815z00_3450;
														long BgL_arg1816z00_3451;

														BgL_arg1815z00_3450 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Ast/app.scm 459 */
															long BgL_arg1817z00_3452;

															BgL_arg1817z00_3452 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3428);
															BgL_arg1816z00_3451 =
																(BgL_arg1817z00_3452 - OBJECT_TYPE);
														}
														BgL_oclassz00_3442 =
															VECTOR_REF(BgL_arg1815z00_3450,
															BgL_arg1816z00_3451);
													}
													{	/* Ast/app.scm 459 */
														bool_t BgL__ortest_1115z00_3443;

														BgL__ortest_1115z00_3443 =
															(BgL_classz00_3426 == BgL_oclassz00_3442);
														if (BgL__ortest_1115z00_3443)
															{	/* Ast/app.scm 459 */
																BgL_res2316z00_3459 = BgL__ortest_1115z00_3443;
															}
														else
															{	/* Ast/app.scm 459 */
																long BgL_odepthz00_3444;

																{	/* Ast/app.scm 459 */
																	obj_t BgL_arg1804z00_3445;

																	BgL_arg1804z00_3445 = (BgL_oclassz00_3442);
																	BgL_odepthz00_3444 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_3445);
																}
																if ((2L < BgL_odepthz00_3444))
																	{	/* Ast/app.scm 459 */
																		obj_t BgL_arg1802z00_3447;

																		{	/* Ast/app.scm 459 */
																			obj_t BgL_arg1803z00_3448;

																			BgL_arg1803z00_3448 =
																				(BgL_oclassz00_3442);
																			BgL_arg1802z00_3447 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_3448, 2L);
																		}
																		BgL_res2316z00_3459 =
																			(BgL_arg1802z00_3447 ==
																			BgL_classz00_3426);
																	}
																else
																	{	/* Ast/app.scm 459 */
																		BgL_res2316z00_3459 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2521z00_5611 = BgL_res2316z00_3459;
											}
									}
								}
							}
							if (BgL_test2521z00_5611)
								{	/* Ast/app.scm 461 */
									BgL_appz00_bglt BgL_callz00_2191;

									{	/* Ast/app.scm 461 */
										BgL_appz00_bglt BgL_new1141z00_2205;

										{	/* Ast/app.scm 462 */
											BgL_appz00_bglt BgL_new1140z00_2213;

											BgL_new1140z00_2213 =
												((BgL_appz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_appz00_bgl))));
											{	/* Ast/app.scm 462 */
												long BgL_arg2133z00_2214;

												BgL_arg2133z00_2214 =
													BGL_CLASS_NUM(BGl_appz00zzast_nodez00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1140z00_2213),
													BgL_arg2133z00_2214);
											}
											{	/* Ast/app.scm 462 */
												BgL_objectz00_bglt BgL_tmpz00_5640;

												BgL_tmpz00_5640 =
													((BgL_objectz00_bglt) BgL_new1140z00_2213);
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5640, BFALSE);
											}
											((BgL_objectz00_bglt) BgL_new1140z00_2213);
											BgL_new1141z00_2205 = BgL_new1140z00_2213;
										}
										((((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt) BgL_new1141z00_2205)))->
												BgL_locz00) = ((obj_t) BgL_locz00_27), BUNSPEC);
										{
											BgL_typez00_bglt BgL_auxz00_5646;

											{	/* Ast/app.scm 463 */
												BgL_typez00_bglt BgL_arg2130z00_2206;

												BgL_arg2130z00_2206 =
													(((BgL_variablez00_bglt) COBJECT(BgL_vz00_2184))->
													BgL_typez00);
												BgL_auxz00_5646 =
													BGl_strictzd2nodezd2typez00zzast_nodez00((
														(BgL_typez00_bglt) BGl_za2_za2z00zztype_cachez00),
													BgL_arg2130z00_2206);
											}
											((((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt) BgL_new1141z00_2205)))->
													BgL_typez00) =
												((BgL_typez00_bglt) BgL_auxz00_5646), BUNSPEC);
										}
										((((BgL_nodezf2effectzf2_bglt) COBJECT(
														((BgL_nodezf2effectzf2_bglt)
															BgL_new1141z00_2205)))->BgL_sidezd2effectzd2) =
											((obj_t) BUNSPEC), BUNSPEC);
										((((BgL_nodezf2effectzf2_bglt)
													COBJECT(((BgL_nodezf2effectzf2_bglt)
															BgL_new1141z00_2205)))->BgL_keyz00) =
											((obj_t) BINT(-1L)), BUNSPEC);
										{
											BgL_varz00_bglt BgL_auxz00_5657;

											{	/* Ast/app.scm 464 */
												bool_t BgL_test2525z00_5658;

												{	/* Ast/app.scm 464 */
													obj_t BgL_classz00_3465;

													BgL_classz00_3465 = BGl_closurez00zzast_nodez00;
													{	/* Ast/app.scm 464 */
														BgL_objectz00_bglt BgL_arg1807z00_3467;

														{	/* Ast/app.scm 464 */
															obj_t BgL_tmpz00_5659;

															BgL_tmpz00_5659 =
																((obj_t) ((BgL_objectz00_bglt) BgL_varz00_28));
															BgL_arg1807z00_3467 =
																(BgL_objectz00_bglt) (BgL_tmpz00_5659);
														}
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Ast/app.scm 464 */
																long BgL_idxz00_3473;

																BgL_idxz00_3473 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_3467);
																{	/* Ast/app.scm 464 */
																	obj_t BgL_arg1800z00_3474;

																	{	/* Ast/app.scm 464 */
																		long BgL_arg1801z00_3475;

																		BgL_arg1801z00_3475 =
																			(BgL_idxz00_3473 + 3L);
																		{	/* Ast/app.scm 464 */
																			obj_t BgL_vectorz00_3477;

																			BgL_vectorz00_3477 =
																				BGl_za2inheritancesza2z00zz__objectz00;
																			BgL_arg1800z00_3474 =
																				VECTOR_REF(BgL_vectorz00_3477,
																				BgL_arg1801z00_3475);
																	}}
																	BgL_test2525z00_5658 =
																		(BgL_arg1800z00_3474 == BgL_classz00_3465);
															}}
														else
															{	/* Ast/app.scm 464 */
																bool_t BgL_res2317z00_3498;

																{	/* Ast/app.scm 464 */
																	obj_t BgL_oclassz00_3481;

																	{	/* Ast/app.scm 464 */
																		obj_t BgL_arg1815z00_3489;
																		long BgL_arg1816z00_3490;

																		BgL_arg1815z00_3489 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Ast/app.scm 464 */
																			long BgL_arg1817z00_3491;

																			BgL_arg1817z00_3491 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_3467);
																			BgL_arg1816z00_3490 =
																				(BgL_arg1817z00_3491 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_3481 =
																			VECTOR_REF(BgL_arg1815z00_3489,
																			BgL_arg1816z00_3490);
																	}
																	{	/* Ast/app.scm 464 */
																		bool_t BgL__ortest_1115z00_3482;

																		BgL__ortest_1115z00_3482 =
																			(BgL_classz00_3465 == BgL_oclassz00_3481);
																		if (BgL__ortest_1115z00_3482)
																			{	/* Ast/app.scm 464 */
																				BgL_res2317z00_3498 =
																					BgL__ortest_1115z00_3482;
																			}
																		else
																			{	/* Ast/app.scm 464 */
																				long BgL_odepthz00_3483;

																				{	/* Ast/app.scm 464 */
																					obj_t BgL_arg1804z00_3484;

																					BgL_arg1804z00_3484 =
																						(BgL_oclassz00_3481);
																					BgL_odepthz00_3483 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_3484);
																				}
																				if ((3L < BgL_odepthz00_3483))
																					{	/* Ast/app.scm 464 */
																						obj_t BgL_arg1802z00_3486;

																						{	/* Ast/app.scm 464 */
																							obj_t BgL_arg1803z00_3487;

																							BgL_arg1803z00_3487 =
																								(BgL_oclassz00_3481);
																							BgL_arg1802z00_3486 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_3487, 3L);
																						}
																						BgL_res2317z00_3498 =
																							(BgL_arg1802z00_3486 ==
																							BgL_classz00_3465);
																					}
																				else
																					{	/* Ast/app.scm 464 */
																						BgL_res2317z00_3498 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2525z00_5658 = BgL_res2317z00_3498;
															}
													}
												}
												if (BgL_test2525z00_5658)
													{	/* Ast/app.scm 464 */
														BgL_refz00_bglt BgL_new1142z00_2209;

														{	/* Ast/app.scm 464 */
															BgL_refz00_bglt BgL_new1146z00_2211;

															BgL_new1146z00_2211 =
																((BgL_refz00_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_refz00_bgl))));
															{	/* Ast/app.scm 464 */
																long BgL_arg2132z00_2212;

																{	/* Ast/app.scm 464 */
																	obj_t BgL_classz00_3499;

																	BgL_classz00_3499 = BGl_refz00zzast_nodez00;
																	BgL_arg2132z00_2212 =
																		BGL_CLASS_NUM(BgL_classz00_3499);
																}
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1146z00_2211),
																	BgL_arg2132z00_2212);
															}
															{	/* Ast/app.scm 464 */
																BgL_objectz00_bglt BgL_tmpz00_5686;

																BgL_tmpz00_5686 =
																	((BgL_objectz00_bglt) BgL_new1146z00_2211);
																BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5686,
																	BFALSE);
															}
															((BgL_objectz00_bglt) BgL_new1146z00_2211);
															BgL_new1142z00_2209 = BgL_new1146z00_2211;
														}
														((((BgL_nodez00_bglt) COBJECT(
																		((BgL_nodez00_bglt) BgL_new1142z00_2209)))->
																BgL_locz00) =
															((obj_t) (((BgL_nodez00_bglt)
																		COBJECT(((BgL_nodez00_bglt)
																				BgL_varz00_28)))->BgL_locz00)),
															BUNSPEC);
														((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																			BgL_new1142z00_2209)))->BgL_typez00) =
															((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																		COBJECT(((BgL_nodez00_bglt)
																				BgL_varz00_28)))->BgL_typez00)),
															BUNSPEC);
														((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																			BgL_new1142z00_2209)))->BgL_variablez00) =
															((BgL_variablez00_bglt) (((BgL_varz00_bglt)
																		COBJECT(((BgL_varz00_bglt) (
																					(BgL_nodez00_bglt) BgL_varz00_28))))->
																	BgL_variablez00)), BUNSPEC);
														BgL_auxz00_5657 =
															((BgL_varz00_bglt) BgL_new1142z00_2209);
													}
												else
													{	/* Ast/app.scm 464 */
														BgL_auxz00_5657 = BgL_varz00_28;
													}
											}
											((((BgL_appz00_bglt) COBJECT(BgL_new1141z00_2205))->
													BgL_funz00) =
												((BgL_varz00_bglt) BgL_auxz00_5657), BUNSPEC);
										}
										((((BgL_appz00_bglt) COBJECT(BgL_new1141z00_2205))->
												BgL_argsz00) = ((obj_t) BgL_argsz00_29), BUNSPEC);
										((((BgL_appz00_bglt) COBJECT(BgL_new1141z00_2205))->
												BgL_stackablez00) = ((obj_t) BFALSE), BUNSPEC);
										BgL_callz00_2191 = BgL_new1141z00_2205;
									}
									{	/* Ast/app.scm 466 */
										bool_t BgL_test2529z00_5707;

										{	/* Ast/app.scm 466 */
											BgL_typez00_bglt BgL_arg2129z00_2204;

											BgL_arg2129z00_2204 =
												(((BgL_variablez00_bglt) COBJECT(BgL_vz00_2184))->
												BgL_typez00);
											BgL_test2529z00_5707 =
												(((obj_t) BgL_arg2129z00_2204) ==
												BGl_za2voidza2z00zztype_cachez00);
										}
										if (BgL_test2529z00_5707)
											{	/* Ast/app.scm 468 */
												BgL_literalz00_bglt BgL_unspecz00_2194;

												{	/* Ast/app.scm 468 */
													BgL_literalz00_bglt BgL_new1148z00_2200;

													{	/* Ast/app.scm 468 */
														BgL_literalz00_bglt BgL_new1147z00_2202;

														BgL_new1147z00_2202 =
															((BgL_literalz00_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_literalz00_bgl))));
														{	/* Ast/app.scm 468 */
															long BgL_arg2127z00_2203;

															BgL_arg2127z00_2203 =
																BGL_CLASS_NUM(BGl_literalz00zzast_nodez00);
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_new1147z00_2202),
																BgL_arg2127z00_2203);
														}
														{	/* Ast/app.scm 468 */
															BgL_objectz00_bglt BgL_tmpz00_5715;

															BgL_tmpz00_5715 =
																((BgL_objectz00_bglt) BgL_new1147z00_2202);
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5715, BFALSE);
														}
														((BgL_objectz00_bglt) BgL_new1147z00_2202);
														BgL_new1148z00_2200 = BgL_new1147z00_2202;
													}
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_new1148z00_2200)))->
															BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
													{
														BgL_typez00_bglt BgL_auxz00_5721;

														{	/* Ast/app.scm 469 */
															BgL_typez00_bglt BgL_arg2126z00_2201;

															BgL_arg2126z00_2201 =
																BGl_getzd2typezd2atomz00zztype_typeofz00
																(BUNSPEC);
															BgL_auxz00_5721 =
																BGl_strictzd2nodezd2typez00zzast_nodez00
																(BgL_arg2126z00_2201,
																((BgL_typez00_bglt)
																	BGl_za2objza2z00zztype_cachez00));
														}
														((((BgL_nodez00_bglt) COBJECT(
																		((BgL_nodez00_bglt) BgL_new1148z00_2200)))->
																BgL_typez00) =
															((BgL_typez00_bglt) BgL_auxz00_5721), BUNSPEC);
													}
													((((BgL_atomz00_bglt) COBJECT(
																	((BgL_atomz00_bglt) BgL_new1148z00_2200)))->
															BgL_valuez00) = ((obj_t) BUNSPEC), BUNSPEC);
													BgL_unspecz00_2194 = BgL_new1148z00_2200;
												}
												{	/* Ast/app.scm 471 */
													BgL_sequencez00_bglt BgL_new1150z00_2195;

													{	/* Ast/app.scm 471 */
														BgL_sequencez00_bglt BgL_new1149z00_2198;

														BgL_new1149z00_2198 =
															((BgL_sequencez00_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_sequencez00_bgl))));
														{	/* Ast/app.scm 471 */
															long BgL_arg2125z00_2199;

															BgL_arg2125z00_2199 =
																BGL_CLASS_NUM(BGl_sequencez00zzast_nodez00);
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_new1149z00_2198),
																BgL_arg2125z00_2199);
														}
														{	/* Ast/app.scm 471 */
															BgL_objectz00_bglt BgL_tmpz00_5733;

															BgL_tmpz00_5733 =
																((BgL_objectz00_bglt) BgL_new1149z00_2198);
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5733, BFALSE);
														}
														((BgL_objectz00_bglt) BgL_new1149z00_2198);
														BgL_new1150z00_2195 = BgL_new1149z00_2198;
													}
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_new1150z00_2195)))->
															BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
													((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																		BgL_new1150z00_2195)))->BgL_typez00) =
														((BgL_typez00_bglt)
															BGl_strictzd2nodezd2typez00zzast_nodez00((
																	(BgL_typez00_bglt)
																	BGl_za2_za2z00zztype_cachez00),
																((BgL_typez00_bglt)
																	BGl_za2objza2z00zztype_cachez00))), BUNSPEC);
													((((BgL_nodezf2effectzf2_bglt)
																COBJECT(((BgL_nodezf2effectzf2_bglt)
																		BgL_new1150z00_2195)))->
															BgL_sidezd2effectzd2) =
														((obj_t) BUNSPEC), BUNSPEC);
													((((BgL_nodezf2effectzf2_bglt)
																COBJECT(((BgL_nodezf2effectzf2_bglt)
																		BgL_new1150z00_2195)))->BgL_keyz00) =
														((obj_t) BINT(-1L)), BUNSPEC);
													{
														obj_t BgL_auxz00_5749;

														{	/* Ast/app.scm 473 */
															obj_t BgL_list2123z00_2196;

															{	/* Ast/app.scm 473 */
																obj_t BgL_arg2124z00_2197;

																BgL_arg2124z00_2197 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_unspecz00_2194), BNIL);
																BgL_list2123z00_2196 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_callz00_2191),
																	BgL_arg2124z00_2197);
															}
															BgL_auxz00_5749 = BgL_list2123z00_2196;
														}
														((((BgL_sequencez00_bglt)
																	COBJECT(BgL_new1150z00_2195))->BgL_nodesz00) =
															((obj_t) BgL_auxz00_5749), BUNSPEC);
													}
													((((BgL_sequencez00_bglt)
																COBJECT(BgL_new1150z00_2195))->BgL_unsafez00) =
														((bool_t) ((bool_t) 0)), BUNSPEC);
													((((BgL_sequencez00_bglt)
																COBJECT(BgL_new1150z00_2195))->BgL_metaz00) =
														((obj_t) BNIL), BUNSPEC);
													return ((obj_t) BgL_new1150z00_2195);
												}
											}
										else
											{	/* Ast/app.scm 466 */
												return ((obj_t) BgL_callz00_2191);
											}
									}
								}
							else
								{	/* Ast/app.scm 477 */
									BgL_funcallz00_bglt BgL_new1152z00_2215;

									{	/* Ast/app.scm 478 */
										BgL_funcallz00_bglt BgL_new1151z00_2222;

										BgL_new1151z00_2222 =
											((BgL_funcallz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_funcallz00_bgl))));
										{	/* Ast/app.scm 478 */
											long BgL_arg2136z00_2223;

											BgL_arg2136z00_2223 =
												BGL_CLASS_NUM(BGl_funcallz00zzast_nodez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1151z00_2222),
												BgL_arg2136z00_2223);
										}
										{	/* Ast/app.scm 478 */
											BgL_objectz00_bglt BgL_tmpz00_5763;

											BgL_tmpz00_5763 =
												((BgL_objectz00_bglt) BgL_new1151z00_2222);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5763, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1151z00_2222);
										BgL_new1152z00_2215 = BgL_new1151z00_2222;
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1152z00_2215)))->
											BgL_locz00) = ((obj_t) BgL_locz00_27), BUNSPEC);
									((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_new1152z00_2215)))->BgL_typez00) =
										((BgL_typez00_bglt) ((BgL_typez00_bglt)
												BGl_za2_za2z00zztype_cachez00)), BUNSPEC);
									((((BgL_funcallz00_bglt) COBJECT(BgL_new1152z00_2215))->
											BgL_funz00) =
										((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_varz00_28)),
										BUNSPEC);
									{
										obj_t BgL_auxz00_5774;

										{	/* Ast/app.scm 481 */
											BgL_refz00_bglt BgL_arg2134z00_2216;

											{	/* Ast/app.scm 481 */
												BgL_refz00_bglt BgL_new1153z00_2218;

												{	/* Ast/app.scm 481 */
													BgL_refz00_bglt BgL_new1157z00_2220;

													BgL_new1157z00_2220 =
														((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_refz00_bgl))));
													{	/* Ast/app.scm 481 */
														long BgL_arg2135z00_2221;

														{	/* Ast/app.scm 481 */
															obj_t BgL_classz00_3517;

															BgL_classz00_3517 = BGl_refz00zzast_nodez00;
															BgL_arg2135z00_2221 =
																BGL_CLASS_NUM(BgL_classz00_3517);
														}
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1157z00_2220),
															BgL_arg2135z00_2221);
													}
													{	/* Ast/app.scm 481 */
														BgL_objectz00_bglt BgL_tmpz00_5779;

														BgL_tmpz00_5779 =
															((BgL_objectz00_bglt) BgL_new1157z00_2220);
														BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5779, BFALSE);
													}
													((BgL_objectz00_bglt) BgL_new1157z00_2220);
													BgL_new1153z00_2218 = BgL_new1157z00_2220;
												}
												((((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt) BgL_new1153z00_2218)))->
														BgL_locz00) =
													((obj_t) (((BgL_nodez00_bglt)
																COBJECT(((BgL_nodez00_bglt) BgL_varz00_28)))->
															BgL_locz00)), BUNSPEC);
												((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																	BgL_new1153z00_2218)))->BgL_typez00) =
													((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																COBJECT(((BgL_nodez00_bglt) BgL_varz00_28)))->
															BgL_typez00)), BUNSPEC);
												((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																	BgL_new1153z00_2218)))->BgL_variablez00) =
													((BgL_variablez00_bglt) (((BgL_varz00_bglt)
																COBJECT(((BgL_varz00_bglt) ((BgL_nodez00_bglt)
																			BgL_varz00_28))))->BgL_variablez00)),
													BUNSPEC);
												BgL_arg2134z00_2216 = BgL_new1153z00_2218;
											}
											BgL_auxz00_5774 =
												MAKE_YOUNG_PAIR(
												((obj_t) BgL_arg2134z00_2216), BgL_argsz00_29);
										}
										((((BgL_funcallz00_bglt) COBJECT(BgL_new1152z00_2215))->
												BgL_argsz00) = ((obj_t) BgL_auxz00_5774), BUNSPEC);
									}
									((((BgL_funcallz00_bglt) COBJECT(BgL_new1152z00_2215))->
											BgL_strengthz00) = ((obj_t) CNST_TABLE_REF(10)), BUNSPEC);
									((((BgL_funcallz00_bglt) COBJECT(BgL_new1152z00_2215))->
											BgL_functionsz00) = ((obj_t) BUNSPEC), BUNSPEC);
									return ((obj_t) BgL_new1152z00_2215);
								}
						}
				}
			}
		}

	}



/* make-va-app-node */
	BgL_nodez00_bglt BGl_makezd2vazd2appzd2nodezd2zzast_appz00(long
		BgL_arityz00_30, obj_t BgL_stackz00_31, obj_t BgL_locz00_32,
		BgL_varz00_bglt BgL_varz00_33, obj_t BgL_argsz00_34)
	{
		{	/* Ast/app.scm 486 */
			{
				obj_t BgL_oldzd2argszd2_2230;
				long BgL_arityz00_2231;
				obj_t BgL_fzd2argszd2_2232;

				BgL_oldzd2argszd2_2230 = BgL_argsz00_34;
				BgL_arityz00_2231 = BgL_arityz00_30;
				BgL_fzd2argszd2_2232 = BNIL;
			BgL_zc3z04anonymousza32139ze3z87_2233:
				if ((BgL_arityz00_2231 == -1L))
					{	/* Ast/app.scm 495 */
						obj_t BgL_lzd2argzd2_2235;

						BgL_lzd2argzd2_2235 =
							BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
							(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(11)));
						{	/* Ast/app.scm 495 */
							obj_t BgL_lzd2expzd2_2236;

							{	/* Ast/app.scm 496 */
								obj_t BgL_arg2144z00_2244;
								obj_t BgL_arg2145z00_2245;

								BgL_arg2144z00_2244 = BGl_letzd2symzd2zzast_letz00();
								{	/* Ast/app.scm 496 */
									obj_t BgL_arg2146z00_2246;
									obj_t BgL_arg2147z00_2247;

									{	/* Ast/app.scm 496 */
										obj_t BgL_arg2148z00_2248;

										{	/* Ast/app.scm 496 */
											obj_t BgL_arg2149z00_2249;

											BgL_arg2149z00_2249 =
												MAKE_YOUNG_PAIR
												(BGl_makezd2argszd2listze70ze7zzast_appz00
												(BgL_oldzd2argszd2_2230), BNIL);
											BgL_arg2148z00_2248 =
												MAKE_YOUNG_PAIR(BgL_lzd2argzd2_2235,
												BgL_arg2149z00_2249);
										}
										BgL_arg2146z00_2246 =
											MAKE_YOUNG_PAIR(BgL_arg2148z00_2248, BNIL);
									}
									BgL_arg2147z00_2247 =
										MAKE_YOUNG_PAIR(BgL_lzd2argzd2_2235, BNIL);
									BgL_arg2145z00_2245 =
										MAKE_YOUNG_PAIR(BgL_arg2146z00_2246, BgL_arg2147z00_2247);
								}
								BgL_lzd2expzd2_2236 =
									MAKE_YOUNG_PAIR(BgL_arg2144z00_2244, BgL_arg2145z00_2245);
							}
							{	/* Ast/app.scm 496 */
								BgL_nodez00_bglt BgL_lzd2nodezd2_2237;

								BgL_lzd2nodezd2_2237 =
									BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_lzd2expzd2_2236,
									BgL_stackz00_31, BgL_locz00_32, CNST_TABLE_REF(2));
								{	/* Ast/app.scm 498 */
									BgL_nodez00_bglt BgL_lzd2varzd2_2238;

									BgL_lzd2varzd2_2238 =
										(((BgL_letzd2varzd2_bglt) COBJECT(
												((BgL_letzd2varzd2_bglt) BgL_lzd2nodezd2_2237)))->
										BgL_bodyz00);
									{	/* Ast/app.scm 499 */
										obj_t BgL_appz00_2239;

										{	/* Ast/app.scm 501 */
											obj_t BgL_arg2142z00_2242;

											{	/* Ast/app.scm 501 */
												obj_t BgL_arg2143z00_2243;

												BgL_arg2143z00_2243 =
													MAKE_YOUNG_PAIR(
													((obj_t) BgL_lzd2varzd2_2238), BgL_fzd2argszd2_2232);
												BgL_arg2142z00_2242 =
													bgl_reverse_bang(BgL_arg2143z00_2243);
											}
											BgL_appz00_2239 =
												BGl_makezd2fxzd2appzd2nodezd2zzast_appz00(BgL_locz00_32,
												BgL_varz00_33, BgL_arg2142z00_2242);
										}
										{	/* Ast/app.scm 500 */

											((((BgL_letzd2varzd2_bglt) COBJECT(
															((BgL_letzd2varzd2_bglt) BgL_lzd2nodezd2_2237)))->
													BgL_bodyz00) =
												((BgL_nodez00_bglt) ((BgL_nodez00_bglt)
														BgL_appz00_2239)), BUNSPEC);
											{
												BgL_typez00_bglt BgL_auxz00_5827;

												{	/* Ast/app.scm 504 */
													BgL_typez00_bglt BgL_arg2141z00_2241;

													BgL_arg2141z00_2241 =
														(((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt)
																	((BgL_letzd2varzd2_bglt)
																		BgL_lzd2nodezd2_2237))))->BgL_typez00);
													BgL_auxz00_5827 =
														BGl_strictzd2nodezd2typez00zzast_nodez00((
															(BgL_typez00_bglt) BGl_za2_za2z00zztype_cachez00),
														BgL_arg2141z00_2241);
												}
												((((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt)
																	((BgL_letzd2varzd2_bglt)
																		BgL_lzd2nodezd2_2237))))->BgL_typez00) =
													((BgL_typez00_bglt) BgL_auxz00_5827), BUNSPEC);
											}
											return
												BGl_cleanzd2userzd2nodez12z12zzast_appz00
												(BgL_lzd2nodezd2_2237);
										}
									}
								}
							}
						}
					}
				else
					{	/* Ast/app.scm 506 */
						obj_t BgL_arg2152z00_2252;
						long BgL_arg2154z00_2253;
						obj_t BgL_arg2155z00_2254;

						BgL_arg2152z00_2252 = CDR(((obj_t) BgL_oldzd2argszd2_2230));
						BgL_arg2154z00_2253 = (BgL_arityz00_2231 + 1L);
						{	/* Ast/app.scm 508 */
							BgL_nodez00_bglt BgL_arg2156z00_2255;

							{	/* Ast/app.scm 508 */
								obj_t BgL_arg2157z00_2256;

								BgL_arg2157z00_2256 = CAR(((obj_t) BgL_oldzd2argszd2_2230));
								BgL_arg2156z00_2255 =
									BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_arg2157z00_2256,
									BgL_stackz00_31, BgL_locz00_32, CNST_TABLE_REF(2));
							}
							BgL_arg2155z00_2254 =
								MAKE_YOUNG_PAIR(
								((obj_t) BgL_arg2156z00_2255), BgL_fzd2argszd2_2232);
						}
						{
							obj_t BgL_fzd2argszd2_5848;
							long BgL_arityz00_5847;
							obj_t BgL_oldzd2argszd2_5846;

							BgL_oldzd2argszd2_5846 = BgL_arg2152z00_2252;
							BgL_arityz00_5847 = BgL_arg2154z00_2253;
							BgL_fzd2argszd2_5848 = BgL_arg2155z00_2254;
							BgL_fzd2argszd2_2232 = BgL_fzd2argszd2_5848;
							BgL_arityz00_2231 = BgL_arityz00_5847;
							BgL_oldzd2argszd2_2230 = BgL_oldzd2argszd2_5846;
							goto BgL_zc3z04anonymousza32139ze3z87_2233;
						}
					}
			}
		}

	}



/* make-args-list~0 */
	obj_t BGl_makezd2argszd2listze70ze7zzast_appz00(obj_t BgL_argsz00_2258)
	{
		{	/* Ast/app.scm 490 */
			if (NULLP(BgL_argsz00_2258))
				{	/* Ast/app.scm 488 */
					return CNST_TABLE_REF(1);
				}
			else
				{	/* Ast/app.scm 490 */
					obj_t BgL_arg2160z00_2261;
					obj_t BgL_arg2161z00_2262;

					{	/* Ast/app.scm 490 */
						obj_t BgL_arg2162z00_2263;

						{	/* Ast/app.scm 490 */
							obj_t BgL_arg2163z00_2264;

							BgL_arg2163z00_2264 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(12), BNIL);
							BgL_arg2162z00_2263 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(13), BgL_arg2163z00_2264);
						}
						BgL_arg2160z00_2261 =
							MAKE_YOUNG_PAIR(CNST_TABLE_REF(7), BgL_arg2162z00_2263);
					}
					{	/* Ast/app.scm 490 */
						obj_t BgL_arg2164z00_2265;
						obj_t BgL_arg2165z00_2266;

						BgL_arg2164z00_2265 = CAR(((obj_t) BgL_argsz00_2258));
						{	/* Ast/app.scm 490 */
							obj_t BgL_arg2166z00_2267;

							{	/* Ast/app.scm 490 */
								obj_t BgL_arg2167z00_2268;

								BgL_arg2167z00_2268 = CDR(((obj_t) BgL_argsz00_2258));
								BgL_arg2166z00_2267 =
									BGl_makezd2argszd2listze70ze7zzast_appz00
									(BgL_arg2167z00_2268);
							}
							BgL_arg2165z00_2266 = MAKE_YOUNG_PAIR(BgL_arg2166z00_2267, BNIL);
						}
						BgL_arg2161z00_2262 =
							MAKE_YOUNG_PAIR(BgL_arg2164z00_2265, BgL_arg2165z00_2266);
					}
					return MAKE_YOUNG_PAIR(BgL_arg2160z00_2261, BgL_arg2161z00_2262);
				}
		}

	}



/* make-special-app-node */
	obj_t BGl_makezd2specialzd2appzd2nodezd2zzast_appz00(obj_t BgL_locz00_36,
		BgL_varz00_bglt BgL_varz00_37, obj_t BgL_argsz00_38)
	{
		{	/* Ast/app.scm 528 */
			{	/* Ast/app.scm 529 */
				BgL_variablez00_bglt BgL_variablez00_2271;

				BgL_variablez00_2271 =
					(((BgL_varz00_bglt) COBJECT(BgL_varz00_37))->BgL_variablez00);
				{	/* Ast/app.scm 529 */
					obj_t BgL_gnamez00_2272;

					BgL_gnamez00_2272 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_globalz00_bglt) BgL_variablez00_2271))))->BgL_namez00);
					{	/* Ast/app.scm 530 */

						{	/* Ast/app.scm 531 */
							obj_t BgL_casezd2valuezd2_2273;

							BgL_casezd2valuezd2_2273 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt)
											((BgL_globalz00_bglt) BgL_variablez00_2271))))->
								BgL_idz00);
							if ((BgL_casezd2valuezd2_2273 == CNST_TABLE_REF(14)))
								{	/* Ast/app.scm 531 */
									{	/* Ast/app.scm 535 */
										bool_t BgL_test2533z00_5876;

										{	/* Ast/app.scm 535 */
											bool_t BgL_test2534z00_5877;

											{	/* Ast/app.scm 535 */
												obj_t BgL_arg2179z00_2286;

												BgL_arg2179z00_2286 = CAR(((obj_t) BgL_argsz00_38));
												{	/* Ast/app.scm 535 */
													obj_t BgL_classz00_3535;

													BgL_classz00_3535 = BGl_varz00zzast_nodez00;
													if (BGL_OBJECTP(BgL_arg2179z00_2286))
														{	/* Ast/app.scm 535 */
															BgL_objectz00_bglt BgL_arg1807z00_3537;

															BgL_arg1807z00_3537 =
																(BgL_objectz00_bglt) (BgL_arg2179z00_2286);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Ast/app.scm 535 */
																	long BgL_idxz00_3543;

																	BgL_idxz00_3543 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_3537);
																	BgL_test2534z00_5877 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_3543 + 2L)) ==
																		BgL_classz00_3535);
																}
															else
																{	/* Ast/app.scm 535 */
																	bool_t BgL_res2319z00_3568;

																	{	/* Ast/app.scm 535 */
																		obj_t BgL_oclassz00_3551;

																		{	/* Ast/app.scm 535 */
																			obj_t BgL_arg1815z00_3559;
																			long BgL_arg1816z00_3560;

																			BgL_arg1815z00_3559 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Ast/app.scm 535 */
																				long BgL_arg1817z00_3561;

																				BgL_arg1817z00_3561 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_3537);
																				BgL_arg1816z00_3560 =
																					(BgL_arg1817z00_3561 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_3551 =
																				VECTOR_REF(BgL_arg1815z00_3559,
																				BgL_arg1816z00_3560);
																		}
																		{	/* Ast/app.scm 535 */
																			bool_t BgL__ortest_1115z00_3552;

																			BgL__ortest_1115z00_3552 =
																				(BgL_classz00_3535 ==
																				BgL_oclassz00_3551);
																			if (BgL__ortest_1115z00_3552)
																				{	/* Ast/app.scm 535 */
																					BgL_res2319z00_3568 =
																						BgL__ortest_1115z00_3552;
																				}
																			else
																				{	/* Ast/app.scm 535 */
																					long BgL_odepthz00_3553;

																					{	/* Ast/app.scm 535 */
																						obj_t BgL_arg1804z00_3554;

																						BgL_arg1804z00_3554 =
																							(BgL_oclassz00_3551);
																						BgL_odepthz00_3553 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_3554);
																					}
																					if ((2L < BgL_odepthz00_3553))
																						{	/* Ast/app.scm 535 */
																							obj_t BgL_arg1802z00_3556;

																							{	/* Ast/app.scm 535 */
																								obj_t BgL_arg1803z00_3557;

																								BgL_arg1803z00_3557 =
																									(BgL_oclassz00_3551);
																								BgL_arg1802z00_3556 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_3557, 2L);
																							}
																							BgL_res2319z00_3568 =
																								(BgL_arg1802z00_3556 ==
																								BgL_classz00_3535);
																						}
																					else
																						{	/* Ast/app.scm 535 */
																							BgL_res2319z00_3568 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2534z00_5877 = BgL_res2319z00_3568;
																}
														}
													else
														{	/* Ast/app.scm 535 */
															BgL_test2534z00_5877 = ((bool_t) 0);
														}
												}
											}
											if (BgL_test2534z00_5877)
												{	/* Ast/app.scm 535 */
													bool_t BgL_test2539z00_5902;

													{	/* Ast/app.scm 535 */
														obj_t BgL_arg2178z00_2285;

														BgL_arg2178z00_2285 = CAR(((obj_t) BgL_argsz00_38));
														{	/* Ast/app.scm 535 */
															obj_t BgL_classz00_3570;

															BgL_classz00_3570 = BGl_closurez00zzast_nodez00;
															if (BGL_OBJECTP(BgL_arg2178z00_2285))
																{	/* Ast/app.scm 535 */
																	BgL_objectz00_bglt BgL_arg1807z00_3572;

																	BgL_arg1807z00_3572 =
																		(BgL_objectz00_bglt) (BgL_arg2178z00_2285);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Ast/app.scm 535 */
																			long BgL_idxz00_3578;

																			BgL_idxz00_3578 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_3572);
																			BgL_test2539z00_5902 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_3578 + 3L)) ==
																				BgL_classz00_3570);
																		}
																	else
																		{	/* Ast/app.scm 535 */
																			bool_t BgL_res2320z00_3603;

																			{	/* Ast/app.scm 535 */
																				obj_t BgL_oclassz00_3586;

																				{	/* Ast/app.scm 535 */
																					obj_t BgL_arg1815z00_3594;
																					long BgL_arg1816z00_3595;

																					BgL_arg1815z00_3594 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Ast/app.scm 535 */
																						long BgL_arg1817z00_3596;

																						BgL_arg1817z00_3596 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_3572);
																						BgL_arg1816z00_3595 =
																							(BgL_arg1817z00_3596 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_3586 =
																						VECTOR_REF(BgL_arg1815z00_3594,
																						BgL_arg1816z00_3595);
																				}
																				{	/* Ast/app.scm 535 */
																					bool_t BgL__ortest_1115z00_3587;

																					BgL__ortest_1115z00_3587 =
																						(BgL_classz00_3570 ==
																						BgL_oclassz00_3586);
																					if (BgL__ortest_1115z00_3587)
																						{	/* Ast/app.scm 535 */
																							BgL_res2320z00_3603 =
																								BgL__ortest_1115z00_3587;
																						}
																					else
																						{	/* Ast/app.scm 535 */
																							long BgL_odepthz00_3588;

																							{	/* Ast/app.scm 535 */
																								obj_t BgL_arg1804z00_3589;

																								BgL_arg1804z00_3589 =
																									(BgL_oclassz00_3586);
																								BgL_odepthz00_3588 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_3589);
																							}
																							if ((3L < BgL_odepthz00_3588))
																								{	/* Ast/app.scm 535 */
																									obj_t BgL_arg1802z00_3591;

																									{	/* Ast/app.scm 535 */
																										obj_t BgL_arg1803z00_3592;

																										BgL_arg1803z00_3592 =
																											(BgL_oclassz00_3586);
																										BgL_arg1802z00_3591 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_3592, 3L);
																									}
																									BgL_res2320z00_3603 =
																										(BgL_arg1802z00_3591 ==
																										BgL_classz00_3570);
																								}
																							else
																								{	/* Ast/app.scm 535 */
																									BgL_res2320z00_3603 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test2539z00_5902 =
																				BgL_res2320z00_3603;
																		}
																}
															else
																{	/* Ast/app.scm 535 */
																	BgL_test2539z00_5902 = ((bool_t) 0);
																}
														}
													}
													if (BgL_test2539z00_5902)
														{	/* Ast/app.scm 535 */
															BgL_test2533z00_5876 = ((bool_t) 0);
														}
													else
														{	/* Ast/app.scm 535 */
															BgL_test2533z00_5876 = ((bool_t) 1);
														}
												}
											else
												{	/* Ast/app.scm 535 */
													BgL_test2533z00_5876 = ((bool_t) 0);
												}
										}
										if (BgL_test2533z00_5876)
											{	/* Ast/app.scm 536 */
												obj_t BgL_arg2177z00_2282;

												BgL_arg2177z00_2282 = CAR(((obj_t) BgL_argsz00_38));
												{	/* Ast/app.scm 536 */
													BgL_typez00_bglt BgL_vz00_3606;

													BgL_vz00_3606 =
														((BgL_typez00_bglt) BGl_za2_za2z00zztype_cachez00);
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_arg2177z00_2282)))->
															BgL_typez00) =
														((BgL_typez00_bglt) BgL_vz00_3606), BUNSPEC);
												}
											}
										else
											{	/* Ast/app.scm 535 */
												BFALSE;
											}
									}
									{	/* Ast/app.scm 537 */
										bool_t BgL_test2544z00_5932;

										{	/* Ast/app.scm 537 */
											bool_t BgL_test2545z00_5933;

											{	/* Ast/app.scm 537 */
												obj_t BgL_arg2189z00_2298;

												{	/* Ast/app.scm 537 */
													obj_t BgL_pairz00_3610;

													BgL_pairz00_3610 = CDR(((obj_t) BgL_argsz00_38));
													BgL_arg2189z00_2298 = CAR(BgL_pairz00_3610);
												}
												{	/* Ast/app.scm 537 */
													obj_t BgL_classz00_3611;

													BgL_classz00_3611 = BGl_varz00zzast_nodez00;
													if (BGL_OBJECTP(BgL_arg2189z00_2298))
														{	/* Ast/app.scm 537 */
															BgL_objectz00_bglt BgL_arg1807z00_3613;

															BgL_arg1807z00_3613 =
																(BgL_objectz00_bglt) (BgL_arg2189z00_2298);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Ast/app.scm 537 */
																	long BgL_idxz00_3619;

																	BgL_idxz00_3619 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_3613);
																	BgL_test2545z00_5933 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_3619 + 2L)) ==
																		BgL_classz00_3611);
																}
															else
																{	/* Ast/app.scm 537 */
																	bool_t BgL_res2321z00_3644;

																	{	/* Ast/app.scm 537 */
																		obj_t BgL_oclassz00_3627;

																		{	/* Ast/app.scm 537 */
																			obj_t BgL_arg1815z00_3635;
																			long BgL_arg1816z00_3636;

																			BgL_arg1815z00_3635 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Ast/app.scm 537 */
																				long BgL_arg1817z00_3637;

																				BgL_arg1817z00_3637 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_3613);
																				BgL_arg1816z00_3636 =
																					(BgL_arg1817z00_3637 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_3627 =
																				VECTOR_REF(BgL_arg1815z00_3635,
																				BgL_arg1816z00_3636);
																		}
																		{	/* Ast/app.scm 537 */
																			bool_t BgL__ortest_1115z00_3628;

																			BgL__ortest_1115z00_3628 =
																				(BgL_classz00_3611 ==
																				BgL_oclassz00_3627);
																			if (BgL__ortest_1115z00_3628)
																				{	/* Ast/app.scm 537 */
																					BgL_res2321z00_3644 =
																						BgL__ortest_1115z00_3628;
																				}
																			else
																				{	/* Ast/app.scm 537 */
																					long BgL_odepthz00_3629;

																					{	/* Ast/app.scm 537 */
																						obj_t BgL_arg1804z00_3630;

																						BgL_arg1804z00_3630 =
																							(BgL_oclassz00_3627);
																						BgL_odepthz00_3629 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_3630);
																					}
																					if ((2L < BgL_odepthz00_3629))
																						{	/* Ast/app.scm 537 */
																							obj_t BgL_arg1802z00_3632;

																							{	/* Ast/app.scm 537 */
																								obj_t BgL_arg1803z00_3633;

																								BgL_arg1803z00_3633 =
																									(BgL_oclassz00_3627);
																								BgL_arg1802z00_3632 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_3633, 2L);
																							}
																							BgL_res2321z00_3644 =
																								(BgL_arg1802z00_3632 ==
																								BgL_classz00_3611);
																						}
																					else
																						{	/* Ast/app.scm 537 */
																							BgL_res2321z00_3644 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2545z00_5933 = BgL_res2321z00_3644;
																}
														}
													else
														{	/* Ast/app.scm 537 */
															BgL_test2545z00_5933 = ((bool_t) 0);
														}
												}
											}
											if (BgL_test2545z00_5933)
												{	/* Ast/app.scm 537 */
													bool_t BgL_test2550z00_5959;

													{	/* Ast/app.scm 537 */
														obj_t BgL_arg2188z00_2297;

														{	/* Ast/app.scm 537 */
															obj_t BgL_pairz00_3648;

															BgL_pairz00_3648 = CDR(((obj_t) BgL_argsz00_38));
															BgL_arg2188z00_2297 = CAR(BgL_pairz00_3648);
														}
														{	/* Ast/app.scm 537 */
															obj_t BgL_classz00_3649;

															BgL_classz00_3649 = BGl_closurez00zzast_nodez00;
															if (BGL_OBJECTP(BgL_arg2188z00_2297))
																{	/* Ast/app.scm 537 */
																	BgL_objectz00_bglt BgL_arg1807z00_3651;

																	BgL_arg1807z00_3651 =
																		(BgL_objectz00_bglt) (BgL_arg2188z00_2297);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Ast/app.scm 537 */
																			long BgL_idxz00_3657;

																			BgL_idxz00_3657 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_3651);
																			BgL_test2550z00_5959 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_3657 + 3L)) ==
																				BgL_classz00_3649);
																		}
																	else
																		{	/* Ast/app.scm 537 */
																			bool_t BgL_res2322z00_3682;

																			{	/* Ast/app.scm 537 */
																				obj_t BgL_oclassz00_3665;

																				{	/* Ast/app.scm 537 */
																					obj_t BgL_arg1815z00_3673;
																					long BgL_arg1816z00_3674;

																					BgL_arg1815z00_3673 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Ast/app.scm 537 */
																						long BgL_arg1817z00_3675;

																						BgL_arg1817z00_3675 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_3651);
																						BgL_arg1816z00_3674 =
																							(BgL_arg1817z00_3675 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_3665 =
																						VECTOR_REF(BgL_arg1815z00_3673,
																						BgL_arg1816z00_3674);
																				}
																				{	/* Ast/app.scm 537 */
																					bool_t BgL__ortest_1115z00_3666;

																					BgL__ortest_1115z00_3666 =
																						(BgL_classz00_3649 ==
																						BgL_oclassz00_3665);
																					if (BgL__ortest_1115z00_3666)
																						{	/* Ast/app.scm 537 */
																							BgL_res2322z00_3682 =
																								BgL__ortest_1115z00_3666;
																						}
																					else
																						{	/* Ast/app.scm 537 */
																							long BgL_odepthz00_3667;

																							{	/* Ast/app.scm 537 */
																								obj_t BgL_arg1804z00_3668;

																								BgL_arg1804z00_3668 =
																									(BgL_oclassz00_3665);
																								BgL_odepthz00_3667 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_3668);
																							}
																							if ((3L < BgL_odepthz00_3667))
																								{	/* Ast/app.scm 537 */
																									obj_t BgL_arg1802z00_3670;

																									{	/* Ast/app.scm 537 */
																										obj_t BgL_arg1803z00_3671;

																										BgL_arg1803z00_3671 =
																											(BgL_oclassz00_3665);
																										BgL_arg1802z00_3670 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_3671, 3L);
																									}
																									BgL_res2322z00_3682 =
																										(BgL_arg1802z00_3670 ==
																										BgL_classz00_3649);
																								}
																							else
																								{	/* Ast/app.scm 537 */
																									BgL_res2322z00_3682 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test2550z00_5959 =
																				BgL_res2322z00_3682;
																		}
																}
															else
																{	/* Ast/app.scm 537 */
																	BgL_test2550z00_5959 = ((bool_t) 0);
																}
														}
													}
													if (BgL_test2550z00_5959)
														{	/* Ast/app.scm 537 */
															BgL_test2544z00_5932 = ((bool_t) 0);
														}
													else
														{	/* Ast/app.scm 537 */
															BgL_test2544z00_5932 = ((bool_t) 1);
														}
												}
											else
												{	/* Ast/app.scm 537 */
													BgL_test2544z00_5932 = ((bool_t) 0);
												}
										}
										if (BgL_test2544z00_5932)
											{	/* Ast/app.scm 538 */
												obj_t BgL_arg2187z00_2294;

												{	/* Ast/app.scm 538 */
													obj_t BgL_pairz00_3686;

													BgL_pairz00_3686 = CDR(((obj_t) BgL_argsz00_38));
													BgL_arg2187z00_2294 = CAR(BgL_pairz00_3686);
												}
												{	/* Ast/app.scm 538 */
													BgL_typez00_bglt BgL_vz00_3688;

													BgL_vz00_3688 =
														((BgL_typez00_bglt) BGl_za2_za2z00zztype_cachez00);
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_arg2187z00_2294)))->
															BgL_typez00) =
														((BgL_typez00_bglt) BgL_vz00_3688), BUNSPEC);
												}
											}
										else
											{	/* Ast/app.scm 537 */
												BFALSE;
											}
									}
									{	/* Ast/app.scm 539 */
										BgL_appz00_bglt BgL_new1162z00_2299;

										{	/* Ast/app.scm 540 */
											BgL_appz00_bglt BgL_new1161z00_2301;

											BgL_new1161z00_2301 =
												((BgL_appz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_appz00_bgl))));
											{	/* Ast/app.scm 540 */
												long BgL_arg2191z00_2302;

												BgL_arg2191z00_2302 =
													BGL_CLASS_NUM(BGl_appz00zzast_nodez00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1161z00_2301),
													BgL_arg2191z00_2302);
											}
											{	/* Ast/app.scm 540 */
												BgL_objectz00_bglt BgL_tmpz00_5995;

												BgL_tmpz00_5995 =
													((BgL_objectz00_bglt) BgL_new1161z00_2301);
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5995, BFALSE);
											}
											((BgL_objectz00_bglt) BgL_new1161z00_2301);
											BgL_new1162z00_2299 = BgL_new1161z00_2301;
										}
										((((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt) BgL_new1162z00_2299)))->
												BgL_locz00) = ((obj_t) BgL_locz00_36), BUNSPEC);
										{
											BgL_typez00_bglt BgL_auxz00_6001;

											{	/* Ast/app.scm 541 */
												BgL_typez00_bglt BgL_arg2190z00_2300;

												BgL_arg2190z00_2300 =
													(((BgL_variablez00_bglt)
														COBJECT(BgL_variablez00_2271))->BgL_typez00);
												BgL_auxz00_6001 =
													BGl_strictzd2nodezd2typez00zzast_nodez00((
														(BgL_typez00_bglt) BGl_za2_za2z00zztype_cachez00),
													BgL_arg2190z00_2300);
											}
											((((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt) BgL_new1162z00_2299)))->
													BgL_typez00) =
												((BgL_typez00_bglt) BgL_auxz00_6001), BUNSPEC);
										}
										((((BgL_nodezf2effectzf2_bglt) COBJECT(
														((BgL_nodezf2effectzf2_bglt)
															BgL_new1162z00_2299)))->BgL_sidezd2effectzd2) =
											((obj_t) BUNSPEC), BUNSPEC);
										((((BgL_nodezf2effectzf2_bglt)
													COBJECT(((BgL_nodezf2effectzf2_bglt)
															BgL_new1162z00_2299)))->BgL_keyz00) =
											((obj_t) BINT(-1L)), BUNSPEC);
										((((BgL_appz00_bglt) COBJECT(BgL_new1162z00_2299))->
												BgL_funz00) =
											((BgL_varz00_bglt) BgL_varz00_37), BUNSPEC);
										((((BgL_appz00_bglt) COBJECT(BgL_new1162z00_2299))->
												BgL_argsz00) = ((obj_t) BgL_argsz00_38), BUNSPEC);
										((((BgL_appz00_bglt) COBJECT(BgL_new1162z00_2299))->
												BgL_stackablez00) = ((obj_t) BFALSE), BUNSPEC);
										return ((obj_t) BgL_new1162z00_2299);
									}
								}
							else
								{	/* Ast/app.scm 531 */
									if ((BgL_casezd2valuezd2_2273 == CNST_TABLE_REF(15)))
										{	/* Ast/app.scm 531 */
											{	/* Ast/app.scm 547 */
												obj_t BgL_arg2193z00_2304;

												BgL_arg2193z00_2304 = CAR(((obj_t) BgL_argsz00_38));
												{	/* Ast/app.scm 547 */
													BgL_typez00_bglt BgL_vz00_3697;

													BgL_vz00_3697 =
														((BgL_typez00_bglt) BGl_za2_za2z00zztype_cachez00);
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_arg2193z00_2304)))->
															BgL_typez00) =
														((BgL_typez00_bglt) BgL_vz00_3697), BUNSPEC);
												}
											}
											{	/* Ast/app.scm 548 */
												BgL_appz00_bglt BgL_new1164z00_2305;

												{	/* Ast/app.scm 549 */
													BgL_appz00_bglt BgL_new1163z00_2307;

													BgL_new1163z00_2307 =
														((BgL_appz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_appz00_bgl))));
													{	/* Ast/app.scm 549 */
														long BgL_arg2196z00_2308;

														BgL_arg2196z00_2308 =
															BGL_CLASS_NUM(BGl_appz00zzast_nodez00);
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1163z00_2307),
															BgL_arg2196z00_2308);
													}
													{	/* Ast/app.scm 549 */
														BgL_objectz00_bglt BgL_tmpz00_6028;

														BgL_tmpz00_6028 =
															((BgL_objectz00_bglt) BgL_new1163z00_2307);
														BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6028, BFALSE);
													}
													((BgL_objectz00_bglt) BgL_new1163z00_2307);
													BgL_new1164z00_2305 = BgL_new1163z00_2307;
												}
												((((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt) BgL_new1164z00_2305)))->
														BgL_locz00) = ((obj_t) BgL_locz00_36), BUNSPEC);
												{
													BgL_typez00_bglt BgL_auxz00_6034;

													{	/* Ast/app.scm 550 */
														BgL_typez00_bglt BgL_arg2194z00_2306;

														BgL_arg2194z00_2306 =
															(((BgL_variablez00_bglt)
																COBJECT(BgL_variablez00_2271))->BgL_typez00);
														BgL_auxz00_6034 =
															BGl_strictzd2nodezd2typez00zzast_nodez00((
																(BgL_typez00_bglt)
																BGl_za2_za2z00zztype_cachez00),
															BgL_arg2194z00_2306);
													}
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_new1164z00_2305)))->
															BgL_typez00) =
														((BgL_typez00_bglt) BgL_auxz00_6034), BUNSPEC);
												}
												((((BgL_nodezf2effectzf2_bglt) COBJECT(
																((BgL_nodezf2effectzf2_bglt)
																	BgL_new1164z00_2305)))->
														BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
												((((BgL_nodezf2effectzf2_bglt)
															COBJECT(((BgL_nodezf2effectzf2_bglt)
																	BgL_new1164z00_2305)))->BgL_keyz00) =
													((obj_t) BINT(-1L)), BUNSPEC);
												((((BgL_appz00_bglt) COBJECT(BgL_new1164z00_2305))->
														BgL_funz00) =
													((BgL_varz00_bglt) BgL_varz00_37), BUNSPEC);
												((((BgL_appz00_bglt) COBJECT(BgL_new1164z00_2305))->
														BgL_argsz00) = ((obj_t) BgL_argsz00_38), BUNSPEC);
												((((BgL_appz00_bglt) COBJECT(BgL_new1164z00_2305))->
														BgL_stackablez00) = ((obj_t) BFALSE), BUNSPEC);
												return ((obj_t) BgL_new1164z00_2305);
											}
										}
									else
										{	/* Ast/app.scm 531 */
											if ((BgL_casezd2valuezd2_2273 == CNST_TABLE_REF(16)))
												{	/* Ast/app.scm 554 */
													obj_t BgL_vtypez00_2310;

													{	/* Ast/app.scm 554 */
														obj_t BgL_pairz00_3706;

														BgL_pairz00_3706 =
															(((BgL_cfunz00_bglt) COBJECT(
																	((BgL_cfunz00_bglt)
																		(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						((BgL_globalz00_bglt)
																							BgL_variablez00_2271))))->
																			BgL_valuez00))))->BgL_argszd2typezd2);
														BgL_vtypez00_2310 = CAR(BgL_pairz00_3706);
													}
													{	/* Ast/app.scm 554 */
														obj_t BgL_ftypez00_2311;

														{	/* Ast/app.scm 555 */
															bool_t BgL_test2557z00_6058;

															{	/* Ast/app.scm 555 */
																obj_t BgL_classz00_3707;

																BgL_classz00_3707 =
																	BGl_tvecz00zztvector_tvectorz00;
																if (BGL_OBJECTP(BgL_vtypez00_2310))
																	{	/* Ast/app.scm 555 */
																		BgL_objectz00_bglt BgL_arg1807z00_3709;

																		BgL_arg1807z00_3709 =
																			(BgL_objectz00_bglt) (BgL_vtypez00_2310);
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Ast/app.scm 555 */
																				long BgL_idxz00_3715;

																				BgL_idxz00_3715 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_3709);
																				BgL_test2557z00_6058 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_3715 + 2L)) ==
																					BgL_classz00_3707);
																			}
																		else
																			{	/* Ast/app.scm 555 */
																				bool_t BgL_res2323z00_3740;

																				{	/* Ast/app.scm 555 */
																					obj_t BgL_oclassz00_3723;

																					{	/* Ast/app.scm 555 */
																						obj_t BgL_arg1815z00_3731;
																						long BgL_arg1816z00_3732;

																						BgL_arg1815z00_3731 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Ast/app.scm 555 */
																							long BgL_arg1817z00_3733;

																							BgL_arg1817z00_3733 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_3709);
																							BgL_arg1816z00_3732 =
																								(BgL_arg1817z00_3733 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_3723 =
																							VECTOR_REF(BgL_arg1815z00_3731,
																							BgL_arg1816z00_3732);
																					}
																					{	/* Ast/app.scm 555 */
																						bool_t BgL__ortest_1115z00_3724;

																						BgL__ortest_1115z00_3724 =
																							(BgL_classz00_3707 ==
																							BgL_oclassz00_3723);
																						if (BgL__ortest_1115z00_3724)
																							{	/* Ast/app.scm 555 */
																								BgL_res2323z00_3740 =
																									BgL__ortest_1115z00_3724;
																							}
																						else
																							{	/* Ast/app.scm 555 */
																								long BgL_odepthz00_3725;

																								{	/* Ast/app.scm 555 */
																									obj_t BgL_arg1804z00_3726;

																									BgL_arg1804z00_3726 =
																										(BgL_oclassz00_3723);
																									BgL_odepthz00_3725 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_3726);
																								}
																								if ((2L < BgL_odepthz00_3725))
																									{	/* Ast/app.scm 555 */
																										obj_t BgL_arg1802z00_3728;

																										{	/* Ast/app.scm 555 */
																											obj_t BgL_arg1803z00_3729;

																											BgL_arg1803z00_3729 =
																												(BgL_oclassz00_3723);
																											BgL_arg1802z00_3728 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_3729,
																												2L);
																										}
																										BgL_res2323z00_3740 =
																											(BgL_arg1802z00_3728 ==
																											BgL_classz00_3707);
																									}
																								else
																									{	/* Ast/app.scm 555 */
																										BgL_res2323z00_3740 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test2557z00_6058 =
																					BgL_res2323z00_3740;
																			}
																	}
																else
																	{	/* Ast/app.scm 555 */
																		BgL_test2557z00_6058 = ((bool_t) 0);
																	}
															}
															if (BgL_test2557z00_6058)
																{
																	BgL_typez00_bglt BgL_auxz00_6081;

																	{
																		BgL_tvecz00_bglt BgL_auxz00_6082;

																		{
																			obj_t BgL_auxz00_6083;

																			{	/* Ast/app.scm 556 */
																				BgL_objectz00_bglt BgL_tmpz00_6084;

																				BgL_tmpz00_6084 =
																					((BgL_objectz00_bglt)
																					((BgL_typez00_bglt)
																						BgL_vtypez00_2310));
																				BgL_auxz00_6083 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_6084);
																			}
																			BgL_auxz00_6082 =
																				((BgL_tvecz00_bglt) BgL_auxz00_6083);
																		}
																		BgL_auxz00_6081 =
																			(((BgL_tvecz00_bglt)
																				COBJECT(BgL_auxz00_6082))->
																			BgL_itemzd2typezd2);
																	}
																	BgL_ftypez00_2311 = ((obj_t) BgL_auxz00_6081);
																}
															else
																{	/* Ast/app.scm 555 */
																	BgL_ftypez00_2311 =
																		BGl_za2_za2z00zztype_cachez00;
																}
														}
														{	/* Ast/app.scm 555 */

															{	/* Ast/app.scm 558 */
																BgL_vlengthz00_bglt BgL_new1166z00_2312;

																{	/* Ast/app.scm 559 */
																	BgL_vlengthz00_bglt BgL_new1165z00_2313;

																	BgL_new1165z00_2313 =
																		((BgL_vlengthz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_vlengthz00_bgl))));
																	{	/* Ast/app.scm 559 */
																		long BgL_arg2198z00_2314;

																		BgL_arg2198z00_2314 =
																			BGL_CLASS_NUM
																			(BGl_vlengthz00zzast_nodez00);
																		BGL_OBJECT_CLASS_NUM_SET((
																				(BgL_objectz00_bglt)
																				BgL_new1165z00_2313),
																			BgL_arg2198z00_2314);
																	}
																	{	/* Ast/app.scm 559 */
																		BgL_objectz00_bglt BgL_tmpz00_6095;

																		BgL_tmpz00_6095 =
																			((BgL_objectz00_bglt)
																			BgL_new1165z00_2313);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6095,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1165z00_2313);
																	BgL_new1166z00_2312 = BgL_new1165z00_2313;
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1166z00_2312)))->BgL_locz00) =
																	((obj_t) BgL_locz00_36), BUNSPEC);
																((((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt)
																					BgL_new1166z00_2312)))->BgL_typez00) =
																	((BgL_typez00_bglt) (((BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt) (
																							(BgL_globalz00_bglt)
																							BgL_variablez00_2271))))->
																			BgL_typez00)), BUNSPEC);
																((((BgL_nodezf2effectzf2_bglt)
																			COBJECT(((BgL_nodezf2effectzf2_bglt)
																					BgL_new1166z00_2312)))->
																		BgL_sidezd2effectzd2) =
																	((obj_t) BUNSPEC), BUNSPEC);
																((((BgL_nodezf2effectzf2_bglt)
																			COBJECT(((BgL_nodezf2effectzf2_bglt)
																					BgL_new1166z00_2312)))->BgL_keyz00) =
																	((obj_t) BINT(-1L)), BUNSPEC);
																((((BgL_externz00_bglt)
																			COBJECT(((BgL_externz00_bglt)
																					BgL_new1166z00_2312)))->
																		BgL_exprza2za2) =
																	((obj_t) BgL_argsz00_38), BUNSPEC);
																((((BgL_externz00_bglt)
																			COBJECT(((BgL_externz00_bglt)
																					BgL_new1166z00_2312)))->
																		BgL_effectz00) =
																	((obj_t) BUNSPEC), BUNSPEC);
																((((BgL_privatez00_bglt)
																			COBJECT(((BgL_privatez00_bglt)
																					BgL_new1166z00_2312)))->
																		BgL_czd2formatzd2) =
																	((obj_t) string_append(BgL_gnamez00_2272,
																			BGl_string2344z00zzast_appz00)), BUNSPEC);
																((((BgL_vlengthz00_bglt)
																			COBJECT(BgL_new1166z00_2312))->
																		BgL_vtypez00) =
																	((BgL_typez00_bglt) ((BgL_typez00_bglt)
																			BgL_vtypez00_2310)), BUNSPEC);
																((((BgL_vlengthz00_bglt)
																			COBJECT(BgL_new1166z00_2312))->
																		BgL_ftypez00) =
																	((obj_t) BgL_ftypez00_2311), BUNSPEC);
																return ((obj_t) BgL_new1166z00_2312);
															}
														}
													}
												}
											else
												{	/* Ast/app.scm 531 */
													bool_t BgL_test2562z00_6122;

													{	/* Ast/app.scm 531 */
														bool_t BgL__ortest_1167z00_2365;

														BgL__ortest_1167z00_2365 =
															(BgL_casezd2valuezd2_2273 == CNST_TABLE_REF(17));
														if (BgL__ortest_1167z00_2365)
															{	/* Ast/app.scm 531 */
																BgL_test2562z00_6122 = BgL__ortest_1167z00_2365;
															}
														else
															{	/* Ast/app.scm 531 */
																BgL_test2562z00_6122 =
																	(BgL_casezd2valuezd2_2273 ==
																	CNST_TABLE_REF(18));
															}
													}
													if (BgL_test2562z00_6122)
														{	/* Ast/app.scm 566 */
															BgL_vrefz00_bglt BgL_new1169z00_2320;

															{	/* Ast/app.scm 567 */
																BgL_vrefz00_bglt BgL_new1168z00_2334;

																BgL_new1168z00_2334 =
																	((BgL_vrefz00_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_vrefz00_bgl))));
																{	/* Ast/app.scm 567 */
																	long BgL_arg2210z00_2335;

																	BgL_arg2210z00_2335 =
																		BGL_CLASS_NUM(BGl_vrefz00zzast_nodez00);
																	BGL_OBJECT_CLASS_NUM_SET(
																		((BgL_objectz00_bglt) BgL_new1168z00_2334),
																		BgL_arg2210z00_2335);
																}
																{	/* Ast/app.scm 567 */
																	BgL_objectz00_bglt BgL_tmpz00_6132;

																	BgL_tmpz00_6132 =
																		((BgL_objectz00_bglt) BgL_new1168z00_2334);
																	BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6132,
																		BFALSE);
																}
																((BgL_objectz00_bglt) BgL_new1168z00_2334);
																BgL_new1169z00_2320 = BgL_new1168z00_2334;
															}
															((((BgL_nodez00_bglt) COBJECT(
																			((BgL_nodez00_bglt)
																				BgL_new1169z00_2320)))->BgL_locz00) =
																((obj_t) BgL_locz00_36), BUNSPEC);
															{
																BgL_typez00_bglt BgL_auxz00_6138;

																{	/* Ast/app.scm 568 */
																	BgL_typez00_bglt BgL_arg2203z00_2321;

																	BgL_arg2203z00_2321 =
																		(((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					((BgL_globalz00_bglt)
																						BgL_variablez00_2271))))->
																		BgL_typez00);
																	BgL_auxz00_6138 =
																		BGl_strictzd2nodezd2typez00zzast_nodez00((
																			(BgL_typez00_bglt)
																			BGl_za2_za2z00zztype_cachez00),
																		BgL_arg2203z00_2321);
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1169z00_2320)))->BgL_typez00) =
																	((BgL_typez00_bglt) BgL_auxz00_6138),
																	BUNSPEC);
															}
															((((BgL_nodezf2effectzf2_bglt) COBJECT(
																			((BgL_nodezf2effectzf2_bglt)
																				BgL_new1169z00_2320)))->
																	BgL_sidezd2effectzd2) =
																((obj_t) BUNSPEC), BUNSPEC);
															((((BgL_nodezf2effectzf2_bglt)
																		COBJECT(((BgL_nodezf2effectzf2_bglt)
																				BgL_new1169z00_2320)))->BgL_keyz00) =
																((obj_t) BINT(-1L)), BUNSPEC);
															((((BgL_externz00_bglt)
																		COBJECT(((BgL_externz00_bglt)
																				BgL_new1169z00_2320)))->
																	BgL_exprza2za2) =
																((obj_t) BgL_argsz00_38), BUNSPEC);
															((((BgL_externz00_bglt)
																		COBJECT(((BgL_externz00_bglt)
																				BgL_new1169z00_2320)))->BgL_effectz00) =
																((obj_t) BUNSPEC), BUNSPEC);
															((((BgL_privatez00_bglt)
																		COBJECT(((BgL_privatez00_bglt)
																				BgL_new1169z00_2320)))->
																	BgL_czd2formatzd2) =
																((obj_t) string_append(BgL_gnamez00_2272,
																		BGl_string2345z00zzast_appz00)), BUNSPEC);
															((((BgL_vrefz00_bglt)
																		COBJECT(BgL_new1169z00_2320))->
																	BgL_ftypez00) =
																((BgL_typez00_bglt) (((BgL_variablez00_bglt)
																			COBJECT(((BgL_variablez00_bglt) (
																						(BgL_globalz00_bglt)
																						BgL_variablez00_2271))))->
																		BgL_typez00)), BUNSPEC);
															{
																BgL_typez00_bglt BgL_auxz00_6162;

																{	/* Ast/app.scm 574 */
																	obj_t BgL_pairz00_3756;

																	BgL_pairz00_3756 =
																		(((BgL_cfunz00_bglt) COBJECT(
																				((BgL_cfunz00_bglt)
																					(((BgL_variablez00_bglt) COBJECT(
																								((BgL_variablez00_bglt)
																									((BgL_globalz00_bglt)
																										BgL_variablez00_2271))))->
																						BgL_valuez00))))->
																		BgL_argszd2typezd2);
																	{	/* Ast/app.scm 574 */
																		obj_t BgL_pairz00_3759;

																		BgL_pairz00_3759 = CDR(BgL_pairz00_3756);
																		BgL_auxz00_6162 =
																			((BgL_typez00_bglt)
																			CAR(BgL_pairz00_3759));
																}}
																((((BgL_vrefz00_bglt)
																			COBJECT(BgL_new1169z00_2320))->
																		BgL_otypez00) =
																	((BgL_typez00_bglt) BgL_auxz00_6162),
																	BUNSPEC);
															}
															{
																BgL_typez00_bglt BgL_auxz00_6172;

																{	/* Ast/app.scm 572 */
																	obj_t BgL_pairz00_3762;

																	BgL_pairz00_3762 =
																		(((BgL_cfunz00_bglt) COBJECT(
																				((BgL_cfunz00_bglt)
																					(((BgL_variablez00_bglt) COBJECT(
																								((BgL_variablez00_bglt)
																									((BgL_globalz00_bglt)
																										BgL_variablez00_2271))))->
																						BgL_valuez00))))->
																		BgL_argszd2typezd2);
																	BgL_auxz00_6172 =
																		((BgL_typez00_bglt) CAR(BgL_pairz00_3762));
																}
																((((BgL_vrefz00_bglt)
																			COBJECT(BgL_new1169z00_2320))->
																		BgL_vtypez00) =
																	((BgL_typez00_bglt) BgL_auxz00_6172),
																	BUNSPEC);
															}
															{
																bool_t BgL_auxz00_6181;

																{	/* Ast/app.scm 571 */
																	obj_t BgL_arg2208z00_2326;

																	{	/* Ast/app.scm 571 */
																		obj_t BgL_arg2209z00_2333;

																		BgL_arg2209z00_2333 =
																			(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						((BgL_globalz00_bglt)
																							BgL_variablez00_2271))))->
																			BgL_idz00);
																		BgL_arg2208z00_2326 =
																			SYMBOL_TO_STRING(BgL_arg2209z00_2333);
																	}
																	{	/* Ast/app.scm 571 */

																		BgL_auxz00_6181 =
																			BGl_stringzd2suffixzf3z21zz__r4_strings_6_7z00
																			(BGl_string2346z00zzast_appz00,
																			BgL_arg2208z00_2326, BFALSE, BFALSE,
																			BFALSE, BFALSE);
																}}
																((((BgL_vrefz00_bglt)
																			COBJECT(BgL_new1169z00_2320))->
																		BgL_unsafez00) =
																	((bool_t) BgL_auxz00_6181), BUNSPEC);
															}
															return ((obj_t) BgL_new1169z00_2320);
														}
													else
														{	/* Ast/app.scm 531 */
															bool_t BgL_test2564z00_6189;

															{	/* Ast/app.scm 531 */
																bool_t BgL__ortest_1170z00_2364;

																BgL__ortest_1170z00_2364 =
																	(BgL_casezd2valuezd2_2273 ==
																	CNST_TABLE_REF(19));
																if (BgL__ortest_1170z00_2364)
																	{	/* Ast/app.scm 531 */
																		BgL_test2564z00_6189 =
																			BgL__ortest_1170z00_2364;
																	}
																else
																	{	/* Ast/app.scm 531 */
																		BgL_test2564z00_6189 =
																			(BgL_casezd2valuezd2_2273 ==
																			CNST_TABLE_REF(20));
																	}
															}
															if (BgL_test2564z00_6189)
																{	/* Ast/app.scm 531 */
																	{	/* Ast/app.scm 578 */
																		obj_t BgL_arg2212z00_2338;

																		{	/* Ast/app.scm 578 */
																			obj_t BgL_pairz00_3770;

																			{	/* Ast/app.scm 578 */
																				obj_t BgL_pairz00_3769;

																				BgL_pairz00_3769 =
																					CDR(((obj_t) BgL_argsz00_38));
																				BgL_pairz00_3770 =
																					CDR(BgL_pairz00_3769);
																			}
																			BgL_arg2212z00_2338 =
																				CAR(BgL_pairz00_3770);
																		}
																		{	/* Ast/app.scm 578 */
																			BgL_typez00_bglt BgL_vz00_3772;

																			BgL_vz00_3772 =
																				((BgL_typez00_bglt)
																				BGl_za2_za2z00zztype_cachez00);
																			((((BgL_nodez00_bglt)
																						COBJECT(((BgL_nodez00_bglt)
																								BgL_arg2212z00_2338)))->
																					BgL_typez00) =
																				((BgL_typez00_bglt) BgL_vz00_3772),
																				BUNSPEC);
																		}
																	}
																	{	/* Ast/app.scm 579 */
																		BgL_vsetz12z12_bglt BgL_new1172z00_2339;

																		{	/* Ast/app.scm 580 */
																			BgL_vsetz12z12_bglt BgL_new1171z00_2348;

																			BgL_new1171z00_2348 =
																				((BgL_vsetz12z12_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_vsetz12z12_bgl))));
																			{	/* Ast/app.scm 580 */
																				long BgL_arg2221z00_2349;

																				BgL_arg2221z00_2349 =
																					BGL_CLASS_NUM
																					(BGl_vsetz12z12zzast_nodez00);
																				BGL_OBJECT_CLASS_NUM_SET((
																						(BgL_objectz00_bglt)
																						BgL_new1171z00_2348),
																					BgL_arg2221z00_2349);
																			}
																			{	/* Ast/app.scm 580 */
																				BgL_objectz00_bglt BgL_tmpz00_6206;

																				BgL_tmpz00_6206 =
																					((BgL_objectz00_bglt)
																					BgL_new1171z00_2348);
																				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6206,
																					BFALSE);
																			}
																			((BgL_objectz00_bglt)
																				BgL_new1171z00_2348);
																			BgL_new1172z00_2339 = BgL_new1171z00_2348;
																		}
																		((((BgL_nodez00_bglt) COBJECT(
																						((BgL_nodez00_bglt)
																							BgL_new1172z00_2339)))->
																				BgL_locz00) =
																			((obj_t) BgL_locz00_36), BUNSPEC);
																		{
																			BgL_typez00_bglt BgL_auxz00_6212;

																			{	/* Ast/app.scm 581 */
																				BgL_typez00_bglt BgL_arg2213z00_2340;

																				BgL_arg2213z00_2340 =
																					(((BgL_variablez00_bglt) COBJECT(
																							((BgL_variablez00_bglt)
																								((BgL_globalz00_bglt)
																									BgL_variablez00_2271))))->
																					BgL_typez00);
																				BgL_auxz00_6212 =
																					BGl_strictzd2nodezd2typez00zzast_nodez00
																					(((BgL_typez00_bglt)
																						BGl_za2unspecza2z00zztype_cachez00),
																					BgL_arg2213z00_2340);
																			}
																			((((BgL_nodez00_bglt) COBJECT(
																							((BgL_nodez00_bglt)
																								BgL_new1172z00_2339)))->
																					BgL_typez00) =
																				((BgL_typez00_bglt) BgL_auxz00_6212),
																				BUNSPEC);
																		}
																		((((BgL_nodezf2effectzf2_bglt) COBJECT(
																						((BgL_nodezf2effectzf2_bglt)
																							BgL_new1172z00_2339)))->
																				BgL_sidezd2effectzd2) =
																			((obj_t) BUNSPEC), BUNSPEC);
																		((((BgL_nodezf2effectzf2_bglt)
																					COBJECT(((BgL_nodezf2effectzf2_bglt)
																							BgL_new1172z00_2339)))->
																				BgL_keyz00) =
																			((obj_t) BINT(-1L)), BUNSPEC);
																		((((BgL_externz00_bglt)
																					COBJECT(((BgL_externz00_bglt)
																							BgL_new1172z00_2339)))->
																				BgL_exprza2za2) =
																			((obj_t) BgL_argsz00_38), BUNSPEC);
																		((((BgL_externz00_bglt)
																					COBJECT(((BgL_externz00_bglt)
																							BgL_new1172z00_2339)))->
																				BgL_effectz00) =
																			((obj_t) BUNSPEC), BUNSPEC);
																		((((BgL_privatez00_bglt)
																					COBJECT(((BgL_privatez00_bglt)
																							BgL_new1172z00_2339)))->
																				BgL_czd2formatzd2) =
																			((obj_t) string_append(BgL_gnamez00_2272,
																					BGl_string2347z00zzast_appz00)),
																			BUNSPEC);
																		{
																			BgL_typez00_bglt BgL_auxz00_6232;

																			{	/* Ast/app.scm 587 */
																				obj_t BgL_pairz00_3780;

																				BgL_pairz00_3780 =
																					(((BgL_cfunz00_bglt) COBJECT(
																							((BgL_cfunz00_bglt)
																								(((BgL_variablez00_bglt)
																										COBJECT((
																												(BgL_variablez00_bglt) (
																													(BgL_globalz00_bglt)
																													BgL_variablez00_2271))))->
																									BgL_valuez00))))->
																					BgL_argszd2typezd2);
																				{	/* Ast/app.scm 587 */
																					obj_t BgL_pairz00_3785;

																					{	/* Ast/app.scm 587 */
																						obj_t BgL_pairz00_3784;

																						BgL_pairz00_3784 =
																							CDR(BgL_pairz00_3780);
																						BgL_pairz00_3785 =
																							CDR(BgL_pairz00_3784);
																					}
																					BgL_auxz00_6232 =
																						((BgL_typez00_bglt)
																						CAR(BgL_pairz00_3785));
																			}}
																			((((BgL_vsetz12z12_bglt)
																						COBJECT(BgL_new1172z00_2339))->
																					BgL_ftypez00) =
																				((BgL_typez00_bglt) BgL_auxz00_6232),
																				BUNSPEC);
																		}
																		{
																			BgL_typez00_bglt BgL_auxz00_6243;

																			{	/* Ast/app.scm 586 */
																				obj_t BgL_pairz00_3788;

																				BgL_pairz00_3788 =
																					(((BgL_cfunz00_bglt) COBJECT(
																							((BgL_cfunz00_bglt)
																								(((BgL_variablez00_bglt)
																										COBJECT((
																												(BgL_variablez00_bglt) (
																													(BgL_globalz00_bglt)
																													BgL_variablez00_2271))))->
																									BgL_valuez00))))->
																					BgL_argszd2typezd2);
																				{	/* Ast/app.scm 586 */
																					obj_t BgL_pairz00_3791;

																					BgL_pairz00_3791 =
																						CDR(BgL_pairz00_3788);
																					BgL_auxz00_6243 =
																						((BgL_typez00_bglt)
																						CAR(BgL_pairz00_3791));
																			}}
																			((((BgL_vsetz12z12_bglt)
																						COBJECT(BgL_new1172z00_2339))->
																					BgL_otypez00) =
																				((BgL_typez00_bglt) BgL_auxz00_6243),
																				BUNSPEC);
																		}
																		{
																			BgL_typez00_bglt BgL_auxz00_6253;

																			{	/* Ast/app.scm 585 */
																				obj_t BgL_pairz00_3794;

																				BgL_pairz00_3794 =
																					(((BgL_cfunz00_bglt) COBJECT(
																							((BgL_cfunz00_bglt)
																								(((BgL_variablez00_bglt)
																										COBJECT((
																												(BgL_variablez00_bglt) (
																													(BgL_globalz00_bglt)
																													BgL_variablez00_2271))))->
																									BgL_valuez00))))->
																					BgL_argszd2typezd2);
																				BgL_auxz00_6253 =
																					((BgL_typez00_bglt)
																					CAR(BgL_pairz00_3794));
																			}
																			((((BgL_vsetz12z12_bglt)
																						COBJECT(BgL_new1172z00_2339))->
																					BgL_vtypez00) =
																				((BgL_typez00_bglt) BgL_auxz00_6253),
																				BUNSPEC);
																		}
																		((((BgL_vsetz12z12_bglt)
																					COBJECT(BgL_new1172z00_2339))->
																				BgL_unsafez00) =
																			((bool_t) ((((BgL_variablez00_bglt)
																							COBJECT(((BgL_variablez00_bglt) (
																										(BgL_globalz00_bglt)
																										BgL_variablez00_2271))))->
																						BgL_idz00) == CNST_TABLE_REF(20))),
																			BUNSPEC);
																		return ((obj_t) BgL_new1172z00_2339);
																	}
																}
															else
																{	/* Ast/app.scm 531 */
																	if (
																		(BgL_casezd2valuezd2_2273 ==
																			CNST_TABLE_REF(21)))
																		{	/* Ast/app.scm 589 */
																			obj_t BgL_stackzd2alloczd2_2351;

																			BgL_stackzd2alloczd2_2351 =
																				(((BgL_funz00_bglt) COBJECT(
																						((BgL_funz00_bglt)
																							(((BgL_variablez00_bglt)
																									COBJECT
																									(BgL_variablez00_2271))->
																								BgL_valuez00))))->
																				BgL_stackzd2allocatorzd2);
																			{	/* Ast/app.scm 589 */
																				obj_t BgL_heapzd2formatzd2_2352;

																				BgL_heapzd2formatzd2_2352 =
																					string_append(BgL_gnamez00_2272,
																					BGl_string2344z00zzast_appz00);
																				{	/* Ast/app.scm 590 */
																					obj_t BgL_stackzd2formatzd2_2353;

																					{	/* Ast/app.scm 591 */
																						bool_t BgL_test2567z00_6276;

																						{	/* Ast/app.scm 591 */
																							obj_t BgL_classz00_3799;

																							BgL_classz00_3799 =
																								BGl_globalz00zzast_varz00;
																							if (BGL_OBJECTP
																								(BgL_stackzd2alloczd2_2351))
																								{	/* Ast/app.scm 591 */
																									BgL_objectz00_bglt
																										BgL_arg1807z00_3801;
																									BgL_arg1807z00_3801 =
																										(BgL_objectz00_bglt)
																										(BgL_stackzd2alloczd2_2351);
																									if (BGL_CONDEXPAND_ISA_ARCH64
																										())
																										{	/* Ast/app.scm 591 */
																											long BgL_idxz00_3807;

																											BgL_idxz00_3807 =
																												BGL_OBJECT_INHERITANCE_NUM
																												(BgL_arg1807z00_3801);
																											BgL_test2567z00_6276 =
																												(VECTOR_REF
																												(BGl_za2inheritancesza2z00zz__objectz00,
																													(BgL_idxz00_3807 +
																														2L)) ==
																												BgL_classz00_3799);
																										}
																									else
																										{	/* Ast/app.scm 591 */
																											bool_t
																												BgL_res2324z00_3832;
																											{	/* Ast/app.scm 591 */
																												obj_t
																													BgL_oclassz00_3815;
																												{	/* Ast/app.scm 591 */
																													obj_t
																														BgL_arg1815z00_3823;
																													long
																														BgL_arg1816z00_3824;
																													BgL_arg1815z00_3823 =
																														(BGl_za2classesza2z00zz__objectz00);
																													{	/* Ast/app.scm 591 */
																														long
																															BgL_arg1817z00_3825;
																														BgL_arg1817z00_3825
																															=
																															BGL_OBJECT_CLASS_NUM
																															(BgL_arg1807z00_3801);
																														BgL_arg1816z00_3824
																															=
																															(BgL_arg1817z00_3825
																															- OBJECT_TYPE);
																													}
																													BgL_oclassz00_3815 =
																														VECTOR_REF
																														(BgL_arg1815z00_3823,
																														BgL_arg1816z00_3824);
																												}
																												{	/* Ast/app.scm 591 */
																													bool_t
																														BgL__ortest_1115z00_3816;
																													BgL__ortest_1115z00_3816
																														=
																														(BgL_classz00_3799
																														==
																														BgL_oclassz00_3815);
																													if (BgL__ortest_1115z00_3816)
																														{	/* Ast/app.scm 591 */
																															BgL_res2324z00_3832
																																=
																																BgL__ortest_1115z00_3816;
																														}
																													else
																														{	/* Ast/app.scm 591 */
																															long
																																BgL_odepthz00_3817;
																															{	/* Ast/app.scm 591 */
																																obj_t
																																	BgL_arg1804z00_3818;
																																BgL_arg1804z00_3818
																																	=
																																	(BgL_oclassz00_3815);
																																BgL_odepthz00_3817
																																	=
																																	BGL_CLASS_DEPTH
																																	(BgL_arg1804z00_3818);
																															}
																															if (
																																(2L <
																																	BgL_odepthz00_3817))
																																{	/* Ast/app.scm 591 */
																																	obj_t
																																		BgL_arg1802z00_3820;
																																	{	/* Ast/app.scm 591 */
																																		obj_t
																																			BgL_arg1803z00_3821;
																																		BgL_arg1803z00_3821
																																			=
																																			(BgL_oclassz00_3815);
																																		BgL_arg1802z00_3820
																																			=
																																			BGL_CLASS_ANCESTORS_REF
																																			(BgL_arg1803z00_3821,
																																			2L);
																																	}
																																	BgL_res2324z00_3832
																																		=
																																		(BgL_arg1802z00_3820
																																		==
																																		BgL_classz00_3799);
																																}
																															else
																																{	/* Ast/app.scm 591 */
																																	BgL_res2324z00_3832
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																												}
																											}
																											BgL_test2567z00_6276 =
																												BgL_res2324z00_3832;
																										}
																								}
																							else
																								{	/* Ast/app.scm 591 */
																									BgL_test2567z00_6276 =
																										((bool_t) 0);
																								}
																						}
																						if (BgL_test2567z00_6276)
																							{	/* Ast/app.scm 592 */
																								obj_t BgL_arg2228z00_2361;

																								BgL_arg2228z00_2361 =
																									(((BgL_variablez00_bglt)
																										COBJECT((
																												(BgL_variablez00_bglt) (
																													(BgL_globalz00_bglt)
																													BgL_stackzd2alloczd2_2351))))->
																									BgL_namez00);
																								BgL_stackzd2formatzd2_2353 =
																									string_append
																									(BgL_arg2228z00_2361,
																									BGl_string2344z00zzast_appz00);
																							}
																						else
																							{	/* Ast/app.scm 591 */
																								BgL_stackzd2formatzd2_2353 =
																									BgL_heapzd2formatzd2_2352;
																							}
																					}
																					{	/* Ast/app.scm 591 */

																						{	/* Ast/app.scm 595 */
																							BgL_vallocz00_bglt
																								BgL_new1174z00_2354;
																							{	/* Ast/app.scm 596 */
																								BgL_vallocz00_bglt
																									BgL_new1173z00_2358;
																								BgL_new1173z00_2358 =
																									((BgL_vallocz00_bglt)
																									BOBJECT(GC_MALLOC(sizeof
																											(struct
																												BgL_vallocz00_bgl))));
																								{	/* Ast/app.scm 596 */
																									long BgL_arg2226z00_2359;

																									BgL_arg2226z00_2359 =
																										BGL_CLASS_NUM
																										(BGl_vallocz00zzast_nodez00);
																									BGL_OBJECT_CLASS_NUM_SET((
																											(BgL_objectz00_bglt)
																											BgL_new1173z00_2358),
																										BgL_arg2226z00_2359);
																								}
																								{	/* Ast/app.scm 596 */
																									BgL_objectz00_bglt
																										BgL_tmpz00_6307;
																									BgL_tmpz00_6307 =
																										((BgL_objectz00_bglt)
																										BgL_new1173z00_2358);
																									BGL_OBJECT_WIDENING_SET
																										(BgL_tmpz00_6307, BFALSE);
																								}
																								((BgL_objectz00_bglt)
																									BgL_new1173z00_2358);
																								BgL_new1174z00_2354 =
																									BgL_new1173z00_2358;
																							}
																							((((BgL_nodez00_bglt) COBJECT(
																											((BgL_nodez00_bglt)
																												BgL_new1174z00_2354)))->
																									BgL_locz00) =
																								((obj_t) BgL_locz00_36),
																								BUNSPEC);
																							{
																								BgL_typez00_bglt
																									BgL_auxz00_6313;
																								{	/* Ast/app.scm 597 */
																									BgL_typez00_bglt
																										BgL_arg2223z00_2355;
																									BgL_arg2223z00_2355 =
																										(((BgL_variablez00_bglt)
																											COBJECT((
																													(BgL_variablez00_bglt)
																													((BgL_globalz00_bglt)
																														BgL_variablez00_2271))))->
																										BgL_typez00);
																									BgL_auxz00_6313 =
																										BGl_strictzd2nodezd2typez00zzast_nodez00
																										(((BgL_typez00_bglt)
																											BGl_za2_za2z00zztype_cachez00),
																										BgL_arg2223z00_2355);
																								}
																								((((BgL_nodez00_bglt) COBJECT(
																												((BgL_nodez00_bglt)
																													BgL_new1174z00_2354)))->
																										BgL_typez00) =
																									((BgL_typez00_bglt)
																										BgL_auxz00_6313), BUNSPEC);
																							}
																							((((BgL_nodezf2effectzf2_bglt)
																										COBJECT((
																												(BgL_nodezf2effectzf2_bglt)
																												BgL_new1174z00_2354)))->
																									BgL_sidezd2effectzd2) =
																								((obj_t) BUNSPEC), BUNSPEC);
																							((((BgL_nodezf2effectzf2_bglt)
																										COBJECT((
																												(BgL_nodezf2effectzf2_bglt)
																												BgL_new1174z00_2354)))->
																									BgL_keyz00) =
																								((obj_t) BINT(-1L)), BUNSPEC);
																							((((BgL_externz00_bglt)
																										COBJECT((
																												(BgL_externz00_bglt)
																												BgL_new1174z00_2354)))->
																									BgL_exprza2za2) =
																								((obj_t) BgL_argsz00_38),
																								BUNSPEC);
																							((((BgL_externz00_bglt)
																										COBJECT((
																												(BgL_externz00_bglt)
																												BgL_new1174z00_2354)))->
																									BgL_effectz00) =
																								((obj_t) BUNSPEC), BUNSPEC);
																							((((BgL_privatez00_bglt)
																										COBJECT((
																												(BgL_privatez00_bglt)
																												BgL_new1174z00_2354)))->
																									BgL_czd2formatzd2) =
																								((obj_t)
																									BgL_heapzd2formatzd2_2352),
																								BUNSPEC);
																							((((BgL_vallocz00_bglt)
																										COBJECT
																										(BgL_new1174z00_2354))->
																									BgL_ftypez00) =
																								((BgL_typez00_bglt)
																									BGl_strictzd2nodezd2typez00zzast_nodez00
																									(((BgL_typez00_bglt)
																											BGl_za2_za2z00zztype_cachez00),
																										((BgL_typez00_bglt)
																											BGl_za2objza2z00zztype_cachez00))),
																								BUNSPEC);
																							{
																								BgL_typez00_bglt
																									BgL_auxz00_6336;
																								{	/* Ast/app.scm 600 */
																									obj_t BgL_pairz00_3841;

																									BgL_pairz00_3841 =
																										(((BgL_cfunz00_bglt)
																											COBJECT((
																													(BgL_cfunz00_bglt) ((
																															(BgL_variablez00_bglt)
																															COBJECT((
																																	(BgL_variablez00_bglt)
																																	((BgL_globalz00_bglt) BgL_variablez00_2271))))->BgL_valuez00))))->BgL_argszd2typezd2);
																									BgL_auxz00_6336 =
																										((BgL_typez00_bglt)
																										CAR(BgL_pairz00_3841));
																								}
																								((((BgL_vallocz00_bglt)
																											COBJECT
																											(BgL_new1174z00_2354))->
																										BgL_otypez00) =
																									((BgL_typez00_bglt)
																										BgL_auxz00_6336), BUNSPEC);
																							}
																							return
																								((obj_t) BgL_new1174z00_2354);
																						}
																					}
																				}
																			}
																		}
																	else
																		{	/* Ast/app.scm 603 */
																			obj_t BgL_arg2230z00_2363;

																			BgL_arg2230z00_2363 =
																				BGl_shapez00zztools_shapez00(
																				((obj_t) BgL_varz00_37));
																			return
																				BGl_errorz00zz__errorz00
																				(BGl_string2348z00zzast_appz00,
																				BGl_string2349z00zzast_appz00,
																				BgL_arg2230z00_2363);
																		}
																}
														}
												}
										}
								}
						}
					}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_appz00(void)
	{
		{	/* Ast/app.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_appz00(void)
	{
		{	/* Ast/app.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_appz00(void)
	{
		{	/* Ast/app.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_appz00(void)
	{
		{	/* Ast/app.scm 14 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2350z00zzast_appz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2350z00zzast_appz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2350z00zzast_appz00));
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2350z00zzast_appz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2350z00zzast_appz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string2350z00zzast_appz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2350z00zzast_appz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2350z00zzast_appz00));
			BGl_modulezd2initializa7ationz75zztvector_tvectorz00(501518119L,
				BSTRING_TO_STRING(BGl_string2350z00zzast_appz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string2350z00zzast_appz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string2350z00zzast_appz00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string2350z00zzast_appz00));
			BGl_modulezd2initializa7ationz75zzast_letz00(469204197L,
				BSTRING_TO_STRING(BGl_string2350z00zzast_appz00));
			BGl_modulezd2initializa7ationz75zzast_privatez00(135263837L,
				BSTRING_TO_STRING(BGl_string2350z00zzast_appz00));
			BGl_modulezd2initializa7ationz75zztools_dssslz00(275867955L,
				BSTRING_TO_STRING(BGl_string2350z00zzast_appz00));
			BGl_modulezd2initializa7ationz75zzexpand_epsz00(359337187L,
				BSTRING_TO_STRING(BGl_string2350z00zzast_appz00));
			return
				BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string2350z00zzast_appz00));
		}

	}

#ifdef __cplusplus
}
#endif
