/*===========================================================================*/
/*   (Ast/unit.scm)                                                          */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/unit.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_AST_UNIT_TYPE_DEFINITIONS
#define BGL_AST_UNIT_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_tclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		struct BgL_globalz00_bgl *BgL_holderz00;
		obj_t BgL_wideningz00;
		long BgL_depthz00;
		bool_t BgL_finalzf3zf3;
		obj_t BgL_constructorz00;
		obj_t BgL_virtualzd2slotszd2numberz00;
		bool_t BgL_abstractzf3zf3;
		obj_t BgL_widezd2typezd2;
		obj_t BgL_subclassesz00;
	}                *BgL_tclassz00_bglt;


#endif													// BGL_AST_UNIT_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_normaliza7ezd2prognz75zztools_prognz00(obj_t);
	extern BgL_globalz00_bglt
		BGl_defzd2globalzd2sfunzd2nozd2warningz12z12zzast_glozd2defzd2(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_makezd2methodzd2nozd2dssslzd2bodyz00zzobject_methodz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_za2unsafezd2arityza2zd2zzengine_paramz00;
	extern BgL_globalz00_bglt
		BGl_defzd2globalzd2svarz12z12zzast_glozd2defzd2(obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	extern bool_t BGl_typezd2existszf3z21zztype_envz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzast_unitz00 = BUNSPEC;
	extern obj_t BGl_dssslzd2keyszd2zztools_dssslz00(obj_t);
	extern obj_t BGl_makezd2typedzd2identz00zzast_identz00(obj_t, obj_t);
	extern obj_t BGl_userzd2errorzf2locationz20zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_za2vectorza2z00zztype_cachez00;
	extern obj_t BGl_funz00zzast_varz00;
	extern obj_t BGl_dssslzd2beforezd2dssslz00zztools_dssslz00(obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_tclassz00zzobject_classz00;
	BGL_IMPORT obj_t BGl_warningz00zz__errorz00(obj_t);
	extern obj_t BGl_fastzd2idzd2ofzd2idzd2zzast_identz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_unitzd2initializa7ersz75zzast_unitz00(void);
	extern obj_t BGl_getzd2toplevelzd2unitz00zzmodule_includez00(void);
	static obj_t BGl_dozd2etazd2expanseze70ze7zzast_unitz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_toplevelzd2initzd2zzast_unitz00(void);
	extern obj_t BGl_makezd2nzd2protoz00zztools_argsz00(obj_t);
	static obj_t BGl_makezd2sfunzd2definitionz00zzast_unitz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_makezd2methodzd2bodyz00zzast_unitz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_makezd2genericzd2definitionz00zzast_unitz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzast_unitz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzast_unitz00(void);
	extern obj_t BGl_markzd2methodz12zc0zzobject_methodz00(obj_t);
	extern obj_t BGl_comptimezd2expandzd2zzexpand_epsz00(obj_t);
	static obj_t BGl_typedzd2argsze70z35zzast_unitz00(obj_t, BgL_globalz00_bglt);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	BGL_IMPORT obj_t BGl_stringzd2containszd2zz__r4_strings_6_7z00(obj_t, obj_t,
		int);
	BGL_IMPORT obj_t BGl_dropz00zz__r4_pairs_and_lists_6_3z00(obj_t, long);
	extern obj_t BGl_userzd2warningzd2zztools_errorz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_bignum_add(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_declarezd2unitz12zc0zzast_unitz00(obj_t, long);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	static BgL_globalz00_bglt
		BGl_makezd2sfunzd2keyzd2closurezd2zzast_unitz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzast_unitz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzast_unitz00(void);
	extern obj_t BGl_makezd2genericzd2bodyz00zzobject_genericz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_za2unitzd2listza2zd2zzast_unitz00 = BUNSPEC;
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	static BgL_globalz00_bglt
		BGl_makezd2sfunzd2optzd2closurezd2zzast_unitz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_epairifyzd2propagatezd2locz00zztools_miscz00(obj_t, obj_t);
	static obj_t BGl_zc3z04anonymousza33092ze3ze70z60zzast_unitz00(obj_t, obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_long_to_bignum(long);
	extern obj_t BGl_makezd2methodzd2dssslzd2bodyzd2zzobject_methodz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern bool_t BGl_globalzd2optionalzf3z21zzast_varz00(obj_t);
	extern obj_t BGl_idzd2ofzd2idz00zzast_identz00(obj_t, obj_t);
	static obj_t BGl_z62unitzd2initializa7erzd2idzc5zzast_unitz00(obj_t, obj_t);
	static obj_t BGl_z62unitzd2sexpza2zd2addzd2headz12z00zzast_unitz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2procedureza2z00zztype_cachez00;
	static bool_t BGl_lambdazf3zf3zzast_unitz00(obj_t);
	static obj_t BGl_z62unitzd2initializa7ersz17zzast_unitz00(obj_t);
	extern bool_t
		BGl_dssslzd2optionalzd2onlyzd2prototypezf3z21zztools_dssslz00(obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	BGL_IMPORT obj_t bgl_append2(obj_t, obj_t);
	extern obj_t BGl_getzd2methodzd2unitz00zzmodule_classz00(void);
	static obj_t BGl_makezd2svarzd2definitionz00zzast_unitz00(obj_t, obj_t);
	static obj_t BGl_normaliza7ezd2prognzf2errorz87zzast_unitz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_za2modulezd2clauseza2zd2zzmodule_modulez00;
	BGL_EXPORTED_DECL obj_t BGl_unitzd2ze3defsz31zzast_unitz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_unitzd2sexpza2zd2addzd2headz12z62zzast_unitz00(obj_t, obj_t);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	extern obj_t BGl_replacez12z12zztools_miscz00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2ze3astz31zzast_unitz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_takez00zz__r4_pairs_and_lists_6_3z00(obj_t, long);
	extern bool_t BGl_globalzd2keyzf3z21zzast_varz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31614ze3ze5zzast_unitz00(obj_t, obj_t);
	static obj_t BGl_etazd2expansezd2zzast_unitz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzast_unitz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_epsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_impusez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_includez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_dssslz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_argsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_methodz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_genericz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_letz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_glozd2defzd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_findzd2gdefszd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__dssslz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_unitzd2sexpza2zd2addz12zb0zzast_unitz00(obj_t,
		obj_t);
	static obj_t BGl_z62unitzd2ze3defsz53zzast_unitz00(obj_t, obj_t);
	static obj_t BGl_z62unitzd2sexpza2zd2addz12zd2zzast_unitz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_unitzd2initzd2callsz00zzast_unitz00(void);
	extern BgL_typez00_bglt BGl_typezd2ofzd2idz00zzast_identz00(obj_t, obj_t);
	static obj_t BGl_z62unitzd2initzd2callsz62zzast_unitz00(obj_t);
	BGL_IMPORT obj_t BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00(obj_t);
	BGL_IMPORT obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		long);
	static obj_t BGl_makezd2sfunzd2keyzd2definitionzd2zzast_unitz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	extern BgL_globalz00_bglt
		BGl_defzd2globalzd2sfunz12z12zzast_glozd2defzd2(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_makezd2dssslzd2functionzd2preludezd2zz__dssslz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_toplevelza2zd2ze3astz93zzast_unitz00(obj_t, obj_t);
	extern obj_t BGl_checkzd2idzd2zzast_identz00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzast_unitz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_unitz00(void);
	extern BgL_localz00_bglt BGl_makezd2localzd2svarz00zzast_localz00(obj_t,
		BgL_typez00_bglt);
	BGL_IMPORT long bgl_list_length(obj_t);
	extern long BGl_globalzd2arityzd2zztools_argsz00(obj_t);
	extern BgL_nodez00_bglt BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00(obj_t,
		obj_t, obj_t);
	extern BgL_localz00_bglt
		BGl_makezd2userzd2localzd2svarzd2zzast_localz00(obj_t, BgL_typez00_bglt);
	static obj_t BGl_importedzd2moduleszd2initz00zzast_unitz00(void);
	BGL_IMPORT obj_t BGl_iotaz00zz__r4_pairs_and_lists_6_3z00(int, obj_t);
	extern obj_t BGl_dssslzd2optionalszd2zztools_dssslz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzast_unitz00(void);
	extern obj_t BGl_parsezd2idzd2zzast_identz00(obj_t, obj_t);
	static obj_t BGl_parsezd2funzd2optzd2argszd2zzast_unitz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_unitzd2initializa7erzd2idza7zzast_unitz00(obj_t);
	extern obj_t BGl_findzd2globalzf2modulez20zzast_envz00(obj_t, obj_t);
	static obj_t BGl_makezd2methodzd2definitionz00zzast_unitz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_letzd2symzd2zzast_letz00(void);
	extern obj_t BGl_getzd2genericzd2unitz00zzmodule_classz00(void);
	extern obj_t BGl_za2unsafezd2evalza2zd2zzengine_paramz00;
	static obj_t BGl_makezd2genericzd2keyzd2definitionzd2zzast_unitz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_dssslzd2findzd2firstzd2formalzd2zztools_dssslz00(obj_t);
	static obj_t BGl_makezd2genericzd2nooptzd2definitionzd2zzast_unitz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern bool_t BGl_userzd2symbolzf3z21zzast_identz00(obj_t);
	extern BgL_typez00_bglt BGl_findzd2typezd2zztype_envz00(obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	extern obj_t BGl_argsza2zd2ze3argszd2listz41zztools_argsz00(obj_t);
	static obj_t BGl_makezd2genericzd2optzd2definitionzd2zzast_unitz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_errorzd2classzd2shadowz00zzast_unitz00(obj_t, obj_t);
	static obj_t BGl_loopze70ze7zzast_unitz00(obj_t, obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	extern bool_t BGl_checkzd2methodzd2definitionz00zzast_glozd2defzd2(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t
		BGl_initializa7ezd2importedzd2modulesza7zzmodule_impusez00(obj_t);
	BGL_IMPORT obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_parsezd2funzd2argsz00zzast_unitz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_stringzd2downcasezd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_unitzd2requirezd2initzd2idzd2zzast_unitz00(obj_t);
	extern bool_t BGl_dssslzd2prototypezf3z21zztools_dssslz00(obj_t);
	extern obj_t BGl_findzd2globalzd2defsz00zzast_findzd2gdefszd2(obj_t);
	BGL_IMPORT bool_t BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(obj_t);
	extern obj_t BGl_compilezd2expandzd2zzexpand_epsz00(obj_t);
	static obj_t __cnst[57];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_unitzd2initializa7erzd2idzd2envz75zzast_unitz00,
		BgL_bgl_za762unitza7d2initia3582z00,
		BGl_z62unitzd2initializa7erzd2idzc5zzast_unitz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_unitzd2ze3defszd2envze3zzast_unitz00,
		BgL_bgl_za762unitza7d2za7e3def3583za7,
		BGl_z62unitzd2ze3defsz53zzast_unitz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_unitzd2sexpza2zd2addzd2headz12zd2envzb0zzast_unitz00,
		BgL_bgl_za762unitza7d2sexpza7a3584za7,
		BGl_z62unitzd2sexpza2zd2addzd2headz12z00zzast_unitz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_unitzd2initzd2callszd2envzd2zzast_unitz00,
		BgL_bgl_za762unitza7d2initza7d3585za7,
		BGl_z62unitzd2initzd2callsz62zzast_unitz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_unitzd2sexpza2zd2addz12zd2envz62zzast_unitz00,
		BgL_bgl_za762unitza7d2sexpza7a3586za7,
		BGl_z62unitzd2sexpza2zd2addz12zd2zzast_unitz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3562z00zzast_unitz00,
		BgL_bgl_string3562za700za7za7a3587za7, "]", 1);
	      DEFINE_STRING(BGl_string3563z00zzast_unitz00,
		BgL_bgl_string3563za700za7za7a3588za7, "      [", 7);
	      DEFINE_STRING(BGl_string3564z00zzast_unitz00,
		BgL_bgl_string3564za700za7za7a3589za7, "Illegal class shadowing", 23);
	      DEFINE_STRING(BGl_string3565z00zzast_unitz00,
		BgL_bgl_string3565za700za7za7a3590za7,
		"Unable to eta-expand #!optional alias", 37);
	      DEFINE_STRING(BGl_string3566z00zzast_unitz00,
		BgL_bgl_string3566za700za7za7a3591za7, "Unable to eta-expand #!key alias",
		32);
	      DEFINE_STRING(BGl_string3567z00zzast_unitz00,
		BgL_bgl_string3567za700za7za7a3592za7, "form no longer supported", 24);
	      DEFINE_STRING(BGl_string3568z00zzast_unitz00,
		BgL_bgl_string3568za700za7za7a3593za7, "define-generic", 14);
	      DEFINE_STRING(BGl_string3569z00zzast_unitz00,
		BgL_bgl_string3569za700za7za7a3594za7, "Illegal '() expression", 22);
	      DEFINE_STRING(BGl_string3570z00zzast_unitz00,
		BgL_bgl_string3570za700za7za7a3595za7, "] expected, provided", 20);
	      DEFINE_STRING(BGl_string3571z00zzast_unitz00,
		BgL_bgl_string3571za700za7za7a3596za7, "..", 2);
	      DEFINE_STRING(BGl_string3572z00zzast_unitz00,
		BgL_bgl_string3572za700za7za7a3597za7, "wrong number of arguments: [", 28);
	      DEFINE_STRING(BGl_string3573z00zzast_unitz00,
		BgL_bgl_string3573za700za7za7a3598za7, "Illegal keyword argument", 24);
	      DEFINE_STRING(BGl_string3574z00zzast_unitz00,
		BgL_bgl_string3574za700za7za7a3599za7,
		"generics can only use on DSSSL keyword", 38);
	      DEFINE_STRING(BGl_string3575z00zzast_unitz00,
		BgL_bgl_string3575za700za7za7a3600za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string3576z00zzast_unitz00,
		BgL_bgl_string3576za700za7za7a3601za7, "Bad generic formal argument", 27);
	      DEFINE_STRING(BGl_string3577z00zzast_unitz00,
		BgL_bgl_string3577za700za7za7a3602za7, "::", 2);
	      DEFINE_STRING(BGl_string3578z00zzast_unitz00,
		BgL_bgl_string3578za700za7za7a3603za7, "Bad method formal argument", 26);
	      DEFINE_STRING(BGl_string3579z00zzast_unitz00,
		BgL_bgl_string3579za700za7za7a3604za7, "ast_unit", 8);
	      DEFINE_STRING(BGl_string3580z00zzast_unitz00,
		BgL_bgl_string3580za700za7za7a3605za7,
		"unit register-generic! sgfun when >=fx index memq vector-ref let -fx +fx eq? v =fx k1 i vector-length var check search l globalization env opt _ case else quote error __error let* $vector-length $vector-ref-ur cons* apply define-method define-generic define-inline labels define sifun __object register-class! sfun read lambda -require-initialization -init now export cgen snifun obj if begin set! @ ",
		400);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_unitzd2initializa7erszd2envza7zzast_unitz00,
		BgL_bgl_za762unitza7d2initia3606z00,
		BGl_z62unitzd2initializa7ersz17zzast_unitz00, 0L, BUNSPEC, 0);
	extern obj_t BGl_userzd2errorzd2envz00zztools_errorz00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzast_unitz00));
		     ADD_ROOT((void *) (&BGl_za2unitzd2listza2zd2zzast_unitz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzast_unitz00(long
		BgL_checksumz00_5814, char *BgL_fromz00_5815)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_unitz00))
				{
					BGl_requirezd2initializa7ationz75zzast_unitz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_unitz00();
					BGl_libraryzd2moduleszd2initz00zzast_unitz00();
					BGl_cnstzd2initzd2zzast_unitz00();
					BGl_importedzd2moduleszd2initz00zzast_unitz00();
					return BGl_toplevelzd2initzd2zzast_unitz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_unitz00(void)
	{
		{	/* Ast/unit.scm 21 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_unit");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "ast_unit");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "ast_unit");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "ast_unit");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_unit");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "ast_unit");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "ast_unit");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"ast_unit");
			BGl_modulezd2initializa7ationz75zz__dssslz00(0L, "ast_unit");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L, "ast_unit");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "ast_unit");
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(0L,
				"ast_unit");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "ast_unit");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "ast_unit");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_unitz00(void)
	{
		{	/* Ast/unit.scm 21 */
			{	/* Ast/unit.scm 21 */
				obj_t BgL_cportz00_5803;

				{	/* Ast/unit.scm 21 */
					obj_t BgL_stringz00_5810;

					BgL_stringz00_5810 = BGl_string3580z00zzast_unitz00;
					{	/* Ast/unit.scm 21 */
						obj_t BgL_startz00_5811;

						BgL_startz00_5811 = BINT(0L);
						{	/* Ast/unit.scm 21 */
							obj_t BgL_endz00_5812;

							BgL_endz00_5812 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_5810)));
							{	/* Ast/unit.scm 21 */

								BgL_cportz00_5803 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_5810, BgL_startz00_5811, BgL_endz00_5812);
				}}}}
				{
					long BgL_iz00_5804;

					BgL_iz00_5804 = 56L;
				BgL_loopz00_5805:
					if ((BgL_iz00_5804 == -1L))
						{	/* Ast/unit.scm 21 */
							return BUNSPEC;
						}
					else
						{	/* Ast/unit.scm 21 */
							{	/* Ast/unit.scm 21 */
								obj_t BgL_arg3581z00_5806;

								{	/* Ast/unit.scm 21 */

									{	/* Ast/unit.scm 21 */
										obj_t BgL_locationz00_5808;

										BgL_locationz00_5808 = BBOOL(((bool_t) 0));
										{	/* Ast/unit.scm 21 */

											BgL_arg3581z00_5806 =
												BGl_readz00zz__readerz00(BgL_cportz00_5803,
												BgL_locationz00_5808);
										}
									}
								}
								{	/* Ast/unit.scm 21 */
									int BgL_tmpz00_5847;

									BgL_tmpz00_5847 = (int) (BgL_iz00_5804);
									CNST_TABLE_SET(BgL_tmpz00_5847, BgL_arg3581z00_5806);
							}}
							{	/* Ast/unit.scm 21 */
								int BgL_auxz00_5809;

								BgL_auxz00_5809 = (int) ((BgL_iz00_5804 - 1L));
								{
									long BgL_iz00_5852;

									BgL_iz00_5852 = (long) (BgL_auxz00_5809);
									BgL_iz00_5804 = BgL_iz00_5852;
									goto BgL_loopz00_5805;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_unitz00(void)
	{
		{	/* Ast/unit.scm 21 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzast_unitz00(void)
	{
		{	/* Ast/unit.scm 21 */
			return (BGl_za2unitzd2listza2zd2zzast_unitz00 = BNIL, BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzast_unitz00(obj_t BgL_l1z00_1, obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1717;

				BgL_headz00_1717 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1718;
					obj_t BgL_tailz00_1719;

					BgL_prevz00_1718 = BgL_headz00_1717;
					BgL_tailz00_1719 = BgL_l1z00_1;
				BgL_loopz00_1720:
					if (PAIRP(BgL_tailz00_1719))
						{
							obj_t BgL_newzd2prevzd2_1722;

							BgL_newzd2prevzd2_1722 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1719), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1718, BgL_newzd2prevzd2_1722);
							{
								obj_t BgL_tailz00_5862;
								obj_t BgL_prevz00_5861;

								BgL_prevz00_5861 = BgL_newzd2prevzd2_1722;
								BgL_tailz00_5862 = CDR(BgL_tailz00_1719);
								BgL_tailz00_1719 = BgL_tailz00_5862;
								BgL_prevz00_1718 = BgL_prevz00_5861;
								goto BgL_loopz00_1720;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1717);
				}
			}
		}

	}



/* unit-sexp*-add! */
	BGL_EXPORTED_DEF obj_t BGl_unitzd2sexpza2zd2addz12zb0zzast_unitz00(obj_t
		BgL_unitz00_25, obj_t BgL_sexpz00_26)
	{
		{	/* Ast/unit.scm 61 */
			if (NULLP(STRUCT_REF(((obj_t) BgL_unitz00_25), (int) (2L))))
				{	/* Ast/unit.scm 63 */
					int BgL_auxz00_5872;
					obj_t BgL_tmpz00_5870;

					BgL_auxz00_5872 = (int) (2L);
					BgL_tmpz00_5870 = ((obj_t) BgL_unitz00_25);
					return STRUCT_SET(BgL_tmpz00_5870, BgL_auxz00_5872, BgL_sexpz00_26);
				}
			else
				{	/* Ast/unit.scm 64 */
					obj_t BgL_arg1448z00_1741;

					BgL_arg1448z00_1741 =
						BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(STRUCT_REF(
							((obj_t) BgL_unitz00_25), (int) (2L)));
					return SET_CDR(BgL_arg1448z00_1741, BgL_sexpz00_26);
				}
		}

	}



/* &unit-sexp*-add! */
	obj_t BGl_z62unitzd2sexpza2zd2addz12zd2zzast_unitz00(obj_t BgL_envz00_5778,
		obj_t BgL_unitz00_5779, obj_t BgL_sexpz00_5780)
	{
		{	/* Ast/unit.scm 61 */
			return
				BGl_unitzd2sexpza2zd2addz12zb0zzast_unitz00(BgL_unitz00_5779,
				BgL_sexpz00_5780);
		}

	}



/* unit-sexp*-add-head! */
	BGL_EXPORTED_DEF obj_t
		BGl_unitzd2sexpza2zd2addzd2headz12z62zzast_unitz00(obj_t BgL_unitz00_27,
		obj_t BgL_sexpz00_28)
	{
		{	/* Ast/unit.scm 69 */
			{	/* Ast/unit.scm 70 */
				obj_t BgL_arg1472z00_4027;

				{	/* Ast/unit.scm 70 */
					obj_t BgL_arg1473z00_4028;

					BgL_arg1473z00_4028 =
						STRUCT_REF(((obj_t) BgL_unitz00_27), (int) (2L));
					BgL_arg1472z00_4027 =
						BGl_appendzd221011zd2zzast_unitz00(BgL_sexpz00_28,
						BgL_arg1473z00_4028);
				}
				{	/* Ast/unit.scm 70 */
					int BgL_auxz00_5887;
					obj_t BgL_tmpz00_5885;

					BgL_auxz00_5887 = (int) (2L);
					BgL_tmpz00_5885 = ((obj_t) BgL_unitz00_27);
					return
						STRUCT_SET(BgL_tmpz00_5885, BgL_auxz00_5887, BgL_arg1472z00_4027);
				}
			}
		}

	}



/* &unit-sexp*-add-head! */
	obj_t BGl_z62unitzd2sexpza2zd2addzd2headz12z00zzast_unitz00(obj_t
		BgL_envz00_5781, obj_t BgL_unitz00_5782, obj_t BgL_sexpz00_5783)
	{
		{	/* Ast/unit.scm 69 */
			return
				BGl_unitzd2sexpza2zd2addzd2headz12z62zzast_unitz00(BgL_unitz00_5782,
				BgL_sexpz00_5783);
		}

	}



/* unit->defs */
	BGL_EXPORTED_DEF obj_t BGl_unitzd2ze3defsz31zzast_unitz00(obj_t
		BgL_unitz00_29)
	{
		{	/* Ast/unit.scm 75 */
			{	/* Ast/unit.scm 77 */
				obj_t BgL_arg1485z00_1746;

				{	/* Ast/unit.scm 77 */
					obj_t BgL_arg1513z00_1751;

					{	/* Ast/unit.scm 77 */
						obj_t BgL_arg1514z00_1752;

						BgL_arg1514z00_1752 =
							STRUCT_REF(((obj_t) BgL_unitz00_29), (int) (0L));
						{	/* Ast/unit.scm 77 */
							obj_t BgL_arg1455z00_4033;

							BgL_arg1455z00_4033 =
								SYMBOL_TO_STRING(((obj_t) BgL_arg1514z00_1752));
							BgL_arg1513z00_1751 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_4033);
					}}
					BgL_arg1485z00_1746 =
						BGl_stringzd2downcasezd2zz__r4_strings_6_7z00(BgL_arg1513z00_1751);
				}
				{	/* Ast/unit.scm 76 */
					obj_t BgL_list1486z00_1747;

					{	/* Ast/unit.scm 76 */
						obj_t BgL_arg1489z00_1748;

						{	/* Ast/unit.scm 76 */
							obj_t BgL_arg1502z00_1749;

							{	/* Ast/unit.scm 76 */
								obj_t BgL_arg1509z00_1750;

								BgL_arg1509z00_1750 =
									MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
								BgL_arg1502z00_1749 =
									MAKE_YOUNG_PAIR(BGl_string3562z00zzast_unitz00,
									BgL_arg1509z00_1750);
							}
							BgL_arg1489z00_1748 =
								MAKE_YOUNG_PAIR(BgL_arg1485z00_1746, BgL_arg1502z00_1749);
						}
						BgL_list1486z00_1747 =
							MAKE_YOUNG_PAIR(BGl_string3563z00zzast_unitz00,
							BgL_arg1489z00_1748);
					}
					BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list1486z00_1747);
			}}
			{	/* Ast/unit.scm 81 */
				obj_t BgL_idz00_1756;

				BgL_idz00_1756 = STRUCT_REF(((obj_t) BgL_unitz00_29), (int) (0L));
				{	/* Ast/unit.scm 81 */
					obj_t BgL_weightz00_1757;

					BgL_weightz00_1757 = STRUCT_REF(((obj_t) BgL_unitz00_29), (int) (1L));
					{	/* Ast/unit.scm 82 */
						obj_t BgL_sexpza2za2_1758;

						BgL_sexpza2za2_1758 =
							STRUCT_REF(((obj_t) BgL_unitz00_29), (int) (2L));
						{	/* Ast/unit.scm 83 */
							obj_t BgL_gdefsz00_1759;

							BgL_gdefsz00_1759 =
								BGl_findzd2globalzd2defsz00zzast_findzd2gdefszd2
								(BgL_sexpza2za2_1758);
							{	/* Ast/unit.scm 84 */

								{	/* Ast/unit.scm 86 */
									obj_t BgL_g1110z00_1760;
									obj_t BgL_g1111z00_1761;

									if (PROCEDUREP(BgL_sexpza2za2_1758))
										{	/* Ast/unit.scm 86 */
											BgL_g1110z00_1760 =
												BGL_PROCEDURE_CALL0(BgL_sexpza2za2_1758);
										}
									else
										{	/* Ast/unit.scm 86 */
											BgL_g1110z00_1760 =
												BGl_toplevelza2zd2ze3astz93zzast_unitz00
												(BgL_sexpza2za2_1758, BgL_gdefsz00_1759);
										}
									if (CBOOL(STRUCT_REF(((obj_t) BgL_unitz00_29), (int) (4L))))
										{	/* Ast/unit.scm 89 */
											obj_t BgL_list1655z00_1820;

											BgL_list1655z00_1820 = MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
											BgL_g1111z00_1761 = BgL_list1655z00_1820;
										}
									else
										{	/* Ast/unit.scm 89 */
											BgL_g1111z00_1761 = BNIL;
										}
									{
										obj_t BgL_aexpza2za2_1764;
										obj_t BgL_initza2za2_1765;
										obj_t BgL_defza2za2_1766;

										BgL_aexpza2za2_1764 = BgL_g1110z00_1760;
										BgL_initza2za2_1765 = BgL_g1111z00_1761;
										BgL_defza2za2_1766 = BNIL;
									BgL_zc3z04anonymousza31536ze3z87_1767:
										if (NULLP(BgL_aexpza2za2_1764))
											{	/* Ast/unit.scm 91 */
												if (NULLP(BgL_initza2za2_1765))
													{	/* Ast/unit.scm 92 */
														return bgl_reverse_bang(BgL_defza2za2_1766);
													}
												else
													{	/* Ast/unit.scm 93 */
														obj_t BgL_bodyz00_1770;

														if (CBOOL(STRUCT_REF(
																	((obj_t) BgL_unitz00_29), (int) (4L))))
															{	/* Ast/unit.scm 94 */
																obj_t BgL_arg1564z00_1784;

																{	/* Ast/unit.scm 94 */
																	obj_t BgL_arg1565z00_1785;
																	obj_t BgL_arg1571z00_1786;

																	{	/* Ast/unit.scm 94 */
																		obj_t BgL_arg1573z00_1787;

																		{	/* Ast/unit.scm 94 */
																			obj_t BgL_arg1575z00_1788;
																			obj_t BgL_arg1576z00_1789;

																			BgL_arg1575z00_1788 =
																				BGl_unitzd2requirezd2initzd2idzd2zzast_unitz00
																				(BgL_idz00_1756);
																			BgL_arg1576z00_1789 =
																				MAKE_YOUNG_PAIR
																				(BGl_za2moduleza2z00zzmodule_modulez00,
																				BNIL);
																			BgL_arg1573z00_1787 =
																				MAKE_YOUNG_PAIR(BgL_arg1575z00_1788,
																				BgL_arg1576z00_1789);
																		}
																		BgL_arg1565z00_1785 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
																			BgL_arg1573z00_1787);
																	}
																	{	/* Ast/unit.scm 96 */
																		obj_t BgL_arg1584z00_1790;

																		{	/* Ast/unit.scm 96 */
																			obj_t BgL_arg1585z00_1791;

																			{	/* Ast/unit.scm 96 */
																				obj_t BgL_arg1589z00_1792;
																				obj_t BgL_arg1591z00_1793;

																				{	/* Ast/unit.scm 96 */
																					obj_t BgL_arg1593z00_1794;

																					{	/* Ast/unit.scm 96 */
																						obj_t BgL_arg1594z00_1795;
																						obj_t BgL_arg1595z00_1796;

																						{	/* Ast/unit.scm 96 */
																							obj_t BgL_arg1602z00_1797;

																							{	/* Ast/unit.scm 96 */
																								obj_t BgL_arg1605z00_1798;
																								obj_t BgL_arg1606z00_1799;

																								BgL_arg1605z00_1798 =
																									BGl_unitzd2requirezd2initzd2idzd2zzast_unitz00
																									(BgL_idz00_1756);
																								BgL_arg1606z00_1799 =
																									MAKE_YOUNG_PAIR
																									(BGl_za2moduleza2z00zzmodule_modulez00,
																									BNIL);
																								BgL_arg1602z00_1797 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1605z00_1798,
																									BgL_arg1606z00_1799);
																							}
																							BgL_arg1594z00_1795 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(0), BgL_arg1602z00_1797);
																						}
																						BgL_arg1595z00_1796 =
																							MAKE_YOUNG_PAIR(BFALSE, BNIL);
																						BgL_arg1593z00_1794 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1594z00_1795,
																							BgL_arg1595z00_1796);
																					}
																					BgL_arg1589z00_1792 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(1),
																						BgL_arg1593z00_1794);
																				}
																				{	/* Ast/unit.scm 99 */
																					obj_t BgL_arg1609z00_1800;
																					obj_t BgL_arg1611z00_1801;

																					{	/* Ast/unit.scm 99 */
																						obj_t
																							BgL_zc3z04anonymousza31614ze3z87_5784;
																						BgL_zc3z04anonymousza31614ze3z87_5784
																							=
																							MAKE_FX_PROCEDURE
																							(BGl_z62zc3z04anonymousza31614ze3ze5zzast_unitz00,
																							(int) (1L), (int) (1L));
																						PROCEDURE_SET
																							(BgL_zc3z04anonymousza31614ze3z87_5784,
																							(int) (0L), BgL_idz00_1756);
																						BgL_arg1609z00_1800 =
																							BGl_initializa7ezd2importedzd2modulesza7zzmodule_impusez00
																							(BgL_zc3z04anonymousza31614ze3z87_5784);
																					}
																					BgL_arg1611z00_1801 =
																						BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																						(bgl_reverse_bang
																						(BgL_initza2za2_1765), BNIL);
																					BgL_arg1591z00_1793 =
																						BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																						(BgL_arg1609z00_1800,
																						BgL_arg1611z00_1801);
																				}
																				BgL_arg1585z00_1791 =
																					MAKE_YOUNG_PAIR(BgL_arg1589z00_1792,
																					BgL_arg1591z00_1793);
																			}
																			BgL_arg1584z00_1790 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(2),
																				BgL_arg1585z00_1791);
																		}
																		BgL_arg1571z00_1786 =
																			MAKE_YOUNG_PAIR(BgL_arg1584z00_1790,
																			BNIL);
																	}
																	BgL_arg1564z00_1784 =
																		MAKE_YOUNG_PAIR(BgL_arg1565z00_1785,
																		BgL_arg1571z00_1786);
																}
																BgL_bodyz00_1770 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
																	BgL_arg1564z00_1784);
															}
														else
															{	/* Ast/unit.scm 93 */
																BgL_bodyz00_1770 =
																	BGl_normaliza7ezd2prognz75zztools_prognz00
																	(bgl_reverse_bang(BgL_initza2za2_1765));
															}
														{	/* Ast/unit.scm 93 */
															BgL_globalz00_bglt BgL_initz00_1771;

															{	/* Ast/unit.scm 104 */
																obj_t BgL_arg1559z00_1781;

																{	/* Ast/unit.scm 104 */
																	obj_t BgL_arg1561z00_1782;

																	BgL_arg1561z00_1782 =
																		BGl_unitzd2initializa7erzd2idza7zzast_unitz00
																		(BgL_idz00_1756);
																	BgL_arg1559z00_1781 =
																		BGl_makezd2typedzd2identz00zzast_identz00
																		(BgL_arg1561z00_1782, CNST_TABLE_REF(4));
																}
																BgL_initz00_1771 =
																	BGl_defzd2globalzd2sfunzd2nozd2warningz12z12zzast_glozd2defzd2
																	(BgL_arg1559z00_1781, BNIL, BNIL,
																	BGl_za2moduleza2z00zzmodule_modulez00,
																	CNST_TABLE_REF(5),
																	BGl_za2modulezd2clauseza2zd2zzmodule_modulez00,
																	CNST_TABLE_REF(6), BgL_bodyz00_1770);
															}
															{	/* Ast/unit.scm 102 */

																if (CBOOL(STRUCT_REF(
																			((obj_t) BgL_unitz00_29), (int) (4L))))
																	{	/* Ast/unit.scm 114 */
																		obj_t BgL_vz00_4043;

																		BgL_vz00_4043 = CNST_TABLE_REF(7);
																		((((BgL_globalz00_bglt)
																					COBJECT(BgL_initz00_1771))->
																				BgL_importz00) =
																			((obj_t) BgL_vz00_4043), BUNSPEC);
																	}
																else
																	{	/* Ast/unit.scm 113 */
																		BFALSE;
																	}
																if (
																	(BgL_unitz00_29 ==
																		BGl_getzd2toplevelzd2unitz00zzmodule_includez00
																		()))
																	{	/* Ast/unit.scm 117 */
																		BFALSE;
																	}
																else
																	{	/* Ast/unit.scm 117 */
																		((((BgL_variablez00_bglt) COBJECT(
																						((BgL_variablez00_bglt)
																							BgL_initz00_1771)))->
																				BgL_userzf3zf3) =
																			((bool_t) ((bool_t) 0)), BUNSPEC);
																	}
																BGl_declarezd2unitz12zc0zzast_unitz00
																	(BgL_idz00_1756,
																	(long) CINT(BgL_weightz00_1757));
																((((BgL_globalz00_bglt)
																			COBJECT(BgL_initz00_1771))->
																		BgL_evaluablezf3zf3) =
																	((bool_t) ((bool_t) 0)), BUNSPEC);
																if (CBOOL(STRUCT_REF(((obj_t) BgL_unitz00_29),
																			(int) (4L))))
																	{	/* Ast/unit.scm 128 */
																		obj_t BgL_idz00_1777;

																		{	/* Ast/unit.scm 128 */
																			obj_t BgL_arg1552z00_1779;

																			BgL_arg1552z00_1779 =
																				BGl_unitzd2requirezd2initzd2idzd2zzast_unitz00
																				(BgL_idz00_1756);
																			BgL_idz00_1777 =
																				BGl_makezd2typedzd2identz00zzast_identz00
																				(BgL_arg1552z00_1779,
																				CNST_TABLE_REF(4));
																		}
																		{	/* Ast/unit.scm 128 */
																			BgL_globalz00_bglt BgL_gloz00_1778;

																			BgL_gloz00_1778 =
																				BGl_defzd2globalzd2svarz12z12zzast_glozd2defzd2
																				(BgL_idz00_1777,
																				BGl_za2moduleza2z00zzmodule_modulez00,
																				BGl_za2modulezd2clauseza2zd2zzmodule_modulez00,
																				CNST_TABLE_REF(8));
																			{	/* Ast/unit.scm 130 */

																				((((BgL_variablez00_bglt) COBJECT(
																								((BgL_variablez00_bglt)
																									BgL_gloz00_1778)))->
																						BgL_userzf3zf3) =
																					((bool_t) ((bool_t) 0)), BUNSPEC);
																				((((BgL_globalz00_bglt)
																							COBJECT(BgL_gloz00_1778))->
																						BgL_evaluablezf3zf3) =
																					((bool_t) ((bool_t) 0)), BUNSPEC);
																	}}}
																else
																	{	/* Ast/unit.scm 127 */
																		BFALSE;
																	}
																{	/* Ast/unit.scm 134 */
																	obj_t BgL_arg1553z00_1780;

																	BgL_arg1553z00_1780 =
																		bgl_reverse_bang(BgL_defza2za2_1766);
																	return
																		MAKE_YOUNG_PAIR(
																		((obj_t) BgL_initz00_1771),
																		BgL_arg1553z00_1780);
																}
															}
														}
													}
											}
										else
											{	/* Ast/unit.scm 136 */
												bool_t BgL_test3619z00_6006;

												{	/* Ast/unit.scm 136 */
													obj_t BgL_arg1651z00_1816;

													BgL_arg1651z00_1816 =
														CAR(((obj_t) BgL_aexpza2za2_1764));
													{	/* Ast/unit.scm 136 */
														obj_t BgL_classz00_4054;

														BgL_classz00_4054 = BGl_globalz00zzast_varz00;
														if (BGL_OBJECTP(BgL_arg1651z00_1816))
															{	/* Ast/unit.scm 136 */
																BgL_objectz00_bglt BgL_arg1807z00_4056;

																BgL_arg1807z00_4056 =
																	(BgL_objectz00_bglt) (BgL_arg1651z00_1816);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Ast/unit.scm 136 */
																		long BgL_idxz00_4062;

																		BgL_idxz00_4062 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_4056);
																		BgL_test3619z00_6006 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_4062 + 2L)) ==
																			BgL_classz00_4054);
																	}
																else
																	{	/* Ast/unit.scm 136 */
																		bool_t BgL_res3506z00_4087;

																		{	/* Ast/unit.scm 136 */
																			obj_t BgL_oclassz00_4070;

																			{	/* Ast/unit.scm 136 */
																				obj_t BgL_arg1815z00_4078;
																				long BgL_arg1816z00_4079;

																				BgL_arg1815z00_4078 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Ast/unit.scm 136 */
																					long BgL_arg1817z00_4080;

																					BgL_arg1817z00_4080 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_4056);
																					BgL_arg1816z00_4079 =
																						(BgL_arg1817z00_4080 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_4070 =
																					VECTOR_REF(BgL_arg1815z00_4078,
																					BgL_arg1816z00_4079);
																			}
																			{	/* Ast/unit.scm 136 */
																				bool_t BgL__ortest_1115z00_4071;

																				BgL__ortest_1115z00_4071 =
																					(BgL_classz00_4054 ==
																					BgL_oclassz00_4070);
																				if (BgL__ortest_1115z00_4071)
																					{	/* Ast/unit.scm 136 */
																						BgL_res3506z00_4087 =
																							BgL__ortest_1115z00_4071;
																					}
																				else
																					{	/* Ast/unit.scm 136 */
																						long BgL_odepthz00_4072;

																						{	/* Ast/unit.scm 136 */
																							obj_t BgL_arg1804z00_4073;

																							BgL_arg1804z00_4073 =
																								(BgL_oclassz00_4070);
																							BgL_odepthz00_4072 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_4073);
																						}
																						if ((2L < BgL_odepthz00_4072))
																							{	/* Ast/unit.scm 136 */
																								obj_t BgL_arg1802z00_4075;

																								{	/* Ast/unit.scm 136 */
																									obj_t BgL_arg1803z00_4076;

																									BgL_arg1803z00_4076 =
																										(BgL_oclassz00_4070);
																									BgL_arg1802z00_4075 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_4076, 2L);
																								}
																								BgL_res3506z00_4087 =
																									(BgL_arg1802z00_4075 ==
																									BgL_classz00_4054);
																							}
																						else
																							{	/* Ast/unit.scm 136 */
																								BgL_res3506z00_4087 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test3619z00_6006 = BgL_res3506z00_4087;
																	}
															}
														else
															{	/* Ast/unit.scm 136 */
																BgL_test3619z00_6006 = ((bool_t) 0);
															}
													}
												}
												if (BgL_test3619z00_6006)
													{	/* Ast/unit.scm 137 */
														obj_t BgL_arg1627z00_1810;
														obj_t BgL_arg1629z00_1811;

														BgL_arg1627z00_1810 =
															CDR(((obj_t) BgL_aexpza2za2_1764));
														{	/* Ast/unit.scm 139 */
															obj_t BgL_arg1630z00_1812;

															BgL_arg1630z00_1812 =
																CAR(((obj_t) BgL_aexpza2za2_1764));
															BgL_arg1629z00_1811 =
																MAKE_YOUNG_PAIR(BgL_arg1630z00_1812,
																BgL_defza2za2_1766);
														}
														{
															obj_t BgL_defza2za2_6037;
															obj_t BgL_aexpza2za2_6036;

															BgL_aexpza2za2_6036 = BgL_arg1627z00_1810;
															BgL_defza2za2_6037 = BgL_arg1629z00_1811;
															BgL_defza2za2_1766 = BgL_defza2za2_6037;
															BgL_aexpza2za2_1764 = BgL_aexpza2za2_6036;
															goto BgL_zc3z04anonymousza31536ze3z87_1767;
														}
													}
												else
													{	/* Ast/unit.scm 140 */
														obj_t BgL_arg1642z00_1813;
														obj_t BgL_arg1646z00_1814;

														BgL_arg1642z00_1813 =
															CDR(((obj_t) BgL_aexpza2za2_1764));
														{	/* Ast/unit.scm 141 */
															obj_t BgL_arg1650z00_1815;

															BgL_arg1650z00_1815 =
																CAR(((obj_t) BgL_aexpza2za2_1764));
															BgL_arg1646z00_1814 =
																MAKE_YOUNG_PAIR(BgL_arg1650z00_1815,
																BgL_initza2za2_1765);
														}
														{
															obj_t BgL_initza2za2_6044;
															obj_t BgL_aexpza2za2_6043;

															BgL_aexpza2za2_6043 = BgL_arg1642z00_1813;
															BgL_initza2za2_6044 = BgL_arg1646z00_1814;
															BgL_initza2za2_1765 = BgL_initza2za2_6044;
															BgL_aexpza2za2_1764 = BgL_aexpza2za2_6043;
															goto BgL_zc3z04anonymousza31536ze3z87_1767;
														}
													}
											}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &unit->defs */
	obj_t BGl_z62unitzd2ze3defsz53zzast_unitz00(obj_t BgL_envz00_5785,
		obj_t BgL_unitz00_5786)
	{
		{	/* Ast/unit.scm 75 */
			return BGl_unitzd2ze3defsz31zzast_unitz00(BgL_unitz00_5786);
		}

	}



/* &<@anonymous:1614> */
	obj_t BGl_z62zc3z04anonymousza31614ze3ze5zzast_unitz00(obj_t BgL_envz00_5787,
		obj_t BgL_mz00_5789)
	{
		{	/* Ast/unit.scm 98 */
			return
				BGl_unitzd2initializa7erzd2idza7zzast_unitz00(PROCEDURE_REF
				(BgL_envz00_5787, (int) (0L)));
		}

	}



/* declare-unit! */
	obj_t BGl_declarezd2unitz12zc0zzast_unitz00(obj_t BgL_idz00_30,
		long BgL_weightz00_31)
	{
		{	/* Ast/unit.scm 152 */
			{	/* Ast/unit.scm 153 */
				bool_t BgL_test3624z00_6049;

				if (NULLP(BGl_za2unitzd2listza2zd2zzast_unitz00))
					{	/* Ast/unit.scm 153 */
						BgL_test3624z00_6049 = ((bool_t) 1);
					}
				else
					{	/* Ast/unit.scm 153 */
						obj_t BgL_arg1711z00_1847;

						BgL_arg1711z00_1847 =
							CDR(CAR(BGl_za2unitzd2listza2zd2zzast_unitz00));
						BgL_test3624z00_6049 =
							(BgL_weightz00_31 < (long) CINT(BgL_arg1711z00_1847));
					}
				if (BgL_test3624z00_6049)
					{	/* Ast/unit.scm 154 */
						obj_t BgL_arg1675z00_1825;

						BgL_arg1675z00_1825 =
							MAKE_YOUNG_PAIR(BgL_idz00_30, BINT(BgL_weightz00_31));
						return (BGl_za2unitzd2listza2zd2zzast_unitz00 =
							MAKE_YOUNG_PAIR(BgL_arg1675z00_1825,
								BGl_za2unitzd2listza2zd2zzast_unitz00), BUNSPEC);
					}
				else
					{
						obj_t BgL_ulistz00_1827;

						BgL_ulistz00_1827 = BGl_za2unitzd2listza2zd2zzast_unitz00;
					BgL_zc3z04anonymousza31676ze3z87_1828:
						{	/* Ast/unit.scm 157 */
							bool_t BgL_test3626z00_6059;

							{	/* Ast/unit.scm 157 */
								long BgL_tmpz00_6060;

								{	/* Ast/unit.scm 157 */
									obj_t BgL_pairz00_4098;

									BgL_pairz00_4098 = CAR(((obj_t) BgL_ulistz00_1827));
									BgL_tmpz00_6060 = (long) CINT(CDR(BgL_pairz00_4098));
								}
								BgL_test3626z00_6059 = (BgL_weightz00_31 < BgL_tmpz00_6060);
							}
							if (BgL_test3626z00_6059)
								{	/* Ast/unit.scm 157 */
									{	/* Ast/unit.scm 158 */
										obj_t BgL_arg1688z00_1832;

										{	/* Ast/unit.scm 158 */
											obj_t BgL_arg1689z00_1833;
											obj_t BgL_arg1691z00_1834;

											BgL_arg1689z00_1833 = CAR(((obj_t) BgL_ulistz00_1827));
											BgL_arg1691z00_1834 = CDR(((obj_t) BgL_ulistz00_1827));
											BgL_arg1688z00_1832 =
												MAKE_YOUNG_PAIR(BgL_arg1689z00_1833,
												BgL_arg1691z00_1834);
										}
										{	/* Ast/unit.scm 158 */
											obj_t BgL_tmpz00_6071;

											BgL_tmpz00_6071 = ((obj_t) BgL_ulistz00_1827);
											SET_CDR(BgL_tmpz00_6071, BgL_arg1688z00_1832);
										}
									}
									{	/* Ast/unit.scm 159 */
										obj_t BgL_arg1692z00_1835;

										BgL_arg1692z00_1835 =
											MAKE_YOUNG_PAIR(BgL_idz00_30, BINT(BgL_weightz00_31));
										{	/* Ast/unit.scm 159 */
											obj_t BgL_tmpz00_6076;

											BgL_tmpz00_6076 = ((obj_t) BgL_ulistz00_1827);
											return SET_CAR(BgL_tmpz00_6076, BgL_arg1692z00_1835);
										}
									}
								}
							else
								{	/* Ast/unit.scm 157 */
									if (NULLP(CDR(((obj_t) BgL_ulistz00_1827))))
										{	/* Ast/unit.scm 161 */
											obj_t BgL_arg1701z00_1838;

											{	/* Ast/unit.scm 161 */
												obj_t BgL_arg1702z00_1839;

												BgL_arg1702z00_1839 =
													MAKE_YOUNG_PAIR(BgL_idz00_30, BINT(BgL_weightz00_31));
												{	/* Ast/unit.scm 161 */
													obj_t BgL_list1703z00_1840;

													BgL_list1703z00_1840 =
														MAKE_YOUNG_PAIR(BgL_arg1702z00_1839, BNIL);
													BgL_arg1701z00_1838 = BgL_list1703z00_1840;
												}
											}
											{	/* Ast/unit.scm 161 */
												obj_t BgL_tmpz00_6086;

												BgL_tmpz00_6086 = ((obj_t) BgL_ulistz00_1827);
												return SET_CDR(BgL_tmpz00_6086, BgL_arg1701z00_1838);
											}
										}
									else
										{	/* Ast/unit.scm 163 */
											obj_t BgL_arg1705z00_1841;

											BgL_arg1705z00_1841 = CDR(((obj_t) BgL_ulistz00_1827));
											{
												obj_t BgL_ulistz00_6091;

												BgL_ulistz00_6091 = BgL_arg1705z00_1841;
												BgL_ulistz00_1827 = BgL_ulistz00_6091;
												goto BgL_zc3z04anonymousza31676ze3z87_1828;
											}
										}
								}
						}
					}
			}
		}

	}



/* unit-initializer-id */
	BGL_EXPORTED_DEF obj_t BGl_unitzd2initializa7erzd2idza7zzast_unitz00(obj_t
		BgL_idz00_32)
	{
		{	/* Ast/unit.scm 168 */
			{	/* Ast/unit.scm 169 */
				obj_t BgL_arg1717z00_1849;

				{	/* Ast/unit.scm 169 */
					obj_t BgL_arg1718z00_1850;
					obj_t BgL_arg1720z00_1851;

					{	/* Ast/unit.scm 169 */
						obj_t BgL_arg1455z00_4110;

						BgL_arg1455z00_4110 = SYMBOL_TO_STRING(((obj_t) BgL_idz00_32));
						BgL_arg1718z00_1850 =
							BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_4110);
					}
					{	/* Ast/unit.scm 169 */
						obj_t BgL_symbolz00_4111;

						BgL_symbolz00_4111 = CNST_TABLE_REF(9);
						{	/* Ast/unit.scm 169 */
							obj_t BgL_arg1455z00_4112;

							BgL_arg1455z00_4112 = SYMBOL_TO_STRING(BgL_symbolz00_4111);
							BgL_arg1720z00_1851 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_4112);
						}
					}
					BgL_arg1717z00_1849 =
						string_append(BgL_arg1718z00_1850, BgL_arg1720z00_1851);
				}
				return bstring_to_symbol(BgL_arg1717z00_1849);
			}
		}

	}



/* &unit-initializer-id */
	obj_t BGl_z62unitzd2initializa7erzd2idzc5zzast_unitz00(obj_t BgL_envz00_5790,
		obj_t BgL_idz00_5791)
	{
		{	/* Ast/unit.scm 168 */
			return BGl_unitzd2initializa7erzd2idza7zzast_unitz00(BgL_idz00_5791);
		}

	}



/* unit-require-init-id */
	obj_t BGl_unitzd2requirezd2initzd2idzd2zzast_unitz00(obj_t BgL_idz00_33)
	{
		{	/* Ast/unit.scm 174 */
			{	/* Ast/unit.scm 175 */
				obj_t BgL_arg1722z00_1852;

				{	/* Ast/unit.scm 175 */
					obj_t BgL_arg1724z00_1853;
					obj_t BgL_arg1733z00_1854;

					{	/* Ast/unit.scm 175 */
						obj_t BgL_arg1455z00_4115;

						BgL_arg1455z00_4115 = SYMBOL_TO_STRING(((obj_t) BgL_idz00_33));
						BgL_arg1724z00_1853 =
							BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_4115);
					}
					{	/* Ast/unit.scm 175 */
						obj_t BgL_symbolz00_4116;

						BgL_symbolz00_4116 = CNST_TABLE_REF(10);
						{	/* Ast/unit.scm 175 */
							obj_t BgL_arg1455z00_4117;

							BgL_arg1455z00_4117 = SYMBOL_TO_STRING(BgL_symbolz00_4116);
							BgL_arg1733z00_1854 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_4117);
						}
					}
					BgL_arg1722z00_1852 =
						string_append(BgL_arg1724z00_1853, BgL_arg1733z00_1854);
				}
				return bstring_to_symbol(BgL_arg1722z00_1852);
			}
		}

	}



/* unit-initializers */
	BGL_EXPORTED_DEF obj_t BGl_unitzd2initializa7ersz75zzast_unitz00(void)
	{
		{	/* Ast/unit.scm 180 */
			{	/* Ast/unit.scm 181 */
				obj_t BgL_l1262z00_1855;

				BgL_l1262z00_1855 = BGl_za2unitzd2listza2zd2zzast_unitz00;
				if (NULLP(BgL_l1262z00_1855))
					{	/* Ast/unit.scm 181 */
						return BNIL;
					}
				else
					{	/* Ast/unit.scm 181 */
						obj_t BgL_head1264z00_1857;

						BgL_head1264z00_1857 = MAKE_YOUNG_PAIR(BNIL, BNIL);
						{
							obj_t BgL_l1262z00_1859;
							obj_t BgL_tail1265z00_1860;

							BgL_l1262z00_1859 = BgL_l1262z00_1855;
							BgL_tail1265z00_1860 = BgL_head1264z00_1857;
						BgL_zc3z04anonymousza31735ze3z87_1861:
							if (NULLP(BgL_l1262z00_1859))
								{	/* Ast/unit.scm 181 */
									return CDR(BgL_head1264z00_1857);
								}
							else
								{	/* Ast/unit.scm 181 */
									obj_t BgL_newtail1266z00_1863;

									{	/* Ast/unit.scm 181 */
										obj_t BgL_arg1738z00_1865;

										{	/* Ast/unit.scm 181 */
											obj_t BgL_unitz00_1866;

											BgL_unitz00_1866 = CAR(((obj_t) BgL_l1262z00_1859));
											{	/* Ast/unit.scm 182 */
												obj_t BgL_arg1739z00_1867;

												{	/* Ast/unit.scm 182 */
													obj_t BgL_arg1740z00_1868;

													BgL_arg1740z00_1868 = CAR(((obj_t) BgL_unitz00_1866));
													BgL_arg1739z00_1867 =
														BGl_unitzd2initializa7erzd2idza7zzast_unitz00
														(BgL_arg1740z00_1868);
												}
												BgL_arg1738z00_1865 =
													BGl_findzd2globalzf2modulez20zzast_envz00
													(BgL_arg1739z00_1867,
													BGl_za2moduleza2z00zzmodule_modulez00);
											}
										}
										BgL_newtail1266z00_1863 =
											MAKE_YOUNG_PAIR(BgL_arg1738z00_1865, BNIL);
									}
									SET_CDR(BgL_tail1265z00_1860, BgL_newtail1266z00_1863);
									{	/* Ast/unit.scm 181 */
										obj_t BgL_arg1737z00_1864;

										BgL_arg1737z00_1864 = CDR(((obj_t) BgL_l1262z00_1859));
										{
											obj_t BgL_tail1265z00_6126;
											obj_t BgL_l1262z00_6125;

											BgL_l1262z00_6125 = BgL_arg1737z00_1864;
											BgL_tail1265z00_6126 = BgL_newtail1266z00_1863;
											BgL_tail1265z00_1860 = BgL_tail1265z00_6126;
											BgL_l1262z00_1859 = BgL_l1262z00_6125;
											goto BgL_zc3z04anonymousza31735ze3z87_1861;
										}
									}
								}
						}
					}
			}
		}

	}



/* &unit-initializers */
	obj_t BGl_z62unitzd2initializa7ersz17zzast_unitz00(obj_t BgL_envz00_5792)
	{
		{	/* Ast/unit.scm 180 */
			return BGl_unitzd2initializa7ersz75zzast_unitz00();
		}

	}



/* unit-init-calls */
	BGL_EXPORTED_DEF obj_t BGl_unitzd2initzd2callsz00zzast_unitz00(void)
	{
		{	/* Ast/unit.scm 188 */
			{	/* Ast/unit.scm 189 */
				obj_t BgL_l1267z00_1870;

				BgL_l1267z00_1870 = BGl_za2unitzd2listza2zd2zzast_unitz00;
				if (NULLP(BgL_l1267z00_1870))
					{	/* Ast/unit.scm 189 */
						return BNIL;
					}
				else
					{	/* Ast/unit.scm 189 */
						obj_t BgL_head1269z00_1872;

						BgL_head1269z00_1872 = MAKE_YOUNG_PAIR(BNIL, BNIL);
						{
							obj_t BgL_l1267z00_1874;
							obj_t BgL_tail1270z00_1875;

							BgL_l1267z00_1874 = BgL_l1267z00_1870;
							BgL_tail1270z00_1875 = BgL_head1269z00_1872;
						BgL_zc3z04anonymousza31742ze3z87_1876:
							if (NULLP(BgL_l1267z00_1874))
								{	/* Ast/unit.scm 189 */
									return CDR(BgL_head1269z00_1872);
								}
							else
								{	/* Ast/unit.scm 189 */
									obj_t BgL_newtail1271z00_1878;

									{	/* Ast/unit.scm 189 */
										obj_t BgL_arg1747z00_1880;

										{	/* Ast/unit.scm 189 */
											obj_t BgL_unitz00_1881;

											BgL_unitz00_1881 = CAR(((obj_t) BgL_l1267z00_1874));
											{	/* Ast/unit.scm 189 */
												obj_t BgL_arg1748z00_1882;

												{	/* Ast/unit.scm 189 */
													obj_t BgL_arg1749z00_1883;

													{	/* Ast/unit.scm 189 */
														obj_t BgL_arg1750z00_1884;
														obj_t BgL_arg1751z00_1885;

														{	/* Ast/unit.scm 189 */
															obj_t BgL_arg1752z00_1886;

															BgL_arg1752z00_1886 =
																CAR(((obj_t) BgL_unitz00_1881));
															BgL_arg1750z00_1884 =
																BGl_unitzd2initializa7erzd2idza7zzast_unitz00
																(BgL_arg1752z00_1886);
														}
														BgL_arg1751z00_1885 =
															MAKE_YOUNG_PAIR
															(BGl_za2moduleza2z00zzmodule_modulez00, BNIL);
														BgL_arg1749z00_1883 =
															MAKE_YOUNG_PAIR(BgL_arg1750z00_1884,
															BgL_arg1751z00_1885);
													}
													BgL_arg1748z00_1882 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
														BgL_arg1749z00_1883);
												}
												BgL_arg1747z00_1880 =
													MAKE_YOUNG_PAIR(BgL_arg1748z00_1882, BNIL);
											}
										}
										BgL_newtail1271z00_1878 =
											MAKE_YOUNG_PAIR(BgL_arg1747z00_1880, BNIL);
									}
									SET_CDR(BgL_tail1270z00_1875, BgL_newtail1271z00_1878);
									{	/* Ast/unit.scm 189 */
										obj_t BgL_arg1746z00_1879;

										BgL_arg1746z00_1879 = CDR(((obj_t) BgL_l1267z00_1874));
										{
											obj_t BgL_tail1270z00_6149;
											obj_t BgL_l1267z00_6148;

											BgL_l1267z00_6148 = BgL_arg1746z00_1879;
											BgL_tail1270z00_6149 = BgL_newtail1271z00_1878;
											BgL_tail1270z00_1875 = BgL_tail1270z00_6149;
											BgL_l1267z00_1874 = BgL_l1267z00_6148;
											goto BgL_zc3z04anonymousza31742ze3z87_1876;
										}
									}
								}
						}
					}
			}
		}

	}



/* &unit-init-calls */
	obj_t BGl_z62unitzd2initzd2callsz62zzast_unitz00(obj_t BgL_envz00_5793)
	{
		{	/* Ast/unit.scm 188 */
			return BGl_unitzd2initzd2callsz00zzast_unitz00();
		}

	}



/* toplevel*->ast */
	obj_t BGl_toplevelza2zd2ze3astz93zzast_unitz00(obj_t BgL_sexpza2za2_34,
		obj_t BgL_gdefsz00_35)
	{
		{	/* Ast/unit.scm 195 */
			{
				obj_t BgL_sexpza2za2_1890;
				obj_t BgL_aexpza2za2_1891;

				BgL_sexpza2za2_1890 = BgL_sexpza2za2_34;
				BgL_aexpza2za2_1891 = BNIL;
			BgL_zc3z04anonymousza31753ze3z87_1892:
				if (NULLP(BgL_sexpza2za2_1890))
					{	/* Ast/unit.scm 198 */
						return bgl_reverse_bang(BgL_aexpza2za2_1891);
					}
				else
					{	/* Ast/unit.scm 200 */
						obj_t BgL_arg1755z00_1894;
						obj_t BgL_arg1761z00_1895;

						BgL_arg1755z00_1894 = CDR(((obj_t) BgL_sexpza2za2_1890));
						{	/* Ast/unit.scm 201 */
							obj_t BgL_arg1762z00_1896;

							{	/* Ast/unit.scm 201 */
								obj_t BgL_arg1765z00_1897;

								BgL_arg1765z00_1897 = CAR(((obj_t) BgL_sexpza2za2_1890));
								BgL_arg1762z00_1896 =
									BGl_toplevelzd2ze3astz31zzast_unitz00(BgL_arg1765z00_1897,
									BgL_gdefsz00_35);
							}
							BgL_arg1761z00_1895 =
								BGl_appendzd221011zd2zzast_unitz00(BgL_arg1762z00_1896,
								BgL_aexpza2za2_1891);
						}
						{
							obj_t BgL_aexpza2za2_6161;
							obj_t BgL_sexpza2za2_6160;

							BgL_sexpza2za2_6160 = BgL_arg1755z00_1894;
							BgL_aexpza2za2_6161 = BgL_arg1761z00_1895;
							BgL_aexpza2za2_1891 = BgL_aexpza2za2_6161;
							BgL_sexpza2za2_1890 = BgL_sexpza2za2_6160;
							goto BgL_zc3z04anonymousza31753ze3z87_1892;
						}
					}
			}
		}

	}



/* lambda? */
	bool_t BGl_lambdazf3zf3zzast_unitz00(obj_t BgL_symz00_36)
	{
		{	/* Ast/unit.scm 206 */
			if (SYMBOLP(BgL_symz00_36))
				{	/* Ast/unit.scm 207 */
					obj_t BgL_arg1767z00_1900;

					BgL_arg1767z00_1900 =
						BGl_fastzd2idzd2ofzd2idzd2zzast_identz00(BgL_symz00_36, BFALSE);
					return (BgL_arg1767z00_1900 == CNST_TABLE_REF(11));
				}
			else
				{	/* Ast/unit.scm 207 */
					return ((bool_t) 0);
				}
		}

	}



/* error-class-shadow */
	obj_t BGl_errorzd2classzd2shadowz00zzast_unitz00(obj_t BgL_idz00_37,
		obj_t BgL_srcz00_38)
	{
		{	/* Ast/unit.scm 212 */
			{	/* Ast/unit.scm 213 */
				obj_t BgL_locz00_1901;

				BgL_locz00_1901 =
					BGl_findzd2locationzd2zztools_locationz00(BgL_srcz00_38);
				if (CBOOL(BgL_locz00_1901))
					{	/* Ast/unit.scm 215 */
						return
							BGl_userzd2errorzf2locationz20zztools_errorz00(BgL_locz00_1901,
							BGl_za2moduleza2z00zzmodule_modulez00,
							BGl_string3564z00zzast_unitz00, BgL_idz00_37, BNIL);
					}
				else
					{	/* Ast/unit.scm 215 */
						return
							BGl_userzd2errorzd2zztools_errorz00
							(BGl_za2moduleza2z00zzmodule_modulez00,
							BGl_string3564z00zzast_unitz00, BgL_idz00_37, BNIL);
					}
			}
		}

	}



/* toplevel->ast */
	obj_t BGl_toplevelzd2ze3astz31zzast_unitz00(obj_t BgL_sexpz00_39,
		obj_t BgL_gdefsz00_40)
	{
		{	/* Ast/unit.scm 224 */
		BGl_toplevelzd2ze3astz31zzast_unitz00:
			{
				obj_t BgL_varz00_1957;
				obj_t BgL_argsz00_1958;
				obj_t BgL_expz00_1959;
				obj_t BgL_varz00_1953;
				obj_t BgL_argsz00_1954;
				obj_t BgL_expz00_1955;
				obj_t BgL_varz00_1948;
				obj_t BgL_modulez00_1949;
				obj_t BgL_argsz00_1950;
				obj_t BgL_expz00_1951;
				obj_t BgL_varz00_1944;
				obj_t BgL_argsz00_1945;
				obj_t BgL_expz00_1946;
				obj_t BgL_varz00_1939;
				obj_t BgL_modulez00_1940;
				obj_t BgL_argsz00_1941;
				obj_t BgL_expz00_1942;
				obj_t BgL_varz00_1936;
				obj_t BgL_expz00_1937;
				obj_t BgL_varz00_1932;
				obj_t BgL_var2z00_1933;
				obj_t BgL_modulez00_1934;
				obj_t BgL_varz00_1929;
				obj_t BgL_var2z00_1930;
				obj_t BgL_varz00_1926;
				obj_t BgL_1zd2expzd2_1927;
				obj_t BgL_varz00_1921;
				obj_t BgL_lamz00_1922;
				obj_t BgL_argsz00_1923;
				obj_t BgL_expz00_1924;
				obj_t BgL_varz00_1916;
				obj_t BgL_argsz00_1917;
				obj_t BgL_expz00_1918;
				obj_t BgL_fz00_1919;
				obj_t BgL_varz00_1912;
				obj_t BgL_argsz00_1913;
				obj_t BgL_expz00_1914;
				obj_t BgL_varz00_1908;
				obj_t BgL_argsz00_1909;
				obj_t BgL_expz00_1910;

				if (PAIRP(BgL_sexpz00_39))
					{	/* Ast/unit.scm 225 */
						if ((CAR(((obj_t) BgL_sexpz00_39)) == CNST_TABLE_REF(2)))
							{	/* Ast/unit.scm 225 */
								if (NULLP(CDR(((obj_t) BgL_sexpz00_39))))
									{	/* Ast/unit.scm 225 */
										{	/* Ast/unit.scm 227 */
											obj_t BgL_list2204z00_2519;

											BgL_list2204z00_2519 =
												MAKE_YOUNG_PAIR(BgL_sexpz00_39, BNIL);
											return BgL_list2204z00_2519;
										}
									}
								else
									{	/* Ast/unit.scm 225 */
										obj_t BgL_arg1775z00_1968;

										BgL_arg1775z00_1968 = CDR(((obj_t) BgL_sexpz00_39));
										return
											bgl_reverse_bang(BGl_toplevelza2zd2ze3astz93zzast_unitz00
											(BgL_arg1775z00_1968, BgL_gdefsz00_40));
									}
							}
						else
							{	/* Ast/unit.scm 225 */
								obj_t BgL_cdrzd2422zd2_1970;

								BgL_cdrzd2422zd2_1970 = CDR(((obj_t) BgL_sexpz00_39));
								if ((CAR(((obj_t) BgL_sexpz00_39)) == CNST_TABLE_REF(17)))
									{	/* Ast/unit.scm 225 */
										if (PAIRP(BgL_cdrzd2422zd2_1970))
											{	/* Ast/unit.scm 225 */
												obj_t BgL_carzd2426zd2_1974;

												BgL_carzd2426zd2_1974 = CAR(BgL_cdrzd2422zd2_1970);
												if (PAIRP(BgL_carzd2426zd2_1974))
													{	/* Ast/unit.scm 225 */
														BgL_varz00_1908 = CAR(BgL_carzd2426zd2_1974);
														BgL_argsz00_1909 = CDR(BgL_carzd2426zd2_1974);
														BgL_expz00_1910 = CDR(BgL_cdrzd2422zd2_1970);
														{	/* Ast/unit.scm 231 */
															obj_t BgL_idz00_2521;

															BgL_idz00_2521 =
																BGl_idzd2ofzd2idz00zzast_identz00
																(BgL_varz00_1908,
																BGl_findzd2locationzd2zztools_locationz00
																(BgL_sexpz00_39));
															{	/* Ast/unit.scm 231 */
																obj_t BgL_defz00_2522;

																BgL_defz00_2522 =
																	BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																	(BgL_idz00_2521, BgL_gdefsz00_40);
																{	/* Ast/unit.scm 232 */
																	obj_t BgL_globalz00_2523;

																	BgL_globalz00_2523 =
																		BGl_findzd2globalzf2modulez20zzast_envz00
																		(BgL_idz00_2521,
																		BGl_za2moduleza2z00zzmodule_modulez00);
																	{	/* Ast/unit.scm 233 */

																		{	/* Ast/unit.scm 241 */
																			bool_t BgL_test3641z00_6204;

																			if (BGl_typezd2existszf3z21zztype_envz00
																				(BgL_varz00_1908))
																				{	/* Ast/unit.scm 241 */
																					BgL_typez00_bglt BgL_arg2209z00_2528;

																					BgL_arg2209z00_2528 =
																						BGl_findzd2typezd2zztype_envz00
																						(BgL_varz00_1908);
																					{	/* Ast/unit.scm 241 */
																						obj_t BgL_classz00_4133;

																						BgL_classz00_4133 =
																							BGl_tclassz00zzobject_classz00;
																						{	/* Ast/unit.scm 241 */
																							BgL_objectz00_bglt
																								BgL_arg1807z00_4135;
																							{	/* Ast/unit.scm 241 */
																								obj_t BgL_tmpz00_6208;

																								BgL_tmpz00_6208 =
																									((obj_t)
																									((BgL_objectz00_bglt)
																										BgL_arg2209z00_2528));
																								BgL_arg1807z00_4135 =
																									(BgL_objectz00_bglt)
																									(BgL_tmpz00_6208);
																							}
																							if (BGL_CONDEXPAND_ISA_ARCH64())
																								{	/* Ast/unit.scm 241 */
																									long BgL_idxz00_4141;

																									BgL_idxz00_4141 =
																										BGL_OBJECT_INHERITANCE_NUM
																										(BgL_arg1807z00_4135);
																									BgL_test3641z00_6204 =
																										(VECTOR_REF
																										(BGl_za2inheritancesza2z00zz__objectz00,
																											(BgL_idxz00_4141 + 2L)) ==
																										BgL_classz00_4133);
																								}
																							else
																								{	/* Ast/unit.scm 241 */
																									bool_t BgL_res3509z00_4166;

																									{	/* Ast/unit.scm 241 */
																										obj_t BgL_oclassz00_4149;

																										{	/* Ast/unit.scm 241 */
																											obj_t BgL_arg1815z00_4157;
																											long BgL_arg1816z00_4158;

																											BgL_arg1815z00_4157 =
																												(BGl_za2classesza2z00zz__objectz00);
																											{	/* Ast/unit.scm 241 */
																												long
																													BgL_arg1817z00_4159;
																												BgL_arg1817z00_4159 =
																													BGL_OBJECT_CLASS_NUM
																													(BgL_arg1807z00_4135);
																												BgL_arg1816z00_4158 =
																													(BgL_arg1817z00_4159 -
																													OBJECT_TYPE);
																											}
																											BgL_oclassz00_4149 =
																												VECTOR_REF
																												(BgL_arg1815z00_4157,
																												BgL_arg1816z00_4158);
																										}
																										{	/* Ast/unit.scm 241 */
																											bool_t
																												BgL__ortest_1115z00_4150;
																											BgL__ortest_1115z00_4150 =
																												(BgL_classz00_4133 ==
																												BgL_oclassz00_4149);
																											if (BgL__ortest_1115z00_4150)
																												{	/* Ast/unit.scm 241 */
																													BgL_res3509z00_4166 =
																														BgL__ortest_1115z00_4150;
																												}
																											else
																												{	/* Ast/unit.scm 241 */
																													long
																														BgL_odepthz00_4151;
																													{	/* Ast/unit.scm 241 */
																														obj_t
																															BgL_arg1804z00_4152;
																														BgL_arg1804z00_4152
																															=
																															(BgL_oclassz00_4149);
																														BgL_odepthz00_4151 =
																															BGL_CLASS_DEPTH
																															(BgL_arg1804z00_4152);
																													}
																													if (
																														(2L <
																															BgL_odepthz00_4151))
																														{	/* Ast/unit.scm 241 */
																															obj_t
																																BgL_arg1802z00_4154;
																															{	/* Ast/unit.scm 241 */
																																obj_t
																																	BgL_arg1803z00_4155;
																																BgL_arg1803z00_4155
																																	=
																																	(BgL_oclassz00_4149);
																																BgL_arg1802z00_4154
																																	=
																																	BGL_CLASS_ANCESTORS_REF
																																	(BgL_arg1803z00_4155,
																																	2L);
																															}
																															BgL_res3509z00_4166
																																=
																																(BgL_arg1802z00_4154
																																==
																																BgL_classz00_4133);
																														}
																													else
																														{	/* Ast/unit.scm 241 */
																															BgL_res3509z00_4166
																																= ((bool_t) 0);
																														}
																												}
																										}
																									}
																									BgL_test3641z00_6204 =
																										BgL_res3509z00_4166;
																								}
																						}
																					}
																				}
																			else
																				{	/* Ast/unit.scm 241 */
																					BgL_test3641z00_6204 = ((bool_t) 0);
																				}
																			if (BgL_test3641z00_6204)
																				{	/* Ast/unit.scm 241 */
																					BGl_errorzd2classzd2shadowz00zzast_unitz00
																						(BgL_varz00_1908, BgL_sexpz00_39);
																				}
																			else
																				{	/* Ast/unit.scm 241 */
																					BFALSE;
																				}
																		}
																		{	/* Ast/unit.scm 243 */
																			bool_t BgL_test3646z00_6232;

																			if (PAIRP(BgL_defz00_2522))
																				{	/* Ast/unit.scm 243 */
																					if (
																						(CAR(CDR(BgL_defz00_2522)) ==
																							CNST_TABLE_REF(12)))
																						{	/* Ast/unit.scm 245 */
																							bool_t BgL__ortest_1116z00_2548;

																							{	/* Ast/unit.scm 245 */
																								bool_t BgL_test3649z00_6240;

																								{	/* Ast/unit.scm 245 */
																									obj_t BgL_classz00_4169;

																									BgL_classz00_4169 =
																										BGl_globalz00zzast_varz00;
																									if (BGL_OBJECTP
																										(BgL_globalz00_2523))
																										{	/* Ast/unit.scm 245 */
																											BgL_objectz00_bglt
																												BgL_arg1807z00_4171;
																											BgL_arg1807z00_4171 =
																												(BgL_objectz00_bglt)
																												(BgL_globalz00_2523);
																											if (BGL_CONDEXPAND_ISA_ARCH64())
																												{	/* Ast/unit.scm 245 */
																													long BgL_idxz00_4177;

																													BgL_idxz00_4177 =
																														BGL_OBJECT_INHERITANCE_NUM
																														(BgL_arg1807z00_4171);
																													BgL_test3649z00_6240 =
																														(VECTOR_REF
																														(BGl_za2inheritancesza2z00zz__objectz00,
																															(BgL_idxz00_4177 +
																																2L)) ==
																														BgL_classz00_4169);
																												}
																											else
																												{	/* Ast/unit.scm 245 */
																													bool_t
																														BgL_res3510z00_4202;
																													{	/* Ast/unit.scm 245 */
																														obj_t
																															BgL_oclassz00_4185;
																														{	/* Ast/unit.scm 245 */
																															obj_t
																																BgL_arg1815z00_4193;
																															long
																																BgL_arg1816z00_4194;
																															BgL_arg1815z00_4193
																																=
																																(BGl_za2classesza2z00zz__objectz00);
																															{	/* Ast/unit.scm 245 */
																																long
																																	BgL_arg1817z00_4195;
																																BgL_arg1817z00_4195
																																	=
																																	BGL_OBJECT_CLASS_NUM
																																	(BgL_arg1807z00_4171);
																																BgL_arg1816z00_4194
																																	=
																																	(BgL_arg1817z00_4195
																																	-
																																	OBJECT_TYPE);
																															}
																															BgL_oclassz00_4185
																																=
																																VECTOR_REF
																																(BgL_arg1815z00_4193,
																																BgL_arg1816z00_4194);
																														}
																														{	/* Ast/unit.scm 245 */
																															bool_t
																																BgL__ortest_1115z00_4186;
																															BgL__ortest_1115z00_4186
																																=
																																(BgL_classz00_4169
																																==
																																BgL_oclassz00_4185);
																															if (BgL__ortest_1115z00_4186)
																																{	/* Ast/unit.scm 245 */
																																	BgL_res3510z00_4202
																																		=
																																		BgL__ortest_1115z00_4186;
																																}
																															else
																																{	/* Ast/unit.scm 245 */
																																	long
																																		BgL_odepthz00_4187;
																																	{	/* Ast/unit.scm 245 */
																																		obj_t
																																			BgL_arg1804z00_4188;
																																		BgL_arg1804z00_4188
																																			=
																																			(BgL_oclassz00_4185);
																																		BgL_odepthz00_4187
																																			=
																																			BGL_CLASS_DEPTH
																																			(BgL_arg1804z00_4188);
																																	}
																																	if (
																																		(2L <
																																			BgL_odepthz00_4187))
																																		{	/* Ast/unit.scm 245 */
																																			obj_t
																																				BgL_arg1802z00_4190;
																																			{	/* Ast/unit.scm 245 */
																																				obj_t
																																					BgL_arg1803z00_4191;
																																				BgL_arg1803z00_4191
																																					=
																																					(BgL_oclassz00_4185);
																																				BgL_arg1802z00_4190
																																					=
																																					BGL_CLASS_ANCESTORS_REF
																																					(BgL_arg1803z00_4191,
																																					2L);
																																			}
																																			BgL_res3510z00_4202
																																				=
																																				(BgL_arg1802z00_4190
																																				==
																																				BgL_classz00_4169);
																																		}
																																	else
																																		{	/* Ast/unit.scm 245 */
																																			BgL_res3510z00_4202
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																														}
																													}
																													BgL_test3649z00_6240 =
																														BgL_res3510z00_4202;
																												}
																										}
																									else
																										{	/* Ast/unit.scm 245 */
																											BgL_test3649z00_6240 =
																												((bool_t) 0);
																										}
																								}
																								if (BgL_test3649z00_6240)
																									{	/* Ast/unit.scm 245 */
																										BgL__ortest_1116z00_2548 =
																											((bool_t) 0);
																									}
																								else
																									{	/* Ast/unit.scm 245 */
																										BgL__ortest_1116z00_2548 =
																											((bool_t) 1);
																									}
																							}
																							if (BgL__ortest_1116z00_2548)
																								{	/* Ast/unit.scm 245 */
																									BgL_test3646z00_6232 =
																										BgL__ortest_1116z00_2548;
																								}
																							else
																								{	/* Ast/unit.scm 245 */
																									BgL_test3646z00_6232 =
																										(
																										(((BgL_variablez00_bglt)
																												COBJECT((
																														(BgL_variablez00_bglt)
																														((BgL_globalz00_bglt) BgL_globalz00_2523))))->BgL_accessz00) == CNST_TABLE_REF(12));
																								}
																						}
																					else
																						{	/* Ast/unit.scm 244 */
																							BgL_test3646z00_6232 =
																								((bool_t) 0);
																						}
																				}
																			else
																				{	/* Ast/unit.scm 243 */
																					BgL_test3646z00_6232 = ((bool_t) 1);
																				}
																			if (BgL_test3646z00_6232)
																				{	/* Ast/unit.scm 249 */
																					obj_t BgL_arg2216z00_2537;

																					{	/* Ast/unit.scm 249 */
																						obj_t BgL_arg2217z00_2538;

																						{	/* Ast/unit.scm 249 */
																							obj_t BgL_arg2218z00_2539;

																							{	/* Ast/unit.scm 249 */
																								obj_t BgL_pairz00_4207;

																								BgL_pairz00_4207 =
																									CDR(((obj_t) BgL_sexpz00_39));
																								BgL_arg2218z00_2539 =
																									CDR(BgL_pairz00_4207);
																							}
																							BgL_arg2217z00_2538 =
																								BGl_findzd2locationzd2zztools_locationz00
																								(BgL_arg2218z00_2539);
																						}
																						BgL_arg2216z00_2537 =
																							BGl_normaliza7ezd2prognzf2errorz87zzast_unitz00
																							(BgL_expz00_1910, BgL_sexpz00_39,
																							BgL_arg2217z00_2538);
																					}
																					return
																						BGl_makezd2sfunzd2definitionz00zzast_unitz00
																						(BgL_varz00_1908,
																						BGl_za2moduleza2z00zzmodule_modulez00,
																						BgL_argsz00_1909,
																						BgL_arg2216z00_2537, BgL_sexpz00_39,
																						CNST_TABLE_REF(13));
																				}
																			else
																				{	/* Ast/unit.scm 251 */
																					obj_t BgL_newzd2sexpzd2_2540;

																					{	/* Ast/unit.scm 251 */
																						obj_t BgL_arg2219z00_2541;

																						{	/* Ast/unit.scm 251 */
																							obj_t BgL_arg2220z00_2542;

																							{	/* Ast/unit.scm 251 */
																								obj_t BgL_arg2221z00_2543;

																								{	/* Ast/unit.scm 251 */
																									obj_t BgL_arg2222z00_2544;

																									BgL_arg2222z00_2544 =
																										MAKE_YOUNG_PAIR
																										(BgL_argsz00_1909,
																										BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																										(BgL_expz00_1910, BNIL));
																									BgL_arg2221z00_2543 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(11),
																										BgL_arg2222z00_2544);
																								}
																								BgL_arg2220z00_2542 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg2221z00_2543, BNIL);
																							}
																							BgL_arg2219z00_2541 =
																								MAKE_YOUNG_PAIR(BgL_varz00_1908,
																								BgL_arg2220z00_2542);
																						}
																						BgL_newzd2sexpzd2_2540 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF(1),
																							BgL_arg2219z00_2541);
																					}
																					BGl_replacez12z12zztools_miscz00
																						(BgL_sexpz00_39,
																						BgL_newzd2sexpzd2_2540);
																					BGL_TAIL return
																						BGl_makezd2svarzd2definitionz00zzast_unitz00
																						(BgL_varz00_1908, BgL_sexpz00_39);
																				}
																		}
																	}
																}
															}
														}
													}
												else
													{	/* Ast/unit.scm 225 */
														obj_t BgL_cdrzd2452zd2_1980;

														BgL_cdrzd2452zd2_1980 =
															CDR(((obj_t) BgL_cdrzd2422zd2_1970));
														if (PAIRP(BgL_cdrzd2452zd2_1980))
															{	/* Ast/unit.scm 225 */
																obj_t BgL_carzd2456zd2_1982;

																BgL_carzd2456zd2_1982 =
																	CAR(BgL_cdrzd2452zd2_1980);
																if (PAIRP(BgL_carzd2456zd2_1982))
																	{	/* Ast/unit.scm 225 */
																		obj_t BgL_cdrzd2461zd2_1984;

																		BgL_cdrzd2461zd2_1984 =
																			CDR(BgL_carzd2456zd2_1982);
																		if (
																			(CAR(BgL_carzd2456zd2_1982) ==
																				CNST_TABLE_REF(11)))
																			{	/* Ast/unit.scm 225 */
																				if (PAIRP(BgL_cdrzd2461zd2_1984))
																					{	/* Ast/unit.scm 225 */
																						if (NULLP(CDR
																								(BgL_cdrzd2452zd2_1980)))
																							{	/* Ast/unit.scm 225 */
																								obj_t BgL_arg1822z00_1990;
																								obj_t BgL_arg1823z00_1991;
																								obj_t BgL_arg1831z00_1992;

																								BgL_arg1822z00_1990 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd2422zd2_1970));
																								BgL_arg1823z00_1991 =
																									CAR(BgL_cdrzd2461zd2_1984);
																								BgL_arg1831z00_1992 =
																									CDR(BgL_cdrzd2461zd2_1984);
																								BgL_varz00_1912 =
																									BgL_arg1822z00_1990;
																								BgL_argsz00_1913 =
																									BgL_arg1823z00_1991;
																								BgL_expz00_1914 =
																									BgL_arg1831z00_1992;
																								{	/* Ast/unit.scm 255 */
																									bool_t BgL_test3660z00_6310;

																									if (BGl_typezd2existszf3z21zztype_envz00(BgL_varz00_1912))
																										{	/* Ast/unit.scm 255 */
																											BgL_typez00_bglt
																												BgL_arg2231z00_2558;
																											BgL_arg2231z00_2558 =
																												BGl_findzd2typezd2zztype_envz00
																												(BgL_varz00_1912);
																											{	/* Ast/unit.scm 255 */
																												obj_t BgL_classz00_4208;

																												BgL_classz00_4208 =
																													BGl_tclassz00zzobject_classz00;
																												{	/* Ast/unit.scm 255 */
																													BgL_objectz00_bglt
																														BgL_arg1807z00_4210;
																													{	/* Ast/unit.scm 255 */
																														obj_t
																															BgL_tmpz00_6314;
																														BgL_tmpz00_6314 =
																															((obj_t) (
																																(BgL_objectz00_bglt)
																																BgL_arg2231z00_2558));
																														BgL_arg1807z00_4210
																															=
																															(BgL_objectz00_bglt)
																															(BgL_tmpz00_6314);
																													}
																													if (BGL_CONDEXPAND_ISA_ARCH64())
																														{	/* Ast/unit.scm 255 */
																															long
																																BgL_idxz00_4216;
																															BgL_idxz00_4216 =
																																BGL_OBJECT_INHERITANCE_NUM
																																(BgL_arg1807z00_4210);
																															BgL_test3660z00_6310
																																=
																																(VECTOR_REF
																																(BGl_za2inheritancesza2z00zz__objectz00,
																																	(BgL_idxz00_4216
																																		+ 2L)) ==
																																BgL_classz00_4208);
																														}
																													else
																														{	/* Ast/unit.scm 255 */
																															bool_t
																																BgL_res3511z00_4241;
																															{	/* Ast/unit.scm 255 */
																																obj_t
																																	BgL_oclassz00_4224;
																																{	/* Ast/unit.scm 255 */
																																	obj_t
																																		BgL_arg1815z00_4232;
																																	long
																																		BgL_arg1816z00_4233;
																																	BgL_arg1815z00_4232
																																		=
																																		(BGl_za2classesza2z00zz__objectz00);
																																	{	/* Ast/unit.scm 255 */
																																		long
																																			BgL_arg1817z00_4234;
																																		BgL_arg1817z00_4234
																																			=
																																			BGL_OBJECT_CLASS_NUM
																																			(BgL_arg1807z00_4210);
																																		BgL_arg1816z00_4233
																																			=
																																			(BgL_arg1817z00_4234
																																			-
																																			OBJECT_TYPE);
																																	}
																																	BgL_oclassz00_4224
																																		=
																																		VECTOR_REF
																																		(BgL_arg1815z00_4232,
																																		BgL_arg1816z00_4233);
																																}
																																{	/* Ast/unit.scm 255 */
																																	bool_t
																																		BgL__ortest_1115z00_4225;
																																	BgL__ortest_1115z00_4225
																																		=
																																		(BgL_classz00_4208
																																		==
																																		BgL_oclassz00_4224);
																																	if (BgL__ortest_1115z00_4225)
																																		{	/* Ast/unit.scm 255 */
																																			BgL_res3511z00_4241
																																				=
																																				BgL__ortest_1115z00_4225;
																																		}
																																	else
																																		{	/* Ast/unit.scm 255 */
																																			long
																																				BgL_odepthz00_4226;
																																			{	/* Ast/unit.scm 255 */
																																				obj_t
																																					BgL_arg1804z00_4227;
																																				BgL_arg1804z00_4227
																																					=
																																					(BgL_oclassz00_4224);
																																				BgL_odepthz00_4226
																																					=
																																					BGL_CLASS_DEPTH
																																					(BgL_arg1804z00_4227);
																																			}
																																			if (
																																				(2L <
																																					BgL_odepthz00_4226))
																																				{	/* Ast/unit.scm 255 */
																																					obj_t
																																						BgL_arg1802z00_4229;
																																					{	/* Ast/unit.scm 255 */
																																						obj_t
																																							BgL_arg1803z00_4230;
																																						BgL_arg1803z00_4230
																																							=
																																							(BgL_oclassz00_4224);
																																						BgL_arg1802z00_4229
																																							=
																																							BGL_CLASS_ANCESTORS_REF
																																							(BgL_arg1803z00_4230,
																																							2L);
																																					}
																																					BgL_res3511z00_4241
																																						=
																																						(BgL_arg1802z00_4229
																																						==
																																						BgL_classz00_4208);
																																				}
																																			else
																																				{	/* Ast/unit.scm 255 */
																																					BgL_res3511z00_4241
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																}
																															}
																															BgL_test3660z00_6310
																																=
																																BgL_res3511z00_4241;
																														}
																												}
																											}
																										}
																									else
																										{	/* Ast/unit.scm 255 */
																											BgL_test3660z00_6310 =
																												((bool_t) 0);
																										}
																									if (BgL_test3660z00_6310)
																										{	/* Ast/unit.scm 255 */
																											BGl_errorzd2classzd2shadowz00zzast_unitz00
																												(BgL_varz00_1912,
																												BgL_sexpz00_39);
																										}
																									else
																										{	/* Ast/unit.scm 255 */
																											BFALSE;
																										}
																								}
																								{	/* Ast/unit.scm 257 */
																									obj_t BgL_idz00_2559;

																									BgL_idz00_2559 =
																										BGl_idzd2ofzd2idz00zzast_identz00
																										(BgL_varz00_1912,
																										BGl_findzd2locationzd2zztools_locationz00
																										(BgL_sexpz00_39));
																									{	/* Ast/unit.scm 257 */
																										obj_t BgL_defz00_2560;

																										BgL_defz00_2560 =
																											BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																											(BgL_idz00_2559,
																											BgL_gdefsz00_40);
																										{	/* Ast/unit.scm 258 */
																											obj_t BgL_globalz00_2561;

																											BgL_globalz00_2561 =
																												BGl_findzd2globalzf2modulez20zzast_envz00
																												(BgL_idz00_2559,
																												BGl_za2moduleza2z00zzmodule_modulez00);
																											{	/* Ast/unit.scm 259 */

																												{	/* Ast/unit.scm 261 */
																													bool_t
																														BgL_test3665z00_6342;
																													{	/* Ast/unit.scm 261 */
																														bool_t
																															BgL_test3666z00_6343;
																														{	/* Ast/unit.scm 261 */
																															obj_t
																																BgL_tmpz00_6344;
																															{	/* Ast/unit.scm 261 */
																																obj_t
																																	BgL_pairz00_4243;
																																BgL_pairz00_4243
																																	=
																																	CDR(((obj_t)
																																		BgL_defz00_2560));
																																BgL_tmpz00_6344
																																	=
																																	CAR
																																	(BgL_pairz00_4243);
																															}
																															BgL_test3666z00_6343
																																=
																																(BgL_tmpz00_6344
																																==
																																CNST_TABLE_REF
																																(12));
																														}
																														if (BgL_test3666z00_6343)
																															{	/* Ast/unit.scm 262 */
																																bool_t
																																	BgL__ortest_1118z00_2575;
																																{	/* Ast/unit.scm 262 */
																																	bool_t
																																		BgL_test3667z00_6350;
																																	{	/* Ast/unit.scm 262 */
																																		obj_t
																																			BgL_classz00_4244;
																																		BgL_classz00_4244
																																			=
																																			BGl_globalz00zzast_varz00;
																																		if (BGL_OBJECTP(BgL_globalz00_2561))
																																			{	/* Ast/unit.scm 262 */
																																				BgL_objectz00_bglt
																																					BgL_arg1807z00_4246;
																																				BgL_arg1807z00_4246
																																					=
																																					(BgL_objectz00_bglt)
																																					(BgL_globalz00_2561);
																																				if (BGL_CONDEXPAND_ISA_ARCH64())
																																					{	/* Ast/unit.scm 262 */
																																						long
																																							BgL_idxz00_4252;
																																						BgL_idxz00_4252
																																							=
																																							BGL_OBJECT_INHERITANCE_NUM
																																							(BgL_arg1807z00_4246);
																																						BgL_test3667z00_6350
																																							=
																																							(VECTOR_REF
																																							(BGl_za2inheritancesza2z00zz__objectz00,
																																								(BgL_idxz00_4252
																																									+
																																									2L))
																																							==
																																							BgL_classz00_4244);
																																					}
																																				else
																																					{	/* Ast/unit.scm 262 */
																																						bool_t
																																							BgL_res3512z00_4277;
																																						{	/* Ast/unit.scm 262 */
																																							obj_t
																																								BgL_oclassz00_4260;
																																							{	/* Ast/unit.scm 262 */
																																								obj_t
																																									BgL_arg1815z00_4268;
																																								long
																																									BgL_arg1816z00_4269;
																																								BgL_arg1815z00_4268
																																									=
																																									(BGl_za2classesza2z00zz__objectz00);
																																								{	/* Ast/unit.scm 262 */
																																									long
																																										BgL_arg1817z00_4270;
																																									BgL_arg1817z00_4270
																																										=
																																										BGL_OBJECT_CLASS_NUM
																																										(BgL_arg1807z00_4246);
																																									BgL_arg1816z00_4269
																																										=
																																										(BgL_arg1817z00_4270
																																										-
																																										OBJECT_TYPE);
																																								}
																																								BgL_oclassz00_4260
																																									=
																																									VECTOR_REF
																																									(BgL_arg1815z00_4268,
																																									BgL_arg1816z00_4269);
																																							}
																																							{	/* Ast/unit.scm 262 */
																																								bool_t
																																									BgL__ortest_1115z00_4261;
																																								BgL__ortest_1115z00_4261
																																									=
																																									(BgL_classz00_4244
																																									==
																																									BgL_oclassz00_4260);
																																								if (BgL__ortest_1115z00_4261)
																																									{	/* Ast/unit.scm 262 */
																																										BgL_res3512z00_4277
																																											=
																																											BgL__ortest_1115z00_4261;
																																									}
																																								else
																																									{	/* Ast/unit.scm 262 */
																																										long
																																											BgL_odepthz00_4262;
																																										{	/* Ast/unit.scm 262 */
																																											obj_t
																																												BgL_arg1804z00_4263;
																																											BgL_arg1804z00_4263
																																												=
																																												(BgL_oclassz00_4260);
																																											BgL_odepthz00_4262
																																												=
																																												BGL_CLASS_DEPTH
																																												(BgL_arg1804z00_4263);
																																										}
																																										if ((2L < BgL_odepthz00_4262))
																																											{	/* Ast/unit.scm 262 */
																																												obj_t
																																													BgL_arg1802z00_4265;
																																												{	/* Ast/unit.scm 262 */
																																													obj_t
																																														BgL_arg1803z00_4266;
																																													BgL_arg1803z00_4266
																																														=
																																														(BgL_oclassz00_4260);
																																													BgL_arg1802z00_4265
																																														=
																																														BGL_CLASS_ANCESTORS_REF
																																														(BgL_arg1803z00_4266,
																																														2L);
																																												}
																																												BgL_res3512z00_4277
																																													=
																																													(BgL_arg1802z00_4265
																																													==
																																													BgL_classz00_4244);
																																											}
																																										else
																																											{	/* Ast/unit.scm 262 */
																																												BgL_res3512z00_4277
																																													=
																																													(
																																													(bool_t)
																																													0);
																																											}
																																									}
																																							}
																																						}
																																						BgL_test3667z00_6350
																																							=
																																							BgL_res3512z00_4277;
																																					}
																																			}
																																		else
																																			{	/* Ast/unit.scm 262 */
																																				BgL_test3667z00_6350
																																					=
																																					(
																																					(bool_t)
																																					0);
																																			}
																																	}
																																	if (BgL_test3667z00_6350)
																																		{	/* Ast/unit.scm 262 */
																																			BgL__ortest_1118z00_2575
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																	else
																																		{	/* Ast/unit.scm 262 */
																																			BgL__ortest_1118z00_2575
																																				=
																																				(
																																				(bool_t)
																																				1);
																																		}
																																}
																																if (BgL__ortest_1118z00_2575)
																																	{	/* Ast/unit.scm 262 */
																																		BgL_test3665z00_6342
																																			=
																																			BgL__ortest_1118z00_2575;
																																	}
																																else
																																	{	/* Ast/unit.scm 262 */
																																		BgL_test3665z00_6342
																																			=
																																			((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt) BgL_globalz00_2561))))->BgL_accessz00) == CNST_TABLE_REF(12));
																																	}
																															}
																														else
																															{	/* Ast/unit.scm 261 */
																																BgL_test3665z00_6342
																																	=
																																	((bool_t) 0);
																															}
																													}
																													if (BgL_test3665z00_6342)
																														{	/* Ast/unit.scm 266 */
																															obj_t
																																BgL_arg2240z00_2571;
																															{	/* Ast/unit.scm 266 */
																																obj_t
																																	BgL_arg2241z00_2572;
																																{	/* Ast/unit.scm 266 */
																																	obj_t
																																		BgL_arg2242z00_2573;
																																	{	/* Ast/unit.scm 266 */
																																		obj_t
																																			BgL_pairz00_4282;
																																		BgL_pairz00_4282
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_sexpz00_39));
																																		BgL_arg2242z00_2573
																																			=
																																			CDR
																																			(BgL_pairz00_4282);
																																	}
																																	BgL_arg2241z00_2572
																																		=
																																		BGl_findzd2locationzd2zztools_locationz00
																																		(BgL_arg2242z00_2573);
																																}
																																BgL_arg2240z00_2571
																																	=
																																	BGl_normaliza7ezd2prognzf2errorz87zzast_unitz00
																																	(BgL_expz00_1914,
																																	BgL_sexpz00_39,
																																	BgL_arg2241z00_2572);
																															}
																															return
																																BGl_makezd2sfunzd2definitionz00zzast_unitz00
																																(BgL_varz00_1912,
																																BGl_za2moduleza2z00zzmodule_modulez00,
																																BgL_argsz00_1913,
																																BgL_arg2240z00_2571,
																																BgL_sexpz00_39,
																																CNST_TABLE_REF
																																(13));
																														}
																													else
																														{	/* Ast/unit.scm 261 */
																															BGL_TAIL return
																																BGl_makezd2svarzd2definitionz00zzast_unitz00
																																(BgL_varz00_1912,
																																BgL_sexpz00_39);
																														}
																												}
																											}
																										}
																									}
																								}
																							}
																						else
																							{	/* Ast/unit.scm 225 */
																								obj_t BgL_cdrzd2529zd2_1993;

																								BgL_cdrzd2529zd2_1993 =
																									CDR(((obj_t) BgL_sexpz00_39));
																								{	/* Ast/unit.scm 225 */
																									obj_t BgL_arg1832z00_1994;
																									obj_t BgL_arg1833z00_1995;

																									BgL_arg1832z00_1994 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd2529zd2_1993));
																									BgL_arg1833z00_1995 =
																										CDR(((obj_t)
																											BgL_cdrzd2529zd2_1993));
																									BgL_varz00_1936 =
																										BgL_arg1832z00_1994;
																									BgL_expz00_1937 =
																										BgL_arg1833z00_1995;
																								BgL_tagzd2368zd2_1938:
																									{	/* Ast/unit.scm 359 */
																										bool_t BgL_test3673z00_6393;

																										if (BGl_typezd2existszf3z21zztype_envz00(BgL_varz00_1936))
																											{	/* Ast/unit.scm 359 */
																												BgL_typez00_bglt
																													BgL_arg2364z00_2746;
																												BgL_arg2364z00_2746 =
																													BGl_findzd2typezd2zztype_envz00
																													(BgL_varz00_1936);
																												{	/* Ast/unit.scm 359 */
																													obj_t
																														BgL_classz00_4685;
																													BgL_classz00_4685 =
																														BGl_tclassz00zzobject_classz00;
																													{	/* Ast/unit.scm 359 */
																														BgL_objectz00_bglt
																															BgL_arg1807z00_4687;
																														{	/* Ast/unit.scm 359 */
																															obj_t
																																BgL_tmpz00_6397;
																															BgL_tmpz00_6397 =
																																((obj_t) (
																																	(BgL_objectz00_bglt)
																																	BgL_arg2364z00_2746));
																															BgL_arg1807z00_4687
																																=
																																(BgL_objectz00_bglt)
																																(BgL_tmpz00_6397);
																														}
																														if (BGL_CONDEXPAND_ISA_ARCH64())
																															{	/* Ast/unit.scm 359 */
																																long
																																	BgL_idxz00_4693;
																																BgL_idxz00_4693
																																	=
																																	BGL_OBJECT_INHERITANCE_NUM
																																	(BgL_arg1807z00_4687);
																																BgL_test3673z00_6393
																																	=
																																	(VECTOR_REF
																																	(BGl_za2inheritancesza2z00zz__objectz00,
																																		(BgL_idxz00_4693
																																			+ 2L)) ==
																																	BgL_classz00_4685);
																															}
																														else
																															{	/* Ast/unit.scm 359 */
																																bool_t
																																	BgL_res3524z00_4718;
																																{	/* Ast/unit.scm 359 */
																																	obj_t
																																		BgL_oclassz00_4701;
																																	{	/* Ast/unit.scm 359 */
																																		obj_t
																																			BgL_arg1815z00_4709;
																																		long
																																			BgL_arg1816z00_4710;
																																		BgL_arg1815z00_4709
																																			=
																																			(BGl_za2classesza2z00zz__objectz00);
																																		{	/* Ast/unit.scm 359 */
																																			long
																																				BgL_arg1817z00_4711;
																																			BgL_arg1817z00_4711
																																				=
																																				BGL_OBJECT_CLASS_NUM
																																				(BgL_arg1807z00_4687);
																																			BgL_arg1816z00_4710
																																				=
																																				(BgL_arg1817z00_4711
																																				-
																																				OBJECT_TYPE);
																																		}
																																		BgL_oclassz00_4701
																																			=
																																			VECTOR_REF
																																			(BgL_arg1815z00_4709,
																																			BgL_arg1816z00_4710);
																																	}
																																	{	/* Ast/unit.scm 359 */
																																		bool_t
																																			BgL__ortest_1115z00_4702;
																																		BgL__ortest_1115z00_4702
																																			=
																																			(BgL_classz00_4685
																																			==
																																			BgL_oclassz00_4701);
																																		if (BgL__ortest_1115z00_4702)
																																			{	/* Ast/unit.scm 359 */
																																				BgL_res3524z00_4718
																																					=
																																					BgL__ortest_1115z00_4702;
																																			}
																																		else
																																			{	/* Ast/unit.scm 359 */
																																				long
																																					BgL_odepthz00_4703;
																																				{	/* Ast/unit.scm 359 */
																																					obj_t
																																						BgL_arg1804z00_4704;
																																					BgL_arg1804z00_4704
																																						=
																																						(BgL_oclassz00_4701);
																																					BgL_odepthz00_4703
																																						=
																																						BGL_CLASS_DEPTH
																																						(BgL_arg1804z00_4704);
																																				}
																																				if (
																																					(2L <
																																						BgL_odepthz00_4703))
																																					{	/* Ast/unit.scm 359 */
																																						obj_t
																																							BgL_arg1802z00_4706;
																																						{	/* Ast/unit.scm 359 */
																																							obj_t
																																								BgL_arg1803z00_4707;
																																							BgL_arg1803z00_4707
																																								=
																																								(BgL_oclassz00_4701);
																																							BgL_arg1802z00_4706
																																								=
																																								BGL_CLASS_ANCESTORS_REF
																																								(BgL_arg1803z00_4707,
																																								2L);
																																						}
																																						BgL_res3524z00_4718
																																							=
																																							(BgL_arg1802z00_4706
																																							==
																																							BgL_classz00_4685);
																																					}
																																				else
																																					{	/* Ast/unit.scm 359 */
																																						BgL_res3524z00_4718
																																							=
																																							(
																																							(bool_t)
																																							0);
																																					}
																																			}
																																	}
																																}
																																BgL_test3673z00_6393
																																	=
																																	BgL_res3524z00_4718;
																															}
																													}
																												}
																											}
																										else
																											{	/* Ast/unit.scm 359 */
																												BgL_test3673z00_6393 =
																													((bool_t) 0);
																											}
																										if (BgL_test3673z00_6393)
																											{	/* Ast/unit.scm 360 */
																												bool_t
																													BgL_test3678z00_6420;
																												if (PAIRP
																													(BgL_expz00_1937))
																													{	/* Ast/unit.scm 360 */
																														obj_t
																															BgL_carzd23472zd2_2727;
																														BgL_carzd23472zd2_2727
																															=
																															CAR(((obj_t)
																																BgL_expz00_1937));
																														if (PAIRP
																															(BgL_carzd23472zd2_2727))
																															{	/* Ast/unit.scm 360 */
																																obj_t
																																	BgL_carzd23474zd2_2729;
																																BgL_carzd23474zd2_2729
																																	=
																																	CAR
																																	(BgL_carzd23472zd2_2727);
																																if (PAIRP
																																	(BgL_carzd23474zd2_2729))
																																	{	/* Ast/unit.scm 360 */
																																		obj_t
																																			BgL_cdrzd23477zd2_2731;
																																		BgL_cdrzd23477zd2_2731
																																			=
																																			CDR
																																			(BgL_carzd23474zd2_2729);
																																		if ((CAR
																																				(BgL_carzd23474zd2_2729)
																																				==
																																				CNST_TABLE_REF
																																				(0)))
																																			{	/* Ast/unit.scm 360 */
																																				if (PAIRP(BgL_cdrzd23477zd2_2731))
																																					{	/* Ast/unit.scm 360 */
																																						obj_t
																																							BgL_cdrzd23479zd2_2734;
																																						BgL_cdrzd23479zd2_2734
																																							=
																																							CDR
																																							(BgL_cdrzd23477zd2_2731);
																																						if (
																																							(CAR
																																								(BgL_cdrzd23477zd2_2731)
																																								==
																																								CNST_TABLE_REF
																																								(14)))
																																							{	/* Ast/unit.scm 360 */
																																								if (PAIRP(BgL_cdrzd23479zd2_2734))
																																									{	/* Ast/unit.scm 360 */
																																										if ((CAR(BgL_cdrzd23479zd2_2734) == CNST_TABLE_REF(15)))
																																											{	/* Ast/unit.scm 360 */
																																												if (NULLP(CDR(BgL_cdrzd23479zd2_2734)))
																																													{	/* Ast/unit.scm 360 */
																																														BgL_test3678z00_6420
																																															=
																																															NULLP
																																															(CDR
																																															(((obj_t) BgL_expz00_1937)));
																																													}
																																												else
																																													{	/* Ast/unit.scm 360 */
																																														BgL_test3678z00_6420
																																															=
																																															(
																																															(bool_t)
																																															0);
																																													}
																																											}
																																										else
																																											{	/* Ast/unit.scm 360 */
																																												BgL_test3678z00_6420
																																													=
																																													(
																																													(bool_t)
																																													0);
																																											}
																																									}
																																								else
																																									{	/* Ast/unit.scm 360 */
																																										BgL_test3678z00_6420
																																											=
																																											(
																																											(bool_t)
																																											0);
																																									}
																																							}
																																						else
																																							{	/* Ast/unit.scm 360 */
																																								BgL_test3678z00_6420
																																									=
																																									(
																																									(bool_t)
																																									0);
																																							}
																																					}
																																				else
																																					{	/* Ast/unit.scm 360 */
																																						BgL_test3678z00_6420
																																							=
																																							(
																																							(bool_t)
																																							0);
																																					}
																																			}
																																		else
																																			{	/* Ast/unit.scm 360 */
																																				BgL_test3678z00_6420
																																					=
																																					(
																																					(bool_t)
																																					0);
																																			}
																																	}
																																else
																																	{	/* Ast/unit.scm 360 */
																																		BgL_test3678z00_6420
																																			=
																																			((bool_t)
																																			0);
																																	}
																															}
																														else
																															{	/* Ast/unit.scm 360 */
																																BgL_test3678z00_6420
																																	=
																																	((bool_t) 0);
																															}
																													}
																												else
																													{	/* Ast/unit.scm 360 */
																														BgL_test3678z00_6420
																															= ((bool_t) 0);
																													}
																												if (BgL_test3678z00_6420)
																													{	/* Ast/unit.scm 360 */
																														BFALSE;
																													}
																												else
																													{	/* Ast/unit.scm 360 */
																														BGl_errorzd2classzd2shadowz00zzast_unitz00
																															(BgL_varz00_1936,
																															BgL_sexpz00_39);
																													}
																											}
																										else
																											{	/* Ast/unit.scm 359 */
																												BFALSE;
																											}
																									}
																									BGL_TAIL return
																										BGl_makezd2svarzd2definitionz00zzast_unitz00
																										(BgL_varz00_1936,
																										BgL_sexpz00_39);
																								}
																							}
																					}
																				else
																					{	/* Ast/unit.scm 225 */
																						obj_t BgL_cdrzd2581zd2_1997;

																						BgL_cdrzd2581zd2_1997 =
																							CDR(((obj_t) BgL_sexpz00_39));
																						{	/* Ast/unit.scm 225 */
																							obj_t BgL_cdrzd2586zd2_1998;

																							BgL_cdrzd2586zd2_1998 =
																								CDR(
																								((obj_t)
																									BgL_cdrzd2581zd2_1997));
																							{	/* Ast/unit.scm 225 */
																								obj_t BgL_carzd2590zd2_1999;

																								BgL_carzd2590zd2_1999 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd2586zd2_1998));
																								if (SYMBOLP
																									(BgL_carzd2590zd2_1999))
																									{	/* Ast/unit.scm 225 */
																										if (NULLP(CDR(
																													((obj_t)
																														BgL_cdrzd2586zd2_1998))))
																											{	/* Ast/unit.scm 225 */
																												obj_t
																													BgL_arg1838z00_2003;
																												BgL_arg1838z00_2003 =
																													CAR(((obj_t)
																														BgL_cdrzd2581zd2_1997));
																												BgL_varz00_1929 =
																													BgL_arg1838z00_2003;
																												BgL_var2z00_1930 =
																													BgL_carzd2590zd2_1999;
																											BgL_tagzd2366zd2_1931:
																												{	/* Ast/unit.scm 307 */
																													bool_t
																														BgL_test3690z00_6470;
																													if (BGl_typezd2existszf3z21zztype_envz00(BgL_varz00_1929))
																														{	/* Ast/unit.scm 307 */
																															BgL_typez00_bglt
																																BgL_arg2299z00_2650;
																															BgL_arg2299z00_2650
																																=
																																BGl_findzd2typezd2zztype_envz00
																																(BgL_varz00_1929);
																															{	/* Ast/unit.scm 307 */
																																obj_t
																																	BgL_classz00_4473;
																																BgL_classz00_4473
																																	=
																																	BGl_tclassz00zzobject_classz00;
																																{	/* Ast/unit.scm 307 */
																																	BgL_objectz00_bglt
																																		BgL_arg1807z00_4475;
																																	{	/* Ast/unit.scm 307 */
																																		obj_t
																																			BgL_tmpz00_6474;
																																		BgL_tmpz00_6474
																																			=
																																			((obj_t) (
																																				(BgL_objectz00_bglt)
																																				BgL_arg2299z00_2650));
																																		BgL_arg1807z00_4475
																																			=
																																			(BgL_objectz00_bglt)
																																			(BgL_tmpz00_6474);
																																	}
																																	if (BGL_CONDEXPAND_ISA_ARCH64())
																																		{	/* Ast/unit.scm 307 */
																																			long
																																				BgL_idxz00_4481;
																																			BgL_idxz00_4481
																																				=
																																				BGL_OBJECT_INHERITANCE_NUM
																																				(BgL_arg1807z00_4475);
																																			BgL_test3690z00_6470
																																				=
																																				(VECTOR_REF
																																				(BGl_za2inheritancesza2z00zz__objectz00,
																																					(BgL_idxz00_4481
																																						+
																																						2L))
																																				==
																																				BgL_classz00_4473);
																																		}
																																	else
																																		{	/* Ast/unit.scm 307 */
																																			bool_t
																																				BgL_res3518z00_4506;
																																			{	/* Ast/unit.scm 307 */
																																				obj_t
																																					BgL_oclassz00_4489;
																																				{	/* Ast/unit.scm 307 */
																																					obj_t
																																						BgL_arg1815z00_4497;
																																					long
																																						BgL_arg1816z00_4498;
																																					BgL_arg1815z00_4497
																																						=
																																						(BGl_za2classesza2z00zz__objectz00);
																																					{	/* Ast/unit.scm 307 */
																																						long
																																							BgL_arg1817z00_4499;
																																						BgL_arg1817z00_4499
																																							=
																																							BGL_OBJECT_CLASS_NUM
																																							(BgL_arg1807z00_4475);
																																						BgL_arg1816z00_4498
																																							=
																																							(BgL_arg1817z00_4499
																																							-
																																							OBJECT_TYPE);
																																					}
																																					BgL_oclassz00_4489
																																						=
																																						VECTOR_REF
																																						(BgL_arg1815z00_4497,
																																						BgL_arg1816z00_4498);
																																				}
																																				{	/* Ast/unit.scm 307 */
																																					bool_t
																																						BgL__ortest_1115z00_4490;
																																					BgL__ortest_1115z00_4490
																																						=
																																						(BgL_classz00_4473
																																						==
																																						BgL_oclassz00_4489);
																																					if (BgL__ortest_1115z00_4490)
																																						{	/* Ast/unit.scm 307 */
																																							BgL_res3518z00_4506
																																								=
																																								BgL__ortest_1115z00_4490;
																																						}
																																					else
																																						{	/* Ast/unit.scm 307 */
																																							long
																																								BgL_odepthz00_4491;
																																							{	/* Ast/unit.scm 307 */
																																								obj_t
																																									BgL_arg1804z00_4492;
																																								BgL_arg1804z00_4492
																																									=
																																									(BgL_oclassz00_4489);
																																								BgL_odepthz00_4491
																																									=
																																									BGL_CLASS_DEPTH
																																									(BgL_arg1804z00_4492);
																																							}
																																							if ((2L < BgL_odepthz00_4491))
																																								{	/* Ast/unit.scm 307 */
																																									obj_t
																																										BgL_arg1802z00_4494;
																																									{	/* Ast/unit.scm 307 */
																																										obj_t
																																											BgL_arg1803z00_4495;
																																										BgL_arg1803z00_4495
																																											=
																																											(BgL_oclassz00_4489);
																																										BgL_arg1802z00_4494
																																											=
																																											BGL_CLASS_ANCESTORS_REF
																																											(BgL_arg1803z00_4495,
																																											2L);
																																									}
																																									BgL_res3518z00_4506
																																										=
																																										(BgL_arg1802z00_4494
																																										==
																																										BgL_classz00_4473);
																																								}
																																							else
																																								{	/* Ast/unit.scm 307 */
																																									BgL_res3518z00_4506
																																										=
																																										(
																																										(bool_t)
																																										0);
																																								}
																																						}
																																				}
																																			}
																																			BgL_test3690z00_6470
																																				=
																																				BgL_res3518z00_4506;
																																		}
																																}
																															}
																														}
																													else
																														{	/* Ast/unit.scm 307 */
																															BgL_test3690z00_6470
																																= ((bool_t) 0);
																														}
																													if (BgL_test3690z00_6470)
																														{	/* Ast/unit.scm 307 */
																															BGl_errorzd2classzd2shadowz00zzast_unitz00
																																(BgL_varz00_1929,
																																BgL_sexpz00_39);
																														}
																													else
																														{	/* Ast/unit.scm 307 */
																															BFALSE;
																														}
																												}
																												{	/* Ast/unit.scm 309 */
																													obj_t BgL_defz00_2651;

																													BgL_defz00_2651 =
																														BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																														(BGl_idzd2ofzd2idz00zzast_identz00
																														(BgL_varz00_1929,
																															BGl_findzd2locationzd2zztools_locationz00
																															(BgL_sexpz00_39)),
																														BgL_gdefsz00_40);
																													{	/* Ast/unit.scm 310 */
																														bool_t
																															BgL_test3695z00_6501;
																														{	/* Ast/unit.scm 310 */
																															obj_t
																																BgL_tmpz00_6502;
																															{	/* Ast/unit.scm 310 */
																																obj_t
																																	BgL_pairz00_4508;
																																BgL_pairz00_4508
																																	=
																																	CDR(((obj_t)
																																		BgL_defz00_2651));
																																BgL_tmpz00_6502
																																	=
																																	CAR
																																	(BgL_pairz00_4508);
																															}
																															BgL_test3695z00_6501
																																=
																																(BgL_tmpz00_6502
																																==
																																CNST_TABLE_REF
																																(12));
																														}
																														if (BgL_test3695z00_6501)
																															{	/* Ast/unit.scm 311 */
																																obj_t
																																	BgL_gz00_2655;
																																BgL_gz00_2655 =
																																	BGl_findzd2globalzd2zzast_envz00
																																	(BgL_var2z00_1930,
																																	BNIL);
																																{	/* Ast/unit.scm 311 */
																																	obj_t
																																		BgL_arityz00_2656;
																																	{	/* Ast/unit.scm 312 */
																																		bool_t
																																			BgL_test3696z00_6509;
																																		{	/* Ast/unit.scm 312 */
																																			obj_t
																																				BgL_classz00_4509;
																																			BgL_classz00_4509
																																				=
																																				BGl_globalz00zzast_varz00;
																																			if (BGL_OBJECTP(BgL_gz00_2655))
																																				{	/* Ast/unit.scm 312 */
																																					BgL_objectz00_bglt
																																						BgL_arg1807z00_4511;
																																					BgL_arg1807z00_4511
																																						=
																																						(BgL_objectz00_bglt)
																																						(BgL_gz00_2655);
																																					if (BGL_CONDEXPAND_ISA_ARCH64())
																																						{	/* Ast/unit.scm 312 */
																																							long
																																								BgL_idxz00_4517;
																																							BgL_idxz00_4517
																																								=
																																								BGL_OBJECT_INHERITANCE_NUM
																																								(BgL_arg1807z00_4511);
																																							BgL_test3696z00_6509
																																								=
																																								(VECTOR_REF
																																								(BGl_za2inheritancesza2z00zz__objectz00,
																																									(BgL_idxz00_4517
																																										+
																																										2L))
																																								==
																																								BgL_classz00_4509);
																																						}
																																					else
																																						{	/* Ast/unit.scm 312 */
																																							bool_t
																																								BgL_res3519z00_4542;
																																							{	/* Ast/unit.scm 312 */
																																								obj_t
																																									BgL_oclassz00_4525;
																																								{	/* Ast/unit.scm 312 */
																																									obj_t
																																										BgL_arg1815z00_4533;
																																									long
																																										BgL_arg1816z00_4534;
																																									BgL_arg1815z00_4533
																																										=
																																										(BGl_za2classesza2z00zz__objectz00);
																																									{	/* Ast/unit.scm 312 */
																																										long
																																											BgL_arg1817z00_4535;
																																										BgL_arg1817z00_4535
																																											=
																																											BGL_OBJECT_CLASS_NUM
																																											(BgL_arg1807z00_4511);
																																										BgL_arg1816z00_4534
																																											=
																																											(BgL_arg1817z00_4535
																																											-
																																											OBJECT_TYPE);
																																									}
																																									BgL_oclassz00_4525
																																										=
																																										VECTOR_REF
																																										(BgL_arg1815z00_4533,
																																										BgL_arg1816z00_4534);
																																								}
																																								{	/* Ast/unit.scm 312 */
																																									bool_t
																																										BgL__ortest_1115z00_4526;
																																									BgL__ortest_1115z00_4526
																																										=
																																										(BgL_classz00_4509
																																										==
																																										BgL_oclassz00_4525);
																																									if (BgL__ortest_1115z00_4526)
																																										{	/* Ast/unit.scm 312 */
																																											BgL_res3519z00_4542
																																												=
																																												BgL__ortest_1115z00_4526;
																																										}
																																									else
																																										{	/* Ast/unit.scm 312 */
																																											long
																																												BgL_odepthz00_4527;
																																											{	/* Ast/unit.scm 312 */
																																												obj_t
																																													BgL_arg1804z00_4528;
																																												BgL_arg1804z00_4528
																																													=
																																													(BgL_oclassz00_4525);
																																												BgL_odepthz00_4527
																																													=
																																													BGL_CLASS_DEPTH
																																													(BgL_arg1804z00_4528);
																																											}
																																											if ((2L < BgL_odepthz00_4527))
																																												{	/* Ast/unit.scm 312 */
																																													obj_t
																																														BgL_arg1802z00_4530;
																																													{	/* Ast/unit.scm 312 */
																																														obj_t
																																															BgL_arg1803z00_4531;
																																														BgL_arg1803z00_4531
																																															=
																																															(BgL_oclassz00_4525);
																																														BgL_arg1802z00_4530
																																															=
																																															BGL_CLASS_ANCESTORS_REF
																																															(BgL_arg1803z00_4531,
																																															2L);
																																													}
																																													BgL_res3519z00_4542
																																														=
																																														(BgL_arg1802z00_4530
																																														==
																																														BgL_classz00_4509);
																																												}
																																											else
																																												{	/* Ast/unit.scm 312 */
																																													BgL_res3519z00_4542
																																														=
																																														(
																																														(bool_t)
																																														0);
																																												}
																																										}
																																								}
																																							}
																																							BgL_test3696z00_6509
																																								=
																																								BgL_res3519z00_4542;
																																						}
																																				}
																																			else
																																				{	/* Ast/unit.scm 312 */
																																					BgL_test3696z00_6509
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																		if (BgL_test3696z00_6509)
																																			{	/* Ast/unit.scm 313 */
																																				bool_t
																																					BgL_test3701z00_6532;
																																				{	/* Ast/unit.scm 313 */
																																					BgL_valuez00_bglt
																																						BgL_arg2308z00_2664;
																																					BgL_arg2308z00_2664
																																						=
																																						(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt) BgL_gz00_2655))))->BgL_valuez00);
																																					{	/* Ast/unit.scm 313 */
																																						obj_t
																																							BgL_classz00_4544;
																																						BgL_classz00_4544
																																							=
																																							BGl_funz00zzast_varz00;
																																						{	/* Ast/unit.scm 313 */
																																							BgL_objectz00_bglt
																																								BgL_arg1807z00_4546;
																																							{	/* Ast/unit.scm 313 */
																																								obj_t
																																									BgL_tmpz00_6536;
																																								BgL_tmpz00_6536
																																									=
																																									(
																																									(obj_t)
																																									((BgL_objectz00_bglt) BgL_arg2308z00_2664));
																																								BgL_arg1807z00_4546
																																									=
																																									(BgL_objectz00_bglt)
																																									(BgL_tmpz00_6536);
																																							}
																																							if (BGL_CONDEXPAND_ISA_ARCH64())
																																								{	/* Ast/unit.scm 313 */
																																									long
																																										BgL_idxz00_4552;
																																									BgL_idxz00_4552
																																										=
																																										BGL_OBJECT_INHERITANCE_NUM
																																										(BgL_arg1807z00_4546);
																																									BgL_test3701z00_6532
																																										=
																																										(VECTOR_REF
																																										(BGl_za2inheritancesza2z00zz__objectz00,
																																											(BgL_idxz00_4552
																																												+
																																												2L))
																																										==
																																										BgL_classz00_4544);
																																								}
																																							else
																																								{	/* Ast/unit.scm 313 */
																																									bool_t
																																										BgL_res3520z00_4577;
																																									{	/* Ast/unit.scm 313 */
																																										obj_t
																																											BgL_oclassz00_4560;
																																										{	/* Ast/unit.scm 313 */
																																											obj_t
																																												BgL_arg1815z00_4568;
																																											long
																																												BgL_arg1816z00_4569;
																																											BgL_arg1815z00_4568
																																												=
																																												(BGl_za2classesza2z00zz__objectz00);
																																											{	/* Ast/unit.scm 313 */
																																												long
																																													BgL_arg1817z00_4570;
																																												BgL_arg1817z00_4570
																																													=
																																													BGL_OBJECT_CLASS_NUM
																																													(BgL_arg1807z00_4546);
																																												BgL_arg1816z00_4569
																																													=
																																													(BgL_arg1817z00_4570
																																													-
																																													OBJECT_TYPE);
																																											}
																																											BgL_oclassz00_4560
																																												=
																																												VECTOR_REF
																																												(BgL_arg1815z00_4568,
																																												BgL_arg1816z00_4569);
																																										}
																																										{	/* Ast/unit.scm 313 */
																																											bool_t
																																												BgL__ortest_1115z00_4561;
																																											BgL__ortest_1115z00_4561
																																												=
																																												(BgL_classz00_4544
																																												==
																																												BgL_oclassz00_4560);
																																											if (BgL__ortest_1115z00_4561)
																																												{	/* Ast/unit.scm 313 */
																																													BgL_res3520z00_4577
																																														=
																																														BgL__ortest_1115z00_4561;
																																												}
																																											else
																																												{	/* Ast/unit.scm 313 */
																																													long
																																														BgL_odepthz00_4562;
																																													{	/* Ast/unit.scm 313 */
																																														obj_t
																																															BgL_arg1804z00_4563;
																																														BgL_arg1804z00_4563
																																															=
																																															(BgL_oclassz00_4560);
																																														BgL_odepthz00_4562
																																															=
																																															BGL_CLASS_DEPTH
																																															(BgL_arg1804z00_4563);
																																													}
																																													if ((2L < BgL_odepthz00_4562))
																																														{	/* Ast/unit.scm 313 */
																																															obj_t
																																																BgL_arg1802z00_4565;
																																															{	/* Ast/unit.scm 313 */
																																																obj_t
																																																	BgL_arg1803z00_4566;
																																																BgL_arg1803z00_4566
																																																	=
																																																	(BgL_oclassz00_4560);
																																																BgL_arg1802z00_4565
																																																	=
																																																	BGL_CLASS_ANCESTORS_REF
																																																	(BgL_arg1803z00_4566,
																																																	2L);
																																															}
																																															BgL_res3520z00_4577
																																																=
																																																(BgL_arg1802z00_4565
																																																==
																																																BgL_classz00_4544);
																																														}
																																													else
																																														{	/* Ast/unit.scm 313 */
																																															BgL_res3520z00_4577
																																																=
																																																(
																																																(bool_t)
																																																0);
																																														}
																																												}
																																										}
																																									}
																																									BgL_test3701z00_6532
																																										=
																																										BgL_res3520z00_4577;
																																								}
																																						}
																																					}
																																				}
																																				if (BgL_test3701z00_6532)
																																					{	/* Ast/unit.scm 313 */
																																						BgL_arityz00_2656
																																							=
																																							BINT
																																							((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) (((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt) BgL_gz00_2655))))->BgL_valuez00))))->BgL_arityz00));
																																					}
																																				else
																																					{	/* Ast/unit.scm 313 */
																																						BgL_arityz00_2656
																																							=
																																							BFALSE;
																																					}
																																			}
																																		else
																																			{	/* Ast/unit.scm 312 */
																																				BgL_arityz00_2656
																																					=
																																					BFALSE;
																																			}
																																	}
																																	{	/* Ast/unit.scm 312 */

																																		if (INTEGERP
																																			(BgL_arityz00_2656))
																																			{	/* Ast/unit.scm 315 */
																																				if (BGl_globalzd2optionalzf3z21zzast_varz00(BgL_gz00_2655))
																																					{	/* Ast/unit.scm 317 */
																																						BGl_userzd2warningzd2zztools_errorz00
																																							(BgL_varz00_1929,
																																							BGl_string3565z00zzast_unitz00,
																																							BgL_sexpz00_39);
																																						BGL_TAIL
																																							return
																																							BGl_makezd2svarzd2definitionz00zzast_unitz00
																																							(BgL_varz00_1929,
																																							BgL_sexpz00_39);
																																					}
																																				else
																																					{	/* Ast/unit.scm 317 */
																																						if (BGl_globalzd2keyzf3z21zzast_varz00(BgL_gz00_2655))
																																							{	/* Ast/unit.scm 322 */
																																								BGl_userzd2warningzd2zztools_errorz00
																																									(BgL_varz00_1929,
																																									BGl_string3566z00zzast_unitz00,
																																									BgL_sexpz00_39);
																																								BGL_TAIL
																																									return
																																									BGl_makezd2svarzd2definitionz00zzast_unitz00
																																									(BgL_varz00_1929,
																																									BgL_sexpz00_39);
																																							}
																																						else
																																							{
																																								obj_t
																																									BgL_sexpz00_6575;
																																								BgL_sexpz00_6575
																																									=
																																									BGl_etazd2expansezd2zzast_unitz00
																																									(BgL_sexpz00_39,
																																									BgL_arityz00_2656);
																																								BgL_sexpz00_39
																																									=
																																									BgL_sexpz00_6575;
																																								goto
																																									BGl_toplevelzd2ze3astz31zzast_unitz00;
																																							}
																																					}
																																			}
																																		else
																																			{	/* Ast/unit.scm 315 */
																																				BGL_TAIL
																																					return
																																					BGl_makezd2svarzd2definitionz00zzast_unitz00
																																					(BgL_varz00_1929,
																																					BgL_sexpz00_39);
																																			}
																																	}
																																}
																															}
																														else
																															{	/* Ast/unit.scm 310 */
																																BGL_TAIL return
																																	BGl_makezd2svarzd2definitionz00zzast_unitz00
																																	(BgL_varz00_1929,
																																	BgL_sexpz00_39);
																															}
																													}
																												}
																											}
																										else
																											{	/* Ast/unit.scm 225 */
																												obj_t
																													BgL_arg1839z00_2005;
																												BgL_arg1839z00_2005 =
																													CAR(((obj_t)
																														BgL_cdrzd2581zd2_1997));
																												{
																													obj_t BgL_expz00_6582;
																													obj_t BgL_varz00_6581;

																													BgL_varz00_6581 =
																														BgL_arg1839z00_2005;
																													BgL_expz00_6582 =
																														BgL_cdrzd2586zd2_1998;
																													BgL_expz00_1937 =
																														BgL_expz00_6582;
																													BgL_varz00_1936 =
																														BgL_varz00_6581;
																													goto
																														BgL_tagzd2368zd2_1938;
																												}
																											}
																									}
																								else
																									{	/* Ast/unit.scm 225 */
																										obj_t BgL_arg1843z00_2009;

																										BgL_arg1843z00_2009 =
																											CAR(
																											((obj_t)
																												BgL_cdrzd2581zd2_1997));
																										{
																											obj_t BgL_expz00_6586;
																											obj_t BgL_varz00_6585;

																											BgL_varz00_6585 =
																												BgL_arg1843z00_2009;
																											BgL_expz00_6586 =
																												BgL_cdrzd2586zd2_1998;
																											BgL_expz00_1937 =
																												BgL_expz00_6586;
																											BgL_varz00_1936 =
																												BgL_varz00_6585;
																											goto
																												BgL_tagzd2368zd2_1938;
																										}
																									}
																							}
																						}
																					}
																			}
																		else
																			{	/* Ast/unit.scm 225 */
																				obj_t BgL_cdrzd2661zd2_2011;

																				BgL_cdrzd2661zd2_2011 =
																					CDR(((obj_t) BgL_sexpz00_39));
																				{	/* Ast/unit.scm 225 */
																					obj_t BgL_cdrzd2668zd2_2012;

																					BgL_cdrzd2668zd2_2012 =
																						CDR(
																						((obj_t) BgL_cdrzd2661zd2_2011));
																					{	/* Ast/unit.scm 225 */
																						obj_t BgL_carzd2674zd2_2013;

																						BgL_carzd2674zd2_2013 =
																							CAR(
																							((obj_t) BgL_cdrzd2668zd2_2012));
																						{	/* Ast/unit.scm 225 */
																							obj_t BgL_cdrzd2680zd2_2014;

																							BgL_cdrzd2680zd2_2014 =
																								CDR(
																								((obj_t)
																									BgL_carzd2674zd2_2013));
																							if ((CAR(((obj_t)
																											BgL_carzd2674zd2_2013)) ==
																									CNST_TABLE_REF(18)))
																								{	/* Ast/unit.scm 225 */
																									if (PAIRP
																										(BgL_cdrzd2680zd2_2014))
																										{	/* Ast/unit.scm 225 */
																											obj_t
																												BgL_carzd2684zd2_2018;
																											obj_t
																												BgL_cdrzd2685zd2_2019;
																											BgL_carzd2684zd2_2018 =
																												CAR
																												(BgL_cdrzd2680zd2_2014);
																											BgL_cdrzd2685zd2_2019 =
																												CDR
																												(BgL_cdrzd2680zd2_2014);
																											if (PAIRP
																												(BgL_carzd2684zd2_2018))
																												{	/* Ast/unit.scm 225 */
																													obj_t
																														BgL_carzd2689zd2_2021;
																													BgL_carzd2689zd2_2021
																														=
																														CAR
																														(BgL_carzd2684zd2_2018);
																													if (PAIRP
																														(BgL_carzd2689zd2_2021))
																														{	/* Ast/unit.scm 225 */
																															obj_t
																																BgL_cdrzd2695zd2_2023;
																															BgL_cdrzd2695zd2_2023
																																=
																																CDR
																																(BgL_carzd2689zd2_2021);
																															{	/* Ast/unit.scm 225 */
																																obj_t
																																	BgL_fz00_2024;
																																BgL_fz00_2024 =
																																	CAR
																																	(BgL_carzd2689zd2_2021);
																																if (PAIRP
																																	(BgL_cdrzd2695zd2_2023))
																																	{	/* Ast/unit.scm 225 */
																																		if (NULLP
																																			(CDR
																																				(BgL_carzd2684zd2_2018)))
																																			{	/* Ast/unit.scm 225 */
																																				if (PAIRP(BgL_cdrzd2685zd2_2019))
																																					{	/* Ast/unit.scm 225 */
																																						if (
																																							(BgL_fz00_2024
																																								==
																																								CAR
																																								(BgL_cdrzd2685zd2_2019)))
																																							{	/* Ast/unit.scm 225 */
																																								if (NULLP(CDR(BgL_cdrzd2685zd2_2019)))
																																									{	/* Ast/unit.scm 225 */
																																										if (NULLP(CDR(((obj_t) BgL_cdrzd2668zd2_2012))))
																																											{	/* Ast/unit.scm 225 */
																																												obj_t
																																													BgL_arg1860z00_2035;
																																												obj_t
																																													BgL_arg1862z00_2036;
																																												obj_t
																																													BgL_arg1863z00_2037;
																																												BgL_arg1860z00_2035
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_cdrzd2661zd2_2011));
																																												BgL_arg1862z00_2036
																																													=
																																													CAR
																																													(BgL_cdrzd2695zd2_2023);
																																												BgL_arg1863z00_2037
																																													=
																																													CDR
																																													(BgL_cdrzd2695zd2_2023);
																																												BgL_varz00_1916
																																													=
																																													BgL_arg1860z00_2035;
																																												BgL_argsz00_1917
																																													=
																																													BgL_arg1862z00_2036;
																																												BgL_expz00_1918
																																													=
																																													BgL_arg1863z00_2037;
																																												BgL_fz00_1919
																																													=
																																													BgL_fz00_2024;
																																												{	/* Ast/unit.scm 270 */
																																													bool_t
																																														BgL_test3718z00_6632;
																																													if (BGl_typezd2existszf3z21zztype_envz00(BgL_varz00_1916))
																																														{	/* Ast/unit.scm 270 */
																																															BgL_typez00_bglt
																																																BgL_arg2250z00_2585;
																																															BgL_arg2250z00_2585
																																																=
																																																BGl_findzd2typezd2zztype_envz00
																																																(BgL_varz00_1916);
																																															{	/* Ast/unit.scm 270 */
																																																obj_t
																																																	BgL_classz00_4283;
																																																BgL_classz00_4283
																																																	=
																																																	BGl_tclassz00zzobject_classz00;
																																																{	/* Ast/unit.scm 270 */
																																																	BgL_objectz00_bglt
																																																		BgL_arg1807z00_4285;
																																																	{	/* Ast/unit.scm 270 */
																																																		obj_t
																																																			BgL_tmpz00_6636;
																																																		BgL_tmpz00_6636
																																																			=
																																																			(
																																																			(obj_t)
																																																			((BgL_objectz00_bglt) BgL_arg2250z00_2585));
																																																		BgL_arg1807z00_4285
																																																			=
																																																			(BgL_objectz00_bglt)
																																																			(BgL_tmpz00_6636);
																																																	}
																																																	if (BGL_CONDEXPAND_ISA_ARCH64())
																																																		{	/* Ast/unit.scm 270 */
																																																			long
																																																				BgL_idxz00_4291;
																																																			BgL_idxz00_4291
																																																				=
																																																				BGL_OBJECT_INHERITANCE_NUM
																																																				(BgL_arg1807z00_4285);
																																																			BgL_test3718z00_6632
																																																				=
																																																				(VECTOR_REF
																																																				(BGl_za2inheritancesza2z00zz__objectz00,
																																																					(BgL_idxz00_4291
																																																						+
																																																						2L))
																																																				==
																																																				BgL_classz00_4283);
																																																		}
																																																	else
																																																		{	/* Ast/unit.scm 270 */
																																																			bool_t
																																																				BgL_res3513z00_4316;
																																																			{	/* Ast/unit.scm 270 */
																																																				obj_t
																																																					BgL_oclassz00_4299;
																																																				{	/* Ast/unit.scm 270 */
																																																					obj_t
																																																						BgL_arg1815z00_4307;
																																																					long
																																																						BgL_arg1816z00_4308;
																																																					BgL_arg1815z00_4307
																																																						=
																																																						(BGl_za2classesza2z00zz__objectz00);
																																																					{	/* Ast/unit.scm 270 */
																																																						long
																																																							BgL_arg1817z00_4309;
																																																						BgL_arg1817z00_4309
																																																							=
																																																							BGL_OBJECT_CLASS_NUM
																																																							(BgL_arg1807z00_4285);
																																																						BgL_arg1816z00_4308
																																																							=
																																																							(BgL_arg1817z00_4309
																																																							-
																																																							OBJECT_TYPE);
																																																					}
																																																					BgL_oclassz00_4299
																																																						=
																																																						VECTOR_REF
																																																						(BgL_arg1815z00_4307,
																																																						BgL_arg1816z00_4308);
																																																				}
																																																				{	/* Ast/unit.scm 270 */
																																																					bool_t
																																																						BgL__ortest_1115z00_4300;
																																																					BgL__ortest_1115z00_4300
																																																						=
																																																						(BgL_classz00_4283
																																																						==
																																																						BgL_oclassz00_4299);
																																																					if (BgL__ortest_1115z00_4300)
																																																						{	/* Ast/unit.scm 270 */
																																																							BgL_res3513z00_4316
																																																								=
																																																								BgL__ortest_1115z00_4300;
																																																						}
																																																					else
																																																						{	/* Ast/unit.scm 270 */
																																																							long
																																																								BgL_odepthz00_4301;
																																																							{	/* Ast/unit.scm 270 */
																																																								obj_t
																																																									BgL_arg1804z00_4302;
																																																								BgL_arg1804z00_4302
																																																									=
																																																									(BgL_oclassz00_4299);
																																																								BgL_odepthz00_4301
																																																									=
																																																									BGL_CLASS_DEPTH
																																																									(BgL_arg1804z00_4302);
																																																							}
																																																							if ((2L < BgL_odepthz00_4301))
																																																								{	/* Ast/unit.scm 270 */
																																																									obj_t
																																																										BgL_arg1802z00_4304;
																																																									{	/* Ast/unit.scm 270 */
																																																										obj_t
																																																											BgL_arg1803z00_4305;
																																																										BgL_arg1803z00_4305
																																																											=
																																																											(BgL_oclassz00_4299);
																																																										BgL_arg1802z00_4304
																																																											=
																																																											BGL_CLASS_ANCESTORS_REF
																																																											(BgL_arg1803z00_4305,
																																																											2L);
																																																									}
																																																									BgL_res3513z00_4316
																																																										=
																																																										(BgL_arg1802z00_4304
																																																										==
																																																										BgL_classz00_4283);
																																																								}
																																																							else
																																																								{	/* Ast/unit.scm 270 */
																																																									BgL_res3513z00_4316
																																																										=
																																																										(
																																																										(bool_t)
																																																										0);
																																																								}
																																																						}
																																																				}
																																																			}
																																																			BgL_test3718z00_6632
																																																				=
																																																				BgL_res3513z00_4316;
																																																		}
																																																}
																																															}
																																														}
																																													else
																																														{	/* Ast/unit.scm 270 */
																																															BgL_test3718z00_6632
																																																=
																																																(
																																																(bool_t)
																																																0);
																																														}
																																													if (BgL_test3718z00_6632)
																																														{	/* Ast/unit.scm 270 */
																																															BGl_errorzd2classzd2shadowz00zzast_unitz00
																																																(BgL_varz00_1916,
																																																BgL_sexpz00_39);
																																														}
																																													else
																																														{	/* Ast/unit.scm 270 */
																																															BFALSE;
																																														}
																																												}
																																												{	/* Ast/unit.scm 272 */
																																													obj_t
																																														BgL_idz00_2586;
																																													BgL_idz00_2586
																																														=
																																														BGl_idzd2ofzd2idz00zzast_identz00
																																														(BgL_varz00_1916,
																																														BGl_findzd2locationzd2zztools_locationz00
																																														(BgL_sexpz00_39));
																																													{	/* Ast/unit.scm 272 */
																																														obj_t
																																															BgL_defz00_2587;
																																														BgL_defz00_2587
																																															=
																																															BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																																															(BgL_idz00_2586,
																																															BgL_gdefsz00_40);
																																														{	/* Ast/unit.scm 273 */
																																															obj_t
																																																BgL_globalz00_2588;
																																															BgL_globalz00_2588
																																																=
																																																BGl_findzd2globalzf2modulez20zzast_envz00
																																																(BgL_idz00_2586,
																																																BGl_za2moduleza2z00zzmodule_modulez00);
																																															{	/* Ast/unit.scm 274 */

																																																{	/* Ast/unit.scm 276 */
																																																	bool_t
																																																		BgL_test3723z00_6664;
																																																	{	/* Ast/unit.scm 276 */
																																																		bool_t
																																																			BgL_test3724z00_6665;
																																																		{	/* Ast/unit.scm 276 */
																																																			obj_t
																																																				BgL_tmpz00_6666;
																																																			{	/* Ast/unit.scm 276 */
																																																				obj_t
																																																					BgL_pairz00_4318;
																																																				BgL_pairz00_4318
																																																					=
																																																					CDR
																																																					(
																																																					((obj_t) BgL_defz00_2587));
																																																				BgL_tmpz00_6666
																																																					=
																																																					CAR
																																																					(BgL_pairz00_4318);
																																																			}
																																																			BgL_test3724z00_6665
																																																				=
																																																				(BgL_tmpz00_6666
																																																				==
																																																				CNST_TABLE_REF
																																																				(12));
																																																		}
																																																		if (BgL_test3724z00_6665)
																																																			{	/* Ast/unit.scm 277 */
																																																				bool_t
																																																					BgL_test3725z00_6672;
																																																				{	/* Ast/unit.scm 277 */
																																																					bool_t
																																																						BgL_test3726z00_6673;
																																																					{	/* Ast/unit.scm 277 */
																																																						obj_t
																																																							BgL_classz00_4319;
																																																						BgL_classz00_4319
																																																							=
																																																							BGl_globalz00zzast_varz00;
																																																						if (BGL_OBJECTP(BgL_globalz00_2588))
																																																							{	/* Ast/unit.scm 277 */
																																																								BgL_objectz00_bglt
																																																									BgL_arg1807z00_4321;
																																																								BgL_arg1807z00_4321
																																																									=
																																																									(BgL_objectz00_bglt)
																																																									(BgL_globalz00_2588);
																																																								if (BGL_CONDEXPAND_ISA_ARCH64())
																																																									{	/* Ast/unit.scm 277 */
																																																										long
																																																											BgL_idxz00_4327;
																																																										BgL_idxz00_4327
																																																											=
																																																											BGL_OBJECT_INHERITANCE_NUM
																																																											(BgL_arg1807z00_4321);
																																																										BgL_test3726z00_6673
																																																											=
																																																											(VECTOR_REF
																																																											(BGl_za2inheritancesza2z00zz__objectz00,
																																																												(BgL_idxz00_4327
																																																													+
																																																													2L))
																																																											==
																																																											BgL_classz00_4319);
																																																									}
																																																								else
																																																									{	/* Ast/unit.scm 277 */
																																																										bool_t
																																																											BgL_res3514z00_4352;
																																																										{	/* Ast/unit.scm 277 */
																																																											obj_t
																																																												BgL_oclassz00_4335;
																																																											{	/* Ast/unit.scm 277 */
																																																												obj_t
																																																													BgL_arg1815z00_4343;
																																																												long
																																																													BgL_arg1816z00_4344;
																																																												BgL_arg1815z00_4343
																																																													=
																																																													(BGl_za2classesza2z00zz__objectz00);
																																																												{	/* Ast/unit.scm 277 */
																																																													long
																																																														BgL_arg1817z00_4345;
																																																													BgL_arg1817z00_4345
																																																														=
																																																														BGL_OBJECT_CLASS_NUM
																																																														(BgL_arg1807z00_4321);
																																																													BgL_arg1816z00_4344
																																																														=
																																																														(BgL_arg1817z00_4345
																																																														-
																																																														OBJECT_TYPE);
																																																												}
																																																												BgL_oclassz00_4335
																																																													=
																																																													VECTOR_REF
																																																													(BgL_arg1815z00_4343,
																																																													BgL_arg1816z00_4344);
																																																											}
																																																											{	/* Ast/unit.scm 277 */
																																																												bool_t
																																																													BgL__ortest_1115z00_4336;
																																																												BgL__ortest_1115z00_4336
																																																													=
																																																													(BgL_classz00_4319
																																																													==
																																																													BgL_oclassz00_4335);
																																																												if (BgL__ortest_1115z00_4336)
																																																													{	/* Ast/unit.scm 277 */
																																																														BgL_res3514z00_4352
																																																															=
																																																															BgL__ortest_1115z00_4336;
																																																													}
																																																												else
																																																													{	/* Ast/unit.scm 277 */
																																																														long
																																																															BgL_odepthz00_4337;
																																																														{	/* Ast/unit.scm 277 */
																																																															obj_t
																																																																BgL_arg1804z00_4338;
																																																															BgL_arg1804z00_4338
																																																																=
																																																																(BgL_oclassz00_4335);
																																																															BgL_odepthz00_4337
																																																																=
																																																																BGL_CLASS_DEPTH
																																																																(BgL_arg1804z00_4338);
																																																														}
																																																														if ((2L < BgL_odepthz00_4337))
																																																															{	/* Ast/unit.scm 277 */
																																																																obj_t
																																																																	BgL_arg1802z00_4340;
																																																																{	/* Ast/unit.scm 277 */
																																																																	obj_t
																																																																		BgL_arg1803z00_4341;
																																																																	BgL_arg1803z00_4341
																																																																		=
																																																																		(BgL_oclassz00_4335);
																																																																	BgL_arg1802z00_4340
																																																																		=
																																																																		BGL_CLASS_ANCESTORS_REF
																																																																		(BgL_arg1803z00_4341,
																																																																		2L);
																																																																}
																																																																BgL_res3514z00_4352
																																																																	=
																																																																	(BgL_arg1802z00_4340
																																																																	==
																																																																	BgL_classz00_4319);
																																																															}
																																																														else
																																																															{	/* Ast/unit.scm 277 */
																																																																BgL_res3514z00_4352
																																																																	=
																																																																	(
																																																																	(bool_t)
																																																																	0);
																																																															}
																																																													}
																																																											}
																																																										}
																																																										BgL_test3726z00_6673
																																																											=
																																																											BgL_res3514z00_4352;
																																																									}
																																																							}
																																																						else
																																																							{	/* Ast/unit.scm 277 */
																																																								BgL_test3726z00_6673
																																																									=
																																																									(
																																																									(bool_t)
																																																									0);
																																																							}
																																																					}
																																																					if (BgL_test3726z00_6673)
																																																						{	/* Ast/unit.scm 277 */
																																																							BgL_test3725z00_6672
																																																								=
																																																								(
																																																								(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt) BgL_globalz00_2588))))->BgL_accessz00) == CNST_TABLE_REF(12));
																																																						}
																																																					else
																																																						{	/* Ast/unit.scm 277 */
																																																							BgL_test3725z00_6672
																																																								=
																																																								(
																																																								(bool_t)
																																																								1);
																																																						}
																																																				}
																																																				if (BgL_test3725z00_6672)
																																																					{	/* Ast/unit.scm 277 */
																																																						BgL_test3723z00_6664
																																																							=
																																																							(BgL_idz00_2586
																																																							==
																																																							BgL_fz00_1919);
																																																					}
																																																				else
																																																					{	/* Ast/unit.scm 277 */
																																																						BgL_test3723z00_6664
																																																							=
																																																							(
																																																							(bool_t)
																																																							0);
																																																					}
																																																			}
																																																		else
																																																			{	/* Ast/unit.scm 276 */
																																																				BgL_test3723z00_6664
																																																					=
																																																					(
																																																					(bool_t)
																																																					0);
																																																			}
																																																	}
																																																	if (BgL_test3723z00_6664)
																																																		{	/* Ast/unit.scm 282 */
																																																			obj_t
																																																				BgL_arg2261z00_2600;
																																																			{	/* Ast/unit.scm 282 */
																																																				obj_t
																																																					BgL_arg2262z00_2601;
																																																				{	/* Ast/unit.scm 282 */
																																																					obj_t
																																																						BgL_arg2263z00_2602;
																																																					{	/* Ast/unit.scm 282 */
																																																						obj_t
																																																							BgL_pairz00_4357;
																																																						BgL_pairz00_4357
																																																							=
																																																							CDR
																																																							(
																																																							((obj_t) BgL_sexpz00_39));
																																																						BgL_arg2263z00_2602
																																																							=
																																																							CDR
																																																							(BgL_pairz00_4357);
																																																					}
																																																					BgL_arg2262z00_2601
																																																						=
																																																						BGl_findzd2locationzd2zztools_locationz00
																																																						(BgL_arg2263z00_2602);
																																																				}
																																																				BgL_arg2261z00_2600
																																																					=
																																																					BGl_normaliza7ezd2prognzf2errorz87zzast_unitz00
																																																					(BgL_expz00_1918,
																																																					BgL_sexpz00_39,
																																																					BgL_arg2262z00_2601);
																																																			}
																																																			return
																																																				BGl_makezd2sfunzd2definitionz00zzast_unitz00
																																																				(BgL_varz00_1916,
																																																				BGl_za2moduleza2z00zzmodule_modulez00,
																																																				BgL_argsz00_1917,
																																																				BgL_arg2261z00_2600,
																																																				BgL_sexpz00_39,
																																																				CNST_TABLE_REF
																																																				(13));
																																																		}
																																																	else
																																																		{	/* Ast/unit.scm 276 */
																																																			BGL_TAIL
																																																				return
																																																				BGl_makezd2svarzd2definitionz00zzast_unitz00
																																																				(BgL_varz00_1916,
																																																				BgL_sexpz00_39);
																																																		}
																																																}
																																															}
																																														}
																																													}
																																												}
																																											}
																																										else
																																											{	/* Ast/unit.scm 225 */
																																												obj_t
																																													BgL_arg1864z00_2039;
																																												obj_t
																																													BgL_arg1866z00_2040;
																																												BgL_arg1864z00_2039
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_cdrzd2661zd2_2011));
																																												BgL_arg1866z00_2040
																																													=
																																													CDR
																																													(
																																													((obj_t) BgL_cdrzd2661zd2_2011));
																																												{
																																													obj_t
																																														BgL_expz00_6715;
																																													obj_t
																																														BgL_varz00_6714;
																																													BgL_varz00_6714
																																														=
																																														BgL_arg1864z00_2039;
																																													BgL_expz00_6715
																																														=
																																														BgL_arg1866z00_2040;
																																													BgL_expz00_1937
																																														=
																																														BgL_expz00_6715;
																																													BgL_varz00_1936
																																														=
																																														BgL_varz00_6714;
																																													goto
																																														BgL_tagzd2368zd2_1938;
																																												}
																																											}
																																									}
																																								else
																																									{	/* Ast/unit.scm 225 */
																																										obj_t
																																											BgL_cdrzd2782zd2_2043;
																																										BgL_cdrzd2782zd2_2043
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_cdrzd2661zd2_2011));
																																										{	/* Ast/unit.scm 225 */
																																											obj_t
																																												BgL_carzd2788zd2_2044;
																																											BgL_carzd2788zd2_2044
																																												=
																																												CAR
																																												(
																																												((obj_t) BgL_cdrzd2782zd2_2043));
																																											{	/* Ast/unit.scm 225 */
																																												obj_t
																																													BgL_carzd2793zd2_2045;
																																												obj_t
																																													BgL_cdrzd2794zd2_2046;
																																												BgL_carzd2793zd2_2045
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_carzd2788zd2_2044));
																																												BgL_cdrzd2794zd2_2046
																																													=
																																													CDR
																																													(
																																													((obj_t) BgL_carzd2788zd2_2044));
																																												if (BGl_lambdazf3zf3zzast_unitz00(BgL_carzd2793zd2_2045))
																																													{	/* Ast/unit.scm 225 */
																																														if (NULLP(CDR(((obj_t) BgL_cdrzd2782zd2_2043))))
																																															{	/* Ast/unit.scm 225 */
																																																obj_t
																																																	BgL_arg1872z00_2050;
																																																obj_t
																																																	BgL_arg1873z00_2051;
																																																obj_t
																																																	BgL_arg1874z00_2052;
																																																BgL_arg1872z00_2050
																																																	=
																																																	CAR
																																																	(
																																																	((obj_t) BgL_cdrzd2661zd2_2011));
																																																BgL_arg1873z00_2051
																																																	=
																																																	CAR
																																																	(
																																																	((obj_t) BgL_cdrzd2794zd2_2046));
																																																BgL_arg1874z00_2052
																																																	=
																																																	CDR
																																																	(
																																																	((obj_t) BgL_cdrzd2794zd2_2046));
																																																BgL_varz00_1921
																																																	=
																																																	BgL_arg1872z00_2050;
																																																BgL_lamz00_1922
																																																	=
																																																	BgL_carzd2793zd2_2045;
																																																BgL_argsz00_1923
																																																	=
																																																	BgL_arg1873z00_2051;
																																																BgL_expz00_1924
																																																	=
																																																	BgL_arg1874z00_2052;
																																															BgL_tagzd2364zd2_1925:
																																																{	/* Ast/unit.scm 286 */
																																																	bool_t
																																																		BgL_test3733z00_6736;
																																																	if (BGl_typezd2existszf3z21zztype_envz00(BgL_varz00_1921))
																																																		{	/* Ast/unit.scm 286 */
																																																			BgL_typez00_bglt
																																																				BgL_arg2271z00_2614;
																																																			BgL_arg2271z00_2614
																																																				=
																																																				BGl_findzd2typezd2zztype_envz00
																																																				(BgL_varz00_1921);
																																																			{	/* Ast/unit.scm 286 */
																																																				obj_t
																																																					BgL_classz00_4358;
																																																				BgL_classz00_4358
																																																					=
																																																					BGl_tclassz00zzobject_classz00;
																																																				{	/* Ast/unit.scm 286 */
																																																					BgL_objectz00_bglt
																																																						BgL_arg1807z00_4360;
																																																					{	/* Ast/unit.scm 286 */
																																																						obj_t
																																																							BgL_tmpz00_6740;
																																																						BgL_tmpz00_6740
																																																							=
																																																							(
																																																							(obj_t)
																																																							((BgL_objectz00_bglt) BgL_arg2271z00_2614));
																																																						BgL_arg1807z00_4360
																																																							=
																																																							(BgL_objectz00_bglt)
																																																							(BgL_tmpz00_6740);
																																																					}
																																																					if (BGL_CONDEXPAND_ISA_ARCH64())
																																																						{	/* Ast/unit.scm 286 */
																																																							long
																																																								BgL_idxz00_4366;
																																																							BgL_idxz00_4366
																																																								=
																																																								BGL_OBJECT_INHERITANCE_NUM
																																																								(BgL_arg1807z00_4360);
																																																							BgL_test3733z00_6736
																																																								=
																																																								(VECTOR_REF
																																																								(BGl_za2inheritancesza2z00zz__objectz00,
																																																									(BgL_idxz00_4366
																																																										+
																																																										2L))
																																																								==
																																																								BgL_classz00_4358);
																																																						}
																																																					else
																																																						{	/* Ast/unit.scm 286 */
																																																							bool_t
																																																								BgL_res3515z00_4391;
																																																							{	/* Ast/unit.scm 286 */
																																																								obj_t
																																																									BgL_oclassz00_4374;
																																																								{	/* Ast/unit.scm 286 */
																																																									obj_t
																																																										BgL_arg1815z00_4382;
																																																									long
																																																										BgL_arg1816z00_4383;
																																																									BgL_arg1815z00_4382
																																																										=
																																																										(BGl_za2classesza2z00zz__objectz00);
																																																									{	/* Ast/unit.scm 286 */
																																																										long
																																																											BgL_arg1817z00_4384;
																																																										BgL_arg1817z00_4384
																																																											=
																																																											BGL_OBJECT_CLASS_NUM
																																																											(BgL_arg1807z00_4360);
																																																										BgL_arg1816z00_4383
																																																											=
																																																											(BgL_arg1817z00_4384
																																																											-
																																																											OBJECT_TYPE);
																																																									}
																																																									BgL_oclassz00_4374
																																																										=
																																																										VECTOR_REF
																																																										(BgL_arg1815z00_4382,
																																																										BgL_arg1816z00_4383);
																																																								}
																																																								{	/* Ast/unit.scm 286 */
																																																									bool_t
																																																										BgL__ortest_1115z00_4375;
																																																									BgL__ortest_1115z00_4375
																																																										=
																																																										(BgL_classz00_4358
																																																										==
																																																										BgL_oclassz00_4374);
																																																									if (BgL__ortest_1115z00_4375)
																																																										{	/* Ast/unit.scm 286 */
																																																											BgL_res3515z00_4391
																																																												=
																																																												BgL__ortest_1115z00_4375;
																																																										}
																																																									else
																																																										{	/* Ast/unit.scm 286 */
																																																											long
																																																												BgL_odepthz00_4376;
																																																											{	/* Ast/unit.scm 286 */
																																																												obj_t
																																																													BgL_arg1804z00_4377;
																																																												BgL_arg1804z00_4377
																																																													=
																																																													(BgL_oclassz00_4374);
																																																												BgL_odepthz00_4376
																																																													=
																																																													BGL_CLASS_DEPTH
																																																													(BgL_arg1804z00_4377);
																																																											}
																																																											if ((2L < BgL_odepthz00_4376))
																																																												{	/* Ast/unit.scm 286 */
																																																													obj_t
																																																														BgL_arg1802z00_4379;
																																																													{	/* Ast/unit.scm 286 */
																																																														obj_t
																																																															BgL_arg1803z00_4380;
																																																														BgL_arg1803z00_4380
																																																															=
																																																															(BgL_oclassz00_4374);
																																																														BgL_arg1802z00_4379
																																																															=
																																																															BGL_CLASS_ANCESTORS_REF
																																																															(BgL_arg1803z00_4380,
																																																															2L);
																																																													}
																																																													BgL_res3515z00_4391
																																																														=
																																																														(BgL_arg1802z00_4379
																																																														==
																																																														BgL_classz00_4358);
																																																												}
																																																											else
																																																												{	/* Ast/unit.scm 286 */
																																																													BgL_res3515z00_4391
																																																														=
																																																														(
																																																														(bool_t)
																																																														0);
																																																												}
																																																										}
																																																								}
																																																							}
																																																							BgL_test3733z00_6736
																																																								=
																																																								BgL_res3515z00_4391;
																																																						}
																																																				}
																																																			}
																																																		}
																																																	else
																																																		{	/* Ast/unit.scm 286 */
																																																			BgL_test3733z00_6736
																																																				=
																																																				(
																																																				(bool_t)
																																																				0);
																																																		}
																																																	if (BgL_test3733z00_6736)
																																																		{	/* Ast/unit.scm 286 */
																																																			BGl_errorzd2classzd2shadowz00zzast_unitz00
																																																				(BgL_varz00_1921,
																																																				BgL_sexpz00_39);
																																																		}
																																																	else
																																																		{	/* Ast/unit.scm 286 */
																																																			BFALSE;
																																																		}
																																																}
																																																{	/* Ast/unit.scm 288 */
																																																	obj_t
																																																		BgL_idz00_2615;
																																																	BgL_idz00_2615
																																																		=
																																																		BGl_idzd2ofzd2idz00zzast_identz00
																																																		(BgL_varz00_1921,
																																																		BGl_findzd2locationzd2zztools_locationz00
																																																		(BgL_sexpz00_39));
																																																	{	/* Ast/unit.scm 288 */
																																																		obj_t
																																																			BgL_defz00_2616;
																																																		BgL_defz00_2616
																																																			=
																																																			BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																																																			(BgL_idz00_2615,
																																																			BgL_gdefsz00_40);
																																																		{	/* Ast/unit.scm 289 */
																																																			obj_t
																																																				BgL_globalz00_2617;
																																																			BgL_globalz00_2617
																																																				=
																																																				BGl_findzd2globalzf2modulez20zzast_envz00
																																																				(BgL_idz00_2615,
																																																				BGl_za2moduleza2z00zzmodule_modulez00);
																																																			{	/* Ast/unit.scm 290 */
																																																				BgL_typez00_bglt
																																																					BgL_tlamz00_2618;
																																																				BgL_tlamz00_2618
																																																					=
																																																					BGl_typezd2ofzd2idz00zzast_identz00
																																																					(BgL_lamz00_1922,
																																																					BFALSE);
																																																				{	/* Ast/unit.scm 291 */

																																																					{	/* Ast/unit.scm 293 */
																																																						bool_t
																																																							BgL_test3738z00_6769;
																																																						{	/* Ast/unit.scm 293 */
																																																							bool_t
																																																								BgL_test3739z00_6770;
																																																							{	/* Ast/unit.scm 293 */
																																																								obj_t
																																																									BgL_tmpz00_6771;
																																																								{	/* Ast/unit.scm 293 */
																																																									obj_t
																																																										BgL_pairz00_4393;
																																																									BgL_pairz00_4393
																																																										=
																																																										CDR
																																																										(
																																																										((obj_t) BgL_defz00_2616));
																																																									BgL_tmpz00_6771
																																																										=
																																																										CAR
																																																										(BgL_pairz00_4393);
																																																								}
																																																								BgL_test3739z00_6770
																																																									=
																																																									(BgL_tmpz00_6771
																																																									==
																																																									CNST_TABLE_REF
																																																									(12));
																																																							}
																																																							if (BgL_test3739z00_6770)
																																																								{	/* Ast/unit.scm 294 */
																																																									bool_t
																																																										BgL__ortest_1119z00_2634;
																																																									{	/* Ast/unit.scm 294 */
																																																										bool_t
																																																											BgL_test3740z00_6777;
																																																										{	/* Ast/unit.scm 294 */
																																																											obj_t
																																																												BgL_classz00_4394;
																																																											BgL_classz00_4394
																																																												=
																																																												BGl_globalz00zzast_varz00;
																																																											if (BGL_OBJECTP(BgL_globalz00_2617))
																																																												{	/* Ast/unit.scm 294 */
																																																													BgL_objectz00_bglt
																																																														BgL_arg1807z00_4396;
																																																													BgL_arg1807z00_4396
																																																														=
																																																														(BgL_objectz00_bglt)
																																																														(BgL_globalz00_2617);
																																																													if (BGL_CONDEXPAND_ISA_ARCH64())
																																																														{	/* Ast/unit.scm 294 */
																																																															long
																																																																BgL_idxz00_4402;
																																																															BgL_idxz00_4402
																																																																=
																																																																BGL_OBJECT_INHERITANCE_NUM
																																																																(BgL_arg1807z00_4396);
																																																															BgL_test3740z00_6777
																																																																=
																																																																(VECTOR_REF
																																																																(BGl_za2inheritancesza2z00zz__objectz00,
																																																																	(BgL_idxz00_4402
																																																																		+
																																																																		2L))
																																																																==
																																																																BgL_classz00_4394);
																																																														}
																																																													else
																																																														{	/* Ast/unit.scm 294 */
																																																															bool_t
																																																																BgL_res3516z00_4427;
																																																															{	/* Ast/unit.scm 294 */
																																																																obj_t
																																																																	BgL_oclassz00_4410;
																																																																{	/* Ast/unit.scm 294 */
																																																																	obj_t
																																																																		BgL_arg1815z00_4418;
																																																																	long
																																																																		BgL_arg1816z00_4419;
																																																																	BgL_arg1815z00_4418
																																																																		=
																																																																		(BGl_za2classesza2z00zz__objectz00);
																																																																	{	/* Ast/unit.scm 294 */
																																																																		long
																																																																			BgL_arg1817z00_4420;
																																																																		BgL_arg1817z00_4420
																																																																			=
																																																																			BGL_OBJECT_CLASS_NUM
																																																																			(BgL_arg1807z00_4396);
																																																																		BgL_arg1816z00_4419
																																																																			=
																																																																			(BgL_arg1817z00_4420
																																																																			-
																																																																			OBJECT_TYPE);
																																																																	}
																																																																	BgL_oclassz00_4410
																																																																		=
																																																																		VECTOR_REF
																																																																		(BgL_arg1815z00_4418,
																																																																		BgL_arg1816z00_4419);
																																																																}
																																																																{	/* Ast/unit.scm 294 */
																																																																	bool_t
																																																																		BgL__ortest_1115z00_4411;
																																																																	BgL__ortest_1115z00_4411
																																																																		=
																																																																		(BgL_classz00_4394
																																																																		==
																																																																		BgL_oclassz00_4410);
																																																																	if (BgL__ortest_1115z00_4411)
																																																																		{	/* Ast/unit.scm 294 */
																																																																			BgL_res3516z00_4427
																																																																				=
																																																																				BgL__ortest_1115z00_4411;
																																																																		}
																																																																	else
																																																																		{	/* Ast/unit.scm 294 */
																																																																			long
																																																																				BgL_odepthz00_4412;
																																																																			{	/* Ast/unit.scm 294 */
																																																																				obj_t
																																																																					BgL_arg1804z00_4413;
																																																																				BgL_arg1804z00_4413
																																																																					=
																																																																					(BgL_oclassz00_4410);
																																																																				BgL_odepthz00_4412
																																																																					=
																																																																					BGL_CLASS_DEPTH
																																																																					(BgL_arg1804z00_4413);
																																																																			}
																																																																			if ((2L < BgL_odepthz00_4412))
																																																																				{	/* Ast/unit.scm 294 */
																																																																					obj_t
																																																																						BgL_arg1802z00_4415;
																																																																					{	/* Ast/unit.scm 294 */
																																																																						obj_t
																																																																							BgL_arg1803z00_4416;
																																																																						BgL_arg1803z00_4416
																																																																							=
																																																																							(BgL_oclassz00_4410);
																																																																						BgL_arg1802z00_4415
																																																																							=
																																																																							BGL_CLASS_ANCESTORS_REF
																																																																							(BgL_arg1803z00_4416,
																																																																							2L);
																																																																					}
																																																																					BgL_res3516z00_4427
																																																																						=
																																																																						(BgL_arg1802z00_4415
																																																																						==
																																																																						BgL_classz00_4394);
																																																																				}
																																																																			else
																																																																				{	/* Ast/unit.scm 294 */
																																																																					BgL_res3516z00_4427
																																																																						=
																																																																						(
																																																																						(bool_t)
																																																																						0);
																																																																				}
																																																																		}
																																																																}
																																																															}
																																																															BgL_test3740z00_6777
																																																																=
																																																																BgL_res3516z00_4427;
																																																														}
																																																												}
																																																											else
																																																												{	/* Ast/unit.scm 294 */
																																																													BgL_test3740z00_6777
																																																														=
																																																														(
																																																														(bool_t)
																																																														0);
																																																												}
																																																										}
																																																										if (BgL_test3740z00_6777)
																																																											{	/* Ast/unit.scm 294 */
																																																												BgL__ortest_1119z00_2634
																																																													=
																																																													(
																																																													(bool_t)
																																																													0);
																																																											}
																																																										else
																																																											{	/* Ast/unit.scm 294 */
																																																												BgL__ortest_1119z00_2634
																																																													=
																																																													(
																																																													(bool_t)
																																																													1);
																																																											}
																																																									}
																																																									if (BgL__ortest_1119z00_2634)
																																																										{	/* Ast/unit.scm 294 */
																																																											BgL_test3738z00_6769
																																																												=
																																																												BgL__ortest_1119z00_2634;
																																																										}
																																																									else
																																																										{	/* Ast/unit.scm 294 */
																																																											BgL_test3738z00_6769
																																																												=
																																																												(
																																																												(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt) BgL_globalz00_2617))))->BgL_accessz00) == CNST_TABLE_REF(12));
																																																										}
																																																								}
																																																							else
																																																								{	/* Ast/unit.scm 293 */
																																																									BgL_test3738z00_6769
																																																										=
																																																										(
																																																										(bool_t)
																																																										0);
																																																								}
																																																						}
																																																						if (BgL_test3738z00_6769)
																																																							{	/* Ast/unit.scm 296 */
																																																								obj_t
																																																									BgL_arg2281z00_2628;
																																																								obj_t
																																																									BgL_arg2282z00_2629;
																																																								BgL_arg2281z00_2628
																																																									=
																																																									BGl_makezd2typedzd2identz00zzast_identz00
																																																									(BgL_idz00_2615,
																																																									(((BgL_typez00_bglt) COBJECT(BgL_tlamz00_2618))->BgL_idz00));
																																																								{	/* Ast/unit.scm 298 */
																																																									obj_t
																																																										BgL_arg2284z00_2631;
																																																									{	/* Ast/unit.scm 298 */
																																																										obj_t
																																																											BgL_arg2286z00_2632;
																																																										{	/* Ast/unit.scm 298 */
																																																											obj_t
																																																												BgL_pairz00_4433;
																																																											BgL_pairz00_4433
																																																												=
																																																												CDR
																																																												(
																																																												((obj_t) BgL_sexpz00_39));
																																																											BgL_arg2286z00_2632
																																																												=
																																																												CDR
																																																												(BgL_pairz00_4433);
																																																										}
																																																										BgL_arg2284z00_2631
																																																											=
																																																											BGl_findzd2locationzd2zztools_locationz00
																																																											(BgL_arg2286z00_2632);
																																																									}
																																																									BgL_arg2282z00_2629
																																																										=
																																																										BGl_normaliza7ezd2prognzf2errorz87zzast_unitz00
																																																										(BgL_expz00_1924,
																																																										BgL_sexpz00_39,
																																																										BgL_arg2284z00_2631);
																																																								}
																																																								return
																																																									BGl_makezd2sfunzd2definitionz00zzast_unitz00
																																																									(BgL_arg2281z00_2628,
																																																									BGl_za2moduleza2z00zzmodule_modulez00,
																																																									BgL_argsz00_1923,
																																																									BgL_arg2282z00_2629,
																																																									BgL_sexpz00_39,
																																																									CNST_TABLE_REF
																																																									(13));
																																																							}
																																																						else
																																																							{	/* Ast/unit.scm 293 */
																																																								BGL_TAIL
																																																									return
																																																									BGl_makezd2svarzd2definitionz00zzast_unitz00
																																																									(BgL_varz00_1921,
																																																									BgL_sexpz00_39);
																																																							}
																																																					}
																																																				}
																																																			}
																																																		}
																																																	}
																																																}
																																															}
																																														else
																																															{	/* Ast/unit.scm 225 */
																																																obj_t
																																																	BgL_cdrzd2837zd2_2053;
																																																BgL_cdrzd2837zd2_2053
																																																	=
																																																	CDR
																																																	(
																																																	((obj_t) BgL_sexpz00_39));
																																																{	/* Ast/unit.scm 225 */
																																																	obj_t
																																																		BgL_arg1875z00_2054;
																																																	obj_t
																																																		BgL_arg1876z00_2055;
																																																	BgL_arg1875z00_2054
																																																		=
																																																		CAR
																																																		(
																																																		((obj_t) BgL_cdrzd2837zd2_2053));
																																																	BgL_arg1876z00_2055
																																																		=
																																																		CDR
																																																		(
																																																		((obj_t) BgL_cdrzd2837zd2_2053));
																																																	{
																																																		obj_t
																																																			BgL_expz00_6823;
																																																		obj_t
																																																			BgL_varz00_6822;
																																																		BgL_varz00_6822
																																																			=
																																																			BgL_arg1875z00_2054;
																																																		BgL_expz00_6823
																																																			=
																																																			BgL_arg1876z00_2055;
																																																		BgL_expz00_1937
																																																			=
																																																			BgL_expz00_6823;
																																																		BgL_varz00_1936
																																																			=
																																																			BgL_varz00_6822;
																																																		goto
																																																			BgL_tagzd2368zd2_1938;
																																																	}
																																																}
																																															}
																																													}
																																												else
																																													{	/* Ast/unit.scm 225 */
																																														obj_t
																																															BgL_cdrzd2864zd2_2057;
																																														BgL_cdrzd2864zd2_2057
																																															=
																																															CDR
																																															(
																																															((obj_t) BgL_sexpz00_39));
																																														{	/* Ast/unit.scm 225 */
																																															obj_t
																																																BgL_cdrzd2869zd2_2058;
																																															BgL_cdrzd2869zd2_2058
																																																=
																																																CDR
																																																(
																																																((obj_t) BgL_cdrzd2864zd2_2057));
																																															{	/* Ast/unit.scm 225 */
																																																obj_t
																																																	BgL_carzd2873zd2_2059;
																																																BgL_carzd2873zd2_2059
																																																	=
																																																	CAR
																																																	(
																																																	((obj_t) BgL_cdrzd2869zd2_2058));
																																																if (SYMBOLP(BgL_carzd2873zd2_2059))
																																																	{	/* Ast/unit.scm 225 */
																																																		if (NULLP(CDR(((obj_t) BgL_cdrzd2869zd2_2058))))
																																																			{	/* Ast/unit.scm 225 */
																																																				obj_t
																																																					BgL_arg1882z00_2063;
																																																				BgL_arg1882z00_2063
																																																					=
																																																					CAR
																																																					(
																																																					((obj_t) BgL_cdrzd2864zd2_2057));
																																																				{
																																																					obj_t
																																																						BgL_var2z00_6839;
																																																					obj_t
																																																						BgL_varz00_6838;
																																																					BgL_varz00_6838
																																																						=
																																																						BgL_arg1882z00_2063;
																																																					BgL_var2z00_6839
																																																						=
																																																						BgL_carzd2873zd2_2059;
																																																					BgL_var2z00_1930
																																																						=
																																																						BgL_var2z00_6839;
																																																					BgL_varz00_1929
																																																						=
																																																						BgL_varz00_6838;
																																																					goto
																																																						BgL_tagzd2366zd2_1931;
																																																				}
																																																			}
																																																		else
																																																			{	/* Ast/unit.scm 225 */
																																																				obj_t
																																																					BgL_arg1883z00_2065;
																																																				BgL_arg1883z00_2065
																																																					=
																																																					CAR
																																																					(
																																																					((obj_t) BgL_cdrzd2864zd2_2057));
																																																				{
																																																					obj_t
																																																						BgL_expz00_6843;
																																																					obj_t
																																																						BgL_varz00_6842;
																																																					BgL_varz00_6842
																																																						=
																																																						BgL_arg1883z00_2065;
																																																					BgL_expz00_6843
																																																						=
																																																						BgL_cdrzd2869zd2_2058;
																																																					BgL_expz00_1937
																																																						=
																																																						BgL_expz00_6843;
																																																					BgL_varz00_1936
																																																						=
																																																						BgL_varz00_6842;
																																																					goto
																																																						BgL_tagzd2368zd2_1938;
																																																				}
																																																			}
																																																	}
																																																else
																																																	{	/* Ast/unit.scm 225 */
																																																		obj_t
																																																			BgL_arg1887z00_2069;
																																																		BgL_arg1887z00_2069
																																																			=
																																																			CAR
																																																			(
																																																			((obj_t) BgL_cdrzd2864zd2_2057));
																																																		{
																																																			obj_t
																																																				BgL_expz00_6847;
																																																			obj_t
																																																				BgL_varz00_6846;
																																																			BgL_varz00_6846
																																																				=
																																																				BgL_arg1887z00_2069;
																																																			BgL_expz00_6847
																																																				=
																																																				BgL_cdrzd2869zd2_2058;
																																																			BgL_expz00_1937
																																																				=
																																																				BgL_expz00_6847;
																																																			BgL_varz00_1936
																																																				=
																																																				BgL_varz00_6846;
																																																			goto
																																																				BgL_tagzd2368zd2_1938;
																																																		}
																																																	}
																																															}
																																														}
																																													}
																																											}
																																										}
																																									}
																																							}
																																						else
																																							{	/* Ast/unit.scm 225 */
																																								obj_t
																																									BgL_cdrzd2950zd2_2073;
																																								BgL_cdrzd2950zd2_2073
																																									=
																																									CDR
																																									(
																																									((obj_t) BgL_cdrzd2661zd2_2011));
																																								{	/* Ast/unit.scm 225 */
																																									obj_t
																																										BgL_carzd2956zd2_2074;
																																									BgL_carzd2956zd2_2074
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_cdrzd2950zd2_2073));
																																									{	/* Ast/unit.scm 225 */
																																										obj_t
																																											BgL_carzd2961zd2_2075;
																																										obj_t
																																											BgL_cdrzd2962zd2_2076;
																																										BgL_carzd2961zd2_2075
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_carzd2956zd2_2074));
																																										BgL_cdrzd2962zd2_2076
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_carzd2956zd2_2074));
																																										if (BGl_lambdazf3zf3zzast_unitz00(BgL_carzd2961zd2_2075))
																																											{	/* Ast/unit.scm 225 */
																																												if (NULLP(CDR(((obj_t) BgL_cdrzd2950zd2_2073))))
																																													{	/* Ast/unit.scm 225 */
																																														obj_t
																																															BgL_arg1893z00_2080;
																																														obj_t
																																															BgL_arg1894z00_2081;
																																														obj_t
																																															BgL_arg1896z00_2082;
																																														BgL_arg1893z00_2080
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_cdrzd2661zd2_2011));
																																														BgL_arg1894z00_2081
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_cdrzd2962zd2_2076));
																																														BgL_arg1896z00_2082
																																															=
																																															CDR
																																															(
																																															((obj_t) BgL_cdrzd2962zd2_2076));
																																														{
																																															obj_t
																																																BgL_expz00_6871;
																																															obj_t
																																																BgL_argsz00_6870;
																																															obj_t
																																																BgL_lamz00_6869;
																																															obj_t
																																																BgL_varz00_6868;
																																															BgL_varz00_6868
																																																=
																																																BgL_arg1893z00_2080;
																																															BgL_lamz00_6869
																																																=
																																																BgL_carzd2961zd2_2075;
																																															BgL_argsz00_6870
																																																=
																																																BgL_arg1894z00_2081;
																																															BgL_expz00_6871
																																																=
																																																BgL_arg1896z00_2082;
																																															BgL_expz00_1924
																																																=
																																																BgL_expz00_6871;
																																															BgL_argsz00_1923
																																																=
																																																BgL_argsz00_6870;
																																															BgL_lamz00_1922
																																																=
																																																BgL_lamz00_6869;
																																															BgL_varz00_1921
																																																=
																																																BgL_varz00_6868;
																																															goto
																																																BgL_tagzd2364zd2_1925;
																																														}
																																													}
																																												else
																																													{	/* Ast/unit.scm 225 */
																																														obj_t
																																															BgL_cdrzd21005zd2_2083;
																																														BgL_cdrzd21005zd2_2083
																																															=
																																															CDR
																																															(
																																															((obj_t) BgL_sexpz00_39));
																																														{	/* Ast/unit.scm 225 */
																																															obj_t
																																																BgL_arg1897z00_2084;
																																															obj_t
																																																BgL_arg1898z00_2085;
																																															BgL_arg1897z00_2084
																																																=
																																																CAR
																																																(
																																																((obj_t) BgL_cdrzd21005zd2_2083));
																																															BgL_arg1898z00_2085
																																																=
																																																CDR
																																																(
																																																((obj_t) BgL_cdrzd21005zd2_2083));
																																															{
																																																obj_t
																																																	BgL_expz00_6879;
																																																obj_t
																																																	BgL_varz00_6878;
																																																BgL_varz00_6878
																																																	=
																																																	BgL_arg1897z00_2084;
																																																BgL_expz00_6879
																																																	=
																																																	BgL_arg1898z00_2085;
																																																BgL_expz00_1937
																																																	=
																																																	BgL_expz00_6879;
																																																BgL_varz00_1936
																																																	=
																																																	BgL_varz00_6878;
																																																goto
																																																	BgL_tagzd2368zd2_1938;
																																															}
																																														}
																																													}
																																											}
																																										else
																																											{	/* Ast/unit.scm 225 */
																																												obj_t
																																													BgL_cdrzd21032zd2_2087;
																																												BgL_cdrzd21032zd2_2087
																																													=
																																													CDR
																																													(
																																													((obj_t) BgL_sexpz00_39));
																																												{	/* Ast/unit.scm 225 */
																																													obj_t
																																														BgL_cdrzd21037zd2_2088;
																																													BgL_cdrzd21037zd2_2088
																																														=
																																														CDR
																																														(
																																														((obj_t) BgL_cdrzd21032zd2_2087));
																																													{	/* Ast/unit.scm 225 */
																																														obj_t
																																															BgL_carzd21041zd2_2089;
																																														BgL_carzd21041zd2_2089
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_cdrzd21037zd2_2088));
																																														if (SYMBOLP(BgL_carzd21041zd2_2089))
																																															{	/* Ast/unit.scm 225 */
																																																if (NULLP(CDR(((obj_t) BgL_cdrzd21037zd2_2088))))
																																																	{	/* Ast/unit.scm 225 */
																																																		obj_t
																																																			BgL_arg1903z00_2093;
																																																		BgL_arg1903z00_2093
																																																			=
																																																			CAR
																																																			(
																																																			((obj_t) BgL_cdrzd21032zd2_2087));
																																																		{
																																																			obj_t
																																																				BgL_var2z00_6895;
																																																			obj_t
																																																				BgL_varz00_6894;
																																																			BgL_varz00_6894
																																																				=
																																																				BgL_arg1903z00_2093;
																																																			BgL_var2z00_6895
																																																				=
																																																				BgL_carzd21041zd2_2089;
																																																			BgL_var2z00_1930
																																																				=
																																																				BgL_var2z00_6895;
																																																			BgL_varz00_1929
																																																				=
																																																				BgL_varz00_6894;
																																																			goto
																																																				BgL_tagzd2366zd2_1931;
																																																		}
																																																	}
																																																else
																																																	{	/* Ast/unit.scm 225 */
																																																		obj_t
																																																			BgL_arg1904z00_2095;
																																																		BgL_arg1904z00_2095
																																																			=
																																																			CAR
																																																			(
																																																			((obj_t) BgL_cdrzd21032zd2_2087));
																																																		{
																																																			obj_t
																																																				BgL_expz00_6899;
																																																			obj_t
																																																				BgL_varz00_6898;
																																																			BgL_varz00_6898
																																																				=
																																																				BgL_arg1904z00_2095;
																																																			BgL_expz00_6899
																																																				=
																																																				BgL_cdrzd21037zd2_2088;
																																																			BgL_expz00_1937
																																																				=
																																																				BgL_expz00_6899;
																																																			BgL_varz00_1936
																																																				=
																																																				BgL_varz00_6898;
																																																			goto
																																																				BgL_tagzd2368zd2_1938;
																																																		}
																																																	}
																																															}
																																														else
																																															{	/* Ast/unit.scm 225 */
																																																obj_t
																																																	BgL_arg1911z00_2099;
																																																BgL_arg1911z00_2099
																																																	=
																																																	CAR
																																																	(
																																																	((obj_t) BgL_cdrzd21032zd2_2087));
																																																{
																																																	obj_t
																																																		BgL_expz00_6903;
																																																	obj_t
																																																		BgL_varz00_6902;
																																																	BgL_varz00_6902
																																																		=
																																																		BgL_arg1911z00_2099;
																																																	BgL_expz00_6903
																																																		=
																																																		BgL_cdrzd21037zd2_2088;
																																																	BgL_expz00_1937
																																																		=
																																																		BgL_expz00_6903;
																																																	BgL_varz00_1936
																																																		=
																																																		BgL_varz00_6902;
																																																	goto
																																																		BgL_tagzd2368zd2_1938;
																																																}
																																															}
																																													}
																																												}
																																											}
																																									}
																																								}
																																							}
																																					}
																																				else
																																					{	/* Ast/unit.scm 225 */
																																						obj_t
																																							BgL_cdrzd21118zd2_2103;
																																						BgL_cdrzd21118zd2_2103
																																							=
																																							CDR
																																							(((obj_t) BgL_cdrzd2661zd2_2011));
																																						{	/* Ast/unit.scm 225 */
																																							obj_t
																																								BgL_carzd21124zd2_2104;
																																							BgL_carzd21124zd2_2104
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_cdrzd21118zd2_2103));
																																							{	/* Ast/unit.scm 225 */
																																								obj_t
																																									BgL_carzd21129zd2_2105;
																																								obj_t
																																									BgL_cdrzd21130zd2_2106;
																																								BgL_carzd21129zd2_2105
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_carzd21124zd2_2104));
																																								BgL_cdrzd21130zd2_2106
																																									=
																																									CDR
																																									(
																																									((obj_t) BgL_carzd21124zd2_2104));
																																								if (BGl_lambdazf3zf3zzast_unitz00(BgL_carzd21129zd2_2105))
																																									{	/* Ast/unit.scm 225 */
																																										if (NULLP(CDR(((obj_t) BgL_cdrzd21118zd2_2103))))
																																											{	/* Ast/unit.scm 225 */
																																												obj_t
																																													BgL_arg1918z00_2110;
																																												obj_t
																																													BgL_arg1919z00_2111;
																																												obj_t
																																													BgL_arg1920z00_2112;
																																												BgL_arg1918z00_2110
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_cdrzd2661zd2_2011));
																																												BgL_arg1919z00_2111
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_cdrzd21130zd2_2106));
																																												BgL_arg1920z00_2112
																																													=
																																													CDR
																																													(
																																													((obj_t) BgL_cdrzd21130zd2_2106));
																																												{
																																													obj_t
																																														BgL_expz00_6927;
																																													obj_t
																																														BgL_argsz00_6926;
																																													obj_t
																																														BgL_lamz00_6925;
																																													obj_t
																																														BgL_varz00_6924;
																																													BgL_varz00_6924
																																														=
																																														BgL_arg1918z00_2110;
																																													BgL_lamz00_6925
																																														=
																																														BgL_carzd21129zd2_2105;
																																													BgL_argsz00_6926
																																														=
																																														BgL_arg1919z00_2111;
																																													BgL_expz00_6927
																																														=
																																														BgL_arg1920z00_2112;
																																													BgL_expz00_1924
																																														=
																																														BgL_expz00_6927;
																																													BgL_argsz00_1923
																																														=
																																														BgL_argsz00_6926;
																																													BgL_lamz00_1922
																																														=
																																														BgL_lamz00_6925;
																																													BgL_varz00_1921
																																														=
																																														BgL_varz00_6924;
																																													goto
																																														BgL_tagzd2364zd2_1925;
																																												}
																																											}
																																										else
																																											{	/* Ast/unit.scm 225 */
																																												obj_t
																																													BgL_cdrzd21173zd2_2113;
																																												BgL_cdrzd21173zd2_2113
																																													=
																																													CDR
																																													(
																																													((obj_t) BgL_sexpz00_39));
																																												{	/* Ast/unit.scm 225 */
																																													obj_t
																																														BgL_arg1923z00_2114;
																																													obj_t
																																														BgL_arg1924z00_2115;
																																													BgL_arg1923z00_2114
																																														=
																																														CAR
																																														(
																																														((obj_t) BgL_cdrzd21173zd2_2113));
																																													BgL_arg1924z00_2115
																																														=
																																														CDR
																																														(
																																														((obj_t) BgL_cdrzd21173zd2_2113));
																																													{
																																														obj_t
																																															BgL_expz00_6935;
																																														obj_t
																																															BgL_varz00_6934;
																																														BgL_varz00_6934
																																															=
																																															BgL_arg1923z00_2114;
																																														BgL_expz00_6935
																																															=
																																															BgL_arg1924z00_2115;
																																														BgL_expz00_1937
																																															=
																																															BgL_expz00_6935;
																																														BgL_varz00_1936
																																															=
																																															BgL_varz00_6934;
																																														goto
																																															BgL_tagzd2368zd2_1938;
																																													}
																																												}
																																											}
																																									}
																																								else
																																									{	/* Ast/unit.scm 225 */
																																										obj_t
																																											BgL_cdrzd21200zd2_2117;
																																										BgL_cdrzd21200zd2_2117
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_sexpz00_39));
																																										{	/* Ast/unit.scm 225 */
																																											obj_t
																																												BgL_cdrzd21205zd2_2118;
																																											BgL_cdrzd21205zd2_2118
																																												=
																																												CDR
																																												(
																																												((obj_t) BgL_cdrzd21200zd2_2117));
																																											{	/* Ast/unit.scm 225 */
																																												obj_t
																																													BgL_carzd21209zd2_2119;
																																												BgL_carzd21209zd2_2119
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_cdrzd21205zd2_2118));
																																												if (SYMBOLP(BgL_carzd21209zd2_2119))
																																													{	/* Ast/unit.scm 225 */
																																														if (NULLP(CDR(((obj_t) BgL_cdrzd21205zd2_2118))))
																																															{	/* Ast/unit.scm 225 */
																																																obj_t
																																																	BgL_arg1929z00_2123;
																																																BgL_arg1929z00_2123
																																																	=
																																																	CAR
																																																	(
																																																	((obj_t) BgL_cdrzd21200zd2_2117));
																																																{
																																																	obj_t
																																																		BgL_var2z00_6951;
																																																	obj_t
																																																		BgL_varz00_6950;
																																																	BgL_varz00_6950
																																																		=
																																																		BgL_arg1929z00_2123;
																																																	BgL_var2z00_6951
																																																		=
																																																		BgL_carzd21209zd2_2119;
																																																	BgL_var2z00_1930
																																																		=
																																																		BgL_var2z00_6951;
																																																	BgL_varz00_1929
																																																		=
																																																		BgL_varz00_6950;
																																																	goto
																																																		BgL_tagzd2366zd2_1931;
																																																}
																																															}
																																														else
																																															{	/* Ast/unit.scm 225 */
																																																obj_t
																																																	BgL_arg1930z00_2125;
																																																BgL_arg1930z00_2125
																																																	=
																																																	CAR
																																																	(
																																																	((obj_t) BgL_cdrzd21200zd2_2117));
																																																{
																																																	obj_t
																																																		BgL_expz00_6955;
																																																	obj_t
																																																		BgL_varz00_6954;
																																																	BgL_varz00_6954
																																																		=
																																																		BgL_arg1930z00_2125;
																																																	BgL_expz00_6955
																																																		=
																																																		BgL_cdrzd21205zd2_2118;
																																																	BgL_expz00_1937
																																																		=
																																																		BgL_expz00_6955;
																																																	BgL_varz00_1936
																																																		=
																																																		BgL_varz00_6954;
																																																	goto
																																																		BgL_tagzd2368zd2_1938;
																																																}
																																															}
																																													}
																																												else
																																													{	/* Ast/unit.scm 225 */
																																														obj_t
																																															BgL_arg1933z00_2129;
																																														BgL_arg1933z00_2129
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_cdrzd21200zd2_2117));
																																														{
																																															obj_t
																																																BgL_expz00_6959;
																																															obj_t
																																																BgL_varz00_6958;
																																															BgL_varz00_6958
																																																=
																																																BgL_arg1933z00_2129;
																																															BgL_expz00_6959
																																																=
																																																BgL_cdrzd21205zd2_2118;
																																															BgL_expz00_1937
																																																=
																																																BgL_expz00_6959;
																																															BgL_varz00_1936
																																																=
																																																BgL_varz00_6958;
																																															goto
																																																BgL_tagzd2368zd2_1938;
																																														}
																																													}
																																											}
																																										}
																																									}
																																							}
																																						}
																																					}
																																			}
																																		else
																																			{	/* Ast/unit.scm 225 */
																																				obj_t
																																					BgL_cdrzd21286zd2_2132;
																																				BgL_cdrzd21286zd2_2132
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_cdrzd2661zd2_2011));
																																				{	/* Ast/unit.scm 225 */
																																					obj_t
																																						BgL_carzd21292zd2_2133;
																																					BgL_carzd21292zd2_2133
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd21286zd2_2132));
																																					{	/* Ast/unit.scm 225 */
																																						obj_t
																																							BgL_carzd21297zd2_2134;
																																						obj_t
																																							BgL_cdrzd21298zd2_2135;
																																						BgL_carzd21297zd2_2134
																																							=
																																							CAR
																																							(((obj_t) BgL_carzd21292zd2_2133));
																																						BgL_cdrzd21298zd2_2135
																																							=
																																							CDR
																																							(((obj_t) BgL_carzd21292zd2_2133));
																																						if (BGl_lambdazf3zf3zzast_unitz00(BgL_carzd21297zd2_2134))
																																							{	/* Ast/unit.scm 225 */
																																								if (NULLP(CDR(((obj_t) BgL_cdrzd21286zd2_2132))))
																																									{	/* Ast/unit.scm 225 */
																																										obj_t
																																											BgL_arg1938z00_2139;
																																										obj_t
																																											BgL_arg1939z00_2140;
																																										obj_t
																																											BgL_arg1940z00_2141;
																																										BgL_arg1938z00_2139
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd2661zd2_2011));
																																										BgL_arg1939z00_2140
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd21298zd2_2135));
																																										BgL_arg1940z00_2141
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_cdrzd21298zd2_2135));
																																										{
																																											obj_t
																																												BgL_expz00_6983;
																																											obj_t
																																												BgL_argsz00_6982;
																																											obj_t
																																												BgL_lamz00_6981;
																																											obj_t
																																												BgL_varz00_6980;
																																											BgL_varz00_6980
																																												=
																																												BgL_arg1938z00_2139;
																																											BgL_lamz00_6981
																																												=
																																												BgL_carzd21297zd2_2134;
																																											BgL_argsz00_6982
																																												=
																																												BgL_arg1939z00_2140;
																																											BgL_expz00_6983
																																												=
																																												BgL_arg1940z00_2141;
																																											BgL_expz00_1924
																																												=
																																												BgL_expz00_6983;
																																											BgL_argsz00_1923
																																												=
																																												BgL_argsz00_6982;
																																											BgL_lamz00_1922
																																												=
																																												BgL_lamz00_6981;
																																											BgL_varz00_1921
																																												=
																																												BgL_varz00_6980;
																																											goto
																																												BgL_tagzd2364zd2_1925;
																																										}
																																									}
																																								else
																																									{	/* Ast/unit.scm 225 */
																																										obj_t
																																											BgL_cdrzd21341zd2_2142;
																																										BgL_cdrzd21341zd2_2142
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_sexpz00_39));
																																										{	/* Ast/unit.scm 225 */
																																											obj_t
																																												BgL_arg1941z00_2143;
																																											obj_t
																																												BgL_arg1942z00_2144;
																																											BgL_arg1941z00_2143
																																												=
																																												CAR
																																												(
																																												((obj_t) BgL_cdrzd21341zd2_2142));
																																											BgL_arg1942z00_2144
																																												=
																																												CDR
																																												(
																																												((obj_t) BgL_cdrzd21341zd2_2142));
																																											{
																																												obj_t
																																													BgL_expz00_6991;
																																												obj_t
																																													BgL_varz00_6990;
																																												BgL_varz00_6990
																																													=
																																													BgL_arg1941z00_2143;
																																												BgL_expz00_6991
																																													=
																																													BgL_arg1942z00_2144;
																																												BgL_expz00_1937
																																													=
																																													BgL_expz00_6991;
																																												BgL_varz00_1936
																																													=
																																													BgL_varz00_6990;
																																												goto
																																													BgL_tagzd2368zd2_1938;
																																											}
																																										}
																																									}
																																							}
																																						else
																																							{	/* Ast/unit.scm 225 */
																																								obj_t
																																									BgL_cdrzd21368zd2_2146;
																																								BgL_cdrzd21368zd2_2146
																																									=
																																									CDR
																																									(
																																									((obj_t) BgL_sexpz00_39));
																																								{	/* Ast/unit.scm 225 */
																																									obj_t
																																										BgL_cdrzd21373zd2_2147;
																																									BgL_cdrzd21373zd2_2147
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_cdrzd21368zd2_2146));
																																									{	/* Ast/unit.scm 225 */
																																										obj_t
																																											BgL_carzd21377zd2_2148;
																																										BgL_carzd21377zd2_2148
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd21373zd2_2147));
																																										if (SYMBOLP(BgL_carzd21377zd2_2148))
																																											{	/* Ast/unit.scm 225 */
																																												if (NULLP(CDR(((obj_t) BgL_cdrzd21373zd2_2147))))
																																													{	/* Ast/unit.scm 225 */
																																														obj_t
																																															BgL_arg1947z00_2152;
																																														BgL_arg1947z00_2152
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_cdrzd21368zd2_2146));
																																														{
																																															obj_t
																																																BgL_var2z00_7007;
																																															obj_t
																																																BgL_varz00_7006;
																																															BgL_varz00_7006
																																																=
																																																BgL_arg1947z00_2152;
																																															BgL_var2z00_7007
																																																=
																																																BgL_carzd21377zd2_2148;
																																															BgL_var2z00_1930
																																																=
																																																BgL_var2z00_7007;
																																															BgL_varz00_1929
																																																=
																																																BgL_varz00_7006;
																																															goto
																																																BgL_tagzd2366zd2_1931;
																																														}
																																													}
																																												else
																																													{	/* Ast/unit.scm 225 */
																																														obj_t
																																															BgL_arg1948z00_2154;
																																														BgL_arg1948z00_2154
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_cdrzd21368zd2_2146));
																																														{
																																															obj_t
																																																BgL_expz00_7011;
																																															obj_t
																																																BgL_varz00_7010;
																																															BgL_varz00_7010
																																																=
																																																BgL_arg1948z00_2154;
																																															BgL_expz00_7011
																																																=
																																																BgL_cdrzd21373zd2_2147;
																																															BgL_expz00_1937
																																																=
																																																BgL_expz00_7011;
																																															BgL_varz00_1936
																																																=
																																																BgL_varz00_7010;
																																															goto
																																																BgL_tagzd2368zd2_1938;
																																														}
																																													}
																																											}
																																										else
																																											{	/* Ast/unit.scm 225 */
																																												obj_t
																																													BgL_arg1951z00_2158;
																																												BgL_arg1951z00_2158
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_cdrzd21368zd2_2146));
																																												{
																																													obj_t
																																														BgL_expz00_7015;
																																													obj_t
																																														BgL_varz00_7014;
																																													BgL_varz00_7014
																																														=
																																														BgL_arg1951z00_2158;
																																													BgL_expz00_7015
																																														=
																																														BgL_cdrzd21373zd2_2147;
																																													BgL_expz00_1937
																																														=
																																														BgL_expz00_7015;
																																													BgL_varz00_1936
																																														=
																																														BgL_varz00_7014;
																																													goto
																																														BgL_tagzd2368zd2_1938;
																																												}
																																											}
																																									}
																																								}
																																							}
																																					}
																																				}
																																			}
																																	}
																																else
																																	{	/* Ast/unit.scm 225 */
																																		obj_t
																																			BgL_cdrzd21454zd2_2162;
																																		BgL_cdrzd21454zd2_2162
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_cdrzd2661zd2_2011));
																																		{	/* Ast/unit.scm 225 */
																																			obj_t
																																				BgL_carzd21460zd2_2163;
																																			BgL_carzd21460zd2_2163
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd21454zd2_2162));
																																			{	/* Ast/unit.scm 225 */
																																				obj_t
																																					BgL_carzd21465zd2_2164;
																																				obj_t
																																					BgL_cdrzd21466zd2_2165;
																																				BgL_carzd21465zd2_2164
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_carzd21460zd2_2163));
																																				BgL_cdrzd21466zd2_2165
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_carzd21460zd2_2163));
																																				if (BGl_lambdazf3zf3zzast_unitz00(BgL_carzd21465zd2_2164))
																																					{	/* Ast/unit.scm 225 */
																																						if (NULLP(CDR(((obj_t) BgL_cdrzd21454zd2_2162))))
																																							{	/* Ast/unit.scm 225 */
																																								obj_t
																																									BgL_arg1957z00_2169;
																																								obj_t
																																									BgL_arg1958z00_2170;
																																								obj_t
																																									BgL_arg1959z00_2171;
																																								BgL_arg1957z00_2169
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd2661zd2_2011));
																																								BgL_arg1958z00_2170
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd21466zd2_2165));
																																								BgL_arg1959z00_2171
																																									=
																																									CDR
																																									(
																																									((obj_t) BgL_cdrzd21466zd2_2165));
																																								{
																																									obj_t
																																										BgL_expz00_7039;
																																									obj_t
																																										BgL_argsz00_7038;
																																									obj_t
																																										BgL_lamz00_7037;
																																									obj_t
																																										BgL_varz00_7036;
																																									BgL_varz00_7036
																																										=
																																										BgL_arg1957z00_2169;
																																									BgL_lamz00_7037
																																										=
																																										BgL_carzd21465zd2_2164;
																																									BgL_argsz00_7038
																																										=
																																										BgL_arg1958z00_2170;
																																									BgL_expz00_7039
																																										=
																																										BgL_arg1959z00_2171;
																																									BgL_expz00_1924
																																										=
																																										BgL_expz00_7039;
																																									BgL_argsz00_1923
																																										=
																																										BgL_argsz00_7038;
																																									BgL_lamz00_1922
																																										=
																																										BgL_lamz00_7037;
																																									BgL_varz00_1921
																																										=
																																										BgL_varz00_7036;
																																									goto
																																										BgL_tagzd2364zd2_1925;
																																								}
																																							}
																																						else
																																							{	/* Ast/unit.scm 225 */
																																								obj_t
																																									BgL_cdrzd21509zd2_2172;
																																								BgL_cdrzd21509zd2_2172
																																									=
																																									CDR
																																									(
																																									((obj_t) BgL_sexpz00_39));
																																								{	/* Ast/unit.scm 225 */
																																									obj_t
																																										BgL_arg1960z00_2173;
																																									obj_t
																																										BgL_arg1961z00_2174;
																																									BgL_arg1960z00_2173
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_cdrzd21509zd2_2172));
																																									BgL_arg1961z00_2174
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_cdrzd21509zd2_2172));
																																									{
																																										obj_t
																																											BgL_expz00_7047;
																																										obj_t
																																											BgL_varz00_7046;
																																										BgL_varz00_7046
																																											=
																																											BgL_arg1960z00_2173;
																																										BgL_expz00_7047
																																											=
																																											BgL_arg1961z00_2174;
																																										BgL_expz00_1937
																																											=
																																											BgL_expz00_7047;
																																										BgL_varz00_1936
																																											=
																																											BgL_varz00_7046;
																																										goto
																																											BgL_tagzd2368zd2_1938;
																																									}
																																								}
																																							}
																																					}
																																				else
																																					{	/* Ast/unit.scm 225 */
																																						obj_t
																																							BgL_cdrzd21536zd2_2176;
																																						BgL_cdrzd21536zd2_2176
																																							=
																																							CDR
																																							(((obj_t) BgL_sexpz00_39));
																																						{	/* Ast/unit.scm 225 */
																																							obj_t
																																								BgL_cdrzd21541zd2_2177;
																																							BgL_cdrzd21541zd2_2177
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_cdrzd21536zd2_2176));
																																							{	/* Ast/unit.scm 225 */
																																								obj_t
																																									BgL_carzd21545zd2_2178;
																																								BgL_carzd21545zd2_2178
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd21541zd2_2177));
																																								if (SYMBOLP(BgL_carzd21545zd2_2178))
																																									{	/* Ast/unit.scm 225 */
																																										if (NULLP(CDR(((obj_t) BgL_cdrzd21541zd2_2177))))
																																											{	/* Ast/unit.scm 225 */
																																												obj_t
																																													BgL_arg1966z00_2182;
																																												BgL_arg1966z00_2182
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_cdrzd21536zd2_2176));
																																												{
																																													obj_t
																																														BgL_var2z00_7063;
																																													obj_t
																																														BgL_varz00_7062;
																																													BgL_varz00_7062
																																														=
																																														BgL_arg1966z00_2182;
																																													BgL_var2z00_7063
																																														=
																																														BgL_carzd21545zd2_2178;
																																													BgL_var2z00_1930
																																														=
																																														BgL_var2z00_7063;
																																													BgL_varz00_1929
																																														=
																																														BgL_varz00_7062;
																																													goto
																																														BgL_tagzd2366zd2_1931;
																																												}
																																											}
																																										else
																																											{	/* Ast/unit.scm 225 */
																																												obj_t
																																													BgL_arg1967z00_2184;
																																												BgL_arg1967z00_2184
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_cdrzd21536zd2_2176));
																																												{
																																													obj_t
																																														BgL_expz00_7067;
																																													obj_t
																																														BgL_varz00_7066;
																																													BgL_varz00_7066
																																														=
																																														BgL_arg1967z00_2184;
																																													BgL_expz00_7067
																																														=
																																														BgL_cdrzd21541zd2_2177;
																																													BgL_expz00_1937
																																														=
																																														BgL_expz00_7067;
																																													BgL_varz00_1936
																																														=
																																														BgL_varz00_7066;
																																													goto
																																														BgL_tagzd2368zd2_1938;
																																												}
																																											}
																																									}
																																								else
																																									{	/* Ast/unit.scm 225 */
																																										obj_t
																																											BgL_arg1970z00_2188;
																																										BgL_arg1970z00_2188
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd21536zd2_2176));
																																										{
																																											obj_t
																																												BgL_expz00_7071;
																																											obj_t
																																												BgL_varz00_7070;
																																											BgL_varz00_7070
																																												=
																																												BgL_arg1970z00_2188;
																																											BgL_expz00_7071
																																												=
																																												BgL_cdrzd21541zd2_2177;
																																											BgL_expz00_1937
																																												=
																																												BgL_expz00_7071;
																																											BgL_varz00_1936
																																												=
																																												BgL_varz00_7070;
																																											goto
																																												BgL_tagzd2368zd2_1938;
																																										}
																																									}
																																							}
																																						}
																																					}
																																			}
																																		}
																																	}
																															}
																														}
																													else
																														{	/* Ast/unit.scm 225 */
																															obj_t
																																BgL_cdrzd21622zd2_2191;
																															BgL_cdrzd21622zd2_2191
																																=
																																CDR(((obj_t)
																																	BgL_cdrzd2661zd2_2011));
																															{	/* Ast/unit.scm 225 */
																																obj_t
																																	BgL_carzd21628zd2_2192;
																																BgL_carzd21628zd2_2192
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd21622zd2_2191));
																																{	/* Ast/unit.scm 225 */
																																	obj_t
																																		BgL_carzd21633zd2_2193;
																																	obj_t
																																		BgL_cdrzd21634zd2_2194;
																																	BgL_carzd21633zd2_2193
																																		=
																																		CAR(((obj_t)
																																			BgL_carzd21628zd2_2192));
																																	BgL_cdrzd21634zd2_2194
																																		=
																																		CDR(((obj_t)
																																			BgL_carzd21628zd2_2192));
																																	if (BGl_lambdazf3zf3zzast_unitz00(BgL_carzd21633zd2_2193))
																																		{	/* Ast/unit.scm 225 */
																																			if (NULLP
																																				(CDR(((obj_t) BgL_cdrzd21622zd2_2191))))
																																				{	/* Ast/unit.scm 225 */
																																					obj_t
																																						BgL_arg1975z00_2198;
																																					obj_t
																																						BgL_arg1976z00_2199;
																																					obj_t
																																						BgL_arg1977z00_2200;
																																					BgL_arg1975z00_2198
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd2661zd2_2011));
																																					BgL_arg1976z00_2199
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd21634zd2_2194));
																																					BgL_arg1977z00_2200
																																						=
																																						CDR(
																																						((obj_t) BgL_cdrzd21634zd2_2194));
																																					{
																																						obj_t
																																							BgL_expz00_7095;
																																						obj_t
																																							BgL_argsz00_7094;
																																						obj_t
																																							BgL_lamz00_7093;
																																						obj_t
																																							BgL_varz00_7092;
																																						BgL_varz00_7092
																																							=
																																							BgL_arg1975z00_2198;
																																						BgL_lamz00_7093
																																							=
																																							BgL_carzd21633zd2_2193;
																																						BgL_argsz00_7094
																																							=
																																							BgL_arg1976z00_2199;
																																						BgL_expz00_7095
																																							=
																																							BgL_arg1977z00_2200;
																																						BgL_expz00_1924
																																							=
																																							BgL_expz00_7095;
																																						BgL_argsz00_1923
																																							=
																																							BgL_argsz00_7094;
																																						BgL_lamz00_1922
																																							=
																																							BgL_lamz00_7093;
																																						BgL_varz00_1921
																																							=
																																							BgL_varz00_7092;
																																						goto
																																							BgL_tagzd2364zd2_1925;
																																					}
																																				}
																																			else
																																				{	/* Ast/unit.scm 225 */
																																					obj_t
																																						BgL_cdrzd21677zd2_2201;
																																					BgL_cdrzd21677zd2_2201
																																						=
																																						CDR(
																																						((obj_t) BgL_sexpz00_39));
																																					{	/* Ast/unit.scm 225 */
																																						obj_t
																																							BgL_arg1978z00_2202;
																																						obj_t
																																							BgL_arg1979z00_2203;
																																						BgL_arg1978z00_2202
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd21677zd2_2201));
																																						BgL_arg1979z00_2203
																																							=
																																							CDR
																																							(((obj_t) BgL_cdrzd21677zd2_2201));
																																						{
																																							obj_t
																																								BgL_expz00_7103;
																																							obj_t
																																								BgL_varz00_7102;
																																							BgL_varz00_7102
																																								=
																																								BgL_arg1978z00_2202;
																																							BgL_expz00_7103
																																								=
																																								BgL_arg1979z00_2203;
																																							BgL_expz00_1937
																																								=
																																								BgL_expz00_7103;
																																							BgL_varz00_1936
																																								=
																																								BgL_varz00_7102;
																																							goto
																																								BgL_tagzd2368zd2_1938;
																																						}
																																					}
																																				}
																																		}
																																	else
																																		{	/* Ast/unit.scm 225 */
																																			obj_t
																																				BgL_cdrzd21704zd2_2205;
																																			BgL_cdrzd21704zd2_2205
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_sexpz00_39));
																																			{	/* Ast/unit.scm 225 */
																																				obj_t
																																					BgL_cdrzd21709zd2_2206;
																																				BgL_cdrzd21709zd2_2206
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_cdrzd21704zd2_2205));
																																				{	/* Ast/unit.scm 225 */
																																					obj_t
																																						BgL_carzd21713zd2_2207;
																																					BgL_carzd21713zd2_2207
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd21709zd2_2206));
																																					if (SYMBOLP(BgL_carzd21713zd2_2207))
																																						{	/* Ast/unit.scm 225 */
																																							if (NULLP(CDR(((obj_t) BgL_cdrzd21709zd2_2206))))
																																								{	/* Ast/unit.scm 225 */
																																									obj_t
																																										BgL_arg1984z00_2211;
																																									BgL_arg1984z00_2211
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_cdrzd21704zd2_2205));
																																									{
																																										obj_t
																																											BgL_var2z00_7119;
																																										obj_t
																																											BgL_varz00_7118;
																																										BgL_varz00_7118
																																											=
																																											BgL_arg1984z00_2211;
																																										BgL_var2z00_7119
																																											=
																																											BgL_carzd21713zd2_2207;
																																										BgL_var2z00_1930
																																											=
																																											BgL_var2z00_7119;
																																										BgL_varz00_1929
																																											=
																																											BgL_varz00_7118;
																																										goto
																																											BgL_tagzd2366zd2_1931;
																																									}
																																								}
																																							else
																																								{	/* Ast/unit.scm 225 */
																																									obj_t
																																										BgL_arg1985z00_2213;
																																									BgL_arg1985z00_2213
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_cdrzd21704zd2_2205));
																																									{
																																										obj_t
																																											BgL_expz00_7123;
																																										obj_t
																																											BgL_varz00_7122;
																																										BgL_varz00_7122
																																											=
																																											BgL_arg1985z00_2213;
																																										BgL_expz00_7123
																																											=
																																											BgL_cdrzd21709zd2_2206;
																																										BgL_expz00_1937
																																											=
																																											BgL_expz00_7123;
																																										BgL_varz00_1936
																																											=
																																											BgL_varz00_7122;
																																										goto
																																											BgL_tagzd2368zd2_1938;
																																									}
																																								}
																																						}
																																					else
																																						{	/* Ast/unit.scm 225 */
																																							obj_t
																																								BgL_arg1988z00_2217;
																																							BgL_arg1988z00_2217
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_cdrzd21704zd2_2205));
																																							{
																																								obj_t
																																									BgL_expz00_7127;
																																								obj_t
																																									BgL_varz00_7126;
																																								BgL_varz00_7126
																																									=
																																									BgL_arg1988z00_2217;
																																								BgL_expz00_7127
																																									=
																																									BgL_cdrzd21709zd2_2206;
																																								BgL_expz00_1937
																																									=
																																									BgL_expz00_7127;
																																								BgL_varz00_1936
																																									=
																																									BgL_varz00_7126;
																																								goto
																																									BgL_tagzd2368zd2_1938;
																																							}
																																						}
																																				}
																																			}
																																		}
																																}
																															}
																														}
																												}
																											else
																												{	/* Ast/unit.scm 225 */
																													obj_t
																														BgL_cdrzd21790zd2_2220;
																													BgL_cdrzd21790zd2_2220
																														=
																														CDR(((obj_t)
																															BgL_cdrzd2661zd2_2011));
																													{	/* Ast/unit.scm 225 */
																														obj_t
																															BgL_carzd21796zd2_2221;
																														BgL_carzd21796zd2_2221
																															=
																															CAR(((obj_t)
																																BgL_cdrzd21790zd2_2220));
																														{	/* Ast/unit.scm 225 */
																															obj_t
																																BgL_carzd21801zd2_2222;
																															obj_t
																																BgL_cdrzd21802zd2_2223;
																															BgL_carzd21801zd2_2222
																																=
																																CAR(((obj_t)
																																	BgL_carzd21796zd2_2221));
																															BgL_cdrzd21802zd2_2223
																																=
																																CDR(((obj_t)
																																	BgL_carzd21796zd2_2221));
																															if (BGl_lambdazf3zf3zzast_unitz00(BgL_carzd21801zd2_2222))
																																{	/* Ast/unit.scm 225 */
																																	if (NULLP(CDR(
																																				((obj_t)
																																					BgL_cdrzd21790zd2_2220))))
																																		{	/* Ast/unit.scm 225 */
																																			obj_t
																																				BgL_arg1993z00_2227;
																																			obj_t
																																				BgL_arg1994z00_2228;
																																			obj_t
																																				BgL_arg1995z00_2229;
																																			BgL_arg1993z00_2227
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd2661zd2_2011));
																																			BgL_arg1994z00_2228
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd21802zd2_2223));
																																			BgL_arg1995z00_2229
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_cdrzd21802zd2_2223));
																																			{
																																				obj_t
																																					BgL_expz00_7151;
																																				obj_t
																																					BgL_argsz00_7150;
																																				obj_t
																																					BgL_lamz00_7149;
																																				obj_t
																																					BgL_varz00_7148;
																																				BgL_varz00_7148
																																					=
																																					BgL_arg1993z00_2227;
																																				BgL_lamz00_7149
																																					=
																																					BgL_carzd21801zd2_2222;
																																				BgL_argsz00_7150
																																					=
																																					BgL_arg1994z00_2228;
																																				BgL_expz00_7151
																																					=
																																					BgL_arg1995z00_2229;
																																				BgL_expz00_1924
																																					=
																																					BgL_expz00_7151;
																																				BgL_argsz00_1923
																																					=
																																					BgL_argsz00_7150;
																																				BgL_lamz00_1922
																																					=
																																					BgL_lamz00_7149;
																																				BgL_varz00_1921
																																					=
																																					BgL_varz00_7148;
																																				goto
																																					BgL_tagzd2364zd2_1925;
																																			}
																																		}
																																	else
																																		{	/* Ast/unit.scm 225 */
																																			obj_t
																																				BgL_cdrzd21845zd2_2230;
																																			BgL_cdrzd21845zd2_2230
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_sexpz00_39));
																																			{	/* Ast/unit.scm 225 */
																																				obj_t
																																					BgL_arg1996z00_2231;
																																				obj_t
																																					BgL_arg1997z00_2232;
																																				BgL_arg1996z00_2231
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_cdrzd21845zd2_2230));
																																				BgL_arg1997z00_2232
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_cdrzd21845zd2_2230));
																																				{
																																					obj_t
																																						BgL_expz00_7159;
																																					obj_t
																																						BgL_varz00_7158;
																																					BgL_varz00_7158
																																						=
																																						BgL_arg1996z00_2231;
																																					BgL_expz00_7159
																																						=
																																						BgL_arg1997z00_2232;
																																					BgL_expz00_1937
																																						=
																																						BgL_expz00_7159;
																																					BgL_varz00_1936
																																						=
																																						BgL_varz00_7158;
																																					goto
																																						BgL_tagzd2368zd2_1938;
																																				}
																																			}
																																		}
																																}
																															else
																																{	/* Ast/unit.scm 225 */
																																	obj_t
																																		BgL_cdrzd21872zd2_2234;
																																	BgL_cdrzd21872zd2_2234
																																		=
																																		CDR(((obj_t)
																																			BgL_sexpz00_39));
																																	{	/* Ast/unit.scm 225 */
																																		obj_t
																																			BgL_cdrzd21877zd2_2235;
																																		BgL_cdrzd21877zd2_2235
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_cdrzd21872zd2_2234));
																																		{	/* Ast/unit.scm 225 */
																																			obj_t
																																				BgL_carzd21881zd2_2236;
																																			BgL_carzd21881zd2_2236
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd21877zd2_2235));
																																			if (SYMBOLP(BgL_carzd21881zd2_2236))
																																				{	/* Ast/unit.scm 225 */
																																					if (NULLP(CDR(((obj_t) BgL_cdrzd21877zd2_2235))))
																																						{	/* Ast/unit.scm 225 */
																																							obj_t
																																								BgL_arg2002z00_2240;
																																							BgL_arg2002z00_2240
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_cdrzd21872zd2_2234));
																																							{
																																								obj_t
																																									BgL_var2z00_7175;
																																								obj_t
																																									BgL_varz00_7174;
																																								BgL_varz00_7174
																																									=
																																									BgL_arg2002z00_2240;
																																								BgL_var2z00_7175
																																									=
																																									BgL_carzd21881zd2_2236;
																																								BgL_var2z00_1930
																																									=
																																									BgL_var2z00_7175;
																																								BgL_varz00_1929
																																									=
																																									BgL_varz00_7174;
																																								goto
																																									BgL_tagzd2366zd2_1931;
																																							}
																																						}
																																					else
																																						{	/* Ast/unit.scm 225 */
																																							obj_t
																																								BgL_arg2003z00_2242;
																																							BgL_arg2003z00_2242
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_cdrzd21872zd2_2234));
																																							{
																																								obj_t
																																									BgL_expz00_7179;
																																								obj_t
																																									BgL_varz00_7178;
																																								BgL_varz00_7178
																																									=
																																									BgL_arg2003z00_2242;
																																								BgL_expz00_7179
																																									=
																																									BgL_cdrzd21877zd2_2235;
																																								BgL_expz00_1937
																																									=
																																									BgL_expz00_7179;
																																								BgL_varz00_1936
																																									=
																																									BgL_varz00_7178;
																																								goto
																																									BgL_tagzd2368zd2_1938;
																																							}
																																						}
																																				}
																																			else
																																				{	/* Ast/unit.scm 225 */
																																					obj_t
																																						BgL_arg2007z00_2246;
																																					BgL_arg2007z00_2246
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd21872zd2_2234));
																																					{
																																						obj_t
																																							BgL_expz00_7183;
																																						obj_t
																																							BgL_varz00_7182;
																																						BgL_varz00_7182
																																							=
																																							BgL_arg2007z00_2246;
																																						BgL_expz00_7183
																																							=
																																							BgL_cdrzd21877zd2_2235;
																																						BgL_expz00_1937
																																							=
																																							BgL_expz00_7183;
																																						BgL_varz00_1936
																																							=
																																							BgL_varz00_7182;
																																						goto
																																							BgL_tagzd2368zd2_1938;
																																					}
																																				}
																																		}
																																	}
																																}
																														}
																													}
																												}
																										}
																									else
																										{	/* Ast/unit.scm 225 */
																											obj_t
																												BgL_cdrzd21973zd2_2249;
																											BgL_cdrzd21973zd2_2249 =
																												CDR(((obj_t)
																													BgL_cdrzd2661zd2_2011));
																											{	/* Ast/unit.scm 225 */
																												obj_t
																													BgL_carzd21977zd2_2250;
																												BgL_carzd21977zd2_2250 =
																													CAR(((obj_t)
																														BgL_cdrzd21973zd2_2249));
																												if (SYMBOLP
																													(BgL_carzd21977zd2_2250))
																													{	/* Ast/unit.scm 225 */
																														if (NULLP(CDR(
																																	((obj_t)
																																		BgL_cdrzd21973zd2_2249))))
																															{	/* Ast/unit.scm 225 */
																																obj_t
																																	BgL_arg2012z00_2254;
																																BgL_arg2012z00_2254
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd2661zd2_2011));
																																{
																																	obj_t
																																		BgL_var2z00_7197;
																																	obj_t
																																		BgL_varz00_7196;
																																	BgL_varz00_7196
																																		=
																																		BgL_arg2012z00_2254;
																																	BgL_var2z00_7197
																																		=
																																		BgL_carzd21977zd2_2250;
																																	BgL_var2z00_1930
																																		=
																																		BgL_var2z00_7197;
																																	BgL_varz00_1929
																																		=
																																		BgL_varz00_7196;
																																	goto
																																		BgL_tagzd2366zd2_1931;
																																}
																															}
																														else
																															{	/* Ast/unit.scm 225 */
																																obj_t
																																	BgL_cdrzd21999zd2_2255;
																																BgL_cdrzd21999zd2_2255
																																	=
																																	CDR(((obj_t)
																																		BgL_sexpz00_39));
																																{	/* Ast/unit.scm 225 */
																																	obj_t
																																		BgL_arg2013z00_2256;
																																	obj_t
																																		BgL_arg2014z00_2257;
																																	BgL_arg2013z00_2256
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd21999zd2_2255));
																																	BgL_arg2014z00_2257
																																		=
																																		CDR(((obj_t)
																																			BgL_cdrzd21999zd2_2255));
																																	{
																																		obj_t
																																			BgL_expz00_7205;
																																		obj_t
																																			BgL_varz00_7204;
																																		BgL_varz00_7204
																																			=
																																			BgL_arg2013z00_2256;
																																		BgL_expz00_7205
																																			=
																																			BgL_arg2014z00_2257;
																																		BgL_expz00_1937
																																			=
																																			BgL_expz00_7205;
																																		BgL_varz00_1936
																																			=
																																			BgL_varz00_7204;
																																		goto
																																			BgL_tagzd2368zd2_1938;
																																	}
																																}
																															}
																													}
																												else
																													{	/* Ast/unit.scm 225 */
																														obj_t
																															BgL_cdrzd22025zd2_2259;
																														BgL_cdrzd22025zd2_2259
																															=
																															CDR(((obj_t)
																																BgL_sexpz00_39));
																														{	/* Ast/unit.scm 225 */
																															obj_t
																																BgL_arg2016z00_2260;
																															obj_t
																																BgL_arg2017z00_2261;
																															BgL_arg2016z00_2260
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd22025zd2_2259));
																															BgL_arg2017z00_2261
																																=
																																CDR(((obj_t)
																																	BgL_cdrzd22025zd2_2259));
																															{
																																obj_t
																																	BgL_expz00_7213;
																																obj_t
																																	BgL_varz00_7212;
																																BgL_varz00_7212
																																	=
																																	BgL_arg2016z00_2260;
																																BgL_expz00_7213
																																	=
																																	BgL_arg2017z00_2261;
																																BgL_expz00_1937
																																	=
																																	BgL_expz00_7213;
																																BgL_varz00_1936
																																	=
																																	BgL_varz00_7212;
																																goto
																																	BgL_tagzd2368zd2_1938;
																															}
																														}
																													}
																											}
																										}
																								}
																							else
																								{	/* Ast/unit.scm 225 */
																									obj_t BgL_cdrzd22054zd2_2263;

																									BgL_cdrzd22054zd2_2263 =
																										CDR(
																										((obj_t)
																											BgL_cdrzd2661zd2_2011));
																									{	/* Ast/unit.scm 225 */
																										obj_t
																											BgL_carzd22060zd2_2264;
																										BgL_carzd22060zd2_2264 =
																											CAR(((obj_t)
																												BgL_cdrzd22054zd2_2263));
																										{	/* Ast/unit.scm 225 */
																											obj_t
																												BgL_carzd22065zd2_2265;
																											obj_t
																												BgL_cdrzd22066zd2_2266;
																											BgL_carzd22065zd2_2265 =
																												CAR(((obj_t)
																													BgL_carzd22060zd2_2264));
																											BgL_cdrzd22066zd2_2266 =
																												CDR(((obj_t)
																													BgL_carzd22060zd2_2264));
																											if (BGl_lambdazf3zf3zzast_unitz00(BgL_carzd22065zd2_2265))
																												{	/* Ast/unit.scm 225 */
																													if (PAIRP
																														(BgL_cdrzd22066zd2_2266))
																														{	/* Ast/unit.scm 225 */
																															if (NULLP(CDR(
																																		((obj_t)
																																			BgL_cdrzd22054zd2_2263))))
																																{	/* Ast/unit.scm 225 */
																																	obj_t
																																		BgL_arg2022z00_2271;
																																	obj_t
																																		BgL_arg2024z00_2272;
																																	obj_t
																																		BgL_arg2025z00_2273;
																																	BgL_arg2022z00_2271
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd2661zd2_2011));
																																	BgL_arg2024z00_2272
																																		=
																																		CAR
																																		(BgL_cdrzd22066zd2_2266);
																																	BgL_arg2025z00_2273
																																		=
																																		CDR
																																		(BgL_cdrzd22066zd2_2266);
																																	{
																																		obj_t
																																			BgL_expz00_7237;
																																		obj_t
																																			BgL_argsz00_7236;
																																		obj_t
																																			BgL_lamz00_7235;
																																		obj_t
																																			BgL_varz00_7234;
																																		BgL_varz00_7234
																																			=
																																			BgL_arg2022z00_2271;
																																		BgL_lamz00_7235
																																			=
																																			BgL_carzd22065zd2_2265;
																																		BgL_argsz00_7236
																																			=
																																			BgL_arg2024z00_2272;
																																		BgL_expz00_7237
																																			=
																																			BgL_arg2025z00_2273;
																																		BgL_expz00_1924
																																			=
																																			BgL_expz00_7237;
																																		BgL_argsz00_1923
																																			=
																																			BgL_argsz00_7236;
																																		BgL_lamz00_1922
																																			=
																																			BgL_lamz00_7235;
																																		BgL_varz00_1921
																																			=
																																			BgL_varz00_7234;
																																		goto
																																			BgL_tagzd2364zd2_1925;
																																	}
																																}
																															else
																																{	/* Ast/unit.scm 225 */
																																	obj_t
																																		BgL_cdrzd22111zd2_2274;
																																	BgL_cdrzd22111zd2_2274
																																		=
																																		CDR(((obj_t)
																																			BgL_sexpz00_39));
																																	{	/* Ast/unit.scm 225 */
																																		obj_t
																																			BgL_arg2026z00_2275;
																																		obj_t
																																			BgL_arg2027z00_2276;
																																		BgL_arg2026z00_2275
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd22111zd2_2274));
																																		BgL_arg2027z00_2276
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_cdrzd22111zd2_2274));
																																		{
																																			obj_t
																																				BgL_expz00_7245;
																																			obj_t
																																				BgL_varz00_7244;
																																			BgL_varz00_7244
																																				=
																																				BgL_arg2026z00_2275;
																																			BgL_expz00_7245
																																				=
																																				BgL_arg2027z00_2276;
																																			BgL_expz00_1937
																																				=
																																				BgL_expz00_7245;
																																			BgL_varz00_1936
																																				=
																																				BgL_varz00_7244;
																																			goto
																																				BgL_tagzd2368zd2_1938;
																																		}
																																	}
																																}
																														}
																													else
																														{	/* Ast/unit.scm 225 */
																															obj_t
																																BgL_cdrzd22138zd2_2278;
																															BgL_cdrzd22138zd2_2278
																																=
																																CDR(((obj_t)
																																	BgL_sexpz00_39));
																															{	/* Ast/unit.scm 225 */
																																obj_t
																																	BgL_cdrzd22143zd2_2279;
																																BgL_cdrzd22143zd2_2279
																																	=
																																	CDR(((obj_t)
																																		BgL_cdrzd22138zd2_2278));
																																{	/* Ast/unit.scm 225 */
																																	obj_t
																																		BgL_carzd22147zd2_2280;
																																	BgL_carzd22147zd2_2280
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd22143zd2_2279));
																																	if (SYMBOLP
																																		(BgL_carzd22147zd2_2280))
																																		{	/* Ast/unit.scm 225 */
																																			if (NULLP
																																				(CDR(((obj_t) BgL_cdrzd22143zd2_2279))))
																																				{	/* Ast/unit.scm 225 */
																																					obj_t
																																						BgL_arg2033z00_2284;
																																					BgL_arg2033z00_2284
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd22138zd2_2278));
																																					{
																																						obj_t
																																							BgL_var2z00_7261;
																																						obj_t
																																							BgL_varz00_7260;
																																						BgL_varz00_7260
																																							=
																																							BgL_arg2033z00_2284;
																																						BgL_var2z00_7261
																																							=
																																							BgL_carzd22147zd2_2280;
																																						BgL_var2z00_1930
																																							=
																																							BgL_var2z00_7261;
																																						BgL_varz00_1929
																																							=
																																							BgL_varz00_7260;
																																						goto
																																							BgL_tagzd2366zd2_1931;
																																					}
																																				}
																																			else
																																				{	/* Ast/unit.scm 225 */
																																					obj_t
																																						BgL_arg2034z00_2286;
																																					BgL_arg2034z00_2286
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd22138zd2_2278));
																																					{
																																						obj_t
																																							BgL_expz00_7265;
																																						obj_t
																																							BgL_varz00_7264;
																																						BgL_varz00_7264
																																							=
																																							BgL_arg2034z00_2286;
																																						BgL_expz00_7265
																																							=
																																							BgL_cdrzd22143zd2_2279;
																																						BgL_expz00_1937
																																							=
																																							BgL_expz00_7265;
																																						BgL_varz00_1936
																																							=
																																							BgL_varz00_7264;
																																						goto
																																							BgL_tagzd2368zd2_1938;
																																					}
																																				}
																																		}
																																	else
																																		{	/* Ast/unit.scm 225 */
																																			obj_t
																																				BgL_arg2038z00_2290;
																																			BgL_arg2038z00_2290
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd22138zd2_2278));
																																			{
																																				obj_t
																																					BgL_expz00_7269;
																																				obj_t
																																					BgL_varz00_7268;
																																				BgL_varz00_7268
																																					=
																																					BgL_arg2038z00_2290;
																																				BgL_expz00_7269
																																					=
																																					BgL_cdrzd22143zd2_2279;
																																				BgL_expz00_1937
																																					=
																																					BgL_expz00_7269;
																																				BgL_varz00_1936
																																					=
																																					BgL_varz00_7268;
																																				goto
																																					BgL_tagzd2368zd2_1938;
																																			}
																																		}
																																}
																															}
																														}
																												}
																											else
																												{	/* Ast/unit.scm 225 */
																													obj_t
																														BgL_cdrzd22214zd2_2292;
																													BgL_cdrzd22214zd2_2292
																														=
																														CDR(((obj_t)
																															BgL_sexpz00_39));
																													{	/* Ast/unit.scm 225 */
																														obj_t
																															BgL_cdrzd22219zd2_2293;
																														BgL_cdrzd22219zd2_2293
																															=
																															CDR(((obj_t)
																																BgL_cdrzd22214zd2_2292));
																														{	/* Ast/unit.scm 225 */
																															obj_t
																																BgL_carzd22223zd2_2294;
																															BgL_carzd22223zd2_2294
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd22219zd2_2293));
																															{	/* Ast/unit.scm 225 */
																																obj_t
																																	BgL_cdrzd22227zd2_2295;
																																BgL_cdrzd22227zd2_2295
																																	=
																																	CDR(((obj_t)
																																		BgL_carzd22223zd2_2294));
																																if ((CAR((
																																				(obj_t)
																																				BgL_carzd22223zd2_2294))
																																		==
																																		CNST_TABLE_REF
																																		(2)))
																																	{	/* Ast/unit.scm 225 */
																																		if (PAIRP
																																			(BgL_cdrzd22227zd2_2295))
																																			{	/* Ast/unit.scm 225 */
																																				if (NULLP(CDR(BgL_cdrzd22227zd2_2295)))
																																					{	/* Ast/unit.scm 225 */
																																						if (NULLP(CDR(((obj_t) BgL_cdrzd22219zd2_2293))))
																																							{	/* Ast/unit.scm 225 */
																																								obj_t
																																									BgL_arg2048z00_2303;
																																								obj_t
																																									BgL_arg2049z00_2304;
																																								BgL_arg2048z00_2303
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd22214zd2_2292));
																																								BgL_arg2049z00_2304
																																									=
																																									CAR
																																									(BgL_cdrzd22227zd2_2295);
																																								BgL_varz00_1926
																																									=
																																									BgL_arg2048z00_2303;
																																								BgL_1zd2expzd2_1927
																																									=
																																									BgL_arg2049z00_2304;
																																								{	/* Ast/unit.scm 302 */
																																									bool_t
																																										BgL_test3783z00_7295;
																																									if (BGl_typezd2existszf3z21zztype_envz00(BgL_varz00_1926))
																																										{	/* Ast/unit.scm 302 */
																																											BgL_typez00_bglt
																																												BgL_arg2294z00_2644;
																																											BgL_arg2294z00_2644
																																												=
																																												BGl_findzd2typezd2zztype_envz00
																																												(BgL_varz00_1926);
																																											{	/* Ast/unit.scm 302 */
																																												obj_t
																																													BgL_classz00_4434;
																																												BgL_classz00_4434
																																													=
																																													BGl_tclassz00zzobject_classz00;
																																												{	/* Ast/unit.scm 302 */
																																													BgL_objectz00_bglt
																																														BgL_arg1807z00_4436;
																																													{	/* Ast/unit.scm 302 */
																																														obj_t
																																															BgL_tmpz00_7299;
																																														BgL_tmpz00_7299
																																															=
																																															(
																																															(obj_t)
																																															((BgL_objectz00_bglt) BgL_arg2294z00_2644));
																																														BgL_arg1807z00_4436
																																															=
																																															(BgL_objectz00_bglt)
																																															(BgL_tmpz00_7299);
																																													}
																																													if (BGL_CONDEXPAND_ISA_ARCH64())
																																														{	/* Ast/unit.scm 302 */
																																															long
																																																BgL_idxz00_4442;
																																															BgL_idxz00_4442
																																																=
																																																BGL_OBJECT_INHERITANCE_NUM
																																																(BgL_arg1807z00_4436);
																																															BgL_test3783z00_7295
																																																=
																																																(VECTOR_REF
																																																(BGl_za2inheritancesza2z00zz__objectz00,
																																																	(BgL_idxz00_4442
																																																		+
																																																		2L))
																																																==
																																																BgL_classz00_4434);
																																														}
																																													else
																																														{	/* Ast/unit.scm 302 */
																																															bool_t
																																																BgL_res3517z00_4467;
																																															{	/* Ast/unit.scm 302 */
																																																obj_t
																																																	BgL_oclassz00_4450;
																																																{	/* Ast/unit.scm 302 */
																																																	obj_t
																																																		BgL_arg1815z00_4458;
																																																	long
																																																		BgL_arg1816z00_4459;
																																																	BgL_arg1815z00_4458
																																																		=
																																																		(BGl_za2classesza2z00zz__objectz00);
																																																	{	/* Ast/unit.scm 302 */
																																																		long
																																																			BgL_arg1817z00_4460;
																																																		BgL_arg1817z00_4460
																																																			=
																																																			BGL_OBJECT_CLASS_NUM
																																																			(BgL_arg1807z00_4436);
																																																		BgL_arg1816z00_4459
																																																			=
																																																			(BgL_arg1817z00_4460
																																																			-
																																																			OBJECT_TYPE);
																																																	}
																																																	BgL_oclassz00_4450
																																																		=
																																																		VECTOR_REF
																																																		(BgL_arg1815z00_4458,
																																																		BgL_arg1816z00_4459);
																																																}
																																																{	/* Ast/unit.scm 302 */
																																																	bool_t
																																																		BgL__ortest_1115z00_4451;
																																																	BgL__ortest_1115z00_4451
																																																		=
																																																		(BgL_classz00_4434
																																																		==
																																																		BgL_oclassz00_4450);
																																																	if (BgL__ortest_1115z00_4451)
																																																		{	/* Ast/unit.scm 302 */
																																																			BgL_res3517z00_4467
																																																				=
																																																				BgL__ortest_1115z00_4451;
																																																		}
																																																	else
																																																		{	/* Ast/unit.scm 302 */
																																																			long
																																																				BgL_odepthz00_4452;
																																																			{	/* Ast/unit.scm 302 */
																																																				obj_t
																																																					BgL_arg1804z00_4453;
																																																				BgL_arg1804z00_4453
																																																					=
																																																					(BgL_oclassz00_4450);
																																																				BgL_odepthz00_4452
																																																					=
																																																					BGL_CLASS_DEPTH
																																																					(BgL_arg1804z00_4453);
																																																			}
																																																			if ((2L < BgL_odepthz00_4452))
																																																				{	/* Ast/unit.scm 302 */
																																																					obj_t
																																																						BgL_arg1802z00_4455;
																																																					{	/* Ast/unit.scm 302 */
																																																						obj_t
																																																							BgL_arg1803z00_4456;
																																																						BgL_arg1803z00_4456
																																																							=
																																																							(BgL_oclassz00_4450);
																																																						BgL_arg1802z00_4455
																																																							=
																																																							BGL_CLASS_ANCESTORS_REF
																																																							(BgL_arg1803z00_4456,
																																																							2L);
																																																					}
																																																					BgL_res3517z00_4467
																																																						=
																																																						(BgL_arg1802z00_4455
																																																						==
																																																						BgL_classz00_4434);
																																																				}
																																																			else
																																																				{	/* Ast/unit.scm 302 */
																																																					BgL_res3517z00_4467
																																																						=
																																																						(
																																																						(bool_t)
																																																						0);
																																																				}
																																																		}
																																																}
																																															}
																																															BgL_test3783z00_7295
																																																=
																																																BgL_res3517z00_4467;
																																														}
																																												}
																																											}
																																										}
																																									else
																																										{	/* Ast/unit.scm 302 */
																																											BgL_test3783z00_7295
																																												=
																																												(
																																												(bool_t)
																																												0);
																																										}
																																									if (BgL_test3783z00_7295)
																																										{	/* Ast/unit.scm 302 */
																																											BGl_errorzd2classzd2shadowz00zzast_unitz00
																																												(BgL_varz00_1926,
																																												BgL_sexpz00_39);
																																										}
																																									else
																																										{	/* Ast/unit.scm 302 */
																																											BFALSE;
																																										}
																																								}
																																								{	/* Ast/unit.scm 304 */
																																									obj_t
																																										BgL_arg2295z00_2645;
																																									{	/* Ast/unit.scm 304 */
																																										obj_t
																																											BgL_pairz00_4471;
																																										BgL_pairz00_4471
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_sexpz00_39));
																																										BgL_arg2295z00_2645
																																											=
																																											CDR
																																											(BgL_pairz00_4471);
																																									}
																																									{	/* Ast/unit.scm 304 */
																																										obj_t
																																											BgL_tmpz00_7326;
																																										BgL_tmpz00_7326
																																											=
																																											(
																																											(obj_t)
																																											BgL_arg2295z00_2645);
																																										SET_CAR
																																											(BgL_tmpz00_7326,
																																											BgL_1zd2expzd2_1927);
																																									}
																																								}
																																								{

																																									goto
																																										BGl_toplevelzd2ze3astz31zzast_unitz00;
																																								}
																																							}
																																						else
																																							{	/* Ast/unit.scm 225 */
																																								obj_t
																																									BgL_arg2050z00_2306;
																																								obj_t
																																									BgL_arg2051z00_2307;
																																								BgL_arg2050z00_2306
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd22214zd2_2292));
																																								BgL_arg2051z00_2307
																																									=
																																									CDR
																																									(
																																									((obj_t) BgL_cdrzd22214zd2_2292));
																																								{
																																									obj_t
																																										BgL_expz00_7334;
																																									obj_t
																																										BgL_varz00_7333;
																																									BgL_varz00_7333
																																										=
																																										BgL_arg2050z00_2306;
																																									BgL_expz00_7334
																																										=
																																										BgL_arg2051z00_2307;
																																									BgL_expz00_1937
																																										=
																																										BgL_expz00_7334;
																																									BgL_varz00_1936
																																										=
																																										BgL_varz00_7333;
																																									goto
																																										BgL_tagzd2368zd2_1938;
																																								}
																																							}
																																					}
																																				else
																																					{	/* Ast/unit.scm 225 */
																																						obj_t
																																							BgL_cdrzd22280zd2_2310;
																																						BgL_cdrzd22280zd2_2310
																																							=
																																							CDR
																																							(((obj_t) BgL_cdrzd22214zd2_2292));
																																						{	/* Ast/unit.scm 225 */
																																							obj_t
																																								BgL_carzd22284zd2_2311;
																																							BgL_carzd22284zd2_2311
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_cdrzd22280zd2_2310));
																																							if (SYMBOLP(BgL_carzd22284zd2_2311))
																																								{	/* Ast/unit.scm 225 */
																																									if (NULLP(CDR(((obj_t) BgL_cdrzd22280zd2_2310))))
																																										{	/* Ast/unit.scm 225 */
																																											obj_t
																																												BgL_arg2057z00_2315;
																																											BgL_arg2057z00_2315
																																												=
																																												CAR
																																												(
																																												((obj_t) BgL_cdrzd22214zd2_2292));
																																											{
																																												obj_t
																																													BgL_var2z00_7348;
																																												obj_t
																																													BgL_varz00_7347;
																																												BgL_varz00_7347
																																													=
																																													BgL_arg2057z00_2315;
																																												BgL_var2z00_7348
																																													=
																																													BgL_carzd22284zd2_2311;
																																												BgL_var2z00_1930
																																													=
																																													BgL_var2z00_7348;
																																												BgL_varz00_1929
																																													=
																																													BgL_varz00_7347;
																																												goto
																																													BgL_tagzd2366zd2_1931;
																																											}
																																										}
																																									else
																																										{	/* Ast/unit.scm 225 */
																																											obj_t
																																												BgL_cdrzd22306zd2_2316;
																																											BgL_cdrzd22306zd2_2316
																																												=
																																												CDR
																																												(
																																												((obj_t) BgL_sexpz00_39));
																																											{	/* Ast/unit.scm 225 */
																																												obj_t
																																													BgL_arg2058z00_2317;
																																												obj_t
																																													BgL_arg2059z00_2318;
																																												BgL_arg2058z00_2317
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_cdrzd22306zd2_2316));
																																												BgL_arg2059z00_2318
																																													=
																																													CDR
																																													(
																																													((obj_t) BgL_cdrzd22306zd2_2316));
																																												{
																																													obj_t
																																														BgL_expz00_7356;
																																													obj_t
																																														BgL_varz00_7355;
																																													BgL_varz00_7355
																																														=
																																														BgL_arg2058z00_2317;
																																													BgL_expz00_7356
																																														=
																																														BgL_arg2059z00_2318;
																																													BgL_expz00_1937
																																														=
																																														BgL_expz00_7356;
																																													BgL_varz00_1936
																																														=
																																														BgL_varz00_7355;
																																													goto
																																														BgL_tagzd2368zd2_1938;
																																												}
																																											}
																																										}
																																								}
																																							else
																																								{	/* Ast/unit.scm 225 */
																																									obj_t
																																										BgL_cdrzd22332zd2_2320;
																																									BgL_cdrzd22332zd2_2320
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_sexpz00_39));
																																									{	/* Ast/unit.scm 225 */
																																										obj_t
																																											BgL_arg2061z00_2321;
																																										obj_t
																																											BgL_arg2062z00_2322;
																																										BgL_arg2061z00_2321
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd22332zd2_2320));
																																										BgL_arg2062z00_2322
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_cdrzd22332zd2_2320));
																																										{
																																											obj_t
																																												BgL_expz00_7364;
																																											obj_t
																																												BgL_varz00_7363;
																																											BgL_varz00_7363
																																												=
																																												BgL_arg2061z00_2321;
																																											BgL_expz00_7364
																																												=
																																												BgL_arg2062z00_2322;
																																											BgL_expz00_1937
																																												=
																																												BgL_expz00_7364;
																																											BgL_varz00_1936
																																												=
																																												BgL_varz00_7363;
																																											goto
																																												BgL_tagzd2368zd2_1938;
																																										}
																																									}
																																								}
																																						}
																																					}
																																			}
																																		else
																																			{	/* Ast/unit.scm 225 */
																																				obj_t
																																					BgL_cdrzd22355zd2_2325;
																																				BgL_cdrzd22355zd2_2325
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_cdrzd22214zd2_2292));
																																				{	/* Ast/unit.scm 225 */
																																					obj_t
																																						BgL_carzd22359zd2_2326;
																																					BgL_carzd22359zd2_2326
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd22355zd2_2325));
																																					if (SYMBOLP(BgL_carzd22359zd2_2326))
																																						{	/* Ast/unit.scm 225 */
																																							if (NULLP(CDR(((obj_t) BgL_cdrzd22355zd2_2325))))
																																								{	/* Ast/unit.scm 225 */
																																									obj_t
																																										BgL_arg2067z00_2330;
																																									BgL_arg2067z00_2330
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_cdrzd22214zd2_2292));
																																									{
																																										obj_t
																																											BgL_var2z00_7378;
																																										obj_t
																																											BgL_varz00_7377;
																																										BgL_varz00_7377
																																											=
																																											BgL_arg2067z00_2330;
																																										BgL_var2z00_7378
																																											=
																																											BgL_carzd22359zd2_2326;
																																										BgL_var2z00_1930
																																											=
																																											BgL_var2z00_7378;
																																										BgL_varz00_1929
																																											=
																																											BgL_varz00_7377;
																																										goto
																																											BgL_tagzd2366zd2_1931;
																																									}
																																								}
																																							else
																																								{	/* Ast/unit.scm 225 */
																																									obj_t
																																										BgL_cdrzd22381zd2_2331;
																																									BgL_cdrzd22381zd2_2331
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_sexpz00_39));
																																									{	/* Ast/unit.scm 225 */
																																										obj_t
																																											BgL_arg2068z00_2332;
																																										obj_t
																																											BgL_arg2069z00_2333;
																																										BgL_arg2068z00_2332
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd22381zd2_2331));
																																										BgL_arg2069z00_2333
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_cdrzd22381zd2_2331));
																																										{
																																											obj_t
																																												BgL_expz00_7386;
																																											obj_t
																																												BgL_varz00_7385;
																																											BgL_varz00_7385
																																												=
																																												BgL_arg2068z00_2332;
																																											BgL_expz00_7386
																																												=
																																												BgL_arg2069z00_2333;
																																											BgL_expz00_1937
																																												=
																																												BgL_expz00_7386;
																																											BgL_varz00_1936
																																												=
																																												BgL_varz00_7385;
																																											goto
																																												BgL_tagzd2368zd2_1938;
																																										}
																																									}
																																								}
																																						}
																																					else
																																						{	/* Ast/unit.scm 225 */
																																							obj_t
																																								BgL_cdrzd22407zd2_2335;
																																							BgL_cdrzd22407zd2_2335
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_sexpz00_39));
																																							{	/* Ast/unit.scm 225 */
																																								obj_t
																																									BgL_arg2072z00_2336;
																																								obj_t
																																									BgL_arg2074z00_2337;
																																								BgL_arg2072z00_2336
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd22407zd2_2335));
																																								BgL_arg2074z00_2337
																																									=
																																									CDR
																																									(
																																									((obj_t) BgL_cdrzd22407zd2_2335));
																																								{
																																									obj_t
																																										BgL_expz00_7394;
																																									obj_t
																																										BgL_varz00_7393;
																																									BgL_varz00_7393
																																										=
																																										BgL_arg2072z00_2336;
																																									BgL_expz00_7394
																																										=
																																										BgL_arg2074z00_2337;
																																									BgL_expz00_1937
																																										=
																																										BgL_expz00_7394;
																																									BgL_varz00_1936
																																										=
																																										BgL_varz00_7393;
																																									goto
																																										BgL_tagzd2368zd2_1938;
																																								}
																																							}
																																						}
																																				}
																																			}
																																	}
																																else
																																	{	/* Ast/unit.scm 225 */
																																		obj_t
																																			BgL_cdrzd22430zd2_2339;
																																		BgL_cdrzd22430zd2_2339
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_cdrzd22214zd2_2292));
																																		{	/* Ast/unit.scm 225 */
																																			obj_t
																																				BgL_carzd22434zd2_2340;
																																			BgL_carzd22434zd2_2340
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd22430zd2_2339));
																																			if (SYMBOLP(BgL_carzd22434zd2_2340))
																																				{	/* Ast/unit.scm 225 */
																																					if (NULLP(CDR(((obj_t) BgL_cdrzd22430zd2_2339))))
																																						{	/* Ast/unit.scm 225 */
																																							obj_t
																																								BgL_arg2078z00_2344;
																																							BgL_arg2078z00_2344
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_cdrzd22214zd2_2292));
																																							{
																																								obj_t
																																									BgL_var2z00_7408;
																																								obj_t
																																									BgL_varz00_7407;
																																								BgL_varz00_7407
																																									=
																																									BgL_arg2078z00_2344;
																																								BgL_var2z00_7408
																																									=
																																									BgL_carzd22434zd2_2340;
																																								BgL_var2z00_1930
																																									=
																																									BgL_var2z00_7408;
																																								BgL_varz00_1929
																																									=
																																									BgL_varz00_7407;
																																								goto
																																									BgL_tagzd2366zd2_1931;
																																							}
																																						}
																																					else
																																						{	/* Ast/unit.scm 225 */
																																							obj_t
																																								BgL_cdrzd22456zd2_2345;
																																							BgL_cdrzd22456zd2_2345
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_sexpz00_39));
																																							{	/* Ast/unit.scm 225 */
																																								obj_t
																																									BgL_arg2079z00_2346;
																																								obj_t
																																									BgL_arg2080z00_2347;
																																								BgL_arg2079z00_2346
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_cdrzd22456zd2_2345));
																																								BgL_arg2080z00_2347
																																									=
																																									CDR
																																									(
																																									((obj_t) BgL_cdrzd22456zd2_2345));
																																								{
																																									obj_t
																																										BgL_expz00_7416;
																																									obj_t
																																										BgL_varz00_7415;
																																									BgL_varz00_7415
																																										=
																																										BgL_arg2079z00_2346;
																																									BgL_expz00_7416
																																										=
																																										BgL_arg2080z00_2347;
																																									BgL_expz00_1937
																																										=
																																										BgL_expz00_7416;
																																									BgL_varz00_1936
																																										=
																																										BgL_varz00_7415;
																																									goto
																																										BgL_tagzd2368zd2_1938;
																																								}
																																							}
																																						}
																																				}
																																			else
																																				{	/* Ast/unit.scm 225 */
																																					obj_t
																																						BgL_cdrzd22475zd2_2349;
																																					BgL_cdrzd22475zd2_2349
																																						=
																																						CDR(
																																						((obj_t) BgL_sexpz00_39));
																																					{	/* Ast/unit.scm 225 */
																																						obj_t
																																							BgL_cdrzd22481zd2_2350;
																																						BgL_cdrzd22481zd2_2350
																																							=
																																							CDR
																																							(((obj_t) BgL_cdrzd22475zd2_2349));
																																						{	/* Ast/unit.scm 225 */
																																							obj_t
																																								BgL_carzd22486zd2_2351;
																																							BgL_carzd22486zd2_2351
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_cdrzd22481zd2_2350));
																																							{	/* Ast/unit.scm 225 */
																																								obj_t
																																									BgL_cdrzd22491zd2_2352;
																																								BgL_cdrzd22491zd2_2352
																																									=
																																									CDR
																																									(
																																									((obj_t) BgL_carzd22486zd2_2351));
																																								if ((CAR(((obj_t) BgL_carzd22486zd2_2351)) == CNST_TABLE_REF(0)))
																																									{	/* Ast/unit.scm 225 */
																																										if (PAIRP(BgL_cdrzd22491zd2_2352))
																																											{	/* Ast/unit.scm 225 */
																																												obj_t
																																													BgL_carzd22494zd2_2356;
																																												obj_t
																																													BgL_cdrzd22495zd2_2357;
																																												BgL_carzd22494zd2_2356
																																													=
																																													CAR
																																													(BgL_cdrzd22491zd2_2352);
																																												BgL_cdrzd22495zd2_2357
																																													=
																																													CDR
																																													(BgL_cdrzd22491zd2_2352);
																																												if (SYMBOLP(BgL_carzd22494zd2_2356))
																																													{	/* Ast/unit.scm 225 */
																																														if (PAIRP(BgL_cdrzd22495zd2_2357))
																																															{	/* Ast/unit.scm 225 */
																																																obj_t
																																																	BgL_carzd22500zd2_2360;
																																																BgL_carzd22500zd2_2360
																																																	=
																																																	CAR
																																																	(BgL_cdrzd22495zd2_2357);
																																																if (SYMBOLP(BgL_carzd22500zd2_2360))
																																																	{	/* Ast/unit.scm 225 */
																																																		if (NULLP(CDR(BgL_cdrzd22495zd2_2357)))
																																																			{	/* Ast/unit.scm 225 */
																																																				if (NULLP(CDR(((obj_t) BgL_cdrzd22481zd2_2350))))
																																																					{	/* Ast/unit.scm 225 */
																																																						obj_t
																																																							BgL_arg2093z00_2366;
																																																						BgL_arg2093z00_2366
																																																							=
																																																							CAR
																																																							(
																																																							((obj_t) BgL_cdrzd22475zd2_2349));
																																																						BgL_varz00_1932
																																																							=
																																																							BgL_arg2093z00_2366;
																																																						BgL_var2z00_1933
																																																							=
																																																							BgL_carzd22494zd2_2356;
																																																						BgL_modulez00_1934
																																																							=
																																																							BgL_carzd22500zd2_2360;
																																																						{	/* Ast/unit.scm 333 */
																																																							bool_t
																																																								BgL_test3801z00_7450;
																																																							if (BGl_typezd2existszf3z21zztype_envz00(BgL_varz00_1932))
																																																								{	/* Ast/unit.scm 333 */
																																																									BgL_typez00_bglt
																																																										BgL_arg2317z00_2674;
																																																									BgL_arg2317z00_2674
																																																										=
																																																										BGl_findzd2typezd2zztype_envz00
																																																										(BgL_varz00_1932);
																																																									{	/* Ast/unit.scm 333 */
																																																										obj_t
																																																											BgL_classz00_4580;
																																																										BgL_classz00_4580
																																																											=
																																																											BGl_tclassz00zzobject_classz00;
																																																										{	/* Ast/unit.scm 333 */
																																																											BgL_objectz00_bglt
																																																												BgL_arg1807z00_4582;
																																																											{	/* Ast/unit.scm 333 */
																																																												obj_t
																																																													BgL_tmpz00_7454;
																																																												BgL_tmpz00_7454
																																																													=
																																																													(
																																																													(obj_t)
																																																													((BgL_objectz00_bglt) BgL_arg2317z00_2674));
																																																												BgL_arg1807z00_4582
																																																													=
																																																													(BgL_objectz00_bglt)
																																																													(BgL_tmpz00_7454);
																																																											}
																																																											if (BGL_CONDEXPAND_ISA_ARCH64())
																																																												{	/* Ast/unit.scm 333 */
																																																													long
																																																														BgL_idxz00_4588;
																																																													BgL_idxz00_4588
																																																														=
																																																														BGL_OBJECT_INHERITANCE_NUM
																																																														(BgL_arg1807z00_4582);
																																																													BgL_test3801z00_7450
																																																														=
																																																														(VECTOR_REF
																																																														(BGl_za2inheritancesza2z00zz__objectz00,
																																																															(BgL_idxz00_4588
																																																																+
																																																																2L))
																																																														==
																																																														BgL_classz00_4580);
																																																												}
																																																											else
																																																												{	/* Ast/unit.scm 333 */
																																																													bool_t
																																																														BgL_res3521z00_4613;
																																																													{	/* Ast/unit.scm 333 */
																																																														obj_t
																																																															BgL_oclassz00_4596;
																																																														{	/* Ast/unit.scm 333 */
																																																															obj_t
																																																																BgL_arg1815z00_4604;
																																																															long
																																																																BgL_arg1816z00_4605;
																																																															BgL_arg1815z00_4604
																																																																=
																																																																(BGl_za2classesza2z00zz__objectz00);
																																																															{	/* Ast/unit.scm 333 */
																																																																long
																																																																	BgL_arg1817z00_4606;
																																																																BgL_arg1817z00_4606
																																																																	=
																																																																	BGL_OBJECT_CLASS_NUM
																																																																	(BgL_arg1807z00_4582);
																																																																BgL_arg1816z00_4605
																																																																	=
																																																																	(BgL_arg1817z00_4606
																																																																	-
																																																																	OBJECT_TYPE);
																																																															}
																																																															BgL_oclassz00_4596
																																																																=
																																																																VECTOR_REF
																																																																(BgL_arg1815z00_4604,
																																																																BgL_arg1816z00_4605);
																																																														}
																																																														{	/* Ast/unit.scm 333 */
																																																															bool_t
																																																																BgL__ortest_1115z00_4597;
																																																															BgL__ortest_1115z00_4597
																																																																=
																																																																(BgL_classz00_4580
																																																																==
																																																																BgL_oclassz00_4596);
																																																															if (BgL__ortest_1115z00_4597)
																																																																{	/* Ast/unit.scm 333 */
																																																																	BgL_res3521z00_4613
																																																																		=
																																																																		BgL__ortest_1115z00_4597;
																																																																}
																																																															else
																																																																{	/* Ast/unit.scm 333 */
																																																																	long
																																																																		BgL_odepthz00_4598;
																																																																	{	/* Ast/unit.scm 333 */
																																																																		obj_t
																																																																			BgL_arg1804z00_4599;
																																																																		BgL_arg1804z00_4599
																																																																			=
																																																																			(BgL_oclassz00_4596);
																																																																		BgL_odepthz00_4598
																																																																			=
																																																																			BGL_CLASS_DEPTH
																																																																			(BgL_arg1804z00_4599);
																																																																	}
																																																																	if ((2L < BgL_odepthz00_4598))
																																																																		{	/* Ast/unit.scm 333 */
																																																																			obj_t
																																																																				BgL_arg1802z00_4601;
																																																																			{	/* Ast/unit.scm 333 */
																																																																				obj_t
																																																																					BgL_arg1803z00_4602;
																																																																				BgL_arg1803z00_4602
																																																																					=
																																																																					(BgL_oclassz00_4596);
																																																																				BgL_arg1802z00_4601
																																																																					=
																																																																					BGL_CLASS_ANCESTORS_REF
																																																																					(BgL_arg1803z00_4602,
																																																																					2L);
																																																																			}
																																																																			BgL_res3521z00_4613
																																																																				=
																																																																				(BgL_arg1802z00_4601
																																																																				==
																																																																				BgL_classz00_4580);
																																																																		}
																																																																	else
																																																																		{	/* Ast/unit.scm 333 */
																																																																			BgL_res3521z00_4613
																																																																				=
																																																																				(
																																																																				(bool_t)
																																																																				0);
																																																																		}
																																																																}
																																																														}
																																																													}
																																																													BgL_test3801z00_7450
																																																														=
																																																														BgL_res3521z00_4613;
																																																												}
																																																										}
																																																									}
																																																								}
																																																							else
																																																								{	/* Ast/unit.scm 333 */
																																																									BgL_test3801z00_7450
																																																										=
																																																										(
																																																										(bool_t)
																																																										0);
																																																								}
																																																							if (BgL_test3801z00_7450)
																																																								{	/* Ast/unit.scm 333 */
																																																									BGl_errorzd2classzd2shadowz00zzast_unitz00
																																																										(BgL_varz00_1932,
																																																										BgL_sexpz00_39);
																																																								}
																																																							else
																																																								{	/* Ast/unit.scm 333 */
																																																									BFALSE;
																																																								}
																																																						}
																																																						{	/* Ast/unit.scm 335 */
																																																							obj_t
																																																								BgL_defz00_2675;
																																																							BgL_defz00_2675
																																																								=
																																																								BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																																																								(BGl_idzd2ofzd2idz00zzast_identz00
																																																								(BgL_varz00_1932,
																																																									BGl_findzd2locationzd2zztools_locationz00
																																																									(BgL_sexpz00_39)),
																																																								BgL_gdefsz00_40);
																																																							{	/* Ast/unit.scm 336 */
																																																								bool_t
																																																									BgL_test3806z00_7481;
																																																								{	/* Ast/unit.scm 336 */
																																																									obj_t
																																																										BgL_tmpz00_7482;
																																																									{	/* Ast/unit.scm 336 */
																																																										obj_t
																																																											BgL_pairz00_4615;
																																																										BgL_pairz00_4615
																																																											=
																																																											CDR
																																																											(
																																																											((obj_t) BgL_defz00_2675));
																																																										BgL_tmpz00_7482
																																																											=
																																																											CAR
																																																											(BgL_pairz00_4615);
																																																									}
																																																									BgL_test3806z00_7481
																																																										=
																																																										(BgL_tmpz00_7482
																																																										==
																																																										CNST_TABLE_REF
																																																										(12));
																																																								}
																																																								if (BgL_test3806z00_7481)
																																																									{	/* Ast/unit.scm 337 */
																																																										obj_t
																																																											BgL_gz00_2679;
																																																										BgL_gz00_2679
																																																											=
																																																											BGl_findzd2globalzf2modulez20zzast_envz00
																																																											(BgL_var2z00_1933,
																																																											BgL_modulez00_1934);
																																																										{	/* Ast/unit.scm 337 */
																																																											obj_t
																																																												BgL_arityz00_2680;
																																																											{	/* Ast/unit.scm 338 */
																																																												bool_t
																																																													BgL_test3807z00_7489;
																																																												{	/* Ast/unit.scm 338 */
																																																													obj_t
																																																														BgL_classz00_4616;
																																																													BgL_classz00_4616
																																																														=
																																																														BGl_globalz00zzast_varz00;
																																																													if (BGL_OBJECTP(BgL_gz00_2679))
																																																														{	/* Ast/unit.scm 338 */
																																																															BgL_objectz00_bglt
																																																																BgL_arg1807z00_4618;
																																																															BgL_arg1807z00_4618
																																																																=
																																																																(BgL_objectz00_bglt)
																																																																(BgL_gz00_2679);
																																																															if (BGL_CONDEXPAND_ISA_ARCH64())
																																																																{	/* Ast/unit.scm 338 */
																																																																	long
																																																																		BgL_idxz00_4624;
																																																																	BgL_idxz00_4624
																																																																		=
																																																																		BGL_OBJECT_INHERITANCE_NUM
																																																																		(BgL_arg1807z00_4618);
																																																																	BgL_test3807z00_7489
																																																																		=
																																																																		(VECTOR_REF
																																																																		(BGl_za2inheritancesza2z00zz__objectz00,
																																																																			(BgL_idxz00_4624
																																																																				+
																																																																				2L))
																																																																		==
																																																																		BgL_classz00_4616);
																																																																}
																																																															else
																																																																{	/* Ast/unit.scm 338 */
																																																																	bool_t
																																																																		BgL_res3522z00_4649;
																																																																	{	/* Ast/unit.scm 338 */
																																																																		obj_t
																																																																			BgL_oclassz00_4632;
																																																																		{	/* Ast/unit.scm 338 */
																																																																			obj_t
																																																																				BgL_arg1815z00_4640;
																																																																			long
																																																																				BgL_arg1816z00_4641;
																																																																			BgL_arg1815z00_4640
																																																																				=
																																																																				(BGl_za2classesza2z00zz__objectz00);
																																																																			{	/* Ast/unit.scm 338 */
																																																																				long
																																																																					BgL_arg1817z00_4642;
																																																																				BgL_arg1817z00_4642
																																																																					=
																																																																					BGL_OBJECT_CLASS_NUM
																																																																					(BgL_arg1807z00_4618);
																																																																				BgL_arg1816z00_4641
																																																																					=
																																																																					(BgL_arg1817z00_4642
																																																																					-
																																																																					OBJECT_TYPE);
																																																																			}
																																																																			BgL_oclassz00_4632
																																																																				=
																																																																				VECTOR_REF
																																																																				(BgL_arg1815z00_4640,
																																																																				BgL_arg1816z00_4641);
																																																																		}
																																																																		{	/* Ast/unit.scm 338 */
																																																																			bool_t
																																																																				BgL__ortest_1115z00_4633;
																																																																			BgL__ortest_1115z00_4633
																																																																				=
																																																																				(BgL_classz00_4616
																																																																				==
																																																																				BgL_oclassz00_4632);
																																																																			if (BgL__ortest_1115z00_4633)
																																																																				{	/* Ast/unit.scm 338 */
																																																																					BgL_res3522z00_4649
																																																																						=
																																																																						BgL__ortest_1115z00_4633;
																																																																				}
																																																																			else
																																																																				{	/* Ast/unit.scm 338 */
																																																																					long
																																																																						BgL_odepthz00_4634;
																																																																					{	/* Ast/unit.scm 338 */
																																																																						obj_t
																																																																							BgL_arg1804z00_4635;
																																																																						BgL_arg1804z00_4635
																																																																							=
																																																																							(BgL_oclassz00_4632);
																																																																						BgL_odepthz00_4634
																																																																							=
																																																																							BGL_CLASS_DEPTH
																																																																							(BgL_arg1804z00_4635);
																																																																					}
																																																																					if ((2L < BgL_odepthz00_4634))
																																																																						{	/* Ast/unit.scm 338 */
																																																																							obj_t
																																																																								BgL_arg1802z00_4637;
																																																																							{	/* Ast/unit.scm 338 */
																																																																								obj_t
																																																																									BgL_arg1803z00_4638;
																																																																								BgL_arg1803z00_4638
																																																																									=
																																																																									(BgL_oclassz00_4632);
																																																																								BgL_arg1802z00_4637
																																																																									=
																																																																									BGL_CLASS_ANCESTORS_REF
																																																																									(BgL_arg1803z00_4638,
																																																																									2L);
																																																																							}
																																																																							BgL_res3522z00_4649
																																																																								=
																																																																								(BgL_arg1802z00_4637
																																																																								==
																																																																								BgL_classz00_4616);
																																																																						}
																																																																					else
																																																																						{	/* Ast/unit.scm 338 */
																																																																							BgL_res3522z00_4649
																																																																								=
																																																																								(
																																																																								(bool_t)
																																																																								0);
																																																																						}
																																																																				}
																																																																		}
																																																																	}
																																																																	BgL_test3807z00_7489
																																																																		=
																																																																		BgL_res3522z00_4649;
																																																																}
																																																														}
																																																													else
																																																														{	/* Ast/unit.scm 338 */
																																																															BgL_test3807z00_7489
																																																																=
																																																																(
																																																																(bool_t)
																																																																0);
																																																														}
																																																												}
																																																												if (BgL_test3807z00_7489)
																																																													{	/* Ast/unit.scm 339 */
																																																														bool_t
																																																															BgL_test3812z00_7512;
																																																														{	/* Ast/unit.scm 339 */
																																																															BgL_valuez00_bglt
																																																																BgL_arg2324z00_2687;
																																																															BgL_arg2324z00_2687
																																																																=
																																																																(
																																																																((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt) BgL_gz00_2679))))->BgL_valuez00);
																																																															{	/* Ast/unit.scm 339 */
																																																																obj_t
																																																																	BgL_classz00_4651;
																																																																BgL_classz00_4651
																																																																	=
																																																																	BGl_funz00zzast_varz00;
																																																																{	/* Ast/unit.scm 339 */
																																																																	BgL_objectz00_bglt
																																																																		BgL_arg1807z00_4653;
																																																																	{	/* Ast/unit.scm 339 */
																																																																		obj_t
																																																																			BgL_tmpz00_7516;
																																																																		BgL_tmpz00_7516
																																																																			=
																																																																			(
																																																																			(obj_t)
																																																																			((BgL_objectz00_bglt) BgL_arg2324z00_2687));
																																																																		BgL_arg1807z00_4653
																																																																			=
																																																																			(BgL_objectz00_bglt)
																																																																			(BgL_tmpz00_7516);
																																																																	}
																																																																	if (BGL_CONDEXPAND_ISA_ARCH64())
																																																																		{	/* Ast/unit.scm 339 */
																																																																			long
																																																																				BgL_idxz00_4659;
																																																																			BgL_idxz00_4659
																																																																				=
																																																																				BGL_OBJECT_INHERITANCE_NUM
																																																																				(BgL_arg1807z00_4653);
																																																																			BgL_test3812z00_7512
																																																																				=
																																																																				(VECTOR_REF
																																																																				(BGl_za2inheritancesza2z00zz__objectz00,
																																																																					(BgL_idxz00_4659
																																																																						+
																																																																						2L))
																																																																				==
																																																																				BgL_classz00_4651);
																																																																		}
																																																																	else
																																																																		{	/* Ast/unit.scm 339 */
																																																																			bool_t
																																																																				BgL_res3523z00_4684;
																																																																			{	/* Ast/unit.scm 339 */
																																																																				obj_t
																																																																					BgL_oclassz00_4667;
																																																																				{	/* Ast/unit.scm 339 */
																																																																					obj_t
																																																																						BgL_arg1815z00_4675;
																																																																					long
																																																																						BgL_arg1816z00_4676;
																																																																					BgL_arg1815z00_4675
																																																																						=
																																																																						(BGl_za2classesza2z00zz__objectz00);
																																																																					{	/* Ast/unit.scm 339 */
																																																																						long
																																																																							BgL_arg1817z00_4677;
																																																																						BgL_arg1817z00_4677
																																																																							=
																																																																							BGL_OBJECT_CLASS_NUM
																																																																							(BgL_arg1807z00_4653);
																																																																						BgL_arg1816z00_4676
																																																																							=
																																																																							(BgL_arg1817z00_4677
																																																																							-
																																																																							OBJECT_TYPE);
																																																																					}
																																																																					BgL_oclassz00_4667
																																																																						=
																																																																						VECTOR_REF
																																																																						(BgL_arg1815z00_4675,
																																																																						BgL_arg1816z00_4676);
																																																																				}
																																																																				{	/* Ast/unit.scm 339 */
																																																																					bool_t
																																																																						BgL__ortest_1115z00_4668;
																																																																					BgL__ortest_1115z00_4668
																																																																						=
																																																																						(BgL_classz00_4651
																																																																						==
																																																																						BgL_oclassz00_4667);
																																																																					if (BgL__ortest_1115z00_4668)
																																																																						{	/* Ast/unit.scm 339 */
																																																																							BgL_res3523z00_4684
																																																																								=
																																																																								BgL__ortest_1115z00_4668;
																																																																						}
																																																																					else
																																																																						{	/* Ast/unit.scm 339 */
																																																																							long
																																																																								BgL_odepthz00_4669;
																																																																							{	/* Ast/unit.scm 339 */
																																																																								obj_t
																																																																									BgL_arg1804z00_4670;
																																																																								BgL_arg1804z00_4670
																																																																									=
																																																																									(BgL_oclassz00_4667);
																																																																								BgL_odepthz00_4669
																																																																									=
																																																																									BGL_CLASS_DEPTH
																																																																									(BgL_arg1804z00_4670);
																																																																							}
																																																																							if ((2L < BgL_odepthz00_4669))
																																																																								{	/* Ast/unit.scm 339 */
																																																																									obj_t
																																																																										BgL_arg1802z00_4672;
																																																																									{	/* Ast/unit.scm 339 */
																																																																										obj_t
																																																																											BgL_arg1803z00_4673;
																																																																										BgL_arg1803z00_4673
																																																																											=
																																																																											(BgL_oclassz00_4667);
																																																																										BgL_arg1802z00_4672
																																																																											=
																																																																											BGL_CLASS_ANCESTORS_REF
																																																																											(BgL_arg1803z00_4673,
																																																																											2L);
																																																																									}
																																																																									BgL_res3523z00_4684
																																																																										=
																																																																										(BgL_arg1802z00_4672
																																																																										==
																																																																										BgL_classz00_4651);
																																																																								}
																																																																							else
																																																																								{	/* Ast/unit.scm 339 */
																																																																									BgL_res3523z00_4684
																																																																										=
																																																																										(
																																																																										(bool_t)
																																																																										0);
																																																																								}
																																																																						}
																																																																				}
																																																																			}
																																																																			BgL_test3812z00_7512
																																																																				=
																																																																				BgL_res3523z00_4684;
																																																																		}
																																																																}
																																																															}
																																																														}
																																																														if (BgL_test3812z00_7512)
																																																															{	/* Ast/unit.scm 339 */
																																																																BgL_arityz00_2680
																																																																	=
																																																																	BINT
																																																																	(BGl_globalzd2arityzd2zztools_argsz00
																																																																	(BgL_gz00_2679));
																																																															}
																																																														else
																																																															{	/* Ast/unit.scm 339 */
																																																																BgL_arityz00_2680
																																																																	=
																																																																	BFALSE;
																																																															}
																																																													}
																																																												else
																																																													{	/* Ast/unit.scm 338 */
																																																														BgL_arityz00_2680
																																																															=
																																																															BFALSE;
																																																													}
																																																											}
																																																											{	/* Ast/unit.scm 338 */

																																																												if (INTEGERP(BgL_arityz00_2680))
																																																													{	/* Ast/unit.scm 341 */
																																																														if (BGl_globalzd2optionalzf3z21zzast_varz00(BgL_gz00_2679))
																																																															{	/* Ast/unit.scm 343 */
																																																																BGl_userzd2warningzd2zztools_errorz00
																																																																	(BgL_varz00_1932,
																																																																	BGl_string3565z00zzast_unitz00,
																																																																	BgL_sexpz00_39);
																																																																BGL_TAIL
																																																																	return
																																																																	BGl_makezd2svarzd2definitionz00zzast_unitz00
																																																																	(BgL_varz00_1932,
																																																																	BgL_sexpz00_39);
																																																															}
																																																														else
																																																															{	/* Ast/unit.scm 343 */
																																																																if (BGl_globalzd2keyzf3z21zzast_varz00(BgL_gz00_2679))
																																																																	{	/* Ast/unit.scm 348 */
																																																																		BGl_userzd2warningzd2zztools_errorz00
																																																																			(BgL_varz00_1932,
																																																																			BGl_string3566z00zzast_unitz00,
																																																																			BgL_sexpz00_39);
																																																																		BGL_TAIL
																																																																			return
																																																																			BGl_makezd2svarzd2definitionz00zzast_unitz00
																																																																			(BgL_varz00_1932,
																																																																			BgL_sexpz00_39);
																																																																	}
																																																																else
																																																																	{
																																																																		obj_t
																																																																			BgL_sexpz00_7551;
																																																																		BgL_sexpz00_7551
																																																																			=
																																																																			BGl_etazd2expansezd2zzast_unitz00
																																																																			(BgL_sexpz00_39,
																																																																			BgL_arityz00_2680);
																																																																		BgL_sexpz00_39
																																																																			=
																																																																			BgL_sexpz00_7551;
																																																																		goto
																																																																			BGl_toplevelzd2ze3astz31zzast_unitz00;
																																																																	}
																																																															}
																																																													}
																																																												else
																																																													{	/* Ast/unit.scm 341 */
																																																														BGL_TAIL
																																																															return
																																																															BGl_makezd2svarzd2definitionz00zzast_unitz00
																																																															(BgL_varz00_1932,
																																																															BgL_sexpz00_39);
																																																													}
																																																											}
																																																										}
																																																									}
																																																								else
																																																									{	/* Ast/unit.scm 336 */
																																																										BGL_TAIL
																																																											return
																																																											BGl_makezd2svarzd2definitionz00zzast_unitz00
																																																											(BgL_varz00_1932,
																																																											BgL_sexpz00_39);
																																																									}
																																																							}
																																																						}
																																																					}
																																																				else
																																																					{	/* Ast/unit.scm 225 */
																																																						obj_t
																																																							BgL_arg2094z00_2368;
																																																						obj_t
																																																							BgL_arg2095z00_2369;
																																																						BgL_arg2094z00_2368
																																																							=
																																																							CAR
																																																							(
																																																							((obj_t) BgL_cdrzd22475zd2_2349));
																																																						BgL_arg2095z00_2369
																																																							=
																																																							CDR
																																																							(
																																																							((obj_t) BgL_cdrzd22475zd2_2349));
																																																						{
																																																							obj_t
																																																								BgL_expz00_7560;
																																																							obj_t
																																																								BgL_varz00_7559;
																																																							BgL_varz00_7559
																																																								=
																																																								BgL_arg2094z00_2368;
																																																							BgL_expz00_7560
																																																								=
																																																								BgL_arg2095z00_2369;
																																																							BgL_expz00_1937
																																																								=
																																																								BgL_expz00_7560;
																																																							BgL_varz00_1936
																																																								=
																																																								BgL_varz00_7559;
																																																							goto
																																																								BgL_tagzd2368zd2_1938;
																																																						}
																																																					}
																																																			}
																																																		else
																																																			{	/* Ast/unit.scm 225 */
																																																				obj_t
																																																					BgL_arg2097z00_2372;
																																																				obj_t
																																																					BgL_arg2098z00_2373;
																																																				BgL_arg2097z00_2372
																																																					=
																																																					CAR
																																																					(
																																																					((obj_t) BgL_cdrzd22475zd2_2349));
																																																				BgL_arg2098z00_2373
																																																					=
																																																					CDR
																																																					(
																																																					((obj_t) BgL_cdrzd22475zd2_2349));
																																																				{
																																																					obj_t
																																																						BgL_expz00_7566;
																																																					obj_t
																																																						BgL_varz00_7565;
																																																					BgL_varz00_7565
																																																						=
																																																						BgL_arg2097z00_2372;
																																																					BgL_expz00_7566
																																																						=
																																																						BgL_arg2098z00_2373;
																																																					BgL_expz00_1937
																																																						=
																																																						BgL_expz00_7566;
																																																					BgL_varz00_1936
																																																						=
																																																						BgL_varz00_7565;
																																																					goto
																																																						BgL_tagzd2368zd2_1938;
																																																				}
																																																			}
																																																	}
																																																else
																																																	{	/* Ast/unit.scm 225 */
																																																		obj_t
																																																			BgL_arg2100z00_2376;
																																																		obj_t
																																																			BgL_arg2101z00_2377;
																																																		BgL_arg2100z00_2376
																																																			=
																																																			CAR
																																																			(
																																																			((obj_t) BgL_cdrzd22475zd2_2349));
																																																		BgL_arg2101z00_2377
																																																			=
																																																			CDR
																																																			(
																																																			((obj_t) BgL_cdrzd22475zd2_2349));
																																																		{
																																																			obj_t
																																																				BgL_expz00_7572;
																																																			obj_t
																																																				BgL_varz00_7571;
																																																			BgL_varz00_7571
																																																				=
																																																				BgL_arg2100z00_2376;
																																																			BgL_expz00_7572
																																																				=
																																																				BgL_arg2101z00_2377;
																																																			BgL_expz00_1937
																																																				=
																																																				BgL_expz00_7572;
																																																			BgL_varz00_1936
																																																				=
																																																				BgL_varz00_7571;
																																																			goto
																																																				BgL_tagzd2368zd2_1938;
																																																		}
																																																	}
																																															}
																																														else
																																															{	/* Ast/unit.scm 225 */
																																																obj_t
																																																	BgL_arg2102z00_2379;
																																																obj_t
																																																	BgL_arg2103z00_2380;
																																																BgL_arg2102z00_2379
																																																	=
																																																	CAR
																																																	(
																																																	((obj_t) BgL_cdrzd22475zd2_2349));
																																																BgL_arg2103z00_2380
																																																	=
																																																	CDR
																																																	(
																																																	((obj_t) BgL_cdrzd22475zd2_2349));
																																																{
																																																	obj_t
																																																		BgL_expz00_7578;
																																																	obj_t
																																																		BgL_varz00_7577;
																																																	BgL_varz00_7577
																																																		=
																																																		BgL_arg2102z00_2379;
																																																	BgL_expz00_7578
																																																		=
																																																		BgL_arg2103z00_2380;
																																																	BgL_expz00_1937
																																																		=
																																																		BgL_expz00_7578;
																																																	BgL_varz00_1936
																																																		=
																																																		BgL_varz00_7577;
																																																	goto
																																																		BgL_tagzd2368zd2_1938;
																																																}
																																															}
																																													}
																																												else
																																													{	/* Ast/unit.scm 225 */
																																														obj_t
																																															BgL_arg2104z00_2382;
																																														obj_t
																																															BgL_arg2105z00_2383;
																																														BgL_arg2104z00_2382
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_cdrzd22475zd2_2349));
																																														BgL_arg2105z00_2383
																																															=
																																															CDR
																																															(
																																															((obj_t) BgL_cdrzd22475zd2_2349));
																																														{
																																															obj_t
																																																BgL_expz00_7584;
																																															obj_t
																																																BgL_varz00_7583;
																																															BgL_varz00_7583
																																																=
																																																BgL_arg2104z00_2382;
																																															BgL_expz00_7584
																																																=
																																																BgL_arg2105z00_2383;
																																															BgL_expz00_1937
																																																=
																																																BgL_expz00_7584;
																																															BgL_varz00_1936
																																																=
																																																BgL_varz00_7583;
																																															goto
																																																BgL_tagzd2368zd2_1938;
																																														}
																																													}
																																											}
																																										else
																																											{	/* Ast/unit.scm 225 */
																																												obj_t
																																													BgL_arg2106z00_2385;
																																												obj_t
																																													BgL_arg2107z00_2386;
																																												BgL_arg2106z00_2385
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_cdrzd22475zd2_2349));
																																												BgL_arg2107z00_2386
																																													=
																																													CDR
																																													(
																																													((obj_t) BgL_cdrzd22475zd2_2349));
																																												{
																																													obj_t
																																														BgL_expz00_7590;
																																													obj_t
																																														BgL_varz00_7589;
																																													BgL_varz00_7589
																																														=
																																														BgL_arg2106z00_2385;
																																													BgL_expz00_7590
																																														=
																																														BgL_arg2107z00_2386;
																																													BgL_expz00_1937
																																														=
																																														BgL_expz00_7590;
																																													BgL_varz00_1936
																																														=
																																														BgL_varz00_7589;
																																													goto
																																														BgL_tagzd2368zd2_1938;
																																												}
																																											}
																																									}
																																								else
																																									{	/* Ast/unit.scm 225 */
																																										obj_t
																																											BgL_arg2108z00_2388;
																																										obj_t
																																											BgL_arg2109z00_2389;
																																										BgL_arg2108z00_2388
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd22475zd2_2349));
																																										BgL_arg2109z00_2389
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_cdrzd22475zd2_2349));
																																										{
																																											obj_t
																																												BgL_expz00_7596;
																																											obj_t
																																												BgL_varz00_7595;
																																											BgL_varz00_7595
																																												=
																																												BgL_arg2108z00_2388;
																																											BgL_expz00_7596
																																												=
																																												BgL_arg2109z00_2389;
																																											BgL_expz00_1937
																																												=
																																												BgL_expz00_7596;
																																											BgL_varz00_1936
																																												=
																																												BgL_varz00_7595;
																																											goto
																																												BgL_tagzd2368zd2_1938;
																																										}
																																									}
																																							}
																																						}
																																					}
																																				}
																																		}
																																	}
																															}
																														}
																													}
																												}
																										}
																									}
																								}
																						}
																					}
																				}
																			}
																	}
																else
																	{	/* Ast/unit.scm 225 */
																		obj_t BgL_cdrzd22667zd2_2394;

																		BgL_cdrzd22667zd2_2394 =
																			CDR(((obj_t) BgL_sexpz00_39));
																		{	/* Ast/unit.scm 225 */
																			obj_t BgL_cdrzd22672zd2_2395;

																			BgL_cdrzd22672zd2_2395 =
																				CDR(((obj_t) BgL_cdrzd22667zd2_2394));
																			{	/* Ast/unit.scm 225 */
																				obj_t BgL_carzd22676zd2_2396;

																				BgL_carzd22676zd2_2396 =
																					CAR(((obj_t) BgL_cdrzd22672zd2_2395));
																				if (SYMBOLP(BgL_carzd22676zd2_2396))
																					{	/* Ast/unit.scm 225 */
																						if (NULLP(CDR(
																									((obj_t)
																										BgL_cdrzd22672zd2_2395))))
																							{	/* Ast/unit.scm 225 */
																								obj_t BgL_arg2117z00_2400;

																								BgL_arg2117z00_2400 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd22667zd2_2394));
																								{
																									obj_t BgL_var2z00_7612;
																									obj_t BgL_varz00_7611;

																									BgL_varz00_7611 =
																										BgL_arg2117z00_2400;
																									BgL_var2z00_7612 =
																										BgL_carzd22676zd2_2396;
																									BgL_var2z00_1930 =
																										BgL_var2z00_7612;
																									BgL_varz00_1929 =
																										BgL_varz00_7611;
																									goto BgL_tagzd2366zd2_1931;
																								}
																							}
																						else
																							{	/* Ast/unit.scm 225 */
																								obj_t BgL_arg2118z00_2402;

																								BgL_arg2118z00_2402 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd22667zd2_2394));
																								{
																									obj_t BgL_expz00_7616;
																									obj_t BgL_varz00_7615;

																									BgL_varz00_7615 =
																										BgL_arg2118z00_2402;
																									BgL_expz00_7616 =
																										BgL_cdrzd22672zd2_2395;
																									BgL_expz00_1937 =
																										BgL_expz00_7616;
																									BgL_varz00_1936 =
																										BgL_varz00_7615;
																									goto BgL_tagzd2368zd2_1938;
																								}
																							}
																					}
																				else
																					{	/* Ast/unit.scm 225 */
																						obj_t BgL_arg2121z00_2406;

																						BgL_arg2121z00_2406 =
																							CAR(
																							((obj_t) BgL_cdrzd22667zd2_2394));
																						{
																							obj_t BgL_expz00_7620;
																							obj_t BgL_varz00_7619;

																							BgL_varz00_7619 =
																								BgL_arg2121z00_2406;
																							BgL_expz00_7620 =
																								BgL_cdrzd22672zd2_2395;
																							BgL_expz00_1937 = BgL_expz00_7620;
																							BgL_varz00_1936 = BgL_varz00_7619;
																							goto BgL_tagzd2368zd2_1938;
																						}
																					}
																			}
																		}
																	}
															}
														else
															{	/* Ast/unit.scm 225 */
																obj_t BgL_cdrzd22792zd2_2408;

																BgL_cdrzd22792zd2_2408 =
																	CDR(((obj_t) BgL_sexpz00_39));
																{	/* Ast/unit.scm 225 */
																	obj_t BgL_arg2123z00_2409;
																	obj_t BgL_arg2124z00_2410;

																	BgL_arg2123z00_2409 =
																		CAR(((obj_t) BgL_cdrzd22792zd2_2408));
																	BgL_arg2124z00_2410 =
																		CDR(((obj_t) BgL_cdrzd22792zd2_2408));
																	{
																		obj_t BgL_expz00_7628;
																		obj_t BgL_varz00_7627;

																		BgL_varz00_7627 = BgL_arg2123z00_2409;
																		BgL_expz00_7628 = BgL_arg2124z00_2410;
																		BgL_expz00_1937 = BgL_expz00_7628;
																		BgL_varz00_1936 = BgL_varz00_7627;
																		goto BgL_tagzd2368zd2_1938;
																	}
																}
															}
													}
											}
										else
											{	/* Ast/unit.scm 225 */
											BgL_tagzd2374zd2_1961:
												{	/* Ast/unit.scm 396 */
													obj_t BgL_list2398z00_2784;

													BgL_list2398z00_2784 =
														MAKE_YOUNG_PAIR(BgL_sexpz00_39, BNIL);
													return BgL_list2398z00_2784;
												}
											}
									}
								else
									{	/* Ast/unit.scm 225 */
										if ((CAR(((obj_t) BgL_sexpz00_39)) == CNST_TABLE_REF(19)))
											{	/* Ast/unit.scm 225 */
												if (PAIRP(BgL_cdrzd2422zd2_1970))
													{	/* Ast/unit.scm 225 */
														obj_t BgL_carzd22976zd2_2415;

														BgL_carzd22976zd2_2415 = CAR(BgL_cdrzd2422zd2_1970);
														if (PAIRP(BgL_carzd22976zd2_2415))
															{	/* Ast/unit.scm 225 */
																obj_t BgL_carzd22981zd2_2417;

																BgL_carzd22981zd2_2417 =
																	CAR(BgL_carzd22976zd2_2415);
																if (PAIRP(BgL_carzd22981zd2_2417))
																	{	/* Ast/unit.scm 225 */
																		obj_t BgL_cdrzd22986zd2_2419;

																		BgL_cdrzd22986zd2_2419 =
																			CDR(BgL_carzd22981zd2_2417);
																		if (
																			(CAR(BgL_carzd22981zd2_2417) ==
																				CNST_TABLE_REF(0)))
																			{	/* Ast/unit.scm 225 */
																				if (PAIRP(BgL_cdrzd22986zd2_2419))
																					{	/* Ast/unit.scm 225 */
																						obj_t BgL_cdrzd22990zd2_2423;

																						BgL_cdrzd22990zd2_2423 =
																							CDR(BgL_cdrzd22986zd2_2419);
																						if (PAIRP(BgL_cdrzd22990zd2_2423))
																							{	/* Ast/unit.scm 225 */
																								if (NULLP(CDR
																										(BgL_cdrzd22990zd2_2423)))
																									{	/* Ast/unit.scm 225 */
																										BgL_varz00_1939 =
																											CAR
																											(BgL_cdrzd22986zd2_2419);
																										BgL_modulez00_1940 =
																											CAR
																											(BgL_cdrzd22990zd2_2423);
																										BgL_argsz00_1941 =
																											CDR
																											(BgL_carzd22976zd2_2415);
																										BgL_expz00_1942 =
																											CDR
																											(BgL_cdrzd2422zd2_1970);
																										{	/* Ast/unit.scm 366 */
																											bool_t
																												BgL_test3829z00_7656;
																											if (BGl_typezd2existszf3z21zztype_envz00(BgL_varz00_1939))
																												{	/* Ast/unit.scm 366 */
																													BgL_typez00_bglt
																														BgL_arg2368z00_2751;
																													BgL_arg2368z00_2751 =
																														BGl_findzd2typezd2zztype_envz00
																														(BgL_varz00_1939);
																													{	/* Ast/unit.scm 366 */
																														obj_t
																															BgL_classz00_4728;
																														BgL_classz00_4728 =
																															BGl_tclassz00zzobject_classz00;
																														{	/* Ast/unit.scm 366 */
																															BgL_objectz00_bglt
																																BgL_arg1807z00_4730;
																															{	/* Ast/unit.scm 366 */
																																obj_t
																																	BgL_tmpz00_7660;
																																BgL_tmpz00_7660
																																	=
																																	((obj_t) (
																																		(BgL_objectz00_bglt)
																																		BgL_arg2368z00_2751));
																																BgL_arg1807z00_4730
																																	=
																																	(BgL_objectz00_bglt)
																																	(BgL_tmpz00_7660);
																															}
																															if (BGL_CONDEXPAND_ISA_ARCH64())
																																{	/* Ast/unit.scm 366 */
																																	long
																																		BgL_idxz00_4736;
																																	BgL_idxz00_4736
																																		=
																																		BGL_OBJECT_INHERITANCE_NUM
																																		(BgL_arg1807z00_4730);
																																	BgL_test3829z00_7656
																																		=
																																		(VECTOR_REF
																																		(BGl_za2inheritancesza2z00zz__objectz00,
																																			(BgL_idxz00_4736
																																				+
																																				2L)) ==
																																		BgL_classz00_4728);
																																}
																															else
																																{	/* Ast/unit.scm 366 */
																																	bool_t
																																		BgL_res3525z00_4761;
																																	{	/* Ast/unit.scm 366 */
																																		obj_t
																																			BgL_oclassz00_4744;
																																		{	/* Ast/unit.scm 366 */
																																			obj_t
																																				BgL_arg1815z00_4752;
																																			long
																																				BgL_arg1816z00_4753;
																																			BgL_arg1815z00_4752
																																				=
																																				(BGl_za2classesza2z00zz__objectz00);
																																			{	/* Ast/unit.scm 366 */
																																				long
																																					BgL_arg1817z00_4754;
																																				BgL_arg1817z00_4754
																																					=
																																					BGL_OBJECT_CLASS_NUM
																																					(BgL_arg1807z00_4730);
																																				BgL_arg1816z00_4753
																																					=
																																					(BgL_arg1817z00_4754
																																					-
																																					OBJECT_TYPE);
																																			}
																																			BgL_oclassz00_4744
																																				=
																																				VECTOR_REF
																																				(BgL_arg1815z00_4752,
																																				BgL_arg1816z00_4753);
																																		}
																																		{	/* Ast/unit.scm 366 */
																																			bool_t
																																				BgL__ortest_1115z00_4745;
																																			BgL__ortest_1115z00_4745
																																				=
																																				(BgL_classz00_4728
																																				==
																																				BgL_oclassz00_4744);
																																			if (BgL__ortest_1115z00_4745)
																																				{	/* Ast/unit.scm 366 */
																																					BgL_res3525z00_4761
																																						=
																																						BgL__ortest_1115z00_4745;
																																				}
																																			else
																																				{	/* Ast/unit.scm 366 */
																																					long
																																						BgL_odepthz00_4746;
																																					{	/* Ast/unit.scm 366 */
																																						obj_t
																																							BgL_arg1804z00_4747;
																																						BgL_arg1804z00_4747
																																							=
																																							(BgL_oclassz00_4744);
																																						BgL_odepthz00_4746
																																							=
																																							BGL_CLASS_DEPTH
																																							(BgL_arg1804z00_4747);
																																					}
																																					if (
																																						(2L
																																							<
																																							BgL_odepthz00_4746))
																																						{	/* Ast/unit.scm 366 */
																																							obj_t
																																								BgL_arg1802z00_4749;
																																							{	/* Ast/unit.scm 366 */
																																								obj_t
																																									BgL_arg1803z00_4750;
																																								BgL_arg1803z00_4750
																																									=
																																									(BgL_oclassz00_4744);
																																								BgL_arg1802z00_4749
																																									=
																																									BGL_CLASS_ANCESTORS_REF
																																									(BgL_arg1803z00_4750,
																																									2L);
																																							}
																																							BgL_res3525z00_4761
																																								=
																																								(BgL_arg1802z00_4749
																																								==
																																								BgL_classz00_4728);
																																						}
																																					else
																																						{	/* Ast/unit.scm 366 */
																																							BgL_res3525z00_4761
																																								=
																																								(
																																								(bool_t)
																																								0);
																																						}
																																				}
																																		}
																																	}
																																	BgL_test3829z00_7656
																																		=
																																		BgL_res3525z00_4761;
																																}
																														}
																													}
																												}
																											else
																												{	/* Ast/unit.scm 366 */
																													BgL_test3829z00_7656 =
																														((bool_t) 0);
																												}
																											if (BgL_test3829z00_7656)
																												{	/* Ast/unit.scm 366 */
																													BGl_errorzd2classzd2shadowz00zzast_unitz00
																														(BgL_varz00_1939,
																														BgL_sexpz00_39);
																												}
																											else
																												{	/* Ast/unit.scm 366 */
																													BFALSE;
																												}
																										}
																										{	/* Ast/unit.scm 370 */
																											obj_t BgL_arg2369z00_2752;

																											{	/* Ast/unit.scm 370 */
																												obj_t
																													BgL_arg2370z00_2753;
																												{	/* Ast/unit.scm 370 */
																													obj_t
																														BgL_arg2371z00_2754;
																													{	/* Ast/unit.scm 370 */
																														obj_t
																															BgL_pairz00_4765;
																														BgL_pairz00_4765 =
																															CDR(((obj_t)
																																BgL_sexpz00_39));
																														BgL_arg2371z00_2754
																															=
																															CDR
																															(BgL_pairz00_4765);
																													}
																													BgL_arg2370z00_2753 =
																														BGl_findzd2locationzd2zztools_locationz00
																														(BgL_arg2371z00_2754);
																												}
																												BgL_arg2369z00_2752 =
																													BGl_normaliza7ezd2prognzf2errorz87zzast_unitz00
																													(BgL_expz00_1942,
																													BgL_sexpz00_39,
																													BgL_arg2370z00_2753);
																											}
																											return
																												BGl_makezd2sfunzd2definitionz00zzast_unitz00
																												(BgL_varz00_1939,
																												BgL_modulez00_1940,
																												BgL_argsz00_1941,
																												BgL_arg2369z00_2752,
																												BgL_sexpz00_39,
																												CNST_TABLE_REF(16));
																										}
																									}
																								else
																									{	/* Ast/unit.scm 225 */
																										obj_t
																											BgL_cdrzd23006zd2_2431;
																										BgL_cdrzd23006zd2_2431 =
																											CDR(((obj_t)
																												BgL_sexpz00_39));
																										{	/* Ast/unit.scm 225 */
																											obj_t
																												BgL_carzd23010zd2_2432;
																											BgL_carzd23010zd2_2432 =
																												CAR(((obj_t)
																													BgL_cdrzd23006zd2_2431));
																											{	/* Ast/unit.scm 225 */
																												obj_t
																													BgL_arg2141z00_2433;
																												obj_t
																													BgL_arg2142z00_2434;
																												obj_t
																													BgL_arg2143z00_2435;
																												BgL_arg2141z00_2433 =
																													CAR(((obj_t)
																														BgL_carzd23010zd2_2432));
																												BgL_arg2142z00_2434 =
																													CDR(((obj_t)
																														BgL_carzd23010zd2_2432));
																												BgL_arg2143z00_2435 =
																													CDR(((obj_t)
																														BgL_cdrzd23006zd2_2431));
																												BgL_varz00_1944 =
																													BgL_arg2141z00_2433;
																												BgL_argsz00_1945 =
																													BgL_arg2142z00_2434;
																												BgL_expz00_1946 =
																													BgL_arg2143z00_2435;
																											BgL_tagzd2370zd2_1947:
																												{	/* Ast/unit.scm 373 */
																													bool_t
																														BgL_test3834z00_7705;
																													if (BGl_typezd2existszf3z21zztype_envz00(BgL_varz00_1944))
																														{	/* Ast/unit.scm 373 */
																															BgL_typez00_bglt
																																BgL_arg2375z00_2759;
																															BgL_arg2375z00_2759
																																=
																																BGl_findzd2typezd2zztype_envz00
																																(BgL_varz00_1944);
																															{	/* Ast/unit.scm 373 */
																																obj_t
																																	BgL_classz00_4766;
																																BgL_classz00_4766
																																	=
																																	BGl_tclassz00zzobject_classz00;
																																{	/* Ast/unit.scm 373 */
																																	BgL_objectz00_bglt
																																		BgL_arg1807z00_4768;
																																	{	/* Ast/unit.scm 373 */
																																		obj_t
																																			BgL_tmpz00_7709;
																																		BgL_tmpz00_7709
																																			=
																																			((obj_t) (
																																				(BgL_objectz00_bglt)
																																				BgL_arg2375z00_2759));
																																		BgL_arg1807z00_4768
																																			=
																																			(BgL_objectz00_bglt)
																																			(BgL_tmpz00_7709);
																																	}
																																	if (BGL_CONDEXPAND_ISA_ARCH64())
																																		{	/* Ast/unit.scm 373 */
																																			long
																																				BgL_idxz00_4774;
																																			BgL_idxz00_4774
																																				=
																																				BGL_OBJECT_INHERITANCE_NUM
																																				(BgL_arg1807z00_4768);
																																			BgL_test3834z00_7705
																																				=
																																				(VECTOR_REF
																																				(BGl_za2inheritancesza2z00zz__objectz00,
																																					(BgL_idxz00_4774
																																						+
																																						2L))
																																				==
																																				BgL_classz00_4766);
																																		}
																																	else
																																		{	/* Ast/unit.scm 373 */
																																			bool_t
																																				BgL_res3526z00_4799;
																																			{	/* Ast/unit.scm 373 */
																																				obj_t
																																					BgL_oclassz00_4782;
																																				{	/* Ast/unit.scm 373 */
																																					obj_t
																																						BgL_arg1815z00_4790;
																																					long
																																						BgL_arg1816z00_4791;
																																					BgL_arg1815z00_4790
																																						=
																																						(BGl_za2classesza2z00zz__objectz00);
																																					{	/* Ast/unit.scm 373 */
																																						long
																																							BgL_arg1817z00_4792;
																																						BgL_arg1817z00_4792
																																							=
																																							BGL_OBJECT_CLASS_NUM
																																							(BgL_arg1807z00_4768);
																																						BgL_arg1816z00_4791
																																							=
																																							(BgL_arg1817z00_4792
																																							-
																																							OBJECT_TYPE);
																																					}
																																					BgL_oclassz00_4782
																																						=
																																						VECTOR_REF
																																						(BgL_arg1815z00_4790,
																																						BgL_arg1816z00_4791);
																																				}
																																				{	/* Ast/unit.scm 373 */
																																					bool_t
																																						BgL__ortest_1115z00_4783;
																																					BgL__ortest_1115z00_4783
																																						=
																																						(BgL_classz00_4766
																																						==
																																						BgL_oclassz00_4782);
																																					if (BgL__ortest_1115z00_4783)
																																						{	/* Ast/unit.scm 373 */
																																							BgL_res3526z00_4799
																																								=
																																								BgL__ortest_1115z00_4783;
																																						}
																																					else
																																						{	/* Ast/unit.scm 373 */
																																							long
																																								BgL_odepthz00_4784;
																																							{	/* Ast/unit.scm 373 */
																																								obj_t
																																									BgL_arg1804z00_4785;
																																								BgL_arg1804z00_4785
																																									=
																																									(BgL_oclassz00_4782);
																																								BgL_odepthz00_4784
																																									=
																																									BGL_CLASS_DEPTH
																																									(BgL_arg1804z00_4785);
																																							}
																																							if ((2L < BgL_odepthz00_4784))
																																								{	/* Ast/unit.scm 373 */
																																									obj_t
																																										BgL_arg1802z00_4787;
																																									{	/* Ast/unit.scm 373 */
																																										obj_t
																																											BgL_arg1803z00_4788;
																																										BgL_arg1803z00_4788
																																											=
																																											(BgL_oclassz00_4782);
																																										BgL_arg1802z00_4787
																																											=
																																											BGL_CLASS_ANCESTORS_REF
																																											(BgL_arg1803z00_4788,
																																											2L);
																																									}
																																									BgL_res3526z00_4799
																																										=
																																										(BgL_arg1802z00_4787
																																										==
																																										BgL_classz00_4766);
																																								}
																																							else
																																								{	/* Ast/unit.scm 373 */
																																									BgL_res3526z00_4799
																																										=
																																										(
																																										(bool_t)
																																										0);
																																								}
																																						}
																																				}
																																			}
																																			BgL_test3834z00_7705
																																				=
																																				BgL_res3526z00_4799;
																																		}
																																}
																															}
																														}
																													else
																														{	/* Ast/unit.scm 373 */
																															BgL_test3834z00_7705
																																= ((bool_t) 0);
																														}
																													if (BgL_test3834z00_7705)
																														{	/* Ast/unit.scm 373 */
																															BGl_errorzd2classzd2shadowz00zzast_unitz00
																																(BgL_varz00_1944,
																																BgL_sexpz00_39);
																														}
																													else
																														{	/* Ast/unit.scm 373 */
																															BFALSE;
																														}
																												}
																												{	/* Ast/unit.scm 377 */
																													obj_t
																														BgL_arg2376z00_2760;
																													{	/* Ast/unit.scm 377 */
																														obj_t
																															BgL_arg2377z00_2761;
																														{	/* Ast/unit.scm 377 */
																															obj_t
																																BgL_arg2378z00_2762;
																															{	/* Ast/unit.scm 377 */
																																obj_t
																																	BgL_pairz00_4803;
																																BgL_pairz00_4803
																																	=
																																	CDR(((obj_t)
																																		BgL_sexpz00_39));
																																BgL_arg2378z00_2762
																																	=
																																	CDR
																																	(BgL_pairz00_4803);
																															}
																															BgL_arg2377z00_2761
																																=
																																BGl_findzd2locationzd2zztools_locationz00
																																(BgL_arg2378z00_2762);
																														}
																														BgL_arg2376z00_2760
																															=
																															BGl_normaliza7ezd2prognzf2errorz87zzast_unitz00
																															(BgL_expz00_1946,
																															BgL_sexpz00_39,
																															BgL_arg2377z00_2761);
																													}
																													return
																														BGl_makezd2sfunzd2definitionz00zzast_unitz00
																														(BgL_varz00_1944,
																														BGl_za2moduleza2z00zzmodule_modulez00,
																														BgL_argsz00_1945,
																														BgL_arg2376z00_2760,
																														BgL_sexpz00_39,
																														CNST_TABLE_REF(16));
																												}
																											}
																										}
																									}
																							}
																						else
																							{	/* Ast/unit.scm 225 */
																								obj_t BgL_cdrzd23027zd2_2437;

																								BgL_cdrzd23027zd2_2437 =
																									CDR(((obj_t) BgL_sexpz00_39));
																								{	/* Ast/unit.scm 225 */
																									obj_t BgL_carzd23031zd2_2438;

																									BgL_carzd23031zd2_2438 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd23027zd2_2437));
																									{	/* Ast/unit.scm 225 */
																										obj_t BgL_arg2145z00_2439;
																										obj_t BgL_arg2146z00_2440;
																										obj_t BgL_arg2147z00_2441;

																										BgL_arg2145z00_2439 =
																											CAR(
																											((obj_t)
																												BgL_carzd23031zd2_2438));
																										BgL_arg2146z00_2440 =
																											CDR(((obj_t)
																												BgL_carzd23031zd2_2438));
																										BgL_arg2147z00_2441 =
																											CDR(((obj_t)
																												BgL_cdrzd23027zd2_2437));
																										{
																											obj_t BgL_expz00_7752;
																											obj_t BgL_argsz00_7751;
																											obj_t BgL_varz00_7750;

																											BgL_varz00_7750 =
																												BgL_arg2145z00_2439;
																											BgL_argsz00_7751 =
																												BgL_arg2146z00_2440;
																											BgL_expz00_7752 =
																												BgL_arg2147z00_2441;
																											BgL_expz00_1946 =
																												BgL_expz00_7752;
																											BgL_argsz00_1945 =
																												BgL_argsz00_7751;
																											BgL_varz00_1944 =
																												BgL_varz00_7750;
																											goto
																												BgL_tagzd2370zd2_1947;
																										}
																									}
																								}
																							}
																					}
																				else
																					{	/* Ast/unit.scm 225 */
																						obj_t BgL_cdrzd23048zd2_2442;

																						BgL_cdrzd23048zd2_2442 =
																							CDR(((obj_t) BgL_sexpz00_39));
																						{	/* Ast/unit.scm 225 */
																							obj_t BgL_carzd23052zd2_2443;

																							BgL_carzd23052zd2_2443 =
																								CAR(
																								((obj_t)
																									BgL_cdrzd23048zd2_2442));
																							{	/* Ast/unit.scm 225 */
																								obj_t BgL_arg2148z00_2444;
																								obj_t BgL_arg2149z00_2445;
																								obj_t BgL_arg2150z00_2446;

																								BgL_arg2148z00_2444 =
																									CAR(
																									((obj_t)
																										BgL_carzd23052zd2_2443));
																								BgL_arg2149z00_2445 =
																									CDR(((obj_t)
																										BgL_carzd23052zd2_2443));
																								BgL_arg2150z00_2446 =
																									CDR(((obj_t)
																										BgL_cdrzd23048zd2_2442));
																								{
																									obj_t BgL_expz00_7765;
																									obj_t BgL_argsz00_7764;
																									obj_t BgL_varz00_7763;

																									BgL_varz00_7763 =
																										BgL_arg2148z00_2444;
																									BgL_argsz00_7764 =
																										BgL_arg2149z00_2445;
																									BgL_expz00_7765 =
																										BgL_arg2150z00_2446;
																									BgL_expz00_1946 =
																										BgL_expz00_7765;
																									BgL_argsz00_1945 =
																										BgL_argsz00_7764;
																									BgL_varz00_1944 =
																										BgL_varz00_7763;
																									goto BgL_tagzd2370zd2_1947;
																								}
																							}
																						}
																					}
																			}
																		else
																			{	/* Ast/unit.scm 225 */
																				obj_t BgL_cdrzd23069zd2_2447;

																				BgL_cdrzd23069zd2_2447 =
																					CDR(((obj_t) BgL_sexpz00_39));
																				{	/* Ast/unit.scm 225 */
																					obj_t BgL_carzd23073zd2_2448;

																					BgL_carzd23073zd2_2448 =
																						CAR(
																						((obj_t) BgL_cdrzd23069zd2_2447));
																					{	/* Ast/unit.scm 225 */
																						obj_t BgL_arg2151z00_2449;
																						obj_t BgL_arg2152z00_2450;
																						obj_t BgL_arg2154z00_2451;

																						BgL_arg2151z00_2449 =
																							CAR(
																							((obj_t) BgL_carzd23073zd2_2448));
																						BgL_arg2152z00_2450 =
																							CDR(
																							((obj_t) BgL_carzd23073zd2_2448));
																						BgL_arg2154z00_2451 =
																							CDR(
																							((obj_t) BgL_cdrzd23069zd2_2447));
																						{
																							obj_t BgL_expz00_7778;
																							obj_t BgL_argsz00_7777;
																							obj_t BgL_varz00_7776;

																							BgL_varz00_7776 =
																								BgL_arg2151z00_2449;
																							BgL_argsz00_7777 =
																								BgL_arg2152z00_2450;
																							BgL_expz00_7778 =
																								BgL_arg2154z00_2451;
																							BgL_expz00_1946 = BgL_expz00_7778;
																							BgL_argsz00_1945 =
																								BgL_argsz00_7777;
																							BgL_varz00_1944 = BgL_varz00_7776;
																							goto BgL_tagzd2370zd2_1947;
																						}
																					}
																				}
																			}
																	}
																else
																	{	/* Ast/unit.scm 225 */
																		obj_t BgL_cdrzd23090zd2_2453;

																		BgL_cdrzd23090zd2_2453 =
																			CDR(((obj_t) BgL_sexpz00_39));
																		{	/* Ast/unit.scm 225 */
																			obj_t BgL_carzd23094zd2_2454;

																			BgL_carzd23094zd2_2454 =
																				CAR(((obj_t) BgL_cdrzd23090zd2_2453));
																			{	/* Ast/unit.scm 225 */
																				obj_t BgL_arg2156z00_2455;
																				obj_t BgL_arg2157z00_2456;
																				obj_t BgL_arg2158z00_2457;

																				BgL_arg2156z00_2455 =
																					CAR(((obj_t) BgL_carzd23094zd2_2454));
																				BgL_arg2157z00_2456 =
																					CDR(((obj_t) BgL_carzd23094zd2_2454));
																				BgL_arg2158z00_2457 =
																					CDR(((obj_t) BgL_cdrzd23090zd2_2453));
																				{
																					obj_t BgL_expz00_7791;
																					obj_t BgL_argsz00_7790;
																					obj_t BgL_varz00_7789;

																					BgL_varz00_7789 = BgL_arg2156z00_2455;
																					BgL_argsz00_7790 =
																						BgL_arg2157z00_2456;
																					BgL_expz00_7791 = BgL_arg2158z00_2457;
																					BgL_expz00_1946 = BgL_expz00_7791;
																					BgL_argsz00_1945 = BgL_argsz00_7790;
																					BgL_varz00_1944 = BgL_varz00_7789;
																					goto BgL_tagzd2370zd2_1947;
																				}
																			}
																		}
																	}
															}
														else
															{	/* Ast/unit.scm 225 */
																goto BgL_tagzd2374zd2_1961;
															}
													}
												else
													{	/* Ast/unit.scm 225 */
														goto BgL_tagzd2374zd2_1961;
													}
											}
										else
											{	/* Ast/unit.scm 225 */
												obj_t BgL_cdrzd23173zd2_2458;

												BgL_cdrzd23173zd2_2458 = CDR(((obj_t) BgL_sexpz00_39));
												if (
													(CAR(((obj_t) BgL_sexpz00_39)) == CNST_TABLE_REF(20)))
													{	/* Ast/unit.scm 225 */
														if (PAIRP(BgL_cdrzd23173zd2_2458))
															{	/* Ast/unit.scm 225 */
																obj_t BgL_carzd23178zd2_2462;

																BgL_carzd23178zd2_2462 =
																	CAR(BgL_cdrzd23173zd2_2458);
																if (PAIRP(BgL_carzd23178zd2_2462))
																	{	/* Ast/unit.scm 225 */
																		obj_t BgL_carzd23183zd2_2464;

																		BgL_carzd23183zd2_2464 =
																			CAR(BgL_carzd23178zd2_2462);
																		if (PAIRP(BgL_carzd23183zd2_2464))
																			{	/* Ast/unit.scm 225 */
																				obj_t BgL_cdrzd23188zd2_2466;

																				BgL_cdrzd23188zd2_2466 =
																					CDR(BgL_carzd23183zd2_2464);
																				if (
																					(CAR(BgL_carzd23183zd2_2464) ==
																						CNST_TABLE_REF(0)))
																					{	/* Ast/unit.scm 225 */
																						if (PAIRP(BgL_cdrzd23188zd2_2466))
																							{	/* Ast/unit.scm 225 */
																								obj_t BgL_cdrzd23192zd2_2470;

																								BgL_cdrzd23192zd2_2470 =
																									CDR(BgL_cdrzd23188zd2_2466);
																								if (PAIRP
																									(BgL_cdrzd23192zd2_2470))
																									{	/* Ast/unit.scm 225 */
																										if (NULLP(CDR
																												(BgL_cdrzd23192zd2_2470)))
																											{	/* Ast/unit.scm 225 */
																												BgL_varz00_1948 =
																													CAR
																													(BgL_cdrzd23188zd2_2466);
																												BgL_modulez00_1949 =
																													CAR
																													(BgL_cdrzd23192zd2_2470);
																												BgL_argsz00_1950 =
																													CDR
																													(BgL_carzd23178zd2_2462);
																												BgL_expz00_1951 =
																													CDR
																													(BgL_cdrzd23173zd2_2458);
																												{	/* Ast/unit.scm 380 */
																													bool_t
																														BgL_test3847z00_7820;
																													if (BGl_typezd2existszf3z21zztype_envz00(BgL_varz00_1948))
																														{	/* Ast/unit.scm 380 */
																															BgL_typez00_bglt
																																BgL_arg2382z00_2767;
																															BgL_arg2382z00_2767
																																=
																																BGl_findzd2typezd2zztype_envz00
																																(BgL_varz00_1948);
																															{	/* Ast/unit.scm 380 */
																																obj_t
																																	BgL_classz00_4804;
																																BgL_classz00_4804
																																	=
																																	BGl_tclassz00zzobject_classz00;
																																{	/* Ast/unit.scm 380 */
																																	BgL_objectz00_bglt
																																		BgL_arg1807z00_4806;
																																	{	/* Ast/unit.scm 380 */
																																		obj_t
																																			BgL_tmpz00_7824;
																																		BgL_tmpz00_7824
																																			=
																																			((obj_t) (
																																				(BgL_objectz00_bglt)
																																				BgL_arg2382z00_2767));
																																		BgL_arg1807z00_4806
																																			=
																																			(BgL_objectz00_bglt)
																																			(BgL_tmpz00_7824);
																																	}
																																	if (BGL_CONDEXPAND_ISA_ARCH64())
																																		{	/* Ast/unit.scm 380 */
																																			long
																																				BgL_idxz00_4812;
																																			BgL_idxz00_4812
																																				=
																																				BGL_OBJECT_INHERITANCE_NUM
																																				(BgL_arg1807z00_4806);
																																			BgL_test3847z00_7820
																																				=
																																				(VECTOR_REF
																																				(BGl_za2inheritancesza2z00zz__objectz00,
																																					(BgL_idxz00_4812
																																						+
																																						2L))
																																				==
																																				BgL_classz00_4804);
																																		}
																																	else
																																		{	/* Ast/unit.scm 380 */
																																			bool_t
																																				BgL_res3527z00_4837;
																																			{	/* Ast/unit.scm 380 */
																																				obj_t
																																					BgL_oclassz00_4820;
																																				{	/* Ast/unit.scm 380 */
																																					obj_t
																																						BgL_arg1815z00_4828;
																																					long
																																						BgL_arg1816z00_4829;
																																					BgL_arg1815z00_4828
																																						=
																																						(BGl_za2classesza2z00zz__objectz00);
																																					{	/* Ast/unit.scm 380 */
																																						long
																																							BgL_arg1817z00_4830;
																																						BgL_arg1817z00_4830
																																							=
																																							BGL_OBJECT_CLASS_NUM
																																							(BgL_arg1807z00_4806);
																																						BgL_arg1816z00_4829
																																							=
																																							(BgL_arg1817z00_4830
																																							-
																																							OBJECT_TYPE);
																																					}
																																					BgL_oclassz00_4820
																																						=
																																						VECTOR_REF
																																						(BgL_arg1815z00_4828,
																																						BgL_arg1816z00_4829);
																																				}
																																				{	/* Ast/unit.scm 380 */
																																					bool_t
																																						BgL__ortest_1115z00_4821;
																																					BgL__ortest_1115z00_4821
																																						=
																																						(BgL_classz00_4804
																																						==
																																						BgL_oclassz00_4820);
																																					if (BgL__ortest_1115z00_4821)
																																						{	/* Ast/unit.scm 380 */
																																							BgL_res3527z00_4837
																																								=
																																								BgL__ortest_1115z00_4821;
																																						}
																																					else
																																						{	/* Ast/unit.scm 380 */
																																							long
																																								BgL_odepthz00_4822;
																																							{	/* Ast/unit.scm 380 */
																																								obj_t
																																									BgL_arg1804z00_4823;
																																								BgL_arg1804z00_4823
																																									=
																																									(BgL_oclassz00_4820);
																																								BgL_odepthz00_4822
																																									=
																																									BGL_CLASS_DEPTH
																																									(BgL_arg1804z00_4823);
																																							}
																																							if ((2L < BgL_odepthz00_4822))
																																								{	/* Ast/unit.scm 380 */
																																									obj_t
																																										BgL_arg1802z00_4825;
																																									{	/* Ast/unit.scm 380 */
																																										obj_t
																																											BgL_arg1803z00_4826;
																																										BgL_arg1803z00_4826
																																											=
																																											(BgL_oclassz00_4820);
																																										BgL_arg1802z00_4825
																																											=
																																											BGL_CLASS_ANCESTORS_REF
																																											(BgL_arg1803z00_4826,
																																											2L);
																																									}
																																									BgL_res3527z00_4837
																																										=
																																										(BgL_arg1802z00_4825
																																										==
																																										BgL_classz00_4804);
																																								}
																																							else
																																								{	/* Ast/unit.scm 380 */
																																									BgL_res3527z00_4837
																																										=
																																										(
																																										(bool_t)
																																										0);
																																								}
																																						}
																																				}
																																			}
																																			BgL_test3847z00_7820
																																				=
																																				BgL_res3527z00_4837;
																																		}
																																}
																															}
																														}
																													else
																														{	/* Ast/unit.scm 380 */
																															BgL_test3847z00_7820
																																= ((bool_t) 0);
																														}
																													if (BgL_test3847z00_7820)
																														{	/* Ast/unit.scm 380 */
																															BGl_errorzd2classzd2shadowz00zzast_unitz00
																																(BgL_varz00_1948,
																																BgL_sexpz00_39);
																														}
																													else
																														{	/* Ast/unit.scm 380 */
																															BFALSE;
																														}
																												}
																												{	/* Ast/unit.scm 382 */
																													obj_t
																														BgL_list2383z00_2768;
																													{	/* Ast/unit.scm 382 */
																														obj_t
																															BgL_arg2384z00_2769;
																														{	/* Ast/unit.scm 382 */
																															obj_t
																																BgL_arg2385z00_2770;
																															BgL_arg2385z00_2770
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_sexpz00_39,
																																BNIL);
																															BgL_arg2384z00_2769
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_string3567z00zzast_unitz00,
																																BgL_arg2385z00_2770);
																														}
																														BgL_list2383z00_2768
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_string3568z00zzast_unitz00,
																															BgL_arg2384z00_2769);
																													}
																													BGl_warningz00zz__errorz00
																														(BgL_list2383z00_2768);
																												}
																												return
																													BGl_makezd2genericzd2definitionz00zzast_unitz00
																													(BgL_varz00_1948,
																													BgL_modulez00_1949,
																													BgL_argsz00_1950,
																													BgL_expz00_1951,
																													BgL_sexpz00_39);
																											}
																										else
																											{	/* Ast/unit.scm 225 */
																												obj_t
																													BgL_carzd23211zd2_2479;
																												BgL_carzd23211zd2_2479 =
																													CAR(((obj_t)
																														BgL_cdrzd23173zd2_2458));
																												{	/* Ast/unit.scm 225 */
																													obj_t
																														BgL_arg2174z00_2480;
																													obj_t
																														BgL_arg2175z00_2481;
																													obj_t
																														BgL_arg2176z00_2482;
																													BgL_arg2174z00_2480 =
																														CAR(((obj_t)
																															BgL_carzd23211zd2_2479));
																													BgL_arg2175z00_2481 =
																														CDR(((obj_t)
																															BgL_carzd23211zd2_2479));
																													BgL_arg2176z00_2482 =
																														CDR(((obj_t)
																															BgL_cdrzd23173zd2_2458));
																													BgL_varz00_1953 =
																														BgL_arg2174z00_2480;
																													BgL_argsz00_1954 =
																														BgL_arg2175z00_2481;
																													BgL_expz00_1955 =
																														BgL_arg2176z00_2482;
																												BgL_tagzd2372zd2_1956:
																													{	/* Ast/unit.scm 385 */
																														bool_t
																															BgL_test3852z00_7865;
																														if (BGl_typezd2existszf3z21zztype_envz00(BgL_varz00_1953))
																															{	/* Ast/unit.scm 385 */
																																BgL_typez00_bglt
																																	BgL_arg2389z00_2775;
																																BgL_arg2389z00_2775
																																	=
																																	BGl_findzd2typezd2zztype_envz00
																																	(BgL_varz00_1953);
																																{	/* Ast/unit.scm 385 */
																																	obj_t
																																		BgL_classz00_4838;
																																	BgL_classz00_4838
																																		=
																																		BGl_tclassz00zzobject_classz00;
																																	{	/* Ast/unit.scm 385 */
																																		BgL_objectz00_bglt
																																			BgL_arg1807z00_4840;
																																		{	/* Ast/unit.scm 385 */
																																			obj_t
																																				BgL_tmpz00_7869;
																																			BgL_tmpz00_7869
																																				=
																																				((obj_t)
																																				((BgL_objectz00_bglt) BgL_arg2389z00_2775));
																																			BgL_arg1807z00_4840
																																				=
																																				(BgL_objectz00_bglt)
																																				(BgL_tmpz00_7869);
																																		}
																																		if (BGL_CONDEXPAND_ISA_ARCH64())
																																			{	/* Ast/unit.scm 385 */
																																				long
																																					BgL_idxz00_4846;
																																				BgL_idxz00_4846
																																					=
																																					BGL_OBJECT_INHERITANCE_NUM
																																					(BgL_arg1807z00_4840);
																																				BgL_test3852z00_7865
																																					=
																																					(VECTOR_REF
																																					(BGl_za2inheritancesza2z00zz__objectz00,
																																						(BgL_idxz00_4846
																																							+
																																							2L))
																																					==
																																					BgL_classz00_4838);
																																			}
																																		else
																																			{	/* Ast/unit.scm 385 */
																																				bool_t
																																					BgL_res3528z00_4871;
																																				{	/* Ast/unit.scm 385 */
																																					obj_t
																																						BgL_oclassz00_4854;
																																					{	/* Ast/unit.scm 385 */
																																						obj_t
																																							BgL_arg1815z00_4862;
																																						long
																																							BgL_arg1816z00_4863;
																																						BgL_arg1815z00_4862
																																							=
																																							(BGl_za2classesza2z00zz__objectz00);
																																						{	/* Ast/unit.scm 385 */
																																							long
																																								BgL_arg1817z00_4864;
																																							BgL_arg1817z00_4864
																																								=
																																								BGL_OBJECT_CLASS_NUM
																																								(BgL_arg1807z00_4840);
																																							BgL_arg1816z00_4863
																																								=
																																								(BgL_arg1817z00_4864
																																								-
																																								OBJECT_TYPE);
																																						}
																																						BgL_oclassz00_4854
																																							=
																																							VECTOR_REF
																																							(BgL_arg1815z00_4862,
																																							BgL_arg1816z00_4863);
																																					}
																																					{	/* Ast/unit.scm 385 */
																																						bool_t
																																							BgL__ortest_1115z00_4855;
																																						BgL__ortest_1115z00_4855
																																							=
																																							(BgL_classz00_4838
																																							==
																																							BgL_oclassz00_4854);
																																						if (BgL__ortest_1115z00_4855)
																																							{	/* Ast/unit.scm 385 */
																																								BgL_res3528z00_4871
																																									=
																																									BgL__ortest_1115z00_4855;
																																							}
																																						else
																																							{	/* Ast/unit.scm 385 */
																																								long
																																									BgL_odepthz00_4856;
																																								{	/* Ast/unit.scm 385 */
																																									obj_t
																																										BgL_arg1804z00_4857;
																																									BgL_arg1804z00_4857
																																										=
																																										(BgL_oclassz00_4854);
																																									BgL_odepthz00_4856
																																										=
																																										BGL_CLASS_DEPTH
																																										(BgL_arg1804z00_4857);
																																								}
																																								if ((2L < BgL_odepthz00_4856))
																																									{	/* Ast/unit.scm 385 */
																																										obj_t
																																											BgL_arg1802z00_4859;
																																										{	/* Ast/unit.scm 385 */
																																											obj_t
																																												BgL_arg1803z00_4860;
																																											BgL_arg1803z00_4860
																																												=
																																												(BgL_oclassz00_4854);
																																											BgL_arg1802z00_4859
																																												=
																																												BGL_CLASS_ANCESTORS_REF
																																												(BgL_arg1803z00_4860,
																																												2L);
																																										}
																																										BgL_res3528z00_4871
																																											=
																																											(BgL_arg1802z00_4859
																																											==
																																											BgL_classz00_4838);
																																									}
																																								else
																																									{	/* Ast/unit.scm 385 */
																																										BgL_res3528z00_4871
																																											=
																																											(
																																											(bool_t)
																																											0);
																																									}
																																							}
																																					}
																																				}
																																				BgL_test3852z00_7865
																																					=
																																					BgL_res3528z00_4871;
																																			}
																																	}
																																}
																															}
																														else
																															{	/* Ast/unit.scm 385 */
																																BgL_test3852z00_7865
																																	=
																																	((bool_t) 0);
																															}
																														if (BgL_test3852z00_7865)
																															{	/* Ast/unit.scm 385 */
																																BGl_errorzd2classzd2shadowz00zzast_unitz00
																																	(BgL_varz00_1953,
																																	BgL_sexpz00_39);
																															}
																														else
																															{	/* Ast/unit.scm 385 */
																																BFALSE;
																															}
																													}
																													return
																														BGl_makezd2genericzd2definitionz00zzast_unitz00
																														(BgL_varz00_1953,
																														BGl_za2moduleza2z00zzmodule_modulez00,
																														BgL_argsz00_1954,
																														BgL_expz00_1955,
																														BgL_sexpz00_39);
																												}
																											}
																									}
																								else
																									{	/* Ast/unit.scm 225 */
																										obj_t
																											BgL_carzd23231zd2_2485;
																										BgL_carzd23231zd2_2485 =
																											CAR(((obj_t)
																												BgL_cdrzd23173zd2_2458));
																										{	/* Ast/unit.scm 225 */
																											obj_t BgL_arg2178z00_2486;
																											obj_t BgL_arg2179z00_2487;
																											obj_t BgL_arg2180z00_2488;

																											BgL_arg2178z00_2486 =
																												CAR(
																												((obj_t)
																													BgL_carzd23231zd2_2485));
																											BgL_arg2179z00_2487 =
																												CDR(((obj_t)
																													BgL_carzd23231zd2_2485));
																											BgL_arg2180z00_2488 =
																												CDR(((obj_t)
																													BgL_cdrzd23173zd2_2458));
																											{
																												obj_t BgL_expz00_7904;
																												obj_t BgL_argsz00_7903;
																												obj_t BgL_varz00_7902;

																												BgL_varz00_7902 =
																													BgL_arg2178z00_2486;
																												BgL_argsz00_7903 =
																													BgL_arg2179z00_2487;
																												BgL_expz00_7904 =
																													BgL_arg2180z00_2488;
																												BgL_expz00_1955 =
																													BgL_expz00_7904;
																												BgL_argsz00_1954 =
																													BgL_argsz00_7903;
																												BgL_varz00_1953 =
																													BgL_varz00_7902;
																												goto
																													BgL_tagzd2372zd2_1956;
																											}
																										}
																									}
																							}
																						else
																							{	/* Ast/unit.scm 225 */
																								obj_t BgL_carzd23251zd2_2490;

																								BgL_carzd23251zd2_2490 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd23173zd2_2458));
																								{	/* Ast/unit.scm 225 */
																									obj_t BgL_arg2181z00_2491;
																									obj_t BgL_arg2182z00_2492;
																									obj_t BgL_arg2183z00_2493;

																									BgL_arg2181z00_2491 =
																										CAR(
																										((obj_t)
																											BgL_carzd23251zd2_2490));
																									BgL_arg2182z00_2492 =
																										CDR(((obj_t)
																											BgL_carzd23251zd2_2490));
																									BgL_arg2183z00_2493 =
																										CDR(((obj_t)
																											BgL_cdrzd23173zd2_2458));
																									{
																										obj_t BgL_expz00_7915;
																										obj_t BgL_argsz00_7914;
																										obj_t BgL_varz00_7913;

																										BgL_varz00_7913 =
																											BgL_arg2181z00_2491;
																										BgL_argsz00_7914 =
																											BgL_arg2182z00_2492;
																										BgL_expz00_7915 =
																											BgL_arg2183z00_2493;
																										BgL_expz00_1955 =
																											BgL_expz00_7915;
																										BgL_argsz00_1954 =
																											BgL_argsz00_7914;
																										BgL_varz00_1953 =
																											BgL_varz00_7913;
																										goto BgL_tagzd2372zd2_1956;
																									}
																								}
																							}
																					}
																				else
																					{	/* Ast/unit.scm 225 */
																						obj_t BgL_carzd23271zd2_2495;

																						BgL_carzd23271zd2_2495 =
																							CAR(
																							((obj_t) BgL_cdrzd23173zd2_2458));
																						{	/* Ast/unit.scm 225 */
																							obj_t BgL_arg2184z00_2496;
																							obj_t BgL_arg2185z00_2497;
																							obj_t BgL_arg2186z00_2498;

																							BgL_arg2184z00_2496 =
																								CAR(
																								((obj_t)
																									BgL_carzd23271zd2_2495));
																							BgL_arg2185z00_2497 =
																								CDR(((obj_t)
																									BgL_carzd23271zd2_2495));
																							BgL_arg2186z00_2498 =
																								CDR(((obj_t)
																									BgL_cdrzd23173zd2_2458));
																							{
																								obj_t BgL_expz00_7926;
																								obj_t BgL_argsz00_7925;
																								obj_t BgL_varz00_7924;

																								BgL_varz00_7924 =
																									BgL_arg2184z00_2496;
																								BgL_argsz00_7925 =
																									BgL_arg2185z00_2497;
																								BgL_expz00_7926 =
																									BgL_arg2186z00_2498;
																								BgL_expz00_1955 =
																									BgL_expz00_7926;
																								BgL_argsz00_1954 =
																									BgL_argsz00_7925;
																								BgL_varz00_1953 =
																									BgL_varz00_7924;
																								goto BgL_tagzd2372zd2_1956;
																							}
																						}
																					}
																			}
																		else
																			{	/* Ast/unit.scm 225 */
																				obj_t BgL_carzd23291zd2_2501;

																				BgL_carzd23291zd2_2501 =
																					CAR(((obj_t) BgL_cdrzd23173zd2_2458));
																				{	/* Ast/unit.scm 225 */
																					obj_t BgL_arg2188z00_2502;
																					obj_t BgL_arg2189z00_2503;
																					obj_t BgL_arg2190z00_2504;

																					BgL_arg2188z00_2502 =
																						CAR(
																						((obj_t) BgL_carzd23291zd2_2501));
																					BgL_arg2189z00_2503 =
																						CDR(
																						((obj_t) BgL_carzd23291zd2_2501));
																					BgL_arg2190z00_2504 =
																						CDR(
																						((obj_t) BgL_cdrzd23173zd2_2458));
																					{
																						obj_t BgL_expz00_7937;
																						obj_t BgL_argsz00_7936;
																						obj_t BgL_varz00_7935;

																						BgL_varz00_7935 =
																							BgL_arg2188z00_2502;
																						BgL_argsz00_7936 =
																							BgL_arg2189z00_2503;
																						BgL_expz00_7937 =
																							BgL_arg2190z00_2504;
																						BgL_expz00_1955 = BgL_expz00_7937;
																						BgL_argsz00_1954 = BgL_argsz00_7936;
																						BgL_varz00_1953 = BgL_varz00_7935;
																						goto BgL_tagzd2372zd2_1956;
																					}
																				}
																			}
																	}
																else
																	{	/* Ast/unit.scm 225 */
																		goto BgL_tagzd2374zd2_1961;
																	}
															}
														else
															{	/* Ast/unit.scm 225 */
																goto BgL_tagzd2374zd2_1961;
															}
													}
												else
													{	/* Ast/unit.scm 225 */
														if (
															(CAR(
																	((obj_t) BgL_sexpz00_39)) ==
																CNST_TABLE_REF(21)))
															{	/* Ast/unit.scm 225 */
																if (PAIRP(BgL_cdrzd23173zd2_2458))
																	{	/* Ast/unit.scm 225 */
																		obj_t BgL_carzd23341zd2_2509;

																		BgL_carzd23341zd2_2509 =
																			CAR(BgL_cdrzd23173zd2_2458);
																		if (PAIRP(BgL_carzd23341zd2_2509))
																			{	/* Ast/unit.scm 225 */
																				BgL_varz00_1957 =
																					CAR(BgL_carzd23341zd2_2509);
																				BgL_argsz00_1958 =
																					CDR(BgL_carzd23341zd2_2509);
																				BgL_expz00_1959 =
																					CDR(BgL_cdrzd23173zd2_2458);
																				{	/* Ast/unit.scm 389 */
																					bool_t BgL_test3860z00_7948;

																					if (BGl_typezd2existszf3z21zztype_envz00(BgL_varz00_1957))
																						{	/* Ast/unit.scm 389 */
																							BgL_typez00_bglt
																								BgL_arg2393z00_2780;
																							BgL_arg2393z00_2780 =
																								BGl_findzd2typezd2zztype_envz00
																								(BgL_varz00_1957);
																							{	/* Ast/unit.scm 389 */
																								obj_t BgL_classz00_4872;

																								BgL_classz00_4872 =
																									BGl_tclassz00zzobject_classz00;
																								{	/* Ast/unit.scm 389 */
																									BgL_objectz00_bglt
																										BgL_arg1807z00_4874;
																									{	/* Ast/unit.scm 389 */
																										obj_t BgL_tmpz00_7952;

																										BgL_tmpz00_7952 =
																											((obj_t)
																											((BgL_objectz00_bglt)
																												BgL_arg2393z00_2780));
																										BgL_arg1807z00_4874 =
																											(BgL_objectz00_bglt)
																											(BgL_tmpz00_7952);
																									}
																									if (BGL_CONDEXPAND_ISA_ARCH64
																										())
																										{	/* Ast/unit.scm 389 */
																											long BgL_idxz00_4880;

																											BgL_idxz00_4880 =
																												BGL_OBJECT_INHERITANCE_NUM
																												(BgL_arg1807z00_4874);
																											BgL_test3860z00_7948 =
																												(VECTOR_REF
																												(BGl_za2inheritancesza2z00zz__objectz00,
																													(BgL_idxz00_4880 +
																														2L)) ==
																												BgL_classz00_4872);
																										}
																									else
																										{	/* Ast/unit.scm 389 */
																											bool_t
																												BgL_res3529z00_4905;
																											{	/* Ast/unit.scm 389 */
																												obj_t
																													BgL_oclassz00_4888;
																												{	/* Ast/unit.scm 389 */
																													obj_t
																														BgL_arg1815z00_4896;
																													long
																														BgL_arg1816z00_4897;
																													BgL_arg1815z00_4896 =
																														(BGl_za2classesza2z00zz__objectz00);
																													{	/* Ast/unit.scm 389 */
																														long
																															BgL_arg1817z00_4898;
																														BgL_arg1817z00_4898
																															=
																															BGL_OBJECT_CLASS_NUM
																															(BgL_arg1807z00_4874);
																														BgL_arg1816z00_4897
																															=
																															(BgL_arg1817z00_4898
																															- OBJECT_TYPE);
																													}
																													BgL_oclassz00_4888 =
																														VECTOR_REF
																														(BgL_arg1815z00_4896,
																														BgL_arg1816z00_4897);
																												}
																												{	/* Ast/unit.scm 389 */
																													bool_t
																														BgL__ortest_1115z00_4889;
																													BgL__ortest_1115z00_4889
																														=
																														(BgL_classz00_4872
																														==
																														BgL_oclassz00_4888);
																													if (BgL__ortest_1115z00_4889)
																														{	/* Ast/unit.scm 389 */
																															BgL_res3529z00_4905
																																=
																																BgL__ortest_1115z00_4889;
																														}
																													else
																														{	/* Ast/unit.scm 389 */
																															long
																																BgL_odepthz00_4890;
																															{	/* Ast/unit.scm 389 */
																																obj_t
																																	BgL_arg1804z00_4891;
																																BgL_arg1804z00_4891
																																	=
																																	(BgL_oclassz00_4888);
																																BgL_odepthz00_4890
																																	=
																																	BGL_CLASS_DEPTH
																																	(BgL_arg1804z00_4891);
																															}
																															if (
																																(2L <
																																	BgL_odepthz00_4890))
																																{	/* Ast/unit.scm 389 */
																																	obj_t
																																		BgL_arg1802z00_4893;
																																	{	/* Ast/unit.scm 389 */
																																		obj_t
																																			BgL_arg1803z00_4894;
																																		BgL_arg1803z00_4894
																																			=
																																			(BgL_oclassz00_4888);
																																		BgL_arg1802z00_4893
																																			=
																																			BGL_CLASS_ANCESTORS_REF
																																			(BgL_arg1803z00_4894,
																																			2L);
																																	}
																																	BgL_res3529z00_4905
																																		=
																																		(BgL_arg1802z00_4893
																																		==
																																		BgL_classz00_4872);
																																}
																															else
																																{	/* Ast/unit.scm 389 */
																																	BgL_res3529z00_4905
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																												}
																											}
																											BgL_test3860z00_7948 =
																												BgL_res3529z00_4905;
																										}
																								}
																							}
																						}
																					else
																						{	/* Ast/unit.scm 389 */
																							BgL_test3860z00_7948 =
																								((bool_t) 0);
																						}
																					if (BgL_test3860z00_7948)
																						{	/* Ast/unit.scm 389 */
																							BGl_errorzd2classzd2shadowz00zzast_unitz00
																								(BgL_varz00_1957,
																								BgL_sexpz00_39);
																						}
																					else
																						{	/* Ast/unit.scm 389 */
																							BFALSE;
																						}
																				}
																				{	/* Ast/unit.scm 393 */
																					obj_t BgL_arg2395z00_2781;

																					{	/* Ast/unit.scm 393 */
																						obj_t BgL_arg2396z00_2782;

																						{	/* Ast/unit.scm 393 */
																							obj_t BgL_arg2397z00_2783;

																							{	/* Ast/unit.scm 393 */
																								obj_t BgL_pairz00_4909;

																								BgL_pairz00_4909 =
																									CDR(((obj_t) BgL_sexpz00_39));
																								BgL_arg2397z00_2783 =
																									CDR(BgL_pairz00_4909);
																							}
																							BgL_arg2396z00_2782 =
																								BGl_findzd2locationzd2zztools_locationz00
																								(BgL_arg2397z00_2783);
																						}
																						BgL_arg2395z00_2781 =
																							BGl_normaliza7ezd2prognzf2errorz87zzast_unitz00
																							(BgL_expz00_1959, BgL_sexpz00_39,
																							BgL_arg2396z00_2782);
																					}
																					return
																						BGl_makezd2methodzd2definitionz00zzast_unitz00
																						(BgL_varz00_1957, BgL_argsz00_1958,
																						BgL_arg2395z00_2781,
																						BgL_sexpz00_39);
																				}
																			}
																		else
																			{	/* Ast/unit.scm 225 */
																				goto BgL_tagzd2374zd2_1961;
																			}
																	}
																else
																	{	/* Ast/unit.scm 225 */
																		goto BgL_tagzd2374zd2_1961;
																	}
															}
														else
															{	/* Ast/unit.scm 225 */
																goto BgL_tagzd2374zd2_1961;
															}
													}
											}
									}
							}
					}
				else
					{	/* Ast/unit.scm 225 */
						goto BgL_tagzd2374zd2_1961;
					}
			}
		}

	}



/* normalize-progn/error */
	obj_t BGl_normaliza7ezd2prognzf2errorz87zzast_unitz00(obj_t BgL_expz00_41,
		obj_t BgL_srcz00_42, obj_t BgL_locz00_43)
	{
		{	/* Ast/unit.scm 401 */
			if (NULLP(BgL_expz00_41))
				{	/* Ast/unit.scm 403 */
					obj_t BgL_arg2401z00_2786;

					BgL_arg2401z00_2786 =
						BGl_findzd2locationzd2zztools_locationz00(BgL_srcz00_42);
					return
						((obj_t)
						BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00
						(BGl_string3569z00zzast_unitz00, BgL_srcz00_42,
							BgL_arg2401z00_2786));
				}
			else
				{	/* Ast/unit.scm 404 */
					obj_t BgL_expz00_2787;

					BgL_expz00_2787 =
						BGl_normaliza7ezd2prognz75zztools_prognz00(BgL_expz00_41);
					if (CBOOL(BgL_locz00_43))
						{	/* Ast/unit.scm 406 */
							if (EPAIRP(BgL_expz00_2787))
								{	/* Ast/unit.scm 408 */
									return BgL_expz00_2787;
								}
							else
								{	/* Ast/unit.scm 408 */
									if (PAIRP(BgL_expz00_2787))
										{	/* Ast/unit.scm 411 */
											obj_t BgL_arg2404z00_2790;
											obj_t BgL_arg2405z00_2791;

											BgL_arg2404z00_2790 = CAR(BgL_expz00_2787);
											BgL_arg2405z00_2791 = CDR(BgL_expz00_2787);
											{	/* Ast/unit.scm 411 */
												obj_t BgL_res3531z00_5329;

												BgL_res3531z00_5329 =
													MAKE_YOUNG_EPAIR(BgL_arg2404z00_2790,
													BgL_arg2405z00_2791, BgL_locz00_43);
												return BgL_res3531z00_5329;
											}
										}
									else
										{	/* Ast/unit.scm 413 */
											obj_t BgL_arg2407z00_2792;

											{	/* Ast/unit.scm 413 */
												obj_t BgL_list2408z00_2793;

												BgL_list2408z00_2793 =
													MAKE_YOUNG_PAIR(BgL_expz00_2787, BNIL);
												BgL_arg2407z00_2792 = BgL_list2408z00_2793;
											}
											{	/* Ast/unit.scm 413 */
												obj_t BgL_res3533z00_5332;

												{	/* Ast/unit.scm 413 */
													obj_t BgL_obj1z00_5331;

													BgL_obj1z00_5331 = CNST_TABLE_REF(2);
													BgL_res3533z00_5332 =
														MAKE_YOUNG_EPAIR(BgL_obj1z00_5331,
														BgL_arg2407z00_2792, BgL_locz00_43);
												}
												return BgL_res3533z00_5332;
											}
										}
								}
						}
					else
						{	/* Ast/unit.scm 406 */
							return BgL_expz00_2787;
						}
				}
		}

	}



/* eta-expanse */
	obj_t BGl_etazd2expansezd2zzast_unitz00(obj_t BgL_sexpz00_47,
		obj_t BgL_arityz00_48)
	{
		{	/* Ast/unit.scm 431 */
			{	/* Ast/unit.scm 432 */
				obj_t BgL_argsz00_2802;

				BgL_argsz00_2802 =
					BGl_makezd2nzd2protoz00zztools_argsz00(BgL_arityz00_48);
				{
					obj_t BgL_varz00_2880;
					obj_t BgL_id2z00_2881;
					obj_t BgL_modulez00_2882;

					if (PAIRP(BgL_sexpz00_47))
						{	/* Ast/unit.scm 456 */
							obj_t BgL_cdrzd23492zd2_2814;

							BgL_cdrzd23492zd2_2814 = CDR(((obj_t) BgL_sexpz00_47));
							if ((CAR(((obj_t) BgL_sexpz00_47)) == CNST_TABLE_REF(17)))
								{	/* Ast/unit.scm 456 */
									if (PAIRP(BgL_cdrzd23492zd2_2814))
										{	/* Ast/unit.scm 456 */
											obj_t BgL_cdrzd23497zd2_2818;

											BgL_cdrzd23497zd2_2818 = CDR(BgL_cdrzd23492zd2_2814);
											if (PAIRP(BgL_cdrzd23497zd2_2818))
												{	/* Ast/unit.scm 456 */
													obj_t BgL_carzd23501zd2_2820;

													BgL_carzd23501zd2_2820 = CAR(BgL_cdrzd23497zd2_2818);
													if (PAIRP(BgL_carzd23501zd2_2820))
														{	/* Ast/unit.scm 456 */
															obj_t BgL_cdrzd23506zd2_2822;

															BgL_cdrzd23506zd2_2822 =
																CDR(BgL_carzd23501zd2_2820);
															if (
																(CAR(BgL_carzd23501zd2_2820) ==
																	CNST_TABLE_REF(0)))
																{	/* Ast/unit.scm 456 */
																	if (PAIRP(BgL_cdrzd23506zd2_2822))
																		{	/* Ast/unit.scm 456 */
																			obj_t BgL_cdrzd23510zd2_2826;

																			BgL_cdrzd23510zd2_2826 =
																				CDR(BgL_cdrzd23506zd2_2822);
																			if (PAIRP(BgL_cdrzd23510zd2_2826))
																				{	/* Ast/unit.scm 456 */
																					if (NULLP(CDR
																							(BgL_cdrzd23510zd2_2826)))
																						{	/* Ast/unit.scm 456 */
																							if (NULLP(CDR
																									(BgL_cdrzd23497zd2_2818)))
																								{	/* Ast/unit.scm 456 */
																									obj_t BgL_arg2434z00_2833;
																									obj_t BgL_arg2435z00_2834;

																									BgL_arg2434z00_2833 =
																										CAR(BgL_cdrzd23506zd2_2822);
																									BgL_arg2435z00_2834 =
																										CAR(BgL_cdrzd23510zd2_2826);
																									{	/* Ast/unit.scm 458 */
																										obj_t BgL_id2z00_5355;

																										BgL_id2z00_5355 =
																											BGl_idzd2ofzd2idz00zzast_identz00
																											(BgL_arg2434z00_2833,
																											BGl_findzd2locationzd2zztools_locationz00
																											(BgL_sexpz00_47));
																										{	/* Ast/unit.scm 458 */

																											{	/* Ast/unit.scm 459 */
																												obj_t BgL_auxz00_8041;

																												BgL_varz00_2880 =
																													BgL_arg2434z00_2833;
																												BgL_id2z00_2881 =
																													BgL_id2z00_5355;
																												BgL_modulez00_2882 =
																													BgL_arg2435z00_2834;
																												if (((long)
																														CINT
																														(BgL_arityz00_48) >=
																														0L))
																													{	/* Ast/unit.scm 436 */
																														obj_t
																															BgL_arg2482z00_2885;
																														{	/* Ast/unit.scm 436 */
																															obj_t
																																BgL_arg2483z00_2886;
																															obj_t
																																BgL_arg2484z00_2887;
																															BgL_arg2483z00_2886
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_varz00_2880,
																																BgL_argsz00_2802);
																															{	/* Ast/unit.scm 437 */
																																obj_t
																																	BgL_arg2486z00_2888;
																																{	/* Ast/unit.scm 437 */
																																	obj_t
																																		BgL_arg2487z00_2889;
																																	obj_t
																																		BgL_arg2488z00_2890;
																																	{	/* Ast/unit.scm 437 */
																																		obj_t
																																			BgL_arg2490z00_2891;
																																		{	/* Ast/unit.scm 437 */
																																			obj_t
																																				BgL_arg2491z00_2892;
																																			BgL_arg2491z00_2892
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_modulez00_2882,
																																				BNIL);
																																			BgL_arg2490z00_2891
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_id2z00_2881,
																																				BgL_arg2491z00_2892);
																																		}
																																		BgL_arg2487z00_2889
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(0),
																																			BgL_arg2490z00_2891);
																																	}
																																	BgL_arg2488z00_2890
																																		=
																																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																		(BgL_argsz00_2802,
																																		BNIL);
																																	BgL_arg2486z00_2888
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg2487z00_2889,
																																		BgL_arg2488z00_2890);
																																}
																																BgL_arg2484z00_2887
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg2486z00_2888,
																																	BNIL);
																															}
																															BgL_arg2482z00_2885
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg2483z00_2886,
																																BgL_arg2484z00_2887);
																														}
																														BgL_auxz00_8041 =
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(17),
																															BgL_arg2482z00_2885);
																													}
																												else
																													{	/* Ast/unit.scm 435 */
																														if (
																															((long)
																																CINT
																																(BgL_arityz00_48)
																																== -1L))
																															{	/* Ast/unit.scm 439 */
																																obj_t
																																	BgL_arg2493z00_2894;
																																{	/* Ast/unit.scm 439 */
																																	obj_t
																																		BgL_arg2495z00_2895;
																																	obj_t
																																		BgL_arg2497z00_2896;
																																	BgL_arg2495z00_2895
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_varz00_2880,
																																		BgL_argsz00_2802);
																																	{	/* Ast/unit.scm 440 */
																																		obj_t
																																			BgL_arg2500z00_2897;
																																		{	/* Ast/unit.scm 440 */
																																			obj_t
																																				BgL_arg2501z00_2898;
																																			{	/* Ast/unit.scm 440 */
																																				obj_t
																																					BgL_arg2502z00_2899;
																																				obj_t
																																					BgL_arg2503z00_2900;
																																				{	/* Ast/unit.scm 440 */
																																					obj_t
																																						BgL_arg2505z00_2901;
																																					{	/* Ast/unit.scm 440 */
																																						obj_t
																																							BgL_arg2506z00_2902;
																																						BgL_arg2506z00_2902
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_modulez00_2882,
																																							BNIL);
																																						BgL_arg2505z00_2901
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_id2z00_2881,
																																							BgL_arg2506z00_2902);
																																					}
																																					BgL_arg2502z00_2899
																																						=
																																						MAKE_YOUNG_PAIR
																																						(CNST_TABLE_REF
																																						(0),
																																						BgL_arg2505z00_2901);
																																				}
																																				BgL_arg2503z00_2900
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_argsz00_2802,
																																					BNIL);
																																				BgL_arg2501z00_2898
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg2502z00_2899,
																																					BgL_arg2503z00_2900);
																																			}
																																			BgL_arg2500z00_2897
																																				=
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(22),
																																				BgL_arg2501z00_2898);
																																		}
																																		BgL_arg2497z00_2896
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg2500z00_2897,
																																			BNIL);
																																	}
																																	BgL_arg2493z00_2894
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg2495z00_2895,
																																		BgL_arg2497z00_2896);
																																}
																																BgL_auxz00_8041
																																	=
																																	MAKE_YOUNG_PAIR
																																	(CNST_TABLE_REF
																																	(17),
																																	BgL_arg2493z00_2894);
																															}
																														else
																															{	/* Ast/unit.scm 442 */
																																obj_t
																																	BgL_arg2508z00_2903;
																																{	/* Ast/unit.scm 442 */
																																	obj_t
																																		BgL_arg2509z00_2904;
																																	obj_t
																																		BgL_arg2510z00_2905;
																																	BgL_arg2509z00_2904
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_varz00_2880,
																																		BgL_argsz00_2802);
																																	{	/* Ast/unit.scm 443 */
																																		obj_t
																																			BgL_arg2511z00_2906;
																																		{	/* Ast/unit.scm 443 */
																																			obj_t
																																				BgL_arg2512z00_2907;
																																			{	/* Ast/unit.scm 443 */
																																				obj_t
																																					BgL_arg2513z00_2908;
																																				obj_t
																																					BgL_arg2514z00_2909;
																																				{	/* Ast/unit.scm 443 */
																																					obj_t
																																						BgL_arg2515z00_2910;
																																					{	/* Ast/unit.scm 443 */
																																						obj_t
																																							BgL_arg2516z00_2911;
																																						BgL_arg2516z00_2911
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_modulez00_2882,
																																							BNIL);
																																						BgL_arg2515z00_2910
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_id2z00_2881,
																																							BgL_arg2516z00_2911);
																																					}
																																					BgL_arg2513z00_2908
																																						=
																																						MAKE_YOUNG_PAIR
																																						(CNST_TABLE_REF
																																						(0),
																																						BgL_arg2515z00_2910);
																																				}
																																				{	/* Ast/unit.scm 444 */
																																					obj_t
																																						BgL_arg2518z00_2912;
																																					{	/* Ast/unit.scm 444 */
																																						obj_t
																																							BgL_arg2519z00_2913;
																																						BgL_arg2519z00_2913
																																							=
																																							BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																							(BGl_argsza2zd2ze3argszd2listz41zztools_argsz00
																																							(BgL_argsz00_2802),
																																							BNIL);
																																						BgL_arg2518z00_2912
																																							=
																																							MAKE_YOUNG_PAIR
																																							(CNST_TABLE_REF
																																							(23),
																																							BgL_arg2519z00_2913);
																																					}
																																					BgL_arg2514z00_2909
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg2518z00_2912,
																																						BNIL);
																																				}
																																				BgL_arg2512z00_2907
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg2513z00_2908,
																																					BgL_arg2514z00_2909);
																																			}
																																			BgL_arg2511z00_2906
																																				=
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(22),
																																				BgL_arg2512z00_2907);
																																		}
																																		BgL_arg2510z00_2905
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg2511z00_2906,
																																			BNIL);
																																	}
																																	BgL_arg2508z00_2903
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg2509z00_2904,
																																		BgL_arg2510z00_2905);
																																}
																																BgL_auxz00_8041
																																	=
																																	MAKE_YOUNG_PAIR
																																	(CNST_TABLE_REF
																																	(17),
																																	BgL_arg2508z00_2903);
																															}
																													}
																												return
																													BGl_replacez12z12zztools_miscz00
																													(BgL_sexpz00_47,
																													BgL_auxz00_8041);
																											}
																										}
																									}
																								}
																							else
																								{	/* Ast/unit.scm 456 */
																									return BFALSE;
																								}
																						}
																					else
																						{	/* Ast/unit.scm 456 */
																							obj_t BgL_cdrzd23532zd2_2837;

																							BgL_cdrzd23532zd2_2837 =
																								CDR(
																								((obj_t)
																									BgL_cdrzd23492zd2_2814));
																							if (NULLP(CDR(((obj_t)
																											BgL_cdrzd23532zd2_2837))))
																								{	/* Ast/unit.scm 456 */
																									obj_t BgL_arg2442z00_2840;
																									obj_t BgL_arg2443z00_2841;

																									BgL_arg2442z00_2840 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd23492zd2_2814));
																									BgL_arg2443z00_2841 =
																										CAR(((obj_t)
																											BgL_cdrzd23532zd2_2837));
																									{	/* Ast/unit.scm 461 */
																										obj_t BgL_id2z00_5363;

																										BgL_id2z00_5363 =
																											BGl_idzd2ofzd2idz00zzast_identz00
																											(BgL_arg2443z00_2841,
																											BGl_findzd2locationzd2zztools_locationz00
																											(BgL_sexpz00_47));
																										return
																											BGl_replacez12z12zztools_miscz00
																											(BgL_sexpz00_47,
																											BGl_dozd2etazd2expanseze70ze7zzast_unitz00
																											(BgL_arityz00_48,
																												BgL_argsz00_2802,
																												BgL_arg2442z00_2840,
																												BgL_id2z00_5363));
																									}
																								}
																							else
																								{	/* Ast/unit.scm 456 */
																									return BFALSE;
																								}
																						}
																				}
																			else
																				{	/* Ast/unit.scm 456 */
																					obj_t BgL_cdrzd23550zd2_2845;

																					BgL_cdrzd23550zd2_2845 =
																						CDR(
																						((obj_t) BgL_cdrzd23492zd2_2814));
																					if (NULLP(CDR(
																								((obj_t)
																									BgL_cdrzd23550zd2_2845))))
																						{	/* Ast/unit.scm 456 */
																							obj_t BgL_arg2449z00_2848;
																							obj_t BgL_arg2450z00_2849;

																							BgL_arg2449z00_2848 =
																								CAR(
																								((obj_t)
																									BgL_cdrzd23492zd2_2814));
																							BgL_arg2450z00_2849 =
																								CAR(((obj_t)
																									BgL_cdrzd23550zd2_2845));
																							{	/* Ast/unit.scm 461 */
																								obj_t BgL_id2z00_5371;

																								BgL_id2z00_5371 =
																									BGl_idzd2ofzd2idz00zzast_identz00
																									(BgL_arg2450z00_2849,
																									BGl_findzd2locationzd2zztools_locationz00
																									(BgL_sexpz00_47));
																								return
																									BGl_replacez12z12zztools_miscz00
																									(BgL_sexpz00_47,
																									BGl_dozd2etazd2expanseze70ze7zzast_unitz00
																									(BgL_arityz00_48,
																										BgL_argsz00_2802,
																										BgL_arg2449z00_2848,
																										BgL_id2z00_5371));
																							}
																						}
																					else
																						{	/* Ast/unit.scm 456 */
																							return BFALSE;
																						}
																				}
																		}
																	else
																		{	/* Ast/unit.scm 456 */
																			obj_t BgL_cdrzd23568zd2_2852;

																			BgL_cdrzd23568zd2_2852 =
																				CDR(((obj_t) BgL_cdrzd23492zd2_2814));
																			if (NULLP(CDR(
																						((obj_t) BgL_cdrzd23568zd2_2852))))
																				{	/* Ast/unit.scm 456 */
																					obj_t BgL_arg2455z00_2855;
																					obj_t BgL_arg2456z00_2856;

																					BgL_arg2455z00_2855 =
																						CAR(
																						((obj_t) BgL_cdrzd23492zd2_2814));
																					BgL_arg2456z00_2856 =
																						CAR(
																						((obj_t) BgL_cdrzd23568zd2_2852));
																					{	/* Ast/unit.scm 461 */
																						obj_t BgL_id2z00_5379;

																						BgL_id2z00_5379 =
																							BGl_idzd2ofzd2idz00zzast_identz00
																							(BgL_arg2456z00_2856,
																							BGl_findzd2locationzd2zztools_locationz00
																							(BgL_sexpz00_47));
																						return
																							BGl_replacez12z12zztools_miscz00
																							(BgL_sexpz00_47,
																							BGl_dozd2etazd2expanseze70ze7zzast_unitz00
																							(BgL_arityz00_48,
																								BgL_argsz00_2802,
																								BgL_arg2455z00_2855,
																								BgL_id2z00_5379));
																					}
																				}
																			else
																				{	/* Ast/unit.scm 456 */
																					return BFALSE;
																				}
																		}
																}
															else
																{	/* Ast/unit.scm 456 */
																	obj_t BgL_cdrzd23586zd2_2859;

																	BgL_cdrzd23586zd2_2859 =
																		CDR(((obj_t) BgL_cdrzd23492zd2_2814));
																	if (NULLP(CDR(
																				((obj_t) BgL_cdrzd23586zd2_2859))))
																		{	/* Ast/unit.scm 456 */
																			obj_t BgL_arg2460z00_2862;
																			obj_t BgL_arg2461z00_2863;

																			BgL_arg2460z00_2862 =
																				CAR(((obj_t) BgL_cdrzd23492zd2_2814));
																			BgL_arg2461z00_2863 =
																				CAR(((obj_t) BgL_cdrzd23586zd2_2859));
																			{	/* Ast/unit.scm 461 */
																				obj_t BgL_id2z00_5387;

																				BgL_id2z00_5387 =
																					BGl_idzd2ofzd2idz00zzast_identz00
																					(BgL_arg2461z00_2863,
																					BGl_findzd2locationzd2zztools_locationz00
																					(BgL_sexpz00_47));
																				return
																					BGl_replacez12z12zztools_miscz00
																					(BgL_sexpz00_47,
																					BGl_dozd2etazd2expanseze70ze7zzast_unitz00
																					(BgL_arityz00_48, BgL_argsz00_2802,
																						BgL_arg2460z00_2862,
																						BgL_id2z00_5387));
																			}
																		}
																	else
																		{	/* Ast/unit.scm 456 */
																			return BFALSE;
																		}
																}
														}
													else
														{	/* Ast/unit.scm 456 */
															obj_t BgL_cdrzd23604zd2_2867;

															BgL_cdrzd23604zd2_2867 =
																CDR(((obj_t) BgL_cdrzd23492zd2_2814));
															if (NULLP(CDR(((obj_t) BgL_cdrzd23604zd2_2867))))
																{	/* Ast/unit.scm 456 */
																	obj_t BgL_arg2469z00_2870;
																	obj_t BgL_arg2470z00_2871;

																	BgL_arg2469z00_2870 =
																		CAR(((obj_t) BgL_cdrzd23492zd2_2814));
																	BgL_arg2470z00_2871 =
																		CAR(((obj_t) BgL_cdrzd23604zd2_2867));
																	{	/* Ast/unit.scm 461 */
																		obj_t BgL_id2z00_5395;

																		BgL_id2z00_5395 =
																			BGl_idzd2ofzd2idz00zzast_identz00
																			(BgL_arg2470z00_2871,
																			BGl_findzd2locationzd2zztools_locationz00
																			(BgL_sexpz00_47));
																		return
																			BGl_replacez12z12zztools_miscz00
																			(BgL_sexpz00_47,
																			BGl_dozd2etazd2expanseze70ze7zzast_unitz00
																			(BgL_arityz00_48, BgL_argsz00_2802,
																				BgL_arg2469z00_2870, BgL_id2z00_5395));
																	}
																}
															else
																{	/* Ast/unit.scm 456 */
																	return BFALSE;
																}
														}
												}
											else
												{	/* Ast/unit.scm 456 */
													return BFALSE;
												}
										}
									else
										{	/* Ast/unit.scm 456 */
											return BFALSE;
										}
								}
							else
								{	/* Ast/unit.scm 456 */
									return BFALSE;
								}
						}
					else
						{	/* Ast/unit.scm 456 */
							return BFALSE;
						}
				}
			}
		}

	}



/* do-eta-expanse~0 */
	obj_t BGl_dozd2etazd2expanseze70ze7zzast_unitz00(obj_t BgL_arityz00_5801,
		obj_t BgL_argsz00_5800, obj_t BgL_varz00_2915, obj_t BgL_id2z00_2916)
	{
		{	/* Ast/unit.scm 455 */
			if (((long) CINT(BgL_arityz00_5801) >= 0L))
				{	/* Ast/unit.scm 448 */
					obj_t BgL_arg2524z00_2919;

					{	/* Ast/unit.scm 448 */
						obj_t BgL_arg2525z00_2920;
						obj_t BgL_arg2526z00_2921;

						BgL_arg2525z00_2920 =
							MAKE_YOUNG_PAIR(BgL_varz00_2915, BgL_argsz00_5800);
						{	/* Ast/unit.scm 449 */
							obj_t BgL_arg2527z00_2922;

							BgL_arg2527z00_2922 =
								MAKE_YOUNG_PAIR(BgL_id2z00_2916,
								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
								(BgL_argsz00_5800, BNIL));
							BgL_arg2526z00_2921 = MAKE_YOUNG_PAIR(BgL_arg2527z00_2922, BNIL);
						}
						BgL_arg2524z00_2919 =
							MAKE_YOUNG_PAIR(BgL_arg2525z00_2920, BgL_arg2526z00_2921);
					}
					return MAKE_YOUNG_PAIR(CNST_TABLE_REF(17), BgL_arg2524z00_2919);
				}
			else
				{	/* Ast/unit.scm 447 */
					if (((long) CINT(BgL_arityz00_5801) == -1L))
						{	/* Ast/unit.scm 451 */
							obj_t BgL_arg2534z00_2925;

							{	/* Ast/unit.scm 451 */
								obj_t BgL_arg2536z00_2926;
								obj_t BgL_arg2537z00_2927;

								BgL_arg2536z00_2926 =
									MAKE_YOUNG_PAIR(BgL_varz00_2915, BgL_argsz00_5800);
								{	/* Ast/unit.scm 452 */
									obj_t BgL_arg2538z00_2928;

									{	/* Ast/unit.scm 452 */
										obj_t BgL_arg2539z00_2929;

										{	/* Ast/unit.scm 452 */
											obj_t BgL_arg2540z00_2930;

											BgL_arg2540z00_2930 =
												MAKE_YOUNG_PAIR(BgL_argsz00_5800, BNIL);
											BgL_arg2539z00_2929 =
												MAKE_YOUNG_PAIR(BgL_id2z00_2916, BgL_arg2540z00_2930);
										}
										BgL_arg2538z00_2928 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(22), BgL_arg2539z00_2929);
									}
									BgL_arg2537z00_2927 =
										MAKE_YOUNG_PAIR(BgL_arg2538z00_2928, BNIL);
								}
								BgL_arg2534z00_2925 =
									MAKE_YOUNG_PAIR(BgL_arg2536z00_2926, BgL_arg2537z00_2927);
							}
							return MAKE_YOUNG_PAIR(CNST_TABLE_REF(17), BgL_arg2534z00_2925);
						}
					else
						{	/* Ast/unit.scm 454 */
							obj_t BgL_arg2542z00_2931;

							{	/* Ast/unit.scm 454 */
								obj_t BgL_arg2543z00_2932;
								obj_t BgL_arg2545z00_2933;

								BgL_arg2543z00_2932 =
									MAKE_YOUNG_PAIR(BgL_varz00_2915, BgL_argsz00_5800);
								{	/* Ast/unit.scm 455 */
									obj_t BgL_arg2546z00_2934;

									{	/* Ast/unit.scm 455 */
										obj_t BgL_arg2548z00_2935;

										{	/* Ast/unit.scm 455 */
											obj_t BgL_arg2549z00_2936;

											{	/* Ast/unit.scm 455 */
												obj_t BgL_arg2551z00_2937;

												{	/* Ast/unit.scm 455 */
													obj_t BgL_arg2552z00_2938;

													BgL_arg2552z00_2938 =
														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
														(BGl_argsza2zd2ze3argszd2listz41zztools_argsz00
														(BgL_argsz00_5800), BNIL);
													BgL_arg2551z00_2937 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(23),
														BgL_arg2552z00_2938);
												}
												BgL_arg2549z00_2936 =
													MAKE_YOUNG_PAIR(BgL_arg2551z00_2937, BNIL);
											}
											BgL_arg2548z00_2935 =
												MAKE_YOUNG_PAIR(BgL_id2z00_2916, BgL_arg2549z00_2936);
										}
										BgL_arg2546z00_2934 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(22), BgL_arg2548z00_2935);
									}
									BgL_arg2545z00_2933 =
										MAKE_YOUNG_PAIR(BgL_arg2546z00_2934, BNIL);
								}
								BgL_arg2542z00_2931 =
									MAKE_YOUNG_PAIR(BgL_arg2543z00_2932, BgL_arg2545z00_2933);
							}
							return MAKE_YOUNG_PAIR(CNST_TABLE_REF(17), BgL_arg2542z00_2931);
						}
				}
		}

	}



/* make-sfun-definition */
	obj_t BGl_makezd2sfunzd2definitionz00zzast_unitz00(obj_t BgL_idz00_49,
		obj_t BgL_modulez00_50, obj_t BgL_argsz00_51, obj_t BgL_bodyz00_52,
		obj_t BgL_srcz00_53, obj_t BgL_classz00_54)
	{
		{	/* Ast/unit.scm 467 */
			{	/* Ast/unit.scm 468 */
				obj_t BgL_locz00_2942;
				obj_t BgL_optsz00_2943;
				obj_t BgL_keysz00_2944;

				BgL_locz00_2942 =
					BGl_findzd2locationzd2zztools_locationz00(BgL_srcz00_53);
				BgL_optsz00_2943 =
					BGl_dssslzd2optionalszd2zztools_dssslz00(BgL_argsz00_51);
				BgL_keysz00_2944 = BGl_dssslzd2keyszd2zztools_dssslz00(BgL_argsz00_51);
				if (NULLP(BgL_optsz00_2943))
					{	/* Ast/unit.scm 472 */
						if (NULLP(BgL_keysz00_2944))
							{	/* Ast/unit.scm 724 */
								obj_t BgL_localsz00_5404;
								obj_t BgL_bodyz00_5405;

								BgL_localsz00_5404 =
									BGl_parsezd2funzd2argsz00zzast_unitz00(BgL_argsz00_51,
									BgL_srcz00_53, BgL_locz00_2942);
								BgL_bodyz00_5405 =
									BGl_makezd2dssslzd2functionzd2preludezd2zz__dssslz00
									(BgL_idz00_49, BgL_argsz00_51, BgL_bodyz00_52,
									BGl_userzd2errorzd2envz00zztools_errorz00);
								{	/* Ast/unit.scm 726 */
									BgL_globalz00_bglt BgL_arg3157z00_5406;

									BgL_arg3157z00_5406 =
										BGl_defzd2globalzd2sfunz12z12zzast_glozd2defzd2
										(BgL_idz00_49, BgL_argsz00_51, BgL_localsz00_5404,
										BgL_modulez00_50, BgL_classz00_54, BgL_srcz00_53,
										CNST_TABLE_REF(8), BgL_bodyz00_5405);
									{	/* Ast/unit.scm 726 */
										obj_t BgL_list3158z00_5407;

										BgL_list3158z00_5407 =
											MAKE_YOUNG_PAIR(((obj_t) BgL_arg3157z00_5406), BNIL);
										return BgL_list3158z00_5407;
									}
								}
							}
						else
							{	/* Ast/unit.scm 474 */
								return
									BGl_makezd2sfunzd2keyzd2definitionzd2zzast_unitz00
									(BgL_keysz00_2944, BgL_idz00_49, BgL_modulez00_50,
									BgL_argsz00_51, BgL_bodyz00_52, BgL_srcz00_53,
									BgL_classz00_54, BgL_locz00_2942);
							}
					}
				else
					{	/* Ast/unit.scm 483 */
						obj_t BgL_localsz00_5398;

						BgL_localsz00_5398 =
							BGl_parsezd2funzd2optzd2argszd2zzast_unitz00(BgL_argsz00_51,
							BgL_argsz00_51, BgL_locz00_2942);
						{	/* Ast/unit.scm 483 */
							BgL_globalz00_bglt BgL_gloz00_5399;

							BgL_gloz00_5399 =
								BGl_defzd2globalzd2sfunz12z12zzast_glozd2defzd2(BgL_idz00_49,
								BgL_argsz00_51, BgL_localsz00_5398, BgL_modulez00_50,
								BgL_classz00_54, BgL_srcz00_53, CNST_TABLE_REF(8),
								BgL_bodyz00_52);
							{	/* Ast/unit.scm 484 */
								BgL_globalz00_bglt BgL_cloz00_5400;

								BgL_cloz00_5400 =
									BGl_makezd2sfunzd2optzd2closurezd2zzast_unitz00(
									((obj_t) BgL_gloz00_5399), BgL_optsz00_2943, BgL_idz00_49,
									BgL_modulez00_50, BgL_argsz00_51, BgL_bodyz00_52,
									BgL_srcz00_53, BgL_classz00_54, BgL_locz00_2942);
								{	/* Ast/unit.scm 485 */

									{	/* Ast/unit.scm 486 */
										obj_t BgL_list2556z00_5401;

										{	/* Ast/unit.scm 486 */
											obj_t BgL_arg2557z00_5402;

											BgL_arg2557z00_5402 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_cloz00_5400), BNIL);
											BgL_list2556z00_5401 =
												MAKE_YOUNG_PAIR(
												((obj_t) BgL_gloz00_5399), BgL_arg2557z00_5402);
										}
										return BgL_list2556z00_5401;
									}
								}
							}
						}
					}
			}
		}

	}



/* make-sfun-opt-closure */
	BgL_globalz00_bglt BGl_makezd2sfunzd2optzd2closurezd2zzast_unitz00(obj_t
		BgL_gloz00_63, obj_t BgL_optionalsz00_64, obj_t BgL_idz00_65,
		obj_t BgL_modulez00_66, obj_t BgL_argsz00_67, obj_t BgL_bodyz00_68,
		obj_t BgL_srcz00_69, obj_t BgL_classz00_70, obj_t BgL_locz00_71)
	{
		{	/* Ast/unit.scm 499 */
			{	/* Ast/unit.scm 500 */
				long BgL_arityz00_2952;

				BgL_arityz00_2952 =
					(((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt)
								((BgL_sfunz00_bglt)
									(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_gloz00_63))))->
										BgL_valuez00)))))->BgL_arityz00);
				{
					obj_t BgL_optz00_2972;

					{	/* Ast/unit.scm 538 */
						obj_t BgL_idz00_2954;

						{	/* Ast/unit.scm 538 */
							obj_t BgL_arg2572z00_2969;

							{	/* Ast/unit.scm 538 */
								obj_t BgL_arg2578z00_2970;
								obj_t BgL_arg2579z00_2971;

								{	/* Ast/unit.scm 538 */
									obj_t BgL_symbolz00_5468;

									BgL_symbolz00_5468 = CNST_TABLE_REF(32);
									{	/* Ast/unit.scm 538 */
										obj_t BgL_arg1455z00_5469;

										BgL_arg1455z00_5469 = SYMBOL_TO_STRING(BgL_symbolz00_5468);
										BgL_arg2578z00_2970 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_5469);
								}}
								{	/* Ast/unit.scm 538 */
									obj_t BgL_arg1455z00_5471;

									BgL_arg1455z00_5471 =
										SYMBOL_TO_STRING(((obj_t) BgL_idz00_65));
									BgL_arg2579z00_2971 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_5471);
								}
								BgL_arg2572z00_2969 =
									string_append(BgL_arg2578z00_2970, BgL_arg2579z00_2971);
							}
							BgL_idz00_2954 = bstring_to_symbol(BgL_arg2572z00_2969);
						}
						{	/* Ast/unit.scm 538 */
							obj_t BgL_optidz00_2955;

							BgL_optidz00_2955 =
								BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(33));
							{	/* Ast/unit.scm 539 */
								obj_t BgL_envidz00_2956;

								BgL_envidz00_2956 =
									BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(34));
								{	/* Ast/unit.scm 540 */
									BgL_localz00_bglt BgL_optz00_2957;

									BgL_optz00_2957 =
										BGl_makezd2localzd2svarz00zzast_localz00(BgL_optidz00_2955,
										((BgL_typez00_bglt) BGl_za2vectorza2z00zztype_cachez00));
									{	/* Ast/unit.scm 541 */
										BgL_localz00_bglt BgL_envz00_2958;

										BgL_envz00_2958 =
											BGl_makezd2localzd2svarz00zzast_localz00
											(BgL_envidz00_2956,
											((BgL_typez00_bglt)
												BGl_za2procedureza2z00zztype_cachez00));
										{	/* Ast/unit.scm 542 */
											BgL_globalz00_bglt BgL_gz00_2959;

											{	/* Ast/unit.scm 543 */
												obj_t BgL_arg2560z00_2960;
												obj_t BgL_arg2561z00_2961;
												obj_t BgL_arg2562z00_2962;

												{	/* Ast/unit.scm 543 */
													obj_t BgL_list2563z00_2963;

													{	/* Ast/unit.scm 543 */
														obj_t BgL_arg2564z00_2964;

														BgL_arg2564z00_2964 =
															MAKE_YOUNG_PAIR(BgL_optidz00_2955, BNIL);
														BgL_list2563z00_2963 =
															MAKE_YOUNG_PAIR(BgL_envidz00_2956,
															BgL_arg2564z00_2964);
													}
													BgL_arg2560z00_2960 = BgL_list2563z00_2963;
												}
												{	/* Ast/unit.scm 544 */
													obj_t BgL_list2565z00_2965;

													{	/* Ast/unit.scm 544 */
														obj_t BgL_arg2566z00_2966;

														BgL_arg2566z00_2966 =
															MAKE_YOUNG_PAIR(((obj_t) BgL_optz00_2957), BNIL);
														BgL_list2565z00_2965 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_envz00_2958), BgL_arg2566z00_2966);
													}
													BgL_arg2561z00_2961 = BgL_list2565z00_2965;
												}
												{	/* Ast/unit.scm 546 */
													obj_t BgL_auxz00_8246;

													{	/* Ast/unit.scm 546 */
														obj_t BgL_auxz00_8247;

														BgL_optz00_2972 = BgL_optidz00_2955;
														{	/* Ast/unit.scm 502 */
															long BgL_loptz00_2974;
															obj_t BgL_formsz00_2975;
															obj_t BgL_optsz00_2976;

															BgL_loptz00_2974 =
																bgl_list_length(BgL_optionalsz00_64);
															{	/* Ast/unit.scm 503 */
																obj_t BgL_l1272z00_3094;

																BgL_l1272z00_3094 =
																	(((BgL_sfunz00_bglt) COBJECT(
																			((BgL_sfunz00_bglt)
																				(((BgL_variablez00_bglt) COBJECT(
																							((BgL_variablez00_bglt)
																								((BgL_globalz00_bglt)
																									BgL_gloz00_63))))->
																					BgL_valuez00))))->BgL_argsz00);
																if (NULLP(BgL_l1272z00_3094))
																	{	/* Ast/unit.scm 503 */
																		BgL_formsz00_2975 = BNIL;
																	}
																else
																	{	/* Ast/unit.scm 503 */
																		obj_t BgL_head1274z00_3096;

																		{	/* Ast/unit.scm 503 */
																			obj_t BgL_arg2687z00_3108;

																			BgL_arg2687z00_3108 =
																				(((BgL_variablez00_bglt) COBJECT(
																						((BgL_variablez00_bglt)
																							((BgL_localz00_bglt)
																								CAR(
																									((obj_t)
																										BgL_l1272z00_3094))))))->
																				BgL_idz00);
																			BgL_head1274z00_3096 =
																				MAKE_YOUNG_PAIR(BgL_arg2687z00_3108,
																				BNIL);
																		}
																		{	/* Ast/unit.scm 503 */
																			obj_t BgL_g1277z00_3097;

																			BgL_g1277z00_3097 =
																				CDR(((obj_t) BgL_l1272z00_3094));
																			{
																				obj_t BgL_l1272z00_3099;
																				obj_t BgL_tail1275z00_3100;

																				BgL_l1272z00_3099 = BgL_g1277z00_3097;
																				BgL_tail1275z00_3100 =
																					BgL_head1274z00_3096;
																			BgL_zc3z04anonymousza32682ze3z87_3101:
																				if (NULLP(BgL_l1272z00_3099))
																					{	/* Ast/unit.scm 503 */
																						BgL_formsz00_2975 =
																							BgL_head1274z00_3096;
																					}
																				else
																					{	/* Ast/unit.scm 503 */
																						obj_t BgL_newtail1276z00_3103;

																						{	/* Ast/unit.scm 503 */
																							obj_t BgL_arg2685z00_3105;

																							BgL_arg2685z00_3105 =
																								(((BgL_variablez00_bglt)
																									COBJECT((
																											(BgL_variablez00_bglt) (
																												(BgL_localz00_bglt)
																												CAR(((obj_t)
																														BgL_l1272z00_3099))))))->
																								BgL_idz00);
																							BgL_newtail1276z00_3103 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2685z00_3105, BNIL);
																						}
																						SET_CDR(BgL_tail1275z00_3100,
																							BgL_newtail1276z00_3103);
																						{	/* Ast/unit.scm 503 */
																							obj_t BgL_arg2684z00_3104;

																							BgL_arg2684z00_3104 =
																								CDR(
																								((obj_t) BgL_l1272z00_3099));
																							{
																								obj_t BgL_tail1275z00_8276;
																								obj_t BgL_l1272z00_8275;

																								BgL_l1272z00_8275 =
																									BgL_arg2684z00_3104;
																								BgL_tail1275z00_8276 =
																									BgL_newtail1276z00_3103;
																								BgL_tail1275z00_3100 =
																									BgL_tail1275z00_8276;
																								BgL_l1272z00_3099 =
																									BgL_l1272z00_8275;
																								goto
																									BgL_zc3z04anonymousza32682ze3z87_3101;
																							}
																						}
																					}
																			}
																		}
																	}
															}
															{	/* Ast/unit.scm 504 */
																obj_t BgL_l1278z00_3111;

																BgL_l1278z00_3111 =
																	(((BgL_sfunz00_bglt) COBJECT(
																			((BgL_sfunz00_bglt)
																				(((BgL_variablez00_bglt) COBJECT(
																							((BgL_variablez00_bglt)
																								((BgL_globalz00_bglt)
																									BgL_gloz00_63))))->
																					BgL_valuez00))))->BgL_optionalsz00);
																if (NULLP(BgL_l1278z00_3111))
																	{	/* Ast/unit.scm 504 */
																		BgL_optsz00_2976 = BNIL;
																	}
																else
																	{	/* Ast/unit.scm 504 */
																		obj_t BgL_head1280z00_3113;

																		BgL_head1280z00_3113 =
																			MAKE_YOUNG_PAIR(BNIL, BNIL);
																		{
																			obj_t BgL_l1278z00_3115;
																			obj_t BgL_tail1281z00_3116;

																			BgL_l1278z00_3115 = BgL_l1278z00_3111;
																			BgL_tail1281z00_3116 =
																				BgL_head1280z00_3113;
																		BgL_zc3z04anonymousza32691ze3z87_3117:
																			if (NULLP(BgL_l1278z00_3115))
																				{	/* Ast/unit.scm 504 */
																					BgL_optsz00_2976 =
																						CDR(BgL_head1280z00_3113);
																				}
																			else
																				{	/* Ast/unit.scm 504 */
																					obj_t BgL_newtail1282z00_3119;

																					{	/* Ast/unit.scm 504 */
																						obj_t BgL_arg2694z00_3121;

																						{	/* Ast/unit.scm 504 */
																							obj_t BgL_oz00_3122;

																							BgL_oz00_3122 =
																								CAR(
																								((obj_t) BgL_l1278z00_3115));
																							{	/* Ast/unit.scm 505 */
																								obj_t BgL_arg2695z00_3123;

																								BgL_arg2695z00_3123 =
																									CAR(((obj_t) BgL_oz00_3122));
																								BgL_arg2694z00_3121 =
																									BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																									(BgL_arg2695z00_3123,
																									BgL_locz00_71);
																							}
																						}
																						BgL_newtail1282z00_3119 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2694z00_3121, BNIL);
																					}
																					SET_CDR(BgL_tail1281z00_3116,
																						BgL_newtail1282z00_3119);
																					{	/* Ast/unit.scm 504 */
																						obj_t BgL_arg2693z00_3120;

																						BgL_arg2693z00_3120 =
																							CDR(((obj_t) BgL_l1278z00_3115));
																						{
																							obj_t BgL_tail1281z00_8298;
																							obj_t BgL_l1278z00_8297;

																							BgL_l1278z00_8297 =
																								BgL_arg2693z00_3120;
																							BgL_tail1281z00_8298 =
																								BgL_newtail1282z00_3119;
																							BgL_tail1281z00_3116 =
																								BgL_tail1281z00_8298;
																							BgL_l1278z00_3115 =
																								BgL_l1278z00_8297;
																							goto
																								BgL_zc3z04anonymousza32691ze3z87_3117;
																						}
																					}
																				}
																		}
																	}
															}
															{	/* Ast/unit.scm 507 */
																obj_t BgL_arg2584z00_2977;
																obj_t BgL_arg2585z00_2978;

																BgL_arg2584z00_2977 =
																	BGl_letzd2symzd2zzast_letz00();
																{	/* Ast/unit.scm 507 */
																	obj_t BgL_arg2586z00_2979;
																	obj_t BgL_arg2587z00_2980;

																	{	/* Ast/unit.scm 507 */
																		obj_t BgL_ll1283z00_2981;
																		obj_t BgL_ll1284z00_2982;

																		BgL_ll1283z00_2981 =
																			BGl_takez00zz__r4_pairs_and_lists_6_3z00(
																			(((BgL_sfunz00_bglt) COBJECT(
																						((BgL_sfunz00_bglt)
																							(((BgL_variablez00_bglt) COBJECT(
																										((BgL_variablez00_bglt)
																											((BgL_globalz00_bglt)
																												BgL_gloz00_63))))->
																								BgL_valuez00))))->
																				BgL_argszd2namezd2), BgL_arityz00_2952);
																		BgL_ll1284z00_2982 =
																			BGl_iotaz00zz__r4_pairs_and_lists_6_3z00(
																			(int) (BgL_arityz00_2952), BNIL);
																		if (NULLP(BgL_ll1283z00_2981))
																			{	/* Ast/unit.scm 507 */
																				BgL_arg2586z00_2979 = BNIL;
																			}
																		else
																			{	/* Ast/unit.scm 507 */
																				obj_t BgL_head1285z00_2984;

																				BgL_head1285z00_2984 =
																					MAKE_YOUNG_PAIR(BNIL, BNIL);
																				{
																					obj_t BgL_ll1283z00_2986;
																					obj_t BgL_ll1284z00_2987;
																					obj_t BgL_tail1286z00_2988;

																					BgL_ll1283z00_2986 =
																						BgL_ll1283z00_2981;
																					BgL_ll1284z00_2987 =
																						BgL_ll1284z00_2982;
																					BgL_tail1286z00_2988 =
																						BgL_head1285z00_2984;
																				BgL_zc3z04anonymousza32589ze3z87_2989:
																					if (NULLP(BgL_ll1283z00_2986))
																						{	/* Ast/unit.scm 507 */
																							BgL_arg2586z00_2979 =
																								CDR(BgL_head1285z00_2984);
																						}
																					else
																						{	/* Ast/unit.scm 507 */
																							obj_t BgL_newtail1287z00_2991;

																							{	/* Ast/unit.scm 507 */
																								obj_t BgL_arg2594z00_2994;

																								{	/* Ast/unit.scm 507 */
																									obj_t BgL_vz00_2995;
																									obj_t BgL_iz00_2996;

																									BgL_vz00_2995 =
																										CAR(
																										((obj_t)
																											BgL_ll1283z00_2986));
																									BgL_iz00_2996 =
																										CAR(((obj_t)
																											BgL_ll1284z00_2987));
																									{	/* Ast/unit.scm 508 */
																										obj_t BgL_arg2595z00_2997;

																										{	/* Ast/unit.scm 508 */
																											obj_t BgL_arg2596z00_2998;

																											{	/* Ast/unit.scm 508 */
																												obj_t
																													BgL_arg2597z00_2999;
																												{	/* Ast/unit.scm 508 */
																													obj_t
																														BgL_arg2599z00_3000;
																													BgL_arg2599z00_3000 =
																														MAKE_YOUNG_PAIR
																														(BgL_iz00_2996,
																														BNIL);
																													BgL_arg2597z00_2999 =
																														MAKE_YOUNG_PAIR
																														(BgL_optz00_2972,
																														BgL_arg2599z00_3000);
																												}
																												BgL_arg2596z00_2998 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(24),
																													BgL_arg2597z00_2999);
																											}
																											BgL_arg2595z00_2997 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg2596z00_2998,
																												BNIL);
																										}
																										BgL_arg2594z00_2994 =
																											MAKE_YOUNG_PAIR
																											(BgL_vz00_2995,
																											BgL_arg2595z00_2997);
																									}
																								}
																								BgL_newtail1287z00_2991 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg2594z00_2994, BNIL);
																							}
																							SET_CDR(BgL_tail1286z00_2988,
																								BgL_newtail1287z00_2991);
																							{	/* Ast/unit.scm 507 */
																								obj_t BgL_arg2591z00_2992;
																								obj_t BgL_arg2592z00_2993;

																								BgL_arg2591z00_2992 =
																									CDR(
																									((obj_t) BgL_ll1283z00_2986));
																								BgL_arg2592z00_2993 =
																									CDR(
																									((obj_t) BgL_ll1284z00_2987));
																								{
																									obj_t BgL_tail1286z00_8332;
																									obj_t BgL_ll1284z00_8331;
																									obj_t BgL_ll1283z00_8330;

																									BgL_ll1283z00_8330 =
																										BgL_arg2591z00_2992;
																									BgL_ll1284z00_8331 =
																										BgL_arg2592z00_2993;
																									BgL_tail1286z00_8332 =
																										BgL_newtail1287z00_2991;
																									BgL_tail1286z00_2988 =
																										BgL_tail1286z00_8332;
																									BgL_ll1284z00_2987 =
																										BgL_ll1284z00_8331;
																									BgL_ll1283z00_2986 =
																										BgL_ll1283z00_8330;
																									goto
																										BgL_zc3z04anonymousza32589ze3z87_2989;
																								}
																							}
																						}
																				}
																			}
																	}
																	{	/* Ast/unit.scm 511 */
																		obj_t BgL_arg2604z00_3005;

																		{	/* Ast/unit.scm 511 */
																			obj_t BgL_arg2605z00_3006;

																			{	/* Ast/unit.scm 511 */
																				obj_t BgL_arg2607z00_3007;
																				obj_t BgL_arg2608z00_3008;

																				{	/* Ast/unit.scm 511 */
																					obj_t BgL_arg2609z00_3009;

																					BgL_arg2609z00_3009 =
																						MAKE_YOUNG_PAIR(BgL_optz00_2972,
																						BNIL);
																					BgL_arg2607z00_3007 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(25),
																						BgL_arg2609z00_3009);
																				}
																				{	/* Ast/unit.scm 512 */
																					obj_t BgL_arg2610z00_3010;
																					obj_t BgL_arg2611z00_3011;

																					{	/* Ast/unit.scm 512 */
																						obj_t BgL_l1295z00_3012;

																						{	/* Ast/unit.scm 526 */
																							long BgL_arg2650z00_3065;

																							BgL_arg2650z00_3065 =
																								(BgL_loptz00_2974 + 1L);
																							BgL_l1295z00_3012 =
																								BGl_iotaz00zz__r4_pairs_and_lists_6_3z00
																								((int) (BgL_arg2650z00_3065),
																								BNIL);
																						}
																						if (NULLP(BgL_l1295z00_3012))
																							{	/* Ast/unit.scm 512 */
																								BgL_arg2610z00_3010 = BNIL;
																							}
																						else
																							{	/* Ast/unit.scm 512 */
																								obj_t BgL_head1297z00_3014;

																								BgL_head1297z00_3014 =
																									MAKE_YOUNG_PAIR(BNIL, BNIL);
																								{
																									obj_t BgL_l1295z00_3016;
																									obj_t BgL_tail1298z00_3017;

																									BgL_l1295z00_3016 =
																										BgL_l1295z00_3012;
																									BgL_tail1298z00_3017 =
																										BgL_head1297z00_3014;
																								BgL_zc3z04anonymousza32613ze3z87_3018:
																									if (NULLP
																										(BgL_l1295z00_3016))
																										{	/* Ast/unit.scm 512 */
																											BgL_arg2610z00_3010 =
																												CDR
																												(BgL_head1297z00_3014);
																										}
																									else
																										{	/* Ast/unit.scm 512 */
																											obj_t
																												BgL_newtail1299z00_3020;
																											{	/* Ast/unit.scm 512 */
																												obj_t
																													BgL_arg2617z00_3022;
																												{	/* Ast/unit.scm 512 */
																													obj_t BgL_iz00_3023;

																													BgL_iz00_3023 =
																														CAR(
																														((obj_t)
																															BgL_l1295z00_3016));
																													{	/* Ast/unit.scm 513 */
																														obj_t
																															BgL_arg2618z00_3024;
																														obj_t
																															BgL_arg2619z00_3025;
																														{	/* Ast/unit.scm 513 */
																															long
																																BgL_arg2620z00_3026;
																															BgL_arg2620z00_3026
																																=
																																(BgL_arityz00_2952
																																+
																																(long)
																																CINT
																																(BgL_iz00_3023));
																															BgL_arg2618z00_3024
																																=
																																MAKE_YOUNG_PAIR
																																(BINT
																																(BgL_arg2620z00_3026),
																																BNIL);
																														}
																														{	/* Ast/unit.scm 514 */
																															obj_t
																																BgL_arg2621z00_3027;
																															{	/* Ast/unit.scm 514 */
																																obj_t
																																	BgL_arg2622z00_3028;
																																{	/* Ast/unit.scm 514 */
																																	obj_t
																																		BgL_arg2623z00_3029;
																																	obj_t
																																		BgL_arg2624z00_3030;
																																	{	/* Ast/unit.scm 514 */
																																		obj_t
																																			BgL_arg2626z00_3031;
																																		obj_t
																																			BgL_arg2627z00_3032;
																																		{	/* Ast/unit.scm 514 */
																																			obj_t
																																				BgL_ll1289z00_3033;
																																			obj_t
																																				BgL_ll1290z00_3034;
																																			BgL_ll1289z00_3033
																																				=
																																				BGl_takez00zz__r4_pairs_and_lists_6_3z00
																																				(BGl_dropz00zz__r4_pairs_and_lists_6_3z00
																																				(BgL_formsz00_2975,
																																					BgL_arityz00_2952),
																																				(long)
																																				CINT
																																				(BgL_iz00_3023));
																																			{	/* Ast/unit.scm 517 */
																																				obj_t
																																					BgL_list2640z00_3055;
																																				BgL_list2640z00_3055
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BINT
																																					(BgL_arityz00_2952),
																																					BNIL);
																																				BgL_ll1290z00_3034
																																					=
																																					BGl_iotaz00zz__r4_pairs_and_lists_6_3z00
																																					(CINT
																																					(BgL_iz00_3023),
																																					BgL_list2640z00_3055);
																																			}
																																			if (NULLP
																																				(BgL_ll1289z00_3033))
																																				{	/* Ast/unit.scm 514 */
																																					BgL_arg2626z00_3031
																																						=
																																						BNIL;
																																				}
																																			else
																																				{	/* Ast/unit.scm 514 */
																																					obj_t
																																						BgL_head1291z00_3036;
																																					BgL_head1291z00_3036
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BNIL,
																																						BNIL);
																																					{
																																						obj_t
																																							BgL_ll1289z00_3038;
																																						obj_t
																																							BgL_ll1290z00_3039;
																																						obj_t
																																							BgL_tail1292z00_3040;
																																						BgL_ll1289z00_3038
																																							=
																																							BgL_ll1289z00_3033;
																																						BgL_ll1290z00_3039
																																							=
																																							BgL_ll1290z00_3034;
																																						BgL_tail1292z00_3040
																																							=
																																							BgL_head1291z00_3036;
																																					BgL_zc3z04anonymousza32629ze3z87_3041:
																																						if (NULLP(BgL_ll1289z00_3038))
																																							{	/* Ast/unit.scm 514 */
																																								BgL_arg2626z00_3031
																																									=
																																									CDR
																																									(BgL_head1291z00_3036);
																																							}
																																						else
																																							{	/* Ast/unit.scm 514 */
																																								obj_t
																																									BgL_newtail1293z00_3043;
																																								{	/* Ast/unit.scm 514 */
																																									obj_t
																																										BgL_arg2633z00_3046;
																																									{	/* Ast/unit.scm 514 */
																																										obj_t
																																											BgL_vz00_3047;
																																										obj_t
																																											BgL_jz00_3048;
																																										BgL_vz00_3047
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_ll1289z00_3038));
																																										BgL_jz00_3048
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_ll1290z00_3039));
																																										{	/* Ast/unit.scm 515 */
																																											obj_t
																																												BgL_arg2635z00_3049;
																																											{	/* Ast/unit.scm 515 */
																																												obj_t
																																													BgL_arg2636z00_3050;
																																												{	/* Ast/unit.scm 515 */
																																													obj_t
																																														BgL_arg2637z00_3051;
																																													{	/* Ast/unit.scm 515 */
																																														obj_t
																																															BgL_arg2638z00_3052;
																																														BgL_arg2638z00_3052
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BgL_jz00_3048,
																																															BNIL);
																																														BgL_arg2637z00_3051
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BgL_optz00_2972,
																																															BgL_arg2638z00_3052);
																																													}
																																													BgL_arg2636z00_3050
																																														=
																																														MAKE_YOUNG_PAIR
																																														(CNST_TABLE_REF
																																														(24),
																																														BgL_arg2637z00_3051);
																																												}
																																												BgL_arg2635z00_3049
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_arg2636z00_3050,
																																													BNIL);
																																											}
																																											BgL_arg2633z00_3046
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_vz00_3047,
																																												BgL_arg2635z00_3049);
																																										}
																																									}
																																									BgL_newtail1293z00_3043
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_arg2633z00_3046,
																																										BNIL);
																																								}
																																								SET_CDR
																																									(BgL_tail1292z00_3040,
																																									BgL_newtail1293z00_3043);
																																								{	/* Ast/unit.scm 514 */
																																									obj_t
																																										BgL_arg2631z00_3044;
																																									obj_t
																																										BgL_arg2632z00_3045;
																																									BgL_arg2631z00_3044
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_ll1289z00_3038));
																																									BgL_arg2632z00_3045
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_ll1290z00_3039));
																																									{
																																										obj_t
																																											BgL_tail1292z00_8382;
																																										obj_t
																																											BgL_ll1290z00_8381;
																																										obj_t
																																											BgL_ll1289z00_8380;
																																										BgL_ll1289z00_8380
																																											=
																																											BgL_arg2631z00_3044;
																																										BgL_ll1290z00_8381
																																											=
																																											BgL_arg2632z00_3045;
																																										BgL_tail1292z00_8382
																																											=
																																											BgL_newtail1293z00_3043;
																																										BgL_tail1292z00_3040
																																											=
																																											BgL_tail1292z00_8382;
																																										BgL_ll1290z00_3039
																																											=
																																											BgL_ll1290z00_8381;
																																										BgL_ll1289z00_3038
																																											=
																																											BgL_ll1289z00_8380;
																																										goto
																																											BgL_zc3z04anonymousza32629ze3z87_3041;
																																									}
																																								}
																																							}
																																					}
																																				}
																																		}
																																		{	/* Ast/unit.scm 514 */
																																			obj_t
																																				BgL_auxz00_8383;
																																			if ((
																																					(long)
																																					CINT
																																					(BgL_iz00_3023)
																																					<=
																																					BgL_loptz00_2974))
																																				{	/* Ast/unit.scm 518 */
																																					BgL_auxz00_8383
																																						=
																																						BGl_dropz00zz__r4_pairs_and_lists_6_3z00
																																						(BgL_optionalsz00_64,
																																						(long)
																																						CINT
																																						(BgL_iz00_3023));
																																				}
																																			else
																																				{	/* Ast/unit.scm 518 */
																																					BgL_auxz00_8383
																																						=
																																						BNIL;
																																				}
																																			BgL_arg2627z00_3032
																																				=
																																				BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																				(BgL_auxz00_8383,
																																				BNIL);
																																		}
																																		BgL_arg2623z00_3029
																																			=
																																			BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																			(BgL_arg2626z00_3031,
																																			BgL_arg2627z00_3032);
																																	}
																																	{	/* Ast/unit.scm 523 */
																																		obj_t
																																			BgL_arg2643z00_3058;
																																		{	/* Ast/unit.scm 523 */
																																			obj_t
																																				BgL_arg2644z00_3059;
																																			BgL_arg2644z00_3059
																																				=
																																				BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																				(BGl_takez00zz__r4_pairs_and_lists_6_3z00
																																				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) (((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt) BgL_gloz00_63))))->BgL_valuez00))))->BgL_argszd2namezd2), BgL_arityz00_2952), BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_optsz00_2976, BNIL));
																																			BgL_arg2643z00_3058
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_gloz00_63,
																																				BgL_arg2644z00_3059);
																																		}
																																		BgL_arg2624z00_3030
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg2643z00_3058,
																																			BNIL);
																																	}
																																	BgL_arg2622z00_3028
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg2623z00_3029,
																																		BgL_arg2624z00_3030);
																																}
																																BgL_arg2621z00_3027
																																	=
																																	MAKE_YOUNG_PAIR
																																	(CNST_TABLE_REF
																																	(26),
																																	BgL_arg2622z00_3028);
																															}
																															BgL_arg2619z00_3025
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg2621z00_3027,
																																BNIL);
																														}
																														BgL_arg2617z00_3022
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg2618z00_3024,
																															BgL_arg2619z00_3025);
																													}
																												}
																												BgL_newtail1299z00_3020
																													=
																													MAKE_YOUNG_PAIR
																													(BgL_arg2617z00_3022,
																													BNIL);
																											}
																											SET_CDR
																												(BgL_tail1298z00_3017,
																												BgL_newtail1299z00_3020);
																											{	/* Ast/unit.scm 512 */
																												obj_t
																													BgL_arg2615z00_3021;
																												BgL_arg2615z00_3021 =
																													CDR(((obj_t)
																														BgL_l1295z00_3016));
																												{
																													obj_t
																														BgL_tail1298z00_8411;
																													obj_t
																														BgL_l1295z00_8410;
																													BgL_l1295z00_8410 =
																														BgL_arg2615z00_3021;
																													BgL_tail1298z00_8411 =
																														BgL_newtail1299z00_3020;
																													BgL_tail1298z00_3017 =
																														BgL_tail1298z00_8411;
																													BgL_l1295z00_3016 =
																														BgL_l1295z00_8410;
																													goto
																														BgL_zc3z04anonymousza32613ze3z87_3018;
																												}
																											}
																										}
																								}
																							}
																					}
																					{	/* Ast/unit.scm 528 */
																						obj_t BgL_arg2653z00_3067;

																						{	/* Ast/unit.scm 528 */
																							obj_t BgL_arg2654z00_3068;

																							{	/* Ast/unit.scm 528 */
																								obj_t BgL_arg2656z00_3069;

																								if (CBOOL
																									(BGl_za2unsafezd2arityza2zd2zzengine_paramz00))
																									{	/* Ast/unit.scm 528 */
																										BgL_arg2656z00_3069 =
																											BUNSPEC;
																									}
																								else
																									{	/* Ast/unit.scm 530 */
																										obj_t BgL_arg2657z00_3070;
																										obj_t BgL_arg2658z00_3071;

																										{	/* Ast/unit.scm 530 */
																											obj_t BgL_arg2659z00_3072;

																											{	/* Ast/unit.scm 530 */
																												obj_t
																													BgL_arg2660z00_3073;
																												BgL_arg2660z00_3073 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(27),
																													BNIL);
																												BgL_arg2659z00_3072 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(28),
																													BgL_arg2660z00_3073);
																											}
																											BgL_arg2657z00_3070 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(0),
																												BgL_arg2659z00_3072);
																										}
																										{	/* Ast/unit.scm 531 */
																											obj_t BgL_arg2662z00_3074;
																											obj_t BgL_arg2664z00_3075;

																											{	/* Ast/unit.scm 531 */
																												obj_t
																													BgL_arg2665z00_3076;
																												BgL_arg2665z00_3076 =
																													MAKE_YOUNG_PAIR
																													(BgL_idz00_65, BNIL);
																												BgL_arg2662z00_3074 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(29),
																													BgL_arg2665z00_3076);
																											}
																											{	/* Ast/unit.scm 534 */
																												obj_t
																													BgL_arg2666z00_3077;
																												obj_t
																													BgL_arg2667z00_3078;
																												{	/* Ast/unit.scm 534 */
																													obj_t
																														BgL_arg2669z00_3079;
																													obj_t
																														BgL_arg2670z00_3080;
																													{	/* Ast/unit.scm 534 */

																														BgL_arg2669z00_3079
																															=
																															BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																															(BgL_arityz00_2952,
																															10L);
																													}
																													{	/* Ast/unit.scm 535 */
																														obj_t
																															BgL_arg2676z00_3088;
																														{	/* Ast/unit.scm 535 */
																															obj_t
																																BgL_za71za7_5458;
																															obj_t
																																BgL_za72za7_5459;
																															BgL_za71za7_5458 =
																																BINT
																																(BgL_arityz00_2952);
																															BgL_za72za7_5459 =
																																BINT
																																(BgL_loptz00_2974);
																															{	/* Ast/unit.scm 535 */
																																obj_t
																																	BgL_tmpz00_5460;
																																BgL_tmpz00_5460
																																	= BINT(0L);
																																if (BGL_ADDFX_OV
																																	(BgL_za71za7_5458,
																																		BgL_za72za7_5459,
																																		BgL_tmpz00_5460))
																																	{	/* Ast/unit.scm 535 */
																																		BgL_arg2676z00_3088
																																			=
																																			bgl_bignum_add
																																			(bgl_long_to_bignum
																																			((long)
																																				CINT
																																				(BgL_za71za7_5458)),
																																			bgl_long_to_bignum
																																			((long)
																																				CINT
																																				(BgL_za72za7_5459)));
																																	}
																																else
																																	{	/* Ast/unit.scm 535 */
																																		BgL_arg2676z00_3088
																																			=
																																			BgL_tmpz00_5460;
																																	}
																															}
																														}
																														{	/* Ast/unit.scm 535 */

																															BgL_arg2670z00_3080
																																=
																																BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																																((long)
																																CINT
																																(BgL_arg2676z00_3088),
																																10L);
																													}}
																													{	/* Ast/unit.scm 532 */
																														obj_t
																															BgL_list2671z00_3081;
																														{	/* Ast/unit.scm 532 */
																															obj_t
																																BgL_arg2672z00_3082;
																															{	/* Ast/unit.scm 532 */
																																obj_t
																																	BgL_arg2673z00_3083;
																																{	/* Ast/unit.scm 532 */
																																	obj_t
																																		BgL_arg2674z00_3084;
																																	{	/* Ast/unit.scm 532 */
																																		obj_t
																																			BgL_arg2675z00_3085;
																																		BgL_arg2675z00_3085
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_string3570z00zzast_unitz00,
																																			BNIL);
																																		BgL_arg2674z00_3084
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg2670z00_3080,
																																			BgL_arg2675z00_3085);
																																	}
																																	BgL_arg2673z00_3083
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_string3571z00zzast_unitz00,
																																		BgL_arg2674z00_3084);
																																}
																																BgL_arg2672z00_3082
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg2669z00_3079,
																																	BgL_arg2673z00_3083);
																															}
																															BgL_list2671z00_3081
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_string3572z00zzast_unitz00,
																																BgL_arg2672z00_3082);
																														}
																														BgL_arg2666z00_3077
																															=
																															BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																															(BgL_list2671z00_3081);
																												}}
																												{	/* Ast/unit.scm 537 */
																													obj_t
																														BgL_arg2678z00_3092;
																													{	/* Ast/unit.scm 537 */
																														obj_t
																															BgL_arg2680z00_3093;
																														BgL_arg2680z00_3093
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_optz00_2972,
																															BNIL);
																														BgL_arg2678z00_3092
																															=
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(25),
																															BgL_arg2680z00_3093);
																													}
																													BgL_arg2667z00_3078 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg2678z00_3092,
																														BNIL);
																												}
																												BgL_arg2664z00_3075 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg2666z00_3077,
																													BgL_arg2667z00_3078);
																											}
																											BgL_arg2658z00_3071 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg2662z00_3074,
																												BgL_arg2664z00_3075);
																										}
																										BgL_arg2656z00_3069 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg2657z00_3070,
																											BgL_arg2658z00_3071);
																									}
																								BgL_arg2654z00_3068 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg2656z00_3069, BNIL);
																							}
																							BgL_arg2653z00_3067 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(30), BgL_arg2654z00_3068);
																						}
																						BgL_arg2611z00_3011 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2653z00_3067, BNIL);
																					}
																					BgL_arg2608z00_3008 =
																						BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																						(BgL_arg2610z00_3010,
																						BgL_arg2611z00_3011);
																				}
																				BgL_arg2605z00_3006 =
																					MAKE_YOUNG_PAIR(BgL_arg2607z00_3007,
																					BgL_arg2608z00_3008);
																			}
																			BgL_arg2604z00_3005 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(31),
																				BgL_arg2605z00_3006);
																		}
																		BgL_arg2587z00_2980 =
																			MAKE_YOUNG_PAIR(BgL_arg2604z00_3005,
																			BNIL);
																	}
																	BgL_arg2585z00_2978 =
																		MAKE_YOUNG_PAIR(BgL_arg2586z00_2979,
																		BgL_arg2587z00_2980);
																}
																BgL_auxz00_8247 =
																	MAKE_YOUNG_PAIR(BgL_arg2584z00_2977,
																	BgL_arg2585z00_2978);
														}}
														BgL_auxz00_8246 =
															BGl_comptimezd2expandzd2zzexpand_epsz00
															(BgL_auxz00_8247);
													}
													BgL_arg2562z00_2962 =
														BGl_compilezd2expandzd2zzexpand_epsz00
														(BgL_auxz00_8246);
												}
												BgL_gz00_2959 =
													BGl_defzd2globalzd2sfunz12z12zzast_glozd2defzd2
													(BgL_idz00_2954, BgL_arg2560z00_2960,
													BgL_arg2561z00_2961, BgL_modulez00_66,
													BgL_classz00_70, BgL_srcz00_69, CNST_TABLE_REF(35),
													BgL_arg2562z00_2962);
											}
											{	/* Ast/unit.scm 543 */

												((((BgL_globalz00_bglt) COBJECT(BgL_gz00_2959))->
														BgL_evaluablezf3zf3) =
													((bool_t) ((bool_t) 0)), BUNSPEC);
												{	/* Ast/unit.scm 548 */
													BgL_typez00_bglt BgL_vz00_5478;

													BgL_vz00_5478 =
														((BgL_typez00_bglt)
														BGl_za2objza2z00zztype_cachez00);
													((((BgL_variablez00_bglt)
																COBJECT(((BgL_variablez00_bglt)
																		BgL_gz00_2959)))->BgL_typez00) =
														((BgL_typez00_bglt) BgL_vz00_5478), BUNSPEC);
												}
												return BgL_gz00_2959;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* make-sfun-key-definition */
	obj_t BGl_makezd2sfunzd2keyzd2definitionzd2zzast_unitz00(obj_t BgL_keysz00_72,
		obj_t BgL_idz00_73, obj_t BgL_modulez00_74, obj_t BgL_argsz00_75,
		obj_t BgL_bodyz00_76, obj_t BgL_srcz00_77, obj_t BgL_classz00_78,
		obj_t BgL_locz00_79)
	{
		{	/* Ast/unit.scm 554 */
			{	/* Ast/unit.scm 555 */
				obj_t BgL_localsz00_3128;

				{	/* Ast/unit.scm 555 */
					obj_t BgL_arg2700z00_3133;
					obj_t BgL_arg2701z00_3134;

					{	/* Ast/unit.scm 555 */
						obj_t BgL_l1300z00_3135;

						BgL_l1300z00_3135 =
							BGl_dssslzd2beforezd2dssslz00zztools_dssslz00(BgL_argsz00_75);
						if (NULLP(BgL_l1300z00_3135))
							{	/* Ast/unit.scm 555 */
								BgL_arg2700z00_3133 = BNIL;
							}
						else
							{	/* Ast/unit.scm 555 */
								obj_t BgL_head1302z00_3137;

								BgL_head1302z00_3137 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1300z00_3139;
									obj_t BgL_tail1303z00_3140;

									BgL_l1300z00_3139 = BgL_l1300z00_3135;
									BgL_tail1303z00_3140 = BgL_head1302z00_3137;
								BgL_zc3z04anonymousza32703ze3z87_3141:
									if (NULLP(BgL_l1300z00_3139))
										{	/* Ast/unit.scm 555 */
											BgL_arg2700z00_3133 = CDR(BgL_head1302z00_3137);
										}
									else
										{	/* Ast/unit.scm 555 */
											obj_t BgL_newtail1304z00_3143;

											{	/* Ast/unit.scm 555 */
												BgL_localz00_bglt BgL_arg2706z00_3145;

												{	/* Ast/unit.scm 555 */
													obj_t BgL_az00_3146;

													BgL_az00_3146 = CAR(((obj_t) BgL_l1300z00_3139));
													{	/* Ast/unit.scm 556 */
														obj_t BgL_pidz00_3147;

														BgL_pidz00_3147 =
															BGl_checkzd2idzd2zzast_identz00
															(BGl_parsezd2idzd2zzast_identz00(BgL_az00_3146,
																BgL_locz00_79), BgL_srcz00_77);
														{	/* Ast/unit.scm 556 */
															obj_t BgL_idz00_3148;

															BgL_idz00_3148 = CAR(BgL_pidz00_3147);
															{	/* Ast/unit.scm 557 */
																obj_t BgL_typez00_3149;

																BgL_typez00_3149 = CDR(BgL_pidz00_3147);
																{	/* Ast/unit.scm 558 */

																	if (BGl_userzd2symbolzf3z21zzast_identz00
																		(BgL_idz00_3148))
																		{	/* Ast/unit.scm 559 */
																			BgL_arg2706z00_3145 =
																				BGl_makezd2userzd2localzd2svarzd2zzast_localz00
																				(BgL_idz00_3148,
																				((BgL_typez00_bglt) BgL_typez00_3149));
																		}
																	else
																		{	/* Ast/unit.scm 559 */
																			BgL_arg2706z00_3145 =
																				BGl_makezd2localzd2svarz00zzast_localz00
																				(BgL_idz00_3148,
																				((BgL_typez00_bglt) BgL_typez00_3149));
																		}
																}
															}
														}
													}
												}
												BgL_newtail1304z00_3143 =
													MAKE_YOUNG_PAIR(((obj_t) BgL_arg2706z00_3145), BNIL);
											}
											SET_CDR(BgL_tail1303z00_3140, BgL_newtail1304z00_3143);
											{	/* Ast/unit.scm 555 */
												obj_t BgL_arg2705z00_3144;

												BgL_arg2705z00_3144 = CDR(((obj_t) BgL_l1300z00_3139));
												{
													obj_t BgL_tail1303z00_8493;
													obj_t BgL_l1300z00_8492;

													BgL_l1300z00_8492 = BgL_arg2705z00_3144;
													BgL_tail1303z00_8493 = BgL_newtail1304z00_3143;
													BgL_tail1303z00_3140 = BgL_tail1303z00_8493;
													BgL_l1300z00_3139 = BgL_l1300z00_8492;
													goto BgL_zc3z04anonymousza32703ze3z87_3141;
												}
											}
										}
								}
							}
					}
					{	/* Ast/unit.scm 563 */
						obj_t BgL_head1307z00_3155;

						BgL_head1307z00_3155 = MAKE_YOUNG_PAIR(BNIL, BNIL);
						{
							obj_t BgL_l1305z00_3157;
							obj_t BgL_tail1308z00_3158;

							BgL_l1305z00_3157 = BgL_keysz00_72;
							BgL_tail1308z00_3158 = BgL_head1307z00_3155;
						BgL_zc3z04anonymousza32710ze3z87_3159:
							if (NULLP(BgL_l1305z00_3157))
								{	/* Ast/unit.scm 563 */
									BgL_arg2701z00_3134 = CDR(BgL_head1307z00_3155);
								}
							else
								{	/* Ast/unit.scm 563 */
									obj_t BgL_newtail1309z00_3161;

									{	/* Ast/unit.scm 563 */
										BgL_localz00_bglt BgL_arg2714z00_3163;

										{	/* Ast/unit.scm 563 */
											obj_t BgL_oz00_3164;

											BgL_oz00_3164 = CAR(((obj_t) BgL_l1305z00_3157));
											{	/* Ast/unit.scm 564 */
												obj_t BgL_az00_3165;

												BgL_az00_3165 = CAR(((obj_t) BgL_oz00_3164));
												{	/* Ast/unit.scm 564 */
													obj_t BgL_pidz00_3166;

													BgL_pidz00_3166 =
														BGl_checkzd2idzd2zzast_identz00
														(BGl_parsezd2idzd2zzast_identz00(BgL_az00_3165,
															BgL_locz00_79), BgL_srcz00_77);
													{	/* Ast/unit.scm 565 */
														obj_t BgL_idz00_3167;

														BgL_idz00_3167 = CAR(BgL_pidz00_3166);
														{	/* Ast/unit.scm 566 */
															obj_t BgL_typez00_3168;

															BgL_typez00_3168 = CDR(BgL_pidz00_3166);
															{	/* Ast/unit.scm 567 */

																BgL_arg2714z00_3163 =
																	BGl_makezd2userzd2localzd2svarzd2zzast_localz00
																	(BgL_idz00_3167,
																	((BgL_typez00_bglt) BgL_typez00_3168));
															}
														}
													}
												}
											}
										}
										BgL_newtail1309z00_3161 =
											MAKE_YOUNG_PAIR(((obj_t) BgL_arg2714z00_3163), BNIL);
									}
									SET_CDR(BgL_tail1308z00_3158, BgL_newtail1309z00_3161);
									{	/* Ast/unit.scm 563 */
										obj_t BgL_arg2712z00_3162;

										BgL_arg2712z00_3162 = CDR(((obj_t) BgL_l1305z00_3157));
										{
											obj_t BgL_tail1308z00_8514;
											obj_t BgL_l1305z00_8513;

											BgL_l1305z00_8513 = BgL_arg2712z00_3162;
											BgL_tail1308z00_8514 = BgL_newtail1309z00_3161;
											BgL_tail1308z00_3158 = BgL_tail1308z00_8514;
											BgL_l1305z00_3157 = BgL_l1305z00_8513;
											goto BgL_zc3z04anonymousza32710ze3z87_3159;
										}
									}
								}
						}
					}
					BgL_localsz00_3128 =
						BGl_appendzd221011zd2zzast_unitz00(BgL_arg2700z00_3133,
						BgL_arg2701z00_3134);
				}
				{	/* Ast/unit.scm 555 */
					BgL_globalz00_bglt BgL_gloz00_3129;

					BgL_gloz00_3129 =
						BGl_defzd2globalzd2sfunz12z12zzast_glozd2defzd2(BgL_idz00_73,
						BgL_argsz00_75, BgL_localsz00_3128, BgL_modulez00_74,
						BgL_classz00_78, BgL_srcz00_77, CNST_TABLE_REF(8), BgL_bodyz00_76);
					{	/* Ast/unit.scm 570 */
						BgL_globalz00_bglt BgL_cloz00_3130;

						BgL_cloz00_3130 =
							BGl_makezd2sfunzd2keyzd2closurezd2zzast_unitz00(
							((obj_t) BgL_gloz00_3129), BgL_keysz00_72, BgL_idz00_73,
							BgL_modulez00_74, BgL_argsz00_75, BgL_bodyz00_76, BgL_srcz00_77,
							BgL_classz00_78, BgL_locz00_79);
						{	/* Ast/unit.scm 571 */

							{	/* Ast/unit.scm 572 */
								obj_t BgL_list2698z00_3131;

								{	/* Ast/unit.scm 572 */
									obj_t BgL_arg2699z00_3132;

									BgL_arg2699z00_3132 =
										MAKE_YOUNG_PAIR(((obj_t) BgL_cloz00_3130), BNIL);
									BgL_list2698z00_3131 =
										MAKE_YOUNG_PAIR(
										((obj_t) BgL_gloz00_3129), BgL_arg2699z00_3132);
								}
								return BgL_list2698z00_3131;
							}
						}
					}
				}
			}
		}

	}



/* make-sfun-key-closure */
	BgL_globalz00_bglt BGl_makezd2sfunzd2keyzd2closurezd2zzast_unitz00(obj_t
		BgL_gloz00_80, obj_t BgL_keysz00_81, obj_t BgL_idz00_82,
		obj_t BgL_modulez00_83, obj_t BgL_argsz00_84, obj_t BgL_bodyz00_85,
		obj_t BgL_srcz00_86, obj_t BgL_classz00_87, obj_t BgL_locz00_88)
	{
		{	/* Ast/unit.scm 579 */
			{	/* Ast/unit.scm 580 */
				long BgL_arityz00_3171;
				obj_t BgL_ioptz00_3172;
				obj_t BgL_ienvz00_3173;
				long BgL_loptz00_3174;
				obj_t BgL_lz00_3175;
				obj_t BgL_searchz00_3176;
				obj_t BgL_checkz00_3177;
				obj_t BgL_varz00_3178;

				BgL_arityz00_3171 =
					(((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt)
								((BgL_sfunz00_bglt)
									(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_gloz00_80))))->
										BgL_valuez00)))))->BgL_arityz00);
				BgL_ioptz00_3172 =
					BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(33));
				BgL_ienvz00_3173 =
					BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(34));
				BgL_loptz00_3174 = bgl_list_length(BgL_keysz00_81);
				BgL_lz00_3175 = BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(36));
				BgL_searchz00_3176 =
					BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(37));
				BgL_checkz00_3177 =
					BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(38));
				BgL_varz00_3178 =
					BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(39));
				{
					obj_t BgL_keysz00_3197;

					{	/* Ast/unit.scm 657 */
						obj_t BgL_idz00_3181;

						{	/* Ast/unit.scm 657 */
							obj_t BgL_arg2726z00_3194;

							{	/* Ast/unit.scm 657 */
								obj_t BgL_arg2727z00_3195;
								obj_t BgL_arg2728z00_3196;

								{	/* Ast/unit.scm 657 */
									obj_t BgL_symbolz00_5543;

									BgL_symbolz00_5543 = CNST_TABLE_REF(32);
									{	/* Ast/unit.scm 657 */
										obj_t BgL_arg1455z00_5544;

										BgL_arg1455z00_5544 = SYMBOL_TO_STRING(BgL_symbolz00_5543);
										BgL_arg2727z00_3195 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_5544);
								}}
								{	/* Ast/unit.scm 657 */
									obj_t BgL_arg1455z00_5546;

									BgL_arg1455z00_5546 =
										SYMBOL_TO_STRING(((obj_t) BgL_idz00_82));
									BgL_arg2728z00_3196 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_5546);
								}
								BgL_arg2726z00_3194 =
									string_append(BgL_arg2727z00_3195, BgL_arg2728z00_3196);
							}
							BgL_idz00_3181 = bstring_to_symbol(BgL_arg2726z00_3194);
						}
						{	/* Ast/unit.scm 657 */
							BgL_localz00_bglt BgL_optz00_3182;

							BgL_optz00_3182 =
								BGl_makezd2localzd2svarz00zzast_localz00(BgL_ioptz00_3172,
								((BgL_typez00_bglt) BGl_za2vectorza2z00zztype_cachez00));
							{	/* Ast/unit.scm 658 */
								BgL_localz00_bglt BgL_envz00_3183;

								BgL_envz00_3183 =
									BGl_makezd2localzd2svarz00zzast_localz00(BgL_ienvz00_3173,
									((BgL_typez00_bglt) BGl_za2procedureza2z00zztype_cachez00));
								{	/* Ast/unit.scm 659 */
									BgL_globalz00_bglt BgL_gz00_3184;

									{	/* Ast/unit.scm 660 */
										obj_t BgL_arg2716z00_3185;
										obj_t BgL_arg2717z00_3186;
										obj_t BgL_arg2719z00_3187;

										{	/* Ast/unit.scm 660 */
											obj_t BgL_list2720z00_3188;

											{	/* Ast/unit.scm 660 */
												obj_t BgL_arg2721z00_3189;

												BgL_arg2721z00_3189 =
													MAKE_YOUNG_PAIR(BgL_ioptz00_3172, BNIL);
												BgL_list2720z00_3188 =
													MAKE_YOUNG_PAIR(BgL_ienvz00_3173,
													BgL_arg2721z00_3189);
											}
											BgL_arg2716z00_3185 = BgL_list2720z00_3188;
										}
										{	/* Ast/unit.scm 660 */
											obj_t BgL_list2722z00_3190;

											{	/* Ast/unit.scm 660 */
												obj_t BgL_arg2723z00_3191;

												BgL_arg2723z00_3191 =
													MAKE_YOUNG_PAIR(((obj_t) BgL_optz00_3182), BNIL);
												BgL_list2722z00_3190 =
													MAKE_YOUNG_PAIR(
													((obj_t) BgL_envz00_3183), BgL_arg2723z00_3191);
											}
											BgL_arg2717z00_3186 = BgL_list2722z00_3190;
										}
										{	/* Ast/unit.scm 662 */
											obj_t BgL_auxz00_8561;

											{	/* Ast/unit.scm 663 */
												obj_t BgL_auxz00_8562;

												{	/* Ast/unit.scm 594 */
													obj_t BgL_arg2739z00_3215;
													obj_t BgL_arg2740z00_3216;

													BgL_arg2739z00_3215 = BGl_letzd2symzd2zzast_letz00();
													{	/* Ast/unit.scm 594 */
														obj_t BgL_arg2741z00_3217;
														obj_t BgL_arg2742z00_3218;

														{	/* Ast/unit.scm 594 */
															obj_t BgL_arg2743z00_3219;

															{	/* Ast/unit.scm 594 */
																obj_t BgL_arg2744z00_3220;

																{	/* Ast/unit.scm 594 */
																	obj_t BgL_arg2746z00_3221;

																	{	/* Ast/unit.scm 594 */
																		obj_t BgL_arg2747z00_3222;

																		BgL_arg2747z00_3222 =
																			MAKE_YOUNG_PAIR(BgL_ioptz00_3172, BNIL);
																		BgL_arg2746z00_3221 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(40),
																			BgL_arg2747z00_3222);
																	}
																	BgL_arg2744z00_3220 =
																		MAKE_YOUNG_PAIR(BgL_arg2746z00_3221, BNIL);
																}
																BgL_arg2743z00_3219 =
																	MAKE_YOUNG_PAIR(BgL_lz00_3175,
																	BgL_arg2744z00_3220);
															}
															BgL_arg2741z00_3217 =
																MAKE_YOUNG_PAIR(BgL_arg2743z00_3219, BNIL);
														}
														{	/* Ast/unit.scm 595 */
															obj_t BgL_arg2748z00_3223;

															{	/* Ast/unit.scm 595 */
																obj_t BgL_arg2749z00_3224;

																{	/* Ast/unit.scm 595 */
																	obj_t BgL_arg2750z00_3225;
																	obj_t BgL_arg2751z00_3226;

																	{	/* Ast/unit.scm 595 */
																		obj_t BgL_arg2753z00_3227;

																		{	/* Ast/unit.scm 595 */
																			obj_t BgL_arg2754z00_3228;

																			{	/* Ast/unit.scm 595 */
																				obj_t BgL_arg2755z00_3229;
																				obj_t BgL_arg2756z00_3230;

																				{	/* Ast/unit.scm 595 */
																					obj_t BgL_arg2757z00_3231;

																					BgL_arg2757z00_3231 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(41),
																						BNIL);
																					BgL_arg2755z00_3229 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(42),
																						BgL_arg2757z00_3231);
																				}
																				{	/* Ast/unit.scm 596 */
																					obj_t BgL_arg2758z00_3232;

																					{	/* Ast/unit.scm 596 */
																						obj_t BgL_arg2759z00_3233;

																						{	/* Ast/unit.scm 596 */
																							obj_t BgL_arg2760z00_3234;
																							obj_t BgL_arg2761z00_3235;

																							{	/* Ast/unit.scm 596 */
																								obj_t BgL_arg2762z00_3236;

																								{	/* Ast/unit.scm 596 */
																									obj_t BgL_arg2764z00_3237;

																									BgL_arg2764z00_3237 =
																										MAKE_YOUNG_PAIR
																										(BgL_lz00_3175, BNIL);
																									BgL_arg2762z00_3236 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(41),
																										BgL_arg2764z00_3237);
																								}
																								BgL_arg2760z00_3234 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(43), BgL_arg2762z00_3236);
																							}
																							{	/* Ast/unit.scm 598 */
																								obj_t BgL_arg2765z00_3238;

																								{	/* Ast/unit.scm 598 */
																									obj_t BgL_arg2766z00_3239;

																									{	/* Ast/unit.scm 598 */
																										bool_t BgL_test3907z00_8579;

																										if (CBOOL
																											(BGl_za2unsafezd2arityza2zd2zzengine_paramz00))
																											{	/* Ast/unit.scm 599 */
																												bool_t
																													BgL__ortest_1124z00_3327;
																												if ((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt) BgL_gloz00_80)))->BgL_evaluablezf3zf3))
																													{	/* Ast/unit.scm 599 */
																														BgL__ortest_1124z00_3327
																															= ((bool_t) 0);
																													}
																												else
																													{	/* Ast/unit.scm 599 */
																														BgL__ortest_1124z00_3327
																															= ((bool_t) 1);
																													}
																												if (BgL__ortest_1124z00_3327)
																													{	/* Ast/unit.scm 599 */
																														BgL_test3907z00_8579
																															=
																															BgL__ortest_1124z00_3327;
																													}
																												else
																													{	/* Ast/unit.scm 599 */
																														BgL_test3907z00_8579
																															=
																															CBOOL
																															(BGl_za2unsafezd2evalza2zd2zzengine_paramz00);
																													}
																											}
																										else
																											{	/* Ast/unit.scm 598 */
																												BgL_test3907z00_8579 =
																													((bool_t) 0);
																											}
																										if (BgL_test3907z00_8579)
																											{	/* Ast/unit.scm 601 */
																												obj_t
																													BgL_arg2771z00_3243;
																												obj_t
																													BgL_arg2772z00_3244;
																												BgL_arg2771z00_3243 =
																													BGl_letzd2symzd2zzast_letz00
																													();
																												{	/* Ast/unit.scm 601 */
																													obj_t
																														BgL_arg2773z00_3245;
																													obj_t
																														BgL_arg2774z00_3246;
																													{	/* Ast/unit.scm 601 */
																														obj_t
																															BgL_arg2776z00_3247;
																														{	/* Ast/unit.scm 601 */
																															obj_t
																																BgL_arg2777z00_3248;
																															{	/* Ast/unit.scm 601 */
																																obj_t
																																	BgL_arg2778z00_3249;
																																{	/* Ast/unit.scm 601 */
																																	obj_t
																																		BgL_arg2780z00_3250;
																																	{	/* Ast/unit.scm 601 */
																																		obj_t
																																			BgL_arg2781z00_3251;
																																		BgL_arg2781z00_3251
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(41),
																																			BNIL);
																																		BgL_arg2780z00_3250
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_ioptz00_3172,
																																			BgL_arg2781z00_3251);
																																	}
																																	BgL_arg2778z00_3249
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(24),
																																		BgL_arg2780z00_3250);
																																}
																																BgL_arg2777z00_3248
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg2778z00_3249,
																																	BNIL);
																															}
																															BgL_arg2776z00_3247
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(44),
																																BgL_arg2777z00_3248);
																														}
																														BgL_arg2773z00_3245
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg2776z00_3247,
																															BNIL);
																													}
																													{	/* Ast/unit.scm 602 */
																														obj_t
																															BgL_arg2783z00_3252;
																														{	/* Ast/unit.scm 602 */
																															obj_t
																																BgL_arg2784z00_3253;
																															{	/* Ast/unit.scm 602 */
																																obj_t
																																	BgL_arg2786z00_3254;
																																obj_t
																																	BgL_arg2787z00_3255;
																																{	/* Ast/unit.scm 602 */
																																	obj_t
																																		BgL_arg2789z00_3256;
																																	{	/* Ast/unit.scm 602 */
																																		obj_t
																																			BgL_arg2793z00_3257;
																																		BgL_arg2793z00_3257
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(42),
																																			BNIL);
																																		BgL_arg2789z00_3256
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(44),
																																			BgL_arg2793z00_3257);
																																	}
																																	BgL_arg2786z00_3254
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(45),
																																		BgL_arg2789z00_3256);
																																}
																																{	/* Ast/unit.scm 603 */
																																	obj_t
																																		BgL_arg2794z00_3258;
																																	obj_t
																																		BgL_arg2799z00_3259;
																																	{	/* Ast/unit.scm 603 */
																																		obj_t
																																			BgL_arg2800z00_3260;
																																		{	/* Ast/unit.scm 603 */
																																			obj_t
																																				BgL_arg2804z00_3261;
																																			BgL_arg2804z00_3261
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BINT
																																				(1L),
																																				BNIL);
																																			BgL_arg2800z00_3260
																																				=
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(41),
																																				BgL_arg2804z00_3261);
																																		}
																																		BgL_arg2794z00_3258
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(46),
																																			BgL_arg2800z00_3260);
																																	}
																																	{	/* Ast/unit.scm 604 */
																																		obj_t
																																			BgL_arg2805z00_3262;
																																		{	/* Ast/unit.scm 604 */
																																			obj_t
																																				BgL_arg2808z00_3263;
																																			{	/* Ast/unit.scm 604 */
																																				obj_t
																																					BgL_arg2809z00_3264;
																																				{	/* Ast/unit.scm 604 */
																																					obj_t
																																						BgL_arg2811z00_3265;
																																					{	/* Ast/unit.scm 604 */
																																						obj_t
																																							BgL_arg2812z00_3266;
																																						{	/* Ast/unit.scm 604 */
																																							obj_t
																																								BgL_arg2815z00_3267;
																																							BgL_arg2815z00_3267
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BINT
																																								(2L),
																																								BNIL);
																																							BgL_arg2812z00_3266
																																								=
																																								MAKE_YOUNG_PAIR
																																								(CNST_TABLE_REF
																																								(41),
																																								BgL_arg2815z00_3267);
																																						}
																																						BgL_arg2811z00_3265
																																							=
																																							MAKE_YOUNG_PAIR
																																							(CNST_TABLE_REF
																																							(46),
																																							BgL_arg2812z00_3266);
																																					}
																																					BgL_arg2809z00_3264
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg2811z00_3265,
																																						BNIL);
																																				}
																																				BgL_arg2808z00_3263
																																					=
																																					MAKE_YOUNG_PAIR
																																					(CNST_TABLE_REF
																																					(42),
																																					BgL_arg2809z00_3264);
																																			}
																																			BgL_arg2805z00_3262
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_searchz00_3176,
																																				BgL_arg2808z00_3263);
																																		}
																																		BgL_arg2799z00_3259
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg2805z00_3262,
																																			BNIL);
																																	}
																																	BgL_arg2787z00_3255
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg2794z00_3258,
																																		BgL_arg2799z00_3259);
																																}
																																BgL_arg2784z00_3253
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg2786z00_3254,
																																	BgL_arg2787z00_3255);
																															}
																															BgL_arg2783z00_3252
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(3),
																																BgL_arg2784z00_3253);
																														}
																														BgL_arg2774z00_3246
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg2783z00_3252,
																															BNIL);
																													}
																													BgL_arg2772z00_3244 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg2773z00_3245,
																														BgL_arg2774z00_3246);
																												}
																												BgL_arg2766z00_3239 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg2771z00_3243,
																													BgL_arg2772z00_3244);
																											}
																										else
																											{	/* Ast/unit.scm 605 */
																												obj_t
																													BgL_arg2816z00_3268;
																												{	/* Ast/unit.scm 605 */
																													obj_t
																														BgL_arg2818z00_3269;
																													obj_t
																														BgL_arg2820z00_3270;
																													{	/* Ast/unit.scm 605 */
																														obj_t
																															BgL_arg2821z00_3271;
																														{	/* Ast/unit.scm 605 */
																															obj_t
																																BgL_arg2823z00_3272;
																															{	/* Ast/unit.scm 605 */
																																obj_t
																																	BgL_arg2824z00_3273;
																																{	/* Ast/unit.scm 605 */
																																	obj_t
																																		BgL_arg2826z00_3274;
																																	{	/* Ast/unit.scm 605 */
																																		obj_t
																																			BgL_arg2827z00_3275;
																																		BgL_arg2827z00_3275
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BINT(1L),
																																			BNIL);
																																		BgL_arg2826z00_3274
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_lz00_3175,
																																			BgL_arg2827z00_3275);
																																	}
																																	BgL_arg2824z00_3273
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(47),
																																		BgL_arg2826z00_3274);
																																}
																																BgL_arg2823z00_3272
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg2824z00_3273,
																																	BNIL);
																															}
																															BgL_arg2821z00_3271
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(41),
																																BgL_arg2823z00_3272);
																														}
																														BgL_arg2818z00_3269
																															=
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(43),
																															BgL_arg2821z00_3271);
																													}
																													{	/* Ast/unit.scm 606 */
																														obj_t
																															BgL_arg2828z00_3276;
																														obj_t
																															BgL_arg2829z00_3277;
																														{	/* Ast/unit.scm 606 */
																															obj_t
																																BgL_arg2830z00_3278;
																															obj_t
																																BgL_arg2831z00_3279;
																															{	/* Ast/unit.scm 606 */
																																obj_t
																																	BgL_arg2832z00_3280;
																																{	/* Ast/unit.scm 606 */
																																	obj_t
																																		BgL_arg2833z00_3281;
																																	BgL_arg2833z00_3281
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(27), BNIL);
																																	BgL_arg2832z00_3280
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(28),
																																		BgL_arg2833z00_3281);
																																}
																																BgL_arg2830z00_3278
																																	=
																																	MAKE_YOUNG_PAIR
																																	(CNST_TABLE_REF
																																	(0),
																																	BgL_arg2832z00_3280);
																															}
																															{	/* Ast/unit.scm 607 */
																																obj_t
																																	BgL_arg2834z00_3282;
																																obj_t
																																	BgL_arg2835z00_3283;
																																{	/* Ast/unit.scm 607 */
																																	obj_t
																																		BgL_arg2836z00_3284;
																																	BgL_arg2836z00_3284
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_idz00_82,
																																		BNIL);
																																	BgL_arg2834z00_3282
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(29),
																																		BgL_arg2836z00_3284);
																																}
																																{	/* Ast/unit.scm 610 */
																																	obj_t
																																		BgL_arg2837z00_3285;
																																	obj_t
																																		BgL_arg2838z00_3286;
																																	{	/* Ast/unit.scm 610 */
																																		obj_t
																																			BgL_arg2839z00_3287;
																																		obj_t
																																			BgL_arg2844z00_3288;
																																		{	/* Ast/unit.scm 610 */

																																			BgL_arg2839z00_3287
																																				=
																																				BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																																				(BgL_arityz00_3171,
																																				10L);
																																		}
																																		{	/* Ast/unit.scm 611 */
																																			obj_t
																																				BgL_arg2858z00_3296;
																																			{	/* Ast/unit.scm 611 */
																																				obj_t
																																					BgL_za71za7_5501;
																																				obj_t
																																					BgL_za72za7_5502;
																																				BgL_za71za7_5501
																																					=
																																					BINT
																																					(BgL_arityz00_3171);
																																				BgL_za72za7_5502
																																					=
																																					BINT
																																					(BgL_loptz00_3174);
																																				{	/* Ast/unit.scm 611 */
																																					obj_t
																																						BgL_tmpz00_5503;
																																					BgL_tmpz00_5503
																																						=
																																						BINT
																																						(0L);
																																					if (BGL_ADDFX_OV(BgL_za71za7_5501, BgL_za72za7_5502, BgL_tmpz00_5503))
																																						{	/* Ast/unit.scm 611 */
																																							BgL_arg2858z00_3296
																																								=
																																								bgl_bignum_add
																																								(bgl_long_to_bignum
																																								((long) CINT(BgL_za71za7_5501)), bgl_long_to_bignum((long) CINT(BgL_za72za7_5502)));
																																						}
																																					else
																																						{	/* Ast/unit.scm 611 */
																																							BgL_arg2858z00_3296
																																								=
																																								BgL_tmpz00_5503;
																																						}
																																				}
																																			}
																																			{	/* Ast/unit.scm 611 */

																																				BgL_arg2844z00_3288
																																					=
																																					BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																																					(
																																					(long)
																																					CINT
																																					(BgL_arg2858z00_3296),
																																					10L);
																																		}}
																																		{	/* Ast/unit.scm 608 */
																																			obj_t
																																				BgL_list2845z00_3289;
																																			{	/* Ast/unit.scm 608 */
																																				obj_t
																																					BgL_arg2846z00_3290;
																																				{	/* Ast/unit.scm 608 */
																																					obj_t
																																						BgL_arg2847z00_3291;
																																					{	/* Ast/unit.scm 608 */
																																						obj_t
																																							BgL_arg2848z00_3292;
																																						{	/* Ast/unit.scm 608 */
																																							obj_t
																																								BgL_arg2856z00_3293;
																																							BgL_arg2856z00_3293
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_string3570z00zzast_unitz00,
																																								BNIL);
																																							BgL_arg2848z00_3292
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg2844z00_3288,
																																								BgL_arg2856z00_3293);
																																						}
																																						BgL_arg2847z00_3291
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_string3571z00zzast_unitz00,
																																							BgL_arg2848z00_3292);
																																					}
																																					BgL_arg2846z00_3290
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg2839z00_3287,
																																						BgL_arg2847z00_3291);
																																				}
																																				BgL_list2845z00_3289
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_string3572z00zzast_unitz00,
																																					BgL_arg2846z00_3290);
																																			}
																																			BgL_arg2837z00_3285
																																				=
																																				BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																																				(BgL_list2845z00_3289);
																																	}}
																																	{	/* Ast/unit.scm 613 */
																																		obj_t
																																			BgL_arg2864z00_3300;
																																		{	/* Ast/unit.scm 613 */
																																			obj_t
																																				BgL_arg2870z00_3301;
																																			BgL_arg2870z00_3301
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_ioptz00_3172,
																																				BNIL);
																																			BgL_arg2864z00_3300
																																				=
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(25),
																																				BgL_arg2870z00_3301);
																																		}
																																		BgL_arg2838z00_3286
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg2864z00_3300,
																																			BNIL);
																																	}
																																	BgL_arg2835z00_3283
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg2837z00_3285,
																																		BgL_arg2838z00_3286);
																																}
																																BgL_arg2831z00_3279
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg2834z00_3282,
																																	BgL_arg2835z00_3283);
																															}
																															BgL_arg2828z00_3276
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg2830z00_3278,
																																BgL_arg2831z00_3279);
																														}
																														{	/* Ast/unit.scm 614 */
																															obj_t
																																BgL_arg2871z00_3302;
																															{	/* Ast/unit.scm 614 */
																																obj_t
																																	BgL_arg2872z00_3303;
																																{	/* Ast/unit.scm 614 */
																																	obj_t
																																		BgL_arg2873z00_3304;
																																	obj_t
																																		BgL_arg2874z00_3305;
																																	{	/* Ast/unit.scm 614 */
																																		obj_t
																																			BgL_arg2875z00_3306;
																																		{	/* Ast/unit.scm 614 */
																																			obj_t
																																				BgL_arg2876z00_3307;
																																			{	/* Ast/unit.scm 614 */
																																				obj_t
																																					BgL_arg2877z00_3308;
																																				{	/* Ast/unit.scm 614 */
																																					obj_t
																																						BgL_arg2878z00_3309;
																																					{	/* Ast/unit.scm 614 */
																																						obj_t
																																							BgL_arg2879z00_3310;
																																						BgL_arg2879z00_3310
																																							=
																																							MAKE_YOUNG_PAIR
																																							(CNST_TABLE_REF
																																							(41),
																																							BNIL);
																																						BgL_arg2878z00_3309
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_ioptz00_3172,
																																							BgL_arg2879z00_3310);
																																					}
																																					BgL_arg2877z00_3308
																																						=
																																						MAKE_YOUNG_PAIR
																																						(CNST_TABLE_REF
																																						(24),
																																						BgL_arg2878z00_3309);
																																				}
																																				BgL_arg2876z00_3307
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg2877z00_3308,
																																					BNIL);
																																			}
																																			BgL_arg2875z00_3306
																																				=
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(44),
																																				BgL_arg2876z00_3307);
																																		}
																																		BgL_arg2873z00_3304
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg2875z00_3306,
																																			BNIL);
																																	}
																																	{	/* Ast/unit.scm 615 */
																																		obj_t
																																			BgL_arg2880z00_3311;
																																		{	/* Ast/unit.scm 615 */
																																			obj_t
																																				BgL_arg2881z00_3312;
																																			{	/* Ast/unit.scm 615 */
																																				obj_t
																																					BgL_arg2882z00_3313;
																																				obj_t
																																					BgL_arg2883z00_3314;
																																				{	/* Ast/unit.scm 615 */
																																					obj_t
																																						BgL_arg2887z00_3315;
																																					{	/* Ast/unit.scm 615 */
																																						obj_t
																																							BgL_arg2888z00_3316;
																																						BgL_arg2888z00_3316
																																							=
																																							MAKE_YOUNG_PAIR
																																							(CNST_TABLE_REF
																																							(42),
																																							BNIL);
																																						BgL_arg2887z00_3315
																																							=
																																							MAKE_YOUNG_PAIR
																																							(CNST_TABLE_REF
																																							(44),
																																							BgL_arg2888z00_3316);
																																					}
																																					BgL_arg2882z00_3313
																																						=
																																						MAKE_YOUNG_PAIR
																																						(CNST_TABLE_REF
																																						(45),
																																						BgL_arg2887z00_3315);
																																				}
																																				{	/* Ast/unit.scm 616 */
																																					obj_t
																																						BgL_arg2889z00_3317;
																																					obj_t
																																						BgL_arg2890z00_3318;
																																					{	/* Ast/unit.scm 616 */
																																						obj_t
																																							BgL_arg2891z00_3319;
																																						{	/* Ast/unit.scm 616 */
																																							obj_t
																																								BgL_arg2892z00_3320;
																																							BgL_arg2892z00_3320
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BINT
																																								(1L),
																																								BNIL);
																																							BgL_arg2891z00_3319
																																								=
																																								MAKE_YOUNG_PAIR
																																								(CNST_TABLE_REF
																																								(41),
																																								BgL_arg2892z00_3320);
																																						}
																																						BgL_arg2889z00_3317
																																							=
																																							MAKE_YOUNG_PAIR
																																							(CNST_TABLE_REF
																																							(46),
																																							BgL_arg2891z00_3319);
																																					}
																																					{	/* Ast/unit.scm 617 */
																																						obj_t
																																							BgL_arg2893z00_3321;
																																						{	/* Ast/unit.scm 617 */
																																							obj_t
																																								BgL_arg2894z00_3322;
																																							{	/* Ast/unit.scm 617 */
																																								obj_t
																																									BgL_arg2895z00_3323;
																																								{	/* Ast/unit.scm 617 */
																																									obj_t
																																										BgL_arg2896z00_3324;
																																									{	/* Ast/unit.scm 617 */
																																										obj_t
																																											BgL_arg2897z00_3325;
																																										{	/* Ast/unit.scm 617 */
																																											obj_t
																																												BgL_arg2898z00_3326;
																																											BgL_arg2898z00_3326
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BINT
																																												(2L),
																																												BNIL);
																																											BgL_arg2897z00_3325
																																												=
																																												MAKE_YOUNG_PAIR
																																												(CNST_TABLE_REF
																																												(41),
																																												BgL_arg2898z00_3326);
																																										}
																																										BgL_arg2896z00_3324
																																											=
																																											MAKE_YOUNG_PAIR
																																											(CNST_TABLE_REF
																																											(46),
																																											BgL_arg2897z00_3325);
																																									}
																																									BgL_arg2895z00_3323
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_arg2896z00_3324,
																																										BNIL);
																																								}
																																								BgL_arg2894z00_3322
																																									=
																																									MAKE_YOUNG_PAIR
																																									(CNST_TABLE_REF
																																									(42),
																																									BgL_arg2895z00_3323);
																																							}
																																							BgL_arg2893z00_3321
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_searchz00_3176,
																																								BgL_arg2894z00_3322);
																																						}
																																						BgL_arg2890z00_3318
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg2893z00_3321,
																																							BNIL);
																																					}
																																					BgL_arg2883z00_3314
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg2889z00_3317,
																																						BgL_arg2890z00_3318);
																																				}
																																				BgL_arg2881z00_3312
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg2882z00_3313,
																																					BgL_arg2883z00_3314);
																																			}
																																			BgL_arg2880z00_3311
																																				=
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(3),
																																				BgL_arg2881z00_3312);
																																		}
																																		BgL_arg2874z00_3305
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg2880z00_3311,
																																			BNIL);
																																	}
																																	BgL_arg2872z00_3303
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg2873z00_3304,
																																		BgL_arg2874z00_3305);
																																}
																																BgL_arg2871z00_3302
																																	=
																																	MAKE_YOUNG_PAIR
																																	(CNST_TABLE_REF
																																	(48),
																																	BgL_arg2872z00_3303);
																															}
																															BgL_arg2829z00_3277
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg2871z00_3302,
																																BNIL);
																														}
																														BgL_arg2820z00_3270
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg2828z00_3276,
																															BgL_arg2829z00_3277);
																													}
																													BgL_arg2816z00_3268 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg2818z00_3269,
																														BgL_arg2820z00_3270);
																												}
																												BgL_arg2766z00_3239 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(3),
																													BgL_arg2816z00_3268);
																									}}
																									BgL_arg2765z00_3238 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg2766z00_3239, BNIL);
																								}
																								BgL_arg2761z00_3235 =
																									MAKE_YOUNG_PAIR(BINT(-1L),
																									BgL_arg2765z00_3238);
																							}
																							BgL_arg2759z00_3233 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2760z00_3234,
																								BgL_arg2761z00_3235);
																						}
																						BgL_arg2758z00_3232 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
																							BgL_arg2759z00_3233);
																					}
																					BgL_arg2756z00_3230 =
																						MAKE_YOUNG_PAIR(BgL_arg2758z00_3232,
																						BNIL);
																				}
																				BgL_arg2754z00_3228 =
																					MAKE_YOUNG_PAIR(BgL_arg2755z00_3229,
																					BgL_arg2756z00_3230);
																			}
																			BgL_arg2753z00_3227 =
																				MAKE_YOUNG_PAIR(BgL_searchz00_3176,
																				BgL_arg2754z00_3228);
																		}
																		BgL_arg2750z00_3225 =
																			MAKE_YOUNG_PAIR(BgL_arg2753z00_3227,
																			BNIL);
																	}
																	{	/* Ast/unit.scm 618 */
																		obj_t BgL_arg2899z00_3329;

																		{	/* Ast/unit.scm 618 */
																			obj_t BgL_arg2903z00_3330;

																			{	/* Ast/unit.scm 618 */
																				obj_t BgL_arg2904z00_3331;
																				obj_t BgL_arg2905z00_3332;

																				{	/* Ast/unit.scm 618 */
																					obj_t BgL_ll1315z00_3333;
																					obj_t BgL_ll1316z00_3334;

																					BgL_ll1315z00_3333 =
																						BGl_takez00zz__r4_pairs_and_lists_6_3z00
																						((((BgL_sfunz00_bglt)
																								COBJECT(((BgL_sfunz00_bglt) ((
																												(BgL_variablez00_bglt)
																												COBJECT((
																														(BgL_variablez00_bglt)
																														((BgL_globalz00_bglt) BgL_gloz00_80))))->BgL_valuez00))))->BgL_argszd2namezd2), BgL_arityz00_3171);
																					BgL_ll1316z00_3334 =
																						BGl_iotaz00zz__r4_pairs_and_lists_6_3z00
																						((int) (BgL_arityz00_3171), BNIL);
																					if (NULLP(BgL_ll1315z00_3333))
																						{	/* Ast/unit.scm 618 */
																							BgL_arg2904z00_3331 = BNIL;
																						}
																					else
																						{	/* Ast/unit.scm 618 */
																							obj_t BgL_head1317z00_3336;

																							BgL_head1317z00_3336 =
																								MAKE_YOUNG_PAIR(BNIL, BNIL);
																							{
																								obj_t BgL_ll1315z00_3338;
																								obj_t BgL_ll1316z00_3339;
																								obj_t BgL_tail1318z00_3340;

																								BgL_ll1315z00_3338 =
																									BgL_ll1315z00_3333;
																								BgL_ll1316z00_3339 =
																									BgL_ll1316z00_3334;
																								BgL_tail1318z00_3340 =
																									BgL_head1317z00_3336;
																							BgL_zc3z04anonymousza32907ze3z87_3341:
																								if (NULLP
																									(BgL_ll1315z00_3338))
																									{	/* Ast/unit.scm 618 */
																										BgL_arg2904z00_3331 =
																											CDR(BgL_head1317z00_3336);
																									}
																								else
																									{	/* Ast/unit.scm 618 */
																										obj_t
																											BgL_newtail1319z00_3343;
																										{	/* Ast/unit.scm 618 */
																											obj_t BgL_arg2914z00_3346;

																											{	/* Ast/unit.scm 618 */
																												obj_t BgL_vz00_3347;
																												obj_t BgL_iz00_3348;

																												BgL_vz00_3347 =
																													CAR(
																													((obj_t)
																														BgL_ll1315z00_3338));
																												BgL_iz00_3348 =
																													CAR(((obj_t)
																														BgL_ll1316z00_3339));
																												{	/* Ast/unit.scm 619 */
																													obj_t
																														BgL_arg2915z00_3349;
																													{	/* Ast/unit.scm 619 */
																														obj_t
																															BgL_arg2916z00_3350;
																														{	/* Ast/unit.scm 619 */
																															obj_t
																																BgL_arg2917z00_3351;
																															{	/* Ast/unit.scm 619 */
																																obj_t
																																	BgL_arg2918z00_3352;
																																BgL_arg2918z00_3352
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_iz00_3348,
																																	BNIL);
																																BgL_arg2917z00_3351
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_ioptz00_3172,
																																	BgL_arg2918z00_3352);
																															}
																															BgL_arg2916z00_3350
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(24),
																																BgL_arg2917z00_3351);
																														}
																														BgL_arg2915z00_3349
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg2916z00_3350,
																															BNIL);
																													}
																													BgL_arg2914z00_3346 =
																														MAKE_YOUNG_PAIR
																														(BgL_vz00_3347,
																														BgL_arg2915z00_3349);
																												}
																											}
																											BgL_newtail1319z00_3343 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg2914z00_3346,
																												BNIL);
																										}
																										SET_CDR
																											(BgL_tail1318z00_3340,
																											BgL_newtail1319z00_3343);
																										{	/* Ast/unit.scm 618 */
																											obj_t BgL_arg2912z00_3344;
																											obj_t BgL_arg2913z00_3345;

																											BgL_arg2912z00_3344 =
																												CDR(
																												((obj_t)
																													BgL_ll1315z00_3338));
																											BgL_arg2913z00_3345 =
																												CDR(((obj_t)
																													BgL_ll1316z00_3339));
																											{
																												obj_t
																													BgL_tail1318z00_8759;
																												obj_t
																													BgL_ll1316z00_8758;
																												obj_t
																													BgL_ll1315z00_8757;
																												BgL_ll1315z00_8757 =
																													BgL_arg2912z00_3344;
																												BgL_ll1316z00_8758 =
																													BgL_arg2913z00_3345;
																												BgL_tail1318z00_8759 =
																													BgL_newtail1319z00_3343;
																												BgL_tail1318z00_3340 =
																													BgL_tail1318z00_8759;
																												BgL_ll1316z00_3339 =
																													BgL_ll1316z00_8758;
																												BgL_ll1315z00_3338 =
																													BgL_ll1315z00_8757;
																												goto
																													BgL_zc3z04anonymousza32907ze3z87_3341;
																											}
																										}
																									}
																							}
																						}
																				}
																				{	/* Ast/unit.scm 622 */
																					obj_t BgL_arg2923z00_3357;

																					{	/* Ast/unit.scm 622 */
																						obj_t BgL_arg2924z00_3358;

																						{	/* Ast/unit.scm 622 */
																							obj_t BgL_arg2925z00_3359;
																							obj_t BgL_arg2926z00_3360;

																							{	/* Ast/unit.scm 622 */
																								obj_t BgL_head1323z00_3363;

																								BgL_head1323z00_3363 =
																									MAKE_YOUNG_PAIR(BNIL, BNIL);
																								{
																									obj_t BgL_l1321z00_3365;
																									obj_t BgL_tail1324z00_3366;

																									BgL_l1321z00_3365 =
																										BgL_keysz00_81;
																									BgL_tail1324z00_3366 =
																										BgL_head1323z00_3363;
																								BgL_zc3z04anonymousza32928ze3z87_3367:
																									if (NULLP
																										(BgL_l1321z00_3365))
																										{	/* Ast/unit.scm 622 */
																											BgL_arg2925z00_3359 =
																												CDR
																												(BgL_head1323z00_3363);
																										}
																									else
																										{	/* Ast/unit.scm 622 */
																											obj_t
																												BgL_newtail1325z00_3369;
																											{	/* Ast/unit.scm 622 */
																												obj_t
																													BgL_arg2931z00_3371;
																												{	/* Ast/unit.scm 622 */
																													obj_t BgL_pz00_3372;

																													BgL_pz00_3372 =
																														CAR(
																														((obj_t)
																															BgL_l1321z00_3365));
																													{	/* Ast/unit.scm 622 */
																														obj_t
																															BgL_arg2932z00_3373;
																														obj_t
																															BgL_arg2933z00_3374;
																														BgL_arg2932z00_3373
																															=
																															CAR(((obj_t)
																																BgL_pz00_3372));
																														{	/* Ast/unit.scm 622 */
																															obj_t
																																BgL_pairz00_5525;
																															BgL_pairz00_5525 =
																																CDR(((obj_t)
																																	BgL_pz00_3372));
																															BgL_arg2933z00_3374
																																=
																																CAR
																																(BgL_pairz00_5525);
																														}
																														{	/* Ast/unit.scm 622 */
																															obj_t
																																BgL_list2934z00_3375;
																															{	/* Ast/unit.scm 622 */
																																obj_t
																																	BgL_arg2935z00_3376;
																																BgL_arg2935z00_3376
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg2933z00_3374,
																																	BNIL);
																																BgL_list2934z00_3375
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg2932z00_3373,
																																	BgL_arg2935z00_3376);
																															}
																															BgL_arg2931z00_3371
																																=
																																BgL_list2934z00_3375;
																														}
																													}
																												}
																												BgL_newtail1325z00_3369
																													=
																													MAKE_YOUNG_PAIR
																													(BgL_arg2931z00_3371,
																													BNIL);
																											}
																											SET_CDR
																												(BgL_tail1324z00_3366,
																												BgL_newtail1325z00_3369);
																											{	/* Ast/unit.scm 622 */
																												obj_t
																													BgL_arg2930z00_3370;
																												BgL_arg2930z00_3370 =
																													CDR(((obj_t)
																														BgL_l1321z00_3365));
																												{
																													obj_t
																														BgL_tail1324z00_8778;
																													obj_t
																														BgL_l1321z00_8777;
																													BgL_l1321z00_8777 =
																														BgL_arg2930z00_3370;
																													BgL_tail1324z00_8778 =
																														BgL_newtail1325z00_3369;
																													BgL_tail1324z00_3366 =
																														BgL_tail1324z00_8778;
																													BgL_l1321z00_3365 =
																														BgL_l1321z00_8777;
																													goto
																														BgL_zc3z04anonymousza32928ze3z87_3367;
																												}
																											}
																										}
																								}
																							}
																							{	/* Ast/unit.scm 624 */
																								obj_t BgL_arg2936z00_3378;
																								obj_t BgL_arg2940z00_3379;

																								{	/* Ast/unit.scm 624 */
																									bool_t BgL_test3915z00_8779;

																									if (CBOOL
																										(BGl_za2unsafezd2arityza2zd2zzengine_paramz00))
																										{	/* Ast/unit.scm 625 */
																											bool_t
																												BgL__ortest_1125z00_3431;
																											if ((((BgL_globalz00_bglt)
																														COBJECT((
																																(BgL_globalz00_bglt)
																																BgL_gloz00_80)))->
																													BgL_evaluablezf3zf3))
																												{	/* Ast/unit.scm 625 */
																													BgL__ortest_1125z00_3431
																														= ((bool_t) 0);
																												}
																											else
																												{	/* Ast/unit.scm 625 */
																													BgL__ortest_1125z00_3431
																														= ((bool_t) 1);
																												}
																											if (BgL__ortest_1125z00_3431)
																												{	/* Ast/unit.scm 625 */
																													BgL_test3915z00_8779 =
																														BgL__ortest_1125z00_3431;
																												}
																											else
																												{	/* Ast/unit.scm 625 */
																													BgL_test3915z00_8779 =
																														CBOOL
																														(BGl_za2unsafezd2evalza2zd2zzengine_paramz00);
																												}
																										}
																									else
																										{	/* Ast/unit.scm 624 */
																											BgL_test3915z00_8779 =
																												((bool_t) 0);
																										}
																									if (BgL_test3915z00_8779)
																										{	/* Ast/unit.scm 624 */
																											BgL_arg2936z00_3378 =
																												BUNSPEC;
																										}
																									else
																										{	/* Ast/unit.scm 628 */
																											obj_t BgL_arg2943z00_3383;

																											{	/* Ast/unit.scm 628 */
																												obj_t
																													BgL_arg2954z00_3384;
																												obj_t
																													BgL_arg2955z00_3385;
																												{	/* Ast/unit.scm 628 */
																													obj_t
																														BgL_arg2956z00_3386;
																													{	/* Ast/unit.scm 628 */
																														obj_t
																															BgL_arg2966z00_3387;
																														{	/* Ast/unit.scm 628 */
																															obj_t
																																BgL_arg2967z00_3388;
																															obj_t
																																BgL_arg2968z00_3389;
																															BgL_arg2967z00_3388
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(41), BNIL);
																															{	/* Ast/unit.scm 629 */
																																obj_t
																																	BgL_arg2972z00_3390;
																																{	/* Ast/unit.scm 629 */
																																	obj_t
																																		BgL_arg2973z00_3391;
																																	{	/* Ast/unit.scm 629 */
																																		obj_t
																																			BgL_arg2974z00_3392;
																																		obj_t
																																			BgL_arg2975z00_3393;
																																		{	/* Ast/unit.scm 629 */
																																			obj_t
																																				BgL_arg2976z00_3394;
																																			{	/* Ast/unit.scm 629 */
																																				obj_t
																																					BgL_arg2977z00_3395;
																																				BgL_arg2977z00_3395
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_lz00_3175,
																																					BNIL);
																																				BgL_arg2976z00_3394
																																					=
																																					MAKE_YOUNG_PAIR
																																					(CNST_TABLE_REF
																																					(41),
																																					BgL_arg2977z00_3395);
																																			}
																																			BgL_arg2974z00_3392
																																				=
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(43),
																																				BgL_arg2976z00_3394);
																																		}
																																		{	/* Ast/unit.scm 630 */
																																			obj_t
																																				BgL_arg2978z00_3396;
																																			obj_t
																																				BgL_arg2979z00_3397;
																																			{	/* Ast/unit.scm 630 */
																																				obj_t
																																					BgL_arg2980z00_3398;
																																				BgL_arg2980z00_3398
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BNIL,
																																					BNIL);
																																				BgL_arg2978z00_3396
																																					=
																																					MAKE_YOUNG_PAIR
																																					(CNST_TABLE_REF
																																					(29),
																																					BgL_arg2980z00_3398);
																																			}
																																			{	/* Ast/unit.scm 631 */
																																				obj_t
																																					BgL_arg2981z00_3399;
																																				{	/* Ast/unit.scm 631 */
																																					obj_t
																																						BgL_arg2985z00_3400;
																																					{	/* Ast/unit.scm 631 */
																																						obj_t
																																							BgL_arg2986z00_3401;
																																						obj_t
																																							BgL_arg2987z00_3402;
																																						{	/* Ast/unit.scm 631 */
																																							obj_t
																																								BgL_arg2988z00_3403;
																																							{	/* Ast/unit.scm 631 */
																																								obj_t
																																									BgL_arg2989z00_3404;
																																								obj_t
																																									BgL_arg2990z00_3405;
																																								{	/* Ast/unit.scm 631 */
																																									obj_t
																																										BgL_arg2991z00_3406;
																																									{	/* Ast/unit.scm 631 */
																																										obj_t
																																											BgL_arg2992z00_3407;
																																										BgL_arg2992z00_3407
																																											=
																																											MAKE_YOUNG_PAIR
																																											(CNST_TABLE_REF
																																											(41),
																																											BNIL);
																																										BgL_arg2991z00_3406
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_ioptz00_3172,
																																											BgL_arg2992z00_3407);
																																									}
																																									BgL_arg2989z00_3404
																																										=
																																										MAKE_YOUNG_PAIR
																																										(CNST_TABLE_REF
																																										(49),
																																										BgL_arg2991z00_3406);
																																								}
																																								{	/* Ast/unit.scm 632 */
																																									obj_t
																																										BgL_arg2993z00_3408;
																																									{	/* Ast/unit.scm 632 */
																																										obj_t
																																											BgL_arg2994z00_3409;
																																										{	/* Ast/unit.scm 632 */
																																											obj_t
																																												BgL_tmpz00_8802;
																																											BgL_keysz00_3197
																																												=
																																												BgL_keysz00_81;
																																											{	/* Ast/unit.scm 590 */
																																												obj_t
																																													BgL_head1312z00_3201;
																																												BgL_head1312z00_3201
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BNIL,
																																													BNIL);
																																												{
																																													obj_t
																																														BgL_l1310z00_3203;
																																													obj_t
																																														BgL_tail1313z00_3204;
																																													BgL_l1310z00_3203
																																														=
																																														BgL_keysz00_3197;
																																													BgL_tail1313z00_3204
																																														=
																																														BgL_head1312z00_3201;
																																												BgL_zc3z04anonymousza32731ze3z87_3205:
																																													if (NULLP(BgL_l1310z00_3203))
																																														{	/* Ast/unit.scm 590 */
																																															BgL_tmpz00_8802
																																																=
																																																CDR
																																																(BgL_head1312z00_3201);
																																														}
																																													else
																																														{	/* Ast/unit.scm 590 */
																																															obj_t
																																																BgL_newtail1314z00_3207;
																																															{	/* Ast/unit.scm 590 */
																																																obj_t
																																																	BgL_arg2734z00_3209;
																																																{	/* Ast/unit.scm 590 */
																																																	obj_t
																																																		BgL_kz00_3210;
																																																	BgL_kz00_3210
																																																		=
																																																		CAR
																																																		(
																																																		((obj_t) BgL_l1310z00_3203));
																																																	{	/* Ast/unit.scm 591 */
																																																		obj_t
																																																			BgL_arg2736z00_3211;
																																																		{	/* Ast/unit.scm 591 */
																																																			obj_t
																																																				BgL_arg2737z00_3212;
																																																			BgL_arg2737z00_3212
																																																				=
																																																				CAR
																																																				(
																																																				((obj_t) BgL_kz00_3210));
																																																			BgL_arg2736z00_3211
																																																				=
																																																				BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																																																				(BgL_arg2737z00_3212,
																																																				BgL_locz00_88);
																																																		}
																																																		BgL_arg2734z00_3209
																																																			=
																																																			BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00
																																																			(BgL_arg2736z00_3211);
																																																	}
																																																}
																																																BgL_newtail1314z00_3207
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(BgL_arg2734z00_3209,
																																																	BNIL);
																																															}
																																															SET_CDR
																																																(BgL_tail1313z00_3204,
																																																BgL_newtail1314z00_3207);
																																															{	/* Ast/unit.scm 590 */
																																																obj_t
																																																	BgL_arg2733z00_3208;
																																																BgL_arg2733z00_3208
																																																	=
																																																	CDR
																																																	(
																																																	((obj_t) BgL_l1310z00_3203));
																																																{
																																																	obj_t
																																																		BgL_tail1313z00_8818;
																																																	obj_t
																																																		BgL_l1310z00_8817;
																																																	BgL_l1310z00_8817
																																																		=
																																																		BgL_arg2733z00_3208;
																																																	BgL_tail1313z00_8818
																																																		=
																																																		BgL_newtail1314z00_3207;
																																																	BgL_tail1313z00_3204
																																																		=
																																																		BgL_tail1313z00_8818;
																																																	BgL_l1310z00_3203
																																																		=
																																																		BgL_l1310z00_8817;
																																																	goto
																																																		BgL_zc3z04anonymousza32731ze3z87_3205;
																																																}
																																															}
																																														}
																																												}
																																											}
																																											BgL_arg2994z00_3409
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_tmpz00_8802,
																																												BNIL);
																																										}
																																										BgL_arg2993z00_3408
																																											=
																																											MAKE_YOUNG_PAIR
																																											(CNST_TABLE_REF
																																											(29),
																																											BgL_arg2994z00_3409);
																																									}
																																									BgL_arg2990z00_3405
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_arg2993z00_3408,
																																										BNIL);
																																								}
																																								BgL_arg2988z00_3403
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg2989z00_3404,
																																									BgL_arg2990z00_3405);
																																							}
																																							BgL_arg2986z00_3401
																																								=
																																								MAKE_YOUNG_PAIR
																																								(CNST_TABLE_REF
																																								(50),
																																								BgL_arg2988z00_3403);
																																						}
																																						{	/* Ast/unit.scm 633 */
																																							obj_t
																																								BgL_arg2999z00_3411;
																																							obj_t
																																								BgL_arg3000z00_3412;
																																							{	/* Ast/unit.scm 633 */
																																								obj_t
																																									BgL_arg3001z00_3413;
																																								{	/* Ast/unit.scm 633 */
																																									obj_t
																																										BgL_arg3002z00_3414;
																																									{	/* Ast/unit.scm 633 */
																																										obj_t
																																											BgL_arg3003z00_3415;
																																										{	/* Ast/unit.scm 633 */
																																											obj_t
																																												BgL_arg3008z00_3416;
																																											BgL_arg3008z00_3416
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BINT
																																												(2L),
																																												BNIL);
																																											BgL_arg3003z00_3415
																																												=
																																												MAKE_YOUNG_PAIR
																																												(CNST_TABLE_REF
																																												(41),
																																												BgL_arg3008z00_3416);
																																										}
																																										BgL_arg3002z00_3414
																																											=
																																											MAKE_YOUNG_PAIR
																																											(CNST_TABLE_REF
																																											(46),
																																											BgL_arg3003z00_3415);
																																									}
																																									BgL_arg3001z00_3413
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_arg3002z00_3414,
																																										BNIL);
																																								}
																																								BgL_arg2999z00_3411
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_checkz00_3177,
																																									BgL_arg3001z00_3413);
																																							}
																																							{	/* Ast/unit.scm 634 */
																																								obj_t
																																									BgL_arg3009z00_3417;
																																								{	/* Ast/unit.scm 634 */
																																									obj_t
																																										BgL_arg3010z00_3418;
																																									obj_t
																																										BgL_arg3011z00_3419;
																																									{	/* Ast/unit.scm 634 */
																																										obj_t
																																											BgL_arg3012z00_3420;
																																										{	/* Ast/unit.scm 634 */
																																											obj_t
																																												BgL_arg3013z00_3421;
																																											BgL_arg3013z00_3421
																																												=
																																												MAKE_YOUNG_PAIR
																																												(CNST_TABLE_REF
																																												(27),
																																												BNIL);
																																											BgL_arg3012z00_3420
																																												=
																																												MAKE_YOUNG_PAIR
																																												(CNST_TABLE_REF
																																												(28),
																																												BgL_arg3013z00_3421);
																																										}
																																										BgL_arg3010z00_3418
																																											=
																																											MAKE_YOUNG_PAIR
																																											(CNST_TABLE_REF
																																											(0),
																																											BgL_arg3012z00_3420);
																																									}
																																									{	/* Ast/unit.scm 635 */
																																										obj_t
																																											BgL_arg3014z00_3422;
																																										obj_t
																																											BgL_arg3015z00_3423;
																																										{	/* Ast/unit.scm 635 */
																																											obj_t
																																												BgL_arg3016z00_3424;
																																											BgL_arg3016z00_3424
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_idz00_82,
																																												BNIL);
																																											BgL_arg3014z00_3422
																																												=
																																												MAKE_YOUNG_PAIR
																																												(CNST_TABLE_REF
																																												(29),
																																												BgL_arg3016z00_3424);
																																										}
																																										{	/* Ast/unit.scm 637 */
																																											obj_t
																																												BgL_arg3017z00_3425;
																																											{	/* Ast/unit.scm 637 */
																																												obj_t
																																													BgL_arg3018z00_3426;
																																												{	/* Ast/unit.scm 637 */
																																													obj_t
																																														BgL_arg3019z00_3427;
																																													{	/* Ast/unit.scm 637 */
																																														obj_t
																																															BgL_arg3020z00_3428;
																																														BgL_arg3020z00_3428
																																															=
																																															MAKE_YOUNG_PAIR
																																															(CNST_TABLE_REF
																																															(41),
																																															BNIL);
																																														BgL_arg3019z00_3427
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BgL_ioptz00_3172,
																																															BgL_arg3020z00_3428);
																																													}
																																													BgL_arg3018z00_3426
																																														=
																																														MAKE_YOUNG_PAIR
																																														(CNST_TABLE_REF
																																														(49),
																																														BgL_arg3019z00_3427);
																																												}
																																												BgL_arg3017z00_3425
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_arg3018z00_3426,
																																													BNIL);
																																											}
																																											BgL_arg3015z00_3423
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BGl_string3573z00zzast_unitz00,
																																												BgL_arg3017z00_3425);
																																										}
																																										BgL_arg3011z00_3419
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_arg3014z00_3422,
																																											BgL_arg3015z00_3423);
																																									}
																																									BgL_arg3009z00_3417
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_arg3010z00_3418,
																																										BgL_arg3011z00_3419);
																																								}
																																								BgL_arg3000z00_3412
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg3009z00_3417,
																																									BNIL);
																																							}
																																							BgL_arg2987z00_3402
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg2999z00_3411,
																																								BgL_arg3000z00_3412);
																																						}
																																						BgL_arg2985z00_3400
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg2986z00_3401,
																																							BgL_arg2987z00_3402);
																																					}
																																					BgL_arg2981z00_3399
																																						=
																																						MAKE_YOUNG_PAIR
																																						(CNST_TABLE_REF
																																						(3),
																																						BgL_arg2985z00_3400);
																																				}
																																				BgL_arg2979z00_3397
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg2981z00_3399,
																																					BNIL);
																																			}
																																			BgL_arg2975z00_3393
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg2978z00_3396,
																																				BgL_arg2979z00_3397);
																																		}
																																		BgL_arg2973z00_3391
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg2974z00_3392,
																																			BgL_arg2975z00_3393);
																																	}
																																	BgL_arg2972z00_3390
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(3),
																																		BgL_arg2973z00_3391);
																																}
																																BgL_arg2968z00_3389
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg2972z00_3390,
																																	BNIL);
																															}
																															BgL_arg2966z00_3387
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg2967z00_3388,
																																BgL_arg2968z00_3389);
																														}
																														BgL_arg2956z00_3386
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_checkz00_3177,
																															BgL_arg2966z00_3387);
																													}
																													BgL_arg2954z00_3384 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg2956z00_3386,
																														BNIL);
																												}
																												{	/* Ast/unit.scm 638 */
																													obj_t
																														BgL_arg3021z00_3429;
																													{	/* Ast/unit.scm 638 */
																														obj_t
																															BgL_arg3023z00_3430;
																														BgL_arg3023z00_3430
																															=
																															MAKE_YOUNG_PAIR
																															(BINT
																															(BgL_arityz00_3171),
																															BNIL);
																														BgL_arg3021z00_3429
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_checkz00_3177,
																															BgL_arg3023z00_3430);
																													}
																													BgL_arg2955z00_3385 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg3021z00_3429,
																														BNIL);
																												}
																												BgL_arg2943z00_3383 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg2954z00_3384,
																													BgL_arg2955z00_3385);
																											}
																											BgL_arg2936z00_3378 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(18),
																												BgL_arg2943z00_3383);
																										}
																								}
																								{	/* Ast/unit.scm 639 */
																									obj_t BgL_arg3024z00_3433;
																									obj_t BgL_arg3025z00_3434;

																									{	/* Ast/unit.scm 639 */
																										obj_t BgL_head1328z00_3437;

																										BgL_head1328z00_3437 =
																											MAKE_YOUNG_PAIR(BNIL,
																											BNIL);
																										{
																											obj_t BgL_l1326z00_3439;
																											obj_t
																												BgL_tail1329z00_3440;
																											BgL_l1326z00_3439 =
																												BgL_keysz00_81;
																											BgL_tail1329z00_3440 =
																												BgL_head1328z00_3437;
																										BgL_zc3z04anonymousza33027ze3z87_3441:
																											if (NULLP
																												(BgL_l1326z00_3439))
																												{	/* Ast/unit.scm 639 */
																													BgL_arg3024z00_3433 =
																														CDR
																														(BgL_head1328z00_3437);
																												}
																											else
																												{	/* Ast/unit.scm 639 */
																													obj_t
																														BgL_newtail1330z00_3443;
																													{	/* Ast/unit.scm 639 */
																														obj_t
																															BgL_arg3030z00_3445;
																														{	/* Ast/unit.scm 639 */
																															obj_t
																																BgL_pz00_3446;
																															BgL_pz00_3446 =
																																CAR(((obj_t)
																																	BgL_l1326z00_3439));
																															{	/* Ast/unit.scm 640 */
																																obj_t
																																	BgL_iz00_3447;
																																{	/* Ast/unit.scm 640 */
																																	obj_t
																																		BgL_arg3077z00_3471;
																																	BgL_arg3077z00_3471
																																		=
																																		CAR(((obj_t)
																																			BgL_pz00_3446));
																																	BgL_iz00_3447
																																		=
																																		BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																																		(BgL_arg3077z00_3471,
																																		BgL_locz00_88);
																																}
																																{	/* Ast/unit.scm 640 */
																																	obj_t
																																		BgL_k1z00_3448;
																																	BgL_k1z00_3448
																																		=
																																		BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00
																																		(BgL_iz00_3447);
																																	{	/* Ast/unit.scm 641 */
																																		obj_t
																																			BgL_indz00_3449;
																																		BgL_indz00_3449
																																			=
																																			BGl_gensymz00zz__r4_symbols_6_4z00
																																			(CNST_TABLE_REF
																																			(51));
																																		{	/* Ast/unit.scm 642 */

																																			{	/* Ast/unit.scm 643 */
																																				obj_t
																																					BgL_arg3031z00_3450;
																																				obj_t
																																					BgL_arg3032z00_3451;
																																				BgL_arg3031z00_3450
																																					=
																																					BGl_letzd2symzd2zzast_letz00
																																					();
																																				{	/* Ast/unit.scm 643 */
																																					obj_t
																																						BgL_arg3037z00_3452;
																																					obj_t
																																						BgL_arg3038z00_3453;
																																					{	/* Ast/unit.scm 643 */
																																						obj_t
																																							BgL_arg3043z00_3454;
																																						{	/* Ast/unit.scm 643 */
																																							obj_t
																																								BgL_arg3044z00_3455;
																																							{	/* Ast/unit.scm 643 */
																																								obj_t
																																									BgL_arg3049z00_3456;
																																								{	/* Ast/unit.scm 643 */
																																									obj_t
																																										BgL_arg3050z00_3457;
																																									{	/* Ast/unit.scm 643 */
																																										obj_t
																																											BgL_arg3054z00_3458;
																																										BgL_arg3054z00_3458
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BINT
																																											(BgL_arityz00_3171),
																																											BNIL);
																																										BgL_arg3050z00_3457
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_k1z00_3448,
																																											BgL_arg3054z00_3458);
																																									}
																																									BgL_arg3049z00_3456
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_searchz00_3176,
																																										BgL_arg3050z00_3457);
																																								}
																																								BgL_arg3044z00_3455
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg3049z00_3456,
																																									BNIL);
																																							}
																																							BgL_arg3043z00_3454
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_indz00_3449,
																																								BgL_arg3044z00_3455);
																																						}
																																						BgL_arg3037z00_3452
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg3043z00_3454,
																																							BNIL);
																																					}
																																					{	/* Ast/unit.scm 644 */
																																						obj_t
																																							BgL_arg3055z00_3459;
																																						{	/* Ast/unit.scm 644 */
																																							obj_t
																																								BgL_arg3058z00_3460;
																																							{	/* Ast/unit.scm 644 */
																																								obj_t
																																									BgL_arg3059z00_3461;
																																								obj_t
																																									BgL_arg3061z00_3462;
																																								{	/* Ast/unit.scm 644 */
																																									obj_t
																																										BgL_arg3062z00_3463;
																																									{	/* Ast/unit.scm 644 */
																																										obj_t
																																											BgL_arg3066z00_3464;
																																										BgL_arg3066z00_3464
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BINT
																																											(0L),
																																											BNIL);
																																										BgL_arg3062z00_3463
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_indz00_3449,
																																											BgL_arg3066z00_3464);
																																									}
																																									BgL_arg3059z00_3461
																																										=
																																										MAKE_YOUNG_PAIR
																																										(CNST_TABLE_REF
																																										(52),
																																										BgL_arg3062z00_3463);
																																								}
																																								{	/* Ast/unit.scm 645 */
																																									obj_t
																																										BgL_arg3067z00_3465;
																																									{	/* Ast/unit.scm 645 */
																																										obj_t
																																											BgL_arg3070z00_3466;
																																										{	/* Ast/unit.scm 645 */
																																											obj_t
																																												BgL_arg3071z00_3467;
																																											{	/* Ast/unit.scm 645 */
																																												obj_t
																																													BgL_arg3072z00_3468;
																																												{	/* Ast/unit.scm 645 */
																																													obj_t
																																														BgL_arg3073z00_3469;
																																													{	/* Ast/unit.scm 645 */
																																														obj_t
																																															BgL_arg3075z00_3470;
																																														BgL_arg3075z00_3470
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BgL_indz00_3449,
																																															BNIL);
																																														BgL_arg3073z00_3469
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BgL_ioptz00_3172,
																																															BgL_arg3075z00_3470);
																																													}
																																													BgL_arg3072z00_3468
																																														=
																																														MAKE_YOUNG_PAIR
																																														(CNST_TABLE_REF
																																														(24),
																																														BgL_arg3073z00_3469);
																																												}
																																												BgL_arg3071z00_3467
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_arg3072z00_3468,
																																													BNIL);
																																											}
																																											BgL_arg3070z00_3466
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_iz00_3447,
																																												BgL_arg3071z00_3467);
																																										}
																																										BgL_arg3067z00_3465
																																											=
																																											MAKE_YOUNG_PAIR
																																											(CNST_TABLE_REF
																																											(1),
																																											BgL_arg3070z00_3466);
																																									}
																																									BgL_arg3061z00_3462
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_arg3067z00_3465,
																																										BNIL);
																																								}
																																								BgL_arg3058z00_3460
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg3059z00_3461,
																																									BgL_arg3061z00_3462);
																																							}
																																							BgL_arg3055z00_3459
																																								=
																																								MAKE_YOUNG_PAIR
																																								(CNST_TABLE_REF
																																								(53),
																																								BgL_arg3058z00_3460);
																																						}
																																						BgL_arg3038z00_3453
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg3055z00_3459,
																																							BNIL);
																																					}
																																					BgL_arg3032z00_3451
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg3037z00_3452,
																																						BgL_arg3038z00_3453);
																																				}
																																				BgL_arg3030z00_3445
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg3031z00_3450,
																																					BgL_arg3032z00_3451);
																																			}
																																		}
																																	}
																																}
																															}
																														}
																														BgL_newtail1330z00_3443
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg3030z00_3445,
																															BNIL);
																													}
																													SET_CDR
																														(BgL_tail1329z00_3440,
																														BgL_newtail1330z00_3443);
																													{	/* Ast/unit.scm 639 */
																														obj_t
																															BgL_arg3029z00_3444;
																														BgL_arg3029z00_3444
																															=
																															CDR(((obj_t)
																																BgL_l1326z00_3439));
																														{
																															obj_t
																																BgL_tail1329z00_8918;
																															obj_t
																																BgL_l1326z00_8917;
																															BgL_l1326z00_8917
																																=
																																BgL_arg3029z00_3444;
																															BgL_tail1329z00_8918
																																=
																																BgL_newtail1330z00_3443;
																															BgL_tail1329z00_3440
																																=
																																BgL_tail1329z00_8918;
																															BgL_l1326z00_3439
																																=
																																BgL_l1326z00_8917;
																															goto
																																BgL_zc3z04anonymousza33027ze3z87_3441;
																														}
																													}
																												}
																										}
																									}
																									{	/* Ast/unit.scm 647 */
																										obj_t BgL_arg3078z00_3473;

																										{	/* Ast/unit.scm 647 */
																											obj_t BgL_arg3079z00_3474;

																											{	/* Ast/unit.scm 647 */
																												obj_t
																													BgL_arg3080z00_3475;
																												obj_t
																													BgL_arg3081z00_3476;
																												{	/* Ast/unit.scm 647 */
																													obj_t
																														BgL_l1331z00_3477;
																													BgL_l1331z00_3477 =
																														BGl_iotaz00zz__r4_pairs_and_lists_6_3z00
																														((int)
																														(BgL_arityz00_3171),
																														BNIL);
																													if (NULLP
																														(BgL_l1331z00_3477))
																														{	/* Ast/unit.scm 647 */
																															BgL_arg3080z00_3475
																																= BNIL;
																														}
																													else
																														{	/* Ast/unit.scm 647 */
																															obj_t
																																BgL_head1333z00_3479;
																															BgL_head1333z00_3479
																																=
																																MAKE_YOUNG_PAIR
																																(BNIL, BNIL);
																															{
																																obj_t
																																	BgL_l1331z00_3481;
																																obj_t
																																	BgL_tail1334z00_3482;
																																BgL_l1331z00_3481
																																	=
																																	BgL_l1331z00_3477;
																																BgL_tail1334z00_3482
																																	=
																																	BgL_head1333z00_3479;
																															BgL_zc3z04anonymousza33083ze3z87_3483:
																																if (NULLP
																																	(BgL_l1331z00_3481))
																																	{	/* Ast/unit.scm 647 */
																																		BgL_arg3080z00_3475
																																			=
																																			CDR
																																			(BgL_head1333z00_3479);
																																	}
																																else
																																	{	/* Ast/unit.scm 647 */
																																		obj_t
																																			BgL_newtail1335z00_3485;
																																		{	/* Ast/unit.scm 647 */
																																			obj_t
																																				BgL_arg3086z00_3487;
																																			{	/* Ast/unit.scm 647 */
																																				obj_t
																																					BgL_jz00_3488;
																																				BgL_jz00_3488
																																					=
																																					CAR((
																																						(obj_t)
																																						BgL_l1331z00_3481));
																																				{	/* Ast/unit.scm 648 */
																																					obj_t
																																						BgL_arg3087z00_3489;
																																					{	/* Ast/unit.scm 648 */
																																						obj_t
																																							BgL_arg3088z00_3490;
																																						BgL_arg3088z00_3490
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_jz00_3488,
																																							BNIL);
																																						BgL_arg3087z00_3489
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_ioptz00_3172,
																																							BgL_arg3088z00_3490);
																																					}
																																					BgL_arg3086z00_3487
																																						=
																																						MAKE_YOUNG_PAIR
																																						(CNST_TABLE_REF
																																						(24),
																																						BgL_arg3087z00_3489);
																																				}
																																			}
																																			BgL_newtail1335z00_3485
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg3086z00_3487,
																																				BNIL);
																																		}
																																		SET_CDR
																																			(BgL_tail1334z00_3482,
																																			BgL_newtail1335z00_3485);
																																		{	/* Ast/unit.scm 647 */
																																			obj_t
																																				BgL_arg3085z00_3486;
																																			BgL_arg3085z00_3486
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_l1331z00_3481));
																																			{
																																				obj_t
																																					BgL_tail1334z00_8938;
																																				obj_t
																																					BgL_l1331z00_8937;
																																				BgL_l1331z00_8937
																																					=
																																					BgL_arg3085z00_3486;
																																				BgL_tail1334z00_8938
																																					=
																																					BgL_newtail1335z00_3485;
																																				BgL_tail1334z00_3482
																																					=
																																					BgL_tail1334z00_8938;
																																				BgL_l1331z00_3481
																																					=
																																					BgL_l1331z00_8937;
																																				goto
																																					BgL_zc3z04anonymousza33083ze3z87_3483;
																																			}
																																		}
																																	}
																															}
																														}
																												}
																												BgL_arg3081z00_3476 =
																													BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																													(BGl_zc3z04anonymousza33092ze3ze70z60zzast_unitz00
																													(BgL_locz00_88,
																														BgL_keysz00_81),
																													BNIL);
																												BgL_arg3079z00_3474 =
																													BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																													(BgL_arg3080z00_3475,
																													BgL_arg3081z00_3476);
																											}
																											BgL_arg3078z00_3473 =
																												MAKE_YOUNG_PAIR
																												(BgL_gloz00_80,
																												BgL_arg3079z00_3474);
																										}
																										BgL_arg3025z00_3434 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg3078z00_3473,
																											BNIL);
																									}
																									BgL_arg2940z00_3379 =
																										BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																										(BgL_arg3024z00_3433,
																										BgL_arg3025z00_3434);
																								}
																								BgL_arg2926z00_3360 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg2936z00_3378,
																									BgL_arg2940z00_3379);
																							}
																							BgL_arg2924z00_3358 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2925z00_3359,
																								BgL_arg2926z00_3360);
																						}
																						BgL_arg2923z00_3357 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(26), BgL_arg2924z00_3358);
																					}
																					BgL_arg2905z00_3332 =
																						MAKE_YOUNG_PAIR(BgL_arg2923z00_3357,
																						BNIL);
																				}
																				BgL_arg2903z00_3330 =
																					MAKE_YOUNG_PAIR(BgL_arg2904z00_3331,
																					BgL_arg2905z00_3332);
																			}
																			BgL_arg2899z00_3329 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(48),
																				BgL_arg2903z00_3330);
																		}
																		BgL_arg2751z00_3226 =
																			MAKE_YOUNG_PAIR(BgL_arg2899z00_3329,
																			BNIL);
																	}
																	BgL_arg2749z00_3224 =
																		MAKE_YOUNG_PAIR(BgL_arg2750z00_3225,
																		BgL_arg2751z00_3226);
																}
																BgL_arg2748z00_3223 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
																	BgL_arg2749z00_3224);
															}
															BgL_arg2742z00_3218 =
																MAKE_YOUNG_PAIR(BgL_arg2748z00_3223, BNIL);
														}
														BgL_arg2740z00_3216 =
															MAKE_YOUNG_PAIR(BgL_arg2741z00_3217,
															BgL_arg2742z00_3218);
													}
													BgL_auxz00_8562 =
														MAKE_YOUNG_PAIR(BgL_arg2739z00_3215,
														BgL_arg2740z00_3216);
												}
												BgL_auxz00_8561 =
													BGl_comptimezd2expandzd2zzexpand_epsz00
													(BgL_auxz00_8562);
											}
											BgL_arg2719z00_3187 =
												BGl_compilezd2expandzd2zzexpand_epsz00(BgL_auxz00_8561);
										}
										BgL_gz00_3184 =
											BGl_defzd2globalzd2sfunz12z12zzast_glozd2defzd2
											(BgL_idz00_3181, BgL_arg2716z00_3185, BgL_arg2717z00_3186,
											BgL_modulez00_83, BgL_classz00_87, BgL_srcz00_86,
											CNST_TABLE_REF(35), BgL_arg2719z00_3187);
									}
									{	/* Ast/unit.scm 660 */

										{	/* Ast/unit.scm 664 */
											BgL_typez00_bglt BgL_vz00_5551;

											BgL_vz00_5551 =
												((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00);
											((((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt) BgL_gz00_3184)))->
													BgL_typez00) =
												((BgL_typez00_bglt) BgL_vz00_5551), BUNSPEC);
										}
										((((BgL_globalz00_bglt) COBJECT(BgL_gz00_3184))->
												BgL_evaluablezf3zf3) =
											((bool_t) ((bool_t) 0)), BUNSPEC);
										return BgL_gz00_3184;
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* <@anonymous:3092>~0 */
	obj_t BGl_zc3z04anonymousza33092ze3ze70z60zzast_unitz00(obj_t BgL_locz00_5799,
		obj_t BgL_l1337z00_3495)
	{
		{	/* Ast/unit.scm 650 */
			if (NULLP(BgL_l1337z00_3495))
				{	/* Ast/unit.scm 650 */
					return BNIL;
				}
			else
				{	/* Ast/unit.scm 651 */
					obj_t BgL_arg3094z00_3498;
					obj_t BgL_arg3099z00_3499;

					{	/* Ast/unit.scm 651 */
						obj_t BgL_pz00_3500;

						BgL_pz00_3500 = CAR(((obj_t) BgL_l1337z00_3495));
						{	/* Ast/unit.scm 651 */
							obj_t BgL_idz00_3501;

							{	/* Ast/unit.scm 652 */
								obj_t BgL_arg3106z00_3505;

								BgL_arg3106z00_3505 = CAR(((obj_t) BgL_pz00_3500));
								BgL_idz00_3501 =
									BGl_fastzd2idzd2ofzd2idzd2zzast_identz00(BgL_arg3106z00_3505,
									BgL_locz00_5799);
							}
							{	/* Ast/unit.scm 654 */
								obj_t BgL_arg3100z00_3502;

								BgL_arg3100z00_3502 =
									BGl_symbolzd2ze3keywordz31zz__r4_symbols_6_4z00
									(BgL_idz00_3501);
								{	/* Ast/unit.scm 653 */
									obj_t BgL_list3101z00_3503;

									{	/* Ast/unit.scm 653 */
										obj_t BgL_arg3105z00_3504;

										BgL_arg3105z00_3504 = MAKE_YOUNG_PAIR(BgL_idz00_3501, BNIL);
										BgL_list3101z00_3503 =
											MAKE_YOUNG_PAIR(BgL_arg3100z00_3502, BgL_arg3105z00_3504);
									}
									BgL_arg3094z00_3498 = BgL_list3101z00_3503;
								}
							}
						}
					}
					{	/* Ast/unit.scm 650 */
						obj_t BgL_arg3114z00_3506;

						BgL_arg3114z00_3506 = CDR(((obj_t) BgL_l1337z00_3495));
						BgL_arg3099z00_3499 =
							BGl_zc3z04anonymousza33092ze3ze70z60zzast_unitz00(BgL_locz00_5799,
							BgL_arg3114z00_3506);
					}
					return bgl_append2(BgL_arg3094z00_3498, BgL_arg3099z00_3499);
				}
		}

	}



/* parse-fun-args */
	obj_t BGl_parsezd2funzd2argsz00zzast_unitz00(obj_t BgL_argsz00_89,
		obj_t BgL_srcz00_90, obj_t BgL_locz00_91)
	{
		{	/* Ast/unit.scm 671 */
			{
				obj_t BgL_argsz00_3513;
				obj_t BgL_resz00_3514;

				BgL_argsz00_3513 = BgL_argsz00_89;
				BgL_resz00_3514 = BNIL;
			BgL_zc3z04anonymousza33116ze3z87_3515:
				if (NULLP(BgL_argsz00_3513))
					{	/* Ast/unit.scm 675 */
						return bgl_reverse_bang(BgL_resz00_3514);
					}
				else
					{	/* Ast/unit.scm 675 */
						if (PAIRP(BgL_argsz00_3513))
							{	/* Ast/unit.scm 677 */
								if (BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(CAR
										(BgL_argsz00_3513)))
									{	/* Ast/unit.scm 686 */
										obj_t BgL_argz00_3520;

										BgL_argz00_3520 =
											BGl_dssslzd2findzd2firstzd2formalzd2zztools_dssslz00
											(BgL_argsz00_3513);
										if (CBOOL(BgL_argz00_3520))
											{	/* Ast/unit.scm 688 */
												obj_t BgL_arg3124z00_3521;

												{	/* Ast/unit.scm 688 */
													BgL_localz00_bglt BgL_arg3125z00_3522;

													BgL_arg3125z00_3522 =
														BGl_makezd2userzd2localzd2svarzd2zzast_localz00
														(BgL_argz00_3520,
														((BgL_typez00_bglt)
															BGl_za2objza2z00zztype_cachez00));
													BgL_arg3124z00_3521 =
														MAKE_YOUNG_PAIR(((obj_t) BgL_arg3125z00_3522),
														BgL_resz00_3514);
												}
												return bgl_reverse_bang(BgL_arg3124z00_3521);
											}
										else
											{	/* Ast/unit.scm 687 */
												return bgl_reverse_bang(BgL_resz00_3514);
											}
									}
								else
									{	/* Ast/unit.scm 691 */
										obj_t BgL_pidz00_3523;

										BgL_pidz00_3523 =
											BGl_checkzd2idzd2zzast_identz00
											(BGl_parsezd2idzd2zzast_identz00(CAR(BgL_argsz00_3513),
												BgL_locz00_91), BgL_srcz00_90);
										{	/* Ast/unit.scm 691 */
											obj_t BgL_idz00_3524;

											BgL_idz00_3524 = CAR(BgL_pidz00_3523);
											{	/* Ast/unit.scm 692 */
												obj_t BgL_typez00_3525;

												BgL_typez00_3525 = CDR(BgL_pidz00_3523);
												{	/* Ast/unit.scm 693 */

													{	/* Ast/unit.scm 694 */
														obj_t BgL_arg3126z00_3526;
														obj_t BgL_arg3127z00_3527;

														BgL_arg3126z00_3526 = CDR(BgL_argsz00_3513);
														{	/* Ast/unit.scm 695 */
															BgL_localz00_bglt BgL_arg3128z00_3528;

															if (BGl_userzd2symbolzf3z21zzast_identz00
																(BgL_idz00_3524))
																{	/* Ast/unit.scm 695 */
																	BgL_arg3128z00_3528 =
																		BGl_makezd2userzd2localzd2svarzd2zzast_localz00
																		(BgL_idz00_3524,
																		((BgL_typez00_bglt) BgL_typez00_3525));
																}
															else
																{	/* Ast/unit.scm 695 */
																	BgL_arg3128z00_3528 =
																		BGl_makezd2localzd2svarz00zzast_localz00
																		(BgL_idz00_3524,
																		((BgL_typez00_bglt) BgL_typez00_3525));
																}
															BgL_arg3127z00_3527 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_arg3128z00_3528), BgL_resz00_3514);
														}
														{
															obj_t BgL_resz00_9014;
															obj_t BgL_argsz00_9013;

															BgL_argsz00_9013 = BgL_arg3126z00_3526;
															BgL_resz00_9014 = BgL_arg3127z00_3527;
															BgL_resz00_3514 = BgL_resz00_9014;
															BgL_argsz00_3513 = BgL_argsz00_9013;
															goto BgL_zc3z04anonymousza33116ze3z87_3515;
														}
													}
												}
											}
										}
									}
							}
						else
							{	/* Ast/unit.scm 678 */
								obj_t BgL_pidz00_3533;

								BgL_pidz00_3533 =
									BGl_checkzd2idzd2zzast_identz00
									(BGl_parsezd2idzd2zzast_identz00(BgL_argsz00_3513,
										BgL_locz00_91), BgL_srcz00_90);
								{	/* Ast/unit.scm 678 */
									obj_t BgL_idz00_3534;

									BgL_idz00_3534 = CAR(BgL_pidz00_3533);
									{	/* Ast/unit.scm 679 */
										obj_t BgL_typez00_3535;

										BgL_typez00_3535 = CDR(BgL_pidz00_3533);
										{	/* Ast/unit.scm 680 */

											{	/* Ast/unit.scm 684 */
												obj_t BgL_arg3135z00_3536;

												{	/* Ast/unit.scm 684 */
													BgL_localz00_bglt BgL_arg3136z00_3537;

													BgL_arg3136z00_3537 =
														BGl_makezd2userzd2localzd2svarzd2zzast_localz00
														(BgL_idz00_3534,
														((BgL_typez00_bglt) BgL_typez00_3535));
													BgL_arg3135z00_3536 =
														MAKE_YOUNG_PAIR(((obj_t) BgL_arg3136z00_3537),
														BgL_resz00_3514);
												}
												return bgl_reverse_bang(BgL_arg3135z00_3536);
											}
										}
									}
								}
							}
					}
			}
		}

	}



/* parse-fun-opt-args */
	obj_t BGl_parsezd2funzd2optzd2argszd2zzast_unitz00(obj_t BgL_argsz00_92,
		obj_t BgL_srcz00_93, obj_t BgL_locz00_94)
	{
		{	/* Ast/unit.scm 703 */
			{	/* Ast/unit.scm 704 */
				obj_t BgL_arg3139z00_3540;
				obj_t BgL_arg3140z00_3541;

				{	/* Ast/unit.scm 704 */
					obj_t BgL_l1338z00_3542;

					BgL_l1338z00_3542 =
						BGl_dssslzd2beforezd2dssslz00zztools_dssslz00(BgL_argsz00_92);
					if (NULLP(BgL_l1338z00_3542))
						{	/* Ast/unit.scm 704 */
							BgL_arg3139z00_3540 = BNIL;
						}
					else
						{	/* Ast/unit.scm 704 */
							obj_t BgL_head1340z00_3544;

							BgL_head1340z00_3544 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1338z00_3546;
								obj_t BgL_tail1341z00_3547;

								BgL_l1338z00_3546 = BgL_l1338z00_3542;
								BgL_tail1341z00_3547 = BgL_head1340z00_3544;
							BgL_zc3z04anonymousza33142ze3z87_3548:
								if (NULLP(BgL_l1338z00_3546))
									{	/* Ast/unit.scm 704 */
										BgL_arg3139z00_3540 = CDR(BgL_head1340z00_3544);
									}
								else
									{	/* Ast/unit.scm 704 */
										obj_t BgL_newtail1342z00_3550;

										{	/* Ast/unit.scm 704 */
											BgL_localz00_bglt BgL_arg3145z00_3552;

											{	/* Ast/unit.scm 704 */
												obj_t BgL_az00_3553;

												BgL_az00_3553 = CAR(((obj_t) BgL_l1338z00_3546));
												{	/* Ast/unit.scm 705 */
													obj_t BgL_pidz00_3554;

													BgL_pidz00_3554 =
														BGl_checkzd2idzd2zzast_identz00
														(BGl_parsezd2idzd2zzast_identz00(BgL_az00_3553,
															BgL_locz00_94), BgL_srcz00_93);
													{	/* Ast/unit.scm 705 */
														obj_t BgL_idz00_3555;

														BgL_idz00_3555 = CAR(BgL_pidz00_3554);
														{	/* Ast/unit.scm 706 */
															obj_t BgL_typez00_3556;

															BgL_typez00_3556 = CDR(BgL_pidz00_3554);
															{	/* Ast/unit.scm 707 */

																if (BGl_userzd2symbolzf3z21zzast_identz00
																	(BgL_idz00_3555))
																	{	/* Ast/unit.scm 708 */
																		BgL_arg3145z00_3552 =
																			BGl_makezd2userzd2localzd2svarzd2zzast_localz00
																			(BgL_idz00_3555,
																			((BgL_typez00_bglt) BgL_typez00_3556));
																	}
																else
																	{	/* Ast/unit.scm 708 */
																		BgL_arg3145z00_3552 =
																			BGl_makezd2localzd2svarz00zzast_localz00
																			(BgL_idz00_3555,
																			((BgL_typez00_bglt) BgL_typez00_3556));
																	}
															}
														}
													}
												}
											}
											BgL_newtail1342z00_3550 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg3145z00_3552), BNIL);
										}
										SET_CDR(BgL_tail1341z00_3547, BgL_newtail1342z00_3550);
										{	/* Ast/unit.scm 704 */
											obj_t BgL_arg3144z00_3551;

											BgL_arg3144z00_3551 = CDR(((obj_t) BgL_l1338z00_3546));
											{
												obj_t BgL_tail1341z00_9049;
												obj_t BgL_l1338z00_9048;

												BgL_l1338z00_9048 = BgL_arg3144z00_3551;
												BgL_tail1341z00_9049 = BgL_newtail1342z00_3550;
												BgL_tail1341z00_3547 = BgL_tail1341z00_9049;
												BgL_l1338z00_3546 = BgL_l1338z00_9048;
												goto BgL_zc3z04anonymousza33142ze3z87_3548;
											}
										}
									}
							}
						}
				}
				{	/* Ast/unit.scm 712 */
					obj_t BgL_l1343z00_3560;

					BgL_l1343z00_3560 =
						BGl_dssslzd2optionalszd2zztools_dssslz00(BgL_argsz00_92);
					if (NULLP(BgL_l1343z00_3560))
						{	/* Ast/unit.scm 712 */
							BgL_arg3140z00_3541 = BNIL;
						}
					else
						{	/* Ast/unit.scm 712 */
							obj_t BgL_head1345z00_3562;

							BgL_head1345z00_3562 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1343z00_3564;
								obj_t BgL_tail1346z00_3565;

								BgL_l1343z00_3564 = BgL_l1343z00_3560;
								BgL_tail1346z00_3565 = BgL_head1345z00_3562;
							BgL_zc3z04anonymousza33151ze3z87_3566:
								if (NULLP(BgL_l1343z00_3564))
									{	/* Ast/unit.scm 712 */
										BgL_arg3140z00_3541 = CDR(BgL_head1345z00_3562);
									}
								else
									{	/* Ast/unit.scm 712 */
										obj_t BgL_newtail1347z00_3568;

										{	/* Ast/unit.scm 712 */
											BgL_localz00_bglt BgL_arg3155z00_3570;

											{	/* Ast/unit.scm 712 */
												obj_t BgL_oz00_3571;

												BgL_oz00_3571 = CAR(((obj_t) BgL_l1343z00_3564));
												{	/* Ast/unit.scm 713 */
													obj_t BgL_az00_3572;

													BgL_az00_3572 = CAR(((obj_t) BgL_oz00_3571));
													{	/* Ast/unit.scm 713 */
														obj_t BgL_pidz00_3573;

														BgL_pidz00_3573 =
															BGl_checkzd2idzd2zzast_identz00
															(BGl_parsezd2idzd2zzast_identz00(BgL_az00_3572,
																BgL_locz00_94), BgL_srcz00_93);
														{	/* Ast/unit.scm 714 */
															obj_t BgL_idz00_3574;

															BgL_idz00_3574 = CAR(BgL_pidz00_3573);
															{	/* Ast/unit.scm 715 */
																obj_t BgL_typez00_3575;

																BgL_typez00_3575 = CDR(BgL_pidz00_3573);
																{	/* Ast/unit.scm 716 */

																	BgL_arg3155z00_3570 =
																		BGl_makezd2userzd2localzd2svarzd2zzast_localz00
																		(BgL_idz00_3574,
																		((BgL_typez00_bglt) BgL_typez00_3575));
																}
															}
														}
													}
												}
											}
											BgL_newtail1347z00_3568 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg3155z00_3570), BNIL);
										}
										SET_CDR(BgL_tail1346z00_3565, BgL_newtail1347z00_3568);
										{	/* Ast/unit.scm 712 */
											obj_t BgL_arg3154z00_3569;

											BgL_arg3154z00_3569 = CDR(((obj_t) BgL_l1343z00_3564));
											{
												obj_t BgL_tail1346z00_9073;
												obj_t BgL_l1343z00_9072;

												BgL_l1343z00_9072 = BgL_arg3154z00_3569;
												BgL_tail1346z00_9073 = BgL_newtail1347z00_3568;
												BgL_tail1346z00_3565 = BgL_tail1346z00_9073;
												BgL_l1343z00_3564 = BgL_l1343z00_9072;
												goto BgL_zc3z04anonymousza33151ze3z87_3566;
											}
										}
									}
							}
						}
				}
				return
					BGl_appendzd221011zd2zzast_unitz00(BgL_arg3139z00_3540,
					BgL_arg3140z00_3541);
			}
		}

	}



/* make-svar-definition */
	obj_t BGl_makezd2svarzd2definitionz00zzast_unitz00(obj_t BgL_idz00_102,
		obj_t BgL_srcz00_103)
	{
		{	/* Ast/unit.scm 731 */
			BGl_defzd2globalzd2svarz12z12zzast_glozd2defzd2(BgL_idz00_102,
				BGl_za2moduleza2z00zzmodule_modulez00, BgL_srcz00_103,
				CNST_TABLE_REF(8));
			{	/* Ast/unit.scm 735 */
				obj_t BgL_arg3160z00_3582;
				obj_t BgL_arg3161z00_3583;

				BgL_arg3160z00_3582 = CDR(((obj_t) BgL_srcz00_103));
				BgL_arg3161z00_3583 =
					CAR(BGl_checkzd2idzd2zzast_identz00(BGl_parsezd2idzd2zzast_identz00
						(BgL_idz00_102,
							BGl_findzd2locationzd2zztools_locationz00(BgL_srcz00_103)),
						BgL_srcz00_103));
				{	/* Ast/unit.scm 735 */
					obj_t BgL_tmpz00_9083;

					BgL_tmpz00_9083 = ((obj_t) BgL_arg3160z00_3582);
					SET_CAR(BgL_tmpz00_9083, BgL_arg3161z00_3583);
				}
			}
			{	/* Ast/unit.scm 736 */
				obj_t BgL_list3172z00_3587;

				BgL_list3172z00_3587 = MAKE_YOUNG_PAIR(BgL_srcz00_103, BNIL);
				return BgL_list3172z00_3587;
			}
		}

	}



/* make-generic-definition */
	obj_t BGl_makezd2genericzd2definitionz00zzast_unitz00(obj_t BgL_idz00_110,
		obj_t BgL_modulez00_111, obj_t BgL_argsz00_112, obj_t BgL_bodyz00_113,
		obj_t BgL_srcz00_114)
	{
		{	/* Ast/unit.scm 771 */
			{	/* Ast/unit.scm 774 */
				obj_t BgL_optsz00_3620;
				obj_t BgL_keysz00_3621;

				BgL_optsz00_3620 =
					BGl_dssslzd2optionalszd2zztools_dssslz00(BgL_argsz00_112);
				BgL_keysz00_3621 = BGl_dssslzd2keyszd2zztools_dssslz00(BgL_argsz00_112);
				if (NULLP(BgL_optsz00_3620))
					{	/* Ast/unit.scm 777 */
						if (NULLP(BgL_keysz00_3621))
							{	/* Ast/unit.scm 782 */
								BGL_TAIL return
									BGl_makezd2genericzd2nooptzd2definitionzd2zzast_unitz00
									(BgL_idz00_110, BgL_modulez00_111, BgL_argsz00_112,
									BgL_bodyz00_113, BgL_srcz00_114);
							}
						else
							{	/* Ast/unit.scm 782 */
								return
									BGl_makezd2genericzd2keyzd2definitionzd2zzast_unitz00
									(BgL_keysz00_3621, BgL_idz00_110, BgL_modulez00_111,
									BgL_argsz00_112, BgL_bodyz00_113, BgL_srcz00_114);
							}
					}
				else
					{	/* Ast/unit.scm 778 */
						bool_t BgL_test3936z00_9095;

						{	/* Ast/unit.scm 778 */
							obj_t BgL_arg3229z00_3641;

							{	/* Ast/unit.scm 778 */
								obj_t BgL_arg3230z00_3642;

								{	/* Ast/unit.scm 778 */
									obj_t BgL_hook1352z00_3643;

									BgL_hook1352z00_3643 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
									{
										obj_t BgL_l1349z00_3645;
										obj_t BgL_h1350z00_3646;

										BgL_l1349z00_3645 = BgL_argsz00_112;
										BgL_h1350z00_3646 = BgL_hook1352z00_3643;
									BgL_zc3z04anonymousza33231ze3z87_3647:
										if (NULLP(BgL_l1349z00_3645))
											{	/* Ast/unit.scm 778 */
												BgL_arg3230z00_3642 = CDR(BgL_hook1352z00_3643);
											}
										else
											{	/* Ast/unit.scm 778 */
												bool_t BgL_test3938z00_9100;

												{	/* Ast/unit.scm 778 */
													obj_t BgL_arg3237z00_3654;

													BgL_arg3237z00_3654 =
														CAR(((obj_t) BgL_l1349z00_3645));
													BgL_test3938z00_9100 =
														BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00
														(BgL_arg3237z00_3654);
												}
												if (BgL_test3938z00_9100)
													{	/* Ast/unit.scm 778 */
														obj_t BgL_nh1351z00_3650;

														{	/* Ast/unit.scm 778 */
															obj_t BgL_arg3234z00_3652;

															BgL_arg3234z00_3652 =
																CAR(((obj_t) BgL_l1349z00_3645));
															BgL_nh1351z00_3650 =
																MAKE_YOUNG_PAIR(BgL_arg3234z00_3652, BNIL);
														}
														SET_CDR(BgL_h1350z00_3646, BgL_nh1351z00_3650);
														{	/* Ast/unit.scm 778 */
															obj_t BgL_arg3233z00_3651;

															BgL_arg3233z00_3651 =
																CDR(((obj_t) BgL_l1349z00_3645));
															{
																obj_t BgL_h1350z00_9111;
																obj_t BgL_l1349z00_9110;

																BgL_l1349z00_9110 = BgL_arg3233z00_3651;
																BgL_h1350z00_9111 = BgL_nh1351z00_3650;
																BgL_h1350z00_3646 = BgL_h1350z00_9111;
																BgL_l1349z00_3645 = BgL_l1349z00_9110;
																goto BgL_zc3z04anonymousza33231ze3z87_3647;
															}
														}
													}
												else
													{	/* Ast/unit.scm 778 */
														obj_t BgL_arg3236z00_3653;

														BgL_arg3236z00_3653 =
															CDR(((obj_t) BgL_l1349z00_3645));
														{
															obj_t BgL_l1349z00_9114;

															BgL_l1349z00_9114 = BgL_arg3236z00_3653;
															BgL_l1349z00_3645 = BgL_l1349z00_9114;
															goto BgL_zc3z04anonymousza33231ze3z87_3647;
														}
													}
											}
									}
								}
								BgL_arg3229z00_3641 = CDR(((obj_t) BgL_arg3230z00_3642));
							}
							BgL_test3936z00_9095 = PAIRP(BgL_arg3229z00_3641);
						}
						if (BgL_test3936z00_9095)
							{	/* Ast/unit.scm 780 */
								obj_t BgL_arg3226z00_3640;

								BgL_arg3226z00_3640 =
									BGl_findzd2locationzd2zztools_locationz00(BgL_srcz00_114);
								return
									((obj_t)
									BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00
									(BGl_string3574z00zzast_unitz00, BgL_srcz00_114,
										BgL_arg3226z00_3640));
							}
						else
							{	/* Ast/unit.scm 778 */
								return
									BGl_makezd2genericzd2optzd2definitionzd2zzast_unitz00
									(BgL_optsz00_3620, BgL_idz00_110, BgL_modulez00_111,
									BgL_argsz00_112, BgL_bodyz00_113, BgL_srcz00_114);
							}
					}
			}
		}

	}



/* make-generic-opt-definition */
	obj_t BGl_makezd2genericzd2optzd2definitionzd2zzast_unitz00(obj_t
		BgL_optionalsz00_115, obj_t BgL_idz00_116, obj_t BgL_modulez00_117,
		obj_t BgL_argsz00_118, obj_t BgL_bodyz00_119, obj_t BgL_srcz00_120)
	{
		{	/* Ast/unit.scm 790 */
			{	/* Ast/unit.scm 791 */
				obj_t BgL_locz00_3657;

				BgL_locz00_3657 =
					BGl_findzd2locationzd2zztools_locationz00(BgL_srcz00_120);
				{	/* Ast/unit.scm 791 */
					obj_t BgL_genz00_3658;

					BgL_genz00_3658 =
						BGl_makezd2genericzd2nooptzd2definitionzd2zzast_unitz00
						(BgL_idz00_116, BgL_modulez00_117, BgL_argsz00_118, BgL_bodyz00_119,
						BgL_srcz00_120);
					{	/* Ast/unit.scm 792 */
						obj_t BgL_gloz00_3659;

						{	/* Ast/unit.scm 793 */
							obj_t BgL_arg3239z00_3661;

							BgL_arg3239z00_3661 =
								BGl_fastzd2idzd2ofzd2idzd2zzast_identz00(BgL_idz00_116, BFALSE);
							{	/* Ast/unit.scm 793 */
								obj_t BgL_list3240z00_3662;

								BgL_list3240z00_3662 = MAKE_YOUNG_PAIR(BgL_modulez00_117, BNIL);
								BgL_gloz00_3659 =
									BGl_findzd2globalzd2zzast_envz00(BgL_arg3239z00_3661,
									BgL_list3240z00_3662);
							}
						}
						{	/* Ast/unit.scm 793 */
							BgL_globalz00_bglt BgL_cloz00_3660;

							BgL_cloz00_3660 =
								BGl_makezd2sfunzd2optzd2closurezd2zzast_unitz00(BgL_gloz00_3659,
								BgL_optionalsz00_115, BgL_idz00_116, BgL_modulez00_117,
								BgL_argsz00_118, BgL_bodyz00_119, BgL_srcz00_120,
								CNST_TABLE_REF(13), BgL_locz00_3657);
							{	/* Ast/unit.scm 794 */

								return
									MAKE_YOUNG_PAIR(((obj_t) BgL_cloz00_3660), BgL_genz00_3658);
							}
						}
					}
				}
			}
		}

	}



/* make-generic-key-definition */
	obj_t BGl_makezd2genericzd2keyzd2definitionzd2zzast_unitz00(obj_t
		BgL_keysz00_121, obj_t BgL_idz00_122, obj_t BgL_modulez00_123,
		obj_t BgL_argsz00_124, obj_t BgL_bodyz00_125, obj_t BgL_srcz00_126)
	{
		{	/* Ast/unit.scm 800 */
			{	/* Ast/unit.scm 801 */
				obj_t BgL_locz00_3663;

				BgL_locz00_3663 =
					BGl_findzd2locationzd2zztools_locationz00(BgL_srcz00_126);
				{	/* Ast/unit.scm 801 */
					obj_t BgL_genz00_3664;

					BgL_genz00_3664 =
						BGl_makezd2genericzd2nooptzd2definitionzd2zzast_unitz00
						(BgL_idz00_122, BgL_modulez00_123, BgL_argsz00_124, BgL_bodyz00_125,
						BgL_srcz00_126);
					{	/* Ast/unit.scm 802 */
						obj_t BgL_gloz00_3665;

						{	/* Ast/unit.scm 803 */
							obj_t BgL_arg3241z00_3667;

							BgL_arg3241z00_3667 =
								BGl_fastzd2idzd2ofzd2idzd2zzast_identz00(BgL_idz00_122, BFALSE);
							{	/* Ast/unit.scm 803 */
								obj_t BgL_list3242z00_3668;

								BgL_list3242z00_3668 = MAKE_YOUNG_PAIR(BgL_modulez00_123, BNIL);
								BgL_gloz00_3665 =
									BGl_findzd2globalzd2zzast_envz00(BgL_arg3241z00_3667,
									BgL_list3242z00_3668);
							}
						}
						{	/* Ast/unit.scm 803 */
							BgL_globalz00_bglt BgL_cloz00_3666;

							BgL_cloz00_3666 =
								BGl_makezd2sfunzd2keyzd2closurezd2zzast_unitz00(BgL_gloz00_3665,
								BgL_keysz00_121, BgL_idz00_122, BgL_modulez00_123,
								BgL_argsz00_124, BgL_bodyz00_125, BgL_srcz00_126,
								CNST_TABLE_REF(13), BgL_locz00_3663);
							{	/* Ast/unit.scm 804 */

								return
									MAKE_YOUNG_PAIR(((obj_t) BgL_cloz00_3666), BgL_genz00_3664);
							}
						}
					}
				}
			}
		}

	}



/* make-generic-noopt-definition */
	obj_t BGl_makezd2genericzd2nooptzd2definitionzd2zzast_unitz00(obj_t
		BgL_idz00_127, obj_t BgL_modulez00_128, obj_t BgL_argsz00_129,
		obj_t BgL_bodyz00_130, obj_t BgL_srcz00_131)
	{
		{	/* Ast/unit.scm 810 */
			{	/* Ast/unit.scm 833 */
				bool_t BgL_test3939z00_9140;

				if (PAIRP(BgL_argsz00_129))
					{	/* Ast/unit.scm 833 */
						obj_t BgL_tmpz00_9143;

						BgL_tmpz00_9143 = CAR(BgL_argsz00_129);
						BgL_test3939z00_9140 = SYMBOLP(BgL_tmpz00_9143);
					}
				else
					{	/* Ast/unit.scm 833 */
						BgL_test3939z00_9140 = ((bool_t) 0);
					}
				if (BgL_test3939z00_9140)
					{	/* Ast/unit.scm 838 */
						obj_t BgL_locz00_3674;

						BgL_locz00_3674 =
							BGl_findzd2locationzd2zztools_locationz00(BgL_srcz00_131);
						{	/* Ast/unit.scm 838 */
							obj_t BgL_localsz00_3675;

							{	/* Ast/unit.scm 839 */
								bool_t BgL_test3941z00_9147;

								if (BGl_dssslzd2prototypezf3z21zztools_dssslz00
									(BgL_argsz00_129))
									{	/* Ast/unit.scm 840 */
										obj_t BgL_tmpz00_9150;

										BgL_tmpz00_9150 =
											BGl_dssslzd2optionalszd2zztools_dssslz00(BgL_argsz00_129);
										BgL_test3941z00_9147 = PAIRP(BgL_tmpz00_9150);
									}
								else
									{	/* Ast/unit.scm 839 */
										BgL_test3941z00_9147 = ((bool_t) 0);
									}
								if (BgL_test3941z00_9147)
									{	/* Ast/unit.scm 839 */
										BgL_localsz00_3675 =
											BGl_parsezd2funzd2optzd2argszd2zzast_unitz00
											(BgL_argsz00_129, BgL_srcz00_131, BgL_locz00_3674);
									}
								else
									{	/* Ast/unit.scm 839 */
										BgL_localsz00_3675 =
											BGl_parsezd2funzd2argsz00zzast_unitz00(BgL_argsz00_129,
											BgL_srcz00_131, BgL_locz00_3674);
									}
							}
							{	/* Ast/unit.scm 839 */
								obj_t BgL_pidz00_3676;

								BgL_pidz00_3676 =
									BGl_checkzd2idzd2zzast_identz00
									(BGl_parsezd2idzd2zzast_identz00(BgL_idz00_127,
										BgL_locz00_3674), BgL_srcz00_131);
								{	/* Ast/unit.scm 843 */
									obj_t BgL_namez00_3677;

									BgL_namez00_3677 =
										BGl_gensymz00zz__r4_symbols_6_4z00(CAR(BgL_pidz00_3676));
									{	/* Ast/unit.scm 845 */
										obj_t BgL_gbodyz00_3679;

										BgL_gbodyz00_3679 =
											BGl_makezd2genericzd2bodyz00zzobject_genericz00
											(BgL_idz00_127, BgL_localsz00_3675, BgL_argsz00_129,
											BgL_srcz00_131);
										{	/* Ast/unit.scm 846 */
											BgL_globalz00_bglt BgL_genericz00_3680;

											BgL_genericz00_3680 =
												BGl_defzd2globalzd2sfunz12z12zzast_glozd2defzd2
												(BgL_idz00_127, BgL_argsz00_129, BgL_localsz00_3675,
												BgL_modulez00_128, CNST_TABLE_REF(54), BgL_srcz00_131,
												CNST_TABLE_REF(8), BgL_gbodyz00_3679);
											{	/* Ast/unit.scm 847 */
												obj_t BgL_defz00_3681;

												{	/* Ast/unit.scm 849 */
													obj_t BgL_arg3272z00_3703;

													{	/* Ast/unit.scm 849 */
														obj_t BgL_arg3273z00_3704;
														obj_t BgL_arg3275z00_3705;

														{	/* Ast/unit.scm 849 */
															obj_t BgL_arg3276z00_3706;

															{	/* Ast/unit.scm 849 */
																obj_t BgL_arg3281z00_3707;

																{	/* Ast/unit.scm 849 */
																	obj_t BgL_arg3282z00_3708;
																	obj_t BgL_arg3287z00_3709;

																	{	/* Ast/unit.scm 849 */
																		bool_t BgL_test3943z00_9163;

																		if (BGl_dssslzd2prototypezf3z21zztools_dssslz00(BgL_argsz00_129))
																			{	/* Ast/unit.scm 850 */
																				obj_t BgL_tmpz00_9166;

																				BgL_tmpz00_9166 =
																					BGl_dssslzd2optionalszd2zztools_dssslz00
																					(BgL_argsz00_129);
																				BgL_test3943z00_9163 =
																					PAIRP(BgL_tmpz00_9166);
																			}
																		else
																			{	/* Ast/unit.scm 849 */
																				BgL_test3943z00_9163 = ((bool_t) 0);
																			}
																		if (BgL_test3943z00_9163)
																			{	/* Ast/unit.scm 849 */
																				if (NULLP(BgL_localsz00_3675))
																					{	/* Ast/unit.scm 851 */
																						BgL_arg3282z00_3708 = BNIL;
																					}
																				else
																					{	/* Ast/unit.scm 851 */
																						obj_t BgL_head1355z00_3715;

																						{	/* Ast/unit.scm 851 */
																							obj_t BgL_arg3299z00_3727;

																							BgL_arg3299z00_3727 =
																								(((BgL_variablez00_bglt)
																									COBJECT((
																											(BgL_variablez00_bglt) (
																												(BgL_localz00_bglt)
																												CAR(((obj_t)
																														BgL_localsz00_3675))))))->
																								BgL_idz00);
																							BgL_head1355z00_3715 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg3299z00_3727, BNIL);
																						}
																						{	/* Ast/unit.scm 851 */
																							obj_t BgL_g1358z00_3716;

																							BgL_g1358z00_3716 =
																								CDR(
																								((obj_t) BgL_localsz00_3675));
																							{
																								obj_t BgL_l1353z00_3718;
																								obj_t BgL_tail1356z00_3719;

																								BgL_l1353z00_3718 =
																									BgL_g1358z00_3716;
																								BgL_tail1356z00_3719 =
																									BgL_head1355z00_3715;
																							BgL_zc3z04anonymousza33292ze3z87_3720:
																								if (NULLP
																									(BgL_l1353z00_3718))
																									{	/* Ast/unit.scm 851 */
																										BgL_arg3282z00_3708 =
																											BgL_head1355z00_3715;
																									}
																								else
																									{	/* Ast/unit.scm 851 */
																										obj_t
																											BgL_newtail1357z00_3722;
																										{	/* Ast/unit.scm 851 */
																											obj_t BgL_arg3295z00_3724;

																											BgL_arg3295z00_3724 =
																												(((BgL_variablez00_bglt)
																													COBJECT((
																															(BgL_variablez00_bglt)
																															((BgL_localz00_bglt) CAR(((obj_t) BgL_l1353z00_3718))))))->BgL_idz00);
																											BgL_newtail1357z00_3722 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg3295z00_3724,
																												BNIL);
																										}
																										SET_CDR
																											(BgL_tail1356z00_3719,
																											BgL_newtail1357z00_3722);
																										{	/* Ast/unit.scm 851 */
																											obj_t BgL_arg3294z00_3723;

																											BgL_arg3294z00_3723 =
																												CDR(
																												((obj_t)
																													BgL_l1353z00_3718));
																											{
																												obj_t
																													BgL_tail1356z00_9191;
																												obj_t BgL_l1353z00_9190;

																												BgL_l1353z00_9190 =
																													BgL_arg3294z00_3723;
																												BgL_tail1356z00_9191 =
																													BgL_newtail1357z00_3722;
																												BgL_tail1356z00_3719 =
																													BgL_tail1356z00_9191;
																												BgL_l1353z00_3718 =
																													BgL_l1353z00_9190;
																												goto
																													BgL_zc3z04anonymousza33292ze3z87_3720;
																											}
																										}
																									}
																							}
																						}
																					}
																			}
																		else
																			{	/* Ast/unit.scm 849 */
																				BgL_arg3282z00_3708 =
																					BGl_typedzd2argsze70z35zzast_unitz00
																					(BgL_argsz00_129,
																					BgL_genericz00_3680);
																			}
																	}
																	{	/* Ast/unit.scm 856 */
																		obj_t BgL_arg3305z00_3731;

																		if (PAIRP(BgL_bodyz00_130))
																			{	/* Ast/unit.scm 856 */
																				BgL_arg3305z00_3731 = BgL_bodyz00_130;
																			}
																		else
																			{	/* Ast/unit.scm 858 */
																				obj_t BgL_arg3307z00_3733;

																				{	/* Ast/unit.scm 858 */
																					obj_t BgL_arg3308z00_3734;
																					obj_t BgL_arg3309z00_3735;

																					{	/* Ast/unit.scm 858 */
																						obj_t BgL_arg3310z00_3736;

																						{	/* Ast/unit.scm 858 */
																							obj_t BgL_arg3311z00_3737;

																							BgL_arg3311z00_3737 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(27), BNIL);
																							BgL_arg3310z00_3736 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(28), BgL_arg3311z00_3737);
																						}
																						BgL_arg3308z00_3734 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
																							BgL_arg3310z00_3736);
																					}
																					{	/* Ast/unit.scm 859 */
																						obj_t BgL_arg3312z00_3738;
																						obj_t BgL_arg3313z00_3739;

																						{	/* Ast/unit.scm 859 */
																							obj_t BgL_arg3314z00_3740;

																							BgL_arg3314z00_3740 =
																								MAKE_YOUNG_PAIR
																								(BgL_namez00_3677, BNIL);
																							BgL_arg3312z00_3738 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(29), BgL_arg3314z00_3740);
																						}
																						{	/* Ast/unit.scm 860 */
																							obj_t BgL_arg3315z00_3741;

																							BgL_arg3315z00_3741 =
																								MAKE_YOUNG_PAIR
																								(BGl_idzd2ofzd2idz00zzast_identz00
																								(CAR(BgL_argsz00_129),
																									BGl_findzd2locationzd2zztools_locationz00
																									(BgL_srcz00_131)), BNIL);
																							BgL_arg3313z00_3739 =
																								MAKE_YOUNG_PAIR
																								(BGl_string3575z00zzast_unitz00,
																								BgL_arg3315z00_3741);
																						}
																						BgL_arg3309z00_3735 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg3312z00_3738,
																							BgL_arg3313z00_3739);
																					}
																					BgL_arg3307z00_3733 =
																						MAKE_YOUNG_PAIR(BgL_arg3308z00_3734,
																						BgL_arg3309z00_3735);
																				}
																				BgL_arg3305z00_3731 =
																					MAKE_YOUNG_PAIR(BgL_arg3307z00_3733,
																					BNIL);
																			}
																		BgL_arg3287z00_3709 =
																			BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																			(BgL_arg3305z00_3731, BNIL);
																	}
																	BgL_arg3281z00_3707 =
																		MAKE_YOUNG_PAIR(BgL_arg3282z00_3708,
																		BgL_arg3287z00_3709);
																}
																BgL_arg3276z00_3706 =
																	MAKE_YOUNG_PAIR(BgL_namez00_3677,
																	BgL_arg3281z00_3707);
															}
															BgL_arg3273z00_3704 =
																MAKE_YOUNG_PAIR(BgL_arg3276z00_3706, BNIL);
														}
														BgL_arg3275z00_3705 =
															MAKE_YOUNG_PAIR(BgL_namez00_3677, BNIL);
														BgL_arg3272z00_3703 =
															MAKE_YOUNG_PAIR(BgL_arg3273z00_3704,
															BgL_arg3275z00_3705);
													}
													BgL_defz00_3681 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
														BgL_arg3272z00_3703);
												}
												{	/* Ast/unit.scm 849 */

													BGl_markzd2methodz12zc0zzobject_methodz00
														(BgL_namez00_3677);
													{	/* Ast/unit.scm 869 */
														obj_t BgL_ozd2unitzd2_3682;

														BgL_ozd2unitzd2_3682 =
															BGl_getzd2genericzd2unitz00zzmodule_classz00();
														{	/* Ast/unit.scm 869 */
															BgL_typez00_bglt BgL_typez00_3683;

															BgL_typez00_3683 =
																(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			((BgL_localz00_bglt)
																				CAR(
																					((obj_t) BgL_localsz00_3675))))))->
																BgL_typez00);
															{	/* Ast/unit.scm 870 */
																obj_t BgL_genz00_3684;

																{	/* Ast/unit.scm 872 */
																	obj_t BgL_arg3251z00_3690;

																	{	/* Ast/unit.scm 872 */
																		obj_t BgL_arg3252z00_3691;
																		obj_t BgL_arg3254z00_3692;

																		{	/* Ast/unit.scm 872 */
																			obj_t BgL_arg3255z00_3693;

																			{	/* Ast/unit.scm 872 */
																				obj_t BgL_arg3256z00_3694;
																				obj_t BgL_arg3259z00_3695;

																				BgL_arg3256z00_3694 =
																					(((BgL_variablez00_bglt) COBJECT(
																							((BgL_variablez00_bglt)
																								BgL_genericz00_3680)))->
																					BgL_idz00);
																				BgL_arg3259z00_3695 =
																					MAKE_YOUNG_PAIR(BgL_modulez00_128,
																					BNIL);
																				BgL_arg3255z00_3693 =
																					MAKE_YOUNG_PAIR(BgL_arg3256z00_3694,
																					BgL_arg3259z00_3695);
																			}
																			BgL_arg3252z00_3691 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
																				BgL_arg3255z00_3693);
																		}
																		{	/* Ast/unit.scm 873 */
																			obj_t BgL_arg3263z00_3696;
																			obj_t BgL_arg3264z00_3697;

																			BgL_arg3263z00_3696 =
																				BGl_epairifyzd2propagatezd2locz00zztools_miscz00
																				(BgL_defz00_3681, BgL_locz00_3674);
																			{	/* Ast/unit.scm 874 */
																				obj_t BgL_arg3266z00_3698;
																				obj_t BgL_arg3267z00_3699;

																				{	/* Ast/unit.scm 874 */
																					bool_t BgL_test3948z00_9234;

																					{	/* Ast/unit.scm 874 */
																						obj_t BgL_classz00_5615;

																						BgL_classz00_5615 =
																							BGl_tclassz00zzobject_classz00;
																						{	/* Ast/unit.scm 874 */
																							BgL_objectz00_bglt
																								BgL_arg1807z00_5617;
																							{	/* Ast/unit.scm 874 */
																								obj_t BgL_tmpz00_9235;

																								BgL_tmpz00_9235 =
																									((obj_t)
																									((BgL_objectz00_bglt)
																										BgL_typez00_3683));
																								BgL_arg1807z00_5617 =
																									(BgL_objectz00_bglt)
																									(BgL_tmpz00_9235);
																							}
																							if (BGL_CONDEXPAND_ISA_ARCH64())
																								{	/* Ast/unit.scm 874 */
																									long BgL_idxz00_5623;

																									BgL_idxz00_5623 =
																										BGL_OBJECT_INHERITANCE_NUM
																										(BgL_arg1807z00_5617);
																									BgL_test3948z00_9234 =
																										(VECTOR_REF
																										(BGl_za2inheritancesza2z00zz__objectz00,
																											(BgL_idxz00_5623 + 2L)) ==
																										BgL_classz00_5615);
																								}
																							else
																								{	/* Ast/unit.scm 874 */
																									bool_t BgL_res3546z00_5648;

																									{	/* Ast/unit.scm 874 */
																										obj_t BgL_oclassz00_5631;

																										{	/* Ast/unit.scm 874 */
																											obj_t BgL_arg1815z00_5639;
																											long BgL_arg1816z00_5640;

																											BgL_arg1815z00_5639 =
																												(BGl_za2classesza2z00zz__objectz00);
																											{	/* Ast/unit.scm 874 */
																												long
																													BgL_arg1817z00_5641;
																												BgL_arg1817z00_5641 =
																													BGL_OBJECT_CLASS_NUM
																													(BgL_arg1807z00_5617);
																												BgL_arg1816z00_5640 =
																													(BgL_arg1817z00_5641 -
																													OBJECT_TYPE);
																											}
																											BgL_oclassz00_5631 =
																												VECTOR_REF
																												(BgL_arg1815z00_5639,
																												BgL_arg1816z00_5640);
																										}
																										{	/* Ast/unit.scm 874 */
																											bool_t
																												BgL__ortest_1115z00_5632;
																											BgL__ortest_1115z00_5632 =
																												(BgL_classz00_5615 ==
																												BgL_oclassz00_5631);
																											if (BgL__ortest_1115z00_5632)
																												{	/* Ast/unit.scm 874 */
																													BgL_res3546z00_5648 =
																														BgL__ortest_1115z00_5632;
																												}
																											else
																												{	/* Ast/unit.scm 874 */
																													long
																														BgL_odepthz00_5633;
																													{	/* Ast/unit.scm 874 */
																														obj_t
																															BgL_arg1804z00_5634;
																														BgL_arg1804z00_5634
																															=
																															(BgL_oclassz00_5631);
																														BgL_odepthz00_5633 =
																															BGL_CLASS_DEPTH
																															(BgL_arg1804z00_5634);
																													}
																													if (
																														(2L <
																															BgL_odepthz00_5633))
																														{	/* Ast/unit.scm 874 */
																															obj_t
																																BgL_arg1802z00_5636;
																															{	/* Ast/unit.scm 874 */
																																obj_t
																																	BgL_arg1803z00_5637;
																																BgL_arg1803z00_5637
																																	=
																																	(BgL_oclassz00_5631);
																																BgL_arg1802z00_5636
																																	=
																																	BGL_CLASS_ANCESTORS_REF
																																	(BgL_arg1803z00_5637,
																																	2L);
																															}
																															BgL_res3546z00_5648
																																=
																																(BgL_arg1802z00_5636
																																==
																																BgL_classz00_5615);
																														}
																													else
																														{	/* Ast/unit.scm 874 */
																															BgL_res3546z00_5648
																																= ((bool_t) 0);
																														}
																												}
																										}
																									}
																									BgL_test3948z00_9234 =
																										BgL_res3546z00_5648;
																								}
																						}
																					}
																					if (BgL_test3948z00_9234)
																						{
																							BgL_globalz00_bglt
																								BgL_auxz00_9258;
																							{
																								BgL_tclassz00_bglt
																									BgL_auxz00_9259;
																								{
																									obj_t BgL_auxz00_9260;

																									{	/* Ast/unit.scm 874 */
																										BgL_objectz00_bglt
																											BgL_tmpz00_9261;
																										BgL_tmpz00_9261 =
																											((BgL_objectz00_bglt) (
																												(BgL_typez00_bglt)
																												BgL_typez00_3683));
																										BgL_auxz00_9260 =
																											BGL_OBJECT_WIDENING
																											(BgL_tmpz00_9261);
																									}
																									BgL_auxz00_9259 =
																										((BgL_tclassz00_bglt)
																										BgL_auxz00_9260);
																								}
																								BgL_auxz00_9258 =
																									(((BgL_tclassz00_bglt)
																										COBJECT(BgL_auxz00_9259))->
																									BgL_holderz00);
																							}
																							BgL_arg3266z00_3698 =
																								((obj_t) BgL_auxz00_9258);
																						}
																					else
																						{	/* Ast/unit.scm 874 */
																							BgL_arg3266z00_3698 = BFALSE;
																						}
																				}
																				{	/* Ast/unit.scm 875 */
																					obj_t BgL_arg3269z00_3701;

																					{	/* Ast/unit.scm 875 */
																						obj_t BgL_arg1455z00_5652;

																						BgL_arg1455z00_5652 =
																							SYMBOL_TO_STRING
																							(BgL_namez00_3677);
																						BgL_arg3269z00_3701 =
																							BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																							(BgL_arg1455z00_5652);
																					}
																					BgL_arg3267z00_3699 =
																						MAKE_YOUNG_PAIR(BgL_arg3269z00_3701,
																						BNIL);
																				}
																				BgL_arg3264z00_3697 =
																					MAKE_YOUNG_PAIR(BgL_arg3266z00_3698,
																					BgL_arg3267z00_3699);
																			}
																			BgL_arg3254z00_3692 =
																				MAKE_YOUNG_PAIR(BgL_arg3263z00_3696,
																				BgL_arg3264z00_3697);
																		}
																		BgL_arg3251z00_3690 =
																			MAKE_YOUNG_PAIR(BgL_arg3252z00_3691,
																			BgL_arg3254z00_3692);
																	}
																	BgL_genz00_3684 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(55),
																		BgL_arg3251z00_3690);
																}
																{	/* Ast/unit.scm 871 */
																	obj_t BgL_sexpza2za2_3685;

																	{	/* Ast/unit.scm 876 */
																		obj_t BgL_list3249z00_3688;

																		{	/* Ast/unit.scm 876 */
																			obj_t BgL_arg3250z00_3689;

																			BgL_arg3250z00_3689 =
																				MAKE_YOUNG_PAIR(BgL_genz00_3684, BNIL);
																			BgL_list3249z00_3688 =
																				MAKE_YOUNG_PAIR(
																				((obj_t) BgL_genericz00_3680),
																				BgL_arg3250z00_3689);
																		}
																		BgL_sexpza2za2_3685 = BgL_list3249z00_3688;
																	}
																	{	/* Ast/unit.scm 876 */

																		{	/* Ast/unit.scm 877 */
																			bool_t BgL_test3952z00_9279;

																			if (STRUCTP(BgL_ozd2unitzd2_3682))
																				{	/* Ast/unit.scm 877 */
																					BgL_test3952z00_9279 =
																						(STRUCT_KEY(BgL_ozd2unitzd2_3682) ==
																						CNST_TABLE_REF(56));
																				}
																			else
																				{	/* Ast/unit.scm 877 */
																					BgL_test3952z00_9279 = ((bool_t) 0);
																				}
																			if (BgL_test3952z00_9279)
																				{	/* Ast/unit.scm 877 */
																					BGl_unitzd2sexpza2zd2addz12zb0zzast_unitz00
																						(BgL_ozd2unitzd2_3682,
																						BgL_sexpza2za2_3685);
																					{	/* Ast/unit.scm 881 */
																						obj_t BgL_list3248z00_3687;

																						BgL_list3248z00_3687 =
																							MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
																						return BgL_list3248z00_3687;
																					}
																				}
																			else
																				{	/* Ast/unit.scm 877 */
																					return BgL_sexpza2za2_3685;
																				}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				else
					{	/* Ast/unit.scm 833 */
						BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00
							(BGl_string3576z00zzast_unitz00, BgL_srcz00_131,
							BGl_findzd2locationzd2zztools_locationz00(BgL_srcz00_131));
						{	/* Ast/unit.scm 837 */
							obj_t BgL_list3339z00_3753;

							BgL_list3339z00_3753 = MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
							return BgL_list3339z00_3753;
						}
					}
			}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zzast_unitz00(obj_t BgL_argsz00_3769,
		obj_t BgL_formalsz00_3770)
	{
		{	/* Ast/unit.scm 818 */
			{
				obj_t BgL_idz00_3756;

				if (PAIRP(BgL_argsz00_3769))
					{	/* Ast/unit.scm 822 */
						BgL_localz00_bglt BgL_i1132z00_3773;

						BgL_i1132z00_3773 =
							((BgL_localz00_bglt) CAR(((obj_t) BgL_formalsz00_3770)));
						if (BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(CAR
								(BgL_argsz00_3769)))
							{	/* Ast/unit.scm 824 */
								return BgL_argsz00_3769;
							}
						else
							{	/* Ast/unit.scm 826 */
								bool_t BgL_test3956z00_9298;

								{	/* Ast/unit.scm 826 */
									obj_t BgL_arg3383z00_3789;

									BgL_arg3383z00_3789 = CAR(BgL_argsz00_3769);
									{	/* Ast/unit.scm 826 */
										obj_t BgL_tmpz00_9300;

										BgL_idz00_3756 = BgL_arg3383z00_3789;
										{	/* Ast/unit.scm 813 */
											obj_t BgL_arg3352z00_3758;

											BgL_arg3352z00_3758 =
												SYMBOL_TO_STRING(((obj_t) BgL_idz00_3756));
											{	/* Ast/unit.scm 813 */

												BgL_tmpz00_9300 =
													BGl_stringzd2containszd2zz__r4_strings_6_7z00
													(BgL_arg3352z00_3758, BGl_string3577z00zzast_unitz00,
													(int) (0L));
										}}
										BgL_test3956z00_9298 = CBOOL(BgL_tmpz00_9300);
								}}
								if (BgL_test3956z00_9298)
									{	/* Ast/unit.scm 827 */
										obj_t BgL_arg3364z00_3778;
										obj_t BgL_arg3367z00_3779;

										BgL_arg3364z00_3778 = CAR(BgL_argsz00_3769);
										{	/* Ast/unit.scm 828 */
											obj_t BgL_arg3369z00_3780;
											obj_t BgL_arg3370z00_3781;

											BgL_arg3369z00_3780 = CDR(BgL_argsz00_3769);
											BgL_arg3370z00_3781 = CDR(((obj_t) BgL_formalsz00_3770));
											BgL_arg3367z00_3779 =
												BGl_loopze70ze7zzast_unitz00(BgL_arg3369z00_3780,
												BgL_arg3370z00_3781);
										}
										return
											MAKE_YOUNG_PAIR(BgL_arg3364z00_3778, BgL_arg3367z00_3779);
									}
								else
									{	/* Ast/unit.scm 830 */
										obj_t BgL_arg3371z00_3782;
										obj_t BgL_arg3372z00_3783;

										{	/* Ast/unit.scm 830 */
											obj_t BgL_arg3373z00_3784;
											obj_t BgL_arg3375z00_3785;

											BgL_arg3373z00_3784 = CAR(BgL_argsz00_3769);
											BgL_arg3375z00_3785 =
												(((BgL_typez00_bglt) COBJECT(
														(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt) BgL_i1132z00_3773)))->
															BgL_typez00)))->BgL_idz00);
											BgL_arg3371z00_3782 =
												BGl_makezd2typedzd2identz00zzast_identz00
												(BgL_arg3373z00_3784, BgL_arg3375z00_3785);
										}
										{	/* Ast/unit.scm 831 */
											obj_t BgL_arg3381z00_3787;
											obj_t BgL_arg3382z00_3788;

											BgL_arg3381z00_3787 = CDR(BgL_argsz00_3769);
											BgL_arg3382z00_3788 = CDR(((obj_t) BgL_formalsz00_3770));
											BgL_arg3372z00_3783 =
												BGl_loopze70ze7zzast_unitz00(BgL_arg3381z00_3787,
												BgL_arg3382z00_3788);
										}
										return
											MAKE_YOUNG_PAIR(BgL_arg3371z00_3782, BgL_arg3372z00_3783);
									}
							}
					}
				else
					{	/* Ast/unit.scm 820 */
						return BgL_argsz00_3769;
					}
			}
		}

	}



/* typed-args~0 */
	obj_t BGl_typedzd2argsze70z35zzast_unitz00(obj_t BgL_argsz00_3762,
		BgL_globalz00_bglt BgL_genericz00_3763)
	{
		{	/* Ast/unit.scm 831 */
			{	/* Ast/unit.scm 817 */
				BgL_sfunz00_bglt BgL_i1131z00_3766;

				BgL_i1131z00_3766 =
					((BgL_sfunz00_bglt)
					(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_genericz00_3763)))->BgL_valuez00));
				return
					BGl_loopze70ze7zzast_unitz00(BgL_argsz00_3762,
					(((BgL_sfunz00_bglt) COBJECT(BgL_i1131z00_3766))->BgL_argsz00));
			}
		}

	}



/* make-method-definition */
	obj_t BGl_makezd2methodzd2definitionz00zzast_unitz00(obj_t BgL_idz00_132,
		obj_t BgL_argsz00_133, obj_t BgL_bodyz00_134, obj_t BgL_srcz00_135)
	{
		{	/* Ast/unit.scm 886 */
			{	/* Ast/unit.scm 887 */
				bool_t BgL_test3957z00_9327;

				if (PAIRP(BgL_argsz00_133))
					{	/* Ast/unit.scm 887 */
						obj_t BgL_tmpz00_9330;

						BgL_tmpz00_9330 = CAR(BgL_argsz00_133);
						BgL_test3957z00_9327 = SYMBOLP(BgL_tmpz00_9330);
					}
				else
					{	/* Ast/unit.scm 887 */
						BgL_test3957z00_9327 = ((bool_t) 0);
					}
				if (BgL_test3957z00_9327)
					{	/* Ast/unit.scm 892 */
						obj_t BgL_locz00_3797;

						BgL_locz00_3797 =
							BGl_findzd2locationzd2zztools_locationz00(BgL_srcz00_135);
						{	/* Ast/unit.scm 892 */
							obj_t BgL_localsz00_3798;

							BgL_localsz00_3798 =
								BGl_parsezd2funzd2argsz00zzast_unitz00(BgL_argsz00_133,
								BgL_srcz00_135, BgL_locz00_3797);
							{	/* Ast/unit.scm 893 */

								if (BGl_checkzd2methodzd2definitionz00zzast_glozd2defzd2
									(BgL_idz00_132, BgL_argsz00_133, BgL_localsz00_3798,
										BgL_srcz00_135))
									{	/* Ast/unit.scm 896 */
										obj_t BgL_ozd2unitzd2_3800;

										BgL_ozd2unitzd2_3800 =
											BGl_getzd2methodzd2unitz00zzmodule_classz00();
										{	/* Ast/unit.scm 896 */
											obj_t BgL_sexpza2za2_3801;

											BgL_sexpza2za2_3801 =
												BGl_makezd2methodzd2bodyz00zzast_unitz00(BgL_idz00_132,
												BgL_argsz00_133, BgL_localsz00_3798, BgL_bodyz00_134,
												BgL_srcz00_135, BgL_locz00_3797);
											{	/* Ast/unit.scm 897 */

												{	/* Ast/unit.scm 898 */
													bool_t BgL_test3960z00_9339;

													if (STRUCTP(BgL_ozd2unitzd2_3800))
														{	/* Ast/unit.scm 898 */
															BgL_test3960z00_9339 =
																(STRUCT_KEY(BgL_ozd2unitzd2_3800) ==
																CNST_TABLE_REF(56));
														}
													else
														{	/* Ast/unit.scm 898 */
															BgL_test3960z00_9339 = ((bool_t) 0);
														}
													if (BgL_test3960z00_9339)
														{	/* Ast/unit.scm 898 */
															BGl_unitzd2sexpza2zd2addz12zb0zzast_unitz00
																(BgL_ozd2unitzd2_3800, BgL_sexpza2za2_3801);
															{	/* Ast/unit.scm 902 */
																obj_t BgL_list3390z00_3803;

																BgL_list3390z00_3803 =
																	MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
																return BgL_list3390z00_3803;
															}
														}
													else
														{	/* Ast/unit.scm 898 */
															return BgL_sexpza2za2_3801;
														}
												}
											}
										}
									}
								else
									{	/* Ast/unit.scm 895 */
										obj_t BgL_list3391z00_3804;

										BgL_list3391z00_3804 = MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
										return BgL_list3391z00_3804;
									}
							}
						}
					}
				else
					{	/* Ast/unit.scm 887 */
						BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00
							(BGl_string3578z00zzast_unitz00, BgL_srcz00_135,
							BGl_findzd2locationzd2zztools_locationz00(BgL_srcz00_135));
						{	/* Ast/unit.scm 891 */
							obj_t BgL_list3394z00_3806;

							BgL_list3394z00_3806 = MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
							return BgL_list3394z00_3806;
						}
					}
			}
		}

	}



/* make-method-body */
	obj_t BGl_makezd2methodzd2bodyz00zzast_unitz00(obj_t BgL_identz00_136,
		obj_t BgL_argsz00_137, obj_t BgL_localsz00_138, obj_t BgL_bodyz00_139,
		obj_t BgL_srcz00_140, obj_t BgL_locz00_141)
	{
		{	/* Ast/unit.scm 908 */
			if (BGl_dssslzd2prototypezf3z21zztools_dssslz00(BgL_argsz00_137))
				{	/* Ast/unit.scm 910 */
					if (BGl_dssslzd2optionalzd2onlyzd2prototypezf3z21zztools_dssslz00
						(BgL_argsz00_137))
						{	/* Ast/unit.scm 913 */
							obj_t BgL_localsz00_3811;

							BgL_localsz00_3811 =
								BGl_parsezd2funzd2optzd2argszd2zzast_unitz00(BgL_argsz00_137,
								BgL_srcz00_140, BgL_locz00_141);
							{	/* Ast/unit.scm 914 */
								obj_t BgL_arg3398z00_3812;

								if (NULLP(BgL_localsz00_3811))
									{	/* Ast/unit.scm 914 */
										BgL_arg3398z00_3812 = BNIL;
									}
								else
									{	/* Ast/unit.scm 914 */
										obj_t BgL_head1361z00_3815;

										{	/* Ast/unit.scm 914 */
											obj_t BgL_arg3405z00_3827;

											BgL_arg3405z00_3827 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_localz00_bglt)
																CAR(
																	((obj_t) BgL_localsz00_3811))))))->BgL_idz00);
											BgL_head1361z00_3815 =
												MAKE_YOUNG_PAIR(BgL_arg3405z00_3827, BNIL);
										}
										{	/* Ast/unit.scm 914 */
											obj_t BgL_g1364z00_3816;

											BgL_g1364z00_3816 = CDR(((obj_t) BgL_localsz00_3811));
											{
												obj_t BgL_l1359z00_3818;
												obj_t BgL_tail1362z00_3819;

												BgL_l1359z00_3818 = BgL_g1364z00_3816;
												BgL_tail1362z00_3819 = BgL_head1361z00_3815;
											BgL_zc3z04anonymousza33400ze3z87_3820:
												if (NULLP(BgL_l1359z00_3818))
													{	/* Ast/unit.scm 914 */
														BgL_arg3398z00_3812 = BgL_head1361z00_3815;
													}
												else
													{	/* Ast/unit.scm 914 */
														obj_t BgL_newtail1363z00_3822;

														{	/* Ast/unit.scm 914 */
															obj_t BgL_arg3403z00_3824;

															BgL_arg3403z00_3824 =
																(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			((BgL_localz00_bglt)
																				CAR(
																					((obj_t) BgL_l1359z00_3818))))))->
																BgL_idz00);
															BgL_newtail1363z00_3822 =
																MAKE_YOUNG_PAIR(BgL_arg3403z00_3824, BNIL);
														}
														SET_CDR(BgL_tail1362z00_3819,
															BgL_newtail1363z00_3822);
														{	/* Ast/unit.scm 914 */
															obj_t BgL_arg3402z00_3823;

															BgL_arg3402z00_3823 =
																CDR(((obj_t) BgL_l1359z00_3818));
															{
																obj_t BgL_tail1362z00_9378;
																obj_t BgL_l1359z00_9377;

																BgL_l1359z00_9377 = BgL_arg3402z00_3823;
																BgL_tail1362z00_9378 = BgL_newtail1363z00_3822;
																BgL_tail1362z00_3819 = BgL_tail1362z00_9378;
																BgL_l1359z00_3818 = BgL_l1359z00_9377;
																goto BgL_zc3z04anonymousza33400ze3z87_3820;
															}
														}
													}
											}
										}
									}
								return
									BGl_makezd2methodzd2nozd2dssslzd2bodyz00zzobject_methodz00
									(BgL_identz00_136, BgL_arg3398z00_3812, BgL_localsz00_3811,
									BgL_bodyz00_139, BgL_srcz00_140);
							}
						}
					else
						{	/* Ast/unit.scm 912 */
							return
								BGl_makezd2methodzd2dssslzd2bodyzd2zzobject_methodz00
								(BgL_identz00_136, BgL_argsz00_137, BgL_localsz00_138,
								BgL_bodyz00_139, BgL_srcz00_140);
						}
				}
			else
				{	/* Ast/unit.scm 910 */
					return
						BGl_makezd2methodzd2nozd2dssslzd2bodyz00zzobject_methodz00
						(BgL_identz00_136, BgL_argsz00_137, BgL_localsz00_138,
						BgL_bodyz00_139, BgL_srcz00_140);
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_unitz00(void)
	{
		{	/* Ast/unit.scm 21 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_unitz00(void)
	{
		{	/* Ast/unit.scm 21 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_unitz00(void)
	{
		{	/* Ast/unit.scm 21 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_unitz00(void)
	{
		{	/* Ast/unit.scm 21 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zzast_findzd2gdefszd2(502577483L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zzast_glozd2defzd2(44601789L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zzast_letz00(469204197L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zzobject_genericz00(3606241L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zzobject_methodz00(493120707L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zztools_prognz00(301998274L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zztools_argsz00(47102372L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zztools_dssslz00(275867955L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zzmodule_classz00(153808441L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zzmodule_includez00(184800657L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zzmodule_impusez00(478324304L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
			return
				BGl_modulezd2initializa7ationz75zzexpand_epsz00(359337187L,
				BSTRING_TO_STRING(BGl_string3579z00zzast_unitz00));
		}

	}

#ifdef __cplusplus
}
#endif
