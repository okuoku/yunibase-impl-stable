/*===========================================================================*/
/*   (Ast/venv.scm)                                                          */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/venv.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_AST_ENV_TYPE_DEFINITIONS
#define BGL_AST_ENV_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_isfunz00_bgl
	{
		struct BgL_nodez00_bgl *BgL_originalzd2bodyzd2;
		obj_t BgL_recursivezd2callszd2;
		bool_t BgL_tailrecz00;
	}               *BgL_isfunz00_bglt;


#endif													// BGL_AST_ENV_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62globalzd2bucketzd2positionz62zzast_envz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_setzd2genvz12zc0zzast_envz00(obj_t);
	extern obj_t BGl_modulezd2ze3qualifiedzd2typeze3zzread_jvmz00(obj_t);
	BGL_IMPORT int BGl_bigloozd2warningzd2zz__paramz00(void);
	static obj_t BGl_requirezd2initializa7ationz75zzast_envz00 = BUNSPEC;
	extern obj_t BGl_userzd2errorzf2locationz20zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_additionalzd2heapzd2restorezd2globalsz12zc0zzast_envz00(void);
	extern obj_t BGl_funz00zzast_varz00;
	BGL_EXPORTED_DECL obj_t BGl_getzd2globalzf2modulez20zzast_envz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31694ze3ze5zzast_envz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_za2warningzd2overridenzd2variablesza2z00zzengine_paramz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_alreadyzd2restoredzf3z21zzast_envz00(obj_t);
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t BGl_za2Genvza2z00zzast_envz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zzast_envz00(void);
	static obj_t BGl_z62addzd2genvz12za2zzast_envz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_forzd2eachzd2globalz12z12zzast_envz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_bindzd2globalz12zc0zzast_envz00(obj_t, obj_t, obj_t, BgL_valuez00_bglt,
		obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzast_envz00(void);
	static obj_t BGl_objectzd2initzd2zzast_envz00(void);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_typez00zztype_typez00;
	BGL_EXPORTED_DECL obj_t BGl_addzd2genvz12zc0zzast_envz00(obj_t);
	static obj_t
		BGl_z62additionalzd2heapzd2restorezd2globalsz12za2zzast_envz00(obj_t);
	extern obj_t BGl_za2libzd2modeza2zd2zzengine_paramz00;
	extern obj_t BGl_userzd2warningzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	static obj_t BGl_z62findzd2globalzf2modulez42zzast_envz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_z62unbindzd2globalz12za2zzast_envz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzast_envz00(void);
	static obj_t BGl_z62forzd2eachzd2globalz12z70zzast_envz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_initializa7ezd2Genvz12z67zzast_envz00(void);
	static obj_t BGl_z62restorezd2valuezd2typesz12z70zzast_envz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	static obj_t BGl_z62restorezd2valuezd2typesz121248z70zzast_envz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_restorezd2valuezd2typesz12z12zzast_envz00(BgL_valuez00_bglt,
		obj_t);
	static obj_t BGl_z62restorezd2valuezd2typesz121251z70zzast_envz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62restorezd2valuezd2typesz121253z70zzast_envz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62restorezd2valuezd2typesz121255z70zzast_envz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62restorezd2valuezd2typesz121257z70zzast_envz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62getzd2globalzf2modulez42zzast_envz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_restorezd2globalz12zc0zzast_envz00(obj_t);
	static obj_t BGl_z62restorezd2globalz12za2zzast_envz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	static obj_t BGl_z62alreadyzd2restoredzf3z43zzast_envz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_unbindzd2globalz12zc0zzast_envz00(obj_t, obj_t);
	extern obj_t BGl_isfunz00zzinline_inlinez00;
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	extern obj_t BGl_hrtypezd2nodez12zc0zzast_hrtypez00(BgL_nodez00_bglt);
	static obj_t BGl_z62initializa7ezd2Genvz12z05zzast_envz00(obj_t);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	BGL_IMPORT obj_t BGl_makezd2hashtablezd2zz__hashz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinline_inlinez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_jvmz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_hrtypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	static obj_t BGl_z62getzd2genvzb0zzast_envz00(obj_t);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	extern obj_t BGl_cfunz00zzast_varz00;
	static BgL_globalz00_bglt BGl_z62bindzd2globalz12za2zzast_envz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_za2delayedzd2restoredzd2globalza2z00zzast_envz00 = BUNSPEC;
	static obj_t BGl_z62findzd2globalzb0zzast_envz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	static obj_t BGl_z62zc3z04anonymousza31285ze3ze5zzast_envz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zzast_envz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_envz00(void);
	BGL_EXPORTED_DECL obj_t BGl_globalzd2bucketzd2positionz00zzast_envz00(obj_t,
		obj_t);
	static obj_t BGl_errorzd2rebindzd2globalz12z12zzast_envz00(obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzast_envz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzast_envz00(void);
	BGL_IMPORT obj_t BGl_hashtablezd2forzd2eachz00zz__hashz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_findzd2globalzf2modulez20zzast_envz00(obj_t,
		obj_t);
	extern obj_t BGl_valuez00zzast_varz00;
	extern obj_t BGl_userzd2warningzf2locationz20zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t);
	extern BgL_typez00_bglt BGl_findzd2typezd2zztype_envz00(obj_t);
	BGL_IMPORT obj_t BGl_hashtablezd2putz12zc0zz__hashz00(obj_t, obj_t, obj_t);
	static obj_t BGl_za2restoredza2z00zzast_envz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_addzd2qualifiedzd2typez12z12zzread_jvmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_warningzd2overridezd2globalz12z12zzast_envz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62setzd2genvz12za2zzast_envz00(obj_t, obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_getzd2genvzd2zzast_envz00(void);
	BGL_IMPORT obj_t
		BGl_findzd2superzd2classzd2methodzd2zz__objectz00(BgL_objectz00_bglt, obj_t,
		obj_t);
	extern obj_t BGl_modulezd2initializa7ationzd2idza7zzmodule_modulez00(obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t BGl_warningzd2rebindzd2globalz12z12zzast_envz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_hashtablezd2getzd2zz__hashz00(obj_t, obj_t);
	static obj_t __cnst[8];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_addzd2genvz12zd2envz12zzast_envz00,
		BgL_bgl_za762addza7d2genvza7121933za7, BGl_z62addzd2genvz12za2zzast_envz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_findzd2globalzf2modulezd2envzf2zzast_envz00,
		BgL_bgl_za762findza7d2global1934z00,
		BGl_z62findzd2globalzf2modulez42zzast_envz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_setzd2genvz12zd2envz12zzast_envz00,
		BgL_bgl_za762setza7d2genvza7121935za7, BGl_z62setzd2genvz12za2zzast_envz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_findzd2globalzd2envz00zzast_envz00,
		BgL_bgl_za762findza7d2global1936z00, va_generic_entry,
		BGl_z62findzd2globalzb0zzast_envz00, BUNSPEC, -2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_initializa7ezd2Genvz12zd2envzb5zzast_envz00,
		BgL_bgl_za762initializa7a7eza71937za7,
		BGl_z62initializa7ezd2Genvz12z05zzast_envz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_unbindzd2globalz12zd2envz12zzast_envz00,
		BgL_bgl_za762unbindza7d2glob1938z00,
		BGl_z62unbindzd2globalz12za2zzast_envz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1910z00zzast_envz00,
		BgL_bgl_string1910za700za7za7a1939za7, "~a::~a", 6);
	      DEFINE_STRING(BGl_string1911z00zzast_envz00,
		BgL_bgl_string1911za700za7za7a1940za7, "Cannot find global variable", 27);
	      DEFINE_STRING(BGl_string1912z00zzast_envz00,
		BgL_bgl_string1912za700za7za7a1941za7, "Variable overridden by", 22);
	      DEFINE_STRING(BGl_string1913z00zzast_envz00,
		BgL_bgl_string1913za700za7za7a1942za7, "Duplicate definition", 20);
	      DEFINE_STRING(BGl_string1914z00zzast_envz00,
		BgL_bgl_string1914za700za7za7a1943za7, "eval", 4);
	      DEFINE_STRING(BGl_string1915z00zzast_envz00,
		BgL_bgl_string1915za700za7za7a1944za7, "unbind-global!", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1909z00zzast_envz00,
		BgL_bgl_za762za7c3za704anonymo1945za7,
		BGl_z62zc3z04anonymousza31285ze3ze5zzast_envz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1916z00zzast_envz00,
		BgL_bgl_string1916za700za7za7a1946za7, "Can't find global", 17);
	      DEFINE_STRING(BGl_string1918z00zzast_envz00,
		BgL_bgl_string1918za700za7za7a1947za7, "restore-value-types!1248", 24);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzd2bucketzd2positionzd2envzd2zzast_envz00,
		BgL_bgl_za762globalza7d2buck1948z00,
		BGl_z62globalzd2bucketzd2positionz62zzast_envz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1920z00zzast_envz00,
		BgL_bgl_string1920za700za7za7a1949za7, "restore-value-types!", 20);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1917z00zzast_envz00,
		BgL_bgl_za762restoreza7d2val1950z00,
		BGl_z62restorezd2valuezd2typesz121248z70zzast_envz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1924z00zzast_envz00,
		BgL_bgl_string1924za700za7za7a1951za7, "head-restore", 12);
	      DEFINE_STRING(BGl_string1925z00zzast_envz00,
		BgL_bgl_string1925za700za7za7a1952za7,
		"Illegal restored type for foreign function", 42);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1919z00zzast_envz00,
		BgL_bgl_za762restoreza7d2val1953z00,
		BGl_z62restorezd2valuezd2typesz121251z70zzast_envz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1926z00zzast_envz00,
		BgL_bgl_string1926za700za7za7a1954za7, "restore-value-types(sfun)", 25);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_getzd2globalzf2modulezd2envzf2zzast_envz00,
		BgL_bgl_za762getza7d2globalza71955za7,
		BGl_z62getzd2globalzf2modulez42zzast_envz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1927z00zzast_envz00,
		BgL_bgl_string1927za700za7za7a1956za7, "Illegal argument", 16);
	      DEFINE_STRING(BGl_string1928z00zzast_envz00,
		BgL_bgl_string1928za700za7za7a1957za7, "restore-value-types", 19);
	      DEFINE_STRING(BGl_string1929z00zzast_envz00,
		BgL_bgl_string1929za700za7za7a1958za7, "Illegal non pair argument", 25);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1921z00zzast_envz00,
		BgL_bgl_za762restoreza7d2val1959z00,
		BGl_z62restorezd2valuezd2typesz121253z70zzast_envz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1922z00zzast_envz00,
		BgL_bgl_za762restoreza7d2val1960z00,
		BGl_z62restorezd2valuezd2typesz121255z70zzast_envz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1923z00zzast_envz00,
		BgL_bgl_za762restoreza7d2val1961z00,
		BGl_z62restorezd2valuezd2typesz121257z70zzast_envz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1930z00zzast_envz00,
		BgL_bgl_string1930za700za7za7a1962za7, "ast_env", 7);
	      DEFINE_STRING(BGl_string1931z00zzast_envz00,
		BgL_bgl_string1931za700za7za7a1963za7,
		"_ eval now read @ get-global/module foreign the-global-environment ", 67);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_getzd2genvzd2envz00zzast_envz00,
		BgL_bgl_za762getza7d2genvza7b01964za7, BGl_z62getzd2genvzb0zzast_envz00, 0L,
		BUNSPEC, 0);
	     
		DEFINE_STATIC_BGL_GENERIC
		(BGl_restorezd2valuezd2typesz12zd2envzc0zzast_envz00,
		BgL_bgl_za762restoreza7d2val1965z00,
		BGl_z62restorezd2valuezd2typesz12z70zzast_envz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_forzd2eachzd2globalz12zd2envzc0zzast_envz00,
		BgL_bgl_za762forza7d2eachza7d21966za7, va_generic_entry,
		BGl_z62forzd2eachzd2globalz12z70zzast_envz00, BUNSPEC, -2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bindzd2globalz12zd2envz12zzast_envz00,
		BgL_bgl_za762bindza7d2global1967z00, BGl_z62bindzd2globalz12za2zzast_envz00,
		0L, BUNSPEC, 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_restorezd2globalz12zd2envz12zzast_envz00,
		BgL_bgl_za762restoreza7d2glo1968z00,
		BGl_z62restorezd2globalz12za2zzast_envz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_alreadyzd2restoredzf3zd2envzf3zzast_envz00,
		BgL_bgl_za762alreadyza7d2res1969z00,
		BGl_z62alreadyzd2restoredzf3z43zzast_envz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_additionalzd2heapzd2restorezd2globalsz12zd2envz12zzast_envz00,
		BgL_bgl_za762additionalza7d21970z00,
		BGl_z62additionalzd2heapzd2restorezd2globalsz12za2zzast_envz00, 0L, BUNSPEC,
		0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzast_envz00));
		     ADD_ROOT((void *) (&BGl_za2Genvza2z00zzast_envz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2delayedzd2restoredzd2globalza2z00zzast_envz00));
		     ADD_ROOT((void *) (&BGl_za2restoredza2z00zzast_envz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long
		BgL_checksumz00_2809, char *BgL_fromz00_2810)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_envz00))
				{
					BGl_requirezd2initializa7ationz75zzast_envz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_envz00();
					BGl_libraryzd2moduleszd2initz00zzast_envz00();
					BGl_cnstzd2initzd2zzast_envz00();
					BGl_importedzd2moduleszd2initz00zzast_envz00();
					BGl_genericzd2initzd2zzast_envz00();
					BGl_methodzd2initzd2zzast_envz00();
					return BGl_toplevelzd2initzd2zzast_envz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_envz00(void)
	{
		{	/* Ast/venv.scm 14 */
			BGl_modulezd2initializa7ationz75zz__paramz00(0L, "ast_env");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "ast_env");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "ast_env");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_env");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_env");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "ast_env");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "ast_env");
			BGl_modulezd2initializa7ationz75zz__hashz00(0L, "ast_env");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "ast_env");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "ast_env");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L, "ast_env");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "ast_env");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"ast_env");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_envz00(void)
	{
		{	/* Ast/venv.scm 14 */
			{	/* Ast/venv.scm 14 */
				obj_t BgL_cportz00_2676;

				{	/* Ast/venv.scm 14 */
					obj_t BgL_stringz00_2683;

					BgL_stringz00_2683 = BGl_string1931z00zzast_envz00;
					{	/* Ast/venv.scm 14 */
						obj_t BgL_startz00_2684;

						BgL_startz00_2684 = BINT(0L);
						{	/* Ast/venv.scm 14 */
							obj_t BgL_endz00_2685;

							BgL_endz00_2685 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2683)));
							{	/* Ast/venv.scm 14 */

								BgL_cportz00_2676 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2683, BgL_startz00_2684, BgL_endz00_2685);
				}}}}
				{
					long BgL_iz00_2677;

					BgL_iz00_2677 = 7L;
				BgL_loopz00_2678:
					if ((BgL_iz00_2677 == -1L))
						{	/* Ast/venv.scm 14 */
							return BUNSPEC;
						}
					else
						{	/* Ast/venv.scm 14 */
							{	/* Ast/venv.scm 14 */
								obj_t BgL_arg1932z00_2679;

								{	/* Ast/venv.scm 14 */

									{	/* Ast/venv.scm 14 */
										obj_t BgL_locationz00_2681;

										BgL_locationz00_2681 = BBOOL(((bool_t) 0));
										{	/* Ast/venv.scm 14 */

											BgL_arg1932z00_2679 =
												BGl_readz00zz__readerz00(BgL_cportz00_2676,
												BgL_locationz00_2681);
										}
									}
								}
								{	/* Ast/venv.scm 14 */
									int BgL_tmpz00_2843;

									BgL_tmpz00_2843 = (int) (BgL_iz00_2677);
									CNST_TABLE_SET(BgL_tmpz00_2843, BgL_arg1932z00_2679);
							}}
							{	/* Ast/venv.scm 14 */
								int BgL_auxz00_2682;

								BgL_auxz00_2682 = (int) ((BgL_iz00_2677 - 1L));
								{
									long BgL_iz00_2848;

									BgL_iz00_2848 = (long) (BgL_auxz00_2682);
									BgL_iz00_2677 = BgL_iz00_2848;
									goto BgL_loopz00_2678;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_envz00(void)
	{
		{	/* Ast/venv.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzast_envz00(void)
	{
		{	/* Ast/venv.scm 14 */
			BGl_za2Genvza2z00zzast_envz00 = CNST_TABLE_REF(0);
			BGl_za2delayedzd2restoredzd2globalza2z00zzast_envz00 = BNIL;
			BGl_za2restoredza2z00zzast_envz00 = BNIL;
			return BUNSPEC;
		}

	}



/* set-genv! */
	BGL_EXPORTED_DEF obj_t BGl_setzd2genvz12zc0zzast_envz00(obj_t BgL_genvz00_3)
	{
		{	/* Ast/venv.scm 55 */
			return (BGl_za2Genvza2z00zzast_envz00 = BgL_genvz00_3, BUNSPEC);
		}

	}



/* &set-genv! */
	obj_t BGl_z62setzd2genvz12za2zzast_envz00(obj_t BgL_envz00_2608,
		obj_t BgL_genvz00_2609)
	{
		{	/* Ast/venv.scm 55 */
			return BGl_setzd2genvz12zc0zzast_envz00(BgL_genvz00_2609);
		}

	}



/* add-genv! */
	BGL_EXPORTED_DEF obj_t BGl_addzd2genvz12zc0zzast_envz00(obj_t BgL_genvz00_4)
	{
		{	/* Ast/venv.scm 64 */
			BGl_hashtablezd2forzd2eachz00zz__hashz00(BgL_genvz00_4,
				BGl_proc1909z00zzast_envz00);
			return (BGl_za2restoredza2z00zzast_envz00 = BNIL, BUNSPEC);
		}

	}



/* &add-genv! */
	obj_t BGl_z62addzd2genvz12za2zzast_envz00(obj_t BgL_envz00_2611,
		obj_t BgL_genvz00_2612)
	{
		{	/* Ast/venv.scm 64 */
			return BGl_addzd2genvz12zc0zzast_envz00(BgL_genvz00_2612);
		}

	}



/* &<@anonymous:1285> */
	obj_t BGl_z62zc3z04anonymousza31285ze3ze5zzast_envz00(obj_t BgL_envz00_2613,
		obj_t BgL_kz00_2614, obj_t BgL_bucketz00_2615)
	{
		{	/* Ast/venv.scm 67 */
			{	/* Ast/venv.scm 68 */
				bool_t BgL_tmpz00_2855;

				{	/* Ast/venv.scm 68 */
					obj_t BgL_g1242z00_2687;

					BgL_g1242z00_2687 = CDR(((obj_t) BgL_bucketz00_2615));
					{
						obj_t BgL_l1240z00_2689;

						BgL_l1240z00_2689 = BgL_g1242z00_2687;
					BgL_zc3z04anonymousza31286ze3z87_2688:
						if (PAIRP(BgL_l1240z00_2689))
							{	/* Ast/venv.scm 88 */
								{	/* Ast/venv.scm 69 */
									obj_t BgL_newz00_2690;

									BgL_newz00_2690 = CAR(BgL_l1240z00_2689);
									BGl_za2delayedzd2restoredzd2globalza2z00zzast_envz00 =
										MAKE_YOUNG_PAIR(BgL_newz00_2690,
										BGl_za2delayedzd2restoredzd2globalza2z00zzast_envz00);
									{	/* Ast/venv.scm 70 */
										obj_t BgL_modulez00_2691;

										BgL_modulez00_2691 =
											(((BgL_globalz00_bglt) COBJECT(
													((BgL_globalz00_bglt) BgL_newz00_2690)))->
											BgL_modulez00);
										{	/* Ast/venv.scm 70 */
											obj_t BgL_idz00_2692;

											BgL_idz00_2692 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_globalz00_bglt) BgL_newz00_2690))))->
												BgL_idz00);
											{	/* Ast/venv.scm 71 */
												obj_t BgL_bucketz00_2693;

												BgL_bucketz00_2693 =
													BGl_hashtablezd2getzd2zz__hashz00
													(BGl_za2Genvza2z00zzast_envz00, BgL_idz00_2692);
												{	/* Ast/venv.scm 72 */

													if (PAIRP(BgL_bucketz00_2693))
														{	/* Ast/venv.scm 76 */
															bool_t BgL_test1975z00_2870;

															if (
																(BgL_modulez00_2691 ==
																	BGl_za2moduleza2z00zzmodule_modulez00))
																{	/* Ast/venv.scm 76 */
																	BgL_test1975z00_2870 = ((bool_t) 1);
																}
															else
																{	/* Ast/venv.scm 77 */
																	bool_t BgL_test1977z00_2873;

																	{	/* Ast/venv.scm 78 */
																		obj_t BgL_arg1317z00_2694;

																		{
																			BgL_globalz00_bglt BgL_auxz00_2874;

																			{	/* Ast/venv.scm 78 */
																				obj_t BgL_pairz00_2695;

																				BgL_pairz00_2695 =
																					CDR(BgL_bucketz00_2693);
																				BgL_auxz00_2874 =
																					((BgL_globalz00_bglt)
																					CAR(BgL_pairz00_2695));
																			}
																			BgL_arg1317z00_2694 =
																				(((BgL_globalz00_bglt)
																					COBJECT(BgL_auxz00_2874))->
																				BgL_modulez00);
																		}
																		BgL_test1977z00_2873 =
																			(BGl_za2moduleza2z00zzmodule_modulez00 ==
																			BgL_arg1317z00_2694);
																	}
																	if (BgL_test1977z00_2873)
																		{	/* Ast/venv.scm 77 */
																			BgL_test1975z00_2870 = ((bool_t) 0);
																		}
																	else
																		{	/* Ast/venv.scm 77 */
																			BgL_test1975z00_2870 = ((bool_t) 1);
																		}
																}
															if (BgL_test1975z00_2870)
																{	/* Ast/venv.scm 84 */
																	obj_t BgL_newzd2bucketzd2_2696;

																	BgL_newzd2bucketzd2_2696 =
																		MAKE_YOUNG_PAIR(BgL_newz00_2690,
																		CDR(BgL_bucketz00_2693));
																	SET_CDR(BgL_bucketz00_2693,
																		BgL_newzd2bucketzd2_2696);
																}
															else
																{	/* Ast/venv.scm 87 */
																	obj_t BgL_arg1314z00_2697;
																	obj_t BgL_arg1315z00_2698;

																	BgL_arg1314z00_2697 = CDR(BgL_bucketz00_2693);
																	BgL_arg1315z00_2698 =
																		MAKE_YOUNG_PAIR(BgL_newz00_2690,
																		CDR(CDR(BgL_bucketz00_2693)));
																	{	/* Ast/venv.scm 87 */
																		obj_t BgL_tmpz00_2887;

																		BgL_tmpz00_2887 =
																			((obj_t) BgL_arg1314z00_2697);
																		SET_CDR(BgL_tmpz00_2887,
																			BgL_arg1315z00_2698);
																	}
																}
														}
													else
														{	/* Ast/venv.scm 75 */
															obj_t BgL_arg1319z00_2699;

															{	/* Ast/venv.scm 75 */
																obj_t BgL_list1320z00_2700;

																{	/* Ast/venv.scm 75 */
																	obj_t BgL_arg1321z00_2701;

																	BgL_arg1321z00_2701 =
																		MAKE_YOUNG_PAIR(BgL_newz00_2690, BNIL);
																	BgL_list1320z00_2700 =
																		MAKE_YOUNG_PAIR(BgL_idz00_2692,
																		BgL_arg1321z00_2701);
																}
																BgL_arg1319z00_2699 = BgL_list1320z00_2700;
															}
															BGl_hashtablezd2putz12zc0zz__hashz00
																(BGl_za2Genvza2z00zzast_envz00, BgL_idz00_2692,
																BgL_arg1319z00_2699);
														}
												}
											}
										}
									}
								}
								{
									obj_t BgL_l1240z00_2893;

									BgL_l1240z00_2893 = CDR(BgL_l1240z00_2689);
									BgL_l1240z00_2689 = BgL_l1240z00_2893;
									goto BgL_zc3z04anonymousza31286ze3z87_2688;
								}
							}
						else
							{	/* Ast/venv.scm 88 */
								BgL_tmpz00_2855 = ((bool_t) 1);
							}
					}
				}
				return BBOOL(BgL_tmpz00_2855);
			}
		}

	}



/* additional-heap-restore-globals! */
	BGL_EXPORTED_DEF obj_t
		BGl_additionalzd2heapzd2restorezd2globalsz12zc0zzast_envz00(void)
	{
		{	/* Ast/venv.scm 105 */
			{
				obj_t BgL_l1243z00_1613;

				BgL_l1243z00_1613 =
					BGl_za2delayedzd2restoredzd2globalza2z00zzast_envz00;
			BgL_zc3z04anonymousza31323ze3z87_1614:
				if (PAIRP(BgL_l1243z00_1613))
					{	/* Ast/venv.scm 106 */
						BGl_restorezd2globalz12zc0zzast_envz00(CAR(BgL_l1243z00_1613));
						{
							obj_t BgL_l1243z00_2900;

							BgL_l1243z00_2900 = CDR(BgL_l1243z00_1613);
							BgL_l1243z00_1613 = BgL_l1243z00_2900;
							goto BgL_zc3z04anonymousza31323ze3z87_1614;
						}
					}
				else
					{	/* Ast/venv.scm 106 */
						((bool_t) 1);
					}
			}
			return BTRUE;
		}

	}



/* &additional-heap-restore-globals! */
	obj_t BGl_z62additionalzd2heapzd2restorezd2globalsz12za2zzast_envz00(obj_t
		BgL_envz00_2616)
	{
		{	/* Ast/venv.scm 105 */
			return BGl_additionalzd2heapzd2restorezd2globalsz12zc0zzast_envz00();
		}

	}



/* restore-global! */
	BGL_EXPORTED_DEF obj_t BGl_restorezd2globalz12zc0zzast_envz00(obj_t
		BgL_newz00_6)
	{
		{	/* Ast/venv.scm 112 */
			BGl_za2restoredza2z00zzast_envz00 =
				MAKE_YOUNG_PAIR(BgL_newz00_6, BGl_za2restoredza2z00zzast_envz00);
			{	/* Ast/venv.scm 115 */
				obj_t BgL_idz00_1619;

				BgL_idz00_1619 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt)
								((BgL_globalz00_bglt) BgL_newz00_6))))->BgL_idz00);
				{	/* Ast/venv.scm 115 */
					BgL_typez00_bglt BgL_typez00_1620;

					BgL_typez00_1620 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_globalz00_bglt) BgL_newz00_6))))->BgL_typez00);
					{	/* Ast/venv.scm 116 */
						BgL_valuez00_bglt BgL_valuez00_1621;

						BgL_valuez00_1621 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt) BgL_newz00_6))))->BgL_valuez00);
						{	/* Ast/venv.scm 117 */
							obj_t BgL_typeidz00_1622;

							BgL_typeidz00_1622 =
								(((BgL_typez00_bglt) COBJECT(BgL_typez00_1620))->BgL_idz00);
							{	/* Ast/venv.scm 118 */

								{	/* Ast/venv.scm 120 */
									BgL_typez00_bglt BgL_arg1327z00_1623;

									BgL_arg1327z00_1623 =
										BGl_findzd2typezd2zztype_envz00(BgL_typeidz00_1622);
									((((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_globalz00_bglt) BgL_newz00_6))))->
											BgL_typez00) =
										((BgL_typez00_bglt) BgL_arg1327z00_1623), BUNSPEC);
								}
								BGl_restorezd2valuezd2typesz12z12zzast_envz00(BgL_valuez00_1621,
									BgL_idz00_1619);
								{	/* Ast/venv.scm 124 */
									bool_t BgL_test1979z00_2919;

									{	/* Ast/venv.scm 124 */
										bool_t BgL_test1980z00_2920;

										{	/* Ast/venv.scm 124 */
											obj_t BgL_arg1348z00_1638;

											BgL_arg1348z00_1638 =
												BGl_thezd2backendzd2zzbackend_backendz00();
											BgL_test1980z00_2920 =
												(((BgL_backendz00_bglt) COBJECT(
														((BgL_backendz00_bglt) BgL_arg1348z00_1638)))->
												BgL_qualifiedzd2typeszd2);
										}
										if (BgL_test1980z00_2920)
											{	/* Ast/venv.scm 124 */
												if (
													((((BgL_globalz00_bglt) COBJECT(
																	((BgL_globalz00_bglt) BgL_newz00_6)))->
															BgL_modulez00) == CNST_TABLE_REF(1)))
													{	/* Ast/venv.scm 125 */
														BgL_test1979z00_2919 = ((bool_t) 0);
													}
												else
													{	/* Ast/venv.scm 125 */
														BgL_test1979z00_2919 = ((bool_t) 1);
													}
											}
										else
											{	/* Ast/venv.scm 124 */
												BgL_test1979z00_2919 = ((bool_t) 0);
											}
									}
									if (BgL_test1979z00_2919)
										{	/* Ast/venv.scm 126 */
											obj_t BgL_arg1339z00_1631;
											obj_t BgL_arg1340z00_1632;
											obj_t BgL_arg1342z00_1633;

											BgL_arg1339z00_1631 =
												(((BgL_globalz00_bglt) COBJECT(
														((BgL_globalz00_bglt) BgL_newz00_6)))->
												BgL_modulez00);
											BgL_arg1340z00_1632 =
												(((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
															BgL_newz00_6)))->BgL_jvmzd2typezd2namez00);
											BgL_arg1342z00_1633 =
												BGl_shapez00zztools_shapez00(BgL_newz00_6);
											{	/* Ast/venv.scm 126 */
												obj_t BgL_list1343z00_1634;

												BgL_list1343z00_1634 =
													MAKE_YOUNG_PAIR(BgL_arg1342z00_1633, BNIL);
												return
													BGl_addzd2qualifiedzd2typez12z12zzread_jvmz00
													(BgL_arg1339z00_1631, BgL_arg1340z00_1632,
													BgL_list1343z00_1634);
											}
										}
									else
										{	/* Ast/venv.scm 124 */
											return BFALSE;
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* &restore-global! */
	obj_t BGl_z62restorezd2globalz12za2zzast_envz00(obj_t BgL_envz00_2617,
		obj_t BgL_newz00_2618)
	{
		{	/* Ast/venv.scm 112 */
			return BGl_restorezd2globalz12zc0zzast_envz00(BgL_newz00_2618);
		}

	}



/* already-restored? */
	BGL_EXPORTED_DEF obj_t BGl_alreadyzd2restoredzf3z21zzast_envz00(obj_t
		BgL_funz00_8)
	{
		{	/* Ast/venv.scm 147 */
			return
				BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_funz00_8,
				BGl_za2restoredza2z00zzast_envz00);
		}

	}



/* &already-restored? */
	obj_t BGl_z62alreadyzd2restoredzf3z43zzast_envz00(obj_t BgL_envz00_2619,
		obj_t BgL_funz00_2620)
	{
		{	/* Ast/venv.scm 147 */
			return BGl_alreadyzd2restoredzf3z21zzast_envz00(BgL_funz00_2620);
		}

	}



/* get-genv */
	BGL_EXPORTED_DEF obj_t BGl_getzd2genvzd2zzast_envz00(void)
	{
		{	/* Ast/venv.scm 226 */
			return BGl_za2Genvza2z00zzast_envz00;
		}

	}



/* &get-genv */
	obj_t BGl_z62getzd2genvzb0zzast_envz00(obj_t BgL_envz00_2621)
	{
		{	/* Ast/venv.scm 226 */
			return BGl_getzd2genvzd2zzast_envz00();
		}

	}



/* initialize-Genv! */
	BGL_EXPORTED_DEF obj_t BGl_initializa7ezd2Genvz12z67zzast_envz00(void)
	{
		{	/* Ast/venv.scm 232 */
			return (BGl_za2Genvza2z00zzast_envz00 =
				BGl_makezd2hashtablezd2zz__hashz00(BNIL), BUNSPEC);
		}

	}



/* &initialize-Genv! */
	obj_t BGl_z62initializa7ezd2Genvz12z05zzast_envz00(obj_t BgL_envz00_2622)
	{
		{	/* Ast/venv.scm 232 */
			return BGl_initializa7ezd2Genvz12z67zzast_envz00();
		}

	}



/* find-global */
	BGL_EXPORTED_DEF obj_t BGl_findzd2globalzd2zzast_envz00(obj_t BgL_idz00_19,
		obj_t BgL_modulez00_20)
	{
		{	/* Ast/venv.scm 238 */
			{	/* Ast/venv.scm 240 */
				obj_t BgL_bucketz00_1640;
				obj_t BgL_modulez00_1641;

				BgL_bucketz00_1640 =
					BGl_hashtablezd2getzd2zz__hashz00(BGl_za2Genvza2z00zzast_envz00,
					BgL_idz00_19);
				if (NULLP(BgL_modulez00_20))
					{	/* Ast/venv.scm 241 */
						BgL_modulez00_1641 = BNIL;
					}
				else
					{	/* Ast/venv.scm 241 */
						BgL_modulez00_1641 = CAR(((obj_t) BgL_modulez00_20));
					}
				if (PAIRP(BgL_bucketz00_1640))
					{	/* Ast/venv.scm 243 */
						if (NULLP(CDR(BgL_bucketz00_1640)))
							{	/* Ast/venv.scm 245 */
								return BFALSE;
							}
						else
							{	/* Ast/venv.scm 245 */
								if (NULLP(BgL_modulez00_1641))
									{	/* Ast/venv.scm 247 */
										return CAR(CDR(BgL_bucketz00_1640));
									}
								else
									{	/* Ast/venv.scm 250 */
										obj_t BgL_g1108z00_1646;

										BgL_g1108z00_1646 = CDR(BgL_bucketz00_1640);
										{
											obj_t BgL_globalsz00_1648;

											BgL_globalsz00_1648 = BgL_g1108z00_1646;
										BgL_zc3z04anonymousza31356ze3z87_1649:
											if (NULLP(BgL_globalsz00_1648))
												{	/* Ast/venv.scm 252 */
													return BFALSE;
												}
											else
												{	/* Ast/venv.scm 252 */
													if (
														((((BgL_globalz00_bglt) COBJECT(
																		((BgL_globalz00_bglt)
																			CAR(
																				((obj_t) BgL_globalsz00_1648)))))->
																BgL_modulez00) == BgL_modulez00_1641))
														{	/* Ast/venv.scm 254 */
															return CAR(((obj_t) BgL_globalsz00_1648));
														}
													else
														{	/* Ast/venv.scm 257 */
															obj_t BgL_arg1367z00_1654;

															BgL_arg1367z00_1654 =
																CDR(((obj_t) BgL_globalsz00_1648));
															{
																obj_t BgL_globalsz00_2969;

																BgL_globalsz00_2969 = BgL_arg1367z00_1654;
																BgL_globalsz00_1648 = BgL_globalsz00_2969;
																goto BgL_zc3z04anonymousza31356ze3z87_1649;
															}
														}
												}
										}
									}
							}
					}
				else
					{	/* Ast/venv.scm 243 */
						return BFALSE;
					}
			}
		}

	}



/* &find-global */
	obj_t BGl_z62findzd2globalzb0zzast_envz00(obj_t BgL_envz00_2623,
		obj_t BgL_idz00_2624, obj_t BgL_modulez00_2625)
	{
		{	/* Ast/venv.scm 238 */
			return
				BGl_findzd2globalzd2zzast_envz00(BgL_idz00_2624, BgL_modulez00_2625);
		}

	}



/* find-global/module */
	BGL_EXPORTED_DEF obj_t BGl_findzd2globalzf2modulez20zzast_envz00(obj_t
		BgL_idz00_21, obj_t BgL_modulez00_22)
	{
		{	/* Ast/venv.scm 262 */
			{	/* Ast/venv.scm 263 */
				obj_t BgL_bucketz00_1660;

				BgL_bucketz00_1660 =
					BGl_hashtablezd2getzd2zz__hashz00(BGl_za2Genvza2z00zzast_envz00,
					BgL_idz00_21);
				if (PAIRP(BgL_bucketz00_1660))
					{	/* Ast/venv.scm 265 */
						if (NULLP(CDR(BgL_bucketz00_1660)))
							{	/* Ast/venv.scm 267 */
								return BFALSE;
							}
						else
							{	/* Ast/venv.scm 272 */
								obj_t BgL_g1109z00_1665;

								BgL_g1109z00_1665 = CDR(BgL_bucketz00_1660);
								{
									obj_t BgL_globalsz00_1667;

									BgL_globalsz00_1667 = BgL_g1109z00_1665;
								BgL_zc3z04anonymousza31412ze3z87_1668:
									if (NULLP(BgL_globalsz00_1667))
										{	/* Ast/venv.scm 274 */
											return BFALSE;
										}
									else
										{	/* Ast/venv.scm 274 */
											if (
												((((BgL_globalz00_bglt) COBJECT(
																((BgL_globalz00_bglt)
																	CAR(
																		((obj_t) BgL_globalsz00_1667)))))->
														BgL_modulez00) == BgL_modulez00_22))
												{	/* Ast/venv.scm 276 */
													return CAR(((obj_t) BgL_globalsz00_1667));
												}
											else
												{	/* Ast/venv.scm 279 */
													obj_t BgL_arg1434z00_1673;

													BgL_arg1434z00_1673 =
														CDR(((obj_t) BgL_globalsz00_1667));
													{
														obj_t BgL_globalsz00_2990;

														BgL_globalsz00_2990 = BgL_arg1434z00_1673;
														BgL_globalsz00_1667 = BgL_globalsz00_2990;
														goto BgL_zc3z04anonymousza31412ze3z87_1668;
													}
												}
										}
								}
							}
					}
				else
					{	/* Ast/venv.scm 265 */
						return BFALSE;
					}
			}
		}

	}



/* &find-global/module */
	obj_t BGl_z62findzd2globalzf2modulez42zzast_envz00(obj_t BgL_envz00_2626,
		obj_t BgL_idz00_2627, obj_t BgL_modulez00_2628)
	{
		{	/* Ast/venv.scm 262 */
			return
				BGl_findzd2globalzf2modulez20zzast_envz00(BgL_idz00_2627,
				BgL_modulez00_2628);
		}

	}



/* get-global/module */
	BGL_EXPORTED_DEF obj_t BGl_getzd2globalzf2modulez20zzast_envz00(obj_t
		BgL_idz00_23, obj_t BgL_modulez00_24)
	{
		{	/* Ast/venv.scm 284 */
			{	/* Ast/venv.scm 285 */
				obj_t BgL_globalz00_1678;

				BgL_globalz00_1678 =
					BGl_findzd2globalzf2modulez20zzast_envz00(BgL_idz00_23,
					BgL_modulez00_24);
				{	/* Ast/venv.scm 286 */
					bool_t BgL_test1992z00_2993;

					{	/* Ast/venv.scm 286 */
						bool_t BgL_test1993z00_2994;

						{	/* Ast/venv.scm 286 */
							obj_t BgL_classz00_2105;

							BgL_classz00_2105 = BGl_globalz00zzast_varz00;
							if (BGL_OBJECTP(BgL_globalz00_1678))
								{	/* Ast/venv.scm 286 */
									BgL_objectz00_bglt BgL_arg1807z00_2107;

									BgL_arg1807z00_2107 =
										(BgL_objectz00_bglt) (BgL_globalz00_1678);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Ast/venv.scm 286 */
											long BgL_idxz00_2113;

											BgL_idxz00_2113 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2107);
											BgL_test1993z00_2994 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2113 + 2L)) == BgL_classz00_2105);
										}
									else
										{	/* Ast/venv.scm 286 */
											bool_t BgL_res1891z00_2138;

											{	/* Ast/venv.scm 286 */
												obj_t BgL_oclassz00_2121;

												{	/* Ast/venv.scm 286 */
													obj_t BgL_arg1815z00_2129;
													long BgL_arg1816z00_2130;

													BgL_arg1815z00_2129 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Ast/venv.scm 286 */
														long BgL_arg1817z00_2131;

														BgL_arg1817z00_2131 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2107);
														BgL_arg1816z00_2130 =
															(BgL_arg1817z00_2131 - OBJECT_TYPE);
													}
													BgL_oclassz00_2121 =
														VECTOR_REF(BgL_arg1815z00_2129,
														BgL_arg1816z00_2130);
												}
												{	/* Ast/venv.scm 286 */
													bool_t BgL__ortest_1115z00_2122;

													BgL__ortest_1115z00_2122 =
														(BgL_classz00_2105 == BgL_oclassz00_2121);
													if (BgL__ortest_1115z00_2122)
														{	/* Ast/venv.scm 286 */
															BgL_res1891z00_2138 = BgL__ortest_1115z00_2122;
														}
													else
														{	/* Ast/venv.scm 286 */
															long BgL_odepthz00_2123;

															{	/* Ast/venv.scm 286 */
																obj_t BgL_arg1804z00_2124;

																BgL_arg1804z00_2124 = (BgL_oclassz00_2121);
																BgL_odepthz00_2123 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2124);
															}
															if ((2L < BgL_odepthz00_2123))
																{	/* Ast/venv.scm 286 */
																	obj_t BgL_arg1802z00_2126;

																	{	/* Ast/venv.scm 286 */
																		obj_t BgL_arg1803z00_2127;

																		BgL_arg1803z00_2127 = (BgL_oclassz00_2121);
																		BgL_arg1802z00_2126 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2127, 2L);
																	}
																	BgL_res1891z00_2138 =
																		(BgL_arg1802z00_2126 == BgL_classz00_2105);
																}
															else
																{	/* Ast/venv.scm 286 */
																	BgL_res1891z00_2138 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test1993z00_2994 = BgL_res1891z00_2138;
										}
								}
							else
								{	/* Ast/venv.scm 286 */
									BgL_test1993z00_2994 = ((bool_t) 0);
								}
						}
						if (BgL_test1993z00_2994)
							{	/* Ast/venv.scm 286 */
								BgL_test1992z00_2993 = ((bool_t) 0);
							}
						else
							{	/* Ast/venv.scm 286 */
								if (CBOOL(BGl_za2libzd2modeza2zd2zzengine_paramz00))
									{	/* Ast/venv.scm 286 */
										BgL_test1992z00_2993 = ((bool_t) 0);
									}
								else
									{	/* Ast/venv.scm 286 */
										BgL_test1992z00_2993 = ((bool_t) 1);
									}
							}
					}
					if (BgL_test1992z00_2993)
						{	/* Ast/venv.scm 289 */
							obj_t BgL_arg1472z00_1681;

							{	/* Ast/venv.scm 289 */
								obj_t BgL_list1473z00_1682;

								{	/* Ast/venv.scm 289 */
									obj_t BgL_arg1485z00_1683;

									BgL_arg1485z00_1683 = MAKE_YOUNG_PAIR(BgL_modulez00_24, BNIL);
									BgL_list1473z00_1682 =
										MAKE_YOUNG_PAIR(BgL_idz00_23, BgL_arg1485z00_1683);
								}
								BgL_arg1472z00_1681 =
									BGl_formatz00zz__r4_output_6_10_3z00
									(BGl_string1910z00zzast_envz00, BgL_list1473z00_1682);
							}
							BGl_internalzd2errorzd2zztools_errorz00(CNST_TABLE_REF(2),
								BGl_string1911z00zzast_envz00, BgL_arg1472z00_1681);
						}
					else
						{	/* Ast/venv.scm 286 */
							BFALSE;
						}
				}
				return BgL_globalz00_1678;
			}
		}

	}



/* &get-global/module */
	obj_t BGl_z62getzd2globalzf2modulez42zzast_envz00(obj_t BgL_envz00_2629,
		obj_t BgL_idz00_2630, obj_t BgL_modulez00_2631)
	{
		{	/* Ast/venv.scm 284 */
			return
				BGl_getzd2globalzf2modulez20zzast_envz00(BgL_idz00_2630,
				BgL_modulez00_2631);
		}

	}



/* warning-override-global! */
	obj_t BGl_warningzd2overridezd2globalz12z12zzast_envz00(obj_t BgL_nz00_25,
		obj_t BgL_oz00_26, obj_t BgL_srcez00_27)
	{
		{	/* Ast/venv.scm 295 */
			{	/* Ast/venv.scm 296 */
				bool_t BgL_test1999z00_3025;

				{	/* Ast/venv.scm 296 */
					bool_t BgL_test2000z00_3026;

					{	/* Ast/venv.scm 296 */
						int BgL_arg1553z00_1705;

						BgL_arg1553z00_1705 = BGl_bigloozd2warningzd2zz__paramz00();
						BgL_test2000z00_3026 = ((long) (BgL_arg1553z00_1705) > 0L);
					}
					if (BgL_test2000z00_3026)
						{	/* Ast/venv.scm 296 */
							BgL_test1999z00_3025 =
								CBOOL
								(BGl_za2warningzd2overridenzd2variablesza2z00zzengine_paramz00);
						}
					else
						{	/* Ast/venv.scm 296 */
							BgL_test1999z00_3025 = ((bool_t) 0);
						}
				}
				if (BgL_test1999z00_3025)
					{	/* Ast/venv.scm 298 */
						obj_t BgL_locz00_1690;
						obj_t BgL_oldz00_1691;
						obj_t BgL_newz00_1692;

						{	/* Ast/venv.scm 298 */
							obj_t BgL__ortest_1111z00_1694;

							{	/* Ast/venv.scm 298 */
								obj_t BgL_arg1509z00_1695;

								BgL_arg1509z00_1695 =
									(((BgL_globalz00_bglt) COBJECT(
											((BgL_globalz00_bglt) BgL_oz00_26)))->BgL_srcz00);
								BgL__ortest_1111z00_1694 =
									BGl_findzd2locationzd2zztools_locationz00
									(BgL_arg1509z00_1695);
							}
							if (CBOOL(BgL__ortest_1111z00_1694))
								{	/* Ast/venv.scm 298 */
									BgL_locz00_1690 = BgL__ortest_1111z00_1694;
								}
							else
								{	/* Ast/venv.scm 298 */
									BgL_locz00_1690 =
										BGl_findzd2locationzd2zztools_locationz00(BgL_srcez00_27);
								}
						}
						{	/* Ast/venv.scm 299 */
							obj_t BgL_arg1513z00_1696;

							{	/* Ast/venv.scm 299 */
								obj_t BgL_arg1514z00_1697;
								obj_t BgL_arg1516z00_1698;

								BgL_arg1514z00_1697 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_oz00_26))))->BgL_idz00);
								{	/* Ast/venv.scm 299 */
									obj_t BgL_arg1535z00_1699;

									BgL_arg1535z00_1699 =
										(((BgL_globalz00_bglt) COBJECT(
												((BgL_globalz00_bglt) BgL_oz00_26)))->BgL_modulez00);
									BgL_arg1516z00_1698 =
										MAKE_YOUNG_PAIR(BgL_arg1535z00_1699, BNIL);
								}
								BgL_arg1513z00_1696 =
									MAKE_YOUNG_PAIR(BgL_arg1514z00_1697, BgL_arg1516z00_1698);
							}
							BgL_oldz00_1691 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BgL_arg1513z00_1696);
						}
						{	/* Ast/venv.scm 300 */
							obj_t BgL_arg1540z00_1700;

							{	/* Ast/venv.scm 300 */
								obj_t BgL_arg1544z00_1701;
								obj_t BgL_arg1546z00_1702;

								BgL_arg1544z00_1701 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_nz00_25))))->BgL_idz00);
								{	/* Ast/venv.scm 300 */
									obj_t BgL_arg1552z00_1703;

									BgL_arg1552z00_1703 =
										(((BgL_globalz00_bglt) COBJECT(
												((BgL_globalz00_bglt) BgL_nz00_25)))->BgL_modulez00);
									BgL_arg1546z00_1702 =
										MAKE_YOUNG_PAIR(BgL_arg1552z00_1703, BNIL);
								}
								BgL_arg1540z00_1700 =
									MAKE_YOUNG_PAIR(BgL_arg1544z00_1701, BgL_arg1546z00_1702);
							}
							BgL_newz00_1692 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BgL_arg1540z00_1700);
						}
						if (CBOOL(BgL_locz00_1690))
							{	/* Ast/venv.scm 302 */
								return
									BGl_userzd2warningzf2locationz20zztools_errorz00
									(BgL_locz00_1690, BgL_oldz00_1691,
									BGl_string1912z00zzast_envz00, BgL_newz00_1692);
							}
						else
							{	/* Ast/venv.scm 302 */
								BGL_TAIL return
									BGl_userzd2warningzd2zztools_errorz00(BgL_oldz00_1691,
									BGl_string1912z00zzast_envz00, BgL_newz00_1692);
							}
					}
				else
					{	/* Ast/venv.scm 296 */
						return BFALSE;
					}
			}
		}

	}



/* error-rebind-global! */
	obj_t BGl_errorzd2rebindzd2globalz12z12zzast_envz00(obj_t BgL_oz00_28,
		obj_t BgL_srcz00_29)
	{
		{	/* Ast/venv.scm 309 */
			{	/* Ast/venv.scm 311 */
				obj_t BgL_locz00_1707;

				BgL_locz00_1707 =
					BGl_findzd2locationzd2zztools_locationz00(BgL_srcz00_29);
				if (CBOOL(BgL_locz00_1707))
					{	/* Ast/venv.scm 314 */
						obj_t BgL_arg1559z00_1709;
						obj_t BgL_arg1561z00_1710;

						BgL_arg1559z00_1709 =
							(((BgL_globalz00_bglt) COBJECT(
									((BgL_globalz00_bglt) BgL_oz00_28)))->BgL_modulez00);
						BgL_arg1561z00_1710 = BGl_shapez00zztools_shapez00(BgL_oz00_28);
						return
							BGl_userzd2errorzf2locationz20zztools_errorz00(BgL_locz00_1707,
							BgL_arg1559z00_1709, BGl_string1913z00zzast_envz00,
							BgL_arg1561z00_1710, BNIL);
					}
				else
					{	/* Ast/venv.scm 315 */
						obj_t BgL_arg1564z00_1712;
						obj_t BgL_arg1565z00_1713;

						BgL_arg1564z00_1712 =
							(((BgL_globalz00_bglt) COBJECT(
									((BgL_globalz00_bglt) BgL_oz00_28)))->BgL_modulez00);
						BgL_arg1565z00_1713 = BGl_shapez00zztools_shapez00(BgL_oz00_28);
						return
							BGl_userzd2errorzd2zztools_errorz00(BgL_arg1564z00_1712,
							BGl_string1913z00zzast_envz00, BgL_arg1565z00_1713, BNIL);
					}
			}
		}

	}



/* warning-rebind-global! */
	obj_t BGl_warningzd2rebindzd2globalz12z12zzast_envz00(obj_t BgL_oz00_30,
		obj_t BgL_srcz00_31)
	{
		{	/* Ast/venv.scm 320 */
			{	/* Ast/venv.scm 322 */
				obj_t BgL_locz00_1716;

				BgL_locz00_1716 =
					BGl_findzd2locationzd2zztools_locationz00(BgL_srcz00_31);
				if (CBOOL(BgL_locz00_1716))
					{	/* Ast/venv.scm 325 */
						obj_t BgL_arg1571z00_1718;
						obj_t BgL_arg1573z00_1719;

						BgL_arg1571z00_1718 =
							(((BgL_globalz00_bglt) COBJECT(
									((BgL_globalz00_bglt) BgL_oz00_30)))->BgL_modulez00);
						BgL_arg1573z00_1719 = BGl_shapez00zztools_shapez00(BgL_oz00_30);
						return
							BGl_userzd2warningzf2locationz20zztools_errorz00(BgL_locz00_1716,
							BgL_arg1571z00_1718, BGl_string1913z00zzast_envz00,
							BgL_arg1573z00_1719);
					}
				else
					{	/* Ast/venv.scm 326 */
						obj_t BgL_arg1575z00_1720;
						obj_t BgL_arg1576z00_1721;

						BgL_arg1575z00_1720 =
							(((BgL_globalz00_bglt) COBJECT(
									((BgL_globalz00_bglt) BgL_oz00_30)))->BgL_modulez00);
						BgL_arg1576z00_1721 = BGl_shapez00zztools_shapez00(BgL_oz00_30);
						return
							BGl_userzd2warningzd2zztools_errorz00(BgL_arg1575z00_1720,
							BGl_string1913z00zzast_envz00, BgL_arg1576z00_1721);
					}
			}
		}

	}



/* bind-global! */
	BGL_EXPORTED_DEF BgL_globalz00_bglt BGl_bindzd2globalz12zc0zzast_envz00(obj_t
		BgL_idz00_32, obj_t BgL_aliasz00_33, obj_t BgL_modulez00_34,
		BgL_valuez00_bglt BgL_valuez00_35, obj_t BgL_importz00_36,
		obj_t BgL_srcz00_37)
	{
		{	/* Ast/venv.scm 343 */
			{	/* Ast/venv.scm 346 */
				obj_t BgL_identz00_1722;

				if (CBOOL(BgL_aliasz00_33))
					{	/* Ast/venv.scm 346 */
						BgL_identz00_1722 = BgL_aliasz00_33;
					}
				else
					{	/* Ast/venv.scm 346 */
						BgL_identz00_1722 = BgL_idz00_32;
					}
				{	/* Ast/venv.scm 346 */
					obj_t BgL_oldz00_1723;

					{	/* Ast/venv.scm 347 */
						obj_t BgL_list1647z00_1758;

						BgL_list1647z00_1758 = MAKE_YOUNG_PAIR(BgL_modulez00_34, BNIL);
						BgL_oldz00_1723 =
							BGl_findzd2globalzd2zzast_envz00(BgL_identz00_1722,
							BgL_list1647z00_1758);
					}
					{	/* Ast/venv.scm 347 */

						{	/* Ast/venv.scm 348 */
							bool_t BgL_test2006z00_3085;

							{	/* Ast/venv.scm 348 */
								obj_t BgL_classz00_2142;

								BgL_classz00_2142 = BGl_globalz00zzast_varz00;
								if (BGL_OBJECTP(BgL_oldz00_1723))
									{	/* Ast/venv.scm 348 */
										BgL_objectz00_bglt BgL_arg1807z00_2144;

										BgL_arg1807z00_2144 =
											(BgL_objectz00_bglt) (BgL_oldz00_1723);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Ast/venv.scm 348 */
												long BgL_idxz00_2150;

												BgL_idxz00_2150 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2144);
												BgL_test2006z00_3085 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2150 + 2L)) == BgL_classz00_2142);
											}
										else
											{	/* Ast/venv.scm 348 */
												bool_t BgL_res1892z00_2175;

												{	/* Ast/venv.scm 348 */
													obj_t BgL_oclassz00_2158;

													{	/* Ast/venv.scm 348 */
														obj_t BgL_arg1815z00_2166;
														long BgL_arg1816z00_2167;

														BgL_arg1815z00_2166 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Ast/venv.scm 348 */
															long BgL_arg1817z00_2168;

															BgL_arg1817z00_2168 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2144);
															BgL_arg1816z00_2167 =
																(BgL_arg1817z00_2168 - OBJECT_TYPE);
														}
														BgL_oclassz00_2158 =
															VECTOR_REF(BgL_arg1815z00_2166,
															BgL_arg1816z00_2167);
													}
													{	/* Ast/venv.scm 348 */
														bool_t BgL__ortest_1115z00_2159;

														BgL__ortest_1115z00_2159 =
															(BgL_classz00_2142 == BgL_oclassz00_2158);
														if (BgL__ortest_1115z00_2159)
															{	/* Ast/venv.scm 348 */
																BgL_res1892z00_2175 = BgL__ortest_1115z00_2159;
															}
														else
															{	/* Ast/venv.scm 348 */
																long BgL_odepthz00_2160;

																{	/* Ast/venv.scm 348 */
																	obj_t BgL_arg1804z00_2161;

																	BgL_arg1804z00_2161 = (BgL_oclassz00_2158);
																	BgL_odepthz00_2160 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2161);
																}
																if ((2L < BgL_odepthz00_2160))
																	{	/* Ast/venv.scm 348 */
																		obj_t BgL_arg1802z00_2163;

																		{	/* Ast/venv.scm 348 */
																			obj_t BgL_arg1803z00_2164;

																			BgL_arg1803z00_2164 =
																				(BgL_oclassz00_2158);
																			BgL_arg1802z00_2163 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2164, 2L);
																		}
																		BgL_res1892z00_2175 =
																			(BgL_arg1802z00_2163 ==
																			BgL_classz00_2142);
																	}
																else
																	{	/* Ast/venv.scm 348 */
																		BgL_res1892z00_2175 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2006z00_3085 = BgL_res1892z00_2175;
											}
									}
								else
									{	/* Ast/venv.scm 348 */
										BgL_test2006z00_3085 = ((bool_t) 0);
									}
							}
							if (BgL_test2006z00_3085)
								{	/* Ast/venv.scm 348 */
									if (CBOOL(BGl_za2libzd2modeza2zd2zzengine_paramz00))
										{	/* Ast/venv.scm 350 */
											return ((BgL_globalz00_bglt) BgL_oldz00_1723);
										}
									else
										{	/* Ast/venv.scm 350 */
											if (
												(BgL_modulez00_34 ==
													(((BgL_globalz00_bglt) COBJECT(
																((BgL_globalz00_bglt) BgL_oldz00_1723)))->
														BgL_modulez00)))
												{	/* Ast/venv.scm 352 */
													BGl_warningzd2rebindzd2globalz12z12zzast_envz00
														(BgL_oldz00_1723, BgL_srcz00_37);
													return ((BgL_globalz00_bglt) BgL_oldz00_1723);
												}
											else
												{	/* Ast/venv.scm 352 */
													return
														((BgL_globalz00_bglt)
														BGl_errorzd2rebindzd2globalz12z12zzast_envz00
														(BgL_oldz00_1723, BgL_srcz00_37));
												}
										}
								}
							else
								{	/* Ast/venv.scm 357 */
									obj_t BgL_bucketz00_1728;
									BgL_globalz00_bglt BgL_newz00_1729;

									BgL_bucketz00_1728 =
										BGl_hashtablezd2getzd2zz__hashz00
										(BGl_za2Genvza2z00zzast_envz00, BgL_identz00_1722);
									{	/* Ast/venv.scm 358 */
										BgL_globalz00_bglt BgL_new1116z00_1755;

										{	/* Ast/venv.scm 364 */
											BgL_globalz00_bglt BgL_new1115z00_1756;

											BgL_new1115z00_1756 =
												((BgL_globalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_globalz00_bgl))));
											{	/* Ast/venv.scm 364 */
												long BgL_arg1646z00_1757;

												BgL_arg1646z00_1757 =
													BGL_CLASS_NUM(BGl_globalz00zzast_varz00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1115z00_1756),
													BgL_arg1646z00_1757);
											}
											{	/* Ast/venv.scm 364 */
												BgL_objectz00_bglt BgL_tmpz00_3124;

												BgL_tmpz00_3124 =
													((BgL_objectz00_bglt) BgL_new1115z00_1756);
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3124, BFALSE);
											}
											((BgL_objectz00_bglt) BgL_new1115z00_1756);
											BgL_new1116z00_1755 = BgL_new1115z00_1756;
										}
										((((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_new1116z00_1755)))->
												BgL_idz00) = ((obj_t) BgL_identz00_1722), BUNSPEC);
										((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
															BgL_new1116z00_1755)))->BgL_namez00) =
											((obj_t) BFALSE), BUNSPEC);
										((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
															BgL_new1116z00_1755)))->BgL_typez00) =
											((BgL_typez00_bglt) ((BgL_typez00_bglt)
													BGl_za2_za2z00zztype_cachez00)), BUNSPEC);
										((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
															BgL_new1116z00_1755)))->BgL_valuez00) =
											((BgL_valuez00_bglt) BgL_valuez00_35), BUNSPEC);
										((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
															BgL_new1116z00_1755)))->BgL_accessz00) =
											((obj_t) CNST_TABLE_REF(4)), BUNSPEC);
										((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
															BgL_new1116z00_1755)))->BgL_fastzd2alphazd2) =
											((obj_t) BUNSPEC), BUNSPEC);
										((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
															BgL_new1116z00_1755)))->BgL_removablez00) =
											((obj_t) CNST_TABLE_REF(5)), BUNSPEC);
										((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
															BgL_new1116z00_1755)))->BgL_occurrencez00) =
											((long) 0L), BUNSPEC);
										((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
															BgL_new1116z00_1755)))->BgL_occurrencewz00) =
											((long) 0L), BUNSPEC);
										((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
															BgL_new1116z00_1755)))->BgL_userzf3zf3) =
											((bool_t) ((bool_t) 1)), BUNSPEC);
										((((BgL_globalz00_bglt) COBJECT(BgL_new1116z00_1755))->
												BgL_modulez00) = ((obj_t) BgL_modulez00_34), BUNSPEC);
										((((BgL_globalz00_bglt) COBJECT(BgL_new1116z00_1755))->
												BgL_importz00) = ((obj_t) BgL_importz00_36), BUNSPEC);
										((((BgL_globalz00_bglt) COBJECT(BgL_new1116z00_1755))->
												BgL_evaluablezf3zf3) =
											((bool_t) ((bool_t) 1)), BUNSPEC);
										((((BgL_globalz00_bglt) COBJECT(BgL_new1116z00_1755))->
												BgL_evalzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
										((((BgL_globalz00_bglt) COBJECT(BgL_new1116z00_1755))->
												BgL_libraryz00) = ((obj_t) BFALSE), BUNSPEC);
										((((BgL_globalz00_bglt) COBJECT(BgL_new1116z00_1755))->
												BgL_pragmaz00) = ((obj_t) BNIL), BUNSPEC);
										((((BgL_globalz00_bglt) COBJECT(BgL_new1116z00_1755))->
												BgL_srcz00) = ((obj_t) BgL_srcz00_37), BUNSPEC);
										{
											obj_t BgL_auxz00_3158;

											if ((BgL_importz00_36 == CNST_TABLE_REF(6)))
												{	/* Ast/venv.scm 361 */
													BgL_auxz00_3158 = BGl_string1914z00zzast_envz00;
												}
											else
												{	/* Ast/venv.scm 361 */
													BgL_auxz00_3158 =
														BGl_modulezd2ze3qualifiedzd2typeze3zzread_jvmz00
														(BgL_modulez00_34);
												}
											((((BgL_globalz00_bglt) COBJECT(BgL_new1116z00_1755))->
													BgL_jvmzd2typezd2namez00) =
												((obj_t) BgL_auxz00_3158), BUNSPEC);
										}
										((((BgL_globalz00_bglt) COBJECT(BgL_new1116z00_1755))->
												BgL_initz00) = ((obj_t) BUNSPEC), BUNSPEC);
										{
											obj_t BgL_auxz00_3165;

											if (CBOOL(BgL_aliasz00_33))
												{	/* Ast/venv.scm 365 */
													BgL_auxz00_3165 = BgL_idz00_32;
												}
											else
												{	/* Ast/venv.scm 365 */
													BgL_auxz00_3165 = BFALSE;
												}
											((((BgL_globalz00_bglt) COBJECT(BgL_new1116z00_1755))->
													BgL_aliasz00) = ((obj_t) BgL_auxz00_3165), BUNSPEC);
										}
										BgL_newz00_1729 = BgL_new1116z00_1755;
									}
									{	/* Ast/venv.scm 371 */
										bool_t BgL_test2015z00_3169;

										if (PAIRP(BgL_bucketz00_1728))
											{	/* Ast/venv.scm 371 */
												BgL_test2015z00_3169 = NULLP(CDR(BgL_bucketz00_1728));
											}
										else
											{	/* Ast/venv.scm 371 */
												BgL_test2015z00_3169 = ((bool_t) 1);
											}
										if (BgL_test2015z00_3169)
											{	/* Ast/venv.scm 371 */
												{	/* Ast/venv.scm 373 */
													obj_t BgL_arg1595z00_1733;

													{	/* Ast/venv.scm 373 */
														obj_t BgL_list1596z00_1734;

														{	/* Ast/venv.scm 373 */
															obj_t BgL_arg1602z00_1735;

															BgL_arg1602z00_1735 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_newz00_1729), BNIL);
															BgL_list1596z00_1734 =
																MAKE_YOUNG_PAIR(BgL_identz00_1722,
																BgL_arg1602z00_1735);
														}
														BgL_arg1595z00_1733 = BgL_list1596z00_1734;
													}
													BGl_hashtablezd2putz12zc0zz__hashz00
														(BGl_za2Genvza2z00zzast_envz00, BgL_identz00_1722,
														BgL_arg1595z00_1733);
												}
												return BgL_newz00_1729;
											}
										else
											{	/* Ast/venv.scm 376 */
												obj_t BgL_oldza2za2_1736;

												BgL_oldza2za2_1736 = CDR(((obj_t) BgL_bucketz00_1728));
												{	/* Ast/venv.scm 376 */
													obj_t BgL_midz00_1737;

													{	/* Ast/venv.scm 378 */
														obj_t BgL_arg1629z00_1751;

														BgL_arg1629z00_1751 =
															(((BgL_globalz00_bglt) COBJECT(
																	((BgL_globalz00_bglt)
																		CAR(
																			((obj_t) BgL_oldza2za2_1736)))))->
															BgL_modulez00);
														BgL_midz00_1737 =
															BGl_modulezd2initializa7ationzd2idza7zzmodule_modulez00
															(BgL_arg1629z00_1751);
													}
													{	/* Ast/venv.scm 377 */

														{	/* Ast/venv.scm 380 */
															bool_t BgL_test2017z00_3185;

															{	/* Ast/venv.scm 380 */
																obj_t BgL_arg1626z00_1749;

																BgL_arg1626z00_1749 =
																	(((BgL_globalz00_bglt) COBJECT(
																			((BgL_globalz00_bglt)
																				CAR(
																					((obj_t) BgL_oldza2za2_1736)))))->
																	BgL_modulez00);
																BgL_test2017z00_3185 =
																	(BgL_arg1626z00_1749 ==
																	BGl_za2moduleza2z00zzmodule_modulez00);
															}
															if (BgL_test2017z00_3185)
																{	/* Ast/venv.scm 380 */
																	{	/* Ast/venv.scm 382 */
																		bool_t BgL_test2018z00_3191;

																		if ((BgL_identz00_1722 == BgL_midz00_1737))
																			{	/* Ast/venv.scm 382 */
																				BgL_test2018z00_3191 = ((bool_t) 0);
																			}
																		else
																			{	/* Ast/venv.scm 382 */
																				if (CBOOL
																					(BGl_za2libzd2modeza2zd2zzengine_paramz00))
																					{	/* Ast/venv.scm 382 */
																						BgL_test2018z00_3191 = ((bool_t) 0);
																					}
																				else
																					{	/* Ast/venv.scm 382 */
																						BgL_test2018z00_3191 = ((bool_t) 1);
																					}
																			}
																		if (BgL_test2018z00_3191)
																			{	/* Ast/venv.scm 383 */
																				obj_t BgL_arg1609z00_1742;

																				BgL_arg1609z00_1742 =
																					CAR(((obj_t) BgL_oldza2za2_1736));
																				BGl_warningzd2overridezd2globalz12z12zzast_envz00
																					(BgL_arg1609z00_1742,
																					((obj_t) BgL_newz00_1729),
																					BgL_srcz00_37);
																			}
																		else
																			{	/* Ast/venv.scm 382 */
																				BFALSE;
																			}
																	}
																	{	/* Ast/venv.scm 384 */
																		obj_t BgL_arg1611z00_1743;
																		obj_t BgL_arg1613z00_1744;

																		BgL_arg1611z00_1743 =
																			CDR(((obj_t) BgL_bucketz00_1728));
																		{	/* Ast/venv.scm 384 */
																			obj_t BgL_arg1615z00_1745;

																			{	/* Ast/venv.scm 384 */
																				obj_t BgL_pairz00_2193;

																				BgL_pairz00_2193 =
																					CDR(((obj_t) BgL_bucketz00_1728));
																				BgL_arg1615z00_1745 =
																					CDR(BgL_pairz00_2193);
																			}
																			BgL_arg1613z00_1744 =
																				MAKE_YOUNG_PAIR(
																				((obj_t) BgL_newz00_1729),
																				BgL_arg1615z00_1745);
																		}
																		{	/* Ast/venv.scm 384 */
																			obj_t BgL_tmpz00_3207;

																			BgL_tmpz00_3207 =
																				((obj_t) BgL_arg1611z00_1743);
																			SET_CDR(BgL_tmpz00_3207,
																				BgL_arg1613z00_1744);
																		}
																	}
																	return BgL_newz00_1729;
																}
															else
																{	/* Ast/venv.scm 387 */
																	obj_t BgL_newza2za2_1746;

																	BgL_newza2za2_1746 =
																		MAKE_YOUNG_PAIR(
																		((obj_t) BgL_newz00_1729),
																		BgL_oldza2za2_1736);
																	{	/* Ast/venv.scm 388 */
																		bool_t BgL_test2021z00_3212;

																		if ((BgL_identz00_1722 == BgL_midz00_1737))
																			{	/* Ast/venv.scm 388 */
																				BgL_test2021z00_3212 = ((bool_t) 0);
																			}
																		else
																			{	/* Ast/venv.scm 388 */
																				if (CBOOL
																					(BGl_za2libzd2modeza2zd2zzengine_paramz00))
																					{	/* Ast/venv.scm 388 */
																						BgL_test2021z00_3212 = ((bool_t) 0);
																					}
																				else
																					{	/* Ast/venv.scm 388 */
																						BgL_test2021z00_3212 = ((bool_t) 1);
																					}
																			}
																		if (BgL_test2021z00_3212)
																			{	/* Ast/venv.scm 389 */
																				obj_t BgL_arg1625z00_1748;

																				BgL_arg1625z00_1748 =
																					CAR(((obj_t) BgL_oldza2za2_1736));
																				BGl_warningzd2overridezd2globalz12z12zzast_envz00
																					(((obj_t) BgL_newz00_1729),
																					BgL_arg1625z00_1748, BgL_srcz00_37);
																			}
																		else
																			{	/* Ast/venv.scm 388 */
																				BFALSE;
																			}
																	}
																	{	/* Ast/venv.scm 390 */
																		obj_t BgL_tmpz00_3221;

																		BgL_tmpz00_3221 =
																			((obj_t) BgL_bucketz00_1728);
																		SET_CDR(BgL_tmpz00_3221,
																			BgL_newza2za2_1746);
																	}
																	return BgL_newz00_1729;
																}
														}
													}
												}
											}
									}
								}
						}
					}
				}
			}
		}

	}



/* &bind-global! */
	BgL_globalz00_bglt BGl_z62bindzd2globalz12za2zzast_envz00(obj_t
		BgL_envz00_2632, obj_t BgL_idz00_2633, obj_t BgL_aliasz00_2634,
		obj_t BgL_modulez00_2635, obj_t BgL_valuez00_2636, obj_t BgL_importz00_2637,
		obj_t BgL_srcz00_2638)
	{
		{	/* Ast/venv.scm 343 */
			return
				BGl_bindzd2globalz12zc0zzast_envz00(BgL_idz00_2633, BgL_aliasz00_2634,
				BgL_modulez00_2635, ((BgL_valuez00_bglt) BgL_valuez00_2636),
				BgL_importz00_2637, BgL_srcz00_2638);
		}

	}



/* unbind-global! */
	BGL_EXPORTED_DEF obj_t BGl_unbindzd2globalz12zc0zzast_envz00(obj_t
		BgL_idz00_38, obj_t BgL_modulez00_39)
	{
		{	/* Ast/venv.scm 396 */
			{	/* Ast/venv.scm 397 */
				obj_t BgL_globalz00_1760;

				{	/* Ast/venv.scm 397 */
					obj_t BgL_list1689z00_1779;

					BgL_list1689z00_1779 = MAKE_YOUNG_PAIR(BgL_modulez00_39, BNIL);
					BgL_globalz00_1760 =
						BGl_findzd2globalzd2zzast_envz00(BgL_idz00_38,
						BgL_list1689z00_1779);
				}
				{	/* Ast/venv.scm 398 */
					bool_t BgL_test2024z00_3228;

					{	/* Ast/venv.scm 398 */
						obj_t BgL_classz00_2197;

						BgL_classz00_2197 = BGl_globalz00zzast_varz00;
						if (BGL_OBJECTP(BgL_globalz00_1760))
							{	/* Ast/venv.scm 398 */
								BgL_objectz00_bglt BgL_arg1807z00_2199;

								BgL_arg1807z00_2199 = (BgL_objectz00_bglt) (BgL_globalz00_1760);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Ast/venv.scm 398 */
										long BgL_idxz00_2205;

										BgL_idxz00_2205 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2199);
										BgL_test2024z00_3228 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2205 + 2L)) == BgL_classz00_2197);
									}
								else
									{	/* Ast/venv.scm 398 */
										bool_t BgL_res1894z00_2230;

										{	/* Ast/venv.scm 398 */
											obj_t BgL_oclassz00_2213;

											{	/* Ast/venv.scm 398 */
												obj_t BgL_arg1815z00_2221;
												long BgL_arg1816z00_2222;

												BgL_arg1815z00_2221 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Ast/venv.scm 398 */
													long BgL_arg1817z00_2223;

													BgL_arg1817z00_2223 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2199);
													BgL_arg1816z00_2222 =
														(BgL_arg1817z00_2223 - OBJECT_TYPE);
												}
												BgL_oclassz00_2213 =
													VECTOR_REF(BgL_arg1815z00_2221, BgL_arg1816z00_2222);
											}
											{	/* Ast/venv.scm 398 */
												bool_t BgL__ortest_1115z00_2214;

												BgL__ortest_1115z00_2214 =
													(BgL_classz00_2197 == BgL_oclassz00_2213);
												if (BgL__ortest_1115z00_2214)
													{	/* Ast/venv.scm 398 */
														BgL_res1894z00_2230 = BgL__ortest_1115z00_2214;
													}
												else
													{	/* Ast/venv.scm 398 */
														long BgL_odepthz00_2215;

														{	/* Ast/venv.scm 398 */
															obj_t BgL_arg1804z00_2216;

															BgL_arg1804z00_2216 = (BgL_oclassz00_2213);
															BgL_odepthz00_2215 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2216);
														}
														if ((2L < BgL_odepthz00_2215))
															{	/* Ast/venv.scm 398 */
																obj_t BgL_arg1802z00_2218;

																{	/* Ast/venv.scm 398 */
																	obj_t BgL_arg1803z00_2219;

																	BgL_arg1803z00_2219 = (BgL_oclassz00_2213);
																	BgL_arg1802z00_2218 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2219,
																		2L);
																}
																BgL_res1894z00_2230 =
																	(BgL_arg1802z00_2218 == BgL_classz00_2197);
															}
														else
															{	/* Ast/venv.scm 398 */
																BgL_res1894z00_2230 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2024z00_3228 = BgL_res1894z00_2230;
									}
							}
						else
							{	/* Ast/venv.scm 398 */
								BgL_test2024z00_3228 = ((bool_t) 0);
							}
					}
					if (BgL_test2024z00_3228)
						{	/* Ast/venv.scm 400 */
							obj_t BgL_bucketz00_1762;

							BgL_bucketz00_1762 =
								BGl_hashtablezd2getzd2zz__hashz00(BGl_za2Genvza2z00zzast_envz00,
								BgL_idz00_38);
							{	/* Ast/venv.scm 401 */
								obj_t BgL_g1117z00_1763;

								BgL_g1117z00_1763 = CDR(((obj_t) BgL_bucketz00_1762));
								{
									obj_t BgL_curz00_1765;
									obj_t BgL_prevz00_1766;

									BgL_curz00_1765 = BgL_g1117z00_1763;
									BgL_prevz00_1766 = BgL_bucketz00_1762;
								BgL_zc3z04anonymousza31649ze3z87_1767:
									if ((CAR(((obj_t) BgL_curz00_1765)) == BgL_globalz00_1760))
										{	/* Ast/venv.scm 404 */
											obj_t BgL_arg1654z00_1770;

											BgL_arg1654z00_1770 = CDR(((obj_t) BgL_curz00_1765));
											{	/* Ast/venv.scm 404 */
												obj_t BgL_tmpz00_3260;

												BgL_tmpz00_3260 = ((obj_t) BgL_prevz00_1766);
												return SET_CDR(BgL_tmpz00_3260, BgL_arg1654z00_1770);
											}
										}
									else
										{	/* Ast/venv.scm 405 */
											obj_t BgL_arg1661z00_1771;
											obj_t BgL_arg1663z00_1772;

											BgL_arg1661z00_1771 = CDR(((obj_t) BgL_curz00_1765));
											BgL_arg1663z00_1772 = CDR(((obj_t) BgL_prevz00_1766));
											{
												obj_t BgL_prevz00_3268;
												obj_t BgL_curz00_3267;

												BgL_curz00_3267 = BgL_arg1661z00_1771;
												BgL_prevz00_3268 = BgL_arg1663z00_1772;
												BgL_prevz00_1766 = BgL_prevz00_3268;
												BgL_curz00_1765 = BgL_curz00_3267;
												goto BgL_zc3z04anonymousza31649ze3z87_1767;
											}
										}
								}
							}
						}
					else
						{	/* Ast/venv.scm 399 */
							obj_t BgL_arg1678z00_1775;

							{	/* Ast/venv.scm 399 */
								obj_t BgL_arg1681z00_1777;

								{	/* Ast/venv.scm 399 */
									obj_t BgL_arg1688z00_1778;

									BgL_arg1688z00_1778 = MAKE_YOUNG_PAIR(BgL_modulez00_39, BNIL);
									BgL_arg1681z00_1777 =
										MAKE_YOUNG_PAIR(BgL_idz00_38, BgL_arg1688z00_1778);
								}
								BgL_arg1678z00_1775 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BgL_arg1681z00_1777);
							}
							return
								BGl_userzd2errorzd2zztools_errorz00
								(BGl_string1915z00zzast_envz00, BGl_string1916z00zzast_envz00,
								BgL_arg1678z00_1775, BNIL);
						}
				}
			}
		}

	}



/* &unbind-global! */
	obj_t BGl_z62unbindzd2globalz12za2zzast_envz00(obj_t BgL_envz00_2639,
		obj_t BgL_idz00_2640, obj_t BgL_modulez00_2641)
	{
		{	/* Ast/venv.scm 396 */
			return
				BGl_unbindzd2globalz12zc0zzast_envz00(BgL_idz00_2640,
				BgL_modulez00_2641);
		}

	}



/* for-each-global! */
	BGL_EXPORTED_DEF obj_t BGl_forzd2eachzd2globalz12z12zzast_envz00(obj_t
		BgL_procz00_40, obj_t BgL_envz00_41)
	{
		{	/* Ast/venv.scm 410 */
			{	/* Ast/venv.scm 411 */
				obj_t BgL_arg1691z00_1780;

				if (NULLP(BgL_envz00_41))
					{	/* Ast/venv.scm 411 */
						BgL_arg1691z00_1780 = BGl_za2Genvza2z00zzast_envz00;
					}
				else
					{	/* Ast/venv.scm 411 */
						BgL_arg1691z00_1780 = CAR(((obj_t) BgL_envz00_41));
					}
				{	/* Ast/venv.scm 412 */
					obj_t BgL_zc3z04anonymousza31694ze3z87_2642;

					BgL_zc3z04anonymousza31694ze3z87_2642 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31694ze3ze5zzast_envz00,
						(int) (2L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31694ze3z87_2642,
						(int) (0L), BgL_procz00_40);
					return
						BGl_hashtablezd2forzd2eachz00zz__hashz00(BgL_arg1691z00_1780,
						BgL_zc3z04anonymousza31694ze3z87_2642);
				}
			}
		}

	}



/* &for-each-global! */
	obj_t BGl_z62forzd2eachzd2globalz12z70zzast_envz00(obj_t BgL_envz00_2643,
		obj_t BgL_procz00_2644, obj_t BgL_envz00_2645)
	{
		{	/* Ast/venv.scm 410 */
			return
				BGl_forzd2eachzd2globalz12z12zzast_envz00(BgL_procz00_2644,
				BgL_envz00_2645);
		}

	}



/* &<@anonymous:1694> */
	obj_t BGl_z62zc3z04anonymousza31694ze3ze5zzast_envz00(obj_t BgL_envz00_2646,
		obj_t BgL_kz00_2648, obj_t BgL_bucketz00_2649)
	{
		{	/* Ast/venv.scm 412 */
			{	/* Ast/venv.scm 412 */
				obj_t BgL_procz00_2647;

				BgL_procz00_2647 = ((obj_t) PROCEDURE_REF(BgL_envz00_2646, (int) (0L)));
				{	/* Ast/venv.scm 412 */
					bool_t BgL_tmpz00_3289;

					{	/* Ast/venv.scm 412 */
						obj_t BgL_g1247z00_2702;

						BgL_g1247z00_2702 = CDR(((obj_t) BgL_bucketz00_2649));
						{
							obj_t BgL_l1245z00_2704;

							BgL_l1245z00_2704 = BgL_g1247z00_2702;
						BgL_zc3z04anonymousza31695ze3z87_2703:
							if (PAIRP(BgL_l1245z00_2704))
								{	/* Ast/venv.scm 412 */
									{	/* Ast/venv.scm 412 */
										obj_t BgL_arg1699z00_2705;

										BgL_arg1699z00_2705 = CAR(BgL_l1245z00_2704);
										BGL_PROCEDURE_CALL1(BgL_procz00_2647, BgL_arg1699z00_2705);
									}
									{
										obj_t BgL_l1245z00_3299;

										BgL_l1245z00_3299 = CDR(BgL_l1245z00_2704);
										BgL_l1245z00_2704 = BgL_l1245z00_3299;
										goto BgL_zc3z04anonymousza31695ze3z87_2703;
									}
								}
							else
								{	/* Ast/venv.scm 412 */
									BgL_tmpz00_3289 = ((bool_t) 1);
								}
						}
					}
					return BBOOL(BgL_tmpz00_3289);
				}
			}
		}

	}



/* global-bucket-position */
	BGL_EXPORTED_DEF obj_t BGl_globalzd2bucketzd2positionz00zzast_envz00(obj_t
		BgL_idz00_42, obj_t BgL_modulez00_43)
	{
		{	/* Ast/venv.scm 417 */
			{	/* Ast/venv.scm 418 */
				obj_t BgL_bucketz00_1795;

				BgL_bucketz00_1795 =
					BGl_hashtablezd2getzd2zz__hashz00(BGl_za2Genvza2z00zzast_envz00,
					BgL_idz00_42);
				if (PAIRP(BgL_bucketz00_1795))
					{	/* Ast/venv.scm 421 */
						obj_t BgL_g1118z00_1797;

						BgL_g1118z00_1797 = CDR(BgL_bucketz00_1795);
						{
							obj_t BgL_globalsz00_1799;
							long BgL_posz00_1800;

							{	/* Ast/venv.scm 421 */
								long BgL_tmpz00_3306;

								BgL_globalsz00_1799 = BgL_g1118z00_1797;
								BgL_posz00_1800 = 0L;
							BgL_zc3z04anonymousza31702ze3z87_1801:
								if (NULLP(BgL_globalsz00_1799))
									{	/* Ast/venv.scm 424 */
										BgL_tmpz00_3306 = -1L;
									}
								else
									{	/* Ast/venv.scm 424 */
										if (
											((((BgL_globalz00_bglt) COBJECT(
															((BgL_globalz00_bglt)
																CAR(
																	((obj_t) BgL_globalsz00_1799)))))->
													BgL_modulez00) == BgL_modulez00_43))
											{	/* Ast/venv.scm 426 */
												BgL_tmpz00_3306 = BgL_posz00_1800;
											}
										else
											{	/* Ast/venv.scm 429 */
												obj_t BgL_arg1714z00_1806;
												long BgL_arg1717z00_1807;

												BgL_arg1714z00_1806 =
													CDR(((obj_t) BgL_globalsz00_1799));
												BgL_arg1717z00_1807 = (BgL_posz00_1800 + 1L);
												{
													long BgL_posz00_3319;
													obj_t BgL_globalsz00_3318;

													BgL_globalsz00_3318 = BgL_arg1714z00_1806;
													BgL_posz00_3319 = BgL_arg1717z00_1807;
													BgL_posz00_1800 = BgL_posz00_3319;
													BgL_globalsz00_1799 = BgL_globalsz00_3318;
													goto BgL_zc3z04anonymousza31702ze3z87_1801;
												}
											}
									}
								return BINT(BgL_tmpz00_3306);
							}
						}
					}
				else
					{	/* Ast/venv.scm 419 */
						return BINT(-1L);
					}
			}
		}

	}



/* &global-bucket-position */
	obj_t BGl_z62globalzd2bucketzd2positionz62zzast_envz00(obj_t BgL_envz00_2650,
		obj_t BgL_idz00_2651, obj_t BgL_modulez00_2652)
	{
		{	/* Ast/venv.scm 417 */
			return
				BGl_globalzd2bucketzd2positionz00zzast_envz00(BgL_idz00_2651,
				BgL_modulez00_2652);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_envz00(void)
	{
		{	/* Ast/venv.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_envz00(void)
	{
		{	/* Ast/venv.scm 14 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_restorezd2valuezd2typesz12zd2envzc0zzast_envz00,
				BGl_proc1917z00zzast_envz00, BGl_valuez00zzast_varz00,
				BGl_string1918z00zzast_envz00);
		}

	}



/* &restore-value-types!1248 */
	obj_t BGl_z62restorezd2valuezd2typesz121248z70zzast_envz00(obj_t
		BgL_envz00_2654, obj_t BgL_valuez00_2655, obj_t BgL_idz00_2656)
	{
		{	/* Ast/venv.scm 153 */
			return BUNSPEC;
		}

	}



/* restore-value-types! */
	obj_t BGl_restorezd2valuezd2typesz12z12zzast_envz00(BgL_valuez00_bglt
		BgL_valuez00_9, obj_t BgL_idz00_10)
	{
		{	/* Ast/venv.scm 153 */
			{	/* Ast/venv.scm 153 */
				obj_t BgL_method1249z00_1816;

				{	/* Ast/venv.scm 153 */
					obj_t BgL_res1899z00_2276;

					{	/* Ast/venv.scm 153 */
						long BgL_objzd2classzd2numz00_2247;

						BgL_objzd2classzd2numz00_2247 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_valuez00_9));
						{	/* Ast/venv.scm 153 */
							obj_t BgL_arg1811z00_2248;

							BgL_arg1811z00_2248 =
								PROCEDURE_REF
								(BGl_restorezd2valuezd2typesz12zd2envzc0zzast_envz00,
								(int) (1L));
							{	/* Ast/venv.scm 153 */
								int BgL_offsetz00_2251;

								BgL_offsetz00_2251 = (int) (BgL_objzd2classzd2numz00_2247);
								{	/* Ast/venv.scm 153 */
									long BgL_offsetz00_2252;

									BgL_offsetz00_2252 =
										((long) (BgL_offsetz00_2251) - OBJECT_TYPE);
									{	/* Ast/venv.scm 153 */
										long BgL_modz00_2253;

										BgL_modz00_2253 =
											(BgL_offsetz00_2252 >> (int) ((long) ((int) (4L))));
										{	/* Ast/venv.scm 153 */
											long BgL_restz00_2255;

											BgL_restz00_2255 =
												(BgL_offsetz00_2252 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/venv.scm 153 */

												{	/* Ast/venv.scm 153 */
													obj_t BgL_bucketz00_2257;

													BgL_bucketz00_2257 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2248), BgL_modz00_2253);
													BgL_res1899z00_2276 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2257), BgL_restz00_2255);
					}}}}}}}}
					BgL_method1249z00_1816 = BgL_res1899z00_2276;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1249z00_1816,
					((obj_t) BgL_valuez00_9), BgL_idz00_10);
			}
		}

	}



/* &restore-value-types! */
	obj_t BGl_z62restorezd2valuezd2typesz12z70zzast_envz00(obj_t BgL_envz00_2657,
		obj_t BgL_valuez00_2658, obj_t BgL_idz00_2659)
	{
		{	/* Ast/venv.scm 153 */
			return
				BGl_restorezd2valuezd2typesz12z12zzast_envz00(
				((BgL_valuez00_bglt) BgL_valuez00_2658), BgL_idz00_2659);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_envz00(void)
	{
		{	/* Ast/venv.scm 14 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_restorezd2valuezd2typesz12zd2envzc0zzast_envz00,
				BGl_funz00zzast_varz00, BGl_proc1919z00zzast_envz00,
				BGl_string1920z00zzast_envz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_restorezd2valuezd2typesz12zd2envzc0zzast_envz00,
				BGl_sfunz00zzast_varz00, BGl_proc1921z00zzast_envz00,
				BGl_string1920z00zzast_envz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_restorezd2valuezd2typesz12zd2envzc0zzast_envz00,
				BGl_isfunz00zzinline_inlinez00, BGl_proc1922z00zzast_envz00,
				BGl_string1920z00zzast_envz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_restorezd2valuezd2typesz12zd2envzc0zzast_envz00,
				BGl_cfunz00zzast_varz00, BGl_proc1923z00zzast_envz00,
				BGl_string1920z00zzast_envz00);
		}

	}



/* &restore-value-types!1257 */
	obj_t BGl_z62restorezd2valuezd2typesz121257z70zzast_envz00(obj_t
		BgL_envz00_2664, obj_t BgL_valuez00_2665, obj_t BgL_idz00_2666)
	{
		{	/* Ast/venv.scm 208 */
			{	/* Ast/venv.scm 208 */
				bool_t BgL_tmpz00_3361;

				{

					{	/* Ast/venv.scm 208 */
						obj_t BgL_nextzd2method1256zd2_2709;

						BgL_nextzd2method1256zd2_2709 =
							BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
							((BgL_objectz00_bglt)
								((BgL_cfunz00_bglt) BgL_valuez00_2665)),
							BGl_restorezd2valuezd2typesz12zd2envzc0zzast_envz00,
							BGl_cfunz00zzast_varz00);
						BGL_PROCEDURE_CALL2(BgL_nextzd2method1256zd2_2709,
							((obj_t) ((BgL_cfunz00_bglt) BgL_valuez00_2665)), BgL_idz00_2666);
					}
					{
						obj_t BgL_argsz00_2711;

						BgL_argsz00_2711 =
							(((BgL_cfunz00_bglt) COBJECT(
									((BgL_cfunz00_bglt) BgL_valuez00_2665)))->BgL_argszd2typezd2);
					BgL_loopz00_2710:
						if (PAIRP(BgL_argsz00_2711))
							{	/* Ast/venv.scm 212 */
								if (
									((((BgL_typez00_bglt) COBJECT(
													((BgL_typez00_bglt)
														CAR(BgL_argsz00_2711))))->BgL_idz00) ==
										CNST_TABLE_REF(7)))
									{	/* Ast/venv.scm 214 */
										BGl_userzd2warningzd2zztools_errorz00
											(BGl_string1924z00zzast_envz00,
											BGl_string1925z00zzast_envz00, BgL_idz00_2666);
										SET_CAR(BgL_argsz00_2711, BGl_za2objza2z00zztype_cachez00);
									}
								else
									{	/* Ast/venv.scm 220 */
										BgL_typez00_bglt BgL_arg1773z00_2712;

										{	/* Ast/venv.scm 220 */
											obj_t BgL_arg1775z00_2713;

											BgL_arg1775z00_2713 =
												(((BgL_typez00_bglt) COBJECT(
														((BgL_typez00_bglt)
															CAR(BgL_argsz00_2711))))->BgL_idz00);
											BgL_arg1773z00_2712 =
												BGl_findzd2typezd2zztype_envz00(BgL_arg1775z00_2713);
										}
										{	/* Ast/venv.scm 220 */
											obj_t BgL_tmpz00_3386;

											BgL_tmpz00_3386 = ((obj_t) BgL_arg1773z00_2712);
											SET_CAR(BgL_argsz00_2711, BgL_tmpz00_3386);
										}
									}
								{
									obj_t BgL_argsz00_3389;

									BgL_argsz00_3389 = CDR(BgL_argsz00_2711);
									BgL_argsz00_2711 = BgL_argsz00_3389;
									goto BgL_loopz00_2710;
								}
							}
						else
							{	/* Ast/venv.scm 212 */
								BgL_tmpz00_3361 = ((bool_t) 0);
							}
					}
				}
				return BBOOL(BgL_tmpz00_3361);
			}
		}

	}



/* &restore-value-types!1255 */
	obj_t BGl_z62restorezd2valuezd2typesz121255z70zzast_envz00(obj_t
		BgL_envz00_2667, obj_t BgL_valuez00_2668, obj_t BgL_idz00_2669)
	{
		{	/* Ast/venv.scm 201 */
			{

				{	/* Ast/venv.scm 201 */
					obj_t BgL_nextzd2method1254zd2_2716;

					BgL_nextzd2method1254zd2_2716 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_sfunz00_bglt) BgL_valuez00_2668)),
						BGl_restorezd2valuezd2typesz12zd2envzc0zzast_envz00,
						BGl_isfunz00zzinline_inlinez00);
					BGL_PROCEDURE_CALL2(BgL_nextzd2method1254zd2_2716,
						((obj_t) ((BgL_sfunz00_bglt) BgL_valuez00_2668)), BgL_idz00_2669);
				}
				{	/* Ast/venv.scm 203 */
					BgL_nodez00_bglt BgL_arg1765z00_2717;

					{
						BgL_isfunz00_bglt BgL_auxz00_3404;

						{
							obj_t BgL_auxz00_3405;

							{	/* Ast/venv.scm 203 */
								BgL_objectz00_bglt BgL_tmpz00_3406;

								BgL_tmpz00_3406 =
									((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_valuez00_2668));
								BgL_auxz00_3405 = BGL_OBJECT_WIDENING(BgL_tmpz00_3406);
							}
							BgL_auxz00_3404 = ((BgL_isfunz00_bglt) BgL_auxz00_3405);
						}
						BgL_arg1765z00_2717 =
							(((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_3404))->
							BgL_originalzd2bodyzd2);
					}
					return BGl_hrtypezd2nodez12zc0zzast_hrtypez00(BgL_arg1765z00_2717);
				}
			}
		}

	}



/* &restore-value-types!1253 */
	obj_t BGl_z62restorezd2valuezd2typesz121253z70zzast_envz00(obj_t
		BgL_envz00_2670, obj_t BgL_valuez00_2671, obj_t BgL_idz00_2672)
	{
		{	/* Ast/venv.scm 167 */
			{

				{	/* Ast/venv.scm 167 */
					obj_t BgL_nextzd2method1252zd2_2720;

					BgL_nextzd2method1252zd2_2720 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_sfunz00_bglt) BgL_valuez00_2671)),
						BGl_restorezd2valuezd2typesz12zd2envzc0zzast_envz00,
						BGl_sfunz00zzast_varz00);
					BGL_PROCEDURE_CALL2(BgL_nextzd2method1252zd2_2720,
						((obj_t) ((BgL_sfunz00_bglt) BgL_valuez00_2671)), BgL_idz00_2672);
				}
				{
					obj_t BgL_argsz00_2722;

					BgL_argsz00_2722 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt) BgL_valuez00_2671)))->BgL_argsz00);
				BgL_loopz00_2721:
					if (PAIRP(BgL_argsz00_2722))
						{	/* Ast/venv.scm 173 */
							obj_t BgL_argz00_2723;

							BgL_argz00_2723 = CAR(BgL_argsz00_2722);
							{	/* Ast/venv.scm 175 */
								bool_t BgL_test2038z00_3426;

								{	/* Ast/venv.scm 175 */
									obj_t BgL_classz00_2724;

									BgL_classz00_2724 = BGl_typez00zztype_typez00;
									if (BGL_OBJECTP(BgL_argz00_2723))
										{	/* Ast/venv.scm 175 */
											BgL_objectz00_bglt BgL_arg1807z00_2725;

											BgL_arg1807z00_2725 =
												(BgL_objectz00_bglt) (BgL_argz00_2723);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Ast/venv.scm 175 */
													long BgL_idxz00_2726;

													BgL_idxz00_2726 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2725);
													BgL_test2038z00_3426 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_2726 + 1L)) == BgL_classz00_2724);
												}
											else
												{	/* Ast/venv.scm 175 */
													bool_t BgL_res1901z00_2729;

													{	/* Ast/venv.scm 175 */
														obj_t BgL_oclassz00_2730;

														{	/* Ast/venv.scm 175 */
															obj_t BgL_arg1815z00_2731;
															long BgL_arg1816z00_2732;

															BgL_arg1815z00_2731 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Ast/venv.scm 175 */
																long BgL_arg1817z00_2733;

																BgL_arg1817z00_2733 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2725);
																BgL_arg1816z00_2732 =
																	(BgL_arg1817z00_2733 - OBJECT_TYPE);
															}
															BgL_oclassz00_2730 =
																VECTOR_REF(BgL_arg1815z00_2731,
																BgL_arg1816z00_2732);
														}
														{	/* Ast/venv.scm 175 */
															bool_t BgL__ortest_1115z00_2734;

															BgL__ortest_1115z00_2734 =
																(BgL_classz00_2724 == BgL_oclassz00_2730);
															if (BgL__ortest_1115z00_2734)
																{	/* Ast/venv.scm 175 */
																	BgL_res1901z00_2729 =
																		BgL__ortest_1115z00_2734;
																}
															else
																{	/* Ast/venv.scm 175 */
																	long BgL_odepthz00_2735;

																	{	/* Ast/venv.scm 175 */
																		obj_t BgL_arg1804z00_2736;

																		BgL_arg1804z00_2736 = (BgL_oclassz00_2730);
																		BgL_odepthz00_2735 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_2736);
																	}
																	if ((1L < BgL_odepthz00_2735))
																		{	/* Ast/venv.scm 175 */
																			obj_t BgL_arg1802z00_2737;

																			{	/* Ast/venv.scm 175 */
																				obj_t BgL_arg1803z00_2738;

																				BgL_arg1803z00_2738 =
																					(BgL_oclassz00_2730);
																				BgL_arg1802z00_2737 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_2738, 1L);
																			}
																			BgL_res1901z00_2729 =
																				(BgL_arg1802z00_2737 ==
																				BgL_classz00_2724);
																		}
																	else
																		{	/* Ast/venv.scm 175 */
																			BgL_res1901z00_2729 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2038z00_3426 = BgL_res1901z00_2729;
												}
										}
									else
										{	/* Ast/venv.scm 175 */
											BgL_test2038z00_3426 = ((bool_t) 0);
										}
								}
								if (BgL_test2038z00_3426)
									{	/* Ast/venv.scm 176 */
										BgL_typez00_bglt BgL_arg1740z00_2739;

										{	/* Ast/venv.scm 176 */
											obj_t BgL_arg1746z00_2740;

											BgL_arg1746z00_2740 =
												(((BgL_typez00_bglt) COBJECT(
														((BgL_typez00_bglt) BgL_argz00_2723)))->BgL_idz00);
											BgL_arg1740z00_2739 =
												BGl_findzd2typezd2zztype_envz00(BgL_arg1746z00_2740);
										}
										{	/* Ast/venv.scm 176 */
											obj_t BgL_tmpz00_3452;

											BgL_tmpz00_3452 = ((obj_t) BgL_arg1740z00_2739);
											SET_CAR(BgL_argsz00_2722, BgL_tmpz00_3452);
										}
									}
								else
									{	/* Ast/venv.scm 177 */
										bool_t BgL_test2043z00_3455;

										{	/* Ast/venv.scm 177 */
											obj_t BgL_classz00_2741;

											BgL_classz00_2741 = BGl_localz00zzast_varz00;
											if (BGL_OBJECTP(BgL_argz00_2723))
												{	/* Ast/venv.scm 177 */
													BgL_objectz00_bglt BgL_arg1807z00_2742;

													BgL_arg1807z00_2742 =
														(BgL_objectz00_bglt) (BgL_argz00_2723);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Ast/venv.scm 177 */
															long BgL_idxz00_2743;

															BgL_idxz00_2743 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2742);
															BgL_test2043z00_3455 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_2743 + 2L)) == BgL_classz00_2741);
														}
													else
														{	/* Ast/venv.scm 177 */
															bool_t BgL_res1902z00_2746;

															{	/* Ast/venv.scm 177 */
																obj_t BgL_oclassz00_2747;

																{	/* Ast/venv.scm 177 */
																	obj_t BgL_arg1815z00_2748;
																	long BgL_arg1816z00_2749;

																	BgL_arg1815z00_2748 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Ast/venv.scm 177 */
																		long BgL_arg1817z00_2750;

																		BgL_arg1817z00_2750 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2742);
																		BgL_arg1816z00_2749 =
																			(BgL_arg1817z00_2750 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_2747 =
																		VECTOR_REF(BgL_arg1815z00_2748,
																		BgL_arg1816z00_2749);
																}
																{	/* Ast/venv.scm 177 */
																	bool_t BgL__ortest_1115z00_2751;

																	BgL__ortest_1115z00_2751 =
																		(BgL_classz00_2741 == BgL_oclassz00_2747);
																	if (BgL__ortest_1115z00_2751)
																		{	/* Ast/venv.scm 177 */
																			BgL_res1902z00_2746 =
																				BgL__ortest_1115z00_2751;
																		}
																	else
																		{	/* Ast/venv.scm 177 */
																			long BgL_odepthz00_2752;

																			{	/* Ast/venv.scm 177 */
																				obj_t BgL_arg1804z00_2753;

																				BgL_arg1804z00_2753 =
																					(BgL_oclassz00_2747);
																				BgL_odepthz00_2752 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_2753);
																			}
																			if ((2L < BgL_odepthz00_2752))
																				{	/* Ast/venv.scm 177 */
																					obj_t BgL_arg1802z00_2754;

																					{	/* Ast/venv.scm 177 */
																						obj_t BgL_arg1803z00_2755;

																						BgL_arg1803z00_2755 =
																							(BgL_oclassz00_2747);
																						BgL_arg1802z00_2754 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_2755, 2L);
																					}
																					BgL_res1902z00_2746 =
																						(BgL_arg1802z00_2754 ==
																						BgL_classz00_2741);
																				}
																			else
																				{	/* Ast/venv.scm 177 */
																					BgL_res1902z00_2746 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2043z00_3455 = BgL_res1902z00_2746;
														}
												}
											else
												{	/* Ast/venv.scm 177 */
													BgL_test2043z00_3455 = ((bool_t) 0);
												}
										}
										if (BgL_test2043z00_3455)
											{	/* Ast/venv.scm 178 */
												BgL_typez00_bglt BgL_newzd2typezd2_2756;

												{	/* Ast/venv.scm 178 */
													obj_t BgL_arg1748z00_2757;

													BgL_arg1748z00_2757 =
														(((BgL_typez00_bglt) COBJECT(
																(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_localz00_bglt)
																					BgL_argz00_2723))))->BgL_typez00)))->
														BgL_idz00);
													BgL_newzd2typezd2_2756 =
														BGl_findzd2typezd2zztype_envz00
														(BgL_arg1748z00_2757);
												}
												((((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt) BgL_argz00_2723))))->
														BgL_typez00) =
													((BgL_typez00_bglt) BgL_newzd2typezd2_2756), BUNSPEC);
											}
										else
											{	/* Ast/venv.scm 177 */
												BGl_errorz00zz__errorz00(BGl_string1926z00zzast_envz00,
													BGl_string1927z00zzast_envz00,
													BGl_shapez00zztools_shapez00(BgL_argz00_2723));
											}
									}
							}
							{
								obj_t BgL_argsz00_3488;

								BgL_argsz00_3488 = CDR(BgL_argsz00_2722);
								BgL_argsz00_2722 = BgL_argsz00_3488;
								goto BgL_loopz00_2721;
							}
						}
					else
						{	/* Ast/venv.scm 172 */
							if (NULLP(BgL_argsz00_2722))
								{	/* Ast/venv.scm 186 */
									obj_t BgL_bodyz00_2758;

									BgL_bodyz00_2758 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt) BgL_valuez00_2671)))->BgL_bodyz00);
									{	/* Ast/venv.scm 188 */
										bool_t BgL_test2049z00_3494;

										{	/* Ast/venv.scm 188 */
											obj_t BgL_classz00_2759;

											BgL_classz00_2759 = BGl_nodez00zzast_nodez00;
											if (BGL_OBJECTP(BgL_bodyz00_2758))
												{	/* Ast/venv.scm 188 */
													BgL_objectz00_bglt BgL_arg1807z00_2760;

													BgL_arg1807z00_2760 =
														(BgL_objectz00_bglt) (BgL_bodyz00_2758);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Ast/venv.scm 188 */
															long BgL_idxz00_2761;

															BgL_idxz00_2761 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2760);
															BgL_test2049z00_3494 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_2761 + 1L)) == BgL_classz00_2759);
														}
													else
														{	/* Ast/venv.scm 188 */
															bool_t BgL_res1903z00_2764;

															{	/* Ast/venv.scm 188 */
																obj_t BgL_oclassz00_2765;

																{	/* Ast/venv.scm 188 */
																	obj_t BgL_arg1815z00_2766;
																	long BgL_arg1816z00_2767;

																	BgL_arg1815z00_2766 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Ast/venv.scm 188 */
																		long BgL_arg1817z00_2768;

																		BgL_arg1817z00_2768 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2760);
																		BgL_arg1816z00_2767 =
																			(BgL_arg1817z00_2768 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_2765 =
																		VECTOR_REF(BgL_arg1815z00_2766,
																		BgL_arg1816z00_2767);
																}
																{	/* Ast/venv.scm 188 */
																	bool_t BgL__ortest_1115z00_2769;

																	BgL__ortest_1115z00_2769 =
																		(BgL_classz00_2759 == BgL_oclassz00_2765);
																	if (BgL__ortest_1115z00_2769)
																		{	/* Ast/venv.scm 188 */
																			BgL_res1903z00_2764 =
																				BgL__ortest_1115z00_2769;
																		}
																	else
																		{	/* Ast/venv.scm 188 */
																			long BgL_odepthz00_2770;

																			{	/* Ast/venv.scm 188 */
																				obj_t BgL_arg1804z00_2771;

																				BgL_arg1804z00_2771 =
																					(BgL_oclassz00_2765);
																				BgL_odepthz00_2770 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_2771);
																			}
																			if ((1L < BgL_odepthz00_2770))
																				{	/* Ast/venv.scm 188 */
																					obj_t BgL_arg1802z00_2772;

																					{	/* Ast/venv.scm 188 */
																						obj_t BgL_arg1803z00_2773;

																						BgL_arg1803z00_2773 =
																							(BgL_oclassz00_2765);
																						BgL_arg1802z00_2772 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_2773, 1L);
																					}
																					BgL_res1903z00_2764 =
																						(BgL_arg1802z00_2772 ==
																						BgL_classz00_2759);
																				}
																			else
																				{	/* Ast/venv.scm 188 */
																					BgL_res1903z00_2764 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2049z00_3494 = BgL_res1903z00_2764;
														}
												}
											else
												{	/* Ast/venv.scm 188 */
													BgL_test2049z00_3494 = ((bool_t) 0);
												}
										}
										if (BgL_test2049z00_3494)
											{	/* Ast/venv.scm 189 */
												BgL_typez00_bglt BgL_tresz00_2774;

												BgL_tresz00_2774 =
													(((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt) BgL_bodyz00_2758)))->
													BgL_typez00);
												BGl_hrtypezd2nodez12zc0zzast_hrtypez00((
														(BgL_nodez00_bglt) BgL_bodyz00_2758));
												{	/* Ast/venv.scm 191 */
													bool_t BgL_test2054z00_3521;

													{	/* Ast/venv.scm 191 */
														obj_t BgL_classz00_2775;

														BgL_classz00_2775 = BGl_typez00zztype_typez00;
														{	/* Ast/venv.scm 191 */
															BgL_objectz00_bglt BgL_arg1807z00_2776;

															{	/* Ast/venv.scm 191 */
																obj_t BgL_tmpz00_3522;

																BgL_tmpz00_3522 =
																	((obj_t)
																	((BgL_objectz00_bglt) BgL_tresz00_2774));
																BgL_arg1807z00_2776 =
																	(BgL_objectz00_bglt) (BgL_tmpz00_3522);
															}
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Ast/venv.scm 191 */
																	long BgL_idxz00_2777;

																	BgL_idxz00_2777 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_2776);
																	BgL_test2054z00_3521 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_2777 + 1L)) ==
																		BgL_classz00_2775);
																}
															else
																{	/* Ast/venv.scm 191 */
																	bool_t BgL_res1904z00_2780;

																	{	/* Ast/venv.scm 191 */
																		obj_t BgL_oclassz00_2781;

																		{	/* Ast/venv.scm 191 */
																			obj_t BgL_arg1815z00_2782;
																			long BgL_arg1816z00_2783;

																			BgL_arg1815z00_2782 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Ast/venv.scm 191 */
																				long BgL_arg1817z00_2784;

																				BgL_arg1817z00_2784 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_2776);
																				BgL_arg1816z00_2783 =
																					(BgL_arg1817z00_2784 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_2781 =
																				VECTOR_REF(BgL_arg1815z00_2782,
																				BgL_arg1816z00_2783);
																		}
																		{	/* Ast/venv.scm 191 */
																			bool_t BgL__ortest_1115z00_2785;

																			BgL__ortest_1115z00_2785 =
																				(BgL_classz00_2775 ==
																				BgL_oclassz00_2781);
																			if (BgL__ortest_1115z00_2785)
																				{	/* Ast/venv.scm 191 */
																					BgL_res1904z00_2780 =
																						BgL__ortest_1115z00_2785;
																				}
																			else
																				{	/* Ast/venv.scm 191 */
																					long BgL_odepthz00_2786;

																					{	/* Ast/venv.scm 191 */
																						obj_t BgL_arg1804z00_2787;

																						BgL_arg1804z00_2787 =
																							(BgL_oclassz00_2781);
																						BgL_odepthz00_2786 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_2787);
																					}
																					if ((1L < BgL_odepthz00_2786))
																						{	/* Ast/venv.scm 191 */
																							obj_t BgL_arg1802z00_2788;

																							{	/* Ast/venv.scm 191 */
																								obj_t BgL_arg1803z00_2789;

																								BgL_arg1803z00_2789 =
																									(BgL_oclassz00_2781);
																								BgL_arg1802z00_2788 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_2789, 1L);
																							}
																							BgL_res1904z00_2780 =
																								(BgL_arg1802z00_2788 ==
																								BgL_classz00_2775);
																						}
																					else
																						{	/* Ast/venv.scm 191 */
																							BgL_res1904z00_2780 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2054z00_3521 = BgL_res1904z00_2780;
																}
														}
													}
													if (BgL_test2054z00_3521)
														{	/* Ast/venv.scm 192 */
															BgL_typez00_bglt BgL_arg1755z00_2790;

															BgL_arg1755z00_2790 =
																BGl_findzd2typezd2zztype_envz00(
																(((BgL_typez00_bglt)
																		COBJECT(BgL_tresz00_2774))->BgL_idz00));
															return ((((BgL_nodez00_bglt)
																		COBJECT(((BgL_nodez00_bglt)
																				BgL_bodyz00_2758)))->BgL_typez00) =
																((BgL_typez00_bglt) BgL_arg1755z00_2790),
																BUNSPEC);
														}
													else
														{	/* Ast/venv.scm 191 */
															return BFALSE;
														}
												}
											}
										else
											{	/* Ast/venv.scm 188 */
												return BFALSE;
											}
									}
								}
							else
								{	/* Ast/venv.scm 185 */
									return
										BGl_errorz00zz__errorz00(BGl_string1928z00zzast_envz00,
										BGl_string1929z00zzast_envz00,
										BGl_shapez00zztools_shapez00(BgL_argsz00_2722));
								}
						}
				}
			}
		}

	}



/* &restore-value-types!1251 */
	obj_t BGl_z62restorezd2valuezd2typesz121251z70zzast_envz00(obj_t
		BgL_envz00_2673, obj_t BgL_valuez00_2674, obj_t BgL_idz00_2675)
	{
		{	/* Ast/venv.scm 159 */
			{	/* Ast/venv.scm 161 */
				bool_t BgL_test2058z00_3553;

				{	/* Ast/venv.scm 161 */
					obj_t BgL_arg1735z00_2792;

					BgL_arg1735z00_2792 =
						(((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt) BgL_valuez00_2674)))->BgL_predicatezd2ofzd2);
					{	/* Ast/venv.scm 161 */
						obj_t BgL_classz00_2793;

						BgL_classz00_2793 = BGl_typez00zztype_typez00;
						if (BGL_OBJECTP(BgL_arg1735z00_2792))
							{	/* Ast/venv.scm 161 */
								BgL_objectz00_bglt BgL_arg1807z00_2794;

								BgL_arg1807z00_2794 =
									(BgL_objectz00_bglt) (BgL_arg1735z00_2792);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Ast/venv.scm 161 */
										long BgL_idxz00_2795;

										BgL_idxz00_2795 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2794);
										BgL_test2058z00_3553 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2795 + 1L)) == BgL_classz00_2793);
									}
								else
									{	/* Ast/venv.scm 161 */
										bool_t BgL_res1900z00_2798;

										{	/* Ast/venv.scm 161 */
											obj_t BgL_oclassz00_2799;

											{	/* Ast/venv.scm 161 */
												obj_t BgL_arg1815z00_2800;
												long BgL_arg1816z00_2801;

												BgL_arg1815z00_2800 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Ast/venv.scm 161 */
													long BgL_arg1817z00_2802;

													BgL_arg1817z00_2802 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2794);
													BgL_arg1816z00_2801 =
														(BgL_arg1817z00_2802 - OBJECT_TYPE);
												}
												BgL_oclassz00_2799 =
													VECTOR_REF(BgL_arg1815z00_2800, BgL_arg1816z00_2801);
											}
											{	/* Ast/venv.scm 161 */
												bool_t BgL__ortest_1115z00_2803;

												BgL__ortest_1115z00_2803 =
													(BgL_classz00_2793 == BgL_oclassz00_2799);
												if (BgL__ortest_1115z00_2803)
													{	/* Ast/venv.scm 161 */
														BgL_res1900z00_2798 = BgL__ortest_1115z00_2803;
													}
												else
													{	/* Ast/venv.scm 161 */
														long BgL_odepthz00_2804;

														{	/* Ast/venv.scm 161 */
															obj_t BgL_arg1804z00_2805;

															BgL_arg1804z00_2805 = (BgL_oclassz00_2799);
															BgL_odepthz00_2804 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2805);
														}
														if ((1L < BgL_odepthz00_2804))
															{	/* Ast/venv.scm 161 */
																obj_t BgL_arg1802z00_2806;

																{	/* Ast/venv.scm 161 */
																	obj_t BgL_arg1803z00_2807;

																	BgL_arg1803z00_2807 = (BgL_oclassz00_2799);
																	BgL_arg1802z00_2806 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2807,
																		1L);
																}
																BgL_res1900z00_2798 =
																	(BgL_arg1802z00_2806 == BgL_classz00_2793);
															}
														else
															{	/* Ast/venv.scm 161 */
																BgL_res1900z00_2798 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2058z00_3553 = BgL_res1900z00_2798;
									}
							}
						else
							{	/* Ast/venv.scm 161 */
								BgL_test2058z00_3553 = ((bool_t) 0);
							}
					}
				}
				if (BgL_test2058z00_3553)
					{
						obj_t BgL_auxz00_3578;

						{	/* Ast/venv.scm 162 */
							obj_t BgL_arg1733z00_2808;

							BgL_arg1733z00_2808 =
								(((BgL_typez00_bglt) COBJECT(
										((BgL_typez00_bglt)
											(((BgL_funz00_bglt) COBJECT(
														((BgL_funz00_bglt) BgL_valuez00_2674)))->
												BgL_predicatezd2ofzd2))))->BgL_idz00);
							BgL_auxz00_3578 =
								((obj_t) BGl_findzd2typezd2zztype_envz00(BgL_arg1733z00_2808));
						}
						return
							((((BgL_funz00_bglt) COBJECT(
										((BgL_funz00_bglt) BgL_valuez00_2674)))->
								BgL_predicatezd2ofzd2) = ((obj_t) BgL_auxz00_3578), BUNSPEC);
					}
				else
					{	/* Ast/venv.scm 161 */
						return BFALSE;
					}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_envz00(void)
	{
		{	/* Ast/venv.scm 14 */
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1930z00zzast_envz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1930z00zzast_envz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1930z00zzast_envz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1930z00zzast_envz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1930z00zzast_envz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1930z00zzast_envz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1930z00zzast_envz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1930z00zzast_envz00));
			BGl_modulezd2initializa7ationz75zzast_hrtypez00(156957722L,
				BSTRING_TO_STRING(BGl_string1930z00zzast_envz00));
			BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string1930z00zzast_envz00));
			BGl_modulezd2initializa7ationz75zzread_jvmz00(261574382L,
				BSTRING_TO_STRING(BGl_string1930z00zzast_envz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1930z00zzast_envz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1930z00zzast_envz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1930z00zzast_envz00));
			return
				BGl_modulezd2initializa7ationz75zzinline_inlinez00(20504962L,
				BSTRING_TO_STRING(BGl_string1930z00zzast_envz00));
		}

	}

#ifdef __cplusplus
}
#endif
