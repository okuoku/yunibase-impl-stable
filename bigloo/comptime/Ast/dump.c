/*===========================================================================*/
/*   (Ast/dump.scm)                                                          */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/dump.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_AST_DUMP_TYPE_DEFINITIONS
#define BGL_AST_DUMP_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_patchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
		struct BgL_varz00_bgl *BgL_refz00;
		long BgL_indexz00;
		obj_t BgL_patchidz00;
	}               *BgL_patchz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_pragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_formatz00;
		obj_t BgL_srfi0z00;
	}                *BgL_pragmaz00_bglt;

	typedef struct BgL_genpatchidz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		long BgL_indexz00;
		long BgL_rindexz00;
	}                    *BgL_genpatchidz00_bglt;

	typedef struct BgL_getfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_getfieldz00_bglt;

	typedef struct BgL_setfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_setfieldz00_bglt;

	typedef struct BgL_newz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_argszd2typezd2;
	}             *BgL_newz00_bglt;

	typedef struct BgL_vallocz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                *BgL_vallocz00_bglt;

	typedef struct BgL_vrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}              *BgL_vrefz00_bglt;

	typedef struct BgL_vsetz12z12_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}                 *BgL_vsetz12z12_bglt;

	typedef struct BgL_vlengthz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_ftypez00;
	}                 *BgL_vlengthz00_bglt;

	typedef struct BgL_instanceofz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_classz00;
	}                    *BgL_instanceofz00_bglt;

	typedef struct BgL_castzd2nullzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
	}                     *BgL_castzd2nullzd2_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_retblockz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                  *BgL_retblockz00_bglt;

	typedef struct BgL_returnz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_retblockz00_bgl *BgL_blockz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                *BgL_returnz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;


#endif													// BGL_AST_DUMP_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62nodezd2ze3sexpzd2vref1380z81zzast_dumpz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	extern obj_t BGl_returnz00zzast_nodez00;
	static obj_t BGl_requirezd2initializa7ationz75zzast_dumpz00 = BUNSPEC;
	extern obj_t BGl_makezd2typedzd2identz00zzast_identz00(obj_t, obj_t);
	extern obj_t BGl_closurez00zzast_nodez00;
	extern obj_t BGl_za2accesszd2shapezf3za2z21zzengine_paramz00;
	static obj_t BGl_z62nodezd2ze3sexp1346z53zzast_dumpz00(obj_t, obj_t);
	extern obj_t BGl_newz00zzast_nodez00;
	static obj_t BGl_z62nodezd2ze3sexpzd2valloc1384z81zzast_dumpz00(obj_t, obj_t);
	extern obj_t BGl_vlengthz00zzast_nodez00;
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62nodezd2ze3sexpzd2atom1349z81zzast_dumpz00(obj_t, obj_t);
	static obj_t BGl_z62nodezd2ze3sexpzd2return1414z81zzast_dumpz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(obj_t);
	static obj_t BGl_z62nodezd2ze3sexpzd2getfield1369z81zzast_dumpz00(obj_t,
		obj_t);
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t BGl_toplevelzd2initzd2zzast_dumpz00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static obj_t BGl_z62nodezd2ze3sexpzd2new1374z81zzast_dumpz00(obj_t, obj_t);
	static obj_t BGl_z62nodezd2ze3sexpzd2appzd2ly1363z53zzast_dumpz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t BGl_za2typezd2shapezf3za2z21zzengine_paramz00;
	extern obj_t BGl_locationzd2shapezd2zztools_locationz00(obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzast_dumpz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	extern obj_t BGl_getfieldz00zzast_nodez00;
	static obj_t BGl_objectzd2initzd2zzast_dumpz00(void);
	extern obj_t BGl_castz00zzast_nodez00;
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	static obj_t BGl_z62nodezd2ze3sexpzd2setfield1371z81zzast_dumpz00(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2ze3sexpzd2makezd2box1416z53zzast_dumpz00(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2ze3sexpzd2boxzd2setz121420z41zzast_dumpz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd2ze3sexpzd2var1351z81zzast_dumpz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzast_dumpz00(void);
	static obj_t BGl_z62nodezd2ze3sexpzd2letzd2fun1401z53zzast_dumpz00(obj_t,
		obj_t);
	extern obj_t BGl_argszd2listzd2ze3argsza2z41zztools_argsz00(obj_t, obj_t);
	static obj_t BGl_z62nodezd2ze3sexpzd2switch1399z81zzast_dumpz00(obj_t, obj_t);
	static obj_t BGl_z62nodezd2ze3sexpzd2vlength1377z81zzast_dumpz00(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2ze3sexpzd2condition1394z81zzast_dumpz00(obj_t,
		obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	static obj_t BGl_z62nodezd2ze3sexpzd2jumpzd2exzd2i1408z81zzast_dumpz00(obj_t,
		obj_t);
	extern BgL_typez00_bglt BGl_getzd2typezd2zztype_typeofz00(BgL_nodez00_bglt,
		bool_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t BGl_z62nodezd2ze3sexpzd2vsetz121382z93zzast_dumpz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_pragmaz00zzast_nodez00;
	static obj_t BGl_z62nodezd2ze3sexpzd2instanceo1386z81zzast_dumpz00(obj_t,
		obj_t);
	extern obj_t BGl_za2alloczd2shapezf3za2z21zzengine_paramz00;
	static obj_t BGl_z62nodezd2ze3sexpzd2setzd2exzd2it1406z81zzast_dumpz00(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2ze3sexpzd2app1361z81zzast_dumpz00(obj_t, obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	extern obj_t BGl_za2typenodezd2shapezf3za2z21zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_effectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_argsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_cfunz00zzast_varz00;
	static obj_t BGl_z62nodezd2ze3sexpzd2sync1359z81zzast_dumpz00(obj_t, obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t BGl_z62nodezd2ze3sexpzd2cast1390z81zzast_dumpz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	extern obj_t BGl_vrefz00zzast_nodez00;
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	BGL_IMPORT obj_t create_struct(obj_t, int);
	static obj_t BGl_z62nodezd2ze3sexpzd2sequence1357z81zzast_dumpz00(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2ze3sexpzd2pragma1367z81zzast_dumpz00(obj_t, obj_t);
	static obj_t BGl_z62nodezd2ze3sexpzd2setq1392z81zzast_dumpz00(obj_t, obj_t);
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzast_dumpz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_dumpz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzast_dumpz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzast_dumpz00(void);
	static obj_t BGl_z62nodezd2ze3sexpzd2fail1396z81zzast_dumpz00(obj_t, obj_t);
	extern bool_t BGl_sidezd2effectzf3z21zzeffect_effectz00(BgL_nodez00_bglt);
	extern obj_t BGl_vallocz00zzast_nodez00;
	static obj_t BGl_z62nodezd2ze3sexpzd2letzd2var1403z53zzast_dumpz00(obj_t,
		obj_t);
	extern obj_t BGl_retblockz00zzast_nodez00;
	extern obj_t BGl_instanceofz00zzast_nodez00;
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	static obj_t BGl_z62nodezd2ze3sexpzd2retblock1412z81zzast_dumpz00(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2ze3sexpzd2boxzd2ref1418z53zzast_dumpz00(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2ze3sexpzd2castzd2null1388z53zzast_dumpz00(obj_t,
		obj_t);
	extern obj_t BGl_setfieldz00zzast_nodez00;
	extern obj_t BGl_castzd2nullzd2zzast_nodez00;
	static obj_t BGl_shapezd2typedzd2nodez00zzast_dumpz00(obj_t,
		BgL_typez00_bglt);
	extern obj_t BGl_genpatchidz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd2ze3sexpzd2genpatchi1426z81zzast_dumpz00(obj_t,
		obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	static obj_t BGl_z62nodezd2ze3sexpzd2funcall1365z81zzast_dumpz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_nodezd2ze3sexpz31zzast_dumpz00(BgL_nodez00_bglt);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t BGl_z62nodezd2ze3sexpzd2kwote1355z81zzast_dumpz00(obj_t, obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static obj_t BGl_z62nodezd2ze3sexpzd2closure1353z81zzast_dumpz00(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2ze3sexpz53zzast_dumpz00(obj_t, obj_t);
	static obj_t BGl_z62nodezd2ze3sexpzd2patch1423z81zzast_dumpz00(obj_t, obj_t);
	extern obj_t BGl_vsetz12z12zzast_nodez00;
	extern obj_t BGl_patchz00zzast_nodez00;
	static obj_t __cnst[53];


	   
		 
		DEFINE_STRING(BGl_string2407z00zzast_dumpz00,
		BgL_bgl_string2407za700za7za7a2456za7, "::", 2);
	      DEFINE_STRING(BGl_string2409z00zzast_dumpz00,
		BgL_bgl_string2409za700za7za7a2457za7, "node->sexp1346", 14);
	      DEFINE_STRING(BGl_string2410z00zzast_dumpz00,
		BgL_bgl_string2410za700za7za7a2458za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string2412z00zzast_dumpz00,
		BgL_bgl_string2412za700za7za7a2459za7, "node->sexp", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2408z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2460za7,
		BGl_z62nodezd2ze3sexp1346z53zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2411z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2461za7,
		BGl_z62nodezd2ze3sexpzd2atom1349z81zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2413z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2462za7,
		BGl_z62nodezd2ze3sexpzd2var1351z81zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2414z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2463za7,
		BGl_z62nodezd2ze3sexpzd2closure1353z81zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2415z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2464za7,
		BGl_z62nodezd2ze3sexpzd2kwote1355z81zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2416z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2465za7,
		BGl_z62nodezd2ze3sexpzd2sequence1357z81zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2417z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2466za7,
		BGl_z62nodezd2ze3sexpzd2sync1359z81zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2418z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2467za7,
		BGl_z62nodezd2ze3sexpzd2app1361z81zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2419z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2468za7,
		BGl_z62nodezd2ze3sexpzd2appzd2ly1363z53zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2420z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2469za7,
		BGl_z62nodezd2ze3sexpzd2funcall1365z81zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2421z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2470za7,
		BGl_z62nodezd2ze3sexpzd2pragma1367z81zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2422z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2471za7,
		BGl_z62nodezd2ze3sexpzd2getfield1369z81zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2423z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2472za7,
		BGl_z62nodezd2ze3sexpzd2setfield1371z81zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2424z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2473za7,
		BGl_z62nodezd2ze3sexpzd2new1374z81zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2425z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2474za7,
		BGl_z62nodezd2ze3sexpzd2vlength1377z81zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2426z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2475za7,
		BGl_z62nodezd2ze3sexpzd2vref1380z81zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2427z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2476za7,
		BGl_z62nodezd2ze3sexpzd2vsetz121382z93zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2428z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2477za7,
		BGl_z62nodezd2ze3sexpzd2valloc1384z81zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2429z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2478za7,
		BGl_z62nodezd2ze3sexpzd2instanceo1386z81zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2430z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2479za7,
		BGl_z62nodezd2ze3sexpzd2castzd2null1388z53zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2431z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2480za7,
		BGl_z62nodezd2ze3sexpzd2cast1390z81zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2432z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2481za7,
		BGl_z62nodezd2ze3sexpzd2setq1392z81zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2433z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2482za7,
		BGl_z62nodezd2ze3sexpzd2condition1394z81zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2434z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2483za7,
		BGl_z62nodezd2ze3sexpzd2fail1396z81zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2435z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2484za7,
		BGl_z62nodezd2ze3sexpzd2switch1399z81zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2436z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2485za7,
		BGl_z62nodezd2ze3sexpzd2letzd2fun1401z53zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2437z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2486za7,
		BGl_z62nodezd2ze3sexpzd2letzd2var1403z53zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2438z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2487za7,
		BGl_z62nodezd2ze3sexpzd2setzd2exzd2it1406z81zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2439z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2488za7,
		BGl_z62nodezd2ze3sexpzd2jumpzd2exzd2i1408z81zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2447z00zzast_dumpz00,
		BgL_bgl_string2447za700za7za7a2489za7, " stackable: ", 12);
	      DEFINE_STRING(BGl_string2448z00zzast_dumpz00,
		BgL_bgl_string2448za700za7za7a2490za7, "]", 1);
	      DEFINE_STRING(BGl_string2449z00zzast_dumpz00,
		BgL_bgl_string2449za700za7za7a2491za7, "[::", 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2440z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2492za7,
		BGl_z62nodezd2ze3sexpzd2retblock1412z81zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2441z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2493za7,
		BGl_z62nodezd2ze3sexpzd2return1414z81zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2442z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2494za7,
		BGl_z62nodezd2ze3sexpzd2makezd2box1416z53zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2443z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2495za7,
		BGl_z62nodezd2ze3sexpzd2boxzd2ref1418z53zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2450z00zzast_dumpz00,
		BgL_bgl_string2450za700za7za7a2496za7, "valloc::", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2444z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2497za7,
		BGl_z62nodezd2ze3sexpzd2boxzd2setz121420z41zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2451z00zzast_dumpz00,
		BgL_bgl_string2451za700za7za7a2498za7, "ftype:", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2445z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2499za7,
		BGl_z62nodezd2ze3sexpzd2patch1423z81zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2452z00zzast_dumpz00,
		BgL_bgl_string2452za700za7za7a2500za7, "~a[::~a]", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2446z00zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2501za7,
		BGl_z62nodezd2ze3sexpzd2genpatchi1426z81zzast_dumpz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2453z00zzast_dumpz00,
		BgL_bgl_string2453za700za7za7a2502za7, "ast_dump", 8);
	      DEFINE_STRING(BGl_string2454z00zzast_dumpz00,
		BgL_bgl_string2454za700za7za7a2503za7,
		"(quote ()) closure quote a-tvector begin unsafe :prelock synchronize :args-type :type app-stackable side-effect? node apply funcall funcall-el elight funcall-l light free-pragma pragma getfield setfield new vlength vref vref-ur vset! vset-ur! isa? |cast-null::| cast set! if failure case :stackable labels :side-effect :removable? let :body :onexit set-exit jump-exit retblock return make-box box-ref box-set! patch genpatchid node->sexp1346 ",
		442);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00,
		BgL_bgl_za762nodeza7d2za7e3sex2504za7,
		BGl_z62nodezd2ze3sexpz53zzast_dumpz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzast_dumpz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long
		BgL_checksumz00_3872, char *BgL_fromz00_3873)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_dumpz00))
				{
					BGl_requirezd2initializa7ationz75zzast_dumpz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_dumpz00();
					BGl_libraryzd2moduleszd2initz00zzast_dumpz00();
					BGl_cnstzd2initzd2zzast_dumpz00();
					BGl_importedzd2moduleszd2initz00zzast_dumpz00();
					BGl_genericzd2initzd2zzast_dumpz00();
					BGl_methodzd2initzd2zzast_dumpz00();
					return BGl_toplevelzd2initzd2zzast_dumpz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_dumpz00(void)
	{
		{	/* Ast/dump.scm 14 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_dump");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "ast_dump");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "ast_dump");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "ast_dump");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "ast_dump");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_dump");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "ast_dump");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "ast_dump");
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(0L, "ast_dump");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L, "ast_dump");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "ast_dump");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"ast_dump");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "ast_dump");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "ast_dump");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "ast_dump");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_dumpz00(void)
	{
		{	/* Ast/dump.scm 14 */
			{	/* Ast/dump.scm 14 */
				obj_t BgL_cportz00_3133;

				{	/* Ast/dump.scm 14 */
					obj_t BgL_stringz00_3140;

					BgL_stringz00_3140 = BGl_string2454z00zzast_dumpz00;
					{	/* Ast/dump.scm 14 */
						obj_t BgL_startz00_3141;

						BgL_startz00_3141 = BINT(0L);
						{	/* Ast/dump.scm 14 */
							obj_t BgL_endz00_3142;

							BgL_endz00_3142 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3140)));
							{	/* Ast/dump.scm 14 */

								BgL_cportz00_3133 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3140, BgL_startz00_3141, BgL_endz00_3142);
				}}}}
				{
					long BgL_iz00_3134;

					BgL_iz00_3134 = 52L;
				BgL_loopz00_3135:
					if ((BgL_iz00_3134 == -1L))
						{	/* Ast/dump.scm 14 */
							return BUNSPEC;
						}
					else
						{	/* Ast/dump.scm 14 */
							{	/* Ast/dump.scm 14 */
								obj_t BgL_arg2455z00_3136;

								{	/* Ast/dump.scm 14 */

									{	/* Ast/dump.scm 14 */
										obj_t BgL_locationz00_3138;

										BgL_locationz00_3138 = BBOOL(((bool_t) 0));
										{	/* Ast/dump.scm 14 */

											BgL_arg2455z00_3136 =
												BGl_readz00zz__readerz00(BgL_cportz00_3133,
												BgL_locationz00_3138);
										}
									}
								}
								{	/* Ast/dump.scm 14 */
									int BgL_tmpz00_3908;

									BgL_tmpz00_3908 = (int) (BgL_iz00_3134);
									CNST_TABLE_SET(BgL_tmpz00_3908, BgL_arg2455z00_3136);
							}}
							{	/* Ast/dump.scm 14 */
								int BgL_auxz00_3139;

								BgL_auxz00_3139 = (int) ((BgL_iz00_3134 - 1L));
								{
									long BgL_iz00_3913;

									BgL_iz00_3913 = (long) (BgL_auxz00_3139);
									BgL_iz00_3134 = BgL_iz00_3913;
									goto BgL_loopz00_3135;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_dumpz00(void)
	{
		{	/* Ast/dump.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzast_dumpz00(void)
	{
		{	/* Ast/dump.scm 14 */
			return BUNSPEC;
		}

	}



/* shape-typed-node */
	obj_t BGl_shapezd2typedzd2nodez00zzast_dumpz00(obj_t BgL_idz00_48,
		BgL_typez00_bglt BgL_typez00_49)
	{
		{	/* Ast/dump.scm 440 */
			if (CBOOL(BGl_za2typenodezd2shapezf3za2z21zzengine_paramz00))
				{	/* Ast/dump.scm 442 */
					obj_t BgL_arg1489z00_1412;

					{	/* Ast/dump.scm 442 */
						obj_t BgL_arg1502z00_1413;
						obj_t BgL_arg1509z00_1414;

						{	/* Ast/dump.scm 442 */
							obj_t BgL_arg1455z00_2497;

							BgL_arg1455z00_2497 = SYMBOL_TO_STRING(BgL_idz00_48);
							BgL_arg1502z00_1413 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_2497);
						}
						BgL_arg1509z00_1414 =
							BGl_shapez00zztools_shapez00(((obj_t) BgL_typez00_49));
						BgL_arg1489z00_1412 =
							string_append_3(BgL_arg1502z00_1413,
							BGl_string2407z00zzast_dumpz00, BgL_arg1509z00_1414);
					}
					return bstring_to_symbol(BgL_arg1489z00_1412);
				}
			else
				{	/* Ast/dump.scm 441 */
					return BgL_idz00_48;
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_dumpz00(void)
	{
		{	/* Ast/dump.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_dumpz00(void)
	{
		{	/* Ast/dump.scm 14 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00, BGl_proc2408z00zzast_dumpz00,
				BGl_nodez00zzast_nodez00, BGl_string2409z00zzast_dumpz00);
		}

	}



/* &node->sexp1346 */
	obj_t BGl_z62nodezd2ze3sexp1346z53zzast_dumpz00(obj_t BgL_envz00_3023,
		obj_t BgL_nodez00_3024)
	{
		{	/* Ast/dump.scm 37 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(0),
				BGl_string2410z00zzast_dumpz00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_3024)));
		}

	}



/* node->sexp */
	BGL_EXPORTED_DEF obj_t BGl_nodezd2ze3sexpz31zzast_dumpz00(BgL_nodez00_bglt
		BgL_nodez00_14)
	{
		{	/* Ast/dump.scm 37 */
			{	/* Ast/dump.scm 37 */
				obj_t BgL_method1347z00_1419;

				{	/* Ast/dump.scm 37 */
					obj_t BgL_res2398z00_2529;

					{	/* Ast/dump.scm 37 */
						long BgL_objzd2classzd2numz00_2500;

						BgL_objzd2classzd2numz00_2500 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_14));
						{	/* Ast/dump.scm 37 */
							obj_t BgL_arg1811z00_2501;

							BgL_arg1811z00_2501 =
								PROCEDURE_REF(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00,
								(int) (1L));
							{	/* Ast/dump.scm 37 */
								int BgL_offsetz00_2504;

								BgL_offsetz00_2504 = (int) (BgL_objzd2classzd2numz00_2500);
								{	/* Ast/dump.scm 37 */
									long BgL_offsetz00_2505;

									BgL_offsetz00_2505 =
										((long) (BgL_offsetz00_2504) - OBJECT_TYPE);
									{	/* Ast/dump.scm 37 */
										long BgL_modz00_2506;

										BgL_modz00_2506 =
											(BgL_offsetz00_2505 >> (int) ((long) ((int) (4L))));
										{	/* Ast/dump.scm 37 */
											long BgL_restz00_2508;

											BgL_restz00_2508 =
												(BgL_offsetz00_2505 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/dump.scm 37 */

												{	/* Ast/dump.scm 37 */
													obj_t BgL_bucketz00_2510;

													BgL_bucketz00_2510 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2501), BgL_modz00_2506);
													BgL_res2398z00_2529 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2510), BgL_restz00_2508);
					}}}}}}}}
					BgL_method1347z00_1419 = BgL_res2398z00_2529;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1347z00_1419, ((obj_t) BgL_nodez00_14));
			}
		}

	}



/* &node->sexp */
	obj_t BGl_z62nodezd2ze3sexpz53zzast_dumpz00(obj_t BgL_envz00_3025,
		obj_t BgL_nodez00_3026)
	{
		{	/* Ast/dump.scm 37 */
			return
				BGl_nodezd2ze3sexpz31zzast_dumpz00(
				((BgL_nodez00_bglt) BgL_nodez00_3026));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_dumpz00(void)
	{
		{	/* Ast/dump.scm 14 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00, BGl_atomz00zzast_nodez00,
				BGl_proc2411z00zzast_dumpz00, BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00, BGl_varz00zzast_nodez00,
				BGl_proc2413z00zzast_dumpz00, BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00, BGl_closurez00zzast_nodez00,
				BGl_proc2414z00zzast_dumpz00, BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00, BGl_kwotez00zzast_nodez00,
				BGl_proc2415z00zzast_dumpz00, BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00, BGl_sequencez00zzast_nodez00,
				BGl_proc2416z00zzast_dumpz00, BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00, BGl_syncz00zzast_nodez00,
				BGl_proc2417z00zzast_dumpz00, BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00, BGl_appz00zzast_nodez00,
				BGl_proc2418z00zzast_dumpz00, BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00, BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc2419z00zzast_dumpz00, BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00, BGl_funcallz00zzast_nodez00,
				BGl_proc2420z00zzast_dumpz00, BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00, BGl_pragmaz00zzast_nodez00,
				BGl_proc2421z00zzast_dumpz00, BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00, BGl_getfieldz00zzast_nodez00,
				BGl_proc2422z00zzast_dumpz00, BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00, BGl_setfieldz00zzast_nodez00,
				BGl_proc2423z00zzast_dumpz00, BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00, BGl_newz00zzast_nodez00,
				BGl_proc2424z00zzast_dumpz00, BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00, BGl_vlengthz00zzast_nodez00,
				BGl_proc2425z00zzast_dumpz00, BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00, BGl_vrefz00zzast_nodez00,
				BGl_proc2426z00zzast_dumpz00, BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00, BGl_vsetz12z12zzast_nodez00,
				BGl_proc2427z00zzast_dumpz00, BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00, BGl_vallocz00zzast_nodez00,
				BGl_proc2428z00zzast_dumpz00, BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00,
				BGl_instanceofz00zzast_nodez00, BGl_proc2429z00zzast_dumpz00,
				BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00,
				BGl_castzd2nullzd2zzast_nodez00, BGl_proc2430z00zzast_dumpz00,
				BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00, BGl_castz00zzast_nodez00,
				BGl_proc2431z00zzast_dumpz00, BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00, BGl_setqz00zzast_nodez00,
				BGl_proc2432z00zzast_dumpz00, BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc2433z00zzast_dumpz00,
				BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00, BGl_failz00zzast_nodez00,
				BGl_proc2434z00zzast_dumpz00, BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00, BGl_switchz00zzast_nodez00,
				BGl_proc2435z00zzast_dumpz00, BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc2436z00zzast_dumpz00,
				BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc2437z00zzast_dumpz00,
				BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc2438z00zzast_dumpz00,
				BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc2439z00zzast_dumpz00,
				BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00, BGl_retblockz00zzast_nodez00,
				BGl_proc2440z00zzast_dumpz00, BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00, BGl_returnz00zzast_nodez00,
				BGl_proc2441z00zzast_dumpz00, BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc2442z00zzast_dumpz00,
				BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc2443z00zzast_dumpz00,
				BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc2444z00zzast_dumpz00,
				BGl_string2412z00zzast_dumpz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00, BGl_patchz00zzast_nodez00,
				BGl_proc2445z00zzast_dumpz00, BGl_string2412z00zzast_dumpz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3sexpzd2envze3zzast_dumpz00,
				BGl_genpatchidz00zzast_nodez00, BGl_proc2446z00zzast_dumpz00,
				BGl_string2412z00zzast_dumpz00);
		}

	}



/* &node->sexp-genpatchi1426 */
	obj_t BGl_z62nodezd2ze3sexpzd2genpatchi1426z81zzast_dumpz00(obj_t
		BgL_envz00_3062, obj_t BgL_nodez00_3063)
	{
		{	/* Ast/dump.scm 455 */
			{	/* Ast/dump.scm 457 */
				obj_t BgL_arg2327z00_3146;

				{	/* Ast/dump.scm 457 */
					long BgL_arg2328z00_3147;

					BgL_arg2328z00_3147 =
						(((BgL_genpatchidz00_bglt) COBJECT(
								((BgL_genpatchidz00_bglt) BgL_nodez00_3063)))->BgL_indexz00);
					BgL_arg2327z00_3146 =
						MAKE_YOUNG_PAIR(BINT(BgL_arg2328z00_3147), BNIL);
				}
				return MAKE_YOUNG_PAIR(CNST_TABLE_REF(1), BgL_arg2327z00_3146);
			}
		}

	}



/* &node->sexp-patch1423 */
	obj_t BGl_z62nodezd2ze3sexpzd2patch1423z81zzast_dumpz00(obj_t BgL_envz00_3064,
		obj_t BgL_nodez00_3065)
	{
		{	/* Ast/dump.scm 448 */
			{	/* Ast/dump.scm 450 */
				obj_t BgL_arg2318z00_3149;
				obj_t BgL_arg2319z00_3150;

				{	/* Ast/dump.scm 450 */
					BgL_typez00_bglt BgL_arg2320z00_3151;

					BgL_arg2320z00_3151 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_patchz00_bglt) BgL_nodez00_3065))))->BgL_typez00);
					{	/* Ast/dump.scm 450 */
						obj_t BgL_idz00_3152;

						BgL_idz00_3152 = CNST_TABLE_REF(2);
						if (CBOOL(BGl_za2typenodezd2shapezf3za2z21zzengine_paramz00))
							{	/* Ast/dump.scm 442 */
								obj_t BgL_arg1489z00_3153;

								{	/* Ast/dump.scm 442 */
									obj_t BgL_arg1502z00_3154;
									obj_t BgL_arg1509z00_3155;

									{	/* Ast/dump.scm 442 */
										obj_t BgL_arg1455z00_3156;

										BgL_arg1455z00_3156 = SYMBOL_TO_STRING(BgL_idz00_3152);
										BgL_arg1502z00_3154 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_3156);
									}
									BgL_arg1509z00_3155 =
										BGl_shapez00zztools_shapez00(((obj_t) BgL_arg2320z00_3151));
									BgL_arg1489z00_3153 =
										string_append_3(BgL_arg1502z00_3154,
										BGl_string2407z00zzast_dumpz00, BgL_arg1509z00_3155);
								}
								BgL_arg2318z00_3149 = bstring_to_symbol(BgL_arg1489z00_3153);
							}
						else
							{	/* Ast/dump.scm 441 */
								BgL_arg2318z00_3149 = BgL_idz00_3152;
							}
					}
				}
				{	/* Ast/dump.scm 450 */
					obj_t BgL_arg2321z00_3157;
					obj_t BgL_arg2323z00_3158;

					{	/* Ast/dump.scm 450 */
						BgL_varz00_bglt BgL_arg2324z00_3159;

						BgL_arg2324z00_3159 =
							(((BgL_patchz00_bglt) COBJECT(
									((BgL_patchz00_bglt) BgL_nodez00_3065)))->BgL_refz00);
						BgL_arg2321z00_3157 =
							BGl_nodezd2ze3sexpz31zzast_dumpz00(
							((BgL_nodez00_bglt) BgL_arg2324z00_3159));
					}
					{	/* Ast/dump.scm 450 */
						obj_t BgL_arg2325z00_3160;

						{	/* Ast/dump.scm 450 */
							obj_t BgL_arg2326z00_3161;

							BgL_arg2326z00_3161 =
								(((BgL_atomz00_bglt) COBJECT(
										((BgL_atomz00_bglt)
											((BgL_patchz00_bglt) BgL_nodez00_3065))))->BgL_valuez00);
							BgL_arg2325z00_3160 =
								BGl_nodezd2ze3sexpz31zzast_dumpz00(
								((BgL_nodez00_bglt) BgL_arg2326z00_3161));
						}
						BgL_arg2323z00_3158 = MAKE_YOUNG_PAIR(BgL_arg2325z00_3160, BNIL);
					}
					BgL_arg2319z00_3150 =
						MAKE_YOUNG_PAIR(BgL_arg2321z00_3157, BgL_arg2323z00_3158);
				}
				return MAKE_YOUNG_PAIR(BgL_arg2318z00_3149, BgL_arg2319z00_3150);
			}
		}

	}



/* &node->sexp-box-set!1420 */
	obj_t BGl_z62nodezd2ze3sexpzd2boxzd2setz121420z41zzast_dumpz00(obj_t
		BgL_envz00_3066, obj_t BgL_nodez00_3067)
	{
		{	/* Ast/dump.scm 432 */
			{	/* Ast/dump.scm 434 */
				obj_t BgL_arg2312z00_3163;

				{	/* Ast/dump.scm 434 */
					obj_t BgL_arg2313z00_3164;
					obj_t BgL_arg2314z00_3165;

					{	/* Ast/dump.scm 434 */
						BgL_varz00_bglt BgL_arg2315z00_3166;

						BgL_arg2315z00_3166 =
							(((BgL_boxzd2setz12zc0_bglt) COBJECT(
									((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3067)))->BgL_varz00);
						BgL_arg2313z00_3164 =
							BGl_nodezd2ze3sexpz31zzast_dumpz00(
							((BgL_nodez00_bglt) BgL_arg2315z00_3166));
					}
					BgL_arg2314z00_3165 =
						MAKE_YOUNG_PAIR(BGl_nodezd2ze3sexpz31zzast_dumpz00(
							(((BgL_boxzd2setz12zc0_bglt) COBJECT(
										((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3067)))->
								BgL_valuez00)), BNIL);
					BgL_arg2312z00_3163 =
						MAKE_YOUNG_PAIR(BgL_arg2313z00_3164, BgL_arg2314z00_3165);
				}
				return MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BgL_arg2312z00_3163);
			}
		}

	}



/* &node->sexp-box-ref1418 */
	obj_t BGl_z62nodezd2ze3sexpzd2boxzd2ref1418z53zzast_dumpz00(obj_t
		BgL_envz00_3068, obj_t BgL_nodez00_3069)
	{
		{	/* Ast/dump.scm 424 */
			{	/* Ast/dump.scm 426 */
				obj_t BgL_arg2307z00_3168;
				obj_t BgL_arg2308z00_3169;

				{	/* Ast/dump.scm 426 */
					BgL_typez00_bglt BgL_arg2309z00_3170;

					BgL_arg2309z00_3170 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_boxzd2refzd2_bglt) BgL_nodez00_3069))))->BgL_typez00);
					{	/* Ast/dump.scm 426 */
						obj_t BgL_idz00_3171;

						BgL_idz00_3171 = CNST_TABLE_REF(4);
						if (CBOOL(BGl_za2typenodezd2shapezf3za2z21zzengine_paramz00))
							{	/* Ast/dump.scm 442 */
								obj_t BgL_arg1489z00_3172;

								{	/* Ast/dump.scm 442 */
									obj_t BgL_arg1502z00_3173;
									obj_t BgL_arg1509z00_3174;

									{	/* Ast/dump.scm 442 */
										obj_t BgL_arg1455z00_3175;

										BgL_arg1455z00_3175 = SYMBOL_TO_STRING(BgL_idz00_3171);
										BgL_arg1502z00_3173 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_3175);
									}
									BgL_arg1509z00_3174 =
										BGl_shapez00zztools_shapez00(((obj_t) BgL_arg2309z00_3170));
									BgL_arg1489z00_3172 =
										string_append_3(BgL_arg1502z00_3173,
										BGl_string2407z00zzast_dumpz00, BgL_arg1509z00_3174);
								}
								BgL_arg2307z00_3168 = bstring_to_symbol(BgL_arg1489z00_3172);
							}
						else
							{	/* Ast/dump.scm 441 */
								BgL_arg2307z00_3168 = BgL_idz00_3171;
							}
					}
				}
				{	/* Ast/dump.scm 427 */
					obj_t BgL_arg2310z00_3176;

					{	/* Ast/dump.scm 427 */
						BgL_varz00_bglt BgL_arg2311z00_3177;

						BgL_arg2311z00_3177 =
							(((BgL_boxzd2refzd2_bglt) COBJECT(
									((BgL_boxzd2refzd2_bglt) BgL_nodez00_3069)))->BgL_varz00);
						BgL_arg2310z00_3176 =
							BGl_nodezd2ze3sexpz31zzast_dumpz00(
							((BgL_nodez00_bglt) BgL_arg2311z00_3177));
					}
					BgL_arg2308z00_3169 = MAKE_YOUNG_PAIR(BgL_arg2310z00_3176, BNIL);
				}
				return MAKE_YOUNG_PAIR(BgL_arg2307z00_3168, BgL_arg2308z00_3169);
			}
		}

	}



/* &node->sexp-make-box1416 */
	obj_t BGl_z62nodezd2ze3sexpzd2makezd2box1416z53zzast_dumpz00(obj_t
		BgL_envz00_3070, obj_t BgL_nodez00_3071)
	{
		{	/* Ast/dump.scm 413 */
			{	/* Ast/dump.scm 415 */
				obj_t BgL_arg2296z00_3179;
				obj_t BgL_arg2297z00_3180;

				{	/* Ast/dump.scm 415 */
					BgL_typez00_bglt BgL_arg2298z00_3181;

					BgL_arg2298z00_3181 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_makezd2boxzd2_bglt) BgL_nodez00_3071))))->BgL_typez00);
					{	/* Ast/dump.scm 415 */
						obj_t BgL_idz00_3182;

						BgL_idz00_3182 = CNST_TABLE_REF(5);
						if (CBOOL(BGl_za2typenodezd2shapezf3za2z21zzengine_paramz00))
							{	/* Ast/dump.scm 442 */
								obj_t BgL_arg1489z00_3183;

								{	/* Ast/dump.scm 442 */
									obj_t BgL_arg1502z00_3184;
									obj_t BgL_arg1509z00_3185;

									{	/* Ast/dump.scm 442 */
										obj_t BgL_arg1455z00_3186;

										BgL_arg1455z00_3186 = SYMBOL_TO_STRING(BgL_idz00_3182);
										BgL_arg1502z00_3184 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_3186);
									}
									BgL_arg1509z00_3185 =
										BGl_shapez00zztools_shapez00(((obj_t) BgL_arg2298z00_3181));
									BgL_arg1489z00_3183 =
										string_append_3(BgL_arg1502z00_3184,
										BGl_string2407z00zzast_dumpz00, BgL_arg1509z00_3185);
								}
								BgL_arg2296z00_3179 = bstring_to_symbol(BgL_arg1489z00_3183);
							}
						else
							{	/* Ast/dump.scm 441 */
								BgL_arg2296z00_3179 = BgL_idz00_3182;
							}
					}
				}
				{	/* Ast/dump.scm 416 */
					obj_t BgL_arg2299z00_3187;
					obj_t BgL_arg2301z00_3188;

					if (CBOOL(BGl_za2alloczd2shapezf3za2z21zzengine_paramz00))
						{	/* Ast/dump.scm 417 */
							obj_t BgL_arg2302z00_3189;

							BgL_arg2302z00_3189 =
								(((BgL_makezd2boxzd2_bglt) COBJECT(
										((BgL_makezd2boxzd2_bglt) BgL_nodez00_3071)))->
								BgL_stackablez00);
							{	/* Ast/dump.scm 417 */
								obj_t BgL_list2303z00_3190;

								{	/* Ast/dump.scm 417 */
									obj_t BgL_arg2304z00_3191;

									BgL_arg2304z00_3191 =
										MAKE_YOUNG_PAIR(BgL_arg2302z00_3189, BNIL);
									BgL_list2303z00_3190 =
										MAKE_YOUNG_PAIR(BGl_string2447z00zzast_dumpz00,
										BgL_arg2304z00_3191);
								}
								BgL_arg2299z00_3187 = BgL_list2303z00_3190;
							}
						}
					else
						{	/* Ast/dump.scm 416 */
							BgL_arg2299z00_3187 = BNIL;
						}
					BgL_arg2301z00_3188 =
						MAKE_YOUNG_PAIR(BGl_nodezd2ze3sexpz31zzast_dumpz00(
							(((BgL_makezd2boxzd2_bglt) COBJECT(
										((BgL_makezd2boxzd2_bglt) BgL_nodez00_3071)))->
								BgL_valuez00)), BNIL);
					BgL_arg2297z00_3180 =
						BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_arg2299z00_3187,
						BgL_arg2301z00_3188);
				}
				return MAKE_YOUNG_PAIR(BgL_arg2296z00_3179, BgL_arg2297z00_3180);
			}
		}

	}



/* &node->sexp-return1414 */
	obj_t BGl_z62nodezd2ze3sexpzd2return1414z81zzast_dumpz00(obj_t
		BgL_envz00_3072, obj_t BgL_nodez00_3073)
	{
		{	/* Ast/dump.scm 405 */
			{	/* Ast/dump.scm 407 */
				obj_t BgL_arg2291z00_3193;
				obj_t BgL_arg2292z00_3194;

				{	/* Ast/dump.scm 407 */
					BgL_typez00_bglt BgL_arg2293z00_3195;

					BgL_arg2293z00_3195 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_returnz00_bglt) BgL_nodez00_3073))))->BgL_typez00);
					{	/* Ast/dump.scm 407 */
						obj_t BgL_idz00_3196;

						BgL_idz00_3196 = CNST_TABLE_REF(6);
						if (CBOOL(BGl_za2typenodezd2shapezf3za2z21zzengine_paramz00))
							{	/* Ast/dump.scm 442 */
								obj_t BgL_arg1489z00_3197;

								{	/* Ast/dump.scm 442 */
									obj_t BgL_arg1502z00_3198;
									obj_t BgL_arg1509z00_3199;

									{	/* Ast/dump.scm 442 */
										obj_t BgL_arg1455z00_3200;

										BgL_arg1455z00_3200 = SYMBOL_TO_STRING(BgL_idz00_3196);
										BgL_arg1502z00_3198 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_3200);
									}
									BgL_arg1509z00_3199 =
										BGl_shapez00zztools_shapez00(((obj_t) BgL_arg2293z00_3195));
									BgL_arg1489z00_3197 =
										string_append_3(BgL_arg1502z00_3198,
										BGl_string2407z00zzast_dumpz00, BgL_arg1509z00_3199);
								}
								BgL_arg2291z00_3193 = bstring_to_symbol(BgL_arg1489z00_3197);
							}
						else
							{	/* Ast/dump.scm 441 */
								BgL_arg2291z00_3193 = BgL_idz00_3196;
							}
					}
				}
				BgL_arg2292z00_3194 =
					MAKE_YOUNG_PAIR(BGl_nodezd2ze3sexpz31zzast_dumpz00(
						(((BgL_returnz00_bglt) COBJECT(
									((BgL_returnz00_bglt) BgL_nodez00_3073)))->BgL_valuez00)),
					BNIL);
				return MAKE_YOUNG_PAIR(BgL_arg2291z00_3193, BgL_arg2292z00_3194);
			}
		}

	}



/* &node->sexp-retblock1412 */
	obj_t BGl_z62nodezd2ze3sexpzd2retblock1412z81zzast_dumpz00(obj_t
		BgL_envz00_3074, obj_t BgL_nodez00_3075)
	{
		{	/* Ast/dump.scm 397 */
			{	/* Ast/dump.scm 399 */
				obj_t BgL_arg2286z00_3202;
				obj_t BgL_arg2287z00_3203;

				{	/* Ast/dump.scm 399 */
					BgL_typez00_bglt BgL_arg2288z00_3204;

					BgL_arg2288z00_3204 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_retblockz00_bglt) BgL_nodez00_3075))))->BgL_typez00);
					{	/* Ast/dump.scm 399 */
						obj_t BgL_idz00_3205;

						BgL_idz00_3205 = CNST_TABLE_REF(7);
						if (CBOOL(BGl_za2typenodezd2shapezf3za2z21zzengine_paramz00))
							{	/* Ast/dump.scm 442 */
								obj_t BgL_arg1489z00_3206;

								{	/* Ast/dump.scm 442 */
									obj_t BgL_arg1502z00_3207;
									obj_t BgL_arg1509z00_3208;

									{	/* Ast/dump.scm 442 */
										obj_t BgL_arg1455z00_3209;

										BgL_arg1455z00_3209 = SYMBOL_TO_STRING(BgL_idz00_3205);
										BgL_arg1502z00_3207 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_3209);
									}
									BgL_arg1509z00_3208 =
										BGl_shapez00zztools_shapez00(((obj_t) BgL_arg2288z00_3204));
									BgL_arg1489z00_3206 =
										string_append_3(BgL_arg1502z00_3207,
										BGl_string2407z00zzast_dumpz00, BgL_arg1509z00_3208);
								}
								BgL_arg2286z00_3202 = bstring_to_symbol(BgL_arg1489z00_3206);
							}
						else
							{	/* Ast/dump.scm 441 */
								BgL_arg2286z00_3202 = BgL_idz00_3205;
							}
					}
				}
				BgL_arg2287z00_3203 =
					MAKE_YOUNG_PAIR(BGl_nodezd2ze3sexpz31zzast_dumpz00(
						(((BgL_retblockz00_bglt) COBJECT(
									((BgL_retblockz00_bglt) BgL_nodez00_3075)))->BgL_bodyz00)),
					BNIL);
				return MAKE_YOUNG_PAIR(BgL_arg2286z00_3202, BgL_arg2287z00_3203);
			}
		}

	}



/* &node->sexp-jump-ex-i1408 */
	obj_t BGl_z62nodezd2ze3sexpzd2jumpzd2exzd2i1408z81zzast_dumpz00(obj_t
		BgL_envz00_3076, obj_t BgL_nodez00_3077)
	{
		{	/* Ast/dump.scm 388 */
			{	/* Ast/dump.scm 390 */
				obj_t BgL_arg2276z00_3211;
				obj_t BgL_arg2277z00_3212;

				{	/* Ast/dump.scm 390 */
					BgL_typez00_bglt BgL_arg2279z00_3213;

					BgL_arg2279z00_3213 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3077))))->
						BgL_typez00);
					{	/* Ast/dump.scm 390 */
						obj_t BgL_idz00_3214;

						BgL_idz00_3214 = CNST_TABLE_REF(8);
						if (CBOOL(BGl_za2typenodezd2shapezf3za2z21zzengine_paramz00))
							{	/* Ast/dump.scm 442 */
								obj_t BgL_arg1489z00_3215;

								{	/* Ast/dump.scm 442 */
									obj_t BgL_arg1502z00_3216;
									obj_t BgL_arg1509z00_3217;

									{	/* Ast/dump.scm 442 */
										obj_t BgL_arg1455z00_3218;

										BgL_arg1455z00_3218 = SYMBOL_TO_STRING(BgL_idz00_3214);
										BgL_arg1502z00_3216 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_3218);
									}
									BgL_arg1509z00_3217 =
										BGl_shapez00zztools_shapez00(((obj_t) BgL_arg2279z00_3213));
									BgL_arg1489z00_3215 =
										string_append_3(BgL_arg1502z00_3216,
										BGl_string2407z00zzast_dumpz00, BgL_arg1509z00_3217);
								}
								BgL_arg2276z00_3211 = bstring_to_symbol(BgL_arg1489z00_3215);
							}
						else
							{	/* Ast/dump.scm 441 */
								BgL_arg2276z00_3211 = BgL_idz00_3214;
							}
					}
				}
				{	/* Ast/dump.scm 391 */
					obj_t BgL_arg2280z00_3219;
					obj_t BgL_arg2281z00_3220;

					BgL_arg2280z00_3219 =
						BGl_nodezd2ze3sexpz31zzast_dumpz00(
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3077)))->
							BgL_exitz00));
					BgL_arg2281z00_3220 =
						MAKE_YOUNG_PAIR(BGl_nodezd2ze3sexpz31zzast_dumpz00(((
									(BgL_jumpzd2exzd2itz00_bglt)
									COBJECT(((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3077)))->
								BgL_valuez00)), BNIL);
					BgL_arg2277z00_3212 =
						MAKE_YOUNG_PAIR(BgL_arg2280z00_3219, BgL_arg2281z00_3220);
				}
				return MAKE_YOUNG_PAIR(BgL_arg2276z00_3211, BgL_arg2277z00_3212);
			}
		}

	}



/* &node->sexp-set-ex-it1406 */
	obj_t BGl_z62nodezd2ze3sexpzd2setzd2exzd2it1406z81zzast_dumpz00(obj_t
		BgL_envz00_3078, obj_t BgL_nodez00_3079)
	{
		{	/* Ast/dump.scm 378 */
			{	/* Ast/dump.scm 380 */
				obj_t BgL_arg2263z00_3222;
				obj_t BgL_arg2264z00_3223;

				{	/* Ast/dump.scm 380 */
					BgL_typez00_bglt BgL_arg2265z00_3224;

					BgL_arg2265z00_3224 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3079))))->
						BgL_typez00);
					{	/* Ast/dump.scm 380 */
						obj_t BgL_idz00_3225;

						BgL_idz00_3225 = CNST_TABLE_REF(9);
						if (CBOOL(BGl_za2typenodezd2shapezf3za2z21zzengine_paramz00))
							{	/* Ast/dump.scm 442 */
								obj_t BgL_arg1489z00_3226;

								{	/* Ast/dump.scm 442 */
									obj_t BgL_arg1502z00_3227;
									obj_t BgL_arg1509z00_3228;

									{	/* Ast/dump.scm 442 */
										obj_t BgL_arg1455z00_3229;

										BgL_arg1455z00_3229 = SYMBOL_TO_STRING(BgL_idz00_3225);
										BgL_arg1502z00_3227 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_3229);
									}
									BgL_arg1509z00_3228 =
										BGl_shapez00zztools_shapez00(((obj_t) BgL_arg2265z00_3224));
									BgL_arg1489z00_3226 =
										string_append_3(BgL_arg1502z00_3227,
										BGl_string2407z00zzast_dumpz00, BgL_arg1509z00_3228);
								}
								BgL_arg2263z00_3222 = bstring_to_symbol(BgL_arg1489z00_3226);
							}
						else
							{	/* Ast/dump.scm 441 */
								BgL_arg2263z00_3222 = BgL_idz00_3225;
							}
					}
				}
				{	/* Ast/dump.scm 381 */
					obj_t BgL_arg2266z00_3230;
					obj_t BgL_arg2267z00_3231;

					{	/* Ast/dump.scm 381 */
						BgL_varz00_bglt BgL_arg2268z00_3232;

						BgL_arg2268z00_3232 =
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
									((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3079)))->BgL_varz00);
						BgL_arg2266z00_3230 =
							BGl_nodezd2ze3sexpz31zzast_dumpz00(
							((BgL_nodez00_bglt) BgL_arg2268z00_3232));
					}
					{	/* Ast/dump.scm 382 */
						obj_t BgL_arg2269z00_3233;

						{	/* Ast/dump.scm 382 */
							obj_t BgL_arg2270z00_3234;
							obj_t BgL_arg2271z00_3235;

							BgL_arg2270z00_3234 =
								BGl_nodezd2ze3sexpz31zzast_dumpz00(
								(((BgL_setzd2exzd2itz00_bglt) COBJECT(
											((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3079)))->
									BgL_bodyz00));
							{	/* Ast/dump.scm 383 */
								obj_t BgL_arg2273z00_3236;

								BgL_arg2273z00_3236 =
									MAKE_YOUNG_PAIR(BGl_nodezd2ze3sexpz31zzast_dumpz00(
										(((BgL_setzd2exzd2itz00_bglt) COBJECT(
													((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3079)))->
											BgL_onexitz00)), BNIL);
								BgL_arg2271z00_3235 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(10), BgL_arg2273z00_3236);
							}
							BgL_arg2269z00_3233 =
								MAKE_YOUNG_PAIR(BgL_arg2270z00_3234, BgL_arg2271z00_3235);
						}
						BgL_arg2267z00_3231 =
							MAKE_YOUNG_PAIR(CNST_TABLE_REF(11), BgL_arg2269z00_3233);
					}
					BgL_arg2264z00_3223 =
						MAKE_YOUNG_PAIR(BgL_arg2266z00_3230, BgL_arg2267z00_3231);
				}
				return MAKE_YOUNG_PAIR(BgL_arg2263z00_3222, BgL_arg2264z00_3223);
			}
		}

	}



/* &node->sexp-let-var1403 */
	obj_t BGl_z62nodezd2ze3sexpzd2letzd2var1403z53zzast_dumpz00(obj_t
		BgL_envz00_3080, obj_t BgL_nodez00_3081)
	{
		{	/* Ast/dump.scm 361 */
			{	/* Ast/dump.scm 363 */
				obj_t BgL_symz00_3238;

				{	/* Ast/dump.scm 363 */
					BgL_typez00_bglt BgL_arg2262z00_3239;

					BgL_arg2262z00_3239 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_letzd2varzd2_bglt) BgL_nodez00_3081))))->BgL_typez00);
					{	/* Ast/dump.scm 363 */
						obj_t BgL_idz00_3240;

						BgL_idz00_3240 = CNST_TABLE_REF(12);
						if (CBOOL(BGl_za2typenodezd2shapezf3za2z21zzengine_paramz00))
							{	/* Ast/dump.scm 442 */
								obj_t BgL_arg1489z00_3241;

								{	/* Ast/dump.scm 442 */
									obj_t BgL_arg1502z00_3242;
									obj_t BgL_arg1509z00_3243;

									{	/* Ast/dump.scm 442 */
										obj_t BgL_arg1455z00_3244;

										BgL_arg1455z00_3244 = SYMBOL_TO_STRING(BgL_idz00_3240);
										BgL_arg1502z00_3242 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_3244);
									}
									BgL_arg1509z00_3243 =
										BGl_shapez00zztools_shapez00(((obj_t) BgL_arg2262z00_3239));
									BgL_arg1489z00_3241 =
										string_append_3(BgL_arg1502z00_3242,
										BGl_string2407z00zzast_dumpz00, BgL_arg1509z00_3243);
								}
								BgL_symz00_3238 = bstring_to_symbol(BgL_arg1489z00_3241);
							}
						else
							{	/* Ast/dump.scm 441 */
								BgL_symz00_3238 = BgL_idz00_3240;
							}
					}
				}
				{	/* Ast/dump.scm 364 */
					obj_t BgL_arg2238z00_3245;
					obj_t BgL_arg2239z00_3246;

					BgL_arg2238z00_3245 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_letzd2varzd2_bglt) BgL_nodez00_3081))))->BgL_locz00);
					{	/* Ast/dump.scm 366 */
						obj_t BgL_arg2240z00_3247;

						{	/* Ast/dump.scm 366 */
							obj_t BgL_arg2241z00_3248;
							obj_t BgL_arg2242z00_3249;

							if (CBOOL(BGl_za2accesszd2shapezf3za2z21zzengine_paramz00))
								{	/* Ast/dump.scm 367 */
									obj_t BgL_arg2243z00_3250;

									{	/* Ast/dump.scm 367 */
										bool_t BgL_arg2244z00_3251;
										obj_t BgL_arg2245z00_3252;

										BgL_arg2244z00_3251 =
											BGl_sidezd2effectzf3z21zzeffect_effectz00(
											((BgL_nodez00_bglt)
												((BgL_letzd2varzd2_bglt) BgL_nodez00_3081)));
										{	/* Ast/dump.scm 368 */
											obj_t BgL_arg2246z00_3253;

											{	/* Ast/dump.scm 368 */
												bool_t BgL_arg2247z00_3254;

												BgL_arg2247z00_3254 =
													(((BgL_letzd2varzd2_bglt) COBJECT(
															((BgL_letzd2varzd2_bglt) BgL_nodez00_3081)))->
													BgL_removablezf3zf3);
												BgL_arg2246z00_3253 =
													MAKE_YOUNG_PAIR(BBOOL(BgL_arg2247z00_3254), BNIL);
											}
											BgL_arg2245z00_3252 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(13),
												BgL_arg2246z00_3253);
										}
										BgL_arg2243z00_3250 =
											MAKE_YOUNG_PAIR(BBOOL(BgL_arg2244z00_3251),
											BgL_arg2245z00_3252);
									}
									BgL_arg2241z00_3248 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(14), BgL_arg2243z00_3250);
								}
							else
								{	/* Ast/dump.scm 366 */
									BgL_arg2241z00_3248 = BNIL;
								}
							{	/* Ast/dump.scm 370 */
								obj_t BgL_arg2248z00_3255;
								obj_t BgL_arg2249z00_3256;

								{	/* Ast/dump.scm 370 */
									obj_t BgL_l1341z00_3257;

									BgL_l1341z00_3257 =
										(((BgL_letzd2varzd2_bglt) COBJECT(
												((BgL_letzd2varzd2_bglt) BgL_nodez00_3081)))->
										BgL_bindingsz00);
									if (NULLP(BgL_l1341z00_3257))
										{	/* Ast/dump.scm 370 */
											BgL_arg2248z00_3255 = BNIL;
										}
									else
										{	/* Ast/dump.scm 370 */
											obj_t BgL_head1343z00_3258;

											BgL_head1343z00_3258 = MAKE_YOUNG_PAIR(BNIL, BNIL);
											{
												obj_t BgL_l1341z00_3260;
												obj_t BgL_tail1344z00_3261;

												BgL_l1341z00_3260 = BgL_l1341z00_3257;
												BgL_tail1344z00_3261 = BgL_head1343z00_3258;
											BgL_zc3z04anonymousza32251ze3z87_3259:
												if (NULLP(BgL_l1341z00_3260))
													{	/* Ast/dump.scm 370 */
														BgL_arg2248z00_3255 = CDR(BgL_head1343z00_3258);
													}
												else
													{	/* Ast/dump.scm 370 */
														obj_t BgL_newtail1345z00_3262;

														{	/* Ast/dump.scm 370 */
															obj_t BgL_arg2254z00_3263;

															{	/* Ast/dump.scm 370 */
																obj_t BgL_bz00_3264;

																BgL_bz00_3264 =
																	CAR(((obj_t) BgL_l1341z00_3260));
																{	/* Ast/dump.scm 371 */
																	obj_t BgL_arg2255z00_3265;
																	obj_t BgL_arg2256z00_3266;

																	{	/* Ast/dump.scm 371 */
																		obj_t BgL_arg2257z00_3267;

																		BgL_arg2257z00_3267 =
																			CAR(((obj_t) BgL_bz00_3264));
																		BgL_arg2255z00_3265 =
																			BGl_shapez00zztools_shapez00
																			(BgL_arg2257z00_3267);
																	}
																	{	/* Ast/dump.scm 371 */
																		obj_t BgL_arg2258z00_3268;

																		{	/* Ast/dump.scm 371 */
																			obj_t BgL_arg2259z00_3269;

																			BgL_arg2259z00_3269 =
																				CDR(((obj_t) BgL_bz00_3264));
																			BgL_arg2258z00_3268 =
																				BGl_nodezd2ze3sexpz31zzast_dumpz00(
																				((BgL_nodez00_bglt)
																					BgL_arg2259z00_3269));
																		}
																		BgL_arg2256z00_3266 =
																			MAKE_YOUNG_PAIR(BgL_arg2258z00_3268,
																			BNIL);
																	}
																	BgL_arg2254z00_3263 =
																		MAKE_YOUNG_PAIR(BgL_arg2255z00_3265,
																		BgL_arg2256z00_3266);
																}
															}
															BgL_newtail1345z00_3262 =
																MAKE_YOUNG_PAIR(BgL_arg2254z00_3263, BNIL);
														}
														SET_CDR(BgL_tail1344z00_3261,
															BgL_newtail1345z00_3262);
														{	/* Ast/dump.scm 370 */
															obj_t BgL_arg2253z00_3270;

															BgL_arg2253z00_3270 =
																CDR(((obj_t) BgL_l1341z00_3260));
															{
																obj_t BgL_tail1344z00_4218;
																obj_t BgL_l1341z00_4217;

																BgL_l1341z00_4217 = BgL_arg2253z00_3270;
																BgL_tail1344z00_4218 = BgL_newtail1345z00_3262;
																BgL_tail1344z00_3261 = BgL_tail1344z00_4218;
																BgL_l1341z00_3260 = BgL_l1341z00_4217;
																goto BgL_zc3z04anonymousza32251ze3z87_3259;
															}
														}
													}
											}
										}
								}
								BgL_arg2249z00_3256 =
									MAKE_YOUNG_PAIR(BGl_nodezd2ze3sexpz31zzast_dumpz00(
										(((BgL_letzd2varzd2_bglt) COBJECT(
													((BgL_letzd2varzd2_bglt) BgL_nodez00_3081)))->
											BgL_bodyz00)), BNIL);
								BgL_arg2242z00_3249 =
									MAKE_YOUNG_PAIR(BgL_arg2248z00_3255, BgL_arg2249z00_3256);
							}
							BgL_arg2240z00_3247 =
								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
								(BgL_arg2241z00_3248, BgL_arg2242z00_3249);
						}
						BgL_arg2239z00_3246 =
							MAKE_YOUNG_PAIR(BgL_symz00_3238, BgL_arg2240z00_3247);
					}
					return
						BGl_locationzd2shapezd2zztools_locationz00(BgL_arg2238z00_3245,
						BgL_arg2239z00_3246);
				}
			}
		}

	}



/* &node->sexp-let-fun1401 */
	obj_t BGl_z62nodezd2ze3sexpzd2letzd2fun1401z53zzast_dumpz00(obj_t
		BgL_envz00_3082, obj_t BgL_nodez00_3083)
	{
		{	/* Ast/dump.scm 340 */
			{	/* Ast/dump.scm 342 */
				obj_t BgL_symz00_3272;

				{	/* Ast/dump.scm 342 */
					BgL_typez00_bglt BgL_arg2237z00_3273;

					BgL_arg2237z00_3273 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_letzd2funzd2_bglt) BgL_nodez00_3083))))->BgL_typez00);
					{	/* Ast/dump.scm 342 */
						obj_t BgL_idz00_3274;

						BgL_idz00_3274 = CNST_TABLE_REF(15);
						if (CBOOL(BGl_za2typenodezd2shapezf3za2z21zzengine_paramz00))
							{	/* Ast/dump.scm 442 */
								obj_t BgL_arg1489z00_3275;

								{	/* Ast/dump.scm 442 */
									obj_t BgL_arg1502z00_3276;
									obj_t BgL_arg1509z00_3277;

									{	/* Ast/dump.scm 442 */
										obj_t BgL_arg1455z00_3278;

										BgL_arg1455z00_3278 = SYMBOL_TO_STRING(BgL_idz00_3274);
										BgL_arg1502z00_3276 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_3278);
									}
									BgL_arg1509z00_3277 =
										BGl_shapez00zztools_shapez00(((obj_t) BgL_arg2237z00_3273));
									BgL_arg1489z00_3275 =
										string_append_3(BgL_arg1502z00_3276,
										BGl_string2407z00zzast_dumpz00, BgL_arg1509z00_3277);
								}
								BgL_symz00_3272 = bstring_to_symbol(BgL_arg1489z00_3275);
							}
						else
							{	/* Ast/dump.scm 441 */
								BgL_symz00_3272 = BgL_idz00_3274;
							}
					}
				}
				{	/* Ast/dump.scm 343 */
					obj_t BgL_arg2202z00_3279;
					obj_t BgL_arg2203z00_3280;

					BgL_arg2202z00_3279 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_letzd2funzd2_bglt) BgL_nodez00_3083))))->BgL_locz00);
					{	/* Ast/dump.scm 344 */
						obj_t BgL_arg2204z00_3281;

						{	/* Ast/dump.scm 344 */
							obj_t BgL_arg2205z00_3282;
							obj_t BgL_arg2206z00_3283;

							{	/* Ast/dump.scm 344 */
								obj_t BgL_l1336z00_3284;

								BgL_l1336z00_3284 =
									(((BgL_letzd2funzd2_bglt) COBJECT(
											((BgL_letzd2funzd2_bglt) BgL_nodez00_3083)))->
									BgL_localsz00);
								if (NULLP(BgL_l1336z00_3284))
									{	/* Ast/dump.scm 344 */
										BgL_arg2205z00_3282 = BNIL;
									}
								else
									{	/* Ast/dump.scm 344 */
										obj_t BgL_head1338z00_3285;

										BgL_head1338z00_3285 = MAKE_YOUNG_PAIR(BNIL, BNIL);
										{
											obj_t BgL_l1336z00_3287;
											obj_t BgL_tail1339z00_3288;

											BgL_l1336z00_3287 = BgL_l1336z00_3284;
											BgL_tail1339z00_3288 = BgL_head1338z00_3285;
										BgL_zc3z04anonymousza32208ze3z87_3286:
											if (NULLP(BgL_l1336z00_3287))
												{	/* Ast/dump.scm 344 */
													BgL_arg2205z00_3282 = CDR(BgL_head1338z00_3285);
												}
											else
												{	/* Ast/dump.scm 344 */
													obj_t BgL_newtail1340z00_3289;

													{	/* Ast/dump.scm 344 */
														obj_t BgL_arg2211z00_3290;

														{	/* Ast/dump.scm 344 */
															obj_t BgL_funz00_3291;

															BgL_funz00_3291 =
																CAR(((obj_t) BgL_l1336z00_3287));
															{	/* Ast/dump.scm 345 */
																BgL_valuez00_bglt BgL_fz00_3292;

																BgL_fz00_3292 =
																	(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_localz00_bglt)
																					BgL_funz00_3291))))->BgL_valuez00);
																{	/* Ast/dump.scm 346 */
																	obj_t BgL_arg2212z00_3293;
																	obj_t BgL_arg2213z00_3294;

																	BgL_arg2212z00_3293 =
																		BGl_shapez00zztools_shapez00
																		(BgL_funz00_3291);
																	{	/* Ast/dump.scm 347 */
																		obj_t BgL_arg2214z00_3295;
																		obj_t BgL_arg2215z00_3296;

																		{	/* Ast/dump.scm 348 */
																			bool_t BgL_test2523z00_4256;

																			{	/* Ast/dump.scm 348 */
																				obj_t BgL_tmpz00_4257;

																				BgL_tmpz00_4257 =
																					(((BgL_sfunz00_bglt) COBJECT(
																							((BgL_sfunz00_bglt)
																								BgL_fz00_3292)))->
																					BgL_stackablez00);
																				BgL_test2523z00_4256 =
																					BOOLEANP(BgL_tmpz00_4257);
																			}
																			if (BgL_test2523z00_4256)
																				{	/* Ast/dump.scm 349 */
																					obj_t BgL_arg2218z00_3297;

																					{	/* Ast/dump.scm 349 */
																						obj_t BgL_arg2219z00_3298;

																						BgL_arg2219z00_3298 =
																							(((BgL_sfunz00_bglt) COBJECT(
																									((BgL_sfunz00_bglt)
																										BgL_fz00_3292)))->
																							BgL_stackablez00);
																						BgL_arg2218z00_3297 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2219z00_3298, BNIL);
																					}
																					BgL_arg2214z00_3295 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(16),
																						BgL_arg2218z00_3297);
																				}
																			else
																				{	/* Ast/dump.scm 348 */
																					BgL_arg2214z00_3295 = BNIL;
																				}
																		}
																		{	/* Ast/dump.scm 352 */
																			obj_t BgL_arg2221z00_3299;
																			obj_t BgL_arg2222z00_3300;

																			{	/* Ast/dump.scm 352 */
																				obj_t BgL_arg2223z00_3301;
																				long BgL_arg2224z00_3302;

																				{	/* Ast/dump.scm 352 */
																					obj_t BgL_l1330z00_3303;

																					BgL_l1330z00_3303 =
																						(((BgL_sfunz00_bglt) COBJECT(
																								((BgL_sfunz00_bglt)
																									BgL_fz00_3292)))->
																						BgL_argsz00);
																					if (NULLP(BgL_l1330z00_3303))
																						{	/* Ast/dump.scm 352 */
																							BgL_arg2223z00_3301 = BNIL;
																						}
																					else
																						{	/* Ast/dump.scm 352 */
																							obj_t BgL_head1332z00_3304;

																							{	/* Ast/dump.scm 352 */
																								obj_t BgL_arg2231z00_3305;

																								{	/* Ast/dump.scm 352 */
																									obj_t BgL_arg2232z00_3306;

																									BgL_arg2232z00_3306 =
																										CAR(
																										((obj_t)
																											BgL_l1330z00_3303));
																									BgL_arg2231z00_3305 =
																										BGl_shapez00zztools_shapez00
																										(BgL_arg2232z00_3306);
																								}
																								BgL_head1332z00_3304 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg2231z00_3305, BNIL);
																							}
																							{	/* Ast/dump.scm 352 */
																								obj_t BgL_g1335z00_3307;

																								BgL_g1335z00_3307 =
																									CDR(
																									((obj_t) BgL_l1330z00_3303));
																								{
																									obj_t BgL_l1330z00_3309;
																									obj_t BgL_tail1333z00_3310;

																									BgL_l1330z00_3309 =
																										BgL_g1335z00_3307;
																									BgL_tail1333z00_3310 =
																										BgL_head1332z00_3304;
																								BgL_zc3z04anonymousza32226ze3z87_3308:
																									if (NULLP
																										(BgL_l1330z00_3309))
																										{	/* Ast/dump.scm 352 */
																											BgL_arg2223z00_3301 =
																												BgL_head1332z00_3304;
																										}
																									else
																										{	/* Ast/dump.scm 352 */
																											obj_t
																												BgL_newtail1334z00_3311;
																											{	/* Ast/dump.scm 352 */
																												obj_t
																													BgL_arg2229z00_3312;
																												{	/* Ast/dump.scm 352 */
																													obj_t
																														BgL_arg2230z00_3313;
																													BgL_arg2230z00_3313 =
																														CAR(((obj_t)
																															BgL_l1330z00_3309));
																													BgL_arg2229z00_3312 =
																														BGl_shapez00zztools_shapez00
																														(BgL_arg2230z00_3313);
																												}
																												BgL_newtail1334z00_3311
																													=
																													MAKE_YOUNG_PAIR
																													(BgL_arg2229z00_3312,
																													BNIL);
																											}
																											SET_CDR
																												(BgL_tail1333z00_3310,
																												BgL_newtail1334z00_3311);
																											{	/* Ast/dump.scm 352 */
																												obj_t
																													BgL_arg2228z00_3314;
																												BgL_arg2228z00_3314 =
																													CDR(((obj_t)
																														BgL_l1330z00_3309));
																												{
																													obj_t
																														BgL_tail1333z00_4286;
																													obj_t
																														BgL_l1330z00_4285;
																													BgL_l1330z00_4285 =
																														BgL_arg2228z00_3314;
																													BgL_tail1333z00_4286 =
																														BgL_newtail1334z00_3311;
																													BgL_tail1333z00_3310 =
																														BgL_tail1333z00_4286;
																													BgL_l1330z00_3309 =
																														BgL_l1330z00_4285;
																													goto
																														BgL_zc3z04anonymousza32226ze3z87_3308;
																												}
																											}
																										}
																								}
																							}
																						}
																				}
																				BgL_arg2224z00_3302 =
																					(((BgL_funz00_bglt) COBJECT(
																							((BgL_funz00_bglt)
																								((BgL_sfunz00_bglt)
																									BgL_fz00_3292))))->
																					BgL_arityz00);
																				BgL_arg2221z00_3299 =
																					BGl_argszd2listzd2ze3argsza2z41zztools_argsz00
																					(BgL_arg2223z00_3301,
																					BINT(BgL_arg2224z00_3302));
																			}
																			{	/* Ast/dump.scm 354 */
																				obj_t BgL_arg2233z00_3315;

																				{	/* Ast/dump.scm 354 */
																					obj_t BgL_arg2234z00_3316;

																					BgL_arg2234z00_3316 =
																						(((BgL_sfunz00_bglt) COBJECT(
																								((BgL_sfunz00_bglt)
																									BgL_fz00_3292)))->
																						BgL_bodyz00);
																					BgL_arg2233z00_3315 =
																						BGl_nodezd2ze3sexpz31zzast_dumpz00((
																							(BgL_nodez00_bglt)
																							BgL_arg2234z00_3316));
																				}
																				BgL_arg2222z00_3300 =
																					MAKE_YOUNG_PAIR(BgL_arg2233z00_3315,
																					BNIL);
																			}
																			BgL_arg2215z00_3296 =
																				MAKE_YOUNG_PAIR(BgL_arg2221z00_3299,
																				BgL_arg2222z00_3300);
																		}
																		BgL_arg2213z00_3294 =
																			BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																			(BgL_arg2214z00_3295,
																			BgL_arg2215z00_3296);
																	}
																	BgL_arg2211z00_3290 =
																		MAKE_YOUNG_PAIR(BgL_arg2212z00_3293,
																		BgL_arg2213z00_3294);
																}
															}
														}
														BgL_newtail1340z00_3289 =
															MAKE_YOUNG_PAIR(BgL_arg2211z00_3290, BNIL);
													}
													SET_CDR(BgL_tail1339z00_3288,
														BgL_newtail1340z00_3289);
													{	/* Ast/dump.scm 344 */
														obj_t BgL_arg2210z00_3317;

														BgL_arg2210z00_3317 =
															CDR(((obj_t) BgL_l1336z00_3287));
														{
															obj_t BgL_tail1339z00_4305;
															obj_t BgL_l1336z00_4304;

															BgL_l1336z00_4304 = BgL_arg2210z00_3317;
															BgL_tail1339z00_4305 = BgL_newtail1340z00_3289;
															BgL_tail1339z00_3288 = BgL_tail1339z00_4305;
															BgL_l1336z00_3287 = BgL_l1336z00_4304;
															goto BgL_zc3z04anonymousza32208ze3z87_3286;
														}
													}
												}
										}
									}
							}
							BgL_arg2206z00_3283 =
								MAKE_YOUNG_PAIR(BGl_nodezd2ze3sexpz31zzast_dumpz00(
									(((BgL_letzd2funzd2_bglt) COBJECT(
												((BgL_letzd2funzd2_bglt) BgL_nodez00_3083)))->
										BgL_bodyz00)), BNIL);
							BgL_arg2204z00_3281 =
								MAKE_YOUNG_PAIR(BgL_arg2205z00_3282, BgL_arg2206z00_3283);
						}
						BgL_arg2203z00_3280 =
							MAKE_YOUNG_PAIR(BgL_symz00_3272, BgL_arg2204z00_3281);
					}
					return
						BGl_locationzd2shapezd2zztools_locationz00(BgL_arg2202z00_3279,
						BgL_arg2203z00_3280);
				}
			}
		}

	}



/* &node->sexp-switch1399 */
	obj_t BGl_z62nodezd2ze3sexpzd2switch1399z81zzast_dumpz00(obj_t
		BgL_envz00_3084, obj_t BgL_nodez00_3085)
	{
		{	/* Ast/dump.scm 328 */
			{	/* Ast/dump.scm 330 */
				obj_t BgL_arg2184z00_3319;
				obj_t BgL_arg2185z00_3320;

				BgL_arg2184z00_3319 =
					(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_switchz00_bglt) BgL_nodez00_3085))))->BgL_locz00);
				{	/* Ast/dump.scm 331 */
					obj_t BgL_arg2186z00_3321;
					obj_t BgL_arg2187z00_3322;

					{	/* Ast/dump.scm 331 */
						BgL_typez00_bglt BgL_arg2188z00_3323;

						BgL_arg2188z00_3323 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_switchz00_bglt) BgL_nodez00_3085))))->BgL_typez00);
						{	/* Ast/dump.scm 331 */
							obj_t BgL_idz00_3324;

							BgL_idz00_3324 = CNST_TABLE_REF(17);
							if (CBOOL(BGl_za2typenodezd2shapezf3za2z21zzengine_paramz00))
								{	/* Ast/dump.scm 442 */
									obj_t BgL_arg1489z00_3325;

									{	/* Ast/dump.scm 442 */
										obj_t BgL_arg1502z00_3326;
										obj_t BgL_arg1509z00_3327;

										{	/* Ast/dump.scm 442 */
											obj_t BgL_arg1455z00_3328;

											BgL_arg1455z00_3328 = SYMBOL_TO_STRING(BgL_idz00_3324);
											BgL_arg1502z00_3326 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_3328);
										}
										BgL_arg1509z00_3327 =
											BGl_shapez00zztools_shapez00(
											((obj_t) BgL_arg2188z00_3323));
										BgL_arg1489z00_3325 =
											string_append_3(BgL_arg1502z00_3326,
											BGl_string2407z00zzast_dumpz00, BgL_arg1509z00_3327);
									}
									BgL_arg2186z00_3321 = bstring_to_symbol(BgL_arg1489z00_3325);
								}
							else
								{	/* Ast/dump.scm 441 */
									BgL_arg2186z00_3321 = BgL_idz00_3324;
								}
						}
					}
					{	/* Ast/dump.scm 332 */
						obj_t BgL_arg2189z00_3329;
						obj_t BgL_arg2190z00_3330;

						BgL_arg2189z00_3329 =
							BGl_nodezd2ze3sexpz31zzast_dumpz00(
							(((BgL_switchz00_bglt) COBJECT(
										((BgL_switchz00_bglt) BgL_nodez00_3085)))->BgL_testz00));
						{	/* Ast/dump.scm 333 */
							obj_t BgL_arg2192z00_3331;

							{	/* Ast/dump.scm 333 */
								obj_t BgL_l1325z00_3332;

								BgL_l1325z00_3332 =
									(((BgL_switchz00_bglt) COBJECT(
											((BgL_switchz00_bglt) BgL_nodez00_3085)))->
									BgL_clausesz00);
								if (NULLP(BgL_l1325z00_3332))
									{	/* Ast/dump.scm 333 */
										BgL_arg2192z00_3331 = BNIL;
									}
								else
									{	/* Ast/dump.scm 333 */
										obj_t BgL_head1327z00_3333;

										BgL_head1327z00_3333 = MAKE_YOUNG_PAIR(BNIL, BNIL);
										{
											obj_t BgL_l1325z00_3335;
											obj_t BgL_tail1328z00_3336;

											BgL_l1325z00_3335 = BgL_l1325z00_3332;
											BgL_tail1328z00_3336 = BgL_head1327z00_3333;
										BgL_zc3z04anonymousza32194ze3z87_3334:
											if (NULLP(BgL_l1325z00_3335))
												{	/* Ast/dump.scm 333 */
													BgL_arg2192z00_3331 = CDR(BgL_head1327z00_3333);
												}
											else
												{	/* Ast/dump.scm 333 */
													obj_t BgL_newtail1329z00_3337;

													{	/* Ast/dump.scm 333 */
														obj_t BgL_arg2197z00_3338;

														{	/* Ast/dump.scm 333 */
															obj_t BgL_clausez00_3339;

															BgL_clausez00_3339 =
																CAR(((obj_t) BgL_l1325z00_3335));
															{	/* Ast/dump.scm 334 */
																obj_t BgL_arg2198z00_3340;
																obj_t BgL_arg2199z00_3341;

																BgL_arg2198z00_3340 =
																	CAR(((obj_t) BgL_clausez00_3339));
																{	/* Ast/dump.scm 334 */
																	obj_t BgL_arg2200z00_3342;

																	{	/* Ast/dump.scm 334 */
																		obj_t BgL_arg2201z00_3343;

																		BgL_arg2201z00_3343 =
																			CDR(((obj_t) BgL_clausez00_3339));
																		BgL_arg2200z00_3342 =
																			BGl_nodezd2ze3sexpz31zzast_dumpz00(
																			((BgL_nodez00_bglt) BgL_arg2201z00_3343));
																	}
																	BgL_arg2199z00_3341 =
																		MAKE_YOUNG_PAIR(BgL_arg2200z00_3342, BNIL);
																}
																BgL_arg2197z00_3338 =
																	MAKE_YOUNG_PAIR(BgL_arg2198z00_3340,
																	BgL_arg2199z00_3341);
															}
														}
														BgL_newtail1329z00_3337 =
															MAKE_YOUNG_PAIR(BgL_arg2197z00_3338, BNIL);
													}
													SET_CDR(BgL_tail1328z00_3336,
														BgL_newtail1329z00_3337);
													{	/* Ast/dump.scm 333 */
														obj_t BgL_arg2196z00_3344;

														BgL_arg2196z00_3344 =
															CDR(((obj_t) BgL_l1325z00_3335));
														{
															obj_t BgL_tail1328z00_4354;
															obj_t BgL_l1325z00_4353;

															BgL_l1325z00_4353 = BgL_arg2196z00_3344;
															BgL_tail1328z00_4354 = BgL_newtail1329z00_3337;
															BgL_tail1328z00_3336 = BgL_tail1328z00_4354;
															BgL_l1325z00_3335 = BgL_l1325z00_4353;
															goto BgL_zc3z04anonymousza32194ze3z87_3334;
														}
													}
												}
										}
									}
							}
							BgL_arg2190z00_3330 =
								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
								(BgL_arg2192z00_3331, BNIL);
						}
						BgL_arg2187z00_3322 =
							MAKE_YOUNG_PAIR(BgL_arg2189z00_3329, BgL_arg2190z00_3330);
					}
					BgL_arg2185z00_3320 =
						MAKE_YOUNG_PAIR(BgL_arg2186z00_3321, BgL_arg2187z00_3322);
				}
				return
					BGl_locationzd2shapezd2zztools_locationz00(BgL_arg2184z00_3319,
					BgL_arg2185z00_3320);
			}
		}

	}



/* &node->sexp-fail1396 */
	obj_t BGl_z62nodezd2ze3sexpzd2fail1396z81zzast_dumpz00(obj_t BgL_envz00_3086,
		obj_t BgL_nodez00_3087)
	{
		{	/* Ast/dump.scm 318 */
			{	/* Ast/dump.scm 320 */
				obj_t BgL_arg2173z00_3346;
				obj_t BgL_arg2174z00_3347;

				BgL_arg2173z00_3346 =
					(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_failz00_bglt) BgL_nodez00_3087))))->BgL_locz00);
				{	/* Ast/dump.scm 321 */
					obj_t BgL_arg2175z00_3348;

					{	/* Ast/dump.scm 321 */
						obj_t BgL_arg2176z00_3349;
						obj_t BgL_arg2177z00_3350;

						BgL_arg2176z00_3349 =
							BGl_nodezd2ze3sexpz31zzast_dumpz00(
							(((BgL_failz00_bglt) COBJECT(
										((BgL_failz00_bglt) BgL_nodez00_3087)))->BgL_procz00));
						{	/* Ast/dump.scm 322 */
							obj_t BgL_arg2179z00_3351;
							obj_t BgL_arg2180z00_3352;

							BgL_arg2179z00_3351 =
								BGl_nodezd2ze3sexpz31zzast_dumpz00(
								(((BgL_failz00_bglt) COBJECT(
											((BgL_failz00_bglt) BgL_nodez00_3087)))->BgL_msgz00));
							BgL_arg2180z00_3352 =
								MAKE_YOUNG_PAIR(BGl_nodezd2ze3sexpz31zzast_dumpz00(
									(((BgL_failz00_bglt) COBJECT(
												((BgL_failz00_bglt) BgL_nodez00_3087)))->BgL_objz00)),
								BNIL);
							BgL_arg2177z00_3350 =
								MAKE_YOUNG_PAIR(BgL_arg2179z00_3351, BgL_arg2180z00_3352);
						}
						BgL_arg2175z00_3348 =
							MAKE_YOUNG_PAIR(BgL_arg2176z00_3349, BgL_arg2177z00_3350);
					}
					BgL_arg2174z00_3347 =
						MAKE_YOUNG_PAIR(CNST_TABLE_REF(18), BgL_arg2175z00_3348);
				}
				return
					BGl_locationzd2shapezd2zztools_locationz00(BgL_arg2173z00_3346,
					BgL_arg2174z00_3347);
			}
		}

	}



/* &node->sexp-condition1394 */
	obj_t BGl_z62nodezd2ze3sexpzd2condition1394z81zzast_dumpz00(obj_t
		BgL_envz00_3088, obj_t BgL_nodez00_3089)
	{
		{	/* Ast/dump.scm 307 */
			{	/* Ast/dump.scm 309 */
				obj_t BgL_arg2160z00_3354;
				obj_t BgL_arg2161z00_3355;

				BgL_arg2160z00_3354 =
					(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_conditionalz00_bglt) BgL_nodez00_3089))))->BgL_locz00);
				{	/* Ast/dump.scm 310 */
					obj_t BgL_arg2162z00_3356;
					obj_t BgL_arg2163z00_3357;

					{	/* Ast/dump.scm 310 */
						BgL_typez00_bglt BgL_arg2164z00_3358;

						BgL_arg2164z00_3358 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_conditionalz00_bglt) BgL_nodez00_3089))))->
							BgL_typez00);
						{	/* Ast/dump.scm 310 */
							obj_t BgL_idz00_3359;

							BgL_idz00_3359 = CNST_TABLE_REF(19);
							if (CBOOL(BGl_za2typenodezd2shapezf3za2z21zzengine_paramz00))
								{	/* Ast/dump.scm 442 */
									obj_t BgL_arg1489z00_3360;

									{	/* Ast/dump.scm 442 */
										obj_t BgL_arg1502z00_3361;
										obj_t BgL_arg1509z00_3362;

										{	/* Ast/dump.scm 442 */
											obj_t BgL_arg1455z00_3363;

											BgL_arg1455z00_3363 = SYMBOL_TO_STRING(BgL_idz00_3359);
											BgL_arg1502z00_3361 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_3363);
										}
										BgL_arg1509z00_3362 =
											BGl_shapez00zztools_shapez00(
											((obj_t) BgL_arg2164z00_3358));
										BgL_arg1489z00_3360 =
											string_append_3(BgL_arg1502z00_3361,
											BGl_string2407z00zzast_dumpz00, BgL_arg1509z00_3362);
									}
									BgL_arg2162z00_3356 = bstring_to_symbol(BgL_arg1489z00_3360);
								}
							else
								{	/* Ast/dump.scm 441 */
									BgL_arg2162z00_3356 = BgL_idz00_3359;
								}
						}
					}
					{	/* Ast/dump.scm 311 */
						obj_t BgL_arg2165z00_3364;
						obj_t BgL_arg2166z00_3365;

						BgL_arg2165z00_3364 =
							BGl_nodezd2ze3sexpz31zzast_dumpz00(
							(((BgL_conditionalz00_bglt) COBJECT(
										((BgL_conditionalz00_bglt) BgL_nodez00_3089)))->
								BgL_testz00));
						{	/* Ast/dump.scm 312 */
							obj_t BgL_arg2168z00_3366;
							obj_t BgL_arg2169z00_3367;

							BgL_arg2168z00_3366 =
								BGl_nodezd2ze3sexpz31zzast_dumpz00(
								(((BgL_conditionalz00_bglt) COBJECT(
											((BgL_conditionalz00_bglt) BgL_nodez00_3089)))->
									BgL_truez00));
							BgL_arg2169z00_3367 =
								MAKE_YOUNG_PAIR(BGl_nodezd2ze3sexpz31zzast_dumpz00(((
											(BgL_conditionalz00_bglt)
											COBJECT(((BgL_conditionalz00_bglt) BgL_nodez00_3089)))->
										BgL_falsez00)), BNIL);
							BgL_arg2166z00_3365 =
								MAKE_YOUNG_PAIR(BgL_arg2168z00_3366, BgL_arg2169z00_3367);
						}
						BgL_arg2163z00_3357 =
							MAKE_YOUNG_PAIR(BgL_arg2165z00_3364, BgL_arg2166z00_3365);
					}
					BgL_arg2161z00_3355 =
						MAKE_YOUNG_PAIR(BgL_arg2162z00_3356, BgL_arg2163z00_3357);
				}
				return
					BGl_locationzd2shapezd2zztools_locationz00(BgL_arg2160z00_3354,
					BgL_arg2161z00_3355);
			}
		}

	}



/* &node->sexp-setq1392 */
	obj_t BGl_z62nodezd2ze3sexpzd2setq1392z81zzast_dumpz00(obj_t BgL_envz00_3090,
		obj_t BgL_nodez00_3091)
	{
		{	/* Ast/dump.scm 298 */
			{	/* Ast/dump.scm 300 */
				obj_t BgL_arg2151z00_3369;
				obj_t BgL_arg2152z00_3370;

				BgL_arg2151z00_3369 =
					(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_setqz00_bglt) BgL_nodez00_3091))))->BgL_locz00);
				{	/* Ast/dump.scm 301 */
					obj_t BgL_arg2154z00_3371;

					{	/* Ast/dump.scm 301 */
						obj_t BgL_arg2155z00_3372;
						obj_t BgL_arg2156z00_3373;

						{	/* Ast/dump.scm 301 */
							BgL_varz00_bglt BgL_arg2157z00_3374;

							BgL_arg2157z00_3374 =
								(((BgL_setqz00_bglt) COBJECT(
										((BgL_setqz00_bglt) BgL_nodez00_3091)))->BgL_varz00);
							BgL_arg2155z00_3372 =
								BGl_nodezd2ze3sexpz31zzast_dumpz00(
								((BgL_nodez00_bglt) BgL_arg2157z00_3374));
						}
						BgL_arg2156z00_3373 =
							MAKE_YOUNG_PAIR(BGl_nodezd2ze3sexpz31zzast_dumpz00(
								(((BgL_setqz00_bglt) COBJECT(
											((BgL_setqz00_bglt) BgL_nodez00_3091)))->BgL_valuez00)),
							BNIL);
						BgL_arg2154z00_3371 =
							MAKE_YOUNG_PAIR(BgL_arg2155z00_3372, BgL_arg2156z00_3373);
					}
					BgL_arg2152z00_3370 =
						MAKE_YOUNG_PAIR(CNST_TABLE_REF(20), BgL_arg2154z00_3371);
				}
				return
					BGl_locationzd2shapezd2zztools_locationz00(BgL_arg2151z00_3369,
					BgL_arg2152z00_3370);
			}
		}

	}



/* &node->sexp-cast1390 */
	obj_t BGl_z62nodezd2ze3sexpzd2cast1390z81zzast_dumpz00(obj_t BgL_envz00_3092,
		obj_t BgL_nodez00_3093)
	{
		{	/* Ast/dump.scm 290 */
			{	/* Ast/dump.scm 293 */
				obj_t BgL_arg2145z00_3376;
				obj_t BgL_arg2146z00_3377;

				{	/* Ast/dump.scm 293 */
					obj_t BgL_arg2147z00_3378;

					BgL_arg2147z00_3378 =
						(((BgL_typez00_bglt) COBJECT(
								(((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt)
												((BgL_castz00_bglt) BgL_nodez00_3093))))->
									BgL_typez00)))->BgL_idz00);
					BgL_arg2145z00_3376 =
						BGl_makezd2typedzd2identz00zzast_identz00(CNST_TABLE_REF(21),
						BgL_arg2147z00_3378);
				}
				BgL_arg2146z00_3377 =
					MAKE_YOUNG_PAIR(BGl_nodezd2ze3sexpz31zzast_dumpz00(
						(((BgL_castz00_bglt) COBJECT(
									((BgL_castz00_bglt) BgL_nodez00_3093)))->BgL_argz00)), BNIL);
				return MAKE_YOUNG_PAIR(BgL_arg2145z00_3376, BgL_arg2146z00_3377);
			}
		}

	}



/* &node->sexp-cast-null1388 */
	obj_t BGl_z62nodezd2ze3sexpzd2castzd2null1388z53zzast_dumpz00(obj_t
		BgL_envz00_3094, obj_t BgL_nodez00_3095)
	{
		{	/* Ast/dump.scm 282 */
			{	/* Ast/dump.scm 285 */
				obj_t BgL_arg2142z00_3380;

				{	/* Ast/dump.scm 285 */
					obj_t BgL_arg2143z00_3381;

					BgL_arg2143z00_3381 =
						(((BgL_typez00_bglt) COBJECT(
								(((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt)
												((BgL_castzd2nullzd2_bglt) BgL_nodez00_3095))))->
									BgL_typez00)))->BgL_idz00);
					BgL_arg2142z00_3380 = MAKE_YOUNG_PAIR(BgL_arg2143z00_3381, BNIL);
				}
				return MAKE_YOUNG_PAIR(CNST_TABLE_REF(22), BgL_arg2142z00_3380);
			}
		}

	}



/* &node->sexp-instanceo1386 */
	obj_t BGl_z62nodezd2ze3sexpzd2instanceo1386z81zzast_dumpz00(obj_t
		BgL_envz00_3096, obj_t BgL_nodez00_3097)
	{
		{	/* Ast/dump.scm 274 */
			{	/* Ast/dump.scm 277 */
				obj_t BgL_arg2134z00_3383;

				{	/* Ast/dump.scm 277 */
					obj_t BgL_arg2135z00_3384;
					obj_t BgL_arg2136z00_3385;

					{	/* Ast/dump.scm 277 */
						obj_t BgL_arg2137z00_3386;

						{	/* Ast/dump.scm 277 */
							obj_t BgL_pairz00_3387;

							BgL_pairz00_3387 =
								(((BgL_externz00_bglt) COBJECT(
										((BgL_externz00_bglt)
											((BgL_instanceofz00_bglt) BgL_nodez00_3097))))->
								BgL_exprza2za2);
							BgL_arg2137z00_3386 = CAR(BgL_pairz00_3387);
						}
						BgL_arg2135z00_3384 =
							BGl_nodezd2ze3sexpz31zzast_dumpz00(
							((BgL_nodez00_bglt) BgL_arg2137z00_3386));
					}
					BgL_arg2136z00_3385 =
						MAKE_YOUNG_PAIR(
						(((BgL_typez00_bglt) COBJECT(
									(((BgL_instanceofz00_bglt) COBJECT(
												((BgL_instanceofz00_bglt) BgL_nodez00_3097)))->
										BgL_classz00)))->BgL_idz00), BNIL);
					BgL_arg2134z00_3383 =
						MAKE_YOUNG_PAIR(BgL_arg2135z00_3384, BgL_arg2136z00_3385);
				}
				return MAKE_YOUNG_PAIR(CNST_TABLE_REF(23), BgL_arg2134z00_3383);
			}
		}

	}



/* &node->sexp-valloc1384 */
	obj_t BGl_z62nodezd2ze3sexpzd2valloc1384z81zzast_dumpz00(obj_t
		BgL_envz00_3098, obj_t BgL_nodez00_3099)
	{
		{	/* Ast/dump.scm 262 */
			{	/* Ast/dump.scm 267 */
				obj_t BgL_arg2113z00_3389;
				obj_t BgL_arg2114z00_3390;

				{	/* Ast/dump.scm 267 */
					obj_t BgL_arg2115z00_3391;

					{	/* Ast/dump.scm 267 */
						obj_t BgL_arg2116z00_3392;
						obj_t BgL_arg2117z00_3393;

						{	/* Ast/dump.scm 267 */
							BgL_typez00_bglt BgL_arg2123z00_3394;

							BgL_arg2123z00_3394 =
								BGl_getzd2typezd2zztype_typeofz00(
								((BgL_nodez00_bglt)
									((BgL_vallocz00_bglt) BgL_nodez00_3099)), ((bool_t) 0));
							BgL_arg2116z00_3392 =
								BGl_shapez00zztools_shapez00(((obj_t) BgL_arg2123z00_3394));
						}
						{	/* Ast/dump.scm 268 */
							BgL_typez00_bglt BgL_arg2124z00_3395;

							BgL_arg2124z00_3395 =
								(((BgL_vallocz00_bglt) COBJECT(
										((BgL_vallocz00_bglt) BgL_nodez00_3099)))->BgL_ftypez00);
							BgL_arg2117z00_3393 =
								BGl_shapez00zztools_shapez00(((obj_t) BgL_arg2124z00_3395));
						}
						{	/* Ast/dump.scm 266 */
							obj_t BgL_list2118z00_3396;

							{	/* Ast/dump.scm 266 */
								obj_t BgL_arg2119z00_3397;

								{	/* Ast/dump.scm 266 */
									obj_t BgL_arg2120z00_3398;

									{	/* Ast/dump.scm 266 */
										obj_t BgL_arg2121z00_3399;

										{	/* Ast/dump.scm 266 */
											obj_t BgL_arg2122z00_3400;

											BgL_arg2122z00_3400 =
												MAKE_YOUNG_PAIR(BGl_string2448z00zzast_dumpz00, BNIL);
											BgL_arg2121z00_3399 =
												MAKE_YOUNG_PAIR(BgL_arg2117z00_3393,
												BgL_arg2122z00_3400);
										}
										BgL_arg2120z00_3398 =
											MAKE_YOUNG_PAIR(BGl_string2449z00zzast_dumpz00,
											BgL_arg2121z00_3399);
									}
									BgL_arg2119z00_3397 =
										MAKE_YOUNG_PAIR(BgL_arg2116z00_3392, BgL_arg2120z00_3398);
								}
								BgL_list2118z00_3396 =
									MAKE_YOUNG_PAIR(BGl_string2450z00zzast_dumpz00,
									BgL_arg2119z00_3397);
							}
							BgL_arg2115z00_3391 =
								BGl_stringzd2appendzd2zz__r4_strings_6_7z00
								(BgL_list2118z00_3396);
						}
					}
					BgL_arg2113z00_3389 = bstring_to_symbol(BgL_arg2115z00_3391);
				}
				{	/* Ast/dump.scm 269 */
					obj_t BgL_arg2125z00_3401;

					{	/* Ast/dump.scm 269 */
						obj_t BgL_l1319z00_3402;

						BgL_l1319z00_3402 =
							(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt)
										((BgL_vallocz00_bglt) BgL_nodez00_3099))))->BgL_exprza2za2);
						if (NULLP(BgL_l1319z00_3402))
							{	/* Ast/dump.scm 269 */
								BgL_arg2125z00_3401 = BNIL;
							}
						else
							{	/* Ast/dump.scm 269 */
								obj_t BgL_head1321z00_3403;

								{	/* Ast/dump.scm 269 */
									obj_t BgL_arg2132z00_3404;

									{	/* Ast/dump.scm 269 */
										obj_t BgL_arg2133z00_3405;

										BgL_arg2133z00_3405 = CAR(BgL_l1319z00_3402);
										BgL_arg2132z00_3404 =
											BGl_nodezd2ze3sexpz31zzast_dumpz00(
											((BgL_nodez00_bglt) BgL_arg2133z00_3405));
									}
									BgL_head1321z00_3403 =
										MAKE_YOUNG_PAIR(BgL_arg2132z00_3404, BNIL);
								}
								{	/* Ast/dump.scm 269 */
									obj_t BgL_g1324z00_3406;

									BgL_g1324z00_3406 = CDR(BgL_l1319z00_3402);
									{
										obj_t BgL_l1319z00_3408;
										obj_t BgL_tail1322z00_3409;

										BgL_l1319z00_3408 = BgL_g1324z00_3406;
										BgL_tail1322z00_3409 = BgL_head1321z00_3403;
									BgL_zc3z04anonymousza32127ze3z87_3407:
										if (NULLP(BgL_l1319z00_3408))
											{	/* Ast/dump.scm 269 */
												BgL_arg2125z00_3401 = BgL_head1321z00_3403;
											}
										else
											{	/* Ast/dump.scm 269 */
												obj_t BgL_newtail1323z00_3410;

												{	/* Ast/dump.scm 269 */
													obj_t BgL_arg2130z00_3411;

													{	/* Ast/dump.scm 269 */
														obj_t BgL_arg2131z00_3412;

														BgL_arg2131z00_3412 =
															CAR(((obj_t) BgL_l1319z00_3408));
														BgL_arg2130z00_3411 =
															BGl_nodezd2ze3sexpz31zzast_dumpz00(
															((BgL_nodez00_bglt) BgL_arg2131z00_3412));
													}
													BgL_newtail1323z00_3410 =
														MAKE_YOUNG_PAIR(BgL_arg2130z00_3411, BNIL);
												}
												SET_CDR(BgL_tail1322z00_3409, BgL_newtail1323z00_3410);
												{	/* Ast/dump.scm 269 */
													obj_t BgL_arg2129z00_3413;

													BgL_arg2129z00_3413 =
														CDR(((obj_t) BgL_l1319z00_3408));
													{
														obj_t BgL_tail1322z00_4489;
														obj_t BgL_l1319z00_4488;

														BgL_l1319z00_4488 = BgL_arg2129z00_3413;
														BgL_tail1322z00_4489 = BgL_newtail1323z00_3410;
														BgL_tail1322z00_3409 = BgL_tail1322z00_4489;
														BgL_l1319z00_3408 = BgL_l1319z00_4488;
														goto BgL_zc3z04anonymousza32127ze3z87_3407;
													}
												}
											}
									}
								}
							}
					}
					BgL_arg2114z00_3390 =
						BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_arg2125z00_3401,
						BNIL);
				}
				return MAKE_YOUNG_PAIR(BgL_arg2113z00_3389, BgL_arg2114z00_3390);
			}
		}

	}



/* &node->sexp-vset!1382 */
	obj_t BGl_z62nodezd2ze3sexpzd2vsetz121382z93zzast_dumpz00(obj_t
		BgL_envz00_3100, obj_t BgL_nodez00_3101)
	{
		{	/* Ast/dump.scm 251 */
			{	/* Ast/dump.scm 254 */
				obj_t BgL_idz00_3415;

				{	/* Ast/dump.scm 254 */
					obj_t BgL_arg2110z00_3416;
					BgL_typez00_bglt BgL_arg2111z00_3417;

					if (
						(((BgL_vsetz12z12_bglt) COBJECT(
									((BgL_vsetz12z12_bglt) BgL_nodez00_3101)))->BgL_unsafez00))
						{	/* Ast/dump.scm 254 */
							BgL_arg2110z00_3416 = CNST_TABLE_REF(24);
						}
					else
						{	/* Ast/dump.scm 254 */
							BgL_arg2110z00_3416 = CNST_TABLE_REF(25);
						}
					BgL_arg2111z00_3417 =
						BGl_getzd2typezd2zztype_typeofz00(
						((BgL_nodez00_bglt)
							((BgL_vsetz12z12_bglt) BgL_nodez00_3101)), ((bool_t) 0));
					BgL_idz00_3415 =
						BGl_shapezd2typedzd2nodez00zzast_dumpz00(BgL_arg2110z00_3416,
						BgL_arg2111z00_3417);
				}
				if (CBOOL(BGl_za2typezd2shapezf3za2z21zzengine_paramz00))
					{	/* Ast/dump.scm 256 */
						obj_t BgL_arg2086z00_3418;

						{	/* Ast/dump.scm 256 */
							obj_t BgL_arg2087z00_3419;
							obj_t BgL_arg2088z00_3420;

							{	/* Ast/dump.scm 256 */
								obj_t BgL_v1306z00_3421;

								BgL_v1306z00_3421 = create_vector(1L);
								{	/* Ast/dump.scm 256 */
									obj_t BgL_arg2089z00_3422;

									{	/* Ast/dump.scm 256 */
										BgL_typez00_bglt BgL_arg2090z00_3423;

										BgL_arg2090z00_3423 =
											(((BgL_vsetz12z12_bglt) COBJECT(
													((BgL_vsetz12z12_bglt) BgL_nodez00_3101)))->
											BgL_ftypez00);
										BgL_arg2089z00_3422 =
											BGl_shapez00zztools_shapez00(((obj_t)
												BgL_arg2090z00_3423));
									}
									VECTOR_SET(BgL_v1306z00_3421, 0L, BgL_arg2089z00_3422);
								}
								BgL_arg2087z00_3419 = BgL_v1306z00_3421;
							}
							{	/* Ast/dump.scm 256 */
								obj_t BgL_arg2091z00_3424;

								{	/* Ast/dump.scm 256 */
									obj_t BgL_l1307z00_3425;

									BgL_l1307z00_3425 =
										(((BgL_externz00_bglt) COBJECT(
												((BgL_externz00_bglt)
													((BgL_vsetz12z12_bglt) BgL_nodez00_3101))))->
										BgL_exprza2za2);
									if (NULLP(BgL_l1307z00_3425))
										{	/* Ast/dump.scm 256 */
											BgL_arg2091z00_3424 = BNIL;
										}
									else
										{	/* Ast/dump.scm 256 */
											obj_t BgL_head1309z00_3426;

											{	/* Ast/dump.scm 256 */
												obj_t BgL_arg2098z00_3427;

												{	/* Ast/dump.scm 256 */
													obj_t BgL_arg2099z00_3428;

													BgL_arg2099z00_3428 = CAR(BgL_l1307z00_3425);
													BgL_arg2098z00_3427 =
														BGl_nodezd2ze3sexpz31zzast_dumpz00(
														((BgL_nodez00_bglt) BgL_arg2099z00_3428));
												}
												BgL_head1309z00_3426 =
													MAKE_YOUNG_PAIR(BgL_arg2098z00_3427, BNIL);
											}
											{	/* Ast/dump.scm 256 */
												obj_t BgL_g1312z00_3429;

												BgL_g1312z00_3429 = CDR(BgL_l1307z00_3425);
												{
													obj_t BgL_l1307z00_3431;
													obj_t BgL_tail1310z00_3432;

													BgL_l1307z00_3431 = BgL_g1312z00_3429;
													BgL_tail1310z00_3432 = BgL_head1309z00_3426;
												BgL_zc3z04anonymousza32093ze3z87_3430:
													if (NULLP(BgL_l1307z00_3431))
														{	/* Ast/dump.scm 256 */
															BgL_arg2091z00_3424 = BgL_head1309z00_3426;
														}
													else
														{	/* Ast/dump.scm 256 */
															obj_t BgL_newtail1311z00_3433;

															{	/* Ast/dump.scm 256 */
																obj_t BgL_arg2096z00_3434;

																{	/* Ast/dump.scm 256 */
																	obj_t BgL_arg2097z00_3435;

																	BgL_arg2097z00_3435 =
																		CAR(((obj_t) BgL_l1307z00_3431));
																	BgL_arg2096z00_3434 =
																		BGl_nodezd2ze3sexpz31zzast_dumpz00(
																		((BgL_nodez00_bglt) BgL_arg2097z00_3435));
																}
																BgL_newtail1311z00_3433 =
																	MAKE_YOUNG_PAIR(BgL_arg2096z00_3434, BNIL);
															}
															SET_CDR(BgL_tail1310z00_3432,
																BgL_newtail1311z00_3433);
															{	/* Ast/dump.scm 256 */
																obj_t BgL_arg2095z00_3436;

																BgL_arg2095z00_3436 =
																	CDR(((obj_t) BgL_l1307z00_3431));
																{
																	obj_t BgL_tail1310z00_4530;
																	obj_t BgL_l1307z00_4529;

																	BgL_l1307z00_4529 = BgL_arg2095z00_3436;
																	BgL_tail1310z00_4530 =
																		BgL_newtail1311z00_3433;
																	BgL_tail1310z00_3432 = BgL_tail1310z00_4530;
																	BgL_l1307z00_3431 = BgL_l1307z00_4529;
																	goto BgL_zc3z04anonymousza32093ze3z87_3430;
																}
															}
														}
												}
											}
										}
								}
								BgL_arg2088z00_3420 =
									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
									(BgL_arg2091z00_3424, BNIL);
							}
							BgL_arg2086z00_3418 =
								MAKE_YOUNG_PAIR(BgL_arg2087z00_3419, BgL_arg2088z00_3420);
						}
						return MAKE_YOUNG_PAIR(BgL_idz00_3415, BgL_arg2086z00_3418);
					}
				else
					{	/* Ast/dump.scm 257 */
						obj_t BgL_arg2100z00_3437;

						{	/* Ast/dump.scm 257 */
							obj_t BgL_arg2101z00_3438;

							{	/* Ast/dump.scm 257 */
								obj_t BgL_l1313z00_3439;

								BgL_l1313z00_3439 =
									(((BgL_externz00_bglt) COBJECT(
											((BgL_externz00_bglt)
												((BgL_vsetz12z12_bglt) BgL_nodez00_3101))))->
									BgL_exprza2za2);
								if (NULLP(BgL_l1313z00_3439))
									{	/* Ast/dump.scm 257 */
										BgL_arg2101z00_3438 = BNIL;
									}
								else
									{	/* Ast/dump.scm 257 */
										obj_t BgL_head1315z00_3440;

										{	/* Ast/dump.scm 257 */
											obj_t BgL_arg2108z00_3441;

											{	/* Ast/dump.scm 257 */
												obj_t BgL_arg2109z00_3442;

												BgL_arg2109z00_3442 = CAR(BgL_l1313z00_3439);
												BgL_arg2108z00_3441 =
													BGl_nodezd2ze3sexpz31zzast_dumpz00(
													((BgL_nodez00_bglt) BgL_arg2109z00_3442));
											}
											BgL_head1315z00_3440 =
												MAKE_YOUNG_PAIR(BgL_arg2108z00_3441, BNIL);
										}
										{	/* Ast/dump.scm 257 */
											obj_t BgL_g1318z00_3443;

											BgL_g1318z00_3443 = CDR(BgL_l1313z00_3439);
											{
												obj_t BgL_l1313z00_3445;
												obj_t BgL_tail1316z00_3446;

												BgL_l1313z00_3445 = BgL_g1318z00_3443;
												BgL_tail1316z00_3446 = BgL_head1315z00_3440;
											BgL_zc3z04anonymousza32103ze3z87_3444:
												if (NULLP(BgL_l1313z00_3445))
													{	/* Ast/dump.scm 257 */
														BgL_arg2101z00_3438 = BgL_head1315z00_3440;
													}
												else
													{	/* Ast/dump.scm 257 */
														obj_t BgL_newtail1317z00_3447;

														{	/* Ast/dump.scm 257 */
															obj_t BgL_arg2106z00_3448;

															{	/* Ast/dump.scm 257 */
																obj_t BgL_arg2107z00_3449;

																BgL_arg2107z00_3449 =
																	CAR(((obj_t) BgL_l1313z00_3445));
																BgL_arg2106z00_3448 =
																	BGl_nodezd2ze3sexpz31zzast_dumpz00(
																	((BgL_nodez00_bglt) BgL_arg2107z00_3449));
															}
															BgL_newtail1317z00_3447 =
																MAKE_YOUNG_PAIR(BgL_arg2106z00_3448, BNIL);
														}
														SET_CDR(BgL_tail1316z00_3446,
															BgL_newtail1317z00_3447);
														{	/* Ast/dump.scm 257 */
															obj_t BgL_arg2105z00_3450;

															BgL_arg2105z00_3450 =
																CDR(((obj_t) BgL_l1313z00_3445));
															{
																obj_t BgL_tail1316z00_4555;
																obj_t BgL_l1313z00_4554;

																BgL_l1313z00_4554 = BgL_arg2105z00_3450;
																BgL_tail1316z00_4555 = BgL_newtail1317z00_3447;
																BgL_tail1316z00_3446 = BgL_tail1316z00_4555;
																BgL_l1313z00_3445 = BgL_l1313z00_4554;
																goto BgL_zc3z04anonymousza32103ze3z87_3444;
															}
														}
													}
											}
										}
									}
							}
							BgL_arg2100z00_3437 =
								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
								(BgL_arg2101z00_3438, BNIL);
						}
						return MAKE_YOUNG_PAIR(BgL_idz00_3415, BgL_arg2100z00_3437);
					}
			}
		}

	}



/* &node->sexp-vref1380 */
	obj_t BGl_z62nodezd2ze3sexpzd2vref1380z81zzast_dumpz00(obj_t BgL_envz00_3102,
		obj_t BgL_nodez00_3103)
	{
		{	/* Ast/dump.scm 235 */
			{	/* Ast/dump.scm 238 */
				obj_t BgL_idz00_3452;

				if (
					(((BgL_vrefz00_bglt) COBJECT(
								((BgL_vrefz00_bglt) BgL_nodez00_3103)))->BgL_unsafez00))
					{	/* Ast/dump.scm 238 */
						BgL_idz00_3452 = CNST_TABLE_REF(26);
					}
				else
					{	/* Ast/dump.scm 238 */
						BgL_idz00_3452 = CNST_TABLE_REF(27);
					}
				if (CBOOL(BGl_za2typezd2shapezf3za2z21zzengine_paramz00))
					{	/* Ast/dump.scm 241 */
						obj_t BgL_arg2046z00_3453;
						obj_t BgL_arg2047z00_3454;

						{	/* Ast/dump.scm 241 */
							obj_t BgL_arg2048z00_3455;

							{	/* Ast/dump.scm 241 */
								obj_t BgL_arg2049z00_3456;
								obj_t BgL_arg2050z00_3457;
								obj_t BgL_arg2051z00_3458;

								{	/* Ast/dump.scm 241 */
									obj_t BgL_arg1455z00_3459;

									BgL_arg1455z00_3459 = SYMBOL_TO_STRING(BgL_idz00_3452);
									BgL_arg2049z00_3456 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_3459);
								}
								{	/* Ast/dump.scm 243 */
									BgL_typez00_bglt BgL_arg2060z00_3460;

									BgL_arg2060z00_3460 =
										BGl_getzd2typezd2zztype_typeofz00(
										((BgL_nodez00_bglt)
											((BgL_vrefz00_bglt) BgL_nodez00_3103)), ((bool_t) 0));
									BgL_arg2050z00_3457 =
										BGl_shapez00zztools_shapez00(((obj_t) BgL_arg2060z00_3460));
								}
								{	/* Ast/dump.scm 244 */
									BgL_typez00_bglt BgL_arg2061z00_3461;

									BgL_arg2061z00_3461 =
										(((BgL_vrefz00_bglt) COBJECT(
												((BgL_vrefz00_bglt) BgL_nodez00_3103)))->BgL_ftypez00);
									BgL_arg2051z00_3458 =
										BGl_shapez00zztools_shapez00(((obj_t) BgL_arg2061z00_3461));
								}
								{	/* Ast/dump.scm 241 */
									obj_t BgL_list2052z00_3462;

									{	/* Ast/dump.scm 241 */
										obj_t BgL_arg2055z00_3463;

										{	/* Ast/dump.scm 241 */
											obj_t BgL_arg2056z00_3464;

											{	/* Ast/dump.scm 241 */
												obj_t BgL_arg2057z00_3465;

												{	/* Ast/dump.scm 241 */
													obj_t BgL_arg2058z00_3466;

													{	/* Ast/dump.scm 241 */
														obj_t BgL_arg2059z00_3467;

														BgL_arg2059z00_3467 =
															MAKE_YOUNG_PAIR(BGl_string2448z00zzast_dumpz00,
															BNIL);
														BgL_arg2058z00_3466 =
															MAKE_YOUNG_PAIR(BgL_arg2051z00_3458,
															BgL_arg2059z00_3467);
													}
													BgL_arg2057z00_3465 =
														MAKE_YOUNG_PAIR(BGl_string2449z00zzast_dumpz00,
														BgL_arg2058z00_3466);
												}
												BgL_arg2056z00_3464 =
													MAKE_YOUNG_PAIR(BgL_arg2050z00_3457,
													BgL_arg2057z00_3465);
											}
											BgL_arg2055z00_3463 =
												MAKE_YOUNG_PAIR(BGl_string2407z00zzast_dumpz00,
												BgL_arg2056z00_3464);
										}
										BgL_list2052z00_3462 =
											MAKE_YOUNG_PAIR(BgL_arg2049z00_3456, BgL_arg2055z00_3463);
									}
									BgL_arg2048z00_3455 =
										BGl_stringzd2appendzd2zz__r4_strings_6_7z00
										(BgL_list2052z00_3462);
								}
							}
							BgL_arg2046z00_3453 = bstring_to_symbol(BgL_arg2048z00_3455);
						}
						{	/* Ast/dump.scm 245 */
							obj_t BgL_arg2062z00_3468;

							{	/* Ast/dump.scm 245 */
								obj_t BgL_l1294z00_3469;

								BgL_l1294z00_3469 =
									(((BgL_externz00_bglt) COBJECT(
											((BgL_externz00_bglt)
												((BgL_vrefz00_bglt) BgL_nodez00_3103))))->
									BgL_exprza2za2);
								if (NULLP(BgL_l1294z00_3469))
									{	/* Ast/dump.scm 245 */
										BgL_arg2062z00_3468 = BNIL;
									}
								else
									{	/* Ast/dump.scm 245 */
										obj_t BgL_head1296z00_3470;

										{	/* Ast/dump.scm 245 */
											obj_t BgL_arg2070z00_3471;

											{	/* Ast/dump.scm 245 */
												obj_t BgL_arg2072z00_3472;

												BgL_arg2072z00_3472 = CAR(BgL_l1294z00_3469);
												BgL_arg2070z00_3471 =
													BGl_nodezd2ze3sexpz31zzast_dumpz00(
													((BgL_nodez00_bglt) BgL_arg2072z00_3472));
											}
											BgL_head1296z00_3470 =
												MAKE_YOUNG_PAIR(BgL_arg2070z00_3471, BNIL);
										}
										{	/* Ast/dump.scm 245 */
											obj_t BgL_g1299z00_3473;

											BgL_g1299z00_3473 = CDR(BgL_l1294z00_3469);
											{
												obj_t BgL_l1294z00_3475;
												obj_t BgL_tail1297z00_3476;

												BgL_l1294z00_3475 = BgL_g1299z00_3473;
												BgL_tail1297z00_3476 = BgL_head1296z00_3470;
											BgL_zc3z04anonymousza32064ze3z87_3474:
												if (NULLP(BgL_l1294z00_3475))
													{	/* Ast/dump.scm 245 */
														BgL_arg2062z00_3468 = BgL_head1296z00_3470;
													}
												else
													{	/* Ast/dump.scm 245 */
														obj_t BgL_newtail1298z00_3477;

														{	/* Ast/dump.scm 245 */
															obj_t BgL_arg2068z00_3478;

															{	/* Ast/dump.scm 245 */
																obj_t BgL_arg2069z00_3479;

																BgL_arg2069z00_3479 =
																	CAR(((obj_t) BgL_l1294z00_3475));
																BgL_arg2068z00_3478 =
																	BGl_nodezd2ze3sexpz31zzast_dumpz00(
																	((BgL_nodez00_bglt) BgL_arg2069z00_3479));
															}
															BgL_newtail1298z00_3477 =
																MAKE_YOUNG_PAIR(BgL_arg2068z00_3478, BNIL);
														}
														SET_CDR(BgL_tail1297z00_3476,
															BgL_newtail1298z00_3477);
														{	/* Ast/dump.scm 245 */
															obj_t BgL_arg2067z00_3480;

															BgL_arg2067z00_3480 =
																CDR(((obj_t) BgL_l1294z00_3475));
															{
																obj_t BgL_tail1297z00_4605;
																obj_t BgL_l1294z00_4604;

																BgL_l1294z00_4604 = BgL_arg2067z00_3480;
																BgL_tail1297z00_4605 = BgL_newtail1298z00_3477;
																BgL_tail1297z00_3476 = BgL_tail1297z00_4605;
																BgL_l1294z00_3475 = BgL_l1294z00_4604;
																goto BgL_zc3z04anonymousza32064ze3z87_3474;
															}
														}
													}
											}
										}
									}
							}
							BgL_arg2047z00_3454 =
								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
								(BgL_arg2062z00_3468, BNIL);
						}
						return MAKE_YOUNG_PAIR(BgL_arg2046z00_3453, BgL_arg2047z00_3454);
					}
				else
					{	/* Ast/dump.scm 246 */
						obj_t BgL_arg2074z00_3481;

						{	/* Ast/dump.scm 246 */
							obj_t BgL_arg2075z00_3482;

							{	/* Ast/dump.scm 246 */
								obj_t BgL_l1300z00_3483;

								BgL_l1300z00_3483 =
									(((BgL_externz00_bglt) COBJECT(
											((BgL_externz00_bglt)
												((BgL_vrefz00_bglt) BgL_nodez00_3103))))->
									BgL_exprza2za2);
								if (NULLP(BgL_l1300z00_3483))
									{	/* Ast/dump.scm 246 */
										BgL_arg2075z00_3482 = BNIL;
									}
								else
									{	/* Ast/dump.scm 246 */
										obj_t BgL_head1302z00_3484;

										{	/* Ast/dump.scm 246 */
											obj_t BgL_arg2082z00_3485;

											{	/* Ast/dump.scm 246 */
												obj_t BgL_arg2083z00_3486;

												BgL_arg2083z00_3486 = CAR(BgL_l1300z00_3483);
												BgL_arg2082z00_3485 =
													BGl_nodezd2ze3sexpz31zzast_dumpz00(
													((BgL_nodez00_bglt) BgL_arg2083z00_3486));
											}
											BgL_head1302z00_3484 =
												MAKE_YOUNG_PAIR(BgL_arg2082z00_3485, BNIL);
										}
										{	/* Ast/dump.scm 246 */
											obj_t BgL_g1305z00_3487;

											BgL_g1305z00_3487 = CDR(BgL_l1300z00_3483);
											{
												obj_t BgL_l1300z00_3489;
												obj_t BgL_tail1303z00_3490;

												BgL_l1300z00_3489 = BgL_g1305z00_3487;
												BgL_tail1303z00_3490 = BgL_head1302z00_3484;
											BgL_zc3z04anonymousza32077ze3z87_3488:
												if (NULLP(BgL_l1300z00_3489))
													{	/* Ast/dump.scm 246 */
														BgL_arg2075z00_3482 = BgL_head1302z00_3484;
													}
												else
													{	/* Ast/dump.scm 246 */
														obj_t BgL_newtail1304z00_3491;

														{	/* Ast/dump.scm 246 */
															obj_t BgL_arg2080z00_3492;

															{	/* Ast/dump.scm 246 */
																obj_t BgL_arg2081z00_3493;

																BgL_arg2081z00_3493 =
																	CAR(((obj_t) BgL_l1300z00_3489));
																BgL_arg2080z00_3492 =
																	BGl_nodezd2ze3sexpz31zzast_dumpz00(
																	((BgL_nodez00_bglt) BgL_arg2081z00_3493));
															}
															BgL_newtail1304z00_3491 =
																MAKE_YOUNG_PAIR(BgL_arg2080z00_3492, BNIL);
														}
														SET_CDR(BgL_tail1303z00_3490,
															BgL_newtail1304z00_3491);
														{	/* Ast/dump.scm 246 */
															obj_t BgL_arg2079z00_3494;

															BgL_arg2079z00_3494 =
																CDR(((obj_t) BgL_l1300z00_3489));
															{
																obj_t BgL_tail1303z00_4629;
																obj_t BgL_l1300z00_4628;

																BgL_l1300z00_4628 = BgL_arg2079z00_3494;
																BgL_tail1303z00_4629 = BgL_newtail1304z00_3491;
																BgL_tail1303z00_3490 = BgL_tail1303z00_4629;
																BgL_l1300z00_3489 = BgL_l1300z00_4628;
																goto BgL_zc3z04anonymousza32077ze3z87_3488;
															}
														}
													}
											}
										}
									}
							}
							BgL_arg2074z00_3481 =
								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
								(BgL_arg2075z00_3482, BNIL);
						}
						return MAKE_YOUNG_PAIR(BgL_idz00_3452, BgL_arg2074z00_3481);
					}
			}
		}

	}



/* &node->sexp-vlength1377 */
	obj_t BGl_z62nodezd2ze3sexpzd2vlength1377z81zzast_dumpz00(obj_t
		BgL_envz00_3104, obj_t BgL_nodez00_3105)
	{
		{	/* Ast/dump.scm 224 */
			{	/* Ast/dump.scm 227 */
				obj_t BgL_arg2030z00_3496;
				obj_t BgL_arg2031z00_3497;

				BgL_arg2030z00_3496 =
					(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_vlengthz00_bglt) BgL_nodez00_3105))))->BgL_locz00);
				{	/* Ast/dump.scm 228 */
					obj_t BgL_arg2033z00_3498;
					obj_t BgL_arg2034z00_3499;

					{	/* Ast/dump.scm 228 */
						BgL_typez00_bglt BgL_arg2036z00_3500;

						BgL_arg2036z00_3500 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_vlengthz00_bglt) BgL_nodez00_3105))))->BgL_typez00);
						{	/* Ast/dump.scm 228 */
							obj_t BgL_idz00_3501;

							BgL_idz00_3501 = CNST_TABLE_REF(28);
							if (CBOOL(BGl_za2typenodezd2shapezf3za2z21zzengine_paramz00))
								{	/* Ast/dump.scm 442 */
									obj_t BgL_arg1489z00_3502;

									{	/* Ast/dump.scm 442 */
										obj_t BgL_arg1502z00_3503;
										obj_t BgL_arg1509z00_3504;

										{	/* Ast/dump.scm 442 */
											obj_t BgL_arg1455z00_3505;

											BgL_arg1455z00_3505 = SYMBOL_TO_STRING(BgL_idz00_3501);
											BgL_arg1502z00_3503 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_3505);
										}
										BgL_arg1509z00_3504 =
											BGl_shapez00zztools_shapez00(
											((obj_t) BgL_arg2036z00_3500));
										BgL_arg1489z00_3502 =
											string_append_3(BgL_arg1502z00_3503,
											BGl_string2407z00zzast_dumpz00, BgL_arg1509z00_3504);
									}
									BgL_arg2033z00_3498 = bstring_to_symbol(BgL_arg1489z00_3502);
								}
							else
								{	/* Ast/dump.scm 441 */
									BgL_arg2033z00_3498 = BgL_idz00_3501;
								}
						}
					}
					{	/* Ast/dump.scm 229 */
						obj_t BgL_arg2037z00_3506;
						obj_t BgL_arg2038z00_3507;

						{	/* Ast/dump.scm 229 */
							obj_t BgL_arg2039z00_3508;

							BgL_arg2039z00_3508 =
								MAKE_YOUNG_PAIR(BGl_shapez00zztools_shapez00(
									(((BgL_vlengthz00_bglt) COBJECT(
												((BgL_vlengthz00_bglt) BgL_nodez00_3105)))->
										BgL_ftypez00)), BNIL);
							BgL_arg2037z00_3506 =
								MAKE_YOUNG_PAIR(BGl_string2451z00zzast_dumpz00,
								BgL_arg2039z00_3508);
						}
						{	/* Ast/dump.scm 230 */
							obj_t BgL_arg2042z00_3509;

							{	/* Ast/dump.scm 230 */
								obj_t BgL_arg2044z00_3510;

								{	/* Ast/dump.scm 230 */
									obj_t BgL_pairz00_3511;

									BgL_pairz00_3511 =
										(((BgL_externz00_bglt) COBJECT(
												((BgL_externz00_bglt)
													((BgL_vlengthz00_bglt) BgL_nodez00_3105))))->
										BgL_exprza2za2);
									BgL_arg2044z00_3510 = CAR(BgL_pairz00_3511);
								}
								BgL_arg2042z00_3509 =
									BGl_nodezd2ze3sexpz31zzast_dumpz00(
									((BgL_nodez00_bglt) BgL_arg2044z00_3510));
							}
							BgL_arg2038z00_3507 = MAKE_YOUNG_PAIR(BgL_arg2042z00_3509, BNIL);
						}
						BgL_arg2034z00_3499 =
							MAKE_YOUNG_PAIR(BgL_arg2037z00_3506, BgL_arg2038z00_3507);
					}
					BgL_arg2031z00_3497 =
						MAKE_YOUNG_PAIR(BgL_arg2033z00_3498, BgL_arg2034z00_3499);
				}
				return
					BGl_locationzd2shapezd2zztools_locationz00(BgL_arg2030z00_3496,
					BgL_arg2031z00_3497);
			}
		}

	}



/* &node->sexp-new1374 */
	obj_t BGl_z62nodezd2ze3sexpzd2new1374z81zzast_dumpz00(obj_t BgL_envz00_3106,
		obj_t BgL_nodez00_3107)
	{
		{	/* Ast/dump.scm 214 */
			{	/* Ast/dump.scm 217 */
				obj_t BgL_arg2011z00_3513;
				obj_t BgL_arg2012z00_3514;

				BgL_arg2011z00_3513 =
					(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_newz00_bglt) BgL_nodez00_3107))))->BgL_locz00);
				{	/* Ast/dump.scm 218 */
					obj_t BgL_arg2013z00_3515;
					obj_t BgL_arg2014z00_3516;

					{	/* Ast/dump.scm 218 */
						BgL_typez00_bglt BgL_arg2015z00_3517;

						BgL_arg2015z00_3517 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_newz00_bglt) BgL_nodez00_3107))))->BgL_typez00);
						{	/* Ast/dump.scm 218 */
							obj_t BgL_idz00_3518;

							BgL_idz00_3518 = CNST_TABLE_REF(29);
							if (CBOOL(BGl_za2typenodezd2shapezf3za2z21zzengine_paramz00))
								{	/* Ast/dump.scm 442 */
									obj_t BgL_arg1489z00_3519;

									{	/* Ast/dump.scm 442 */
										obj_t BgL_arg1502z00_3520;
										obj_t BgL_arg1509z00_3521;

										{	/* Ast/dump.scm 442 */
											obj_t BgL_arg1455z00_3522;

											BgL_arg1455z00_3522 = SYMBOL_TO_STRING(BgL_idz00_3518);
											BgL_arg1502z00_3520 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_3522);
										}
										BgL_arg1509z00_3521 =
											BGl_shapez00zztools_shapez00(
											((obj_t) BgL_arg2015z00_3517));
										BgL_arg1489z00_3519 =
											string_append_3(BgL_arg1502z00_3520,
											BGl_string2407z00zzast_dumpz00, BgL_arg1509z00_3521);
									}
									BgL_arg2013z00_3515 = bstring_to_symbol(BgL_arg1489z00_3519);
								}
							else
								{	/* Ast/dump.scm 441 */
									BgL_arg2013z00_3515 = BgL_idz00_3518;
								}
						}
					}
					{	/* Ast/dump.scm 219 */
						obj_t BgL_arg2016z00_3523;
						obj_t BgL_arg2017z00_3524;

						{	/* Ast/dump.scm 219 */
							obj_t BgL_arg2018z00_3525;

							BgL_arg2018z00_3525 =
								(((BgL_typez00_bglt) COBJECT(
										(((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt)
														((BgL_newz00_bglt) BgL_nodez00_3107))))->
											BgL_typez00)))->BgL_idz00);
							BgL_arg2016z00_3523 = MAKE_YOUNG_PAIR(BgL_arg2018z00_3525, BNIL);
						}
						{	/* Ast/dump.scm 219 */
							obj_t BgL_arg2020z00_3526;

							{	/* Ast/dump.scm 219 */
								obj_t BgL_l1288z00_3527;

								BgL_l1288z00_3527 =
									(((BgL_externz00_bglt) COBJECT(
											((BgL_externz00_bglt)
												((BgL_newz00_bglt) BgL_nodez00_3107))))->
									BgL_exprza2za2);
								if (NULLP(BgL_l1288z00_3527))
									{	/* Ast/dump.scm 219 */
										BgL_arg2020z00_3526 = BNIL;
									}
								else
									{	/* Ast/dump.scm 219 */
										obj_t BgL_head1290z00_3528;

										{	/* Ast/dump.scm 219 */
											obj_t BgL_arg2027z00_3529;

											{	/* Ast/dump.scm 219 */
												obj_t BgL_arg2029z00_3530;

												BgL_arg2029z00_3530 = CAR(BgL_l1288z00_3527);
												BgL_arg2027z00_3529 =
													BGl_nodezd2ze3sexpz31zzast_dumpz00(
													((BgL_nodez00_bglt) BgL_arg2029z00_3530));
											}
											BgL_head1290z00_3528 =
												MAKE_YOUNG_PAIR(BgL_arg2027z00_3529, BNIL);
										}
										{	/* Ast/dump.scm 219 */
											obj_t BgL_g1293z00_3531;

											BgL_g1293z00_3531 = CDR(BgL_l1288z00_3527);
											{
												obj_t BgL_l1288z00_3533;
												obj_t BgL_tail1291z00_3534;

												BgL_l1288z00_3533 = BgL_g1293z00_3531;
												BgL_tail1291z00_3534 = BgL_head1290z00_3528;
											BgL_zc3z04anonymousza32022ze3z87_3532:
												if (NULLP(BgL_l1288z00_3533))
													{	/* Ast/dump.scm 219 */
														BgL_arg2020z00_3526 = BgL_head1290z00_3528;
													}
												else
													{	/* Ast/dump.scm 219 */
														obj_t BgL_newtail1292z00_3535;

														{	/* Ast/dump.scm 219 */
															obj_t BgL_arg2025z00_3536;

															{	/* Ast/dump.scm 219 */
																obj_t BgL_arg2026z00_3537;

																BgL_arg2026z00_3537 =
																	CAR(((obj_t) BgL_l1288z00_3533));
																BgL_arg2025z00_3536 =
																	BGl_nodezd2ze3sexpz31zzast_dumpz00(
																	((BgL_nodez00_bglt) BgL_arg2026z00_3537));
															}
															BgL_newtail1292z00_3535 =
																MAKE_YOUNG_PAIR(BgL_arg2025z00_3536, BNIL);
														}
														SET_CDR(BgL_tail1291z00_3534,
															BgL_newtail1292z00_3535);
														{	/* Ast/dump.scm 219 */
															obj_t BgL_arg2024z00_3538;

															BgL_arg2024z00_3538 =
																CDR(((obj_t) BgL_l1288z00_3533));
															{
																obj_t BgL_tail1291z00_4703;
																obj_t BgL_l1288z00_4702;

																BgL_l1288z00_4702 = BgL_arg2024z00_3538;
																BgL_tail1291z00_4703 = BgL_newtail1292z00_3535;
																BgL_tail1291z00_3534 = BgL_tail1291z00_4703;
																BgL_l1288z00_3533 = BgL_l1288z00_4702;
																goto BgL_zc3z04anonymousza32022ze3z87_3532;
															}
														}
													}
											}
										}
									}
							}
							BgL_arg2017z00_3524 =
								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
								(BgL_arg2020z00_3526, BNIL);
						}
						BgL_arg2014z00_3516 =
							MAKE_YOUNG_PAIR(BgL_arg2016z00_3523, BgL_arg2017z00_3524);
					}
					BgL_arg2012z00_3514 =
						MAKE_YOUNG_PAIR(BgL_arg2013z00_3515, BgL_arg2014z00_3516);
				}
				return
					BGl_locationzd2shapezd2zztools_locationz00(BgL_arg2011z00_3513,
					BgL_arg2012z00_3514);
			}
		}

	}



/* &node->sexp-setfield1371 */
	obj_t BGl_z62nodezd2ze3sexpzd2setfield1371z81zzast_dumpz00(obj_t
		BgL_envz00_3108, obj_t BgL_nodez00_3109)
	{
		{	/* Ast/dump.scm 204 */
			{	/* Ast/dump.scm 207 */
				obj_t BgL_arg1990z00_3540;
				obj_t BgL_arg1991z00_3541;

				BgL_arg1990z00_3540 =
					(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_setfieldz00_bglt) BgL_nodez00_3109))))->BgL_locz00);
				{	/* Ast/dump.scm 208 */
					obj_t BgL_arg1992z00_3542;

					{	/* Ast/dump.scm 208 */
						obj_t BgL_arg1993z00_3543;
						obj_t BgL_arg1994z00_3544;

						{	/* Ast/dump.scm 208 */
							obj_t BgL_arg1995z00_3545;
							obj_t BgL_arg1996z00_3546;

							BgL_arg1995z00_3545 =
								(((BgL_setfieldz00_bglt) COBJECT(
										((BgL_setfieldz00_bglt) BgL_nodez00_3109)))->BgL_fnamez00);
							BgL_arg1996z00_3546 =
								MAKE_YOUNG_PAIR(
								(((BgL_typez00_bglt) COBJECT(
											(((BgL_setfieldz00_bglt) COBJECT(
														((BgL_setfieldz00_bglt) BgL_nodez00_3109)))->
												BgL_ftypez00)))->BgL_idz00), BNIL);
							BgL_arg1993z00_3543 =
								MAKE_YOUNG_PAIR(BgL_arg1995z00_3545, BgL_arg1996z00_3546);
						}
						{	/* Ast/dump.scm 209 */
							obj_t BgL_arg1999z00_3547;
							obj_t BgL_arg2000z00_3548;

							BgL_arg1999z00_3547 =
								(((BgL_typez00_bglt) COBJECT(
										(((BgL_setfieldz00_bglt) COBJECT(
													((BgL_setfieldz00_bglt) BgL_nodez00_3109)))->
											BgL_otypez00)))->BgL_idz00);
							{	/* Ast/dump.scm 209 */
								obj_t BgL_arg2002z00_3549;

								{	/* Ast/dump.scm 209 */
									obj_t BgL_l1282z00_3550;

									BgL_l1282z00_3550 =
										(((BgL_externz00_bglt) COBJECT(
												((BgL_externz00_bglt)
													((BgL_setfieldz00_bglt) BgL_nodez00_3109))))->
										BgL_exprza2za2);
									if (NULLP(BgL_l1282z00_3550))
										{	/* Ast/dump.scm 209 */
											BgL_arg2002z00_3549 = BNIL;
										}
									else
										{	/* Ast/dump.scm 209 */
											obj_t BgL_head1284z00_3551;

											{	/* Ast/dump.scm 209 */
												obj_t BgL_arg2009z00_3552;

												{	/* Ast/dump.scm 209 */
													obj_t BgL_arg2010z00_3553;

													BgL_arg2010z00_3553 = CAR(BgL_l1282z00_3550);
													BgL_arg2009z00_3552 =
														BGl_nodezd2ze3sexpz31zzast_dumpz00(
														((BgL_nodez00_bglt) BgL_arg2010z00_3553));
												}
												BgL_head1284z00_3551 =
													MAKE_YOUNG_PAIR(BgL_arg2009z00_3552, BNIL);
											}
											{	/* Ast/dump.scm 209 */
												obj_t BgL_g1287z00_3554;

												BgL_g1287z00_3554 = CDR(BgL_l1282z00_3550);
												{
													obj_t BgL_l1282z00_3556;
													obj_t BgL_tail1285z00_3557;

													BgL_l1282z00_3556 = BgL_g1287z00_3554;
													BgL_tail1285z00_3557 = BgL_head1284z00_3551;
												BgL_zc3z04anonymousza32004ze3z87_3555:
													if (NULLP(BgL_l1282z00_3556))
														{	/* Ast/dump.scm 209 */
															BgL_arg2002z00_3549 = BgL_head1284z00_3551;
														}
													else
														{	/* Ast/dump.scm 209 */
															obj_t BgL_newtail1286z00_3558;

															{	/* Ast/dump.scm 209 */
																obj_t BgL_arg2007z00_3559;

																{	/* Ast/dump.scm 209 */
																	obj_t BgL_arg2008z00_3560;

																	BgL_arg2008z00_3560 =
																		CAR(((obj_t) BgL_l1282z00_3556));
																	BgL_arg2007z00_3559 =
																		BGl_nodezd2ze3sexpz31zzast_dumpz00(
																		((BgL_nodez00_bglt) BgL_arg2008z00_3560));
																}
																BgL_newtail1286z00_3558 =
																	MAKE_YOUNG_PAIR(BgL_arg2007z00_3559, BNIL);
															}
															SET_CDR(BgL_tail1285z00_3557,
																BgL_newtail1286z00_3558);
															{	/* Ast/dump.scm 209 */
																obj_t BgL_arg2006z00_3561;

																BgL_arg2006z00_3561 =
																	CDR(((obj_t) BgL_l1282z00_3556));
																{
																	obj_t BgL_tail1285z00_4742;
																	obj_t BgL_l1282z00_4741;

																	BgL_l1282z00_4741 = BgL_arg2006z00_3561;
																	BgL_tail1285z00_4742 =
																		BgL_newtail1286z00_3558;
																	BgL_tail1285z00_3557 = BgL_tail1285z00_4742;
																	BgL_l1282z00_3556 = BgL_l1282z00_4741;
																	goto BgL_zc3z04anonymousza32004ze3z87_3555;
																}
															}
														}
												}
											}
										}
								}
								BgL_arg2000z00_3548 =
									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
									(BgL_arg2002z00_3549, BNIL);
							}
							BgL_arg1994z00_3544 =
								MAKE_YOUNG_PAIR(BgL_arg1999z00_3547, BgL_arg2000z00_3548);
						}
						BgL_arg1992z00_3542 =
							MAKE_YOUNG_PAIR(BgL_arg1993z00_3543, BgL_arg1994z00_3544);
					}
					BgL_arg1991z00_3541 =
						MAKE_YOUNG_PAIR(CNST_TABLE_REF(30), BgL_arg1992z00_3542);
				}
				return
					BGl_locationzd2shapezd2zztools_locationz00(BgL_arg1990z00_3540,
					BgL_arg1991z00_3541);
			}
		}

	}



/* &node->sexp-getfield1369 */
	obj_t BGl_z62nodezd2ze3sexpzd2getfield1369z81zzast_dumpz00(obj_t
		BgL_envz00_3110, obj_t BgL_nodez00_3111)
	{
		{	/* Ast/dump.scm 193 */
			{	/* Ast/dump.scm 196 */
				obj_t BgL_arg1973z00_3563;
				obj_t BgL_arg1974z00_3564;

				BgL_arg1973z00_3563 =
					(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_getfieldz00_bglt) BgL_nodez00_3111))))->BgL_locz00);
				{	/* Ast/dump.scm 197 */
					obj_t BgL_arg1975z00_3565;
					obj_t BgL_arg1976z00_3566;

					{	/* Ast/dump.scm 197 */
						BgL_typez00_bglt BgL_arg1977z00_3567;

						BgL_arg1977z00_3567 =
							BGl_getzd2typezd2zztype_typeofz00(
							((BgL_nodez00_bglt)
								((BgL_getfieldz00_bglt) BgL_nodez00_3111)), ((bool_t) 0));
						{	/* Ast/dump.scm 197 */
							obj_t BgL_idz00_3568;

							BgL_idz00_3568 = CNST_TABLE_REF(31);
							if (CBOOL(BGl_za2typenodezd2shapezf3za2z21zzengine_paramz00))
								{	/* Ast/dump.scm 442 */
									obj_t BgL_arg1489z00_3569;

									{	/* Ast/dump.scm 442 */
										obj_t BgL_arg1502z00_3570;
										obj_t BgL_arg1509z00_3571;

										{	/* Ast/dump.scm 442 */
											obj_t BgL_arg1455z00_3572;

											BgL_arg1455z00_3572 = SYMBOL_TO_STRING(BgL_idz00_3568);
											BgL_arg1502z00_3570 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_3572);
										}
										BgL_arg1509z00_3571 =
											BGl_shapez00zztools_shapez00(
											((obj_t) BgL_arg1977z00_3567));
										BgL_arg1489z00_3569 =
											string_append_3(BgL_arg1502z00_3570,
											BGl_string2407z00zzast_dumpz00, BgL_arg1509z00_3571);
									}
									BgL_arg1975z00_3565 = bstring_to_symbol(BgL_arg1489z00_3569);
								}
							else
								{	/* Ast/dump.scm 441 */
									BgL_arg1975z00_3565 = BgL_idz00_3568;
								}
						}
					}
					{	/* Ast/dump.scm 198 */
						obj_t BgL_arg1978z00_3573;
						obj_t BgL_arg1979z00_3574;

						{	/* Ast/dump.scm 198 */
							obj_t BgL_arg1980z00_3575;
							obj_t BgL_arg1981z00_3576;

							BgL_arg1980z00_3575 =
								(((BgL_getfieldz00_bglt) COBJECT(
										((BgL_getfieldz00_bglt) BgL_nodez00_3111)))->BgL_fnamez00);
							BgL_arg1981z00_3576 =
								MAKE_YOUNG_PAIR(
								(((BgL_typez00_bglt) COBJECT(
											(((BgL_getfieldz00_bglt) COBJECT(
														((BgL_getfieldz00_bglt) BgL_nodez00_3111)))->
												BgL_ftypez00)))->BgL_idz00), BNIL);
							BgL_arg1978z00_3573 =
								MAKE_YOUNG_PAIR(BgL_arg1980z00_3575, BgL_arg1981z00_3576);
						}
						{	/* Ast/dump.scm 199 */
							obj_t BgL_arg1984z00_3577;
							obj_t BgL_arg1985z00_3578;

							BgL_arg1984z00_3577 =
								(((BgL_typez00_bglt) COBJECT(
										(((BgL_getfieldz00_bglt) COBJECT(
													((BgL_getfieldz00_bglt) BgL_nodez00_3111)))->
											BgL_otypez00)))->BgL_idz00);
							{	/* Ast/dump.scm 199 */
								obj_t BgL_arg1987z00_3579;

								{	/* Ast/dump.scm 199 */
									obj_t BgL_arg1988z00_3580;

									{	/* Ast/dump.scm 199 */
										obj_t BgL_pairz00_3581;

										BgL_pairz00_3581 =
											(((BgL_externz00_bglt) COBJECT(
													((BgL_externz00_bglt)
														((BgL_getfieldz00_bglt) BgL_nodez00_3111))))->
											BgL_exprza2za2);
										BgL_arg1988z00_3580 = CAR(BgL_pairz00_3581);
									}
									BgL_arg1987z00_3579 =
										BGl_nodezd2ze3sexpz31zzast_dumpz00(
										((BgL_nodez00_bglt) BgL_arg1988z00_3580));
								}
								BgL_arg1985z00_3578 =
									MAKE_YOUNG_PAIR(BgL_arg1987z00_3579, BNIL);
							}
							BgL_arg1979z00_3574 =
								MAKE_YOUNG_PAIR(BgL_arg1984z00_3577, BgL_arg1985z00_3578);
						}
						BgL_arg1976z00_3566 =
							MAKE_YOUNG_PAIR(BgL_arg1978z00_3573, BgL_arg1979z00_3574);
					}
					BgL_arg1974z00_3564 =
						MAKE_YOUNG_PAIR(BgL_arg1975z00_3565, BgL_arg1976z00_3566);
				}
				return
					BGl_locationzd2shapezd2zztools_locationz00(BgL_arg1973z00_3563,
					BgL_arg1974z00_3564);
			}
		}

	}



/* &node->sexp-pragma1367 */
	obj_t BGl_z62nodezd2ze3sexpzd2pragma1367z81zzast_dumpz00(obj_t
		BgL_envz00_3112, obj_t BgL_nodez00_3113)
	{
		{	/* Ast/dump.scm 180 */
			{	/* Ast/dump.scm 182 */
				obj_t BgL_arg1956z00_3583;
				obj_t BgL_arg1957z00_3584;

				BgL_arg1956z00_3583 =
					(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_pragmaz00_bglt) BgL_nodez00_3113))))->BgL_locz00);
				{	/* Ast/dump.scm 183 */
					obj_t BgL_pz00_3585;

					if (CBOOL(
							(((BgL_nodezf2effectzf2_bglt) COBJECT(
										((BgL_nodezf2effectzf2_bglt)
											((BgL_pragmaz00_bglt) BgL_nodez00_3113))))->
								BgL_sidezd2effectzd2)))
						{	/* Ast/dump.scm 183 */
							BgL_pz00_3585 = CNST_TABLE_REF(32);
						}
					else
						{	/* Ast/dump.scm 183 */
							BgL_pz00_3585 = CNST_TABLE_REF(33);
						}
					{	/* Ast/dump.scm 186 */
						obj_t BgL_arg1958z00_3586;
						obj_t BgL_arg1959z00_3587;

						{	/* Ast/dump.scm 186 */
							BgL_typez00_bglt BgL_arg1960z00_3588;

							BgL_arg1960z00_3588 =
								BGl_getzd2typezd2zztype_typeofz00(
								((BgL_nodez00_bglt)
									((BgL_pragmaz00_bglt) BgL_nodez00_3113)), ((bool_t) 0));
							BgL_arg1958z00_3586 =
								BGl_shapezd2typedzd2nodez00zzast_dumpz00(BgL_pz00_3585,
								BgL_arg1960z00_3588);
						}
						{	/* Ast/dump.scm 187 */
							obj_t BgL_arg1961z00_3589;
							obj_t BgL_arg1962z00_3590;

							BgL_arg1961z00_3589 =
								(((BgL_pragmaz00_bglt) COBJECT(
										((BgL_pragmaz00_bglt) BgL_nodez00_3113)))->BgL_formatz00);
							{	/* Ast/dump.scm 188 */
								obj_t BgL_arg1963z00_3591;

								{	/* Ast/dump.scm 188 */
									obj_t BgL_l1276z00_3592;

									BgL_l1276z00_3592 =
										(((BgL_externz00_bglt) COBJECT(
												((BgL_externz00_bglt)
													((BgL_pragmaz00_bglt) BgL_nodez00_3113))))->
										BgL_exprza2za2);
									if (NULLP(BgL_l1276z00_3592))
										{	/* Ast/dump.scm 188 */
											BgL_arg1963z00_3591 = BNIL;
										}
									else
										{	/* Ast/dump.scm 188 */
											obj_t BgL_head1278z00_3593;

											{	/* Ast/dump.scm 188 */
												obj_t BgL_arg1970z00_3594;

												{	/* Ast/dump.scm 188 */
													obj_t BgL_arg1971z00_3595;

													BgL_arg1971z00_3595 = CAR(BgL_l1276z00_3592);
													BgL_arg1970z00_3594 =
														BGl_nodezd2ze3sexpz31zzast_dumpz00(
														((BgL_nodez00_bglt) BgL_arg1971z00_3595));
												}
												BgL_head1278z00_3593 =
													MAKE_YOUNG_PAIR(BgL_arg1970z00_3594, BNIL);
											}
											{	/* Ast/dump.scm 188 */
												obj_t BgL_g1281z00_3596;

												BgL_g1281z00_3596 = CDR(BgL_l1276z00_3592);
												{
													obj_t BgL_l1276z00_3598;
													obj_t BgL_tail1279z00_3599;

													BgL_l1276z00_3598 = BgL_g1281z00_3596;
													BgL_tail1279z00_3599 = BgL_head1278z00_3593;
												BgL_zc3z04anonymousza31965ze3z87_3597:
													if (NULLP(BgL_l1276z00_3598))
														{	/* Ast/dump.scm 188 */
															BgL_arg1963z00_3591 = BgL_head1278z00_3593;
														}
													else
														{	/* Ast/dump.scm 188 */
															obj_t BgL_newtail1280z00_3600;

															{	/* Ast/dump.scm 188 */
																obj_t BgL_arg1968z00_3601;

																{	/* Ast/dump.scm 188 */
																	obj_t BgL_arg1969z00_3602;

																	BgL_arg1969z00_3602 =
																		CAR(((obj_t) BgL_l1276z00_3598));
																	BgL_arg1968z00_3601 =
																		BGl_nodezd2ze3sexpz31zzast_dumpz00(
																		((BgL_nodez00_bglt) BgL_arg1969z00_3602));
																}
																BgL_newtail1280z00_3600 =
																	MAKE_YOUNG_PAIR(BgL_arg1968z00_3601, BNIL);
															}
															SET_CDR(BgL_tail1279z00_3599,
																BgL_newtail1280z00_3600);
															{	/* Ast/dump.scm 188 */
																obj_t BgL_arg1967z00_3603;

																BgL_arg1967z00_3603 =
																	CDR(((obj_t) BgL_l1276z00_3598));
																{
																	obj_t BgL_tail1279z00_4822;
																	obj_t BgL_l1276z00_4821;

																	BgL_l1276z00_4821 = BgL_arg1967z00_3603;
																	BgL_tail1279z00_4822 =
																		BgL_newtail1280z00_3600;
																	BgL_tail1279z00_3599 = BgL_tail1279z00_4822;
																	BgL_l1276z00_3598 = BgL_l1276z00_4821;
																	goto BgL_zc3z04anonymousza31965ze3z87_3597;
																}
															}
														}
												}
											}
										}
								}
								BgL_arg1962z00_3590 =
									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
									(BgL_arg1963z00_3591, BNIL);
							}
							BgL_arg1959z00_3587 =
								MAKE_YOUNG_PAIR(BgL_arg1961z00_3589, BgL_arg1962z00_3590);
						}
						BgL_arg1957z00_3584 =
							MAKE_YOUNG_PAIR(BgL_arg1958z00_3586, BgL_arg1959z00_3587);
					}
				}
				return
					BGl_locationzd2shapezd2zztools_locationz00(BgL_arg1956z00_3583,
					BgL_arg1957z00_3584);
			}
		}

	}



/* &node->sexp-funcall1365 */
	obj_t BGl_z62nodezd2ze3sexpzd2funcall1365z81zzast_dumpz00(obj_t
		BgL_envz00_3114, obj_t BgL_nodez00_3115)
	{
		{	/* Ast/dump.scm 165 */
			{	/* Ast/dump.scm 167 */
				obj_t BgL_opz00_3605;

				{	/* Ast/dump.scm 167 */
					obj_t BgL_casezd2valuezd2_3606;

					BgL_casezd2valuezd2_3606 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_3115)))->BgL_strengthz00);
					if ((BgL_casezd2valuezd2_3606 == CNST_TABLE_REF(34)))
						{	/* Ast/dump.scm 167 */
							BgL_opz00_3605 = CNST_TABLE_REF(35);
						}
					else
						{	/* Ast/dump.scm 167 */
							if ((BgL_casezd2valuezd2_3606 == CNST_TABLE_REF(36)))
								{	/* Ast/dump.scm 167 */
									BgL_opz00_3605 = CNST_TABLE_REF(37);
								}
							else
								{	/* Ast/dump.scm 167 */
									BgL_opz00_3605 = CNST_TABLE_REF(38);
								}
						}
				}
				{	/* Ast/dump.scm 167 */
					obj_t BgL_topz00_3607;

					{	/* Ast/dump.scm 171 */
						BgL_typez00_bglt BgL_arg1953z00_3608;

						BgL_arg1953z00_3608 =
							BGl_getzd2typezd2zztype_typeofz00(
							((BgL_nodez00_bglt)
								((BgL_funcallz00_bglt) BgL_nodez00_3115)), ((bool_t) 0));
						BgL_topz00_3607 =
							BGl_shapezd2typedzd2nodez00zzast_dumpz00(BgL_opz00_3605,
							BgL_arg1953z00_3608);
					}
					{	/* Ast/dump.scm 171 */

						{	/* Ast/dump.scm 172 */
							obj_t BgL_arg1938z00_3609;
							obj_t BgL_arg1939z00_3610;

							BgL_arg1938z00_3609 =
								(((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_funcallz00_bglt) BgL_nodez00_3115))))->BgL_locz00);
							{	/* Ast/dump.scm 174 */
								obj_t BgL_arg1940z00_3611;

								{	/* Ast/dump.scm 174 */
									obj_t BgL_arg1941z00_3612;
									obj_t BgL_arg1942z00_3613;

									BgL_arg1941z00_3612 =
										BGl_nodezd2ze3sexpz31zzast_dumpz00(
										(((BgL_funcallz00_bglt) COBJECT(
													((BgL_funcallz00_bglt) BgL_nodez00_3115)))->
											BgL_funz00));
									{	/* Ast/dump.scm 175 */
										obj_t BgL_arg1944z00_3614;

										{	/* Ast/dump.scm 175 */
											obj_t BgL_l1270z00_3615;

											BgL_l1270z00_3615 =
												(((BgL_funcallz00_bglt) COBJECT(
														((BgL_funcallz00_bglt) BgL_nodez00_3115)))->
												BgL_argsz00);
											if (NULLP(BgL_l1270z00_3615))
												{	/* Ast/dump.scm 175 */
													BgL_arg1944z00_3614 = BNIL;
												}
											else
												{	/* Ast/dump.scm 175 */
													obj_t BgL_head1272z00_3616;

													{	/* Ast/dump.scm 175 */
														obj_t BgL_arg1951z00_3617;

														{	/* Ast/dump.scm 175 */
															obj_t BgL_arg1952z00_3618;

															BgL_arg1952z00_3618 =
																CAR(((obj_t) BgL_l1270z00_3615));
															BgL_arg1951z00_3617 =
																BGl_nodezd2ze3sexpz31zzast_dumpz00(
																((BgL_nodez00_bglt) BgL_arg1952z00_3618));
														}
														BgL_head1272z00_3616 =
															MAKE_YOUNG_PAIR(BgL_arg1951z00_3617, BNIL);
													}
													{	/* Ast/dump.scm 175 */
														obj_t BgL_g1275z00_3619;

														BgL_g1275z00_3619 =
															CDR(((obj_t) BgL_l1270z00_3615));
														{
															obj_t BgL_l1270z00_3621;
															obj_t BgL_tail1273z00_3622;

															BgL_l1270z00_3621 = BgL_g1275z00_3619;
															BgL_tail1273z00_3622 = BgL_head1272z00_3616;
														BgL_zc3z04anonymousza31946ze3z87_3620:
															if (NULLP(BgL_l1270z00_3621))
																{	/* Ast/dump.scm 175 */
																	BgL_arg1944z00_3614 = BgL_head1272z00_3616;
																}
															else
																{	/* Ast/dump.scm 175 */
																	obj_t BgL_newtail1274z00_3623;

																	{	/* Ast/dump.scm 175 */
																		obj_t BgL_arg1949z00_3624;

																		{	/* Ast/dump.scm 175 */
																			obj_t BgL_arg1950z00_3625;

																			BgL_arg1950z00_3625 =
																				CAR(((obj_t) BgL_l1270z00_3621));
																			BgL_arg1949z00_3624 =
																				BGl_nodezd2ze3sexpz31zzast_dumpz00(
																				((BgL_nodez00_bglt)
																					BgL_arg1950z00_3625));
																		}
																		BgL_newtail1274z00_3623 =
																			MAKE_YOUNG_PAIR(BgL_arg1949z00_3624,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1273z00_3622,
																		BgL_newtail1274z00_3623);
																	{	/* Ast/dump.scm 175 */
																		obj_t BgL_arg1948z00_3626;

																		BgL_arg1948z00_3626 =
																			CDR(((obj_t) BgL_l1270z00_3621));
																		{
																			obj_t BgL_tail1273z00_4870;
																			obj_t BgL_l1270z00_4869;

																			BgL_l1270z00_4869 = BgL_arg1948z00_3626;
																			BgL_tail1273z00_4870 =
																				BgL_newtail1274z00_3623;
																			BgL_tail1273z00_3622 =
																				BgL_tail1273z00_4870;
																			BgL_l1270z00_3621 = BgL_l1270z00_4869;
																			goto
																				BgL_zc3z04anonymousza31946ze3z87_3620;
																		}
																	}
																}
														}
													}
												}
										}
										BgL_arg1942z00_3613 =
											BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
											(BgL_arg1944z00_3614, BNIL);
									}
									BgL_arg1940z00_3611 =
										MAKE_YOUNG_PAIR(BgL_arg1941z00_3612, BgL_arg1942z00_3613);
								}
								BgL_arg1939z00_3610 =
									MAKE_YOUNG_PAIR(BgL_topz00_3607, BgL_arg1940z00_3611);
							}
							return
								BGl_locationzd2shapezd2zztools_locationz00(BgL_arg1938z00_3609,
								BgL_arg1939z00_3610);
						}
					}
				}
			}
		}

	}



/* &node->sexp-app-ly1363 */
	obj_t BGl_z62nodezd2ze3sexpzd2appzd2ly1363z53zzast_dumpz00(obj_t
		BgL_envz00_3116, obj_t BgL_nodez00_3117)
	{
		{	/* Ast/dump.scm 155 */
			{	/* Ast/dump.scm 157 */
				obj_t BgL_topz00_3628;

				{	/* Ast/dump.scm 157 */
					obj_t BgL_arg1936z00_3629;

					{	/* Ast/dump.scm 157 */
						BgL_typez00_bglt BgL_arg1937z00_3630;

						BgL_arg1937z00_3630 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_appzd2lyzd2_bglt) BgL_nodez00_3117))))->BgL_typez00);
						BgL_arg1936z00_3629 =
							BGl_shapez00zztools_shapez00(((obj_t) BgL_arg1937z00_3630));
					}
					{	/* Ast/dump.scm 157 */
						obj_t BgL_idz00_3631;

						BgL_idz00_3631 = CNST_TABLE_REF(39);
						if (CBOOL(BGl_za2typenodezd2shapezf3za2z21zzengine_paramz00))
							{	/* Ast/dump.scm 442 */
								obj_t BgL_arg1489z00_3632;

								{	/* Ast/dump.scm 442 */
									obj_t BgL_arg1502z00_3633;
									obj_t BgL_arg1509z00_3634;

									{	/* Ast/dump.scm 442 */
										obj_t BgL_arg1455z00_3635;

										BgL_arg1455z00_3635 = SYMBOL_TO_STRING(BgL_idz00_3631);
										BgL_arg1502z00_3633 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_3635);
									}
									BgL_arg1509z00_3634 =
										BGl_shapez00zztools_shapez00(BgL_arg1936z00_3629);
									BgL_arg1489z00_3632 =
										string_append_3(BgL_arg1502z00_3633,
										BGl_string2407z00zzast_dumpz00, BgL_arg1509z00_3634);
								}
								BgL_topz00_3628 = bstring_to_symbol(BgL_arg1489z00_3632);
							}
						else
							{	/* Ast/dump.scm 441 */
								BgL_topz00_3628 = BgL_idz00_3631;
							}
					}
				}
				{	/* Ast/dump.scm 158 */
					obj_t BgL_arg1928z00_3636;
					obj_t BgL_arg1929z00_3637;

					BgL_arg1928z00_3636 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_3117))))->BgL_locz00);
					{	/* Ast/dump.scm 159 */
						obj_t BgL_arg1930z00_3638;

						{	/* Ast/dump.scm 159 */
							obj_t BgL_arg1931z00_3639;
							obj_t BgL_arg1932z00_3640;

							BgL_arg1931z00_3639 =
								BGl_nodezd2ze3sexpz31zzast_dumpz00(
								(((BgL_appzd2lyzd2_bglt) COBJECT(
											((BgL_appzd2lyzd2_bglt) BgL_nodez00_3117)))->BgL_funz00));
							BgL_arg1932z00_3640 =
								MAKE_YOUNG_PAIR(BGl_nodezd2ze3sexpz31zzast_dumpz00(
									(((BgL_appzd2lyzd2_bglt) COBJECT(
												((BgL_appzd2lyzd2_bglt) BgL_nodez00_3117)))->
										BgL_argz00)), BNIL);
							BgL_arg1930z00_3638 =
								MAKE_YOUNG_PAIR(BgL_arg1931z00_3639, BgL_arg1932z00_3640);
						}
						BgL_arg1929z00_3637 =
							MAKE_YOUNG_PAIR(BgL_topz00_3628, BgL_arg1930z00_3638);
					}
					return
						BGl_locationzd2shapezd2zztools_locationz00(BgL_arg1928z00_3636,
						BgL_arg1929z00_3637);
				}
			}
		}

	}



/* &node->sexp-app1361 */
	obj_t BGl_z62nodezd2ze3sexpzd2app1361z81zzast_dumpz00(obj_t BgL_envz00_3118,
		obj_t BgL_nodez00_3119)
	{
		{	/* Ast/dump.scm 118 */
			{	/* Ast/dump.scm 120 */
				obj_t BgL_arg1767z00_3642;
				obj_t BgL_arg1770z00_3643;

				BgL_arg1767z00_3642 =
					(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_appz00_bglt) BgL_nodez00_3119))))->BgL_locz00);
				if (CBOOL(BGl_za2typezd2shapezf3za2z21zzengine_paramz00))
					{	/* Ast/dump.scm 123 */
						obj_t BgL_arg1771z00_3644;
						obj_t BgL_arg1773z00_3645;

						{	/* Ast/dump.scm 123 */
							BgL_varz00_bglt BgL_arg1775z00_3646;

							BgL_arg1775z00_3646 =
								(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_nodez00_3119)))->BgL_funz00);
							BgL_arg1771z00_3644 =
								BGl_nodezd2ze3sexpz31zzast_dumpz00(
								((BgL_nodez00_bglt) BgL_arg1775z00_3646));
						}
						{	/* Ast/dump.scm 124 */
							obj_t BgL_arg1798z00_3647;
							obj_t BgL_arg1799z00_3648;

							if (CBOOL(BGl_za2accesszd2shapezf3za2z21zzengine_paramz00))
								{	/* Ast/dump.scm 125 */
									obj_t BgL_arg1805z00_3649;

									{	/* Ast/dump.scm 125 */
										obj_t BgL_arg1806z00_3650;

										{	/* Ast/dump.scm 125 */
											obj_t BgL_arg1808z00_3651;

											BgL_arg1808z00_3651 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(40), BNIL);
											BgL_arg1806z00_3650 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(41),
												BgL_arg1808z00_3651);
										}
										BgL_arg1805z00_3649 =
											MAKE_YOUNG_PAIR(BgL_arg1806z00_3650, BNIL);
									}
									BgL_arg1798z00_3647 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(14), BgL_arg1805z00_3649);
								}
							else
								{	/* Ast/dump.scm 124 */
									BgL_arg1798z00_3647 = BNIL;
								}
							{	/* Ast/dump.scm 127 */
								obj_t BgL_arg1812z00_3652;
								obj_t BgL_arg1820z00_3653;

								if (CBOOL(BGl_za2alloczd2shapezf3za2z21zzengine_paramz00))
									{	/* Ast/dump.scm 128 */
										obj_t BgL_arg1822z00_3654;

										{	/* Ast/dump.scm 128 */
											obj_t BgL_arg1823z00_3655;

											{	/* Ast/dump.scm 128 */
												obj_t BgL_arg1831z00_3656;

												BgL_arg1831z00_3656 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(40), BNIL);
												BgL_arg1823z00_3655 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(42),
													BgL_arg1831z00_3656);
											}
											BgL_arg1822z00_3654 =
												MAKE_YOUNG_PAIR(BgL_arg1823z00_3655, BNIL);
										}
										BgL_arg1812z00_3652 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(16), BgL_arg1822z00_3654);
									}
								else
									{	/* Ast/dump.scm 127 */
										BgL_arg1812z00_3652 = BNIL;
									}
								{	/* Ast/dump.scm 130 */
									obj_t BgL_arg1832z00_3657;
									obj_t BgL_arg1833z00_3658;

									{	/* Ast/dump.scm 130 */
										obj_t BgL_arg1834z00_3659;

										{	/* Ast/dump.scm 130 */
											BgL_typez00_bglt BgL_arg1837z00_3660;

											BgL_arg1837z00_3660 =
												(((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt)
															((BgL_appz00_bglt) BgL_nodez00_3119))))->
												BgL_typez00);
											BgL_arg1834z00_3659 =
												BGl_shapez00zztools_shapez00(((obj_t)
													BgL_arg1837z00_3660));
										}
										{	/* Ast/dump.scm 130 */
											obj_t BgL_list1835z00_3661;

											{	/* Ast/dump.scm 130 */
												obj_t BgL_arg1836z00_3662;

												BgL_arg1836z00_3662 =
													MAKE_YOUNG_PAIR(BgL_arg1834z00_3659, BNIL);
												BgL_list1835z00_3661 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(43),
													BgL_arg1836z00_3662);
											}
											BgL_arg1832z00_3657 = BgL_list1835z00_3661;
										}
									}
									{	/* Ast/dump.scm 131 */
										obj_t BgL_arg1838z00_3663;
										obj_t BgL_arg1839z00_3664;

										{	/* Ast/dump.scm 131 */
											bool_t BgL_test2562z00_4936;

											{	/* Ast/dump.scm 132 */
												BgL_valuez00_bglt BgL_arg1859z00_3665;

												BgL_arg1859z00_3665 =
													(((BgL_variablez00_bglt) COBJECT(
															(((BgL_varz00_bglt) COBJECT(
																		(((BgL_appz00_bglt) COBJECT(
																					((BgL_appz00_bglt)
																						BgL_nodez00_3119)))->BgL_funz00)))->
																BgL_variablez00)))->BgL_valuez00);
												{	/* Ast/dump.scm 131 */
													obj_t BgL_classz00_3666;

													BgL_classz00_3666 = BGl_cfunz00zzast_varz00;
													{	/* Ast/dump.scm 131 */
														BgL_objectz00_bglt BgL_arg1807z00_3667;

														{	/* Ast/dump.scm 131 */
															obj_t BgL_tmpz00_4941;

															BgL_tmpz00_4941 =
																((obj_t)
																((BgL_objectz00_bglt) BgL_arg1859z00_3665));
															BgL_arg1807z00_3667 =
																(BgL_objectz00_bglt) (BgL_tmpz00_4941);
														}
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Ast/dump.scm 131 */
																long BgL_idxz00_3668;

																BgL_idxz00_3668 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_3667);
																BgL_test2562z00_4936 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_3668 + 3L)) ==
																	BgL_classz00_3666);
															}
														else
															{	/* Ast/dump.scm 131 */
																bool_t BgL_res2403z00_3671;

																{	/* Ast/dump.scm 131 */
																	obj_t BgL_oclassz00_3672;

																	{	/* Ast/dump.scm 131 */
																		obj_t BgL_arg1815z00_3673;
																		long BgL_arg1816z00_3674;

																		BgL_arg1815z00_3673 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Ast/dump.scm 131 */
																			long BgL_arg1817z00_3675;

																			BgL_arg1817z00_3675 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_3667);
																			BgL_arg1816z00_3674 =
																				(BgL_arg1817z00_3675 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_3672 =
																			VECTOR_REF(BgL_arg1815z00_3673,
																			BgL_arg1816z00_3674);
																	}
																	{	/* Ast/dump.scm 131 */
																		bool_t BgL__ortest_1115z00_3676;

																		BgL__ortest_1115z00_3676 =
																			(BgL_classz00_3666 == BgL_oclassz00_3672);
																		if (BgL__ortest_1115z00_3676)
																			{	/* Ast/dump.scm 131 */
																				BgL_res2403z00_3671 =
																					BgL__ortest_1115z00_3676;
																			}
																		else
																			{	/* Ast/dump.scm 131 */
																				long BgL_odepthz00_3677;

																				{	/* Ast/dump.scm 131 */
																					obj_t BgL_arg1804z00_3678;

																					BgL_arg1804z00_3678 =
																						(BgL_oclassz00_3672);
																					BgL_odepthz00_3677 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_3678);
																				}
																				if ((3L < BgL_odepthz00_3677))
																					{	/* Ast/dump.scm 131 */
																						obj_t BgL_arg1802z00_3679;

																						{	/* Ast/dump.scm 131 */
																							obj_t BgL_arg1803z00_3680;

																							BgL_arg1803z00_3680 =
																								(BgL_oclassz00_3672);
																							BgL_arg1802z00_3679 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_3680, 3L);
																						}
																						BgL_res2403z00_3671 =
																							(BgL_arg1802z00_3679 ==
																							BgL_classz00_3666);
																					}
																				else
																					{	/* Ast/dump.scm 131 */
																						BgL_res2403z00_3671 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2562z00_4936 = BgL_res2403z00_3671;
															}
													}
												}
											}
											if (BgL_test2562z00_4936)
												{	/* Ast/dump.scm 133 */
													BgL_valuez00_bglt BgL_cfz00_3681;

													BgL_cfz00_3681 =
														(((BgL_variablez00_bglt) COBJECT(
																(((BgL_varz00_bglt) COBJECT(
																			(((BgL_appz00_bglt) COBJECT(
																						((BgL_appz00_bglt)
																							BgL_nodez00_3119)))->
																				BgL_funz00)))->BgL_variablez00)))->
														BgL_valuez00);
													{	/* Ast/dump.scm 136 */
														obj_t BgL_arg1845z00_3682;

														{	/* Ast/dump.scm 136 */
															obj_t BgL_arg1846z00_3683;

															{	/* Ast/dump.scm 136 */
																obj_t BgL_arg1847z00_3684;

																{	/* Ast/dump.scm 136 */
																	obj_t BgL_l1240z00_3685;

																	BgL_l1240z00_3685 =
																		(((BgL_cfunz00_bglt) COBJECT(
																				((BgL_cfunz00_bglt) BgL_cfz00_3681)))->
																		BgL_argszd2typezd2);
																	if (NULLP(BgL_l1240z00_3685))
																		{	/* Ast/dump.scm 136 */
																			BgL_arg1847z00_3684 = BNIL;
																		}
																	else
																		{	/* Ast/dump.scm 136 */
																			obj_t BgL_head1242z00_3686;

																			{	/* Ast/dump.scm 136 */
																				obj_t BgL_arg1854z00_3687;

																				BgL_arg1854z00_3687 =
																					(((BgL_typez00_bglt) COBJECT(
																							((BgL_typez00_bglt)
																								CAR(
																									((obj_t)
																										BgL_l1240z00_3685)))))->
																					BgL_idz00);
																				BgL_head1242z00_3686 =
																					MAKE_YOUNG_PAIR(BgL_arg1854z00_3687,
																					BNIL);
																			}
																			{	/* Ast/dump.scm 136 */
																				obj_t BgL_g1245z00_3688;

																				BgL_g1245z00_3688 =
																					CDR(((obj_t) BgL_l1240z00_3685));
																				{
																					obj_t BgL_l1240z00_3690;
																					obj_t BgL_tail1243z00_3691;

																					BgL_l1240z00_3690 = BgL_g1245z00_3688;
																					BgL_tail1243z00_3691 =
																						BgL_head1242z00_3686;
																				BgL_zc3z04anonymousza31849ze3z87_3689:
																					if (NULLP(BgL_l1240z00_3690))
																						{	/* Ast/dump.scm 136 */
																							BgL_arg1847z00_3684 =
																								BgL_head1242z00_3686;
																						}
																					else
																						{	/* Ast/dump.scm 136 */
																							obj_t BgL_newtail1244z00_3692;

																							{	/* Ast/dump.scm 136 */
																								obj_t BgL_arg1852z00_3693;

																								BgL_arg1852z00_3693 =
																									(((BgL_typez00_bglt) COBJECT(
																											((BgL_typez00_bglt)
																												CAR(
																													((obj_t)
																														BgL_l1240z00_3690)))))->
																									BgL_idz00);
																								BgL_newtail1244z00_3692 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1852z00_3693, BNIL);
																							}
																							SET_CDR(BgL_tail1243z00_3691,
																								BgL_newtail1244z00_3692);
																							{	/* Ast/dump.scm 136 */
																								obj_t BgL_arg1851z00_3694;

																								BgL_arg1851z00_3694 =
																									CDR(
																									((obj_t) BgL_l1240z00_3690));
																								{
																									obj_t BgL_tail1243z00_4990;
																									obj_t BgL_l1240z00_4989;

																									BgL_l1240z00_4989 =
																										BgL_arg1851z00_3694;
																									BgL_tail1243z00_4990 =
																										BgL_newtail1244z00_3692;
																									BgL_tail1243z00_3691 =
																										BgL_tail1243z00_4990;
																									BgL_l1240z00_3690 =
																										BgL_l1240z00_4989;
																									goto
																										BgL_zc3z04anonymousza31849ze3z87_3689;
																								}
																							}
																						}
																				}
																			}
																		}
																}
																BgL_arg1846z00_3683 =
																	MAKE_YOUNG_PAIR(BgL_arg1847z00_3684, BNIL);
															}
															BgL_arg1845z00_3682 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(44),
																BgL_arg1846z00_3683);
														}
														BgL_arg1838z00_3663 =
															MAKE_YOUNG_PAIR(BgL_arg1845z00_3682, BNIL);
													}
												}
											else
												{	/* Ast/dump.scm 131 */
													BgL_arg1838z00_3663 = BNIL;
												}
										}
										{	/* Ast/dump.scm 138 */
											obj_t BgL_arg1863z00_3695;

											{	/* Ast/dump.scm 138 */
												obj_t BgL_l1246z00_3696;

												BgL_l1246z00_3696 =
													(((BgL_appz00_bglt) COBJECT(
															((BgL_appz00_bglt) BgL_nodez00_3119)))->
													BgL_argsz00);
												if (NULLP(BgL_l1246z00_3696))
													{	/* Ast/dump.scm 138 */
														BgL_arg1863z00_3695 = BNIL;
													}
												else
													{	/* Ast/dump.scm 138 */
														obj_t BgL_head1248z00_3697;

														{	/* Ast/dump.scm 138 */
															obj_t BgL_arg1872z00_3698;

															{	/* Ast/dump.scm 138 */
																obj_t BgL_arg1873z00_3699;

																BgL_arg1873z00_3699 =
																	CAR(((obj_t) BgL_l1246z00_3696));
																BgL_arg1872z00_3698 =
																	BGl_nodezd2ze3sexpz31zzast_dumpz00(
																	((BgL_nodez00_bglt) BgL_arg1873z00_3699));
															}
															BgL_head1248z00_3697 =
																MAKE_YOUNG_PAIR(BgL_arg1872z00_3698, BNIL);
														}
														{	/* Ast/dump.scm 138 */
															obj_t BgL_g1251z00_3700;

															BgL_g1251z00_3700 =
																CDR(((obj_t) BgL_l1246z00_3696));
															{
																obj_t BgL_l1246z00_3702;
																obj_t BgL_tail1249z00_3703;

																BgL_l1246z00_3702 = BgL_g1251z00_3700;
																BgL_tail1249z00_3703 = BgL_head1248z00_3697;
															BgL_zc3z04anonymousza31865ze3z87_3701:
																if (NULLP(BgL_l1246z00_3702))
																	{	/* Ast/dump.scm 138 */
																		BgL_arg1863z00_3695 = BgL_head1248z00_3697;
																	}
																else
																	{	/* Ast/dump.scm 138 */
																		obj_t BgL_newtail1250z00_3704;

																		{	/* Ast/dump.scm 138 */
																			obj_t BgL_arg1869z00_3705;

																			{	/* Ast/dump.scm 138 */
																				obj_t BgL_arg1870z00_3706;

																				BgL_arg1870z00_3706 =
																					CAR(((obj_t) BgL_l1246z00_3702));
																				BgL_arg1869z00_3705 =
																					BGl_nodezd2ze3sexpz31zzast_dumpz00(
																					((BgL_nodez00_bglt)
																						BgL_arg1870z00_3706));
																			}
																			BgL_newtail1250z00_3704 =
																				MAKE_YOUNG_PAIR(BgL_arg1869z00_3705,
																				BNIL);
																		}
																		SET_CDR(BgL_tail1249z00_3703,
																			BgL_newtail1250z00_3704);
																		{	/* Ast/dump.scm 138 */
																			obj_t BgL_arg1868z00_3707;

																			BgL_arg1868z00_3707 =
																				CDR(((obj_t) BgL_l1246z00_3702));
																			{
																				obj_t BgL_tail1249z00_5017;
																				obj_t BgL_l1246z00_5016;

																				BgL_l1246z00_5016 = BgL_arg1868z00_3707;
																				BgL_tail1249z00_5017 =
																					BgL_newtail1250z00_3704;
																				BgL_tail1249z00_3703 =
																					BgL_tail1249z00_5017;
																				BgL_l1246z00_3702 = BgL_l1246z00_5016;
																				goto
																					BgL_zc3z04anonymousza31865ze3z87_3701;
																			}
																		}
																	}
															}
														}
													}
											}
											BgL_arg1839z00_3664 =
												BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
												(BgL_arg1863z00_3695, BNIL);
										}
										BgL_arg1833z00_3658 =
											BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
											(BgL_arg1838z00_3663, BgL_arg1839z00_3664);
									}
									BgL_arg1820z00_3653 =
										MAKE_YOUNG_PAIR(BgL_arg1832z00_3657, BgL_arg1833z00_3658);
								}
								BgL_arg1799z00_3648 =
									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
									(BgL_arg1812z00_3652, BgL_arg1820z00_3653);
							}
							BgL_arg1773z00_3645 =
								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
								(BgL_arg1798z00_3647, BgL_arg1799z00_3648);
						}
						BgL_arg1770z00_3643 =
							MAKE_YOUNG_PAIR(BgL_arg1771z00_3644, BgL_arg1773z00_3645);
					}
				else
					{	/* Ast/dump.scm 122 */
						if (CBOOL(BGl_za2accesszd2shapezf3za2z21zzengine_paramz00))
							{	/* Ast/dump.scm 140 */
								obj_t BgL_arg1874z00_3708;
								obj_t BgL_arg1875z00_3709;

								{	/* Ast/dump.scm 140 */
									BgL_varz00_bglt BgL_arg1876z00_3710;

									BgL_arg1876z00_3710 =
										(((BgL_appz00_bglt) COBJECT(
												((BgL_appz00_bglt) BgL_nodez00_3119)))->BgL_funz00);
									BgL_arg1874z00_3708 =
										BGl_nodezd2ze3sexpz31zzast_dumpz00(
										((BgL_nodez00_bglt) BgL_arg1876z00_3710));
								}
								{	/* Ast/dump.scm 141 */
									obj_t BgL_arg1877z00_3711;

									{	/* Ast/dump.scm 141 */
										bool_t BgL_arg1878z00_3712;
										obj_t BgL_arg1879z00_3713;

										BgL_arg1878z00_3712 =
											BGl_sidezd2effectzf3z21zzeffect_effectz00(
											((BgL_nodez00_bglt)
												((BgL_appz00_bglt) BgL_nodez00_3119)));
										{	/* Ast/dump.scm 142 */
											obj_t BgL_arg1880z00_3714;

											{	/* Ast/dump.scm 142 */
												obj_t BgL_arg1882z00_3715;
												obj_t BgL_arg1883z00_3716;

												BgL_arg1882z00_3715 =
													(((BgL_appz00_bglt) COBJECT(
															((BgL_appz00_bglt) BgL_nodez00_3119)))->
													BgL_stackablez00);
												{	/* Ast/dump.scm 143 */
													obj_t BgL_arg1884z00_3717;

													{	/* Ast/dump.scm 143 */
														obj_t BgL_l1252z00_3718;

														BgL_l1252z00_3718 =
															(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt) BgL_nodez00_3119)))->
															BgL_argsz00);
														if (NULLP(BgL_l1252z00_3718))
															{	/* Ast/dump.scm 143 */
																BgL_arg1884z00_3717 = BNIL;
															}
														else
															{	/* Ast/dump.scm 143 */
																obj_t BgL_head1254z00_3719;

																{	/* Ast/dump.scm 143 */
																	obj_t BgL_arg1891z00_3720;

																	{	/* Ast/dump.scm 143 */
																		obj_t BgL_arg1892z00_3721;

																		BgL_arg1892z00_3721 =
																			CAR(((obj_t) BgL_l1252z00_3718));
																		BgL_arg1891z00_3720 =
																			BGl_nodezd2ze3sexpz31zzast_dumpz00(
																			((BgL_nodez00_bglt) BgL_arg1892z00_3721));
																	}
																	BgL_head1254z00_3719 =
																		MAKE_YOUNG_PAIR(BgL_arg1891z00_3720, BNIL);
																}
																{	/* Ast/dump.scm 143 */
																	obj_t BgL_g1257z00_3722;

																	BgL_g1257z00_3722 =
																		CDR(((obj_t) BgL_l1252z00_3718));
																	{
																		obj_t BgL_l1252z00_3724;
																		obj_t BgL_tail1255z00_3725;

																		BgL_l1252z00_3724 = BgL_g1257z00_3722;
																		BgL_tail1255z00_3725 = BgL_head1254z00_3719;
																	BgL_zc3z04anonymousza31886ze3z87_3723:
																		if (NULLP(BgL_l1252z00_3724))
																			{	/* Ast/dump.scm 143 */
																				BgL_arg1884z00_3717 =
																					BgL_head1254z00_3719;
																			}
																		else
																			{	/* Ast/dump.scm 143 */
																				obj_t BgL_newtail1256z00_3726;

																				{	/* Ast/dump.scm 143 */
																					obj_t BgL_arg1889z00_3727;

																					{	/* Ast/dump.scm 143 */
																						obj_t BgL_arg1890z00_3728;

																						BgL_arg1890z00_3728 =
																							CAR(((obj_t) BgL_l1252z00_3724));
																						BgL_arg1889z00_3727 =
																							BGl_nodezd2ze3sexpz31zzast_dumpz00
																							(((BgL_nodez00_bglt)
																								BgL_arg1890z00_3728));
																					}
																					BgL_newtail1256z00_3726 =
																						MAKE_YOUNG_PAIR(BgL_arg1889z00_3727,
																						BNIL);
																				}
																				SET_CDR(BgL_tail1255z00_3725,
																					BgL_newtail1256z00_3726);
																				{	/* Ast/dump.scm 143 */
																					obj_t BgL_arg1888z00_3729;

																					BgL_arg1888z00_3729 =
																						CDR(((obj_t) BgL_l1252z00_3724));
																					{
																						obj_t BgL_tail1255z00_5057;
																						obj_t BgL_l1252z00_5056;

																						BgL_l1252z00_5056 =
																							BgL_arg1888z00_3729;
																						BgL_tail1255z00_5057 =
																							BgL_newtail1256z00_3726;
																						BgL_tail1255z00_3725 =
																							BgL_tail1255z00_5057;
																						BgL_l1252z00_3724 =
																							BgL_l1252z00_5056;
																						goto
																							BgL_zc3z04anonymousza31886ze3z87_3723;
																					}
																				}
																			}
																	}
																}
															}
													}
													BgL_arg1883z00_3716 =
														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
														(BgL_arg1884z00_3717, BNIL);
												}
												BgL_arg1880z00_3714 =
													MAKE_YOUNG_PAIR(BgL_arg1882z00_3715,
													BgL_arg1883z00_3716);
											}
											BgL_arg1879z00_3713 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(16),
												BgL_arg1880z00_3714);
										}
										BgL_arg1877z00_3711 =
											MAKE_YOUNG_PAIR(BBOOL(BgL_arg1878z00_3712),
											BgL_arg1879z00_3713);
									}
									BgL_arg1875z00_3709 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(14), BgL_arg1877z00_3711);
								}
								BgL_arg1770z00_3643 =
									MAKE_YOUNG_PAIR(BgL_arg1874z00_3708, BgL_arg1875z00_3709);
							}
						else
							{	/* Ast/dump.scm 139 */
								if (CBOOL(BGl_za2alloczd2shapezf3za2z21zzengine_paramz00))
									{	/* Ast/dump.scm 145 */
										obj_t BgL_arg1893z00_3730;
										obj_t BgL_arg1894z00_3731;

										{	/* Ast/dump.scm 145 */
											BgL_varz00_bglt BgL_arg1896z00_3732;

											BgL_arg1896z00_3732 =
												(((BgL_appz00_bglt) COBJECT(
														((BgL_appz00_bglt) BgL_nodez00_3119)))->BgL_funz00);
											BgL_arg1893z00_3730 =
												BGl_nodezd2ze3sexpz31zzast_dumpz00(
												((BgL_nodez00_bglt) BgL_arg1896z00_3732));
										}
										{	/* Ast/dump.scm 146 */
											obj_t BgL_arg1897z00_3733;

											{	/* Ast/dump.scm 146 */
												obj_t BgL_arg1898z00_3734;
												obj_t BgL_arg1899z00_3735;

												BgL_arg1898z00_3734 =
													(((BgL_appz00_bglt) COBJECT(
															((BgL_appz00_bglt) BgL_nodez00_3119)))->
													BgL_stackablez00);
												{	/* Ast/dump.scm 147 */
													obj_t BgL_arg1901z00_3736;

													{	/* Ast/dump.scm 147 */
														obj_t BgL_l1258z00_3737;

														BgL_l1258z00_3737 =
															(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt) BgL_nodez00_3119)))->
															BgL_argsz00);
														if (NULLP(BgL_l1258z00_3737))
															{	/* Ast/dump.scm 147 */
																BgL_arg1901z00_3736 = BNIL;
															}
														else
															{	/* Ast/dump.scm 147 */
																obj_t BgL_head1260z00_3738;

																{	/* Ast/dump.scm 147 */
																	obj_t BgL_arg1912z00_3739;

																	{	/* Ast/dump.scm 147 */
																		obj_t BgL_arg1913z00_3740;

																		BgL_arg1913z00_3740 =
																			CAR(((obj_t) BgL_l1258z00_3737));
																		BgL_arg1912z00_3739 =
																			BGl_nodezd2ze3sexpz31zzast_dumpz00(
																			((BgL_nodez00_bglt) BgL_arg1913z00_3740));
																	}
																	BgL_head1260z00_3738 =
																		MAKE_YOUNG_PAIR(BgL_arg1912z00_3739, BNIL);
																}
																{	/* Ast/dump.scm 147 */
																	obj_t BgL_g1263z00_3741;

																	BgL_g1263z00_3741 =
																		CDR(((obj_t) BgL_l1258z00_3737));
																	{
																		obj_t BgL_l1258z00_3743;
																		obj_t BgL_tail1261z00_3744;

																		BgL_l1258z00_3743 = BgL_g1263z00_3741;
																		BgL_tail1261z00_3744 = BgL_head1260z00_3738;
																	BgL_zc3z04anonymousza31903ze3z87_3742:
																		if (NULLP(BgL_l1258z00_3743))
																			{	/* Ast/dump.scm 147 */
																				BgL_arg1901z00_3736 =
																					BgL_head1260z00_3738;
																			}
																		else
																			{	/* Ast/dump.scm 147 */
																				obj_t BgL_newtail1262z00_3745;

																				{	/* Ast/dump.scm 147 */
																					obj_t BgL_arg1910z00_3746;

																					{	/* Ast/dump.scm 147 */
																						obj_t BgL_arg1911z00_3747;

																						BgL_arg1911z00_3747 =
																							CAR(((obj_t) BgL_l1258z00_3743));
																						BgL_arg1910z00_3746 =
																							BGl_nodezd2ze3sexpz31zzast_dumpz00
																							(((BgL_nodez00_bglt)
																								BgL_arg1911z00_3747));
																					}
																					BgL_newtail1262z00_3745 =
																						MAKE_YOUNG_PAIR(BgL_arg1910z00_3746,
																						BNIL);
																				}
																				SET_CDR(BgL_tail1261z00_3744,
																					BgL_newtail1262z00_3745);
																				{	/* Ast/dump.scm 147 */
																					obj_t BgL_arg1906z00_3748;

																					BgL_arg1906z00_3748 =
																						CDR(((obj_t) BgL_l1258z00_3743));
																					{
																						obj_t BgL_tail1261z00_5097;
																						obj_t BgL_l1258z00_5096;

																						BgL_l1258z00_5096 =
																							BgL_arg1906z00_3748;
																						BgL_tail1261z00_5097 =
																							BgL_newtail1262z00_3745;
																						BgL_tail1261z00_3744 =
																							BgL_tail1261z00_5097;
																						BgL_l1258z00_3743 =
																							BgL_l1258z00_5096;
																						goto
																							BgL_zc3z04anonymousza31903ze3z87_3742;
																					}
																				}
																			}
																	}
																}
															}
													}
													BgL_arg1899z00_3735 =
														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
														(BgL_arg1901z00_3736, BNIL);
												}
												BgL_arg1897z00_3733 =
													MAKE_YOUNG_PAIR(BgL_arg1898z00_3734,
													BgL_arg1899z00_3735);
											}
											BgL_arg1894z00_3731 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(16),
												BgL_arg1897z00_3733);
										}
										BgL_arg1770z00_3643 =
											MAKE_YOUNG_PAIR(BgL_arg1893z00_3730, BgL_arg1894z00_3731);
									}
								else
									{	/* Ast/dump.scm 149 */
										obj_t BgL_arg1914z00_3749;
										obj_t BgL_arg1916z00_3750;

										{	/* Ast/dump.scm 149 */
											BgL_varz00_bglt BgL_arg1917z00_3751;

											BgL_arg1917z00_3751 =
												(((BgL_appz00_bglt) COBJECT(
														((BgL_appz00_bglt) BgL_nodez00_3119)))->BgL_funz00);
											BgL_arg1914z00_3749 =
												BGl_nodezd2ze3sexpz31zzast_dumpz00(
												((BgL_nodez00_bglt) BgL_arg1917z00_3751));
										}
										{	/* Ast/dump.scm 150 */
											obj_t BgL_arg1918z00_3752;

											{	/* Ast/dump.scm 150 */
												obj_t BgL_l1264z00_3753;

												BgL_l1264z00_3753 =
													(((BgL_appz00_bglt) COBJECT(
															((BgL_appz00_bglt) BgL_nodez00_3119)))->
													BgL_argsz00);
												if (NULLP(BgL_l1264z00_3753))
													{	/* Ast/dump.scm 150 */
														BgL_arg1918z00_3752 = BNIL;
													}
												else
													{	/* Ast/dump.scm 150 */
														obj_t BgL_head1266z00_3754;

														{	/* Ast/dump.scm 150 */
															obj_t BgL_arg1926z00_3755;

															{	/* Ast/dump.scm 150 */
																obj_t BgL_arg1927z00_3756;

																BgL_arg1927z00_3756 =
																	CAR(((obj_t) BgL_l1264z00_3753));
																BgL_arg1926z00_3755 =
																	BGl_nodezd2ze3sexpz31zzast_dumpz00(
																	((BgL_nodez00_bglt) BgL_arg1927z00_3756));
															}
															BgL_head1266z00_3754 =
																MAKE_YOUNG_PAIR(BgL_arg1926z00_3755, BNIL);
														}
														{	/* Ast/dump.scm 150 */
															obj_t BgL_g1269z00_3757;

															BgL_g1269z00_3757 =
																CDR(((obj_t) BgL_l1264z00_3753));
															{
																obj_t BgL_l1264z00_3759;
																obj_t BgL_tail1267z00_3760;

																BgL_l1264z00_3759 = BgL_g1269z00_3757;
																BgL_tail1267z00_3760 = BgL_head1266z00_3754;
															BgL_zc3z04anonymousza31920ze3z87_3758:
																if (NULLP(BgL_l1264z00_3759))
																	{	/* Ast/dump.scm 150 */
																		BgL_arg1918z00_3752 = BgL_head1266z00_3754;
																	}
																else
																	{	/* Ast/dump.scm 150 */
																		obj_t BgL_newtail1268z00_3761;

																		{	/* Ast/dump.scm 150 */
																			obj_t BgL_arg1924z00_3762;

																			{	/* Ast/dump.scm 150 */
																				obj_t BgL_arg1925z00_3763;

																				BgL_arg1925z00_3763 =
																					CAR(((obj_t) BgL_l1264z00_3759));
																				BgL_arg1924z00_3762 =
																					BGl_nodezd2ze3sexpz31zzast_dumpz00(
																					((BgL_nodez00_bglt)
																						BgL_arg1925z00_3763));
																			}
																			BgL_newtail1268z00_3761 =
																				MAKE_YOUNG_PAIR(BgL_arg1924z00_3762,
																				BNIL);
																		}
																		SET_CDR(BgL_tail1267z00_3760,
																			BgL_newtail1268z00_3761);
																		{	/* Ast/dump.scm 150 */
																			obj_t BgL_arg1923z00_3764;

																			BgL_arg1923z00_3764 =
																				CDR(((obj_t) BgL_l1264z00_3759));
																			{
																				obj_t BgL_tail1267z00_5129;
																				obj_t BgL_l1264z00_5128;

																				BgL_l1264z00_5128 = BgL_arg1923z00_3764;
																				BgL_tail1267z00_5129 =
																					BgL_newtail1268z00_3761;
																				BgL_tail1267z00_3760 =
																					BgL_tail1267z00_5129;
																				BgL_l1264z00_3759 = BgL_l1264z00_5128;
																				goto
																					BgL_zc3z04anonymousza31920ze3z87_3758;
																			}
																		}
																	}
															}
														}
													}
											}
											BgL_arg1916z00_3750 =
												BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
												(BgL_arg1918z00_3752, BNIL);
										}
										BgL_arg1770z00_3643 =
											MAKE_YOUNG_PAIR(BgL_arg1914z00_3749, BgL_arg1916z00_3750);
									}
							}
					}
				return
					BGl_locationzd2shapezd2zztools_locationz00(BgL_arg1767z00_3642,
					BgL_arg1770z00_3643);
			}
		}

	}



/* &node->sexp-sync1359 */
	obj_t BGl_z62nodezd2ze3sexpzd2sync1359z81zzast_dumpz00(obj_t BgL_envz00_3120,
		obj_t BgL_nodez00_3121)
	{
		{	/* Ast/dump.scm 107 */
			{	/* Ast/dump.scm 109 */
				obj_t BgL_symz00_3766;

				{	/* Ast/dump.scm 109 */
					BgL_typez00_bglt BgL_arg1765z00_3767;

					BgL_arg1765z00_3767 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_syncz00_bglt) BgL_nodez00_3121))))->BgL_typez00);
					{	/* Ast/dump.scm 109 */
						obj_t BgL_idz00_3768;

						BgL_idz00_3768 = CNST_TABLE_REF(45);
						if (CBOOL(BGl_za2typenodezd2shapezf3za2z21zzengine_paramz00))
							{	/* Ast/dump.scm 442 */
								obj_t BgL_arg1489z00_3769;

								{	/* Ast/dump.scm 442 */
									obj_t BgL_arg1502z00_3770;
									obj_t BgL_arg1509z00_3771;

									{	/* Ast/dump.scm 442 */
										obj_t BgL_arg1455z00_3772;

										BgL_arg1455z00_3772 = SYMBOL_TO_STRING(BgL_idz00_3768);
										BgL_arg1502z00_3770 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_3772);
									}
									BgL_arg1509z00_3771 =
										BGl_shapez00zztools_shapez00(((obj_t) BgL_arg1765z00_3767));
									BgL_arg1489z00_3769 =
										string_append_3(BgL_arg1502z00_3770,
										BGl_string2407z00zzast_dumpz00, BgL_arg1509z00_3771);
								}
								BgL_symz00_3766 = bstring_to_symbol(BgL_arg1489z00_3769);
							}
						else
							{	/* Ast/dump.scm 441 */
								BgL_symz00_3766 = BgL_idz00_3768;
							}
					}
				}
				{	/* Ast/dump.scm 110 */
					obj_t BgL_arg1746z00_3773;
					obj_t BgL_arg1747z00_3774;

					BgL_arg1746z00_3773 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_syncz00_bglt) BgL_nodez00_3121))))->BgL_locz00);
					{	/* Ast/dump.scm 111 */
						obj_t BgL_arg1748z00_3775;

						{	/* Ast/dump.scm 111 */
							obj_t BgL_arg1749z00_3776;
							obj_t BgL_arg1750z00_3777;

							BgL_arg1749z00_3776 =
								BGl_nodezd2ze3sexpz31zzast_dumpz00(
								(((BgL_syncz00_bglt) COBJECT(
											((BgL_syncz00_bglt) BgL_nodez00_3121)))->BgL_mutexz00));
							{	/* Ast/dump.scm 112 */
								obj_t BgL_arg1752z00_3778;

								{	/* Ast/dump.scm 112 */
									obj_t BgL_arg1753z00_3779;
									obj_t BgL_arg1754z00_3780;

									BgL_arg1753z00_3779 =
										BGl_nodezd2ze3sexpz31zzast_dumpz00(
										(((BgL_syncz00_bglt) COBJECT(
													((BgL_syncz00_bglt) BgL_nodez00_3121)))->
											BgL_prelockz00));
									BgL_arg1754z00_3780 =
										MAKE_YOUNG_PAIR(BGl_nodezd2ze3sexpz31zzast_dumpz00(((
													(BgL_syncz00_bglt) COBJECT(((BgL_syncz00_bglt)
															BgL_nodez00_3121)))->BgL_bodyz00)), BNIL);
									BgL_arg1752z00_3778 =
										MAKE_YOUNG_PAIR(BgL_arg1753z00_3779, BgL_arg1754z00_3780);
								}
								BgL_arg1750z00_3777 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(46), BgL_arg1752z00_3778);
							}
							BgL_arg1748z00_3775 =
								MAKE_YOUNG_PAIR(BgL_arg1749z00_3776, BgL_arg1750z00_3777);
						}
						BgL_arg1747z00_3774 =
							MAKE_YOUNG_PAIR(BgL_symz00_3766, BgL_arg1748z00_3775);
					}
					return
						BGl_locationzd2shapezd2zztools_locationz00(BgL_arg1746z00_3773,
						BgL_arg1747z00_3774);
				}
			}
		}

	}



/* &node->sexp-sequence1357 */
	obj_t BGl_z62nodezd2ze3sexpzd2sequence1357z81zzast_dumpz00(obj_t
		BgL_envz00_3122, obj_t BgL_nodez00_3123)
	{
		{	/* Ast/dump.scm 95 */
			{	/* Ast/dump.scm 98 */
				obj_t BgL_symz00_3782;

				{	/* Ast/dump.scm 99 */
					obj_t BgL_arg1738z00_3783;
					BgL_typez00_bglt BgL_arg1739z00_3784;

					if (
						(((BgL_sequencez00_bglt) COBJECT(
									((BgL_sequencez00_bglt) BgL_nodez00_3123)))->BgL_unsafez00))
						{	/* Ast/dump.scm 99 */
							BgL_arg1738z00_3783 = CNST_TABLE_REF(47);
						}
					else
						{	/* Ast/dump.scm 99 */
							BgL_arg1738z00_3783 = CNST_TABLE_REF(48);
						}
					BgL_arg1739z00_3784 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_sequencez00_bglt) BgL_nodez00_3123))))->BgL_typez00);
					BgL_symz00_3782 =
						BGl_shapezd2typedzd2nodez00zzast_dumpz00(BgL_arg1738z00_3783,
						BgL_arg1739z00_3784);
				}
				{	/* Ast/dump.scm 100 */
					obj_t BgL_arg1708z00_3785;
					obj_t BgL_arg1709z00_3786;

					BgL_arg1708z00_3785 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_sequencez00_bglt) BgL_nodez00_3123))))->BgL_locz00);
					{	/* Ast/dump.scm 101 */
						obj_t BgL_arg1710z00_3787;

						{	/* Ast/dump.scm 101 */
							obj_t BgL_arg1711z00_3788;
							obj_t BgL_arg1714z00_3789;

							if (NULLP(
									(((BgL_sequencez00_bglt) COBJECT(
												((BgL_sequencez00_bglt) BgL_nodez00_3123)))->
										BgL_metaz00)))
								{	/* Ast/dump.scm 101 */
									BgL_arg1711z00_3788 = BNIL;
								}
							else
								{	/* Ast/dump.scm 101 */
									obj_t BgL_arg1720z00_3790;

									BgL_arg1720z00_3790 =
										(((BgL_sequencez00_bglt) COBJECT(
												((BgL_sequencez00_bglt) BgL_nodez00_3123)))->
										BgL_metaz00);
									{	/* Ast/dump.scm 101 */
										obj_t BgL_list1721z00_3791;

										BgL_list1721z00_3791 =
											MAKE_YOUNG_PAIR(BgL_arg1720z00_3790, BNIL);
										BgL_arg1711z00_3788 = BgL_list1721z00_3791;
									}
								}
							{	/* Ast/dump.scm 102 */
								obj_t BgL_arg1724z00_3792;

								{	/* Ast/dump.scm 102 */
									obj_t BgL_l1234z00_3793;

									BgL_l1234z00_3793 =
										(((BgL_sequencez00_bglt) COBJECT(
												((BgL_sequencez00_bglt) BgL_nodez00_3123)))->
										BgL_nodesz00);
									if (NULLP(BgL_l1234z00_3793))
										{	/* Ast/dump.scm 102 */
											BgL_arg1724z00_3792 = BNIL;
										}
									else
										{	/* Ast/dump.scm 102 */
											obj_t BgL_head1236z00_3794;

											{	/* Ast/dump.scm 102 */
												obj_t BgL_arg1736z00_3795;

												{	/* Ast/dump.scm 102 */
													obj_t BgL_arg1737z00_3796;

													BgL_arg1737z00_3796 = CAR(BgL_l1234z00_3793);
													BgL_arg1736z00_3795 =
														BGl_nodezd2ze3sexpz31zzast_dumpz00(
														((BgL_nodez00_bglt) BgL_arg1737z00_3796));
												}
												BgL_head1236z00_3794 =
													MAKE_YOUNG_PAIR(BgL_arg1736z00_3795, BNIL);
											}
											{	/* Ast/dump.scm 102 */
												obj_t BgL_g1239z00_3797;

												BgL_g1239z00_3797 = CDR(BgL_l1234z00_3793);
												{
													obj_t BgL_l1234z00_3799;
													obj_t BgL_tail1237z00_3800;

													BgL_l1234z00_3799 = BgL_g1239z00_3797;
													BgL_tail1237z00_3800 = BgL_head1236z00_3794;
												BgL_zc3z04anonymousza31726ze3z87_3798:
													if (NULLP(BgL_l1234z00_3799))
														{	/* Ast/dump.scm 102 */
															BgL_arg1724z00_3792 = BgL_head1236z00_3794;
														}
													else
														{	/* Ast/dump.scm 102 */
															obj_t BgL_newtail1238z00_3801;

															{	/* Ast/dump.scm 102 */
																obj_t BgL_arg1734z00_3802;

																{	/* Ast/dump.scm 102 */
																	obj_t BgL_arg1735z00_3803;

																	BgL_arg1735z00_3803 =
																		CAR(((obj_t) BgL_l1234z00_3799));
																	BgL_arg1734z00_3802 =
																		BGl_nodezd2ze3sexpz31zzast_dumpz00(
																		((BgL_nodez00_bglt) BgL_arg1735z00_3803));
																}
																BgL_newtail1238z00_3801 =
																	MAKE_YOUNG_PAIR(BgL_arg1734z00_3802, BNIL);
															}
															SET_CDR(BgL_tail1237z00_3800,
																BgL_newtail1238z00_3801);
															{	/* Ast/dump.scm 102 */
																obj_t BgL_arg1733z00_3804;

																BgL_arg1733z00_3804 =
																	CDR(((obj_t) BgL_l1234z00_3799));
																{
																	obj_t BgL_tail1237z00_5203;
																	obj_t BgL_l1234z00_5202;

																	BgL_l1234z00_5202 = BgL_arg1733z00_3804;
																	BgL_tail1237z00_5203 =
																		BgL_newtail1238z00_3801;
																	BgL_tail1237z00_3800 = BgL_tail1237z00_5203;
																	BgL_l1234z00_3799 = BgL_l1234z00_5202;
																	goto BgL_zc3z04anonymousza31726ze3z87_3798;
																}
															}
														}
												}
											}
										}
								}
								BgL_arg1714z00_3789 =
									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
									(BgL_arg1724z00_3792, BNIL);
							}
							BgL_arg1710z00_3787 =
								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
								(BgL_arg1711z00_3788, BgL_arg1714z00_3789);
						}
						BgL_arg1709z00_3786 =
							MAKE_YOUNG_PAIR(BgL_symz00_3782, BgL_arg1710z00_3787);
					}
					return
						BGl_locationzd2shapezd2zztools_locationz00(BgL_arg1708z00_3785,
						BgL_arg1709z00_3786);
				}
			}
		}

	}



/* &node->sexp-kwote1355 */
	obj_t BGl_z62nodezd2ze3sexpzd2kwote1355z81zzast_dumpz00(obj_t BgL_envz00_3124,
		obj_t BgL_nodez00_3125)
	{
		{	/* Ast/dump.scm 83 */
			{	/* Ast/dump.scm 85 */
				obj_t BgL_arg1689z00_3806;
				obj_t BgL_arg1691z00_3807;

				BgL_arg1689z00_3806 =
					(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_kwotez00_bglt) BgL_nodez00_3125))))->BgL_locz00);
				{	/* Ast/dump.scm 86 */
					obj_t BgL_valuez00_3808;

					BgL_valuez00_3808 =
						(((BgL_kwotez00_bglt) COBJECT(
								((BgL_kwotez00_bglt) BgL_nodez00_3125)))->BgL_valuez00);
					{	/* Ast/dump.scm 87 */
						bool_t BgL_test2583z00_5213;

						if (STRUCTP(BgL_valuez00_3808))
							{	/* Ast/dump.scm 87 */
								BgL_test2583z00_5213 =
									(STRUCT_KEY(BgL_valuez00_3808) == CNST_TABLE_REF(49));
							}
						else
							{	/* Ast/dump.scm 87 */
								BgL_test2583z00_5213 = ((bool_t) 0);
							}
						if (BgL_test2583z00_5213)
							{	/* Ast/dump.scm 88 */
								obj_t BgL_arg1699z00_3809;

								{	/* Ast/dump.scm 88 */
									obj_t BgL_arg1700z00_3810;

									{	/* Ast/dump.scm 88 */
										obj_t BgL_arg1701z00_3811;
										obj_t BgL_arg1702z00_3812;

										BgL_arg1701z00_3811 =
											BGl_shapez00zztools_shapez00(STRUCT_REF(BgL_valuez00_3808,
												(int) (0L)));
										BgL_arg1702z00_3812 =
											STRUCT_REF(BgL_valuez00_3808, (int) (1L));
										{	/* Ast/dump.scm 88 */
											obj_t BgL_newz00_3813;

											BgL_newz00_3813 =
												create_struct(CNST_TABLE_REF(49), (int) (2L));
											{	/* Ast/dump.scm 88 */
												int BgL_tmpz00_5227;

												BgL_tmpz00_5227 = (int) (1L);
												STRUCT_SET(BgL_newz00_3813, BgL_tmpz00_5227,
													BgL_arg1702z00_3812);
											}
											{	/* Ast/dump.scm 88 */
												int BgL_tmpz00_5230;

												BgL_tmpz00_5230 = (int) (0L);
												STRUCT_SET(BgL_newz00_3813, BgL_tmpz00_5230,
													BgL_arg1701z00_3811);
											}
											BgL_arg1700z00_3810 = BgL_newz00_3813;
									}}
									BgL_arg1699z00_3809 =
										MAKE_YOUNG_PAIR(BgL_arg1700z00_3810, BNIL);
								}
								BgL_arg1691z00_3807 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(50), BgL_arg1699z00_3809);
							}
						else
							{	/* Ast/dump.scm 90 */
								obj_t BgL_arg1705z00_3814;

								BgL_arg1705z00_3814 = MAKE_YOUNG_PAIR(BgL_valuez00_3808, BNIL);
								BgL_arg1691z00_3807 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(50), BgL_arg1705z00_3814);
							}
					}
				}
				return
					BGl_locationzd2shapezd2zztools_locationz00(BgL_arg1689z00_3806,
					BgL_arg1691z00_3807);
			}
		}

	}



/* &node->sexp-closure1353 */
	obj_t BGl_z62nodezd2ze3sexpzd2closure1353z81zzast_dumpz00(obj_t
		BgL_envz00_3126, obj_t BgL_nodez00_3127)
	{
		{	/* Ast/dump.scm 70 */
			{	/* Ast/dump.scm 73 */
				BgL_variablez00_bglt BgL_vz00_3816;

				BgL_vz00_3816 =
					(((BgL_varz00_bglt) COBJECT(
							((BgL_varz00_bglt)
								((BgL_closurez00_bglt) BgL_nodez00_3127))))->BgL_variablez00);
				{	/* Ast/dump.scm 73 */
					BgL_valuez00_bglt BgL_fz00_3817;

					BgL_fz00_3817 =
						(((BgL_variablez00_bglt) COBJECT(BgL_vz00_3816))->BgL_valuez00);
					{	/* Ast/dump.scm 74 */

						{	/* Ast/dump.scm 75 */
							obj_t BgL_arg1642z00_3818;
							obj_t BgL_arg1646z00_3819;

							BgL_arg1642z00_3818 =
								(((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_closurez00_bglt) BgL_nodez00_3127))))->BgL_locz00);
							{	/* Ast/dump.scm 76 */
								obj_t BgL_arg1650z00_3820;
								obj_t BgL_arg1651z00_3821;

								{	/* Ast/dump.scm 76 */
									BgL_typez00_bglt BgL_arg1654z00_3822;

									BgL_arg1654z00_3822 =
										(((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt)
													((BgL_closurez00_bglt) BgL_nodez00_3127))))->
										BgL_typez00);
									{	/* Ast/dump.scm 76 */
										obj_t BgL_idz00_3823;

										BgL_idz00_3823 = CNST_TABLE_REF(51);
										if (CBOOL
											(BGl_za2typenodezd2shapezf3za2z21zzengine_paramz00))
											{	/* Ast/dump.scm 442 */
												obj_t BgL_arg1489z00_3824;

												{	/* Ast/dump.scm 442 */
													obj_t BgL_arg1502z00_3825;
													obj_t BgL_arg1509z00_3826;

													{	/* Ast/dump.scm 442 */
														obj_t BgL_arg1455z00_3827;

														BgL_arg1455z00_3827 =
															SYMBOL_TO_STRING(BgL_idz00_3823);
														BgL_arg1502z00_3825 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1455z00_3827);
													}
													BgL_arg1509z00_3826 =
														BGl_shapez00zztools_shapez00(
														((obj_t) BgL_arg1654z00_3822));
													BgL_arg1489z00_3824 =
														string_append_3(BgL_arg1502z00_3825,
														BGl_string2407z00zzast_dumpz00,
														BgL_arg1509z00_3826);
												}
												BgL_arg1650z00_3820 =
													bstring_to_symbol(BgL_arg1489z00_3824);
											}
										else
											{	/* Ast/dump.scm 441 */
												BgL_arg1650z00_3820 = BgL_idz00_3823;
											}
									}
								}
								{	/* Ast/dump.scm 77 */
									obj_t BgL_arg1661z00_3828;
									obj_t BgL_arg1663z00_3829;

									if (CBOOL(BGl_za2alloczd2shapezf3za2z21zzengine_paramz00))
										{	/* Ast/dump.scm 77 */
											obj_t BgL_arg1675z00_3830;

											{	/* Ast/dump.scm 77 */
												obj_t BgL_arg1678z00_3831;

												BgL_arg1678z00_3831 =
													(((BgL_sfunz00_bglt) COBJECT(
															((BgL_sfunz00_bglt) BgL_fz00_3817)))->
													BgL_stackablez00);
												BgL_arg1675z00_3830 =
													MAKE_YOUNG_PAIR(BgL_arg1678z00_3831, BNIL);
											}
											BgL_arg1661z00_3828 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(16),
												BgL_arg1675z00_3830);
										}
									else
										{	/* Ast/dump.scm 77 */
											BgL_arg1661z00_3828 = BNIL;
										}
									{	/* Ast/dump.scm 78 */
										obj_t BgL_arg1681z00_3832;

										{	/* Ast/dump.scm 78 */
											BgL_variablez00_bglt BgL_arg1688z00_3833;

											BgL_arg1688z00_3833 =
												(((BgL_varz00_bglt) COBJECT(
														((BgL_varz00_bglt)
															((BgL_closurez00_bglt) BgL_nodez00_3127))))->
												BgL_variablez00);
											BgL_arg1681z00_3832 =
												BGl_shapez00zztools_shapez00(((obj_t)
													BgL_arg1688z00_3833));
										}
										BgL_arg1663z00_3829 =
											MAKE_YOUNG_PAIR(BgL_arg1681z00_3832, BNIL);
									}
									BgL_arg1651z00_3821 =
										BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
										(BgL_arg1661z00_3828, BgL_arg1663z00_3829);
								}
								BgL_arg1646z00_3819 =
									MAKE_YOUNG_PAIR(BgL_arg1650z00_3820, BgL_arg1651z00_3821);
							}
							return
								BGl_locationzd2shapezd2zztools_locationz00(BgL_arg1642z00_3818,
								BgL_arg1646z00_3819);
						}
					}
				}
			}
		}

	}



/* &node->sexp-var1351 */
	obj_t BGl_z62nodezd2ze3sexpzd2var1351z81zzast_dumpz00(obj_t BgL_envz00_3128,
		obj_t BgL_nodez00_3129)
	{
		{	/* Ast/dump.scm 56 */
			{	/* Ast/dump.scm 59 */
				obj_t BgL_vshapez00_3835;

				{	/* Ast/dump.scm 59 */
					BgL_variablez00_bglt BgL_arg1630z00_3836;

					BgL_arg1630z00_3836 =
						(((BgL_varz00_bglt) COBJECT(
								((BgL_varz00_bglt) BgL_nodez00_3129)))->BgL_variablez00);
					BgL_vshapez00_3835 =
						BGl_shapez00zztools_shapez00(((obj_t) BgL_arg1630z00_3836));
				}
				{	/* Ast/dump.scm 59 */
					obj_t BgL_tvshapez00_3837;

					{	/* Ast/dump.scm 60 */
						bool_t BgL_test2587z00_5279;

						if (CBOOL(BGl_za2typezd2shapezf3za2z21zzengine_paramz00))
							{	/* Ast/dump.scm 61 */
								bool_t BgL_test2589z00_5282;

								{	/* Ast/dump.scm 61 */
									BgL_valuez00_bglt BgL_arg1627z00_3838;

									BgL_arg1627z00_3838 =
										(((BgL_variablez00_bglt) COBJECT(
												(((BgL_varz00_bglt) COBJECT(
															((BgL_varz00_bglt) BgL_nodez00_3129)))->
													BgL_variablez00)))->BgL_valuez00);
									{	/* Ast/dump.scm 61 */
										obj_t BgL_classz00_3839;

										BgL_classz00_3839 = BGl_sfunz00zzast_varz00;
										{	/* Ast/dump.scm 61 */
											BgL_objectz00_bglt BgL_arg1807z00_3840;

											{	/* Ast/dump.scm 61 */
												obj_t BgL_tmpz00_5286;

												BgL_tmpz00_5286 =
													((obj_t) ((BgL_objectz00_bglt) BgL_arg1627z00_3838));
												BgL_arg1807z00_3840 =
													(BgL_objectz00_bglt) (BgL_tmpz00_5286);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Ast/dump.scm 61 */
													long BgL_idxz00_3841;

													BgL_idxz00_3841 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3840);
													BgL_test2589z00_5282 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_3841 + 3L)) == BgL_classz00_3839);
												}
											else
												{	/* Ast/dump.scm 61 */
													bool_t BgL_res2399z00_3844;

													{	/* Ast/dump.scm 61 */
														obj_t BgL_oclassz00_3845;

														{	/* Ast/dump.scm 61 */
															obj_t BgL_arg1815z00_3846;
															long BgL_arg1816z00_3847;

															BgL_arg1815z00_3846 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Ast/dump.scm 61 */
																long BgL_arg1817z00_3848;

																BgL_arg1817z00_3848 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3840);
																BgL_arg1816z00_3847 =
																	(BgL_arg1817z00_3848 - OBJECT_TYPE);
															}
															BgL_oclassz00_3845 =
																VECTOR_REF(BgL_arg1815z00_3846,
																BgL_arg1816z00_3847);
														}
														{	/* Ast/dump.scm 61 */
															bool_t BgL__ortest_1115z00_3849;

															BgL__ortest_1115z00_3849 =
																(BgL_classz00_3839 == BgL_oclassz00_3845);
															if (BgL__ortest_1115z00_3849)
																{	/* Ast/dump.scm 61 */
																	BgL_res2399z00_3844 =
																		BgL__ortest_1115z00_3849;
																}
															else
																{	/* Ast/dump.scm 61 */
																	long BgL_odepthz00_3850;

																	{	/* Ast/dump.scm 61 */
																		obj_t BgL_arg1804z00_3851;

																		BgL_arg1804z00_3851 = (BgL_oclassz00_3845);
																		BgL_odepthz00_3850 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_3851);
																	}
																	if ((3L < BgL_odepthz00_3850))
																		{	/* Ast/dump.scm 61 */
																			obj_t BgL_arg1802z00_3852;

																			{	/* Ast/dump.scm 61 */
																				obj_t BgL_arg1803z00_3853;

																				BgL_arg1803z00_3853 =
																					(BgL_oclassz00_3845);
																				BgL_arg1802z00_3852 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_3853, 3L);
																			}
																			BgL_res2399z00_3844 =
																				(BgL_arg1802z00_3852 ==
																				BgL_classz00_3839);
																		}
																	else
																		{	/* Ast/dump.scm 61 */
																			BgL_res2399z00_3844 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2589z00_5282 = BgL_res2399z00_3844;
												}
										}
									}
								}
								if (BgL_test2589z00_5282)
									{	/* Ast/dump.scm 61 */
										BgL_test2587z00_5279 = ((bool_t) 0);
									}
								else
									{	/* Ast/dump.scm 61 */
										if (
											(((obj_t)
													(((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt)
																	((BgL_varz00_bglt) BgL_nodez00_3129))))->
														BgL_typez00)) ==
												((obj_t) (((BgL_variablez00_bglt)
															COBJECT((((BgL_varz00_bglt)
																		COBJECT(((BgL_varz00_bglt)
																				BgL_nodez00_3129)))->
																	BgL_variablez00)))->BgL_typez00))))
											{	/* Ast/dump.scm 62 */
												BgL_test2587z00_5279 = ((bool_t) 0);
											}
										else
											{	/* Ast/dump.scm 62 */
												BgL_test2587z00_5279 = ((bool_t) 1);
											}
									}
							}
						else
							{	/* Ast/dump.scm 60 */
								BgL_test2587z00_5279 = ((bool_t) 0);
							}
						if (BgL_test2587z00_5279)
							{	/* Ast/dump.scm 63 */
								obj_t BgL_arg1609z00_3854;

								{	/* Ast/dump.scm 63 */
									obj_t BgL_arg1611z00_3855;

									{	/* Ast/dump.scm 63 */
										BgL_typez00_bglt BgL_arg1615z00_3856;

										BgL_arg1615z00_3856 =
											(((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt)
														((BgL_varz00_bglt) BgL_nodez00_3129))))->
											BgL_typez00);
										BgL_arg1611z00_3855 =
											BGl_shapez00zztools_shapez00(((obj_t)
												BgL_arg1615z00_3856));
									}
									{	/* Ast/dump.scm 63 */
										obj_t BgL_list1612z00_3857;

										{	/* Ast/dump.scm 63 */
											obj_t BgL_arg1613z00_3858;

											BgL_arg1613z00_3858 =
												MAKE_YOUNG_PAIR(BgL_arg1611z00_3855, BNIL);
											BgL_list1612z00_3857 =
												MAKE_YOUNG_PAIR(BgL_vshapez00_3835,
												BgL_arg1613z00_3858);
										}
										BgL_arg1609z00_3854 =
											BGl_formatz00zz__r4_output_6_10_3z00
											(BGl_string2452z00zzast_dumpz00, BgL_list1612z00_3857);
									}
								}
								BgL_tvshapez00_3837 = bstring_to_symbol(BgL_arg1609z00_3854);
							}
						else
							{	/* Ast/dump.scm 60 */
								BgL_tvshapez00_3837 = BgL_vshapez00_3835;
							}
					}
					{	/* Ast/dump.scm 60 */

						{	/* Ast/dump.scm 65 */
							obj_t BgL_arg1573z00_3859;

							BgL_arg1573z00_3859 =
								(((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_varz00_bglt) BgL_nodez00_3129))))->BgL_locz00);
							return
								BGl_locationzd2shapezd2zztools_locationz00(BgL_arg1573z00_3859,
								BgL_tvshapez00_3837);
						}
					}
				}
			}
		}

	}



/* &node->sexp-atom1349 */
	obj_t BGl_z62nodezd2ze3sexpzd2atom1349z81zzast_dumpz00(obj_t BgL_envz00_3130,
		obj_t BgL_nodez00_3131)
	{
		{	/* Ast/dump.scm 42 */
			if (NULLP(
					(((BgL_atomz00_bglt) COBJECT(
								((BgL_atomz00_bglt) BgL_nodez00_3131)))->BgL_valuez00)))
				{	/* Ast/dump.scm 45 */
					obj_t BgL_arg1516z00_3861;

					BgL_arg1516z00_3861 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_atomz00_bglt) BgL_nodez00_3131))))->BgL_locz00);
					return
						BGl_locationzd2shapezd2zztools_locationz00(BgL_arg1516z00_3861,
						CNST_TABLE_REF(52));
				}
			else
				{	/* Ast/dump.scm 46 */
					obj_t BgL_arg1535z00_3862;
					obj_t BgL_arg1540z00_3863;

					BgL_arg1535z00_3862 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_atomz00_bglt) BgL_nodez00_3131))))->BgL_locz00);
					if (CBOOL(BGl_za2typezd2shapezf3za2z21zzengine_paramz00))
						{	/* Ast/dump.scm 48 */
							obj_t BgL_arg1544z00_3864;

							{	/* Ast/dump.scm 48 */
								obj_t BgL_arg1546z00_3865;
								obj_t BgL_arg1552z00_3866;

								BgL_arg1546z00_3865 =
									(((BgL_atomz00_bglt) COBJECT(
											((BgL_atomz00_bglt) BgL_nodez00_3131)))->BgL_valuez00);
								{	/* Ast/dump.scm 49 */
									obj_t BgL_arg1553z00_3867;
									obj_t BgL_arg1559z00_3868;

									{	/* Ast/dump.scm 49 */
										BgL_typez00_bglt BgL_arg1561z00_3869;

										BgL_arg1561z00_3869 =
											BGl_getzd2typezd2zztype_typeofz00(
											((BgL_nodez00_bglt)
												((BgL_atomz00_bglt) BgL_nodez00_3131)), ((bool_t) 0));
										BgL_arg1553z00_3867 =
											BGl_shapez00zztools_shapez00(
											((obj_t) BgL_arg1561z00_3869));
									}
									{	/* Ast/dump.scm 50 */
										obj_t BgL_arg1564z00_3870;

										{	/* Ast/dump.scm 50 */
											BgL_typez00_bglt BgL_arg1565z00_3871;

											BgL_arg1565z00_3871 =
												(((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt)
															((BgL_atomz00_bglt) BgL_nodez00_3131))))->
												BgL_typez00);
											BgL_arg1564z00_3870 =
												BGl_shapez00zztools_shapez00(((obj_t)
													BgL_arg1565z00_3871));
										}
										BgL_arg1559z00_3868 =
											MAKE_YOUNG_PAIR(BgL_arg1564z00_3870, BNIL);
									}
									BgL_arg1552z00_3866 =
										MAKE_YOUNG_PAIR(BgL_arg1553z00_3867, BgL_arg1559z00_3868);
								}
								BgL_arg1544z00_3864 =
									MAKE_YOUNG_PAIR(BgL_arg1546z00_3865, BgL_arg1552z00_3866);
							}
							BgL_arg1540z00_3863 =
								BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00
								(BgL_arg1544z00_3864);
						}
					else
						{	/* Ast/dump.scm 47 */
							BgL_arg1540z00_3863 =
								(((BgL_atomz00_bglt) COBJECT(
										((BgL_atomz00_bglt) BgL_nodez00_3131)))->BgL_valuez00);
						}
					return
						BGl_locationzd2shapezd2zztools_locationz00(BgL_arg1535z00_3862,
						BgL_arg1540z00_3863);
				}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_dumpz00(void)
	{
		{	/* Ast/dump.scm 14 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2453z00zzast_dumpz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2453z00zzast_dumpz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2453z00zzast_dumpz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2453z00zzast_dumpz00));
			BGl_modulezd2initializa7ationz75zztools_argsz00(47102372L,
				BSTRING_TO_STRING(BGl_string2453z00zzast_dumpz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string2453z00zzast_dumpz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string2453z00zzast_dumpz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2453z00zzast_dumpz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string2453z00zzast_dumpz00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string2453z00zzast_dumpz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2453z00zzast_dumpz00));
			return
				BGl_modulezd2initializa7ationz75zzeffect_effectz00(460136018L,
				BSTRING_TO_STRING(BGl_string2453z00zzast_dumpz00));
		}

	}

#ifdef __cplusplus
}
#endif
