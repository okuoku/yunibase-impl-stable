/*===========================================================================*/
/*   (Ast/shrinkify.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/shrinkify.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_AST_SHRINKIFY_TYPE_DEFINITIONS
#define BGL_AST_SHRINKIFY_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;


#endif													// BGL_AST_SHRINKIFY_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t
		BGl_shrinkzd2variablez12zc0zzast_shrinkifyz00(BgL_variablez00_bglt);
	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t
		BGl_z62shrinkzd2nodez12zd2letzd2fun1289za2zzast_shrinkifyz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzast_shrinkifyz00 = BUNSPEC;
	static obj_t BGl_z62shrinkzd2nodez121252za2zzast_shrinkifyz00(obj_t, obj_t);
	static obj_t
		BGl_z62shrinkzd2nodez12zd2appzd2ly1267za2zzast_shrinkifyz00(obj_t, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_toplevelzd2initzd2zzast_shrinkifyz00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	extern obj_t BGl_forzd2eachzd2globalz12z12zzast_envz00(obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzast_shrinkifyz00(void);
	static obj_t BGl_z62shrinkzd2variablez12za2zzast_shrinkifyz00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzast_shrinkifyz00(void);
	extern obj_t BGl_castz00zzast_nodez00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static obj_t BGl_z62shrinkzd2nodez12zd2extern1271z70zzast_shrinkifyz00(obj_t,
		obj_t);
	static obj_t BGl_z62shrinkzd2nodez12zd2switch1281z70zzast_shrinkifyz00(obj_t,
		obj_t);
	static obj_t BGl_z62shrinkzd2nodez12zd2kwote1259z70zzast_shrinkifyz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_shrinkifyz12z12zzast_shrinkifyz00(obj_t);
	static obj_t BGl_methodzd2initzd2zzast_shrinkifyz00(void);
	static obj_t
		BGl_z62shrinkzd2nodez12zd2setzd2exzd21293z70zzast_shrinkifyz00(obj_t,
		obj_t);
	static obj_t BGl_z62shrinkzd2nodez12zd2funcall1269z70zzast_shrinkifyz00(obj_t,
		obj_t);
	static obj_t BGl_z62shrinkzd2nodez12zd2cast1273z70zzast_shrinkifyz00(obj_t,
		obj_t);
	static obj_t BGl_z62shrinkzd2nodez12zd2conditi1277z70zzast_shrinkifyz00(obj_t,
		obj_t);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t BGl_z62shrinkifyz12z70zzast_shrinkifyz00(obj_t, obj_t);
	static obj_t BGl_z62shrinkzd2nodez12zd2setq1275z70zzast_shrinkifyz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62shrinkzd2nodez12zd2boxzd2set1287za2zzast_shrinkifyz00(obj_t, obj_t);
	static obj_t
		BGl_z62shrinkzd2nodez12zd2letzd2var1291za2zzast_shrinkifyz00(obj_t, obj_t);
	static obj_t BGl_z62shrinkzd2nodez12zd2sync1263z70zzast_shrinkifyz00(obj_t,
		obj_t);
	static obj_t BGl_z62shrinkzd2nodez12zd2fail1279z70zzast_shrinkifyz00(obj_t,
		obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t
		BGl_z62shrinkzd2nodez12zd2boxzd2ref1285za2zzast_shrinkifyz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62shrinkzd2variablez121250za2zzast_shrinkifyz00(obj_t,
		obj_t);
	static obj_t BGl_z62shrinkzd2nodez12zd2sequenc1261z70zzast_shrinkifyz00(obj_t,
		obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static obj_t BGl_z62shrinkzd2nodez12zd2var1257z70zzast_shrinkifyz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzast_shrinkifyz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t
		BGl_z62shrinkzd2nodez12zd2makezd2bo1283za2zzast_shrinkifyz00(obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzast_shrinkifyz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_shrinkifyz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzast_shrinkifyz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzast_shrinkifyz00(void);
	static obj_t BGl_z62shrinkzd2nodez12zd2app1265z70zzast_shrinkifyz00(obj_t,
		obj_t);
	static obj_t BGl_z62shrinkzd2nodez12za2zzast_shrinkifyz00(obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_variablez00zzast_varz00;
	extern obj_t BGl_switchz00zzast_nodez00;
	static bool_t BGl_shrinkzd2nodeza2z12z62zzast_shrinkifyz00(obj_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t
		BGl_z62shrinkzd2nodez12zd2jumpzd2ex1295za2zzast_shrinkifyz00(obj_t, obj_t);
	static obj_t BGl_z62shrinkzd2nodez12zd2atom1255z70zzast_shrinkifyz00(obj_t,
		obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static obj_t __cnst[1];


	   
		 
		DEFINE_STRING(BGl_string1800z00zzast_shrinkifyz00,
		BgL_bgl_string1800za700za7za7a1806za7, "shrink-node!1252 ", 17);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_shrinkifyz12zd2envzc0zzast_shrinkifyz00,
		BgL_bgl_za762shrinkifyza712za71807za7,
		BGl_z62shrinkifyz12z70zzast_shrinkifyz00, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_GENERIC
		(BGl_shrinkzd2variablez12zd2envz12zzast_shrinkifyz00,
		BgL_bgl_za762shrinkza7d2vari1808z00,
		BGl_z62shrinkzd2variablez12za2zzast_shrinkifyz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1773z00zzast_shrinkifyz00,
		BgL_bgl_string1773za700za7za7a1809za7, "shrink-variable!1250", 20);
	      DEFINE_STRING(BGl_string1775z00zzast_shrinkifyz00,
		BgL_bgl_string1775za700za7za7a1810za7, "shrink-node!1252", 16);
	      DEFINE_STRING(BGl_string1776z00zzast_shrinkifyz00,
		BgL_bgl_string1776za700za7za7a1811za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string1778z00zzast_shrinkifyz00,
		BgL_bgl_string1778za700za7za7a1812za7, "shrink-node!", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1772z00zzast_shrinkifyz00,
		BgL_bgl_za762shrinkza7d2vari1813z00,
		BGl_z62shrinkzd2variablez121250za2zzast_shrinkifyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1774z00zzast_shrinkifyz00,
		BgL_bgl_za762shrinkza7d2node1814z00,
		BGl_z62shrinkzd2nodez121252za2zzast_shrinkifyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1777z00zzast_shrinkifyz00,
		BgL_bgl_za762shrinkza7d2node1815z00,
		BGl_z62shrinkzd2nodez12zd2atom1255z70zzast_shrinkifyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1779z00zzast_shrinkifyz00,
		BgL_bgl_za762shrinkza7d2node1816z00,
		BGl_z62shrinkzd2nodez12zd2var1257z70zzast_shrinkifyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1780z00zzast_shrinkifyz00,
		BgL_bgl_za762shrinkza7d2node1817z00,
		BGl_z62shrinkzd2nodez12zd2kwote1259z70zzast_shrinkifyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1781z00zzast_shrinkifyz00,
		BgL_bgl_za762shrinkza7d2node1818z00,
		BGl_z62shrinkzd2nodez12zd2sequenc1261z70zzast_shrinkifyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1782z00zzast_shrinkifyz00,
		BgL_bgl_za762shrinkza7d2node1819z00,
		BGl_z62shrinkzd2nodez12zd2sync1263z70zzast_shrinkifyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1783z00zzast_shrinkifyz00,
		BgL_bgl_za762shrinkza7d2node1820z00,
		BGl_z62shrinkzd2nodez12zd2app1265z70zzast_shrinkifyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1784z00zzast_shrinkifyz00,
		BgL_bgl_za762shrinkza7d2node1821z00,
		BGl_z62shrinkzd2nodez12zd2appzd2ly1267za2zzast_shrinkifyz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1785z00zzast_shrinkifyz00,
		BgL_bgl_za762shrinkza7d2node1822z00,
		BGl_z62shrinkzd2nodez12zd2funcall1269z70zzast_shrinkifyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1786z00zzast_shrinkifyz00,
		BgL_bgl_za762shrinkza7d2node1823z00,
		BGl_z62shrinkzd2nodez12zd2extern1271z70zzast_shrinkifyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1787z00zzast_shrinkifyz00,
		BgL_bgl_za762shrinkza7d2node1824z00,
		BGl_z62shrinkzd2nodez12zd2cast1273z70zzast_shrinkifyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1788z00zzast_shrinkifyz00,
		BgL_bgl_za762shrinkza7d2node1825z00,
		BGl_z62shrinkzd2nodez12zd2setq1275z70zzast_shrinkifyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1789z00zzast_shrinkifyz00,
		BgL_bgl_za762shrinkza7d2node1826z00,
		BGl_z62shrinkzd2nodez12zd2conditi1277z70zzast_shrinkifyz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1799z00zzast_shrinkifyz00,
		BgL_bgl_string1799za700za7za7a1827za7, "ast_shrinkify", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1790z00zzast_shrinkifyz00,
		BgL_bgl_za762shrinkza7d2node1828z00,
		BGl_z62shrinkzd2nodez12zd2fail1279z70zzast_shrinkifyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1791z00zzast_shrinkifyz00,
		BgL_bgl_za762shrinkza7d2node1829z00,
		BGl_z62shrinkzd2nodez12zd2switch1281z70zzast_shrinkifyz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1792z00zzast_shrinkifyz00,
		BgL_bgl_za762shrinkza7d2node1830z00,
		BGl_z62shrinkzd2nodez12zd2makezd2bo1283za2zzast_shrinkifyz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1793z00zzast_shrinkifyz00,
		BgL_bgl_za762shrinkza7d2node1831z00,
		BGl_z62shrinkzd2nodez12zd2boxzd2ref1285za2zzast_shrinkifyz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1794z00zzast_shrinkifyz00,
		BgL_bgl_za762shrinkza7d2node1832z00,
		BGl_z62shrinkzd2nodez12zd2boxzd2set1287za2zzast_shrinkifyz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1795z00zzast_shrinkifyz00,
		BgL_bgl_za762shrinkza7d2node1833z00,
		BGl_z62shrinkzd2nodez12zd2letzd2fun1289za2zzast_shrinkifyz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1796z00zzast_shrinkifyz00,
		BgL_bgl_za762shrinkza7d2node1834z00,
		BGl_z62shrinkzd2nodez12zd2letzd2var1291za2zzast_shrinkifyz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1797z00zzast_shrinkifyz00,
		BgL_bgl_za762shrinkza7d2node1835z00,
		BGl_z62shrinkzd2nodez12zd2setzd2exzd21293z70zzast_shrinkifyz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1798z00zzast_shrinkifyz00,
		BgL_bgl_za762shrinkza7d2node1836z00,
		BGl_z62shrinkzd2nodez12zd2jumpzd2ex1295za2zzast_shrinkifyz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_GENERIC(BGl_shrinkzd2nodez12zd2envz12zzast_shrinkifyz00,
		BgL_bgl_za762shrinkza7d2node1837z00,
		BGl_z62shrinkzd2nodez12za2zzast_shrinkifyz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzast_shrinkifyz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzast_shrinkifyz00(long
		BgL_checksumz00_2226, char *BgL_fromz00_2227)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_shrinkifyz00))
				{
					BGl_requirezd2initializa7ationz75zzast_shrinkifyz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_shrinkifyz00();
					BGl_libraryzd2moduleszd2initz00zzast_shrinkifyz00();
					BGl_cnstzd2initzd2zzast_shrinkifyz00();
					BGl_importedzd2moduleszd2initz00zzast_shrinkifyz00();
					BGl_genericzd2initzd2zzast_shrinkifyz00();
					BGl_methodzd2initzd2zzast_shrinkifyz00();
					return BGl_toplevelzd2initzd2zzast_shrinkifyz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_shrinkifyz00(void)
	{
		{	/* Ast/shrinkify.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "ast_shrinkify");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_shrinkify");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"ast_shrinkify");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "ast_shrinkify");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"ast_shrinkify");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "ast_shrinkify");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"ast_shrinkify");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"ast_shrinkify");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_shrinkify");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_shrinkifyz00(void)
	{
		{	/* Ast/shrinkify.scm 15 */
			{	/* Ast/shrinkify.scm 15 */
				obj_t BgL_cportz00_2112;

				{	/* Ast/shrinkify.scm 15 */
					obj_t BgL_stringz00_2119;

					BgL_stringz00_2119 = BGl_string1800z00zzast_shrinkifyz00;
					{	/* Ast/shrinkify.scm 15 */
						obj_t BgL_startz00_2120;

						BgL_startz00_2120 = BINT(0L);
						{	/* Ast/shrinkify.scm 15 */
							obj_t BgL_endz00_2121;

							BgL_endz00_2121 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2119)));
							{	/* Ast/shrinkify.scm 15 */

								BgL_cportz00_2112 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2119, BgL_startz00_2120, BgL_endz00_2121);
				}}}}
				{
					long BgL_iz00_2113;

					BgL_iz00_2113 = 0L;
				BgL_loopz00_2114:
					if ((BgL_iz00_2113 == -1L))
						{	/* Ast/shrinkify.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Ast/shrinkify.scm 15 */
							{	/* Ast/shrinkify.scm 15 */
								obj_t BgL_arg1805z00_2115;

								{	/* Ast/shrinkify.scm 15 */

									{	/* Ast/shrinkify.scm 15 */
										obj_t BgL_locationz00_2117;

										BgL_locationz00_2117 = BBOOL(((bool_t) 0));
										{	/* Ast/shrinkify.scm 15 */

											BgL_arg1805z00_2115 =
												BGl_readz00zz__readerz00(BgL_cportz00_2112,
												BgL_locationz00_2117);
										}
									}
								}
								{	/* Ast/shrinkify.scm 15 */
									int BgL_tmpz00_2256;

									BgL_tmpz00_2256 = (int) (BgL_iz00_2113);
									CNST_TABLE_SET(BgL_tmpz00_2256, BgL_arg1805z00_2115);
							}}
							{	/* Ast/shrinkify.scm 15 */
								int BgL_auxz00_2118;

								BgL_auxz00_2118 = (int) ((BgL_iz00_2113 - 1L));
								{
									long BgL_iz00_2261;

									BgL_iz00_2261 = (long) (BgL_auxz00_2118);
									BgL_iz00_2113 = BgL_iz00_2261;
									goto BgL_loopz00_2114;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_shrinkifyz00(void)
	{
		{	/* Ast/shrinkify.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzast_shrinkifyz00(void)
	{
		{	/* Ast/shrinkify.scm 15 */
			return BUNSPEC;
		}

	}



/* shrinkify! */
	BGL_EXPORTED_DEF obj_t BGl_shrinkifyz12z12zzast_shrinkifyz00(obj_t
		BgL_globalsz00_3)
	{
		{	/* Ast/shrinkify.scm 26 */
			BGl_forzd2eachzd2globalz12z12zzast_envz00
				(BGl_shrinkzd2variablez12zd2envz12zzast_shrinkifyz00, BNIL);
			{
				obj_t BgL_l1234z00_1379;

				BgL_l1234z00_1379 = BgL_globalsz00_3;
			BgL_zc3z04anonymousza31307ze3z87_1380:
				if (PAIRP(BgL_l1234z00_1379))
					{	/* Ast/shrinkify.scm 28 */
						{	/* Ast/shrinkify.scm 29 */
							obj_t BgL_globalz00_1382;

							BgL_globalz00_1382 = CAR(BgL_l1234z00_1379);
							{	/* Ast/shrinkify.scm 29 */
								BgL_valuez00_bglt BgL_sfunz00_1383;

								BgL_sfunz00_1383 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_globalz00_1382))))->
									BgL_valuez00);
								{	/* Ast/shrinkify.scm 30 */
									obj_t BgL_g1233z00_1384;

									BgL_g1233z00_1384 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt) BgL_sfunz00_1383)))->BgL_argsz00);
									{
										obj_t BgL_l1231z00_1386;

										BgL_l1231z00_1386 = BgL_g1233z00_1384;
									BgL_zc3z04anonymousza31309ze3z87_1387:
										if (PAIRP(BgL_l1231z00_1386))
											{	/* Ast/shrinkify.scm 30 */
												{	/* Ast/shrinkify.scm 30 */
													obj_t BgL_arg1312z00_1389;

													BgL_arg1312z00_1389 = CAR(BgL_l1231z00_1386);
													BGl_shrinkzd2variablez12zc0zzast_shrinkifyz00(
														((BgL_variablez00_bglt) BgL_arg1312z00_1389));
												}
												{
													obj_t BgL_l1231z00_2278;

													BgL_l1231z00_2278 = CDR(BgL_l1231z00_1386);
													BgL_l1231z00_1386 = BgL_l1231z00_2278;
													goto BgL_zc3z04anonymousza31309ze3z87_1387;
												}
											}
										else
											{	/* Ast/shrinkify.scm 30 */
												((bool_t) 1);
											}
									}
								}
								{	/* Ast/shrinkify.scm 31 */
									obj_t BgL_arg1315z00_1392;

									BgL_arg1315z00_1392 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt)
													(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_globalz00_bglt) BgL_globalz00_1382))))->
														BgL_valuez00))))->BgL_bodyz00);
									BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(((BgL_nodez00_bglt)
											BgL_arg1315z00_1392));
								}
							}
						}
						{
							obj_t BgL_l1234z00_2287;

							BgL_l1234z00_2287 = CDR(BgL_l1234z00_1379);
							BgL_l1234z00_1379 = BgL_l1234z00_2287;
							goto BgL_zc3z04anonymousza31307ze3z87_1380;
						}
					}
				else
					{	/* Ast/shrinkify.scm 28 */
						((bool_t) 1);
					}
			}
			return BgL_globalsz00_3;
		}

	}



/* &shrinkify! */
	obj_t BGl_z62shrinkifyz12z70zzast_shrinkifyz00(obj_t BgL_envz00_2036,
		obj_t BgL_globalsz00_2037)
	{
		{	/* Ast/shrinkify.scm 26 */
			return BGl_shrinkifyz12z12zzast_shrinkifyz00(BgL_globalsz00_2037);
		}

	}



/* shrink-node*! */
	bool_t BGl_shrinkzd2nodeza2z12z62zzast_shrinkifyz00(obj_t BgL_nodeza2za2_27)
	{
		{	/* Ast/shrinkify.scm 232 */
			{
				obj_t BgL_l1248z00_1397;

				BgL_l1248z00_1397 = BgL_nodeza2za2_27;
			BgL_zc3z04anonymousza31318ze3z87_1398:
				if (PAIRP(BgL_l1248z00_1397))
					{	/* Ast/shrinkify.scm 233 */
						{	/* Ast/shrinkify.scm 233 */
							obj_t BgL_arg1320z00_1400;

							BgL_arg1320z00_1400 = CAR(BgL_l1248z00_1397);
							BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
								((BgL_nodez00_bglt) BgL_arg1320z00_1400));
						}
						{
							obj_t BgL_l1248z00_2295;

							BgL_l1248z00_2295 = CDR(BgL_l1248z00_1397);
							BgL_l1248z00_1397 = BgL_l1248z00_2295;
							goto BgL_zc3z04anonymousza31318ze3z87_1398;
						}
					}
				else
					{	/* Ast/shrinkify.scm 233 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_shrinkifyz00(void)
	{
		{	/* Ast/shrinkify.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_shrinkifyz00(void)
	{
		{	/* Ast/shrinkify.scm 15 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_shrinkzd2variablez12zd2envz12zzast_shrinkifyz00,
				BGl_proc1772z00zzast_shrinkifyz00, BGl_variablez00zzast_varz00,
				BGl_string1773z00zzast_shrinkifyz00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_shrinkzd2nodez12zd2envz12zzast_shrinkifyz00,
				BGl_proc1774z00zzast_shrinkifyz00, BGl_nodez00zzast_nodez00,
				BGl_string1775z00zzast_shrinkifyz00);
		}

	}



/* &shrink-node!1252 */
	obj_t BGl_z62shrinkzd2nodez121252za2zzast_shrinkifyz00(obj_t BgL_envz00_2042,
		obj_t BgL_nodez00_2043)
	{
		{	/* Ast/shrinkify.scm 47 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(0),
				BGl_string1776z00zzast_shrinkifyz00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_2043)));
		}

	}



/* &shrink-variable!1250 */
	obj_t BGl_z62shrinkzd2variablez121250za2zzast_shrinkifyz00(obj_t
		BgL_envz00_2044, obj_t BgL_variablez00_2045)
	{
		{	/* Ast/shrinkify.scm 38 */
			{	/* Ast/shrinkify.scm 39 */
				bool_t BgL_test1843z00_2303;

				{	/* Ast/shrinkify.scm 39 */
					obj_t BgL_tmpz00_2304;

					{	/* Ast/shrinkify.scm 39 */
						BgL_objectz00_bglt BgL_tmpz00_2305;

						BgL_tmpz00_2305 =
							((BgL_objectz00_bglt)
							((BgL_variablez00_bglt) BgL_variablez00_2045));
						BgL_tmpz00_2304 = BGL_OBJECT_WIDENING(BgL_tmpz00_2305);
					}
					BgL_test1843z00_2303 = CBOOL(BgL_tmpz00_2304);
				}
				if (BgL_test1843z00_2303)
					{	/* Ast/shrinkify.scm 39 */
						{	/* Ast/shrinkify.scm 40 */
							long BgL_arg1325z00_2125;

							{	/* Ast/shrinkify.scm 40 */
								obj_t BgL_arg1326z00_2126;

								{	/* Ast/shrinkify.scm 40 */
									obj_t BgL_arg1327z00_2127;

									{	/* Ast/shrinkify.scm 40 */
										obj_t BgL_arg1815z00_2128;
										long BgL_arg1816z00_2129;

										BgL_arg1815z00_2128 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Ast/shrinkify.scm 40 */
											long BgL_arg1817z00_2130;

											BgL_arg1817z00_2130 =
												BGL_OBJECT_CLASS_NUM(
												((BgL_objectz00_bglt)
													((BgL_variablez00_bglt) BgL_variablez00_2045)));
											BgL_arg1816z00_2129 = (BgL_arg1817z00_2130 - OBJECT_TYPE);
										}
										BgL_arg1327z00_2127 =
											VECTOR_REF(BgL_arg1815z00_2128, BgL_arg1816z00_2129);
									}
									BgL_arg1326z00_2126 =
										BGl_classzd2superzd2zz__objectz00(BgL_arg1327z00_2127);
								}
								{	/* Ast/shrinkify.scm 40 */
									obj_t BgL_tmpz00_2317;

									BgL_tmpz00_2317 = ((obj_t) BgL_arg1326z00_2126);
									BgL_arg1325z00_2125 = BGL_CLASS_NUM(BgL_tmpz00_2317);
							}}
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_variablez00_bglt) BgL_variablez00_2045)),
								BgL_arg1325z00_2125);
						}
						{	/* Ast/shrinkify.scm 40 */
							BgL_objectz00_bglt BgL_tmpz00_2323;

							BgL_tmpz00_2323 =
								((BgL_objectz00_bglt)
								((BgL_variablez00_bglt) BgL_variablez00_2045));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2323, BFALSE);
						}
						((BgL_objectz00_bglt)
							((BgL_variablez00_bglt) BgL_variablez00_2045));
						((obj_t) ((BgL_variablez00_bglt) BgL_variablez00_2045));
					}
				else
					{	/* Ast/shrinkify.scm 39 */
						BFALSE;
					}
			}
			{	/* Ast/shrinkify.scm 41 */
				bool_t BgL_test1844z00_2331;

				{	/* Ast/shrinkify.scm 41 */
					obj_t BgL_tmpz00_2332;

					{	/* Ast/shrinkify.scm 41 */
						BgL_objectz00_bglt BgL_tmpz00_2333;

						BgL_tmpz00_2333 =
							((BgL_objectz00_bglt)
							(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_variablez00_2045)))->
								BgL_valuez00));
						BgL_tmpz00_2332 = BGL_OBJECT_WIDENING(BgL_tmpz00_2333);
					}
					BgL_test1844z00_2331 = CBOOL(BgL_tmpz00_2332);
				}
				if (BgL_test1844z00_2331)
					{	/* Ast/shrinkify.scm 42 */
						BgL_valuez00_bglt BgL_o1103z00_2131;

						BgL_o1103z00_2131 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_variablez00_2045)))->
							BgL_valuez00);
						{	/* Ast/shrinkify.scm 42 */
							long BgL_arg1331z00_2132;

							{	/* Ast/shrinkify.scm 42 */
								obj_t BgL_arg1332z00_2133;

								{	/* Ast/shrinkify.scm 42 */
									obj_t BgL_arg1333z00_2134;

									{	/* Ast/shrinkify.scm 42 */
										obj_t BgL_arg1815z00_2135;
										long BgL_arg1816z00_2136;

										BgL_arg1815z00_2135 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Ast/shrinkify.scm 42 */
											long BgL_arg1817z00_2137;

											BgL_arg1817z00_2137 =
												BGL_OBJECT_CLASS_NUM(
												((BgL_objectz00_bglt) BgL_o1103z00_2131));
											BgL_arg1816z00_2136 = (BgL_arg1817z00_2137 - OBJECT_TYPE);
										}
										BgL_arg1333z00_2134 =
											VECTOR_REF(BgL_arg1815z00_2135, BgL_arg1816z00_2136);
									}
									BgL_arg1332z00_2133 =
										BGl_classzd2superzd2zz__objectz00(BgL_arg1333z00_2134);
								}
								{	/* Ast/shrinkify.scm 42 */
									obj_t BgL_tmpz00_2347;

									BgL_tmpz00_2347 = ((obj_t) BgL_arg1332z00_2133);
									BgL_arg1331z00_2132 = BGL_CLASS_NUM(BgL_tmpz00_2347);
							}}
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_o1103z00_2131), BgL_arg1331z00_2132);
						}
						{	/* Ast/shrinkify.scm 42 */
							BgL_objectz00_bglt BgL_tmpz00_2352;

							BgL_tmpz00_2352 = ((BgL_objectz00_bglt) BgL_o1103z00_2131);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2352, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_o1103z00_2131);
						return ((obj_t) BgL_o1103z00_2131);
					}
				else
					{	/* Ast/shrinkify.scm 41 */
						return BFALSE;
					}
			}
		}

	}



/* shrink-variable! */
	obj_t BGl_shrinkzd2variablez12zc0zzast_shrinkifyz00(BgL_variablez00_bglt
		BgL_variablez00_4)
	{
		{	/* Ast/shrinkify.scm 38 */
			{	/* Ast/shrinkify.scm 38 */
				obj_t BgL_method1251z00_1423;

				{	/* Ast/shrinkify.scm 38 */
					obj_t BgL_res1766z00_1848;

					{	/* Ast/shrinkify.scm 38 */
						long BgL_objzd2classzd2numz00_1819;

						BgL_objzd2classzd2numz00_1819 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_variablez00_4));
						{	/* Ast/shrinkify.scm 38 */
							obj_t BgL_arg1811z00_1820;

							BgL_arg1811z00_1820 =
								PROCEDURE_REF
								(BGl_shrinkzd2variablez12zd2envz12zzast_shrinkifyz00,
								(int) (1L));
							{	/* Ast/shrinkify.scm 38 */
								int BgL_offsetz00_1823;

								BgL_offsetz00_1823 = (int) (BgL_objzd2classzd2numz00_1819);
								{	/* Ast/shrinkify.scm 38 */
									long BgL_offsetz00_1824;

									BgL_offsetz00_1824 =
										((long) (BgL_offsetz00_1823) - OBJECT_TYPE);
									{	/* Ast/shrinkify.scm 38 */
										long BgL_modz00_1825;

										BgL_modz00_1825 =
											(BgL_offsetz00_1824 >> (int) ((long) ((int) (4L))));
										{	/* Ast/shrinkify.scm 38 */
											long BgL_restz00_1827;

											BgL_restz00_1827 =
												(BgL_offsetz00_1824 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/shrinkify.scm 38 */

												{	/* Ast/shrinkify.scm 38 */
													obj_t BgL_bucketz00_1829;

													BgL_bucketz00_1829 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_1820), BgL_modz00_1825);
													BgL_res1766z00_1848 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_1829), BgL_restz00_1827);
					}}}}}}}}
					BgL_method1251z00_1423 = BgL_res1766z00_1848;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1251z00_1423,
					((obj_t) BgL_variablez00_4));
			}
		}

	}



/* &shrink-variable! */
	obj_t BGl_z62shrinkzd2variablez12za2zzast_shrinkifyz00(obj_t BgL_envz00_2038,
		obj_t BgL_variablez00_2039)
	{
		{	/* Ast/shrinkify.scm 38 */
			return
				BGl_shrinkzd2variablez12zc0zzast_shrinkifyz00(
				((BgL_variablez00_bglt) BgL_variablez00_2039));
		}

	}



/* shrink-node! */
	BGL_EXPORTED_DEF obj_t
		BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(BgL_nodez00_bglt BgL_nodez00_5)
	{
		{	/* Ast/shrinkify.scm 47 */
			{	/* Ast/shrinkify.scm 47 */
				obj_t BgL_method1253z00_1424;

				{	/* Ast/shrinkify.scm 47 */
					obj_t BgL_res1771z00_1879;

					{	/* Ast/shrinkify.scm 47 */
						long BgL_objzd2classzd2numz00_1850;

						BgL_objzd2classzd2numz00_1850 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_5));
						{	/* Ast/shrinkify.scm 47 */
							obj_t BgL_arg1811z00_1851;

							BgL_arg1811z00_1851 =
								PROCEDURE_REF(BGl_shrinkzd2nodez12zd2envz12zzast_shrinkifyz00,
								(int) (1L));
							{	/* Ast/shrinkify.scm 47 */
								int BgL_offsetz00_1854;

								BgL_offsetz00_1854 = (int) (BgL_objzd2classzd2numz00_1850);
								{	/* Ast/shrinkify.scm 47 */
									long BgL_offsetz00_1855;

									BgL_offsetz00_1855 =
										((long) (BgL_offsetz00_1854) - OBJECT_TYPE);
									{	/* Ast/shrinkify.scm 47 */
										long BgL_modz00_1856;

										BgL_modz00_1856 =
											(BgL_offsetz00_1855 >> (int) ((long) ((int) (4L))));
										{	/* Ast/shrinkify.scm 47 */
											long BgL_restz00_1858;

											BgL_restz00_1858 =
												(BgL_offsetz00_1855 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/shrinkify.scm 47 */

												{	/* Ast/shrinkify.scm 47 */
													obj_t BgL_bucketz00_1860;

													BgL_bucketz00_1860 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_1851), BgL_modz00_1856);
													BgL_res1771z00_1879 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_1860), BgL_restz00_1858);
					}}}}}}}}
					BgL_method1253z00_1424 = BgL_res1771z00_1879;
				}
				{	/* Ast/shrinkify.scm 47 */
					obj_t BgL_xz00_2111;

					BgL_xz00_2111 =
						BGL_PROCEDURE_CALL1(BgL_method1253z00_1424,
						((obj_t) BgL_nodez00_5));
					return BUNSPEC;
				}
			}
		}

	}



/* &shrink-node! */
	obj_t BGl_z62shrinkzd2nodez12za2zzast_shrinkifyz00(obj_t BgL_envz00_2046,
		obj_t BgL_nodez00_2047)
	{
		{	/* Ast/shrinkify.scm 47 */
			return
				BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
				((BgL_nodez00_bglt) BgL_nodez00_2047));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_shrinkifyz00(void)
	{
		{	/* Ast/shrinkify.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shrinkzd2nodez12zd2envz12zzast_shrinkifyz00,
				BGl_atomz00zzast_nodez00, BGl_proc1777z00zzast_shrinkifyz00,
				BGl_string1778z00zzast_shrinkifyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shrinkzd2nodez12zd2envz12zzast_shrinkifyz00,
				BGl_varz00zzast_nodez00, BGl_proc1779z00zzast_shrinkifyz00,
				BGl_string1778z00zzast_shrinkifyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shrinkzd2nodez12zd2envz12zzast_shrinkifyz00,
				BGl_kwotez00zzast_nodez00, BGl_proc1780z00zzast_shrinkifyz00,
				BGl_string1778z00zzast_shrinkifyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shrinkzd2nodez12zd2envz12zzast_shrinkifyz00,
				BGl_sequencez00zzast_nodez00, BGl_proc1781z00zzast_shrinkifyz00,
				BGl_string1778z00zzast_shrinkifyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shrinkzd2nodez12zd2envz12zzast_shrinkifyz00,
				BGl_syncz00zzast_nodez00, BGl_proc1782z00zzast_shrinkifyz00,
				BGl_string1778z00zzast_shrinkifyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shrinkzd2nodez12zd2envz12zzast_shrinkifyz00,
				BGl_appz00zzast_nodez00, BGl_proc1783z00zzast_shrinkifyz00,
				BGl_string1778z00zzast_shrinkifyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shrinkzd2nodez12zd2envz12zzast_shrinkifyz00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1784z00zzast_shrinkifyz00,
				BGl_string1778z00zzast_shrinkifyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shrinkzd2nodez12zd2envz12zzast_shrinkifyz00,
				BGl_funcallz00zzast_nodez00, BGl_proc1785z00zzast_shrinkifyz00,
				BGl_string1778z00zzast_shrinkifyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shrinkzd2nodez12zd2envz12zzast_shrinkifyz00,
				BGl_externz00zzast_nodez00, BGl_proc1786z00zzast_shrinkifyz00,
				BGl_string1778z00zzast_shrinkifyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shrinkzd2nodez12zd2envz12zzast_shrinkifyz00,
				BGl_castz00zzast_nodez00, BGl_proc1787z00zzast_shrinkifyz00,
				BGl_string1778z00zzast_shrinkifyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shrinkzd2nodez12zd2envz12zzast_shrinkifyz00,
				BGl_setqz00zzast_nodez00, BGl_proc1788z00zzast_shrinkifyz00,
				BGl_string1778z00zzast_shrinkifyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shrinkzd2nodez12zd2envz12zzast_shrinkifyz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1789z00zzast_shrinkifyz00,
				BGl_string1778z00zzast_shrinkifyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shrinkzd2nodez12zd2envz12zzast_shrinkifyz00,
				BGl_failz00zzast_nodez00, BGl_proc1790z00zzast_shrinkifyz00,
				BGl_string1778z00zzast_shrinkifyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shrinkzd2nodez12zd2envz12zzast_shrinkifyz00,
				BGl_switchz00zzast_nodez00, BGl_proc1791z00zzast_shrinkifyz00,
				BGl_string1778z00zzast_shrinkifyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shrinkzd2nodez12zd2envz12zzast_shrinkifyz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1792z00zzast_shrinkifyz00,
				BGl_string1778z00zzast_shrinkifyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shrinkzd2nodez12zd2envz12zzast_shrinkifyz00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1793z00zzast_shrinkifyz00,
				BGl_string1778z00zzast_shrinkifyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shrinkzd2nodez12zd2envz12zzast_shrinkifyz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1794z00zzast_shrinkifyz00,
				BGl_string1778z00zzast_shrinkifyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shrinkzd2nodez12zd2envz12zzast_shrinkifyz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1795z00zzast_shrinkifyz00,
				BGl_string1778z00zzast_shrinkifyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shrinkzd2nodez12zd2envz12zzast_shrinkifyz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1796z00zzast_shrinkifyz00,
				BGl_string1778z00zzast_shrinkifyz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shrinkzd2nodez12zd2envz12zzast_shrinkifyz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc1797z00zzast_shrinkifyz00,
				BGl_string1778z00zzast_shrinkifyz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shrinkzd2nodez12zd2envz12zzast_shrinkifyz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc1798z00zzast_shrinkifyz00,
				BGl_string1778z00zzast_shrinkifyz00);
		}

	}



/* &shrink-node!-jump-ex1295 */
	obj_t BGl_z62shrinkzd2nodez12zd2jumpzd2ex1295za2zzast_shrinkifyz00(obj_t
		BgL_envz00_2069, obj_t BgL_nodez00_2070)
	{
		{	/* Ast/shrinkify.scm 224 */
			BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2070)))->BgL_exitz00));
			BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2070)))->BgL_valuez00));
			return BUNSPEC;
		}

	}



/* &shrink-node!-set-ex-1293 */
	obj_t BGl_z62shrinkzd2nodez12zd2setzd2exzd21293z70zzast_shrinkifyz00(obj_t
		BgL_envz00_2071, obj_t BgL_nodez00_2072)
	{
		{	/* Ast/shrinkify.scm 214 */
			{	/* Ast/shrinkify.scm 216 */
				BgL_varz00_bglt BgL_arg1611z00_2140;

				BgL_arg1611z00_2140 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2072)))->BgL_varz00);
				BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
					((BgL_nodez00_bglt) BgL_arg1611z00_2140));
			}
			BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2072)))->BgL_bodyz00));
			BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2072)))->BgL_onexitz00));
			return BUNSPEC;
		}

	}



/* &shrink-node!-let-var1291 */
	obj_t BGl_z62shrinkzd2nodez12zd2letzd2var1291za2zzast_shrinkifyz00(obj_t
		BgL_envz00_2073, obj_t BgL_nodez00_2074)
	{
		{	/* Ast/shrinkify.scm 202 */
			BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
				(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2074)))->BgL_bodyz00));
			{	/* Ast/shrinkify.scm 205 */
				obj_t BgL_g1247z00_2142;

				BgL_g1247z00_2142 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2074)))->BgL_bindingsz00);
				{
					obj_t BgL_l1245z00_2144;

					BgL_l1245z00_2144 = BgL_g1247z00_2142;
				BgL_zc3z04anonymousza31603ze3z87_2143:
					if (PAIRP(BgL_l1245z00_2144))
						{	/* Ast/shrinkify.scm 205 */
							{	/* Ast/shrinkify.scm 206 */
								obj_t BgL_bindingz00_2145;

								BgL_bindingz00_2145 = CAR(BgL_l1245z00_2144);
								{	/* Ast/shrinkify.scm 206 */
									obj_t BgL_arg1605z00_2146;

									BgL_arg1605z00_2146 = CAR(((obj_t) BgL_bindingz00_2145));
									BGl_shrinkzd2variablez12zc0zzast_shrinkifyz00(
										((BgL_variablez00_bglt) BgL_arg1605z00_2146));
								}
								{	/* Ast/shrinkify.scm 207 */
									obj_t BgL_arg1606z00_2147;

									BgL_arg1606z00_2147 = CDR(((obj_t) BgL_bindingz00_2145));
									BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
										((BgL_nodez00_bglt) BgL_arg1606z00_2147));
								}
							}
							{
								obj_t BgL_l1245z00_2474;

								BgL_l1245z00_2474 = CDR(BgL_l1245z00_2144);
								BgL_l1245z00_2144 = BgL_l1245z00_2474;
								goto BgL_zc3z04anonymousza31603ze3z87_2143;
							}
						}
					else
						{	/* Ast/shrinkify.scm 205 */
							((bool_t) 1);
						}
				}
			}
			return BUNSPEC;
		}

	}



/* &shrink-node!-let-fun1289 */
	obj_t BGl_z62shrinkzd2nodez12zd2letzd2fun1289za2zzast_shrinkifyz00(obj_t
		BgL_envz00_2075, obj_t BgL_nodez00_2076)
	{
		{	/* Ast/shrinkify.scm 188 */
			BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
				(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2076)))->BgL_bodyz00));
			{	/* Ast/shrinkify.scm 191 */
				obj_t BgL_g1244z00_2149;

				BgL_g1244z00_2149 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2076)))->BgL_localsz00);
				{
					obj_t BgL_l1242z00_2151;

					BgL_l1242z00_2151 = BgL_g1244z00_2149;
				BgL_zc3z04anonymousza31586ze3z87_2150:
					if (PAIRP(BgL_l1242z00_2151))
						{	/* Ast/shrinkify.scm 191 */
							{	/* Ast/shrinkify.scm 192 */
								obj_t BgL_localz00_2152;

								BgL_localz00_2152 = CAR(BgL_l1242z00_2151);
								{	/* Ast/shrinkify.scm 192 */
									BgL_valuez00_bglt BgL_sfunz00_2153;

									BgL_sfunz00_2153 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_localz00_2152))))->
										BgL_valuez00);
									BGl_shrinkzd2variablez12zc0zzast_shrinkifyz00((
											(BgL_variablez00_bglt) BgL_localz00_2152));
									{	/* Ast/shrinkify.scm 194 */
										obj_t BgL_g1241z00_2154;

										BgL_g1241z00_2154 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_sfunz00_2153)))->BgL_argsz00);
										{
											obj_t BgL_l1239z00_2156;

											BgL_l1239z00_2156 = BgL_g1241z00_2154;
										BgL_zc3z04anonymousza31588ze3z87_2155:
											if (PAIRP(BgL_l1239z00_2156))
												{	/* Ast/shrinkify.scm 194 */
													{	/* Ast/shrinkify.scm 194 */
														obj_t BgL_arg1591z00_2157;

														BgL_arg1591z00_2157 = CAR(BgL_l1239z00_2156);
														BGl_shrinkzd2variablez12zc0zzast_shrinkifyz00(
															((BgL_variablez00_bglt) BgL_arg1591z00_2157));
													}
													{
														obj_t BgL_l1239z00_2496;

														BgL_l1239z00_2496 = CDR(BgL_l1239z00_2156);
														BgL_l1239z00_2156 = BgL_l1239z00_2496;
														goto BgL_zc3z04anonymousza31588ze3z87_2155;
													}
												}
											else
												{	/* Ast/shrinkify.scm 194 */
													((bool_t) 1);
												}
										}
									}
									{	/* Ast/shrinkify.scm 195 */
										obj_t BgL_arg1594z00_2158;

										BgL_arg1594z00_2158 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_sfunz00_2153)))->BgL_bodyz00);
										BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
											((BgL_nodez00_bglt) BgL_arg1594z00_2158));
									}
								}
							}
							{
								obj_t BgL_l1242z00_2502;

								BgL_l1242z00_2502 = CDR(BgL_l1242z00_2151);
								BgL_l1242z00_2151 = BgL_l1242z00_2502;
								goto BgL_zc3z04anonymousza31586ze3z87_2150;
							}
						}
					else
						{	/* Ast/shrinkify.scm 191 */
							((bool_t) 1);
						}
				}
			}
			return BUNSPEC;
		}

	}



/* &shrink-node!-box-set1287 */
	obj_t BGl_z62shrinkzd2nodez12zd2boxzd2set1287za2zzast_shrinkifyz00(obj_t
		BgL_envz00_2077, obj_t BgL_nodez00_2078)
	{
		{	/* Ast/shrinkify.scm 179 */
			{	/* Ast/shrinkify.scm 180 */
				bool_t BgL_test1848z00_2504;

				{	/* Ast/shrinkify.scm 180 */
					obj_t BgL_tmpz00_2505;

					{	/* Ast/shrinkify.scm 180 */
						BgL_objectz00_bglt BgL_tmpz00_2506;

						BgL_tmpz00_2506 =
							((BgL_objectz00_bglt)
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2078));
						BgL_tmpz00_2505 = BGL_OBJECT_WIDENING(BgL_tmpz00_2506);
					}
					BgL_test1848z00_2504 = CBOOL(BgL_tmpz00_2505);
				}
				if (BgL_test1848z00_2504)
					{	/* Ast/shrinkify.scm 180 */
						{	/* Ast/shrinkify.scm 180 */
							long BgL_arg1571z00_2160;

							{	/* Ast/shrinkify.scm 180 */
								obj_t BgL_arg1573z00_2161;

								{	/* Ast/shrinkify.scm 180 */
									obj_t BgL_arg1575z00_2162;

									{	/* Ast/shrinkify.scm 180 */
										obj_t BgL_arg1815z00_2163;
										long BgL_arg1816z00_2164;

										BgL_arg1815z00_2163 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Ast/shrinkify.scm 180 */
											long BgL_arg1817z00_2165;

											BgL_arg1817z00_2165 =
												BGL_OBJECT_CLASS_NUM(
												((BgL_objectz00_bglt)
													((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2078)));
											BgL_arg1816z00_2164 = (BgL_arg1817z00_2165 - OBJECT_TYPE);
										}
										BgL_arg1575z00_2162 =
											VECTOR_REF(BgL_arg1815z00_2163, BgL_arg1816z00_2164);
									}
									BgL_arg1573z00_2161 =
										BGl_classzd2superzd2zz__objectz00(BgL_arg1575z00_2162);
								}
								{	/* Ast/shrinkify.scm 180 */
									obj_t BgL_tmpz00_2518;

									BgL_tmpz00_2518 = ((obj_t) BgL_arg1573z00_2161);
									BgL_arg1571z00_2160 = BGL_CLASS_NUM(BgL_tmpz00_2518);
							}}
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2078)),
								BgL_arg1571z00_2160);
						}
						{	/* Ast/shrinkify.scm 180 */
							BgL_objectz00_bglt BgL_tmpz00_2524;

							BgL_tmpz00_2524 =
								((BgL_objectz00_bglt)
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2078));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2524, BFALSE);
						}
						((BgL_objectz00_bglt)
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2078));
						((obj_t) ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2078));
					}
				else
					{	/* Ast/shrinkify.scm 180 */
						BFALSE;
					}
			}
			{	/* Ast/shrinkify.scm 181 */
				BgL_varz00_bglt BgL_arg1576z00_2166;

				BgL_arg1576z00_2166 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2078)))->BgL_varz00);
				BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
					((BgL_nodez00_bglt) BgL_arg1576z00_2166));
			}
			BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
				(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2078)))->BgL_valuez00));
			return BUNSPEC;
		}

	}



/* &shrink-node!-box-ref1285 */
	obj_t BGl_z62shrinkzd2nodez12zd2boxzd2ref1285za2zzast_shrinkifyz00(obj_t
		BgL_envz00_2079, obj_t BgL_nodez00_2080)
	{
		{	/* Ast/shrinkify.scm 171 */
			{	/* Ast/shrinkify.scm 172 */
				bool_t BgL_test1849z00_2539;

				{	/* Ast/shrinkify.scm 172 */
					obj_t BgL_tmpz00_2540;

					{	/* Ast/shrinkify.scm 172 */
						BgL_objectz00_bglt BgL_tmpz00_2541;

						BgL_tmpz00_2541 =
							((BgL_objectz00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_2080));
						BgL_tmpz00_2540 = BGL_OBJECT_WIDENING(BgL_tmpz00_2541);
					}
					BgL_test1849z00_2539 = CBOOL(BgL_tmpz00_2540);
				}
				if (BgL_test1849z00_2539)
					{	/* Ast/shrinkify.scm 172 */
						{	/* Ast/shrinkify.scm 172 */
							long BgL_arg1559z00_2168;

							{	/* Ast/shrinkify.scm 172 */
								obj_t BgL_arg1561z00_2169;

								{	/* Ast/shrinkify.scm 172 */
									obj_t BgL_arg1564z00_2170;

									{	/* Ast/shrinkify.scm 172 */
										obj_t BgL_arg1815z00_2171;
										long BgL_arg1816z00_2172;

										BgL_arg1815z00_2171 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Ast/shrinkify.scm 172 */
											long BgL_arg1817z00_2173;

											BgL_arg1817z00_2173 =
												BGL_OBJECT_CLASS_NUM(
												((BgL_objectz00_bglt)
													((BgL_boxzd2refzd2_bglt) BgL_nodez00_2080)));
											BgL_arg1816z00_2172 = (BgL_arg1817z00_2173 - OBJECT_TYPE);
										}
										BgL_arg1564z00_2170 =
											VECTOR_REF(BgL_arg1815z00_2171, BgL_arg1816z00_2172);
									}
									BgL_arg1561z00_2169 =
										BGl_classzd2superzd2zz__objectz00(BgL_arg1564z00_2170);
								}
								{	/* Ast/shrinkify.scm 172 */
									obj_t BgL_tmpz00_2553;

									BgL_tmpz00_2553 = ((obj_t) BgL_arg1561z00_2169);
									BgL_arg1559z00_2168 = BGL_CLASS_NUM(BgL_tmpz00_2553);
							}}
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_boxzd2refzd2_bglt) BgL_nodez00_2080)),
								BgL_arg1559z00_2168);
						}
						{	/* Ast/shrinkify.scm 172 */
							BgL_objectz00_bglt BgL_tmpz00_2559;

							BgL_tmpz00_2559 =
								((BgL_objectz00_bglt)
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_2080));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2559, BFALSE);
						}
						((BgL_objectz00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_2080));
						((obj_t) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_2080));
					}
				else
					{	/* Ast/shrinkify.scm 172 */
						BFALSE;
					}
			}
			{	/* Ast/shrinkify.scm 173 */
				BgL_varz00_bglt BgL_arg1565z00_2174;

				BgL_arg1565z00_2174 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_nodez00_2080)))->BgL_varz00);
				BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
					((BgL_nodez00_bglt) BgL_arg1565z00_2174));
			}
			return BUNSPEC;
		}

	}



/* &shrink-node!-make-bo1283 */
	obj_t BGl_z62shrinkzd2nodez12zd2makezd2bo1283za2zzast_shrinkifyz00(obj_t
		BgL_envz00_2081, obj_t BgL_nodez00_2082)
	{
		{	/* Ast/shrinkify.scm 163 */
			{	/* Ast/shrinkify.scm 164 */
				bool_t BgL_test1850z00_2571;

				{	/* Ast/shrinkify.scm 164 */
					obj_t BgL_tmpz00_2572;

					{	/* Ast/shrinkify.scm 164 */
						BgL_objectz00_bglt BgL_tmpz00_2573;

						BgL_tmpz00_2573 =
							((BgL_objectz00_bglt)
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_2082));
						BgL_tmpz00_2572 = BGL_OBJECT_WIDENING(BgL_tmpz00_2573);
					}
					BgL_test1850z00_2571 = CBOOL(BgL_tmpz00_2572);
				}
				if (BgL_test1850z00_2571)
					{	/* Ast/shrinkify.scm 164 */
						{	/* Ast/shrinkify.scm 164 */
							long BgL_arg1544z00_2176;

							{	/* Ast/shrinkify.scm 164 */
								obj_t BgL_arg1546z00_2177;

								{	/* Ast/shrinkify.scm 164 */
									obj_t BgL_arg1552z00_2178;

									{	/* Ast/shrinkify.scm 164 */
										obj_t BgL_arg1815z00_2179;
										long BgL_arg1816z00_2180;

										BgL_arg1815z00_2179 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Ast/shrinkify.scm 164 */
											long BgL_arg1817z00_2181;

											BgL_arg1817z00_2181 =
												BGL_OBJECT_CLASS_NUM(
												((BgL_objectz00_bglt)
													((BgL_makezd2boxzd2_bglt) BgL_nodez00_2082)));
											BgL_arg1816z00_2180 = (BgL_arg1817z00_2181 - OBJECT_TYPE);
										}
										BgL_arg1552z00_2178 =
											VECTOR_REF(BgL_arg1815z00_2179, BgL_arg1816z00_2180);
									}
									BgL_arg1546z00_2177 =
										BGl_classzd2superzd2zz__objectz00(BgL_arg1552z00_2178);
								}
								{	/* Ast/shrinkify.scm 164 */
									obj_t BgL_tmpz00_2585;

									BgL_tmpz00_2585 = ((obj_t) BgL_arg1546z00_2177);
									BgL_arg1544z00_2176 = BGL_CLASS_NUM(BgL_tmpz00_2585);
							}}
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_makezd2boxzd2_bglt) BgL_nodez00_2082)),
								BgL_arg1544z00_2176);
						}
						{	/* Ast/shrinkify.scm 164 */
							BgL_objectz00_bglt BgL_tmpz00_2591;

							BgL_tmpz00_2591 =
								((BgL_objectz00_bglt)
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_2082));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2591, BFALSE);
						}
						((BgL_objectz00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_2082));
						((obj_t) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_2082));
					}
				else
					{	/* Ast/shrinkify.scm 164 */
						BFALSE;
					}
			}
			BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
				(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_2082)))->BgL_valuez00));
			return BUNSPEC;
		}

	}



/* &shrink-node!-switch1281 */
	obj_t BGl_z62shrinkzd2nodez12zd2switch1281z70zzast_shrinkifyz00(obj_t
		BgL_envz00_2083, obj_t BgL_nodez00_2084)
	{
		{	/* Ast/shrinkify.scm 153 */
			BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
				(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_2084)))->BgL_testz00));
			{	/* Ast/shrinkify.scm 155 */
				obj_t BgL_g1238z00_2183;

				BgL_g1238z00_2183 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_2084)))->BgL_clausesz00);
				{
					obj_t BgL_l1236z00_2185;

					BgL_l1236z00_2185 = BgL_g1238z00_2183;
				BgL_zc3z04anonymousza31517ze3z87_2184:
					if (PAIRP(BgL_l1236z00_2185))
						{	/* Ast/shrinkify.scm 157 */
							{	/* Ast/shrinkify.scm 156 */
								obj_t BgL_clausez00_2186;

								BgL_clausez00_2186 = CAR(BgL_l1236z00_2185);
								{	/* Ast/shrinkify.scm 156 */
									obj_t BgL_arg1535z00_2187;

									BgL_arg1535z00_2187 = CDR(((obj_t) BgL_clausez00_2186));
									BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
										((BgL_nodez00_bglt) BgL_arg1535z00_2187));
								}
							}
							{
								obj_t BgL_l1236z00_2614;

								BgL_l1236z00_2614 = CDR(BgL_l1236z00_2185);
								BgL_l1236z00_2185 = BgL_l1236z00_2614;
								goto BgL_zc3z04anonymousza31517ze3z87_2184;
							}
						}
					else
						{	/* Ast/shrinkify.scm 157 */
							((bool_t) 1);
						}
				}
			}
			return BUNSPEC;
		}

	}



/* &shrink-node!-fail1279 */
	obj_t BGl_z62shrinkzd2nodez12zd2fail1279z70zzast_shrinkifyz00(obj_t
		BgL_envz00_2085, obj_t BgL_nodez00_2086)
	{
		{	/* Ast/shrinkify.scm 144 */
			BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2086)))->BgL_procz00));
			BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2086)))->BgL_msgz00));
			BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2086)))->BgL_objz00));
			return BUNSPEC;
		}

	}



/* &shrink-node!-conditi1277 */
	obj_t BGl_z62shrinkzd2nodez12zd2conditi1277z70zzast_shrinkifyz00(obj_t
		BgL_envz00_2087, obj_t BgL_nodez00_2088)
	{
		{	/* Ast/shrinkify.scm 135 */
			BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2088)))->BgL_testz00));
			BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2088)))->BgL_truez00));
			BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2088)))->BgL_falsez00));
			return BUNSPEC;
		}

	}



/* &shrink-node!-setq1275 */
	obj_t BGl_z62shrinkzd2nodez12zd2setq1275z70zzast_shrinkifyz00(obj_t
		BgL_envz00_2089, obj_t BgL_nodez00_2090)
	{
		{	/* Ast/shrinkify.scm 128 */
			BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
				(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_2090)))->BgL_valuez00));
			return BUNSPEC;
		}

	}



/* &shrink-node!-cast1273 */
	obj_t BGl_z62shrinkzd2nodez12zd2cast1273z70zzast_shrinkifyz00(obj_t
		BgL_envz00_2091, obj_t BgL_nodez00_2092)
	{
		{	/* Ast/shrinkify.scm 121 */
			BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
				(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_2092)))->BgL_argz00));
			return BUNSPEC;
		}

	}



/* &shrink-node!-extern1271 */
	obj_t BGl_z62shrinkzd2nodez12zd2extern1271z70zzast_shrinkifyz00(obj_t
		BgL_envz00_2093, obj_t BgL_nodez00_2094)
	{
		{	/* Ast/shrinkify.scm 113 */
			{	/* Ast/shrinkify.scm 114 */
				bool_t BgL_test1852z00_2640;

				{	/* Ast/shrinkify.scm 114 */
					obj_t BgL_tmpz00_2641;

					{	/* Ast/shrinkify.scm 114 */
						BgL_objectz00_bglt BgL_tmpz00_2642;

						BgL_tmpz00_2642 =
							((BgL_objectz00_bglt) ((BgL_externz00_bglt) BgL_nodez00_2094));
						BgL_tmpz00_2641 = BGL_OBJECT_WIDENING(BgL_tmpz00_2642);
					}
					BgL_test1852z00_2640 = CBOOL(BgL_tmpz00_2641);
				}
				if (BgL_test1852z00_2640)
					{	/* Ast/shrinkify.scm 114 */
						{	/* Ast/shrinkify.scm 114 */
							long BgL_arg1437z00_2193;

							{	/* Ast/shrinkify.scm 114 */
								obj_t BgL_arg1448z00_2194;

								{	/* Ast/shrinkify.scm 114 */
									obj_t BgL_arg1453z00_2195;

									{	/* Ast/shrinkify.scm 114 */
										obj_t BgL_arg1815z00_2196;
										long BgL_arg1816z00_2197;

										BgL_arg1815z00_2196 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Ast/shrinkify.scm 114 */
											long BgL_arg1817z00_2198;

											BgL_arg1817z00_2198 =
												BGL_OBJECT_CLASS_NUM(
												((BgL_objectz00_bglt)
													((BgL_externz00_bglt) BgL_nodez00_2094)));
											BgL_arg1816z00_2197 = (BgL_arg1817z00_2198 - OBJECT_TYPE);
										}
										BgL_arg1453z00_2195 =
											VECTOR_REF(BgL_arg1815z00_2196, BgL_arg1816z00_2197);
									}
									BgL_arg1448z00_2194 =
										BGl_classzd2superzd2zz__objectz00(BgL_arg1453z00_2195);
								}
								{	/* Ast/shrinkify.scm 114 */
									obj_t BgL_tmpz00_2654;

									BgL_tmpz00_2654 = ((obj_t) BgL_arg1448z00_2194);
									BgL_arg1437z00_2193 = BGL_CLASS_NUM(BgL_tmpz00_2654);
							}}
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_externz00_bglt) BgL_nodez00_2094)),
								BgL_arg1437z00_2193);
						}
						{	/* Ast/shrinkify.scm 114 */
							BgL_objectz00_bglt BgL_tmpz00_2660;

							BgL_tmpz00_2660 =
								((BgL_objectz00_bglt) ((BgL_externz00_bglt) BgL_nodez00_2094));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2660, BFALSE);
						}
						((BgL_objectz00_bglt) ((BgL_externz00_bglt) BgL_nodez00_2094));
						((obj_t) ((BgL_externz00_bglt) BgL_nodez00_2094));
					}
				else
					{	/* Ast/shrinkify.scm 114 */
						BFALSE;
					}
			}
			BGl_shrinkzd2nodeza2z12z62zzast_shrinkifyz00(
				(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_2094)))->BgL_exprza2za2));
			return BUNSPEC;
		}

	}



/* &shrink-node!-funcall1269 */
	obj_t BGl_z62shrinkzd2nodez12zd2funcall1269z70zzast_shrinkifyz00(obj_t
		BgL_envz00_2095, obj_t BgL_nodez00_2096)
	{
		{	/* Ast/shrinkify.scm 104 */
			{	/* Ast/shrinkify.scm 105 */
				bool_t BgL_test1853z00_2671;

				{	/* Ast/shrinkify.scm 105 */
					obj_t BgL_tmpz00_2672;

					{	/* Ast/shrinkify.scm 105 */
						BgL_objectz00_bglt BgL_tmpz00_2673;

						BgL_tmpz00_2673 =
							((BgL_objectz00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_2096));
						BgL_tmpz00_2672 = BGL_OBJECT_WIDENING(BgL_tmpz00_2673);
					}
					BgL_test1853z00_2671 = CBOOL(BgL_tmpz00_2672);
				}
				if (BgL_test1853z00_2671)
					{	/* Ast/shrinkify.scm 105 */
						{	/* Ast/shrinkify.scm 105 */
							long BgL_arg1408z00_2200;

							{	/* Ast/shrinkify.scm 105 */
								obj_t BgL_arg1410z00_2201;

								{	/* Ast/shrinkify.scm 105 */
									obj_t BgL_arg1421z00_2202;

									{	/* Ast/shrinkify.scm 105 */
										obj_t BgL_arg1815z00_2203;
										long BgL_arg1816z00_2204;

										BgL_arg1815z00_2203 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Ast/shrinkify.scm 105 */
											long BgL_arg1817z00_2205;

											BgL_arg1817z00_2205 =
												BGL_OBJECT_CLASS_NUM(
												((BgL_objectz00_bglt)
													((BgL_funcallz00_bglt) BgL_nodez00_2096)));
											BgL_arg1816z00_2204 = (BgL_arg1817z00_2205 - OBJECT_TYPE);
										}
										BgL_arg1421z00_2202 =
											VECTOR_REF(BgL_arg1815z00_2203, BgL_arg1816z00_2204);
									}
									BgL_arg1410z00_2201 =
										BGl_classzd2superzd2zz__objectz00(BgL_arg1421z00_2202);
								}
								{	/* Ast/shrinkify.scm 105 */
									obj_t BgL_tmpz00_2685;

									BgL_tmpz00_2685 = ((obj_t) BgL_arg1410z00_2201);
									BgL_arg1408z00_2200 = BGL_CLASS_NUM(BgL_tmpz00_2685);
							}}
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_funcallz00_bglt) BgL_nodez00_2096)),
								BgL_arg1408z00_2200);
						}
						{	/* Ast/shrinkify.scm 105 */
							BgL_objectz00_bglt BgL_tmpz00_2691;

							BgL_tmpz00_2691 =
								((BgL_objectz00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_2096));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2691, BFALSE);
						}
						((BgL_objectz00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_2096));
						((obj_t) ((BgL_funcallz00_bglt) BgL_nodez00_2096));
					}
				else
					{	/* Ast/shrinkify.scm 105 */
						BFALSE;
					}
			}
			BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_2096)))->BgL_funz00));
			BGl_shrinkzd2nodeza2z12z62zzast_shrinkifyz00(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_2096)))->BgL_argsz00));
			return BUNSPEC;
		}

	}



/* &shrink-node!-app-ly1267 */
	obj_t BGl_z62shrinkzd2nodez12zd2appzd2ly1267za2zzast_shrinkifyz00(obj_t
		BgL_envz00_2097, obj_t BgL_nodez00_2098)
	{
		{	/* Ast/shrinkify.scm 95 */
			{	/* Ast/shrinkify.scm 96 */
				bool_t BgL_test1854z00_2705;

				{	/* Ast/shrinkify.scm 96 */
					obj_t BgL_tmpz00_2706;

					{	/* Ast/shrinkify.scm 96 */
						BgL_objectz00_bglt BgL_tmpz00_2707;

						BgL_tmpz00_2707 =
							((BgL_objectz00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_2098));
						BgL_tmpz00_2706 = BGL_OBJECT_WIDENING(BgL_tmpz00_2707);
					}
					BgL_test1854z00_2705 = CBOOL(BgL_tmpz00_2706);
				}
				if (BgL_test1854z00_2705)
					{	/* Ast/shrinkify.scm 96 */
						{	/* Ast/shrinkify.scm 96 */
							long BgL_arg1364z00_2207;

							{	/* Ast/shrinkify.scm 96 */
								obj_t BgL_arg1367z00_2208;

								{	/* Ast/shrinkify.scm 96 */
									obj_t BgL_arg1370z00_2209;

									{	/* Ast/shrinkify.scm 96 */
										obj_t BgL_arg1815z00_2210;
										long BgL_arg1816z00_2211;

										BgL_arg1815z00_2210 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Ast/shrinkify.scm 96 */
											long BgL_arg1817z00_2212;

											BgL_arg1817z00_2212 =
												BGL_OBJECT_CLASS_NUM(
												((BgL_objectz00_bglt)
													((BgL_appzd2lyzd2_bglt) BgL_nodez00_2098)));
											BgL_arg1816z00_2211 = (BgL_arg1817z00_2212 - OBJECT_TYPE);
										}
										BgL_arg1370z00_2209 =
											VECTOR_REF(BgL_arg1815z00_2210, BgL_arg1816z00_2211);
									}
									BgL_arg1367z00_2208 =
										BGl_classzd2superzd2zz__objectz00(BgL_arg1370z00_2209);
								}
								{	/* Ast/shrinkify.scm 96 */
									obj_t BgL_tmpz00_2719;

									BgL_tmpz00_2719 = ((obj_t) BgL_arg1367z00_2208);
									BgL_arg1364z00_2207 = BGL_CLASS_NUM(BgL_tmpz00_2719);
							}}
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_2098)),
								BgL_arg1364z00_2207);
						}
						{	/* Ast/shrinkify.scm 96 */
							BgL_objectz00_bglt BgL_tmpz00_2725;

							BgL_tmpz00_2725 =
								((BgL_objectz00_bglt)
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2098));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2725, BFALSE);
						}
						((BgL_objectz00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_2098));
						((obj_t) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_2098));
					}
				else
					{	/* Ast/shrinkify.scm 96 */
						BFALSE;
					}
			}
			BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
				(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_2098)))->BgL_funz00));
			BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
				(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_2098)))->BgL_argz00));
			return BUNSPEC;
		}

	}



/* &shrink-node!-app1265 */
	obj_t BGl_z62shrinkzd2nodez12zd2app1265z70zzast_shrinkifyz00(obj_t
		BgL_envz00_2099, obj_t BgL_nodez00_2100)
	{
		{	/* Ast/shrinkify.scm 86 */
			{	/* Ast/shrinkify.scm 87 */
				bool_t BgL_test1855z00_2739;

				{	/* Ast/shrinkify.scm 87 */
					obj_t BgL_tmpz00_2740;

					{	/* Ast/shrinkify.scm 87 */
						BgL_objectz00_bglt BgL_tmpz00_2741;

						BgL_tmpz00_2741 =
							((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_2100));
						BgL_tmpz00_2740 = BGL_OBJECT_WIDENING(BgL_tmpz00_2741);
					}
					BgL_test1855z00_2739 = CBOOL(BgL_tmpz00_2740);
				}
				if (BgL_test1855z00_2739)
					{	/* Ast/shrinkify.scm 87 */
						{	/* Ast/shrinkify.scm 87 */
							long BgL_arg1348z00_2214;

							{	/* Ast/shrinkify.scm 87 */
								obj_t BgL_arg1349z00_2215;

								{	/* Ast/shrinkify.scm 87 */
									obj_t BgL_arg1351z00_2216;

									{	/* Ast/shrinkify.scm 87 */
										obj_t BgL_arg1815z00_2217;
										long BgL_arg1816z00_2218;

										BgL_arg1815z00_2217 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Ast/shrinkify.scm 87 */
											long BgL_arg1817z00_2219;

											BgL_arg1817z00_2219 =
												BGL_OBJECT_CLASS_NUM(
												((BgL_objectz00_bglt)
													((BgL_appz00_bglt) BgL_nodez00_2100)));
											BgL_arg1816z00_2218 = (BgL_arg1817z00_2219 - OBJECT_TYPE);
										}
										BgL_arg1351z00_2216 =
											VECTOR_REF(BgL_arg1815z00_2217, BgL_arg1816z00_2218);
									}
									BgL_arg1349z00_2215 =
										BGl_classzd2superzd2zz__objectz00(BgL_arg1351z00_2216);
								}
								{	/* Ast/shrinkify.scm 87 */
									obj_t BgL_tmpz00_2753;

									BgL_tmpz00_2753 = ((obj_t) BgL_arg1349z00_2215);
									BgL_arg1348z00_2214 = BGL_CLASS_NUM(BgL_tmpz00_2753);
							}}
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_appz00_bglt) BgL_nodez00_2100)), BgL_arg1348z00_2214);
						}
						{	/* Ast/shrinkify.scm 87 */
							BgL_objectz00_bglt BgL_tmpz00_2759;

							BgL_tmpz00_2759 =
								((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_2100));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2759, BFALSE);
						}
						((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_2100));
						((obj_t) ((BgL_appz00_bglt) BgL_nodez00_2100));
					}
				else
					{	/* Ast/shrinkify.scm 87 */
						BFALSE;
					}
			}
			{	/* Ast/shrinkify.scm 88 */
				BgL_varz00_bglt BgL_arg1352z00_2220;

				BgL_arg1352z00_2220 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_2100)))->BgL_funz00);
				BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
					((BgL_nodez00_bglt) BgL_arg1352z00_2220));
			}
			BGl_shrinkzd2nodeza2z12z62zzast_shrinkifyz00(
				(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_2100)))->BgL_argsz00));
			return BUNSPEC;
		}

	}



/* &shrink-node!-sync1263 */
	obj_t BGl_z62shrinkzd2nodez12zd2sync1263z70zzast_shrinkifyz00(obj_t
		BgL_envz00_2101, obj_t BgL_nodez00_2102)
	{
		{	/* Ast/shrinkify.scm 77 */
			BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2102)))->BgL_mutexz00));
			BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2102)))->BgL_prelockz00));
			BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2102)))->BgL_bodyz00));
			return BUNSPEC;
		}

	}



/* &shrink-node!-sequenc1261 */
	obj_t BGl_z62shrinkzd2nodez12zd2sequenc1261z70zzast_shrinkifyz00(obj_t
		BgL_envz00_2103, obj_t BgL_nodez00_2104)
	{
		{	/* Ast/shrinkify.scm 70 */
			BGl_shrinkzd2nodeza2z12z62zzast_shrinkifyz00(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_2104)))->BgL_nodesz00));
			return BUNSPEC;
		}

	}



/* &shrink-node!-kwote1259 */
	obj_t BGl_z62shrinkzd2nodez12zd2kwote1259z70zzast_shrinkifyz00(obj_t
		BgL_envz00_2105, obj_t BgL_nodez00_2106)
	{
		{	/* Ast/shrinkify.scm 64 */
			return BUNSPEC;
		}

	}



/* &shrink-node!-var1257 */
	obj_t BGl_z62shrinkzd2nodez12zd2var1257z70zzast_shrinkifyz00(obj_t
		BgL_envz00_2107, obj_t BgL_nodez00_2108)
	{
		{	/* Ast/shrinkify.scm 58 */
			return BUNSPEC;
		}

	}



/* &shrink-node!-atom1255 */
	obj_t BGl_z62shrinkzd2nodez12zd2atom1255z70zzast_shrinkifyz00(obj_t
		BgL_envz00_2109, obj_t BgL_nodez00_2110)
	{
		{	/* Ast/shrinkify.scm 52 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_shrinkifyz00(void)
	{
		{	/* Ast/shrinkify.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1799z00zzast_shrinkifyz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1799z00zzast_shrinkifyz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1799z00zzast_shrinkifyz00));
			return
				BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1799z00zzast_shrinkifyz00));
		}

	}

#ifdef __cplusplus
}
#endif
