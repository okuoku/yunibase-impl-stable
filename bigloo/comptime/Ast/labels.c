/*===========================================================================*/
/*   (Ast/labels.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/labels.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_AST_LABELS_TYPE_DEFINITIONS
#define BGL_AST_LABELS_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;


#endif													// BGL_AST_LABELS_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_normaliza7ezd2prognz75zztools_prognz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzast_labelsz00 = BUNSPEC;
	extern obj_t BGl_findzd2locationzf2locz20zztools_locationz00(obj_t, obj_t);
	static obj_t BGl_makezd2localzd2nooptzd2sfunzd2zzast_labelsz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t BGl_toplevelzd2initzd2zzast_labelsz00(void);
	static obj_t BGl_labelszd2bindingzd2zzast_labelsz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_genericzd2initzd2zzast_labelsz00(void);
	extern obj_t BGl_leavezd2functionzd2zztools_errorz00(void);
	static obj_t BGl_objectzd2initzd2zzast_labelsz00(void);
	BGL_EXPORTED_DECL obj_t BGl_labelszd2symzf3z21zzast_labelsz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_labelszd2symzd2zzast_labelsz00(void);
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	extern long BGl_localzd2arityzd2zztools_argsz00(obj_t);
	static BgL_letzd2funzd2_bglt BGl_z62labelszd2ze3nodez53zzast_labelsz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_z62labelszd2symzb0zzast_labelsz00(obj_t);
	extern BgL_localz00_bglt BGl_makezd2localzd2sfunz00zzast_localz00(obj_t,
		BgL_typez00_bglt, BgL_sfunz00_bglt);
	extern BgL_localz00_bglt
		BGl_makezd2userzd2localzd2sfunzd2zzast_localz00(obj_t, BgL_typez00_bglt,
		BgL_sfunz00_bglt);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzast_labelsz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzast_labelsz00(void);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	extern BgL_typez00_bglt
		BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_z62labelszd2symzf3z43zzast_labelsz00(obj_t, obj_t);
	extern bool_t BGl_dssslzd2arityzd2za7erozf3z54zztools_argsz00(int, obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzast_labelsz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_dssslz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_argsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__dssslz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_enterzd2functionzd2zztools_errorz00(obj_t);
	BGL_IMPORT obj_t BGl_makezd2dssslzd2functionzd2preludezd2zz__dssslz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzast_labelsz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_labelsz00(void);
	extern BgL_localz00_bglt BGl_makezd2localzd2svarz00zzast_localz00(obj_t,
		BgL_typez00_bglt);
	static obj_t BGl_allocatezd2sfunszd2zzast_labelsz00(obj_t, obj_t);
	extern BgL_nodez00_bglt BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00(obj_t,
		obj_t, obj_t);
	extern BgL_localz00_bglt
		BGl_makezd2userzd2localzd2svarzd2zzast_localz00(obj_t, BgL_typez00_bglt);
	static obj_t BGl_importedzd2moduleszd2initz00zzast_labelsz00(void);
	extern obj_t BGl_dssslzd2argsza2zd2ze3argszd2listz93zztools_dssslz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzast_labelsz00(void);
	extern obj_t BGl_parsezd2idzd2zzast_identz00(obj_t, obj_t);
	extern BgL_nodez00_bglt BGl_sexpzd2ze3nodez31zzast_sexpz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern bool_t BGl_userzd2symbolzf3z21zzast_identz00(obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	BGL_EXPORTED_DECL BgL_letzd2funzd2_bglt
		BGl_labelszd2ze3nodez31zzast_labelsz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_za2labelsza2z00zzast_labelsz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t __cnst[4];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_labelszd2symzf3zd2envzf3zzast_labelsz00,
		BgL_bgl_za762labelsza7d2symza71704za7,
		BGl_z62labelszd2symzf3z43zzast_labelsz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1700z00zzast_labelsz00,
		BgL_bgl_string1700za700za7za7a1705za7, "Illegal `labels' form", 21);
	      DEFINE_STRING(BGl_string1701z00zzast_labelsz00,
		BgL_bgl_string1701za700za7za7a1706za7, "ast_labels", 10);
	      DEFINE_STRING(BGl_string1702z00zzast_labelsz00,
		BgL_bgl_string1702za700za7za7a1707za7, "value \077\077? plain labels ", 23);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_labelszd2ze3nodezd2envze3zzast_labelsz00,
		BgL_bgl_za762labelsza7d2za7e3n1708za7,
		BGl_z62labelszd2ze3nodez53zzast_labelsz00, 0L, BUNSPEC, 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_labelszd2symzd2envz00zzast_labelsz00,
		BgL_bgl_za762labelsza7d2symza71709za7,
		BGl_z62labelszd2symzb0zzast_labelsz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1697z00zzast_labelsz00,
		BgL_bgl_string1697za700za7za7a1710za7, "Illegal `labels' expression", 27);
	      DEFINE_STRING(BGl_string1698z00zzast_labelsz00,
		BgL_bgl_string1698za700za7za7a1711za7, "Illegal `binding' form", 22);
	      DEFINE_STRING(BGl_string1699z00zzast_labelsz00,
		BgL_bgl_string1699za700za7za7a1712za7, "Illegal formal type", 19);
	extern obj_t BGl_userzd2errorzd2envz00zztools_errorz00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzast_labelsz00));
		     ADD_ROOT((void *) (&BGl_za2labelsza2z00zzast_labelsz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzast_labelsz00(long
		BgL_checksumz00_1793, char *BgL_fromz00_1794)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_labelsz00))
				{
					BGl_requirezd2initializa7ationz75zzast_labelsz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_labelsz00();
					BGl_libraryzd2moduleszd2initz00zzast_labelsz00();
					BGl_cnstzd2initzd2zzast_labelsz00();
					BGl_importedzd2moduleszd2initz00zzast_labelsz00();
					return BGl_toplevelzd2initzd2zzast_labelsz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_labelsz00(void)
	{
		{	/* Ast/labels.scm 14 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_labels");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_labels");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "ast_labels");
			BGl_modulezd2initializa7ationz75zz__dssslz00(0L, "ast_labels");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "ast_labels");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "ast_labels");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"ast_labels");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "ast_labels");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "ast_labels");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_labelsz00(void)
	{
		{	/* Ast/labels.scm 14 */
			{	/* Ast/labels.scm 14 */
				obj_t BgL_cportz00_1782;

				{	/* Ast/labels.scm 14 */
					obj_t BgL_stringz00_1789;

					BgL_stringz00_1789 = BGl_string1702z00zzast_labelsz00;
					{	/* Ast/labels.scm 14 */
						obj_t BgL_startz00_1790;

						BgL_startz00_1790 = BINT(0L);
						{	/* Ast/labels.scm 14 */
							obj_t BgL_endz00_1791;

							BgL_endz00_1791 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1789)));
							{	/* Ast/labels.scm 14 */

								BgL_cportz00_1782 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1789, BgL_startz00_1790, BgL_endz00_1791);
				}}}}
				{
					long BgL_iz00_1783;

					BgL_iz00_1783 = 3L;
				BgL_loopz00_1784:
					if ((BgL_iz00_1783 == -1L))
						{	/* Ast/labels.scm 14 */
							return BUNSPEC;
						}
					else
						{	/* Ast/labels.scm 14 */
							{	/* Ast/labels.scm 14 */
								obj_t BgL_arg1703z00_1785;

								{	/* Ast/labels.scm 14 */

									{	/* Ast/labels.scm 14 */
										obj_t BgL_locationz00_1787;

										BgL_locationz00_1787 = BBOOL(((bool_t) 0));
										{	/* Ast/labels.scm 14 */

											BgL_arg1703z00_1785 =
												BGl_readz00zz__readerz00(BgL_cportz00_1782,
												BgL_locationz00_1787);
										}
									}
								}
								{	/* Ast/labels.scm 14 */
									int BgL_tmpz00_1821;

									BgL_tmpz00_1821 = (int) (BgL_iz00_1783);
									CNST_TABLE_SET(BgL_tmpz00_1821, BgL_arg1703z00_1785);
							}}
							{	/* Ast/labels.scm 14 */
								int BgL_auxz00_1788;

								BgL_auxz00_1788 = (int) ((BgL_iz00_1783 - 1L));
								{
									long BgL_iz00_1826;

									BgL_iz00_1826 = (long) (BgL_auxz00_1788);
									BgL_iz00_1783 = BgL_iz00_1826;
									goto BgL_loopz00_1784;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_labelsz00(void)
	{
		{	/* Ast/labels.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzast_labelsz00(void)
	{
		{	/* Ast/labels.scm 14 */
			return (BGl_za2labelsza2z00zzast_labelsz00 =
				BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(0)), BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzast_labelsz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1374;

				BgL_headz00_1374 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1375;
					obj_t BgL_tailz00_1376;

					BgL_prevz00_1375 = BgL_headz00_1374;
					BgL_tailz00_1376 = BgL_l1z00_1;
				BgL_loopz00_1377:
					if (PAIRP(BgL_tailz00_1376))
						{
							obj_t BgL_newzd2prevzd2_1379;

							BgL_newzd2prevzd2_1379 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1376), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1375, BgL_newzd2prevzd2_1379);
							{
								obj_t BgL_tailz00_1838;
								obj_t BgL_prevz00_1837;

								BgL_prevz00_1837 = BgL_newzd2prevzd2_1379;
								BgL_tailz00_1838 = CDR(BgL_tailz00_1376);
								BgL_tailz00_1376 = BgL_tailz00_1838;
								BgL_prevz00_1375 = BgL_prevz00_1837;
								goto BgL_loopz00_1377;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1374);
				}
			}
		}

	}



/* labels-sym */
	BGL_EXPORTED_DEF obj_t BGl_labelszd2symzd2zzast_labelsz00(void)
	{
		{	/* Ast/labels.scm 39 */
			return BGl_za2labelsza2z00zzast_labelsz00;
		}

	}



/* &labels-sym */
	obj_t BGl_z62labelszd2symzb0zzast_labelsz00(obj_t BgL_envz00_1768)
	{
		{	/* Ast/labels.scm 39 */
			return BGl_labelszd2symzd2zzast_labelsz00();
		}

	}



/* labels-sym? */
	BGL_EXPORTED_DEF obj_t BGl_labelszd2symzf3z21zzast_labelsz00(obj_t
		BgL_symz00_17)
	{
		{	/* Ast/labels.scm 45 */
			return BBOOL((BgL_symz00_17 == BGl_za2labelsza2z00zzast_labelsz00));
		}

	}



/* &labels-sym? */
	obj_t BGl_z62labelszd2symzf3z43zzast_labelsz00(obj_t BgL_envz00_1769,
		obj_t BgL_symz00_1770)
	{
		{	/* Ast/labels.scm 45 */
			return BGl_labelszd2symzf3z21zzast_labelsz00(BgL_symz00_1770);
		}

	}



/* labels->node */
	BGL_EXPORTED_DEF BgL_letzd2funzd2_bglt
		BGl_labelszd2ze3nodez31zzast_labelsz00(obj_t BgL_expz00_18,
		obj_t BgL_stackz00_19, obj_t BgL_locz00_20, obj_t BgL_sitez00_21)
	{
		{	/* Ast/labels.scm 51 */
			{	/* Ast/labels.scm 52 */
				obj_t BgL_locz00_1394;

				BgL_locz00_1394 =
					BGl_findzd2locationzf2locz20zztools_locationz00(BgL_expz00_18,
					BgL_locz00_20);
				{
					obj_t BgL_bindingsz00_1395;
					obj_t BgL_bodyz00_1396;

					if (PAIRP(BgL_expz00_18))
						{	/* Ast/labels.scm 53 */
							obj_t BgL_cdrzd2367zd2_1401;

							BgL_cdrzd2367zd2_1401 = CDR(((obj_t) BgL_expz00_18));
							if (PAIRP(BgL_cdrzd2367zd2_1401))
								{	/* Ast/labels.scm 53 */
									obj_t BgL_carzd2370zd2_1403;

									BgL_carzd2370zd2_1403 = CAR(BgL_cdrzd2367zd2_1401);
									if (PAIRP(BgL_carzd2370zd2_1403))
										{	/* Ast/labels.scm 53 */
											BgL_bindingsz00_1395 = BgL_carzd2370zd2_1403;
											BgL_bodyz00_1396 = CDR(BgL_cdrzd2367zd2_1401);
											{	/* Ast/labels.scm 55 */
												obj_t BgL_localsz00_1406;

												BgL_localsz00_1406 =
													BGl_allocatezd2sfunszd2zzast_labelsz00
													(BgL_bindingsz00_1395, BgL_locz00_1394);
												{	/* Ast/labels.scm 55 */
													obj_t BgL_newzd2stackzd2_1407;

													BgL_newzd2stackzd2_1407 =
														BGl_appendzd221011zd2zzast_labelsz00
														(BgL_localsz00_1406, BgL_stackz00_19);
													{	/* Ast/labels.scm 56 */
														BgL_nodez00_bglt BgL_bodyz00_1408;

														BgL_bodyz00_1408 =
															BGl_sexpzd2ze3nodez31zzast_sexpz00
															(BGl_normaliza7ezd2prognz75zztools_prognz00
															(BgL_bodyz00_1396), BgL_newzd2stackzd2_1407,
															BgL_locz00_1394, BgL_sitez00_21);
														{	/* Ast/labels.scm 57 */
															obj_t BgL_locz00_1409;

															BgL_locz00_1409 =
																BGl_findzd2locationzf2locz20zztools_locationz00
																(BgL_expz00_18, BgL_locz00_1394);
															{	/* Ast/labels.scm 58 */

																{
																	obj_t BgL_ll1229z00_1411;
																	obj_t BgL_ll1230z00_1412;

																	BgL_ll1229z00_1411 = BgL_localsz00_1406;
																	BgL_ll1230z00_1412 = BgL_bindingsz00_1395;
																BgL_zc3z04anonymousza31311ze3z87_1413:
																	if (NULLP(BgL_ll1229z00_1411))
																		{	/* Ast/labels.scm 60 */
																			((bool_t) 1);
																		}
																	else
																		{	/* Ast/labels.scm 60 */
																			{	/* Ast/labels.scm 60 */
																				obj_t BgL_funz00_1415;
																				obj_t BgL_bz00_1416;

																				BgL_funz00_1415 =
																					CAR(((obj_t) BgL_ll1229z00_1411));
																				BgL_bz00_1416 =
																					CAR(((obj_t) BgL_ll1230z00_1412));
																				BGl_labelszd2bindingzd2zzast_labelsz00
																					(BgL_funz00_1415, BgL_bz00_1416,
																					BgL_newzd2stackzd2_1407,
																					BgL_locz00_1409);
																			}
																			{	/* Ast/labels.scm 60 */
																				obj_t BgL_arg1314z00_1417;
																				obj_t BgL_arg1315z00_1418;

																				BgL_arg1314z00_1417 =
																					CDR(((obj_t) BgL_ll1229z00_1411));
																				BgL_arg1315z00_1418 =
																					CDR(((obj_t) BgL_ll1230z00_1412));
																				{
																					obj_t BgL_ll1230z00_1872;
																					obj_t BgL_ll1229z00_1871;

																					BgL_ll1229z00_1871 =
																						BgL_arg1314z00_1417;
																					BgL_ll1230z00_1872 =
																						BgL_arg1315z00_1418;
																					BgL_ll1230z00_1412 =
																						BgL_ll1230z00_1872;
																					BgL_ll1229z00_1411 =
																						BgL_ll1229z00_1871;
																					goto
																						BgL_zc3z04anonymousza31311ze3z87_1413;
																				}
																			}
																		}
																}
																{	/* Ast/labels.scm 64 */
																	BgL_letzd2funzd2_bglt BgL_new1107z00_1420;

																	{	/* Ast/labels.scm 65 */
																		BgL_letzd2funzd2_bglt BgL_new1106z00_1422;

																		BgL_new1106z00_1422 =
																			((BgL_letzd2funzd2_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_letzd2funzd2_bgl))));
																		{	/* Ast/labels.scm 65 */
																			long BgL_arg1317z00_1423;

																			BgL_arg1317z00_1423 =
																				BGL_CLASS_NUM
																				(BGl_letzd2funzd2zzast_nodez00);
																			BGL_OBJECT_CLASS_NUM_SET((
																					(BgL_objectz00_bglt)
																					BgL_new1106z00_1422),
																				BgL_arg1317z00_1423);
																		}
																		{	/* Ast/labels.scm 65 */
																			BgL_objectz00_bglt BgL_tmpz00_1877;

																			BgL_tmpz00_1877 =
																				((BgL_objectz00_bglt)
																				BgL_new1106z00_1422);
																			BGL_OBJECT_WIDENING_SET(BgL_tmpz00_1877,
																				BFALSE);
																		}
																		((BgL_objectz00_bglt) BgL_new1106z00_1422);
																		BgL_new1107z00_1420 = BgL_new1106z00_1422;
																	}
																	((((BgL_nodez00_bglt) COBJECT(
																					((BgL_nodez00_bglt)
																						BgL_new1107z00_1420)))->
																			BgL_locz00) =
																		((obj_t) BgL_locz00_1409), BUNSPEC);
																	{
																		BgL_typez00_bglt BgL_auxz00_1883;

																		{	/* Ast/labels.scm 66 */
																			BgL_typez00_bglt BgL_arg1316z00_1421;

																			BgL_arg1316z00_1421 =
																				(((BgL_nodez00_bglt)
																					COBJECT(BgL_bodyz00_1408))->
																				BgL_typez00);
																			BgL_auxz00_1883 =
																				BGl_strictzd2nodezd2typez00zzast_nodez00
																				(((BgL_typez00_bglt)
																					BGl_za2_za2z00zztype_cachez00),
																				BgL_arg1316z00_1421);
																		}
																		((((BgL_nodez00_bglt) COBJECT(
																						((BgL_nodez00_bglt)
																							BgL_new1107z00_1420)))->
																				BgL_typez00) =
																			((BgL_typez00_bglt) BgL_auxz00_1883),
																			BUNSPEC);
																	}
																	((((BgL_nodezf2effectzf2_bglt) COBJECT(
																					((BgL_nodezf2effectzf2_bglt)
																						BgL_new1107z00_1420)))->
																			BgL_sidezd2effectzd2) =
																		((obj_t) BUNSPEC), BUNSPEC);
																	((((BgL_nodezf2effectzf2_bglt)
																				COBJECT(((BgL_nodezf2effectzf2_bglt)
																						BgL_new1107z00_1420)))->
																			BgL_keyz00) =
																		((obj_t) BINT(-1L)), BUNSPEC);
																	((((BgL_letzd2funzd2_bglt)
																				COBJECT(BgL_new1107z00_1420))->
																			BgL_localsz00) =
																		((obj_t) BgL_localsz00_1406), BUNSPEC);
																	((((BgL_letzd2funzd2_bglt)
																				COBJECT(BgL_new1107z00_1420))->
																			BgL_bodyz00) =
																		((BgL_nodez00_bglt) BgL_bodyz00_1408),
																		BUNSPEC);
																	return BgL_new1107z00_1420;
																}
															}
														}
													}
												}
											}
										}
									else
										{
											BgL_nodez00_bglt BgL_auxz00_1897;

										BgL_tagzd2360zd2_1398:
											BgL_auxz00_1897 =
												BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00
												(BGl_string1697z00zzast_labelsz00, BgL_expz00_18,
												BgL_locz00_1394);
											return ((BgL_letzd2funzd2_bglt) BgL_auxz00_1897);
										}
								}
							else
								{
									BgL_nodez00_bglt BgL_auxz00_1900;

									goto BgL_tagzd2360zd2_1398;
									return ((BgL_letzd2funzd2_bglt) BgL_auxz00_1900);
								}
						}
					else
						{
							BgL_nodez00_bglt BgL_auxz00_1902;

							goto BgL_tagzd2360zd2_1398;
							return ((BgL_letzd2funzd2_bglt) BgL_auxz00_1902);
						}
				}
			}
		}

	}



/* &labels->node */
	BgL_letzd2funzd2_bglt BGl_z62labelszd2ze3nodez53zzast_labelsz00(obj_t
		BgL_envz00_1771, obj_t BgL_expz00_1772, obj_t BgL_stackz00_1773,
		obj_t BgL_locz00_1774, obj_t BgL_sitez00_1775)
	{
		{	/* Ast/labels.scm 51 */
			return
				BGl_labelszd2ze3nodez31zzast_labelsz00(BgL_expz00_1772,
				BgL_stackz00_1773, BgL_locz00_1774, BgL_sitez00_1775);
		}

	}



/* allocate-sfuns */
	obj_t BGl_allocatezd2sfunszd2zzast_labelsz00(obj_t BgL_bindingsz00_22,
		obj_t BgL_locz00_23)
	{
		{	/* Ast/labels.scm 75 */
			{
				obj_t BgL_bindingsz00_1427;
				obj_t BgL_resz00_1428;

				BgL_bindingsz00_1427 = BgL_bindingsz00_22;
				BgL_resz00_1428 = BNIL;
			BgL_zc3z04anonymousza31319ze3z87_1429:
				if (NULLP(BgL_bindingsz00_1427))
					{	/* Ast/labels.scm 78 */
						return bgl_reverse_bang(BgL_resz00_1428);
					}
				else
					{	/* Ast/labels.scm 80 */
						obj_t BgL_srcz00_1431;

						BgL_srcz00_1431 = CAR(((obj_t) BgL_bindingsz00_1427));
						{
							obj_t BgL_funz00_1432;
							obj_t BgL_argsz00_1433;
							obj_t BgL_bodyz00_1434;

							if (PAIRP(BgL_srcz00_1431))
								{	/* Ast/labels.scm 81 */
									obj_t BgL_carzd2385zd2_1439;
									obj_t BgL_cdrzd2386zd2_1440;

									BgL_carzd2385zd2_1439 = CAR(((obj_t) BgL_srcz00_1431));
									BgL_cdrzd2386zd2_1440 = CDR(((obj_t) BgL_srcz00_1431));
									if (SYMBOLP(BgL_carzd2385zd2_1439))
										{	/* Ast/labels.scm 81 */
											if (PAIRP(BgL_cdrzd2386zd2_1440))
												{	/* Ast/labels.scm 81 */
													BgL_funz00_1432 = BgL_carzd2385zd2_1439;
													BgL_argsz00_1433 = CAR(BgL_cdrzd2386zd2_1440);
													BgL_bodyz00_1434 = CDR(BgL_cdrzd2386zd2_1440);
													{	/* Ast/labels.scm 83 */
														obj_t BgL_locz00_1445;

														{	/* Ast/labels.scm 83 */
															obj_t BgL_arg1329z00_1449;

															BgL_arg1329z00_1449 =
																CAR(((obj_t) BgL_bindingsz00_1427));
															BgL_locz00_1445 =
																BGl_findzd2locationzf2locz20zztools_locationz00
																(BgL_arg1329z00_1449, BgL_locz00_23);
														}
														{	/* Ast/labels.scm 83 */
															obj_t BgL_funz00_1446;

															BgL_funz00_1446 =
																BGl_makezd2localzd2nooptzd2sfunzd2zzast_labelsz00
																(BgL_locz00_1445, BgL_srcz00_1431,
																BgL_funz00_1432, BgL_argsz00_1433,
																BgL_bodyz00_1434);
															{	/* Ast/labels.scm 84 */

																{	/* Ast/labels.scm 85 */
																	obj_t BgL_arg1327z00_1447;
																	obj_t BgL_arg1328z00_1448;

																	BgL_arg1327z00_1447 =
																		CDR(((obj_t) BgL_bindingsz00_1427));
																	BgL_arg1328z00_1448 =
																		MAKE_YOUNG_PAIR(BgL_funz00_1446,
																		BgL_resz00_1428);
																	{
																		obj_t BgL_resz00_1928;
																		obj_t BgL_bindingsz00_1927;

																		BgL_bindingsz00_1927 = BgL_arg1327z00_1447;
																		BgL_resz00_1928 = BgL_arg1328z00_1448;
																		BgL_resz00_1428 = BgL_resz00_1928;
																		BgL_bindingsz00_1427 = BgL_bindingsz00_1927;
																		goto BgL_zc3z04anonymousza31319ze3z87_1429;
																	}
																}
															}
														}
													}
												}
											else
												{	/* Ast/labels.scm 81 */
												BgL_tagzd2377zd2_1436:
													BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00
														(BGl_string1698z00zzast_labelsz00, BgL_srcz00_1431,
														BgL_locz00_23);
													return BNIL;
												}
										}
									else
										{	/* Ast/labels.scm 81 */
											goto BgL_tagzd2377zd2_1436;
										}
								}
							else
								{	/* Ast/labels.scm 81 */
									goto BgL_tagzd2377zd2_1436;
								}
						}
					}
			}
		}

	}



/* make-local-noopt-sfun */
	obj_t BGl_makezd2localzd2nooptzd2sfunzd2zzast_labelsz00(obj_t BgL_locz00_24,
		obj_t BgL_srcz00_25, obj_t BgL_funz00_26, obj_t BgL_argsz00_27,
		obj_t BgL_bodyz00_28)
	{
		{	/* Ast/labels.scm 93 */
			{	/* Ast/labels.scm 94 */
				obj_t BgL_idzd2typezd2_1451;

				BgL_idzd2typezd2_1451 =
					BGl_parsezd2idzd2zzast_identz00(BgL_funz00_26, BgL_locz00_24);
				{	/* Ast/labels.scm 94 */
					obj_t BgL_idz00_1452;

					BgL_idz00_1452 = CAR(BgL_idzd2typezd2_1451);
					{	/* Ast/labels.scm 95 */
						obj_t BgL_typez00_1453;

						BgL_typez00_1453 = CDR(BgL_idzd2typezd2_1451);
						{	/* Ast/labels.scm 96 */
							long BgL_arityz00_1454;

							BgL_arityz00_1454 =
								BGl_localzd2arityzd2zztools_argsz00(BgL_argsz00_27);
							{	/* Ast/labels.scm 97 */
								obj_t BgL_formalsz00_1455;

								{	/* Ast/labels.scm 98 */
									obj_t BgL_l1232z00_1509;

									BgL_l1232z00_1509 =
										BGl_dssslzd2argsza2zd2ze3argszd2listz93zztools_dssslz00
										(BgL_argsz00_27);
									if (NULLP(BgL_l1232z00_1509))
										{	/* Ast/labels.scm 98 */
											BgL_formalsz00_1455 = BNIL;
										}
									else
										{	/* Ast/labels.scm 98 */
											obj_t BgL_head1234z00_1511;

											BgL_head1234z00_1511 = MAKE_YOUNG_PAIR(BNIL, BNIL);
											{
												obj_t BgL_l1232z00_1513;
												obj_t BgL_tail1235z00_1514;

												BgL_l1232z00_1513 = BgL_l1232z00_1509;
												BgL_tail1235z00_1514 = BgL_head1234z00_1511;
											BgL_zc3z04anonymousza31383ze3z87_1515:
												if (NULLP(BgL_l1232z00_1513))
													{	/* Ast/labels.scm 98 */
														BgL_formalsz00_1455 = CDR(BgL_head1234z00_1511);
													}
												else
													{	/* Ast/labels.scm 98 */
														obj_t BgL_newtail1236z00_1517;

														{	/* Ast/labels.scm 98 */
															obj_t BgL_arg1410z00_1519;

															{	/* Ast/labels.scm 98 */
																obj_t BgL_az00_1520;

																BgL_az00_1520 =
																	CAR(((obj_t) BgL_l1232z00_1513));
																BgL_arg1410z00_1519 =
																	BGl_parsezd2idzd2zzast_identz00(BgL_az00_1520,
																	BgL_locz00_24);
															}
															BgL_newtail1236z00_1517 =
																MAKE_YOUNG_PAIR(BgL_arg1410z00_1519, BNIL);
														}
														SET_CDR(BgL_tail1235z00_1514,
															BgL_newtail1236z00_1517);
														{	/* Ast/labels.scm 98 */
															obj_t BgL_arg1408z00_1518;

															BgL_arg1408z00_1518 =
																CDR(((obj_t) BgL_l1232z00_1513));
															{
																obj_t BgL_tail1235z00_1951;
																obj_t BgL_l1232z00_1950;

																BgL_l1232z00_1950 = BgL_arg1408z00_1518;
																BgL_tail1235z00_1951 = BgL_newtail1236z00_1517;
																BgL_tail1235z00_1514 = BgL_tail1235z00_1951;
																BgL_l1232z00_1513 = BgL_l1232z00_1950;
																goto BgL_zc3z04anonymousza31383ze3z87_1515;
															}
														}
													}
											}
										}
								}
								{	/* Ast/labels.scm 98 */

									{	/* Ast/labels.scm 102 */
										bool_t BgL_test1729z00_1952;

										if ((BgL_arityz00_1454 >= 0L))
											{	/* Ast/labels.scm 102 */
												BgL_test1729z00_1952 = ((bool_t) 1);
											}
										else
											{	/* Ast/labels.scm 102 */
												if (BGl_dssslzd2arityzd2za7erozf3z54zztools_argsz00(
														(int) (BgL_arityz00_1454), BgL_formalsz00_1455))
													{	/* Ast/labels.scm 106 */
														BgL_test1729z00_1952 = ((bool_t) 1);
													}
												else
													{	/* Ast/labels.scm 107 */
														obj_t BgL_largz00_1506;

														BgL_largz00_1506 =
															CAR
															(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00
															(BgL_formalsz00_1455));
														{	/* Ast/labels.scm 107 */
															obj_t BgL_typez00_1507;

															BgL_typez00_1507 =
																CDR(((obj_t) BgL_largz00_1506));
															{	/* Ast/labels.scm 108 */

																if (
																	(BgL_typez00_1507 ==
																		BGl_za2objza2z00zztype_cachez00))
																	{	/* Ast/labels.scm 110 */
																		BgL_test1729z00_1952 = ((bool_t) 1);
																	}
																else
																	{	/* Ast/labels.scm 110 */
																		if (
																			(BgL_typez00_1507 ==
																				BGl_za2_za2z00zztype_cachez00))
																			{	/* Ast/labels.scm 112 */
																				{	/* Ast/labels.scm 113 */
																					obj_t BgL_objz00_1736;

																					BgL_objz00_1736 =
																						BGl_za2objza2z00zztype_cachez00;
																					{	/* Ast/labels.scm 113 */
																						obj_t BgL_tmpz00_1966;

																						BgL_tmpz00_1966 =
																							((obj_t) BgL_largz00_1506);
																						SET_CDR(BgL_tmpz00_1966,
																							BgL_objz00_1736);
																					}
																				}
																				BgL_test1729z00_1952 = ((bool_t) 1);
																			}
																		else
																			{	/* Ast/labels.scm 112 */
																				BgL_test1729z00_1952 = ((bool_t) 0);
																			}
																	}
															}
														}
													}
											}
										if (BgL_test1729z00_1952)
											{	/* Ast/labels.scm 117 */
												obj_t BgL_argsz00_1462;

												if (NULLP(BgL_formalsz00_1455))
													{	/* Ast/labels.scm 117 */
														BgL_argsz00_1462 = BNIL;
													}
												else
													{	/* Ast/labels.scm 117 */
														obj_t BgL_head1239z00_1486;

														BgL_head1239z00_1486 = MAKE_YOUNG_PAIR(BNIL, BNIL);
														{
															obj_t BgL_l1237z00_1488;
															obj_t BgL_tail1240z00_1489;

															BgL_l1237z00_1488 = BgL_formalsz00_1455;
															BgL_tail1240z00_1489 = BgL_head1239z00_1486;
														BgL_zc3z04anonymousza31351ze3z87_1490:
															if (NULLP(BgL_l1237z00_1488))
																{	/* Ast/labels.scm 117 */
																	BgL_argsz00_1462 = CDR(BgL_head1239z00_1486);
																}
															else
																{	/* Ast/labels.scm 117 */
																	obj_t BgL_newtail1241z00_1492;

																	{	/* Ast/labels.scm 117 */
																		BgL_localz00_bglt BgL_arg1364z00_1494;

																		{	/* Ast/labels.scm 117 */
																			obj_t BgL_fz00_1495;

																			BgL_fz00_1495 =
																				CAR(((obj_t) BgL_l1237z00_1488));
																			{	/* Ast/labels.scm 118 */
																				bool_t BgL_test1738z00_1977;

																				{	/* Ast/labels.scm 118 */
																					obj_t BgL_arg1377z00_1502;

																					BgL_arg1377z00_1502 =
																						CAR(((obj_t) BgL_fz00_1495));
																					BgL_test1738z00_1977 =
																						BGl_userzd2symbolzf3z21zzast_identz00
																						(BgL_arg1377z00_1502);
																				}
																				if (BgL_test1738z00_1977)
																					{	/* Ast/labels.scm 119 */
																						obj_t BgL_arg1370z00_1498;
																						obj_t BgL_arg1371z00_1499;

																						BgL_arg1370z00_1498 =
																							CAR(((obj_t) BgL_fz00_1495));
																						BgL_arg1371z00_1499 =
																							CDR(((obj_t) BgL_fz00_1495));
																						BgL_arg1364z00_1494 =
																							BGl_makezd2userzd2localzd2svarzd2zzast_localz00
																							(BgL_arg1370z00_1498,
																							((BgL_typez00_bglt)
																								BgL_arg1371z00_1499));
																					}
																				else
																					{	/* Ast/labels.scm 120 */
																						obj_t BgL_arg1375z00_1500;
																						obj_t BgL_arg1376z00_1501;

																						BgL_arg1375z00_1500 =
																							CAR(((obj_t) BgL_fz00_1495));
																						BgL_arg1376z00_1501 =
																							CDR(((obj_t) BgL_fz00_1495));
																						BgL_arg1364z00_1494 =
																							BGl_makezd2localzd2svarz00zzast_localz00
																							(BgL_arg1375z00_1500,
																							((BgL_typez00_bglt)
																								BgL_arg1376z00_1501));
																					}
																			}
																		}
																		BgL_newtail1241z00_1492 =
																			MAKE_YOUNG_PAIR(
																			((obj_t) BgL_arg1364z00_1494), BNIL);
																	}
																	SET_CDR(BgL_tail1240z00_1489,
																		BgL_newtail1241z00_1492);
																	{	/* Ast/labels.scm 117 */
																		obj_t BgL_arg1361z00_1493;

																		BgL_arg1361z00_1493 =
																			CDR(((obj_t) BgL_l1237z00_1488));
																		{
																			obj_t BgL_tail1240z00_1999;
																			obj_t BgL_l1237z00_1998;

																			BgL_l1237z00_1998 = BgL_arg1361z00_1493;
																			BgL_tail1240z00_1999 =
																				BgL_newtail1241z00_1492;
																			BgL_tail1240z00_1489 =
																				BgL_tail1240z00_1999;
																			BgL_l1237z00_1488 = BgL_l1237z00_1998;
																			goto
																				BgL_zc3z04anonymousza31351ze3z87_1490;
																		}
																	}
																}
														}
													}
												{	/* Ast/labels.scm 117 */
													BgL_sfunz00_bglt BgL_sfunz00_1463;

													{	/* Ast/labels.scm 122 */
														BgL_sfunz00_bglt BgL_new1110z00_1465;

														{	/* Ast/labels.scm 124 */
															BgL_sfunz00_bglt BgL_new1109z00_1482;

															BgL_new1109z00_1482 =
																((BgL_sfunz00_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_sfunz00_bgl))));
															{	/* Ast/labels.scm 124 */
																long BgL_arg1348z00_1483;

																BgL_arg1348z00_1483 =
																	BGL_CLASS_NUM(BGl_sfunz00zzast_varz00);
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1109z00_1482),
																	BgL_arg1348z00_1483);
															}
															{	/* Ast/labels.scm 124 */
																BgL_objectz00_bglt BgL_tmpz00_2004;

																BgL_tmpz00_2004 =
																	((BgL_objectz00_bglt) BgL_new1109z00_1482);
																BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2004,
																	BFALSE);
															}
															((BgL_objectz00_bglt) BgL_new1109z00_1482);
															BgL_new1110z00_1465 = BgL_new1109z00_1482;
														}
														((((BgL_funz00_bglt) COBJECT(
																		((BgL_funz00_bglt) BgL_new1110z00_1465)))->
																BgL_arityz00) =
															((long) BgL_arityz00_1454), BUNSPEC);
														((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																			BgL_new1110z00_1465)))->
																BgL_sidezd2effectzd2) =
															((obj_t) BUNSPEC), BUNSPEC);
														((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																			BgL_new1110z00_1465)))->
																BgL_predicatezd2ofzd2) =
															((obj_t) BFALSE), BUNSPEC);
														((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																			BgL_new1110z00_1465)))->
																BgL_stackzd2allocatorzd2) =
															((obj_t) BFALSE), BUNSPEC);
														((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																			BgL_new1110z00_1465)))->BgL_topzf3zf3) =
															((bool_t) ((bool_t) 1)), BUNSPEC);
														((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																			BgL_new1110z00_1465)))->
																BgL_thezd2closurezd2) =
															((obj_t) BUNSPEC), BUNSPEC);
														((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																			BgL_new1110z00_1465)))->BgL_effectz00) =
															((obj_t) BUNSPEC), BUNSPEC);
														((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																			BgL_new1110z00_1465)))->BgL_failsafez00) =
															((obj_t) BUNSPEC), BUNSPEC);
														((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																			BgL_new1110z00_1465)))->
																BgL_argszd2noescapezd2) =
															((obj_t) BNIL), BUNSPEC);
														((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
																			BgL_new1110z00_1465)))->
																BgL_argszd2retescapezd2) =
															((obj_t) BNIL), BUNSPEC);
														((((BgL_sfunz00_bglt)
																	COBJECT(BgL_new1110z00_1465))->
																BgL_propertyz00) = ((obj_t) BNIL), BUNSPEC);
														((((BgL_sfunz00_bglt)
																	COBJECT(BgL_new1110z00_1465))->BgL_argsz00) =
															((obj_t) BgL_argsz00_1462), BUNSPEC);
														{
															obj_t BgL_auxz00_2030;

															if (NULLP(BgL_formalsz00_1455))
																{	/* Ast/labels.scm 127 */
																	BgL_auxz00_2030 = BNIL;
																}
															else
																{	/* Ast/labels.scm 127 */
																	obj_t BgL_head1244z00_1468;

																	{	/* Ast/labels.scm 127 */
																		obj_t BgL_arg1343z00_1480;

																		{	/* Ast/labels.scm 127 */
																			obj_t BgL_pairz00_1751;

																			BgL_pairz00_1751 =
																				CAR(((obj_t) BgL_formalsz00_1455));
																			BgL_arg1343z00_1480 =
																				CAR(BgL_pairz00_1751);
																		}
																		BgL_head1244z00_1468 =
																			MAKE_YOUNG_PAIR(BgL_arg1343z00_1480,
																			BNIL);
																	}
																	{	/* Ast/labels.scm 127 */
																		obj_t BgL_g1247z00_1469;

																		BgL_g1247z00_1469 =
																			CDR(((obj_t) BgL_formalsz00_1455));
																		{
																			obj_t BgL_l1242z00_1471;
																			obj_t BgL_tail1245z00_1472;

																			BgL_l1242z00_1471 = BgL_g1247z00_1469;
																			BgL_tail1245z00_1472 =
																				BgL_head1244z00_1468;
																		BgL_zc3z04anonymousza31337ze3z87_1473:
																			if (NULLP(BgL_l1242z00_1471))
																				{	/* Ast/labels.scm 127 */
																					BgL_auxz00_2030 =
																						BgL_head1244z00_1468;
																				}
																			else
																				{	/* Ast/labels.scm 127 */
																					obj_t BgL_newtail1246z00_1475;

																					{	/* Ast/labels.scm 127 */
																						obj_t BgL_arg1340z00_1477;

																						{	/* Ast/labels.scm 127 */
																							obj_t BgL_pairz00_1754;

																							BgL_pairz00_1754 =
																								CAR(
																								((obj_t) BgL_l1242z00_1471));
																							BgL_arg1340z00_1477 =
																								CAR(BgL_pairz00_1754);
																						}
																						BgL_newtail1246z00_1475 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1340z00_1477, BNIL);
																					}
																					SET_CDR(BgL_tail1245z00_1472,
																						BgL_newtail1246z00_1475);
																					{	/* Ast/labels.scm 127 */
																						obj_t BgL_arg1339z00_1476;

																						BgL_arg1339z00_1476 =
																							CDR(((obj_t) BgL_l1242z00_1471));
																						{
																							obj_t BgL_tail1245z00_2049;
																							obj_t BgL_l1242z00_2048;

																							BgL_l1242z00_2048 =
																								BgL_arg1339z00_1476;
																							BgL_tail1245z00_2049 =
																								BgL_newtail1246z00_1475;
																							BgL_tail1245z00_1472 =
																								BgL_tail1245z00_2049;
																							BgL_l1242z00_1471 =
																								BgL_l1242z00_2048;
																							goto
																								BgL_zc3z04anonymousza31337ze3z87_1473;
																						}
																					}
																				}
																		}
																	}
																}
															((((BgL_sfunz00_bglt)
																		COBJECT(BgL_new1110z00_1465))->
																	BgL_argszd2namezd2) =
																((obj_t) BgL_auxz00_2030), BUNSPEC);
														}
														((((BgL_sfunz00_bglt)
																	COBJECT(BgL_new1110z00_1465))->BgL_bodyz00) =
															((obj_t) BUNSPEC), BUNSPEC);
														((((BgL_sfunz00_bglt)
																	COBJECT(BgL_new1110z00_1465))->BgL_classz00) =
															((obj_t) CNST_TABLE_REF(1)), BUNSPEC);
														((((BgL_sfunz00_bglt)
																	COBJECT(BgL_new1110z00_1465))->
																BgL_dssslzd2keywordszd2) =
															((obj_t) BNIL), BUNSPEC);
														((((BgL_sfunz00_bglt)
																	COBJECT(BgL_new1110z00_1465))->BgL_locz00) =
															((obj_t)
																BGl_findzd2locationzf2locz20zztools_locationz00
																(BgL_bodyz00_28, BgL_locz00_24)), BUNSPEC);
														((((BgL_sfunz00_bglt)
																	COBJECT(BgL_new1110z00_1465))->
																BgL_optionalsz00) = ((obj_t) BNIL), BUNSPEC);
														((((BgL_sfunz00_bglt)
																	COBJECT(BgL_new1110z00_1465))->BgL_keysz00) =
															((obj_t) BNIL), BUNSPEC);
														((((BgL_sfunz00_bglt)
																	COBJECT(BgL_new1110z00_1465))->
																BgL_thezd2closurezd2globalz00) =
															((obj_t) BUNSPEC), BUNSPEC);
														((((BgL_sfunz00_bglt)
																	COBJECT(BgL_new1110z00_1465))->
																BgL_strengthz00) =
															((obj_t) CNST_TABLE_REF(2)), BUNSPEC);
														((((BgL_sfunz00_bglt)
																	COBJECT(BgL_new1110z00_1465))->
																BgL_stackablez00) = ((obj_t) BUNSPEC), BUNSPEC);
														BgL_sfunz00_1463 = BgL_new1110z00_1465;
													}
													{	/* Ast/labels.scm 122 */

														if (BGl_userzd2symbolzf3z21zzast_identz00
															(BgL_idz00_1452))
															{	/* Ast/labels.scm 128 */
																return
																	((obj_t)
																	BGl_makezd2userzd2localzd2sfunzd2zzast_localz00
																	(BgL_idz00_1452,
																		((BgL_typez00_bglt) BgL_typez00_1453),
																		BgL_sfunz00_1463));
															}
														else
															{	/* Ast/labels.scm 128 */
																return
																	((obj_t)
																	BGl_makezd2localzd2sfunz00zzast_localz00
																	(BgL_idz00_1452,
																		((BgL_typez00_bglt) BgL_typez00_1453),
																		BgL_sfunz00_1463));
															}
													}
												}
											}
										else
											{	/* Ast/labels.scm 102 */
												BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00
													(BGl_string1699z00zzast_labelsz00, BgL_srcz00_25,
													BgL_locz00_24);
												return BNIL;
											}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* labels-binding */
	obj_t BGl_labelszd2bindingzd2zzast_labelsz00(obj_t BgL_localz00_29,
		obj_t BgL_bindingz00_30, obj_t BgL_stackz00_31, obj_t BgL_locz00_32)
	{
		{	/* Ast/labels.scm 138 */
			{
				obj_t BgL_argsz00_1522;
				obj_t BgL_bodyz00_1523;

				if (PAIRP(BgL_bindingz00_30))
					{	/* Ast/labels.scm 139 */
						obj_t BgL_cdrzd2404zd2_1528;

						BgL_cdrzd2404zd2_1528 = CDR(((obj_t) BgL_bindingz00_30));
						if (PAIRP(BgL_cdrzd2404zd2_1528))
							{	/* Ast/labels.scm 139 */
								BgL_argsz00_1522 = CAR(BgL_cdrzd2404zd2_1528);
								BgL_bodyz00_1523 = CDR(BgL_cdrzd2404zd2_1528);
								{	/* Ast/labels.scm 141 */
									obj_t BgL_arg1434z00_1532;

									BgL_arg1434z00_1532 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_localz00_29))))->BgL_idz00);
									BGl_enterzd2functionzd2zztools_errorz00(BgL_arg1434z00_1532);
								}
								{	/* Ast/labels.scm 142 */
									obj_t BgL_locz00_1533;

									BgL_locz00_1533 =
										BGl_findzd2locationzf2locz20zztools_locationz00
										(BgL_bindingz00_30, BgL_locz00_32);
									{	/* Ast/labels.scm 142 */
										obj_t BgL_body2z00_1534;

										{	/* Ast/labels.scm 143 */
											obj_t BgL_arg1473z00_1541;
											obj_t BgL_arg1485z00_1542;

											BgL_arg1473z00_1541 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_localz00_bglt) BgL_localz00_29))))->
												BgL_idz00);
											BgL_arg1485z00_1542 =
												BGl_normaliza7ezd2prognz75zztools_prognz00
												(BgL_bodyz00_1523);
											BgL_body2z00_1534 =
												BGl_makezd2dssslzd2functionzd2preludezd2zz__dssslz00
												(BgL_arg1473z00_1541, BgL_argsz00_1522,
												BgL_arg1485z00_1542,
												BGl_userzd2errorzd2envz00zztools_errorz00);
										}
										{	/* Ast/labels.scm 143 */
											BgL_nodez00_bglt BgL_body3z00_1535;

											{	/* Ast/labels.scm 148 */
												obj_t BgL_arg1453z00_1538;

												{	/* Ast/labels.scm 148 */
													obj_t BgL_arg1454z00_1539;

													BgL_arg1454z00_1539 =
														(((BgL_sfunz00_bglt) COBJECT(
																((BgL_sfunz00_bglt)
																	(((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					((BgL_localz00_bglt)
																						BgL_localz00_29))))->
																		BgL_valuez00))))->BgL_argsz00);
													BgL_arg1453z00_1538 =
														BGl_appendzd221011zd2zzast_labelsz00
														(BgL_arg1454z00_1539, BgL_stackz00_31);
												}
												BgL_body3z00_1535 =
													BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_body2z00_1534,
													BgL_arg1453z00_1538, BgL_locz00_1533,
													CNST_TABLE_REF(3));
											}
											{	/* Ast/labels.scm 147 */

												{	/* Ast/labels.scm 151 */
													BgL_valuez00_bglt BgL_arg1437z00_1536;

													BgL_arg1437z00_1536 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt) BgL_localz00_29))))->
														BgL_valuez00);
													((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
																		BgL_arg1437z00_1536)))->BgL_locz00) =
														((obj_t) BgL_locz00_1533), BUNSPEC);
												}
												{	/* Ast/labels.scm 152 */
													BgL_valuez00_bglt BgL_arg1448z00_1537;

													BgL_arg1448z00_1537 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt) BgL_localz00_29))))->
														BgL_valuez00);
													((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
																		BgL_arg1448z00_1537)))->BgL_bodyz00) =
														((obj_t) ((obj_t) BgL_body3z00_1535)), BUNSPEC);
												}
												return BGl_leavezd2functionzd2zztools_errorz00();
											}
										}
									}
								}
							}
						else
							{
								BgL_nodez00_bglt BgL_auxz00_2110;

							BgL_tagzd2397zd2_1525:
								BgL_auxz00_2110 =
									BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00
									(BGl_string1700z00zzast_labelsz00, BgL_bindingz00_30,
									BgL_locz00_32);
								return ((obj_t) BgL_auxz00_2110);
							}
					}
				else
					{
						BgL_nodez00_bglt BgL_auxz00_2113;

						goto BgL_tagzd2397zd2_1525;
						return ((obj_t) BgL_auxz00_2113);
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_labelsz00(void)
	{
		{	/* Ast/labels.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_labelsz00(void)
	{
		{	/* Ast/labels.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_labelsz00(void)
	{
		{	/* Ast/labels.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_labelsz00(void)
	{
		{	/* Ast/labels.scm 14 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1701z00zzast_labelsz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1701z00zzast_labelsz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1701z00zzast_labelsz00));
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1701z00zzast_labelsz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1701z00zzast_labelsz00));
			BGl_modulezd2initializa7ationz75zztools_prognz00(301998274L,
				BSTRING_TO_STRING(BGl_string1701z00zzast_labelsz00));
			BGl_modulezd2initializa7ationz75zztools_argsz00(47102372L,
				BSTRING_TO_STRING(BGl_string1701z00zzast_labelsz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1701z00zzast_labelsz00));
			BGl_modulezd2initializa7ationz75zztools_dssslz00(275867955L,
				BSTRING_TO_STRING(BGl_string1701z00zzast_labelsz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1701z00zzast_labelsz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1701z00zzast_labelsz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1701z00zzast_labelsz00));
			return
				BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string1701z00zzast_labelsz00));
		}

	}

#ifdef __cplusplus
}
#endif
