/*===========================================================================*/
/*   (Ast/remove.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/remove.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_AST_REMOVE_TYPE_DEFINITIONS
#define BGL_AST_REMOVE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_retblockz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                  *BgL_retblockz00_bglt;

	typedef struct BgL_returnz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_retblockz00_bgl *BgL_blockz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                *BgL_returnz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;


#endif													// BGL_AST_REMOVE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_za2optimza2z00zzengine_paramz00;
	extern obj_t BGl_setqz00zzast_nodez00;
	extern obj_t BGl_returnz00zzast_nodez00;
	static obj_t BGl_requirezd2initializa7ationz75zzast_removez00 = BUNSPEC;
	static obj_t BGl_z62nodezd2removez12zd2appzd2ly1244za2zzast_removez00(obj_t,
		obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_z62nodezd2removez12za2zzast_removez00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzast_removez00(void);
	extern obj_t BGl_occurzd2nodezd2sfunz12z12zzast_occurz00(BgL_sfunz00_bglt,
		BgL_globalz00_bglt);
	extern obj_t BGl_sequencez00zzast_nodez00;
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62nodezd2removez12zd2extern1248z70zzast_removez00(obj_t,
		obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzast_removez00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_z62nodezd2removez12zd2return1276z70zzast_removez00(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2removez12zd2switch1258z70zzast_removez00(obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zzast_removez00(void);
	extern obj_t BGl_castz00zzast_nodez00;
	static obj_t BGl_nodezd2removeza2z12z62zzast_removez00(obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static obj_t BGl_z62nodezd2removez121229za2zzast_removez00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	static long BGl_za2removedzd2countza2zd2zzast_removez00 = 0L;
	static obj_t
		BGl_z62nodezd2removez12zd2setzd2exzd21270z70zzast_removez00(obj_t, obj_t);
	static obj_t BGl_z62nodezd2removez12zd2funcall1246z70zzast_removez00(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2removez12zd2kwote1236z70zzast_removez00(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2removez12zd2conditi1254z70zzast_removez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_z62nodezd2removez12zd2retbloc1274z70zzast_removez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd2removez12zd2letzd2var1268za2zzast_removez00(obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zzast_removez00(void);
	static obj_t BGl_z62nodezd2removez12zd2boxzd2set1264za2zzast_removez00(obj_t,
		obj_t);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t BGl_z62nodezd2removez12zd2cast1250z70zzast_removez00(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2removez12zd2boxzd2ref1262za2zzast_removez00(obj_t,
		obj_t);
	extern obj_t BGl_za2bdbzd2debugza2zd2zzengine_paramz00;
	static obj_t BGl_z62nodezd2removez12zd2setq1252z70zzast_removez00(obj_t,
		obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t BGl_z62nodezd2removez12zd2sequenc1238z70zzast_removez00(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2removez12zd2sync1240z70zzast_removez00(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2removez12zd2fail1256z70zzast_removez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_removezd2varzd2zzast_removez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_removezd2varzd2fromz12z12zzast_removez00(obj_t,
		BgL_variablez00_bglt);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62removezd2varzb0zzast_removez00(obj_t, obj_t, obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static obj_t BGl_globalzd2removezd2sfunz12z12zzast_removez00(obj_t);
	static obj_t BGl_z62nodezd2removez12zd2makezd2bo1260za2zzast_removez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzast_removez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_occurz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_effectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t BGl_z62nodezd2removez12zd2var1234z70zzast_removez00(obj_t,
		obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	static obj_t BGl_z62removezd2varzd2fromz12z70zzast_removez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	extern obj_t BGl_occurzd2varzd2zzast_occurz00(obj_t);
	static obj_t BGl_cnstzd2initzd2zzast_removez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_removez00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_nodezd2removez12zc0zzast_removez00(BgL_nodez00_bglt);
	static obj_t BGl_importedzd2moduleszd2initz00zzast_removez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzast_removez00(void);
	extern bool_t BGl_sidezd2effectzf3z21zzeffect_effectz00(BgL_nodez00_bglt);
	extern obj_t BGl_retblockz00zzast_nodez00;
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	static obj_t BGl_z62nodezd2removez12zd2app1242z70zzast_removez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd2removez12zd2jumpzd2ex1272za2zzast_removez00(obj_t,
		obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t BGl_conditionalz00zzast_nodez00;
	extern obj_t BGl_funcallz00zzast_nodez00;
	static obj_t BGl_z62nodezd2removez12zd2atom1232z70zzast_removez00(obj_t,
		obj_t);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t BGl_z62nodezd2removez12zd2letzd2fun1266za2zzast_removez00(obj_t,
		obj_t);
	static obj_t __cnst[7];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_removezd2varzd2fromz12zd2envzc0zzast_removez00,
		BgL_bgl_za762removeza7d2varza71921za7,
		BGl_z62removezd2varzd2fromz12z70zzast_removez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1900z00zzast_removez00,
		BgL_bgl_za762nodeza7d2remove1922z00,
		BGl_z62nodezd2removez12zd2app1242z70zzast_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1901z00zzast_removez00,
		BgL_bgl_za762nodeza7d2remove1923z00,
		BGl_z62nodezd2removez12zd2appzd2ly1244za2zzast_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1902z00zzast_removez00,
		BgL_bgl_za762nodeza7d2remove1924z00,
		BGl_z62nodezd2removez12zd2funcall1246z70zzast_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1903z00zzast_removez00,
		BgL_bgl_za762nodeza7d2remove1925z00,
		BGl_z62nodezd2removez12zd2extern1248z70zzast_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1904z00zzast_removez00,
		BgL_bgl_za762nodeza7d2remove1926z00,
		BGl_z62nodezd2removez12zd2cast1250z70zzast_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1905z00zzast_removez00,
		BgL_bgl_za762nodeza7d2remove1927z00,
		BGl_z62nodezd2removez12zd2setq1252z70zzast_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1906z00zzast_removez00,
		BgL_bgl_za762nodeza7d2remove1928z00,
		BGl_z62nodezd2removez12zd2conditi1254z70zzast_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1907z00zzast_removez00,
		BgL_bgl_za762nodeza7d2remove1929z00,
		BGl_z62nodezd2removez12zd2fail1256z70zzast_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1908z00zzast_removez00,
		BgL_bgl_za762nodeza7d2remove1930z00,
		BGl_z62nodezd2removez12zd2switch1258z70zzast_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1909z00zzast_removez00,
		BgL_bgl_za762nodeza7d2remove1931z00,
		BGl_z62nodezd2removez12zd2makezd2bo1260za2zzast_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1918z00zzast_removez00,
		BgL_bgl_string1918za700za7za7a1932za7, "ast_remove", 10);
	      DEFINE_STRING(BGl_string1919z00zzast_removez00,
		BgL_bgl_string1919za700za7za7a1933za7,
		"read node-remove!1229 done no-remove export ok now ", 51);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1910z00zzast_removez00,
		BgL_bgl_za762nodeza7d2remove1934z00,
		BGl_z62nodezd2removez12zd2boxzd2ref1262za2zzast_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1911z00zzast_removez00,
		BgL_bgl_za762nodeza7d2remove1935z00,
		BGl_z62nodezd2removez12zd2boxzd2set1264za2zzast_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1912z00zzast_removez00,
		BgL_bgl_za762nodeza7d2remove1936z00,
		BGl_z62nodezd2removez12zd2letzd2fun1266za2zzast_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1913z00zzast_removez00,
		BgL_bgl_za762nodeza7d2remove1937z00,
		BGl_z62nodezd2removez12zd2letzd2var1268za2zzast_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1914z00zzast_removez00,
		BgL_bgl_za762nodeza7d2remove1938z00,
		BGl_z62nodezd2removez12zd2setzd2exzd21270z70zzast_removez00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1915z00zzast_removez00,
		BgL_bgl_za762nodeza7d2remove1939z00,
		BGl_z62nodezd2removez12zd2jumpzd2ex1272za2zzast_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1916z00zzast_removez00,
		BgL_bgl_za762nodeza7d2remove1940z00,
		BGl_z62nodezd2removez12zd2retbloc1274z70zzast_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1917z00zzast_removez00,
		BgL_bgl_za762nodeza7d2remove1941z00,
		BGl_z62nodezd2removez12zd2return1276z70zzast_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1883z00zzast_removez00,
		BgL_bgl_string1883za700za7za7a1942za7, "/", 1);
	      DEFINE_STRING(BGl_string1884z00zzast_removez00,
		BgL_bgl_string1884za700za7za7a1943za7, "remove-var-from!", 16);
	      DEFINE_STRING(BGl_string1885z00zzast_removez00,
		BgL_bgl_string1885za700za7za7a1944za7, "already removable variable", 26);
	      DEFINE_STRING(BGl_string1886z00zzast_removez00,
		BgL_bgl_string1886za700za7za7a1945za7, "         removing ", 18);
	      DEFINE_STRING(BGl_string1887z00zzast_removez00,
		BgL_bgl_string1887za700za7za7a1946za7, ")", 1);
	      DEFINE_STRING(BGl_string1888z00zzast_removez00,
		BgL_bgl_string1888za700za7za7a1947za7, " (import: ", 10);
	      DEFINE_STRING(BGl_string1889z00zzast_removez00,
		BgL_bgl_string1889za700za7za7a1948za7, " removed ", 9);
	      DEFINE_STRING(BGl_string1890z00zzast_removez00,
		BgL_bgl_string1890za700za7za7a1949za7, "        ", 8);
	      DEFINE_STRING(BGl_string1892z00zzast_removez00,
		BgL_bgl_string1892za700za7za7a1950za7, "node-remove!1229", 16);
	      DEFINE_STRING(BGl_string1893z00zzast_removez00,
		BgL_bgl_string1893za700za7za7a1951za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string1895z00zzast_removez00,
		BgL_bgl_string1895za700za7za7a1952za7, "node-remove!", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1891z00zzast_removez00,
		BgL_bgl_za762nodeza7d2remove1953z00,
		BGl_z62nodezd2removez121229za2zzast_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1894z00zzast_removez00,
		BgL_bgl_za762nodeza7d2remove1954z00,
		BGl_z62nodezd2removez12zd2atom1232z70zzast_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1896z00zzast_removez00,
		BgL_bgl_za762nodeza7d2remove1955z00,
		BGl_z62nodezd2removez12zd2var1234z70zzast_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1897z00zzast_removez00,
		BgL_bgl_za762nodeza7d2remove1956z00,
		BGl_z62nodezd2removez12zd2kwote1236z70zzast_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1898z00zzast_removez00,
		BgL_bgl_za762nodeza7d2remove1957z00,
		BGl_z62nodezd2removez12zd2sequenc1238z70zzast_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1899z00zzast_removez00,
		BgL_bgl_za762nodeza7d2remove1958z00,
		BGl_z62nodezd2removez12zd2sync1240z70zzast_removez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_nodezd2removez12zd2envz12zzast_removez00,
		BgL_bgl_za762nodeza7d2remove1959z00,
		BGl_z62nodezd2removez12za2zzast_removez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_removezd2varzd2envz00zzast_removez00,
		BgL_bgl_za762removeza7d2varza71960za7,
		BGl_z62removezd2varzb0zzast_removez00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzast_removez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzast_removez00(long
		BgL_checksumz00_2287, char *BgL_fromz00_2288)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_removez00))
				{
					BGl_requirezd2initializa7ationz75zzast_removez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_removez00();
					BGl_libraryzd2moduleszd2initz00zzast_removez00();
					BGl_cnstzd2initzd2zzast_removez00();
					BGl_importedzd2moduleszd2initz00zzast_removez00();
					BGl_genericzd2initzd2zzast_removez00();
					BGl_methodzd2initzd2zzast_removez00();
					return BGl_toplevelzd2initzd2zzast_removez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_removez00(void)
	{
		{	/* Ast/remove.scm 16 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "ast_remove");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "ast_remove");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_remove");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_remove");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "ast_remove");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "ast_remove");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"ast_remove");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "ast_remove");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "ast_remove");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"ast_remove");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "ast_remove");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_removez00(void)
	{
		{	/* Ast/remove.scm 16 */
			{	/* Ast/remove.scm 16 */
				obj_t BgL_cportz00_2176;

				{	/* Ast/remove.scm 16 */
					obj_t BgL_stringz00_2183;

					BgL_stringz00_2183 = BGl_string1919z00zzast_removez00;
					{	/* Ast/remove.scm 16 */
						obj_t BgL_startz00_2184;

						BgL_startz00_2184 = BINT(0L);
						{	/* Ast/remove.scm 16 */
							obj_t BgL_endz00_2185;

							BgL_endz00_2185 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2183)));
							{	/* Ast/remove.scm 16 */

								BgL_cportz00_2176 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2183, BgL_startz00_2184, BgL_endz00_2185);
				}}}}
				{
					long BgL_iz00_2177;

					BgL_iz00_2177 = 6L;
				BgL_loopz00_2178:
					if ((BgL_iz00_2177 == -1L))
						{	/* Ast/remove.scm 16 */
							return BUNSPEC;
						}
					else
						{	/* Ast/remove.scm 16 */
							{	/* Ast/remove.scm 16 */
								obj_t BgL_arg1920z00_2179;

								{	/* Ast/remove.scm 16 */

									{	/* Ast/remove.scm 16 */
										obj_t BgL_locationz00_2181;

										BgL_locationz00_2181 = BBOOL(((bool_t) 0));
										{	/* Ast/remove.scm 16 */

											BgL_arg1920z00_2179 =
												BGl_readz00zz__readerz00(BgL_cportz00_2176,
												BgL_locationz00_2181);
										}
									}
								}
								{	/* Ast/remove.scm 16 */
									int BgL_tmpz00_2319;

									BgL_tmpz00_2319 = (int) (BgL_iz00_2177);
									CNST_TABLE_SET(BgL_tmpz00_2319, BgL_arg1920z00_2179);
							}}
							{	/* Ast/remove.scm 16 */
								int BgL_auxz00_2182;

								BgL_auxz00_2182 = (int) ((BgL_iz00_2177 - 1L));
								{
									long BgL_iz00_2324;

									BgL_iz00_2324 = (long) (BgL_auxz00_2182);
									BgL_iz00_2177 = BgL_iz00_2324;
									goto BgL_loopz00_2178;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_removez00(void)
	{
		{	/* Ast/remove.scm 16 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzast_removez00(void)
	{
		{	/* Ast/remove.scm 16 */
			BGl_za2removedzd2countza2zd2zzast_removez00 = 0L;
			return BUNSPEC;
		}

	}



/* remove-var-from! */
	BGL_EXPORTED_DEF obj_t BGl_removezd2varzd2fromz12z12zzast_removez00(obj_t
		BgL_passz00_3, BgL_variablez00_bglt BgL_varz00_4)
	{
		{	/* Ast/remove.scm 32 */
			{	/* Ast/remove.scm 33 */
				obj_t BgL_oldz00_1382;

				BgL_oldz00_1382 =
					(((BgL_variablez00_bglt) COBJECT(BgL_varz00_4))->BgL_removablez00);
				if ((BgL_oldz00_1382 == CNST_TABLE_REF(0)))
					{	/* Ast/remove.scm 35 */
						return
							((((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt)
											((BgL_globalz00_bglt) BgL_varz00_4))))->
								BgL_removablez00) = ((obj_t) BgL_passz00_3), BUNSPEC);
					}
				else
					{	/* Ast/remove.scm 35 */
						if ((BgL_oldz00_1382 == BgL_passz00_3))
							{	/* Ast/remove.scm 38 */
								return CNST_TABLE_REF(1);
							}
						else
							{	/* Ast/remove.scm 38 */
								if ((BgL_passz00_3 == CNST_TABLE_REF(0)))
									{	/* Ast/remove.scm 40 */
										return CNST_TABLE_REF(1);
									}
								else
									{	/* Ast/remove.scm 45 */
										obj_t BgL_arg1305z00_1383;

										{	/* Ast/remove.scm 45 */
											obj_t BgL_arg1306z00_1384;
											obj_t BgL_arg1307z00_1385;

											{	/* Ast/remove.scm 45 */
												obj_t BgL_arg1455z00_1848;

												BgL_arg1455z00_1848 =
													SYMBOL_TO_STRING(((obj_t) BgL_oldz00_1382));
												BgL_arg1306z00_1384 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_1848);
											}
											{	/* Ast/remove.scm 47 */
												obj_t BgL_arg1455z00_1850;

												BgL_arg1455z00_1850 = SYMBOL_TO_STRING(BgL_passz00_3);
												BgL_arg1307z00_1385 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_1850);
											}
											BgL_arg1305z00_1383 =
												string_append_3(BgL_arg1306z00_1384,
												BGl_string1883z00zzast_removez00, BgL_arg1307z00_1385);
										}
										return
											BGl_internalzd2errorzd2zztools_errorz00
											(BGl_string1884z00zzast_removez00,
											BGl_string1885z00zzast_removez00, BgL_arg1305z00_1383);
									}
							}
					}
			}
		}

	}



/* &remove-var-from! */
	obj_t BGl_z62removezd2varzd2fromz12z70zzast_removez00(obj_t BgL_envz00_2096,
		obj_t BgL_passz00_2097, obj_t BgL_varz00_2098)
	{
		{	/* Ast/remove.scm 32 */
			return
				BGl_removezd2varzd2fromz12z12zzast_removez00(BgL_passz00_2097,
				((BgL_variablez00_bglt) BgL_varz00_2098));
		}

	}



/* remove-var */
	BGL_EXPORTED_DEF obj_t BGl_removezd2varzd2zzast_removez00(obj_t BgL_passz00_5,
		obj_t BgL_globalsz00_6)
	{
		{	/* Ast/remove.scm 57 */
			BGl_occurzd2varzd2zzast_occurz00(BgL_globalsz00_6);
			{
				obj_t BgL_globalsz00_1388;
				obj_t BgL_resz00_1389;
				bool_t BgL_fixpz00_1390;

				BgL_globalsz00_1388 = BgL_globalsz00_6;
				BgL_resz00_1389 = BNIL;
				BgL_fixpz00_1390 = ((bool_t) 1);
			BgL_zc3z04anonymousza31308ze3z87_1391:
				if (NULLP(BgL_globalsz00_1388))
					{	/* Ast/remove.scm 69 */
						obj_t BgL_nglobalsz00_1393;

						BgL_nglobalsz00_1393 = bgl_reverse_bang(BgL_resz00_1389);
						BGl_occurzd2varzd2zzast_occurz00(BgL_nglobalsz00_1393);
						if (BgL_fixpz00_1390)
							{	/* Ast/remove.scm 72 */
								return BgL_nglobalsz00_1393;
							}
						else
							{
								bool_t BgL_fixpz00_2358;
								obj_t BgL_resz00_2357;
								obj_t BgL_globalsz00_2356;

								BgL_globalsz00_2356 = BgL_nglobalsz00_1393;
								BgL_resz00_2357 = BNIL;
								BgL_fixpz00_2358 = ((bool_t) 1);
								BgL_fixpz00_1390 = BgL_fixpz00_2358;
								BgL_resz00_1389 = BgL_resz00_2357;
								BgL_globalsz00_1388 = BgL_globalsz00_2356;
								goto BgL_zc3z04anonymousza31308ze3z87_1391;
							}
					}
				else
					{	/* Ast/remove.scm 75 */
						obj_t BgL_globalz00_1394;

						BgL_globalz00_1394 = CAR(((obj_t) BgL_globalsz00_1388));
						if (
							((((BgL_globalz00_bglt) COBJECT(
											((BgL_globalz00_bglt) BgL_globalz00_1394)))->
									BgL_importz00) == CNST_TABLE_REF(2)))
							{	/* Ast/remove.scm 78 */
								BGl_globalzd2removezd2sfunz12z12zzast_removez00
									(BgL_globalz00_1394);
								{	/* Ast/remove.scm 80 */
									obj_t BgL_arg1312z00_1397;
									obj_t BgL_arg1314z00_1398;

									BgL_arg1312z00_1397 = CDR(((obj_t) BgL_globalsz00_1388));
									BgL_arg1314z00_1398 =
										MAKE_YOUNG_PAIR(BgL_globalz00_1394, BgL_resz00_1389);
									{
										obj_t BgL_resz00_2371;
										obj_t BgL_globalsz00_2370;

										BgL_globalsz00_2370 = BgL_arg1312z00_1397;
										BgL_resz00_2371 = BgL_arg1314z00_1398;
										BgL_resz00_1389 = BgL_resz00_2371;
										BgL_globalsz00_1388 = BgL_globalsz00_2370;
										goto BgL_zc3z04anonymousza31308ze3z87_1391;
									}
								}
							}
						else
							{	/* Ast/remove.scm 83 */
								bool_t BgL_test1969z00_2372;

								if (
									((((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_globalz00_bglt) BgL_globalz00_1394))))->
											BgL_removablez00) == BgL_passz00_5))
									{	/* Ast/remove.scm 83 */
										BgL_test1969z00_2372 = ((bool_t) 1);
									}
								else
									{	/* Ast/remove.scm 83 */
										if (PAIRP(BgL_passz00_5))
											{	/* Ast/remove.scm 84 */
												BgL_test1969z00_2372 =
													CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
														(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_globalz00_bglt)
																			BgL_globalz00_1394))))->BgL_removablez00),
														BgL_passz00_5));
											}
										else
											{	/* Ast/remove.scm 84 */
												BgL_test1969z00_2372 = ((bool_t) 0);
											}
									}
								if (BgL_test1969z00_2372)
									{	/* Ast/remove.scm 83 */
										if (
											((((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_globalz00_bglt) BgL_globalz00_1394))))->
													BgL_occurrencez00) <= 0L))
											{	/* Ast/remove.scm 86 */
												{	/* Ast/remove.scm 90 */
													obj_t BgL_arg1322z00_1407;

													BgL_arg1322z00_1407 =
														BGl_shapez00zztools_shapez00(BgL_globalz00_1394);
													{	/* Ast/remove.scm 89 */
														obj_t BgL_list1323z00_1408;

														{	/* Ast/remove.scm 89 */
															obj_t BgL_arg1325z00_1409;

															{	/* Ast/remove.scm 89 */
																obj_t BgL_arg1326z00_1410;

																BgL_arg1326z00_1410 =
																	MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)),
																	BNIL);
																BgL_arg1325z00_1409 =
																	MAKE_YOUNG_PAIR(BgL_arg1322z00_1407,
																	BgL_arg1326z00_1410);
															}
															BgL_list1323z00_1408 =
																MAKE_YOUNG_PAIR
																(BGl_string1886z00zzast_removez00,
																BgL_arg1325z00_1409);
														}
														BGl_verbosez00zztools_speekz00(BINT(3L),
															BgL_list1323z00_1408);
												}}
												{	/* Ast/remove.scm 91 */
													obj_t BgL_arg1327z00_1411;

													BgL_arg1327z00_1411 =
														CDR(((obj_t) BgL_globalsz00_1388));
													{
														bool_t BgL_fixpz00_2400;
														obj_t BgL_globalsz00_2399;

														BgL_globalsz00_2399 = BgL_arg1327z00_1411;
														BgL_fixpz00_2400 = ((bool_t) 0);
														BgL_fixpz00_1390 = BgL_fixpz00_2400;
														BgL_globalsz00_1388 = BgL_globalsz00_2399;
														goto BgL_zc3z04anonymousza31308ze3z87_1391;
													}
												}
											}
										else
											{	/* Ast/remove.scm 86 */
												{	/* Ast/remove.scm 93 */
													obj_t BgL_vz00_1860;

													BgL_vz00_1860 = CNST_TABLE_REF(0);
													((((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_globalz00_bglt)
																			BgL_globalz00_1394))))->
															BgL_removablez00) =
														((obj_t) BgL_vz00_1860), BUNSPEC);
												}
												BGl_globalzd2removezd2sfunz12z12zzast_removez00
													(BgL_globalz00_1394);
												{	/* Ast/remove.scm 95 */
													obj_t BgL_arg1328z00_1412;
													obj_t BgL_arg1329z00_1413;

													BgL_arg1328z00_1412 =
														CDR(((obj_t) BgL_globalsz00_1388));
													BgL_arg1329z00_1413 =
														MAKE_YOUNG_PAIR(BgL_globalz00_1394,
														BgL_resz00_1389);
													{
														obj_t BgL_resz00_2410;
														obj_t BgL_globalsz00_2409;

														BgL_globalsz00_2409 = BgL_arg1328z00_1412;
														BgL_resz00_2410 = BgL_arg1329z00_1413;
														BgL_resz00_1389 = BgL_resz00_2410;
														BgL_globalsz00_1388 = BgL_globalsz00_2409;
														goto BgL_zc3z04anonymousza31308ze3z87_1391;
													}
												}
											}
									}
								else
									{	/* Ast/remove.scm 96 */
										bool_t BgL_test1973z00_2411;

										if (
											((((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_globalz00_bglt) BgL_globalz00_1394))))->
													BgL_occurrencez00) <= 0L))
											{	/* Ast/remove.scm 96 */
												if (
													((((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_globalz00_bglt)
																			BgL_globalz00_1394))))->
															BgL_removablez00) == CNST_TABLE_REF(0)))
													{	/* Ast/remove.scm 97 */
														if ((BgL_passz00_5 == CNST_TABLE_REF(3)))
															{	/* Ast/remove.scm 98 */
																BgL_test1973z00_2411 = ((bool_t) 0);
															}
														else
															{	/* Ast/remove.scm 98 */
																BgL_test1973z00_2411 = ((bool_t) 1);
															}
													}
												else
													{	/* Ast/remove.scm 97 */
														BgL_test1973z00_2411 = ((bool_t) 0);
													}
											}
										else
											{	/* Ast/remove.scm 96 */
												BgL_test1973z00_2411 = ((bool_t) 0);
											}
										if (BgL_test1973z00_2411)
											{	/* Ast/remove.scm 96 */
												{	/* Ast/remove.scm 100 */
													obj_t BgL_arg1342z00_1422;
													obj_t BgL_arg1343z00_1423;

													BgL_arg1342z00_1422 =
														BGl_shapez00zztools_shapez00(BgL_globalz00_1394);
													BgL_arg1343z00_1423 =
														(((BgL_globalz00_bglt) COBJECT(
																((BgL_globalz00_bglt) BgL_globalz00_1394)))->
														BgL_importz00);
													{	/* Ast/remove.scm 100 */
														obj_t BgL_list1344z00_1424;

														{	/* Ast/remove.scm 100 */
															obj_t BgL_arg1346z00_1425;

															{	/* Ast/remove.scm 100 */
																obj_t BgL_arg1348z00_1426;

																{	/* Ast/remove.scm 100 */
																	obj_t BgL_arg1349z00_1427;

																	{	/* Ast/remove.scm 100 */
																		obj_t BgL_arg1351z00_1428;

																		{	/* Ast/remove.scm 100 */
																			obj_t BgL_arg1352z00_1429;

																			{	/* Ast/remove.scm 100 */
																				obj_t BgL_arg1361z00_1430;

																				BgL_arg1361z00_1430 =
																					MAKE_YOUNG_PAIR(BCHAR(((unsigned char)
																							10)), BNIL);
																				BgL_arg1352z00_1429 =
																					MAKE_YOUNG_PAIR
																					(BGl_string1887z00zzast_removez00,
																					BgL_arg1361z00_1430);
																			}
																			BgL_arg1351z00_1428 =
																				MAKE_YOUNG_PAIR(BgL_arg1343z00_1423,
																				BgL_arg1352z00_1429);
																		}
																		BgL_arg1349z00_1427 =
																			MAKE_YOUNG_PAIR
																			(BGl_string1888z00zzast_removez00,
																			BgL_arg1351z00_1428);
																	}
																	BgL_arg1348z00_1426 =
																		MAKE_YOUNG_PAIR
																		(BGl_string1889z00zzast_removez00,
																		BgL_arg1349z00_1427);
																}
																BgL_arg1346z00_1425 =
																	MAKE_YOUNG_PAIR(BgL_arg1342z00_1422,
																	BgL_arg1348z00_1426);
															}
															BgL_list1344z00_1424 =
																MAKE_YOUNG_PAIR
																(BGl_string1890z00zzast_removez00,
																BgL_arg1346z00_1425);
														}
														BGl_verbosez00zztools_speekz00(BINT(3L),
															BgL_list1344z00_1424);
												}}
												{	/* Ast/remove.scm 103 */
													obj_t BgL_arg1364z00_1431;

													BgL_arg1364z00_1431 =
														CDR(((obj_t) BgL_globalsz00_1388));
													{
														bool_t BgL_fixpz00_2442;
														obj_t BgL_globalsz00_2441;

														BgL_globalsz00_2441 = BgL_arg1364z00_1431;
														BgL_fixpz00_2442 = ((bool_t) 0);
														BgL_fixpz00_1390 = BgL_fixpz00_2442;
														BgL_globalsz00_1388 = BgL_globalsz00_2441;
														goto BgL_zc3z04anonymousza31308ze3z87_1391;
													}
												}
											}
										else
											{	/* Ast/remove.scm 96 */
												BGl_globalzd2removezd2sfunz12z12zzast_removez00
													(BgL_globalz00_1394);
												{	/* Ast/remove.scm 107 */
													obj_t BgL_arg1367z00_1432;
													obj_t BgL_arg1370z00_1433;

													BgL_arg1367z00_1432 =
														CDR(((obj_t) BgL_globalsz00_1388));
													BgL_arg1370z00_1433 =
														MAKE_YOUNG_PAIR(BgL_globalz00_1394,
														BgL_resz00_1389);
													{
														obj_t BgL_resz00_2448;
														obj_t BgL_globalsz00_2447;

														BgL_globalsz00_2447 = BgL_arg1367z00_1432;
														BgL_resz00_2448 = BgL_arg1370z00_1433;
														BgL_resz00_1389 = BgL_resz00_2448;
														BgL_globalsz00_1388 = BgL_globalsz00_2447;
														goto BgL_zc3z04anonymousza31308ze3z87_1391;
													}
												}
											}
									}
							}
					}
			}
		}

	}



/* &remove-var */
	obj_t BGl_z62removezd2varzb0zzast_removez00(obj_t BgL_envz00_2099,
		obj_t BgL_passz00_2100, obj_t BgL_globalsz00_2101)
	{
		{	/* Ast/remove.scm 57 */
			return
				BGl_removezd2varzd2zzast_removez00(BgL_passz00_2100,
				BgL_globalsz00_2101);
		}

	}



/* global-remove-sfun! */
	obj_t BGl_globalzd2removezd2sfunz12z12zzast_removez00(obj_t BgL_globalz00_7)
	{
		{	/* Ast/remove.scm 112 */
		BGl_globalzd2removezd2sfunz12z12zzast_removez00:
			{	/* Ast/remove.scm 115 */
				BgL_valuez00_bglt BgL_sfunz00_1444;

				BgL_sfunz00_1444 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt)
								((BgL_globalz00_bglt) BgL_globalz00_7))))->BgL_valuez00);
				BGl_za2removedzd2countza2zd2zzast_removez00 = 0L;
				{	/* Ast/remove.scm 117 */
					obj_t BgL_arg1379z00_1445;

					{	/* Ast/remove.scm 117 */
						obj_t BgL_arg1380z00_1446;

						BgL_arg1380z00_1446 =
							(((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_sfunz00_1444)))->BgL_bodyz00);
						BgL_arg1379z00_1445 =
							BGl_nodezd2removez12zc0zzast_removez00(
							((BgL_nodez00_bglt) BgL_arg1380z00_1446));
					}
					((((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_sfunz00_1444)))->BgL_bodyz00) =
						((obj_t) BgL_arg1379z00_1445), BUNSPEC);
				}
				{	/* Ast/remove.scm 118 */
					bool_t BgL_test1977z00_2459;

					if (((long) CINT(BGl_za2optimza2z00zzengine_paramz00) >= 2L))
						{	/* Ast/remove.scm 118 */
							BgL_test1977z00_2459 =
								(BGl_za2removedzd2countza2zd2zzast_removez00 > 0L);
						}
					else
						{	/* Ast/remove.scm 118 */
							BgL_test1977z00_2459 = ((bool_t) 0);
						}
					if (BgL_test1977z00_2459)
						{	/* Ast/remove.scm 118 */
							{	/* Ast/remove.scm 121 */
								BgL_valuez00_bglt BgL_arg1408z00_1449;

								BgL_arg1408z00_1449 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_globalz00_7))))->
									BgL_valuez00);
								BGl_occurzd2nodezd2sfunz12z12zzast_occurz00(((BgL_sfunz00_bglt)
										BgL_arg1408z00_1449),
									((BgL_globalz00_bglt) BgL_globalz00_7));
							}
							{

								goto BGl_globalzd2removezd2sfunz12z12zzast_removez00;
							}
						}
					else
						{	/* Ast/remove.scm 118 */
							return BgL_globalz00_7;
						}
				}
			}
		}

	}



/* node-remove*! */
	obj_t BGl_nodezd2removeza2z12z62zzast_removez00(obj_t BgL_nodeza2za2_32)
	{
		{	/* Ast/remove.scm 356 */
		BGl_nodezd2removeza2z12z62zzast_removez00:
			if (NULLP(BgL_nodeza2za2_32))
				{	/* Ast/remove.scm 357 */
					return CNST_TABLE_REF(4);
				}
			else
				{	/* Ast/remove.scm 357 */
					{	/* Ast/remove.scm 360 */
						obj_t BgL_arg1421z00_1452;

						{	/* Ast/remove.scm 360 */
							obj_t BgL_arg1422z00_1453;

							BgL_arg1422z00_1453 = CAR(((obj_t) BgL_nodeza2za2_32));
							BgL_arg1421z00_1452 =
								BGl_nodezd2removez12zc0zzast_removez00(
								((BgL_nodez00_bglt) BgL_arg1422z00_1453));
						}
						{	/* Ast/remove.scm 360 */
							obj_t BgL_tmpz00_2477;

							BgL_tmpz00_2477 = ((obj_t) BgL_nodeza2za2_32);
							SET_CAR(BgL_tmpz00_2477, BgL_arg1421z00_1452);
						}
					}
					{	/* Ast/remove.scm 361 */
						obj_t BgL_arg1434z00_1454;

						BgL_arg1434z00_1454 = CDR(((obj_t) BgL_nodeza2za2_32));
						{
							obj_t BgL_nodeza2za2_2482;

							BgL_nodeza2za2_2482 = BgL_arg1434z00_1454;
							BgL_nodeza2za2_32 = BgL_nodeza2za2_2482;
							goto BGl_nodezd2removeza2z12z62zzast_removez00;
						}
					}
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_removez00(void)
	{
		{	/* Ast/remove.scm 16 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_removez00(void)
	{
		{	/* Ast/remove.scm 16 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_nodezd2removez12zd2envz12zzast_removez00,
				BGl_proc1891z00zzast_removez00, BGl_nodez00zzast_nodez00,
				BGl_string1892z00zzast_removez00);
		}

	}



/* &node-remove!1229 */
	obj_t BGl_z62nodezd2removez121229za2zzast_removez00(obj_t BgL_envz00_2103,
		obj_t BgL_nodez00_2104)
	{
		{	/* Ast/remove.scm 128 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(5),
				BGl_string1893z00zzast_removez00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_2104)));
		}

	}



/* node-remove! */
	BGL_EXPORTED_DEF obj_t BGl_nodezd2removez12zc0zzast_removez00(BgL_nodez00_bglt
		BgL_nodez00_8)
	{
		{	/* Ast/remove.scm 128 */
			{	/* Ast/remove.scm 128 */
				obj_t BgL_method1230z00_1459;

				{	/* Ast/remove.scm 128 */
					obj_t BgL_res1880z00_1907;

					{	/* Ast/remove.scm 128 */
						long BgL_objzd2classzd2numz00_1878;

						BgL_objzd2classzd2numz00_1878 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_8));
						{	/* Ast/remove.scm 128 */
							obj_t BgL_arg1811z00_1879;

							BgL_arg1811z00_1879 =
								PROCEDURE_REF(BGl_nodezd2removez12zd2envz12zzast_removez00,
								(int) (1L));
							{	/* Ast/remove.scm 128 */
								int BgL_offsetz00_1882;

								BgL_offsetz00_1882 = (int) (BgL_objzd2classzd2numz00_1878);
								{	/* Ast/remove.scm 128 */
									long BgL_offsetz00_1883;

									BgL_offsetz00_1883 =
										((long) (BgL_offsetz00_1882) - OBJECT_TYPE);
									{	/* Ast/remove.scm 128 */
										long BgL_modz00_1884;

										BgL_modz00_1884 =
											(BgL_offsetz00_1883 >> (int) ((long) ((int) (4L))));
										{	/* Ast/remove.scm 128 */
											long BgL_restz00_1886;

											BgL_restz00_1886 =
												(BgL_offsetz00_1883 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/remove.scm 128 */

												{	/* Ast/remove.scm 128 */
													obj_t BgL_bucketz00_1888;

													BgL_bucketz00_1888 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_1879), BgL_modz00_1884);
													BgL_res1880z00_1907 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_1888), BgL_restz00_1886);
					}}}}}}}}
					BgL_method1230z00_1459 = BgL_res1880z00_1907;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1230z00_1459, ((obj_t) BgL_nodez00_8));
			}
		}

	}



/* &node-remove! */
	obj_t BGl_z62nodezd2removez12za2zzast_removez00(obj_t BgL_envz00_2105,
		obj_t BgL_nodez00_2106)
	{
		{	/* Ast/remove.scm 128 */
			return
				BGl_nodezd2removez12zc0zzast_removez00(
				((BgL_nodez00_bglt) BgL_nodez00_2106));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_removez00(void)
	{
		{	/* Ast/remove.scm 16 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2removez12zd2envz12zzast_removez00, BGl_atomz00zzast_nodez00,
				BGl_proc1894z00zzast_removez00, BGl_string1895z00zzast_removez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2removez12zd2envz12zzast_removez00, BGl_varz00zzast_nodez00,
				BGl_proc1896z00zzast_removez00, BGl_string1895z00zzast_removez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2removez12zd2envz12zzast_removez00,
				BGl_kwotez00zzast_nodez00, BGl_proc1897z00zzast_removez00,
				BGl_string1895z00zzast_removez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2removez12zd2envz12zzast_removez00,
				BGl_sequencez00zzast_nodez00, BGl_proc1898z00zzast_removez00,
				BGl_string1895z00zzast_removez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2removez12zd2envz12zzast_removez00, BGl_syncz00zzast_nodez00,
				BGl_proc1899z00zzast_removez00, BGl_string1895z00zzast_removez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2removez12zd2envz12zzast_removez00, BGl_appz00zzast_nodez00,
				BGl_proc1900z00zzast_removez00, BGl_string1895z00zzast_removez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2removez12zd2envz12zzast_removez00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1901z00zzast_removez00,
				BGl_string1895z00zzast_removez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2removez12zd2envz12zzast_removez00,
				BGl_funcallz00zzast_nodez00, BGl_proc1902z00zzast_removez00,
				BGl_string1895z00zzast_removez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2removez12zd2envz12zzast_removez00,
				BGl_externz00zzast_nodez00, BGl_proc1903z00zzast_removez00,
				BGl_string1895z00zzast_removez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2removez12zd2envz12zzast_removez00, BGl_castz00zzast_nodez00,
				BGl_proc1904z00zzast_removez00, BGl_string1895z00zzast_removez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2removez12zd2envz12zzast_removez00, BGl_setqz00zzast_nodez00,
				BGl_proc1905z00zzast_removez00, BGl_string1895z00zzast_removez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2removez12zd2envz12zzast_removez00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1906z00zzast_removez00,
				BGl_string1895z00zzast_removez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2removez12zd2envz12zzast_removez00, BGl_failz00zzast_nodez00,
				BGl_proc1907z00zzast_removez00, BGl_string1895z00zzast_removez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2removez12zd2envz12zzast_removez00,
				BGl_switchz00zzast_nodez00, BGl_proc1908z00zzast_removez00,
				BGl_string1895z00zzast_removez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2removez12zd2envz12zzast_removez00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1909z00zzast_removez00,
				BGl_string1895z00zzast_removez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2removez12zd2envz12zzast_removez00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1910z00zzast_removez00,
				BGl_string1895z00zzast_removez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2removez12zd2envz12zzast_removez00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1911z00zzast_removez00,
				BGl_string1895z00zzast_removez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2removez12zd2envz12zzast_removez00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1912z00zzast_removez00,
				BGl_string1895z00zzast_removez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2removez12zd2envz12zzast_removez00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1913z00zzast_removez00,
				BGl_string1895z00zzast_removez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2removez12zd2envz12zzast_removez00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc1914z00zzast_removez00,
				BGl_string1895z00zzast_removez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2removez12zd2envz12zzast_removez00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc1915z00zzast_removez00,
				BGl_string1895z00zzast_removez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2removez12zd2envz12zzast_removez00,
				BGl_retblockz00zzast_nodez00, BGl_proc1916z00zzast_removez00,
				BGl_string1895z00zzast_removez00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2removez12zd2envz12zzast_removez00,
				BGl_returnz00zzast_nodez00, BGl_proc1917z00zzast_removez00,
				BGl_string1895z00zzast_removez00);
		}

	}



/* &node-remove!-return1276 */
	obj_t BGl_z62nodezd2removez12zd2return1276z70zzast_removez00(obj_t
		BgL_envz00_2130, obj_t BgL_nodez00_2131)
	{
		{	/* Ast/remove.scm 349 */
			{	/* Ast/remove.scm 350 */
				obj_t BgL_arg1762z00_2189;

				BgL_arg1762z00_2189 =
					BGl_nodezd2removez12zc0zzast_removez00(
					(((BgL_returnz00_bglt) COBJECT(
								((BgL_returnz00_bglt) BgL_nodez00_2131)))->BgL_valuez00));
				((((BgL_returnz00_bglt) COBJECT(
								((BgL_returnz00_bglt) BgL_nodez00_2131)))->BgL_valuez00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_arg1762z00_2189)),
					BUNSPEC);
			}
			return ((obj_t) ((BgL_returnz00_bglt) BgL_nodez00_2131));
		}

	}



/* &node-remove!-retbloc1274 */
	obj_t BGl_z62nodezd2removez12zd2retbloc1274z70zzast_removez00(obj_t
		BgL_envz00_2132, obj_t BgL_nodez00_2133)
	{
		{	/* Ast/remove.scm 342 */
			{	/* Ast/remove.scm 343 */
				obj_t BgL_arg1755z00_2191;

				BgL_arg1755z00_2191 =
					BGl_nodezd2removez12zc0zzast_removez00(
					(((BgL_retblockz00_bglt) COBJECT(
								((BgL_retblockz00_bglt) BgL_nodez00_2133)))->BgL_bodyz00));
				((((BgL_retblockz00_bglt) COBJECT(
								((BgL_retblockz00_bglt) BgL_nodez00_2133)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_arg1755z00_2191)),
					BUNSPEC);
			}
			return ((obj_t) ((BgL_retblockz00_bglt) BgL_nodez00_2133));
		}

	}



/* &node-remove!-jump-ex1272 */
	obj_t BGl_z62nodezd2removez12zd2jumpzd2ex1272za2zzast_removez00(obj_t
		BgL_envz00_2134, obj_t BgL_nodez00_2135)
	{
		{	/* Ast/remove.scm 334 */
			{	/* Ast/remove.scm 335 */
				obj_t BgL_arg1751z00_2193;

				BgL_arg1751z00_2193 =
					BGl_nodezd2removez12zc0zzast_removez00(
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2135)))->
						BgL_exitz00));
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(((BgL_jumpzd2exzd2itz00_bglt)
									BgL_nodez00_2135)))->BgL_exitz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_arg1751z00_2193)),
					BUNSPEC);
			}
			{	/* Ast/remove.scm 336 */
				obj_t BgL_arg1753z00_2194;

				BgL_arg1753z00_2194 =
					BGl_nodezd2removez12zc0zzast_removez00(
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2135)))->
						BgL_valuez00));
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(((BgL_jumpzd2exzd2itz00_bglt)
									BgL_nodez00_2135)))->BgL_valuez00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_arg1753z00_2194)),
					BUNSPEC);
			}
			return ((obj_t) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2135));
		}

	}



/* &node-remove!-set-ex-1270 */
	obj_t BGl_z62nodezd2removez12zd2setzd2exzd21270z70zzast_removez00(obj_t
		BgL_envz00_2136, obj_t BgL_nodez00_2137)
	{
		{	/* Ast/remove.scm 324 */
			{	/* Ast/remove.scm 325 */
				obj_t BgL_arg1735z00_2196;

				BgL_arg1735z00_2196 =
					BGl_nodezd2removez12zc0zzast_removez00(
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2137)))->BgL_bodyz00));
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2137)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_arg1735z00_2196)),
					BUNSPEC);
			}
			{	/* Ast/remove.scm 326 */
				obj_t BgL_arg1737z00_2197;

				BgL_arg1737z00_2197 =
					BGl_nodezd2removez12zc0zzast_removez00(
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2137)))->
						BgL_onexitz00));
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(((BgL_setzd2exzd2itz00_bglt)
									BgL_nodez00_2137)))->BgL_onexitz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_arg1737z00_2197)),
					BUNSPEC);
			}
			if (
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt)
										(((BgL_varz00_bglt) COBJECT(
													(((BgL_setzd2exzd2itz00_bglt) COBJECT(
																((BgL_setzd2exzd2itz00_bglt)
																	BgL_nodez00_2137)))->BgL_varz00)))->
											BgL_variablez00)))))->BgL_occurrencez00) <= 0L))
				{	/* Ast/remove.scm 327 */
					return
						((obj_t)
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
									((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2137)))->
							BgL_bodyz00));
				}
			else
				{	/* Ast/remove.scm 327 */
					return ((obj_t) ((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2137));
				}
		}

	}



/* &node-remove!-let-var1268 */
	obj_t BGl_z62nodezd2removez12zd2letzd2var1268za2zzast_removez00(obj_t
		BgL_envz00_2138, obj_t BgL_nodez00_2139)
	{
		{	/* Ast/remove.scm 285 */
			{
				BgL_nodez00_bglt BgL_auxz00_2598;

				{	/* Ast/remove.scm 287 */
					BgL_nodez00_bglt BgL_arg1689z00_2199;

					BgL_arg1689z00_2199 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2139)))->BgL_bodyz00);
					BgL_auxz00_2598 =
						((BgL_nodez00_bglt)
						BGl_nodezd2removez12zc0zzast_removez00(BgL_arg1689z00_2199));
				}
				((((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2139)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2598), BUNSPEC);
			}
			{
				obj_t BgL_oldzd2bindingszd2_2201;
				obj_t BgL_newzd2bindingszd2_2202;

				{	/* Ast/remove.scm 288 */
					obj_t BgL_arg1691z00_2227;

					BgL_arg1691z00_2227 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2139)))->BgL_bindingsz00);
					{
						BgL_nodez00_bglt BgL_auxz00_2607;

						BgL_oldzd2bindingszd2_2201 = BgL_arg1691z00_2227;
						BgL_newzd2bindingszd2_2202 = BNIL;
					BgL_loopz00_2200:
						if (NULLP(BgL_oldzd2bindingszd2_2201))
							{	/* Ast/remove.scm 290 */
								if (NULLP(BgL_newzd2bindingszd2_2202))
									{	/* Ast/remove.scm 291 */
										if (
											(((BgL_letzd2varzd2_bglt) COBJECT(
														((BgL_letzd2varzd2_bglt) BgL_nodez00_2139)))->
												BgL_removablezf3zf3))
											{	/* Ast/remove.scm 292 */
												BgL_auxz00_2607 =
													(((BgL_letzd2varzd2_bglt) COBJECT(
															((BgL_letzd2varzd2_bglt) BgL_nodez00_2139)))->
													BgL_bodyz00);
											}
										else
											{	/* Ast/remove.scm 292 */
												((((BgL_letzd2varzd2_bglt) COBJECT(
																((BgL_letzd2varzd2_bglt) BgL_nodez00_2139)))->
														BgL_bindingsz00) = ((obj_t) BNIL), BUNSPEC);
												BgL_auxz00_2607 =
													((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt)
														BgL_nodez00_2139));
											}
									}
								else
									{	/* Ast/remove.scm 291 */
										((((BgL_letzd2varzd2_bglt) COBJECT(
														((BgL_letzd2varzd2_bglt) BgL_nodez00_2139)))->
												BgL_bindingsz00) =
											((obj_t) bgl_reverse_bang(BgL_newzd2bindingszd2_2202)),
											BUNSPEC);
										BgL_auxz00_2607 =
											((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt)
												BgL_nodez00_2139));
									}
							}
						else
							{	/* Ast/remove.scm 300 */
								obj_t BgL_bindingz00_2203;

								BgL_bindingz00_2203 = CAR(((obj_t) BgL_oldzd2bindingszd2_2201));
								{	/* Ast/remove.scm 300 */
									obj_t BgL_varz00_2204;

									BgL_varz00_2204 = CAR(((obj_t) BgL_bindingz00_2203));
									{	/* Ast/remove.scm 301 */
										obj_t BgL_valz00_2205;

										{	/* Ast/remove.scm 302 */
											obj_t BgL_arg1734z00_2206;

											BgL_arg1734z00_2206 = CDR(((obj_t) BgL_bindingz00_2203));
											BgL_valz00_2205 =
												BGl_nodezd2removez12zc0zzast_removez00(
												((BgL_nodez00_bglt) BgL_arg1734z00_2206));
										}
										{	/* Ast/remove.scm 302 */

											{	/* Ast/remove.scm 303 */
												bool_t BgL_test1984z00_2634;

												if (
													((((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt) BgL_varz00_2204))))->
															BgL_occurrencez00) <= 0L))
													{	/* Ast/remove.scm 303 */
														if (BGl_sidezd2effectzf3z21zzeffect_effectz00(
																((BgL_nodez00_bglt) BgL_valz00_2205)))
															{	/* Ast/remove.scm 304 */
																BgL_test1984z00_2634 = ((bool_t) 0);
															}
														else
															{	/* Ast/remove.scm 304 */
																if (
																	((((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						((BgL_localz00_bglt)
																							BgL_varz00_2204))))->
																			BgL_accessz00) == CNST_TABLE_REF(6)))
																	{	/* Ast/remove.scm 306 */
																		bool_t BgL__ortest_1108z00_2207;

																		if (
																			(((BgL_variablez00_bglt) COBJECT(
																						((BgL_variablez00_bglt)
																							((BgL_localz00_bglt)
																								BgL_varz00_2204))))->
																				BgL_userzf3zf3))
																			{	/* Ast/remove.scm 306 */
																				BgL__ortest_1108z00_2207 = ((bool_t) 0);
																			}
																		else
																			{	/* Ast/remove.scm 306 */
																				BgL__ortest_1108z00_2207 = ((bool_t) 1);
																			}
																		if (BgL__ortest_1108z00_2207)
																			{	/* Ast/remove.scm 306 */
																				BgL_test1984z00_2634 =
																					BgL__ortest_1108z00_2207;
																			}
																		else
																			{	/* Ast/remove.scm 306 */
																				BgL_test1984z00_2634 =
																					(
																					(long)
																					CINT
																					(BGl_za2bdbzd2debugza2zd2zzengine_paramz00)
																					== 0L);
																	}}
																else
																	{	/* Ast/remove.scm 305 */
																		BgL_test1984z00_2634 = ((bool_t) 0);
																	}
															}
													}
												else
													{	/* Ast/remove.scm 303 */
														BgL_test1984z00_2634 = ((bool_t) 0);
													}
												if (BgL_test1984z00_2634)
													{	/* Ast/remove.scm 303 */
														{	/* Ast/remove.scm 309 */
															bool_t BgL_test1990z00_2656;

															{	/* Ast/remove.scm 309 */
																obj_t BgL_classz00_2208;

																BgL_classz00_2208 = BGl_varz00zzast_nodez00;
																if (BGL_OBJECTP(BgL_valz00_2205))
																	{	/* Ast/remove.scm 309 */
																		BgL_objectz00_bglt BgL_arg1807z00_2209;

																		BgL_arg1807z00_2209 =
																			(BgL_objectz00_bglt) (BgL_valz00_2205);
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Ast/remove.scm 309 */
																				long BgL_idxz00_2210;

																				BgL_idxz00_2210 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_2209);
																				BgL_test1990z00_2656 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_2210 + 2L)) ==
																					BgL_classz00_2208);
																			}
																		else
																			{	/* Ast/remove.scm 309 */
																				bool_t BgL_res1881z00_2213;

																				{	/* Ast/remove.scm 309 */
																					obj_t BgL_oclassz00_2214;

																					{	/* Ast/remove.scm 309 */
																						obj_t BgL_arg1815z00_2215;
																						long BgL_arg1816z00_2216;

																						BgL_arg1815z00_2215 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Ast/remove.scm 309 */
																							long BgL_arg1817z00_2217;

																							BgL_arg1817z00_2217 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_2209);
																							BgL_arg1816z00_2216 =
																								(BgL_arg1817z00_2217 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_2214 =
																							VECTOR_REF(BgL_arg1815z00_2215,
																							BgL_arg1816z00_2216);
																					}
																					{	/* Ast/remove.scm 309 */
																						bool_t BgL__ortest_1115z00_2218;

																						BgL__ortest_1115z00_2218 =
																							(BgL_classz00_2208 ==
																							BgL_oclassz00_2214);
																						if (BgL__ortest_1115z00_2218)
																							{	/* Ast/remove.scm 309 */
																								BgL_res1881z00_2213 =
																									BgL__ortest_1115z00_2218;
																							}
																						else
																							{	/* Ast/remove.scm 309 */
																								long BgL_odepthz00_2219;

																								{	/* Ast/remove.scm 309 */
																									obj_t BgL_arg1804z00_2220;

																									BgL_arg1804z00_2220 =
																										(BgL_oclassz00_2214);
																									BgL_odepthz00_2219 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_2220);
																								}
																								if ((2L < BgL_odepthz00_2219))
																									{	/* Ast/remove.scm 309 */
																										obj_t BgL_arg1802z00_2221;

																										{	/* Ast/remove.scm 309 */
																											obj_t BgL_arg1803z00_2222;

																											BgL_arg1803z00_2222 =
																												(BgL_oclassz00_2214);
																											BgL_arg1802z00_2221 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_2222,
																												2L);
																										}
																										BgL_res1881z00_2213 =
																											(BgL_arg1802z00_2221 ==
																											BgL_classz00_2208);
																									}
																								else
																									{	/* Ast/remove.scm 309 */
																										BgL_res1881z00_2213 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test1990z00_2656 =
																					BgL_res1881z00_2213;
																			}
																	}
																else
																	{	/* Ast/remove.scm 309 */
																		BgL_test1990z00_2656 = ((bool_t) 0);
																	}
															}
															if (BgL_test1990z00_2656)
																{	/* Ast/remove.scm 310 */
																	BgL_variablez00_bglt BgL_vz00_2223;

																	BgL_vz00_2223 =
																		(((BgL_varz00_bglt) COBJECT(
																				((BgL_varz00_bglt) BgL_valz00_2205)))->
																		BgL_variablez00);
																	((((BgL_variablez00_bglt)
																				COBJECT(BgL_vz00_2223))->
																			BgL_occurrencez00) =
																		((long) ((((BgL_variablez00_bglt)
																						COBJECT(BgL_vz00_2223))->
																					BgL_occurrencez00) - 1L)), BUNSPEC);
																}
															else
																{	/* Ast/remove.scm 309 */
																	BFALSE;
																}
														}
														BGl_za2removedzd2countza2zd2zzast_removez00 =
															(BGl_za2removedzd2countza2zd2zzast_removez00 +
															1L);
														{	/* Ast/remove.scm 315 */
															obj_t BgL_arg1718z00_2224;

															BgL_arg1718z00_2224 =
																CDR(((obj_t) BgL_oldzd2bindingszd2_2201));
															{
																obj_t BgL_oldzd2bindingszd2_2687;

																BgL_oldzd2bindingszd2_2687 =
																	BgL_arg1718z00_2224;
																BgL_oldzd2bindingszd2_2201 =
																	BgL_oldzd2bindingszd2_2687;
																goto BgL_loopz00_2200;
															}
														}
													}
												else
													{	/* Ast/remove.scm 303 */
														{	/* Ast/remove.scm 317 */
															obj_t BgL_tmpz00_2688;

															BgL_tmpz00_2688 = ((obj_t) BgL_bindingz00_2203);
															SET_CDR(BgL_tmpz00_2688, BgL_valz00_2205);
														}
														{	/* Ast/remove.scm 318 */
															obj_t BgL_arg1720z00_2225;
															obj_t BgL_arg1722z00_2226;

															BgL_arg1720z00_2225 =
																CDR(((obj_t) BgL_oldzd2bindingszd2_2201));
															BgL_arg1722z00_2226 =
																MAKE_YOUNG_PAIR(BgL_bindingz00_2203,
																BgL_newzd2bindingszd2_2202);
															{
																obj_t BgL_newzd2bindingszd2_2695;
																obj_t BgL_oldzd2bindingszd2_2694;

																BgL_oldzd2bindingszd2_2694 =
																	BgL_arg1720z00_2225;
																BgL_newzd2bindingszd2_2695 =
																	BgL_arg1722z00_2226;
																BgL_newzd2bindingszd2_2202 =
																	BgL_newzd2bindingszd2_2695;
																BgL_oldzd2bindingszd2_2201 =
																	BgL_oldzd2bindingszd2_2694;
																goto BgL_loopz00_2200;
															}
														}
													}
											}
										}
									}
								}
							}
						return ((obj_t) BgL_auxz00_2607);
					}
				}
			}
		}

	}



/* &node-remove!-let-fun1266 */
	obj_t BGl_z62nodezd2removez12zd2letzd2fun1266za2zzast_removez00(obj_t
		BgL_envz00_2140, obj_t BgL_nodez00_2141)
	{
		{	/* Ast/remove.scm 262 */
			{
				BgL_nodez00_bglt BgL_auxz00_2697;

				{	/* Ast/remove.scm 264 */
					BgL_nodez00_bglt BgL_arg1646z00_2229;

					BgL_arg1646z00_2229 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2141)))->BgL_bodyz00);
					BgL_auxz00_2697 =
						((BgL_nodez00_bglt)
						BGl_nodezd2removez12zc0zzast_removez00(BgL_arg1646z00_2229));
				}
				((((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2141)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2697), BUNSPEC);
			}
			{
				obj_t BgL_oldzd2localszd2_2231;
				obj_t BgL_newzd2localszd2_2232;

				{	/* Ast/remove.scm 265 */
					obj_t BgL_arg1650z00_2240;

					BgL_arg1650z00_2240 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2141)))->BgL_localsz00);
					{
						BgL_nodez00_bglt BgL_auxz00_2706;

						BgL_oldzd2localszd2_2231 = BgL_arg1650z00_2240;
						BgL_newzd2localszd2_2232 = BNIL;
					BgL_loopz00_2230:
						if (NULLP(BgL_oldzd2localszd2_2231))
							{	/* Ast/remove.scm 267 */
								if (NULLP(BgL_newzd2localszd2_2232))
									{	/* Ast/remove.scm 268 */
										BgL_auxz00_2706 =
											(((BgL_letzd2funzd2_bglt) COBJECT(
													((BgL_letzd2funzd2_bglt) BgL_nodez00_2141)))->
											BgL_bodyz00);
									}
								else
									{	/* Ast/remove.scm 268 */
										((((BgL_letzd2funzd2_bglt) COBJECT(
														((BgL_letzd2funzd2_bglt) BgL_nodez00_2141)))->
												BgL_localsz00) =
											((obj_t) bgl_reverse_bang(BgL_newzd2localszd2_2232)),
											BUNSPEC);
										BgL_auxz00_2706 =
											((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt)
												BgL_nodez00_2141));
									}
							}
						else
							{	/* Ast/remove.scm 273 */
								obj_t BgL_localz00_2233;

								BgL_localz00_2233 = CAR(((obj_t) BgL_oldzd2localszd2_2231));
								if (
									((((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt) BgL_localz00_2233))))->
											BgL_occurrencez00) <= 0L))
									{	/* Ast/remove.scm 274 */
										BGl_za2removedzd2countza2zd2zzast_removez00 =
											(BGl_za2removedzd2countza2zd2zzast_removez00 + 1L);
										{	/* Ast/remove.scm 277 */
											obj_t BgL_arg1661z00_2234;

											BgL_arg1661z00_2234 =
												CDR(((obj_t) BgL_oldzd2localszd2_2231));
											{
												obj_t BgL_oldzd2localszd2_2728;

												BgL_oldzd2localszd2_2728 = BgL_arg1661z00_2234;
												BgL_oldzd2localszd2_2231 = BgL_oldzd2localszd2_2728;
												goto BgL_loopz00_2230;
											}
										}
									}
								else
									{	/* Ast/remove.scm 278 */
										BgL_valuez00_bglt BgL_sfunz00_2235;

										BgL_sfunz00_2235 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt) BgL_localz00_2233))))->
											BgL_valuez00);
										{	/* Ast/remove.scm 279 */
											obj_t BgL_arg1663z00_2236;

											{	/* Ast/remove.scm 279 */
												obj_t BgL_arg1675z00_2237;

												BgL_arg1675z00_2237 =
													(((BgL_sfunz00_bglt) COBJECT(
															((BgL_sfunz00_bglt) BgL_sfunz00_2235)))->
													BgL_bodyz00);
												BgL_arg1663z00_2236 =
													BGl_nodezd2removez12zc0zzast_removez00((
														(BgL_nodez00_bglt) BgL_arg1675z00_2237));
											}
											((((BgL_sfunz00_bglt) COBJECT(
															((BgL_sfunz00_bglt) BgL_sfunz00_2235)))->
													BgL_bodyz00) =
												((obj_t) BgL_arg1663z00_2236), BUNSPEC);
										}
										{	/* Ast/remove.scm 280 */
											obj_t BgL_arg1678z00_2238;
											obj_t BgL_arg1681z00_2239;

											BgL_arg1678z00_2238 =
												CDR(((obj_t) BgL_oldzd2localszd2_2231));
											BgL_arg1681z00_2239 =
												MAKE_YOUNG_PAIR(BgL_localz00_2233,
												BgL_newzd2localszd2_2232);
											{
												obj_t BgL_newzd2localszd2_2742;
												obj_t BgL_oldzd2localszd2_2741;

												BgL_oldzd2localszd2_2741 = BgL_arg1678z00_2238;
												BgL_newzd2localszd2_2742 = BgL_arg1681z00_2239;
												BgL_newzd2localszd2_2232 = BgL_newzd2localszd2_2742;
												BgL_oldzd2localszd2_2231 = BgL_oldzd2localszd2_2741;
												goto BgL_loopz00_2230;
											}
										}
									}
							}
						return ((obj_t) BgL_auxz00_2706);
					}
				}
			}
		}

	}



/* &node-remove!-box-set1264 */
	obj_t BGl_z62nodezd2removez12zd2boxzd2set1264za2zzast_removez00(obj_t
		BgL_envz00_2142, obj_t BgL_nodez00_2143)
	{
		{	/* Ast/remove.scm 254 */
			{	/* Ast/remove.scm 255 */
				obj_t BgL_arg1627z00_2242;

				{	/* Ast/remove.scm 255 */
					BgL_varz00_bglt BgL_arg1629z00_2243;

					BgL_arg1629z00_2243 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2143)))->BgL_varz00);
					BgL_arg1627z00_2242 =
						BGl_nodezd2removez12zc0zzast_removez00(
						((BgL_nodez00_bglt) BgL_arg1629z00_2243));
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2143)))->BgL_varz00) =
					((BgL_varz00_bglt) ((BgL_varz00_bglt) BgL_arg1627z00_2242)), BUNSPEC);
			}
			{	/* Ast/remove.scm 256 */
				obj_t BgL_arg1630z00_2244;

				BgL_arg1630z00_2244 =
					BGl_nodezd2removez12zc0zzast_removez00(
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2143)))->BgL_valuez00));
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2143)))->BgL_valuez00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_arg1630z00_2244)),
					BUNSPEC);
			}
			return ((obj_t) ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2143));
		}

	}



/* &node-remove!-box-ref1262 */
	obj_t BGl_z62nodezd2removez12zd2boxzd2ref1262za2zzast_removez00(obj_t
		BgL_envz00_2144, obj_t BgL_nodez00_2145)
	{
		{	/* Ast/remove.scm 247 */
			{	/* Ast/remove.scm 248 */
				obj_t BgL_arg1625z00_2246;

				{	/* Ast/remove.scm 248 */
					BgL_varz00_bglt BgL_arg1626z00_2247;

					BgL_arg1626z00_2247 =
						(((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_2145)))->BgL_varz00);
					BgL_arg1625z00_2246 =
						BGl_nodezd2removez12zc0zzast_removez00(
						((BgL_nodez00_bglt) BgL_arg1626z00_2247));
				}
				((((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_2145)))->BgL_varz00) =
					((BgL_varz00_bglt) ((BgL_varz00_bglt) BgL_arg1625z00_2246)), BUNSPEC);
			}
			return ((obj_t) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_2145));
		}

	}



/* &node-remove!-make-bo1260 */
	obj_t BGl_z62nodezd2removez12zd2makezd2bo1260za2zzast_removez00(obj_t
		BgL_envz00_2146, obj_t BgL_nodez00_2147)
	{
		{	/* Ast/remove.scm 240 */
			{	/* Ast/remove.scm 241 */
				obj_t BgL_arg1615z00_2249;

				BgL_arg1615z00_2249 =
					BGl_nodezd2removez12zc0zzast_removez00(
					(((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_2147)))->BgL_valuez00));
				((((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_2147)))->BgL_valuez00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_arg1615z00_2249)),
					BUNSPEC);
			}
			return ((obj_t) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_2147));
		}

	}



/* &node-remove!-switch1258 */
	obj_t BGl_z62nodezd2removez12zd2switch1258z70zzast_removez00(obj_t
		BgL_envz00_2148, obj_t BgL_nodez00_2149)
	{
		{	/* Ast/remove.scm 230 */
			{	/* Ast/remove.scm 231 */
				obj_t BgL_arg1605z00_2251;

				BgL_arg1605z00_2251 =
					BGl_nodezd2removez12zc0zzast_removez00(
					(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2149)))->BgL_testz00));
				((((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2149)))->BgL_testz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_arg1605z00_2251)),
					BUNSPEC);
			}
			{	/* Ast/remove.scm 232 */
				obj_t BgL_g1228z00_2252;

				BgL_g1228z00_2252 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_2149)))->BgL_clausesz00);
				{
					obj_t BgL_l1226z00_2254;

					BgL_l1226z00_2254 = BgL_g1228z00_2252;
				BgL_zc3z04anonymousza31607ze3z87_2253:
					if (PAIRP(BgL_l1226z00_2254))
						{	/* Ast/remove.scm 234 */
							{	/* Ast/remove.scm 233 */
								obj_t BgL_clausez00_2255;

								BgL_clausez00_2255 = CAR(BgL_l1226z00_2254);
								{	/* Ast/remove.scm 233 */
									obj_t BgL_arg1609z00_2256;

									{	/* Ast/remove.scm 233 */
										obj_t BgL_arg1611z00_2257;

										BgL_arg1611z00_2257 = CDR(((obj_t) BgL_clausez00_2255));
										BgL_arg1609z00_2256 =
											BGl_nodezd2removez12zc0zzast_removez00(
											((BgL_nodez00_bglt) BgL_arg1611z00_2257));
									}
									{	/* Ast/remove.scm 233 */
										obj_t BgL_tmpz00_2791;

										BgL_tmpz00_2791 = ((obj_t) BgL_clausez00_2255);
										SET_CDR(BgL_tmpz00_2791, BgL_arg1609z00_2256);
									}
								}
							}
							{
								obj_t BgL_l1226z00_2794;

								BgL_l1226z00_2794 = CDR(BgL_l1226z00_2254);
								BgL_l1226z00_2254 = BgL_l1226z00_2794;
								goto BgL_zc3z04anonymousza31607ze3z87_2253;
							}
						}
					else
						{	/* Ast/remove.scm 234 */
							((bool_t) 1);
						}
				}
			}
			return ((obj_t) ((BgL_switchz00_bglt) BgL_nodez00_2149));
		}

	}



/* &node-remove!-fail1256 */
	obj_t BGl_z62nodezd2removez12zd2fail1256z70zzast_removez00(obj_t
		BgL_envz00_2150, obj_t BgL_nodez00_2151)
	{
		{	/* Ast/remove.scm 221 */
			{	/* Ast/remove.scm 222 */
				obj_t BgL_arg1589z00_2259;

				BgL_arg1589z00_2259 =
					BGl_nodezd2removez12zc0zzast_removez00(
					(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2151)))->BgL_procz00));
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2151)))->BgL_procz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_arg1589z00_2259)),
					BUNSPEC);
			}
			{	/* Ast/remove.scm 223 */
				obj_t BgL_arg1593z00_2260;

				BgL_arg1593z00_2260 =
					BGl_nodezd2removez12zc0zzast_removez00(
					(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2151)))->BgL_msgz00));
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2151)))->BgL_msgz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_arg1593z00_2260)),
					BUNSPEC);
			}
			{	/* Ast/remove.scm 224 */
				obj_t BgL_arg1595z00_2261;

				BgL_arg1595z00_2261 =
					BGl_nodezd2removez12zc0zzast_removez00(
					(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2151)))->BgL_objz00));
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2151)))->BgL_objz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_arg1595z00_2261)),
					BUNSPEC);
			}
			return ((obj_t) ((BgL_failz00_bglt) BgL_nodez00_2151));
		}

	}



/* &node-remove!-conditi1254 */
	obj_t BGl_z62nodezd2removez12zd2conditi1254z70zzast_removez00(obj_t
		BgL_envz00_2152, obj_t BgL_nodez00_2153)
	{
		{	/* Ast/remove.scm 212 */
			{	/* Ast/remove.scm 213 */
				obj_t BgL_arg1571z00_2263;

				BgL_arg1571z00_2263 =
					BGl_nodezd2removez12zc0zzast_removez00(
					(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2153)))->BgL_testz00));
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2153)))->BgL_testz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_arg1571z00_2263)),
					BUNSPEC);
			}
			{	/* Ast/remove.scm 214 */
				obj_t BgL_arg1575z00_2264;

				BgL_arg1575z00_2264 =
					BGl_nodezd2removez12zc0zzast_removez00(
					(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2153)))->BgL_truez00));
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2153)))->BgL_truez00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_arg1575z00_2264)),
					BUNSPEC);
			}
			{	/* Ast/remove.scm 215 */
				obj_t BgL_arg1584z00_2265;

				BgL_arg1584z00_2265 =
					BGl_nodezd2removez12zc0zzast_removez00(
					(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2153)))->BgL_falsez00));
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2153)))->BgL_falsez00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_arg1584z00_2265)),
					BUNSPEC);
			}
			return ((obj_t) ((BgL_conditionalz00_bglt) BgL_nodez00_2153));
		}

	}



/* &node-remove!-setq1252 */
	obj_t BGl_z62nodezd2removez12zd2setq1252z70zzast_removez00(obj_t
		BgL_envz00_2154, obj_t BgL_nodez00_2155)
	{
		{	/* Ast/remove.scm 205 */
			{	/* Ast/remove.scm 206 */
				obj_t BgL_arg1564z00_2267;

				BgL_arg1564z00_2267 =
					BGl_nodezd2removez12zc0zzast_removez00(
					(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2155)))->BgL_valuez00));
				((((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2155)))->BgL_valuez00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_arg1564z00_2267)),
					BUNSPEC);
			}
			return ((obj_t) ((BgL_setqz00_bglt) BgL_nodez00_2155));
		}

	}



/* &node-remove!-cast1250 */
	obj_t BGl_z62nodezd2removez12zd2cast1250z70zzast_removez00(obj_t
		BgL_envz00_2156, obj_t BgL_nodez00_2157)
	{
		{	/* Ast/remove.scm 198 */
			{	/* Ast/remove.scm 199 */
				obj_t BgL_arg1559z00_2269;

				BgL_arg1559z00_2269 =
					BGl_nodezd2removez12zc0zzast_removez00(
					(((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nodez00_2157)))->BgL_argz00));
				((((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nodez00_2157)))->BgL_argz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_arg1559z00_2269)),
					BUNSPEC);
			}
			return ((obj_t) ((BgL_castz00_bglt) BgL_nodez00_2157));
		}

	}



/* &node-remove!-extern1248 */
	obj_t BGl_z62nodezd2removez12zd2extern1248z70zzast_removez00(obj_t
		BgL_envz00_2158, obj_t BgL_nodez00_2159)
	{
		{	/* Ast/remove.scm 191 */
			BGl_nodezd2removeza2z12z62zzast_removez00(
				(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_2159)))->BgL_exprza2za2));
			return ((obj_t) ((BgL_externz00_bglt) BgL_nodez00_2159));
		}

	}



/* &node-remove!-funcall1246 */
	obj_t BGl_z62nodezd2removez12zd2funcall1246z70zzast_removez00(obj_t
		BgL_envz00_2160, obj_t BgL_nodez00_2161)
	{
		{	/* Ast/remove.scm 183 */
			{	/* Ast/remove.scm 184 */
				obj_t BgL_arg1544z00_2272;

				BgL_arg1544z00_2272 =
					BGl_nodezd2removez12zc0zzast_removez00(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2161)))->BgL_funz00));
				((((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2161)))->BgL_funz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_arg1544z00_2272)),
					BUNSPEC);
			}
			BGl_nodezd2removeza2z12z62zzast_removez00(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_2161)))->BgL_argsz00));
			return ((obj_t) ((BgL_funcallz00_bglt) BgL_nodez00_2161));
		}

	}



/* &node-remove!-app-ly1244 */
	obj_t BGl_z62nodezd2removez12zd2appzd2ly1244za2zzast_removez00(obj_t
		BgL_envz00_2162, obj_t BgL_nodez00_2163)
	{
		{	/* Ast/remove.scm 175 */
			{	/* Ast/remove.scm 176 */
				obj_t BgL_arg1514z00_2274;

				BgL_arg1514z00_2274 =
					BGl_nodezd2removez12zc0zzast_removez00(
					(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2163)))->BgL_funz00));
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2163)))->BgL_funz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_arg1514z00_2274)),
					BUNSPEC);
			}
			{	/* Ast/remove.scm 177 */
				obj_t BgL_arg1535z00_2275;

				BgL_arg1535z00_2275 =
					BGl_nodezd2removez12zc0zzast_removez00(
					(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2163)))->BgL_argz00));
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2163)))->BgL_argz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_arg1535z00_2275)),
					BUNSPEC);
			}
			return ((obj_t) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_2163));
		}

	}



/* &node-remove!-app1242 */
	obj_t BGl_z62nodezd2removez12zd2app1242z70zzast_removez00(obj_t
		BgL_envz00_2164, obj_t BgL_nodez00_2165)
	{
		{	/* Ast/remove.scm 167 */
			{	/* Ast/remove.scm 168 */
				obj_t BgL_arg1502z00_2277;

				{	/* Ast/remove.scm 168 */
					BgL_varz00_bglt BgL_arg1509z00_2278;

					BgL_arg1509z00_2278 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_2165)))->BgL_funz00);
					BgL_arg1502z00_2277 =
						BGl_nodezd2removez12zc0zzast_removez00(
						((BgL_nodez00_bglt) BgL_arg1509z00_2278));
				}
				((((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_2165)))->BgL_funz00) =
					((BgL_varz00_bglt) ((BgL_varz00_bglt) BgL_arg1502z00_2277)), BUNSPEC);
			}
			BGl_nodezd2removeza2z12z62zzast_removez00(
				(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_2165)))->BgL_argsz00));
			return ((obj_t) ((BgL_appz00_bglt) BgL_nodez00_2165));
		}

	}



/* &node-remove!-sync1240 */
	obj_t BGl_z62nodezd2removez12zd2sync1240z70zzast_removez00(obj_t
		BgL_envz00_2166, obj_t BgL_nodez00_2167)
	{
		{	/* Ast/remove.scm 158 */
			{	/* Ast/remove.scm 159 */
				obj_t BgL_arg1453z00_2280;

				BgL_arg1453z00_2280 =
					BGl_nodezd2removez12zc0zzast_removez00(
					(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2167)))->BgL_mutexz00));
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2167)))->BgL_mutexz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_arg1453z00_2280)),
					BUNSPEC);
			}
			{	/* Ast/remove.scm 160 */
				obj_t BgL_arg1472z00_2281;

				BgL_arg1472z00_2281 =
					BGl_nodezd2removez12zc0zzast_removez00(
					(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2167)))->BgL_prelockz00));
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2167)))->BgL_prelockz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_arg1472z00_2281)),
					BUNSPEC);
			}
			{	/* Ast/remove.scm 161 */
				obj_t BgL_arg1485z00_2282;

				BgL_arg1485z00_2282 =
					BGl_nodezd2removez12zc0zzast_removez00(
					(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2167)))->BgL_bodyz00));
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2167)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_arg1485z00_2282)),
					BUNSPEC);
			}
			return ((obj_t) ((BgL_syncz00_bglt) BgL_nodez00_2167));
		}

	}



/* &node-remove!-sequenc1238 */
	obj_t BGl_z62nodezd2removez12zd2sequenc1238z70zzast_removez00(obj_t
		BgL_envz00_2168, obj_t BgL_nodez00_2169)
	{
		{	/* Ast/remove.scm 151 */
			BGl_nodezd2removeza2z12z62zzast_removez00(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_2169)))->BgL_nodesz00));
			return ((obj_t) ((BgL_sequencez00_bglt) BgL_nodez00_2169));
		}

	}



/* &node-remove!-kwote1236 */
	obj_t BGl_z62nodezd2removez12zd2kwote1236z70zzast_removez00(obj_t
		BgL_envz00_2170, obj_t BgL_nodez00_2171)
	{
		{	/* Ast/remove.scm 145 */
			return ((obj_t) ((BgL_kwotez00_bglt) BgL_nodez00_2171));
		}

	}



/* &node-remove!-var1234 */
	obj_t BGl_z62nodezd2removez12zd2var1234z70zzast_removez00(obj_t
		BgL_envz00_2172, obj_t BgL_nodez00_2173)
	{
		{	/* Ast/remove.scm 139 */
			return ((obj_t) ((BgL_varz00_bglt) BgL_nodez00_2173));
		}

	}



/* &node-remove!-atom1232 */
	obj_t BGl_z62nodezd2removez12zd2atom1232z70zzast_removez00(obj_t
		BgL_envz00_2174, obj_t BgL_nodez00_2175)
	{
		{	/* Ast/remove.scm 133 */
			return ((obj_t) ((BgL_atomz00_bglt) BgL_nodez00_2175));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_removez00(void)
	{
		{	/* Ast/remove.scm 16 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1918z00zzast_removez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1918z00zzast_removez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1918z00zzast_removez00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1918z00zzast_removez00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1918z00zzast_removez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1918z00zzast_removez00));
			BGl_modulezd2initializa7ationz75zzeffect_effectz00(460136018L,
				BSTRING_TO_STRING(BGl_string1918z00zzast_removez00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1918z00zzast_removez00));
			BGl_modulezd2initializa7ationz75zzast_occurz00(282085879L,
				BSTRING_TO_STRING(BGl_string1918z00zzast_removez00));
			return
				BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1918z00zzast_removez00));
		}

	}

#ifdef __cplusplus
}
#endif
