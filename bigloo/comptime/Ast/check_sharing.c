/*===========================================================================*/
/*   (Ast/check_sharing.scm)                                                 */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/check_sharing.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_AST_CHECKzd2SHARINGzd2_TYPE_DEFINITIONS
#define BGL_BgL_AST_CHECKzd2SHARINGzd2_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;


#endif													// BGL_BgL_AST_CHECKzd2SHARINGzd2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t BGl_requirezd2initializa7ationz75zzast_checkzd2sharingzd2 =
		BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2(obj_t,
		obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	static obj_t
		BGl_z62checkzd2nodezd2sharingzd2resetz12za2zzast_checkzd2sharingzd2(obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62checkzd2nodezd2sharingzd2b1305zb0zzast_checkzd2sharingzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62checkzd2nodezd2sharingzd2j1301zb0zzast_checkzd2sharingzd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzast_checkzd2sharingzd2(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	static obj_t
		BGl_z62checkzd2nodezd2sharingzd2a1276zb0zzast_checkzd2sharingzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62checkzd2nodezd2sharingzd2a1279zb0zzast_checkzd2sharingzd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzast_checkzd2sharingzd2(void);
	static obj_t BGl_objectzd2initzd2zzast_checkzd2sharingzd2(void);
	BGL_EXPORTED_DECL obj_t
		BGl_checkzd2nodezd2sharingzd2resetz12zc0zzast_checkzd2sharingzd2(void);
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	static bool_t BGl_checkzd2nodezd2sharingza2za2zzast_checkzd2sharingzd2(obj_t,
		BgL_nodez00_bglt);
	static obj_t BGl_z62checkzd2nodezd2sharingz62zzast_checkzd2sharingzd2(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_za2previousza2z00zzast_checkzd2sharingzd2 = BUNSPEC;
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzast_checkzd2sharingzd2(void);
	extern obj_t BGl_externz00zzast_nodez00;
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	extern obj_t BGl_za2compilerzd2sharingzd2debugzf3za2zf3zzengine_paramz00;
	static obj_t
		BGl_z62checkzd2nodezd2sharingzd2s1272zb0zzast_checkzd2sharingzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62checkzd2nodezd2sharingzd2s1274zb0zzast_checkzd2sharingzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62checkzd2nodezd2sharingzd2c1285zb0zzast_checkzd2sharingzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62checkzd2nodezd2sharingzd2c1289zb0zzast_checkzd2sharingzd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t
		BGl_z62checkzd2nodezd2sharingzd2s1287zb0zzast_checkzd2sharingzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62checkzd2nodezd2sharingzd2s1293zb0zzast_checkzd2sharingzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62checkzd2nodezd2sharingzd2s1299zb0zzast_checkzd2sharingzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62checkzd2nodezd2sharingzd2m1303zb0zzast_checkzd2sharingzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62checkzd2nodezd2sharing1269z62zzast_checkzd2sharingzd2(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzast_checkzd2sharingzd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_includez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t
		BGl_z62checkzd2nodezd2sharingzd2l1295zb0zzast_checkzd2sharingzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62checkzd2nodezd2sharingzd2l1297zb0zzast_checkzd2sharingzd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzast_checkzd2sharingzd2(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_checkzd2sharingzd2(void);
	static obj_t BGl_z62checkzd2sharingzb0zzast_checkzd2sharingzd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzast_checkzd2sharingzd2(void);
	static obj_t BGl_gczd2rootszd2initz00zzast_checkzd2sharingzd2(void);
	static obj_t
		BGl_z62checkzd2nodezd2sharingzd2e1283zb0zzast_checkzd2sharingzd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2(BgL_nodez00_bglt,
		obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	extern obj_t BGl_userzd2warningzf2locationz20zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62checkzd2nodezd2sharingzd2f1281zb0zzast_checkzd2sharingzd2(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_za2checkzd2sharingzd2passza2z00zzast_checkzd2sharingzd2 =
		BUNSPEC;
	static obj_t
		BGl_z62checkzd2nodezd2sharingzd2f1291zb0zzast_checkzd2sharingzd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	BGL_IMPORT obj_t bgl_find_runtime_type(obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	BGL_IMPORT obj_t
		BGl_findzd2superzd2classzd2methodzd2zz__objectz00(BgL_objectz00_bglt, obj_t,
		obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static obj_t __cnst[1];


	   
		 
		DEFINE_STRING(BGl_string1743z00zzast_checkzd2sharingzd2,
		BgL_bgl_string1743za700za7za7a1776za7, " error", 6);
	      DEFINE_STRING(BGl_string1744z00zzast_checkzd2sharingzd2,
		BgL_bgl_string1744za700za7za7a1777za7, "s", 1);
	      DEFINE_STRING(BGl_string1745z00zzast_checkzd2sharingzd2,
		BgL_bgl_string1745za700za7za7a1778za7, "", 0);
	      DEFINE_STRING(BGl_string1746z00zzast_checkzd2sharingzd2,
		BgL_bgl_string1746za700za7za7a1779za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string1747z00zzast_checkzd2sharingzd2,
		BgL_bgl_string1747za700za7za7a1780za7, "failure during postlude hook", 28);
	      DEFINE_STRING(BGl_string1749z00zzast_checkzd2sharingzd2,
		BgL_bgl_string1749za700za7za7a1781za7, "check-node-sharing1269", 22);
	      DEFINE_STRING(BGl_string1750z00zzast_checkzd2sharingzd2,
		BgL_bgl_string1750za700za7za7a1782za7, "check-node-sharing (~a)", 23);
	      DEFINE_STRING(BGl_string1751z00zzast_checkzd2sharingzd2,
		BgL_bgl_string1751za700za7za7a1783za7, "shared node", 11);
	      DEFINE_STRING(BGl_string1752z00zzast_checkzd2sharingzd2,
		BgL_bgl_string1752za700za7za7a1784za7, "node    : ", 10);
	      DEFINE_STRING(BGl_string1753z00zzast_checkzd2sharingzd2,
		BgL_bgl_string1753za700za7za7a1785za7, "context : ", 10);
	      DEFINE_STRING(BGl_string1754z00zzast_checkzd2sharingzd2,
		BgL_bgl_string1754za700za7za7a1786za7, "================================",
		32);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1748z00zzast_checkzd2sharingzd2,
		BgL_bgl_za762checkza7d2nodeza71787za7,
		BGl_z62checkzd2nodezd2sharing1269z62zzast_checkzd2sharingzd2, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string1756z00zzast_checkzd2sharingzd2,
		BgL_bgl_string1756za700za7za7a1788za7, "check-node-sharing", 18);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1755z00zzast_checkzd2sharingzd2,
		BgL_bgl_za762checkza7d2nodeza71789za7,
		BGl_z62checkzd2nodezd2sharingzd2s1272zb0zzast_checkzd2sharingzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1757z00zzast_checkzd2sharingzd2,
		BgL_bgl_za762checkza7d2nodeza71790za7,
		BGl_z62checkzd2nodezd2sharingzd2s1274zb0zzast_checkzd2sharingzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1758z00zzast_checkzd2sharingzd2,
		BgL_bgl_za762checkza7d2nodeza71791za7,
		BGl_z62checkzd2nodezd2sharingzd2a1276zb0zzast_checkzd2sharingzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1759z00zzast_checkzd2sharingzd2,
		BgL_bgl_za762checkza7d2nodeza71792za7,
		BGl_z62checkzd2nodezd2sharingzd2a1279zb0zzast_checkzd2sharingzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1760z00zzast_checkzd2sharingzd2,
		BgL_bgl_za762checkza7d2nodeza71793za7,
		BGl_z62checkzd2nodezd2sharingzd2f1281zb0zzast_checkzd2sharingzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1761z00zzast_checkzd2sharingzd2,
		BgL_bgl_za762checkza7d2nodeza71794za7,
		BGl_z62checkzd2nodezd2sharingzd2e1283zb0zzast_checkzd2sharingzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1762z00zzast_checkzd2sharingzd2,
		BgL_bgl_za762checkza7d2nodeza71795za7,
		BGl_z62checkzd2nodezd2sharingzd2c1285zb0zzast_checkzd2sharingzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1763z00zzast_checkzd2sharingzd2,
		BgL_bgl_za762checkza7d2nodeza71796za7,
		BGl_z62checkzd2nodezd2sharingzd2s1287zb0zzast_checkzd2sharingzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1764z00zzast_checkzd2sharingzd2,
		BgL_bgl_za762checkza7d2nodeza71797za7,
		BGl_z62checkzd2nodezd2sharingzd2c1289zb0zzast_checkzd2sharingzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1765z00zzast_checkzd2sharingzd2,
		BgL_bgl_za762checkza7d2nodeza71798za7,
		BGl_z62checkzd2nodezd2sharingzd2f1291zb0zzast_checkzd2sharingzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1766z00zzast_checkzd2sharingzd2,
		BgL_bgl_za762checkza7d2nodeza71799za7,
		BGl_z62checkzd2nodezd2sharingzd2s1293zb0zzast_checkzd2sharingzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1773z00zzast_checkzd2sharingzd2,
		BgL_bgl_string1773za700za7za7a1800za7, "ast_check-sharing", 17);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1767z00zzast_checkzd2sharingzd2,
		BgL_bgl_za762checkza7d2nodeza71801za7,
		BGl_z62checkzd2nodezd2sharingzd2l1295zb0zzast_checkzd2sharingzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1774z00zzast_checkzd2sharingzd2,
		BgL_bgl_string1774za700za7za7a1802za7, "done ", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1768z00zzast_checkzd2sharingzd2,
		BgL_bgl_za762checkza7d2nodeza71803za7,
		BGl_z62checkzd2nodezd2sharingzd2l1297zb0zzast_checkzd2sharingzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1769z00zzast_checkzd2sharingzd2,
		BgL_bgl_za762checkza7d2nodeza71804za7,
		BGl_z62checkzd2nodezd2sharingzd2s1299zb0zzast_checkzd2sharingzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1770z00zzast_checkzd2sharingzd2,
		BgL_bgl_za762checkza7d2nodeza71805za7,
		BGl_z62checkzd2nodezd2sharingzd2j1301zb0zzast_checkzd2sharingzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1771z00zzast_checkzd2sharingzd2,
		BgL_bgl_za762checkza7d2nodeza71806za7,
		BGl_z62checkzd2nodezd2sharingzd2m1303zb0zzast_checkzd2sharingzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1772z00zzast_checkzd2sharingzd2,
		BgL_bgl_za762checkza7d2nodeza71807za7,
		BGl_z62checkzd2nodezd2sharingzd2b1305zb0zzast_checkzd2sharingzd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_checkzd2nodezd2sharingzd2resetz12zd2envz12zzast_checkzd2sharingzd2,
		BgL_bgl_za762checkza7d2nodeza71808za7,
		BGl_z62checkzd2nodezd2sharingzd2resetz12za2zzast_checkzd2sharingzd2, 0L,
		BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_checkzd2sharingzd2envz00zzast_checkzd2sharingzd2,
		BgL_bgl_za762checkza7d2shari1809z00,
		BGl_z62checkzd2sharingzb0zzast_checkzd2sharingzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
		BgL_bgl_za762checkza7d2nodeza71810za7,
		BGl_z62checkzd2nodezd2sharingz62zzast_checkzd2sharingzd2, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzast_checkzd2sharingzd2));
		     ADD_ROOT((void *) (&BGl_za2previousza2z00zzast_checkzd2sharingzd2));
		   
			 ADD_ROOT((void
				*) (&BGl_za2checkzd2sharingzd2passza2z00zzast_checkzd2sharingzd2));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzast_checkzd2sharingzd2(long
		BgL_checksumz00_2333, char *BgL_fromz00_2334)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_checkzd2sharingzd2))
				{
					BGl_requirezd2initializa7ationz75zzast_checkzd2sharingzd2 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_checkzd2sharingzd2();
					BGl_libraryzd2moduleszd2initz00zzast_checkzd2sharingzd2();
					BGl_cnstzd2initzd2zzast_checkzd2sharingzd2();
					BGl_importedzd2moduleszd2initz00zzast_checkzd2sharingzd2();
					BGl_genericzd2initzd2zzast_checkzd2sharingzd2();
					BGl_methodzd2initzd2zzast_checkzd2sharingzd2();
					return BGl_toplevelzd2initzd2zzast_checkzd2sharingzd2();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_checkzd2sharingzd2(void)
	{
		{	/* Ast/check_sharing.scm 17 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"ast_check-sharing");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "ast_check-sharing");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"ast_check-sharing");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_check-sharing");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"ast_check-sharing");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_check-sharing");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"ast_check-sharing");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "ast_check-sharing");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"ast_check-sharing");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"ast_check-sharing");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "ast_check-sharing");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "ast_check-sharing");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_checkzd2sharingzd2(void)
	{
		{	/* Ast/check_sharing.scm 17 */
			{	/* Ast/check_sharing.scm 17 */
				obj_t BgL_cportz00_2219;

				{	/* Ast/check_sharing.scm 17 */
					obj_t BgL_stringz00_2226;

					BgL_stringz00_2226 = BGl_string1774z00zzast_checkzd2sharingzd2;
					{	/* Ast/check_sharing.scm 17 */
						obj_t BgL_startz00_2227;

						BgL_startz00_2227 = BINT(0L);
						{	/* Ast/check_sharing.scm 17 */
							obj_t BgL_endz00_2228;

							BgL_endz00_2228 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2226)));
							{	/* Ast/check_sharing.scm 17 */

								BgL_cportz00_2219 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2226, BgL_startz00_2227, BgL_endz00_2228);
				}}}}
				{
					long BgL_iz00_2220;

					BgL_iz00_2220 = 0L;
				BgL_loopz00_2221:
					if ((BgL_iz00_2220 == -1L))
						{	/* Ast/check_sharing.scm 17 */
							return BUNSPEC;
						}
					else
						{	/* Ast/check_sharing.scm 17 */
							{	/* Ast/check_sharing.scm 17 */
								obj_t BgL_arg1775z00_2222;

								{	/* Ast/check_sharing.scm 17 */

									{	/* Ast/check_sharing.scm 17 */
										obj_t BgL_locationz00_2224;

										BgL_locationz00_2224 = BBOOL(((bool_t) 0));
										{	/* Ast/check_sharing.scm 17 */

											BgL_arg1775z00_2222 =
												BGl_readz00zz__readerz00(BgL_cportz00_2219,
												BgL_locationz00_2224);
										}
									}
								}
								{	/* Ast/check_sharing.scm 17 */
									int BgL_tmpz00_2366;

									BgL_tmpz00_2366 = (int) (BgL_iz00_2220);
									CNST_TABLE_SET(BgL_tmpz00_2366, BgL_arg1775z00_2222);
							}}
							{	/* Ast/check_sharing.scm 17 */
								int BgL_auxz00_2225;

								BgL_auxz00_2225 = (int) ((BgL_iz00_2220 - 1L));
								{
									long BgL_iz00_2371;

									BgL_iz00_2371 = (long) (BgL_auxz00_2225);
									BgL_iz00_2220 = BgL_iz00_2371;
									goto BgL_loopz00_2221;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_checkzd2sharingzd2(void)
	{
		{	/* Ast/check_sharing.scm 17 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzast_checkzd2sharingzd2(void)
	{
		{	/* Ast/check_sharing.scm 17 */
			BGl_za2checkzd2sharingzd2passza2z00zzast_checkzd2sharingzd2 = BFALSE;
			BGl_za2previousza2z00zzast_checkzd2sharingzd2 = BNIL;
			return BUNSPEC;
		}

	}



/* check-sharing */
	BGL_EXPORTED_DEF obj_t BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2(obj_t
		BgL_passz00_25, obj_t BgL_globalsz00_26)
	{
		{	/* Ast/check_sharing.scm 38 */
			if (CBOOL(BGl_za2compilerzd2sharingzd2debugzf3za2zf3zzengine_paramz00))
				{	/* Ast/check_sharing.scm 39 */
					BGl_za2checkzd2sharingzd2passza2z00zzast_checkzd2sharingzd2 =
						BgL_passz00_25;
					{
						obj_t BgL_l1252z00_1665;

						BgL_l1252z00_1665 = BgL_globalsz00_26;
					BgL_zc3z04anonymousza31324ze3z87_1666:
						if (PAIRP(BgL_l1252z00_1665))
							{	/* Ast/check_sharing.scm 42 */
								{	/* Ast/check_sharing.scm 43 */
									obj_t BgL_globalz00_1668;

									BgL_globalz00_1668 = CAR(BgL_l1252z00_1665);
									{	/* Ast/check_sharing.scm 57 */
										BgL_valuez00_bglt BgL_funz00_2049;

										BgL_funz00_2049 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_globalz00_1668)))->
											BgL_valuez00);
										{	/* Ast/check_sharing.scm 57 */
											obj_t BgL_bodyz00_2050;

											BgL_bodyz00_2050 =
												(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_funz00_2049)))->
												BgL_bodyz00);
											{	/* Ast/check_sharing.scm 58 */

												BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2(
													((BgL_nodez00_bglt) BgL_bodyz00_2050), BUNSPEC);
											}
										}
									}
								}
								{
									obj_t BgL_l1252z00_2385;

									BgL_l1252z00_2385 = CDR(BgL_l1252z00_1665);
									BgL_l1252z00_1665 = BgL_l1252z00_2385;
									goto BgL_zc3z04anonymousza31324ze3z87_1666;
								}
							}
						else
							{	/* Ast/check_sharing.scm 42 */
								((bool_t) 1);
							}
					}
					BGl_za2previousza2z00zzast_checkzd2sharingzd2 = BNIL;
					if (
						((long) CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) >
							0L))
						{	/* Ast/check_sharing.scm 46 */
							{	/* Ast/check_sharing.scm 46 */
								obj_t BgL_port1254z00_1673;

								{	/* Ast/check_sharing.scm 46 */
									obj_t BgL_tmpz00_2390;

									BgL_tmpz00_2390 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_port1254z00_1673 =
										BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_2390);
								}
								bgl_display_obj
									(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
									BgL_port1254z00_1673);
								bgl_display_string(BGl_string1743z00zzast_checkzd2sharingzd2,
									BgL_port1254z00_1673);
								{	/* Ast/check_sharing.scm 46 */
									obj_t BgL_arg1328z00_1674;

									{	/* Ast/check_sharing.scm 46 */
										bool_t BgL_test1816z00_2395;

										if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
											(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
											{	/* Ast/check_sharing.scm 46 */
												if (INTEGERP
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
													{	/* Ast/check_sharing.scm 46 */
														BgL_test1816z00_2395 =
															(
															(long)
															CINT
															(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
															> 1L);
													}
												else
													{	/* Ast/check_sharing.scm 46 */
														BgL_test1816z00_2395 =
															BGl_2ze3ze3zz__r4_numbers_6_5z00
															(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
															BINT(1L));
													}
											}
										else
											{	/* Ast/check_sharing.scm 46 */
												BgL_test1816z00_2395 = ((bool_t) 0);
											}
										if (BgL_test1816z00_2395)
											{	/* Ast/check_sharing.scm 46 */
												BgL_arg1328z00_1674 =
													BGl_string1744z00zzast_checkzd2sharingzd2;
											}
										else
											{	/* Ast/check_sharing.scm 46 */
												BgL_arg1328z00_1674 =
													BGl_string1745z00zzast_checkzd2sharingzd2;
											}
									}
									bgl_display_obj(BgL_arg1328z00_1674, BgL_port1254z00_1673);
								}
								bgl_display_string(BGl_string1746z00zzast_checkzd2sharingzd2,
									BgL_port1254z00_1673);
								bgl_display_char(((unsigned char) 10), BgL_port1254z00_1673);
							}
							{	/* Ast/check_sharing.scm 46 */
								obj_t BgL_list1331z00_1678;

								BgL_list1331z00_1678 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
								return BGl_exitz00zz__errorz00(BgL_list1331z00_1678);
							}
						}
					else
						{	/* Ast/check_sharing.scm 46 */
							obj_t BgL_g1110z00_1679;

							BgL_g1110z00_1679 = BNIL;
							{
								obj_t BgL_hooksz00_1682;
								obj_t BgL_hnamesz00_1683;

								BgL_hooksz00_1682 = BgL_g1110z00_1679;
								BgL_hnamesz00_1683 = BNIL;
							BgL_zc3z04anonymousza31332ze3z87_1684:
								if (NULLP(BgL_hooksz00_1682))
									{	/* Ast/check_sharing.scm 46 */
										return BgL_globalsz00_26;
									}
								else
									{	/* Ast/check_sharing.scm 46 */
										bool_t BgL_test1821z00_2412;

										{	/* Ast/check_sharing.scm 46 */
											obj_t BgL_fun1343z00_1691;

											BgL_fun1343z00_1691 = CAR(((obj_t) BgL_hooksz00_1682));
											BgL_test1821z00_2412 =
												CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1343z00_1691));
										}
										if (BgL_test1821z00_2412)
											{	/* Ast/check_sharing.scm 46 */
												obj_t BgL_arg1339z00_1688;
												obj_t BgL_arg1340z00_1689;

												BgL_arg1339z00_1688 = CDR(((obj_t) BgL_hooksz00_1682));
												BgL_arg1340z00_1689 = CDR(((obj_t) BgL_hnamesz00_1683));
												{
													obj_t BgL_hnamesz00_2424;
													obj_t BgL_hooksz00_2423;

													BgL_hooksz00_2423 = BgL_arg1339z00_1688;
													BgL_hnamesz00_2424 = BgL_arg1340z00_1689;
													BgL_hnamesz00_1683 = BgL_hnamesz00_2424;
													BgL_hooksz00_1682 = BgL_hooksz00_2423;
													goto BgL_zc3z04anonymousza31332ze3z87_1684;
												}
											}
										else
											{	/* Ast/check_sharing.scm 46 */
												obj_t BgL_arg1342z00_1690;

												BgL_arg1342z00_1690 = CAR(((obj_t) BgL_hnamesz00_1683));
												return
													BGl_internalzd2errorzd2zztools_errorz00
													(BGl_za2currentzd2passza2zd2zzengine_passz00,
													BGl_string1747z00zzast_checkzd2sharingzd2,
													BgL_arg1342z00_1690);
											}
									}
							}
						}
				}
			else
				{	/* Ast/check_sharing.scm 39 */
					return BFALSE;
				}
		}

	}



/* &check-sharing */
	obj_t BGl_z62checkzd2sharingzb0zzast_checkzd2sharingzd2(obj_t BgL_envz00_2139,
		obj_t BgL_passz00_2140, obj_t BgL_globalsz00_2141)
	{
		{	/* Ast/check_sharing.scm 38 */
			return
				BGl_checkzd2sharingzd2zzast_checkzd2sharingzd2(BgL_passz00_2140,
				BgL_globalsz00_2141);
		}

	}



/* check-node-sharing-reset! */
	BGL_EXPORTED_DEF obj_t
		BGl_checkzd2nodezd2sharingzd2resetz12zc0zzast_checkzd2sharingzd2(void)
	{
		{	/* Ast/check_sharing.scm 69 */
			return (BGl_za2previousza2z00zzast_checkzd2sharingzd2 = BNIL, BUNSPEC);
		}

	}



/* &check-node-sharing-reset! */
	obj_t
		BGl_z62checkzd2nodezd2sharingzd2resetz12za2zzast_checkzd2sharingzd2(obj_t
		BgL_envz00_2142)
	{
		{	/* Ast/check_sharing.scm 69 */
			return BGl_checkzd2nodezd2sharingzd2resetz12zc0zzast_checkzd2sharingzd2();
		}

	}



/* check-node-sharing* */
	bool_t BGl_checkzd2nodezd2sharingza2za2zzast_checkzd2sharingzd2(obj_t
		BgL_nodeza2za2_65, BgL_nodez00_bglt BgL_contextz00_66)
	{
		{	/* Ast/check_sharing.scm 241 */
			{
				obj_t BgL_l1267z00_1697;

				BgL_l1267z00_1697 = BgL_nodeza2za2_65;
			BgL_zc3z04anonymousza31345ze3z87_1698:
				if (PAIRP(BgL_l1267z00_1697))
					{	/* Ast/check_sharing.scm 242 */
						{	/* Ast/check_sharing.scm 242 */
							obj_t BgL_nodez00_1700;

							BgL_nodez00_1700 = CAR(BgL_l1267z00_1697);
							BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2(
								((BgL_nodez00_bglt) BgL_nodez00_1700),
								((obj_t) BgL_contextz00_66));
						}
						{
							obj_t BgL_l1267z00_2436;

							BgL_l1267z00_2436 = CDR(BgL_l1267z00_1697);
							BgL_l1267z00_1697 = BgL_l1267z00_2436;
							goto BgL_zc3z04anonymousza31345ze3z87_1698;
						}
					}
				else
					{	/* Ast/check_sharing.scm 242 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_checkzd2sharingzd2(void)
	{
		{	/* Ast/check_sharing.scm 17 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_checkzd2sharingzd2(void)
	{
		{	/* Ast/check_sharing.scm 17 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
				BGl_proc1748z00zzast_checkzd2sharingzd2, BGl_nodez00zzast_nodez00,
				BGl_string1749z00zzast_checkzd2sharingzd2);
		}

	}



/* &check-node-sharing1269 */
	obj_t BGl_z62checkzd2nodezd2sharing1269z62zzast_checkzd2sharingzd2(obj_t
		BgL_envz00_2144, obj_t BgL_nodez00_2145, obj_t BgL_contextz00_2146)
	{
		{	/* Ast/check_sharing.scm 75 */
			if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
						((obj_t)
							((BgL_nodez00_bglt) BgL_nodez00_2145)),
						BGl_za2previousza2z00zzast_checkzd2sharingzd2)))
				{	/* Ast/check_sharing.scm 76 */
					{	/* Ast/check_sharing.scm 78 */
						obj_t BgL_arg1351z00_2231;
						obj_t BgL_arg1352z00_2232;
						obj_t BgL_arg1361z00_2233;

						BgL_arg1351z00_2231 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_nodez00_2145)))->BgL_locz00);
						{	/* Ast/check_sharing.scm 79 */
							obj_t BgL_list1362z00_2234;

							BgL_list1362z00_2234 =
								MAKE_YOUNG_PAIR
								(BGl_za2checkzd2sharingzd2passza2z00zzast_checkzd2sharingzd2,
								BNIL);
							BgL_arg1352z00_2232 =
								BGl_formatz00zz__r4_output_6_10_3z00
								(BGl_string1750z00zzast_checkzd2sharingzd2,
								BgL_list1362z00_2234);
						}
						BgL_arg1361z00_2233 =
							bgl_find_runtime_type(
							((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_2145)));
						BGl_userzd2warningzf2locationz20zztools_errorz00
							(BgL_arg1351z00_2231, BgL_arg1352z00_2232,
							BGl_string1751z00zzast_checkzd2sharingzd2, BgL_arg1361z00_2233);
					}
					{	/* Ast/check_sharing.scm 83 */
						obj_t BgL_port1255z00_2235;

						{	/* Ast/check_sharing.scm 83 */
							obj_t BgL_tmpz00_2452;

							BgL_tmpz00_2452 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_port1255z00_2235 =
								BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_2452);
						}
						bgl_display_string(BGl_string1752z00zzast_checkzd2sharingzd2,
							BgL_port1255z00_2235);
						{	/* Ast/check_sharing.scm 83 */
							obj_t BgL_arg1364z00_2236;

							BgL_arg1364z00_2236 =
								BGl_shapez00zztools_shapez00(
								((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_2145)));
							bgl_display_obj(BgL_arg1364z00_2236, BgL_port1255z00_2235);
						}
						bgl_display_char(((unsigned char) 10), BgL_port1255z00_2235);
					}
					{	/* Ast/check_sharing.scm 84 */
						obj_t BgL_port1256z00_2237;

						{	/* Ast/check_sharing.scm 84 */
							obj_t BgL_tmpz00_2461;

							BgL_tmpz00_2461 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_port1256z00_2237 =
								BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_2461);
						}
						bgl_display_string(BGl_string1753z00zzast_checkzd2sharingzd2,
							BgL_port1256z00_2237);
						bgl_display_obj(BGl_shapez00zztools_shapez00(BgL_contextz00_2146),
							BgL_port1256z00_2237);
						bgl_display_char(((unsigned char) 10), BgL_port1256z00_2237);
					}
					{	/* Ast/check_sharing.scm 85 */
						obj_t BgL_port1257z00_2238;

						{	/* Ast/check_sharing.scm 85 */
							obj_t BgL_tmpz00_2468;

							BgL_tmpz00_2468 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_port1257z00_2238 =
								BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_2468);
						}
						bgl_display_string(BGl_string1754z00zzast_checkzd2sharingzd2,
							BgL_port1257z00_2238);
						bgl_display_char(((unsigned char) 10), BgL_port1257z00_2238);
				}}
			else
				{	/* Ast/check_sharing.scm 76 */
					BGl_za2previousza2z00zzast_checkzd2sharingzd2 =
						MAKE_YOUNG_PAIR(
						((obj_t)
							((BgL_nodez00_bglt) BgL_nodez00_2145)),
						BGl_za2previousza2z00zzast_checkzd2sharingzd2);
				}
			return CNST_TABLE_REF(0);
		}

	}



/* check-node-sharing */
	BGL_EXPORTED_DEF obj_t
		BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2(BgL_nodez00_bglt
		BgL_nodez00_29, obj_t BgL_contextz00_30)
	{
		{	/* Ast/check_sharing.scm 75 */
			{	/* Ast/check_sharing.scm 75 */
				obj_t BgL_method1270z00_1718;

				{	/* Ast/check_sharing.scm 75 */
					obj_t BgL_res1742z00_2116;

					{	/* Ast/check_sharing.scm 75 */
						long BgL_objzd2classzd2numz00_2087;

						BgL_objzd2classzd2numz00_2087 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_29));
						{	/* Ast/check_sharing.scm 75 */
							obj_t BgL_arg1811z00_2088;

							BgL_arg1811z00_2088 =
								PROCEDURE_REF
								(BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
								(int) (1L));
							{	/* Ast/check_sharing.scm 75 */
								int BgL_offsetz00_2091;

								BgL_offsetz00_2091 = (int) (BgL_objzd2classzd2numz00_2087);
								{	/* Ast/check_sharing.scm 75 */
									long BgL_offsetz00_2092;

									BgL_offsetz00_2092 =
										((long) (BgL_offsetz00_2091) - OBJECT_TYPE);
									{	/* Ast/check_sharing.scm 75 */
										long BgL_modz00_2093;

										BgL_modz00_2093 =
											(BgL_offsetz00_2092 >> (int) ((long) ((int) (4L))));
										{	/* Ast/check_sharing.scm 75 */
											long BgL_restz00_2095;

											BgL_restz00_2095 =
												(BgL_offsetz00_2092 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/check_sharing.scm 75 */

												{	/* Ast/check_sharing.scm 75 */
													obj_t BgL_bucketz00_2097;

													BgL_bucketz00_2097 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2088), BgL_modz00_2093);
													BgL_res1742z00_2116 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2097), BgL_restz00_2095);
					}}}}}}}}
					BgL_method1270z00_1718 = BgL_res1742z00_2116;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1270z00_1718,
					((obj_t) BgL_nodez00_29), BgL_contextz00_30);
			}
		}

	}



/* &check-node-sharing */
	obj_t BGl_z62checkzd2nodezd2sharingz62zzast_checkzd2sharingzd2(obj_t
		BgL_envz00_2147, obj_t BgL_nodez00_2148, obj_t BgL_contextz00_2149)
	{
		{	/* Ast/check_sharing.scm 75 */
			return
				BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2(
				((BgL_nodez00_bglt) BgL_nodez00_2148), BgL_contextz00_2149);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_checkzd2sharingzd2(void)
	{
		{	/* Ast/check_sharing.scm 17 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
				BGl_sequencez00zzast_nodez00, BGl_proc1755z00zzast_checkzd2sharingzd2,
				BGl_string1756z00zzast_checkzd2sharingzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
				BGl_syncz00zzast_nodez00, BGl_proc1757z00zzast_checkzd2sharingzd2,
				BGl_string1756z00zzast_checkzd2sharingzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
				BGl_appz00zzast_nodez00, BGl_proc1758z00zzast_checkzd2sharingzd2,
				BGl_string1756z00zzast_checkzd2sharingzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1759z00zzast_checkzd2sharingzd2,
				BGl_string1756z00zzast_checkzd2sharingzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
				BGl_funcallz00zzast_nodez00, BGl_proc1760z00zzast_checkzd2sharingzd2,
				BGl_string1756z00zzast_checkzd2sharingzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
				BGl_externz00zzast_nodez00, BGl_proc1761z00zzast_checkzd2sharingzd2,
				BGl_string1756z00zzast_checkzd2sharingzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
				BGl_castz00zzast_nodez00, BGl_proc1762z00zzast_checkzd2sharingzd2,
				BGl_string1756z00zzast_checkzd2sharingzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
				BGl_setqz00zzast_nodez00, BGl_proc1763z00zzast_checkzd2sharingzd2,
				BGl_string1756z00zzast_checkzd2sharingzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
				BGl_conditionalz00zzast_nodez00,
				BGl_proc1764z00zzast_checkzd2sharingzd2,
				BGl_string1756z00zzast_checkzd2sharingzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
				BGl_failz00zzast_nodez00, BGl_proc1765z00zzast_checkzd2sharingzd2,
				BGl_string1756z00zzast_checkzd2sharingzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
				BGl_switchz00zzast_nodez00, BGl_proc1766z00zzast_checkzd2sharingzd2,
				BGl_string1756z00zzast_checkzd2sharingzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1767z00zzast_checkzd2sharingzd2,
				BGl_string1756z00zzast_checkzd2sharingzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1768z00zzast_checkzd2sharingzd2,
				BGl_string1756z00zzast_checkzd2sharingzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
				BGl_setzd2exzd2itz00zzast_nodez00,
				BGl_proc1769z00zzast_checkzd2sharingzd2,
				BGl_string1756z00zzast_checkzd2sharingzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
				BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc1770z00zzast_checkzd2sharingzd2,
				BGl_string1756z00zzast_checkzd2sharingzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1771z00zzast_checkzd2sharingzd2,
				BGl_string1756z00zzast_checkzd2sharingzd2);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
				BGl_boxzd2setz12zc0zzast_nodez00,
				BGl_proc1772z00zzast_checkzd2sharingzd2,
				BGl_string1756z00zzast_checkzd2sharingzd2);
		}

	}



/* &check-node-sharing-b1305 */
	obj_t BGl_z62checkzd2nodezd2sharingzd2b1305zb0zzast_checkzd2sharingzd2(obj_t
		BgL_envz00_2167, obj_t BgL_nodez00_2168, obj_t BgL_contextz00_2169)
	{
		{	/* Ast/check_sharing.scm 233 */
			{

				{	/* Ast/check_sharing.scm 233 */
					obj_t BgL_nextzd2method1304zd2_2241;

					BgL_nextzd2method1304zd2_2241 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2168)),
						BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
						BGl_boxzd2setz12zc0zzast_nodez00);
					BGL_PROCEDURE_CALL2(BgL_nextzd2method1304zd2_2241,
						((obj_t) ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2168)),
						BgL_contextz00_2169);
				}
				{	/* Ast/check_sharing.scm 236 */
					BgL_nodez00_bglt BgL_arg1559z00_2242;

					BgL_arg1559z00_2242 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2168)))->BgL_valuez00);
					return
						BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2
						(BgL_arg1559z00_2242,
						((obj_t) ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2168)));
				}
			}
		}

	}



/* &check-node-sharing-m1303 */
	obj_t BGl_z62checkzd2nodezd2sharingzd2m1303zb0zzast_checkzd2sharingzd2(obj_t
		BgL_envz00_2170, obj_t BgL_nodez00_2171, obj_t BgL_contextz00_2172)
	{
		{	/* Ast/check_sharing.scm 226 */
			{

				{	/* Ast/check_sharing.scm 226 */
					obj_t BgL_nextzd2method1302zd2_2245;

					BgL_nextzd2method1302zd2_2245 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_2171)),
						BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
						BGl_makezd2boxzd2zzast_nodez00);
					BGL_PROCEDURE_CALL2(BgL_nextzd2method1302zd2_2245,
						((obj_t) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_2171)),
						BgL_contextz00_2172);
				}
				{	/* Ast/check_sharing.scm 228 */
					BgL_nodez00_bglt BgL_arg1553z00_2246;

					BgL_arg1553z00_2246 =
						(((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_2171)))->BgL_valuez00);
					return
						BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2
						(BgL_arg1553z00_2246,
						((obj_t) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_2171)));
				}
			}
		}

	}



/* &check-node-sharing-j1301 */
	obj_t BGl_z62checkzd2nodezd2sharingzd2j1301zb0zzast_checkzd2sharingzd2(obj_t
		BgL_envz00_2173, obj_t BgL_nodez00_2174, obj_t BgL_contextz00_2175)
	{
		{	/* Ast/check_sharing.scm 217 */
			{

				{	/* Ast/check_sharing.scm 217 */
					obj_t BgL_nextzd2method1300zd2_2249;

					BgL_nextzd2method1300zd2_2249 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2174)),
						BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
						BGl_jumpzd2exzd2itz00zzast_nodez00);
					BGL_PROCEDURE_CALL2(BgL_nextzd2method1300zd2_2249,
						((obj_t) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2174)),
						BgL_contextz00_2175);
				}
				{	/* Ast/check_sharing.scm 220 */
					BgL_nodez00_bglt BgL_arg1546z00_2250;

					BgL_arg1546z00_2250 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2174)))->BgL_exitz00);
					BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2
						(BgL_arg1546z00_2250,
						((obj_t) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2174)));
				}
				{	/* Ast/check_sharing.scm 221 */
					BgL_nodez00_bglt BgL_arg1552z00_2251;

					BgL_arg1552z00_2251 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2174)))->
						BgL_valuez00);
					return
						BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2
						(BgL_arg1552z00_2251,
						((obj_t) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2174)));
				}
			}
		}

	}



/* &check-node-sharing-s1299 */
	obj_t BGl_z62checkzd2nodezd2sharingzd2s1299zb0zzast_checkzd2sharingzd2(obj_t
		BgL_envz00_2176, obj_t BgL_nodez00_2177, obj_t BgL_contextz00_2178)
	{
		{	/* Ast/check_sharing.scm 209 */
			{

				{	/* Ast/check_sharing.scm 209 */
					obj_t BgL_nextzd2method1298zd2_2254;

					BgL_nextzd2method1298zd2_2254 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2177)),
						BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
						BGl_setzd2exzd2itz00zzast_nodez00);
					BGL_PROCEDURE_CALL2(BgL_nextzd2method1298zd2_2254,
						((obj_t) ((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2177)),
						BgL_contextz00_2178);
				}
				{	/* Ast/check_sharing.scm 211 */
					BgL_nodez00_bglt BgL_arg1540z00_2255;

					BgL_arg1540z00_2255 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2177)))->BgL_bodyz00);
					BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2
						(BgL_arg1540z00_2255,
						((obj_t) ((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2177)));
				}
				{	/* Ast/check_sharing.scm 212 */
					BgL_nodez00_bglt BgL_arg1544z00_2256;

					BgL_arg1544z00_2256 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2177)))->
						BgL_onexitz00);
					return
						BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2
						(BgL_arg1544z00_2256,
						((obj_t) ((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2177)));
				}
			}
		}

	}



/* &check-node-sharing-l1297 */
	obj_t BGl_z62checkzd2nodezd2sharingzd2l1297zb0zzast_checkzd2sharingzd2(obj_t
		BgL_envz00_2179, obj_t BgL_nodez00_2180, obj_t BgL_contextz00_2181)
	{
		{	/* Ast/check_sharing.scm 198 */
			{

				{	/* Ast/check_sharing.scm 198 */
					obj_t BgL_nextzd2method1296zd2_2259;

					BgL_nextzd2method1296zd2_2259 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2180)),
						BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
						BGl_letzd2varzd2zzast_nodez00);
					BGL_PROCEDURE_CALL2(BgL_nextzd2method1296zd2_2259,
						((obj_t) ((BgL_letzd2varzd2_bglt) BgL_nodez00_2180)),
						BgL_contextz00_2181);
				}
				{	/* Ast/check_sharing.scm 201 */
					BgL_nodez00_bglt BgL_arg1513z00_2260;

					BgL_arg1513z00_2260 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2180)))->BgL_bodyz00);
					BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2
						(BgL_arg1513z00_2260,
						((obj_t) ((BgL_letzd2varzd2_bglt) BgL_nodez00_2180)));
				}
				{	/* Ast/check_sharing.scm 202 */
					obj_t BgL_g1266z00_2261;

					BgL_g1266z00_2261 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2180)))->BgL_bindingsz00);
					{
						obj_t BgL_l1264z00_2263;

						{	/* Ast/check_sharing.scm 202 */
							bool_t BgL_tmpz00_2614;

							BgL_l1264z00_2263 = BgL_g1266z00_2261;
						BgL_zc3z04anonymousza31514ze3z87_2262:
							if (PAIRP(BgL_l1264z00_2263))
								{	/* Ast/check_sharing.scm 202 */
									{	/* Ast/check_sharing.scm 203 */
										obj_t BgL_bindingz00_2264;

										BgL_bindingz00_2264 = CAR(BgL_l1264z00_2263);
										{	/* Ast/check_sharing.scm 203 */
											obj_t BgL_arg1516z00_2265;

											BgL_arg1516z00_2265 = CDR(((obj_t) BgL_bindingz00_2264));
											BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2(
												((BgL_nodez00_bglt) BgL_arg1516z00_2265),
												((obj_t) ((BgL_letzd2varzd2_bglt) BgL_nodez00_2180)));
										}
									}
									{
										obj_t BgL_l1264z00_2624;

										BgL_l1264z00_2624 = CDR(BgL_l1264z00_2263);
										BgL_l1264z00_2263 = BgL_l1264z00_2624;
										goto BgL_zc3z04anonymousza31514ze3z87_2262;
									}
								}
							else
								{	/* Ast/check_sharing.scm 202 */
									BgL_tmpz00_2614 = ((bool_t) 1);
								}
							return BBOOL(BgL_tmpz00_2614);
						}
					}
				}
			}
		}

	}



/* &check-node-sharing-l1295 */
	obj_t BGl_z62checkzd2nodezd2sharingzd2l1295zb0zzast_checkzd2sharingzd2(obj_t
		BgL_envz00_2182, obj_t BgL_nodez00_2183, obj_t BgL_contextz00_2184)
	{
		{	/* Ast/check_sharing.scm 189 */
			{

				{	/* Ast/check_sharing.scm 189 */
					obj_t BgL_nextzd2method1294zd2_2268;

					BgL_nextzd2method1294zd2_2268 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2183)),
						BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
						BGl_letzd2funzd2zzast_nodez00);
					BGL_PROCEDURE_CALL2(BgL_nextzd2method1294zd2_2268,
						((obj_t) ((BgL_letzd2funzd2_bglt) BgL_nodez00_2183)),
						BgL_contextz00_2184);
				}
				{	/* Ast/check_sharing.scm 192 */
					obj_t BgL_g1263z00_2269;

					BgL_g1263z00_2269 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2183)))->BgL_localsz00);
					{
						obj_t BgL_l1261z00_2271;

						BgL_l1261z00_2271 = BgL_g1263z00_2269;
					BgL_zc3z04anonymousza31490ze3z87_2270:
						if (PAIRP(BgL_l1261z00_2271))
							{	/* Ast/check_sharing.scm 192 */
								{	/* Ast/check_sharing.scm 192 */
									obj_t BgL_fz00_2272;

									BgL_fz00_2272 = CAR(BgL_l1261z00_2271);
									{	/* Ast/check_sharing.scm 57 */
										BgL_valuez00_bglt BgL_funz00_2273;

										BgL_funz00_2273 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_fz00_2272)))->
											BgL_valuez00);
										{	/* Ast/check_sharing.scm 57 */
											obj_t BgL_bodyz00_2274;

											BgL_bodyz00_2274 =
												(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_funz00_2273)))->
												BgL_bodyz00);
											{	/* Ast/check_sharing.scm 58 */

												BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2(
													((BgL_nodez00_bglt) BgL_bodyz00_2274),
													((obj_t) ((BgL_letzd2funzd2_bglt) BgL_nodez00_2183)));
											}
										}
									}
								}
								{
									obj_t BgL_l1261z00_2650;

									BgL_l1261z00_2650 = CDR(BgL_l1261z00_2271);
									BgL_l1261z00_2271 = BgL_l1261z00_2650;
									goto BgL_zc3z04anonymousza31490ze3z87_2270;
								}
							}
						else
							{	/* Ast/check_sharing.scm 192 */
								((bool_t) 1);
							}
					}
				}
				{	/* Ast/check_sharing.scm 193 */
					BgL_nodez00_bglt BgL_arg1509z00_2275;

					BgL_arg1509z00_2275 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2183)))->BgL_bodyz00);
					return
						BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2
						(BgL_arg1509z00_2275,
						((obj_t) ((BgL_letzd2funzd2_bglt) BgL_nodez00_2183)));
				}
			}
		}

	}



/* &check-node-sharing-s1293 */
	obj_t BGl_z62checkzd2nodezd2sharingzd2s1293zb0zzast_checkzd2sharingzd2(obj_t
		BgL_envz00_2185, obj_t BgL_nodez00_2186, obj_t BgL_contextz00_2187)
	{
		{	/* Ast/check_sharing.scm 175 */
			{

				{	/* Ast/check_sharing.scm 175 */
					obj_t BgL_nextzd2method1292zd2_2278;

					BgL_nextzd2method1292zd2_2278 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_switchz00_bglt) BgL_nodez00_2186)),
						BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
						BGl_switchz00zzast_nodez00);
					BGL_PROCEDURE_CALL2(BgL_nextzd2method1292zd2_2278,
						((obj_t) ((BgL_switchz00_bglt) BgL_nodez00_2186)),
						BgL_contextz00_2187);
				}
				{	/* Ast/check_sharing.scm 178 */
					BgL_nodez00_bglt BgL_arg1473z00_2279;

					BgL_arg1473z00_2279 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2186)))->BgL_testz00);
					BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2
						(BgL_arg1473z00_2279,
						((obj_t) ((BgL_switchz00_bglt) BgL_nodez00_2186)));
				}
				{	/* Ast/check_sharing.scm 179 */
					obj_t BgL_g1260z00_2280;

					BgL_g1260z00_2280 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2186)))->BgL_clausesz00);
					{
						obj_t BgL_l1258z00_2282;

						{	/* Ast/check_sharing.scm 179 */
							bool_t BgL_tmpz00_2674;

							BgL_l1258z00_2282 = BgL_g1260z00_2280;
						BgL_zc3z04anonymousza31474ze3z87_2281:
							if (PAIRP(BgL_l1258z00_2282))
								{	/* Ast/check_sharing.scm 179 */
									{	/* Ast/check_sharing.scm 180 */
										obj_t BgL_clausez00_2283;

										BgL_clausez00_2283 = CAR(BgL_l1258z00_2282);
										{	/* Ast/check_sharing.scm 180 */
											obj_t BgL_arg1485z00_2284;

											BgL_arg1485z00_2284 = CDR(((obj_t) BgL_clausez00_2283));
											BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2(
												((BgL_nodez00_bglt) BgL_arg1485z00_2284),
												((obj_t) ((BgL_switchz00_bglt) BgL_nodez00_2186)));
										}
									}
									{
										obj_t BgL_l1258z00_2684;

										BgL_l1258z00_2684 = CDR(BgL_l1258z00_2282);
										BgL_l1258z00_2282 = BgL_l1258z00_2684;
										goto BgL_zc3z04anonymousza31474ze3z87_2281;
									}
								}
							else
								{	/* Ast/check_sharing.scm 179 */
									BgL_tmpz00_2674 = ((bool_t) 1);
								}
							return BBOOL(BgL_tmpz00_2674);
						}
					}
				}
			}
		}

	}



/* &check-node-sharing-f1291 */
	obj_t BGl_z62checkzd2nodezd2sharingzd2f1291zb0zzast_checkzd2sharingzd2(obj_t
		BgL_envz00_2188, obj_t BgL_nodez00_2189, obj_t BgL_contextz00_2190)
	{
		{	/* Ast/check_sharing.scm 165 */
			{

				{	/* Ast/check_sharing.scm 165 */
					obj_t BgL_nextzd2method1290zd2_2287;

					BgL_nextzd2method1290zd2_2287 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_failz00_bglt) BgL_nodez00_2189)),
						BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
						BGl_failz00zzast_nodez00);
					BGL_PROCEDURE_CALL2(BgL_nextzd2method1290zd2_2287,
						((obj_t) ((BgL_failz00_bglt) BgL_nodez00_2189)),
						BgL_contextz00_2190);
				}
				{	/* Ast/check_sharing.scm 168 */
					BgL_nodez00_bglt BgL_arg1453z00_2288;

					BgL_arg1453z00_2288 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2189)))->BgL_procz00);
					BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2
						(BgL_arg1453z00_2288,
						((obj_t) ((BgL_failz00_bglt) BgL_nodez00_2189)));
				}
				{	/* Ast/check_sharing.scm 169 */
					BgL_nodez00_bglt BgL_arg1454z00_2289;

					BgL_arg1454z00_2289 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2189)))->BgL_msgz00);
					BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2
						(BgL_arg1454z00_2289,
						((obj_t) ((BgL_failz00_bglt) BgL_nodez00_2189)));
				}
				{	/* Ast/check_sharing.scm 170 */
					BgL_nodez00_bglt BgL_arg1472z00_2290;

					BgL_arg1472z00_2290 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2189)))->BgL_objz00);
					return
						BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2
						(BgL_arg1472z00_2290,
						((obj_t) ((BgL_failz00_bglt) BgL_nodez00_2189)));
				}
			}
		}

	}



/* &check-node-sharing-c1289 */
	obj_t BGl_z62checkzd2nodezd2sharingzd2c1289zb0zzast_checkzd2sharingzd2(obj_t
		BgL_envz00_2191, obj_t BgL_nodez00_2192, obj_t BgL_contextz00_2193)
	{
		{	/* Ast/check_sharing.scm 155 */
			{

				{	/* Ast/check_sharing.scm 155 */
					obj_t BgL_nextzd2method1288zd2_2293;

					BgL_nextzd2method1288zd2_2293 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_conditionalz00_bglt) BgL_nodez00_2192)),
						BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
						BGl_conditionalz00zzast_nodez00);
					BGL_PROCEDURE_CALL2(BgL_nextzd2method1288zd2_2293,
						((obj_t) ((BgL_conditionalz00_bglt) BgL_nodez00_2192)),
						BgL_contextz00_2193);
				}
				{	/* Ast/check_sharing.scm 158 */
					BgL_nodez00_bglt BgL_arg1434z00_2294;

					BgL_arg1434z00_2294 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2192)))->BgL_testz00);
					BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2
						(BgL_arg1434z00_2294,
						((obj_t) ((BgL_conditionalz00_bglt) BgL_nodez00_2192)));
				}
				{	/* Ast/check_sharing.scm 159 */
					BgL_nodez00_bglt BgL_arg1437z00_2295;

					BgL_arg1437z00_2295 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2192)))->BgL_truez00);
					BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2
						(BgL_arg1437z00_2295,
						((obj_t) ((BgL_conditionalz00_bglt) BgL_nodez00_2192)));
				}
				{	/* Ast/check_sharing.scm 160 */
					BgL_nodez00_bglt BgL_arg1448z00_2296;

					BgL_arg1448z00_2296 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2192)))->BgL_falsez00);
					return
						BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2
						(BgL_arg1448z00_2296,
						((obj_t) ((BgL_conditionalz00_bglt) BgL_nodez00_2192)));
				}
			}
		}

	}



/* &check-node-sharing-s1287 */
	obj_t BGl_z62checkzd2nodezd2sharingzd2s1287zb0zzast_checkzd2sharingzd2(obj_t
		BgL_envz00_2194, obj_t BgL_nodez00_2195, obj_t BgL_contextz00_2196)
	{
		{	/* Ast/check_sharing.scm 148 */
			{

				{	/* Ast/check_sharing.scm 148 */
					obj_t BgL_nextzd2method1286zd2_2299;

					BgL_nextzd2method1286zd2_2299 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_setqz00_bglt) BgL_nodez00_2195)),
						BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
						BGl_setqz00zzast_nodez00);
					BGL_PROCEDURE_CALL2(BgL_nextzd2method1286zd2_2299,
						((obj_t) ((BgL_setqz00_bglt) BgL_nodez00_2195)),
						BgL_contextz00_2196);
				}
				{	/* Ast/check_sharing.scm 150 */
					BgL_nodez00_bglt BgL_arg1422z00_2300;

					BgL_arg1422z00_2300 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2195)))->BgL_valuez00);
					return
						BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2
						(BgL_arg1422z00_2300,
						((obj_t) ((BgL_setqz00_bglt) BgL_nodez00_2195)));
				}
			}
		}

	}



/* &check-node-sharing-c1285 */
	obj_t BGl_z62checkzd2nodezd2sharingzd2c1285zb0zzast_checkzd2sharingzd2(obj_t
		BgL_envz00_2197, obj_t BgL_nodez00_2198, obj_t BgL_contextz00_2199)
	{
		{	/* Ast/check_sharing.scm 141 */
			{

				{	/* Ast/check_sharing.scm 141 */
					obj_t BgL_nextzd2method1284zd2_2303;

					BgL_nextzd2method1284zd2_2303 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_castz00_bglt) BgL_nodez00_2198)),
						BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
						BGl_castz00zzast_nodez00);
					BGL_PROCEDURE_CALL2(BgL_nextzd2method1284zd2_2303,
						((obj_t) ((BgL_castz00_bglt) BgL_nodez00_2198)),
						BgL_contextz00_2199);
				}
				{	/* Ast/check_sharing.scm 143 */
					BgL_nodez00_bglt BgL_arg1421z00_2304;

					BgL_arg1421z00_2304 =
						(((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nodez00_2198)))->BgL_argz00);
					return
						BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2
						(BgL_arg1421z00_2304,
						((obj_t) ((BgL_castz00_bglt) BgL_nodez00_2198)));
				}
			}
		}

	}



/* &check-node-sharing-e1283 */
	obj_t BGl_z62checkzd2nodezd2sharingzd2e1283zb0zzast_checkzd2sharingzd2(obj_t
		BgL_envz00_2200, obj_t BgL_nodez00_2201, obj_t BgL_contextz00_2202)
	{
		{	/* Ast/check_sharing.scm 134 */
			{

				{	/* Ast/check_sharing.scm 134 */
					obj_t BgL_nextzd2method1282zd2_2307;

					BgL_nextzd2method1282zd2_2307 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_externz00_bglt) BgL_nodez00_2201)),
						BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
						BGl_externz00zzast_nodez00);
					BGL_PROCEDURE_CALL2(BgL_nextzd2method1282zd2_2307,
						((obj_t) ((BgL_externz00_bglt) BgL_nodez00_2201)),
						BgL_contextz00_2202);
				}
				{	/* Ast/check_sharing.scm 136 */
					obj_t BgL_arg1410z00_2308;

					BgL_arg1410z00_2308 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_nodez00_2201)))->BgL_exprza2za2);
					return
						BBOOL(BGl_checkzd2nodezd2sharingza2za2zzast_checkzd2sharingzd2
						(BgL_arg1410z00_2308,
							((BgL_nodez00_bglt) ((BgL_externz00_bglt) BgL_nodez00_2201))));
				}
			}
		}

	}



/* &check-node-sharing-f1281 */
	obj_t BGl_z62checkzd2nodezd2sharingzd2f1281zb0zzast_checkzd2sharingzd2(obj_t
		BgL_envz00_2203, obj_t BgL_nodez00_2204, obj_t BgL_contextz00_2205)
	{
		{	/* Ast/check_sharing.scm 125 */
			{

				{	/* Ast/check_sharing.scm 125 */
					obj_t BgL_nextzd2method1280zd2_2311;

					BgL_nextzd2method1280zd2_2311 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_funcallz00_bglt) BgL_nodez00_2204)),
						BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
						BGl_funcallz00zzast_nodez00);
					BGL_PROCEDURE_CALL2(BgL_nextzd2method1280zd2_2311,
						((obj_t) ((BgL_funcallz00_bglt) BgL_nodez00_2204)),
						BgL_contextz00_2205);
				}
				{	/* Ast/check_sharing.scm 128 */
					BgL_nodez00_bglt BgL_arg1380z00_2312;

					BgL_arg1380z00_2312 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2204)))->BgL_funz00);
					BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2
						(BgL_arg1380z00_2312,
						((obj_t) ((BgL_funcallz00_bglt) BgL_nodez00_2204)));
				}
				{	/* Ast/check_sharing.scm 129 */
					obj_t BgL_arg1408z00_2313;

					BgL_arg1408z00_2313 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2204)))->BgL_argsz00);
					return
						BBOOL(BGl_checkzd2nodezd2sharingza2za2zzast_checkzd2sharingzd2
						(BgL_arg1408z00_2313,
							((BgL_nodez00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_2204))));
				}
			}
		}

	}



/* &check-node-sharing-a1279 */
	obj_t BGl_z62checkzd2nodezd2sharingzd2a1279zb0zzast_checkzd2sharingzd2(obj_t
		BgL_envz00_2206, obj_t BgL_nodez00_2207, obj_t BgL_contextz00_2208)
	{
		{	/* Ast/check_sharing.scm 116 */
			{

				{	/* Ast/check_sharing.scm 116 */
					obj_t BgL_nextzd2method1278zd2_2316;

					BgL_nextzd2method1278zd2_2316 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_2207)),
						BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
						BGl_appzd2lyzd2zzast_nodez00);
					BGL_PROCEDURE_CALL2(BgL_nextzd2method1278zd2_2316,
						((obj_t) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_2207)),
						BgL_contextz00_2208);
				}
				{	/* Ast/check_sharing.scm 119 */
					BgL_nodez00_bglt BgL_arg1378z00_2317;

					BgL_arg1378z00_2317 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2207)))->BgL_funz00);
					BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2
						(BgL_arg1378z00_2317,
						((obj_t) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_2207)));
				}
				{	/* Ast/check_sharing.scm 120 */
					BgL_nodez00_bglt BgL_arg1379z00_2318;

					BgL_arg1379z00_2318 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2207)))->BgL_argz00);
					return
						BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2
						(BgL_arg1379z00_2318,
						((obj_t) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_2207)));
				}
			}
		}

	}



/* &check-node-sharing-a1276 */
	obj_t BGl_z62checkzd2nodezd2sharingzd2a1276zb0zzast_checkzd2sharingzd2(obj_t
		BgL_envz00_2209, obj_t BgL_nodez00_2210, obj_t BgL_contextz00_2211)
	{
		{	/* Ast/check_sharing.scm 108 */
			{

				{	/* Ast/check_sharing.scm 108 */
					obj_t BgL_nextzd2method1275zd2_2321;

					BgL_nextzd2method1275zd2_2321 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_appz00_bglt) BgL_nodez00_2210)),
						BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
						BGl_appz00zzast_nodez00);
					BGL_PROCEDURE_CALL2(BgL_nextzd2method1275zd2_2321,
						((obj_t) ((BgL_appz00_bglt) BgL_nodez00_2210)),
						BgL_contextz00_2211);
				}
				{	/* Ast/check_sharing.scm 111 */
					obj_t BgL_arg1377z00_2322;

					BgL_arg1377z00_2322 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_2210)))->BgL_argsz00);
					return
						BBOOL(BGl_checkzd2nodezd2sharingza2za2zzast_checkzd2sharingzd2
						(BgL_arg1377z00_2322,
							((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_2210))));
				}
			}
		}

	}



/* &check-node-sharing-s1274 */
	obj_t BGl_z62checkzd2nodezd2sharingzd2s1274zb0zzast_checkzd2sharingzd2(obj_t
		BgL_envz00_2212, obj_t BgL_nodez00_2213, obj_t BgL_contextz00_2214)
	{
		{	/* Ast/check_sharing.scm 99 */
			{

				{	/* Ast/check_sharing.scm 99 */
					obj_t BgL_nextzd2method1273zd2_2325;

					BgL_nextzd2method1273zd2_2325 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_syncz00_bglt) BgL_nodez00_2213)),
						BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
						BGl_syncz00zzast_nodez00);
					BGL_PROCEDURE_CALL2(BgL_nextzd2method1273zd2_2325,
						((obj_t) ((BgL_syncz00_bglt) BgL_nodez00_2213)),
						BgL_contextz00_2214);
				}
				{	/* Ast/check_sharing.scm 101 */
					BgL_nodez00_bglt BgL_arg1371z00_2326;

					BgL_arg1371z00_2326 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2213)))->BgL_mutexz00);
					BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2
						(BgL_arg1371z00_2326,
						((obj_t) ((BgL_syncz00_bglt) BgL_nodez00_2213)));
				}
				{	/* Ast/check_sharing.scm 102 */
					BgL_nodez00_bglt BgL_arg1375z00_2327;

					BgL_arg1375z00_2327 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2213)))->BgL_prelockz00);
					BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2
						(BgL_arg1375z00_2327,
						((obj_t) ((BgL_syncz00_bglt) BgL_nodez00_2213)));
				}
				{	/* Ast/check_sharing.scm 103 */
					BgL_nodez00_bglt BgL_arg1376z00_2328;

					BgL_arg1376z00_2328 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2213)))->BgL_bodyz00);
					return
						BGl_checkzd2nodezd2sharingz00zzast_checkzd2sharingzd2
						(BgL_arg1376z00_2328,
						((obj_t) ((BgL_syncz00_bglt) BgL_nodez00_2213)));
				}
			}
		}

	}



/* &check-node-sharing-s1272 */
	obj_t BGl_z62checkzd2nodezd2sharingzd2s1272zb0zzast_checkzd2sharingzd2(obj_t
		BgL_envz00_2215, obj_t BgL_nodez00_2216, obj_t BgL_contextz00_2217)
	{
		{	/* Ast/check_sharing.scm 92 */
			{

				{	/* Ast/check_sharing.scm 92 */
					obj_t BgL_nextzd2method1271zd2_2331;

					BgL_nextzd2method1271zd2_2331 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_sequencez00_bglt) BgL_nodez00_2216)),
						BGl_checkzd2nodezd2sharingzd2envzd2zzast_checkzd2sharingzd2,
						BGl_sequencez00zzast_nodez00);
					BGL_PROCEDURE_CALL2(BgL_nextzd2method1271zd2_2331,
						((obj_t) ((BgL_sequencez00_bglt) BgL_nodez00_2216)),
						BgL_contextz00_2217);
				}
				{	/* Ast/check_sharing.scm 94 */
					obj_t BgL_arg1370z00_2332;

					BgL_arg1370z00_2332 =
						(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_2216)))->BgL_nodesz00);
					return
						BBOOL(BGl_checkzd2nodezd2sharingza2za2zzast_checkzd2sharingzd2
						(BgL_arg1370z00_2332,
							((BgL_nodez00_bglt) ((BgL_sequencez00_bglt) BgL_nodez00_2216))));
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_checkzd2sharingzd2(void)
	{
		{	/* Ast/check_sharing.scm 17 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1773z00zzast_checkzd2sharingzd2));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1773z00zzast_checkzd2sharingzd2));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1773z00zzast_checkzd2sharingzd2));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1773z00zzast_checkzd2sharingzd2));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1773z00zzast_checkzd2sharingzd2));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1773z00zzast_checkzd2sharingzd2));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1773z00zzast_checkzd2sharingzd2));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1773z00zzast_checkzd2sharingzd2));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1773z00zzast_checkzd2sharingzd2));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1773z00zzast_checkzd2sharingzd2));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1773z00zzast_checkzd2sharingzd2));
			BGl_modulezd2initializa7ationz75zzmodule_includez00(184800657L,
				BSTRING_TO_STRING(BGl_string1773z00zzast_checkzd2sharingzd2));
			return
				BGl_modulezd2initializa7ationz75zzmodule_classz00(153808441L,
				BSTRING_TO_STRING(BGl_string1773z00zzast_checkzd2sharingzd2));
		}

	}

#ifdef __cplusplus
}
#endif
