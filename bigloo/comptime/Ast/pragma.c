/*===========================================================================*/
/*   (Ast/pragma.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/pragma.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_AST_PRAGMA_TYPE_DEFINITIONS
#define BGL_AST_PRAGMA_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_pragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_formatz00;
		obj_t BgL_srfi0z00;
	}                *BgL_pragmaz00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;


#endif													// BGL_AST_PRAGMA_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static BgL_nodez00_bglt
		BGl_z62pragmazf2typezd2ze3nodeza1zzast_pragmaz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzast_pragmaz00 = BUNSPEC;
	static obj_t BGl_z62getzd2staticzd2pragmasz62zzast_pragmaz00(obj_t);
	extern obj_t BGl_findzd2locationzf2locz20zztools_locationz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_toplevelzd2initzd2zzast_pragmaz00(void);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzast_pragmaz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_addzd2staticzd2pragmaz12z12zzast_pragmaz00(BgL_nodez00_bglt);
	static obj_t BGl_objectzd2initzd2zzast_pragmaz00(void);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzast_pragmaz00(void);
	extern obj_t BGl_varz00zzast_nodez00;
	BGL_IMPORT obj_t rgc_buffer_substring(obj_t, long, long);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_getzd2staticzd2pragmasz00zzast_pragmaz00(void);
	extern obj_t BGl_pragmaz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzast_pragmaz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__rgcz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_getzd2maxzd2indexz00zzast_pragmaz00(obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_pragmazf2typezd2ze3nodezc3zzast_pragmaz00(bool_t, obj_t,
		BgL_typez00_bglt, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzast_pragmaz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_pragmaz00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	extern BgL_nodez00_bglt BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzast_pragmaz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzast_pragmaz00(void);
	static obj_t BGl_parserze70ze7zzast_pragmaz00(obj_t);
	static obj_t BGl_z62addzd2staticzd2pragmaz12z70zzast_pragmaz00(obj_t, obj_t);
	static obj_t BGl_za2staticzd2pragmazd2listza2z00zzast_pragmaz00 = BUNSPEC;
	extern BgL_nodez00_bglt BGl_sexpzd2ze3nodez31zzast_sexpz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_userzd2warningzf2locationz20zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT bool_t rgc_fill_buffer(obj_t);
	static obj_t __cnst[4];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2staticzd2pragmaszd2envzd2zzast_pragmaz00,
		BgL_bgl_za762getza7d2staticza71883za7,
		BGl_z62getzd2staticzd2pragmasz62zzast_pragmaz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1869z00zzast_pragmaz00,
		BgL_bgl_string1869za700za7za7a1884za7,
		"Wrong number of arguments in `pragma' form", 42);
	      DEFINE_STRING(BGl_string1870z00zzast_pragmaz00,
		BgL_bgl_string1870za700za7za7a1885za7, "", 0);
	      DEFINE_STRING(BGl_string1871z00zzast_pragmaz00,
		BgL_bgl_string1871za700za7za7a1886za7, "Illegal `pragma' expression", 27);
	      DEFINE_STRING(BGl_string1872z00zzast_pragmaz00,
		BgL_bgl_string1872za700za7za7a1887za7, "Illegal \"pragma\" form", 21);
	      DEFINE_STRING(BGl_string1873z00zzast_pragmaz00,
		BgL_bgl_string1873za700za7za7a1888za7, "pragma", 6);
	      DEFINE_STRING(BGl_string1874z00zzast_pragmaz00,
		BgL_bgl_string1874za700za7za7a1889za7, "Pragma ignored with this back-end",
		33);
	      DEFINE_STRING(BGl_string1875z00zzast_pragmaz00,
		BgL_bgl_string1875za700za7za7a1890za7, "regular-grammar", 15);
	      DEFINE_STRING(BGl_string1876z00zzast_pragmaz00,
		BgL_bgl_string1876za700za7za7a1891za7, "Illegal match", 13);
	      DEFINE_STRING(BGl_string1877z00zzast_pragmaz00,
		BgL_bgl_string1877za700za7za7a1892za7, "Illegal range `~a'", 18);
	      DEFINE_STRING(BGl_string1878z00zzast_pragmaz00,
		BgL_bgl_string1878za700za7za7a1893za7, "the-substring", 13);
	      DEFINE_STRING(BGl_string1879z00zzast_pragmaz00,
		BgL_bgl_string1879za700za7za7a1894za7, "ast_pragma", 10);
	      DEFINE_STRING(BGl_string1880z00zzast_pragmaz00,
		BgL_bgl_string1880za700za7za7a1895za7, ":srfi set! value bigloo-c ", 26);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_addzd2staticzd2pragmaz12zd2envzc0zzast_pragmaz00,
		BgL_bgl_za762addza7d2staticza71896za7,
		BGl_z62addzd2staticzd2pragmaz12z70zzast_pragmaz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_pragmazf2typezd2ze3nodezd2envz11zzast_pragmaz00,
		BgL_bgl_za762pragmaza7f2type1897z00,
		BGl_z62pragmazf2typezd2ze3nodeza1zzast_pragmaz00, 0L, BUNSPEC, 7);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzast_pragmaz00));
		   
			 ADD_ROOT((void *) (&BGl_za2staticzd2pragmazd2listza2z00zzast_pragmaz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzast_pragmaz00(long
		BgL_checksumz00_2279, char *BgL_fromz00_2280)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_pragmaz00))
				{
					BGl_requirezd2initializa7ationz75zzast_pragmaz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_pragmaz00();
					BGl_libraryzd2moduleszd2initz00zzast_pragmaz00();
					BGl_cnstzd2initzd2zzast_pragmaz00();
					BGl_importedzd2moduleszd2initz00zzast_pragmaz00();
					return BGl_toplevelzd2initzd2zzast_pragmaz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_pragmaz00(void)
	{
		{	/* Ast/pragma.scm 14 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "ast_pragma");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "ast_pragma");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_pragma");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_pragma");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "ast_pragma");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "ast_pragma");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "ast_pragma");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L,
				"ast_pragma");
			BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(0L, "ast_pragma");
			BGl_modulezd2initializa7ationz75zz__rgcz00(0L, "ast_pragma");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "ast_pragma");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "ast_pragma");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"ast_pragma");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "ast_pragma");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"ast_pragma");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_pragmaz00(void)
	{
		{	/* Ast/pragma.scm 14 */
			{	/* Ast/pragma.scm 14 */
				obj_t BgL_cportz00_2268;

				{	/* Ast/pragma.scm 14 */
					obj_t BgL_stringz00_2275;

					BgL_stringz00_2275 = BGl_string1880z00zzast_pragmaz00;
					{	/* Ast/pragma.scm 14 */
						obj_t BgL_startz00_2276;

						BgL_startz00_2276 = BINT(0L);
						{	/* Ast/pragma.scm 14 */
							obj_t BgL_endz00_2277;

							BgL_endz00_2277 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2275)));
							{	/* Ast/pragma.scm 14 */

								BgL_cportz00_2268 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2275, BgL_startz00_2276, BgL_endz00_2277);
				}}}}
				{
					long BgL_iz00_2269;

					BgL_iz00_2269 = 3L;
				BgL_loopz00_2270:
					if ((BgL_iz00_2269 == -1L))
						{	/* Ast/pragma.scm 14 */
							return BUNSPEC;
						}
					else
						{	/* Ast/pragma.scm 14 */
							{	/* Ast/pragma.scm 14 */
								obj_t BgL_arg1882z00_2271;

								{	/* Ast/pragma.scm 14 */

									{	/* Ast/pragma.scm 14 */
										obj_t BgL_locationz00_2273;

										BgL_locationz00_2273 = BBOOL(((bool_t) 0));
										{	/* Ast/pragma.scm 14 */

											BgL_arg1882z00_2271 =
												BGl_readz00zz__readerz00(BgL_cportz00_2268,
												BgL_locationz00_2273);
										}
									}
								}
								{	/* Ast/pragma.scm 14 */
									int BgL_tmpz00_2313;

									BgL_tmpz00_2313 = (int) (BgL_iz00_2269);
									CNST_TABLE_SET(BgL_tmpz00_2313, BgL_arg1882z00_2271);
							}}
							{	/* Ast/pragma.scm 14 */
								int BgL_auxz00_2274;

								BgL_auxz00_2274 = (int) ((BgL_iz00_2269 - 1L));
								{
									long BgL_iz00_2318;

									BgL_iz00_2318 = (long) (BgL_auxz00_2274);
									BgL_iz00_2269 = BgL_iz00_2318;
									goto BgL_loopz00_2270;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_pragmaz00(void)
	{
		{	/* Ast/pragma.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzast_pragmaz00(void)
	{
		{	/* Ast/pragma.scm 14 */
			return (BGl_za2staticzd2pragmazd2listza2z00zzast_pragmaz00 =
				BNIL, BUNSPEC);
		}

	}



/* get-static-pragmas */
	BGL_EXPORTED_DEF obj_t BGl_getzd2staticzd2pragmasz00zzast_pragmaz00(void)
	{
		{	/* Ast/pragma.scm 33 */
			return
				bgl_reverse_bang(BGl_za2staticzd2pragmazd2listza2z00zzast_pragmaz00);
		}

	}



/* &get-static-pragmas */
	obj_t BGl_z62getzd2staticzd2pragmasz62zzast_pragmaz00(obj_t BgL_envz00_2257)
	{
		{	/* Ast/pragma.scm 33 */
			return BGl_getzd2staticzd2pragmasz00zzast_pragmaz00();
		}

	}



/* add-static-pragma! */
	BGL_EXPORTED_DEF obj_t
		BGl_addzd2staticzd2pragmaz12z12zzast_pragmaz00(BgL_nodez00_bglt BgL_pz00_3)
	{
		{	/* Ast/pragma.scm 39 */
			return (BGl_za2staticzd2pragmazd2listza2z00zzast_pragmaz00 =
				MAKE_YOUNG_PAIR(
					((obj_t) BgL_pz00_3),
					BGl_za2staticzd2pragmazd2listza2z00zzast_pragmaz00), BUNSPEC);
		}

	}



/* &add-static-pragma! */
	obj_t BGl_z62addzd2staticzd2pragmaz12z70zzast_pragmaz00(obj_t BgL_envz00_2258,
		obj_t BgL_pz00_2259)
	{
		{	/* Ast/pragma.scm 39 */
			return
				BGl_addzd2staticzd2pragmaz12z12zzast_pragmaz00(
				((BgL_nodez00_bglt) BgL_pz00_2259));
		}

	}



/* pragma/type->node */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_pragmazf2typezd2ze3nodezc3zzast_pragmaz00(bool_t BgL_freez00_4,
		obj_t BgL_effectz00_5, BgL_typez00_bglt BgL_typez00_6, obj_t BgL_expz00_7,
		obj_t BgL_stackz00_8, obj_t BgL_locz00_9, obj_t BgL_sitez00_10)
	{
		{	/* Ast/pragma.scm 45 */
			{	/* Ast/pragma.scm 46 */
				bool_t BgL_test1900z00_2327;

				{	/* Ast/pragma.scm 46 */
					obj_t BgL_arg1502z00_1598;

					BgL_arg1502z00_1598 = BGl_thezd2backendzd2zzbackend_backendz00();
					BgL_test1900z00_2327 =
						(((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_arg1502z00_1598)))->
						BgL_pragmazd2supportzd2);
				}
				if (BgL_test1900z00_2327)
					{
						obj_t BgL_srfiz00_1469;
						obj_t BgL_identz00_1470;
						obj_t BgL_identz00_1467;
						obj_t BgL_srfiz00_1463;
						obj_t BgL_formatz00_1464;
						obj_t BgL_valuesz00_1465;
						obj_t BgL_formatz00_1460;
						obj_t BgL_valuesz00_1461;

						if (PAIRP(BgL_expz00_7))
							{	/* Ast/pragma.scm 53 */
								obj_t BgL_cdrzd2114zd2_1475;

								BgL_cdrzd2114zd2_1475 = CDR(((obj_t) BgL_expz00_7));
								if (PAIRP(BgL_cdrzd2114zd2_1475))
									{	/* Ast/pragma.scm 53 */
										obj_t BgL_carzd2117zd2_1477;

										BgL_carzd2117zd2_1477 = CAR(BgL_cdrzd2114zd2_1475);
										if (STRINGP(BgL_carzd2117zd2_1477))
											{	/* Ast/pragma.scm 53 */
												BgL_formatz00_1460 = BgL_carzd2117zd2_1477;
												BgL_valuesz00_1461 = CDR(BgL_cdrzd2114zd2_1475);
												{	/* Ast/pragma.scm 55 */
													obj_t BgL_maxzd2indexzd2_1536;
													obj_t BgL_locz00_1537;

													BgL_maxzd2indexzd2_1536 =
														BGl_getzd2maxzd2indexz00zzast_pragmaz00
														(BgL_formatz00_1460);
													BgL_locz00_1537 =
														BGl_findzd2locationzf2locz20zztools_locationz00
														(BgL_expz00_7, BgL_locz00_9);
													if (((long) CINT(BgL_maxzd2indexzd2_1536) ==
															bgl_list_length(BgL_valuesz00_1461)))
														{
															obj_t BgL_expsz00_1542;
															obj_t BgL_nodesz00_1543;

															{
																BgL_pragmaz00_bglt BgL_auxz00_2346;

																BgL_expsz00_1542 = BgL_valuesz00_1461;
																BgL_nodesz00_1543 = BNIL;
															BgL_zc3z04anonymousza31345ze3z87_1544:
																if (NULLP(BgL_expsz00_1542))
																	{	/* Ast/pragma.scm 65 */
																		BgL_pragmaz00_bglt BgL_new1105z00_1546;

																		{	/* Ast/pragma.scm 66 */
																			BgL_pragmaz00_bglt BgL_new1104z00_1547;

																			BgL_new1104z00_1547 =
																				((BgL_pragmaz00_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_pragmaz00_bgl))));
																			{	/* Ast/pragma.scm 66 */
																				long BgL_arg1348z00_1548;

																				BgL_arg1348z00_1548 =
																					BGL_CLASS_NUM
																					(BGl_pragmaz00zzast_nodez00);
																				BGL_OBJECT_CLASS_NUM_SET((
																						(BgL_objectz00_bglt)
																						BgL_new1104z00_1547),
																					BgL_arg1348z00_1548);
																			}
																			{	/* Ast/pragma.scm 66 */
																				BgL_objectz00_bglt BgL_tmpz00_2353;

																				BgL_tmpz00_2353 =
																					((BgL_objectz00_bglt)
																					BgL_new1104z00_1547);
																				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2353,
																					BFALSE);
																			}
																			((BgL_objectz00_bglt)
																				BgL_new1104z00_1547);
																			BgL_new1105z00_1546 = BgL_new1104z00_1547;
																		}
																		((((BgL_nodez00_bglt) COBJECT(
																						((BgL_nodez00_bglt)
																							BgL_new1105z00_1546)))->
																				BgL_locz00) =
																			((obj_t) BgL_locz00_1537), BUNSPEC);
																		((((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt)
																							BgL_new1105z00_1546)))->
																				BgL_typez00) =
																			((BgL_typez00_bglt) BgL_typez00_6),
																			BUNSPEC);
																		{
																			obj_t BgL_auxz00_2361;

																			if (BgL_freez00_4)
																				{	/* Ast/pragma.scm 70 */
																					BgL_auxz00_2361 = BFALSE;
																				}
																			else
																				{	/* Ast/pragma.scm 70 */
																					BgL_auxz00_2361 = BTRUE;
																				}
																			((((BgL_nodezf2effectzf2_bglt) COBJECT(
																							((BgL_nodezf2effectzf2_bglt)
																								BgL_new1105z00_1546)))->
																					BgL_sidezd2effectzd2) =
																				((obj_t) BgL_auxz00_2361), BUNSPEC);
																		}
																		((((BgL_nodezf2effectzf2_bglt) COBJECT(
																						((BgL_nodezf2effectzf2_bglt)
																							BgL_new1105z00_1546)))->
																				BgL_keyz00) =
																			((obj_t) BINT(-1L)), BUNSPEC);
																		((((BgL_externz00_bglt)
																					COBJECT(((BgL_externz00_bglt)
																							BgL_new1105z00_1546)))->
																				BgL_exprza2za2) =
																			((obj_t)
																				bgl_reverse_bang(BgL_nodesz00_1543)),
																			BUNSPEC);
																		((((BgL_externz00_bglt)
																					COBJECT(((BgL_externz00_bglt)
																							BgL_new1105z00_1546)))->
																				BgL_effectz00) =
																			((obj_t) BgL_effectz00_5), BUNSPEC);
																		((((BgL_pragmaz00_bglt)
																					COBJECT(BgL_new1105z00_1546))->
																				BgL_formatz00) =
																			((obj_t) BgL_formatz00_1460), BUNSPEC);
																		((((BgL_pragmaz00_bglt)
																					COBJECT(BgL_new1105z00_1546))->
																				BgL_srfi0z00) =
																			((obj_t) CNST_TABLE_REF(0)), BUNSPEC);
																		BgL_auxz00_2346 = BgL_new1105z00_1546;
																	}
																else
																	{	/* Ast/pragma.scm 72 */
																		obj_t BgL_arg1349z00_1549;
																		obj_t BgL_arg1351z00_1550;

																		BgL_arg1349z00_1549 =
																			CDR(((obj_t) BgL_expsz00_1542));
																		{	/* Ast/pragma.scm 74 */
																			BgL_nodez00_bglt BgL_arg1352z00_1551;

																			{	/* Ast/pragma.scm 74 */
																				obj_t BgL_arg1361z00_1552;
																				obj_t BgL_arg1364z00_1553;
																				obj_t BgL_arg1367z00_1554;

																				BgL_arg1361z00_1552 =
																					CAR(((obj_t) BgL_expsz00_1542));
																				{	/* Ast/pragma.scm 76 */
																					obj_t BgL_arg1370z00_1555;

																					BgL_arg1370z00_1555 =
																						CAR(((obj_t) BgL_expsz00_1542));
																					BgL_arg1364z00_1553 =
																						BGl_findzd2locationzf2locz20zztools_locationz00
																						(BgL_arg1370z00_1555,
																						BgL_locz00_1537);
																				}
																				if (BgL_freez00_4)
																					{	/* Ast/pragma.scm 77 */
																						BgL_arg1367z00_1554 =
																							CNST_TABLE_REF(1);
																					}
																				else
																					{	/* Ast/pragma.scm 77 */
																						BgL_arg1367z00_1554 =
																							CNST_TABLE_REF(2);
																					}
																				BgL_arg1352z00_1551 =
																					BGl_sexpzd2ze3nodez31zzast_sexpz00
																					(BgL_arg1361z00_1552, BgL_stackz00_8,
																					BgL_arg1364z00_1553,
																					BgL_arg1367z00_1554);
																			}
																			BgL_arg1351z00_1550 =
																				MAKE_YOUNG_PAIR(
																				((obj_t) BgL_arg1352z00_1551),
																				BgL_nodesz00_1543);
																		}
																		{
																			obj_t BgL_nodesz00_2390;
																			obj_t BgL_expsz00_2389;

																			BgL_expsz00_2389 = BgL_arg1349z00_1549;
																			BgL_nodesz00_2390 = BgL_arg1351z00_1550;
																			BgL_nodesz00_1543 = BgL_nodesz00_2390;
																			BgL_expsz00_1542 = BgL_expsz00_2389;
																			goto
																				BgL_zc3z04anonymousza31345ze3z87_1544;
																		}
																	}
																return ((BgL_nodez00_bglt) BgL_auxz00_2346);
															}
														}
													else
														{	/* Ast/pragma.scm 57 */
															return
																BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00
																(BGl_string1869z00zzast_pragmaz00, BgL_expz00_7,
																BgL_locz00_1537);
														}
												}
											}
										else
											{	/* Ast/pragma.scm 53 */
												obj_t BgL_cdrzd2136zd2_1481;

												BgL_cdrzd2136zd2_1481 =
													CDR(((obj_t) BgL_cdrzd2114zd2_1475));
												if (
													(CAR(
															((obj_t) BgL_cdrzd2114zd2_1475)) ==
														CNST_TABLE_REF(3)))
													{	/* Ast/pragma.scm 53 */
														if (PAIRP(BgL_cdrzd2136zd2_1481))
															{	/* Ast/pragma.scm 53 */
																obj_t BgL_carzd2140zd2_1485;
																obj_t BgL_cdrzd2141zd2_1486;

																BgL_carzd2140zd2_1485 =
																	CAR(BgL_cdrzd2136zd2_1481);
																BgL_cdrzd2141zd2_1486 =
																	CDR(BgL_cdrzd2136zd2_1481);
																if (SYMBOLP(BgL_carzd2140zd2_1485))
																	{	/* Ast/pragma.scm 53 */
																		if (PAIRP(BgL_cdrzd2141zd2_1486))
																			{	/* Ast/pragma.scm 53 */
																				obj_t BgL_carzd2147zd2_1489;

																				BgL_carzd2147zd2_1489 =
																					CAR(BgL_cdrzd2141zd2_1486);
																				if (STRINGP(BgL_carzd2147zd2_1489))
																					{	/* Ast/pragma.scm 53 */
																						BgL_srfiz00_1463 =
																							BgL_carzd2140zd2_1485;
																						BgL_formatz00_1464 =
																							BgL_carzd2147zd2_1489;
																						BgL_valuesz00_1465 =
																							CDR(BgL_cdrzd2141zd2_1486);
																						{	/* Ast/pragma.scm 80 */
																							obj_t BgL_maxzd2indexzd2_1558;
																							obj_t BgL_locz00_1559;

																							BgL_maxzd2indexzd2_1558 =
																								BGl_getzd2maxzd2indexz00zzast_pragmaz00
																								(BgL_formatz00_1464);
																							BgL_locz00_1559 =
																								BGl_findzd2locationzf2locz20zztools_locationz00
																								(BgL_expz00_7, BgL_locz00_9);
																							if (((long)
																									CINT(BgL_maxzd2indexzd2_1558)
																									==
																									bgl_list_length
																									(BgL_valuesz00_1465)))
																								{
																									obj_t BgL_expsz00_1564;
																									obj_t BgL_nodesz00_1565;

																									{
																										BgL_pragmaz00_bglt
																											BgL_auxz00_2418;
																										BgL_expsz00_1564 =
																											BgL_valuesz00_1465;
																										BgL_nodesz00_1565 = BNIL;
																									BgL_zc3z04anonymousza31383ze3z87_1566:
																										if (NULLP
																											(BgL_expsz00_1564))
																											{	/* Ast/pragma.scm 90 */
																												BgL_pragmaz00_bglt
																													BgL_new1108z00_1568;
																												{	/* Ast/pragma.scm 91 */
																													BgL_pragmaz00_bglt
																														BgL_new1107z00_1569;
																													BgL_new1107z00_1569 =
																														(
																														(BgL_pragmaz00_bglt)
																														BOBJECT(GC_MALLOC
																															(sizeof(struct
																																	BgL_pragmaz00_bgl))));
																													{	/* Ast/pragma.scm 91 */
																														long
																															BgL_arg1408z00_1570;
																														BgL_arg1408z00_1570
																															=
																															BGL_CLASS_NUM
																															(BGl_pragmaz00zzast_nodez00);
																														BGL_OBJECT_CLASS_NUM_SET
																															(((BgL_objectz00_bglt) BgL_new1107z00_1569), BgL_arg1408z00_1570);
																													}
																													{	/* Ast/pragma.scm 91 */
																														BgL_objectz00_bglt
																															BgL_tmpz00_2425;
																														BgL_tmpz00_2425 =
																															(
																															(BgL_objectz00_bglt)
																															BgL_new1107z00_1569);
																														BGL_OBJECT_WIDENING_SET
																															(BgL_tmpz00_2425,
																															BFALSE);
																													}
																													((BgL_objectz00_bglt)
																														BgL_new1107z00_1569);
																													BgL_new1108z00_1568 =
																														BgL_new1107z00_1569;
																												}
																												((((BgL_nodez00_bglt)
																															COBJECT((
																																	(BgL_nodez00_bglt)
																																	BgL_new1108z00_1568)))->
																														BgL_locz00) =
																													((obj_t)
																														BgL_locz00_1559),
																													BUNSPEC);
																												((((BgL_nodez00_bglt)
																															COBJECT((
																																	(BgL_nodez00_bglt)
																																	BgL_new1108z00_1568)))->
																														BgL_typez00) =
																													((BgL_typez00_bglt)
																														BgL_typez00_6),
																													BUNSPEC);
																												{
																													obj_t BgL_auxz00_2433;

																													if (BgL_freez00_4)
																														{	/* Ast/pragma.scm 96 */
																															BgL_auxz00_2433 =
																																BFALSE;
																														}
																													else
																														{	/* Ast/pragma.scm 96 */
																															BgL_auxz00_2433 =
																																BTRUE;
																														}
																													((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1108z00_1568)))->BgL_sidezd2effectzd2) = ((obj_t) BgL_auxz00_2433), BUNSPEC);
																												}
																												((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1108z00_1568)))->BgL_keyz00) = ((obj_t) BINT(-1L)), BUNSPEC);
																												((((BgL_externz00_bglt)
																															COBJECT((
																																	(BgL_externz00_bglt)
																																	BgL_new1108z00_1568)))->
																														BgL_exprza2za2) =
																													((obj_t)
																														bgl_reverse_bang
																														(BgL_nodesz00_1565)),
																													BUNSPEC);
																												((((BgL_externz00_bglt)
																															COBJECT((
																																	(BgL_externz00_bglt)
																																	BgL_new1108z00_1568)))->
																														BgL_effectz00) =
																													((obj_t)
																														BgL_effectz00_5),
																													BUNSPEC);
																												((((BgL_pragmaz00_bglt)
																															COBJECT
																															(BgL_new1108z00_1568))->
																														BgL_formatz00) =
																													((obj_t)
																														BgL_formatz00_1464),
																													BUNSPEC);
																												((((BgL_pragmaz00_bglt)
																															COBJECT
																															(BgL_new1108z00_1568))->
																														BgL_srfi0z00) =
																													((obj_t)
																														BgL_srfiz00_1463),
																													BUNSPEC);
																												BgL_auxz00_2418 =
																													BgL_new1108z00_1568;
																											}
																										else
																											{	/* Ast/pragma.scm 98 */
																												obj_t
																													BgL_arg1410z00_1571;
																												obj_t
																													BgL_arg1421z00_1572;
																												BgL_arg1410z00_1571 =
																													CDR(((obj_t)
																														BgL_expsz00_1564));
																												{	/* Ast/pragma.scm 100 */
																													BgL_nodez00_bglt
																														BgL_arg1422z00_1573;
																													{	/* Ast/pragma.scm 100 */
																														obj_t
																															BgL_arg1434z00_1574;
																														obj_t
																															BgL_arg1437z00_1575;
																														obj_t
																															BgL_arg1448z00_1576;
																														BgL_arg1434z00_1574
																															=
																															CAR(((obj_t)
																																BgL_expsz00_1564));
																														{	/* Ast/pragma.scm 102 */
																															obj_t
																																BgL_arg1453z00_1577;
																															BgL_arg1453z00_1577
																																=
																																CAR(((obj_t)
																																	BgL_expsz00_1564));
																															BgL_arg1437z00_1575
																																=
																																BGl_findzd2locationzf2locz20zztools_locationz00
																																(BgL_arg1453z00_1577,
																																BgL_locz00_1559);
																														}
																														if (BgL_freez00_4)
																															{	/* Ast/pragma.scm 103 */
																																BgL_arg1448z00_1576
																																	=
																																	CNST_TABLE_REF
																																	(1);
																															}
																														else
																															{	/* Ast/pragma.scm 103 */
																																BgL_arg1448z00_1576
																																	=
																																	CNST_TABLE_REF
																																	(2);
																															}
																														BgL_arg1422z00_1573
																															=
																															BGl_sexpzd2ze3nodez31zzast_sexpz00
																															(BgL_arg1434z00_1574,
																															BgL_stackz00_8,
																															BgL_arg1437z00_1575,
																															BgL_arg1448z00_1576);
																													}
																													BgL_arg1421z00_1572 =
																														MAKE_YOUNG_PAIR(
																														((obj_t)
																															BgL_arg1422z00_1573),
																														BgL_nodesz00_1565);
																												}
																												{
																													obj_t
																														BgL_nodesz00_2461;
																													obj_t
																														BgL_expsz00_2460;
																													BgL_expsz00_2460 =
																														BgL_arg1410z00_1571;
																													BgL_nodesz00_2461 =
																														BgL_arg1421z00_1572;
																													BgL_nodesz00_1565 =
																														BgL_nodesz00_2461;
																													BgL_expsz00_1564 =
																														BgL_expsz00_2460;
																													goto
																														BgL_zc3z04anonymousza31383ze3z87_1566;
																												}
																											}
																										return
																											((BgL_nodez00_bglt)
																											BgL_auxz00_2418);
																									}
																								}
																							else
																								{	/* Ast/pragma.scm 82 */
																									return
																										BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00
																										(BGl_string1869z00zzast_pragmaz00,
																										BgL_expz00_7,
																										BgL_locz00_1559);
																								}
																						}
																					}
																				else
																					{	/* Ast/pragma.scm 53 */
																						obj_t BgL_cdrzd2167zd2_1492;

																						{	/* Ast/pragma.scm 53 */
																							obj_t BgL_pairz00_2148;

																							BgL_pairz00_2148 =
																								CDR(((obj_t) BgL_expz00_7));
																							BgL_cdrzd2167zd2_1492 =
																								CDR(BgL_pairz00_2148);
																						}
																						{	/* Ast/pragma.scm 53 */
																							obj_t BgL_carzd2171zd2_1493;
																							obj_t BgL_cdrzd2172zd2_1494;

																							BgL_carzd2171zd2_1493 =
																								CAR(
																								((obj_t)
																									BgL_cdrzd2167zd2_1492));
																							BgL_cdrzd2172zd2_1494 =
																								CDR(((obj_t)
																									BgL_cdrzd2167zd2_1492));
																							if (SYMBOLP
																								(BgL_carzd2171zd2_1493))
																								{	/* Ast/pragma.scm 53 */
																									if (NULLP(CDR(
																												((obj_t)
																													BgL_cdrzd2172zd2_1494))))
																										{	/* Ast/pragma.scm 53 */
																											obj_t BgL_arg1306z00_1498;

																											BgL_arg1306z00_1498 =
																												CAR(
																												((obj_t)
																													BgL_cdrzd2172zd2_1494));
																											BgL_srfiz00_1469 =
																												BgL_carzd2171zd2_1493;
																											BgL_identz00_1470 =
																												BgL_arg1306z00_1498;
																										BgL_tagzd2104zd2_1471:
																											{	/* Ast/pragma.scm 120 */
																												BgL_nodez00_bglt
																													BgL_vz00_1589;
																												BgL_vz00_1589 =
																													BGl_sexpzd2ze3nodez31zzast_sexpz00
																													(BgL_identz00_1470,
																													BgL_stackz00_8,
																													BgL_locz00_9,
																													BgL_sitez00_10);
																												{	/* Ast/pragma.scm 121 */
																													bool_t
																														BgL_test1919z00_2481;
																													{	/* Ast/pragma.scm 121 */
																														obj_t
																															BgL_classz00_2098;
																														BgL_classz00_2098 =
																															BGl_varz00zzast_nodez00;
																														{	/* Ast/pragma.scm 121 */
																															BgL_objectz00_bglt
																																BgL_arg1807z00_2100;
																															{	/* Ast/pragma.scm 121 */
																																obj_t
																																	BgL_tmpz00_2482;
																																BgL_tmpz00_2482
																																	=
																																	((obj_t) (
																																		(BgL_objectz00_bglt)
																																		BgL_vz00_1589));
																																BgL_arg1807z00_2100
																																	=
																																	(BgL_objectz00_bglt)
																																	(BgL_tmpz00_2482);
																															}
																															if (BGL_CONDEXPAND_ISA_ARCH64())
																																{	/* Ast/pragma.scm 121 */
																																	long
																																		BgL_idxz00_2106;
																																	BgL_idxz00_2106
																																		=
																																		BGL_OBJECT_INHERITANCE_NUM
																																		(BgL_arg1807z00_2100);
																																	BgL_test1919z00_2481
																																		=
																																		(VECTOR_REF
																																		(BGl_za2inheritancesza2z00zz__objectz00,
																																			(BgL_idxz00_2106
																																				+
																																				2L)) ==
																																		BgL_classz00_2098);
																																}
																															else
																																{	/* Ast/pragma.scm 121 */
																																	bool_t
																																		BgL_res1867z00_2131;
																																	{	/* Ast/pragma.scm 121 */
																																		obj_t
																																			BgL_oclassz00_2114;
																																		{	/* Ast/pragma.scm 121 */
																																			obj_t
																																				BgL_arg1815z00_2122;
																																			long
																																				BgL_arg1816z00_2123;
																																			BgL_arg1815z00_2122
																																				=
																																				(BGl_za2classesza2z00zz__objectz00);
																																			{	/* Ast/pragma.scm 121 */
																																				long
																																					BgL_arg1817z00_2124;
																																				BgL_arg1817z00_2124
																																					=
																																					BGL_OBJECT_CLASS_NUM
																																					(BgL_arg1807z00_2100);
																																				BgL_arg1816z00_2123
																																					=
																																					(BgL_arg1817z00_2124
																																					-
																																					OBJECT_TYPE);
																																			}
																																			BgL_oclassz00_2114
																																				=
																																				VECTOR_REF
																																				(BgL_arg1815z00_2122,
																																				BgL_arg1816z00_2123);
																																		}
																																		{	/* Ast/pragma.scm 121 */
																																			bool_t
																																				BgL__ortest_1115z00_2115;
																																			BgL__ortest_1115z00_2115
																																				=
																																				(BgL_classz00_2098
																																				==
																																				BgL_oclassz00_2114);
																																			if (BgL__ortest_1115z00_2115)
																																				{	/* Ast/pragma.scm 121 */
																																					BgL_res1867z00_2131
																																						=
																																						BgL__ortest_1115z00_2115;
																																				}
																																			else
																																				{	/* Ast/pragma.scm 121 */
																																					long
																																						BgL_odepthz00_2116;
																																					{	/* Ast/pragma.scm 121 */
																																						obj_t
																																							BgL_arg1804z00_2117;
																																						BgL_arg1804z00_2117
																																							=
																																							(BgL_oclassz00_2114);
																																						BgL_odepthz00_2116
																																							=
																																							BGL_CLASS_DEPTH
																																							(BgL_arg1804z00_2117);
																																					}
																																					if (
																																						(2L
																																							<
																																							BgL_odepthz00_2116))
																																						{	/* Ast/pragma.scm 121 */
																																							obj_t
																																								BgL_arg1802z00_2119;
																																							{	/* Ast/pragma.scm 121 */
																																								obj_t
																																									BgL_arg1803z00_2120;
																																								BgL_arg1803z00_2120
																																									=
																																									(BgL_oclassz00_2114);
																																								BgL_arg1802z00_2119
																																									=
																																									BGL_CLASS_ANCESTORS_REF
																																									(BgL_arg1803z00_2120,
																																									2L);
																																							}
																																							BgL_res1867z00_2131
																																								=
																																								(BgL_arg1802z00_2119
																																								==
																																								BgL_classz00_2098);
																																						}
																																					else
																																						{	/* Ast/pragma.scm 121 */
																																							BgL_res1867z00_2131
																																								=
																																								(
																																								(bool_t)
																																								0);
																																						}
																																				}
																																		}
																																	}
																																	BgL_test1919z00_2481
																																		=
																																		BgL_res1867z00_2131;
																																}
																														}
																													}
																													if (BgL_test1919z00_2481)
																														{	/* Ast/pragma.scm 124 */
																															BgL_pragmaz00_bglt
																																BgL_new1118z00_1593;
																															{	/* Ast/pragma.scm 125 */
																																BgL_pragmaz00_bglt
																																	BgL_new1116z00_1595;
																																BgL_new1116z00_1595
																																	=
																																	(
																																	(BgL_pragmaz00_bglt)
																																	BOBJECT
																																	(GC_MALLOC
																																		(sizeof
																																			(struct
																																				BgL_pragmaz00_bgl))));
																																{	/* Ast/pragma.scm 125 */
																																	long
																																		BgL_arg1485z00_1596;
																																	BgL_arg1485z00_1596
																																		=
																																		BGL_CLASS_NUM
																																		(BGl_pragmaz00zzast_nodez00);
																																	BGL_OBJECT_CLASS_NUM_SET
																																		(((BgL_objectz00_bglt) BgL_new1116z00_1595), BgL_arg1485z00_1596);
																																}
																																{	/* Ast/pragma.scm 125 */
																																	BgL_objectz00_bglt
																																		BgL_tmpz00_2509;
																																	BgL_tmpz00_2509
																																		=
																																		(
																																		(BgL_objectz00_bglt)
																																		BgL_new1116z00_1595);
																																	BGL_OBJECT_WIDENING_SET
																																		(BgL_tmpz00_2509,
																																		BFALSE);
																																}
																																((BgL_objectz00_bglt) BgL_new1116z00_1595);
																																BgL_new1118z00_1593
																																	=
																																	BgL_new1116z00_1595;
																															}
																															((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1118z00_1593)))->BgL_locz00) = ((obj_t) BgL_locz00_9), BUNSPEC);
																															((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1118z00_1593)))->BgL_typez00) = ((BgL_typez00_bglt) BgL_typez00_6), BUNSPEC);
																															{
																																obj_t
																																	BgL_auxz00_2517;
																																if (BgL_freez00_4)
																																	{	/* Ast/pragma.scm 130 */
																																		BgL_auxz00_2517
																																			= BFALSE;
																																	}
																																else
																																	{	/* Ast/pragma.scm 130 */
																																		BgL_auxz00_2517
																																			= BTRUE;
																																	}
																																((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1118z00_1593)))->BgL_sidezd2effectzd2) = ((obj_t) BgL_auxz00_2517), BUNSPEC);
																															}
																															((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1118z00_1593)))->BgL_keyz00) = ((obj_t) BINT(-1L)), BUNSPEC);
																															{
																																obj_t
																																	BgL_auxz00_2524;
																																{	/* Ast/pragma.scm 129 */
																																	obj_t
																																		BgL_list1475z00_1594;
																																	BgL_list1475z00_1594
																																		=
																																		MAKE_YOUNG_PAIR
																																		(((obj_t)
																																			BgL_vz00_1589),
																																		BNIL);
																																	BgL_auxz00_2524
																																		=
																																		BgL_list1475z00_1594;
																																}
																																((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt) BgL_new1118z00_1593)))->BgL_exprza2za2) = ((obj_t) BgL_auxz00_2524), BUNSPEC);
																															}
																															((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt) BgL_new1118z00_1593)))->BgL_effectz00) = ((obj_t) BgL_effectz00_5), BUNSPEC);
																															((((BgL_pragmaz00_bglt) COBJECT(BgL_new1118z00_1593))->BgL_formatz00) = ((obj_t) BGl_string1870z00zzast_pragmaz00), BUNSPEC);
																															((((BgL_pragmaz00_bglt) COBJECT(BgL_new1118z00_1593))->BgL_srfi0z00) = ((obj_t) BgL_srfiz00_1469), BUNSPEC);
																															return
																																(
																																(BgL_nodez00_bglt)
																																BgL_new1118z00_1593);
																														}
																													else
																														{	/* Ast/pragma.scm 121 */
																															return
																																BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00
																																(BGl_string1871z00zzast_pragmaz00,
																																BgL_expz00_7,
																																BGl_findzd2locationzf2locz20zztools_locationz00
																																(BgL_expz00_7,
																																	BgL_locz00_9));
																														}
																												}
																											}
																										}
																									else
																										{	/* Ast/pragma.scm 53 */
																										BgL_tagzd2105zd2_1472:
																											return
																												BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00
																												(BGl_string1872z00zzast_pragmaz00,
																												BgL_expz00_7,
																												BgL_locz00_9);
																										}
																								}
																							else
																								{	/* Ast/pragma.scm 53 */
																									goto BgL_tagzd2105zd2_1472;
																								}
																						}
																					}
																			}
																		else
																			{	/* Ast/pragma.scm 53 */
																				goto BgL_tagzd2105zd2_1472;
																			}
																	}
																else
																	{	/* Ast/pragma.scm 53 */
																		obj_t BgL_cdrzd2202zd2_1501;

																		{	/* Ast/pragma.scm 53 */
																			obj_t BgL_pairz00_2154;

																			BgL_pairz00_2154 =
																				CDR(((obj_t) BgL_expz00_7));
																			BgL_cdrzd2202zd2_1501 =
																				CDR(BgL_pairz00_2154);
																		}
																		{	/* Ast/pragma.scm 53 */
																			obj_t BgL_carzd2205zd2_1502;
																			obj_t BgL_cdrzd2206zd2_1503;

																			BgL_carzd2205zd2_1502 =
																				CAR(((obj_t) BgL_cdrzd2202zd2_1501));
																			BgL_cdrzd2206zd2_1503 =
																				CDR(((obj_t) BgL_cdrzd2202zd2_1501));
																			if (SYMBOLP(BgL_carzd2205zd2_1502))
																				{	/* Ast/pragma.scm 53 */
																					if (PAIRP(BgL_cdrzd2206zd2_1503))
																						{	/* Ast/pragma.scm 53 */
																							if (NULLP(CDR
																									(BgL_cdrzd2206zd2_1503)))
																								{
																									obj_t BgL_identz00_2552;
																									obj_t BgL_srfiz00_2551;

																									BgL_srfiz00_2551 =
																										BgL_carzd2205zd2_1502;
																									BgL_identz00_2552 =
																										CAR(BgL_cdrzd2206zd2_1503);
																									BgL_identz00_1470 =
																										BgL_identz00_2552;
																									BgL_srfiz00_1469 =
																										BgL_srfiz00_2551;
																									goto BgL_tagzd2104zd2_1471;
																								}
																							else
																								{	/* Ast/pragma.scm 53 */
																									goto BgL_tagzd2105zd2_1472;
																								}
																						}
																					else
																						{	/* Ast/pragma.scm 53 */
																							goto BgL_tagzd2105zd2_1472;
																						}
																				}
																			else
																				{	/* Ast/pragma.scm 53 */
																					goto BgL_tagzd2105zd2_1472;
																				}
																		}
																	}
															}
														else
															{	/* Ast/pragma.scm 53 */
																obj_t BgL_cdrzd2218zd2_1511;

																BgL_cdrzd2218zd2_1511 =
																	CDR(((obj_t) BgL_expz00_7));
																if (NULLP(CDR(((obj_t) BgL_cdrzd2218zd2_1511))))
																	{	/* Ast/pragma.scm 53 */
																		obj_t BgL_arg1319z00_1514;

																		BgL_arg1319z00_1514 =
																			CAR(((obj_t) BgL_cdrzd2218zd2_1511));
																		BgL_identz00_1467 = BgL_arg1319z00_1514;
																	BgL_tagzd2103zd2_1468:
																		{	/* Ast/pragma.scm 106 */
																			BgL_nodez00_bglt BgL_vz00_1580;

																			BgL_vz00_1580 =
																				BGl_sexpzd2ze3nodez31zzast_sexpz00
																				(BgL_identz00_1467, BgL_stackz00_8,
																				BgL_locz00_9, BgL_sitez00_10);
																			{	/* Ast/pragma.scm 107 */
																				bool_t BgL_test1928z00_2563;

																				{	/* Ast/pragma.scm 107 */
																					obj_t BgL_classz00_2059;

																					BgL_classz00_2059 =
																						BGl_varz00zzast_nodez00;
																					{	/* Ast/pragma.scm 107 */
																						BgL_objectz00_bglt
																							BgL_arg1807z00_2061;
																						{	/* Ast/pragma.scm 107 */
																							obj_t BgL_tmpz00_2564;

																							BgL_tmpz00_2564 =
																								((obj_t)
																								((BgL_objectz00_bglt)
																									BgL_vz00_1580));
																							BgL_arg1807z00_2061 =
																								(BgL_objectz00_bglt)
																								(BgL_tmpz00_2564);
																						}
																						if (BGL_CONDEXPAND_ISA_ARCH64())
																							{	/* Ast/pragma.scm 107 */
																								long BgL_idxz00_2067;

																								BgL_idxz00_2067 =
																									BGL_OBJECT_INHERITANCE_NUM
																									(BgL_arg1807z00_2061);
																								BgL_test1928z00_2563 =
																									(VECTOR_REF
																									(BGl_za2inheritancesza2z00zz__objectz00,
																										(BgL_idxz00_2067 + 2L)) ==
																									BgL_classz00_2059);
																							}
																						else
																							{	/* Ast/pragma.scm 107 */
																								bool_t BgL_res1865z00_2092;

																								{	/* Ast/pragma.scm 107 */
																									obj_t BgL_oclassz00_2075;

																									{	/* Ast/pragma.scm 107 */
																										obj_t BgL_arg1815z00_2083;
																										long BgL_arg1816z00_2084;

																										BgL_arg1815z00_2083 =
																											(BGl_za2classesza2z00zz__objectz00);
																										{	/* Ast/pragma.scm 107 */
																											long BgL_arg1817z00_2085;

																											BgL_arg1817z00_2085 =
																												BGL_OBJECT_CLASS_NUM
																												(BgL_arg1807z00_2061);
																											BgL_arg1816z00_2084 =
																												(BgL_arg1817z00_2085 -
																												OBJECT_TYPE);
																										}
																										BgL_oclassz00_2075 =
																											VECTOR_REF
																											(BgL_arg1815z00_2083,
																											BgL_arg1816z00_2084);
																									}
																									{	/* Ast/pragma.scm 107 */
																										bool_t
																											BgL__ortest_1115z00_2076;
																										BgL__ortest_1115z00_2076 =
																											(BgL_classz00_2059 ==
																											BgL_oclassz00_2075);
																										if (BgL__ortest_1115z00_2076)
																											{	/* Ast/pragma.scm 107 */
																												BgL_res1865z00_2092 =
																													BgL__ortest_1115z00_2076;
																											}
																										else
																											{	/* Ast/pragma.scm 107 */
																												long BgL_odepthz00_2077;

																												{	/* Ast/pragma.scm 107 */
																													obj_t
																														BgL_arg1804z00_2078;
																													BgL_arg1804z00_2078 =
																														(BgL_oclassz00_2075);
																													BgL_odepthz00_2077 =
																														BGL_CLASS_DEPTH
																														(BgL_arg1804z00_2078);
																												}
																												if (
																													(2L <
																														BgL_odepthz00_2077))
																													{	/* Ast/pragma.scm 107 */
																														obj_t
																															BgL_arg1802z00_2080;
																														{	/* Ast/pragma.scm 107 */
																															obj_t
																																BgL_arg1803z00_2081;
																															BgL_arg1803z00_2081
																																=
																																(BgL_oclassz00_2075);
																															BgL_arg1802z00_2080
																																=
																																BGL_CLASS_ANCESTORS_REF
																																(BgL_arg1803z00_2081,
																																2L);
																														}
																														BgL_res1865z00_2092
																															=
																															(BgL_arg1802z00_2080
																															==
																															BgL_classz00_2059);
																													}
																												else
																													{	/* Ast/pragma.scm 107 */
																														BgL_res1865z00_2092
																															= ((bool_t) 0);
																													}
																											}
																									}
																								}
																								BgL_test1928z00_2563 =
																									BgL_res1865z00_2092;
																							}
																					}
																				}
																				if (BgL_test1928z00_2563)
																					{	/* Ast/pragma.scm 110 */
																						BgL_pragmaz00_bglt
																							BgL_new1113z00_1584;
																						{	/* Ast/pragma.scm 111 */
																							BgL_pragmaz00_bglt
																								BgL_new1111z00_1586;
																							BgL_new1111z00_1586 =
																								((BgL_pragmaz00_bglt)
																								BOBJECT(GC_MALLOC(sizeof(struct
																											BgL_pragmaz00_bgl))));
																							{	/* Ast/pragma.scm 111 */
																								long BgL_arg1472z00_1587;

																								BgL_arg1472z00_1587 =
																									BGL_CLASS_NUM
																									(BGl_pragmaz00zzast_nodez00);
																								BGL_OBJECT_CLASS_NUM_SET((
																										(BgL_objectz00_bglt)
																										BgL_new1111z00_1586),
																									BgL_arg1472z00_1587);
																							}
																							{	/* Ast/pragma.scm 111 */
																								BgL_objectz00_bglt
																									BgL_tmpz00_2591;
																								BgL_tmpz00_2591 =
																									((BgL_objectz00_bglt)
																									BgL_new1111z00_1586);
																								BGL_OBJECT_WIDENING_SET
																									(BgL_tmpz00_2591, BFALSE);
																							}
																							((BgL_objectz00_bglt)
																								BgL_new1111z00_1586);
																							BgL_new1113z00_1584 =
																								BgL_new1111z00_1586;
																						}
																						((((BgL_nodez00_bglt) COBJECT(
																										((BgL_nodez00_bglt)
																											BgL_new1113z00_1584)))->
																								BgL_locz00) =
																							((obj_t) BgL_locz00_9), BUNSPEC);
																						((((BgL_nodez00_bglt)
																									COBJECT(((BgL_nodez00_bglt)
																											BgL_new1113z00_1584)))->
																								BgL_typez00) =
																							((BgL_typez00_bglt)
																								BgL_typez00_6), BUNSPEC);
																						{
																							obj_t BgL_auxz00_2599;

																							if (BgL_freez00_4)
																								{	/* Ast/pragma.scm 115 */
																									BgL_auxz00_2599 = BFALSE;
																								}
																							else
																								{	/* Ast/pragma.scm 115 */
																									BgL_auxz00_2599 = BTRUE;
																								}
																							((((BgL_nodezf2effectzf2_bglt)
																										COBJECT((
																												(BgL_nodezf2effectzf2_bglt)
																												BgL_new1113z00_1584)))->
																									BgL_sidezd2effectzd2) =
																								((obj_t) BgL_auxz00_2599),
																								BUNSPEC);
																						}
																						((((BgL_nodezf2effectzf2_bglt)
																									COBJECT((
																											(BgL_nodezf2effectzf2_bglt)
																											BgL_new1113z00_1584)))->
																								BgL_keyz00) =
																							((obj_t) BINT(-1L)), BUNSPEC);
																						{
																							obj_t BgL_auxz00_2606;

																							{	/* Ast/pragma.scm 114 */
																								obj_t BgL_list1456z00_1585;

																								BgL_list1456z00_1585 =
																									MAKE_YOUNG_PAIR(
																									((obj_t) BgL_vz00_1580),
																									BNIL);
																								BgL_auxz00_2606 =
																									BgL_list1456z00_1585;
																							}
																							((((BgL_externz00_bglt) COBJECT(
																											((BgL_externz00_bglt)
																												BgL_new1113z00_1584)))->
																									BgL_exprza2za2) =
																								((obj_t) BgL_auxz00_2606),
																								BUNSPEC);
																						}
																						((((BgL_externz00_bglt) COBJECT(
																										((BgL_externz00_bglt)
																											BgL_new1113z00_1584)))->
																								BgL_effectz00) =
																							((obj_t) BgL_effectz00_5),
																							BUNSPEC);
																						((((BgL_pragmaz00_bglt)
																									COBJECT
																									(BgL_new1113z00_1584))->
																								BgL_formatz00) =
																							((obj_t)
																								BGl_string1870z00zzast_pragmaz00),
																							BUNSPEC);
																						((((BgL_pragmaz00_bglt)
																									COBJECT
																									(BgL_new1113z00_1584))->
																								BgL_srfi0z00) =
																							((obj_t) CNST_TABLE_REF(0)),
																							BUNSPEC);
																						return ((BgL_nodez00_bglt)
																							BgL_new1113z00_1584);
																					}
																				else
																					{	/* Ast/pragma.scm 107 */
																						return
																							BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00
																							(BGl_string1871z00zzast_pragmaz00,
																							BgL_expz00_7,
																							BGl_findzd2locationzf2locz20zztools_locationz00
																							(BgL_expz00_7, BgL_locz00_9));
																					}
																			}
																		}
																	}
																else
																	{	/* Ast/pragma.scm 53 */
																		goto BgL_tagzd2105zd2_1472;
																	}
															}
													}
												else
													{	/* Ast/pragma.scm 53 */
														obj_t BgL_cdrzd2231zd2_1516;

														BgL_cdrzd2231zd2_1516 = CDR(((obj_t) BgL_expz00_7));
														if (NULLP(CDR(((obj_t) BgL_cdrzd2231zd2_1516))))
															{	/* Ast/pragma.scm 53 */
																obj_t BgL_arg1323z00_1519;

																BgL_arg1323z00_1519 =
																	CAR(((obj_t) BgL_cdrzd2231zd2_1516));
																{
																	obj_t BgL_identz00_2627;

																	BgL_identz00_2627 = BgL_arg1323z00_1519;
																	BgL_identz00_1467 = BgL_identz00_2627;
																	goto BgL_tagzd2103zd2_1468;
																}
															}
														else
															{	/* Ast/pragma.scm 53 */
																obj_t BgL_cdrzd2245zd2_1521;

																BgL_cdrzd2245zd2_1521 =
																	CDR(((obj_t) BgL_cdrzd2231zd2_1516));
																if (
																	(CAR(
																			((obj_t) BgL_cdrzd2231zd2_1516)) ==
																		CNST_TABLE_REF(3)))
																	{	/* Ast/pragma.scm 53 */
																		if (PAIRP(BgL_cdrzd2245zd2_1521))
																			{	/* Ast/pragma.scm 53 */
																				obj_t BgL_carzd2248zd2_1525;
																				obj_t BgL_cdrzd2249zd2_1526;

																				BgL_carzd2248zd2_1525 =
																					CAR(BgL_cdrzd2245zd2_1521);
																				BgL_cdrzd2249zd2_1526 =
																					CDR(BgL_cdrzd2245zd2_1521);
																				if (SYMBOLP(BgL_carzd2248zd2_1525))
																					{	/* Ast/pragma.scm 53 */
																						if (PAIRP(BgL_cdrzd2249zd2_1526))
																							{	/* Ast/pragma.scm 53 */
																								if (NULLP(CDR
																										(BgL_cdrzd2249zd2_1526)))
																									{
																										obj_t BgL_identz00_2647;
																										obj_t BgL_srfiz00_2646;

																										BgL_srfiz00_2646 =
																											BgL_carzd2248zd2_1525;
																										BgL_identz00_2647 =
																											CAR
																											(BgL_cdrzd2249zd2_1526);
																										BgL_identz00_1470 =
																											BgL_identz00_2647;
																										BgL_srfiz00_1469 =
																											BgL_srfiz00_2646;
																										goto BgL_tagzd2104zd2_1471;
																									}
																								else
																									{	/* Ast/pragma.scm 53 */
																										goto BgL_tagzd2105zd2_1472;
																									}
																							}
																						else
																							{	/* Ast/pragma.scm 53 */
																								goto BgL_tagzd2105zd2_1472;
																							}
																					}
																				else
																					{	/* Ast/pragma.scm 53 */
																						goto BgL_tagzd2105zd2_1472;
																					}
																			}
																		else
																			{	/* Ast/pragma.scm 53 */
																				goto BgL_tagzd2105zd2_1472;
																			}
																	}
																else
																	{	/* Ast/pragma.scm 53 */
																		goto BgL_tagzd2105zd2_1472;
																	}
															}
													}
											}
									}
								else
									{	/* Ast/pragma.scm 53 */
										goto BgL_tagzd2105zd2_1472;
									}
							}
						else
							{	/* Ast/pragma.scm 53 */
								goto BgL_tagzd2105zd2_1472;
							}
					}
				else
					{	/* Ast/pragma.scm 46 */
						BGl_userzd2warningzf2locationz20zztools_errorz00(BgL_locz00_9,
							BGl_string1873z00zzast_pragmaz00,
							BGl_string1874z00zzast_pragmaz00, BgL_expz00_7);
						return BGl_sexpzd2ze3nodez31zzast_sexpz00(BUNSPEC, BgL_stackz00_8,
							BgL_locz00_9, BgL_sitez00_10);
					}
			}
		}

	}



/* &pragma/type->node */
	BgL_nodez00_bglt BGl_z62pragmazf2typezd2ze3nodeza1zzast_pragmaz00(obj_t
		BgL_envz00_2260, obj_t BgL_freez00_2261, obj_t BgL_effectz00_2262,
		obj_t BgL_typez00_2263, obj_t BgL_expz00_2264, obj_t BgL_stackz00_2265,
		obj_t BgL_locz00_2266, obj_t BgL_sitez00_2267)
	{
		{	/* Ast/pragma.scm 45 */
			return
				BGl_pragmazf2typezd2ze3nodezc3zzast_pragmaz00(CBOOL(BgL_freez00_2261),
				BgL_effectz00_2262, ((BgL_typez00_bglt) BgL_typez00_2263),
				BgL_expz00_2264, BgL_stackz00_2265, BgL_locz00_2266, BgL_sitez00_2267);
		}

	}



/* get-max-index */
	obj_t BGl_getzd2maxzd2indexz00zzast_pragmaz00(obj_t BgL_fmtz00_11)
	{
		{	/* Ast/pragma.scm 140 */
			{	/* Ast/pragma.scm 141 */
				obj_t BgL_portz00_1600;

				{	/* Ast/pragma.scm 148 */
					long BgL_endz00_1879;

					BgL_endz00_1879 = STRING_LENGTH(BgL_fmtz00_11);
					{	/* Ast/pragma.scm 148 */

						BgL_portz00_1600 =
							BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(BgL_fmtz00_11,
							BINT(0L), BINT(BgL_endz00_1879));
				}}
				{	/* Ast/pragma.scm 149 */
					obj_t BgL_g1129z00_1601;

					BgL_g1129z00_1601 =
						BGl_parserze70ze7zzast_pragmaz00(BgL_portz00_1600);
					{
						obj_t BgL_expz00_1603;
						obj_t BgL_maxz00_1604;

						BgL_expz00_1603 = BgL_g1129z00_1601;
						BgL_maxz00_1604 = BINT(0L);
					BgL_zc3z04anonymousza31503ze3z87_1605:
						if (EOF_OBJECTP(BgL_expz00_1603))
							{	/* Ast/pragma.scm 152 */
								return BgL_maxz00_1604;
							}
						else
							{	/* Ast/pragma.scm 152 */
								if (CHARP(BgL_expz00_1603))
									{
										obj_t BgL_expz00_2663;

										BgL_expz00_2663 =
											BGl_parserze70ze7zzast_pragmaz00(BgL_portz00_1600);
										BgL_expz00_1603 = BgL_expz00_2663;
										goto BgL_zc3z04anonymousza31503ze3z87_1605;
									}
								else
									{	/* Ast/pragma.scm 157 */
										obj_t BgL_arg1513z00_1609;
										obj_t BgL_arg1514z00_1610;

										BgL_arg1513z00_1609 =
											BGl_parserze70ze7zzast_pragmaz00(BgL_portz00_1600);
										if (
											((long) CINT(BgL_expz00_1603) >
												(long) CINT(BgL_maxz00_1604)))
											{	/* Ast/pragma.scm 157 */
												BgL_arg1514z00_1610 = BgL_expz00_1603;
											}
										else
											{	/* Ast/pragma.scm 157 */
												BgL_arg1514z00_1610 = BgL_maxz00_1604;
											}
										{
											obj_t BgL_maxz00_2671;
											obj_t BgL_expz00_2670;

											BgL_expz00_2670 = BgL_arg1513z00_1609;
											BgL_maxz00_2671 = BgL_arg1514z00_1610;
											BgL_maxz00_1604 = BgL_maxz00_2671;
											BgL_expz00_1603 = BgL_expz00_2670;
											goto BgL_zc3z04anonymousza31503ze3z87_1605;
										}
									}
							}
					}
				}
			}
		}

	}



/* parser~0 */
	obj_t BGl_parserze70ze7zzast_pragmaz00(obj_t BgL_iportz00_1614)
	{
		{	/* Ast/pragma.scm 141 */
			{
				obj_t BgL_iportz00_1648;
				long BgL_lastzd2matchzd2_1649;
				long BgL_forwardz00_1650;
				long BgL_bufposz00_1651;
				obj_t BgL_iportz00_1661;
				long BgL_lastzd2matchzd2_1662;
				long BgL_forwardz00_1663;
				long BgL_bufposz00_1664;
				obj_t BgL_iportz00_1676;
				long BgL_lastzd2matchzd2_1677;
				long BgL_forwardz00_1678;
				long BgL_bufposz00_1679;
				obj_t BgL_iportz00_1689;
				long BgL_lastzd2matchzd2_1690;
				long BgL_forwardz00_1691;
				long BgL_bufposz00_1692;
				obj_t BgL_iportz00_1704;
				long BgL_lastzd2matchzd2_1705;
				long BgL_forwardz00_1706;
				long BgL_bufposz00_1707;
				int BgL_minz00_1729;
				int BgL_maxz00_1730;

			BgL_zc3z04anonymousza31746ze3z87_1835:
				RGC_START_MATCH(BgL_iportz00_1614);
				{	/* Ast/pragma.scm 141 */
					long BgL_matchz00_1836;

					{	/* Ast/pragma.scm 141 */
						long BgL_arg1749z00_1843;
						long BgL_arg1750z00_1844;

						BgL_arg1749z00_1843 = RGC_BUFFER_FORWARD(BgL_iportz00_1614);
						BgL_arg1750z00_1844 = RGC_BUFFER_BUFPOS(BgL_iportz00_1614);
						BgL_iportz00_1704 = BgL_iportz00_1614;
						BgL_lastzd2matchzd2_1705 = 2L;
						BgL_forwardz00_1706 = BgL_arg1749z00_1843;
						BgL_bufposz00_1707 = BgL_arg1750z00_1844;
					BgL_zc3z04anonymousza31585ze3z87_1708:
						if ((BgL_forwardz00_1706 == BgL_bufposz00_1707))
							{	/* Ast/pragma.scm 141 */
								if (rgc_fill_buffer(BgL_iportz00_1704))
									{	/* Ast/pragma.scm 141 */
										long BgL_arg1589z00_1711;
										long BgL_arg1591z00_1712;

										BgL_arg1589z00_1711 = RGC_BUFFER_FORWARD(BgL_iportz00_1704);
										BgL_arg1591z00_1712 = RGC_BUFFER_BUFPOS(BgL_iportz00_1704);
										{
											long BgL_bufposz00_2683;
											long BgL_forwardz00_2682;

											BgL_forwardz00_2682 = BgL_arg1589z00_1711;
											BgL_bufposz00_2683 = BgL_arg1591z00_1712;
											BgL_bufposz00_1707 = BgL_bufposz00_2683;
											BgL_forwardz00_1706 = BgL_forwardz00_2682;
											goto BgL_zc3z04anonymousza31585ze3z87_1708;
										}
									}
								else
									{	/* Ast/pragma.scm 141 */
										BgL_matchz00_1836 = BgL_lastzd2matchzd2_1705;
									}
							}
						else
							{	/* Ast/pragma.scm 141 */
								int BgL_curz00_1713;

								BgL_curz00_1713 =
									RGC_BUFFER_GET_CHAR(BgL_iportz00_1704, BgL_forwardz00_1706);
								{	/* Ast/pragma.scm 141 */

									if (((long) (BgL_curz00_1713) == 36L))
										{	/* Ast/pragma.scm 141 */
											BgL_iportz00_1661 = BgL_iportz00_1704;
											BgL_lastzd2matchzd2_1662 = BgL_lastzd2matchzd2_1705;
											BgL_forwardz00_1663 = (1L + BgL_forwardz00_1706);
											BgL_bufposz00_1664 = BgL_bufposz00_1707;
										BgL_zc3z04anonymousza31545ze3z87_1665:
											{	/* Ast/pragma.scm 141 */
												long BgL_newzd2matchzd2_1666;

												RGC_STOP_MATCH(BgL_iportz00_1661, BgL_forwardz00_1663);
												BgL_newzd2matchzd2_1666 = 2L;
												if ((BgL_forwardz00_1663 == BgL_bufposz00_1664))
													{	/* Ast/pragma.scm 141 */
														if (rgc_fill_buffer(BgL_iportz00_1661))
															{	/* Ast/pragma.scm 141 */
																long BgL_arg1552z00_1669;
																long BgL_arg1553z00_1670;

																BgL_arg1552z00_1669 =
																	RGC_BUFFER_FORWARD(BgL_iportz00_1661);
																BgL_arg1553z00_1670 =
																	RGC_BUFFER_BUFPOS(BgL_iportz00_1661);
																{
																	long BgL_bufposz00_2696;
																	long BgL_forwardz00_2695;

																	BgL_forwardz00_2695 = BgL_arg1552z00_1669;
																	BgL_bufposz00_2696 = BgL_arg1553z00_1670;
																	BgL_bufposz00_1664 = BgL_bufposz00_2696;
																	BgL_forwardz00_1663 = BgL_forwardz00_2695;
																	goto BgL_zc3z04anonymousza31545ze3z87_1665;
																}
															}
														else
															{	/* Ast/pragma.scm 141 */
																BgL_matchz00_1836 = BgL_newzd2matchzd2_1666;
															}
													}
												else
													{	/* Ast/pragma.scm 141 */
														int BgL_curz00_1671;

														BgL_curz00_1671 =
															RGC_BUFFER_GET_CHAR(BgL_iportz00_1661,
															BgL_forwardz00_1663);
														{	/* Ast/pragma.scm 141 */

															{	/* Ast/pragma.scm 141 */
																bool_t BgL_test1947z00_2698;

																if (((long) (BgL_curz00_1671) >= 48L))
																	{	/* Ast/pragma.scm 141 */
																		BgL_test1947z00_2698 =
																			((long) (BgL_curz00_1671) < 58L);
																	}
																else
																	{	/* Ast/pragma.scm 141 */
																		BgL_test1947z00_2698 = ((bool_t) 0);
																	}
																if (BgL_test1947z00_2698)
																	{	/* Ast/pragma.scm 141 */
																		BgL_iportz00_1689 = BgL_iportz00_1661;
																		BgL_lastzd2matchzd2_1690 =
																			BgL_newzd2matchzd2_1666;
																		BgL_forwardz00_1691 =
																			(1L + BgL_forwardz00_1663);
																		BgL_bufposz00_1692 = BgL_bufposz00_1664;
																	BgL_zc3z04anonymousza31572ze3z87_1693:
																		{	/* Ast/pragma.scm 141 */
																			long BgL_newzd2matchzd2_1694;

																			RGC_STOP_MATCH(BgL_iportz00_1689,
																				BgL_forwardz00_1691);
																			BgL_newzd2matchzd2_1694 = 0L;
																			if (
																				(BgL_forwardz00_1691 ==
																					BgL_bufposz00_1692))
																				{	/* Ast/pragma.scm 141 */
																					if (rgc_fill_buffer
																						(BgL_iportz00_1689))
																						{	/* Ast/pragma.scm 141 */
																							long BgL_arg1575z00_1697;
																							long BgL_arg1576z00_1698;

																							BgL_arg1575z00_1697 =
																								RGC_BUFFER_FORWARD
																								(BgL_iportz00_1689);
																							BgL_arg1576z00_1698 =
																								RGC_BUFFER_BUFPOS
																								(BgL_iportz00_1689);
																							{
																								long BgL_bufposz00_2712;
																								long BgL_forwardz00_2711;

																								BgL_forwardz00_2711 =
																									BgL_arg1575z00_1697;
																								BgL_bufposz00_2712 =
																									BgL_arg1576z00_1698;
																								BgL_bufposz00_1692 =
																									BgL_bufposz00_2712;
																								BgL_forwardz00_1691 =
																									BgL_forwardz00_2711;
																								goto
																									BgL_zc3z04anonymousza31572ze3z87_1693;
																							}
																						}
																					else
																						{	/* Ast/pragma.scm 141 */
																							BgL_matchz00_1836 =
																								BgL_newzd2matchzd2_1694;
																						}
																				}
																			else
																				{	/* Ast/pragma.scm 141 */
																					int BgL_curz00_1699;

																					BgL_curz00_1699 =
																						RGC_BUFFER_GET_CHAR
																						(BgL_iportz00_1689,
																						BgL_forwardz00_1691);
																					{	/* Ast/pragma.scm 141 */

																						{	/* Ast/pragma.scm 141 */
																							bool_t BgL_test1951z00_2714;

																							if (
																								((long) (BgL_curz00_1699) >=
																									48L))
																								{	/* Ast/pragma.scm 141 */
																									BgL_test1951z00_2714 =
																										(
																										(long) (BgL_curz00_1699) <
																										58L);
																								}
																							else
																								{	/* Ast/pragma.scm 141 */
																									BgL_test1951z00_2714 =
																										((bool_t) 0);
																								}
																							if (BgL_test1951z00_2714)
																								{
																									long BgL_forwardz00_2721;
																									long BgL_lastzd2matchzd2_2720;

																									BgL_lastzd2matchzd2_2720 =
																										BgL_newzd2matchzd2_1694;
																									BgL_forwardz00_2721 =
																										(1L + BgL_forwardz00_1691);
																									BgL_forwardz00_1691 =
																										BgL_forwardz00_2721;
																									BgL_lastzd2matchzd2_1690 =
																										BgL_lastzd2matchzd2_2720;
																									goto
																										BgL_zc3z04anonymousza31572ze3z87_1693;
																								}
																							else
																								{	/* Ast/pragma.scm 141 */
																									BgL_matchz00_1836 =
																										BgL_newzd2matchzd2_1694;
																								}
																						}
																					}
																				}
																		}
																	}
																else
																	{	/* Ast/pragma.scm 141 */
																		BgL_matchz00_1836 = BgL_newzd2matchzd2_1666;
																	}
															}
														}
													}
											}
										}
									else
										{	/* Ast/pragma.scm 141 */
											BgL_iportz00_1676 = BgL_iportz00_1704;
											BgL_lastzd2matchzd2_1677 = BgL_lastzd2matchzd2_1705;
											BgL_forwardz00_1678 = (1L + BgL_forwardz00_1706);
											BgL_bufposz00_1679 = BgL_bufposz00_1707;
										BgL_zc3z04anonymousza31560ze3z87_1680:
											{	/* Ast/pragma.scm 141 */
												long BgL_newzd2matchzd2_1681;

												RGC_STOP_MATCH(BgL_iportz00_1676, BgL_forwardz00_1678);
												BgL_newzd2matchzd2_1681 = 1L;
												if ((BgL_forwardz00_1678 == BgL_bufposz00_1679))
													{	/* Ast/pragma.scm 141 */
														if (rgc_fill_buffer(BgL_iportz00_1676))
															{	/* Ast/pragma.scm 141 */
																long BgL_arg1564z00_1684;
																long BgL_arg1565z00_1685;

																BgL_arg1564z00_1684 =
																	RGC_BUFFER_FORWARD(BgL_iportz00_1676);
																BgL_arg1565z00_1685 =
																	RGC_BUFFER_BUFPOS(BgL_iportz00_1676);
																{
																	long BgL_bufposz00_2733;
																	long BgL_forwardz00_2732;

																	BgL_forwardz00_2732 = BgL_arg1564z00_1684;
																	BgL_bufposz00_2733 = BgL_arg1565z00_1685;
																	BgL_bufposz00_1679 = BgL_bufposz00_2733;
																	BgL_forwardz00_1678 = BgL_forwardz00_2732;
																	goto BgL_zc3z04anonymousza31560ze3z87_1680;
																}
															}
														else
															{	/* Ast/pragma.scm 141 */
																BgL_matchz00_1836 = BgL_newzd2matchzd2_1681;
															}
													}
												else
													{	/* Ast/pragma.scm 141 */
														int BgL_curz00_1686;

														BgL_curz00_1686 =
															RGC_BUFFER_GET_CHAR(BgL_iportz00_1676,
															BgL_forwardz00_1678);
														{	/* Ast/pragma.scm 141 */

															if (((long) (BgL_curz00_1686) == 36L))
																{	/* Ast/pragma.scm 141 */
																	BgL_matchz00_1836 = BgL_newzd2matchzd2_1681;
																}
															else
																{	/* Ast/pragma.scm 141 */
																	BgL_iportz00_1648 = BgL_iportz00_1676;
																	BgL_lastzd2matchzd2_1649 =
																		BgL_newzd2matchzd2_1681;
																	BgL_forwardz00_1650 =
																		(1L + BgL_forwardz00_1678);
																	BgL_bufposz00_1651 = BgL_bufposz00_1679;
																BgL_zc3z04anonymousza31517ze3z87_1652:
																	{	/* Ast/pragma.scm 141 */
																		long BgL_newzd2matchzd2_1653;

																		RGC_STOP_MATCH(BgL_iportz00_1648,
																			BgL_forwardz00_1650);
																		BgL_newzd2matchzd2_1653 = 1L;
																		if (
																			(BgL_forwardz00_1650 ==
																				BgL_bufposz00_1651))
																			{	/* Ast/pragma.scm 141 */
																				if (rgc_fill_buffer(BgL_iportz00_1648))
																					{	/* Ast/pragma.scm 141 */
																						long BgL_arg1535z00_1656;
																						long BgL_arg1540z00_1657;

																						BgL_arg1535z00_1656 =
																							RGC_BUFFER_FORWARD
																							(BgL_iportz00_1648);
																						BgL_arg1540z00_1657 =
																							RGC_BUFFER_BUFPOS
																							(BgL_iportz00_1648);
																						{
																							long BgL_bufposz00_2746;
																							long BgL_forwardz00_2745;

																							BgL_forwardz00_2745 =
																								BgL_arg1535z00_1656;
																							BgL_bufposz00_2746 =
																								BgL_arg1540z00_1657;
																							BgL_bufposz00_1651 =
																								BgL_bufposz00_2746;
																							BgL_forwardz00_1650 =
																								BgL_forwardz00_2745;
																							goto
																								BgL_zc3z04anonymousza31517ze3z87_1652;
																						}
																					}
																				else
																					{	/* Ast/pragma.scm 141 */
																						BgL_matchz00_1836 =
																							BgL_newzd2matchzd2_1653;
																					}
																			}
																		else
																			{	/* Ast/pragma.scm 141 */
																				int BgL_curz00_1658;

																				BgL_curz00_1658 =
																					RGC_BUFFER_GET_CHAR(BgL_iportz00_1648,
																					BgL_forwardz00_1650);
																				{	/* Ast/pragma.scm 141 */

																					if (((long) (BgL_curz00_1658) == 36L))
																						{	/* Ast/pragma.scm 141 */
																							BgL_matchz00_1836 =
																								BgL_newzd2matchzd2_1653;
																						}
																					else
																						{
																							long BgL_forwardz00_2752;
																							long BgL_lastzd2matchzd2_2751;

																							BgL_lastzd2matchzd2_2751 =
																								BgL_newzd2matchzd2_1653;
																							BgL_forwardz00_2752 =
																								(1L + BgL_forwardz00_1650);
																							BgL_forwardz00_1650 =
																								BgL_forwardz00_2752;
																							BgL_lastzd2matchzd2_1649 =
																								BgL_lastzd2matchzd2_2751;
																							goto
																								BgL_zc3z04anonymousza31517ze3z87_1652;
																						}
																				}
																			}
																	}
																}
														}
													}
											}
										}
								}
							}
					}
					RGC_SET_FILEPOS(BgL_iportz00_1614);
					{

						switch (BgL_matchz00_1836)
							{
							case 2L:

								{	/* Ast/pragma.scm 141 */
									bool_t BgL_test1959z00_2757;

									{	/* Ast/pragma.scm 141 */
										long BgL_arg1740z00_1826;

										BgL_arg1740z00_1826 =
											RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_1614);
										BgL_test1959z00_2757 = (BgL_arg1740z00_1826 == 0L);
									}
									if (BgL_test1959z00_2757)
										{	/* Ast/pragma.scm 141 */
											return BEOF;
										}
									else
										{	/* Ast/pragma.scm 141 */
											return BCHAR(RGC_BUFFER_CHARACTER(BgL_iportz00_1614));
										}
								}
								break;
							case 1L:

								goto BgL_zc3z04anonymousza31746ze3z87_1835;
								break;
							case 0L:

								{	/* Ast/pragma.scm 143 */
									obj_t BgL_arg1747z00_1839;

									{	/* Ast/pragma.scm 143 */
										long BgL_arg1748z00_1842;

										BgL_arg1748z00_1842 =
											RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_1614);
										BgL_minz00_1729 = (int) (1L);
										BgL_maxz00_1730 = (int) (BgL_arg1748z00_1842);
										if (((long) (BgL_maxz00_1730) < (long) (BgL_minz00_1729)))
											{	/* Ast/pragma.scm 141 */
												long BgL_arg1605z00_1733;

												BgL_arg1605z00_1733 =
													RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_1614);
												{	/* Ast/pragma.scm 141 */
													long BgL_za72za7_2236;

													BgL_za72za7_2236 = (long) (BgL_maxz00_1730);
													BgL_maxz00_1730 =
														(int) ((BgL_arg1605z00_1733 + BgL_za72za7_2236));
											}}
										else
											{	/* Ast/pragma.scm 141 */
												BFALSE;
											}
										{	/* Ast/pragma.scm 141 */
											bool_t BgL_test1961z00_2771;

											if (((long) (BgL_minz00_1729) >= 0L))
												{	/* Ast/pragma.scm 141 */
													if (
														((long) (BgL_maxz00_1730) >=
															(long) (BgL_minz00_1729)))
														{	/* Ast/pragma.scm 141 */
															long BgL_arg1625z00_1744;

															BgL_arg1625z00_1744 =
																RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_1614);
															BgL_test1961z00_2771 =
																(
																(long) (BgL_maxz00_1730) <=
																BgL_arg1625z00_1744);
														}
													else
														{	/* Ast/pragma.scm 141 */
															BgL_test1961z00_2771 = ((bool_t) 0);
														}
												}
											else
												{	/* Ast/pragma.scm 141 */
													BgL_test1961z00_2771 = ((bool_t) 0);
												}
											if (BgL_test1961z00_2771)
												{	/* Ast/pragma.scm 141 */
													BgL_arg1747z00_1839 =
														rgc_buffer_substring(BgL_iportz00_1614,
														(long) (BgL_minz00_1729), (long) (BgL_maxz00_1730));
												}
											else
												{	/* Ast/pragma.scm 141 */
													obj_t BgL_arg1611z00_1738;
													obj_t BgL_arg1613z00_1739;

													{	/* Ast/pragma.scm 141 */
														obj_t BgL_arg1615z00_1740;

														{	/* Ast/pragma.scm 141 */
															long BgL_arg1602z00_1728;

															BgL_arg1602z00_1728 =
																RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_1614);
															BgL_arg1615z00_1740 =
																rgc_buffer_substring(BgL_iportz00_1614, 0L,
																BgL_arg1602z00_1728);
														}
														{	/* Ast/pragma.scm 141 */
															obj_t BgL_list1616z00_1741;

															BgL_list1616z00_1741 =
																MAKE_YOUNG_PAIR(BgL_arg1615z00_1740, BNIL);
															BgL_arg1611z00_1738 =
																BGl_formatz00zz__r4_output_6_10_3z00
																(BGl_string1877z00zzast_pragmaz00,
																BgL_list1616z00_1741);
													}}
													BgL_arg1613z00_1739 =
														MAKE_YOUNG_PAIR(BINT(BgL_minz00_1729),
														BINT(BgL_maxz00_1730));
													BgL_arg1747z00_1839 =
														BGl_errorz00zz__errorz00
														(BGl_string1878z00zzast_pragmaz00,
														BgL_arg1611z00_1738, BgL_arg1613z00_1739);
									}}}
									{	/* Ast/pragma.scm 143 */

										return
											BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00
											(BgL_arg1747z00_1839, BINT(10L));
									}
								}
								break;
							default:
								return
									BGl_errorz00zz__errorz00(BGl_string1875z00zzast_pragmaz00,
									BGl_string1876z00zzast_pragmaz00, BINT(BgL_matchz00_1836));
							}
					}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_pragmaz00(void)
	{
		{	/* Ast/pragma.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_pragmaz00(void)
	{
		{	/* Ast/pragma.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_pragmaz00(void)
	{
		{	/* Ast/pragma.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_pragmaz00(void)
	{
		{	/* Ast/pragma.scm 14 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1879z00zzast_pragmaz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1879z00zzast_pragmaz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1879z00zzast_pragmaz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1879z00zzast_pragmaz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1879z00zzast_pragmaz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1879z00zzast_pragmaz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1879z00zzast_pragmaz00));
			return
				BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1879z00zzast_pragmaz00));
		}

	}

#ifdef __cplusplus
}
#endif
