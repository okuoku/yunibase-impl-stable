/*===========================================================================*/
/*   (Ast/check_type.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/check_type.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_AST_CHECKzd2TYPEzd2_TYPE_DEFINITIONS
#define BGL_BgL_AST_CHECKzd2TYPEzd2_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;


#endif													// BGL_BgL_AST_CHECKzd2TYPEzd2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t
		BGl_z52checkzd2nodezd2typez52zzast_checkzd2typezd2(obj_t, BgL_nodez00_bglt,
		bool_t, bool_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	extern obj_t BGl_za2intza2z00zztype_cachez00;
	static obj_t
		BGl_z62checkzd2nodezd2typezd2letzd21316z62zzast_checkzd2typezd2(obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzast_checkzd2typezd2 = BUNSPEC;
	static obj_t
		BGl_z62checkzd2nodezd2typezd2letzd21318z62zzast_checkzd2typezd2(obj_t,
		obj_t);
	extern obj_t BGl_closurez00zzast_nodez00;
	extern obj_t BGl_userzd2errorzf2locationz20zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_za2pairzd2nilza2zd2zztype_cachez00;
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_tclassz00zzobject_classz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t
		BGl_z62checkzd2nodezd2typezd2setq1308zb0zzast_checkzd2typezd2(obj_t, obj_t);
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t
		BGl_z62checkzd2nodezd2typezd2appzd21330z62zzast_checkzd2typezd2(obj_t,
		obj_t);
	static obj_t BGl_toplevelzd2initzd2zzast_checkzd2typezd2(void);
	static obj_t
		BGl_z62checkzd2nodezd2typezd2sequ1304zb0zzast_checkzd2typezd2(obj_t, obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62checkzd2nodezd2typezd2fail1312zb0zzast_checkzd2typezd2(obj_t, obj_t);
	static obj_t
		BGl_z62checkzd2nodezd2typezd2sync1306zb0zzast_checkzd2typezd2(obj_t, obj_t);
	static obj_t
		BGl_z62checkzd2nodezd2typezd2make1324zb0zzast_checkzd2typezd2(obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzast_checkzd2typezd2(void);
	static obj_t
		BGl_z62checkzd2nodezd2typezd2swit1314zb0zzast_checkzd2typezd2(obj_t, obj_t);
	static obj_t BGl_z62z52checkzd2nodezd2typez30zzast_checkzd2typezd2(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_za2cellza2z00zztype_cachez00;
	static obj_t BGl_objectzd2initzd2zzast_checkzd2typezd2(void);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	extern obj_t BGl_za2foreignza2z00zztype_cachez00;
	extern obj_t BGl_za2longza2z00zztype_cachez00;
	extern obj_t BGl_za2magicza2z00zztype_cachez00;
	static bool_t BGl_za2checkzd2correctnessza2zd2zzast_checkzd2typezd2;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62checkzd2nodezd2typezd2kwot1300zb0zzast_checkzd2typezd2(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzast_checkzd2typezd2(void);
	BGL_IMPORT obj_t bgl_typeof(obj_t);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	static obj_t
		BGl_z62checkzd2nodezd2typezd2clos1302zb0zzast_checkzd2typezd2(obj_t, obj_t);
	static obj_t
		BGl_z62checkzd2nodezd2typezd2boxzd21326z62zzast_checkzd2typezd2(obj_t,
		obj_t);
	static obj_t
		BGl_z62checkzd2nodezd2typezd2boxzd21328z62zzast_checkzd2typezd2(obj_t,
		obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern BgL_typez00_bglt BGl_getzd2typezd2zztype_typeofz00(BgL_nodez00_bglt,
		bool_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t
		BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(BgL_nodez00_bglt);
	extern obj_t BGl_za2pairza2z00zztype_cachez00;
	static obj_t BGl_z62checkzd2nodezd2typez62zzast_checkzd2typezd2(obj_t, obj_t);
	extern BgL_typez00_bglt BGl_getzd2typezd2atomz00zztype_typeofz00(obj_t);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	extern obj_t BGl_za2procedureza2z00zztype_cachez00;
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	static obj_t BGl_errzd2nozd2typez00zzast_checkzd2typezd2(BgL_nodez00_bglt);
	static obj_t BGl_z62checkzd2nodezd2type1293z62zzast_checkzd2typezd2(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static obj_t BGl_za2checkzd2typezd2passza2z00zzast_checkzd2typezd2 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_checkzd2typezd2zzast_checkzd2typezd2(obj_t, obj_t,
		bool_t, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzast_checkzd2typezd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_includez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztvector_tvectorz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_coercionz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern bool_t BGl_typezd2subclasszf3z21zzobject_classz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	static obj_t
		BGl_z62checkzd2nodezd2typezd2setzd21320z62zzast_checkzd2typezd2(obj_t,
		obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static bool_t BGl_za2checkzd2fullza2zd2zzast_checkzd2typezd2;
	static obj_t BGl_z62checkzd2typezb0zzast_checkzd2typezd2(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static bool_t BGl_eqtypezf3zf3zzast_checkzd2typezd2(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_tprintz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	static obj_t
		BGl_z62checkzd2nodezd2typezd2func1332zb0zzast_checkzd2typezd2(obj_t, obj_t);
	extern obj_t BGl_kwotez00zzast_nodez00;
	static obj_t
		BGl_z62checkzd2nodezd2typezd2var1296zb0zzast_checkzd2typezd2(obj_t, obj_t);
	extern bool_t BGl_coercerzd2existszf3z21zztype_coercionz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzast_checkzd2typezd2(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_checkzd2typezd2(void);
	static obj_t
		BGl_z62checkzd2nodezd2typezd2app1334zb0zzast_checkzd2typezd2(obj_t, obj_t);
	extern obj_t BGl_za2unsafezd2typeza2zd2zzengine_paramz00;
	static obj_t BGl_importedzd2moduleszd2initz00zzast_checkzd2typezd2(void);
	static obj_t BGl_gczd2rootszd2initz00zzast_checkzd2typezd2(void);
	extern obj_t BGl_tvecz00zztvector_tvectorz00;
	static bool_t BGl_subtypezf3zf3zzast_checkzd2typezd2(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_za2bnilza2z00zztype_cachez00;
	extern obj_t BGl_za2compilerzd2typezd2debugzf3za2zf3zzengine_paramz00;
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	extern obj_t BGl_za2unspecza2z00zztype_cachez00;
	extern obj_t BGl_za2epairza2z00zztype_cachez00;
	static bool_t BGl_atomzd2subtypezf3z21zzast_checkzd2typezd2(BgL_typez00_bglt,
		BgL_typez00_bglt);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	extern bool_t BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_bglt);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	extern BgL_typez00_bglt BGl_getzd2typezd2kwotez00zztype_typeofz00(obj_t);
	static obj_t BGl_errz00zzast_checkzd2typezd2(obj_t, BgL_typez00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_IMPORT obj_t
		BGl_findzd2superzd2classzd2methodzd2zz__objectz00(BgL_objectz00_bglt, obj_t,
		obj_t);
	static obj_t
		BGl_z62checkzd2nodezd2typezd2atom1298zb0zzast_checkzd2typezd2(obj_t, obj_t);
	static obj_t
		BGl_z62checkzd2nodezd2typezd2cond1310zb0zzast_checkzd2typezd2(obj_t, obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t
		BGl_z62checkzd2nodezd2typezd2jump1322zb0zzast_checkzd2typezd2(obj_t, obj_t);
	static obj_t __cnst[3];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2113z00zzast_checkzd2typezd2,
		BgL_bgl_za762checkza7d2nodeza72145za7,
		BGl_z62checkzd2nodezd2typezd2atom1298zb0zzast_checkzd2typezd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2114z00zzast_checkzd2typezd2,
		BgL_bgl_za762checkza7d2nodeza72146za7,
		BGl_z62checkzd2nodezd2typezd2kwot1300zb0zzast_checkzd2typezd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2115z00zzast_checkzd2typezd2,
		BgL_bgl_za762checkza7d2nodeza72147za7,
		BGl_z62checkzd2nodezd2typezd2clos1302zb0zzast_checkzd2typezd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2116z00zzast_checkzd2typezd2,
		BgL_bgl_za762checkza7d2nodeza72148za7,
		BGl_z62checkzd2nodezd2typezd2sequ1304zb0zzast_checkzd2typezd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2117z00zzast_checkzd2typezd2,
		BgL_bgl_za762checkza7d2nodeza72149za7,
		BGl_z62checkzd2nodezd2typezd2sync1306zb0zzast_checkzd2typezd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2118z00zzast_checkzd2typezd2,
		BgL_bgl_za762checkza7d2nodeza72150za7,
		BGl_z62checkzd2nodezd2typezd2setq1308zb0zzast_checkzd2typezd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2119z00zzast_checkzd2typezd2,
		BgL_bgl_za762checkza7d2nodeza72151za7,
		BGl_z62checkzd2nodezd2typezd2cond1310zb0zzast_checkzd2typezd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2120z00zzast_checkzd2typezd2,
		BgL_bgl_za762checkza7d2nodeza72152za7,
		BGl_z62checkzd2nodezd2typezd2fail1312zb0zzast_checkzd2typezd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2121z00zzast_checkzd2typezd2,
		BgL_bgl_za762checkza7d2nodeza72153za7,
		BGl_z62checkzd2nodezd2typezd2swit1314zb0zzast_checkzd2typezd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2122z00zzast_checkzd2typezd2,
		BgL_bgl_za762checkza7d2nodeza72154za7,
		BGl_z62checkzd2nodezd2typezd2letzd21316z62zzast_checkzd2typezd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2123z00zzast_checkzd2typezd2,
		BgL_bgl_za762checkza7d2nodeza72155za7,
		BGl_z62checkzd2nodezd2typezd2letzd21318z62zzast_checkzd2typezd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2124z00zzast_checkzd2typezd2,
		BgL_bgl_za762checkza7d2nodeza72156za7,
		BGl_z62checkzd2nodezd2typezd2setzd21320z62zzast_checkzd2typezd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2125z00zzast_checkzd2typezd2,
		BgL_bgl_za762checkza7d2nodeza72157za7,
		BGl_z62checkzd2nodezd2typezd2jump1322zb0zzast_checkzd2typezd2, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string2132z00zzast_checkzd2typezd2,
		BgL_bgl_string2132za700za7za7a2158za7, " check-full=", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2126z00zzast_checkzd2typezd2,
		BgL_bgl_za762checkza7d2nodeza72159za7,
		BGl_z62checkzd2nodezd2typezd2make1324zb0zzast_checkzd2typezd2, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string2133z00zzast_checkzd2typezd2,
		BgL_bgl_string2133za700za7za7a2160za7, " sub=", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2127z00zzast_checkzd2typezd2,
		BgL_bgl_za762checkza7d2nodeza72161za7,
		BGl_z62checkzd2nodezd2typezd2boxzd21326z62zzast_checkzd2typezd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2134z00zzast_checkzd2typezd2,
		BgL_bgl_string2134za700za7za7a2162za7, " eq=", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2128z00zzast_checkzd2typezd2,
		BgL_bgl_za762checkza7d2nodeza72163za7,
		BGl_z62checkzd2nodezd2typezd2boxzd21328z62zzast_checkzd2typezd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2135z00zzast_checkzd2typezd2,
		BgL_bgl_string2135za700za7za7a2164za7, " vtype=", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2129z00zzast_checkzd2typezd2,
		BgL_bgl_za762checkza7d2nodeza72165za7,
		BGl_z62checkzd2nodezd2typezd2appzd21330z62zzast_checkzd2typezd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2136z00zzast_checkzd2typezd2,
		BgL_bgl_string2136za700za7za7a2166za7, " type=", 6);
	      DEFINE_STRING(BGl_string2137z00zzast_checkzd2typezd2,
		BgL_bgl_string2137za700za7za7a2167za7, " loc=", 5);
	      DEFINE_STRING(BGl_string2138z00zzast_checkzd2typezd2,
		BgL_bgl_string2138za700za7za7a2168za7, "ERR: ", 5);
	      DEFINE_STRING(BGl_string2139z00zzast_checkzd2typezd2,
		BgL_bgl_string2139za700za7za7a2169za7, ":", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2130z00zzast_checkzd2typezd2,
		BgL_bgl_za762checkza7d2nodeza72170za7,
		BGl_z62checkzd2nodezd2typezd2func1332zb0zzast_checkzd2typezd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2131z00zzast_checkzd2typezd2,
		BgL_bgl_za762checkza7d2nodeza72171za7,
		BGl_z62checkzd2nodezd2typezd2app1334zb0zzast_checkzd2typezd2, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string2140z00zzast_checkzd2typezd2,
		BgL_bgl_string2140za700za7za7a2172za7, ",", 1);
	      DEFINE_STRING(BGl_string2141z00zzast_checkzd2typezd2,
		BgL_bgl_string2141za700za7za7a2173za7, "Ast/check_type.scm", 18);
	      DEFINE_STRING(BGl_string2142z00zzast_checkzd2typezd2,
		BgL_bgl_string2142za700za7za7a2174za7, "ast_check-type", 14);
	      DEFINE_STRING(BGl_string2143z00zzast_checkzd2typezd2,
		BgL_bgl_string2143za700za7za7a2175za7, "static elight tvector ", 22);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_checkzd2typezd2envz00zzast_checkzd2typezd2,
		BgL_bgl_za762checkza7d2typeza72176za7,
		BGl_z62checkzd2typezb0zzast_checkzd2typezd2, 0L, BUNSPEC, 4);
	     
		DEFINE_STATIC_BGL_GENERIC
		(BGl_checkzd2nodezd2typezd2envzd2zzast_checkzd2typezd2,
		BgL_bgl_za762checkza7d2nodeza72177za7,
		BGl_z62checkzd2nodezd2typez62zzast_checkzd2typezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_z52checkzd2nodezd2typezd2envz80zzast_checkzd2typezd2,
		BgL_bgl_za762za752checkza7d2no2178za7,
		BGl_z62z52checkzd2nodezd2typez30zzast_checkzd2typezd2, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string2101z00zzast_checkzd2typezd2,
		BgL_bgl_string2101za700za7za7a2179za7, " error", 6);
	      DEFINE_STRING(BGl_string2102z00zzast_checkzd2typezd2,
		BgL_bgl_string2102za700za7za7a2180za7, "s", 1);
	      DEFINE_STRING(BGl_string2103z00zzast_checkzd2typezd2,
		BgL_bgl_string2103za700za7za7a2181za7, "", 0);
	      DEFINE_STRING(BGl_string2104z00zzast_checkzd2typezd2,
		BgL_bgl_string2104za700za7za7a2182za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string2105z00zzast_checkzd2typezd2,
		BgL_bgl_string2105za700za7za7a2183za7, "failure during postlude hook", 28);
	      DEFINE_STRING(BGl_string2106z00zzast_checkzd2typezd2,
		BgL_bgl_string2106za700za7za7a2184za7, "check-node-type (~a)", 20);
	      DEFINE_STRING(BGl_string2107z00zzast_checkzd2typezd2,
		BgL_bgl_string2107za700za7za7a2185za7,
		"Inconsistent type [~a], \"~a\" expected, \"~a\" provided", 52);
	      DEFINE_STRING(BGl_string2108z00zzast_checkzd2typezd2,
		BgL_bgl_string2108za700za7za7a2186za7, "Untyped node", 12);
	      DEFINE_STRING(BGl_string2110z00zzast_checkzd2typezd2,
		BgL_bgl_string2110za700za7za7a2187za7, "check-node-type1293", 19);
	      DEFINE_STRING(BGl_string2112z00zzast_checkzd2typezd2,
		BgL_bgl_string2112za700za7za7a2188za7, "check-node-type", 15);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2109z00zzast_checkzd2typezd2,
		BgL_bgl_za762checkza7d2nodeza72189za7,
		BGl_z62checkzd2nodezd2type1293z62zzast_checkzd2typezd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2111z00zzast_checkzd2typezd2,
		BgL_bgl_za762checkza7d2nodeza72190za7,
		BGl_z62checkzd2nodezd2typezd2var1296zb0zzast_checkzd2typezd2, 0L, BUNSPEC,
		1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzast_checkzd2typezd2));
		     ADD_ROOT((void
				*) (&BGl_za2checkzd2typezd2passza2z00zzast_checkzd2typezd2));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzast_checkzd2typezd2(long
		BgL_checksumz00_3108, char *BgL_fromz00_3109)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_checkzd2typezd2))
				{
					BGl_requirezd2initializa7ationz75zzast_checkzd2typezd2 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_checkzd2typezd2();
					BGl_libraryzd2moduleszd2initz00zzast_checkzd2typezd2();
					BGl_cnstzd2initzd2zzast_checkzd2typezd2();
					BGl_importedzd2moduleszd2initz00zzast_checkzd2typezd2();
					BGl_genericzd2initzd2zzast_checkzd2typezd2();
					BGl_methodzd2initzd2zzast_checkzd2typezd2();
					return BGl_toplevelzd2initzd2zzast_checkzd2typezd2();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_checkzd2typezd2(void)
	{
		{	/* Ast/check_type.scm 16 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"ast_check-type");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "ast_check-type");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"ast_check-type");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"ast_check-type");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_check-type");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"ast_check-type");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "ast_check-type");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_check-type");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"ast_check-type");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "ast_check-type");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"ast_check-type");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"ast_check-type");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "ast_check-type");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_checkzd2typezd2(void)
	{
		{	/* Ast/check_type.scm 16 */
			{	/* Ast/check_type.scm 16 */
				obj_t BgL_cportz00_2900;

				{	/* Ast/check_type.scm 16 */
					obj_t BgL_stringz00_2907;

					BgL_stringz00_2907 = BGl_string2143z00zzast_checkzd2typezd2;
					{	/* Ast/check_type.scm 16 */
						obj_t BgL_startz00_2908;

						BgL_startz00_2908 = BINT(0L);
						{	/* Ast/check_type.scm 16 */
							obj_t BgL_endz00_2909;

							BgL_endz00_2909 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2907)));
							{	/* Ast/check_type.scm 16 */

								BgL_cportz00_2900 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2907, BgL_startz00_2908, BgL_endz00_2909);
				}}}}
				{
					long BgL_iz00_2901;

					BgL_iz00_2901 = 2L;
				BgL_loopz00_2902:
					if ((BgL_iz00_2901 == -1L))
						{	/* Ast/check_type.scm 16 */
							return BUNSPEC;
						}
					else
						{	/* Ast/check_type.scm 16 */
							{	/* Ast/check_type.scm 16 */
								obj_t BgL_arg2144z00_2903;

								{	/* Ast/check_type.scm 16 */

									{	/* Ast/check_type.scm 16 */
										obj_t BgL_locationz00_2905;

										BgL_locationz00_2905 = BBOOL(((bool_t) 0));
										{	/* Ast/check_type.scm 16 */

											BgL_arg2144z00_2903 =
												BGl_readz00zz__readerz00(BgL_cportz00_2900,
												BgL_locationz00_2905);
										}
									}
								}
								{	/* Ast/check_type.scm 16 */
									int BgL_tmpz00_3142;

									BgL_tmpz00_3142 = (int) (BgL_iz00_2901);
									CNST_TABLE_SET(BgL_tmpz00_3142, BgL_arg2144z00_2903);
							}}
							{	/* Ast/check_type.scm 16 */
								int BgL_auxz00_2906;

								BgL_auxz00_2906 = (int) ((BgL_iz00_2901 - 1L));
								{
									long BgL_iz00_3147;

									BgL_iz00_3147 = (long) (BgL_auxz00_2906);
									BgL_iz00_2901 = BgL_iz00_3147;
									goto BgL_loopz00_2902;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_checkzd2typezd2(void)
	{
		{	/* Ast/check_type.scm 16 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzast_checkzd2typezd2(void)
	{
		{	/* Ast/check_type.scm 16 */
			BGl_za2checkzd2typezd2passza2z00zzast_checkzd2typezd2 = BFALSE;
			BGl_za2checkzd2fullza2zd2zzast_checkzd2typezd2 = ((bool_t) 0);
			BGl_za2checkzd2correctnessza2zd2zzast_checkzd2typezd2 = ((bool_t) 0);
			return BUNSPEC;
		}

	}



/* %check-node-type */
	BGL_EXPORTED_DEF obj_t
		BGl_z52checkzd2nodezd2typez52zzast_checkzd2typezd2(obj_t BgL_funz00_25,
		BgL_nodez00_bglt BgL_nodez00_26, bool_t BgL_fullz00_27,
		bool_t BgL_correctnessz00_28)
	{
		{	/* Ast/check_type.scm 41 */
			BGl_za2checkzd2typezd2passza2z00zzast_checkzd2typezd2 = BgL_funz00_25;
			BGl_za2checkzd2fullza2zd2zzast_checkzd2typezd2 = BgL_fullz00_27;
			BGl_za2checkzd2correctnessza2zd2zzast_checkzd2typezd2 =
				BgL_correctnessz00_28;
			return BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(BgL_nodez00_26);
		}

	}



/* &%check-node-type */
	obj_t BGl_z62z52checkzd2nodezd2typez30zzast_checkzd2typezd2(obj_t
		BgL_envz00_2824, obj_t BgL_funz00_2825, obj_t BgL_nodez00_2826,
		obj_t BgL_fullz00_2827, obj_t BgL_correctnessz00_2828)
	{
		{	/* Ast/check_type.scm 41 */
			return
				BGl_z52checkzd2nodezd2typez52zzast_checkzd2typezd2(BgL_funz00_2825,
				((BgL_nodez00_bglt) BgL_nodez00_2826),
				CBOOL(BgL_fullz00_2827), CBOOL(BgL_correctnessz00_2828));
		}

	}



/* check-type */
	BGL_EXPORTED_DEF obj_t BGl_checkzd2typezd2zzast_checkzd2typezd2(obj_t
		BgL_passz00_29, obj_t BgL_globalsz00_30, bool_t BgL_fullz00_31,
		bool_t BgL_correctnessz00_32)
	{
		{	/* Ast/check_type.scm 50 */
			if (CBOOL(BGl_za2compilerzd2typezd2debugzf3za2zf3zzengine_paramz00))
				{	/* Ast/check_type.scm 51 */
					BGl_za2checkzd2typezd2passza2z00zzast_checkzd2typezd2 =
						BgL_passz00_29;
					BGl_za2checkzd2fullza2zd2zzast_checkzd2typezd2 = BgL_fullz00_31;
					BGl_za2checkzd2correctnessza2zd2zzast_checkzd2typezd2 =
						BgL_correctnessz00_32;
					{
						obj_t BgL_l1275z00_1725;

						BgL_l1275z00_1725 = BgL_globalsz00_30;
					BgL_zc3z04anonymousza31365ze3z87_1726:
						if (PAIRP(BgL_l1275z00_1725))
							{	/* Ast/check_type.scm 55 */
								{	/* Ast/check_type.scm 55 */
									obj_t BgL_arg1367z00_1728;

									BgL_arg1367z00_1728 = CAR(BgL_l1275z00_1725);
									{	/* Ast/check_type.scm 69 */
										BgL_valuez00_bglt BgL_funz00_2387;

										BgL_funz00_2387 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_arg1367z00_1728)))->
											BgL_valuez00);
										{	/* Ast/check_type.scm 69 */
											obj_t BgL_bodyz00_2388;

											BgL_bodyz00_2388 =
												(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_funz00_2387)))->
												BgL_bodyz00);
											{	/* Ast/check_type.scm 70 */

												BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
													((BgL_nodez00_bglt) BgL_bodyz00_2388));
											}
										}
									}
								}
								{
									obj_t BgL_l1275z00_3166;

									BgL_l1275z00_3166 = CDR(BgL_l1275z00_1725);
									BgL_l1275z00_1725 = BgL_l1275z00_3166;
									goto BgL_zc3z04anonymousza31365ze3z87_1726;
								}
							}
						else
							{	/* Ast/check_type.scm 55 */
								((bool_t) 1);
							}
					}
					if (
						((long) CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) >
							0L))
						{	/* Ast/check_type.scm 56 */
							{	/* Ast/check_type.scm 56 */
								obj_t BgL_port1277z00_1733;

								{	/* Ast/check_type.scm 56 */
									obj_t BgL_tmpz00_3171;

									BgL_tmpz00_3171 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_port1277z00_1733 =
										BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_3171);
								}
								bgl_display_obj
									(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
									BgL_port1277z00_1733);
								bgl_display_string(BGl_string2101z00zzast_checkzd2typezd2,
									BgL_port1277z00_1733);
								{	/* Ast/check_type.scm 56 */
									obj_t BgL_arg1375z00_1734;

									{	/* Ast/check_type.scm 56 */
										bool_t BgL_test2196z00_3176;

										if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
											(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
											{	/* Ast/check_type.scm 56 */
												if (INTEGERP
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
													{	/* Ast/check_type.scm 56 */
														BgL_test2196z00_3176 =
															(
															(long)
															CINT
															(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
															> 1L);
													}
												else
													{	/* Ast/check_type.scm 56 */
														BgL_test2196z00_3176 =
															BGl_2ze3ze3zz__r4_numbers_6_5z00
															(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
															BINT(1L));
													}
											}
										else
											{	/* Ast/check_type.scm 56 */
												BgL_test2196z00_3176 = ((bool_t) 0);
											}
										if (BgL_test2196z00_3176)
											{	/* Ast/check_type.scm 56 */
												BgL_arg1375z00_1734 =
													BGl_string2102z00zzast_checkzd2typezd2;
											}
										else
											{	/* Ast/check_type.scm 56 */
												BgL_arg1375z00_1734 =
													BGl_string2103z00zzast_checkzd2typezd2;
											}
									}
									bgl_display_obj(BgL_arg1375z00_1734, BgL_port1277z00_1733);
								}
								bgl_display_string(BGl_string2104z00zzast_checkzd2typezd2,
									BgL_port1277z00_1733);
								bgl_display_char(((unsigned char) 10), BgL_port1277z00_1733);
							}
							{	/* Ast/check_type.scm 56 */
								obj_t BgL_list1384z00_1738;

								BgL_list1384z00_1738 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
								return BGl_exitz00zz__errorz00(BgL_list1384z00_1738);
							}
						}
					else
						{	/* Ast/check_type.scm 56 */
							obj_t BgL_g1111z00_1739;

							BgL_g1111z00_1739 = BNIL;
							{
								obj_t BgL_hooksz00_1742;
								obj_t BgL_hnamesz00_1743;

								BgL_hooksz00_1742 = BgL_g1111z00_1739;
								BgL_hnamesz00_1743 = BNIL;
							BgL_zc3z04anonymousza31385ze3z87_1744:
								if (NULLP(BgL_hooksz00_1742))
									{	/* Ast/check_type.scm 56 */
										return BgL_globalsz00_30;
									}
								else
									{	/* Ast/check_type.scm 56 */
										bool_t BgL_test2200z00_3193;

										{	/* Ast/check_type.scm 56 */
											obj_t BgL_fun1423z00_1751;

											BgL_fun1423z00_1751 = CAR(((obj_t) BgL_hooksz00_1742));
											BgL_test2200z00_3193 =
												CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1423z00_1751));
										}
										if (BgL_test2200z00_3193)
											{	/* Ast/check_type.scm 56 */
												obj_t BgL_arg1408z00_1748;
												obj_t BgL_arg1410z00_1749;

												BgL_arg1408z00_1748 = CDR(((obj_t) BgL_hooksz00_1742));
												BgL_arg1410z00_1749 = CDR(((obj_t) BgL_hnamesz00_1743));
												{
													obj_t BgL_hnamesz00_3205;
													obj_t BgL_hooksz00_3204;

													BgL_hooksz00_3204 = BgL_arg1408z00_1748;
													BgL_hnamesz00_3205 = BgL_arg1410z00_1749;
													BgL_hnamesz00_1743 = BgL_hnamesz00_3205;
													BgL_hooksz00_1742 = BgL_hooksz00_3204;
													goto BgL_zc3z04anonymousza31385ze3z87_1744;
												}
											}
										else
											{	/* Ast/check_type.scm 56 */
												obj_t BgL_arg1421z00_1750;

												BgL_arg1421z00_1750 = CAR(((obj_t) BgL_hnamesz00_1743));
												return
													BGl_internalzd2errorzd2zztools_errorz00
													(BGl_za2currentzd2passza2zd2zzengine_passz00,
													BGl_string2105z00zzast_checkzd2typezd2,
													BgL_arg1421z00_1750);
											}
									}
							}
						}
				}
			else
				{	/* Ast/check_type.scm 51 */
					return BFALSE;
				}
		}

	}



/* &check-type */
	obj_t BGl_z62checkzd2typezb0zzast_checkzd2typezd2(obj_t BgL_envz00_2829,
		obj_t BgL_passz00_2830, obj_t BgL_globalsz00_2831, obj_t BgL_fullz00_2832,
		obj_t BgL_correctnessz00_2833)
	{
		{	/* Ast/check_type.scm 50 */
			return
				BGl_checkzd2typezd2zzast_checkzd2typezd2(BgL_passz00_2830,
				BgL_globalsz00_2831, CBOOL(BgL_fullz00_2832),
				CBOOL(BgL_correctnessz00_2833));
		}

	}



/* err */
	obj_t BGl_errz00zzast_checkzd2typezd2(obj_t BgL_nodez00_34,
		BgL_typez00_bglt BgL_t1z00_35, obj_t BgL_t2z00_36)
	{
		{	/* Ast/check_type.scm 76 */
			{	/* Ast/check_type.scm 77 */
				obj_t BgL_arg1434z00_1756;
				obj_t BgL_arg1437z00_1757;
				obj_t BgL_arg1448z00_1758;
				obj_t BgL_arg1453z00_1759;

				BgL_arg1434z00_1756 =
					(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_nodez00_34)))->BgL_locz00);
				{	/* Ast/check_type.scm 78 */
					obj_t BgL_list1455z00_1761;

					BgL_list1455z00_1761 =
						MAKE_YOUNG_PAIR
						(BGl_za2checkzd2typezd2passza2z00zzast_checkzd2typezd2, BNIL);
					BgL_arg1437z00_1757 =
						BGl_formatz00zz__r4_output_6_10_3z00
						(BGl_string2106z00zzast_checkzd2typezd2, BgL_list1455z00_1761);
				}
				{	/* Ast/check_type.scm 79 */
					obj_t BgL_arg1472z00_1762;
					obj_t BgL_arg1473z00_1763;
					obj_t BgL_arg1485z00_1764;

					BgL_arg1472z00_1762 = bgl_typeof(BgL_nodez00_34);
					BgL_arg1473z00_1763 = BGl_shapez00zztools_shapez00(BgL_t2z00_36);
					BgL_arg1485z00_1764 =
						BGl_shapez00zztools_shapez00(((obj_t) BgL_t1z00_35));
					{	/* Ast/check_type.scm 79 */
						obj_t BgL_list1486z00_1765;

						{	/* Ast/check_type.scm 79 */
							obj_t BgL_arg1489z00_1766;

							{	/* Ast/check_type.scm 79 */
								obj_t BgL_arg1502z00_1767;

								BgL_arg1502z00_1767 =
									MAKE_YOUNG_PAIR(BgL_arg1485z00_1764, BNIL);
								BgL_arg1489z00_1766 =
									MAKE_YOUNG_PAIR(BgL_arg1473z00_1763, BgL_arg1502z00_1767);
							}
							BgL_list1486z00_1765 =
								MAKE_YOUNG_PAIR(BgL_arg1472z00_1762, BgL_arg1489z00_1766);
						}
						BgL_arg1448z00_1758 =
							BGl_formatz00zz__r4_output_6_10_3z00
							(BGl_string2107z00zzast_checkzd2typezd2, BgL_list1486z00_1765);
					}
				}
				BgL_arg1453z00_1759 = BGl_shapez00zztools_shapez00(BgL_nodez00_34);
				return
					BGl_userzd2errorzf2locationz20zztools_errorz00(BgL_arg1434z00_1756,
					BgL_arg1437z00_1757, BgL_arg1448z00_1758, BgL_arg1453z00_1759, BNIL);
			}
		}

	}



/* err-no-type */
	obj_t BGl_errzd2nozd2typez00zzast_checkzd2typezd2(BgL_nodez00_bglt
		BgL_nodez00_37)
	{
		{	/* Ast/check_type.scm 85 */
			{	/* Ast/check_type.scm 86 */
				obj_t BgL_arg1509z00_1768;
				obj_t BgL_arg1513z00_1769;
				obj_t BgL_arg1514z00_1770;

				BgL_arg1509z00_1768 =
					(((BgL_nodez00_bglt) COBJECT(BgL_nodez00_37))->BgL_locz00);
				{	/* Ast/check_type.scm 87 */
					obj_t BgL_list1516z00_1772;

					BgL_list1516z00_1772 =
						MAKE_YOUNG_PAIR
						(BGl_za2checkzd2typezd2passza2z00zzast_checkzd2typezd2, BNIL);
					BgL_arg1513z00_1769 =
						BGl_formatz00zz__r4_output_6_10_3z00
						(BGl_string2106z00zzast_checkzd2typezd2, BgL_list1516z00_1772);
				}
				BgL_arg1514z00_1770 =
					BGl_shapez00zztools_shapez00(((obj_t) BgL_nodez00_37));
				return
					BGl_userzd2errorzf2locationz20zztools_errorz00(BgL_arg1509z00_1768,
					BgL_arg1513z00_1769, BGl_string2108z00zzast_checkzd2typezd2,
					BgL_arg1514z00_1770, BNIL);
			}
		}

	}



/* eqtype? */
	bool_t BGl_eqtypezf3zf3zzast_checkzd2typezd2(obj_t BgL_t1z00_60,
		obj_t BgL_t2z00_61)
	{
		{	/* Ast/check_type.scm 320 */
			{	/* Ast/check_type.scm 321 */
				bool_t BgL__ortest_1135z00_1778;

				{	/* Ast/check_type.scm 321 */
					bool_t BgL__ortest_1136z00_1780;

					BgL__ortest_1136z00_1780 = (BgL_t1z00_60 == BgL_t2z00_61);
					if (BgL__ortest_1136z00_1780)
						{	/* Ast/check_type.scm 321 */
							BgL__ortest_1135z00_1778 = BgL__ortest_1136z00_1780;
						}
					else
						{	/* Ast/check_type.scm 321 */
							bool_t BgL__ortest_1137z00_1781;

							BgL__ortest_1137z00_1781 =
								(BgL_t1z00_60 == BGl_za2magicza2z00zztype_cachez00);
							if (BgL__ortest_1137z00_1781)
								{	/* Ast/check_type.scm 321 */
									BgL__ortest_1135z00_1778 = BgL__ortest_1137z00_1781;
								}
							else
								{	/* Ast/check_type.scm 321 */
									BgL__ortest_1135z00_1778 =
										(BgL_t2z00_61 == BGl_za2magicza2z00zztype_cachez00);
								}
						}
				}
				if (BgL__ortest_1135z00_1778)
					{	/* Ast/check_type.scm 321 */
						return BgL__ortest_1135z00_1778;
					}
				else
					{	/* Ast/check_type.scm 321 */
						if ((BgL_t1z00_60 == BGl_za2_za2z00zztype_cachez00))
							{	/* Ast/check_type.scm 322 */
								if (BGl_za2checkzd2fullza2zd2zzast_checkzd2typezd2)
									{	/* Ast/check_type.scm 322 */
										return ((bool_t) 0);
									}
								else
									{	/* Ast/check_type.scm 322 */
										return ((bool_t) 1);
									}
							}
						else
							{	/* Ast/check_type.scm 322 */
								return ((bool_t) 0);
							}
					}
			}
		}

	}



/* subtype? */
	bool_t BGl_subtypezf3zf3zzast_checkzd2typezd2(BgL_typez00_bglt BgL_t1z00_62,
		BgL_typez00_bglt BgL_t2z00_63)
	{
		{	/* Ast/check_type.scm 329 */
			if ((((obj_t) BgL_t1z00_62) == ((obj_t) BgL_t2z00_63)))
				{	/* Ast/check_type.scm 331 */
					return ((bool_t) 1);
				}
			else
				{	/* Ast/check_type.scm 333 */
					bool_t BgL_test2207z00_3245;

					if ((((obj_t) BgL_t2z00_63) == BGl_za2_za2z00zztype_cachez00))
						{	/* Ast/check_type.scm 333 */
							BgL_test2207z00_3245 = ((bool_t) 1);
						}
					else
						{	/* Ast/check_type.scm 333 */
							BgL_test2207z00_3245 =
								(((obj_t) BgL_t1z00_62) == BGl_za2_za2z00zztype_cachez00);
						}
					if (BgL_test2207z00_3245)
						{	/* Ast/check_type.scm 333 */
							if (BGl_za2checkzd2fullza2zd2zzast_checkzd2typezd2)
								{	/* Ast/check_type.scm 334 */
									return ((bool_t) 0);
								}
							else
								{	/* Ast/check_type.scm 334 */
									return ((bool_t) 1);
								}
						}
					else
						{	/* Ast/check_type.scm 335 */
							bool_t BgL_test2210z00_3252;

							if (BGl_za2checkzd2correctnessza2zd2zzast_checkzd2typezd2)
								{	/* Ast/check_type.scm 335 */
									BgL_test2210z00_3252 =
										CBOOL(BGl_za2unsafezd2typeza2zd2zzengine_paramz00);
								}
							else
								{	/* Ast/check_type.scm 335 */
									BgL_test2210z00_3252 = ((bool_t) 1);
								}
							if (BgL_test2210z00_3252)
								{	/* Ast/check_type.scm 335 */
									return ((bool_t) 1);
								}
							else
								{	/* Ast/check_type.scm 337 */
									bool_t BgL_test2212z00_3255;

									if (
										(((obj_t) BgL_t1z00_62) ==
											BGl_za2magicza2z00zztype_cachez00))
										{	/* Ast/check_type.scm 337 */
											BgL_test2212z00_3255 = ((bool_t) 1);
										}
									else
										{	/* Ast/check_type.scm 337 */
											BgL_test2212z00_3255 =
												(
												((obj_t) BgL_t2z00_63) ==
												BGl_za2magicza2z00zztype_cachez00);
										}
									if (BgL_test2212z00_3255)
										{	/* Ast/check_type.scm 337 */
											return ((bool_t) 1);
										}
									else
										{	/* Ast/check_type.scm 337 */
											if (
												(((obj_t) BgL_t2z00_63) ==
													BGl_za2pairzd2nilza2zd2zztype_cachez00))
												{	/* Ast/check_type.scm 341 */
													bool_t BgL__ortest_1139z00_1785;

													BgL__ortest_1139z00_1785 =
														(
														((obj_t) BgL_t1z00_62) ==
														BGl_za2pairza2z00zztype_cachez00);
													if (BgL__ortest_1139z00_1785)
														{	/* Ast/check_type.scm 341 */
															return BgL__ortest_1139z00_1785;
														}
													else
														{	/* Ast/check_type.scm 341 */
															bool_t BgL__ortest_1140z00_1786;

															BgL__ortest_1140z00_1786 =
																(
																((obj_t) BgL_t1z00_62) ==
																BGl_za2epairza2z00zztype_cachez00);
															if (BgL__ortest_1140z00_1786)
																{	/* Ast/check_type.scm 341 */
																	return BgL__ortest_1140z00_1786;
																}
															else
																{	/* Ast/check_type.scm 341 */
																	return
																		(
																		((obj_t) BgL_t1z00_62) ==
																		BGl_za2bnilza2z00zztype_cachez00);
																}
														}
												}
											else
												{	/* Ast/check_type.scm 342 */
													bool_t BgL_test2217z00_3272;

													if (
														(((obj_t) BgL_t2z00_63) ==
															BGl_za2pairza2z00zztype_cachez00))
														{	/* Ast/check_type.scm 342 */
															BgL_test2217z00_3272 =
																(BGl_za2epairza2z00zztype_cachez00 ==
																((obj_t) BgL_t1z00_62));
														}
													else
														{	/* Ast/check_type.scm 342 */
															BgL_test2217z00_3272 = ((bool_t) 0);
														}
													if (BgL_test2217z00_3272)
														{	/* Ast/check_type.scm 342 */
															return ((bool_t) 1);
														}
													else
														{	/* Ast/check_type.scm 344 */
															bool_t BgL_test2219z00_3278;

															{	/* Ast/check_type.scm 344 */
																bool_t BgL_test2220z00_3279;

																{	/* Ast/check_type.scm 344 */
																	obj_t BgL_classz00_2411;

																	BgL_classz00_2411 =
																		BGl_tclassz00zzobject_classz00;
																	{	/* Ast/check_type.scm 344 */
																		BgL_objectz00_bglt BgL_arg1807z00_2413;

																		{	/* Ast/check_type.scm 344 */
																			obj_t BgL_tmpz00_3280;

																			BgL_tmpz00_3280 =
																				((obj_t)
																				((BgL_objectz00_bglt) BgL_t1z00_62));
																			BgL_arg1807z00_2413 =
																				(BgL_objectz00_bglt) (BgL_tmpz00_3280);
																		}
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Ast/check_type.scm 344 */
																				long BgL_idxz00_2419;

																				BgL_idxz00_2419 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_2413);
																				BgL_test2220z00_3279 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_2419 + 2L)) ==
																					BgL_classz00_2411);
																			}
																		else
																			{	/* Ast/check_type.scm 344 */
																				bool_t BgL_res2086z00_2444;

																				{	/* Ast/check_type.scm 344 */
																					obj_t BgL_oclassz00_2427;

																					{	/* Ast/check_type.scm 344 */
																						obj_t BgL_arg1815z00_2435;
																						long BgL_arg1816z00_2436;

																						BgL_arg1815z00_2435 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Ast/check_type.scm 344 */
																							long BgL_arg1817z00_2437;

																							BgL_arg1817z00_2437 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_2413);
																							BgL_arg1816z00_2436 =
																								(BgL_arg1817z00_2437 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_2427 =
																							VECTOR_REF(BgL_arg1815z00_2435,
																							BgL_arg1816z00_2436);
																					}
																					{	/* Ast/check_type.scm 344 */
																						bool_t BgL__ortest_1115z00_2428;

																						BgL__ortest_1115z00_2428 =
																							(BgL_classz00_2411 ==
																							BgL_oclassz00_2427);
																						if (BgL__ortest_1115z00_2428)
																							{	/* Ast/check_type.scm 344 */
																								BgL_res2086z00_2444 =
																									BgL__ortest_1115z00_2428;
																							}
																						else
																							{	/* Ast/check_type.scm 344 */
																								long BgL_odepthz00_2429;

																								{	/* Ast/check_type.scm 344 */
																									obj_t BgL_arg1804z00_2430;

																									BgL_arg1804z00_2430 =
																										(BgL_oclassz00_2427);
																									BgL_odepthz00_2429 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_2430);
																								}
																								if ((2L < BgL_odepthz00_2429))
																									{	/* Ast/check_type.scm 344 */
																										obj_t BgL_arg1802z00_2432;

																										{	/* Ast/check_type.scm 344 */
																											obj_t BgL_arg1803z00_2433;

																											BgL_arg1803z00_2433 =
																												(BgL_oclassz00_2427);
																											BgL_arg1802z00_2432 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_2433,
																												2L);
																										}
																										BgL_res2086z00_2444 =
																											(BgL_arg1802z00_2432 ==
																											BgL_classz00_2411);
																									}
																								else
																									{	/* Ast/check_type.scm 344 */
																										BgL_res2086z00_2444 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test2220z00_3279 =
																					BgL_res2086z00_2444;
																			}
																	}
																}
																if (BgL_test2220z00_3279)
																	{	/* Ast/check_type.scm 344 */
																		obj_t BgL_classz00_2445;

																		BgL_classz00_2445 =
																			BGl_tclassz00zzobject_classz00;
																		{	/* Ast/check_type.scm 344 */
																			BgL_objectz00_bglt BgL_arg1807z00_2447;

																			{	/* Ast/check_type.scm 344 */
																				obj_t BgL_tmpz00_3303;

																				BgL_tmpz00_3303 =
																					((obj_t)
																					((BgL_objectz00_bglt) BgL_t2z00_63));
																				BgL_arg1807z00_2447 =
																					(BgL_objectz00_bglt)
																					(BgL_tmpz00_3303);
																			}
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Ast/check_type.scm 344 */
																					long BgL_idxz00_2453;

																					BgL_idxz00_2453 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_2447);
																					BgL_test2219z00_3278 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_2453 + 2L)) ==
																						BgL_classz00_2445);
																				}
																			else
																				{	/* Ast/check_type.scm 344 */
																					bool_t BgL_res2087z00_2478;

																					{	/* Ast/check_type.scm 344 */
																						obj_t BgL_oclassz00_2461;

																						{	/* Ast/check_type.scm 344 */
																							obj_t BgL_arg1815z00_2469;
																							long BgL_arg1816z00_2470;

																							BgL_arg1815z00_2469 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Ast/check_type.scm 344 */
																								long BgL_arg1817z00_2471;

																								BgL_arg1817z00_2471 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_2447);
																								BgL_arg1816z00_2470 =
																									(BgL_arg1817z00_2471 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_2461 =
																								VECTOR_REF(BgL_arg1815z00_2469,
																								BgL_arg1816z00_2470);
																						}
																						{	/* Ast/check_type.scm 344 */
																							bool_t BgL__ortest_1115z00_2462;

																							BgL__ortest_1115z00_2462 =
																								(BgL_classz00_2445 ==
																								BgL_oclassz00_2461);
																							if (BgL__ortest_1115z00_2462)
																								{	/* Ast/check_type.scm 344 */
																									BgL_res2087z00_2478 =
																										BgL__ortest_1115z00_2462;
																								}
																							else
																								{	/* Ast/check_type.scm 344 */
																									long BgL_odepthz00_2463;

																									{	/* Ast/check_type.scm 344 */
																										obj_t BgL_arg1804z00_2464;

																										BgL_arg1804z00_2464 =
																											(BgL_oclassz00_2461);
																										BgL_odepthz00_2463 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_2464);
																									}
																									if ((2L < BgL_odepthz00_2463))
																										{	/* Ast/check_type.scm 344 */
																											obj_t BgL_arg1802z00_2466;

																											{	/* Ast/check_type.scm 344 */
																												obj_t
																													BgL_arg1803z00_2467;
																												BgL_arg1803z00_2467 =
																													(BgL_oclassz00_2461);
																												BgL_arg1802z00_2466 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_2467,
																													2L);
																											}
																											BgL_res2087z00_2478 =
																												(BgL_arg1802z00_2466 ==
																												BgL_classz00_2445);
																										}
																									else
																										{	/* Ast/check_type.scm 344 */
																											BgL_res2087z00_2478 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test2219z00_3278 =
																						BgL_res2087z00_2478;
																				}
																		}
																	}
																else
																	{	/* Ast/check_type.scm 344 */
																		BgL_test2219z00_3278 = ((bool_t) 0);
																	}
															}
															if (BgL_test2219z00_3278)
																{	/* Ast/check_type.scm 344 */
																	BGL_TAIL return
																		BGl_typezd2subclasszf3z21zzobject_classz00
																		(BgL_t1z00_62, BgL_t2z00_63);
																}
															else
																{	/* Ast/check_type.scm 344 */
																	if (
																		(((obj_t) BgL_t2z00_63) ==
																			BGl_za2objza2z00zztype_cachez00))
																		{	/* Ast/check_type.scm 347 */
																			return ((bool_t) 1);
																		}
																	else
																		{	/* Ast/check_type.scm 350 */
																			bool_t BgL_test2228z00_3330;

																			if (
																				(((obj_t) BgL_t2z00_63) ==
																					BGl_za2foreignza2z00zztype_cachez00))
																				{	/* Ast/check_type.scm 350 */
																					BgL_test2228z00_3330 =
																						BGl_coercerzd2existszf3z21zztype_coercionz00
																						(BgL_t1z00_62, BgL_t2z00_63);
																				}
																			else
																				{	/* Ast/check_type.scm 350 */
																					BgL_test2228z00_3330 = ((bool_t) 0);
																				}
																			if (BgL_test2228z00_3330)
																				{	/* Ast/check_type.scm 350 */
																					return ((bool_t) 1);
																				}
																			else
																				{	/* Ast/check_type.scm 353 */
																					bool_t BgL_test2230z00_3335;

																					if (
																						(((obj_t) BgL_t1z00_62) ==
																							BGl_za2foreignza2z00zztype_cachez00))
																						{	/* Ast/check_type.scm 353 */
																							BgL_test2230z00_3335 =
																								BGl_coercerzd2existszf3z21zztype_coercionz00
																								(BgL_t1z00_62, BgL_t2z00_63);
																						}
																					else
																						{	/* Ast/check_type.scm 353 */
																							BgL_test2230z00_3335 =
																								((bool_t) 0);
																						}
																					if (BgL_test2230z00_3335)
																						{	/* Ast/check_type.scm 353 */
																							return ((bool_t) 1);
																						}
																					else
																						{	/* Ast/check_type.scm 353 */
																							if (
																								((((BgL_typez00_bglt)
																											COBJECT(BgL_t1z00_62))->
																										BgL_idz00) ==
																									CNST_TABLE_REF(0)))
																								{	/* Ast/check_type.scm 356 */
																									obj_t BgL_classz00_2480;

																									BgL_classz00_2480 =
																										BGl_tvecz00zztvector_tvectorz00;
																									{	/* Ast/check_type.scm 356 */
																										BgL_objectz00_bglt
																											BgL_arg1807z00_2482;
																										{	/* Ast/check_type.scm 356 */
																											obj_t BgL_tmpz00_3344;

																											BgL_tmpz00_3344 =
																												((obj_t)
																												((BgL_objectz00_bglt)
																													BgL_t2z00_63));
																											BgL_arg1807z00_2482 =
																												(BgL_objectz00_bglt)
																												(BgL_tmpz00_3344);
																										}
																										if (BGL_CONDEXPAND_ISA_ARCH64())
																											{	/* Ast/check_type.scm 356 */
																												long BgL_idxz00_2488;

																												BgL_idxz00_2488 =
																													BGL_OBJECT_INHERITANCE_NUM
																													(BgL_arg1807z00_2482);
																												return (VECTOR_REF
																													(BGl_za2inheritancesza2z00zz__objectz00,
																														(BgL_idxz00_2488 +
																															2L)) ==
																													BgL_classz00_2480);
																											}
																										else
																											{	/* Ast/check_type.scm 356 */
																												bool_t
																													BgL_res2088z00_2513;
																												{	/* Ast/check_type.scm 356 */
																													obj_t
																														BgL_oclassz00_2496;
																													{	/* Ast/check_type.scm 356 */
																														obj_t
																															BgL_arg1815z00_2504;
																														long
																															BgL_arg1816z00_2505;
																														BgL_arg1815z00_2504
																															=
																															(BGl_za2classesza2z00zz__objectz00);
																														{	/* Ast/check_type.scm 356 */
																															long
																																BgL_arg1817z00_2506;
																															BgL_arg1817z00_2506
																																=
																																BGL_OBJECT_CLASS_NUM
																																(BgL_arg1807z00_2482);
																															BgL_arg1816z00_2505
																																=
																																(BgL_arg1817z00_2506
																																- OBJECT_TYPE);
																														}
																														BgL_oclassz00_2496 =
																															VECTOR_REF
																															(BgL_arg1815z00_2504,
																															BgL_arg1816z00_2505);
																													}
																													{	/* Ast/check_type.scm 356 */
																														bool_t
																															BgL__ortest_1115z00_2497;
																														BgL__ortest_1115z00_2497
																															=
																															(BgL_classz00_2480
																															==
																															BgL_oclassz00_2496);
																														if (BgL__ortest_1115z00_2497)
																															{	/* Ast/check_type.scm 356 */
																																BgL_res2088z00_2513
																																	=
																																	BgL__ortest_1115z00_2497;
																															}
																														else
																															{	/* Ast/check_type.scm 356 */
																																long
																																	BgL_odepthz00_2498;
																																{	/* Ast/check_type.scm 356 */
																																	obj_t
																																		BgL_arg1804z00_2499;
																																	BgL_arg1804z00_2499
																																		=
																																		(BgL_oclassz00_2496);
																																	BgL_odepthz00_2498
																																		=
																																		BGL_CLASS_DEPTH
																																		(BgL_arg1804z00_2499);
																																}
																																if (
																																	(2L <
																																		BgL_odepthz00_2498))
																																	{	/* Ast/check_type.scm 356 */
																																		obj_t
																																			BgL_arg1802z00_2501;
																																		{	/* Ast/check_type.scm 356 */
																																			obj_t
																																				BgL_arg1803z00_2502;
																																			BgL_arg1803z00_2502
																																				=
																																				(BgL_oclassz00_2496);
																																			BgL_arg1802z00_2501
																																				=
																																				BGL_CLASS_ANCESTORS_REF
																																				(BgL_arg1803z00_2502,
																																				2L);
																																		}
																																		BgL_res2088z00_2513
																																			=
																																			(BgL_arg1802z00_2501
																																			==
																																			BgL_classz00_2480);
																																	}
																																else
																																	{	/* Ast/check_type.scm 356 */
																																		BgL_res2088z00_2513
																																			=
																																			((bool_t)
																																			0);
																																	}
																															}
																													}
																												}
																												return
																													BgL_res2088z00_2513;
																											}
																									}
																								}
																							else
																								{	/* Ast/check_type.scm 356 */
																									return ((bool_t) 0);
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
				}
		}

	}



/* atom-subtype? */
	bool_t BGl_atomzd2subtypezf3z21zzast_checkzd2typezd2(BgL_typez00_bglt
		BgL_t1z00_64, BgL_typez00_bglt BgL_t2z00_65)
	{
		{	/* Ast/check_type.scm 364 */
			if ((((obj_t) BgL_t1z00_64) == BGl_za2_za2z00zztype_cachez00))
				{	/* Ast/check_type.scm 366 */
					return ((bool_t) 1);
				}
			else
				{	/* Ast/check_type.scm 366 */
					if ((((obj_t) BgL_t1z00_64) == ((obj_t) BgL_t2z00_65)))
						{	/* Ast/check_type.scm 368 */
							return ((bool_t) 1);
						}
					else
						{	/* Ast/check_type.scm 370 */
							bool_t BgL_test2238z00_3374;

							if ((((obj_t) BgL_t1z00_64) == BGl_za2longza2z00zztype_cachez00))
								{	/* Ast/check_type.scm 370 */
									BgL_test2238z00_3374 = ((bool_t) 1);
								}
							else
								{	/* Ast/check_type.scm 370 */
									BgL_test2238z00_3374 =
										(((obj_t) BgL_t1z00_64) == BGl_za2intza2z00zztype_cachez00);
								}
							if (BgL_test2238z00_3374)
								{	/* Ast/check_type.scm 371 */
									bool_t BgL__ortest_1141z00_1800;

									BgL__ortest_1141z00_1800 =
										(
										((obj_t) BgL_t2z00_65) == BGl_za2longza2z00zztype_cachez00);
									if (BgL__ortest_1141z00_1800)
										{	/* Ast/check_type.scm 371 */
											return BgL__ortest_1141z00_1800;
										}
									else
										{	/* Ast/check_type.scm 371 */
											return
												(
												((obj_t) BgL_t2z00_65) ==
												BGl_za2intza2z00zztype_cachez00);
										}
								}
							else
								{	/* Ast/check_type.scm 370 */
									return ((bool_t) 0);
								}
						}
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_checkzd2typezd2(void)
	{
		{	/* Ast/check_type.scm 16 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_checkzd2typezd2(void)
	{
		{	/* Ast/check_type.scm 16 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_checkzd2nodezd2typezd2envzd2zzast_checkzd2typezd2,
				BGl_proc2109z00zzast_checkzd2typezd2, BGl_nodez00zzast_nodez00,
				BGl_string2110z00zzast_checkzd2typezd2);
		}

	}



/* &check-node-type1293 */
	obj_t BGl_z62checkzd2nodezd2type1293z62zzast_checkzd2typezd2(obj_t
		BgL_envz00_2835, obj_t BgL_nodez00_2836)
	{
		{	/* Ast/check_type.scm 103 */
			{	/* Ast/check_type.scm 104 */
				bool_t BgL_test2241z00_3386;

				if (BGl_za2checkzd2fullza2zd2zzast_checkzd2typezd2)
					{	/* Ast/check_type.scm 104 */
						BgL_typez00_bglt BgL_arg1584z00_2912;

						BgL_arg1584z00_2912 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_nodez00_2836)))->BgL_typez00);
						BgL_test2241z00_3386 =
							(((obj_t) BgL_arg1584z00_2912) == BGl_za2_za2z00zztype_cachez00);
					}
				else
					{	/* Ast/check_type.scm 104 */
						BgL_test2241z00_3386 = ((bool_t) 0);
					}
				if (BgL_test2241z00_3386)
					{	/* Ast/check_type.scm 104 */
						return
							BGl_errzd2nozd2typez00zzast_checkzd2typezd2(
							((BgL_nodez00_bglt) BgL_nodez00_2836));
					}
				else
					{	/* Ast/check_type.scm 104 */
						return BFALSE;
					}
			}
		}

	}



/* check-node-type */
	obj_t BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(BgL_nodez00_bglt
		BgL_nodez00_39)
	{
		{	/* Ast/check_type.scm 103 */
			{	/* Ast/check_type.scm 103 */
				obj_t BgL_method1294z00_1808;

				{	/* Ast/check_type.scm 103 */
					obj_t BgL_res2093z00_2545;

					{	/* Ast/check_type.scm 103 */
						long BgL_objzd2classzd2numz00_2516;

						BgL_objzd2classzd2numz00_2516 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_39));
						{	/* Ast/check_type.scm 103 */
							obj_t BgL_arg1811z00_2517;

							BgL_arg1811z00_2517 =
								PROCEDURE_REF
								(BGl_checkzd2nodezd2typezd2envzd2zzast_checkzd2typezd2,
								(int) (1L));
							{	/* Ast/check_type.scm 103 */
								int BgL_offsetz00_2520;

								BgL_offsetz00_2520 = (int) (BgL_objzd2classzd2numz00_2516);
								{	/* Ast/check_type.scm 103 */
									long BgL_offsetz00_2521;

									BgL_offsetz00_2521 =
										((long) (BgL_offsetz00_2520) - OBJECT_TYPE);
									{	/* Ast/check_type.scm 103 */
										long BgL_modz00_2522;

										BgL_modz00_2522 =
											(BgL_offsetz00_2521 >> (int) ((long) ((int) (4L))));
										{	/* Ast/check_type.scm 103 */
											long BgL_restz00_2524;

											BgL_restz00_2524 =
												(BgL_offsetz00_2521 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/check_type.scm 103 */

												{	/* Ast/check_type.scm 103 */
													obj_t BgL_bucketz00_2526;

													BgL_bucketz00_2526 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2517), BgL_modz00_2522);
													BgL_res2093z00_2545 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2526), BgL_restz00_2524);
					}}}}}}}}
					BgL_method1294z00_1808 = BgL_res2093z00_2545;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1294z00_1808, ((obj_t) BgL_nodez00_39));
			}
		}

	}



/* &check-node-type */
	obj_t BGl_z62checkzd2nodezd2typez62zzast_checkzd2typezd2(obj_t
		BgL_envz00_2837, obj_t BgL_nodez00_2838)
	{
		{	/* Ast/check_type.scm 103 */
			return
				BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
				((BgL_nodez00_bglt) BgL_nodez00_2838));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_checkzd2typezd2(void)
	{
		{	/* Ast/check_type.scm 16 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2typezd2envzd2zzast_checkzd2typezd2,
				BGl_varz00zzast_nodez00, BGl_proc2111z00zzast_checkzd2typezd2,
				BGl_string2112z00zzast_checkzd2typezd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2typezd2envzd2zzast_checkzd2typezd2,
				BGl_atomz00zzast_nodez00, BGl_proc2113z00zzast_checkzd2typezd2,
				BGl_string2112z00zzast_checkzd2typezd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2typezd2envzd2zzast_checkzd2typezd2,
				BGl_kwotez00zzast_nodez00, BGl_proc2114z00zzast_checkzd2typezd2,
				BGl_string2112z00zzast_checkzd2typezd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2typezd2envzd2zzast_checkzd2typezd2,
				BGl_closurez00zzast_nodez00, BGl_proc2115z00zzast_checkzd2typezd2,
				BGl_string2112z00zzast_checkzd2typezd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2typezd2envzd2zzast_checkzd2typezd2,
				BGl_sequencez00zzast_nodez00, BGl_proc2116z00zzast_checkzd2typezd2,
				BGl_string2112z00zzast_checkzd2typezd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2typezd2envzd2zzast_checkzd2typezd2,
				BGl_syncz00zzast_nodez00, BGl_proc2117z00zzast_checkzd2typezd2,
				BGl_string2112z00zzast_checkzd2typezd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2typezd2envzd2zzast_checkzd2typezd2,
				BGl_setqz00zzast_nodez00, BGl_proc2118z00zzast_checkzd2typezd2,
				BGl_string2112z00zzast_checkzd2typezd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2typezd2envzd2zzast_checkzd2typezd2,
				BGl_conditionalz00zzast_nodez00, BGl_proc2119z00zzast_checkzd2typezd2,
				BGl_string2112z00zzast_checkzd2typezd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2typezd2envzd2zzast_checkzd2typezd2,
				BGl_failz00zzast_nodez00, BGl_proc2120z00zzast_checkzd2typezd2,
				BGl_string2112z00zzast_checkzd2typezd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2typezd2envzd2zzast_checkzd2typezd2,
				BGl_switchz00zzast_nodez00, BGl_proc2121z00zzast_checkzd2typezd2,
				BGl_string2112z00zzast_checkzd2typezd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2typezd2envzd2zzast_checkzd2typezd2,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc2122z00zzast_checkzd2typezd2,
				BGl_string2112z00zzast_checkzd2typezd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2typezd2envzd2zzast_checkzd2typezd2,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc2123z00zzast_checkzd2typezd2,
				BGl_string2112z00zzast_checkzd2typezd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2typezd2envzd2zzast_checkzd2typezd2,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc2124z00zzast_checkzd2typezd2,
				BGl_string2112z00zzast_checkzd2typezd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2typezd2envzd2zzast_checkzd2typezd2,
				BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc2125z00zzast_checkzd2typezd2,
				BGl_string2112z00zzast_checkzd2typezd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2typezd2envzd2zzast_checkzd2typezd2,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc2126z00zzast_checkzd2typezd2,
				BGl_string2112z00zzast_checkzd2typezd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2typezd2envzd2zzast_checkzd2typezd2,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc2127z00zzast_checkzd2typezd2,
				BGl_string2112z00zzast_checkzd2typezd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2typezd2envzd2zzast_checkzd2typezd2,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc2128z00zzast_checkzd2typezd2,
				BGl_string2112z00zzast_checkzd2typezd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2typezd2envzd2zzast_checkzd2typezd2,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc2129z00zzast_checkzd2typezd2,
				BGl_string2112z00zzast_checkzd2typezd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2typezd2envzd2zzast_checkzd2typezd2,
				BGl_funcallz00zzast_nodez00, BGl_proc2130z00zzast_checkzd2typezd2,
				BGl_string2112z00zzast_checkzd2typezd2);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2nodezd2typezd2envzd2zzast_checkzd2typezd2,
				BGl_appz00zzast_nodez00, BGl_proc2131z00zzast_checkzd2typezd2,
				BGl_string2112z00zzast_checkzd2typezd2);
		}

	}



/* &check-node-type-app1334 */
	obj_t BGl_z62checkzd2nodezd2typezd2app1334zb0zzast_checkzd2typezd2(obj_t
		BgL_envz00_2859, obj_t BgL_nodez00_2860)
	{
		{	/* Ast/check_type.scm 311 */
			{	/* Ast/check_type.scm 313 */
				obj_t BgL_g1292z00_2914;

				BgL_g1292z00_2914 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_2860)))->BgL_argsz00);
				{
					obj_t BgL_l1290z00_2916;

					BgL_l1290z00_2916 = BgL_g1292z00_2914;
				BgL_zc3z04anonymousza31994ze3z87_2915:
					if (PAIRP(BgL_l1290z00_2916))
						{	/* Ast/check_type.scm 313 */
							{	/* Ast/check_type.scm 313 */
								obj_t BgL_arg1996z00_2917;

								BgL_arg1996z00_2917 = CAR(BgL_l1290z00_2916);
								BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
									((BgL_nodez00_bglt) BgL_arg1996z00_2917));
							}
							{
								obj_t BgL_l1290z00_3453;

								BgL_l1290z00_3453 = CDR(BgL_l1290z00_2916);
								BgL_l1290z00_2916 = BgL_l1290z00_3453;
								goto BgL_zc3z04anonymousza31994ze3z87_2915;
							}
						}
					else
						{	/* Ast/check_type.scm 313 */
							((bool_t) 1);
						}
				}
			}
			{	/* Ast/check_type.scm 314 */
				bool_t BgL_test2244z00_3455;

				{	/* Ast/check_type.scm 314 */
					BgL_typez00_bglt BgL_arg2008z00_2918;
					BgL_typez00_bglt BgL_arg2009z00_2919;

					BgL_arg2008z00_2918 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_appz00_bglt) BgL_nodez00_2860))))->BgL_typez00);
					BgL_arg2009z00_2919 =
						(((BgL_variablez00_bglt) COBJECT(
								(((BgL_varz00_bglt) COBJECT(
											(((BgL_appz00_bglt) COBJECT(
														((BgL_appz00_bglt) BgL_nodez00_2860)))->
												BgL_funz00)))->BgL_variablez00)))->BgL_typez00);
					BgL_test2244z00_3455 =
						BGl_subtypezf3zf3zzast_checkzd2typezd2(BgL_arg2008z00_2918,
						BgL_arg2009z00_2919);
				}
				if (BgL_test2244z00_3455)
					{	/* Ast/check_type.scm 314 */
						return BFALSE;
					}
				else
					{	/* Ast/check_type.scm 315 */
						BgL_typez00_bglt BgL_arg2003z00_2920;
						BgL_typez00_bglt BgL_arg2004z00_2921;

						BgL_arg2003z00_2920 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_appz00_bglt) BgL_nodez00_2860))))->BgL_typez00);
						BgL_arg2004z00_2921 =
							(((BgL_variablez00_bglt) COBJECT(
									(((BgL_varz00_bglt) COBJECT(
												(((BgL_appz00_bglt) COBJECT(
															((BgL_appz00_bglt) BgL_nodez00_2860)))->
													BgL_funz00)))->BgL_variablez00)))->BgL_typez00);
						return BGl_errz00zzast_checkzd2typezd2(((obj_t) ((BgL_appz00_bglt)
									BgL_nodez00_2860)), BgL_arg2003z00_2920,
							((obj_t) BgL_arg2004z00_2921));
					}
			}
		}

	}



/* &check-node-type-func1332 */
	obj_t BGl_z62checkzd2nodezd2typezd2func1332zb0zzast_checkzd2typezd2(obj_t
		BgL_envz00_2861, obj_t BgL_nodez00_2862)
	{
		{	/* Ast/check_type.scm 301 */
			{

				if (
					((((BgL_funcallz00_bglt) COBJECT(
									((BgL_funcallz00_bglt) BgL_nodez00_2862)))->
							BgL_strengthz00) == CNST_TABLE_REF(1)))
					{	/* Ast/check_type.scm 303 */
						BFALSE;
					}
				else
					{	/* Ast/check_type.scm 303 */
						BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
							(((BgL_funcallz00_bglt) COBJECT(
										((BgL_funcallz00_bglt) BgL_nodez00_2862)))->BgL_funz00));
					}
				{	/* Ast/check_type.scm 305 */
					obj_t BgL_g1289z00_2925;

					BgL_g1289z00_2925 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2862)))->BgL_argsz00);
					{
						obj_t BgL_l1287z00_2927;

						BgL_l1287z00_2927 = BgL_g1289z00_2925;
					BgL_zc3z04anonymousza31990ze3z87_2926:
						if (PAIRP(BgL_l1287z00_2927))
							{	/* Ast/check_type.scm 305 */
								{	/* Ast/check_type.scm 305 */
									obj_t BgL_arg1992z00_2928;

									BgL_arg1992z00_2928 = CAR(BgL_l1287z00_2927);
									BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
										((BgL_nodez00_bglt) BgL_arg1992z00_2928));
								}
								{
									obj_t BgL_l1287z00_3490;

									BgL_l1287z00_3490 = CDR(BgL_l1287z00_2927);
									BgL_l1287z00_2927 = BgL_l1287z00_3490;
									goto BgL_zc3z04anonymousza31990ze3z87_2926;
								}
							}
						else
							{	/* Ast/check_type.scm 305 */
								((bool_t) 1);
							}
					}
				}
				{	/* Ast/check_type.scm 301 */
					obj_t BgL_nextzd2method1331zd2_2924;

					BgL_nextzd2method1331zd2_2924 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_funcallz00_bglt) BgL_nodez00_2862)),
						BGl_checkzd2nodezd2typezd2envzd2zzast_checkzd2typezd2,
						BGl_funcallz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1331zd2_2924,
						((obj_t) ((BgL_funcallz00_bglt) BgL_nodez00_2862)));
				}
			}
		}

	}



/* &check-node-type-app-1330 */
	obj_t BGl_z62checkzd2nodezd2typezd2appzd21330z62zzast_checkzd2typezd2(obj_t
		BgL_envz00_2863, obj_t BgL_nodez00_2864)
	{
		{	/* Ast/check_type.scm 292 */
			{

				BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
					(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2864)))->BgL_funz00));
				BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
					(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2864)))->BgL_argz00));
				{	/* Ast/check_type.scm 292 */
					obj_t BgL_nextzd2method1329zd2_2931;

					BgL_nextzd2method1329zd2_2931 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_2864)),
						BGl_checkzd2nodezd2typezd2envzd2zzast_checkzd2typezd2,
						BGl_appzd2lyzd2zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1329zd2_2931,
						((obj_t) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_2864)));
				}
			}
		}

	}



/* &check-node-type-box-1328 */
	obj_t BGl_z62checkzd2nodezd2typezd2boxzd21328z62zzast_checkzd2typezd2(obj_t
		BgL_envz00_2865, obj_t BgL_nodez00_2866)
	{
		{	/* Ast/check_type.scm 282 */
			{	/* Ast/check_type.scm 284 */
				BgL_varz00_bglt BgL_arg1978z00_2933;

				BgL_arg1978z00_2933 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2866)))->BgL_varz00);
				BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
					((BgL_nodez00_bglt) BgL_arg1978z00_2933));
			}
			BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
				(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2866)))->BgL_valuez00));
			{	/* Ast/check_type.scm 286 */
				bool_t BgL_test2247z00_3523;

				{	/* Ast/check_type.scm 286 */
					BgL_typez00_bglt BgL_arg1983z00_2934;

					BgL_arg1983z00_2934 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2866))))->
						BgL_typez00);
					BgL_test2247z00_3523 =
						(((obj_t) BgL_arg1983z00_2934) ==
						BGl_za2unspecza2z00zztype_cachez00);
				}
				if (BgL_test2247z00_3523)
					{	/* Ast/check_type.scm 286 */
						return BFALSE;
					}
				else
					{	/* Ast/check_type.scm 287 */
						BgL_typez00_bglt BgL_arg1982z00_2935;

						BgL_arg1982z00_2935 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2866))))->
							BgL_typez00);
						return
							BGl_errz00zzast_checkzd2typezd2(((obj_t) (
									(BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2866)),
							BgL_arg1982z00_2935, BGl_za2unspecza2z00zztype_cachez00);
					}
			}
		}

	}



/* &check-node-type-box-1326 */
	obj_t BGl_z62checkzd2nodezd2typezd2boxzd21326z62zzast_checkzd2typezd2(obj_t
		BgL_envz00_2867, obj_t BgL_nodez00_2868)
	{
		{	/* Ast/check_type.scm 273 */
			{	/* Ast/check_type.scm 275 */
				BgL_varz00_bglt BgL_arg1968z00_2937;

				BgL_arg1968z00_2937 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_nodez00_2868)))->BgL_varz00);
				BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
					((BgL_nodez00_bglt) BgL_arg1968z00_2937));
			}
			{	/* Ast/check_type.scm 276 */
				bool_t BgL_test2248z00_3539;

				{	/* Ast/check_type.scm 276 */
					BgL_typez00_bglt BgL_arg1976z00_2938;

					BgL_arg1976z00_2938 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									(((BgL_boxzd2refzd2_bglt) COBJECT(
												((BgL_boxzd2refzd2_bglt) BgL_nodez00_2868)))->
										BgL_varz00))))->BgL_typez00);
					BgL_test2248z00_3539 =
						BGl_eqtypezf3zf3zzast_checkzd2typezd2
						(BGl_za2cellza2z00zztype_cachez00, ((obj_t) BgL_arg1976z00_2938));
				}
				if (BgL_test2248z00_3539)
					{	/* Ast/check_type.scm 276 */
						return BFALSE;
					}
				else
					{	/* Ast/check_type.scm 277 */
						BgL_typez00_bglt BgL_arg1972z00_2939;
						BgL_typez00_bglt BgL_arg1973z00_2940;

						BgL_arg1972z00_2939 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_boxzd2refzd2_bglt) BgL_nodez00_2868))))->BgL_typez00);
						BgL_arg1973z00_2940 =
							(((BgL_variablez00_bglt) COBJECT(
									(((BgL_varz00_bglt) COBJECT(
												(((BgL_boxzd2refzd2_bglt) COBJECT(
															((BgL_boxzd2refzd2_bglt) BgL_nodez00_2868)))->
													BgL_varz00)))->BgL_variablez00)))->BgL_typez00);
						return
							BGl_errz00zzast_checkzd2typezd2(((obj_t) ((BgL_boxzd2refzd2_bglt)
									BgL_nodez00_2868)), BgL_arg1972z00_2939,
							((obj_t) BgL_arg1973z00_2940));
					}
			}
		}

	}



/* &check-node-type-make1324 */
	obj_t BGl_z62checkzd2nodezd2typezd2make1324zb0zzast_checkzd2typezd2(obj_t
		BgL_envz00_2869, obj_t BgL_nodez00_2870)
	{
		{	/* Ast/check_type.scm 264 */
			BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
				(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_2870)))->BgL_valuez00));
			{	/* Ast/check_type.scm 267 */
				bool_t BgL_test2249z00_3560;

				{	/* Ast/check_type.scm 267 */
					BgL_typez00_bglt BgL_arg1967z00_2942;

					BgL_arg1967z00_2942 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_makezd2boxzd2_bglt) BgL_nodez00_2870))))->BgL_typez00);
					BgL_test2249z00_3560 =
						(((obj_t) BgL_arg1967z00_2942) == BGl_za2cellza2z00zztype_cachez00);
				}
				if (BgL_test2249z00_3560)
					{	/* Ast/check_type.scm 267 */
						return BFALSE;
					}
				else
					{	/* Ast/check_type.scm 268 */
						BgL_typez00_bglt BgL_arg1966z00_2943;

						BgL_arg1966z00_2943 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_makezd2boxzd2_bglt) BgL_nodez00_2870))))->
							BgL_typez00);
						return
							BGl_errz00zzast_checkzd2typezd2(((obj_t) ((BgL_makezd2boxzd2_bglt)
									BgL_nodez00_2870)), BgL_arg1966z00_2943,
							BGl_za2cellza2z00zztype_cachez00);
					}
			}
		}

	}



/* &check-node-type-jump1322 */
	obj_t BGl_z62checkzd2nodezd2typezd2jump1322zb0zzast_checkzd2typezd2(obj_t
		BgL_envz00_2871, obj_t BgL_nodez00_2872)
	{
		{	/* Ast/check_type.scm 254 */
			BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2872)))->BgL_exitz00));
			BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2872)))->BgL_valuez00));
			{	/* Ast/check_type.scm 258 */
				bool_t BgL_test2250z00_3578;

				{	/* Ast/check_type.scm 258 */
					BgL_typez00_bglt BgL_arg1962z00_2945;

					BgL_arg1962z00_2945 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2872))))->
						BgL_typez00);
					BgL_test2250z00_3578 =
						(((obj_t) BgL_arg1962z00_2945) ==
						BGl_za2unspecza2z00zztype_cachez00);
				}
				if (BgL_test2250z00_3578)
					{	/* Ast/check_type.scm 258 */
						return BFALSE;
					}
				else
					{	/* Ast/check_type.scm 259 */
						BgL_typez00_bglt BgL_arg1961z00_2946;

						BgL_arg1961z00_2946 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2872))))->
							BgL_typez00);
						return
							BGl_errz00zzast_checkzd2typezd2(((obj_t) (
									(BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2872)),
							BgL_arg1961z00_2946, BGl_za2unspecza2z00zztype_cachez00);
					}
			}
		}

	}



/* &check-node-type-set-1320 */
	obj_t BGl_z62checkzd2nodezd2typezd2setzd21320z62zzast_checkzd2typezd2(obj_t
		BgL_envz00_2873, obj_t BgL_nodez00_2874)
	{
		{	/* Ast/check_type.scm 243 */
			{	/* Ast/check_type.scm 245 */
				BgL_varz00_bglt BgL_arg1946z00_2948;

				BgL_arg1946z00_2948 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2874)))->BgL_varz00);
				BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
					((BgL_nodez00_bglt) BgL_arg1946z00_2948));
			}
			BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2874)))->BgL_bodyz00));
			BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2874)))->BgL_onexitz00));
			{	/* Ast/check_type.scm 248 */
				bool_t BgL_test2251z00_3600;

				{	/* Ast/check_type.scm 248 */
					BgL_typez00_bglt BgL_arg1955z00_2949;
					BgL_typez00_bglt BgL_arg1956z00_2950;

					BgL_arg1955z00_2949 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2874))))->
						BgL_typez00);
					BgL_arg1956z00_2950 =
						BGl_getzd2typezd2zztype_typeofz00(((BgL_nodez00_bglt) (
								(BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2874)), ((bool_t) 0));
					BgL_test2251z00_3600 =
						BGl_eqtypezf3zf3zzast_checkzd2typezd2(((obj_t) BgL_arg1955z00_2949),
						((obj_t) BgL_arg1956z00_2950));
				}
				if (BgL_test2251z00_3600)
					{	/* Ast/check_type.scm 248 */
						return BFALSE;
					}
				else
					{	/* Ast/check_type.scm 249 */
						BgL_typez00_bglt BgL_arg1952z00_2951;
						BgL_typez00_bglt BgL_arg1953z00_2952;

						BgL_arg1952z00_2951 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2874))))->
							BgL_typez00);
						BgL_arg1953z00_2952 =
							(((BgL_nodez00_bglt) COBJECT((((BgL_setzd2exzd2itz00_bglt)
											COBJECT(((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2874)))->
										BgL_bodyz00)))->BgL_typez00);
						return
							BGl_errz00zzast_checkzd2typezd2(((obj_t) (
									(BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2874)),
							BgL_arg1952z00_2951, ((obj_t) BgL_arg1953z00_2952));
					}
			}
		}

	}



/* &check-node-type-let-1318 */
	obj_t BGl_z62checkzd2nodezd2typezd2letzd21318z62zzast_checkzd2typezd2(obj_t
		BgL_envz00_2875, obj_t BgL_nodez00_2876)
	{
		{	/* Ast/check_type.scm 233 */
			{	/* Ast/check_type.scm 235 */
				obj_t BgL_g1286z00_2954;

				BgL_g1286z00_2954 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2876)))->BgL_bindingsz00);
				{
					obj_t BgL_l1284z00_2956;

					BgL_l1284z00_2956 = BgL_g1286z00_2954;
				BgL_zc3z04anonymousza31931ze3z87_2955:
					if (PAIRP(BgL_l1284z00_2956))
						{	/* Ast/check_type.scm 235 */
							{	/* Ast/check_type.scm 235 */
								obj_t BgL_bz00_2957;

								BgL_bz00_2957 = CAR(BgL_l1284z00_2956);
								{	/* Ast/check_type.scm 235 */
									obj_t BgL_arg1933z00_2958;

									BgL_arg1933z00_2958 = CDR(((obj_t) BgL_bz00_2957));
									BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
										((BgL_nodez00_bglt) BgL_arg1933z00_2958));
								}
							}
							{
								obj_t BgL_l1284z00_3629;

								BgL_l1284z00_3629 = CDR(BgL_l1284z00_2956);
								BgL_l1284z00_2956 = BgL_l1284z00_3629;
								goto BgL_zc3z04anonymousza31931ze3z87_2955;
							}
						}
					else
						{	/* Ast/check_type.scm 235 */
							((bool_t) 1);
						}
				}
			}
			BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
				(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2876)))->BgL_bodyz00));
			{	/* Ast/check_type.scm 237 */
				bool_t BgL_test2253z00_3634;

				{	/* Ast/check_type.scm 237 */
					BgL_typez00_bglt BgL_arg1943z00_2959;
					BgL_typez00_bglt BgL_arg1944z00_2960;

					BgL_arg1943z00_2959 =
						(((BgL_nodez00_bglt) COBJECT(
								(((BgL_letzd2varzd2_bglt) COBJECT(
											((BgL_letzd2varzd2_bglt) BgL_nodez00_2876)))->
									BgL_bodyz00)))->BgL_typez00);
					BgL_arg1944z00_2960 =
						(((BgL_nodez00_bglt)
							COBJECT(((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt)
										BgL_nodez00_2876))))->BgL_typez00);
					BgL_test2253z00_3634 =
						BGl_subtypezf3zf3zzast_checkzd2typezd2(BgL_arg1943z00_2959,
						BgL_arg1944z00_2960);
				}
				if (BgL_test2253z00_3634)
					{	/* Ast/check_type.scm 237 */
						return BFALSE;
					}
				else
					{	/* Ast/check_type.scm 238 */
						BgL_typez00_bglt BgL_arg1940z00_2961;
						BgL_typez00_bglt BgL_arg1941z00_2962;

						BgL_arg1940z00_2961 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_letzd2varzd2_bglt) BgL_nodez00_2876))))->BgL_typez00);
						BgL_arg1941z00_2962 =
							(((BgL_nodez00_bglt) COBJECT(
									(((BgL_letzd2varzd2_bglt) COBJECT(
												((BgL_letzd2varzd2_bglt) BgL_nodez00_2876)))->
										BgL_bodyz00)))->BgL_typez00);
						return
							BGl_errz00zzast_checkzd2typezd2(((obj_t) ((BgL_letzd2varzd2_bglt)
									BgL_nodez00_2876)), BgL_arg1940z00_2961,
							((obj_t) BgL_arg1941z00_2962));
					}
			}
		}

	}



/* &check-node-type-let-1316 */
	obj_t BGl_z62checkzd2nodezd2typezd2letzd21316z62zzast_checkzd2typezd2(obj_t
		BgL_envz00_2877, obj_t BgL_nodez00_2878)
	{
		{	/* Ast/check_type.scm 223 */
			{	/* Ast/check_type.scm 225 */
				obj_t BgL_g1283z00_2964;

				BgL_g1283z00_2964 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2878)))->BgL_localsz00);
				{
					obj_t BgL_l1281z00_2966;

					BgL_l1281z00_2966 = BgL_g1283z00_2964;
				BgL_zc3z04anonymousza31913ze3z87_2965:
					if (PAIRP(BgL_l1281z00_2966))
						{	/* Ast/check_type.scm 225 */
							{	/* Ast/check_type.scm 225 */
								obj_t BgL_arg1916z00_2967;

								BgL_arg1916z00_2967 = CAR(BgL_l1281z00_2966);
								{	/* Ast/check_type.scm 69 */
									BgL_valuez00_bglt BgL_funz00_2968;

									BgL_funz00_2968 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_arg1916z00_2967)))->
										BgL_valuez00);
									{	/* Ast/check_type.scm 69 */
										obj_t BgL_bodyz00_2969;

										BgL_bodyz00_2969 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_funz00_2968)))->BgL_bodyz00);
										{	/* Ast/check_type.scm 70 */

											BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
												((BgL_nodez00_bglt) BgL_bodyz00_2969));
										}
									}
								}
							}
							{
								obj_t BgL_l1281z00_3663;

								BgL_l1281z00_3663 = CDR(BgL_l1281z00_2966);
								BgL_l1281z00_2966 = BgL_l1281z00_3663;
								goto BgL_zc3z04anonymousza31913ze3z87_2965;
							}
						}
					else
						{	/* Ast/check_type.scm 225 */
							((bool_t) 1);
						}
				}
			}
			BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
				(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2878)))->BgL_bodyz00));
			{	/* Ast/check_type.scm 227 */
				bool_t BgL_test2255z00_3668;

				{	/* Ast/check_type.scm 227 */
					BgL_typez00_bglt BgL_arg1928z00_2970;
					BgL_typez00_bglt BgL_arg1929z00_2971;

					BgL_arg1928z00_2970 =
						(((BgL_nodez00_bglt) COBJECT(
								(((BgL_letzd2funzd2_bglt) COBJECT(
											((BgL_letzd2funzd2_bglt) BgL_nodez00_2878)))->
									BgL_bodyz00)))->BgL_typez00);
					BgL_arg1929z00_2971 =
						(((BgL_nodez00_bglt)
							COBJECT(((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt)
										BgL_nodez00_2878))))->BgL_typez00);
					BgL_test2255z00_3668 =
						BGl_subtypezf3zf3zzast_checkzd2typezd2(BgL_arg1928z00_2970,
						BgL_arg1929z00_2971);
				}
				if (BgL_test2255z00_3668)
					{	/* Ast/check_type.scm 227 */
						return BFALSE;
					}
				else
					{	/* Ast/check_type.scm 228 */
						BgL_typez00_bglt BgL_arg1925z00_2972;
						BgL_typez00_bglt BgL_arg1926z00_2973;

						BgL_arg1925z00_2972 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_letzd2funzd2_bglt) BgL_nodez00_2878))))->BgL_typez00);
						BgL_arg1926z00_2973 =
							(((BgL_nodez00_bglt) COBJECT(
									(((BgL_letzd2funzd2_bglt) COBJECT(
												((BgL_letzd2funzd2_bglt) BgL_nodez00_2878)))->
										BgL_bodyz00)))->BgL_typez00);
						return
							BGl_errz00zzast_checkzd2typezd2(((obj_t) ((BgL_letzd2funzd2_bglt)
									BgL_nodez00_2878)), BgL_arg1925z00_2972,
							((obj_t) BgL_arg1926z00_2973));
					}
			}
		}

	}



/* &check-node-type-swit1314 */
	obj_t BGl_z62checkzd2nodezd2typezd2swit1314zb0zzast_checkzd2typezd2(obj_t
		BgL_envz00_2879, obj_t BgL_nodez00_2880)
	{
		{	/* Ast/check_type.scm 207 */
			{

				BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
					(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2880)))->BgL_testz00));
				{
					obj_t BgL_clausesz00_2978;

					BgL_clausesz00_2978 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2880)))->BgL_clausesz00);
				BgL_loopz00_2977:
					if (NULLP(BgL_clausesz00_2978))
						{	/* Ast/check_type.scm 211 */
							{	/* Ast/check_type.scm 207 */
								obj_t BgL_nextzd2method1313zd2_2976;

								BgL_nextzd2method1313zd2_2976 =
									BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
									((BgL_objectz00_bglt)
										((BgL_switchz00_bglt) BgL_nodez00_2880)),
									BGl_checkzd2nodezd2typezd2envzd2zzast_checkzd2typezd2,
									BGl_switchz00zzast_nodez00);
								return BGL_PROCEDURE_CALL1(BgL_nextzd2method1313zd2_2976,
									((obj_t) ((BgL_switchz00_bglt) BgL_nodez00_2880)));
							}
						}
					else
						{	/* Ast/check_type.scm 213 */
							obj_t BgL_clausez00_2979;

							BgL_clausez00_2979 = CAR(((obj_t) BgL_clausesz00_2978));
							{	/* Ast/check_type.scm 214 */
								obj_t BgL_arg1901z00_2980;

								BgL_arg1901z00_2980 = CDR(((obj_t) BgL_clausez00_2979));
								BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
									((BgL_nodez00_bglt) BgL_arg1901z00_2980));
							}
							{	/* Ast/check_type.scm 215 */
								BgL_typez00_bglt BgL_ntypez00_2981;

								BgL_ntypez00_2981 =
									(((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt)
												CDR(((obj_t) BgL_clausez00_2979)))))->BgL_typez00);
								{	/* Ast/check_type.scm 216 */
									bool_t BgL_test2257z00_3710;

									{	/* Ast/check_type.scm 216 */
										BgL_typez00_bglt BgL_arg1911z00_2982;

										BgL_arg1911z00_2982 =
											(((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt)
														((BgL_switchz00_bglt) BgL_nodez00_2880))))->
											BgL_typez00);
										BgL_test2257z00_3710 =
											BGl_subtypezf3zf3zzast_checkzd2typezd2(BgL_ntypez00_2981,
											BgL_arg1911z00_2982);
									}
									if (BgL_test2257z00_3710)
										{	/* Ast/check_type.scm 217 */
											obj_t BgL_arg1904z00_2983;

											BgL_arg1904z00_2983 = CDR(((obj_t) BgL_clausesz00_2978));
											{
												obj_t BgL_clausesz00_3717;

												BgL_clausesz00_3717 = BgL_arg1904z00_2983;
												BgL_clausesz00_2978 = BgL_clausesz00_3717;
												goto BgL_loopz00_2977;
											}
										}
									else
										{	/* Ast/check_type.scm 218 */
											obj_t BgL_arg1906z00_2984;
											BgL_typez00_bglt BgL_arg1910z00_2985;

											BgL_arg1906z00_2984 = CDR(((obj_t) BgL_clausez00_2979));
											BgL_arg1910z00_2985 =
												(((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt)
															((BgL_switchz00_bglt) BgL_nodez00_2880))))->
												BgL_typez00);
											return
												BGl_errz00zzast_checkzd2typezd2(BgL_arg1906z00_2984,
												BgL_ntypez00_2981, ((obj_t) BgL_arg1910z00_2985));
										}
								}
							}
						}
				}
			}
		}

	}



/* &check-node-type-fail1312 */
	obj_t BGl_z62checkzd2nodezd2typezd2fail1312zb0zzast_checkzd2typezd2(obj_t
		BgL_envz00_2881, obj_t BgL_nodez00_2882)
	{
		{	/* Ast/check_type.scm 196 */
			BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2882)))->BgL_procz00));
			BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2882)))->BgL_msgz00));
			BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2882)))->BgL_objz00));
			{	/* Ast/check_type.scm 201 */
				bool_t BgL_test2258z00_3736;

				{	/* Ast/check_type.scm 201 */
					BgL_typez00_bglt BgL_arg1894z00_2987;

					BgL_arg1894z00_2987 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_failz00_bglt) BgL_nodez00_2882))))->BgL_typez00);
					BgL_test2258z00_3736 =
						(
						((obj_t) BgL_arg1894z00_2987) == BGl_za2magicza2z00zztype_cachez00);
				}
				if (BgL_test2258z00_3736)
					{	/* Ast/check_type.scm 201 */
						return BFALSE;
					}
				else
					{	/* Ast/check_type.scm 202 */
						BgL_typez00_bglt BgL_arg1893z00_2988;

						BgL_arg1893z00_2988 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_failz00_bglt) BgL_nodez00_2882))))->BgL_typez00);
						return
							BGl_errz00zzast_checkzd2typezd2(
							((obj_t)
								((BgL_failz00_bglt) BgL_nodez00_2882)), BgL_arg1893z00_2988,
							BGl_za2magicza2z00zztype_cachez00);
					}
			}
		}

	}



/* &check-node-type-cond1310 */
	obj_t BGl_z62checkzd2nodezd2typezd2cond1310zb0zzast_checkzd2typezd2(obj_t
		BgL_envz00_2883, obj_t BgL_nodez00_2884)
	{
		{	/* Ast/check_type.scm 186 */
			{

				BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
					(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2884)))->BgL_testz00));
				BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
					(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2884)))->BgL_truez00));
				BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
					(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2884)))->BgL_falsez00));
				{	/* Ast/check_type.scm 186 */
					obj_t BgL_nextzd2method1309zd2_2991;

					BgL_nextzd2method1309zd2_2991 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_conditionalz00_bglt) BgL_nodez00_2884)),
						BGl_checkzd2nodezd2typezd2envzd2zzast_checkzd2typezd2,
						BGl_conditionalz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1309zd2_2991,
						((obj_t) ((BgL_conditionalz00_bglt) BgL_nodez00_2884)));
				}
			}
		}

	}



/* &check-node-type-setq1308 */
	obj_t BGl_z62checkzd2nodezd2typezd2setq1308zb0zzast_checkzd2typezd2(obj_t
		BgL_envz00_2885, obj_t BgL_nodez00_2886)
	{
		{	/* Ast/check_type.scm 176 */
			BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
				(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_2886)))->BgL_valuez00));
			{	/* Ast/check_type.scm 179 */
				BgL_varz00_bglt BgL_arg1879z00_2993;

				BgL_arg1879z00_2993 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_2886)))->BgL_varz00);
				BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
					((BgL_nodez00_bglt) BgL_arg1879z00_2993));
			}
			{	/* Ast/check_type.scm 180 */
				bool_t BgL_test2259z00_3773;

				{	/* Ast/check_type.scm 180 */
					BgL_typez00_bglt BgL_arg1883z00_2994;

					BgL_arg1883z00_2994 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_setqz00_bglt) BgL_nodez00_2886))))->BgL_typez00);
					BgL_test2259z00_3773 =
						(
						((obj_t) BgL_arg1883z00_2994) ==
						BGl_za2unspecza2z00zztype_cachez00);
				}
				if (BgL_test2259z00_3773)
					{	/* Ast/check_type.scm 180 */
						return BFALSE;
					}
				else
					{	/* Ast/check_type.scm 181 */
						BgL_typez00_bglt BgL_arg1882z00_2995;

						BgL_arg1882z00_2995 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_setqz00_bglt) BgL_nodez00_2886))))->BgL_typez00);
						return
							BGl_errz00zzast_checkzd2typezd2(
							((obj_t)
								((BgL_setqz00_bglt) BgL_nodez00_2886)), BgL_arg1882z00_2995,
							BGl_za2unspecza2z00zztype_cachez00);
					}
			}
		}

	}



/* &check-node-type-sync1306 */
	obj_t BGl_z62checkzd2nodezd2typezd2sync1306zb0zzast_checkzd2typezd2(obj_t
		BgL_envz00_2887, obj_t BgL_nodez00_2888)
	{
		{	/* Ast/check_type.scm 167 */
			BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2888)))->BgL_mutexz00));
			BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2888)))->BgL_prelockz00));
			return
				BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2888)))->BgL_bodyz00));
		}

	}



/* &check-node-type-sequ1304 */
	obj_t BGl_z62checkzd2nodezd2typezd2sequ1304zb0zzast_checkzd2typezd2(obj_t
		BgL_envz00_2889, obj_t BgL_nodez00_2890)
	{
		{	/* Ast/check_type.scm 154 */
			if (NULLP(
					(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_2890)))->BgL_nodesz00)))
				{	/* Ast/check_type.scm 161 */
					bool_t BgL_test2261z00_3798;

					{	/* Ast/check_type.scm 161 */
						BgL_typez00_bglt BgL_arg1873z00_2998;

						BgL_arg1873z00_2998 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_sequencez00_bglt) BgL_nodez00_2890))))->BgL_typez00);
						BgL_test2261z00_3798 =
							BGl_eqtypezf3zf3zzast_checkzd2typezd2(
							((obj_t) BgL_arg1873z00_2998),
							BGl_za2unspecza2z00zztype_cachez00);
					}
					if (BgL_test2261z00_3798)
						{	/* Ast/check_type.scm 161 */
							return BFALSE;
						}
					else
						{	/* Ast/check_type.scm 162 */
							BgL_typez00_bglt BgL_arg1872z00_2999;

							BgL_arg1872z00_2999 =
								(((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_sequencez00_bglt) BgL_nodez00_2890))))->
								BgL_typez00);
							return
								BGl_errz00zzast_checkzd2typezd2(((obj_t) ((BgL_sequencez00_bglt)
										BgL_nodez00_2890)), BgL_arg1872z00_2999,
								BGl_za2unspecza2z00zztype_cachez00);
						}
				}
			else
				{	/* Ast/check_type.scm 156 */
					{	/* Ast/check_type.scm 158 */
						obj_t BgL_g1280z00_3000;

						BgL_g1280z00_3000 =
							(((BgL_sequencez00_bglt) COBJECT(
									((BgL_sequencez00_bglt) BgL_nodez00_2890)))->BgL_nodesz00);
						{
							obj_t BgL_l1278z00_3002;

							BgL_l1278z00_3002 = BgL_g1280z00_3000;
						BgL_zc3z04anonymousza31846ze3z87_3001:
							if (PAIRP(BgL_l1278z00_3002))
								{	/* Ast/check_type.scm 158 */
									{	/* Ast/check_type.scm 158 */
										obj_t BgL_arg1848z00_3003;

										BgL_arg1848z00_3003 = CAR(BgL_l1278z00_3002);
										BGl_checkzd2nodezd2typez00zzast_checkzd2typezd2(
											((BgL_nodez00_bglt) BgL_arg1848z00_3003));
									}
									{
										obj_t BgL_l1278z00_3817;

										BgL_l1278z00_3817 = CDR(BgL_l1278z00_3002);
										BgL_l1278z00_3002 = BgL_l1278z00_3817;
										goto BgL_zc3z04anonymousza31846ze3z87_3001;
									}
								}
							else
								{	/* Ast/check_type.scm 158 */
									((bool_t) 1);
								}
						}
					}
					{	/* Ast/check_type.scm 159 */
						bool_t BgL_test2263z00_3819;

						{	/* Ast/check_type.scm 159 */
							BgL_typez00_bglt BgL_arg1862z00_3004;
							BgL_typez00_bglt BgL_arg1863z00_3005;

							BgL_arg1862z00_3004 =
								(((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											CAR(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(
													(((BgL_sequencez00_bglt) COBJECT(
																((BgL_sequencez00_bglt) BgL_nodez00_2890)))->
														BgL_nodesz00))))))->BgL_typez00);
							BgL_arg1863z00_3005 =
								(((BgL_nodez00_bglt)
									COBJECT(((BgL_nodez00_bglt) ((BgL_sequencez00_bglt)
												BgL_nodez00_2890))))->BgL_typez00);
							BgL_test2263z00_3819 =
								BGl_subtypezf3zf3zzast_checkzd2typezd2(BgL_arg1862z00_3004,
								BgL_arg1863z00_3005);
						}
						if (BgL_test2263z00_3819)
							{	/* Ast/check_type.scm 159 */
								return BFALSE;
							}
						else
							{	/* Ast/check_type.scm 160 */
								BgL_typez00_bglt BgL_arg1856z00_3006;
								BgL_typez00_bglt BgL_arg1857z00_3007;

								BgL_arg1856z00_3006 =
									(((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt)
												CAR(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(
														(((BgL_sequencez00_bglt) COBJECT(
																	((BgL_sequencez00_bglt) BgL_nodez00_2890)))->
															BgL_nodesz00))))))->BgL_typez00);
								BgL_arg1857z00_3007 =
									(((BgL_nodez00_bglt)
										COBJECT(((BgL_nodez00_bglt) ((BgL_sequencez00_bglt)
													BgL_nodez00_2890))))->BgL_typez00);
								return
									BGl_errz00zzast_checkzd2typezd2(((obj_t) (
											(BgL_sequencez00_bglt) BgL_nodez00_2890)),
									BgL_arg1856z00_3006, ((obj_t) BgL_arg1857z00_3007));
							}
					}
				}
		}

	}



/* &check-node-type-clos1302 */
	obj_t BGl_z62checkzd2nodezd2typezd2clos1302zb0zzast_checkzd2typezd2(obj_t
		BgL_envz00_2891, obj_t BgL_nodez00_2892)
	{
		{	/* Ast/check_type.scm 146 */
			{	/* Ast/check_type.scm 148 */
				bool_t BgL_test2264z00_3843;

				{	/* Ast/check_type.scm 148 */
					BgL_typez00_bglt BgL_arg1843z00_3009;

					BgL_arg1843z00_3009 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_closurez00_bglt) BgL_nodez00_2892))))->BgL_typez00);
					BgL_test2264z00_3843 =
						(
						((obj_t) BgL_arg1843z00_3009) ==
						BGl_za2procedureza2z00zztype_cachez00);
				}
				if (BgL_test2264z00_3843)
					{	/* Ast/check_type.scm 148 */
						return BFALSE;
					}
				else
					{	/* Ast/check_type.scm 149 */
						BgL_typez00_bglt BgL_arg1842z00_3010;

						BgL_arg1842z00_3010 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_closurez00_bglt) BgL_nodez00_2892))))->BgL_typez00);
						return
							BGl_errz00zzast_checkzd2typezd2(
							((obj_t)
								((BgL_closurez00_bglt) BgL_nodez00_2892)), BgL_arg1842z00_3010,
							BGl_za2procedureza2z00zztype_cachez00);
					}
			}
		}

	}



/* &check-node-type-kwot1300 */
	obj_t BGl_z62checkzd2nodezd2typezd2kwot1300zb0zzast_checkzd2typezd2(obj_t
		BgL_envz00_2893, obj_t BgL_nodez00_2894)
	{
		{	/* Ast/check_type.scm 138 */
			{	/* Ast/check_type.scm 140 */
				bool_t BgL_test2265z00_3855;

				{	/* Ast/check_type.scm 140 */
					BgL_typez00_bglt BgL_arg1837z00_3012;
					BgL_typez00_bglt BgL_arg1838z00_3013;

					BgL_arg1837z00_3012 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_kwotez00_bglt) BgL_nodez00_2894))))->BgL_typez00);
					BgL_arg1838z00_3013 =
						BGl_getzd2typezd2kwotez00zztype_typeofz00(
						(((BgL_kwotez00_bglt) COBJECT(
									((BgL_kwotez00_bglt) BgL_nodez00_2894)))->BgL_valuez00));
					BgL_test2265z00_3855 =
						BGl_eqtypezf3zf3zzast_checkzd2typezd2(
						((obj_t) BgL_arg1837z00_3012), ((obj_t) BgL_arg1838z00_3013));
				}
				if (BgL_test2265z00_3855)
					{	/* Ast/check_type.scm 140 */
						return BFALSE;
					}
				else
					{	/* Ast/check_type.scm 141 */
						BgL_typez00_bglt BgL_arg1834z00_3014;
						BgL_typez00_bglt BgL_arg1835z00_3015;

						BgL_arg1834z00_3014 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_kwotez00_bglt) BgL_nodez00_2894))))->BgL_typez00);
						BgL_arg1835z00_3015 =
							BGl_getzd2typezd2kwotez00zztype_typeofz00(
							(((BgL_kwotez00_bglt) COBJECT(
										((BgL_kwotez00_bglt) BgL_nodez00_2894)))->BgL_valuez00));
						return
							BGl_errz00zzast_checkzd2typezd2(
							((obj_t)
								((BgL_kwotez00_bglt) BgL_nodez00_2894)), BgL_arg1834z00_3014,
							((obj_t) BgL_arg1835z00_3015));
					}
			}
		}

	}



/* &check-node-type-atom1298 */
	obj_t BGl_z62checkzd2nodezd2typezd2atom1298zb0zzast_checkzd2typezd2(obj_t
		BgL_envz00_2895, obj_t BgL_nodez00_2896)
	{
		{	/* Ast/check_type.scm 130 */
			{	/* Ast/check_type.scm 132 */
				bool_t BgL_test2266z00_3875;

				{	/* Ast/check_type.scm 132 */
					BgL_typez00_bglt BgL_arg1812z00_3017;
					BgL_typez00_bglt BgL_arg1820z00_3018;

					BgL_arg1812z00_3017 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_atomz00_bglt) BgL_nodez00_2896))))->BgL_typez00);
					BgL_arg1820z00_3018 =
						BGl_getzd2typezd2atomz00zztype_typeofz00(
						(((BgL_atomz00_bglt) COBJECT(
									((BgL_atomz00_bglt) BgL_nodez00_2896)))->BgL_valuez00));
					BgL_test2266z00_3875 =
						BGl_atomzd2subtypezf3z21zzast_checkzd2typezd2(BgL_arg1812z00_3017,
						BgL_arg1820z00_3018);
				}
				if (BgL_test2266z00_3875)
					{	/* Ast/check_type.scm 132 */
						return BFALSE;
					}
				else
					{	/* Ast/check_type.scm 133 */
						BgL_typez00_bglt BgL_arg1805z00_3019;
						BgL_typez00_bglt BgL_arg1806z00_3020;

						BgL_arg1805z00_3019 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_atomz00_bglt) BgL_nodez00_2896))))->BgL_typez00);
						BgL_arg1806z00_3020 =
							BGl_getzd2typezd2atomz00zztype_typeofz00(
							(((BgL_atomz00_bglt) COBJECT(
										((BgL_atomz00_bglt) BgL_nodez00_2896)))->BgL_valuez00));
						return
							BGl_errz00zzast_checkzd2typezd2(
							((obj_t)
								((BgL_atomz00_bglt) BgL_nodez00_2896)), BgL_arg1805z00_3019,
							((obj_t) BgL_arg1806z00_3020));
					}
			}
		}

	}



/* &check-node-type-var1296 */
	obj_t BGl_z62checkzd2nodezd2typezd2var1296zb0zzast_checkzd2typezd2(obj_t
		BgL_envz00_2897, obj_t BgL_nodez00_2898)
	{
		{	/* Ast/check_type.scm 110 */
			{	/* Ast/check_type.scm 112 */
				BgL_variablez00_bglt BgL_i1114z00_3022;

				BgL_i1114z00_3022 =
					(((BgL_varz00_bglt) COBJECT(
							((BgL_varz00_bglt) BgL_nodez00_2898)))->BgL_variablez00);
				{	/* Ast/check_type.scm 113 */
					bool_t BgL_test2267z00_3895;

					{	/* Ast/check_type.scm 113 */
						BgL_valuez00_bglt BgL_arg1770z00_3023;

						BgL_arg1770z00_3023 =
							(((BgL_variablez00_bglt) COBJECT(
									(((BgL_varz00_bglt) COBJECT(
												((BgL_varz00_bglt) BgL_nodez00_2898)))->
										BgL_variablez00)))->BgL_valuez00);
						{	/* Ast/check_type.scm 113 */
							obj_t BgL_classz00_3024;

							BgL_classz00_3024 = BGl_sfunz00zzast_varz00;
							{	/* Ast/check_type.scm 113 */
								BgL_objectz00_bglt BgL_arg1807z00_3025;

								{	/* Ast/check_type.scm 113 */
									obj_t BgL_tmpz00_3899;

									BgL_tmpz00_3899 =
										((obj_t) ((BgL_objectz00_bglt) BgL_arg1770z00_3023));
									BgL_arg1807z00_3025 = (BgL_objectz00_bglt) (BgL_tmpz00_3899);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Ast/check_type.scm 113 */
										long BgL_idxz00_3026;

										BgL_idxz00_3026 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3025);
										BgL_test2267z00_3895 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_3026 + 3L)) == BgL_classz00_3024);
									}
								else
									{	/* Ast/check_type.scm 113 */
										bool_t BgL_res2094z00_3029;

										{	/* Ast/check_type.scm 113 */
											obj_t BgL_oclassz00_3030;

											{	/* Ast/check_type.scm 113 */
												obj_t BgL_arg1815z00_3031;
												long BgL_arg1816z00_3032;

												BgL_arg1815z00_3031 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Ast/check_type.scm 113 */
													long BgL_arg1817z00_3033;

													BgL_arg1817z00_3033 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3025);
													BgL_arg1816z00_3032 =
														(BgL_arg1817z00_3033 - OBJECT_TYPE);
												}
												BgL_oclassz00_3030 =
													VECTOR_REF(BgL_arg1815z00_3031, BgL_arg1816z00_3032);
											}
											{	/* Ast/check_type.scm 113 */
												bool_t BgL__ortest_1115z00_3034;

												BgL__ortest_1115z00_3034 =
													(BgL_classz00_3024 == BgL_oclassz00_3030);
												if (BgL__ortest_1115z00_3034)
													{	/* Ast/check_type.scm 113 */
														BgL_res2094z00_3029 = BgL__ortest_1115z00_3034;
													}
												else
													{	/* Ast/check_type.scm 113 */
														long BgL_odepthz00_3035;

														{	/* Ast/check_type.scm 113 */
															obj_t BgL_arg1804z00_3036;

															BgL_arg1804z00_3036 = (BgL_oclassz00_3030);
															BgL_odepthz00_3035 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_3036);
														}
														if ((3L < BgL_odepthz00_3035))
															{	/* Ast/check_type.scm 113 */
																obj_t BgL_arg1802z00_3037;

																{	/* Ast/check_type.scm 113 */
																	obj_t BgL_arg1803z00_3038;

																	BgL_arg1803z00_3038 = (BgL_oclassz00_3030);
																	BgL_arg1802z00_3037 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3038,
																		3L);
																}
																BgL_res2094z00_3029 =
																	(BgL_arg1802z00_3037 == BgL_classz00_3024);
															}
														else
															{	/* Ast/check_type.scm 113 */
																BgL_res2094z00_3029 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2267z00_3895 = BgL_res2094z00_3029;
									}
							}
						}
					}
					if (BgL_test2267z00_3895)
						{	/* Ast/check_type.scm 113 */
							return BFALSE;
						}
					else
						{	/* Ast/check_type.scm 113 */
							{	/* Ast/check_type.scm 114 */
								bool_t BgL_test2271z00_3922;

								{	/* Ast/check_type.scm 114 */
									bool_t BgL_test2272z00_3923;

									{	/* Ast/check_type.scm 114 */
										BgL_typez00_bglt BgL_arg1738z00_3039;
										BgL_typez00_bglt BgL_arg1739z00_3040;

										BgL_arg1738z00_3039 =
											(((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt)
														((BgL_varz00_bglt) BgL_nodez00_2898))))->
											BgL_typez00);
										BgL_arg1739z00_3040 =
											(((BgL_variablez00_bglt) COBJECT(BgL_i1114z00_3022))->
											BgL_typez00);
										BgL_test2272z00_3923 =
											BGl_subtypezf3zf3zzast_checkzd2typezd2
											(BgL_arg1738z00_3039, BgL_arg1739z00_3040);
									}
									if (BgL_test2272z00_3923)
										{	/* Ast/check_type.scm 114 */
											BgL_test2271z00_3922 = ((bool_t) 1);
										}
									else
										{	/* Ast/check_type.scm 115 */
											bool_t BgL_test2273z00_3929;

											{	/* Ast/check_type.scm 115 */
												bool_t BgL_test2274z00_3930;

												{	/* Ast/check_type.scm 115 */
													BgL_typez00_bglt BgL_arg1737z00_3041;

													BgL_arg1737z00_3041 =
														(((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt)
																	((BgL_varz00_bglt) BgL_nodez00_2898))))->
														BgL_typez00);
													BgL_test2274z00_3930 =
														(((obj_t) BgL_arg1737z00_3041) ==
														BGl_za2objza2z00zztype_cachez00);
												}
												if (BgL_test2274z00_3930)
													{	/* Ast/check_type.scm 115 */
														BgL_test2273z00_3929 =
															BGl_bigloozd2typezf3z21zztype_typez00(
															(((BgL_variablez00_bglt)
																	COBJECT(BgL_i1114z00_3022))->BgL_typez00));
													}
												else
													{	/* Ast/check_type.scm 115 */
														BgL_test2273z00_3929 = ((bool_t) 0);
													}
											}
											if (BgL_test2273z00_3929)
												{	/* Ast/check_type.scm 115 */
													BgL_test2271z00_3922 = ((bool_t) 1);
												}
											else
												{	/* Ast/check_type.scm 116 */
													bool_t BgL_test2275z00_3938;

													{	/* Ast/check_type.scm 116 */
														BgL_typez00_bglt BgL_arg1735z00_3042;

														BgL_arg1735z00_3042 =
															(((BgL_variablez00_bglt)
																COBJECT(BgL_i1114z00_3022))->BgL_typez00);
														{	/* Ast/check_type.scm 116 */
															obj_t BgL_classz00_3043;

															BgL_classz00_3043 =
																BGl_tclassz00zzobject_classz00;
															{	/* Ast/check_type.scm 116 */
																BgL_objectz00_bglt BgL_arg1807z00_3044;

																{	/* Ast/check_type.scm 116 */
																	obj_t BgL_tmpz00_3940;

																	BgL_tmpz00_3940 =
																		((obj_t)
																		((BgL_objectz00_bglt) BgL_arg1735z00_3042));
																	BgL_arg1807z00_3044 =
																		(BgL_objectz00_bglt) (BgL_tmpz00_3940);
																}
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Ast/check_type.scm 116 */
																		long BgL_idxz00_3045;

																		BgL_idxz00_3045 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_3044);
																		BgL_test2275z00_3938 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_3045 + 2L)) ==
																			BgL_classz00_3043);
																	}
																else
																	{	/* Ast/check_type.scm 116 */
																		bool_t BgL_res2095z00_3048;

																		{	/* Ast/check_type.scm 116 */
																			obj_t BgL_oclassz00_3049;

																			{	/* Ast/check_type.scm 116 */
																				obj_t BgL_arg1815z00_3050;
																				long BgL_arg1816z00_3051;

																				BgL_arg1815z00_3050 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Ast/check_type.scm 116 */
																					long BgL_arg1817z00_3052;

																					BgL_arg1817z00_3052 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_3044);
																					BgL_arg1816z00_3051 =
																						(BgL_arg1817z00_3052 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_3049 =
																					VECTOR_REF(BgL_arg1815z00_3050,
																					BgL_arg1816z00_3051);
																			}
																			{	/* Ast/check_type.scm 116 */
																				bool_t BgL__ortest_1115z00_3053;

																				BgL__ortest_1115z00_3053 =
																					(BgL_classz00_3043 ==
																					BgL_oclassz00_3049);
																				if (BgL__ortest_1115z00_3053)
																					{	/* Ast/check_type.scm 116 */
																						BgL_res2095z00_3048 =
																							BgL__ortest_1115z00_3053;
																					}
																				else
																					{	/* Ast/check_type.scm 116 */
																						long BgL_odepthz00_3054;

																						{	/* Ast/check_type.scm 116 */
																							obj_t BgL_arg1804z00_3055;

																							BgL_arg1804z00_3055 =
																								(BgL_oclassz00_3049);
																							BgL_odepthz00_3054 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_3055);
																						}
																						if ((2L < BgL_odepthz00_3054))
																							{	/* Ast/check_type.scm 116 */
																								obj_t BgL_arg1802z00_3056;

																								{	/* Ast/check_type.scm 116 */
																									obj_t BgL_arg1803z00_3057;

																									BgL_arg1803z00_3057 =
																										(BgL_oclassz00_3049);
																									BgL_arg1802z00_3056 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_3057, 2L);
																								}
																								BgL_res2095z00_3048 =
																									(BgL_arg1802z00_3056 ==
																									BgL_classz00_3043);
																							}
																						else
																							{	/* Ast/check_type.scm 116 */
																								BgL_res2095z00_3048 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2275z00_3938 = BgL_res2095z00_3048;
																	}
															}
														}
													}
													if (BgL_test2275z00_3938)
														{	/* Ast/check_type.scm 116 */
															BgL_typez00_bglt BgL_arg1733z00_3058;
															BgL_typez00_bglt BgL_arg1734z00_3059;

															BgL_arg1733z00_3058 =
																(((BgL_variablez00_bglt)
																	COBJECT(BgL_i1114z00_3022))->BgL_typez00);
															BgL_arg1734z00_3059 =
																(((BgL_nodez00_bglt)
																	COBJECT(((BgL_nodez00_bglt) ((BgL_varz00_bglt)
																				BgL_nodez00_2898))))->BgL_typez00);
															BgL_test2271z00_3922 =
																BGl_subtypezf3zf3zzast_checkzd2typezd2
																(BgL_arg1733z00_3058, BgL_arg1734z00_3059);
														}
													else
														{	/* Ast/check_type.scm 116 */
															BgL_test2271z00_3922 = ((bool_t) 0);
														}
												}
										}
								}
								if (BgL_test2271z00_3922)
									{	/* Ast/check_type.scm 114 */
										BFALSE;
									}
								else
									{	/* Ast/check_type.scm 114 */
										{	/* Ast/check_type.scm 117 */
											obj_t BgL_arg1629z00_3060;
											obj_t BgL_arg1630z00_3061;
											obj_t BgL_arg1642z00_3062;
											obj_t BgL_arg1646z00_3063;
											obj_t BgL_arg1650z00_3064;
											bool_t BgL_arg1651z00_3065;
											bool_t BgL_arg1654z00_3066;

											{	/* Ast/check_type.scm 117 */
												obj_t BgL_tmpz00_3968;

												BgL_tmpz00_3968 = BGL_CURRENT_DYNAMIC_ENV();
												BgL_arg1629z00_3060 =
													BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_3968);
											}
											BgL_arg1630z00_3061 =
												BGl_shapez00zztools_shapez00(
												((obj_t) ((BgL_varz00_bglt) BgL_nodez00_2898)));
											BgL_arg1642z00_3062 =
												(((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt)
															((BgL_varz00_bglt) BgL_nodez00_2898))))->
												BgL_locz00);
											{	/* Ast/check_type.scm 118 */
												BgL_typez00_bglt BgL_arg1710z00_3067;

												BgL_arg1710z00_3067 =
													(((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt)
																((BgL_varz00_bglt) BgL_nodez00_2898))))->
													BgL_typez00);
												BgL_arg1646z00_3063 =
													BGl_shapez00zztools_shapez00(((obj_t)
														BgL_arg1710z00_3067));
											}
											{	/* Ast/check_type.scm 118 */
												BgL_typez00_bglt BgL_arg1711z00_3068;

												BgL_arg1711z00_3068 =
													(((BgL_variablez00_bglt) COBJECT(BgL_i1114z00_3022))->
													BgL_typez00);
												BgL_arg1650z00_3064 =
													BGl_shapez00zztools_shapez00(((obj_t)
														BgL_arg1711z00_3068));
											}
											BgL_arg1651z00_3065 =
												(
												((obj_t)
													(((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt)
																	((BgL_varz00_bglt) BgL_nodez00_2898))))->
														BgL_typez00)) ==
												((obj_t) (((BgL_variablez00_bglt)
															COBJECT(BgL_i1114z00_3022))->BgL_typez00)));
											{	/* Ast/check_type.scm 119 */
												BgL_typez00_bglt BgL_arg1718z00_3069;
												BgL_typez00_bglt BgL_arg1720z00_3070;

												BgL_arg1718z00_3069 =
													(((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt)
																((BgL_varz00_bglt) BgL_nodez00_2898))))->
													BgL_typez00);
												BgL_arg1720z00_3070 =
													(((BgL_variablez00_bglt) COBJECT(BgL_i1114z00_3022))->
													BgL_typez00);
												BgL_arg1654z00_3066 =
													BGl_subtypezf3zf3zzast_checkzd2typezd2
													(BgL_arg1718z00_3069, BgL_arg1720z00_3070);
											}
											{	/* Ast/check_type.scm 117 */
												obj_t BgL_list1655z00_3071;

												{	/* Ast/check_type.scm 117 */
													obj_t BgL_arg1661z00_3072;

													{	/* Ast/check_type.scm 117 */
														obj_t BgL_arg1663z00_3073;

														{	/* Ast/check_type.scm 117 */
															obj_t BgL_arg1675z00_3074;

															{	/* Ast/check_type.scm 117 */
																obj_t BgL_arg1678z00_3075;

																{	/* Ast/check_type.scm 117 */
																	obj_t BgL_arg1681z00_3076;

																	{	/* Ast/check_type.scm 117 */
																		obj_t BgL_arg1688z00_3077;

																		{	/* Ast/check_type.scm 117 */
																			obj_t BgL_arg1689z00_3078;

																			{	/* Ast/check_type.scm 117 */
																				obj_t BgL_arg1691z00_3079;

																				{	/* Ast/check_type.scm 117 */
																					obj_t BgL_arg1692z00_3080;

																					{	/* Ast/check_type.scm 117 */
																						obj_t BgL_arg1699z00_3081;

																						{	/* Ast/check_type.scm 117 */
																							obj_t BgL_arg1700z00_3082;

																							{	/* Ast/check_type.scm 117 */
																								obj_t BgL_arg1701z00_3083;

																								{	/* Ast/check_type.scm 117 */
																									obj_t BgL_arg1702z00_3084;

																									{	/* Ast/check_type.scm 117 */
																										obj_t BgL_arg1703z00_3085;

																										{	/* Ast/check_type.scm 117 */
																											obj_t BgL_arg1705z00_3086;

																											{	/* Ast/check_type.scm 117 */
																												obj_t
																													BgL_arg1708z00_3087;
																												{	/* Ast/check_type.scm 117 */
																													obj_t
																														BgL_arg1709z00_3088;
																													BgL_arg1709z00_3088 =
																														MAKE_YOUNG_PAIR
																														(BBOOL
																														(BGl_za2checkzd2fullza2zd2zzast_checkzd2typezd2),
																														BNIL);
																													BgL_arg1708z00_3087 =
																														MAKE_YOUNG_PAIR
																														(BGl_string2132z00zzast_checkzd2typezd2,
																														BgL_arg1709z00_3088);
																												}
																												BgL_arg1705z00_3086 =
																													MAKE_YOUNG_PAIR(BBOOL
																													(BgL_arg1654z00_3066),
																													BgL_arg1708z00_3087);
																											}
																											BgL_arg1703z00_3085 =
																												MAKE_YOUNG_PAIR
																												(BGl_string2133z00zzast_checkzd2typezd2,
																												BgL_arg1705z00_3086);
																										}
																										BgL_arg1702z00_3084 =
																											MAKE_YOUNG_PAIR(BBOOL
																											(BgL_arg1651z00_3065),
																											BgL_arg1703z00_3085);
																									}
																									BgL_arg1701z00_3083 =
																										MAKE_YOUNG_PAIR
																										(BGl_string2134z00zzast_checkzd2typezd2,
																										BgL_arg1702z00_3084);
																								}
																								BgL_arg1700z00_3082 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1650z00_3064,
																									BgL_arg1701z00_3083);
																							}
																							BgL_arg1699z00_3081 =
																								MAKE_YOUNG_PAIR
																								(BGl_string2135z00zzast_checkzd2typezd2,
																								BgL_arg1700z00_3082);
																						}
																						BgL_arg1692z00_3080 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1646z00_3063,
																							BgL_arg1699z00_3081);
																					}
																					BgL_arg1691z00_3079 =
																						MAKE_YOUNG_PAIR
																						(BGl_string2136z00zzast_checkzd2typezd2,
																						BgL_arg1692z00_3080);
																				}
																				BgL_arg1689z00_3078 =
																					MAKE_YOUNG_PAIR(BgL_arg1642z00_3062,
																					BgL_arg1691z00_3079);
																			}
																			BgL_arg1688z00_3077 =
																				MAKE_YOUNG_PAIR
																				(BGl_string2137z00zzast_checkzd2typezd2,
																				BgL_arg1689z00_3078);
																		}
																		BgL_arg1681z00_3076 =
																			MAKE_YOUNG_PAIR(BgL_arg1630z00_3061,
																			BgL_arg1688z00_3077);
																	}
																	BgL_arg1678z00_3075 =
																		MAKE_YOUNG_PAIR
																		(BGl_string2138z00zzast_checkzd2typezd2,
																		BgL_arg1681z00_3076);
																}
																BgL_arg1675z00_3074 =
																	MAKE_YOUNG_PAIR
																	(BGl_string2139z00zzast_checkzd2typezd2,
																	BgL_arg1678z00_3075);
															}
															BgL_arg1663z00_3073 =
																MAKE_YOUNG_PAIR(BINT(117L),
																BgL_arg1675z00_3074);
														}
														BgL_arg1661z00_3072 =
															MAKE_YOUNG_PAIR
															(BGl_string2140z00zzast_checkzd2typezd2,
															BgL_arg1663z00_3073);
													}
													BgL_list1655z00_3071 =
														MAKE_YOUNG_PAIR
														(BGl_string2141z00zzast_checkzd2typezd2,
														BgL_arg1661z00_3072);
												}
												BGl_tprintz00zz__r4_output_6_10_3z00
													(BgL_arg1629z00_3060, BgL_list1655z00_3071);
											}
										}
										{	/* Ast/check_type.scm 121 */
											BgL_typez00_bglt BgL_arg1722z00_3089;
											BgL_typez00_bglt BgL_arg1724z00_3090;

											BgL_arg1722z00_3089 =
												(((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt)
															((BgL_varz00_bglt) BgL_nodez00_2898))))->
												BgL_typez00);
											BgL_arg1724z00_3090 =
												(((BgL_variablez00_bglt) COBJECT(BgL_i1114z00_3022))->
												BgL_typez00);
											BGl_errz00zzast_checkzd2typezd2(((obj_t) (
														(BgL_varz00_bglt) BgL_nodez00_2898)),
												BgL_arg1722z00_3089, ((obj_t) BgL_arg1724z00_3090));
										}
									}
							}
							{	/* Ast/check_type.scm 122 */
								bool_t BgL_test2279z00_4028;

								{	/* Ast/check_type.scm 122 */
									bool_t BgL_test2280z00_4029;

									{	/* Ast/check_type.scm 122 */
										BgL_typez00_bglt BgL_arg1767z00_3091;

										BgL_arg1767z00_3091 =
											(((BgL_variablez00_bglt) COBJECT(BgL_i1114z00_3022))->
											BgL_typez00);
										BgL_test2280z00_4029 =
											(((obj_t) BgL_arg1767z00_3091) ==
											BGl_za2_za2z00zztype_cachez00);
									}
									if (BgL_test2280z00_4029)
										{	/* Ast/check_type.scm 123 */
											bool_t BgL_test2281z00_4033;

											{	/* Ast/check_type.scm 123 */
												BgL_variablez00_bglt BgL_arg1765z00_3092;

												BgL_arg1765z00_3092 =
													(((BgL_varz00_bglt) COBJECT(
															((BgL_varz00_bglt) BgL_nodez00_2898)))->
													BgL_variablez00);
												{	/* Ast/check_type.scm 123 */
													obj_t BgL_classz00_3093;

													BgL_classz00_3093 = BGl_globalz00zzast_varz00;
													{	/* Ast/check_type.scm 123 */
														BgL_objectz00_bglt BgL_arg1807z00_3094;

														{	/* Ast/check_type.scm 123 */
															obj_t BgL_tmpz00_4036;

															BgL_tmpz00_4036 =
																((obj_t)
																((BgL_objectz00_bglt) BgL_arg1765z00_3092));
															BgL_arg1807z00_3094 =
																(BgL_objectz00_bglt) (BgL_tmpz00_4036);
														}
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Ast/check_type.scm 123 */
																long BgL_idxz00_3095;

																BgL_idxz00_3095 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_3094);
																BgL_test2281z00_4033 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_3095 + 2L)) ==
																	BgL_classz00_3093);
															}
														else
															{	/* Ast/check_type.scm 123 */
																bool_t BgL_res2096z00_3098;

																{	/* Ast/check_type.scm 123 */
																	obj_t BgL_oclassz00_3099;

																	{	/* Ast/check_type.scm 123 */
																		obj_t BgL_arg1815z00_3100;
																		long BgL_arg1816z00_3101;

																		BgL_arg1815z00_3100 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Ast/check_type.scm 123 */
																			long BgL_arg1817z00_3102;

																			BgL_arg1817z00_3102 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_3094);
																			BgL_arg1816z00_3101 =
																				(BgL_arg1817z00_3102 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_3099 =
																			VECTOR_REF(BgL_arg1815z00_3100,
																			BgL_arg1816z00_3101);
																	}
																	{	/* Ast/check_type.scm 123 */
																		bool_t BgL__ortest_1115z00_3103;

																		BgL__ortest_1115z00_3103 =
																			(BgL_classz00_3093 == BgL_oclassz00_3099);
																		if (BgL__ortest_1115z00_3103)
																			{	/* Ast/check_type.scm 123 */
																				BgL_res2096z00_3098 =
																					BgL__ortest_1115z00_3103;
																			}
																		else
																			{	/* Ast/check_type.scm 123 */
																				long BgL_odepthz00_3104;

																				{	/* Ast/check_type.scm 123 */
																					obj_t BgL_arg1804z00_3105;

																					BgL_arg1804z00_3105 =
																						(BgL_oclassz00_3099);
																					BgL_odepthz00_3104 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_3105);
																				}
																				if ((2L < BgL_odepthz00_3104))
																					{	/* Ast/check_type.scm 123 */
																						obj_t BgL_arg1802z00_3106;

																						{	/* Ast/check_type.scm 123 */
																							obj_t BgL_arg1803z00_3107;

																							BgL_arg1803z00_3107 =
																								(BgL_oclassz00_3099);
																							BgL_arg1802z00_3106 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_3107, 2L);
																						}
																						BgL_res2096z00_3098 =
																							(BgL_arg1802z00_3106 ==
																							BgL_classz00_3093);
																					}
																				else
																					{	/* Ast/check_type.scm 123 */
																						BgL_res2096z00_3098 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2281z00_4033 = BgL_res2096z00_3098;
															}
													}
												}
											}
											if (BgL_test2281z00_4033)
												{	/* Ast/check_type.scm 123 */
													if (
														((((BgL_globalz00_bglt) COBJECT(
																		((BgL_globalz00_bglt)
																			(((BgL_varz00_bglt) COBJECT(
																						((BgL_varz00_bglt)
																							BgL_nodez00_2898)))->
																				BgL_variablez00))))->BgL_importz00) ==
															CNST_TABLE_REF(2)))
														{	/* Ast/check_type.scm 124 */
															BgL_test2279z00_4028 = ((bool_t) 0);
														}
													else
														{	/* Ast/check_type.scm 124 */
															BgL_test2279z00_4028 = ((bool_t) 1);
														}
												}
											else
												{	/* Ast/check_type.scm 123 */
													BgL_test2279z00_4028 = ((bool_t) 0);
												}
										}
									else
										{	/* Ast/check_type.scm 122 */
											BgL_test2279z00_4028 = ((bool_t) 0);
										}
								}
								if (BgL_test2279z00_4028)
									{	/* Ast/check_type.scm 122 */
										return
											BGl_errzd2nozd2typez00zzast_checkzd2typezd2(
											((BgL_nodez00_bglt)
												((BgL_varz00_bglt) BgL_nodez00_2898)));
									}
								else
									{	/* Ast/check_type.scm 122 */
										return BFALSE;
									}
							}
						}
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_checkzd2typezd2(void)
	{
		{	/* Ast/check_type.scm 16 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string2142z00zzast_checkzd2typezd2));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2142z00zzast_checkzd2typezd2));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string2142z00zzast_checkzd2typezd2));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2142z00zzast_checkzd2typezd2));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2142z00zzast_checkzd2typezd2));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2142z00zzast_checkzd2typezd2));
			BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string2142z00zzast_checkzd2typezd2));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2142z00zzast_checkzd2typezd2));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2142z00zzast_checkzd2typezd2));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2142z00zzast_checkzd2typezd2));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2142z00zzast_checkzd2typezd2));
			BGl_modulezd2initializa7ationz75zztype_coercionz00(116865673L,
				BSTRING_TO_STRING(BGl_string2142z00zzast_checkzd2typezd2));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string2142z00zzast_checkzd2typezd2));
			BGl_modulezd2initializa7ationz75zztvector_tvectorz00(501518119L,
				BSTRING_TO_STRING(BGl_string2142z00zzast_checkzd2typezd2));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string2142z00zzast_checkzd2typezd2));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string2142z00zzast_checkzd2typezd2));
			BGl_modulezd2initializa7ationz75zzmodule_includez00(184800657L,
				BSTRING_TO_STRING(BGl_string2142z00zzast_checkzd2typezd2));
			return
				BGl_modulezd2initializa7ationz75zzmodule_classz00(153808441L,
				BSTRING_TO_STRING(BGl_string2142z00zzast_checkzd2typezd2));
		}

	}

#ifdef __cplusplus
}
#endif
