/*===========================================================================*/
/*   (Ast/lvtype.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/lvtype.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_AST_LVTYPE_TYPE_DEFINITIONS
#define BGL_AST_LVTYPE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_retblockz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                  *BgL_retblockz00_bglt;

	typedef struct BgL_returnz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_retblockz00_bgl *BgL_blockz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                *BgL_returnz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;


#endif													// BGL_AST_LVTYPE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62lvtypezd2astz12za2zzast_lvtypez00(obj_t, obj_t);
	static obj_t BGl_z62lvtypezd2nodez12zd2boxzd2ref1301za2zzast_lvtypez00(obj_t,
		obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	extern obj_t BGl_returnz00zzast_nodez00;
	static obj_t BGl_requirezd2initializa7ationz75zzast_lvtypez00 = BUNSPEC;
	extern obj_t BGl_closurez00zzast_nodez00;
	static obj_t
		BGl_z62lvtypezd2nodez12zd2setzd2exzd21289z70zzast_lvtypez00(obj_t, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_z62lvtypezd2nodez12zd2conditi1279z70zzast_lvtypez00(obj_t,
		obj_t);
	static obj_t
		BGl_setzd2variablezd2typez12z12zzast_lvtypez00(BgL_variablez00_bglt,
		BgL_typez00_bglt);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_z62lvtypezd2nodez12zd2funcall1270z70zzast_lvtypez00(obj_t,
		obj_t);
	static obj_t BGl_z62lvtypezd2nodez12zd2cast1274z70zzast_lvtypez00(obj_t,
		obj_t);
	static obj_t BGl_z62lvtypezd2nodez12zd2retbloc1293z70zzast_lvtypez00(obj_t,
		obj_t);
	static obj_t BGl_toplevelzd2initzd2zzast_lvtypez00(void);
	static obj_t BGl_z62lvtypezd2nodez12zd2letzd2var1287za2zzast_lvtypez00(obj_t,
		obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static obj_t BGl_z62lvtypezd2nodez12zd2appzd2ly1268za2zzast_lvtypez00(obj_t,
		obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lvtypezd2nodez12zd2setq1276z70zzast_lvtypez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_lvtypezd2nodez12zc0zzast_lvtypez00(BgL_nodez00_bglt);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzast_lvtypez00(void);
	static obj_t BGl_z62lvtypezd2nodez12zd2boxzd2set1299za2zzast_lvtypez00(obj_t,
		obj_t);
	static obj_t BGl_z62lvtypezd2nodez12zd2closure1260z70zzast_lvtypez00(obj_t,
		obj_t);
	static obj_t BGl_z62lvtypezd2nodez12zd2sync1264z70zzast_lvtypez00(obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zzast_lvtypez00(void);
	extern obj_t BGl_castz00zzast_nodez00;
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	static obj_t BGl_z62lvtypezd2nodez12zd2fail1281z70zzast_lvtypez00(obj_t,
		obj_t);
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_lvtypezd2nodezd2zzast_lvtypez00(BgL_nodez00_bglt);
	static obj_t BGl_z62lvtypezd2nodez12zd2sequenc1262z70zzast_lvtypez00(obj_t,
		obj_t);
	static obj_t BGl_z62lvtypezd2nodez12zd2return1295z70zzast_lvtypez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_z62lvtypezd2nodez12zd2extern1272z70zzast_lvtypez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzast_lvtypez00(void);
	static obj_t BGl_z62lvtypezd2nodez12zd2switch1283z70zzast_lvtypez00(obj_t,
		obj_t);
	extern obj_t BGl_externz00zzast_nodez00;
	static bool_t BGl_lvtypezd2nodeza2z12z62zzast_lvtypez00(obj_t);
	static obj_t BGl_z62lvtypezd2nodez12zd2makezd2bo1297za2zzast_lvtypez00(obj_t,
		obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern BgL_typez00_bglt BGl_getzd2typezd2zztype_typeofz00(BgL_nodez00_bglt,
		bool_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	static BgL_nodez00_bglt BGl_z62lvtypezd2nodezb0zzast_lvtypez00(obj_t, obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static obj_t BGl_z62lvtypezd2nodez121251za2zzast_lvtypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzast_lvtypez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t BGl_z62lvtypezd2nodez12za2zzast_lvtypez00(obj_t, obj_t);
	static obj_t BGl_z62lvtypezd2nodez12zd2jumpzd2ex1291za2zzast_lvtypez00(obj_t,
		obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzast_lvtypez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_lvtypez00(void);
	static obj_t BGl_z62lvtypezd2nodez12zd2var1258z70zzast_lvtypez00(obj_t,
		obj_t);
	static obj_t BGl_z62lvtypezd2nodez12zd2atom1254z70zzast_lvtypez00(obj_t,
		obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzast_lvtypez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzast_lvtypez00(void);
	static obj_t BGl_z62lvtypezd2nodez12zd2kwote1256z70zzast_lvtypez00(obj_t,
		obj_t);
	extern obj_t BGl_retblockz00zzast_nodez00;
	BGL_EXPORTED_DECL obj_t BGl_lvtypezd2astz12zc0zzast_lvtypez00(obj_t);
	static obj_t BGl_z62lvtypezd2nodez12zd2letzd2fun1285za2zzast_lvtypez00(obj_t,
		obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	static obj_t BGl_z62lvtypezd2nodez12zd2app1266z70zzast_lvtypez00(obj_t,
		obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t BGl_getzd2objzd2typez00zzast_lvtypez00(BgL_nodez00_bglt);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static obj_t __cnst[1];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_lvtypezd2nodezd2envz00zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2node1772z00, BGl_z62lvtypezd2nodezb0zzast_lvtypez00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_lvtypezd2nodez12zd2envz12zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2node1773z00,
		BGl_z62lvtypezd2nodez12za2zzast_lvtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1741z00zzast_lvtypez00,
		BgL_bgl_string1741za700za7za7a1774za7, "lvtype-node!1251", 16);
	      DEFINE_STRING(BGl_string1742z00zzast_lvtypez00,
		BgL_bgl_string1742za700za7za7a1775za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string1744z00zzast_lvtypez00,
		BgL_bgl_string1744za700za7za7a1776za7, "lvtype-node!", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1740z00zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2node1777z00,
		BGl_z62lvtypezd2nodez121251za2zzast_lvtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1743z00zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2node1778z00,
		BGl_z62lvtypezd2nodez12zd2atom1254z70zzast_lvtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1745z00zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2node1779z00,
		BGl_z62lvtypezd2nodez12zd2kwote1256z70zzast_lvtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1746z00zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2node1780z00,
		BGl_z62lvtypezd2nodez12zd2var1258z70zzast_lvtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1747z00zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2node1781z00,
		BGl_z62lvtypezd2nodez12zd2closure1260z70zzast_lvtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1748z00zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2node1782z00,
		BGl_z62lvtypezd2nodez12zd2sequenc1262z70zzast_lvtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1749z00zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2node1783z00,
		BGl_z62lvtypezd2nodez12zd2sync1264z70zzast_lvtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1750z00zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2node1784z00,
		BGl_z62lvtypezd2nodez12zd2app1266z70zzast_lvtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1751z00zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2node1785z00,
		BGl_z62lvtypezd2nodez12zd2appzd2ly1268za2zzast_lvtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1752z00zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2node1786z00,
		BGl_z62lvtypezd2nodez12zd2funcall1270z70zzast_lvtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1753z00zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2node1787z00,
		BGl_z62lvtypezd2nodez12zd2extern1272z70zzast_lvtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1754z00zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2node1788z00,
		BGl_z62lvtypezd2nodez12zd2cast1274z70zzast_lvtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1755z00zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2node1789z00,
		BGl_z62lvtypezd2nodez12zd2setq1276z70zzast_lvtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1756z00zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2node1790z00,
		BGl_z62lvtypezd2nodez12zd2conditi1279z70zzast_lvtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1757z00zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2node1791z00,
		BGl_z62lvtypezd2nodez12zd2fail1281z70zzast_lvtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1758z00zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2node1792z00,
		BGl_z62lvtypezd2nodez12zd2switch1283z70zzast_lvtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1759z00zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2node1793z00,
		BGl_z62lvtypezd2nodez12zd2letzd2fun1285za2zzast_lvtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1768z00zzast_lvtypez00,
		BgL_bgl_string1768za700za7za7a1794za7, "Unexpected closure", 18);
	      DEFINE_STRING(BGl_string1769z00zzast_lvtypez00,
		BgL_bgl_string1769za700za7za7a1795za7, "ast_lvtype", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1760z00zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2node1796z00,
		BGl_z62lvtypezd2nodez12zd2letzd2var1287za2zzast_lvtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1761z00zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2node1797z00,
		BGl_z62lvtypezd2nodez12zd2setzd2exzd21289z70zzast_lvtypez00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1762z00zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2node1798z00,
		BGl_z62lvtypezd2nodez12zd2jumpzd2ex1291za2zzast_lvtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1763z00zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2node1799z00,
		BGl_z62lvtypezd2nodez12zd2retbloc1293z70zzast_lvtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1770z00zzast_lvtypez00,
		BgL_bgl_string1770za700za7za7a1800za7, "lvtype-node!1251 ", 17);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1764z00zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2node1801z00,
		BGl_z62lvtypezd2nodez12zd2return1295z70zzast_lvtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1765z00zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2node1802z00,
		BGl_z62lvtypezd2nodez12zd2makezd2bo1297za2zzast_lvtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1766z00zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2node1803z00,
		BGl_z62lvtypezd2nodez12zd2boxzd2set1299za2zzast_lvtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1767z00zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2node1804z00,
		BGl_z62lvtypezd2nodez12zd2boxzd2ref1301za2zzast_lvtypez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_lvtypezd2astz12zd2envz12zzast_lvtypez00,
		BgL_bgl_za762lvtypeza7d2astza71805za7,
		BGl_z62lvtypezd2astz12za2zzast_lvtypez00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzast_lvtypez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzast_lvtypez00(long
		BgL_checksumz00_1987, char *BgL_fromz00_1988)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_lvtypez00))
				{
					BGl_requirezd2initializa7ationz75zzast_lvtypez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_lvtypez00();
					BGl_libraryzd2moduleszd2initz00zzast_lvtypez00();
					BGl_cnstzd2initzd2zzast_lvtypez00();
					BGl_importedzd2moduleszd2initz00zzast_lvtypez00();
					BGl_genericzd2initzd2zzast_lvtypez00();
					BGl_methodzd2initzd2zzast_lvtypez00();
					return BGl_toplevelzd2initzd2zzast_lvtypez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_lvtypez00(void)
	{
		{	/* Ast/lvtype.scm 16 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "ast_lvtype");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_lvtype");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "ast_lvtype");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "ast_lvtype");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "ast_lvtype");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "ast_lvtype");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "ast_lvtype");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"ast_lvtype");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_lvtype");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_lvtypez00(void)
	{
		{	/* Ast/lvtype.scm 16 */
			{	/* Ast/lvtype.scm 16 */
				obj_t BgL_cportz00_1918;

				{	/* Ast/lvtype.scm 16 */
					obj_t BgL_stringz00_1925;

					BgL_stringz00_1925 = BGl_string1770z00zzast_lvtypez00;
					{	/* Ast/lvtype.scm 16 */
						obj_t BgL_startz00_1926;

						BgL_startz00_1926 = BINT(0L);
						{	/* Ast/lvtype.scm 16 */
							obj_t BgL_endz00_1927;

							BgL_endz00_1927 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1925)));
							{	/* Ast/lvtype.scm 16 */

								BgL_cportz00_1918 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1925, BgL_startz00_1926, BgL_endz00_1927);
				}}}}
				{
					long BgL_iz00_1919;

					BgL_iz00_1919 = 0L;
				BgL_loopz00_1920:
					if ((BgL_iz00_1919 == -1L))
						{	/* Ast/lvtype.scm 16 */
							return BUNSPEC;
						}
					else
						{	/* Ast/lvtype.scm 16 */
							{	/* Ast/lvtype.scm 16 */
								obj_t BgL_arg1771z00_1921;

								{	/* Ast/lvtype.scm 16 */

									{	/* Ast/lvtype.scm 16 */
										obj_t BgL_locationz00_1923;

										BgL_locationz00_1923 = BBOOL(((bool_t) 0));
										{	/* Ast/lvtype.scm 16 */

											BgL_arg1771z00_1921 =
												BGl_readz00zz__readerz00(BgL_cportz00_1918,
												BgL_locationz00_1923);
										}
									}
								}
								{	/* Ast/lvtype.scm 16 */
									int BgL_tmpz00_2017;

									BgL_tmpz00_2017 = (int) (BgL_iz00_1919);
									CNST_TABLE_SET(BgL_tmpz00_2017, BgL_arg1771z00_1921);
							}}
							{	/* Ast/lvtype.scm 16 */
								int BgL_auxz00_1924;

								BgL_auxz00_1924 = (int) ((BgL_iz00_1919 - 1L));
								{
									long BgL_iz00_2022;

									BgL_iz00_2022 = (long) (BgL_auxz00_1924);
									BgL_iz00_1919 = BgL_iz00_2022;
									goto BgL_loopz00_1920;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_lvtypez00(void)
	{
		{	/* Ast/lvtype.scm 16 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzast_lvtypez00(void)
	{
		{	/* Ast/lvtype.scm 16 */
			return BUNSPEC;
		}

	}



/* lvtype-ast! */
	BGL_EXPORTED_DEF obj_t BGl_lvtypezd2astz12zc0zzast_lvtypez00(obj_t
		BgL_astz00_3)
	{
		{	/* Ast/lvtype.scm 32 */
			{
				obj_t BgL_l1238z00_1384;

				BgL_l1238z00_1384 = BgL_astz00_3;
			BgL_zc3z04anonymousza31308ze3z87_1385:
				if (PAIRP(BgL_l1238z00_1384))
					{	/* Ast/lvtype.scm 33 */
						{	/* Ast/lvtype.scm 34 */
							obj_t BgL_gz00_1387;

							BgL_gz00_1387 = CAR(BgL_l1238z00_1384);
							{	/* Ast/lvtype.scm 34 */
								obj_t BgL_arg1310z00_1388;

								BgL_arg1310z00_1388 =
									(((BgL_sfunz00_bglt) COBJECT(
											((BgL_sfunz00_bglt)
												(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_globalz00_bglt) BgL_gz00_1387))))->
													BgL_valuez00))))->BgL_bodyz00);
								BGl_lvtypezd2nodez12zc0zzast_lvtypez00(((BgL_nodez00_bglt)
										BgL_arg1310z00_1388));
							}
						}
						{
							obj_t BgL_l1238z00_2035;

							BgL_l1238z00_2035 = CDR(BgL_l1238z00_1384);
							BgL_l1238z00_1384 = BgL_l1238z00_2035;
							goto BgL_zc3z04anonymousza31308ze3z87_1385;
						}
					}
				else
					{	/* Ast/lvtype.scm 33 */
						((bool_t) 1);
					}
			}
			return BgL_astz00_3;
		}

	}



/* &lvtype-ast! */
	obj_t BGl_z62lvtypezd2astz12za2zzast_lvtypez00(obj_t BgL_envz00_1837,
		obj_t BgL_astz00_1838)
	{
		{	/* Ast/lvtype.scm 32 */
			return BGl_lvtypezd2astz12zc0zzast_lvtypez00(BgL_astz00_1838);
		}

	}



/* get-obj-type */
	obj_t BGl_getzd2objzd2typez00zzast_lvtypez00(BgL_nodez00_bglt BgL_nodez00_4)
	{
		{	/* Ast/lvtype.scm 41 */
			{	/* Ast/lvtype.scm 42 */
				BgL_typez00_bglt BgL_tyz00_1392;

				BgL_tyz00_1392 =
					BGl_getzd2typezd2zztype_typeofz00(BgL_nodez00_4, ((bool_t) 0));
				if ((((obj_t) BgL_tyz00_1392) == BGl_za2_za2z00zztype_cachez00))
					{	/* Ast/lvtype.scm 43 */
						return BGl_za2objza2z00zztype_cachez00;
					}
				else
					{	/* Ast/lvtype.scm 43 */
						return ((obj_t) BgL_tyz00_1392);
					}
			}
		}

	}



/* lvtype-node */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_lvtypezd2nodezd2zzast_lvtypez00(BgL_nodez00_bglt BgL_nodez00_5)
	{
		{	/* Ast/lvtype.scm 50 */
			BGl_lvtypezd2nodez12zc0zzast_lvtypez00(BgL_nodez00_5);
			return BgL_nodez00_5;
		}

	}



/* &lvtype-node */
	BgL_nodez00_bglt BGl_z62lvtypezd2nodezb0zzast_lvtypez00(obj_t BgL_envz00_1839,
		obj_t BgL_nodez00_1840)
	{
		{	/* Ast/lvtype.scm 50 */
			return
				BGl_lvtypezd2nodezd2zzast_lvtypez00(
				((BgL_nodez00_bglt) BgL_nodez00_1840));
		}

	}



/* lvtype-node*! */
	bool_t BGl_lvtypezd2nodeza2z12z62zzast_lvtypez00(obj_t BgL_nodeza2za2_31)
	{
		{	/* Ast/lvtype.scm 268 */
			{
				obj_t BgL_l1249z00_1394;

				BgL_l1249z00_1394 = BgL_nodeza2za2_31;
			BgL_zc3z04anonymousza31313ze3z87_1395:
				if (PAIRP(BgL_l1249z00_1394))
					{	/* Ast/lvtype.scm 269 */
						{	/* Ast/lvtype.scm 269 */
							obj_t BgL_arg1315z00_1397;

							BgL_arg1315z00_1397 = CAR(BgL_l1249z00_1394);
							BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
								((BgL_nodez00_bglt) BgL_arg1315z00_1397));
						}
						{
							obj_t BgL_l1249z00_2051;

							BgL_l1249z00_2051 = CDR(BgL_l1249z00_1394);
							BgL_l1249z00_1394 = BgL_l1249z00_2051;
							goto BgL_zc3z04anonymousza31313ze3z87_1395;
						}
					}
				else
					{	/* Ast/lvtype.scm 269 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* set-variable-type! */
	obj_t BGl_setzd2variablezd2typez12z12zzast_lvtypez00(BgL_variablez00_bglt
		BgL_variablez00_32, BgL_typez00_bglt BgL_typez00_33)
	{
		{	/* Ast/lvtype.scm 274 */
			{	/* Ast/lvtype.scm 275 */
				obj_t BgL_ntypez00_1400;
				BgL_typez00_bglt BgL_otypez00_1401;

				if ((((obj_t) BgL_typez00_33) == BGl_za2_za2z00zztype_cachez00))
					{	/* Ast/lvtype.scm 275 */
						BgL_ntypez00_1400 = BGl_za2objza2z00zztype_cachez00;
					}
				else
					{	/* Ast/lvtype.scm 275 */
						BgL_ntypez00_1400 = ((obj_t) BgL_typez00_33);
					}
				BgL_otypez00_1401 =
					(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_32))->BgL_typez00);
				if ((((obj_t) BgL_otypez00_1401) == BGl_za2_za2z00zztype_cachez00))
					{	/* Ast/lvtype.scm 277 */
						return
							((((BgL_variablez00_bglt) COBJECT(BgL_variablez00_32))->
								BgL_typez00) =
							((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_ntypez00_1400)),
							BUNSPEC);
					}
				else
					{	/* Ast/lvtype.scm 277 */
						return BFALSE;
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_lvtypez00(void)
	{
		{	/* Ast/lvtype.scm 16 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_lvtypez00(void)
	{
		{	/* Ast/lvtype.scm 16 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_lvtypezd2nodez12zd2envz12zzast_lvtypez00,
				BGl_proc1740z00zzast_lvtypez00, BGl_nodez00zzast_nodez00,
				BGl_string1741z00zzast_lvtypez00);
		}

	}



/* &lvtype-node!1251 */
	obj_t BGl_z62lvtypezd2nodez121251za2zzast_lvtypez00(obj_t BgL_envz00_1842,
		obj_t BgL_nodez00_1843)
	{
		{	/* Ast/lvtype.scm 57 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(0),
				BGl_string1742z00zzast_lvtypez00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_1843)));
		}

	}



/* lvtype-node! */
	BGL_EXPORTED_DEF obj_t BGl_lvtypezd2nodez12zc0zzast_lvtypez00(BgL_nodez00_bglt
		BgL_nodez00_6)
	{
		{	/* Ast/lvtype.scm 57 */
			{	/* Ast/lvtype.scm 57 */
				obj_t BgL_method1252z00_1406;

				{	/* Ast/lvtype.scm 57 */
					obj_t BgL_res1739z00_1813;

					{	/* Ast/lvtype.scm 57 */
						long BgL_objzd2classzd2numz00_1784;

						BgL_objzd2classzd2numz00_1784 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_6));
						{	/* Ast/lvtype.scm 57 */
							obj_t BgL_arg1811z00_1785;

							BgL_arg1811z00_1785 =
								PROCEDURE_REF(BGl_lvtypezd2nodez12zd2envz12zzast_lvtypez00,
								(int) (1L));
							{	/* Ast/lvtype.scm 57 */
								int BgL_offsetz00_1788;

								BgL_offsetz00_1788 = (int) (BgL_objzd2classzd2numz00_1784);
								{	/* Ast/lvtype.scm 57 */
									long BgL_offsetz00_1789;

									BgL_offsetz00_1789 =
										((long) (BgL_offsetz00_1788) - OBJECT_TYPE);
									{	/* Ast/lvtype.scm 57 */
										long BgL_modz00_1790;

										BgL_modz00_1790 =
											(BgL_offsetz00_1789 >> (int) ((long) ((int) (4L))));
										{	/* Ast/lvtype.scm 57 */
											long BgL_restz00_1792;

											BgL_restz00_1792 =
												(BgL_offsetz00_1789 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/lvtype.scm 57 */

												{	/* Ast/lvtype.scm 57 */
													obj_t BgL_bucketz00_1794;

													BgL_bucketz00_1794 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_1785), BgL_modz00_1790);
													BgL_res1739z00_1813 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_1794), BgL_restz00_1792);
					}}}}}}}}
					BgL_method1252z00_1406 = BgL_res1739z00_1813;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1252z00_1406, ((obj_t) BgL_nodez00_6));
			}
		}

	}



/* &lvtype-node! */
	obj_t BGl_z62lvtypezd2nodez12za2zzast_lvtypez00(obj_t BgL_envz00_1844,
		obj_t BgL_nodez00_1845)
	{
		{	/* Ast/lvtype.scm 57 */
			return
				BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
				((BgL_nodez00_bglt) BgL_nodez00_1845));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_lvtypez00(void)
	{
		{	/* Ast/lvtype.scm 16 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_lvtypezd2nodez12zd2envz12zzast_lvtypez00, BGl_atomz00zzast_nodez00,
				BGl_proc1743z00zzast_lvtypez00, BGl_string1744z00zzast_lvtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_lvtypezd2nodez12zd2envz12zzast_lvtypez00,
				BGl_kwotez00zzast_nodez00, BGl_proc1745z00zzast_lvtypez00,
				BGl_string1744z00zzast_lvtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_lvtypezd2nodez12zd2envz12zzast_lvtypez00, BGl_varz00zzast_nodez00,
				BGl_proc1746z00zzast_lvtypez00, BGl_string1744z00zzast_lvtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_lvtypezd2nodez12zd2envz12zzast_lvtypez00,
				BGl_closurez00zzast_nodez00, BGl_proc1747z00zzast_lvtypez00,
				BGl_string1744z00zzast_lvtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_lvtypezd2nodez12zd2envz12zzast_lvtypez00,
				BGl_sequencez00zzast_nodez00, BGl_proc1748z00zzast_lvtypez00,
				BGl_string1744z00zzast_lvtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_lvtypezd2nodez12zd2envz12zzast_lvtypez00, BGl_syncz00zzast_nodez00,
				BGl_proc1749z00zzast_lvtypez00, BGl_string1744z00zzast_lvtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_lvtypezd2nodez12zd2envz12zzast_lvtypez00, BGl_appz00zzast_nodez00,
				BGl_proc1750z00zzast_lvtypez00, BGl_string1744z00zzast_lvtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_lvtypezd2nodez12zd2envz12zzast_lvtypez00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1751z00zzast_lvtypez00,
				BGl_string1744z00zzast_lvtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_lvtypezd2nodez12zd2envz12zzast_lvtypez00,
				BGl_funcallz00zzast_nodez00, BGl_proc1752z00zzast_lvtypez00,
				BGl_string1744z00zzast_lvtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_lvtypezd2nodez12zd2envz12zzast_lvtypez00,
				BGl_externz00zzast_nodez00, BGl_proc1753z00zzast_lvtypez00,
				BGl_string1744z00zzast_lvtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_lvtypezd2nodez12zd2envz12zzast_lvtypez00, BGl_castz00zzast_nodez00,
				BGl_proc1754z00zzast_lvtypez00, BGl_string1744z00zzast_lvtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_lvtypezd2nodez12zd2envz12zzast_lvtypez00, BGl_setqz00zzast_nodez00,
				BGl_proc1755z00zzast_lvtypez00, BGl_string1744z00zzast_lvtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_lvtypezd2nodez12zd2envz12zzast_lvtypez00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1756z00zzast_lvtypez00,
				BGl_string1744z00zzast_lvtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_lvtypezd2nodez12zd2envz12zzast_lvtypez00, BGl_failz00zzast_nodez00,
				BGl_proc1757z00zzast_lvtypez00, BGl_string1744z00zzast_lvtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_lvtypezd2nodez12zd2envz12zzast_lvtypez00,
				BGl_switchz00zzast_nodez00, BGl_proc1758z00zzast_lvtypez00,
				BGl_string1744z00zzast_lvtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_lvtypezd2nodez12zd2envz12zzast_lvtypez00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1759z00zzast_lvtypez00,
				BGl_string1744z00zzast_lvtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_lvtypezd2nodez12zd2envz12zzast_lvtypez00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1760z00zzast_lvtypez00,
				BGl_string1744z00zzast_lvtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_lvtypezd2nodez12zd2envz12zzast_lvtypez00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc1761z00zzast_lvtypez00,
				BGl_string1744z00zzast_lvtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_lvtypezd2nodez12zd2envz12zzast_lvtypez00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc1762z00zzast_lvtypez00,
				BGl_string1744z00zzast_lvtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_lvtypezd2nodez12zd2envz12zzast_lvtypez00,
				BGl_retblockz00zzast_nodez00, BGl_proc1763z00zzast_lvtypez00,
				BGl_string1744z00zzast_lvtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_lvtypezd2nodez12zd2envz12zzast_lvtypez00,
				BGl_returnz00zzast_nodez00, BGl_proc1764z00zzast_lvtypez00,
				BGl_string1744z00zzast_lvtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_lvtypezd2nodez12zd2envz12zzast_lvtypez00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1765z00zzast_lvtypez00,
				BGl_string1744z00zzast_lvtypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_lvtypezd2nodez12zd2envz12zzast_lvtypez00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1766z00zzast_lvtypez00,
				BGl_string1744z00zzast_lvtypez00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_lvtypezd2nodez12zd2envz12zzast_lvtypez00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1767z00zzast_lvtypez00,
				BGl_string1744z00zzast_lvtypez00);
		}

	}



/* &lvtype-node!-box-ref1301 */
	obj_t BGl_z62lvtypezd2nodez12zd2boxzd2ref1301za2zzast_lvtypez00(obj_t
		BgL_envz00_1870, obj_t BgL_nodez00_1871)
	{
		{	/* Ast/lvtype.scm 261 */
			{	/* Ast/lvtype.scm 263 */
				BgL_varz00_bglt BgL_arg1575z00_1931;

				BgL_arg1575z00_1931 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_nodez00_1871)))->BgL_varz00);
				return
					BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
					((BgL_nodez00_bglt) BgL_arg1575z00_1931));
			}
		}

	}



/* &lvtype-node!-box-set1299 */
	obj_t BGl_z62lvtypezd2nodez12zd2boxzd2set1299za2zzast_lvtypez00(obj_t
		BgL_envz00_1872, obj_t BgL_nodez00_1873)
	{
		{	/* Ast/lvtype.scm 253 */
			{	/* Ast/lvtype.scm 255 */
				BgL_varz00_bglt BgL_arg1571z00_1933;

				BgL_arg1571z00_1933 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_1873)))->BgL_varz00);
				BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
					((BgL_nodez00_bglt) BgL_arg1571z00_1933));
			}
			return
				BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
				(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_1873)))->BgL_valuez00));
		}

	}



/* &lvtype-node!-make-bo1297 */
	obj_t BGl_z62lvtypezd2nodez12zd2makezd2bo1297za2zzast_lvtypez00(obj_t
		BgL_envz00_1874, obj_t BgL_nodez00_1875)
	{
		{	/* Ast/lvtype.scm 246 */
			return
				BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
				(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_1875)))->BgL_valuez00));
		}

	}



/* &lvtype-node!-return1295 */
	obj_t BGl_z62lvtypezd2nodez12zd2return1295z70zzast_lvtypez00(obj_t
		BgL_envz00_1876, obj_t BgL_nodez00_1877)
	{
		{	/* Ast/lvtype.scm 239 */
			return
				BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
				(((BgL_returnz00_bglt) COBJECT(
							((BgL_returnz00_bglt) BgL_nodez00_1877)))->BgL_valuez00));
		}

	}



/* &lvtype-node!-retbloc1293 */
	obj_t BGl_z62lvtypezd2nodez12zd2retbloc1293z70zzast_lvtypez00(obj_t
		BgL_envz00_1878, obj_t BgL_nodez00_1879)
	{
		{	/* Ast/lvtype.scm 232 */
			return
				BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
				(((BgL_retblockz00_bglt) COBJECT(
							((BgL_retblockz00_bglt) BgL_nodez00_1879)))->BgL_bodyz00));
		}

	}



/* &lvtype-node!-jump-ex1291 */
	obj_t BGl_z62lvtypezd2nodez12zd2jumpzd2ex1291za2zzast_lvtypez00(obj_t
		BgL_envz00_1880, obj_t BgL_nodez00_1881)
	{
		{	/* Ast/lvtype.scm 224 */
			BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_1881)))->BgL_exitz00));
			return
				BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_1881)))->BgL_valuez00));
		}

	}



/* &lvtype-node!-set-ex-1289 */
	obj_t BGl_z62lvtypezd2nodez12zd2setzd2exzd21289z70zzast_lvtypez00(obj_t
		BgL_envz00_1882, obj_t BgL_nodez00_1883)
	{
		{	/* Ast/lvtype.scm 215 */
			BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_1883)))->BgL_bodyz00));
			BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_1883)))->BgL_onexitz00));
			{	/* Ast/lvtype.scm 219 */
				BgL_varz00_bglt BgL_arg1552z00_1939;

				BgL_arg1552z00_1939 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_1883)))->BgL_varz00);
				return
					BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
					((BgL_nodez00_bglt) BgL_arg1552z00_1939));
			}
		}

	}



/* &lvtype-node!-let-var1287 */
	obj_t BGl_z62lvtypezd2nodez12zd2letzd2var1287za2zzast_lvtypez00(obj_t
		BgL_envz00_1884, obj_t BgL_nodez00_1885)
	{
		{	/* Ast/lvtype.scm 200 */
			{	/* Ast/lvtype.scm 202 */
				obj_t BgL_g1248z00_1941;

				BgL_g1248z00_1941 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_1885)))->BgL_bindingsz00);
				{
					obj_t BgL_l1246z00_1943;

					BgL_l1246z00_1943 = BgL_g1248z00_1941;
				BgL_zc3z04anonymousza31503ze3z87_1942:
					if (PAIRP(BgL_l1246z00_1943))
						{	/* Ast/lvtype.scm 202 */
							{	/* Ast/lvtype.scm 203 */
								obj_t BgL_bindingz00_1944;

								BgL_bindingz00_1944 = CAR(BgL_l1246z00_1943);
								{	/* Ast/lvtype.scm 203 */
									obj_t BgL_varz00_1945;
									obj_t BgL_valz00_1946;

									BgL_varz00_1945 = CAR(((obj_t) BgL_bindingz00_1944));
									BgL_valz00_1946 = CDR(((obj_t) BgL_bindingz00_1944));
									BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
										((BgL_nodez00_bglt) BgL_valz00_1946));
									{	/* Ast/lvtype.scm 206 */
										BgL_typez00_bglt BgL_arg1509z00_1947;

										BgL_arg1509z00_1947 =
											BGl_getzd2typezd2zztype_typeofz00(
											((BgL_nodez00_bglt) BgL_valz00_1946), ((bool_t) 0));
										BGl_setzd2variablezd2typez12z12zzast_lvtypez00(
											((BgL_variablez00_bglt) BgL_varz00_1945),
											BgL_arg1509z00_1947);
									}
								}
							}
							{
								obj_t BgL_l1246z00_2175;

								BgL_l1246z00_2175 = CDR(BgL_l1246z00_1943);
								BgL_l1246z00_1943 = BgL_l1246z00_2175;
								goto BgL_zc3z04anonymousza31503ze3z87_1942;
							}
						}
					else
						{	/* Ast/lvtype.scm 202 */
							((bool_t) 1);
						}
				}
			}
			BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
				(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_1885)))->BgL_bodyz00));
			{	/* Ast/lvtype.scm 209 */
				bool_t BgL_test1816z00_2180;

				{	/* Ast/lvtype.scm 209 */
					BgL_typez00_bglt BgL_arg1540z00_1948;

					BgL_arg1540z00_1948 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_letzd2varzd2_bglt) BgL_nodez00_1885))))->BgL_typez00);
					BgL_test1816z00_2180 =
						(((obj_t) BgL_arg1540z00_1948) == BGl_za2_za2z00zztype_cachez00);
				}
				if (BgL_test1816z00_2180)
					{
						BgL_typez00_bglt BgL_auxz00_2186;

						{	/* Ast/lvtype.scm 210 */
							BgL_nodez00_bglt BgL_arg1535z00_1949;

							BgL_arg1535z00_1949 =
								(((BgL_letzd2varzd2_bglt) COBJECT(
										((BgL_letzd2varzd2_bglt) BgL_nodez00_1885)))->BgL_bodyz00);
							BgL_auxz00_2186 =
								((BgL_typez00_bglt)
								BGl_getzd2objzd2typez00zzast_lvtypez00(BgL_arg1535z00_1949));
						}
						return
							((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_letzd2varzd2_bglt) BgL_nodez00_1885))))->
								BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_2186), BUNSPEC);
					}
				else
					{	/* Ast/lvtype.scm 209 */
						return BFALSE;
					}
			}
		}

	}



/* &lvtype-node!-let-fun1285 */
	obj_t BGl_z62lvtypezd2nodez12zd2letzd2fun1285za2zzast_lvtypez00(obj_t
		BgL_envz00_1886, obj_t BgL_nodez00_1887)
	{
		{	/* Ast/lvtype.scm 188 */
			{	/* Ast/lvtype.scm 190 */
				obj_t BgL_g1245z00_1951;

				BgL_g1245z00_1951 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_1887)))->BgL_localsz00);
				{
					obj_t BgL_l1243z00_1953;

					BgL_l1243z00_1953 = BgL_g1245z00_1951;
				BgL_zc3z04anonymousza31449ze3z87_1952:
					if (PAIRP(BgL_l1243z00_1953))
						{	/* Ast/lvtype.scm 190 */
							{	/* Ast/lvtype.scm 191 */
								obj_t BgL_localz00_1954;

								BgL_localz00_1954 = CAR(BgL_l1243z00_1953);
								{	/* Ast/lvtype.scm 191 */
									obj_t BgL_arg1453z00_1955;

									BgL_arg1453z00_1955 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt)
													(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt) BgL_localz00_1954))))->
														BgL_valuez00))))->BgL_bodyz00);
									BGl_lvtypezd2nodez12zc0zzast_lvtypez00(((BgL_nodez00_bglt)
											BgL_arg1453z00_1955));
								}
							}
							{
								obj_t BgL_l1243z00_2206;

								BgL_l1243z00_2206 = CDR(BgL_l1243z00_1953);
								BgL_l1243z00_1953 = BgL_l1243z00_2206;
								goto BgL_zc3z04anonymousza31449ze3z87_1952;
							}
						}
					else
						{	/* Ast/lvtype.scm 190 */
							((bool_t) 1);
						}
				}
			}
			BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
				(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_1887)))->BgL_bodyz00));
			{	/* Ast/lvtype.scm 194 */
				bool_t BgL_test1818z00_2211;

				{	/* Ast/lvtype.scm 194 */
					BgL_typez00_bglt BgL_arg1502z00_1956;

					BgL_arg1502z00_1956 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_letzd2funzd2_bglt) BgL_nodez00_1887))))->BgL_typez00);
					BgL_test1818z00_2211 =
						(((obj_t) BgL_arg1502z00_1956) == BGl_za2_za2z00zztype_cachez00);
				}
				if (BgL_test1818z00_2211)
					{
						BgL_typez00_bglt BgL_auxz00_2217;

						{	/* Ast/lvtype.scm 195 */
							BgL_nodez00_bglt BgL_arg1489z00_1957;

							BgL_arg1489z00_1957 =
								(((BgL_letzd2funzd2_bglt) COBJECT(
										((BgL_letzd2funzd2_bglt) BgL_nodez00_1887)))->BgL_bodyz00);
							BgL_auxz00_2217 =
								((BgL_typez00_bglt)
								BGl_getzd2objzd2typez00zzast_lvtypez00(BgL_arg1489z00_1957));
						}
						return
							((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_letzd2funzd2_bglt) BgL_nodez00_1887))))->
								BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_2217), BUNSPEC);
					}
				else
					{	/* Ast/lvtype.scm 194 */
						return BFALSE;
					}
			}
		}

	}



/* &lvtype-node!-switch1283 */
	obj_t BGl_z62lvtypezd2nodez12zd2switch1283z70zzast_lvtypez00(obj_t
		BgL_envz00_1888, obj_t BgL_nodez00_1889)
	{
		{	/* Ast/lvtype.scm 178 */
			BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
				(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_1889)))->BgL_testz00));
			{	/* Ast/lvtype.scm 181 */
				obj_t BgL_g1242z00_1959;

				BgL_g1242z00_1959 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_1889)))->BgL_clausesz00);
				{
					obj_t BgL_l1240z00_1961;

					{	/* Ast/lvtype.scm 181 */
						bool_t BgL_tmpz00_2230;

						BgL_l1240z00_1961 = BgL_g1242z00_1959;
					BgL_zc3z04anonymousza31435ze3z87_1960:
						if (PAIRP(BgL_l1240z00_1961))
							{	/* Ast/lvtype.scm 181 */
								{	/* Ast/lvtype.scm 182 */
									obj_t BgL_clausez00_1962;

									BgL_clausez00_1962 = CAR(BgL_l1240z00_1961);
									{	/* Ast/lvtype.scm 182 */
										obj_t BgL_arg1437z00_1963;

										BgL_arg1437z00_1963 = CDR(((obj_t) BgL_clausez00_1962));
										BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
											((BgL_nodez00_bglt) BgL_arg1437z00_1963));
									}
								}
								{
									obj_t BgL_l1240z00_2238;

									BgL_l1240z00_2238 = CDR(BgL_l1240z00_1961);
									BgL_l1240z00_1961 = BgL_l1240z00_2238;
									goto BgL_zc3z04anonymousza31435ze3z87_1960;
								}
							}
						else
							{	/* Ast/lvtype.scm 181 */
								BgL_tmpz00_2230 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_2230);
					}
				}
			}
		}

	}



/* &lvtype-node!-fail1281 */
	obj_t BGl_z62lvtypezd2nodez12zd2fail1281z70zzast_lvtypez00(obj_t
		BgL_envz00_1890, obj_t BgL_nodez00_1891)
	{
		{	/* Ast/lvtype.scm 169 */
			BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_1891)))->BgL_procz00));
			BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_1891)))->BgL_msgz00));
			return
				BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_1891)))->BgL_objz00));
		}

	}



/* &lvtype-node!-conditi1279 */
	obj_t BGl_z62lvtypezd2nodez12zd2conditi1279z70zzast_lvtypez00(obj_t
		BgL_envz00_1892, obj_t BgL_nodez00_1893)
	{
		{	/* Ast/lvtype.scm 158 */
			BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_1893)))->BgL_testz00));
			BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_1893)))->BgL_truez00));
			BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_1893)))->BgL_falsez00));
			{	/* Ast/lvtype.scm 163 */
				bool_t BgL_test1821z00_2259;

				{	/* Ast/lvtype.scm 163 */
					BgL_typez00_bglt BgL_arg1408z00_1966;

					BgL_arg1408z00_1966 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_conditionalz00_bglt) BgL_nodez00_1893))))->BgL_typez00);
					BgL_test1821z00_2259 =
						(((obj_t) BgL_arg1408z00_1966) == BGl_za2_za2z00zztype_cachez00);
				}
				if (BgL_test1821z00_2259)
					{	/* Ast/lvtype.scm 163 */
						return
							((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_conditionalz00_bglt) BgL_nodez00_1893))))->
								BgL_typez00) =
							((BgL_typez00_bglt) ((BgL_typez00_bglt)
									BGl_getzd2objzd2typez00zzast_lvtypez00(((BgL_nodez00_bglt) (
												(BgL_conditionalz00_bglt) BgL_nodez00_1893))))),
							BUNSPEC);
					}
				else
					{	/* Ast/lvtype.scm 163 */
						return BFALSE;
					}
			}
		}

	}



/* &lvtype-node!-setq1276 */
	obj_t BGl_z62lvtypezd2nodez12zd2setq1276z70zzast_lvtypez00(obj_t
		BgL_envz00_1894, obj_t BgL_nodez00_1895)
	{
		{	/* Ast/lvtype.scm 150 */
			BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
				(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_1895)))->BgL_valuez00));
			{	/* Ast/lvtype.scm 153 */
				BgL_varz00_bglt BgL_arg1370z00_1968;

				BgL_arg1370z00_1968 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_1895)))->BgL_varz00);
				return
					BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
					((BgL_nodez00_bglt) BgL_arg1370z00_1968));
			}
		}

	}



/* &lvtype-node!-cast1274 */
	obj_t BGl_z62lvtypezd2nodez12zd2cast1274z70zzast_lvtypez00(obj_t
		BgL_envz00_1896, obj_t BgL_nodez00_1897)
	{
		{	/* Ast/lvtype.scm 143 */
			return
				BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
				(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_1897)))->BgL_argz00));
		}

	}



/* &lvtype-node!-extern1272 */
	obj_t BGl_z62lvtypezd2nodez12zd2extern1272z70zzast_lvtypez00(obj_t
		BgL_envz00_1898, obj_t BgL_nodez00_1899)
	{
		{	/* Ast/lvtype.scm 134 */
			BGl_lvtypezd2nodeza2z12z62zzast_lvtypez00(
				(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_1899)))->BgL_exprza2za2));
			{	/* Ast/lvtype.scm 137 */
				bool_t BgL_test1822z00_2285;

				{	/* Ast/lvtype.scm 137 */
					BgL_typez00_bglt BgL_arg1361z00_1971;

					BgL_arg1361z00_1971 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_externz00_bglt) BgL_nodez00_1899))))->BgL_typez00);
					BgL_test1822z00_2285 =
						(((obj_t) BgL_arg1361z00_1971) == BGl_za2_za2z00zztype_cachez00);
				}
				if (BgL_test1822z00_2285)
					{	/* Ast/lvtype.scm 137 */
						return
							((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_externz00_bglt) BgL_nodez00_1899))))->BgL_typez00) =
							((BgL_typez00_bglt) ((BgL_typez00_bglt)
									BGl_getzd2objzd2typez00zzast_lvtypez00(((BgL_nodez00_bglt) (
												(BgL_externz00_bglt) BgL_nodez00_1899))))), BUNSPEC);
					}
				else
					{	/* Ast/lvtype.scm 137 */
						return BFALSE;
					}
			}
		}

	}



/* &lvtype-node!-funcall1270 */
	obj_t BGl_z62lvtypezd2nodez12zd2funcall1270z70zzast_lvtypez00(obj_t
		BgL_envz00_1900, obj_t BgL_nodez00_1901)
	{
		{	/* Ast/lvtype.scm 126 */
			BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_1901)))->BgL_funz00));
			{	/* Ast/lvtype.scm 129 */
				obj_t BgL_arg1349z00_1973;

				BgL_arg1349z00_1973 =
					(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_1901)))->BgL_argsz00);
				return
					BBOOL(BGl_lvtypezd2nodeza2z12z62zzast_lvtypez00(BgL_arg1349z00_1973));
			}
		}

	}



/* &lvtype-node!-app-ly1268 */
	obj_t BGl_z62lvtypezd2nodez12zd2appzd2ly1268za2zzast_lvtypez00(obj_t
		BgL_envz00_1902, obj_t BgL_nodez00_1903)
	{
		{	/* Ast/lvtype.scm 118 */
			BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
				(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_1903)))->BgL_funz00));
			return
				BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
				(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_1903)))->BgL_argz00));
		}

	}



/* &lvtype-node!-app1266 */
	obj_t BGl_z62lvtypezd2nodez12zd2app1266z70zzast_lvtypez00(obj_t
		BgL_envz00_1904, obj_t BgL_nodez00_1905)
	{
		{	/* Ast/lvtype.scm 109 */
			BGl_lvtypezd2nodeza2z12z62zzast_lvtypez00(
				(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_1905)))->BgL_argsz00));
			{	/* Ast/lvtype.scm 112 */
				bool_t BgL_test1824z00_2314;

				{	/* Ast/lvtype.scm 112 */
					BgL_typez00_bglt BgL_arg1342z00_1976;

					BgL_arg1342z00_1976 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_appz00_bglt) BgL_nodez00_1905))))->BgL_typez00);
					BgL_test1824z00_2314 =
						(((obj_t) BgL_arg1342z00_1976) == BGl_za2_za2z00zztype_cachez00);
				}
				if (BgL_test1824z00_2314)
					{	/* Ast/lvtype.scm 112 */
						return
							((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_appz00_bglt) BgL_nodez00_1905))))->BgL_typez00) =
							((BgL_typez00_bglt) ((BgL_typez00_bglt)
									BGl_getzd2objzd2typez00zzast_lvtypez00(((BgL_nodez00_bglt) (
												(BgL_appz00_bglt) BgL_nodez00_1905))))), BUNSPEC);
					}
				else
					{	/* Ast/lvtype.scm 112 */
						return BFALSE;
					}
			}
		}

	}



/* &lvtype-node!-sync1264 */
	obj_t BGl_z62lvtypezd2nodez12zd2sync1264z70zzast_lvtypez00(obj_t
		BgL_envz00_1906, obj_t BgL_nodez00_1907)
	{
		{	/* Ast/lvtype.scm 98 */
			BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_1907)))->BgL_mutexz00));
			BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_1907)))->BgL_prelockz00));
			BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_1907)))->BgL_bodyz00));
			{	/* Ast/lvtype.scm 103 */
				bool_t BgL_test1825z00_2336;

				{	/* Ast/lvtype.scm 103 */
					BgL_typez00_bglt BgL_arg1335z00_1978;

					BgL_arg1335z00_1978 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_syncz00_bglt) BgL_nodez00_1907))))->BgL_typez00);
					BgL_test1825z00_2336 =
						(((obj_t) BgL_arg1335z00_1978) == BGl_za2_za2z00zztype_cachez00);
				}
				if (BgL_test1825z00_2336)
					{	/* Ast/lvtype.scm 103 */
						return
							((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_syncz00_bglt) BgL_nodez00_1907))))->BgL_typez00) =
							((BgL_typez00_bglt) ((BgL_typez00_bglt)
									BGl_getzd2objzd2typez00zzast_lvtypez00(((BgL_nodez00_bglt) (
												(BgL_syncz00_bglt) BgL_nodez00_1907))))), BUNSPEC);
					}
				else
					{	/* Ast/lvtype.scm 103 */
						return BFALSE;
					}
			}
		}

	}



/* &lvtype-node!-sequenc1262 */
	obj_t BGl_z62lvtypezd2nodez12zd2sequenc1262z70zzast_lvtypez00(obj_t
		BgL_envz00_1908, obj_t BgL_nodez00_1909)
	{
		{	/* Ast/lvtype.scm 89 */
			BGl_lvtypezd2nodeza2z12z62zzast_lvtypez00(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_1909)))->BgL_nodesz00));
			{	/* Ast/lvtype.scm 92 */
				bool_t BgL_test1826z00_2352;

				{	/* Ast/lvtype.scm 92 */
					BgL_typez00_bglt BgL_arg1327z00_1980;

					BgL_arg1327z00_1980 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_sequencez00_bglt) BgL_nodez00_1909))))->BgL_typez00);
					BgL_test1826z00_2352 =
						(((obj_t) BgL_arg1327z00_1980) == BGl_za2_za2z00zztype_cachez00);
				}
				if (BgL_test1826z00_2352)
					{	/* Ast/lvtype.scm 92 */
						return
							((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_sequencez00_bglt) BgL_nodez00_1909))))->
								BgL_typez00) =
							((BgL_typez00_bglt) ((BgL_typez00_bglt)
									BGl_getzd2objzd2typez00zzast_lvtypez00(((BgL_nodez00_bglt) (
												(BgL_sequencez00_bglt) BgL_nodez00_1909))))), BUNSPEC);
					}
				else
					{	/* Ast/lvtype.scm 92 */
						return BFALSE;
					}
			}
		}

	}



/* &lvtype-node!-closure1260 */
	obj_t BGl_z62lvtypezd2nodez12zd2closure1260z70zzast_lvtypez00(obj_t
		BgL_envz00_1910, obj_t BgL_nodez00_1911)
	{
		{	/* Ast/lvtype.scm 83 */
			{	/* Ast/lvtype.scm 84 */
				obj_t BgL_arg1322z00_1982;

				BgL_arg1322z00_1982 =
					BGl_shapez00zztools_shapez00(
					((obj_t) ((BgL_closurez00_bglt) BgL_nodez00_1911)));
				return
					BGl_internalzd2errorzd2zztools_errorz00
					(BGl_string1744z00zzast_lvtypez00, BGl_string1768z00zzast_lvtypez00,
					BgL_arg1322z00_1982);
			}
		}

	}



/* &lvtype-node!-var1258 */
	obj_t BGl_z62lvtypezd2nodez12zd2var1258z70zzast_lvtypez00(obj_t
		BgL_envz00_1912, obj_t BgL_nodez00_1913)
	{
		{	/* Ast/lvtype.scm 74 */
			{	/* Ast/lvtype.scm 76 */
				bool_t BgL_test1827z00_2369;

				{	/* Ast/lvtype.scm 76 */
					BgL_typez00_bglt BgL_arg1321z00_1984;

					BgL_arg1321z00_1984 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_varz00_bglt) BgL_nodez00_1913))))->BgL_typez00);
					BgL_test1827z00_2369 =
						(((obj_t) BgL_arg1321z00_1984) == BGl_za2_za2z00zztype_cachez00);
				}
				if (BgL_test1827z00_2369)
					{	/* Ast/lvtype.scm 76 */
						((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_varz00_bglt) BgL_nodez00_1913))))->BgL_typez00) =
							((BgL_typez00_bglt) (((BgL_variablez00_bglt)
										COBJECT((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
															BgL_nodez00_1913)))->BgL_variablez00)))->
									BgL_typez00)), BUNSPEC);
					}
				else
					{	/* Ast/lvtype.scm 76 */
						BFALSE;
					}
			}
			return ((obj_t) ((BgL_varz00_bglt) BgL_nodez00_1913));
		}

	}



/* &lvtype-node!-kwote1256 */
	obj_t BGl_z62lvtypezd2nodez12zd2kwote1256z70zzast_lvtypez00(obj_t
		BgL_envz00_1914, obj_t BgL_nodez00_1915)
	{
		{	/* Ast/lvtype.scm 68 */
			return ((obj_t) ((BgL_kwotez00_bglt) BgL_nodez00_1915));
		}

	}



/* &lvtype-node!-atom1254 */
	obj_t BGl_z62lvtypezd2nodez12zd2atom1254z70zzast_lvtypez00(obj_t
		BgL_envz00_1916, obj_t BgL_nodez00_1917)
	{
		{	/* Ast/lvtype.scm 62 */
			return ((obj_t) ((BgL_atomz00_bglt) BgL_nodez00_1917));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_lvtypez00(void)
	{
		{	/* Ast/lvtype.scm 16 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1769z00zzast_lvtypez00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1769z00zzast_lvtypez00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string1769z00zzast_lvtypez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1769z00zzast_lvtypez00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1769z00zzast_lvtypez00));
			BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string1769z00zzast_lvtypez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1769z00zzast_lvtypez00));
			return
				BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1769z00zzast_lvtypez00));
		}

	}

#ifdef __cplusplus
}
#endif
