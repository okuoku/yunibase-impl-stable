/*===========================================================================*/
/*   (Ast/walk.scm)                                                          */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/walk.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_AST_WALK_TYPE_DEFINITIONS
#define BGL_AST_WALK_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_retblockz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                  *BgL_retblockz00_bglt;

	typedef struct BgL_returnz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_retblockz00_bgl *BgL_blockz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                *BgL_returnz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;


#endif													// BGL_AST_WALK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62walk2za2zd2jumpzd2exzd2it1941z12zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62walk1z12zd2setzd2exzd2it1923za2zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk2za2zd2retblock1965z12zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1za2zd2app1722z12zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt BGl_z62walk0z12zd2cast1825za2zzast_walkz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_zc3z04anonymousza32350ze3ze70z60zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk1z12zd2extern1803za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk2za2zd2node1675z12zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	static BgL_nodez00_bglt BGl_z62walk3z12zd2fail1903za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_returnz00zzast_nodez00;
	static obj_t BGl_z62walk3zd2setzd2exzd2it1911zb0zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk2zd2setq1837zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk0z12z70zzast_walkz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk0za2zc0zzast_walkz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk3zd2node1669zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzast_walkz00 = BUNSPEC;
	static BgL_nodez00_bglt BGl_z62walk3z12zd2return1999za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1zd2boxzd2ref2028z62zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk2za2zd2funcall1773z12zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk3za2zd2boxzd2ref2040zc0zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk2zd2letzd2fun2128z62zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk1zd2boxzd2setz122053z70zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk2z12zd2app1732za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1za2zd2appzd2ly1747zc0zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk1z12z70zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk2za2zd2sync2088z12zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk1za2zc0zzast_walkz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_zc3z04anonymousza32530ze3ze70z60zzast_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk0z12zd2letzd2fun2140z70zzast_walkz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk31636z62zzast_walkz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk1zd2retblock1955zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_z62walk1zd2app1714zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk0zd2funcall1761zb0zzast_walkz00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk3z12zd2setq1855za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk0za2zd2boxzd2setz122060zd2zzast_walkz00(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt BGl_z62walk2z12z70zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk2z12zd2switch2120za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk3z12zd2funcall1783za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk3za2zd2extern1799z12zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk2za2zc0zzast_walkz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_zc3z04anonymousza33511ze3ze70z60zzast_walkz00(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62walk0z12zd2conditional1873za2zzast_walkz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk3zd2sync2082zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk0za2zd2retblock1961z12zzast_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk1zd2setzd2exzd2it1907zb0zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk0z12zd2setq1849za2zzast_walkz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk3za2zd2letzd2var2163zc0zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1za2zd2conditional1867z12zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk3z12z70zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzast_walkz00(void);
	static obj_t BGl_z62walk3za2zc0zzast_walkz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk3za2zd2return1991z12zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1zd2letzd2var2150z62zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static obj_t BGl_z62walk0zd2extern1785zb0zzast_walkz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk0zd2return1977zb0zzast_walkz00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk4z12z70zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1za2zd2fail1891z12zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk4za2zc0zzast_walkz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk2za2zd2cast1821z12zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk2za2zd2switch2112z12zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzast_walkz00(void);
	static BgL_nodez00_bglt
		BGl_z62walk2z12zd2conditional1877za2zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk0zd2node1663zb0zzast_walkz00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk1z12zd2node1681za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk3zd2cast1815zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62walk2z12zd2makezd2box2022z70zzast_walkz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_zc3z04anonymousza32540ze3ze70z60zzast_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_zc3z04anonymousza33243ze3ze70z60zzast_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_zc3z04anonymousza32271ze3ze70z60zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk0za2zd2funcall1769z12zzast_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zzast_walkz00(void);
	BGL_IMPORT obj_t BGl_appendz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62walk1zd2appzd2ly1739z62zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk0zd2conditional1857zb0zzast_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk1za2zd2boxzd2ref2036zc0zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk41639z62zzast_walkz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk3zd2extern1791zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk2zd2sequence1691zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk3za2zd2conditional1871z12zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk2zd2fail1885zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32211ze3ze5zzast_walkz00(obj_t);
	static obj_t BGl_z62walk3zd2return1983zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32203ze3ze5zzast_walkz00(obj_t);
	extern obj_t BGl_castz00zzast_nodez00;
	static obj_t BGl_z62walk3zd2funcall1767zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk0z121651z70zzast_walkz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static obj_t BGl_z62walk2zd2conditional1861zb0zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk0zd2sync2076zb0zzast_walkz00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk1z12zd2sync2094za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk2za2zd2letzd2fun2136zc0zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1za2zd2sequence1697z12zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk1z12zd2funcall1779za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk2z12zd2boxzd2ref2047z70zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_zc3z04anonymousza33423ze3ze70z60zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk3z12zd2sequence1710za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62walk1z12zd2jumpzd2exzd2it1947za2zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk3za2zd2makezd2box2016zc0zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1za2zd2letzd2var2159zc0zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk1z12zd2return1995za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk2za2zd2app1724z12zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk2za2zd2setq1845z12zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk2zd2switch2104zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62walk0z12zd2makezd2box2018z70zzast_walkz00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62walk3z12zd2jumpzd2exzd2it1951za2zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk0zd2letzd2fun2124z62zzast_walkz00(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt BGl_z62walk3z12zd2letzd2fun2146z70zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk3zd2setq1839zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk0z12zd2fail1897za2zzast_walkz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk0zd2cast1809zb0zzast_walkz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk2zd2makezd2box2005z62zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk0z12zd2switch2116za2zzast_walkz00(obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk1z12zd2cast1827za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_zc3z04anonymousza33522ze3ze70z60zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk3zd2jumpzd2exzd2it1935zb0zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_zc3z04anonymousza33253ze3ze70z60zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk3za2zd2node1677z12zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzast_walkz00(obj_t, obj_t);
	static obj_t BGl_z62walk0za21641zc0zzast_walkz00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk2z12zd2letzd2var2169z70zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzast_walkz00(void);
	static BgL_nodez00_bglt
		BGl_z62walk2z12zd2boxzd2setz122072z62zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk2zd2app1716zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk3z12zd2app1735za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1za2zd2extern1795z12zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32231ze3ze5zzast_walkz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32223ze3ze5zzast_walkz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32215ze3ze5zzast_walkz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32207ze3ze5zzast_walkz00(obj_t);
	static obj_t BGl_z62walk1za2zd2return1987z12zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t BGl_zc3z04anonymousza32264ze3ze70z60zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk1z12zd2sequence1705za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk2z12zd2appzd2ly1757z70zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk2z12zd2retblock1973za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk0zd2sequence1687zb0zzast_walkz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk2za2zd2setzd2exzd2it1917z12zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk0za2zd2node1671z12zzast_walkz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1zd2jumpzd2exzd2it1931zb0zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk1za2zd2makezd2box2012zc0zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk1z121653z70zzast_walkz00(obj_t, obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk2z12zd2extern1805za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk0za2zd2switch2108z12zzast_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk3za2zd2sync2090z12zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk3za2zd2boxzd2setz122066zd2zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk0za2zd2letzd2fun2132zc0zzast_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_zc3z04anonymousza33496ze3ze70z60zzast_walkz00(obj_t, obj_t);
	static obj_t BGl_z62walk2za2zd2appzd2ly1749zc0zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62walk0z12zd2boxzd2setz122068z62zzast_walkz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk0za2zd2sync2084z12zzast_walkz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk0za2zd2setzd2exzd2it1913z12zzast_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk3za2zd2retblock1967z12zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk2zd2boxzd2ref2030z62zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk0z12zd2boxzd2ref2043z70zzast_walkz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t BGl_z62walk3zd2letzd2fun2130z62zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk3za2zd2funcall1775z12zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk0zd2setq1833zb0zzast_walkz00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk1z12zd2setq1851za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk0z12zd2retblock1969za2zzast_walkz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk1za2zd2jumpzd2exzd2it1939z12zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk2zd2boxzd2setz122055z70zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk0z12zd2app1728za2zzast_walkz00(obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62walk2z12zd2setzd2exzd2it1925za2zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk1zd2funcall1763zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1zd2node1665zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk2za2zd2fail1893z12zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk3z12zd2switch2122za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32235ze3ze5zzast_walkz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32227ze3ze5zzast_walkz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32219ze3ze5zzast_walkz00(obj_t);
	static obj_t BGl_z62walk3za2zd2cast1823z12zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk1z12zd2letzd2fun2142z70zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_zc3z04anonymousza32329ze3ze70z60zzast_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk1za21643zc0zzast_walkz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1zd2extern1787zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk3za2zd2jumpzd2exzd2it1943z12zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk2zd2retblock1957zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk3zd2fail1887zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk0zd2makezd2box2001z62zzast_walkz00(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt BGl_z62walk2z12zd2node1683za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1zd2return1979zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t bgl_append2(obj_t, obj_t);
	static obj_t BGl_z62walk0za2zd2cast1817z12zzast_walkz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk2zd2letzd2var2152z62zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_zc3z04anonymousza32257ze3ze70z60zzast_walkz00(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt BGl_z62walk0z12zd2letzd2var2165z70zzast_walkz00(obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62walk0z12zd2setzd2exzd2it1921za2zzast_walkz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1zd2sync2078zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk1za2zd2boxzd2setz122062zd2zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk3za2zd2switch2114z12zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk2z121655z70zzast_walkz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_walk0z00zzast_walkz00(BgL_nodez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_walk1z00zzast_walkz00(BgL_nodez00_bglt, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_walk2z00zzast_walkz00(BgL_nodez00_bglt, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_walk3z00zzast_walkz00(BgL_nodez00_bglt, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk2z12zd2sync2096za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t BGl_walk4z00zzast_walkz00(BgL_nodez00_bglt, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1za2zd2retblock1963z12zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk2zd2setzd2exzd2it1909zb0zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk3za2zd2app1726z12zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk2zd2appzd2ly1741z62zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_zc3z04anonymousza33380ze3ze70z60zzast_walkz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzast_walkz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__pp_circlez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	static obj_t BGl_z62walk0za2zd2conditional1865z12zzast_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk3zd2app1718zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk0zd2switch2100zb0zzast_walkz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32190ze3ze5zzast_walkz00(obj_t);
	static obj_t BGl_z62walk3za2zd2setq1847z12zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32182ze3ze5zzast_walkz00(obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static BgL_nodez00_bglt BGl_z62walk0z12zd2appzd2ly1753z70zzast_walkz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_zc3z04anonymousza32446ze3ze70z60zzast_walkz00(obj_t, obj_t);
	static obj_t BGl_zc3z04anonymousza32339ze3ze70z60zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk2za2zd2boxzd2ref2038zc0zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk3zd2sequence1693zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1zd2cast1811zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt BGl_z62walk1z12zd2fail1899za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk3zd2switch2106zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1za2zd2funcall1771z12zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62walk3z12zd2makezd2box2024z70zzast_walkz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk0z12zd2extern1801za2zzast_walkz00(obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk2z12zd2cast1829za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_zc3z04anonymousza33534ze3ze70z60zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62walk1z12zd2conditional1875za2zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk0zd2boxzd2setz122051z70zzast_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk0zd2fail1881zb0zzast_walkz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk2za21645zc0zzast_walkz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	static obj_t BGl_z62walk0za2zd2setq1841z12zzast_walkz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk3za2zd2letzd2fun2138zc0zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk2za2zd2conditional1869z12zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk2z12zd2return1997za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk0zd2boxzd2ref2026z62zzast_walkz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static BgL_nodez00_bglt BGl_z62walk3z12zd2boxzd2ref2049z70zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1zd2letzd2fun2126z62zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk0zd2retblock1953zb0zzast_walkz00(obj_t, obj_t, obj_t);
	static obj_t BGl_zc3z04anonymousza32464ze3ze70z60zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_walkz00(void);
	static obj_t BGl_z62walk0za2zd2appzd2ly1745zc0zzast_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk0zd2setzd2exzd2it1905zb0zzast_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk0z62zzast_walkz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1z62zzast_walkz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk2z62zzast_walkz00(obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk3z62zzast_walkz00(obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk4z62zzast_walkz00(obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk2z12zd2funcall1781za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzast_walkz00(void);
	BGL_IMPORT obj_t
		BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62walk01630z62zzast_walkz00(obj_t, obj_t, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzast_walkz00(void);
	static obj_t BGl_z62walk2za2zd2sequence1699z12zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk1z12zd2switch2118za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1za2zd2node1673z12zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt BGl_z62walk2z12zd2fail1901za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk3z121658z70zzast_walkz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk1zd2conditional1859zb0zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_walk0z12z12zzast_walkz00(BgL_nodez00_bglt, obj_t);
	static BgL_nodez00_bglt
		BGl_z62walk3z12zd2conditional1879za2zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk2za2zd2letzd2var2161zc0zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk0za2zd2app1720z12zzast_walkz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_walk0za2za2zzast_walkz00(BgL_nodez00_bglt, obj_t);
	static obj_t BGl_z62walk2za2zd2extern1797z12zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk3zd2makezd2box2007z62zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_zc3z04anonymousza32321ze3ze70z60zzast_walkz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32194ze3ze5zzast_walkz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32186ze3ze5zzast_walkz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32178ze3ze5zzast_walkz00(obj_t);
	extern obj_t BGl_retblockz00zzast_nodez00;
	static obj_t BGl_z62walk2za2zd2return1989z12zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk3zd2conditional1863zb0zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_zc3z04anonymousza32456ze3ze70z60zzast_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk0zd2letzd2var2148z62zzast_walkz00(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt BGl_z62walk3z12zd2appzd2ly1759z70zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1zd2setq1835zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_walk1z12z12zzast_walkz00(BgL_nodez00_bglt, obj_t, obj_t);
	static obj_t BGl_z62walk1za2zd2sync2086z12zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_walk1za2za2zzast_walkz00(BgL_nodez00_bglt, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_writezd2circlezd2zz__pp_circlez00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62walk1z12zd2makezd2box2020z70zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk2zd2node1667zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62walk0z12zd2jumpzd2exzd2it1945za2zzast_walkz00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk3z12zd2extern1807za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62walk2z12zd2jumpzd2exzd2it1949za2zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk3z12zd2letzd2var2171z70zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk1z12zd2app1730za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_walk2z12z12zzast_walkz00(BgL_nodez00_bglt, obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk2z12zd2setq1853za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1zd2sequence1689zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62walk3z12zd2boxzd2setz122074z62zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_walk2za2za2zzast_walkz00(BgL_nodez00_bglt, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk3za2zd2fail1895z12zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1za2zd2switch2110z12zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk0za2zd2boxzd2ref2034zc0zzast_walkz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	static BgL_nodez00_bglt BGl_z62walk2z12zd2sequence1708za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk3z12zd2retblock1975za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_walk3z12z12zzast_walkz00(BgL_nodez00_bglt, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk3za21647zc0zzast_walkz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk0za2zd2sequence1695z12zzast_walkz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_walk3za2za2zzast_walkz00(BgL_nodez00_bglt, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk3zd2boxzd2ref2032z62zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk3z12zd2node1685za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk0za2zd2fail1889z12zzast_walkz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk11632z62zzast_walkz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk2za2zd2makezd2box2014zc0zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1za2zd2cast1819z12zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk2zd2sync2080zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk0zd2app1712zb0zzast_walkz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk3za2zd2appzd2ly1751zc0zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1za2zd2letzd2fun2134zc0zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk2zd2extern1789zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_walk4z12z12zzast_walkz00(BgL_nodez00_bglt, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk2zd2funcall1765zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_walk4za2za2zzast_walkz00(BgL_nodez00_bglt, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk0z12zd2funcall1777za2zzast_walkz00(obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk0z12zd2node1679za2zzast_walkz00(obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk1z12zd2boxzd2ref2045z70zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	static obj_t BGl_z62walk3za2zd2setzd2exzd2it1919z12zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_zc3z04anonymousza32250ze3ze70z60zzast_walkz00(obj_t, obj_t);
	static obj_t BGl_z62walk2zd2jumpzd2exzd2it1933zb0zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk0zd2appzd2ly1737z62zzast_walkz00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk3z12zd2sync2098za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk0za2zd2letzd2var2157zc0zzast_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk2zd2return1981zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk3zd2boxzd2setz122058z70zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t BGl_z62walk0zd2jumpzd2exzd2it1929zb0zzast_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk4z121660z70zzast_walkz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32199ze3ze5zzast_walkz00(obj_t);
	static BgL_nodez00_bglt BGl_z62walk2z12zd2letzd2fun2144z70zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_zc3z04anonymousza33392ze3ze70z60zzast_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk3zd2letzd2var2154z62zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_zc3z04anonymousza32556ze3ze70z60zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_zc3z04anonymousza32475ze3ze70z60zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62walk1z12zd2boxzd2setz122070z62zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk3za2zd2sequence1701z12zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk3zd2retblock1959zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk0z12zd2return1993za2zzast_walkz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk2zd2cast1813zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk1zd2makezd2box2003z62zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk1za2zd2setzd2exzd2it1915z12zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_zc3z04anonymousza33402ze3ze70z60zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk0z12zd2sequence1703za2zzast_walkz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk3zd2appzd2ly1743z62zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk0za2zd2makezd2box2009zc0zzast_walkz00(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt BGl_z62walk1z12zd2retblock1971za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk0z12zd2sync2092za2zzast_walkz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_zc3z04anonymousza33268ze3ze70z60zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk1z12zd2letzd2var2167z70zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1zd2fail1883zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62walk3z12zd2setzd2exzd2it1927za2zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1zd2switch2102zb0zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk21634z62zzast_walkz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_zc3z04anonymousza32548ze3ze70z60zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk0za2zd2jumpzd2exzd2it1937z12zzast_walkz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62walk0za2zd2extern1793z12zzast_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk4za21649zc0zzast_walkz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk1za2zd2setq1843z12zzast_walkz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62walk2za2zd2boxzd2setz122064zd2zzast_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk3z12zd2cast1831za2zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62walk0za2zd2return1985z12zzast_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_zc3z04anonymousza33232ze3ze70z60zzast_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62walk1z12zd2appzd2ly1755z70zzast_walkz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4000z00zzast_walkz00,
		BgL_bgl_za762walk1za712za7d2sw4112za7,
		BGl_z62walk1z12zd2switch2118za2zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4001z00zzast_walkz00,
		BgL_bgl_za762walk2za712za7d2sw4113za7,
		BGl_z62walk2z12zd2switch2120za2zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4002z00zzast_walkz00,
		BgL_bgl_za762walk3za712za7d2sw4114za7,
		BGl_z62walk3z12zd2switch2122za2zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4003z00zzast_walkz00,
		BgL_bgl_za762walk0za7d2letza7d4115za7,
		BGl_z62walk0zd2letzd2fun2124z62zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4004z00zzast_walkz00,
		BgL_bgl_za762walk1za7d2letza7d4116za7,
		BGl_z62walk1zd2letzd2fun2126z62zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4005z00zzast_walkz00,
		BgL_bgl_za762walk2za7d2letza7d4117za7,
		BGl_z62walk2zd2letzd2fun2128z62zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4006z00zzast_walkz00,
		BgL_bgl_za762walk3za7d2letza7d4118za7,
		BGl_z62walk3zd2letzd2fun2130z62zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4007z00zzast_walkz00,
		BgL_bgl_za762walk0za7a2za7d2le4119za7,
		BGl_z62walk0za2zd2letzd2fun2132zc0zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4008z00zzast_walkz00,
		BgL_bgl_za762walk1za7a2za7d2le4120za7,
		BGl_z62walk1za2zd2letzd2fun2134zc0zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4009z00zzast_walkz00,
		BgL_bgl_za762walk2za7a2za7d2le4121za7,
		BGl_z62walk2za2zd2letzd2fun2136zc0zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4010z00zzast_walkz00,
		BgL_bgl_za762walk3za7a2za7d2le4122za7,
		BGl_z62walk3za2zd2letzd2fun2138zc0zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4011z00zzast_walkz00,
		BgL_bgl_za762walk0za712za7d2le4123za7,
		BGl_z62walk0z12zd2letzd2fun2140z70zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4012z00zzast_walkz00,
		BgL_bgl_za762walk1za712za7d2le4124za7,
		BGl_z62walk1z12zd2letzd2fun2142z70zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4013z00zzast_walkz00,
		BgL_bgl_za762walk2za712za7d2le4125za7,
		BGl_z62walk2z12zd2letzd2fun2144z70zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4014z00zzast_walkz00,
		BgL_bgl_za762walk3za712za7d2le4126za7,
		BGl_z62walk3z12zd2letzd2fun2146z70zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4015z00zzast_walkz00,
		BgL_bgl_za762walk0za7d2letza7d4127za7,
		BGl_z62walk0zd2letzd2var2148z62zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4016z00zzast_walkz00,
		BgL_bgl_za762walk1za7d2letza7d4128za7,
		BGl_z62walk1zd2letzd2var2150z62zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4017z00zzast_walkz00,
		BgL_bgl_za762walk2za7d2letza7d4129za7,
		BGl_z62walk2zd2letzd2var2152z62zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4018z00zzast_walkz00,
		BgL_bgl_za762walk3za7d2letza7d4130za7,
		BGl_z62walk3zd2letzd2var2154z62zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_walk2za2zd2envz70zzast_walkz00,
		BgL_bgl_za762walk2za7a2za7c0za7za74131za7, BGl_z62walk2za2zc0zzast_walkz00,
		0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4019z00zzast_walkz00,
		BgL_bgl_za762walk0za7a2za7d2le4132za7,
		BGl_z62walk0za2zd2letzd2var2157zc0zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string4027z00zzast_walkz00,
		BgL_bgl_string4027za700za7za7a4133za7, "ast_walk", 8);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_walk3z12zd2envzc0zzast_walkz00,
		BgL_bgl_za762walk3za712za770za7za74134za7, BGl_z62walk3z12z70zzast_walkz00,
		0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4020z00zzast_walkz00,
		BgL_bgl_za762walk1za7a2za7d2le4135za7,
		BGl_z62walk1za2zd2letzd2var2159zc0zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4021z00zzast_walkz00,
		BgL_bgl_za762walk2za7a2za7d2le4136za7,
		BGl_z62walk2za2zd2letzd2var2161zc0zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4022z00zzast_walkz00,
		BgL_bgl_za762walk3za7a2za7d2le4137za7,
		BGl_z62walk3za2zd2letzd2var2163zc0zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4023z00zzast_walkz00,
		BgL_bgl_za762walk0za712za7d2le4138za7,
		BGl_z62walk0z12zd2letzd2var2165z70zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4024z00zzast_walkz00,
		BgL_bgl_za762walk1za712za7d2le4139za7,
		BGl_z62walk1z12zd2letzd2var2167z70zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4025z00zzast_walkz00,
		BgL_bgl_za762walk2za712za7d2le4140za7,
		BGl_z62walk2z12zd2letzd2var2169z70zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc4026z00zzast_walkz00,
		BgL_bgl_za762walk3za712za7d2le4141za7,
		BGl_z62walk3z12zd2letzd2var2171z70zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_walk3za2zd2envz70zzast_walkz00,
		BgL_bgl_za762walk3za7a2za7c0za7za74142za7, BGl_z62walk3za2zc0zzast_walkz00,
		0L, BUNSPEC, 5);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_walk4z12zd2envzc0zzast_walkz00,
		BgL_bgl_za762walk4za712za770za7za74143za7, BGl_z62walk4z12z70zzast_walkz00,
		0L, BUNSPEC, 6);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_walk0zd2envzd2zzast_walkz00,
		BgL_bgl_za762walk0za762za7za7ast4144z00, BGl_z62walk0z62zzast_walkz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_walk4za2zd2envz70zzast_walkz00,
		BgL_bgl_za762walk4za7a2za7c0za7za74145za7, BGl_z62walk4za2zc0zzast_walkz00,
		0L, BUNSPEC, 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3800z00zzast_walkz00,
		BgL_bgl_za762walk1za7d2app174146z00, BGl_z62walk1zd2app1714zb0zzast_walkz00,
		0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3801z00zzast_walkz00,
		BgL_bgl_za762walk2za7d2app174147z00, BGl_z62walk2zd2app1716zb0zzast_walkz00,
		0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3802z00zzast_walkz00,
		BgL_bgl_za762walk3za7d2app174148z00, BGl_z62walk3zd2app1718zb0zzast_walkz00,
		0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3803z00zzast_walkz00,
		BgL_bgl_za762walk0za7a2za7d2ap4149za7,
		BGl_z62walk0za2zd2app1720z12zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3804z00zzast_walkz00,
		BgL_bgl_za762walk1za7a2za7d2ap4150za7,
		BGl_z62walk1za2zd2app1722z12zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3805z00zzast_walkz00,
		BgL_bgl_za762walk2za7a2za7d2ap4151za7,
		BGl_z62walk2za2zd2app1724z12zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3806z00zzast_walkz00,
		BgL_bgl_za762walk3za7a2za7d2ap4152za7,
		BGl_z62walk3za2zd2app1726z12zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STRING(BGl_string3731z00zzast_walkz00,
		BgL_bgl_string3731za700za7za7a4153za7, "walk01630", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3807z00zzast_walkz00,
		BgL_bgl_za762walk0za712za7d2ap4154za7,
		BGl_z62walk0z12zd2app1728za2zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3808z00zzast_walkz00,
		BgL_bgl_za762walk1za712za7d2ap4155za7,
		BGl_z62walk1z12zd2app1730za2zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string3733z00zzast_walkz00,
		BgL_bgl_string3733za700za7za7a4156za7, "walk11632", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3809z00zzast_walkz00,
		BgL_bgl_za762walk2za712za7d2ap4157za7,
		BGl_z62walk2z12zd2app1732za2zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string3735z00zzast_walkz00,
		BgL_bgl_string3735za700za7za7a4158za7, "walk21634", 9);
	      DEFINE_STRING(BGl_string3737z00zzast_walkz00,
		BgL_bgl_string3737za700za7za7a4159za7, "walk31636", 9);
	      DEFINE_STRING(BGl_string3739z00zzast_walkz00,
		BgL_bgl_string3739za700za7za7a4160za7, "walk41639", 9);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_walk1zd2envzd2zzast_walkz00,
		BgL_bgl_za762walk1za762za7za7ast4161z00, BGl_z62walk1z62zzast_walkz00, 0L,
		BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3810z00zzast_walkz00,
		BgL_bgl_za762walk3za712za7d2ap4162za7,
		BGl_z62walk3z12zd2app1735za2zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3811z00zzast_walkz00,
		BgL_bgl_za762walk0za7d2appza7d4163za7,
		BGl_z62walk0zd2appzd2ly1737z62zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3730z00zzast_walkz00,
		BgL_bgl_za762walk01630za762za74164za7, BGl_z62walk01630z62zzast_walkz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3812z00zzast_walkz00,
		BgL_bgl_za762walk1za7d2appza7d4165za7,
		BGl_z62walk1zd2appzd2ly1739z62zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3813z00zzast_walkz00,
		BgL_bgl_za762walk2za7d2appza7d4166za7,
		BGl_z62walk2zd2appzd2ly1741z62zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3732z00zzast_walkz00,
		BgL_bgl_za762walk11632za762za74167za7, BGl_z62walk11632z62zzast_walkz00, 0L,
		BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3814z00zzast_walkz00,
		BgL_bgl_za762walk3za7d2appza7d4168za7,
		BGl_z62walk3zd2appzd2ly1743z62zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3815z00zzast_walkz00,
		BgL_bgl_za762walk0za7a2za7d2ap4169za7,
		BGl_z62walk0za2zd2appzd2ly1745zc0zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3734z00zzast_walkz00,
		BgL_bgl_za762walk21634za762za74170za7, BGl_z62walk21634z62zzast_walkz00, 0L,
		BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3816z00zzast_walkz00,
		BgL_bgl_za762walk1za7a2za7d2ap4171za7,
		BGl_z62walk1za2zd2appzd2ly1747zc0zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string3741z00zzast_walkz00,
		BgL_bgl_string3741za700za7za7a4172za7, "walk0*1641", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3817z00zzast_walkz00,
		BgL_bgl_za762walk2za7a2za7d2ap4173za7,
		BGl_z62walk2za2zd2appzd2ly1749zc0zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3736z00zzast_walkz00,
		BgL_bgl_za762walk31636za762za74174za7, BGl_z62walk31636z62zzast_walkz00, 0L,
		BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3818z00zzast_walkz00,
		BgL_bgl_za762walk3za7a2za7d2ap4175za7,
		BGl_z62walk3za2zd2appzd2ly1751zc0zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STRING(BGl_string3743z00zzast_walkz00,
		BgL_bgl_string3743za700za7za7a4176za7, "walk1*1643", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3819z00zzast_walkz00,
		BgL_bgl_za762walk0za712za7d2ap4177za7,
		BGl_z62walk0z12zd2appzd2ly1753z70zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3738z00zzast_walkz00,
		BgL_bgl_za762walk41639za762za74178za7, BGl_z62walk41639z62zzast_walkz00, 0L,
		BUNSPEC, 6);
	      DEFINE_STRING(BGl_string3745z00zzast_walkz00,
		BgL_bgl_string3745za700za7za7a4179za7, "walk2*1645", 10);
	      DEFINE_STRING(BGl_string3747z00zzast_walkz00,
		BgL_bgl_string3747za700za7za7a4180za7, "walk3*1647", 10);
	      DEFINE_STRING(BGl_string3749z00zzast_walkz00,
		BgL_bgl_string3749za700za7za7a4181za7, "walk4*1649", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3900z00zzast_walkz00,
		BgL_bgl_za762walk1za7a2za7d2se4182za7,
		BGl_z62walk1za2zd2setzd2exzd2it1915z12zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3901z00zzast_walkz00,
		BgL_bgl_za762walk2za7a2za7d2se4183za7,
		BGl_z62walk2za2zd2setzd2exzd2it1917z12zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3820z00zzast_walkz00,
		BgL_bgl_za762walk1za712za7d2ap4184za7,
		BGl_z62walk1z12zd2appzd2ly1755z70zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3902z00zzast_walkz00,
		BgL_bgl_za762walk3za7a2za7d2se4185za7,
		BGl_z62walk3za2zd2setzd2exzd2it1919z12zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3821z00zzast_walkz00,
		BgL_bgl_za762walk2za712za7d2ap4186za7,
		BGl_z62walk2z12zd2appzd2ly1757z70zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3740z00zzast_walkz00,
		BgL_bgl_za762walk0za7a21641za74187za7, BGl_z62walk0za21641zc0zzast_walkz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3903z00zzast_walkz00,
		BgL_bgl_za762walk0za712za7d2se4188za7,
		BGl_z62walk0z12zd2setzd2exzd2it1921za2zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3822z00zzast_walkz00,
		BgL_bgl_za762walk3za712za7d2ap4189za7,
		BGl_z62walk3z12zd2appzd2ly1759z70zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3904z00zzast_walkz00,
		BgL_bgl_za762walk1za712za7d2se4190za7,
		BGl_z62walk1z12zd2setzd2exzd2it1923za2zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3823z00zzast_walkz00,
		BgL_bgl_za762walk0za7d2funca4191z00,
		BGl_z62walk0zd2funcall1761zb0zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3742z00zzast_walkz00,
		BgL_bgl_za762walk1za7a21643za74192za7, BGl_z62walk1za21643zc0zzast_walkz00,
		0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3905z00zzast_walkz00,
		BgL_bgl_za762walk2za712za7d2se4193za7,
		BGl_z62walk2z12zd2setzd2exzd2it1925za2zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3824z00zzast_walkz00,
		BgL_bgl_za762walk1za7d2funca4194z00,
		BGl_z62walk1zd2funcall1763zb0zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3906z00zzast_walkz00,
		BgL_bgl_za762walk3za712za7d2se4195za7,
		BGl_z62walk3z12zd2setzd2exzd2it1927za2zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3825z00zzast_walkz00,
		BgL_bgl_za762walk2za7d2funca4196z00,
		BGl_z62walk2zd2funcall1765zb0zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3744z00zzast_walkz00,
		BgL_bgl_za762walk2za7a21645za74197za7, BGl_z62walk2za21645zc0zzast_walkz00,
		0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3907z00zzast_walkz00,
		BgL_bgl_za762walk0za7d2jumpza74198za7,
		BGl_z62walk0zd2jumpzd2exzd2it1929zb0zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3826z00zzast_walkz00,
		BgL_bgl_za762walk3za7d2funca4199z00,
		BGl_z62walk3zd2funcall1767zb0zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STRING(BGl_string3751z00zzast_walkz00,
		BgL_bgl_string3751za700za7za7a4200za7, "walk0!1651", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3908z00zzast_walkz00,
		BgL_bgl_za762walk1za7d2jumpza74201za7,
		BGl_z62walk1zd2jumpzd2exzd2it1931zb0zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3827z00zzast_walkz00,
		BgL_bgl_za762walk0za7a2za7d2fu4202za7,
		BGl_z62walk0za2zd2funcall1769z12zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3746z00zzast_walkz00,
		BgL_bgl_za762walk3za7a21647za74203za7, BGl_z62walk3za21647zc0zzast_walkz00,
		0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3909z00zzast_walkz00,
		BgL_bgl_za762walk2za7d2jumpza74204za7,
		BGl_z62walk2zd2jumpzd2exzd2it1933zb0zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3828z00zzast_walkz00,
		BgL_bgl_za762walk1za7a2za7d2fu4205za7,
		BGl_z62walk1za2zd2funcall1771z12zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string3753z00zzast_walkz00,
		BgL_bgl_string3753za700za7za7a4206za7, "walk1!1653", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3829z00zzast_walkz00,
		BgL_bgl_za762walk2za7a2za7d2fu4207za7,
		BGl_z62walk2za2zd2funcall1773z12zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3748z00zzast_walkz00,
		BgL_bgl_za762walk4za7a21649za74208za7, BGl_z62walk4za21649zc0zzast_walkz00,
		0L, BUNSPEC, 6);
	      DEFINE_STRING(BGl_string3755z00zzast_walkz00,
		BgL_bgl_string3755za700za7za7a4209za7, "walk2!1655", 10);
	      DEFINE_STRING(BGl_string3757z00zzast_walkz00,
		BgL_bgl_string3757za700za7za7a4210za7, "walk3!1658", 10);
	      DEFINE_STRING(BGl_string3759z00zzast_walkz00,
		BgL_bgl_string3759za700za7za7a4211za7, "walk4!1660", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3910z00zzast_walkz00,
		BgL_bgl_za762walk3za7d2jumpza74212za7,
		BGl_z62walk3zd2jumpzd2exzd2it1935zb0zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3911z00zzast_walkz00,
		BgL_bgl_za762walk0za7a2za7d2ju4213za7,
		BGl_z62walk0za2zd2jumpzd2exzd2it1937z12zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3830z00zzast_walkz00,
		BgL_bgl_za762walk3za7a2za7d2fu4214za7,
		BGl_z62walk3za2zd2funcall1775z12zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3912z00zzast_walkz00,
		BgL_bgl_za762walk1za7a2za7d2ju4215za7,
		BGl_z62walk1za2zd2jumpzd2exzd2it1939z12zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3831z00zzast_walkz00,
		BgL_bgl_za762walk0za712za7d2fu4216za7,
		BGl_z62walk0z12zd2funcall1777za2zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3750z00zzast_walkz00,
		BgL_bgl_za762walk0za7121651za74217za7, BGl_z62walk0z121651z70zzast_walkz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3913z00zzast_walkz00,
		BgL_bgl_za762walk2za7a2za7d2ju4218za7,
		BGl_z62walk2za2zd2jumpzd2exzd2it1941z12zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3832z00zzast_walkz00,
		BgL_bgl_za762walk1za712za7d2fu4219za7,
		BGl_z62walk1z12zd2funcall1779za2zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3914z00zzast_walkz00,
		BgL_bgl_za762walk3za7a2za7d2ju4220za7,
		BGl_z62walk3za2zd2jumpzd2exzd2it1943z12zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3833z00zzast_walkz00,
		BgL_bgl_za762walk2za712za7d2fu4221za7,
		BGl_z62walk2z12zd2funcall1781za2zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3752z00zzast_walkz00,
		BgL_bgl_za762walk1za7121653za74222za7, BGl_z62walk1z121653z70zzast_walkz00,
		0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3915z00zzast_walkz00,
		BgL_bgl_za762walk0za712za7d2ju4223za7,
		BGl_z62walk0z12zd2jumpzd2exzd2it1945za2zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3834z00zzast_walkz00,
		BgL_bgl_za762walk3za712za7d2fu4224za7,
		BGl_z62walk3z12zd2funcall1783za2zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3916z00zzast_walkz00,
		BgL_bgl_za762walk1za712za7d2ju4225za7,
		BGl_z62walk1z12zd2jumpzd2exzd2it1947za2zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3835z00zzast_walkz00,
		BgL_bgl_za762walk0za7d2exter4226z00,
		BGl_z62walk0zd2extern1785zb0zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3760z00zzast_walkz00,
		BgL_bgl_string3760za700za7za7a4227za7, "walk4!", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3754z00zzast_walkz00,
		BgL_bgl_za762walk2za7121655za74228za7, BGl_z62walk2z121655z70zzast_walkz00,
		0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3917z00zzast_walkz00,
		BgL_bgl_za762walk2za712za7d2ju4229za7,
		BGl_z62walk2z12zd2jumpzd2exzd2it1949za2zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3836z00zzast_walkz00,
		BgL_bgl_za762walk1za7d2exter4230z00,
		BGl_z62walk1zd2extern1787zb0zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string3761z00zzast_walkz00,
		BgL_bgl_string3761za700za7za7a4231za7, "Internal Error: forgot Node type",
		32);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3918z00zzast_walkz00,
		BgL_bgl_za762walk3za712za7d2ju4232za7,
		BGl_z62walk3z12zd2jumpzd2exzd2it1951za2zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3837z00zzast_walkz00,
		BgL_bgl_za762walk2za7d2exter4233z00,
		BGl_z62walk2zd2extern1789zb0zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string3762z00zzast_walkz00,
		BgL_bgl_string3762za700za7za7a4234za7, "walk3!", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3756z00zzast_walkz00,
		BgL_bgl_za762walk3za7121658za74235za7, BGl_z62walk3z121658z70zzast_walkz00,
		0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3919z00zzast_walkz00,
		BgL_bgl_za762walk0za7d2retbl4236z00,
		BGl_z62walk0zd2retblock1953zb0zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3838z00zzast_walkz00,
		BgL_bgl_za762walk3za7d2exter4237z00,
		BGl_z62walk3zd2extern1791zb0zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STRING(BGl_string3763z00zzast_walkz00,
		BgL_bgl_string3763za700za7za7a4238za7, "walk2!", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3839z00zzast_walkz00,
		BgL_bgl_za762walk0za7a2za7d2ex4239za7,
		BGl_z62walk0za2zd2extern1793z12zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3764z00zzast_walkz00,
		BgL_bgl_string3764za700za7za7a4240za7, "walk1!", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3758z00zzast_walkz00,
		BgL_bgl_za762walk4za7121660za74241za7, BGl_z62walk4z121660z70zzast_walkz00,
		0L, BUNSPEC, 6);
	      DEFINE_STRING(BGl_string3765z00zzast_walkz00,
		BgL_bgl_string3765za700za7za7a4242za7, "walk0!", 6);
	      DEFINE_STRING(BGl_string3766z00zzast_walkz00,
		BgL_bgl_string3766za700za7za7a4243za7, "walk3*", 6);
	      DEFINE_STRING(BGl_string3767z00zzast_walkz00,
		BgL_bgl_string3767za700za7za7a4244za7, "walk2*", 6);
	      DEFINE_STRING(BGl_string3768z00zzast_walkz00,
		BgL_bgl_string3768za700za7za7a4245za7, "walk1*", 6);
	      DEFINE_STRING(BGl_string3769z00zzast_walkz00,
		BgL_bgl_string3769za700za7za7a4246za7, "walk0*", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3920z00zzast_walkz00,
		BgL_bgl_za762walk1za7d2retbl4247z00,
		BGl_z62walk1zd2retblock1955zb0zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3921z00zzast_walkz00,
		BgL_bgl_za762walk2za7d2retbl4248z00,
		BGl_z62walk2zd2retblock1957zb0zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3840z00zzast_walkz00,
		BgL_bgl_za762walk1za7a2za7d2ex4249za7,
		BGl_z62walk1za2zd2extern1795z12zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3922z00zzast_walkz00,
		BgL_bgl_za762walk3za7d2retbl4250z00,
		BGl_z62walk3zd2retblock1959zb0zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3841z00zzast_walkz00,
		BgL_bgl_za762walk2za7a2za7d2ex4251za7,
		BGl_z62walk2za2zd2extern1797z12zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3923z00zzast_walkz00,
		BgL_bgl_za762walk0za7a2za7d2re4252za7,
		BGl_z62walk0za2zd2retblock1961z12zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3842z00zzast_walkz00,
		BgL_bgl_za762walk3za7a2za7d2ex4253za7,
		BGl_z62walk3za2zd2extern1799z12zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3924z00zzast_walkz00,
		BgL_bgl_za762walk1za7a2za7d2re4254za7,
		BGl_z62walk1za2zd2retblock1963z12zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3843z00zzast_walkz00,
		BgL_bgl_za762walk0za712za7d2ex4255za7,
		BGl_z62walk0z12zd2extern1801za2zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3925z00zzast_walkz00,
		BgL_bgl_za762walk2za7a2za7d2re4256za7,
		BGl_z62walk2za2zd2retblock1965z12zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3844z00zzast_walkz00,
		BgL_bgl_za762walk1za712za7d2ex4257za7,
		BGl_z62walk1z12zd2extern1803za2zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3926z00zzast_walkz00,
		BgL_bgl_za762walk3za7a2za7d2re4258za7,
		BGl_z62walk3za2zd2retblock1967z12zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3845z00zzast_walkz00,
		BgL_bgl_za762walk2za712za7d2ex4259za7,
		BGl_z62walk2z12zd2extern1805za2zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string3770z00zzast_walkz00,
		BgL_bgl_string3770za700za7za7a4260za7, "walk4", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3927z00zzast_walkz00,
		BgL_bgl_za762walk0za712za7d2re4261za7,
		BGl_z62walk0z12zd2retblock1969za2zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3846z00zzast_walkz00,
		BgL_bgl_za762walk3za712za7d2ex4262za7,
		BGl_z62walk3z12zd2extern1807za2zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STRING(BGl_string3771z00zzast_walkz00,
		BgL_bgl_string3771za700za7za7a4263za7, "walk3", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3928z00zzast_walkz00,
		BgL_bgl_za762walk1za712za7d2re4264za7,
		BGl_z62walk1z12zd2retblock1971za2zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3847z00zzast_walkz00,
		BgL_bgl_za762walk0za7d2cast14265z00,
		BGl_z62walk0zd2cast1809zb0zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3772z00zzast_walkz00,
		BgL_bgl_string3772za700za7za7a4266za7, "walk2", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3929z00zzast_walkz00,
		BgL_bgl_za762walk2za712za7d2re4267za7,
		BGl_z62walk2z12zd2retblock1973za2zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3848z00zzast_walkz00,
		BgL_bgl_za762walk1za7d2cast14268z00,
		BGl_z62walk1zd2cast1811zb0zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string3773z00zzast_walkz00,
		BgL_bgl_string3773za700za7za7a4269za7, "walk1", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3849z00zzast_walkz00,
		BgL_bgl_za762walk2za7d2cast14270z00,
		BGl_z62walk2zd2cast1813zb0zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string3774z00zzast_walkz00,
		BgL_bgl_string3774za700za7za7a4271za7, "walk0", 5);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_walk2zd2envzd2zzast_walkz00,
		BgL_bgl_za762walk2za762za7za7ast4272z00, BGl_z62walk2z62zzast_walkz00, 0L,
		BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3930z00zzast_walkz00,
		BgL_bgl_za762walk3za712za7d2re4273za7,
		BGl_z62walk3z12zd2retblock1975za2zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3931z00zzast_walkz00,
		BgL_bgl_za762walk0za7d2retur4274z00,
		BGl_z62walk0zd2return1977zb0zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3850z00zzast_walkz00,
		BgL_bgl_za762walk3za7d2cast14275z00,
		BGl_z62walk3zd2cast1815zb0zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3932z00zzast_walkz00,
		BgL_bgl_za762walk1za7d2retur4276z00,
		BGl_z62walk1zd2return1979zb0zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3851z00zzast_walkz00,
		BgL_bgl_za762walk0za7a2za7d2ca4277za7,
		BGl_z62walk0za2zd2cast1817z12zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3933z00zzast_walkz00,
		BgL_bgl_za762walk2za7d2retur4278z00,
		BGl_z62walk2zd2return1981zb0zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3852z00zzast_walkz00,
		BgL_bgl_za762walk1za7a2za7d2ca4279za7,
		BGl_z62walk1za2zd2cast1819z12zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3934z00zzast_walkz00,
		BgL_bgl_za762walk3za7d2retur4280z00,
		BGl_z62walk3zd2return1983zb0zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3853z00zzast_walkz00,
		BgL_bgl_za762walk2za7a2za7d2ca4281za7,
		BGl_z62walk2za2zd2cast1821z12zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3935z00zzast_walkz00,
		BgL_bgl_za762walk0za7a2za7d2re4282za7,
		BGl_z62walk0za2zd2return1985z12zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3854z00zzast_walkz00,
		BgL_bgl_za762walk3za7a2za7d2ca4283za7,
		BGl_z62walk3za2zd2cast1823z12zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3936z00zzast_walkz00,
		BgL_bgl_za762walk1za7a2za7d2re4284za7,
		BGl_z62walk1za2zd2return1987z12zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3855z00zzast_walkz00,
		BgL_bgl_za762walk0za712za7d2ca4285za7,
		BGl_z62walk0z12zd2cast1825za2zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3937z00zzast_walkz00,
		BgL_bgl_za762walk2za7a2za7d2re4286za7,
		BGl_z62walk2za2zd2return1989z12zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3856z00zzast_walkz00,
		BgL_bgl_za762walk1za712za7d2ca4287za7,
		BGl_z62walk1z12zd2cast1827za2zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3775z00zzast_walkz00,
		BgL_bgl_za762walk0za7d2node14288z00,
		BGl_z62walk0zd2node1663zb0zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3938z00zzast_walkz00,
		BgL_bgl_za762walk3za7a2za7d2re4289za7,
		BGl_z62walk3za2zd2return1991z12zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3857z00zzast_walkz00,
		BgL_bgl_za762walk2za712za7d2ca4290za7,
		BGl_z62walk2z12zd2cast1829za2zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3776z00zzast_walkz00,
		BgL_bgl_za762walk1za7d2node14291z00,
		BGl_z62walk1zd2node1665zb0zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3939z00zzast_walkz00,
		BgL_bgl_za762walk0za712za7d2re4292za7,
		BGl_z62walk0z12zd2return1993za2zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3858z00zzast_walkz00,
		BgL_bgl_za762walk3za712za7d2ca4293za7,
		BGl_z62walk3z12zd2cast1831za2zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3777z00zzast_walkz00,
		BgL_bgl_za762walk2za7d2node14294z00,
		BGl_z62walk2zd2node1667zb0zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3859z00zzast_walkz00,
		BgL_bgl_za762walk0za7d2setq14295z00,
		BGl_z62walk0zd2setq1833zb0zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3778z00zzast_walkz00,
		BgL_bgl_za762walk3za7d2node14296z00,
		BGl_z62walk3zd2node1669zb0zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3779z00zzast_walkz00,
		BgL_bgl_za762walk0za7a2za7d2no4297za7,
		BGl_z62walk0za2zd2node1671z12zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_walk0z12zd2envzc0zzast_walkz00,
		BgL_bgl_za762walk0za712za770za7za74298za7, BGl_z62walk0z12z70zzast_walkz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3940z00zzast_walkz00,
		BgL_bgl_za762walk1za712za7d2re4299za7,
		BGl_z62walk1z12zd2return1995za2zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3941z00zzast_walkz00,
		BgL_bgl_za762walk2za712za7d2re4300za7,
		BGl_z62walk2z12zd2return1997za2zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3860z00zzast_walkz00,
		BgL_bgl_za762walk1za7d2setq14301z00,
		BGl_z62walk1zd2setq1835zb0zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3942z00zzast_walkz00,
		BgL_bgl_za762walk3za712za7d2re4302za7,
		BGl_z62walk3z12zd2return1999za2zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3861z00zzast_walkz00,
		BgL_bgl_za762walk2za7d2setq14303z00,
		BGl_z62walk2zd2setq1837zb0zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3780z00zzast_walkz00,
		BgL_bgl_za762walk1za7a2za7d2no4304za7,
		BGl_z62walk1za2zd2node1673z12zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3943z00zzast_walkz00,
		BgL_bgl_za762walk0za7d2makeza74305za7,
		BGl_z62walk0zd2makezd2box2001z62zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3862z00zzast_walkz00,
		BgL_bgl_za762walk3za7d2setq14306z00,
		BGl_z62walk3zd2setq1839zb0zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3781z00zzast_walkz00,
		BgL_bgl_za762walk2za7a2za7d2no4307za7,
		BGl_z62walk2za2zd2node1675z12zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3944z00zzast_walkz00,
		BgL_bgl_za762walk1za7d2makeza74308za7,
		BGl_z62walk1zd2makezd2box2003z62zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3863z00zzast_walkz00,
		BgL_bgl_za762walk0za7a2za7d2se4309za7,
		BGl_z62walk0za2zd2setq1841z12zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3782z00zzast_walkz00,
		BgL_bgl_za762walk3za7a2za7d2no4310za7,
		BGl_z62walk3za2zd2node1677z12zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3945z00zzast_walkz00,
		BgL_bgl_za762walk2za7d2makeza74311za7,
		BGl_z62walk2zd2makezd2box2005z62zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3864z00zzast_walkz00,
		BgL_bgl_za762walk1za7a2za7d2se4312za7,
		BGl_z62walk1za2zd2setq1843z12zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3783z00zzast_walkz00,
		BgL_bgl_za762walk0za712za7d2no4313za7,
		BGl_z62walk0z12zd2node1679za2zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3946z00zzast_walkz00,
		BgL_bgl_za762walk3za7d2makeza74314za7,
		BGl_z62walk3zd2makezd2box2007z62zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3865z00zzast_walkz00,
		BgL_bgl_za762walk2za7a2za7d2se4315za7,
		BGl_z62walk2za2zd2setq1845z12zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3784z00zzast_walkz00,
		BgL_bgl_za762walk1za712za7d2no4316za7,
		BGl_z62walk1z12zd2node1681za2zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3947z00zzast_walkz00,
		BgL_bgl_za762walk0za7a2za7d2ma4317za7,
		BGl_z62walk0za2zd2makezd2box2009zc0zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3866z00zzast_walkz00,
		BgL_bgl_za762walk3za7a2za7d2se4318za7,
		BGl_z62walk3za2zd2setq1847z12zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3785z00zzast_walkz00,
		BgL_bgl_za762walk2za712za7d2no4319za7,
		BGl_z62walk2z12zd2node1683za2zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3948z00zzast_walkz00,
		BgL_bgl_za762walk1za7a2za7d2ma4320za7,
		BGl_z62walk1za2zd2makezd2box2012zc0zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3867z00zzast_walkz00,
		BgL_bgl_za762walk0za712za7d2se4321za7,
		BGl_z62walk0z12zd2setq1849za2zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3786z00zzast_walkz00,
		BgL_bgl_za762walk3za712za7d2no4322za7,
		BGl_z62walk3z12zd2node1685za2zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3949z00zzast_walkz00,
		BgL_bgl_za762walk2za7a2za7d2ma4323za7,
		BGl_z62walk2za2zd2makezd2box2014zc0zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3868z00zzast_walkz00,
		BgL_bgl_za762walk1za712za7d2se4324za7,
		BGl_z62walk1z12zd2setq1851za2zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3787z00zzast_walkz00,
		BgL_bgl_za762walk0za7d2seque4325z00,
		BGl_z62walk0zd2sequence1687zb0zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3869z00zzast_walkz00,
		BgL_bgl_za762walk2za712za7d2se4326za7,
		BGl_z62walk2z12zd2setq1853za2zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3788z00zzast_walkz00,
		BgL_bgl_za762walk1za7d2seque4327z00,
		BGl_z62walk1zd2sequence1689zb0zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3789z00zzast_walkz00,
		BgL_bgl_za762walk2za7d2seque4328z00,
		BGl_z62walk2zd2sequence1691zb0zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3950z00zzast_walkz00,
		BgL_bgl_za762walk3za7a2za7d2ma4329za7,
		BGl_z62walk3za2zd2makezd2box2016zc0zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3951z00zzast_walkz00,
		BgL_bgl_za762walk0za712za7d2ma4330za7,
		BGl_z62walk0z12zd2makezd2box2018z70zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3870z00zzast_walkz00,
		BgL_bgl_za762walk3za712za7d2se4331za7,
		BGl_z62walk3z12zd2setq1855za2zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3952z00zzast_walkz00,
		BgL_bgl_za762walk1za712za7d2ma4332za7,
		BGl_z62walk1z12zd2makezd2box2020z70zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3871z00zzast_walkz00,
		BgL_bgl_za762walk0za7d2condi4333z00,
		BGl_z62walk0zd2conditional1857zb0zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3790z00zzast_walkz00,
		BgL_bgl_za762walk3za7d2seque4334z00,
		BGl_z62walk3zd2sequence1693zb0zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3953z00zzast_walkz00,
		BgL_bgl_za762walk2za712za7d2ma4335za7,
		BGl_z62walk2z12zd2makezd2box2022z70zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3872z00zzast_walkz00,
		BgL_bgl_za762walk1za7d2condi4336z00,
		BGl_z62walk1zd2conditional1859zb0zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3791z00zzast_walkz00,
		BgL_bgl_za762walk0za7a2za7d2se4337za7,
		BGl_z62walk0za2zd2sequence1695z12zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3954z00zzast_walkz00,
		BgL_bgl_za762walk3za712za7d2ma4338za7,
		BGl_z62walk3z12zd2makezd2box2024z70zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3873z00zzast_walkz00,
		BgL_bgl_za762walk2za7d2condi4339z00,
		BGl_z62walk2zd2conditional1861zb0zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3792z00zzast_walkz00,
		BgL_bgl_za762walk1za7a2za7d2se4340za7,
		BGl_z62walk1za2zd2sequence1697z12zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3955z00zzast_walkz00,
		BgL_bgl_za762walk0za7d2boxza7d4341za7,
		BGl_z62walk0zd2boxzd2ref2026z62zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3874z00zzast_walkz00,
		BgL_bgl_za762walk3za7d2condi4342z00,
		BGl_z62walk3zd2conditional1863zb0zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3793z00zzast_walkz00,
		BgL_bgl_za762walk2za7a2za7d2se4343za7,
		BGl_z62walk2za2zd2sequence1699z12zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3956z00zzast_walkz00,
		BgL_bgl_za762walk1za7d2boxza7d4344za7,
		BGl_z62walk1zd2boxzd2ref2028z62zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3875z00zzast_walkz00,
		BgL_bgl_za762walk0za7a2za7d2co4345za7,
		BGl_z62walk0za2zd2conditional1865z12zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3794z00zzast_walkz00,
		BgL_bgl_za762walk3za7a2za7d2se4346za7,
		BGl_z62walk3za2zd2sequence1701z12zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3957z00zzast_walkz00,
		BgL_bgl_za762walk2za7d2boxza7d4347za7,
		BGl_z62walk2zd2boxzd2ref2030z62zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3876z00zzast_walkz00,
		BgL_bgl_za762walk1za7a2za7d2co4348za7,
		BGl_z62walk1za2zd2conditional1867z12zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3795z00zzast_walkz00,
		BgL_bgl_za762walk0za712za7d2se4349za7,
		BGl_z62walk0z12zd2sequence1703za2zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3958z00zzast_walkz00,
		BgL_bgl_za762walk3za7d2boxza7d4350za7,
		BGl_z62walk3zd2boxzd2ref2032z62zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3877z00zzast_walkz00,
		BgL_bgl_za762walk2za7a2za7d2co4351za7,
		BGl_z62walk2za2zd2conditional1869z12zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3796z00zzast_walkz00,
		BgL_bgl_za762walk1za712za7d2se4352za7,
		BGl_z62walk1z12zd2sequence1705za2zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3959z00zzast_walkz00,
		BgL_bgl_za762walk0za7a2za7d2bo4353za7,
		BGl_z62walk0za2zd2boxzd2ref2034zc0zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3878z00zzast_walkz00,
		BgL_bgl_za762walk3za7a2za7d2co4354za7,
		BGl_z62walk3za2zd2conditional1871z12zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3797z00zzast_walkz00,
		BgL_bgl_za762walk2za712za7d2se4355za7,
		BGl_z62walk2z12zd2sequence1708za2zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3879z00zzast_walkz00,
		BgL_bgl_za762walk0za712za7d2co4356za7,
		BGl_z62walk0z12zd2conditional1873za2zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3798z00zzast_walkz00,
		BgL_bgl_za762walk3za712za7d2se4357za7,
		BGl_z62walk3z12zd2sequence1710za2zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3799z00zzast_walkz00,
		BgL_bgl_za762walk0za7d2app174358z00, BGl_z62walk0zd2app1712zb0zzast_walkz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3960z00zzast_walkz00,
		BgL_bgl_za762walk1za7a2za7d2bo4359za7,
		BGl_z62walk1za2zd2boxzd2ref2036zc0zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3961z00zzast_walkz00,
		BgL_bgl_za762walk2za7a2za7d2bo4360za7,
		BGl_z62walk2za2zd2boxzd2ref2038zc0zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3880z00zzast_walkz00,
		BgL_bgl_za762walk1za712za7d2co4361za7,
		BGl_z62walk1z12zd2conditional1875za2zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3962z00zzast_walkz00,
		BgL_bgl_za762walk3za7a2za7d2bo4362za7,
		BGl_z62walk3za2zd2boxzd2ref2040zc0zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3881z00zzast_walkz00,
		BgL_bgl_za762walk2za712za7d2co4363za7,
		BGl_z62walk2z12zd2conditional1877za2zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3963z00zzast_walkz00,
		BgL_bgl_za762walk0za712za7d2bo4364za7,
		BGl_z62walk0z12zd2boxzd2ref2043z70zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3882z00zzast_walkz00,
		BgL_bgl_za762walk3za712za7d2co4365za7,
		BGl_z62walk3z12zd2conditional1879za2zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3964z00zzast_walkz00,
		BgL_bgl_za762walk1za712za7d2bo4366za7,
		BGl_z62walk1z12zd2boxzd2ref2045z70zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3883z00zzast_walkz00,
		BgL_bgl_za762walk0za7d2fail14367z00,
		BGl_z62walk0zd2fail1881zb0zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3965z00zzast_walkz00,
		BgL_bgl_za762walk2za712za7d2bo4368za7,
		BGl_z62walk2z12zd2boxzd2ref2047z70zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3884z00zzast_walkz00,
		BgL_bgl_za762walk1za7d2fail14369z00,
		BGl_z62walk1zd2fail1883zb0zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3966z00zzast_walkz00,
		BgL_bgl_za762walk3za712za7d2bo4370za7,
		BGl_z62walk3z12zd2boxzd2ref2049z70zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3885z00zzast_walkz00,
		BgL_bgl_za762walk2za7d2fail14371z00,
		BGl_z62walk2zd2fail1885zb0zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3967z00zzast_walkz00,
		BgL_bgl_za762walk0za7d2boxza7d4372za7,
		BGl_z62walk0zd2boxzd2setz122051z70zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3886z00zzast_walkz00,
		BgL_bgl_za762walk3za7d2fail14373z00,
		BGl_z62walk3zd2fail1887zb0zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3968z00zzast_walkz00,
		BgL_bgl_za762walk1za7d2boxza7d4374za7,
		BGl_z62walk1zd2boxzd2setz122053z70zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3887z00zzast_walkz00,
		BgL_bgl_za762walk0za7a2za7d2fa4375za7,
		BGl_z62walk0za2zd2fail1889z12zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3969z00zzast_walkz00,
		BgL_bgl_za762walk2za7d2boxza7d4376za7,
		BGl_z62walk2zd2boxzd2setz122055z70zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3888z00zzast_walkz00,
		BgL_bgl_za762walk1za7a2za7d2fa4377za7,
		BGl_z62walk1za2zd2fail1891z12zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3889z00zzast_walkz00,
		BgL_bgl_za762walk2za7a2za7d2fa4378za7,
		BGl_z62walk2za2zd2fail1893z12zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_walk3zd2envzd2zzast_walkz00,
		BgL_bgl_za762walk3za762za7za7ast4379z00, BGl_z62walk3z62zzast_walkz00, 0L,
		BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3970z00zzast_walkz00,
		BgL_bgl_za762walk3za7d2boxza7d4380za7,
		BGl_z62walk3zd2boxzd2setz122058z70zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3971z00zzast_walkz00,
		BgL_bgl_za762walk0za7a2za7d2bo4381za7,
		BGl_z62walk0za2zd2boxzd2setz122060zd2zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3890z00zzast_walkz00,
		BgL_bgl_za762walk3za7a2za7d2fa4382za7,
		BGl_z62walk3za2zd2fail1895z12zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3972z00zzast_walkz00,
		BgL_bgl_za762walk1za7a2za7d2bo4383za7,
		BGl_z62walk1za2zd2boxzd2setz122062zd2zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3891z00zzast_walkz00,
		BgL_bgl_za762walk0za712za7d2fa4384za7,
		BGl_z62walk0z12zd2fail1897za2zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3973z00zzast_walkz00,
		BgL_bgl_za762walk2za7a2za7d2bo4385za7,
		BGl_z62walk2za2zd2boxzd2setz122064zd2zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3892z00zzast_walkz00,
		BgL_bgl_za762walk1za712za7d2fa4386za7,
		BGl_z62walk1z12zd2fail1899za2zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3974z00zzast_walkz00,
		BgL_bgl_za762walk3za7a2za7d2bo4387za7,
		BGl_z62walk3za2zd2boxzd2setz122066zd2zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3893z00zzast_walkz00,
		BgL_bgl_za762walk2za712za7d2fa4388za7,
		BGl_z62walk2z12zd2fail1901za2zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3975z00zzast_walkz00,
		BgL_bgl_za762walk0za712za7d2bo4389za7,
		BGl_z62walk0z12zd2boxzd2setz122068z62zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3894z00zzast_walkz00,
		BgL_bgl_za762walk3za712za7d2fa4390za7,
		BGl_z62walk3z12zd2fail1903za2zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3976z00zzast_walkz00,
		BgL_bgl_za762walk1za712za7d2bo4391za7,
		BGl_z62walk1z12zd2boxzd2setz122070z62zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3895z00zzast_walkz00,
		BgL_bgl_za762walk0za7d2setza7d4392za7,
		BGl_z62walk0zd2setzd2exzd2it1905zb0zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3977z00zzast_walkz00,
		BgL_bgl_za762walk2za712za7d2bo4393za7,
		BGl_z62walk2z12zd2boxzd2setz122072z62zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3896z00zzast_walkz00,
		BgL_bgl_za762walk1za7d2setza7d4394za7,
		BGl_z62walk1zd2setzd2exzd2it1907zb0zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3978z00zzast_walkz00,
		BgL_bgl_za762walk3za712za7d2bo4395za7,
		BGl_z62walk3z12zd2boxzd2setz122074z62zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3897z00zzast_walkz00,
		BgL_bgl_za762walk2za7d2setza7d4396za7,
		BGl_z62walk2zd2setzd2exzd2it1909zb0zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3979z00zzast_walkz00,
		BgL_bgl_za762walk0za7d2sync24397z00,
		BGl_z62walk0zd2sync2076zb0zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3898z00zzast_walkz00,
		BgL_bgl_za762walk3za7d2setza7d4398za7,
		BGl_z62walk3zd2setzd2exzd2it1911zb0zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3899z00zzast_walkz00,
		BgL_bgl_za762walk0za7a2za7d2se4399za7,
		BGl_z62walk0za2zd2setzd2exzd2it1913z12zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_walk0za2zd2envz70zzast_walkz00,
		BgL_bgl_za762walk0za7a2za7c0za7za74400za7, BGl_z62walk0za2zc0zzast_walkz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3980z00zzast_walkz00,
		BgL_bgl_za762walk1za7d2sync24401z00,
		BGl_z62walk1zd2sync2078zb0zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3981z00zzast_walkz00,
		BgL_bgl_za762walk2za7d2sync24402z00,
		BGl_z62walk2zd2sync2080zb0zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3982z00zzast_walkz00,
		BgL_bgl_za762walk3za7d2sync24403z00,
		BGl_z62walk3zd2sync2082zb0zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3983z00zzast_walkz00,
		BgL_bgl_za762walk0za7a2za7d2sy4404za7,
		BGl_z62walk0za2zd2sync2084z12zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3984z00zzast_walkz00,
		BgL_bgl_za762walk1za7a2za7d2sy4405za7,
		BGl_z62walk1za2zd2sync2086z12zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3985z00zzast_walkz00,
		BgL_bgl_za762walk2za7a2za7d2sy4406za7,
		BGl_z62walk2za2zd2sync2088z12zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3986z00zzast_walkz00,
		BgL_bgl_za762walk3za7a2za7d2sy4407za7,
		BGl_z62walk3za2zd2sync2090z12zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3987z00zzast_walkz00,
		BgL_bgl_za762walk0za712za7d2sy4408za7,
		BGl_z62walk0z12zd2sync2092za2zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3988z00zzast_walkz00,
		BgL_bgl_za762walk1za712za7d2sy4409za7,
		BGl_z62walk1z12zd2sync2094za2zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3989z00zzast_walkz00,
		BgL_bgl_za762walk2za712za7d2sy4410za7,
		BGl_z62walk2z12zd2sync2096za2zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_walk1z12zd2envzc0zzast_walkz00,
		BgL_bgl_za762walk1za712za770za7za74411za7, BGl_z62walk1z12z70zzast_walkz00,
		0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3990z00zzast_walkz00,
		BgL_bgl_za762walk3za712za7d2sy4412za7,
		BGl_z62walk3z12zd2sync2098za2zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3991z00zzast_walkz00,
		BgL_bgl_za762walk0za7d2switc4413z00,
		BGl_z62walk0zd2switch2100zb0zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3992z00zzast_walkz00,
		BgL_bgl_za762walk1za7d2switc4414z00,
		BGl_z62walk1zd2switch2102zb0zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3993z00zzast_walkz00,
		BgL_bgl_za762walk2za7d2switc4415z00,
		BGl_z62walk2zd2switch2104zb0zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3994z00zzast_walkz00,
		BgL_bgl_za762walk3za7d2switc4416z00,
		BGl_z62walk3zd2switch2106zb0zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3995z00zzast_walkz00,
		BgL_bgl_za762walk0za7a2za7d2sw4417za7,
		BGl_z62walk0za2zd2switch2108z12zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3996z00zzast_walkz00,
		BgL_bgl_za762walk1za7a2za7d2sw4418za7,
		BGl_z62walk1za2zd2switch2110z12zzast_walkz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3997z00zzast_walkz00,
		BgL_bgl_za762walk2za7a2za7d2sw4419za7,
		BGl_z62walk2za2zd2switch2112z12zzast_walkz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3998z00zzast_walkz00,
		BgL_bgl_za762walk3za7a2za7d2sw4420za7,
		BGl_z62walk3za2zd2switch2114z12zzast_walkz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3999z00zzast_walkz00,
		BgL_bgl_za762walk0za712za7d2sw4421za7,
		BGl_z62walk0z12zd2switch2116za2zzast_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_walk4zd2envzd2zzast_walkz00,
		BgL_bgl_za762walk4za762za7za7ast4422z00, BGl_z62walk4z62zzast_walkz00, 0L,
		BUNSPEC, 6);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_walk1za2zd2envz70zzast_walkz00,
		BgL_bgl_za762walk1za7a2za7c0za7za74423za7, BGl_z62walk1za2zc0zzast_walkz00,
		0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_walk2z12zd2envzc0zzast_walkz00,
		BgL_bgl_za762walk2za712za770za7za74424za7, BGl_z62walk2z12z70zzast_walkz00,
		0L, BUNSPEC, 4);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzast_walkz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzast_walkz00(long
		BgL_checksumz00_9923, char *BgL_fromz00_9924)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_walkz00))
				{
					BGl_requirezd2initializa7ationz75zzast_walkz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_walkz00();
					BGl_libraryzd2moduleszd2initz00zzast_walkz00();
					BGl_importedzd2moduleszd2initz00zzast_walkz00();
					BGl_genericzd2initzd2zzast_walkz00();
					BGl_methodzd2initzd2zzast_walkz00();
					return BGl_toplevelzd2initzd2zzast_walkz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_walkz00(void)
	{
		{	/* Ast/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "ast_walk");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_walk");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "ast_walk");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "ast_walk");
			BGl_modulezd2initializa7ationz75zz__pp_circlez00(0L, "ast_walk");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "ast_walk");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "ast_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"ast_walk");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_walk");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_walkz00(void)
	{
		{	/* Ast/walk.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzast_walkz00(void)
	{
		{	/* Ast/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzast_walkz00(obj_t BgL_l1z00_1, obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_2286;

				BgL_headz00_2286 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_2287;
					obj_t BgL_tailz00_2288;

					BgL_prevz00_2287 = BgL_headz00_2286;
					BgL_tailz00_2288 = BgL_l1z00_1;
				BgL_loopz00_2289:
					if (PAIRP(BgL_tailz00_2288))
						{
							obj_t BgL_newzd2prevzd2_2291;

							BgL_newzd2prevzd2_2291 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_2288), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_2287, BgL_newzd2prevzd2_2291);
							{
								obj_t BgL_tailz00_9951;
								obj_t BgL_prevz00_9950;

								BgL_prevz00_9950 = BgL_newzd2prevzd2_2291;
								BgL_tailz00_9951 = CDR(BgL_tailz00_2288);
								BgL_tailz00_2288 = BgL_tailz00_9951;
								BgL_prevz00_2287 = BgL_prevz00_9950;
								goto BgL_loopz00_2289;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_2286);
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_walkz00(void)
	{
		{	/* Ast/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_walkz00(void)
	{
		{	/* Ast/walk.scm 15 */
			BGl_registerzd2genericz12zc0zz__objectz00(BGl_walk0zd2envzd2zzast_walkz00,
				BGl_proc3730z00zzast_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string3731z00zzast_walkz00);
			BGl_registerzd2genericz12zc0zz__objectz00(BGl_walk1zd2envzd2zzast_walkz00,
				BGl_proc3732z00zzast_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string3733z00zzast_walkz00);
			BGl_registerzd2genericz12zc0zz__objectz00(BGl_walk2zd2envzd2zzast_walkz00,
				BGl_proc3734z00zzast_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string3735z00zzast_walkz00);
			BGl_registerzd2genericz12zc0zz__objectz00(BGl_walk3zd2envzd2zzast_walkz00,
				BGl_proc3736z00zzast_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string3737z00zzast_walkz00);
			BGl_registerzd2genericz12zc0zz__objectz00(BGl_walk4zd2envzd2zzast_walkz00,
				BGl_proc3738z00zzast_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string3739z00zzast_walkz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_walk0za2zd2envz70zzast_walkz00, BGl_proc3740z00zzast_walkz00,
				BGl_nodez00zzast_nodez00, BGl_string3741z00zzast_walkz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_walk1za2zd2envz70zzast_walkz00, BGl_proc3742z00zzast_walkz00,
				BGl_nodez00zzast_nodez00, BGl_string3743z00zzast_walkz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_walk2za2zd2envz70zzast_walkz00, BGl_proc3744z00zzast_walkz00,
				BGl_nodez00zzast_nodez00, BGl_string3745z00zzast_walkz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_walk3za2zd2envz70zzast_walkz00, BGl_proc3746z00zzast_walkz00,
				BGl_nodez00zzast_nodez00, BGl_string3747z00zzast_walkz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_walk4za2zd2envz70zzast_walkz00, BGl_proc3748z00zzast_walkz00,
				BGl_nodez00zzast_nodez00, BGl_string3749z00zzast_walkz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_walk0z12zd2envzc0zzast_walkz00, BGl_proc3750z00zzast_walkz00,
				BGl_nodez00zzast_nodez00, BGl_string3751z00zzast_walkz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_walk1z12zd2envzc0zzast_walkz00, BGl_proc3752z00zzast_walkz00,
				BGl_nodez00zzast_nodez00, BGl_string3753z00zzast_walkz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_walk2z12zd2envzc0zzast_walkz00, BGl_proc3754z00zzast_walkz00,
				BGl_nodez00zzast_nodez00, BGl_string3755z00zzast_walkz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_walk3z12zd2envzc0zzast_walkz00, BGl_proc3756z00zzast_walkz00,
				BGl_nodez00zzast_nodez00, BGl_string3757z00zzast_walkz00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_walk4z12zd2envzc0zzast_walkz00, BGl_proc3758z00zzast_walkz00,
				BGl_nodez00zzast_nodez00, BGl_string3759z00zzast_walkz00);
		}

	}



/* &walk4!1660 */
	obj_t BGl_z62walk4z121660z70zzast_walkz00(obj_t BgL_envz00_7032,
		obj_t BgL_nz00_7033, obj_t BgL_pz00_7034, obj_t BgL_arg0z00_7035,
		obj_t BgL_arg1z00_7036, obj_t BgL_arg2z00_7037, obj_t BgL_arg3z00_7038)
	{
		{	/* Ast/walk.scm 160 */
			{	/* Ast/walk.scm 162 */
				obj_t BgL_arg2233z00_8670;

				{	/* Ast/walk.scm 162 */
					obj_t BgL_zc3z04anonymousza32235ze3z87_8671;

					BgL_zc3z04anonymousza32235ze3z87_8671 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza32235ze3ze5zzast_walkz00,
						(int) (0L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza32235ze3z87_8671,
						(int) (0L), ((obj_t) ((BgL_nodez00_bglt) BgL_nz00_7033)));
					BgL_arg2233z00_8670 =
						BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00
						(BgL_zc3z04anonymousza32235ze3z87_8671);
				}
				return
					BGl_errorz00zz__errorz00(BGl_string3760z00zzast_walkz00,
					BGl_string3761z00zzast_walkz00, BgL_arg2233z00_8670);
			}
		}

	}



/* &<@anonymous:2235> */
	obj_t BGl_z62zc3z04anonymousza32235ze3ze5zzast_walkz00(obj_t BgL_envz00_7039)
	{
		{	/* Ast/walk.scm 162 */
			{	/* Ast/walk.scm 162 */
				BgL_nodez00_bglt BgL_nz00_7040;

				BgL_nz00_7040 =
					((BgL_nodez00_bglt) PROCEDURE_REF(BgL_envz00_7039, (int) (0L)));
				{	/* Ast/walk.scm 162 */
					obj_t BgL_portz00_8672;

					{	/* Ast/walk.scm 162 */
						obj_t BgL_tmpz00_9981;

						BgL_tmpz00_9981 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_portz00_8672 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_9981);
					}
					{	/* Ast/walk.scm 162 */

						return
							BGl_writezd2circlezd2zz__pp_circlez00(
							((obj_t) BgL_nz00_7040), BgL_portz00_8672);
					}
				}
			}
		}

	}



/* &walk3!1658 */
	obj_t BGl_z62walk3z121658z70zzast_walkz00(obj_t BgL_envz00_7041,
		obj_t BgL_nz00_7042, obj_t BgL_pz00_7043, obj_t BgL_arg0z00_7044,
		obj_t BgL_arg1z00_7045, obj_t BgL_arg2z00_7046)
	{
		{	/* Ast/walk.scm 157 */
			{	/* Ast/walk.scm 159 */
				obj_t BgL_arg2229z00_8675;

				{	/* Ast/walk.scm 159 */
					obj_t BgL_zc3z04anonymousza32231ze3z87_8676;

					BgL_zc3z04anonymousza32231ze3z87_8676 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza32231ze3ze5zzast_walkz00,
						(int) (0L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza32231ze3z87_8676,
						(int) (0L), ((obj_t) ((BgL_nodez00_bglt) BgL_nz00_7042)));
					BgL_arg2229z00_8675 =
						BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00
						(BgL_zc3z04anonymousza32231ze3z87_8676);
				}
				return
					BGl_errorz00zz__errorz00(BGl_string3762z00zzast_walkz00,
					BGl_string3761z00zzast_walkz00, BgL_arg2229z00_8675);
			}
		}

	}



/* &<@anonymous:2231> */
	obj_t BGl_z62zc3z04anonymousza32231ze3ze5zzast_walkz00(obj_t BgL_envz00_7047)
	{
		{	/* Ast/walk.scm 159 */
			{	/* Ast/walk.scm 159 */
				BgL_nodez00_bglt BgL_nz00_7048;

				BgL_nz00_7048 =
					((BgL_nodez00_bglt) PROCEDURE_REF(BgL_envz00_7047, (int) (0L)));
				{	/* Ast/walk.scm 159 */
					obj_t BgL_portz00_8677;

					{	/* Ast/walk.scm 159 */
						obj_t BgL_tmpz00_9998;

						BgL_tmpz00_9998 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_portz00_8677 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_9998);
					}
					{	/* Ast/walk.scm 159 */

						return
							BGl_writezd2circlezd2zz__pp_circlez00(
							((obj_t) BgL_nz00_7048), BgL_portz00_8677);
					}
				}
			}
		}

	}



/* &walk2!1655 */
	obj_t BGl_z62walk2z121655z70zzast_walkz00(obj_t BgL_envz00_7049,
		obj_t BgL_nz00_7050, obj_t BgL_pz00_7051, obj_t BgL_arg0z00_7052,
		obj_t BgL_arg1z00_7053)
	{
		{	/* Ast/walk.scm 154 */
			{	/* Ast/walk.scm 156 */
				obj_t BgL_arg2225z00_8680;

				{	/* Ast/walk.scm 156 */
					obj_t BgL_zc3z04anonymousza32227ze3z87_8681;

					BgL_zc3z04anonymousza32227ze3z87_8681 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza32227ze3ze5zzast_walkz00,
						(int) (0L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza32227ze3z87_8681,
						(int) (0L), ((obj_t) ((BgL_nodez00_bglt) BgL_nz00_7050)));
					BgL_arg2225z00_8680 =
						BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00
						(BgL_zc3z04anonymousza32227ze3z87_8681);
				}
				return
					BGl_errorz00zz__errorz00(BGl_string3763z00zzast_walkz00,
					BGl_string3761z00zzast_walkz00, BgL_arg2225z00_8680);
			}
		}

	}



/* &<@anonymous:2227> */
	obj_t BGl_z62zc3z04anonymousza32227ze3ze5zzast_walkz00(obj_t BgL_envz00_7054)
	{
		{	/* Ast/walk.scm 156 */
			{	/* Ast/walk.scm 156 */
				BgL_nodez00_bglt BgL_nz00_7055;

				BgL_nz00_7055 =
					((BgL_nodez00_bglt) PROCEDURE_REF(BgL_envz00_7054, (int) (0L)));
				{	/* Ast/walk.scm 156 */
					obj_t BgL_portz00_8682;

					{	/* Ast/walk.scm 156 */
						obj_t BgL_tmpz00_10015;

						BgL_tmpz00_10015 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_portz00_8682 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_10015);
					}
					{	/* Ast/walk.scm 156 */

						return
							BGl_writezd2circlezd2zz__pp_circlez00(
							((obj_t) BgL_nz00_7055), BgL_portz00_8682);
					}
				}
			}
		}

	}



/* &walk1!1653 */
	obj_t BGl_z62walk1z121653z70zzast_walkz00(obj_t BgL_envz00_7056,
		obj_t BgL_nz00_7057, obj_t BgL_pz00_7058, obj_t BgL_arg0z00_7059)
	{
		{	/* Ast/walk.scm 151 */
			{	/* Ast/walk.scm 153 */
				obj_t BgL_arg2221z00_8685;

				{	/* Ast/walk.scm 153 */
					obj_t BgL_zc3z04anonymousza32223ze3z87_8686;

					BgL_zc3z04anonymousza32223ze3z87_8686 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza32223ze3ze5zzast_walkz00,
						(int) (0L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza32223ze3z87_8686,
						(int) (0L), ((obj_t) ((BgL_nodez00_bglt) BgL_nz00_7057)));
					BgL_arg2221z00_8685 =
						BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00
						(BgL_zc3z04anonymousza32223ze3z87_8686);
				}
				return
					BGl_errorz00zz__errorz00(BGl_string3764z00zzast_walkz00,
					BGl_string3761z00zzast_walkz00, BgL_arg2221z00_8685);
			}
		}

	}



/* &<@anonymous:2223> */
	obj_t BGl_z62zc3z04anonymousza32223ze3ze5zzast_walkz00(obj_t BgL_envz00_7060)
	{
		{	/* Ast/walk.scm 153 */
			{	/* Ast/walk.scm 153 */
				BgL_nodez00_bglt BgL_nz00_7061;

				BgL_nz00_7061 =
					((BgL_nodez00_bglt) PROCEDURE_REF(BgL_envz00_7060, (int) (0L)));
				{	/* Ast/walk.scm 153 */
					obj_t BgL_portz00_8687;

					{	/* Ast/walk.scm 153 */
						obj_t BgL_tmpz00_10032;

						BgL_tmpz00_10032 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_portz00_8687 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_10032);
					}
					{	/* Ast/walk.scm 153 */

						return
							BGl_writezd2circlezd2zz__pp_circlez00(
							((obj_t) BgL_nz00_7061), BgL_portz00_8687);
					}
				}
			}
		}

	}



/* &walk0!1651 */
	obj_t BGl_z62walk0z121651z70zzast_walkz00(obj_t BgL_envz00_7062,
		obj_t BgL_nz00_7063, obj_t BgL_pz00_7064)
	{
		{	/* Ast/walk.scm 148 */
			{	/* Ast/walk.scm 150 */
				obj_t BgL_arg2217z00_8690;

				{	/* Ast/walk.scm 150 */
					obj_t BgL_zc3z04anonymousza32219ze3z87_8691;

					BgL_zc3z04anonymousza32219ze3z87_8691 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza32219ze3ze5zzast_walkz00,
						(int) (0L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza32219ze3z87_8691,
						(int) (0L), ((obj_t) ((BgL_nodez00_bglt) BgL_nz00_7063)));
					BgL_arg2217z00_8690 =
						BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00
						(BgL_zc3z04anonymousza32219ze3z87_8691);
				}
				return
					BGl_errorz00zz__errorz00(BGl_string3765z00zzast_walkz00,
					BGl_string3761z00zzast_walkz00, BgL_arg2217z00_8690);
			}
		}

	}



/* &<@anonymous:2219> */
	obj_t BGl_z62zc3z04anonymousza32219ze3ze5zzast_walkz00(obj_t BgL_envz00_7065)
	{
		{	/* Ast/walk.scm 150 */
			{	/* Ast/walk.scm 150 */
				BgL_nodez00_bglt BgL_nz00_7066;

				BgL_nz00_7066 =
					((BgL_nodez00_bglt) PROCEDURE_REF(BgL_envz00_7065, (int) (0L)));
				{	/* Ast/walk.scm 150 */
					obj_t BgL_portz00_8692;

					{	/* Ast/walk.scm 150 */
						obj_t BgL_tmpz00_10049;

						BgL_tmpz00_10049 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_portz00_8692 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_10049);
					}
					{	/* Ast/walk.scm 150 */

						return
							BGl_writezd2circlezd2zz__pp_circlez00(
							((obj_t) BgL_nz00_7066), BgL_portz00_8692);
					}
				}
			}
		}

	}



/* &walk4*1649 */
	obj_t BGl_z62walk4za21649zc0zzast_walkz00(obj_t BgL_envz00_7067,
		obj_t BgL_nz00_7068, obj_t BgL_pz00_7069, obj_t BgL_arg0z00_7070,
		obj_t BgL_arg1z00_7071, obj_t BgL_arg2z00_7072, obj_t BgL_arg3z00_7073)
	{
		{	/* Ast/walk.scm 144 */
			{	/* Ast/walk.scm 146 */
				obj_t BgL_arg2213z00_8695;

				{	/* Ast/walk.scm 146 */
					obj_t BgL_zc3z04anonymousza32215ze3z87_8696;

					BgL_zc3z04anonymousza32215ze3z87_8696 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza32215ze3ze5zzast_walkz00,
						(int) (0L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza32215ze3z87_8696,
						(int) (0L), ((obj_t) ((BgL_nodez00_bglt) BgL_nz00_7068)));
					BgL_arg2213z00_8695 =
						BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00
						(BgL_zc3z04anonymousza32215ze3z87_8696);
				}
				return
					BGl_errorz00zz__errorz00(BGl_string3760z00zzast_walkz00,
					BGl_string3761z00zzast_walkz00, BgL_arg2213z00_8695);
			}
		}

	}



/* &<@anonymous:2215> */
	obj_t BGl_z62zc3z04anonymousza32215ze3ze5zzast_walkz00(obj_t BgL_envz00_7074)
	{
		{	/* Ast/walk.scm 146 */
			{	/* Ast/walk.scm 146 */
				BgL_nodez00_bglt BgL_nz00_7075;

				BgL_nz00_7075 =
					((BgL_nodez00_bglt) PROCEDURE_REF(BgL_envz00_7074, (int) (0L)));
				{	/* Ast/walk.scm 146 */
					obj_t BgL_portz00_8697;

					{	/* Ast/walk.scm 146 */
						obj_t BgL_tmpz00_10066;

						BgL_tmpz00_10066 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_portz00_8697 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_10066);
					}
					{	/* Ast/walk.scm 146 */

						return
							BGl_writezd2circlezd2zz__pp_circlez00(
							((obj_t) BgL_nz00_7075), BgL_portz00_8697);
					}
				}
			}
		}

	}



/* &walk3*1647 */
	obj_t BGl_z62walk3za21647zc0zzast_walkz00(obj_t BgL_envz00_7076,
		obj_t BgL_nz00_7077, obj_t BgL_pz00_7078, obj_t BgL_arg0z00_7079,
		obj_t BgL_arg1z00_7080, obj_t BgL_arg2z00_7081)
	{
		{	/* Ast/walk.scm 141 */
			{	/* Ast/walk.scm 143 */
				obj_t BgL_arg2209z00_8700;

				{	/* Ast/walk.scm 143 */
					obj_t BgL_zc3z04anonymousza32211ze3z87_8701;

					BgL_zc3z04anonymousza32211ze3z87_8701 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza32211ze3ze5zzast_walkz00,
						(int) (0L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza32211ze3z87_8701,
						(int) (0L), ((obj_t) ((BgL_nodez00_bglt) BgL_nz00_7077)));
					BgL_arg2209z00_8700 =
						BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00
						(BgL_zc3z04anonymousza32211ze3z87_8701);
				}
				return
					BGl_errorz00zz__errorz00(BGl_string3766z00zzast_walkz00,
					BGl_string3761z00zzast_walkz00, BgL_arg2209z00_8700);
			}
		}

	}



/* &<@anonymous:2211> */
	obj_t BGl_z62zc3z04anonymousza32211ze3ze5zzast_walkz00(obj_t BgL_envz00_7082)
	{
		{	/* Ast/walk.scm 143 */
			{	/* Ast/walk.scm 143 */
				BgL_nodez00_bglt BgL_nz00_7083;

				BgL_nz00_7083 =
					((BgL_nodez00_bglt) PROCEDURE_REF(BgL_envz00_7082, (int) (0L)));
				{	/* Ast/walk.scm 143 */
					obj_t BgL_portz00_8702;

					{	/* Ast/walk.scm 143 */
						obj_t BgL_tmpz00_10083;

						BgL_tmpz00_10083 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_portz00_8702 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_10083);
					}
					{	/* Ast/walk.scm 143 */

						return
							BGl_writezd2circlezd2zz__pp_circlez00(
							((obj_t) BgL_nz00_7083), BgL_portz00_8702);
					}
				}
			}
		}

	}



/* &walk2*1645 */
	obj_t BGl_z62walk2za21645zc0zzast_walkz00(obj_t BgL_envz00_7084,
		obj_t BgL_nz00_7085, obj_t BgL_pz00_7086, obj_t BgL_arg0z00_7087,
		obj_t BgL_arg1z00_7088)
	{
		{	/* Ast/walk.scm 138 */
			{	/* Ast/walk.scm 140 */
				obj_t BgL_arg2205z00_8705;

				{	/* Ast/walk.scm 140 */
					obj_t BgL_zc3z04anonymousza32207ze3z87_8706;

					BgL_zc3z04anonymousza32207ze3z87_8706 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza32207ze3ze5zzast_walkz00,
						(int) (0L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza32207ze3z87_8706,
						(int) (0L), ((obj_t) ((BgL_nodez00_bglt) BgL_nz00_7085)));
					BgL_arg2205z00_8705 =
						BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00
						(BgL_zc3z04anonymousza32207ze3z87_8706);
				}
				return
					BGl_errorz00zz__errorz00(BGl_string3767z00zzast_walkz00,
					BGl_string3761z00zzast_walkz00, BgL_arg2205z00_8705);
			}
		}

	}



/* &<@anonymous:2207> */
	obj_t BGl_z62zc3z04anonymousza32207ze3ze5zzast_walkz00(obj_t BgL_envz00_7089)
	{
		{	/* Ast/walk.scm 140 */
			{	/* Ast/walk.scm 140 */
				BgL_nodez00_bglt BgL_nz00_7090;

				BgL_nz00_7090 =
					((BgL_nodez00_bglt) PROCEDURE_REF(BgL_envz00_7089, (int) (0L)));
				{	/* Ast/walk.scm 140 */
					obj_t BgL_portz00_8707;

					{	/* Ast/walk.scm 140 */
						obj_t BgL_tmpz00_10100;

						BgL_tmpz00_10100 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_portz00_8707 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_10100);
					}
					{	/* Ast/walk.scm 140 */

						return
							BGl_writezd2circlezd2zz__pp_circlez00(
							((obj_t) BgL_nz00_7090), BgL_portz00_8707);
					}
				}
			}
		}

	}



/* &walk1*1643 */
	obj_t BGl_z62walk1za21643zc0zzast_walkz00(obj_t BgL_envz00_7091,
		obj_t BgL_nz00_7092, obj_t BgL_pz00_7093, obj_t BgL_arg0z00_7094)
	{
		{	/* Ast/walk.scm 135 */
			{	/* Ast/walk.scm 137 */
				obj_t BgL_arg2201z00_8710;

				{	/* Ast/walk.scm 137 */
					obj_t BgL_zc3z04anonymousza32203ze3z87_8711;

					BgL_zc3z04anonymousza32203ze3z87_8711 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza32203ze3ze5zzast_walkz00,
						(int) (0L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza32203ze3z87_8711,
						(int) (0L), ((obj_t) ((BgL_nodez00_bglt) BgL_nz00_7092)));
					BgL_arg2201z00_8710 =
						BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00
						(BgL_zc3z04anonymousza32203ze3z87_8711);
				}
				return
					BGl_errorz00zz__errorz00(BGl_string3768z00zzast_walkz00,
					BGl_string3761z00zzast_walkz00, BgL_arg2201z00_8710);
			}
		}

	}



/* &<@anonymous:2203> */
	obj_t BGl_z62zc3z04anonymousza32203ze3ze5zzast_walkz00(obj_t BgL_envz00_7095)
	{
		{	/* Ast/walk.scm 137 */
			{	/* Ast/walk.scm 137 */
				BgL_nodez00_bglt BgL_nz00_7096;

				BgL_nz00_7096 =
					((BgL_nodez00_bglt) PROCEDURE_REF(BgL_envz00_7095, (int) (0L)));
				{	/* Ast/walk.scm 137 */
					obj_t BgL_portz00_8712;

					{	/* Ast/walk.scm 137 */
						obj_t BgL_tmpz00_10117;

						BgL_tmpz00_10117 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_portz00_8712 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_10117);
					}
					{	/* Ast/walk.scm 137 */

						return
							BGl_writezd2circlezd2zz__pp_circlez00(
							((obj_t) BgL_nz00_7096), BgL_portz00_8712);
					}
				}
			}
		}

	}



/* &walk0*1641 */
	obj_t BGl_z62walk0za21641zc0zzast_walkz00(obj_t BgL_envz00_7097,
		obj_t BgL_nz00_7098, obj_t BgL_pz00_7099)
	{
		{	/* Ast/walk.scm 132 */
			{	/* Ast/walk.scm 134 */
				obj_t BgL_arg2197z00_8715;

				{	/* Ast/walk.scm 134 */
					obj_t BgL_zc3z04anonymousza32199ze3z87_8716;

					BgL_zc3z04anonymousza32199ze3z87_8716 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza32199ze3ze5zzast_walkz00,
						(int) (0L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza32199ze3z87_8716,
						(int) (0L), ((obj_t) ((BgL_nodez00_bglt) BgL_nz00_7098)));
					BgL_arg2197z00_8715 =
						BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00
						(BgL_zc3z04anonymousza32199ze3z87_8716);
				}
				return
					BGl_errorz00zz__errorz00(BGl_string3769z00zzast_walkz00,
					BGl_string3761z00zzast_walkz00, BgL_arg2197z00_8715);
			}
		}

	}



/* &<@anonymous:2199> */
	obj_t BGl_z62zc3z04anonymousza32199ze3ze5zzast_walkz00(obj_t BgL_envz00_7100)
	{
		{	/* Ast/walk.scm 134 */
			{	/* Ast/walk.scm 134 */
				BgL_nodez00_bglt BgL_nz00_7101;

				BgL_nz00_7101 =
					((BgL_nodez00_bglt) PROCEDURE_REF(BgL_envz00_7100, (int) (0L)));
				{	/* Ast/walk.scm 134 */
					obj_t BgL_portz00_8717;

					{	/* Ast/walk.scm 134 */
						obj_t BgL_tmpz00_10134;

						BgL_tmpz00_10134 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_portz00_8717 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_10134);
					}
					{	/* Ast/walk.scm 134 */

						return
							BGl_writezd2circlezd2zz__pp_circlez00(
							((obj_t) BgL_nz00_7101), BgL_portz00_8717);
					}
				}
			}
		}

	}



/* &walk41639 */
	obj_t BGl_z62walk41639z62zzast_walkz00(obj_t BgL_envz00_7102,
		obj_t BgL_nz00_7103, obj_t BgL_pz00_7104, obj_t BgL_arg0z00_7105,
		obj_t BgL_arg1z00_7106, obj_t BgL_arg2z00_7107, obj_t BgL_arg3z00_7108)
	{
		{	/* Ast/walk.scm 128 */
			{	/* Ast/walk.scm 130 */
				obj_t BgL_arg2192z00_8720;

				{	/* Ast/walk.scm 130 */
					obj_t BgL_zc3z04anonymousza32194ze3z87_8721;

					BgL_zc3z04anonymousza32194ze3z87_8721 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza32194ze3ze5zzast_walkz00,
						(int) (0L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza32194ze3z87_8721,
						(int) (0L), ((obj_t) ((BgL_nodez00_bglt) BgL_nz00_7103)));
					BgL_arg2192z00_8720 =
						BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00
						(BgL_zc3z04anonymousza32194ze3z87_8721);
				}
				return
					BGl_errorz00zz__errorz00(BGl_string3770z00zzast_walkz00,
					BGl_string3761z00zzast_walkz00, BgL_arg2192z00_8720);
			}
		}

	}



/* &<@anonymous:2194> */
	obj_t BGl_z62zc3z04anonymousza32194ze3ze5zzast_walkz00(obj_t BgL_envz00_7109)
	{
		{	/* Ast/walk.scm 130 */
			{	/* Ast/walk.scm 130 */
				BgL_nodez00_bglt BgL_nz00_7110;

				BgL_nz00_7110 =
					((BgL_nodez00_bglt) PROCEDURE_REF(BgL_envz00_7109, (int) (0L)));
				{	/* Ast/walk.scm 130 */
					obj_t BgL_portz00_8722;

					{	/* Ast/walk.scm 130 */
						obj_t BgL_tmpz00_10151;

						BgL_tmpz00_10151 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_portz00_8722 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_10151);
					}
					{	/* Ast/walk.scm 130 */

						return
							BGl_writezd2circlezd2zz__pp_circlez00(
							((obj_t) BgL_nz00_7110), BgL_portz00_8722);
					}
				}
			}
		}

	}



/* &walk31636 */
	obj_t BGl_z62walk31636z62zzast_walkz00(obj_t BgL_envz00_7111,
		obj_t BgL_nz00_7112, obj_t BgL_pz00_7113, obj_t BgL_arg0z00_7114,
		obj_t BgL_arg1z00_7115, obj_t BgL_arg2z00_7116)
	{
		{	/* Ast/walk.scm 125 */
			{	/* Ast/walk.scm 127 */
				obj_t BgL_arg2188z00_8725;

				{	/* Ast/walk.scm 127 */
					obj_t BgL_zc3z04anonymousza32190ze3z87_8726;

					BgL_zc3z04anonymousza32190ze3z87_8726 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza32190ze3ze5zzast_walkz00,
						(int) (0L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza32190ze3z87_8726,
						(int) (0L), ((obj_t) ((BgL_nodez00_bglt) BgL_nz00_7112)));
					BgL_arg2188z00_8725 =
						BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00
						(BgL_zc3z04anonymousza32190ze3z87_8726);
				}
				return
					BGl_errorz00zz__errorz00(BGl_string3771z00zzast_walkz00,
					BGl_string3761z00zzast_walkz00, BgL_arg2188z00_8725);
			}
		}

	}



/* &<@anonymous:2190> */
	obj_t BGl_z62zc3z04anonymousza32190ze3ze5zzast_walkz00(obj_t BgL_envz00_7117)
	{
		{	/* Ast/walk.scm 127 */
			{	/* Ast/walk.scm 127 */
				BgL_nodez00_bglt BgL_nz00_7118;

				BgL_nz00_7118 =
					((BgL_nodez00_bglt) PROCEDURE_REF(BgL_envz00_7117, (int) (0L)));
				{	/* Ast/walk.scm 127 */
					obj_t BgL_portz00_8727;

					{	/* Ast/walk.scm 127 */
						obj_t BgL_tmpz00_10168;

						BgL_tmpz00_10168 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_portz00_8727 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_10168);
					}
					{	/* Ast/walk.scm 127 */

						return
							BGl_writezd2circlezd2zz__pp_circlez00(
							((obj_t) BgL_nz00_7118), BgL_portz00_8727);
					}
				}
			}
		}

	}



/* &walk21634 */
	obj_t BGl_z62walk21634z62zzast_walkz00(obj_t BgL_envz00_7119,
		obj_t BgL_nz00_7120, obj_t BgL_pz00_7121, obj_t BgL_arg0z00_7122,
		obj_t BgL_arg1z00_7123)
	{
		{	/* Ast/walk.scm 122 */
			{	/* Ast/walk.scm 124 */
				obj_t BgL_arg2184z00_8730;

				{	/* Ast/walk.scm 124 */
					obj_t BgL_zc3z04anonymousza32186ze3z87_8731;

					BgL_zc3z04anonymousza32186ze3z87_8731 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza32186ze3ze5zzast_walkz00,
						(int) (0L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza32186ze3z87_8731,
						(int) (0L), ((obj_t) ((BgL_nodez00_bglt) BgL_nz00_7120)));
					BgL_arg2184z00_8730 =
						BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00
						(BgL_zc3z04anonymousza32186ze3z87_8731);
				}
				return
					BGl_errorz00zz__errorz00(BGl_string3772z00zzast_walkz00,
					BGl_string3761z00zzast_walkz00, BgL_arg2184z00_8730);
			}
		}

	}



/* &<@anonymous:2186> */
	obj_t BGl_z62zc3z04anonymousza32186ze3ze5zzast_walkz00(obj_t BgL_envz00_7124)
	{
		{	/* Ast/walk.scm 124 */
			{	/* Ast/walk.scm 124 */
				BgL_nodez00_bglt BgL_nz00_7125;

				BgL_nz00_7125 =
					((BgL_nodez00_bglt) PROCEDURE_REF(BgL_envz00_7124, (int) (0L)));
				{	/* Ast/walk.scm 124 */
					obj_t BgL_portz00_8732;

					{	/* Ast/walk.scm 124 */
						obj_t BgL_tmpz00_10185;

						BgL_tmpz00_10185 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_portz00_8732 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_10185);
					}
					{	/* Ast/walk.scm 124 */

						return
							BGl_writezd2circlezd2zz__pp_circlez00(
							((obj_t) BgL_nz00_7125), BgL_portz00_8732);
					}
				}
			}
		}

	}



/* &walk11632 */
	obj_t BGl_z62walk11632z62zzast_walkz00(obj_t BgL_envz00_7126,
		obj_t BgL_nz00_7127, obj_t BgL_pz00_7128, obj_t BgL_arg0z00_7129)
	{
		{	/* Ast/walk.scm 119 */
			{	/* Ast/walk.scm 121 */
				obj_t BgL_arg2180z00_8735;

				{	/* Ast/walk.scm 121 */
					obj_t BgL_zc3z04anonymousza32182ze3z87_8736;

					BgL_zc3z04anonymousza32182ze3z87_8736 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza32182ze3ze5zzast_walkz00,
						(int) (0L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza32182ze3z87_8736,
						(int) (0L), ((obj_t) ((BgL_nodez00_bglt) BgL_nz00_7127)));
					BgL_arg2180z00_8735 =
						BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00
						(BgL_zc3z04anonymousza32182ze3z87_8736);
				}
				return
					BGl_errorz00zz__errorz00(BGl_string3773z00zzast_walkz00,
					BGl_string3761z00zzast_walkz00, BgL_arg2180z00_8735);
			}
		}

	}



/* &<@anonymous:2182> */
	obj_t BGl_z62zc3z04anonymousza32182ze3ze5zzast_walkz00(obj_t BgL_envz00_7130)
	{
		{	/* Ast/walk.scm 121 */
			{	/* Ast/walk.scm 121 */
				BgL_nodez00_bglt BgL_nz00_7131;

				BgL_nz00_7131 =
					((BgL_nodez00_bglt) PROCEDURE_REF(BgL_envz00_7130, (int) (0L)));
				{	/* Ast/walk.scm 121 */
					obj_t BgL_portz00_8737;

					{	/* Ast/walk.scm 121 */
						obj_t BgL_tmpz00_10202;

						BgL_tmpz00_10202 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_portz00_8737 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_10202);
					}
					{	/* Ast/walk.scm 121 */

						return
							BGl_writezd2circlezd2zz__pp_circlez00(
							((obj_t) BgL_nz00_7131), BgL_portz00_8737);
					}
				}
			}
		}

	}



/* &walk01630 */
	obj_t BGl_z62walk01630z62zzast_walkz00(obj_t BgL_envz00_7132,
		obj_t BgL_nz00_7133, obj_t BgL_pz00_7134)
	{
		{	/* Ast/walk.scm 116 */
			{	/* Ast/walk.scm 118 */
				obj_t BgL_arg2176z00_8740;

				{	/* Ast/walk.scm 118 */
					obj_t BgL_zc3z04anonymousza32178ze3z87_8741;

					BgL_zc3z04anonymousza32178ze3z87_8741 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza32178ze3ze5zzast_walkz00,
						(int) (0L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza32178ze3z87_8741,
						(int) (0L), ((obj_t) ((BgL_nodez00_bglt) BgL_nz00_7133)));
					BgL_arg2176z00_8740 =
						BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00
						(BgL_zc3z04anonymousza32178ze3z87_8741);
				}
				return
					BGl_errorz00zz__errorz00(BGl_string3774z00zzast_walkz00,
					BGl_string3761z00zzast_walkz00, BgL_arg2176z00_8740);
			}
		}

	}



/* &<@anonymous:2178> */
	obj_t BGl_z62zc3z04anonymousza32178ze3ze5zzast_walkz00(obj_t BgL_envz00_7135)
	{
		{	/* Ast/walk.scm 118 */
			{	/* Ast/walk.scm 118 */
				BgL_nodez00_bglt BgL_nz00_7136;

				BgL_nz00_7136 =
					((BgL_nodez00_bglt) PROCEDURE_REF(BgL_envz00_7135, (int) (0L)));
				{	/* Ast/walk.scm 118 */
					obj_t BgL_portz00_8742;

					{	/* Ast/walk.scm 118 */
						obj_t BgL_tmpz00_10219;

						BgL_tmpz00_10219 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_portz00_8742 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_10219);
					}
					{	/* Ast/walk.scm 118 */

						return
							BGl_writezd2circlezd2zz__pp_circlez00(
							((obj_t) BgL_nz00_7136), BgL_portz00_8742);
					}
				}
			}
		}

	}



/* walk0 */
	BGL_EXPORTED_DEF obj_t BGl_walk0z00zzast_walkz00(BgL_nodez00_bglt BgL_nz00_3,
		obj_t BgL_pz00_4)
	{
		{	/* Ast/walk.scm 116 */
			{	/* Ast/walk.scm 116 */
				obj_t BgL_method1631z00_2489;

				{	/* Ast/walk.scm 116 */
					obj_t BgL_res3659z00_6022;

					{	/* Ast/walk.scm 116 */
						long BgL_objzd2classzd2numz00_5993;

						BgL_objzd2classzd2numz00_5993 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nz00_3));
						{	/* Ast/walk.scm 116 */
							obj_t BgL_arg1811z00_5994;

							BgL_arg1811z00_5994 =
								PROCEDURE_REF(BGl_walk0zd2envzd2zzast_walkz00, (int) (1L));
							{	/* Ast/walk.scm 116 */
								int BgL_offsetz00_5997;

								BgL_offsetz00_5997 = (int) (BgL_objzd2classzd2numz00_5993);
								{	/* Ast/walk.scm 116 */
									long BgL_offsetz00_5998;

									BgL_offsetz00_5998 =
										((long) (BgL_offsetz00_5997) - OBJECT_TYPE);
									{	/* Ast/walk.scm 116 */
										long BgL_modz00_5999;

										BgL_modz00_5999 =
											(BgL_offsetz00_5998 >> (int) ((long) ((int) (4L))));
										{	/* Ast/walk.scm 116 */
											long BgL_restz00_6001;

											BgL_restz00_6001 =
												(BgL_offsetz00_5998 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/walk.scm 116 */

												{	/* Ast/walk.scm 116 */
													obj_t BgL_bucketz00_6003;

													BgL_bucketz00_6003 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_5994), BgL_modz00_5999);
													BgL_res3659z00_6022 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_6003), BgL_restz00_6001);
					}}}}}}}}
					BgL_method1631z00_2489 = BgL_res3659z00_6022;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1631z00_2489,
					((obj_t) BgL_nz00_3), BgL_pz00_4);
			}
		}

	}



/* &walk0 */
	obj_t BGl_z62walk0z62zzast_walkz00(obj_t BgL_envz00_7137, obj_t BgL_nz00_7138,
		obj_t BgL_pz00_7139)
	{
		{	/* Ast/walk.scm 116 */
			return
				BGl_walk0z00zzast_walkz00(
				((BgL_nodez00_bglt) BgL_nz00_7138), BgL_pz00_7139);
		}

	}



/* walk1 */
	BGL_EXPORTED_DEF obj_t BGl_walk1z00zzast_walkz00(BgL_nodez00_bglt BgL_nz00_5,
		obj_t BgL_pz00_6, obj_t BgL_arg0z00_7)
	{
		{	/* Ast/walk.scm 119 */
			{	/* Ast/walk.scm 119 */
				obj_t BgL_method1633z00_2490;

				{	/* Ast/walk.scm 119 */
					obj_t BgL_res3664z00_6053;

					{	/* Ast/walk.scm 119 */
						long BgL_objzd2classzd2numz00_6024;

						BgL_objzd2classzd2numz00_6024 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nz00_5));
						{	/* Ast/walk.scm 119 */
							obj_t BgL_arg1811z00_6025;

							BgL_arg1811z00_6025 =
								PROCEDURE_REF(BGl_walk1zd2envzd2zzast_walkz00, (int) (1L));
							{	/* Ast/walk.scm 119 */
								int BgL_offsetz00_6028;

								BgL_offsetz00_6028 = (int) (BgL_objzd2classzd2numz00_6024);
								{	/* Ast/walk.scm 119 */
									long BgL_offsetz00_6029;

									BgL_offsetz00_6029 =
										((long) (BgL_offsetz00_6028) - OBJECT_TYPE);
									{	/* Ast/walk.scm 119 */
										long BgL_modz00_6030;

										BgL_modz00_6030 =
											(BgL_offsetz00_6029 >> (int) ((long) ((int) (4L))));
										{	/* Ast/walk.scm 119 */
											long BgL_restz00_6032;

											BgL_restz00_6032 =
												(BgL_offsetz00_6029 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/walk.scm 119 */

												{	/* Ast/walk.scm 119 */
													obj_t BgL_bucketz00_6034;

													BgL_bucketz00_6034 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_6025), BgL_modz00_6030);
													BgL_res3664z00_6053 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_6034), BgL_restz00_6032);
					}}}}}}}}
					BgL_method1633z00_2490 = BgL_res3664z00_6053;
				}
				return
					BGL_PROCEDURE_CALL3(BgL_method1633z00_2490,
					((obj_t) BgL_nz00_5), BgL_pz00_6, BgL_arg0z00_7);
			}
		}

	}



/* &walk1 */
	obj_t BGl_z62walk1z62zzast_walkz00(obj_t BgL_envz00_7140, obj_t BgL_nz00_7141,
		obj_t BgL_pz00_7142, obj_t BgL_arg0z00_7143)
	{
		{	/* Ast/walk.scm 119 */
			return
				BGl_walk1z00zzast_walkz00(
				((BgL_nodez00_bglt) BgL_nz00_7141), BgL_pz00_7142, BgL_arg0z00_7143);
		}

	}



/* walk2 */
	BGL_EXPORTED_DEF obj_t BGl_walk2z00zzast_walkz00(BgL_nodez00_bglt BgL_nz00_8,
		obj_t BgL_pz00_9, obj_t BgL_arg0z00_10, obj_t BgL_arg1z00_11)
	{
		{	/* Ast/walk.scm 122 */
			{	/* Ast/walk.scm 122 */
				obj_t BgL_method1635z00_2491;

				{	/* Ast/walk.scm 122 */
					obj_t BgL_res3669z00_6084;

					{	/* Ast/walk.scm 122 */
						long BgL_objzd2classzd2numz00_6055;

						BgL_objzd2classzd2numz00_6055 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nz00_8));
						{	/* Ast/walk.scm 122 */
							obj_t BgL_arg1811z00_6056;

							BgL_arg1811z00_6056 =
								PROCEDURE_REF(BGl_walk2zd2envzd2zzast_walkz00, (int) (1L));
							{	/* Ast/walk.scm 122 */
								int BgL_offsetz00_6059;

								BgL_offsetz00_6059 = (int) (BgL_objzd2classzd2numz00_6055);
								{	/* Ast/walk.scm 122 */
									long BgL_offsetz00_6060;

									BgL_offsetz00_6060 =
										((long) (BgL_offsetz00_6059) - OBJECT_TYPE);
									{	/* Ast/walk.scm 122 */
										long BgL_modz00_6061;

										BgL_modz00_6061 =
											(BgL_offsetz00_6060 >> (int) ((long) ((int) (4L))));
										{	/* Ast/walk.scm 122 */
											long BgL_restz00_6063;

											BgL_restz00_6063 =
												(BgL_offsetz00_6060 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/walk.scm 122 */

												{	/* Ast/walk.scm 122 */
													obj_t BgL_bucketz00_6065;

													BgL_bucketz00_6065 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_6056), BgL_modz00_6061);
													BgL_res3669z00_6084 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_6065), BgL_restz00_6063);
					}}}}}}}}
					BgL_method1635z00_2491 = BgL_res3669z00_6084;
				}
				return
					BGL_PROCEDURE_CALL4(BgL_method1635z00_2491,
					((obj_t) BgL_nz00_8), BgL_pz00_9, BgL_arg0z00_10, BgL_arg1z00_11);
			}
		}

	}



/* &walk2 */
	obj_t BGl_z62walk2z62zzast_walkz00(obj_t BgL_envz00_7144, obj_t BgL_nz00_7145,
		obj_t BgL_pz00_7146, obj_t BgL_arg0z00_7147, obj_t BgL_arg1z00_7148)
	{
		{	/* Ast/walk.scm 122 */
			return
				BGl_walk2z00zzast_walkz00(
				((BgL_nodez00_bglt) BgL_nz00_7145), BgL_pz00_7146, BgL_arg0z00_7147,
				BgL_arg1z00_7148);
		}

	}



/* walk3 */
	BGL_EXPORTED_DEF obj_t BGl_walk3z00zzast_walkz00(BgL_nodez00_bglt BgL_nz00_12,
		obj_t BgL_pz00_13, obj_t BgL_arg0z00_14, obj_t BgL_arg1z00_15,
		obj_t BgL_arg2z00_16)
	{
		{	/* Ast/walk.scm 125 */
			{	/* Ast/walk.scm 125 */
				obj_t BgL_method1638z00_2492;

				{	/* Ast/walk.scm 125 */
					obj_t BgL_res3674z00_6115;

					{	/* Ast/walk.scm 125 */
						long BgL_objzd2classzd2numz00_6086;

						BgL_objzd2classzd2numz00_6086 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nz00_12));
						{	/* Ast/walk.scm 125 */
							obj_t BgL_arg1811z00_6087;

							BgL_arg1811z00_6087 =
								PROCEDURE_REF(BGl_walk3zd2envzd2zzast_walkz00, (int) (1L));
							{	/* Ast/walk.scm 125 */
								int BgL_offsetz00_6090;

								BgL_offsetz00_6090 = (int) (BgL_objzd2classzd2numz00_6086);
								{	/* Ast/walk.scm 125 */
									long BgL_offsetz00_6091;

									BgL_offsetz00_6091 =
										((long) (BgL_offsetz00_6090) - OBJECT_TYPE);
									{	/* Ast/walk.scm 125 */
										long BgL_modz00_6092;

										BgL_modz00_6092 =
											(BgL_offsetz00_6091 >> (int) ((long) ((int) (4L))));
										{	/* Ast/walk.scm 125 */
											long BgL_restz00_6094;

											BgL_restz00_6094 =
												(BgL_offsetz00_6091 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/walk.scm 125 */

												{	/* Ast/walk.scm 125 */
													obj_t BgL_bucketz00_6096;

													BgL_bucketz00_6096 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_6087), BgL_modz00_6092);
													BgL_res3674z00_6115 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_6096), BgL_restz00_6094);
					}}}}}}}}
					BgL_method1638z00_2492 = BgL_res3674z00_6115;
				}
				return
					BGL_PROCEDURE_CALL5(BgL_method1638z00_2492,
					((obj_t) BgL_nz00_12), BgL_pz00_13, BgL_arg0z00_14, BgL_arg1z00_15,
					BgL_arg2z00_16);
			}
		}

	}



/* &walk3 */
	obj_t BGl_z62walk3z62zzast_walkz00(obj_t BgL_envz00_7149, obj_t BgL_nz00_7150,
		obj_t BgL_pz00_7151, obj_t BgL_arg0z00_7152, obj_t BgL_arg1z00_7153,
		obj_t BgL_arg2z00_7154)
	{
		{	/* Ast/walk.scm 125 */
			return
				BGl_walk3z00zzast_walkz00(
				((BgL_nodez00_bglt) BgL_nz00_7150), BgL_pz00_7151, BgL_arg0z00_7152,
				BgL_arg1z00_7153, BgL_arg2z00_7154);
		}

	}



/* walk4 */
	BGL_EXPORTED_DEF obj_t BGl_walk4z00zzast_walkz00(BgL_nodez00_bglt BgL_nz00_17,
		obj_t BgL_pz00_18, obj_t BgL_arg0z00_19, obj_t BgL_arg1z00_20,
		obj_t BgL_arg2z00_21, obj_t BgL_arg3z00_22)
	{
		{	/* Ast/walk.scm 128 */
			{	/* Ast/walk.scm 128 */
				obj_t BgL_method1640z00_2493;

				{	/* Ast/walk.scm 128 */
					obj_t BgL_res3679z00_6146;

					{	/* Ast/walk.scm 128 */
						long BgL_objzd2classzd2numz00_6117;

						BgL_objzd2classzd2numz00_6117 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nz00_17));
						{	/* Ast/walk.scm 128 */
							obj_t BgL_arg1811z00_6118;

							BgL_arg1811z00_6118 =
								PROCEDURE_REF(BGl_walk4zd2envzd2zzast_walkz00, (int) (1L));
							{	/* Ast/walk.scm 128 */
								int BgL_offsetz00_6121;

								BgL_offsetz00_6121 = (int) (BgL_objzd2classzd2numz00_6117);
								{	/* Ast/walk.scm 128 */
									long BgL_offsetz00_6122;

									BgL_offsetz00_6122 =
										((long) (BgL_offsetz00_6121) - OBJECT_TYPE);
									{	/* Ast/walk.scm 128 */
										long BgL_modz00_6123;

										BgL_modz00_6123 =
											(BgL_offsetz00_6122 >> (int) ((long) ((int) (4L))));
										{	/* Ast/walk.scm 128 */
											long BgL_restz00_6125;

											BgL_restz00_6125 =
												(BgL_offsetz00_6122 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/walk.scm 128 */

												{	/* Ast/walk.scm 128 */
													obj_t BgL_bucketz00_6127;

													BgL_bucketz00_6127 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_6118), BgL_modz00_6123);
													BgL_res3679z00_6146 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_6127), BgL_restz00_6125);
					}}}}}}}}
					BgL_method1640z00_2493 = BgL_res3679z00_6146;
				}
				return
					BGL_PROCEDURE_CALL6(BgL_method1640z00_2493,
					((obj_t) BgL_nz00_17), BgL_pz00_18, BgL_arg0z00_19, BgL_arg1z00_20,
					BgL_arg2z00_21, BgL_arg3z00_22);
			}
		}

	}



/* &walk4 */
	obj_t BGl_z62walk4z62zzast_walkz00(obj_t BgL_envz00_7155, obj_t BgL_nz00_7156,
		obj_t BgL_pz00_7157, obj_t BgL_arg0z00_7158, obj_t BgL_arg1z00_7159,
		obj_t BgL_arg2z00_7160, obj_t BgL_arg3z00_7161)
	{
		{	/* Ast/walk.scm 128 */
			return
				BGl_walk4z00zzast_walkz00(
				((BgL_nodez00_bglt) BgL_nz00_7156), BgL_pz00_7157, BgL_arg0z00_7158,
				BgL_arg1z00_7159, BgL_arg2z00_7160, BgL_arg3z00_7161);
		}

	}



/* walk0* */
	BGL_EXPORTED_DEF obj_t BGl_walk0za2za2zzast_walkz00(BgL_nodez00_bglt
		BgL_nz00_23, obj_t BgL_pz00_24)
	{
		{	/* Ast/walk.scm 132 */
			{	/* Ast/walk.scm 132 */
				obj_t BgL_method1642z00_2494;

				{	/* Ast/walk.scm 132 */
					obj_t BgL_res3684z00_6177;

					{	/* Ast/walk.scm 132 */
						long BgL_objzd2classzd2numz00_6148;

						BgL_objzd2classzd2numz00_6148 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nz00_23));
						{	/* Ast/walk.scm 132 */
							obj_t BgL_arg1811z00_6149;

							BgL_arg1811z00_6149 =
								PROCEDURE_REF(BGl_walk0za2zd2envz70zzast_walkz00, (int) (1L));
							{	/* Ast/walk.scm 132 */
								int BgL_offsetz00_6152;

								BgL_offsetz00_6152 = (int) (BgL_objzd2classzd2numz00_6148);
								{	/* Ast/walk.scm 132 */
									long BgL_offsetz00_6153;

									BgL_offsetz00_6153 =
										((long) (BgL_offsetz00_6152) - OBJECT_TYPE);
									{	/* Ast/walk.scm 132 */
										long BgL_modz00_6154;

										BgL_modz00_6154 =
											(BgL_offsetz00_6153 >> (int) ((long) ((int) (4L))));
										{	/* Ast/walk.scm 132 */
											long BgL_restz00_6156;

											BgL_restz00_6156 =
												(BgL_offsetz00_6153 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/walk.scm 132 */

												{	/* Ast/walk.scm 132 */
													obj_t BgL_bucketz00_6158;

													BgL_bucketz00_6158 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_6149), BgL_modz00_6154);
													BgL_res3684z00_6177 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_6158), BgL_restz00_6156);
					}}}}}}}}
					BgL_method1642z00_2494 = BgL_res3684z00_6177;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1642z00_2494,
					((obj_t) BgL_nz00_23), BgL_pz00_24);
			}
		}

	}



/* &walk0* */
	obj_t BGl_z62walk0za2zc0zzast_walkz00(obj_t BgL_envz00_7162,
		obj_t BgL_nz00_7163, obj_t BgL_pz00_7164)
	{
		{	/* Ast/walk.scm 132 */
			return
				BGl_walk0za2za2zzast_walkz00(
				((BgL_nodez00_bglt) BgL_nz00_7163), BgL_pz00_7164);
		}

	}



/* walk1* */
	BGL_EXPORTED_DEF obj_t BGl_walk1za2za2zzast_walkz00(BgL_nodez00_bglt
		BgL_nz00_25, obj_t BgL_pz00_26, obj_t BgL_arg0z00_27)
	{
		{	/* Ast/walk.scm 135 */
			{	/* Ast/walk.scm 135 */
				obj_t BgL_method1644z00_2495;

				{	/* Ast/walk.scm 135 */
					obj_t BgL_res3689z00_6208;

					{	/* Ast/walk.scm 135 */
						long BgL_objzd2classzd2numz00_6179;

						BgL_objzd2classzd2numz00_6179 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nz00_25));
						{	/* Ast/walk.scm 135 */
							obj_t BgL_arg1811z00_6180;

							BgL_arg1811z00_6180 =
								PROCEDURE_REF(BGl_walk1za2zd2envz70zzast_walkz00, (int) (1L));
							{	/* Ast/walk.scm 135 */
								int BgL_offsetz00_6183;

								BgL_offsetz00_6183 = (int) (BgL_objzd2classzd2numz00_6179);
								{	/* Ast/walk.scm 135 */
									long BgL_offsetz00_6184;

									BgL_offsetz00_6184 =
										((long) (BgL_offsetz00_6183) - OBJECT_TYPE);
									{	/* Ast/walk.scm 135 */
										long BgL_modz00_6185;

										BgL_modz00_6185 =
											(BgL_offsetz00_6184 >> (int) ((long) ((int) (4L))));
										{	/* Ast/walk.scm 135 */
											long BgL_restz00_6187;

											BgL_restz00_6187 =
												(BgL_offsetz00_6184 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/walk.scm 135 */

												{	/* Ast/walk.scm 135 */
													obj_t BgL_bucketz00_6189;

													BgL_bucketz00_6189 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_6180), BgL_modz00_6185);
													BgL_res3689z00_6208 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_6189), BgL_restz00_6187);
					}}}}}}}}
					BgL_method1644z00_2495 = BgL_res3689z00_6208;
				}
				return
					BGL_PROCEDURE_CALL3(BgL_method1644z00_2495,
					((obj_t) BgL_nz00_25), BgL_pz00_26, BgL_arg0z00_27);
			}
		}

	}



/* &walk1* */
	obj_t BGl_z62walk1za2zc0zzast_walkz00(obj_t BgL_envz00_7165,
		obj_t BgL_nz00_7166, obj_t BgL_pz00_7167, obj_t BgL_arg0z00_7168)
	{
		{	/* Ast/walk.scm 135 */
			return
				BGl_walk1za2za2zzast_walkz00(
				((BgL_nodez00_bglt) BgL_nz00_7166), BgL_pz00_7167, BgL_arg0z00_7168);
		}

	}



/* walk2* */
	BGL_EXPORTED_DEF obj_t BGl_walk2za2za2zzast_walkz00(BgL_nodez00_bglt
		BgL_nz00_28, obj_t BgL_pz00_29, obj_t BgL_arg0z00_30, obj_t BgL_arg1z00_31)
	{
		{	/* Ast/walk.scm 138 */
			{	/* Ast/walk.scm 138 */
				obj_t BgL_method1646z00_2496;

				{	/* Ast/walk.scm 138 */
					obj_t BgL_res3694z00_6239;

					{	/* Ast/walk.scm 138 */
						long BgL_objzd2classzd2numz00_6210;

						BgL_objzd2classzd2numz00_6210 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nz00_28));
						{	/* Ast/walk.scm 138 */
							obj_t BgL_arg1811z00_6211;

							BgL_arg1811z00_6211 =
								PROCEDURE_REF(BGl_walk2za2zd2envz70zzast_walkz00, (int) (1L));
							{	/* Ast/walk.scm 138 */
								int BgL_offsetz00_6214;

								BgL_offsetz00_6214 = (int) (BgL_objzd2classzd2numz00_6210);
								{	/* Ast/walk.scm 138 */
									long BgL_offsetz00_6215;

									BgL_offsetz00_6215 =
										((long) (BgL_offsetz00_6214) - OBJECT_TYPE);
									{	/* Ast/walk.scm 138 */
										long BgL_modz00_6216;

										BgL_modz00_6216 =
											(BgL_offsetz00_6215 >> (int) ((long) ((int) (4L))));
										{	/* Ast/walk.scm 138 */
											long BgL_restz00_6218;

											BgL_restz00_6218 =
												(BgL_offsetz00_6215 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/walk.scm 138 */

												{	/* Ast/walk.scm 138 */
													obj_t BgL_bucketz00_6220;

													BgL_bucketz00_6220 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_6211), BgL_modz00_6216);
													BgL_res3694z00_6239 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_6220), BgL_restz00_6218);
					}}}}}}}}
					BgL_method1646z00_2496 = BgL_res3694z00_6239;
				}
				return
					BGL_PROCEDURE_CALL4(BgL_method1646z00_2496,
					((obj_t) BgL_nz00_28), BgL_pz00_29, BgL_arg0z00_30, BgL_arg1z00_31);
			}
		}

	}



/* &walk2* */
	obj_t BGl_z62walk2za2zc0zzast_walkz00(obj_t BgL_envz00_7169,
		obj_t BgL_nz00_7170, obj_t BgL_pz00_7171, obj_t BgL_arg0z00_7172,
		obj_t BgL_arg1z00_7173)
	{
		{	/* Ast/walk.scm 138 */
			return
				BGl_walk2za2za2zzast_walkz00(
				((BgL_nodez00_bglt) BgL_nz00_7170), BgL_pz00_7171, BgL_arg0z00_7172,
				BgL_arg1z00_7173);
		}

	}



/* walk3* */
	BGL_EXPORTED_DEF obj_t BGl_walk3za2za2zzast_walkz00(BgL_nodez00_bglt
		BgL_nz00_32, obj_t BgL_pz00_33, obj_t BgL_arg0z00_34, obj_t BgL_arg1z00_35,
		obj_t BgL_arg2z00_36)
	{
		{	/* Ast/walk.scm 141 */
			{	/* Ast/walk.scm 141 */
				obj_t BgL_method1648z00_2497;

				{	/* Ast/walk.scm 141 */
					obj_t BgL_res3699z00_6270;

					{	/* Ast/walk.scm 141 */
						long BgL_objzd2classzd2numz00_6241;

						BgL_objzd2classzd2numz00_6241 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nz00_32));
						{	/* Ast/walk.scm 141 */
							obj_t BgL_arg1811z00_6242;

							BgL_arg1811z00_6242 =
								PROCEDURE_REF(BGl_walk3za2zd2envz70zzast_walkz00, (int) (1L));
							{	/* Ast/walk.scm 141 */
								int BgL_offsetz00_6245;

								BgL_offsetz00_6245 = (int) (BgL_objzd2classzd2numz00_6241);
								{	/* Ast/walk.scm 141 */
									long BgL_offsetz00_6246;

									BgL_offsetz00_6246 =
										((long) (BgL_offsetz00_6245) - OBJECT_TYPE);
									{	/* Ast/walk.scm 141 */
										long BgL_modz00_6247;

										BgL_modz00_6247 =
											(BgL_offsetz00_6246 >> (int) ((long) ((int) (4L))));
										{	/* Ast/walk.scm 141 */
											long BgL_restz00_6249;

											BgL_restz00_6249 =
												(BgL_offsetz00_6246 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/walk.scm 141 */

												{	/* Ast/walk.scm 141 */
													obj_t BgL_bucketz00_6251;

													BgL_bucketz00_6251 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_6242), BgL_modz00_6247);
													BgL_res3699z00_6270 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_6251), BgL_restz00_6249);
					}}}}}}}}
					BgL_method1648z00_2497 = BgL_res3699z00_6270;
				}
				return
					BGL_PROCEDURE_CALL5(BgL_method1648z00_2497,
					((obj_t) BgL_nz00_32), BgL_pz00_33, BgL_arg0z00_34, BgL_arg1z00_35,
					BgL_arg2z00_36);
			}
		}

	}



/* &walk3* */
	obj_t BGl_z62walk3za2zc0zzast_walkz00(obj_t BgL_envz00_7174,
		obj_t BgL_nz00_7175, obj_t BgL_pz00_7176, obj_t BgL_arg0z00_7177,
		obj_t BgL_arg1z00_7178, obj_t BgL_arg2z00_7179)
	{
		{	/* Ast/walk.scm 141 */
			return
				BGl_walk3za2za2zzast_walkz00(
				((BgL_nodez00_bglt) BgL_nz00_7175), BgL_pz00_7176, BgL_arg0z00_7177,
				BgL_arg1z00_7178, BgL_arg2z00_7179);
		}

	}



/* walk4* */
	BGL_EXPORTED_DEF obj_t BGl_walk4za2za2zzast_walkz00(BgL_nodez00_bglt
		BgL_nz00_37, obj_t BgL_pz00_38, obj_t BgL_arg0z00_39, obj_t BgL_arg1z00_40,
		obj_t BgL_arg2z00_41, obj_t BgL_arg3z00_42)
	{
		{	/* Ast/walk.scm 144 */
			{	/* Ast/walk.scm 144 */
				obj_t BgL_method1650z00_2498;

				{	/* Ast/walk.scm 144 */
					obj_t BgL_res3704z00_6301;

					{	/* Ast/walk.scm 144 */
						long BgL_objzd2classzd2numz00_6272;

						BgL_objzd2classzd2numz00_6272 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nz00_37));
						{	/* Ast/walk.scm 144 */
							obj_t BgL_arg1811z00_6273;

							BgL_arg1811z00_6273 =
								PROCEDURE_REF(BGl_walk4za2zd2envz70zzast_walkz00, (int) (1L));
							{	/* Ast/walk.scm 144 */
								int BgL_offsetz00_6276;

								BgL_offsetz00_6276 = (int) (BgL_objzd2classzd2numz00_6272);
								{	/* Ast/walk.scm 144 */
									long BgL_offsetz00_6277;

									BgL_offsetz00_6277 =
										((long) (BgL_offsetz00_6276) - OBJECT_TYPE);
									{	/* Ast/walk.scm 144 */
										long BgL_modz00_6278;

										BgL_modz00_6278 =
											(BgL_offsetz00_6277 >> (int) ((long) ((int) (4L))));
										{	/* Ast/walk.scm 144 */
											long BgL_restz00_6280;

											BgL_restz00_6280 =
												(BgL_offsetz00_6277 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/walk.scm 144 */

												{	/* Ast/walk.scm 144 */
													obj_t BgL_bucketz00_6282;

													BgL_bucketz00_6282 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_6273), BgL_modz00_6278);
													BgL_res3704z00_6301 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_6282), BgL_restz00_6280);
					}}}}}}}}
					BgL_method1650z00_2498 = BgL_res3704z00_6301;
				}
				return
					BGL_PROCEDURE_CALL6(BgL_method1650z00_2498,
					((obj_t) BgL_nz00_37), BgL_pz00_38, BgL_arg0z00_39, BgL_arg1z00_40,
					BgL_arg2z00_41, BgL_arg3z00_42);
			}
		}

	}



/* &walk4* */
	obj_t BGl_z62walk4za2zc0zzast_walkz00(obj_t BgL_envz00_7180,
		obj_t BgL_nz00_7181, obj_t BgL_pz00_7182, obj_t BgL_arg0z00_7183,
		obj_t BgL_arg1z00_7184, obj_t BgL_arg2z00_7185, obj_t BgL_arg3z00_7186)
	{
		{	/* Ast/walk.scm 144 */
			return
				BGl_walk4za2za2zzast_walkz00(
				((BgL_nodez00_bglt) BgL_nz00_7181), BgL_pz00_7182, BgL_arg0z00_7183,
				BgL_arg1z00_7184, BgL_arg2z00_7185, BgL_arg3z00_7186);
		}

	}



/* walk0! */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_walk0z12z12zzast_walkz00(BgL_nodez00_bglt BgL_nz00_43,
		obj_t BgL_pz00_44)
	{
		{	/* Ast/walk.scm 148 */
			{	/* Ast/walk.scm 148 */
				obj_t BgL_method1652z00_2499;

				{	/* Ast/walk.scm 148 */
					obj_t BgL_res3709z00_6332;

					{	/* Ast/walk.scm 148 */
						long BgL_objzd2classzd2numz00_6303;

						BgL_objzd2classzd2numz00_6303 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nz00_43));
						{	/* Ast/walk.scm 148 */
							obj_t BgL_arg1811z00_6304;

							BgL_arg1811z00_6304 =
								PROCEDURE_REF(BGl_walk0z12zd2envzc0zzast_walkz00, (int) (1L));
							{	/* Ast/walk.scm 148 */
								int BgL_offsetz00_6307;

								BgL_offsetz00_6307 = (int) (BgL_objzd2classzd2numz00_6303);
								{	/* Ast/walk.scm 148 */
									long BgL_offsetz00_6308;

									BgL_offsetz00_6308 =
										((long) (BgL_offsetz00_6307) - OBJECT_TYPE);
									{	/* Ast/walk.scm 148 */
										long BgL_modz00_6309;

										BgL_modz00_6309 =
											(BgL_offsetz00_6308 >> (int) ((long) ((int) (4L))));
										{	/* Ast/walk.scm 148 */
											long BgL_restz00_6311;

											BgL_restz00_6311 =
												(BgL_offsetz00_6308 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/walk.scm 148 */

												{	/* Ast/walk.scm 148 */
													obj_t BgL_bucketz00_6313;

													BgL_bucketz00_6313 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_6304), BgL_modz00_6309);
													BgL_res3709z00_6332 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_6313), BgL_restz00_6311);
					}}}}}}}}
					BgL_method1652z00_2499 = BgL_res3709z00_6332;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL2(BgL_method1652z00_2499,
						((obj_t) BgL_nz00_43), BgL_pz00_44));
			}
		}

	}



/* &walk0! */
	BgL_nodez00_bglt BGl_z62walk0z12z70zzast_walkz00(obj_t BgL_envz00_7187,
		obj_t BgL_nz00_7188, obj_t BgL_pz00_7189)
	{
		{	/* Ast/walk.scm 148 */
			return
				BGl_walk0z12z12zzast_walkz00(
				((BgL_nodez00_bglt) BgL_nz00_7188), BgL_pz00_7189);
		}

	}



/* walk1! */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_walk1z12z12zzast_walkz00(BgL_nodez00_bglt BgL_nz00_45,
		obj_t BgL_pz00_46, obj_t BgL_arg0z00_47)
	{
		{	/* Ast/walk.scm 151 */
			{	/* Ast/walk.scm 151 */
				obj_t BgL_method1654z00_2500;

				{	/* Ast/walk.scm 151 */
					obj_t BgL_res3714z00_6363;

					{	/* Ast/walk.scm 151 */
						long BgL_objzd2classzd2numz00_6334;

						BgL_objzd2classzd2numz00_6334 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nz00_45));
						{	/* Ast/walk.scm 151 */
							obj_t BgL_arg1811z00_6335;

							BgL_arg1811z00_6335 =
								PROCEDURE_REF(BGl_walk1z12zd2envzc0zzast_walkz00, (int) (1L));
							{	/* Ast/walk.scm 151 */
								int BgL_offsetz00_6338;

								BgL_offsetz00_6338 = (int) (BgL_objzd2classzd2numz00_6334);
								{	/* Ast/walk.scm 151 */
									long BgL_offsetz00_6339;

									BgL_offsetz00_6339 =
										((long) (BgL_offsetz00_6338) - OBJECT_TYPE);
									{	/* Ast/walk.scm 151 */
										long BgL_modz00_6340;

										BgL_modz00_6340 =
											(BgL_offsetz00_6339 >> (int) ((long) ((int) (4L))));
										{	/* Ast/walk.scm 151 */
											long BgL_restz00_6342;

											BgL_restz00_6342 =
												(BgL_offsetz00_6339 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/walk.scm 151 */

												{	/* Ast/walk.scm 151 */
													obj_t BgL_bucketz00_6344;

													BgL_bucketz00_6344 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_6335), BgL_modz00_6340);
													BgL_res3714z00_6363 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_6344), BgL_restz00_6342);
					}}}}}}}}
					BgL_method1654z00_2500 = BgL_res3714z00_6363;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL3(BgL_method1654z00_2500,
						((obj_t) BgL_nz00_45), BgL_pz00_46, BgL_arg0z00_47));
			}
		}

	}



/* &walk1! */
	BgL_nodez00_bglt BGl_z62walk1z12z70zzast_walkz00(obj_t BgL_envz00_7190,
		obj_t BgL_nz00_7191, obj_t BgL_pz00_7192, obj_t BgL_arg0z00_7193)
	{
		{	/* Ast/walk.scm 151 */
			return
				BGl_walk1z12z12zzast_walkz00(
				((BgL_nodez00_bglt) BgL_nz00_7191), BgL_pz00_7192, BgL_arg0z00_7193);
		}

	}



/* walk2! */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_walk2z12z12zzast_walkz00(BgL_nodez00_bglt BgL_nz00_48,
		obj_t BgL_pz00_49, obj_t BgL_arg0z00_50, obj_t BgL_arg1z00_51)
	{
		{	/* Ast/walk.scm 154 */
			{	/* Ast/walk.scm 154 */
				obj_t BgL_method1657z00_2501;

				{	/* Ast/walk.scm 154 */
					obj_t BgL_res3719z00_6394;

					{	/* Ast/walk.scm 154 */
						long BgL_objzd2classzd2numz00_6365;

						BgL_objzd2classzd2numz00_6365 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nz00_48));
						{	/* Ast/walk.scm 154 */
							obj_t BgL_arg1811z00_6366;

							BgL_arg1811z00_6366 =
								PROCEDURE_REF(BGl_walk2z12zd2envzc0zzast_walkz00, (int) (1L));
							{	/* Ast/walk.scm 154 */
								int BgL_offsetz00_6369;

								BgL_offsetz00_6369 = (int) (BgL_objzd2classzd2numz00_6365);
								{	/* Ast/walk.scm 154 */
									long BgL_offsetz00_6370;

									BgL_offsetz00_6370 =
										((long) (BgL_offsetz00_6369) - OBJECT_TYPE);
									{	/* Ast/walk.scm 154 */
										long BgL_modz00_6371;

										BgL_modz00_6371 =
											(BgL_offsetz00_6370 >> (int) ((long) ((int) (4L))));
										{	/* Ast/walk.scm 154 */
											long BgL_restz00_6373;

											BgL_restz00_6373 =
												(BgL_offsetz00_6370 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/walk.scm 154 */

												{	/* Ast/walk.scm 154 */
													obj_t BgL_bucketz00_6375;

													BgL_bucketz00_6375 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_6366), BgL_modz00_6371);
													BgL_res3719z00_6394 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_6375), BgL_restz00_6373);
					}}}}}}}}
					BgL_method1657z00_2501 = BgL_res3719z00_6394;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL4(BgL_method1657z00_2501,
						((obj_t) BgL_nz00_48), BgL_pz00_49, BgL_arg0z00_50,
						BgL_arg1z00_51));
			}
		}

	}



/* &walk2! */
	BgL_nodez00_bglt BGl_z62walk2z12z70zzast_walkz00(obj_t BgL_envz00_7194,
		obj_t BgL_nz00_7195, obj_t BgL_pz00_7196, obj_t BgL_arg0z00_7197,
		obj_t BgL_arg1z00_7198)
	{
		{	/* Ast/walk.scm 154 */
			return
				BGl_walk2z12z12zzast_walkz00(
				((BgL_nodez00_bglt) BgL_nz00_7195), BgL_pz00_7196, BgL_arg0z00_7197,
				BgL_arg1z00_7198);
		}

	}



/* walk3! */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_walk3z12z12zzast_walkz00(BgL_nodez00_bglt BgL_nz00_52,
		obj_t BgL_pz00_53, obj_t BgL_arg0z00_54, obj_t BgL_arg1z00_55,
		obj_t BgL_arg2z00_56)
	{
		{	/* Ast/walk.scm 157 */
			{	/* Ast/walk.scm 157 */
				obj_t BgL_method1659z00_2502;

				{	/* Ast/walk.scm 157 */
					obj_t BgL_res3724z00_6425;

					{	/* Ast/walk.scm 157 */
						long BgL_objzd2classzd2numz00_6396;

						BgL_objzd2classzd2numz00_6396 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nz00_52));
						{	/* Ast/walk.scm 157 */
							obj_t BgL_arg1811z00_6397;

							BgL_arg1811z00_6397 =
								PROCEDURE_REF(BGl_walk3z12zd2envzc0zzast_walkz00, (int) (1L));
							{	/* Ast/walk.scm 157 */
								int BgL_offsetz00_6400;

								BgL_offsetz00_6400 = (int) (BgL_objzd2classzd2numz00_6396);
								{	/* Ast/walk.scm 157 */
									long BgL_offsetz00_6401;

									BgL_offsetz00_6401 =
										((long) (BgL_offsetz00_6400) - OBJECT_TYPE);
									{	/* Ast/walk.scm 157 */
										long BgL_modz00_6402;

										BgL_modz00_6402 =
											(BgL_offsetz00_6401 >> (int) ((long) ((int) (4L))));
										{	/* Ast/walk.scm 157 */
											long BgL_restz00_6404;

											BgL_restz00_6404 =
												(BgL_offsetz00_6401 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/walk.scm 157 */

												{	/* Ast/walk.scm 157 */
													obj_t BgL_bucketz00_6406;

													BgL_bucketz00_6406 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_6397), BgL_modz00_6402);
													BgL_res3724z00_6425 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_6406), BgL_restz00_6404);
					}}}}}}}}
					BgL_method1659z00_2502 = BgL_res3724z00_6425;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL5(BgL_method1659z00_2502,
						((obj_t) BgL_nz00_52), BgL_pz00_53, BgL_arg0z00_54, BgL_arg1z00_55,
						BgL_arg2z00_56));
			}
		}

	}



/* &walk3! */
	BgL_nodez00_bglt BGl_z62walk3z12z70zzast_walkz00(obj_t BgL_envz00_7199,
		obj_t BgL_nz00_7200, obj_t BgL_pz00_7201, obj_t BgL_arg0z00_7202,
		obj_t BgL_arg1z00_7203, obj_t BgL_arg2z00_7204)
	{
		{	/* Ast/walk.scm 157 */
			return
				BGl_walk3z12z12zzast_walkz00(
				((BgL_nodez00_bglt) BgL_nz00_7200), BgL_pz00_7201, BgL_arg0z00_7202,
				BgL_arg1z00_7203, BgL_arg2z00_7204);
		}

	}



/* walk4! */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_walk4z12z12zzast_walkz00(BgL_nodez00_bglt BgL_nz00_57,
		obj_t BgL_pz00_58, obj_t BgL_arg0z00_59, obj_t BgL_arg1z00_60,
		obj_t BgL_arg2z00_61, obj_t BgL_arg3z00_62)
	{
		{	/* Ast/walk.scm 160 */
			{	/* Ast/walk.scm 160 */
				obj_t BgL_method1661z00_2503;

				{	/* Ast/walk.scm 160 */
					obj_t BgL_res3729z00_6456;

					{	/* Ast/walk.scm 160 */
						long BgL_objzd2classzd2numz00_6427;

						BgL_objzd2classzd2numz00_6427 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nz00_57));
						{	/* Ast/walk.scm 160 */
							obj_t BgL_arg1811z00_6428;

							BgL_arg1811z00_6428 =
								PROCEDURE_REF(BGl_walk4z12zd2envzc0zzast_walkz00, (int) (1L));
							{	/* Ast/walk.scm 160 */
								int BgL_offsetz00_6431;

								BgL_offsetz00_6431 = (int) (BgL_objzd2classzd2numz00_6427);
								{	/* Ast/walk.scm 160 */
									long BgL_offsetz00_6432;

									BgL_offsetz00_6432 =
										((long) (BgL_offsetz00_6431) - OBJECT_TYPE);
									{	/* Ast/walk.scm 160 */
										long BgL_modz00_6433;

										BgL_modz00_6433 =
											(BgL_offsetz00_6432 >> (int) ((long) ((int) (4L))));
										{	/* Ast/walk.scm 160 */
											long BgL_restz00_6435;

											BgL_restz00_6435 =
												(BgL_offsetz00_6432 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/walk.scm 160 */

												{	/* Ast/walk.scm 160 */
													obj_t BgL_bucketz00_6437;

													BgL_bucketz00_6437 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_6428), BgL_modz00_6433);
													BgL_res3729z00_6456 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_6437), BgL_restz00_6435);
					}}}}}}}}
					BgL_method1661z00_2503 = BgL_res3729z00_6456;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL6(BgL_method1661z00_2503,
						((obj_t) BgL_nz00_57), BgL_pz00_58, BgL_arg0z00_59, BgL_arg1z00_60,
						BgL_arg2z00_61, BgL_arg3z00_62));
			}
		}

	}



/* &walk4! */
	BgL_nodez00_bglt BGl_z62walk4z12z70zzast_walkz00(obj_t BgL_envz00_7205,
		obj_t BgL_nz00_7206, obj_t BgL_pz00_7207, obj_t BgL_arg0z00_7208,
		obj_t BgL_arg1z00_7209, obj_t BgL_arg2z00_7210, obj_t BgL_arg3z00_7211)
	{
		{	/* Ast/walk.scm 160 */
			return
				BGl_walk4z12z12zzast_walkz00(
				((BgL_nodez00_bglt) BgL_nz00_7206), BgL_pz00_7207, BgL_arg0z00_7208,
				BgL_arg1z00_7209, BgL_arg2z00_7210, BgL_arg3z00_7211);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_walkz00(void)
	{
		{	/* Ast/walk.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0zd2envzd2zzast_walkz00, BGl_nodez00zzast_nodez00,
				BGl_proc3775z00zzast_walkz00, BGl_string3774z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1zd2envzd2zzast_walkz00, BGl_nodez00zzast_nodez00,
				BGl_proc3776z00zzast_walkz00, BGl_string3773z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2zd2envzd2zzast_walkz00, BGl_nodez00zzast_nodez00,
				BGl_proc3777z00zzast_walkz00, BGl_string3772z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3zd2envzd2zzast_walkz00, BGl_nodez00zzast_nodez00,
				BGl_proc3778z00zzast_walkz00, BGl_string3771z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0za2zd2envz70zzast_walkz00, BGl_nodez00zzast_nodez00,
				BGl_proc3779z00zzast_walkz00, BGl_string3769z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1za2zd2envz70zzast_walkz00, BGl_nodez00zzast_nodez00,
				BGl_proc3780z00zzast_walkz00, BGl_string3768z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2za2zd2envz70zzast_walkz00, BGl_nodez00zzast_nodez00,
				BGl_proc3781z00zzast_walkz00, BGl_string3767z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3za2zd2envz70zzast_walkz00, BGl_nodez00zzast_nodez00,
				BGl_proc3782z00zzast_walkz00, BGl_string3766z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0z12zd2envzc0zzast_walkz00, BGl_nodez00zzast_nodez00,
				BGl_proc3783z00zzast_walkz00, BGl_string3765z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1z12zd2envzc0zzast_walkz00, BGl_nodez00zzast_nodez00,
				BGl_proc3784z00zzast_walkz00, BGl_string3764z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2z12zd2envzc0zzast_walkz00, BGl_nodez00zzast_nodez00,
				BGl_proc3785z00zzast_walkz00, BGl_string3763z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3z12zd2envzc0zzast_walkz00, BGl_nodez00zzast_nodez00,
				BGl_proc3786z00zzast_walkz00, BGl_string3762z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0zd2envzd2zzast_walkz00, BGl_sequencez00zzast_nodez00,
				BGl_proc3787z00zzast_walkz00, BGl_string3774z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1zd2envzd2zzast_walkz00, BGl_sequencez00zzast_nodez00,
				BGl_proc3788z00zzast_walkz00, BGl_string3773z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2zd2envzd2zzast_walkz00, BGl_sequencez00zzast_nodez00,
				BGl_proc3789z00zzast_walkz00, BGl_string3772z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3zd2envzd2zzast_walkz00, BGl_sequencez00zzast_nodez00,
				BGl_proc3790z00zzast_walkz00, BGl_string3771z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0za2zd2envz70zzast_walkz00, BGl_sequencez00zzast_nodez00,
				BGl_proc3791z00zzast_walkz00, BGl_string3769z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1za2zd2envz70zzast_walkz00, BGl_sequencez00zzast_nodez00,
				BGl_proc3792z00zzast_walkz00, BGl_string3768z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2za2zd2envz70zzast_walkz00, BGl_sequencez00zzast_nodez00,
				BGl_proc3793z00zzast_walkz00, BGl_string3767z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3za2zd2envz70zzast_walkz00, BGl_sequencez00zzast_nodez00,
				BGl_proc3794z00zzast_walkz00, BGl_string3766z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0z12zd2envzc0zzast_walkz00, BGl_sequencez00zzast_nodez00,
				BGl_proc3795z00zzast_walkz00, BGl_string3765z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1z12zd2envzc0zzast_walkz00, BGl_sequencez00zzast_nodez00,
				BGl_proc3796z00zzast_walkz00, BGl_string3764z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2z12zd2envzc0zzast_walkz00, BGl_sequencez00zzast_nodez00,
				BGl_proc3797z00zzast_walkz00, BGl_string3763z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3z12zd2envzc0zzast_walkz00, BGl_sequencez00zzast_nodez00,
				BGl_proc3798z00zzast_walkz00, BGl_string3762z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0zd2envzd2zzast_walkz00, BGl_appz00zzast_nodez00,
				BGl_proc3799z00zzast_walkz00, BGl_string3774z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1zd2envzd2zzast_walkz00, BGl_appz00zzast_nodez00,
				BGl_proc3800z00zzast_walkz00, BGl_string3773z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2zd2envzd2zzast_walkz00, BGl_appz00zzast_nodez00,
				BGl_proc3801z00zzast_walkz00, BGl_string3772z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3zd2envzd2zzast_walkz00, BGl_appz00zzast_nodez00,
				BGl_proc3802z00zzast_walkz00, BGl_string3771z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0za2zd2envz70zzast_walkz00, BGl_appz00zzast_nodez00,
				BGl_proc3803z00zzast_walkz00, BGl_string3769z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1za2zd2envz70zzast_walkz00, BGl_appz00zzast_nodez00,
				BGl_proc3804z00zzast_walkz00, BGl_string3768z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2za2zd2envz70zzast_walkz00, BGl_appz00zzast_nodez00,
				BGl_proc3805z00zzast_walkz00, BGl_string3767z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3za2zd2envz70zzast_walkz00, BGl_appz00zzast_nodez00,
				BGl_proc3806z00zzast_walkz00, BGl_string3766z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0z12zd2envzc0zzast_walkz00, BGl_appz00zzast_nodez00,
				BGl_proc3807z00zzast_walkz00, BGl_string3765z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1z12zd2envzc0zzast_walkz00, BGl_appz00zzast_nodez00,
				BGl_proc3808z00zzast_walkz00, BGl_string3764z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2z12zd2envzc0zzast_walkz00, BGl_appz00zzast_nodez00,
				BGl_proc3809z00zzast_walkz00, BGl_string3763z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3z12zd2envzc0zzast_walkz00, BGl_appz00zzast_nodez00,
				BGl_proc3810z00zzast_walkz00, BGl_string3762z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0zd2envzd2zzast_walkz00, BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc3811z00zzast_walkz00, BGl_string3774z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1zd2envzd2zzast_walkz00, BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc3812z00zzast_walkz00, BGl_string3773z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2zd2envzd2zzast_walkz00, BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc3813z00zzast_walkz00, BGl_string3772z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3zd2envzd2zzast_walkz00, BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc3814z00zzast_walkz00, BGl_string3771z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0za2zd2envz70zzast_walkz00, BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc3815z00zzast_walkz00, BGl_string3769z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1za2zd2envz70zzast_walkz00, BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc3816z00zzast_walkz00, BGl_string3768z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2za2zd2envz70zzast_walkz00, BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc3817z00zzast_walkz00, BGl_string3767z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3za2zd2envz70zzast_walkz00, BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc3818z00zzast_walkz00, BGl_string3766z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0z12zd2envzc0zzast_walkz00, BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc3819z00zzast_walkz00, BGl_string3765z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1z12zd2envzc0zzast_walkz00, BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc3820z00zzast_walkz00, BGl_string3764z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2z12zd2envzc0zzast_walkz00, BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc3821z00zzast_walkz00, BGl_string3763z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3z12zd2envzc0zzast_walkz00, BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc3822z00zzast_walkz00, BGl_string3762z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0zd2envzd2zzast_walkz00, BGl_funcallz00zzast_nodez00,
				BGl_proc3823z00zzast_walkz00, BGl_string3774z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1zd2envzd2zzast_walkz00, BGl_funcallz00zzast_nodez00,
				BGl_proc3824z00zzast_walkz00, BGl_string3773z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2zd2envzd2zzast_walkz00, BGl_funcallz00zzast_nodez00,
				BGl_proc3825z00zzast_walkz00, BGl_string3772z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3zd2envzd2zzast_walkz00, BGl_funcallz00zzast_nodez00,
				BGl_proc3826z00zzast_walkz00, BGl_string3771z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0za2zd2envz70zzast_walkz00, BGl_funcallz00zzast_nodez00,
				BGl_proc3827z00zzast_walkz00, BGl_string3769z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1za2zd2envz70zzast_walkz00, BGl_funcallz00zzast_nodez00,
				BGl_proc3828z00zzast_walkz00, BGl_string3768z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2za2zd2envz70zzast_walkz00, BGl_funcallz00zzast_nodez00,
				BGl_proc3829z00zzast_walkz00, BGl_string3767z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3za2zd2envz70zzast_walkz00, BGl_funcallz00zzast_nodez00,
				BGl_proc3830z00zzast_walkz00, BGl_string3766z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0z12zd2envzc0zzast_walkz00, BGl_funcallz00zzast_nodez00,
				BGl_proc3831z00zzast_walkz00, BGl_string3765z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1z12zd2envzc0zzast_walkz00, BGl_funcallz00zzast_nodez00,
				BGl_proc3832z00zzast_walkz00, BGl_string3764z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2z12zd2envzc0zzast_walkz00, BGl_funcallz00zzast_nodez00,
				BGl_proc3833z00zzast_walkz00, BGl_string3763z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3z12zd2envzc0zzast_walkz00, BGl_funcallz00zzast_nodez00,
				BGl_proc3834z00zzast_walkz00, BGl_string3762z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0zd2envzd2zzast_walkz00, BGl_externz00zzast_nodez00,
				BGl_proc3835z00zzast_walkz00, BGl_string3774z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1zd2envzd2zzast_walkz00, BGl_externz00zzast_nodez00,
				BGl_proc3836z00zzast_walkz00, BGl_string3773z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2zd2envzd2zzast_walkz00, BGl_externz00zzast_nodez00,
				BGl_proc3837z00zzast_walkz00, BGl_string3772z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3zd2envzd2zzast_walkz00, BGl_externz00zzast_nodez00,
				BGl_proc3838z00zzast_walkz00, BGl_string3771z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0za2zd2envz70zzast_walkz00, BGl_externz00zzast_nodez00,
				BGl_proc3839z00zzast_walkz00, BGl_string3769z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1za2zd2envz70zzast_walkz00, BGl_externz00zzast_nodez00,
				BGl_proc3840z00zzast_walkz00, BGl_string3768z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2za2zd2envz70zzast_walkz00, BGl_externz00zzast_nodez00,
				BGl_proc3841z00zzast_walkz00, BGl_string3767z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3za2zd2envz70zzast_walkz00, BGl_externz00zzast_nodez00,
				BGl_proc3842z00zzast_walkz00, BGl_string3766z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0z12zd2envzc0zzast_walkz00, BGl_externz00zzast_nodez00,
				BGl_proc3843z00zzast_walkz00, BGl_string3765z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1z12zd2envzc0zzast_walkz00, BGl_externz00zzast_nodez00,
				BGl_proc3844z00zzast_walkz00, BGl_string3764z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2z12zd2envzc0zzast_walkz00, BGl_externz00zzast_nodez00,
				BGl_proc3845z00zzast_walkz00, BGl_string3763z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3z12zd2envzc0zzast_walkz00, BGl_externz00zzast_nodez00,
				BGl_proc3846z00zzast_walkz00, BGl_string3762z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0zd2envzd2zzast_walkz00, BGl_castz00zzast_nodez00,
				BGl_proc3847z00zzast_walkz00, BGl_string3774z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1zd2envzd2zzast_walkz00, BGl_castz00zzast_nodez00,
				BGl_proc3848z00zzast_walkz00, BGl_string3773z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2zd2envzd2zzast_walkz00, BGl_castz00zzast_nodez00,
				BGl_proc3849z00zzast_walkz00, BGl_string3772z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3zd2envzd2zzast_walkz00, BGl_castz00zzast_nodez00,
				BGl_proc3850z00zzast_walkz00, BGl_string3771z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0za2zd2envz70zzast_walkz00, BGl_castz00zzast_nodez00,
				BGl_proc3851z00zzast_walkz00, BGl_string3769z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1za2zd2envz70zzast_walkz00, BGl_castz00zzast_nodez00,
				BGl_proc3852z00zzast_walkz00, BGl_string3768z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2za2zd2envz70zzast_walkz00, BGl_castz00zzast_nodez00,
				BGl_proc3853z00zzast_walkz00, BGl_string3767z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3za2zd2envz70zzast_walkz00, BGl_castz00zzast_nodez00,
				BGl_proc3854z00zzast_walkz00, BGl_string3766z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0z12zd2envzc0zzast_walkz00, BGl_castz00zzast_nodez00,
				BGl_proc3855z00zzast_walkz00, BGl_string3765z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1z12zd2envzc0zzast_walkz00, BGl_castz00zzast_nodez00,
				BGl_proc3856z00zzast_walkz00, BGl_string3764z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2z12zd2envzc0zzast_walkz00, BGl_castz00zzast_nodez00,
				BGl_proc3857z00zzast_walkz00, BGl_string3763z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3z12zd2envzc0zzast_walkz00, BGl_castz00zzast_nodez00,
				BGl_proc3858z00zzast_walkz00, BGl_string3762z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0zd2envzd2zzast_walkz00, BGl_setqz00zzast_nodez00,
				BGl_proc3859z00zzast_walkz00, BGl_string3774z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1zd2envzd2zzast_walkz00, BGl_setqz00zzast_nodez00,
				BGl_proc3860z00zzast_walkz00, BGl_string3773z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2zd2envzd2zzast_walkz00, BGl_setqz00zzast_nodez00,
				BGl_proc3861z00zzast_walkz00, BGl_string3772z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3zd2envzd2zzast_walkz00, BGl_setqz00zzast_nodez00,
				BGl_proc3862z00zzast_walkz00, BGl_string3771z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0za2zd2envz70zzast_walkz00, BGl_setqz00zzast_nodez00,
				BGl_proc3863z00zzast_walkz00, BGl_string3769z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1za2zd2envz70zzast_walkz00, BGl_setqz00zzast_nodez00,
				BGl_proc3864z00zzast_walkz00, BGl_string3768z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2za2zd2envz70zzast_walkz00, BGl_setqz00zzast_nodez00,
				BGl_proc3865z00zzast_walkz00, BGl_string3767z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3za2zd2envz70zzast_walkz00, BGl_setqz00zzast_nodez00,
				BGl_proc3866z00zzast_walkz00, BGl_string3766z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0z12zd2envzc0zzast_walkz00, BGl_setqz00zzast_nodez00,
				BGl_proc3867z00zzast_walkz00, BGl_string3765z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1z12zd2envzc0zzast_walkz00, BGl_setqz00zzast_nodez00,
				BGl_proc3868z00zzast_walkz00, BGl_string3764z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2z12zd2envzc0zzast_walkz00, BGl_setqz00zzast_nodez00,
				BGl_proc3869z00zzast_walkz00, BGl_string3763z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3z12zd2envzc0zzast_walkz00, BGl_setqz00zzast_nodez00,
				BGl_proc3870z00zzast_walkz00, BGl_string3762z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0zd2envzd2zzast_walkz00, BGl_conditionalz00zzast_nodez00,
				BGl_proc3871z00zzast_walkz00, BGl_string3774z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1zd2envzd2zzast_walkz00, BGl_conditionalz00zzast_nodez00,
				BGl_proc3872z00zzast_walkz00, BGl_string3773z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2zd2envzd2zzast_walkz00, BGl_conditionalz00zzast_nodez00,
				BGl_proc3873z00zzast_walkz00, BGl_string3772z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3zd2envzd2zzast_walkz00, BGl_conditionalz00zzast_nodez00,
				BGl_proc3874z00zzast_walkz00, BGl_string3771z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0za2zd2envz70zzast_walkz00, BGl_conditionalz00zzast_nodez00,
				BGl_proc3875z00zzast_walkz00, BGl_string3769z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1za2zd2envz70zzast_walkz00, BGl_conditionalz00zzast_nodez00,
				BGl_proc3876z00zzast_walkz00, BGl_string3768z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2za2zd2envz70zzast_walkz00, BGl_conditionalz00zzast_nodez00,
				BGl_proc3877z00zzast_walkz00, BGl_string3767z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3za2zd2envz70zzast_walkz00, BGl_conditionalz00zzast_nodez00,
				BGl_proc3878z00zzast_walkz00, BGl_string3766z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0z12zd2envzc0zzast_walkz00, BGl_conditionalz00zzast_nodez00,
				BGl_proc3879z00zzast_walkz00, BGl_string3765z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1z12zd2envzc0zzast_walkz00, BGl_conditionalz00zzast_nodez00,
				BGl_proc3880z00zzast_walkz00, BGl_string3764z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2z12zd2envzc0zzast_walkz00, BGl_conditionalz00zzast_nodez00,
				BGl_proc3881z00zzast_walkz00, BGl_string3763z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3z12zd2envzc0zzast_walkz00, BGl_conditionalz00zzast_nodez00,
				BGl_proc3882z00zzast_walkz00, BGl_string3762z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0zd2envzd2zzast_walkz00, BGl_failz00zzast_nodez00,
				BGl_proc3883z00zzast_walkz00, BGl_string3774z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1zd2envzd2zzast_walkz00, BGl_failz00zzast_nodez00,
				BGl_proc3884z00zzast_walkz00, BGl_string3773z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2zd2envzd2zzast_walkz00, BGl_failz00zzast_nodez00,
				BGl_proc3885z00zzast_walkz00, BGl_string3772z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3zd2envzd2zzast_walkz00, BGl_failz00zzast_nodez00,
				BGl_proc3886z00zzast_walkz00, BGl_string3771z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0za2zd2envz70zzast_walkz00, BGl_failz00zzast_nodez00,
				BGl_proc3887z00zzast_walkz00, BGl_string3769z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1za2zd2envz70zzast_walkz00, BGl_failz00zzast_nodez00,
				BGl_proc3888z00zzast_walkz00, BGl_string3768z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2za2zd2envz70zzast_walkz00, BGl_failz00zzast_nodez00,
				BGl_proc3889z00zzast_walkz00, BGl_string3767z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3za2zd2envz70zzast_walkz00, BGl_failz00zzast_nodez00,
				BGl_proc3890z00zzast_walkz00, BGl_string3766z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0z12zd2envzc0zzast_walkz00, BGl_failz00zzast_nodez00,
				BGl_proc3891z00zzast_walkz00, BGl_string3765z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1z12zd2envzc0zzast_walkz00, BGl_failz00zzast_nodez00,
				BGl_proc3892z00zzast_walkz00, BGl_string3764z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2z12zd2envzc0zzast_walkz00, BGl_failz00zzast_nodez00,
				BGl_proc3893z00zzast_walkz00, BGl_string3763z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3z12zd2envzc0zzast_walkz00, BGl_failz00zzast_nodez00,
				BGl_proc3894z00zzast_walkz00, BGl_string3762z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0zd2envzd2zzast_walkz00, BGl_setzd2exzd2itz00zzast_nodez00,
				BGl_proc3895z00zzast_walkz00, BGl_string3774z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1zd2envzd2zzast_walkz00, BGl_setzd2exzd2itz00zzast_nodez00,
				BGl_proc3896z00zzast_walkz00, BGl_string3773z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2zd2envzd2zzast_walkz00, BGl_setzd2exzd2itz00zzast_nodez00,
				BGl_proc3897z00zzast_walkz00, BGl_string3772z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3zd2envzd2zzast_walkz00, BGl_setzd2exzd2itz00zzast_nodez00,
				BGl_proc3898z00zzast_walkz00, BGl_string3771z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0za2zd2envz70zzast_walkz00, BGl_setzd2exzd2itz00zzast_nodez00,
				BGl_proc3899z00zzast_walkz00, BGl_string3769z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1za2zd2envz70zzast_walkz00, BGl_setzd2exzd2itz00zzast_nodez00,
				BGl_proc3900z00zzast_walkz00, BGl_string3768z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2za2zd2envz70zzast_walkz00, BGl_setzd2exzd2itz00zzast_nodez00,
				BGl_proc3901z00zzast_walkz00, BGl_string3767z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3za2zd2envz70zzast_walkz00, BGl_setzd2exzd2itz00zzast_nodez00,
				BGl_proc3902z00zzast_walkz00, BGl_string3766z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0z12zd2envzc0zzast_walkz00, BGl_setzd2exzd2itz00zzast_nodez00,
				BGl_proc3903z00zzast_walkz00, BGl_string3765z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1z12zd2envzc0zzast_walkz00, BGl_setzd2exzd2itz00zzast_nodez00,
				BGl_proc3904z00zzast_walkz00, BGl_string3764z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2z12zd2envzc0zzast_walkz00, BGl_setzd2exzd2itz00zzast_nodez00,
				BGl_proc3905z00zzast_walkz00, BGl_string3763z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3z12zd2envzc0zzast_walkz00, BGl_setzd2exzd2itz00zzast_nodez00,
				BGl_proc3906z00zzast_walkz00, BGl_string3762z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0zd2envzd2zzast_walkz00, BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc3907z00zzast_walkz00, BGl_string3774z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1zd2envzd2zzast_walkz00, BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc3908z00zzast_walkz00, BGl_string3773z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2zd2envzd2zzast_walkz00, BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc3909z00zzast_walkz00, BGl_string3772z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3zd2envzd2zzast_walkz00, BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc3910z00zzast_walkz00, BGl_string3771z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0za2zd2envz70zzast_walkz00, BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc3911z00zzast_walkz00, BGl_string3769z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1za2zd2envz70zzast_walkz00, BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc3912z00zzast_walkz00, BGl_string3768z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2za2zd2envz70zzast_walkz00, BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc3913z00zzast_walkz00, BGl_string3767z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3za2zd2envz70zzast_walkz00, BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc3914z00zzast_walkz00, BGl_string3766z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0z12zd2envzc0zzast_walkz00, BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc3915z00zzast_walkz00, BGl_string3765z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1z12zd2envzc0zzast_walkz00, BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc3916z00zzast_walkz00, BGl_string3764z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2z12zd2envzc0zzast_walkz00, BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc3917z00zzast_walkz00, BGl_string3763z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3z12zd2envzc0zzast_walkz00, BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc3918z00zzast_walkz00, BGl_string3762z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0zd2envzd2zzast_walkz00, BGl_retblockz00zzast_nodez00,
				BGl_proc3919z00zzast_walkz00, BGl_string3774z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1zd2envzd2zzast_walkz00, BGl_retblockz00zzast_nodez00,
				BGl_proc3920z00zzast_walkz00, BGl_string3773z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2zd2envzd2zzast_walkz00, BGl_retblockz00zzast_nodez00,
				BGl_proc3921z00zzast_walkz00, BGl_string3772z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3zd2envzd2zzast_walkz00, BGl_retblockz00zzast_nodez00,
				BGl_proc3922z00zzast_walkz00, BGl_string3771z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0za2zd2envz70zzast_walkz00, BGl_retblockz00zzast_nodez00,
				BGl_proc3923z00zzast_walkz00, BGl_string3769z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1za2zd2envz70zzast_walkz00, BGl_retblockz00zzast_nodez00,
				BGl_proc3924z00zzast_walkz00, BGl_string3768z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2za2zd2envz70zzast_walkz00, BGl_retblockz00zzast_nodez00,
				BGl_proc3925z00zzast_walkz00, BGl_string3767z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3za2zd2envz70zzast_walkz00, BGl_retblockz00zzast_nodez00,
				BGl_proc3926z00zzast_walkz00, BGl_string3766z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0z12zd2envzc0zzast_walkz00, BGl_retblockz00zzast_nodez00,
				BGl_proc3927z00zzast_walkz00, BGl_string3765z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1z12zd2envzc0zzast_walkz00, BGl_retblockz00zzast_nodez00,
				BGl_proc3928z00zzast_walkz00, BGl_string3764z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2z12zd2envzc0zzast_walkz00, BGl_retblockz00zzast_nodez00,
				BGl_proc3929z00zzast_walkz00, BGl_string3763z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3z12zd2envzc0zzast_walkz00, BGl_retblockz00zzast_nodez00,
				BGl_proc3930z00zzast_walkz00, BGl_string3762z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0zd2envzd2zzast_walkz00, BGl_returnz00zzast_nodez00,
				BGl_proc3931z00zzast_walkz00, BGl_string3774z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1zd2envzd2zzast_walkz00, BGl_returnz00zzast_nodez00,
				BGl_proc3932z00zzast_walkz00, BGl_string3773z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2zd2envzd2zzast_walkz00, BGl_returnz00zzast_nodez00,
				BGl_proc3933z00zzast_walkz00, BGl_string3772z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3zd2envzd2zzast_walkz00, BGl_returnz00zzast_nodez00,
				BGl_proc3934z00zzast_walkz00, BGl_string3771z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0za2zd2envz70zzast_walkz00, BGl_returnz00zzast_nodez00,
				BGl_proc3935z00zzast_walkz00, BGl_string3769z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1za2zd2envz70zzast_walkz00, BGl_returnz00zzast_nodez00,
				BGl_proc3936z00zzast_walkz00, BGl_string3768z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2za2zd2envz70zzast_walkz00, BGl_returnz00zzast_nodez00,
				BGl_proc3937z00zzast_walkz00, BGl_string3767z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3za2zd2envz70zzast_walkz00, BGl_returnz00zzast_nodez00,
				BGl_proc3938z00zzast_walkz00, BGl_string3766z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0z12zd2envzc0zzast_walkz00, BGl_returnz00zzast_nodez00,
				BGl_proc3939z00zzast_walkz00, BGl_string3765z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1z12zd2envzc0zzast_walkz00, BGl_returnz00zzast_nodez00,
				BGl_proc3940z00zzast_walkz00, BGl_string3764z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2z12zd2envzc0zzast_walkz00, BGl_returnz00zzast_nodez00,
				BGl_proc3941z00zzast_walkz00, BGl_string3763z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3z12zd2envzc0zzast_walkz00, BGl_returnz00zzast_nodez00,
				BGl_proc3942z00zzast_walkz00, BGl_string3762z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0zd2envzd2zzast_walkz00, BGl_makezd2boxzd2zzast_nodez00,
				BGl_proc3943z00zzast_walkz00, BGl_string3774z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1zd2envzd2zzast_walkz00, BGl_makezd2boxzd2zzast_nodez00,
				BGl_proc3944z00zzast_walkz00, BGl_string3773z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2zd2envzd2zzast_walkz00, BGl_makezd2boxzd2zzast_nodez00,
				BGl_proc3945z00zzast_walkz00, BGl_string3772z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3zd2envzd2zzast_walkz00, BGl_makezd2boxzd2zzast_nodez00,
				BGl_proc3946z00zzast_walkz00, BGl_string3771z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0za2zd2envz70zzast_walkz00, BGl_makezd2boxzd2zzast_nodez00,
				BGl_proc3947z00zzast_walkz00, BGl_string3769z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1za2zd2envz70zzast_walkz00, BGl_makezd2boxzd2zzast_nodez00,
				BGl_proc3948z00zzast_walkz00, BGl_string3768z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2za2zd2envz70zzast_walkz00, BGl_makezd2boxzd2zzast_nodez00,
				BGl_proc3949z00zzast_walkz00, BGl_string3767z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3za2zd2envz70zzast_walkz00, BGl_makezd2boxzd2zzast_nodez00,
				BGl_proc3950z00zzast_walkz00, BGl_string3766z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0z12zd2envzc0zzast_walkz00, BGl_makezd2boxzd2zzast_nodez00,
				BGl_proc3951z00zzast_walkz00, BGl_string3765z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1z12zd2envzc0zzast_walkz00, BGl_makezd2boxzd2zzast_nodez00,
				BGl_proc3952z00zzast_walkz00, BGl_string3764z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2z12zd2envzc0zzast_walkz00, BGl_makezd2boxzd2zzast_nodez00,
				BGl_proc3953z00zzast_walkz00, BGl_string3763z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3z12zd2envzc0zzast_walkz00, BGl_makezd2boxzd2zzast_nodez00,
				BGl_proc3954z00zzast_walkz00, BGl_string3762z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0zd2envzd2zzast_walkz00, BGl_boxzd2refzd2zzast_nodez00,
				BGl_proc3955z00zzast_walkz00, BGl_string3774z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1zd2envzd2zzast_walkz00, BGl_boxzd2refzd2zzast_nodez00,
				BGl_proc3956z00zzast_walkz00, BGl_string3773z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2zd2envzd2zzast_walkz00, BGl_boxzd2refzd2zzast_nodez00,
				BGl_proc3957z00zzast_walkz00, BGl_string3772z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3zd2envzd2zzast_walkz00, BGl_boxzd2refzd2zzast_nodez00,
				BGl_proc3958z00zzast_walkz00, BGl_string3771z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0za2zd2envz70zzast_walkz00, BGl_boxzd2refzd2zzast_nodez00,
				BGl_proc3959z00zzast_walkz00, BGl_string3769z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1za2zd2envz70zzast_walkz00, BGl_boxzd2refzd2zzast_nodez00,
				BGl_proc3960z00zzast_walkz00, BGl_string3768z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2za2zd2envz70zzast_walkz00, BGl_boxzd2refzd2zzast_nodez00,
				BGl_proc3961z00zzast_walkz00, BGl_string3767z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3za2zd2envz70zzast_walkz00, BGl_boxzd2refzd2zzast_nodez00,
				BGl_proc3962z00zzast_walkz00, BGl_string3766z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0z12zd2envzc0zzast_walkz00, BGl_boxzd2refzd2zzast_nodez00,
				BGl_proc3963z00zzast_walkz00, BGl_string3765z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1z12zd2envzc0zzast_walkz00, BGl_boxzd2refzd2zzast_nodez00,
				BGl_proc3964z00zzast_walkz00, BGl_string3764z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2z12zd2envzc0zzast_walkz00, BGl_boxzd2refzd2zzast_nodez00,
				BGl_proc3965z00zzast_walkz00, BGl_string3763z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3z12zd2envzc0zzast_walkz00, BGl_boxzd2refzd2zzast_nodez00,
				BGl_proc3966z00zzast_walkz00, BGl_string3762z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0zd2envzd2zzast_walkz00, BGl_boxzd2setz12zc0zzast_nodez00,
				BGl_proc3967z00zzast_walkz00, BGl_string3774z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1zd2envzd2zzast_walkz00, BGl_boxzd2setz12zc0zzast_nodez00,
				BGl_proc3968z00zzast_walkz00, BGl_string3773z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2zd2envzd2zzast_walkz00, BGl_boxzd2setz12zc0zzast_nodez00,
				BGl_proc3969z00zzast_walkz00, BGl_string3772z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3zd2envzd2zzast_walkz00, BGl_boxzd2setz12zc0zzast_nodez00,
				BGl_proc3970z00zzast_walkz00, BGl_string3771z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0za2zd2envz70zzast_walkz00, BGl_boxzd2setz12zc0zzast_nodez00,
				BGl_proc3971z00zzast_walkz00, BGl_string3769z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1za2zd2envz70zzast_walkz00, BGl_boxzd2setz12zc0zzast_nodez00,
				BGl_proc3972z00zzast_walkz00, BGl_string3768z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2za2zd2envz70zzast_walkz00, BGl_boxzd2setz12zc0zzast_nodez00,
				BGl_proc3973z00zzast_walkz00, BGl_string3767z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3za2zd2envz70zzast_walkz00, BGl_boxzd2setz12zc0zzast_nodez00,
				BGl_proc3974z00zzast_walkz00, BGl_string3766z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0z12zd2envzc0zzast_walkz00, BGl_boxzd2setz12zc0zzast_nodez00,
				BGl_proc3975z00zzast_walkz00, BGl_string3765z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1z12zd2envzc0zzast_walkz00, BGl_boxzd2setz12zc0zzast_nodez00,
				BGl_proc3976z00zzast_walkz00, BGl_string3764z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2z12zd2envzc0zzast_walkz00, BGl_boxzd2setz12zc0zzast_nodez00,
				BGl_proc3977z00zzast_walkz00, BGl_string3763z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3z12zd2envzc0zzast_walkz00, BGl_boxzd2setz12zc0zzast_nodez00,
				BGl_proc3978z00zzast_walkz00, BGl_string3762z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0zd2envzd2zzast_walkz00, BGl_syncz00zzast_nodez00,
				BGl_proc3979z00zzast_walkz00, BGl_string3774z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1zd2envzd2zzast_walkz00, BGl_syncz00zzast_nodez00,
				BGl_proc3980z00zzast_walkz00, BGl_string3773z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2zd2envzd2zzast_walkz00, BGl_syncz00zzast_nodez00,
				BGl_proc3981z00zzast_walkz00, BGl_string3772z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3zd2envzd2zzast_walkz00, BGl_syncz00zzast_nodez00,
				BGl_proc3982z00zzast_walkz00, BGl_string3771z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0za2zd2envz70zzast_walkz00, BGl_syncz00zzast_nodez00,
				BGl_proc3983z00zzast_walkz00, BGl_string3769z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1za2zd2envz70zzast_walkz00, BGl_syncz00zzast_nodez00,
				BGl_proc3984z00zzast_walkz00, BGl_string3768z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2za2zd2envz70zzast_walkz00, BGl_syncz00zzast_nodez00,
				BGl_proc3985z00zzast_walkz00, BGl_string3767z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3za2zd2envz70zzast_walkz00, BGl_syncz00zzast_nodez00,
				BGl_proc3986z00zzast_walkz00, BGl_string3766z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0z12zd2envzc0zzast_walkz00, BGl_syncz00zzast_nodez00,
				BGl_proc3987z00zzast_walkz00, BGl_string3765z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1z12zd2envzc0zzast_walkz00, BGl_syncz00zzast_nodez00,
				BGl_proc3988z00zzast_walkz00, BGl_string3764z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2z12zd2envzc0zzast_walkz00, BGl_syncz00zzast_nodez00,
				BGl_proc3989z00zzast_walkz00, BGl_string3763z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3z12zd2envzc0zzast_walkz00, BGl_syncz00zzast_nodez00,
				BGl_proc3990z00zzast_walkz00, BGl_string3762z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0zd2envzd2zzast_walkz00, BGl_switchz00zzast_nodez00,
				BGl_proc3991z00zzast_walkz00, BGl_string3774z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1zd2envzd2zzast_walkz00, BGl_switchz00zzast_nodez00,
				BGl_proc3992z00zzast_walkz00, BGl_string3773z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2zd2envzd2zzast_walkz00, BGl_switchz00zzast_nodez00,
				BGl_proc3993z00zzast_walkz00, BGl_string3772z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3zd2envzd2zzast_walkz00, BGl_switchz00zzast_nodez00,
				BGl_proc3994z00zzast_walkz00, BGl_string3771z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0za2zd2envz70zzast_walkz00, BGl_switchz00zzast_nodez00,
				BGl_proc3995z00zzast_walkz00, BGl_string3769z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1za2zd2envz70zzast_walkz00, BGl_switchz00zzast_nodez00,
				BGl_proc3996z00zzast_walkz00, BGl_string3768z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2za2zd2envz70zzast_walkz00, BGl_switchz00zzast_nodez00,
				BGl_proc3997z00zzast_walkz00, BGl_string3767z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3za2zd2envz70zzast_walkz00, BGl_switchz00zzast_nodez00,
				BGl_proc3998z00zzast_walkz00, BGl_string3766z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0z12zd2envzc0zzast_walkz00, BGl_switchz00zzast_nodez00,
				BGl_proc3999z00zzast_walkz00, BGl_string3765z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1z12zd2envzc0zzast_walkz00, BGl_switchz00zzast_nodez00,
				BGl_proc4000z00zzast_walkz00, BGl_string3764z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2z12zd2envzc0zzast_walkz00, BGl_switchz00zzast_nodez00,
				BGl_proc4001z00zzast_walkz00, BGl_string3763z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3z12zd2envzc0zzast_walkz00, BGl_switchz00zzast_nodez00,
				BGl_proc4002z00zzast_walkz00, BGl_string3762z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0zd2envzd2zzast_walkz00, BGl_letzd2funzd2zzast_nodez00,
				BGl_proc4003z00zzast_walkz00, BGl_string3774z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1zd2envzd2zzast_walkz00, BGl_letzd2funzd2zzast_nodez00,
				BGl_proc4004z00zzast_walkz00, BGl_string3773z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2zd2envzd2zzast_walkz00, BGl_letzd2funzd2zzast_nodez00,
				BGl_proc4005z00zzast_walkz00, BGl_string3772z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3zd2envzd2zzast_walkz00, BGl_letzd2funzd2zzast_nodez00,
				BGl_proc4006z00zzast_walkz00, BGl_string3771z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0za2zd2envz70zzast_walkz00, BGl_letzd2funzd2zzast_nodez00,
				BGl_proc4007z00zzast_walkz00, BGl_string3769z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1za2zd2envz70zzast_walkz00, BGl_letzd2funzd2zzast_nodez00,
				BGl_proc4008z00zzast_walkz00, BGl_string3768z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2za2zd2envz70zzast_walkz00, BGl_letzd2funzd2zzast_nodez00,
				BGl_proc4009z00zzast_walkz00, BGl_string3767z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3za2zd2envz70zzast_walkz00, BGl_letzd2funzd2zzast_nodez00,
				BGl_proc4010z00zzast_walkz00, BGl_string3766z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0z12zd2envzc0zzast_walkz00, BGl_letzd2funzd2zzast_nodez00,
				BGl_proc4011z00zzast_walkz00, BGl_string3765z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1z12zd2envzc0zzast_walkz00, BGl_letzd2funzd2zzast_nodez00,
				BGl_proc4012z00zzast_walkz00, BGl_string3764z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2z12zd2envzc0zzast_walkz00, BGl_letzd2funzd2zzast_nodez00,
				BGl_proc4013z00zzast_walkz00, BGl_string3763z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3z12zd2envzc0zzast_walkz00, BGl_letzd2funzd2zzast_nodez00,
				BGl_proc4014z00zzast_walkz00, BGl_string3762z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0zd2envzd2zzast_walkz00, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc4015z00zzast_walkz00, BGl_string3774z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1zd2envzd2zzast_walkz00, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc4016z00zzast_walkz00, BGl_string3773z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2zd2envzd2zzast_walkz00, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc4017z00zzast_walkz00, BGl_string3772z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3zd2envzd2zzast_walkz00, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc4018z00zzast_walkz00, BGl_string3771z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0za2zd2envz70zzast_walkz00, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc4019z00zzast_walkz00, BGl_string3769z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1za2zd2envz70zzast_walkz00, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc4020z00zzast_walkz00, BGl_string3768z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2za2zd2envz70zzast_walkz00, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc4021z00zzast_walkz00, BGl_string3767z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3za2zd2envz70zzast_walkz00, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc4022z00zzast_walkz00, BGl_string3766z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk0z12zd2envzc0zzast_walkz00, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc4023z00zzast_walkz00, BGl_string3765z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk1z12zd2envzc0zzast_walkz00, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc4024z00zzast_walkz00, BGl_string3764z00zzast_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk2z12zd2envzc0zzast_walkz00, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc4025z00zzast_walkz00, BGl_string3763z00zzast_walkz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_walk3z12zd2envzc0zzast_walkz00, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc4026z00zzast_walkz00, BGl_string3762z00zzast_walkz00);
		}

	}



/* &walk3!-let-var2171 */
	BgL_nodez00_bglt BGl_z62walk3z12zd2letzd2var2171z70zzast_walkz00(obj_t
		BgL_envz00_7464, obj_t BgL_nz00_7465, obj_t BgL_pz00_7466,
		obj_t BgL_arg0z00_7467, obj_t BgL_arg1z00_7468, obj_t BgL_arg2z00_7469)
	{
		{	/* Ast/walk.scm 320 */
			{
				obj_t BgL_fieldsz00_8745;

				BgL_fieldsz00_8745 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nz00_7465)))->BgL_bindingsz00);
			BgL_loopz00_8744:
				if (NULLP(BgL_fieldsz00_8745))
					{	/* Ast/walk.scm 320 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 320 */
						{	/* Ast/walk.scm 320 */
							obj_t BgL_arg3578z00_8746;
							obj_t BgL_arg3580z00_8747;

							BgL_arg3578z00_8746 = CAR(((obj_t) BgL_fieldsz00_8745));
							{	/* Ast/walk.scm 320 */
								obj_t BgL_arg3581z00_8748;

								{	/* Ast/walk.scm 320 */
									obj_t BgL_pairz00_8749;

									BgL_pairz00_8749 = CAR(((obj_t) BgL_fieldsz00_8745));
									BgL_arg3581z00_8748 = CDR(BgL_pairz00_8749);
								}
								BgL_arg3580z00_8747 =
									BGL_PROCEDURE_CALL4(BgL_pz00_7466, BgL_arg3581z00_8748,
									BgL_arg0z00_7467, BgL_arg1z00_7468, BgL_arg2z00_7469);
							}
							{	/* Ast/walk.scm 320 */
								obj_t BgL_tmpz00_11020;

								BgL_tmpz00_11020 = ((obj_t) BgL_arg3578z00_8746);
								SET_CDR(BgL_tmpz00_11020, BgL_arg3580z00_8747);
							}
						}
						{	/* Ast/walk.scm 320 */
							obj_t BgL_arg3584z00_8750;

							BgL_arg3584z00_8750 = CDR(((obj_t) BgL_fieldsz00_8745));
							{
								obj_t BgL_fieldsz00_11025;

								BgL_fieldsz00_11025 = BgL_arg3584z00_8750;
								BgL_fieldsz00_8745 = BgL_fieldsz00_11025;
								goto BgL_loopz00_8744;
							}
						}
					}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_11028;

				{	/* Ast/walk.scm 320 */
					BgL_nodez00_bglt BgL_arg3585z00_8751;

					BgL_arg3585z00_8751 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nz00_7465)))->BgL_bodyz00);
					BgL_auxz00_11028 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_7466,
							((obj_t) BgL_arg3585z00_8751), BgL_arg0z00_7467, BgL_arg1z00_7468,
							BgL_arg2z00_7469));
				}
				((((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nz00_7465)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_11028), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nz00_7465));
		}

	}



/* &walk2!-let-var2169 */
	BgL_nodez00_bglt BGl_z62walk2z12zd2letzd2var2169z70zzast_walkz00(obj_t
		BgL_envz00_7470, obj_t BgL_nz00_7471, obj_t BgL_pz00_7472,
		obj_t BgL_arg0z00_7473, obj_t BgL_arg1z00_7474)
	{
		{	/* Ast/walk.scm 320 */
			{
				obj_t BgL_fieldsz00_8754;

				BgL_fieldsz00_8754 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nz00_7471)))->BgL_bindingsz00);
			BgL_loopz00_8753:
				if (NULLP(BgL_fieldsz00_8754))
					{	/* Ast/walk.scm 320 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 320 */
						{	/* Ast/walk.scm 320 */
							obj_t BgL_arg3568z00_8755;
							obj_t BgL_arg3569z00_8756;

							BgL_arg3568z00_8755 = CAR(((obj_t) BgL_fieldsz00_8754));
							{	/* Ast/walk.scm 320 */
								obj_t BgL_arg3570z00_8757;

								{	/* Ast/walk.scm 320 */
									obj_t BgL_pairz00_8758;

									BgL_pairz00_8758 = CAR(((obj_t) BgL_fieldsz00_8754));
									BgL_arg3570z00_8757 = CDR(BgL_pairz00_8758);
								}
								BgL_arg3569z00_8756 =
									BGL_PROCEDURE_CALL3(BgL_pz00_7472, BgL_arg3570z00_8757,
									BgL_arg0z00_7473, BgL_arg1z00_7474);
							}
							{	/* Ast/walk.scm 320 */
								obj_t BgL_tmpz00_11057;

								BgL_tmpz00_11057 = ((obj_t) BgL_arg3568z00_8755);
								SET_CDR(BgL_tmpz00_11057, BgL_arg3569z00_8756);
							}
						}
						{	/* Ast/walk.scm 320 */
							obj_t BgL_arg3573z00_8759;

							BgL_arg3573z00_8759 = CDR(((obj_t) BgL_fieldsz00_8754));
							{
								obj_t BgL_fieldsz00_11062;

								BgL_fieldsz00_11062 = BgL_arg3573z00_8759;
								BgL_fieldsz00_8754 = BgL_fieldsz00_11062;
								goto BgL_loopz00_8753;
							}
						}
					}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_11065;

				{	/* Ast/walk.scm 320 */
					BgL_nodez00_bglt BgL_arg3574z00_8760;

					BgL_arg3574z00_8760 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nz00_7471)))->BgL_bodyz00);
					BgL_auxz00_11065 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_7472,
							((obj_t) BgL_arg3574z00_8760), BgL_arg0z00_7473,
							BgL_arg1z00_7474));
				}
				((((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nz00_7471)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_11065), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nz00_7471));
		}

	}



/* &walk1!-let-var2167 */
	BgL_nodez00_bglt BGl_z62walk1z12zd2letzd2var2167z70zzast_walkz00(obj_t
		BgL_envz00_7475, obj_t BgL_nz00_7476, obj_t BgL_pz00_7477,
		obj_t BgL_arg0z00_7478)
	{
		{	/* Ast/walk.scm 320 */
			{
				obj_t BgL_fieldsz00_8763;

				BgL_fieldsz00_8763 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nz00_7476)))->BgL_bindingsz00);
			BgL_loopz00_8762:
				if (NULLP(BgL_fieldsz00_8763))
					{	/* Ast/walk.scm 320 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 320 */
						{	/* Ast/walk.scm 320 */
							obj_t BgL_arg3556z00_8764;
							obj_t BgL_arg3558z00_8765;

							BgL_arg3556z00_8764 = CAR(((obj_t) BgL_fieldsz00_8763));
							{	/* Ast/walk.scm 320 */
								obj_t BgL_arg3560z00_8766;

								{	/* Ast/walk.scm 320 */
									obj_t BgL_pairz00_8767;

									BgL_pairz00_8767 = CAR(((obj_t) BgL_fieldsz00_8763));
									BgL_arg3560z00_8766 = CDR(BgL_pairz00_8767);
								}
								BgL_arg3558z00_8765 =
									BGL_PROCEDURE_CALL2(BgL_pz00_7477, BgL_arg3560z00_8766,
									BgL_arg0z00_7478);
							}
							{	/* Ast/walk.scm 320 */
								obj_t BgL_tmpz00_11092;

								BgL_tmpz00_11092 = ((obj_t) BgL_arg3556z00_8764);
								SET_CDR(BgL_tmpz00_11092, BgL_arg3558z00_8765);
							}
						}
						{	/* Ast/walk.scm 320 */
							obj_t BgL_arg3563z00_8768;

							BgL_arg3563z00_8768 = CDR(((obj_t) BgL_fieldsz00_8763));
							{
								obj_t BgL_fieldsz00_11097;

								BgL_fieldsz00_11097 = BgL_arg3563z00_8768;
								BgL_fieldsz00_8763 = BgL_fieldsz00_11097;
								goto BgL_loopz00_8762;
							}
						}
					}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_11100;

				{	/* Ast/walk.scm 320 */
					BgL_nodez00_bglt BgL_arg3564z00_8769;

					BgL_arg3564z00_8769 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nz00_7476)))->BgL_bodyz00);
					BgL_auxz00_11100 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_7477,
							((obj_t) BgL_arg3564z00_8769), BgL_arg0z00_7478));
				}
				((((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nz00_7476)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_11100), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nz00_7476));
		}

	}



/* &walk0!-let-var2165 */
	BgL_nodez00_bglt BGl_z62walk0z12zd2letzd2var2165z70zzast_walkz00(obj_t
		BgL_envz00_7479, obj_t BgL_nz00_7480, obj_t BgL_pz00_7481)
	{
		{	/* Ast/walk.scm 320 */
			{
				obj_t BgL_fieldsz00_8772;

				BgL_fieldsz00_8772 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nz00_7480)))->BgL_bindingsz00);
			BgL_loopz00_8771:
				if (NULLP(BgL_fieldsz00_8772))
					{	/* Ast/walk.scm 320 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 320 */
						{	/* Ast/walk.scm 320 */
							obj_t BgL_arg3545z00_8773;
							obj_t BgL_arg3546z00_8774;

							BgL_arg3545z00_8773 = CAR(((obj_t) BgL_fieldsz00_8772));
							{	/* Ast/walk.scm 320 */
								obj_t BgL_arg3548z00_8775;

								{	/* Ast/walk.scm 320 */
									obj_t BgL_pairz00_8776;

									BgL_pairz00_8776 = CAR(((obj_t) BgL_fieldsz00_8772));
									BgL_arg3548z00_8775 = CDR(BgL_pairz00_8776);
								}
								BgL_arg3546z00_8774 =
									BGL_PROCEDURE_CALL1(BgL_pz00_7481, BgL_arg3548z00_8775);
							}
							{	/* Ast/walk.scm 320 */
								obj_t BgL_tmpz00_11125;

								BgL_tmpz00_11125 = ((obj_t) BgL_arg3545z00_8773);
								SET_CDR(BgL_tmpz00_11125, BgL_arg3546z00_8774);
							}
						}
						{	/* Ast/walk.scm 320 */
							obj_t BgL_arg3551z00_8777;

							BgL_arg3551z00_8777 = CDR(((obj_t) BgL_fieldsz00_8772));
							{
								obj_t BgL_fieldsz00_11130;

								BgL_fieldsz00_11130 = BgL_arg3551z00_8777;
								BgL_fieldsz00_8772 = BgL_fieldsz00_11130;
								goto BgL_loopz00_8771;
							}
						}
					}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_11133;

				{	/* Ast/walk.scm 320 */
					BgL_nodez00_bglt BgL_arg3552z00_8778;

					BgL_arg3552z00_8778 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nz00_7480)))->BgL_bodyz00);
					BgL_auxz00_11133 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_7481, ((obj_t) BgL_arg3552z00_8778)));
				}
				((((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nz00_7480)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_11133), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nz00_7480));
		}

	}



/* &walk3*-let-var2163 */
	obj_t BGl_z62walk3za2zd2letzd2var2163zc0zzast_walkz00(obj_t BgL_envz00_7482,
		obj_t BgL_nz00_7483, obj_t BgL_pz00_7484, obj_t BgL_arg0z00_7485,
		obj_t BgL_arg1z00_7486, obj_t BgL_arg2z00_7487)
	{
		{	/* Ast/walk.scm 320 */
			{	/* Ast/walk.scm 320 */
				obj_t BgL_res4028z00_8783;

				{	/* Ast/walk.scm 320 */
					obj_t BgL_arg3532z00_8780;
					obj_t BgL_arg3533z00_8781;

					BgL_arg3532z00_8780 =
						BGl_zc3z04anonymousza33534ze3ze70z60zzast_walkz00(BgL_arg2z00_7487,
						BgL_arg1z00_7486, BgL_arg0z00_7485, BgL_pz00_7484,
						(((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt)
										BgL_nz00_7483)))->BgL_bindingsz00));
					{	/* Ast/walk.scm 320 */
						BgL_nodez00_bglt BgL_arg3541z00_8782;

						BgL_arg3541z00_8782 =
							(((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_nz00_7483)))->BgL_bodyz00);
						BgL_arg3533z00_8781 =
							BGL_PROCEDURE_CALL4(BgL_pz00_7484,
							((obj_t) BgL_arg3541z00_8782), BgL_arg0z00_7485, BgL_arg1z00_7486,
							BgL_arg2z00_7487);
					}
					BgL_res4028z00_8783 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg3532z00_8780,
						BgL_arg3533z00_8781);
				}
				return BgL_res4028z00_8783;
			}
		}

	}



/* <@anonymous:3534>~0 */
	obj_t BGl_zc3z04anonymousza33534ze3ze70z60zzast_walkz00(obj_t
		BgL_arg2z00_8667, obj_t BgL_arg1z00_8666, obj_t BgL_arg0z00_8665,
		obj_t BgL_pz00_8664, obj_t BgL_l1628z00_5677)
	{
		{	/* Ast/walk.scm 320 */
			if (NULLP(BgL_l1628z00_5677))
				{	/* Ast/walk.scm 320 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 320 */
					obj_t BgL_arg3536z00_5680;
					obj_t BgL_arg3538z00_5681;

					{	/* Ast/walk.scm 320 */
						obj_t BgL_fz00_5682;

						BgL_fz00_5682 = CAR(((obj_t) BgL_l1628z00_5677));
						{	/* Ast/walk.scm 320 */
							obj_t BgL_arg3539z00_5683;

							BgL_arg3539z00_5683 = CDR(((obj_t) BgL_fz00_5682));
							BgL_arg3536z00_5680 =
								BGL_PROCEDURE_CALL4(BgL_pz00_8664, BgL_arg3539z00_5683,
								BgL_arg0z00_8665, BgL_arg1z00_8666, BgL_arg2z00_8667);
						}
					}
					{	/* Ast/walk.scm 320 */
						obj_t BgL_arg3540z00_5684;

						BgL_arg3540z00_5684 = CDR(((obj_t) BgL_l1628z00_5677));
						BgL_arg3538z00_5681 =
							BGl_zc3z04anonymousza33534ze3ze70z60zzast_walkz00
							(BgL_arg2z00_8667, BgL_arg1z00_8666, BgL_arg0z00_8665,
							BgL_pz00_8664, BgL_arg3540z00_5684);
					}
					return bgl_append2(BgL_arg3536z00_5680, BgL_arg3538z00_5681);
				}
		}

	}



/* &walk2*-let-var2161 */
	obj_t BGl_z62walk2za2zd2letzd2var2161zc0zzast_walkz00(obj_t BgL_envz00_7488,
		obj_t BgL_nz00_7489, obj_t BgL_pz00_7490, obj_t BgL_arg0z00_7491,
		obj_t BgL_arg1z00_7492)
	{
		{	/* Ast/walk.scm 320 */
			{	/* Ast/walk.scm 320 */
				obj_t BgL_res4029z00_8788;

				{	/* Ast/walk.scm 320 */
					obj_t BgL_arg3520z00_8785;
					obj_t BgL_arg3521z00_8786;

					BgL_arg3520z00_8785 =
						BGl_zc3z04anonymousza33522ze3ze70z60zzast_walkz00(BgL_arg1z00_7492,
						BgL_arg0z00_7491, BgL_pz00_7490,
						(((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt)
										BgL_nz00_7489)))->BgL_bindingsz00));
					{	/* Ast/walk.scm 320 */
						BgL_nodez00_bglt BgL_arg3531z00_8787;

						BgL_arg3531z00_8787 =
							(((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_nz00_7489)))->BgL_bodyz00);
						BgL_arg3521z00_8786 =
							BGL_PROCEDURE_CALL3(BgL_pz00_7490,
							((obj_t) BgL_arg3531z00_8787), BgL_arg0z00_7491,
							BgL_arg1z00_7492);
					}
					BgL_res4029z00_8788 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg3520z00_8785,
						BgL_arg3521z00_8786);
				}
				return BgL_res4029z00_8788;
			}
		}

	}



/* <@anonymous:3522>~0 */
	obj_t BGl_zc3z04anonymousza33522ze3ze70z60zzast_walkz00(obj_t
		BgL_arg1z00_8663, obj_t BgL_arg0z00_8662, obj_t BgL_pz00_8661,
		obj_t BgL_l1625z00_5654)
	{
		{	/* Ast/walk.scm 320 */
			if (NULLP(BgL_l1625z00_5654))
				{	/* Ast/walk.scm 320 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 320 */
					obj_t BgL_arg3524z00_5657;
					obj_t BgL_arg3525z00_5658;

					{	/* Ast/walk.scm 320 */
						obj_t BgL_fz00_5659;

						BgL_fz00_5659 = CAR(((obj_t) BgL_l1625z00_5654));
						{	/* Ast/walk.scm 320 */
							obj_t BgL_arg3528z00_5660;

							BgL_arg3528z00_5660 = CDR(((obj_t) BgL_fz00_5659));
							BgL_arg3524z00_5657 =
								BGL_PROCEDURE_CALL3(BgL_pz00_8661, BgL_arg3528z00_5660,
								BgL_arg0z00_8662, BgL_arg1z00_8663);
						}
					}
					{	/* Ast/walk.scm 320 */
						obj_t BgL_arg3529z00_5661;

						BgL_arg3529z00_5661 = CDR(((obj_t) BgL_l1625z00_5654));
						BgL_arg3525z00_5658 =
							BGl_zc3z04anonymousza33522ze3ze70z60zzast_walkz00
							(BgL_arg1z00_8663, BgL_arg0z00_8662, BgL_pz00_8661,
							BgL_arg3529z00_5661);
					}
					return bgl_append2(BgL_arg3524z00_5657, BgL_arg3525z00_5658);
				}
		}

	}



/* &walk1*-let-var2159 */
	obj_t BGl_z62walk1za2zd2letzd2var2159zc0zzast_walkz00(obj_t BgL_envz00_7493,
		obj_t BgL_nz00_7494, obj_t BgL_pz00_7495, obj_t BgL_arg0z00_7496)
	{
		{	/* Ast/walk.scm 320 */
			{	/* Ast/walk.scm 320 */
				obj_t BgL_res4030z00_8793;

				{	/* Ast/walk.scm 320 */
					obj_t BgL_arg3508z00_8790;
					obj_t BgL_arg3510z00_8791;

					BgL_arg3508z00_8790 =
						BGl_zc3z04anonymousza33511ze3ze70z60zzast_walkz00(BgL_arg0z00_7496,
						BgL_pz00_7495,
						(((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt)
										BgL_nz00_7494)))->BgL_bindingsz00));
					{	/* Ast/walk.scm 320 */
						BgL_nodez00_bglt BgL_arg3518z00_8792;

						BgL_arg3518z00_8792 =
							(((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_nz00_7494)))->BgL_bodyz00);
						BgL_arg3510z00_8791 =
							BGL_PROCEDURE_CALL2(BgL_pz00_7495,
							((obj_t) BgL_arg3518z00_8792), BgL_arg0z00_7496);
					}
					BgL_res4030z00_8793 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg3508z00_8790,
						BgL_arg3510z00_8791);
				}
				return BgL_res4030z00_8793;
			}
		}

	}



/* <@anonymous:3511>~0 */
	obj_t BGl_zc3z04anonymousza33511ze3ze70z60zzast_walkz00(obj_t
		BgL_arg0z00_8660, obj_t BgL_pz00_8659, obj_t BgL_l1622z00_5632)
	{
		{	/* Ast/walk.scm 320 */
			if (NULLP(BgL_l1622z00_5632))
				{	/* Ast/walk.scm 320 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 320 */
					obj_t BgL_arg3513z00_5635;
					obj_t BgL_arg3514z00_5636;

					{	/* Ast/walk.scm 320 */
						obj_t BgL_fz00_5637;

						BgL_fz00_5637 = CAR(((obj_t) BgL_l1622z00_5632));
						{	/* Ast/walk.scm 320 */
							obj_t BgL_arg3515z00_5638;

							BgL_arg3515z00_5638 = CDR(((obj_t) BgL_fz00_5637));
							BgL_arg3513z00_5635 =
								BGL_PROCEDURE_CALL2(BgL_pz00_8659, BgL_arg3515z00_5638,
								BgL_arg0z00_8660);
						}
					}
					{	/* Ast/walk.scm 320 */
						obj_t BgL_arg3517z00_5639;

						BgL_arg3517z00_5639 = CDR(((obj_t) BgL_l1622z00_5632));
						BgL_arg3514z00_5636 =
							BGl_zc3z04anonymousza33511ze3ze70z60zzast_walkz00
							(BgL_arg0z00_8660, BgL_pz00_8659, BgL_arg3517z00_5639);
					}
					return bgl_append2(BgL_arg3513z00_5635, BgL_arg3514z00_5636);
				}
		}

	}



/* &walk0*-let-var2157 */
	obj_t BGl_z62walk0za2zd2letzd2var2157zc0zzast_walkz00(obj_t BgL_envz00_7497,
		obj_t BgL_nz00_7498, obj_t BgL_pz00_7499)
	{
		{	/* Ast/walk.scm 320 */
			{	/* Ast/walk.scm 320 */
				obj_t BgL_res4031z00_8798;

				{	/* Ast/walk.scm 320 */
					obj_t BgL_arg3494z00_8795;
					obj_t BgL_arg3495z00_8796;

					BgL_arg3494z00_8795 =
						BGl_zc3z04anonymousza33496ze3ze70z60zzast_walkz00(BgL_pz00_7499,
						(((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_nz00_7498)))->BgL_bindingsz00));
					{	/* Ast/walk.scm 320 */
						BgL_nodez00_bglt BgL_arg3506z00_8797;

						BgL_arg3506z00_8797 =
							(((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_nz00_7498)))->BgL_bodyz00);
						BgL_arg3495z00_8796 =
							BGL_PROCEDURE_CALL1(BgL_pz00_7499, ((obj_t) BgL_arg3506z00_8797));
					}
					BgL_res4031z00_8798 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg3494z00_8795,
						BgL_arg3495z00_8796);
				}
				return BgL_res4031z00_8798;
			}
		}

	}



/* <@anonymous:3496>~0 */
	obj_t BGl_zc3z04anonymousza33496ze3ze70z60zzast_walkz00(obj_t BgL_pz00_8658,
		obj_t BgL_l1619z00_5611)
	{
		{	/* Ast/walk.scm 320 */
			if (NULLP(BgL_l1619z00_5611))
				{	/* Ast/walk.scm 320 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 320 */
					obj_t BgL_arg3500z00_5614;
					obj_t BgL_arg3502z00_5615;

					{	/* Ast/walk.scm 320 */
						obj_t BgL_fz00_5616;

						BgL_fz00_5616 = CAR(((obj_t) BgL_l1619z00_5611));
						{	/* Ast/walk.scm 320 */
							obj_t BgL_arg3503z00_5617;

							BgL_arg3503z00_5617 = CDR(((obj_t) BgL_fz00_5616));
							BgL_arg3500z00_5614 =
								BGL_PROCEDURE_CALL1(BgL_pz00_8658, BgL_arg3503z00_5617);
						}
					}
					{	/* Ast/walk.scm 320 */
						obj_t BgL_arg3504z00_5618;

						BgL_arg3504z00_5618 = CDR(((obj_t) BgL_l1619z00_5611));
						BgL_arg3502z00_5615 =
							BGl_zc3z04anonymousza33496ze3ze70z60zzast_walkz00(BgL_pz00_8658,
							BgL_arg3504z00_5618);
					}
					return bgl_append2(BgL_arg3500z00_5614, BgL_arg3502z00_5615);
				}
		}

	}



/* &walk3-let-var2154 */
	obj_t BGl_z62walk3zd2letzd2var2154z62zzast_walkz00(obj_t BgL_envz00_7500,
		obj_t BgL_nz00_7501, obj_t BgL_pz00_7502, obj_t BgL_arg0z00_7503,
		obj_t BgL_arg1z00_7504, obj_t BgL_arg2z00_7505)
	{
		{	/* Ast/walk.scm 320 */
			{	/* Ast/walk.scm 320 */
				obj_t BgL_g1617z00_8800;

				BgL_g1617z00_8800 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nz00_7501)))->BgL_bindingsz00);
				{
					obj_t BgL_l1615z00_8802;

					BgL_l1615z00_8802 = BgL_g1617z00_8800;
				BgL_zc3z04anonymousza33488ze3z87_8801:
					if (PAIRP(BgL_l1615z00_8802))
						{	/* Ast/walk.scm 320 */
							{	/* Ast/walk.scm 320 */
								obj_t BgL_fz00_8803;

								BgL_fz00_8803 = CAR(BgL_l1615z00_8802);
								{	/* Ast/walk.scm 320 */
									obj_t BgL_arg3490z00_8804;

									BgL_arg3490z00_8804 = CDR(((obj_t) BgL_fz00_8803));
									BGL_PROCEDURE_CALL4(BgL_pz00_7502, BgL_arg3490z00_8804,
										BgL_arg0z00_7503, BgL_arg1z00_7504, BgL_arg2z00_7505);
								}
							}
							{
								obj_t BgL_l1615z00_11272;

								BgL_l1615z00_11272 = CDR(BgL_l1615z00_8802);
								BgL_l1615z00_8802 = BgL_l1615z00_11272;
								goto BgL_zc3z04anonymousza33488ze3z87_8801;
							}
						}
					else
						{	/* Ast/walk.scm 320 */
							((bool_t) 1);
						}
				}
			}
			{	/* Ast/walk.scm 320 */
				BgL_nodez00_bglt BgL_arg3493z00_8805;

				BgL_arg3493z00_8805 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nz00_7501)))->BgL_bodyz00);
				return
					BGL_PROCEDURE_CALL4(BgL_pz00_7502,
					((obj_t) BgL_arg3493z00_8805), BgL_arg0z00_7503, BgL_arg1z00_7504,
					BgL_arg2z00_7505);
			}
		}

	}



/* &walk2-let-var2152 */
	obj_t BGl_z62walk2zd2letzd2var2152z62zzast_walkz00(obj_t BgL_envz00_7506,
		obj_t BgL_nz00_7507, obj_t BgL_pz00_7508, obj_t BgL_arg0z00_7509,
		obj_t BgL_arg1z00_7510)
	{
		{	/* Ast/walk.scm 320 */
			{	/* Ast/walk.scm 320 */
				obj_t BgL_g1614z00_8807;

				BgL_g1614z00_8807 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nz00_7507)))->BgL_bindingsz00);
				{
					obj_t BgL_l1612z00_8809;

					BgL_l1612z00_8809 = BgL_g1614z00_8807;
				BgL_zc3z04anonymousza33483ze3z87_8808:
					if (PAIRP(BgL_l1612z00_8809))
						{	/* Ast/walk.scm 320 */
							{	/* Ast/walk.scm 320 */
								obj_t BgL_fz00_8810;

								BgL_fz00_8810 = CAR(BgL_l1612z00_8809);
								{	/* Ast/walk.scm 320 */
									obj_t BgL_arg3485z00_8811;

									BgL_arg3485z00_8811 = CDR(((obj_t) BgL_fz00_8810));
									BGL_PROCEDURE_CALL3(BgL_pz00_7508, BgL_arg3485z00_8811,
										BgL_arg0z00_7509, BgL_arg1z00_7510);
								}
							}
							{
								obj_t BgL_l1612z00_11297;

								BgL_l1612z00_11297 = CDR(BgL_l1612z00_8809);
								BgL_l1612z00_8809 = BgL_l1612z00_11297;
								goto BgL_zc3z04anonymousza33483ze3z87_8808;
							}
						}
					else
						{	/* Ast/walk.scm 320 */
							((bool_t) 1);
						}
				}
			}
			{	/* Ast/walk.scm 320 */
				BgL_nodez00_bglt BgL_arg3487z00_8812;

				BgL_arg3487z00_8812 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nz00_7507)))->BgL_bodyz00);
				return
					BGL_PROCEDURE_CALL3(BgL_pz00_7508,
					((obj_t) BgL_arg3487z00_8812), BgL_arg0z00_7509, BgL_arg1z00_7510);
			}
		}

	}



/* &walk1-let-var2150 */
	obj_t BGl_z62walk1zd2letzd2var2150z62zzast_walkz00(obj_t BgL_envz00_7511,
		obj_t BgL_nz00_7512, obj_t BgL_pz00_7513, obj_t BgL_arg0z00_7514)
	{
		{	/* Ast/walk.scm 320 */
			{	/* Ast/walk.scm 320 */
				obj_t BgL_g1611z00_8814;

				BgL_g1611z00_8814 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nz00_7512)))->BgL_bindingsz00);
				{
					obj_t BgL_l1609z00_8816;

					BgL_l1609z00_8816 = BgL_g1611z00_8814;
				BgL_zc3z04anonymousza33478ze3z87_8815:
					if (PAIRP(BgL_l1609z00_8816))
						{	/* Ast/walk.scm 320 */
							{	/* Ast/walk.scm 320 */
								obj_t BgL_fz00_8817;

								BgL_fz00_8817 = CAR(BgL_l1609z00_8816);
								{	/* Ast/walk.scm 320 */
									obj_t BgL_arg3480z00_8818;

									BgL_arg3480z00_8818 = CDR(((obj_t) BgL_fz00_8817));
									BGL_PROCEDURE_CALL2(BgL_pz00_7513, BgL_arg3480z00_8818,
										BgL_arg0z00_7514);
								}
							}
							{
								obj_t BgL_l1609z00_11320;

								BgL_l1609z00_11320 = CDR(BgL_l1609z00_8816);
								BgL_l1609z00_8816 = BgL_l1609z00_11320;
								goto BgL_zc3z04anonymousza33478ze3z87_8815;
							}
						}
					else
						{	/* Ast/walk.scm 320 */
							((bool_t) 1);
						}
				}
			}
			{	/* Ast/walk.scm 320 */
				BgL_nodez00_bglt BgL_arg3482z00_8819;

				BgL_arg3482z00_8819 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nz00_7512)))->BgL_bodyz00);
				return
					BGL_PROCEDURE_CALL2(BgL_pz00_7513,
					((obj_t) BgL_arg3482z00_8819), BgL_arg0z00_7514);
			}
		}

	}



/* &walk0-let-var2148 */
	obj_t BGl_z62walk0zd2letzd2var2148z62zzast_walkz00(obj_t BgL_envz00_7515,
		obj_t BgL_nz00_7516, obj_t BgL_pz00_7517)
	{
		{	/* Ast/walk.scm 320 */
			{	/* Ast/walk.scm 320 */
				obj_t BgL_g1608z00_8821;

				BgL_g1608z00_8821 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nz00_7516)))->BgL_bindingsz00);
				{
					obj_t BgL_l1606z00_8823;

					BgL_l1606z00_8823 = BgL_g1608z00_8821;
				BgL_zc3z04anonymousza33473ze3z87_8822:
					if (PAIRP(BgL_l1606z00_8823))
						{	/* Ast/walk.scm 320 */
							{	/* Ast/walk.scm 320 */
								obj_t BgL_fz00_8824;

								BgL_fz00_8824 = CAR(BgL_l1606z00_8823);
								{	/* Ast/walk.scm 320 */
									obj_t BgL_arg3475z00_8825;

									BgL_arg3475z00_8825 = CDR(((obj_t) BgL_fz00_8824));
									BGL_PROCEDURE_CALL1(BgL_pz00_7517, BgL_arg3475z00_8825);
								}
							}
							{
								obj_t BgL_l1606z00_11341;

								BgL_l1606z00_11341 = CDR(BgL_l1606z00_8823);
								BgL_l1606z00_8823 = BgL_l1606z00_11341;
								goto BgL_zc3z04anonymousza33473ze3z87_8822;
							}
						}
					else
						{	/* Ast/walk.scm 320 */
							((bool_t) 1);
						}
				}
			}
			{	/* Ast/walk.scm 320 */
				BgL_nodez00_bglt BgL_arg3477z00_8826;

				BgL_arg3477z00_8826 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nz00_7516)))->BgL_bodyz00);
				return
					BGL_PROCEDURE_CALL1(BgL_pz00_7517, ((obj_t) BgL_arg3477z00_8826));
			}
		}

	}



/* &walk3!-let-fun2146 */
	BgL_nodez00_bglt BGl_z62walk3z12zd2letzd2fun2146z70zzast_walkz00(obj_t
		BgL_envz00_7518, obj_t BgL_nz00_7519, obj_t BgL_pz00_7520,
		obj_t BgL_arg0z00_7521, obj_t BgL_arg1z00_7522, obj_t BgL_arg2z00_7523)
	{
		{	/* Ast/walk.scm 315 */
			{
				obj_t BgL_fieldsz00_8829;

				BgL_fieldsz00_8829 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nz00_7519)))->BgL_localsz00);
			BgL_loopz00_8828:
				if (NULLP(BgL_fieldsz00_8829))
					{	/* Ast/walk.scm 315 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 315 */
						{	/* Ast/walk.scm 315 */
							obj_t BgL_vz00_8830;
							obj_t BgL_bz00_8831;

							BgL_vz00_8830 = CAR(((obj_t) BgL_fieldsz00_8829));
							{	/* Ast/walk.scm 315 */
								obj_t BgL_arg3469z00_8832;

								BgL_arg3469z00_8832 =
									(((BgL_sfunz00_bglt) COBJECT(
											((BgL_sfunz00_bglt)
												(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																CAR(
																	((obj_t) BgL_fieldsz00_8829)))))->
													BgL_valuez00))))->BgL_bodyz00);
								BgL_bz00_8831 =
									BGL_PROCEDURE_CALL4(BgL_pz00_7520, BgL_arg3469z00_8832,
									BgL_arg0z00_7521, BgL_arg1z00_7522, BgL_arg2z00_7523);
							}
							{	/* Ast/walk.scm 317 */
								BgL_valuez00_bglt BgL_arg3468z00_8833;

								BgL_arg3468z00_8833 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_vz00_8830)))->BgL_valuez00);
								((((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt) BgL_arg3468z00_8833)))->
										BgL_bodyz00) = ((obj_t) BgL_bz00_8831), BUNSPEC);
							}
						}
						{	/* Ast/walk.scm 315 */
							obj_t BgL_arg3471z00_8834;

							BgL_arg3471z00_8834 = CDR(((obj_t) BgL_fieldsz00_8829));
							{
								obj_t BgL_fieldsz00_11373;

								BgL_fieldsz00_11373 = BgL_arg3471z00_8834;
								BgL_fieldsz00_8829 = BgL_fieldsz00_11373;
								goto BgL_loopz00_8828;
							}
						}
					}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_11376;

				{	/* Ast/walk.scm 315 */
					BgL_nodez00_bglt BgL_arg3472z00_8835;

					BgL_arg3472z00_8835 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nz00_7519)))->BgL_bodyz00);
					BgL_auxz00_11376 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_7520,
							((obj_t) BgL_arg3472z00_8835), BgL_arg0z00_7521, BgL_arg1z00_7522,
							BgL_arg2z00_7523));
				}
				((((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nz00_7519)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_11376), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nz00_7519));
		}

	}



/* &walk2!-let-fun2144 */
	BgL_nodez00_bglt BGl_z62walk2z12zd2letzd2fun2144z70zzast_walkz00(obj_t
		BgL_envz00_7524, obj_t BgL_nz00_7525, obj_t BgL_pz00_7526,
		obj_t BgL_arg0z00_7527, obj_t BgL_arg1z00_7528)
	{
		{	/* Ast/walk.scm 315 */
			{
				obj_t BgL_fieldsz00_8838;

				BgL_fieldsz00_8838 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nz00_7525)))->BgL_localsz00);
			BgL_loopz00_8837:
				if (NULLP(BgL_fieldsz00_8838))
					{	/* Ast/walk.scm 315 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 315 */
						{	/* Ast/walk.scm 315 */
							obj_t BgL_vz00_8839;
							obj_t BgL_bz00_8840;

							BgL_vz00_8839 = CAR(((obj_t) BgL_fieldsz00_8838));
							{	/* Ast/walk.scm 315 */
								obj_t BgL_arg3460z00_8841;

								BgL_arg3460z00_8841 =
									(((BgL_sfunz00_bglt) COBJECT(
											((BgL_sfunz00_bglt)
												(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																CAR(
																	((obj_t) BgL_fieldsz00_8838)))))->
													BgL_valuez00))))->BgL_bodyz00);
								BgL_bz00_8840 =
									BGL_PROCEDURE_CALL3(BgL_pz00_7526, BgL_arg3460z00_8841,
									BgL_arg0z00_7527, BgL_arg1z00_7528);
							}
							{	/* Ast/walk.scm 317 */
								BgL_valuez00_bglt BgL_arg3459z00_8842;

								BgL_arg3459z00_8842 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_vz00_8839)))->BgL_valuez00);
								((((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt) BgL_arg3459z00_8842)))->
										BgL_bodyz00) = ((obj_t) BgL_bz00_8840), BUNSPEC);
							}
						}
						{	/* Ast/walk.scm 315 */
							obj_t BgL_arg3463z00_8843;

							BgL_arg3463z00_8843 = CDR(((obj_t) BgL_fieldsz00_8838));
							{
								obj_t BgL_fieldsz00_11414;

								BgL_fieldsz00_11414 = BgL_arg3463z00_8843;
								BgL_fieldsz00_8838 = BgL_fieldsz00_11414;
								goto BgL_loopz00_8837;
							}
						}
					}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_11417;

				{	/* Ast/walk.scm 315 */
					BgL_nodez00_bglt BgL_arg3464z00_8844;

					BgL_arg3464z00_8844 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nz00_7525)))->BgL_bodyz00);
					BgL_auxz00_11417 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_7526,
							((obj_t) BgL_arg3464z00_8844), BgL_arg0z00_7527,
							BgL_arg1z00_7528));
				}
				((((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nz00_7525)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_11417), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nz00_7525));
		}

	}



/* &walk1!-let-fun2142 */
	BgL_nodez00_bglt BGl_z62walk1z12zd2letzd2fun2142z70zzast_walkz00(obj_t
		BgL_envz00_7529, obj_t BgL_nz00_7530, obj_t BgL_pz00_7531,
		obj_t BgL_arg0z00_7532)
	{
		{	/* Ast/walk.scm 315 */
			{
				obj_t BgL_fieldsz00_8847;

				BgL_fieldsz00_8847 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nz00_7530)))->BgL_localsz00);
			BgL_loopz00_8846:
				if (NULLP(BgL_fieldsz00_8847))
					{	/* Ast/walk.scm 315 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 315 */
						{	/* Ast/walk.scm 315 */
							obj_t BgL_vz00_8848;
							obj_t BgL_bz00_8849;

							BgL_vz00_8848 = CAR(((obj_t) BgL_fieldsz00_8847));
							{	/* Ast/walk.scm 315 */
								obj_t BgL_arg3451z00_8850;

								BgL_arg3451z00_8850 =
									(((BgL_sfunz00_bglt) COBJECT(
											((BgL_sfunz00_bglt)
												(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																CAR(
																	((obj_t) BgL_fieldsz00_8847)))))->
													BgL_valuez00))))->BgL_bodyz00);
								BgL_bz00_8849 =
									BGL_PROCEDURE_CALL2(BgL_pz00_7531, BgL_arg3451z00_8850,
									BgL_arg0z00_7532);
							}
							{	/* Ast/walk.scm 317 */
								BgL_valuez00_bglt BgL_arg3449z00_8851;

								BgL_arg3449z00_8851 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_vz00_8848)))->BgL_valuez00);
								((((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt) BgL_arg3449z00_8851)))->
										BgL_bodyz00) = ((obj_t) BgL_bz00_8849), BUNSPEC);
							}
						}
						{	/* Ast/walk.scm 315 */
							obj_t BgL_arg3453z00_8852;

							BgL_arg3453z00_8852 = CDR(((obj_t) BgL_fieldsz00_8847));
							{
								obj_t BgL_fieldsz00_11453;

								BgL_fieldsz00_11453 = BgL_arg3453z00_8852;
								BgL_fieldsz00_8847 = BgL_fieldsz00_11453;
								goto BgL_loopz00_8846;
							}
						}
					}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_11456;

				{	/* Ast/walk.scm 315 */
					BgL_nodez00_bglt BgL_arg3454z00_8853;

					BgL_arg3454z00_8853 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nz00_7530)))->BgL_bodyz00);
					BgL_auxz00_11456 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_7531,
							((obj_t) BgL_arg3454z00_8853), BgL_arg0z00_7532));
				}
				((((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nz00_7530)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_11456), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nz00_7530));
		}

	}



/* &walk0!-let-fun2140 */
	BgL_nodez00_bglt BGl_z62walk0z12zd2letzd2fun2140z70zzast_walkz00(obj_t
		BgL_envz00_7533, obj_t BgL_nz00_7534, obj_t BgL_pz00_7535)
	{
		{	/* Ast/walk.scm 315 */
			{
				obj_t BgL_fieldsz00_8856;

				BgL_fieldsz00_8856 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nz00_7534)))->BgL_localsz00);
			BgL_loopz00_8855:
				if (NULLP(BgL_fieldsz00_8856))
					{	/* Ast/walk.scm 315 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 315 */
						{	/* Ast/walk.scm 315 */
							obj_t BgL_vz00_8857;
							obj_t BgL_bz00_8858;

							BgL_vz00_8857 = CAR(((obj_t) BgL_fieldsz00_8856));
							{	/* Ast/walk.scm 315 */
								obj_t BgL_arg3440z00_8859;

								BgL_arg3440z00_8859 =
									(((BgL_sfunz00_bglt) COBJECT(
											((BgL_sfunz00_bglt)
												(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																CAR(
																	((obj_t) BgL_fieldsz00_8856)))))->
													BgL_valuez00))))->BgL_bodyz00);
								BgL_bz00_8858 =
									BGL_PROCEDURE_CALL1(BgL_pz00_7535, BgL_arg3440z00_8859);
							}
							{	/* Ast/walk.scm 317 */
								BgL_valuez00_bglt BgL_arg3439z00_8860;

								BgL_arg3439z00_8860 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_vz00_8857)))->BgL_valuez00);
								((((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt) BgL_arg3439z00_8860)))->
										BgL_bodyz00) = ((obj_t) BgL_bz00_8858), BUNSPEC);
							}
						}
						{	/* Ast/walk.scm 315 */
							obj_t BgL_arg3443z00_8861;

							BgL_arg3443z00_8861 = CDR(((obj_t) BgL_fieldsz00_8856));
							{
								obj_t BgL_fieldsz00_11490;

								BgL_fieldsz00_11490 = BgL_arg3443z00_8861;
								BgL_fieldsz00_8856 = BgL_fieldsz00_11490;
								goto BgL_loopz00_8855;
							}
						}
					}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_11493;

				{	/* Ast/walk.scm 315 */
					BgL_nodez00_bglt BgL_arg3445z00_8862;

					BgL_arg3445z00_8862 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nz00_7534)))->BgL_bodyz00);
					BgL_auxz00_11493 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_7535, ((obj_t) BgL_arg3445z00_8862)));
				}
				((((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nz00_7534)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_11493), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nz00_7534));
		}

	}



/* &walk3*-let-fun2138 */
	obj_t BGl_z62walk3za2zd2letzd2fun2138zc0zzast_walkz00(obj_t BgL_envz00_7536,
		obj_t BgL_nz00_7537, obj_t BgL_pz00_7538, obj_t BgL_arg0z00_7539,
		obj_t BgL_arg1z00_7540, obj_t BgL_arg2z00_7541)
	{
		{	/* Ast/walk.scm 315 */
			{	/* Ast/walk.scm 315 */
				obj_t BgL_res4032z00_8867;

				{	/* Ast/walk.scm 315 */
					obj_t BgL_arg3416z00_8864;
					obj_t BgL_arg3422z00_8865;

					BgL_arg3416z00_8864 =
						BGl_zc3z04anonymousza33423ze3ze70z60zzast_walkz00(BgL_arg2z00_7541,
						BgL_arg1z00_7540, BgL_arg0z00_7539, BgL_pz00_7538,
						(((BgL_letzd2funzd2_bglt) COBJECT(((BgL_letzd2funzd2_bglt)
										BgL_nz00_7537)))->BgL_localsz00));
					{	/* Ast/walk.scm 315 */
						BgL_nodez00_bglt BgL_arg3434z00_8866;

						BgL_arg3434z00_8866 =
							(((BgL_letzd2funzd2_bglt) COBJECT(
									((BgL_letzd2funzd2_bglt) BgL_nz00_7537)))->BgL_bodyz00);
						BgL_arg3422z00_8865 =
							BGL_PROCEDURE_CALL4(BgL_pz00_7538,
							((obj_t) BgL_arg3434z00_8866), BgL_arg0z00_7539, BgL_arg1z00_7540,
							BgL_arg2z00_7541);
					}
					BgL_res4032z00_8867 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg3416z00_8864,
						BgL_arg3422z00_8865);
				}
				return BgL_res4032z00_8867;
			}
		}

	}



/* <@anonymous:3423>~0 */
	obj_t BGl_zc3z04anonymousza33423ze3ze70z60zzast_walkz00(obj_t
		BgL_arg2z00_8657, obj_t BgL_arg1z00_8656, obj_t BgL_arg0z00_8655,
		obj_t BgL_pz00_8654, obj_t BgL_l1604z00_5433)
	{
		{	/* Ast/walk.scm 315 */
			if (NULLP(BgL_l1604z00_5433))
				{	/* Ast/walk.scm 315 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 315 */
					obj_t BgL_arg3425z00_5436;
					obj_t BgL_arg3429z00_5437;

					{	/* Ast/walk.scm 315 */
						obj_t BgL_fz00_5438;

						BgL_fz00_5438 = CAR(((obj_t) BgL_l1604z00_5433));
						{	/* Ast/walk.scm 316 */
							obj_t BgL_arg3430z00_5439;

							BgL_arg3430z00_5439 =
								(((BgL_sfunz00_bglt) COBJECT(
										((BgL_sfunz00_bglt)
											(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_fz00_5438)))->
												BgL_valuez00))))->BgL_bodyz00);
							BgL_arg3425z00_5436 =
								BGL_PROCEDURE_CALL4(BgL_pz00_8654, BgL_arg3430z00_5439,
								BgL_arg0z00_8655, BgL_arg1z00_8656, BgL_arg2z00_8657);
						}
					}
					{	/* Ast/walk.scm 315 */
						obj_t BgL_arg3433z00_5442;

						BgL_arg3433z00_5442 = CDR(((obj_t) BgL_l1604z00_5433));
						BgL_arg3429z00_5437 =
							BGl_zc3z04anonymousza33423ze3ze70z60zzast_walkz00
							(BgL_arg2z00_8657, BgL_arg1z00_8656, BgL_arg0z00_8655,
							BgL_pz00_8654, BgL_arg3433z00_5442);
					}
					return bgl_append2(BgL_arg3425z00_5436, BgL_arg3429z00_5437);
				}
		}

	}



/* &walk2*-let-fun2136 */
	obj_t BGl_z62walk2za2zd2letzd2fun2136zc0zzast_walkz00(obj_t BgL_envz00_7542,
		obj_t BgL_nz00_7543, obj_t BgL_pz00_7544, obj_t BgL_arg0z00_7545,
		obj_t BgL_arg1z00_7546)
	{
		{	/* Ast/walk.scm 315 */
			{	/* Ast/walk.scm 315 */
				obj_t BgL_res4033z00_8872;

				{	/* Ast/walk.scm 315 */
					obj_t BgL_arg3400z00_8869;
					obj_t BgL_arg3401z00_8870;

					BgL_arg3400z00_8869 =
						BGl_zc3z04anonymousza33402ze3ze70z60zzast_walkz00(BgL_arg1z00_7546,
						BgL_arg0z00_7545, BgL_pz00_7544,
						(((BgL_letzd2funzd2_bglt) COBJECT(((BgL_letzd2funzd2_bglt)
										BgL_nz00_7543)))->BgL_localsz00));
					{	/* Ast/walk.scm 315 */
						BgL_nodez00_bglt BgL_arg3413z00_8871;

						BgL_arg3413z00_8871 =
							(((BgL_letzd2funzd2_bglt) COBJECT(
									((BgL_letzd2funzd2_bglt) BgL_nz00_7543)))->BgL_bodyz00);
						BgL_arg3401z00_8870 =
							BGL_PROCEDURE_CALL3(BgL_pz00_7544,
							((obj_t) BgL_arg3413z00_8871), BgL_arg0z00_7545,
							BgL_arg1z00_7546);
					}
					BgL_res4033z00_8872 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg3400z00_8869,
						BgL_arg3401z00_8870);
				}
				return BgL_res4033z00_8872;
			}
		}

	}



/* <@anonymous:3402>~0 */
	obj_t BGl_zc3z04anonymousza33402ze3ze70z60zzast_walkz00(obj_t
		BgL_arg1z00_8653, obj_t BgL_arg0z00_8652, obj_t BgL_pz00_8651,
		obj_t BgL_l1601z00_5408)
	{
		{	/* Ast/walk.scm 315 */
			if (NULLP(BgL_l1601z00_5408))
				{	/* Ast/walk.scm 315 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 315 */
					obj_t BgL_arg3404z00_5411;
					obj_t BgL_arg3405z00_5412;

					{	/* Ast/walk.scm 315 */
						obj_t BgL_fz00_5413;

						BgL_fz00_5413 = CAR(((obj_t) BgL_l1601z00_5408));
						{	/* Ast/walk.scm 316 */
							obj_t BgL_arg3406z00_5414;

							BgL_arg3406z00_5414 =
								(((BgL_sfunz00_bglt) COBJECT(
										((BgL_sfunz00_bglt)
											(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_fz00_5413)))->
												BgL_valuez00))))->BgL_bodyz00);
							BgL_arg3404z00_5411 =
								BGL_PROCEDURE_CALL3(BgL_pz00_8651, BgL_arg3406z00_5414,
								BgL_arg0z00_8652, BgL_arg1z00_8653);
						}
					}
					{	/* Ast/walk.scm 315 */
						obj_t BgL_arg3410z00_5417;

						BgL_arg3410z00_5417 = CDR(((obj_t) BgL_l1601z00_5408));
						BgL_arg3405z00_5412 =
							BGl_zc3z04anonymousza33402ze3ze70z60zzast_walkz00
							(BgL_arg1z00_8653, BgL_arg0z00_8652, BgL_pz00_8651,
							BgL_arg3410z00_5417);
					}
					return bgl_append2(BgL_arg3404z00_5411, BgL_arg3405z00_5412);
				}
		}

	}



/* &walk1*-let-fun2134 */
	obj_t BGl_z62walk1za2zd2letzd2fun2134zc0zzast_walkz00(obj_t BgL_envz00_7547,
		obj_t BgL_nz00_7548, obj_t BgL_pz00_7549, obj_t BgL_arg0z00_7550)
	{
		{	/* Ast/walk.scm 315 */
			{	/* Ast/walk.scm 315 */
				obj_t BgL_res4034z00_8877;

				{	/* Ast/walk.scm 315 */
					obj_t BgL_arg3388z00_8874;
					obj_t BgL_arg3391z00_8875;

					BgL_arg3388z00_8874 =
						BGl_zc3z04anonymousza33392ze3ze70z60zzast_walkz00(BgL_arg0z00_7550,
						BgL_pz00_7549,
						(((BgL_letzd2funzd2_bglt) COBJECT(((BgL_letzd2funzd2_bglt)
										BgL_nz00_7548)))->BgL_localsz00));
					{	/* Ast/walk.scm 315 */
						BgL_nodez00_bglt BgL_arg3399z00_8876;

						BgL_arg3399z00_8876 =
							(((BgL_letzd2funzd2_bglt) COBJECT(
									((BgL_letzd2funzd2_bglt) BgL_nz00_7548)))->BgL_bodyz00);
						BgL_arg3391z00_8875 =
							BGL_PROCEDURE_CALL2(BgL_pz00_7549,
							((obj_t) BgL_arg3399z00_8876), BgL_arg0z00_7550);
					}
					BgL_res4034z00_8877 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg3388z00_8874,
						BgL_arg3391z00_8875);
				}
				return BgL_res4034z00_8877;
			}
		}

	}



/* <@anonymous:3392>~0 */
	obj_t BGl_zc3z04anonymousza33392ze3ze70z60zzast_walkz00(obj_t
		BgL_arg0z00_8650, obj_t BgL_pz00_8649, obj_t BgL_l1598z00_5384)
	{
		{	/* Ast/walk.scm 315 */
			if (NULLP(BgL_l1598z00_5384))
				{	/* Ast/walk.scm 315 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 315 */
					obj_t BgL_arg3394z00_5387;
					obj_t BgL_arg3395z00_5388;

					{	/* Ast/walk.scm 315 */
						obj_t BgL_fz00_5389;

						BgL_fz00_5389 = CAR(((obj_t) BgL_l1598z00_5384));
						{	/* Ast/walk.scm 316 */
							obj_t BgL_arg3396z00_5390;

							BgL_arg3396z00_5390 =
								(((BgL_sfunz00_bglt) COBJECT(
										((BgL_sfunz00_bglt)
											(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_fz00_5389)))->
												BgL_valuez00))))->BgL_bodyz00);
							BgL_arg3394z00_5387 =
								BGL_PROCEDURE_CALL2(BgL_pz00_8649, BgL_arg3396z00_5390,
								BgL_arg0z00_8650);
						}
					}
					{	/* Ast/walk.scm 315 */
						obj_t BgL_arg3398z00_5393;

						BgL_arg3398z00_5393 = CDR(((obj_t) BgL_l1598z00_5384));
						BgL_arg3395z00_5388 =
							BGl_zc3z04anonymousza33392ze3ze70z60zzast_walkz00
							(BgL_arg0z00_8650, BgL_pz00_8649, BgL_arg3398z00_5393);
					}
					return bgl_append2(BgL_arg3394z00_5387, BgL_arg3395z00_5388);
				}
		}

	}



/* &walk0*-let-fun2132 */
	obj_t BGl_z62walk0za2zd2letzd2fun2132zc0zzast_walkz00(obj_t BgL_envz00_7551,
		obj_t BgL_nz00_7552, obj_t BgL_pz00_7553)
	{
		{	/* Ast/walk.scm 315 */
			{	/* Ast/walk.scm 315 */
				obj_t BgL_res4035z00_8882;

				{	/* Ast/walk.scm 315 */
					obj_t BgL_arg3375z00_8879;
					obj_t BgL_arg3379z00_8880;

					BgL_arg3375z00_8879 =
						BGl_zc3z04anonymousza33380ze3ze70z60zzast_walkz00(BgL_pz00_7553,
						(((BgL_letzd2funzd2_bglt) COBJECT(
									((BgL_letzd2funzd2_bglt) BgL_nz00_7552)))->BgL_localsz00));
					{	/* Ast/walk.scm 315 */
						BgL_nodez00_bglt BgL_arg3387z00_8881;

						BgL_arg3387z00_8881 =
							(((BgL_letzd2funzd2_bglt) COBJECT(
									((BgL_letzd2funzd2_bglt) BgL_nz00_7552)))->BgL_bodyz00);
						BgL_arg3379z00_8880 =
							BGL_PROCEDURE_CALL1(BgL_pz00_7553, ((obj_t) BgL_arg3387z00_8881));
					}
					BgL_res4035z00_8882 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg3375z00_8879,
						BgL_arg3379z00_8880);
				}
				return BgL_res4035z00_8882;
			}
		}

	}



/* <@anonymous:3380>~0 */
	obj_t BGl_zc3z04anonymousza33380ze3ze70z60zzast_walkz00(obj_t BgL_pz00_8648,
		obj_t BgL_l1595z00_5361)
	{
		{	/* Ast/walk.scm 315 */
			if (NULLP(BgL_l1595z00_5361))
				{	/* Ast/walk.scm 315 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 315 */
					obj_t BgL_arg3382z00_5364;
					obj_t BgL_arg3383z00_5365;

					{	/* Ast/walk.scm 315 */
						obj_t BgL_fz00_5366;

						BgL_fz00_5366 = CAR(((obj_t) BgL_l1595z00_5361));
						{	/* Ast/walk.scm 316 */
							obj_t BgL_arg3384z00_5367;

							BgL_arg3384z00_5367 =
								(((BgL_sfunz00_bglt) COBJECT(
										((BgL_sfunz00_bglt)
											(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_fz00_5366)))->
												BgL_valuez00))))->BgL_bodyz00);
							BgL_arg3382z00_5364 =
								BGL_PROCEDURE_CALL1(BgL_pz00_8648, BgL_arg3384z00_5367);
						}
					}
					{	/* Ast/walk.scm 315 */
						obj_t BgL_arg3386z00_5370;

						BgL_arg3386z00_5370 = CDR(((obj_t) BgL_l1595z00_5361));
						BgL_arg3383z00_5365 =
							BGl_zc3z04anonymousza33380ze3ze70z60zzast_walkz00(BgL_pz00_8648,
							BgL_arg3386z00_5370);
					}
					return bgl_append2(BgL_arg3382z00_5364, BgL_arg3383z00_5365);
				}
		}

	}



/* &walk3-let-fun2130 */
	obj_t BGl_z62walk3zd2letzd2fun2130z62zzast_walkz00(obj_t BgL_envz00_7554,
		obj_t BgL_nz00_7555, obj_t BgL_pz00_7556, obj_t BgL_arg0z00_7557,
		obj_t BgL_arg1z00_7558, obj_t BgL_arg2z00_7559)
	{
		{	/* Ast/walk.scm 315 */
			{	/* Ast/walk.scm 315 */
				obj_t BgL_g1593z00_8884;

				BgL_g1593z00_8884 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nz00_7555)))->BgL_localsz00);
				{
					obj_t BgL_l1591z00_8886;

					BgL_l1591z00_8886 = BgL_g1593z00_8884;
				BgL_zc3z04anonymousza33368ze3z87_8885:
					if (PAIRP(BgL_l1591z00_8886))
						{	/* Ast/walk.scm 315 */
							{	/* Ast/walk.scm 315 */
								obj_t BgL_fz00_8887;

								BgL_fz00_8887 = CAR(BgL_l1591z00_8886);
								{	/* Ast/walk.scm 316 */
									obj_t BgL_arg3370z00_8888;

									BgL_arg3370z00_8888 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt)
													(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt) BgL_fz00_8887)))->
														BgL_valuez00))))->BgL_bodyz00);
									BGL_PROCEDURE_CALL4(BgL_pz00_7556, BgL_arg3370z00_8888,
										BgL_arg0z00_7557, BgL_arg1z00_7558, BgL_arg2z00_7559);
								}
							}
							{
								obj_t BgL_l1591z00_11642;

								BgL_l1591z00_11642 = CDR(BgL_l1591z00_8886);
								BgL_l1591z00_8886 = BgL_l1591z00_11642;
								goto BgL_zc3z04anonymousza33368ze3z87_8885;
							}
						}
					else
						{	/* Ast/walk.scm 315 */
							((bool_t) 1);
						}
				}
			}
			{	/* Ast/walk.scm 315 */
				BgL_nodez00_bglt BgL_arg3373z00_8889;

				BgL_arg3373z00_8889 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nz00_7555)))->BgL_bodyz00);
				return
					BGL_PROCEDURE_CALL4(BgL_pz00_7556,
					((obj_t) BgL_arg3373z00_8889), BgL_arg0z00_7557, BgL_arg1z00_7558,
					BgL_arg2z00_7559);
			}
		}

	}



/* &walk2-let-fun2128 */
	obj_t BGl_z62walk2zd2letzd2fun2128z62zzast_walkz00(obj_t BgL_envz00_7560,
		obj_t BgL_nz00_7561, obj_t BgL_pz00_7562, obj_t BgL_arg0z00_7563,
		obj_t BgL_arg1z00_7564)
	{
		{	/* Ast/walk.scm 315 */
			{	/* Ast/walk.scm 315 */
				obj_t BgL_g1590z00_8891;

				BgL_g1590z00_8891 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nz00_7561)))->BgL_localsz00);
				{
					obj_t BgL_l1588z00_8893;

					BgL_l1588z00_8893 = BgL_g1590z00_8891;
				BgL_zc3z04anonymousza33359ze3z87_8892:
					if (PAIRP(BgL_l1588z00_8893))
						{	/* Ast/walk.scm 315 */
							{	/* Ast/walk.scm 315 */
								obj_t BgL_fz00_8894;

								BgL_fz00_8894 = CAR(BgL_l1588z00_8893);
								{	/* Ast/walk.scm 316 */
									obj_t BgL_arg3361z00_8895;

									BgL_arg3361z00_8895 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt)
													(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt) BgL_fz00_8894)))->
														BgL_valuez00))))->BgL_bodyz00);
									BGL_PROCEDURE_CALL3(BgL_pz00_7562, BgL_arg3361z00_8895,
										BgL_arg0z00_7563, BgL_arg1z00_7564);
								}
							}
							{
								obj_t BgL_l1588z00_11669;

								BgL_l1588z00_11669 = CDR(BgL_l1588z00_8893);
								BgL_l1588z00_8893 = BgL_l1588z00_11669;
								goto BgL_zc3z04anonymousza33359ze3z87_8892;
							}
						}
					else
						{	/* Ast/walk.scm 315 */
							((bool_t) 1);
						}
				}
			}
			{	/* Ast/walk.scm 315 */
				BgL_nodez00_bglt BgL_arg3367z00_8896;

				BgL_arg3367z00_8896 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nz00_7561)))->BgL_bodyz00);
				return
					BGL_PROCEDURE_CALL3(BgL_pz00_7562,
					((obj_t) BgL_arg3367z00_8896), BgL_arg0z00_7563, BgL_arg1z00_7564);
			}
		}

	}



/* &walk1-let-fun2126 */
	obj_t BGl_z62walk1zd2letzd2fun2126z62zzast_walkz00(obj_t BgL_envz00_7565,
		obj_t BgL_nz00_7566, obj_t BgL_pz00_7567, obj_t BgL_arg0z00_7568)
	{
		{	/* Ast/walk.scm 315 */
			{	/* Ast/walk.scm 315 */
				obj_t BgL_g1587z00_8898;

				BgL_g1587z00_8898 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nz00_7566)))->BgL_localsz00);
				{
					obj_t BgL_l1585z00_8900;

					BgL_l1585z00_8900 = BgL_g1587z00_8898;
				BgL_zc3z04anonymousza33350ze3z87_8899:
					if (PAIRP(BgL_l1585z00_8900))
						{	/* Ast/walk.scm 315 */
							{	/* Ast/walk.scm 315 */
								obj_t BgL_fz00_8901;

								BgL_fz00_8901 = CAR(BgL_l1585z00_8900);
								{	/* Ast/walk.scm 316 */
									obj_t BgL_arg3352z00_8902;

									BgL_arg3352z00_8902 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt)
													(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt) BgL_fz00_8901)))->
														BgL_valuez00))))->BgL_bodyz00);
									BGL_PROCEDURE_CALL2(BgL_pz00_7567, BgL_arg3352z00_8902,
										BgL_arg0z00_7568);
								}
							}
							{
								obj_t BgL_l1585z00_11694;

								BgL_l1585z00_11694 = CDR(BgL_l1585z00_8900);
								BgL_l1585z00_8900 = BgL_l1585z00_11694;
								goto BgL_zc3z04anonymousza33350ze3z87_8899;
							}
						}
					else
						{	/* Ast/walk.scm 315 */
							((bool_t) 1);
						}
				}
			}
			{	/* Ast/walk.scm 315 */
				BgL_nodez00_bglt BgL_arg3358z00_8903;

				BgL_arg3358z00_8903 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nz00_7566)))->BgL_bodyz00);
				return
					BGL_PROCEDURE_CALL2(BgL_pz00_7567,
					((obj_t) BgL_arg3358z00_8903), BgL_arg0z00_7568);
			}
		}

	}



/* &walk0-let-fun2124 */
	obj_t BGl_z62walk0zd2letzd2fun2124z62zzast_walkz00(obj_t BgL_envz00_7569,
		obj_t BgL_nz00_7570, obj_t BgL_pz00_7571)
	{
		{	/* Ast/walk.scm 315 */
			{	/* Ast/walk.scm 315 */
				obj_t BgL_g1584z00_8905;

				BgL_g1584z00_8905 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nz00_7570)))->BgL_localsz00);
				{
					obj_t BgL_l1582z00_8907;

					BgL_l1582z00_8907 = BgL_g1584z00_8905;
				BgL_zc3z04anonymousza33328ze3z87_8906:
					if (PAIRP(BgL_l1582z00_8907))
						{	/* Ast/walk.scm 315 */
							{	/* Ast/walk.scm 315 */
								obj_t BgL_fz00_8908;

								BgL_fz00_8908 = CAR(BgL_l1582z00_8907);
								{	/* Ast/walk.scm 316 */
									obj_t BgL_arg3337z00_8909;

									BgL_arg3337z00_8909 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt)
													(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt) BgL_fz00_8908)))->
														BgL_valuez00))))->BgL_bodyz00);
									BGL_PROCEDURE_CALL1(BgL_pz00_7571, BgL_arg3337z00_8909);
								}
							}
							{
								obj_t BgL_l1582z00_11717;

								BgL_l1582z00_11717 = CDR(BgL_l1582z00_8907);
								BgL_l1582z00_8907 = BgL_l1582z00_11717;
								goto BgL_zc3z04anonymousza33328ze3z87_8906;
							}
						}
					else
						{	/* Ast/walk.scm 315 */
							((bool_t) 1);
						}
				}
			}
			{	/* Ast/walk.scm 315 */
				BgL_nodez00_bglt BgL_arg3349z00_8910;

				BgL_arg3349z00_8910 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nz00_7570)))->BgL_bodyz00);
				return
					BGL_PROCEDURE_CALL1(BgL_pz00_7571, ((obj_t) BgL_arg3349z00_8910));
			}
		}

	}



/* &walk3!-switch2122 */
	BgL_nodez00_bglt BGl_z62walk3z12zd2switch2122za2zzast_walkz00(obj_t
		BgL_envz00_7572, obj_t BgL_nz00_7573, obj_t BgL_pz00_7574,
		obj_t BgL_arg0z00_7575, obj_t BgL_arg1z00_7576, obj_t BgL_arg2z00_7577)
	{
		{	/* Ast/walk.scm 312 */
			{
				BgL_nodez00_bglt BgL_auxz00_11726;

				{	/* Ast/walk.scm 312 */
					BgL_nodez00_bglt BgL_arg3315z00_8912;

					BgL_arg3315z00_8912 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nz00_7573)))->BgL_testz00);
					BgL_auxz00_11726 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_7574,
							((obj_t) BgL_arg3315z00_8912), BgL_arg0z00_7575, BgL_arg1z00_7576,
							BgL_arg2z00_7577));
				}
				((((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nz00_7573)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_11726), BUNSPEC);
			}
			{
				obj_t BgL_fieldsz00_8914;

				BgL_fieldsz00_8914 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nz00_7573)))->BgL_clausesz00);
			BgL_loopz00_8913:
				if (NULLP(BgL_fieldsz00_8914))
					{	/* Ast/walk.scm 312 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 312 */
						{	/* Ast/walk.scm 312 */
							obj_t BgL_arg3321z00_8915;
							obj_t BgL_arg3322z00_8916;

							BgL_arg3321z00_8915 = CAR(((obj_t) BgL_fieldsz00_8914));
							{	/* Ast/walk.scm 312 */
								obj_t BgL_arg3323z00_8917;

								{	/* Ast/walk.scm 312 */
									obj_t BgL_pairz00_8918;

									BgL_pairz00_8918 = CAR(((obj_t) BgL_fieldsz00_8914));
									BgL_arg3323z00_8917 = CDR(BgL_pairz00_8918);
								}
								BgL_arg3322z00_8916 =
									BGL_PROCEDURE_CALL4(BgL_pz00_7574, BgL_arg3323z00_8917,
									BgL_arg0z00_7575, BgL_arg1z00_7576, BgL_arg2z00_7577);
							}
							{	/* Ast/walk.scm 312 */
								obj_t BgL_tmpz00_11754;

								BgL_tmpz00_11754 = ((obj_t) BgL_arg3321z00_8915);
								SET_CDR(BgL_tmpz00_11754, BgL_arg3322z00_8916);
							}
						}
						{	/* Ast/walk.scm 312 */
							obj_t BgL_arg3327z00_8919;

							BgL_arg3327z00_8919 = CDR(((obj_t) BgL_fieldsz00_8914));
							{
								obj_t BgL_fieldsz00_11759;

								BgL_fieldsz00_11759 = BgL_arg3327z00_8919;
								BgL_fieldsz00_8914 = BgL_fieldsz00_11759;
								goto BgL_loopz00_8913;
							}
						}
					}
			}
			return ((BgL_nodez00_bglt) ((BgL_switchz00_bglt) BgL_nz00_7573));
		}

	}



/* &walk2!-switch2120 */
	BgL_nodez00_bglt BGl_z62walk2z12zd2switch2120za2zzast_walkz00(obj_t
		BgL_envz00_7578, obj_t BgL_nz00_7579, obj_t BgL_pz00_7580,
		obj_t BgL_arg0z00_7581, obj_t BgL_arg1z00_7582)
	{
		{	/* Ast/walk.scm 312 */
			{
				BgL_nodez00_bglt BgL_auxz00_11764;

				{	/* Ast/walk.scm 312 */
					BgL_nodez00_bglt BgL_arg3306z00_8921;

					BgL_arg3306z00_8921 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nz00_7579)))->BgL_testz00);
					BgL_auxz00_11764 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_7580,
							((obj_t) BgL_arg3306z00_8921), BgL_arg0z00_7581,
							BgL_arg1z00_7582));
				}
				((((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nz00_7579)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_11764), BUNSPEC);
			}
			{
				obj_t BgL_fieldsz00_8923;

				BgL_fieldsz00_8923 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nz00_7579)))->BgL_clausesz00);
			BgL_loopz00_8922:
				if (NULLP(BgL_fieldsz00_8923))
					{	/* Ast/walk.scm 312 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 312 */
						{	/* Ast/walk.scm 312 */
							obj_t BgL_arg3310z00_8924;
							obj_t BgL_arg3311z00_8925;

							BgL_arg3310z00_8924 = CAR(((obj_t) BgL_fieldsz00_8923));
							{	/* Ast/walk.scm 312 */
								obj_t BgL_arg3312z00_8926;

								{	/* Ast/walk.scm 312 */
									obj_t BgL_pairz00_8927;

									BgL_pairz00_8927 = CAR(((obj_t) BgL_fieldsz00_8923));
									BgL_arg3312z00_8926 = CDR(BgL_pairz00_8927);
								}
								BgL_arg3311z00_8925 =
									BGL_PROCEDURE_CALL3(BgL_pz00_7580, BgL_arg3312z00_8926,
									BgL_arg0z00_7581, BgL_arg1z00_7582);
							}
							{	/* Ast/walk.scm 312 */
								obj_t BgL_tmpz00_11790;

								BgL_tmpz00_11790 = ((obj_t) BgL_arg3310z00_8924);
								SET_CDR(BgL_tmpz00_11790, BgL_arg3311z00_8925);
							}
						}
						{	/* Ast/walk.scm 312 */
							obj_t BgL_arg3314z00_8928;

							BgL_arg3314z00_8928 = CDR(((obj_t) BgL_fieldsz00_8923));
							{
								obj_t BgL_fieldsz00_11795;

								BgL_fieldsz00_11795 = BgL_arg3314z00_8928;
								BgL_fieldsz00_8923 = BgL_fieldsz00_11795;
								goto BgL_loopz00_8922;
							}
						}
					}
			}
			return ((BgL_nodez00_bglt) ((BgL_switchz00_bglt) BgL_nz00_7579));
		}

	}



/* &walk1!-switch2118 */
	BgL_nodez00_bglt BGl_z62walk1z12zd2switch2118za2zzast_walkz00(obj_t
		BgL_envz00_7583, obj_t BgL_nz00_7584, obj_t BgL_pz00_7585,
		obj_t BgL_arg0z00_7586)
	{
		{	/* Ast/walk.scm 312 */
			{
				BgL_nodez00_bglt BgL_auxz00_11800;

				{	/* Ast/walk.scm 312 */
					BgL_nodez00_bglt BgL_arg3294z00_8930;

					BgL_arg3294z00_8930 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nz00_7584)))->BgL_testz00);
					BgL_auxz00_11800 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_7585,
							((obj_t) BgL_arg3294z00_8930), BgL_arg0z00_7586));
				}
				((((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nz00_7584)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_11800), BUNSPEC);
			}
			{
				obj_t BgL_fieldsz00_8932;

				BgL_fieldsz00_8932 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nz00_7584)))->BgL_clausesz00);
			BgL_loopz00_8931:
				if (NULLP(BgL_fieldsz00_8932))
					{	/* Ast/walk.scm 312 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 312 */
						{	/* Ast/walk.scm 312 */
							obj_t BgL_arg3298z00_8933;
							obj_t BgL_arg3299z00_8934;

							BgL_arg3298z00_8933 = CAR(((obj_t) BgL_fieldsz00_8932));
							{	/* Ast/walk.scm 312 */
								obj_t BgL_arg3301z00_8935;

								{	/* Ast/walk.scm 312 */
									obj_t BgL_pairz00_8936;

									BgL_pairz00_8936 = CAR(((obj_t) BgL_fieldsz00_8932));
									BgL_arg3301z00_8935 = CDR(BgL_pairz00_8936);
								}
								BgL_arg3299z00_8934 =
									BGL_PROCEDURE_CALL2(BgL_pz00_7585, BgL_arg3301z00_8935,
									BgL_arg0z00_7586);
							}
							{	/* Ast/walk.scm 312 */
								obj_t BgL_tmpz00_11824;

								BgL_tmpz00_11824 = ((obj_t) BgL_arg3298z00_8933);
								SET_CDR(BgL_tmpz00_11824, BgL_arg3299z00_8934);
							}
						}
						{	/* Ast/walk.scm 312 */
							obj_t BgL_arg3305z00_8937;

							BgL_arg3305z00_8937 = CDR(((obj_t) BgL_fieldsz00_8932));
							{
								obj_t BgL_fieldsz00_11829;

								BgL_fieldsz00_11829 = BgL_arg3305z00_8937;
								BgL_fieldsz00_8932 = BgL_fieldsz00_11829;
								goto BgL_loopz00_8931;
							}
						}
					}
			}
			return ((BgL_nodez00_bglt) ((BgL_switchz00_bglt) BgL_nz00_7584));
		}

	}



/* &walk0!-switch2116 */
	BgL_nodez00_bglt BGl_z62walk0z12zd2switch2116za2zzast_walkz00(obj_t
		BgL_envz00_7587, obj_t BgL_nz00_7588, obj_t BgL_pz00_7589)
	{
		{	/* Ast/walk.scm 312 */
			{
				BgL_nodez00_bglt BgL_auxz00_11834;

				{	/* Ast/walk.scm 312 */
					BgL_nodez00_bglt BgL_arg3276z00_8939;

					BgL_arg3276z00_8939 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nz00_7588)))->BgL_testz00);
					BgL_auxz00_11834 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_7589, ((obj_t) BgL_arg3276z00_8939)));
				}
				((((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nz00_7588)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_11834), BUNSPEC);
			}
			{
				obj_t BgL_fieldsz00_8941;

				BgL_fieldsz00_8941 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nz00_7588)))->BgL_clausesz00);
			BgL_loopz00_8940:
				if (NULLP(BgL_fieldsz00_8941))
					{	/* Ast/walk.scm 312 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 312 */
						{	/* Ast/walk.scm 312 */
							obj_t BgL_arg3287z00_8942;
							obj_t BgL_arg3288z00_8943;

							BgL_arg3287z00_8942 = CAR(((obj_t) BgL_fieldsz00_8941));
							{	/* Ast/walk.scm 312 */
								obj_t BgL_arg3289z00_8944;

								{	/* Ast/walk.scm 312 */
									obj_t BgL_pairz00_8945;

									BgL_pairz00_8945 = CAR(((obj_t) BgL_fieldsz00_8941));
									BgL_arg3289z00_8944 = CDR(BgL_pairz00_8945);
								}
								BgL_arg3288z00_8943 =
									BGL_PROCEDURE_CALL1(BgL_pz00_7589, BgL_arg3289z00_8944);
							}
							{	/* Ast/walk.scm 312 */
								obj_t BgL_tmpz00_11856;

								BgL_tmpz00_11856 = ((obj_t) BgL_arg3287z00_8942);
								SET_CDR(BgL_tmpz00_11856, BgL_arg3288z00_8943);
							}
						}
						{	/* Ast/walk.scm 312 */
							obj_t BgL_arg3292z00_8946;

							BgL_arg3292z00_8946 = CDR(((obj_t) BgL_fieldsz00_8941));
							{
								obj_t BgL_fieldsz00_11861;

								BgL_fieldsz00_11861 = BgL_arg3292z00_8946;
								BgL_fieldsz00_8941 = BgL_fieldsz00_11861;
								goto BgL_loopz00_8940;
							}
						}
					}
			}
			return ((BgL_nodez00_bglt) ((BgL_switchz00_bglt) BgL_nz00_7588));
		}

	}



/* &walk3*-switch2114 */
	obj_t BGl_z62walk3za2zd2switch2114z12zzast_walkz00(obj_t BgL_envz00_7590,
		obj_t BgL_nz00_7591, obj_t BgL_pz00_7592, obj_t BgL_arg0z00_7593,
		obj_t BgL_arg1z00_7594, obj_t BgL_arg2z00_7595)
	{
		{	/* Ast/walk.scm 312 */
			{	/* Ast/walk.scm 312 */
				obj_t BgL_res4036z00_8951;

				{	/* Ast/walk.scm 312 */
					obj_t BgL_arg3264z00_8948;
					obj_t BgL_arg3266z00_8949;

					{	/* Ast/walk.scm 312 */
						BgL_nodez00_bglt BgL_arg3267z00_8950;

						BgL_arg3267z00_8950 =
							(((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt) BgL_nz00_7591)))->BgL_testz00);
						BgL_arg3264z00_8948 =
							BGL_PROCEDURE_CALL4(BgL_pz00_7592,
							((obj_t) BgL_arg3267z00_8950), BgL_arg0z00_7593, BgL_arg1z00_7594,
							BgL_arg2z00_7595);
					}
					BgL_arg3266z00_8949 =
						BGl_zc3z04anonymousza33268ze3ze70z60zzast_walkz00(BgL_arg2z00_7595,
						BgL_arg1z00_7594, BgL_arg0z00_7593, BgL_pz00_7592,
						(((BgL_switchz00_bglt) COBJECT(((BgL_switchz00_bglt)
										BgL_nz00_7591)))->BgL_clausesz00));
					BgL_res4036z00_8951 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg3264z00_8948,
						BgL_arg3266z00_8949);
				}
				return BgL_res4036z00_8951;
			}
		}

	}



/* <@anonymous:3268>~0 */
	obj_t BGl_zc3z04anonymousza33268ze3ze70z60zzast_walkz00(obj_t
		BgL_arg2z00_8647, obj_t BgL_arg1z00_8646, obj_t BgL_arg0z00_8645,
		obj_t BgL_pz00_8644, obj_t BgL_l1579z00_5186)
	{
		{	/* Ast/walk.scm 312 */
			if (NULLP(BgL_l1579z00_5186))
				{	/* Ast/walk.scm 312 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 312 */
					obj_t BgL_arg3271z00_5189;
					obj_t BgL_arg3272z00_5190;

					{	/* Ast/walk.scm 312 */
						obj_t BgL_fz00_5191;

						BgL_fz00_5191 = CAR(((obj_t) BgL_l1579z00_5186));
						{	/* Ast/walk.scm 312 */
							obj_t BgL_arg3273z00_5192;

							BgL_arg3273z00_5192 = CDR(((obj_t) BgL_fz00_5191));
							BgL_arg3271z00_5189 =
								BGL_PROCEDURE_CALL4(BgL_pz00_8644, BgL_arg3273z00_5192,
								BgL_arg0z00_8645, BgL_arg1z00_8646, BgL_arg2z00_8647);
						}
					}
					{	/* Ast/walk.scm 312 */
						obj_t BgL_arg3275z00_5193;

						BgL_arg3275z00_5193 = CDR(((obj_t) BgL_l1579z00_5186));
						BgL_arg3272z00_5190 =
							BGl_zc3z04anonymousza33268ze3ze70z60zzast_walkz00
							(BgL_arg2z00_8647, BgL_arg1z00_8646, BgL_arg0z00_8645,
							BgL_pz00_8644, BgL_arg3275z00_5193);
					}
					return bgl_append2(BgL_arg3271z00_5189, BgL_arg3272z00_5190);
				}
		}

	}



/* &walk2*-switch2112 */
	obj_t BGl_z62walk2za2zd2switch2112z12zzast_walkz00(obj_t BgL_envz00_7596,
		obj_t BgL_nz00_7597, obj_t BgL_pz00_7598, obj_t BgL_arg0z00_7599,
		obj_t BgL_arg1z00_7600)
	{
		{	/* Ast/walk.scm 312 */
			{	/* Ast/walk.scm 312 */
				obj_t BgL_res4037z00_8956;

				{	/* Ast/walk.scm 312 */
					obj_t BgL_arg3250z00_8953;
					obj_t BgL_arg3251z00_8954;

					{	/* Ast/walk.scm 312 */
						BgL_nodez00_bglt BgL_arg3252z00_8955;

						BgL_arg3252z00_8955 =
							(((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt) BgL_nz00_7597)))->BgL_testz00);
						BgL_arg3250z00_8953 =
							BGL_PROCEDURE_CALL3(BgL_pz00_7598,
							((obj_t) BgL_arg3252z00_8955), BgL_arg0z00_7599,
							BgL_arg1z00_7600);
					}
					BgL_arg3251z00_8954 =
						BGl_zc3z04anonymousza33253ze3ze70z60zzast_walkz00(BgL_arg1z00_7600,
						BgL_arg0z00_7599, BgL_pz00_7598,
						(((BgL_switchz00_bglt) COBJECT(((BgL_switchz00_bglt)
										BgL_nz00_7597)))->BgL_clausesz00));
					BgL_res4037z00_8956 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg3250z00_8953,
						BgL_arg3251z00_8954);
				}
				return BgL_res4037z00_8956;
			}
		}

	}



/* <@anonymous:3253>~0 */
	obj_t BGl_zc3z04anonymousza33253ze3ze70z60zzast_walkz00(obj_t
		BgL_arg1z00_8643, obj_t BgL_arg0z00_8642, obj_t BgL_pz00_8641,
		obj_t BgL_l1576z00_5163)
	{
		{	/* Ast/walk.scm 312 */
			if (NULLP(BgL_l1576z00_5163))
				{	/* Ast/walk.scm 312 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 312 */
					obj_t BgL_arg3255z00_5166;
					obj_t BgL_arg3256z00_5167;

					{	/* Ast/walk.scm 312 */
						obj_t BgL_fz00_5168;

						BgL_fz00_5168 = CAR(((obj_t) BgL_l1576z00_5163));
						{	/* Ast/walk.scm 312 */
							obj_t BgL_arg3259z00_5169;

							BgL_arg3259z00_5169 = CDR(((obj_t) BgL_fz00_5168));
							BgL_arg3255z00_5166 =
								BGL_PROCEDURE_CALL3(BgL_pz00_8641, BgL_arg3259z00_5169,
								BgL_arg0z00_8642, BgL_arg1z00_8643);
						}
					}
					{	/* Ast/walk.scm 312 */
						obj_t BgL_arg3263z00_5170;

						BgL_arg3263z00_5170 = CDR(((obj_t) BgL_l1576z00_5163));
						BgL_arg3256z00_5167 =
							BGl_zc3z04anonymousza33253ze3ze70z60zzast_walkz00
							(BgL_arg1z00_8643, BgL_arg0z00_8642, BgL_pz00_8641,
							BgL_arg3263z00_5170);
					}
					return bgl_append2(BgL_arg3255z00_5166, BgL_arg3256z00_5167);
				}
		}

	}



/* &walk1*-switch2110 */
	obj_t BGl_z62walk1za2zd2switch2110z12zzast_walkz00(obj_t BgL_envz00_7601,
		obj_t BgL_nz00_7602, obj_t BgL_pz00_7603, obj_t BgL_arg0z00_7604)
	{
		{	/* Ast/walk.scm 312 */
			{	/* Ast/walk.scm 312 */
				obj_t BgL_res4038z00_8961;

				{	/* Ast/walk.scm 312 */
					obj_t BgL_arg3240z00_8958;
					obj_t BgL_arg3241z00_8959;

					{	/* Ast/walk.scm 312 */
						BgL_nodez00_bglt BgL_arg3242z00_8960;

						BgL_arg3242z00_8960 =
							(((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt) BgL_nz00_7602)))->BgL_testz00);
						BgL_arg3240z00_8958 =
							BGL_PROCEDURE_CALL2(BgL_pz00_7603,
							((obj_t) BgL_arg3242z00_8960), BgL_arg0z00_7604);
					}
					BgL_arg3241z00_8959 =
						BGl_zc3z04anonymousza33243ze3ze70z60zzast_walkz00(BgL_arg0z00_7604,
						BgL_pz00_7603,
						(((BgL_switchz00_bglt) COBJECT(((BgL_switchz00_bglt)
										BgL_nz00_7602)))->BgL_clausesz00));
					BgL_res4038z00_8961 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg3240z00_8958,
						BgL_arg3241z00_8959);
				}
				return BgL_res4038z00_8961;
			}
		}

	}



/* <@anonymous:3243>~0 */
	obj_t BGl_zc3z04anonymousza33243ze3ze70z60zzast_walkz00(obj_t
		BgL_arg0z00_8640, obj_t BgL_pz00_8639, obj_t BgL_l1573z00_5141)
	{
		{	/* Ast/walk.scm 312 */
			if (NULLP(BgL_l1573z00_5141))
				{	/* Ast/walk.scm 312 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 312 */
					obj_t BgL_arg3245z00_5144;
					obj_t BgL_arg3246z00_5145;

					{	/* Ast/walk.scm 312 */
						obj_t BgL_fz00_5146;

						BgL_fz00_5146 = CAR(((obj_t) BgL_l1573z00_5141));
						{	/* Ast/walk.scm 312 */
							obj_t BgL_arg3247z00_5147;

							BgL_arg3247z00_5147 = CDR(((obj_t) BgL_fz00_5146));
							BgL_arg3245z00_5144 =
								BGL_PROCEDURE_CALL2(BgL_pz00_8639, BgL_arg3247z00_5147,
								BgL_arg0z00_8640);
						}
					}
					{	/* Ast/walk.scm 312 */
						obj_t BgL_arg3249z00_5148;

						BgL_arg3249z00_5148 = CDR(((obj_t) BgL_l1573z00_5141));
						BgL_arg3246z00_5145 =
							BGl_zc3z04anonymousza33243ze3ze70z60zzast_walkz00
							(BgL_arg0z00_8640, BgL_pz00_8639, BgL_arg3249z00_5148);
					}
					return bgl_append2(BgL_arg3245z00_5144, BgL_arg3246z00_5145);
				}
		}

	}



/* &walk0*-switch2108 */
	obj_t BGl_z62walk0za2zd2switch2108z12zzast_walkz00(obj_t BgL_envz00_7605,
		obj_t BgL_nz00_7606, obj_t BgL_pz00_7607)
	{
		{	/* Ast/walk.scm 312 */
			{	/* Ast/walk.scm 312 */
				obj_t BgL_res4039z00_8966;

				{	/* Ast/walk.scm 312 */
					obj_t BgL_arg3229z00_8963;
					obj_t BgL_arg3230z00_8964;

					{	/* Ast/walk.scm 312 */
						BgL_nodez00_bglt BgL_arg3231z00_8965;

						BgL_arg3231z00_8965 =
							(((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt) BgL_nz00_7606)))->BgL_testz00);
						BgL_arg3229z00_8963 =
							BGL_PROCEDURE_CALL1(BgL_pz00_7607, ((obj_t) BgL_arg3231z00_8965));
					}
					BgL_arg3230z00_8964 =
						BGl_zc3z04anonymousza33232ze3ze70z60zzast_walkz00(BgL_pz00_7607,
						(((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt) BgL_nz00_7606)))->BgL_clausesz00));
					BgL_res4039z00_8966 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg3229z00_8963,
						BgL_arg3230z00_8964);
				}
				return BgL_res4039z00_8966;
			}
		}

	}



/* <@anonymous:3232>~0 */
	obj_t BGl_zc3z04anonymousza33232ze3ze70z60zzast_walkz00(obj_t BgL_pz00_8638,
		obj_t BgL_l1570z00_5120)
	{
		{	/* Ast/walk.scm 312 */
			if (NULLP(BgL_l1570z00_5120))
				{	/* Ast/walk.scm 312 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 312 */
					obj_t BgL_arg3234z00_5123;
					obj_t BgL_arg3236z00_5124;

					{	/* Ast/walk.scm 312 */
						obj_t BgL_fz00_5125;

						BgL_fz00_5125 = CAR(((obj_t) BgL_l1570z00_5120));
						{	/* Ast/walk.scm 312 */
							obj_t BgL_arg3237z00_5126;

							BgL_arg3237z00_5126 = CDR(((obj_t) BgL_fz00_5125));
							BgL_arg3234z00_5123 =
								BGL_PROCEDURE_CALL1(BgL_pz00_8638, BgL_arg3237z00_5126);
						}
					}
					{	/* Ast/walk.scm 312 */
						obj_t BgL_arg3239z00_5127;

						BgL_arg3239z00_5127 = CDR(((obj_t) BgL_l1570z00_5120));
						BgL_arg3236z00_5124 =
							BGl_zc3z04anonymousza33232ze3ze70z60zzast_walkz00(BgL_pz00_8638,
							BgL_arg3239z00_5127);
					}
					return bgl_append2(BgL_arg3234z00_5123, BgL_arg3236z00_5124);
				}
		}

	}



/* &walk3-switch2106 */
	obj_t BGl_z62walk3zd2switch2106zb0zzast_walkz00(obj_t BgL_envz00_7608,
		obj_t BgL_nz00_7609, obj_t BgL_pz00_7610, obj_t BgL_arg0z00_7611,
		obj_t BgL_arg1z00_7612, obj_t BgL_arg2z00_7613)
	{
		{	/* Ast/walk.scm 312 */
			{	/* Ast/walk.scm 312 */
				BgL_nodez00_bglt BgL_arg3220z00_8968;

				BgL_arg3220z00_8968 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nz00_7609)))->BgL_testz00);
				BGL_PROCEDURE_CALL4(BgL_pz00_7610,
					((obj_t) BgL_arg3220z00_8968), BgL_arg0z00_7611, BgL_arg1z00_7612,
					BgL_arg2z00_7613);
			}
			{	/* Ast/walk.scm 312 */
				obj_t BgL_g1568z00_8969;

				BgL_g1568z00_8969 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nz00_7609)))->BgL_clausesz00);
				{
					obj_t BgL_l1566z00_8971;

					{	/* Ast/walk.scm 312 */
						bool_t BgL_tmpz00_11990;

						BgL_l1566z00_8971 = BgL_g1568z00_8969;
					BgL_zc3z04anonymousza33221ze3z87_8970:
						if (PAIRP(BgL_l1566z00_8971))
							{	/* Ast/walk.scm 312 */
								{	/* Ast/walk.scm 312 */
									obj_t BgL_fz00_8972;

									BgL_fz00_8972 = CAR(BgL_l1566z00_8971);
									{	/* Ast/walk.scm 312 */
										obj_t BgL_arg3223z00_8973;

										BgL_arg3223z00_8973 = CDR(((obj_t) BgL_fz00_8972));
										BGL_PROCEDURE_CALL4(BgL_pz00_7610, BgL_arg3223z00_8973,
											BgL_arg0z00_7611, BgL_arg1z00_7612, BgL_arg2z00_7613);
									}
								}
								{
									obj_t BgL_l1566z00_12003;

									BgL_l1566z00_12003 = CDR(BgL_l1566z00_8971);
									BgL_l1566z00_8971 = BgL_l1566z00_12003;
									goto BgL_zc3z04anonymousza33221ze3z87_8970;
								}
							}
						else
							{	/* Ast/walk.scm 312 */
								BgL_tmpz00_11990 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_11990);
					}
				}
			}
		}

	}



/* &walk2-switch2104 */
	obj_t BGl_z62walk2zd2switch2104zb0zzast_walkz00(obj_t BgL_envz00_7614,
		obj_t BgL_nz00_7615, obj_t BgL_pz00_7616, obj_t BgL_arg0z00_7617,
		obj_t BgL_arg1z00_7618)
	{
		{	/* Ast/walk.scm 312 */
			{	/* Ast/walk.scm 312 */
				BgL_nodez00_bglt BgL_arg3211z00_8975;

				BgL_arg3211z00_8975 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nz00_7615)))->BgL_testz00);
				BGL_PROCEDURE_CALL3(BgL_pz00_7616,
					((obj_t) BgL_arg3211z00_8975), BgL_arg0z00_7617, BgL_arg1z00_7618);
			}
			{	/* Ast/walk.scm 312 */
				obj_t BgL_g1565z00_8976;

				BgL_g1565z00_8976 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nz00_7615)))->BgL_clausesz00);
				{
					obj_t BgL_l1563z00_8978;

					{	/* Ast/walk.scm 312 */
						bool_t BgL_tmpz00_12017;

						BgL_l1563z00_8978 = BgL_g1565z00_8976;
					BgL_zc3z04anonymousza33212ze3z87_8977:
						if (PAIRP(BgL_l1563z00_8978))
							{	/* Ast/walk.scm 312 */
								{	/* Ast/walk.scm 312 */
									obj_t BgL_fz00_8979;

									BgL_fz00_8979 = CAR(BgL_l1563z00_8978);
									{	/* Ast/walk.scm 312 */
										obj_t BgL_arg3217z00_8980;

										BgL_arg3217z00_8980 = CDR(((obj_t) BgL_fz00_8979));
										BGL_PROCEDURE_CALL3(BgL_pz00_7616, BgL_arg3217z00_8980,
											BgL_arg0z00_7617, BgL_arg1z00_7618);
									}
								}
								{
									obj_t BgL_l1563z00_12029;

									BgL_l1563z00_12029 = CDR(BgL_l1563z00_8978);
									BgL_l1563z00_8978 = BgL_l1563z00_12029;
									goto BgL_zc3z04anonymousza33212ze3z87_8977;
								}
							}
						else
							{	/* Ast/walk.scm 312 */
								BgL_tmpz00_12017 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_12017);
					}
				}
			}
		}

	}



/* &walk1-switch2102 */
	obj_t BGl_z62walk1zd2switch2102zb0zzast_walkz00(obj_t BgL_envz00_7619,
		obj_t BgL_nz00_7620, obj_t BgL_pz00_7621, obj_t BgL_arg0z00_7622)
	{
		{	/* Ast/walk.scm 312 */
			{	/* Ast/walk.scm 312 */
				BgL_nodez00_bglt BgL_arg3205z00_8982;

				BgL_arg3205z00_8982 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nz00_7620)))->BgL_testz00);
				BGL_PROCEDURE_CALL2(BgL_pz00_7621,
					((obj_t) BgL_arg3205z00_8982), BgL_arg0z00_7622);
			}
			{	/* Ast/walk.scm 312 */
				obj_t BgL_g1562z00_8983;

				BgL_g1562z00_8983 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nz00_7620)))->BgL_clausesz00);
				{
					obj_t BgL_l1560z00_8985;

					{	/* Ast/walk.scm 312 */
						bool_t BgL_tmpz00_12042;

						BgL_l1560z00_8985 = BgL_g1562z00_8983;
					BgL_zc3z04anonymousza33206ze3z87_8984:
						if (PAIRP(BgL_l1560z00_8985))
							{	/* Ast/walk.scm 312 */
								{	/* Ast/walk.scm 312 */
									obj_t BgL_fz00_8986;

									BgL_fz00_8986 = CAR(BgL_l1560z00_8985);
									{	/* Ast/walk.scm 312 */
										obj_t BgL_arg3208z00_8987;

										BgL_arg3208z00_8987 = CDR(((obj_t) BgL_fz00_8986));
										BGL_PROCEDURE_CALL2(BgL_pz00_7621, BgL_arg3208z00_8987,
											BgL_arg0z00_7622);
									}
								}
								{
									obj_t BgL_l1560z00_12053;

									BgL_l1560z00_12053 = CDR(BgL_l1560z00_8985);
									BgL_l1560z00_8985 = BgL_l1560z00_12053;
									goto BgL_zc3z04anonymousza33206ze3z87_8984;
								}
							}
						else
							{	/* Ast/walk.scm 312 */
								BgL_tmpz00_12042 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_12042);
					}
				}
			}
		}

	}



/* &walk0-switch2100 */
	obj_t BGl_z62walk0zd2switch2100zb0zzast_walkz00(obj_t BgL_envz00_7623,
		obj_t BgL_nz00_7624, obj_t BgL_pz00_7625)
	{
		{	/* Ast/walk.scm 312 */
			{	/* Ast/walk.scm 312 */
				BgL_nodez00_bglt BgL_arg3200z00_8989;

				BgL_arg3200z00_8989 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nz00_7624)))->BgL_testz00);
				BGL_PROCEDURE_CALL1(BgL_pz00_7625, ((obj_t) BgL_arg3200z00_8989));
			}
			{	/* Ast/walk.scm 312 */
				obj_t BgL_g1559z00_8990;

				BgL_g1559z00_8990 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nz00_7624)))->BgL_clausesz00);
				{
					obj_t BgL_l1557z00_8992;

					{	/* Ast/walk.scm 312 */
						bool_t BgL_tmpz00_12065;

						BgL_l1557z00_8992 = BgL_g1559z00_8990;
					BgL_zc3z04anonymousza33201ze3z87_8991:
						if (PAIRP(BgL_l1557z00_8992))
							{	/* Ast/walk.scm 312 */
								{	/* Ast/walk.scm 312 */
									obj_t BgL_fz00_8993;

									BgL_fz00_8993 = CAR(BgL_l1557z00_8992);
									{	/* Ast/walk.scm 312 */
										obj_t BgL_arg3203z00_8994;

										BgL_arg3203z00_8994 = CDR(((obj_t) BgL_fz00_8993));
										BGL_PROCEDURE_CALL1(BgL_pz00_7625, BgL_arg3203z00_8994);
									}
								}
								{
									obj_t BgL_l1557z00_12075;

									BgL_l1557z00_12075 = CDR(BgL_l1557z00_8992);
									BgL_l1557z00_8992 = BgL_l1557z00_12075;
									goto BgL_zc3z04anonymousza33201ze3z87_8991;
								}
							}
						else
							{	/* Ast/walk.scm 312 */
								BgL_tmpz00_12065 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_12065);
					}
				}
			}
		}

	}



/* &walk3!-sync2098 */
	BgL_nodez00_bglt BGl_z62walk3z12zd2sync2098za2zzast_walkz00(obj_t
		BgL_envz00_7626, obj_t BgL_nz00_7627, obj_t BgL_pz00_7628,
		obj_t BgL_arg0z00_7629, obj_t BgL_arg1z00_7630, obj_t BgL_arg2z00_7631)
	{
		{	/* Ast/walk.scm 311 */
			{
				BgL_nodez00_bglt BgL_auxz00_12078;

				{	/* Ast/walk.scm 311 */
					BgL_nodez00_bglt BgL_arg3192z00_8996;

					BgL_arg3192z00_8996 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nz00_7627)))->BgL_mutexz00);
					BgL_auxz00_12078 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_7628,
							((obj_t) BgL_arg3192z00_8996), BgL_arg0z00_7629, BgL_arg1z00_7630,
							BgL_arg2z00_7631));
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nz00_7627)))->BgL_mutexz00) =
					((BgL_nodez00_bglt) BgL_auxz00_12078), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_12092;

				{	/* Ast/walk.scm 311 */
					BgL_nodez00_bglt BgL_arg3195z00_8997;

					BgL_arg3195z00_8997 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nz00_7627)))->BgL_prelockz00);
					BgL_auxz00_12092 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_7628,
							((obj_t) BgL_arg3195z00_8997), BgL_arg0z00_7629, BgL_arg1z00_7630,
							BgL_arg2z00_7631));
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nz00_7627)))->BgL_prelockz00) =
					((BgL_nodez00_bglt) BgL_auxz00_12092), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_12106;

				{	/* Ast/walk.scm 311 */
					BgL_nodez00_bglt BgL_arg3199z00_8998;

					BgL_arg3199z00_8998 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nz00_7627)))->BgL_bodyz00);
					BgL_auxz00_12106 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_7628,
							((obj_t) BgL_arg3199z00_8998), BgL_arg0z00_7629, BgL_arg1z00_7630,
							BgL_arg2z00_7631));
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nz00_7627)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_12106), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_syncz00_bglt) BgL_nz00_7627));
		}

	}



/* &walk2!-sync2096 */
	BgL_nodez00_bglt BGl_z62walk2z12zd2sync2096za2zzast_walkz00(obj_t
		BgL_envz00_7632, obj_t BgL_nz00_7633, obj_t BgL_pz00_7634,
		obj_t BgL_arg0z00_7635, obj_t BgL_arg1z00_7636)
	{
		{	/* Ast/walk.scm 311 */
			{
				BgL_nodez00_bglt BgL_auxz00_12122;

				{	/* Ast/walk.scm 311 */
					BgL_nodez00_bglt BgL_arg3187z00_9000;

					BgL_arg3187z00_9000 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nz00_7633)))->BgL_mutexz00);
					BgL_auxz00_12122 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_7634,
							((obj_t) BgL_arg3187z00_9000), BgL_arg0z00_7635,
							BgL_arg1z00_7636));
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nz00_7633)))->BgL_mutexz00) =
					((BgL_nodez00_bglt) BgL_auxz00_12122), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_12135;

				{	/* Ast/walk.scm 311 */
					BgL_nodez00_bglt BgL_arg3189z00_9001;

					BgL_arg3189z00_9001 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nz00_7633)))->BgL_prelockz00);
					BgL_auxz00_12135 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_7634,
							((obj_t) BgL_arg3189z00_9001), BgL_arg0z00_7635,
							BgL_arg1z00_7636));
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nz00_7633)))->BgL_prelockz00) =
					((BgL_nodez00_bglt) BgL_auxz00_12135), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_12148;

				{	/* Ast/walk.scm 311 */
					BgL_nodez00_bglt BgL_arg3190z00_9002;

					BgL_arg3190z00_9002 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nz00_7633)))->BgL_bodyz00);
					BgL_auxz00_12148 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_7634,
							((obj_t) BgL_arg3190z00_9002), BgL_arg0z00_7635,
							BgL_arg1z00_7636));
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nz00_7633)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_12148), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_syncz00_bglt) BgL_nz00_7633));
		}

	}



/* &walk1!-sync2094 */
	BgL_nodez00_bglt BGl_z62walk1z12zd2sync2094za2zzast_walkz00(obj_t
		BgL_envz00_7637, obj_t BgL_nz00_7638, obj_t BgL_pz00_7639,
		obj_t BgL_arg0z00_7640)
	{
		{	/* Ast/walk.scm 311 */
			{
				BgL_nodez00_bglt BgL_auxz00_12163;

				{	/* Ast/walk.scm 311 */
					BgL_nodez00_bglt BgL_arg3182z00_9004;

					BgL_arg3182z00_9004 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nz00_7638)))->BgL_mutexz00);
					BgL_auxz00_12163 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_7639,
							((obj_t) BgL_arg3182z00_9004), BgL_arg0z00_7640));
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nz00_7638)))->BgL_mutexz00) =
					((BgL_nodez00_bglt) BgL_auxz00_12163), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_12175;

				{	/* Ast/walk.scm 311 */
					BgL_nodez00_bglt BgL_arg3185z00_9005;

					BgL_arg3185z00_9005 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nz00_7638)))->BgL_prelockz00);
					BgL_auxz00_12175 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_7639,
							((obj_t) BgL_arg3185z00_9005), BgL_arg0z00_7640));
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nz00_7638)))->BgL_prelockz00) =
					((BgL_nodez00_bglt) BgL_auxz00_12175), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_12187;

				{	/* Ast/walk.scm 311 */
					BgL_nodez00_bglt BgL_arg3186z00_9006;

					BgL_arg3186z00_9006 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nz00_7638)))->BgL_bodyz00);
					BgL_auxz00_12187 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_7639,
							((obj_t) BgL_arg3186z00_9006), BgL_arg0z00_7640));
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nz00_7638)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_12187), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_syncz00_bglt) BgL_nz00_7638));
		}

	}



/* &walk0!-sync2092 */
	BgL_nodez00_bglt BGl_z62walk0z12zd2sync2092za2zzast_walkz00(obj_t
		BgL_envz00_7641, obj_t BgL_nz00_7642, obj_t BgL_pz00_7643)
	{
		{	/* Ast/walk.scm 311 */
			{
				BgL_nodez00_bglt BgL_auxz00_12201;

				{	/* Ast/walk.scm 311 */
					BgL_nodez00_bglt BgL_arg3178z00_9008;

					BgL_arg3178z00_9008 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nz00_7642)))->BgL_mutexz00);
					BgL_auxz00_12201 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_7643, ((obj_t) BgL_arg3178z00_9008)));
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nz00_7642)))->BgL_mutexz00) =
					((BgL_nodez00_bglt) BgL_auxz00_12201), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_12212;

				{	/* Ast/walk.scm 311 */
					BgL_nodez00_bglt BgL_arg3180z00_9009;

					BgL_arg3180z00_9009 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nz00_7642)))->BgL_prelockz00);
					BgL_auxz00_12212 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_7643, ((obj_t) BgL_arg3180z00_9009)));
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nz00_7642)))->BgL_prelockz00) =
					((BgL_nodez00_bglt) BgL_auxz00_12212), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_12223;

				{	/* Ast/walk.scm 311 */
					BgL_nodez00_bglt BgL_arg3181z00_9010;

					BgL_arg3181z00_9010 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nz00_7642)))->BgL_bodyz00);
					BgL_auxz00_12223 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_7643, ((obj_t) BgL_arg3181z00_9010)));
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nz00_7642)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_12223), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_syncz00_bglt) BgL_nz00_7642));
		}

	}



/* &walk3*-sync2090 */
	obj_t BGl_z62walk3za2zd2sync2090z12zzast_walkz00(obj_t BgL_envz00_7644,
		obj_t BgL_nz00_7645, obj_t BgL_pz00_7646, obj_t BgL_arg0z00_7647,
		obj_t BgL_arg1z00_7648, obj_t BgL_arg2z00_7649)
	{
		{	/* Ast/walk.scm 311 */
			{	/* Ast/walk.scm 311 */
				obj_t BgL_res4040z00_9021;

				{	/* Ast/walk.scm 311 */
					obj_t BgL_arg3160z00_9012;
					obj_t BgL_arg3161z00_9013;
					obj_t BgL_arg3162z00_9014;

					{	/* Ast/walk.scm 311 */
						BgL_nodez00_bglt BgL_arg3174z00_9015;

						BgL_arg3174z00_9015 =
							(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nz00_7645)))->BgL_mutexz00);
						BgL_arg3160z00_9012 =
							BGL_PROCEDURE_CALL4(BgL_pz00_7646,
							((obj_t) BgL_arg3174z00_9015), BgL_arg0z00_7647, BgL_arg1z00_7648,
							BgL_arg2z00_7649);
					}
					{	/* Ast/walk.scm 311 */
						BgL_nodez00_bglt BgL_arg3175z00_9016;

						BgL_arg3175z00_9016 =
							(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nz00_7645)))->BgL_prelockz00);
						BgL_arg3161z00_9013 =
							BGL_PROCEDURE_CALL4(BgL_pz00_7646,
							((obj_t) BgL_arg3175z00_9016), BgL_arg0z00_7647, BgL_arg1z00_7648,
							BgL_arg2z00_7649);
					}
					{	/* Ast/walk.scm 311 */
						BgL_nodez00_bglt BgL_arg3177z00_9017;

						BgL_arg3177z00_9017 =
							(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nz00_7645)))->BgL_bodyz00);
						BgL_arg3162z00_9014 =
							BGL_PROCEDURE_CALL4(BgL_pz00_7646,
							((obj_t) BgL_arg3177z00_9017), BgL_arg0z00_7647, BgL_arg1z00_7648,
							BgL_arg2z00_7649);
					}
					{	/* Ast/walk.scm 311 */
						obj_t BgL_list3163z00_9018;

						{	/* Ast/walk.scm 311 */
							obj_t BgL_arg3171z00_9019;

							{	/* Ast/walk.scm 311 */
								obj_t BgL_arg3172z00_9020;

								BgL_arg3172z00_9020 =
									MAKE_YOUNG_PAIR(BgL_arg3162z00_9014, BNIL);
								BgL_arg3171z00_9019 =
									MAKE_YOUNG_PAIR(BgL_arg3161z00_9013, BgL_arg3172z00_9020);
							}
							BgL_list3163z00_9018 =
								MAKE_YOUNG_PAIR(BgL_arg3160z00_9012, BgL_arg3171z00_9019);
						}
						BgL_res4040z00_9021 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list3163z00_9018);
					}
				}
				return BgL_res4040z00_9021;
			}
		}

	}



/* &walk2*-sync2088 */
	obj_t BGl_z62walk2za2zd2sync2088z12zzast_walkz00(obj_t BgL_envz00_7650,
		obj_t BgL_nz00_7651, obj_t BgL_pz00_7652, obj_t BgL_arg0z00_7653,
		obj_t BgL_arg1z00_7654)
	{
		{	/* Ast/walk.scm 311 */
			{	/* Ast/walk.scm 311 */
				obj_t BgL_res4041z00_9032;

				{	/* Ast/walk.scm 311 */
					obj_t BgL_arg3145z00_9023;
					obj_t BgL_arg3146z00_9024;
					obj_t BgL_arg3149z00_9025;

					{	/* Ast/walk.scm 311 */
						BgL_nodez00_bglt BgL_arg3156z00_9026;

						BgL_arg3156z00_9026 =
							(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nz00_7651)))->BgL_mutexz00);
						BgL_arg3145z00_9023 =
							BGL_PROCEDURE_CALL3(BgL_pz00_7652,
							((obj_t) BgL_arg3156z00_9026), BgL_arg0z00_7653,
							BgL_arg1z00_7654);
					}
					{	/* Ast/walk.scm 311 */
						BgL_nodez00_bglt BgL_arg3157z00_9027;

						BgL_arg3157z00_9027 =
							(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nz00_7651)))->BgL_prelockz00);
						BgL_arg3146z00_9024 =
							BGL_PROCEDURE_CALL3(BgL_pz00_7652,
							((obj_t) BgL_arg3157z00_9027), BgL_arg0z00_7653,
							BgL_arg1z00_7654);
					}
					{	/* Ast/walk.scm 311 */
						BgL_nodez00_bglt BgL_arg3158z00_9028;

						BgL_arg3158z00_9028 =
							(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nz00_7651)))->BgL_bodyz00);
						BgL_arg3149z00_9025 =
							BGL_PROCEDURE_CALL3(BgL_pz00_7652,
							((obj_t) BgL_arg3158z00_9028), BgL_arg0z00_7653,
							BgL_arg1z00_7654);
					}
					{	/* Ast/walk.scm 311 */
						obj_t BgL_list3150z00_9029;

						{	/* Ast/walk.scm 311 */
							obj_t BgL_arg3154z00_9030;

							{	/* Ast/walk.scm 311 */
								obj_t BgL_arg3155z00_9031;

								BgL_arg3155z00_9031 =
									MAKE_YOUNG_PAIR(BgL_arg3149z00_9025, BNIL);
								BgL_arg3154z00_9030 =
									MAKE_YOUNG_PAIR(BgL_arg3146z00_9024, BgL_arg3155z00_9031);
							}
							BgL_list3150z00_9029 =
								MAKE_YOUNG_PAIR(BgL_arg3145z00_9023, BgL_arg3154z00_9030);
						}
						BgL_res4041z00_9032 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list3150z00_9029);
					}
				}
				return BgL_res4041z00_9032;
			}
		}

	}



/* &walk1*-sync2086 */
	obj_t BGl_z62walk1za2zd2sync2086z12zzast_walkz00(obj_t BgL_envz00_7655,
		obj_t BgL_nz00_7656, obj_t BgL_pz00_7657, obj_t BgL_arg0z00_7658)
	{
		{	/* Ast/walk.scm 311 */
			{	/* Ast/walk.scm 311 */
				obj_t BgL_res4042z00_9043;

				{	/* Ast/walk.scm 311 */
					obj_t BgL_arg3132z00_9034;
					obj_t BgL_arg3133z00_9035;
					obj_t BgL_arg3135z00_9036;

					{	/* Ast/walk.scm 311 */
						BgL_nodez00_bglt BgL_arg3140z00_9037;

						BgL_arg3140z00_9037 =
							(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nz00_7656)))->BgL_mutexz00);
						BgL_arg3132z00_9034 =
							BGL_PROCEDURE_CALL2(BgL_pz00_7657,
							((obj_t) BgL_arg3140z00_9037), BgL_arg0z00_7658);
					}
					{	/* Ast/walk.scm 311 */
						BgL_nodez00_bglt BgL_arg3141z00_9038;

						BgL_arg3141z00_9038 =
							(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nz00_7656)))->BgL_prelockz00);
						BgL_arg3133z00_9035 =
							BGL_PROCEDURE_CALL2(BgL_pz00_7657,
							((obj_t) BgL_arg3141z00_9038), BgL_arg0z00_7658);
					}
					{	/* Ast/walk.scm 311 */
						BgL_nodez00_bglt BgL_arg3144z00_9039;

						BgL_arg3144z00_9039 =
							(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nz00_7656)))->BgL_bodyz00);
						BgL_arg3135z00_9036 =
							BGL_PROCEDURE_CALL2(BgL_pz00_7657,
							((obj_t) BgL_arg3144z00_9039), BgL_arg0z00_7658);
					}
					{	/* Ast/walk.scm 311 */
						obj_t BgL_list3136z00_9040;

						{	/* Ast/walk.scm 311 */
							obj_t BgL_arg3137z00_9041;

							{	/* Ast/walk.scm 311 */
								obj_t BgL_arg3139z00_9042;

								BgL_arg3139z00_9042 =
									MAKE_YOUNG_PAIR(BgL_arg3135z00_9036, BNIL);
								BgL_arg3137z00_9041 =
									MAKE_YOUNG_PAIR(BgL_arg3133z00_9035, BgL_arg3139z00_9042);
							}
							BgL_list3136z00_9040 =
								MAKE_YOUNG_PAIR(BgL_arg3132z00_9034, BgL_arg3137z00_9041);
						}
						BgL_res4042z00_9043 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list3136z00_9040);
					}
				}
				return BgL_res4042z00_9043;
			}
		}

	}



/* &walk0*-sync2084 */
	obj_t BGl_z62walk0za2zd2sync2084z12zzast_walkz00(obj_t BgL_envz00_7659,
		obj_t BgL_nz00_7660, obj_t BgL_pz00_7661)
	{
		{	/* Ast/walk.scm 311 */
			{	/* Ast/walk.scm 311 */
				obj_t BgL_res4043z00_9054;

				{	/* Ast/walk.scm 311 */
					obj_t BgL_arg3121z00_9045;
					obj_t BgL_arg3122z00_9046;
					obj_t BgL_arg3124z00_9047;

					{	/* Ast/walk.scm 311 */
						BgL_nodez00_bglt BgL_arg3128z00_9048;

						BgL_arg3128z00_9048 =
							(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nz00_7660)))->BgL_mutexz00);
						BgL_arg3121z00_9045 =
							BGL_PROCEDURE_CALL1(BgL_pz00_7661, ((obj_t) BgL_arg3128z00_9048));
					}
					{	/* Ast/walk.scm 311 */
						BgL_nodez00_bglt BgL_arg3129z00_9049;

						BgL_arg3129z00_9049 =
							(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nz00_7660)))->BgL_prelockz00);
						BgL_arg3122z00_9046 =
							BGL_PROCEDURE_CALL1(BgL_pz00_7661, ((obj_t) BgL_arg3129z00_9049));
					}
					{	/* Ast/walk.scm 311 */
						BgL_nodez00_bglt BgL_arg3131z00_9050;

						BgL_arg3131z00_9050 =
							(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nz00_7660)))->BgL_bodyz00);
						BgL_arg3124z00_9047 =
							BGL_PROCEDURE_CALL1(BgL_pz00_7661, ((obj_t) BgL_arg3131z00_9050));
					}
					{	/* Ast/walk.scm 311 */
						obj_t BgL_list3125z00_9051;

						{	/* Ast/walk.scm 311 */
							obj_t BgL_arg3126z00_9052;

							{	/* Ast/walk.scm 311 */
								obj_t BgL_arg3127z00_9053;

								BgL_arg3127z00_9053 =
									MAKE_YOUNG_PAIR(BgL_arg3124z00_9047, BNIL);
								BgL_arg3126z00_9052 =
									MAKE_YOUNG_PAIR(BgL_arg3122z00_9046, BgL_arg3127z00_9053);
							}
							BgL_list3125z00_9051 =
								MAKE_YOUNG_PAIR(BgL_arg3121z00_9045, BgL_arg3126z00_9052);
						}
						BgL_res4043z00_9054 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list3125z00_9051);
					}
				}
				return BgL_res4043z00_9054;
			}
		}

	}



/* &walk3-sync2082 */
	obj_t BGl_z62walk3zd2sync2082zb0zzast_walkz00(obj_t BgL_envz00_7662,
		obj_t BgL_nz00_7663, obj_t BgL_pz00_7664, obj_t BgL_arg0z00_7665,
		obj_t BgL_arg1z00_7666, obj_t BgL_arg2z00_7667)
	{
		{	/* Ast/walk.scm 311 */
			{	/* Ast/walk.scm 311 */
				BgL_nodez00_bglt BgL_arg3115z00_9056;

				BgL_arg3115z00_9056 =
					(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nz00_7663)))->BgL_mutexz00);
				BGL_PROCEDURE_CALL4(BgL_pz00_7664,
					((obj_t) BgL_arg3115z00_9056), BgL_arg0z00_7665, BgL_arg1z00_7666,
					BgL_arg2z00_7667);
			}
			{	/* Ast/walk.scm 311 */
				BgL_nodez00_bglt BgL_arg3116z00_9057;

				BgL_arg3116z00_9057 =
					(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nz00_7663)))->BgL_prelockz00);
				BGL_PROCEDURE_CALL4(BgL_pz00_7664,
					((obj_t) BgL_arg3116z00_9057), BgL_arg0z00_7665, BgL_arg1z00_7666,
					BgL_arg2z00_7667);
			}
			{	/* Ast/walk.scm 311 */
				BgL_nodez00_bglt BgL_arg3118z00_9058;

				BgL_arg3118z00_9058 =
					(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nz00_7663)))->BgL_bodyz00);
				return
					BGL_PROCEDURE_CALL4(BgL_pz00_7664,
					((obj_t) BgL_arg3118z00_9058), BgL_arg0z00_7665, BgL_arg1z00_7666,
					BgL_arg2z00_7667);
			}
		}

	}



/* &walk2-sync2080 */
	obj_t BGl_z62walk2zd2sync2080zb0zzast_walkz00(obj_t BgL_envz00_7668,
		obj_t BgL_nz00_7669, obj_t BgL_pz00_7670, obj_t BgL_arg0z00_7671,
		obj_t BgL_arg1z00_7672)
	{
		{	/* Ast/walk.scm 311 */
			{	/* Ast/walk.scm 311 */
				BgL_nodez00_bglt BgL_arg3105z00_9060;

				BgL_arg3105z00_9060 =
					(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nz00_7669)))->BgL_mutexz00);
				BGL_PROCEDURE_CALL3(BgL_pz00_7670,
					((obj_t) BgL_arg3105z00_9060), BgL_arg0z00_7671, BgL_arg1z00_7672);
			}
			{	/* Ast/walk.scm 311 */
				BgL_nodez00_bglt BgL_arg3106z00_9061;

				BgL_arg3106z00_9061 =
					(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nz00_7669)))->BgL_prelockz00);
				BGL_PROCEDURE_CALL3(BgL_pz00_7670,
					((obj_t) BgL_arg3106z00_9061), BgL_arg0z00_7671, BgL_arg1z00_7672);
			}
			{	/* Ast/walk.scm 311 */
				BgL_nodez00_bglt BgL_arg3114z00_9062;

				BgL_arg3114z00_9062 =
					(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nz00_7669)))->BgL_bodyz00);
				return
					BGL_PROCEDURE_CALL3(BgL_pz00_7670,
					((obj_t) BgL_arg3114z00_9062), BgL_arg0z00_7671, BgL_arg1z00_7672);
			}
		}

	}



/* &walk1-sync2078 */
	obj_t BGl_z62walk1zd2sync2078zb0zzast_walkz00(obj_t BgL_envz00_7673,
		obj_t BgL_nz00_7674, obj_t BgL_pz00_7675, obj_t BgL_arg0z00_7676)
	{
		{	/* Ast/walk.scm 311 */
			{	/* Ast/walk.scm 311 */
				BgL_nodez00_bglt BgL_arg3094z00_9064;

				BgL_arg3094z00_9064 =
					(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nz00_7674)))->BgL_mutexz00);
				BGL_PROCEDURE_CALL2(BgL_pz00_7675,
					((obj_t) BgL_arg3094z00_9064), BgL_arg0z00_7676);
			}
			{	/* Ast/walk.scm 311 */
				BgL_nodez00_bglt BgL_arg3099z00_9065;

				BgL_arg3099z00_9065 =
					(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nz00_7674)))->BgL_prelockz00);
				BGL_PROCEDURE_CALL2(BgL_pz00_7675,
					((obj_t) BgL_arg3099z00_9065), BgL_arg0z00_7676);
			}
			{	/* Ast/walk.scm 311 */
				BgL_nodez00_bglt BgL_arg3100z00_9066;

				BgL_arg3100z00_9066 =
					(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nz00_7674)))->BgL_bodyz00);
				return
					BGL_PROCEDURE_CALL2(BgL_pz00_7675,
					((obj_t) BgL_arg3100z00_9066), BgL_arg0z00_7676);
			}
		}

	}



/* &walk0-sync2076 */
	obj_t BGl_z62walk0zd2sync2076zb0zzast_walkz00(obj_t BgL_envz00_7677,
		obj_t BgL_nz00_7678, obj_t BgL_pz00_7679)
	{
		{	/* Ast/walk.scm 311 */
			{	/* Ast/walk.scm 311 */
				BgL_nodez00_bglt BgL_arg3091z00_9068;

				BgL_arg3091z00_9068 =
					(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nz00_7678)))->BgL_mutexz00);
				BGL_PROCEDURE_CALL1(BgL_pz00_7679, ((obj_t) BgL_arg3091z00_9068));
			}
			{	/* Ast/walk.scm 311 */
				BgL_nodez00_bglt BgL_arg3092z00_9069;

				BgL_arg3092z00_9069 =
					(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nz00_7678)))->BgL_prelockz00);
				BGL_PROCEDURE_CALL1(BgL_pz00_7679, ((obj_t) BgL_arg3092z00_9069));
			}
			{	/* Ast/walk.scm 311 */
				BgL_nodez00_bglt BgL_arg3093z00_9070;

				BgL_arg3093z00_9070 =
					(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nz00_7678)))->BgL_bodyz00);
				return
					BGL_PROCEDURE_CALL1(BgL_pz00_7679, ((obj_t) BgL_arg3093z00_9070));
			}
		}

	}



/* &walk3!-box-set!2074 */
	BgL_nodez00_bglt BGl_z62walk3z12zd2boxzd2setz122074z62zzast_walkz00(obj_t
		BgL_envz00_7680, obj_t BgL_nz00_7681, obj_t BgL_pz00_7682,
		obj_t BgL_arg0z00_7683, obj_t BgL_arg1z00_7684, obj_t BgL_arg2z00_7685)
	{
		{	/* Ast/walk.scm 310 */
			{
				BgL_varz00_bglt BgL_auxz00_12456;

				{	/* Ast/walk.scm 310 */
					BgL_varz00_bglt BgL_arg3087z00_9072;

					BgL_arg3087z00_9072 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7681)))->BgL_varz00);
					BgL_auxz00_12456 =
						((BgL_varz00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_7682,
							((obj_t) BgL_arg3087z00_9072), BgL_arg0z00_7683, BgL_arg1z00_7684,
							BgL_arg2z00_7685));
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7681)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_12456), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_12470;

				{	/* Ast/walk.scm 310 */
					BgL_nodez00_bglt BgL_arg3088z00_9073;

					BgL_arg3088z00_9073 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7681)))->BgL_valuez00);
					BgL_auxz00_12470 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_7682,
							((obj_t) BgL_arg3088z00_9073), BgL_arg0z00_7683, BgL_arg1z00_7684,
							BgL_arg2z00_7685));
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7681)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_12470), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7681));
		}

	}



/* &walk2!-box-set!2072 */
	BgL_nodez00_bglt BGl_z62walk2z12zd2boxzd2setz122072z62zzast_walkz00(obj_t
		BgL_envz00_7686, obj_t BgL_nz00_7687, obj_t BgL_pz00_7688,
		obj_t BgL_arg0z00_7689, obj_t BgL_arg1z00_7690)
	{
		{	/* Ast/walk.scm 310 */
			{
				BgL_varz00_bglt BgL_auxz00_12486;

				{	/* Ast/walk.scm 310 */
					BgL_varz00_bglt BgL_arg3085z00_9075;

					BgL_arg3085z00_9075 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7687)))->BgL_varz00);
					BgL_auxz00_12486 =
						((BgL_varz00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_7688,
							((obj_t) BgL_arg3085z00_9075), BgL_arg0z00_7689,
							BgL_arg1z00_7690));
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7687)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_12486), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_12499;

				{	/* Ast/walk.scm 310 */
					BgL_nodez00_bglt BgL_arg3086z00_9076;

					BgL_arg3086z00_9076 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7687)))->BgL_valuez00);
					BgL_auxz00_12499 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_7688,
							((obj_t) BgL_arg3086z00_9076), BgL_arg0z00_7689,
							BgL_arg1z00_7690));
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7687)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_12499), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7687));
		}

	}



/* &walk1!-box-set!2070 */
	BgL_nodez00_bglt BGl_z62walk1z12zd2boxzd2setz122070z62zzast_walkz00(obj_t
		BgL_envz00_7691, obj_t BgL_nz00_7692, obj_t BgL_pz00_7693,
		obj_t BgL_arg0z00_7694)
	{
		{	/* Ast/walk.scm 310 */
			{
				BgL_varz00_bglt BgL_auxz00_12514;

				{	/* Ast/walk.scm 310 */
					BgL_varz00_bglt BgL_arg3083z00_9078;

					BgL_arg3083z00_9078 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7692)))->BgL_varz00);
					BgL_auxz00_12514 =
						((BgL_varz00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_7693,
							((obj_t) BgL_arg3083z00_9078), BgL_arg0z00_7694));
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7692)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_12514), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_12526;

				{	/* Ast/walk.scm 310 */
					BgL_nodez00_bglt BgL_arg3084z00_9079;

					BgL_arg3084z00_9079 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7692)))->BgL_valuez00);
					BgL_auxz00_12526 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_7693,
							((obj_t) BgL_arg3084z00_9079), BgL_arg0z00_7694));
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7692)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_12526), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7692));
		}

	}



/* &walk0!-box-set!2068 */
	BgL_nodez00_bglt BGl_z62walk0z12zd2boxzd2setz122068z62zzast_walkz00(obj_t
		BgL_envz00_7695, obj_t BgL_nz00_7696, obj_t BgL_pz00_7697)
	{
		{	/* Ast/walk.scm 310 */
			{
				BgL_varz00_bglt BgL_auxz00_12540;

				{	/* Ast/walk.scm 310 */
					BgL_varz00_bglt BgL_arg3081z00_9081;

					BgL_arg3081z00_9081 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7696)))->BgL_varz00);
					BgL_auxz00_12540 =
						((BgL_varz00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_7697, ((obj_t) BgL_arg3081z00_9081)));
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7696)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_12540), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_12551;

				{	/* Ast/walk.scm 310 */
					BgL_nodez00_bglt BgL_arg3082z00_9082;

					BgL_arg3082z00_9082 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7696)))->BgL_valuez00);
					BgL_auxz00_12551 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_7697, ((obj_t) BgL_arg3082z00_9082)));
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7696)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_12551), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7696));
		}

	}



/* &walk3*-box-set!2066 */
	obj_t BGl_z62walk3za2zd2boxzd2setz122066zd2zzast_walkz00(obj_t
		BgL_envz00_7698, obj_t BgL_nz00_7699, obj_t BgL_pz00_7700,
		obj_t BgL_arg0z00_7701, obj_t BgL_arg1z00_7702, obj_t BgL_arg2z00_7703)
	{
		{	/* Ast/walk.scm 310 */
			{	/* Ast/walk.scm 310 */
				obj_t BgL_res4044z00_9088;

				{	/* Ast/walk.scm 310 */
					obj_t BgL_arg3077z00_9084;
					obj_t BgL_arg3078z00_9085;

					{	/* Ast/walk.scm 310 */
						BgL_varz00_bglt BgL_arg3079z00_9086;

						BgL_arg3079z00_9086 =
							(((BgL_boxzd2setz12zc0_bglt) COBJECT(
									((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7699)))->BgL_varz00);
						BgL_arg3077z00_9084 =
							BGL_PROCEDURE_CALL4(BgL_pz00_7700,
							((obj_t) BgL_arg3079z00_9086), BgL_arg0z00_7701, BgL_arg1z00_7702,
							BgL_arg2z00_7703);
					}
					{	/* Ast/walk.scm 310 */
						BgL_nodez00_bglt BgL_arg3080z00_9087;

						BgL_arg3080z00_9087 =
							(((BgL_boxzd2setz12zc0_bglt) COBJECT(
									((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7699)))->BgL_valuez00);
						BgL_arg3078z00_9085 =
							BGL_PROCEDURE_CALL4(BgL_pz00_7700,
							((obj_t) BgL_arg3080z00_9087), BgL_arg0z00_7701, BgL_arg1z00_7702,
							BgL_arg2z00_7703);
					}
					BgL_res4044z00_9088 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg3077z00_9084,
						BgL_arg3078z00_9085);
				}
				return BgL_res4044z00_9088;
			}
		}

	}



/* &walk2*-box-set!2064 */
	obj_t BGl_z62walk2za2zd2boxzd2setz122064zd2zzast_walkz00(obj_t
		BgL_envz00_7704, obj_t BgL_nz00_7705, obj_t BgL_pz00_7706,
		obj_t BgL_arg0z00_7707, obj_t BgL_arg1z00_7708)
	{
		{	/* Ast/walk.scm 310 */
			{	/* Ast/walk.scm 310 */
				obj_t BgL_res4045z00_9094;

				{	/* Ast/walk.scm 310 */
					obj_t BgL_arg3071z00_9090;
					obj_t BgL_arg3072z00_9091;

					{	/* Ast/walk.scm 310 */
						BgL_varz00_bglt BgL_arg3073z00_9092;

						BgL_arg3073z00_9092 =
							(((BgL_boxzd2setz12zc0_bglt) COBJECT(
									((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7705)))->BgL_varz00);
						BgL_arg3071z00_9090 =
							BGL_PROCEDURE_CALL3(BgL_pz00_7706,
							((obj_t) BgL_arg3073z00_9092), BgL_arg0z00_7707,
							BgL_arg1z00_7708);
					}
					{	/* Ast/walk.scm 310 */
						BgL_nodez00_bglt BgL_arg3075z00_9093;

						BgL_arg3075z00_9093 =
							(((BgL_boxzd2setz12zc0_bglt) COBJECT(
									((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7705)))->BgL_valuez00);
						BgL_arg3072z00_9091 =
							BGL_PROCEDURE_CALL3(BgL_pz00_7706,
							((obj_t) BgL_arg3075z00_9093), BgL_arg0z00_7707,
							BgL_arg1z00_7708);
					}
					BgL_res4045z00_9094 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg3071z00_9090,
						BgL_arg3072z00_9091);
				}
				return BgL_res4045z00_9094;
			}
		}

	}



/* &walk1*-box-set!2062 */
	obj_t BGl_z62walk1za2zd2boxzd2setz122062zd2zzast_walkz00(obj_t
		BgL_envz00_7709, obj_t BgL_nz00_7710, obj_t BgL_pz00_7711,
		obj_t BgL_arg0z00_7712)
	{
		{	/* Ast/walk.scm 310 */
			{	/* Ast/walk.scm 310 */
				obj_t BgL_res4046z00_9100;

				{	/* Ast/walk.scm 310 */
					obj_t BgL_arg3062z00_9096;
					obj_t BgL_arg3066z00_9097;

					{	/* Ast/walk.scm 310 */
						BgL_varz00_bglt BgL_arg3067z00_9098;

						BgL_arg3067z00_9098 =
							(((BgL_boxzd2setz12zc0_bglt) COBJECT(
									((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7710)))->BgL_varz00);
						BgL_arg3062z00_9096 =
							BGL_PROCEDURE_CALL2(BgL_pz00_7711,
							((obj_t) BgL_arg3067z00_9098), BgL_arg0z00_7712);
					}
					{	/* Ast/walk.scm 310 */
						BgL_nodez00_bglt BgL_arg3070z00_9099;

						BgL_arg3070z00_9099 =
							(((BgL_boxzd2setz12zc0_bglt) COBJECT(
									((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7710)))->BgL_valuez00);
						BgL_arg3066z00_9097 =
							BGL_PROCEDURE_CALL2(BgL_pz00_7711,
							((obj_t) BgL_arg3070z00_9099), BgL_arg0z00_7712);
					}
					BgL_res4046z00_9100 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg3062z00_9096,
						BgL_arg3066z00_9097);
				}
				return BgL_res4046z00_9100;
			}
		}

	}



/* &walk0*-box-set!2060 */
	obj_t BGl_z62walk0za2zd2boxzd2setz122060zd2zzast_walkz00(obj_t
		BgL_envz00_7713, obj_t BgL_nz00_7714, obj_t BgL_pz00_7715)
	{
		{	/* Ast/walk.scm 310 */
			{	/* Ast/walk.scm 310 */
				obj_t BgL_res4047z00_9106;

				{	/* Ast/walk.scm 310 */
					obj_t BgL_arg3055z00_9102;
					obj_t BgL_arg3058z00_9103;

					{	/* Ast/walk.scm 310 */
						BgL_varz00_bglt BgL_arg3059z00_9104;

						BgL_arg3059z00_9104 =
							(((BgL_boxzd2setz12zc0_bglt) COBJECT(
									((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7714)))->BgL_varz00);
						BgL_arg3055z00_9102 =
							BGL_PROCEDURE_CALL1(BgL_pz00_7715, ((obj_t) BgL_arg3059z00_9104));
					}
					{	/* Ast/walk.scm 310 */
						BgL_nodez00_bglt BgL_arg3061z00_9105;

						BgL_arg3061z00_9105 =
							(((BgL_boxzd2setz12zc0_bglt) COBJECT(
									((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7714)))->BgL_valuez00);
						BgL_arg3058z00_9103 =
							BGL_PROCEDURE_CALL1(BgL_pz00_7715, ((obj_t) BgL_arg3061z00_9105));
					}
					BgL_res4047z00_9106 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg3055z00_9102,
						BgL_arg3058z00_9103);
				}
				return BgL_res4047z00_9106;
			}
		}

	}



/* &walk3-box-set!2058 */
	obj_t BGl_z62walk3zd2boxzd2setz122058z70zzast_walkz00(obj_t BgL_envz00_7716,
		obj_t BgL_nz00_7717, obj_t BgL_pz00_7718, obj_t BgL_arg0z00_7719,
		obj_t BgL_arg1z00_7720, obj_t BgL_arg2z00_7721)
	{
		{	/* Ast/walk.scm 310 */
			{	/* Ast/walk.scm 310 */
				BgL_varz00_bglt BgL_arg3050z00_9108;

				BgL_arg3050z00_9108 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7717)))->BgL_varz00);
				BGL_PROCEDURE_CALL4(BgL_pz00_7718,
					((obj_t) BgL_arg3050z00_9108), BgL_arg0z00_7719, BgL_arg1z00_7720,
					BgL_arg2z00_7721);
			}
			{	/* Ast/walk.scm 310 */
				BgL_nodez00_bglt BgL_arg3054z00_9109;

				BgL_arg3054z00_9109 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7717)))->BgL_valuez00);
				return
					BGL_PROCEDURE_CALL4(BgL_pz00_7718,
					((obj_t) BgL_arg3054z00_9109), BgL_arg0z00_7719, BgL_arg1z00_7720,
					BgL_arg2z00_7721);
			}
		}

	}



/* &walk2-box-set!2055 */
	obj_t BGl_z62walk2zd2boxzd2setz122055z70zzast_walkz00(obj_t BgL_envz00_7722,
		obj_t BgL_nz00_7723, obj_t BgL_pz00_7724, obj_t BgL_arg0z00_7725,
		obj_t BgL_arg1z00_7726)
	{
		{	/* Ast/walk.scm 310 */
			{	/* Ast/walk.scm 310 */
				BgL_varz00_bglt BgL_arg3044z00_9111;

				BgL_arg3044z00_9111 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7723)))->BgL_varz00);
				BGL_PROCEDURE_CALL3(BgL_pz00_7724,
					((obj_t) BgL_arg3044z00_9111), BgL_arg0z00_7725, BgL_arg1z00_7726);
			}
			{	/* Ast/walk.scm 310 */
				BgL_nodez00_bglt BgL_arg3049z00_9112;

				BgL_arg3049z00_9112 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7723)))->BgL_valuez00);
				return
					BGL_PROCEDURE_CALL3(BgL_pz00_7724,
					((obj_t) BgL_arg3049z00_9112), BgL_arg0z00_7725, BgL_arg1z00_7726);
			}
		}

	}



/* &walk1-box-set!2053 */
	obj_t BGl_z62walk1zd2boxzd2setz122053z70zzast_walkz00(obj_t BgL_envz00_7727,
		obj_t BgL_nz00_7728, obj_t BgL_pz00_7729, obj_t BgL_arg0z00_7730)
	{
		{	/* Ast/walk.scm 310 */
			{	/* Ast/walk.scm 310 */
				BgL_varz00_bglt BgL_arg3038z00_9114;

				BgL_arg3038z00_9114 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7728)))->BgL_varz00);
				BGL_PROCEDURE_CALL2(BgL_pz00_7729,
					((obj_t) BgL_arg3038z00_9114), BgL_arg0z00_7730);
			}
			{	/* Ast/walk.scm 310 */
				BgL_nodez00_bglt BgL_arg3043z00_9115;

				BgL_arg3043z00_9115 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7728)))->BgL_valuez00);
				return
					BGL_PROCEDURE_CALL2(BgL_pz00_7729,
					((obj_t) BgL_arg3043z00_9115), BgL_arg0z00_7730);
			}
		}

	}



/* &walk0-box-set!2051 */
	obj_t BGl_z62walk0zd2boxzd2setz122051z70zzast_walkz00(obj_t BgL_envz00_7731,
		obj_t BgL_nz00_7732, obj_t BgL_pz00_7733)
	{
		{	/* Ast/walk.scm 310 */
			{	/* Ast/walk.scm 310 */
				BgL_varz00_bglt BgL_arg3032z00_9117;

				BgL_arg3032z00_9117 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7732)))->BgL_varz00);
				BGL_PROCEDURE_CALL1(BgL_pz00_7733, ((obj_t) BgL_arg3032z00_9117));
			}
			{	/* Ast/walk.scm 310 */
				BgL_nodez00_bglt BgL_arg3037z00_9118;

				BgL_arg3037z00_9118 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nz00_7732)))->BgL_valuez00);
				return
					BGL_PROCEDURE_CALL1(BgL_pz00_7733, ((obj_t) BgL_arg3037z00_9118));
			}
		}

	}



/* &walk3!-box-ref2049 */
	BgL_nodez00_bglt BGl_z62walk3z12zd2boxzd2ref2049z70zzast_walkz00(obj_t
		BgL_envz00_7734, obj_t BgL_nz00_7735, obj_t BgL_pz00_7736,
		obj_t BgL_arg0z00_7737, obj_t BgL_arg1z00_7738, obj_t BgL_arg2z00_7739)
	{
		{	/* Ast/walk.scm 309 */
			{
				BgL_varz00_bglt BgL_auxz00_12704;

				{	/* Ast/walk.scm 309 */
					BgL_varz00_bglt BgL_arg3031z00_9120;

					BgL_arg3031z00_9120 =
						(((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nz00_7735)))->BgL_varz00);
					BgL_auxz00_12704 =
						((BgL_varz00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_7736,
							((obj_t) BgL_arg3031z00_9120), BgL_arg0z00_7737, BgL_arg1z00_7738,
							BgL_arg2z00_7739));
				}
				((((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nz00_7735)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_12704), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nz00_7735));
		}

	}



/* &walk2!-box-ref2047 */
	BgL_nodez00_bglt BGl_z62walk2z12zd2boxzd2ref2047z70zzast_walkz00(obj_t
		BgL_envz00_7740, obj_t BgL_nz00_7741, obj_t BgL_pz00_7742,
		obj_t BgL_arg0z00_7743, obj_t BgL_arg1z00_7744)
	{
		{	/* Ast/walk.scm 309 */
			{
				BgL_varz00_bglt BgL_auxz00_12720;

				{	/* Ast/walk.scm 309 */
					BgL_varz00_bglt BgL_arg3030z00_9122;

					BgL_arg3030z00_9122 =
						(((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nz00_7741)))->BgL_varz00);
					BgL_auxz00_12720 =
						((BgL_varz00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_7742,
							((obj_t) BgL_arg3030z00_9122), BgL_arg0z00_7743,
							BgL_arg1z00_7744));
				}
				((((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nz00_7741)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_12720), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nz00_7741));
		}

	}



/* &walk1!-box-ref2045 */
	BgL_nodez00_bglt BGl_z62walk1z12zd2boxzd2ref2045z70zzast_walkz00(obj_t
		BgL_envz00_7745, obj_t BgL_nz00_7746, obj_t BgL_pz00_7747,
		obj_t BgL_arg0z00_7748)
	{
		{	/* Ast/walk.scm 309 */
			{
				BgL_varz00_bglt BgL_auxz00_12735;

				{	/* Ast/walk.scm 309 */
					BgL_varz00_bglt BgL_arg3029z00_9124;

					BgL_arg3029z00_9124 =
						(((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nz00_7746)))->BgL_varz00);
					BgL_auxz00_12735 =
						((BgL_varz00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_7747,
							((obj_t) BgL_arg3029z00_9124), BgL_arg0z00_7748));
				}
				((((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nz00_7746)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_12735), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nz00_7746));
		}

	}



/* &walk0!-box-ref2043 */
	BgL_nodez00_bglt BGl_z62walk0z12zd2boxzd2ref2043z70zzast_walkz00(obj_t
		BgL_envz00_7749, obj_t BgL_nz00_7750, obj_t BgL_pz00_7751)
	{
		{	/* Ast/walk.scm 309 */
			{
				BgL_varz00_bglt BgL_auxz00_12749;

				{	/* Ast/walk.scm 309 */
					BgL_varz00_bglt BgL_arg3027z00_9126;

					BgL_arg3027z00_9126 =
						(((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nz00_7750)))->BgL_varz00);
					BgL_auxz00_12749 =
						((BgL_varz00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_7751, ((obj_t) BgL_arg3027z00_9126)));
				}
				((((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nz00_7750)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_12749), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nz00_7750));
		}

	}



/* &walk3*-box-ref2040 */
	obj_t BGl_z62walk3za2zd2boxzd2ref2040zc0zzast_walkz00(obj_t BgL_envz00_7752,
		obj_t BgL_nz00_7753, obj_t BgL_pz00_7754, obj_t BgL_arg0z00_7755,
		obj_t BgL_arg1z00_7756, obj_t BgL_arg2z00_7757)
	{
		{	/* Ast/walk.scm 309 */
			{	/* Ast/walk.scm 309 */
				obj_t BgL_res4048z00_9131;

				{	/* Ast/walk.scm 309 */
					obj_t BgL_arg3024z00_9128;

					{	/* Ast/walk.scm 309 */
						BgL_varz00_bglt BgL_arg3026z00_9129;

						BgL_arg3026z00_9129 =
							(((BgL_boxzd2refzd2_bglt) COBJECT(
									((BgL_boxzd2refzd2_bglt) BgL_nz00_7753)))->BgL_varz00);
						BgL_arg3024z00_9128 =
							BGL_PROCEDURE_CALL4(BgL_pz00_7754,
							((obj_t) BgL_arg3026z00_9129), BgL_arg0z00_7755, BgL_arg1z00_7756,
							BgL_arg2z00_7757);
					}
					{	/* Ast/walk.scm 309 */
						obj_t BgL_list3025z00_9130;

						BgL_list3025z00_9130 = MAKE_YOUNG_PAIR(BgL_arg3024z00_9128, BNIL);
						BgL_res4048z00_9131 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list3025z00_9130);
					}
				}
				return BgL_res4048z00_9131;
			}
		}

	}



/* &walk2*-box-ref2038 */
	obj_t BGl_z62walk2za2zd2boxzd2ref2038zc0zzast_walkz00(obj_t BgL_envz00_7758,
		obj_t BgL_nz00_7759, obj_t BgL_pz00_7760, obj_t BgL_arg0z00_7761,
		obj_t BgL_arg1z00_7762)
	{
		{	/* Ast/walk.scm 309 */
			{	/* Ast/walk.scm 309 */
				obj_t BgL_res4049z00_9136;

				{	/* Ast/walk.scm 309 */
					obj_t BgL_arg3021z00_9133;

					{	/* Ast/walk.scm 309 */
						BgL_varz00_bglt BgL_arg3023z00_9134;

						BgL_arg3023z00_9134 =
							(((BgL_boxzd2refzd2_bglt) COBJECT(
									((BgL_boxzd2refzd2_bglt) BgL_nz00_7759)))->BgL_varz00);
						BgL_arg3021z00_9133 =
							BGL_PROCEDURE_CALL3(BgL_pz00_7760,
							((obj_t) BgL_arg3023z00_9134), BgL_arg0z00_7761,
							BgL_arg1z00_7762);
					}
					{	/* Ast/walk.scm 309 */
						obj_t BgL_list3022z00_9135;

						BgL_list3022z00_9135 = MAKE_YOUNG_PAIR(BgL_arg3021z00_9133, BNIL);
						BgL_res4049z00_9136 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list3022z00_9135);
					}
				}
				return BgL_res4049z00_9136;
			}
		}

	}



/* &walk1*-box-ref2036 */
	obj_t BGl_z62walk1za2zd2boxzd2ref2036zc0zzast_walkz00(obj_t BgL_envz00_7763,
		obj_t BgL_nz00_7764, obj_t BgL_pz00_7765, obj_t BgL_arg0z00_7766)
	{
		{	/* Ast/walk.scm 309 */
			{	/* Ast/walk.scm 309 */
				obj_t BgL_res4050z00_9141;

				{	/* Ast/walk.scm 309 */
					obj_t BgL_arg3018z00_9138;

					{	/* Ast/walk.scm 309 */
						BgL_varz00_bglt BgL_arg3020z00_9139;

						BgL_arg3020z00_9139 =
							(((BgL_boxzd2refzd2_bglt) COBJECT(
									((BgL_boxzd2refzd2_bglt) BgL_nz00_7764)))->BgL_varz00);
						BgL_arg3018z00_9138 =
							BGL_PROCEDURE_CALL2(BgL_pz00_7765,
							((obj_t) BgL_arg3020z00_9139), BgL_arg0z00_7766);
					}
					{	/* Ast/walk.scm 309 */
						obj_t BgL_list3019z00_9140;

						BgL_list3019z00_9140 = MAKE_YOUNG_PAIR(BgL_arg3018z00_9138, BNIL);
						BgL_res4050z00_9141 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list3019z00_9140);
					}
				}
				return BgL_res4050z00_9141;
			}
		}

	}



/* &walk0*-box-ref2034 */
	obj_t BGl_z62walk0za2zd2boxzd2ref2034zc0zzast_walkz00(obj_t BgL_envz00_7767,
		obj_t BgL_nz00_7768, obj_t BgL_pz00_7769)
	{
		{	/* Ast/walk.scm 309 */
			{	/* Ast/walk.scm 309 */
				obj_t BgL_res4051z00_9146;

				{	/* Ast/walk.scm 309 */
					obj_t BgL_arg3015z00_9143;

					{	/* Ast/walk.scm 309 */
						BgL_varz00_bglt BgL_arg3017z00_9144;

						BgL_arg3017z00_9144 =
							(((BgL_boxzd2refzd2_bglt) COBJECT(
									((BgL_boxzd2refzd2_bglt) BgL_nz00_7768)))->BgL_varz00);
						BgL_arg3015z00_9143 =
							BGL_PROCEDURE_CALL1(BgL_pz00_7769, ((obj_t) BgL_arg3017z00_9144));
					}
					{	/* Ast/walk.scm 309 */
						obj_t BgL_list3016z00_9145;

						BgL_list3016z00_9145 = MAKE_YOUNG_PAIR(BgL_arg3015z00_9143, BNIL);
						BgL_res4051z00_9146 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list3016z00_9145);
					}
				}
				return BgL_res4051z00_9146;
			}
		}

	}



/* &walk3-box-ref2032 */
	obj_t BGl_z62walk3zd2boxzd2ref2032z62zzast_walkz00(obj_t BgL_envz00_7770,
		obj_t BgL_nz00_7771, obj_t BgL_pz00_7772, obj_t BgL_arg0z00_7773,
		obj_t BgL_arg1z00_7774, obj_t BgL_arg2z00_7775)
	{
		{	/* Ast/walk.scm 309 */
			{	/* Ast/walk.scm 309 */
				BgL_varz00_bglt BgL_arg3014z00_9148;

				BgL_arg3014z00_9148 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_nz00_7771)))->BgL_varz00);
				return
					BGL_PROCEDURE_CALL4(BgL_pz00_7772,
					((obj_t) BgL_arg3014z00_9148), BgL_arg0z00_7773, BgL_arg1z00_7774,
					BgL_arg2z00_7775);
			}
		}

	}



/* &walk2-box-ref2030 */
	obj_t BGl_z62walk2zd2boxzd2ref2030z62zzast_walkz00(obj_t BgL_envz00_7776,
		obj_t BgL_nz00_7777, obj_t BgL_pz00_7778, obj_t BgL_arg0z00_7779,
		obj_t BgL_arg1z00_7780)
	{
		{	/* Ast/walk.scm 309 */
			{	/* Ast/walk.scm 309 */
				BgL_varz00_bglt BgL_arg3013z00_9150;

				BgL_arg3013z00_9150 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_nz00_7777)))->BgL_varz00);
				return
					BGL_PROCEDURE_CALL3(BgL_pz00_7778,
					((obj_t) BgL_arg3013z00_9150), BgL_arg0z00_7779, BgL_arg1z00_7780);
			}
		}

	}



/* &walk1-box-ref2028 */
	obj_t BGl_z62walk1zd2boxzd2ref2028z62zzast_walkz00(obj_t BgL_envz00_7781,
		obj_t BgL_nz00_7782, obj_t BgL_pz00_7783, obj_t BgL_arg0z00_7784)
	{
		{	/* Ast/walk.scm 309 */
			{	/* Ast/walk.scm 309 */
				BgL_varz00_bglt BgL_arg3012z00_9152;

				BgL_arg3012z00_9152 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_nz00_7782)))->BgL_varz00);
				return
					BGL_PROCEDURE_CALL2(BgL_pz00_7783,
					((obj_t) BgL_arg3012z00_9152), BgL_arg0z00_7784);
			}
		}

	}



/* &walk0-box-ref2026 */
	obj_t BGl_z62walk0zd2boxzd2ref2026z62zzast_walkz00(obj_t BgL_envz00_7785,
		obj_t BgL_nz00_7786, obj_t BgL_pz00_7787)
	{
		{	/* Ast/walk.scm 309 */
			{	/* Ast/walk.scm 309 */
				BgL_varz00_bglt BgL_arg3011z00_9154;

				BgL_arg3011z00_9154 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_nz00_7786)))->BgL_varz00);
				return
					BGL_PROCEDURE_CALL1(BgL_pz00_7787, ((obj_t) BgL_arg3011z00_9154));
			}
		}

	}



/* &walk3!-make-box2024 */
	BgL_nodez00_bglt BGl_z62walk3z12zd2makezd2box2024z70zzast_walkz00(obj_t
		BgL_envz00_7788, obj_t BgL_nz00_7789, obj_t BgL_pz00_7790,
		obj_t BgL_arg0z00_7791, obj_t BgL_arg1z00_7792, obj_t BgL_arg2z00_7793)
	{
		{	/* Ast/walk.scm 308 */
			{
				BgL_nodez00_bglt BgL_auxz00_12838;

				{	/* Ast/walk.scm 308 */
					BgL_nodez00_bglt BgL_arg3010z00_9156;

					BgL_arg3010z00_9156 =
						(((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nz00_7789)))->BgL_valuez00);
					BgL_auxz00_12838 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_7790,
							((obj_t) BgL_arg3010z00_9156), BgL_arg0z00_7791, BgL_arg1z00_7792,
							BgL_arg2z00_7793));
				}
				((((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nz00_7789)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_12838), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nz00_7789));
		}

	}



/* &walk2!-make-box2022 */
	BgL_nodez00_bglt BGl_z62walk2z12zd2makezd2box2022z70zzast_walkz00(obj_t
		BgL_envz00_7794, obj_t BgL_nz00_7795, obj_t BgL_pz00_7796,
		obj_t BgL_arg0z00_7797, obj_t BgL_arg1z00_7798)
	{
		{	/* Ast/walk.scm 308 */
			{
				BgL_nodez00_bglt BgL_auxz00_12854;

				{	/* Ast/walk.scm 308 */
					BgL_nodez00_bglt BgL_arg3009z00_9158;

					BgL_arg3009z00_9158 =
						(((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nz00_7795)))->BgL_valuez00);
					BgL_auxz00_12854 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_7796,
							((obj_t) BgL_arg3009z00_9158), BgL_arg0z00_7797,
							BgL_arg1z00_7798));
				}
				((((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nz00_7795)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_12854), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nz00_7795));
		}

	}



/* &walk1!-make-box2020 */
	BgL_nodez00_bglt BGl_z62walk1z12zd2makezd2box2020z70zzast_walkz00(obj_t
		BgL_envz00_7799, obj_t BgL_nz00_7800, obj_t BgL_pz00_7801,
		obj_t BgL_arg0z00_7802)
	{
		{	/* Ast/walk.scm 308 */
			{
				BgL_nodez00_bglt BgL_auxz00_12869;

				{	/* Ast/walk.scm 308 */
					BgL_nodez00_bglt BgL_arg3008z00_9160;

					BgL_arg3008z00_9160 =
						(((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nz00_7800)))->BgL_valuez00);
					BgL_auxz00_12869 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_7801,
							((obj_t) BgL_arg3008z00_9160), BgL_arg0z00_7802));
				}
				((((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nz00_7800)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_12869), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nz00_7800));
		}

	}



/* &walk0!-make-box2018 */
	BgL_nodez00_bglt BGl_z62walk0z12zd2makezd2box2018z70zzast_walkz00(obj_t
		BgL_envz00_7803, obj_t BgL_nz00_7804, obj_t BgL_pz00_7805)
	{
		{	/* Ast/walk.scm 308 */
			{
				BgL_nodez00_bglt BgL_auxz00_12883;

				{	/* Ast/walk.scm 308 */
					BgL_nodez00_bglt BgL_arg3003z00_9162;

					BgL_arg3003z00_9162 =
						(((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nz00_7804)))->BgL_valuez00);
					BgL_auxz00_12883 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_7805, ((obj_t) BgL_arg3003z00_9162)));
				}
				((((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nz00_7804)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_12883), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nz00_7804));
		}

	}



/* &walk3*-make-box2016 */
	obj_t BGl_z62walk3za2zd2makezd2box2016zc0zzast_walkz00(obj_t BgL_envz00_7806,
		obj_t BgL_nz00_7807, obj_t BgL_pz00_7808, obj_t BgL_arg0z00_7809,
		obj_t BgL_arg1z00_7810, obj_t BgL_arg2z00_7811)
	{
		{	/* Ast/walk.scm 308 */
			{	/* Ast/walk.scm 308 */
				obj_t BgL_res4052z00_9167;

				{	/* Ast/walk.scm 308 */
					obj_t BgL_arg3000z00_9164;

					{	/* Ast/walk.scm 308 */
						BgL_nodez00_bglt BgL_arg3002z00_9165;

						BgL_arg3002z00_9165 =
							(((BgL_makezd2boxzd2_bglt) COBJECT(
									((BgL_makezd2boxzd2_bglt) BgL_nz00_7807)))->BgL_valuez00);
						BgL_arg3000z00_9164 =
							BGL_PROCEDURE_CALL4(BgL_pz00_7808,
							((obj_t) BgL_arg3002z00_9165), BgL_arg0z00_7809, BgL_arg1z00_7810,
							BgL_arg2z00_7811);
					}
					{	/* Ast/walk.scm 308 */
						obj_t BgL_list3001z00_9166;

						BgL_list3001z00_9166 = MAKE_YOUNG_PAIR(BgL_arg3000z00_9164, BNIL);
						BgL_res4052z00_9167 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list3001z00_9166);
					}
				}
				return BgL_res4052z00_9167;
			}
		}

	}



/* &walk2*-make-box2014 */
	obj_t BGl_z62walk2za2zd2makezd2box2014zc0zzast_walkz00(obj_t BgL_envz00_7812,
		obj_t BgL_nz00_7813, obj_t BgL_pz00_7814, obj_t BgL_arg0z00_7815,
		obj_t BgL_arg1z00_7816)
	{
		{	/* Ast/walk.scm 308 */
			{	/* Ast/walk.scm 308 */
				obj_t BgL_res4053z00_9172;

				{	/* Ast/walk.scm 308 */
					obj_t BgL_arg2995z00_9169;

					{	/* Ast/walk.scm 308 */
						BgL_nodez00_bglt BgL_arg2999z00_9170;

						BgL_arg2999z00_9170 =
							(((BgL_makezd2boxzd2_bglt) COBJECT(
									((BgL_makezd2boxzd2_bglt) BgL_nz00_7813)))->BgL_valuez00);
						BgL_arg2995z00_9169 =
							BGL_PROCEDURE_CALL3(BgL_pz00_7814,
							((obj_t) BgL_arg2999z00_9170), BgL_arg0z00_7815,
							BgL_arg1z00_7816);
					}
					{	/* Ast/walk.scm 308 */
						obj_t BgL_list2996z00_9171;

						BgL_list2996z00_9171 = MAKE_YOUNG_PAIR(BgL_arg2995z00_9169, BNIL);
						BgL_res4053z00_9172 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2996z00_9171);
					}
				}
				return BgL_res4053z00_9172;
			}
		}

	}



/* &walk1*-make-box2012 */
	obj_t BGl_z62walk1za2zd2makezd2box2012zc0zzast_walkz00(obj_t BgL_envz00_7817,
		obj_t BgL_nz00_7818, obj_t BgL_pz00_7819, obj_t BgL_arg0z00_7820)
	{
		{	/* Ast/walk.scm 308 */
			{	/* Ast/walk.scm 308 */
				obj_t BgL_res4054z00_9177;

				{	/* Ast/walk.scm 308 */
					obj_t BgL_arg2992z00_9174;

					{	/* Ast/walk.scm 308 */
						BgL_nodez00_bglt BgL_arg2994z00_9175;

						BgL_arg2994z00_9175 =
							(((BgL_makezd2boxzd2_bglt) COBJECT(
									((BgL_makezd2boxzd2_bglt) BgL_nz00_7818)))->BgL_valuez00);
						BgL_arg2992z00_9174 =
							BGL_PROCEDURE_CALL2(BgL_pz00_7819,
							((obj_t) BgL_arg2994z00_9175), BgL_arg0z00_7820);
					}
					{	/* Ast/walk.scm 308 */
						obj_t BgL_list2993z00_9176;

						BgL_list2993z00_9176 = MAKE_YOUNG_PAIR(BgL_arg2992z00_9174, BNIL);
						BgL_res4054z00_9177 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2993z00_9176);
					}
				}
				return BgL_res4054z00_9177;
			}
		}

	}



/* &walk0*-make-box2009 */
	obj_t BGl_z62walk0za2zd2makezd2box2009zc0zzast_walkz00(obj_t BgL_envz00_7821,
		obj_t BgL_nz00_7822, obj_t BgL_pz00_7823)
	{
		{	/* Ast/walk.scm 308 */
			{	/* Ast/walk.scm 308 */
				obj_t BgL_res4055z00_9182;

				{	/* Ast/walk.scm 308 */
					obj_t BgL_arg2989z00_9179;

					{	/* Ast/walk.scm 308 */
						BgL_nodez00_bglt BgL_arg2991z00_9180;

						BgL_arg2991z00_9180 =
							(((BgL_makezd2boxzd2_bglt) COBJECT(
									((BgL_makezd2boxzd2_bglt) BgL_nz00_7822)))->BgL_valuez00);
						BgL_arg2989z00_9179 =
							BGL_PROCEDURE_CALL1(BgL_pz00_7823, ((obj_t) BgL_arg2991z00_9180));
					}
					{	/* Ast/walk.scm 308 */
						obj_t BgL_list2990z00_9181;

						BgL_list2990z00_9181 = MAKE_YOUNG_PAIR(BgL_arg2989z00_9179, BNIL);
						BgL_res4055z00_9182 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2990z00_9181);
					}
				}
				return BgL_res4055z00_9182;
			}
		}

	}



/* &walk3-make-box2007 */
	obj_t BGl_z62walk3zd2makezd2box2007z62zzast_walkz00(obj_t BgL_envz00_7824,
		obj_t BgL_nz00_7825, obj_t BgL_pz00_7826, obj_t BgL_arg0z00_7827,
		obj_t BgL_arg1z00_7828, obj_t BgL_arg2z00_7829)
	{
		{	/* Ast/walk.scm 308 */
			{	/* Ast/walk.scm 308 */
				BgL_nodez00_bglt BgL_arg2988z00_9184;

				BgL_arg2988z00_9184 =
					(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nz00_7825)))->BgL_valuez00);
				return
					BGL_PROCEDURE_CALL4(BgL_pz00_7826,
					((obj_t) BgL_arg2988z00_9184), BgL_arg0z00_7827, BgL_arg1z00_7828,
					BgL_arg2z00_7829);
			}
		}

	}



/* &walk2-make-box2005 */
	obj_t BGl_z62walk2zd2makezd2box2005z62zzast_walkz00(obj_t BgL_envz00_7830,
		obj_t BgL_nz00_7831, obj_t BgL_pz00_7832, obj_t BgL_arg0z00_7833,
		obj_t BgL_arg1z00_7834)
	{
		{	/* Ast/walk.scm 308 */
			{	/* Ast/walk.scm 308 */
				BgL_nodez00_bglt BgL_arg2987z00_9186;

				BgL_arg2987z00_9186 =
					(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nz00_7831)))->BgL_valuez00);
				return
					BGL_PROCEDURE_CALL3(BgL_pz00_7832,
					((obj_t) BgL_arg2987z00_9186), BgL_arg0z00_7833, BgL_arg1z00_7834);
			}
		}

	}



/* &walk1-make-box2003 */
	obj_t BGl_z62walk1zd2makezd2box2003z62zzast_walkz00(obj_t BgL_envz00_7835,
		obj_t BgL_nz00_7836, obj_t BgL_pz00_7837, obj_t BgL_arg0z00_7838)
	{
		{	/* Ast/walk.scm 308 */
			{	/* Ast/walk.scm 308 */
				BgL_nodez00_bglt BgL_arg2986z00_9188;

				BgL_arg2986z00_9188 =
					(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nz00_7836)))->BgL_valuez00);
				return
					BGL_PROCEDURE_CALL2(BgL_pz00_7837,
					((obj_t) BgL_arg2986z00_9188), BgL_arg0z00_7838);
			}
		}

	}



/* &walk0-make-box2001 */
	obj_t BGl_z62walk0zd2makezd2box2001z62zzast_walkz00(obj_t BgL_envz00_7839,
		obj_t BgL_nz00_7840, obj_t BgL_pz00_7841)
	{
		{	/* Ast/walk.scm 308 */
			{	/* Ast/walk.scm 308 */
				BgL_nodez00_bglt BgL_arg2985z00_9190;

				BgL_arg2985z00_9190 =
					(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nz00_7840)))->BgL_valuez00);
				return
					BGL_PROCEDURE_CALL1(BgL_pz00_7841, ((obj_t) BgL_arg2985z00_9190));
			}
		}

	}



/* &walk3!-return1999 */
	BgL_nodez00_bglt BGl_z62walk3z12zd2return1999za2zzast_walkz00(obj_t
		BgL_envz00_7842, obj_t BgL_nz00_7843, obj_t BgL_pz00_7844,
		obj_t BgL_arg0z00_7845, obj_t BgL_arg1z00_7846, obj_t BgL_arg2z00_7847)
	{
		{	/* Ast/walk.scm 307 */
			{
				BgL_nodez00_bglt BgL_auxz00_12972;

				{	/* Ast/walk.scm 307 */
					BgL_nodez00_bglt BgL_arg2981z00_9192;

					BgL_arg2981z00_9192 =
						(((BgL_returnz00_bglt) COBJECT(
								((BgL_returnz00_bglt) BgL_nz00_7843)))->BgL_valuez00);
					BgL_auxz00_12972 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_7844,
							((obj_t) BgL_arg2981z00_9192), BgL_arg0z00_7845, BgL_arg1z00_7846,
							BgL_arg2z00_7847));
				}
				((((BgL_returnz00_bglt) COBJECT(
								((BgL_returnz00_bglt) BgL_nz00_7843)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_12972), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_returnz00_bglt) BgL_nz00_7843));
		}

	}



/* &walk2!-return1997 */
	BgL_nodez00_bglt BGl_z62walk2z12zd2return1997za2zzast_walkz00(obj_t
		BgL_envz00_7848, obj_t BgL_nz00_7849, obj_t BgL_pz00_7850,
		obj_t BgL_arg0z00_7851, obj_t BgL_arg1z00_7852)
	{
		{	/* Ast/walk.scm 307 */
			{
				BgL_nodez00_bglt BgL_auxz00_12988;

				{	/* Ast/walk.scm 307 */
					BgL_nodez00_bglt BgL_arg2980z00_9194;

					BgL_arg2980z00_9194 =
						(((BgL_returnz00_bglt) COBJECT(
								((BgL_returnz00_bglt) BgL_nz00_7849)))->BgL_valuez00);
					BgL_auxz00_12988 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_7850,
							((obj_t) BgL_arg2980z00_9194), BgL_arg0z00_7851,
							BgL_arg1z00_7852));
				}
				((((BgL_returnz00_bglt) COBJECT(
								((BgL_returnz00_bglt) BgL_nz00_7849)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_12988), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_returnz00_bglt) BgL_nz00_7849));
		}

	}



/* &walk1!-return1995 */
	BgL_nodez00_bglt BGl_z62walk1z12zd2return1995za2zzast_walkz00(obj_t
		BgL_envz00_7853, obj_t BgL_nz00_7854, obj_t BgL_pz00_7855,
		obj_t BgL_arg0z00_7856)
	{
		{	/* Ast/walk.scm 307 */
			{
				BgL_nodez00_bglt BgL_auxz00_13003;

				{	/* Ast/walk.scm 307 */
					BgL_nodez00_bglt BgL_arg2979z00_9196;

					BgL_arg2979z00_9196 =
						(((BgL_returnz00_bglt) COBJECT(
								((BgL_returnz00_bglt) BgL_nz00_7854)))->BgL_valuez00);
					BgL_auxz00_13003 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_7855,
							((obj_t) BgL_arg2979z00_9196), BgL_arg0z00_7856));
				}
				((((BgL_returnz00_bglt) COBJECT(
								((BgL_returnz00_bglt) BgL_nz00_7854)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_13003), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_returnz00_bglt) BgL_nz00_7854));
		}

	}



/* &walk0!-return1993 */
	BgL_nodez00_bglt BGl_z62walk0z12zd2return1993za2zzast_walkz00(obj_t
		BgL_envz00_7857, obj_t BgL_nz00_7858, obj_t BgL_pz00_7859)
	{
		{	/* Ast/walk.scm 307 */
			{
				BgL_nodez00_bglt BgL_auxz00_13017;

				{	/* Ast/walk.scm 307 */
					BgL_nodez00_bglt BgL_arg2978z00_9198;

					BgL_arg2978z00_9198 =
						(((BgL_returnz00_bglt) COBJECT(
								((BgL_returnz00_bglt) BgL_nz00_7858)))->BgL_valuez00);
					BgL_auxz00_13017 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_7859, ((obj_t) BgL_arg2978z00_9198)));
				}
				((((BgL_returnz00_bglt) COBJECT(
								((BgL_returnz00_bglt) BgL_nz00_7858)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_13017), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_returnz00_bglt) BgL_nz00_7858));
		}

	}



/* &walk3*-return1991 */
	obj_t BGl_z62walk3za2zd2return1991z12zzast_walkz00(obj_t BgL_envz00_7860,
		obj_t BgL_nz00_7861, obj_t BgL_pz00_7862, obj_t BgL_arg0z00_7863,
		obj_t BgL_arg1z00_7864, obj_t BgL_arg2z00_7865)
	{
		{	/* Ast/walk.scm 307 */
			{	/* Ast/walk.scm 307 */
				obj_t BgL_res4056z00_9203;

				{	/* Ast/walk.scm 307 */
					obj_t BgL_arg2975z00_9200;

					{	/* Ast/walk.scm 307 */
						BgL_nodez00_bglt BgL_arg2977z00_9201;

						BgL_arg2977z00_9201 =
							(((BgL_returnz00_bglt) COBJECT(
									((BgL_returnz00_bglt) BgL_nz00_7861)))->BgL_valuez00);
						BgL_arg2975z00_9200 =
							BGL_PROCEDURE_CALL4(BgL_pz00_7862,
							((obj_t) BgL_arg2977z00_9201), BgL_arg0z00_7863, BgL_arg1z00_7864,
							BgL_arg2z00_7865);
					}
					{	/* Ast/walk.scm 307 */
						obj_t BgL_list2976z00_9202;

						BgL_list2976z00_9202 = MAKE_YOUNG_PAIR(BgL_arg2975z00_9200, BNIL);
						BgL_res4056z00_9203 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2976z00_9202);
					}
				}
				return BgL_res4056z00_9203;
			}
		}

	}



/* &walk2*-return1989 */
	obj_t BGl_z62walk2za2zd2return1989z12zzast_walkz00(obj_t BgL_envz00_7866,
		obj_t BgL_nz00_7867, obj_t BgL_pz00_7868, obj_t BgL_arg0z00_7869,
		obj_t BgL_arg1z00_7870)
	{
		{	/* Ast/walk.scm 307 */
			{	/* Ast/walk.scm 307 */
				obj_t BgL_res4057z00_9208;

				{	/* Ast/walk.scm 307 */
					obj_t BgL_arg2972z00_9205;

					{	/* Ast/walk.scm 307 */
						BgL_nodez00_bglt BgL_arg2974z00_9206;

						BgL_arg2974z00_9206 =
							(((BgL_returnz00_bglt) COBJECT(
									((BgL_returnz00_bglt) BgL_nz00_7867)))->BgL_valuez00);
						BgL_arg2972z00_9205 =
							BGL_PROCEDURE_CALL3(BgL_pz00_7868,
							((obj_t) BgL_arg2974z00_9206), BgL_arg0z00_7869,
							BgL_arg1z00_7870);
					}
					{	/* Ast/walk.scm 307 */
						obj_t BgL_list2973z00_9207;

						BgL_list2973z00_9207 = MAKE_YOUNG_PAIR(BgL_arg2972z00_9205, BNIL);
						BgL_res4057z00_9208 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2973z00_9207);
					}
				}
				return BgL_res4057z00_9208;
			}
		}

	}



/* &walk1*-return1987 */
	obj_t BGl_z62walk1za2zd2return1987z12zzast_walkz00(obj_t BgL_envz00_7871,
		obj_t BgL_nz00_7872, obj_t BgL_pz00_7873, obj_t BgL_arg0z00_7874)
	{
		{	/* Ast/walk.scm 307 */
			{	/* Ast/walk.scm 307 */
				obj_t BgL_res4058z00_9213;

				{	/* Ast/walk.scm 307 */
					obj_t BgL_arg2966z00_9210;

					{	/* Ast/walk.scm 307 */
						BgL_nodez00_bglt BgL_arg2968z00_9211;

						BgL_arg2968z00_9211 =
							(((BgL_returnz00_bglt) COBJECT(
									((BgL_returnz00_bglt) BgL_nz00_7872)))->BgL_valuez00);
						BgL_arg2966z00_9210 =
							BGL_PROCEDURE_CALL2(BgL_pz00_7873,
							((obj_t) BgL_arg2968z00_9211), BgL_arg0z00_7874);
					}
					{	/* Ast/walk.scm 307 */
						obj_t BgL_list2967z00_9212;

						BgL_list2967z00_9212 = MAKE_YOUNG_PAIR(BgL_arg2966z00_9210, BNIL);
						BgL_res4058z00_9213 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2967z00_9212);
					}
				}
				return BgL_res4058z00_9213;
			}
		}

	}



/* &walk0*-return1985 */
	obj_t BGl_z62walk0za2zd2return1985z12zzast_walkz00(obj_t BgL_envz00_7875,
		obj_t BgL_nz00_7876, obj_t BgL_pz00_7877)
	{
		{	/* Ast/walk.scm 307 */
			{	/* Ast/walk.scm 307 */
				obj_t BgL_res4059z00_9218;

				{	/* Ast/walk.scm 307 */
					obj_t BgL_arg2954z00_9215;

					{	/* Ast/walk.scm 307 */
						BgL_nodez00_bglt BgL_arg2956z00_9216;

						BgL_arg2956z00_9216 =
							(((BgL_returnz00_bglt) COBJECT(
									((BgL_returnz00_bglt) BgL_nz00_7876)))->BgL_valuez00);
						BgL_arg2954z00_9215 =
							BGL_PROCEDURE_CALL1(BgL_pz00_7877, ((obj_t) BgL_arg2956z00_9216));
					}
					{	/* Ast/walk.scm 307 */
						obj_t BgL_list2955z00_9217;

						BgL_list2955z00_9217 = MAKE_YOUNG_PAIR(BgL_arg2954z00_9215, BNIL);
						BgL_res4059z00_9218 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2955z00_9217);
					}
				}
				return BgL_res4059z00_9218;
			}
		}

	}



/* &walk3-return1983 */
	obj_t BGl_z62walk3zd2return1983zb0zzast_walkz00(obj_t BgL_envz00_7878,
		obj_t BgL_nz00_7879, obj_t BgL_pz00_7880, obj_t BgL_arg0z00_7881,
		obj_t BgL_arg1z00_7882, obj_t BgL_arg2z00_7883)
	{
		{	/* Ast/walk.scm 307 */
			{	/* Ast/walk.scm 307 */
				BgL_nodez00_bglt BgL_arg2943z00_9220;

				BgL_arg2943z00_9220 =
					(((BgL_returnz00_bglt) COBJECT(
							((BgL_returnz00_bglt) BgL_nz00_7879)))->BgL_valuez00);
				return
					BGL_PROCEDURE_CALL4(BgL_pz00_7880,
					((obj_t) BgL_arg2943z00_9220), BgL_arg0z00_7881, BgL_arg1z00_7882,
					BgL_arg2z00_7883);
			}
		}

	}



/* &walk2-return1981 */
	obj_t BGl_z62walk2zd2return1981zb0zzast_walkz00(obj_t BgL_envz00_7884,
		obj_t BgL_nz00_7885, obj_t BgL_pz00_7886, obj_t BgL_arg0z00_7887,
		obj_t BgL_arg1z00_7888)
	{
		{	/* Ast/walk.scm 307 */
			{	/* Ast/walk.scm 307 */
				BgL_nodez00_bglt BgL_arg2942z00_9222;

				BgL_arg2942z00_9222 =
					(((BgL_returnz00_bglt) COBJECT(
							((BgL_returnz00_bglt) BgL_nz00_7885)))->BgL_valuez00);
				return
					BGL_PROCEDURE_CALL3(BgL_pz00_7886,
					((obj_t) BgL_arg2942z00_9222), BgL_arg0z00_7887, BgL_arg1z00_7888);
			}
		}

	}



/* &walk1-return1979 */
	obj_t BGl_z62walk1zd2return1979zb0zzast_walkz00(obj_t BgL_envz00_7889,
		obj_t BgL_nz00_7890, obj_t BgL_pz00_7891, obj_t BgL_arg0z00_7892)
	{
		{	/* Ast/walk.scm 307 */
			{	/* Ast/walk.scm 307 */
				BgL_nodez00_bglt BgL_arg2941z00_9224;

				BgL_arg2941z00_9224 =
					(((BgL_returnz00_bglt) COBJECT(
							((BgL_returnz00_bglt) BgL_nz00_7890)))->BgL_valuez00);
				return
					BGL_PROCEDURE_CALL2(BgL_pz00_7891,
					((obj_t) BgL_arg2941z00_9224), BgL_arg0z00_7892);
			}
		}

	}



/* &walk0-return1977 */
	obj_t BGl_z62walk0zd2return1977zb0zzast_walkz00(obj_t BgL_envz00_7893,
		obj_t BgL_nz00_7894, obj_t BgL_pz00_7895)
	{
		{	/* Ast/walk.scm 307 */
			{	/* Ast/walk.scm 307 */
				BgL_nodez00_bglt BgL_arg2940z00_9226;

				BgL_arg2940z00_9226 =
					(((BgL_returnz00_bglt) COBJECT(
							((BgL_returnz00_bglt) BgL_nz00_7894)))->BgL_valuez00);
				return
					BGL_PROCEDURE_CALL1(BgL_pz00_7895, ((obj_t) BgL_arg2940z00_9226));
			}
		}

	}



/* &walk3!-retblock1975 */
	BgL_nodez00_bglt BGl_z62walk3z12zd2retblock1975za2zzast_walkz00(obj_t
		BgL_envz00_7896, obj_t BgL_nz00_7897, obj_t BgL_pz00_7898,
		obj_t BgL_arg0z00_7899, obj_t BgL_arg1z00_7900, obj_t BgL_arg2z00_7901)
	{
		{	/* Ast/walk.scm 306 */
			{
				BgL_nodez00_bglt BgL_auxz00_13106;

				{	/* Ast/walk.scm 306 */
					BgL_nodez00_bglt BgL_arg2936z00_9228;

					BgL_arg2936z00_9228 =
						(((BgL_retblockz00_bglt) COBJECT(
								((BgL_retblockz00_bglt) BgL_nz00_7897)))->BgL_bodyz00);
					BgL_auxz00_13106 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_7898,
							((obj_t) BgL_arg2936z00_9228), BgL_arg0z00_7899, BgL_arg1z00_7900,
							BgL_arg2z00_7901));
				}
				((((BgL_retblockz00_bglt) COBJECT(
								((BgL_retblockz00_bglt) BgL_nz00_7897)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13106), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_retblockz00_bglt) BgL_nz00_7897));
		}

	}



/* &walk2!-retblock1973 */
	BgL_nodez00_bglt BGl_z62walk2z12zd2retblock1973za2zzast_walkz00(obj_t
		BgL_envz00_7902, obj_t BgL_nz00_7903, obj_t BgL_pz00_7904,
		obj_t BgL_arg0z00_7905, obj_t BgL_arg1z00_7906)
	{
		{	/* Ast/walk.scm 306 */
			{
				BgL_nodez00_bglt BgL_auxz00_13122;

				{	/* Ast/walk.scm 306 */
					BgL_nodez00_bglt BgL_arg2935z00_9230;

					BgL_arg2935z00_9230 =
						(((BgL_retblockz00_bglt) COBJECT(
								((BgL_retblockz00_bglt) BgL_nz00_7903)))->BgL_bodyz00);
					BgL_auxz00_13122 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_7904,
							((obj_t) BgL_arg2935z00_9230), BgL_arg0z00_7905,
							BgL_arg1z00_7906));
				}
				((((BgL_retblockz00_bglt) COBJECT(
								((BgL_retblockz00_bglt) BgL_nz00_7903)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13122), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_retblockz00_bglt) BgL_nz00_7903));
		}

	}



/* &walk1!-retblock1971 */
	BgL_nodez00_bglt BGl_z62walk1z12zd2retblock1971za2zzast_walkz00(obj_t
		BgL_envz00_7907, obj_t BgL_nz00_7908, obj_t BgL_pz00_7909,
		obj_t BgL_arg0z00_7910)
	{
		{	/* Ast/walk.scm 306 */
			{
				BgL_nodez00_bglt BgL_auxz00_13137;

				{	/* Ast/walk.scm 306 */
					BgL_nodez00_bglt BgL_arg2934z00_9232;

					BgL_arg2934z00_9232 =
						(((BgL_retblockz00_bglt) COBJECT(
								((BgL_retblockz00_bglt) BgL_nz00_7908)))->BgL_bodyz00);
					BgL_auxz00_13137 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_7909,
							((obj_t) BgL_arg2934z00_9232), BgL_arg0z00_7910));
				}
				((((BgL_retblockz00_bglt) COBJECT(
								((BgL_retblockz00_bglt) BgL_nz00_7908)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13137), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_retblockz00_bglt) BgL_nz00_7908));
		}

	}



/* &walk0!-retblock1969 */
	BgL_nodez00_bglt BGl_z62walk0z12zd2retblock1969za2zzast_walkz00(obj_t
		BgL_envz00_7911, obj_t BgL_nz00_7912, obj_t BgL_pz00_7913)
	{
		{	/* Ast/walk.scm 306 */
			{
				BgL_nodez00_bglt BgL_auxz00_13151;

				{	/* Ast/walk.scm 306 */
					BgL_nodez00_bglt BgL_arg2933z00_9234;

					BgL_arg2933z00_9234 =
						(((BgL_retblockz00_bglt) COBJECT(
								((BgL_retblockz00_bglt) BgL_nz00_7912)))->BgL_bodyz00);
					BgL_auxz00_13151 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_7913, ((obj_t) BgL_arg2933z00_9234)));
				}
				((((BgL_retblockz00_bglt) COBJECT(
								((BgL_retblockz00_bglt) BgL_nz00_7912)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13151), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_retblockz00_bglt) BgL_nz00_7912));
		}

	}



/* &walk3*-retblock1967 */
	obj_t BGl_z62walk3za2zd2retblock1967z12zzast_walkz00(obj_t BgL_envz00_7914,
		obj_t BgL_nz00_7915, obj_t BgL_pz00_7916, obj_t BgL_arg0z00_7917,
		obj_t BgL_arg1z00_7918, obj_t BgL_arg2z00_7919)
	{
		{	/* Ast/walk.scm 306 */
			{	/* Ast/walk.scm 306 */
				obj_t BgL_res4060z00_9239;

				{	/* Ast/walk.scm 306 */
					obj_t BgL_arg2930z00_9236;

					{	/* Ast/walk.scm 306 */
						BgL_nodez00_bglt BgL_arg2932z00_9237;

						BgL_arg2932z00_9237 =
							(((BgL_retblockz00_bglt) COBJECT(
									((BgL_retblockz00_bglt) BgL_nz00_7915)))->BgL_bodyz00);
						BgL_arg2930z00_9236 =
							BGL_PROCEDURE_CALL4(BgL_pz00_7916,
							((obj_t) BgL_arg2932z00_9237), BgL_arg0z00_7917, BgL_arg1z00_7918,
							BgL_arg2z00_7919);
					}
					{	/* Ast/walk.scm 306 */
						obj_t BgL_list2931z00_9238;

						BgL_list2931z00_9238 = MAKE_YOUNG_PAIR(BgL_arg2930z00_9236, BNIL);
						BgL_res4060z00_9239 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2931z00_9238);
					}
				}
				return BgL_res4060z00_9239;
			}
		}

	}



/* &walk2*-retblock1965 */
	obj_t BGl_z62walk2za2zd2retblock1965z12zzast_walkz00(obj_t BgL_envz00_7920,
		obj_t BgL_nz00_7921, obj_t BgL_pz00_7922, obj_t BgL_arg0z00_7923,
		obj_t BgL_arg1z00_7924)
	{
		{	/* Ast/walk.scm 306 */
			{	/* Ast/walk.scm 306 */
				obj_t BgL_res4061z00_9244;

				{	/* Ast/walk.scm 306 */
					obj_t BgL_arg2927z00_9241;

					{	/* Ast/walk.scm 306 */
						BgL_nodez00_bglt BgL_arg2929z00_9242;

						BgL_arg2929z00_9242 =
							(((BgL_retblockz00_bglt) COBJECT(
									((BgL_retblockz00_bglt) BgL_nz00_7921)))->BgL_bodyz00);
						BgL_arg2927z00_9241 =
							BGL_PROCEDURE_CALL3(BgL_pz00_7922,
							((obj_t) BgL_arg2929z00_9242), BgL_arg0z00_7923,
							BgL_arg1z00_7924);
					}
					{	/* Ast/walk.scm 306 */
						obj_t BgL_list2928z00_9243;

						BgL_list2928z00_9243 = MAKE_YOUNG_PAIR(BgL_arg2927z00_9241, BNIL);
						BgL_res4061z00_9244 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2928z00_9243);
					}
				}
				return BgL_res4061z00_9244;
			}
		}

	}



/* &walk1*-retblock1963 */
	obj_t BGl_z62walk1za2zd2retblock1963z12zzast_walkz00(obj_t BgL_envz00_7925,
		obj_t BgL_nz00_7926, obj_t BgL_pz00_7927, obj_t BgL_arg0z00_7928)
	{
		{	/* Ast/walk.scm 306 */
			{	/* Ast/walk.scm 306 */
				obj_t BgL_res4062z00_9249;

				{	/* Ast/walk.scm 306 */
					obj_t BgL_arg2924z00_9246;

					{	/* Ast/walk.scm 306 */
						BgL_nodez00_bglt BgL_arg2926z00_9247;

						BgL_arg2926z00_9247 =
							(((BgL_retblockz00_bglt) COBJECT(
									((BgL_retblockz00_bglt) BgL_nz00_7926)))->BgL_bodyz00);
						BgL_arg2924z00_9246 =
							BGL_PROCEDURE_CALL2(BgL_pz00_7927,
							((obj_t) BgL_arg2926z00_9247), BgL_arg0z00_7928);
					}
					{	/* Ast/walk.scm 306 */
						obj_t BgL_list2925z00_9248;

						BgL_list2925z00_9248 = MAKE_YOUNG_PAIR(BgL_arg2924z00_9246, BNIL);
						BgL_res4062z00_9249 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2925z00_9248);
					}
				}
				return BgL_res4062z00_9249;
			}
		}

	}



/* &walk0*-retblock1961 */
	obj_t BGl_z62walk0za2zd2retblock1961z12zzast_walkz00(obj_t BgL_envz00_7929,
		obj_t BgL_nz00_7930, obj_t BgL_pz00_7931)
	{
		{	/* Ast/walk.scm 306 */
			{	/* Ast/walk.scm 306 */
				obj_t BgL_res4063z00_9254;

				{	/* Ast/walk.scm 306 */
					obj_t BgL_arg2920z00_9251;

					{	/* Ast/walk.scm 306 */
						BgL_nodez00_bglt BgL_arg2923z00_9252;

						BgL_arg2923z00_9252 =
							(((BgL_retblockz00_bglt) COBJECT(
									((BgL_retblockz00_bglt) BgL_nz00_7930)))->BgL_bodyz00);
						BgL_arg2920z00_9251 =
							BGL_PROCEDURE_CALL1(BgL_pz00_7931, ((obj_t) BgL_arg2923z00_9252));
					}
					{	/* Ast/walk.scm 306 */
						obj_t BgL_list2921z00_9253;

						BgL_list2921z00_9253 = MAKE_YOUNG_PAIR(BgL_arg2920z00_9251, BNIL);
						BgL_res4063z00_9254 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2921z00_9253);
					}
				}
				return BgL_res4063z00_9254;
			}
		}

	}



/* &walk3-retblock1959 */
	obj_t BGl_z62walk3zd2retblock1959zb0zzast_walkz00(obj_t BgL_envz00_7932,
		obj_t BgL_nz00_7933, obj_t BgL_pz00_7934, obj_t BgL_arg0z00_7935,
		obj_t BgL_arg1z00_7936, obj_t BgL_arg2z00_7937)
	{
		{	/* Ast/walk.scm 306 */
			{	/* Ast/walk.scm 306 */
				BgL_nodez00_bglt BgL_arg2919z00_9256;

				BgL_arg2919z00_9256 =
					(((BgL_retblockz00_bglt) COBJECT(
							((BgL_retblockz00_bglt) BgL_nz00_7933)))->BgL_bodyz00);
				return
					BGL_PROCEDURE_CALL4(BgL_pz00_7934,
					((obj_t) BgL_arg2919z00_9256), BgL_arg0z00_7935, BgL_arg1z00_7936,
					BgL_arg2z00_7937);
			}
		}

	}



/* &walk2-retblock1957 */
	obj_t BGl_z62walk2zd2retblock1957zb0zzast_walkz00(obj_t BgL_envz00_7938,
		obj_t BgL_nz00_7939, obj_t BgL_pz00_7940, obj_t BgL_arg0z00_7941,
		obj_t BgL_arg1z00_7942)
	{
		{	/* Ast/walk.scm 306 */
			{	/* Ast/walk.scm 306 */
				BgL_nodez00_bglt BgL_arg2918z00_9258;

				BgL_arg2918z00_9258 =
					(((BgL_retblockz00_bglt) COBJECT(
							((BgL_retblockz00_bglt) BgL_nz00_7939)))->BgL_bodyz00);
				return
					BGL_PROCEDURE_CALL3(BgL_pz00_7940,
					((obj_t) BgL_arg2918z00_9258), BgL_arg0z00_7941, BgL_arg1z00_7942);
			}
		}

	}



/* &walk1-retblock1955 */
	obj_t BGl_z62walk1zd2retblock1955zb0zzast_walkz00(obj_t BgL_envz00_7943,
		obj_t BgL_nz00_7944, obj_t BgL_pz00_7945, obj_t BgL_arg0z00_7946)
	{
		{	/* Ast/walk.scm 306 */
			{	/* Ast/walk.scm 306 */
				BgL_nodez00_bglt BgL_arg2917z00_9260;

				BgL_arg2917z00_9260 =
					(((BgL_retblockz00_bglt) COBJECT(
							((BgL_retblockz00_bglt) BgL_nz00_7944)))->BgL_bodyz00);
				return
					BGL_PROCEDURE_CALL2(BgL_pz00_7945,
					((obj_t) BgL_arg2917z00_9260), BgL_arg0z00_7946);
			}
		}

	}



/* &walk0-retblock1953 */
	obj_t BGl_z62walk0zd2retblock1953zb0zzast_walkz00(obj_t BgL_envz00_7947,
		obj_t BgL_nz00_7948, obj_t BgL_pz00_7949)
	{
		{	/* Ast/walk.scm 306 */
			{	/* Ast/walk.scm 306 */
				BgL_nodez00_bglt BgL_arg2916z00_9262;

				BgL_arg2916z00_9262 =
					(((BgL_retblockz00_bglt) COBJECT(
							((BgL_retblockz00_bglt) BgL_nz00_7948)))->BgL_bodyz00);
				return
					BGL_PROCEDURE_CALL1(BgL_pz00_7949, ((obj_t) BgL_arg2916z00_9262));
			}
		}

	}



/* &walk3!-jump-ex-it1951 */
	BgL_nodez00_bglt BGl_z62walk3z12zd2jumpzd2exzd2it1951za2zzast_walkz00(obj_t
		BgL_envz00_7950, obj_t BgL_nz00_7951, obj_t BgL_pz00_7952,
		obj_t BgL_arg0z00_7953, obj_t BgL_arg1z00_7954, obj_t BgL_arg2z00_7955)
	{
		{	/* Ast/walk.scm 305 */
			{
				BgL_nodez00_bglt BgL_auxz00_13240;

				{	/* Ast/walk.scm 305 */
					BgL_nodez00_bglt BgL_arg2914z00_9264;

					BgL_arg2914z00_9264 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7951)))->BgL_exitz00);
					BgL_auxz00_13240 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_7952,
							((obj_t) BgL_arg2914z00_9264), BgL_arg0z00_7953, BgL_arg1z00_7954,
							BgL_arg2z00_7955));
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7951)))->BgL_exitz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13240), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_13254;

				{	/* Ast/walk.scm 305 */
					BgL_nodez00_bglt BgL_arg2915z00_9265;

					BgL_arg2915z00_9265 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7951)))->BgL_valuez00);
					BgL_auxz00_13254 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_7952,
							((obj_t) BgL_arg2915z00_9265), BgL_arg0z00_7953, BgL_arg1z00_7954,
							BgL_arg2z00_7955));
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7951)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_13254), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7951));
		}

	}



/* &walk2!-jump-ex-it1949 */
	BgL_nodez00_bglt BGl_z62walk2z12zd2jumpzd2exzd2it1949za2zzast_walkz00(obj_t
		BgL_envz00_7956, obj_t BgL_nz00_7957, obj_t BgL_pz00_7958,
		obj_t BgL_arg0z00_7959, obj_t BgL_arg1z00_7960)
	{
		{	/* Ast/walk.scm 305 */
			{
				BgL_nodez00_bglt BgL_auxz00_13270;

				{	/* Ast/walk.scm 305 */
					BgL_nodez00_bglt BgL_arg2912z00_9267;

					BgL_arg2912z00_9267 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7957)))->BgL_exitz00);
					BgL_auxz00_13270 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_7958,
							((obj_t) BgL_arg2912z00_9267), BgL_arg0z00_7959,
							BgL_arg1z00_7960));
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7957)))->BgL_exitz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13270), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_13283;

				{	/* Ast/walk.scm 305 */
					BgL_nodez00_bglt BgL_arg2913z00_9268;

					BgL_arg2913z00_9268 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7957)))->BgL_valuez00);
					BgL_auxz00_13283 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_7958,
							((obj_t) BgL_arg2913z00_9268), BgL_arg0z00_7959,
							BgL_arg1z00_7960));
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7957)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_13283), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7957));
		}

	}



/* &walk1!-jump-ex-it1947 */
	BgL_nodez00_bglt BGl_z62walk1z12zd2jumpzd2exzd2it1947za2zzast_walkz00(obj_t
		BgL_envz00_7961, obj_t BgL_nz00_7962, obj_t BgL_pz00_7963,
		obj_t BgL_arg0z00_7964)
	{
		{	/* Ast/walk.scm 305 */
			{
				BgL_nodez00_bglt BgL_auxz00_13298;

				{	/* Ast/walk.scm 305 */
					BgL_nodez00_bglt BgL_arg2907z00_9270;

					BgL_arg2907z00_9270 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7962)))->BgL_exitz00);
					BgL_auxz00_13298 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_7963,
							((obj_t) BgL_arg2907z00_9270), BgL_arg0z00_7964));
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7962)))->BgL_exitz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13298), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_13310;

				{	/* Ast/walk.scm 305 */
					BgL_nodez00_bglt BgL_arg2908z00_9271;

					BgL_arg2908z00_9271 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7962)))->BgL_valuez00);
					BgL_auxz00_13310 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_7963,
							((obj_t) BgL_arg2908z00_9271), BgL_arg0z00_7964));
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7962)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_13310), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7962));
		}

	}



/* &walk0!-jump-ex-it1945 */
	BgL_nodez00_bglt BGl_z62walk0z12zd2jumpzd2exzd2it1945za2zzast_walkz00(obj_t
		BgL_envz00_7965, obj_t BgL_nz00_7966, obj_t BgL_pz00_7967)
	{
		{	/* Ast/walk.scm 305 */
			{
				BgL_nodez00_bglt BgL_auxz00_13324;

				{	/* Ast/walk.scm 305 */
					BgL_nodez00_bglt BgL_arg2905z00_9273;

					BgL_arg2905z00_9273 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7966)))->BgL_exitz00);
					BgL_auxz00_13324 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_7967, ((obj_t) BgL_arg2905z00_9273)));
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7966)))->BgL_exitz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13324), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_13335;

				{	/* Ast/walk.scm 305 */
					BgL_nodez00_bglt BgL_arg2906z00_9274;

					BgL_arg2906z00_9274 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7966)))->BgL_valuez00);
					BgL_auxz00_13335 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_7967, ((obj_t) BgL_arg2906z00_9274)));
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7966)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_13335), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7966));
		}

	}



/* &walk3*-jump-ex-it1943 */
	obj_t BGl_z62walk3za2zd2jumpzd2exzd2it1943z12zzast_walkz00(obj_t
		BgL_envz00_7968, obj_t BgL_nz00_7969, obj_t BgL_pz00_7970,
		obj_t BgL_arg0z00_7971, obj_t BgL_arg1z00_7972, obj_t BgL_arg2z00_7973)
	{
		{	/* Ast/walk.scm 305 */
			{	/* Ast/walk.scm 305 */
				obj_t BgL_res4064z00_9280;

				{	/* Ast/walk.scm 305 */
					obj_t BgL_arg2898z00_9276;
					obj_t BgL_arg2899z00_9277;

					{	/* Ast/walk.scm 305 */
						BgL_nodez00_bglt BgL_arg2903z00_9278;

						BgL_arg2903z00_9278 =
							(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7969)))->BgL_exitz00);
						BgL_arg2898z00_9276 =
							BGL_PROCEDURE_CALL4(BgL_pz00_7970,
							((obj_t) BgL_arg2903z00_9278), BgL_arg0z00_7971, BgL_arg1z00_7972,
							BgL_arg2z00_7973);
					}
					{	/* Ast/walk.scm 305 */
						BgL_nodez00_bglt BgL_arg2904z00_9279;

						BgL_arg2904z00_9279 =
							(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7969)))->BgL_valuez00);
						BgL_arg2899z00_9277 =
							BGL_PROCEDURE_CALL4(BgL_pz00_7970,
							((obj_t) BgL_arg2904z00_9279), BgL_arg0z00_7971, BgL_arg1z00_7972,
							BgL_arg2z00_7973);
					}
					BgL_res4064z00_9280 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg2898z00_9276,
						BgL_arg2899z00_9277);
				}
				return BgL_res4064z00_9280;
			}
		}

	}



/* &walk2*-jump-ex-it1941 */
	obj_t BGl_z62walk2za2zd2jumpzd2exzd2it1941z12zzast_walkz00(obj_t
		BgL_envz00_7974, obj_t BgL_nz00_7975, obj_t BgL_pz00_7976,
		obj_t BgL_arg0z00_7977, obj_t BgL_arg1z00_7978)
	{
		{	/* Ast/walk.scm 305 */
			{	/* Ast/walk.scm 305 */
				obj_t BgL_res4065z00_9286;

				{	/* Ast/walk.scm 305 */
					obj_t BgL_arg2894z00_9282;
					obj_t BgL_arg2895z00_9283;

					{	/* Ast/walk.scm 305 */
						BgL_nodez00_bglt BgL_arg2896z00_9284;

						BgL_arg2896z00_9284 =
							(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7975)))->BgL_exitz00);
						BgL_arg2894z00_9282 =
							BGL_PROCEDURE_CALL3(BgL_pz00_7976,
							((obj_t) BgL_arg2896z00_9284), BgL_arg0z00_7977,
							BgL_arg1z00_7978);
					}
					{	/* Ast/walk.scm 305 */
						BgL_nodez00_bglt BgL_arg2897z00_9285;

						BgL_arg2897z00_9285 =
							(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7975)))->BgL_valuez00);
						BgL_arg2895z00_9283 =
							BGL_PROCEDURE_CALL3(BgL_pz00_7976,
							((obj_t) BgL_arg2897z00_9285), BgL_arg0z00_7977,
							BgL_arg1z00_7978);
					}
					BgL_res4065z00_9286 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg2894z00_9282,
						BgL_arg2895z00_9283);
				}
				return BgL_res4065z00_9286;
			}
		}

	}



/* &walk1*-jump-ex-it1939 */
	obj_t BGl_z62walk1za2zd2jumpzd2exzd2it1939z12zzast_walkz00(obj_t
		BgL_envz00_7979, obj_t BgL_nz00_7980, obj_t BgL_pz00_7981,
		obj_t BgL_arg0z00_7982)
	{
		{	/* Ast/walk.scm 305 */
			{	/* Ast/walk.scm 305 */
				obj_t BgL_res4066z00_9292;

				{	/* Ast/walk.scm 305 */
					obj_t BgL_arg2890z00_9288;
					obj_t BgL_arg2891z00_9289;

					{	/* Ast/walk.scm 305 */
						BgL_nodez00_bglt BgL_arg2892z00_9290;

						BgL_arg2892z00_9290 =
							(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7980)))->BgL_exitz00);
						BgL_arg2890z00_9288 =
							BGL_PROCEDURE_CALL2(BgL_pz00_7981,
							((obj_t) BgL_arg2892z00_9290), BgL_arg0z00_7982);
					}
					{	/* Ast/walk.scm 305 */
						BgL_nodez00_bglt BgL_arg2893z00_9291;

						BgL_arg2893z00_9291 =
							(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7980)))->BgL_valuez00);
						BgL_arg2891z00_9289 =
							BGL_PROCEDURE_CALL2(BgL_pz00_7981,
							((obj_t) BgL_arg2893z00_9291), BgL_arg0z00_7982);
					}
					BgL_res4066z00_9292 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg2890z00_9288,
						BgL_arg2891z00_9289);
				}
				return BgL_res4066z00_9292;
			}
		}

	}



/* &walk0*-jump-ex-it1937 */
	obj_t BGl_z62walk0za2zd2jumpzd2exzd2it1937z12zzast_walkz00(obj_t
		BgL_envz00_7983, obj_t BgL_nz00_7984, obj_t BgL_pz00_7985)
	{
		{	/* Ast/walk.scm 305 */
			{	/* Ast/walk.scm 305 */
				obj_t BgL_res4067z00_9298;

				{	/* Ast/walk.scm 305 */
					obj_t BgL_arg2883z00_9294;
					obj_t BgL_arg2887z00_9295;

					{	/* Ast/walk.scm 305 */
						BgL_nodez00_bglt BgL_arg2888z00_9296;

						BgL_arg2888z00_9296 =
							(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7984)))->BgL_exitz00);
						BgL_arg2883z00_9294 =
							BGL_PROCEDURE_CALL1(BgL_pz00_7985, ((obj_t) BgL_arg2888z00_9296));
					}
					{	/* Ast/walk.scm 305 */
						BgL_nodez00_bglt BgL_arg2889z00_9297;

						BgL_arg2889z00_9297 =
							(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7984)))->BgL_valuez00);
						BgL_arg2887z00_9295 =
							BGL_PROCEDURE_CALL1(BgL_pz00_7985, ((obj_t) BgL_arg2889z00_9297));
					}
					BgL_res4067z00_9298 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg2883z00_9294,
						BgL_arg2887z00_9295);
				}
				return BgL_res4067z00_9298;
			}
		}

	}



/* &walk3-jump-ex-it1935 */
	obj_t BGl_z62walk3zd2jumpzd2exzd2it1935zb0zzast_walkz00(obj_t BgL_envz00_7986,
		obj_t BgL_nz00_7987, obj_t BgL_pz00_7988, obj_t BgL_arg0z00_7989,
		obj_t BgL_arg1z00_7990, obj_t BgL_arg2z00_7991)
	{
		{	/* Ast/walk.scm 305 */
			{	/* Ast/walk.scm 305 */
				BgL_nodez00_bglt BgL_arg2881z00_9300;

				BgL_arg2881z00_9300 =
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7987)))->BgL_exitz00);
				BGL_PROCEDURE_CALL4(BgL_pz00_7988,
					((obj_t) BgL_arg2881z00_9300), BgL_arg0z00_7989, BgL_arg1z00_7990,
					BgL_arg2z00_7991);
			}
			{	/* Ast/walk.scm 305 */
				BgL_nodez00_bglt BgL_arg2882z00_9301;

				BgL_arg2882z00_9301 =
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7987)))->BgL_valuez00);
				return
					BGL_PROCEDURE_CALL4(BgL_pz00_7988,
					((obj_t) BgL_arg2882z00_9301), BgL_arg0z00_7989, BgL_arg1z00_7990,
					BgL_arg2z00_7991);
			}
		}

	}



/* &walk2-jump-ex-it1933 */
	obj_t BGl_z62walk2zd2jumpzd2exzd2it1933zb0zzast_walkz00(obj_t BgL_envz00_7992,
		obj_t BgL_nz00_7993, obj_t BgL_pz00_7994, obj_t BgL_arg0z00_7995,
		obj_t BgL_arg1z00_7996)
	{
		{	/* Ast/walk.scm 305 */
			{	/* Ast/walk.scm 305 */
				BgL_nodez00_bglt BgL_arg2879z00_9303;

				BgL_arg2879z00_9303 =
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7993)))->BgL_exitz00);
				BGL_PROCEDURE_CALL3(BgL_pz00_7994,
					((obj_t) BgL_arg2879z00_9303), BgL_arg0z00_7995, BgL_arg1z00_7996);
			}
			{	/* Ast/walk.scm 305 */
				BgL_nodez00_bglt BgL_arg2880z00_9304;

				BgL_arg2880z00_9304 =
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7993)))->BgL_valuez00);
				return
					BGL_PROCEDURE_CALL3(BgL_pz00_7994,
					((obj_t) BgL_arg2880z00_9304), BgL_arg0z00_7995, BgL_arg1z00_7996);
			}
		}

	}



/* &walk1-jump-ex-it1931 */
	obj_t BGl_z62walk1zd2jumpzd2exzd2it1931zb0zzast_walkz00(obj_t BgL_envz00_7997,
		obj_t BgL_nz00_7998, obj_t BgL_pz00_7999, obj_t BgL_arg0z00_8000)
	{
		{	/* Ast/walk.scm 305 */
			{	/* Ast/walk.scm 305 */
				BgL_nodez00_bglt BgL_arg2877z00_9306;

				BgL_arg2877z00_9306 =
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7998)))->BgL_exitz00);
				BGL_PROCEDURE_CALL2(BgL_pz00_7999,
					((obj_t) BgL_arg2877z00_9306), BgL_arg0z00_8000);
			}
			{	/* Ast/walk.scm 305 */
				BgL_nodez00_bglt BgL_arg2878z00_9307;

				BgL_arg2878z00_9307 =
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_7998)))->BgL_valuez00);
				return
					BGL_PROCEDURE_CALL2(BgL_pz00_7999,
					((obj_t) BgL_arg2878z00_9307), BgL_arg0z00_8000);
			}
		}

	}



/* &walk0-jump-ex-it1929 */
	obj_t BGl_z62walk0zd2jumpzd2exzd2it1929zb0zzast_walkz00(obj_t BgL_envz00_8001,
		obj_t BgL_nz00_8002, obj_t BgL_pz00_8003)
	{
		{	/* Ast/walk.scm 305 */
			{	/* Ast/walk.scm 305 */
				BgL_nodez00_bglt BgL_arg2875z00_9309;

				BgL_arg2875z00_9309 =
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_8002)))->BgL_exitz00);
				BGL_PROCEDURE_CALL1(BgL_pz00_8003, ((obj_t) BgL_arg2875z00_9309));
			}
			{	/* Ast/walk.scm 305 */
				BgL_nodez00_bglt BgL_arg2876z00_9310;

				BgL_arg2876z00_9310 =
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nz00_8002)))->BgL_valuez00);
				return
					BGL_PROCEDURE_CALL1(BgL_pz00_8003, ((obj_t) BgL_arg2876z00_9310));
			}
		}

	}



/* &walk3!-set-ex-it1927 */
	BgL_nodez00_bglt BGl_z62walk3z12zd2setzd2exzd2it1927za2zzast_walkz00(obj_t
		BgL_envz00_8004, obj_t BgL_nz00_8005, obj_t BgL_pz00_8006,
		obj_t BgL_arg0z00_8007, obj_t BgL_arg1z00_8008, obj_t BgL_arg2z00_8009)
	{
		{	/* Ast/walk.scm 304 */
			{
				BgL_nodez00_bglt BgL_auxz00_13488;

				{	/* Ast/walk.scm 304 */
					BgL_nodez00_bglt BgL_arg2873z00_9312;

					BgL_arg2873z00_9312 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8005)))->BgL_bodyz00);
					BgL_auxz00_13488 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_8006,
							((obj_t) BgL_arg2873z00_9312), BgL_arg0z00_8007, BgL_arg1z00_8008,
							BgL_arg2z00_8009));
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8005)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13488), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_13502;

				{	/* Ast/walk.scm 304 */
					BgL_nodez00_bglt BgL_arg2874z00_9313;

					BgL_arg2874z00_9313 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8005)))->BgL_onexitz00);
					BgL_auxz00_13502 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_8006,
							((obj_t) BgL_arg2874z00_9313), BgL_arg0z00_8007, BgL_arg1z00_8008,
							BgL_arg2z00_8009));
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8005)))->BgL_onexitz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13502), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8005));
		}

	}



/* &walk2!-set-ex-it1925 */
	BgL_nodez00_bglt BGl_z62walk2z12zd2setzd2exzd2it1925za2zzast_walkz00(obj_t
		BgL_envz00_8010, obj_t BgL_nz00_8011, obj_t BgL_pz00_8012,
		obj_t BgL_arg0z00_8013, obj_t BgL_arg1z00_8014)
	{
		{	/* Ast/walk.scm 304 */
			{
				BgL_nodez00_bglt BgL_auxz00_13518;

				{	/* Ast/walk.scm 304 */
					BgL_nodez00_bglt BgL_arg2871z00_9315;

					BgL_arg2871z00_9315 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8011)))->BgL_bodyz00);
					BgL_auxz00_13518 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_8012,
							((obj_t) BgL_arg2871z00_9315), BgL_arg0z00_8013,
							BgL_arg1z00_8014));
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8011)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13518), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_13531;

				{	/* Ast/walk.scm 304 */
					BgL_nodez00_bglt BgL_arg2872z00_9316;

					BgL_arg2872z00_9316 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8011)))->BgL_onexitz00);
					BgL_auxz00_13531 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_8012,
							((obj_t) BgL_arg2872z00_9316), BgL_arg0z00_8013,
							BgL_arg1z00_8014));
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8011)))->BgL_onexitz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13531), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8011));
		}

	}



/* &walk1!-set-ex-it1923 */
	BgL_nodez00_bglt BGl_z62walk1z12zd2setzd2exzd2it1923za2zzast_walkz00(obj_t
		BgL_envz00_8015, obj_t BgL_nz00_8016, obj_t BgL_pz00_8017,
		obj_t BgL_arg0z00_8018)
	{
		{	/* Ast/walk.scm 304 */
			{
				BgL_nodez00_bglt BgL_auxz00_13546;

				{	/* Ast/walk.scm 304 */
					BgL_nodez00_bglt BgL_arg2864z00_9318;

					BgL_arg2864z00_9318 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8016)))->BgL_bodyz00);
					BgL_auxz00_13546 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_8017,
							((obj_t) BgL_arg2864z00_9318), BgL_arg0z00_8018));
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8016)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13546), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_13558;

				{	/* Ast/walk.scm 304 */
					BgL_nodez00_bglt BgL_arg2870z00_9319;

					BgL_arg2870z00_9319 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8016)))->BgL_onexitz00);
					BgL_auxz00_13558 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_8017,
							((obj_t) BgL_arg2870z00_9319), BgL_arg0z00_8018));
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8016)))->BgL_onexitz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13558), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8016));
		}

	}



/* &walk0!-set-ex-it1921 */
	BgL_nodez00_bglt BGl_z62walk0z12zd2setzd2exzd2it1921za2zzast_walkz00(obj_t
		BgL_envz00_8019, obj_t BgL_nz00_8020, obj_t BgL_pz00_8021)
	{
		{	/* Ast/walk.scm 304 */
			{
				BgL_nodez00_bglt BgL_auxz00_13572;

				{	/* Ast/walk.scm 304 */
					BgL_nodez00_bglt BgL_arg2858z00_9321;

					BgL_arg2858z00_9321 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8020)))->BgL_bodyz00);
					BgL_auxz00_13572 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_8021, ((obj_t) BgL_arg2858z00_9321)));
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8020)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13572), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_13583;

				{	/* Ast/walk.scm 304 */
					BgL_nodez00_bglt BgL_arg2859z00_9322;

					BgL_arg2859z00_9322 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8020)))->BgL_onexitz00);
					BgL_auxz00_13583 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_8021, ((obj_t) BgL_arg2859z00_9322)));
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8020)))->BgL_onexitz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13583), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8020));
		}

	}



/* &walk3*-set-ex-it1919 */
	obj_t BGl_z62walk3za2zd2setzd2exzd2it1919z12zzast_walkz00(obj_t
		BgL_envz00_8022, obj_t BgL_nz00_8023, obj_t BgL_pz00_8024,
		obj_t BgL_arg0z00_8025, obj_t BgL_arg1z00_8026, obj_t BgL_arg2z00_8027)
	{
		{	/* Ast/walk.scm 304 */
			{	/* Ast/walk.scm 304 */
				obj_t BgL_res4068z00_9328;

				{	/* Ast/walk.scm 304 */
					obj_t BgL_arg2846z00_9324;
					obj_t BgL_arg2847z00_9325;

					{	/* Ast/walk.scm 304 */
						BgL_nodez00_bglt BgL_arg2848z00_9326;

						BgL_arg2848z00_9326 =
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
									((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8023)))->BgL_bodyz00);
						BgL_arg2846z00_9324 =
							BGL_PROCEDURE_CALL4(BgL_pz00_8024,
							((obj_t) BgL_arg2848z00_9326), BgL_arg0z00_8025, BgL_arg1z00_8026,
							BgL_arg2z00_8027);
					}
					{	/* Ast/walk.scm 304 */
						BgL_nodez00_bglt BgL_arg2856z00_9327;

						BgL_arg2856z00_9327 =
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
									((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8023)))->BgL_onexitz00);
						BgL_arg2847z00_9325 =
							BGL_PROCEDURE_CALL4(BgL_pz00_8024,
							((obj_t) BgL_arg2856z00_9327), BgL_arg0z00_8025, BgL_arg1z00_8026,
							BgL_arg2z00_8027);
					}
					BgL_res4068z00_9328 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg2846z00_9324,
						BgL_arg2847z00_9325);
				}
				return BgL_res4068z00_9328;
			}
		}

	}



/* &walk2*-set-ex-it1917 */
	obj_t BGl_z62walk2za2zd2setzd2exzd2it1917z12zzast_walkz00(obj_t
		BgL_envz00_8028, obj_t BgL_nz00_8029, obj_t BgL_pz00_8030,
		obj_t BgL_arg0z00_8031, obj_t BgL_arg1z00_8032)
	{
		{	/* Ast/walk.scm 304 */
			{	/* Ast/walk.scm 304 */
				obj_t BgL_res4069z00_9334;

				{	/* Ast/walk.scm 304 */
					obj_t BgL_arg2837z00_9330;
					obj_t BgL_arg2838z00_9331;

					{	/* Ast/walk.scm 304 */
						BgL_nodez00_bglt BgL_arg2839z00_9332;

						BgL_arg2839z00_9332 =
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
									((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8029)))->BgL_bodyz00);
						BgL_arg2837z00_9330 =
							BGL_PROCEDURE_CALL3(BgL_pz00_8030,
							((obj_t) BgL_arg2839z00_9332), BgL_arg0z00_8031,
							BgL_arg1z00_8032);
					}
					{	/* Ast/walk.scm 304 */
						BgL_nodez00_bglt BgL_arg2844z00_9333;

						BgL_arg2844z00_9333 =
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
									((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8029)))->BgL_onexitz00);
						BgL_arg2838z00_9331 =
							BGL_PROCEDURE_CALL3(BgL_pz00_8030,
							((obj_t) BgL_arg2844z00_9333), BgL_arg0z00_8031,
							BgL_arg1z00_8032);
					}
					BgL_res4069z00_9334 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg2837z00_9330,
						BgL_arg2838z00_9331);
				}
				return BgL_res4069z00_9334;
			}
		}

	}



/* &walk1*-set-ex-it1915 */
	obj_t BGl_z62walk1za2zd2setzd2exzd2it1915z12zzast_walkz00(obj_t
		BgL_envz00_8033, obj_t BgL_nz00_8034, obj_t BgL_pz00_8035,
		obj_t BgL_arg0z00_8036)
	{
		{	/* Ast/walk.scm 304 */
			{	/* Ast/walk.scm 304 */
				obj_t BgL_res4070z00_9340;

				{	/* Ast/walk.scm 304 */
					obj_t BgL_arg2833z00_9336;
					obj_t BgL_arg2834z00_9337;

					{	/* Ast/walk.scm 304 */
						BgL_nodez00_bglt BgL_arg2835z00_9338;

						BgL_arg2835z00_9338 =
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
									((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8034)))->BgL_bodyz00);
						BgL_arg2833z00_9336 =
							BGL_PROCEDURE_CALL2(BgL_pz00_8035,
							((obj_t) BgL_arg2835z00_9338), BgL_arg0z00_8036);
					}
					{	/* Ast/walk.scm 304 */
						BgL_nodez00_bglt BgL_arg2836z00_9339;

						BgL_arg2836z00_9339 =
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
									((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8034)))->BgL_onexitz00);
						BgL_arg2834z00_9337 =
							BGL_PROCEDURE_CALL2(BgL_pz00_8035,
							((obj_t) BgL_arg2836z00_9339), BgL_arg0z00_8036);
					}
					BgL_res4070z00_9340 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg2833z00_9336,
						BgL_arg2834z00_9337);
				}
				return BgL_res4070z00_9340;
			}
		}

	}



/* &walk0*-set-ex-it1913 */
	obj_t BGl_z62walk0za2zd2setzd2exzd2it1913z12zzast_walkz00(obj_t
		BgL_envz00_8037, obj_t BgL_nz00_8038, obj_t BgL_pz00_8039)
	{
		{	/* Ast/walk.scm 304 */
			{	/* Ast/walk.scm 304 */
				obj_t BgL_res4071z00_9346;

				{	/* Ast/walk.scm 304 */
					obj_t BgL_arg2829z00_9342;
					obj_t BgL_arg2830z00_9343;

					{	/* Ast/walk.scm 304 */
						BgL_nodez00_bglt BgL_arg2831z00_9344;

						BgL_arg2831z00_9344 =
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
									((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8038)))->BgL_bodyz00);
						BgL_arg2829z00_9342 =
							BGL_PROCEDURE_CALL1(BgL_pz00_8039, ((obj_t) BgL_arg2831z00_9344));
					}
					{	/* Ast/walk.scm 304 */
						BgL_nodez00_bglt BgL_arg2832z00_9345;

						BgL_arg2832z00_9345 =
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
									((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8038)))->BgL_onexitz00);
						BgL_arg2830z00_9343 =
							BGL_PROCEDURE_CALL1(BgL_pz00_8039, ((obj_t) BgL_arg2832z00_9345));
					}
					BgL_res4071z00_9346 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg2829z00_9342,
						BgL_arg2830z00_9343);
				}
				return BgL_res4071z00_9346;
			}
		}

	}



/* &walk3-set-ex-it1911 */
	obj_t BGl_z62walk3zd2setzd2exzd2it1911zb0zzast_walkz00(obj_t BgL_envz00_8040,
		obj_t BgL_nz00_8041, obj_t BgL_pz00_8042, obj_t BgL_arg0z00_8043,
		obj_t BgL_arg1z00_8044, obj_t BgL_arg2z00_8045)
	{
		{	/* Ast/walk.scm 304 */
			{	/* Ast/walk.scm 304 */
				BgL_nodez00_bglt BgL_arg2827z00_9348;

				BgL_arg2827z00_9348 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8041)))->BgL_bodyz00);
				BGL_PROCEDURE_CALL4(BgL_pz00_8042,
					((obj_t) BgL_arg2827z00_9348), BgL_arg0z00_8043, BgL_arg1z00_8044,
					BgL_arg2z00_8045);
			}
			{	/* Ast/walk.scm 304 */
				BgL_nodez00_bglt BgL_arg2828z00_9349;

				BgL_arg2828z00_9349 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8041)))->BgL_onexitz00);
				return
					BGL_PROCEDURE_CALL4(BgL_pz00_8042,
					((obj_t) BgL_arg2828z00_9349), BgL_arg0z00_8043, BgL_arg1z00_8044,
					BgL_arg2z00_8045);
			}
		}

	}



/* &walk2-set-ex-it1909 */
	obj_t BGl_z62walk2zd2setzd2exzd2it1909zb0zzast_walkz00(obj_t BgL_envz00_8046,
		obj_t BgL_nz00_8047, obj_t BgL_pz00_8048, obj_t BgL_arg0z00_8049,
		obj_t BgL_arg1z00_8050)
	{
		{	/* Ast/walk.scm 304 */
			{	/* Ast/walk.scm 304 */
				BgL_nodez00_bglt BgL_arg2824z00_9351;

				BgL_arg2824z00_9351 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8047)))->BgL_bodyz00);
				BGL_PROCEDURE_CALL3(BgL_pz00_8048,
					((obj_t) BgL_arg2824z00_9351), BgL_arg0z00_8049, BgL_arg1z00_8050);
			}
			{	/* Ast/walk.scm 304 */
				BgL_nodez00_bglt BgL_arg2826z00_9352;

				BgL_arg2826z00_9352 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8047)))->BgL_onexitz00);
				return
					BGL_PROCEDURE_CALL3(BgL_pz00_8048,
					((obj_t) BgL_arg2826z00_9352), BgL_arg0z00_8049, BgL_arg1z00_8050);
			}
		}

	}



/* &walk1-set-ex-it1907 */
	obj_t BGl_z62walk1zd2setzd2exzd2it1907zb0zzast_walkz00(obj_t BgL_envz00_8051,
		obj_t BgL_nz00_8052, obj_t BgL_pz00_8053, obj_t BgL_arg0z00_8054)
	{
		{	/* Ast/walk.scm 304 */
			{	/* Ast/walk.scm 304 */
				BgL_nodez00_bglt BgL_arg2821z00_9354;

				BgL_arg2821z00_9354 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8052)))->BgL_bodyz00);
				BGL_PROCEDURE_CALL2(BgL_pz00_8053,
					((obj_t) BgL_arg2821z00_9354), BgL_arg0z00_8054);
			}
			{	/* Ast/walk.scm 304 */
				BgL_nodez00_bglt BgL_arg2823z00_9355;

				BgL_arg2823z00_9355 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8052)))->BgL_onexitz00);
				return
					BGL_PROCEDURE_CALL2(BgL_pz00_8053,
					((obj_t) BgL_arg2823z00_9355), BgL_arg0z00_8054);
			}
		}

	}



/* &walk0-set-ex-it1905 */
	obj_t BGl_z62walk0zd2setzd2exzd2it1905zb0zzast_walkz00(obj_t BgL_envz00_8055,
		obj_t BgL_nz00_8056, obj_t BgL_pz00_8057)
	{
		{	/* Ast/walk.scm 304 */
			{	/* Ast/walk.scm 304 */
				BgL_nodez00_bglt BgL_arg2818z00_9357;

				BgL_arg2818z00_9357 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8056)))->BgL_bodyz00);
				BGL_PROCEDURE_CALL1(BgL_pz00_8057, ((obj_t) BgL_arg2818z00_9357));
			}
			{	/* Ast/walk.scm 304 */
				BgL_nodez00_bglt BgL_arg2820z00_9358;

				BgL_arg2820z00_9358 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nz00_8056)))->BgL_onexitz00);
				return
					BGL_PROCEDURE_CALL1(BgL_pz00_8057, ((obj_t) BgL_arg2820z00_9358));
			}
		}

	}



/* &walk3!-fail1903 */
	BgL_nodez00_bglt BGl_z62walk3z12zd2fail1903za2zzast_walkz00(obj_t
		BgL_envz00_8058, obj_t BgL_nz00_8059, obj_t BgL_pz00_8060,
		obj_t BgL_arg0z00_8061, obj_t BgL_arg1z00_8062, obj_t BgL_arg2z00_8063)
	{
		{	/* Ast/walk.scm 303 */
			{
				BgL_nodez00_bglt BgL_auxz00_13736;

				{	/* Ast/walk.scm 303 */
					BgL_nodez00_bglt BgL_arg2812z00_9360;

					BgL_arg2812z00_9360 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nz00_8059)))->BgL_procz00);
					BgL_auxz00_13736 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_8060,
							((obj_t) BgL_arg2812z00_9360), BgL_arg0z00_8061, BgL_arg1z00_8062,
							BgL_arg2z00_8063));
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nz00_8059)))->BgL_procz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13736), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_13750;

				{	/* Ast/walk.scm 303 */
					BgL_nodez00_bglt BgL_arg2815z00_9361;

					BgL_arg2815z00_9361 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nz00_8059)))->BgL_msgz00);
					BgL_auxz00_13750 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_8060,
							((obj_t) BgL_arg2815z00_9361), BgL_arg0z00_8061, BgL_arg1z00_8062,
							BgL_arg2z00_8063));
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nz00_8059)))->BgL_msgz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13750), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_13764;

				{	/* Ast/walk.scm 303 */
					BgL_nodez00_bglt BgL_arg2816z00_9362;

					BgL_arg2816z00_9362 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nz00_8059)))->BgL_objz00);
					BgL_auxz00_13764 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_8060,
							((obj_t) BgL_arg2816z00_9362), BgL_arg0z00_8061, BgL_arg1z00_8062,
							BgL_arg2z00_8063));
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nz00_8059)))->BgL_objz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13764), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_failz00_bglt) BgL_nz00_8059));
		}

	}



/* &walk2!-fail1901 */
	BgL_nodez00_bglt BGl_z62walk2z12zd2fail1901za2zzast_walkz00(obj_t
		BgL_envz00_8064, obj_t BgL_nz00_8065, obj_t BgL_pz00_8066,
		obj_t BgL_arg0z00_8067, obj_t BgL_arg1z00_8068)
	{
		{	/* Ast/walk.scm 303 */
			{
				BgL_nodez00_bglt BgL_auxz00_13780;

				{	/* Ast/walk.scm 303 */
					BgL_nodez00_bglt BgL_arg2808z00_9364;

					BgL_arg2808z00_9364 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nz00_8065)))->BgL_procz00);
					BgL_auxz00_13780 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_8066,
							((obj_t) BgL_arg2808z00_9364), BgL_arg0z00_8067,
							BgL_arg1z00_8068));
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nz00_8065)))->BgL_procz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13780), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_13793;

				{	/* Ast/walk.scm 303 */
					BgL_nodez00_bglt BgL_arg2809z00_9365;

					BgL_arg2809z00_9365 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nz00_8065)))->BgL_msgz00);
					BgL_auxz00_13793 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_8066,
							((obj_t) BgL_arg2809z00_9365), BgL_arg0z00_8067,
							BgL_arg1z00_8068));
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nz00_8065)))->BgL_msgz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13793), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_13806;

				{	/* Ast/walk.scm 303 */
					BgL_nodez00_bglt BgL_arg2811z00_9366;

					BgL_arg2811z00_9366 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nz00_8065)))->BgL_objz00);
					BgL_auxz00_13806 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_8066,
							((obj_t) BgL_arg2811z00_9366), BgL_arg0z00_8067,
							BgL_arg1z00_8068));
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nz00_8065)))->BgL_objz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13806), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_failz00_bglt) BgL_nz00_8065));
		}

	}



/* &walk1!-fail1899 */
	BgL_nodez00_bglt BGl_z62walk1z12zd2fail1899za2zzast_walkz00(obj_t
		BgL_envz00_8069, obj_t BgL_nz00_8070, obj_t BgL_pz00_8071,
		obj_t BgL_arg0z00_8072)
	{
		{	/* Ast/walk.scm 303 */
			{
				BgL_nodez00_bglt BgL_auxz00_13821;

				{	/* Ast/walk.scm 303 */
					BgL_nodez00_bglt BgL_arg2800z00_9368;

					BgL_arg2800z00_9368 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nz00_8070)))->BgL_procz00);
					BgL_auxz00_13821 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_8071,
							((obj_t) BgL_arg2800z00_9368), BgL_arg0z00_8072));
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nz00_8070)))->BgL_procz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13821), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_13833;

				{	/* Ast/walk.scm 303 */
					BgL_nodez00_bglt BgL_arg2804z00_9369;

					BgL_arg2804z00_9369 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nz00_8070)))->BgL_msgz00);
					BgL_auxz00_13833 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_8071,
							((obj_t) BgL_arg2804z00_9369), BgL_arg0z00_8072));
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nz00_8070)))->BgL_msgz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13833), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_13845;

				{	/* Ast/walk.scm 303 */
					BgL_nodez00_bglt BgL_arg2805z00_9370;

					BgL_arg2805z00_9370 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nz00_8070)))->BgL_objz00);
					BgL_auxz00_13845 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_8071,
							((obj_t) BgL_arg2805z00_9370), BgL_arg0z00_8072));
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nz00_8070)))->BgL_objz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13845), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_failz00_bglt) BgL_nz00_8070));
		}

	}



/* &walk0!-fail1897 */
	BgL_nodez00_bglt BGl_z62walk0z12zd2fail1897za2zzast_walkz00(obj_t
		BgL_envz00_8073, obj_t BgL_nz00_8074, obj_t BgL_pz00_8075)
	{
		{	/* Ast/walk.scm 303 */
			{
				BgL_nodez00_bglt BgL_auxz00_13859;

				{	/* Ast/walk.scm 303 */
					BgL_nodez00_bglt BgL_arg2793z00_9372;

					BgL_arg2793z00_9372 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nz00_8074)))->BgL_procz00);
					BgL_auxz00_13859 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_8075, ((obj_t) BgL_arg2793z00_9372)));
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nz00_8074)))->BgL_procz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13859), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_13870;

				{	/* Ast/walk.scm 303 */
					BgL_nodez00_bglt BgL_arg2794z00_9373;

					BgL_arg2794z00_9373 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nz00_8074)))->BgL_msgz00);
					BgL_auxz00_13870 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_8075, ((obj_t) BgL_arg2794z00_9373)));
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nz00_8074)))->BgL_msgz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13870), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_13881;

				{	/* Ast/walk.scm 303 */
					BgL_nodez00_bglt BgL_arg2799z00_9374;

					BgL_arg2799z00_9374 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nz00_8074)))->BgL_objz00);
					BgL_auxz00_13881 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_8075, ((obj_t) BgL_arg2799z00_9374)));
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nz00_8074)))->BgL_objz00) =
					((BgL_nodez00_bglt) BgL_auxz00_13881), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_failz00_bglt) BgL_nz00_8074));
		}

	}



/* &walk3*-fail1895 */
	obj_t BGl_z62walk3za2zd2fail1895z12zzast_walkz00(obj_t BgL_envz00_8076,
		obj_t BgL_nz00_8077, obj_t BgL_pz00_8078, obj_t BgL_arg0z00_8079,
		obj_t BgL_arg1z00_8080, obj_t BgL_arg2z00_8081)
	{
		{	/* Ast/walk.scm 303 */
			{	/* Ast/walk.scm 303 */
				obj_t BgL_res4072z00_9385;

				{	/* Ast/walk.scm 303 */
					obj_t BgL_arg2777z00_9376;
					obj_t BgL_arg2778z00_9377;
					obj_t BgL_arg2780z00_9378;

					{	/* Ast/walk.scm 303 */
						BgL_nodez00_bglt BgL_arg2786z00_9379;

						BgL_arg2786z00_9379 =
							(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nz00_8077)))->BgL_procz00);
						BgL_arg2777z00_9376 =
							BGL_PROCEDURE_CALL4(BgL_pz00_8078,
							((obj_t) BgL_arg2786z00_9379), BgL_arg0z00_8079, BgL_arg1z00_8080,
							BgL_arg2z00_8081);
					}
					{	/* Ast/walk.scm 303 */
						BgL_nodez00_bglt BgL_arg2787z00_9380;

						BgL_arg2787z00_9380 =
							(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nz00_8077)))->BgL_msgz00);
						BgL_arg2778z00_9377 =
							BGL_PROCEDURE_CALL4(BgL_pz00_8078,
							((obj_t) BgL_arg2787z00_9380), BgL_arg0z00_8079, BgL_arg1z00_8080,
							BgL_arg2z00_8081);
					}
					{	/* Ast/walk.scm 303 */
						BgL_nodez00_bglt BgL_arg2789z00_9381;

						BgL_arg2789z00_9381 =
							(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nz00_8077)))->BgL_objz00);
						BgL_arg2780z00_9378 =
							BGL_PROCEDURE_CALL4(BgL_pz00_8078,
							((obj_t) BgL_arg2789z00_9381), BgL_arg0z00_8079, BgL_arg1z00_8080,
							BgL_arg2z00_8081);
					}
					{	/* Ast/walk.scm 303 */
						obj_t BgL_list2781z00_9382;

						{	/* Ast/walk.scm 303 */
							obj_t BgL_arg2783z00_9383;

							{	/* Ast/walk.scm 303 */
								obj_t BgL_arg2784z00_9384;

								BgL_arg2784z00_9384 =
									MAKE_YOUNG_PAIR(BgL_arg2780z00_9378, BNIL);
								BgL_arg2783z00_9383 =
									MAKE_YOUNG_PAIR(BgL_arg2778z00_9377, BgL_arg2784z00_9384);
							}
							BgL_list2781z00_9382 =
								MAKE_YOUNG_PAIR(BgL_arg2777z00_9376, BgL_arg2783z00_9383);
						}
						BgL_res4072z00_9385 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2781z00_9382);
					}
				}
				return BgL_res4072z00_9385;
			}
		}

	}



/* &walk2*-fail1893 */
	obj_t BGl_z62walk2za2zd2fail1893z12zzast_walkz00(obj_t BgL_envz00_8082,
		obj_t BgL_nz00_8083, obj_t BgL_pz00_8084, obj_t BgL_arg0z00_8085,
		obj_t BgL_arg1z00_8086)
	{
		{	/* Ast/walk.scm 303 */
			{	/* Ast/walk.scm 303 */
				obj_t BgL_res4073z00_9396;

				{	/* Ast/walk.scm 303 */
					obj_t BgL_arg2764z00_9387;
					obj_t BgL_arg2765z00_9388;
					obj_t BgL_arg2766z00_9389;

					{	/* Ast/walk.scm 303 */
						BgL_nodez00_bglt BgL_arg2773z00_9390;

						BgL_arg2773z00_9390 =
							(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nz00_8083)))->BgL_procz00);
						BgL_arg2764z00_9387 =
							BGL_PROCEDURE_CALL3(BgL_pz00_8084,
							((obj_t) BgL_arg2773z00_9390), BgL_arg0z00_8085,
							BgL_arg1z00_8086);
					}
					{	/* Ast/walk.scm 303 */
						BgL_nodez00_bglt BgL_arg2774z00_9391;

						BgL_arg2774z00_9391 =
							(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nz00_8083)))->BgL_msgz00);
						BgL_arg2765z00_9388 =
							BGL_PROCEDURE_CALL3(BgL_pz00_8084,
							((obj_t) BgL_arg2774z00_9391), BgL_arg0z00_8085,
							BgL_arg1z00_8086);
					}
					{	/* Ast/walk.scm 303 */
						BgL_nodez00_bglt BgL_arg2776z00_9392;

						BgL_arg2776z00_9392 =
							(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nz00_8083)))->BgL_objz00);
						BgL_arg2766z00_9389 =
							BGL_PROCEDURE_CALL3(BgL_pz00_8084,
							((obj_t) BgL_arg2776z00_9392), BgL_arg0z00_8085,
							BgL_arg1z00_8086);
					}
					{	/* Ast/walk.scm 303 */
						obj_t BgL_list2767z00_9393;

						{	/* Ast/walk.scm 303 */
							obj_t BgL_arg2771z00_9394;

							{	/* Ast/walk.scm 303 */
								obj_t BgL_arg2772z00_9395;

								BgL_arg2772z00_9395 =
									MAKE_YOUNG_PAIR(BgL_arg2766z00_9389, BNIL);
								BgL_arg2771z00_9394 =
									MAKE_YOUNG_PAIR(BgL_arg2765z00_9388, BgL_arg2772z00_9395);
							}
							BgL_list2767z00_9393 =
								MAKE_YOUNG_PAIR(BgL_arg2764z00_9387, BgL_arg2771z00_9394);
						}
						BgL_res4073z00_9396 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2767z00_9393);
					}
				}
				return BgL_res4073z00_9396;
			}
		}

	}



/* &walk1*-fail1891 */
	obj_t BGl_z62walk1za2zd2fail1891z12zzast_walkz00(obj_t BgL_envz00_8087,
		obj_t BgL_nz00_8088, obj_t BgL_pz00_8089, obj_t BgL_arg0z00_8090)
	{
		{	/* Ast/walk.scm 303 */
			{	/* Ast/walk.scm 303 */
				obj_t BgL_res4074z00_9407;

				{	/* Ast/walk.scm 303 */
					obj_t BgL_arg2754z00_9398;
					obj_t BgL_arg2755z00_9399;
					obj_t BgL_arg2756z00_9400;

					{	/* Ast/walk.scm 303 */
						BgL_nodez00_bglt BgL_arg2760z00_9401;

						BgL_arg2760z00_9401 =
							(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nz00_8088)))->BgL_procz00);
						BgL_arg2754z00_9398 =
							BGL_PROCEDURE_CALL2(BgL_pz00_8089,
							((obj_t) BgL_arg2760z00_9401), BgL_arg0z00_8090);
					}
					{	/* Ast/walk.scm 303 */
						BgL_nodez00_bglt BgL_arg2761z00_9402;

						BgL_arg2761z00_9402 =
							(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nz00_8088)))->BgL_msgz00);
						BgL_arg2755z00_9399 =
							BGL_PROCEDURE_CALL2(BgL_pz00_8089,
							((obj_t) BgL_arg2761z00_9402), BgL_arg0z00_8090);
					}
					{	/* Ast/walk.scm 303 */
						BgL_nodez00_bglt BgL_arg2762z00_9403;

						BgL_arg2762z00_9403 =
							(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nz00_8088)))->BgL_objz00);
						BgL_arg2756z00_9400 =
							BGL_PROCEDURE_CALL2(BgL_pz00_8089,
							((obj_t) BgL_arg2762z00_9403), BgL_arg0z00_8090);
					}
					{	/* Ast/walk.scm 303 */
						obj_t BgL_list2757z00_9404;

						{	/* Ast/walk.scm 303 */
							obj_t BgL_arg2758z00_9405;

							{	/* Ast/walk.scm 303 */
								obj_t BgL_arg2759z00_9406;

								BgL_arg2759z00_9406 =
									MAKE_YOUNG_PAIR(BgL_arg2756z00_9400, BNIL);
								BgL_arg2758z00_9405 =
									MAKE_YOUNG_PAIR(BgL_arg2755z00_9399, BgL_arg2759z00_9406);
							}
							BgL_list2757z00_9404 =
								MAKE_YOUNG_PAIR(BgL_arg2754z00_9398, BgL_arg2758z00_9405);
						}
						BgL_res4074z00_9407 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2757z00_9404);
					}
				}
				return BgL_res4074z00_9407;
			}
		}

	}



/* &walk0*-fail1889 */
	obj_t BGl_z62walk0za2zd2fail1889z12zzast_walkz00(obj_t BgL_envz00_8091,
		obj_t BgL_nz00_8092, obj_t BgL_pz00_8093)
	{
		{	/* Ast/walk.scm 303 */
			{	/* Ast/walk.scm 303 */
				obj_t BgL_res4075z00_9418;

				{	/* Ast/walk.scm 303 */
					obj_t BgL_arg2743z00_9409;
					obj_t BgL_arg2744z00_9410;
					obj_t BgL_arg2746z00_9411;

					{	/* Ast/walk.scm 303 */
						BgL_nodez00_bglt BgL_arg2750z00_9412;

						BgL_arg2750z00_9412 =
							(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nz00_8092)))->BgL_procz00);
						BgL_arg2743z00_9409 =
							BGL_PROCEDURE_CALL1(BgL_pz00_8093, ((obj_t) BgL_arg2750z00_9412));
					}
					{	/* Ast/walk.scm 303 */
						BgL_nodez00_bglt BgL_arg2751z00_9413;

						BgL_arg2751z00_9413 =
							(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nz00_8092)))->BgL_msgz00);
						BgL_arg2744z00_9410 =
							BGL_PROCEDURE_CALL1(BgL_pz00_8093, ((obj_t) BgL_arg2751z00_9413));
					}
					{	/* Ast/walk.scm 303 */
						BgL_nodez00_bglt BgL_arg2753z00_9414;

						BgL_arg2753z00_9414 =
							(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nz00_8092)))->BgL_objz00);
						BgL_arg2746z00_9411 =
							BGL_PROCEDURE_CALL1(BgL_pz00_8093, ((obj_t) BgL_arg2753z00_9414));
					}
					{	/* Ast/walk.scm 303 */
						obj_t BgL_list2747z00_9415;

						{	/* Ast/walk.scm 303 */
							obj_t BgL_arg2748z00_9416;

							{	/* Ast/walk.scm 303 */
								obj_t BgL_arg2749z00_9417;

								BgL_arg2749z00_9417 =
									MAKE_YOUNG_PAIR(BgL_arg2746z00_9411, BNIL);
								BgL_arg2748z00_9416 =
									MAKE_YOUNG_PAIR(BgL_arg2744z00_9410, BgL_arg2749z00_9417);
							}
							BgL_list2747z00_9415 =
								MAKE_YOUNG_PAIR(BgL_arg2743z00_9409, BgL_arg2748z00_9416);
						}
						BgL_res4075z00_9418 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2747z00_9415);
					}
				}
				return BgL_res4075z00_9418;
			}
		}

	}



/* &walk3-fail1887 */
	obj_t BGl_z62walk3zd2fail1887zb0zzast_walkz00(obj_t BgL_envz00_8094,
		obj_t BgL_nz00_8095, obj_t BgL_pz00_8096, obj_t BgL_arg0z00_8097,
		obj_t BgL_arg1z00_8098, obj_t BgL_arg2z00_8099)
	{
		{	/* Ast/walk.scm 303 */
			{	/* Ast/walk.scm 303 */
				BgL_nodez00_bglt BgL_arg2740z00_9420;

				BgL_arg2740z00_9420 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nz00_8095)))->BgL_procz00);
				BGL_PROCEDURE_CALL4(BgL_pz00_8096,
					((obj_t) BgL_arg2740z00_9420), BgL_arg0z00_8097, BgL_arg1z00_8098,
					BgL_arg2z00_8099);
			}
			{	/* Ast/walk.scm 303 */
				BgL_nodez00_bglt BgL_arg2741z00_9421;

				BgL_arg2741z00_9421 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nz00_8095)))->BgL_msgz00);
				BGL_PROCEDURE_CALL4(BgL_pz00_8096,
					((obj_t) BgL_arg2741z00_9421), BgL_arg0z00_8097, BgL_arg1z00_8098,
					BgL_arg2z00_8099);
			}
			{	/* Ast/walk.scm 303 */
				BgL_nodez00_bglt BgL_arg2742z00_9422;

				BgL_arg2742z00_9422 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nz00_8095)))->BgL_objz00);
				return
					BGL_PROCEDURE_CALL4(BgL_pz00_8096,
					((obj_t) BgL_arg2742z00_9422), BgL_arg0z00_8097, BgL_arg1z00_8098,
					BgL_arg2z00_8099);
			}
		}

	}



/* &walk2-fail1885 */
	obj_t BGl_z62walk2zd2fail1885zb0zzast_walkz00(obj_t BgL_envz00_8100,
		obj_t BgL_nz00_8101, obj_t BgL_pz00_8102, obj_t BgL_arg0z00_8103,
		obj_t BgL_arg1z00_8104)
	{
		{	/* Ast/walk.scm 303 */
			{	/* Ast/walk.scm 303 */
				BgL_nodez00_bglt BgL_arg2737z00_9424;

				BgL_arg2737z00_9424 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nz00_8101)))->BgL_procz00);
				BGL_PROCEDURE_CALL3(BgL_pz00_8102,
					((obj_t) BgL_arg2737z00_9424), BgL_arg0z00_8103, BgL_arg1z00_8104);
			}
			{	/* Ast/walk.scm 303 */
				BgL_nodez00_bglt BgL_arg2738z00_9425;

				BgL_arg2738z00_9425 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nz00_8101)))->BgL_msgz00);
				BGL_PROCEDURE_CALL3(BgL_pz00_8102,
					((obj_t) BgL_arg2738z00_9425), BgL_arg0z00_8103, BgL_arg1z00_8104);
			}
			{	/* Ast/walk.scm 303 */
				BgL_nodez00_bglt BgL_arg2739z00_9426;

				BgL_arg2739z00_9426 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nz00_8101)))->BgL_objz00);
				return
					BGL_PROCEDURE_CALL3(BgL_pz00_8102,
					((obj_t) BgL_arg2739z00_9426), BgL_arg0z00_8103, BgL_arg1z00_8104);
			}
		}

	}



/* &walk1-fail1883 */
	obj_t BGl_z62walk1zd2fail1883zb0zzast_walkz00(obj_t BgL_envz00_8105,
		obj_t BgL_nz00_8106, obj_t BgL_pz00_8107, obj_t BgL_arg0z00_8108)
	{
		{	/* Ast/walk.scm 303 */
			{	/* Ast/walk.scm 303 */
				BgL_nodez00_bglt BgL_arg2733z00_9428;

				BgL_arg2733z00_9428 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nz00_8106)))->BgL_procz00);
				BGL_PROCEDURE_CALL2(BgL_pz00_8107,
					((obj_t) BgL_arg2733z00_9428), BgL_arg0z00_8108);
			}
			{	/* Ast/walk.scm 303 */
				BgL_nodez00_bglt BgL_arg2734z00_9429;

				BgL_arg2734z00_9429 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nz00_8106)))->BgL_msgz00);
				BGL_PROCEDURE_CALL2(BgL_pz00_8107,
					((obj_t) BgL_arg2734z00_9429), BgL_arg0z00_8108);
			}
			{	/* Ast/walk.scm 303 */
				BgL_nodez00_bglt BgL_arg2736z00_9430;

				BgL_arg2736z00_9430 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nz00_8106)))->BgL_objz00);
				return
					BGL_PROCEDURE_CALL2(BgL_pz00_8107,
					((obj_t) BgL_arg2736z00_9430), BgL_arg0z00_8108);
			}
		}

	}



/* &walk0-fail1881 */
	obj_t BGl_z62walk0zd2fail1881zb0zzast_walkz00(obj_t BgL_envz00_8109,
		obj_t BgL_nz00_8110, obj_t BgL_pz00_8111)
	{
		{	/* Ast/walk.scm 303 */
			{	/* Ast/walk.scm 303 */
				BgL_nodez00_bglt BgL_arg2728z00_9432;

				BgL_arg2728z00_9432 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nz00_8110)))->BgL_procz00);
				BGL_PROCEDURE_CALL1(BgL_pz00_8111, ((obj_t) BgL_arg2728z00_9432));
			}
			{	/* Ast/walk.scm 303 */
				BgL_nodez00_bglt BgL_arg2729z00_9433;

				BgL_arg2729z00_9433 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nz00_8110)))->BgL_msgz00);
				BGL_PROCEDURE_CALL1(BgL_pz00_8111, ((obj_t) BgL_arg2729z00_9433));
			}
			{	/* Ast/walk.scm 303 */
				BgL_nodez00_bglt BgL_arg2731z00_9434;

				BgL_arg2731z00_9434 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nz00_8110)))->BgL_objz00);
				return
					BGL_PROCEDURE_CALL1(BgL_pz00_8111, ((obj_t) BgL_arg2731z00_9434));
			}
		}

	}



/* &walk3!-conditional1879 */
	BgL_nodez00_bglt BGl_z62walk3z12zd2conditional1879za2zzast_walkz00(obj_t
		BgL_envz00_8112, obj_t BgL_nz00_8113, obj_t BgL_pz00_8114,
		obj_t BgL_arg0z00_8115, obj_t BgL_arg1z00_8116, obj_t BgL_arg2z00_8117)
	{
		{	/* Ast/walk.scm 302 */
			{
				BgL_nodez00_bglt BgL_auxz00_14114;

				{	/* Ast/walk.scm 302 */
					BgL_nodez00_bglt BgL_arg2725z00_9436;

					BgL_arg2725z00_9436 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nz00_8113)))->BgL_testz00);
					BgL_auxz00_14114 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_8114,
							((obj_t) BgL_arg2725z00_9436), BgL_arg0z00_8115, BgL_arg1z00_8116,
							BgL_arg2z00_8117));
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nz00_8113)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_14114), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_14128;

				{	/* Ast/walk.scm 302 */
					BgL_nodez00_bglt BgL_arg2726z00_9437;

					BgL_arg2726z00_9437 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nz00_8113)))->BgL_truez00);
					BgL_auxz00_14128 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_8114,
							((obj_t) BgL_arg2726z00_9437), BgL_arg0z00_8115, BgL_arg1z00_8116,
							BgL_arg2z00_8117));
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nz00_8113)))->BgL_truez00) =
					((BgL_nodez00_bglt) BgL_auxz00_14128), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_14142;

				{	/* Ast/walk.scm 302 */
					BgL_nodez00_bglt BgL_arg2727z00_9438;

					BgL_arg2727z00_9438 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nz00_8113)))->BgL_falsez00);
					BgL_auxz00_14142 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_8114,
							((obj_t) BgL_arg2727z00_9438), BgL_arg0z00_8115, BgL_arg1z00_8116,
							BgL_arg2z00_8117));
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nz00_8113)))->BgL_falsez00) =
					((BgL_nodez00_bglt) BgL_auxz00_14142), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt) BgL_nz00_8113));
		}

	}



/* &walk2!-conditional1877 */
	BgL_nodez00_bglt BGl_z62walk2z12zd2conditional1877za2zzast_walkz00(obj_t
		BgL_envz00_8118, obj_t BgL_nz00_8119, obj_t BgL_pz00_8120,
		obj_t BgL_arg0z00_8121, obj_t BgL_arg1z00_8122)
	{
		{	/* Ast/walk.scm 302 */
			{
				BgL_nodez00_bglt BgL_auxz00_14158;

				{	/* Ast/walk.scm 302 */
					BgL_nodez00_bglt BgL_arg2722z00_9440;

					BgL_arg2722z00_9440 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nz00_8119)))->BgL_testz00);
					BgL_auxz00_14158 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_8120,
							((obj_t) BgL_arg2722z00_9440), BgL_arg0z00_8121,
							BgL_arg1z00_8122));
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nz00_8119)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_14158), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_14171;

				{	/* Ast/walk.scm 302 */
					BgL_nodez00_bglt BgL_arg2723z00_9441;

					BgL_arg2723z00_9441 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nz00_8119)))->BgL_truez00);
					BgL_auxz00_14171 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_8120,
							((obj_t) BgL_arg2723z00_9441), BgL_arg0z00_8121,
							BgL_arg1z00_8122));
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nz00_8119)))->BgL_truez00) =
					((BgL_nodez00_bglt) BgL_auxz00_14171), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_14184;

				{	/* Ast/walk.scm 302 */
					BgL_nodez00_bglt BgL_arg2724z00_9442;

					BgL_arg2724z00_9442 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nz00_8119)))->BgL_falsez00);
					BgL_auxz00_14184 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_8120,
							((obj_t) BgL_arg2724z00_9442), BgL_arg0z00_8121,
							BgL_arg1z00_8122));
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nz00_8119)))->BgL_falsez00) =
					((BgL_nodez00_bglt) BgL_auxz00_14184), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt) BgL_nz00_8119));
		}

	}



/* &walk1!-conditional1875 */
	BgL_nodez00_bglt BGl_z62walk1z12zd2conditional1875za2zzast_walkz00(obj_t
		BgL_envz00_8123, obj_t BgL_nz00_8124, obj_t BgL_pz00_8125,
		obj_t BgL_arg0z00_8126)
	{
		{	/* Ast/walk.scm 302 */
			{
				BgL_nodez00_bglt BgL_auxz00_14199;

				{	/* Ast/walk.scm 302 */
					BgL_nodez00_bglt BgL_arg2717z00_9444;

					BgL_arg2717z00_9444 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nz00_8124)))->BgL_testz00);
					BgL_auxz00_14199 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_8125,
							((obj_t) BgL_arg2717z00_9444), BgL_arg0z00_8126));
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nz00_8124)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_14199), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_14211;

				{	/* Ast/walk.scm 302 */
					BgL_nodez00_bglt BgL_arg2719z00_9445;

					BgL_arg2719z00_9445 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nz00_8124)))->BgL_truez00);
					BgL_auxz00_14211 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_8125,
							((obj_t) BgL_arg2719z00_9445), BgL_arg0z00_8126));
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nz00_8124)))->BgL_truez00) =
					((BgL_nodez00_bglt) BgL_auxz00_14211), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_14223;

				{	/* Ast/walk.scm 302 */
					BgL_nodez00_bglt BgL_arg2721z00_9446;

					BgL_arg2721z00_9446 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nz00_8124)))->BgL_falsez00);
					BgL_auxz00_14223 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_8125,
							((obj_t) BgL_arg2721z00_9446), BgL_arg0z00_8126));
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nz00_8124)))->BgL_falsez00) =
					((BgL_nodez00_bglt) BgL_auxz00_14223), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt) BgL_nz00_8124));
		}

	}



/* &walk0!-conditional1873 */
	BgL_nodez00_bglt BGl_z62walk0z12zd2conditional1873za2zzast_walkz00(obj_t
		BgL_envz00_8127, obj_t BgL_nz00_8128, obj_t BgL_pz00_8129)
	{
		{	/* Ast/walk.scm 302 */
			{
				BgL_nodez00_bglt BgL_auxz00_14237;

				{	/* Ast/walk.scm 302 */
					BgL_nodez00_bglt BgL_arg2714z00_9448;

					BgL_arg2714z00_9448 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nz00_8128)))->BgL_testz00);
					BgL_auxz00_14237 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_8129, ((obj_t) BgL_arg2714z00_9448)));
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nz00_8128)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_14237), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_14248;

				{	/* Ast/walk.scm 302 */
					BgL_nodez00_bglt BgL_arg2715z00_9449;

					BgL_arg2715z00_9449 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nz00_8128)))->BgL_truez00);
					BgL_auxz00_14248 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_8129, ((obj_t) BgL_arg2715z00_9449)));
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nz00_8128)))->BgL_truez00) =
					((BgL_nodez00_bglt) BgL_auxz00_14248), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_14259;

				{	/* Ast/walk.scm 302 */
					BgL_nodez00_bglt BgL_arg2716z00_9450;

					BgL_arg2716z00_9450 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nz00_8128)))->BgL_falsez00);
					BgL_auxz00_14259 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_8129, ((obj_t) BgL_arg2716z00_9450)));
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nz00_8128)))->BgL_falsez00) =
					((BgL_nodez00_bglt) BgL_auxz00_14259), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt) BgL_nz00_8128));
		}

	}



/* &walk3*-conditional1871 */
	obj_t BGl_z62walk3za2zd2conditional1871z12zzast_walkz00(obj_t BgL_envz00_8130,
		obj_t BgL_nz00_8131, obj_t BgL_pz00_8132, obj_t BgL_arg0z00_8133,
		obj_t BgL_arg1z00_8134, obj_t BgL_arg2z00_8135)
	{
		{	/* Ast/walk.scm 302 */
			{	/* Ast/walk.scm 302 */
				obj_t BgL_res4076z00_9461;

				{	/* Ast/walk.scm 302 */
					obj_t BgL_arg2704z00_9452;
					obj_t BgL_arg2705z00_9453;
					obj_t BgL_arg2706z00_9454;

					{	/* Ast/walk.scm 302 */
						BgL_nodez00_bglt BgL_arg2710z00_9455;

						BgL_arg2710z00_9455 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nz00_8131)))->BgL_testz00);
						BgL_arg2704z00_9452 =
							BGL_PROCEDURE_CALL4(BgL_pz00_8132,
							((obj_t) BgL_arg2710z00_9455), BgL_arg0z00_8133, BgL_arg1z00_8134,
							BgL_arg2z00_8135);
					}
					{	/* Ast/walk.scm 302 */
						BgL_nodez00_bglt BgL_arg2711z00_9456;

						BgL_arg2711z00_9456 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nz00_8131)))->BgL_truez00);
						BgL_arg2705z00_9453 =
							BGL_PROCEDURE_CALL4(BgL_pz00_8132,
							((obj_t) BgL_arg2711z00_9456), BgL_arg0z00_8133, BgL_arg1z00_8134,
							BgL_arg2z00_8135);
					}
					{	/* Ast/walk.scm 302 */
						BgL_nodez00_bglt BgL_arg2712z00_9457;

						BgL_arg2712z00_9457 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nz00_8131)))->BgL_falsez00);
						BgL_arg2706z00_9454 =
							BGL_PROCEDURE_CALL4(BgL_pz00_8132,
							((obj_t) BgL_arg2712z00_9457), BgL_arg0z00_8133, BgL_arg1z00_8134,
							BgL_arg2z00_8135);
					}
					{	/* Ast/walk.scm 302 */
						obj_t BgL_list2707z00_9458;

						{	/* Ast/walk.scm 302 */
							obj_t BgL_arg2708z00_9459;

							{	/* Ast/walk.scm 302 */
								obj_t BgL_arg2709z00_9460;

								BgL_arg2709z00_9460 =
									MAKE_YOUNG_PAIR(BgL_arg2706z00_9454, BNIL);
								BgL_arg2708z00_9459 =
									MAKE_YOUNG_PAIR(BgL_arg2705z00_9453, BgL_arg2709z00_9460);
							}
							BgL_list2707z00_9458 =
								MAKE_YOUNG_PAIR(BgL_arg2704z00_9452, BgL_arg2708z00_9459);
						}
						BgL_res4076z00_9461 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2707z00_9458);
					}
				}
				return BgL_res4076z00_9461;
			}
		}

	}



/* &walk2*-conditional1869 */
	obj_t BGl_z62walk2za2zd2conditional1869z12zzast_walkz00(obj_t BgL_envz00_8136,
		obj_t BgL_nz00_8137, obj_t BgL_pz00_8138, obj_t BgL_arg0z00_8139,
		obj_t BgL_arg1z00_8140)
	{
		{	/* Ast/walk.scm 302 */
			{	/* Ast/walk.scm 302 */
				obj_t BgL_res4077z00_9472;

				{	/* Ast/walk.scm 302 */
					obj_t BgL_arg2695z00_9463;
					obj_t BgL_arg2696z00_9464;
					obj_t BgL_arg2697z00_9465;

					{	/* Ast/walk.scm 302 */
						BgL_nodez00_bglt BgL_arg2701z00_9466;

						BgL_arg2701z00_9466 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nz00_8137)))->BgL_testz00);
						BgL_arg2695z00_9463 =
							BGL_PROCEDURE_CALL3(BgL_pz00_8138,
							((obj_t) BgL_arg2701z00_9466), BgL_arg0z00_8139,
							BgL_arg1z00_8140);
					}
					{	/* Ast/walk.scm 302 */
						BgL_nodez00_bglt BgL_arg2702z00_9467;

						BgL_arg2702z00_9467 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nz00_8137)))->BgL_truez00);
						BgL_arg2696z00_9464 =
							BGL_PROCEDURE_CALL3(BgL_pz00_8138,
							((obj_t) BgL_arg2702z00_9467), BgL_arg0z00_8139,
							BgL_arg1z00_8140);
					}
					{	/* Ast/walk.scm 302 */
						BgL_nodez00_bglt BgL_arg2703z00_9468;

						BgL_arg2703z00_9468 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nz00_8137)))->BgL_falsez00);
						BgL_arg2697z00_9465 =
							BGL_PROCEDURE_CALL3(BgL_pz00_8138,
							((obj_t) BgL_arg2703z00_9468), BgL_arg0z00_8139,
							BgL_arg1z00_8140);
					}
					{	/* Ast/walk.scm 302 */
						obj_t BgL_list2698z00_9469;

						{	/* Ast/walk.scm 302 */
							obj_t BgL_arg2699z00_9470;

							{	/* Ast/walk.scm 302 */
								obj_t BgL_arg2700z00_9471;

								BgL_arg2700z00_9471 =
									MAKE_YOUNG_PAIR(BgL_arg2697z00_9465, BNIL);
								BgL_arg2699z00_9470 =
									MAKE_YOUNG_PAIR(BgL_arg2696z00_9464, BgL_arg2700z00_9471);
							}
							BgL_list2698z00_9469 =
								MAKE_YOUNG_PAIR(BgL_arg2695z00_9463, BgL_arg2699z00_9470);
						}
						BgL_res4077z00_9472 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2698z00_9469);
					}
				}
				return BgL_res4077z00_9472;
			}
		}

	}



/* &walk1*-conditional1867 */
	obj_t BGl_z62walk1za2zd2conditional1867z12zzast_walkz00(obj_t BgL_envz00_8141,
		obj_t BgL_nz00_8142, obj_t BgL_pz00_8143, obj_t BgL_arg0z00_8144)
	{
		{	/* Ast/walk.scm 302 */
			{	/* Ast/walk.scm 302 */
				obj_t BgL_res4078z00_9483;

				{	/* Ast/walk.scm 302 */
					obj_t BgL_arg2686z00_9474;
					obj_t BgL_arg2687z00_9475;
					obj_t BgL_arg2688z00_9476;

					{	/* Ast/walk.scm 302 */
						BgL_nodez00_bglt BgL_arg2692z00_9477;

						BgL_arg2692z00_9477 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nz00_8142)))->BgL_testz00);
						BgL_arg2686z00_9474 =
							BGL_PROCEDURE_CALL2(BgL_pz00_8143,
							((obj_t) BgL_arg2692z00_9477), BgL_arg0z00_8144);
					}
					{	/* Ast/walk.scm 302 */
						BgL_nodez00_bglt BgL_arg2693z00_9478;

						BgL_arg2693z00_9478 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nz00_8142)))->BgL_truez00);
						BgL_arg2687z00_9475 =
							BGL_PROCEDURE_CALL2(BgL_pz00_8143,
							((obj_t) BgL_arg2693z00_9478), BgL_arg0z00_8144);
					}
					{	/* Ast/walk.scm 302 */
						BgL_nodez00_bglt BgL_arg2694z00_9479;

						BgL_arg2694z00_9479 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nz00_8142)))->BgL_falsez00);
						BgL_arg2688z00_9476 =
							BGL_PROCEDURE_CALL2(BgL_pz00_8143,
							((obj_t) BgL_arg2694z00_9479), BgL_arg0z00_8144);
					}
					{	/* Ast/walk.scm 302 */
						obj_t BgL_list2689z00_9480;

						{	/* Ast/walk.scm 302 */
							obj_t BgL_arg2690z00_9481;

							{	/* Ast/walk.scm 302 */
								obj_t BgL_arg2691z00_9482;

								BgL_arg2691z00_9482 =
									MAKE_YOUNG_PAIR(BgL_arg2688z00_9476, BNIL);
								BgL_arg2690z00_9481 =
									MAKE_YOUNG_PAIR(BgL_arg2687z00_9475, BgL_arg2691z00_9482);
							}
							BgL_list2689z00_9480 =
								MAKE_YOUNG_PAIR(BgL_arg2686z00_9474, BgL_arg2690z00_9481);
						}
						BgL_res4078z00_9483 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2689z00_9480);
					}
				}
				return BgL_res4078z00_9483;
			}
		}

	}



/* &walk0*-conditional1865 */
	obj_t BGl_z62walk0za2zd2conditional1865z12zzast_walkz00(obj_t BgL_envz00_8145,
		obj_t BgL_nz00_8146, obj_t BgL_pz00_8147)
	{
		{	/* Ast/walk.scm 302 */
			{	/* Ast/walk.scm 302 */
				obj_t BgL_res4079z00_9494;

				{	/* Ast/walk.scm 302 */
					obj_t BgL_arg2675z00_9485;
					obj_t BgL_arg2676z00_9486;
					obj_t BgL_arg2678z00_9487;

					{	/* Ast/walk.scm 302 */
						BgL_nodez00_bglt BgL_arg2682z00_9488;

						BgL_arg2682z00_9488 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nz00_8146)))->BgL_testz00);
						BgL_arg2675z00_9485 =
							BGL_PROCEDURE_CALL1(BgL_pz00_8147, ((obj_t) BgL_arg2682z00_9488));
					}
					{	/* Ast/walk.scm 302 */
						BgL_nodez00_bglt BgL_arg2684z00_9489;

						BgL_arg2684z00_9489 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nz00_8146)))->BgL_truez00);
						BgL_arg2676z00_9486 =
							BGL_PROCEDURE_CALL1(BgL_pz00_8147, ((obj_t) BgL_arg2684z00_9489));
					}
					{	/* Ast/walk.scm 302 */
						BgL_nodez00_bglt BgL_arg2685z00_9490;

						BgL_arg2685z00_9490 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nz00_8146)))->BgL_falsez00);
						BgL_arg2678z00_9487 =
							BGL_PROCEDURE_CALL1(BgL_pz00_8147, ((obj_t) BgL_arg2685z00_9490));
					}
					{	/* Ast/walk.scm 302 */
						obj_t BgL_list2679z00_9491;

						{	/* Ast/walk.scm 302 */
							obj_t BgL_arg2680z00_9492;

							{	/* Ast/walk.scm 302 */
								obj_t BgL_arg2681z00_9493;

								BgL_arg2681z00_9493 =
									MAKE_YOUNG_PAIR(BgL_arg2678z00_9487, BNIL);
								BgL_arg2680z00_9492 =
									MAKE_YOUNG_PAIR(BgL_arg2676z00_9486, BgL_arg2681z00_9493);
							}
							BgL_list2679z00_9491 =
								MAKE_YOUNG_PAIR(BgL_arg2675z00_9485, BgL_arg2680z00_9492);
						}
						BgL_res4079z00_9494 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2679z00_9491);
					}
				}
				return BgL_res4079z00_9494;
			}
		}

	}



/* &walk3-conditional1863 */
	obj_t BGl_z62walk3zd2conditional1863zb0zzast_walkz00(obj_t BgL_envz00_8148,
		obj_t BgL_nz00_8149, obj_t BgL_pz00_8150, obj_t BgL_arg0z00_8151,
		obj_t BgL_arg1z00_8152, obj_t BgL_arg2z00_8153)
	{
		{	/* Ast/walk.scm 302 */
			{	/* Ast/walk.scm 302 */
				BgL_nodez00_bglt BgL_arg2672z00_9496;

				BgL_arg2672z00_9496 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nz00_8149)))->BgL_testz00);
				BGL_PROCEDURE_CALL4(BgL_pz00_8150,
					((obj_t) BgL_arg2672z00_9496), BgL_arg0z00_8151, BgL_arg1z00_8152,
					BgL_arg2z00_8153);
			}
			{	/* Ast/walk.scm 302 */
				BgL_nodez00_bglt BgL_arg2673z00_9497;

				BgL_arg2673z00_9497 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nz00_8149)))->BgL_truez00);
				BGL_PROCEDURE_CALL4(BgL_pz00_8150,
					((obj_t) BgL_arg2673z00_9497), BgL_arg0z00_8151, BgL_arg1z00_8152,
					BgL_arg2z00_8153);
			}
			{	/* Ast/walk.scm 302 */
				BgL_nodez00_bglt BgL_arg2674z00_9498;

				BgL_arg2674z00_9498 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nz00_8149)))->BgL_falsez00);
				return
					BGL_PROCEDURE_CALL4(BgL_pz00_8150,
					((obj_t) BgL_arg2674z00_9498), BgL_arg0z00_8151, BgL_arg1z00_8152,
					BgL_arg2z00_8153);
			}
		}

	}



/* &walk2-conditional1861 */
	obj_t BGl_z62walk2zd2conditional1861zb0zzast_walkz00(obj_t BgL_envz00_8154,
		obj_t BgL_nz00_8155, obj_t BgL_pz00_8156, obj_t BgL_arg0z00_8157,
		obj_t BgL_arg1z00_8158)
	{
		{	/* Ast/walk.scm 302 */
			{	/* Ast/walk.scm 302 */
				BgL_nodez00_bglt BgL_arg2667z00_9500;

				BgL_arg2667z00_9500 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nz00_8155)))->BgL_testz00);
				BGL_PROCEDURE_CALL3(BgL_pz00_8156,
					((obj_t) BgL_arg2667z00_9500), BgL_arg0z00_8157, BgL_arg1z00_8158);
			}
			{	/* Ast/walk.scm 302 */
				BgL_nodez00_bglt BgL_arg2669z00_9501;

				BgL_arg2669z00_9501 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nz00_8155)))->BgL_truez00);
				BGL_PROCEDURE_CALL3(BgL_pz00_8156,
					((obj_t) BgL_arg2669z00_9501), BgL_arg0z00_8157, BgL_arg1z00_8158);
			}
			{	/* Ast/walk.scm 302 */
				BgL_nodez00_bglt BgL_arg2670z00_9502;

				BgL_arg2670z00_9502 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nz00_8155)))->BgL_falsez00);
				return
					BGL_PROCEDURE_CALL3(BgL_pz00_8156,
					((obj_t) BgL_arg2670z00_9502), BgL_arg0z00_8157, BgL_arg1z00_8158);
			}
		}

	}



/* &walk1-conditional1859 */
	obj_t BGl_z62walk1zd2conditional1859zb0zzast_walkz00(obj_t BgL_envz00_8159,
		obj_t BgL_nz00_8160, obj_t BgL_pz00_8161, obj_t BgL_arg0z00_8162)
	{
		{	/* Ast/walk.scm 302 */
			{	/* Ast/walk.scm 302 */
				BgL_nodez00_bglt BgL_arg2664z00_9504;

				BgL_arg2664z00_9504 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nz00_8160)))->BgL_testz00);
				BGL_PROCEDURE_CALL2(BgL_pz00_8161,
					((obj_t) BgL_arg2664z00_9504), BgL_arg0z00_8162);
			}
			{	/* Ast/walk.scm 302 */
				BgL_nodez00_bglt BgL_arg2665z00_9505;

				BgL_arg2665z00_9505 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nz00_8160)))->BgL_truez00);
				BGL_PROCEDURE_CALL2(BgL_pz00_8161,
					((obj_t) BgL_arg2665z00_9505), BgL_arg0z00_8162);
			}
			{	/* Ast/walk.scm 302 */
				BgL_nodez00_bglt BgL_arg2666z00_9506;

				BgL_arg2666z00_9506 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nz00_8160)))->BgL_falsez00);
				return
					BGL_PROCEDURE_CALL2(BgL_pz00_8161,
					((obj_t) BgL_arg2666z00_9506), BgL_arg0z00_8162);
			}
		}

	}



/* &walk0-conditional1857 */
	obj_t BGl_z62walk0zd2conditional1857zb0zzast_walkz00(obj_t BgL_envz00_8163,
		obj_t BgL_nz00_8164, obj_t BgL_pz00_8165)
	{
		{	/* Ast/walk.scm 302 */
			{	/* Ast/walk.scm 302 */
				BgL_nodez00_bglt BgL_arg2659z00_9508;

				BgL_arg2659z00_9508 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nz00_8164)))->BgL_testz00);
				BGL_PROCEDURE_CALL1(BgL_pz00_8165, ((obj_t) BgL_arg2659z00_9508));
			}
			{	/* Ast/walk.scm 302 */
				BgL_nodez00_bglt BgL_arg2660z00_9509;

				BgL_arg2660z00_9509 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nz00_8164)))->BgL_truez00);
				BGL_PROCEDURE_CALL1(BgL_pz00_8165, ((obj_t) BgL_arg2660z00_9509));
			}
			{	/* Ast/walk.scm 302 */
				BgL_nodez00_bglt BgL_arg2662z00_9510;

				BgL_arg2662z00_9510 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nz00_8164)))->BgL_falsez00);
				return
					BGL_PROCEDURE_CALL1(BgL_pz00_8165, ((obj_t) BgL_arg2662z00_9510));
			}
		}

	}



/* &walk3!-setq1855 */
	BgL_nodez00_bglt BGl_z62walk3z12zd2setq1855za2zzast_walkz00(obj_t
		BgL_envz00_8166, obj_t BgL_nz00_8167, obj_t BgL_pz00_8168,
		obj_t BgL_arg0z00_8169, obj_t BgL_arg1z00_8170, obj_t BgL_arg2z00_8171)
	{
		{	/* Ast/walk.scm 301 */
			{
				BgL_varz00_bglt BgL_auxz00_14492;

				{	/* Ast/walk.scm 301 */
					BgL_varz00_bglt BgL_arg2657z00_9512;

					BgL_arg2657z00_9512 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nz00_8167)))->BgL_varz00);
					BgL_auxz00_14492 =
						((BgL_varz00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_8168,
							((obj_t) BgL_arg2657z00_9512), BgL_arg0z00_8169, BgL_arg1z00_8170,
							BgL_arg2z00_8171));
				}
				((((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nz00_8167)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_14492), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_14506;

				{	/* Ast/walk.scm 301 */
					BgL_nodez00_bglt BgL_arg2658z00_9513;

					BgL_arg2658z00_9513 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nz00_8167)))->BgL_valuez00);
					BgL_auxz00_14506 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_8168,
							((obj_t) BgL_arg2658z00_9513), BgL_arg0z00_8169, BgL_arg1z00_8170,
							BgL_arg2z00_8171));
				}
				((((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nz00_8167)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_14506), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_setqz00_bglt) BgL_nz00_8167));
		}

	}



/* &walk2!-setq1853 */
	BgL_nodez00_bglt BGl_z62walk2z12zd2setq1853za2zzast_walkz00(obj_t
		BgL_envz00_8172, obj_t BgL_nz00_8173, obj_t BgL_pz00_8174,
		obj_t BgL_arg0z00_8175, obj_t BgL_arg1z00_8176)
	{
		{	/* Ast/walk.scm 301 */
			{
				BgL_varz00_bglt BgL_auxz00_14522;

				{	/* Ast/walk.scm 301 */
					BgL_varz00_bglt BgL_arg2654z00_9515;

					BgL_arg2654z00_9515 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nz00_8173)))->BgL_varz00);
					BgL_auxz00_14522 =
						((BgL_varz00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_8174,
							((obj_t) BgL_arg2654z00_9515), BgL_arg0z00_8175,
							BgL_arg1z00_8176));
				}
				((((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nz00_8173)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_14522), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_14535;

				{	/* Ast/walk.scm 301 */
					BgL_nodez00_bglt BgL_arg2656z00_9516;

					BgL_arg2656z00_9516 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nz00_8173)))->BgL_valuez00);
					BgL_auxz00_14535 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_8174,
							((obj_t) BgL_arg2656z00_9516), BgL_arg0z00_8175,
							BgL_arg1z00_8176));
				}
				((((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nz00_8173)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_14535), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_setqz00_bglt) BgL_nz00_8173));
		}

	}



/* &walk1!-setq1851 */
	BgL_nodez00_bglt BGl_z62walk1z12zd2setq1851za2zzast_walkz00(obj_t
		BgL_envz00_8177, obj_t BgL_nz00_8178, obj_t BgL_pz00_8179,
		obj_t BgL_arg0z00_8180)
	{
		{	/* Ast/walk.scm 301 */
			{
				BgL_varz00_bglt BgL_auxz00_14550;

				{	/* Ast/walk.scm 301 */
					BgL_varz00_bglt BgL_arg2651z00_9518;

					BgL_arg2651z00_9518 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nz00_8178)))->BgL_varz00);
					BgL_auxz00_14550 =
						((BgL_varz00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_8179,
							((obj_t) BgL_arg2651z00_9518), BgL_arg0z00_8180));
				}
				((((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nz00_8178)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_14550), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_14562;

				{	/* Ast/walk.scm 301 */
					BgL_nodez00_bglt BgL_arg2653z00_9519;

					BgL_arg2653z00_9519 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nz00_8178)))->BgL_valuez00);
					BgL_auxz00_14562 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_8179,
							((obj_t) BgL_arg2653z00_9519), BgL_arg0z00_8180));
				}
				((((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nz00_8178)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_14562), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_setqz00_bglt) BgL_nz00_8178));
		}

	}



/* &walk0!-setq1849 */
	BgL_nodez00_bglt BGl_z62walk0z12zd2setq1849za2zzast_walkz00(obj_t
		BgL_envz00_8181, obj_t BgL_nz00_8182, obj_t BgL_pz00_8183)
	{
		{	/* Ast/walk.scm 301 */
			{
				BgL_varz00_bglt BgL_auxz00_14576;

				{	/* Ast/walk.scm 301 */
					BgL_varz00_bglt BgL_arg2649z00_9521;

					BgL_arg2649z00_9521 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nz00_8182)))->BgL_varz00);
					BgL_auxz00_14576 =
						((BgL_varz00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_8183, ((obj_t) BgL_arg2649z00_9521)));
				}
				((((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nz00_8182)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_14576), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_14587;

				{	/* Ast/walk.scm 301 */
					BgL_nodez00_bglt BgL_arg2650z00_9522;

					BgL_arg2650z00_9522 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nz00_8182)))->BgL_valuez00);
					BgL_auxz00_14587 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_8183, ((obj_t) BgL_arg2650z00_9522)));
				}
				((((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nz00_8182)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_14587), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_setqz00_bglt) BgL_nz00_8182));
		}

	}



/* &walk3*-setq1847 */
	obj_t BGl_z62walk3za2zd2setq1847z12zzast_walkz00(obj_t BgL_envz00_8184,
		obj_t BgL_nz00_8185, obj_t BgL_pz00_8186, obj_t BgL_arg0z00_8187,
		obj_t BgL_arg1z00_8188, obj_t BgL_arg2z00_8189)
	{
		{	/* Ast/walk.scm 301 */
			{	/* Ast/walk.scm 301 */
				obj_t BgL_res4080z00_9528;

				{	/* Ast/walk.scm 301 */
					obj_t BgL_arg2644z00_9524;
					obj_t BgL_arg2645z00_9525;

					{	/* Ast/walk.scm 301 */
						BgL_varz00_bglt BgL_arg2647z00_9526;

						BgL_arg2647z00_9526 =
							(((BgL_setqz00_bglt) COBJECT(
									((BgL_setqz00_bglt) BgL_nz00_8185)))->BgL_varz00);
						BgL_arg2644z00_9524 =
							BGL_PROCEDURE_CALL4(BgL_pz00_8186,
							((obj_t) BgL_arg2647z00_9526), BgL_arg0z00_8187, BgL_arg1z00_8188,
							BgL_arg2z00_8189);
					}
					{	/* Ast/walk.scm 301 */
						BgL_nodez00_bglt BgL_arg2648z00_9527;

						BgL_arg2648z00_9527 =
							(((BgL_setqz00_bglt) COBJECT(
									((BgL_setqz00_bglt) BgL_nz00_8185)))->BgL_valuez00);
						BgL_arg2645z00_9525 =
							BGL_PROCEDURE_CALL4(BgL_pz00_8186,
							((obj_t) BgL_arg2648z00_9527), BgL_arg0z00_8187, BgL_arg1z00_8188,
							BgL_arg2z00_8189);
					}
					BgL_res4080z00_9528 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg2644z00_9524,
						BgL_arg2645z00_9525);
				}
				return BgL_res4080z00_9528;
			}
		}

	}



/* &walk2*-setq1845 */
	obj_t BGl_z62walk2za2zd2setq1845z12zzast_walkz00(obj_t BgL_envz00_8190,
		obj_t BgL_nz00_8191, obj_t BgL_pz00_8192, obj_t BgL_arg0z00_8193,
		obj_t BgL_arg1z00_8194)
	{
		{	/* Ast/walk.scm 301 */
			{	/* Ast/walk.scm 301 */
				obj_t BgL_res4081z00_9534;

				{	/* Ast/walk.scm 301 */
					obj_t BgL_arg2640z00_9530;
					obj_t BgL_arg2641z00_9531;

					{	/* Ast/walk.scm 301 */
						BgL_varz00_bglt BgL_arg2642z00_9532;

						BgL_arg2642z00_9532 =
							(((BgL_setqz00_bglt) COBJECT(
									((BgL_setqz00_bglt) BgL_nz00_8191)))->BgL_varz00);
						BgL_arg2640z00_9530 =
							BGL_PROCEDURE_CALL3(BgL_pz00_8192,
							((obj_t) BgL_arg2642z00_9532), BgL_arg0z00_8193,
							BgL_arg1z00_8194);
					}
					{	/* Ast/walk.scm 301 */
						BgL_nodez00_bglt BgL_arg2643z00_9533;

						BgL_arg2643z00_9533 =
							(((BgL_setqz00_bglt) COBJECT(
									((BgL_setqz00_bglt) BgL_nz00_8191)))->BgL_valuez00);
						BgL_arg2641z00_9531 =
							BGL_PROCEDURE_CALL3(BgL_pz00_8192,
							((obj_t) BgL_arg2643z00_9533), BgL_arg0z00_8193,
							BgL_arg1z00_8194);
					}
					BgL_res4081z00_9534 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg2640z00_9530,
						BgL_arg2641z00_9531);
				}
				return BgL_res4081z00_9534;
			}
		}

	}



/* &walk1*-setq1843 */
	obj_t BGl_z62walk1za2zd2setq1843z12zzast_walkz00(obj_t BgL_envz00_8195,
		obj_t BgL_nz00_8196, obj_t BgL_pz00_8197, obj_t BgL_arg0z00_8198)
	{
		{	/* Ast/walk.scm 301 */
			{	/* Ast/walk.scm 301 */
				obj_t BgL_res4082z00_9540;

				{	/* Ast/walk.scm 301 */
					obj_t BgL_arg2636z00_9536;
					obj_t BgL_arg2637z00_9537;

					{	/* Ast/walk.scm 301 */
						BgL_varz00_bglt BgL_arg2638z00_9538;

						BgL_arg2638z00_9538 =
							(((BgL_setqz00_bglt) COBJECT(
									((BgL_setqz00_bglt) BgL_nz00_8196)))->BgL_varz00);
						BgL_arg2636z00_9536 =
							BGL_PROCEDURE_CALL2(BgL_pz00_8197,
							((obj_t) BgL_arg2638z00_9538), BgL_arg0z00_8198);
					}
					{	/* Ast/walk.scm 301 */
						BgL_nodez00_bglt BgL_arg2639z00_9539;

						BgL_arg2639z00_9539 =
							(((BgL_setqz00_bglt) COBJECT(
									((BgL_setqz00_bglt) BgL_nz00_8196)))->BgL_valuez00);
						BgL_arg2637z00_9537 =
							BGL_PROCEDURE_CALL2(BgL_pz00_8197,
							((obj_t) BgL_arg2639z00_9539), BgL_arg0z00_8198);
					}
					BgL_res4082z00_9540 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg2636z00_9536,
						BgL_arg2637z00_9537);
				}
				return BgL_res4082z00_9540;
			}
		}

	}



/* &walk0*-setq1841 */
	obj_t BGl_z62walk0za2zd2setq1841z12zzast_walkz00(obj_t BgL_envz00_8199,
		obj_t BgL_nz00_8200, obj_t BgL_pz00_8201)
	{
		{	/* Ast/walk.scm 301 */
			{	/* Ast/walk.scm 301 */
				obj_t BgL_res4083z00_9546;

				{	/* Ast/walk.scm 301 */
					obj_t BgL_arg2631z00_9542;
					obj_t BgL_arg2632z00_9543;

					{	/* Ast/walk.scm 301 */
						BgL_varz00_bglt BgL_arg2633z00_9544;

						BgL_arg2633z00_9544 =
							(((BgL_setqz00_bglt) COBJECT(
									((BgL_setqz00_bglt) BgL_nz00_8200)))->BgL_varz00);
						BgL_arg2631z00_9542 =
							BGL_PROCEDURE_CALL1(BgL_pz00_8201, ((obj_t) BgL_arg2633z00_9544));
					}
					{	/* Ast/walk.scm 301 */
						BgL_nodez00_bglt BgL_arg2635z00_9545;

						BgL_arg2635z00_9545 =
							(((BgL_setqz00_bglt) COBJECT(
									((BgL_setqz00_bglt) BgL_nz00_8200)))->BgL_valuez00);
						BgL_arg2632z00_9543 =
							BGL_PROCEDURE_CALL1(BgL_pz00_8201, ((obj_t) BgL_arg2635z00_9545));
					}
					BgL_res4083z00_9546 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg2631z00_9542,
						BgL_arg2632z00_9543);
				}
				return BgL_res4083z00_9546;
			}
		}

	}



/* &walk3-setq1839 */
	obj_t BGl_z62walk3zd2setq1839zb0zzast_walkz00(obj_t BgL_envz00_8202,
		obj_t BgL_nz00_8203, obj_t BgL_pz00_8204, obj_t BgL_arg0z00_8205,
		obj_t BgL_arg1z00_8206, obj_t BgL_arg2z00_8207)
	{
		{	/* Ast/walk.scm 301 */
			{	/* Ast/walk.scm 301 */
				BgL_varz00_bglt BgL_arg2629z00_9548;

				BgL_arg2629z00_9548 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nz00_8203)))->BgL_varz00);
				BGL_PROCEDURE_CALL4(BgL_pz00_8204,
					((obj_t) BgL_arg2629z00_9548), BgL_arg0z00_8205, BgL_arg1z00_8206,
					BgL_arg2z00_8207);
			}
			{	/* Ast/walk.scm 301 */
				BgL_nodez00_bglt BgL_arg2630z00_9549;

				BgL_arg2630z00_9549 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nz00_8203)))->BgL_valuez00);
				return
					BGL_PROCEDURE_CALL4(BgL_pz00_8204,
					((obj_t) BgL_arg2630z00_9549), BgL_arg0z00_8205, BgL_arg1z00_8206,
					BgL_arg2z00_8207);
			}
		}

	}



/* &walk2-setq1837 */
	obj_t BGl_z62walk2zd2setq1837zb0zzast_walkz00(obj_t BgL_envz00_8208,
		obj_t BgL_nz00_8209, obj_t BgL_pz00_8210, obj_t BgL_arg0z00_8211,
		obj_t BgL_arg1z00_8212)
	{
		{	/* Ast/walk.scm 301 */
			{	/* Ast/walk.scm 301 */
				BgL_varz00_bglt BgL_arg2627z00_9551;

				BgL_arg2627z00_9551 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nz00_8209)))->BgL_varz00);
				BGL_PROCEDURE_CALL3(BgL_pz00_8210,
					((obj_t) BgL_arg2627z00_9551), BgL_arg0z00_8211, BgL_arg1z00_8212);
			}
			{	/* Ast/walk.scm 301 */
				BgL_nodez00_bglt BgL_arg2628z00_9552;

				BgL_arg2628z00_9552 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nz00_8209)))->BgL_valuez00);
				return
					BGL_PROCEDURE_CALL3(BgL_pz00_8210,
					((obj_t) BgL_arg2628z00_9552), BgL_arg0z00_8211, BgL_arg1z00_8212);
			}
		}

	}



/* &walk1-setq1835 */
	obj_t BGl_z62walk1zd2setq1835zb0zzast_walkz00(obj_t BgL_envz00_8213,
		obj_t BgL_nz00_8214, obj_t BgL_pz00_8215, obj_t BgL_arg0z00_8216)
	{
		{	/* Ast/walk.scm 301 */
			{	/* Ast/walk.scm 301 */
				BgL_varz00_bglt BgL_arg2624z00_9554;

				BgL_arg2624z00_9554 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nz00_8214)))->BgL_varz00);
				BGL_PROCEDURE_CALL2(BgL_pz00_8215,
					((obj_t) BgL_arg2624z00_9554), BgL_arg0z00_8216);
			}
			{	/* Ast/walk.scm 301 */
				BgL_nodez00_bglt BgL_arg2626z00_9555;

				BgL_arg2626z00_9555 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nz00_8214)))->BgL_valuez00);
				return
					BGL_PROCEDURE_CALL2(BgL_pz00_8215,
					((obj_t) BgL_arg2626z00_9555), BgL_arg0z00_8216);
			}
		}

	}



/* &walk0-setq1833 */
	obj_t BGl_z62walk0zd2setq1833zb0zzast_walkz00(obj_t BgL_envz00_8217,
		obj_t BgL_nz00_8218, obj_t BgL_pz00_8219)
	{
		{	/* Ast/walk.scm 301 */
			{	/* Ast/walk.scm 301 */
				BgL_varz00_bglt BgL_arg2622z00_9557;

				BgL_arg2622z00_9557 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nz00_8218)))->BgL_varz00);
				BGL_PROCEDURE_CALL1(BgL_pz00_8219, ((obj_t) BgL_arg2622z00_9557));
			}
			{	/* Ast/walk.scm 301 */
				BgL_nodez00_bglt BgL_arg2623z00_9558;

				BgL_arg2623z00_9558 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nz00_8218)))->BgL_valuez00);
				return
					BGL_PROCEDURE_CALL1(BgL_pz00_8219, ((obj_t) BgL_arg2623z00_9558));
			}
		}

	}



/* &walk3!-cast1831 */
	BgL_nodez00_bglt BGl_z62walk3z12zd2cast1831za2zzast_walkz00(obj_t
		BgL_envz00_8220, obj_t BgL_nz00_8221, obj_t BgL_pz00_8222,
		obj_t BgL_arg0z00_8223, obj_t BgL_arg1z00_8224, obj_t BgL_arg2z00_8225)
	{
		{	/* Ast/walk.scm 300 */
			{
				BgL_nodez00_bglt BgL_auxz00_14740;

				{	/* Ast/walk.scm 300 */
					BgL_nodez00_bglt BgL_arg2621z00_9560;

					BgL_arg2621z00_9560 =
						(((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nz00_8221)))->BgL_argz00);
					BgL_auxz00_14740 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_8222,
							((obj_t) BgL_arg2621z00_9560), BgL_arg0z00_8223, BgL_arg1z00_8224,
							BgL_arg2z00_8225));
				}
				((((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nz00_8221)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_14740), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_castz00_bglt) BgL_nz00_8221));
		}

	}



/* &walk2!-cast1829 */
	BgL_nodez00_bglt BGl_z62walk2z12zd2cast1829za2zzast_walkz00(obj_t
		BgL_envz00_8226, obj_t BgL_nz00_8227, obj_t BgL_pz00_8228,
		obj_t BgL_arg0z00_8229, obj_t BgL_arg1z00_8230)
	{
		{	/* Ast/walk.scm 300 */
			{
				BgL_nodez00_bglt BgL_auxz00_14756;

				{	/* Ast/walk.scm 300 */
					BgL_nodez00_bglt BgL_arg2620z00_9562;

					BgL_arg2620z00_9562 =
						(((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nz00_8227)))->BgL_argz00);
					BgL_auxz00_14756 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_8228,
							((obj_t) BgL_arg2620z00_9562), BgL_arg0z00_8229,
							BgL_arg1z00_8230));
				}
				((((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nz00_8227)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_14756), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_castz00_bglt) BgL_nz00_8227));
		}

	}



/* &walk1!-cast1827 */
	BgL_nodez00_bglt BGl_z62walk1z12zd2cast1827za2zzast_walkz00(obj_t
		BgL_envz00_8231, obj_t BgL_nz00_8232, obj_t BgL_pz00_8233,
		obj_t BgL_arg0z00_8234)
	{
		{	/* Ast/walk.scm 300 */
			{
				BgL_nodez00_bglt BgL_auxz00_14771;

				{	/* Ast/walk.scm 300 */
					BgL_nodez00_bglt BgL_arg2619z00_9564;

					BgL_arg2619z00_9564 =
						(((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nz00_8232)))->BgL_argz00);
					BgL_auxz00_14771 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_8233,
							((obj_t) BgL_arg2619z00_9564), BgL_arg0z00_8234));
				}
				((((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nz00_8232)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_14771), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_castz00_bglt) BgL_nz00_8232));
		}

	}



/* &walk0!-cast1825 */
	BgL_nodez00_bglt BGl_z62walk0z12zd2cast1825za2zzast_walkz00(obj_t
		BgL_envz00_8235, obj_t BgL_nz00_8236, obj_t BgL_pz00_8237)
	{
		{	/* Ast/walk.scm 300 */
			{
				BgL_nodez00_bglt BgL_auxz00_14785;

				{	/* Ast/walk.scm 300 */
					BgL_nodez00_bglt BgL_arg2618z00_9566;

					BgL_arg2618z00_9566 =
						(((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nz00_8236)))->BgL_argz00);
					BgL_auxz00_14785 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_8237, ((obj_t) BgL_arg2618z00_9566)));
				}
				((((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nz00_8236)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_14785), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_castz00_bglt) BgL_nz00_8236));
		}

	}



/* &walk3*-cast1823 */
	obj_t BGl_z62walk3za2zd2cast1823z12zzast_walkz00(obj_t BgL_envz00_8238,
		obj_t BgL_nz00_8239, obj_t BgL_pz00_8240, obj_t BgL_arg0z00_8241,
		obj_t BgL_arg1z00_8242, obj_t BgL_arg2z00_8243)
	{
		{	/* Ast/walk.scm 300 */
			{	/* Ast/walk.scm 300 */
				obj_t BgL_res4084z00_9571;

				{	/* Ast/walk.scm 300 */
					obj_t BgL_arg2614z00_9568;

					{	/* Ast/walk.scm 300 */
						BgL_nodez00_bglt BgL_arg2617z00_9569;

						BgL_arg2617z00_9569 =
							(((BgL_castz00_bglt) COBJECT(
									((BgL_castz00_bglt) BgL_nz00_8239)))->BgL_argz00);
						BgL_arg2614z00_9568 =
							BGL_PROCEDURE_CALL4(BgL_pz00_8240,
							((obj_t) BgL_arg2617z00_9569), BgL_arg0z00_8241, BgL_arg1z00_8242,
							BgL_arg2z00_8243);
					}
					{	/* Ast/walk.scm 300 */
						obj_t BgL_list2615z00_9570;

						BgL_list2615z00_9570 = MAKE_YOUNG_PAIR(BgL_arg2614z00_9568, BNIL);
						BgL_res4084z00_9571 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2615z00_9570);
					}
				}
				return BgL_res4084z00_9571;
			}
		}

	}



/* &walk2*-cast1821 */
	obj_t BGl_z62walk2za2zd2cast1821z12zzast_walkz00(obj_t BgL_envz00_8244,
		obj_t BgL_nz00_8245, obj_t BgL_pz00_8246, obj_t BgL_arg0z00_8247,
		obj_t BgL_arg1z00_8248)
	{
		{	/* Ast/walk.scm 300 */
			{	/* Ast/walk.scm 300 */
				obj_t BgL_res4085z00_9576;

				{	/* Ast/walk.scm 300 */
					obj_t BgL_arg2611z00_9573;

					{	/* Ast/walk.scm 300 */
						BgL_nodez00_bglt BgL_arg2613z00_9574;

						BgL_arg2613z00_9574 =
							(((BgL_castz00_bglt) COBJECT(
									((BgL_castz00_bglt) BgL_nz00_8245)))->BgL_argz00);
						BgL_arg2611z00_9573 =
							BGL_PROCEDURE_CALL3(BgL_pz00_8246,
							((obj_t) BgL_arg2613z00_9574), BgL_arg0z00_8247,
							BgL_arg1z00_8248);
					}
					{	/* Ast/walk.scm 300 */
						obj_t BgL_list2612z00_9575;

						BgL_list2612z00_9575 = MAKE_YOUNG_PAIR(BgL_arg2611z00_9573, BNIL);
						BgL_res4085z00_9576 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2612z00_9575);
					}
				}
				return BgL_res4085z00_9576;
			}
		}

	}



/* &walk1*-cast1819 */
	obj_t BGl_z62walk1za2zd2cast1819z12zzast_walkz00(obj_t BgL_envz00_8249,
		obj_t BgL_nz00_8250, obj_t BgL_pz00_8251, obj_t BgL_arg0z00_8252)
	{
		{	/* Ast/walk.scm 300 */
			{	/* Ast/walk.scm 300 */
				obj_t BgL_res4086z00_9581;

				{	/* Ast/walk.scm 300 */
					obj_t BgL_arg2608z00_9578;

					{	/* Ast/walk.scm 300 */
						BgL_nodez00_bglt BgL_arg2610z00_9579;

						BgL_arg2610z00_9579 =
							(((BgL_castz00_bglt) COBJECT(
									((BgL_castz00_bglt) BgL_nz00_8250)))->BgL_argz00);
						BgL_arg2608z00_9578 =
							BGL_PROCEDURE_CALL2(BgL_pz00_8251,
							((obj_t) BgL_arg2610z00_9579), BgL_arg0z00_8252);
					}
					{	/* Ast/walk.scm 300 */
						obj_t BgL_list2609z00_9580;

						BgL_list2609z00_9580 = MAKE_YOUNG_PAIR(BgL_arg2608z00_9578, BNIL);
						BgL_res4086z00_9581 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2609z00_9580);
					}
				}
				return BgL_res4086z00_9581;
			}
		}

	}



/* &walk0*-cast1817 */
	obj_t BGl_z62walk0za2zd2cast1817z12zzast_walkz00(obj_t BgL_envz00_8253,
		obj_t BgL_nz00_8254, obj_t BgL_pz00_8255)
	{
		{	/* Ast/walk.scm 300 */
			{	/* Ast/walk.scm 300 */
				obj_t BgL_res4087z00_9586;

				{	/* Ast/walk.scm 300 */
					obj_t BgL_arg2605z00_9583;

					{	/* Ast/walk.scm 300 */
						BgL_nodez00_bglt BgL_arg2607z00_9584;

						BgL_arg2607z00_9584 =
							(((BgL_castz00_bglt) COBJECT(
									((BgL_castz00_bglt) BgL_nz00_8254)))->BgL_argz00);
						BgL_arg2605z00_9583 =
							BGL_PROCEDURE_CALL1(BgL_pz00_8255, ((obj_t) BgL_arg2607z00_9584));
					}
					{	/* Ast/walk.scm 300 */
						obj_t BgL_list2606z00_9585;

						BgL_list2606z00_9585 = MAKE_YOUNG_PAIR(BgL_arg2605z00_9583, BNIL);
						BgL_res4087z00_9586 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2606z00_9585);
					}
				}
				return BgL_res4087z00_9586;
			}
		}

	}



/* &walk3-cast1815 */
	obj_t BGl_z62walk3zd2cast1815zb0zzast_walkz00(obj_t BgL_envz00_8256,
		obj_t BgL_nz00_8257, obj_t BgL_pz00_8258, obj_t BgL_arg0z00_8259,
		obj_t BgL_arg1z00_8260, obj_t BgL_arg2z00_8261)
	{
		{	/* Ast/walk.scm 300 */
			{	/* Ast/walk.scm 300 */
				BgL_nodez00_bglt BgL_arg2604z00_9588;

				BgL_arg2604z00_9588 =
					(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nz00_8257)))->BgL_argz00);
				return
					BGL_PROCEDURE_CALL4(BgL_pz00_8258,
					((obj_t) BgL_arg2604z00_9588), BgL_arg0z00_8259, BgL_arg1z00_8260,
					BgL_arg2z00_8261);
			}
		}

	}



/* &walk2-cast1813 */
	obj_t BGl_z62walk2zd2cast1813zb0zzast_walkz00(obj_t BgL_envz00_8262,
		obj_t BgL_nz00_8263, obj_t BgL_pz00_8264, obj_t BgL_arg0z00_8265,
		obj_t BgL_arg1z00_8266)
	{
		{	/* Ast/walk.scm 300 */
			{	/* Ast/walk.scm 300 */
				BgL_nodez00_bglt BgL_arg2601z00_9590;

				BgL_arg2601z00_9590 =
					(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nz00_8263)))->BgL_argz00);
				return
					BGL_PROCEDURE_CALL3(BgL_pz00_8264,
					((obj_t) BgL_arg2601z00_9590), BgL_arg0z00_8265, BgL_arg1z00_8266);
			}
		}

	}



/* &walk1-cast1811 */
	obj_t BGl_z62walk1zd2cast1811zb0zzast_walkz00(obj_t BgL_envz00_8267,
		obj_t BgL_nz00_8268, obj_t BgL_pz00_8269, obj_t BgL_arg0z00_8270)
	{
		{	/* Ast/walk.scm 300 */
			{	/* Ast/walk.scm 300 */
				BgL_nodez00_bglt BgL_arg2600z00_9592;

				BgL_arg2600z00_9592 =
					(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nz00_8268)))->BgL_argz00);
				return
					BGL_PROCEDURE_CALL2(BgL_pz00_8269,
					((obj_t) BgL_arg2600z00_9592), BgL_arg0z00_8270);
			}
		}

	}



/* &walk0-cast1809 */
	obj_t BGl_z62walk0zd2cast1809zb0zzast_walkz00(obj_t BgL_envz00_8271,
		obj_t BgL_nz00_8272, obj_t BgL_pz00_8273)
	{
		{	/* Ast/walk.scm 300 */
			{	/* Ast/walk.scm 300 */
				BgL_nodez00_bglt BgL_arg2599z00_9594;

				BgL_arg2599z00_9594 =
					(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nz00_8272)))->BgL_argz00);
				return
					BGL_PROCEDURE_CALL1(BgL_pz00_8273, ((obj_t) BgL_arg2599z00_9594));
			}
		}

	}



/* &walk3!-extern1807 */
	BgL_nodez00_bglt BGl_z62walk3z12zd2extern1807za2zzast_walkz00(obj_t
		BgL_envz00_8274, obj_t BgL_nz00_8275, obj_t BgL_pz00_8276,
		obj_t BgL_arg0z00_8277, obj_t BgL_arg1z00_8278, obj_t BgL_arg2z00_8279)
	{
		{	/* Ast/walk.scm 299 */
			{
				obj_t BgL_fieldsz00_9597;

				BgL_fieldsz00_9597 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nz00_8275)))->BgL_exprza2za2);
			BgL_loopz00_9596:
				if (NULLP(BgL_fieldsz00_9597))
					{	/* Ast/walk.scm 299 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 299 */
						{	/* Ast/walk.scm 299 */
							obj_t BgL_arg2595z00_9598;

							{	/* Ast/walk.scm 299 */
								obj_t BgL_arg2596z00_9599;

								BgL_arg2596z00_9599 = CAR(((obj_t) BgL_fieldsz00_9597));
								BgL_arg2595z00_9598 =
									BGL_PROCEDURE_CALL4(BgL_pz00_8276, BgL_arg2596z00_9599,
									BgL_arg0z00_8277, BgL_arg1z00_8278, BgL_arg2z00_8279);
							}
							{	/* Ast/walk.scm 299 */
								obj_t BgL_tmpz00_14885;

								BgL_tmpz00_14885 = ((obj_t) BgL_fieldsz00_9597);
								SET_CAR(BgL_tmpz00_14885, BgL_arg2595z00_9598);
							}
						}
						{	/* Ast/walk.scm 299 */
							obj_t BgL_arg2597z00_9600;

							BgL_arg2597z00_9600 = CDR(((obj_t) BgL_fieldsz00_9597));
							{
								obj_t BgL_fieldsz00_14890;

								BgL_fieldsz00_14890 = BgL_arg2597z00_9600;
								BgL_fieldsz00_9597 = BgL_fieldsz00_14890;
								goto BgL_loopz00_9596;
							}
						}
					}
			}
			return ((BgL_nodez00_bglt) ((BgL_externz00_bglt) BgL_nz00_8275));
		}

	}



/* &walk2!-extern1805 */
	BgL_nodez00_bglt BGl_z62walk2z12zd2extern1805za2zzast_walkz00(obj_t
		BgL_envz00_8280, obj_t BgL_nz00_8281, obj_t BgL_pz00_8282,
		obj_t BgL_arg0z00_8283, obj_t BgL_arg1z00_8284)
	{
		{	/* Ast/walk.scm 299 */
			{
				obj_t BgL_fieldsz00_9603;

				BgL_fieldsz00_9603 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nz00_8281)))->BgL_exprza2za2);
			BgL_loopz00_9602:
				if (NULLP(BgL_fieldsz00_9603))
					{	/* Ast/walk.scm 299 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 299 */
						{	/* Ast/walk.scm 299 */
							obj_t BgL_arg2589z00_9604;

							{	/* Ast/walk.scm 299 */
								obj_t BgL_arg2590z00_9605;

								BgL_arg2590z00_9605 = CAR(((obj_t) BgL_fieldsz00_9603));
								BgL_arg2589z00_9604 =
									BGL_PROCEDURE_CALL3(BgL_pz00_8282, BgL_arg2590z00_9605,
									BgL_arg0z00_8283, BgL_arg1z00_8284);
							}
							{	/* Ast/walk.scm 299 */
								obj_t BgL_tmpz00_14905;

								BgL_tmpz00_14905 = ((obj_t) BgL_fieldsz00_9603);
								SET_CAR(BgL_tmpz00_14905, BgL_arg2589z00_9604);
							}
						}
						{	/* Ast/walk.scm 299 */
							obj_t BgL_arg2591z00_9606;

							BgL_arg2591z00_9606 = CDR(((obj_t) BgL_fieldsz00_9603));
							{
								obj_t BgL_fieldsz00_14910;

								BgL_fieldsz00_14910 = BgL_arg2591z00_9606;
								BgL_fieldsz00_9603 = BgL_fieldsz00_14910;
								goto BgL_loopz00_9602;
							}
						}
					}
			}
			return ((BgL_nodez00_bglt) ((BgL_externz00_bglt) BgL_nz00_8281));
		}

	}



/* &walk1!-extern1803 */
	BgL_nodez00_bglt BGl_z62walk1z12zd2extern1803za2zzast_walkz00(obj_t
		BgL_envz00_8285, obj_t BgL_nz00_8286, obj_t BgL_pz00_8287,
		obj_t BgL_arg0z00_8288)
	{
		{	/* Ast/walk.scm 299 */
			{
				obj_t BgL_fieldsz00_9609;

				BgL_fieldsz00_9609 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nz00_8286)))->BgL_exprza2za2);
			BgL_loopz00_9608:
				if (NULLP(BgL_fieldsz00_9609))
					{	/* Ast/walk.scm 299 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 299 */
						{	/* Ast/walk.scm 299 */
							obj_t BgL_arg2578z00_9610;

							{	/* Ast/walk.scm 299 */
								obj_t BgL_arg2579z00_9611;

								BgL_arg2579z00_9611 = CAR(((obj_t) BgL_fieldsz00_9609));
								BgL_arg2578z00_9610 =
									BGL_PROCEDURE_CALL2(BgL_pz00_8287, BgL_arg2579z00_9611,
									BgL_arg0z00_8288);
							}
							{	/* Ast/walk.scm 299 */
								obj_t BgL_tmpz00_14924;

								BgL_tmpz00_14924 = ((obj_t) BgL_fieldsz00_9609);
								SET_CAR(BgL_tmpz00_14924, BgL_arg2578z00_9610);
							}
						}
						{	/* Ast/walk.scm 299 */
							obj_t BgL_arg2584z00_9612;

							BgL_arg2584z00_9612 = CDR(((obj_t) BgL_fieldsz00_9609));
							{
								obj_t BgL_fieldsz00_14929;

								BgL_fieldsz00_14929 = BgL_arg2584z00_9612;
								BgL_fieldsz00_9609 = BgL_fieldsz00_14929;
								goto BgL_loopz00_9608;
							}
						}
					}
			}
			return ((BgL_nodez00_bglt) ((BgL_externz00_bglt) BgL_nz00_8286));
		}

	}



/* &walk0!-extern1801 */
	BgL_nodez00_bglt BGl_z62walk0z12zd2extern1801za2zzast_walkz00(obj_t
		BgL_envz00_8289, obj_t BgL_nz00_8290, obj_t BgL_pz00_8291)
	{
		{	/* Ast/walk.scm 299 */
			{
				obj_t BgL_fieldsz00_9615;

				BgL_fieldsz00_9615 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nz00_8290)))->BgL_exprza2za2);
			BgL_loopz00_9614:
				if (NULLP(BgL_fieldsz00_9615))
					{	/* Ast/walk.scm 299 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 299 */
						{	/* Ast/walk.scm 299 */
							obj_t BgL_arg2566z00_9616;

							{	/* Ast/walk.scm 299 */
								obj_t BgL_arg2567z00_9617;

								BgL_arg2567z00_9617 = CAR(((obj_t) BgL_fieldsz00_9615));
								BgL_arg2566z00_9616 =
									BGL_PROCEDURE_CALL1(BgL_pz00_8291, BgL_arg2567z00_9617);
							}
							{	/* Ast/walk.scm 299 */
								obj_t BgL_tmpz00_14942;

								BgL_tmpz00_14942 = ((obj_t) BgL_fieldsz00_9615);
								SET_CAR(BgL_tmpz00_14942, BgL_arg2566z00_9616);
							}
						}
						{	/* Ast/walk.scm 299 */
							obj_t BgL_arg2568z00_9618;

							BgL_arg2568z00_9618 = CDR(((obj_t) BgL_fieldsz00_9615));
							{
								obj_t BgL_fieldsz00_14947;

								BgL_fieldsz00_14947 = BgL_arg2568z00_9618;
								BgL_fieldsz00_9615 = BgL_fieldsz00_14947;
								goto BgL_loopz00_9614;
							}
						}
					}
			}
			return ((BgL_nodez00_bglt) ((BgL_externz00_bglt) BgL_nz00_8290));
		}

	}



/* &walk3*-extern1799 */
	obj_t BGl_z62walk3za2zd2extern1799z12zzast_walkz00(obj_t BgL_envz00_8292,
		obj_t BgL_nz00_8293, obj_t BgL_pz00_8294, obj_t BgL_arg0z00_8295,
		obj_t BgL_arg1z00_8296, obj_t BgL_arg2z00_8297)
	{
		{	/* Ast/walk.scm 299 */
			{	/* Ast/walk.scm 299 */
				obj_t BgL_res4088z00_9622;

				{	/* Ast/walk.scm 299 */
					obj_t BgL_arg2554z00_9620;

					BgL_arg2554z00_9620 =
						BGl_zc3z04anonymousza32556ze3ze70z60zzast_walkz00(BgL_arg2z00_8297,
						BgL_arg1z00_8296, BgL_arg0z00_8295, BgL_pz00_8294,
						(((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
										BgL_nz00_8293)))->BgL_exprza2za2));
					{	/* Ast/walk.scm 299 */
						obj_t BgL_list2555z00_9621;

						BgL_list2555z00_9621 = MAKE_YOUNG_PAIR(BgL_arg2554z00_9620, BNIL);
						BgL_res4088z00_9622 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2555z00_9621);
					}
				}
				return BgL_res4088z00_9622;
			}
		}

	}



/* <@anonymous:2556>~0 */
	obj_t BGl_zc3z04anonymousza32556ze3ze70z60zzast_walkz00(obj_t
		BgL_arg2z00_8637, obj_t BgL_arg1z00_8636, obj_t BgL_arg0z00_8635,
		obj_t BgL_pz00_8634, obj_t BgL_l1555z00_3477)
	{
		{	/* Ast/walk.scm 299 */
			if (NULLP(BgL_l1555z00_3477))
				{	/* Ast/walk.scm 299 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 299 */
					obj_t BgL_arg2560z00_3480;
					obj_t BgL_arg2561z00_3481;

					{	/* Ast/walk.scm 299 */
						obj_t BgL_fz00_3482;

						BgL_fz00_3482 = CAR(((obj_t) BgL_l1555z00_3477));
						BgL_arg2560z00_3480 =
							BGL_PROCEDURE_CALL4(BgL_pz00_8634, BgL_fz00_3482,
							BgL_arg0z00_8635, BgL_arg1z00_8636, BgL_arg2z00_8637);
					}
					{	/* Ast/walk.scm 299 */
						obj_t BgL_arg2562z00_3483;

						BgL_arg2562z00_3483 = CDR(((obj_t) BgL_l1555z00_3477));
						BgL_arg2561z00_3481 =
							BGl_zc3z04anonymousza32556ze3ze70z60zzast_walkz00
							(BgL_arg2z00_8637, BgL_arg1z00_8636, BgL_arg0z00_8635,
							BgL_pz00_8634, BgL_arg2562z00_3483);
					}
					return bgl_append2(BgL_arg2560z00_3480, BgL_arg2561z00_3481);
				}
		}

	}



/* &walk2*-extern1797 */
	obj_t BGl_z62walk2za2zd2extern1797z12zzast_walkz00(obj_t BgL_envz00_8298,
		obj_t BgL_nz00_8299, obj_t BgL_pz00_8300, obj_t BgL_arg0z00_8301,
		obj_t BgL_arg1z00_8302)
	{
		{	/* Ast/walk.scm 299 */
			{	/* Ast/walk.scm 299 */
				obj_t BgL_res4089z00_9626;

				{	/* Ast/walk.scm 299 */
					obj_t BgL_arg2546z00_9624;

					BgL_arg2546z00_9624 =
						BGl_zc3z04anonymousza32548ze3ze70z60zzast_walkz00(BgL_arg1z00_8302,
						BgL_arg0z00_8301, BgL_pz00_8300,
						(((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
										BgL_nz00_8299)))->BgL_exprza2za2));
					{	/* Ast/walk.scm 299 */
						obj_t BgL_list2547z00_9625;

						BgL_list2547z00_9625 = MAKE_YOUNG_PAIR(BgL_arg2546z00_9624, BNIL);
						BgL_res4089z00_9626 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2547z00_9625);
					}
				}
				return BgL_res4089z00_9626;
			}
		}

	}



/* <@anonymous:2548>~0 */
	obj_t BGl_zc3z04anonymousza32548ze3ze70z60zzast_walkz00(obj_t
		BgL_arg1z00_8633, obj_t BgL_arg0z00_8632, obj_t BgL_pz00_8631,
		obj_t BgL_l1552z00_3456)
	{
		{	/* Ast/walk.scm 299 */
			if (NULLP(BgL_l1552z00_3456))
				{	/* Ast/walk.scm 299 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 299 */
					obj_t BgL_arg2551z00_3459;
					obj_t BgL_arg2552z00_3460;

					{	/* Ast/walk.scm 299 */
						obj_t BgL_fz00_3461;

						BgL_fz00_3461 = CAR(((obj_t) BgL_l1552z00_3456));
						BgL_arg2551z00_3459 =
							BGL_PROCEDURE_CALL3(BgL_pz00_8631, BgL_fz00_3461,
							BgL_arg0z00_8632, BgL_arg1z00_8633);
					}
					{	/* Ast/walk.scm 299 */
						obj_t BgL_arg2553z00_3462;

						BgL_arg2553z00_3462 = CDR(((obj_t) BgL_l1552z00_3456));
						BgL_arg2552z00_3460 =
							BGl_zc3z04anonymousza32548ze3ze70z60zzast_walkz00
							(BgL_arg1z00_8633, BgL_arg0z00_8632, BgL_pz00_8631,
							BgL_arg2553z00_3462);
					}
					return bgl_append2(BgL_arg2551z00_3459, BgL_arg2552z00_3460);
				}
		}

	}



/* &walk1*-extern1795 */
	obj_t BGl_z62walk1za2zd2extern1795z12zzast_walkz00(obj_t BgL_envz00_8303,
		obj_t BgL_nz00_8304, obj_t BgL_pz00_8305, obj_t BgL_arg0z00_8306)
	{
		{	/* Ast/walk.scm 299 */
			{	/* Ast/walk.scm 299 */
				obj_t BgL_res4090z00_9630;

				{	/* Ast/walk.scm 299 */
					obj_t BgL_arg2538z00_9628;

					BgL_arg2538z00_9628 =
						BGl_zc3z04anonymousza32540ze3ze70z60zzast_walkz00(BgL_arg0z00_8306,
						BgL_pz00_8305,
						(((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt)
										BgL_nz00_8304)))->BgL_exprza2za2));
					{	/* Ast/walk.scm 299 */
						obj_t BgL_list2539z00_9629;

						BgL_list2539z00_9629 = MAKE_YOUNG_PAIR(BgL_arg2538z00_9628, BNIL);
						BgL_res4090z00_9630 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2539z00_9629);
					}
				}
				return BgL_res4090z00_9630;
			}
		}

	}



/* <@anonymous:2540>~0 */
	obj_t BGl_zc3z04anonymousza32540ze3ze70z60zzast_walkz00(obj_t
		BgL_arg0z00_8630, obj_t BgL_pz00_8629, obj_t BgL_l1549z00_3436)
	{
		{	/* Ast/walk.scm 299 */
			if (NULLP(BgL_l1549z00_3436))
				{	/* Ast/walk.scm 299 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 299 */
					obj_t BgL_arg2542z00_3439;
					obj_t BgL_arg2543z00_3440;

					{	/* Ast/walk.scm 299 */
						obj_t BgL_fz00_3441;

						BgL_fz00_3441 = CAR(((obj_t) BgL_l1549z00_3436));
						BgL_arg2542z00_3439 =
							BGL_PROCEDURE_CALL2(BgL_pz00_8629, BgL_fz00_3441,
							BgL_arg0z00_8630);
					}
					{	/* Ast/walk.scm 299 */
						obj_t BgL_arg2545z00_3442;

						BgL_arg2545z00_3442 = CDR(((obj_t) BgL_l1549z00_3436));
						BgL_arg2543z00_3440 =
							BGl_zc3z04anonymousza32540ze3ze70z60zzast_walkz00
							(BgL_arg0z00_8630, BgL_pz00_8629, BgL_arg2545z00_3442);
					}
					return bgl_append2(BgL_arg2542z00_3439, BgL_arg2543z00_3440);
				}
		}

	}



/* &walk0*-extern1793 */
	obj_t BGl_z62walk0za2zd2extern1793z12zzast_walkz00(obj_t BgL_envz00_8307,
		obj_t BgL_nz00_8308, obj_t BgL_pz00_8309)
	{
		{	/* Ast/walk.scm 299 */
			{	/* Ast/walk.scm 299 */
				obj_t BgL_res4091z00_9634;

				{	/* Ast/walk.scm 299 */
					obj_t BgL_arg2528z00_9632;

					BgL_arg2528z00_9632 =
						BGl_zc3z04anonymousza32530ze3ze70z60zzast_walkz00(BgL_pz00_8309,
						(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt) BgL_nz00_8308)))->BgL_exprza2za2));
					{	/* Ast/walk.scm 299 */
						obj_t BgL_list2529z00_9633;

						BgL_list2529z00_9633 = MAKE_YOUNG_PAIR(BgL_arg2528z00_9632, BNIL);
						BgL_res4091z00_9634 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2529z00_9633);
					}
				}
				return BgL_res4091z00_9634;
			}
		}

	}



/* <@anonymous:2530>~0 */
	obj_t BGl_zc3z04anonymousza32530ze3ze70z60zzast_walkz00(obj_t BgL_pz00_8628,
		obj_t BgL_l1546z00_3417)
	{
		{	/* Ast/walk.scm 299 */
			if (NULLP(BgL_l1546z00_3417))
				{	/* Ast/walk.scm 299 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 299 */
					obj_t BgL_arg2534z00_3420;
					obj_t BgL_arg2536z00_3421;

					{	/* Ast/walk.scm 299 */
						obj_t BgL_fz00_3422;

						BgL_fz00_3422 = CAR(((obj_t) BgL_l1546z00_3417));
						BgL_arg2534z00_3420 =
							BGL_PROCEDURE_CALL1(BgL_pz00_8628, BgL_fz00_3422);
					}
					{	/* Ast/walk.scm 299 */
						obj_t BgL_arg2537z00_3423;

						BgL_arg2537z00_3423 = CDR(((obj_t) BgL_l1546z00_3417));
						BgL_arg2536z00_3421 =
							BGl_zc3z04anonymousza32530ze3ze70z60zzast_walkz00(BgL_pz00_8628,
							BgL_arg2537z00_3423);
					}
					return bgl_append2(BgL_arg2534z00_3420, BgL_arg2536z00_3421);
				}
		}

	}



/* &walk3-extern1791 */
	obj_t BGl_z62walk3zd2extern1791zb0zzast_walkz00(obj_t BgL_envz00_8310,
		obj_t BgL_nz00_8311, obj_t BgL_pz00_8312, obj_t BgL_arg0z00_8313,
		obj_t BgL_arg1z00_8314, obj_t BgL_arg2z00_8315)
	{
		{	/* Ast/walk.scm 299 */
			{	/* Ast/walk.scm 299 */
				obj_t BgL_g1544z00_9636;

				BgL_g1544z00_9636 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nz00_8311)))->BgL_exprza2za2);
				{
					obj_t BgL_l1542z00_9638;

					{	/* Ast/walk.scm 299 */
						bool_t BgL_tmpz00_15028;

						BgL_l1542z00_9638 = BgL_g1544z00_9636;
					BgL_zc3z04anonymousza32525ze3z87_9637:
						if (PAIRP(BgL_l1542z00_9638))
							{	/* Ast/walk.scm 299 */
								{	/* Ast/walk.scm 299 */
									obj_t BgL_fz00_9639;

									BgL_fz00_9639 = CAR(BgL_l1542z00_9638);
									BGL_PROCEDURE_CALL4(BgL_pz00_8312, BgL_fz00_9639,
										BgL_arg0z00_8313, BgL_arg1z00_8314, BgL_arg2z00_8315);
								}
								{
									obj_t BgL_l1542z00_15039;

									BgL_l1542z00_15039 = CDR(BgL_l1542z00_9638);
									BgL_l1542z00_9638 = BgL_l1542z00_15039;
									goto BgL_zc3z04anonymousza32525ze3z87_9637;
								}
							}
						else
							{	/* Ast/walk.scm 299 */
								BgL_tmpz00_15028 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_15028);
					}
				}
			}
		}

	}



/* &walk2-extern1789 */
	obj_t BGl_z62walk2zd2extern1789zb0zzast_walkz00(obj_t BgL_envz00_8316,
		obj_t BgL_nz00_8317, obj_t BgL_pz00_8318, obj_t BgL_arg0z00_8319,
		obj_t BgL_arg1z00_8320)
	{
		{	/* Ast/walk.scm 299 */
			{	/* Ast/walk.scm 299 */
				obj_t BgL_g1541z00_9641;

				BgL_g1541z00_9641 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nz00_8317)))->BgL_exprza2za2);
				{
					obj_t BgL_l1539z00_9643;

					{	/* Ast/walk.scm 299 */
						bool_t BgL_tmpz00_15044;

						BgL_l1539z00_9643 = BgL_g1541z00_9641;
					BgL_zc3z04anonymousza32522ze3z87_9642:
						if (PAIRP(BgL_l1539z00_9643))
							{	/* Ast/walk.scm 299 */
								{	/* Ast/walk.scm 299 */
									obj_t BgL_fz00_9644;

									BgL_fz00_9644 = CAR(BgL_l1539z00_9643);
									BGL_PROCEDURE_CALL3(BgL_pz00_8318, BgL_fz00_9644,
										BgL_arg0z00_8319, BgL_arg1z00_8320);
								}
								{
									obj_t BgL_l1539z00_15054;

									BgL_l1539z00_15054 = CDR(BgL_l1539z00_9643);
									BgL_l1539z00_9643 = BgL_l1539z00_15054;
									goto BgL_zc3z04anonymousza32522ze3z87_9642;
								}
							}
						else
							{	/* Ast/walk.scm 299 */
								BgL_tmpz00_15044 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_15044);
					}
				}
			}
		}

	}



/* &walk1-extern1787 */
	obj_t BGl_z62walk1zd2extern1787zb0zzast_walkz00(obj_t BgL_envz00_8321,
		obj_t BgL_nz00_8322, obj_t BgL_pz00_8323, obj_t BgL_arg0z00_8324)
	{
		{	/* Ast/walk.scm 299 */
			{	/* Ast/walk.scm 299 */
				obj_t BgL_g1538z00_9646;

				BgL_g1538z00_9646 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nz00_8322)))->BgL_exprza2za2);
				{
					obj_t BgL_l1536z00_9648;

					{	/* Ast/walk.scm 299 */
						bool_t BgL_tmpz00_15059;

						BgL_l1536z00_9648 = BgL_g1538z00_9646;
					BgL_zc3z04anonymousza32519ze3z87_9647:
						if (PAIRP(BgL_l1536z00_9648))
							{	/* Ast/walk.scm 299 */
								{	/* Ast/walk.scm 299 */
									obj_t BgL_fz00_9649;

									BgL_fz00_9649 = CAR(BgL_l1536z00_9648);
									BGL_PROCEDURE_CALL2(BgL_pz00_8323, BgL_fz00_9649,
										BgL_arg0z00_8324);
								}
								{
									obj_t BgL_l1536z00_15068;

									BgL_l1536z00_15068 = CDR(BgL_l1536z00_9648);
									BgL_l1536z00_9648 = BgL_l1536z00_15068;
									goto BgL_zc3z04anonymousza32519ze3z87_9647;
								}
							}
						else
							{	/* Ast/walk.scm 299 */
								BgL_tmpz00_15059 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_15059);
					}
				}
			}
		}

	}



/* &walk0-extern1785 */
	obj_t BGl_z62walk0zd2extern1785zb0zzast_walkz00(obj_t BgL_envz00_8325,
		obj_t BgL_nz00_8326, obj_t BgL_pz00_8327)
	{
		{	/* Ast/walk.scm 299 */
			{	/* Ast/walk.scm 299 */
				obj_t BgL_g1535z00_9651;

				BgL_g1535z00_9651 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nz00_8326)))->BgL_exprza2za2);
				{
					obj_t BgL_l1533z00_9653;

					{	/* Ast/walk.scm 299 */
						bool_t BgL_tmpz00_15073;

						BgL_l1533z00_9653 = BgL_g1535z00_9651;
					BgL_zc3z04anonymousza32516ze3z87_9652:
						if (PAIRP(BgL_l1533z00_9653))
							{	/* Ast/walk.scm 299 */
								{	/* Ast/walk.scm 299 */
									obj_t BgL_fz00_9654;

									BgL_fz00_9654 = CAR(BgL_l1533z00_9653);
									BGL_PROCEDURE_CALL1(BgL_pz00_8327, BgL_fz00_9654);
								}
								{
									obj_t BgL_l1533z00_15081;

									BgL_l1533z00_15081 = CDR(BgL_l1533z00_9653);
									BgL_l1533z00_9653 = BgL_l1533z00_15081;
									goto BgL_zc3z04anonymousza32516ze3z87_9652;
								}
							}
						else
							{	/* Ast/walk.scm 299 */
								BgL_tmpz00_15073 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_15073);
					}
				}
			}
		}

	}



/* &walk3!-funcall1783 */
	BgL_nodez00_bglt BGl_z62walk3z12zd2funcall1783za2zzast_walkz00(obj_t
		BgL_envz00_8328, obj_t BgL_nz00_8329, obj_t BgL_pz00_8330,
		obj_t BgL_arg0z00_8331, obj_t BgL_arg1z00_8332, obj_t BgL_arg2z00_8333)
	{
		{	/* Ast/walk.scm 298 */
			{
				BgL_nodez00_bglt BgL_auxz00_15084;

				{	/* Ast/walk.scm 298 */
					BgL_nodez00_bglt BgL_arg2509z00_9656;

					BgL_arg2509z00_9656 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nz00_8329)))->BgL_funz00);
					BgL_auxz00_15084 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_8330,
							((obj_t) BgL_arg2509z00_9656), BgL_arg0z00_8331, BgL_arg1z00_8332,
							BgL_arg2z00_8333));
				}
				((((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nz00_8329)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_15084), BUNSPEC);
			}
			{
				obj_t BgL_fieldsz00_9658;

				BgL_fieldsz00_9658 =
					(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nz00_8329)))->BgL_argsz00);
			BgL_loopz00_9657:
				if (NULLP(BgL_fieldsz00_9658))
					{	/* Ast/walk.scm 298 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 298 */
						{	/* Ast/walk.scm 298 */
							obj_t BgL_arg2513z00_9659;

							{	/* Ast/walk.scm 298 */
								obj_t BgL_arg2514z00_9660;

								BgL_arg2514z00_9660 = CAR(((obj_t) BgL_fieldsz00_9658));
								BgL_arg2513z00_9659 =
									BGL_PROCEDURE_CALL4(BgL_pz00_8330, BgL_arg2514z00_9660,
									BgL_arg0z00_8331, BgL_arg1z00_8332, BgL_arg2z00_8333);
							}
							{	/* Ast/walk.scm 298 */
								obj_t BgL_tmpz00_15109;

								BgL_tmpz00_15109 = ((obj_t) BgL_fieldsz00_9658);
								SET_CAR(BgL_tmpz00_15109, BgL_arg2513z00_9659);
							}
						}
						{	/* Ast/walk.scm 298 */
							obj_t BgL_arg2515z00_9661;

							BgL_arg2515z00_9661 = CDR(((obj_t) BgL_fieldsz00_9658));
							{
								obj_t BgL_fieldsz00_15114;

								BgL_fieldsz00_15114 = BgL_arg2515z00_9661;
								BgL_fieldsz00_9658 = BgL_fieldsz00_15114;
								goto BgL_loopz00_9657;
							}
						}
					}
			}
			return ((BgL_nodez00_bglt) ((BgL_funcallz00_bglt) BgL_nz00_8329));
		}

	}



/* &walk2!-funcall1781 */
	BgL_nodez00_bglt BGl_z62walk2z12zd2funcall1781za2zzast_walkz00(obj_t
		BgL_envz00_8334, obj_t BgL_nz00_8335, obj_t BgL_pz00_8336,
		obj_t BgL_arg0z00_8337, obj_t BgL_arg1z00_8338)
	{
		{	/* Ast/walk.scm 298 */
			{
				BgL_nodez00_bglt BgL_auxz00_15119;

				{	/* Ast/walk.scm 298 */
					BgL_nodez00_bglt BgL_arg2501z00_9663;

					BgL_arg2501z00_9663 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nz00_8335)))->BgL_funz00);
					BgL_auxz00_15119 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_8336,
							((obj_t) BgL_arg2501z00_9663), BgL_arg0z00_8337,
							BgL_arg1z00_8338));
				}
				((((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nz00_8335)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_15119), BUNSPEC);
			}
			{
				obj_t BgL_fieldsz00_9665;

				BgL_fieldsz00_9665 =
					(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nz00_8335)))->BgL_argsz00);
			BgL_loopz00_9664:
				if (NULLP(BgL_fieldsz00_9665))
					{	/* Ast/walk.scm 298 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 298 */
						{	/* Ast/walk.scm 298 */
							obj_t BgL_arg2505z00_9666;

							{	/* Ast/walk.scm 298 */
								obj_t BgL_arg2506z00_9667;

								BgL_arg2506z00_9667 = CAR(((obj_t) BgL_fieldsz00_9665));
								BgL_arg2505z00_9666 =
									BGL_PROCEDURE_CALL3(BgL_pz00_8336, BgL_arg2506z00_9667,
									BgL_arg0z00_8337, BgL_arg1z00_8338);
							}
							{	/* Ast/walk.scm 298 */
								obj_t BgL_tmpz00_15142;

								BgL_tmpz00_15142 = ((obj_t) BgL_fieldsz00_9665);
								SET_CAR(BgL_tmpz00_15142, BgL_arg2505z00_9666);
							}
						}
						{	/* Ast/walk.scm 298 */
							obj_t BgL_arg2508z00_9668;

							BgL_arg2508z00_9668 = CDR(((obj_t) BgL_fieldsz00_9665));
							{
								obj_t BgL_fieldsz00_15147;

								BgL_fieldsz00_15147 = BgL_arg2508z00_9668;
								BgL_fieldsz00_9665 = BgL_fieldsz00_15147;
								goto BgL_loopz00_9664;
							}
						}
					}
			}
			return ((BgL_nodez00_bglt) ((BgL_funcallz00_bglt) BgL_nz00_8335));
		}

	}



/* &walk1!-funcall1779 */
	BgL_nodez00_bglt BGl_z62walk1z12zd2funcall1779za2zzast_walkz00(obj_t
		BgL_envz00_8339, obj_t BgL_nz00_8340, obj_t BgL_pz00_8341,
		obj_t BgL_arg0z00_8342)
	{
		{	/* Ast/walk.scm 298 */
			{
				BgL_nodez00_bglt BgL_auxz00_15152;

				{	/* Ast/walk.scm 298 */
					BgL_nodez00_bglt BgL_arg2490z00_9670;

					BgL_arg2490z00_9670 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nz00_8340)))->BgL_funz00);
					BgL_auxz00_15152 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_8341,
							((obj_t) BgL_arg2490z00_9670), BgL_arg0z00_8342));
				}
				((((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nz00_8340)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_15152), BUNSPEC);
			}
			{
				obj_t BgL_fieldsz00_9672;

				BgL_fieldsz00_9672 =
					(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nz00_8340)))->BgL_argsz00);
			BgL_loopz00_9671:
				if (NULLP(BgL_fieldsz00_9672))
					{	/* Ast/walk.scm 298 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 298 */
						{	/* Ast/walk.scm 298 */
							obj_t BgL_arg2495z00_9673;

							{	/* Ast/walk.scm 298 */
								obj_t BgL_arg2497z00_9674;

								BgL_arg2497z00_9674 = CAR(((obj_t) BgL_fieldsz00_9672));
								BgL_arg2495z00_9673 =
									BGL_PROCEDURE_CALL2(BgL_pz00_8341, BgL_arg2497z00_9674,
									BgL_arg0z00_8342);
							}
							{	/* Ast/walk.scm 298 */
								obj_t BgL_tmpz00_15173;

								BgL_tmpz00_15173 = ((obj_t) BgL_fieldsz00_9672);
								SET_CAR(BgL_tmpz00_15173, BgL_arg2495z00_9673);
							}
						}
						{	/* Ast/walk.scm 298 */
							obj_t BgL_arg2500z00_9675;

							BgL_arg2500z00_9675 = CDR(((obj_t) BgL_fieldsz00_9672));
							{
								obj_t BgL_fieldsz00_15178;

								BgL_fieldsz00_15178 = BgL_arg2500z00_9675;
								BgL_fieldsz00_9672 = BgL_fieldsz00_15178;
								goto BgL_loopz00_9671;
							}
						}
					}
			}
			return ((BgL_nodez00_bglt) ((BgL_funcallz00_bglt) BgL_nz00_8340));
		}

	}



/* &walk0!-funcall1777 */
	BgL_nodez00_bglt BGl_z62walk0z12zd2funcall1777za2zzast_walkz00(obj_t
		BgL_envz00_8343, obj_t BgL_nz00_8344, obj_t BgL_pz00_8345)
	{
		{	/* Ast/walk.scm 298 */
			{
				BgL_nodez00_bglt BgL_auxz00_15183;

				{	/* Ast/walk.scm 298 */
					BgL_nodez00_bglt BgL_arg2482z00_9677;

					BgL_arg2482z00_9677 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nz00_8344)))->BgL_funz00);
					BgL_auxz00_15183 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_8345, ((obj_t) BgL_arg2482z00_9677)));
				}
				((((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nz00_8344)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_15183), BUNSPEC);
			}
			{
				obj_t BgL_fieldsz00_9679;

				BgL_fieldsz00_9679 =
					(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nz00_8344)))->BgL_argsz00);
			BgL_loopz00_9678:
				if (NULLP(BgL_fieldsz00_9679))
					{	/* Ast/walk.scm 298 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 298 */
						{	/* Ast/walk.scm 298 */
							obj_t BgL_arg2486z00_9680;

							{	/* Ast/walk.scm 298 */
								obj_t BgL_arg2487z00_9681;

								BgL_arg2487z00_9681 = CAR(((obj_t) BgL_fieldsz00_9679));
								BgL_arg2486z00_9680 =
									BGL_PROCEDURE_CALL1(BgL_pz00_8345, BgL_arg2487z00_9681);
							}
							{	/* Ast/walk.scm 298 */
								obj_t BgL_tmpz00_15202;

								BgL_tmpz00_15202 = ((obj_t) BgL_fieldsz00_9679);
								SET_CAR(BgL_tmpz00_15202, BgL_arg2486z00_9680);
							}
						}
						{	/* Ast/walk.scm 298 */
							obj_t BgL_arg2488z00_9682;

							BgL_arg2488z00_9682 = CDR(((obj_t) BgL_fieldsz00_9679));
							{
								obj_t BgL_fieldsz00_15207;

								BgL_fieldsz00_15207 = BgL_arg2488z00_9682;
								BgL_fieldsz00_9679 = BgL_fieldsz00_15207;
								goto BgL_loopz00_9678;
							}
						}
					}
			}
			return ((BgL_nodez00_bglt) ((BgL_funcallz00_bglt) BgL_nz00_8344));
		}

	}



/* &walk3*-funcall1775 */
	obj_t BGl_z62walk3za2zd2funcall1775z12zzast_walkz00(obj_t BgL_envz00_8346,
		obj_t BgL_nz00_8347, obj_t BgL_pz00_8348, obj_t BgL_arg0z00_8349,
		obj_t BgL_arg1z00_8350, obj_t BgL_arg2z00_8351)
	{
		{	/* Ast/walk.scm 298 */
			{	/* Ast/walk.scm 298 */
				obj_t BgL_res4092z00_9687;

				{	/* Ast/walk.scm 298 */
					obj_t BgL_arg2471z00_9684;
					obj_t BgL_arg2473z00_9685;

					{	/* Ast/walk.scm 298 */
						BgL_nodez00_bglt BgL_arg2474z00_9686;

						BgL_arg2474z00_9686 =
							(((BgL_funcallz00_bglt) COBJECT(
									((BgL_funcallz00_bglt) BgL_nz00_8347)))->BgL_funz00);
						BgL_arg2471z00_9684 =
							BGL_PROCEDURE_CALL4(BgL_pz00_8348,
							((obj_t) BgL_arg2474z00_9686), BgL_arg0z00_8349, BgL_arg1z00_8350,
							BgL_arg2z00_8351);
					}
					BgL_arg2473z00_9685 =
						BGl_zc3z04anonymousza32475ze3ze70z60zzast_walkz00(BgL_arg2z00_8351,
						BgL_arg1z00_8350, BgL_arg0z00_8349, BgL_pz00_8348,
						(((BgL_funcallz00_bglt) COBJECT(((BgL_funcallz00_bglt)
										BgL_nz00_8347)))->BgL_argsz00));
					BgL_res4092z00_9687 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg2471z00_9684,
						BgL_arg2473z00_9685);
				}
				return BgL_res4092z00_9687;
			}
		}

	}



/* <@anonymous:2475>~0 */
	obj_t BGl_zc3z04anonymousza32475ze3ze70z60zzast_walkz00(obj_t
		BgL_arg2z00_8627, obj_t BgL_arg1z00_8626, obj_t BgL_arg0z00_8625,
		obj_t BgL_pz00_8624, obj_t BgL_l1531z00_3267)
	{
		{	/* Ast/walk.scm 298 */
			if (NULLP(BgL_l1531z00_3267))
				{	/* Ast/walk.scm 298 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 298 */
					obj_t BgL_arg2479z00_3270;
					obj_t BgL_arg2480z00_3271;

					{	/* Ast/walk.scm 298 */
						obj_t BgL_fz00_3272;

						BgL_fz00_3272 = CAR(((obj_t) BgL_l1531z00_3267));
						BgL_arg2479z00_3270 =
							BGL_PROCEDURE_CALL4(BgL_pz00_8624, BgL_fz00_3272,
							BgL_arg0z00_8625, BgL_arg1z00_8626, BgL_arg2z00_8627);
					}
					{	/* Ast/walk.scm 298 */
						obj_t BgL_arg2481z00_3273;

						BgL_arg2481z00_3273 = CDR(((obj_t) BgL_l1531z00_3267));
						BgL_arg2480z00_3271 =
							BGl_zc3z04anonymousza32475ze3ze70z60zzast_walkz00
							(BgL_arg2z00_8627, BgL_arg1z00_8626, BgL_arg0z00_8625,
							BgL_pz00_8624, BgL_arg2481z00_3273);
					}
					return bgl_append2(BgL_arg2479z00_3270, BgL_arg2480z00_3271);
				}
		}

	}



/* &walk2*-funcall1773 */
	obj_t BGl_z62walk2za2zd2funcall1773z12zzast_walkz00(obj_t BgL_envz00_8352,
		obj_t BgL_nz00_8353, obj_t BgL_pz00_8354, obj_t BgL_arg0z00_8355,
		obj_t BgL_arg1z00_8356)
	{
		{	/* Ast/walk.scm 298 */
			{	/* Ast/walk.scm 298 */
				obj_t BgL_res4093z00_9692;

				{	/* Ast/walk.scm 298 */
					obj_t BgL_arg2461z00_9689;
					obj_t BgL_arg2462z00_9690;

					{	/* Ast/walk.scm 298 */
						BgL_nodez00_bglt BgL_arg2463z00_9691;

						BgL_arg2463z00_9691 =
							(((BgL_funcallz00_bglt) COBJECT(
									((BgL_funcallz00_bglt) BgL_nz00_8353)))->BgL_funz00);
						BgL_arg2461z00_9689 =
							BGL_PROCEDURE_CALL3(BgL_pz00_8354,
							((obj_t) BgL_arg2463z00_9691), BgL_arg0z00_8355,
							BgL_arg1z00_8356);
					}
					BgL_arg2462z00_9690 =
						BGl_zc3z04anonymousza32464ze3ze70z60zzast_walkz00(BgL_arg1z00_8356,
						BgL_arg0z00_8355, BgL_pz00_8354,
						(((BgL_funcallz00_bglt) COBJECT(((BgL_funcallz00_bglt)
										BgL_nz00_8353)))->BgL_argsz00));
					BgL_res4093z00_9692 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg2461z00_9689,
						BgL_arg2462z00_9690);
				}
				return BgL_res4093z00_9692;
			}
		}

	}



/* <@anonymous:2464>~0 */
	obj_t BGl_zc3z04anonymousza32464ze3ze70z60zzast_walkz00(obj_t
		BgL_arg1z00_8623, obj_t BgL_arg0z00_8622, obj_t BgL_pz00_8621,
		obj_t BgL_l1528z00_3245)
	{
		{	/* Ast/walk.scm 298 */
			if (NULLP(BgL_l1528z00_3245))
				{	/* Ast/walk.scm 298 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 298 */
					obj_t BgL_arg2466z00_3248;
					obj_t BgL_arg2469z00_3249;

					{	/* Ast/walk.scm 298 */
						obj_t BgL_fz00_3250;

						BgL_fz00_3250 = CAR(((obj_t) BgL_l1528z00_3245));
						BgL_arg2466z00_3248 =
							BGL_PROCEDURE_CALL3(BgL_pz00_8621, BgL_fz00_3250,
							BgL_arg0z00_8622, BgL_arg1z00_8623);
					}
					{	/* Ast/walk.scm 298 */
						obj_t BgL_arg2470z00_3251;

						BgL_arg2470z00_3251 = CDR(((obj_t) BgL_l1528z00_3245));
						BgL_arg2469z00_3249 =
							BGl_zc3z04anonymousza32464ze3ze70z60zzast_walkz00
							(BgL_arg1z00_8623, BgL_arg0z00_8622, BgL_pz00_8621,
							BgL_arg2470z00_3251);
					}
					return bgl_append2(BgL_arg2466z00_3248, BgL_arg2469z00_3249);
				}
		}

	}



/* &walk1*-funcall1771 */
	obj_t BGl_z62walk1za2zd2funcall1771z12zzast_walkz00(obj_t BgL_envz00_8357,
		obj_t BgL_nz00_8358, obj_t BgL_pz00_8359, obj_t BgL_arg0z00_8360)
	{
		{	/* Ast/walk.scm 298 */
			{	/* Ast/walk.scm 298 */
				obj_t BgL_res4094z00_9697;

				{	/* Ast/walk.scm 298 */
					obj_t BgL_arg2452z00_9694;
					obj_t BgL_arg2453z00_9695;

					{	/* Ast/walk.scm 298 */
						BgL_nodez00_bglt BgL_arg2455z00_9696;

						BgL_arg2455z00_9696 =
							(((BgL_funcallz00_bglt) COBJECT(
									((BgL_funcallz00_bglt) BgL_nz00_8358)))->BgL_funz00);
						BgL_arg2452z00_9694 =
							BGL_PROCEDURE_CALL2(BgL_pz00_8359,
							((obj_t) BgL_arg2455z00_9696), BgL_arg0z00_8360);
					}
					BgL_arg2453z00_9695 =
						BGl_zc3z04anonymousza32456ze3ze70z60zzast_walkz00(BgL_arg0z00_8360,
						BgL_pz00_8359,
						(((BgL_funcallz00_bglt) COBJECT(((BgL_funcallz00_bglt)
										BgL_nz00_8358)))->BgL_argsz00));
					BgL_res4094z00_9697 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg2452z00_9694,
						BgL_arg2453z00_9695);
				}
				return BgL_res4094z00_9697;
			}
		}

	}



/* <@anonymous:2456>~0 */
	obj_t BGl_zc3z04anonymousza32456ze3ze70z60zzast_walkz00(obj_t
		BgL_arg0z00_8620, obj_t BgL_pz00_8619, obj_t BgL_l1525z00_3224)
	{
		{	/* Ast/walk.scm 298 */
			if (NULLP(BgL_l1525z00_3224))
				{	/* Ast/walk.scm 298 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 298 */
					obj_t BgL_arg2458z00_3227;
					obj_t BgL_arg2459z00_3228;

					{	/* Ast/walk.scm 298 */
						obj_t BgL_fz00_3229;

						BgL_fz00_3229 = CAR(((obj_t) BgL_l1525z00_3224));
						BgL_arg2458z00_3227 =
							BGL_PROCEDURE_CALL2(BgL_pz00_8619, BgL_fz00_3229,
							BgL_arg0z00_8620);
					}
					{	/* Ast/walk.scm 298 */
						obj_t BgL_arg2460z00_3230;

						BgL_arg2460z00_3230 = CDR(((obj_t) BgL_l1525z00_3224));
						BgL_arg2459z00_3228 =
							BGl_zc3z04anonymousza32456ze3ze70z60zzast_walkz00
							(BgL_arg0z00_8620, BgL_pz00_8619, BgL_arg2460z00_3230);
					}
					return bgl_append2(BgL_arg2458z00_3227, BgL_arg2459z00_3228);
				}
		}

	}



/* &walk0*-funcall1769 */
	obj_t BGl_z62walk0za2zd2funcall1769z12zzast_walkz00(obj_t BgL_envz00_8361,
		obj_t BgL_nz00_8362, obj_t BgL_pz00_8363)
	{
		{	/* Ast/walk.scm 298 */
			{	/* Ast/walk.scm 298 */
				obj_t BgL_res4095z00_9702;

				{	/* Ast/walk.scm 298 */
					obj_t BgL_arg2443z00_9699;
					obj_t BgL_arg2444z00_9700;

					{	/* Ast/walk.scm 298 */
						BgL_nodez00_bglt BgL_arg2445z00_9701;

						BgL_arg2445z00_9701 =
							(((BgL_funcallz00_bglt) COBJECT(
									((BgL_funcallz00_bglt) BgL_nz00_8362)))->BgL_funz00);
						BgL_arg2443z00_9699 =
							BGL_PROCEDURE_CALL1(BgL_pz00_8363, ((obj_t) BgL_arg2445z00_9701));
					}
					BgL_arg2444z00_9700 =
						BGl_zc3z04anonymousza32446ze3ze70z60zzast_walkz00(BgL_pz00_8363,
						(((BgL_funcallz00_bglt) COBJECT(
									((BgL_funcallz00_bglt) BgL_nz00_8362)))->BgL_argsz00));
					BgL_res4095z00_9702 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg2443z00_9699,
						BgL_arg2444z00_9700);
				}
				return BgL_res4095z00_9702;
			}
		}

	}



/* <@anonymous:2446>~0 */
	obj_t BGl_zc3z04anonymousza32446ze3ze70z60zzast_walkz00(obj_t BgL_pz00_8618,
		obj_t BgL_l1522z00_3204)
	{
		{	/* Ast/walk.scm 298 */
			if (NULLP(BgL_l1522z00_3204))
				{	/* Ast/walk.scm 298 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 298 */
					obj_t BgL_arg2449z00_3207;
					obj_t BgL_arg2450z00_3208;

					{	/* Ast/walk.scm 298 */
						obj_t BgL_fz00_3209;

						BgL_fz00_3209 = CAR(((obj_t) BgL_l1522z00_3204));
						BgL_arg2449z00_3207 =
							BGL_PROCEDURE_CALL1(BgL_pz00_8618, BgL_fz00_3209);
					}
					{	/* Ast/walk.scm 298 */
						obj_t BgL_arg2451z00_3210;

						BgL_arg2451z00_3210 = CDR(((obj_t) BgL_l1522z00_3204));
						BgL_arg2450z00_3208 =
							BGl_zc3z04anonymousza32446ze3ze70z60zzast_walkz00(BgL_pz00_8618,
							BgL_arg2451z00_3210);
					}
					return bgl_append2(BgL_arg2449z00_3207, BgL_arg2450z00_3208);
				}
		}

	}



/* &walk3-funcall1767 */
	obj_t BGl_z62walk3zd2funcall1767zb0zzast_walkz00(obj_t BgL_envz00_8364,
		obj_t BgL_nz00_8365, obj_t BgL_pz00_8366, obj_t BgL_arg0z00_8367,
		obj_t BgL_arg1z00_8368, obj_t BgL_arg2z00_8369)
	{
		{	/* Ast/walk.scm 298 */
			{	/* Ast/walk.scm 298 */
				BgL_nodez00_bglt BgL_arg2439z00_9704;

				BgL_arg2439z00_9704 =
					(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nz00_8365)))->BgL_funz00);
				BGL_PROCEDURE_CALL4(BgL_pz00_8366,
					((obj_t) BgL_arg2439z00_9704), BgL_arg0z00_8367, BgL_arg1z00_8368,
					BgL_arg2z00_8369);
			}
			{	/* Ast/walk.scm 298 */
				obj_t BgL_g1520z00_9705;

				BgL_g1520z00_9705 =
					(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nz00_8365)))->BgL_argsz00);
				{
					obj_t BgL_l1518z00_9707;

					{	/* Ast/walk.scm 298 */
						bool_t BgL_tmpz00_15328;

						BgL_l1518z00_9707 = BgL_g1520z00_9705;
					BgL_zc3z04anonymousza32440ze3z87_9706:
						if (PAIRP(BgL_l1518z00_9707))
							{	/* Ast/walk.scm 298 */
								{	/* Ast/walk.scm 298 */
									obj_t BgL_fz00_9708;

									BgL_fz00_9708 = CAR(BgL_l1518z00_9707);
									BGL_PROCEDURE_CALL4(BgL_pz00_8366, BgL_fz00_9708,
										BgL_arg0z00_8367, BgL_arg1z00_8368, BgL_arg2z00_8369);
								}
								{
									obj_t BgL_l1518z00_15339;

									BgL_l1518z00_15339 = CDR(BgL_l1518z00_9707);
									BgL_l1518z00_9707 = BgL_l1518z00_15339;
									goto BgL_zc3z04anonymousza32440ze3z87_9706;
								}
							}
						else
							{	/* Ast/walk.scm 298 */
								BgL_tmpz00_15328 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_15328);
					}
				}
			}
		}

	}



/* &walk2-funcall1765 */
	obj_t BGl_z62walk2zd2funcall1765zb0zzast_walkz00(obj_t BgL_envz00_8370,
		obj_t BgL_nz00_8371, obj_t BgL_pz00_8372, obj_t BgL_arg0z00_8373,
		obj_t BgL_arg1z00_8374)
	{
		{	/* Ast/walk.scm 298 */
			{	/* Ast/walk.scm 298 */
				BgL_nodez00_bglt BgL_arg2435z00_9710;

				BgL_arg2435z00_9710 =
					(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nz00_8371)))->BgL_funz00);
				BGL_PROCEDURE_CALL3(BgL_pz00_8372,
					((obj_t) BgL_arg2435z00_9710), BgL_arg0z00_8373, BgL_arg1z00_8374);
			}
			{	/* Ast/walk.scm 298 */
				obj_t BgL_g1517z00_9711;

				BgL_g1517z00_9711 =
					(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nz00_8371)))->BgL_argsz00);
				{
					obj_t BgL_l1515z00_9713;

					{	/* Ast/walk.scm 298 */
						bool_t BgL_tmpz00_15353;

						BgL_l1515z00_9713 = BgL_g1517z00_9711;
					BgL_zc3z04anonymousza32436ze3z87_9712:
						if (PAIRP(BgL_l1515z00_9713))
							{	/* Ast/walk.scm 298 */
								{	/* Ast/walk.scm 298 */
									obj_t BgL_fz00_9714;

									BgL_fz00_9714 = CAR(BgL_l1515z00_9713);
									BGL_PROCEDURE_CALL3(BgL_pz00_8372, BgL_fz00_9714,
										BgL_arg0z00_8373, BgL_arg1z00_8374);
								}
								{
									obj_t BgL_l1515z00_15363;

									BgL_l1515z00_15363 = CDR(BgL_l1515z00_9713);
									BgL_l1515z00_9713 = BgL_l1515z00_15363;
									goto BgL_zc3z04anonymousza32436ze3z87_9712;
								}
							}
						else
							{	/* Ast/walk.scm 298 */
								BgL_tmpz00_15353 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_15353);
					}
				}
			}
		}

	}



/* &walk1-funcall1763 */
	obj_t BGl_z62walk1zd2funcall1763zb0zzast_walkz00(obj_t BgL_envz00_8375,
		obj_t BgL_nz00_8376, obj_t BgL_pz00_8377, obj_t BgL_arg0z00_8378)
	{
		{	/* Ast/walk.scm 298 */
			{	/* Ast/walk.scm 298 */
				BgL_nodez00_bglt BgL_arg2430z00_9716;

				BgL_arg2430z00_9716 =
					(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nz00_8376)))->BgL_funz00);
				BGL_PROCEDURE_CALL2(BgL_pz00_8377,
					((obj_t) BgL_arg2430z00_9716), BgL_arg0z00_8378);
			}
			{	/* Ast/walk.scm 298 */
				obj_t BgL_g1514z00_9717;

				BgL_g1514z00_9717 =
					(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nz00_8376)))->BgL_argsz00);
				{
					obj_t BgL_l1512z00_9719;

					{	/* Ast/walk.scm 298 */
						bool_t BgL_tmpz00_15376;

						BgL_l1512z00_9719 = BgL_g1514z00_9717;
					BgL_zc3z04anonymousza32431ze3z87_9718:
						if (PAIRP(BgL_l1512z00_9719))
							{	/* Ast/walk.scm 298 */
								{	/* Ast/walk.scm 298 */
									obj_t BgL_fz00_9720;

									BgL_fz00_9720 = CAR(BgL_l1512z00_9719);
									BGL_PROCEDURE_CALL2(BgL_pz00_8377, BgL_fz00_9720,
										BgL_arg0z00_8378);
								}
								{
									obj_t BgL_l1512z00_15385;

									BgL_l1512z00_15385 = CDR(BgL_l1512z00_9719);
									BgL_l1512z00_9719 = BgL_l1512z00_15385;
									goto BgL_zc3z04anonymousza32431ze3z87_9718;
								}
							}
						else
							{	/* Ast/walk.scm 298 */
								BgL_tmpz00_15376 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_15376);
					}
				}
			}
		}

	}



/* &walk0-funcall1761 */
	obj_t BGl_z62walk0zd2funcall1761zb0zzast_walkz00(obj_t BgL_envz00_8379,
		obj_t BgL_nz00_8380, obj_t BgL_pz00_8381)
	{
		{	/* Ast/walk.scm 298 */
			{	/* Ast/walk.scm 298 */
				BgL_nodez00_bglt BgL_arg2426z00_9722;

				BgL_arg2426z00_9722 =
					(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nz00_8380)))->BgL_funz00);
				BGL_PROCEDURE_CALL1(BgL_pz00_8381, ((obj_t) BgL_arg2426z00_9722));
			}
			{	/* Ast/walk.scm 298 */
				obj_t BgL_g1511z00_9723;

				BgL_g1511z00_9723 =
					(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nz00_8380)))->BgL_argsz00);
				{
					obj_t BgL_l1509z00_9725;

					{	/* Ast/walk.scm 298 */
						bool_t BgL_tmpz00_15397;

						BgL_l1509z00_9725 = BgL_g1511z00_9723;
					BgL_zc3z04anonymousza32427ze3z87_9724:
						if (PAIRP(BgL_l1509z00_9725))
							{	/* Ast/walk.scm 298 */
								{	/* Ast/walk.scm 298 */
									obj_t BgL_fz00_9726;

									BgL_fz00_9726 = CAR(BgL_l1509z00_9725);
									BGL_PROCEDURE_CALL1(BgL_pz00_8381, BgL_fz00_9726);
								}
								{
									obj_t BgL_l1509z00_15405;

									BgL_l1509z00_15405 = CDR(BgL_l1509z00_9725);
									BgL_l1509z00_9725 = BgL_l1509z00_15405;
									goto BgL_zc3z04anonymousza32427ze3z87_9724;
								}
							}
						else
							{	/* Ast/walk.scm 298 */
								BgL_tmpz00_15397 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_15397);
					}
				}
			}
		}

	}



/* &walk3!-app-ly1759 */
	BgL_nodez00_bglt BGl_z62walk3z12zd2appzd2ly1759z70zzast_walkz00(obj_t
		BgL_envz00_8382, obj_t BgL_nz00_8383, obj_t BgL_pz00_8384,
		obj_t BgL_arg0z00_8385, obj_t BgL_arg1z00_8386, obj_t BgL_arg2z00_8387)
	{
		{	/* Ast/walk.scm 297 */
			{
				BgL_nodez00_bglt BgL_auxz00_15408;

				{	/* Ast/walk.scm 297 */
					BgL_nodez00_bglt BgL_arg2424z00_9728;

					BgL_arg2424z00_9728 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nz00_8383)))->BgL_funz00);
					BgL_auxz00_15408 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_8384,
							((obj_t) BgL_arg2424z00_9728), BgL_arg0z00_8385, BgL_arg1z00_8386,
							BgL_arg2z00_8387));
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nz00_8383)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_15408), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_15422;

				{	/* Ast/walk.scm 297 */
					BgL_nodez00_bglt BgL_arg2425z00_9729;

					BgL_arg2425z00_9729 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nz00_8383)))->BgL_argz00);
					BgL_auxz00_15422 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_8384,
							((obj_t) BgL_arg2425z00_9729), BgL_arg0z00_8385, BgL_arg1z00_8386,
							BgL_arg2z00_8387));
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nz00_8383)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_15422), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nz00_8383));
		}

	}



/* &walk2!-app-ly1757 */
	BgL_nodez00_bglt BGl_z62walk2z12zd2appzd2ly1757z70zzast_walkz00(obj_t
		BgL_envz00_8388, obj_t BgL_nz00_8389, obj_t BgL_pz00_8390,
		obj_t BgL_arg0z00_8391, obj_t BgL_arg1z00_8392)
	{
		{	/* Ast/walk.scm 297 */
			{
				BgL_nodez00_bglt BgL_auxz00_15438;

				{	/* Ast/walk.scm 297 */
					BgL_nodez00_bglt BgL_arg2421z00_9731;

					BgL_arg2421z00_9731 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nz00_8389)))->BgL_funz00);
					BgL_auxz00_15438 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_8390,
							((obj_t) BgL_arg2421z00_9731), BgL_arg0z00_8391,
							BgL_arg1z00_8392));
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nz00_8389)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_15438), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_15451;

				{	/* Ast/walk.scm 297 */
					BgL_nodez00_bglt BgL_arg2423z00_9732;

					BgL_arg2423z00_9732 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nz00_8389)))->BgL_argz00);
					BgL_auxz00_15451 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_8390,
							((obj_t) BgL_arg2423z00_9732), BgL_arg0z00_8391,
							BgL_arg1z00_8392));
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nz00_8389)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_15451), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nz00_8389));
		}

	}



/* &walk1!-app-ly1755 */
	BgL_nodez00_bglt BGl_z62walk1z12zd2appzd2ly1755z70zzast_walkz00(obj_t
		BgL_envz00_8393, obj_t BgL_nz00_8394, obj_t BgL_pz00_8395,
		obj_t BgL_arg0z00_8396)
	{
		{	/* Ast/walk.scm 297 */
			{
				BgL_nodez00_bglt BgL_auxz00_15466;

				{	/* Ast/walk.scm 297 */
					BgL_nodez00_bglt BgL_arg2419z00_9734;

					BgL_arg2419z00_9734 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nz00_8394)))->BgL_funz00);
					BgL_auxz00_15466 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_8395,
							((obj_t) BgL_arg2419z00_9734), BgL_arg0z00_8396));
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nz00_8394)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_15466), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_15478;

				{	/* Ast/walk.scm 297 */
					BgL_nodez00_bglt BgL_arg2420z00_9735;

					BgL_arg2420z00_9735 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nz00_8394)))->BgL_argz00);
					BgL_auxz00_15478 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_8395,
							((obj_t) BgL_arg2420z00_9735), BgL_arg0z00_8396));
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nz00_8394)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_15478), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nz00_8394));
		}

	}



/* &walk0!-app-ly1753 */
	BgL_nodez00_bglt BGl_z62walk0z12zd2appzd2ly1753z70zzast_walkz00(obj_t
		BgL_envz00_8397, obj_t BgL_nz00_8398, obj_t BgL_pz00_8399)
	{
		{	/* Ast/walk.scm 297 */
			{
				BgL_nodez00_bglt BgL_auxz00_15492;

				{	/* Ast/walk.scm 297 */
					BgL_nodez00_bglt BgL_arg2417z00_9737;

					BgL_arg2417z00_9737 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nz00_8398)))->BgL_funz00);
					BgL_auxz00_15492 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_8399, ((obj_t) BgL_arg2417z00_9737)));
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nz00_8398)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_15492), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_15503;

				{	/* Ast/walk.scm 297 */
					BgL_nodez00_bglt BgL_arg2418z00_9738;

					BgL_arg2418z00_9738 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nz00_8398)))->BgL_argz00);
					BgL_auxz00_15503 =
						((BgL_nodez00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_8399, ((obj_t) BgL_arg2418z00_9738)));
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nz00_8398)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_15503), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nz00_8398));
		}

	}



/* &walk3*-app-ly1751 */
	obj_t BGl_z62walk3za2zd2appzd2ly1751zc0zzast_walkz00(obj_t BgL_envz00_8400,
		obj_t BgL_nz00_8401, obj_t BgL_pz00_8402, obj_t BgL_arg0z00_8403,
		obj_t BgL_arg1z00_8404, obj_t BgL_arg2z00_8405)
	{
		{	/* Ast/walk.scm 297 */
			{	/* Ast/walk.scm 297 */
				obj_t BgL_res4096z00_9744;

				{	/* Ast/walk.scm 297 */
					obj_t BgL_arg2411z00_9740;
					obj_t BgL_arg2412z00_9741;

					{	/* Ast/walk.scm 297 */
						BgL_nodez00_bglt BgL_arg2414z00_9742;

						BgL_arg2414z00_9742 =
							(((BgL_appzd2lyzd2_bglt) COBJECT(
									((BgL_appzd2lyzd2_bglt) BgL_nz00_8401)))->BgL_funz00);
						BgL_arg2411z00_9740 =
							BGL_PROCEDURE_CALL4(BgL_pz00_8402,
							((obj_t) BgL_arg2414z00_9742), BgL_arg0z00_8403, BgL_arg1z00_8404,
							BgL_arg2z00_8405);
					}
					{	/* Ast/walk.scm 297 */
						BgL_nodez00_bglt BgL_arg2415z00_9743;

						BgL_arg2415z00_9743 =
							(((BgL_appzd2lyzd2_bglt) COBJECT(
									((BgL_appzd2lyzd2_bglt) BgL_nz00_8401)))->BgL_argz00);
						BgL_arg2412z00_9741 =
							BGL_PROCEDURE_CALL4(BgL_pz00_8402,
							((obj_t) BgL_arg2415z00_9743), BgL_arg0z00_8403, BgL_arg1z00_8404,
							BgL_arg2z00_8405);
					}
					BgL_res4096z00_9744 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg2411z00_9740,
						BgL_arg2412z00_9741);
				}
				return BgL_res4096z00_9744;
			}
		}

	}



/* &walk2*-app-ly1749 */
	obj_t BGl_z62walk2za2zd2appzd2ly1749zc0zzast_walkz00(obj_t BgL_envz00_8406,
		obj_t BgL_nz00_8407, obj_t BgL_pz00_8408, obj_t BgL_arg0z00_8409,
		obj_t BgL_arg1z00_8410)
	{
		{	/* Ast/walk.scm 297 */
			{	/* Ast/walk.scm 297 */
				obj_t BgL_res4097z00_9750;

				{	/* Ast/walk.scm 297 */
					obj_t BgL_arg2405z00_9746;
					obj_t BgL_arg2407z00_9747;

					{	/* Ast/walk.scm 297 */
						BgL_nodez00_bglt BgL_arg2408z00_9748;

						BgL_arg2408z00_9748 =
							(((BgL_appzd2lyzd2_bglt) COBJECT(
									((BgL_appzd2lyzd2_bglt) BgL_nz00_8407)))->BgL_funz00);
						BgL_arg2405z00_9746 =
							BGL_PROCEDURE_CALL3(BgL_pz00_8408,
							((obj_t) BgL_arg2408z00_9748), BgL_arg0z00_8409,
							BgL_arg1z00_8410);
					}
					{	/* Ast/walk.scm 297 */
						BgL_nodez00_bglt BgL_arg2410z00_9749;

						BgL_arg2410z00_9749 =
							(((BgL_appzd2lyzd2_bglt) COBJECT(
									((BgL_appzd2lyzd2_bglt) BgL_nz00_8407)))->BgL_argz00);
						BgL_arg2407z00_9747 =
							BGL_PROCEDURE_CALL3(BgL_pz00_8408,
							((obj_t) BgL_arg2410z00_9749), BgL_arg0z00_8409,
							BgL_arg1z00_8410);
					}
					BgL_res4097z00_9750 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg2405z00_9746,
						BgL_arg2407z00_9747);
				}
				return BgL_res4097z00_9750;
			}
		}

	}



/* &walk1*-app-ly1747 */
	obj_t BGl_z62walk1za2zd2appzd2ly1747zc0zzast_walkz00(obj_t BgL_envz00_8411,
		obj_t BgL_nz00_8412, obj_t BgL_pz00_8413, obj_t BgL_arg0z00_8414)
	{
		{	/* Ast/walk.scm 297 */
			{	/* Ast/walk.scm 297 */
				obj_t BgL_res4098z00_9756;

				{	/* Ast/walk.scm 297 */
					obj_t BgL_arg2401z00_9752;
					obj_t BgL_arg2402z00_9753;

					{	/* Ast/walk.scm 297 */
						BgL_nodez00_bglt BgL_arg2403z00_9754;

						BgL_arg2403z00_9754 =
							(((BgL_appzd2lyzd2_bglt) COBJECT(
									((BgL_appzd2lyzd2_bglt) BgL_nz00_8412)))->BgL_funz00);
						BgL_arg2401z00_9752 =
							BGL_PROCEDURE_CALL2(BgL_pz00_8413,
							((obj_t) BgL_arg2403z00_9754), BgL_arg0z00_8414);
					}
					{	/* Ast/walk.scm 297 */
						BgL_nodez00_bglt BgL_arg2404z00_9755;

						BgL_arg2404z00_9755 =
							(((BgL_appzd2lyzd2_bglt) COBJECT(
									((BgL_appzd2lyzd2_bglt) BgL_nz00_8412)))->BgL_argz00);
						BgL_arg2402z00_9753 =
							BGL_PROCEDURE_CALL2(BgL_pz00_8413,
							((obj_t) BgL_arg2404z00_9755), BgL_arg0z00_8414);
					}
					BgL_res4098z00_9756 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg2401z00_9752,
						BgL_arg2402z00_9753);
				}
				return BgL_res4098z00_9756;
			}
		}

	}



/* &walk0*-app-ly1745 */
	obj_t BGl_z62walk0za2zd2appzd2ly1745zc0zzast_walkz00(obj_t BgL_envz00_8415,
		obj_t BgL_nz00_8416, obj_t BgL_pz00_8417)
	{
		{	/* Ast/walk.scm 297 */
			{	/* Ast/walk.scm 297 */
				obj_t BgL_res4099z00_9762;

				{	/* Ast/walk.scm 297 */
					obj_t BgL_arg2396z00_9758;
					obj_t BgL_arg2397z00_9759;

					{	/* Ast/walk.scm 297 */
						BgL_nodez00_bglt BgL_arg2398z00_9760;

						BgL_arg2398z00_9760 =
							(((BgL_appzd2lyzd2_bglt) COBJECT(
									((BgL_appzd2lyzd2_bglt) BgL_nz00_8416)))->BgL_funz00);
						BgL_arg2396z00_9758 =
							BGL_PROCEDURE_CALL1(BgL_pz00_8417, ((obj_t) BgL_arg2398z00_9760));
					}
					{	/* Ast/walk.scm 297 */
						BgL_nodez00_bglt BgL_arg2399z00_9761;

						BgL_arg2399z00_9761 =
							(((BgL_appzd2lyzd2_bglt) COBJECT(
									((BgL_appzd2lyzd2_bglt) BgL_nz00_8416)))->BgL_argz00);
						BgL_arg2397z00_9759 =
							BGL_PROCEDURE_CALL1(BgL_pz00_8417, ((obj_t) BgL_arg2399z00_9761));
					}
					BgL_res4099z00_9762 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg2396z00_9758,
						BgL_arg2397z00_9759);
				}
				return BgL_res4099z00_9762;
			}
		}

	}



/* &walk3-app-ly1743 */
	obj_t BGl_z62walk3zd2appzd2ly1743z62zzast_walkz00(obj_t BgL_envz00_8418,
		obj_t BgL_nz00_8419, obj_t BgL_pz00_8420, obj_t BgL_arg0z00_8421,
		obj_t BgL_arg1z00_8422, obj_t BgL_arg2z00_8423)
	{
		{	/* Ast/walk.scm 297 */
			{	/* Ast/walk.scm 297 */
				BgL_nodez00_bglt BgL_arg2393z00_9764;

				BgL_arg2393z00_9764 =
					(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nz00_8419)))->BgL_funz00);
				BGL_PROCEDURE_CALL4(BgL_pz00_8420,
					((obj_t) BgL_arg2393z00_9764), BgL_arg0z00_8421, BgL_arg1z00_8422,
					BgL_arg2z00_8423);
			}
			{	/* Ast/walk.scm 297 */
				BgL_nodez00_bglt BgL_arg2395z00_9765;

				BgL_arg2395z00_9765 =
					(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nz00_8419)))->BgL_argz00);
				return
					BGL_PROCEDURE_CALL4(BgL_pz00_8420,
					((obj_t) BgL_arg2395z00_9765), BgL_arg0z00_8421, BgL_arg1z00_8422,
					BgL_arg2z00_8423);
			}
		}

	}



/* &walk2-app-ly1741 */
	obj_t BGl_z62walk2zd2appzd2ly1741z62zzast_walkz00(obj_t BgL_envz00_8424,
		obj_t BgL_nz00_8425, obj_t BgL_pz00_8426, obj_t BgL_arg0z00_8427,
		obj_t BgL_arg1z00_8428)
	{
		{	/* Ast/walk.scm 297 */
			{	/* Ast/walk.scm 297 */
				BgL_nodez00_bglt BgL_arg2391z00_9767;

				BgL_arg2391z00_9767 =
					(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nz00_8425)))->BgL_funz00);
				BGL_PROCEDURE_CALL3(BgL_pz00_8426,
					((obj_t) BgL_arg2391z00_9767), BgL_arg0z00_8427, BgL_arg1z00_8428);
			}
			{	/* Ast/walk.scm 297 */
				BgL_nodez00_bglt BgL_arg2392z00_9768;

				BgL_arg2392z00_9768 =
					(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nz00_8425)))->BgL_argz00);
				return
					BGL_PROCEDURE_CALL3(BgL_pz00_8426,
					((obj_t) BgL_arg2392z00_9768), BgL_arg0z00_8427, BgL_arg1z00_8428);
			}
		}

	}



/* &walk1-app-ly1739 */
	obj_t BGl_z62walk1zd2appzd2ly1739z62zzast_walkz00(obj_t BgL_envz00_8429,
		obj_t BgL_nz00_8430, obj_t BgL_pz00_8431, obj_t BgL_arg0z00_8432)
	{
		{	/* Ast/walk.scm 297 */
			{	/* Ast/walk.scm 297 */
				BgL_nodez00_bglt BgL_arg2389z00_9770;

				BgL_arg2389z00_9770 =
					(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nz00_8430)))->BgL_funz00);
				BGL_PROCEDURE_CALL2(BgL_pz00_8431,
					((obj_t) BgL_arg2389z00_9770), BgL_arg0z00_8432);
			}
			{	/* Ast/walk.scm 297 */
				BgL_nodez00_bglt BgL_arg2390z00_9771;

				BgL_arg2390z00_9771 =
					(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nz00_8430)))->BgL_argz00);
				return
					BGL_PROCEDURE_CALL2(BgL_pz00_8431,
					((obj_t) BgL_arg2390z00_9771), BgL_arg0z00_8432);
			}
		}

	}



/* &walk0-app-ly1737 */
	obj_t BGl_z62walk0zd2appzd2ly1737z62zzast_walkz00(obj_t BgL_envz00_8433,
		obj_t BgL_nz00_8434, obj_t BgL_pz00_8435)
	{
		{	/* Ast/walk.scm 297 */
			{	/* Ast/walk.scm 297 */
				BgL_nodez00_bglt BgL_arg2387z00_9773;

				BgL_arg2387z00_9773 =
					(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nz00_8434)))->BgL_funz00);
				BGL_PROCEDURE_CALL1(BgL_pz00_8435, ((obj_t) BgL_arg2387z00_9773));
			}
			{	/* Ast/walk.scm 297 */
				BgL_nodez00_bglt BgL_arg2388z00_9774;

				BgL_arg2388z00_9774 =
					(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nz00_8434)))->BgL_argz00);
				return
					BGL_PROCEDURE_CALL1(BgL_pz00_8435, ((obj_t) BgL_arg2388z00_9774));
			}
		}

	}



/* &walk3!-app1735 */
	BgL_nodez00_bglt BGl_z62walk3z12zd2app1735za2zzast_walkz00(obj_t
		BgL_envz00_8436, obj_t BgL_nz00_8437, obj_t BgL_pz00_8438,
		obj_t BgL_arg0z00_8439, obj_t BgL_arg1z00_8440, obj_t BgL_arg2z00_8441)
	{
		{	/* Ast/walk.scm 296 */
			{
				BgL_varz00_bglt BgL_auxz00_15656;

				{	/* Ast/walk.scm 296 */
					BgL_varz00_bglt BgL_arg2380z00_9776;

					BgL_arg2380z00_9776 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nz00_8437)))->BgL_funz00);
					BgL_auxz00_15656 =
						((BgL_varz00_bglt)
						BGL_PROCEDURE_CALL4(BgL_pz00_8438,
							((obj_t) BgL_arg2380z00_9776), BgL_arg0z00_8439, BgL_arg1z00_8440,
							BgL_arg2z00_8441));
				}
				((((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nz00_8437)))->BgL_funz00) =
					((BgL_varz00_bglt) BgL_auxz00_15656), BUNSPEC);
			}
			{
				obj_t BgL_fieldsz00_9778;

				BgL_fieldsz00_9778 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nz00_8437)))->BgL_argsz00);
			BgL_loopz00_9777:
				if (NULLP(BgL_fieldsz00_9778))
					{	/* Ast/walk.scm 296 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 296 */
						{	/* Ast/walk.scm 296 */
							obj_t BgL_arg2384z00_9779;

							{	/* Ast/walk.scm 296 */
								obj_t BgL_arg2385z00_9780;

								BgL_arg2385z00_9780 = CAR(((obj_t) BgL_fieldsz00_9778));
								BgL_arg2384z00_9779 =
									BGL_PROCEDURE_CALL4(BgL_pz00_8438, BgL_arg2385z00_9780,
									BgL_arg0z00_8439, BgL_arg1z00_8440, BgL_arg2z00_8441);
							}
							{	/* Ast/walk.scm 296 */
								obj_t BgL_tmpz00_15681;

								BgL_tmpz00_15681 = ((obj_t) BgL_fieldsz00_9778);
								SET_CAR(BgL_tmpz00_15681, BgL_arg2384z00_9779);
							}
						}
						{	/* Ast/walk.scm 296 */
							obj_t BgL_arg2386z00_9781;

							BgL_arg2386z00_9781 = CDR(((obj_t) BgL_fieldsz00_9778));
							{
								obj_t BgL_fieldsz00_15686;

								BgL_fieldsz00_15686 = BgL_arg2386z00_9781;
								BgL_fieldsz00_9778 = BgL_fieldsz00_15686;
								goto BgL_loopz00_9777;
							}
						}
					}
			}
			return ((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nz00_8437));
		}

	}



/* &walk2!-app1732 */
	BgL_nodez00_bglt BGl_z62walk2z12zd2app1732za2zzast_walkz00(obj_t
		BgL_envz00_8442, obj_t BgL_nz00_8443, obj_t BgL_pz00_8444,
		obj_t BgL_arg0z00_8445, obj_t BgL_arg1z00_8446)
	{
		{	/* Ast/walk.scm 296 */
			{
				BgL_varz00_bglt BgL_auxz00_15691;

				{	/* Ast/walk.scm 296 */
					BgL_varz00_bglt BgL_arg2373z00_9783;

					BgL_arg2373z00_9783 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nz00_8443)))->BgL_funz00);
					BgL_auxz00_15691 =
						((BgL_varz00_bglt)
						BGL_PROCEDURE_CALL3(BgL_pz00_8444,
							((obj_t) BgL_arg2373z00_9783), BgL_arg0z00_8445,
							BgL_arg1z00_8446));
				}
				((((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nz00_8443)))->BgL_funz00) =
					((BgL_varz00_bglt) BgL_auxz00_15691), BUNSPEC);
			}
			{
				obj_t BgL_fieldsz00_9785;

				BgL_fieldsz00_9785 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nz00_8443)))->BgL_argsz00);
			BgL_loopz00_9784:
				if (NULLP(BgL_fieldsz00_9785))
					{	/* Ast/walk.scm 296 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 296 */
						{	/* Ast/walk.scm 296 */
							obj_t BgL_arg2377z00_9786;

							{	/* Ast/walk.scm 296 */
								obj_t BgL_arg2378z00_9787;

								BgL_arg2378z00_9787 = CAR(((obj_t) BgL_fieldsz00_9785));
								BgL_arg2377z00_9786 =
									BGL_PROCEDURE_CALL3(BgL_pz00_8444, BgL_arg2378z00_9787,
									BgL_arg0z00_8445, BgL_arg1z00_8446);
							}
							{	/* Ast/walk.scm 296 */
								obj_t BgL_tmpz00_15714;

								BgL_tmpz00_15714 = ((obj_t) BgL_fieldsz00_9785);
								SET_CAR(BgL_tmpz00_15714, BgL_arg2377z00_9786);
							}
						}
						{	/* Ast/walk.scm 296 */
							obj_t BgL_arg2379z00_9788;

							BgL_arg2379z00_9788 = CDR(((obj_t) BgL_fieldsz00_9785));
							{
								obj_t BgL_fieldsz00_15719;

								BgL_fieldsz00_15719 = BgL_arg2379z00_9788;
								BgL_fieldsz00_9785 = BgL_fieldsz00_15719;
								goto BgL_loopz00_9784;
							}
						}
					}
			}
			return ((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nz00_8443));
		}

	}



/* &walk1!-app1730 */
	BgL_nodez00_bglt BGl_z62walk1z12zd2app1730za2zzast_walkz00(obj_t
		BgL_envz00_8447, obj_t BgL_nz00_8448, obj_t BgL_pz00_8449,
		obj_t BgL_arg0z00_8450)
	{
		{	/* Ast/walk.scm 296 */
			{
				BgL_varz00_bglt BgL_auxz00_15724;

				{	/* Ast/walk.scm 296 */
					BgL_varz00_bglt BgL_arg2365z00_9790;

					BgL_arg2365z00_9790 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nz00_8448)))->BgL_funz00);
					BgL_auxz00_15724 =
						((BgL_varz00_bglt)
						BGL_PROCEDURE_CALL2(BgL_pz00_8449,
							((obj_t) BgL_arg2365z00_9790), BgL_arg0z00_8450));
				}
				((((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nz00_8448)))->BgL_funz00) =
					((BgL_varz00_bglt) BgL_auxz00_15724), BUNSPEC);
			}
			{
				obj_t BgL_fieldsz00_9792;

				BgL_fieldsz00_9792 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nz00_8448)))->BgL_argsz00);
			BgL_loopz00_9791:
				if (NULLP(BgL_fieldsz00_9792))
					{	/* Ast/walk.scm 296 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 296 */
						{	/* Ast/walk.scm 296 */
							obj_t BgL_arg2369z00_9793;

							{	/* Ast/walk.scm 296 */
								obj_t BgL_arg2370z00_9794;

								BgL_arg2370z00_9794 = CAR(((obj_t) BgL_fieldsz00_9792));
								BgL_arg2369z00_9793 =
									BGL_PROCEDURE_CALL2(BgL_pz00_8449, BgL_arg2370z00_9794,
									BgL_arg0z00_8450);
							}
							{	/* Ast/walk.scm 296 */
								obj_t BgL_tmpz00_15745;

								BgL_tmpz00_15745 = ((obj_t) BgL_fieldsz00_9792);
								SET_CAR(BgL_tmpz00_15745, BgL_arg2369z00_9793);
							}
						}
						{	/* Ast/walk.scm 296 */
							obj_t BgL_arg2371z00_9795;

							BgL_arg2371z00_9795 = CDR(((obj_t) BgL_fieldsz00_9792));
							{
								obj_t BgL_fieldsz00_15750;

								BgL_fieldsz00_15750 = BgL_arg2371z00_9795;
								BgL_fieldsz00_9792 = BgL_fieldsz00_15750;
								goto BgL_loopz00_9791;
							}
						}
					}
			}
			return ((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nz00_8448));
		}

	}



/* &walk0!-app1728 */
	BgL_nodez00_bglt BGl_z62walk0z12zd2app1728za2zzast_walkz00(obj_t
		BgL_envz00_8451, obj_t BgL_nz00_8452, obj_t BgL_pz00_8453)
	{
		{	/* Ast/walk.scm 296 */
			{
				BgL_varz00_bglt BgL_auxz00_15755;

				{	/* Ast/walk.scm 296 */
					BgL_varz00_bglt BgL_arg2355z00_9797;

					BgL_arg2355z00_9797 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nz00_8452)))->BgL_funz00);
					BgL_auxz00_15755 =
						((BgL_varz00_bglt)
						BGL_PROCEDURE_CALL1(BgL_pz00_8453, ((obj_t) BgL_arg2355z00_9797)));
				}
				((((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nz00_8452)))->BgL_funz00) =
					((BgL_varz00_bglt) BgL_auxz00_15755), BUNSPEC);
			}
			{
				obj_t BgL_fieldsz00_9799;

				BgL_fieldsz00_9799 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nz00_8452)))->BgL_argsz00);
			BgL_loopz00_9798:
				if (NULLP(BgL_fieldsz00_9799))
					{	/* Ast/walk.scm 296 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 296 */
						{	/* Ast/walk.scm 296 */
							obj_t BgL_arg2361z00_9800;

							{	/* Ast/walk.scm 296 */
								obj_t BgL_arg2363z00_9801;

								BgL_arg2363z00_9801 = CAR(((obj_t) BgL_fieldsz00_9799));
								BgL_arg2361z00_9800 =
									BGL_PROCEDURE_CALL1(BgL_pz00_8453, BgL_arg2363z00_9801);
							}
							{	/* Ast/walk.scm 296 */
								obj_t BgL_tmpz00_15774;

								BgL_tmpz00_15774 = ((obj_t) BgL_fieldsz00_9799);
								SET_CAR(BgL_tmpz00_15774, BgL_arg2361z00_9800);
							}
						}
						{	/* Ast/walk.scm 296 */
							obj_t BgL_arg2364z00_9802;

							BgL_arg2364z00_9802 = CDR(((obj_t) BgL_fieldsz00_9799));
							{
								obj_t BgL_fieldsz00_15779;

								BgL_fieldsz00_15779 = BgL_arg2364z00_9802;
								BgL_fieldsz00_9799 = BgL_fieldsz00_15779;
								goto BgL_loopz00_9798;
							}
						}
					}
			}
			return ((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nz00_8452));
		}

	}



/* &walk3*-app1726 */
	obj_t BGl_z62walk3za2zd2app1726z12zzast_walkz00(obj_t BgL_envz00_8454,
		obj_t BgL_nz00_8455, obj_t BgL_pz00_8456, obj_t BgL_arg0z00_8457,
		obj_t BgL_arg1z00_8458, obj_t BgL_arg2z00_8459)
	{
		{	/* Ast/walk.scm 296 */
			{	/* Ast/walk.scm 296 */
				obj_t BgL_res4100z00_9807;

				{	/* Ast/walk.scm 296 */
					obj_t BgL_arg2346z00_9804;
					obj_t BgL_arg2348z00_9805;

					{	/* Ast/walk.scm 296 */
						BgL_varz00_bglt BgL_arg2349z00_9806;

						BgL_arg2349z00_9806 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt) BgL_nz00_8455)))->BgL_funz00);
						BgL_arg2346z00_9804 =
							BGL_PROCEDURE_CALL4(BgL_pz00_8456,
							((obj_t) BgL_arg2349z00_9806), BgL_arg0z00_8457, BgL_arg1z00_8458,
							BgL_arg2z00_8459);
					}
					BgL_arg2348z00_9805 =
						BGl_zc3z04anonymousza32350ze3ze70z60zzast_walkz00(BgL_arg2z00_8459,
						BgL_arg1z00_8458, BgL_arg0z00_8457, BgL_pz00_8456,
						(((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt) BgL_nz00_8455)))->
							BgL_argsz00));
					BgL_res4100z00_9807 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg2346z00_9804,
						BgL_arg2348z00_9805);
				}
				return BgL_res4100z00_9807;
			}
		}

	}



/* <@anonymous:2350>~0 */
	obj_t BGl_zc3z04anonymousza32350ze3ze70z60zzast_walkz00(obj_t
		BgL_arg2z00_8617, obj_t BgL_arg1z00_8616, obj_t BgL_arg0z00_8615,
		obj_t BgL_pz00_8614, obj_t BgL_l1507z00_2927)
	{
		{	/* Ast/walk.scm 296 */
			if (NULLP(BgL_l1507z00_2927))
				{	/* Ast/walk.scm 296 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 296 */
					obj_t BgL_arg2352z00_2930;
					obj_t BgL_arg2353z00_2931;

					{	/* Ast/walk.scm 296 */
						obj_t BgL_fz00_2932;

						BgL_fz00_2932 = CAR(((obj_t) BgL_l1507z00_2927));
						BgL_arg2352z00_2930 =
							BGL_PROCEDURE_CALL4(BgL_pz00_8614, BgL_fz00_2932,
							BgL_arg0z00_8615, BgL_arg1z00_8616, BgL_arg2z00_8617);
					}
					{	/* Ast/walk.scm 296 */
						obj_t BgL_arg2354z00_2933;

						BgL_arg2354z00_2933 = CDR(((obj_t) BgL_l1507z00_2927));
						BgL_arg2353z00_2931 =
							BGl_zc3z04anonymousza32350ze3ze70z60zzast_walkz00
							(BgL_arg2z00_8617, BgL_arg1z00_8616, BgL_arg0z00_8615,
							BgL_pz00_8614, BgL_arg2354z00_2933);
					}
					return bgl_append2(BgL_arg2352z00_2930, BgL_arg2353z00_2931);
				}
		}

	}



/* &walk2*-app1724 */
	obj_t BGl_z62walk2za2zd2app1724z12zzast_walkz00(obj_t BgL_envz00_8460,
		obj_t BgL_nz00_8461, obj_t BgL_pz00_8462, obj_t BgL_arg0z00_8463,
		obj_t BgL_arg1z00_8464)
	{
		{	/* Ast/walk.scm 296 */
			{	/* Ast/walk.scm 296 */
				obj_t BgL_res4101z00_9812;

				{	/* Ast/walk.scm 296 */
					obj_t BgL_arg2336z00_9809;
					obj_t BgL_arg2337z00_9810;

					{	/* Ast/walk.scm 296 */
						BgL_varz00_bglt BgL_arg2338z00_9811;

						BgL_arg2338z00_9811 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt) BgL_nz00_8461)))->BgL_funz00);
						BgL_arg2336z00_9809 =
							BGL_PROCEDURE_CALL3(BgL_pz00_8462,
							((obj_t) BgL_arg2338z00_9811), BgL_arg0z00_8463,
							BgL_arg1z00_8464);
					}
					BgL_arg2337z00_9810 =
						BGl_zc3z04anonymousza32339ze3ze70z60zzast_walkz00(BgL_arg1z00_8464,
						BgL_arg0z00_8463, BgL_pz00_8462,
						(((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt) BgL_nz00_8461)))->
							BgL_argsz00));
					BgL_res4101z00_9812 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg2336z00_9809,
						BgL_arg2337z00_9810);
				}
				return BgL_res4101z00_9812;
			}
		}

	}



/* <@anonymous:2339>~0 */
	obj_t BGl_zc3z04anonymousza32339ze3ze70z60zzast_walkz00(obj_t
		BgL_arg1z00_8613, obj_t BgL_arg0z00_8612, obj_t BgL_pz00_8611,
		obj_t BgL_l1504z00_2905)
	{
		{	/* Ast/walk.scm 296 */
			if (NULLP(BgL_l1504z00_2905))
				{	/* Ast/walk.scm 296 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 296 */
					obj_t BgL_arg2341z00_2908;
					obj_t BgL_arg2342z00_2909;

					{	/* Ast/walk.scm 296 */
						obj_t BgL_fz00_2910;

						BgL_fz00_2910 = CAR(((obj_t) BgL_l1504z00_2905));
						BgL_arg2341z00_2908 =
							BGL_PROCEDURE_CALL3(BgL_pz00_8611, BgL_fz00_2910,
							BgL_arg0z00_8612, BgL_arg1z00_8613);
					}
					{	/* Ast/walk.scm 296 */
						obj_t BgL_arg2345z00_2911;

						BgL_arg2345z00_2911 = CDR(((obj_t) BgL_l1504z00_2905));
						BgL_arg2342z00_2909 =
							BGl_zc3z04anonymousza32339ze3ze70z60zzast_walkz00
							(BgL_arg1z00_8613, BgL_arg0z00_8612, BgL_pz00_8611,
							BgL_arg2345z00_2911);
					}
					return bgl_append2(BgL_arg2341z00_2908, BgL_arg2342z00_2909);
				}
		}

	}



/* &walk1*-app1722 */
	obj_t BGl_z62walk1za2zd2app1722z12zzast_walkz00(obj_t BgL_envz00_8465,
		obj_t BgL_nz00_8466, obj_t BgL_pz00_8467, obj_t BgL_arg0z00_8468)
	{
		{	/* Ast/walk.scm 296 */
			{	/* Ast/walk.scm 296 */
				obj_t BgL_res4102z00_9817;

				{	/* Ast/walk.scm 296 */
					obj_t BgL_arg2326z00_9814;
					obj_t BgL_arg2327z00_9815;

					{	/* Ast/walk.scm 296 */
						BgL_varz00_bglt BgL_arg2328z00_9816;

						BgL_arg2328z00_9816 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt) BgL_nz00_8466)))->BgL_funz00);
						BgL_arg2326z00_9814 =
							BGL_PROCEDURE_CALL2(BgL_pz00_8467,
							((obj_t) BgL_arg2328z00_9816), BgL_arg0z00_8468);
					}
					BgL_arg2327z00_9815 =
						BGl_zc3z04anonymousza32329ze3ze70z60zzast_walkz00(BgL_arg0z00_8468,
						BgL_pz00_8467,
						(((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt) BgL_nz00_8466)))->
							BgL_argsz00));
					BgL_res4102z00_9817 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg2326z00_9814,
						BgL_arg2327z00_9815);
				}
				return BgL_res4102z00_9817;
			}
		}

	}



/* <@anonymous:2329>~0 */
	obj_t BGl_zc3z04anonymousza32329ze3ze70z60zzast_walkz00(obj_t
		BgL_arg0z00_8610, obj_t BgL_pz00_8609, obj_t BgL_l1501z00_2884)
	{
		{	/* Ast/walk.scm 296 */
			if (NULLP(BgL_l1501z00_2884))
				{	/* Ast/walk.scm 296 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 296 */
					obj_t BgL_arg2331z00_2887;
					obj_t BgL_arg2333z00_2888;

					{	/* Ast/walk.scm 296 */
						obj_t BgL_fz00_2889;

						BgL_fz00_2889 = CAR(((obj_t) BgL_l1501z00_2884));
						BgL_arg2331z00_2887 =
							BGL_PROCEDURE_CALL2(BgL_pz00_8609, BgL_fz00_2889,
							BgL_arg0z00_8610);
					}
					{	/* Ast/walk.scm 296 */
						obj_t BgL_arg2335z00_2890;

						BgL_arg2335z00_2890 = CDR(((obj_t) BgL_l1501z00_2884));
						BgL_arg2333z00_2888 =
							BGl_zc3z04anonymousza32329ze3ze70z60zzast_walkz00
							(BgL_arg0z00_8610, BgL_pz00_8609, BgL_arg2335z00_2890);
					}
					return bgl_append2(BgL_arg2331z00_2887, BgL_arg2333z00_2888);
				}
		}

	}



/* &walk0*-app1720 */
	obj_t BGl_z62walk0za2zd2app1720z12zzast_walkz00(obj_t BgL_envz00_8469,
		obj_t BgL_nz00_8470, obj_t BgL_pz00_8471)
	{
		{	/* Ast/walk.scm 296 */
			{	/* Ast/walk.scm 296 */
				obj_t BgL_res4103z00_9822;

				{	/* Ast/walk.scm 296 */
					obj_t BgL_arg2318z00_9819;
					obj_t BgL_arg2319z00_9820;

					{	/* Ast/walk.scm 296 */
						BgL_varz00_bglt BgL_arg2320z00_9821;

						BgL_arg2320z00_9821 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt) BgL_nz00_8470)))->BgL_funz00);
						BgL_arg2318z00_9819 =
							BGL_PROCEDURE_CALL1(BgL_pz00_8471, ((obj_t) BgL_arg2320z00_9821));
					}
					BgL_arg2319z00_9820 =
						BGl_zc3z04anonymousza32321ze3ze70z60zzast_walkz00(BgL_pz00_8471,
						(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt) BgL_nz00_8470)))->BgL_argsz00));
					BgL_res4103z00_9822 =
						BGl_appendzd221011zd2zzast_walkz00(BgL_arg2318z00_9819,
						BgL_arg2319z00_9820);
				}
				return BgL_res4103z00_9822;
			}
		}

	}



/* <@anonymous:2321>~0 */
	obj_t BGl_zc3z04anonymousza32321ze3ze70z60zzast_walkz00(obj_t BgL_pz00_8608,
		obj_t BgL_l1498z00_2864)
	{
		{	/* Ast/walk.scm 296 */
			if (NULLP(BgL_l1498z00_2864))
				{	/* Ast/walk.scm 296 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 296 */
					obj_t BgL_arg2323z00_2867;
					obj_t BgL_arg2324z00_2868;

					{	/* Ast/walk.scm 296 */
						obj_t BgL_fz00_2869;

						BgL_fz00_2869 = CAR(((obj_t) BgL_l1498z00_2864));
						BgL_arg2323z00_2867 =
							BGL_PROCEDURE_CALL1(BgL_pz00_8608, BgL_fz00_2869);
					}
					{	/* Ast/walk.scm 296 */
						obj_t BgL_arg2325z00_2870;

						BgL_arg2325z00_2870 = CDR(((obj_t) BgL_l1498z00_2864));
						BgL_arg2324z00_2868 =
							BGl_zc3z04anonymousza32321ze3ze70z60zzast_walkz00(BgL_pz00_8608,
							BgL_arg2325z00_2870);
					}
					return bgl_append2(BgL_arg2323z00_2867, BgL_arg2324z00_2868);
				}
		}

	}



/* &walk3-app1718 */
	obj_t BGl_z62walk3zd2app1718zb0zzast_walkz00(obj_t BgL_envz00_8472,
		obj_t BgL_nz00_8473, obj_t BgL_pz00_8474, obj_t BgL_arg0z00_8475,
		obj_t BgL_arg1z00_8476, obj_t BgL_arg2z00_8477)
	{
		{	/* Ast/walk.scm 296 */
			{	/* Ast/walk.scm 296 */
				BgL_varz00_bglt BgL_arg2314z00_9824;

				BgL_arg2314z00_9824 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nz00_8473)))->BgL_funz00);
				BGL_PROCEDURE_CALL4(BgL_pz00_8474,
					((obj_t) BgL_arg2314z00_9824), BgL_arg0z00_8475, BgL_arg1z00_8476,
					BgL_arg2z00_8477);
			}
			{	/* Ast/walk.scm 296 */
				obj_t BgL_g1496z00_9825;

				BgL_g1496z00_9825 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nz00_8473)))->BgL_argsz00);
				{
					obj_t BgL_l1494z00_9827;

					{	/* Ast/walk.scm 296 */
						bool_t BgL_tmpz00_15900;

						BgL_l1494z00_9827 = BgL_g1496z00_9825;
					BgL_zc3z04anonymousza32315ze3z87_9826:
						if (PAIRP(BgL_l1494z00_9827))
							{	/* Ast/walk.scm 296 */
								{	/* Ast/walk.scm 296 */
									obj_t BgL_fz00_9828;

									BgL_fz00_9828 = CAR(BgL_l1494z00_9827);
									BGL_PROCEDURE_CALL4(BgL_pz00_8474, BgL_fz00_9828,
										BgL_arg0z00_8475, BgL_arg1z00_8476, BgL_arg2z00_8477);
								}
								{
									obj_t BgL_l1494z00_15911;

									BgL_l1494z00_15911 = CDR(BgL_l1494z00_9827);
									BgL_l1494z00_9827 = BgL_l1494z00_15911;
									goto BgL_zc3z04anonymousza32315ze3z87_9826;
								}
							}
						else
							{	/* Ast/walk.scm 296 */
								BgL_tmpz00_15900 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_15900);
					}
				}
			}
		}

	}



/* &walk2-app1716 */
	obj_t BGl_z62walk2zd2app1716zb0zzast_walkz00(obj_t BgL_envz00_8478,
		obj_t BgL_nz00_8479, obj_t BgL_pz00_8480, obj_t BgL_arg0z00_8481,
		obj_t BgL_arg1z00_8482)
	{
		{	/* Ast/walk.scm 296 */
			{	/* Ast/walk.scm 296 */
				BgL_varz00_bglt BgL_arg2310z00_9830;

				BgL_arg2310z00_9830 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nz00_8479)))->BgL_funz00);
				BGL_PROCEDURE_CALL3(BgL_pz00_8480,
					((obj_t) BgL_arg2310z00_9830), BgL_arg0z00_8481, BgL_arg1z00_8482);
			}
			{	/* Ast/walk.scm 296 */
				obj_t BgL_g1493z00_9831;

				BgL_g1493z00_9831 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nz00_8479)))->BgL_argsz00);
				{
					obj_t BgL_l1491z00_9833;

					{	/* Ast/walk.scm 296 */
						bool_t BgL_tmpz00_15925;

						BgL_l1491z00_9833 = BgL_g1493z00_9831;
					BgL_zc3z04anonymousza32311ze3z87_9832:
						if (PAIRP(BgL_l1491z00_9833))
							{	/* Ast/walk.scm 296 */
								{	/* Ast/walk.scm 296 */
									obj_t BgL_fz00_9834;

									BgL_fz00_9834 = CAR(BgL_l1491z00_9833);
									BGL_PROCEDURE_CALL3(BgL_pz00_8480, BgL_fz00_9834,
										BgL_arg0z00_8481, BgL_arg1z00_8482);
								}
								{
									obj_t BgL_l1491z00_15935;

									BgL_l1491z00_15935 = CDR(BgL_l1491z00_9833);
									BgL_l1491z00_9833 = BgL_l1491z00_15935;
									goto BgL_zc3z04anonymousza32311ze3z87_9832;
								}
							}
						else
							{	/* Ast/walk.scm 296 */
								BgL_tmpz00_15925 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_15925);
					}
				}
			}
		}

	}



/* &walk1-app1714 */
	obj_t BGl_z62walk1zd2app1714zb0zzast_walkz00(obj_t BgL_envz00_8483,
		obj_t BgL_nz00_8484, obj_t BgL_pz00_8485, obj_t BgL_arg0z00_8486)
	{
		{	/* Ast/walk.scm 296 */
			{	/* Ast/walk.scm 296 */
				BgL_varz00_bglt BgL_arg2306z00_9836;

				BgL_arg2306z00_9836 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nz00_8484)))->BgL_funz00);
				BGL_PROCEDURE_CALL2(BgL_pz00_8485,
					((obj_t) BgL_arg2306z00_9836), BgL_arg0z00_8486);
			}
			{	/* Ast/walk.scm 296 */
				obj_t BgL_g1490z00_9837;

				BgL_g1490z00_9837 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nz00_8484)))->BgL_argsz00);
				{
					obj_t BgL_l1488z00_9839;

					{	/* Ast/walk.scm 296 */
						bool_t BgL_tmpz00_15948;

						BgL_l1488z00_9839 = BgL_g1490z00_9837;
					BgL_zc3z04anonymousza32307ze3z87_9838:
						if (PAIRP(BgL_l1488z00_9839))
							{	/* Ast/walk.scm 296 */
								{	/* Ast/walk.scm 296 */
									obj_t BgL_fz00_9840;

									BgL_fz00_9840 = CAR(BgL_l1488z00_9839);
									BGL_PROCEDURE_CALL2(BgL_pz00_8485, BgL_fz00_9840,
										BgL_arg0z00_8486);
								}
								{
									obj_t BgL_l1488z00_15957;

									BgL_l1488z00_15957 = CDR(BgL_l1488z00_9839);
									BgL_l1488z00_9839 = BgL_l1488z00_15957;
									goto BgL_zc3z04anonymousza32307ze3z87_9838;
								}
							}
						else
							{	/* Ast/walk.scm 296 */
								BgL_tmpz00_15948 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_15948);
					}
				}
			}
		}

	}



/* &walk0-app1712 */
	obj_t BGl_z62walk0zd2app1712zb0zzast_walkz00(obj_t BgL_envz00_8487,
		obj_t BgL_nz00_8488, obj_t BgL_pz00_8489)
	{
		{	/* Ast/walk.scm 296 */
			{	/* Ast/walk.scm 296 */
				BgL_varz00_bglt BgL_arg2302z00_9842;

				BgL_arg2302z00_9842 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nz00_8488)))->BgL_funz00);
				BGL_PROCEDURE_CALL1(BgL_pz00_8489, ((obj_t) BgL_arg2302z00_9842));
			}
			{	/* Ast/walk.scm 296 */
				obj_t BgL_g1487z00_9843;

				BgL_g1487z00_9843 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nz00_8488)))->BgL_argsz00);
				{
					obj_t BgL_l1485z00_9845;

					{	/* Ast/walk.scm 296 */
						bool_t BgL_tmpz00_15969;

						BgL_l1485z00_9845 = BgL_g1487z00_9843;
					BgL_zc3z04anonymousza32303ze3z87_9844:
						if (PAIRP(BgL_l1485z00_9845))
							{	/* Ast/walk.scm 296 */
								{	/* Ast/walk.scm 296 */
									obj_t BgL_fz00_9846;

									BgL_fz00_9846 = CAR(BgL_l1485z00_9845);
									BGL_PROCEDURE_CALL1(BgL_pz00_8489, BgL_fz00_9846);
								}
								{
									obj_t BgL_l1485z00_15977;

									BgL_l1485z00_15977 = CDR(BgL_l1485z00_9845);
									BgL_l1485z00_9845 = BgL_l1485z00_15977;
									goto BgL_zc3z04anonymousza32303ze3z87_9844;
								}
							}
						else
							{	/* Ast/walk.scm 296 */
								BgL_tmpz00_15969 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_15969);
					}
				}
			}
		}

	}



/* &walk3!-sequence1710 */
	BgL_nodez00_bglt BGl_z62walk3z12zd2sequence1710za2zzast_walkz00(obj_t
		BgL_envz00_8490, obj_t BgL_nz00_8491, obj_t BgL_pz00_8492,
		obj_t BgL_arg0z00_8493, obj_t BgL_arg1z00_8494, obj_t BgL_arg2z00_8495)
	{
		{	/* Ast/walk.scm 295 */
			{
				obj_t BgL_fieldsz00_9849;

				BgL_fieldsz00_9849 =
					(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nz00_8491)))->BgL_nodesz00);
			BgL_loopz00_9848:
				if (NULLP(BgL_fieldsz00_9849))
					{	/* Ast/walk.scm 295 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 295 */
						{	/* Ast/walk.scm 295 */
							obj_t BgL_arg2298z00_9850;

							{	/* Ast/walk.scm 295 */
								obj_t BgL_arg2299z00_9851;

								BgL_arg2299z00_9851 = CAR(((obj_t) BgL_fieldsz00_9849));
								BgL_arg2298z00_9850 =
									BGL_PROCEDURE_CALL4(BgL_pz00_8492, BgL_arg2299z00_9851,
									BgL_arg0z00_8493, BgL_arg1z00_8494, BgL_arg2z00_8495);
							}
							{	/* Ast/walk.scm 295 */
								obj_t BgL_tmpz00_15991;

								BgL_tmpz00_15991 = ((obj_t) BgL_fieldsz00_9849);
								SET_CAR(BgL_tmpz00_15991, BgL_arg2298z00_9850);
							}
						}
						{	/* Ast/walk.scm 295 */
							obj_t BgL_arg2301z00_9852;

							BgL_arg2301z00_9852 = CDR(((obj_t) BgL_fieldsz00_9849));
							{
								obj_t BgL_fieldsz00_15996;

								BgL_fieldsz00_15996 = BgL_arg2301z00_9852;
								BgL_fieldsz00_9849 = BgL_fieldsz00_15996;
								goto BgL_loopz00_9848;
							}
						}
					}
			}
			return ((BgL_nodez00_bglt) ((BgL_sequencez00_bglt) BgL_nz00_8491));
		}

	}



/* &walk2!-sequence1708 */
	BgL_nodez00_bglt BGl_z62walk2z12zd2sequence1708za2zzast_walkz00(obj_t
		BgL_envz00_8496, obj_t BgL_nz00_8497, obj_t BgL_pz00_8498,
		obj_t BgL_arg0z00_8499, obj_t BgL_arg1z00_8500)
	{
		{	/* Ast/walk.scm 295 */
			{
				obj_t BgL_fieldsz00_9855;

				BgL_fieldsz00_9855 =
					(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nz00_8497)))->BgL_nodesz00);
			BgL_loopz00_9854:
				if (NULLP(BgL_fieldsz00_9855))
					{	/* Ast/walk.scm 295 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 295 */
						{	/* Ast/walk.scm 295 */
							obj_t BgL_arg2292z00_9856;

							{	/* Ast/walk.scm 295 */
								obj_t BgL_arg2293z00_9857;

								BgL_arg2293z00_9857 = CAR(((obj_t) BgL_fieldsz00_9855));
								BgL_arg2292z00_9856 =
									BGL_PROCEDURE_CALL3(BgL_pz00_8498, BgL_arg2293z00_9857,
									BgL_arg0z00_8499, BgL_arg1z00_8500);
							}
							{	/* Ast/walk.scm 295 */
								obj_t BgL_tmpz00_16011;

								BgL_tmpz00_16011 = ((obj_t) BgL_fieldsz00_9855);
								SET_CAR(BgL_tmpz00_16011, BgL_arg2292z00_9856);
							}
						}
						{	/* Ast/walk.scm 295 */
							obj_t BgL_arg2294z00_9858;

							BgL_arg2294z00_9858 = CDR(((obj_t) BgL_fieldsz00_9855));
							{
								obj_t BgL_fieldsz00_16016;

								BgL_fieldsz00_16016 = BgL_arg2294z00_9858;
								BgL_fieldsz00_9855 = BgL_fieldsz00_16016;
								goto BgL_loopz00_9854;
							}
						}
					}
			}
			return ((BgL_nodez00_bglt) ((BgL_sequencez00_bglt) BgL_nz00_8497));
		}

	}



/* &walk1!-sequence1705 */
	BgL_nodez00_bglt BGl_z62walk1z12zd2sequence1705za2zzast_walkz00(obj_t
		BgL_envz00_8501, obj_t BgL_nz00_8502, obj_t BgL_pz00_8503,
		obj_t BgL_arg0z00_8504)
	{
		{	/* Ast/walk.scm 295 */
			{
				obj_t BgL_fieldsz00_9861;

				BgL_fieldsz00_9861 =
					(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nz00_8502)))->BgL_nodesz00);
			BgL_loopz00_9860:
				if (NULLP(BgL_fieldsz00_9861))
					{	/* Ast/walk.scm 295 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 295 */
						{	/* Ast/walk.scm 295 */
							obj_t BgL_arg2286z00_9862;

							{	/* Ast/walk.scm 295 */
								obj_t BgL_arg2287z00_9863;

								BgL_arg2287z00_9863 = CAR(((obj_t) BgL_fieldsz00_9861));
								BgL_arg2286z00_9862 =
									BGL_PROCEDURE_CALL2(BgL_pz00_8503, BgL_arg2287z00_9863,
									BgL_arg0z00_8504);
							}
							{	/* Ast/walk.scm 295 */
								obj_t BgL_tmpz00_16030;

								BgL_tmpz00_16030 = ((obj_t) BgL_fieldsz00_9861);
								SET_CAR(BgL_tmpz00_16030, BgL_arg2286z00_9862);
							}
						}
						{	/* Ast/walk.scm 295 */
							obj_t BgL_arg2288z00_9864;

							BgL_arg2288z00_9864 = CDR(((obj_t) BgL_fieldsz00_9861));
							{
								obj_t BgL_fieldsz00_16035;

								BgL_fieldsz00_16035 = BgL_arg2288z00_9864;
								BgL_fieldsz00_9861 = BgL_fieldsz00_16035;
								goto BgL_loopz00_9860;
							}
						}
					}
			}
			return ((BgL_nodez00_bglt) ((BgL_sequencez00_bglt) BgL_nz00_8502));
		}

	}



/* &walk0!-sequence1703 */
	BgL_nodez00_bglt BGl_z62walk0z12zd2sequence1703za2zzast_walkz00(obj_t
		BgL_envz00_8505, obj_t BgL_nz00_8506, obj_t BgL_pz00_8507)
	{
		{	/* Ast/walk.scm 295 */
			{
				obj_t BgL_fieldsz00_9867;

				BgL_fieldsz00_9867 =
					(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nz00_8506)))->BgL_nodesz00);
			BgL_loopz00_9866:
				if (NULLP(BgL_fieldsz00_9867))
					{	/* Ast/walk.scm 295 */
						((bool_t) 0);
					}
				else
					{	/* Ast/walk.scm 295 */
						{	/* Ast/walk.scm 295 */
							obj_t BgL_arg2279z00_9868;

							{	/* Ast/walk.scm 295 */
								obj_t BgL_arg2280z00_9869;

								BgL_arg2280z00_9869 = CAR(((obj_t) BgL_fieldsz00_9867));
								BgL_arg2279z00_9868 =
									BGL_PROCEDURE_CALL1(BgL_pz00_8507, BgL_arg2280z00_9869);
							}
							{	/* Ast/walk.scm 295 */
								obj_t BgL_tmpz00_16048;

								BgL_tmpz00_16048 = ((obj_t) BgL_fieldsz00_9867);
								SET_CAR(BgL_tmpz00_16048, BgL_arg2279z00_9868);
							}
						}
						{	/* Ast/walk.scm 295 */
							obj_t BgL_arg2281z00_9870;

							BgL_arg2281z00_9870 = CDR(((obj_t) BgL_fieldsz00_9867));
							{
								obj_t BgL_fieldsz00_16053;

								BgL_fieldsz00_16053 = BgL_arg2281z00_9870;
								BgL_fieldsz00_9867 = BgL_fieldsz00_16053;
								goto BgL_loopz00_9866;
							}
						}
					}
			}
			return ((BgL_nodez00_bglt) ((BgL_sequencez00_bglt) BgL_nz00_8506));
		}

	}



/* &walk3*-sequence1701 */
	obj_t BGl_z62walk3za2zd2sequence1701z12zzast_walkz00(obj_t BgL_envz00_8508,
		obj_t BgL_nz00_8509, obj_t BgL_pz00_8510, obj_t BgL_arg0z00_8511,
		obj_t BgL_arg1z00_8512, obj_t BgL_arg2z00_8513)
	{
		{	/* Ast/walk.scm 295 */
			{	/* Ast/walk.scm 295 */
				obj_t BgL_res4104z00_9874;

				{	/* Ast/walk.scm 295 */
					obj_t BgL_arg2269z00_9872;

					BgL_arg2269z00_9872 =
						BGl_zc3z04anonymousza32271ze3ze70z60zzast_walkz00(BgL_arg2z00_8513,
						BgL_arg1z00_8512, BgL_arg0z00_8511, BgL_pz00_8510,
						(((BgL_sequencez00_bglt) COBJECT(((BgL_sequencez00_bglt)
										BgL_nz00_8509)))->BgL_nodesz00));
					{	/* Ast/walk.scm 295 */
						obj_t BgL_list2270z00_9873;

						BgL_list2270z00_9873 = MAKE_YOUNG_PAIR(BgL_arg2269z00_9872, BNIL);
						BgL_res4104z00_9874 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2270z00_9873);
					}
				}
				return BgL_res4104z00_9874;
			}
		}

	}



/* <@anonymous:2271>~0 */
	obj_t BGl_zc3z04anonymousza32271ze3ze70z60zzast_walkz00(obj_t
		BgL_arg2z00_8607, obj_t BgL_arg1z00_8606, obj_t BgL_arg0z00_8605,
		obj_t BgL_pz00_8604, obj_t BgL_l1483z00_2713)
	{
		{	/* Ast/walk.scm 295 */
			if (NULLP(BgL_l1483z00_2713))
				{	/* Ast/walk.scm 295 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 295 */
					obj_t BgL_arg2273z00_2716;
					obj_t BgL_arg2274z00_2717;

					{	/* Ast/walk.scm 295 */
						obj_t BgL_fz00_2718;

						BgL_fz00_2718 = CAR(((obj_t) BgL_l1483z00_2713));
						BgL_arg2273z00_2716 =
							BGL_PROCEDURE_CALL4(BgL_pz00_8604, BgL_fz00_2718,
							BgL_arg0z00_8605, BgL_arg1z00_8606, BgL_arg2z00_8607);
					}
					{	/* Ast/walk.scm 295 */
						obj_t BgL_arg2275z00_2719;

						BgL_arg2275z00_2719 = CDR(((obj_t) BgL_l1483z00_2713));
						BgL_arg2274z00_2717 =
							BGl_zc3z04anonymousza32271ze3ze70z60zzast_walkz00
							(BgL_arg2z00_8607, BgL_arg1z00_8606, BgL_arg0z00_8605,
							BgL_pz00_8604, BgL_arg2275z00_2719);
					}
					return bgl_append2(BgL_arg2273z00_2716, BgL_arg2274z00_2717);
				}
		}

	}



/* &walk2*-sequence1699 */
	obj_t BGl_z62walk2za2zd2sequence1699z12zzast_walkz00(obj_t BgL_envz00_8514,
		obj_t BgL_nz00_8515, obj_t BgL_pz00_8516, obj_t BgL_arg0z00_8517,
		obj_t BgL_arg1z00_8518)
	{
		{	/* Ast/walk.scm 295 */
			{	/* Ast/walk.scm 295 */
				obj_t BgL_res4105z00_9878;

				{	/* Ast/walk.scm 295 */
					obj_t BgL_arg2262z00_9876;

					BgL_arg2262z00_9876 =
						BGl_zc3z04anonymousza32264ze3ze70z60zzast_walkz00(BgL_arg1z00_8518,
						BgL_arg0z00_8517, BgL_pz00_8516,
						(((BgL_sequencez00_bglt) COBJECT(((BgL_sequencez00_bglt)
										BgL_nz00_8515)))->BgL_nodesz00));
					{	/* Ast/walk.scm 295 */
						obj_t BgL_list2263z00_9877;

						BgL_list2263z00_9877 = MAKE_YOUNG_PAIR(BgL_arg2262z00_9876, BNIL);
						BgL_res4105z00_9878 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2263z00_9877);
					}
				}
				return BgL_res4105z00_9878;
			}
		}

	}



/* <@anonymous:2264>~0 */
	obj_t BGl_zc3z04anonymousza32264ze3ze70z60zzast_walkz00(obj_t
		BgL_arg1z00_8603, obj_t BgL_arg0z00_8602, obj_t BgL_pz00_8601,
		obj_t BgL_l1480z00_2692)
	{
		{	/* Ast/walk.scm 295 */
			if (NULLP(BgL_l1480z00_2692))
				{	/* Ast/walk.scm 295 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 295 */
					obj_t BgL_arg2266z00_2695;
					obj_t BgL_arg2267z00_2696;

					{	/* Ast/walk.scm 295 */
						obj_t BgL_fz00_2697;

						BgL_fz00_2697 = CAR(((obj_t) BgL_l1480z00_2692));
						BgL_arg2266z00_2695 =
							BGL_PROCEDURE_CALL3(BgL_pz00_8601, BgL_fz00_2697,
							BgL_arg0z00_8602, BgL_arg1z00_8603);
					}
					{	/* Ast/walk.scm 295 */
						obj_t BgL_arg2268z00_2698;

						BgL_arg2268z00_2698 = CDR(((obj_t) BgL_l1480z00_2692));
						BgL_arg2267z00_2696 =
							BGl_zc3z04anonymousza32264ze3ze70z60zzast_walkz00
							(BgL_arg1z00_8603, BgL_arg0z00_8602, BgL_pz00_8601,
							BgL_arg2268z00_2698);
					}
					return bgl_append2(BgL_arg2266z00_2695, BgL_arg2267z00_2696);
				}
		}

	}



/* &walk1*-sequence1697 */
	obj_t BGl_z62walk1za2zd2sequence1697z12zzast_walkz00(obj_t BgL_envz00_8519,
		obj_t BgL_nz00_8520, obj_t BgL_pz00_8521, obj_t BgL_arg0z00_8522)
	{
		{	/* Ast/walk.scm 295 */
			{	/* Ast/walk.scm 295 */
				obj_t BgL_res4106z00_9882;

				{	/* Ast/walk.scm 295 */
					obj_t BgL_arg2255z00_9880;

					BgL_arg2255z00_9880 =
						BGl_zc3z04anonymousza32257ze3ze70z60zzast_walkz00(BgL_arg0z00_8522,
						BgL_pz00_8521,
						(((BgL_sequencez00_bglt) COBJECT(((BgL_sequencez00_bglt)
										BgL_nz00_8520)))->BgL_nodesz00));
					{	/* Ast/walk.scm 295 */
						obj_t BgL_list2256z00_9881;

						BgL_list2256z00_9881 = MAKE_YOUNG_PAIR(BgL_arg2255z00_9880, BNIL);
						BgL_res4106z00_9882 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2256z00_9881);
					}
				}
				return BgL_res4106z00_9882;
			}
		}

	}



/* <@anonymous:2257>~0 */
	obj_t BGl_zc3z04anonymousza32257ze3ze70z60zzast_walkz00(obj_t
		BgL_arg0z00_8600, obj_t BgL_pz00_8599, obj_t BgL_l1477z00_2672)
	{
		{	/* Ast/walk.scm 295 */
			if (NULLP(BgL_l1477z00_2672))
				{	/* Ast/walk.scm 295 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 295 */
					obj_t BgL_arg2259z00_2675;
					obj_t BgL_arg2260z00_2676;

					{	/* Ast/walk.scm 295 */
						obj_t BgL_fz00_2677;

						BgL_fz00_2677 = CAR(((obj_t) BgL_l1477z00_2672));
						BgL_arg2259z00_2675 =
							BGL_PROCEDURE_CALL2(BgL_pz00_8599, BgL_fz00_2677,
							BgL_arg0z00_8600);
					}
					{	/* Ast/walk.scm 295 */
						obj_t BgL_arg2261z00_2678;

						BgL_arg2261z00_2678 = CDR(((obj_t) BgL_l1477z00_2672));
						BgL_arg2260z00_2676 =
							BGl_zc3z04anonymousza32257ze3ze70z60zzast_walkz00
							(BgL_arg0z00_8600, BgL_pz00_8599, BgL_arg2261z00_2678);
					}
					return bgl_append2(BgL_arg2259z00_2675, BgL_arg2260z00_2676);
				}
		}

	}



/* &walk0*-sequence1695 */
	obj_t BGl_z62walk0za2zd2sequence1695z12zzast_walkz00(obj_t BgL_envz00_8523,
		obj_t BgL_nz00_8524, obj_t BgL_pz00_8525)
	{
		{	/* Ast/walk.scm 295 */
			{	/* Ast/walk.scm 295 */
				obj_t BgL_res4107z00_9886;

				{	/* Ast/walk.scm 295 */
					obj_t BgL_arg2248z00_9884;

					BgL_arg2248z00_9884 =
						BGl_zc3z04anonymousza32250ze3ze70z60zzast_walkz00(BgL_pz00_8525,
						(((BgL_sequencez00_bglt) COBJECT(
									((BgL_sequencez00_bglt) BgL_nz00_8524)))->BgL_nodesz00));
					{	/* Ast/walk.scm 295 */
						obj_t BgL_list2249z00_9885;

						BgL_list2249z00_9885 = MAKE_YOUNG_PAIR(BgL_arg2248z00_9884, BNIL);
						BgL_res4107z00_9886 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2249z00_9885);
					}
				}
				return BgL_res4107z00_9886;
			}
		}

	}



/* <@anonymous:2250>~0 */
	obj_t BGl_zc3z04anonymousza32250ze3ze70z60zzast_walkz00(obj_t BgL_pz00_8598,
		obj_t BgL_l1474z00_2653)
	{
		{	/* Ast/walk.scm 295 */
			if (NULLP(BgL_l1474z00_2653))
				{	/* Ast/walk.scm 295 */
					return BNIL;
				}
			else
				{	/* Ast/walk.scm 295 */
					obj_t BgL_arg2252z00_2656;
					obj_t BgL_arg2253z00_2657;

					{	/* Ast/walk.scm 295 */
						obj_t BgL_fz00_2658;

						BgL_fz00_2658 = CAR(((obj_t) BgL_l1474z00_2653));
						BgL_arg2252z00_2656 =
							BGL_PROCEDURE_CALL1(BgL_pz00_8598, BgL_fz00_2658);
					}
					{	/* Ast/walk.scm 295 */
						obj_t BgL_arg2254z00_2659;

						BgL_arg2254z00_2659 = CDR(((obj_t) BgL_l1474z00_2653));
						BgL_arg2253z00_2657 =
							BGl_zc3z04anonymousza32250ze3ze70z60zzast_walkz00(BgL_pz00_8598,
							BgL_arg2254z00_2659);
					}
					return bgl_append2(BgL_arg2252z00_2656, BgL_arg2253z00_2657);
				}
		}

	}



/* &walk3-sequence1693 */
	obj_t BGl_z62walk3zd2sequence1693zb0zzast_walkz00(obj_t BgL_envz00_8526,
		obj_t BgL_nz00_8527, obj_t BgL_pz00_8528, obj_t BgL_arg0z00_8529,
		obj_t BgL_arg1z00_8530, obj_t BgL_arg2z00_8531)
	{
		{	/* Ast/walk.scm 295 */
			{	/* Ast/walk.scm 295 */
				obj_t BgL_g1472z00_9888;

				BgL_g1472z00_9888 =
					(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nz00_8527)))->BgL_nodesz00);
				{
					obj_t BgL_l1470z00_9890;

					{	/* Ast/walk.scm 295 */
						bool_t BgL_tmpz00_16134;

						BgL_l1470z00_9890 = BgL_g1472z00_9888;
					BgL_zc3z04anonymousza32245ze3z87_9889:
						if (PAIRP(BgL_l1470z00_9890))
							{	/* Ast/walk.scm 295 */
								{	/* Ast/walk.scm 295 */
									obj_t BgL_fz00_9891;

									BgL_fz00_9891 = CAR(BgL_l1470z00_9890);
									BGL_PROCEDURE_CALL4(BgL_pz00_8528, BgL_fz00_9891,
										BgL_arg0z00_8529, BgL_arg1z00_8530, BgL_arg2z00_8531);
								}
								{
									obj_t BgL_l1470z00_16145;

									BgL_l1470z00_16145 = CDR(BgL_l1470z00_9890);
									BgL_l1470z00_9890 = BgL_l1470z00_16145;
									goto BgL_zc3z04anonymousza32245ze3z87_9889;
								}
							}
						else
							{	/* Ast/walk.scm 295 */
								BgL_tmpz00_16134 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_16134);
					}
				}
			}
		}

	}



/* &walk2-sequence1691 */
	obj_t BGl_z62walk2zd2sequence1691zb0zzast_walkz00(obj_t BgL_envz00_8532,
		obj_t BgL_nz00_8533, obj_t BgL_pz00_8534, obj_t BgL_arg0z00_8535,
		obj_t BgL_arg1z00_8536)
	{
		{	/* Ast/walk.scm 295 */
			{	/* Ast/walk.scm 295 */
				obj_t BgL_g1469z00_9893;

				BgL_g1469z00_9893 =
					(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nz00_8533)))->BgL_nodesz00);
				{
					obj_t BgL_l1467z00_9895;

					{	/* Ast/walk.scm 295 */
						bool_t BgL_tmpz00_16150;

						BgL_l1467z00_9895 = BgL_g1469z00_9893;
					BgL_zc3z04anonymousza32242ze3z87_9894:
						if (PAIRP(BgL_l1467z00_9895))
							{	/* Ast/walk.scm 295 */
								{	/* Ast/walk.scm 295 */
									obj_t BgL_fz00_9896;

									BgL_fz00_9896 = CAR(BgL_l1467z00_9895);
									BGL_PROCEDURE_CALL3(BgL_pz00_8534, BgL_fz00_9896,
										BgL_arg0z00_8535, BgL_arg1z00_8536);
								}
								{
									obj_t BgL_l1467z00_16160;

									BgL_l1467z00_16160 = CDR(BgL_l1467z00_9895);
									BgL_l1467z00_9895 = BgL_l1467z00_16160;
									goto BgL_zc3z04anonymousza32242ze3z87_9894;
								}
							}
						else
							{	/* Ast/walk.scm 295 */
								BgL_tmpz00_16150 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_16150);
					}
				}
			}
		}

	}



/* &walk1-sequence1689 */
	obj_t BGl_z62walk1zd2sequence1689zb0zzast_walkz00(obj_t BgL_envz00_8537,
		obj_t BgL_nz00_8538, obj_t BgL_pz00_8539, obj_t BgL_arg0z00_8540)
	{
		{	/* Ast/walk.scm 295 */
			{	/* Ast/walk.scm 295 */
				obj_t BgL_g1466z00_9898;

				BgL_g1466z00_9898 =
					(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nz00_8538)))->BgL_nodesz00);
				{
					obj_t BgL_l1464z00_9900;

					{	/* Ast/walk.scm 295 */
						bool_t BgL_tmpz00_16165;

						BgL_l1464z00_9900 = BgL_g1466z00_9898;
					BgL_zc3z04anonymousza32239ze3z87_9899:
						if (PAIRP(BgL_l1464z00_9900))
							{	/* Ast/walk.scm 295 */
								{	/* Ast/walk.scm 295 */
									obj_t BgL_fz00_9901;

									BgL_fz00_9901 = CAR(BgL_l1464z00_9900);
									BGL_PROCEDURE_CALL2(BgL_pz00_8539, BgL_fz00_9901,
										BgL_arg0z00_8540);
								}
								{
									obj_t BgL_l1464z00_16174;

									BgL_l1464z00_16174 = CDR(BgL_l1464z00_9900);
									BgL_l1464z00_9900 = BgL_l1464z00_16174;
									goto BgL_zc3z04anonymousza32239ze3z87_9899;
								}
							}
						else
							{	/* Ast/walk.scm 295 */
								BgL_tmpz00_16165 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_16165);
					}
				}
			}
		}

	}



/* &walk0-sequence1687 */
	obj_t BGl_z62walk0zd2sequence1687zb0zzast_walkz00(obj_t BgL_envz00_8541,
		obj_t BgL_nz00_8542, obj_t BgL_pz00_8543)
	{
		{	/* Ast/walk.scm 295 */
			{	/* Ast/walk.scm 295 */
				obj_t BgL_g1463z00_9903;

				BgL_g1463z00_9903 =
					(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nz00_8542)))->BgL_nodesz00);
				{
					obj_t BgL_l1461z00_9905;

					{	/* Ast/walk.scm 295 */
						bool_t BgL_tmpz00_16179;

						BgL_l1461z00_9905 = BgL_g1463z00_9903;
					BgL_zc3z04anonymousza32236ze3z87_9904:
						if (PAIRP(BgL_l1461z00_9905))
							{	/* Ast/walk.scm 295 */
								{	/* Ast/walk.scm 295 */
									obj_t BgL_fz00_9906;

									BgL_fz00_9906 = CAR(BgL_l1461z00_9905);
									BGL_PROCEDURE_CALL1(BgL_pz00_8543, BgL_fz00_9906);
								}
								{
									obj_t BgL_l1461z00_16187;

									BgL_l1461z00_16187 = CDR(BgL_l1461z00_9905);
									BgL_l1461z00_9905 = BgL_l1461z00_16187;
									goto BgL_zc3z04anonymousza32236ze3z87_9904;
								}
							}
						else
							{	/* Ast/walk.scm 295 */
								BgL_tmpz00_16179 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_16179);
					}
				}
			}
		}

	}



/* &walk3!-node1685 */
	BgL_nodez00_bglt BGl_z62walk3z12zd2node1685za2zzast_walkz00(obj_t
		BgL_envz00_8544, obj_t BgL_nz00_8545, obj_t BgL_pz00_8546,
		obj_t BgL_arg0z00_8547, obj_t BgL_arg1z00_8548, obj_t BgL_arg2z00_8549)
	{
		{	/* Ast/walk.scm 294 */
			return ((BgL_nodez00_bglt) BgL_nz00_8545);
		}

	}



/* &walk2!-node1683 */
	BgL_nodez00_bglt BGl_z62walk2z12zd2node1683za2zzast_walkz00(obj_t
		BgL_envz00_8550, obj_t BgL_nz00_8551, obj_t BgL_pz00_8552,
		obj_t BgL_arg0z00_8553, obj_t BgL_arg1z00_8554)
	{
		{	/* Ast/walk.scm 294 */
			return ((BgL_nodez00_bglt) BgL_nz00_8551);
		}

	}



/* &walk1!-node1681 */
	BgL_nodez00_bglt BGl_z62walk1z12zd2node1681za2zzast_walkz00(obj_t
		BgL_envz00_8555, obj_t BgL_nz00_8556, obj_t BgL_pz00_8557,
		obj_t BgL_arg0z00_8558)
	{
		{	/* Ast/walk.scm 294 */
			return ((BgL_nodez00_bglt) BgL_nz00_8556);
		}

	}



/* &walk0!-node1679 */
	BgL_nodez00_bglt BGl_z62walk0z12zd2node1679za2zzast_walkz00(obj_t
		BgL_envz00_8559, obj_t BgL_nz00_8560, obj_t BgL_pz00_8561)
	{
		{	/* Ast/walk.scm 294 */
			return ((BgL_nodez00_bglt) BgL_nz00_8560);
		}

	}



/* &walk3*-node1677 */
	obj_t BGl_z62walk3za2zd2node1677z12zzast_walkz00(obj_t BgL_envz00_8562,
		obj_t BgL_nz00_8563, obj_t BgL_pz00_8564, obj_t BgL_arg0z00_8565,
		obj_t BgL_arg1z00_8566, obj_t BgL_arg2z00_8567)
	{
		{	/* Ast/walk.scm 294 */
			return BNIL;
		}

	}



/* &walk2*-node1675 */
	obj_t BGl_z62walk2za2zd2node1675z12zzast_walkz00(obj_t BgL_envz00_8568,
		obj_t BgL_nz00_8569, obj_t BgL_pz00_8570, obj_t BgL_arg0z00_8571,
		obj_t BgL_arg1z00_8572)
	{
		{	/* Ast/walk.scm 294 */
			return BNIL;
		}

	}



/* &walk1*-node1673 */
	obj_t BGl_z62walk1za2zd2node1673z12zzast_walkz00(obj_t BgL_envz00_8573,
		obj_t BgL_nz00_8574, obj_t BgL_pz00_8575, obj_t BgL_arg0z00_8576)
	{
		{	/* Ast/walk.scm 294 */
			return BNIL;
		}

	}



/* &walk0*-node1671 */
	obj_t BGl_z62walk0za2zd2node1671z12zzast_walkz00(obj_t BgL_envz00_8577,
		obj_t BgL_nz00_8578, obj_t BgL_pz00_8579)
	{
		{	/* Ast/walk.scm 294 */
			return BNIL;
		}

	}



/* &walk3-node1669 */
	obj_t BGl_z62walk3zd2node1669zb0zzast_walkz00(obj_t BgL_envz00_8580,
		obj_t BgL_nz00_8581, obj_t BgL_pz00_8582, obj_t BgL_arg0z00_8583,
		obj_t BgL_arg1z00_8584, obj_t BgL_arg2z00_8585)
	{
		{	/* Ast/walk.scm 294 */
			return BFALSE;
		}

	}



/* &walk2-node1667 */
	obj_t BGl_z62walk2zd2node1667zb0zzast_walkz00(obj_t BgL_envz00_8586,
		obj_t BgL_nz00_8587, obj_t BgL_pz00_8588, obj_t BgL_arg0z00_8589,
		obj_t BgL_arg1z00_8590)
	{
		{	/* Ast/walk.scm 294 */
			return BFALSE;
		}

	}



/* &walk1-node1665 */
	obj_t BGl_z62walk1zd2node1665zb0zzast_walkz00(obj_t BgL_envz00_8591,
		obj_t BgL_nz00_8592, obj_t BgL_pz00_8593, obj_t BgL_arg0z00_8594)
	{
		{	/* Ast/walk.scm 294 */
			return BFALSE;
		}

	}



/* &walk0-node1663 */
	obj_t BGl_z62walk0zd2node1663zb0zzast_walkz00(obj_t BgL_envz00_8595,
		obj_t BgL_nz00_8596, obj_t BgL_pz00_8597)
	{
		{	/* Ast/walk.scm 294 */
			return BFALSE;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_walkz00(void)
	{
		{	/* Ast/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string4027z00zzast_walkz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string4027z00zzast_walkz00));
			return
				BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string4027z00zzast_walkz00));
		}

	}

#ifdef __cplusplus
}
#endif
