/*===========================================================================*/
/*   (Ast/check_init.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/check_init.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_AST_CHECKzd2GLOBALzd2INITz00_TYPE_DEFINITIONS
#define BGL_BgL_AST_CHECKzd2GLOBALzd2INITz00_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;


#endif													// BGL_BgL_AST_CHECKzd2GLOBALzd2INITz00_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t BGl_requirezd2initializa7ationz75zzast_checkzd2globalzd2initz00 =
		BUNSPEC;
	static obj_t
		BGl_z62checkzd2initzd2sequence1260z62zzast_checkzd2globalzd2initz00(obj_t,
		obj_t);
	extern obj_t BGl_funz00zzast_varz00;
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzast_checkzd2globalzd2initz00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static obj_t
		BGl_z62checkzd2initzd2cast1272z62zzast_checkzd2globalzd2initz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62checkzd2initzd2condition1276z62zzast_checkzd2globalzd2initz00(obj_t,
		obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzast_checkzd2globalzd2initz00(void);
	static obj_t BGl_objectzd2initzd2zzast_checkzd2globalzd2initz00(void);
	static obj_t
		BGl_z62checkzd2initzd2setq1274z62zzast_checkzd2globalzd2initz00(obj_t,
		obj_t);
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t
		BGl_z62checkzd2initzd2sync1262z62zzast_checkzd2globalzd2initz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62checkzd2initzd2fail1279z62zzast_checkzd2globalzd2initz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzast_checkzd2globalzd2initz00(void);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t
		BGl_z62checkzd2initzd2jumpzd2exzd2i1289z62zzast_checkzd2globalzd2initz00
		(obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t
		BGl_z62checkzd2initzd2letzd2fun1283zb0zzast_checkzd2globalzd2initz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	static obj_t
		BGl_z62checkzd2initzd2setzd2exzd2it1287z62zzast_checkzd2globalzd2initz00
		(obj_t, obj_t);
	static obj_t
		BGl_z62checkzd2initzd2var1258z62zzast_checkzd2globalzd2initz00(obj_t,
		obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	static obj_t BGl_z62checkzd2init1255zb0zzast_checkzd2globalzd2initz00(obj_t,
		obj_t);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzast_checkzd2globalzd2initz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_includez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t
		BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(BgL_nodez00_bglt);
	static obj_t
		BGl_z62checkzd2initzd2appzd2ly1266zb0zzast_checkzd2globalzd2initz00(obj_t,
		obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzast_checkzd2globalzd2initz00(void);
	static obj_t
		BGl_libraryzd2moduleszd2initz00zzast_checkzd2globalzd2initz00(void);
	static obj_t
		BGl_z62checkzd2initzd2app1264z62zzast_checkzd2globalzd2initz00(obj_t,
		obj_t);
	static obj_t BGl_z62checkzd2initzb0zzast_checkzd2globalzd2initz00(obj_t,
		obj_t);
	static obj_t
		BGl_importedzd2moduleszd2initz00zzast_checkzd2globalzd2initz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzast_checkzd2globalzd2initz00(void);
	static obj_t BGl_za2globalsza2z00zzast_checkzd2globalzd2initz00 = BUNSPEC;
	static obj_t
		BGl_z62checkzd2initzd2funcall1268z62zzast_checkzd2globalzd2initz00(obj_t,
		obj_t);
	static bool_t BGl_checkzd2initza2z70zzast_checkzd2globalzd2initz00(obj_t);
	static obj_t
		BGl_z62checkzd2initzd2extern1270z62zzast_checkzd2globalzd2initz00(obj_t,
		obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	static obj_t
		BGl_z62checkzd2initzd2boxzd2setz121293za2zzast_checkzd2globalzd2initz00
		(obj_t, obj_t);
	extern obj_t BGl_userzd2warningzf2locationz20zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62checkzd2initzd2switch1281z62zzast_checkzd2globalzd2initz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62checkzd2initzd2makezd2box1291zb0zzast_checkzd2globalzd2initz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62checkzd2initzd2letzd2var1285zb0zzast_checkzd2globalzd2initz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t BGl_conditionalz00zzast_nodez00;
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t __cnst[1];


	   
		 
		DEFINE_STRING(BGl_string1735z00zzast_checkzd2globalzd2initz00,
		BgL_bgl_string1735za700za7za7a1762za7, "check-init1255", 14);
	      DEFINE_STRING(BGl_string1737z00zzast_checkzd2globalzd2initz00,
		BgL_bgl_string1737za700za7za7a1763za7, "check-init", 10);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1734z00zzast_checkzd2globalzd2initz00,
		BgL_bgl_za762checkza7d2init11764z00,
		BGl_z62checkzd2init1255zb0zzast_checkzd2globalzd2initz00, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1736z00zzast_checkzd2globalzd2initz00,
		BgL_bgl_za762checkza7d2initza71765za7,
		BGl_z62checkzd2initzd2var1258z62zzast_checkzd2globalzd2initz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1738z00zzast_checkzd2globalzd2initz00,
		BgL_bgl_za762checkza7d2initza71766za7,
		BGl_z62checkzd2initzd2sequence1260z62zzast_checkzd2globalzd2initz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1739z00zzast_checkzd2globalzd2initz00,
		BgL_bgl_za762checkza7d2initza71767za7,
		BGl_z62checkzd2initzd2sync1262z62zzast_checkzd2globalzd2initz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1740z00zzast_checkzd2globalzd2initz00,
		BgL_bgl_za762checkza7d2initza71768za7,
		BGl_z62checkzd2initzd2app1264z62zzast_checkzd2globalzd2initz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1741z00zzast_checkzd2globalzd2initz00,
		BgL_bgl_za762checkza7d2initza71769za7,
		BGl_z62checkzd2initzd2appzd2ly1266zb0zzast_checkzd2globalzd2initz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1742z00zzast_checkzd2globalzd2initz00,
		BgL_bgl_za762checkza7d2initza71770za7,
		BGl_z62checkzd2initzd2funcall1268z62zzast_checkzd2globalzd2initz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1743z00zzast_checkzd2globalzd2initz00,
		BgL_bgl_za762checkza7d2initza71771za7,
		BGl_z62checkzd2initzd2extern1270z62zzast_checkzd2globalzd2initz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1744z00zzast_checkzd2globalzd2initz00,
		BgL_bgl_za762checkza7d2initza71772za7,
		BGl_z62checkzd2initzd2cast1272z62zzast_checkzd2globalzd2initz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1745z00zzast_checkzd2globalzd2initz00,
		BgL_bgl_za762checkza7d2initza71773za7,
		BGl_z62checkzd2initzd2setq1274z62zzast_checkzd2globalzd2initz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1746z00zzast_checkzd2globalzd2initz00,
		BgL_bgl_za762checkza7d2initza71774za7,
		BGl_z62checkzd2initzd2condition1276z62zzast_checkzd2globalzd2initz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1747z00zzast_checkzd2globalzd2initz00,
		BgL_bgl_za762checkza7d2initza71775za7,
		BGl_z62checkzd2initzd2fail1279z62zzast_checkzd2globalzd2initz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1748z00zzast_checkzd2globalzd2initz00,
		BgL_bgl_za762checkza7d2initza71776za7,
		BGl_z62checkzd2initzd2switch1281z62zzast_checkzd2globalzd2initz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1755z00zzast_checkzd2globalzd2initz00,
		BgL_bgl_string1755za700za7za7a1777za7, "Variable used before initialized",
		32);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1749z00zzast_checkzd2globalzd2initz00,
		BgL_bgl_za762checkza7d2initza71778za7,
		BGl_z62checkzd2initzd2letzd2fun1283zb0zzast_checkzd2globalzd2initz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1756z00zzast_checkzd2globalzd2initz00,
		BgL_bgl_string1756za700za7za7a1779za7,
		"Illegal type for prematurely used variable", 42);
	      DEFINE_STRING(BGl_string1757z00zzast_checkzd2globalzd2initz00,
		BgL_bgl_string1757za700za7za7a1780za7, "ast_check-global-init", 21);
	      DEFINE_STRING(BGl_string1758z00zzast_checkzd2globalzd2initz00,
		BgL_bgl_string1758za700za7za7a1781za7, "done ", 5);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1750z00zzast_checkzd2globalzd2initz00,
		BgL_bgl_za762checkza7d2initza71782za7,
		BGl_z62checkzd2initzd2letzd2var1285zb0zzast_checkzd2globalzd2initz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1751z00zzast_checkzd2globalzd2initz00,
		BgL_bgl_za762checkza7d2initza71783za7,
		BGl_z62checkzd2initzd2setzd2exzd2it1287z62zzast_checkzd2globalzd2initz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1752z00zzast_checkzd2globalzd2initz00,
		BgL_bgl_za762checkza7d2initza71784za7,
		BGl_z62checkzd2initzd2jumpzd2exzd2i1289z62zzast_checkzd2globalzd2initz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1753z00zzast_checkzd2globalzd2initz00,
		BgL_bgl_za762checkza7d2initza71785za7,
		BGl_z62checkzd2initzd2makezd2box1291zb0zzast_checkzd2globalzd2initz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1754z00zzast_checkzd2globalzd2initz00,
		BgL_bgl_za762checkza7d2initza71786za7,
		BGl_z62checkzd2initzd2boxzd2setz121293za2zzast_checkzd2globalzd2initz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_GENERIC
		(BGl_checkzd2initzd2envz00zzast_checkzd2globalzd2initz00,
		BgL_bgl_za762checkza7d2initza71787za7,
		BGl_z62checkzd2initzb0zzast_checkzd2globalzd2initz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzast_checkzd2globalzd2initz00));
		     ADD_ROOT((void
				*) (&BGl_za2globalsza2z00zzast_checkzd2globalzd2initz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzast_checkzd2globalzd2initz00(long
		BgL_checksumz00_2383, char *BgL_fromz00_2384)
	{
		{
			if (CBOOL
				(BGl_requirezd2initializa7ationz75zzast_checkzd2globalzd2initz00))
				{
					BGl_requirezd2initializa7ationz75zzast_checkzd2globalzd2initz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_checkzd2globalzd2initz00();
					BGl_libraryzd2moduleszd2initz00zzast_checkzd2globalzd2initz00();
					BGl_cnstzd2initzd2zzast_checkzd2globalzd2initz00();
					BGl_importedzd2moduleszd2initz00zzast_checkzd2globalzd2initz00();
					BGl_genericzd2initzd2zzast_checkzd2globalzd2initz00();
					BGl_methodzd2initzd2zzast_checkzd2globalzd2initz00();
					return BGl_toplevelzd2initzd2zzast_checkzd2globalzd2initz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_checkzd2globalzd2initz00(void)
	{
		{	/* Ast/check_init.scm 18 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L,
				"ast_check-global-init");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_check-global-init");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L,
				"ast_check-global-init");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"ast_check-global-init");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "ast_check-global-init");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"ast_check-global-init");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"ast_check-global-init");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"ast_check-global-init");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "ast_check-global-init");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"ast_check-global-init");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L,
				"ast_check-global-init");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_checkzd2globalzd2initz00(void)
	{
		{	/* Ast/check_init.scm 18 */
			{	/* Ast/check_init.scm 18 */
				obj_t BgL_cportz00_2290;

				{	/* Ast/check_init.scm 18 */
					obj_t BgL_stringz00_2297;

					BgL_stringz00_2297 = BGl_string1758z00zzast_checkzd2globalzd2initz00;
					{	/* Ast/check_init.scm 18 */
						obj_t BgL_startz00_2298;

						BgL_startz00_2298 = BINT(0L);
						{	/* Ast/check_init.scm 18 */
							obj_t BgL_endz00_2299;

							BgL_endz00_2299 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2297)));
							{	/* Ast/check_init.scm 18 */

								BgL_cportz00_2290 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2297, BgL_startz00_2298, BgL_endz00_2299);
				}}}}
				{
					long BgL_iz00_2291;

					BgL_iz00_2291 = 0L;
				BgL_loopz00_2292:
					if ((BgL_iz00_2291 == -1L))
						{	/* Ast/check_init.scm 18 */
							return BUNSPEC;
						}
					else
						{	/* Ast/check_init.scm 18 */
							{	/* Ast/check_init.scm 18 */
								obj_t BgL_arg1761z00_2293;

								{	/* Ast/check_init.scm 18 */

									{	/* Ast/check_init.scm 18 */
										obj_t BgL_locationz00_2295;

										BgL_locationz00_2295 = BBOOL(((bool_t) 0));
										{	/* Ast/check_init.scm 18 */

											BgL_arg1761z00_2293 =
												BGl_readz00zz__readerz00(BgL_cportz00_2290,
												BgL_locationz00_2295);
										}
									}
								}
								{	/* Ast/check_init.scm 18 */
									int BgL_tmpz00_2415;

									BgL_tmpz00_2415 = (int) (BgL_iz00_2291);
									CNST_TABLE_SET(BgL_tmpz00_2415, BgL_arg1761z00_2293);
							}}
							{	/* Ast/check_init.scm 18 */
								int BgL_auxz00_2296;

								BgL_auxz00_2296 = (int) ((BgL_iz00_2291 - 1L));
								{
									long BgL_iz00_2420;

									BgL_iz00_2420 = (long) (BgL_auxz00_2296);
									BgL_iz00_2291 = BgL_iz00_2420;
									goto BgL_loopz00_2292;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_checkzd2globalzd2initz00(void)
	{
		{	/* Ast/check_init.scm 18 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzast_checkzd2globalzd2initz00(void)
	{
		{	/* Ast/check_init.scm 18 */
			BGl_za2globalsza2z00zzast_checkzd2globalzd2initz00 = BNIL;
			return BUNSPEC;
		}

	}



/* check-init* */
	bool_t BGl_checkzd2initza2z70zzast_checkzd2globalzd2initz00(obj_t
		BgL_nodeza2za2_46)
	{
		{	/* Ast/check_init.scm 221 */
			{
				obj_t BgL_l1253z00_1645;

				BgL_l1253z00_1645 = BgL_nodeza2za2_46;
			BgL_zc3z04anonymousza31320ze3z87_1646:
				if (PAIRP(BgL_l1253z00_1645))
					{	/* Ast/check_init.scm 222 */
						{	/* Ast/check_init.scm 222 */
							obj_t BgL_arg1322z00_1648;

							BgL_arg1322z00_1648 = CAR(BgL_l1253z00_1645);
							BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(
								((BgL_nodez00_bglt) BgL_arg1322z00_1648));
						}
						{
							obj_t BgL_l1253z00_2428;

							BgL_l1253z00_2428 = CDR(BgL_l1253z00_1645);
							BgL_l1253z00_1645 = BgL_l1253z00_2428;
							goto BgL_zc3z04anonymousza31320ze3z87_1646;
						}
					}
				else
					{	/* Ast/check_init.scm 222 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_checkzd2globalzd2initz00(void)
	{
		{	/* Ast/check_init.scm 18 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_checkzd2globalzd2initz00(void)
	{
		{	/* Ast/check_init.scm 18 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_checkzd2initzd2envz00zzast_checkzd2globalzd2initz00,
				BGl_proc1734z00zzast_checkzd2globalzd2initz00, BGl_nodez00zzast_nodez00,
				BGl_string1735z00zzast_checkzd2globalzd2initz00);
		}

	}



/* &check-init1255 */
	obj_t BGl_z62checkzd2init1255zb0zzast_checkzd2globalzd2initz00(obj_t
		BgL_envz00_2231, obj_t BgL_nodez00_2232)
	{
		{	/* Ast/check_init.scm 57 */
			return CNST_TABLE_REF(0);
		}

	}



/* check-init */
	obj_t BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(BgL_nodez00_bglt
		BgL_nodez00_27)
	{
		{	/* Ast/check_init.scm 57 */
			{	/* Ast/check_init.scm 57 */
				obj_t BgL_method1256z00_1655;

				{	/* Ast/check_init.scm 57 */
					obj_t BgL_res1728z00_2020;

					{	/* Ast/check_init.scm 57 */
						long BgL_objzd2classzd2numz00_1991;

						BgL_objzd2classzd2numz00_1991 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_27));
						{	/* Ast/check_init.scm 57 */
							obj_t BgL_arg1811z00_1992;

							BgL_arg1811z00_1992 =
								PROCEDURE_REF
								(BGl_checkzd2initzd2envz00zzast_checkzd2globalzd2initz00,
								(int) (1L));
							{	/* Ast/check_init.scm 57 */
								int BgL_offsetz00_1995;

								BgL_offsetz00_1995 = (int) (BgL_objzd2classzd2numz00_1991);
								{	/* Ast/check_init.scm 57 */
									long BgL_offsetz00_1996;

									BgL_offsetz00_1996 =
										((long) (BgL_offsetz00_1995) - OBJECT_TYPE);
									{	/* Ast/check_init.scm 57 */
										long BgL_modz00_1997;

										BgL_modz00_1997 =
											(BgL_offsetz00_1996 >> (int) ((long) ((int) (4L))));
										{	/* Ast/check_init.scm 57 */
											long BgL_restz00_1999;

											BgL_restz00_1999 =
												(BgL_offsetz00_1996 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/check_init.scm 57 */

												{	/* Ast/check_init.scm 57 */
													obj_t BgL_bucketz00_2001;

													BgL_bucketz00_2001 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_1992), BgL_modz00_1997);
													BgL_res1728z00_2020 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2001), BgL_restz00_1999);
					}}}}}}}}
					BgL_method1256z00_1655 = BgL_res1728z00_2020;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1256z00_1655, ((obj_t) BgL_nodez00_27));
			}
		}

	}



/* &check-init */
	obj_t BGl_z62checkzd2initzb0zzast_checkzd2globalzd2initz00(obj_t
		BgL_envz00_2233, obj_t BgL_nodez00_2234)
	{
		{	/* Ast/check_init.scm 57 */
			return
				BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(
				((BgL_nodez00_bglt) BgL_nodez00_2234));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_checkzd2globalzd2initz00(void)
	{
		{	/* Ast/check_init.scm 18 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2initzd2envz00zzast_checkzd2globalzd2initz00,
				BGl_varz00zzast_nodez00, BGl_proc1736z00zzast_checkzd2globalzd2initz00,
				BGl_string1737z00zzast_checkzd2globalzd2initz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2initzd2envz00zzast_checkzd2globalzd2initz00,
				BGl_sequencez00zzast_nodez00,
				BGl_proc1738z00zzast_checkzd2globalzd2initz00,
				BGl_string1737z00zzast_checkzd2globalzd2initz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2initzd2envz00zzast_checkzd2globalzd2initz00,
				BGl_syncz00zzast_nodez00, BGl_proc1739z00zzast_checkzd2globalzd2initz00,
				BGl_string1737z00zzast_checkzd2globalzd2initz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2initzd2envz00zzast_checkzd2globalzd2initz00,
				BGl_appz00zzast_nodez00, BGl_proc1740z00zzast_checkzd2globalzd2initz00,
				BGl_string1737z00zzast_checkzd2globalzd2initz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2initzd2envz00zzast_checkzd2globalzd2initz00,
				BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc1741z00zzast_checkzd2globalzd2initz00,
				BGl_string1737z00zzast_checkzd2globalzd2initz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2initzd2envz00zzast_checkzd2globalzd2initz00,
				BGl_funcallz00zzast_nodez00,
				BGl_proc1742z00zzast_checkzd2globalzd2initz00,
				BGl_string1737z00zzast_checkzd2globalzd2initz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2initzd2envz00zzast_checkzd2globalzd2initz00,
				BGl_externz00zzast_nodez00,
				BGl_proc1743z00zzast_checkzd2globalzd2initz00,
				BGl_string1737z00zzast_checkzd2globalzd2initz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2initzd2envz00zzast_checkzd2globalzd2initz00,
				BGl_castz00zzast_nodez00, BGl_proc1744z00zzast_checkzd2globalzd2initz00,
				BGl_string1737z00zzast_checkzd2globalzd2initz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2initzd2envz00zzast_checkzd2globalzd2initz00,
				BGl_setqz00zzast_nodez00, BGl_proc1745z00zzast_checkzd2globalzd2initz00,
				BGl_string1737z00zzast_checkzd2globalzd2initz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2initzd2envz00zzast_checkzd2globalzd2initz00,
				BGl_conditionalz00zzast_nodez00,
				BGl_proc1746z00zzast_checkzd2globalzd2initz00,
				BGl_string1737z00zzast_checkzd2globalzd2initz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2initzd2envz00zzast_checkzd2globalzd2initz00,
				BGl_failz00zzast_nodez00, BGl_proc1747z00zzast_checkzd2globalzd2initz00,
				BGl_string1737z00zzast_checkzd2globalzd2initz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2initzd2envz00zzast_checkzd2globalzd2initz00,
				BGl_switchz00zzast_nodez00,
				BGl_proc1748z00zzast_checkzd2globalzd2initz00,
				BGl_string1737z00zzast_checkzd2globalzd2initz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2initzd2envz00zzast_checkzd2globalzd2initz00,
				BGl_letzd2funzd2zzast_nodez00,
				BGl_proc1749z00zzast_checkzd2globalzd2initz00,
				BGl_string1737z00zzast_checkzd2globalzd2initz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2initzd2envz00zzast_checkzd2globalzd2initz00,
				BGl_letzd2varzd2zzast_nodez00,
				BGl_proc1750z00zzast_checkzd2globalzd2initz00,
				BGl_string1737z00zzast_checkzd2globalzd2initz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2initzd2envz00zzast_checkzd2globalzd2initz00,
				BGl_setzd2exzd2itz00zzast_nodez00,
				BGl_proc1751z00zzast_checkzd2globalzd2initz00,
				BGl_string1737z00zzast_checkzd2globalzd2initz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2initzd2envz00zzast_checkzd2globalzd2initz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc1752z00zzast_checkzd2globalzd2initz00,
				BGl_string1737z00zzast_checkzd2globalzd2initz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2initzd2envz00zzast_checkzd2globalzd2initz00,
				BGl_makezd2boxzd2zzast_nodez00,
				BGl_proc1753z00zzast_checkzd2globalzd2initz00,
				BGl_string1737z00zzast_checkzd2globalzd2initz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_checkzd2initzd2envz00zzast_checkzd2globalzd2initz00,
				BGl_boxzd2setz12zc0zzast_nodez00,
				BGl_proc1754z00zzast_checkzd2globalzd2initz00,
				BGl_string1737z00zzast_checkzd2globalzd2initz00);
		}

	}



/* &check-init-box-set!1293 */
	obj_t
		BGl_z62checkzd2initzd2boxzd2setz121293za2zzast_checkzd2globalzd2initz00
		(obj_t BgL_envz00_2253, obj_t BgL_nodez00_2254)
	{
		{	/* Ast/check_init.scm 214 */
			return
				BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(
				(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2254)))->BgL_valuez00));
		}

	}



/* &check-init-make-box1291 */
	obj_t
		BGl_z62checkzd2initzd2makezd2box1291zb0zzast_checkzd2globalzd2initz00(obj_t
		BgL_envz00_2255, obj_t BgL_nodez00_2256)
	{
		{	/* Ast/check_init.scm 208 */
			return
				BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(
				(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_2256)))->BgL_valuez00));
		}

	}



/* &check-init-jump-ex-i1289 */
	obj_t
		BGl_z62checkzd2initzd2jumpzd2exzd2i1289z62zzast_checkzd2globalzd2initz00
		(obj_t BgL_envz00_2257, obj_t BgL_nodez00_2258)
	{
		{	/* Ast/check_init.scm 200 */
			BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2258)))->BgL_exitz00));
			return
				BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2258)))->BgL_valuez00));
		}

	}



/* &check-init-set-ex-it1287 */
	obj_t
		BGl_z62checkzd2initzd2setzd2exzd2it1287z62zzast_checkzd2globalzd2initz00
		(obj_t BgL_envz00_2259, obj_t BgL_nodez00_2260)
	{
		{	/* Ast/check_init.scm 193 */
			BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2260)))->BgL_bodyz00));
			return
				BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2260)))->BgL_onexitz00));
		}

	}



/* &check-init-let-var1285 */
	obj_t
		BGl_z62checkzd2initzd2letzd2var1285zb0zzast_checkzd2globalzd2initz00(obj_t
		BgL_envz00_2261, obj_t BgL_nodez00_2262)
	{
		{	/* Ast/check_init.scm 183 */
			{	/* Ast/check_init.scm 184 */
				bool_t BgL_tmpz00_2500;

				BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(
					(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2262)))->BgL_bodyz00));
				{	/* Ast/check_init.scm 186 */
					obj_t BgL_g1252z00_2307;

					BgL_g1252z00_2307 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2262)))->BgL_bindingsz00);
					{
						obj_t BgL_l1250z00_2309;

						BgL_l1250z00_2309 = BgL_g1252z00_2307;
					BgL_zc3z04anonymousza31490ze3z87_2308:
						if (PAIRP(BgL_l1250z00_2309))
							{	/* Ast/check_init.scm 186 */
								{	/* Ast/check_init.scm 187 */
									obj_t BgL_bindingz00_2310;

									BgL_bindingz00_2310 = CAR(BgL_l1250z00_2309);
									{	/* Ast/check_init.scm 187 */
										obj_t BgL_arg1502z00_2311;

										BgL_arg1502z00_2311 = CDR(((obj_t) BgL_bindingz00_2310));
										BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(
											((BgL_nodez00_bglt) BgL_arg1502z00_2311));
									}
								}
								{
									obj_t BgL_l1250z00_2513;

									BgL_l1250z00_2513 = CDR(BgL_l1250z00_2309);
									BgL_l1250z00_2309 = BgL_l1250z00_2513;
									goto BgL_zc3z04anonymousza31490ze3z87_2308;
								}
							}
						else
							{	/* Ast/check_init.scm 186 */
								BgL_tmpz00_2500 = ((bool_t) 1);
							}
					}
				}
				return BBOOL(BgL_tmpz00_2500);
			}
		}

	}



/* &check-init-let-fun1283 */
	obj_t
		BGl_z62checkzd2initzd2letzd2fun1283zb0zzast_checkzd2globalzd2initz00(obj_t
		BgL_envz00_2263, obj_t BgL_nodez00_2264)
	{
		{	/* Ast/check_init.scm 176 */
			return
				BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(
				(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2264)))->BgL_bodyz00));
		}

	}



/* &check-init-switch1281 */
	obj_t BGl_z62checkzd2initzd2switch1281z62zzast_checkzd2globalzd2initz00(obj_t
		BgL_envz00_2265, obj_t BgL_nodez00_2266)
	{
		{	/* Ast/check_init.scm 163 */
			{	/* Ast/check_init.scm 164 */
				bool_t BgL_tmpz00_2519;

				BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(
					(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2266)))->BgL_testz00));
				{	/* Ast/check_init.scm 166 */
					obj_t BgL_g1249z00_2314;

					BgL_g1249z00_2314 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2266)))->BgL_clausesz00);
					{
						obj_t BgL_l1247z00_2316;

						BgL_l1247z00_2316 = BgL_g1249z00_2314;
					BgL_zc3z04anonymousza31455ze3z87_2315:
						if (PAIRP(BgL_l1247z00_2316))
							{	/* Ast/check_init.scm 166 */
								{	/* Ast/check_init.scm 167 */
									obj_t BgL_clausez00_2317;

									BgL_clausez00_2317 = CAR(BgL_l1247z00_2316);
									{	/* Ast/check_init.scm 167 */
										obj_t BgL_arg1472z00_2318;

										BgL_arg1472z00_2318 = CDR(((obj_t) BgL_clausez00_2317));
										BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(
											((BgL_nodez00_bglt) BgL_arg1472z00_2318));
									}
								}
								{
									obj_t BgL_l1247z00_2532;

									BgL_l1247z00_2532 = CDR(BgL_l1247z00_2316);
									BgL_l1247z00_2316 = BgL_l1247z00_2532;
									goto BgL_zc3z04anonymousza31455ze3z87_2315;
								}
							}
						else
							{	/* Ast/check_init.scm 166 */
								BgL_tmpz00_2519 = ((bool_t) 1);
							}
					}
				}
				return BBOOL(BgL_tmpz00_2519);
			}
		}

	}



/* &check-init-fail1279 */
	obj_t BGl_z62checkzd2initzd2fail1279z62zzast_checkzd2globalzd2initz00(obj_t
		BgL_envz00_2267, obj_t BgL_nodez00_2268)
	{
		{	/* Ast/check_init.scm 154 */
			BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2268)))->BgL_procz00));
			BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2268)))->BgL_msgz00));
			return
				BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2268)))->BgL_objz00));
		}

	}



/* &check-init-condition1276 */
	obj_t
		BGl_z62checkzd2initzd2condition1276z62zzast_checkzd2globalzd2initz00(obj_t
		BgL_envz00_2269, obj_t BgL_nodez00_2270)
	{
		{	/* Ast/check_init.scm 145 */
			BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2270)))->BgL_testz00));
			BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2270)))->BgL_truez00));
			return
				BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2270)))->BgL_falsez00));
		}

	}



/* &check-init-setq1274 */
	obj_t BGl_z62checkzd2initzd2setq1274z62zzast_checkzd2globalzd2initz00(obj_t
		BgL_envz00_2271, obj_t BgL_nodez00_2272)
	{
		{	/* Ast/check_init.scm 133 */
			{	/* Ast/check_init.scm 137 */
				BgL_variablez00_bglt BgL_varz00_2322;

				BgL_varz00_2322 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_setqz00_bglt) COBJECT(
										((BgL_setqz00_bglt) BgL_nodez00_2272)))->BgL_varz00)))->
					BgL_variablez00);
				{	/* Ast/check_init.scm 138 */
					bool_t BgL_test1793z00_2556;

					{	/* Ast/check_init.scm 138 */
						obj_t BgL_classz00_2323;

						BgL_classz00_2323 = BGl_globalz00zzast_varz00;
						{	/* Ast/check_init.scm 138 */
							BgL_objectz00_bglt BgL_arg1807z00_2324;

							{	/* Ast/check_init.scm 138 */
								obj_t BgL_tmpz00_2557;

								BgL_tmpz00_2557 =
									((obj_t) ((BgL_objectz00_bglt) BgL_varz00_2322));
								BgL_arg1807z00_2324 = (BgL_objectz00_bglt) (BgL_tmpz00_2557);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Ast/check_init.scm 138 */
									long BgL_idxz00_2325;

									BgL_idxz00_2325 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2324);
									BgL_test1793z00_2556 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2325 + 2L)) == BgL_classz00_2323);
								}
							else
								{	/* Ast/check_init.scm 138 */
									bool_t BgL_res1731z00_2328;

									{	/* Ast/check_init.scm 138 */
										obj_t BgL_oclassz00_2329;

										{	/* Ast/check_init.scm 138 */
											obj_t BgL_arg1815z00_2330;
											long BgL_arg1816z00_2331;

											BgL_arg1815z00_2330 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Ast/check_init.scm 138 */
												long BgL_arg1817z00_2332;

												BgL_arg1817z00_2332 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2324);
												BgL_arg1816z00_2331 =
													(BgL_arg1817z00_2332 - OBJECT_TYPE);
											}
											BgL_oclassz00_2329 =
												VECTOR_REF(BgL_arg1815z00_2330, BgL_arg1816z00_2331);
										}
										{	/* Ast/check_init.scm 138 */
											bool_t BgL__ortest_1115z00_2333;

											BgL__ortest_1115z00_2333 =
												(BgL_classz00_2323 == BgL_oclassz00_2329);
											if (BgL__ortest_1115z00_2333)
												{	/* Ast/check_init.scm 138 */
													BgL_res1731z00_2328 = BgL__ortest_1115z00_2333;
												}
											else
												{	/* Ast/check_init.scm 138 */
													long BgL_odepthz00_2334;

													{	/* Ast/check_init.scm 138 */
														obj_t BgL_arg1804z00_2335;

														BgL_arg1804z00_2335 = (BgL_oclassz00_2329);
														BgL_odepthz00_2334 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2335);
													}
													if ((2L < BgL_odepthz00_2334))
														{	/* Ast/check_init.scm 138 */
															obj_t BgL_arg1802z00_2336;

															{	/* Ast/check_init.scm 138 */
																obj_t BgL_arg1803z00_2337;

																BgL_arg1803z00_2337 = (BgL_oclassz00_2329);
																BgL_arg1802z00_2336 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2337,
																	2L);
															}
															BgL_res1731z00_2328 =
																(BgL_arg1802z00_2336 == BgL_classz00_2323);
														}
													else
														{	/* Ast/check_init.scm 138 */
															BgL_res1731z00_2328 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1793z00_2556 = BgL_res1731z00_2328;
								}
						}
					}
					if (BgL_test1793z00_2556)
						{	/* Ast/check_init.scm 138 */
							BGl_za2globalsza2z00zzast_checkzd2globalzd2initz00 =
								MAKE_YOUNG_PAIR(
								((obj_t) BgL_varz00_2322),
								BGl_za2globalsza2z00zzast_checkzd2globalzd2initz00);
						}
					else
						{	/* Ast/check_init.scm 138 */
							BFALSE;
						}
				}
			}
			return
				BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(
				(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_2272)))->BgL_valuez00));
		}

	}



/* &check-init-cast1272 */
	obj_t BGl_z62checkzd2initzd2cast1272z62zzast_checkzd2globalzd2initz00(obj_t
		BgL_envz00_2273, obj_t BgL_nodez00_2274)
	{
		{	/* Ast/check_init.scm 127 */
			return
				BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(
				(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_2274)))->BgL_argz00));
		}

	}



/* &check-init-extern1270 */
	obj_t BGl_z62checkzd2initzd2extern1270z62zzast_checkzd2globalzd2initz00(obj_t
		BgL_envz00_2275, obj_t BgL_nodez00_2276)
	{
		{	/* Ast/check_init.scm 121 */
			return
				BBOOL(BGl_checkzd2initza2z70zzast_checkzd2globalzd2initz00(
					(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_nodez00_2276)))->BgL_exprza2za2)));
		}

	}



/* &check-init-funcall1268 */
	obj_t BGl_z62checkzd2initzd2funcall1268z62zzast_checkzd2globalzd2initz00(obj_t
		BgL_envz00_2277, obj_t BgL_nodez00_2278)
	{
		{	/* Ast/check_init.scm 113 */
			{	/* Ast/check_init.scm 114 */
				bool_t BgL_tmpz00_2592;

				BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2278)))->BgL_funz00));
				BgL_tmpz00_2592 =
					BGl_checkzd2initza2z70zzast_checkzd2globalzd2initz00(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2278)))->BgL_argsz00));
				return BBOOL(BgL_tmpz00_2592);
			}
		}

	}



/* &check-init-app-ly1266 */
	obj_t
		BGl_z62checkzd2initzd2appzd2ly1266zb0zzast_checkzd2globalzd2initz00(obj_t
		BgL_envz00_2279, obj_t BgL_nodez00_2280)
	{
		{	/* Ast/check_init.scm 105 */
			BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(
				(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_2280)))->BgL_funz00));
			return
				BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(
				(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_2280)))->BgL_argz00));
		}

	}



/* &check-init-app1264 */
	obj_t BGl_z62checkzd2initzd2app1264z62zzast_checkzd2globalzd2initz00(obj_t
		BgL_envz00_2281, obj_t BgL_nodez00_2282)
	{
		{	/* Ast/check_init.scm 98 */
			return
				BBOOL(BGl_checkzd2initza2z70zzast_checkzd2globalzd2initz00(
					(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_2282)))->BgL_argsz00)));
		}

	}



/* &check-init-sync1262 */
	obj_t BGl_z62checkzd2initzd2sync1262z62zzast_checkzd2globalzd2initz00(obj_t
		BgL_envz00_2283, obj_t BgL_nodez00_2284)
	{
		{	/* Ast/check_init.scm 90 */
			BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2284)))->BgL_mutexz00));
			BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2284)))->BgL_prelockz00));
			return
				BGl_checkzd2initzd2zzast_checkzd2globalzd2initz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2284)))->BgL_bodyz00));
		}

	}



/* &check-init-sequence1260 */
	obj_t
		BGl_z62checkzd2initzd2sequence1260z62zzast_checkzd2globalzd2initz00(obj_t
		BgL_envz00_2285, obj_t BgL_nodez00_2286)
	{
		{	/* Ast/check_init.scm 84 */
			return
				BBOOL(BGl_checkzd2initza2z70zzast_checkzd2globalzd2initz00(
					(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_2286)))->BgL_nodesz00)));
		}

	}



/* &check-init-var1258 */
	obj_t BGl_z62checkzd2initzd2var1258z62zzast_checkzd2globalzd2initz00(obj_t
		BgL_envz00_2287, obj_t BgL_nodez00_2288)
	{
		{	/* Ast/check_init.scm 63 */
			{	/* Ast/check_init.scm 64 */
				BgL_variablez00_bglt BgL_varz00_2346;

				BgL_varz00_2346 =
					(((BgL_varz00_bglt) COBJECT(
							((BgL_varz00_bglt) BgL_nodez00_2288)))->BgL_variablez00);
				{	/* Ast/check_init.scm 65 */
					bool_t BgL_test1797z00_2625;

					{	/* Ast/check_init.scm 65 */
						bool_t BgL_test1800z00_2626;

						{	/* Ast/check_init.scm 65 */
							obj_t BgL_classz00_2347;

							BgL_classz00_2347 = BGl_globalz00zzast_varz00;
							{	/* Ast/check_init.scm 65 */
								BgL_objectz00_bglt BgL_arg1807z00_2348;

								{	/* Ast/check_init.scm 65 */
									obj_t BgL_tmpz00_2627;

									BgL_tmpz00_2627 =
										((obj_t) ((BgL_objectz00_bglt) BgL_varz00_2346));
									BgL_arg1807z00_2348 = (BgL_objectz00_bglt) (BgL_tmpz00_2627);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Ast/check_init.scm 65 */
										long BgL_idxz00_2349;

										BgL_idxz00_2349 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2348);
										BgL_test1800z00_2626 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2349 + 2L)) == BgL_classz00_2347);
									}
								else
									{	/* Ast/check_init.scm 65 */
										bool_t BgL_res1729z00_2352;

										{	/* Ast/check_init.scm 65 */
											obj_t BgL_oclassz00_2353;

											{	/* Ast/check_init.scm 65 */
												obj_t BgL_arg1815z00_2354;
												long BgL_arg1816z00_2355;

												BgL_arg1815z00_2354 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Ast/check_init.scm 65 */
													long BgL_arg1817z00_2356;

													BgL_arg1817z00_2356 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2348);
													BgL_arg1816z00_2355 =
														(BgL_arg1817z00_2356 - OBJECT_TYPE);
												}
												BgL_oclassz00_2353 =
													VECTOR_REF(BgL_arg1815z00_2354, BgL_arg1816z00_2355);
											}
											{	/* Ast/check_init.scm 65 */
												bool_t BgL__ortest_1115z00_2357;

												BgL__ortest_1115z00_2357 =
													(BgL_classz00_2347 == BgL_oclassz00_2353);
												if (BgL__ortest_1115z00_2357)
													{	/* Ast/check_init.scm 65 */
														BgL_res1729z00_2352 = BgL__ortest_1115z00_2357;
													}
												else
													{	/* Ast/check_init.scm 65 */
														long BgL_odepthz00_2358;

														{	/* Ast/check_init.scm 65 */
															obj_t BgL_arg1804z00_2359;

															BgL_arg1804z00_2359 = (BgL_oclassz00_2353);
															BgL_odepthz00_2358 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2359);
														}
														if ((2L < BgL_odepthz00_2358))
															{	/* Ast/check_init.scm 65 */
																obj_t BgL_arg1802z00_2360;

																{	/* Ast/check_init.scm 65 */
																	obj_t BgL_arg1803z00_2361;

																	BgL_arg1803z00_2361 = (BgL_oclassz00_2353);
																	BgL_arg1802z00_2360 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2361,
																		2L);
																}
																BgL_res1729z00_2352 =
																	(BgL_arg1802z00_2360 == BgL_classz00_2347);
															}
														else
															{	/* Ast/check_init.scm 65 */
																BgL_res1729z00_2352 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1800z00_2626 = BgL_res1729z00_2352;
									}
							}
						}
						if (BgL_test1800z00_2626)
							{	/* Ast/check_init.scm 66 */
								bool_t BgL_test1804z00_2650;

								{	/* Ast/check_init.scm 66 */
									BgL_valuez00_bglt BgL_arg1351z00_2362;

									BgL_arg1351z00_2362 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_varz00_2346))))->
										BgL_valuez00);
									{	/* Ast/check_init.scm 66 */
										obj_t BgL_classz00_2363;

										BgL_classz00_2363 = BGl_funz00zzast_varz00;
										{	/* Ast/check_init.scm 66 */
											BgL_objectz00_bglt BgL_arg1807z00_2364;

											{	/* Ast/check_init.scm 66 */
												obj_t BgL_tmpz00_2654;

												BgL_tmpz00_2654 =
													((obj_t) ((BgL_objectz00_bglt) BgL_arg1351z00_2362));
												BgL_arg1807z00_2364 =
													(BgL_objectz00_bglt) (BgL_tmpz00_2654);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Ast/check_init.scm 66 */
													long BgL_idxz00_2365;

													BgL_idxz00_2365 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2364);
													BgL_test1804z00_2650 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_2365 + 2L)) == BgL_classz00_2363);
												}
											else
												{	/* Ast/check_init.scm 66 */
													bool_t BgL_res1730z00_2368;

													{	/* Ast/check_init.scm 66 */
														obj_t BgL_oclassz00_2369;

														{	/* Ast/check_init.scm 66 */
															obj_t BgL_arg1815z00_2370;
															long BgL_arg1816z00_2371;

															BgL_arg1815z00_2370 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Ast/check_init.scm 66 */
																long BgL_arg1817z00_2372;

																BgL_arg1817z00_2372 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2364);
																BgL_arg1816z00_2371 =
																	(BgL_arg1817z00_2372 - OBJECT_TYPE);
															}
															BgL_oclassz00_2369 =
																VECTOR_REF(BgL_arg1815z00_2370,
																BgL_arg1816z00_2371);
														}
														{	/* Ast/check_init.scm 66 */
															bool_t BgL__ortest_1115z00_2373;

															BgL__ortest_1115z00_2373 =
																(BgL_classz00_2363 == BgL_oclassz00_2369);
															if (BgL__ortest_1115z00_2373)
																{	/* Ast/check_init.scm 66 */
																	BgL_res1730z00_2368 =
																		BgL__ortest_1115z00_2373;
																}
															else
																{	/* Ast/check_init.scm 66 */
																	long BgL_odepthz00_2374;

																	{	/* Ast/check_init.scm 66 */
																		obj_t BgL_arg1804z00_2375;

																		BgL_arg1804z00_2375 = (BgL_oclassz00_2369);
																		BgL_odepthz00_2374 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_2375);
																	}
																	if ((2L < BgL_odepthz00_2374))
																		{	/* Ast/check_init.scm 66 */
																			obj_t BgL_arg1802z00_2376;

																			{	/* Ast/check_init.scm 66 */
																				obj_t BgL_arg1803z00_2377;

																				BgL_arg1803z00_2377 =
																					(BgL_oclassz00_2369);
																				BgL_arg1802z00_2376 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_2377, 2L);
																			}
																			BgL_res1730z00_2368 =
																				(BgL_arg1802z00_2376 ==
																				BgL_classz00_2363);
																		}
																	else
																		{	/* Ast/check_init.scm 66 */
																			BgL_res1730z00_2368 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test1804z00_2650 = BgL_res1730z00_2368;
												}
										}
									}
								}
								if (BgL_test1804z00_2650)
									{	/* Ast/check_init.scm 66 */
										BgL_test1797z00_2625 = ((bool_t) 0);
									}
								else
									{	/* Ast/check_init.scm 66 */
										if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
													((obj_t) BgL_varz00_2346),
													BGl_za2globalsza2z00zzast_checkzd2globalzd2initz00)))
											{	/* Ast/check_init.scm 67 */
												BgL_test1797z00_2625 = ((bool_t) 0);
											}
										else
											{	/* Ast/check_init.scm 68 */
												obj_t BgL_arg1349z00_2378;

												BgL_arg1349z00_2378 =
													(((BgL_globalz00_bglt) COBJECT(
															((BgL_globalz00_bglt) BgL_varz00_2346)))->
													BgL_modulez00);
												BgL_test1797z00_2625 =
													(BgL_arg1349z00_2378 ==
													BGl_za2moduleza2z00zzmodule_modulez00);
											}
									}
							}
						else
							{	/* Ast/check_init.scm 65 */
								BgL_test1797z00_2625 = ((bool_t) 0);
							}
					}
					if (BgL_test1797z00_2625)
						{	/* Ast/check_init.scm 65 */
							{	/* Ast/check_init.scm 70 */
								obj_t BgL_arg1335z00_2379;
								obj_t BgL_arg1339z00_2380;

								BgL_arg1335z00_2379 =
									(((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt)
												((BgL_varz00_bglt) BgL_nodez00_2288))))->BgL_locz00);
								BgL_arg1339z00_2380 =
									(((BgL_variablez00_bglt) COBJECT(BgL_varz00_2346))->
									BgL_idz00);
								BGl_userzd2warningzf2locationz20zztools_errorz00
									(BgL_arg1335z00_2379,
									BGl_string1737z00zzast_checkzd2globalzd2initz00,
									BGl_string1755z00zzast_checkzd2globalzd2initz00,
									BgL_arg1339z00_2380);
							}
							{	/* Ast/check_init.scm 74 */
								bool_t BgL_test1812z00_2689;

								{	/* Ast/check_init.scm 74 */
									BgL_typez00_bglt BgL_arg1348z00_2381;

									BgL_arg1348z00_2381 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_varz00_2346))))->
										BgL_typez00);
									BgL_test1812z00_2689 =
										(((obj_t) BgL_arg1348z00_2381) ==
										BGl_za2_za2z00zztype_cachez00);
								}
								if (BgL_test1812z00_2689)
									{	/* Ast/check_init.scm 75 */
										BgL_typez00_bglt BgL_vz00_2382;

										BgL_vz00_2382 =
											((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00);
										((((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_globalz00_bglt) BgL_varz00_2346))))->
												BgL_typez00) =
											((BgL_typez00_bglt) BgL_vz00_2382), BUNSPEC);
									}
								else
									{	/* Ast/check_init.scm 74 */
										BGl_errorz00zz__errorz00(
											(((BgL_typez00_bglt) COBJECT(
														(((BgL_variablez00_bglt) COBJECT(BgL_varz00_2346))->
															BgL_typez00)))->BgL_idz00),
											BGl_string1756z00zzast_checkzd2globalzd2initz00,
											(((BgL_variablez00_bglt) COBJECT(BgL_varz00_2346))->
												BgL_idz00));
									}
							}
							return (BGl_za2globalsza2z00zzast_checkzd2globalzd2initz00 =
								MAKE_YOUNG_PAIR(
									((obj_t) BgL_varz00_2346),
									BGl_za2globalsza2z00zzast_checkzd2globalzd2initz00), BUNSPEC);
						}
					else
						{	/* Ast/check_init.scm 65 */
							return BFALSE;
						}
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_checkzd2globalzd2initz00(void)
	{
		{	/* Ast/check_init.scm 18 */
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1757z00zzast_checkzd2globalzd2initz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1757z00zzast_checkzd2globalzd2initz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1757z00zzast_checkzd2globalzd2initz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1757z00zzast_checkzd2globalzd2initz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1757z00zzast_checkzd2globalzd2initz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1757z00zzast_checkzd2globalzd2initz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1757z00zzast_checkzd2globalzd2initz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1757z00zzast_checkzd2globalzd2initz00));
			BGl_modulezd2initializa7ationz75zzmodule_includez00(184800657L,
				BSTRING_TO_STRING(BGl_string1757z00zzast_checkzd2globalzd2initz00));
			return
				BGl_modulezd2initializa7ationz75zzmodule_classz00(153808441L,
				BSTRING_TO_STRING(BGl_string1757z00zzast_checkzd2globalzd2initz00));
		}

	}

#ifdef __cplusplus
}
#endif
