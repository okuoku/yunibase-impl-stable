/*===========================================================================*/
/*   (Ast/let.scm)                                                           */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/let.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_AST_LET_TYPE_DEFINITIONS
#define BGL_AST_LET_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_tclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		struct BgL_globalz00_bgl *BgL_holderz00;
		obj_t BgL_wideningz00;
		long BgL_depthz00;
		bool_t BgL_finalzf3zf3;
		obj_t BgL_constructorz00;
		obj_t BgL_virtualzd2slotszd2numberz00;
		bool_t BgL_abstractzf3zf3;
		obj_t BgL_widezd2typezd2;
		obj_t BgL_subclassesz00;
	}                *BgL_tclassz00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;


#endif													// BGL_AST_LET_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_normaliza7ezd2prognz75zztools_prognz00(obj_t);
	static obj_t BGl_z62letzd2symzf3z43zzast_letz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	extern obj_t BGl_za2optimza2z00zzengine_paramz00;
	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t BGl_requirezd2initializa7ationz75zzast_letz00 = BUNSPEC;
	extern obj_t BGl_closurez00zzast_nodez00;
	static obj_t BGl_z62letcollapsez62zzast_letz00(obj_t, obj_t);
	extern obj_t BGl_findzd2locationzf2locz20zztools_locationz00(obj_t, obj_t);
	static BgL_letzd2varzd2_bglt BGl_makezd2genericzd2letz00zzast_letz00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_tclassz00zzobject_classz00;
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_zc3z04anonymousza33080ze3ze70z60zzast_letz00(obj_t);
	extern obj_t BGl_sfunz00zzast_varz00;
	extern obj_t BGl_fastzd2idzd2ofzd2idzd2zzast_identz00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzast_letz00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static bool_t BGl_directzd2functionzf3z21zzast_letz00(obj_t, bool_t);
	static bool_t BGl_safezd2letzd2optimzf3ze70z14zzast_letz00(obj_t,
		BgL_letzd2varzd2_bglt);
	static obj_t BGl_zc3z04anonymousza33072ze3ze70z60zzast_letz00(obj_t);
	BGL_IMPORT obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	BGL_IMPORT obj_t bgl_reverse(obj_t);
	static obj_t BGl_genericzd2initzd2zzast_letz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_z62letstarz62zzast_letz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_filterzd2mapzd2zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32872ze3ze5zzast_letz00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzast_letz00(void);
	static BgL_nodez00_bglt BGl_z62stage1z62zzast_letz00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62stage2z62zzast_letz00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62stage3z62zzast_letz00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62stage4z62zzast_letz00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62stage5z62zzast_letz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62stage7z62zzast_letz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	static obj_t BGl_z62letzd2symzb0zzast_letz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32890ze3ze5zzast_letz00(obj_t, obj_t);
	static bool_t BGl_constantzf3zf3zzast_letz00(obj_t);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	static bool_t BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	BGL_EXPORTED_DECL obj_t BGl_letzd2symzf3z21zzast_letz00(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzast_letz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzast_letz00(void);
	extern obj_t BGl_externz00zzast_nodez00;
	static BgL_nodez00_bglt BGl_z62letzd2ze3nodez53zzast_letz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_epairifyzd2propagatezd2locz00zztools_miscz00(obj_t, obj_t);
	static BgL_letzd2varzd2_bglt BGl_letzd2orzd2letrecz00zzast_letz00(obj_t,
		BgL_letzd2varzd2_bglt, obj_t);
	static obj_t BGl__functionzf3zf3zzast_letz00(obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t BGl_z62split3801z62zzast_letz00(obj_t, obj_t);
	static obj_t BGl_z62split3802z62zzast_letz00(obj_t, obj_t);
	static obj_t BGl_z62split3803z62zzast_letz00(obj_t, obj_t);
	static obj_t BGl_z62split3804z62zzast_letz00(obj_t, obj_t);
	static obj_t BGl_z62split3805z62zzast_letz00(obj_t, obj_t);
	static obj_t BGl_functionzf3zf3zzast_letz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	BGL_EXPORTED_DECL BgL_nodez00_bglt BGl_letzd2ze3nodez31zzast_letz00(obj_t,
		obj_t, obj_t, obj_t);
	extern BgL_typez00_bglt
		BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_za2procedureza2z00zztype_cachez00;
	static BgL_letzd2funzd2_bglt BGl_letzd2ze3labelsz31zzast_letz00(obj_t,
		BgL_nodez00_bglt, obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	BGL_IMPORT obj_t bgl_append2(obj_t, obj_t);
	static obj_t BGl_z62mutablezd2inzf3z43zzast_letz00(obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62letrecza2zd2ze3nodezf1zzast_letz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static obj_t BGl_z62usedzd2inzf3z43zzast_letz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzast_letz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_removez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_occurz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_substitutez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	static obj_t BGl_z62splitz62zzast_letz00(obj_t, obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	extern BgL_nodez00_bglt BGl_substitutez12z12zzast_substitutez00(obj_t, obj_t,
		BgL_nodez00_bglt, obj_t);
	extern BgL_typez00_bglt BGl_typezd2ofzd2idz00zzast_identz00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
	static bool_t BGl_safezd2reczd2valzf3ze70z14zzast_letz00(obj_t);
	extern obj_t BGl_kwotez00zzast_nodez00;
	static obj_t BGl_letrecursiveze70ze7zzast_letz00(obj_t, obj_t);
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzast_letz00(void);
	BGL_IMPORT bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_letz00(void);
	extern BgL_localz00_bglt BGl_makezd2localzd2svarz00zzast_localz00(obj_t,
		BgL_typez00_bglt);
	extern obj_t BGl_nodezd2removez12zc0zzast_removez00(BgL_nodez00_bglt);
	extern BgL_nodez00_bglt BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00(obj_t,
		obj_t, obj_t);
	extern BgL_localz00_bglt
		BGl_makezd2userzd2localzd2svarzd2zzast_localz00(obj_t, BgL_typez00_bglt);
	static obj_t BGl_importedzd2moduleszd2initz00zzast_letz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzast_letz00(void);
	static obj_t BGl_appendza2ze70z45zzast_letz00(obj_t, obj_t);
	extern obj_t BGl_parsezd2idzd2zzast_identz00(obj_t, obj_t);
	static obj_t BGl_za2letza2z00zzast_letz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_letzd2symzd2zzast_letz00(void);
	extern BgL_nodez00_bglt BGl_sexpzd2ze3nodez31zzast_sexpz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern bool_t BGl_userzd2symbolzf3z21zzast_identz00(obj_t);
	extern obj_t BGl_za2unspecza2z00zztype_cachez00;
	extern obj_t BGl_za2callzf2cczf3za2z01zzengine_paramz00;
	extern obj_t BGl_occurzd2nodez12zc0zzast_occurz00(BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	extern obj_t BGl_usezd2variablez12zc0zzast_sexpz00(BgL_variablez00_bglt,
		obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	static bool_t BGl_loopze70ze7zzast_letz00(obj_t);
	static obj_t BGl_loopze71ze7zzast_letz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_loopze72ze7zzast_letz00(obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_letrecza2zd2ze3nodez93zzast_letz00(obj_t, obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62splitzd2headzd2letrecz62zzast_letz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static BgL_nodezf2effectzf2_bglt
		BGl_makezd2smartzd2genericzd2letzd2zzast_letz00(obj_t,
		BgL_letzd2varzd2_bglt, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32833ze3ze5zzast_letz00(obj_t, obj_t);
	static obj_t __cnst[39];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_functionzf3zd2envz21zzast_letz00,
		BgL_bgl__functionza7f3za7f3za73825za7, opt_generic_entry,
		BGl__functionzf3zf3zzast_letz00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_letzd2symzd2envz00zzast_letz00,
		BgL_bgl_za762letza7d2symza7b0za73826z00, BGl_z62letzd2symzb0zzast_letz00,
		0L, BUNSPEC, 0);
	      DEFINE_INT64(BGl_int643817z00zzast_letz00,
		BgL_bgl_int643817za700za7za7as3827za7, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_letrecza2zd2ze3nodezd2envz41zzast_letz00,
		BgL_bgl_za762letrecza7a2za7d2za73828z00,
		BGl_z62letrecza2zd2ze3nodezf1zzast_letz00, 0L, BUNSPEC, 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_letzd2ze3nodezd2envze3zzast_letz00,
		BgL_bgl_za762letza7d2za7e3node3829za7, BGl_z62letzd2ze3nodez53zzast_letz00,
		0L, BUNSPEC, 4);
	      DEFINE_UINT64(BGl_uint643818z00zzast_letz00,
		BgL_bgl_uint643818za700za7za7a3830za7, 0);
	      DEFINE_INT32(BGl_int323815z00zzast_letz00,
		BgL_bgl_int323815za700za7za7as3831za7, 0);
	      DEFINE_REAL(BGl_real3819z00zzast_letz00,
		BgL_bgl_real3819za700za7za7ast3832za7, 0.0);
	      DEFINE_STRING(BGl_string3807z00zzast_letz00,
		BgL_bgl_string3807za700za7za7a3833za7, "Illegal `", 9);
	      DEFINE_STRING(BGl_string3808z00zzast_letz00,
		BgL_bgl_string3808za700za7za7a3834za7, "' form", 6);
	      DEFINE_STRING(BGl_string3810z00zzast_letz00,
		BgL_bgl_string3810za700za7za7a3835za7, "Illegal 'letrec*' form", 22);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc3809z00zzast_letz00,
		BgL_bgl_za762split3801za762za73836za7, BGl_z62split3801z62zzast_letz00);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_letzd2symzf3zd2envzf3zzast_letz00,
		BgL_bgl_za762letza7d2symza7f3za73837z00, BGl_z62letzd2symzf3z43zzast_letz00,
		0L, BUNSPEC, 1);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc3811z00zzast_letz00,
		BgL_bgl_za762split3802za762za73838za7, BGl_z62split3802z62zzast_letz00);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc3812z00zzast_letz00,
		BgL_bgl_za762split3803za762za73839za7, BGl_z62split3803z62zzast_letz00);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3813z00zzast_letz00,
		BgL_bgl_za762za7c3za704anonymo3840za7,
		BGl_z62zc3z04anonymousza32833ze3ze5zzast_letz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3820z00zzast_letz00,
		BgL_bgl_string3820za700za7za7a3841za7, "type-undefined", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3814z00zzast_letz00,
		BgL_bgl_za762za7c3za704anonymo3842za7,
		BGl_z62zc3z04anonymousza32872ze3ze5zzast_letz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3821z00zzast_letz00,
		BgL_bgl_string3821za700za7za7a3843za7, "cannot undefined type", 21);
	      DEFINE_STRING(BGl_string3822z00zzast_letz00,
		BgL_bgl_string3822za700za7za7a3844za7, "ast_let", 7);
	      DEFINE_STRING(BGl_string3823z00zzast_letz00,
		BgL_bgl_string3823za700za7za7a3845za7,
		"not if (+ - * +fx -fx *fx +elong -elong *elong +llong -llong *llong +s8 -s8 *s8 +u8 -u8 *u8 +s16 -s16 *s16 +u16 -u16 *u16 +s32 -s32 *s32 +u32 -u32 *u32 +s64 -s64 *s64 +u64 -u64 *u64 +f32 -f32 *f32 +f64 -f64 *f64 > >= < <= = >fx >=fx <fx <=fx =fx >elong >=elong <elong <=elong =elong >llong >=llong <llong <=llong =llong >s8 >=s8 <s8 <=s8 =s8 >u8 >=u8 <u8 <=u8 =u8 >s16 >=s16 <s16 <=s16 =s16 >u16 >=u16 <u16 <=u16 =u16 >s32 >=s32 <s32 <=s32 =s32 >u32 >=u32 <u32 <=u32 =u32 >s64 >=s64 <s64 <=s64 =s64 >u64 >=u64 <u64 <=u64 =u64 >f32 >=f32 <f32 <=f32 =f32 >f64 >=f64 <f64 <=f64 =f64 eq? equal? bit-lsh bit-rsh bit-ursh bit-not bit-xor bit-lshelong bit-rshelong bit-urshelong bit-notelong bit-xorelong bit-lshs8 bit-rshs8 bit-urshs8 bit-nots8 bit-xors8 bit-lshu8 bit-rshu8 bit-urshu8 bit-notu8 bit-xoru8 bit-lshs16 bit-rshs16 bit-urshs16 bit-nots16 bit-xors16 bit-lshu16 bit-rshu16 bit-urshu16 bit-notu16 bit-xoru16 bit-lshs32 bit-rshs32 bit-urshs32 bit-nots32 bit-xors32 bit-lshu32 bit-rshu32 bit-urshu32 bit-notu32 bit-xoru32 "
		"bit-lshs64 bit-rshs64 bit-urshs64 bit-nots64 bit-xors64 bit-lshu64 bit-rshu64 bit-urshu64 bit-notu64 bit-xoru64) char make-cell cell list procedure bool long int real double uint64 int64 uint32 int32 uint16 int16 uint8 int8 _ obj class-nil @ (_ obj) let* quote letrec begin labels lambda read set! letrec* write location value let ",
		1355);
	      DEFINE_UINT32(BGl_uint323816z00zzast_letz00,
		BgL_bgl_uint323816za700za7za7a3846za7, 0);
	BGL_IMPORT obj_t BGl_expzd2envzd2zz__r4_numbers_6_5z00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzast_letz00));
		     ADD_ROOT((void *) (&BGl_za2letza2z00zzast_letz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzast_letz00(long
		BgL_checksumz00_8277, char *BgL_fromz00_8278)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_letz00))
				{
					BGl_requirezd2initializa7ationz75zzast_letz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_letz00();
					BGl_libraryzd2moduleszd2initz00zzast_letz00();
					BGl_cnstzd2initzd2zzast_letz00();
					BGl_importedzd2moduleszd2initz00zzast_letz00();
					return BGl_toplevelzd2initzd2zzast_letz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_letz00(void)
	{
		{	/* Ast/let.scm 14 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_let");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "ast_let");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "ast_let");
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(0L,
				"ast_let");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_let");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "ast_let");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "ast_let");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "ast_let");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "ast_let");
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(0L, "ast_let");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L, "ast_let");
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(0L,
				"ast_let");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L, "ast_let");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "ast_let");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"ast_let");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "ast_let");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "ast_let");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_letz00(void)
	{
		{	/* Ast/let.scm 14 */
			{	/* Ast/let.scm 14 */
				obj_t BgL_cportz00_7849;

				{	/* Ast/let.scm 14 */
					obj_t BgL_stringz00_7856;

					BgL_stringz00_7856 = BGl_string3823z00zzast_letz00;
					{	/* Ast/let.scm 14 */
						obj_t BgL_startz00_7857;

						BgL_startz00_7857 = BINT(0L);
						{	/* Ast/let.scm 14 */
							obj_t BgL_endz00_7858;

							BgL_endz00_7858 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_7856)));
							{	/* Ast/let.scm 14 */

								BgL_cportz00_7849 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_7856, BgL_startz00_7857, BgL_endz00_7858);
				}}}}
				{
					long BgL_iz00_7850;

					BgL_iz00_7850 = 38L;
				BgL_loopz00_7851:
					if ((BgL_iz00_7850 == -1L))
						{	/* Ast/let.scm 14 */
							return BUNSPEC;
						}
					else
						{	/* Ast/let.scm 14 */
							{	/* Ast/let.scm 14 */
								obj_t BgL_arg3824z00_7852;

								{	/* Ast/let.scm 14 */

									{	/* Ast/let.scm 14 */
										obj_t BgL_locationz00_7854;

										BgL_locationz00_7854 = BBOOL(((bool_t) 0));
										{	/* Ast/let.scm 14 */

											BgL_arg3824z00_7852 =
												BGl_readz00zz__readerz00(BgL_cportz00_7849,
												BgL_locationz00_7854);
										}
									}
								}
								{	/* Ast/let.scm 14 */
									int BgL_tmpz00_8313;

									BgL_tmpz00_8313 = (int) (BgL_iz00_7850);
									CNST_TABLE_SET(BgL_tmpz00_8313, BgL_arg3824z00_7852);
							}}
							{	/* Ast/let.scm 14 */
								int BgL_auxz00_7855;

								BgL_auxz00_7855 = (int) ((BgL_iz00_7850 - 1L));
								{
									long BgL_iz00_8318;

									BgL_iz00_8318 = (long) (BgL_auxz00_7855);
									BgL_iz00_7850 = BgL_iz00_8318;
									goto BgL_loopz00_7851;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_letz00(void)
	{
		{	/* Ast/let.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzast_letz00(void)
	{
		{	/* Ast/let.scm 14 */
			return (BGl_za2letza2z00zzast_letz00 =
				BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(0)), BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzast_letz00(obj_t BgL_l1z00_1, obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1712;

				BgL_headz00_1712 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1713;
					obj_t BgL_tailz00_1714;

					BgL_prevz00_1713 = BgL_headz00_1712;
					BgL_tailz00_1714 = BgL_l1z00_1;
				BgL_loopz00_1715:
					if (PAIRP(BgL_tailz00_1714))
						{
							obj_t BgL_newzd2prevzd2_1717;

							BgL_newzd2prevzd2_1717 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1714), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1713, BgL_newzd2prevzd2_1717);
							{
								obj_t BgL_tailz00_8330;
								obj_t BgL_prevz00_8329;

								BgL_prevz00_8329 = BgL_newzd2prevzd2_1717;
								BgL_tailz00_8330 = CDR(BgL_tailz00_1714);
								BgL_tailz00_1714 = BgL_tailz00_8330;
								BgL_prevz00_1713 = BgL_prevz00_8329;
								goto BgL_loopz00_1715;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1712);
				}
			}
		}

	}



/* let-sym */
	BGL_EXPORTED_DEF obj_t BGl_letzd2symzd2zzast_letz00(void)
	{
		{	/* Ast/let.scm 46 */
			return BGl_za2letza2z00zzast_letz00;
		}

	}



/* &let-sym */
	obj_t BGl_z62letzd2symzb0zzast_letz00(obj_t BgL_envz00_7736)
	{
		{	/* Ast/let.scm 46 */
			return BGl_letzd2symzd2zzast_letz00();
		}

	}



/* let-sym? */
	BGL_EXPORTED_DEF obj_t BGl_letzd2symzf3z21zzast_letz00(obj_t BgL_symz00_17)
	{
		{	/* Ast/let.scm 52 */
			return BBOOL((BgL_symz00_17 == BGl_za2letza2z00zzast_letz00));
		}

	}



/* &let-sym? */
	obj_t BGl_z62letzd2symzf3z43zzast_letz00(obj_t BgL_envz00_7737,
		obj_t BgL_symz00_7738)
	{
		{	/* Ast/let.scm 52 */
			return BGl_letzd2symzf3z21zzast_letz00(BgL_symz00_7738);
		}

	}



/* let->node */
	BGL_EXPORTED_DEF BgL_nodez00_bglt BGl_letzd2ze3nodez31zzast_letz00(obj_t
		BgL_expz00_18, obj_t BgL_stackz00_19, obj_t BgL_olocz00_20,
		obj_t BgL_sitez00_21)
	{
		{	/* Ast/let.scm 58 */
			{
				obj_t BgL_bindingsz00_1734;
				obj_t BgL_bodyz00_1732;

				if (PAIRP(BgL_expz00_18))
					{	/* Ast/let.scm 69 */
						obj_t BgL_cdrzd2367zd2_1739;

						BgL_cdrzd2367zd2_1739 = CDR(((obj_t) BgL_expz00_18));
						if (PAIRP(BgL_cdrzd2367zd2_1739))
							{	/* Ast/let.scm 69 */
								if (NULLP(CAR(BgL_cdrzd2367zd2_1739)))
									{	/* Ast/let.scm 69 */
										obj_t BgL_arg1611z00_1743;

										BgL_arg1611z00_1743 = CDR(BgL_cdrzd2367zd2_1739);
										{
											BgL_letzd2varzd2_bglt BgL_auxz00_8347;

											BgL_bodyz00_1732 = BgL_arg1611z00_1743;
											{	/* Ast/let.scm 72 */
												obj_t BgL_nlocz00_1747;

												BgL_nlocz00_1747 =
													BGl_findzd2locationzf2locz20zztools_locationz00
													(BgL_expz00_18, BgL_olocz00_20);
												{	/* Ast/let.scm 72 */
													obj_t BgL_blocz00_1748;

													if (PAIRP(BgL_bodyz00_1732))
														{	/* Ast/let.scm 73 */
															BgL_blocz00_1748 =
																BGl_findzd2locationzf2locz20zztools_locationz00
																(CAR(BgL_bodyz00_1732), BgL_nlocz00_1747);
														}
													else
														{	/* Ast/let.scm 73 */
															BgL_blocz00_1748 = BgL_nlocz00_1747;
														}
													{	/* Ast/let.scm 73 */
														BgL_nodez00_bglt BgL_bodyz00_1749;

														{	/* Ast/let.scm 76 */
															obj_t BgL_arg1629z00_1755;

															BgL_arg1629z00_1755 =
																BGl_normaliza7ezd2prognz75zztools_prognz00
																(BgL_bodyz00_1732);
															BgL_bodyz00_1749 =
																BGl_sexpzd2ze3nodez31zzast_sexpz00
																(BgL_arg1629z00_1755, BgL_stackz00_19,
																BgL_blocz00_1748, CNST_TABLE_REF(1));
														}
														{	/* Ast/let.scm 76 */

															{	/* Ast/let.scm 81 */
																BgL_letzd2varzd2_bglt BgL_new1111z00_1750;

																{	/* Ast/let.scm 82 */
																	BgL_letzd2varzd2_bglt BgL_new1110z00_1753;

																	BgL_new1110z00_1753 =
																		((BgL_letzd2varzd2_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_letzd2varzd2_bgl))));
																	{	/* Ast/let.scm 82 */
																		long BgL_arg1627z00_1754;

																		BgL_arg1627z00_1754 =
																			BGL_CLASS_NUM
																			(BGl_letzd2varzd2zzast_nodez00);
																		BGL_OBJECT_CLASS_NUM_SET((
																				(BgL_objectz00_bglt)
																				BgL_new1110z00_1753),
																			BgL_arg1627z00_1754);
																	}
																	{	/* Ast/let.scm 82 */
																		BgL_objectz00_bglt BgL_tmpz00_8360;

																		BgL_tmpz00_8360 =
																			((BgL_objectz00_bglt)
																			BgL_new1110z00_1753);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8360,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1110z00_1753);
																	BgL_new1111z00_1750 = BgL_new1110z00_1753;
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1111z00_1750)))->BgL_locz00) =
																	((obj_t) BgL_nlocz00_1747), BUNSPEC);
																{
																	BgL_typez00_bglt BgL_auxz00_8366;

																	{	/* Ast/let.scm 83 */
																		BgL_typez00_bglt BgL_arg1625z00_1751;

																		BgL_arg1625z00_1751 =
																			(((BgL_nodez00_bglt)
																				COBJECT(BgL_bodyz00_1749))->
																			BgL_typez00);
																		BgL_auxz00_8366 =
																			BGl_strictzd2nodezd2typez00zzast_nodez00((
																				(BgL_typez00_bglt)
																				BGl_za2_za2z00zztype_cachez00),
																			BgL_arg1625z00_1751);
																	}
																	((((BgL_nodez00_bglt) COBJECT(
																					((BgL_nodez00_bglt)
																						BgL_new1111z00_1750)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) BgL_auxz00_8366),
																		BUNSPEC);
																}
																((((BgL_nodezf2effectzf2_bglt) COBJECT(
																				((BgL_nodezf2effectzf2_bglt)
																					BgL_new1111z00_1750)))->
																		BgL_sidezd2effectzd2) =
																	((obj_t) BUNSPEC), BUNSPEC);
																((((BgL_nodezf2effectzf2_bglt)
																			COBJECT(((BgL_nodezf2effectzf2_bglt)
																					BgL_new1111z00_1750)))->BgL_keyz00) =
																	((obj_t) BINT(-1L)), BUNSPEC);
																((((BgL_letzd2varzd2_bglt)
																			COBJECT(BgL_new1111z00_1750))->
																		BgL_bindingsz00) = ((obj_t) BNIL), BUNSPEC);
																((((BgL_letzd2varzd2_bglt)
																			COBJECT(BgL_new1111z00_1750))->
																		BgL_bodyz00) =
																	((BgL_nodez00_bglt) BgL_bodyz00_1749),
																	BUNSPEC);
																{
																	bool_t BgL_auxz00_8379;

																	{	/* Ast/let.scm 86 */
																		obj_t BgL_arg1626z00_1752;

																		BgL_arg1626z00_1752 =
																			BGl_thezd2backendzd2zzbackend_backendz00
																			();
																		BgL_auxz00_8379 =
																			(((BgL_backendz00_bglt)
																				COBJECT(((BgL_backendz00_bglt)
																						BgL_arg1626z00_1752)))->
																			BgL_removezd2emptyzd2letz00);
																	}
																	((((BgL_letzd2varzd2_bglt)
																				COBJECT(BgL_new1111z00_1750))->
																			BgL_removablezf3zf3) =
																		((bool_t) BgL_auxz00_8379), BUNSPEC);
																}
																BgL_auxz00_8347 = BgL_new1111z00_1750;
											}}}}}
											return ((BgL_nodez00_bglt) BgL_auxz00_8347);
										}
									}
								else
									{	/* Ast/let.scm 69 */
										obj_t BgL_arg1613z00_1744;

										BgL_arg1613z00_1744 = CAR(((obj_t) BgL_cdrzd2367zd2_1739));
										BgL_bindingsz00_1734 = BgL_arg1613z00_1744;
										{	/* Ast/let.scm 88 */
											bool_t BgL_test3854z00_8387;

											{	/* Ast/let.scm 88 */
												bool_t BgL_test3855z00_8388;

												if (PAIRP(BgL_bindingsz00_1734))
													{	/* Ast/let.scm 88 */
														BgL_test3855z00_8388 = ((bool_t) 1);
													}
												else
													{	/* Ast/let.scm 88 */
														BgL_test3855z00_8388 = NULLP(BgL_bindingsz00_1734);
													}
												if (BgL_test3855z00_8388)
													{
														obj_t BgL_bindingsz00_1786;

														BgL_bindingsz00_1786 = BgL_bindingsz00_1734;
													BgL_zc3z04anonymousza31692ze3z87_1787:
														if (NULLP(BgL_bindingsz00_1786))
															{	/* Ast/let.scm 90 */
																BgL_test3854z00_8387 = ((bool_t) 0);
															}
														else
															{	/* Ast/let.scm 92 */
																obj_t BgL_bindingz00_1789;

																BgL_bindingz00_1789 =
																	CAR(((obj_t) BgL_bindingsz00_1786));
																{

																	if (PAIRP(BgL_bindingz00_1789))
																		{	/* Ast/let.scm 93 */
																			obj_t BgL_cdrzd2388zd2_1794;

																			BgL_cdrzd2388zd2_1794 =
																				CDR(BgL_bindingz00_1789);
																			if (PAIRP(BgL_cdrzd2388zd2_1794))
																				{	/* Ast/let.scm 93 */
																					if (NULLP(CDR(BgL_cdrzd2388zd2_1794)))
																						{	/* Ast/let.scm 93 */
																							{
																								obj_t BgL_bindingsz00_8404;

																								BgL_bindingsz00_8404 =
																									CDR(
																									((obj_t)
																										BgL_bindingsz00_1786));
																								BgL_bindingsz00_1786 =
																									BgL_bindingsz00_8404;
																								goto
																									BgL_zc3z04anonymousza31692ze3z87_1787;
																							}
																						}
																					else
																						{	/* Ast/let.scm 93 */
																							BgL_test3854z00_8387 =
																								((bool_t) 1);
																						}
																				}
																			else
																				{	/* Ast/let.scm 93 */
																					BgL_test3854z00_8387 = ((bool_t) 1);
																				}
																		}
																	else
																		{	/* Ast/let.scm 93 */
																			BgL_test3854z00_8387 = ((bool_t) 1);
																		}
																}
															}
													}
												else
													{	/* Ast/let.scm 88 */
														BgL_test3854z00_8387 = ((bool_t) 1);
													}
											}
											if (BgL_test3854z00_8387)
												{	/* Ast/let.scm 99 */
													obj_t BgL_arg1675z00_1778;
													obj_t BgL_arg1678z00_1779;

													{	/* Ast/let.scm 99 */
														obj_t BgL_arg1681z00_1780;

														{	/* Ast/let.scm 99 */
															obj_t BgL_arg1688z00_1781;

															BgL_arg1688z00_1781 =
																CAR(((obj_t) BgL_expz00_18));
															{	/* Ast/let.scm 99 */
																obj_t BgL_arg1455z00_4812;

																BgL_arg1455z00_4812 =
																	SYMBOL_TO_STRING(
																	((obj_t) BgL_arg1688z00_1781));
																BgL_arg1681z00_1780 =
																	BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																	(BgL_arg1455z00_4812);
															}
														}
														BgL_arg1675z00_1778 =
															string_append_3(BGl_string3807z00zzast_letz00,
															BgL_arg1681z00_1780,
															BGl_string3808z00zzast_letz00);
													}
													BgL_arg1678z00_1779 =
														BGl_findzd2locationzf2locz20zztools_locationz00
														(BgL_expz00_18, BgL_olocz00_20);
													return
														BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00
														(BgL_arg1675z00_1778, BgL_expz00_18,
														BgL_arg1678z00_1779);
												}
											else
												{	/* Ast/let.scm 103 */
													obj_t BgL_arg1689z00_1782;
													BgL_letzd2varzd2_bglt BgL_arg1691z00_1783;

													BgL_arg1689z00_1782 = CAR(((obj_t) BgL_expz00_18));
													BgL_arg1691z00_1783 =
														BGl_makezd2genericzd2letz00zzast_letz00
														(BgL_expz00_18, BgL_stackz00_19, BgL_olocz00_20,
														BgL_sitez00_21);
													return ((BgL_nodez00_bglt)
														BGl_makezd2smartzd2genericzd2letzd2zzast_letz00
														(BgL_arg1689z00_1782, BgL_arg1691z00_1783,
															BgL_sitez00_21));
												}
										}
									}
							}
						else
							{	/* Ast/let.scm 69 */
							BgL_tagzd2361zd2_1736:
								{	/* Ast/let.scm 108 */
									obj_t BgL_arg1701z00_1801;
									obj_t BgL_arg1702z00_1802;

									{	/* Ast/let.scm 108 */
										obj_t BgL_arg1703z00_1803;

										{	/* Ast/let.scm 108 */
											obj_t BgL_arg1705z00_1804;

											BgL_arg1705z00_1804 = CAR(((obj_t) BgL_expz00_18));
											{	/* Ast/let.scm 108 */
												obj_t BgL_arg1455z00_4816;

												BgL_arg1455z00_4816 =
													SYMBOL_TO_STRING(((obj_t) BgL_arg1705z00_1804));
												BgL_arg1703z00_1803 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_4816);
											}
										}
										BgL_arg1701z00_1801 =
											string_append_3(BGl_string3807z00zzast_letz00,
											BgL_arg1703z00_1803, BGl_string3808z00zzast_letz00);
									}
									BgL_arg1702z00_1802 =
										BGl_findzd2locationzf2locz20zztools_locationz00
										(BgL_expz00_18, BgL_olocz00_20);
									return
										BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00
										(BgL_arg1701z00_1801, BgL_expz00_18, BgL_arg1702z00_1802);
								}
							}
					}
				else
					{	/* Ast/let.scm 69 */
						goto BgL_tagzd2361zd2_1736;
					}
			}
		}

	}



/* &let->node */
	BgL_nodez00_bglt BGl_z62letzd2ze3nodez53zzast_letz00(obj_t BgL_envz00_7739,
		obj_t BgL_expz00_7740, obj_t BgL_stackz00_7741, obj_t BgL_olocz00_7742,
		obj_t BgL_sitez00_7743)
	{
		{	/* Ast/let.scm 58 */
			return
				BGl_letzd2ze3nodez31zzast_letz00(BgL_expz00_7740, BgL_stackz00_7741,
				BgL_olocz00_7742, BgL_sitez00_7743);
		}

	}



/* make-generic-let */
	BgL_letzd2varzd2_bglt BGl_makezd2genericzd2letz00zzast_letz00(obj_t
		BgL_expz00_22, obj_t BgL_stackz00_23, obj_t BgL_olocz00_24,
		obj_t BgL_sitez00_25)
	{
		{	/* Ast/let.scm 115 */
			{	/* Ast/let.scm 116 */
				obj_t BgL_bindingsz00_1805;

				{	/* Ast/let.scm 116 */
					obj_t BgL_pairz00_4825;

					BgL_pairz00_4825 = CDR(((obj_t) BgL_expz00_22));
					BgL_bindingsz00_1805 = CAR(BgL_pairz00_4825);
				}
				{	/* Ast/let.scm 116 */
					obj_t BgL_locz00_1806;

					BgL_locz00_1806 =
						BGl_findzd2locationzf2locz20zztools_locationz00(BgL_expz00_22,
						BgL_olocz00_24);
					{	/* Ast/let.scm 117 */
						obj_t BgL_blocz00_1807;

						{	/* Ast/let.scm 118 */
							bool_t BgL_test3861z00_8433;

							{	/* Ast/let.scm 118 */
								obj_t BgL_tmpz00_8434;

								{	/* Ast/let.scm 118 */
									obj_t BgL_pairz00_4829;

									BgL_pairz00_4829 = CDR(((obj_t) BgL_expz00_22));
									BgL_tmpz00_8434 = CDR(BgL_pairz00_4829);
								}
								BgL_test3861z00_8433 = PAIRP(BgL_tmpz00_8434);
							}
							if (BgL_test3861z00_8433)
								{	/* Ast/let.scm 119 */
									obj_t BgL_arg1773z00_1882;

									{	/* Ast/let.scm 119 */
										obj_t BgL_pairz00_4835;

										{	/* Ast/let.scm 119 */
											obj_t BgL_pairz00_4834;

											BgL_pairz00_4834 = CDR(((obj_t) BgL_expz00_22));
											BgL_pairz00_4835 = CDR(BgL_pairz00_4834);
										}
										BgL_arg1773z00_1882 = CAR(BgL_pairz00_4835);
									}
									BgL_blocz00_1807 =
										BGl_findzd2locationzf2locz20zztools_locationz00
										(BgL_arg1773z00_1882, BFALSE);
								}
							else
								{	/* Ast/let.scm 118 */
									BgL_blocz00_1807 = BFALSE;
								}
						}
						{	/* Ast/let.scm 121 */
							obj_t BgL_bodyz00_1809;

							{	/* Ast/let.scm 124 */
								obj_t BgL_arg1765z00_1876;

								{	/* Ast/let.scm 124 */
									obj_t BgL_pairz00_4849;

									BgL_pairz00_4849 = CDR(((obj_t) BgL_expz00_22));
									BgL_arg1765z00_1876 = CDR(BgL_pairz00_4849);
								}
								BgL_bodyz00_1809 =
									BGl_normaliza7ezd2prognz75zztools_prognz00
									(BgL_arg1765z00_1876);
							}
							{	/* Ast/let.scm 124 */
								obj_t BgL_loczd2biszd2_1810;

								BgL_loczd2biszd2_1810 =
									BGl_findzd2locationzf2locz20zztools_locationz00
									(BgL_bodyz00_1809, BgL_locz00_1806);
								{	/* Ast/let.scm 125 */
									obj_t BgL_nlocz00_1811;

									{	/* Ast/let.scm 126 */
										bool_t BgL_test3862z00_8449;

										if (STRUCTP(BgL_blocz00_1807))
											{	/* Ast/let.scm 126 */
												BgL_test3862z00_8449 =
													(STRUCT_KEY(BgL_blocz00_1807) == CNST_TABLE_REF(2));
											}
										else
											{	/* Ast/let.scm 126 */
												BgL_test3862z00_8449 = ((bool_t) 0);
											}
										if (BgL_test3862z00_8449)
											{	/* Ast/let.scm 126 */
												BgL_nlocz00_1811 = BgL_blocz00_1807;
											}
										else
											{	/* Ast/let.scm 126 */
												BgL_nlocz00_1811 = BgL_locz00_1806;
											}
									}
									{	/* Ast/let.scm 126 */
										obj_t BgL_framez00_1812;

										if (NULLP(BgL_bindingsz00_1805))
											{	/* Ast/let.scm 129 */
												BgL_framez00_1812 = BNIL;
											}
										else
											{	/* Ast/let.scm 129 */
												obj_t BgL_head1359z00_1859;

												BgL_head1359z00_1859 = MAKE_YOUNG_PAIR(BNIL, BNIL);
												{
													obj_t BgL_l1357z00_1861;
													obj_t BgL_tail1360z00_1862;

													BgL_l1357z00_1861 = BgL_bindingsz00_1805;
													BgL_tail1360z00_1862 = BgL_head1359z00_1859;
												BgL_zc3z04anonymousza31751ze3z87_1863:
													if (NULLP(BgL_l1357z00_1861))
														{	/* Ast/let.scm 129 */
															BgL_framez00_1812 = CDR(BgL_head1359z00_1859);
														}
													else
														{	/* Ast/let.scm 129 */
															obj_t BgL_newtail1361z00_1865;

															{	/* Ast/let.scm 129 */
																BgL_localz00_bglt BgL_arg1754z00_1867;

																{	/* Ast/let.scm 129 */
																	obj_t BgL_bindingz00_1868;

																	BgL_bindingz00_1868 =
																		CAR(((obj_t) BgL_l1357z00_1861));
																	{	/* Ast/let.scm 130 */
																		obj_t BgL_varzd2idzd2_1869;

																		{	/* Ast/let.scm 130 */
																			obj_t BgL_arg1761z00_1873;

																			BgL_arg1761z00_1873 =
																				CAR(((obj_t) BgL_bindingz00_1868));
																			BgL_varzd2idzd2_1869 =
																				BGl_parsezd2idzd2zzast_identz00
																				(BgL_arg1761z00_1873, BgL_nlocz00_1811);
																		}
																		{	/* Ast/let.scm 130 */
																			obj_t BgL_idz00_1870;

																			BgL_idz00_1870 =
																				CAR(BgL_varzd2idzd2_1869);
																			{	/* Ast/let.scm 131 */
																				obj_t BgL_typez00_1871;

																				BgL_typez00_1871 =
																					CDR(BgL_varzd2idzd2_1869);
																				{	/* Ast/let.scm 132 */

																					if (BGl_userzd2symbolzf3z21zzast_identz00(BgL_idz00_1870))
																						{	/* Ast/let.scm 133 */
																							BgL_arg1754z00_1867 =
																								BGl_makezd2userzd2localzd2svarzd2zzast_localz00
																								(BgL_idz00_1870,
																								((BgL_typez00_bglt)
																									BgL_typez00_1871));
																						}
																					else
																						{	/* Ast/let.scm 133 */
																							BgL_arg1754z00_1867 =
																								BGl_makezd2localzd2svarz00zzast_localz00
																								(BgL_idz00_1870,
																								((BgL_typez00_bglt)
																									BgL_typez00_1871));
																						}
																				}
																			}
																		}
																	}
																}
																BgL_newtail1361z00_1865 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_arg1754z00_1867), BNIL);
															}
															SET_CDR(BgL_tail1360z00_1862,
																BgL_newtail1361z00_1865);
															{	/* Ast/let.scm 129 */
																obj_t BgL_arg1753z00_1866;

																BgL_arg1753z00_1866 =
																	CDR(((obj_t) BgL_l1357z00_1861));
																{
																	obj_t BgL_tail1360z00_8480;
																	obj_t BgL_l1357z00_8479;

																	BgL_l1357z00_8479 = BgL_arg1753z00_1866;
																	BgL_tail1360z00_8480 =
																		BgL_newtail1361z00_1865;
																	BgL_tail1360z00_1862 = BgL_tail1360z00_8480;
																	BgL_l1357z00_1861 = BgL_l1357z00_8479;
																	goto BgL_zc3z04anonymousza31751ze3z87_1863;
																}
															}
														}
												}
											}
										{	/* Ast/let.scm 129 */
											obj_t BgL_newzd2stackzd2_1813;

											BgL_newzd2stackzd2_1813 =
												BGl_appendzd221011zd2zzast_letz00(BgL_framez00_1812,
												BgL_stackz00_23);
											{	/* Ast/let.scm 137 */

												{	/* Ast/let.scm 144 */
													BgL_nodez00_bglt BgL_bodyz00_1814;

													BgL_bodyz00_1814 =
														BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_bodyz00_1809,
														BgL_newzd2stackzd2_1813, BgL_nlocz00_1811,
														CNST_TABLE_REF(1));
													{	/* Ast/let.scm 144 */
														obj_t BgL_bstackz00_1815;

														{	/* Ast/let.scm 145 */
															bool_t BgL_test3867z00_8484;

															if (
																(CAR(
																		((obj_t) BgL_expz00_22)) ==
																	CNST_TABLE_REF(0)))
																{	/* Ast/let.scm 145 */
																	BgL_test3867z00_8484 = ((bool_t) 1);
																}
															else
																{	/* Ast/let.scm 145 */
																	obj_t BgL_arg1748z00_1855;

																	BgL_arg1748z00_1855 =
																		CAR(((obj_t) BgL_expz00_22));
																	BgL_test3867z00_8484 =
																		(BgL_arg1748z00_1855 ==
																		BGl_za2letza2z00zzast_letz00);
																}
															if (BgL_test3867z00_8484)
																{	/* Ast/let.scm 145 */
																	BgL_bstackz00_1815 = BgL_stackz00_23;
																}
															else
																{	/* Ast/let.scm 145 */
																	BgL_bstackz00_1815 = BgL_newzd2stackzd2_1813;
																}
														}
														{	/* Ast/let.scm 145 */
															obj_t BgL_bindingsz00_1816;

															if (NULLP(BgL_bindingsz00_1805))
																{	/* Ast/let.scm 148 */
																	BgL_bindingsz00_1816 = BNIL;
																}
															else
																{	/* Ast/let.scm 148 */
																	obj_t BgL_head1364z00_1831;

																	BgL_head1364z00_1831 =
																		MAKE_YOUNG_PAIR(BNIL, BNIL);
																	{
																		obj_t BgL_ll1362z00_1833;
																		obj_t BgL_ll1363z00_1834;
																		obj_t BgL_tail1365z00_1835;

																		BgL_ll1362z00_1833 = BgL_bindingsz00_1805;
																		BgL_ll1363z00_1834 = BgL_framez00_1812;
																		BgL_tail1365z00_1835 = BgL_head1364z00_1831;
																	BgL_zc3z04anonymousza31720ze3z87_1836:
																		if (NULLP(BgL_ll1362z00_1833))
																			{	/* Ast/let.scm 148 */
																				BgL_bindingsz00_1816 =
																					CDR(BgL_head1364z00_1831);
																			}
																		else
																			{	/* Ast/let.scm 148 */
																				obj_t BgL_newtail1366z00_1838;

																				{	/* Ast/let.scm 148 */
																					obj_t BgL_arg1733z00_1841;

																					{	/* Ast/let.scm 148 */
																						obj_t BgL_bindingz00_1842;
																						obj_t BgL_varz00_1843;

																						BgL_bindingz00_1842 =
																							CAR(((obj_t) BgL_ll1362z00_1833));
																						BgL_varz00_1843 =
																							CAR(((obj_t) BgL_ll1363z00_1834));
																						{	/* Ast/let.scm 151 */
																							BgL_nodez00_bglt
																								BgL_arg1734z00_1844;
																							{	/* Ast/let.scm 151 */
																								obj_t BgL_arg1735z00_1845;
																								obj_t BgL_arg1736z00_1846;

																								{	/* Ast/let.scm 151 */
																									obj_t BgL_arg1737z00_1847;

																									BgL_arg1737z00_1847 =
																										CDR(
																										((obj_t)
																											BgL_bindingz00_1842));
																									BgL_arg1735z00_1845 =
																										BGl_normaliza7ezd2prognz75zztools_prognz00
																										(BgL_arg1737z00_1847);
																								}
																								BgL_arg1736z00_1846 =
																									BGl_findzd2locationzf2locz20zztools_locationz00
																									(BgL_bindingz00_1842,
																									BgL_nlocz00_1811);
																								BgL_arg1734z00_1844 =
																									BGl_sexpzd2ze3nodez31zzast_sexpz00
																									(BgL_arg1735z00_1845,
																									BgL_bstackz00_1815,
																									BgL_arg1736z00_1846,
																									CNST_TABLE_REF(1));
																							}
																							BgL_arg1733z00_1841 =
																								MAKE_YOUNG_PAIR(BgL_varz00_1843,
																								((obj_t) BgL_arg1734z00_1844));
																						}
																					}
																					BgL_newtail1366z00_1838 =
																						MAKE_YOUNG_PAIR(BgL_arg1733z00_1841,
																						BNIL);
																				}
																				SET_CDR(BgL_tail1365z00_1835,
																					BgL_newtail1366z00_1838);
																				{	/* Ast/let.scm 148 */
																					obj_t BgL_arg1722z00_1839;
																					obj_t BgL_arg1724z00_1840;

																					BgL_arg1722z00_1839 =
																						CDR(((obj_t) BgL_ll1362z00_1833));
																					BgL_arg1724z00_1840 =
																						CDR(((obj_t) BgL_ll1363z00_1834));
																					{
																						obj_t BgL_tail1365z00_8519;
																						obj_t BgL_ll1363z00_8518;
																						obj_t BgL_ll1362z00_8517;

																						BgL_ll1362z00_8517 =
																							BgL_arg1722z00_1839;
																						BgL_ll1363z00_8518 =
																							BgL_arg1724z00_1840;
																						BgL_tail1365z00_8519 =
																							BgL_newtail1366z00_1838;
																						BgL_tail1365z00_1835 =
																							BgL_tail1365z00_8519;
																						BgL_ll1363z00_1834 =
																							BgL_ll1363z00_8518;
																						BgL_ll1362z00_1833 =
																							BgL_ll1362z00_8517;
																						goto
																							BgL_zc3z04anonymousza31720ze3z87_1836;
																					}
																				}
																			}
																	}
																}
															{	/* Ast/let.scm 148 */
																obj_t BgL_locz00_1817;

																{	/* Ast/let.scm 157 */
																	obj_t BgL_locz00_1822;

																	{	/* Ast/let.scm 159 */
																		obj_t BgL_arg1714z00_1824;

																		if (PAIRP(BgL_bindingsz00_1816))
																			{
																				BgL_nodez00_bglt BgL_auxz00_8522;

																				{	/* Ast/let.scm 160 */
																					obj_t BgL_pairz00_4871;

																					BgL_pairz00_4871 =
																						CAR(BgL_bindingsz00_1816);
																					BgL_auxz00_8522 =
																						((BgL_nodez00_bglt)
																						CDR(BgL_pairz00_4871));
																				}
																				BgL_arg1714z00_1824 =
																					(((BgL_nodez00_bglt)
																						COBJECT(BgL_auxz00_8522))->
																					BgL_locz00);
																			}
																		else
																			{	/* Ast/let.scm 159 */
																				BgL_arg1714z00_1824 =
																					(((BgL_nodez00_bglt)
																						COBJECT(BgL_bodyz00_1814))->
																					BgL_locz00);
																			}
																		BgL_locz00_1822 =
																			BGl_findzd2locationzf2locz20zztools_locationz00
																			(BgL_expz00_22, BgL_arg1714z00_1824);
																	}
																	{	/* Ast/let.scm 162 */
																		bool_t BgL_test3872z00_8529;

																		if (STRUCTP(BgL_locz00_1822))
																			{	/* Ast/let.scm 162 */
																				BgL_test3872z00_8529 =
																					(STRUCT_KEY(BgL_locz00_1822) ==
																					CNST_TABLE_REF(2));
																			}
																		else
																			{	/* Ast/let.scm 162 */
																				BgL_test3872z00_8529 = ((bool_t) 0);
																			}
																		if (BgL_test3872z00_8529)
																			{	/* Ast/let.scm 162 */
																				BgL_locz00_1817 = BgL_locz00_1822;
																			}
																		else
																			{	/* Ast/let.scm 162 */
																				BgL_locz00_1817 = BgL_olocz00_24;
																			}
																	}
																}
																{	/* Ast/let.scm 157 */
																	BgL_letzd2varzd2_bglt BgL_nodez00_1818;

																	{	/* Ast/let.scm 165 */
																		BgL_letzd2varzd2_bglt BgL_new1114z00_1819;

																		{	/* Ast/let.scm 166 */
																			BgL_letzd2varzd2_bglt BgL_new1113z00_1820;

																			BgL_new1113z00_1820 =
																				((BgL_letzd2varzd2_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_letzd2varzd2_bgl))));
																			{	/* Ast/let.scm 166 */
																				long BgL_arg1708z00_1821;

																				BgL_arg1708z00_1821 =
																					BGL_CLASS_NUM
																					(BGl_letzd2varzd2zzast_nodez00);
																				BGL_OBJECT_CLASS_NUM_SET((
																						(BgL_objectz00_bglt)
																						BgL_new1113z00_1820),
																					BgL_arg1708z00_1821);
																			}
																			{	/* Ast/let.scm 166 */
																				BgL_objectz00_bglt BgL_tmpz00_8539;

																				BgL_tmpz00_8539 =
																					((BgL_objectz00_bglt)
																					BgL_new1113z00_1820);
																				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8539,
																					BFALSE);
																			}
																			((BgL_objectz00_bglt)
																				BgL_new1113z00_1820);
																			BgL_new1114z00_1819 = BgL_new1113z00_1820;
																		}
																		((((BgL_nodez00_bglt) COBJECT(
																						((BgL_nodez00_bglt)
																							BgL_new1114z00_1819)))->
																				BgL_locz00) =
																			((obj_t) BgL_locz00_1817), BUNSPEC);
																		((((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt)
																							BgL_new1114z00_1819)))->
																				BgL_typez00) =
																			((BgL_typez00_bglt) ((BgL_typez00_bglt)
																					BGl_za2_za2z00zztype_cachez00)),
																			BUNSPEC);
																		((((BgL_nodezf2effectzf2_bglt)
																					COBJECT(((BgL_nodezf2effectzf2_bglt)
																							BgL_new1114z00_1819)))->
																				BgL_sidezd2effectzd2) =
																			((obj_t) BUNSPEC), BUNSPEC);
																		((((BgL_nodezf2effectzf2_bglt)
																					COBJECT(((BgL_nodezf2effectzf2_bglt)
																							BgL_new1114z00_1819)))->
																				BgL_keyz00) =
																			((obj_t) BINT(-1L)), BUNSPEC);
																		((((BgL_letzd2varzd2_bglt)
																					COBJECT(BgL_new1114z00_1819))->
																				BgL_bindingsz00) =
																			((obj_t) BgL_bindingsz00_1816), BUNSPEC);
																		((((BgL_letzd2varzd2_bglt)
																					COBJECT(BgL_new1114z00_1819))->
																				BgL_bodyz00) =
																			((BgL_nodez00_bglt) BgL_bodyz00_1814),
																			BUNSPEC);
																		((((BgL_letzd2varzd2_bglt)
																					COBJECT(BgL_new1114z00_1819))->
																				BgL_removablezf3zf3) =
																			((bool_t) ((bool_t) 1)), BUNSPEC);
																		BgL_nodez00_1818 = BgL_new1114z00_1819;
																	}
																	{	/* Ast/let.scm 165 */

																		BGl_occurzd2nodez12zc0zzast_occurz00(
																			((BgL_nodez00_bglt) BgL_nodez00_1818));
																		BGl_nodezd2removez12zc0zzast_removez00(
																			((BgL_nodez00_bglt) BgL_nodez00_1818));
																		return BgL_nodez00_1818;
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* make-smart-generic-let */
	BgL_nodezf2effectzf2_bglt
		BGl_makezd2smartzd2genericzd2letzd2zzast_letz00(obj_t
		BgL_letzf2letreczf2_26, BgL_letzd2varzd2_bglt BgL_nodezd2letzd2_27,
		obj_t BgL_sitez00_28)
	{
		{	/* Ast/let.scm 186 */
			{	/* Ast/let.scm 189 */
				obj_t BgL_g1115z00_1884;

				BgL_g1115z00_1884 =
					(((BgL_letzd2varzd2_bglt) COBJECT(BgL_nodezd2letzd2_27))->
					BgL_bindingsz00);
				{
					obj_t BgL_bindingsz00_1888;
					obj_t BgL_funz00_1889;
					obj_t BgL_valuez00_1890;

					BgL_bindingsz00_1888 = BgL_g1115z00_1884;
					BgL_funz00_1889 = BNIL;
					BgL_valuez00_1890 = BNIL;
				BgL_zc3z04anonymousza31776ze3z87_1891:
					if (NULLP(BgL_bindingsz00_1888))
						{	/* Ast/let.scm 192 */
							if (NULLP(BgL_funz00_1889))
								{	/* Ast/let.scm 200 */
									obj_t BgL_varsz00_1894;

									{	/* Ast/let.scm 200 */
										obj_t BgL_l1368z00_1895;

										BgL_l1368z00_1895 =
											(((BgL_letzd2varzd2_bglt) COBJECT(BgL_nodezd2letzd2_27))->
											BgL_bindingsz00);
										if (NULLP(BgL_l1368z00_1895))
											{	/* Ast/let.scm 200 */
												BgL_varsz00_1894 = BNIL;
											}
										else
											{	/* Ast/let.scm 200 */
												obj_t BgL_head1370z00_1897;

												{	/* Ast/let.scm 200 */
													obj_t BgL_arg1806z00_1909;

													{	/* Ast/let.scm 200 */
														obj_t BgL_pairz00_4885;

														BgL_pairz00_4885 = CAR(((obj_t) BgL_l1368z00_1895));
														BgL_arg1806z00_1909 = CAR(BgL_pairz00_4885);
													}
													BgL_head1370z00_1897 =
														MAKE_YOUNG_PAIR(BgL_arg1806z00_1909, BNIL);
												}
												{	/* Ast/let.scm 200 */
													obj_t BgL_g1373z00_1898;

													BgL_g1373z00_1898 = CDR(((obj_t) BgL_l1368z00_1895));
													{
														obj_t BgL_l1368z00_1900;
														obj_t BgL_tail1371z00_1901;

														BgL_l1368z00_1900 = BgL_g1373z00_1898;
														BgL_tail1371z00_1901 = BgL_head1370z00_1897;
													BgL_zc3z04anonymousza31780ze3z87_1902:
														if (NULLP(BgL_l1368z00_1900))
															{	/* Ast/let.scm 200 */
																BgL_varsz00_1894 = BgL_head1370z00_1897;
															}
														else
															{	/* Ast/let.scm 200 */
																obj_t BgL_newtail1372z00_1904;

																{	/* Ast/let.scm 200 */
																	obj_t BgL_arg1799z00_1906;

																	{	/* Ast/let.scm 200 */
																		obj_t BgL_pairz00_4888;

																		BgL_pairz00_4888 =
																			CAR(((obj_t) BgL_l1368z00_1900));
																		BgL_arg1799z00_1906 = CAR(BgL_pairz00_4888);
																	}
																	BgL_newtail1372z00_1904 =
																		MAKE_YOUNG_PAIR(BgL_arg1799z00_1906, BNIL);
																}
																SET_CDR(BgL_tail1371z00_1901,
																	BgL_newtail1372z00_1904);
																{	/* Ast/let.scm 200 */
																	obj_t BgL_arg1798z00_1905;

																	BgL_arg1798z00_1905 =
																		CDR(((obj_t) BgL_l1368z00_1900));
																	{
																		obj_t BgL_tail1371z00_8584;
																		obj_t BgL_l1368z00_8583;

																		BgL_l1368z00_8583 = BgL_arg1798z00_1905;
																		BgL_tail1371z00_8584 =
																			BgL_newtail1372z00_1904;
																		BgL_tail1371z00_1901 = BgL_tail1371z00_8584;
																		BgL_l1368z00_1900 = BgL_l1368z00_8583;
																		goto BgL_zc3z04anonymousza31780ze3z87_1902;
																	}
																}
															}
													}
												}
											}
									}
									return
										((BgL_nodezf2effectzf2_bglt)
										BGl_letzd2orzd2letrecz00zzast_letz00(BgL_letzf2letreczf2_26,
											BgL_nodezd2letzd2_27, BgL_varsz00_1894));
								}
							else
								{	/* Ast/let.scm 199 */
									if (NULLP(BgL_valuez00_1890))
										{	/* Ast/let.scm 203 */
											BgL_nodez00_bglt BgL_arg1812z00_1912;

											BgL_arg1812z00_1912 =
												(((BgL_letzd2varzd2_bglt)
													COBJECT(BgL_nodezd2letzd2_27))->BgL_bodyz00);
											return ((BgL_nodezf2effectzf2_bglt)
												BGl_letzd2ze3labelsz31zzast_letz00(BgL_funz00_1889,
													BgL_arg1812z00_1912, BgL_sitez00_28));
										}
									else
										{	/* Ast/let.scm 205 */
											obj_t BgL_varsz00_1913;

											{	/* Ast/let.scm 205 */
												obj_t BgL_l1374z00_1920;

												BgL_l1374z00_1920 =
													(((BgL_letzd2varzd2_bglt)
														COBJECT(BgL_nodezd2letzd2_27))->BgL_bindingsz00);
												if (NULLP(BgL_l1374z00_1920))
													{	/* Ast/let.scm 205 */
														BgL_varsz00_1913 = BNIL;
													}
												else
													{	/* Ast/let.scm 205 */
														obj_t BgL_head1376z00_1922;

														{	/* Ast/let.scm 205 */
															obj_t BgL_arg1834z00_1934;

															{	/* Ast/let.scm 205 */
																obj_t BgL_pairz00_4894;

																BgL_pairz00_4894 =
																	CAR(((obj_t) BgL_l1374z00_1920));
																BgL_arg1834z00_1934 = CAR(BgL_pairz00_4894);
															}
															BgL_head1376z00_1922 =
																MAKE_YOUNG_PAIR(BgL_arg1834z00_1934, BNIL);
														}
														{	/* Ast/let.scm 205 */
															obj_t BgL_g1379z00_1923;

															BgL_g1379z00_1923 =
																CDR(((obj_t) BgL_l1374z00_1920));
															{
																obj_t BgL_l1374z00_1925;
																obj_t BgL_tail1377z00_1926;

																BgL_l1374z00_1925 = BgL_g1379z00_1923;
																BgL_tail1377z00_1926 = BgL_head1376z00_1922;
															BgL_zc3z04anonymousza31825ze3z87_1927:
																if (NULLP(BgL_l1374z00_1925))
																	{	/* Ast/let.scm 205 */
																		BgL_varsz00_1913 = BgL_head1376z00_1922;
																	}
																else
																	{	/* Ast/let.scm 205 */
																		obj_t BgL_newtail1378z00_1929;

																		{	/* Ast/let.scm 205 */
																			obj_t BgL_arg1832z00_1931;

																			{	/* Ast/let.scm 205 */
																				obj_t BgL_pairz00_4897;

																				BgL_pairz00_4897 =
																					CAR(((obj_t) BgL_l1374z00_1925));
																				BgL_arg1832z00_1931 =
																					CAR(BgL_pairz00_4897);
																			}
																			BgL_newtail1378z00_1929 =
																				MAKE_YOUNG_PAIR(BgL_arg1832z00_1931,
																				BNIL);
																		}
																		SET_CDR(BgL_tail1377z00_1926,
																			BgL_newtail1378z00_1929);
																		{	/* Ast/let.scm 205 */
																			obj_t BgL_arg1831z00_1930;

																			BgL_arg1831z00_1930 =
																				CDR(((obj_t) BgL_l1374z00_1925));
																			{
																				obj_t BgL_tail1377z00_8611;
																				obj_t BgL_l1374z00_8610;

																				BgL_l1374z00_8610 = BgL_arg1831z00_1930;
																				BgL_tail1377z00_8611 =
																					BgL_newtail1378z00_1929;
																				BgL_tail1377z00_1926 =
																					BgL_tail1377z00_8611;
																				BgL_l1374z00_1925 = BgL_l1374z00_8610;
																				goto
																					BgL_zc3z04anonymousza31825ze3z87_1927;
																			}
																		}
																	}
															}
														}
													}
											}
											((((BgL_letzd2varzd2_bglt)
														COBJECT(BgL_nodezd2letzd2_27))->BgL_bindingsz00) =
												((obj_t) bgl_reverse_bang(BgL_valuez00_1890)), BUNSPEC);
											{	/* Ast/let.scm 210 */
												BgL_letzd2varzd2_bglt BgL_nletz00_1915;

												BgL_nletz00_1915 =
													BGl_letzd2orzd2letrecz00zzast_letz00
													(BgL_letzf2letreczf2_26, BgL_nodezd2letzd2_27,
													BgL_varsz00_1913);
												{	/* Ast/let.scm 210 */
													BgL_letzd2funzd2_bglt BgL_nbodyz00_1916;

													BgL_nbodyz00_1916 =
														BGl_letzd2ze3labelsz31zzast_letz00(BgL_funz00_1889,
														(((BgL_letzd2varzd2_bglt)
																COBJECT(BgL_nletz00_1915))->BgL_bodyz00),
														BgL_sitez00_28);
													{	/* Ast/let.scm 211 */

														((((BgL_letzd2varzd2_bglt)
																	COBJECT(BgL_nletz00_1915))->BgL_bodyz00) =
															((BgL_nodez00_bglt) ((BgL_nodez00_bglt)
																	BgL_nbodyz00_1916)), BUNSPEC);
														{
															BgL_typez00_bglt BgL_auxz00_8619;

															{	/* Ast/let.scm 214 */
																BgL_typez00_bglt BgL_arg1822z00_1918;

																BgL_arg1822z00_1918 =
																	(((BgL_nodez00_bglt) COBJECT(
																			((BgL_nodez00_bglt) BgL_nletz00_1915)))->
																	BgL_typez00);
																BgL_auxz00_8619 =
																	BGl_strictzd2nodezd2typez00zzast_nodez00((
																		(BgL_typez00_bglt)
																		BGl_za2_za2z00zztype_cachez00),
																	BgL_arg1822z00_1918);
															}
															((((BgL_nodez00_bglt) COBJECT(
																			((BgL_nodez00_bglt) BgL_nletz00_1915)))->
																	BgL_typez00) =
																((BgL_typez00_bglt) BgL_auxz00_8619), BUNSPEC);
														}
														return
															((BgL_nodezf2effectzf2_bglt) BgL_nletz00_1915);
													}
												}
											}
										}
								}
						}
					else
						{	/* Ast/let.scm 216 */
							obj_t BgL_bindingz00_1936;

							BgL_bindingz00_1936 = CAR(((obj_t) BgL_bindingsz00_1888));
							{	/* Ast/let.scm 216 */
								obj_t BgL_varz00_1937;

								BgL_varz00_1937 = CAR(((obj_t) BgL_bindingz00_1936));
								{	/* Ast/let.scm 217 */
									obj_t BgL_sexpz00_1938;

									BgL_sexpz00_1938 = CDR(((obj_t) BgL_bindingz00_1936));
									{	/* Ast/let.scm 218 */

										{	/* Ast/let.scm 219 */
											bool_t BgL_test3881z00_8633;

											{	/* Ast/let.scm 219 */
												obj_t BgL_classz00_4905;

												BgL_classz00_4905 = BGl_letzd2funzd2zzast_nodez00;
												if (BGL_OBJECTP(BgL_sexpz00_1938))
													{	/* Ast/let.scm 219 */
														BgL_objectz00_bglt BgL_arg1807z00_4907;

														BgL_arg1807z00_4907 =
															(BgL_objectz00_bglt) (BgL_sexpz00_1938);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Ast/let.scm 219 */
																long BgL_idxz00_4913;

																BgL_idxz00_4913 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_4907);
																BgL_test3881z00_8633 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_4913 + 3L)) ==
																	BgL_classz00_4905);
															}
														else
															{	/* Ast/let.scm 219 */
																bool_t BgL_res3743z00_4938;

																{	/* Ast/let.scm 219 */
																	obj_t BgL_oclassz00_4921;

																	{	/* Ast/let.scm 219 */
																		obj_t BgL_arg1815z00_4929;
																		long BgL_arg1816z00_4930;

																		BgL_arg1815z00_4929 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Ast/let.scm 219 */
																			long BgL_arg1817z00_4931;

																			BgL_arg1817z00_4931 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_4907);
																			BgL_arg1816z00_4930 =
																				(BgL_arg1817z00_4931 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_4921 =
																			VECTOR_REF(BgL_arg1815z00_4929,
																			BgL_arg1816z00_4930);
																	}
																	{	/* Ast/let.scm 219 */
																		bool_t BgL__ortest_1115z00_4922;

																		BgL__ortest_1115z00_4922 =
																			(BgL_classz00_4905 == BgL_oclassz00_4921);
																		if (BgL__ortest_1115z00_4922)
																			{	/* Ast/let.scm 219 */
																				BgL_res3743z00_4938 =
																					BgL__ortest_1115z00_4922;
																			}
																		else
																			{	/* Ast/let.scm 219 */
																				long BgL_odepthz00_4923;

																				{	/* Ast/let.scm 219 */
																					obj_t BgL_arg1804z00_4924;

																					BgL_arg1804z00_4924 =
																						(BgL_oclassz00_4921);
																					BgL_odepthz00_4923 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_4924);
																				}
																				if ((3L < BgL_odepthz00_4923))
																					{	/* Ast/let.scm 219 */
																						obj_t BgL_arg1802z00_4926;

																						{	/* Ast/let.scm 219 */
																							obj_t BgL_arg1803z00_4927;

																							BgL_arg1803z00_4927 =
																								(BgL_oclassz00_4921);
																							BgL_arg1802z00_4926 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_4927, 3L);
																						}
																						BgL_res3743z00_4938 =
																							(BgL_arg1802z00_4926 ==
																							BgL_classz00_4905);
																					}
																				else
																					{	/* Ast/let.scm 219 */
																						BgL_res3743z00_4938 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test3881z00_8633 = BgL_res3743z00_4938;
															}
													}
												else
													{	/* Ast/let.scm 219 */
														BgL_test3881z00_8633 = ((bool_t) 0);
													}
											}
											if (BgL_test3881z00_8633)
												{	/* Ast/let.scm 220 */
													obj_t BgL_localsz00_1940;

													BgL_localsz00_1940 =
														(((BgL_letzd2funzd2_bglt) COBJECT(
																((BgL_letzd2funzd2_bglt) BgL_sexpz00_1938)))->
														BgL_localsz00);
													{	/* Ast/let.scm 220 */
														BgL_nodez00_bglt BgL_bodyz00_1941;

														BgL_bodyz00_1941 =
															(((BgL_letzd2funzd2_bglt) COBJECT(
																	((BgL_letzd2funzd2_bglt) BgL_sexpz00_1938)))->
															BgL_bodyz00);
														{	/* Ast/let.scm 221 */

															{	/* Ast/let.scm 222 */
																bool_t BgL_test3886z00_8660;

																if (NULLP(BgL_localsz00_1940))
																	{	/* Ast/let.scm 222 */
																		BgL_test3886z00_8660 = ((bool_t) 1);
																	}
																else
																	{	/* Ast/let.scm 222 */
																		if (NULLP(CDR(
																					((obj_t) BgL_localsz00_1940))))
																			{	/* Ast/let.scm 222 */
																				BgL_test3886z00_8660 = ((bool_t) 0);
																			}
																		else
																			{	/* Ast/let.scm 222 */
																				BgL_test3886z00_8660 = ((bool_t) 1);
																			}
																	}
																if (BgL_test3886z00_8660)
																	{	/* Ast/let.scm 226 */
																		obj_t BgL_arg1842z00_1947;
																		obj_t BgL_arg1843z00_1948;

																		BgL_arg1842z00_1947 =
																			CDR(((obj_t) BgL_bindingsz00_1888));
																		{	/* Ast/let.scm 228 */
																			obj_t BgL_arg1844z00_1949;

																			BgL_arg1844z00_1949 =
																				CAR(((obj_t) BgL_bindingsz00_1888));
																			BgL_arg1843z00_1948 =
																				MAKE_YOUNG_PAIR(BgL_arg1844z00_1949,
																				BgL_valuez00_1890);
																		}
																		{
																			obj_t BgL_valuez00_8673;
																			obj_t BgL_bindingsz00_8672;

																			BgL_bindingsz00_8672 =
																				BgL_arg1842z00_1947;
																			BgL_valuez00_8673 = BgL_arg1843z00_1948;
																			BgL_valuez00_1890 = BgL_valuez00_8673;
																			BgL_bindingsz00_1888 =
																				BgL_bindingsz00_8672;
																			goto
																				BgL_zc3z04anonymousza31776ze3z87_1891;
																		}
																	}
																else
																	{	/* Ast/let.scm 229 */
																		bool_t BgL_test3889z00_8674;

																		{	/* Ast/let.scm 229 */
																			obj_t BgL_classz00_4944;

																			BgL_classz00_4944 =
																				BGl_varz00zzast_nodez00;
																			{	/* Ast/let.scm 229 */
																				BgL_objectz00_bglt BgL_arg1807z00_4946;

																				{	/* Ast/let.scm 229 */
																					obj_t BgL_tmpz00_8675;

																					BgL_tmpz00_8675 =
																						((obj_t)
																						((BgL_objectz00_bglt)
																							BgL_bodyz00_1941));
																					BgL_arg1807z00_4946 =
																						(BgL_objectz00_bglt)
																						(BgL_tmpz00_8675);
																				}
																				if (BGL_CONDEXPAND_ISA_ARCH64())
																					{	/* Ast/let.scm 229 */
																						long BgL_idxz00_4952;

																						BgL_idxz00_4952 =
																							BGL_OBJECT_INHERITANCE_NUM
																							(BgL_arg1807z00_4946);
																						BgL_test3889z00_8674 =
																							(VECTOR_REF
																							(BGl_za2inheritancesza2z00zz__objectz00,
																								(BgL_idxz00_4952 + 2L)) ==
																							BgL_classz00_4944);
																					}
																				else
																					{	/* Ast/let.scm 229 */
																						bool_t BgL_res3744z00_4977;

																						{	/* Ast/let.scm 229 */
																							obj_t BgL_oclassz00_4960;

																							{	/* Ast/let.scm 229 */
																								obj_t BgL_arg1815z00_4968;
																								long BgL_arg1816z00_4969;

																								BgL_arg1815z00_4968 =
																									(BGl_za2classesza2z00zz__objectz00);
																								{	/* Ast/let.scm 229 */
																									long BgL_arg1817z00_4970;

																									BgL_arg1817z00_4970 =
																										BGL_OBJECT_CLASS_NUM
																										(BgL_arg1807z00_4946);
																									BgL_arg1816z00_4969 =
																										(BgL_arg1817z00_4970 -
																										OBJECT_TYPE);
																								}
																								BgL_oclassz00_4960 =
																									VECTOR_REF
																									(BgL_arg1815z00_4968,
																									BgL_arg1816z00_4969);
																							}
																							{	/* Ast/let.scm 229 */
																								bool_t BgL__ortest_1115z00_4961;

																								BgL__ortest_1115z00_4961 =
																									(BgL_classz00_4944 ==
																									BgL_oclassz00_4960);
																								if (BgL__ortest_1115z00_4961)
																									{	/* Ast/let.scm 229 */
																										BgL_res3744z00_4977 =
																											BgL__ortest_1115z00_4961;
																									}
																								else
																									{	/* Ast/let.scm 229 */
																										long BgL_odepthz00_4962;

																										{	/* Ast/let.scm 229 */
																											obj_t BgL_arg1804z00_4963;

																											BgL_arg1804z00_4963 =
																												(BgL_oclassz00_4960);
																											BgL_odepthz00_4962 =
																												BGL_CLASS_DEPTH
																												(BgL_arg1804z00_4963);
																										}
																										if (
																											(2L < BgL_odepthz00_4962))
																											{	/* Ast/let.scm 229 */
																												obj_t
																													BgL_arg1802z00_4965;
																												{	/* Ast/let.scm 229 */
																													obj_t
																														BgL_arg1803z00_4966;
																													BgL_arg1803z00_4966 =
																														(BgL_oclassz00_4960);
																													BgL_arg1802z00_4965 =
																														BGL_CLASS_ANCESTORS_REF
																														(BgL_arg1803z00_4966,
																														2L);
																												}
																												BgL_res3744z00_4977 =
																													(BgL_arg1802z00_4965
																													== BgL_classz00_4944);
																											}
																										else
																											{	/* Ast/let.scm 229 */
																												BgL_res3744z00_4977 =
																													((bool_t) 0);
																											}
																									}
																							}
																						}
																						BgL_test3889z00_8674 =
																							BgL_res3744z00_4977;
																					}
																			}
																		}
																		if (BgL_test3889z00_8674)
																			{	/* Ast/let.scm 230 */
																				BgL_variablez00_bglt BgL_resz00_1951;
																				obj_t BgL_auxz00_1952;

																				BgL_resz00_1951 =
																					(((BgL_varz00_bglt) COBJECT(
																							((BgL_varz00_bglt)
																								BgL_bodyz00_1941)))->
																					BgL_variablez00);
																				BgL_auxz00_1952 =
																					CAR(((obj_t) BgL_localsz00_1940));
																				{	/* Ast/let.scm 232 */
																					bool_t BgL_test3893z00_8702;

																					if (
																						(((obj_t) BgL_resz00_1951) ==
																							BgL_auxz00_1952))
																						{	/* Ast/let.scm 232 */
																							if (
																								((((BgL_variablez00_bglt)
																											COBJECT((
																													(BgL_variablez00_bglt)
																													((BgL_localz00_bglt)
																														BgL_varz00_1937))))->
																										BgL_accessz00) ==
																									CNST_TABLE_REF(3)))
																								{	/* Ast/let.scm 236 */
																									BgL_test3893z00_8702 =
																										((bool_t) 1);
																								}
																							else
																								{	/* Ast/let.scm 237 */
																									bool_t BgL_test3896z00_8712;

																									{	/* Ast/let.scm 238 */
																										bool_t BgL_test3897z00_8713;

																										{	/* Ast/let.scm 238 */
																											BgL_typez00_bglt
																												BgL_arg1873z00_1982;
																											BgL_arg1873z00_1982 =
																												(((BgL_variablez00_bglt)
																													COBJECT((
																															(BgL_variablez00_bglt)
																															((BgL_localz00_bglt) BgL_varz00_1937))))->BgL_typez00);
																											BgL_test3897z00_8713 =
																												(((obj_t)
																													BgL_arg1873z00_1982)
																												==
																												BGl_za2procedureza2z00zztype_cachez00);
																										}
																										if (BgL_test3897z00_8713)
																											{	/* Ast/let.scm 238 */
																												BgL_test3896z00_8712 =
																													((bool_t) 1);
																											}
																										else
																											{	/* Ast/let.scm 239 */
																												bool_t
																													BgL_test3898z00_8719;
																												{	/* Ast/let.scm 239 */
																													BgL_typez00_bglt
																														BgL_arg1872z00_1981;
																													BgL_arg1872z00_1981 =
																														(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt) BgL_varz00_1937))))->BgL_typez00);
																													BgL_test3898z00_8719 =
																														(((obj_t)
																															BgL_arg1872z00_1981)
																														==
																														BGl_za2_za2z00zztype_cachez00);
																												}
																												if (BgL_test3898z00_8719)
																													{	/* Ast/let.scm 239 */
																														BgL_test3896z00_8712
																															= ((bool_t) 1);
																													}
																												else
																													{	/* Ast/let.scm 240 */
																														BgL_typez00_bglt
																															BgL_arg1870z00_1980;
																														BgL_arg1870z00_1980
																															=
																															(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt) BgL_varz00_1937))))->BgL_typez00);
																														BgL_test3896z00_8712
																															=
																															(((obj_t)
																																BgL_arg1870z00_1980)
																															==
																															BGl_za2objza2z00zztype_cachez00);
																													}
																											}
																									}
																									if (BgL_test3896z00_8712)
																										{	/* Ast/let.scm 237 */
																											BgL_test3893z00_8702 =
																												((bool_t) 0);
																										}
																									else
																										{	/* Ast/let.scm 237 */
																											BgL_test3893z00_8702 =
																												((bool_t) 1);
																										}
																								}
																						}
																					else
																						{	/* Ast/let.scm 232 */
																							BgL_test3893z00_8702 =
																								((bool_t) 1);
																						}
																					if (BgL_test3893z00_8702)
																						{	/* Ast/let.scm 242 */
																							obj_t BgL_arg1862z00_1970;
																							obj_t BgL_arg1863z00_1971;

																							BgL_arg1862z00_1970 =
																								CDR(
																								((obj_t) BgL_bindingsz00_1888));
																							{	/* Ast/let.scm 244 */
																								obj_t BgL_arg1864z00_1972;

																								BgL_arg1864z00_1972 =
																									CAR(
																									((obj_t)
																										BgL_bindingsz00_1888));
																								BgL_arg1863z00_1971 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1864z00_1972,
																									BgL_valuez00_1890);
																							}
																							{
																								obj_t BgL_valuez00_8736;
																								obj_t BgL_bindingsz00_8735;

																								BgL_bindingsz00_8735 =
																									BgL_arg1862z00_1970;
																								BgL_valuez00_8736 =
																									BgL_arg1863z00_1971;
																								BgL_valuez00_1890 =
																									BgL_valuez00_8736;
																								BgL_bindingsz00_1888 =
																									BgL_bindingsz00_8735;
																								goto
																									BgL_zc3z04anonymousza31776ze3z87_1891;
																							}
																						}
																					else
																						{	/* Ast/let.scm 246 */
																							obj_t BgL_arg1866z00_1973;
																							obj_t BgL_arg1868z00_1974;

																							BgL_arg1866z00_1973 =
																								CDR(
																								((obj_t) BgL_bindingsz00_1888));
																							{	/* Ast/let.scm 247 */
																								obj_t BgL_arg1869z00_1975;

																								BgL_arg1869z00_1975 =
																									CAR(
																									((obj_t)
																										BgL_bindingsz00_1888));
																								BgL_arg1868z00_1974 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1869z00_1975,
																									BgL_funz00_1889);
																							}
																							{
																								obj_t BgL_funz00_8743;
																								obj_t BgL_bindingsz00_8742;

																								BgL_bindingsz00_8742 =
																									BgL_arg1866z00_1973;
																								BgL_funz00_8743 =
																									BgL_arg1868z00_1974;
																								BgL_funz00_1889 =
																									BgL_funz00_8743;
																								BgL_bindingsz00_1888 =
																									BgL_bindingsz00_8742;
																								goto
																									BgL_zc3z04anonymousza31776ze3z87_1891;
																							}
																						}
																				}
																			}
																		else
																			{	/* Ast/let.scm 249 */
																				obj_t BgL_arg1875z00_1984;
																				obj_t BgL_arg1876z00_1985;

																				BgL_arg1875z00_1984 =
																					CDR(((obj_t) BgL_bindingsz00_1888));
																				{	/* Ast/let.scm 251 */
																					obj_t BgL_arg1877z00_1986;

																					BgL_arg1877z00_1986 =
																						CAR(((obj_t) BgL_bindingsz00_1888));
																					BgL_arg1876z00_1985 =
																						MAKE_YOUNG_PAIR(BgL_arg1877z00_1986,
																						BgL_valuez00_1890);
																				}
																				{
																					obj_t BgL_valuez00_8750;
																					obj_t BgL_bindingsz00_8749;

																					BgL_bindingsz00_8749 =
																						BgL_arg1875z00_1984;
																					BgL_valuez00_8750 =
																						BgL_arg1876z00_1985;
																					BgL_valuez00_1890 = BgL_valuez00_8750;
																					BgL_bindingsz00_1888 =
																						BgL_bindingsz00_8749;
																					goto
																						BgL_zc3z04anonymousza31776ze3z87_1891;
																				}
																			}
																	}
															}
														}
													}
												}
											else
												{	/* Ast/let.scm 252 */
													obj_t BgL_arg1879z00_1990;
													obj_t BgL_arg1880z00_1991;

													BgL_arg1879z00_1990 =
														CDR(((obj_t) BgL_bindingsz00_1888));
													{	/* Ast/let.scm 254 */
														obj_t BgL_arg1882z00_1992;

														BgL_arg1882z00_1992 =
															CAR(((obj_t) BgL_bindingsz00_1888));
														BgL_arg1880z00_1991 =
															MAKE_YOUNG_PAIR(BgL_arg1882z00_1992,
															BgL_valuez00_1890);
													}
													{
														obj_t BgL_valuez00_8757;
														obj_t BgL_bindingsz00_8756;

														BgL_bindingsz00_8756 = BgL_arg1879z00_1990;
														BgL_valuez00_8757 = BgL_arg1880z00_1991;
														BgL_valuez00_1890 = BgL_valuez00_8757;
														BgL_bindingsz00_1888 = BgL_bindingsz00_8756;
														goto BgL_zc3z04anonymousza31776ze3z87_1891;
													}
												}
										}
									}
								}
							}
						}
				}
			}
		}

	}



/* let-or-letrec */
	BgL_letzd2varzd2_bglt BGl_letzd2orzd2letrecz00zzast_letz00(obj_t
		BgL_letzf2letreczf2_29, BgL_letzd2varzd2_bglt BgL_nodezd2letzd2_30,
		obj_t BgL_varsz00_31)
	{
		{	/* Ast/let.scm 266 */
			{
				BgL_letzd2varzd2_bglt BgL_nodez00_2243;

				{	/* Ast/let.scm 375 */
					bool_t BgL_test3899z00_8758;

					if ((BgL_letzf2letreczf2_29 == CNST_TABLE_REF(0)))
						{	/* Ast/let.scm 375 */
							BgL_test3899z00_8758 = ((bool_t) 1);
						}
					else
						{	/* Ast/let.scm 375 */
							BgL_test3899z00_8758 =
								(BgL_letzf2letreczf2_29 == BGl_za2letza2z00zzast_letz00);
						}
					if (BgL_test3899z00_8758)
						{	/* Ast/let.scm 375 */
							return BgL_nodezd2letzd2_30;
						}
					else
						{	/* Ast/let.scm 377 */
							bool_t BgL_test3901z00_8763;

							BgL_nodez00_2243 = BgL_nodezd2letzd2_30;
							{	/* Ast/let.scm 359 */
								obj_t BgL_g1405z00_2246;

								BgL_g1405z00_2246 =
									(((BgL_letzd2varzd2_bglt) COBJECT(BgL_nodez00_2243))->
									BgL_bindingsz00);
								{
									obj_t BgL_l1403z00_2248;

									BgL_l1403z00_2248 = BgL_g1405z00_2246;
								BgL_zc3z04anonymousza32005ze3z87_2249:
									if (NULLP(BgL_l1403z00_2248))
										{	/* Ast/let.scm 359 */
											BgL_test3901z00_8763 = ((bool_t) 1);
										}
									else
										{	/* Ast/let.scm 359 */
											bool_t BgL_test3903z00_8767;

											{	/* Ast/let.scm 359 */
												obj_t BgL_bz00_2254;

												BgL_bz00_2254 = CAR(((obj_t) BgL_l1403z00_2248));
												{	/* Ast/let.scm 359 */
													obj_t BgL_arg2008z00_2255;

													BgL_arg2008z00_2255 = CDR(((obj_t) BgL_bz00_2254));
													BgL_test3903z00_8767 =
														BGl_safezd2reczd2valzf3ze70z14zzast_letz00
														(BgL_arg2008z00_2255);
												}
											}
											if (BgL_test3903z00_8767)
												{	/* Ast/let.scm 359 */
													obj_t BgL_arg2007z00_2253;

													BgL_arg2007z00_2253 =
														CDR(((obj_t) BgL_l1403z00_2248));
													{
														obj_t BgL_l1403z00_8775;

														BgL_l1403z00_8775 = BgL_arg2007z00_2253;
														BgL_l1403z00_2248 = BgL_l1403z00_8775;
														goto BgL_zc3z04anonymousza32005ze3z87_2249;
													}
												}
											else
												{	/* Ast/let.scm 359 */
													BgL_test3901z00_8763 = ((bool_t) 0);
												}
										}
								}
							}
							if (BgL_test3901z00_8763)
								{	/* Ast/let.scm 377 */
									return BgL_nodezd2letzd2_30;
								}
							else
								{	/* Ast/let.scm 379 */
									bool_t BgL_test3904z00_8776;

									if (((long) CINT(BGl_za2optimza2z00zzengine_paramz00) >= 1L))
										{	/* Ast/let.scm 379 */
											if (CBOOL(BGl_za2callzf2cczf3za2z01zzengine_paramz00))
												{	/* Ast/let.scm 379 */
													BgL_test3904z00_8776 = ((bool_t) 0);
												}
											else
												{	/* Ast/let.scm 379 */
													BgL_test3904z00_8776 =
														BGl_safezd2letzd2optimzf3ze70z14zzast_letz00
														(BgL_varsz00_31, BgL_nodezd2letzd2_30);
												}
										}
									else
										{	/* Ast/let.scm 379 */
											BgL_test3904z00_8776 = ((bool_t) 0);
										}
									if (BgL_test3904z00_8776)
										{	/* Ast/let.scm 379 */
											return BgL_nodezd2letzd2_30;
										}
									else
										{	/* Ast/let.scm 379 */
											if ((BgL_letzf2letreczf2_29 == CNST_TABLE_REF(4)))
												{	/* Ast/let.scm 382 */
													obj_t BgL_bindingsz00_2002;

													BgL_bindingsz00_2002 =
														(((BgL_letzd2varzd2_bglt)
															COBJECT(BgL_nodezd2letzd2_30))->BgL_bindingsz00);
													{	/* Ast/let.scm 382 */
														BgL_nodez00_bglt BgL_bodyz00_2003;

														BgL_bodyz00_2003 =
															(((BgL_letzd2varzd2_bglt)
																COBJECT(BgL_nodezd2letzd2_30))->BgL_bodyz00);
														{	/* Ast/let.scm 383 */
															BgL_nodez00_bglt BgL_seqz00_2004;

															{	/* Ast/let.scm 384 */
																bool_t BgL_test3908z00_8788;

																{	/* Ast/let.scm 384 */
																	obj_t BgL_classz00_5775;

																	BgL_classz00_5775 =
																		BGl_sequencez00zzast_nodez00;
																	{	/* Ast/let.scm 384 */
																		BgL_objectz00_bglt BgL_arg1807z00_5777;

																		{	/* Ast/let.scm 384 */
																			obj_t BgL_tmpz00_8789;

																			BgL_tmpz00_8789 =
																				((obj_t)
																				((BgL_objectz00_bglt)
																					BgL_bodyz00_2003));
																			BgL_arg1807z00_5777 =
																				(BgL_objectz00_bglt) (BgL_tmpz00_8789);
																		}
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Ast/let.scm 384 */
																				long BgL_idxz00_5783;

																				BgL_idxz00_5783 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_5777);
																				BgL_test3908z00_8788 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_5783 + 3L)) ==
																					BgL_classz00_5775);
																			}
																		else
																			{	/* Ast/let.scm 384 */
																				bool_t BgL_res3767z00_5808;

																				{	/* Ast/let.scm 384 */
																					obj_t BgL_oclassz00_5791;

																					{	/* Ast/let.scm 384 */
																						obj_t BgL_arg1815z00_5799;
																						long BgL_arg1816z00_5800;

																						BgL_arg1815z00_5799 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Ast/let.scm 384 */
																							long BgL_arg1817z00_5801;

																							BgL_arg1817z00_5801 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_5777);
																							BgL_arg1816z00_5800 =
																								(BgL_arg1817z00_5801 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_5791 =
																							VECTOR_REF(BgL_arg1815z00_5799,
																							BgL_arg1816z00_5800);
																					}
																					{	/* Ast/let.scm 384 */
																						bool_t BgL__ortest_1115z00_5792;

																						BgL__ortest_1115z00_5792 =
																							(BgL_classz00_5775 ==
																							BgL_oclassz00_5791);
																						if (BgL__ortest_1115z00_5792)
																							{	/* Ast/let.scm 384 */
																								BgL_res3767z00_5808 =
																									BgL__ortest_1115z00_5792;
																							}
																						else
																							{	/* Ast/let.scm 384 */
																								long BgL_odepthz00_5793;

																								{	/* Ast/let.scm 384 */
																									obj_t BgL_arg1804z00_5794;

																									BgL_arg1804z00_5794 =
																										(BgL_oclassz00_5791);
																									BgL_odepthz00_5793 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_5794);
																								}
																								if ((3L < BgL_odepthz00_5793))
																									{	/* Ast/let.scm 384 */
																										obj_t BgL_arg1802z00_5796;

																										{	/* Ast/let.scm 384 */
																											obj_t BgL_arg1803z00_5797;

																											BgL_arg1803z00_5797 =
																												(BgL_oclassz00_5791);
																											BgL_arg1802z00_5796 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_5797,
																												3L);
																										}
																										BgL_res3767z00_5808 =
																											(BgL_arg1802z00_5796 ==
																											BgL_classz00_5775);
																									}
																								else
																									{	/* Ast/let.scm 384 */
																										BgL_res3767z00_5808 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test3908z00_8788 =
																					BgL_res3767z00_5808;
																			}
																	}
																}
																if (BgL_test3908z00_8788)
																	{	/* Ast/let.scm 384 */
																		BgL_seqz00_2004 = BgL_bodyz00_2003;
																	}
																else
																	{	/* Ast/let.scm 386 */
																		BgL_sequencez00_bglt BgL_new1157z00_2033;

																		{	/* Ast/let.scm 387 */
																			BgL_sequencez00_bglt BgL_new1156z00_2035;

																			BgL_new1156z00_2035 =
																				((BgL_sequencez00_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_sequencez00_bgl))));
																			{	/* Ast/let.scm 387 */
																				long BgL_arg1901z00_2036;

																				BgL_arg1901z00_2036 =
																					BGL_CLASS_NUM
																					(BGl_sequencez00zzast_nodez00);
																				BGL_OBJECT_CLASS_NUM_SET((
																						(BgL_objectz00_bglt)
																						BgL_new1156z00_2035),
																					BgL_arg1901z00_2036);
																			}
																			{	/* Ast/let.scm 387 */
																				BgL_objectz00_bglt BgL_tmpz00_8816;

																				BgL_tmpz00_8816 =
																					((BgL_objectz00_bglt)
																					BgL_new1156z00_2035);
																				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8816,
																					BFALSE);
																			}
																			((BgL_objectz00_bglt)
																				BgL_new1156z00_2035);
																			BgL_new1157z00_2033 = BgL_new1156z00_2035;
																		}
																		((((BgL_nodez00_bglt) COBJECT(
																						((BgL_nodez00_bglt)
																							BgL_new1157z00_2033)))->
																				BgL_locz00) =
																			((obj_t) (((BgL_nodez00_bglt)
																						COBJECT(BgL_bodyz00_2003))->
																					BgL_locz00)), BUNSPEC);
																		((((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt)
																							BgL_new1157z00_2033)))->
																				BgL_typez00) =
																			((BgL_typez00_bglt) ((BgL_typez00_bglt)
																					BGl_za2_za2z00zztype_cachez00)),
																			BUNSPEC);
																		((((BgL_nodezf2effectzf2_bglt)
																					COBJECT(((BgL_nodezf2effectzf2_bglt)
																							BgL_new1157z00_2033)))->
																				BgL_sidezd2effectzd2) =
																			((obj_t) BUNSPEC), BUNSPEC);
																		((((BgL_nodezf2effectzf2_bglt)
																					COBJECT(((BgL_nodezf2effectzf2_bglt)
																							BgL_new1157z00_2033)))->
																				BgL_keyz00) =
																			((obj_t) BINT(-1L)), BUNSPEC);
																		{
																			obj_t BgL_auxz00_8831;

																			{	/* Ast/let.scm 389 */
																				obj_t BgL_list1900z00_2034;

																				BgL_list1900z00_2034 =
																					MAKE_YOUNG_PAIR(
																					((obj_t) BgL_bodyz00_2003), BNIL);
																				BgL_auxz00_8831 = BgL_list1900z00_2034;
																			}
																			((((BgL_sequencez00_bglt)
																						COBJECT(BgL_new1157z00_2033))->
																					BgL_nodesz00) =
																				((obj_t) BgL_auxz00_8831), BUNSPEC);
																		}
																		((((BgL_sequencez00_bglt)
																					COBJECT(BgL_new1157z00_2033))->
																				BgL_unsafez00) =
																			((bool_t) ((bool_t) 0)), BUNSPEC);
																		((((BgL_sequencez00_bglt)
																					COBJECT(BgL_new1157z00_2033))->
																				BgL_metaz00) = ((obj_t) BNIL), BUNSPEC);
																		BgL_seqz00_2004 =
																			((BgL_nodez00_bglt) BgL_new1157z00_2033);
															}}
															{	/* Ast/let.scm 384 */

																((((BgL_letzd2varzd2_bglt)
																			COBJECT(BgL_nodezd2letzd2_30))->
																		BgL_bodyz00) =
																	((BgL_nodez00_bglt) BgL_seqz00_2004),
																	BUNSPEC);
																{
																	obj_t BgL_bindingsz00_2007;
																	obj_t BgL_nsequencez00_2008;

																	BgL_bindingsz00_2007 = BgL_bindingsz00_2002;
																	BgL_nsequencez00_2008 = BNIL;
																BgL_zc3z04anonymousza31887ze3z87_2009:
																	if (NULLP(BgL_bindingsz00_2007))
																		{	/* Ast/let.scm 393 */
																			{	/* Ast/let.scm 397 */
																				BgL_sequencez00_bglt
																					BgL_arg1889z00_2011;
																				{	/* Ast/let.scm 397 */
																					BgL_sequencez00_bglt
																						BgL_new1161z00_2012;
																					{	/* Ast/let.scm 398 */
																						BgL_sequencez00_bglt
																							BgL_new1160z00_2015;
																						BgL_new1160z00_2015 =
																							((BgL_sequencez00_bglt)
																							BOBJECT(GC_MALLOC(sizeof(struct
																										BgL_sequencez00_bgl))));
																						{	/* Ast/let.scm 398 */
																							long BgL_arg1892z00_2016;

																							BgL_arg1892z00_2016 =
																								BGL_CLASS_NUM
																								(BGl_sequencez00zzast_nodez00);
																							BGL_OBJECT_CLASS_NUM_SET((
																									(BgL_objectz00_bglt)
																									BgL_new1160z00_2015),
																								BgL_arg1892z00_2016);
																						}
																						{	/* Ast/let.scm 398 */
																							BgL_objectz00_bglt
																								BgL_tmpz00_8845;
																							BgL_tmpz00_8845 =
																								((BgL_objectz00_bglt)
																								BgL_new1160z00_2015);
																							BGL_OBJECT_WIDENING_SET
																								(BgL_tmpz00_8845, BFALSE);
																						}
																						((BgL_objectz00_bglt)
																							BgL_new1160z00_2015);
																						BgL_new1161z00_2012 =
																							BgL_new1160z00_2015;
																					}
																					((((BgL_nodez00_bglt) COBJECT(
																									((BgL_nodez00_bglt)
																										BgL_new1161z00_2012)))->
																							BgL_locz00) =
																						((obj_t) (((BgL_nodez00_bglt)
																									COBJECT(BgL_seqz00_2004))->
																								BgL_locz00)), BUNSPEC);
																					((((BgL_nodez00_bglt)
																								COBJECT(((BgL_nodez00_bglt)
																										BgL_new1161z00_2012)))->
																							BgL_typez00) =
																						((BgL_typez00_bglt) (
																								(BgL_typez00_bglt)
																								BGl_za2_za2z00zztype_cachez00)),
																						BUNSPEC);
																					((((BgL_nodezf2effectzf2_bglt)
																								COBJECT((
																										(BgL_nodezf2effectzf2_bglt)
																										BgL_new1161z00_2012)))->
																							BgL_sidezd2effectzd2) =
																						((obj_t) BUNSPEC), BUNSPEC);
																					((((BgL_nodezf2effectzf2_bglt)
																								COBJECT((
																										(BgL_nodezf2effectzf2_bglt)
																										BgL_new1161z00_2012)))->
																							BgL_keyz00) =
																						((obj_t) BINT(-1L)), BUNSPEC);
																					{
																						obj_t BgL_auxz00_8860;

																						{	/* Ast/let.scm 400 */
																							obj_t BgL_arg1890z00_2013;
																							obj_t BgL_arg1891z00_2014;

																							BgL_arg1890z00_2013 =
																								bgl_reverse_bang
																								(BgL_nsequencez00_2008);
																							BgL_arg1891z00_2014 =
																								(((BgL_sequencez00_bglt)
																									COBJECT((
																											(BgL_sequencez00_bglt)
																											BgL_seqz00_2004)))->
																								BgL_nodesz00);
																							BgL_auxz00_8860 =
																								BGl_appendzd221011zd2zzast_letz00
																								(BgL_arg1890z00_2013,
																								BgL_arg1891z00_2014);
																						}
																						((((BgL_sequencez00_bglt)
																									COBJECT
																									(BgL_new1161z00_2012))->
																								BgL_nodesz00) =
																							((obj_t) BgL_auxz00_8860),
																							BUNSPEC);
																					}
																					((((BgL_sequencez00_bglt)
																								COBJECT(BgL_new1161z00_2012))->
																							BgL_unsafez00) =
																						((bool_t) ((bool_t) 0)), BUNSPEC);
																					((((BgL_sequencez00_bglt)
																								COBJECT(BgL_new1161z00_2012))->
																							BgL_metaz00) =
																						((obj_t) BNIL), BUNSPEC);
																					BgL_arg1889z00_2011 =
																						BgL_new1161z00_2012;
																				}
																				((((BgL_letzd2varzd2_bglt)
																							COBJECT(BgL_nodezd2letzd2_30))->
																						BgL_bodyz00) =
																					((BgL_nodez00_bglt) (
																							(BgL_nodez00_bglt)
																							BgL_arg1889z00_2011)), BUNSPEC);
																			}
																			return BgL_nodezd2letzd2_30;
																		}
																	else
																		{	/* Ast/let.scm 403 */
																			obj_t BgL_bindingz00_2017;

																			BgL_bindingz00_2017 =
																				CAR(((obj_t) BgL_bindingsz00_2007));
																			{	/* Ast/let.scm 403 */
																				obj_t BgL_varz00_2018;

																				BgL_varz00_2018 =
																					CAR(((obj_t) BgL_bindingz00_2017));
																				{	/* Ast/let.scm 404 */
																					obj_t BgL_valz00_2019;

																					BgL_valz00_2019 =
																						CDR(((obj_t) BgL_bindingz00_2017));
																					{	/* Ast/let.scm 405 */
																						obj_t BgL_locz00_2020;

																						BgL_locz00_2020 =
																							(((BgL_nodez00_bglt) COBJECT(
																									((BgL_nodez00_bglt)
																										BgL_valz00_2019)))->
																							BgL_locz00);
																						{	/* Ast/let.scm 406 */

																							{	/* Ast/let.scm 407 */
																								BgL_setqz00_bglt
																									BgL_initz00_2021;
																								{	/* Ast/let.scm 407 */
																									BgL_setqz00_bglt
																										BgL_new1163z00_2025;
																									{	/* Ast/let.scm 408 */
																										BgL_setqz00_bglt
																											BgL_new1162z00_2029;
																										BgL_new1162z00_2029 =
																											((BgL_setqz00_bglt)
																											BOBJECT(GC_MALLOC(sizeof
																													(struct
																														BgL_setqz00_bgl))));
																										{	/* Ast/let.scm 408 */
																											long BgL_arg1898z00_2030;

																											BgL_arg1898z00_2030 =
																												BGL_CLASS_NUM
																												(BGl_setqz00zzast_nodez00);
																											BGL_OBJECT_CLASS_NUM_SET((
																													(BgL_objectz00_bglt)
																													BgL_new1162z00_2029),
																												BgL_arg1898z00_2030);
																										}
																										{	/* Ast/let.scm 408 */
																											BgL_objectz00_bglt
																												BgL_tmpz00_8882;
																											BgL_tmpz00_8882 =
																												((BgL_objectz00_bglt)
																												BgL_new1162z00_2029);
																											BGL_OBJECT_WIDENING_SET
																												(BgL_tmpz00_8882,
																												BFALSE);
																										}
																										((BgL_objectz00_bglt)
																											BgL_new1162z00_2029);
																										BgL_new1163z00_2025 =
																											BgL_new1162z00_2029;
																									}
																									((((BgL_nodez00_bglt) COBJECT(
																													((BgL_nodez00_bglt)
																														BgL_new1163z00_2025)))->
																											BgL_locz00) =
																										((obj_t) BgL_locz00_2020),
																										BUNSPEC);
																									((((BgL_nodez00_bglt)
																												COBJECT((
																														(BgL_nodez00_bglt)
																														BgL_new1163z00_2025)))->
																											BgL_typez00) =
																										((BgL_typez00_bglt) (
																												(BgL_typez00_bglt)
																												BGl_za2unspecza2z00zztype_cachez00)),
																										BUNSPEC);
																									{
																										BgL_varz00_bglt
																											BgL_auxz00_8891;
																										{	/* Ast/let.scm 410 */
																											BgL_refz00_bglt
																												BgL_new1165z00_2026;
																											{	/* Ast/let.scm 412 */
																												BgL_refz00_bglt
																													BgL_new1164z00_2027;
																												BgL_new1164z00_2027 =
																													((BgL_refz00_bglt)
																													BOBJECT(GC_MALLOC
																														(sizeof(struct
																																BgL_refz00_bgl))));
																												{	/* Ast/let.scm 412 */
																													long
																														BgL_arg1897z00_2028;
																													{	/* Ast/let.scm 412 */
																														obj_t
																															BgL_classz00_5833;
																														BgL_classz00_5833 =
																															BGl_refz00zzast_nodez00;
																														BgL_arg1897z00_2028
																															=
																															BGL_CLASS_NUM
																															(BgL_classz00_5833);
																													}
																													BGL_OBJECT_CLASS_NUM_SET
																														(((BgL_objectz00_bglt) BgL_new1164z00_2027), BgL_arg1897z00_2028);
																												}
																												{	/* Ast/let.scm 412 */
																													BgL_objectz00_bglt
																														BgL_tmpz00_8896;
																													BgL_tmpz00_8896 =
																														(
																														(BgL_objectz00_bglt)
																														BgL_new1164z00_2027);
																													BGL_OBJECT_WIDENING_SET
																														(BgL_tmpz00_8896,
																														BFALSE);
																												}
																												((BgL_objectz00_bglt)
																													BgL_new1164z00_2027);
																												BgL_new1165z00_2026 =
																													BgL_new1164z00_2027;
																											}
																											((((BgL_nodez00_bglt)
																														COBJECT((
																																(BgL_nodez00_bglt)
																																BgL_new1165z00_2026)))->
																													BgL_locz00) =
																												((obj_t)
																													BgL_locz00_2020),
																												BUNSPEC);
																											((((BgL_nodez00_bglt)
																														COBJECT((
																																(BgL_nodez00_bglt)
																																BgL_new1165z00_2026)))->
																													BgL_typez00) =
																												((BgL_typez00_bglt) (
																														(BgL_typez00_bglt)
																														BGl_za2_za2z00zztype_cachez00)),
																												BUNSPEC);
																											((((BgL_varz00_bglt)
																														COBJECT((
																																(BgL_varz00_bglt)
																																BgL_new1165z00_2026)))->
																													BgL_variablez00) =
																												((BgL_variablez00_bglt)
																													((BgL_variablez00_bglt) BgL_varz00_2018)), BUNSPEC);
																											BgL_auxz00_8891 =
																												((BgL_varz00_bglt)
																												BgL_new1165z00_2026);
																										}
																										((((BgL_setqz00_bglt)
																													COBJECT
																													(BgL_new1163z00_2025))->
																												BgL_varz00) =
																											((BgL_varz00_bglt)
																												BgL_auxz00_8891),
																											BUNSPEC);
																									}
																									((((BgL_setqz00_bglt)
																												COBJECT
																												(BgL_new1163z00_2025))->
																											BgL_valuez00) =
																										((BgL_nodez00_bglt) (
																												(BgL_nodez00_bglt)
																												BgL_valz00_2019)),
																										BUNSPEC);
																									BgL_initz00_2021 =
																										BgL_new1163z00_2025;
																								}
																								BGl_usezd2variablez12zc0zzast_sexpz00
																									(((BgL_variablez00_bglt)
																										BgL_varz00_2018),
																									BgL_locz00_2020,
																									CNST_TABLE_REF(5));
																								{	/* Ast/let.scm 417 */
																									BgL_nodez00_bglt
																										BgL_arg1893z00_2022;
																									BgL_arg1893z00_2022 =
																										BGl_sexpzd2ze3nodez31zzast_sexpz00
																										(BUNSPEC, BNIL,
																										BgL_locz00_2020,
																										CNST_TABLE_REF(1));
																									{	/* Ast/let.scm 416 */
																										obj_t BgL_auxz00_8919;
																										obj_t BgL_tmpz00_8917;

																										BgL_auxz00_8919 =
																											((obj_t)
																											BgL_arg1893z00_2022);
																										BgL_tmpz00_8917 =
																											((obj_t)
																											BgL_bindingz00_2017);
																										SET_CDR(BgL_tmpz00_8917,
																											BgL_auxz00_8919);
																								}}
																								{	/* Ast/let.scm 418 */
																									obj_t BgL_arg1894z00_2023;
																									obj_t BgL_arg1896z00_2024;

																									BgL_arg1894z00_2023 =
																										CDR(
																										((obj_t)
																											BgL_bindingsz00_2007));
																									BgL_arg1896z00_2024 =
																										MAKE_YOUNG_PAIR(((obj_t)
																											BgL_initz00_2021),
																										BgL_nsequencez00_2008);
																									{
																										obj_t BgL_nsequencez00_8927;
																										obj_t BgL_bindingsz00_8926;

																										BgL_bindingsz00_8926 =
																											BgL_arg1894z00_2023;
																										BgL_nsequencez00_8927 =
																											BgL_arg1896z00_2024;
																										BgL_nsequencez00_2008 =
																											BgL_nsequencez00_8927;
																										BgL_bindingsz00_2007 =
																											BgL_bindingsz00_8926;
																										goto
																											BgL_zc3z04anonymousza31887ze3z87_2009;
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																}
															}
														}
													}
												}
											else
												{	/* Ast/let.scm 421 */
													obj_t BgL_bindingsz00_2037;

													BgL_bindingsz00_2037 =
														(((BgL_letzd2varzd2_bglt)
															COBJECT(BgL_nodezd2letzd2_30))->BgL_bindingsz00);
													{	/* Ast/let.scm 421 */
														BgL_nodez00_bglt BgL_bodyz00_2038;

														BgL_bodyz00_2038 =
															(((BgL_letzd2varzd2_bglt)
																COBJECT(BgL_nodezd2letzd2_30))->BgL_bodyz00);
														{	/* Ast/let.scm 422 */
															BgL_nodez00_bglt BgL_seqz00_2039;

															{	/* Ast/let.scm 423 */
																bool_t BgL_test3913z00_8930;

																{	/* Ast/let.scm 423 */
																	obj_t BgL_classz00_5841;

																	BgL_classz00_5841 =
																		BGl_sequencez00zzast_nodez00;
																	{	/* Ast/let.scm 423 */
																		BgL_objectz00_bglt BgL_arg1807z00_5843;

																		{	/* Ast/let.scm 423 */
																			obj_t BgL_tmpz00_8931;

																			BgL_tmpz00_8931 =
																				((obj_t)
																				((BgL_objectz00_bglt)
																					BgL_bodyz00_2038));
																			BgL_arg1807z00_5843 =
																				(BgL_objectz00_bglt) (BgL_tmpz00_8931);
																		}
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Ast/let.scm 423 */
																				long BgL_idxz00_5849;

																				BgL_idxz00_5849 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_5843);
																				BgL_test3913z00_8930 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_5849 + 3L)) ==
																					BgL_classz00_5841);
																			}
																		else
																			{	/* Ast/let.scm 423 */
																				bool_t BgL_res3769z00_5874;

																				{	/* Ast/let.scm 423 */
																					obj_t BgL_oclassz00_5857;

																					{	/* Ast/let.scm 423 */
																						obj_t BgL_arg1815z00_5865;
																						long BgL_arg1816z00_5866;

																						BgL_arg1815z00_5865 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Ast/let.scm 423 */
																							long BgL_arg1817z00_5867;

																							BgL_arg1817z00_5867 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_5843);
																							BgL_arg1816z00_5866 =
																								(BgL_arg1817z00_5867 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_5857 =
																							VECTOR_REF(BgL_arg1815z00_5865,
																							BgL_arg1816z00_5866);
																					}
																					{	/* Ast/let.scm 423 */
																						bool_t BgL__ortest_1115z00_5858;

																						BgL__ortest_1115z00_5858 =
																							(BgL_classz00_5841 ==
																							BgL_oclassz00_5857);
																						if (BgL__ortest_1115z00_5858)
																							{	/* Ast/let.scm 423 */
																								BgL_res3769z00_5874 =
																									BgL__ortest_1115z00_5858;
																							}
																						else
																							{	/* Ast/let.scm 423 */
																								long BgL_odepthz00_5859;

																								{	/* Ast/let.scm 423 */
																									obj_t BgL_arg1804z00_5860;

																									BgL_arg1804z00_5860 =
																										(BgL_oclassz00_5857);
																									BgL_odepthz00_5859 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_5860);
																								}
																								if ((3L < BgL_odepthz00_5859))
																									{	/* Ast/let.scm 423 */
																										obj_t BgL_arg1802z00_5862;

																										{	/* Ast/let.scm 423 */
																											obj_t BgL_arg1803z00_5863;

																											BgL_arg1803z00_5863 =
																												(BgL_oclassz00_5857);
																											BgL_arg1802z00_5862 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_5863,
																												3L);
																										}
																										BgL_res3769z00_5874 =
																											(BgL_arg1802z00_5862 ==
																											BgL_classz00_5841);
																									}
																								else
																									{	/* Ast/let.scm 423 */
																										BgL_res3769z00_5874 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test3913z00_8930 =
																					BgL_res3769z00_5874;
																			}
																	}
																}
																if (BgL_test3913z00_8930)
																	{	/* Ast/let.scm 423 */
																		BgL_seqz00_2039 = BgL_bodyz00_2038;
																	}
																else
																	{	/* Ast/let.scm 425 */
																		BgL_sequencez00_bglt BgL_new1167z00_2085;

																		{	/* Ast/let.scm 426 */
																			BgL_sequencez00_bglt BgL_new1166z00_2087;

																			BgL_new1166z00_2087 =
																				((BgL_sequencez00_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_sequencez00_bgl))));
																			{	/* Ast/let.scm 426 */
																				long BgL_arg1928z00_2088;

																				BgL_arg1928z00_2088 =
																					BGL_CLASS_NUM
																					(BGl_sequencez00zzast_nodez00);
																				BGL_OBJECT_CLASS_NUM_SET((
																						(BgL_objectz00_bglt)
																						BgL_new1166z00_2087),
																					BgL_arg1928z00_2088);
																			}
																			{	/* Ast/let.scm 426 */
																				BgL_objectz00_bglt BgL_tmpz00_8958;

																				BgL_tmpz00_8958 =
																					((BgL_objectz00_bglt)
																					BgL_new1166z00_2087);
																				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8958,
																					BFALSE);
																			}
																			((BgL_objectz00_bglt)
																				BgL_new1166z00_2087);
																			BgL_new1167z00_2085 = BgL_new1166z00_2087;
																		}
																		((((BgL_nodez00_bglt) COBJECT(
																						((BgL_nodez00_bglt)
																							BgL_new1167z00_2085)))->
																				BgL_locz00) =
																			((obj_t) (((BgL_nodez00_bglt)
																						COBJECT(BgL_bodyz00_2038))->
																					BgL_locz00)), BUNSPEC);
																		((((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt)
																							BgL_new1167z00_2085)))->
																				BgL_typez00) =
																			((BgL_typez00_bglt) ((BgL_typez00_bglt)
																					BGl_za2_za2z00zztype_cachez00)),
																			BUNSPEC);
																		((((BgL_nodezf2effectzf2_bglt)
																					COBJECT(((BgL_nodezf2effectzf2_bglt)
																							BgL_new1167z00_2085)))->
																				BgL_sidezd2effectzd2) =
																			((obj_t) BUNSPEC), BUNSPEC);
																		((((BgL_nodezf2effectzf2_bglt)
																					COBJECT(((BgL_nodezf2effectzf2_bglt)
																							BgL_new1167z00_2085)))->
																				BgL_keyz00) =
																			((obj_t) BINT(-1L)), BUNSPEC);
																		{
																			obj_t BgL_auxz00_8973;

																			{	/* Ast/let.scm 428 */
																				obj_t BgL_list1927z00_2086;

																				BgL_list1927z00_2086 =
																					MAKE_YOUNG_PAIR(
																					((obj_t) BgL_bodyz00_2038), BNIL);
																				BgL_auxz00_8973 = BgL_list1927z00_2086;
																			}
																			((((BgL_sequencez00_bglt)
																						COBJECT(BgL_new1167z00_2085))->
																					BgL_nodesz00) =
																				((obj_t) BgL_auxz00_8973), BUNSPEC);
																		}
																		((((BgL_sequencez00_bglt)
																					COBJECT(BgL_new1167z00_2085))->
																				BgL_unsafez00) =
																			((bool_t) ((bool_t) 0)), BUNSPEC);
																		((((BgL_sequencez00_bglt)
																					COBJECT(BgL_new1167z00_2085))->
																				BgL_metaz00) = ((obj_t) BNIL), BUNSPEC);
																		BgL_seqz00_2039 =
																			((BgL_nodez00_bglt) BgL_new1167z00_2085);
															}}
															{	/* Ast/let.scm 423 */

																((((BgL_letzd2varzd2_bglt)
																			COBJECT(BgL_nodezd2letzd2_30))->
																		BgL_bodyz00) =
																	((BgL_nodez00_bglt) BgL_seqz00_2039),
																	BUNSPEC);
																{	/* Ast/let.scm 430 */
																	obj_t BgL_g1169z00_2041;

																	BgL_g1169z00_2041 =
																		(((BgL_sequencez00_bglt) COBJECT(
																				((BgL_sequencez00_bglt)
																					BgL_seqz00_2039)))->BgL_nodesz00);
																	{
																		obj_t BgL_bindingsz00_2043;
																		obj_t BgL_nbindingsz00_2044;
																		obj_t BgL_nsequencez00_2045;

																		BgL_bindingsz00_2043 = BgL_bindingsz00_2037;
																		BgL_nbindingsz00_2044 = BNIL;
																		BgL_nsequencez00_2045 = BgL_g1169z00_2041;
																	BgL_zc3z04anonymousza31902ze3z87_2046:
																		if (NULLP(BgL_bindingsz00_2043))
																			{	/* Ast/let.scm 434 */
																				obj_t BgL_typz00_2048;

																				if (NULLP(BgL_nsequencez00_2045))
																					{	/* Ast/let.scm 434 */
																						BgL_typz00_2048 =
																							BGl_za2unspecza2z00zztype_cachez00;
																					}
																				else
																					{	/* Ast/let.scm 434 */
																						BgL_typz00_2048 =
																							((obj_t)
																							(((BgL_nodez00_bglt) COBJECT(
																										((BgL_nodez00_bglt)
																											CAR
																											(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00
																												(BgL_nsequencez00_2045)))))->
																								BgL_typez00));
																					}
																				{	/* Ast/let.scm 434 */
																					BgL_sequencez00_bglt BgL_seqz00_2049;

																					{	/* Ast/let.scm 437 */
																						BgL_sequencez00_bglt
																							BgL_new1171z00_2055;
																						{	/* Ast/let.scm 438 */
																							BgL_sequencez00_bglt
																								BgL_new1170z00_2056;
																							BgL_new1170z00_2056 =
																								((BgL_sequencez00_bglt)
																								BOBJECT(GC_MALLOC(sizeof(struct
																											BgL_sequencez00_bgl))));
																							{	/* Ast/let.scm 438 */
																								long BgL_arg1910z00_2057;

																								BgL_arg1910z00_2057 =
																									BGL_CLASS_NUM
																									(BGl_sequencez00zzast_nodez00);
																								BGL_OBJECT_CLASS_NUM_SET((
																										(BgL_objectz00_bglt)
																										BgL_new1170z00_2056),
																									BgL_arg1910z00_2057);
																							}
																							{	/* Ast/let.scm 438 */
																								BgL_objectz00_bglt
																									BgL_tmpz00_8996;
																								BgL_tmpz00_8996 =
																									((BgL_objectz00_bglt)
																									BgL_new1170z00_2056);
																								BGL_OBJECT_WIDENING_SET
																									(BgL_tmpz00_8996, BFALSE);
																							}
																							((BgL_objectz00_bglt)
																								BgL_new1170z00_2056);
																							BgL_new1171z00_2055 =
																								BgL_new1170z00_2056;
																						}
																						((((BgL_nodez00_bglt) COBJECT(
																										((BgL_nodez00_bglt)
																											BgL_new1171z00_2055)))->
																								BgL_locz00) =
																							((obj_t) (((BgL_nodez00_bglt)
																										COBJECT(BgL_seqz00_2039))->
																									BgL_locz00)), BUNSPEC);
																						((((BgL_nodez00_bglt)
																									COBJECT(((BgL_nodez00_bglt)
																											BgL_new1171z00_2055)))->
																								BgL_typez00) =
																							((BgL_typez00_bglt) (
																									(BgL_typez00_bglt)
																									BGl_za2_za2z00zztype_cachez00)),
																							BUNSPEC);
																						((((BgL_nodezf2effectzf2_bglt)
																									COBJECT((
																											(BgL_nodezf2effectzf2_bglt)
																											BgL_new1171z00_2055)))->
																								BgL_sidezd2effectzd2) =
																							((obj_t) BUNSPEC), BUNSPEC);
																						((((BgL_nodezf2effectzf2_bglt)
																									COBJECT((
																											(BgL_nodezf2effectzf2_bglt)
																											BgL_new1171z00_2055)))->
																								BgL_keyz00) =
																							((obj_t) BINT(-1L)), BUNSPEC);
																						((((BgL_sequencez00_bglt)
																									COBJECT
																									(BgL_new1171z00_2055))->
																								BgL_nodesz00) =
																							((obj_t) BgL_nsequencez00_2045),
																							BUNSPEC);
																						((((BgL_sequencez00_bglt)
																									COBJECT
																									(BgL_new1171z00_2055))->
																								BgL_unsafez00) =
																							((bool_t) ((bool_t) 0)), BUNSPEC);
																						((((BgL_sequencez00_bglt)
																									COBJECT
																									(BgL_new1171z00_2055))->
																								BgL_metaz00) =
																							((obj_t) BNIL), BUNSPEC);
																						BgL_seqz00_2049 =
																							BgL_new1171z00_2055;
																					}
																					{	/* Ast/let.scm 437 */
																						BgL_letzd2varzd2_bglt
																							BgL_letbz00_2050;
																						{	/* Ast/let.scm 441 */
																							BgL_letzd2varzd2_bglt
																								BgL_new1173z00_2051;
																							{	/* Ast/let.scm 442 */
																								BgL_letzd2varzd2_bglt
																									BgL_new1172z00_2053;
																								BgL_new1172z00_2053 =
																									((BgL_letzd2varzd2_bglt)
																									BOBJECT(GC_MALLOC(sizeof
																											(struct
																												BgL_letzd2varzd2_bgl))));
																								{	/* Ast/let.scm 442 */
																									long BgL_arg1906z00_2054;

																									BgL_arg1906z00_2054 =
																										BGL_CLASS_NUM
																										(BGl_letzd2varzd2zzast_nodez00);
																									BGL_OBJECT_CLASS_NUM_SET((
																											(BgL_objectz00_bglt)
																											BgL_new1172z00_2053),
																										BgL_arg1906z00_2054);
																								}
																								{	/* Ast/let.scm 442 */
																									BgL_objectz00_bglt
																										BgL_tmpz00_9018;
																									BgL_tmpz00_9018 =
																										((BgL_objectz00_bglt)
																										BgL_new1172z00_2053);
																									BGL_OBJECT_WIDENING_SET
																										(BgL_tmpz00_9018, BFALSE);
																								}
																								((BgL_objectz00_bglt)
																									BgL_new1172z00_2053);
																								BgL_new1173z00_2051 =
																									BgL_new1172z00_2053;
																							}
																							((((BgL_nodez00_bglt) COBJECT(
																											((BgL_nodez00_bglt)
																												BgL_new1173z00_2051)))->
																									BgL_locz00) =
																								((obj_t) (((BgL_nodez00_bglt)
																											COBJECT((
																													(BgL_nodez00_bglt)
																													BgL_nodezd2letzd2_30)))->
																										BgL_locz00)), BUNSPEC);
																							{
																								BgL_typez00_bglt
																									BgL_auxz00_9026;
																								{	/* Ast/let.scm 443 */
																									BgL_typez00_bglt
																										BgL_arg1904z00_2052;
																									BgL_arg1904z00_2052 =
																										(((BgL_nodez00_bglt)
																											COBJECT((
																													(BgL_nodez00_bglt)
																													BgL_nodezd2letzd2_30)))->
																										BgL_typez00);
																									BgL_auxz00_9026 =
																										BGl_strictzd2nodezd2typez00zzast_nodez00
																										(((BgL_typez00_bglt)
																											BGl_za2_za2z00zztype_cachez00),
																										BgL_arg1904z00_2052);
																								}
																								((((BgL_nodez00_bglt) COBJECT(
																												((BgL_nodez00_bglt)
																													BgL_new1173z00_2051)))->
																										BgL_typez00) =
																									((BgL_typez00_bglt)
																										BgL_auxz00_9026), BUNSPEC);
																							}
																							((((BgL_nodezf2effectzf2_bglt)
																										COBJECT((
																												(BgL_nodezf2effectzf2_bglt)
																												BgL_new1173z00_2051)))->
																									BgL_sidezd2effectzd2) =
																								((obj_t) BUNSPEC), BUNSPEC);
																							((((BgL_nodezf2effectzf2_bglt)
																										COBJECT((
																												(BgL_nodezf2effectzf2_bglt)
																												BgL_new1173z00_2051)))->
																									BgL_keyz00) =
																								((obj_t) BINT(-1L)), BUNSPEC);
																							((((BgL_letzd2varzd2_bglt)
																										COBJECT
																										(BgL_new1173z00_2051))->
																									BgL_bindingsz00) =
																								((obj_t) BgL_nbindingsz00_2044),
																								BUNSPEC);
																							((((BgL_letzd2varzd2_bglt)
																										COBJECT
																										(BgL_new1173z00_2051))->
																									BgL_bodyz00) =
																								((BgL_nodez00_bglt) (
																										(BgL_nodez00_bglt)
																										BgL_seqz00_2049)), BUNSPEC);
																							((((BgL_letzd2varzd2_bglt)
																										COBJECT
																										(BgL_new1173z00_2051))->
																									BgL_removablezf3zf3) =
																								((bool_t) ((bool_t) 1)),
																								BUNSPEC);
																							BgL_letbz00_2050 =
																								BgL_new1173z00_2051;
																						}
																						{	/* Ast/let.scm 441 */

																							((((BgL_letzd2varzd2_bglt)
																										COBJECT
																										(BgL_nodezd2letzd2_30))->
																									BgL_bodyz00) =
																								((BgL_nodez00_bglt) (
																										(BgL_nodez00_bglt)
																										BgL_letbz00_2050)),
																								BUNSPEC);
																							return BgL_nodezd2letzd2_30;
																						}
																					}
																				}
																			}
																		else
																			{	/* Ast/let.scm 449 */
																				obj_t BgL_bindingz00_2061;

																				BgL_bindingz00_2061 =
																					CAR(((obj_t) BgL_bindingsz00_2043));
																				{	/* Ast/let.scm 449 */
																					obj_t BgL_varz00_2062;

																					BgL_varz00_2062 =
																						CAR(((obj_t) BgL_bindingz00_2061));
																					{	/* Ast/let.scm 450 */
																						obj_t BgL_valz00_2063;

																						BgL_valz00_2063 =
																							CDR(
																							((obj_t) BgL_bindingz00_2061));
																						{	/* Ast/let.scm 451 */
																							obj_t BgL_locz00_2064;

																							BgL_locz00_2064 =
																								(((BgL_nodez00_bglt) COBJECT(
																										((BgL_nodez00_bglt)
																											BgL_valz00_2063)))->
																								BgL_locz00);
																							{	/* Ast/let.scm 452 */
																								BgL_localz00_bglt
																									BgL_nvarz00_2065;
																								{	/* Ast/let.scm 453 */
																									obj_t BgL_arg1925z00_2081;

																									{	/* Ast/let.scm 453 */

																										{	/* Ast/let.scm 453 */

																											BgL_arg1925z00_2081 =
																												BGl_gensymz00zz__r4_symbols_6_4z00
																												(BFALSE);
																										}
																									}
																									BgL_nvarz00_2065 =
																										BGl_makezd2localzd2svarz00zzast_localz00
																										(BgL_arg1925z00_2081,
																										((BgL_typez00_bglt)
																											BGl_za2_za2z00zztype_cachez00));
																								}
																								{	/* Ast/let.scm 453 */

																									{	/* Ast/let.scm 454 */
																										BgL_setqz00_bglt
																											BgL_initz00_2066;
																										{	/* Ast/let.scm 454 */
																											BgL_setqz00_bglt
																												BgL_new1175z00_2072;
																											{	/* Ast/let.scm 455 */
																												BgL_setqz00_bglt
																													BgL_new1174z00_2079;
																												BgL_new1174z00_2079 =
																													((BgL_setqz00_bglt)
																													BOBJECT(GC_MALLOC
																														(sizeof(struct
																																BgL_setqz00_bgl))));
																												{	/* Ast/let.scm 455 */
																													long
																														BgL_arg1924z00_2080;
																													BgL_arg1924z00_2080 =
																														BGL_CLASS_NUM
																														(BGl_setqz00zzast_nodez00);
																													BGL_OBJECT_CLASS_NUM_SET
																														(((BgL_objectz00_bglt) BgL_new1174z00_2079), BgL_arg1924z00_2080);
																												}
																												{	/* Ast/let.scm 455 */
																													BgL_objectz00_bglt
																														BgL_tmpz00_9059;
																													BgL_tmpz00_9059 =
																														(
																														(BgL_objectz00_bglt)
																														BgL_new1174z00_2079);
																													BGL_OBJECT_WIDENING_SET
																														(BgL_tmpz00_9059,
																														BFALSE);
																												}
																												((BgL_objectz00_bglt)
																													BgL_new1174z00_2079);
																												BgL_new1175z00_2072 =
																													BgL_new1174z00_2079;
																											}
																											((((BgL_nodez00_bglt)
																														COBJECT((
																																(BgL_nodez00_bglt)
																																BgL_new1175z00_2072)))->
																													BgL_locz00) =
																												((obj_t)
																													BgL_locz00_2064),
																												BUNSPEC);
																											((((BgL_nodez00_bglt)
																														COBJECT((
																																(BgL_nodez00_bglt)
																																BgL_new1175z00_2072)))->
																													BgL_typez00) =
																												((BgL_typez00_bglt) (
																														(BgL_typez00_bglt)
																														BGl_za2unspecza2z00zztype_cachez00)),
																												BUNSPEC);
																											{
																												BgL_varz00_bglt
																													BgL_auxz00_9068;
																												{	/* Ast/let.scm 457 */
																													BgL_refz00_bglt
																														BgL_new1177z00_2073;
																													{	/* Ast/let.scm 459 */
																														BgL_refz00_bglt
																															BgL_new1176z00_2074;
																														BgL_new1176z00_2074
																															=
																															((BgL_refz00_bglt)
																															BOBJECT(GC_MALLOC
																																(sizeof(struct
																																		BgL_refz00_bgl))));
																														{	/* Ast/let.scm 459 */
																															long
																																BgL_arg1920z00_2075;
																															{	/* Ast/let.scm 459 */
																																obj_t
																																	BgL_classz00_5907;
																																BgL_classz00_5907
																																	=
																																	BGl_refz00zzast_nodez00;
																																BgL_arg1920z00_2075
																																	=
																																	BGL_CLASS_NUM
																																	(BgL_classz00_5907);
																															}
																															BGL_OBJECT_CLASS_NUM_SET
																																(((BgL_objectz00_bglt) BgL_new1176z00_2074), BgL_arg1920z00_2075);
																														}
																														{	/* Ast/let.scm 459 */
																															BgL_objectz00_bglt
																																BgL_tmpz00_9073;
																															BgL_tmpz00_9073 =
																																(
																																(BgL_objectz00_bglt)
																																BgL_new1176z00_2074);
																															BGL_OBJECT_WIDENING_SET
																																(BgL_tmpz00_9073,
																																BFALSE);
																														}
																														((BgL_objectz00_bglt) BgL_new1176z00_2074);
																														BgL_new1177z00_2073
																															=
																															BgL_new1176z00_2074;
																													}
																													((((BgL_nodez00_bglt)
																																COBJECT((
																																		(BgL_nodez00_bglt)
																																		BgL_new1177z00_2073)))->
																															BgL_locz00) =
																														((obj_t)
																															BgL_locz00_2064),
																														BUNSPEC);
																													((((BgL_nodez00_bglt)
																																COBJECT((
																																		(BgL_nodez00_bglt)
																																		BgL_new1177z00_2073)))->
																															BgL_typez00) =
																														((BgL_typez00_bglt)
																															((BgL_typez00_bglt) BGl_za2_za2z00zztype_cachez00)), BUNSPEC);
																													((((BgL_varz00_bglt)
																																COBJECT((
																																		(BgL_varz00_bglt)
																																		BgL_new1177z00_2073)))->
																															BgL_variablez00) =
																														((BgL_variablez00_bglt) ((BgL_variablez00_bglt) BgL_varz00_2062)), BUNSPEC);
																													BgL_auxz00_9068 =
																														((BgL_varz00_bglt)
																														BgL_new1177z00_2073);
																												}
																												((((BgL_setqz00_bglt)
																															COBJECT
																															(BgL_new1175z00_2072))->
																														BgL_varz00) =
																													((BgL_varz00_bglt)
																														BgL_auxz00_9068),
																													BUNSPEC);
																											}
																											{
																												BgL_nodez00_bglt
																													BgL_auxz00_9087;
																												{	/* Ast/let.scm 461 */
																													BgL_refz00_bglt
																														BgL_new1179z00_2076;
																													{	/* Ast/let.scm 463 */
																														BgL_refz00_bglt
																															BgL_new1178z00_2077;
																														BgL_new1178z00_2077
																															=
																															((BgL_refz00_bglt)
																															BOBJECT(GC_MALLOC
																																(sizeof(struct
																																		BgL_refz00_bgl))));
																														{	/* Ast/let.scm 463 */
																															long
																																BgL_arg1923z00_2078;
																															{	/* Ast/let.scm 463 */
																																obj_t
																																	BgL_classz00_5911;
																																BgL_classz00_5911
																																	=
																																	BGl_refz00zzast_nodez00;
																																BgL_arg1923z00_2078
																																	=
																																	BGL_CLASS_NUM
																																	(BgL_classz00_5911);
																															}
																															BGL_OBJECT_CLASS_NUM_SET
																																(((BgL_objectz00_bglt) BgL_new1178z00_2077), BgL_arg1923z00_2078);
																														}
																														{	/* Ast/let.scm 463 */
																															BgL_objectz00_bglt
																																BgL_tmpz00_9092;
																															BgL_tmpz00_9092 =
																																(
																																(BgL_objectz00_bglt)
																																BgL_new1178z00_2077);
																															BGL_OBJECT_WIDENING_SET
																																(BgL_tmpz00_9092,
																																BFALSE);
																														}
																														((BgL_objectz00_bglt) BgL_new1178z00_2077);
																														BgL_new1179z00_2076
																															=
																															BgL_new1178z00_2077;
																													}
																													((((BgL_nodez00_bglt)
																																COBJECT((
																																		(BgL_nodez00_bglt)
																																		BgL_new1179z00_2076)))->
																															BgL_locz00) =
																														((obj_t)
																															BgL_locz00_2064),
																														BUNSPEC);
																													((((BgL_nodez00_bglt)
																																COBJECT((
																																		(BgL_nodez00_bglt)
																																		BgL_new1179z00_2076)))->
																															BgL_typez00) =
																														((BgL_typez00_bglt)
																															((BgL_typez00_bglt) BGl_za2_za2z00zztype_cachez00)), BUNSPEC);
																													((((BgL_varz00_bglt)
																																COBJECT((
																																		(BgL_varz00_bglt)
																																		BgL_new1179z00_2076)))->
																															BgL_variablez00) =
																														((BgL_variablez00_bglt) ((BgL_variablez00_bglt) BgL_nvarz00_2065)), BUNSPEC);
																													BgL_auxz00_9087 =
																														((BgL_nodez00_bglt)
																														BgL_new1179z00_2076);
																												}
																												((((BgL_setqz00_bglt)
																															COBJECT
																															(BgL_new1175z00_2072))->
																														BgL_valuez00) =
																													((BgL_nodez00_bglt)
																														BgL_auxz00_9087),
																													BUNSPEC);
																											}
																											BgL_initz00_2066 =
																												BgL_new1175z00_2072;
																										}
																										BGl_usezd2variablez12zc0zzast_sexpz00
																											(((BgL_variablez00_bglt)
																												BgL_varz00_2062),
																											BgL_locz00_2064,
																											CNST_TABLE_REF(5));
																										BGl_usezd2variablez12zc0zzast_sexpz00
																											(((BgL_variablez00_bglt)
																												BgL_nvarz00_2065),
																											BgL_locz00_2064,
																											CNST_TABLE_REF(5));
																										{	/* Ast/let.scm 468 */
																											BgL_nodez00_bglt
																												BgL_arg1914z00_2067;
																											BgL_arg1914z00_2067 =
																												BGl_sexpzd2ze3nodez31zzast_sexpz00
																												(BUNSPEC, BNIL,
																												BgL_locz00_2064,
																												CNST_TABLE_REF(1));
																											{	/* Ast/let.scm 467 */
																												obj_t BgL_auxz00_9116;
																												obj_t BgL_tmpz00_9114;

																												BgL_auxz00_9116 =
																													((obj_t)
																													BgL_arg1914z00_2067);
																												BgL_tmpz00_9114 =
																													((obj_t)
																													BgL_bindingz00_2061);
																												SET_CDR(BgL_tmpz00_9114,
																													BgL_auxz00_9116);
																										}}
																										{	/* Ast/let.scm 469 */
																											obj_t BgL_arg1916z00_2068;
																											obj_t BgL_arg1917z00_2069;
																											obj_t BgL_arg1918z00_2070;

																											BgL_arg1916z00_2068 =
																												CDR(
																												((obj_t)
																													BgL_bindingsz00_2043));
																											{	/* Ast/let.scm 470 */
																												obj_t
																													BgL_arg1919z00_2071;
																												BgL_arg1919z00_2071 =
																													MAKE_YOUNG_PAIR((
																														(obj_t)
																														BgL_nvarz00_2065),
																													BgL_valz00_2063);
																												BgL_arg1917z00_2069 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1919z00_2071,
																													BgL_nbindingsz00_2044);
																											}
																											BgL_arg1918z00_2070 =
																												MAKE_YOUNG_PAIR(
																												((obj_t)
																													BgL_initz00_2066),
																												BgL_nsequencez00_2045);
																											{
																												obj_t
																													BgL_nsequencez00_9128;
																												obj_t
																													BgL_nbindingsz00_9127;
																												obj_t
																													BgL_bindingsz00_9126;
																												BgL_bindingsz00_9126 =
																													BgL_arg1916z00_2068;
																												BgL_nbindingsz00_9127 =
																													BgL_arg1917z00_2069;
																												BgL_nsequencez00_9128 =
																													BgL_arg1918z00_2070;
																												BgL_nsequencez00_2045 =
																													BgL_nsequencez00_9128;
																												BgL_nbindingsz00_2044 =
																													BgL_nbindingsz00_9127;
																												BgL_bindingsz00_2043 =
																													BgL_bindingsz00_9126;
																												goto
																													BgL_zc3z04anonymousza31902ze3z87_2046;
																											}
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																	}
																}
															}
														}
													}
												}
										}
								}
						}
				}
			}
		}

	}



/* safe-rec-val?~0 */
	bool_t BGl_safezd2reczd2valzf3ze70z14zzast_letz00(obj_t BgL_valz00_2090)
	{
		{	/* Ast/let.scm 270 */
			{	/* Ast/let.scm 269 */
				bool_t BgL__ortest_1119z00_2092;

				{	/* Ast/let.scm 269 */
					obj_t BgL_classz00_4992;

					BgL_classz00_4992 = BGl_atomz00zzast_nodez00;
					if (BGL_OBJECTP(BgL_valz00_2090))
						{	/* Ast/let.scm 269 */
							BgL_objectz00_bglt BgL_arg1807z00_4994;

							BgL_arg1807z00_4994 = (BgL_objectz00_bglt) (BgL_valz00_2090);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Ast/let.scm 269 */
									long BgL_idxz00_5000;

									BgL_idxz00_5000 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4994);
									BgL__ortest_1119z00_2092 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_5000 + 2L)) == BgL_classz00_4992);
								}
							else
								{	/* Ast/let.scm 269 */
									bool_t BgL_res3745z00_5025;

									{	/* Ast/let.scm 269 */
										obj_t BgL_oclassz00_5008;

										{	/* Ast/let.scm 269 */
											obj_t BgL_arg1815z00_5016;
											long BgL_arg1816z00_5017;

											BgL_arg1815z00_5016 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Ast/let.scm 269 */
												long BgL_arg1817z00_5018;

												BgL_arg1817z00_5018 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4994);
												BgL_arg1816z00_5017 =
													(BgL_arg1817z00_5018 - OBJECT_TYPE);
											}
											BgL_oclassz00_5008 =
												VECTOR_REF(BgL_arg1815z00_5016, BgL_arg1816z00_5017);
										}
										{	/* Ast/let.scm 269 */
											bool_t BgL__ortest_1115z00_5009;

											BgL__ortest_1115z00_5009 =
												(BgL_classz00_4992 == BgL_oclassz00_5008);
											if (BgL__ortest_1115z00_5009)
												{	/* Ast/let.scm 269 */
													BgL_res3745z00_5025 = BgL__ortest_1115z00_5009;
												}
											else
												{	/* Ast/let.scm 269 */
													long BgL_odepthz00_5010;

													{	/* Ast/let.scm 269 */
														obj_t BgL_arg1804z00_5011;

														BgL_arg1804z00_5011 = (BgL_oclassz00_5008);
														BgL_odepthz00_5010 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_5011);
													}
													if ((2L < BgL_odepthz00_5010))
														{	/* Ast/let.scm 269 */
															obj_t BgL_arg1802z00_5013;

															{	/* Ast/let.scm 269 */
																obj_t BgL_arg1803z00_5014;

																BgL_arg1803z00_5014 = (BgL_oclassz00_5008);
																BgL_arg1802z00_5013 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5014,
																	2L);
															}
															BgL_res3745z00_5025 =
																(BgL_arg1802z00_5013 == BgL_classz00_4992);
														}
													else
														{	/* Ast/let.scm 269 */
															BgL_res3745z00_5025 = ((bool_t) 0);
														}
												}
										}
									}
									BgL__ortest_1119z00_2092 = BgL_res3745z00_5025;
								}
						}
					else
						{	/* Ast/let.scm 269 */
							BgL__ortest_1119z00_2092 = ((bool_t) 0);
						}
				}
				if (BgL__ortest_1119z00_2092)
					{	/* Ast/let.scm 269 */
						return BgL__ortest_1119z00_2092;
					}
				else
					{	/* Ast/let.scm 269 */
						bool_t BgL__ortest_1120z00_2093;

						{	/* Ast/let.scm 269 */
							obj_t BgL_classz00_5026;

							BgL_classz00_5026 = BGl_closurez00zzast_nodez00;
							if (BGL_OBJECTP(BgL_valz00_2090))
								{	/* Ast/let.scm 269 */
									BgL_objectz00_bglt BgL_arg1807z00_5028;

									BgL_arg1807z00_5028 = (BgL_objectz00_bglt) (BgL_valz00_2090);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Ast/let.scm 269 */
											long BgL_idxz00_5034;

											BgL_idxz00_5034 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5028);
											BgL__ortest_1120z00_2093 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_5034 + 3L)) == BgL_classz00_5026);
										}
									else
										{	/* Ast/let.scm 269 */
											bool_t BgL_res3746z00_5059;

											{	/* Ast/let.scm 269 */
												obj_t BgL_oclassz00_5042;

												{	/* Ast/let.scm 269 */
													obj_t BgL_arg1815z00_5050;
													long BgL_arg1816z00_5051;

													BgL_arg1815z00_5050 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Ast/let.scm 269 */
														long BgL_arg1817z00_5052;

														BgL_arg1817z00_5052 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5028);
														BgL_arg1816z00_5051 =
															(BgL_arg1817z00_5052 - OBJECT_TYPE);
													}
													BgL_oclassz00_5042 =
														VECTOR_REF(BgL_arg1815z00_5050,
														BgL_arg1816z00_5051);
												}
												{	/* Ast/let.scm 269 */
													bool_t BgL__ortest_1115z00_5043;

													BgL__ortest_1115z00_5043 =
														(BgL_classz00_5026 == BgL_oclassz00_5042);
													if (BgL__ortest_1115z00_5043)
														{	/* Ast/let.scm 269 */
															BgL_res3746z00_5059 = BgL__ortest_1115z00_5043;
														}
													else
														{	/* Ast/let.scm 269 */
															long BgL_odepthz00_5044;

															{	/* Ast/let.scm 269 */
																obj_t BgL_arg1804z00_5045;

																BgL_arg1804z00_5045 = (BgL_oclassz00_5042);
																BgL_odepthz00_5044 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_5045);
															}
															if ((3L < BgL_odepthz00_5044))
																{	/* Ast/let.scm 269 */
																	obj_t BgL_arg1802z00_5047;

																	{	/* Ast/let.scm 269 */
																		obj_t BgL_arg1803z00_5048;

																		BgL_arg1803z00_5048 = (BgL_oclassz00_5042);
																		BgL_arg1802z00_5047 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_5048, 3L);
																	}
																	BgL_res3746z00_5059 =
																		(BgL_arg1802z00_5047 == BgL_classz00_5026);
																}
															else
																{	/* Ast/let.scm 269 */
																	BgL_res3746z00_5059 = ((bool_t) 0);
																}
														}
												}
											}
											BgL__ortest_1120z00_2093 = BgL_res3746z00_5059;
										}
								}
							else
								{	/* Ast/let.scm 269 */
									BgL__ortest_1120z00_2093 = ((bool_t) 0);
								}
						}
						if (BgL__ortest_1120z00_2093)
							{	/* Ast/let.scm 269 */
								return BgL__ortest_1120z00_2093;
							}
						else
							{	/* Ast/let.scm 269 */
								bool_t BgL__ortest_1121z00_2094;

								{	/* Ast/let.scm 269 */
									obj_t BgL_classz00_5060;

									BgL_classz00_5060 = BGl_kwotez00zzast_nodez00;
									if (BGL_OBJECTP(BgL_valz00_2090))
										{	/* Ast/let.scm 269 */
											BgL_objectz00_bglt BgL_arg1807z00_5062;

											BgL_arg1807z00_5062 =
												(BgL_objectz00_bglt) (BgL_valz00_2090);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Ast/let.scm 269 */
													long BgL_idxz00_5068;

													BgL_idxz00_5068 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5062);
													BgL__ortest_1121z00_2094 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_5068 + 2L)) == BgL_classz00_5060);
												}
											else
												{	/* Ast/let.scm 269 */
													bool_t BgL_res3747z00_5093;

													{	/* Ast/let.scm 269 */
														obj_t BgL_oclassz00_5076;

														{	/* Ast/let.scm 269 */
															obj_t BgL_arg1815z00_5084;
															long BgL_arg1816z00_5085;

															BgL_arg1815z00_5084 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Ast/let.scm 269 */
																long BgL_arg1817z00_5086;

																BgL_arg1817z00_5086 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5062);
																BgL_arg1816z00_5085 =
																	(BgL_arg1817z00_5086 - OBJECT_TYPE);
															}
															BgL_oclassz00_5076 =
																VECTOR_REF(BgL_arg1815z00_5084,
																BgL_arg1816z00_5085);
														}
														{	/* Ast/let.scm 269 */
															bool_t BgL__ortest_1115z00_5077;

															BgL__ortest_1115z00_5077 =
																(BgL_classz00_5060 == BgL_oclassz00_5076);
															if (BgL__ortest_1115z00_5077)
																{	/* Ast/let.scm 269 */
																	BgL_res3747z00_5093 =
																		BgL__ortest_1115z00_5077;
																}
															else
																{	/* Ast/let.scm 269 */
																	long BgL_odepthz00_5078;

																	{	/* Ast/let.scm 269 */
																		obj_t BgL_arg1804z00_5079;

																		BgL_arg1804z00_5079 = (BgL_oclassz00_5076);
																		BgL_odepthz00_5078 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_5079);
																	}
																	if ((2L < BgL_odepthz00_5078))
																		{	/* Ast/let.scm 269 */
																			obj_t BgL_arg1802z00_5081;

																			{	/* Ast/let.scm 269 */
																				obj_t BgL_arg1803z00_5082;

																				BgL_arg1803z00_5082 =
																					(BgL_oclassz00_5076);
																				BgL_arg1802z00_5081 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_5082, 2L);
																			}
																			BgL_res3747z00_5093 =
																				(BgL_arg1802z00_5081 ==
																				BgL_classz00_5060);
																		}
																	else
																		{	/* Ast/let.scm 269 */
																			BgL_res3747z00_5093 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL__ortest_1121z00_2094 = BgL_res3747z00_5093;
												}
										}
									else
										{	/* Ast/let.scm 269 */
											BgL__ortest_1121z00_2094 = ((bool_t) 0);
										}
								}
								if (BgL__ortest_1121z00_2094)
									{	/* Ast/let.scm 269 */
										return BgL__ortest_1121z00_2094;
									}
								else
									{	/* Ast/let.scm 270 */
										bool_t BgL_test3934z00_9198;

										{	/* Ast/let.scm 270 */
											obj_t BgL_classz00_5094;

											BgL_classz00_5094 = BGl_sequencez00zzast_nodez00;
											if (BGL_OBJECTP(BgL_valz00_2090))
												{	/* Ast/let.scm 270 */
													BgL_objectz00_bglt BgL_arg1807z00_5096;

													BgL_arg1807z00_5096 =
														(BgL_objectz00_bglt) (BgL_valz00_2090);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Ast/let.scm 270 */
															long BgL_idxz00_5102;

															BgL_idxz00_5102 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5096);
															BgL_test3934z00_9198 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_5102 + 3L)) == BgL_classz00_5094);
														}
													else
														{	/* Ast/let.scm 270 */
															bool_t BgL_res3748z00_5127;

															{	/* Ast/let.scm 270 */
																obj_t BgL_oclassz00_5110;

																{	/* Ast/let.scm 270 */
																	obj_t BgL_arg1815z00_5118;
																	long BgL_arg1816z00_5119;

																	BgL_arg1815z00_5118 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Ast/let.scm 270 */
																		long BgL_arg1817z00_5120;

																		BgL_arg1817z00_5120 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5096);
																		BgL_arg1816z00_5119 =
																			(BgL_arg1817z00_5120 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_5110 =
																		VECTOR_REF(BgL_arg1815z00_5118,
																		BgL_arg1816z00_5119);
																}
																{	/* Ast/let.scm 270 */
																	bool_t BgL__ortest_1115z00_5111;

																	BgL__ortest_1115z00_5111 =
																		(BgL_classz00_5094 == BgL_oclassz00_5110);
																	if (BgL__ortest_1115z00_5111)
																		{	/* Ast/let.scm 270 */
																			BgL_res3748z00_5127 =
																				BgL__ortest_1115z00_5111;
																		}
																	else
																		{	/* Ast/let.scm 270 */
																			long BgL_odepthz00_5112;

																			{	/* Ast/let.scm 270 */
																				obj_t BgL_arg1804z00_5113;

																				BgL_arg1804z00_5113 =
																					(BgL_oclassz00_5110);
																				BgL_odepthz00_5112 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_5113);
																			}
																			if ((3L < BgL_odepthz00_5112))
																				{	/* Ast/let.scm 270 */
																					obj_t BgL_arg1802z00_5115;

																					{	/* Ast/let.scm 270 */
																						obj_t BgL_arg1803z00_5116;

																						BgL_arg1803z00_5116 =
																							(BgL_oclassz00_5110);
																						BgL_arg1802z00_5115 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_5116, 3L);
																					}
																					BgL_res3748z00_5127 =
																						(BgL_arg1802z00_5115 ==
																						BgL_classz00_5094);
																				}
																			else
																				{	/* Ast/let.scm 270 */
																					BgL_res3748z00_5127 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test3934z00_9198 = BgL_res3748z00_5127;
														}
												}
											else
												{	/* Ast/let.scm 270 */
													BgL_test3934z00_9198 = ((bool_t) 0);
												}
										}
										if (BgL_test3934z00_9198)
											{	/* Ast/let.scm 270 */
												obj_t BgL_g1382z00_2096;

												BgL_g1382z00_2096 =
													(((BgL_sequencez00_bglt) COBJECT(
															((BgL_sequencez00_bglt) BgL_valz00_2090)))->
													BgL_nodesz00);
												{
													obj_t BgL_l1380z00_2098;

													BgL_l1380z00_2098 = BgL_g1382z00_2096;
												BgL_zc3z04anonymousza31930ze3z87_2099:
													if (NULLP(BgL_l1380z00_2098))
														{	/* Ast/let.scm 270 */
															return ((bool_t) 1);
														}
													else
														{	/* Ast/let.scm 270 */
															bool_t BgL_test3940z00_9225;

															{	/* Ast/let.scm 270 */
																obj_t BgL_arg1933z00_2104;

																BgL_arg1933z00_2104 =
																	CAR(((obj_t) BgL_l1380z00_2098));
																BgL_test3940z00_9225 =
																	BGl_safezd2reczd2valzf3ze70z14zzast_letz00
																	(BgL_arg1933z00_2104);
															}
															if (BgL_test3940z00_9225)
																{	/* Ast/let.scm 270 */
																	obj_t BgL_arg1932z00_2103;

																	BgL_arg1932z00_2103 =
																		CDR(((obj_t) BgL_l1380z00_2098));
																	{
																		obj_t BgL_l1380z00_9231;

																		BgL_l1380z00_9231 = BgL_arg1932z00_2103;
																		BgL_l1380z00_2098 = BgL_l1380z00_9231;
																		goto BgL_zc3z04anonymousza31930ze3z87_2099;
																	}
																}
															else
																{	/* Ast/let.scm 270 */
																	return ((bool_t) 0);
																}
														}
												}
											}
										else
											{	/* Ast/let.scm 270 */
												return ((bool_t) 0);
											}
									}
							}
					}
			}
		}

	}



/* safe-rec-val-optim?~0 */
	bool_t BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00(obj_t
		BgL_valz00_2106, obj_t BgL_varsz00_2107)
	{
		{	/* Ast/let.scm 355 */
		BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00:
			{	/* Ast/let.scm 273 */
				bool_t BgL__ortest_1123z00_2109;

				BgL__ortest_1123z00_2109 =
					BGl_safezd2reczd2valzf3ze70z14zzast_letz00(BgL_valz00_2106);
				if (BgL__ortest_1123z00_2109)
					{	/* Ast/let.scm 273 */
						return BgL__ortest_1123z00_2109;
					}
				else
					{	/* Ast/let.scm 273 */
						if (NULLP(BgL_valz00_2106))
							{	/* Ast/let.scm 275 */
								return ((bool_t) 1);
							}
						else
							{	/* Ast/let.scm 277 */
								bool_t BgL_test3943z00_9236;

								{	/* Ast/let.scm 277 */
									obj_t BgL_classz00_5131;

									BgL_classz00_5131 = BGl_atomz00zzast_nodez00;
									if (BGL_OBJECTP(BgL_valz00_2106))
										{	/* Ast/let.scm 277 */
											BgL_objectz00_bglt BgL_arg1807z00_5133;

											BgL_arg1807z00_5133 =
												(BgL_objectz00_bglt) (BgL_valz00_2106);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Ast/let.scm 277 */
													long BgL_idxz00_5139;

													BgL_idxz00_5139 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5133);
													BgL_test3943z00_9236 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_5139 + 2L)) == BgL_classz00_5131);
												}
											else
												{	/* Ast/let.scm 277 */
													bool_t BgL_res3749z00_5164;

													{	/* Ast/let.scm 277 */
														obj_t BgL_oclassz00_5147;

														{	/* Ast/let.scm 277 */
															obj_t BgL_arg1815z00_5155;
															long BgL_arg1816z00_5156;

															BgL_arg1815z00_5155 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Ast/let.scm 277 */
																long BgL_arg1817z00_5157;

																BgL_arg1817z00_5157 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5133);
																BgL_arg1816z00_5156 =
																	(BgL_arg1817z00_5157 - OBJECT_TYPE);
															}
															BgL_oclassz00_5147 =
																VECTOR_REF(BgL_arg1815z00_5155,
																BgL_arg1816z00_5156);
														}
														{	/* Ast/let.scm 277 */
															bool_t BgL__ortest_1115z00_5148;

															BgL__ortest_1115z00_5148 =
																(BgL_classz00_5131 == BgL_oclassz00_5147);
															if (BgL__ortest_1115z00_5148)
																{	/* Ast/let.scm 277 */
																	BgL_res3749z00_5164 =
																		BgL__ortest_1115z00_5148;
																}
															else
																{	/* Ast/let.scm 277 */
																	long BgL_odepthz00_5149;

																	{	/* Ast/let.scm 277 */
																		obj_t BgL_arg1804z00_5150;

																		BgL_arg1804z00_5150 = (BgL_oclassz00_5147);
																		BgL_odepthz00_5149 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_5150);
																	}
																	if ((2L < BgL_odepthz00_5149))
																		{	/* Ast/let.scm 277 */
																			obj_t BgL_arg1802z00_5152;

																			{	/* Ast/let.scm 277 */
																				obj_t BgL_arg1803z00_5153;

																				BgL_arg1803z00_5153 =
																					(BgL_oclassz00_5147);
																				BgL_arg1802z00_5152 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_5153, 2L);
																			}
																			BgL_res3749z00_5164 =
																				(BgL_arg1802z00_5152 ==
																				BgL_classz00_5131);
																		}
																	else
																		{	/* Ast/let.scm 277 */
																			BgL_res3749z00_5164 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test3943z00_9236 = BgL_res3749z00_5164;
												}
										}
									else
										{	/* Ast/let.scm 277 */
											BgL_test3943z00_9236 = ((bool_t) 0);
										}
								}
								if (BgL_test3943z00_9236)
									{	/* Ast/let.scm 277 */
										return ((bool_t) 1);
									}
								else
									{	/* Ast/let.scm 279 */
										bool_t BgL_test3948z00_9259;

										{	/* Ast/let.scm 279 */
											obj_t BgL_classz00_5165;

											BgL_classz00_5165 = BGl_varz00zzast_nodez00;
											if (BGL_OBJECTP(BgL_valz00_2106))
												{	/* Ast/let.scm 279 */
													BgL_objectz00_bglt BgL_arg1807z00_5167;

													BgL_arg1807z00_5167 =
														(BgL_objectz00_bglt) (BgL_valz00_2106);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Ast/let.scm 279 */
															long BgL_idxz00_5173;

															BgL_idxz00_5173 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5167);
															BgL_test3948z00_9259 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_5173 + 2L)) == BgL_classz00_5165);
														}
													else
														{	/* Ast/let.scm 279 */
															bool_t BgL_res3750z00_5198;

															{	/* Ast/let.scm 279 */
																obj_t BgL_oclassz00_5181;

																{	/* Ast/let.scm 279 */
																	obj_t BgL_arg1815z00_5189;
																	long BgL_arg1816z00_5190;

																	BgL_arg1815z00_5189 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Ast/let.scm 279 */
																		long BgL_arg1817z00_5191;

																		BgL_arg1817z00_5191 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5167);
																		BgL_arg1816z00_5190 =
																			(BgL_arg1817z00_5191 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_5181 =
																		VECTOR_REF(BgL_arg1815z00_5189,
																		BgL_arg1816z00_5190);
																}
																{	/* Ast/let.scm 279 */
																	bool_t BgL__ortest_1115z00_5182;

																	BgL__ortest_1115z00_5182 =
																		(BgL_classz00_5165 == BgL_oclassz00_5181);
																	if (BgL__ortest_1115z00_5182)
																		{	/* Ast/let.scm 279 */
																			BgL_res3750z00_5198 =
																				BgL__ortest_1115z00_5182;
																		}
																	else
																		{	/* Ast/let.scm 279 */
																			long BgL_odepthz00_5183;

																			{	/* Ast/let.scm 279 */
																				obj_t BgL_arg1804z00_5184;

																				BgL_arg1804z00_5184 =
																					(BgL_oclassz00_5181);
																				BgL_odepthz00_5183 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_5184);
																			}
																			if ((2L < BgL_odepthz00_5183))
																				{	/* Ast/let.scm 279 */
																					obj_t BgL_arg1802z00_5186;

																					{	/* Ast/let.scm 279 */
																						obj_t BgL_arg1803z00_5187;

																						BgL_arg1803z00_5187 =
																							(BgL_oclassz00_5181);
																						BgL_arg1802z00_5186 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_5187, 2L);
																					}
																					BgL_res3750z00_5198 =
																						(BgL_arg1802z00_5186 ==
																						BgL_classz00_5165);
																				}
																			else
																				{	/* Ast/let.scm 279 */
																					BgL_res3750z00_5198 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test3948z00_9259 = BgL_res3750z00_5198;
														}
												}
											else
												{	/* Ast/let.scm 279 */
													BgL_test3948z00_9259 = ((bool_t) 0);
												}
										}
										if (BgL_test3948z00_9259)
											{	/* Ast/let.scm 279 */
												if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
															((obj_t)
																(((BgL_varz00_bglt) COBJECT(
																			((BgL_varz00_bglt) BgL_valz00_2106)))->
																	BgL_variablez00)), BgL_varsz00_2107)))
													{	/* Ast/let.scm 280 */
														return ((bool_t) 0);
													}
												else
													{	/* Ast/let.scm 280 */
														return ((bool_t) 1);
													}
											}
										else
											{	/* Ast/let.scm 281 */
												bool_t BgL_test3954z00_9288;

												{	/* Ast/let.scm 281 */
													obj_t BgL_classz00_5200;

													BgL_classz00_5200 = BGl_sequencez00zzast_nodez00;
													if (BGL_OBJECTP(BgL_valz00_2106))
														{	/* Ast/let.scm 281 */
															BgL_objectz00_bglt BgL_arg1807z00_5202;

															BgL_arg1807z00_5202 =
																(BgL_objectz00_bglt) (BgL_valz00_2106);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Ast/let.scm 281 */
																	long BgL_idxz00_5208;

																	BgL_idxz00_5208 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_5202);
																	BgL_test3954z00_9288 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_5208 + 3L)) ==
																		BgL_classz00_5200);
																}
															else
																{	/* Ast/let.scm 281 */
																	bool_t BgL_res3751z00_5233;

																	{	/* Ast/let.scm 281 */
																		obj_t BgL_oclassz00_5216;

																		{	/* Ast/let.scm 281 */
																			obj_t BgL_arg1815z00_5224;
																			long BgL_arg1816z00_5225;

																			BgL_arg1815z00_5224 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Ast/let.scm 281 */
																				long BgL_arg1817z00_5226;

																				BgL_arg1817z00_5226 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_5202);
																				BgL_arg1816z00_5225 =
																					(BgL_arg1817z00_5226 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_5216 =
																				VECTOR_REF(BgL_arg1815z00_5224,
																				BgL_arg1816z00_5225);
																		}
																		{	/* Ast/let.scm 281 */
																			bool_t BgL__ortest_1115z00_5217;

																			BgL__ortest_1115z00_5217 =
																				(BgL_classz00_5200 ==
																				BgL_oclassz00_5216);
																			if (BgL__ortest_1115z00_5217)
																				{	/* Ast/let.scm 281 */
																					BgL_res3751z00_5233 =
																						BgL__ortest_1115z00_5217;
																				}
																			else
																				{	/* Ast/let.scm 281 */
																					long BgL_odepthz00_5218;

																					{	/* Ast/let.scm 281 */
																						obj_t BgL_arg1804z00_5219;

																						BgL_arg1804z00_5219 =
																							(BgL_oclassz00_5216);
																						BgL_odepthz00_5218 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_5219);
																					}
																					if ((3L < BgL_odepthz00_5218))
																						{	/* Ast/let.scm 281 */
																							obj_t BgL_arg1802z00_5221;

																							{	/* Ast/let.scm 281 */
																								obj_t BgL_arg1803z00_5222;

																								BgL_arg1803z00_5222 =
																									(BgL_oclassz00_5216);
																								BgL_arg1802z00_5221 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_5222, 3L);
																							}
																							BgL_res3751z00_5233 =
																								(BgL_arg1802z00_5221 ==
																								BgL_classz00_5200);
																						}
																					else
																						{	/* Ast/let.scm 281 */
																							BgL_res3751z00_5233 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test3954z00_9288 = BgL_res3751z00_5233;
																}
														}
													else
														{	/* Ast/let.scm 281 */
															BgL_test3954z00_9288 = ((bool_t) 0);
														}
												}
												if (BgL_test3954z00_9288)
													{	/* Ast/let.scm 282 */
														obj_t BgL_arg1942z00_2117;

														BgL_arg1942z00_2117 =
															(((BgL_sequencez00_bglt) COBJECT(
																	((BgL_sequencez00_bglt) BgL_valz00_2106)))->
															BgL_nodesz00);
														{
															obj_t BgL_valz00_9313;

															BgL_valz00_9313 = BgL_arg1942z00_2117;
															BgL_valz00_2106 = BgL_valz00_9313;
															goto
																BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00;
														}
													}
												else
													{	/* Ast/let.scm 283 */
														bool_t BgL_test3959z00_9314;

														{	/* Ast/let.scm 283 */
															obj_t BgL_classz00_5235;

															BgL_classz00_5235 = BGl_appz00zzast_nodez00;
															if (BGL_OBJECTP(BgL_valz00_2106))
																{	/* Ast/let.scm 283 */
																	BgL_objectz00_bglt BgL_arg1807z00_5237;

																	BgL_arg1807z00_5237 =
																		(BgL_objectz00_bglt) (BgL_valz00_2106);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Ast/let.scm 283 */
																			long BgL_idxz00_5243;

																			BgL_idxz00_5243 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_5237);
																			BgL_test3959z00_9314 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_5243 + 3L)) ==
																				BgL_classz00_5235);
																		}
																	else
																		{	/* Ast/let.scm 283 */
																			bool_t BgL_res3752z00_5268;

																			{	/* Ast/let.scm 283 */
																				obj_t BgL_oclassz00_5251;

																				{	/* Ast/let.scm 283 */
																					obj_t BgL_arg1815z00_5259;
																					long BgL_arg1816z00_5260;

																					BgL_arg1815z00_5259 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Ast/let.scm 283 */
																						long BgL_arg1817z00_5261;

																						BgL_arg1817z00_5261 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_5237);
																						BgL_arg1816z00_5260 =
																							(BgL_arg1817z00_5261 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_5251 =
																						VECTOR_REF(BgL_arg1815z00_5259,
																						BgL_arg1816z00_5260);
																				}
																				{	/* Ast/let.scm 283 */
																					bool_t BgL__ortest_1115z00_5252;

																					BgL__ortest_1115z00_5252 =
																						(BgL_classz00_5235 ==
																						BgL_oclassz00_5251);
																					if (BgL__ortest_1115z00_5252)
																						{	/* Ast/let.scm 283 */
																							BgL_res3752z00_5268 =
																								BgL__ortest_1115z00_5252;
																						}
																					else
																						{	/* Ast/let.scm 283 */
																							long BgL_odepthz00_5253;

																							{	/* Ast/let.scm 283 */
																								obj_t BgL_arg1804z00_5254;

																								BgL_arg1804z00_5254 =
																									(BgL_oclassz00_5251);
																								BgL_odepthz00_5253 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_5254);
																							}
																							if ((3L < BgL_odepthz00_5253))
																								{	/* Ast/let.scm 283 */
																									obj_t BgL_arg1802z00_5256;

																									{	/* Ast/let.scm 283 */
																										obj_t BgL_arg1803z00_5257;

																										BgL_arg1803z00_5257 =
																											(BgL_oclassz00_5251);
																										BgL_arg1802z00_5256 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_5257, 3L);
																									}
																									BgL_res3752z00_5268 =
																										(BgL_arg1802z00_5256 ==
																										BgL_classz00_5235);
																								}
																							else
																								{	/* Ast/let.scm 283 */
																									BgL_res3752z00_5268 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test3959z00_9314 =
																				BgL_res3752z00_5268;
																		}
																}
															else
																{	/* Ast/let.scm 283 */
																	BgL_test3959z00_9314 = ((bool_t) 0);
																}
														}
														if (BgL_test3959z00_9314)
															{	/* Ast/let.scm 285 */
																bool_t BgL_test3964z00_9337;

																{	/* Ast/let.scm 285 */
																	BgL_varz00_bglt BgL_arg1945z00_2122;

																	BgL_arg1945z00_2122 =
																		(((BgL_appz00_bglt) COBJECT(
																				((BgL_appz00_bglt) BgL_valz00_2106)))->
																		BgL_funz00);
																	BgL_test3964z00_9337 =
																		BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00
																		(((obj_t) BgL_arg1945z00_2122),
																		BgL_varsz00_2107);
																}
																if (BgL_test3964z00_9337)
																	{	/* Ast/let.scm 286 */
																		obj_t BgL_arg1944z00_2121;

																		BgL_arg1944z00_2121 =
																			(((BgL_appz00_bglt) COBJECT(
																					((BgL_appz00_bglt)
																						BgL_valz00_2106)))->BgL_argsz00);
																		{
																			obj_t BgL_valz00_9344;

																			BgL_valz00_9344 = BgL_arg1944z00_2121;
																			BgL_valz00_2106 = BgL_valz00_9344;
																			goto
																				BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00;
																		}
																	}
																else
																	{	/* Ast/let.scm 285 */
																		return ((bool_t) 0);
																	}
															}
														else
															{	/* Ast/let.scm 283 */
																if (PAIRP(BgL_valz00_2106))
																	{
																		obj_t BgL_l1384z00_2125;

																		BgL_l1384z00_2125 = BgL_valz00_2106;
																	BgL_zc3z04anonymousza31947ze3z87_2126:
																		if (NULLP(BgL_l1384z00_2125))
																			{	/* Ast/let.scm 288 */
																				return ((bool_t) 1);
																			}
																		else
																			{	/* Ast/let.scm 288 */
																				bool_t BgL_test3967z00_9349;

																				{	/* Ast/let.scm 288 */
																					obj_t BgL_vz00_2131;

																					BgL_vz00_2131 =
																						CAR(((obj_t) BgL_l1384z00_2125));
																					BgL_test3967z00_9349 =
																						BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00
																						(BgL_vz00_2131, BgL_varsz00_2107);
																				}
																				if (BgL_test3967z00_9349)
																					{	/* Ast/let.scm 288 */
																						obj_t BgL_arg1949z00_2130;

																						BgL_arg1949z00_2130 =
																							CDR(((obj_t) BgL_l1384z00_2125));
																						{
																							obj_t BgL_l1384z00_9355;

																							BgL_l1384z00_9355 =
																								BgL_arg1949z00_2130;
																							BgL_l1384z00_2125 =
																								BgL_l1384z00_9355;
																							goto
																								BgL_zc3z04anonymousza31947ze3z87_2126;
																						}
																					}
																				else
																					{	/* Ast/let.scm 288 */
																						return ((bool_t) 0);
																					}
																			}
																	}
																else
																	{	/* Ast/let.scm 289 */
																		bool_t BgL_test3968z00_9356;

																		{	/* Ast/let.scm 289 */
																			obj_t BgL_classz00_5271;

																			BgL_classz00_5271 =
																				BGl_appzd2lyzd2zzast_nodez00;
																			if (BGL_OBJECTP(BgL_valz00_2106))
																				{	/* Ast/let.scm 289 */
																					BgL_objectz00_bglt
																						BgL_arg1807z00_5273;
																					BgL_arg1807z00_5273 =
																						(BgL_objectz00_bglt)
																						(BgL_valz00_2106);
																					if (BGL_CONDEXPAND_ISA_ARCH64())
																						{	/* Ast/let.scm 289 */
																							long BgL_idxz00_5279;

																							BgL_idxz00_5279 =
																								BGL_OBJECT_INHERITANCE_NUM
																								(BgL_arg1807z00_5273);
																							BgL_test3968z00_9356 =
																								(VECTOR_REF
																								(BGl_za2inheritancesza2z00zz__objectz00,
																									(BgL_idxz00_5279 + 2L)) ==
																								BgL_classz00_5271);
																						}
																					else
																						{	/* Ast/let.scm 289 */
																							bool_t BgL_res3753z00_5304;

																							{	/* Ast/let.scm 289 */
																								obj_t BgL_oclassz00_5287;

																								{	/* Ast/let.scm 289 */
																									obj_t BgL_arg1815z00_5295;
																									long BgL_arg1816z00_5296;

																									BgL_arg1815z00_5295 =
																										(BGl_za2classesza2z00zz__objectz00);
																									{	/* Ast/let.scm 289 */
																										long BgL_arg1817z00_5297;

																										BgL_arg1817z00_5297 =
																											BGL_OBJECT_CLASS_NUM
																											(BgL_arg1807z00_5273);
																										BgL_arg1816z00_5296 =
																											(BgL_arg1817z00_5297 -
																											OBJECT_TYPE);
																									}
																									BgL_oclassz00_5287 =
																										VECTOR_REF
																										(BgL_arg1815z00_5295,
																										BgL_arg1816z00_5296);
																								}
																								{	/* Ast/let.scm 289 */
																									bool_t
																										BgL__ortest_1115z00_5288;
																									BgL__ortest_1115z00_5288 =
																										(BgL_classz00_5271 ==
																										BgL_oclassz00_5287);
																									if (BgL__ortest_1115z00_5288)
																										{	/* Ast/let.scm 289 */
																											BgL_res3753z00_5304 =
																												BgL__ortest_1115z00_5288;
																										}
																									else
																										{	/* Ast/let.scm 289 */
																											long BgL_odepthz00_5289;

																											{	/* Ast/let.scm 289 */
																												obj_t
																													BgL_arg1804z00_5290;
																												BgL_arg1804z00_5290 =
																													(BgL_oclassz00_5287);
																												BgL_odepthz00_5289 =
																													BGL_CLASS_DEPTH
																													(BgL_arg1804z00_5290);
																											}
																											if (
																												(2L <
																													BgL_odepthz00_5289))
																												{	/* Ast/let.scm 289 */
																													obj_t
																														BgL_arg1802z00_5292;
																													{	/* Ast/let.scm 289 */
																														obj_t
																															BgL_arg1803z00_5293;
																														BgL_arg1803z00_5293
																															=
																															(BgL_oclassz00_5287);
																														BgL_arg1802z00_5292
																															=
																															BGL_CLASS_ANCESTORS_REF
																															(BgL_arg1803z00_5293,
																															2L);
																													}
																													BgL_res3753z00_5304 =
																														(BgL_arg1802z00_5292
																														==
																														BgL_classz00_5271);
																												}
																											else
																												{	/* Ast/let.scm 289 */
																													BgL_res3753z00_5304 =
																														((bool_t) 0);
																												}
																										}
																								}
																							}
																							BgL_test3968z00_9356 =
																								BgL_res3753z00_5304;
																						}
																				}
																			else
																				{	/* Ast/let.scm 289 */
																					BgL_test3968z00_9356 = ((bool_t) 0);
																				}
																		}
																		if (BgL_test3968z00_9356)
																			{	/* Ast/let.scm 291 */
																				bool_t BgL_test3973z00_9379;

																				{	/* Ast/let.scm 291 */
																					BgL_nodez00_bglt BgL_arg1952z00_2137;

																					BgL_arg1952z00_2137 =
																						(((BgL_appzd2lyzd2_bglt) COBJECT(
																								((BgL_appzd2lyzd2_bglt)
																									BgL_valz00_2106)))->
																						BgL_funz00);
																					BgL_test3973z00_9379 =
																						BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00
																						(((obj_t) BgL_arg1952z00_2137),
																						BgL_varsz00_2107);
																				}
																				if (BgL_test3973z00_9379)
																					{	/* Ast/let.scm 292 */
																						BgL_nodez00_bglt
																							BgL_arg1951z00_2136;
																						BgL_arg1951z00_2136 =
																							(((BgL_appzd2lyzd2_bglt)
																								COBJECT(((BgL_appzd2lyzd2_bglt)
																										BgL_valz00_2106)))->
																							BgL_argz00);
																						{
																							obj_t BgL_valz00_9386;

																							BgL_valz00_9386 =
																								((obj_t) BgL_arg1951z00_2136);
																							BgL_valz00_2106 = BgL_valz00_9386;
																							goto
																								BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00;
																						}
																					}
																				else
																					{	/* Ast/let.scm 291 */
																						return ((bool_t) 0);
																					}
																			}
																		else
																			{	/* Ast/let.scm 293 */
																				bool_t BgL_test3974z00_9388;

																				{	/* Ast/let.scm 293 */
																					obj_t BgL_classz00_5305;

																					BgL_classz00_5305 =
																						BGl_funcallz00zzast_nodez00;
																					if (BGL_OBJECTP(BgL_valz00_2106))
																						{	/* Ast/let.scm 293 */
																							BgL_objectz00_bglt
																								BgL_arg1807z00_5307;
																							BgL_arg1807z00_5307 =
																								(BgL_objectz00_bglt)
																								(BgL_valz00_2106);
																							if (BGL_CONDEXPAND_ISA_ARCH64())
																								{	/* Ast/let.scm 293 */
																									long BgL_idxz00_5313;

																									BgL_idxz00_5313 =
																										BGL_OBJECT_INHERITANCE_NUM
																										(BgL_arg1807z00_5307);
																									BgL_test3974z00_9388 =
																										(VECTOR_REF
																										(BGl_za2inheritancesza2z00zz__objectz00,
																											(BgL_idxz00_5313 + 2L)) ==
																										BgL_classz00_5305);
																								}
																							else
																								{	/* Ast/let.scm 293 */
																									bool_t BgL_res3754z00_5338;

																									{	/* Ast/let.scm 293 */
																										obj_t BgL_oclassz00_5321;

																										{	/* Ast/let.scm 293 */
																											obj_t BgL_arg1815z00_5329;
																											long BgL_arg1816z00_5330;

																											BgL_arg1815z00_5329 =
																												(BGl_za2classesza2z00zz__objectz00);
																											{	/* Ast/let.scm 293 */
																												long
																													BgL_arg1817z00_5331;
																												BgL_arg1817z00_5331 =
																													BGL_OBJECT_CLASS_NUM
																													(BgL_arg1807z00_5307);
																												BgL_arg1816z00_5330 =
																													(BgL_arg1817z00_5331 -
																													OBJECT_TYPE);
																											}
																											BgL_oclassz00_5321 =
																												VECTOR_REF
																												(BgL_arg1815z00_5329,
																												BgL_arg1816z00_5330);
																										}
																										{	/* Ast/let.scm 293 */
																											bool_t
																												BgL__ortest_1115z00_5322;
																											BgL__ortest_1115z00_5322 =
																												(BgL_classz00_5305 ==
																												BgL_oclassz00_5321);
																											if (BgL__ortest_1115z00_5322)
																												{	/* Ast/let.scm 293 */
																													BgL_res3754z00_5338 =
																														BgL__ortest_1115z00_5322;
																												}
																											else
																												{	/* Ast/let.scm 293 */
																													long
																														BgL_odepthz00_5323;
																													{	/* Ast/let.scm 293 */
																														obj_t
																															BgL_arg1804z00_5324;
																														BgL_arg1804z00_5324
																															=
																															(BgL_oclassz00_5321);
																														BgL_odepthz00_5323 =
																															BGL_CLASS_DEPTH
																															(BgL_arg1804z00_5324);
																													}
																													if (
																														(2L <
																															BgL_odepthz00_5323))
																														{	/* Ast/let.scm 293 */
																															obj_t
																																BgL_arg1802z00_5326;
																															{	/* Ast/let.scm 293 */
																																obj_t
																																	BgL_arg1803z00_5327;
																																BgL_arg1803z00_5327
																																	=
																																	(BgL_oclassz00_5321);
																																BgL_arg1802z00_5326
																																	=
																																	BGL_CLASS_ANCESTORS_REF
																																	(BgL_arg1803z00_5327,
																																	2L);
																															}
																															BgL_res3754z00_5338
																																=
																																(BgL_arg1802z00_5326
																																==
																																BgL_classz00_5305);
																														}
																													else
																														{	/* Ast/let.scm 293 */
																															BgL_res3754z00_5338
																																= ((bool_t) 0);
																														}
																												}
																										}
																									}
																									BgL_test3974z00_9388 =
																										BgL_res3754z00_5338;
																								}
																						}
																					else
																						{	/* Ast/let.scm 293 */
																							BgL_test3974z00_9388 =
																								((bool_t) 0);
																						}
																				}
																				if (BgL_test3974z00_9388)
																					{	/* Ast/let.scm 295 */
																						bool_t BgL_test3979z00_9411;

																						{	/* Ast/let.scm 295 */
																							BgL_nodez00_bglt
																								BgL_arg1955z00_2142;
																							BgL_arg1955z00_2142 =
																								(((BgL_funcallz00_bglt)
																									COBJECT(((BgL_funcallz00_bglt)
																											BgL_valz00_2106)))->
																								BgL_funz00);
																							BgL_test3979z00_9411 =
																								BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00
																								(((obj_t) BgL_arg1955z00_2142),
																								BgL_varsz00_2107);
																						}
																						if (BgL_test3979z00_9411)
																							{	/* Ast/let.scm 296 */
																								obj_t BgL_arg1954z00_2141;

																								BgL_arg1954z00_2141 =
																									(((BgL_funcallz00_bglt)
																										COBJECT((
																												(BgL_funcallz00_bglt)
																												BgL_valz00_2106)))->
																									BgL_argsz00);
																								{
																									obj_t BgL_valz00_9418;

																									BgL_valz00_9418 =
																										BgL_arg1954z00_2141;
																									BgL_valz00_2106 =
																										BgL_valz00_9418;
																									goto
																										BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00;
																								}
																							}
																						else
																							{	/* Ast/let.scm 295 */
																								return ((bool_t) 0);
																							}
																					}
																				else
																					{	/* Ast/let.scm 297 */
																						bool_t BgL_test3980z00_9419;

																						{	/* Ast/let.scm 297 */
																							obj_t BgL_classz00_5339;

																							BgL_classz00_5339 =
																								BGl_externz00zzast_nodez00;
																							if (BGL_OBJECTP(BgL_valz00_2106))
																								{	/* Ast/let.scm 297 */
																									BgL_objectz00_bglt
																										BgL_arg1807z00_5341;
																									BgL_arg1807z00_5341 =
																										(BgL_objectz00_bglt)
																										(BgL_valz00_2106);
																									if (BGL_CONDEXPAND_ISA_ARCH64
																										())
																										{	/* Ast/let.scm 297 */
																											long BgL_idxz00_5347;

																											BgL_idxz00_5347 =
																												BGL_OBJECT_INHERITANCE_NUM
																												(BgL_arg1807z00_5341);
																											BgL_test3980z00_9419 =
																												(VECTOR_REF
																												(BGl_za2inheritancesza2z00zz__objectz00,
																													(BgL_idxz00_5347 +
																														3L)) ==
																												BgL_classz00_5339);
																										}
																									else
																										{	/* Ast/let.scm 297 */
																											bool_t
																												BgL_res3755z00_5372;
																											{	/* Ast/let.scm 297 */
																												obj_t
																													BgL_oclassz00_5355;
																												{	/* Ast/let.scm 297 */
																													obj_t
																														BgL_arg1815z00_5363;
																													long
																														BgL_arg1816z00_5364;
																													BgL_arg1815z00_5363 =
																														(BGl_za2classesza2z00zz__objectz00);
																													{	/* Ast/let.scm 297 */
																														long
																															BgL_arg1817z00_5365;
																														BgL_arg1817z00_5365
																															=
																															BGL_OBJECT_CLASS_NUM
																															(BgL_arg1807z00_5341);
																														BgL_arg1816z00_5364
																															=
																															(BgL_arg1817z00_5365
																															- OBJECT_TYPE);
																													}
																													BgL_oclassz00_5355 =
																														VECTOR_REF
																														(BgL_arg1815z00_5363,
																														BgL_arg1816z00_5364);
																												}
																												{	/* Ast/let.scm 297 */
																													bool_t
																														BgL__ortest_1115z00_5356;
																													BgL__ortest_1115z00_5356
																														=
																														(BgL_classz00_5339
																														==
																														BgL_oclassz00_5355);
																													if (BgL__ortest_1115z00_5356)
																														{	/* Ast/let.scm 297 */
																															BgL_res3755z00_5372
																																=
																																BgL__ortest_1115z00_5356;
																														}
																													else
																														{	/* Ast/let.scm 297 */
																															long
																																BgL_odepthz00_5357;
																															{	/* Ast/let.scm 297 */
																																obj_t
																																	BgL_arg1804z00_5358;
																																BgL_arg1804z00_5358
																																	=
																																	(BgL_oclassz00_5355);
																																BgL_odepthz00_5357
																																	=
																																	BGL_CLASS_DEPTH
																																	(BgL_arg1804z00_5358);
																															}
																															if (
																																(3L <
																																	BgL_odepthz00_5357))
																																{	/* Ast/let.scm 297 */
																																	obj_t
																																		BgL_arg1802z00_5360;
																																	{	/* Ast/let.scm 297 */
																																		obj_t
																																			BgL_arg1803z00_5361;
																																		BgL_arg1803z00_5361
																																			=
																																			(BgL_oclassz00_5355);
																																		BgL_arg1802z00_5360
																																			=
																																			BGL_CLASS_ANCESTORS_REF
																																			(BgL_arg1803z00_5361,
																																			3L);
																																	}
																																	BgL_res3755z00_5372
																																		=
																																		(BgL_arg1802z00_5360
																																		==
																																		BgL_classz00_5339);
																																}
																															else
																																{	/* Ast/let.scm 297 */
																																	BgL_res3755z00_5372
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																												}
																											}
																											BgL_test3980z00_9419 =
																												BgL_res3755z00_5372;
																										}
																								}
																							else
																								{	/* Ast/let.scm 297 */
																									BgL_test3980z00_9419 =
																										((bool_t) 0);
																								}
																						}
																						if (BgL_test3980z00_9419)
																							{	/* Ast/let.scm 299 */
																								obj_t BgL_g1389z00_2145;

																								BgL_g1389z00_2145 =
																									(((BgL_externz00_bglt)
																										COBJECT((
																												(BgL_externz00_bglt)
																												BgL_valz00_2106)))->
																									BgL_exprza2za2);
																								{
																									obj_t BgL_l1387z00_2147;

																									BgL_l1387z00_2147 =
																										BgL_g1389z00_2145;
																								BgL_zc3z04anonymousza31957ze3z87_2148:
																									if (NULLP
																										(BgL_l1387z00_2147))
																										{	/* Ast/let.scm 299 */
																											return ((bool_t) 1);
																										}
																									else
																										{	/* Ast/let.scm 299 */
																											bool_t
																												BgL_test3986z00_9446;
																											{	/* Ast/let.scm 300 */
																												obj_t BgL_ez00_2153;

																												BgL_ez00_2153 =
																													CAR(
																													((obj_t)
																														BgL_l1387z00_2147));
																												BgL_test3986z00_9446 =
																													BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00
																													(BgL_ez00_2153,
																													BgL_varsz00_2107);
																											}
																											if (BgL_test3986z00_9446)
																												{	/* Ast/let.scm 299 */
																													obj_t
																														BgL_arg1959z00_2152;
																													BgL_arg1959z00_2152 =
																														CDR(((obj_t)
																															BgL_l1387z00_2147));
																													{
																														obj_t
																															BgL_l1387z00_9452;
																														BgL_l1387z00_9452 =
																															BgL_arg1959z00_2152;
																														BgL_l1387z00_2147 =
																															BgL_l1387z00_9452;
																														goto
																															BgL_zc3z04anonymousza31957ze3z87_2148;
																													}
																												}
																											else
																												{	/* Ast/let.scm 299 */
																													return ((bool_t) 0);
																												}
																										}
																								}
																							}
																						else
																							{	/* Ast/let.scm 302 */
																								bool_t BgL_test3987z00_9453;

																								{	/* Ast/let.scm 302 */
																									obj_t BgL_classz00_5375;

																									BgL_classz00_5375 =
																										BGl_conditionalz00zzast_nodez00;
																									if (BGL_OBJECTP
																										(BgL_valz00_2106))
																										{	/* Ast/let.scm 302 */
																											BgL_objectz00_bglt
																												BgL_arg1807z00_5377;
																											BgL_arg1807z00_5377 =
																												(BgL_objectz00_bglt)
																												(BgL_valz00_2106);
																											if (BGL_CONDEXPAND_ISA_ARCH64())
																												{	/* Ast/let.scm 302 */
																													long BgL_idxz00_5383;

																													BgL_idxz00_5383 =
																														BGL_OBJECT_INHERITANCE_NUM
																														(BgL_arg1807z00_5377);
																													BgL_test3987z00_9453 =
																														(VECTOR_REF
																														(BGl_za2inheritancesza2z00zz__objectz00,
																															(BgL_idxz00_5383 +
																																3L)) ==
																														BgL_classz00_5375);
																												}
																											else
																												{	/* Ast/let.scm 302 */
																													bool_t
																														BgL_res3756z00_5408;
																													{	/* Ast/let.scm 302 */
																														obj_t
																															BgL_oclassz00_5391;
																														{	/* Ast/let.scm 302 */
																															obj_t
																																BgL_arg1815z00_5399;
																															long
																																BgL_arg1816z00_5400;
																															BgL_arg1815z00_5399
																																=
																																(BGl_za2classesza2z00zz__objectz00);
																															{	/* Ast/let.scm 302 */
																																long
																																	BgL_arg1817z00_5401;
																																BgL_arg1817z00_5401
																																	=
																																	BGL_OBJECT_CLASS_NUM
																																	(BgL_arg1807z00_5377);
																																BgL_arg1816z00_5400
																																	=
																																	(BgL_arg1817z00_5401
																																	-
																																	OBJECT_TYPE);
																															}
																															BgL_oclassz00_5391
																																=
																																VECTOR_REF
																																(BgL_arg1815z00_5399,
																																BgL_arg1816z00_5400);
																														}
																														{	/* Ast/let.scm 302 */
																															bool_t
																																BgL__ortest_1115z00_5392;
																															BgL__ortest_1115z00_5392
																																=
																																(BgL_classz00_5375
																																==
																																BgL_oclassz00_5391);
																															if (BgL__ortest_1115z00_5392)
																																{	/* Ast/let.scm 302 */
																																	BgL_res3756z00_5408
																																		=
																																		BgL__ortest_1115z00_5392;
																																}
																															else
																																{	/* Ast/let.scm 302 */
																																	long
																																		BgL_odepthz00_5393;
																																	{	/* Ast/let.scm 302 */
																																		obj_t
																																			BgL_arg1804z00_5394;
																																		BgL_arg1804z00_5394
																																			=
																																			(BgL_oclassz00_5391);
																																		BgL_odepthz00_5393
																																			=
																																			BGL_CLASS_DEPTH
																																			(BgL_arg1804z00_5394);
																																	}
																																	if (
																																		(3L <
																																			BgL_odepthz00_5393))
																																		{	/* Ast/let.scm 302 */
																																			obj_t
																																				BgL_arg1802z00_5396;
																																			{	/* Ast/let.scm 302 */
																																				obj_t
																																					BgL_arg1803z00_5397;
																																				BgL_arg1803z00_5397
																																					=
																																					(BgL_oclassz00_5391);
																																				BgL_arg1802z00_5396
																																					=
																																					BGL_CLASS_ANCESTORS_REF
																																					(BgL_arg1803z00_5397,
																																					3L);
																																			}
																																			BgL_res3756z00_5408
																																				=
																																				(BgL_arg1802z00_5396
																																				==
																																				BgL_classz00_5375);
																																		}
																																	else
																																		{	/* Ast/let.scm 302 */
																																			BgL_res3756z00_5408
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																														}
																													}
																													BgL_test3987z00_9453 =
																														BgL_res3756z00_5408;
																												}
																										}
																									else
																										{	/* Ast/let.scm 302 */
																											BgL_test3987z00_9453 =
																												((bool_t) 0);
																										}
																								}
																								if (BgL_test3987z00_9453)
																									{	/* Ast/let.scm 304 */
																										bool_t BgL_test3992z00_9476;

																										{	/* Ast/let.scm 304 */
																											BgL_nodez00_bglt
																												BgL_arg1963z00_2161;
																											BgL_arg1963z00_2161 =
																												(((BgL_conditionalz00_bglt) COBJECT(((BgL_conditionalz00_bglt) BgL_valz00_2106)))->BgL_testz00);
																											BgL_test3992z00_9476 =
																												BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00
																												(((obj_t)
																													BgL_arg1963z00_2161),
																												BgL_varsz00_2107);
																										}
																										if (BgL_test3992z00_9476)
																											{	/* Ast/let.scm 305 */
																												bool_t
																													BgL_test3993z00_9481;
																												{	/* Ast/let.scm 305 */
																													BgL_nodez00_bglt
																														BgL_arg1962z00_2160;
																													BgL_arg1962z00_2160 =
																														(((BgL_conditionalz00_bglt) COBJECT(((BgL_conditionalz00_bglt) BgL_valz00_2106)))->BgL_truez00);
																													BgL_test3993z00_9481 =
																														BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00
																														(((obj_t)
																															BgL_arg1962z00_2160),
																														BgL_varsz00_2107);
																												}
																												if (BgL_test3993z00_9481)
																													{	/* Ast/let.scm 306 */
																														BgL_nodez00_bglt
																															BgL_arg1961z00_2159;
																														BgL_arg1961z00_2159
																															=
																															(((BgL_conditionalz00_bglt) COBJECT(((BgL_conditionalz00_bglt) BgL_valz00_2106)))->BgL_falsez00);
																														{
																															obj_t
																																BgL_valz00_9488;
																															BgL_valz00_9488 =
																																((obj_t)
																																BgL_arg1961z00_2159);
																															BgL_valz00_2106 =
																																BgL_valz00_9488;
																															goto
																																BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00;
																														}
																													}
																												else
																													{	/* Ast/let.scm 305 */
																														return ((bool_t) 0);
																													}
																											}
																										else
																											{	/* Ast/let.scm 304 */
																												return ((bool_t) 0);
																											}
																									}
																								else
																									{	/* Ast/let.scm 307 */
																										bool_t BgL_test3994z00_9490;

																										{	/* Ast/let.scm 307 */
																											obj_t BgL_classz00_5409;

																											BgL_classz00_5409 =
																												BGl_setqz00zzast_nodez00;
																											if (BGL_OBJECTP
																												(BgL_valz00_2106))
																												{	/* Ast/let.scm 307 */
																													BgL_objectz00_bglt
																														BgL_arg1807z00_5411;
																													BgL_arg1807z00_5411 =
																														(BgL_objectz00_bglt)
																														(BgL_valz00_2106);
																													if (BGL_CONDEXPAND_ISA_ARCH64())
																														{	/* Ast/let.scm 307 */
																															long
																																BgL_idxz00_5417;
																															BgL_idxz00_5417 =
																																BGL_OBJECT_INHERITANCE_NUM
																																(BgL_arg1807z00_5411);
																															BgL_test3994z00_9490
																																=
																																(VECTOR_REF
																																(BGl_za2inheritancesza2z00zz__objectz00,
																																	(BgL_idxz00_5417
																																		+ 2L)) ==
																																BgL_classz00_5409);
																														}
																													else
																														{	/* Ast/let.scm 307 */
																															bool_t
																																BgL_res3757z00_5442;
																															{	/* Ast/let.scm 307 */
																																obj_t
																																	BgL_oclassz00_5425;
																																{	/* Ast/let.scm 307 */
																																	obj_t
																																		BgL_arg1815z00_5433;
																																	long
																																		BgL_arg1816z00_5434;
																																	BgL_arg1815z00_5433
																																		=
																																		(BGl_za2classesza2z00zz__objectz00);
																																	{	/* Ast/let.scm 307 */
																																		long
																																			BgL_arg1817z00_5435;
																																		BgL_arg1817z00_5435
																																			=
																																			BGL_OBJECT_CLASS_NUM
																																			(BgL_arg1807z00_5411);
																																		BgL_arg1816z00_5434
																																			=
																																			(BgL_arg1817z00_5435
																																			-
																																			OBJECT_TYPE);
																																	}
																																	BgL_oclassz00_5425
																																		=
																																		VECTOR_REF
																																		(BgL_arg1815z00_5433,
																																		BgL_arg1816z00_5434);
																																}
																																{	/* Ast/let.scm 307 */
																																	bool_t
																																		BgL__ortest_1115z00_5426;
																																	BgL__ortest_1115z00_5426
																																		=
																																		(BgL_classz00_5409
																																		==
																																		BgL_oclassz00_5425);
																																	if (BgL__ortest_1115z00_5426)
																																		{	/* Ast/let.scm 307 */
																																			BgL_res3757z00_5442
																																				=
																																				BgL__ortest_1115z00_5426;
																																		}
																																	else
																																		{	/* Ast/let.scm 307 */
																																			long
																																				BgL_odepthz00_5427;
																																			{	/* Ast/let.scm 307 */
																																				obj_t
																																					BgL_arg1804z00_5428;
																																				BgL_arg1804z00_5428
																																					=
																																					(BgL_oclassz00_5425);
																																				BgL_odepthz00_5427
																																					=
																																					BGL_CLASS_DEPTH
																																					(BgL_arg1804z00_5428);
																																			}
																																			if (
																																				(2L <
																																					BgL_odepthz00_5427))
																																				{	/* Ast/let.scm 307 */
																																					obj_t
																																						BgL_arg1802z00_5430;
																																					{	/* Ast/let.scm 307 */
																																						obj_t
																																							BgL_arg1803z00_5431;
																																						BgL_arg1803z00_5431
																																							=
																																							(BgL_oclassz00_5425);
																																						BgL_arg1802z00_5430
																																							=
																																							BGL_CLASS_ANCESTORS_REF
																																							(BgL_arg1803z00_5431,
																																							2L);
																																					}
																																					BgL_res3757z00_5442
																																						=
																																						(BgL_arg1802z00_5430
																																						==
																																						BgL_classz00_5409);
																																				}
																																			else
																																				{	/* Ast/let.scm 307 */
																																					BgL_res3757z00_5442
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																}
																															}
																															BgL_test3994z00_9490
																																=
																																BgL_res3757z00_5442;
																														}
																												}
																											else
																												{	/* Ast/let.scm 307 */
																													BgL_test3994z00_9490 =
																														((bool_t) 0);
																												}
																										}
																										if (BgL_test3994z00_9490)
																											{	/* Ast/let.scm 309 */
																												bool_t
																													BgL_test3999z00_9513;
																												{	/* Ast/let.scm 309 */
																													BgL_varz00_bglt
																														BgL_arg1966z00_2166;
																													BgL_arg1966z00_2166 =
																														(((BgL_setqz00_bglt)
																															COBJECT((
																																	(BgL_setqz00_bglt)
																																	BgL_valz00_2106)))->
																														BgL_varz00);
																													BgL_test3999z00_9513 =
																														BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00
																														(((obj_t)
																															BgL_arg1966z00_2166),
																														BgL_varsz00_2107);
																												}
																												if (BgL_test3999z00_9513)
																													{	/* Ast/let.scm 310 */
																														BgL_nodez00_bglt
																															BgL_arg1965z00_2165;
																														BgL_arg1965z00_2165
																															=
																															(((BgL_setqz00_bglt) COBJECT(((BgL_setqz00_bglt) BgL_valz00_2106)))->BgL_valuez00);
																														{
																															obj_t
																																BgL_valz00_9520;
																															BgL_valz00_9520 =
																																((obj_t)
																																BgL_arg1965z00_2165);
																															BgL_valz00_2106 =
																																BgL_valz00_9520;
																															goto
																																BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00;
																														}
																													}
																												else
																													{	/* Ast/let.scm 309 */
																														return ((bool_t) 0);
																													}
																											}
																										else
																											{	/* Ast/let.scm 311 */
																												bool_t
																													BgL_test4000z00_9522;
																												{	/* Ast/let.scm 311 */
																													obj_t
																														BgL_classz00_5443;
																													BgL_classz00_5443 =
																														BGl_failz00zzast_nodez00;
																													if (BGL_OBJECTP
																														(BgL_valz00_2106))
																														{	/* Ast/let.scm 311 */
																															BgL_objectz00_bglt
																																BgL_arg1807z00_5445;
																															BgL_arg1807z00_5445
																																=
																																(BgL_objectz00_bglt)
																																(BgL_valz00_2106);
																															if (BGL_CONDEXPAND_ISA_ARCH64())
																																{	/* Ast/let.scm 311 */
																																	long
																																		BgL_idxz00_5451;
																																	BgL_idxz00_5451
																																		=
																																		BGL_OBJECT_INHERITANCE_NUM
																																		(BgL_arg1807z00_5445);
																																	BgL_test4000z00_9522
																																		=
																																		(VECTOR_REF
																																		(BGl_za2inheritancesza2z00zz__objectz00,
																																			(BgL_idxz00_5451
																																				+
																																				2L)) ==
																																		BgL_classz00_5443);
																																}
																															else
																																{	/* Ast/let.scm 311 */
																																	bool_t
																																		BgL_res3758z00_5476;
																																	{	/* Ast/let.scm 311 */
																																		obj_t
																																			BgL_oclassz00_5459;
																																		{	/* Ast/let.scm 311 */
																																			obj_t
																																				BgL_arg1815z00_5467;
																																			long
																																				BgL_arg1816z00_5468;
																																			BgL_arg1815z00_5467
																																				=
																																				(BGl_za2classesza2z00zz__objectz00);
																																			{	/* Ast/let.scm 311 */
																																				long
																																					BgL_arg1817z00_5469;
																																				BgL_arg1817z00_5469
																																					=
																																					BGL_OBJECT_CLASS_NUM
																																					(BgL_arg1807z00_5445);
																																				BgL_arg1816z00_5468
																																					=
																																					(BgL_arg1817z00_5469
																																					-
																																					OBJECT_TYPE);
																																			}
																																			BgL_oclassz00_5459
																																				=
																																				VECTOR_REF
																																				(BgL_arg1815z00_5467,
																																				BgL_arg1816z00_5468);
																																		}
																																		{	/* Ast/let.scm 311 */
																																			bool_t
																																				BgL__ortest_1115z00_5460;
																																			BgL__ortest_1115z00_5460
																																				=
																																				(BgL_classz00_5443
																																				==
																																				BgL_oclassz00_5459);
																																			if (BgL__ortest_1115z00_5460)
																																				{	/* Ast/let.scm 311 */
																																					BgL_res3758z00_5476
																																						=
																																						BgL__ortest_1115z00_5460;
																																				}
																																			else
																																				{	/* Ast/let.scm 311 */
																																					long
																																						BgL_odepthz00_5461;
																																					{	/* Ast/let.scm 311 */
																																						obj_t
																																							BgL_arg1804z00_5462;
																																						BgL_arg1804z00_5462
																																							=
																																							(BgL_oclassz00_5459);
																																						BgL_odepthz00_5461
																																							=
																																							BGL_CLASS_DEPTH
																																							(BgL_arg1804z00_5462);
																																					}
																																					if (
																																						(2L
																																							<
																																							BgL_odepthz00_5461))
																																						{	/* Ast/let.scm 311 */
																																							obj_t
																																								BgL_arg1802z00_5464;
																																							{	/* Ast/let.scm 311 */
																																								obj_t
																																									BgL_arg1803z00_5465;
																																								BgL_arg1803z00_5465
																																									=
																																									(BgL_oclassz00_5459);
																																								BgL_arg1802z00_5464
																																									=
																																									BGL_CLASS_ANCESTORS_REF
																																									(BgL_arg1803z00_5465,
																																									2L);
																																							}
																																							BgL_res3758z00_5476
																																								=
																																								(BgL_arg1802z00_5464
																																								==
																																								BgL_classz00_5443);
																																						}
																																					else
																																						{	/* Ast/let.scm 311 */
																																							BgL_res3758z00_5476
																																								=
																																								(
																																								(bool_t)
																																								0);
																																						}
																																				}
																																		}
																																	}
																																	BgL_test4000z00_9522
																																		=
																																		BgL_res3758z00_5476;
																																}
																														}
																													else
																														{	/* Ast/let.scm 311 */
																															BgL_test4000z00_9522
																																= ((bool_t) 0);
																														}
																												}
																												if (BgL_test4000z00_9522)
																													{	/* Ast/let.scm 313 */
																														bool_t
																															BgL_test4005z00_9545;
																														{	/* Ast/let.scm 313 */
																															BgL_nodez00_bglt
																																BgL_arg1970z00_2173;
																															BgL_arg1970z00_2173
																																=
																																(((BgL_failz00_bglt) COBJECT(((BgL_failz00_bglt) BgL_valz00_2106)))->BgL_procz00);
																															BgL_test4005z00_9545
																																=
																																BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00
																																(((obj_t)
																																	BgL_arg1970z00_2173),
																																BgL_varsz00_2107);
																														}
																														if (BgL_test4005z00_9545)
																															{	/* Ast/let.scm 314 */
																																bool_t
																																	BgL_test4006z00_9550;
																																{	/* Ast/let.scm 314 */
																																	BgL_nodez00_bglt
																																		BgL_arg1969z00_2172;
																																	BgL_arg1969z00_2172
																																		=
																																		(((BgL_failz00_bglt) COBJECT(((BgL_failz00_bglt) BgL_valz00_2106)))->BgL_msgz00);
																																	BgL_test4006z00_9550
																																		=
																																		BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00
																																		(((obj_t)
																																			BgL_arg1969z00_2172),
																																		BgL_varsz00_2107);
																																}
																																if (BgL_test4006z00_9550)
																																	{	/* Ast/let.scm 315 */
																																		BgL_nodez00_bglt
																																			BgL_arg1968z00_2171;
																																		BgL_arg1968z00_2171
																																			=
																																			(((BgL_failz00_bglt) COBJECT(((BgL_failz00_bglt) BgL_valz00_2106)))->BgL_objz00);
																																		{
																																			obj_t
																																				BgL_valz00_9557;
																																			BgL_valz00_9557
																																				=
																																				((obj_t)
																																				BgL_arg1968z00_2171);
																																			BgL_valz00_2106
																																				=
																																				BgL_valz00_9557;
																																			goto
																																				BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00;
																																		}
																																	}
																																else
																																	{	/* Ast/let.scm 314 */
																																		return (
																																			(bool_t)
																																			0);
																																	}
																															}
																														else
																															{	/* Ast/let.scm 313 */
																																return ((bool_t)
																																	0);
																															}
																													}
																												else
																													{	/* Ast/let.scm 316 */
																														bool_t
																															BgL_test4007z00_9559;
																														{	/* Ast/let.scm 316 */
																															obj_t
																																BgL_classz00_5477;
																															BgL_classz00_5477
																																=
																																BGl_switchz00zzast_nodez00;
																															if (BGL_OBJECTP
																																(BgL_valz00_2106))
																																{	/* Ast/let.scm 316 */
																																	BgL_objectz00_bglt
																																		BgL_arg1807z00_5479;
																																	BgL_arg1807z00_5479
																																		=
																																		(BgL_objectz00_bglt)
																																		(BgL_valz00_2106);
																																	if (BGL_CONDEXPAND_ISA_ARCH64())
																																		{	/* Ast/let.scm 316 */
																																			long
																																				BgL_idxz00_5485;
																																			BgL_idxz00_5485
																																				=
																																				BGL_OBJECT_INHERITANCE_NUM
																																				(BgL_arg1807z00_5479);
																																			BgL_test4007z00_9559
																																				=
																																				(VECTOR_REF
																																				(BGl_za2inheritancesza2z00zz__objectz00,
																																					(BgL_idxz00_5485
																																						+
																																						3L))
																																				==
																																				BgL_classz00_5477);
																																		}
																																	else
																																		{	/* Ast/let.scm 316 */
																																			bool_t
																																				BgL_res3759z00_5510;
																																			{	/* Ast/let.scm 316 */
																																				obj_t
																																					BgL_oclassz00_5493;
																																				{	/* Ast/let.scm 316 */
																																					obj_t
																																						BgL_arg1815z00_5501;
																																					long
																																						BgL_arg1816z00_5502;
																																					BgL_arg1815z00_5501
																																						=
																																						(BGl_za2classesza2z00zz__objectz00);
																																					{	/* Ast/let.scm 316 */
																																						long
																																							BgL_arg1817z00_5503;
																																						BgL_arg1817z00_5503
																																							=
																																							BGL_OBJECT_CLASS_NUM
																																							(BgL_arg1807z00_5479);
																																						BgL_arg1816z00_5502
																																							=
																																							(BgL_arg1817z00_5503
																																							-
																																							OBJECT_TYPE);
																																					}
																																					BgL_oclassz00_5493
																																						=
																																						VECTOR_REF
																																						(BgL_arg1815z00_5501,
																																						BgL_arg1816z00_5502);
																																				}
																																				{	/* Ast/let.scm 316 */
																																					bool_t
																																						BgL__ortest_1115z00_5494;
																																					BgL__ortest_1115z00_5494
																																						=
																																						(BgL_classz00_5477
																																						==
																																						BgL_oclassz00_5493);
																																					if (BgL__ortest_1115z00_5494)
																																						{	/* Ast/let.scm 316 */
																																							BgL_res3759z00_5510
																																								=
																																								BgL__ortest_1115z00_5494;
																																						}
																																					else
																																						{	/* Ast/let.scm 316 */
																																							long
																																								BgL_odepthz00_5495;
																																							{	/* Ast/let.scm 316 */
																																								obj_t
																																									BgL_arg1804z00_5496;
																																								BgL_arg1804z00_5496
																																									=
																																									(BgL_oclassz00_5493);
																																								BgL_odepthz00_5495
																																									=
																																									BGL_CLASS_DEPTH
																																									(BgL_arg1804z00_5496);
																																							}
																																							if ((3L < BgL_odepthz00_5495))
																																								{	/* Ast/let.scm 316 */
																																									obj_t
																																										BgL_arg1802z00_5498;
																																									{	/* Ast/let.scm 316 */
																																										obj_t
																																											BgL_arg1803z00_5499;
																																										BgL_arg1803z00_5499
																																											=
																																											(BgL_oclassz00_5493);
																																										BgL_arg1802z00_5498
																																											=
																																											BGL_CLASS_ANCESTORS_REF
																																											(BgL_arg1803z00_5499,
																																											3L);
																																									}
																																									BgL_res3759z00_5510
																																										=
																																										(BgL_arg1802z00_5498
																																										==
																																										BgL_classz00_5477);
																																								}
																																							else
																																								{	/* Ast/let.scm 316 */
																																									BgL_res3759z00_5510
																																										=
																																										(
																																										(bool_t)
																																										0);
																																								}
																																						}
																																				}
																																			}
																																			BgL_test4007z00_9559
																																				=
																																				BgL_res3759z00_5510;
																																		}
																																}
																															else
																																{	/* Ast/let.scm 316 */
																																	BgL_test4007z00_9559
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																														if (BgL_test4007z00_9559)
																															{	/* Ast/let.scm 318 */
																																bool_t
																																	BgL_test4012z00_9582;
																																{	/* Ast/let.scm 318 */
																																	BgL_nodez00_bglt
																																		BgL_arg1976z00_2188;
																																	BgL_arg1976z00_2188
																																		=
																																		(((BgL_switchz00_bglt) COBJECT(((BgL_switchz00_bglt) BgL_valz00_2106)))->BgL_testz00);
																																	BgL_test4012z00_9582
																																		=
																																		BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00
																																		(((obj_t)
																																			BgL_arg1976z00_2188),
																																		BgL_varsz00_2107);
																																}
																																if (BgL_test4012z00_9582)
																																	{	/* Ast/let.scm 319 */
																																		obj_t
																																			BgL_g1393z00_2177;
																																		BgL_g1393z00_2177
																																			=
																																			(((BgL_switchz00_bglt) COBJECT(((BgL_switchz00_bglt) BgL_valz00_2106)))->BgL_clausesz00);
																																		{
																																			obj_t
																																				BgL_l1391z00_2179;
																																			BgL_l1391z00_2179
																																				=
																																				BgL_g1393z00_2177;
																																		BgL_zc3z04anonymousza31972ze3z87_2180:
																																			if (NULLP
																																				(BgL_l1391z00_2179))
																																				{	/* Ast/let.scm 319 */
																																					return
																																						(
																																						(bool_t)
																																						1);
																																				}
																																			else
																																				{	/* Ast/let.scm 319 */
																																					bool_t
																																						BgL_test4014z00_9591;
																																					{	/* Ast/let.scm 320 */
																																						obj_t
																																							BgL_clausez00_2185;
																																						BgL_clausez00_2185
																																							=
																																							CAR
																																							(((obj_t) BgL_l1391z00_2179));
																																						{	/* Ast/let.scm 320 */
																																							obj_t
																																								BgL_arg1975z00_2186;
																																							BgL_arg1975z00_2186
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_clausez00_2185));
																																							BgL_test4014z00_9591
																																								=
																																								BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00
																																								(BgL_arg1975z00_2186,
																																								BgL_varsz00_2107);
																																						}
																																					}
																																					if (BgL_test4014z00_9591)
																																						{	/* Ast/let.scm 319 */
																																							obj_t
																																								BgL_arg1974z00_2184;
																																							BgL_arg1974z00_2184
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_l1391z00_2179));
																																							{
																																								obj_t
																																									BgL_l1391z00_9599;
																																								BgL_l1391z00_9599
																																									=
																																									BgL_arg1974z00_2184;
																																								BgL_l1391z00_2179
																																									=
																																									BgL_l1391z00_9599;
																																								goto
																																									BgL_zc3z04anonymousza31972ze3z87_2180;
																																							}
																																						}
																																					else
																																						{	/* Ast/let.scm 319 */
																																							return
																																								(
																																								(bool_t)
																																								0);
																																						}
																																				}
																																		}
																																	}
																																else
																																	{	/* Ast/let.scm 318 */
																																		return (
																																			(bool_t)
																																			0);
																																	}
																															}
																														else
																															{	/* Ast/let.scm 322 */
																																bool_t
																																	BgL_test4015z00_9600;
																																{	/* Ast/let.scm 322 */
																																	obj_t
																																		BgL_classz00_5514;
																																	BgL_classz00_5514
																																		=
																																		BGl_letzd2funzd2zzast_nodez00;
																																	if (BGL_OBJECTP(BgL_valz00_2106))
																																		{	/* Ast/let.scm 322 */
																																			BgL_objectz00_bglt
																																				BgL_arg1807z00_5516;
																																			BgL_arg1807z00_5516
																																				=
																																				(BgL_objectz00_bglt)
																																				(BgL_valz00_2106);
																																			if (BGL_CONDEXPAND_ISA_ARCH64())
																																				{	/* Ast/let.scm 322 */
																																					long
																																						BgL_idxz00_5522;
																																					BgL_idxz00_5522
																																						=
																																						BGL_OBJECT_INHERITANCE_NUM
																																						(BgL_arg1807z00_5516);
																																					BgL_test4015z00_9600
																																						=
																																						(VECTOR_REF
																																						(BGl_za2inheritancesza2z00zz__objectz00,
																																							(BgL_idxz00_5522
																																								+
																																								3L))
																																						==
																																						BgL_classz00_5514);
																																				}
																																			else
																																				{	/* Ast/let.scm 322 */
																																					bool_t
																																						BgL_res3760z00_5547;
																																					{	/* Ast/let.scm 322 */
																																						obj_t
																																							BgL_oclassz00_5530;
																																						{	/* Ast/let.scm 322 */
																																							obj_t
																																								BgL_arg1815z00_5538;
																																							long
																																								BgL_arg1816z00_5539;
																																							BgL_arg1815z00_5538
																																								=
																																								(BGl_za2classesza2z00zz__objectz00);
																																							{	/* Ast/let.scm 322 */
																																								long
																																									BgL_arg1817z00_5540;
																																								BgL_arg1817z00_5540
																																									=
																																									BGL_OBJECT_CLASS_NUM
																																									(BgL_arg1807z00_5516);
																																								BgL_arg1816z00_5539
																																									=
																																									(BgL_arg1817z00_5540
																																									-
																																									OBJECT_TYPE);
																																							}
																																							BgL_oclassz00_5530
																																								=
																																								VECTOR_REF
																																								(BgL_arg1815z00_5538,
																																								BgL_arg1816z00_5539);
																																						}
																																						{	/* Ast/let.scm 322 */
																																							bool_t
																																								BgL__ortest_1115z00_5531;
																																							BgL__ortest_1115z00_5531
																																								=
																																								(BgL_classz00_5514
																																								==
																																								BgL_oclassz00_5530);
																																							if (BgL__ortest_1115z00_5531)
																																								{	/* Ast/let.scm 322 */
																																									BgL_res3760z00_5547
																																										=
																																										BgL__ortest_1115z00_5531;
																																								}
																																							else
																																								{	/* Ast/let.scm 322 */
																																									long
																																										BgL_odepthz00_5532;
																																									{	/* Ast/let.scm 322 */
																																										obj_t
																																											BgL_arg1804z00_5533;
																																										BgL_arg1804z00_5533
																																											=
																																											(BgL_oclassz00_5530);
																																										BgL_odepthz00_5532
																																											=
																																											BGL_CLASS_DEPTH
																																											(BgL_arg1804z00_5533);
																																									}
																																									if ((3L < BgL_odepthz00_5532))
																																										{	/* Ast/let.scm 322 */
																																											obj_t
																																												BgL_arg1802z00_5535;
																																											{	/* Ast/let.scm 322 */
																																												obj_t
																																													BgL_arg1803z00_5536;
																																												BgL_arg1803z00_5536
																																													=
																																													(BgL_oclassz00_5530);
																																												BgL_arg1802z00_5535
																																													=
																																													BGL_CLASS_ANCESTORS_REF
																																													(BgL_arg1803z00_5536,
																																													3L);
																																											}
																																											BgL_res3760z00_5547
																																												=
																																												(BgL_arg1802z00_5535
																																												==
																																												BgL_classz00_5514);
																																										}
																																									else
																																										{	/* Ast/let.scm 322 */
																																											BgL_res3760z00_5547
																																												=
																																												(
																																												(bool_t)
																																												0);
																																										}
																																								}
																																						}
																																					}
																																					BgL_test4015z00_9600
																																						=
																																						BgL_res3760z00_5547;
																																				}
																																		}
																																	else
																																		{	/* Ast/let.scm 322 */
																																			BgL_test4015z00_9600
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																																if (BgL_test4015z00_9600)
																																	{	/* Ast/let.scm 324 */
																																		bool_t
																																			BgL_test4020z00_9623;
																																		{	/* Ast/let.scm 324 */
																																			BgL_nodez00_bglt
																																				BgL_arg1983z00_2204;
																																			BgL_arg1983z00_2204
																																				=
																																				(((BgL_letzd2funzd2_bglt) COBJECT(((BgL_letzd2funzd2_bglt) BgL_valz00_2106)))->BgL_bodyz00);
																																			BgL_test4020z00_9623
																																				=
																																				BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00
																																				(((obj_t) BgL_arg1983z00_2204), BgL_varsz00_2107);
																																		}
																																		if (BgL_test4020z00_9623)
																																			{	/* Ast/let.scm 325 */
																																				obj_t
																																					BgL_g1397z00_2192;
																																				BgL_g1397z00_2192
																																					=
																																					(((BgL_letzd2funzd2_bglt) COBJECT(((BgL_letzd2funzd2_bglt) BgL_valz00_2106)))->BgL_localsz00);
																																				{
																																					obj_t
																																						BgL_l1395z00_2194;
																																					BgL_l1395z00_2194
																																						=
																																						BgL_g1397z00_2192;
																																				BgL_zc3z04anonymousza31978ze3z87_2195:
																																					if (NULLP(BgL_l1395z00_2194))
																																						{	/* Ast/let.scm 325 */
																																							return
																																								(
																																								(bool_t)
																																								1);
																																						}
																																					else
																																						{	/* Ast/let.scm 325 */
																																							bool_t
																																								BgL_test4022z00_9632;
																																							{	/* Ast/let.scm 326 */
																																								obj_t
																																									BgL_fz00_2200;
																																								BgL_fz00_2200
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_l1395z00_2194));
																																								{	/* Ast/let.scm 327 */
																																									obj_t
																																										BgL_arg1981z00_2201;
																																									BgL_arg1981z00_2201
																																										=
																																										(
																																										((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) (((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt) BgL_fz00_2200))))->BgL_valuez00))))->BgL_bodyz00);
																																									BgL_test4022z00_9632
																																										=
																																										BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00
																																										(BgL_arg1981z00_2201,
																																										BgL_varsz00_2107);
																																								}
																																							}
																																							if (BgL_test4022z00_9632)
																																								{	/* Ast/let.scm 325 */
																																									obj_t
																																										BgL_arg1980z00_2199;
																																									BgL_arg1980z00_2199
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_l1395z00_2194));
																																									{
																																										obj_t
																																											BgL_l1395z00_9643;
																																										BgL_l1395z00_9643
																																											=
																																											BgL_arg1980z00_2199;
																																										BgL_l1395z00_2194
																																											=
																																											BgL_l1395z00_9643;
																																										goto
																																											BgL_zc3z04anonymousza31978ze3z87_2195;
																																									}
																																								}
																																							else
																																								{	/* Ast/let.scm 325 */
																																									return
																																										(
																																										(bool_t)
																																										0);
																																								}
																																						}
																																				}
																																			}
																																		else
																																			{	/* Ast/let.scm 324 */
																																				return (
																																					(bool_t)
																																					0);
																																			}
																																	}
																																else
																																	{	/* Ast/let.scm 329 */
																																		bool_t
																																			BgL_test4023z00_9644;
																																		{	/* Ast/let.scm 329 */
																																			obj_t
																																				BgL_classz00_5552;
																																			BgL_classz00_5552
																																				=
																																				BGl_letzd2varzd2zzast_nodez00;
																																			if (BGL_OBJECTP(BgL_valz00_2106))
																																				{	/* Ast/let.scm 329 */
																																					BgL_objectz00_bglt
																																						BgL_arg1807z00_5554;
																																					BgL_arg1807z00_5554
																																						=
																																						(BgL_objectz00_bglt)
																																						(BgL_valz00_2106);
																																					if (BGL_CONDEXPAND_ISA_ARCH64())
																																						{	/* Ast/let.scm 329 */
																																							long
																																								BgL_idxz00_5560;
																																							BgL_idxz00_5560
																																								=
																																								BGL_OBJECT_INHERITANCE_NUM
																																								(BgL_arg1807z00_5554);
																																							BgL_test4023z00_9644
																																								=
																																								(VECTOR_REF
																																								(BGl_za2inheritancesza2z00zz__objectz00,
																																									(BgL_idxz00_5560
																																										+
																																										3L))
																																								==
																																								BgL_classz00_5552);
																																						}
																																					else
																																						{	/* Ast/let.scm 329 */
																																							bool_t
																																								BgL_res3761z00_5585;
																																							{	/* Ast/let.scm 329 */
																																								obj_t
																																									BgL_oclassz00_5568;
																																								{	/* Ast/let.scm 329 */
																																									obj_t
																																										BgL_arg1815z00_5576;
																																									long
																																										BgL_arg1816z00_5577;
																																									BgL_arg1815z00_5576
																																										=
																																										(BGl_za2classesza2z00zz__objectz00);
																																									{	/* Ast/let.scm 329 */
																																										long
																																											BgL_arg1817z00_5578;
																																										BgL_arg1817z00_5578
																																											=
																																											BGL_OBJECT_CLASS_NUM
																																											(BgL_arg1807z00_5554);
																																										BgL_arg1816z00_5577
																																											=
																																											(BgL_arg1817z00_5578
																																											-
																																											OBJECT_TYPE);
																																									}
																																									BgL_oclassz00_5568
																																										=
																																										VECTOR_REF
																																										(BgL_arg1815z00_5576,
																																										BgL_arg1816z00_5577);
																																								}
																																								{	/* Ast/let.scm 329 */
																																									bool_t
																																										BgL__ortest_1115z00_5569;
																																									BgL__ortest_1115z00_5569
																																										=
																																										(BgL_classz00_5552
																																										==
																																										BgL_oclassz00_5568);
																																									if (BgL__ortest_1115z00_5569)
																																										{	/* Ast/let.scm 329 */
																																											BgL_res3761z00_5585
																																												=
																																												BgL__ortest_1115z00_5569;
																																										}
																																									else
																																										{	/* Ast/let.scm 329 */
																																											long
																																												BgL_odepthz00_5570;
																																											{	/* Ast/let.scm 329 */
																																												obj_t
																																													BgL_arg1804z00_5571;
																																												BgL_arg1804z00_5571
																																													=
																																													(BgL_oclassz00_5568);
																																												BgL_odepthz00_5570
																																													=
																																													BGL_CLASS_DEPTH
																																													(BgL_arg1804z00_5571);
																																											}
																																											if ((3L < BgL_odepthz00_5570))
																																												{	/* Ast/let.scm 329 */
																																													obj_t
																																														BgL_arg1802z00_5573;
																																													{	/* Ast/let.scm 329 */
																																														obj_t
																																															BgL_arg1803z00_5574;
																																														BgL_arg1803z00_5574
																																															=
																																															(BgL_oclassz00_5568);
																																														BgL_arg1802z00_5573
																																															=
																																															BGL_CLASS_ANCESTORS_REF
																																															(BgL_arg1803z00_5574,
																																															3L);
																																													}
																																													BgL_res3761z00_5585
																																														=
																																														(BgL_arg1802z00_5573
																																														==
																																														BgL_classz00_5552);
																																												}
																																											else
																																												{	/* Ast/let.scm 329 */
																																													BgL_res3761z00_5585
																																														=
																																														(
																																														(bool_t)
																																														0);
																																												}
																																										}
																																								}
																																							}
																																							BgL_test4023z00_9644
																																								=
																																								BgL_res3761z00_5585;
																																						}
																																				}
																																			else
																																				{	/* Ast/let.scm 329 */
																																					BgL_test4023z00_9644
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																		if (BgL_test4023z00_9644)
																																			{	/* Ast/let.scm 331 */
																																				bool_t
																																					BgL_test4028z00_9667;
																																				{	/* Ast/let.scm 331 */
																																					BgL_nodez00_bglt
																																						BgL_arg1989z00_2219;
																																					BgL_arg1989z00_2219
																																						=
																																						(((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt) BgL_valz00_2106)))->BgL_bodyz00);
																																					BgL_test4028z00_9667
																																						=
																																						BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00
																																						(((obj_t) BgL_arg1989z00_2219), BgL_varsz00_2107);
																																				}
																																				if (BgL_test4028z00_9667)
																																					{	/* Ast/let.scm 332 */
																																						obj_t
																																							BgL_g1401z00_2208;
																																						BgL_g1401z00_2208
																																							=
																																							(((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt) BgL_valz00_2106)))->BgL_bindingsz00);
																																						{
																																							obj_t
																																								BgL_l1399z00_2210;
																																							BgL_l1399z00_2210
																																								=
																																								BgL_g1401z00_2208;
																																						BgL_zc3z04anonymousza31985ze3z87_2211:
																																							if (NULLP(BgL_l1399z00_2210))
																																								{	/* Ast/let.scm 332 */
																																									return
																																										(
																																										(bool_t)
																																										1);
																																								}
																																							else
																																								{	/* Ast/let.scm 332 */
																																									bool_t
																																										BgL_test4030z00_9676;
																																									{	/* Ast/let.scm 333 */
																																										obj_t
																																											BgL_bindingz00_2216;
																																										BgL_bindingz00_2216
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_l1399z00_2210));
																																										{	/* Ast/let.scm 333 */
																																											obj_t
																																												BgL_arg1988z00_2217;
																																											BgL_arg1988z00_2217
																																												=
																																												CDR
																																												(
																																												((obj_t) BgL_bindingz00_2216));
																																											BgL_test4030z00_9676
																																												=
																																												BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00
																																												(BgL_arg1988z00_2217,
																																												BgL_varsz00_2107);
																																										}
																																									}
																																									if (BgL_test4030z00_9676)
																																										{	/* Ast/let.scm 332 */
																																											obj_t
																																												BgL_arg1987z00_2215;
																																											BgL_arg1987z00_2215
																																												=
																																												CDR
																																												(
																																												((obj_t) BgL_l1399z00_2210));
																																											{
																																												obj_t
																																													BgL_l1399z00_9684;
																																												BgL_l1399z00_9684
																																													=
																																													BgL_arg1987z00_2215;
																																												BgL_l1399z00_2210
																																													=
																																													BgL_l1399z00_9684;
																																												goto
																																													BgL_zc3z04anonymousza31985ze3z87_2211;
																																											}
																																										}
																																									else
																																										{	/* Ast/let.scm 332 */
																																											return
																																												(
																																												(bool_t)
																																												0);
																																										}
																																								}
																																						}
																																					}
																																				else
																																					{	/* Ast/let.scm 331 */
																																						return
																																							(
																																							(bool_t)
																																							0);
																																					}
																																			}
																																		else
																																			{	/* Ast/let.scm 335 */
																																				bool_t
																																					BgL_test4031z00_9685;
																																				{	/* Ast/let.scm 335 */
																																					obj_t
																																						BgL_classz00_5589;
																																					BgL_classz00_5589
																																						=
																																						BGl_setzd2exzd2itz00zzast_nodez00;
																																					if (BGL_OBJECTP(BgL_valz00_2106))
																																						{	/* Ast/let.scm 335 */
																																							BgL_objectz00_bglt
																																								BgL_arg1807z00_5591;
																																							BgL_arg1807z00_5591
																																								=
																																								(BgL_objectz00_bglt)
																																								(BgL_valz00_2106);
																																							if (BGL_CONDEXPAND_ISA_ARCH64())
																																								{	/* Ast/let.scm 335 */
																																									long
																																										BgL_idxz00_5597;
																																									BgL_idxz00_5597
																																										=
																																										BGL_OBJECT_INHERITANCE_NUM
																																										(BgL_arg1807z00_5591);
																																									BgL_test4031z00_9685
																																										=
																																										(VECTOR_REF
																																										(BGl_za2inheritancesza2z00zz__objectz00,
																																											(BgL_idxz00_5597
																																												+
																																												2L))
																																										==
																																										BgL_classz00_5589);
																																								}
																																							else
																																								{	/* Ast/let.scm 335 */
																																									bool_t
																																										BgL_res3762z00_5622;
																																									{	/* Ast/let.scm 335 */
																																										obj_t
																																											BgL_oclassz00_5605;
																																										{	/* Ast/let.scm 335 */
																																											obj_t
																																												BgL_arg1815z00_5613;
																																											long
																																												BgL_arg1816z00_5614;
																																											BgL_arg1815z00_5613
																																												=
																																												(BGl_za2classesza2z00zz__objectz00);
																																											{	/* Ast/let.scm 335 */
																																												long
																																													BgL_arg1817z00_5615;
																																												BgL_arg1817z00_5615
																																													=
																																													BGL_OBJECT_CLASS_NUM
																																													(BgL_arg1807z00_5591);
																																												BgL_arg1816z00_5614
																																													=
																																													(BgL_arg1817z00_5615
																																													-
																																													OBJECT_TYPE);
																																											}
																																											BgL_oclassz00_5605
																																												=
																																												VECTOR_REF
																																												(BgL_arg1815z00_5613,
																																												BgL_arg1816z00_5614);
																																										}
																																										{	/* Ast/let.scm 335 */
																																											bool_t
																																												BgL__ortest_1115z00_5606;
																																											BgL__ortest_1115z00_5606
																																												=
																																												(BgL_classz00_5589
																																												==
																																												BgL_oclassz00_5605);
																																											if (BgL__ortest_1115z00_5606)
																																												{	/* Ast/let.scm 335 */
																																													BgL_res3762z00_5622
																																														=
																																														BgL__ortest_1115z00_5606;
																																												}
																																											else
																																												{	/* Ast/let.scm 335 */
																																													long
																																														BgL_odepthz00_5607;
																																													{	/* Ast/let.scm 335 */
																																														obj_t
																																															BgL_arg1804z00_5608;
																																														BgL_arg1804z00_5608
																																															=
																																															(BgL_oclassz00_5605);
																																														BgL_odepthz00_5607
																																															=
																																															BGL_CLASS_DEPTH
																																															(BgL_arg1804z00_5608);
																																													}
																																													if ((2L < BgL_odepthz00_5607))
																																														{	/* Ast/let.scm 335 */
																																															obj_t
																																																BgL_arg1802z00_5610;
																																															{	/* Ast/let.scm 335 */
																																																obj_t
																																																	BgL_arg1803z00_5611;
																																																BgL_arg1803z00_5611
																																																	=
																																																	(BgL_oclassz00_5605);
																																																BgL_arg1802z00_5610
																																																	=
																																																	BGL_CLASS_ANCESTORS_REF
																																																	(BgL_arg1803z00_5611,
																																																	2L);
																																															}
																																															BgL_res3762z00_5622
																																																=
																																																(BgL_arg1802z00_5610
																																																==
																																																BgL_classz00_5589);
																																														}
																																													else
																																														{	/* Ast/let.scm 335 */
																																															BgL_res3762z00_5622
																																																=
																																																(
																																																(bool_t)
																																																0);
																																														}
																																												}
																																										}
																																									}
																																									BgL_test4031z00_9685
																																										=
																																										BgL_res3762z00_5622;
																																								}
																																						}
																																					else
																																						{	/* Ast/let.scm 335 */
																																							BgL_test4031z00_9685
																																								=
																																								(
																																								(bool_t)
																																								0);
																																						}
																																				}
																																				if (BgL_test4031z00_9685)
																																					{	/* Ast/let.scm 337 */
																																						bool_t
																																							BgL_test4036z00_9708;
																																						{	/* Ast/let.scm 337 */
																																							BgL_varz00_bglt
																																								BgL_arg1993z00_2226;
																																							BgL_arg1993z00_2226
																																								=
																																								(
																																								((BgL_setzd2exzd2itz00_bglt) COBJECT(((BgL_setzd2exzd2itz00_bglt) BgL_valz00_2106)))->BgL_varz00);
																																							BgL_test4036z00_9708
																																								=
																																								BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00
																																								(
																																								((obj_t) BgL_arg1993z00_2226), BgL_varsz00_2107);
																																						}
																																						if (BgL_test4036z00_9708)
																																							{	/* Ast/let.scm 338 */
																																								bool_t
																																									BgL_test4037z00_9713;
																																								{	/* Ast/let.scm 338 */
																																									BgL_nodez00_bglt
																																										BgL_arg1992z00_2225;
																																									BgL_arg1992z00_2225
																																										=
																																										(
																																										((BgL_setzd2exzd2itz00_bglt) COBJECT(((BgL_setzd2exzd2itz00_bglt) BgL_valz00_2106)))->BgL_bodyz00);
																																									BgL_test4037z00_9713
																																										=
																																										BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00
																																										(
																																										((obj_t) BgL_arg1992z00_2225), BgL_varsz00_2107);
																																								}
																																								if (BgL_test4037z00_9713)
																																									{	/* Ast/let.scm 339 */
																																										BgL_nodez00_bglt
																																											BgL_arg1991z00_2224;
																																										BgL_arg1991z00_2224
																																											=
																																											(
																																											((BgL_setzd2exzd2itz00_bglt) COBJECT(((BgL_setzd2exzd2itz00_bglt) BgL_valz00_2106)))->BgL_onexitz00);
																																										{
																																											obj_t
																																												BgL_valz00_9720;
																																											BgL_valz00_9720
																																												=
																																												(
																																												(obj_t)
																																												BgL_arg1991z00_2224);
																																											BgL_valz00_2106
																																												=
																																												BgL_valz00_9720;
																																											goto
																																												BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00;
																																										}
																																									}
																																								else
																																									{	/* Ast/let.scm 338 */
																																										return
																																											(
																																											(bool_t)
																																											0);
																																									}
																																							}
																																						else
																																							{	/* Ast/let.scm 337 */
																																								return
																																									(
																																									(bool_t)
																																									0);
																																							}
																																					}
																																				else
																																					{	/* Ast/let.scm 340 */
																																						bool_t
																																							BgL_test4038z00_9722;
																																						{	/* Ast/let.scm 340 */
																																							obj_t
																																								BgL_classz00_5623;
																																							BgL_classz00_5623
																																								=
																																								BGl_jumpzd2exzd2itz00zzast_nodez00;
																																							if (BGL_OBJECTP(BgL_valz00_2106))
																																								{	/* Ast/let.scm 340 */
																																									BgL_objectz00_bglt
																																										BgL_arg1807z00_5625;
																																									BgL_arg1807z00_5625
																																										=
																																										(BgL_objectz00_bglt)
																																										(BgL_valz00_2106);
																																									if (BGL_CONDEXPAND_ISA_ARCH64())
																																										{	/* Ast/let.scm 340 */
																																											long
																																												BgL_idxz00_5631;
																																											BgL_idxz00_5631
																																												=
																																												BGL_OBJECT_INHERITANCE_NUM
																																												(BgL_arg1807z00_5625);
																																											BgL_test4038z00_9722
																																												=
																																												(VECTOR_REF
																																												(BGl_za2inheritancesza2z00zz__objectz00,
																																													(BgL_idxz00_5631
																																														+
																																														2L))
																																												==
																																												BgL_classz00_5623);
																																										}
																																									else
																																										{	/* Ast/let.scm 340 */
																																											bool_t
																																												BgL_res3763z00_5656;
																																											{	/* Ast/let.scm 340 */
																																												obj_t
																																													BgL_oclassz00_5639;
																																												{	/* Ast/let.scm 340 */
																																													obj_t
																																														BgL_arg1815z00_5647;
																																													long
																																														BgL_arg1816z00_5648;
																																													BgL_arg1815z00_5647
																																														=
																																														(BGl_za2classesza2z00zz__objectz00);
																																													{	/* Ast/let.scm 340 */
																																														long
																																															BgL_arg1817z00_5649;
																																														BgL_arg1817z00_5649
																																															=
																																															BGL_OBJECT_CLASS_NUM
																																															(BgL_arg1807z00_5625);
																																														BgL_arg1816z00_5648
																																															=
																																															(BgL_arg1817z00_5649
																																															-
																																															OBJECT_TYPE);
																																													}
																																													BgL_oclassz00_5639
																																														=
																																														VECTOR_REF
																																														(BgL_arg1815z00_5647,
																																														BgL_arg1816z00_5648);
																																												}
																																												{	/* Ast/let.scm 340 */
																																													bool_t
																																														BgL__ortest_1115z00_5640;
																																													BgL__ortest_1115z00_5640
																																														=
																																														(BgL_classz00_5623
																																														==
																																														BgL_oclassz00_5639);
																																													if (BgL__ortest_1115z00_5640)
																																														{	/* Ast/let.scm 340 */
																																															BgL_res3763z00_5656
																																																=
																																																BgL__ortest_1115z00_5640;
																																														}
																																													else
																																														{	/* Ast/let.scm 340 */
																																															long
																																																BgL_odepthz00_5641;
																																															{	/* Ast/let.scm 340 */
																																																obj_t
																																																	BgL_arg1804z00_5642;
																																																BgL_arg1804z00_5642
																																																	=
																																																	(BgL_oclassz00_5639);
																																																BgL_odepthz00_5641
																																																	=
																																																	BGL_CLASS_DEPTH
																																																	(BgL_arg1804z00_5642);
																																															}
																																															if ((2L < BgL_odepthz00_5641))
																																																{	/* Ast/let.scm 340 */
																																																	obj_t
																																																		BgL_arg1802z00_5644;
																																																	{	/* Ast/let.scm 340 */
																																																		obj_t
																																																			BgL_arg1803z00_5645;
																																																		BgL_arg1803z00_5645
																																																			=
																																																			(BgL_oclassz00_5639);
																																																		BgL_arg1802z00_5644
																																																			=
																																																			BGL_CLASS_ANCESTORS_REF
																																																			(BgL_arg1803z00_5645,
																																																			2L);
																																																	}
																																																	BgL_res3763z00_5656
																																																		=
																																																		(BgL_arg1802z00_5644
																																																		==
																																																		BgL_classz00_5623);
																																																}
																																															else
																																																{	/* Ast/let.scm 340 */
																																																	BgL_res3763z00_5656
																																																		=
																																																		(
																																																		(bool_t)
																																																		0);
																																																}
																																														}
																																												}
																																											}
																																											BgL_test4038z00_9722
																																												=
																																												BgL_res3763z00_5656;
																																										}
																																								}
																																							else
																																								{	/* Ast/let.scm 340 */
																																									BgL_test4038z00_9722
																																										=
																																										(
																																										(bool_t)
																																										0);
																																								}
																																						}
																																						if (BgL_test4038z00_9722)
																																							{	/* Ast/let.scm 342 */
																																								bool_t
																																									BgL_test4043z00_9745;
																																								{	/* Ast/let.scm 342 */
																																									BgL_nodez00_bglt
																																										BgL_arg1996z00_2231;
																																									BgL_arg1996z00_2231
																																										=
																																										(
																																										((BgL_jumpzd2exzd2itz00_bglt) COBJECT(((BgL_jumpzd2exzd2itz00_bglt) BgL_valz00_2106)))->BgL_exitz00);
																																									BgL_test4043z00_9745
																																										=
																																										BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00
																																										(
																																										((obj_t) BgL_arg1996z00_2231), BgL_varsz00_2107);
																																								}
																																								if (BgL_test4043z00_9745)
																																									{	/* Ast/let.scm 343 */
																																										BgL_nodez00_bglt
																																											BgL_arg1995z00_2230;
																																										BgL_arg1995z00_2230
																																											=
																																											(
																																											((BgL_jumpzd2exzd2itz00_bglt) COBJECT(((BgL_jumpzd2exzd2itz00_bglt) BgL_valz00_2106)))->BgL_valuez00);
																																										{
																																											obj_t
																																												BgL_valz00_9752;
																																											BgL_valz00_9752
																																												=
																																												(
																																												(obj_t)
																																												BgL_arg1995z00_2230);
																																											BgL_valz00_2106
																																												=
																																												BgL_valz00_9752;
																																											goto
																																												BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00;
																																										}
																																									}
																																								else
																																									{	/* Ast/let.scm 342 */
																																										return
																																											(
																																											(bool_t)
																																											0);
																																									}
																																							}
																																						else
																																							{	/* Ast/let.scm 344 */
																																								bool_t
																																									BgL_test4044z00_9754;
																																								{	/* Ast/let.scm 344 */
																																									obj_t
																																										BgL_classz00_5657;
																																									BgL_classz00_5657
																																										=
																																										BGl_makezd2boxzd2zzast_nodez00;
																																									if (BGL_OBJECTP(BgL_valz00_2106))
																																										{	/* Ast/let.scm 344 */
																																											BgL_objectz00_bglt
																																												BgL_arg1807z00_5659;
																																											BgL_arg1807z00_5659
																																												=
																																												(BgL_objectz00_bglt)
																																												(BgL_valz00_2106);
																																											if (BGL_CONDEXPAND_ISA_ARCH64())
																																												{	/* Ast/let.scm 344 */
																																													long
																																														BgL_idxz00_5665;
																																													BgL_idxz00_5665
																																														=
																																														BGL_OBJECT_INHERITANCE_NUM
																																														(BgL_arg1807z00_5659);
																																													BgL_test4044z00_9754
																																														=
																																														(VECTOR_REF
																																														(BGl_za2inheritancesza2z00zz__objectz00,
																																															(BgL_idxz00_5665
																																																+
																																																3L))
																																														==
																																														BgL_classz00_5657);
																																												}
																																											else
																																												{	/* Ast/let.scm 344 */
																																													bool_t
																																														BgL_res3764z00_5690;
																																													{	/* Ast/let.scm 344 */
																																														obj_t
																																															BgL_oclassz00_5673;
																																														{	/* Ast/let.scm 344 */
																																															obj_t
																																																BgL_arg1815z00_5681;
																																															long
																																																BgL_arg1816z00_5682;
																																															BgL_arg1815z00_5681
																																																=
																																																(BGl_za2classesza2z00zz__objectz00);
																																															{	/* Ast/let.scm 344 */
																																																long
																																																	BgL_arg1817z00_5683;
																																																BgL_arg1817z00_5683
																																																	=
																																																	BGL_OBJECT_CLASS_NUM
																																																	(BgL_arg1807z00_5659);
																																																BgL_arg1816z00_5682
																																																	=
																																																	(BgL_arg1817z00_5683
																																																	-
																																																	OBJECT_TYPE);
																																															}
																																															BgL_oclassz00_5673
																																																=
																																																VECTOR_REF
																																																(BgL_arg1815z00_5681,
																																																BgL_arg1816z00_5682);
																																														}
																																														{	/* Ast/let.scm 344 */
																																															bool_t
																																																BgL__ortest_1115z00_5674;
																																															BgL__ortest_1115z00_5674
																																																=
																																																(BgL_classz00_5657
																																																==
																																																BgL_oclassz00_5673);
																																															if (BgL__ortest_1115z00_5674)
																																																{	/* Ast/let.scm 344 */
																																																	BgL_res3764z00_5690
																																																		=
																																																		BgL__ortest_1115z00_5674;
																																																}
																																															else
																																																{	/* Ast/let.scm 344 */
																																																	long
																																																		BgL_odepthz00_5675;
																																																	{	/* Ast/let.scm 344 */
																																																		obj_t
																																																			BgL_arg1804z00_5676;
																																																		BgL_arg1804z00_5676
																																																			=
																																																			(BgL_oclassz00_5673);
																																																		BgL_odepthz00_5675
																																																			=
																																																			BGL_CLASS_DEPTH
																																																			(BgL_arg1804z00_5676);
																																																	}
																																																	if ((3L < BgL_odepthz00_5675))
																																																		{	/* Ast/let.scm 344 */
																																																			obj_t
																																																				BgL_arg1802z00_5678;
																																																			{	/* Ast/let.scm 344 */
																																																				obj_t
																																																					BgL_arg1803z00_5679;
																																																				BgL_arg1803z00_5679
																																																					=
																																																					(BgL_oclassz00_5673);
																																																				BgL_arg1802z00_5678
																																																					=
																																																					BGL_CLASS_ANCESTORS_REF
																																																					(BgL_arg1803z00_5679,
																																																					3L);
																																																			}
																																																			BgL_res3764z00_5690
																																																				=
																																																				(BgL_arg1802z00_5678
																																																				==
																																																				BgL_classz00_5657);
																																																		}
																																																	else
																																																		{	/* Ast/let.scm 344 */
																																																			BgL_res3764z00_5690
																																																				=
																																																				(
																																																				(bool_t)
																																																				0);
																																																		}
																																																}
																																														}
																																													}
																																													BgL_test4044z00_9754
																																														=
																																														BgL_res3764z00_5690;
																																												}
																																										}
																																									else
																																										{	/* Ast/let.scm 344 */
																																											BgL_test4044z00_9754
																																												=
																																												(
																																												(bool_t)
																																												0);
																																										}
																																								}
																																								if (BgL_test4044z00_9754)
																																									{	/* Ast/let.scm 346 */
																																										BgL_nodez00_bglt
																																											BgL_arg1998z00_2234;
																																										BgL_arg1998z00_2234
																																											=
																																											(
																																											((BgL_makezd2boxzd2_bglt) COBJECT(((BgL_makezd2boxzd2_bglt) BgL_valz00_2106)))->BgL_valuez00);
																																										{
																																											obj_t
																																												BgL_valz00_9779;
																																											BgL_valz00_9779
																																												=
																																												(
																																												(obj_t)
																																												BgL_arg1998z00_2234);
																																											BgL_valz00_2106
																																												=
																																												BgL_valz00_9779;
																																											goto
																																												BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00;
																																										}
																																									}
																																								else
																																									{	/* Ast/let.scm 347 */
																																										bool_t
																																											BgL_test4049z00_9781;
																																										{	/* Ast/let.scm 347 */
																																											obj_t
																																												BgL_classz00_5691;
																																											BgL_classz00_5691
																																												=
																																												BGl_boxzd2refzd2zzast_nodez00;
																																											if (BGL_OBJECTP(BgL_valz00_2106))
																																												{	/* Ast/let.scm 347 */
																																													BgL_objectz00_bglt
																																														BgL_arg1807z00_5693;
																																													BgL_arg1807z00_5693
																																														=
																																														(BgL_objectz00_bglt)
																																														(BgL_valz00_2106);
																																													if (BGL_CONDEXPAND_ISA_ARCH64())
																																														{	/* Ast/let.scm 347 */
																																															long
																																																BgL_idxz00_5699;
																																															BgL_idxz00_5699
																																																=
																																																BGL_OBJECT_INHERITANCE_NUM
																																																(BgL_arg1807z00_5693);
																																															BgL_test4049z00_9781
																																																=
																																																(VECTOR_REF
																																																(BGl_za2inheritancesza2z00zz__objectz00,
																																																	(BgL_idxz00_5699
																																																		+
																																																		3L))
																																																==
																																																BgL_classz00_5691);
																																														}
																																													else
																																														{	/* Ast/let.scm 347 */
																																															bool_t
																																																BgL_res3765z00_5724;
																																															{	/* Ast/let.scm 347 */
																																																obj_t
																																																	BgL_oclassz00_5707;
																																																{	/* Ast/let.scm 347 */
																																																	obj_t
																																																		BgL_arg1815z00_5715;
																																																	long
																																																		BgL_arg1816z00_5716;
																																																	BgL_arg1815z00_5715
																																																		=
																																																		(BGl_za2classesza2z00zz__objectz00);
																																																	{	/* Ast/let.scm 347 */
																																																		long
																																																			BgL_arg1817z00_5717;
																																																		BgL_arg1817z00_5717
																																																			=
																																																			BGL_OBJECT_CLASS_NUM
																																																			(BgL_arg1807z00_5693);
																																																		BgL_arg1816z00_5716
																																																			=
																																																			(BgL_arg1817z00_5717
																																																			-
																																																			OBJECT_TYPE);
																																																	}
																																																	BgL_oclassz00_5707
																																																		=
																																																		VECTOR_REF
																																																		(BgL_arg1815z00_5715,
																																																		BgL_arg1816z00_5716);
																																																}
																																																{	/* Ast/let.scm 347 */
																																																	bool_t
																																																		BgL__ortest_1115z00_5708;
																																																	BgL__ortest_1115z00_5708
																																																		=
																																																		(BgL_classz00_5691
																																																		==
																																																		BgL_oclassz00_5707);
																																																	if (BgL__ortest_1115z00_5708)
																																																		{	/* Ast/let.scm 347 */
																																																			BgL_res3765z00_5724
																																																				=
																																																				BgL__ortest_1115z00_5708;
																																																		}
																																																	else
																																																		{	/* Ast/let.scm 347 */
																																																			long
																																																				BgL_odepthz00_5709;
																																																			{	/* Ast/let.scm 347 */
																																																				obj_t
																																																					BgL_arg1804z00_5710;
																																																				BgL_arg1804z00_5710
																																																					=
																																																					(BgL_oclassz00_5707);
																																																				BgL_odepthz00_5709
																																																					=
																																																					BGL_CLASS_DEPTH
																																																					(BgL_arg1804z00_5710);
																																																			}
																																																			if ((3L < BgL_odepthz00_5709))
																																																				{	/* Ast/let.scm 347 */
																																																					obj_t
																																																						BgL_arg1802z00_5712;
																																																					{	/* Ast/let.scm 347 */
																																																						obj_t
																																																							BgL_arg1803z00_5713;
																																																						BgL_arg1803z00_5713
																																																							=
																																																							(BgL_oclassz00_5707);
																																																						BgL_arg1802z00_5712
																																																							=
																																																							BGL_CLASS_ANCESTORS_REF
																																																							(BgL_arg1803z00_5713,
																																																							3L);
																																																					}
																																																					BgL_res3765z00_5724
																																																						=
																																																						(BgL_arg1802z00_5712
																																																						==
																																																						BgL_classz00_5691);
																																																				}
																																																			else
																																																				{	/* Ast/let.scm 347 */
																																																					BgL_res3765z00_5724
																																																						=
																																																						(
																																																						(bool_t)
																																																						0);
																																																				}
																																																		}
																																																}
																																															}
																																															BgL_test4049z00_9781
																																																=
																																																BgL_res3765z00_5724;
																																														}
																																												}
																																											else
																																												{	/* Ast/let.scm 347 */
																																													BgL_test4049z00_9781
																																														=
																																														(
																																														(bool_t)
																																														0);
																																												}
																																										}
																																										if (BgL_test4049z00_9781)
																																											{	/* Ast/let.scm 349 */
																																												BgL_varz00_bglt
																																													BgL_arg2000z00_2237;
																																												BgL_arg2000z00_2237
																																													=
																																													(
																																													((BgL_boxzd2refzd2_bglt) COBJECT(((BgL_boxzd2refzd2_bglt) BgL_valz00_2106)))->BgL_varz00);
																																												{
																																													obj_t
																																														BgL_valz00_9806;
																																													BgL_valz00_9806
																																														=
																																														(
																																														(obj_t)
																																														BgL_arg2000z00_2237);
																																													BgL_valz00_2106
																																														=
																																														BgL_valz00_9806;
																																													goto
																																														BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00;
																																												}
																																											}
																																										else
																																											{	/* Ast/let.scm 350 */
																																												bool_t
																																													BgL_test4054z00_9808;
																																												{	/* Ast/let.scm 350 */
																																													obj_t
																																														BgL_classz00_5725;
																																													BgL_classz00_5725
																																														=
																																														BGl_boxzd2setz12zc0zzast_nodez00;
																																													if (BGL_OBJECTP(BgL_valz00_2106))
																																														{	/* Ast/let.scm 350 */
																																															BgL_objectz00_bglt
																																																BgL_arg1807z00_5727;
																																															BgL_arg1807z00_5727
																																																=
																																																(BgL_objectz00_bglt)
																																																(BgL_valz00_2106);
																																															if (BGL_CONDEXPAND_ISA_ARCH64())
																																																{	/* Ast/let.scm 350 */
																																																	long
																																																		BgL_idxz00_5733;
																																																	BgL_idxz00_5733
																																																		=
																																																		BGL_OBJECT_INHERITANCE_NUM
																																																		(BgL_arg1807z00_5727);
																																																	BgL_test4054z00_9808
																																																		=
																																																		(VECTOR_REF
																																																		(BGl_za2inheritancesza2z00zz__objectz00,
																																																			(BgL_idxz00_5733
																																																				+
																																																				2L))
																																																		==
																																																		BgL_classz00_5725);
																																																}
																																															else
																																																{	/* Ast/let.scm 350 */
																																																	bool_t
																																																		BgL_res3766z00_5758;
																																																	{	/* Ast/let.scm 350 */
																																																		obj_t
																																																			BgL_oclassz00_5741;
																																																		{	/* Ast/let.scm 350 */
																																																			obj_t
																																																				BgL_arg1815z00_5749;
																																																			long
																																																				BgL_arg1816z00_5750;
																																																			BgL_arg1815z00_5749
																																																				=
																																																				(BGl_za2classesza2z00zz__objectz00);
																																																			{	/* Ast/let.scm 350 */
																																																				long
																																																					BgL_arg1817z00_5751;
																																																				BgL_arg1817z00_5751
																																																					=
																																																					BGL_OBJECT_CLASS_NUM
																																																					(BgL_arg1807z00_5727);
																																																				BgL_arg1816z00_5750
																																																					=
																																																					(BgL_arg1817z00_5751
																																																					-
																																																					OBJECT_TYPE);
																																																			}
																																																			BgL_oclassz00_5741
																																																				=
																																																				VECTOR_REF
																																																				(BgL_arg1815z00_5749,
																																																				BgL_arg1816z00_5750);
																																																		}
																																																		{	/* Ast/let.scm 350 */
																																																			bool_t
																																																				BgL__ortest_1115z00_5742;
																																																			BgL__ortest_1115z00_5742
																																																				=
																																																				(BgL_classz00_5725
																																																				==
																																																				BgL_oclassz00_5741);
																																																			if (BgL__ortest_1115z00_5742)
																																																				{	/* Ast/let.scm 350 */
																																																					BgL_res3766z00_5758
																																																						=
																																																						BgL__ortest_1115z00_5742;
																																																				}
																																																			else
																																																				{	/* Ast/let.scm 350 */
																																																					long
																																																						BgL_odepthz00_5743;
																																																					{	/* Ast/let.scm 350 */
																																																						obj_t
																																																							BgL_arg1804z00_5744;
																																																						BgL_arg1804z00_5744
																																																							=
																																																							(BgL_oclassz00_5741);
																																																						BgL_odepthz00_5743
																																																							=
																																																							BGL_CLASS_DEPTH
																																																							(BgL_arg1804z00_5744);
																																																					}
																																																					if ((2L < BgL_odepthz00_5743))
																																																						{	/* Ast/let.scm 350 */
																																																							obj_t
																																																								BgL_arg1802z00_5746;
																																																							{	/* Ast/let.scm 350 */
																																																								obj_t
																																																									BgL_arg1803z00_5747;
																																																								BgL_arg1803z00_5747
																																																									=
																																																									(BgL_oclassz00_5741);
																																																								BgL_arg1802z00_5746
																																																									=
																																																									BGL_CLASS_ANCESTORS_REF
																																																									(BgL_arg1803z00_5747,
																																																									2L);
																																																							}
																																																							BgL_res3766z00_5758
																																																								=
																																																								(BgL_arg1802z00_5746
																																																								==
																																																								BgL_classz00_5725);
																																																						}
																																																					else
																																																						{	/* Ast/let.scm 350 */
																																																							BgL_res3766z00_5758
																																																								=
																																																								(
																																																								(bool_t)
																																																								0);
																																																						}
																																																				}
																																																		}
																																																	}
																																																	BgL_test4054z00_9808
																																																		=
																																																		BgL_res3766z00_5758;
																																																}
																																														}
																																													else
																																														{	/* Ast/let.scm 350 */
																																															BgL_test4054z00_9808
																																																=
																																																(
																																																(bool_t)
																																																0);
																																														}
																																												}
																																												if (BgL_test4054z00_9808)
																																													{	/* Ast/let.scm 352 */
																																														bool_t
																																															BgL_test4059z00_9831;
																																														{	/* Ast/let.scm 352 */
																																															BgL_varz00_bglt
																																																BgL_arg2003z00_2242;
																																															BgL_arg2003z00_2242
																																																=
																																																(
																																																((BgL_boxzd2setz12zc0_bglt) COBJECT(((BgL_boxzd2setz12zc0_bglt) BgL_valz00_2106)))->BgL_varz00);
																																															BgL_test4059z00_9831
																																																=
																																																BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00
																																																(
																																																((obj_t) BgL_arg2003z00_2242), BgL_varsz00_2107);
																																														}
																																														if (BgL_test4059z00_9831)
																																															{	/* Ast/let.scm 353 */
																																																BgL_nodez00_bglt
																																																	BgL_arg2002z00_2241;
																																																BgL_arg2002z00_2241
																																																	=
																																																	(
																																																	((BgL_boxzd2setz12zc0_bglt) COBJECT(((BgL_boxzd2setz12zc0_bglt) BgL_valz00_2106)))->BgL_valuez00);
																																																{
																																																	obj_t
																																																		BgL_valz00_9838;
																																																	BgL_valz00_9838
																																																		=
																																																		(
																																																		(obj_t)
																																																		BgL_arg2002z00_2241);
																																																	BgL_valz00_2106
																																																		=
																																																		BgL_valz00_9838;
																																																	goto
																																																		BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00;
																																																}
																																															}
																																														else
																																															{	/* Ast/let.scm 352 */
																																																return
																																																	(
																																																	(bool_t)
																																																	0);
																																															}
																																													}
																																												else
																																													{	/* Ast/let.scm 350 */
																																														return
																																															(
																																															(bool_t)
																																															0);
																																													}
																																											}
																																									}
																																							}
																																					}
																																			}
																																	}
																															}
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* safe-let-optim?~0 */
	bool_t BGl_safezd2letzd2optimzf3ze70z14zzast_letz00(obj_t BgL_varsz00_7847,
		BgL_letzd2varzd2_bglt BgL_nodez00_2257)
	{
		{	/* Ast/let.scm 372 */
			if (NULLP(
					(((BgL_letzd2varzd2_bglt) COBJECT(BgL_nodez00_2257))->
						BgL_bindingsz00)))
				{	/* Ast/let.scm 364 */
					return ((bool_t) 1);
				}
			else
				{	/* Ast/let.scm 364 */
					if (NULLP(CDR(
								(((BgL_letzd2varzd2_bglt) COBJECT(BgL_nodez00_2257))->
									BgL_bindingsz00))))
						{	/* Ast/let.scm 367 */
							obj_t BgL_arg2015z00_2265;

							{	/* Ast/let.scm 367 */
								obj_t BgL_pairz00_5763;

								BgL_pairz00_5763 =
									(((BgL_letzd2varzd2_bglt) COBJECT(BgL_nodez00_2257))->
									BgL_bindingsz00);
								BgL_arg2015z00_2265 = CDR(CAR(BgL_pairz00_5763));
							}
							return
								BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00
								(BgL_arg2015z00_2265, BgL_varsz00_7847);
						}
					else
						{	/* Ast/let.scm 369 */
							obj_t BgL_g1409z00_2267;

							BgL_g1409z00_2267 =
								(((BgL_letzd2varzd2_bglt) COBJECT(BgL_nodez00_2257))->
								BgL_bindingsz00);
							{
								obj_t BgL_l1407z00_2269;

								BgL_l1407z00_2269 = BgL_g1409z00_2267;
							BgL_zc3z04anonymousza32017ze3z87_2270:
								if (NULLP(BgL_l1407z00_2269))
									{	/* Ast/let.scm 369 */
										return ((bool_t) 1);
									}
								else
									{	/* Ast/let.scm 369 */
										bool_t BgL_test4063z00_9854;

										{	/* Ast/let.scm 370 */
											obj_t BgL_bz00_2275;

											BgL_bz00_2275 = CAR(((obj_t) BgL_l1407z00_2269));
											if (
												((((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	CAR(
																		((obj_t) BgL_bz00_2275)))))->
														BgL_accessz00) == CNST_TABLE_REF(6)))
												{	/* Ast/let.scm 371 */
													obj_t BgL_arg2024z00_2279;

													BgL_arg2024z00_2279 = CDR(((obj_t) BgL_bz00_2275));
													BgL_test4063z00_9854 =
														BGl_safezd2reczd2valzd2optimzf3ze70zc6zzast_letz00
														(BgL_arg2024z00_2279, BgL_varsz00_7847);
												}
											else
												{	/* Ast/let.scm 370 */
													BgL_test4063z00_9854 = ((bool_t) 0);
												}
										}
										if (BgL_test4063z00_9854)
											{	/* Ast/let.scm 369 */
												obj_t BgL_arg2019z00_2274;

												BgL_arg2019z00_2274 = CDR(((obj_t) BgL_l1407z00_2269));
												{
													obj_t BgL_l1407z00_9869;

													BgL_l1407z00_9869 = BgL_arg2019z00_2274;
													BgL_l1407z00_2269 = BgL_l1407z00_9869;
													goto BgL_zc3z04anonymousza32017ze3z87_2270;
												}
											}
										else
											{	/* Ast/let.scm 369 */
												return ((bool_t) 0);
											}
									}
							}
						}
				}
		}

	}



/* let->labels */
	BgL_letzd2funzd2_bglt BGl_letzd2ze3labelsz31zzast_letz00(obj_t
		BgL_valuezd2bindingszd2_32, BgL_nodez00_bglt BgL_nodez00_33,
		obj_t BgL_sitez00_34)
	{
		{	/* Ast/let.scm 480 */
			{	/* Ast/let.scm 482 */
				obj_t BgL_oldzd2funszd2_2290;
				obj_t BgL_newzd2funszd2_2291;

				if (NULLP(BgL_valuezd2bindingszd2_32))
					{	/* Ast/let.scm 482 */
						BgL_oldzd2funszd2_2290 = BNIL;
					}
				else
					{	/* Ast/let.scm 482 */
						obj_t BgL_head1413z00_2328;

						{	/* Ast/let.scm 482 */
							obj_t BgL_arg2058z00_2340;

							{	/* Ast/let.scm 482 */
								obj_t BgL_pairz00_5918;

								BgL_pairz00_5918 = CAR(((obj_t) BgL_valuezd2bindingszd2_32));
								BgL_arg2058z00_2340 = CAR(BgL_pairz00_5918);
							}
							BgL_head1413z00_2328 = MAKE_YOUNG_PAIR(BgL_arg2058z00_2340, BNIL);
						}
						{	/* Ast/let.scm 482 */
							obj_t BgL_g1416z00_2329;

							BgL_g1416z00_2329 = CDR(((obj_t) BgL_valuezd2bindingszd2_32));
							{
								obj_t BgL_l1411z00_2331;
								obj_t BgL_tail1414z00_2332;

								BgL_l1411z00_2331 = BgL_g1416z00_2329;
								BgL_tail1414z00_2332 = BgL_head1413z00_2328;
							BgL_zc3z04anonymousza32052ze3z87_2333:
								if (NULLP(BgL_l1411z00_2331))
									{	/* Ast/let.scm 482 */
										BgL_oldzd2funszd2_2290 = BgL_head1413z00_2328;
									}
								else
									{	/* Ast/let.scm 482 */
										obj_t BgL_newtail1415z00_2335;

										{	/* Ast/let.scm 482 */
											obj_t BgL_arg2056z00_2337;

											{	/* Ast/let.scm 482 */
												obj_t BgL_pairz00_5921;

												BgL_pairz00_5921 = CAR(((obj_t) BgL_l1411z00_2331));
												BgL_arg2056z00_2337 = CAR(BgL_pairz00_5921);
											}
											BgL_newtail1415z00_2335 =
												MAKE_YOUNG_PAIR(BgL_arg2056z00_2337, BNIL);
										}
										SET_CDR(BgL_tail1414z00_2332, BgL_newtail1415z00_2335);
										{	/* Ast/let.scm 482 */
											obj_t BgL_arg2055z00_2336;

											BgL_arg2055z00_2336 = CDR(((obj_t) BgL_l1411z00_2331));
											{
												obj_t BgL_tail1414z00_9888;
												obj_t BgL_l1411z00_9887;

												BgL_l1411z00_9887 = BgL_arg2055z00_2336;
												BgL_tail1414z00_9888 = BgL_newtail1415z00_2335;
												BgL_tail1414z00_2332 = BgL_tail1414z00_9888;
												BgL_l1411z00_2331 = BgL_l1411z00_9887;
												goto BgL_zc3z04anonymousza32052ze3z87_2333;
											}
										}
									}
							}
						}
					}
				if (NULLP(BgL_valuezd2bindingszd2_32))
					{	/* Ast/let.scm 483 */
						BgL_newzd2funszd2_2291 = BNIL;
					}
				else
					{	/* Ast/let.scm 483 */
						obj_t BgL_head1419z00_2344;

						BgL_head1419z00_2344 = MAKE_YOUNG_PAIR(BNIL, BNIL);
						{
							obj_t BgL_l1417z00_2346;
							obj_t BgL_tail1420z00_2347;

							BgL_l1417z00_2346 = BgL_valuezd2bindingszd2_32;
							BgL_tail1420z00_2347 = BgL_head1419z00_2344;
						BgL_zc3z04anonymousza32061ze3z87_2348:
							if (NULLP(BgL_l1417z00_2346))
								{	/* Ast/let.scm 483 */
									BgL_newzd2funszd2_2291 = CDR(BgL_head1419z00_2344);
								}
							else
								{	/* Ast/let.scm 483 */
									obj_t BgL_newtail1421z00_2350;

									{	/* Ast/let.scm 483 */
										BgL_localz00_bglt BgL_arg2064z00_2352;

										{	/* Ast/let.scm 483 */
											obj_t BgL_bindingz00_2353;

											BgL_bindingz00_2353 = CAR(((obj_t) BgL_l1417z00_2346));
											{	/* Ast/let.scm 484 */
												obj_t BgL_ovarz00_2354;

												BgL_ovarz00_2354 = CAR(((obj_t) BgL_bindingz00_2353));
												{	/* Ast/let.scm 484 */
													obj_t BgL_valz00_2355;

													BgL_valz00_2355 = CDR(((obj_t) BgL_bindingz00_2353));
													{	/* Ast/let.scm 485 */
														obj_t BgL_auxz00_2356;

														{	/* Ast/let.scm 486 */
															obj_t BgL_pairz00_5929;

															BgL_pairz00_5929 =
																(((BgL_letzd2funzd2_bglt) COBJECT(
																		((BgL_letzd2funzd2_bglt)
																			BgL_valz00_2355)))->BgL_localsz00);
															BgL_auxz00_2356 = CAR(BgL_pairz00_5929);
														}
														{	/* Ast/let.scm 486 */
															obj_t BgL_idz00_2357;

															if (
																(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_localz00_bglt)
																					BgL_ovarz00_2354))))->BgL_userzf3zf3))
																{	/* Ast/let.scm 487 */
																	BgL_idz00_2357 =
																		(((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					((BgL_localz00_bglt)
																						BgL_ovarz00_2354))))->BgL_idz00);
																}
															else
																{	/* Ast/let.scm 487 */
																	BgL_idz00_2357 =
																		(((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					((BgL_localz00_bglt)
																						BgL_auxz00_2356))))->BgL_idz00);
																}
															{	/* Ast/let.scm 487 */
																BgL_localz00_bglt BgL_newz00_2358;

																{	/* Ast/let.scm 490 */
																	BgL_typez00_bglt BgL_arg2069z00_2363;

																	BgL_arg2069z00_2363 =
																		(((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					((BgL_localz00_bglt)
																						BgL_auxz00_2356))))->BgL_typez00);
																	BgL_newz00_2358 =
																		BGl_makezd2localzd2svarz00zzast_localz00
																		(BgL_idz00_2357, BgL_arg2069z00_2363);
																}
																{	/* Ast/let.scm 490 */

																	{	/* Ast/let.scm 491 */
																		BgL_valuez00_bglt BgL_arg2065z00_2359;

																		BgL_arg2065z00_2359 =
																			(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						((BgL_localz00_bglt)
																							BgL_auxz00_2356))))->
																			BgL_valuez00);
																		((((BgL_variablez00_bglt)
																					COBJECT(((BgL_variablez00_bglt)
																							BgL_newz00_2358)))->
																				BgL_valuez00) =
																			((BgL_valuez00_bglt) BgL_arg2065z00_2359),
																			BUNSPEC);
																	}
																	{	/* Ast/let.scm 492 */
																		bool_t BgL_arg2067z00_2360;

																		{	/* Ast/let.scm 492 */
																			bool_t BgL__ortest_1180z00_2361;

																			BgL__ortest_1180z00_2361 =
																				(((BgL_variablez00_bglt) COBJECT(
																						((BgL_variablez00_bglt)
																							((BgL_localz00_bglt)
																								BgL_auxz00_2356))))->
																				BgL_userzf3zf3);
																			if (BgL__ortest_1180z00_2361)
																				{	/* Ast/let.scm 492 */
																					BgL_arg2067z00_2360 =
																						BgL__ortest_1180z00_2361;
																				}
																			else
																				{	/* Ast/let.scm 492 */
																					BgL_arg2067z00_2360 =
																						(((BgL_variablez00_bglt) COBJECT(
																								((BgL_variablez00_bglt)
																									((BgL_localz00_bglt)
																										BgL_ovarz00_2354))))->
																						BgL_userzf3zf3);
																				}
																		}
																		((((BgL_variablez00_bglt) COBJECT(
																						((BgL_variablez00_bglt)
																							BgL_newz00_2358)))->
																				BgL_userzf3zf3) =
																			((bool_t) BgL_arg2067z00_2360), BUNSPEC);
																	}
																	{	/* Ast/let.scm 494 */
																		obj_t BgL_arg2068z00_2362;

																		BgL_arg2068z00_2362 =
																			(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						((BgL_localz00_bglt)
																							BgL_auxz00_2356))))->BgL_namez00);
																		((((BgL_variablez00_bglt)
																					COBJECT(((BgL_variablez00_bglt)
																							BgL_newz00_2358)))->BgL_namez00) =
																			((obj_t) BgL_arg2068z00_2362), BUNSPEC);
																	}
																	BgL_arg2064z00_2352 = BgL_newz00_2358;
																}
															}
														}
													}
												}
											}
										}
										BgL_newtail1421z00_2350 =
											MAKE_YOUNG_PAIR(((obj_t) BgL_arg2064z00_2352), BNIL);
									}
									SET_CDR(BgL_tail1420z00_2347, BgL_newtail1421z00_2350);
									{	/* Ast/let.scm 483 */
										obj_t BgL_arg2063z00_2351;

										BgL_arg2063z00_2351 = CDR(((obj_t) BgL_l1417z00_2346));
										{
											obj_t BgL_tail1420z00_9943;
											obj_t BgL_l1417z00_9942;

											BgL_l1417z00_9942 = BgL_arg2063z00_2351;
											BgL_tail1420z00_9943 = BgL_newtail1421z00_2350;
											BgL_tail1420z00_2347 = BgL_tail1420z00_9943;
											BgL_l1417z00_2346 = BgL_l1417z00_9942;
											goto BgL_zc3z04anonymousza32061ze3z87_2348;
										}
									}
								}
						}
					}
				{
					obj_t BgL_vbindingsz00_2293;
					obj_t BgL_nvarsz00_2294;

					BgL_vbindingsz00_2293 = BgL_valuezd2bindingszd2_32;
					BgL_nvarsz00_2294 = BgL_newzd2funszd2_2291;
				BgL_zc3z04anonymousza32031ze3z87_2295:
					if (NULLP(BgL_vbindingsz00_2293))
						{	/* Ast/let.scm 502 */
							BgL_nodez00_bglt BgL_bodyz00_2297;

							BgL_bodyz00_2297 =
								BGl_substitutez12z12zzast_substitutez00(BgL_oldzd2funszd2_2290,
								BgL_newzd2funszd2_2291, BgL_nodez00_33, BgL_sitez00_34);
							{	/* Ast/let.scm 502 */
								obj_t BgL_funsz00_2298;

								BgL_funsz00_2298 = bgl_reverse_bang(BgL_newzd2funszd2_2291);
								{	/* Ast/let.scm 503 */
									obj_t BgL_locz00_2299;

									{	/* Ast/let.scm 504 */
										bool_t BgL_test4072z00_9948;

										if (NULLP(BgL_funsz00_2298))
											{	/* Ast/let.scm 504 */
												BgL_test4072z00_9948 = ((bool_t) 0);
											}
										else
											{	/* Ast/let.scm 505 */
												BgL_valuez00_bglt BgL_arg2042z00_2311;

												BgL_arg2042z00_2311 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_localz00_bglt)
																	CAR(BgL_funsz00_2298)))))->BgL_valuez00);
												{	/* Ast/let.scm 505 */
													obj_t BgL_classz00_5947;

													BgL_classz00_5947 = BGl_sfunz00zzast_varz00;
													{	/* Ast/let.scm 505 */
														BgL_objectz00_bglt BgL_arg1807z00_5949;

														{	/* Ast/let.scm 505 */
															obj_t BgL_tmpz00_9955;

															BgL_tmpz00_9955 =
																((obj_t)
																((BgL_objectz00_bglt) BgL_arg2042z00_2311));
															BgL_arg1807z00_5949 =
																(BgL_objectz00_bglt) (BgL_tmpz00_9955);
														}
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Ast/let.scm 505 */
																long BgL_idxz00_5955;

																BgL_idxz00_5955 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_5949);
																BgL_test4072z00_9948 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_5955 + 3L)) ==
																	BgL_classz00_5947);
															}
														else
															{	/* Ast/let.scm 505 */
																bool_t BgL_res3771z00_5980;

																{	/* Ast/let.scm 505 */
																	obj_t BgL_oclassz00_5963;

																	{	/* Ast/let.scm 505 */
																		obj_t BgL_arg1815z00_5971;
																		long BgL_arg1816z00_5972;

																		BgL_arg1815z00_5971 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Ast/let.scm 505 */
																			long BgL_arg1817z00_5973;

																			BgL_arg1817z00_5973 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_5949);
																			BgL_arg1816z00_5972 =
																				(BgL_arg1817z00_5973 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_5963 =
																			VECTOR_REF(BgL_arg1815z00_5971,
																			BgL_arg1816z00_5972);
																	}
																	{	/* Ast/let.scm 505 */
																		bool_t BgL__ortest_1115z00_5964;

																		BgL__ortest_1115z00_5964 =
																			(BgL_classz00_5947 == BgL_oclassz00_5963);
																		if (BgL__ortest_1115z00_5964)
																			{	/* Ast/let.scm 505 */
																				BgL_res3771z00_5980 =
																					BgL__ortest_1115z00_5964;
																			}
																		else
																			{	/* Ast/let.scm 505 */
																				long BgL_odepthz00_5965;

																				{	/* Ast/let.scm 505 */
																					obj_t BgL_arg1804z00_5966;

																					BgL_arg1804z00_5966 =
																						(BgL_oclassz00_5963);
																					BgL_odepthz00_5965 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_5966);
																				}
																				if ((3L < BgL_odepthz00_5965))
																					{	/* Ast/let.scm 505 */
																						obj_t BgL_arg1802z00_5968;

																						{	/* Ast/let.scm 505 */
																							obj_t BgL_arg1803z00_5969;

																							BgL_arg1803z00_5969 =
																								(BgL_oclassz00_5963);
																							BgL_arg1802z00_5968 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_5969, 3L);
																						}
																						BgL_res3771z00_5980 =
																							(BgL_arg1802z00_5968 ==
																							BgL_classz00_5947);
																					}
																				else
																					{	/* Ast/let.scm 505 */
																						BgL_res3771z00_5980 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test4072z00_9948 = BgL_res3771z00_5980;
															}
													}
												}
											}
										if (BgL_test4072z00_9948)
											{	/* Ast/let.scm 504 */
												BgL_locz00_2299 =
													(((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt)
																(((BgL_sfunz00_bglt) COBJECT(
																			((BgL_sfunz00_bglt)
																				(((BgL_variablez00_bglt) COBJECT(
																							((BgL_variablez00_bglt)
																								((BgL_localz00_bglt)
																									CAR(BgL_funsz00_2298)))))->
																					BgL_valuez00))))->BgL_bodyz00))))->
													BgL_locz00);
											}
										else
											{	/* Ast/let.scm 504 */
												BgL_locz00_2299 =
													(((BgL_nodez00_bglt) COBJECT(BgL_nodez00_33))->
													BgL_locz00);
											}
									}
									{	/* Ast/let.scm 504 */

										{	/* Ast/let.scm 508 */
											BgL_letzd2funzd2_bglt BgL_new1183z00_2300;

											{	/* Ast/let.scm 509 */
												BgL_letzd2funzd2_bglt BgL_new1182z00_2301;

												BgL_new1182z00_2301 =
													((BgL_letzd2funzd2_bglt)
													BOBJECT(GC_MALLOC(sizeof(struct
																BgL_letzd2funzd2_bgl))));
												{	/* Ast/let.scm 509 */
													long BgL_arg2033z00_2302;

													BgL_arg2033z00_2302 =
														BGL_CLASS_NUM(BGl_letzd2funzd2zzast_nodez00);
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt) BgL_new1182z00_2301),
														BgL_arg2033z00_2302);
												}
												{	/* Ast/let.scm 509 */
													BgL_objectz00_bglt BgL_tmpz00_9991;

													BgL_tmpz00_9991 =
														((BgL_objectz00_bglt) BgL_new1182z00_2301);
													BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9991, BFALSE);
												}
												((BgL_objectz00_bglt) BgL_new1182z00_2301);
												BgL_new1183z00_2300 = BgL_new1182z00_2301;
											}
											((((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt) BgL_new1183z00_2300)))->
													BgL_locz00) = ((obj_t) BgL_locz00_2299), BUNSPEC);
											((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																BgL_new1183z00_2300)))->BgL_typez00) =
												((BgL_typez00_bglt) (((BgL_nodez00_bglt)
															COBJECT(BgL_nodez00_33))->BgL_typez00)), BUNSPEC);
											((((BgL_nodezf2effectzf2_bglt)
														COBJECT(((BgL_nodezf2effectzf2_bglt)
																BgL_new1183z00_2300)))->BgL_sidezd2effectzd2) =
												((obj_t) BUNSPEC), BUNSPEC);
											((((BgL_nodezf2effectzf2_bglt)
														COBJECT(((BgL_nodezf2effectzf2_bglt)
																BgL_new1183z00_2300)))->BgL_keyz00) =
												((obj_t) BINT(-1L)), BUNSPEC);
											((((BgL_letzd2funzd2_bglt) COBJECT(BgL_new1183z00_2300))->
													BgL_localsz00) = ((obj_t) BgL_funsz00_2298), BUNSPEC);
											((((BgL_letzd2funzd2_bglt) COBJECT(BgL_new1183z00_2300))->
													BgL_bodyz00) =
												((BgL_nodez00_bglt) BgL_bodyz00_2297), BUNSPEC);
											return BgL_new1183z00_2300;
										}
									}
								}
							}
						}
					else
						{	/* Ast/let.scm 514 */
							obj_t BgL_bindingz00_2313;

							BgL_bindingz00_2313 = CAR(((obj_t) BgL_vbindingsz00_2293));
							{	/* Ast/let.scm 514 */
								obj_t BgL_nvarz00_2314;

								BgL_nvarz00_2314 = CAR(((obj_t) BgL_nvarsz00_2294));
								{	/* Ast/let.scm 515 */
									BgL_valuez00_bglt BgL_sfunz00_2315;

									BgL_sfunz00_2315 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_nvarz00_2314))))->
										BgL_valuez00);
									{	/* Ast/let.scm 516 */
										obj_t BgL_bodyz00_2316;

										BgL_bodyz00_2316 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_sfunz00_2315)))->BgL_bodyz00);
										{	/* Ast/let.scm 517 */
											obj_t BgL_valz00_2317;

											BgL_valz00_2317 = CDR(((obj_t) BgL_bindingz00_2313));
											{	/* Ast/let.scm 518 */
												obj_t BgL_auxz00_2318;

												{	/* Ast/let.scm 519 */
													obj_t BgL_pairz00_5997;

													BgL_pairz00_5997 =
														(((BgL_letzd2funzd2_bglt) COBJECT(
																((BgL_letzd2funzd2_bglt) BgL_valz00_2317)))->
														BgL_localsz00);
													BgL_auxz00_2318 = CAR(BgL_pairz00_5997);
												}
												{	/* Ast/let.scm 519 */

													{	/* Ast/let.scm 521 */
														BgL_nodez00_bglt BgL_arg2045z00_2319;

														{	/* Ast/let.scm 521 */
															obj_t BgL_arg2046z00_2320;
															obj_t BgL_arg2047z00_2321;

															BgL_arg2046z00_2320 =
																MAKE_YOUNG_PAIR(BgL_auxz00_2318,
																BgL_oldzd2funszd2_2290);
															BgL_arg2047z00_2321 =
																MAKE_YOUNG_PAIR(BgL_nvarz00_2314,
																BgL_newzd2funszd2_2291);
															BgL_arg2045z00_2319 =
																BGl_substitutez12z12zzast_substitutez00
																(BgL_arg2046z00_2320, BgL_arg2047z00_2321,
																((BgL_nodez00_bglt) BgL_bodyz00_2316),
																CNST_TABLE_REF(1));
														}
														((((BgL_sfunz00_bglt) COBJECT(
																		((BgL_sfunz00_bglt) BgL_sfunz00_2315)))->
																BgL_bodyz00) =
															((obj_t) ((obj_t) BgL_arg2045z00_2319)), BUNSPEC);
													}
													{	/* Ast/let.scm 526 */
														obj_t BgL_arg2048z00_2322;
														obj_t BgL_arg2049z00_2323;

														BgL_arg2048z00_2322 =
															CDR(((obj_t) BgL_vbindingsz00_2293));
														BgL_arg2049z00_2323 =
															CDR(((obj_t) BgL_nvarsz00_2294));
														{
															obj_t BgL_nvarsz00_10034;
															obj_t BgL_vbindingsz00_10033;

															BgL_vbindingsz00_10033 = BgL_arg2048z00_2322;
															BgL_nvarsz00_10034 = BgL_arg2049z00_2323;
															BgL_nvarsz00_2294 = BgL_nvarsz00_10034;
															BgL_vbindingsz00_2293 = BgL_vbindingsz00_10033;
															goto BgL_zc3z04anonymousza32031ze3z87_2295;
														}
													}
												}
											}
										}
									}
								}
							}
						}
				}
			}
		}

	}



/* letrec*->node */
	BGL_EXPORTED_DEF BgL_nodez00_bglt BGl_letrecza2zd2ze3nodez93zzast_letz00(obj_t
		BgL_sexpz00_35, obj_t BgL_stackz00_36, obj_t BgL_locz00_37,
		obj_t BgL_sitez00_38)
	{
		{	/* Ast/let.scm 560 */
			{	/* Ast/let.scm 563 */
				obj_t BgL_stage1z00_7762;
				obj_t BgL_stage2z00_7761;
				obj_t BgL_stage3z00_7759;
				obj_t BgL_stage4z00_7757;
				obj_t BgL_stage5z00_7756;
				obj_t BgL_stage7z00_7755;

				{
					int BgL_tmpz00_10035;

					BgL_tmpz00_10035 = (int) (8L);
					BgL_stage1z00_7762 =
						MAKE_L_PROCEDURE(BGl_z62stage1z62zzast_letz00, BgL_tmpz00_10035);
				}
				{
					int BgL_tmpz00_10038;

					BgL_tmpz00_10038 = (int) (7L);
					BgL_stage2z00_7761 = MAKE_EL_PROCEDURE(BgL_tmpz00_10038);
				}
				{
					int BgL_tmpz00_10041;

					BgL_tmpz00_10041 = (int) (6L);
					BgL_stage3z00_7759 =
						MAKE_L_PROCEDURE(BGl_z62stage3z62zzast_letz00, BgL_tmpz00_10041);
				}
				{
					int BgL_tmpz00_10044;

					BgL_tmpz00_10044 = (int) (5L);
					BgL_stage4z00_7757 =
						MAKE_L_PROCEDURE(BGl_z62stage4z62zzast_letz00, BgL_tmpz00_10044);
				}
				{
					int BgL_tmpz00_10047;

					BgL_tmpz00_10047 = (int) (4L);
					BgL_stage5z00_7756 = MAKE_EL_PROCEDURE(BgL_tmpz00_10047);
				}
				{
					int BgL_tmpz00_10050;

					BgL_tmpz00_10050 = (int) (3L);
					BgL_stage7z00_7755 = MAKE_EL_PROCEDURE(BgL_tmpz00_10050);
				}
				PROCEDURE_L_SET(BgL_stage1z00_7762, (int) (0L), BgL_stage7z00_7755);
				PROCEDURE_L_SET(BgL_stage1z00_7762, (int) (1L), BgL_stage5z00_7756);
				PROCEDURE_L_SET(BgL_stage1z00_7762, (int) (2L), BgL_stage4z00_7757);
				PROCEDURE_L_SET(BgL_stage1z00_7762, (int) (3L), BgL_stage3z00_7759);
				PROCEDURE_L_SET(BgL_stage1z00_7762, (int) (4L), BgL_stackz00_36);
				PROCEDURE_L_SET(BgL_stage1z00_7762, (int) (5L), BgL_locz00_37);
				PROCEDURE_L_SET(BgL_stage1z00_7762, (int) (6L), BgL_sitez00_38);
				PROCEDURE_L_SET(BgL_stage1z00_7762, (int) (7L), BgL_stage2z00_7761);
				PROCEDURE_EL_SET(BgL_stage2z00_7761, (int) (0L), BgL_stage7z00_7755);
				PROCEDURE_EL_SET(BgL_stage2z00_7761, (int) (1L), BgL_stackz00_36);
				PROCEDURE_EL_SET(BgL_stage2z00_7761, (int) (2L), BgL_locz00_37);
				PROCEDURE_EL_SET(BgL_stage2z00_7761, (int) (3L), BgL_sitez00_38);
				PROCEDURE_EL_SET(BgL_stage2z00_7761, (int) (4L), BgL_stage5z00_7756);
				PROCEDURE_EL_SET(BgL_stage2z00_7761, (int) (5L), BgL_stage4z00_7757);
				PROCEDURE_EL_SET(BgL_stage2z00_7761, (int) (6L), BgL_stage3z00_7759);
				PROCEDURE_L_SET(BgL_stage3z00_7759, (int) (0L), BgL_stage7z00_7755);
				PROCEDURE_L_SET(BgL_stage3z00_7759, (int) (1L), BgL_stackz00_36);
				PROCEDURE_L_SET(BgL_stage3z00_7759, (int) (2L), BgL_locz00_37);
				PROCEDURE_L_SET(BgL_stage3z00_7759, (int) (3L), BgL_sitez00_38);
				PROCEDURE_L_SET(BgL_stage3z00_7759, (int) (4L), BgL_stage5z00_7756);
				PROCEDURE_L_SET(BgL_stage3z00_7759, (int) (5L), BgL_stage4z00_7757);
				PROCEDURE_L_SET(BgL_stage4z00_7757, (int) (0L), BgL_stage7z00_7755);
				PROCEDURE_L_SET(BgL_stage4z00_7757, (int) (1L), BgL_stackz00_36);
				PROCEDURE_L_SET(BgL_stage4z00_7757, (int) (2L), BgL_locz00_37);
				PROCEDURE_L_SET(BgL_stage4z00_7757, (int) (3L), BgL_sitez00_38);
				PROCEDURE_L_SET(BgL_stage4z00_7757, (int) (4L), BgL_stage5z00_7756);
				PROCEDURE_EL_SET(BgL_stage5z00_7756, (int) (0L), BgL_stage7z00_7755);
				PROCEDURE_EL_SET(BgL_stage5z00_7756, (int) (1L), BgL_stackz00_36);
				PROCEDURE_EL_SET(BgL_stage5z00_7756, (int) (2L), BgL_locz00_37);
				PROCEDURE_EL_SET(BgL_stage5z00_7756, (int) (3L), BgL_sitez00_38);
				PROCEDURE_EL_SET(BgL_stage7z00_7755, (int) (0L), BgL_locz00_37);
				PROCEDURE_EL_SET(BgL_stage7z00_7755, (int) (1L), BgL_stackz00_36);
				PROCEDURE_EL_SET(BgL_stage7z00_7755, (int) (2L), BgL_sitez00_38);
				{
					obj_t BgL_bz00_2473;
					obj_t BgL_vz00_2474;
					obj_t BgL_varsz00_2475;
					obj_t BgL_ebindingsz00_4170;
					obj_t BgL_bodyz00_4171;
					obj_t BgL_bindingsz00_4205;
					obj_t BgL_bodyz00_4206;
					obj_t BgL_expz00_4256;

					{
						obj_t BgL_bodyz00_2405;
						obj_t BgL_bindingsz00_2407;
						obj_t BgL_bindingz00_2408;
						obj_t BgL_bodyz00_2409;
						obj_t BgL_bindingsz00_2411;
						obj_t BgL_bodyz00_2412;

						if (PAIRP(BgL_sexpz00_35))
							{	/* Ast/let.scm 1198 */
								obj_t BgL_cdrzd22817zd2_2417;

								BgL_cdrzd22817zd2_2417 = CDR(((obj_t) BgL_sexpz00_35));
								if ((CAR(((obj_t) BgL_sexpz00_35)) == CNST_TABLE_REF(4)))
									{	/* Ast/let.scm 1198 */
										if (PAIRP(BgL_cdrzd22817zd2_2417))
											{	/* Ast/let.scm 1198 */
												if (NULLP(CAR(BgL_cdrzd22817zd2_2417)))
													{	/* Ast/let.scm 1198 */
														BgL_bodyz00_2405 = CDR(BgL_cdrzd22817zd2_2417);
														{	/* Ast/let.scm 1202 */
															obj_t BgL_arg2093z00_2442;

															{	/* Ast/let.scm 1202 */
																obj_t BgL_arg2094z00_2443;

																{	/* Ast/let.scm 1202 */
																	obj_t BgL_arg2095z00_2444;

																	BgL_arg2095z00_2444 =
																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																		(BgL_bodyz00_2405, BNIL);
																	BgL_arg2094z00_2443 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(9),
																		BgL_arg2095z00_2444);
																}
																BgL_arg2093z00_2442 =
																	BGl_epairifyzd2propagatezd2locz00zztools_miscz00
																	(BgL_arg2094z00_2443, BgL_locz00_37);
															}
															BGL_TAIL return
																BGl_sexpzd2ze3nodez31zzast_sexpz00
																(BgL_arg2093z00_2442, BgL_stackz00_36,
																BgL_locz00_37, BgL_sitez00_38);
														}
													}
												else
													{	/* Ast/let.scm 1198 */
														obj_t BgL_carzd22833zd2_2425;

														BgL_carzd22833zd2_2425 =
															CAR(((obj_t) BgL_cdrzd22817zd2_2417));
														if (PAIRP(BgL_carzd22833zd2_2425))
															{	/* Ast/let.scm 1198 */
																if (NULLP(CDR(BgL_carzd22833zd2_2425)))
																	{	/* Ast/let.scm 1198 */
																		obj_t BgL_arg2083z00_2429;
																		obj_t BgL_arg2084z00_2430;

																		BgL_arg2083z00_2429 =
																			CAR(BgL_carzd22833zd2_2425);
																		BgL_arg2084z00_2430 =
																			CDR(((obj_t) BgL_cdrzd22817zd2_2417));
																		BgL_bindingsz00_2407 =
																			BgL_carzd22833zd2_2425;
																		BgL_bindingz00_2408 = BgL_arg2083z00_2429;
																		BgL_bodyz00_2409 = BgL_arg2084z00_2430;
																		{	/* Ast/let.scm 1206 */
																			obj_t BgL_arg2096z00_2445;

																			{	/* Ast/let.scm 1206 */
																				obj_t BgL_arg2097z00_2446;

																				{	/* Ast/let.scm 1206 */
																					obj_t BgL_arg2098z00_2447;

																					BgL_arg2098z00_2447 =
																						MAKE_YOUNG_PAIR
																						(BgL_bindingsz00_2407,
																						BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																						(BgL_bodyz00_2409, BNIL));
																					BgL_arg2097z00_2446 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
																						BgL_arg2098z00_2447);
																				}
																				BgL_arg2096z00_2445 =
																					BGl_epairifyzd2propagatezd2locz00zztools_miscz00
																					(BgL_arg2097z00_2446, BgL_locz00_37);
																			}
																			BGL_TAIL return
																				BGl_sexpzd2ze3nodez31zzast_sexpz00
																				(BgL_arg2096z00_2445, BgL_stackz00_36,
																				BgL_locz00_37, BgL_sitez00_38);
																		}
																	}
																else
																	{	/* Ast/let.scm 1198 */
																		obj_t BgL_cdrzd22849zd2_2431;

																		BgL_cdrzd22849zd2_2431 =
																			CDR(((obj_t) BgL_sexpz00_35));
																		{	/* Ast/let.scm 1198 */
																			obj_t BgL_carzd22852zd2_2432;

																			BgL_carzd22852zd2_2432 =
																				CAR(((obj_t) BgL_cdrzd22849zd2_2431));
																			if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_carzd22852zd2_2432))
																				{	/* Ast/let.scm 1198 */
																					obj_t BgL_arg2086z00_2434;

																					BgL_arg2086z00_2434 =
																						CDR(
																						((obj_t) BgL_cdrzd22849zd2_2431));
																					BgL_bindingsz00_2411 =
																						BgL_carzd22852zd2_2432;
																					BgL_bodyz00_2412 =
																						BgL_arg2086z00_2434;
																				BgL_tagzd22809zd2_2413:
																					{	/* Ast/let.scm 1209 */
																						bool_t BgL_test4084z00_10163;

																						{
																							obj_t BgL_l1575z00_2460;

																							BgL_l1575z00_2460 =
																								BgL_bindingsz00_2411;
																						BgL_zc3z04anonymousza32105ze3z87_2461:
																							if (NULLP
																								(BgL_l1575z00_2460))
																								{	/* Ast/let.scm 1209 */
																									BgL_test4084z00_10163 =
																										((bool_t) 1);
																								}
																							else
																								{	/* Ast/let.scm 1209 */
																									bool_t BgL_test4086z00_10166;

																									{	/* Ast/let.scm 1209 */
																										obj_t BgL_arg2107z00_2466;

																										BgL_arg2107z00_2466 =
																											CAR(
																											((obj_t)
																												BgL_l1575z00_2460));
																										BgL_expz00_4256 =
																											BgL_arg2107z00_2466;
																										if (PAIRP(BgL_expz00_4256))
																											{	/* Ast/let.scm 1195 */
																												obj_t
																													BgL_cdrzd22644zd2_4267;
																												BgL_cdrzd22644zd2_4267 =
																													CDR(((obj_t)
																														BgL_expz00_4256));
																												if (PAIRP
																													(BgL_cdrzd22644zd2_4267))
																													{	/* Ast/let.scm 1195 */
																														obj_t
																															BgL_carzd22645zd2_4269;
																														BgL_carzd22645zd2_4269
																															=
																															CAR
																															(BgL_cdrzd22644zd2_4267);
																														if (PAIRP
																															(BgL_carzd22645zd2_4269))
																															{	/* Ast/let.scm 1195 */
																																if (
																																	(CAR
																																		(BgL_carzd22645zd2_4269)
																																		==
																																		CNST_TABLE_REF
																																		(7)))
																																	{	/* Ast/let.scm 1195 */
																																		bool_t
																																			BgL_test4091z00_10182;
																																		{	/* Ast/let.scm 1195 */
																																			obj_t
																																				BgL_tmpz00_10183;
																																			BgL_tmpz00_10183
																																				=
																																				CDR
																																				(BgL_carzd22645zd2_4269);
																																			BgL_test4091z00_10182
																																				=
																																				PAIRP
																																				(BgL_tmpz00_10183);
																																		}
																																		if (BgL_test4091z00_10182)
																																			{	/* Ast/let.scm 1195 */
																																				BgL_test4086z00_10166
																																					=
																																					NULLP
																																					(CDR
																																					(BgL_cdrzd22644zd2_4267));
																																			}
																																		else
																																			{	/* Ast/let.scm 1195 */
																																				BgL_test4086z00_10166
																																					=
																																					(
																																					(bool_t)
																																					0);
																																			}
																																	}
																																else
																																	{	/* Ast/let.scm 1195 */
																																		obj_t
																																			BgL_carzd22671zd2_4280;
																																		BgL_carzd22671zd2_4280
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd22644zd2_4267));
																																		{	/* Ast/let.scm 1195 */
																																			obj_t
																																				BgL_cdrzd22676zd2_4281;
																																			BgL_cdrzd22676zd2_4281
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_carzd22671zd2_4280));
																																			if ((CAR((
																																							(obj_t)
																																							BgL_carzd22671zd2_4280))
																																					==
																																					CNST_TABLE_REF
																																					(8)))
																																				{	/* Ast/let.scm 1195 */
																																					if (PAIRP(BgL_cdrzd22676zd2_4281))
																																						{	/* Ast/let.scm 1195 */
																																							obj_t
																																								BgL_carzd22679zd2_4285;
																																							obj_t
																																								BgL_cdrzd22680zd2_4286;
																																							BgL_carzd22679zd2_4285
																																								=
																																								CAR
																																								(BgL_cdrzd22676zd2_4281);
																																							BgL_cdrzd22680zd2_4286
																																								=
																																								CDR
																																								(BgL_cdrzd22676zd2_4281);
																																							if (PAIRP(BgL_carzd22679zd2_4285))
																																								{	/* Ast/let.scm 1195 */
																																									obj_t
																																										BgL_carzd22682zd2_4288;
																																									BgL_carzd22682zd2_4288
																																										=
																																										CAR
																																										(BgL_carzd22679zd2_4285);
																																									if (PAIRP(BgL_carzd22682zd2_4288))
																																										{	/* Ast/let.scm 1195 */
																																											if (NULLP(CDR(BgL_carzd22679zd2_4285)))
																																												{	/* Ast/let.scm 1195 */
																																													if (PAIRP(BgL_cdrzd22680zd2_4286))
																																														{	/* Ast/let.scm 1195 */
																																															obj_t
																																																BgL_carzd22689zd2_4293;
																																															BgL_carzd22689zd2_4293
																																																=
																																																CAR
																																																(BgL_cdrzd22680zd2_4286);
																																															if (SYMBOLP(BgL_carzd22689zd2_4293))
																																																{	/* Ast/let.scm 1195 */
																																																	if (NULLP(CDR(BgL_cdrzd22680zd2_4286)))
																																																		{	/* Ast/let.scm 1195 */
																																																			if (NULLP(CDR(((obj_t) BgL_cdrzd22644zd2_4267))))
																																																				{	/* Ast/let.scm 1195 */
																																																					obj_t
																																																						BgL_arg3472z00_4299;
																																																					BgL_arg3472z00_4299
																																																						=
																																																						CAR
																																																						(BgL_carzd22682zd2_4288);
																																																					BgL_test4086z00_10166
																																																						=
																																																						(BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																																																						(BgL_arg3472z00_4299,
																																																							BFALSE)
																																																						==
																																																						BgL_carzd22689zd2_4293);
																																																				}
																																																			else
																																																				{	/* Ast/let.scm 1195 */
																																																					BgL_test4086z00_10166
																																																						=
																																																						(
																																																						(bool_t)
																																																						0);
																																																				}
																																																		}
																																																	else
																																																		{	/* Ast/let.scm 1195 */
																																																			obj_t
																																																				BgL_cdrzd22698zd2_4301;
																																																			BgL_cdrzd22698zd2_4301
																																																				=
																																																				CDR
																																																				(
																																																				((obj_t) BgL_expz00_4256));
																																																			{	/* Ast/let.scm 1195 */
																																																				obj_t
																																																					BgL_carzd22703zd2_4302;
																																																				{	/* Ast/let.scm 1195 */
																																																					obj_t
																																																						BgL_pairz00_6772;
																																																					BgL_pairz00_6772
																																																						=
																																																						CAR
																																																						(
																																																						((obj_t) BgL_cdrzd22698zd2_4301));
																																																					BgL_carzd22703zd2_4302
																																																						=
																																																						CAR
																																																						(BgL_pairz00_6772);
																																																				}
																																																				if (SYMBOLP(BgL_carzd22703zd2_4302))
																																																					{	/* Ast/let.scm 1195 */
																																																						if (NULLP(CDR(((obj_t) BgL_cdrzd22698zd2_4301))))
																																																							{	/* Ast/let.scm 1193 */
																																																								obj_t
																																																									BgL_arg3529z00_6774;
																																																								BgL_arg3529z00_6774
																																																									=
																																																									BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																																																									(BgL_carzd22703zd2_4302,
																																																									BFALSE);
																																																								BgL_test4086z00_10166
																																																									=
																																																									(BgL_arg3529z00_6774
																																																									==
																																																									CNST_TABLE_REF
																																																									(7));
																																																							}
																																																						else
																																																							{	/* Ast/let.scm 1195 */
																																																								BgL_test4086z00_10166
																																																									=
																																																									(
																																																									(bool_t)
																																																									0);
																																																							}
																																																					}
																																																				else
																																																					{	/* Ast/let.scm 1195 */
																																																						BgL_test4086z00_10166
																																																							=
																																																							(
																																																							(bool_t)
																																																							0);
																																																					}
																																																			}
																																																		}
																																																}
																																															else
																																																{	/* Ast/let.scm 1195 */
																																																	obj_t
																																																		BgL_cdrzd22710zd2_4309;
																																																	BgL_cdrzd22710zd2_4309
																																																		=
																																																		CDR
																																																		(
																																																		((obj_t) BgL_expz00_4256));
																																																	{	/* Ast/let.scm 1195 */
																																																		obj_t
																																																			BgL_carzd22715zd2_4310;
																																																		{	/* Ast/let.scm 1195 */
																																																			obj_t
																																																				BgL_pairz00_6777;
																																																			BgL_pairz00_6777
																																																				=
																																																				CAR
																																																				(
																																																				((obj_t) BgL_cdrzd22710zd2_4309));
																																																			BgL_carzd22715zd2_4310
																																																				=
																																																				CAR
																																																				(BgL_pairz00_6777);
																																																		}
																																																		if (SYMBOLP(BgL_carzd22715zd2_4310))
																																																			{	/* Ast/let.scm 1195 */
																																																				if (NULLP(CDR(((obj_t) BgL_cdrzd22710zd2_4309))))
																																																					{	/* Ast/let.scm 1193 */
																																																						obj_t
																																																							BgL_arg3529z00_6779;
																																																						BgL_arg3529z00_6779
																																																							=
																																																							BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																																																							(BgL_carzd22715zd2_4310,
																																																							BFALSE);
																																																						BgL_test4086z00_10166
																																																							=
																																																							(BgL_arg3529z00_6779
																																																							==
																																																							CNST_TABLE_REF
																																																							(7));
																																																					}
																																																				else
																																																					{	/* Ast/let.scm 1195 */
																																																						BgL_test4086z00_10166
																																																							=
																																																							(
																																																							(bool_t)
																																																							0);
																																																					}
																																																			}
																																																		else
																																																			{	/* Ast/let.scm 1195 */
																																																				BgL_test4086z00_10166
																																																					=
																																																					(
																																																					(bool_t)
																																																					0);
																																																			}
																																																	}
																																																}
																																														}
																																													else
																																														{	/* Ast/let.scm 1195 */
																																															obj_t
																																																BgL_cdrzd22722zd2_4316;
																																															BgL_cdrzd22722zd2_4316
																																																=
																																																CDR
																																																(
																																																((obj_t) BgL_expz00_4256));
																																															{	/* Ast/let.scm 1195 */
																																																obj_t
																																																	BgL_carzd22727zd2_4317;
																																																{	/* Ast/let.scm 1195 */
																																																	obj_t
																																																		BgL_pairz00_6782;
																																																	BgL_pairz00_6782
																																																		=
																																																		CAR
																																																		(
																																																		((obj_t) BgL_cdrzd22722zd2_4316));
																																																	BgL_carzd22727zd2_4317
																																																		=
																																																		CAR
																																																		(BgL_pairz00_6782);
																																																}
																																																if (SYMBOLP(BgL_carzd22727zd2_4317))
																																																	{	/* Ast/let.scm 1195 */
																																																		if (NULLP(CDR(((obj_t) BgL_cdrzd22722zd2_4316))))
																																																			{	/* Ast/let.scm 1193 */
																																																				obj_t
																																																					BgL_arg3529z00_6784;
																																																				BgL_arg3529z00_6784
																																																					=
																																																					BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																																																					(BgL_carzd22727zd2_4317,
																																																					BFALSE);
																																																				BgL_test4086z00_10166
																																																					=
																																																					(BgL_arg3529z00_6784
																																																					==
																																																					CNST_TABLE_REF
																																																					(7));
																																																			}
																																																		else
																																																			{	/* Ast/let.scm 1195 */
																																																				BgL_test4086z00_10166
																																																					=
																																																					(
																																																					(bool_t)
																																																					0);
																																																			}
																																																	}
																																																else
																																																	{	/* Ast/let.scm 1195 */
																																																		BgL_test4086z00_10166
																																																			=
																																																			(
																																																			(bool_t)
																																																			0);
																																																	}
																																															}
																																														}
																																												}
																																											else
																																												{	/* Ast/let.scm 1195 */
																																													obj_t
																																														BgL_cdrzd22734zd2_4323;
																																													BgL_cdrzd22734zd2_4323
																																														=
																																														CDR
																																														(
																																														((obj_t) BgL_expz00_4256));
																																													{	/* Ast/let.scm 1195 */
																																														obj_t
																																															BgL_carzd22739zd2_4324;
																																														{	/* Ast/let.scm 1195 */
																																															obj_t
																																																BgL_pairz00_6787;
																																															BgL_pairz00_6787
																																																=
																																																CAR
																																																(
																																																((obj_t) BgL_cdrzd22734zd2_4323));
																																															BgL_carzd22739zd2_4324
																																																=
																																																CAR
																																																(BgL_pairz00_6787);
																																														}
																																														if (SYMBOLP(BgL_carzd22739zd2_4324))
																																															{	/* Ast/let.scm 1195 */
																																																if (NULLP(CDR(((obj_t) BgL_cdrzd22734zd2_4323))))
																																																	{	/* Ast/let.scm 1193 */
																																																		obj_t
																																																			BgL_arg3529z00_6789;
																																																		BgL_arg3529z00_6789
																																																			=
																																																			BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																																																			(BgL_carzd22739zd2_4324,
																																																			BFALSE);
																																																		BgL_test4086z00_10166
																																																			=
																																																			(BgL_arg3529z00_6789
																																																			==
																																																			CNST_TABLE_REF
																																																			(7));
																																																	}
																																																else
																																																	{	/* Ast/let.scm 1195 */
																																																		BgL_test4086z00_10166
																																																			=
																																																			(
																																																			(bool_t)
																																																			0);
																																																	}
																																															}
																																														else
																																															{	/* Ast/let.scm 1195 */
																																																BgL_test4086z00_10166
																																																	=
																																																	(
																																																	(bool_t)
																																																	0);
																																															}
																																													}
																																												}
																																										}
																																									else
																																										{	/* Ast/let.scm 1195 */
																																											obj_t
																																												BgL_cdrzd22748zd2_4331;
																																											BgL_cdrzd22748zd2_4331
																																												=
																																												CDR
																																												(
																																												((obj_t) BgL_expz00_4256));
																																											{	/* Ast/let.scm 1195 */
																																												obj_t
																																													BgL_carzd22753zd2_4332;
																																												{	/* Ast/let.scm 1195 */
																																													obj_t
																																														BgL_pairz00_6792;
																																													BgL_pairz00_6792
																																														=
																																														CAR
																																														(
																																														((obj_t) BgL_cdrzd22748zd2_4331));
																																													BgL_carzd22753zd2_4332
																																														=
																																														CAR
																																														(BgL_pairz00_6792);
																																												}
																																												if (SYMBOLP(BgL_carzd22753zd2_4332))
																																													{	/* Ast/let.scm 1195 */
																																														if (NULLP(CDR(((obj_t) BgL_cdrzd22748zd2_4331))))
																																															{	/* Ast/let.scm 1193 */
																																																obj_t
																																																	BgL_arg3529z00_6794;
																																																BgL_arg3529z00_6794
																																																	=
																																																	BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																																																	(BgL_carzd22753zd2_4332,
																																																	BFALSE);
																																																BgL_test4086z00_10166
																																																	=
																																																	(BgL_arg3529z00_6794
																																																	==
																																																	CNST_TABLE_REF
																																																	(7));
																																															}
																																														else
																																															{	/* Ast/let.scm 1195 */
																																																BgL_test4086z00_10166
																																																	=
																																																	(
																																																	(bool_t)
																																																	0);
																																															}
																																													}
																																												else
																																													{	/* Ast/let.scm 1195 */
																																														BgL_test4086z00_10166
																																															=
																																															(
																																															(bool_t)
																																															0);
																																													}
																																											}
																																										}
																																								}
																																							else
																																								{	/* Ast/let.scm 1195 */
																																									obj_t
																																										BgL_cdrzd22762zd2_4338;
																																									BgL_cdrzd22762zd2_4338
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_expz00_4256));
																																									{	/* Ast/let.scm 1195 */
																																										obj_t
																																											BgL_carzd22767zd2_4339;
																																										{	/* Ast/let.scm 1195 */
																																											obj_t
																																												BgL_pairz00_6797;
																																											BgL_pairz00_6797
																																												=
																																												CAR
																																												(
																																												((obj_t) BgL_cdrzd22762zd2_4338));
																																											BgL_carzd22767zd2_4339
																																												=
																																												CAR
																																												(BgL_pairz00_6797);
																																										}
																																										if (SYMBOLP(BgL_carzd22767zd2_4339))
																																											{	/* Ast/let.scm 1195 */
																																												if (NULLP(CDR(((obj_t) BgL_cdrzd22762zd2_4338))))
																																													{	/* Ast/let.scm 1193 */
																																														obj_t
																																															BgL_arg3529z00_6799;
																																														BgL_arg3529z00_6799
																																															=
																																															BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																																															(BgL_carzd22767zd2_4339,
																																															BFALSE);
																																														BgL_test4086z00_10166
																																															=
																																															(BgL_arg3529z00_6799
																																															==
																																															CNST_TABLE_REF
																																															(7));
																																													}
																																												else
																																													{	/* Ast/let.scm 1195 */
																																														BgL_test4086z00_10166
																																															=
																																															(
																																															(bool_t)
																																															0);
																																													}
																																											}
																																										else
																																											{	/* Ast/let.scm 1195 */
																																												BgL_test4086z00_10166
																																													=
																																													(
																																													(bool_t)
																																													0);
																																											}
																																									}
																																								}
																																						}
																																					else
																																						{	/* Ast/let.scm 1195 */
																																							BgL_test4086z00_10166
																																								=
																																								(
																																								(bool_t)
																																								0);
																																						}
																																				}
																																			else
																																				{	/* Ast/let.scm 1195 */
																																					obj_t
																																						BgL_cdrzd22778zd2_4345;
																																					BgL_cdrzd22778zd2_4345
																																						=
																																						CDR(
																																						((obj_t) BgL_expz00_4256));
																																					{	/* Ast/let.scm 1195 */
																																						obj_t
																																							BgL_carzd22780zd2_4346;
																																						BgL_carzd22780zd2_4346
																																							=
																																							CAR
																																							(((obj_t) BgL_cdrzd22778zd2_4345));
																																						{	/* Ast/let.scm 1195 */
																																							obj_t
																																								BgL_carzd22783zd2_4347;
																																							BgL_carzd22783zd2_4347
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_carzd22780zd2_4346));
																																							if (SYMBOLP(BgL_carzd22783zd2_4347))
																																								{	/* Ast/let.scm 1195 */
																																									bool_t
																																										BgL_test4114z00_10316;
																																									{	/* Ast/let.scm 1195 */
																																										obj_t
																																											BgL_tmpz00_10317;
																																										BgL_tmpz00_10317
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_carzd22780zd2_4346));
																																										BgL_test4114z00_10316
																																											=
																																											PAIRP
																																											(BgL_tmpz00_10317);
																																									}
																																									if (BgL_test4114z00_10316)
																																										{	/* Ast/let.scm 1195 */
																																											if (NULLP(CDR(((obj_t) BgL_cdrzd22778zd2_4345))))
																																												{	/* Ast/let.scm 1193 */
																																													obj_t
																																														BgL_arg3529z00_6805;
																																													BgL_arg3529z00_6805
																																														=
																																														BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																																														(BgL_carzd22783zd2_4347,
																																														BFALSE);
																																													BgL_test4086z00_10166
																																														=
																																														(BgL_arg3529z00_6805
																																														==
																																														CNST_TABLE_REF
																																														(7));
																																												}
																																											else
																																												{	/* Ast/let.scm 1195 */
																																													BgL_test4086z00_10166
																																														=
																																														(
																																														(bool_t)
																																														0);
																																												}
																																										}
																																									else
																																										{	/* Ast/let.scm 1195 */
																																											BgL_test4086z00_10166
																																												=
																																												(
																																												(bool_t)
																																												0);
																																										}
																																								}
																																							else
																																								{	/* Ast/let.scm 1195 */
																																									BgL_test4086z00_10166
																																										=
																																										(
																																										(bool_t)
																																										0);
																																								}
																																						}
																																					}
																																				}
																																		}
																																	}
																															}
																														else
																															{	/* Ast/let.scm 1195 */
																																BgL_test4086z00_10166
																																	=
																																	((bool_t) 0);
																															}
																													}
																												else
																													{	/* Ast/let.scm 1195 */
																														BgL_test4086z00_10166
																															= ((bool_t) 0);
																													}
																											}
																										else
																											{	/* Ast/let.scm 1195 */
																												BgL_test4086z00_10166 =
																													((bool_t) 0);
																											}
																									}
																									if (BgL_test4086z00_10166)
																										{	/* Ast/let.scm 1209 */
																											obj_t BgL_arg2106z00_2465;

																											BgL_arg2106z00_2465 =
																												CDR(
																												((obj_t)
																													BgL_l1575z00_2460));
																											{
																												obj_t
																													BgL_l1575z00_10330;
																												BgL_l1575z00_10330 =
																													BgL_arg2106z00_2465;
																												BgL_l1575z00_2460 =
																													BgL_l1575z00_10330;
																												goto
																													BgL_zc3z04anonymousza32105ze3z87_2461;
																											}
																										}
																									else
																										{	/* Ast/let.scm 1209 */
																											BgL_test4084z00_10163 =
																												((bool_t) 0);
																										}
																								}
																						}
																						if (BgL_test4084z00_10163)
																							{	/* Ast/let.scm 1209 */
																								{	/* Ast/let.scm 1183 */
																									obj_t BgL_objz00_6809;

																									BgL_objz00_6809 =
																										CNST_TABLE_REF(10);
																									{	/* Ast/let.scm 1183 */
																										obj_t BgL_tmpz00_10332;

																										BgL_tmpz00_10332 =
																											((obj_t) BgL_sexpz00_35);
																										SET_CAR(BgL_tmpz00_10332,
																											BgL_objz00_6809);
																									}
																								}
																								BGL_TAIL return
																									BGl_sexpzd2ze3nodez31zzast_sexpz00
																									(BgL_sexpz00_35,
																									BgL_stackz00_36,
																									BgL_locz00_37,
																									BgL_sitez00_38);
																							}
																						else
																							{	/* Ast/let.scm 1209 */
																								BgL_bindingsz00_4205 =
																									BgL_bindingsz00_2411;
																								BgL_bodyz00_4206 =
																									BgL_bodyz00_2412;
																								{	/* Ast/let.scm 1168 */
																									obj_t BgL_varsz00_4208;

																									if (NULLP
																										(BgL_bindingsz00_4205))
																										{	/* Ast/let.scm 1168 */
																											BgL_varsz00_4208 = BNIL;
																										}
																									else
																										{	/* Ast/let.scm 1168 */
																											obj_t
																												BgL_head1564z00_4239;
																											BgL_head1564z00_4239 =
																												MAKE_YOUNG_PAIR(BNIL,
																												BNIL);
																											{
																												obj_t BgL_l1562z00_4241;
																												obj_t
																													BgL_tail1565z00_4242;
																												BgL_l1562z00_4241 =
																													BgL_bindingsz00_4205;
																												BgL_tail1565z00_4242 =
																													BgL_head1564z00_4239;
																											BgL_zc3z04anonymousza33438ze3z87_4243:
																												if (NULLP
																													(BgL_l1562z00_4241))
																													{	/* Ast/let.scm 1168 */
																														BgL_varsz00_4208 =
																															CDR
																															(BgL_head1564z00_4239);
																													}
																												else
																													{	/* Ast/let.scm 1168 */
																														obj_t
																															BgL_newtail1566z00_4245;
																														{	/* Ast/let.scm 1168 */
																															obj_t
																																BgL_arg3442z00_4247;
																															{	/* Ast/let.scm 1168 */
																																obj_t
																																	BgL_bz00_4248;
																																BgL_bz00_4248 =
																																	CAR(((obj_t)
																																		BgL_l1562z00_4241));
																																{	/* Ast/let.scm 1168 */
																																	obj_t
																																		BgL_arg3443z00_4249;
																																	BgL_arg3443z00_4249
																																		=
																																		CAR(((obj_t)
																																			BgL_bz00_4248));
																																	BgL_arg3442z00_4247
																																		=
																																		BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																																		(BgL_arg3443z00_4249,
																																		BgL_locz00_37);
																																}
																															}
																															BgL_newtail1566z00_4245
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg3442z00_4247,
																																BNIL);
																														}
																														SET_CDR
																															(BgL_tail1565z00_4242,
																															BgL_newtail1566z00_4245);
																														{	/* Ast/let.scm 1168 */
																															obj_t
																																BgL_arg3440z00_4246;
																															BgL_arg3440z00_4246
																																=
																																CDR(((obj_t)
																																	BgL_l1562z00_4241));
																															{
																																obj_t
																																	BgL_tail1565z00_10352;
																																obj_t
																																	BgL_l1562z00_10351;
																																BgL_l1562z00_10351
																																	=
																																	BgL_arg3440z00_4246;
																																BgL_tail1565z00_10352
																																	=
																																	BgL_newtail1566z00_4245;
																																BgL_tail1565z00_4242
																																	=
																																	BgL_tail1565z00_10352;
																																BgL_l1562z00_4241
																																	=
																																	BgL_l1562z00_10351;
																																goto
																																	BgL_zc3z04anonymousza33438ze3z87_4243;
																															}
																														}
																													}
																											}
																										}
																									{	/* Ast/let.scm 1168 */
																										obj_t BgL_ebindingsz00_4209;

																										if (NULLP
																											(BgL_bindingsz00_4205))
																											{	/* Ast/let.scm 1169 */
																												BgL_ebindingsz00_4209 =
																													BNIL;
																											}
																										else
																											{	/* Ast/let.scm 1169 */
																												obj_t
																													BgL_head1569z00_4223;
																												BgL_head1569z00_4223 =
																													MAKE_YOUNG_PAIR(BNIL,
																													BNIL);
																												{
																													obj_t
																														BgL_ll1567z00_4225;
																													obj_t
																														BgL_ll1568z00_4226;
																													obj_t
																														BgL_tail1570z00_4227;
																													BgL_ll1567z00_4225 =
																														BgL_bindingsz00_4205;
																													BgL_ll1568z00_4226 =
																														BgL_varsz00_4208;
																													BgL_tail1570z00_4227 =
																														BgL_head1569z00_4223;
																												BgL_zc3z04anonymousza33431ze3z87_4228:
																													if (NULLP
																														(BgL_ll1567z00_4225))
																														{	/* Ast/let.scm 1169 */
																															BgL_ebindingsz00_4209
																																=
																																CDR
																																(BgL_head1569z00_4223);
																														}
																													else
																														{	/* Ast/let.scm 1169 */
																															obj_t
																																BgL_newtail1571z00_4230;
																															{	/* Ast/let.scm 1169 */
																																obj_t
																																	BgL_arg3436z00_4233;
																																{	/* Ast/let.scm 1169 */
																																	obj_t
																																		BgL_bz00_4234;
																																	obj_t
																																		BgL_vz00_4235;
																																	BgL_bz00_4234
																																		=
																																		CAR(((obj_t)
																																			BgL_ll1567z00_4225));
																																	BgL_vz00_4235
																																		=
																																		CAR(((obj_t)
																																			BgL_ll1568z00_4226));
																																	BgL_bz00_2473
																																		=
																																		BgL_bz00_4234;
																																	BgL_vz00_2474
																																		=
																																		BgL_vz00_4235;
																																	BgL_varsz00_2475
																																		=
																																		BgL_varsz00_4208;
																																	{	/* Ast/let.scm 568 */
																																		obj_t
																																			BgL_arg2111z00_2477;
																																		obj_t
																																			BgL_arg2112z00_2478;
																																		{	/* Ast/let.scm 568 */
																																			obj_t
																																				BgL_arg2117z00_2483;
																																			{	/* Ast/let.scm 568 */
																																				obj_t
																																					BgL_pairz00_6004;
																																				BgL_pairz00_6004
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_bz00_2473));
																																				BgL_arg2117z00_2483
																																					=
																																					CAR
																																					(BgL_pairz00_6004);
																																			}
																																			BgL_arg2111z00_2477
																																				=
																																				BGl_loopze71ze7zzast_letz00
																																				(BgL_varsz00_2475,
																																				BgL_arg2117z00_2483,
																																				BNIL,
																																				BNIL);
																																		}
																																		{	/* Ast/let.scm 569 */
																																			obj_t
																																				BgL_arg2118z00_2484;
																																			{	/* Ast/let.scm 569 */
																																				obj_t
																																					BgL_pairz00_6008;
																																				BgL_pairz00_6008
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_bz00_2473));
																																				BgL_arg2118z00_2484
																																					=
																																					CAR
																																					(BgL_pairz00_6008);
																																			}
																																			BgL_arg2112z00_2478
																																				=
																																				BGl_loopze72ze7zzast_letz00
																																				(BgL_vz00_2474,
																																				BgL_varsz00_2475,
																																				BgL_arg2118z00_2484,
																																				BNIL,
																																				BNIL);
																																		}
																																		{	/* Ast/let.scm 567 */
																																			obj_t
																																				BgL_list2113z00_2479;
																																			{	/* Ast/let.scm 567 */
																																				obj_t
																																					BgL_arg2114z00_2480;
																																				{	/* Ast/let.scm 567 */
																																					obj_t
																																						BgL_arg2115z00_2481;
																																					{	/* Ast/let.scm 567 */
																																						obj_t
																																							BgL_arg2116z00_2482;
																																						BgL_arg2116z00_2482
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg2112z00_2478,
																																							BNIL);
																																						BgL_arg2115z00_2481
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg2111z00_2477,
																																							BgL_arg2116z00_2482);
																																					}
																																					BgL_arg2114z00_2480
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_vz00_2474,
																																						BgL_arg2115z00_2481);
																																				}
																																				BgL_list2113z00_2479
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_bz00_2473,
																																					BgL_arg2114z00_2480);
																																			}
																																			BgL_arg3436z00_4233
																																				=
																																				BgL_list2113z00_2479;
																																		}
																																	}
																																}
																																BgL_newtail1571z00_4230
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg3436z00_4233,
																																	BNIL);
																															}
																															SET_CDR
																																(BgL_tail1570z00_4227,
																																BgL_newtail1571z00_4230);
																															{	/* Ast/let.scm 1169 */
																																obj_t
																																	BgL_arg3433z00_4231;
																																obj_t
																																	BgL_arg3434z00_4232;
																																BgL_arg3433z00_4231
																																	=
																																	CDR(((obj_t)
																																		BgL_ll1567z00_4225));
																																BgL_arg3434z00_4232
																																	=
																																	CDR(((obj_t)
																																		BgL_ll1568z00_4226));
																																{
																																	obj_t
																																		BgL_tail1570z00_10383;
																																	obj_t
																																		BgL_ll1568z00_10382;
																																	obj_t
																																		BgL_ll1567z00_10381;
																																	BgL_ll1567z00_10381
																																		=
																																		BgL_arg3433z00_4231;
																																	BgL_ll1568z00_10382
																																		=
																																		BgL_arg3434z00_4232;
																																	BgL_tail1570z00_10383
																																		=
																																		BgL_newtail1571z00_4230;
																																	BgL_tail1570z00_4227
																																		=
																																		BgL_tail1570z00_10383;
																																	BgL_ll1568z00_4226
																																		=
																																		BgL_ll1568z00_10382;
																																	BgL_ll1567z00_4225
																																		=
																																		BgL_ll1567z00_10381;
																																	goto
																																		BgL_zc3z04anonymousza33431ze3z87_4228;
																																}
																															}
																														}
																												}
																											}
																										{	/* Ast/let.scm 1169 */

																											{	/* Ast/let.scm 1180 */
																												obj_t
																													BgL_arg3422z00_4217;
																												{	/* Ast/let.scm 1180 */
																													obj_t
																														BgL_arg3425z00_4218;
																													{	/* Ast/let.scm 1180 */
																														obj_t
																															BgL_arg3429z00_4219;
																														BgL_arg3429z00_4219
																															=
																															BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																															(BgL_bodyz00_4206,
																															BNIL);
																														BgL_arg3425z00_4218
																															=
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(9),
																															BgL_arg3429z00_4219);
																													}
																													BgL_arg3422z00_4217 =
																														BGl_epairifyzd2propagatezd2locz00zztools_miscz00
																														(BgL_arg3425z00_4218,
																														BgL_locz00_37);
																												}
																												BgL_ebindingsz00_4170 =
																													BgL_ebindingsz00_4209;
																												BgL_bodyz00_4171 =
																													BgL_arg3422z00_4217;
																												return
																													BGl_z62splitzd2headzd2letrecz62zzast_letz00
																													(BgL_sitez00_38,
																													BgL_locz00_37,
																													BgL_stackz00_36,
																													BgL_ebindingsz00_4170,
																													BgL_bodyz00_4171,
																													BGl_proc3809z00zzast_letz00,
																													BgL_stage1z00_7762);
																											}
																										}
																									}
																								}
																							}
																					}
																				}
																			else
																				{	/* Ast/let.scm 1198 */
																				BgL_tagzd22810zd2_2414:
																					return
																						BGl_errorzd2sexpzd2ze3nodeze3zzast_sexpz00
																						(BGl_string3810z00zzast_letz00,
																						BGl_expzd2envzd2zz__r4_numbers_6_5z00,
																						BGl_findzd2locationzf2locz20zztools_locationz00
																						(BGl_expzd2envzd2zz__r4_numbers_6_5z00,
																							BgL_locz00_37));
																				}
																		}
																	}
															}
														else
															{	/* Ast/let.scm 1198 */
																obj_t BgL_cdrzd22864zd2_2436;

																BgL_cdrzd22864zd2_2436 =
																	CDR(((obj_t) BgL_sexpz00_35));
																{	/* Ast/let.scm 1198 */
																	obj_t BgL_carzd22868zd2_2437;

																	BgL_carzd22868zd2_2437 =
																		CAR(((obj_t) BgL_cdrzd22864zd2_2436));
																	if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_carzd22868zd2_2437))
																		{	/* Ast/let.scm 1198 */
																			obj_t BgL_arg2089z00_2439;

																			BgL_arg2089z00_2439 =
																				CDR(((obj_t) BgL_cdrzd22864zd2_2436));
																			{
																				obj_t BgL_bodyz00_10400;
																				obj_t BgL_bindingsz00_10399;

																				BgL_bindingsz00_10399 =
																					BgL_carzd22868zd2_2437;
																				BgL_bodyz00_10400 = BgL_arg2089z00_2439;
																				BgL_bodyz00_2412 = BgL_bodyz00_10400;
																				BgL_bindingsz00_2411 =
																					BgL_bindingsz00_10399;
																				goto BgL_tagzd22809zd2_2413;
																			}
																		}
																	else
																		{	/* Ast/let.scm 1198 */
																			goto BgL_tagzd22810zd2_2414;
																		}
																}
															}
													}
											}
										else
											{	/* Ast/let.scm 1198 */
												goto BgL_tagzd22810zd2_2414;
											}
									}
								else
									{	/* Ast/let.scm 1198 */
										goto BgL_tagzd22810zd2_2414;
									}
							}
						else
							{	/* Ast/let.scm 1198 */
								goto BgL_tagzd22810zd2_2414;
							}
					}
				}
			}
		}

	}



/* loop~1 */
	obj_t BGl_loopze71ze7zzast_letz00(obj_t BgL_varsz00_7844,
		obj_t BgL_sexpz00_2987, obj_t BgL_resz00_2988, obj_t BgL_envz00_2989)
	{
		{	/* Ast/let.scm 798 */
		BGl_loopze71ze7zzast_letz00:
			{

				if (SYMBOLP(BgL_sexpz00_2987))
					{	/* Ast/let.scm 798 */
						if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_sexpz00_2987,
									BgL_varsz00_7844)))
							{	/* Ast/let.scm 804 */
								if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
										(BgL_sexpz00_2987, BgL_envz00_2989)))
									{	/* Ast/let.scm 805 */
										return BgL_resz00_2988;
									}
								else
									{	/* Ast/let.scm 805 */
										if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
												(BgL_sexpz00_2987, BgL_resz00_2988)))
											{	/* Ast/let.scm 806 */
												return BgL_resz00_2988;
											}
										else
											{	/* Ast/let.scm 806 */
												return
													MAKE_YOUNG_PAIR(BgL_sexpz00_2987, BgL_resz00_2988);
											}
									}
							}
						else
							{	/* Ast/let.scm 804 */
								return BgL_resz00_2988;
							}
					}
				else
					{	/* Ast/let.scm 798 */
						if (PAIRP(BgL_sexpz00_2987))
							{	/* Ast/let.scm 798 */
								obj_t BgL_cdrzd2506zd2_3020;

								BgL_cdrzd2506zd2_3020 = CDR(((obj_t) BgL_sexpz00_2987));
								if ((CAR(((obj_t) BgL_sexpz00_2987)) == CNST_TABLE_REF(11)))
									{	/* Ast/let.scm 798 */
										if (PAIRP(BgL_cdrzd2506zd2_3020))
											{	/* Ast/let.scm 798 */
												if (NULLP(CDR(BgL_cdrzd2506zd2_3020)))
													{	/* Ast/let.scm 798 */
														return BgL_resz00_2988;
													}
												else
													{	/* Ast/let.scm 798 */
													BgL_tagzd2488zd2_3015:
														{	/* Ast/let.scm 825 */
															obj_t BgL_arg2614z00_3176;
															obj_t BgL_arg2615z00_3177;

															BgL_arg2614z00_3176 =
																CAR(((obj_t) BgL_sexpz00_2987));
															{	/* Ast/let.scm 825 */
																obj_t BgL_arg2617z00_3178;

																BgL_arg2617z00_3178 =
																	CDR(((obj_t) BgL_sexpz00_2987));
																BgL_arg2615z00_3177 =
																	BGl_loopze71ze7zzast_letz00(BgL_varsz00_7844,
																	BgL_arg2617z00_3178, BgL_resz00_2988,
																	BgL_envz00_2989);
															}
															{
																obj_t BgL_resz00_10433;
																obj_t BgL_sexpz00_10432;

																BgL_sexpz00_10432 = BgL_arg2614z00_3176;
																BgL_resz00_10433 = BgL_arg2615z00_3177;
																BgL_resz00_2988 = BgL_resz00_10433;
																BgL_sexpz00_2987 = BgL_sexpz00_10432;
																goto BGl_loopze71ze7zzast_letz00;
															}
														}
													}
											}
										else
											{	/* Ast/let.scm 798 */
												goto BgL_tagzd2488zd2_3015;
											}
									}
								else
									{	/* Ast/let.scm 798 */
										if ((CAR(((obj_t) BgL_sexpz00_2987)) == CNST_TABLE_REF(0)))
											{	/* Ast/let.scm 798 */
												if (PAIRP(BgL_cdrzd2506zd2_3020))
													{	/* Ast/let.scm 798 */
														obj_t BgL_carzd2602zd2_3031;
														obj_t BgL_cdrzd2603zd2_3032;

														BgL_carzd2602zd2_3031 = CAR(BgL_cdrzd2506zd2_3020);
														BgL_cdrzd2603zd2_3032 = CDR(BgL_cdrzd2506zd2_3020);
														if (PAIRP(BgL_carzd2602zd2_3031))
															{	/* Ast/let.scm 798 */
																obj_t BgL_carzd2606zd2_3034;

																BgL_carzd2606zd2_3034 =
																	CAR(BgL_carzd2602zd2_3031);
																if (PAIRP(BgL_carzd2606zd2_3034))
																	{	/* Ast/let.scm 798 */
																		obj_t BgL_cdrzd2611zd2_3036;

																		BgL_cdrzd2611zd2_3036 =
																			CDR(BgL_carzd2606zd2_3034);
																		if (PAIRP(BgL_cdrzd2611zd2_3036))
																			{	/* Ast/let.scm 798 */
																				if (NULLP(CDR(BgL_cdrzd2611zd2_3036)))
																					{	/* Ast/let.scm 798 */
																						if (NULLP(CDR
																								(BgL_carzd2602zd2_3031)))
																							{	/* Ast/let.scm 798 */
																								if (PAIRP
																									(BgL_cdrzd2603zd2_3032))
																									{	/* Ast/let.scm 798 */
																										if (NULLP(CDR
																												(BgL_cdrzd2603zd2_3032)))
																											{	/* Ast/let.scm 798 */
																												obj_t
																													BgL_arg2475z00_3045;
																												obj_t
																													BgL_arg2476z00_3046;
																												obj_t
																													BgL_arg2479z00_3047;
																												BgL_arg2475z00_3045 =
																													CAR
																													(BgL_carzd2606zd2_3034);
																												BgL_arg2476z00_3046 =
																													CAR
																													(BgL_cdrzd2611zd2_3036);
																												BgL_arg2479z00_3047 =
																													CAR
																													(BgL_cdrzd2603zd2_3032);
																												{	/* Ast/let.scm 812 */
																													obj_t
																														BgL_arg2596z00_6252;
																													obj_t
																														BgL_arg2597z00_6253;
																													BgL_arg2596z00_6252 =
																														BGl_loopze71ze7zzast_letz00
																														(BgL_varsz00_7844,
																														BgL_arg2476z00_3046,
																														BgL_resz00_2988,
																														BgL_envz00_2989);
																													BgL_arg2597z00_6253 =
																														MAKE_YOUNG_PAIR
																														(BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																														(BgL_arg2475z00_3045,
																															BFALSE),
																														BgL_envz00_2989);
																													{
																														obj_t
																															BgL_envz00_10470;
																														obj_t
																															BgL_resz00_10469;
																														obj_t
																															BgL_sexpz00_10468;
																														BgL_sexpz00_10468 =
																															BgL_arg2479z00_3047;
																														BgL_resz00_10469 =
																															BgL_arg2596z00_6252;
																														BgL_envz00_10470 =
																															BgL_arg2597z00_6253;
																														BgL_envz00_2989 =
																															BgL_envz00_10470;
																														BgL_resz00_2988 =
																															BgL_resz00_10469;
																														BgL_sexpz00_2987 =
																															BgL_sexpz00_10468;
																														goto
																															BGl_loopze71ze7zzast_letz00;
																													}
																												}
																											}
																										else
																											{	/* Ast/let.scm 798 */
																												goto
																													BgL_tagzd2488zd2_3015;
																											}
																									}
																								else
																									{	/* Ast/let.scm 798 */
																										goto BgL_tagzd2488zd2_3015;
																									}
																							}
																						else
																							{	/* Ast/let.scm 798 */
																								goto BgL_tagzd2488zd2_3015;
																							}
																					}
																				else
																					{	/* Ast/let.scm 798 */
																						goto BgL_tagzd2488zd2_3015;
																					}
																			}
																		else
																			{	/* Ast/let.scm 798 */
																				goto BgL_tagzd2488zd2_3015;
																			}
																	}
																else
																	{	/* Ast/let.scm 798 */
																		goto BgL_tagzd2488zd2_3015;
																	}
															}
														else
															{	/* Ast/let.scm 798 */
																goto BgL_tagzd2488zd2_3015;
															}
													}
												else
													{	/* Ast/let.scm 798 */
														goto BgL_tagzd2488zd2_3015;
													}
											}
										else
											{	/* Ast/let.scm 798 */
												obj_t BgL_cdrzd2886zd2_3051;

												BgL_cdrzd2886zd2_3051 = CDR(((obj_t) BgL_sexpz00_2987));
												if (
													(CAR(
															((obj_t) BgL_sexpz00_2987)) ==
														CNST_TABLE_REF(12)))
													{	/* Ast/let.scm 798 */
														if (PAIRP(BgL_cdrzd2886zd2_3051))
															{	/* Ast/let.scm 798 */
																obj_t BgL_carzd2890zd2_3055;
																obj_t BgL_cdrzd2891zd2_3056;

																BgL_carzd2890zd2_3055 =
																	CAR(BgL_cdrzd2886zd2_3051);
																BgL_cdrzd2891zd2_3056 =
																	CDR(BgL_cdrzd2886zd2_3051);
																if (PAIRP(BgL_carzd2890zd2_3055))
																	{	/* Ast/let.scm 798 */
																		obj_t BgL_carzd2894zd2_3058;

																		BgL_carzd2894zd2_3058 =
																			CAR(BgL_carzd2890zd2_3055);
																		if (PAIRP(BgL_carzd2894zd2_3058))
																			{	/* Ast/let.scm 798 */
																				obj_t BgL_cdrzd2899zd2_3060;

																				BgL_cdrzd2899zd2_3060 =
																					CDR(BgL_carzd2894zd2_3058);
																				if (PAIRP(BgL_cdrzd2899zd2_3060))
																					{	/* Ast/let.scm 798 */
																						if (NULLP(CDR
																								(BgL_cdrzd2899zd2_3060)))
																							{	/* Ast/let.scm 798 */
																								if (NULLP(CDR
																										(BgL_carzd2890zd2_3055)))
																									{	/* Ast/let.scm 798 */
																										if (PAIRP
																											(BgL_cdrzd2891zd2_3056))
																											{	/* Ast/let.scm 798 */
																												if (NULLP(CDR
																														(BgL_cdrzd2891zd2_3056)))
																													{	/* Ast/let.scm 798 */
																														obj_t
																															BgL_arg2497z00_3069;
																														obj_t
																															BgL_arg2500z00_3070;
																														obj_t
																															BgL_arg2501z00_3071;
																														BgL_arg2497z00_3069
																															=
																															CAR
																															(BgL_carzd2894zd2_3058);
																														BgL_arg2500z00_3070
																															=
																															CAR
																															(BgL_cdrzd2899zd2_3060);
																														BgL_arg2501z00_3071
																															=
																															CAR
																															(BgL_cdrzd2891zd2_3056);
																														{	/* Ast/let.scm 814 */
																															obj_t
																																BgL_arg2600z00_6267;
																															obj_t
																																BgL_arg2601z00_6268;
																															BgL_arg2600z00_6267
																																=
																																BGl_loopze71ze7zzast_letz00
																																(BgL_varsz00_7844,
																																BgL_arg2500z00_3070,
																																BgL_resz00_2988,
																																BgL_envz00_2989);
																															BgL_arg2601z00_6268
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																																(BgL_arg2497z00_3069,
																																	BFALSE),
																																BgL_envz00_2989);
																															{
																																obj_t
																																	BgL_envz00_10509;
																																obj_t
																																	BgL_resz00_10508;
																																obj_t
																																	BgL_sexpz00_10507;
																																BgL_sexpz00_10507
																																	=
																																	BgL_arg2501z00_3071;
																																BgL_resz00_10508
																																	=
																																	BgL_arg2600z00_6267;
																																BgL_envz00_10509
																																	=
																																	BgL_arg2601z00_6268;
																																BgL_envz00_2989
																																	=
																																	BgL_envz00_10509;
																																BgL_resz00_2988
																																	=
																																	BgL_resz00_10508;
																																BgL_sexpz00_2987
																																	=
																																	BgL_sexpz00_10507;
																																goto
																																	BGl_loopze71ze7zzast_letz00;
																															}
																														}
																													}
																												else
																													{	/* Ast/let.scm 798 */
																														goto
																															BgL_tagzd2488zd2_3015;
																													}
																											}
																										else
																											{	/* Ast/let.scm 798 */
																												goto
																													BgL_tagzd2488zd2_3015;
																											}
																									}
																								else
																									{	/* Ast/let.scm 798 */
																										goto BgL_tagzd2488zd2_3015;
																									}
																							}
																						else
																							{	/* Ast/let.scm 798 */
																								goto BgL_tagzd2488zd2_3015;
																							}
																					}
																				else
																					{	/* Ast/let.scm 798 */
																						goto BgL_tagzd2488zd2_3015;
																					}
																			}
																		else
																			{	/* Ast/let.scm 798 */
																				goto BgL_tagzd2488zd2_3015;
																			}
																	}
																else
																	{	/* Ast/let.scm 798 */
																		goto BgL_tagzd2488zd2_3015;
																	}
															}
														else
															{	/* Ast/let.scm 798 */
																goto BgL_tagzd2488zd2_3015;
															}
													}
												else
													{	/* Ast/let.scm 798 */
														if (
															(CAR(
																	((obj_t) BgL_sexpz00_2987)) ==
																CNST_TABLE_REF(10)))
															{	/* Ast/let.scm 798 */
																if (PAIRP(BgL_cdrzd2886zd2_3051))
																	{	/* Ast/let.scm 798 */
																		obj_t BgL_carzd21114zd2_3079;
																		obj_t BgL_cdrzd21115zd2_3080;

																		BgL_carzd21114zd2_3079 =
																			CAR(BgL_cdrzd2886zd2_3051);
																		BgL_cdrzd21115zd2_3080 =
																			CDR(BgL_cdrzd2886zd2_3051);
																		if (PAIRP(BgL_carzd21114zd2_3079))
																			{	/* Ast/let.scm 798 */
																				obj_t BgL_carzd21118zd2_3082;

																				BgL_carzd21118zd2_3082 =
																					CAR(BgL_carzd21114zd2_3079);
																				if (PAIRP(BgL_carzd21118zd2_3082))
																					{	/* Ast/let.scm 798 */
																						obj_t BgL_cdrzd21123zd2_3084;

																						BgL_cdrzd21123zd2_3084 =
																							CDR(BgL_carzd21118zd2_3082);
																						if (PAIRP(BgL_cdrzd21123zd2_3084))
																							{	/* Ast/let.scm 798 */
																								if (NULLP(CDR
																										(BgL_cdrzd21123zd2_3084)))
																									{	/* Ast/let.scm 798 */
																										if (NULLP(CDR
																												(BgL_carzd21114zd2_3079)))
																											{	/* Ast/let.scm 798 */
																												if (PAIRP
																													(BgL_cdrzd21115zd2_3080))
																													{	/* Ast/let.scm 798 */
																														if (NULLP(CDR
																																(BgL_cdrzd21115zd2_3080)))
																															{	/* Ast/let.scm 798 */
																																obj_t
																																	BgL_arg2521z00_3093;
																																obj_t
																																	BgL_arg2524z00_3094;
																																obj_t
																																	BgL_arg2525z00_3095;
																																BgL_arg2521z00_3093
																																	=
																																	CAR
																																	(BgL_carzd21118zd2_3082);
																																BgL_arg2524z00_3094
																																	=
																																	CAR
																																	(BgL_cdrzd21123zd2_3084);
																																BgL_arg2525z00_3095
																																	=
																																	CAR
																																	(BgL_cdrzd21115zd2_3080);
																																{	/* Ast/let.scm 816 */
																																	obj_t
																																		BgL_nenvz00_6282;
																																	BgL_nenvz00_6282
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																																		(BgL_arg2521z00_3093,
																																			BFALSE),
																																		BgL_envz00_2989);
																																	{
																																		obj_t
																																			BgL_envz00_10546;
																																		obj_t
																																			BgL_resz00_10544;
																																		obj_t
																																			BgL_sexpz00_10543;
																																		BgL_sexpz00_10543
																																			=
																																			BgL_arg2525z00_3095;
																																		BgL_resz00_10544
																																			=
																																			BGl_loopze71ze7zzast_letz00
																																			(BgL_varsz00_7844,
																																			BgL_arg2524z00_3094,
																																			BgL_resz00_2988,
																																			BgL_nenvz00_6282);
																																		BgL_envz00_10546
																																			=
																																			BgL_nenvz00_6282;
																																		BgL_envz00_2989
																																			=
																																			BgL_envz00_10546;
																																		BgL_resz00_2988
																																			=
																																			BgL_resz00_10544;
																																		BgL_sexpz00_2987
																																			=
																																			BgL_sexpz00_10543;
																																		goto
																																			BGl_loopze71ze7zzast_letz00;
																																	}
																																}
																															}
																														else
																															{	/* Ast/let.scm 798 */
																																goto
																																	BgL_tagzd2488zd2_3015;
																															}
																													}
																												else
																													{	/* Ast/let.scm 798 */
																														goto
																															BgL_tagzd2488zd2_3015;
																													}
																											}
																										else
																											{	/* Ast/let.scm 798 */
																												goto
																													BgL_tagzd2488zd2_3015;
																											}
																									}
																								else
																									{	/* Ast/let.scm 798 */
																										goto BgL_tagzd2488zd2_3015;
																									}
																							}
																						else
																							{	/* Ast/let.scm 798 */
																								goto BgL_tagzd2488zd2_3015;
																							}
																					}
																				else
																					{	/* Ast/let.scm 798 */
																						goto BgL_tagzd2488zd2_3015;
																					}
																			}
																		else
																			{	/* Ast/let.scm 798 */
																				goto BgL_tagzd2488zd2_3015;
																			}
																	}
																else
																	{	/* Ast/let.scm 798 */
																		goto BgL_tagzd2488zd2_3015;
																	}
															}
														else
															{	/* Ast/let.scm 798 */
																obj_t BgL_cdrzd21270zd2_3099;

																BgL_cdrzd21270zd2_3099 =
																	CDR(((obj_t) BgL_sexpz00_2987));
																if (
																	(CAR(
																			((obj_t) BgL_sexpz00_2987)) ==
																		CNST_TABLE_REF(4)))
																	{	/* Ast/let.scm 798 */
																		if (PAIRP(BgL_cdrzd21270zd2_3099))
																			{	/* Ast/let.scm 798 */
																				obj_t BgL_carzd21274zd2_3103;
																				obj_t BgL_cdrzd21275zd2_3104;

																				BgL_carzd21274zd2_3103 =
																					CAR(BgL_cdrzd21270zd2_3099);
																				BgL_cdrzd21275zd2_3104 =
																					CDR(BgL_cdrzd21270zd2_3099);
																				if (PAIRP(BgL_carzd21274zd2_3103))
																					{	/* Ast/let.scm 798 */
																						obj_t BgL_carzd21278zd2_3106;

																						BgL_carzd21278zd2_3106 =
																							CAR(BgL_carzd21274zd2_3103);
																						if (PAIRP(BgL_carzd21278zd2_3106))
																							{	/* Ast/let.scm 798 */
																								obj_t BgL_cdrzd21283zd2_3108;

																								BgL_cdrzd21283zd2_3108 =
																									CDR(BgL_carzd21278zd2_3106);
																								if (PAIRP
																									(BgL_cdrzd21283zd2_3108))
																									{	/* Ast/let.scm 798 */
																										if (NULLP(CDR
																												(BgL_cdrzd21283zd2_3108)))
																											{	/* Ast/let.scm 798 */
																												if (NULLP(CDR
																														(BgL_carzd21274zd2_3103)))
																													{	/* Ast/let.scm 798 */
																														if (PAIRP
																															(BgL_cdrzd21275zd2_3104))
																															{	/* Ast/let.scm 798 */
																																if (NULLP(CDR
																																		(BgL_cdrzd21275zd2_3104)))
																																	{	/* Ast/let.scm 798 */
																																		obj_t
																																			BgL_arg2545z00_3117;
																																		obj_t
																																			BgL_arg2546z00_3118;
																																		obj_t
																																			BgL_arg2548z00_3119;
																																		BgL_arg2545z00_3117
																																			=
																																			CAR
																																			(BgL_carzd21278zd2_3106);
																																		BgL_arg2546z00_3118
																																			=
																																			CAR
																																			(BgL_cdrzd21283zd2_3108);
																																		BgL_arg2548z00_3119
																																			=
																																			CAR
																																			(BgL_cdrzd21275zd2_3104);
																																		{	/* Ast/let.scm 819 */
																																			obj_t
																																				BgL_nenvz00_6297;
																																			BgL_nenvz00_6297
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																																				(BgL_arg2545z00_3117,
																																					BFALSE),
																																				BgL_envz00_2989);
																																			{
																																				obj_t
																																					BgL_envz00_10585;
																																				obj_t
																																					BgL_resz00_10583;
																																				obj_t
																																					BgL_sexpz00_10582;
																																				BgL_sexpz00_10582
																																					=
																																					BgL_arg2548z00_3119;
																																				BgL_resz00_10583
																																					=
																																					BGl_loopze71ze7zzast_letz00
																																					(BgL_varsz00_7844,
																																					BgL_arg2546z00_3118,
																																					BgL_resz00_2988,
																																					BgL_nenvz00_6297);
																																				BgL_envz00_10585
																																					=
																																					BgL_nenvz00_6297;
																																				BgL_envz00_2989
																																					=
																																					BgL_envz00_10585;
																																				BgL_resz00_2988
																																					=
																																					BgL_resz00_10583;
																																				BgL_sexpz00_2987
																																					=
																																					BgL_sexpz00_10582;
																																				goto
																																					BGl_loopze71ze7zzast_letz00;
																																			}
																																		}
																																	}
																																else
																																	{	/* Ast/let.scm 798 */
																																		goto
																																			BgL_tagzd2488zd2_3015;
																																	}
																															}
																														else
																															{	/* Ast/let.scm 798 */
																																goto
																																	BgL_tagzd2488zd2_3015;
																															}
																													}
																												else
																													{	/* Ast/let.scm 798 */
																														goto
																															BgL_tagzd2488zd2_3015;
																													}
																											}
																										else
																											{	/* Ast/let.scm 798 */
																												goto
																													BgL_tagzd2488zd2_3015;
																											}
																									}
																								else
																									{	/* Ast/let.scm 798 */
																										goto BgL_tagzd2488zd2_3015;
																									}
																							}
																						else
																							{	/* Ast/let.scm 798 */
																								goto BgL_tagzd2488zd2_3015;
																							}
																					}
																				else
																					{	/* Ast/let.scm 798 */
																						goto BgL_tagzd2488zd2_3015;
																					}
																			}
																		else
																			{	/* Ast/let.scm 798 */
																				goto BgL_tagzd2488zd2_3015;
																			}
																	}
																else
																	{	/* Ast/let.scm 798 */
																		if (
																			(CAR(
																					((obj_t) BgL_sexpz00_2987)) ==
																				CNST_TABLE_REF(8)))
																			{	/* Ast/let.scm 798 */
																				if (PAIRP(BgL_cdrzd21270zd2_3099))
																					{	/* Ast/let.scm 798 */
																						obj_t BgL_carzd21371zd2_3127;
																						obj_t BgL_cdrzd21372zd2_3128;

																						BgL_carzd21371zd2_3127 =
																							CAR(BgL_cdrzd21270zd2_3099);
																						BgL_cdrzd21372zd2_3128 =
																							CDR(BgL_cdrzd21270zd2_3099);
																						if (PAIRP(BgL_carzd21371zd2_3127))
																							{	/* Ast/let.scm 798 */
																								obj_t BgL_carzd21376zd2_3130;

																								BgL_carzd21376zd2_3130 =
																									CAR(BgL_carzd21371zd2_3127);
																								if (PAIRP
																									(BgL_carzd21376zd2_3130))
																									{	/* Ast/let.scm 798 */
																										obj_t
																											BgL_cdrzd21382zd2_3132;
																										BgL_cdrzd21382zd2_3132 =
																											CDR
																											(BgL_carzd21376zd2_3130);
																										if (PAIRP
																											(BgL_cdrzd21382zd2_3132))
																											{	/* Ast/let.scm 798 */
																												obj_t
																													BgL_cdrzd21387zd2_3134;
																												BgL_cdrzd21387zd2_3134 =
																													CDR
																													(BgL_cdrzd21382zd2_3132);
																												if (PAIRP
																													(BgL_cdrzd21387zd2_3134))
																													{	/* Ast/let.scm 798 */
																														if (NULLP(CDR
																																(BgL_cdrzd21387zd2_3134)))
																															{	/* Ast/let.scm 798 */
																																if (NULLP(CDR
																																		(BgL_carzd21371zd2_3127)))
																																	{	/* Ast/let.scm 798 */
																																		if (PAIRP
																																			(BgL_cdrzd21372zd2_3128))
																																			{	/* Ast/let.scm 798 */
																																				if (NULLP(CDR(BgL_cdrzd21372zd2_3128)))
																																					{	/* Ast/let.scm 798 */
																																						obj_t
																																							BgL_arg2567z00_3143;
																																						obj_t
																																							BgL_arg2568z00_3144;
																																						obj_t
																																							BgL_arg2572z00_3145;
																																						obj_t
																																							BgL_arg2578z00_3146;
																																						BgL_arg2567z00_3143
																																							=
																																							CAR
																																							(BgL_carzd21376zd2_3130);
																																						BgL_arg2568z00_3144
																																							=
																																							CAR
																																							(BgL_cdrzd21382zd2_3132);
																																						BgL_arg2572z00_3145
																																							=
																																							CAR
																																							(BgL_cdrzd21387zd2_3134);
																																						BgL_arg2578z00_3146
																																							=
																																							CAR
																																							(BgL_cdrzd21372zd2_3128);
																																						{	/* Ast/let.scm 822 */
																																							obj_t
																																								BgL_fenvz00_6314;
																																							BgL_fenvz00_6314
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																																								(BgL_arg2567z00_3143,
																																									BFALSE),
																																								BGl_appendza2ze70z45zzast_letz00
																																								(BgL_arg2568z00_3144,
																																									BgL_envz00_2989));
																																							{	/* Ast/let.scm 823 */
																																								obj_t
																																									BgL_arg2610z00_6317;
																																								obj_t
																																									BgL_arg2611z00_6318;
																																								BgL_arg2610z00_6317
																																									=
																																									BGl_loopze71ze7zzast_letz00
																																									(BgL_varsz00_7844,
																																									BgL_arg2572z00_3145,
																																									BgL_resz00_2988,
																																									BgL_fenvz00_6314);
																																								BgL_arg2611z00_6318
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg2567z00_3143,
																																									BgL_envz00_2989);
																																								{
																																									obj_t
																																										BgL_envz00_10628;
																																									obj_t
																																										BgL_resz00_10627;
																																									obj_t
																																										BgL_sexpz00_10626;
																																									BgL_sexpz00_10626
																																										=
																																										BgL_arg2578z00_3146;
																																									BgL_resz00_10627
																																										=
																																										BgL_arg2610z00_6317;
																																									BgL_envz00_10628
																																										=
																																										BgL_arg2611z00_6318;
																																									BgL_envz00_2989
																																										=
																																										BgL_envz00_10628;
																																									BgL_resz00_2988
																																										=
																																										BgL_resz00_10627;
																																									BgL_sexpz00_2987
																																										=
																																										BgL_sexpz00_10626;
																																									goto
																																										BGl_loopze71ze7zzast_letz00;
																																								}
																																							}
																																						}
																																					}
																																				else
																																					{	/* Ast/let.scm 798 */
																																						goto
																																							BgL_tagzd2488zd2_3015;
																																					}
																																			}
																																		else
																																			{	/* Ast/let.scm 798 */
																																				goto
																																					BgL_tagzd2488zd2_3015;
																																			}
																																	}
																																else
																																	{	/* Ast/let.scm 798 */
																																		goto
																																			BgL_tagzd2488zd2_3015;
																																	}
																															}
																														else
																															{	/* Ast/let.scm 798 */
																																goto
																																	BgL_tagzd2488zd2_3015;
																															}
																													}
																												else
																													{	/* Ast/let.scm 798 */
																														goto
																															BgL_tagzd2488zd2_3015;
																													}
																											}
																										else
																											{	/* Ast/let.scm 798 */
																												goto
																													BgL_tagzd2488zd2_3015;
																											}
																									}
																								else
																									{	/* Ast/let.scm 798 */
																										goto BgL_tagzd2488zd2_3015;
																									}
																							}
																						else
																							{	/* Ast/let.scm 798 */
																								goto BgL_tagzd2488zd2_3015;
																							}
																					}
																				else
																					{	/* Ast/let.scm 798 */
																						goto BgL_tagzd2488zd2_3015;
																					}
																			}
																		else
																			{	/* Ast/let.scm 798 */
																				goto BgL_tagzd2488zd2_3015;
																			}
																	}
															}
													}
											}
									}
							}
						else
							{	/* Ast/let.scm 798 */
								return BgL_resz00_2988;
							}
					}
			}
		}

	}



/* append*~0 */
	obj_t BGl_appendza2ze70z45zzast_letz00(obj_t BgL_pairz00_2970,
		obj_t BgL_lstz00_2971)
	{
		{	/* Ast/let.scm 793 */
			if (NULLP(BgL_pairz00_2970))
				{	/* Ast/let.scm 788 */
					return BgL_lstz00_2971;
				}
			else
				{	/* Ast/let.scm 788 */
					if (PAIRP(BgL_pairz00_2970))
						{	/* Ast/let.scm 790 */
							return
								MAKE_YOUNG_PAIR(BGl_fastzd2idzd2ofzd2idzd2zzast_identz00(CAR
									(BgL_pairz00_2970), BFALSE),
								BGl_appendza2ze70z45zzast_letz00(CDR(BgL_pairz00_2970),
									BgL_lstz00_2971));
						}
					else
						{	/* Ast/let.scm 790 */
							return
								MAKE_YOUNG_PAIR(BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
								(BgL_pairz00_2970, BFALSE), BgL_lstz00_2971);
						}
				}
		}

	}



/* loop~2 */
	obj_t BGl_loopze72ze7zzast_letz00(obj_t BgL_vz00_7846, obj_t BgL_varsz00_7845,
		obj_t BgL_sexpz00_3187, obj_t BgL_resz00_3188, obj_t BgL_envz00_3189)
	{
		{	/* Ast/let.scm 830 */
		BGl_loopze72ze7zzast_letz00:
			{
				obj_t BgL_varz00_3193;
				obj_t BgL_valz00_3194;

				if (PAIRP(BgL_sexpz00_3187))
					{	/* Ast/let.scm 830 */
						obj_t BgL_cdrzd21461zd2_3222;

						BgL_cdrzd21461zd2_3222 = CDR(((obj_t) BgL_sexpz00_3187));
						if ((CAR(((obj_t) BgL_sexpz00_3187)) == CNST_TABLE_REF(11)))
							{	/* Ast/let.scm 830 */
								if (PAIRP(BgL_cdrzd21461zd2_3222))
									{	/* Ast/let.scm 830 */
										if (NULLP(CDR(BgL_cdrzd21461zd2_3222)))
											{	/* Ast/let.scm 830 */
												return BgL_resz00_3188;
											}
										else
											{	/* Ast/let.scm 830 */
											BgL_tagzd21446zd2_3218:
												{	/* Ast/let.scm 858 */
													obj_t BgL_arg2774z00_3394;
													obj_t BgL_arg2776z00_3395;

													BgL_arg2774z00_3394 = CAR(((obj_t) BgL_sexpz00_3187));
													{	/* Ast/let.scm 858 */
														obj_t BgL_arg2777z00_3396;

														BgL_arg2777z00_3396 =
															CDR(((obj_t) BgL_sexpz00_3187));
														BgL_arg2776z00_3395 =
															BGl_loopze72ze7zzast_letz00(BgL_vz00_7846,
															BgL_varsz00_7845, BgL_arg2777z00_3396,
															BgL_resz00_3188, BgL_envz00_3189);
													}
													{
														obj_t BgL_resz00_10660;
														obj_t BgL_sexpz00_10659;

														BgL_sexpz00_10659 = BgL_arg2774z00_3394;
														BgL_resz00_10660 = BgL_arg2776z00_3395;
														BgL_resz00_3188 = BgL_resz00_10660;
														BgL_sexpz00_3187 = BgL_sexpz00_10659;
														goto BGl_loopze72ze7zzast_letz00;
													}
												}
											}
									}
								else
									{	/* Ast/let.scm 830 */
										goto BgL_tagzd21446zd2_3218;
									}
							}
						else
							{	/* Ast/let.scm 830 */
								if ((CAR(((obj_t) BgL_sexpz00_3187)) == CNST_TABLE_REF(5)))
									{	/* Ast/let.scm 830 */
										if (PAIRP(BgL_cdrzd21461zd2_3222))
											{	/* Ast/let.scm 830 */
												obj_t BgL_carzd21582zd2_3233;
												obj_t BgL_cdrzd21583zd2_3234;

												BgL_carzd21582zd2_3233 = CAR(BgL_cdrzd21461zd2_3222);
												BgL_cdrzd21583zd2_3234 = CDR(BgL_cdrzd21461zd2_3222);
												if (SYMBOLP(BgL_carzd21582zd2_3233))
													{	/* Ast/let.scm 830 */
														if (PAIRP(BgL_cdrzd21583zd2_3234))
															{	/* Ast/let.scm 830 */
																if (NULLP(CDR(BgL_cdrzd21583zd2_3234)))
																	{	/* Ast/let.scm 830 */
																		BgL_varz00_3193 = BgL_carzd21582zd2_3233;
																		BgL_valz00_3194 =
																			CAR(BgL_cdrzd21583zd2_3234);
																		{	/* Ast/let.scm 839 */
																			bool_t BgL_test4186z00_10677;

																			if ((BgL_sexpz00_3187 == BgL_vz00_7846))
																				{	/* Ast/let.scm 839 */
																					BgL_test4186z00_10677 = ((bool_t) 1);
																				}
																			else
																				{	/* Ast/let.scm 839 */
																					if (CBOOL
																						(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																							(BgL_varz00_3193,
																								BgL_varsz00_7845)))
																						{	/* Ast/let.scm 839 */
																							BgL_test4186z00_10677 =
																								((bool_t) 0);
																						}
																					else
																						{	/* Ast/let.scm 839 */
																							BgL_test4186z00_10677 =
																								((bool_t) 1);
																						}
																				}
																			if (BgL_test4186z00_10677)
																				{
																					obj_t BgL_sexpz00_10683;

																					BgL_sexpz00_10683 = BgL_varz00_3193;
																					BgL_sexpz00_3187 = BgL_sexpz00_10683;
																					goto BGl_loopze72ze7zzast_letz00;
																				}
																			else
																				{	/* Ast/let.scm 839 */
																					if (CBOOL
																						(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																							(BgL_varz00_3193,
																								BgL_resz00_3188)))
																						{
																							obj_t BgL_sexpz00_10687;

																							BgL_sexpz00_10687 =
																								BgL_valz00_3194;
																							BgL_sexpz00_3187 =
																								BgL_sexpz00_10687;
																							goto BGl_loopze72ze7zzast_letz00;
																						}
																					else
																						{	/* Ast/let.scm 840 */
																							if (CBOOL
																								(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																									(BgL_varz00_3193,
																										BgL_envz00_3189)))
																								{
																									obj_t BgL_sexpz00_10691;

																									BgL_sexpz00_10691 =
																										BgL_valz00_3194;
																									BgL_sexpz00_3187 =
																										BgL_sexpz00_10691;
																									goto
																										BGl_loopze72ze7zzast_letz00;
																								}
																							else
																								{	/* Ast/let.scm 842 */
																									obj_t BgL_arg2755z00_3375;

																									BgL_arg2755z00_3375 =
																										MAKE_YOUNG_PAIR
																										(BgL_varz00_3193,
																										BgL_resz00_3188);
																									{
																										obj_t BgL_resz00_10694;
																										obj_t BgL_sexpz00_10693;

																										BgL_sexpz00_10693 =
																											BgL_valz00_3194;
																										BgL_resz00_10694 =
																											BgL_arg2755z00_3375;
																										BgL_resz00_3188 =
																											BgL_resz00_10694;
																										BgL_sexpz00_3187 =
																											BgL_sexpz00_10693;
																										goto
																											BGl_loopze72ze7zzast_letz00;
																									}
																								}
																						}
																				}
																		}
																	}
																else
																	{	/* Ast/let.scm 830 */
																		goto BgL_tagzd21446zd2_3218;
																	}
															}
														else
															{	/* Ast/let.scm 830 */
																goto BgL_tagzd21446zd2_3218;
															}
													}
												else
													{	/* Ast/let.scm 830 */
														goto BgL_tagzd21446zd2_3218;
													}
											}
										else
											{	/* Ast/let.scm 830 */
												goto BgL_tagzd21446zd2_3218;
											}
									}
								else
									{	/* Ast/let.scm 830 */
										obj_t BgL_cdrzd21784zd2_3241;

										BgL_cdrzd21784zd2_3241 = CDR(((obj_t) BgL_sexpz00_3187));
										if ((CAR(((obj_t) BgL_sexpz00_3187)) == CNST_TABLE_REF(0)))
											{	/* Ast/let.scm 830 */
												if (PAIRP(BgL_cdrzd21784zd2_3241))
													{	/* Ast/let.scm 830 */
														obj_t BgL_carzd21788zd2_3245;
														obj_t BgL_cdrzd21789zd2_3246;

														BgL_carzd21788zd2_3245 =
															CAR(BgL_cdrzd21784zd2_3241);
														BgL_cdrzd21789zd2_3246 =
															CDR(BgL_cdrzd21784zd2_3241);
														if (PAIRP(BgL_carzd21788zd2_3245))
															{	/* Ast/let.scm 830 */
																obj_t BgL_carzd21792zd2_3248;

																BgL_carzd21792zd2_3248 =
																	CAR(BgL_carzd21788zd2_3245);
																if (PAIRP(BgL_carzd21792zd2_3248))
																	{	/* Ast/let.scm 830 */
																		obj_t BgL_cdrzd21797zd2_3250;

																		BgL_cdrzd21797zd2_3250 =
																			CDR(BgL_carzd21792zd2_3248);
																		if (PAIRP(BgL_cdrzd21797zd2_3250))
																			{	/* Ast/let.scm 830 */
																				if (NULLP(CDR(BgL_cdrzd21797zd2_3250)))
																					{	/* Ast/let.scm 830 */
																						if (NULLP(CDR
																								(BgL_carzd21788zd2_3245)))
																							{	/* Ast/let.scm 830 */
																								if (PAIRP
																									(BgL_cdrzd21789zd2_3246))
																									{	/* Ast/let.scm 830 */
																										if (NULLP(CDR
																												(BgL_cdrzd21789zd2_3246)))
																											{	/* Ast/let.scm 830 */
																												obj_t
																													BgL_arg2653z00_3259;
																												obj_t
																													BgL_arg2654z00_3260;
																												obj_t
																													BgL_arg2656z00_3261;
																												BgL_arg2653z00_3259 =
																													CAR
																													(BgL_carzd21792zd2_3248);
																												BgL_arg2654z00_3260 =
																													CAR
																													(BgL_cdrzd21797zd2_3250);
																												BgL_arg2656z00_3261 =
																													CAR
																													(BgL_cdrzd21789zd2_3246);
																												{	/* Ast/let.scm 845 */
																													obj_t
																														BgL_arg2756z00_6359;
																													obj_t
																														BgL_arg2757z00_6360;
																													BgL_arg2756z00_6359 =
																														BGl_loopze72ze7zzast_letz00
																														(BgL_vz00_7846,
																														BgL_varsz00_7845,
																														BgL_arg2654z00_3260,
																														BgL_resz00_3188,
																														BgL_envz00_3189);
																													BgL_arg2757z00_6360 =
																														MAKE_YOUNG_PAIR
																														(BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																														(BgL_arg2653z00_3259,
																															BFALSE),
																														BgL_envz00_3189);
																													{
																														obj_t
																															BgL_envz00_10734;
																														obj_t
																															BgL_resz00_10733;
																														obj_t
																															BgL_sexpz00_10732;
																														BgL_sexpz00_10732 =
																															BgL_arg2656z00_3261;
																														BgL_resz00_10733 =
																															BgL_arg2756z00_6359;
																														BgL_envz00_10734 =
																															BgL_arg2757z00_6360;
																														BgL_envz00_3189 =
																															BgL_envz00_10734;
																														BgL_resz00_3188 =
																															BgL_resz00_10733;
																														BgL_sexpz00_3187 =
																															BgL_sexpz00_10732;
																														goto
																															BGl_loopze72ze7zzast_letz00;
																													}
																												}
																											}
																										else
																											{	/* Ast/let.scm 830 */
																												goto
																													BgL_tagzd21446zd2_3218;
																											}
																									}
																								else
																									{	/* Ast/let.scm 830 */
																										goto BgL_tagzd21446zd2_3218;
																									}
																							}
																						else
																							{	/* Ast/let.scm 830 */
																								goto BgL_tagzd21446zd2_3218;
																							}
																					}
																				else
																					{	/* Ast/let.scm 830 */
																						goto BgL_tagzd21446zd2_3218;
																					}
																			}
																		else
																			{	/* Ast/let.scm 830 */
																				goto BgL_tagzd21446zd2_3218;
																			}
																	}
																else
																	{	/* Ast/let.scm 830 */
																		goto BgL_tagzd21446zd2_3218;
																	}
															}
														else
															{	/* Ast/let.scm 830 */
																goto BgL_tagzd21446zd2_3218;
															}
													}
												else
													{	/* Ast/let.scm 830 */
														goto BgL_tagzd21446zd2_3218;
													}
											}
										else
											{	/* Ast/let.scm 830 */
												if (
													(CAR(
															((obj_t) BgL_sexpz00_3187)) ==
														CNST_TABLE_REF(12)))
													{	/* Ast/let.scm 830 */
														if (PAIRP(BgL_cdrzd21784zd2_3241))
															{	/* Ast/let.scm 830 */
																obj_t BgL_carzd22076zd2_3269;
																obj_t BgL_cdrzd22077zd2_3270;

																BgL_carzd22076zd2_3269 =
																	CAR(BgL_cdrzd21784zd2_3241);
																BgL_cdrzd22077zd2_3270 =
																	CDR(BgL_cdrzd21784zd2_3241);
																if (PAIRP(BgL_carzd22076zd2_3269))
																	{	/* Ast/let.scm 830 */
																		obj_t BgL_carzd22080zd2_3272;

																		BgL_carzd22080zd2_3272 =
																			CAR(BgL_carzd22076zd2_3269);
																		if (PAIRP(BgL_carzd22080zd2_3272))
																			{	/* Ast/let.scm 830 */
																				obj_t BgL_cdrzd22085zd2_3274;

																				BgL_cdrzd22085zd2_3274 =
																					CDR(BgL_carzd22080zd2_3272);
																				if (PAIRP(BgL_cdrzd22085zd2_3274))
																					{	/* Ast/let.scm 830 */
																						if (NULLP(CDR
																								(BgL_cdrzd22085zd2_3274)))
																							{	/* Ast/let.scm 830 */
																								if (NULLP(CDR
																										(BgL_carzd22076zd2_3269)))
																									{	/* Ast/let.scm 830 */
																										if (PAIRP
																											(BgL_cdrzd22077zd2_3270))
																											{	/* Ast/let.scm 830 */
																												if (NULLP(CDR
																														(BgL_cdrzd22077zd2_3270)))
																													{	/* Ast/let.scm 830 */
																														obj_t
																															BgL_arg2674z00_3283;
																														obj_t
																															BgL_arg2675z00_3284;
																														obj_t
																															BgL_arg2676z00_3285;
																														BgL_arg2674z00_3283
																															=
																															CAR
																															(BgL_carzd22080zd2_3272);
																														BgL_arg2675z00_3284
																															=
																															CAR
																															(BgL_cdrzd22085zd2_3274);
																														BgL_arg2676z00_3285
																															=
																															CAR
																															(BgL_cdrzd22077zd2_3270);
																														{	/* Ast/let.scm 847 */
																															obj_t
																																BgL_arg2759z00_6374;
																															obj_t
																																BgL_arg2760z00_6375;
																															BgL_arg2759z00_6374
																																=
																																BGl_loopze72ze7zzast_letz00
																																(BgL_vz00_7846,
																																BgL_varsz00_7845,
																																BgL_arg2675z00_3284,
																																BgL_resz00_3188,
																																BgL_envz00_3189);
																															BgL_arg2760z00_6375
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																																(BgL_arg2674z00_3283,
																																	BFALSE),
																																BgL_envz00_3189);
																															{
																																obj_t
																																	BgL_envz00_10771;
																																obj_t
																																	BgL_resz00_10770;
																																obj_t
																																	BgL_sexpz00_10769;
																																BgL_sexpz00_10769
																																	=
																																	BgL_arg2676z00_3285;
																																BgL_resz00_10770
																																	=
																																	BgL_arg2759z00_6374;
																																BgL_envz00_10771
																																	=
																																	BgL_arg2760z00_6375;
																																BgL_envz00_3189
																																	=
																																	BgL_envz00_10771;
																																BgL_resz00_3188
																																	=
																																	BgL_resz00_10770;
																																BgL_sexpz00_3187
																																	=
																																	BgL_sexpz00_10769;
																																goto
																																	BGl_loopze72ze7zzast_letz00;
																															}
																														}
																													}
																												else
																													{	/* Ast/let.scm 830 */
																														goto
																															BgL_tagzd21446zd2_3218;
																													}
																											}
																										else
																											{	/* Ast/let.scm 830 */
																												goto
																													BgL_tagzd21446zd2_3218;
																											}
																									}
																								else
																									{	/* Ast/let.scm 830 */
																										goto BgL_tagzd21446zd2_3218;
																									}
																							}
																						else
																							{	/* Ast/let.scm 830 */
																								goto BgL_tagzd21446zd2_3218;
																							}
																					}
																				else
																					{	/* Ast/let.scm 830 */
																						goto BgL_tagzd21446zd2_3218;
																					}
																			}
																		else
																			{	/* Ast/let.scm 830 */
																				goto BgL_tagzd21446zd2_3218;
																			}
																	}
																else
																	{	/* Ast/let.scm 830 */
																		goto BgL_tagzd21446zd2_3218;
																	}
															}
														else
															{	/* Ast/let.scm 830 */
																goto BgL_tagzd21446zd2_3218;
															}
													}
												else
													{	/* Ast/let.scm 830 */
														obj_t BgL_cdrzd22296zd2_3289;

														BgL_cdrzd22296zd2_3289 =
															CDR(((obj_t) BgL_sexpz00_3187));
														if (
															(CAR(
																	((obj_t) BgL_sexpz00_3187)) ==
																CNST_TABLE_REF(10)))
															{	/* Ast/let.scm 830 */
																if (PAIRP(BgL_cdrzd22296zd2_3289))
																	{	/* Ast/let.scm 830 */
																		obj_t BgL_carzd22300zd2_3293;
																		obj_t BgL_cdrzd22301zd2_3294;

																		BgL_carzd22300zd2_3293 =
																			CAR(BgL_cdrzd22296zd2_3289);
																		BgL_cdrzd22301zd2_3294 =
																			CDR(BgL_cdrzd22296zd2_3289);
																		if (PAIRP(BgL_carzd22300zd2_3293))
																			{	/* Ast/let.scm 830 */
																				obj_t BgL_carzd22304zd2_3296;

																				BgL_carzd22304zd2_3296 =
																					CAR(BgL_carzd22300zd2_3293);
																				if (PAIRP(BgL_carzd22304zd2_3296))
																					{	/* Ast/let.scm 830 */
																						obj_t BgL_cdrzd22309zd2_3298;

																						BgL_cdrzd22309zd2_3298 =
																							CDR(BgL_carzd22304zd2_3296);
																						if (PAIRP(BgL_cdrzd22309zd2_3298))
																							{	/* Ast/let.scm 830 */
																								if (NULLP(CDR
																										(BgL_cdrzd22309zd2_3298)))
																									{	/* Ast/let.scm 830 */
																										if (NULLP(CDR
																												(BgL_carzd22300zd2_3293)))
																											{	/* Ast/let.scm 830 */
																												if (PAIRP
																													(BgL_cdrzd22301zd2_3294))
																													{	/* Ast/let.scm 830 */
																														if (NULLP(CDR
																																(BgL_cdrzd22301zd2_3294)))
																															{	/* Ast/let.scm 830 */
																																obj_t
																																	BgL_arg2695z00_3307;
																																obj_t
																																	BgL_arg2696z00_3308;
																																obj_t
																																	BgL_arg2697z00_3309;
																																BgL_arg2695z00_3307
																																	=
																																	CAR
																																	(BgL_carzd22304zd2_3296);
																																BgL_arg2696z00_3308
																																	=
																																	CAR
																																	(BgL_cdrzd22309zd2_3298);
																																BgL_arg2697z00_3309
																																	=
																																	CAR
																																	(BgL_cdrzd22301zd2_3294);
																																{	/* Ast/let.scm 849 */
																																	obj_t
																																		BgL_nenvz00_6389;
																																	BgL_nenvz00_6389
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																																		(BgL_arg2695z00_3307,
																																			BFALSE),
																																		BgL_envz00_3189);
																																	{
																																		obj_t
																																			BgL_envz00_10810;
																																		obj_t
																																			BgL_resz00_10808;
																																		obj_t
																																			BgL_sexpz00_10807;
																																		BgL_sexpz00_10807
																																			=
																																			BgL_arg2697z00_3309;
																																		BgL_resz00_10808
																																			=
																																			BGl_loopze72ze7zzast_letz00
																																			(BgL_vz00_7846,
																																			BgL_varsz00_7845,
																																			BgL_arg2696z00_3308,
																																			BgL_resz00_3188,
																																			BgL_nenvz00_6389);
																																		BgL_envz00_10810
																																			=
																																			BgL_nenvz00_6389;
																																		BgL_envz00_3189
																																			=
																																			BgL_envz00_10810;
																																		BgL_resz00_3188
																																			=
																																			BgL_resz00_10808;
																																		BgL_sexpz00_3187
																																			=
																																			BgL_sexpz00_10807;
																																		goto
																																			BGl_loopze72ze7zzast_letz00;
																																	}
																																}
																															}
																														else
																															{	/* Ast/let.scm 830 */
																																goto
																																	BgL_tagzd21446zd2_3218;
																															}
																													}
																												else
																													{	/* Ast/let.scm 830 */
																														goto
																															BgL_tagzd21446zd2_3218;
																													}
																											}
																										else
																											{	/* Ast/let.scm 830 */
																												goto
																													BgL_tagzd21446zd2_3218;
																											}
																									}
																								else
																									{	/* Ast/let.scm 830 */
																										goto BgL_tagzd21446zd2_3218;
																									}
																							}
																						else
																							{	/* Ast/let.scm 830 */
																								goto BgL_tagzd21446zd2_3218;
																							}
																					}
																				else
																					{	/* Ast/let.scm 830 */
																						goto BgL_tagzd21446zd2_3218;
																					}
																			}
																		else
																			{	/* Ast/let.scm 830 */
																				goto BgL_tagzd21446zd2_3218;
																			}
																	}
																else
																	{	/* Ast/let.scm 830 */
																		goto BgL_tagzd21446zd2_3218;
																	}
															}
														else
															{	/* Ast/let.scm 830 */
																if (
																	(CAR(
																			((obj_t) BgL_sexpz00_3187)) ==
																		CNST_TABLE_REF(4)))
																	{	/* Ast/let.scm 830 */
																		if (PAIRP(BgL_cdrzd22296zd2_3289))
																			{	/* Ast/let.scm 830 */
																				obj_t BgL_carzd22460zd2_3317;
																				obj_t BgL_cdrzd22461zd2_3318;

																				BgL_carzd22460zd2_3317 =
																					CAR(BgL_cdrzd22296zd2_3289);
																				BgL_cdrzd22461zd2_3318 =
																					CDR(BgL_cdrzd22296zd2_3289);
																				if (PAIRP(BgL_carzd22460zd2_3317))
																					{	/* Ast/let.scm 830 */
																						obj_t BgL_carzd22464zd2_3320;

																						BgL_carzd22464zd2_3320 =
																							CAR(BgL_carzd22460zd2_3317);
																						if (PAIRP(BgL_carzd22464zd2_3320))
																							{	/* Ast/let.scm 830 */
																								obj_t BgL_cdrzd22469zd2_3322;

																								BgL_cdrzd22469zd2_3322 =
																									CDR(BgL_carzd22464zd2_3320);
																								if (PAIRP
																									(BgL_cdrzd22469zd2_3322))
																									{	/* Ast/let.scm 830 */
																										if (NULLP(CDR
																												(BgL_cdrzd22469zd2_3322)))
																											{	/* Ast/let.scm 830 */
																												if (NULLP(CDR
																														(BgL_carzd22460zd2_3317)))
																													{	/* Ast/let.scm 830 */
																														if (PAIRP
																															(BgL_cdrzd22461zd2_3318))
																															{	/* Ast/let.scm 830 */
																																if (NULLP(CDR
																																		(BgL_cdrzd22461zd2_3318)))
																																	{	/* Ast/let.scm 830 */
																																		obj_t
																																			BgL_arg2714z00_3331;
																																		obj_t
																																			BgL_arg2715z00_3332;
																																		obj_t
																																			BgL_arg2716z00_3333;
																																		BgL_arg2714z00_3331
																																			=
																																			CAR
																																			(BgL_carzd22464zd2_3320);
																																		BgL_arg2715z00_3332
																																			=
																																			CAR
																																			(BgL_cdrzd22469zd2_3322);
																																		BgL_arg2716z00_3333
																																			=
																																			CAR
																																			(BgL_cdrzd22461zd2_3318);
																																		{	/* Ast/let.scm 852 */
																																			obj_t
																																				BgL_nenvz00_6404;
																																			BgL_nenvz00_6404
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																																				(BgL_arg2714z00_3331,
																																					BFALSE),
																																				BgL_envz00_3189);
																																			{
																																				obj_t
																																					BgL_envz00_10847;
																																				obj_t
																																					BgL_resz00_10845;
																																				obj_t
																																					BgL_sexpz00_10844;
																																				BgL_sexpz00_10844
																																					=
																																					BgL_arg2716z00_3333;
																																				BgL_resz00_10845
																																					=
																																					BGl_loopze72ze7zzast_letz00
																																					(BgL_vz00_7846,
																																					BgL_varsz00_7845,
																																					BgL_arg2715z00_3332,
																																					BgL_resz00_3188,
																																					BgL_nenvz00_6404);
																																				BgL_envz00_10847
																																					=
																																					BgL_nenvz00_6404;
																																				BgL_envz00_3189
																																					=
																																					BgL_envz00_10847;
																																				BgL_resz00_3188
																																					=
																																					BgL_resz00_10845;
																																				BgL_sexpz00_3187
																																					=
																																					BgL_sexpz00_10844;
																																				goto
																																					BGl_loopze72ze7zzast_letz00;
																																			}
																																		}
																																	}
																																else
																																	{	/* Ast/let.scm 830 */
																																		goto
																																			BgL_tagzd21446zd2_3218;
																																	}
																															}
																														else
																															{	/* Ast/let.scm 830 */
																																goto
																																	BgL_tagzd21446zd2_3218;
																															}
																													}
																												else
																													{	/* Ast/let.scm 830 */
																														goto
																															BgL_tagzd21446zd2_3218;
																													}
																											}
																										else
																											{	/* Ast/let.scm 830 */
																												goto
																													BgL_tagzd21446zd2_3218;
																											}
																									}
																								else
																									{	/* Ast/let.scm 830 */
																										goto BgL_tagzd21446zd2_3218;
																									}
																							}
																						else
																							{	/* Ast/let.scm 830 */
																								goto BgL_tagzd21446zd2_3218;
																							}
																					}
																				else
																					{	/* Ast/let.scm 830 */
																						goto BgL_tagzd21446zd2_3218;
																					}
																			}
																		else
																			{	/* Ast/let.scm 830 */
																				goto BgL_tagzd21446zd2_3218;
																			}
																	}
																else
																	{	/* Ast/let.scm 830 */
																		obj_t BgL_cdrzd22552zd2_3337;

																		BgL_cdrzd22552zd2_3337 =
																			CDR(((obj_t) BgL_sexpz00_3187));
																		if (
																			(CAR(
																					((obj_t) BgL_sexpz00_3187)) ==
																				CNST_TABLE_REF(8)))
																			{	/* Ast/let.scm 830 */
																				if (PAIRP(BgL_cdrzd22552zd2_3337))
																					{	/* Ast/let.scm 830 */
																						obj_t BgL_carzd22557zd2_3341;
																						obj_t BgL_cdrzd22558zd2_3342;

																						BgL_carzd22557zd2_3341 =
																							CAR(BgL_cdrzd22552zd2_3337);
																						BgL_cdrzd22558zd2_3342 =
																							CDR(BgL_cdrzd22552zd2_3337);
																						if (PAIRP(BgL_carzd22557zd2_3341))
																							{	/* Ast/let.scm 830 */
																								obj_t BgL_carzd22562zd2_3344;

																								BgL_carzd22562zd2_3344 =
																									CAR(BgL_carzd22557zd2_3341);
																								if (PAIRP
																									(BgL_carzd22562zd2_3344))
																									{	/* Ast/let.scm 830 */
																										obj_t
																											BgL_cdrzd22568zd2_3346;
																										BgL_cdrzd22568zd2_3346 =
																											CDR
																											(BgL_carzd22562zd2_3344);
																										if (PAIRP
																											(BgL_cdrzd22568zd2_3346))
																											{	/* Ast/let.scm 830 */
																												obj_t
																													BgL_cdrzd22573zd2_3348;
																												BgL_cdrzd22573zd2_3348 =
																													CDR
																													(BgL_cdrzd22568zd2_3346);
																												if (PAIRP
																													(BgL_cdrzd22573zd2_3348))
																													{	/* Ast/let.scm 830 */
																														if (NULLP(CDR
																																(BgL_cdrzd22573zd2_3348)))
																															{	/* Ast/let.scm 830 */
																																if (NULLP(CDR
																																		(BgL_carzd22557zd2_3341)))
																																	{	/* Ast/let.scm 830 */
																																		if (PAIRP
																																			(BgL_cdrzd22558zd2_3342))
																																			{	/* Ast/let.scm 830 */
																																				if (NULLP(CDR(BgL_cdrzd22558zd2_3342)))
																																					{	/* Ast/let.scm 830 */
																																						obj_t
																																							BgL_arg2736z00_3357;
																																						obj_t
																																							BgL_arg2737z00_3358;
																																						obj_t
																																							BgL_arg2738z00_3359;
																																						obj_t
																																							BgL_arg2739z00_3360;
																																						BgL_arg2736z00_3357
																																							=
																																							CAR
																																							(BgL_carzd22562zd2_3344);
																																						BgL_arg2737z00_3358
																																							=
																																							CAR
																																							(BgL_cdrzd22568zd2_3346);
																																						BgL_arg2738z00_3359
																																							=
																																							CAR
																																							(BgL_cdrzd22573zd2_3348);
																																						BgL_arg2739z00_3360
																																							=
																																							CAR
																																							(BgL_cdrzd22558zd2_3342);
																																						{	/* Ast/let.scm 855 */
																																							obj_t
																																								BgL_fenvz00_6421;
																																							BgL_fenvz00_6421
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																																								(BgL_arg2736z00_3357,
																																									BFALSE),
																																								BGl_appendza2ze70z45zzast_letz00
																																								(BgL_arg2737z00_3358,
																																									BgL_envz00_3189));
																																							{	/* Ast/let.scm 856 */
																																								obj_t
																																									BgL_arg2767z00_6424;
																																								obj_t
																																									BgL_arg2771z00_6425;
																																								BgL_arg2767z00_6424
																																									=
																																									BGl_loopze72ze7zzast_letz00
																																									(BgL_vz00_7846,
																																									BgL_varsz00_7845,
																																									BgL_arg2738z00_3359,
																																									BgL_resz00_3188,
																																									BgL_fenvz00_6421);
																																								BgL_arg2771z00_6425
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg2736z00_3357,
																																									BgL_envz00_3189);
																																								{
																																									obj_t
																																										BgL_envz00_10892;
																																									obj_t
																																										BgL_resz00_10891;
																																									obj_t
																																										BgL_sexpz00_10890;
																																									BgL_sexpz00_10890
																																										=
																																										BgL_arg2739z00_3360;
																																									BgL_resz00_10891
																																										=
																																										BgL_arg2767z00_6424;
																																									BgL_envz00_10892
																																										=
																																										BgL_arg2771z00_6425;
																																									BgL_envz00_3189
																																										=
																																										BgL_envz00_10892;
																																									BgL_resz00_3188
																																										=
																																										BgL_resz00_10891;
																																									BgL_sexpz00_3187
																																										=
																																										BgL_sexpz00_10890;
																																									goto
																																										BGl_loopze72ze7zzast_letz00;
																																								}
																																							}
																																						}
																																					}
																																				else
																																					{	/* Ast/let.scm 830 */
																																						goto
																																							BgL_tagzd21446zd2_3218;
																																					}
																																			}
																																		else
																																			{	/* Ast/let.scm 830 */
																																				goto
																																					BgL_tagzd21446zd2_3218;
																																			}
																																	}
																																else
																																	{	/* Ast/let.scm 830 */
																																		goto
																																			BgL_tagzd21446zd2_3218;
																																	}
																															}
																														else
																															{	/* Ast/let.scm 830 */
																																goto
																																	BgL_tagzd21446zd2_3218;
																															}
																													}
																												else
																													{	/* Ast/let.scm 830 */
																														goto
																															BgL_tagzd21446zd2_3218;
																													}
																											}
																										else
																											{	/* Ast/let.scm 830 */
																												goto
																													BgL_tagzd21446zd2_3218;
																											}
																									}
																								else
																									{	/* Ast/let.scm 830 */
																										goto BgL_tagzd21446zd2_3218;
																									}
																							}
																						else
																							{	/* Ast/let.scm 830 */
																								goto BgL_tagzd21446zd2_3218;
																							}
																					}
																				else
																					{	/* Ast/let.scm 830 */
																						goto BgL_tagzd21446zd2_3218;
																					}
																			}
																		else
																			{	/* Ast/let.scm 830 */
																				goto BgL_tagzd21446zd2_3218;
																			}
																	}
															}
													}
											}
									}
							}
					}
				else
					{	/* Ast/let.scm 830 */
						return BgL_resz00_3188;
					}
			}
		}

	}



/* &letrec*->node */
	BgL_nodez00_bglt BGl_z62letrecza2zd2ze3nodezf1zzast_letz00(obj_t
		BgL_envz00_7764, obj_t BgL_sexpz00_7765, obj_t BgL_stackz00_7766,
		obj_t BgL_locz00_7767, obj_t BgL_sitez00_7768)
	{
		{	/* Ast/let.scm 560 */
			return
				BGl_letrecza2zd2ze3nodez93zzast_letz00(BgL_sexpz00_7765,
				BgL_stackz00_7766, BgL_locz00_7767, BgL_sitez00_7768);
		}

	}



/* &letstar */
	obj_t BGl_z62letstarz62zzast_letz00(obj_t BgL_ebindingsz00_2530,
		obj_t BgL_bodyz00_2531)
	{
		{	/* Ast/let.scm 602 */
			if (NULLP(BgL_ebindingsz00_2530))
				{	/* Ast/let.scm 595 */
					return BgL_bodyz00_2531;
				}
			else
				{	/* Ast/let.scm 597 */
					bool_t BgL_test4238z00_10896;

					{	/* Ast/let.scm 597 */
						obj_t BgL_arg2165z00_2553;
						obj_t BgL_arg2166z00_2554;

						BgL_arg2165z00_2553 = CAR(((obj_t) BgL_ebindingsz00_2530));
						{	/* Ast/let.scm 597 */
							obj_t BgL_arg2167z00_2555;

							BgL_arg2167z00_2555 = CAR(((obj_t) BgL_ebindingsz00_2530));
							{	/* Ast/let.scm 597 */
								obj_t BgL_list2168z00_2556;

								BgL_list2168z00_2556 =
									MAKE_YOUNG_PAIR(BgL_arg2167z00_2555, BNIL);
								BgL_arg2166z00_2554 = BgL_list2168z00_2556;
							}
						}
						BgL_test4238z00_10896 =
							CBOOL(BGl_z62usedzd2inzf3z43zzast_letz00(BgL_arg2165z00_2553,
								BgL_arg2166z00_2554));
					}
					if (BgL_test4238z00_10896)
						{	/* Ast/let.scm 598 */
							obj_t BgL_arg2150z00_2539;

							{	/* Ast/let.scm 598 */
								obj_t BgL_arg2151z00_2540;
								obj_t BgL_arg2152z00_2541;

								{	/* Ast/let.scm 598 */
									obj_t BgL_arg2154z00_2542;

									{	/* Ast/let.scm 571 */
										obj_t BgL_pairz00_6068;

										BgL_pairz00_6068 = CAR(((obj_t) BgL_ebindingsz00_2530));
										BgL_arg2154z00_2542 = CAR(BgL_pairz00_6068);
									}
									BgL_arg2151z00_2540 =
										MAKE_YOUNG_PAIR(BgL_arg2154z00_2542, BNIL);
								}
								{	/* Ast/let.scm 599 */
									obj_t BgL_arg2156z00_2544;

									{	/* Ast/let.scm 599 */
										obj_t BgL_arg2157z00_2545;

										BgL_arg2157z00_2545 = CDR(((obj_t) BgL_ebindingsz00_2530));
										BgL_arg2156z00_2544 =
											BGl_z62letstarz62zzast_letz00(BgL_arg2157z00_2545,
											BgL_bodyz00_2531);
									}
									BgL_arg2152z00_2541 =
										MAKE_YOUNG_PAIR(BgL_arg2156z00_2544, BNIL);
								}
								BgL_arg2150z00_2539 =
									MAKE_YOUNG_PAIR(BgL_arg2151z00_2540, BgL_arg2152z00_2541);
							}
							return MAKE_YOUNG_PAIR(CNST_TABLE_REF(10), BgL_arg2150z00_2539);
						}
					else
						{	/* Ast/let.scm 601 */
							obj_t BgL_arg2158z00_2546;

							{	/* Ast/let.scm 601 */
								obj_t BgL_arg2159z00_2547;
								obj_t BgL_arg2160z00_2548;

								{	/* Ast/let.scm 601 */
									obj_t BgL_arg2161z00_2549;

									{	/* Ast/let.scm 571 */
										obj_t BgL_pairz00_6071;

										BgL_pairz00_6071 = CAR(((obj_t) BgL_ebindingsz00_2530));
										BgL_arg2161z00_2549 = CAR(BgL_pairz00_6071);
									}
									BgL_arg2159z00_2547 =
										MAKE_YOUNG_PAIR(BgL_arg2161z00_2549, BNIL);
								}
								{	/* Ast/let.scm 602 */
									obj_t BgL_arg2163z00_2551;

									{	/* Ast/let.scm 602 */
										obj_t BgL_arg2164z00_2552;

										BgL_arg2164z00_2552 = CDR(((obj_t) BgL_ebindingsz00_2530));
										BgL_arg2163z00_2551 =
											BGl_z62letstarz62zzast_letz00(BgL_arg2164z00_2552,
											BgL_bodyz00_2531);
									}
									BgL_arg2160z00_2548 =
										MAKE_YOUNG_PAIR(BgL_arg2163z00_2551, BNIL);
								}
								BgL_arg2158z00_2546 =
									MAKE_YOUNG_PAIR(BgL_arg2159z00_2547, BgL_arg2160z00_2548);
							}
							return MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BgL_arg2158z00_2546);
						}
				}
		}

	}



/* &used-in? */
	obj_t BGl_z62usedzd2inzf3z43zzast_letz00(obj_t BgL_bz00_2515,
		obj_t BgL_ebindingsz00_2516)
	{
		{	/* Ast/let.scm 591 */
			{	/* Ast/let.scm 588 */
				obj_t BgL_varz00_2518;

				{	/* Ast/let.scm 573 */
					obj_t BgL_pairz00_6054;

					BgL_pairz00_6054 = CDR(((obj_t) BgL_bz00_2515));
					BgL_varz00_2518 = CAR(BgL_pairz00_6054);
				}
				{
					obj_t BgL_list1425z00_2520;

					BgL_list1425z00_2520 = BgL_ebindingsz00_2516;
				BgL_zc3z04anonymousza32136ze3z87_2521:
					if (PAIRP(BgL_list1425z00_2520))
						{	/* Ast/let.scm 589 */
							bool_t BgL_test4240z00_10931;

							{	/* Ast/let.scm 590 */
								obj_t BgL_tmpz00_10932;

								{	/* Ast/let.scm 590 */
									obj_t BgL_auxz00_10933;

									{	/* Ast/let.scm 574 */
										obj_t BgL_pairz00_6061;

										{	/* Ast/let.scm 574 */
											obj_t BgL_pairz00_6060;

											BgL_pairz00_6060 =
												CDR(((obj_t) CAR(BgL_list1425z00_2520)));
											BgL_pairz00_6061 = CDR(BgL_pairz00_6060);
										}
										BgL_auxz00_10933 = CAR(BgL_pairz00_6061);
									}
									BgL_tmpz00_10932 =
										BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_varz00_2518,
										BgL_auxz00_10933);
								}
								BgL_test4240z00_10931 = CBOOL(BgL_tmpz00_10932);
							}
							if (BgL_test4240z00_10931)
								{	/* Ast/let.scm 589 */
									return CAR(BgL_list1425z00_2520);
								}
							else
								{
									obj_t BgL_list1425z00_10942;

									BgL_list1425z00_10942 = CDR(BgL_list1425z00_2520);
									BgL_list1425z00_2520 = BgL_list1425z00_10942;
									goto BgL_zc3z04anonymousza32136ze3z87_2521;
								}
						}
					else
						{	/* Ast/let.scm 589 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &mutable-in? */
	obj_t BGl_z62mutablezd2inzf3z43zzast_letz00(obj_t BgL_bz00_2496,
		obj_t BgL_ebindingsz00_2497)
	{
		{	/* Ast/let.scm 582 */
			{	/* Ast/let.scm 579 */
				obj_t BgL_varz00_2499;

				{	/* Ast/let.scm 573 */
					obj_t BgL_pairz00_6038;

					BgL_pairz00_6038 = CDR(((obj_t) BgL_bz00_2496));
					BgL_varz00_2499 = CAR(BgL_pairz00_6038);
				}
				{
					obj_t BgL_list1423z00_2501;

					BgL_list1423z00_2501 = BgL_ebindingsz00_2497;
				BgL_zc3z04anonymousza32126ze3z87_2502:
					if (PAIRP(BgL_list1423z00_2501))
						{	/* Ast/let.scm 580 */
							bool_t BgL_test4242z00_10949;

							{	/* Ast/let.scm 581 */
								obj_t BgL_tmpz00_10950;

								{	/* Ast/let.scm 581 */
									obj_t BgL_auxz00_10951;

									{	/* Ast/let.scm 575 */
										obj_t BgL_pairz00_6047;

										{	/* Ast/let.scm 575 */
											obj_t BgL_pairz00_6046;

											{	/* Ast/let.scm 575 */
												obj_t BgL_pairz00_6045;

												BgL_pairz00_6045 =
													CDR(((obj_t) CAR(BgL_list1423z00_2501)));
												BgL_pairz00_6046 = CDR(BgL_pairz00_6045);
											}
											BgL_pairz00_6047 = CDR(BgL_pairz00_6046);
										}
										BgL_auxz00_10951 = CAR(BgL_pairz00_6047);
									}
									BgL_tmpz00_10950 =
										BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_varz00_2499,
										BgL_auxz00_10951);
								}
								BgL_test4242z00_10949 = CBOOL(BgL_tmpz00_10950);
							}
							if (BgL_test4242z00_10949)
								{	/* Ast/let.scm 580 */
									return CAR(BgL_list1423z00_2501);
								}
							else
								{
									obj_t BgL_list1423z00_10961;

									BgL_list1423z00_10961 = CDR(BgL_list1423z00_2501);
									BgL_list1423z00_2501 = BgL_list1423z00_10961;
									goto BgL_zc3z04anonymousza32126ze3z87_2502;
								}
						}
					else
						{	/* Ast/let.scm 580 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &split-head-letrec */
	BgL_nodez00_bglt BGl_z62splitzd2headzd2letrecz62zzast_letz00(obj_t
		BgL_sitez00_7771, obj_t BgL_locz00_7770, obj_t BgL_stackz00_7769,
		obj_t BgL_ebindingsz00_2786, obj_t BgL_bodyz00_2787,
		obj_t BgL_splitz00_2788, obj_t BgL_kontz00_2789)
	{
		{	/* Ast/let.scm 688 */
			if (NULLP(BgL_ebindingsz00_2786))
				{	/* Ast/let.scm 672 */
					return
						BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_bodyz00_2787,
						BgL_stackz00_7769, BgL_locz00_7770, BgL_sitez00_7771);
				}
			else
				{	/* Ast/let.scm 675 */
					obj_t BgL_reczd2bindingszd2_2792;

					BgL_reczd2bindingszd2_2792 =
						((obj_t(*)(obj_t,
								obj_t))
						PROCEDURE_L_ENTRY(BgL_splitz00_2788)) (BgL_splitz00_2788,
						BgL_ebindingsz00_2786);
					{	/* Ast/let.scm 676 */
						obj_t BgL_recza2zd2bindingsz70_2793;

						{	/* Ast/let.scm 681 */
							obj_t BgL_tmpz00_6125;

							{	/* Ast/let.scm 681 */
								int BgL_tmpz00_10970;

								BgL_tmpz00_10970 = (int) (1L);
								BgL_tmpz00_6125 = BGL_MVALUES_VAL(BgL_tmpz00_10970);
							}
							{	/* Ast/let.scm 681 */
								int BgL_tmpz00_10973;

								BgL_tmpz00_10973 = (int) (1L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_10973, BUNSPEC);
							}
							BgL_recza2zd2bindingsz70_2793 = BgL_tmpz00_6125;
						}
						if (NULLP(BgL_reczd2bindingszd2_2792))
							{	/* Ast/let.scm 681 */
								return
									((BgL_nodez00_bglt(*)(obj_t, obj_t,
											obj_t))
									PROCEDURE_L_ENTRY(BgL_kontz00_2789)) (BgL_kontz00_2789,
									BgL_ebindingsz00_2786, BgL_bodyz00_2787);
							}
						else
							{	/* Ast/let.scm 685 */
								obj_t BgL_arg2317z00_2795;

								{	/* Ast/let.scm 685 */
									obj_t BgL_arg2318z00_2796;

									{	/* Ast/let.scm 685 */
										obj_t BgL_arg2319z00_2797;

										{	/* Ast/let.scm 685 */
											obj_t BgL_arg2320z00_2798;

											{	/* Ast/let.scm 685 */
												obj_t BgL_arg2321z00_2799;
												obj_t BgL_arg2323z00_2800;

												if (NULLP(BgL_recza2zd2bindingsz70_2793))
													{	/* Ast/let.scm 685 */
														BgL_arg2321z00_2799 = BNIL;
													}
												else
													{	/* Ast/let.scm 685 */
														obj_t BgL_head1437z00_2803;

														{	/* Ast/let.scm 685 */
															obj_t BgL_arg2331z00_2815;

															{	/* Ast/let.scm 571 */
																obj_t BgL_pairz00_6127;

																BgL_pairz00_6127 =
																	CAR(((obj_t) BgL_recza2zd2bindingsz70_2793));
																BgL_arg2331z00_2815 = CAR(BgL_pairz00_6127);
															}
															BgL_head1437z00_2803 =
																MAKE_YOUNG_PAIR(BgL_arg2331z00_2815, BNIL);
														}
														{	/* Ast/let.scm 685 */
															obj_t BgL_g1440z00_2804;

															BgL_g1440z00_2804 =
																CDR(((obj_t) BgL_recza2zd2bindingsz70_2793));
															{
																obj_t BgL_l1435z00_2806;
																obj_t BgL_tail1438z00_2807;

																BgL_l1435z00_2806 = BgL_g1440z00_2804;
																BgL_tail1438z00_2807 = BgL_head1437z00_2803;
															BgL_zc3z04anonymousza32325ze3z87_2808:
																if (NULLP(BgL_l1435z00_2806))
																	{	/* Ast/let.scm 685 */
																		BgL_arg2321z00_2799 = BgL_head1437z00_2803;
																	}
																else
																	{	/* Ast/let.scm 685 */
																		obj_t BgL_newtail1439z00_2810;

																		{	/* Ast/let.scm 685 */
																			obj_t BgL_arg2328z00_2812;

																			{	/* Ast/let.scm 571 */
																				obj_t BgL_pairz00_6130;

																				BgL_pairz00_6130 =
																					CAR(((obj_t) BgL_l1435z00_2806));
																				BgL_arg2328z00_2812 =
																					CAR(BgL_pairz00_6130);
																			}
																			BgL_newtail1439z00_2810 =
																				MAKE_YOUNG_PAIR(BgL_arg2328z00_2812,
																				BNIL);
																		}
																		SET_CDR(BgL_tail1438z00_2807,
																			BgL_newtail1439z00_2810);
																		{	/* Ast/let.scm 685 */
																			obj_t BgL_arg2327z00_2811;

																			BgL_arg2327z00_2811 =
																				CDR(((obj_t) BgL_l1435z00_2806));
																			{
																				obj_t BgL_tail1438z00_11001;
																				obj_t BgL_l1435z00_11000;

																				BgL_l1435z00_11000 =
																					BgL_arg2327z00_2811;
																				BgL_tail1438z00_11001 =
																					BgL_newtail1439z00_2810;
																				BgL_tail1438z00_2807 =
																					BgL_tail1438z00_11001;
																				BgL_l1435z00_2806 = BgL_l1435z00_11000;
																				goto
																					BgL_zc3z04anonymousza32325ze3z87_2808;
																			}
																		}
																	}
															}
														}
													}
												BgL_arg2323z00_2800 =
													MAKE_YOUNG_PAIR(BgL_bodyz00_2787, BNIL);
												BgL_arg2320z00_2798 =
													MAKE_YOUNG_PAIR(BgL_arg2321z00_2799,
													BgL_arg2323z00_2800);
											}
											BgL_arg2319z00_2797 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(4), BgL_arg2320z00_2798);
										}
										BgL_arg2318z00_2796 =
											BGl_letrecursiveze70ze7zzast_letz00
											(BgL_reczd2bindingszd2_2792, BgL_arg2319z00_2797);
									}
									BgL_arg2317z00_2795 =
										BGl_z62letcollapsez62zzast_letz00
										(BgL_reczd2bindingszd2_2792, BgL_arg2318z00_2796);
								}
								return
									BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_arg2317z00_2795,
									BgL_stackz00_7769, BgL_locz00_7770, BgL_sitez00_7771);
							}
					}
				}
		}

	}



/* letrecursive~0 */
	obj_t BGl_letrecursiveze70ze7zzast_letz00(obj_t BgL_ebindingsz00_2753,
		obj_t BgL_bodyz00_2754)
	{
		{	/* Ast/let.scm 668 */
			if (NULLP(BgL_ebindingsz00_2753))
				{	/* Ast/let.scm 661 */
					return BgL_bodyz00_2754;
				}
			else
				{	/* Ast/let.scm 663 */
					bool_t BgL_test4248z00_11011;

					{	/* Ast/let.scm 663 */
						obj_t BgL_arg2313z00_2785;

						BgL_arg2313z00_2785 = CAR(((obj_t) BgL_ebindingsz00_2753));
						BgL_test4248z00_11011 =
							CBOOL(BGl_z62usedzd2inzf3z43zzast_letz00(BgL_arg2313z00_2785,
								BgL_ebindingsz00_2753));
					}
					if (BgL_test4248z00_11011)
						{	/* Ast/let.scm 664 */
							obj_t BgL_arg2293z00_2759;

							{	/* Ast/let.scm 664 */
								obj_t BgL_arg2294z00_2760;
								obj_t BgL_arg2295z00_2761;

								{	/* Ast/let.scm 664 */
									obj_t BgL_head1431z00_2764;

									{	/* Ast/let.scm 664 */
										obj_t BgL_arg2304z00_2776;

										{	/* Ast/let.scm 571 */
											obj_t BgL_pairz00_6116;

											BgL_pairz00_6116 = CAR(((obj_t) BgL_ebindingsz00_2753));
											BgL_arg2304z00_2776 = CAR(BgL_pairz00_6116);
										}
										BgL_head1431z00_2764 =
											MAKE_YOUNG_PAIR(BgL_arg2304z00_2776, BNIL);
									}
									{	/* Ast/let.scm 664 */
										obj_t BgL_g1434z00_2765;

										BgL_g1434z00_2765 = CDR(((obj_t) BgL_ebindingsz00_2753));
										{
											obj_t BgL_l1429z00_2767;
											obj_t BgL_tail1432z00_2768;

											BgL_l1429z00_2767 = BgL_g1434z00_2765;
											BgL_tail1432z00_2768 = BgL_head1431z00_2764;
										BgL_zc3z04anonymousza32297ze3z87_2769:
											if (NULLP(BgL_l1429z00_2767))
												{	/* Ast/let.scm 664 */
													BgL_arg2294z00_2760 = BgL_head1431z00_2764;
												}
											else
												{	/* Ast/let.scm 664 */
													obj_t BgL_newtail1433z00_2771;

													{	/* Ast/let.scm 664 */
														obj_t BgL_arg2301z00_2773;

														{	/* Ast/let.scm 571 */
															obj_t BgL_pairz00_6119;

															BgL_pairz00_6119 =
																CAR(((obj_t) BgL_l1429z00_2767));
															BgL_arg2301z00_2773 = CAR(BgL_pairz00_6119);
														}
														BgL_newtail1433z00_2771 =
															MAKE_YOUNG_PAIR(BgL_arg2301z00_2773, BNIL);
													}
													SET_CDR(BgL_tail1432z00_2768,
														BgL_newtail1433z00_2771);
													{	/* Ast/let.scm 664 */
														obj_t BgL_arg2299z00_2772;

														BgL_arg2299z00_2772 =
															CDR(((obj_t) BgL_l1429z00_2767));
														{
															obj_t BgL_tail1432z00_11032;
															obj_t BgL_l1429z00_11031;

															BgL_l1429z00_11031 = BgL_arg2299z00_2772;
															BgL_tail1432z00_11032 = BgL_newtail1433z00_2771;
															BgL_tail1432z00_2768 = BgL_tail1432z00_11032;
															BgL_l1429z00_2767 = BgL_l1429z00_11031;
															goto BgL_zc3z04anonymousza32297ze3z87_2769;
														}
													}
												}
										}
									}
								}
								BgL_arg2295z00_2761 = MAKE_YOUNG_PAIR(BgL_bodyz00_2754, BNIL);
								BgL_arg2293z00_2759 =
									MAKE_YOUNG_PAIR(BgL_arg2294z00_2760, BgL_arg2295z00_2761);
							}
							return MAKE_YOUNG_PAIR(CNST_TABLE_REF(10), BgL_arg2293z00_2759);
						}
					else
						{	/* Ast/let.scm 667 */
							obj_t BgL_arg2306z00_2778;

							{	/* Ast/let.scm 667 */
								obj_t BgL_arg2307z00_2779;
								obj_t BgL_arg2308z00_2780;

								{	/* Ast/let.scm 667 */
									obj_t BgL_arg2309z00_2781;

									{	/* Ast/let.scm 571 */
										obj_t BgL_pairz00_6123;

										BgL_pairz00_6123 = CAR(((obj_t) BgL_ebindingsz00_2753));
										BgL_arg2309z00_2781 = CAR(BgL_pairz00_6123);
									}
									BgL_arg2307z00_2779 =
										MAKE_YOUNG_PAIR(BgL_arg2309z00_2781, BNIL);
								}
								{	/* Ast/let.scm 668 */
									obj_t BgL_arg2311z00_2783;

									{	/* Ast/let.scm 668 */
										obj_t BgL_arg2312z00_2784;

										BgL_arg2312z00_2784 = CDR(((obj_t) BgL_ebindingsz00_2753));
										BgL_arg2311z00_2783 =
											BGl_letrecursiveze70ze7zzast_letz00(BgL_arg2312z00_2784,
											BgL_bodyz00_2754);
									}
									BgL_arg2308z00_2780 =
										MAKE_YOUNG_PAIR(BgL_arg2311z00_2783, BNIL);
								}
								BgL_arg2306z00_2778 =
									MAKE_YOUNG_PAIR(BgL_arg2307z00_2779, BgL_arg2308z00_2780);
							}
							return MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BgL_arg2306z00_2778);
						}
				}
		}

	}



/* &letcollapse */
	obj_t BGl_z62letcollapsez62zzast_letz00(obj_t BgL_ebindingsz00_2735,
		obj_t BgL_exprz00_2736)
	{
		{	/* Ast/let.scm 657 */
			{
				obj_t BgL_ebindingsz00_2557;
				obj_t BgL_exprz00_2558;
				obj_t BgL_ebindingsz00_2670;
				obj_t BgL_exprz00_2671;

				if (PAIRP(BgL_exprz00_2736))
					{	/* Ast/let.scm 657 */
						if ((CAR(((obj_t) BgL_exprz00_2736)) == CNST_TABLE_REF(0)))
							{	/* Ast/let.scm 657 */
								BgL_ebindingsz00_2557 = BgL_ebindingsz00_2735;
								BgL_exprz00_2558 = BgL_exprz00_2736;
								{
									obj_t BgL_exprz00_2562;
									obj_t BgL_bindingsz00_2563;
									obj_t BgL_ebindingsz00_2564;

									BgL_exprz00_2562 = BgL_exprz00_2558;
									BgL_bindingsz00_2563 = BNIL;
									BgL_ebindingsz00_2564 = BgL_ebindingsz00_2557;
								BgL_zc3z04anonymousza32170ze3z87_2565:
									{
										obj_t BgL_bindingz00_2566;
										obj_t BgL_varz00_2567;
										obj_t BgL_valz00_2568;
										obj_t BgL_subexprz00_2569;

										if (PAIRP(BgL_exprz00_2562))
											{	/* Ast/let.scm 605 */
												obj_t BgL_cdrzd2403zd2_2574;

												BgL_cdrzd2403zd2_2574 = CDR(((obj_t) BgL_exprz00_2562));
												if (
													(CAR(
															((obj_t) BgL_exprz00_2562)) == CNST_TABLE_REF(0)))
													{	/* Ast/let.scm 605 */
														if (PAIRP(BgL_cdrzd2403zd2_2574))
															{	/* Ast/let.scm 605 */
																obj_t BgL_carzd2408zd2_2578;
																obj_t BgL_cdrzd2409zd2_2579;

																BgL_carzd2408zd2_2578 =
																	CAR(BgL_cdrzd2403zd2_2574);
																BgL_cdrzd2409zd2_2579 =
																	CDR(BgL_cdrzd2403zd2_2574);
																if (PAIRP(BgL_carzd2408zd2_2578))
																	{	/* Ast/let.scm 605 */
																		obj_t BgL_carzd2413zd2_2581;

																		BgL_carzd2413zd2_2581 =
																			CAR(BgL_carzd2408zd2_2578);
																		if (PAIRP(BgL_carzd2413zd2_2581))
																			{	/* Ast/let.scm 605 */
																				obj_t BgL_cdrzd2424zd2_2583;

																				BgL_cdrzd2424zd2_2583 =
																					CDR(BgL_carzd2413zd2_2581);
																				if (PAIRP(BgL_cdrzd2424zd2_2583))
																					{	/* Ast/let.scm 605 */
																						if (NULLP(CDR
																								(BgL_cdrzd2424zd2_2583)))
																							{	/* Ast/let.scm 605 */
																								if (NULLP(CDR
																										(BgL_carzd2408zd2_2578)))
																									{	/* Ast/let.scm 605 */
																										if (PAIRP
																											(BgL_cdrzd2409zd2_2579))
																											{	/* Ast/let.scm 605 */
																												if (NULLP(CDR
																														(BgL_cdrzd2409zd2_2579)))
																													{	/* Ast/let.scm 605 */
																														BgL_bindingz00_2566
																															=
																															BgL_carzd2413zd2_2581;
																														BgL_varz00_2567 =
																															CAR
																															(BgL_carzd2413zd2_2581);
																														BgL_valz00_2568 =
																															CAR
																															(BgL_cdrzd2424zd2_2583);
																														BgL_subexprz00_2569
																															=
																															CAR
																															(BgL_cdrzd2409zd2_2579);
																														{	/* Ast/let.scm 611 */
																															bool_t
																																BgL_test4262z00_11087;
																															{	/* Ast/let.scm 611 */
																																bool_t
																																	BgL_test4263z00_11088;
																																if (BGl_constantzf3zf3zzast_letz00(BgL_valz00_2568))
																																	{	/* Ast/let.scm 611 */
																																		BgL_test4263z00_11088
																																			=
																																			((bool_t)
																																			1);
																																	}
																																else
																																	{	/* Ast/let.scm 611 */

																																		BgL_test4263z00_11088
																																			=
																																			CBOOL
																																			(BGl_functionzf3zf3zzast_letz00
																																			(BgL_valz00_2568,
																																				BFALSE));
																																	}
																																if (BgL_test4263z00_11088)
																																	{	/* Ast/let.scm 612 */
																																		bool_t
																																			BgL_test4265z00_11093;
																																		{
																																			obj_t
																																				BgL_list1427z00_2653;
																																			{	/* Ast/let.scm 614 */
																																				obj_t
																																					BgL_tmpz00_11094;
																																				{	/* Ast/let.scm 574 */
																																					obj_t
																																						BgL_pairz00_6074;
																																					BgL_pairz00_6074
																																						=
																																						CAR(
																																						((obj_t) BgL_ebindingsz00_2564));
																																					BgL_list1427z00_2653
																																						=
																																						CAR
																																						(CDR
																																						(CDR
																																							(BgL_pairz00_6074)));
																																				}
																																			BgL_zc3z04anonymousza32224ze3z87_2654:
																																				if (PAIRP(BgL_list1427z00_2653))
																																					{	/* Ast/let.scm 614 */
																																						bool_t
																																							BgL_test4267z00_11097;
																																						{	/* Ast/let.scm 613 */
																																							obj_t
																																								BgL_tmpz00_11098;
																																							BgL_tmpz00_11098
																																								=
																																								BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																																								(CAR
																																								(BgL_list1427z00_2653),
																																								BgL_bindingsz00_2563);
																																							BgL_test4267z00_11097
																																								=
																																								PAIRP
																																								(BgL_tmpz00_11098);
																																						}
																																						if (BgL_test4267z00_11097)
																																							{	/* Ast/let.scm 614 */
																																								BgL_tmpz00_11094
																																									=
																																									CAR
																																									(BgL_list1427z00_2653);
																																							}
																																						else
																																							{
																																								obj_t
																																									BgL_list1427z00_11103;
																																								BgL_list1427z00_11103
																																									=
																																									CDR
																																									(BgL_list1427z00_2653);
																																								BgL_list1427z00_2653
																																									=
																																									BgL_list1427z00_11103;
																																								goto
																																									BgL_zc3z04anonymousza32224ze3z87_2654;
																																							}
																																					}
																																				else
																																					{	/* Ast/let.scm 614 */
																																						BgL_tmpz00_11094
																																							=
																																							BFALSE;
																																					}
																																				BgL_test4265z00_11093
																																					=
																																					CBOOL
																																					(BgL_tmpz00_11094);
																																			}
																																		}
																																		if (BgL_test4265z00_11093)
																																			{	/* Ast/let.scm 612 */
																																				BgL_test4262z00_11087
																																					=
																																					(
																																					(bool_t)
																																					0);
																																			}
																																		else
																																			{	/* Ast/let.scm 612 */
																																				BgL_test4262z00_11087
																																					=
																																					(
																																					(bool_t)
																																					1);
																																			}
																																	}
																																else
																																	{	/* Ast/let.scm 611 */
																																		BgL_test4262z00_11087
																																			=
																																			((bool_t)
																																			0);
																																	}
																															}
																															if (BgL_test4262z00_11087)
																																{	/* Ast/let.scm 619 */
																																	obj_t
																																		BgL_arg2207z00_2632;
																																	obj_t
																																		BgL_arg2208z00_2633;
																																	BgL_arg2207z00_2632
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_bindingz00_2566,
																																		BgL_bindingsz00_2563);
																																	BgL_arg2208z00_2633
																																		=
																																		CDR(((obj_t)
																																			BgL_ebindingsz00_2564));
																																	{
																																		obj_t
																																			BgL_ebindingsz00_11116;
																																		obj_t
																																			BgL_bindingsz00_11115;
																																		obj_t
																																			BgL_exprz00_11114;
																																		BgL_exprz00_11114
																																			=
																																			BgL_subexprz00_2569;
																																		BgL_bindingsz00_11115
																																			=
																																			BgL_arg2207z00_2632;
																																		BgL_ebindingsz00_11116
																																			=
																																			BgL_arg2208z00_2633;
																																		BgL_ebindingsz00_2564
																																			=
																																			BgL_ebindingsz00_11116;
																																		BgL_bindingsz00_2563
																																			=
																																			BgL_bindingsz00_11115;
																																		BgL_exprz00_2562
																																			=
																																			BgL_exprz00_11114;
																																		goto
																																			BgL_zc3z04anonymousza32170ze3z87_2565;
																																	}
																																}
																															else
																																{	/* Ast/let.scm 611 */
																																	if (NULLP
																																		(BgL_bindingsz00_2563))
																																		{	/* Ast/let.scm 621 */
																																			obj_t
																																				BgL_arg2210z00_2635;
																																			{	/* Ast/let.scm 621 */
																																				obj_t
																																					BgL_arg2211z00_2636;
																																				obj_t
																																					BgL_arg2212z00_2637;
																																				BgL_arg2211z00_2636
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_bindingz00_2566,
																																					BNIL);
																																				{	/* Ast/let.scm 621 */
																																					obj_t
																																						BgL_arg2213z00_2638;
																																					{	/* Ast/let.scm 621 */
																																						obj_t
																																							BgL_arg2214z00_2639;
																																						BgL_arg2214z00_2639
																																							=
																																							CDR
																																							(((obj_t) BgL_ebindingsz00_2564));
																																						BgL_arg2213z00_2638
																																							=
																																							BGl_z62letcollapsez62zzast_letz00
																																							(BgL_arg2214z00_2639,
																																							BgL_subexprz00_2569);
																																					}
																																					BgL_arg2212z00_2637
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg2213z00_2638,
																																						BNIL);
																																				}
																																				BgL_arg2210z00_2635
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg2211z00_2636,
																																					BgL_arg2212z00_2637);
																																			}
																																			return
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(0),
																																				BgL_arg2210z00_2635);
																																		}
																																	else
																																		{	/* Ast/let.scm 623 */
																																			obj_t
																																				BgL_arg2215z00_2640;
																																			{	/* Ast/let.scm 623 */
																																				obj_t
																																					BgL_arg2216z00_2641;
																																				obj_t
																																					BgL_arg2217z00_2642;
																																				BgL_arg2216z00_2641
																																					=
																																					bgl_reverse
																																					(BgL_bindingsz00_2563);
																																				{	/* Ast/let.scm 624 */
																																					obj_t
																																						BgL_arg2218z00_2643;
																																					{	/* Ast/let.scm 624 */
																																						obj_t
																																							BgL_arg2219z00_2644;
																																						{	/* Ast/let.scm 624 */
																																							obj_t
																																								BgL_arg2220z00_2645;
																																							obj_t
																																								BgL_arg2221z00_2646;
																																							BgL_arg2220z00_2645
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_bindingz00_2566,
																																								BNIL);
																																							{	/* Ast/let.scm 624 */
																																								obj_t
																																									BgL_arg2222z00_2647;
																																								{	/* Ast/let.scm 624 */
																																									obj_t
																																										BgL_arg2223z00_2648;
																																									BgL_arg2223z00_2648
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_ebindingsz00_2564));
																																									BgL_arg2222z00_2647
																																										=
																																										BGl_z62letcollapsez62zzast_letz00
																																										(BgL_arg2223z00_2648,
																																										BgL_subexprz00_2569);
																																								}
																																								BgL_arg2221z00_2646
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg2222z00_2647,
																																									BNIL);
																																							}
																																							BgL_arg2219z00_2644
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg2220z00_2645,
																																								BgL_arg2221z00_2646);
																																						}
																																						BgL_arg2218z00_2643
																																							=
																																							MAKE_YOUNG_PAIR
																																							(CNST_TABLE_REF
																																							(0),
																																							BgL_arg2219z00_2644);
																																					}
																																					BgL_arg2217z00_2642
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg2218z00_2643,
																																						BNIL);
																																				}
																																				BgL_arg2215z00_2640
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg2216z00_2641,
																																					BgL_arg2217z00_2642);
																																			}
																																			return
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(0),
																																				BgL_arg2215z00_2640);
																																		}
																																}
																														}
																													}
																												else
																													{	/* Ast/let.scm 605 */
																													BgL_tagzd2392zd2_2571:
																														if (NULLP
																															(BgL_bindingsz00_2563))
																															{	/* Ast/let.scm 626 */
																																return
																																	BgL_exprz00_2562;
																															}
																														else
																															{	/* Ast/let.scm 627 */
																																obj_t
																																	BgL_arg2229z00_2666;
																																{	/* Ast/let.scm 627 */
																																	obj_t
																																		BgL_arg2230z00_2667;
																																	obj_t
																																		BgL_arg2231z00_2668;
																																	BgL_arg2230z00_2667
																																		=
																																		bgl_reverse
																																		(BgL_bindingsz00_2563);
																																	BgL_arg2231z00_2668
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_exprz00_2562,
																																		BNIL);
																																	BgL_arg2229z00_2666
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg2230z00_2667,
																																		BgL_arg2231z00_2668);
																																}
																																return
																																	MAKE_YOUNG_PAIR
																																	(CNST_TABLE_REF
																																	(0),
																																	BgL_arg2229z00_2666);
																															}
																													}
																											}
																										else
																											{	/* Ast/let.scm 605 */
																												goto
																													BgL_tagzd2392zd2_2571;
																											}
																									}
																								else
																									{	/* Ast/let.scm 605 */
																										goto BgL_tagzd2392zd2_2571;
																									}
																							}
																						else
																							{	/* Ast/let.scm 605 */
																								goto BgL_tagzd2392zd2_2571;
																							}
																					}
																				else
																					{	/* Ast/let.scm 605 */
																						goto BgL_tagzd2392zd2_2571;
																					}
																			}
																		else
																			{	/* Ast/let.scm 605 */
																				goto BgL_tagzd2392zd2_2571;
																			}
																	}
																else
																	{	/* Ast/let.scm 605 */
																		goto BgL_tagzd2392zd2_2571;
																	}
															}
														else
															{	/* Ast/let.scm 605 */
																goto BgL_tagzd2392zd2_2571;
															}
													}
												else
													{	/* Ast/let.scm 605 */
														goto BgL_tagzd2392zd2_2571;
													}
											}
										else
											{	/* Ast/let.scm 605 */
												goto BgL_tagzd2392zd2_2571;
											}
									}
								}
							}
						else
							{	/* Ast/let.scm 657 */
								if ((CAR(((obj_t) BgL_exprz00_2736)) == CNST_TABLE_REF(10)))
									{	/* Ast/let.scm 657 */
										BgL_ebindingsz00_2670 = BgL_ebindingsz00_2735;
										BgL_exprz00_2671 = BgL_exprz00_2736;
										{
											obj_t BgL_exprz00_2675;
											obj_t BgL_bindingsz00_2676;
											obj_t BgL_ebindingsz00_2677;

											BgL_exprz00_2675 = BgL_exprz00_2671;
											BgL_bindingsz00_2676 = BNIL;
											BgL_ebindingsz00_2677 = BgL_ebindingsz00_2670;
										BgL_zc3z04anonymousza32233ze3z87_2678:
											{
												obj_t BgL_bindingz00_2679;
												obj_t BgL_valz00_2680;
												obj_t BgL_subexprz00_2681;

												if (PAIRP(BgL_exprz00_2675))
													{	/* Ast/let.scm 631 */
														obj_t BgL_cdrzd2444zd2_2686;

														BgL_cdrzd2444zd2_2686 =
															CDR(((obj_t) BgL_exprz00_2675));
														if (
															(CAR(
																	((obj_t) BgL_exprz00_2675)) ==
																CNST_TABLE_REF(10)))
															{	/* Ast/let.scm 631 */
																if (PAIRP(BgL_cdrzd2444zd2_2686))
																	{	/* Ast/let.scm 631 */
																		obj_t BgL_carzd2448zd2_2690;
																		obj_t BgL_cdrzd2449zd2_2691;

																		BgL_carzd2448zd2_2690 =
																			CAR(BgL_cdrzd2444zd2_2686);
																		BgL_cdrzd2449zd2_2691 =
																			CDR(BgL_cdrzd2444zd2_2686);
																		if (PAIRP(BgL_carzd2448zd2_2690))
																			{	/* Ast/let.scm 631 */
																				obj_t BgL_carzd2452zd2_2693;

																				BgL_carzd2452zd2_2693 =
																					CAR(BgL_carzd2448zd2_2690);
																				if (PAIRP(BgL_carzd2452zd2_2693))
																					{	/* Ast/let.scm 631 */
																						obj_t BgL_cdrzd2460zd2_2695;

																						BgL_cdrzd2460zd2_2695 =
																							CDR(BgL_carzd2452zd2_2693);
																						if (PAIRP(BgL_cdrzd2460zd2_2695))
																							{	/* Ast/let.scm 631 */
																								if (NULLP(CDR
																										(BgL_cdrzd2460zd2_2695)))
																									{	/* Ast/let.scm 631 */
																										if (NULLP(CDR
																												(BgL_carzd2448zd2_2690)))
																											{	/* Ast/let.scm 631 */
																												if (PAIRP
																													(BgL_cdrzd2449zd2_2691))
																													{	/* Ast/let.scm 631 */
																														if (NULLP(CDR
																																(BgL_cdrzd2449zd2_2691)))
																															{	/* Ast/let.scm 631 */
																																BgL_bindingz00_2679
																																	=
																																	BgL_carzd2452zd2_2693;
																																BgL_valz00_2680
																																	=
																																	CAR
																																	(BgL_cdrzd2460zd2_2695);
																																BgL_subexprz00_2681
																																	=
																																	CAR
																																	(BgL_cdrzd2449zd2_2691);
																																if (CBOOL
																																	(BGl_functionzf3zf3zzast_letz00
																																		(BgL_valz00_2680,
																																			BTRUE)))
																																	{	/* Ast/let.scm 638 */
																																		obj_t
																																			BgL_arg2255z00_2711;
																																		BgL_arg2255z00_2711
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_bindingz00_2679,
																																			BgL_bindingsz00_2676);
																																		{
																																			obj_t
																																				BgL_bindingsz00_11192;
																																			obj_t
																																				BgL_exprz00_11191;
																																			BgL_exprz00_11191
																																				=
																																				BgL_subexprz00_2681;
																																			BgL_bindingsz00_11192
																																				=
																																				BgL_arg2255z00_2711;
																																			BgL_bindingsz00_2676
																																				=
																																				BgL_bindingsz00_11192;
																																			BgL_exprz00_2675
																																				=
																																				BgL_exprz00_11191;
																																			goto
																																				BgL_zc3z04anonymousza32233ze3z87_2678;
																																		}
																																	}
																																else
																																	{	/* Ast/let.scm 637 */
																																		if (NULLP
																																			(BgL_bindingsz00_2676))
																																			{	/* Ast/let.scm 640 */
																																				obj_t
																																					BgL_arg2257z00_2713;
																																				{	/* Ast/let.scm 640 */
																																					obj_t
																																						BgL_arg2258z00_2714;
																																					obj_t
																																						BgL_arg2259z00_2715;
																																					BgL_arg2258z00_2714
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_bindingz00_2679,
																																						BNIL);
																																					{	/* Ast/let.scm 640 */
																																						obj_t
																																							BgL_arg2260z00_2716;
																																						{	/* Ast/let.scm 640 */
																																							obj_t
																																								BgL_arg2261z00_2717;
																																							BgL_arg2261z00_2717
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_ebindingsz00_2677));
																																							BgL_arg2260z00_2716
																																								=
																																								BGl_z62letcollapsez62zzast_letz00
																																								(BgL_arg2261z00_2717,
																																								BgL_subexprz00_2681);
																																						}
																																						BgL_arg2259z00_2715
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg2260z00_2716,
																																							BNIL);
																																					}
																																					BgL_arg2257z00_2713
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg2258z00_2714,
																																						BgL_arg2259z00_2715);
																																				}
																																				return
																																					MAKE_YOUNG_PAIR
																																					(CNST_TABLE_REF
																																					(10),
																																					BgL_arg2257z00_2713);
																																			}
																																		else
																																			{	/* Ast/let.scm 642 */
																																				obj_t
																																					BgL_arg2262z00_2718;
																																				{	/* Ast/let.scm 642 */
																																					obj_t
																																						BgL_arg2263z00_2719;
																																					obj_t
																																						BgL_arg2264z00_2720;
																																					BgL_arg2263z00_2719
																																						=
																																						bgl_reverse
																																						(BgL_bindingsz00_2676);
																																					{	/* Ast/let.scm 643 */
																																						obj_t
																																							BgL_arg2265z00_2721;
																																						{	/* Ast/let.scm 643 */
																																							obj_t
																																								BgL_arg2266z00_2722;
																																							{	/* Ast/let.scm 643 */
																																								obj_t
																																									BgL_arg2267z00_2723;
																																								obj_t
																																									BgL_arg2268z00_2724;
																																								BgL_arg2267z00_2723
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_bindingz00_2679,
																																									BNIL);
																																								{	/* Ast/let.scm 643 */
																																									obj_t
																																										BgL_arg2269z00_2725;
																																									{	/* Ast/let.scm 643 */
																																										obj_t
																																											BgL_arg2270z00_2726;
																																										BgL_arg2270z00_2726
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_ebindingsz00_2677));
																																										BgL_arg2269z00_2725
																																											=
																																											BGl_z62letcollapsez62zzast_letz00
																																											(BgL_arg2270z00_2726,
																																											BgL_subexprz00_2681);
																																									}
																																									BgL_arg2268z00_2724
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_arg2269z00_2725,
																																										BNIL);
																																								}
																																								BgL_arg2266z00_2722
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg2267z00_2723,
																																									BgL_arg2268z00_2724);
																																							}
																																							BgL_arg2265z00_2721
																																								=
																																								MAKE_YOUNG_PAIR
																																								(CNST_TABLE_REF
																																								(10),
																																								BgL_arg2266z00_2722);
																																						}
																																						BgL_arg2264z00_2720
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg2265z00_2721,
																																							BNIL);
																																					}
																																					BgL_arg2262z00_2718
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg2263z00_2719,
																																						BgL_arg2264z00_2720);
																																				}
																																				return
																																					MAKE_YOUNG_PAIR
																																					(CNST_TABLE_REF
																																					(10),
																																					BgL_arg2262z00_2718);
																																			}
																																	}
																															}
																														else
																															{	/* Ast/let.scm 631 */
																															BgL_tagzd2435zd2_2683:
																																if (NULLP
																																	(BgL_bindingsz00_2676))
																																	{	/* Ast/let.scm 645 */
																																		return
																																			BgL_exprz00_2675;
																																	}
																																else
																																	{	/* Ast/let.scm 646 */
																																		obj_t
																																			BgL_arg2272z00_2728;
																																		{	/* Ast/let.scm 646 */
																																			obj_t
																																				BgL_arg2273z00_2729;
																																			obj_t
																																				BgL_arg2274z00_2730;
																																			BgL_arg2273z00_2729
																																				=
																																				bgl_reverse
																																				(BgL_bindingsz00_2676);
																																			BgL_arg2274z00_2730
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_exprz00_2675,
																																				BNIL);
																																			BgL_arg2272z00_2728
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg2273z00_2729,
																																				BgL_arg2274z00_2730);
																																		}
																																		return
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(10),
																																			BgL_arg2272z00_2728);
																																	}
																															}
																													}
																												else
																													{	/* Ast/let.scm 631 */
																														goto
																															BgL_tagzd2435zd2_2683;
																													}
																											}
																										else
																											{	/* Ast/let.scm 631 */
																												goto
																													BgL_tagzd2435zd2_2683;
																											}
																									}
																								else
																									{	/* Ast/let.scm 631 */
																										goto BgL_tagzd2435zd2_2683;
																									}
																							}
																						else
																							{	/* Ast/let.scm 631 */
																								goto BgL_tagzd2435zd2_2683;
																							}
																					}
																				else
																					{	/* Ast/let.scm 631 */
																						goto BgL_tagzd2435zd2_2683;
																					}
																			}
																		else
																			{	/* Ast/let.scm 631 */
																				goto BgL_tagzd2435zd2_2683;
																			}
																	}
																else
																	{	/* Ast/let.scm 631 */
																		goto BgL_tagzd2435zd2_2683;
																	}
															}
														else
															{	/* Ast/let.scm 631 */
																goto BgL_tagzd2435zd2_2683;
															}
													}
												else
													{	/* Ast/let.scm 631 */
														goto BgL_tagzd2435zd2_2683;
													}
											}
										}
									}
								else
									{	/* Ast/let.scm 657 */
										if ((CAR(((obj_t) BgL_exprz00_2736)) == CNST_TABLE_REF(8)))
											{	/* Ast/let.scm 657 */
												return BgL_exprz00_2736;
											}
										else
											{	/* Ast/let.scm 657 */
												return BgL_exprz00_2736;
											}
									}
							}
					}
				else
					{	/* Ast/let.scm 657 */
						return BgL_exprz00_2736;
					}
			}
		}

	}



/* &stage1 */
	BgL_nodez00_bglt BGl_z62stage1z62zzast_letz00(obj_t BgL_envz00_7772,
		obj_t BgL_ebindingsz00_7781, obj_t BgL_bodyz00_7782)
	{
		{	/* Ast/let.scm 1136 */
			{	/* Ast/let.scm 1115 */
				obj_t BgL_stage7z00_7773;
				obj_t BgL_stage5z00_7774;
				obj_t BgL_stage4z00_7775;
				obj_t BgL_stage3z00_7776;
				obj_t BgL_stackz00_7777;
				obj_t BgL_locz00_7778;
				obj_t BgL_sitez00_7779;
				obj_t BgL_stage2z00_7780;

				BgL_stage7z00_7773 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_7772, (int) (0L)));
				BgL_stage5z00_7774 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_7772, (int) (1L)));
				BgL_stage4z00_7775 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_7772, (int) (2L)));
				BgL_stage3z00_7776 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_7772, (int) (3L)));
				BgL_stackz00_7777 = PROCEDURE_L_REF(BgL_envz00_7772, (int) (4L));
				BgL_locz00_7778 = PROCEDURE_L_REF(BgL_envz00_7772, (int) (5L));
				BgL_sitez00_7779 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_7772, (int) (6L)));
				BgL_stage2z00_7780 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_7772, (int) (7L)));
				{
					obj_t BgL_ebindingsz00_7863;
					obj_t BgL_bodyz00_7864;
					obj_t BgL_splitz00_7865;
					obj_t BgL_kontz00_7866;

					{	/* Ast/let.scm 1115 */
						obj_t BgL_splitz00_7883;

						{	/* Ast/let.scm 1115 */
							int BgL_tmpz00_11252;

							BgL_tmpz00_11252 = (int) (0L);
							BgL_splitz00_7883 = MAKE_EL_PROCEDURE(BgL_tmpz00_11252);
						}
						BgL_ebindingsz00_7863 = BgL_ebindingsz00_7781;
						BgL_bodyz00_7864 = BgL_bodyz00_7782;
						BgL_splitz00_7865 = BgL_splitz00_7883;
						BgL_kontz00_7866 = BgL_stage2z00_7780;
						if (NULLP(BgL_ebindingsz00_7863))
							{	/* Ast/let.scm 739 */
								return
									BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_bodyz00_7864,
									BgL_stackz00_7777, BgL_locz00_7778, BgL_sitez00_7779);
							}
						else
							{	/* Ast/let.scm 742 */
								obj_t BgL_recza2zd2bindingsz70_7867;

								BgL_recza2zd2bindingsz70_7867 =
									BGl_z62splitz62zzast_letz00(BgL_splitz00_7865,
									BgL_ebindingsz00_7863);
								{	/* Ast/let.scm 743 */
									obj_t BgL_tailzd2bindingszd2_7868;

									{	/* Ast/let.scm 749 */
										obj_t BgL_tmpz00_7869;

										{	/* Ast/let.scm 749 */
											int BgL_tmpz00_11262;

											BgL_tmpz00_11262 = (int) (1L);
											BgL_tmpz00_7869 = BGL_MVALUES_VAL(BgL_tmpz00_11262);
										}
										{	/* Ast/let.scm 749 */
											int BgL_tmpz00_11265;

											BgL_tmpz00_11265 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_11265, BUNSPEC);
										}
										BgL_tailzd2bindingszd2_7868 = BgL_tmpz00_7869;
									}
									if (NULLP(BgL_tailzd2bindingszd2_7868))
										{	/* Ast/let.scm 749 */
											return
												BGl_z62stage2z62zzast_letz00(BgL_kontz00_7866,
												BgL_ebindingsz00_7863, BgL_bodyz00_7864);
										}
									else
										{	/* Ast/let.scm 749 */
											if (NULLP(BgL_recza2zd2bindingsz70_7867))
												{	/* Ast/let.scm 751 */
													return
														BGl_sexpzd2ze3nodez31zzast_sexpz00
														(BGl_z62letstarz62zzast_letz00
														(BgL_tailzd2bindingszd2_7868, BgL_bodyz00_7864),
														BgL_stackz00_7777, BgL_locz00_7778,
														BgL_sitez00_7779);
												}
											else
												{	/* Ast/let.scm 753 */
													obj_t BgL_arg2391z00_7870;

													{	/* Ast/let.scm 753 */
														obj_t BgL_arg2392z00_7871;

														{	/* Ast/let.scm 753 */
															obj_t BgL_arg2393z00_7872;
															obj_t BgL_arg2395z00_7873;

															{	/* Ast/let.scm 753 */
																obj_t BgL_head1461z00_7874;

																BgL_head1461z00_7874 =
																	MAKE_YOUNG_PAIR(CAR(CAR
																		(BgL_recza2zd2bindingsz70_7867)), BNIL);
																{	/* Ast/let.scm 753 */
																	obj_t BgL_g1464z00_7875;

																	BgL_g1464z00_7875 =
																		CDR(BgL_recza2zd2bindingsz70_7867);
																	{
																		obj_t BgL_l1459z00_7877;
																		obj_t BgL_tail1462z00_7878;

																		BgL_l1459z00_7877 = BgL_g1464z00_7875;
																		BgL_tail1462z00_7878 = BgL_head1461z00_7874;
																	BgL_zc3z04anonymousza32397ze3z87_7876:
																		if (NULLP(BgL_l1459z00_7877))
																			{	/* Ast/let.scm 753 */
																				BgL_arg2393z00_7872 =
																					BgL_head1461z00_7874;
																			}
																		else
																			{	/* Ast/let.scm 753 */
																				obj_t BgL_newtail1463z00_7879;

																				{	/* Ast/let.scm 753 */
																					obj_t BgL_arg2401z00_7880;

																					{	/* Ast/let.scm 571 */
																						obj_t BgL_pairz00_7881;

																						BgL_pairz00_7881 =
																							CAR(((obj_t) BgL_l1459z00_7877));
																						BgL_arg2401z00_7880 =
																							CAR(BgL_pairz00_7881);
																					}
																					BgL_newtail1463z00_7879 =
																						MAKE_YOUNG_PAIR(BgL_arg2401z00_7880,
																						BNIL);
																				}
																				SET_CDR(BgL_tail1462z00_7878,
																					BgL_newtail1463z00_7879);
																				{	/* Ast/let.scm 753 */
																					obj_t BgL_arg2399z00_7882;

																					BgL_arg2399z00_7882 =
																						CDR(((obj_t) BgL_l1459z00_7877));
																					{
																						obj_t BgL_tail1462z00_11293;
																						obj_t BgL_l1459z00_11292;

																						BgL_l1459z00_11292 =
																							BgL_arg2399z00_7882;
																						BgL_tail1462z00_11293 =
																							BgL_newtail1463z00_7879;
																						BgL_tail1462z00_7878 =
																							BgL_tail1462z00_11293;
																						BgL_l1459z00_7877 =
																							BgL_l1459z00_11292;
																						goto
																							BgL_zc3z04anonymousza32397ze3z87_7876;
																					}
																				}
																			}
																	}
																}
															}
															BgL_arg2395z00_7873 =
																MAKE_YOUNG_PAIR(BGl_z62letstarz62zzast_letz00
																(BgL_tailzd2bindingszd2_7868, BgL_bodyz00_7864),
																BNIL);
															BgL_arg2392z00_7871 =
																MAKE_YOUNG_PAIR(BgL_arg2393z00_7872,
																BgL_arg2395z00_7873);
														}
														BgL_arg2391z00_7870 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(4),
															BgL_arg2392z00_7871);
													}
													return
														BGl_sexpzd2ze3nodez31zzast_sexpz00
														(BgL_arg2391z00_7870, BgL_stackz00_7777,
														BgL_locz00_7778, BgL_sitez00_7779);
												}
										}
								}
							}
					}
				}
			}
		}

	}



/* &split3801 */
	obj_t BGl_z62split3801z62zzast_letz00(obj_t BgL_envz00_7783,
		obj_t BgL_ebindingsz00_7784)
	{
		{	/* Ast/let.scm 1160 */
			{
				obj_t BgL_ebindingsz00_7885;
				obj_t BgL_reczd2bindingszd2_7886;
				obj_t BgL_recza2zd2bindingsz70_7887;

				BgL_ebindingsz00_7885 = BgL_ebindingsz00_7784;
				BgL_reczd2bindingszd2_7886 = BNIL;
				BgL_recza2zd2bindingsz70_7887 = BNIL;
			BgL_loopz00_7884:
				if (NULLP(BgL_ebindingsz00_7885))
					{	/* Ast/let.scm 1150 */
						obj_t BgL_val0_1560z00_7888;
						obj_t BgL_val1_1561z00_7889;

						BgL_val0_1560z00_7888 =
							bgl_reverse_bang(BgL_reczd2bindingszd2_7886);
						BgL_val1_1561z00_7889 =
							bgl_reverse_bang(BgL_recza2zd2bindingsz70_7887);
						{	/* Ast/let.scm 1150 */
							int BgL_tmpz00_11304;

							BgL_tmpz00_11304 = (int) (2L);
							BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11304);
						}
						{	/* Ast/let.scm 1150 */
							int BgL_tmpz00_11307;

							BgL_tmpz00_11307 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_11307, BgL_val1_1561z00_7889);
						}
						return BgL_val0_1560z00_7888;
					}
				else
					{	/* Ast/let.scm 1151 */
						bool_t BgL_test4290z00_11310;

						{	/* Ast/let.scm 1151 */
							bool_t BgL_test4291z00_11311;

							{	/* Ast/let.scm 1151 */
								obj_t BgL_arg3407z00_7890;

								{	/* Ast/let.scm 572 */
									obj_t BgL_pairz00_7891;

									BgL_pairz00_7891 =
										CAR(((obj_t) CAR(((obj_t) BgL_ebindingsz00_7885))));
									BgL_arg3407z00_7890 = CAR(CDR(BgL_pairz00_7891));
								}
								BgL_test4291z00_11311 =
									BGl_loopze70ze7zzast_letz00(BgL_arg3407z00_7890);
							}
							if (BgL_test4291z00_11311)
								{	/* Ast/let.scm 1151 */
									BgL_test4290z00_11310 = ((bool_t) 0);
								}
							else
								{	/* Ast/let.scm 1152 */
									obj_t BgL_arg3406z00_7892;

									BgL_arg3406z00_7892 = CAR(((obj_t) BgL_ebindingsz00_7885));
									if (CBOOL(BGl_z62mutablezd2inzf3z43zzast_letz00
											(BgL_arg3406z00_7892, BgL_recza2zd2bindingsz70_7887)))
										{	/* Ast/let.scm 585 */
											BgL_test4290z00_11310 = ((bool_t) 0);
										}
									else
										{	/* Ast/let.scm 585 */
											BgL_test4290z00_11310 = ((bool_t) 1);
										}
								}
						}
						if (BgL_test4290z00_11310)
							{	/* Ast/let.scm 1154 */
								obj_t BgL_arg3400z00_7893;
								obj_t BgL_arg3401z00_7894;

								BgL_arg3400z00_7893 = CDR(((obj_t) BgL_ebindingsz00_7885));
								{	/* Ast/let.scm 1155 */
									obj_t BgL_arg3402z00_7895;

									BgL_arg3402z00_7895 = CAR(((obj_t) BgL_ebindingsz00_7885));
									BgL_arg3401z00_7894 =
										MAKE_YOUNG_PAIR(BgL_arg3402z00_7895,
										BgL_reczd2bindingszd2_7886);
								}
								{
									obj_t BgL_reczd2bindingszd2_11330;
									obj_t BgL_ebindingsz00_11329;

									BgL_ebindingsz00_11329 = BgL_arg3400z00_7893;
									BgL_reczd2bindingszd2_11330 = BgL_arg3401z00_7894;
									BgL_reczd2bindingszd2_7886 = BgL_reczd2bindingszd2_11330;
									BgL_ebindingsz00_7885 = BgL_ebindingsz00_11329;
									goto BgL_loopz00_7884;
								}
							}
						else
							{	/* Ast/let.scm 1158 */
								obj_t BgL_arg3403z00_7896;
								obj_t BgL_arg3404z00_7897;

								BgL_arg3403z00_7896 = CDR(((obj_t) BgL_ebindingsz00_7885));
								{	/* Ast/let.scm 1160 */
									obj_t BgL_arg3405z00_7898;

									BgL_arg3405z00_7898 = CAR(((obj_t) BgL_ebindingsz00_7885));
									BgL_arg3404z00_7897 =
										MAKE_YOUNG_PAIR(BgL_arg3405z00_7898,
										BgL_recza2zd2bindingsz70_7887);
								}
								{
									obj_t BgL_recza2zd2bindingsz70_11337;
									obj_t BgL_ebindingsz00_11336;

									BgL_ebindingsz00_11336 = BgL_arg3403z00_7896;
									BgL_recza2zd2bindingsz70_11337 = BgL_arg3404z00_7897;
									BgL_recza2zd2bindingsz70_7887 =
										BgL_recza2zd2bindingsz70_11337;
									BgL_ebindingsz00_7885 = BgL_ebindingsz00_11336;
									goto BgL_loopz00_7884;
								}
							}
					}
			}
		}

	}



/* &stage2 */
	BgL_nodez00_bglt BGl_z62stage2z62zzast_letz00(obj_t BgL_envz00_7785,
		obj_t BgL_ebindingsz00_7793, obj_t BgL_bodyz00_7794)
	{
		{	/* Ast/let.scm 1104 */
			{	/* Ast/let.scm 1078 */
				obj_t BgL_stage7z00_7786;
				obj_t BgL_stackz00_7787;
				obj_t BgL_locz00_7788;
				obj_t BgL_sitez00_7789;
				obj_t BgL_stage5z00_7790;
				obj_t BgL_stage4z00_7791;
				obj_t BgL_stage3z00_7792;

				BgL_stage7z00_7786 =
					((obj_t) PROCEDURE_EL_REF(BgL_envz00_7785, (int) (0L)));
				BgL_stackz00_7787 = PROCEDURE_EL_REF(BgL_envz00_7785, (int) (1L));
				BgL_locz00_7788 = PROCEDURE_EL_REF(BgL_envz00_7785, (int) (2L));
				BgL_sitez00_7789 =
					((obj_t) PROCEDURE_EL_REF(BgL_envz00_7785, (int) (3L)));
				BgL_stage5z00_7790 =
					((obj_t) PROCEDURE_EL_REF(BgL_envz00_7785, (int) (4L)));
				BgL_stage4z00_7791 =
					((obj_t) PROCEDURE_EL_REF(BgL_envz00_7785, (int) (5L)));
				BgL_stage3z00_7792 =
					((obj_t) PROCEDURE_EL_REF(BgL_envz00_7785, (int) (6L)));
				return
					BGl_z62splitzd2headzd2letrecz62zzast_letz00(BgL_sitez00_7789,
					BgL_locz00_7788, BgL_stackz00_7787, BgL_ebindingsz00_7793,
					BgL_bodyz00_7794, BGl_proc3811z00zzast_letz00, BgL_stage3z00_7792);
			}
		}

	}



/* &split */
	obj_t BGl_z62splitz62zzast_letz00(obj_t BgL_envz00_7795,
		obj_t BgL_ebindingsz00_7796)
	{
		{	/* Ast/let.scm 1136 */
			{	/* Ast/let.scm 1115 */
				obj_t BgL_g1208z00_7900;

				BgL_g1208z00_7900 = bgl_reverse(BgL_ebindingsz00_7796);
				{
					obj_t BgL_rebindingsz00_7902;
					obj_t BgL_letza2zd2bindingsz70_7903;

					BgL_rebindingsz00_7902 = BgL_g1208z00_7900;
					BgL_letza2zd2bindingsz70_7903 = BNIL;
				BgL_loopz00_7901:
					if (NULLP(BgL_rebindingsz00_7902))
						{	/* Ast/let.scm 1118 */
							{	/* Ast/let.scm 1119 */
								int BgL_tmpz00_11361;

								BgL_tmpz00_11361 = (int) (2L);
								BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11361);
							}
							{	/* Ast/let.scm 1119 */
								int BgL_tmpz00_11364;

								BgL_tmpz00_11364 = (int) (1L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_11364,
									BgL_letza2zd2bindingsz70_7903);
							}
							return BNIL;
						}
					else
						{	/* Ast/let.scm 1120 */
							bool_t BgL_test4294z00_11367;

							{	/* Ast/let.scm 1120 */
								bool_t BgL_test4295z00_11368;

								{	/* Ast/let.scm 1120 */
									obj_t BgL_g1556z00_7904;

									{	/* Ast/let.scm 574 */
										obj_t BgL_pairz00_7905;

										BgL_pairz00_7905 = CAR(((obj_t) BgL_rebindingsz00_7902));
										BgL_g1556z00_7904 = CAR(CDR(CDR(BgL_pairz00_7905)));
									}
									{
										obj_t BgL_l1554z00_7907;

										BgL_l1554z00_7907 = BgL_g1556z00_7904;
									BgL_zc3z04anonymousza33383ze3z87_7906:
										if (NULLP(BgL_l1554z00_7907))
											{	/* Ast/let.scm 1124 */
												BgL_test4295z00_11368 = ((bool_t) 1);
											}
										else
											{	/* Ast/let.scm 1121 */
												obj_t BgL_nvz00_7908;

												{	/* Ast/let.scm 1121 */
													obj_t BgL_varz00_7909;

													BgL_varz00_7909 = CAR(((obj_t) BgL_l1554z00_7907));
													{	/* Ast/let.scm 1121 */
														obj_t BgL_g1553z00_7910;

														BgL_g1553z00_7910 =
															CDR(((obj_t) BgL_rebindingsz00_7902));
														{
															obj_t BgL_list1552z00_7912;

															BgL_list1552z00_7912 = BgL_g1553z00_7910;
														BgL_zc3z04anonymousza33385ze3z87_7911:
															if (PAIRP(BgL_list1552z00_7912))
																{	/* Ast/let.scm 1123 */
																	bool_t BgL_test4298z00_11382;

																	{	/* Ast/let.scm 1122 */
																		obj_t BgL_tmpz00_11383;

																		{	/* Ast/let.scm 573 */
																			obj_t BgL_pairz00_7913;

																			BgL_pairz00_7913 =
																				CDR(
																				((obj_t) CAR(BgL_list1552z00_7912)));
																			BgL_tmpz00_11383 = CAR(BgL_pairz00_7913);
																		}
																		BgL_test4298z00_11382 =
																			(BgL_varz00_7909 == BgL_tmpz00_11383);
																	}
																	if (BgL_test4298z00_11382)
																		{	/* Ast/let.scm 1123 */
																			BgL_nvz00_7908 =
																				CAR(BgL_list1552z00_7912);
																		}
																	else
																		{
																			obj_t BgL_list1552z00_11390;

																			BgL_list1552z00_11390 =
																				CDR(BgL_list1552z00_7912);
																			BgL_list1552z00_7912 =
																				BgL_list1552z00_11390;
																			goto
																				BgL_zc3z04anonymousza33385ze3z87_7911;
																		}
																}
															else
																{	/* Ast/let.scm 1123 */
																	BgL_nvz00_7908 = BFALSE;
																}
														}
													}
												}
												if (CBOOL(BgL_nvz00_7908))
													{	/* Ast/let.scm 1124 */
														obj_t BgL_arg3384z00_7914;

														BgL_arg3384z00_7914 =
															CDR(((obj_t) BgL_l1554z00_7907));
														{
															obj_t BgL_l1554z00_11396;

															BgL_l1554z00_11396 = BgL_arg3384z00_7914;
															BgL_l1554z00_7907 = BgL_l1554z00_11396;
															goto BgL_zc3z04anonymousza33383ze3z87_7906;
														}
													}
												else
													{	/* Ast/let.scm 1124 */
														BgL_test4295z00_11368 = ((bool_t) 0);
													}
											}
									}
								}
								if (BgL_test4295z00_11368)
									{	/* Ast/let.scm 1127 */
										bool_t BgL_test4300z00_11397;

										{	/* Ast/let.scm 1127 */
											obj_t BgL_arg3381z00_7915;
											obj_t BgL_arg3382z00_7916;

											BgL_arg3381z00_7915 =
												CAR(((obj_t) BgL_rebindingsz00_7902));
											BgL_arg3382z00_7916 =
												CDR(((obj_t) BgL_rebindingsz00_7902));
											BgL_test4300z00_11397 =
												CBOOL(BGl_z62usedzd2inzf3z43zzast_letz00
												(BgL_arg3381z00_7915, BgL_arg3382z00_7916));
										}
										if (BgL_test4300z00_11397)
											{	/* Ast/let.scm 1127 */
												BgL_test4294z00_11367 = ((bool_t) 0);
											}
										else
											{	/* Ast/let.scm 1127 */
												BgL_test4294z00_11367 = ((bool_t) 1);
											}
									}
								else
									{	/* Ast/let.scm 1120 */
										BgL_test4294z00_11367 = ((bool_t) 0);
									}
							}
							if (BgL_test4294z00_11367)
								{	/* Ast/let.scm 1128 */
									obj_t BgL_arg3373z00_7917;
									obj_t BgL_arg3375z00_7918;

									BgL_arg3373z00_7917 = CDR(((obj_t) BgL_rebindingsz00_7902));
									{	/* Ast/let.scm 1129 */
										obj_t BgL_arg3379z00_7919;

										BgL_arg3379z00_7919 = CAR(((obj_t) BgL_rebindingsz00_7902));
										BgL_arg3375z00_7918 =
											MAKE_YOUNG_PAIR(BgL_arg3379z00_7919,
											BgL_letza2zd2bindingsz70_7903);
									}
									{
										obj_t BgL_letza2zd2bindingsz70_11410;
										obj_t BgL_rebindingsz00_11409;

										BgL_rebindingsz00_11409 = BgL_arg3373z00_7917;
										BgL_letza2zd2bindingsz70_11410 = BgL_arg3375z00_7918;
										BgL_letza2zd2bindingsz70_7903 =
											BgL_letza2zd2bindingsz70_11410;
										BgL_rebindingsz00_7902 = BgL_rebindingsz00_11409;
										goto BgL_loopz00_7901;
									}
								}
							else
								{	/* Ast/let.scm 1136 */
									obj_t BgL_val0_1558z00_7920;

									BgL_val0_1558z00_7920 = bgl_reverse(BgL_rebindingsz00_7902);
									{	/* Ast/let.scm 1136 */
										int BgL_tmpz00_11412;

										BgL_tmpz00_11412 = (int) (2L);
										BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11412);
									}
									{	/* Ast/let.scm 1136 */
										int BgL_tmpz00_11415;

										BgL_tmpz00_11415 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_11415,
											BgL_letza2zd2bindingsz70_7903);
									}
									return BgL_val0_1558z00_7920;
								}
						}
				}
			}
		}

	}



/* &stage3 */
	BgL_nodez00_bglt BGl_z62stage3z62zzast_letz00(obj_t BgL_envz00_7797,
		obj_t BgL_ebindingsz00_7804, obj_t BgL_bodyz00_7805)
	{
		{	/* Ast/let.scm 1068 */
			{	/* Ast/let.scm 1046 */
				obj_t BgL_stage7z00_7798;
				obj_t BgL_stackz00_7799;
				obj_t BgL_locz00_7800;
				obj_t BgL_sitez00_7801;
				obj_t BgL_stage5z00_7802;
				obj_t BgL_stage4z00_7803;

				BgL_stage7z00_7798 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_7797, (int) (0L)));
				BgL_stackz00_7799 = PROCEDURE_L_REF(BgL_envz00_7797, (int) (1L));
				BgL_locz00_7800 = PROCEDURE_L_REF(BgL_envz00_7797, (int) (2L));
				BgL_sitez00_7801 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_7797, (int) (3L)));
				BgL_stage5z00_7802 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_7797, (int) (4L)));
				BgL_stage4z00_7803 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_7797, (int) (5L)));
				return
					BGl_z62splitzd2headzd2letrecz62zzast_letz00(BgL_sitez00_7801,
					BgL_locz00_7800, BgL_stackz00_7799, BgL_ebindingsz00_7804,
					BgL_bodyz00_7805, BGl_proc3812z00zzast_letz00, BgL_stage4z00_7803);
			}
		}

	}



/* &split3802 */
	obj_t BGl_z62split3802z62zzast_letz00(obj_t BgL_envz00_7806,
		obj_t BgL_ebindingsz00_7807)
	{
		{	/* Ast/let.scm 1104 */
			{
				obj_t BgL_ebindingsz00_7923;
				obj_t BgL_valzd2bindingszd2_7924;
				obj_t BgL_recza2zd2bindingsz70_7925;

				BgL_ebindingsz00_7923 = BgL_ebindingsz00_7807;
				BgL_valzd2bindingszd2_7924 = BNIL;
				BgL_recza2zd2bindingsz70_7925 = BNIL;
			BgL_loopz00_7922:
				if (NULLP(BgL_ebindingsz00_7923))
					{	/* Ast/let.scm 1083 */
						obj_t BgL_val0_1539z00_7926;
						obj_t BgL_val1_1540z00_7927;

						BgL_val0_1539z00_7926 =
							bgl_reverse_bang(BgL_valzd2bindingszd2_7924);
						BgL_val1_1540z00_7927 =
							bgl_reverse_bang(BgL_recza2zd2bindingsz70_7925);
						{	/* Ast/let.scm 1083 */
							int BgL_tmpz00_11439;

							BgL_tmpz00_11439 = (int) (2L);
							BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11439);
						}
						{	/* Ast/let.scm 1083 */
							int BgL_tmpz00_11442;

							BgL_tmpz00_11442 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_11442, BgL_val1_1540z00_7927);
						}
						return BgL_val0_1539z00_7926;
					}
				else
					{	/* Ast/let.scm 1084 */
						bool_t BgL_test4302z00_11445;

						{	/* Ast/let.scm 1084 */
							bool_t BgL_test4303z00_11446;

							{	/* Ast/let.scm 1084 */
								obj_t BgL_arg3322z00_7928;

								{	/* Ast/let.scm 572 */
									obj_t BgL_pairz00_7929;

									BgL_pairz00_7929 =
										CAR(((obj_t) CAR(((obj_t) BgL_ebindingsz00_7923))));
									BgL_arg3322z00_7928 = CAR(CDR(BgL_pairz00_7929));
								}
								{	/* Ast/let.scm 1084 */

									BgL_test4303z00_11446 =
										BGl_directzd2functionzf3z21zzast_letz00(BgL_arg3322z00_7928,
										((bool_t) 0));
								}
							}
							if (BgL_test4303z00_11446)
								{	/* Ast/let.scm 1085 */
									obj_t BgL_arg3318z00_7930;
									obj_t BgL_arg3321z00_7931;

									BgL_arg3318z00_7930 = CAR(((obj_t) BgL_ebindingsz00_7923));
									BgL_arg3321z00_7931 = CDR(((obj_t) BgL_ebindingsz00_7923));
									if (CBOOL(BGl_z62mutablezd2inzf3z43zzast_letz00
											(BgL_arg3318z00_7930, ((obj_t) BgL_arg3321z00_7931))))
										{	/* Ast/let.scm 585 */
											BgL_test4302z00_11445 = ((bool_t) 0);
										}
									else
										{	/* Ast/let.scm 585 */
											BgL_test4302z00_11445 = ((bool_t) 1);
										}
								}
							else
								{	/* Ast/let.scm 1084 */
									BgL_test4302z00_11445 = ((bool_t) 0);
								}
						}
						if (BgL_test4302z00_11445)
							{	/* Ast/let.scm 1086 */
								obj_t BgL_arg3249z00_7932;
								obj_t BgL_arg3250z00_7933;

								BgL_arg3249z00_7932 = CDR(((obj_t) BgL_ebindingsz00_7923));
								{	/* Ast/let.scm 1088 */
									obj_t BgL_arg3251z00_7934;

									BgL_arg3251z00_7934 = CAR(((obj_t) BgL_ebindingsz00_7923));
									BgL_arg3250z00_7933 =
										MAKE_YOUNG_PAIR(BgL_arg3251z00_7934,
										BgL_recza2zd2bindingsz70_7925);
								}
								{
									obj_t BgL_recza2zd2bindingsz70_11468;
									obj_t BgL_ebindingsz00_11467;

									BgL_ebindingsz00_11467 = BgL_arg3249z00_7932;
									BgL_recza2zd2bindingsz70_11468 = BgL_arg3250z00_7933;
									BgL_recza2zd2bindingsz70_7925 =
										BgL_recza2zd2bindingsz70_11468;
									BgL_ebindingsz00_7923 = BgL_ebindingsz00_11467;
									goto BgL_loopz00_7922;
								}
							}
						else
							{	/* Ast/let.scm 1089 */
								bool_t BgL_test4305z00_11469;

								{	/* Ast/let.scm 1089 */
									bool_t BgL_test4306z00_11470;

									{	/* Ast/let.scm 1089 */
										obj_t BgL_arg3316z00_7935;

										{	/* Ast/let.scm 572 */
											obj_t BgL_pairz00_7936;

											BgL_pairz00_7936 =
												CAR(((obj_t) CAR(((obj_t) BgL_ebindingsz00_7923))));
											BgL_arg3316z00_7935 = CAR(CDR(BgL_pairz00_7936));
										}
										{	/* Ast/let.scm 1089 */

											BgL_test4306z00_11470 =
												CBOOL(BGl_functionzf3zf3zzast_letz00
												(BgL_arg3316z00_7935, BFALSE));
										}
									}
									if (BgL_test4306z00_11470)
										{	/* Ast/let.scm 1089 */
											BgL_test4305z00_11469 = ((bool_t) 0);
										}
									else
										{	/* Ast/let.scm 1090 */
											bool_t BgL_test4307z00_11479;

											{	/* Ast/let.scm 1090 */
												obj_t BgL_g1546z00_7937;

												{	/* Ast/let.scm 574 */
													obj_t BgL_pairz00_7938;

													BgL_pairz00_7938 =
														CAR(((obj_t) BgL_ebindingsz00_7923));
													BgL_g1546z00_7937 = CAR(CDR(CDR(BgL_pairz00_7938)));
												}
												{
													obj_t BgL_list1545z00_7940;

													{	/* Ast/let.scm 1094 */
														obj_t BgL_tmpz00_11485;

														BgL_list1545z00_7940 = BgL_g1546z00_7937;
													BgL_zc3z04anonymousza33310ze3z87_7939:
														if (PAIRP(BgL_list1545z00_7940))
															{	/* Ast/let.scm 1094 */
																bool_t BgL_test4309z00_11488;

																{	/* Ast/let.scm 1091 */
																	obj_t BgL_varz00_7941;

																	BgL_varz00_7941 = CAR(BgL_list1545z00_7940);
																	{	/* Ast/let.scm 1091 */
																		bool_t BgL_test4310z00_11490;

																		{	/* Ast/let.scm 1091 */
																			obj_t BgL_g1543z00_7942;

																			BgL_g1543z00_7942 =
																				CDR(((obj_t) BgL_ebindingsz00_7923));
																			{
																				obj_t BgL_list1542z00_7944;

																				{	/* Ast/let.scm 1093 */
																					obj_t BgL_tmpz00_11493;

																					BgL_list1542z00_7944 =
																						BgL_g1543z00_7942;
																				BgL_zc3z04anonymousza33312ze3z87_7943:
																					if (PAIRP(BgL_list1542z00_7944))
																						{	/* Ast/let.scm 1093 */
																							bool_t BgL_test4312z00_11496;

																							{	/* Ast/let.scm 1092 */
																								obj_t BgL_tmpz00_11497;

																								{	/* Ast/let.scm 573 */
																									obj_t BgL_pairz00_7945;

																									BgL_pairz00_7945 =
																										CDR(
																										((obj_t)
																											CAR
																											(BgL_list1542z00_7944)));
																									BgL_tmpz00_11497 =
																										CAR(BgL_pairz00_7945);
																								}
																								BgL_test4312z00_11496 =
																									(BgL_tmpz00_11497 ==
																									BgL_varz00_7941);
																							}
																							if (BgL_test4312z00_11496)
																								{	/* Ast/let.scm 1093 */
																									BgL_tmpz00_11493 =
																										CAR(BgL_list1542z00_7944);
																								}
																							else
																								{
																									obj_t BgL_list1542z00_11504;

																									BgL_list1542z00_11504 =
																										CDR(BgL_list1542z00_7944);
																									BgL_list1542z00_7944 =
																										BgL_list1542z00_11504;
																									goto
																										BgL_zc3z04anonymousza33312ze3z87_7943;
																								}
																						}
																					else
																						{	/* Ast/let.scm 1093 */
																							BgL_tmpz00_11493 = BFALSE;
																						}
																					BgL_test4310z00_11490 =
																						CBOOL(BgL_tmpz00_11493);
																				}
																			}
																		}
																		if (BgL_test4310z00_11490)
																			{	/* Ast/let.scm 1091 */
																				BgL_test4309z00_11488 = ((bool_t) 0);
																			}
																		else
																			{	/* Ast/let.scm 1091 */
																				BgL_test4309z00_11488 = ((bool_t) 1);
																			}
																	}
																}
																if (BgL_test4309z00_11488)
																	{	/* Ast/let.scm 1094 */
																		BgL_tmpz00_11485 =
																			CAR(BgL_list1545z00_7940);
																	}
																else
																	{
																		obj_t BgL_list1545z00_11508;

																		BgL_list1545z00_11508 =
																			CDR(BgL_list1545z00_7940);
																		BgL_list1545z00_7940 =
																			BgL_list1545z00_11508;
																		goto BgL_zc3z04anonymousza33310ze3z87_7939;
																	}
															}
														else
															{	/* Ast/let.scm 1094 */
																BgL_tmpz00_11485 = BFALSE;
															}
														BgL_test4307z00_11479 = CBOOL(BgL_tmpz00_11485);
													}
												}
											}
											if (BgL_test4307z00_11479)
												{	/* Ast/let.scm 1090 */
													BgL_test4305z00_11469 = ((bool_t) 0);
												}
											else
												{	/* Ast/let.scm 1095 */
													obj_t BgL_arg3309z00_7946;

													BgL_arg3309z00_7946 =
														CAR(((obj_t) BgL_ebindingsz00_7923));
													if (CBOOL(BGl_z62mutablezd2inzf3z43zzast_letz00
															(BgL_arg3309z00_7946,
																BgL_recza2zd2bindingsz70_7925)))
														{	/* Ast/let.scm 585 */
															BgL_test4305z00_11469 = ((bool_t) 0);
														}
													else
														{	/* Ast/let.scm 585 */
															BgL_test4305z00_11469 = ((bool_t) 1);
														}
												}
										}
								}
								if (BgL_test4305z00_11469)
									{	/* Ast/let.scm 1098 */
										obj_t BgL_arg3305z00_7947;
										obj_t BgL_arg3306z00_7948;

										BgL_arg3305z00_7947 = CDR(((obj_t) BgL_ebindingsz00_7923));
										{	/* Ast/let.scm 1099 */
											obj_t BgL_arg3307z00_7949;

											BgL_arg3307z00_7949 =
												CAR(((obj_t) BgL_ebindingsz00_7923));
											BgL_arg3306z00_7948 =
												MAKE_YOUNG_PAIR(BgL_arg3307z00_7949,
												BgL_valzd2bindingszd2_7924);
										}
										{
											obj_t BgL_valzd2bindingszd2_11522;
											obj_t BgL_ebindingsz00_11521;

											BgL_ebindingsz00_11521 = BgL_arg3305z00_7947;
											BgL_valzd2bindingszd2_11522 = BgL_arg3306z00_7948;
											BgL_valzd2bindingszd2_7924 = BgL_valzd2bindingszd2_11522;
											BgL_ebindingsz00_7923 = BgL_ebindingsz00_11521;
											goto BgL_loopz00_7922;
										}
									}
								else
									{	/* Ast/let.scm 1103 */
										obj_t BgL_val0_1547z00_7950;
										obj_t BgL_val1_1548z00_7951;

										BgL_val0_1547z00_7950 =
											bgl_reverse_bang(BgL_valzd2bindingszd2_7924);
										BgL_val1_1548z00_7951 =
											BGl_appendzd221011zd2zzast_letz00(bgl_reverse_bang
											(BgL_recza2zd2bindingsz70_7925), BgL_ebindingsz00_7923);
										{	/* Ast/let.scm 1103 */
											int BgL_tmpz00_11526;

											BgL_tmpz00_11526 = (int) (2L);
											BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11526);
										}
										{	/* Ast/let.scm 1103 */
											int BgL_tmpz00_11529;

											BgL_tmpz00_11529 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_11529,
												BgL_val1_1548z00_7951);
										}
										return BgL_val0_1547z00_7950;
									}
							}
					}
			}
		}

	}



/* &stage4 */
	BgL_nodez00_bglt BGl_z62stage4z62zzast_letz00(obj_t BgL_envz00_7808,
		obj_t BgL_ebindingsz00_7814, obj_t BgL_bodyz00_7815)
	{
		{	/* Ast/let.scm 1036 */
			{	/* Ast/let.scm 1022 */
				obj_t BgL_stage7z00_7809;
				obj_t BgL_stackz00_7810;
				obj_t BgL_locz00_7811;
				obj_t BgL_sitez00_7812;
				obj_t BgL_stage5z00_7813;

				BgL_stage7z00_7809 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_7808, (int) (0L)));
				BgL_stackz00_7810 = PROCEDURE_L_REF(BgL_envz00_7808, (int) (1L));
				BgL_locz00_7811 = PROCEDURE_L_REF(BgL_envz00_7808, (int) (2L));
				BgL_sitez00_7812 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_7808, (int) (3L)));
				BgL_stage5z00_7813 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_7808, (int) (4L)));
				{
					obj_t BgL_ebindingsz00_7953;
					obj_t BgL_bodyz00_7954;
					obj_t BgL_splitz00_7955;
					obj_t BgL_kontz00_7956;

					{	/* Ast/let.scm 1022 */
						obj_t BgL_splitz00_7977;

						{	/* Ast/let.scm 1022 */
							int BgL_tmpz00_11545;

							BgL_tmpz00_11545 = (int) (0L);
							BgL_splitz00_7977 = MAKE_EL_PROCEDURE(BgL_tmpz00_11545);
						}
						BgL_ebindingsz00_7953 = BgL_ebindingsz00_7814;
						BgL_bodyz00_7954 = BgL_bodyz00_7815;
						BgL_splitz00_7955 = BgL_splitz00_7977;
						BgL_kontz00_7956 = BgL_stage5z00_7813;
						if (NULLP(BgL_ebindingsz00_7953))
							{	/* Ast/let.scm 692 */
								return
									BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_bodyz00_7954,
									BgL_stackz00_7810, BgL_locz00_7811, BgL_sitez00_7812);
							}
						else
							{	/* Ast/let.scm 695 */
								obj_t BgL_letzd2bindingszd2_7957;

								BgL_letzd2bindingszd2_7957 =
									BGl_z62split3804z62zzast_letz00(BgL_splitz00_7955,
									BgL_ebindingsz00_7953);
								{	/* Ast/let.scm 696 */
									obj_t BgL_recza2zd2bindingsz70_7958;

									{	/* Ast/let.scm 702 */
										obj_t BgL_tmpz00_7959;

										{	/* Ast/let.scm 702 */
											int BgL_tmpz00_11555;

											BgL_tmpz00_11555 = (int) (1L);
											BgL_tmpz00_7959 = BGL_MVALUES_VAL(BgL_tmpz00_11555);
										}
										{	/* Ast/let.scm 702 */
											int BgL_tmpz00_11558;

											BgL_tmpz00_11558 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_11558, BUNSPEC);
										}
										BgL_recza2zd2bindingsz70_7958 = BgL_tmpz00_7959;
									}
									{	/* Ast/let.scm 702 */
										bool_t BgL_test4315z00_11561;

										if (NULLP(BgL_letzd2bindingszd2_7957))
											{	/* Ast/let.scm 702 */
												BgL_test4315z00_11561 = ((bool_t) 0);
											}
										else
											{	/* Ast/let.scm 702 */
												BgL_test4315z00_11561 =
													PAIRP(BgL_recza2zd2bindingsz70_7958);
											}
										if (BgL_test4315z00_11561)
											{	/* Ast/let.scm 706 */
												obj_t BgL_arg2338z00_7960;

												{	/* Ast/let.scm 706 */
													obj_t BgL_arg2339z00_7961;

													{	/* Ast/let.scm 706 */
														obj_t BgL_arg2340z00_7962;

														{	/* Ast/let.scm 706 */
															obj_t BgL_arg2341z00_7963;

															{	/* Ast/let.scm 706 */
																obj_t BgL_arg2342z00_7964;
																obj_t BgL_arg2345z00_7965;

																{	/* Ast/let.scm 706 */
																	obj_t BgL_head1443z00_7966;

																	{	/* Ast/let.scm 706 */
																		obj_t BgL_arg2352z00_7967;

																		{	/* Ast/let.scm 571 */
																			obj_t BgL_pairz00_7968;

																			BgL_pairz00_7968 =
																				CAR(
																				((obj_t)
																					BgL_recza2zd2bindingsz70_7958));
																			BgL_arg2352z00_7967 =
																				CAR(BgL_pairz00_7968);
																		}
																		BgL_head1443z00_7966 =
																			MAKE_YOUNG_PAIR(BgL_arg2352z00_7967,
																			BNIL);
																	}
																	{	/* Ast/let.scm 706 */
																		obj_t BgL_g1446z00_7969;

																		BgL_g1446z00_7969 =
																			CDR(
																			((obj_t) BgL_recza2zd2bindingsz70_7958));
																		{
																			obj_t BgL_l1441z00_7971;
																			obj_t BgL_tail1444z00_7972;

																			BgL_l1441z00_7971 = BgL_g1446z00_7969;
																			BgL_tail1444z00_7972 =
																				BgL_head1443z00_7966;
																		BgL_zc3z04anonymousza32347ze3z87_7970:
																			if (NULLP(BgL_l1441z00_7971))
																				{	/* Ast/let.scm 706 */
																					BgL_arg2342z00_7964 =
																						BgL_head1443z00_7966;
																				}
																			else
																				{	/* Ast/let.scm 706 */
																					obj_t BgL_newtail1445z00_7973;

																					{	/* Ast/let.scm 706 */
																						obj_t BgL_arg2350z00_7974;

																						{	/* Ast/let.scm 571 */
																							obj_t BgL_pairz00_7975;

																							BgL_pairz00_7975 =
																								CAR(
																								((obj_t) BgL_l1441z00_7971));
																							BgL_arg2350z00_7974 =
																								CAR(BgL_pairz00_7975);
																						}
																						BgL_newtail1445z00_7973 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2350z00_7974, BNIL);
																					}
																					SET_CDR(BgL_tail1444z00_7972,
																						BgL_newtail1445z00_7973);
																					{	/* Ast/let.scm 706 */
																						obj_t BgL_arg2349z00_7976;

																						BgL_arg2349z00_7976 =
																							CDR(((obj_t) BgL_l1441z00_7971));
																						{
																							obj_t BgL_tail1444z00_11581;
																							obj_t BgL_l1441z00_11580;

																							BgL_l1441z00_11580 =
																								BgL_arg2349z00_7976;
																							BgL_tail1444z00_11581 =
																								BgL_newtail1445z00_7973;
																							BgL_tail1444z00_7972 =
																								BgL_tail1444z00_11581;
																							BgL_l1441z00_7971 =
																								BgL_l1441z00_11580;
																							goto
																								BgL_zc3z04anonymousza32347ze3z87_7970;
																						}
																					}
																				}
																		}
																	}
																}
																BgL_arg2345z00_7965 =
																	MAKE_YOUNG_PAIR(BgL_bodyz00_7954, BNIL);
																BgL_arg2341z00_7963 =
																	MAKE_YOUNG_PAIR(BgL_arg2342z00_7964,
																	BgL_arg2345z00_7965);
															}
															BgL_arg2340z00_7962 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(4),
																BgL_arg2341z00_7963);
														}
														BgL_arg2339z00_7961 =
															BGl_z62letstarz62zzast_letz00
															(BgL_letzd2bindingszd2_7957, BgL_arg2340z00_7962);
													}
													BgL_arg2338z00_7960 =
														BGl_z62letcollapsez62zzast_letz00
														(BgL_letzd2bindingszd2_7957, BgL_arg2339z00_7961);
												}
												return
													BGl_sexpzd2ze3nodez31zzast_sexpz00
													(BgL_arg2338z00_7960, BgL_stackz00_7810,
													BgL_locz00_7811, BgL_sitez00_7812);
											}
										else
											{	/* Ast/let.scm 702 */
												if (NULLP(BgL_letzd2bindingszd2_7957))
													{	/* Ast/let.scm 709 */
														return
															BGl_z62stage5z62zzast_letz00(BgL_kontz00_7956,
															BgL_ebindingsz00_7953, BgL_bodyz00_7954);
													}
												else
													{	/* Ast/let.scm 709 */
														return
															BGl_sexpzd2ze3nodez31zzast_sexpz00
															(BGl_z62letcollapsez62zzast_letz00
															(BgL_letzd2bindingszd2_7957,
																BGl_z62letstarz62zzast_letz00
																(BgL_letzd2bindingszd2_7957, BgL_bodyz00_7954)),
															BgL_stackz00_7810, BgL_locz00_7811,
															BgL_sitez00_7812);
													}
											}
									}
								}
							}
					}
				}
			}
		}

	}



/* &split3803 */
	obj_t BGl_z62split3803z62zzast_letz00(obj_t BgL_envz00_7816,
		obj_t BgL_ebindingsz00_7817)
	{
		{	/* Ast/let.scm 1068 */
			{
				obj_t BgL_ebindingsz00_7979;
				obj_t BgL_funzd2bindingszd2_7980;
				obj_t BgL_recza2zd2bindingsz70_7981;

				BgL_ebindingsz00_7979 = BgL_ebindingsz00_7817;
				BgL_funzd2bindingszd2_7980 = BNIL;
				BgL_recza2zd2bindingsz70_7981 = BNIL;
			BgL_loopz00_7978:
				if (NULLP(BgL_ebindingsz00_7979))
					{	/* Ast/let.scm 1051 */
						obj_t BgL_val0_1530z00_7982;
						obj_t BgL_val1_1531z00_7983;

						BgL_val0_1530z00_7982 =
							bgl_reverse_bang(BgL_funzd2bindingszd2_7980);
						BgL_val1_1531z00_7983 =
							bgl_reverse_bang(BgL_recza2zd2bindingsz70_7981);
						{	/* Ast/let.scm 1051 */
							int BgL_tmpz00_11603;

							BgL_tmpz00_11603 = (int) (2L);
							BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11603);
						}
						{	/* Ast/let.scm 1051 */
							int BgL_tmpz00_11606;

							BgL_tmpz00_11606 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_11606, BgL_val1_1531z00_7983);
						}
						return BgL_val0_1530z00_7982;
					}
				else
					{	/* Ast/let.scm 1052 */
						bool_t BgL_test4320z00_11609;

						{	/* Ast/let.scm 1052 */
							bool_t BgL_test4321z00_11610;

							{	/* Ast/let.scm 1052 */
								obj_t BgL_arg3231z00_7984;

								{	/* Ast/let.scm 572 */
									obj_t BgL_pairz00_7985;

									BgL_pairz00_7985 =
										CAR(((obj_t) CAR(((obj_t) BgL_ebindingsz00_7979))));
									BgL_arg3231z00_7984 = CAR(CDR(BgL_pairz00_7985));
								}
								{	/* Ast/let.scm 1052 */

									BgL_test4321z00_11610 =
										CBOOL(BGl_functionzf3zf3zzast_letz00(BgL_arg3231z00_7984,
											BFALSE));
								}
							}
							if (BgL_test4321z00_11610)
								{	/* Ast/let.scm 1053 */
									bool_t BgL_test4322z00_11619;

									{
										obj_t BgL_list1536z00_7987;

										{	/* Ast/let.scm 1053 */
											obj_t BgL_tmpz00_11620;

											BgL_list1536z00_7987 = BgL_ebindingsz00_7979;
										BgL_zc3z04anonymousza33222ze3z87_7986:
											if (PAIRP(BgL_list1536z00_7987))
												{	/* Ast/let.scm 1053 */
													bool_t BgL_test4324z00_11623;

													{	/* Ast/let.scm 1054 */
														obj_t BgL_tmpz00_11624;

														{	/* Ast/let.scm 1054 */
															obj_t BgL_auxz00_11630;
															obj_t BgL_auxz00_11625;

															{	/* Ast/let.scm 575 */
																obj_t BgL_pairz00_7989;

																{	/* Ast/let.scm 575 */
																	obj_t BgL_pairz00_7990;

																	{	/* Ast/let.scm 575 */
																		obj_t BgL_pairz00_7991;

																		BgL_pairz00_7991 =
																			CDR(((obj_t) CAR(BgL_list1536z00_7987)));
																		BgL_pairz00_7990 = CDR(BgL_pairz00_7991);
																	}
																	BgL_pairz00_7989 = CDR(BgL_pairz00_7990);
																}
																BgL_auxz00_11630 = CAR(BgL_pairz00_7989);
															}
															{	/* Ast/let.scm 573 */
																obj_t BgL_pairz00_7988;

																BgL_pairz00_7988 =
																	CAR(((obj_t) BgL_ebindingsz00_7979));
																BgL_auxz00_11625 = CAR(CDR(BgL_pairz00_7988));
															}
															BgL_tmpz00_11624 =
																BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																(BgL_auxz00_11625, BgL_auxz00_11630);
														}
														BgL_test4324z00_11623 = CBOOL(BgL_tmpz00_11624);
													}
													if (BgL_test4324z00_11623)
														{	/* Ast/let.scm 1053 */
															BgL_tmpz00_11620 = CAR(BgL_list1536z00_7987);
														}
													else
														{
															obj_t BgL_list1536z00_11640;

															BgL_list1536z00_11640 = CDR(BgL_list1536z00_7987);
															BgL_list1536z00_7987 = BgL_list1536z00_11640;
															goto BgL_zc3z04anonymousza33222ze3z87_7986;
														}
												}
											else
												{	/* Ast/let.scm 1053 */
													BgL_tmpz00_11620 = BFALSE;
												}
											BgL_test4322z00_11619 = CBOOL(BgL_tmpz00_11620);
										}
									}
									if (BgL_test4322z00_11619)
										{	/* Ast/let.scm 1053 */
											BgL_test4320z00_11609 = ((bool_t) 0);
										}
									else
										{	/* Ast/let.scm 1053 */
											BgL_test4320z00_11609 = ((bool_t) 1);
										}
								}
							else
								{	/* Ast/let.scm 1052 */
									BgL_test4320z00_11609 = ((bool_t) 0);
								}
						}
						if (BgL_test4320z00_11609)
							{	/* Ast/let.scm 1058 */
								bool_t BgL_test4325z00_11643;

								{	/* Ast/let.scm 1058 */
									obj_t BgL_tmpz00_11644;

									{	/* Ast/let.scm 574 */
										obj_t BgL_pairz00_7992;

										BgL_pairz00_7992 = CAR(((obj_t) BgL_ebindingsz00_7979));
										BgL_tmpz00_11644 = CAR(CDR(CDR(BgL_pairz00_7992)));
									}
									BgL_test4325z00_11643 = NULLP(BgL_tmpz00_11644);
								}
								if (BgL_test4325z00_11643)
									{	/* Ast/let.scm 1059 */
										obj_t BgL_arg3207z00_7993;
										obj_t BgL_arg3208z00_7994;

										BgL_arg3207z00_7993 = CDR(((obj_t) BgL_ebindingsz00_7979));
										{	/* Ast/let.scm 1060 */
											obj_t BgL_arg3210z00_7995;

											BgL_arg3210z00_7995 =
												CAR(((obj_t) BgL_ebindingsz00_7979));
											BgL_arg3208z00_7994 =
												MAKE_YOUNG_PAIR(BgL_arg3210z00_7995,
												BgL_funzd2bindingszd2_7980);
										}
										{
											obj_t BgL_funzd2bindingszd2_11657;
											obj_t BgL_ebindingsz00_11656;

											BgL_ebindingsz00_11656 = BgL_arg3207z00_7993;
											BgL_funzd2bindingszd2_11657 = BgL_arg3208z00_7994;
											BgL_funzd2bindingszd2_7980 = BgL_funzd2bindingszd2_11657;
											BgL_ebindingsz00_7979 = BgL_ebindingsz00_11656;
											goto BgL_loopz00_7978;
										}
									}
								else
									{	/* Ast/let.scm 1062 */
										obj_t BgL_arg3211z00_7996;
										obj_t BgL_arg3212z00_7997;

										BgL_arg3211z00_7996 = CDR(((obj_t) BgL_ebindingsz00_7979));
										{	/* Ast/let.scm 1064 */
											obj_t BgL_arg3217z00_7998;

											BgL_arg3217z00_7998 =
												CAR(((obj_t) BgL_ebindingsz00_7979));
											BgL_arg3212z00_7997 =
												MAKE_YOUNG_PAIR(BgL_arg3217z00_7998,
												BgL_recza2zd2bindingsz70_7981);
										}
										{
											obj_t BgL_recza2zd2bindingsz70_11664;
											obj_t BgL_ebindingsz00_11663;

											BgL_ebindingsz00_11663 = BgL_arg3211z00_7996;
											BgL_recza2zd2bindingsz70_11664 = BgL_arg3212z00_7997;
											BgL_recza2zd2bindingsz70_7981 =
												BgL_recza2zd2bindingsz70_11664;
											BgL_ebindingsz00_7979 = BgL_ebindingsz00_11663;
											goto BgL_loopz00_7978;
										}
									}
							}
						else
							{	/* Ast/let.scm 1067 */
								obj_t BgL_val0_1537z00_7999;
								obj_t BgL_val1_1538z00_8000;

								BgL_val0_1537z00_7999 =
									bgl_reverse_bang(BgL_funzd2bindingszd2_7980);
								BgL_val1_1538z00_8000 =
									BGl_appendzd221011zd2zzast_letz00(bgl_reverse_bang
									(BgL_recza2zd2bindingsz70_7981), BgL_ebindingsz00_7979);
								{	/* Ast/let.scm 1067 */
									int BgL_tmpz00_11668;

									BgL_tmpz00_11668 = (int) (2L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11668);
								}
								{	/* Ast/let.scm 1067 */
									int BgL_tmpz00_11671;

									BgL_tmpz00_11671 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_11671, BgL_val1_1538z00_8000);
								}
								return BgL_val0_1537z00_7999;
							}
					}
			}
		}

	}



/* &stage5 */
	BgL_nodez00_bglt BGl_z62stage5z62zzast_letz00(obj_t BgL_envz00_7818,
		obj_t BgL_ebindingsz00_7823, obj_t BgL_bodyz00_7824)
	{
		{	/* Ast/let.scm 993 */
			{	/* Ast/let.scm 975 */
				obj_t BgL_stage7z00_7819;
				obj_t BgL_stackz00_7820;
				obj_t BgL_locz00_7821;
				obj_t BgL_sitez00_7822;

				BgL_stage7z00_7819 =
					((obj_t) PROCEDURE_EL_REF(BgL_envz00_7818, (int) (0L)));
				BgL_stackz00_7820 = PROCEDURE_EL_REF(BgL_envz00_7818, (int) (1L));
				BgL_locz00_7821 = PROCEDURE_EL_REF(BgL_envz00_7818, (int) (2L));
				BgL_sitez00_7822 =
					((obj_t) PROCEDURE_EL_REF(BgL_envz00_7818, (int) (3L)));
				{
					obj_t BgL_ebindingsz00_8006;
					obj_t BgL_bodyz00_8007;
					obj_t BgL_splitz00_8008;
					obj_t BgL_kontz00_8009;
					obj_t BgL_ebindingsz00_8003;
					obj_t BgL_bodyz00_8004;

					{
						obj_t BgL_ebindingsz00_8042;

						if (NULLP(BgL_ebindingsz00_7823))
							{	/* Ast/let.scm 997 */
								return
									BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_bodyz00_7824,
									BgL_stackz00_7820, BgL_locz00_7821, BgL_sitez00_7822);
							}
						else
							{	/* Ast/let.scm 1000 */
								obj_t BgL_vbindingsz00_8088;

								BgL_ebindingsz00_8042 = BgL_ebindingsz00_7823;
								{
									obj_t BgL_lz00_8044;
									obj_t BgL_vbindingsz00_8045;
									obj_t BgL_fbindingsz00_8046;

									BgL_lz00_8044 = BgL_ebindingsz00_8042;
									BgL_vbindingsz00_8045 = BNIL;
									BgL_fbindingsz00_8046 = BNIL;
								BgL_loopz00_8043:
									if (NULLP(BgL_lz00_8044))
										{	/* Ast/let.scm 981 */
											obj_t BgL_funsz00_8047;
											obj_t BgL_varsz00_8048;

											if (NULLP(BgL_fbindingsz00_8046))
												{	/* Ast/let.scm 981 */
													BgL_funsz00_8047 = BNIL;
												}
											else
												{	/* Ast/let.scm 981 */
													obj_t BgL_head1489z00_8049;

													{	/* Ast/let.scm 981 */
														obj_t BgL_arg3094z00_8050;

														{	/* Ast/let.scm 981 */
															obj_t BgL_pairz00_8051;

															BgL_pairz00_8051 =
																CAR(((obj_t) BgL_fbindingsz00_8046));
															BgL_arg3094z00_8050 = CAR(CDR(BgL_pairz00_8051));
														}
														BgL_head1489z00_8049 =
															MAKE_YOUNG_PAIR(BgL_arg3094z00_8050, BNIL);
													}
													{	/* Ast/let.scm 981 */
														obj_t BgL_g1492z00_8052;

														BgL_g1492z00_8052 =
															CDR(((obj_t) BgL_fbindingsz00_8046));
														{
															obj_t BgL_l1487z00_8054;
															obj_t BgL_tail1490z00_8055;

															BgL_l1487z00_8054 = BgL_g1492z00_8052;
															BgL_tail1490z00_8055 = BgL_head1489z00_8049;
														BgL_zc3z04anonymousza33087ze3z87_8053:
															if (NULLP(BgL_l1487z00_8054))
																{	/* Ast/let.scm 981 */
																	BgL_funsz00_8047 = BgL_head1489z00_8049;
																}
															else
																{	/* Ast/let.scm 981 */
																	obj_t BgL_newtail1491z00_8056;

																	{	/* Ast/let.scm 981 */
																		obj_t BgL_arg3092z00_8057;

																		{	/* Ast/let.scm 981 */
																			obj_t BgL_pairz00_8058;

																			BgL_pairz00_8058 =
																				CAR(((obj_t) BgL_l1487z00_8054));
																			BgL_arg3092z00_8057 =
																				CAR(CDR(BgL_pairz00_8058));
																		}
																		BgL_newtail1491z00_8056 =
																			MAKE_YOUNG_PAIR(BgL_arg3092z00_8057,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1490z00_8055,
																		BgL_newtail1491z00_8056);
																	{	/* Ast/let.scm 981 */
																		obj_t BgL_arg3091z00_8059;

																		BgL_arg3091z00_8059 =
																			CDR(((obj_t) BgL_l1487z00_8054));
																		{
																			obj_t BgL_tail1490z00_11709;
																			obj_t BgL_l1487z00_11708;

																			BgL_l1487z00_11708 = BgL_arg3091z00_8059;
																			BgL_tail1490z00_11709 =
																				BgL_newtail1491z00_8056;
																			BgL_tail1490z00_8055 =
																				BgL_tail1490z00_11709;
																			BgL_l1487z00_8054 = BgL_l1487z00_11708;
																			goto
																				BgL_zc3z04anonymousza33087ze3z87_8053;
																		}
																	}
																}
														}
													}
												}
											if (NULLP(BgL_vbindingsz00_8045))
												{	/* Ast/let.scm 982 */
													BgL_varsz00_8048 = BNIL;
												}
											else
												{	/* Ast/let.scm 982 */
													obj_t BgL_head1495z00_8060;

													{	/* Ast/let.scm 982 */
														obj_t BgL_arg3115z00_8061;

														{	/* Ast/let.scm 982 */
															obj_t BgL_pairz00_8062;

															BgL_pairz00_8062 =
																CAR(((obj_t) BgL_vbindingsz00_8045));
															BgL_arg3115z00_8061 = CAR(CDR(BgL_pairz00_8062));
														}
														BgL_head1495z00_8060 =
															MAKE_YOUNG_PAIR(BgL_arg3115z00_8061, BNIL);
													}
													{	/* Ast/let.scm 982 */
														obj_t BgL_g1498z00_8063;

														BgL_g1498z00_8063 =
															CDR(((obj_t) BgL_vbindingsz00_8045));
														{
															obj_t BgL_l1493z00_8065;
															obj_t BgL_tail1496z00_8066;

															BgL_l1493z00_8065 = BgL_g1498z00_8063;
															BgL_tail1496z00_8066 = BgL_head1495z00_8060;
														BgL_zc3z04anonymousza33101ze3z87_8064:
															if (NULLP(BgL_l1493z00_8065))
																{	/* Ast/let.scm 982 */
																	BgL_varsz00_8048 = BgL_head1495z00_8060;
																}
															else
																{	/* Ast/let.scm 982 */
																	obj_t BgL_newtail1497z00_8067;

																	{	/* Ast/let.scm 982 */
																		obj_t BgL_arg3106z00_8068;

																		{	/* Ast/let.scm 982 */
																			obj_t BgL_pairz00_8069;

																			BgL_pairz00_8069 =
																				CAR(((obj_t) BgL_l1493z00_8065));
																			BgL_arg3106z00_8068 =
																				CAR(CDR(BgL_pairz00_8069));
																		}
																		BgL_newtail1497z00_8067 =
																			MAKE_YOUNG_PAIR(BgL_arg3106z00_8068,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1496z00_8066,
																		BgL_newtail1497z00_8067);
																	{	/* Ast/let.scm 982 */
																		obj_t BgL_arg3105z00_8070;

																		BgL_arg3105z00_8070 =
																			CDR(((obj_t) BgL_l1493z00_8065));
																		{
																			obj_t BgL_tail1496z00_11730;
																			obj_t BgL_l1493z00_11729;

																			BgL_l1493z00_11729 = BgL_arg3105z00_8070;
																			BgL_tail1496z00_11730 =
																				BgL_newtail1497z00_8067;
																			BgL_tail1496z00_8066 =
																				BgL_tail1496z00_11730;
																			BgL_l1493z00_8065 = BgL_l1493z00_11729;
																			goto
																				BgL_zc3z04anonymousza33101ze3z87_8064;
																		}
																	}
																}
														}
													}
												}
											{	/* Ast/let.scm 983 */
												obj_t BgL_freesz00_8071;
												obj_t BgL_setsz00_8072;

												BgL_freesz00_8071 =
													BGl_zc3z04anonymousza33072ze3ze70z60zzast_letz00
													(BgL_vbindingsz00_8045);
												BgL_setsz00_8072 =
													BGl_zc3z04anonymousza33080ze3ze70z60zzast_letz00
													(BgL_fbindingsz00_8046);
												{	/* Ast/let.scm 985 */
													bool_t BgL_test4332z00_11733;

													{
														obj_t BgL_l1503z00_8074;

														{	/* Ast/let.scm 985 */
															obj_t BgL_tmpz00_11734;

															BgL_l1503z00_8074 = BgL_fbindingsz00_8046;
														BgL_zc3z04anonymousza33069ze3z87_8073:
															if (NULLP(BgL_l1503z00_8074))
																{	/* Ast/let.scm 985 */
																	BgL_tmpz00_11734 = BFALSE;
																}
															else
																{	/* Ast/let.scm 985 */
																	obj_t BgL__ortest_1505z00_8075;

																	{	/* Ast/let.scm 985 */
																		obj_t BgL_auxz00_11737;

																		{	/* Ast/let.scm 573 */
																			obj_t BgL_pairz00_8076;

																			BgL_pairz00_8076 =
																				CDR(
																				((obj_t)
																					CAR(((obj_t) BgL_l1503z00_8074))));
																			BgL_auxz00_11737 = CAR(BgL_pairz00_8076);
																		}
																		BgL__ortest_1505z00_8075 =
																			BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																			(BgL_auxz00_11737, BgL_freesz00_8071);
																	}
																	if (CBOOL(BgL__ortest_1505z00_8075))
																		{	/* Ast/let.scm 985 */
																			BgL_tmpz00_11734 =
																				BgL__ortest_1505z00_8075;
																		}
																	else
																		{	/* Ast/let.scm 985 */
																			obj_t BgL_arg3070z00_8077;

																			BgL_arg3070z00_8077 =
																				CDR(((obj_t) BgL_l1503z00_8074));
																			{
																				obj_t BgL_l1503z00_11748;

																				BgL_l1503z00_11748 =
																					BgL_arg3070z00_8077;
																				BgL_l1503z00_8074 = BgL_l1503z00_11748;
																				goto
																					BgL_zc3z04anonymousza33069ze3z87_8073;
																			}
																		}
																}
															BgL_test4332z00_11733 = CBOOL(BgL_tmpz00_11734);
														}
													}
													if (BgL_test4332z00_11733)
														{	/* Ast/let.scm 985 */
															{	/* Ast/let.scm 987 */
																int BgL_tmpz00_11750;

																BgL_tmpz00_11750 = (int) (2L);
																BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11750);
															}
															{	/* Ast/let.scm 987 */
																int BgL_tmpz00_11753;

																BgL_tmpz00_11753 = (int) (1L);
																BGL_MVALUES_VAL_SET(BgL_tmpz00_11753,
																	BgL_ebindingsz00_8042);
															}
															BgL_vbindingsz00_8088 = BNIL;
														}
													else
														{	/* Ast/let.scm 988 */
															obj_t BgL_val0_1508z00_8078;
															obj_t BgL_val1_1509z00_8079;

															BgL_val0_1508z00_8078 =
																bgl_reverse_bang(BgL_vbindingsz00_8045);
															BgL_val1_1509z00_8079 =
																bgl_reverse_bang(BgL_fbindingsz00_8046);
															{	/* Ast/let.scm 988 */
																int BgL_tmpz00_11758;

																BgL_tmpz00_11758 = (int) (2L);
																BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11758);
															}
															{	/* Ast/let.scm 988 */
																int BgL_tmpz00_11761;

																BgL_tmpz00_11761 = (int) (1L);
																BGL_MVALUES_VAL_SET(BgL_tmpz00_11761,
																	BgL_val1_1509z00_8079);
															}
															BgL_vbindingsz00_8088 = BgL_val0_1508z00_8078;
										}}}}
									else
										{	/* Ast/let.scm 990 */
											bool_t BgL_test4335z00_11764;

											{	/* Ast/let.scm 990 */
												obj_t BgL_arg3131z00_8080;

												{	/* Ast/let.scm 572 */
													obj_t BgL_pairz00_8081;

													BgL_pairz00_8081 =
														CAR(((obj_t) CAR(((obj_t) BgL_lz00_8044))));
													BgL_arg3131z00_8080 = CAR(CDR(BgL_pairz00_8081));
												}
												BgL_test4335z00_11764 =
													CBOOL(BGl_functionzf3zf3zzast_letz00
													(BgL_arg3131z00_8080, BTRUE));
											}
											if (BgL_test4335z00_11764)
												{	/* Ast/let.scm 991 */
													obj_t BgL_arg3124z00_8082;
													obj_t BgL_arg3125z00_8083;

													BgL_arg3124z00_8082 = CDR(((obj_t) BgL_lz00_8044));
													{	/* Ast/let.scm 991 */
														obj_t BgL_arg3126z00_8084;

														BgL_arg3126z00_8084 = CAR(((obj_t) BgL_lz00_8044));
														BgL_arg3125z00_8083 =
															MAKE_YOUNG_PAIR(BgL_arg3126z00_8084,
															BgL_fbindingsz00_8046);
													}
													{
														obj_t BgL_fbindingsz00_11779;
														obj_t BgL_lz00_11778;

														BgL_lz00_11778 = BgL_arg3124z00_8082;
														BgL_fbindingsz00_11779 = BgL_arg3125z00_8083;
														BgL_fbindingsz00_8046 = BgL_fbindingsz00_11779;
														BgL_lz00_8044 = BgL_lz00_11778;
														goto BgL_loopz00_8043;
													}
												}
											else
												{	/* Ast/let.scm 993 */
													obj_t BgL_arg3127z00_8085;
													obj_t BgL_arg3128z00_8086;

													BgL_arg3127z00_8085 = CDR(((obj_t) BgL_lz00_8044));
													{	/* Ast/let.scm 993 */
														obj_t BgL_arg3129z00_8087;

														BgL_arg3129z00_8087 = CAR(((obj_t) BgL_lz00_8044));
														BgL_arg3128z00_8086 =
															MAKE_YOUNG_PAIR(BgL_arg3129z00_8087,
															BgL_vbindingsz00_8045);
													}
													{
														obj_t BgL_vbindingsz00_11786;
														obj_t BgL_lz00_11785;

														BgL_lz00_11785 = BgL_arg3127z00_8085;
														BgL_vbindingsz00_11786 = BgL_arg3128z00_8086;
														BgL_vbindingsz00_8045 = BgL_vbindingsz00_11786;
														BgL_lz00_8044 = BgL_lz00_11785;
														goto BgL_loopz00_8043;
													}
												}
										}
								}
								{	/* Ast/let.scm 1002 */
									obj_t BgL_fbindingsz00_8089;

									{	/* Ast/let.scm 1007 */
										obj_t BgL_tmpz00_8090;

										{	/* Ast/let.scm 1007 */
											int BgL_tmpz00_11787;

											BgL_tmpz00_11787 = (int) (1L);
											BgL_tmpz00_8090 = BGL_MVALUES_VAL(BgL_tmpz00_11787);
										}
										{	/* Ast/let.scm 1007 */
											int BgL_tmpz00_11790;

											BgL_tmpz00_11790 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_11790, BUNSPEC);
										}
										BgL_fbindingsz00_8089 = BgL_tmpz00_8090;
									}
									{	/* Ast/let.scm 1007 */
										bool_t BgL_test4336z00_11793;

										if (NULLP(BgL_vbindingsz00_8088))
											{	/* Ast/let.scm 1007 */
												BgL_test4336z00_11793 = ((bool_t) 0);
											}
										else
											{	/* Ast/let.scm 1007 */
												BgL_test4336z00_11793 = PAIRP(BgL_fbindingsz00_8089);
											}
										if (BgL_test4336z00_11793)
											{	/* Ast/let.scm 1009 */
												obj_t BgL_arg3019z00_8091;

												{	/* Ast/let.scm 1009 */
													obj_t BgL_arg3020z00_8092;

													{	/* Ast/let.scm 1009 */
														obj_t BgL_arg3021z00_8093;
														obj_t BgL_arg3023z00_8094;

														{	/* Ast/let.scm 1009 */
															obj_t BgL_head1512z00_8095;

															{	/* Ast/let.scm 1009 */
																obj_t BgL_arg3031z00_8096;

																{	/* Ast/let.scm 571 */
																	obj_t BgL_pairz00_8097;

																	BgL_pairz00_8097 =
																		CAR(((obj_t) BgL_vbindingsz00_8088));
																	BgL_arg3031z00_8096 = CAR(BgL_pairz00_8097);
																}
																BgL_head1512z00_8095 =
																	MAKE_YOUNG_PAIR(BgL_arg3031z00_8096, BNIL);
															}
															{	/* Ast/let.scm 1009 */
																obj_t BgL_g1515z00_8098;

																BgL_g1515z00_8098 =
																	CDR(((obj_t) BgL_vbindingsz00_8088));
																{
																	obj_t BgL_l1510z00_8100;
																	obj_t BgL_tail1513z00_8101;

																	BgL_l1510z00_8100 = BgL_g1515z00_8098;
																	BgL_tail1513z00_8101 = BgL_head1512z00_8095;
																BgL_zc3z04anonymousza33025ze3z87_8099:
																	if (NULLP(BgL_l1510z00_8100))
																		{	/* Ast/let.scm 1009 */
																			BgL_arg3021z00_8093 =
																				BgL_head1512z00_8095;
																		}
																	else
																		{	/* Ast/let.scm 1009 */
																			obj_t BgL_newtail1514z00_8102;

																			{	/* Ast/let.scm 1009 */
																				obj_t BgL_arg3029z00_8103;

																				{	/* Ast/let.scm 571 */
																					obj_t BgL_pairz00_8104;

																					BgL_pairz00_8104 =
																						CAR(((obj_t) BgL_l1510z00_8100));
																					BgL_arg3029z00_8103 =
																						CAR(BgL_pairz00_8104);
																				}
																				BgL_newtail1514z00_8102 =
																					MAKE_YOUNG_PAIR(BgL_arg3029z00_8103,
																					BNIL);
																			}
																			SET_CDR(BgL_tail1513z00_8101,
																				BgL_newtail1514z00_8102);
																			{	/* Ast/let.scm 1009 */
																				obj_t BgL_arg3027z00_8105;

																				BgL_arg3027z00_8105 =
																					CDR(((obj_t) BgL_l1510z00_8100));
																				{
																					obj_t BgL_tail1513z00_11813;
																					obj_t BgL_l1510z00_11812;

																					BgL_l1510z00_11812 =
																						BgL_arg3027z00_8105;
																					BgL_tail1513z00_11813 =
																						BgL_newtail1514z00_8102;
																					BgL_tail1513z00_8101 =
																						BgL_tail1513z00_11813;
																					BgL_l1510z00_8100 =
																						BgL_l1510z00_11812;
																					goto
																						BgL_zc3z04anonymousza33025ze3z87_8099;
																				}
																			}
																		}
																}
															}
														}
														{	/* Ast/let.scm 1010 */
															obj_t BgL_arg3037z00_8106;

															{	/* Ast/let.scm 1010 */
																obj_t BgL_arg3038z00_8107;

																{	/* Ast/let.scm 1010 */
																	obj_t BgL_arg3043z00_8108;
																	obj_t BgL_arg3044z00_8109;

																	{	/* Ast/let.scm 1010 */
																		obj_t BgL_head1518z00_8110;

																		{	/* Ast/let.scm 1010 */
																			obj_t BgL_arg3055z00_8111;

																			{	/* Ast/let.scm 571 */
																				obj_t BgL_pairz00_8112;

																				BgL_pairz00_8112 =
																					CAR(((obj_t) BgL_fbindingsz00_8089));
																				BgL_arg3055z00_8111 =
																					CAR(BgL_pairz00_8112);
																			}
																			BgL_head1518z00_8110 =
																				MAKE_YOUNG_PAIR(BgL_arg3055z00_8111,
																				BNIL);
																		}
																		{	/* Ast/let.scm 1010 */
																			obj_t BgL_g1521z00_8113;

																			BgL_g1521z00_8113 =
																				CDR(((obj_t) BgL_fbindingsz00_8089));
																			{
																				obj_t BgL_l1516z00_8115;
																				obj_t BgL_tail1519z00_8116;

																				BgL_l1516z00_8115 = BgL_g1521z00_8113;
																				BgL_tail1519z00_8116 =
																					BgL_head1518z00_8110;
																			BgL_zc3z04anonymousza33046ze3z87_8114:
																				if (NULLP(BgL_l1516z00_8115))
																					{	/* Ast/let.scm 1010 */
																						BgL_arg3043z00_8108 =
																							BgL_head1518z00_8110;
																					}
																				else
																					{	/* Ast/let.scm 1010 */
																						obj_t BgL_newtail1520z00_8117;

																						{	/* Ast/let.scm 1010 */
																							obj_t BgL_arg3050z00_8118;

																							{	/* Ast/let.scm 571 */
																								obj_t BgL_pairz00_8119;

																								BgL_pairz00_8119 =
																									CAR(
																									((obj_t) BgL_l1516z00_8115));
																								BgL_arg3050z00_8118 =
																									CAR(BgL_pairz00_8119);
																							}
																							BgL_newtail1520z00_8117 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg3050z00_8118, BNIL);
																						}
																						SET_CDR(BgL_tail1519z00_8116,
																							BgL_newtail1520z00_8117);
																						{	/* Ast/let.scm 1010 */
																							obj_t BgL_arg3049z00_8120;

																							BgL_arg3049z00_8120 =
																								CDR(
																								((obj_t) BgL_l1516z00_8115));
																							{
																								obj_t BgL_tail1519z00_11830;
																								obj_t BgL_l1516z00_11829;

																								BgL_l1516z00_11829 =
																									BgL_arg3049z00_8120;
																								BgL_tail1519z00_11830 =
																									BgL_newtail1520z00_8117;
																								BgL_tail1519z00_8116 =
																									BgL_tail1519z00_11830;
																								BgL_l1516z00_8115 =
																									BgL_l1516z00_11829;
																								goto
																									BgL_zc3z04anonymousza33046ze3z87_8114;
																							}
																						}
																					}
																			}
																		}
																	}
																	BgL_arg3044z00_8109 =
																		MAKE_YOUNG_PAIR(BgL_bodyz00_7824, BNIL);
																	BgL_arg3038z00_8107 =
																		MAKE_YOUNG_PAIR(BgL_arg3043z00_8108,
																		BgL_arg3044z00_8109);
																}
																BgL_arg3037z00_8106 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(4),
																	BgL_arg3038z00_8107);
															}
															BgL_arg3023z00_8094 =
																MAKE_YOUNG_PAIR(BgL_arg3037z00_8106, BNIL);
														}
														BgL_arg3020z00_8092 =
															MAKE_YOUNG_PAIR(BgL_arg3021z00_8093,
															BgL_arg3023z00_8094);
													}
													BgL_arg3019z00_8091 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(4),
														BgL_arg3020z00_8092);
												}
												return
													BGl_sexpzd2ze3nodez31zzast_sexpz00
													(BgL_arg3019z00_8091, BgL_stackz00_7820,
													BgL_locz00_7821, BgL_sitez00_7822);
											}
										else
											{	/* Ast/let.scm 1007 */
												BgL_ebindingsz00_8003 = BgL_ebindingsz00_7823;
												BgL_bodyz00_8004 = BgL_bodyz00_7824;
												{	/* Ast/let.scm 937 */
													obj_t BgL_splitz00_8005;

													{	/* Ast/let.scm 975 */
														int BgL_tmpz00_11840;

														BgL_tmpz00_11840 = (int) (0L);
														BgL_splitz00_8005 =
															MAKE_EL_PROCEDURE(BgL_tmpz00_11840);
													}
													BgL_ebindingsz00_8006 = BgL_ebindingsz00_8003;
													BgL_bodyz00_8007 = BgL_bodyz00_8004;
													BgL_splitz00_8008 = BgL_splitz00_8005;
													BgL_kontz00_8009 = BgL_stage7z00_7819;
													if (NULLP(BgL_ebindingsz00_8006))
														{	/* Ast/let.scm 720 */
															return
																BGl_sexpzd2ze3nodez31zzast_sexpz00
																(BgL_bodyz00_8007, BgL_stackz00_7820,
																BgL_locz00_7821, BgL_sitez00_7822);
														}
													else
														{	/* Ast/let.scm 723 */
															obj_t BgL_recza2zd2bindingsz70_8010;

															BgL_recza2zd2bindingsz70_8010 =
																BGl_z62split3805z62zzast_letz00
																(BgL_splitz00_8008, BgL_ebindingsz00_8006);
															{	/* Ast/let.scm 724 */
																obj_t BgL_tailzd2bindingszd2_8011;

																{	/* Ast/let.scm 729 */
																	obj_t BgL_tmpz00_8012;

																	{	/* Ast/let.scm 729 */
																		int BgL_tmpz00_11850;

																		BgL_tmpz00_11850 = (int) (1L);
																		BgL_tmpz00_8012 =
																			BGL_MVALUES_VAL(BgL_tmpz00_11850);
																	}
																	{	/* Ast/let.scm 729 */
																		int BgL_tmpz00_11853;

																		BgL_tmpz00_11853 = (int) (1L);
																		BGL_MVALUES_VAL_SET(BgL_tmpz00_11853,
																			BUNSPEC);
																	}
																	BgL_tailzd2bindingszd2_8011 = BgL_tmpz00_8012;
																}
																if (PAIRP(BgL_tailzd2bindingszd2_8011))
																	{	/* Ast/let.scm 731 */
																		obj_t BgL_arg2361z00_8013;

																		{	/* Ast/let.scm 731 */
																			obj_t BgL_arg2363z00_8014;

																			{	/* Ast/let.scm 731 */
																				obj_t BgL_arg2364z00_8015;
																				obj_t BgL_arg2365z00_8016;

																				if (NULLP
																					(BgL_recza2zd2bindingsz70_8010))
																					{	/* Ast/let.scm 731 */
																						BgL_arg2364z00_8015 = BNIL;
																					}
																				else
																					{	/* Ast/let.scm 731 */
																						obj_t BgL_head1449z00_8017;

																						{	/* Ast/let.scm 731 */
																							obj_t BgL_arg2373z00_8018;

																							{	/* Ast/let.scm 571 */
																								obj_t BgL_pairz00_8019;

																								BgL_pairz00_8019 =
																									CAR(
																									((obj_t)
																										BgL_recza2zd2bindingsz70_8010));
																								BgL_arg2373z00_8018 =
																									CAR(BgL_pairz00_8019);
																							}
																							BgL_head1449z00_8017 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2373z00_8018, BNIL);
																						}
																						{	/* Ast/let.scm 731 */
																							obj_t BgL_g1452z00_8020;

																							BgL_g1452z00_8020 =
																								CDR(
																								((obj_t)
																									BgL_recza2zd2bindingsz70_8010));
																							{
																								obj_t BgL_l1447z00_8022;
																								obj_t BgL_tail1450z00_8023;

																								BgL_l1447z00_8022 =
																									BgL_g1452z00_8020;
																								BgL_tail1450z00_8023 =
																									BgL_head1449z00_8017;
																							BgL_zc3z04anonymousza32367ze3z87_8021:
																								if (NULLP
																									(BgL_l1447z00_8022))
																									{	/* Ast/let.scm 731 */
																										BgL_arg2364z00_8015 =
																											BgL_head1449z00_8017;
																									}
																								else
																									{	/* Ast/let.scm 731 */
																										obj_t
																											BgL_newtail1451z00_8024;
																										{	/* Ast/let.scm 731 */
																											obj_t BgL_arg2370z00_8025;

																											{	/* Ast/let.scm 571 */
																												obj_t BgL_pairz00_8026;

																												BgL_pairz00_8026 =
																													CAR(
																													((obj_t)
																														BgL_l1447z00_8022));
																												BgL_arg2370z00_8025 =
																													CAR(BgL_pairz00_8026);
																											}
																											BgL_newtail1451z00_8024 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg2370z00_8025,
																												BNIL);
																										}
																										SET_CDR
																											(BgL_tail1450z00_8023,
																											BgL_newtail1451z00_8024);
																										{	/* Ast/let.scm 731 */
																											obj_t BgL_arg2369z00_8027;

																											BgL_arg2369z00_8027 =
																												CDR(
																												((obj_t)
																													BgL_l1447z00_8022));
																											{
																												obj_t
																													BgL_tail1450z00_11876;
																												obj_t
																													BgL_l1447z00_11875;
																												BgL_l1447z00_11875 =
																													BgL_arg2369z00_8027;
																												BgL_tail1450z00_11876 =
																													BgL_newtail1451z00_8024;
																												BgL_tail1450z00_8023 =
																													BgL_tail1450z00_11876;
																												BgL_l1447z00_8022 =
																													BgL_l1447z00_11875;
																												goto
																													BgL_zc3z04anonymousza32367ze3z87_8021;
																											}
																										}
																									}
																							}
																						}
																					}
																				{	/* Ast/let.scm 732 */
																					obj_t BgL_arg2375z00_8028;

																					{	/* Ast/let.scm 732 */
																						obj_t BgL_arg2376z00_8029;

																						{	/* Ast/let.scm 732 */
																							obj_t BgL_arg2377z00_8030;
																							obj_t BgL_arg2378z00_8031;

																							{	/* Ast/let.scm 732 */
																								obj_t BgL_head1455z00_8032;

																								BgL_head1455z00_8032 =
																									MAKE_YOUNG_PAIR(CAR(CAR
																										(BgL_tailzd2bindingszd2_8011)),
																									BNIL);
																								{	/* Ast/let.scm 732 */
																									obj_t BgL_g1458z00_8033;

																									BgL_g1458z00_8033 =
																										CDR
																										(BgL_tailzd2bindingszd2_8011);
																									{
																										obj_t BgL_l1453z00_8035;
																										obj_t BgL_tail1456z00_8036;

																										BgL_l1453z00_8035 =
																											BgL_g1458z00_8033;
																										BgL_tail1456z00_8036 =
																											BgL_head1455z00_8032;
																									BgL_zc3z04anonymousza32380ze3z87_8034:
																										if (NULLP
																											(BgL_l1453z00_8035))
																											{	/* Ast/let.scm 732 */
																												BgL_arg2377z00_8030 =
																													BgL_head1455z00_8032;
																											}
																										else
																											{	/* Ast/let.scm 732 */
																												obj_t
																													BgL_newtail1457z00_8037;
																												{	/* Ast/let.scm 732 */
																													obj_t
																														BgL_arg2383z00_8038;
																													{	/* Ast/let.scm 571 */
																														obj_t
																															BgL_pairz00_8039;
																														BgL_pairz00_8039 =
																															CAR(((obj_t)
																																BgL_l1453z00_8035));
																														BgL_arg2383z00_8038
																															=
																															CAR
																															(BgL_pairz00_8039);
																													}
																													BgL_newtail1457z00_8037
																														=
																														MAKE_YOUNG_PAIR
																														(BgL_arg2383z00_8038,
																														BNIL);
																												}
																												SET_CDR
																													(BgL_tail1456z00_8036,
																													BgL_newtail1457z00_8037);
																												{	/* Ast/let.scm 732 */
																													obj_t
																														BgL_arg2382z00_8040;
																													BgL_arg2382z00_8040 =
																														CDR(((obj_t)
																															BgL_l1453z00_8035));
																													{
																														obj_t
																															BgL_tail1456z00_11891;
																														obj_t
																															BgL_l1453z00_11890;
																														BgL_l1453z00_11890 =
																															BgL_arg2382z00_8040;
																														BgL_tail1456z00_11891
																															=
																															BgL_newtail1457z00_8037;
																														BgL_tail1456z00_8036
																															=
																															BgL_tail1456z00_11891;
																														BgL_l1453z00_8035 =
																															BgL_l1453z00_11890;
																														goto
																															BgL_zc3z04anonymousza32380ze3z87_8034;
																													}
																												}
																											}
																									}
																								}
																							}
																							BgL_arg2378z00_8031 =
																								MAKE_YOUNG_PAIR
																								(BgL_bodyz00_8007, BNIL);
																							BgL_arg2376z00_8029 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2377z00_8030,
																								BgL_arg2378z00_8031);
																						}
																						BgL_arg2375z00_8028 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF(4),
																							BgL_arg2376z00_8029);
																					}
																					BgL_arg2365z00_8016 =
																						MAKE_YOUNG_PAIR(BgL_arg2375z00_8028,
																						BNIL);
																				}
																				BgL_arg2363z00_8014 =
																					MAKE_YOUNG_PAIR(BgL_arg2364z00_8015,
																					BgL_arg2365z00_8016);
																			}
																			BgL_arg2361z00_8013 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(4),
																				BgL_arg2363z00_8014);
																		}
																		return
																			BGl_sexpzd2ze3nodez31zzast_sexpz00
																			(BgL_arg2361z00_8013, BgL_stackz00_7820,
																			BgL_locz00_7821, BgL_sitez00_7822);
																	}
																else
																	{	/* Ast/let.scm 729 */
																		BGL_TAIL return
																			BGl_z62stage7z62zzast_letz00
																			(BgL_kontz00_8009, BgL_ebindingsz00_8006,
																			BgL_bodyz00_8007);
																	}
															}
														}
												}
											}
									}
								}
							}
					}
				}
			}
		}

	}



/* <@anonymous:3072>~0 */
	obj_t BGl_zc3z04anonymousza33072ze3ze70z60zzast_letz00(obj_t
		BgL_l1500z00_3726)
	{
		{	/* Ast/let.scm 983 */
			if (NULLP(BgL_l1500z00_3726))
				{	/* Ast/let.scm 983 */
					return BNIL;
				}
			else
				{	/* Ast/let.scm 983 */
					obj_t BgL_arg3075z00_3729;
					obj_t BgL_arg3077z00_3730;

					{	/* Ast/let.scm 574 */
						obj_t BgL_pairz00_6558;

						BgL_pairz00_6558 = CAR(((obj_t) BgL_l1500z00_3726));
						BgL_arg3075z00_3729 = CAR(CDR(CDR(BgL_pairz00_6558)));
					}
					{	/* Ast/let.scm 983 */
						obj_t BgL_arg3079z00_3732;

						BgL_arg3079z00_3732 = CDR(((obj_t) BgL_l1500z00_3726));
						BgL_arg3077z00_3730 =
							BGl_zc3z04anonymousza33072ze3ze70z60zzast_letz00
							(BgL_arg3079z00_3732);
					}
					return bgl_append2(BgL_arg3075z00_3729, BgL_arg3077z00_3730);
				}
		}

	}



/* <@anonymous:3080>~0 */
	obj_t BGl_zc3z04anonymousza33080ze3ze70z60zzast_letz00(obj_t
		BgL_l1502z00_3735)
	{
		{	/* Ast/let.scm 984 */
			if (NULLP(BgL_l1502z00_3735))
				{	/* Ast/let.scm 984 */
					return BNIL;
				}
			else
				{	/* Ast/let.scm 984 */
					obj_t BgL_arg3082z00_3738;
					obj_t BgL_arg3083z00_3739;

					{	/* Ast/let.scm 575 */
						obj_t BgL_pairz00_6566;

						BgL_pairz00_6566 = CAR(((obj_t) BgL_l1502z00_3735));
						BgL_arg3082z00_3738 = CAR(CDR(CDR(CDR(BgL_pairz00_6566))));
					}
					{	/* Ast/let.scm 984 */
						obj_t BgL_arg3085z00_3741;

						BgL_arg3085z00_3741 = CDR(((obj_t) BgL_l1502z00_3735));
						BgL_arg3083z00_3739 =
							BGl_zc3z04anonymousza33080ze3ze70z60zzast_letz00
							(BgL_arg3085z00_3741);
					}
					return bgl_append2(BgL_arg3082z00_3738, BgL_arg3083z00_3739);
				}
		}

	}



/* &split3804 */
	obj_t BGl_z62split3804z62zzast_letz00(obj_t BgL_envz00_7825,
		obj_t BgL_ebindingsz00_7826)
	{
		{	/* Ast/let.scm 1036 */
			{
				obj_t BgL_ebindingsz00_8122;
				obj_t BgL_letza2zd2bindingsz70_8123;

				BgL_ebindingsz00_8122 = BgL_ebindingsz00_7826;
				BgL_letza2zd2bindingsz70_8123 = BNIL;
			BgL_loopz00_8121:
				if (NULLP(BgL_ebindingsz00_8122))
					{	/* Ast/let.scm 1026 */
						obj_t BgL_val0_1522z00_8124;

						BgL_val0_1522z00_8124 =
							bgl_reverse_bang(BgL_letza2zd2bindingsz70_8123);
						{	/* Ast/let.scm 1026 */
							int BgL_tmpz00_11932;

							BgL_tmpz00_11932 = (int) (2L);
							BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11932);
						}
						{	/* Ast/let.scm 1026 */
							int BgL_tmpz00_11935;

							BgL_tmpz00_11935 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_11935, BNIL);
						}
						return BgL_val0_1522z00_8124;
					}
				else
					{	/* Ast/let.scm 1027 */
						bool_t BgL_test4348z00_11938;

						{	/* Ast/let.scm 1027 */
							obj_t BgL_g1526z00_8125;

							BgL_g1526z00_8125 = CDR(((obj_t) BgL_ebindingsz00_8122));
							{
								obj_t BgL_l1524z00_8127;

								{	/* Ast/let.scm 1030 */
									obj_t BgL_tmpz00_11941;

									BgL_l1524z00_8127 = BgL_g1526z00_8125;
								BgL_zc3z04anonymousza33155ze3z87_8126:
									if (NULLP(BgL_l1524z00_8127))
										{	/* Ast/let.scm 1030 */
											BgL_tmpz00_11941 = BFALSE;
										}
									else
										{	/* Ast/let.scm 1028 */
											obj_t BgL__ortest_1527z00_8128;

											{	/* Ast/let.scm 1028 */
												obj_t BgL_auxz00_11950;
												obj_t BgL_auxz00_11944;

												{	/* Ast/let.scm 574 */
													obj_t BgL_pairz00_8130;

													BgL_pairz00_8130 =
														CAR(((obj_t) BgL_ebindingsz00_8122));
													BgL_auxz00_11950 = CAR(CDR(CDR(BgL_pairz00_8130)));
												}
												{	/* Ast/let.scm 573 */
													obj_t BgL_pairz00_8129;

													BgL_pairz00_8129 =
														CDR(((obj_t) CAR(((obj_t) BgL_l1524z00_8127))));
													BgL_auxz00_11944 = CAR(BgL_pairz00_8129);
												}
												BgL__ortest_1527z00_8128 =
													BGl_memqz00zz__r4_pairs_and_lists_6_3z00
													(BgL_auxz00_11944, BgL_auxz00_11950);
											}
											if (CBOOL(BgL__ortest_1527z00_8128))
												{	/* Ast/let.scm 1030 */
													BgL_tmpz00_11941 = BgL__ortest_1527z00_8128;
												}
											else
												{	/* Ast/let.scm 1030 */
													obj_t BgL_arg3156z00_8131;

													BgL_arg3156z00_8131 =
														CDR(((obj_t) BgL_l1524z00_8127));
													{
														obj_t BgL_l1524z00_11961;

														BgL_l1524z00_11961 = BgL_arg3156z00_8131;
														BgL_l1524z00_8127 = BgL_l1524z00_11961;
														goto BgL_zc3z04anonymousza33155ze3z87_8126;
													}
												}
										}
									BgL_test4348z00_11938 = CBOOL(BgL_tmpz00_11941);
								}
							}
						}
						if (BgL_test4348z00_11938)
							{	/* Ast/let.scm 1034 */
								obj_t BgL_val0_1528z00_8132;

								BgL_val0_1528z00_8132 =
									bgl_reverse_bang(BgL_letza2zd2bindingsz70_8123);
								{	/* Ast/let.scm 1034 */
									int BgL_tmpz00_11964;

									BgL_tmpz00_11964 = (int) (2L);
									BGL_MVALUES_NUMBER_SET(BgL_tmpz00_11964);
								}
								{	/* Ast/let.scm 1034 */
									int BgL_tmpz00_11967;

									BgL_tmpz00_11967 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_11967, BgL_ebindingsz00_8122);
								}
								return BgL_val0_1528z00_8132;
							}
						else
							{	/* Ast/let.scm 1036 */
								obj_t BgL_arg3146z00_8133;
								obj_t BgL_arg3149z00_8134;

								BgL_arg3146z00_8133 = CDR(((obj_t) BgL_ebindingsz00_8122));
								{	/* Ast/let.scm 1036 */
									obj_t BgL_arg3154z00_8135;

									BgL_arg3154z00_8135 = CAR(((obj_t) BgL_ebindingsz00_8122));
									BgL_arg3149z00_8134 =
										MAKE_YOUNG_PAIR(BgL_arg3154z00_8135,
										BgL_letza2zd2bindingsz70_8123);
								}
								{
									obj_t BgL_letza2zd2bindingsz70_11976;
									obj_t BgL_ebindingsz00_11975;

									BgL_ebindingsz00_11975 = BgL_arg3146z00_8133;
									BgL_letza2zd2bindingsz70_11976 = BgL_arg3149z00_8134;
									BgL_letza2zd2bindingsz70_8123 =
										BgL_letza2zd2bindingsz70_11976;
									BgL_ebindingsz00_8122 = BgL_ebindingsz00_11975;
									goto BgL_loopz00_8121;
								}
							}
					}
			}
		}

	}



/* &stage7 */
	BgL_nodez00_bglt BGl_z62stage7z62zzast_letz00(obj_t BgL_envz00_7827,
		obj_t BgL_ebindingsz00_7831, obj_t BgL_bodyz00_7832)
	{
		{	/* Ast/let.scm 930 */
			{	/* Ast/let.scm 913 */
				obj_t BgL_locz00_7828;
				obj_t BgL_stackz00_7829;
				obj_t BgL_sitez00_7830;

				BgL_locz00_7828 = PROCEDURE_EL_REF(BgL_envz00_7827, (int) (0L));
				BgL_stackz00_7829 = PROCEDURE_EL_REF(BgL_envz00_7827, (int) (1L));
				BgL_sitez00_7830 =
					((obj_t) PROCEDURE_EL_REF(BgL_envz00_7827, (int) (2L)));
				{
					obj_t BgL_ebindingsz00_8137;
					obj_t BgL_bodyz00_8138;

					{	/* Ast/let.scm 913 */
						obj_t BgL_lastz00_8155;

						BgL_lastz00_8155 =
							CAR(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00
							(BgL_ebindingsz00_7831));
						{	/* Ast/let.scm 913 */
							BgL_typez00_bglt BgL_tz00_8156;

							{	/* Ast/let.scm 914 */
								obj_t BgL_arg2956z00_8157;
								obj_t BgL_arg2966z00_8158;

								{	/* Ast/let.scm 914 */
									obj_t BgL_pairz00_8159;

									BgL_pairz00_8159 = CAR(((obj_t) BgL_lastz00_8155));
									BgL_arg2956z00_8157 = CAR(BgL_pairz00_8159);
								}
								{	/* Ast/let.scm 915 */
									obj_t BgL_arg2968z00_8160;

									BgL_arg2968z00_8160 = CAR(((obj_t) BgL_lastz00_8155));
									BgL_arg2966z00_8158 =
										BGl_findzd2locationzd2zztools_locationz00
										(BgL_arg2968z00_8160);
								}
								BgL_tz00_8156 =
									BGl_typezd2ofzd2idz00zzast_identz00(BgL_arg2956z00_8157,
									BgL_arg2966z00_8158);
							}
							{	/* Ast/let.scm 914 */

								{	/* Ast/let.scm 916 */
									bool_t BgL_test4351z00_11993;

									if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
												(((BgL_typez00_bglt) COBJECT(BgL_tz00_8156))->
													BgL_idz00), CNST_TABLE_REF(13))))
										{	/* Ast/let.scm 917 */
											bool_t BgL_test4353z00_11999;

											{	/* Ast/let.scm 917 */
												obj_t BgL_arg2954z00_8161;

												{	/* Ast/let.scm 572 */
													obj_t BgL_pairz00_8162;

													BgL_pairz00_8162 = CAR(((obj_t) BgL_lastz00_8155));
													BgL_arg2954z00_8161 = CAR(CDR(BgL_pairz00_8162));
												}
												{	/* Ast/let.scm 917 */

													BgL_test4353z00_11999 =
														CBOOL(BGl_functionzf3zf3zzast_letz00
														(BgL_arg2954z00_8161, BFALSE));
												}
											}
											if (BgL_test4353z00_11999)
												{	/* Ast/let.scm 917 */
													BgL_test4351z00_11993 = ((bool_t) 0);
												}
											else
												{	/* Ast/let.scm 917 */
													BgL_test4351z00_11993 = ((bool_t) 1);
												}
										}
									else
										{	/* Ast/let.scm 916 */
											BgL_test4351z00_11993 = ((bool_t) 0);
										}
									if (BgL_test4351z00_11993)
										{	/* Ast/let.scm 921 */
											obj_t BgL_arg2912z00_8163;

											{	/* Ast/let.scm 921 */
												obj_t BgL_arg2913z00_8164;

												{	/* Ast/let.scm 921 */
													obj_t BgL_arg2914z00_8165;
													obj_t BgL_arg2915z00_8166;

													{	/* Ast/let.scm 921 */
														obj_t BgL_arg2916z00_8167;

														{	/* Ast/let.scm 921 */
															obj_t BgL_arg2917z00_8168;
															obj_t BgL_arg2918z00_8169;

															{	/* Ast/let.scm 573 */
																obj_t BgL_pairz00_8170;

																BgL_pairz00_8170 =
																	CDR(((obj_t) BgL_lastz00_8155));
																BgL_arg2917z00_8168 = CAR(BgL_pairz00_8170);
															}
															BgL_arg2918z00_8169 =
																MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
															BgL_arg2916z00_8167 =
																MAKE_YOUNG_PAIR(BgL_arg2917z00_8168,
																BgL_arg2918z00_8169);
														}
														BgL_arg2914z00_8165 =
															MAKE_YOUNG_PAIR(BgL_arg2916z00_8167, BNIL);
													}
													{	/* Ast/let.scm 923 */
														obj_t BgL_arg2919z00_8171;

														{	/* Ast/let.scm 923 */
															obj_t BgL_arg2920z00_8172;

															{	/* Ast/let.scm 923 */
																obj_t BgL_arg2923z00_8173;
																obj_t BgL_arg2924z00_8174;

																{	/* Ast/let.scm 923 */
																	obj_t BgL_arg2925z00_8175;

																	{	/* Ast/let.scm 923 */
																		obj_t BgL_arg2926z00_8176;

																		{	/* Ast/let.scm 923 */
																			obj_t BgL_l1475z00_8177;

																			BgL_l1475z00_8177 =
																				bgl_reverse(BgL_ebindingsz00_7831);
																			if (NULLP(BgL_l1475z00_8177))
																				{	/* Ast/let.scm 923 */
																					BgL_arg2926z00_8176 = BNIL;
																				}
																			else
																				{	/* Ast/let.scm 923 */
																					obj_t BgL_head1477z00_8178;

																					BgL_head1477z00_8178 =
																						MAKE_YOUNG_PAIR(CAR(CAR
																							(BgL_l1475z00_8177)), BNIL);
																					{	/* Ast/let.scm 923 */
																						obj_t BgL_g1480z00_8179;

																						BgL_g1480z00_8179 =
																							CDR(BgL_l1475z00_8177);
																						{
																							obj_t BgL_l1475z00_8181;
																							obj_t BgL_tail1478z00_8182;

																							BgL_l1475z00_8181 =
																								BgL_g1480z00_8179;
																							BgL_tail1478z00_8182 =
																								BgL_head1477z00_8178;
																						BgL_zc3z04anonymousza32928ze3z87_8180:
																							if (NULLP
																								(BgL_l1475z00_8181))
																								{	/* Ast/let.scm 923 */
																									BgL_arg2926z00_8176 =
																										BgL_head1477z00_8178;
																								}
																							else
																								{	/* Ast/let.scm 923 */
																									obj_t BgL_newtail1479z00_8183;

																									{	/* Ast/let.scm 923 */
																										obj_t BgL_arg2931z00_8184;

																										{	/* Ast/let.scm 571 */
																											obj_t BgL_pairz00_8185;

																											BgL_pairz00_8185 =
																												CAR(
																												((obj_t)
																													BgL_l1475z00_8181));
																											BgL_arg2931z00_8184 =
																												CAR(BgL_pairz00_8185);
																										}
																										BgL_newtail1479z00_8183 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg2931z00_8184,
																											BNIL);
																									}
																									SET_CDR(BgL_tail1478z00_8182,
																										BgL_newtail1479z00_8183);
																									{	/* Ast/let.scm 923 */
																										obj_t BgL_arg2930z00_8186;

																										BgL_arg2930z00_8186 =
																											CDR(
																											((obj_t)
																												BgL_l1475z00_8181));
																										{
																											obj_t
																												BgL_tail1478z00_12029;
																											obj_t BgL_l1475z00_12028;

																											BgL_l1475z00_12028 =
																												BgL_arg2930z00_8186;
																											BgL_tail1478z00_12029 =
																												BgL_newtail1479z00_8183;
																											BgL_tail1478z00_8182 =
																												BgL_tail1478z00_12029;
																											BgL_l1475z00_8181 =
																												BgL_l1475z00_12028;
																											goto
																												BgL_zc3z04anonymousza32928ze3z87_8180;
																										}
																									}
																								}
																						}
																					}
																				}
																		}
																		BgL_arg2925z00_8175 =
																			CDR(((obj_t) BgL_arg2926z00_8176));
																	}
																	BgL_arg2923z00_8173 =
																		bgl_reverse(BgL_arg2925z00_8175);
																}
																{	/* Ast/let.scm 925 */
																	obj_t BgL_arg2935z00_8187;
																	obj_t BgL_arg2936z00_8188;

																	{	/* Ast/let.scm 925 */
																		obj_t BgL_arg2940z00_8189;

																		{	/* Ast/let.scm 925 */
																			obj_t BgL_arg2941z00_8190;
																			obj_t BgL_arg2942z00_8191;

																			{	/* Ast/let.scm 573 */
																				obj_t BgL_pairz00_8192;

																				BgL_pairz00_8192 =
																					CDR(((obj_t) BgL_lastz00_8155));
																				BgL_arg2941z00_8190 =
																					CAR(BgL_pairz00_8192);
																			}
																			{	/* Ast/let.scm 925 */
																				obj_t BgL_arg2943z00_8193;

																				{	/* Ast/let.scm 572 */
																					obj_t BgL_pairz00_8194;

																					BgL_pairz00_8194 =
																						CAR(((obj_t) BgL_lastz00_8155));
																					BgL_arg2943z00_8193 =
																						CAR(CDR(BgL_pairz00_8194));
																				}
																				BgL_arg2942z00_8191 =
																					MAKE_YOUNG_PAIR(BgL_arg2943z00_8193,
																					BNIL);
																			}
																			BgL_arg2940z00_8189 =
																				MAKE_YOUNG_PAIR(BgL_arg2941z00_8190,
																				BgL_arg2942z00_8191);
																		}
																		BgL_arg2935z00_8187 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
																			BgL_arg2940z00_8189);
																	}
																	BgL_arg2936z00_8188 =
																		MAKE_YOUNG_PAIR(BgL_bodyz00_7832, BNIL);
																	BgL_arg2924z00_8174 =
																		MAKE_YOUNG_PAIR(BgL_arg2935z00_8187,
																		BgL_arg2936z00_8188);
																}
																BgL_arg2920z00_8172 =
																	MAKE_YOUNG_PAIR(BgL_arg2923z00_8173,
																	BgL_arg2924z00_8174);
															}
															BgL_arg2919z00_8171 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(4),
																BgL_arg2920z00_8172);
														}
														BgL_arg2915z00_8166 =
															MAKE_YOUNG_PAIR(BgL_arg2919z00_8171, BNIL);
													}
													BgL_arg2913z00_8164 =
														MAKE_YOUNG_PAIR(BgL_arg2914z00_8165,
														BgL_arg2915z00_8166);
												}
												BgL_arg2912z00_8163 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
													BgL_arg2913z00_8164);
											}
											return
												BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_arg2912z00_8163,
												BgL_stackz00_7829, BgL_locz00_7828, BgL_sitez00_7830);
										}
									else
										{	/* Ast/let.scm 916 */
											BgL_ebindingsz00_8137 = BgL_ebindingsz00_7831;
											BgL_bodyz00_8138 = BgL_bodyz00_7832;
											{	/* Ast/let.scm 891 */
												obj_t BgL_sexpz00_8139;

												{	/* Ast/let.scm 892 */
													obj_t BgL_arg2828z00_8140;

													{	/* Ast/let.scm 892 */
														obj_t BgL_arg2829z00_8141;
														obj_t BgL_arg2830z00_8142;

														{	/* Ast/let.scm 891 */
															obj_t BgL_list2832z00_8144;

															BgL_list2832z00_8144 =
																MAKE_YOUNG_PAIR(BgL_ebindingsz00_8137, BNIL);
															BgL_arg2829z00_8141 =
																BGl_filterzd2mapzd2zz__r4_control_features_6_9z00
																(BGl_proc3813z00zzast_letz00,
																BgL_list2832z00_8144);
														}
														{	/* Ast/let.scm 897 */
															obj_t BgL_arg2856z00_8145;

															{	/* Ast/let.scm 897 */
																obj_t BgL_arg2858z00_8146;

																{	/* Ast/let.scm 897 */
																	obj_t BgL_arg2859z00_8147;
																	obj_t BgL_arg2864z00_8148;

																	{	/* Ast/let.scm 896 */
																		obj_t BgL_list2871z00_8150;

																		BgL_list2871z00_8150 =
																			MAKE_YOUNG_PAIR(BgL_ebindingsz00_8137,
																			BNIL);
																		BgL_arg2859z00_8147 =
																			BGl_filterzd2mapzd2zz__r4_control_features_6_9z00
																			(BGl_proc3814z00zzast_letz00,
																			BgL_list2871z00_8150);
																	}
																	{	/* Ast/let.scm 902 */
																		obj_t BgL_arg2883z00_8151;
																		obj_t BgL_arg2887z00_8152;

																		{	/* Ast/let.scm 902 */
																			obj_t
																				BgL_zc3z04anonymousza32890ze3z87_8153;
																			BgL_zc3z04anonymousza32890ze3z87_8153 =
																				MAKE_FX_PROCEDURE
																				(BGl_z62zc3z04anonymousza32890ze3ze5zzast_letz00,
																				(int) (1L), (int) (1L));
																			PROCEDURE_SET
																				(BgL_zc3z04anonymousza32890ze3z87_8153,
																				(int) (0L), BgL_locz00_7828);
																			{	/* Ast/let.scm 901 */
																				obj_t BgL_list2889z00_8154;

																				BgL_list2889z00_8154 =
																					MAKE_YOUNG_PAIR(BgL_ebindingsz00_8137,
																					BNIL);
																				BgL_arg2883z00_8151 =
																					BGl_filterzd2mapzd2zz__r4_control_features_6_9z00
																					(BgL_zc3z04anonymousza32890ze3z87_8153,
																					BgL_list2889z00_8154);
																		}}
																		BgL_arg2887z00_8152 =
																			MAKE_YOUNG_PAIR(BgL_bodyz00_8138, BNIL);
																		BgL_arg2864z00_8148 =
																			BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																			(BgL_arg2883z00_8151,
																			BgL_arg2887z00_8152);
																	}
																	BgL_arg2858z00_8146 =
																		MAKE_YOUNG_PAIR(BgL_arg2859z00_8147,
																		BgL_arg2864z00_8148);
																}
																BgL_arg2856z00_8145 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
																	BgL_arg2858z00_8146);
															}
															BgL_arg2830z00_8142 =
																MAKE_YOUNG_PAIR(BgL_arg2856z00_8145, BNIL);
														}
														BgL_arg2828z00_8140 =
															MAKE_YOUNG_PAIR(BgL_arg2829z00_8141,
															BgL_arg2830z00_8142);
													}
													BgL_sexpz00_8139 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
														BgL_arg2828z00_8140);
												}
												return
													BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_sexpz00_8139,
													BgL_stackz00_7829, BgL_locz00_7828, BgL_sitez00_7830);
											}
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* &split3805 */
	obj_t BGl_z62split3805z62zzast_letz00(obj_t BgL_envz00_7833,
		obj_t BgL_ebindingsz00_7834)
	{
		{	/* Ast/let.scm 963 */
			{	/* Ast/let.scm 937 */
				obj_t BgL_g1192z00_8195;

				BgL_g1192z00_8195 = bgl_reverse(BgL_ebindingsz00_7834);
				{
					obj_t BgL_ebindingsz00_8197;
					obj_t BgL_recza2zd2bindingsz70_8198;
					obj_t BgL_letzd2bindingszd2_8199;

					BgL_ebindingsz00_8197 = BgL_g1192z00_8195;
					BgL_recza2zd2bindingsz70_8198 = BNIL;
					BgL_letzd2bindingszd2_8199 = BNIL;
				BgL_loopz00_8196:
					if (NULLP(BgL_ebindingsz00_8197))
						{	/* Ast/let.scm 941 */
							{	/* Ast/let.scm 942 */
								int BgL_tmpz00_12078;

								BgL_tmpz00_12078 = (int) (2L);
								BGL_MVALUES_NUMBER_SET(BgL_tmpz00_12078);
							}
							{	/* Ast/let.scm 942 */
								int BgL_tmpz00_12081;

								BgL_tmpz00_12081 = (int) (1L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_12081,
									BgL_letzd2bindingszd2_8199);
							}
							return BgL_recza2zd2bindingsz70_8198;
						}
					else
						{	/* Ast/let.scm 943 */
							bool_t BgL_test4357z00_12084;

							{	/* Ast/let.scm 943 */
								obj_t BgL_arg3013z00_8200;
								obj_t BgL_arg3014z00_8201;

								BgL_arg3013z00_8200 = CAR(((obj_t) BgL_ebindingsz00_8197));
								BgL_arg3014z00_8201 = CDR(((obj_t) BgL_ebindingsz00_8197));
								if (CBOOL(BGl_z62mutablezd2inzf3z43zzast_letz00
										(BgL_arg3013z00_8200, ((obj_t) BgL_arg3014z00_8201))))
									{	/* Ast/let.scm 585 */
										BgL_test4357z00_12084 = ((bool_t) 0);
									}
								else
									{	/* Ast/let.scm 585 */
										BgL_test4357z00_12084 = ((bool_t) 1);
									}
							}
							if (BgL_test4357z00_12084)
								{	/* Ast/let.scm 945 */
									bool_t BgL_test4359z00_12093;

									{	/* Ast/let.scm 945 */
										obj_t BgL_arg3010z00_8202;

										{	/* Ast/let.scm 572 */
											obj_t BgL_pairz00_8203;

											BgL_pairz00_8203 =
												CAR(((obj_t) CAR(((obj_t) BgL_ebindingsz00_8197))));
											BgL_arg3010z00_8202 = CAR(CDR(BgL_pairz00_8203));
										}
										{	/* Ast/let.scm 945 */

											BgL_test4359z00_12093 =
												CBOOL(BGl_functionzf3zf3zzast_letz00
												(BgL_arg3010z00_8202, BFALSE));
										}
									}
									if (BgL_test4359z00_12093)
										{	/* Ast/let.scm 946 */
											obj_t BgL_arg2985z00_8204;
											obj_t BgL_arg2986z00_8205;

											BgL_arg2985z00_8204 =
												CDR(((obj_t) BgL_ebindingsz00_8197));
											{	/* Ast/let.scm 947 */
												obj_t BgL_arg2987z00_8206;

												BgL_arg2987z00_8206 =
													CAR(((obj_t) BgL_ebindingsz00_8197));
												BgL_arg2986z00_8205 =
													MAKE_YOUNG_PAIR(BgL_arg2987z00_8206,
													BgL_recza2zd2bindingsz70_8198);
											}
											{
												obj_t BgL_recza2zd2bindingsz70_12108;
												obj_t BgL_ebindingsz00_12107;

												BgL_ebindingsz00_12107 = BgL_arg2985z00_8204;
												BgL_recza2zd2bindingsz70_12108 = BgL_arg2986z00_8205;
												BgL_recza2zd2bindingsz70_8198 =
													BgL_recza2zd2bindingsz70_12108;
												BgL_ebindingsz00_8197 = BgL_ebindingsz00_12107;
												goto BgL_loopz00_8196;
											}
										}
									else
										{	/* Ast/let.scm 949 */
											bool_t BgL_test4360z00_12109;

											{	/* Ast/let.scm 949 */
												bool_t BgL_test4361z00_12110;

												{	/* Ast/let.scm 949 */
													obj_t BgL_arg3009z00_8207;

													BgL_arg3009z00_8207 =
														CAR(((obj_t) BgL_ebindingsz00_8197));
													BgL_test4361z00_12110 =
														CBOOL(BGl_z62usedzd2inzf3z43zzast_letz00
														(BgL_arg3009z00_8207,
															BgL_recza2zd2bindingsz70_8198));
												}
												if (BgL_test4361z00_12110)
													{	/* Ast/let.scm 949 */
														BgL_test4360z00_12109 = ((bool_t) 0);
													}
												else
													{	/* Ast/let.scm 950 */
														bool_t BgL_test4362z00_12115;

														{	/* Ast/let.scm 950 */
															obj_t BgL_arg3003z00_8208;
															obj_t BgL_arg3008z00_8209;

															BgL_arg3003z00_8208 =
																CAR(((obj_t) BgL_ebindingsz00_8197));
															BgL_arg3008z00_8209 =
																CDR(((obj_t) BgL_ebindingsz00_8197));
															BgL_test4362z00_12115 =
																CBOOL(BGl_z62usedzd2inzf3z43zzast_letz00
																(BgL_arg3003z00_8208, BgL_arg3008z00_8209));
														}
														if (BgL_test4362z00_12115)
															{	/* Ast/let.scm 950 */
																BgL_test4360z00_12109 = ((bool_t) 0);
															}
														else
															{	/* Ast/let.scm 950 */
																BgL_test4360z00_12109 = ((bool_t) 1);
															}
													}
											}
											if (BgL_test4360z00_12109)
												{	/* Ast/let.scm 951 */
													obj_t BgL_arg2999z00_8210;
													obj_t BgL_arg3000z00_8211;

													BgL_arg2999z00_8210 =
														CDR(((obj_t) BgL_ebindingsz00_8197));
													{	/* Ast/let.scm 953 */
														obj_t BgL_arg3001z00_8212;

														BgL_arg3001z00_8212 =
															CAR(((obj_t) BgL_ebindingsz00_8197));
														BgL_arg3000z00_8211 =
															MAKE_YOUNG_PAIR(BgL_arg3001z00_8212,
															BgL_letzd2bindingszd2_8199);
													}
													{
														obj_t BgL_letzd2bindingszd2_12128;
														obj_t BgL_ebindingsz00_12127;

														BgL_ebindingsz00_12127 = BgL_arg2999z00_8210;
														BgL_letzd2bindingszd2_12128 = BgL_arg3000z00_8211;
														BgL_letzd2bindingszd2_8199 =
															BgL_letzd2bindingszd2_12128;
														BgL_ebindingsz00_8197 = BgL_ebindingsz00_12127;
														goto BgL_loopz00_8196;
													}
												}
											else
												{	/* Ast/let.scm 956 */
													obj_t BgL_val0_1483z00_8213;

													BgL_val0_1483z00_8213 =
														BGl_appendzd221011zd2zzast_letz00(bgl_reverse
														(BgL_ebindingsz00_8197),
														BgL_recza2zd2bindingsz70_8198);
													{	/* Ast/let.scm 956 */
														int BgL_tmpz00_12131;

														BgL_tmpz00_12131 = (int) (2L);
														BGL_MVALUES_NUMBER_SET(BgL_tmpz00_12131);
													}
													{	/* Ast/let.scm 956 */
														int BgL_tmpz00_12134;

														BgL_tmpz00_12134 = (int) (1L);
														BGL_MVALUES_VAL_SET(BgL_tmpz00_12134,
															BgL_letzd2bindingszd2_8199);
													}
													return BgL_val0_1483z00_8213;
												}
										}
								}
							else
								{	/* Ast/let.scm 962 */
									obj_t BgL_val0_1485z00_8214;

									BgL_val0_1485z00_8214 =
										BGl_appendzd221011zd2zzast_letz00(bgl_reverse
										(BgL_ebindingsz00_8197), BgL_recza2zd2bindingsz70_8198);
									{	/* Ast/let.scm 962 */
										int BgL_tmpz00_12139;

										BgL_tmpz00_12139 = (int) (2L);
										BGL_MVALUES_NUMBER_SET(BgL_tmpz00_12139);
									}
									{	/* Ast/let.scm 962 */
										int BgL_tmpz00_12142;

										BgL_tmpz00_12142 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_12142,
											BgL_letzd2bindingszd2_8199);
									}
									return BgL_val0_1485z00_8214;
								}
						}
				}
			}
		}

	}



/* &<@anonymous:2890> */
	obj_t BGl_z62zc3z04anonymousza32890ze3ze5zzast_letz00(obj_t BgL_envz00_7835,
		obj_t BgL_bz00_7837)
	{
		{	/* Ast/let.scm 901 */
			{	/* Ast/let.scm 902 */
				obj_t BgL_locz00_7836;

				BgL_locz00_7836 = PROCEDURE_REF(BgL_envz00_7835, (int) (0L));
				{	/* Ast/let.scm 902 */
					bool_t BgL_test4363z00_12147;

					{	/* Ast/let.scm 902 */
						obj_t BgL_arg2898z00_8215;

						{	/* Ast/let.scm 572 */
							obj_t BgL_pairz00_8216;

							BgL_pairz00_8216 = CAR(((obj_t) BgL_bz00_7837));
							BgL_arg2898z00_8215 = CAR(CDR(BgL_pairz00_8216));
						}
						{	/* Ast/let.scm 902 */

							BgL_test4363z00_12147 =
								BGl_directzd2functionzf3z21zzast_letz00(BgL_arg2898z00_8215,
								((bool_t) 0));
					}}
					if (BgL_test4363z00_12147)
						{	/* Ast/let.scm 902 */
							return BFALSE;
						}
					else
						{	/* Ast/let.scm 903 */
							obj_t BgL_arg2893z00_8217;

							{	/* Ast/let.scm 903 */
								obj_t BgL_arg2894z00_8218;
								obj_t BgL_arg2895z00_8219;

								{	/* Ast/let.scm 903 */
									obj_t BgL_arg2896z00_8220;

									{	/* Ast/let.scm 903 */
										obj_t BgL_pairz00_8221;

										BgL_pairz00_8221 = CAR(((obj_t) BgL_bz00_7837));
										BgL_arg2896z00_8220 = CAR(BgL_pairz00_8221);
									}
									BgL_arg2894z00_8218 =
										BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
										(BgL_arg2896z00_8220, BgL_locz00_7836);
								}
								{	/* Ast/let.scm 904 */
									obj_t BgL_arg2897z00_8222;

									{	/* Ast/let.scm 572 */
										obj_t BgL_pairz00_8223;

										BgL_pairz00_8223 = CAR(((obj_t) BgL_bz00_7837));
										BgL_arg2897z00_8222 = CAR(CDR(BgL_pairz00_8223));
									}
									BgL_arg2895z00_8219 =
										MAKE_YOUNG_PAIR(BgL_arg2897z00_8222, BNIL);
								}
								BgL_arg2893z00_8217 =
									MAKE_YOUNG_PAIR(BgL_arg2894z00_8218, BgL_arg2895z00_8219);
							}
							return MAKE_YOUNG_PAIR(CNST_TABLE_REF(5), BgL_arg2893z00_8217);
						}
				}
			}
		}

	}



/* &<@anonymous:2872> */
	obj_t BGl_z62zc3z04anonymousza32872ze3ze5zzast_letz00(obj_t BgL_envz00_7838,
		obj_t BgL_bz00_7839)
	{
		{	/* Ast/let.scm 896 */
			{	/* Ast/let.scm 897 */
				bool_t BgL_test4364z00_12165;

				{	/* Ast/let.scm 897 */
					obj_t BgL_arg2882z00_8224;

					{	/* Ast/let.scm 572 */
						obj_t BgL_pairz00_8225;

						BgL_pairz00_8225 = CAR(((obj_t) BgL_bz00_7839));
						BgL_arg2882z00_8224 = CAR(CDR(BgL_pairz00_8225));
					}
					{	/* Ast/let.scm 897 */

						BgL_test4364z00_12165 =
							BGl_directzd2functionzf3z21zzast_letz00(BgL_arg2882z00_8224,
							((bool_t) 0));
					}
				}
				if (BgL_test4364z00_12165)
					{	/* Ast/let.scm 898 */
						BgL_typez00_bglt BgL_tyz00_8226;

						{	/* Ast/let.scm 898 */
							obj_t BgL_arg2879z00_8227;
							obj_t BgL_arg2880z00_8228;

							{	/* Ast/let.scm 898 */
								obj_t BgL_pairz00_8229;

								BgL_pairz00_8229 = CAR(((obj_t) BgL_bz00_7839));
								BgL_arg2879z00_8227 = CAR(BgL_pairz00_8229);
							}
							{	/* Ast/let.scm 898 */
								obj_t BgL_arg2881z00_8230;

								BgL_arg2881z00_8230 = CAR(((obj_t) BgL_bz00_7839));
								BgL_arg2880z00_8228 =
									BGl_findzd2locationzd2zztools_locationz00
									(BgL_arg2881z00_8230);
							}
							BgL_tyz00_8226 =
								BGl_typezd2ofzd2idz00zzast_identz00(BgL_arg2879z00_8227,
								BgL_arg2880z00_8228);
						}
						{	/* Ast/let.scm 899 */
							obj_t BgL_arg2875z00_8231;
							obj_t BgL_arg2876z00_8232;

							{	/* Ast/let.scm 899 */
								obj_t BgL_pairz00_8233;

								BgL_pairz00_8233 = CAR(((obj_t) BgL_bz00_7839));
								BgL_arg2875z00_8231 = CAR(BgL_pairz00_8233);
							}
							{	/* Ast/let.scm 572 */
								obj_t BgL_pairz00_8234;

								BgL_pairz00_8234 = CAR(((obj_t) BgL_bz00_7839));
								BgL_arg2876z00_8232 = CAR(CDR(BgL_pairz00_8234));
							}
							{	/* Ast/let.scm 899 */
								obj_t BgL_list2877z00_8235;

								{	/* Ast/let.scm 899 */
									obj_t BgL_arg2878z00_8236;

									BgL_arg2878z00_8236 =
										MAKE_YOUNG_PAIR(BgL_arg2876z00_8232, BNIL);
									BgL_list2877z00_8235 =
										MAKE_YOUNG_PAIR(BgL_arg2875z00_8231, BgL_arg2878z00_8236);
								}
								return BgL_list2877z00_8235;
							}
						}
					}
				else
					{	/* Ast/let.scm 897 */
						return BFALSE;
					}
			}
		}

	}



/* &<@anonymous:2833> */
	obj_t BGl_z62zc3z04anonymousza32833ze3ze5zzast_letz00(obj_t BgL_envz00_7840,
		obj_t BgL_bz00_7841)
	{
		{	/* Ast/let.scm 891 */
			{
				BgL_typez00_bglt BgL_typez00_8238;

				{	/* Ast/let.scm 892 */
					bool_t BgL_test4365z00_12187;

					{	/* Ast/let.scm 892 */
						obj_t BgL_arg2848z00_8265;

						{	/* Ast/let.scm 572 */
							obj_t BgL_pairz00_8266;

							BgL_pairz00_8266 = CAR(((obj_t) BgL_bz00_7841));
							BgL_arg2848z00_8265 = CAR(CDR(BgL_pairz00_8266));
						}
						{	/* Ast/let.scm 892 */

							BgL_test4365z00_12187 =
								BGl_directzd2functionzf3z21zzast_letz00(BgL_arg2848z00_8265,
								((bool_t) 0));
						}
					}
					if (BgL_test4365z00_12187)
						{	/* Ast/let.scm 892 */
							return BFALSE;
						}
					else
						{	/* Ast/let.scm 893 */
							BgL_typez00_bglt BgL_tyz00_8267;

							{	/* Ast/let.scm 893 */
								obj_t BgL_arg2844z00_8268;
								obj_t BgL_arg2846z00_8269;

								{	/* Ast/let.scm 893 */
									obj_t BgL_pairz00_8270;

									BgL_pairz00_8270 = CAR(((obj_t) BgL_bz00_7841));
									BgL_arg2844z00_8268 = CAR(BgL_pairz00_8270);
								}
								{	/* Ast/let.scm 893 */
									obj_t BgL_arg2847z00_8271;

									BgL_arg2847z00_8271 = CAR(((obj_t) BgL_bz00_7841));
									BgL_arg2846z00_8269 =
										BGl_findzd2locationzd2zztools_locationz00
										(BgL_arg2847z00_8271);
								}
								BgL_tyz00_8267 =
									BGl_typezd2ofzd2idz00zzast_identz00(BgL_arg2844z00_8268,
									BgL_arg2846z00_8269);
							}
							{	/* Ast/let.scm 894 */
								obj_t BgL_arg2836z00_8272;
								obj_t BgL_arg2837z00_8273;

								{	/* Ast/let.scm 894 */
									obj_t BgL_pairz00_8274;

									BgL_pairz00_8274 = CAR(((obj_t) BgL_bz00_7841));
									BgL_arg2836z00_8272 = CAR(BgL_pairz00_8274);
								}
								BgL_typez00_8238 = BgL_tyz00_8267;
								{	/* Ast/let.scm 762 */
									bool_t BgL_test4366z00_12203;

									{	/* Ast/let.scm 762 */
										obj_t BgL_classz00_8239;

										BgL_classz00_8239 = BGl_tclassz00zzobject_classz00;
										{	/* Ast/let.scm 762 */
											BgL_objectz00_bglt BgL_arg1807z00_8240;

											{	/* Ast/let.scm 762 */
												obj_t BgL_tmpz00_12204;

												BgL_tmpz00_12204 =
													((obj_t) ((BgL_objectz00_bglt) BgL_typez00_8238));
												BgL_arg1807z00_8240 =
													(BgL_objectz00_bglt) (BgL_tmpz00_12204);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Ast/let.scm 762 */
													long BgL_idxz00_8241;

													BgL_idxz00_8241 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8240);
													BgL_test4366z00_12203 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_8241 + 2L)) == BgL_classz00_8239);
												}
											else
												{	/* Ast/let.scm 762 */
													bool_t BgL_res3774z00_8244;

													{	/* Ast/let.scm 762 */
														obj_t BgL_oclassz00_8245;

														{	/* Ast/let.scm 762 */
															obj_t BgL_arg1815z00_8246;
															long BgL_arg1816z00_8247;

															BgL_arg1815z00_8246 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Ast/let.scm 762 */
																long BgL_arg1817z00_8248;

																BgL_arg1817z00_8248 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8240);
																BgL_arg1816z00_8247 =
																	(BgL_arg1817z00_8248 - OBJECT_TYPE);
															}
															BgL_oclassz00_8245 =
																VECTOR_REF(BgL_arg1815z00_8246,
																BgL_arg1816z00_8247);
														}
														{	/* Ast/let.scm 762 */
															bool_t BgL__ortest_1115z00_8249;

															BgL__ortest_1115z00_8249 =
																(BgL_classz00_8239 == BgL_oclassz00_8245);
															if (BgL__ortest_1115z00_8249)
																{	/* Ast/let.scm 762 */
																	BgL_res3774z00_8244 =
																		BgL__ortest_1115z00_8249;
																}
															else
																{	/* Ast/let.scm 762 */
																	long BgL_odepthz00_8250;

																	{	/* Ast/let.scm 762 */
																		obj_t BgL_arg1804z00_8251;

																		BgL_arg1804z00_8251 = (BgL_oclassz00_8245);
																		BgL_odepthz00_8250 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_8251);
																	}
																	if ((2L < BgL_odepthz00_8250))
																		{	/* Ast/let.scm 762 */
																			obj_t BgL_arg1802z00_8252;

																			{	/* Ast/let.scm 762 */
																				obj_t BgL_arg1803z00_8253;

																				BgL_arg1803z00_8253 =
																					(BgL_oclassz00_8245);
																				BgL_arg1802z00_8252 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_8253, 2L);
																			}
																			BgL_res3774z00_8244 =
																				(BgL_arg1802z00_8252 ==
																				BgL_classz00_8239);
																		}
																	else
																		{	/* Ast/let.scm 762 */
																			BgL_res3774z00_8244 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test4366z00_12203 = BgL_res3774z00_8244;
												}
										}
									}
									if (BgL_test4366z00_12203)
										{	/* Ast/let.scm 763 */
											BgL_globalz00_bglt BgL_vz00_8254;

											{
												BgL_tclassz00_bglt BgL_auxz00_12227;

												{
													obj_t BgL_auxz00_12228;

													{	/* Ast/let.scm 763 */
														BgL_objectz00_bglt BgL_tmpz00_12229;

														BgL_tmpz00_12229 =
															((BgL_objectz00_bglt)
															((BgL_typez00_bglt) BgL_typez00_8238));
														BgL_auxz00_12228 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_12229);
													}
													BgL_auxz00_12227 =
														((BgL_tclassz00_bglt) BgL_auxz00_12228);
												}
												BgL_vz00_8254 =
													(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_12227))->
													BgL_holderz00);
											}
											{	/* Ast/let.scm 764 */
												obj_t BgL_arg2410z00_8255;

												{	/* Ast/let.scm 764 */
													obj_t BgL_arg2411z00_8256;

													{	/* Ast/let.scm 764 */
														obj_t BgL_arg2412z00_8257;

														{	/* Ast/let.scm 764 */
															obj_t BgL_arg2414z00_8258;
															obj_t BgL_arg2415z00_8259;

															BgL_arg2414z00_8258 =
																(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt) BgL_vz00_8254)))->
																BgL_idz00);
															BgL_arg2415z00_8259 =
																MAKE_YOUNG_PAIR((((BgL_globalz00_bglt)
																		COBJECT(BgL_vz00_8254))->BgL_modulez00),
																BNIL);
															BgL_arg2412z00_8257 =
																MAKE_YOUNG_PAIR(BgL_arg2414z00_8258,
																BgL_arg2415z00_8259);
														}
														BgL_arg2411z00_8256 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(14),
															BgL_arg2412z00_8257);
													}
													BgL_arg2410z00_8255 =
														MAKE_YOUNG_PAIR(BgL_arg2411z00_8256, BNIL);
												}
												BgL_arg2837z00_8273 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(15),
													BgL_arg2410z00_8255);
											}
										}
									else
										{	/* Ast/let.scm 765 */
											obj_t BgL_casezd2valuezd2_8260;

											BgL_casezd2valuezd2_8260 =
												(((BgL_typez00_bglt) COBJECT(BgL_typez00_8238))->
												BgL_idz00);
											{	/* Ast/let.scm 765 */
												bool_t BgL_test4370z00_12246;

												{	/* Ast/let.scm 765 */
													bool_t BgL__ortest_1187z00_8261;

													BgL__ortest_1187z00_8261 =
														(BgL_casezd2valuezd2_8260 == CNST_TABLE_REF(16));
													if (BgL__ortest_1187z00_8261)
														{	/* Ast/let.scm 765 */
															BgL_test4370z00_12246 = BgL__ortest_1187z00_8261;
														}
													else
														{	/* Ast/let.scm 765 */
															BgL_test4370z00_12246 =
																(BgL_casezd2valuezd2_8260 ==
																CNST_TABLE_REF(17));
														}
												}
												if (BgL_test4370z00_12246)
													{	/* Ast/let.scm 765 */
														BgL_arg2837z00_8273 = BUNSPEC;
													}
												else
													{	/* Ast/let.scm 765 */
														if (
															(BgL_casezd2valuezd2_8260 == CNST_TABLE_REF(18)))
															{	/* Ast/let.scm 765 */
																BgL_arg2837z00_8273 =
																	BGL_INT8_TO_BINT8((int8_t) (0));
															}
														else
															{	/* Ast/let.scm 765 */
																if (
																	(BgL_casezd2valuezd2_8260 ==
																		CNST_TABLE_REF(19)))
																	{	/* Ast/let.scm 765 */
																		BgL_arg2837z00_8273 =
																			BGL_UINT8_TO_BUINT8((uint8_t) (0));
																	}
																else
																	{	/* Ast/let.scm 765 */
																		if (
																			(BgL_casezd2valuezd2_8260 ==
																				CNST_TABLE_REF(20)))
																			{	/* Ast/let.scm 765 */
																				BgL_arg2837z00_8273 =
																					BGL_INT16_TO_BINT16((int16_t) (0));
																			}
																		else
																			{	/* Ast/let.scm 765 */
																				if (
																					(BgL_casezd2valuezd2_8260 ==
																						CNST_TABLE_REF(21)))
																					{	/* Ast/let.scm 765 */
																						BgL_arg2837z00_8273 =
																							BGL_UINT16_TO_BUINT16((uint16_t)
																							(0));
																					}
																				else
																					{	/* Ast/let.scm 765 */
																						if (
																							(BgL_casezd2valuezd2_8260 ==
																								CNST_TABLE_REF(22)))
																							{	/* Ast/let.scm 765 */
																								BgL_arg2837z00_8273 =
																									BGl_int323815z00zzast_letz00;
																							}
																						else
																							{	/* Ast/let.scm 765 */
																								if (
																									(BgL_casezd2valuezd2_8260 ==
																										CNST_TABLE_REF(23)))
																									{	/* Ast/let.scm 765 */
																										BgL_arg2837z00_8273 =
																											BGl_uint323816z00zzast_letz00;
																									}
																								else
																									{	/* Ast/let.scm 765 */
																										if (
																											(BgL_casezd2valuezd2_8260
																												== CNST_TABLE_REF(24)))
																											{	/* Ast/let.scm 765 */
																												BgL_arg2837z00_8273 =
																													BGl_int643817z00zzast_letz00;
																											}
																										else
																											{	/* Ast/let.scm 765 */
																												if (
																													(BgL_casezd2valuezd2_8260
																														==
																														CNST_TABLE_REF(25)))
																													{	/* Ast/let.scm 765 */
																														BgL_arg2837z00_8273
																															=
																															BGl_uint643818z00zzast_letz00;
																													}
																												else
																													{	/* Ast/let.scm 765 */
																														bool_t
																															BgL_test4380z00_12280;
																														{	/* Ast/let.scm 765 */
																															bool_t
																																BgL__ortest_1188z00_8262;
																															BgL__ortest_1188z00_8262
																																=
																																(BgL_casezd2valuezd2_8260
																																==
																																CNST_TABLE_REF
																																(26));
																															if (BgL__ortest_1188z00_8262)
																																{	/* Ast/let.scm 765 */
																																	BgL_test4380z00_12280
																																		=
																																		BgL__ortest_1188z00_8262;
																																}
																															else
																																{	/* Ast/let.scm 765 */
																																	BgL_test4380z00_12280
																																		=
																																		(BgL_casezd2valuezd2_8260
																																		==
																																		CNST_TABLE_REF
																																		(27));
																																}
																														}
																														if (BgL_test4380z00_12280)
																															{	/* Ast/let.scm 765 */
																																BgL_arg2837z00_8273
																																	=
																																	BGL_REAL_CNST
																																	(BGl_real3819z00zzast_letz00);
																															}
																														else
																															{	/* Ast/let.scm 765 */
																																bool_t
																																	BgL_test4382z00_12286;
																																{	/* Ast/let.scm 765 */
																																	bool_t
																																		BgL__ortest_1189z00_8263;
																																	BgL__ortest_1189z00_8263
																																		=
																																		(BgL_casezd2valuezd2_8260
																																		==
																																		CNST_TABLE_REF
																																		(28));
																																	if (BgL__ortest_1189z00_8263)
																																		{	/* Ast/let.scm 765 */
																																			BgL_test4382z00_12286
																																				=
																																				BgL__ortest_1189z00_8263;
																																		}
																																	else
																																		{	/* Ast/let.scm 765 */
																																			BgL_test4382z00_12286
																																				=
																																				(BgL_casezd2valuezd2_8260
																																				==
																																				CNST_TABLE_REF
																																				(29));
																																		}
																																}
																																if (BgL_test4382z00_12286)
																																	{	/* Ast/let.scm 765 */
																																		BgL_arg2837z00_8273
																																			=
																																			BINT(0L);
																																	}
																																else
																																	{	/* Ast/let.scm 765 */
																																		if (
																																			(BgL_casezd2valuezd2_8260
																																				==
																																				CNST_TABLE_REF
																																				(30)))
																																			{	/* Ast/let.scm 765 */
																																				BgL_arg2837z00_8273
																																					=
																																					BFALSE;
																																			}
																																		else
																																			{	/* Ast/let.scm 765 */
																																				if (
																																					(BgL_casezd2valuezd2_8260
																																						==
																																						CNST_TABLE_REF
																																						(31)))
																																					{	/* Ast/let.scm 765 */
																																						BgL_arg2837z00_8273
																																							=
																																							CNST_TABLE_REF
																																							(32);
																																					}
																																				else
																																					{	/* Ast/let.scm 765 */
																																						if (
																																							(BgL_casezd2valuezd2_8260
																																								==
																																								CNST_TABLE_REF
																																								(33)))
																																							{	/* Ast/let.scm 779 */
																																								obj_t
																																									BgL_arg2432z00_8264;
																																								BgL_arg2432z00_8264
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BFALSE,
																																									BNIL);
																																								BgL_arg2837z00_8273
																																									=
																																									MAKE_YOUNG_PAIR
																																									(CNST_TABLE_REF
																																									(34),
																																									BgL_arg2432z00_8264);
																																							}
																																						else
																																							{	/* Ast/let.scm 765 */
																																								if ((BgL_casezd2valuezd2_8260 == CNST_TABLE_REF(35)))
																																									{	/* Ast/let.scm 765 */
																																										BgL_arg2837z00_8273
																																											=
																																											BCHAR
																																											(
																																											((unsigned char) '\000'));
																																									}
																																								else
																																									{	/* Ast/let.scm 765 */
																																										BgL_arg2837z00_8273
																																											=
																																											BGl_errorz00zz__errorz00
																																											(BGl_string3820z00zzast_letz00,
																																											BGl_string3821z00zzast_letz00,
																																											(((BgL_typez00_bglt) COBJECT(BgL_typez00_8238))->BgL_idz00));
																																									}
																																							}
																																					}
																																			}
																																	}
																															}
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
										}
								}
								{	/* Ast/let.scm 894 */
									obj_t BgL_list2838z00_8275;

									{	/* Ast/let.scm 894 */
										obj_t BgL_arg2839z00_8276;

										BgL_arg2839z00_8276 =
											MAKE_YOUNG_PAIR(BgL_arg2837z00_8273, BNIL);
										BgL_list2838z00_8275 =
											MAKE_YOUNG_PAIR(BgL_arg2836z00_8272, BgL_arg2839z00_8276);
									}
									return BgL_list2838z00_8275;
								}
							}
						}
				}
			}
		}

	}



/* _function? */
	obj_t BGl__functionzf3zf3zzast_letz00(obj_t BgL_env1581z00_42,
		obj_t BgL_opt1580z00_41)
	{
		{	/* Ast/let.scm 1223 */
			{	/* Ast/let.scm 1223 */
				obj_t BgL_expz00_4397;

				BgL_expz00_4397 = VECTOR_REF(BgL_opt1580z00_41, 0L);
				switch (VECTOR_LENGTH(BgL_opt1580z00_41))
					{
					case 1L:

						{	/* Ast/let.scm 1223 */

							return BGl_functionzf3zf3zzast_letz00(BgL_expz00_4397, BFALSE);
						}
						break;
					case 2L:

						{	/* Ast/let.scm 1223 */
							obj_t BgL_directpz00_4401;

							BgL_directpz00_4401 = VECTOR_REF(BgL_opt1580z00_41, 1L);
							{	/* Ast/let.scm 1223 */

								return
									BGl_functionzf3zf3zzast_letz00(BgL_expz00_4397,
									BgL_directpz00_4401);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* function? */
	obj_t BGl_functionzf3zf3zzast_letz00(obj_t BgL_expz00_39,
		obj_t BgL_directpz00_40)
	{
		{	/* Ast/let.scm 1223 */
			{
				obj_t BgL_predz00_4459;
				obj_t BgL_lstz00_4460;

				{
					obj_t BgL_varz00_4407;
					obj_t BgL_argsz00_4408;
					obj_t BgL_zf3zd2z21_4409;

					if (PAIRP(BgL_expz00_39))
						{	/* Ast/let.scm 1229 */
							if ((CAR(((obj_t) BgL_expz00_39)) == CNST_TABLE_REF(11)))
								{	/* Ast/let.scm 1229 */
									return BFALSE;
								}
							else
								{	/* Ast/let.scm 1229 */
									obj_t BgL_cdrzd22926zd2_4416;

									BgL_cdrzd22926zd2_4416 = CDR(((obj_t) BgL_expz00_39));
									if ((CAR(((obj_t) BgL_expz00_39)) == CNST_TABLE_REF(8)))
										{	/* Ast/let.scm 1229 */
											if (PAIRP(BgL_cdrzd22926zd2_4416))
												{	/* Ast/let.scm 1229 */
													obj_t BgL_carzd22929zd2_4420;
													obj_t BgL_cdrzd22930zd2_4421;

													BgL_carzd22929zd2_4420 = CAR(BgL_cdrzd22926zd2_4416);
													BgL_cdrzd22930zd2_4421 = CDR(BgL_cdrzd22926zd2_4416);
													if (PAIRP(BgL_carzd22929zd2_4420))
														{	/* Ast/let.scm 1229 */
															obj_t BgL_carzd22932zd2_4423;

															BgL_carzd22932zd2_4423 =
																CAR(BgL_carzd22929zd2_4420);
															if (PAIRP(BgL_carzd22932zd2_4423))
																{	/* Ast/let.scm 1229 */
																	if (NULLP(CDR(BgL_carzd22929zd2_4420)))
																		{	/* Ast/let.scm 1229 */
																			if (PAIRP(BgL_cdrzd22930zd2_4421))
																				{	/* Ast/let.scm 1229 */
																					if (NULLP(CDR
																							(BgL_cdrzd22930zd2_4421)))
																						{	/* Ast/let.scm 1229 */
																							obj_t BgL_arg3544z00_4430;
																							obj_t BgL_arg3545z00_4431;

																							BgL_arg3544z00_4430 =
																								CAR(BgL_carzd22932zd2_4423);
																							BgL_arg3545z00_4431 =
																								CAR(BgL_cdrzd22930zd2_4421);
																							{	/* Ast/let.scm 1233 */
																								obj_t BgL_arg3563z00_6838;

																								BgL_arg3563z00_6838 =
																									BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																									(BgL_arg3544z00_4430, BFALSE);
																								return
																									BBOOL((BgL_arg3563z00_6838 ==
																										BgL_arg3545z00_4431));
																							}
																						}
																					else
																						{	/* Ast/let.scm 1229 */
																							obj_t BgL_arg3546z00_4433;

																							BgL_arg3546z00_4433 =
																								CAR(((obj_t) BgL_expz00_39));
																							BgL_varz00_4407 =
																								BgL_arg3546z00_4433;
																							BgL_argsz00_4408 =
																								BgL_cdrzd22926zd2_4416;
																							BgL_zf3zd2z21_4409 =
																								BgL_cdrzd22926zd2_4416;
																						BgL_tagzd22908zd2_4410:
																							{	/* Ast/let.scm 1235 */
																								bool_t BgL__ortest_1214z00_4451;

																								BgL__ortest_1214z00_4451 =
																									(BgL_varz00_4407 ==
																									CNST_TABLE_REF(7));
																								if (BgL__ortest_1214z00_4451)
																									{	/* Ast/let.scm 1235 */
																										return
																											BBOOL
																											(BgL__ortest_1214z00_4451);
																									}
																								else
																									{	/* Ast/let.scm 1236 */
																										bool_t
																											BgL__ortest_1215z00_4452;
																										if (SYMBOLP
																											(BgL_varz00_4407))
																											{	/* Ast/let.scm 1236 */
																												obj_t
																													BgL_arg3564z00_4458;
																												BgL_arg3564z00_4458 =
																													BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																													(BgL_varz00_4407,
																													BFALSE);
																												BgL__ortest_1215z00_4452
																													=
																													(BgL_arg3564z00_4458
																													== CNST_TABLE_REF(7));
																											}
																										else
																											{	/* Ast/let.scm 1236 */
																												BgL__ortest_1215z00_4452
																													= ((bool_t) 0);
																											}
																										if (BgL__ortest_1215z00_4452)
																											{	/* Ast/let.scm 1236 */
																												return
																													BBOOL
																													(BgL__ortest_1215z00_4452);
																											}
																										else
																											{	/* Ast/let.scm 1237 */
																												obj_t
																													BgL__ortest_1216z00_4453;
																												{	/* Ast/let.scm 1237 */

																													BgL__ortest_1216z00_4453
																														=
																														BGl_functionzf3zf3zzast_letz00
																														(BgL_varz00_4407,
																														BFALSE);
																												}
																												if (CBOOL
																													(BgL__ortest_1216z00_4453))
																													{	/* Ast/let.scm 1237 */
																														return
																															BgL__ortest_1216z00_4453;
																													}
																												else
																													{	/* Ast/let.scm 1237 */
																														if (CBOOL
																															(BgL_directpz00_40))
																															{	/* Ast/let.scm 1238 */
																																return BFALSE;
																															}
																														else
																															{	/* Ast/let.scm 1238 */
																																BgL_predz00_4459
																																	=
																																	BGl_functionzf3zd2envz21zzast_letz00;
																																BgL_lstz00_4460
																																	=
																																	BgL_argsz00_4408;
																															BgL_zc3z04anonymousza33565ze3z87_4461:
																																if (PAIRP
																																	(BgL_lstz00_4460))
																																	{	/* Ast/let.scm 1227 */
																																		obj_t
																																			BgL__ortest_1213z00_4463;
																																		{	/* Ast/let.scm 1227 */
																																			obj_t
																																				BgL_arg3568z00_4465;
																																			BgL_arg3568z00_4465
																																				=
																																				CAR
																																				(BgL_lstz00_4460);
																																			BgL__ortest_1213z00_4463
																																				=
																																				BGL_PROCEDURE_CALL1
																																				(BgL_predz00_4459,
																																				BgL_arg3568z00_4465);
																																		}
																																		if (CBOOL
																																			(BgL__ortest_1213z00_4463))
																																			{	/* Ast/let.scm 1227 */
																																				return
																																					BgL__ortest_1213z00_4463;
																																			}
																																		else
																																			{
																																				obj_t
																																					BgL_lstz00_12383;
																																				BgL_lstz00_12383
																																					=
																																					CDR
																																					(BgL_lstz00_4460);
																																				BgL_lstz00_4460
																																					=
																																					BgL_lstz00_12383;
																																				goto
																																					BgL_zc3z04anonymousza33565ze3z87_4461;
																																			}
																																	}
																																else
																																	{	/* Ast/let.scm 1226 */
																																		return
																																			BFALSE;
																																	}
																															}
																													}
																											}
																									}
																							}
																						}
																				}
																			else
																				{	/* Ast/let.scm 1229 */
																					obj_t BgL_arg3549z00_4436;

																					BgL_arg3549z00_4436 =
																						CAR(((obj_t) BgL_expz00_39));
																					{
																						obj_t BgL_zf3zd2z21_12389;
																						obj_t BgL_argsz00_12388;
																						obj_t BgL_varz00_12387;

																						BgL_varz00_12387 =
																							BgL_arg3549z00_4436;
																						BgL_argsz00_12388 =
																							BgL_cdrzd22926zd2_4416;
																						BgL_zf3zd2z21_12389 =
																							BgL_cdrzd22926zd2_4416;
																						BgL_zf3zd2z21_4409 =
																							BgL_zf3zd2z21_12389;
																						BgL_argsz00_4408 =
																							BgL_argsz00_12388;
																						BgL_varz00_4407 = BgL_varz00_12387;
																						goto BgL_tagzd22908zd2_4410;
																					}
																				}
																		}
																	else
																		{	/* Ast/let.scm 1229 */
																			obj_t BgL_arg3551z00_4438;

																			BgL_arg3551z00_4438 =
																				CAR(((obj_t) BgL_expz00_39));
																			{
																				obj_t BgL_zf3zd2z21_12394;
																				obj_t BgL_argsz00_12393;
																				obj_t BgL_varz00_12392;

																				BgL_varz00_12392 = BgL_arg3551z00_4438;
																				BgL_argsz00_12393 =
																					BgL_cdrzd22926zd2_4416;
																				BgL_zf3zd2z21_12394 =
																					BgL_cdrzd22926zd2_4416;
																				BgL_zf3zd2z21_4409 =
																					BgL_zf3zd2z21_12394;
																				BgL_argsz00_4408 = BgL_argsz00_12393;
																				BgL_varz00_4407 = BgL_varz00_12392;
																				goto BgL_tagzd22908zd2_4410;
																			}
																		}
																}
															else
																{	/* Ast/let.scm 1229 */
																	obj_t BgL_arg3553z00_4441;

																	BgL_arg3553z00_4441 =
																		CAR(((obj_t) BgL_expz00_39));
																	{
																		obj_t BgL_zf3zd2z21_12399;
																		obj_t BgL_argsz00_12398;
																		obj_t BgL_varz00_12397;

																		BgL_varz00_12397 = BgL_arg3553z00_4441;
																		BgL_argsz00_12398 = BgL_cdrzd22926zd2_4416;
																		BgL_zf3zd2z21_12399 =
																			BgL_cdrzd22926zd2_4416;
																		BgL_zf3zd2z21_4409 = BgL_zf3zd2z21_12399;
																		BgL_argsz00_4408 = BgL_argsz00_12398;
																		BgL_varz00_4407 = BgL_varz00_12397;
																		goto BgL_tagzd22908zd2_4410;
																	}
																}
														}
													else
														{	/* Ast/let.scm 1229 */
															obj_t BgL_arg3555z00_4443;

															BgL_arg3555z00_4443 =
																CAR(((obj_t) BgL_expz00_39));
															{
																obj_t BgL_zf3zd2z21_12404;
																obj_t BgL_argsz00_12403;
																obj_t BgL_varz00_12402;

																BgL_varz00_12402 = BgL_arg3555z00_4443;
																BgL_argsz00_12403 = BgL_cdrzd22926zd2_4416;
																BgL_zf3zd2z21_12404 = BgL_cdrzd22926zd2_4416;
																BgL_zf3zd2z21_4409 = BgL_zf3zd2z21_12404;
																BgL_argsz00_4408 = BgL_argsz00_12403;
																BgL_varz00_4407 = BgL_varz00_12402;
																goto BgL_tagzd22908zd2_4410;
															}
														}
												}
											else
												{	/* Ast/let.scm 1229 */
													obj_t BgL_arg3556z00_4445;

													BgL_arg3556z00_4445 = CAR(((obj_t) BgL_expz00_39));
													{
														obj_t BgL_zf3zd2z21_12409;
														obj_t BgL_argsz00_12408;
														obj_t BgL_varz00_12407;

														BgL_varz00_12407 = BgL_arg3556z00_4445;
														BgL_argsz00_12408 = BgL_cdrzd22926zd2_4416;
														BgL_zf3zd2z21_12409 = BgL_cdrzd22926zd2_4416;
														BgL_zf3zd2z21_4409 = BgL_zf3zd2z21_12409;
														BgL_argsz00_4408 = BgL_argsz00_12408;
														BgL_varz00_4407 = BgL_varz00_12407;
														goto BgL_tagzd22908zd2_4410;
													}
												}
										}
									else
										{	/* Ast/let.scm 1229 */
											obj_t BgL_arg3558z00_4447;

											BgL_arg3558z00_4447 = CAR(((obj_t) BgL_expz00_39));
											{
												obj_t BgL_zf3zd2z21_12414;
												obj_t BgL_argsz00_12413;
												obj_t BgL_varz00_12412;

												BgL_varz00_12412 = BgL_arg3558z00_4447;
												BgL_argsz00_12413 = BgL_cdrzd22926zd2_4416;
												BgL_zf3zd2z21_12414 = BgL_cdrzd22926zd2_4416;
												BgL_zf3zd2z21_4409 = BgL_zf3zd2z21_12414;
												BgL_argsz00_4408 = BgL_argsz00_12413;
												BgL_varz00_4407 = BgL_varz00_12412;
												goto BgL_tagzd22908zd2_4410;
											}
										}
								}
						}
					else
						{	/* Ast/let.scm 1229 */
							return BFALSE;
						}
				}
			}
		}

	}



/* direct-function? */
	bool_t BGl_directzd2functionzf3z21zzast_letz00(obj_t BgL_expz00_43,
		bool_t BgL_directpz00_44)
	{
		{	/* Ast/let.scm 1247 */
			if (PAIRP(BgL_expz00_43))
				{	/* Ast/let.scm 1248 */
					if ((CAR(((obj_t) BgL_expz00_43)) == CNST_TABLE_REF(7)))
						{	/* Ast/let.scm 1248 */
							return ((bool_t) 1);
						}
					else
						{	/* Ast/let.scm 1248 */
							obj_t BgL_cdrzd23078zd2_4483;

							BgL_cdrzd23078zd2_4483 = CDR(((obj_t) BgL_expz00_43));
							if ((CAR(((obj_t) BgL_expz00_43)) == CNST_TABLE_REF(8)))
								{	/* Ast/let.scm 1248 */
									if (PAIRP(BgL_cdrzd23078zd2_4483))
										{	/* Ast/let.scm 1248 */
											obj_t BgL_carzd23081zd2_4487;
											obj_t BgL_cdrzd23082zd2_4488;

											BgL_carzd23081zd2_4487 = CAR(BgL_cdrzd23078zd2_4483);
											BgL_cdrzd23082zd2_4488 = CDR(BgL_cdrzd23078zd2_4483);
											if (PAIRP(BgL_carzd23081zd2_4487))
												{	/* Ast/let.scm 1248 */
													obj_t BgL_carzd23084zd2_4490;

													BgL_carzd23084zd2_4490 = CAR(BgL_carzd23081zd2_4487);
													if (PAIRP(BgL_carzd23084zd2_4490))
														{	/* Ast/let.scm 1248 */
															if (NULLP(CDR(BgL_carzd23081zd2_4487)))
																{	/* Ast/let.scm 1248 */
																	if (PAIRP(BgL_cdrzd23082zd2_4488))
																		{	/* Ast/let.scm 1248 */
																			if (NULLP(CDR(BgL_cdrzd23082zd2_4488)))
																				{	/* Ast/let.scm 1248 */
																					obj_t BgL_arg3582z00_4497;
																					obj_t BgL_arg3584z00_4498;

																					BgL_arg3582z00_4497 =
																						CAR(BgL_carzd23084zd2_4490);
																					BgL_arg3584z00_4498 =
																						CAR(BgL_cdrzd23082zd2_4488);
																					return
																						(BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																						(BgL_arg3582z00_4497,
																							BFALSE) == BgL_arg3584z00_4498);
																				}
																			else
																				{	/* Ast/let.scm 1248 */
																					obj_t BgL_carzd23096zd2_4499;

																					BgL_carzd23096zd2_4499 =
																						CAR(((obj_t) BgL_expz00_43));
																					if (SYMBOLP(BgL_carzd23096zd2_4499))
																						{	/* Ast/let.scm 1254 */
																							obj_t BgL_arg3598z00_6867;

																							BgL_arg3598z00_6867 =
																								BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																								(BgL_carzd23096zd2_4499,
																								BFALSE);
																							return (BgL_arg3598z00_6867 ==
																								CNST_TABLE_REF(7));
																						}
																					else
																						{	/* Ast/let.scm 1248 */
																							return ((bool_t) 0);
																						}
																				}
																		}
																	else
																		{	/* Ast/let.scm 1248 */
																			obj_t BgL_carzd23103zd2_4502;

																			BgL_carzd23103zd2_4502 =
																				CAR(((obj_t) BgL_expz00_43));
																			if (SYMBOLP(BgL_carzd23103zd2_4502))
																				{	/* Ast/let.scm 1254 */
																					obj_t BgL_arg3598z00_6869;

																					BgL_arg3598z00_6869 =
																						BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																						(BgL_carzd23103zd2_4502, BFALSE);
																					return (BgL_arg3598z00_6869 ==
																						CNST_TABLE_REF(7));
																				}
																			else
																				{	/* Ast/let.scm 1248 */
																					return ((bool_t) 0);
																				}
																		}
																}
															else
																{	/* Ast/let.scm 1248 */
																	obj_t BgL_carzd23110zd2_4504;

																	BgL_carzd23110zd2_4504 =
																		CAR(((obj_t) BgL_expz00_43));
																	if (SYMBOLP(BgL_carzd23110zd2_4504))
																		{	/* Ast/let.scm 1254 */
																			obj_t BgL_arg3598z00_6871;

																			BgL_arg3598z00_6871 =
																				BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																				(BgL_carzd23110zd2_4504, BFALSE);
																			return (BgL_arg3598z00_6871 ==
																				CNST_TABLE_REF(7));
																		}
																	else
																		{	/* Ast/let.scm 1248 */
																			return ((bool_t) 0);
																		}
																}
														}
													else
														{	/* Ast/let.scm 1248 */
															obj_t BgL_carzd23117zd2_4507;

															BgL_carzd23117zd2_4507 =
																CAR(((obj_t) BgL_expz00_43));
															if (SYMBOLP(BgL_carzd23117zd2_4507))
																{	/* Ast/let.scm 1254 */
																	obj_t BgL_arg3598z00_6873;

																	BgL_arg3598z00_6873 =
																		BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																		(BgL_carzd23117zd2_4507, BFALSE);
																	return (BgL_arg3598z00_6873 ==
																		CNST_TABLE_REF(7));
																}
															else
																{	/* Ast/let.scm 1248 */
																	return ((bool_t) 0);
																}
														}
												}
											else
												{	/* Ast/let.scm 1248 */
													obj_t BgL_carzd23124zd2_4509;

													BgL_carzd23124zd2_4509 = CAR(((obj_t) BgL_expz00_43));
													if (SYMBOLP(BgL_carzd23124zd2_4509))
														{	/* Ast/let.scm 1254 */
															obj_t BgL_arg3598z00_6875;

															BgL_arg3598z00_6875 =
																BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
																(BgL_carzd23124zd2_4509, BFALSE);
															return (BgL_arg3598z00_6875 == CNST_TABLE_REF(7));
														}
													else
														{	/* Ast/let.scm 1248 */
															return ((bool_t) 0);
														}
												}
										}
									else
										{	/* Ast/let.scm 1248 */
											obj_t BgL_carzd23131zd2_4511;

											BgL_carzd23131zd2_4511 = CAR(((obj_t) BgL_expz00_43));
											if (SYMBOLP(BgL_carzd23131zd2_4511))
												{	/* Ast/let.scm 1254 */
													obj_t BgL_arg3598z00_6877;

													BgL_arg3598z00_6877 =
														BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
														(BgL_carzd23131zd2_4511, BFALSE);
													return (BgL_arg3598z00_6877 == CNST_TABLE_REF(7));
												}
											else
												{	/* Ast/let.scm 1248 */
													return ((bool_t) 0);
												}
										}
								}
							else
								{	/* Ast/let.scm 1248 */
									obj_t BgL_carzd23138zd2_4513;

									BgL_carzd23138zd2_4513 = CAR(((obj_t) BgL_expz00_43));
									if (SYMBOLP(BgL_carzd23138zd2_4513))
										{	/* Ast/let.scm 1254 */
											obj_t BgL_arg3598z00_6879;

											BgL_arg3598z00_6879 =
												BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
												(BgL_carzd23138zd2_4513, BFALSE);
											return (BgL_arg3598z00_6879 == CNST_TABLE_REF(7));
										}
									else
										{	/* Ast/let.scm 1248 */
											return ((bool_t) 0);
										}
								}
						}
				}
			else
				{	/* Ast/let.scm 1248 */
					return ((bool_t) 0);
				}
		}

	}



/* constant? */
	bool_t BGl_constantzf3zf3zzast_letz00(obj_t BgL_valz00_47)
	{
		{	/* Ast/let.scm 1261 */
			{	/* Ast/let.scm 1262 */
				bool_t BgL__ortest_1220z00_4519;

				{	/* Ast/let.scm 1262 */
					obj_t BgL_classz00_6880;

					BgL_classz00_6880 = BGl_atomz00zzast_nodez00;
					if (BGL_OBJECTP(BgL_valz00_47))
						{	/* Ast/let.scm 1262 */
							BgL_objectz00_bglt BgL_arg1807z00_6882;

							BgL_arg1807z00_6882 = (BgL_objectz00_bglt) (BgL_valz00_47);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Ast/let.scm 1262 */
									long BgL_idxz00_6888;

									BgL_idxz00_6888 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6882);
									BgL__ortest_1220z00_4519 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_6888 + 2L)) == BgL_classz00_6880);
								}
							else
								{	/* Ast/let.scm 1262 */
									bool_t BgL_res3777z00_6913;

									{	/* Ast/let.scm 1262 */
										obj_t BgL_oclassz00_6896;

										{	/* Ast/let.scm 1262 */
											obj_t BgL_arg1815z00_6904;
											long BgL_arg1816z00_6905;

											BgL_arg1815z00_6904 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Ast/let.scm 1262 */
												long BgL_arg1817z00_6906;

												BgL_arg1817z00_6906 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6882);
												BgL_arg1816z00_6905 =
													(BgL_arg1817z00_6906 - OBJECT_TYPE);
											}
											BgL_oclassz00_6896 =
												VECTOR_REF(BgL_arg1815z00_6904, BgL_arg1816z00_6905);
										}
										{	/* Ast/let.scm 1262 */
											bool_t BgL__ortest_1115z00_6897;

											BgL__ortest_1115z00_6897 =
												(BgL_classz00_6880 == BgL_oclassz00_6896);
											if (BgL__ortest_1115z00_6897)
												{	/* Ast/let.scm 1262 */
													BgL_res3777z00_6913 = BgL__ortest_1115z00_6897;
												}
											else
												{	/* Ast/let.scm 1262 */
													long BgL_odepthz00_6898;

													{	/* Ast/let.scm 1262 */
														obj_t BgL_arg1804z00_6899;

														BgL_arg1804z00_6899 = (BgL_oclassz00_6896);
														BgL_odepthz00_6898 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_6899);
													}
													if ((2L < BgL_odepthz00_6898))
														{	/* Ast/let.scm 1262 */
															obj_t BgL_arg1802z00_6901;

															{	/* Ast/let.scm 1262 */
																obj_t BgL_arg1803z00_6902;

																BgL_arg1803z00_6902 = (BgL_oclassz00_6896);
																BgL_arg1802z00_6901 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6902,
																	2L);
															}
															BgL_res3777z00_6913 =
																(BgL_arg1802z00_6901 == BgL_classz00_6880);
														}
													else
														{	/* Ast/let.scm 1262 */
															BgL_res3777z00_6913 = ((bool_t) 0);
														}
												}
										}
									}
									BgL__ortest_1220z00_4519 = BgL_res3777z00_6913;
								}
						}
					else
						{	/* Ast/let.scm 1262 */
							BgL__ortest_1220z00_4519 = ((bool_t) 0);
						}
				}
				if (BgL__ortest_1220z00_4519)
					{	/* Ast/let.scm 1262 */
						return BgL__ortest_1220z00_4519;
					}
				else
					{	/* Ast/let.scm 1262 */
						bool_t BgL__ortest_1221z00_4520;

						{	/* Ast/let.scm 1262 */
							obj_t BgL_classz00_6914;

							BgL_classz00_6914 = BGl_kwotez00zzast_nodez00;
							if (BGL_OBJECTP(BgL_valz00_47))
								{	/* Ast/let.scm 1262 */
									BgL_objectz00_bglt BgL_arg1807z00_6916;

									BgL_arg1807z00_6916 = (BgL_objectz00_bglt) (BgL_valz00_47);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Ast/let.scm 1262 */
											long BgL_idxz00_6922;

											BgL_idxz00_6922 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6916);
											BgL__ortest_1221z00_4520 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_6922 + 2L)) == BgL_classz00_6914);
										}
									else
										{	/* Ast/let.scm 1262 */
											bool_t BgL_res3778z00_6947;

											{	/* Ast/let.scm 1262 */
												obj_t BgL_oclassz00_6930;

												{	/* Ast/let.scm 1262 */
													obj_t BgL_arg1815z00_6938;
													long BgL_arg1816z00_6939;

													BgL_arg1815z00_6938 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Ast/let.scm 1262 */
														long BgL_arg1817z00_6940;

														BgL_arg1817z00_6940 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6916);
														BgL_arg1816z00_6939 =
															(BgL_arg1817z00_6940 - OBJECT_TYPE);
													}
													BgL_oclassz00_6930 =
														VECTOR_REF(BgL_arg1815z00_6938,
														BgL_arg1816z00_6939);
												}
												{	/* Ast/let.scm 1262 */
													bool_t BgL__ortest_1115z00_6931;

													BgL__ortest_1115z00_6931 =
														(BgL_classz00_6914 == BgL_oclassz00_6930);
													if (BgL__ortest_1115z00_6931)
														{	/* Ast/let.scm 1262 */
															BgL_res3778z00_6947 = BgL__ortest_1115z00_6931;
														}
													else
														{	/* Ast/let.scm 1262 */
															long BgL_odepthz00_6932;

															{	/* Ast/let.scm 1262 */
																obj_t BgL_arg1804z00_6933;

																BgL_arg1804z00_6933 = (BgL_oclassz00_6930);
																BgL_odepthz00_6932 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_6933);
															}
															if ((2L < BgL_odepthz00_6932))
																{	/* Ast/let.scm 1262 */
																	obj_t BgL_arg1802z00_6935;

																	{	/* Ast/let.scm 1262 */
																		obj_t BgL_arg1803z00_6936;

																		BgL_arg1803z00_6936 = (BgL_oclassz00_6930);
																		BgL_arg1802z00_6935 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_6936, 2L);
																	}
																	BgL_res3778z00_6947 =
																		(BgL_arg1802z00_6935 == BgL_classz00_6914);
																}
															else
																{	/* Ast/let.scm 1262 */
																	BgL_res3778z00_6947 = ((bool_t) 0);
																}
														}
												}
											}
											BgL__ortest_1221z00_4520 = BgL_res3778z00_6947;
										}
								}
							else
								{	/* Ast/let.scm 1262 */
									BgL__ortest_1221z00_4520 = ((bool_t) 0);
								}
						}
						if (BgL__ortest_1221z00_4520)
							{	/* Ast/let.scm 1262 */
								return BgL__ortest_1221z00_4520;
							}
						else
							{	/* Ast/let.scm 1262 */
								bool_t BgL__ortest_1222z00_4521;

								BgL__ortest_1222z00_4521 = NULLP(BgL_valz00_47);
								if (BgL__ortest_1222z00_4521)
									{	/* Ast/let.scm 1262 */
										return BgL__ortest_1222z00_4521;
									}
								else
									{	/* Ast/let.scm 1262 */
										bool_t BgL__ortest_1223z00_4522;

										BgL__ortest_1223z00_4522 =
											BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_valz00_47);
										if (BgL__ortest_1223z00_4522)
											{	/* Ast/let.scm 1262 */
												return BgL__ortest_1223z00_4522;
											}
										else
											{	/* Ast/let.scm 1263 */
												bool_t BgL__ortest_1224z00_4523;

												BgL__ortest_1224z00_4523 = CNSTP(BgL_valz00_47);
												if (BgL__ortest_1224z00_4523)
													{	/* Ast/let.scm 1263 */
														return BgL__ortest_1224z00_4523;
													}
												else
													{	/* Ast/let.scm 1263 */
														bool_t BgL__ortest_1225z00_4524;

														BgL__ortest_1225z00_4524 = STRINGP(BgL_valz00_47);
														if (BgL__ortest_1225z00_4524)
															{	/* Ast/let.scm 1263 */
																return BgL__ortest_1225z00_4524;
															}
														else
															{	/* Ast/let.scm 1263 */
																bool_t BgL__ortest_1226z00_4525;

																BgL__ortest_1226z00_4525 = CHARP(BgL_valz00_47);
																if (BgL__ortest_1226z00_4525)
																	{	/* Ast/let.scm 1263 */
																		return BgL__ortest_1226z00_4525;
																	}
																else
																	{	/* Ast/let.scm 1263 */
																		bool_t BgL__ortest_1227z00_4526;

																		BgL__ortest_1227z00_4526 =
																			BOOLEANP(BgL_valz00_47);
																		if (BgL__ortest_1227z00_4526)
																			{	/* Ast/let.scm 1263 */
																				return BgL__ortest_1227z00_4526;
																			}
																		else
																			{	/* Ast/let.scm 1263 */
																				BGL_TAIL return
																					BGl_numberzf3zf3zz__r4_numbers_6_5z00
																					(BgL_valz00_47);
																			}
																	}
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* loop~0 */
	bool_t BGl_loopze70ze7zzast_letz00(obj_t BgL_valz00_4529)
	{
		{	/* Ast/let.scm 1315 */
		BGl_loopze70ze7zzast_letz00:
			if (BGl_constantzf3zf3zzast_letz00(BgL_valz00_4529))
				{	/* Ast/let.scm 1315 */
					return ((bool_t) 0);
				}
			else
				{	/* Ast/let.scm 1315 */
					if (PAIRP(BgL_valz00_4529))
						{	/* Ast/let.scm 1315 */
							if ((CAR(((obj_t) BgL_valz00_4529)) == CNST_TABLE_REF(11)))
								{	/* Ast/let.scm 1315 */
									return ((bool_t) 0);
								}
							else
								{	/* Ast/let.scm 1315 */
									obj_t BgL_cdrzd23180zd2_4548;

									BgL_cdrzd23180zd2_4548 = CDR(((obj_t) BgL_valz00_4529));
									if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR(
													((obj_t) BgL_valz00_4529)), CNST_TABLE_REF(36))))
										{	/* Ast/let.scm 1315 */
											if (PAIRP(BgL_cdrzd23180zd2_4548))
												{	/* Ast/let.scm 1315 */
													obj_t BgL_cdrzd23184zd2_4552;

													BgL_cdrzd23184zd2_4552 = CDR(BgL_cdrzd23180zd2_4548);
													if (PAIRP(BgL_cdrzd23184zd2_4552))
														{	/* Ast/let.scm 1315 */
															if (NULLP(CDR(BgL_cdrzd23184zd2_4552)))
																{	/* Ast/let.scm 1315 */
																	obj_t BgL_arg3611z00_4556;
																	obj_t BgL_arg3612z00_4557;

																	BgL_arg3611z00_4556 =
																		CAR(BgL_cdrzd23180zd2_4548);
																	BgL_arg3612z00_4557 =
																		CAR(BgL_cdrzd23184zd2_4552);
																	{	/* Ast/let.scm 1319 */
																		bool_t BgL__ortest_1228z00_6958;

																		BgL__ortest_1228z00_6958 =
																			BGl_loopze70ze7zzast_letz00
																			(BgL_arg3611z00_4556);
																		if (BgL__ortest_1228z00_6958)
																			{	/* Ast/let.scm 1319 */
																				return BgL__ortest_1228z00_6958;
																			}
																		else
																			{
																				obj_t BgL_valz00_12587;

																				BgL_valz00_12587 = BgL_arg3612z00_4557;
																				BgL_valz00_4529 = BgL_valz00_12587;
																				goto BGl_loopze70ze7zzast_letz00;
																			}
																	}
																}
															else
																{	/* Ast/let.scm 1315 */
																	if (
																		(CAR(
																				((obj_t) BgL_valz00_4529)) ==
																			CNST_TABLE_REF(37)))
																		{	/* Ast/let.scm 1315 */
																			obj_t BgL_cdrzd23212zd2_4561;

																			BgL_cdrzd23212zd2_4561 =
																				CDR(((obj_t) BgL_cdrzd23180zd2_4548));
																			{	/* Ast/let.scm 1315 */
																				obj_t BgL_cdrzd23219zd2_4562;

																				BgL_cdrzd23219zd2_4562 =
																					CDR(((obj_t) BgL_cdrzd23212zd2_4561));
																				if (PAIRP(BgL_cdrzd23219zd2_4562))
																					{	/* Ast/let.scm 1315 */
																						if (NULLP(CDR
																								(BgL_cdrzd23219zd2_4562)))
																							{	/* Ast/let.scm 1315 */
																								obj_t BgL_arg3618z00_4566;
																								obj_t BgL_arg3620z00_4567;
																								obj_t BgL_arg3621z00_4568;

																								BgL_arg3618z00_4566 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd23180zd2_4548));
																								BgL_arg3620z00_4567 =
																									CAR(((obj_t)
																										BgL_cdrzd23212zd2_4561));
																								BgL_arg3621z00_4568 =
																									CAR(BgL_cdrzd23219zd2_4562);
																								{	/* Ast/let.scm 1321 */
																									bool_t
																										BgL__ortest_1229z00_6967;
																									BgL__ortest_1229z00_6967 =
																										BGl_loopze70ze7zzast_letz00
																										(BgL_arg3618z00_4566);
																									if (BgL__ortest_1229z00_6967)
																										{	/* Ast/let.scm 1321 */
																											return
																												BgL__ortest_1229z00_6967;
																										}
																									else
																										{	/* Ast/let.scm 1321 */
																											bool_t
																												BgL__ortest_1230z00_6968;
																											BgL__ortest_1230z00_6968 =
																												BGl_loopze70ze7zzast_letz00
																												(BgL_arg3620z00_4567);
																											if (BgL__ortest_1230z00_6968)
																												{	/* Ast/let.scm 1321 */
																													return
																														BgL__ortest_1230z00_6968;
																												}
																											else
																												{
																													obj_t
																														BgL_valz00_12611;
																													BgL_valz00_12611 =
																														BgL_arg3621z00_4568;
																													BgL_valz00_4529 =
																														BgL_valz00_12611;
																													goto
																														BGl_loopze70ze7zzast_letz00;
																												}
																										}
																								}
																							}
																						else
																							{	/* Ast/let.scm 1315 */
																								return ((bool_t) 1);
																							}
																					}
																				else
																					{	/* Ast/let.scm 1315 */
																						return ((bool_t) 1);
																					}
																			}
																		}
																	else
																		{	/* Ast/let.scm 1315 */
																			return ((bool_t) 1);
																		}
																}
														}
													else
														{	/* Ast/let.scm 1315 */
															if (
																(CAR(
																		((obj_t) BgL_valz00_4529)) ==
																	CNST_TABLE_REF(38)))
																{	/* Ast/let.scm 1315 */
																	if (NULLP(CDR(
																				((obj_t) BgL_cdrzd23180zd2_4548))))
																		{	/* Ast/let.scm 1315 */
																			obj_t BgL_arg3630z00_4577;

																			BgL_arg3630z00_4577 =
																				CAR(((obj_t) BgL_cdrzd23180zd2_4548));
																			{
																				obj_t BgL_valz00_12623;

																				BgL_valz00_12623 = BgL_arg3630z00_4577;
																				BgL_valz00_4529 = BgL_valz00_12623;
																				goto BGl_loopze70ze7zzast_letz00;
																			}
																		}
																	else
																		{	/* Ast/let.scm 1315 */
																			return ((bool_t) 1);
																		}
																}
															else
																{	/* Ast/let.scm 1315 */
																	return ((bool_t) 1);
																}
														}
												}
											else
												{	/* Ast/let.scm 1315 */
													return ((bool_t) 1);
												}
										}
									else
										{	/* Ast/let.scm 1315 */
											if (
												(CAR(((obj_t) BgL_valz00_4529)) == CNST_TABLE_REF(38)))
												{	/* Ast/let.scm 1315 */
													if (PAIRP(BgL_cdrzd23180zd2_4548))
														{	/* Ast/let.scm 1315 */
															if (NULLP(CDR(BgL_cdrzd23180zd2_4548)))
																{
																	obj_t BgL_valz00_12634;

																	BgL_valz00_12634 =
																		CAR(BgL_cdrzd23180zd2_4548);
																	BgL_valz00_4529 = BgL_valz00_12634;
																	goto BGl_loopze70ze7zzast_letz00;
																}
															else
																{	/* Ast/let.scm 1315 */
																	return ((bool_t) 1);
																}
														}
													else
														{	/* Ast/let.scm 1315 */
															return ((bool_t) 1);
														}
												}
											else
												{	/* Ast/let.scm 1315 */
													obj_t BgL_cdrzd23294zd2_4588;

													BgL_cdrzd23294zd2_4588 =
														CDR(((obj_t) BgL_valz00_4529));
													if (
														(CAR(
																((obj_t) BgL_valz00_4529)) ==
															CNST_TABLE_REF(37)))
														{	/* Ast/let.scm 1315 */
															if (PAIRP(BgL_cdrzd23294zd2_4588))
																{	/* Ast/let.scm 1315 */
																	obj_t BgL_cdrzd23299zd2_4592;

																	BgL_cdrzd23299zd2_4592 =
																		CDR(BgL_cdrzd23294zd2_4588);
																	if (PAIRP(BgL_cdrzd23299zd2_4592))
																		{	/* Ast/let.scm 1315 */
																			obj_t BgL_cdrzd23304zd2_4594;

																			BgL_cdrzd23304zd2_4594 =
																				CDR(BgL_cdrzd23299zd2_4592);
																			if (PAIRP(BgL_cdrzd23304zd2_4594))
																				{	/* Ast/let.scm 1315 */
																					if (NULLP(CDR
																							(BgL_cdrzd23304zd2_4594)))
																						{	/* Ast/let.scm 1315 */
																							obj_t BgL_arg3648z00_4598;
																							obj_t BgL_arg3650z00_4599;
																							obj_t BgL_arg3651z00_4600;

																							BgL_arg3648z00_4598 =
																								CAR(BgL_cdrzd23294zd2_4588);
																							BgL_arg3650z00_4599 =
																								CAR(BgL_cdrzd23299zd2_4592);
																							BgL_arg3651z00_4600 =
																								CAR(BgL_cdrzd23304zd2_4594);
																							{	/* Ast/let.scm 1321 */
																								bool_t BgL__ortest_1229z00_6985;

																								BgL__ortest_1229z00_6985 =
																									BGl_loopze70ze7zzast_letz00
																									(BgL_arg3648z00_4598);
																								if (BgL__ortest_1229z00_6985)
																									{	/* Ast/let.scm 1321 */
																										return
																											BgL__ortest_1229z00_6985;
																									}
																								else
																									{	/* Ast/let.scm 1321 */
																										bool_t
																											BgL__ortest_1230z00_6986;
																										BgL__ortest_1230z00_6986 =
																											BGl_loopze70ze7zzast_letz00
																											(BgL_arg3650z00_4599);
																										if (BgL__ortest_1230z00_6986)
																											{	/* Ast/let.scm 1321 */
																												return
																													BgL__ortest_1230z00_6986;
																											}
																										else
																											{
																												obj_t BgL_valz00_12661;

																												BgL_valz00_12661 =
																													BgL_arg3651z00_4600;
																												BgL_valz00_4529 =
																													BgL_valz00_12661;
																												goto
																													BGl_loopze70ze7zzast_letz00;
																											}
																									}
																							}
																						}
																					else
																						{	/* Ast/let.scm 1315 */
																							return ((bool_t) 1);
																						}
																				}
																			else
																				{	/* Ast/let.scm 1315 */
																					return ((bool_t) 1);
																				}
																		}
																	else
																		{	/* Ast/let.scm 1315 */
																			return ((bool_t) 1);
																		}
																}
															else
																{	/* Ast/let.scm 1315 */
																	return ((bool_t) 1);
																}
														}
													else
														{	/* Ast/let.scm 1315 */
															return ((bool_t) 1);
														}
												}
										}
								}
						}
					else
						{	/* Ast/let.scm 1315 */
							return ((bool_t) 1);
						}
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_letz00(void)
	{
		{	/* Ast/let.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_letz00(void)
	{
		{	/* Ast/let.scm 14 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_letz00(void)
	{
		{	/* Ast/let.scm 14 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_letz00(void)
	{
		{	/* Ast/let.scm 14 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string3822z00zzast_letz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string3822z00zzast_letz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string3822z00zzast_letz00));
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string3822z00zzast_letz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string3822z00zzast_letz00));
			BGl_modulezd2initializa7ationz75zztools_prognz00(301998274L,
				BSTRING_TO_STRING(BGl_string3822z00zzast_letz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string3822z00zzast_letz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string3822z00zzast_letz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string3822z00zzast_letz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string3822z00zzast_letz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string3822z00zzast_letz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string3822z00zzast_letz00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string3822z00zzast_letz00));
			BGl_modulezd2initializa7ationz75zzast_substitutez00(280730341L,
				BSTRING_TO_STRING(BGl_string3822z00zzast_letz00));
			BGl_modulezd2initializa7ationz75zzast_occurz00(282085879L,
				BSTRING_TO_STRING(BGl_string3822z00zzast_letz00));
			BGl_modulezd2initializa7ationz75zzast_removez00(383247627L,
				BSTRING_TO_STRING(BGl_string3822z00zzast_letz00));
			BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string3822z00zzast_letz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string3822z00zzast_letz00));
			return
				BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string3822z00zzast_letz00));
		}

	}

#ifdef __cplusplus
}
#endif
