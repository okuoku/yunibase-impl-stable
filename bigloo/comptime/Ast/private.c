/*===========================================================================*/
/*   (Ast/private.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/private.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_AST_PRIVATE_TYPE_DEFINITIONS
#define BGL_AST_PRIVATE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_privatez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
	}                 *BgL_privatez00_bglt;

	typedef struct BgL_getfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_getfieldz00_bglt;

	typedef struct BgL_setfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_setfieldz00_bglt;

	typedef struct BgL_newz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_argszd2typezd2;
	}             *BgL_newz00_bglt;

	typedef struct BgL_vallocz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                *BgL_vallocz00_bglt;

	typedef struct BgL_vrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}              *BgL_vrefz00_bglt;

	typedef struct BgL_vsetz12z12_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}                 *BgL_vsetz12z12_bglt;

	typedef struct BgL_vlengthz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_ftypez00;
	}                 *BgL_vlengthz00_bglt;

	typedef struct BgL_instanceofz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_classz00;
	}                    *BgL_instanceofz00_bglt;

	typedef struct BgL_castzd2nullzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
	}                     *BgL_castzd2nullzd2_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_feffectz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_readz00;
		obj_t BgL_writez00;
	}                 *BgL_feffectz00_bglt;


#endif													// BGL_AST_PRIVATE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62makezd2privatezd2sexpz62zzast_privatez00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzast_privatez00 = BUNSPEC;
	extern obj_t BGl_newz00zzast_nodez00;
	static obj_t BGl_z62castzd2sexpzd2typez62zzast_privatez00(obj_t, obj_t);
	extern obj_t BGl_vlengthz00zzast_nodez00;
	BGL_EXPORTED_DECL obj_t BGl_privatezd2stampzd2zzast_privatez00(void);
	BGL_IMPORT bool_t bigloo_mangledp(obj_t);
	static obj_t BGl_toplevelzd2initzd2zzast_privatez00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	BGL_IMPORT obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_genericzd2initzd2zzast_privatez00(void);
	extern obj_t BGl_getfieldz00zzast_nodez00;
	BGL_EXPORTED_DECL bool_t BGl_privatezd2sexpzf3z21zzast_privatez00(obj_t);
	static obj_t BGl_objectzd2initzd2zzast_privatez00(void);
	extern obj_t BGl_za2boolza2z00zztype_cachez00;
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_EXPORTED_DECL obj_t BGl_privatezd2nodezd2zzast_privatez00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_castzd2sexpzd2typez00zzast_privatez00(obj_t);
	static obj_t BGl_za2privatezd2stampza2zd2zzast_privatez00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzast_privatez00(void);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzast_privatez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t bigloo_demangle(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_expandzd2metazd2zzast_privatez00(obj_t, obj_t);
	extern obj_t BGl_vrefz00zzast_nodez00;
	BGL_EXPORTED_DECL obj_t BGl_makezd2privatezd2sexpz00zzast_privatez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_castzd2sexpzf3z21zzast_privatez00(obj_t);
	extern obj_t BGl_feffectz00zzast_varz00;
	static obj_t BGl_z62privatezd2nodezb0zzast_privatez00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzast_privatez00(void);
	BGL_IMPORT bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_privatez00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzast_privatez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzast_privatez00(void);
	static obj_t BGl_z62castzd2sexpzf3z43zzast_privatez00(obj_t, obj_t);
	extern obj_t BGl_vallocz00zzast_nodez00;
	static obj_t BGl_z62privatezd2stampzb0zzast_privatez00(obj_t);
	extern obj_t BGl_instanceofz00zzast_nodez00;
	extern BgL_nodez00_bglt BGl_sexpzd2ze3nodez31zzast_sexpz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2unspecza2z00zztype_cachez00;
	static obj_t BGl_z62privatezd2sexpzf3z43zzast_privatez00(obj_t, obj_t);
	extern obj_t BGl_setfieldz00zzast_nodez00;
	extern obj_t BGl_castzd2nullzd2zzast_nodez00;
	extern obj_t BGl_sexpza2zd2ze3nodez93zzast_sexpz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62expandzd2metazb0zzast_privatez00(obj_t, obj_t, obj_t);
	extern BgL_typez00_bglt BGl_usezd2typez12zc0zztype_envz00(obj_t, obj_t);
	extern obj_t BGl_vsetz12z12zzast_nodez00;
	static obj_t __cnst[20];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_privatezd2sexpzf3zd2envzf3zzast_privatez00,
		BgL_bgl_za762privateza7d2sex2673z00,
		BGl_z62privatezd2sexpzf3z43zzast_privatez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_expandzd2metazd2envz00zzast_privatez00,
		BgL_bgl_za762expandza7d2meta2674z00,
		BGl_z62expandzd2metazb0zzast_privatez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_castzd2sexpzd2typezd2envzd2zzast_privatez00,
		BgL_bgl_za762castza7d2sexpza7d2675za7,
		BGl_z62castzd2sexpzd2typez62zzast_privatez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2662z00zzast_privatez00,
		BgL_bgl_string2662za700za7za7a2676za7, "cast-sexp-type", 14);
	      DEFINE_STRING(BGl_string2663z00zzast_privatez00,
		BgL_bgl_string2663za700za7za7a2677za7, "Illegal cast sexp", 17);
	      DEFINE_STRING(BGl_string2664z00zzast_privatez00,
		BgL_bgl_string2664za700za7za7a2678za7, "", 0);
	      DEFINE_STRING(BGl_string2665z00zzast_privatez00,
		BgL_bgl_string2665za700za7za7a2679za7, "private-node", 12);
	      DEFINE_STRING(BGl_string2666z00zzast_privatez00,
		BgL_bgl_string2666za700za7za7a2680za7, "Illegal private kind", 20);
	      DEFINE_STRING(BGl_string2667z00zzast_privatez00,
		BgL_bgl_string2667za700za7za7a2681za7, "meta", 4);
	      DEFINE_STRING(BGl_string2668z00zzast_privatez00,
		BgL_bgl_string2668za700za7za7a2682za7, "bad syntax", 10);
	      DEFINE_STRING(BGl_string2669z00zzast_privatez00,
		BgL_bgl_string2669za700za7za7a2683za7, "ast_private", 11);
	      DEFINE_STRING(BGl_string2670z00zzast_privatez00,
		BgL_bgl_string2670za700za7za7a2684za7,
		"_ unsafe isa instanceof vref meta vset! valloc vlength cast-null quote new setfield getfield vset-ur! vref-ur value - cast ___bgl_private_stamp_mark ",
		149);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_castzd2sexpzf3zd2envzf3zzast_privatez00,
		BgL_bgl_za762castza7d2sexpza7f2685za7,
		BGl_z62castzd2sexpzf3z43zzast_privatez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_privatezd2stampzd2envz00zzast_privatez00,
		BgL_bgl_za762privateza7d2sta2686z00,
		BGl_z62privatezd2stampzb0zzast_privatez00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_privatezd2nodezd2envz00zzast_privatez00,
		BgL_bgl_za762privateza7d2nod2687z00,
		BGl_z62privatezd2nodezb0zzast_privatez00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2privatezd2sexpzd2envzd2zzast_privatez00,
		BgL_bgl_za762makeza7d2privat2688z00, va_generic_entry,
		BGl_z62makezd2privatezd2sexpz62zzast_privatez00, BUNSPEC, -3);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzast_privatez00));
		     ADD_ROOT((void *) (&BGl_za2privatezd2stampza2zd2zzast_privatez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzast_privatez00(long
		BgL_checksumz00_3704, char *BgL_fromz00_3705)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_privatez00))
				{
					BGl_requirezd2initializa7ationz75zzast_privatez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_privatez00();
					BGl_libraryzd2moduleszd2initz00zzast_privatez00();
					BGl_cnstzd2initzd2zzast_privatez00();
					BGl_importedzd2moduleszd2initz00zzast_privatez00();
					return BGl_toplevelzd2initzd2zzast_privatez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_privatez00(void)
	{
		{	/* Ast/private.scm 15 */
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "ast_private");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "ast_private");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_private");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "ast_private");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "ast_private");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_private");
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(0L, "ast_private");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "ast_private");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "ast_private");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_privatez00(void)
	{
		{	/* Ast/private.scm 15 */
			{	/* Ast/private.scm 15 */
				obj_t BgL_cportz00_3693;

				{	/* Ast/private.scm 15 */
					obj_t BgL_stringz00_3700;

					BgL_stringz00_3700 = BGl_string2670z00zzast_privatez00;
					{	/* Ast/private.scm 15 */
						obj_t BgL_startz00_3701;

						BgL_startz00_3701 = BINT(0L);
						{	/* Ast/private.scm 15 */
							obj_t BgL_endz00_3702;

							BgL_endz00_3702 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3700)));
							{	/* Ast/private.scm 15 */

								BgL_cportz00_3693 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3700, BgL_startz00_3701, BgL_endz00_3702);
				}}}}
				{
					long BgL_iz00_3694;

					BgL_iz00_3694 = 19L;
				BgL_loopz00_3695:
					if ((BgL_iz00_3694 == -1L))
						{	/* Ast/private.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Ast/private.scm 15 */
							{	/* Ast/private.scm 15 */
								obj_t BgL_arg2672z00_3696;

								{	/* Ast/private.scm 15 */

									{	/* Ast/private.scm 15 */
										obj_t BgL_locationz00_3698;

										BgL_locationz00_3698 = BBOOL(((bool_t) 0));
										{	/* Ast/private.scm 15 */

											BgL_arg2672z00_3696 =
												BGl_readz00zz__readerz00(BgL_cportz00_3693,
												BgL_locationz00_3698);
										}
									}
								}
								{	/* Ast/private.scm 15 */
									int BgL_tmpz00_3732;

									BgL_tmpz00_3732 = (int) (BgL_iz00_3694);
									CNST_TABLE_SET(BgL_tmpz00_3732, BgL_arg2672z00_3696);
							}}
							{	/* Ast/private.scm 15 */
								int BgL_auxz00_3699;

								BgL_auxz00_3699 = (int) ((BgL_iz00_3694 - 1L));
								{
									long BgL_iz00_3737;

									BgL_iz00_3737 = (long) (BgL_auxz00_3699);
									BgL_iz00_3694 = BgL_iz00_3737;
									goto BgL_loopz00_3695;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_privatez00(void)
	{
		{	/* Ast/private.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzast_privatez00(void)
	{
		{	/* Ast/private.scm 15 */
			return (BGl_za2privatezd2stampza2zd2zzast_privatez00 =
				CNST_TABLE_REF(0), BUNSPEC);
		}

	}



/* private-stamp */
	BGL_EXPORTED_DEF obj_t BGl_privatezd2stampzd2zzast_privatez00(void)
	{
		{	/* Ast/private.scm 43 */
			return CNST_TABLE_REF(0);
		}

	}



/* &private-stamp */
	obj_t BGl_z62privatezd2stampzb0zzast_privatez00(obj_t BgL_envz00_3674)
	{
		{	/* Ast/private.scm 43 */
			return BGl_privatezd2stampzd2zzast_privatez00();
		}

	}



/* private-sexp? */
	BGL_EXPORTED_DEF bool_t BGl_privatezd2sexpzf3z21zzast_privatez00(obj_t
		BgL_sexpz00_3)
	{
		{	/* Ast/private.scm 49 */
			return (CAR(BgL_sexpz00_3) == CNST_TABLE_REF(0));
		}

	}



/* &private-sexp? */
	obj_t BGl_z62privatezd2sexpzf3z43zzast_privatez00(obj_t BgL_envz00_3675,
		obj_t BgL_sexpz00_3676)
	{
		{	/* Ast/private.scm 49 */
			return BBOOL(BGl_privatezd2sexpzf3z21zzast_privatez00(BgL_sexpz00_3676));
		}

	}



/* cast-sexp? */
	BGL_EXPORTED_DEF bool_t BGl_castzd2sexpzf3z21zzast_privatez00(obj_t
		BgL_sexpz00_4)
	{
		{	/* Ast/private.scm 55 */
			if ((CAR(BgL_sexpz00_4) == CNST_TABLE_REF(0)))
				{	/* Ast/private.scm 57 */
					obj_t BgL_cdrzd2105zd2_1370;

					BgL_cdrzd2105zd2_1370 = CDR(BgL_sexpz00_4);
					if (PAIRP(BgL_cdrzd2105zd2_1370))
						{	/* Ast/private.scm 57 */
							obj_t BgL_cdrzd2107zd2_1372;

							BgL_cdrzd2107zd2_1372 = CDR(BgL_cdrzd2105zd2_1370);
							if ((CAR(BgL_cdrzd2105zd2_1370) == CNST_TABLE_REF(1)))
								{	/* Ast/private.scm 57 */
									if (PAIRP(BgL_cdrzd2107zd2_1372))
										{	/* Ast/private.scm 57 */
											obj_t BgL_cdrzd2109zd2_1376;

											BgL_cdrzd2109zd2_1376 = CDR(BgL_cdrzd2107zd2_1372);
											if (PAIRP(BgL_cdrzd2109zd2_1376))
												{	/* Ast/private.scm 57 */
													return NULLP(CDR(BgL_cdrzd2109zd2_1376));
												}
											else
												{	/* Ast/private.scm 57 */
													return ((bool_t) 0);
												}
										}
									else
										{	/* Ast/private.scm 57 */
											return ((bool_t) 0);
										}
								}
							else
								{	/* Ast/private.scm 57 */
									return ((bool_t) 0);
								}
						}
					else
						{	/* Ast/private.scm 57 */
							return ((bool_t) 0);
						}
				}
			else
				{	/* Ast/private.scm 56 */
					return ((bool_t) 0);
				}
		}

	}



/* &cast-sexp? */
	obj_t BGl_z62castzd2sexpzf3z43zzast_privatez00(obj_t BgL_envz00_3677,
		obj_t BgL_sexpz00_3678)
	{
		{	/* Ast/private.scm 55 */
			return BBOOL(BGl_castzd2sexpzf3z21zzast_privatez00(BgL_sexpz00_3678));
		}

	}



/* cast-sexp-type */
	BGL_EXPORTED_DEF obj_t BGl_castzd2sexpzd2typez00zzast_privatez00(obj_t
		BgL_sexpz00_5)
	{
		{	/* Ast/private.scm 64 */
			{

				{	/* Ast/private.scm 65 */
					obj_t BgL_cdrzd2118zd2_1387;

					BgL_cdrzd2118zd2_1387 = CDR(BgL_sexpz00_5);
					if (PAIRP(BgL_cdrzd2118zd2_1387))
						{	/* Ast/private.scm 65 */
							obj_t BgL_cdrzd2121zd2_1389;

							BgL_cdrzd2121zd2_1389 = CDR(BgL_cdrzd2118zd2_1387);
							if ((CAR(BgL_cdrzd2118zd2_1387) == CNST_TABLE_REF(1)))
								{	/* Ast/private.scm 65 */
									if (PAIRP(BgL_cdrzd2121zd2_1389))
										{	/* Ast/private.scm 65 */
											obj_t BgL_cdrzd2124zd2_1393;

											BgL_cdrzd2124zd2_1393 = CDR(BgL_cdrzd2121zd2_1389);
											if (PAIRP(BgL_cdrzd2124zd2_1393))
												{	/* Ast/private.scm 65 */
													if (NULLP(CDR(BgL_cdrzd2124zd2_1393)))
														{	/* Ast/private.scm 65 */
															return CAR(BgL_cdrzd2121zd2_1389);
														}
													else
														{	/* Ast/private.scm 65 */
														BgL_tagzd2113zd2_1384:
															return
																BGl_errorz00zz__errorz00
																(BGl_string2662z00zzast_privatez00,
																BGl_string2663z00zzast_privatez00,
																BgL_sexpz00_5);
														}
												}
											else
												{	/* Ast/private.scm 65 */
													goto BgL_tagzd2113zd2_1384;
												}
										}
									else
										{	/* Ast/private.scm 65 */
											goto BgL_tagzd2113zd2_1384;
										}
								}
							else
								{	/* Ast/private.scm 65 */
									goto BgL_tagzd2113zd2_1384;
								}
						}
					else
						{	/* Ast/private.scm 65 */
							goto BgL_tagzd2113zd2_1384;
						}
				}
			}
		}

	}



/* &cast-sexp-type */
	obj_t BGl_z62castzd2sexpzd2typez62zzast_privatez00(obj_t BgL_envz00_3679,
		obj_t BgL_sexpz00_3680)
	{
		{	/* Ast/private.scm 64 */
			return BGl_castzd2sexpzd2typez00zzast_privatez00(BgL_sexpz00_3680);
		}

	}



/* private-node */
	BGL_EXPORTED_DEF obj_t BGl_privatezd2nodezd2zzast_privatez00(obj_t
		BgL_sexpz00_6, obj_t BgL_stackz00_7, obj_t BgL_locz00_8,
		obj_t BgL_sitez00_9)
	{
		{	/* Ast/private.scm 73 */
			{
				obj_t BgL_typez00_1456;
				obj_t BgL_kwdsz00_1457;
				obj_t BgL_bodyz00_1458;
				obj_t BgL_typez00_1453;
				obj_t BgL_expz00_1454;
				obj_t BgL_vtypez00_1445;
				obj_t BgL_ftypez00_1446;
				obj_t BgL_otypez00_1447;
				obj_t BgL_czd2heapzd2fmtz00_1448;
				obj_t BgL_czd2stackzd2fmtz00_1449;
				obj_t BgL_stackzf3zf3_1450;
				obj_t BgL_restz00_1451;
				obj_t BgL_vtypez00_1439;
				obj_t BgL_ftypez00_1440;
				obj_t BgL_otypez00_1441;
				obj_t BgL_czd2fmtzd2_1442;
				obj_t BgL_restz00_1443;
				obj_t BgL_vtypez00_1433;
				obj_t BgL_ftypez00_1434;
				obj_t BgL_otypez00_1435;
				obj_t BgL_czd2fmtzd2_1436;
				obj_t BgL_restz00_1437;
				obj_t BgL_vtypez00_1427;
				obj_t BgL_ftypez00_1428;
				obj_t BgL_otypez00_1429;
				obj_t BgL_czd2fmtzd2_1430;
				obj_t BgL_expz00_1431;
				obj_t BgL_typez00_1424;
				obj_t BgL_expz00_1425;
				obj_t BgL_typez00_1422;
				obj_t BgL_typez00_1419;
				obj_t BgL_expz00_1420;
				obj_t BgL_typez00_1415;
				obj_t BgL_argszd2typezd2_1416;
				obj_t BgL_restz00_1417;
				obj_t BgL_typez00_1413;
				obj_t BgL_ftypez00_1407;
				obj_t BgL_otypez00_1408;
				obj_t BgL_fieldzd2namezd2_1409;
				obj_t BgL_czd2fmtzd2_1410;
				obj_t BgL_restz00_1411;
				obj_t BgL_ftypez00_1401;
				obj_t BgL_otypez00_1402;
				obj_t BgL_fieldzd2namezd2_1403;
				obj_t BgL_czd2fmtzd2_1404;
				obj_t BgL_objz00_1405;

				{	/* Ast/private.scm 78 */
					obj_t BgL_cdrzd2164zd2_1463;

					BgL_cdrzd2164zd2_1463 = CDR(BgL_sexpz00_6);
					if (PAIRP(BgL_cdrzd2164zd2_1463))
						{	/* Ast/private.scm 78 */
							obj_t BgL_cdrzd2171zd2_1465;

							BgL_cdrzd2171zd2_1465 = CDR(BgL_cdrzd2164zd2_1463);
							if ((CAR(BgL_cdrzd2164zd2_1463) == CNST_TABLE_REF(6)))
								{	/* Ast/private.scm 78 */
									if (PAIRP(BgL_cdrzd2171zd2_1465))
										{	/* Ast/private.scm 78 */
											obj_t BgL_cdrzd2178zd2_1469;

											BgL_cdrzd2178zd2_1469 = CDR(BgL_cdrzd2171zd2_1465);
											if (PAIRP(BgL_cdrzd2178zd2_1469))
												{	/* Ast/private.scm 78 */
													obj_t BgL_cdrzd2185zd2_1471;

													BgL_cdrzd2185zd2_1471 = CDR(BgL_cdrzd2178zd2_1469);
													if (PAIRP(BgL_cdrzd2185zd2_1471))
														{	/* Ast/private.scm 78 */
															obj_t BgL_cdrzd2191zd2_1473;

															BgL_cdrzd2191zd2_1473 =
																CDR(BgL_cdrzd2185zd2_1471);
															if (PAIRP(BgL_cdrzd2191zd2_1473))
																{	/* Ast/private.scm 78 */
																	obj_t BgL_cdrzd2196zd2_1475;

																	BgL_cdrzd2196zd2_1475 =
																		CDR(BgL_cdrzd2191zd2_1473);
																	if (PAIRP(BgL_cdrzd2196zd2_1475))
																		{	/* Ast/private.scm 78 */
																			if (NULLP(CDR(BgL_cdrzd2196zd2_1475)))
																				{	/* Ast/private.scm 78 */
																					obj_t BgL_arg1342z00_1479;
																					obj_t BgL_arg1343z00_1480;
																					obj_t BgL_arg1346z00_1481;
																					obj_t BgL_arg1348z00_1482;
																					obj_t BgL_arg1349z00_1483;

																					BgL_arg1342z00_1479 =
																						CAR(BgL_cdrzd2171zd2_1465);
																					BgL_arg1343z00_1480 =
																						CAR(BgL_cdrzd2178zd2_1469);
																					BgL_arg1346z00_1481 =
																						CAR(BgL_cdrzd2185zd2_1471);
																					BgL_arg1348z00_1482 =
																						CAR(BgL_cdrzd2191zd2_1473);
																					BgL_arg1349z00_1483 =
																						CAR(BgL_cdrzd2196zd2_1475);
																					{
																						BgL_getfieldz00_bglt
																							BgL_auxz00_3818;
																						BgL_ftypez00_1401 =
																							BgL_arg1342z00_1479;
																						BgL_otypez00_1402 =
																							BgL_arg1343z00_1480;
																						BgL_fieldzd2namezd2_1403 =
																							BgL_arg1346z00_1481;
																						BgL_czd2fmtzd2_1404 =
																							BgL_arg1348z00_1482;
																						BgL_objz00_1405 =
																							BgL_arg1349z00_1483;
																						{	/* Ast/private.scm 80 */
																							obj_t BgL_tidz00_2792;
																							BgL_typez00_bglt
																								BgL_ftypez00_2793;
																							BgL_typez00_bglt
																								BgL_otypez00_2794;
																							{	/* Ast/private.scm 82 */
																								obj_t BgL_arg2490z00_2804;

																								{	/* Ast/private.scm 82 */
																									obj_t BgL_arg2495z00_2808;

																									if (bigloo_mangledp
																										(BgL_fieldzd2namezd2_1403))
																										{	/* Ast/private.scm 75 */
																											BgL_arg2495z00_2808 =
																												bigloo_demangle
																												(BgL_fieldzd2namezd2_1403);
																										}
																									else
																										{	/* Ast/private.scm 75 */
																											BgL_arg2495z00_2808 =
																												BgL_fieldzd2namezd2_1403;
																										}
																									BgL_arg2490z00_2804 =
																										bstring_to_symbol(
																										((obj_t)
																											BgL_arg2495z00_2808));
																								}
																								{	/* Ast/private.scm 80 */
																									obj_t BgL_list2491z00_2805;

																									{	/* Ast/private.scm 80 */
																										obj_t BgL_arg2492z00_2806;

																										{	/* Ast/private.scm 80 */
																											obj_t BgL_arg2493z00_2807;

																											BgL_arg2493z00_2807 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg2490z00_2804,
																												BNIL);
																											BgL_arg2492z00_2806 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(2),
																												BgL_arg2493z00_2807);
																										}
																										BgL_list2491z00_2805 =
																											MAKE_YOUNG_PAIR
																											(BgL_otypez00_1402,
																											BgL_arg2492z00_2806);
																									}
																									BgL_tidz00_2792 =
																										BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																										(BgL_list2491z00_2805);
																								}
																							}
																							BgL_ftypez00_2793 =
																								BGl_usezd2typez12zc0zztype_envz00
																								(BgL_ftypez00_1401,
																								BgL_locz00_8);
																							BgL_otypez00_2794 =
																								BGl_usezd2typez12zc0zztype_envz00
																								(BgL_otypez00_1402,
																								BgL_locz00_8);
																							{	/* Ast/private.scm 85 */
																								BgL_getfieldz00_bglt
																									BgL_new1103z00_2795;
																								{	/* Ast/private.scm 86 */
																									BgL_getfieldz00_bglt
																										BgL_new1102z00_2802;
																									BgL_new1102z00_2802 =
																										((BgL_getfieldz00_bglt)
																										BOBJECT(GC_MALLOC(sizeof
																												(struct
																													BgL_getfieldz00_bgl))));
																									{	/* Ast/private.scm 86 */
																										long BgL_arg2488z00_2803;

																										BgL_arg2488z00_2803 =
																											BGL_CLASS_NUM
																											(BGl_getfieldz00zzast_nodez00);
																										BGL_OBJECT_CLASS_NUM_SET((
																												(BgL_objectz00_bglt)
																												BgL_new1102z00_2802),
																											BgL_arg2488z00_2803);
																									}
																									{	/* Ast/private.scm 86 */
																										BgL_objectz00_bglt
																											BgL_tmpz00_3835;
																										BgL_tmpz00_3835 =
																											((BgL_objectz00_bglt)
																											BgL_new1102z00_2802);
																										BGL_OBJECT_WIDENING_SET
																											(BgL_tmpz00_3835, BFALSE);
																									}
																									((BgL_objectz00_bglt)
																										BgL_new1102z00_2802);
																									BgL_new1103z00_2795 =
																										BgL_new1102z00_2802;
																								}
																								((((BgL_nodez00_bglt) COBJECT(
																												((BgL_nodez00_bglt)
																													BgL_new1103z00_2795)))->
																										BgL_locz00) =
																									((obj_t) BgL_locz00_8),
																									BUNSPEC);
																								((((BgL_nodez00_bglt)
																											COBJECT((
																													(BgL_nodez00_bglt)
																													BgL_new1103z00_2795)))->
																										BgL_typez00) =
																									((BgL_typez00_bglt)
																										BgL_ftypez00_2793),
																									BUNSPEC);
																								((((BgL_nodezf2effectzf2_bglt)
																											COBJECT((
																													(BgL_nodezf2effectzf2_bglt)
																													BgL_new1103z00_2795)))->
																										BgL_sidezd2effectzd2) =
																									((obj_t) BFALSE), BUNSPEC);
																								((((BgL_nodezf2effectzf2_bglt)
																											COBJECT((
																													(BgL_nodezf2effectzf2_bglt)
																													BgL_new1103z00_2795)))->
																										BgL_keyz00) =
																									((obj_t) BINT(-1L)), BUNSPEC);
																								{
																									obj_t BgL_auxz00_3848;

																									{	/* Ast/private.scm 92 */
																										BgL_nodez00_bglt
																											BgL_arg2484z00_2796;
																										BgL_arg2484z00_2796 =
																											BGl_sexpzd2ze3nodez31zzast_sexpz00
																											(BgL_objz00_1405,
																											BgL_stackz00_7,
																											BgL_locz00_8,
																											CNST_TABLE_REF(3));
																										{	/* Ast/private.scm 92 */
																											obj_t
																												BgL_list2485z00_2797;
																											BgL_list2485z00_2797 =
																												MAKE_YOUNG_PAIR(((obj_t)
																													BgL_arg2484z00_2796),
																												BNIL);
																											BgL_auxz00_3848 =
																												BgL_list2485z00_2797;
																									}}
																									((((BgL_externz00_bglt)
																												COBJECT((
																														(BgL_externz00_bglt)
																														BgL_new1103z00_2795)))->
																											BgL_exprza2za2) =
																										((obj_t) BgL_auxz00_3848),
																										BUNSPEC);
																								}
																								{
																									obj_t BgL_auxz00_3855;

																									{	/* Ast/private.scm 93 */
																										BgL_feffectz00_bglt
																											BgL_new1105z00_2798;
																										{	/* Ast/private.scm 94 */
																											BgL_feffectz00_bglt
																												BgL_new1104z00_2800;
																											BgL_new1104z00_2800 =
																												((BgL_feffectz00_bglt)
																												BOBJECT(GC_MALLOC(sizeof
																														(struct
																															BgL_feffectz00_bgl))));
																											{	/* Ast/private.scm 94 */
																												long
																													BgL_arg2487z00_2801;
																												{	/* Ast/private.scm 94 */
																													obj_t
																														BgL_classz00_3144;
																													BgL_classz00_3144 =
																														BGl_feffectz00zzast_varz00;
																													BgL_arg2487z00_2801 =
																														BGL_CLASS_NUM
																														(BgL_classz00_3144);
																												}
																												BGL_OBJECT_CLASS_NUM_SET
																													(((BgL_objectz00_bglt)
																														BgL_new1104z00_2800),
																													BgL_arg2487z00_2801);
																											}
																											BgL_new1105z00_2798 =
																												BgL_new1104z00_2800;
																										}
																										{
																											obj_t BgL_auxz00_3861;

																											{	/* Ast/private.scm 94 */
																												obj_t
																													BgL_list2486z00_2799;
																												BgL_list2486z00_2799 =
																													MAKE_YOUNG_PAIR
																													(BgL_tidz00_2792,
																													BNIL);
																												BgL_auxz00_3861 =
																													BgL_list2486z00_2799;
																											}
																											((((BgL_feffectz00_bglt)
																														COBJECT
																														(BgL_new1105z00_2798))->
																													BgL_readz00) =
																												((obj_t)
																													BgL_auxz00_3861),
																												BUNSPEC);
																										}
																										((((BgL_feffectz00_bglt)
																													COBJECT
																													(BgL_new1105z00_2798))->
																												BgL_writez00) =
																											((obj_t) BNIL), BUNSPEC);
																										BgL_auxz00_3855 =
																											((obj_t)
																											BgL_new1105z00_2798);
																									}
																									((((BgL_externz00_bglt)
																												COBJECT((
																														(BgL_externz00_bglt)
																														BgL_new1103z00_2795)))->
																											BgL_effectz00) =
																										((obj_t) BgL_auxz00_3855),
																										BUNSPEC);
																								}
																								((((BgL_privatez00_bglt)
																											COBJECT((
																													(BgL_privatez00_bglt)
																													BgL_new1103z00_2795)))->
																										BgL_czd2formatzd2) =
																									((obj_t) BgL_czd2fmtzd2_1404),
																									BUNSPEC);
																								((((BgL_getfieldz00_bglt)
																											COBJECT
																											(BgL_new1103z00_2795))->
																										BgL_fnamez00) =
																									((obj_t)
																										BgL_fieldzd2namezd2_1403),
																									BUNSPEC);
																								((((BgL_getfieldz00_bglt)
																											COBJECT
																											(BgL_new1103z00_2795))->
																										BgL_ftypez00) =
																									((BgL_typez00_bglt)
																										BgL_ftypez00_2793),
																									BUNSPEC);
																								((((BgL_getfieldz00_bglt)
																											COBJECT
																											(BgL_new1103z00_2795))->
																										BgL_otypez00) =
																									((BgL_typez00_bglt)
																										BgL_otypez00_2794),
																									BUNSPEC);
																								BgL_auxz00_3818 =
																									BgL_new1103z00_2795;
																						}}
																						return ((obj_t) BgL_auxz00_3818);
																					}
																				}
																			else
																				{	/* Ast/private.scm 78 */
																				BgL_tagzd2141zd2_1460:
																					{	/* Ast/private.scm 227 */
																						obj_t BgL_arg2561z00_2924;

																						{	/* Ast/private.scm 227 */
																							bool_t BgL_test2710z00_3873;

																							{	/* Ast/private.scm 227 */
																								obj_t BgL_tmpz00_3874;

																								BgL_tmpz00_3874 =
																									CDR(BgL_sexpz00_6);
																								BgL_test2710z00_3873 =
																									PAIRP(BgL_tmpz00_3874);
																							}
																							if (BgL_test2710z00_3873)
																								{	/* Ast/private.scm 227 */
																									BgL_arg2561z00_2924 =
																										CAR(CDR(BgL_sexpz00_6));
																								}
																							else
																								{	/* Ast/private.scm 227 */
																									BgL_arg2561z00_2924 =
																										BgL_sexpz00_6;
																								}
																						}
																						return
																							BGl_errorz00zz__errorz00
																							(BGl_string2665z00zzast_privatez00,
																							BGl_string2666z00zzast_privatez00,
																							BgL_arg2561z00_2924);
																					}
																				}
																		}
																	else
																		{	/* Ast/private.scm 78 */
																			goto BgL_tagzd2141zd2_1460;
																		}
																}
															else
																{	/* Ast/private.scm 78 */
																	goto BgL_tagzd2141zd2_1460;
																}
														}
													else
														{	/* Ast/private.scm 78 */
															goto BgL_tagzd2141zd2_1460;
														}
												}
											else
												{	/* Ast/private.scm 78 */
													goto BgL_tagzd2141zd2_1460;
												}
										}
									else
										{	/* Ast/private.scm 78 */
											goto BgL_tagzd2141zd2_1460;
										}
								}
							else
								{	/* Ast/private.scm 78 */
									obj_t BgL_cdrzd21936zd2_1567;

									BgL_cdrzd21936zd2_1567 = CDR(((obj_t) BgL_cdrzd2164zd2_1463));
									if (
										(CAR(((obj_t) BgL_cdrzd2164zd2_1463)) == CNST_TABLE_REF(7)))
										{	/* Ast/private.scm 78 */
											if (PAIRP(BgL_cdrzd21936zd2_1567))
												{	/* Ast/private.scm 78 */
													obj_t BgL_cdrzd21943zd2_1571;

													BgL_cdrzd21943zd2_1571 = CDR(BgL_cdrzd21936zd2_1567);
													if (PAIRP(BgL_cdrzd21943zd2_1571))
														{	/* Ast/private.scm 78 */
															obj_t BgL_cdrzd21950zd2_1573;

															BgL_cdrzd21950zd2_1573 =
																CDR(BgL_cdrzd21943zd2_1571);
															if (PAIRP(BgL_cdrzd21950zd2_1573))
																{	/* Ast/private.scm 78 */
																	obj_t BgL_cdrzd21956zd2_1575;

																	BgL_cdrzd21956zd2_1575 =
																		CDR(BgL_cdrzd21950zd2_1573);
																	if (PAIRP(BgL_cdrzd21956zd2_1575))
																		{	/* Ast/private.scm 78 */
																			obj_t BgL_arg1584z00_1577;
																			obj_t BgL_arg1585z00_1578;
																			obj_t BgL_arg1589z00_1579;
																			obj_t BgL_arg1591z00_1580;
																			obj_t BgL_arg1593z00_1581;

																			BgL_arg1584z00_1577 =
																				CAR(BgL_cdrzd21936zd2_1567);
																			BgL_arg1585z00_1578 =
																				CAR(BgL_cdrzd21943zd2_1571);
																			BgL_arg1589z00_1579 =
																				CAR(BgL_cdrzd21950zd2_1573);
																			BgL_arg1591z00_1580 =
																				CAR(BgL_cdrzd21956zd2_1575);
																			BgL_arg1593z00_1581 =
																				CDR(BgL_cdrzd21956zd2_1575);
																			{
																				BgL_setfieldz00_bglt BgL_auxz00_3903;

																				BgL_ftypez00_1407 = BgL_arg1584z00_1577;
																				BgL_otypez00_1408 = BgL_arg1585z00_1578;
																				BgL_fieldzd2namezd2_1409 =
																					BgL_arg1589z00_1579;
																				BgL_czd2fmtzd2_1410 =
																					BgL_arg1591z00_1580;
																				BgL_restz00_1411 = BgL_arg1593z00_1581;
																				{	/* Ast/private.scm 97 */
																					obj_t BgL_tidz00_2809;
																					BgL_typez00_bglt BgL_otypez00_2810;
																					BgL_typez00_bglt BgL_ftypez00_2811;

																					{	/* Ast/private.scm 99 */
																						obj_t BgL_arg2501z00_2819;

																						{	/* Ast/private.scm 99 */
																							obj_t BgL_arg2506z00_2823;

																							if (bigloo_mangledp
																								(BgL_fieldzd2namezd2_1409))
																								{	/* Ast/private.scm 75 */
																									BgL_arg2506z00_2823 =
																										bigloo_demangle
																										(BgL_fieldzd2namezd2_1409);
																								}
																							else
																								{	/* Ast/private.scm 75 */
																									BgL_arg2506z00_2823 =
																										BgL_fieldzd2namezd2_1409;
																								}
																							BgL_arg2501z00_2819 =
																								bstring_to_symbol(
																								((obj_t) BgL_arg2506z00_2823));
																						}
																						{	/* Ast/private.scm 97 */
																							obj_t BgL_list2502z00_2820;

																							{	/* Ast/private.scm 97 */
																								obj_t BgL_arg2503z00_2821;

																								{	/* Ast/private.scm 97 */
																									obj_t BgL_arg2505z00_2822;

																									BgL_arg2505z00_2822 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg2501z00_2819, BNIL);
																									BgL_arg2503z00_2821 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(2),
																										BgL_arg2505z00_2822);
																								}
																								BgL_list2502z00_2820 =
																									MAKE_YOUNG_PAIR
																									(BgL_otypez00_1408,
																									BgL_arg2503z00_2821);
																							}
																							BgL_tidz00_2809 =
																								BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																								(BgL_list2502z00_2820);
																						}
																					}
																					BgL_otypez00_2810 =
																						BGl_usezd2typez12zc0zztype_envz00
																						(BgL_otypez00_1408, BgL_locz00_8);
																					BgL_ftypez00_2811 =
																						BGl_usezd2typez12zc0zztype_envz00
																						(BgL_ftypez00_1407, BgL_locz00_8);
																					{	/* Ast/private.scm 102 */
																						BgL_setfieldz00_bglt
																							BgL_new1107z00_2812;
																						{	/* Ast/private.scm 103 */
																							BgL_setfieldz00_bglt
																								BgL_new1106z00_2817;
																							BgL_new1106z00_2817 =
																								((BgL_setfieldz00_bglt)
																								BOBJECT(GC_MALLOC(sizeof(struct
																											BgL_setfieldz00_bgl))));
																							{	/* Ast/private.scm 103 */
																								long BgL_arg2500z00_2818;

																								BgL_arg2500z00_2818 =
																									BGL_CLASS_NUM
																									(BGl_setfieldz00zzast_nodez00);
																								BGL_OBJECT_CLASS_NUM_SET((
																										(BgL_objectz00_bglt)
																										BgL_new1106z00_2817),
																									BgL_arg2500z00_2818);
																							}
																							{	/* Ast/private.scm 103 */
																								BgL_objectz00_bglt
																									BgL_tmpz00_3920;
																								BgL_tmpz00_3920 =
																									((BgL_objectz00_bglt)
																									BgL_new1106z00_2817);
																								BGL_OBJECT_WIDENING_SET
																									(BgL_tmpz00_3920, BFALSE);
																							}
																							((BgL_objectz00_bglt)
																								BgL_new1106z00_2817);
																							BgL_new1107z00_2812 =
																								BgL_new1106z00_2817;
																						}
																						((((BgL_nodez00_bglt) COBJECT(
																										((BgL_nodez00_bglt)
																											BgL_new1107z00_2812)))->
																								BgL_locz00) =
																							((obj_t) BgL_locz00_8), BUNSPEC);
																						((((BgL_nodez00_bglt)
																									COBJECT(((BgL_nodez00_bglt)
																											BgL_new1107z00_2812)))->
																								BgL_typez00) =
																							((BgL_typez00_bglt) (
																									(BgL_typez00_bglt)
																									BGl_za2objza2z00zztype_cachez00)),
																							BUNSPEC);
																						((((BgL_nodezf2effectzf2_bglt)
																									COBJECT((
																											(BgL_nodezf2effectzf2_bglt)
																											BgL_new1107z00_2812)))->
																								BgL_sidezd2effectzd2) =
																							((obj_t) BTRUE), BUNSPEC);
																						((((BgL_nodezf2effectzf2_bglt)
																									COBJECT((
																											(BgL_nodezf2effectzf2_bglt)
																											BgL_new1107z00_2812)))->
																								BgL_keyz00) =
																							((obj_t) BINT(-1L)), BUNSPEC);
																						((((BgL_externz00_bglt)
																									COBJECT(((BgL_externz00_bglt)
																											BgL_new1107z00_2812)))->
																								BgL_exprza2za2) =
																							((obj_t)
																								BGl_sexpza2zd2ze3nodez93zzast_sexpz00
																								(BgL_restz00_1411,
																									BgL_stackz00_7, BgL_locz00_8,
																									CNST_TABLE_REF(3))), BUNSPEC);
																						{
																							obj_t BgL_auxz00_3938;

																							{	/* Ast/private.scm 110 */
																								BgL_feffectz00_bglt
																									BgL_new1109z00_2813;
																								{	/* Ast/var.scm 179 */
																									BgL_feffectz00_bglt
																										BgL_new1108z00_2815;
																									BgL_new1108z00_2815 =
																										((BgL_feffectz00_bglt)
																										BOBJECT(GC_MALLOC(sizeof
																												(struct
																													BgL_feffectz00_bgl))));
																									{	/* Ast/var.scm 179 */
																										long BgL_arg2497z00_2816;

																										{	/* Ast/var.scm 179 */
																											obj_t BgL_classz00_3154;

																											BgL_classz00_3154 =
																												BGl_feffectz00zzast_varz00;
																											BgL_arg2497z00_2816 =
																												BGL_CLASS_NUM
																												(BgL_classz00_3154);
																										}
																										BGL_OBJECT_CLASS_NUM_SET(
																											((BgL_objectz00_bglt)
																												BgL_new1108z00_2815),
																											BgL_arg2497z00_2816);
																									}
																									BgL_new1109z00_2813 =
																										BgL_new1108z00_2815;
																								}
																								((((BgL_feffectz00_bglt)
																											COBJECT
																											(BgL_new1109z00_2813))->
																										BgL_readz00) =
																									((obj_t) BNIL), BUNSPEC);
																								{
																									obj_t BgL_auxz00_3945;

																									{	/* Ast/private.scm 111 */
																										obj_t BgL_list2496z00_2814;

																										BgL_list2496z00_2814 =
																											MAKE_YOUNG_PAIR
																											(BgL_tidz00_2809, BNIL);
																										BgL_auxz00_3945 =
																											BgL_list2496z00_2814;
																									}
																									((((BgL_feffectz00_bglt)
																												COBJECT
																												(BgL_new1109z00_2813))->
																											BgL_writez00) =
																										((obj_t) BgL_auxz00_3945),
																										BUNSPEC);
																								}
																								BgL_auxz00_3938 =
																									((obj_t) BgL_new1109z00_2813);
																							}
																							((((BgL_externz00_bglt) COBJECT(
																											((BgL_externz00_bglt)
																												BgL_new1107z00_2812)))->
																									BgL_effectz00) =
																								((obj_t) BgL_auxz00_3938),
																								BUNSPEC);
																						}
																						((((BgL_privatez00_bglt) COBJECT(
																										((BgL_privatez00_bglt)
																											BgL_new1107z00_2812)))->
																								BgL_czd2formatzd2) =
																							((obj_t) BgL_czd2fmtzd2_1410),
																							BUNSPEC);
																						((((BgL_setfieldz00_bglt)
																									COBJECT
																									(BgL_new1107z00_2812))->
																								BgL_fnamez00) =
																							((obj_t)
																								BgL_fieldzd2namezd2_1409),
																							BUNSPEC);
																						((((BgL_setfieldz00_bglt)
																									COBJECT
																									(BgL_new1107z00_2812))->
																								BgL_ftypez00) =
																							((BgL_typez00_bglt)
																								BgL_ftypez00_2811), BUNSPEC);
																						((((BgL_setfieldz00_bglt)
																									COBJECT
																									(BgL_new1107z00_2812))->
																								BgL_otypez00) =
																							((BgL_typez00_bglt)
																								BgL_otypez00_2810), BUNSPEC);
																						BgL_auxz00_3903 =
																							BgL_new1107z00_2812;
																				}}
																				return ((obj_t) BgL_auxz00_3903);
																			}
																		}
																	else
																		{	/* Ast/private.scm 78 */
																			goto BgL_tagzd2141zd2_1460;
																		}
																}
															else
																{	/* Ast/private.scm 78 */
																	goto BgL_tagzd2141zd2_1460;
																}
														}
													else
														{	/* Ast/private.scm 78 */
															goto BgL_tagzd2141zd2_1460;
														}
												}
											else
												{	/* Ast/private.scm 78 */
													goto BgL_tagzd2141zd2_1460;
												}
										}
									else
										{	/* Ast/private.scm 78 */
											obj_t BgL_cdrzd22708zd2_1591;

											BgL_cdrzd22708zd2_1591 = CDR(BgL_sexpz00_6);
											{	/* Ast/private.scm 78 */
												obj_t BgL_cdrzd22711zd2_1592;

												BgL_cdrzd22711zd2_1592 =
													CDR(((obj_t) BgL_cdrzd22708zd2_1591));
												if (
													(CAR(
															((obj_t) BgL_cdrzd22708zd2_1591)) ==
														CNST_TABLE_REF(8)))
													{	/* Ast/private.scm 78 */
														if (PAIRP(BgL_cdrzd22711zd2_1592))
															{	/* Ast/private.scm 78 */
																if (NULLP(CDR(BgL_cdrzd22711zd2_1592)))
																	{	/* Ast/private.scm 78 */
																		obj_t BgL_arg1625z00_1598;

																		BgL_arg1625z00_1598 =
																			CAR(BgL_cdrzd22711zd2_1592);
																		{
																			BgL_newz00_bglt BgL_auxz00_3970;

																			BgL_typez00_1413 = BgL_arg1625z00_1598;
																			{	/* Ast/private.scm 114 */
																				BgL_newz00_bglt BgL_new1111z00_2824;

																				{	/* Ast/private.scm 115 */
																					BgL_newz00_bglt BgL_new1110z00_2825;

																					BgL_new1110z00_2825 =
																						((BgL_newz00_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_newz00_bgl))));
																					{	/* Ast/private.scm 115 */
																						long BgL_arg2508z00_2826;

																						BgL_arg2508z00_2826 =
																							BGL_CLASS_NUM
																							(BGl_newz00zzast_nodez00);
																						BGL_OBJECT_CLASS_NUM_SET((
																								(BgL_objectz00_bglt)
																								BgL_new1110z00_2825),
																							BgL_arg2508z00_2826);
																					}
																					{	/* Ast/private.scm 115 */
																						BgL_objectz00_bglt BgL_tmpz00_3975;

																						BgL_tmpz00_3975 =
																							((BgL_objectz00_bglt)
																							BgL_new1110z00_2825);
																						BGL_OBJECT_WIDENING_SET
																							(BgL_tmpz00_3975, BFALSE);
																					}
																					((BgL_objectz00_bglt)
																						BgL_new1110z00_2825);
																					BgL_new1111z00_2824 =
																						BgL_new1110z00_2825;
																				}
																				((((BgL_nodez00_bglt) COBJECT(
																								((BgL_nodez00_bglt)
																									BgL_new1111z00_2824)))->
																						BgL_locz00) =
																					((obj_t) BgL_locz00_8), BUNSPEC);
																				((((BgL_nodez00_bglt)
																							COBJECT(((BgL_nodez00_bglt)
																									BgL_new1111z00_2824)))->
																						BgL_typez00) =
																					((BgL_typez00_bglt)
																						BGl_usezd2typez12zc0zztype_envz00
																						(BgL_typez00_1413, BgL_locz00_8)),
																					BUNSPEC);
																				((((BgL_nodezf2effectzf2_bglt)
																							COBJECT((
																									(BgL_nodezf2effectzf2_bglt)
																									BgL_new1111z00_2824)))->
																						BgL_sidezd2effectzd2) =
																					((obj_t) BTRUE), BUNSPEC);
																				((((BgL_nodezf2effectzf2_bglt)
																							COBJECT((
																									(BgL_nodezf2effectzf2_bglt)
																									BgL_new1111z00_2824)))->
																						BgL_keyz00) =
																					((obj_t) BINT(-1L)), BUNSPEC);
																				((((BgL_externz00_bglt)
																							COBJECT(((BgL_externz00_bglt)
																									BgL_new1111z00_2824)))->
																						BgL_exprza2za2) =
																					((obj_t) BNIL), BUNSPEC);
																				((((BgL_externz00_bglt)
																							COBJECT(((BgL_externz00_bglt)
																									BgL_new1111z00_2824)))->
																						BgL_effectz00) =
																					((obj_t) BUNSPEC), BUNSPEC);
																				((((BgL_privatez00_bglt)
																							COBJECT(((BgL_privatez00_bglt)
																									BgL_new1111z00_2824)))->
																						BgL_czd2formatzd2) =
																					((obj_t)
																						BGl_string2664z00zzast_privatez00),
																					BUNSPEC);
																				((((BgL_newz00_bglt)
																							COBJECT(BgL_new1111z00_2824))->
																						BgL_argszd2typezd2) =
																					((obj_t) BNIL), BUNSPEC);
																				BgL_auxz00_3970 = BgL_new1111z00_2824;
																			}
																			return ((obj_t) BgL_auxz00_3970);
																		}
																	}
																else
																	{	/* Ast/private.scm 78 */
																		obj_t BgL_cdrzd22740zd2_1599;

																		BgL_cdrzd22740zd2_1599 =
																			CDR(((obj_t) BgL_cdrzd22708zd2_1591));
																		{	/* Ast/private.scm 78 */
																			obj_t BgL_cdrzd22746zd2_1600;

																			BgL_cdrzd22746zd2_1600 =
																				CDR(((obj_t) BgL_cdrzd22740zd2_1599));
																			if (PAIRP(BgL_cdrzd22746zd2_1600))
																				{	/* Ast/private.scm 78 */
																					obj_t BgL_carzd22751zd2_1602;

																					BgL_carzd22751zd2_1602 =
																						CAR(BgL_cdrzd22746zd2_1600);
																					if (PAIRP(BgL_carzd22751zd2_1602))
																						{	/* Ast/private.scm 78 */
																							obj_t BgL_cdrzd22755zd2_1604;

																							BgL_cdrzd22755zd2_1604 =
																								CDR(BgL_carzd22751zd2_1602);
																							if (
																								(CAR(BgL_carzd22751zd2_1602) ==
																									CNST_TABLE_REF(9)))
																								{	/* Ast/private.scm 78 */
																									if (PAIRP
																										(BgL_cdrzd22755zd2_1604))
																										{	/* Ast/private.scm 78 */
																											if (NULLP(CDR
																													(BgL_cdrzd22755zd2_1604)))
																												{	/* Ast/private.scm 78 */
																													obj_t
																														BgL_arg1646z00_1610;
																													obj_t
																														BgL_arg1650z00_1611;
																													obj_t
																														BgL_arg1651z00_1612;
																													BgL_arg1646z00_1610 =
																														CAR(((obj_t)
																															BgL_cdrzd22740zd2_1599));
																													BgL_arg1650z00_1611 =
																														CAR
																														(BgL_cdrzd22755zd2_1604);
																													BgL_arg1651z00_1612 =
																														CDR
																														(BgL_cdrzd22746zd2_1600);
																													{
																														BgL_newz00_bglt
																															BgL_auxz00_4020;
																														BgL_typez00_1415 =
																															BgL_arg1646z00_1610;
																														BgL_argszd2typezd2_1416
																															=
																															BgL_arg1650z00_1611;
																														BgL_restz00_1417 =
																															BgL_arg1651z00_1612;
																														if (NULLP
																															(BgL_restz00_1417))
																															{	/* Ast/private.scm 122 */
																																BgL_newz00_bglt
																																	BgL_new1114z00_2828;
																																{	/* Ast/private.scm 123 */
																																	BgL_newz00_bglt
																																		BgL_new1113z00_2842;
																																	BgL_new1113z00_2842
																																		=
																																		(
																																		(BgL_newz00_bglt)
																																		BOBJECT
																																		(GC_MALLOC
																																			(sizeof
																																				(struct
																																					BgL_newz00_bgl))));
																																	{	/* Ast/private.scm 123 */
																																		long
																																			BgL_arg2515z00_2843;
																																		BgL_arg2515z00_2843
																																			=
																																			BGL_CLASS_NUM
																																			(BGl_newz00zzast_nodez00);
																																		BGL_OBJECT_CLASS_NUM_SET
																																			(((BgL_objectz00_bglt) BgL_new1113z00_2842), BgL_arg2515z00_2843);
																																	}
																																	{	/* Ast/private.scm 123 */
																																		BgL_objectz00_bglt
																																			BgL_tmpz00_4027;
																																		BgL_tmpz00_4027
																																			=
																																			(
																																			(BgL_objectz00_bglt)
																																			BgL_new1113z00_2842);
																																		BGL_OBJECT_WIDENING_SET
																																			(BgL_tmpz00_4027,
																																			BFALSE);
																																	}
																																	((BgL_objectz00_bglt) BgL_new1113z00_2842);
																																	BgL_new1114z00_2828
																																		=
																																		BgL_new1113z00_2842;
																																}
																																((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1114z00_2828)))->BgL_locz00) = ((obj_t) BgL_locz00_8), BUNSPEC);
																																((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1114z00_2828)))->BgL_typez00) = ((BgL_typez00_bglt) BGl_usezd2typez12zc0zztype_envz00(BgL_typez00_1415, BgL_locz00_8)), BUNSPEC);
																																((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1114z00_2828)))->BgL_sidezd2effectzd2) = ((obj_t) BTRUE), BUNSPEC);
																																((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1114z00_2828)))->BgL_keyz00) = ((obj_t) BINT(-1L)), BUNSPEC);
																																((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt) BgL_new1114z00_2828)))->BgL_exprza2za2) = ((obj_t) BNIL), BUNSPEC);
																																((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt) BgL_new1114z00_2828)))->BgL_effectz00) = ((obj_t) BUNSPEC), BUNSPEC);
																																((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt) BgL_new1114z00_2828)))->BgL_czd2formatzd2) = ((obj_t) BGl_string2664z00zzast_privatez00), BUNSPEC);
																																{
																																	obj_t
																																		BgL_auxz00_4047;
																																	if (NULLP
																																		(BgL_argszd2typezd2_1416))
																																		{	/* Ast/private.scm 125 */
																																			BgL_auxz00_4047
																																				= BNIL;
																																		}
																																	else
																																		{	/* Ast/private.scm 125 */
																																			obj_t
																																				BgL_head1265z00_2831;
																																			BgL_head1265z00_2831
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BNIL,
																																				BNIL);
																																			{
																																				obj_t
																																					BgL_l1263z00_2833;
																																				obj_t
																																					BgL_tail1266z00_2834;
																																				BgL_l1263z00_2833
																																					=
																																					BgL_argszd2typezd2_1416;
																																				BgL_tail1266z00_2834
																																					=
																																					BgL_head1265z00_2831;
																																			BgL_zc3z04anonymousza32511ze3z87_2835:
																																				if (NULLP(BgL_l1263z00_2833))
																																					{	/* Ast/private.scm 125 */
																																						BgL_auxz00_4047
																																							=
																																							CDR
																																							(BgL_head1265z00_2831);
																																					}
																																				else
																																					{	/* Ast/private.scm 125 */
																																						obj_t
																																							BgL_newtail1267z00_2837;
																																						{	/* Ast/private.scm 125 */
																																							BgL_typez00_bglt
																																								BgL_arg2514z00_2839;
																																							{	/* Ast/private.scm 125 */
																																								obj_t
																																									BgL_tz00_2840;
																																								BgL_tz00_2840
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_l1263z00_2833));
																																								BgL_arg2514z00_2839
																																									=
																																									BGl_usezd2typez12zc0zztype_envz00
																																									(BgL_tz00_2840,
																																									BgL_locz00_8);
																																							}
																																							BgL_newtail1267z00_2837
																																								=
																																								MAKE_YOUNG_PAIR
																																								(
																																								((obj_t) BgL_arg2514z00_2839), BNIL);
																																						}
																																						SET_CDR
																																							(BgL_tail1266z00_2834,
																																							BgL_newtail1267z00_2837);
																																						{	/* Ast/private.scm 125 */
																																							obj_t
																																								BgL_arg2513z00_2838;
																																							BgL_arg2513z00_2838
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_l1263z00_2833));
																																							{
																																								obj_t
																																									BgL_tail1266z00_4063;
																																								obj_t
																																									BgL_l1263z00_4062;
																																								BgL_l1263z00_4062
																																									=
																																									BgL_arg2513z00_2838;
																																								BgL_tail1266z00_4063
																																									=
																																									BgL_newtail1267z00_2837;
																																								BgL_tail1266z00_2834
																																									=
																																									BgL_tail1266z00_4063;
																																								BgL_l1263z00_2833
																																									=
																																									BgL_l1263z00_4062;
																																								goto
																																									BgL_zc3z04anonymousza32511ze3z87_2835;
																																							}
																																						}
																																					}
																																			}
																																		}
																																	((((BgL_newz00_bglt) COBJECT(BgL_new1114z00_2828))->BgL_argszd2typezd2) = ((obj_t) BgL_auxz00_4047), BUNSPEC);
																																}
																																BgL_auxz00_4020
																																	=
																																	BgL_new1114z00_2828;
																															}
																														else
																															{	/* Ast/private.scm 128 */
																																BgL_newz00_bglt
																																	BgL_new1116z00_2844;
																																{	/* Ast/private.scm 129 */
																																	BgL_newz00_bglt
																																		BgL_new1115z00_2859;
																																	BgL_new1115z00_2859
																																		=
																																		(
																																		(BgL_newz00_bglt)
																																		BOBJECT
																																		(GC_MALLOC
																																			(sizeof
																																				(struct
																																					BgL_newz00_bgl))));
																																	{	/* Ast/private.scm 129 */
																																		long
																																			BgL_arg2525z00_2860;
																																		BgL_arg2525z00_2860
																																			=
																																			BGL_CLASS_NUM
																																			(BGl_newz00zzast_nodez00);
																																		BGL_OBJECT_CLASS_NUM_SET
																																			(((BgL_objectz00_bglt) BgL_new1115z00_2859), BgL_arg2525z00_2860);
																																	}
																																	{	/* Ast/private.scm 129 */
																																		BgL_objectz00_bglt
																																			BgL_tmpz00_4069;
																																		BgL_tmpz00_4069
																																			=
																																			(
																																			(BgL_objectz00_bglt)
																																			BgL_new1115z00_2859);
																																		BGL_OBJECT_WIDENING_SET
																																			(BgL_tmpz00_4069,
																																			BFALSE);
																																	}
																																	((BgL_objectz00_bglt) BgL_new1115z00_2859);
																																	BgL_new1116z00_2844
																																		=
																																		BgL_new1115z00_2859;
																																}
																																((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1116z00_2844)))->BgL_locz00) = ((obj_t) BgL_locz00_8), BUNSPEC);
																																((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1116z00_2844)))->BgL_typez00) = ((BgL_typez00_bglt) BGl_usezd2typez12zc0zztype_envz00(BgL_typez00_1415, BgL_locz00_8)), BUNSPEC);
																																((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1116z00_2844)))->BgL_sidezd2effectzd2) = ((obj_t) BTRUE), BUNSPEC);
																																((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1116z00_2844)))->BgL_keyz00) = ((obj_t) BINT(-1L)), BUNSPEC);
																																((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt) BgL_new1116z00_2844)))->BgL_exprza2za2) = ((obj_t) BGl_sexpza2zd2ze3nodez93zzast_sexpz00(BgL_restz00_1417, BgL_stackz00_7, BgL_locz00_8, CNST_TABLE_REF(3))), BUNSPEC);
																																((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt) BgL_new1116z00_2844)))->BgL_effectz00) = ((obj_t) BUNSPEC), BUNSPEC);
																																((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt) BgL_new1116z00_2844)))->BgL_czd2formatzd2) = ((obj_t) BGl_string2664z00zzast_privatez00), BUNSPEC);
																																{
																																	obj_t
																																		BgL_auxz00_4091;
																																	if (NULLP
																																		(BgL_argszd2typezd2_1416))
																																		{	/* Ast/private.scm 131 */
																																			BgL_auxz00_4091
																																				= BNIL;
																																		}
																																	else
																																		{	/* Ast/private.scm 131 */
																																			obj_t
																																				BgL_head1270z00_2848;
																																			BgL_head1270z00_2848
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BNIL,
																																				BNIL);
																																			{
																																				obj_t
																																					BgL_l1268z00_2850;
																																				obj_t
																																					BgL_tail1271z00_2851;
																																				BgL_l1268z00_2850
																																					=
																																					BgL_argszd2typezd2_1416;
																																				BgL_tail1271z00_2851
																																					=
																																					BgL_head1270z00_2848;
																																			BgL_zc3z04anonymousza32518ze3z87_2852:
																																				if (NULLP(BgL_l1268z00_2850))
																																					{	/* Ast/private.scm 131 */
																																						BgL_auxz00_4091
																																							=
																																							CDR
																																							(BgL_head1270z00_2848);
																																					}
																																				else
																																					{	/* Ast/private.scm 131 */
																																						obj_t
																																							BgL_newtail1272z00_2854;
																																						{	/* Ast/private.scm 131 */
																																							BgL_typez00_bglt
																																								BgL_arg2524z00_2856;
																																							{	/* Ast/private.scm 131 */
																																								obj_t
																																									BgL_tz00_2857;
																																								BgL_tz00_2857
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_l1268z00_2850));
																																								BgL_arg2524z00_2856
																																									=
																																									BGl_usezd2typez12zc0zztype_envz00
																																									(BgL_tz00_2857,
																																									BgL_locz00_8);
																																							}
																																							BgL_newtail1272z00_2854
																																								=
																																								MAKE_YOUNG_PAIR
																																								(
																																								((obj_t) BgL_arg2524z00_2856), BNIL);
																																						}
																																						SET_CDR
																																							(BgL_tail1271z00_2851,
																																							BgL_newtail1272z00_2854);
																																						{	/* Ast/private.scm 131 */
																																							obj_t
																																								BgL_arg2521z00_2855;
																																							BgL_arg2521z00_2855
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_l1268z00_2850));
																																							{
																																								obj_t
																																									BgL_tail1271z00_4107;
																																								obj_t
																																									BgL_l1268z00_4106;
																																								BgL_l1268z00_4106
																																									=
																																									BgL_arg2521z00_2855;
																																								BgL_tail1271z00_4107
																																									=
																																									BgL_newtail1272z00_2854;
																																								BgL_tail1271z00_2851
																																									=
																																									BgL_tail1271z00_4107;
																																								BgL_l1268z00_2850
																																									=
																																									BgL_l1268z00_4106;
																																								goto
																																									BgL_zc3z04anonymousza32518ze3z87_2852;
																																							}
																																						}
																																					}
																																			}
																																		}
																																	((((BgL_newz00_bglt) COBJECT(BgL_new1116z00_2844))->BgL_argszd2typezd2) = ((obj_t) BgL_auxz00_4091), BUNSPEC);
																																}
																																BgL_auxz00_4020
																																	=
																																	BgL_new1116z00_2844;
																															}
																														return
																															((obj_t)
																															BgL_auxz00_4020);
																													}
																												}
																											else
																												{	/* Ast/private.scm 78 */
																													goto
																														BgL_tagzd2141zd2_1460;
																												}
																										}
																									else
																										{	/* Ast/private.scm 78 */
																											goto
																												BgL_tagzd2141zd2_1460;
																										}
																								}
																							else
																								{	/* Ast/private.scm 78 */
																									goto BgL_tagzd2141zd2_1460;
																								}
																						}
																					else
																						{	/* Ast/private.scm 78 */
																							goto BgL_tagzd2141zd2_1460;
																						}
																				}
																			else
																				{	/* Ast/private.scm 78 */
																					goto BgL_tagzd2141zd2_1460;
																				}
																		}
																	}
															}
														else
															{	/* Ast/private.scm 78 */
																goto BgL_tagzd2141zd2_1460;
															}
													}
												else
													{	/* Ast/private.scm 78 */
														obj_t BgL_cdrzd27112zd2_1974;

														BgL_cdrzd27112zd2_1974 =
															CDR(((obj_t) BgL_cdrzd22708zd2_1591));
														if (
															(CAR(
																	((obj_t) BgL_cdrzd22708zd2_1591)) ==
																CNST_TABLE_REF(1)))
															{	/* Ast/private.scm 78 */
																if (PAIRP(BgL_cdrzd27112zd2_1974))
																	{	/* Ast/private.scm 78 */
																		obj_t BgL_cdrzd27116zd2_1978;

																		BgL_cdrzd27116zd2_1978 =
																			CDR(BgL_cdrzd27112zd2_1974);
																		if (PAIRP(BgL_cdrzd27116zd2_1978))
																			{	/* Ast/private.scm 78 */
																				if (NULLP(CDR(BgL_cdrzd27116zd2_1978)))
																					{	/* Ast/private.scm 78 */
																						obj_t BgL_arg1984z00_1982;
																						obj_t BgL_arg1985z00_1983;

																						BgL_arg1984z00_1982 =
																							CAR(BgL_cdrzd27112zd2_1974);
																						BgL_arg1985z00_1983 =
																							CAR(BgL_cdrzd27116zd2_1978);
																						{
																							BgL_castz00_bglt BgL_auxz00_4127;

																							BgL_typez00_1419 =
																								BgL_arg1984z00_1982;
																							BgL_expz00_1420 =
																								BgL_arg1985z00_1983;
																							{	/* Ast/private.scm 138 */
																								BgL_castz00_bglt
																									BgL_new1119z00_2861;
																								{	/* Ast/private.scm 139 */
																									BgL_castz00_bglt
																										BgL_new1118z00_2862;
																									BgL_new1118z00_2862 =
																										((BgL_castz00_bglt)
																										BOBJECT(GC_MALLOC(sizeof
																												(struct
																													BgL_castz00_bgl))));
																									{	/* Ast/private.scm 139 */
																										long BgL_arg2526z00_2863;

																										BgL_arg2526z00_2863 =
																											BGL_CLASS_NUM
																											(BGl_castz00zzast_nodez00);
																										BGL_OBJECT_CLASS_NUM_SET((
																												(BgL_objectz00_bglt)
																												BgL_new1118z00_2862),
																											BgL_arg2526z00_2863);
																									}
																									{	/* Ast/private.scm 139 */
																										BgL_objectz00_bglt
																											BgL_tmpz00_4132;
																										BgL_tmpz00_4132 =
																											((BgL_objectz00_bglt)
																											BgL_new1118z00_2862);
																										BGL_OBJECT_WIDENING_SET
																											(BgL_tmpz00_4132, BFALSE);
																									}
																									((BgL_objectz00_bglt)
																										BgL_new1118z00_2862);
																									BgL_new1119z00_2861 =
																										BgL_new1118z00_2862;
																								}
																								((((BgL_nodez00_bglt) COBJECT(
																												((BgL_nodez00_bglt)
																													BgL_new1119z00_2861)))->
																										BgL_locz00) =
																									((obj_t) BgL_locz00_8),
																									BUNSPEC);
																								((((BgL_nodez00_bglt)
																											COBJECT((
																													(BgL_nodez00_bglt)
																													BgL_new1119z00_2861)))->
																										BgL_typez00) =
																									((BgL_typez00_bglt)
																										BGl_usezd2typez12zc0zztype_envz00
																										(BgL_typez00_1419,
																											BgL_locz00_8)), BUNSPEC);
																								((((BgL_castz00_bglt)
																											COBJECT
																											(BgL_new1119z00_2861))->
																										BgL_argz00) =
																									((BgL_nodez00_bglt)
																										BGl_sexpzd2ze3nodez31zzast_sexpz00
																										(BgL_expz00_1420,
																											BgL_stackz00_7,
																											BgL_locz00_8,
																											BgL_sitez00_9)), BUNSPEC);
																								BgL_auxz00_4127 =
																									BgL_new1119z00_2861;
																							}
																							return ((obj_t) BgL_auxz00_4127);
																						}
																					}
																				else
																					{	/* Ast/private.scm 78 */
																						goto BgL_tagzd2141zd2_1460;
																					}
																			}
																		else
																			{	/* Ast/private.scm 78 */
																				goto BgL_tagzd2141zd2_1460;
																			}
																	}
																else
																	{	/* Ast/private.scm 78 */
																		goto BgL_tagzd2141zd2_1460;
																	}
															}
														else
															{	/* Ast/private.scm 78 */
																obj_t BgL_cdrzd27868zd2_2025;

																BgL_cdrzd27868zd2_2025 = CDR(BgL_sexpz00_6);
																{	/* Ast/private.scm 78 */
																	obj_t BgL_cdrzd27871zd2_2026;

																	BgL_cdrzd27871zd2_2026 =
																		CDR(((obj_t) BgL_cdrzd27868zd2_2025));
																	if (
																		(CAR(
																				((obj_t) BgL_cdrzd27868zd2_2025)) ==
																			CNST_TABLE_REF(10)))
																		{	/* Ast/private.scm 78 */
																			if (PAIRP(BgL_cdrzd27871zd2_2026))
																				{	/* Ast/private.scm 78 */
																					if (NULLP(CDR
																							(BgL_cdrzd27871zd2_2026)))
																						{	/* Ast/private.scm 78 */
																							obj_t BgL_arg2015z00_2032;

																							BgL_arg2015z00_2032 =
																								CAR(BgL_cdrzd27871zd2_2026);
																							{
																								BgL_castzd2nullzd2_bglt
																									BgL_auxz00_4158;
																								BgL_typez00_1422 =
																									BgL_arg2015z00_2032;
																								{	/* Ast/private.scm 143 */
																									BgL_castzd2nullzd2_bglt
																										BgL_new1122z00_2864;
																									{	/* Ast/private.scm 144 */
																										BgL_castzd2nullzd2_bglt
																											BgL_new1120z00_2865;
																										BgL_new1120z00_2865 =
																											((BgL_castzd2nullzd2_bglt)
																											BOBJECT(GC_MALLOC(sizeof
																													(struct
																														BgL_castzd2nullzd2_bgl))));
																										{	/* Ast/private.scm 144 */
																											long BgL_arg2527z00_2866;

																											BgL_arg2527z00_2866 =
																												BGL_CLASS_NUM
																												(BGl_castzd2nullzd2zzast_nodez00);
																											BGL_OBJECT_CLASS_NUM_SET((
																													(BgL_objectz00_bglt)
																													BgL_new1120z00_2865),
																												BgL_arg2527z00_2866);
																										}
																										{	/* Ast/private.scm 144 */
																											BgL_objectz00_bglt
																												BgL_tmpz00_4163;
																											BgL_tmpz00_4163 =
																												((BgL_objectz00_bglt)
																												BgL_new1120z00_2865);
																											BGL_OBJECT_WIDENING_SET
																												(BgL_tmpz00_4163,
																												BFALSE);
																										}
																										((BgL_objectz00_bglt)
																											BgL_new1120z00_2865);
																										BgL_new1122z00_2864 =
																											BgL_new1120z00_2865;
																									}
																									((((BgL_nodez00_bglt) COBJECT(
																													((BgL_nodez00_bglt)
																														BgL_new1122z00_2864)))->
																											BgL_locz00) =
																										((obj_t) BgL_locz00_8),
																										BUNSPEC);
																									((((BgL_nodez00_bglt)
																												COBJECT((
																														(BgL_nodez00_bglt)
																														BgL_new1122z00_2864)))->
																											BgL_typez00) =
																										((BgL_typez00_bglt)
																											BGl_usezd2typez12zc0zztype_envz00
																											(BgL_typez00_1422,
																												BgL_locz00_8)),
																										BUNSPEC);
																									((((BgL_nodezf2effectzf2_bglt)
																												COBJECT((
																														(BgL_nodezf2effectzf2_bglt)
																														BgL_new1122z00_2864)))->
																											BgL_sidezd2effectzd2) =
																										((obj_t) BUNSPEC), BUNSPEC);
																									((((BgL_nodezf2effectzf2_bglt)
																												COBJECT((
																														(BgL_nodezf2effectzf2_bglt)
																														BgL_new1122z00_2864)))->
																											BgL_keyz00) =
																										((obj_t) BINT(-1L)),
																										BUNSPEC);
																									((((BgL_externz00_bglt)
																												COBJECT((
																														(BgL_externz00_bglt)
																														BgL_new1122z00_2864)))->
																											BgL_exprza2za2) =
																										((obj_t) BNIL), BUNSPEC);
																									((((BgL_externz00_bglt)
																												COBJECT((
																														(BgL_externz00_bglt)
																														BgL_new1122z00_2864)))->
																											BgL_effectz00) =
																										((obj_t) BUNSPEC), BUNSPEC);
																									((((BgL_privatez00_bglt)
																												COBJECT((
																														(BgL_privatez00_bglt)
																														BgL_new1122z00_2864)))->
																											BgL_czd2formatzd2) =
																										((obj_t)
																											BGl_string2664z00zzast_privatez00),
																										BUNSPEC);
																									BgL_auxz00_4158 =
																										BgL_new1122z00_2864;
																								}
																								return
																									((obj_t) BgL_auxz00_4158);
																							}
																						}
																					else
																						{	/* Ast/private.scm 78 */
																							goto BgL_tagzd2141zd2_1460;
																						}
																				}
																			else
																				{	/* Ast/private.scm 78 */
																					goto BgL_tagzd2141zd2_1460;
																				}
																		}
																	else
																		{	/* Ast/private.scm 78 */
																			obj_t BgL_carzd29148zd2_2127;
																			obj_t BgL_cdrzd29149zd2_2128;

																			BgL_carzd29148zd2_2127 =
																				CAR(((obj_t) BgL_cdrzd27868zd2_2025));
																			BgL_cdrzd29149zd2_2128 =
																				CDR(((obj_t) BgL_cdrzd27868zd2_2025));
																			{

																				if (
																					(BgL_carzd29148zd2_2127 ==
																						CNST_TABLE_REF(16)))
																					{	/* Ast/private.scm 78 */
																					BgL_kapzd29150zd2_2129:
																						if (PAIRP(BgL_cdrzd29149zd2_2128))
																							{	/* Ast/private.scm 78 */
																								obj_t BgL_cdrzd29154zd2_2478;

																								BgL_cdrzd29154zd2_2478 =
																									CDR(BgL_cdrzd29149zd2_2128);
																								if (PAIRP
																									(BgL_cdrzd29154zd2_2478))
																									{	/* Ast/private.scm 78 */
																										if (NULLP(CDR
																												(BgL_cdrzd29154zd2_2478)))
																											{	/* Ast/private.scm 78 */
																												obj_t
																													BgL_arg2288z00_2482;
																												obj_t
																													BgL_arg2289z00_2483;
																												BgL_arg2288z00_2482 =
																													CAR
																													(BgL_cdrzd29149zd2_2128);
																												BgL_arg2289z00_2483 =
																													CAR
																													(BgL_cdrzd29154zd2_2478);
																												{
																													BgL_instanceofz00_bglt
																														BgL_auxz00_4201;
																													BgL_typez00_1424 =
																														BgL_arg2288z00_2482;
																													BgL_expz00_1425 =
																														BgL_arg2289z00_2483;
																													{	/* Ast/private.scm 149 */
																														BgL_instanceofz00_bglt
																															BgL_new1124z00_2867;
																														{	/* Ast/private.scm 150 */
																															BgL_instanceofz00_bglt
																																BgL_new1123z00_2873;
																															BgL_new1123z00_2873
																																=
																																(
																																(BgL_instanceofz00_bglt)
																																BOBJECT
																																(GC_MALLOC
																																	(sizeof(struct
																																			BgL_instanceofz00_bgl))));
																															{	/* Ast/private.scm 150 */
																																long
																																	BgL_arg2536z00_2874;
																																BgL_arg2536z00_2874
																																	=
																																	BGL_CLASS_NUM
																																	(BGl_instanceofz00zzast_nodez00);
																																BGL_OBJECT_CLASS_NUM_SET
																																	(((BgL_objectz00_bglt) BgL_new1123z00_2873), BgL_arg2536z00_2874);
																															}
																															{	/* Ast/private.scm 150 */
																																BgL_objectz00_bglt
																																	BgL_tmpz00_4206;
																																BgL_tmpz00_4206
																																	=
																																	(
																																	(BgL_objectz00_bglt)
																																	BgL_new1123z00_2873);
																																BGL_OBJECT_WIDENING_SET
																																	(BgL_tmpz00_4206,
																																	BFALSE);
																															}
																															((BgL_objectz00_bglt) BgL_new1123z00_2873);
																															BgL_new1124z00_2867
																																=
																																BgL_new1123z00_2873;
																														}
																														((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1124z00_2867)))->BgL_locz00) = ((obj_t) BgL_locz00_8), BUNSPEC);
																														((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1124z00_2867)))->BgL_typez00) = ((BgL_typez00_bglt) ((BgL_typez00_bglt) BGl_za2boolza2z00zztype_cachez00)), BUNSPEC);
																														((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1124z00_2867)))->BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
																														((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1124z00_2867)))->BgL_keyz00) = ((obj_t) BINT(-1L)), BUNSPEC);
																														{
																															obj_t
																																BgL_auxz00_4220;
																															{	/* Ast/private.scm 153 */
																																BgL_nodez00_bglt
																																	BgL_arg2528z00_2868;
																																BgL_arg2528z00_2868
																																	=
																																	BGl_sexpzd2ze3nodez31zzast_sexpz00
																																	(BgL_expz00_1425,
																																	BgL_stackz00_7,
																																	BgL_locz00_8,
																																	BgL_sitez00_9);
																																{	/* Ast/private.scm 153 */
																																	obj_t
																																		BgL_list2529z00_2869;
																																	BgL_list2529z00_2869
																																		=
																																		MAKE_YOUNG_PAIR
																																		(((obj_t)
																																			BgL_arg2528z00_2868),
																																		BNIL);
																																	BgL_auxz00_4220
																																		=
																																		BgL_list2529z00_2869;
																															}}
																															((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt) BgL_new1124z00_2867)))->BgL_exprza2za2) = ((obj_t) BgL_auxz00_4220), BUNSPEC);
																														}
																														{
																															obj_t
																																BgL_auxz00_4226;
																															{	/* Ast/private.scm 154 */
																																BgL_feffectz00_bglt
																																	BgL_new1127z00_2870;
																																{	/* Ast/var.scm 179 */
																																	BgL_feffectz00_bglt
																																		BgL_new1125z00_2871;
																																	BgL_new1125z00_2871
																																		=
																																		(
																																		(BgL_feffectz00_bglt)
																																		BOBJECT
																																		(GC_MALLOC
																																			(sizeof
																																				(struct
																																					BgL_feffectz00_bgl))));
																																	{	/* Ast/var.scm 179 */
																																		long
																																			BgL_arg2534z00_2872;
																																		{	/* Ast/var.scm 179 */
																																			obj_t
																																				BgL_classz00_3191;
																																			BgL_classz00_3191
																																				=
																																				BGl_feffectz00zzast_varz00;
																																			BgL_arg2534z00_2872
																																				=
																																				BGL_CLASS_NUM
																																				(BgL_classz00_3191);
																																		}
																																		BGL_OBJECT_CLASS_NUM_SET
																																			(((BgL_objectz00_bglt) BgL_new1125z00_2871), BgL_arg2534z00_2872);
																																	}
																																	BgL_new1127z00_2870
																																		=
																																		BgL_new1125z00_2871;
																																}
																																((((BgL_feffectz00_bglt) COBJECT(BgL_new1127z00_2870))->BgL_readz00) = ((obj_t) BNIL), BUNSPEC);
																																((((BgL_feffectz00_bglt) COBJECT(BgL_new1127z00_2870))->BgL_writez00) = ((obj_t) BNIL), BUNSPEC);
																																BgL_auxz00_4226
																																	=
																																	((obj_t)
																																	BgL_new1127z00_2870);
																															}
																															((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt) BgL_new1124z00_2867)))->BgL_effectz00) = ((obj_t) BgL_auxz00_4226), BUNSPEC);
																														}
																														((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt) BgL_new1124z00_2867)))->BgL_czd2formatzd2) = ((obj_t) BGl_string2664z00zzast_privatez00), BUNSPEC);
																														((((BgL_instanceofz00_bglt) COBJECT(BgL_new1124z00_2867))->BgL_classz00) = ((BgL_typez00_bglt) BGl_usezd2typez12zc0zztype_envz00(BgL_typez00_1424, BgL_locz00_8)), BUNSPEC);
																														BgL_auxz00_4201 =
																															BgL_new1124z00_2867;
																													}
																													return
																														((obj_t)
																														BgL_auxz00_4201);
																												}
																											}
																										else
																											{	/* Ast/private.scm 78 */
																												obj_t
																													BgL_cdrzd29180zd2_2484;
																												BgL_cdrzd29180zd2_2484 =
																													CDR(BgL_sexpz00_6);
																												{	/* Ast/private.scm 78 */
																													obj_t
																														BgL_cdrzd29189zd2_2485;
																													BgL_cdrzd29189zd2_2485
																														=
																														CDR(((obj_t)
																															BgL_cdrzd29180zd2_2484));
																													if ((CAR(((obj_t)
																																	BgL_cdrzd29180zd2_2484))
																															==
																															CNST_TABLE_REF
																															(11)))
																														{	/* Ast/private.scm 78 */
																															obj_t
																																BgL_cdrzd29198zd2_2488;
																															BgL_cdrzd29198zd2_2488
																																=
																																CDR(((obj_t)
																																	BgL_cdrzd29189zd2_2485));
																															{	/* Ast/private.scm 78 */
																																obj_t
																																	BgL_cdrzd29207zd2_2489;
																																BgL_cdrzd29207zd2_2489
																																	=
																																	CDR(((obj_t)
																																		BgL_cdrzd29198zd2_2488));
																																if (PAIRP
																																	(BgL_cdrzd29207zd2_2489))
																																	{	/* Ast/private.scm 78 */
																																		obj_t
																																			BgL_cdrzd29214zd2_2491;
																																		BgL_cdrzd29214zd2_2491
																																			=
																																			CDR
																																			(BgL_cdrzd29207zd2_2489);
																																		if (PAIRP
																																			(BgL_cdrzd29214zd2_2491))
																																			{	/* Ast/private.scm 78 */
																																				obj_t
																																					BgL_carzd29218zd2_2493;
																																				obj_t
																																					BgL_cdrzd29219zd2_2494;
																																				BgL_carzd29218zd2_2493
																																					=
																																					CAR
																																					(BgL_cdrzd29214zd2_2491);
																																				BgL_cdrzd29219zd2_2494
																																					=
																																					CDR
																																					(BgL_cdrzd29214zd2_2491);
																																				if (STRINGP(BgL_carzd29218zd2_2493))
																																					{	/* Ast/private.scm 78 */
																																						if (PAIRP(BgL_cdrzd29219zd2_2494))
																																							{	/* Ast/private.scm 78 */
																																								if (NULLP(CDR(BgL_cdrzd29219zd2_2494)))
																																									{	/* Ast/private.scm 78 */
																																										obj_t
																																											BgL_arg2298z00_2499;
																																										obj_t
																																											BgL_arg2299z00_2500;
																																										obj_t
																																											BgL_arg2301z00_2501;
																																										obj_t
																																											BgL_arg2302z00_2502;
																																										BgL_arg2298z00_2499
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd29189zd2_2485));
																																										BgL_arg2299z00_2500
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd29198zd2_2488));
																																										BgL_arg2301z00_2501
																																											=
																																											CAR
																																											(BgL_cdrzd29207zd2_2489);
																																										BgL_arg2302z00_2502
																																											=
																																											CAR
																																											(BgL_cdrzd29219zd2_2494);
																																										{
																																											BgL_vlengthz00_bglt
																																												BgL_auxz00_4273;
																																											BgL_vtypez00_1427
																																												=
																																												BgL_arg2298z00_2499;
																																											BgL_ftypez00_1428
																																												=
																																												BgL_arg2299z00_2500;
																																											BgL_otypez00_1429
																																												=
																																												BgL_arg2301z00_2501;
																																											BgL_czd2fmtzd2_1430
																																												=
																																												BgL_carzd29218zd2_2493;
																																											BgL_expz00_1431
																																												=
																																												BgL_arg2302z00_2502;
																																										BgL_tagzd2135zd2_1432:
																																											{	/* Ast/private.scm 157 */
																																												BgL_typez00_bglt
																																													BgL_vtypez00_2875;
																																												BgL_typez00_bglt
																																													BgL_otypez00_2876;
																																												BgL_typez00_bglt
																																													BgL_ftypez00_2877;
																																												BgL_vtypez00_2875
																																													=
																																													BGl_usezd2typez12zc0zztype_envz00
																																													(BgL_vtypez00_1427,
																																													BgL_locz00_8);
																																												BgL_otypez00_2876
																																													=
																																													BGl_usezd2typez12zc0zztype_envz00
																																													(BgL_otypez00_1429,
																																													BgL_locz00_8);
																																												BgL_ftypez00_2877
																																													=
																																													BGl_usezd2typez12zc0zztype_envz00
																																													(BgL_ftypez00_1428,
																																													BgL_locz00_8);
																																												{	/* Ast/private.scm 160 */
																																													BgL_vlengthz00_bglt
																																														BgL_new1130z00_2878;
																																													{	/* Ast/private.scm 161 */
																																														BgL_vlengthz00_bglt
																																															BgL_new1129z00_2884;
																																														BgL_new1129z00_2884
																																															=
																																															(
																																															(BgL_vlengthz00_bglt)
																																															BOBJECT
																																															(GC_MALLOC
																																																(sizeof
																																																	(struct
																																																		BgL_vlengthz00_bgl))));
																																														{	/* Ast/private.scm 161 */
																																															long
																																																BgL_arg2540z00_2885;
																																															BgL_arg2540z00_2885
																																																=
																																																BGL_CLASS_NUM
																																																(BGl_vlengthz00zzast_nodez00);
																																															BGL_OBJECT_CLASS_NUM_SET
																																																(
																																																((BgL_objectz00_bglt) BgL_new1129z00_2884), BgL_arg2540z00_2885);
																																														}
																																														{	/* Ast/private.scm 161 */
																																															BgL_objectz00_bglt
																																																BgL_tmpz00_4281;
																																															BgL_tmpz00_4281
																																																=
																																																(
																																																(BgL_objectz00_bglt)
																																																BgL_new1129z00_2884);
																																															BGL_OBJECT_WIDENING_SET
																																																(BgL_tmpz00_4281,
																																																BFALSE);
																																														}
																																														((BgL_objectz00_bglt) BgL_new1129z00_2884);
																																														BgL_new1130z00_2878
																																															=
																																															BgL_new1129z00_2884;
																																													}
																																													((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1130z00_2878)))->BgL_locz00) = ((obj_t) BgL_locz00_8), BUNSPEC);
																																													((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1130z00_2878)))->BgL_typez00) = ((BgL_typez00_bglt) BgL_otypez00_2876), BUNSPEC);
																																													((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1130z00_2878)))->BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
																																													((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1130z00_2878)))->BgL_keyz00) = ((obj_t) BINT(-1L)), BUNSPEC);
																																													{
																																														obj_t
																																															BgL_auxz00_4294;
																																														{	/* Ast/private.scm 166 */
																																															BgL_nodez00_bglt
																																																BgL_arg2537z00_2879;
																																															BgL_arg2537z00_2879
																																																=
																																																BGl_sexpzd2ze3nodez31zzast_sexpz00
																																																(BgL_expz00_1431,
																																																BgL_stackz00_7,
																																																BgL_locz00_8,
																																																CNST_TABLE_REF
																																																(3));
																																															{	/* Ast/private.scm 166 */
																																																obj_t
																																																	BgL_list2538z00_2880;
																																																BgL_list2538z00_2880
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(
																																																	((obj_t) BgL_arg2537z00_2879), BNIL);
																																																BgL_auxz00_4294
																																																	=
																																																	BgL_list2538z00_2880;
																																														}}
																																														((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt) BgL_new1130z00_2878)))->BgL_exprza2za2) = ((obj_t) BgL_auxz00_4294), BUNSPEC);
																																													}
																																													{
																																														obj_t
																																															BgL_auxz00_4301;
																																														{	/* Ast/private.scm 167 */
																																															BgL_feffectz00_bglt
																																																BgL_new1133z00_2881;
																																															{	/* Ast/var.scm 179 */
																																																BgL_feffectz00_bglt
																																																	BgL_new1131z00_2882;
																																																BgL_new1131z00_2882
																																																	=
																																																	(
																																																	(BgL_feffectz00_bglt)
																																																	BOBJECT
																																																	(GC_MALLOC
																																																		(sizeof
																																																			(struct
																																																				BgL_feffectz00_bgl))));
																																																{	/* Ast/var.scm 179 */
																																																	long
																																																		BgL_arg2539z00_2883;
																																																	{	/* Ast/var.scm 179 */
																																																		obj_t
																																																			BgL_classz00_3199;
																																																		BgL_classz00_3199
																																																			=
																																																			BGl_feffectz00zzast_varz00;
																																																		BgL_arg2539z00_2883
																																																			=
																																																			BGL_CLASS_NUM
																																																			(BgL_classz00_3199);
																																																	}
																																																	BGL_OBJECT_CLASS_NUM_SET
																																																		(
																																																		((BgL_objectz00_bglt) BgL_new1131z00_2882), BgL_arg2539z00_2883);
																																																}
																																																BgL_new1133z00_2881
																																																	=
																																																	BgL_new1131z00_2882;
																																															}
																																															((((BgL_feffectz00_bglt) COBJECT(BgL_new1133z00_2881))->BgL_readz00) = ((obj_t) BNIL), BUNSPEC);
																																															((((BgL_feffectz00_bglt) COBJECT(BgL_new1133z00_2881))->BgL_writez00) = ((obj_t) BNIL), BUNSPEC);
																																															BgL_auxz00_4301
																																																=
																																																(
																																																(obj_t)
																																																BgL_new1133z00_2881);
																																														}
																																														((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt) BgL_new1130z00_2878)))->BgL_effectz00) = ((obj_t) BgL_auxz00_4301), BUNSPEC);
																																													}
																																													((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt) BgL_new1130z00_2878)))->BgL_czd2formatzd2) = ((obj_t) BgL_czd2fmtzd2_1430), BUNSPEC);
																																													((((BgL_vlengthz00_bglt) COBJECT(BgL_new1130z00_2878))->BgL_vtypez00) = ((BgL_typez00_bglt) BgL_vtypez00_2875), BUNSPEC);
																																													((((BgL_vlengthz00_bglt) COBJECT(BgL_new1130z00_2878))->BgL_ftypez00) = ((obj_t) ((obj_t) BgL_ftypez00_2877)), BUNSPEC);
																																													BgL_auxz00_4273
																																														=
																																														BgL_new1130z00_2878;
																																											}}
																																											return
																																												(
																																												(obj_t)
																																												BgL_auxz00_4273);
																																										}
																																									}
																																								else
																																									{	/* Ast/private.scm 78 */
																																										goto
																																											BgL_tagzd2141zd2_1460;
																																									}
																																							}
																																						else
																																							{	/* Ast/private.scm 78 */
																																								goto
																																									BgL_tagzd2141zd2_1460;
																																							}
																																					}
																																				else
																																					{	/* Ast/private.scm 78 */
																																						goto
																																							BgL_tagzd2141zd2_1460;
																																					}
																																			}
																																		else
																																			{	/* Ast/private.scm 78 */
																																				goto
																																					BgL_tagzd2141zd2_1460;
																																			}
																																	}
																																else
																																	{	/* Ast/private.scm 78 */
																																		goto
																																			BgL_tagzd2141zd2_1460;
																																	}
																															}
																														}
																													else
																														{	/* Ast/private.scm 78 */
																															obj_t
																																BgL_carzd210351zd2_2613;
																															obj_t
																																BgL_cdrzd210352zd2_2614;
																															BgL_carzd210351zd2_2613
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd29180zd2_2484));
																															BgL_cdrzd210352zd2_2614
																																=
																																CDR(((obj_t)
																																	BgL_cdrzd29180zd2_2484));
																															{

																																if (
																																	(BgL_carzd210351zd2_2613
																																		==
																																		CNST_TABLE_REF
																																		(15)))
																																	{	/* Ast/private.scm 78 */
																																	BgL_kapzd210353zd2_2615:
																																		{	/* Ast/private.scm 78 */
																																			obj_t
																																				BgL_cdrzd210362zd2_2704;
																																			BgL_cdrzd210362zd2_2704
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_cdrzd210352zd2_2614));
																																			{	/* Ast/private.scm 78 */
																																				obj_t
																																					BgL_cdrzd210371zd2_2705;
																																				BgL_cdrzd210371zd2_2705
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_cdrzd210362zd2_2704));
																																				if (PAIRP(BgL_cdrzd210371zd2_2705))
																																					{	/* Ast/private.scm 78 */
																																						obj_t
																																							BgL_cdrzd210378zd2_2707;
																																						BgL_cdrzd210378zd2_2707
																																							=
																																							CDR
																																							(BgL_cdrzd210371zd2_2705);
																																						if (PAIRP(BgL_cdrzd210378zd2_2707))
																																							{	/* Ast/private.scm 78 */
																																								obj_t
																																									BgL_carzd210382zd2_2709;
																																								BgL_carzd210382zd2_2709
																																									=
																																									CAR
																																									(BgL_cdrzd210378zd2_2707);
																																								if (STRINGP(BgL_carzd210382zd2_2709))
																																									{	/* Ast/private.scm 78 */
																																										obj_t
																																											BgL_arg2425z00_2711;
																																										obj_t
																																											BgL_arg2426z00_2712;
																																										obj_t
																																											BgL_arg2428z00_2713;
																																										obj_t
																																											BgL_arg2429z00_2714;
																																										BgL_arg2425z00_2711
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd210352zd2_2614));
																																										BgL_arg2426z00_2712
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_cdrzd210362zd2_2704));
																																										BgL_arg2428z00_2713
																																											=
																																											CAR
																																											(BgL_cdrzd210371zd2_2705);
																																										BgL_arg2429z00_2714
																																											=
																																											CDR
																																											(BgL_cdrzd210378zd2_2707);
																																										{
																																											BgL_vrefz00_bglt
																																												BgL_auxz00_4342;
																																											BgL_vtypez00_1433
																																												=
																																												BgL_arg2425z00_2711;
																																											BgL_ftypez00_1434
																																												=
																																												BgL_arg2426z00_2712;
																																											BgL_otypez00_1435
																																												=
																																												BgL_arg2428z00_2713;
																																											BgL_czd2fmtzd2_1436
																																												=
																																												BgL_carzd210382zd2_2709;
																																											BgL_restz00_1437
																																												=
																																												BgL_arg2429z00_2714;
																																										BgL_tagzd2136zd2_1438:
																																											{	/* Ast/private.scm 169 */
																																												BgL_typez00_bglt
																																													BgL_ftypez00_2886;
																																												BgL_typez00_bglt
																																													BgL_vtypez00_2887;
																																												BgL_typez00_bglt
																																													BgL_otypez00_2888;
																																												BgL_ftypez00_2886
																																													=
																																													BGl_usezd2typez12zc0zztype_envz00
																																													(BgL_ftypez00_1434,
																																													BgL_locz00_8);
																																												BgL_vtypez00_2887
																																													=
																																													BGl_usezd2typez12zc0zztype_envz00
																																													(BgL_vtypez00_1433,
																																													BgL_locz00_8);
																																												BgL_otypez00_2888
																																													=
																																													BGl_usezd2typez12zc0zztype_envz00
																																													(BgL_otypez00_1435,
																																													BgL_locz00_8);
																																												{	/* Ast/private.scm 172 */
																																													BgL_vrefz00_bglt
																																														BgL_new1135z00_2889;
																																													{	/* Ast/private.scm 173 */
																																														BgL_vrefz00_bglt
																																															BgL_new1134z00_2896;
																																														BgL_new1134z00_2896
																																															=
																																															(
																																															(BgL_vrefz00_bglt)
																																															BOBJECT
																																															(GC_MALLOC
																																																(sizeof
																																																	(struct
																																																		BgL_vrefz00_bgl))));
																																														{	/* Ast/private.scm 173 */
																																															long
																																																BgL_arg2548z00_2897;
																																															BgL_arg2548z00_2897
																																																=
																																																BGL_CLASS_NUM
																																																(BGl_vrefz00zzast_nodez00);
																																															BGL_OBJECT_CLASS_NUM_SET
																																																(
																																																((BgL_objectz00_bglt) BgL_new1134z00_2896), BgL_arg2548z00_2897);
																																														}
																																														{	/* Ast/private.scm 173 */
																																															BgL_objectz00_bglt
																																																BgL_tmpz00_4350;
																																															BgL_tmpz00_4350
																																																=
																																																(
																																																(BgL_objectz00_bglt)
																																																BgL_new1134z00_2896);
																																															BGL_OBJECT_WIDENING_SET
																																																(BgL_tmpz00_4350,
																																																BFALSE);
																																														}
																																														((BgL_objectz00_bglt) BgL_new1134z00_2896);
																																														BgL_new1135z00_2889
																																															=
																																															BgL_new1134z00_2896;
																																													}
																																													((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1135z00_2889)))->BgL_locz00) = ((obj_t) BgL_locz00_8), BUNSPEC);
																																													((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1135z00_2889)))->BgL_typez00) = ((BgL_typez00_bglt) BgL_ftypez00_2886), BUNSPEC);
																																													((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1135z00_2889)))->BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
																																													((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1135z00_2889)))->BgL_keyz00) = ((obj_t) BINT(-1L)), BUNSPEC);
																																													((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt) BgL_new1135z00_2889)))->BgL_exprza2za2) = ((obj_t) BGl_sexpza2zd2ze3nodez93zzast_sexpz00(BgL_restz00_1437, BgL_stackz00_7, BgL_locz00_8, CNST_TABLE_REF(3))), BUNSPEC);
																																													{
																																														obj_t
																																															BgL_auxz00_4367;
																																														{	/* Ast/private.scm 181 */
																																															BgL_feffectz00_bglt
																																																BgL_new1137z00_2890;
																																															{	/* Ast/private.scm 182 */
																																																BgL_feffectz00_bglt
																																																	BgL_new1136z00_2893;
																																																BgL_new1136z00_2893
																																																	=
																																																	(
																																																	(BgL_feffectz00_bglt)
																																																	BOBJECT
																																																	(GC_MALLOC
																																																		(sizeof
																																																			(struct
																																																				BgL_feffectz00_bgl))));
																																																{	/* Ast/private.scm 182 */
																																																	long
																																																		BgL_arg2545z00_2894;
																																																	{	/* Ast/private.scm 182 */
																																																		obj_t
																																																			BgL_classz00_3206;
																																																		BgL_classz00_3206
																																																			=
																																																			BGl_feffectz00zzast_varz00;
																																																		BgL_arg2545z00_2894
																																																			=
																																																			BGL_CLASS_NUM
																																																			(BgL_classz00_3206);
																																																	}
																																																	BGL_OBJECT_CLASS_NUM_SET
																																																		(
																																																		((BgL_objectz00_bglt) BgL_new1136z00_2893), BgL_arg2545z00_2894);
																																																}
																																																BgL_new1137z00_2890
																																																	=
																																																	BgL_new1136z00_2893;
																																															}
																																															{
																																																obj_t
																																																	BgL_auxz00_4373;
																																																{	/* Ast/private.scm 182 */
																																																	obj_t
																																																		BgL_arg2542z00_2891;
																																																	BgL_arg2542z00_2891
																																																		=
																																																		(
																																																		((BgL_typez00_bglt) COBJECT(BgL_ftypez00_2886))->BgL_idz00);
																																																	{	/* Ast/private.scm 182 */
																																																		obj_t
																																																			BgL_list2543z00_2892;
																																																		BgL_list2543z00_2892
																																																			=
																																																			MAKE_YOUNG_PAIR
																																																			(BgL_arg2542z00_2891,
																																																			BNIL);
																																																		BgL_auxz00_4373
																																																			=
																																																			BgL_list2543z00_2892;
																																																}}
																																																((((BgL_feffectz00_bglt) COBJECT(BgL_new1137z00_2890))->BgL_readz00) = ((obj_t) BgL_auxz00_4373), BUNSPEC);
																																															}
																																															((((BgL_feffectz00_bglt) COBJECT(BgL_new1137z00_2890))->BgL_writez00) = ((obj_t) BNIL), BUNSPEC);
																																															BgL_auxz00_4367
																																																=
																																																(
																																																(obj_t)
																																																BgL_new1137z00_2890);
																																														}
																																														((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt) BgL_new1135z00_2889)))->BgL_effectz00) = ((obj_t) BgL_auxz00_4367), BUNSPEC);
																																													}
																																													((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt) BgL_new1135z00_2889)))->BgL_czd2formatzd2) = ((obj_t) BgL_czd2fmtzd2_1436), BUNSPEC);
																																													((((BgL_vrefz00_bglt) COBJECT(BgL_new1135z00_2889))->BgL_ftypez00) = ((BgL_typez00_bglt) BgL_ftypez00_2886), BUNSPEC);
																																													((((BgL_vrefz00_bglt) COBJECT(BgL_new1135z00_2889))->BgL_otypez00) = ((BgL_typez00_bglt) BgL_otypez00_2888), BUNSPEC);
																																													((((BgL_vrefz00_bglt) COBJECT(BgL_new1135z00_2889))->BgL_vtypez00) = ((BgL_typez00_bglt) BgL_vtypez00_2887), BUNSPEC);
																																													{
																																														bool_t
																																															BgL_auxz00_4385;
																																														{	/* Ast/private.scm 180 */
																																															obj_t
																																																BgL_tmpz00_4386;
																																															{	/* Ast/private.scm 180 */
																																																obj_t
																																																	BgL_pairz00_3214;
																																																BgL_pairz00_3214
																																																	=
																																																	CDR
																																																	(BgL_sexpz00_6);
																																																BgL_tmpz00_4386
																																																	=
																																																	CAR
																																																	(BgL_pairz00_3214);
																																															}
																																															BgL_auxz00_4385
																																																=
																																																(BgL_tmpz00_4386
																																																==
																																																CNST_TABLE_REF
																																																(4));
																																														}
																																														((((BgL_vrefz00_bglt) COBJECT(BgL_new1135z00_2889))->BgL_unsafez00) = ((bool_t) BgL_auxz00_4385), BUNSPEC);
																																													}
																																													BgL_auxz00_4342
																																														=
																																														BgL_new1135z00_2889;
																																											}}
																																											return
																																												(
																																												(obj_t)
																																												BgL_auxz00_4342);
																																										}
																																									}
																																								else
																																									{	/* Ast/private.scm 78 */
																																										obj_t
																																											BgL_cdrzd210407zd2_2715;
																																										BgL_cdrzd210407zd2_2715
																																											=
																																											CDR
																																											(BgL_sexpz00_6);
																																										{	/* Ast/private.scm 78 */
																																											obj_t
																																												BgL_carzd210414zd2_2716;
																																											obj_t
																																												BgL_cdrzd210415zd2_2717;
																																											BgL_carzd210414zd2_2716
																																												=
																																												CAR
																																												(
																																												((obj_t) BgL_cdrzd210407zd2_2715));
																																											BgL_cdrzd210415zd2_2717
																																												=
																																												CDR
																																												(
																																												((obj_t) BgL_cdrzd210407zd2_2715));
																																											{

																																												if ((BgL_carzd210414zd2_2716 == CNST_TABLE_REF(13)))
																																													{	/* Ast/private.scm 78 */
																																													BgL_kapzd210416zd2_2718:
																																														{	/* Ast/private.scm 78 */
																																															obj_t
																																																BgL_cdrzd210424zd2_2741;
																																															BgL_cdrzd210424zd2_2741
																																																=
																																																CDR
																																																(
																																																((obj_t) BgL_cdrzd210415zd2_2717));
																																															{	/* Ast/private.scm 78 */
																																																obj_t
																																																	BgL_cdrzd210432zd2_2742;
																																																BgL_cdrzd210432zd2_2742
																																																	=
																																																	CDR
																																																	(
																																																	((obj_t) BgL_cdrzd210424zd2_2741));
																																																{	/* Ast/private.scm 78 */
																																																	obj_t
																																																		BgL_cdrzd210439zd2_2743;
																																																	BgL_cdrzd210439zd2_2743
																																																		=
																																																		CDR
																																																		(
																																																		((obj_t) BgL_cdrzd210432zd2_2742));
																																																	{	/* Ast/private.scm 78 */
																																																		obj_t
																																																			BgL_carzd210444zd2_2744;
																																																		BgL_carzd210444zd2_2744
																																																			=
																																																			CAR
																																																			(
																																																			((obj_t) BgL_cdrzd210439zd2_2743));
																																																		if (STRINGP(BgL_carzd210444zd2_2744))
																																																			{	/* Ast/private.scm 78 */
																																																				obj_t
																																																					BgL_arg2445z00_2746;
																																																				obj_t
																																																					BgL_arg2446z00_2747;
																																																				obj_t
																																																					BgL_arg2447z00_2748;
																																																				obj_t
																																																					BgL_arg2449z00_2749;
																																																				BgL_arg2445z00_2746
																																																					=
																																																					CAR
																																																					(
																																																					((obj_t) BgL_cdrzd210415zd2_2717));
																																																				BgL_arg2446z00_2747
																																																					=
																																																					CAR
																																																					(
																																																					((obj_t) BgL_cdrzd210424zd2_2741));
																																																				BgL_arg2447z00_2748
																																																					=
																																																					CAR
																																																					(
																																																					((obj_t) BgL_cdrzd210432zd2_2742));
																																																				BgL_arg2449z00_2749
																																																					=
																																																					CDR
																																																					(
																																																					((obj_t) BgL_cdrzd210439zd2_2743));
																																																				{
																																																					BgL_vsetz12z12_bglt
																																																						BgL_auxz00_4419;
																																																					BgL_vtypez00_1439
																																																						=
																																																						BgL_arg2445z00_2746;
																																																					BgL_ftypez00_1440
																																																						=
																																																						BgL_arg2446z00_2747;
																																																					BgL_otypez00_1441
																																																						=
																																																						BgL_arg2447z00_2748;
																																																					BgL_czd2fmtzd2_1442
																																																						=
																																																						BgL_carzd210444zd2_2744;
																																																					BgL_restz00_1443
																																																						=
																																																						BgL_arg2449z00_2749;
																																																				BgL_tagzd2137zd2_1444:
																																																					{	/* Ast/private.scm 184 */
																																																						BgL_typez00_bglt
																																																							BgL_ftypez00_2898;
																																																						BgL_typez00_bglt
																																																							BgL_vtypez00_2899;
																																																						BgL_typez00_bglt
																																																							BgL_otypez00_2900;
																																																						BgL_ftypez00_2898
																																																							=
																																																							BGl_usezd2typez12zc0zztype_envz00
																																																							(BgL_ftypez00_1440,
																																																							BgL_locz00_8);
																																																						BgL_vtypez00_2899
																																																							=
																																																							BGl_usezd2typez12zc0zztype_envz00
																																																							(BgL_vtypez00_1439,
																																																							BgL_locz00_8);
																																																						BgL_otypez00_2900
																																																							=
																																																							BGl_usezd2typez12zc0zztype_envz00
																																																							(BgL_otypez00_1441,
																																																							BgL_locz00_8);
																																																						{	/* Ast/private.scm 187 */
																																																							BgL_vsetz12z12_bglt
																																																								BgL_new1139z00_2901;
																																																							{	/* Ast/private.scm 188 */
																																																								BgL_vsetz12z12_bglt
																																																									BgL_new1138z00_2908;
																																																								BgL_new1138z00_2908
																																																									=
																																																									(
																																																									(BgL_vsetz12z12_bglt)
																																																									BOBJECT
																																																									(GC_MALLOC
																																																										(sizeof
																																																											(struct
																																																												BgL_vsetz12z12_bgl))));
																																																								{	/* Ast/private.scm 188 */
																																																									long
																																																										BgL_arg2553z00_2909;
																																																									BgL_arg2553z00_2909
																																																										=
																																																										BGL_CLASS_NUM
																																																										(BGl_vsetz12z12zzast_nodez00);
																																																									BGL_OBJECT_CLASS_NUM_SET
																																																										(
																																																										((BgL_objectz00_bglt) BgL_new1138z00_2908), BgL_arg2553z00_2909);
																																																								}
																																																								{	/* Ast/private.scm 188 */
																																																									BgL_objectz00_bglt
																																																										BgL_tmpz00_4427;
																																																									BgL_tmpz00_4427
																																																										=
																																																										(
																																																										(BgL_objectz00_bglt)
																																																										BgL_new1138z00_2908);
																																																									BGL_OBJECT_WIDENING_SET
																																																										(BgL_tmpz00_4427,
																																																										BFALSE);
																																																								}
																																																								((BgL_objectz00_bglt) BgL_new1138z00_2908);
																																																								BgL_new1139z00_2901
																																																									=
																																																									BgL_new1138z00_2908;
																																																							}
																																																							((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1139z00_2901)))->BgL_locz00) = ((obj_t) BgL_locz00_8), BUNSPEC);
																																																							((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1139z00_2901)))->BgL_typez00) = ((BgL_typez00_bglt) ((BgL_typez00_bglt) BGl_za2unspecza2z00zztype_cachez00)), BUNSPEC);
																																																							((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1139z00_2901)))->BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
																																																							((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1139z00_2901)))->BgL_keyz00) = ((obj_t) BINT(-1L)), BUNSPEC);
																																																							((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt) BgL_new1139z00_2901)))->BgL_exprza2za2) = ((obj_t) BGl_sexpza2zd2ze3nodez93zzast_sexpz00(BgL_restz00_1443, BgL_stackz00_7, BgL_locz00_8, CNST_TABLE_REF(3))), BUNSPEC);
																																																							{
																																																								obj_t
																																																									BgL_auxz00_4445;
																																																								{	/* Ast/private.scm 196 */
																																																									BgL_feffectz00_bglt
																																																										BgL_new1141z00_2902;
																																																									{	/* Ast/var.scm 179 */
																																																										BgL_feffectz00_bglt
																																																											BgL_new1140z00_2905;
																																																										BgL_new1140z00_2905
																																																											=
																																																											(
																																																											(BgL_feffectz00_bglt)
																																																											BOBJECT
																																																											(GC_MALLOC
																																																												(sizeof
																																																													(struct
																																																														BgL_feffectz00_bgl))));
																																																										{	/* Ast/var.scm 179 */
																																																											long
																																																												BgL_arg2551z00_2906;
																																																											{	/* Ast/var.scm 179 */
																																																												obj_t
																																																													BgL_classz00_3219;
																																																												BgL_classz00_3219
																																																													=
																																																													BGl_feffectz00zzast_varz00;
																																																												BgL_arg2551z00_2906
																																																													=
																																																													BGL_CLASS_NUM
																																																													(BgL_classz00_3219);
																																																											}
																																																											BGL_OBJECT_CLASS_NUM_SET
																																																												(
																																																												((BgL_objectz00_bglt) BgL_new1140z00_2905), BgL_arg2551z00_2906);
																																																										}
																																																										BgL_new1141z00_2902
																																																											=
																																																											BgL_new1140z00_2905;
																																																									}
																																																									((((BgL_feffectz00_bglt) COBJECT(BgL_new1141z00_2902))->BgL_readz00) = ((obj_t) BNIL), BUNSPEC);
																																																									{
																																																										obj_t
																																																											BgL_auxz00_4452;
																																																										{	/* Ast/private.scm 197 */
																																																											obj_t
																																																												BgL_arg2549z00_2903;
																																																											BgL_arg2549z00_2903
																																																												=
																																																												(
																																																												((BgL_typez00_bglt) COBJECT(BgL_ftypez00_2898))->BgL_idz00);
																																																											{	/* Ast/private.scm 197 */
																																																												obj_t
																																																													BgL_list2550z00_2904;
																																																												BgL_list2550z00_2904
																																																													=
																																																													MAKE_YOUNG_PAIR
																																																													(BgL_arg2549z00_2903,
																																																													BNIL);
																																																												BgL_auxz00_4452
																																																													=
																																																													BgL_list2550z00_2904;
																																																										}}
																																																										((((BgL_feffectz00_bglt) COBJECT(BgL_new1141z00_2902))->BgL_writez00) = ((obj_t) BgL_auxz00_4452), BUNSPEC);
																																																									}
																																																									BgL_auxz00_4445
																																																										=
																																																										(
																																																										(obj_t)
																																																										BgL_new1141z00_2902);
																																																								}
																																																								((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt) BgL_new1139z00_2901)))->BgL_effectz00) = ((obj_t) BgL_auxz00_4445), BUNSPEC);
																																																							}
																																																							((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt) BgL_new1139z00_2901)))->BgL_czd2formatzd2) = ((obj_t) BgL_czd2fmtzd2_1442), BUNSPEC);
																																																							((((BgL_vsetz12z12_bglt) COBJECT(BgL_new1139z00_2901))->BgL_ftypez00) = ((BgL_typez00_bglt) BgL_ftypez00_2898), BUNSPEC);
																																																							((((BgL_vsetz12z12_bglt) COBJECT(BgL_new1139z00_2901))->BgL_otypez00) = ((BgL_typez00_bglt) BgL_otypez00_2900), BUNSPEC);
																																																							((((BgL_vsetz12z12_bglt) COBJECT(BgL_new1139z00_2901))->BgL_vtypez00) = ((BgL_typez00_bglt) BgL_vtypez00_2899), BUNSPEC);
																																																							{
																																																								bool_t
																																																									BgL_auxz00_4463;
																																																								{	/* Ast/private.scm 195 */
																																																									obj_t
																																																										BgL_tmpz00_4464;
																																																									{	/* Ast/private.scm 195 */
																																																										obj_t
																																																											BgL_pairz00_3227;
																																																										BgL_pairz00_3227
																																																											=
																																																											CDR
																																																											(BgL_sexpz00_6);
																																																										BgL_tmpz00_4464
																																																											=
																																																											CAR
																																																											(BgL_pairz00_3227);
																																																									}
																																																									BgL_auxz00_4463
																																																										=
																																																										(BgL_tmpz00_4464
																																																										==
																																																										CNST_TABLE_REF
																																																										(5));
																																																								}
																																																								((((BgL_vsetz12z12_bglt) COBJECT(BgL_new1139z00_2901))->BgL_unsafez00) = ((bool_t) BgL_auxz00_4463), BUNSPEC);
																																																							}
																																																							BgL_auxz00_4419
																																																								=
																																																								BgL_new1139z00_2901;
																																																					}}
																																																					return
																																																						(
																																																						(obj_t)
																																																						BgL_auxz00_4419);
																																																				}
																																																			}
																																																		else
																																																			{	/* Ast/private.scm 78 */
																																																				obj_t
																																																					BgL_cdrzd210470zd2_2750;
																																																				BgL_cdrzd210470zd2_2750
																																																					=
																																																					CDR
																																																					(BgL_sexpz00_6);
																																																				{	/* Ast/private.scm 78 */
																																																					obj_t
																																																						BgL_cdrzd210480zd2_2751;
																																																					BgL_cdrzd210480zd2_2751
																																																						=
																																																						CDR
																																																						(
																																																						((obj_t) BgL_cdrzd210470zd2_2750));
																																																					if ((CAR(((obj_t) BgL_cdrzd210470zd2_2750)) == CNST_TABLE_REF(12)))
																																																						{	/* Ast/private.scm 78 */
																																																							obj_t
																																																								BgL_cdrzd210490zd2_2754;
																																																							BgL_cdrzd210490zd2_2754
																																																								=
																																																								CDR
																																																								(
																																																								((obj_t) BgL_cdrzd210480zd2_2751));
																																																							{	/* Ast/private.scm 78 */
																																																								obj_t
																																																									BgL_cdrzd210500zd2_2755;
																																																								BgL_cdrzd210500zd2_2755
																																																									=
																																																									CDR
																																																									(
																																																									((obj_t) BgL_cdrzd210490zd2_2754));
																																																								{	/* Ast/private.scm 78 */
																																																									obj_t
																																																										BgL_cdrzd210509zd2_2756;
																																																									BgL_cdrzd210509zd2_2756
																																																										=
																																																										CDR
																																																										(
																																																										((obj_t) BgL_cdrzd210500zd2_2755));
																																																									{	/* Ast/private.scm 78 */
																																																										obj_t
																																																											BgL_carzd210516zd2_2757;
																																																										obj_t
																																																											BgL_cdrzd210517zd2_2758;
																																																										BgL_carzd210516zd2_2757
																																																											=
																																																											CAR
																																																											(
																																																											((obj_t) BgL_cdrzd210509zd2_2756));
																																																										BgL_cdrzd210517zd2_2758
																																																											=
																																																											CDR
																																																											(
																																																											((obj_t) BgL_cdrzd210509zd2_2756));
																																																										if (STRINGP(BgL_carzd210516zd2_2757))
																																																											{	/* Ast/private.scm 78 */
																																																												if (PAIRP(BgL_cdrzd210517zd2_2758))
																																																													{	/* Ast/private.scm 78 */
																																																														obj_t
																																																															BgL_carzd210524zd2_2761;
																																																														obj_t
																																																															BgL_cdrzd210525zd2_2762;
																																																														BgL_carzd210524zd2_2761
																																																															=
																																																															CAR
																																																															(BgL_cdrzd210517zd2_2758);
																																																														BgL_cdrzd210525zd2_2762
																																																															=
																																																															CDR
																																																															(BgL_cdrzd210517zd2_2758);
																																																														if (STRINGP(BgL_carzd210524zd2_2761))
																																																															{	/* Ast/private.scm 78 */
																																																																if (PAIRP(BgL_cdrzd210525zd2_2762))
																																																																	{	/* Ast/private.scm 78 */
																																																																		obj_t
																																																																			BgL_carzd210531zd2_2765;
																																																																		BgL_carzd210531zd2_2765
																																																																			=
																																																																			CAR
																																																																			(BgL_cdrzd210525zd2_2762);
																																																																		if (BOOLEANP(BgL_carzd210531zd2_2765))
																																																																			{	/* Ast/private.scm 78 */
																																																																				obj_t
																																																																					BgL_arg2457z00_2767;
																																																																				obj_t
																																																																					BgL_arg2458z00_2768;
																																																																				obj_t
																																																																					BgL_arg2459z00_2769;
																																																																				obj_t
																																																																					BgL_arg2460z00_2770;
																																																																				BgL_arg2457z00_2767
																																																																					=
																																																																					CAR
																																																																					(
																																																																					((obj_t) BgL_cdrzd210480zd2_2751));
																																																																				BgL_arg2458z00_2768
																																																																					=
																																																																					CAR
																																																																					(
																																																																					((obj_t) BgL_cdrzd210490zd2_2754));
																																																																				BgL_arg2459z00_2769
																																																																					=
																																																																					CAR
																																																																					(
																																																																					((obj_t) BgL_cdrzd210500zd2_2755));
																																																																				BgL_arg2460z00_2770
																																																																					=
																																																																					CDR
																																																																					(BgL_cdrzd210525zd2_2762);
																																																																				{
																																																																					BgL_vallocz00_bglt
																																																																						BgL_auxz00_4509;
																																																																					BgL_vtypez00_1445
																																																																						=
																																																																						BgL_arg2457z00_2767;
																																																																					BgL_ftypez00_1446
																																																																						=
																																																																						BgL_arg2458z00_2768;
																																																																					BgL_otypez00_1447
																																																																						=
																																																																						BgL_arg2459z00_2769;
																																																																					BgL_czd2heapzd2fmtz00_1448
																																																																						=
																																																																						BgL_carzd210516zd2_2757;
																																																																					BgL_czd2stackzd2fmtz00_1449
																																																																						=
																																																																						BgL_carzd210524zd2_2761;
																																																																					BgL_stackzf3zf3_1450
																																																																						=
																																																																						BgL_carzd210531zd2_2765;
																																																																					BgL_restz00_1451
																																																																						=
																																																																						BgL_arg2460z00_2770;
																																																																				BgL_tagzd2138zd2_1452:
																																																																					{	/* Ast/private.scm 202 */
																																																																						BgL_typez00_bglt
																																																																							BgL_ftypez00_2910;
																																																																						BgL_typez00_bglt
																																																																							BgL_vtypez00_2911;
																																																																						BgL_typez00_bglt
																																																																							BgL_otypez00_2912;
																																																																						BgL_ftypez00_2910
																																																																							=
																																																																							BGl_usezd2typez12zc0zztype_envz00
																																																																							(BgL_ftypez00_1446,
																																																																							BgL_locz00_8);
																																																																						BgL_vtypez00_2911
																																																																							=
																																																																							BGl_usezd2typez12zc0zztype_envz00
																																																																							(BgL_vtypez00_1445,
																																																																							BgL_locz00_8);
																																																																						BgL_otypez00_2912
																																																																							=
																																																																							BGl_usezd2typez12zc0zztype_envz00
																																																																							(BgL_otypez00_1447,
																																																																							BgL_locz00_8);
																																																																						{	/* Ast/private.scm 205 */
																																																																							BgL_vallocz00_bglt
																																																																								BgL_new1143z00_2913;
																																																																							{	/* Ast/private.scm 206 */
																																																																								BgL_vallocz00_bglt
																																																																									BgL_new1142z00_2914;
																																																																								BgL_new1142z00_2914
																																																																									=
																																																																									(
																																																																									(BgL_vallocz00_bglt)
																																																																									BOBJECT
																																																																									(GC_MALLOC
																																																																										(sizeof
																																																																											(struct
																																																																												BgL_vallocz00_bgl))));
																																																																								{	/* Ast/private.scm 206 */
																																																																									long
																																																																										BgL_arg2554z00_2915;
																																																																									BgL_arg2554z00_2915
																																																																										=
																																																																										BGL_CLASS_NUM
																																																																										(BGl_vallocz00zzast_nodez00);
																																																																									BGL_OBJECT_CLASS_NUM_SET
																																																																										(
																																																																										((BgL_objectz00_bglt) BgL_new1142z00_2914), BgL_arg2554z00_2915);
																																																																								}
																																																																								{	/* Ast/private.scm 206 */
																																																																									BgL_objectz00_bglt
																																																																										BgL_tmpz00_4517;
																																																																									BgL_tmpz00_4517
																																																																										=
																																																																										(
																																																																										(BgL_objectz00_bglt)
																																																																										BgL_new1142z00_2914);
																																																																									BGL_OBJECT_WIDENING_SET
																																																																										(BgL_tmpz00_4517,
																																																																										BFALSE);
																																																																								}
																																																																								((BgL_objectz00_bglt) BgL_new1142z00_2914);
																																																																								BgL_new1143z00_2913
																																																																									=
																																																																									BgL_new1142z00_2914;
																																																																							}
																																																																							((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1143z00_2913)))->BgL_locz00) = ((obj_t) BgL_locz00_8), BUNSPEC);
																																																																							((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1143z00_2913)))->BgL_typez00) = ((BgL_typez00_bglt) BgL_vtypez00_2911), BUNSPEC);
																																																																							((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1143z00_2913)))->BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
																																																																							((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1143z00_2913)))->BgL_keyz00) = ((obj_t) BINT(-1L)), BUNSPEC);
																																																																							((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt) BgL_new1143z00_2913)))->BgL_exprza2za2) = ((obj_t) BGl_sexpza2zd2ze3nodez93zzast_sexpz00(BgL_restz00_1451, BgL_stackz00_7, BgL_locz00_8, CNST_TABLE_REF(3))), BUNSPEC);
																																																																							((((BgL_externz00_bglt) COBJECT(((BgL_externz00_bglt) BgL_new1143z00_2913)))->BgL_effectz00) = ((obj_t) BUNSPEC), BUNSPEC);
																																																																							((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt) BgL_new1143z00_2913)))->BgL_czd2formatzd2) = ((obj_t) BgL_czd2heapzd2fmtz00_1448), BUNSPEC);
																																																																							((((BgL_vallocz00_bglt) COBJECT(BgL_new1143z00_2913))->BgL_ftypez00) = ((BgL_typez00_bglt) BgL_ftypez00_2910), BUNSPEC);
																																																																							((((BgL_vallocz00_bglt) COBJECT(BgL_new1143z00_2913))->BgL_otypez00) = ((BgL_typez00_bglt) BgL_otypez00_2912), BUNSPEC);
																																																																							BgL_auxz00_4509
																																																																								=
																																																																								BgL_new1143z00_2913;
																																																																					}}
																																																																					return
																																																																						(
																																																																						(obj_t)
																																																																						BgL_auxz00_4509);
																																																																				}
																																																																			}
																																																																		else
																																																																			{	/* Ast/private.scm 78 */
																																																																				goto
																																																																					BgL_tagzd2141zd2_1460;
																																																																			}
																																																																	}
																																																																else
																																																																	{	/* Ast/private.scm 78 */
																																																																		goto
																																																																			BgL_tagzd2141zd2_1460;
																																																																	}
																																																															}
																																																														else
																																																															{	/* Ast/private.scm 78 */
																																																																goto
																																																																	BgL_tagzd2141zd2_1460;
																																																															}
																																																													}
																																																												else
																																																													{	/* Ast/private.scm 78 */
																																																														goto
																																																															BgL_tagzd2141zd2_1460;
																																																													}
																																																											}
																																																										else
																																																											{	/* Ast/private.scm 78 */
																																																												goto
																																																													BgL_tagzd2141zd2_1460;
																																																											}
																																																									}
																																																								}
																																																							}
																																																						}
																																																					else
																																																						{	/* Ast/private.scm 78 */
																																																							goto
																																																								BgL_tagzd2141zd2_1460;
																																																						}
																																																				}
																																																			}
																																																	}
																																																}
																																															}
																																														}
																																													}
																																												else
																																													{	/* Ast/private.scm 78 */
																																														if ((BgL_carzd210414zd2_2716 == CNST_TABLE_REF(5)))
																																															{	/* Ast/private.scm 78 */
																																																goto
																																																	BgL_kapzd210416zd2_2718;
																																															}
																																														else
																																															{	/* Ast/private.scm 78 */
																																																obj_t
																																																	BgL_cdrzd210640zd2_2720;
																																																BgL_cdrzd210640zd2_2720
																																																	=
																																																	CDR
																																																	(
																																																	((obj_t) BgL_cdrzd210407zd2_2715));
																																																if ((CAR(((obj_t) BgL_cdrzd210407zd2_2715)) == CNST_TABLE_REF(12)))
																																																	{	/* Ast/private.scm 78 */
																																																		obj_t
																																																			BgL_cdrzd210650zd2_2723;
																																																		BgL_cdrzd210650zd2_2723
																																																			=
																																																			CDR
																																																			(
																																																			((obj_t) BgL_cdrzd210640zd2_2720));
																																																		{	/* Ast/private.scm 78 */
																																																			obj_t
																																																				BgL_cdrzd210660zd2_2724;
																																																			BgL_cdrzd210660zd2_2724
																																																				=
																																																				CDR
																																																				(
																																																				((obj_t) BgL_cdrzd210650zd2_2723));
																																																			{	/* Ast/private.scm 78 */
																																																				obj_t
																																																					BgL_cdrzd210669zd2_2725;
																																																				BgL_cdrzd210669zd2_2725
																																																					=
																																																					CDR
																																																					(
																																																					((obj_t) BgL_cdrzd210660zd2_2724));
																																																				{	/* Ast/private.scm 78 */
																																																					obj_t
																																																						BgL_carzd210676zd2_2726;
																																																					obj_t
																																																						BgL_cdrzd210677zd2_2727;
																																																					BgL_carzd210676zd2_2726
																																																						=
																																																						CAR
																																																						(
																																																						((obj_t) BgL_cdrzd210669zd2_2725));
																																																					BgL_cdrzd210677zd2_2727
																																																						=
																																																						CDR
																																																						(
																																																						((obj_t) BgL_cdrzd210669zd2_2725));
																																																					if (STRINGP(BgL_carzd210676zd2_2726))
																																																						{	/* Ast/private.scm 78 */
																																																							if (PAIRP(BgL_cdrzd210677zd2_2727))
																																																								{	/* Ast/private.scm 78 */
																																																									obj_t
																																																										BgL_carzd210684zd2_2730;
																																																									obj_t
																																																										BgL_cdrzd210685zd2_2731;
																																																									BgL_carzd210684zd2_2730
																																																										=
																																																										CAR
																																																										(BgL_cdrzd210677zd2_2727);
																																																									BgL_cdrzd210685zd2_2731
																																																										=
																																																										CDR
																																																										(BgL_cdrzd210677zd2_2727);
																																																									if (STRINGP(BgL_carzd210684zd2_2730))
																																																										{	/* Ast/private.scm 78 */
																																																											if (PAIRP(BgL_cdrzd210685zd2_2731))
																																																												{	/* Ast/private.scm 78 */
																																																													obj_t
																																																														BgL_carzd210691zd2_2734;
																																																													BgL_carzd210691zd2_2734
																																																														=
																																																														CAR
																																																														(BgL_cdrzd210685zd2_2731);
																																																													if (BOOLEANP(BgL_carzd210691zd2_2734))
																																																														{	/* Ast/private.scm 78 */
																																																															obj_t
																																																																BgL_arg2437z00_2736;
																																																															obj_t
																																																																BgL_arg2438z00_2737;
																																																															obj_t
																																																																BgL_arg2439z00_2738;
																																																															obj_t
																																																																BgL_arg2442z00_2739;
																																																															BgL_arg2437z00_2736
																																																																=
																																																																CAR
																																																																(
																																																																((obj_t) BgL_cdrzd210640zd2_2720));
																																																															BgL_arg2438z00_2737
																																																																=
																																																																CAR
																																																																(
																																																																((obj_t) BgL_cdrzd210650zd2_2723));
																																																															BgL_arg2439z00_2738
																																																																=
																																																																CAR
																																																																(
																																																																((obj_t) BgL_cdrzd210660zd2_2724));
																																																															BgL_arg2442z00_2739
																																																																=
																																																																CDR
																																																																(BgL_cdrzd210685zd2_2731);
																																																															{
																																																																BgL_vallocz00_bglt
																																																																	BgL_auxz00_4581;
																																																																{
																																																																	obj_t
																																																																		BgL_restz00_4588;
																																																																	obj_t
																																																																		BgL_stackzf3zf3_4587;
																																																																	obj_t
																																																																		BgL_czd2stackzd2fmtz00_4586;
																																																																	obj_t
																																																																		BgL_czd2heapzd2fmtz00_4585;
																																																																	obj_t
																																																																		BgL_otypez00_4584;
																																																																	obj_t
																																																																		BgL_ftypez00_4583;
																																																																	obj_t
																																																																		BgL_vtypez00_4582;
																																																																	BgL_vtypez00_4582
																																																																		=
																																																																		BgL_arg2437z00_2736;
																																																																	BgL_ftypez00_4583
																																																																		=
																																																																		BgL_arg2438z00_2737;
																																																																	BgL_otypez00_4584
																																																																		=
																																																																		BgL_arg2439z00_2738;
																																																																	BgL_czd2heapzd2fmtz00_4585
																																																																		=
																																																																		BgL_carzd210676zd2_2726;
																																																																	BgL_czd2stackzd2fmtz00_4586
																																																																		=
																																																																		BgL_carzd210684zd2_2730;
																																																																	BgL_stackzf3zf3_4587
																																																																		=
																																																																		BgL_carzd210691zd2_2734;
																																																																	BgL_restz00_4588
																																																																		=
																																																																		BgL_arg2442z00_2739;
																																																																	BgL_restz00_1451
																																																																		=
																																																																		BgL_restz00_4588;
																																																																	BgL_stackzf3zf3_1450
																																																																		=
																																																																		BgL_stackzf3zf3_4587;
																																																																	BgL_czd2stackzd2fmtz00_1449
																																																																		=
																																																																		BgL_czd2stackzd2fmtz00_4586;
																																																																	BgL_czd2heapzd2fmtz00_1448
																																																																		=
																																																																		BgL_czd2heapzd2fmtz00_4585;
																																																																	BgL_otypez00_1447
																																																																		=
																																																																		BgL_otypez00_4584;
																																																																	BgL_ftypez00_1446
																																																																		=
																																																																		BgL_ftypez00_4583;
																																																																	BgL_vtypez00_1445
																																																																		=
																																																																		BgL_vtypez00_4582;
																																																																	goto
																																																																		BgL_tagzd2138zd2_1452;
																																																																}
																																																																return
																																																																	(
																																																																	(obj_t)
																																																																	BgL_auxz00_4581);
																																																															}
																																																														}
																																																													else
																																																														{	/* Ast/private.scm 78 */
																																																															goto
																																																																BgL_tagzd2141zd2_1460;
																																																														}
																																																												}
																																																											else
																																																												{	/* Ast/private.scm 78 */
																																																													goto
																																																														BgL_tagzd2141zd2_1460;
																																																												}
																																																										}
																																																									else
																																																										{	/* Ast/private.scm 78 */
																																																											goto
																																																												BgL_tagzd2141zd2_1460;
																																																										}
																																																								}
																																																							else
																																																								{	/* Ast/private.scm 78 */
																																																									goto
																																																										BgL_tagzd2141zd2_1460;
																																																								}
																																																						}
																																																					else
																																																						{	/* Ast/private.scm 78 */
																																																							goto
																																																								BgL_tagzd2141zd2_1460;
																																																						}
																																																				}
																																																			}
																																																		}
																																																	}
																																																else
																																																	{	/* Ast/private.scm 78 */
																																																		goto
																																																			BgL_tagzd2141zd2_1460;
																																																	}
																																															}
																																													}
																																											}
																																										}
																																									}
																																							}
																																						else
																																							{	/* Ast/private.scm 78 */
																																								obj_t
																																									BgL_cdrzd210821zd2_2772;
																																								BgL_cdrzd210821zd2_2772
																																									=
																																									CDR
																																									(BgL_sexpz00_6);
																																								{	/* Ast/private.scm 78 */
																																									obj_t
																																										BgL_cdrzd210827zd2_2773;
																																									BgL_cdrzd210827zd2_2773
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_cdrzd210821zd2_2772));
																																									if ((CAR(((obj_t) BgL_cdrzd210821zd2_2772)) == CNST_TABLE_REF(14)))
																																										{	/* Ast/private.scm 78 */
																																											obj_t
																																												BgL_cdrzd210833zd2_2776;
																																											BgL_cdrzd210833zd2_2776
																																												=
																																												CDR
																																												(
																																												((obj_t) BgL_cdrzd210827zd2_2773));
																																											{	/* Ast/private.scm 78 */
																																												obj_t
																																													BgL_cdrzd210839zd2_2777;
																																												BgL_cdrzd210839zd2_2777
																																													=
																																													CDR
																																													(
																																													((obj_t) BgL_cdrzd210833zd2_2776));
																																												if (NULLP(CDR(((obj_t) BgL_cdrzd210839zd2_2777))))
																																													{	/* Ast/private.scm 78 */
																																														obj_t
																																															BgL_arg2469z00_2780;
																																														obj_t
																																															BgL_arg2470z00_2781;
																																														obj_t
																																															BgL_arg2471z00_2782;
																																														BgL_arg2469z00_2780
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_cdrzd210827zd2_2773));
																																														BgL_arg2470z00_2781
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_cdrzd210833zd2_2776));
																																														BgL_arg2471z00_2782
																																															=
																																															CAR
																																															(
																																															((obj_t) BgL_cdrzd210839zd2_2777));
																																														{
																																															BgL_sequencez00_bglt
																																																BgL_auxz00_4612;
																																															BgL_typez00_1456
																																																=
																																																BgL_arg2469z00_2780;
																																															BgL_kwdsz00_1457
																																																=
																																																BgL_arg2470z00_2781;
																																															BgL_bodyz00_1458
																																																=
																																																BgL_arg2471z00_2782;
																																														BgL_tagzd2140zd2_1459:
																																															{	/* Ast/private.scm 219 */
																																																BgL_sequencez00_bglt
																																																	BgL_new1147z00_2921;
																																																{	/* Ast/private.scm 220 */
																																																	BgL_sequencez00_bglt
																																																		BgL_new1146z00_2922;
																																																	BgL_new1146z00_2922
																																																		=
																																																		(
																																																		(BgL_sequencez00_bglt)
																																																		BOBJECT
																																																		(GC_MALLOC
																																																			(sizeof
																																																				(struct
																																																					BgL_sequencez00_bgl))));
																																																	{	/* Ast/private.scm 220 */
																																																		long
																																																			BgL_arg2560z00_2923;
																																																		BgL_arg2560z00_2923
																																																			=
																																																			BGL_CLASS_NUM
																																																			(BGl_sequencez00zzast_nodez00);
																																																		BGL_OBJECT_CLASS_NUM_SET
																																																			(
																																																			((BgL_objectz00_bglt) BgL_new1146z00_2922), BgL_arg2560z00_2923);
																																																	}
																																																	{	/* Ast/private.scm 220 */
																																																		BgL_objectz00_bglt
																																																			BgL_tmpz00_4617;
																																																		BgL_tmpz00_4617
																																																			=
																																																			(
																																																			(BgL_objectz00_bglt)
																																																			BgL_new1146z00_2922);
																																																		BGL_OBJECT_WIDENING_SET
																																																			(BgL_tmpz00_4617,
																																																			BFALSE);
																																																	}
																																																	((BgL_objectz00_bglt) BgL_new1146z00_2922);
																																																	BgL_new1147z00_2921
																																																		=
																																																		BgL_new1146z00_2922;
																																																}
																																																((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1147z00_2921)))->BgL_locz00) = ((obj_t) BgL_locz00_8), BUNSPEC);
																																																((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1147z00_2921)))->BgL_typez00) = ((BgL_typez00_bglt) BGl_usezd2typez12zc0zztype_envz00(BgL_typez00_1456, BgL_locz00_8)), BUNSPEC);
																																																((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1147z00_2921)))->BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
																																																((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1147z00_2921)))->BgL_keyz00) = ((obj_t) BINT(-1L)), BUNSPEC);
																																																((((BgL_sequencez00_bglt) COBJECT(BgL_new1147z00_2921))->BgL_nodesz00) = ((obj_t) BGl_sexpza2zd2ze3nodez93zzast_sexpz00(BgL_bodyz00_1458, BgL_stackz00_7, BgL_locz00_8, CNST_TABLE_REF(3))), BUNSPEC);
																																																((((BgL_sequencez00_bglt) COBJECT(BgL_new1147z00_2921))->BgL_unsafez00) = ((bool_t) ((bool_t) 0)), BUNSPEC);
																																																((((BgL_sequencez00_bglt) COBJECT(BgL_new1147z00_2921))->BgL_metaz00) = ((obj_t) BgL_kwdsz00_1457), BUNSPEC);
																																																BgL_auxz00_4612
																																																	=
																																																	BgL_new1147z00_2921;
																																															}
																																															return
																																																(
																																																(obj_t)
																																																BgL_auxz00_4612);
																																														}
																																													}
																																												else
																																													{	/* Ast/private.scm 78 */
																																														goto
																																															BgL_tagzd2141zd2_1460;
																																													}
																																											}
																																										}
																																									else
																																										{	/* Ast/private.scm 78 */
																																											goto
																																												BgL_tagzd2141zd2_1460;
																																										}
																																								}
																																							}
																																					}
																																				else
																																					{	/* Ast/private.scm 78 */
																																						goto
																																							BgL_tagzd2141zd2_1460;
																																					}
																																			}
																																		}
																																	}
																																else
																																	{	/* Ast/private.scm 78 */
																																		if (
																																			(BgL_carzd210351zd2_2613
																																				==
																																				CNST_TABLE_REF
																																				(4)))
																																			{	/* Ast/private.scm 78 */
																																				goto
																																					BgL_kapzd210353zd2_2615;
																																			}
																																		else
																																			{	/* Ast/private.scm 78 */
																																				obj_t
																																					BgL_cdrzd210914zd2_2616;
																																				BgL_cdrzd210914zd2_2616
																																					=
																																					CDR
																																					(BgL_sexpz00_6);
																																				{	/* Ast/private.scm 78 */
																																					obj_t
																																						BgL_carzd210922zd2_2617;
																																					obj_t
																																						BgL_cdrzd210923zd2_2618;
																																					BgL_carzd210922zd2_2617
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd210914zd2_2616));
																																					BgL_cdrzd210923zd2_2618
																																						=
																																						CDR(
																																						((obj_t) BgL_cdrzd210914zd2_2616));
																																					{

																																						if (
																																							(BgL_carzd210922zd2_2617
																																								==
																																								CNST_TABLE_REF
																																								(13)))
																																							{	/* Ast/private.scm 78 */
																																							BgL_kapzd210924zd2_2619:
																																								{	/* Ast/private.scm 78 */
																																									obj_t
																																										BgL_cdrzd210933zd2_2658;
																																									BgL_cdrzd210933zd2_2658
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_cdrzd210923zd2_2618));
																																									{	/* Ast/private.scm 78 */
																																										obj_t
																																											BgL_cdrzd210942zd2_2659;
																																										BgL_cdrzd210942zd2_2659
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_cdrzd210933zd2_2658));
																																										if (PAIRP(BgL_cdrzd210942zd2_2659))
																																											{	/* Ast/private.scm 78 */
																																												obj_t
																																													BgL_cdrzd210949zd2_2661;
																																												BgL_cdrzd210949zd2_2661
																																													=
																																													CDR
																																													(BgL_cdrzd210942zd2_2659);
																																												if (PAIRP(BgL_cdrzd210949zd2_2661))
																																													{	/* Ast/private.scm 78 */
																																														obj_t
																																															BgL_carzd210953zd2_2663;
																																														BgL_carzd210953zd2_2663
																																															=
																																															CAR
																																															(BgL_cdrzd210949zd2_2661);
																																														if (STRINGP(BgL_carzd210953zd2_2663))
																																															{	/* Ast/private.scm 78 */
																																																obj_t
																																																	BgL_arg2393z00_2665;
																																																obj_t
																																																	BgL_arg2395z00_2666;
																																																obj_t
																																																	BgL_arg2396z00_2667;
																																																obj_t
																																																	BgL_arg2397z00_2668;
																																																BgL_arg2393z00_2665
																																																	=
																																																	CAR
																																																	(
																																																	((obj_t) BgL_cdrzd210923zd2_2618));
																																																BgL_arg2395z00_2666
																																																	=
																																																	CAR
																																																	(
																																																	((obj_t) BgL_cdrzd210933zd2_2658));
																																																BgL_arg2396z00_2667
																																																	=
																																																	CAR
																																																	(BgL_cdrzd210942zd2_2659);
																																																BgL_arg2397z00_2668
																																																	=
																																																	CDR
																																																	(BgL_cdrzd210949zd2_2661);
																																																{
																																																	BgL_vsetz12z12_bglt
																																																		BgL_auxz00_4666;
																																																	{
																																																		obj_t
																																																			BgL_restz00_4671;
																																																		obj_t
																																																			BgL_czd2fmtzd2_4670;
																																																		obj_t
																																																			BgL_otypez00_4669;
																																																		obj_t
																																																			BgL_ftypez00_4668;
																																																		obj_t
																																																			BgL_vtypez00_4667;
																																																		BgL_vtypez00_4667
																																																			=
																																																			BgL_arg2393z00_2665;
																																																		BgL_ftypez00_4668
																																																			=
																																																			BgL_arg2395z00_2666;
																																																		BgL_otypez00_4669
																																																			=
																																																			BgL_arg2396z00_2667;
																																																		BgL_czd2fmtzd2_4670
																																																			=
																																																			BgL_carzd210953zd2_2663;
																																																		BgL_restz00_4671
																																																			=
																																																			BgL_arg2397z00_2668;
																																																		BgL_restz00_1443
																																																			=
																																																			BgL_restz00_4671;
																																																		BgL_czd2fmtzd2_1442
																																																			=
																																																			BgL_czd2fmtzd2_4670;
																																																		BgL_otypez00_1441
																																																			=
																																																			BgL_otypez00_4669;
																																																		BgL_ftypez00_1440
																																																			=
																																																			BgL_ftypez00_4668;
																																																		BgL_vtypez00_1439
																																																			=
																																																			BgL_vtypez00_4667;
																																																		goto
																																																			BgL_tagzd2137zd2_1444;
																																																	}
																																																	return
																																																		(
																																																		(obj_t)
																																																		BgL_auxz00_4666);
																																																}
																																															}
																																														else
																																															{	/* Ast/private.scm 78 */
																																																obj_t
																																																	BgL_cdrzd210979zd2_2669;
																																																BgL_cdrzd210979zd2_2669
																																																	=
																																																	CDR
																																																	(BgL_sexpz00_6);
																																																{	/* Ast/private.scm 78 */
																																																	obj_t
																																																		BgL_cdrzd210989zd2_2670;
																																																	BgL_cdrzd210989zd2_2670
																																																		=
																																																		CDR
																																																		(
																																																		((obj_t) BgL_cdrzd210979zd2_2669));
																																																	if ((CAR(((obj_t) BgL_cdrzd210979zd2_2669)) == CNST_TABLE_REF(12)))
																																																		{	/* Ast/private.scm 78 */
																																																			obj_t
																																																				BgL_cdrzd210999zd2_2673;
																																																			BgL_cdrzd210999zd2_2673
																																																				=
																																																				CDR
																																																				(
																																																				((obj_t) BgL_cdrzd210989zd2_2670));
																																																			{	/* Ast/private.scm 78 */
																																																				obj_t
																																																					BgL_cdrzd211009zd2_2674;
																																																				BgL_cdrzd211009zd2_2674
																																																					=
																																																					CDR
																																																					(
																																																					((obj_t) BgL_cdrzd210999zd2_2673));
																																																				{	/* Ast/private.scm 78 */
																																																					obj_t
																																																						BgL_cdrzd211018zd2_2675;
																																																					BgL_cdrzd211018zd2_2675
																																																						=
																																																						CDR
																																																						(
																																																						((obj_t) BgL_cdrzd211009zd2_2674));
																																																					{	/* Ast/private.scm 78 */
																																																						obj_t
																																																							BgL_carzd211025zd2_2676;
																																																						obj_t
																																																							BgL_cdrzd211026zd2_2677;
																																																						BgL_carzd211025zd2_2676
																																																							=
																																																							CAR
																																																							(
																																																							((obj_t) BgL_cdrzd211018zd2_2675));
																																																						BgL_cdrzd211026zd2_2677
																																																							=
																																																							CDR
																																																							(
																																																							((obj_t) BgL_cdrzd211018zd2_2675));
																																																						if (STRINGP(BgL_carzd211025zd2_2676))
																																																							{	/* Ast/private.scm 78 */
																																																								if (PAIRP(BgL_cdrzd211026zd2_2677))
																																																									{	/* Ast/private.scm 78 */
																																																										obj_t
																																																											BgL_carzd211033zd2_2680;
																																																										obj_t
																																																											BgL_cdrzd211034zd2_2681;
																																																										BgL_carzd211033zd2_2680
																																																											=
																																																											CAR
																																																											(BgL_cdrzd211026zd2_2677);
																																																										BgL_cdrzd211034zd2_2681
																																																											=
																																																											CDR
																																																											(BgL_cdrzd211026zd2_2677);
																																																										if (STRINGP(BgL_carzd211033zd2_2680))
																																																											{	/* Ast/private.scm 78 */
																																																												if (PAIRP(BgL_cdrzd211034zd2_2681))
																																																													{	/* Ast/private.scm 78 */
																																																														obj_t
																																																															BgL_carzd211040zd2_2684;
																																																														BgL_carzd211040zd2_2684
																																																															=
																																																															CAR
																																																															(BgL_cdrzd211034zd2_2681);
																																																														if (BOOLEANP(BgL_carzd211040zd2_2684))
																																																															{	/* Ast/private.scm 78 */
																																																																obj_t
																																																																	BgL_arg2405z00_2686;
																																																																obj_t
																																																																	BgL_arg2407z00_2687;
																																																																obj_t
																																																																	BgL_arg2408z00_2688;
																																																																obj_t
																																																																	BgL_arg2410z00_2689;
																																																																BgL_arg2405z00_2686
																																																																	=
																																																																	CAR
																																																																	(
																																																																	((obj_t) BgL_cdrzd210989zd2_2670));
																																																																BgL_arg2407z00_2687
																																																																	=
																																																																	CAR
																																																																	(
																																																																	((obj_t) BgL_cdrzd210999zd2_2673));
																																																																BgL_arg2408z00_2688
																																																																	=
																																																																	CAR
																																																																	(
																																																																	((obj_t) BgL_cdrzd211009zd2_2674));
																																																																BgL_arg2410z00_2689
																																																																	=
																																																																	CDR
																																																																	(BgL_cdrzd211034zd2_2681);
																																																																{
																																																																	BgL_vallocz00_bglt
																																																																		BgL_auxz00_4711;
																																																																	{
																																																																		obj_t
																																																																			BgL_restz00_4718;
																																																																		obj_t
																																																																			BgL_stackzf3zf3_4717;
																																																																		obj_t
																																																																			BgL_czd2stackzd2fmtz00_4716;
																																																																		obj_t
																																																																			BgL_czd2heapzd2fmtz00_4715;
																																																																		obj_t
																																																																			BgL_otypez00_4714;
																																																																		obj_t
																																																																			BgL_ftypez00_4713;
																																																																		obj_t
																																																																			BgL_vtypez00_4712;
																																																																		BgL_vtypez00_4712
																																																																			=
																																																																			BgL_arg2405z00_2686;
																																																																		BgL_ftypez00_4713
																																																																			=
																																																																			BgL_arg2407z00_2687;
																																																																		BgL_otypez00_4714
																																																																			=
																																																																			BgL_arg2408z00_2688;
																																																																		BgL_czd2heapzd2fmtz00_4715
																																																																			=
																																																																			BgL_carzd211025zd2_2676;
																																																																		BgL_czd2stackzd2fmtz00_4716
																																																																			=
																																																																			BgL_carzd211033zd2_2680;
																																																																		BgL_stackzf3zf3_4717
																																																																			=
																																																																			BgL_carzd211040zd2_2684;
																																																																		BgL_restz00_4718
																																																																			=
																																																																			BgL_arg2410z00_2689;
																																																																		BgL_restz00_1451
																																																																			=
																																																																			BgL_restz00_4718;
																																																																		BgL_stackzf3zf3_1450
																																																																			=
																																																																			BgL_stackzf3zf3_4717;
																																																																		BgL_czd2stackzd2fmtz00_1449
																																																																			=
																																																																			BgL_czd2stackzd2fmtz00_4716;
																																																																		BgL_czd2heapzd2fmtz00_1448
																																																																			=
																																																																			BgL_czd2heapzd2fmtz00_4715;
																																																																		BgL_otypez00_1447
																																																																			=
																																																																			BgL_otypez00_4714;
																																																																		BgL_ftypez00_1446
																																																																			=
																																																																			BgL_ftypez00_4713;
																																																																		BgL_vtypez00_1445
																																																																			=
																																																																			BgL_vtypez00_4712;
																																																																		goto
																																																																			BgL_tagzd2138zd2_1452;
																																																																	}
																																																																	return
																																																																		(
																																																																		(obj_t)
																																																																		BgL_auxz00_4711);
																																																																}
																																																															}
																																																														else
																																																															{	/* Ast/private.scm 78 */
																																																																goto
																																																																	BgL_tagzd2141zd2_1460;
																																																															}
																																																													}
																																																												else
																																																													{	/* Ast/private.scm 78 */
																																																														goto
																																																															BgL_tagzd2141zd2_1460;
																																																													}
																																																											}
																																																										else
																																																											{	/* Ast/private.scm 78 */
																																																												goto
																																																													BgL_tagzd2141zd2_1460;
																																																											}
																																																									}
																																																								else
																																																									{	/* Ast/private.scm 78 */
																																																										goto
																																																											BgL_tagzd2141zd2_1460;
																																																									}
																																																							}
																																																						else
																																																							{	/* Ast/private.scm 78 */
																																																								goto
																																																									BgL_tagzd2141zd2_1460;
																																																							}
																																																					}
																																																				}
																																																			}
																																																		}
																																																	else
																																																		{	/* Ast/private.scm 78 */
																																																			goto
																																																				BgL_tagzd2141zd2_1460;
																																																		}
																																																}
																																															}
																																													}
																																												else
																																													{	/* Ast/private.scm 78 */
																																														obj_t
																																															BgL_cdrzd211152zd2_2691;
																																														BgL_cdrzd211152zd2_2691
																																															=
																																															CDR
																																															(BgL_sexpz00_6);
																																														{	/* Ast/private.scm 78 */
																																															obj_t
																																																BgL_cdrzd211158zd2_2692;
																																															BgL_cdrzd211158zd2_2692
																																																=
																																																CDR
																																																(
																																																((obj_t) BgL_cdrzd211152zd2_2691));
																																															if ((CAR(((obj_t) BgL_cdrzd211152zd2_2691)) == CNST_TABLE_REF(14)))
																																																{	/* Ast/private.scm 78 */
																																																	obj_t
																																																		BgL_cdrzd211164zd2_2695;
																																																	BgL_cdrzd211164zd2_2695
																																																		=
																																																		CDR
																																																		(
																																																		((obj_t) BgL_cdrzd211158zd2_2692));
																																																	{	/* Ast/private.scm 78 */
																																																		obj_t
																																																			BgL_cdrzd211170zd2_2696;
																																																		BgL_cdrzd211170zd2_2696
																																																			=
																																																			CDR
																																																			(
																																																			((obj_t) BgL_cdrzd211164zd2_2695));
																																																		if (NULLP(CDR(((obj_t) BgL_cdrzd211170zd2_2696))))
																																																			{	/* Ast/private.scm 78 */
																																																				obj_t
																																																					BgL_arg2417z00_2699;
																																																				obj_t
																																																					BgL_arg2418z00_2700;
																																																				obj_t
																																																					BgL_arg2419z00_2701;
																																																				BgL_arg2417z00_2699
																																																					=
																																																					CAR
																																																					(
																																																					((obj_t) BgL_cdrzd211158zd2_2692));
																																																				BgL_arg2418z00_2700
																																																					=
																																																					CAR
																																																					(
																																																					((obj_t) BgL_cdrzd211164zd2_2695));
																																																				BgL_arg2419z00_2701
																																																					=
																																																					CAR
																																																					(
																																																					((obj_t) BgL_cdrzd211170zd2_2696));
																																																				{
																																																					BgL_sequencez00_bglt
																																																						BgL_auxz00_4742;
																																																					{
																																																						obj_t
																																																							BgL_bodyz00_4745;
																																																						obj_t
																																																							BgL_kwdsz00_4744;
																																																						obj_t
																																																							BgL_typez00_4743;
																																																						BgL_typez00_4743
																																																							=
																																																							BgL_arg2417z00_2699;
																																																						BgL_kwdsz00_4744
																																																							=
																																																							BgL_arg2418z00_2700;
																																																						BgL_bodyz00_4745
																																																							=
																																																							BgL_arg2419z00_2701;
																																																						BgL_bodyz00_1458
																																																							=
																																																							BgL_bodyz00_4745;
																																																						BgL_kwdsz00_1457
																																																							=
																																																							BgL_kwdsz00_4744;
																																																						BgL_typez00_1456
																																																							=
																																																							BgL_typez00_4743;
																																																						goto
																																																							BgL_tagzd2140zd2_1459;
																																																					}
																																																					return
																																																						(
																																																						(obj_t)
																																																						BgL_auxz00_4742);
																																																				}
																																																			}
																																																		else
																																																			{	/* Ast/private.scm 78 */
																																																				goto
																																																					BgL_tagzd2141zd2_1460;
																																																			}
																																																	}
																																																}
																																															else
																																																{	/* Ast/private.scm 78 */
																																																	goto
																																																		BgL_tagzd2141zd2_1460;
																																																}
																																														}
																																													}
																																											}
																																										else
																																											{	/* Ast/private.scm 78 */
																																												goto
																																													BgL_tagzd2141zd2_1460;
																																											}
																																									}
																																								}
																																							}
																																						else
																																							{	/* Ast/private.scm 78 */
																																								if ((BgL_carzd210922zd2_2617 == CNST_TABLE_REF(5)))
																																									{	/* Ast/private.scm 78 */
																																										goto
																																											BgL_kapzd210924zd2_2619;
																																									}
																																								else
																																									{	/* Ast/private.scm 78 */
																																										obj_t
																																											BgL_cdrzd211240zd2_2621;
																																										BgL_cdrzd211240zd2_2621
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_cdrzd210914zd2_2616));
																																										if ((CAR(((obj_t) BgL_cdrzd210914zd2_2616)) == CNST_TABLE_REF(12)))
																																											{	/* Ast/private.scm 78 */
																																												obj_t
																																													BgL_cdrzd211251zd2_2624;
																																												BgL_cdrzd211251zd2_2624
																																													=
																																													CDR
																																													(
																																													((obj_t) BgL_cdrzd211240zd2_2621));
																																												{	/* Ast/private.scm 78 */
																																													obj_t
																																														BgL_cdrzd211262zd2_2625;
																																													BgL_cdrzd211262zd2_2625
																																														=
																																														CDR
																																														(
																																														((obj_t) BgL_cdrzd211251zd2_2624));
																																													if (PAIRP(BgL_cdrzd211262zd2_2625))
																																														{	/* Ast/private.scm 78 */
																																															obj_t
																																																BgL_cdrzd211271zd2_2627;
																																															BgL_cdrzd211271zd2_2627
																																																=
																																																CDR
																																																(BgL_cdrzd211262zd2_2625);
																																															if (PAIRP(BgL_cdrzd211271zd2_2627))
																																																{	/* Ast/private.scm 78 */
																																																	obj_t
																																																		BgL_carzd211277zd2_2629;
																																																	obj_t
																																																		BgL_cdrzd211278zd2_2630;
																																																	BgL_carzd211277zd2_2629
																																																		=
																																																		CAR
																																																		(BgL_cdrzd211271zd2_2627);
																																																	BgL_cdrzd211278zd2_2630
																																																		=
																																																		CDR
																																																		(BgL_cdrzd211271zd2_2627);
																																																	if (STRINGP(BgL_carzd211277zd2_2629))
																																																		{	/* Ast/private.scm 78 */
																																																			if (PAIRP(BgL_cdrzd211278zd2_2630))
																																																				{	/* Ast/private.scm 78 */
																																																					obj_t
																																																						BgL_carzd211285zd2_2633;
																																																					obj_t
																																																						BgL_cdrzd211286zd2_2634;
																																																					BgL_carzd211285zd2_2633
																																																						=
																																																						CAR
																																																						(BgL_cdrzd211278zd2_2630);
																																																					BgL_cdrzd211286zd2_2634
																																																						=
																																																						CDR
																																																						(BgL_cdrzd211278zd2_2630);
																																																					if (STRINGP(BgL_carzd211285zd2_2633))
																																																						{	/* Ast/private.scm 78 */
																																																							if (PAIRP(BgL_cdrzd211286zd2_2634))
																																																								{	/* Ast/private.scm 78 */
																																																									obj_t
																																																										BgL_carzd211292zd2_2637;
																																																									BgL_carzd211292zd2_2637
																																																										=
																																																										CAR
																																																										(BgL_cdrzd211286zd2_2634);
																																																									if (BOOLEANP(BgL_carzd211292zd2_2637))
																																																										{	/* Ast/private.scm 78 */
																																																											obj_t
																																																												BgL_arg2375z00_2639;
																																																											obj_t
																																																												BgL_arg2376z00_2640;
																																																											obj_t
																																																												BgL_arg2377z00_2641;
																																																											obj_t
																																																												BgL_arg2378z00_2642;
																																																											BgL_arg2375z00_2639
																																																												=
																																																												CAR
																																																												(
																																																												((obj_t) BgL_cdrzd211240zd2_2621));
																																																											BgL_arg2376z00_2640
																																																												=
																																																												CAR
																																																												(
																																																												((obj_t) BgL_cdrzd211251zd2_2624));
																																																											BgL_arg2377z00_2641
																																																												=
																																																												CAR
																																																												(BgL_cdrzd211262zd2_2625);
																																																											BgL_arg2378z00_2642
																																																												=
																																																												CDR
																																																												(BgL_cdrzd211286zd2_2634);
																																																											{
																																																												BgL_vallocz00_bglt
																																																													BgL_auxz00_4787;
																																																												{
																																																													obj_t
																																																														BgL_restz00_4794;
																																																													obj_t
																																																														BgL_stackzf3zf3_4793;
																																																													obj_t
																																																														BgL_czd2stackzd2fmtz00_4792;
																																																													obj_t
																																																														BgL_czd2heapzd2fmtz00_4791;
																																																													obj_t
																																																														BgL_otypez00_4790;
																																																													obj_t
																																																														BgL_ftypez00_4789;
																																																													obj_t
																																																														BgL_vtypez00_4788;
																																																													BgL_vtypez00_4788
																																																														=
																																																														BgL_arg2375z00_2639;
																																																													BgL_ftypez00_4789
																																																														=
																																																														BgL_arg2376z00_2640;
																																																													BgL_otypez00_4790
																																																														=
																																																														BgL_arg2377z00_2641;
																																																													BgL_czd2heapzd2fmtz00_4791
																																																														=
																																																														BgL_carzd211277zd2_2629;
																																																													BgL_czd2stackzd2fmtz00_4792
																																																														=
																																																														BgL_carzd211285zd2_2633;
																																																													BgL_stackzf3zf3_4793
																																																														=
																																																														BgL_carzd211292zd2_2637;
																																																													BgL_restz00_4794
																																																														=
																																																														BgL_arg2378z00_2642;
																																																													BgL_restz00_1451
																																																														=
																																																														BgL_restz00_4794;
																																																													BgL_stackzf3zf3_1450
																																																														=
																																																														BgL_stackzf3zf3_4793;
																																																													BgL_czd2stackzd2fmtz00_1449
																																																														=
																																																														BgL_czd2stackzd2fmtz00_4792;
																																																													BgL_czd2heapzd2fmtz00_1448
																																																														=
																																																														BgL_czd2heapzd2fmtz00_4791;
																																																													BgL_otypez00_1447
																																																														=
																																																														BgL_otypez00_4790;
																																																													BgL_ftypez00_1446
																																																														=
																																																														BgL_ftypez00_4789;
																																																													BgL_vtypez00_1445
																																																														=
																																																														BgL_vtypez00_4788;
																																																													goto
																																																														BgL_tagzd2138zd2_1452;
																																																												}
																																																												return
																																																													(
																																																													(obj_t)
																																																													BgL_auxz00_4787);
																																																											}
																																																										}
																																																									else
																																																										{	/* Ast/private.scm 78 */
																																																											goto
																																																												BgL_tagzd2141zd2_1460;
																																																										}
																																																								}
																																																							else
																																																								{	/* Ast/private.scm 78 */
																																																									goto
																																																										BgL_tagzd2141zd2_1460;
																																																								}
																																																						}
																																																					else
																																																						{	/* Ast/private.scm 78 */
																																																							goto
																																																								BgL_tagzd2141zd2_1460;
																																																						}
																																																				}
																																																			else
																																																				{	/* Ast/private.scm 78 */
																																																					goto
																																																						BgL_tagzd2141zd2_1460;
																																																				}
																																																		}
																																																	else
																																																		{	/* Ast/private.scm 78 */
																																																			goto
																																																				BgL_tagzd2141zd2_1460;
																																																		}
																																																}
																																															else
																																																{	/* Ast/private.scm 78 */
																																																	goto
																																																		BgL_tagzd2141zd2_1460;
																																																}
																																														}
																																													else
																																														{	/* Ast/private.scm 78 */
																																															goto
																																																BgL_tagzd2141zd2_1460;
																																														}
																																												}
																																											}
																																										else
																																											{	/* Ast/private.scm 78 */
																																												obj_t
																																													BgL_cdrzd211399zd2_2643;
																																												BgL_cdrzd211399zd2_2643
																																													=
																																													CDR
																																													(BgL_sexpz00_6);
																																												{	/* Ast/private.scm 78 */
																																													obj_t
																																														BgL_cdrzd211406zd2_2644;
																																													BgL_cdrzd211406zd2_2644
																																														=
																																														CDR
																																														(
																																														((obj_t) BgL_cdrzd211399zd2_2643));
																																													if ((CAR(((obj_t) BgL_cdrzd211399zd2_2643)) == CNST_TABLE_REF(14)))
																																														{	/* Ast/private.scm 78 */
																																															obj_t
																																																BgL_cdrzd211413zd2_2647;
																																															BgL_cdrzd211413zd2_2647
																																																=
																																																CDR
																																																(
																																																((obj_t) BgL_cdrzd211406zd2_2644));
																																															{	/* Ast/private.scm 78 */
																																																obj_t
																																																	BgL_cdrzd211420zd2_2648;
																																																BgL_cdrzd211420zd2_2648
																																																	=
																																																	CDR
																																																	(
																																																	((obj_t) BgL_cdrzd211413zd2_2647));
																																																if (PAIRP(BgL_cdrzd211420zd2_2648))
																																																	{	/* Ast/private.scm 78 */
																																																		if (NULLP(CDR(BgL_cdrzd211420zd2_2648)))
																																																			{	/* Ast/private.scm 78 */
																																																				obj_t
																																																					BgL_arg2384z00_2652;
																																																				obj_t
																																																					BgL_arg2385z00_2653;
																																																				obj_t
																																																					BgL_arg2386z00_2654;
																																																				BgL_arg2384z00_2652
																																																					=
																																																					CAR
																																																					(
																																																					((obj_t) BgL_cdrzd211406zd2_2644));
																																																				BgL_arg2385z00_2653
																																																					=
																																																					CAR
																																																					(
																																																					((obj_t) BgL_cdrzd211413zd2_2647));
																																																				BgL_arg2386z00_2654
																																																					=
																																																					CAR
																																																					(BgL_cdrzd211420zd2_2648);
																																																				{
																																																					BgL_sequencez00_bglt
																																																						BgL_auxz00_4818;
																																																					{
																																																						obj_t
																																																							BgL_bodyz00_4821;
																																																						obj_t
																																																							BgL_kwdsz00_4820;
																																																						obj_t
																																																							BgL_typez00_4819;
																																																						BgL_typez00_4819
																																																							=
																																																							BgL_arg2384z00_2652;
																																																						BgL_kwdsz00_4820
																																																							=
																																																							BgL_arg2385z00_2653;
																																																						BgL_bodyz00_4821
																																																							=
																																																							BgL_arg2386z00_2654;
																																																						BgL_bodyz00_1458
																																																							=
																																																							BgL_bodyz00_4821;
																																																						BgL_kwdsz00_1457
																																																							=
																																																							BgL_kwdsz00_4820;
																																																						BgL_typez00_1456
																																																							=
																																																							BgL_typez00_4819;
																																																						goto
																																																							BgL_tagzd2140zd2_1459;
																																																					}
																																																					return
																																																						(
																																																						(obj_t)
																																																						BgL_auxz00_4818);
																																																				}
																																																			}
																																																		else
																																																			{	/* Ast/private.scm 78 */
																																																				goto
																																																					BgL_tagzd2141zd2_1460;
																																																			}
																																																	}
																																																else
																																																	{	/* Ast/private.scm 78 */
																																																		goto
																																																			BgL_tagzd2141zd2_1460;
																																																	}
																																															}
																																														}
																																													else
																																														{	/* Ast/private.scm 78 */
																																															goto
																																																BgL_tagzd2141zd2_1460;
																																														}
																																												}
																																											}
																																									}
																																							}
																																					}
																																				}
																																			}
																																	}
																															}
																														}
																												}
																											}
																									}
																								else
																									{	/* Ast/private.scm 78 */
																										goto BgL_tagzd2141zd2_1460;
																									}
																							}
																						else
																							{	/* Ast/private.scm 78 */
																								goto BgL_tagzd2141zd2_1460;
																							}
																					}
																				else
																					{	/* Ast/private.scm 78 */
																						if (
																							(BgL_carzd29148zd2_2127 ==
																								CNST_TABLE_REF(17)))
																							{	/* Ast/private.scm 78 */
																								goto BgL_kapzd29150zd2_2129;
																							}
																						else
																							{	/* Ast/private.scm 78 */
																								obj_t BgL_cdrzd211613zd2_2130;

																								BgL_cdrzd211613zd2_2130 =
																									CDR(BgL_sexpz00_6);
																								{	/* Ast/private.scm 78 */
																									obj_t BgL_cdrzd211620zd2_2131;

																									BgL_cdrzd211620zd2_2131 =
																										CDR(
																										((obj_t)
																											BgL_cdrzd211613zd2_2130));
																									if ((CAR(((obj_t)
																													BgL_cdrzd211613zd2_2130))
																											== CNST_TABLE_REF(11)))
																										{	/* Ast/private.scm 78 */
																											if (PAIRP
																												(BgL_cdrzd211620zd2_2131))
																												{	/* Ast/private.scm 78 */
																													obj_t
																														BgL_cdrzd211627zd2_2135;
																													BgL_cdrzd211627zd2_2135
																														=
																														CDR
																														(BgL_cdrzd211620zd2_2131);
																													if (PAIRP
																														(BgL_cdrzd211627zd2_2135))
																														{	/* Ast/private.scm 78 */
																															obj_t
																																BgL_cdrzd211634zd2_2137;
																															BgL_cdrzd211634zd2_2137
																																=
																																CDR
																																(BgL_cdrzd211627zd2_2135);
																															if (PAIRP
																																(BgL_cdrzd211634zd2_2137))
																																{	/* Ast/private.scm 78 */
																																	obj_t
																																		BgL_cdrzd211640zd2_2139;
																																	BgL_cdrzd211640zd2_2139
																																		=
																																		CDR
																																		(BgL_cdrzd211634zd2_2137);
																																	if (PAIRP
																																		(BgL_cdrzd211640zd2_2139))
																																		{	/* Ast/private.scm 78 */
																																			obj_t
																																				BgL_carzd211644zd2_2141;
																																			obj_t
																																				BgL_cdrzd211645zd2_2142;
																																			BgL_carzd211644zd2_2141
																																				=
																																				CAR
																																				(BgL_cdrzd211640zd2_2139);
																																			BgL_cdrzd211645zd2_2142
																																				=
																																				CDR
																																				(BgL_cdrzd211640zd2_2139);
																																			if (STRINGP(BgL_carzd211644zd2_2141))
																																				{	/* Ast/private.scm 78 */
																																					if (PAIRP(BgL_cdrzd211645zd2_2142))
																																						{	/* Ast/private.scm 78 */
																																							if (NULLP(CDR(BgL_cdrzd211645zd2_2142)))
																																								{	/* Ast/private.scm 78 */
																																									obj_t
																																										BgL_arg2088z00_2147;
																																									obj_t
																																										BgL_arg2089z00_2148;
																																									obj_t
																																										BgL_arg2090z00_2149;
																																									obj_t
																																										BgL_arg2091z00_2150;
																																									BgL_arg2088z00_2147
																																										=
																																										CAR
																																										(BgL_cdrzd211620zd2_2131);
																																									BgL_arg2089z00_2148
																																										=
																																										CAR
																																										(BgL_cdrzd211627zd2_2135);
																																									BgL_arg2090z00_2149
																																										=
																																										CAR
																																										(BgL_cdrzd211634zd2_2137);
																																									BgL_arg2091z00_2150
																																										=
																																										CAR
																																										(BgL_cdrzd211645zd2_2142);
																																									{
																																										BgL_vlengthz00_bglt
																																											BgL_auxz00_4858;
																																										{
																																											obj_t
																																												BgL_expz00_4863;
																																											obj_t
																																												BgL_czd2fmtzd2_4862;
																																											obj_t
																																												BgL_otypez00_4861;
																																											obj_t
																																												BgL_ftypez00_4860;
																																											obj_t
																																												BgL_vtypez00_4859;
																																											BgL_vtypez00_4859
																																												=
																																												BgL_arg2088z00_2147;
																																											BgL_ftypez00_4860
																																												=
																																												BgL_arg2089z00_2148;
																																											BgL_otypez00_4861
																																												=
																																												BgL_arg2090z00_2149;
																																											BgL_czd2fmtzd2_4862
																																												=
																																												BgL_carzd211644zd2_2141;
																																											BgL_expz00_4863
																																												=
																																												BgL_arg2091z00_2150;
																																											BgL_expz00_1431
																																												=
																																												BgL_expz00_4863;
																																											BgL_czd2fmtzd2_1430
																																												=
																																												BgL_czd2fmtzd2_4862;
																																											BgL_otypez00_1429
																																												=
																																												BgL_otypez00_4861;
																																											BgL_ftypez00_1428
																																												=
																																												BgL_ftypez00_4860;
																																											BgL_vtypez00_1427
																																												=
																																												BgL_vtypez00_4859;
																																											goto
																																												BgL_tagzd2135zd2_1432;
																																										}
																																										return
																																											(
																																											(obj_t)
																																											BgL_auxz00_4858);
																																									}
																																								}
																																							else
																																								{	/* Ast/private.scm 78 */
																																									goto
																																										BgL_tagzd2141zd2_1460;
																																								}
																																						}
																																					else
																																						{	/* Ast/private.scm 78 */
																																							goto
																																								BgL_tagzd2141zd2_1460;
																																						}
																																				}
																																			else
																																				{	/* Ast/private.scm 78 */
																																					goto
																																						BgL_tagzd2141zd2_1460;
																																				}
																																		}
																																	else
																																		{	/* Ast/private.scm 78 */
																																			goto
																																				BgL_tagzd2141zd2_1460;
																																		}
																																}
																															else
																																{	/* Ast/private.scm 78 */
																																	goto
																																		BgL_tagzd2141zd2_1460;
																																}
																														}
																													else
																														{	/* Ast/private.scm 78 */
																															goto
																																BgL_tagzd2141zd2_1460;
																														}
																												}
																											else
																												{	/* Ast/private.scm 78 */
																													goto
																														BgL_tagzd2141zd2_1460;
																												}
																										}
																									else
																										{	/* Ast/private.scm 78 */
																											obj_t
																												BgL_carzd212992zd2_2261;
																											obj_t
																												BgL_cdrzd212993zd2_2262;
																											BgL_carzd212992zd2_2261 =
																												CAR(((obj_t)
																													BgL_cdrzd211613zd2_2130));
																											BgL_cdrzd212993zd2_2262 =
																												CDR(((obj_t)
																													BgL_cdrzd211613zd2_2130));
																											{

																												if (
																													(BgL_carzd212992zd2_2261
																														==
																														CNST_TABLE_REF(15)))
																													{	/* Ast/private.scm 78 */
																													BgL_kapzd212994zd2_2263:
																														if (PAIRP
																															(BgL_cdrzd212993zd2_2262))
																															{	/* Ast/private.scm 78 */
																																obj_t
																																	BgL_cdrzd213001zd2_2383;
																																BgL_cdrzd213001zd2_2383
																																	=
																																	CDR
																																	(BgL_cdrzd212993zd2_2262);
																																if (PAIRP
																																	(BgL_cdrzd213001zd2_2383))
																																	{	/* Ast/private.scm 78 */
																																		obj_t
																																			BgL_cdrzd213008zd2_2385;
																																		BgL_cdrzd213008zd2_2385
																																			=
																																			CDR
																																			(BgL_cdrzd213001zd2_2383);
																																		if (PAIRP
																																			(BgL_cdrzd213008zd2_2385))
																																			{	/* Ast/private.scm 78 */
																																				obj_t
																																					BgL_cdrzd213014zd2_2387;
																																				BgL_cdrzd213014zd2_2387
																																					=
																																					CDR
																																					(BgL_cdrzd213008zd2_2385);
																																				if (PAIRP(BgL_cdrzd213014zd2_2387))
																																					{	/* Ast/private.scm 78 */
																																						obj_t
																																							BgL_carzd213018zd2_2389;
																																						BgL_carzd213018zd2_2389
																																							=
																																							CAR
																																							(BgL_cdrzd213014zd2_2387);
																																						if (STRINGP(BgL_carzd213018zd2_2389))
																																							{	/* Ast/private.scm 78 */
																																								obj_t
																																									BgL_arg2231z00_2391;
																																								obj_t
																																									BgL_arg2232z00_2392;
																																								obj_t
																																									BgL_arg2233z00_2393;
																																								obj_t
																																									BgL_arg2234z00_2394;
																																								BgL_arg2231z00_2391
																																									=
																																									CAR
																																									(BgL_cdrzd212993zd2_2262);
																																								BgL_arg2232z00_2392
																																									=
																																									CAR
																																									(BgL_cdrzd213001zd2_2383);
																																								BgL_arg2233z00_2393
																																									=
																																									CAR
																																									(BgL_cdrzd213008zd2_2385);
																																								BgL_arg2234z00_2394
																																									=
																																									CDR
																																									(BgL_cdrzd213014zd2_2387);
																																								{
																																									BgL_vrefz00_bglt
																																										BgL_auxz00_4890;
																																									{
																																										obj_t
																																											BgL_restz00_4895;
																																										obj_t
																																											BgL_czd2fmtzd2_4894;
																																										obj_t
																																											BgL_otypez00_4893;
																																										obj_t
																																											BgL_ftypez00_4892;
																																										obj_t
																																											BgL_vtypez00_4891;
																																										BgL_vtypez00_4891
																																											=
																																											BgL_arg2231z00_2391;
																																										BgL_ftypez00_4892
																																											=
																																											BgL_arg2232z00_2392;
																																										BgL_otypez00_4893
																																											=
																																											BgL_arg2233z00_2393;
																																										BgL_czd2fmtzd2_4894
																																											=
																																											BgL_carzd213018zd2_2389;
																																										BgL_restz00_4895
																																											=
																																											BgL_arg2234z00_2394;
																																										BgL_restz00_1437
																																											=
																																											BgL_restz00_4895;
																																										BgL_czd2fmtzd2_1436
																																											=
																																											BgL_czd2fmtzd2_4894;
																																										BgL_otypez00_1435
																																											=
																																											BgL_otypez00_4893;
																																										BgL_ftypez00_1434
																																											=
																																											BgL_ftypez00_4892;
																																										BgL_vtypez00_1433
																																											=
																																											BgL_vtypez00_4891;
																																										goto
																																											BgL_tagzd2136zd2_1438;
																																									}
																																									return
																																										(
																																										(obj_t)
																																										BgL_auxz00_4890);
																																								}
																																							}
																																						else
																																							{	/* Ast/private.scm 78 */
																																								obj_t
																																									BgL_cdrzd213045zd2_2395;
																																								BgL_cdrzd213045zd2_2395
																																									=
																																									CDR
																																									(BgL_sexpz00_6);
																																								{	/* Ast/private.scm 78 */
																																									obj_t
																																										BgL_carzd213054zd2_2396;
																																									obj_t
																																										BgL_cdrzd213055zd2_2397;
																																									BgL_carzd213054zd2_2396
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_cdrzd213045zd2_2395));
																																									BgL_cdrzd213055zd2_2397
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_cdrzd213045zd2_2395));
																																									{

																																										if ((BgL_carzd213054zd2_2396 == CNST_TABLE_REF(13)))
																																											{	/* Ast/private.scm 78 */
																																											BgL_kapzd213056zd2_2398:
																																												{	/* Ast/private.scm 78 */
																																													obj_t
																																														BgL_cdrzd213066zd2_2421;
																																													BgL_cdrzd213066zd2_2421
																																														=
																																														CDR
																																														(
																																														((obj_t) BgL_cdrzd213055zd2_2397));
																																													{	/* Ast/private.scm 78 */
																																														obj_t
																																															BgL_cdrzd213076zd2_2422;
																																														BgL_cdrzd213076zd2_2422
																																															=
																																															CDR
																																															(
																																															((obj_t) BgL_cdrzd213066zd2_2421));
																																														{	/* Ast/private.scm 78 */
																																															obj_t
																																																BgL_cdrzd213084zd2_2423;
																																															BgL_cdrzd213084zd2_2423
																																																=
																																																CDR
																																																(
																																																((obj_t) BgL_cdrzd213076zd2_2422));
																																															{	/* Ast/private.scm 78 */
																																																obj_t
																																																	BgL_carzd213089zd2_2424;
																																																BgL_carzd213089zd2_2424
																																																	=
																																																	CAR
																																																	(
																																																	((obj_t) BgL_cdrzd213084zd2_2423));
																																																if (STRINGP(BgL_carzd213089zd2_2424))
																																																	{	/* Ast/private.scm 78 */
																																																		obj_t
																																																			BgL_arg2248z00_2426;
																																																		obj_t
																																																			BgL_arg2249z00_2427;
																																																		obj_t
																																																			BgL_arg2250z00_2428;
																																																		obj_t
																																																			BgL_arg2251z00_2429;
																																																		BgL_arg2248z00_2426
																																																			=
																																																			CAR
																																																			(
																																																			((obj_t) BgL_cdrzd213055zd2_2397));
																																																		BgL_arg2249z00_2427
																																																			=
																																																			CAR
																																																			(
																																																			((obj_t) BgL_cdrzd213066zd2_2421));
																																																		BgL_arg2250z00_2428
																																																			=
																																																			CAR
																																																			(
																																																			((obj_t) BgL_cdrzd213076zd2_2422));
																																																		BgL_arg2251z00_2429
																																																			=
																																																			CDR
																																																			(
																																																			((obj_t) BgL_cdrzd213084zd2_2423));
																																																		{
																																																			BgL_vsetz12z12_bglt
																																																				BgL_auxz00_4923;
																																																			{
																																																				obj_t
																																																					BgL_restz00_4928;
																																																				obj_t
																																																					BgL_czd2fmtzd2_4927;
																																																				obj_t
																																																					BgL_otypez00_4926;
																																																				obj_t
																																																					BgL_ftypez00_4925;
																																																				obj_t
																																																					BgL_vtypez00_4924;
																																																				BgL_vtypez00_4924
																																																					=
																																																					BgL_arg2248z00_2426;
																																																				BgL_ftypez00_4925
																																																					=
																																																					BgL_arg2249z00_2427;
																																																				BgL_otypez00_4926
																																																					=
																																																					BgL_arg2250z00_2428;
																																																				BgL_czd2fmtzd2_4927
																																																					=
																																																					BgL_carzd213089zd2_2424;
																																																				BgL_restz00_4928
																																																					=
																																																					BgL_arg2251z00_2429;
																																																				BgL_restz00_1443
																																																					=
																																																					BgL_restz00_4928;
																																																				BgL_czd2fmtzd2_1442
																																																					=
																																																					BgL_czd2fmtzd2_4927;
																																																				BgL_otypez00_1441
																																																					=
																																																					BgL_otypez00_4926;
																																																				BgL_ftypez00_1440
																																																					=
																																																					BgL_ftypez00_4925;
																																																				BgL_vtypez00_1439
																																																					=
																																																					BgL_vtypez00_4924;
																																																				goto
																																																					BgL_tagzd2137zd2_1444;
																																																			}
																																																			return
																																																				(
																																																				(obj_t)
																																																				BgL_auxz00_4923);
																																																		}
																																																	}
																																																else
																																																	{	/* Ast/private.scm 78 */
																																																		obj_t
																																																			BgL_cdrzd213117zd2_2430;
																																																		BgL_cdrzd213117zd2_2430
																																																			=
																																																			CDR
																																																			(BgL_sexpz00_6);
																																																		{	/* Ast/private.scm 78 */
																																																			obj_t
																																																				BgL_cdrzd213129zd2_2431;
																																																			BgL_cdrzd213129zd2_2431
																																																				=
																																																				CDR
																																																				(
																																																				((obj_t) BgL_cdrzd213117zd2_2430));
																																																			if ((CAR(((obj_t) BgL_cdrzd213117zd2_2430)) == CNST_TABLE_REF(12)))
																																																				{	/* Ast/private.scm 78 */
																																																					obj_t
																																																						BgL_cdrzd213141zd2_2434;
																																																					BgL_cdrzd213141zd2_2434
																																																						=
																																																						CDR
																																																						(
																																																						((obj_t) BgL_cdrzd213129zd2_2431));
																																																					{	/* Ast/private.scm 78 */
																																																						obj_t
																																																							BgL_cdrzd213153zd2_2435;
																																																						BgL_cdrzd213153zd2_2435
																																																							=
																																																							CDR
																																																							(
																																																							((obj_t) BgL_cdrzd213141zd2_2434));
																																																						{	/* Ast/private.scm 78 */
																																																							obj_t
																																																								BgL_cdrzd213163zd2_2436;
																																																							BgL_cdrzd213163zd2_2436
																																																								=
																																																								CDR
																																																								(
																																																								((obj_t) BgL_cdrzd213153zd2_2435));
																																																							{	/* Ast/private.scm 78 */
																																																								obj_t
																																																									BgL_carzd213170zd2_2437;
																																																								obj_t
																																																									BgL_cdrzd213171zd2_2438;
																																																								BgL_carzd213170zd2_2437
																																																									=
																																																									CAR
																																																									(
																																																									((obj_t) BgL_cdrzd213163zd2_2436));
																																																								BgL_cdrzd213171zd2_2438
																																																									=
																																																									CDR
																																																									(
																																																									((obj_t) BgL_cdrzd213163zd2_2436));
																																																								if (STRINGP(BgL_carzd213170zd2_2437))
																																																									{	/* Ast/private.scm 78 */
																																																										if (PAIRP(BgL_cdrzd213171zd2_2438))
																																																											{	/* Ast/private.scm 78 */
																																																												obj_t
																																																													BgL_carzd213178zd2_2441;
																																																												obj_t
																																																													BgL_cdrzd213179zd2_2442;
																																																												BgL_carzd213178zd2_2441
																																																													=
																																																													CAR
																																																													(BgL_cdrzd213171zd2_2438);
																																																												BgL_cdrzd213179zd2_2442
																																																													=
																																																													CDR
																																																													(BgL_cdrzd213171zd2_2438);
																																																												if (STRINGP(BgL_carzd213178zd2_2441))
																																																													{	/* Ast/private.scm 78 */
																																																														if (PAIRP(BgL_cdrzd213179zd2_2442))
																																																															{	/* Ast/private.scm 78 */
																																																																obj_t
																																																																	BgL_carzd213185zd2_2445;
																																																																BgL_carzd213185zd2_2445
																																																																	=
																																																																	CAR
																																																																	(BgL_cdrzd213179zd2_2442);
																																																																if (BOOLEANP(BgL_carzd213185zd2_2445))
																																																																	{	/* Ast/private.scm 78 */
																																																																		obj_t
																																																																			BgL_arg2259z00_2447;
																																																																		obj_t
																																																																			BgL_arg2260z00_2448;
																																																																		obj_t
																																																																			BgL_arg2261z00_2449;
																																																																		obj_t
																																																																			BgL_arg2262z00_2450;
																																																																		BgL_arg2259z00_2447
																																																																			=
																																																																			CAR
																																																																			(
																																																																			((obj_t) BgL_cdrzd213129zd2_2431));
																																																																		BgL_arg2260z00_2448
																																																																			=
																																																																			CAR
																																																																			(
																																																																			((obj_t) BgL_cdrzd213141zd2_2434));
																																																																		BgL_arg2261z00_2449
																																																																			=
																																																																			CAR
																																																																			(
																																																																			((obj_t) BgL_cdrzd213153zd2_2435));
																																																																		BgL_arg2262z00_2450
																																																																			=
																																																																			CDR
																																																																			(BgL_cdrzd213179zd2_2442);
																																																																		{
																																																																			BgL_vallocz00_bglt
																																																																				BgL_auxz00_4968;
																																																																			{
																																																																				obj_t
																																																																					BgL_restz00_4975;
																																																																				obj_t
																																																																					BgL_stackzf3zf3_4974;
																																																																				obj_t
																																																																					BgL_czd2stackzd2fmtz00_4973;
																																																																				obj_t
																																																																					BgL_czd2heapzd2fmtz00_4972;
																																																																				obj_t
																																																																					BgL_otypez00_4971;
																																																																				obj_t
																																																																					BgL_ftypez00_4970;
																																																																				obj_t
																																																																					BgL_vtypez00_4969;
																																																																				BgL_vtypez00_4969
																																																																					=
																																																																					BgL_arg2259z00_2447;
																																																																				BgL_ftypez00_4970
																																																																					=
																																																																					BgL_arg2260z00_2448;
																																																																				BgL_otypez00_4971
																																																																					=
																																																																					BgL_arg2261z00_2449;
																																																																				BgL_czd2heapzd2fmtz00_4972
																																																																					=
																																																																					BgL_carzd213170zd2_2437;
																																																																				BgL_czd2stackzd2fmtz00_4973
																																																																					=
																																																																					BgL_carzd213178zd2_2441;
																																																																				BgL_stackzf3zf3_4974
																																																																					=
																																																																					BgL_carzd213185zd2_2445;
																																																																				BgL_restz00_4975
																																																																					=
																																																																					BgL_arg2262z00_2450;
																																																																				BgL_restz00_1451
																																																																					=
																																																																					BgL_restz00_4975;
																																																																				BgL_stackzf3zf3_1450
																																																																					=
																																																																					BgL_stackzf3zf3_4974;
																																																																				BgL_czd2stackzd2fmtz00_1449
																																																																					=
																																																																					BgL_czd2stackzd2fmtz00_4973;
																																																																				BgL_czd2heapzd2fmtz00_1448
																																																																					=
																																																																					BgL_czd2heapzd2fmtz00_4972;
																																																																				BgL_otypez00_1447
																																																																					=
																																																																					BgL_otypez00_4971;
																																																																				BgL_ftypez00_1446
																																																																					=
																																																																					BgL_ftypez00_4970;
																																																																				BgL_vtypez00_1445
																																																																					=
																																																																					BgL_vtypez00_4969;
																																																																				goto
																																																																					BgL_tagzd2138zd2_1452;
																																																																			}
																																																																			return
																																																																				(
																																																																				(obj_t)
																																																																				BgL_auxz00_4968);
																																																																		}
																																																																	}
																																																																else
																																																																	{	/* Ast/private.scm 78 */
																																																																		goto
																																																																			BgL_tagzd2141zd2_1460;
																																																																	}
																																																															}
																																																														else
																																																															{	/* Ast/private.scm 78 */
																																																																goto
																																																																	BgL_tagzd2141zd2_1460;
																																																															}
																																																													}
																																																												else
																																																													{	/* Ast/private.scm 78 */
																																																														goto
																																																															BgL_tagzd2141zd2_1460;
																																																													}
																																																											}
																																																										else
																																																											{	/* Ast/private.scm 78 */
																																																												goto
																																																													BgL_tagzd2141zd2_1460;
																																																											}
																																																									}
																																																								else
																																																									{	/* Ast/private.scm 78 */
																																																										goto
																																																											BgL_tagzd2141zd2_1460;
																																																									}
																																																							}
																																																						}
																																																					}
																																																				}
																																																			else
																																																				{	/* Ast/private.scm 78 */
																																																					goto
																																																						BgL_tagzd2141zd2_1460;
																																																				}
																																																		}
																																																	}
																																															}
																																														}
																																													}
																																												}
																																											}
																																										else
																																											{	/* Ast/private.scm 78 */
																																												if ((BgL_carzd213054zd2_2396 == CNST_TABLE_REF(5)))
																																													{	/* Ast/private.scm 78 */
																																														goto
																																															BgL_kapzd213056zd2_2398;
																																													}
																																												else
																																													{	/* Ast/private.scm 78 */
																																														obj_t
																																															BgL_cdrzd213302zd2_2400;
																																														BgL_cdrzd213302zd2_2400
																																															=
																																															CDR
																																															(
																																															((obj_t) BgL_cdrzd213045zd2_2395));
																																														if ((CAR(((obj_t) BgL_cdrzd213045zd2_2395)) == CNST_TABLE_REF(12)))
																																															{	/* Ast/private.scm 78 */
																																																obj_t
																																																	BgL_cdrzd213314zd2_2403;
																																																BgL_cdrzd213314zd2_2403
																																																	=
																																																	CDR
																																																	(
																																																	((obj_t) BgL_cdrzd213302zd2_2400));
																																																{	/* Ast/private.scm 78 */
																																																	obj_t
																																																		BgL_cdrzd213326zd2_2404;
																																																	BgL_cdrzd213326zd2_2404
																																																		=
																																																		CDR
																																																		(
																																																		((obj_t) BgL_cdrzd213314zd2_2403));
																																																	{	/* Ast/private.scm 78 */
																																																		obj_t
																																																			BgL_cdrzd213336zd2_2405;
																																																		BgL_cdrzd213336zd2_2405
																																																			=
																																																			CDR
																																																			(
																																																			((obj_t) BgL_cdrzd213326zd2_2404));
																																																		{	/* Ast/private.scm 78 */
																																																			obj_t
																																																				BgL_carzd213343zd2_2406;
																																																			obj_t
																																																				BgL_cdrzd213344zd2_2407;
																																																			BgL_carzd213343zd2_2406
																																																				=
																																																				CAR
																																																				(
																																																				((obj_t) BgL_cdrzd213336zd2_2405));
																																																			BgL_cdrzd213344zd2_2407
																																																				=
																																																				CDR
																																																				(
																																																				((obj_t) BgL_cdrzd213336zd2_2405));
																																																			if (STRINGP(BgL_carzd213343zd2_2406))
																																																				{	/* Ast/private.scm 78 */
																																																					if (PAIRP(BgL_cdrzd213344zd2_2407))
																																																						{	/* Ast/private.scm 78 */
																																																							obj_t
																																																								BgL_carzd213351zd2_2410;
																																																							obj_t
																																																								BgL_cdrzd213352zd2_2411;
																																																							BgL_carzd213351zd2_2410
																																																								=
																																																								CAR
																																																								(BgL_cdrzd213344zd2_2407);
																																																							BgL_cdrzd213352zd2_2411
																																																								=
																																																								CDR
																																																								(BgL_cdrzd213344zd2_2407);
																																																							if (STRINGP(BgL_carzd213351zd2_2410))
																																																								{	/* Ast/private.scm 78 */
																																																									if (PAIRP(BgL_cdrzd213352zd2_2411))
																																																										{	/* Ast/private.scm 78 */
																																																											obj_t
																																																												BgL_carzd213358zd2_2414;
																																																											BgL_carzd213358zd2_2414
																																																												=
																																																												CAR
																																																												(BgL_cdrzd213352zd2_2411);
																																																											if (BOOLEANP(BgL_carzd213358zd2_2414))
																																																												{	/* Ast/private.scm 78 */
																																																													obj_t
																																																														BgL_arg2242z00_2416;
																																																													obj_t
																																																														BgL_arg2243z00_2417;
																																																													obj_t
																																																														BgL_arg2244z00_2418;
																																																													obj_t
																																																														BgL_arg2245z00_2419;
																																																													BgL_arg2242z00_2416
																																																														=
																																																														CAR
																																																														(
																																																														((obj_t) BgL_cdrzd213302zd2_2400));
																																																													BgL_arg2243z00_2417
																																																														=
																																																														CAR
																																																														(
																																																														((obj_t) BgL_cdrzd213314zd2_2403));
																																																													BgL_arg2244z00_2418
																																																														=
																																																														CAR
																																																														(
																																																														((obj_t) BgL_cdrzd213326zd2_2404));
																																																													BgL_arg2245z00_2419
																																																														=
																																																														CDR
																																																														(BgL_cdrzd213352zd2_2411);
																																																													{
																																																														BgL_vallocz00_bglt
																																																															BgL_auxz00_5017;
																																																														{
																																																															obj_t
																																																																BgL_restz00_5024;
																																																															obj_t
																																																																BgL_stackzf3zf3_5023;
																																																															obj_t
																																																																BgL_czd2stackzd2fmtz00_5022;
																																																															obj_t
																																																																BgL_czd2heapzd2fmtz00_5021;
																																																															obj_t
																																																																BgL_otypez00_5020;
																																																															obj_t
																																																																BgL_ftypez00_5019;
																																																															obj_t
																																																																BgL_vtypez00_5018;
																																																															BgL_vtypez00_5018
																																																																=
																																																																BgL_arg2242z00_2416;
																																																															BgL_ftypez00_5019
																																																																=
																																																																BgL_arg2243z00_2417;
																																																															BgL_otypez00_5020
																																																																=
																																																																BgL_arg2244z00_2418;
																																																															BgL_czd2heapzd2fmtz00_5021
																																																																=
																																																																BgL_carzd213343zd2_2406;
																																																															BgL_czd2stackzd2fmtz00_5022
																																																																=
																																																																BgL_carzd213351zd2_2410;
																																																															BgL_stackzf3zf3_5023
																																																																=
																																																																BgL_carzd213358zd2_2414;
																																																															BgL_restz00_5024
																																																																=
																																																																BgL_arg2245z00_2419;
																																																															BgL_restz00_1451
																																																																=
																																																																BgL_restz00_5024;
																																																															BgL_stackzf3zf3_1450
																																																																=
																																																																BgL_stackzf3zf3_5023;
																																																															BgL_czd2stackzd2fmtz00_1449
																																																																=
																																																																BgL_czd2stackzd2fmtz00_5022;
																																																															BgL_czd2heapzd2fmtz00_1448
																																																																=
																																																																BgL_czd2heapzd2fmtz00_5021;
																																																															BgL_otypez00_1447
																																																																=
																																																																BgL_otypez00_5020;
																																																															BgL_ftypez00_1446
																																																																=
																																																																BgL_ftypez00_5019;
																																																															BgL_vtypez00_1445
																																																																=
																																																																BgL_vtypez00_5018;
																																																															goto
																																																																BgL_tagzd2138zd2_1452;
																																																														}
																																																														return
																																																															(
																																																															(obj_t)
																																																															BgL_auxz00_5017);
																																																													}
																																																												}
																																																											else
																																																												{	/* Ast/private.scm 78 */
																																																													goto
																																																														BgL_tagzd2141zd2_1460;
																																																												}
																																																										}
																																																									else
																																																										{	/* Ast/private.scm 78 */
																																																											goto
																																																												BgL_tagzd2141zd2_1460;
																																																										}
																																																								}
																																																							else
																																																								{	/* Ast/private.scm 78 */
																																																									goto
																																																										BgL_tagzd2141zd2_1460;
																																																								}
																																																						}
																																																					else
																																																						{	/* Ast/private.scm 78 */
																																																							goto
																																																								BgL_tagzd2141zd2_1460;
																																																						}
																																																				}
																																																			else
																																																				{	/* Ast/private.scm 78 */
																																																					goto
																																																						BgL_tagzd2141zd2_1460;
																																																				}
																																																		}
																																																	}
																																																}
																																															}
																																														else
																																															{	/* Ast/private.scm 78 */
																																																goto
																																																	BgL_tagzd2141zd2_1460;
																																															}
																																													}
																																											}
																																									}
																																								}
																																							}
																																					}
																																				else
																																					{	/* Ast/private.scm 78 */
																																						obj_t
																																							BgL_cdrzd213500zd2_2452;
																																						BgL_cdrzd213500zd2_2452
																																							=
																																							CDR
																																							(BgL_sexpz00_6);
																																						{	/* Ast/private.scm 78 */
																																							obj_t
																																								BgL_cdrzd213508zd2_2453;
																																							BgL_cdrzd213508zd2_2453
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_cdrzd213500zd2_2452));
																																							if ((CAR(((obj_t) BgL_cdrzd213500zd2_2452)) == CNST_TABLE_REF(14)))
																																								{	/* Ast/private.scm 78 */
																																									obj_t
																																										BgL_cdrzd213516zd2_2456;
																																									BgL_cdrzd213516zd2_2456
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_cdrzd213508zd2_2453));
																																									{	/* Ast/private.scm 78 */
																																										obj_t
																																											BgL_cdrzd213524zd2_2457;
																																										BgL_cdrzd213524zd2_2457
																																											=
																																											CDR
																																											(
																																											((obj_t) BgL_cdrzd213516zd2_2456));
																																										if (NULLP(CDR(((obj_t) BgL_cdrzd213524zd2_2457))))
																																											{	/* Ast/private.scm 78 */
																																												obj_t
																																													BgL_arg2268z00_2460;
																																												obj_t
																																													BgL_arg2269z00_2461;
																																												obj_t
																																													BgL_arg2270z00_2462;
																																												BgL_arg2268z00_2460
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_cdrzd213508zd2_2453));
																																												BgL_arg2269z00_2461
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_cdrzd213516zd2_2456));
																																												BgL_arg2270z00_2462
																																													=
																																													CAR
																																													(
																																													((obj_t) BgL_cdrzd213524zd2_2457));
																																												{
																																													BgL_sequencez00_bglt
																																														BgL_auxz00_5048;
																																													{
																																														obj_t
																																															BgL_bodyz00_5051;
																																														obj_t
																																															BgL_kwdsz00_5050;
																																														obj_t
																																															BgL_typez00_5049;
																																														BgL_typez00_5049
																																															=
																																															BgL_arg2268z00_2460;
																																														BgL_kwdsz00_5050
																																															=
																																															BgL_arg2269z00_2461;
																																														BgL_bodyz00_5051
																																															=
																																															BgL_arg2270z00_2462;
																																														BgL_bodyz00_1458
																																															=
																																															BgL_bodyz00_5051;
																																														BgL_kwdsz00_1457
																																															=
																																															BgL_kwdsz00_5050;
																																														BgL_typez00_1456
																																															=
																																															BgL_typez00_5049;
																																														goto
																																															BgL_tagzd2140zd2_1459;
																																													}
																																													return
																																														(
																																														(obj_t)
																																														BgL_auxz00_5048);
																																												}
																																											}
																																										else
																																											{	/* Ast/private.scm 78 */
																																												goto
																																													BgL_tagzd2141zd2_1460;
																																											}
																																									}
																																								}
																																							else
																																								{	/* Ast/private.scm 78 */
																																									goto
																																										BgL_tagzd2141zd2_1460;
																																								}
																																						}
																																					}
																																			}
																																		else
																																			{	/* Ast/private.scm 78 */
																																				obj_t
																																					BgL_cdrzd213581zd2_2465;
																																				BgL_cdrzd213581zd2_2465
																																					=
																																					CDR
																																					(BgL_sexpz00_6);
																																				{	/* Ast/private.scm 78 */
																																					obj_t
																																						BgL_cdrzd213587zd2_2466;
																																					BgL_cdrzd213587zd2_2466
																																						=
																																						CDR(
																																						((obj_t) BgL_cdrzd213581zd2_2465));
																																					if (
																																						(CAR
																																							(((obj_t) BgL_cdrzd213581zd2_2465)) == CNST_TABLE_REF(18)))
																																						{	/* Ast/private.scm 78 */
																																							obj_t
																																								BgL_cdrzd213593zd2_2469;
																																							BgL_cdrzd213593zd2_2469
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_cdrzd213587zd2_2466));
																																							if (NULLP(CDR(((obj_t) BgL_cdrzd213593zd2_2469))))
																																								{	/* Ast/private.scm 78 */
																																									obj_t
																																										BgL_arg2277z00_2472;
																																									obj_t
																																										BgL_arg2279z00_2473;
																																									BgL_arg2277z00_2472
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_cdrzd213587zd2_2466));
																																									BgL_arg2279z00_2473
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_cdrzd213593zd2_2469));
																																									{
																																										BgL_sequencez00_bglt
																																											BgL_auxz00_5071;
																																										BgL_typez00_1453
																																											=
																																											BgL_arg2277z00_2472;
																																										BgL_expz00_1454
																																											=
																																											BgL_arg2279z00_2473;
																																									BgL_tagzd2139zd2_1455:
																																										{	/* Ast/private.scm 213 */
																																											BgL_sequencez00_bglt
																																												BgL_new1145z00_2916;
																																											{	/* Ast/private.scm 214 */
																																												BgL_sequencez00_bglt
																																													BgL_new1144z00_2919;
																																												BgL_new1144z00_2919
																																													=
																																													(
																																													(BgL_sequencez00_bglt)
																																													BOBJECT
																																													(GC_MALLOC
																																														(sizeof
																																															(struct
																																																BgL_sequencez00_bgl))));
																																												{	/* Ast/private.scm 214 */
																																													long
																																														BgL_arg2557z00_2920;
																																													BgL_arg2557z00_2920
																																														=
																																														BGL_CLASS_NUM
																																														(BGl_sequencez00zzast_nodez00);
																																													BGL_OBJECT_CLASS_NUM_SET
																																														(
																																														((BgL_objectz00_bglt) BgL_new1144z00_2919), BgL_arg2557z00_2920);
																																												}
																																												{	/* Ast/private.scm 214 */
																																													BgL_objectz00_bglt
																																														BgL_tmpz00_5076;
																																													BgL_tmpz00_5076
																																														=
																																														(
																																														(BgL_objectz00_bglt)
																																														BgL_new1144z00_2919);
																																													BGL_OBJECT_WIDENING_SET
																																														(BgL_tmpz00_5076,
																																														BFALSE);
																																												}
																																												((BgL_objectz00_bglt) BgL_new1144z00_2919);
																																												BgL_new1145z00_2916
																																													=
																																													BgL_new1144z00_2919;
																																											}
																																											((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1145z00_2916)))->BgL_locz00) = ((obj_t) BgL_locz00_8), BUNSPEC);
																																											((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_new1145z00_2916)))->BgL_typez00) = ((BgL_typez00_bglt) BGl_usezd2typez12zc0zztype_envz00(BgL_typez00_1453, BgL_locz00_8)), BUNSPEC);
																																											((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1145z00_2916)))->BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
																																											((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1145z00_2916)))->BgL_keyz00) = ((obj_t) BINT(-1L)), BUNSPEC);
																																											{
																																												obj_t
																																													BgL_auxz00_5090;
																																												{	/* Ast/private.scm 217 */
																																													BgL_nodez00_bglt
																																														BgL_arg2555z00_2917;
																																													BgL_arg2555z00_2917
																																														=
																																														BGl_sexpzd2ze3nodez31zzast_sexpz00
																																														(BgL_expz00_1454,
																																														BgL_stackz00_7,
																																														BgL_locz00_8,
																																														CNST_TABLE_REF
																																														(3));
																																													{	/* Ast/private.scm 217 */
																																														obj_t
																																															BgL_list2556z00_2918;
																																														BgL_list2556z00_2918
																																															=
																																															MAKE_YOUNG_PAIR
																																															(
																																															((obj_t) BgL_arg2555z00_2917), BNIL);
																																														BgL_auxz00_5090
																																															=
																																															BgL_list2556z00_2918;
																																												}}
																																												((((BgL_sequencez00_bglt) COBJECT(BgL_new1145z00_2916))->BgL_nodesz00) = ((obj_t) BgL_auxz00_5090), BUNSPEC);
																																											}
																																											((((BgL_sequencez00_bglt) COBJECT(BgL_new1145z00_2916))->BgL_unsafez00) = ((bool_t) ((bool_t) 1)), BUNSPEC);
																																											((((BgL_sequencez00_bglt) COBJECT(BgL_new1145z00_2916))->BgL_metaz00) = ((obj_t) BNIL), BUNSPEC);
																																											BgL_auxz00_5071
																																												=
																																												BgL_new1145z00_2916;
																																										}
																																										return
																																											(
																																											(obj_t)
																																											BgL_auxz00_5071);
																																									}
																																								}
																																							else
																																								{	/* Ast/private.scm 78 */
																																									goto
																																										BgL_tagzd2141zd2_1460;
																																								}
																																						}
																																					else
																																						{	/* Ast/private.scm 78 */
																																							goto
																																								BgL_tagzd2141zd2_1460;
																																						}
																																				}
																																			}
																																	}
																																else
																																	{	/* Ast/private.scm 78 */
																																		goto
																																			BgL_tagzd2141zd2_1460;
																																	}
																															}
																														else
																															{	/* Ast/private.scm 78 */
																																goto
																																	BgL_tagzd2141zd2_1460;
																															}
																													}
																												else
																													{	/* Ast/private.scm 78 */
																														if (
																															(BgL_carzd212992zd2_2261
																																==
																																CNST_TABLE_REF
																																(4)))
																															{	/* Ast/private.scm 78 */
																																goto
																																	BgL_kapzd212994zd2_2263;
																															}
																														else
																															{	/* Ast/private.scm 78 */
																																obj_t
																																	BgL_cdrzd213732zd2_2264;
																																BgL_cdrzd213732zd2_2264
																																	=
																																	CDR
																																	(BgL_sexpz00_6);
																																{	/* Ast/private.scm 78 */
																																	obj_t
																																		BgL_carzd213738zd2_2265;
																																	obj_t
																																		BgL_cdrzd213739zd2_2266;
																																	BgL_carzd213738zd2_2265
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd213732zd2_2264));
																																	BgL_cdrzd213739zd2_2266
																																		=
																																		CDR(((obj_t)
																																			BgL_cdrzd213732zd2_2264));
																																	{

																																		if (
																																			(BgL_carzd213738zd2_2265
																																				==
																																				CNST_TABLE_REF
																																				(13)))
																																			{	/* Ast/private.scm 78 */
																																			BgL_kapzd213740zd2_2267:
																																				if (PAIRP(BgL_cdrzd213739zd2_2266))
																																					{	/* Ast/private.scm 78 */
																																						obj_t
																																							BgL_cdrzd213747zd2_2324;
																																						BgL_cdrzd213747zd2_2324
																																							=
																																							CDR
																																							(BgL_cdrzd213739zd2_2266);
																																						if (PAIRP(BgL_cdrzd213747zd2_2324))
																																							{	/* Ast/private.scm 78 */
																																								obj_t
																																									BgL_cdrzd213754zd2_2326;
																																								BgL_cdrzd213754zd2_2326
																																									=
																																									CDR
																																									(BgL_cdrzd213747zd2_2324);
																																								if (PAIRP(BgL_cdrzd213754zd2_2326))
																																									{	/* Ast/private.scm 78 */
																																										obj_t
																																											BgL_cdrzd213760zd2_2328;
																																										BgL_cdrzd213760zd2_2328
																																											=
																																											CDR
																																											(BgL_cdrzd213754zd2_2326);
																																										if (PAIRP(BgL_cdrzd213760zd2_2328))
																																											{	/* Ast/private.scm 78 */
																																												obj_t
																																													BgL_carzd213764zd2_2330;
																																												BgL_carzd213764zd2_2330
																																													=
																																													CAR
																																													(BgL_cdrzd213760zd2_2328);
																																												if (STRINGP(BgL_carzd213764zd2_2330))
																																													{	/* Ast/private.scm 78 */
																																														obj_t
																																															BgL_arg2192z00_2332;
																																														obj_t
																																															BgL_arg2193z00_2333;
																																														obj_t
																																															BgL_arg2194z00_2334;
																																														obj_t
																																															BgL_arg2196z00_2335;
																																														BgL_arg2192z00_2332
																																															=
																																															CAR
																																															(BgL_cdrzd213739zd2_2266);
																																														BgL_arg2193z00_2333
																																															=
																																															CAR
																																															(BgL_cdrzd213747zd2_2324);
																																														BgL_arg2194z00_2334
																																															=
																																															CAR
																																															(BgL_cdrzd213754zd2_2326);
																																														BgL_arg2196z00_2335
																																															=
																																															CDR
																																															(BgL_cdrzd213760zd2_2328);
																																														{
																																															BgL_vsetz12z12_bglt
																																																BgL_auxz00_5128;
																																															{
																																																obj_t
																																																	BgL_restz00_5133;
																																																obj_t
																																																	BgL_czd2fmtzd2_5132;
																																																obj_t
																																																	BgL_otypez00_5131;
																																																obj_t
																																																	BgL_ftypez00_5130;
																																																obj_t
																																																	BgL_vtypez00_5129;
																																																BgL_vtypez00_5129
																																																	=
																																																	BgL_arg2192z00_2332;
																																																BgL_ftypez00_5130
																																																	=
																																																	BgL_arg2193z00_2333;
																																																BgL_otypez00_5131
																																																	=
																																																	BgL_arg2194z00_2334;
																																																BgL_czd2fmtzd2_5132
																																																	=
																																																	BgL_carzd213764zd2_2330;
																																																BgL_restz00_5133
																																																	=
																																																	BgL_arg2196z00_2335;
																																																BgL_restz00_1443
																																																	=
																																																	BgL_restz00_5133;
																																																BgL_czd2fmtzd2_1442
																																																	=
																																																	BgL_czd2fmtzd2_5132;
																																																BgL_otypez00_1441
																																																	=
																																																	BgL_otypez00_5131;
																																																BgL_ftypez00_1440
																																																	=
																																																	BgL_ftypez00_5130;
																																																BgL_vtypez00_1439
																																																	=
																																																	BgL_vtypez00_5129;
																																																goto
																																																	BgL_tagzd2137zd2_1444;
																																															}
																																															return
																																																(
																																																(obj_t)
																																																BgL_auxz00_5128);
																																														}
																																													}
																																												else
																																													{	/* Ast/private.scm 78 */
																																														obj_t
																																															BgL_cdrzd213792zd2_2336;
																																														BgL_cdrzd213792zd2_2336
																																															=
																																															CDR
																																															(BgL_sexpz00_6);
																																														{	/* Ast/private.scm 78 */
																																															obj_t
																																																BgL_cdrzd213804zd2_2337;
																																															BgL_cdrzd213804zd2_2337
																																																=
																																																CDR
																																																(
																																																((obj_t) BgL_cdrzd213792zd2_2336));
																																															if ((CAR(((obj_t) BgL_cdrzd213792zd2_2336)) == CNST_TABLE_REF(12)))
																																																{	/* Ast/private.scm 78 */
																																																	obj_t
																																																		BgL_cdrzd213816zd2_2340;
																																																	BgL_cdrzd213816zd2_2340
																																																		=
																																																		CDR
																																																		(
																																																		((obj_t) BgL_cdrzd213804zd2_2337));
																																																	{	/* Ast/private.scm 78 */
																																																		obj_t
																																																			BgL_cdrzd213828zd2_2341;
																																																		BgL_cdrzd213828zd2_2341
																																																			=
																																																			CDR
																																																			(
																																																			((obj_t) BgL_cdrzd213816zd2_2340));
																																																		{	/* Ast/private.scm 78 */
																																																			obj_t
																																																				BgL_cdrzd213838zd2_2342;
																																																			BgL_cdrzd213838zd2_2342
																																																				=
																																																				CDR
																																																				(
																																																				((obj_t) BgL_cdrzd213828zd2_2341));
																																																			{	/* Ast/private.scm 78 */
																																																				obj_t
																																																					BgL_carzd213845zd2_2343;
																																																				obj_t
																																																					BgL_cdrzd213846zd2_2344;
																																																				BgL_carzd213845zd2_2343
																																																					=
																																																					CAR
																																																					(
																																																					((obj_t) BgL_cdrzd213838zd2_2342));
																																																				BgL_cdrzd213846zd2_2344
																																																					=
																																																					CDR
																																																					(
																																																					((obj_t) BgL_cdrzd213838zd2_2342));
																																																				if (STRINGP(BgL_carzd213845zd2_2343))
																																																					{	/* Ast/private.scm 78 */
																																																						if (PAIRP(BgL_cdrzd213846zd2_2344))
																																																							{	/* Ast/private.scm 78 */
																																																								obj_t
																																																									BgL_carzd213853zd2_2347;
																																																								obj_t
																																																									BgL_cdrzd213854zd2_2348;
																																																								BgL_carzd213853zd2_2347
																																																									=
																																																									CAR
																																																									(BgL_cdrzd213846zd2_2344);
																																																								BgL_cdrzd213854zd2_2348
																																																									=
																																																									CDR
																																																									(BgL_cdrzd213846zd2_2344);
																																																								if (STRINGP(BgL_carzd213853zd2_2347))
																																																									{	/* Ast/private.scm 78 */
																																																										if (PAIRP(BgL_cdrzd213854zd2_2348))
																																																											{	/* Ast/private.scm 78 */
																																																												obj_t
																																																													BgL_carzd213860zd2_2351;
																																																												BgL_carzd213860zd2_2351
																																																													=
																																																													CAR
																																																													(BgL_cdrzd213854zd2_2348);
																																																												if (BOOLEANP(BgL_carzd213860zd2_2351))
																																																													{	/* Ast/private.scm 78 */
																																																														obj_t
																																																															BgL_arg2204z00_2353;
																																																														obj_t
																																																															BgL_arg2205z00_2354;
																																																														obj_t
																																																															BgL_arg2206z00_2355;
																																																														obj_t
																																																															BgL_arg2207z00_2356;
																																																														BgL_arg2204z00_2353
																																																															=
																																																															CAR
																																																															(
																																																															((obj_t) BgL_cdrzd213804zd2_2337));
																																																														BgL_arg2205z00_2354
																																																															=
																																																															CAR
																																																															(
																																																															((obj_t) BgL_cdrzd213816zd2_2340));
																																																														BgL_arg2206z00_2355
																																																															=
																																																															CAR
																																																															(
																																																															((obj_t) BgL_cdrzd213828zd2_2341));
																																																														BgL_arg2207z00_2356
																																																															=
																																																															CDR
																																																															(BgL_cdrzd213854zd2_2348);
																																																														{
																																																															BgL_vallocz00_bglt
																																																																BgL_auxz00_5173;
																																																															{
																																																																obj_t
																																																																	BgL_restz00_5180;
																																																																obj_t
																																																																	BgL_stackzf3zf3_5179;
																																																																obj_t
																																																																	BgL_czd2stackzd2fmtz00_5178;
																																																																obj_t
																																																																	BgL_czd2heapzd2fmtz00_5177;
																																																																obj_t
																																																																	BgL_otypez00_5176;
																																																																obj_t
																																																																	BgL_ftypez00_5175;
																																																																obj_t
																																																																	BgL_vtypez00_5174;
																																																																BgL_vtypez00_5174
																																																																	=
																																																																	BgL_arg2204z00_2353;
																																																																BgL_ftypez00_5175
																																																																	=
																																																																	BgL_arg2205z00_2354;
																																																																BgL_otypez00_5176
																																																																	=
																																																																	BgL_arg2206z00_2355;
																																																																BgL_czd2heapzd2fmtz00_5177
																																																																	=
																																																																	BgL_carzd213845zd2_2343;
																																																																BgL_czd2stackzd2fmtz00_5178
																																																																	=
																																																																	BgL_carzd213853zd2_2347;
																																																																BgL_stackzf3zf3_5179
																																																																	=
																																																																	BgL_carzd213860zd2_2351;
																																																																BgL_restz00_5180
																																																																	=
																																																																	BgL_arg2207z00_2356;
																																																																BgL_restz00_1451
																																																																	=
																																																																	BgL_restz00_5180;
																																																																BgL_stackzf3zf3_1450
																																																																	=
																																																																	BgL_stackzf3zf3_5179;
																																																																BgL_czd2stackzd2fmtz00_1449
																																																																	=
																																																																	BgL_czd2stackzd2fmtz00_5178;
																																																																BgL_czd2heapzd2fmtz00_1448
																																																																	=
																																																																	BgL_czd2heapzd2fmtz00_5177;
																																																																BgL_otypez00_1447
																																																																	=
																																																																	BgL_otypez00_5176;
																																																																BgL_ftypez00_1446
																																																																	=
																																																																	BgL_ftypez00_5175;
																																																																BgL_vtypez00_1445
																																																																	=
																																																																	BgL_vtypez00_5174;
																																																																goto
																																																																	BgL_tagzd2138zd2_1452;
																																																															}
																																																															return
																																																																(
																																																																(obj_t)
																																																																BgL_auxz00_5173);
																																																														}
																																																													}
																																																												else
																																																													{	/* Ast/private.scm 78 */
																																																														goto
																																																															BgL_tagzd2141zd2_1460;
																																																													}
																																																											}
																																																										else
																																																											{	/* Ast/private.scm 78 */
																																																												goto
																																																													BgL_tagzd2141zd2_1460;
																																																											}
																																																									}
																																																								else
																																																									{	/* Ast/private.scm 78 */
																																																										goto
																																																											BgL_tagzd2141zd2_1460;
																																																									}
																																																							}
																																																						else
																																																							{	/* Ast/private.scm 78 */
																																																								goto
																																																									BgL_tagzd2141zd2_1460;
																																																							}
																																																					}
																																																				else
																																																					{	/* Ast/private.scm 78 */
																																																						goto
																																																							BgL_tagzd2141zd2_1460;
																																																					}
																																																			}
																																																		}
																																																	}
																																																}
																																															else
																																																{	/* Ast/private.scm 78 */
																																																	goto
																																																		BgL_tagzd2141zd2_1460;
																																																}
																																														}
																																													}
																																											}
																																										else
																																											{	/* Ast/private.scm 78 */
																																												obj_t
																																													BgL_cdrzd213982zd2_2358;
																																												BgL_cdrzd213982zd2_2358
																																													=
																																													CDR
																																													(BgL_sexpz00_6);
																																												{	/* Ast/private.scm 78 */
																																													obj_t
																																														BgL_cdrzd213990zd2_2359;
																																													BgL_cdrzd213990zd2_2359
																																														=
																																														CDR
																																														(
																																														((obj_t) BgL_cdrzd213982zd2_2358));
																																													if ((CAR(((obj_t) BgL_cdrzd213982zd2_2358)) == CNST_TABLE_REF(14)))
																																														{	/* Ast/private.scm 78 */
																																															obj_t
																																																BgL_cdrzd213998zd2_2362;
																																															BgL_cdrzd213998zd2_2362
																																																=
																																																CDR
																																																(
																																																((obj_t) BgL_cdrzd213990zd2_2359));
																																															{	/* Ast/private.scm 78 */
																																																obj_t
																																																	BgL_cdrzd214006zd2_2363;
																																																BgL_cdrzd214006zd2_2363
																																																	=
																																																	CDR
																																																	(
																																																	((obj_t) BgL_cdrzd213998zd2_2362));
																																																if (NULLP(CDR(((obj_t) BgL_cdrzd214006zd2_2363))))
																																																	{	/* Ast/private.scm 78 */
																																																		obj_t
																																																			BgL_arg2213z00_2366;
																																																		obj_t
																																																			BgL_arg2214z00_2367;
																																																		obj_t
																																																			BgL_arg2215z00_2368;
																																																		BgL_arg2213z00_2366
																																																			=
																																																			CAR
																																																			(
																																																			((obj_t) BgL_cdrzd213990zd2_2359));
																																																		BgL_arg2214z00_2367
																																																			=
																																																			CAR
																																																			(
																																																			((obj_t) BgL_cdrzd213998zd2_2362));
																																																		BgL_arg2215z00_2368
																																																			=
																																																			CAR
																																																			(
																																																			((obj_t) BgL_cdrzd214006zd2_2363));
																																																		{
																																																			BgL_sequencez00_bglt
																																																				BgL_auxz00_5204;
																																																			{
																																																				obj_t
																																																					BgL_bodyz00_5207;
																																																				obj_t
																																																					BgL_kwdsz00_5206;
																																																				obj_t
																																																					BgL_typez00_5205;
																																																				BgL_typez00_5205
																																																					=
																																																					BgL_arg2213z00_2366;
																																																				BgL_kwdsz00_5206
																																																					=
																																																					BgL_arg2214z00_2367;
																																																				BgL_bodyz00_5207
																																																					=
																																																					BgL_arg2215z00_2368;
																																																				BgL_bodyz00_1458
																																																					=
																																																					BgL_bodyz00_5207;
																																																				BgL_kwdsz00_1457
																																																					=
																																																					BgL_kwdsz00_5206;
																																																				BgL_typez00_1456
																																																					=
																																																					BgL_typez00_5205;
																																																				goto
																																																					BgL_tagzd2140zd2_1459;
																																																			}
																																																			return
																																																				(
																																																				(obj_t)
																																																				BgL_auxz00_5204);
																																																		}
																																																	}
																																																else
																																																	{	/* Ast/private.scm 78 */
																																																		goto
																																																			BgL_tagzd2141zd2_1460;
																																																	}
																																															}
																																														}
																																													else
																																														{	/* Ast/private.scm 78 */
																																															goto
																																																BgL_tagzd2141zd2_1460;
																																														}
																																												}
																																											}
																																									}
																																								else
																																									{	/* Ast/private.scm 78 */
																																										obj_t
																																											BgL_cdrzd214044zd2_2371;
																																										BgL_cdrzd214044zd2_2371
																																											=
																																											CDR
																																											(BgL_sexpz00_6);
																																										{	/* Ast/private.scm 78 */
																																											obj_t
																																												BgL_cdrzd214050zd2_2372;
																																											BgL_cdrzd214050zd2_2372
																																												=
																																												CDR
																																												(
																																												((obj_t) BgL_cdrzd214044zd2_2371));
																																											if ((CAR(((obj_t) BgL_cdrzd214044zd2_2371)) == CNST_TABLE_REF(18)))
																																												{	/* Ast/private.scm 78 */
																																													obj_t
																																														BgL_cdrzd214056zd2_2375;
																																													BgL_cdrzd214056zd2_2375
																																														=
																																														CDR
																																														(
																																														((obj_t) BgL_cdrzd214050zd2_2372));
																																													if (NULLP(CDR(((obj_t) BgL_cdrzd214056zd2_2375))))
																																														{	/* Ast/private.scm 78 */
																																															obj_t
																																																BgL_arg2222z00_2378;
																																															obj_t
																																																BgL_arg2223z00_2379;
																																															BgL_arg2222z00_2378
																																																=
																																																CAR
																																																(
																																																((obj_t) BgL_cdrzd214050zd2_2372));
																																															BgL_arg2223z00_2379
																																																=
																																																CAR
																																																(
																																																((obj_t) BgL_cdrzd214056zd2_2375));
																																															{
																																																BgL_sequencez00_bglt
																																																	BgL_auxz00_5227;
																																																{
																																																	obj_t
																																																		BgL_expz00_5229;
																																																	obj_t
																																																		BgL_typez00_5228;
																																																	BgL_typez00_5228
																																																		=
																																																		BgL_arg2222z00_2378;
																																																	BgL_expz00_5229
																																																		=
																																																		BgL_arg2223z00_2379;
																																																	BgL_expz00_1454
																																																		=
																																																		BgL_expz00_5229;
																																																	BgL_typez00_1453
																																																		=
																																																		BgL_typez00_5228;
																																																	goto
																																																		BgL_tagzd2139zd2_1455;
																																																}
																																																return
																																																	(
																																																	(obj_t)
																																																	BgL_auxz00_5227);
																																															}
																																														}
																																													else
																																														{	/* Ast/private.scm 78 */
																																															goto
																																																BgL_tagzd2141zd2_1460;
																																														}
																																												}
																																											else
																																												{	/* Ast/private.scm 78 */
																																													goto
																																														BgL_tagzd2141zd2_1460;
																																												}
																																										}
																																									}
																																							}
																																						else
																																							{	/* Ast/private.scm 78 */
																																								goto
																																									BgL_tagzd2141zd2_1460;
																																							}
																																					}
																																				else
																																					{	/* Ast/private.scm 78 */
																																						goto
																																							BgL_tagzd2141zd2_1460;
																																					}
																																			}
																																		else
																																			{	/* Ast/private.scm 78 */
																																				if (
																																					(BgL_carzd213738zd2_2265
																																						==
																																						CNST_TABLE_REF
																																						(5)))
																																					{	/* Ast/private.scm 78 */
																																						goto
																																							BgL_kapzd213740zd2_2267;
																																					}
																																				else
																																					{	/* Ast/private.scm 78 */
																																						obj_t
																																							BgL_cdrzd214170zd2_2269;
																																						BgL_cdrzd214170zd2_2269
																																							=
																																							CDR
																																							(((obj_t) BgL_cdrzd213732zd2_2264));
																																						if (
																																							(CAR
																																								(((obj_t) BgL_cdrzd213732zd2_2264)) == CNST_TABLE_REF(12)))
																																							{	/* Ast/private.scm 78 */
																																								if (PAIRP(BgL_cdrzd214170zd2_2269))
																																									{	/* Ast/private.scm 78 */
																																										obj_t
																																											BgL_cdrzd214179zd2_2273;
																																										BgL_cdrzd214179zd2_2273
																																											=
																																											CDR
																																											(BgL_cdrzd214170zd2_2269);
																																										if (PAIRP(BgL_cdrzd214179zd2_2273))
																																											{	/* Ast/private.scm 78 */
																																												obj_t
																																													BgL_cdrzd214188zd2_2275;
																																												BgL_cdrzd214188zd2_2275
																																													=
																																													CDR
																																													(BgL_cdrzd214179zd2_2273);
																																												if (PAIRP(BgL_cdrzd214188zd2_2275))
																																													{	/* Ast/private.scm 78 */
																																														obj_t
																																															BgL_cdrzd214196zd2_2277;
																																														BgL_cdrzd214196zd2_2277
																																															=
																																															CDR
																																															(BgL_cdrzd214188zd2_2275);
																																														if (PAIRP(BgL_cdrzd214196zd2_2277))
																																															{	/* Ast/private.scm 78 */
																																																obj_t
																																																	BgL_carzd214202zd2_2279;
																																																obj_t
																																																	BgL_cdrzd214203zd2_2280;
																																																BgL_carzd214202zd2_2279
																																																	=
																																																	CAR
																																																	(BgL_cdrzd214196zd2_2277);
																																																BgL_cdrzd214203zd2_2280
																																																	=
																																																	CDR
																																																	(BgL_cdrzd214196zd2_2277);
																																																if (STRINGP(BgL_carzd214202zd2_2279))
																																																	{	/* Ast/private.scm 78 */
																																																		if (PAIRP(BgL_cdrzd214203zd2_2280))
																																																			{	/* Ast/private.scm 78 */
																																																				obj_t
																																																					BgL_carzd214210zd2_2283;
																																																				obj_t
																																																					BgL_cdrzd214211zd2_2284;
																																																				BgL_carzd214210zd2_2283
																																																					=
																																																					CAR
																																																					(BgL_cdrzd214203zd2_2280);
																																																				BgL_cdrzd214211zd2_2284
																																																					=
																																																					CDR
																																																					(BgL_cdrzd214203zd2_2280);
																																																				if (STRINGP(BgL_carzd214210zd2_2283))
																																																					{	/* Ast/private.scm 78 */
																																																						if (PAIRP(BgL_cdrzd214211zd2_2284))
																																																							{	/* Ast/private.scm 78 */
																																																								obj_t
																																																									BgL_carzd214217zd2_2287;
																																																								BgL_carzd214217zd2_2287
																																																									=
																																																									CAR
																																																									(BgL_cdrzd214211zd2_2284);
																																																								if (BOOLEANP(BgL_carzd214217zd2_2287))
																																																									{	/* Ast/private.scm 78 */
																																																										obj_t
																																																											BgL_arg2160z00_2289;
																																																										obj_t
																																																											BgL_arg2161z00_2290;
																																																										obj_t
																																																											BgL_arg2162z00_2291;
																																																										obj_t
																																																											BgL_arg2163z00_2292;
																																																										BgL_arg2160z00_2289
																																																											=
																																																											CAR
																																																											(BgL_cdrzd214170zd2_2269);
																																																										BgL_arg2161z00_2290
																																																											=
																																																											CAR
																																																											(BgL_cdrzd214179zd2_2273);
																																																										BgL_arg2162z00_2291
																																																											=
																																																											CAR
																																																											(BgL_cdrzd214188zd2_2275);
																																																										BgL_arg2163z00_2292
																																																											=
																																																											CDR
																																																											(BgL_cdrzd214211zd2_2284);
																																																										{
																																																											BgL_vallocz00_bglt
																																																												BgL_auxz00_5271;
																																																											{
																																																												obj_t
																																																													BgL_restz00_5278;
																																																												obj_t
																																																													BgL_stackzf3zf3_5277;
																																																												obj_t
																																																													BgL_czd2stackzd2fmtz00_5276;
																																																												obj_t
																																																													BgL_czd2heapzd2fmtz00_5275;
																																																												obj_t
																																																													BgL_otypez00_5274;
																																																												obj_t
																																																													BgL_ftypez00_5273;
																																																												obj_t
																																																													BgL_vtypez00_5272;
																																																												BgL_vtypez00_5272
																																																													=
																																																													BgL_arg2160z00_2289;
																																																												BgL_ftypez00_5273
																																																													=
																																																													BgL_arg2161z00_2290;
																																																												BgL_otypez00_5274
																																																													=
																																																													BgL_arg2162z00_2291;
																																																												BgL_czd2heapzd2fmtz00_5275
																																																													=
																																																													BgL_carzd214202zd2_2279;
																																																												BgL_czd2stackzd2fmtz00_5276
																																																													=
																																																													BgL_carzd214210zd2_2283;
																																																												BgL_stackzf3zf3_5277
																																																													=
																																																													BgL_carzd214217zd2_2287;
																																																												BgL_restz00_5278
																																																													=
																																																													BgL_arg2163z00_2292;
																																																												BgL_restz00_1451
																																																													=
																																																													BgL_restz00_5278;
																																																												BgL_stackzf3zf3_1450
																																																													=
																																																													BgL_stackzf3zf3_5277;
																																																												BgL_czd2stackzd2fmtz00_1449
																																																													=
																																																													BgL_czd2stackzd2fmtz00_5276;
																																																												BgL_czd2heapzd2fmtz00_1448
																																																													=
																																																													BgL_czd2heapzd2fmtz00_5275;
																																																												BgL_otypez00_1447
																																																													=
																																																													BgL_otypez00_5274;
																																																												BgL_ftypez00_1446
																																																													=
																																																													BgL_ftypez00_5273;
																																																												BgL_vtypez00_1445
																																																													=
																																																													BgL_vtypez00_5272;
																																																												goto
																																																													BgL_tagzd2138zd2_1452;
																																																											}
																																																											return
																																																												(
																																																												(obj_t)
																																																												BgL_auxz00_5271);
																																																										}
																																																									}
																																																								else
																																																									{	/* Ast/private.scm 78 */
																																																										goto
																																																											BgL_tagzd2141zd2_1460;
																																																									}
																																																							}
																																																						else
																																																							{	/* Ast/private.scm 78 */
																																																								goto
																																																									BgL_tagzd2141zd2_1460;
																																																							}
																																																					}
																																																				else
																																																					{	/* Ast/private.scm 78 */
																																																						goto
																																																							BgL_tagzd2141zd2_1460;
																																																					}
																																																			}
																																																		else
																																																			{	/* Ast/private.scm 78 */
																																																				goto
																																																					BgL_tagzd2141zd2_1460;
																																																			}
																																																	}
																																																else
																																																	{	/* Ast/private.scm 78 */
																																																		goto
																																																			BgL_tagzd2141zd2_1460;
																																																	}
																																															}
																																														else
																																															{	/* Ast/private.scm 78 */
																																																goto
																																																	BgL_tagzd2141zd2_1460;
																																															}
																																													}
																																												else
																																													{	/* Ast/private.scm 78 */
																																														goto
																																															BgL_tagzd2141zd2_1460;
																																													}
																																											}
																																										else
																																											{	/* Ast/private.scm 78 */
																																												goto
																																													BgL_tagzd2141zd2_1460;
																																											}
																																									}
																																								else
																																									{	/* Ast/private.scm 78 */
																																										goto
																																											BgL_tagzd2141zd2_1460;
																																									}
																																							}
																																						else
																																							{	/* Ast/private.scm 78 */
																																								obj_t
																																									BgL_cdrzd214338zd2_2293;
																																								BgL_cdrzd214338zd2_2293
																																									=
																																									CDR
																																									(BgL_sexpz00_6);
																																								{	/* Ast/private.scm 78 */
																																									obj_t
																																										BgL_cdrzd214342zd2_2294;
																																									BgL_cdrzd214342zd2_2294
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_cdrzd214338zd2_2293));
																																									if ((CAR(((obj_t) BgL_cdrzd214338zd2_2293)) == CNST_TABLE_REF(18)))
																																										{	/* Ast/private.scm 78 */
																																											if (PAIRP(BgL_cdrzd214342zd2_2294))
																																												{	/* Ast/private.scm 78 */
																																													obj_t
																																														BgL_cdrzd214346zd2_2298;
																																													BgL_cdrzd214346zd2_2298
																																														=
																																														CDR
																																														(BgL_cdrzd214342zd2_2294);
																																													if (PAIRP(BgL_cdrzd214346zd2_2298))
																																														{	/* Ast/private.scm 78 */
																																															if (NULLP(CDR(BgL_cdrzd214346zd2_2298)))
																																																{	/* Ast/private.scm 78 */
																																																	obj_t
																																																		BgL_arg2170z00_2302;
																																																	obj_t
																																																		BgL_arg2171z00_2303;
																																																	BgL_arg2170z00_2302
																																																		=
																																																		CAR
																																																		(BgL_cdrzd214342zd2_2294);
																																																	BgL_arg2171z00_2303
																																																		=
																																																		CAR
																																																		(BgL_cdrzd214346zd2_2298);
																																																	{
																																																		BgL_sequencez00_bglt
																																																			BgL_auxz00_5298;
																																																		{
																																																			obj_t
																																																				BgL_expz00_5300;
																																																			obj_t
																																																				BgL_typez00_5299;
																																																			BgL_typez00_5299
																																																				=
																																																				BgL_arg2170z00_2302;
																																																			BgL_expz00_5300
																																																				=
																																																				BgL_arg2171z00_2303;
																																																			BgL_expz00_1454
																																																				=
																																																				BgL_expz00_5300;
																																																			BgL_typez00_1453
																																																				=
																																																				BgL_typez00_5299;
																																																			goto
																																																				BgL_tagzd2139zd2_1455;
																																																		}
																																																		return
																																																			(
																																																			(obj_t)
																																																			BgL_auxz00_5298);
																																																	}
																																																}
																																															else
																																																{	/* Ast/private.scm 78 */
																																																	goto
																																																		BgL_tagzd2141zd2_1460;
																																																}
																																														}
																																													else
																																														{	/* Ast/private.scm 78 */
																																															goto
																																																BgL_tagzd2141zd2_1460;
																																														}
																																												}
																																											else
																																												{	/* Ast/private.scm 78 */
																																													goto
																																														BgL_tagzd2141zd2_1460;
																																												}
																																										}
																																									else
																																										{	/* Ast/private.scm 78 */
																																											obj_t
																																												BgL_cdrzd214382zd2_2306;
																																											BgL_cdrzd214382zd2_2306
																																												=
																																												CDR
																																												(
																																												((obj_t) BgL_cdrzd214338zd2_2293));
																																											if ((CAR(((obj_t) BgL_cdrzd214338zd2_2293)) == CNST_TABLE_REF(14)))
																																												{	/* Ast/private.scm 78 */
																																													if (PAIRP(BgL_cdrzd214382zd2_2306))
																																														{	/* Ast/private.scm 78 */
																																															obj_t
																																																BgL_cdrzd214387zd2_2310;
																																															BgL_cdrzd214387zd2_2310
																																																=
																																																CDR
																																																(BgL_cdrzd214382zd2_2306);
																																															if (PAIRP(BgL_cdrzd214387zd2_2310))
																																																{	/* Ast/private.scm 78 */
																																																	obj_t
																																																		BgL_cdrzd214392zd2_2312;
																																																	BgL_cdrzd214392zd2_2312
																																																		=
																																																		CDR
																																																		(BgL_cdrzd214387zd2_2310);
																																																	if (PAIRP(BgL_cdrzd214392zd2_2312))
																																																		{	/* Ast/private.scm 78 */
																																																			if (NULLP(CDR(BgL_cdrzd214392zd2_2312)))
																																																				{	/* Ast/private.scm 78 */
																																																					obj_t
																																																						BgL_arg2180z00_2316;
																																																					obj_t
																																																						BgL_arg2181z00_2317;
																																																					obj_t
																																																						BgL_arg2182z00_2318;
																																																					BgL_arg2180z00_2316
																																																						=
																																																						CAR
																																																						(BgL_cdrzd214382zd2_2306);
																																																					BgL_arg2181z00_2317
																																																						=
																																																						CAR
																																																						(BgL_cdrzd214387zd2_2310);
																																																					BgL_arg2182z00_2318
																																																						=
																																																						CAR
																																																						(BgL_cdrzd214392zd2_2312);
																																																					{
																																																						BgL_sequencez00_bglt
																																																							BgL_auxz00_5323;
																																																						{
																																																							obj_t
																																																								BgL_bodyz00_5326;
																																																							obj_t
																																																								BgL_kwdsz00_5325;
																																																							obj_t
																																																								BgL_typez00_5324;
																																																							BgL_typez00_5324
																																																								=
																																																								BgL_arg2180z00_2316;
																																																							BgL_kwdsz00_5325
																																																								=
																																																								BgL_arg2181z00_2317;
																																																							BgL_bodyz00_5326
																																																								=
																																																								BgL_arg2182z00_2318;
																																																							BgL_bodyz00_1458
																																																								=
																																																								BgL_bodyz00_5326;
																																																							BgL_kwdsz00_1457
																																																								=
																																																								BgL_kwdsz00_5325;
																																																							BgL_typez00_1456
																																																								=
																																																								BgL_typez00_5324;
																																																							goto
																																																								BgL_tagzd2140zd2_1459;
																																																						}
																																																						return
																																																							(
																																																							(obj_t)
																																																							BgL_auxz00_5323);
																																																					}
																																																				}
																																																			else
																																																				{	/* Ast/private.scm 78 */
																																																					goto
																																																						BgL_tagzd2141zd2_1460;
																																																				}
																																																		}
																																																	else
																																																		{	/* Ast/private.scm 78 */
																																																			goto
																																																				BgL_tagzd2141zd2_1460;
																																																		}
																																																}
																																															else
																																																{	/* Ast/private.scm 78 */
																																																	goto
																																																		BgL_tagzd2141zd2_1460;
																																																}
																																														}
																																													else
																																														{	/* Ast/private.scm 78 */
																																															goto
																																																BgL_tagzd2141zd2_1460;
																																														}
																																												}
																																											else
																																												{	/* Ast/private.scm 78 */
																																													goto
																																														BgL_tagzd2141zd2_1460;
																																												}
																																										}
																																								}
																																							}
																																					}
																																			}
																																	}
																																}
																															}
																													}
																											}
																										}
																								}
																							}
																					}
																			}
																		}
																}
															}
													}
											}
										}
								}
						}
					else
						{	/* Ast/private.scm 78 */
							goto BgL_tagzd2141zd2_1460;
						}
				}
			}
		}

	}



/* &private-node */
	obj_t BGl_z62privatezd2nodezb0zzast_privatez00(obj_t BgL_envz00_3681,
		obj_t BgL_sexpz00_3682, obj_t BgL_stackz00_3683, obj_t BgL_locz00_3684,
		obj_t BgL_sitez00_3685)
	{
		{	/* Ast/private.scm 73 */
			return
				BGl_privatezd2nodezd2zzast_privatez00(BgL_sexpz00_3682,
				BgL_stackz00_3683, BgL_locz00_3684, BgL_sitez00_3685);
		}

	}



/* make-private-sexp */
	BGL_EXPORTED_DEF obj_t BGl_makezd2privatezd2sexpz00zzast_privatez00(obj_t
		BgL_kindz00_10, obj_t BgL_typezd2idzd2_11, obj_t BgL_objsz00_12)
	{
		{	/* Ast/private.scm 241 */
			{	/* Ast/private.scm 247 */
				obj_t BgL_arg2567z00_3661;

				{	/* Ast/private.scm 247 */
					obj_t BgL_arg2568z00_3662;

					BgL_arg2568z00_3662 =
						MAKE_YOUNG_PAIR(BgL_typezd2idzd2_11, BgL_objsz00_12);
					BgL_arg2567z00_3661 =
						MAKE_YOUNG_PAIR(BgL_kindz00_10, BgL_arg2568z00_3662);
				}
				return MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BgL_arg2567z00_3661);
			}
		}

	}



/* &make-private-sexp */
	obj_t BGl_z62makezd2privatezd2sexpz62zzast_privatez00(obj_t BgL_envz00_3686,
		obj_t BgL_kindz00_3687, obj_t BgL_typezd2idzd2_3688, obj_t BgL_objsz00_3689)
	{
		{	/* Ast/private.scm 241 */
			return
				BGl_makezd2privatezd2sexpz00zzast_privatez00(BgL_kindz00_3687,
				BgL_typezd2idzd2_3688, BgL_objsz00_3689);
		}

	}



/* expand-meta */
	BGL_EXPORTED_DEF obj_t BGl_expandzd2metazd2zzast_privatez00(obj_t BgL_xz00_13,
		obj_t BgL_ez00_14)
	{
		{	/* Ast/private.scm 252 */
			{
				obj_t BgL_keywordsz00_2934;
				obj_t BgL_bodyz00_2935;

				{	/* Ast/private.scm 253 */
					obj_t BgL_cdrzd214748zd2_2940;

					BgL_cdrzd214748zd2_2940 = CDR(BgL_xz00_13);
					if (PAIRP(BgL_cdrzd214748zd2_2940))
						{	/* Ast/private.scm 253 */
							obj_t BgL_carzd214751zd2_2942;

							BgL_carzd214751zd2_2942 = CAR(BgL_cdrzd214748zd2_2940);
							if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
								(BgL_carzd214751zd2_2942))
								{	/* Ast/private.scm 253 */
									BgL_keywordsz00_2934 = BgL_carzd214751zd2_2942;
									BgL_bodyz00_2935 = CDR(BgL_cdrzd214748zd2_2940);
									{	/* Ast/private.scm 255 */
										obj_t BgL_arg2578z00_2945;

										if (NULLP(BgL_bodyz00_2935))
											{	/* Ast/private.scm 255 */
												BgL_arg2578z00_2945 = BNIL;
											}
										else
											{	/* Ast/private.scm 255 */
												obj_t BgL_head1275z00_2950;

												BgL_head1275z00_2950 = MAKE_YOUNG_PAIR(BNIL, BNIL);
												{
													obj_t BgL_l1273z00_2952;
													obj_t BgL_tail1276z00_2953;

													BgL_l1273z00_2952 = BgL_bodyz00_2935;
													BgL_tail1276z00_2953 = BgL_head1275z00_2950;
												BgL_zc3z04anonymousza32586ze3z87_2954:
													if (NULLP(BgL_l1273z00_2952))
														{	/* Ast/private.scm 255 */
															BgL_arg2578z00_2945 = CDR(BgL_head1275z00_2950);
														}
													else
														{	/* Ast/private.scm 255 */
															obj_t BgL_newtail1277z00_2956;

															{	/* Ast/private.scm 255 */
																obj_t BgL_arg2590z00_2958;

																{	/* Ast/private.scm 255 */
																	obj_t BgL_xz00_2959;

																	BgL_xz00_2959 =
																		CAR(((obj_t) BgL_l1273z00_2952));
																	BgL_arg2590z00_2958 =
																		BGL_PROCEDURE_CALL2(BgL_ez00_14,
																		BgL_xz00_2959, BgL_ez00_14);
																}
																BgL_newtail1277z00_2956 =
																	MAKE_YOUNG_PAIR(BgL_arg2590z00_2958, BNIL);
															}
															SET_CDR(BgL_tail1276z00_2953,
																BgL_newtail1277z00_2956);
															{	/* Ast/private.scm 255 */
																obj_t BgL_arg2589z00_2957;

																BgL_arg2589z00_2957 =
																	CDR(((obj_t) BgL_l1273z00_2952));
																{
																	obj_t BgL_tail1276z00_5358;
																	obj_t BgL_l1273z00_5357;

																	BgL_l1273z00_5357 = BgL_arg2589z00_2957;
																	BgL_tail1276z00_5358 =
																		BgL_newtail1277z00_2956;
																	BgL_tail1276z00_2953 = BgL_tail1276z00_5358;
																	BgL_l1273z00_2952 = BgL_l1273z00_5357;
																	goto BgL_zc3z04anonymousza32586ze3z87_2954;
																}
															}
														}
												}
											}
										{	/* Ast/private.scm 255 */
											obj_t BgL_list2579z00_2946;

											{	/* Ast/private.scm 255 */
												obj_t BgL_arg2584z00_2947;

												BgL_arg2584z00_2947 =
													MAKE_YOUNG_PAIR(BgL_arg2578z00_2945, BNIL);
												BgL_list2579z00_2946 =
													MAKE_YOUNG_PAIR(BgL_keywordsz00_2934,
													BgL_arg2584z00_2947);
											}
											{	/* Ast/private.scm 255 */
												obj_t BgL_kindz00_3667;
												obj_t BgL_typezd2idzd2_3668;

												BgL_kindz00_3667 = CNST_TABLE_REF(14);
												BgL_typezd2idzd2_3668 = CNST_TABLE_REF(19);
												{	/* Ast/private.scm 247 */
													obj_t BgL_arg2567z00_3669;

													{	/* Ast/private.scm 247 */
														obj_t BgL_arg2568z00_3670;

														BgL_arg2568z00_3670 =
															MAKE_YOUNG_PAIR(BgL_typezd2idzd2_3668,
															BgL_list2579z00_2946);
														BgL_arg2567z00_3669 =
															MAKE_YOUNG_PAIR(BgL_kindz00_3667,
															BgL_arg2568z00_3670);
													}
													return
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
														BgL_arg2567z00_3669);
												}
											}
										}
									}
								}
							else
								{	/* Ast/private.scm 253 */
								BgL_tagzd214741zd2_2937:
									return
										BGl_errorz00zz__errorz00(BGl_string2667z00zzast_privatez00,
										BGl_string2668z00zzast_privatez00, BgL_xz00_13);
								}
						}
					else
						{	/* Ast/private.scm 253 */
							goto BgL_tagzd214741zd2_2937;
						}
				}
			}
		}

	}



/* &expand-meta */
	obj_t BGl_z62expandzd2metazb0zzast_privatez00(obj_t BgL_envz00_3690,
		obj_t BgL_xz00_3691, obj_t BgL_ez00_3692)
	{
		{	/* Ast/private.scm 252 */
			return BGl_expandzd2metazd2zzast_privatez00(BgL_xz00_3691, BgL_ez00_3692);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_privatez00(void)
	{
		{	/* Ast/private.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_privatez00(void)
	{
		{	/* Ast/private.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_privatez00(void)
	{
		{	/* Ast/private.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_privatez00(void)
	{
		{	/* Ast/private.scm 15 */
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2669z00zzast_privatez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2669z00zzast_privatez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2669z00zzast_privatez00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2669z00zzast_privatez00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string2669z00zzast_privatez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2669z00zzast_privatez00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string2669z00zzast_privatez00));
			return
				BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2669z00zzast_privatez00));
		}

	}

#ifdef __cplusplus
}
#endif
