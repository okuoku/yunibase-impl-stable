/*===========================================================================*/
/*   (Ast/alphatize.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/alphatize.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_AST_ALPHATIZE_TYPE_DEFINITIONS
#define BGL_AST_ALPHATIZE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_sexitz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_handlerz00;
		bool_t BgL_detachedzf3zf3;
	}               *BgL_sexitz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_literalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}                 *BgL_literalz00_bglt;

	typedef struct BgL_patchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
		struct BgL_varz00_bgl *BgL_refz00;
		long BgL_indexz00;
		obj_t BgL_patchidz00;
	}               *BgL_patchz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_pragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_formatz00;
		obj_t BgL_srfi0z00;
	}                *BgL_pragmaz00_bglt;

	typedef struct BgL_privatez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
	}                 *BgL_privatez00_bglt;

	typedef struct BgL_getfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_getfieldz00_bglt;

	typedef struct BgL_setfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_setfieldz00_bglt;

	typedef struct BgL_newz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_argszd2typezd2;
	}             *BgL_newz00_bglt;

	typedef struct BgL_vallocz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                *BgL_vallocz00_bglt;

	typedef struct BgL_vrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}              *BgL_vrefz00_bglt;

	typedef struct BgL_vsetz12z12_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}                 *BgL_vsetz12z12_bglt;

	typedef struct BgL_vlengthz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_ftypez00;
	}                 *BgL_vlengthz00_bglt;

	typedef struct BgL_instanceofz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_classz00;
	}                    *BgL_instanceofz00_bglt;

	typedef struct BgL_castzd2nullzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
	}                     *BgL_castzd2nullzd2_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_retblockz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                  *BgL_retblockz00_bglt;

	typedef struct BgL_returnz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_retblockz00_bgl *BgL_blockz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                *BgL_returnz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_retblockzf2alphazf2_bgl
	{
		struct BgL_retblockz00_bgl *BgL_alphaz00;
	}                          *BgL_retblockzf2alphazf2_bglt;


#endif													// BGL_AST_ALPHATIZE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2valloc1643zc5zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	static BgL_retblockz00_bglt BGl_z62lambda1776z62zzast_alphatiza7eza7(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1777z62zzast_alphatiza7eza7(obj_t, obj_t, obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	extern obj_t BGl_returnz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzast_alphatiza7eza7 = BUNSPEC;
	extern obj_t BGl_closurez00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2letzd2fun1663z17zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_funz00zzast_varz00;
	extern obj_t BGl_newz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2appzd2ly1622z17zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_vlengthz00zzast_nodez00;
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_alphatiza7eza7zzast_alphatiza7eza7(obj_t, obj_t, obj_t,
		BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_getzd2locationzd2zzast_alphatiza7eza7(BgL_nodez00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t BGl_toplevelzd2initzd2zzast_alphatiza7eza7(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2literal1606zc5zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzast_alphatiza7eza7(void);
	extern obj_t BGl_getfieldz00zzast_nodez00;
	static obj_t BGl_retblockzf2alphazf2zzast_alphatiza7eza7 = BUNSPEC;
	extern obj_t BGl_sexitz00zzast_varz00;
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzast_alphatiza7eza7(void);
	extern obj_t BGl_castz00zzast_nodez00;
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	extern obj_t BGl_typez00zztype_typez00;
	static obj_t BGl_z62dozd2alphatiza7e1603z17zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2return1673zc5zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2switch1655zc5zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	extern BgL_localz00_bglt BGl_makezd2localzd2sfunz00zzast_localz00(obj_t,
		BgL_typez00_bglt, BgL_sfunz00_bglt);
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2funcall1624zc5zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2kwote1614zc5zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzast_alphatiza7eza7(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzast_alphatiza7eza7(void);
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2setzd2exzd21667zc5zzast_alphatiza7eza7(obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2patch1608zc5zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2castzd2nu1628z17zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2conditi1651zc5zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2closure1612zc5zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_alphatiza7ezd2sanszd2closureza7zzast_alphatiza7eza7(obj_t, obj_t, obj_t,
		BgL_nodez00_bglt, BgL_variablez00_bglt);
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2retbloc1671zc5zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2letzd2var1665z17zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2boxzd2set1661z17zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2cast1647zc5zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2boxzd2ref1659z17zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2sync1618zc5zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	extern bool_t BGl_globalzd2optionalzf3z21zzast_varz00(obj_t);
	static obj_t BGl_za2nozd2alphatiza7ezd2closureza2za7zzast_alphatiza7eza7 =
		BUNSPEC;
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2setfiel1633zc5zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2new1635zc5zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	extern BgL_localz00_bglt BGl_makezd2localzd2sexitz00zzast_localz00(obj_t,
		BgL_typez00_bglt, BgL_sexitz00_bglt);
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2sequenc1616zc5zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_pragmaz00zzast_nodez00;
	extern BgL_typez00_bglt
		BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_za2procedureza2z00zztype_cachez00;
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2setq1649zc5zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2instanc1645zc5zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(BgL_nodez00_bglt, obj_t);
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2fail1653zc5zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	extern bool_t BGl_globalzd2keyzf3z21zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzast_alphatiza7eza7(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_appz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_applyz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2pragma1626zc5zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	extern obj_t BGl_vrefz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2makezd2bo1657z17zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2var1610zc5zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzast_alphatiza7eza7(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_alphatiza7eza7(void);
	extern BgL_localz00_bglt BGl_makezd2localzd2svarz00zzast_localz00(obj_t,
		BgL_typez00_bglt);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzast_alphatiza7eza7(void);
	static obj_t BGl_gczd2rootszd2initz00zzast_alphatiza7eza7(void);
	static obj_t BGl_z62zc3z04anonymousza31715ze3ze5zzast_alphatiza7eza7(obj_t);
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2getfiel1631zc5zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_vallocz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62alphatiza7ezd2sanszd2closurezc5zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_retblockz00zzast_nodez00;
	extern obj_t BGl_instanceofz00zzast_nodez00;
	extern BgL_nodez00_bglt BGl_sexpzd2ze3nodez31zzast_sexpz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_dozd2alphatiza7eza2zd7zzast_alphatiza7eza7(obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern BgL_nodez00_bglt BGl_knownzd2appzd2lyzd2ze3nodez31zzast_applyz00(obj_t,
		obj_t, BgL_nodez00_bglt, BgL_nodez00_bglt, obj_t);
	extern obj_t BGl_setfieldz00zzast_nodez00;
	extern obj_t BGl_castzd2nullzd2zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2vref1639zc5zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2app1620zc5zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_usezd2variablez12zc0zzast_sexpz00(BgL_variablez00_bglt,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2vsetz121641zd7zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_variablez00zzast_varz00;
	extern obj_t BGl_literalz00zzast_nodez00;
	extern obj_t BGl_switchz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2vlength1637zc5zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2jumpzd2ex1669z17zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31762ze3ze5zzast_alphatiza7eza7(obj_t,
		obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static BgL_retblockz00_bglt BGl_z62lambda1753z62zzast_alphatiza7eza7(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_retblockz00_bglt BGl_z62lambda1756z62zzast_alphatiza7eza7(obj_t,
		obj_t);
	extern obj_t BGl_vsetz12z12zzast_nodez00;
	static BgL_nodez00_bglt BGl_z62dozd2alphatiza7ez17zzast_alphatiza7eza7(obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62alphatiza7ezc5zzast_alphatiza7eza7(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_patchz00zzast_nodez00;
	static BgL_retblockz00_bglt BGl_z62lambda1763z62zzast_alphatiza7eza7(obj_t,
		obj_t);
	static obj_t __cnst[8];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2144z00zzast_alphatiza7eza7,
		BgL_bgl_za762za7c3za704anonymo2197za7,
		BGl_z62zc3z04anonymousza31715ze3ze5zzast_alphatiza7eza7, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2145z00zzast_alphatiza7eza7,
		BgL_bgl_za762lambda1777za7622198z00,
		BGl_z62lambda1777z62zzast_alphatiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2152z00zzast_alphatiza7eza7,
		BgL_bgl_string2152za700za7za7a2199za7, "do-alphatize1603", 16);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2146z00zzast_alphatiza7eza7,
		BgL_bgl_za762lambda1776za7622200z00,
		BGl_z62lambda1776z62zzast_alphatiza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2153z00zzast_alphatiza7eza7,
		BgL_bgl_string2153za700za7za7a2201za7, "No method for this object", 25);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2147z00zzast_alphatiza7eza7,
		BgL_bgl_za762lambda1763za7622202z00,
		BGl_z62lambda1763z62zzast_alphatiza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2148z00zzast_alphatiza7eza7,
		BgL_bgl_za762za7c3za704anonymo2203za7,
		BGl_z62zc3z04anonymousza31762ze3ze5zzast_alphatiza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2155z00zzast_alphatiza7eza7,
		BgL_bgl_string2155za700za7za7a2204za7, "do-alphatize", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2149z00zzast_alphatiza7eza7,
		BgL_bgl_za762lambda1756za7622205z00,
		BGl_z62lambda1756z62zzast_alphatiza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2150z00zzast_alphatiza7eza7,
		BgL_bgl_za762lambda1753za7622206z00,
		BGl_z62lambda1753z62zzast_alphatiza7eza7, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2151z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72207za7,
		BGl_z62dozd2alphatiza7e1603z17zzast_alphatiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2154z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72208za7,
		BGl_z62dozd2alphatiza7ezd2literal1606zc5zzast_alphatiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2156z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72209za7,
		BGl_z62dozd2alphatiza7ezd2patch1608zc5zzast_alphatiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2157z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72210za7,
		BGl_z62dozd2alphatiza7ezd2var1610zc5zzast_alphatiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2158z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72211za7,
		BGl_z62dozd2alphatiza7ezd2closure1612zc5zzast_alphatiza7eza7, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_alphatiza7ezd2sanszd2closurezd2envz75zzast_alphatiza7eza7,
		BgL_bgl_za762alphatiza7a7eza7d2212za7,
		BGl_z62alphatiza7ezd2sanszd2closurezc5zzast_alphatiza7eza7, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2159z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72213za7,
		BGl_z62dozd2alphatiza7ezd2kwote1614zc5zzast_alphatiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2160z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72214za7,
		BGl_z62dozd2alphatiza7ezd2sequenc1616zc5zzast_alphatiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2161z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72215za7,
		BGl_z62dozd2alphatiza7ezd2sync1618zc5zzast_alphatiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2162z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72216za7,
		BGl_z62dozd2alphatiza7ezd2app1620zc5zzast_alphatiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2163z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72217za7,
		BGl_z62dozd2alphatiza7ezd2appzd2ly1622z17zzast_alphatiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2164z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72218za7,
		BGl_z62dozd2alphatiza7ezd2funcall1624zc5zzast_alphatiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2165z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72219za7,
		BGl_z62dozd2alphatiza7ezd2pragma1626zc5zzast_alphatiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2166z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72220za7,
		BGl_z62dozd2alphatiza7ezd2castzd2nu1628z17zzast_alphatiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2167z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72221za7,
		BGl_z62dozd2alphatiza7ezd2getfiel1631zc5zzast_alphatiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2168z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72222za7,
		BGl_z62dozd2alphatiza7ezd2setfiel1633zc5zzast_alphatiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2169z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72223za7,
		BGl_z62dozd2alphatiza7ezd2new1635zc5zzast_alphatiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2170z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72224za7,
		BGl_z62dozd2alphatiza7ezd2vlength1637zc5zzast_alphatiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2171z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72225za7,
		BGl_z62dozd2alphatiza7ezd2vref1639zc5zzast_alphatiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2172z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72226za7,
		BGl_z62dozd2alphatiza7ezd2vsetz121641zd7zzast_alphatiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2173z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72227za7,
		BGl_z62dozd2alphatiza7ezd2valloc1643zc5zzast_alphatiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2174z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72228za7,
		BGl_z62dozd2alphatiza7ezd2instanc1645zc5zzast_alphatiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2175z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72229za7,
		BGl_z62dozd2alphatiza7ezd2cast1647zc5zzast_alphatiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2176z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72230za7,
		BGl_z62dozd2alphatiza7ezd2setq1649zc5zzast_alphatiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2177z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72231za7,
		BGl_z62dozd2alphatiza7ezd2conditi1651zc5zzast_alphatiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2178z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72232za7,
		BGl_z62dozd2alphatiza7ezd2fail1653zc5zzast_alphatiza7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2179z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72233za7,
		BGl_z62dozd2alphatiza7ezd2switch1655zc5zzast_alphatiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2189z00zzast_alphatiza7eza7,
		BgL_bgl_string2189za700za7za7a2234za7, "alphatize", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2180z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72235za7,
		BGl_z62dozd2alphatiza7ezd2makezd2bo1657z17zzast_alphatiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2181z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72236za7,
		BGl_z62dozd2alphatiza7ezd2boxzd2ref1659z17zzast_alphatiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2182z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72237za7,
		BGl_z62dozd2alphatiza7ezd2boxzd2set1661z17zzast_alphatiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2183z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72238za7,
		BGl_z62dozd2alphatiza7ezd2letzd2fun1663z17zzast_alphatiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2190z00zzast_alphatiza7eza7,
		BgL_bgl_string2190za700za7za7a2239za7, "Illegal alphatization (setq)", 28);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2184z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72240za7,
		BGl_z62dozd2alphatiza7ezd2letzd2var1665z17zzast_alphatiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2191z00zzast_alphatiza7eza7,
		BgL_bgl_string2191za700za7za7a2241za7, "Illegal alphatization (closure)",
		31);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2185z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72242za7,
		BGl_z62dozd2alphatiza7ezd2setzd2exzd21667zc5zzast_alphatiza7eza7, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2192z00zzast_alphatiza7eza7,
		BgL_bgl_string2192za700za7za7a2243za7, "Illegal alphatization (var)", 27);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2186z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72244za7,
		BGl_z62dozd2alphatiza7ezd2jumpzd2ex1669z17zzast_alphatiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2193z00zzast_alphatiza7eza7,
		BgL_bgl_string2193za700za7za7a2245za7, "ast_alphatize", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2187z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72246za7,
		BGl_z62dozd2alphatiza7ezd2retbloc1671zc5zzast_alphatiza7eza7, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2194z00zzast_alphatiza7eza7,
		BgL_bgl_string2194za700za7za7a2247za7,
		"value app set! do-alphatize1603 ast_alphatize retblock/alpha alpha location ",
		76);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2188z00zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72248za7,
		BGl_z62dozd2alphatiza7ezd2return1673zc5zzast_alphatiza7eza7, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_alphatiza7ezd2envz75zzast_alphatiza7eza7,
		BgL_bgl_za762alphatiza7a7eza7c2249za7,
		BGl_z62alphatiza7ezc5zzast_alphatiza7eza7, 0L, BUNSPEC, 4);
	     
		DEFINE_STATIC_BGL_GENERIC(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
		BgL_bgl_za762doza7d2alphatiza72250za7,
		BGl_z62dozd2alphatiza7ez17zzast_alphatiza7eza7, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzast_alphatiza7eza7));
		     ADD_ROOT((void *) (&BGl_retblockzf2alphazf2zzast_alphatiza7eza7));
		   
			 ADD_ROOT((void
				*) (&BGl_za2nozd2alphatiza7ezd2closureza2za7zzast_alphatiza7eza7));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzast_alphatiza7eza7(long
		BgL_checksumz00_4276, char *BgL_fromz00_4277)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_alphatiza7eza7))
				{
					BGl_requirezd2initializa7ationz75zzast_alphatiza7eza7 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_alphatiza7eza7();
					BGl_libraryzd2moduleszd2initz00zzast_alphatiza7eza7();
					BGl_cnstzd2initzd2zzast_alphatiza7eza7();
					BGl_importedzd2moduleszd2initz00zzast_alphatiza7eza7();
					BGl_objectzd2initzd2zzast_alphatiza7eza7();
					BGl_genericzd2initzd2zzast_alphatiza7eza7();
					BGl_methodzd2initzd2zzast_alphatiza7eza7();
					return BGl_toplevelzd2initzd2zzast_alphatiza7eza7();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_alphatiza7eza7(void)
	{
		{	/* Ast/alphatize.scm 14 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_alphatize");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "ast_alphatize");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_alphatize");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"ast_alphatize");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "ast_alphatize");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"ast_alphatize");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "ast_alphatize");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"ast_alphatize");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"ast_alphatize");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"ast_alphatize");
			BGl_modulezd2initializa7ationz75zz__bexitz00(0L, "ast_alphatize");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "ast_alphatize");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "ast_alphatize");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_alphatiza7eza7(void)
	{
		{	/* Ast/alphatize.scm 14 */
			{	/* Ast/alphatize.scm 14 */
				obj_t BgL_cportz00_3687;

				{	/* Ast/alphatize.scm 14 */
					obj_t BgL_stringz00_3694;

					BgL_stringz00_3694 = BGl_string2194z00zzast_alphatiza7eza7;
					{	/* Ast/alphatize.scm 14 */
						obj_t BgL_startz00_3695;

						BgL_startz00_3695 = BINT(0L);
						{	/* Ast/alphatize.scm 14 */
							obj_t BgL_endz00_3696;

							BgL_endz00_3696 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3694)));
							{	/* Ast/alphatize.scm 14 */

								BgL_cportz00_3687 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3694, BgL_startz00_3695, BgL_endz00_3696);
				}}}}
				{
					long BgL_iz00_3688;

					BgL_iz00_3688 = 7L;
				BgL_loopz00_3689:
					if ((BgL_iz00_3688 == -1L))
						{	/* Ast/alphatize.scm 14 */
							return BUNSPEC;
						}
					else
						{	/* Ast/alphatize.scm 14 */
							{	/* Ast/alphatize.scm 14 */
								obj_t BgL_arg2196z00_3690;

								{	/* Ast/alphatize.scm 14 */

									{	/* Ast/alphatize.scm 14 */
										obj_t BgL_locationz00_3692;

										BgL_locationz00_3692 = BBOOL(((bool_t) 0));
										{	/* Ast/alphatize.scm 14 */

											BgL_arg2196z00_3690 =
												BGl_readz00zz__readerz00(BgL_cportz00_3687,
												BgL_locationz00_3692);
										}
									}
								}
								{	/* Ast/alphatize.scm 14 */
									int BgL_tmpz00_4311;

									BgL_tmpz00_4311 = (int) (BgL_iz00_3688);
									CNST_TABLE_SET(BgL_tmpz00_4311, BgL_arg2196z00_3690);
							}}
							{	/* Ast/alphatize.scm 14 */
								int BgL_auxz00_3693;

								BgL_auxz00_3693 = (int) ((BgL_iz00_3688 - 1L));
								{
									long BgL_iz00_4316;

									BgL_iz00_4316 = (long) (BgL_auxz00_3693);
									BgL_iz00_3688 = BgL_iz00_4316;
									goto BgL_loopz00_3689;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_alphatiza7eza7(void)
	{
		{	/* Ast/alphatize.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzast_alphatiza7eza7(void)
	{
		{	/* Ast/alphatize.scm 14 */
			BGl_za2nozd2alphatiza7ezd2closureza2za7zzast_alphatiza7eza7 = BFALSE;
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzast_alphatiza7eza7(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1441;

				BgL_headz00_1441 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1442;
					obj_t BgL_tailz00_1443;

					BgL_prevz00_1442 = BgL_headz00_1441;
					BgL_tailz00_1443 = BgL_l1z00_1;
				BgL_loopz00_1444:
					if (PAIRP(BgL_tailz00_1443))
						{
							obj_t BgL_newzd2prevzd2_1446;

							BgL_newzd2prevzd2_1446 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1443), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1442, BgL_newzd2prevzd2_1446);
							{
								obj_t BgL_tailz00_4326;
								obj_t BgL_prevz00_4325;

								BgL_prevz00_4325 = BgL_newzd2prevzd2_1446;
								BgL_tailz00_4326 = CDR(BgL_tailz00_1443);
								BgL_tailz00_1443 = BgL_tailz00_4326;
								BgL_prevz00_1442 = BgL_prevz00_4325;
								goto BgL_loopz00_1444;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1441);
				}
			}
		}

	}



/* alphatize */
	BGL_EXPORTED_DEF BgL_nodez00_bglt BGl_alphatiza7eza7zzast_alphatiza7eza7(obj_t
		BgL_whatza2za2_17, obj_t BgL_byza2za2_18, obj_t BgL_locz00_19,
		BgL_nodez00_bglt BgL_nodez00_20)
	{
		{	/* Ast/alphatize.scm 41 */
			{
				obj_t BgL_ll1555z00_1462;
				obj_t BgL_ll1556z00_1463;

				BgL_ll1555z00_1462 = BgL_whatza2za2_17;
				BgL_ll1556z00_1463 = BgL_byza2za2_18;
			BgL_zc3z04anonymousza31702ze3z87_1464:
				if (NULLP(BgL_ll1555z00_1462))
					{	/* Ast/alphatize.scm 43 */
						((bool_t) 1);
					}
				else
					{	/* Ast/alphatize.scm 43 */
						{	/* Ast/alphatize.scm 44 */
							obj_t BgL_whatz00_1466;
							obj_t BgL_byz00_1467;

							BgL_whatz00_1466 = CAR(((obj_t) BgL_ll1555z00_1462));
							BgL_byz00_1467 = CAR(((obj_t) BgL_ll1556z00_1463));
							((((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_whatz00_1466)))->
									BgL_fastzd2alphazd2) = ((obj_t) BgL_byz00_1467), BUNSPEC);
						}
						{	/* Ast/alphatize.scm 43 */
							obj_t BgL_arg1705z00_1468;
							obj_t BgL_arg1708z00_1469;

							BgL_arg1705z00_1468 = CDR(((obj_t) BgL_ll1555z00_1462));
							BgL_arg1708z00_1469 = CDR(((obj_t) BgL_ll1556z00_1463));
							{
								obj_t BgL_ll1556z00_4342;
								obj_t BgL_ll1555z00_4341;

								BgL_ll1555z00_4341 = BgL_arg1705z00_1468;
								BgL_ll1556z00_4342 = BgL_arg1708z00_1469;
								BgL_ll1556z00_1463 = BgL_ll1556z00_4342;
								BgL_ll1555z00_1462 = BgL_ll1555z00_4341;
								goto BgL_zc3z04anonymousza31702ze3z87_1464;
							}
						}
					}
			}
			{	/* Ast/alphatize.scm 47 */
				BgL_nodez00_bglt BgL_resz00_1471;

				BgL_resz00_1471 =
					BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(BgL_nodez00_20,
					BgL_locz00_19);
				{
					obj_t BgL_l1558z00_1473;

					BgL_l1558z00_1473 = BgL_whatza2za2_17;
				BgL_zc3z04anonymousza31709ze3z87_1474:
					if (PAIRP(BgL_l1558z00_1473))
						{	/* Ast/alphatize.scm 49 */
							{	/* Ast/alphatize.scm 50 */
								obj_t BgL_whatz00_1476;

								BgL_whatz00_1476 = CAR(BgL_l1558z00_1473);
								((((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_whatz00_1476)))->
										BgL_fastzd2alphazd2) = ((obj_t) BUNSPEC), BUNSPEC);
							}
							{
								obj_t BgL_l1558z00_4349;

								BgL_l1558z00_4349 = CDR(BgL_l1558z00_1473);
								BgL_l1558z00_1473 = BgL_l1558z00_4349;
								goto BgL_zc3z04anonymousza31709ze3z87_1474;
							}
						}
					else
						{	/* Ast/alphatize.scm 49 */
							((bool_t) 1);
						}
				}
				return BgL_resz00_1471;
			}
		}

	}



/* &alphatize */
	BgL_nodez00_bglt BGl_z62alphatiza7ezc5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3508, obj_t BgL_whatza2za2_3509, obj_t BgL_byza2za2_3510,
		obj_t BgL_locz00_3511, obj_t BgL_nodez00_3512)
	{
		{	/* Ast/alphatize.scm 41 */
			return
				BGl_alphatiza7eza7zzast_alphatiza7eza7(BgL_whatza2za2_3509,
				BgL_byza2za2_3510, BgL_locz00_3511,
				((BgL_nodez00_bglt) BgL_nodez00_3512));
		}

	}



/* alphatize-sans-closure */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_alphatiza7ezd2sanszd2closureza7zzast_alphatiza7eza7(obj_t
		BgL_whatza2za2_21, obj_t BgL_byza2za2_22, obj_t BgL_locz00_23,
		BgL_nodez00_bglt BgL_nodez00_24, BgL_variablez00_bglt BgL_closurez00_25)
	{
		{	/* Ast/alphatize.scm 62 */
			BGl_za2nozd2alphatiza7ezd2closureza2za7zzast_alphatiza7eza7 =
				((obj_t) BgL_closurez00_25);
			{	/* Ast/alphatize.scm 64 */
				obj_t BgL_exitd1112z00_2490;

				BgL_exitd1112z00_2490 = BGL_EXITD_TOP_AS_OBJ();
				{	/* Ast/alphatize.scm 64 */
					obj_t BgL_arg1828z00_2493;

					{	/* Ast/alphatize.scm 64 */
						obj_t BgL_arg1829z00_2494;

						BgL_arg1829z00_2494 = BGL_EXITD_PROTECT(BgL_exitd1112z00_2490);
						BgL_arg1828z00_2493 =
							MAKE_YOUNG_PAIR(BGl_proc2144z00zzast_alphatiza7eza7,
							BgL_arg1829z00_2494);
					}
					BGL_EXITD_PROTECT_SET(BgL_exitd1112z00_2490, BgL_arg1828z00_2493);
					BUNSPEC;
				}
				{	/* Ast/alphatize.scm 65 */
					BgL_nodez00_bglt BgL_tmp1114z00_2492;

					BgL_tmp1114z00_2492 =
						BGl_alphatiza7eza7zzast_alphatiza7eza7(BgL_whatza2za2_21,
						BgL_byza2za2_22, BgL_locz00_23, BgL_nodez00_24);
					{	/* Ast/alphatize.scm 64 */
						bool_t BgL_test2256z00_4359;

						{	/* Ast/alphatize.scm 64 */
							obj_t BgL_arg1827z00_2496;

							BgL_arg1827z00_2496 = BGL_EXITD_PROTECT(BgL_exitd1112z00_2490);
							BgL_test2256z00_4359 = PAIRP(BgL_arg1827z00_2496);
						}
						if (BgL_test2256z00_4359)
							{	/* Ast/alphatize.scm 64 */
								obj_t BgL_arg1825z00_2497;

								{	/* Ast/alphatize.scm 64 */
									obj_t BgL_arg1826z00_2498;

									BgL_arg1826z00_2498 =
										BGL_EXITD_PROTECT(BgL_exitd1112z00_2490);
									BgL_arg1825z00_2497 = CDR(((obj_t) BgL_arg1826z00_2498));
								}
								BGL_EXITD_PROTECT_SET(BgL_exitd1112z00_2490,
									BgL_arg1825z00_2497);
								BUNSPEC;
							}
						else
							{	/* Ast/alphatize.scm 64 */
								BFALSE;
							}
					}
					BGl_za2nozd2alphatiza7ezd2closureza2za7zzast_alphatiza7eza7 = BFALSE;
					return BgL_tmp1114z00_2492;
				}
			}
		}

	}



/* &alphatize-sans-closure */
	BgL_nodez00_bglt
		BGl_z62alphatiza7ezd2sanszd2closurezc5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3514, obj_t BgL_whatza2za2_3515, obj_t BgL_byza2za2_3516,
		obj_t BgL_locz00_3517, obj_t BgL_nodez00_3518, obj_t BgL_closurez00_3519)
	{
		{	/* Ast/alphatize.scm 62 */
			return
				BGl_alphatiza7ezd2sanszd2closureza7zzast_alphatiza7eza7
				(BgL_whatza2za2_3515, BgL_byza2za2_3516, BgL_locz00_3517,
				((BgL_nodez00_bglt) BgL_nodez00_3518),
				((BgL_variablez00_bglt) BgL_closurez00_3519));
		}

	}



/* &<@anonymous:1715> */
	obj_t BGl_z62zc3z04anonymousza31715ze3ze5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3520)
	{
		{	/* Ast/alphatize.scm 64 */
			return (BGl_za2nozd2alphatiza7ezd2closureza2za7zzast_alphatiza7eza7 =
				BFALSE, BUNSPEC);
		}

	}



/* get-location */
	obj_t BGl_getzd2locationzd2zzast_alphatiza7eza7(BgL_nodez00_bglt
		BgL_nodez00_26, obj_t BgL_locz00_27)
	{
		{	/* Ast/alphatize.scm 80 */
			{	/* Ast/alphatize.scm 82 */
				bool_t BgL_test2257z00_4369;

				{	/* Ast/alphatize.scm 82 */
					obj_t BgL_arg1736z00_1494;

					BgL_arg1736z00_1494 =
						(((BgL_nodez00_bglt) COBJECT(BgL_nodez00_26))->BgL_locz00);
					if (STRUCTP(BgL_arg1736z00_1494))
						{	/* Ast/alphatize.scm 82 */
							BgL_test2257z00_4369 =
								(STRUCT_KEY(BgL_arg1736z00_1494) == CNST_TABLE_REF(0));
						}
					else
						{	/* Ast/alphatize.scm 82 */
							BgL_test2257z00_4369 = ((bool_t) 0);
						}
				}
				if (BgL_test2257z00_4369)
					{	/* Ast/alphatize.scm 84 */
						bool_t BgL_test2259z00_4376;

						if (STRUCTP(BgL_locz00_27))
							{	/* Ast/alphatize.scm 84 */
								BgL_test2259z00_4376 =
									(STRUCT_KEY(BgL_locz00_27) == CNST_TABLE_REF(0));
							}
						else
							{	/* Ast/alphatize.scm 84 */
								BgL_test2259z00_4376 = ((bool_t) 0);
							}
						if (BgL_test2259z00_4376)
							{	/* Ast/alphatize.scm 86 */
								bool_t BgL_test2261z00_4382;

								{	/* Ast/alphatize.scm 86 */
									obj_t BgL_arg1733z00_1491;
									obj_t BgL_arg1734z00_1492;

									BgL_arg1733z00_1491 =
										STRUCT_REF(
										(((BgL_nodez00_bglt) COBJECT(BgL_nodez00_26))->BgL_locz00),
										(int) (0L));
									BgL_arg1734z00_1492 = STRUCT_REF(BgL_locz00_27, (int) (0L));
									{	/* Ast/alphatize.scm 86 */
										long BgL_l1z00_2514;

										BgL_l1z00_2514 =
											STRING_LENGTH(((obj_t) BgL_arg1733z00_1491));
										if (
											(BgL_l1z00_2514 ==
												STRING_LENGTH(((obj_t) BgL_arg1734z00_1492))))
											{	/* Ast/alphatize.scm 86 */
												int BgL_arg1282z00_2517;

												{	/* Ast/alphatize.scm 86 */
													char *BgL_auxz00_4397;
													char *BgL_tmpz00_4394;

													BgL_auxz00_4397 =
														BSTRING_TO_STRING(((obj_t) BgL_arg1734z00_1492));
													BgL_tmpz00_4394 =
														BSTRING_TO_STRING(((obj_t) BgL_arg1733z00_1491));
													BgL_arg1282z00_2517 =
														memcmp(BgL_tmpz00_4394, BgL_auxz00_4397,
														BgL_l1z00_2514);
												}
												BgL_test2261z00_4382 =
													((long) (BgL_arg1282z00_2517) == 0L);
											}
										else
											{	/* Ast/alphatize.scm 86 */
												BgL_test2261z00_4382 = ((bool_t) 0);
											}
									}
								}
								if (BgL_test2261z00_4382)
									{	/* Ast/alphatize.scm 86 */
										return
											(((BgL_nodez00_bglt) COBJECT(BgL_nodez00_26))->
											BgL_locz00);
									}
								else
									{	/* Ast/alphatize.scm 86 */
										return BgL_locz00_27;
									}
							}
						else
							{	/* Ast/alphatize.scm 84 */
								return
									(((BgL_nodez00_bglt) COBJECT(BgL_nodez00_26))->BgL_locz00);
							}
					}
				else
					{	/* Ast/alphatize.scm 82 */
						return BgL_locz00_27;
					}
			}
		}

	}



/* do-alphatize* */
	obj_t BGl_dozd2alphatiza7eza2zd7zzast_alphatiza7eza7(obj_t BgL_nodesz00_28,
		obj_t BgL_locz00_29)
	{
		{	/* Ast/alphatize.scm 94 */
			if (NULLP(BgL_nodesz00_28))
				{	/* Ast/alphatize.scm 95 */
					return BNIL;
				}
			else
				{	/* Ast/alphatize.scm 95 */
					obj_t BgL_head1562z00_1497;

					BgL_head1562z00_1497 = MAKE_YOUNG_PAIR(BNIL, BNIL);
					{
						obj_t BgL_l1560z00_1499;
						obj_t BgL_tail1563z00_1500;

						BgL_l1560z00_1499 = BgL_nodesz00_28;
						BgL_tail1563z00_1500 = BgL_head1562z00_1497;
					BgL_zc3z04anonymousza31738ze3z87_1501:
						if (NULLP(BgL_l1560z00_1499))
							{	/* Ast/alphatize.scm 95 */
								return CDR(BgL_head1562z00_1497);
							}
						else
							{	/* Ast/alphatize.scm 95 */
								obj_t BgL_newtail1564z00_1503;

								{	/* Ast/alphatize.scm 95 */
									BgL_nodez00_bglt BgL_arg1746z00_1505;

									{	/* Ast/alphatize.scm 95 */
										obj_t BgL_nodez00_1506;

										BgL_nodez00_1506 = CAR(((obj_t) BgL_l1560z00_1499));
										BgL_arg1746z00_1505 =
											BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(
											((BgL_nodez00_bglt) BgL_nodez00_1506), BgL_locz00_29);
									}
									BgL_newtail1564z00_1503 =
										MAKE_YOUNG_PAIR(((obj_t) BgL_arg1746z00_1505), BNIL);
								}
								SET_CDR(BgL_tail1563z00_1500, BgL_newtail1564z00_1503);
								{	/* Ast/alphatize.scm 95 */
									obj_t BgL_arg1740z00_1504;

									BgL_arg1740z00_1504 = CDR(((obj_t) BgL_l1560z00_1499));
									{
										obj_t BgL_tail1563z00_4421;
										obj_t BgL_l1560z00_4420;

										BgL_l1560z00_4420 = BgL_arg1740z00_1504;
										BgL_tail1563z00_4421 = BgL_newtail1564z00_1503;
										BgL_tail1563z00_1500 = BgL_tail1563z00_4421;
										BgL_l1560z00_1499 = BgL_l1560z00_4420;
										goto BgL_zc3z04anonymousza31738ze3z87_1501;
									}
								}
							}
					}
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_alphatiza7eza7(void)
	{
		{	/* Ast/alphatize.scm 14 */
			{	/* Ast/alphatize.scm 26 */
				obj_t BgL_arg1751z00_1512;
				obj_t BgL_arg1752z00_1513;

				{	/* Ast/alphatize.scm 26 */
					obj_t BgL_v1601z00_1538;

					BgL_v1601z00_1538 = create_vector(1L);
					{	/* Ast/alphatize.scm 26 */
						obj_t BgL_arg1771z00_1539;

						BgL_arg1771z00_1539 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc2146z00zzast_alphatiza7eza7,
							BGl_proc2145z00zzast_alphatiza7eza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_retblockz00zzast_nodez00);
						VECTOR_SET(BgL_v1601z00_1538, 0L, BgL_arg1771z00_1539);
					}
					BgL_arg1751z00_1512 = BgL_v1601z00_1538;
				}
				{	/* Ast/alphatize.scm 26 */
					obj_t BgL_v1602z00_1549;

					BgL_v1602z00_1549 = create_vector(0L);
					BgL_arg1752z00_1513 = BgL_v1602z00_1549;
				}
				return (BGl_retblockzf2alphazf2zzast_alphatiza7eza7 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(2),
						CNST_TABLE_REF(3), BGl_retblockz00zzast_nodez00, 248L,
						BGl_proc2150z00zzast_alphatiza7eza7,
						BGl_proc2149z00zzast_alphatiza7eza7, BFALSE,
						BGl_proc2148z00zzast_alphatiza7eza7,
						BGl_proc2147z00zzast_alphatiza7eza7, BgL_arg1751z00_1512,
						BgL_arg1752z00_1513), BUNSPEC);
			}
		}

	}



/* &lambda1763 */
	BgL_retblockz00_bglt BGl_z62lambda1763z62zzast_alphatiza7eza7(obj_t
		BgL_envz00_3527, obj_t BgL_o1111z00_3528)
	{
		{	/* Ast/alphatize.scm 26 */
			{	/* Ast/alphatize.scm 26 */
				long BgL_arg1765z00_3699;

				{	/* Ast/alphatize.scm 26 */
					obj_t BgL_arg1767z00_3700;

					{	/* Ast/alphatize.scm 26 */
						obj_t BgL_arg1770z00_3701;

						{	/* Ast/alphatize.scm 26 */
							obj_t BgL_arg1815z00_3702;
							long BgL_arg1816z00_3703;

							BgL_arg1815z00_3702 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Ast/alphatize.scm 26 */
								long BgL_arg1817z00_3704;

								BgL_arg1817z00_3704 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_retblockz00_bglt) BgL_o1111z00_3528)));
								BgL_arg1816z00_3703 = (BgL_arg1817z00_3704 - OBJECT_TYPE);
							}
							BgL_arg1770z00_3701 =
								VECTOR_REF(BgL_arg1815z00_3702, BgL_arg1816z00_3703);
						}
						BgL_arg1767z00_3700 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1770z00_3701);
					}
					{	/* Ast/alphatize.scm 26 */
						obj_t BgL_tmpz00_4437;

						BgL_tmpz00_4437 = ((obj_t) BgL_arg1767z00_3700);
						BgL_arg1765z00_3699 = BGL_CLASS_NUM(BgL_tmpz00_4437);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_retblockz00_bglt) BgL_o1111z00_3528)), BgL_arg1765z00_3699);
			}
			{	/* Ast/alphatize.scm 26 */
				BgL_objectz00_bglt BgL_tmpz00_4443;

				BgL_tmpz00_4443 =
					((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_o1111z00_3528));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4443, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_o1111z00_3528));
			return
				((BgL_retblockz00_bglt) ((BgL_retblockz00_bglt) BgL_o1111z00_3528));
		}

	}



/* &<@anonymous:1762> */
	obj_t BGl_z62zc3z04anonymousza31762ze3ze5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3529, obj_t BgL_new1110z00_3530)
	{
		{	/* Ast/alphatize.scm 26 */
			{
				BgL_retblockz00_bglt BgL_auxz00_4451;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_retblockz00_bglt) BgL_new1110z00_3530))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_4455;

					{	/* Ast/alphatize.scm 26 */
						obj_t BgL_classz00_3706;

						BgL_classz00_3706 = BGl_typez00zztype_typez00;
						{	/* Ast/alphatize.scm 26 */
							obj_t BgL__ortest_1117z00_3707;

							BgL__ortest_1117z00_3707 = BGL_CLASS_NIL(BgL_classz00_3706);
							if (CBOOL(BgL__ortest_1117z00_3707))
								{	/* Ast/alphatize.scm 26 */
									BgL_auxz00_4455 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_3707);
								}
							else
								{	/* Ast/alphatize.scm 26 */
									BgL_auxz00_4455 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3706));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_retblockz00_bglt) BgL_new1110z00_3530))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_4455), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_4465;

					{	/* Ast/alphatize.scm 26 */
						obj_t BgL_classz00_3708;

						BgL_classz00_3708 = BGl_nodez00zzast_nodez00;
						{	/* Ast/alphatize.scm 26 */
							obj_t BgL__ortest_1117z00_3709;

							BgL__ortest_1117z00_3709 = BGL_CLASS_NIL(BgL_classz00_3708);
							if (CBOOL(BgL__ortest_1117z00_3709))
								{	/* Ast/alphatize.scm 26 */
									BgL_auxz00_4465 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_3709);
								}
							else
								{	/* Ast/alphatize.scm 26 */
									BgL_auxz00_4465 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3708));
								}
						}
					}
					((((BgL_retblockz00_bglt) COBJECT(
									((BgL_retblockz00_bglt)
										((BgL_retblockz00_bglt) BgL_new1110z00_3530))))->
							BgL_bodyz00) = ((BgL_nodez00_bglt) BgL_auxz00_4465), BUNSPEC);
				}
				{
					BgL_retblockz00_bglt BgL_auxz00_4482;
					BgL_retblockzf2alphazf2_bglt BgL_auxz00_4475;

					{	/* Ast/alphatize.scm 26 */
						obj_t BgL_classz00_3710;

						BgL_classz00_3710 = BGl_retblockz00zzast_nodez00;
						{	/* Ast/alphatize.scm 26 */
							obj_t BgL__ortest_1117z00_3711;

							BgL__ortest_1117z00_3711 = BGL_CLASS_NIL(BgL_classz00_3710);
							if (CBOOL(BgL__ortest_1117z00_3711))
								{	/* Ast/alphatize.scm 26 */
									BgL_auxz00_4482 =
										((BgL_retblockz00_bglt) BgL__ortest_1117z00_3711);
								}
							else
								{	/* Ast/alphatize.scm 26 */
									BgL_auxz00_4482 =
										((BgL_retblockz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3710));
								}
						}
					}
					{
						obj_t BgL_auxz00_4476;

						{	/* Ast/alphatize.scm 26 */
							BgL_objectz00_bglt BgL_tmpz00_4477;

							BgL_tmpz00_4477 =
								((BgL_objectz00_bglt)
								((BgL_retblockz00_bglt) BgL_new1110z00_3530));
							BgL_auxz00_4476 = BGL_OBJECT_WIDENING(BgL_tmpz00_4477);
						}
						BgL_auxz00_4475 = ((BgL_retblockzf2alphazf2_bglt) BgL_auxz00_4476);
					}
					((((BgL_retblockzf2alphazf2_bglt) COBJECT(BgL_auxz00_4475))->
							BgL_alphaz00) =
						((BgL_retblockz00_bglt) BgL_auxz00_4482), BUNSPEC);
				}
				BgL_auxz00_4451 = ((BgL_retblockz00_bglt) BgL_new1110z00_3530);
				return ((obj_t) BgL_auxz00_4451);
			}
		}

	}



/* &lambda1756 */
	BgL_retblockz00_bglt BGl_z62lambda1756z62zzast_alphatiza7eza7(obj_t
		BgL_envz00_3531, obj_t BgL_o1107z00_3532)
	{
		{	/* Ast/alphatize.scm 26 */
			{	/* Ast/alphatize.scm 26 */
				BgL_retblockzf2alphazf2_bglt BgL_wide1109z00_3713;

				BgL_wide1109z00_3713 =
					((BgL_retblockzf2alphazf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_retblockzf2alphazf2_bgl))));
				{	/* Ast/alphatize.scm 26 */
					obj_t BgL_auxz00_4497;
					BgL_objectz00_bglt BgL_tmpz00_4493;

					BgL_auxz00_4497 = ((obj_t) BgL_wide1109z00_3713);
					BgL_tmpz00_4493 =
						((BgL_objectz00_bglt)
						((BgL_retblockz00_bglt)
							((BgL_retblockz00_bglt) BgL_o1107z00_3532)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4493, BgL_auxz00_4497);
				}
				((BgL_objectz00_bglt)
					((BgL_retblockz00_bglt) ((BgL_retblockz00_bglt) BgL_o1107z00_3532)));
				{	/* Ast/alphatize.scm 26 */
					long BgL_arg1761z00_3714;

					BgL_arg1761z00_3714 =
						BGL_CLASS_NUM(BGl_retblockzf2alphazf2zzast_alphatiza7eza7);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_retblockz00_bglt)
								((BgL_retblockz00_bglt) BgL_o1107z00_3532))),
						BgL_arg1761z00_3714);
				}
				return
					((BgL_retblockz00_bglt)
					((BgL_retblockz00_bglt) ((BgL_retblockz00_bglt) BgL_o1107z00_3532)));
			}
		}

	}



/* &lambda1753 */
	BgL_retblockz00_bglt BGl_z62lambda1753z62zzast_alphatiza7eza7(obj_t
		BgL_envz00_3533, obj_t BgL_loc1103z00_3534, obj_t BgL_type1104z00_3535,
		obj_t BgL_body1105z00_3536, obj_t BgL_alpha1106z00_3537)
	{
		{	/* Ast/alphatize.scm 26 */
			{	/* Ast/alphatize.scm 26 */
				BgL_retblockz00_bglt BgL_new1444z00_3718;

				{	/* Ast/alphatize.scm 26 */
					BgL_retblockz00_bglt BgL_tmp1442z00_3719;
					BgL_retblockzf2alphazf2_bglt BgL_wide1443z00_3720;

					{
						BgL_retblockz00_bglt BgL_auxz00_4511;

						{	/* Ast/alphatize.scm 26 */
							BgL_retblockz00_bglt BgL_new1441z00_3721;

							BgL_new1441z00_3721 =
								((BgL_retblockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_retblockz00_bgl))));
							{	/* Ast/alphatize.scm 26 */
								long BgL_arg1755z00_3722;

								BgL_arg1755z00_3722 =
									BGL_CLASS_NUM(BGl_retblockz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1441z00_3721),
									BgL_arg1755z00_3722);
							}
							{	/* Ast/alphatize.scm 26 */
								BgL_objectz00_bglt BgL_tmpz00_4516;

								BgL_tmpz00_4516 = ((BgL_objectz00_bglt) BgL_new1441z00_3721);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4516, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1441z00_3721);
							BgL_auxz00_4511 = BgL_new1441z00_3721;
						}
						BgL_tmp1442z00_3719 = ((BgL_retblockz00_bglt) BgL_auxz00_4511);
					}
					BgL_wide1443z00_3720 =
						((BgL_retblockzf2alphazf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_retblockzf2alphazf2_bgl))));
					{	/* Ast/alphatize.scm 26 */
						obj_t BgL_auxz00_4524;
						BgL_objectz00_bglt BgL_tmpz00_4522;

						BgL_auxz00_4524 = ((obj_t) BgL_wide1443z00_3720);
						BgL_tmpz00_4522 = ((BgL_objectz00_bglt) BgL_tmp1442z00_3719);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4522, BgL_auxz00_4524);
					}
					((BgL_objectz00_bglt) BgL_tmp1442z00_3719);
					{	/* Ast/alphatize.scm 26 */
						long BgL_arg1754z00_3723;

						BgL_arg1754z00_3723 =
							BGL_CLASS_NUM(BGl_retblockzf2alphazf2zzast_alphatiza7eza7);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1442z00_3719), BgL_arg1754z00_3723);
					}
					BgL_new1444z00_3718 = ((BgL_retblockz00_bglt) BgL_tmp1442z00_3719);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1444z00_3718)))->BgL_locz00) =
					((obj_t) BgL_loc1103z00_3534), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1444z00_3718)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1104z00_3535)),
					BUNSPEC);
				((((BgL_retblockz00_bglt) COBJECT(((BgL_retblockz00_bglt)
									BgL_new1444z00_3718)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_body1105z00_3536)),
					BUNSPEC);
				{
					BgL_retblockzf2alphazf2_bglt BgL_auxz00_4540;

					{
						obj_t BgL_auxz00_4541;

						{	/* Ast/alphatize.scm 26 */
							BgL_objectz00_bglt BgL_tmpz00_4542;

							BgL_tmpz00_4542 = ((BgL_objectz00_bglt) BgL_new1444z00_3718);
							BgL_auxz00_4541 = BGL_OBJECT_WIDENING(BgL_tmpz00_4542);
						}
						BgL_auxz00_4540 = ((BgL_retblockzf2alphazf2_bglt) BgL_auxz00_4541);
					}
					((((BgL_retblockzf2alphazf2_bglt) COBJECT(BgL_auxz00_4540))->
							BgL_alphaz00) =
						((BgL_retblockz00_bglt) ((BgL_retblockz00_bglt)
								BgL_alpha1106z00_3537)), BUNSPEC);
				}
				return BgL_new1444z00_3718;
			}
		}

	}



/* &lambda1777 */
	obj_t BGl_z62lambda1777z62zzast_alphatiza7eza7(obj_t BgL_envz00_3538,
		obj_t BgL_oz00_3539, obj_t BgL_vz00_3540)
	{
		{	/* Ast/alphatize.scm 26 */
			{
				BgL_retblockzf2alphazf2_bglt BgL_auxz00_4548;

				{
					obj_t BgL_auxz00_4549;

					{	/* Ast/alphatize.scm 26 */
						BgL_objectz00_bglt BgL_tmpz00_4550;

						BgL_tmpz00_4550 =
							((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_oz00_3539));
						BgL_auxz00_4549 = BGL_OBJECT_WIDENING(BgL_tmpz00_4550);
					}
					BgL_auxz00_4548 = ((BgL_retblockzf2alphazf2_bglt) BgL_auxz00_4549);
				}
				return
					((((BgL_retblockzf2alphazf2_bglt) COBJECT(BgL_auxz00_4548))->
						BgL_alphaz00) =
					((BgL_retblockz00_bglt) ((BgL_retblockz00_bglt) BgL_vz00_3540)),
					BUNSPEC);
			}
		}

	}



/* &lambda1776 */
	BgL_retblockz00_bglt BGl_z62lambda1776z62zzast_alphatiza7eza7(obj_t
		BgL_envz00_3541, obj_t BgL_oz00_3542)
	{
		{	/* Ast/alphatize.scm 26 */
			{
				BgL_retblockzf2alphazf2_bglt BgL_auxz00_4557;

				{
					obj_t BgL_auxz00_4558;

					{	/* Ast/alphatize.scm 26 */
						BgL_objectz00_bglt BgL_tmpz00_4559;

						BgL_tmpz00_4559 =
							((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_oz00_3542));
						BgL_auxz00_4558 = BGL_OBJECT_WIDENING(BgL_tmpz00_4559);
					}
					BgL_auxz00_4557 = ((BgL_retblockzf2alphazf2_bglt) BgL_auxz00_4558);
				}
				return
					(((BgL_retblockzf2alphazf2_bglt) COBJECT(BgL_auxz00_4557))->
					BgL_alphaz00);
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_alphatiza7eza7(void)
	{
		{	/* Ast/alphatize.scm 14 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_proc2151z00zzast_alphatiza7eza7, BGl_nodez00zzast_nodez00,
				BGl_string2152z00zzast_alphatiza7eza7);
		}

	}



/* &do-alphatize1603 */
	obj_t BGl_z62dozd2alphatiza7e1603z17zzast_alphatiza7eza7(obj_t
		BgL_envz00_3544, obj_t BgL_nodez00_3545, obj_t BgL_locz00_3546)
	{
		{	/* Ast/alphatize.scm 100 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(4),
				BGl_string2153z00zzast_alphatiza7eza7,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_3545)));
		}

	}



/* do-alphatize */
	BgL_nodez00_bglt BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(BgL_nodez00_bglt
		BgL_nodez00_30, obj_t BgL_locz00_31)
	{
		{	/* Ast/alphatize.scm 100 */
			{	/* Ast/alphatize.scm 100 */
				obj_t BgL_method1604z00_1559;

				{	/* Ast/alphatize.scm 100 */
					obj_t BgL_res2124z00_2595;

					{	/* Ast/alphatize.scm 100 */
						long BgL_objzd2classzd2numz00_2566;

						BgL_objzd2classzd2numz00_2566 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_30));
						{	/* Ast/alphatize.scm 100 */
							obj_t BgL_arg1811z00_2567;

							BgL_arg1811z00_2567 =
								PROCEDURE_REF(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
								(int) (1L));
							{	/* Ast/alphatize.scm 100 */
								int BgL_offsetz00_2570;

								BgL_offsetz00_2570 = (int) (BgL_objzd2classzd2numz00_2566);
								{	/* Ast/alphatize.scm 100 */
									long BgL_offsetz00_2571;

									BgL_offsetz00_2571 =
										((long) (BgL_offsetz00_2570) - OBJECT_TYPE);
									{	/* Ast/alphatize.scm 100 */
										long BgL_modz00_2572;

										BgL_modz00_2572 =
											(BgL_offsetz00_2571 >> (int) ((long) ((int) (4L))));
										{	/* Ast/alphatize.scm 100 */
											long BgL_restz00_2574;

											BgL_restz00_2574 =
												(BgL_offsetz00_2571 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/alphatize.scm 100 */

												{	/* Ast/alphatize.scm 100 */
													obj_t BgL_bucketz00_2576;

													BgL_bucketz00_2576 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2567), BgL_modz00_2572);
													BgL_res2124z00_2595 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2576), BgL_restz00_2574);
					}}}}}}}}
					BgL_method1604z00_1559 = BgL_res2124z00_2595;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL2(BgL_method1604z00_1559,
						((obj_t) BgL_nodez00_30), BgL_locz00_31));
			}
		}

	}



/* &do-alphatize */
	BgL_nodez00_bglt BGl_z62dozd2alphatiza7ez17zzast_alphatiza7eza7(obj_t
		BgL_envz00_3547, obj_t BgL_nodez00_3548, obj_t BgL_locz00_3549)
	{
		{	/* Ast/alphatize.scm 100 */
			return
				BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(
				((BgL_nodez00_bglt) BgL_nodez00_3548), BgL_locz00_3549);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_alphatiza7eza7(void)
	{
		{	/* Ast/alphatize.scm 14 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_literalz00zzast_nodez00, BGl_proc2154z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_patchz00zzast_nodez00, BGl_proc2156z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_varz00zzast_nodez00, BGl_proc2157z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_closurez00zzast_nodez00, BGl_proc2158z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_kwotez00zzast_nodez00, BGl_proc2159z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_sequencez00zzast_nodez00, BGl_proc2160z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_syncz00zzast_nodez00, BGl_proc2161z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_appz00zzast_nodez00, BGl_proc2162z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc2163z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_funcallz00zzast_nodez00, BGl_proc2164z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_pragmaz00zzast_nodez00, BGl_proc2165z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_castzd2nullzd2zzast_nodez00, BGl_proc2166z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_getfieldz00zzast_nodez00, BGl_proc2167z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_setfieldz00zzast_nodez00, BGl_proc2168z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_newz00zzast_nodez00, BGl_proc2169z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_vlengthz00zzast_nodez00, BGl_proc2170z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_vrefz00zzast_nodez00, BGl_proc2171z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_vsetz12z12zzast_nodez00, BGl_proc2172z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_vallocz00zzast_nodez00, BGl_proc2173z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_instanceofz00zzast_nodez00, BGl_proc2174z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_castz00zzast_nodez00, BGl_proc2175z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_setqz00zzast_nodez00, BGl_proc2176z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_conditionalz00zzast_nodez00, BGl_proc2177z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_failz00zzast_nodez00, BGl_proc2178z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_switchz00zzast_nodez00, BGl_proc2179z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc2180z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc2181z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc2182z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc2183z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc2184z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc2185z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc2186z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_retblockz00zzast_nodez00, BGl_proc2187z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dozd2alphatiza7ezd2envza7zzast_alphatiza7eza7,
				BGl_returnz00zzast_nodez00, BGl_proc2188z00zzast_alphatiza7eza7,
				BGl_string2155z00zzast_alphatiza7eza7);
		}

	}



/* &do-alphatize-return1673 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2return1673zc5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3584, obj_t BgL_nodez00_3585, obj_t BgL_locz00_3586)
	{
		{	/* Ast/alphatize.scm 541 */
			{	/* Ast/alphatize.scm 543 */
				bool_t BgL_test2268z00_4638;

				{	/* Ast/alphatize.scm 543 */
					BgL_retblockz00_bglt BgL_arg2057z00_3729;

					BgL_arg2057z00_3729 =
						(((BgL_returnz00_bglt) COBJECT(
								((BgL_returnz00_bglt) BgL_nodez00_3585)))->BgL_blockz00);
					{	/* Ast/alphatize.scm 543 */
						obj_t BgL_classz00_3730;

						BgL_classz00_3730 = BGl_retblockzf2alphazf2zzast_alphatiza7eza7;
						{	/* Ast/alphatize.scm 543 */
							obj_t BgL_oclassz00_3731;

							{	/* Ast/alphatize.scm 543 */
								obj_t BgL_arg1815z00_3732;
								long BgL_arg1816z00_3733;

								BgL_arg1815z00_3732 = (BGl_za2classesza2z00zz__objectz00);
								{	/* Ast/alphatize.scm 543 */
									long BgL_arg1817z00_3734;

									BgL_arg1817z00_3734 =
										BGL_OBJECT_CLASS_NUM(
										((BgL_objectz00_bglt) BgL_arg2057z00_3729));
									BgL_arg1816z00_3733 = (BgL_arg1817z00_3734 - OBJECT_TYPE);
								}
								BgL_oclassz00_3731 =
									VECTOR_REF(BgL_arg1815z00_3732, BgL_arg1816z00_3733);
							}
							BgL_test2268z00_4638 = (BgL_oclassz00_3731 == BgL_classz00_3730);
				}}}
				if (BgL_test2268z00_4638)
					{	/* Ast/alphatize.scm 544 */
						BgL_retblockz00_bglt BgL_i1432z00_3735;

						BgL_i1432z00_3735 =
							((BgL_retblockz00_bglt)
							(((BgL_returnz00_bglt) COBJECT(
										((BgL_returnz00_bglt) BgL_nodez00_3585)))->BgL_blockz00));
						{	/* Ast/alphatize.scm 545 */
							BgL_returnz00_bglt BgL_new1433z00_3736;

							{	/* Ast/alphatize.scm 545 */
								BgL_returnz00_bglt BgL_new1436z00_3737;

								BgL_new1436z00_3737 =
									((BgL_returnz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_returnz00_bgl))));
								{	/* Ast/alphatize.scm 545 */
									long BgL_arg2051z00_3738;

									BgL_arg2051z00_3738 =
										BGL_CLASS_NUM(BGl_returnz00zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1436z00_3737),
										BgL_arg2051z00_3738);
								}
								{	/* Ast/alphatize.scm 545 */
									BgL_objectz00_bglt BgL_tmpz00_4654;

									BgL_tmpz00_4654 = ((BgL_objectz00_bglt) BgL_new1436z00_3737);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4654, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1436z00_3737);
								BgL_new1433z00_3736 = BgL_new1436z00_3737;
							}
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt) BgL_new1433z00_3736)))->BgL_locz00) =
								((obj_t) (((BgL_nodez00_bglt)
											COBJECT(((BgL_nodez00_bglt) ((BgL_returnz00_bglt)
														BgL_nodez00_3585))))->BgL_locz00)), BUNSPEC);
							((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
												BgL_new1433z00_3736)))->BgL_typez00) =
								((BgL_typez00_bglt) (((BgL_nodez00_bglt)
											COBJECT(((BgL_nodez00_bglt) ((BgL_returnz00_bglt)
														BgL_nodez00_3585))))->BgL_typez00)), BUNSPEC);
							{
								BgL_retblockz00_bglt BgL_auxz00_4668;

								{
									BgL_retblockzf2alphazf2_bglt BgL_auxz00_4669;

									{
										obj_t BgL_auxz00_4670;

										{	/* Ast/alphatize.scm 546 */
											BgL_objectz00_bglt BgL_tmpz00_4671;

											BgL_tmpz00_4671 =
												((BgL_objectz00_bglt) BgL_i1432z00_3735);
											BgL_auxz00_4670 = BGL_OBJECT_WIDENING(BgL_tmpz00_4671);
										}
										BgL_auxz00_4669 =
											((BgL_retblockzf2alphazf2_bglt) BgL_auxz00_4670);
									}
									BgL_auxz00_4668 =
										(((BgL_retblockzf2alphazf2_bglt) COBJECT(BgL_auxz00_4669))->
										BgL_alphaz00);
								}
								((((BgL_returnz00_bglt) COBJECT(BgL_new1433z00_3736))->
										BgL_blockz00) =
									((BgL_retblockz00_bglt) BgL_auxz00_4668), BUNSPEC);
							}
							{
								BgL_nodez00_bglt BgL_auxz00_4677;

								{	/* Ast/alphatize.scm 547 */
									BgL_nodez00_bglt BgL_arg2049z00_3739;
									obj_t BgL_arg2050z00_3740;

									BgL_arg2049z00_3739 =
										(((BgL_returnz00_bglt) COBJECT(
												((BgL_returnz00_bglt) BgL_nodez00_3585)))->
										BgL_valuez00);
									BgL_arg2050z00_3740 =
										(((BgL_nodez00_bglt)
											COBJECT(((BgL_nodez00_bglt) ((BgL_returnz00_bglt)
														BgL_nodez00_3585))))->BgL_locz00);
									BgL_auxz00_4677 =
										BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7
										(BgL_arg2049z00_3739, BgL_arg2050z00_3740);
								}
								((((BgL_returnz00_bglt) COBJECT(BgL_new1433z00_3736))->
										BgL_valuez00) =
									((BgL_nodez00_bglt) BgL_auxz00_4677), BUNSPEC);
							}
							return ((BgL_nodez00_bglt) BgL_new1433z00_3736);
						}
					}
				else
					{	/* Ast/alphatize.scm 548 */
						BgL_returnz00_bglt BgL_new1437z00_3741;

						{	/* Ast/alphatize.scm 548 */
							BgL_returnz00_bglt BgL_new1440z00_3742;

							BgL_new1440z00_3742 =
								((BgL_returnz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_returnz00_bgl))));
							{	/* Ast/alphatize.scm 548 */
								long BgL_arg2056z00_3743;

								BgL_arg2056z00_3743 = BGL_CLASS_NUM(BGl_returnz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1440z00_3742),
									BgL_arg2056z00_3743);
							}
							{	/* Ast/alphatize.scm 548 */
								BgL_objectz00_bglt BgL_tmpz00_4690;

								BgL_tmpz00_4690 = ((BgL_objectz00_bglt) BgL_new1440z00_3742);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4690, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1440z00_3742);
							BgL_new1437z00_3741 = BgL_new1440z00_3742;
						}
						((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt) BgL_new1437z00_3741)))->BgL_locz00) =
							((obj_t) (((BgL_nodez00_bglt)
										COBJECT(((BgL_nodez00_bglt) ((BgL_returnz00_bglt)
													BgL_nodez00_3585))))->BgL_locz00)), BUNSPEC);
						((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
											BgL_new1437z00_3741)))->BgL_typez00) =
							((BgL_typez00_bglt) (((BgL_nodez00_bglt)
										COBJECT(((BgL_nodez00_bglt) ((BgL_returnz00_bglt)
													BgL_nodez00_3585))))->BgL_typez00)), BUNSPEC);
						((((BgL_returnz00_bglt) COBJECT(BgL_new1437z00_3741))->
								BgL_blockz00) =
							((BgL_retblockz00_bglt) (((BgL_returnz00_bglt)
										COBJECT(((BgL_returnz00_bglt) BgL_nodez00_3585)))->
									BgL_blockz00)), BUNSPEC);
						{
							BgL_nodez00_bglt BgL_auxz00_4707;

							{	/* Ast/alphatize.scm 550 */
								BgL_nodez00_bglt BgL_arg2052z00_3744;
								obj_t BgL_arg2055z00_3745;

								BgL_arg2052z00_3744 =
									(((BgL_returnz00_bglt) COBJECT(
											((BgL_returnz00_bglt) BgL_nodez00_3585)))->BgL_valuez00);
								BgL_arg2055z00_3745 =
									(((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt)
												((BgL_returnz00_bglt) BgL_nodez00_3585))))->BgL_locz00);
								BgL_auxz00_4707 =
									BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7
									(BgL_arg2052z00_3744, BgL_arg2055z00_3745);
							}
							((((BgL_returnz00_bglt) COBJECT(BgL_new1437z00_3741))->
									BgL_valuez00) =
								((BgL_nodez00_bglt) BgL_auxz00_4707), BUNSPEC);
						}
						return ((BgL_nodez00_bglt) BgL_new1437z00_3741);
					}
			}
		}

	}



/* &do-alphatize-retbloc1671 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2retbloc1671zc5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3587, obj_t BgL_nodez00_3588, obj_t BgL_locz00_3589)
	{
		{	/* Ast/alphatize.scm 527 */
			{	/* Ast/alphatize.scm 529 */
				BgL_retblockz00_bglt BgL_resz00_3747;

				{	/* Ast/alphatize.scm 529 */
					BgL_retblockz00_bglt BgL_new1420z00_3748;

					{	/* Ast/alphatize.scm 529 */
						BgL_retblockz00_bglt BgL_new1424z00_3749;

						BgL_new1424z00_3749 =
							((BgL_retblockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_retblockz00_bgl))));
						{	/* Ast/alphatize.scm 529 */
							long BgL_arg2046z00_3750;

							BgL_arg2046z00_3750 = BGL_CLASS_NUM(BGl_retblockz00zzast_nodez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1424z00_3749),
								BgL_arg2046z00_3750);
						}
						{	/* Ast/alphatize.scm 529 */
							BgL_objectz00_bglt BgL_tmpz00_4720;

							BgL_tmpz00_4720 = ((BgL_objectz00_bglt) BgL_new1424z00_3749);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4720, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_new1424z00_3749);
						BgL_new1420z00_3748 = BgL_new1424z00_3749;
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_new1420z00_3748)))->BgL_locz00) =
						((obj_t) (((BgL_nodez00_bglt)
									COBJECT(((BgL_nodez00_bglt) ((BgL_retblockz00_bglt)
												BgL_nodez00_3588))))->BgL_locz00)), BUNSPEC);
					((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_new1420z00_3748)))->BgL_typez00) =
						((BgL_typez00_bglt) (((BgL_nodez00_bglt)
									COBJECT(((BgL_nodez00_bglt) ((BgL_retblockz00_bglt)
												BgL_nodez00_3588))))->BgL_typez00)), BUNSPEC);
					((((BgL_retblockz00_bglt) COBJECT(BgL_new1420z00_3748))->
							BgL_bodyz00) =
						((BgL_nodez00_bglt) (((BgL_retblockz00_bglt)
									COBJECT(((BgL_retblockz00_bglt) ((BgL_nodez00_bglt) (
													(BgL_retblockz00_bglt) BgL_nodez00_3588)))))->
								BgL_bodyz00)), BUNSPEC);
					BgL_resz00_3747 = BgL_new1420z00_3748;
				}
				{	/* Ast/alphatize.scm 530 */
					BgL_retblockzf2alphazf2_bglt BgL_wide1427z00_3751;

					BgL_wide1427z00_3751 =
						((BgL_retblockzf2alphazf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_retblockzf2alphazf2_bgl))));
					{	/* Ast/alphatize.scm 530 */
						obj_t BgL_auxz00_4744;
						BgL_objectz00_bglt BgL_tmpz00_4740;

						BgL_auxz00_4744 = ((obj_t) BgL_wide1427z00_3751);
						BgL_tmpz00_4740 =
							((BgL_objectz00_bglt)
							((BgL_retblockz00_bglt)
								((BgL_retblockz00_bglt) BgL_nodez00_3588)));
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4740, BgL_auxz00_4744);
					}
					((BgL_objectz00_bglt)
						((BgL_retblockz00_bglt) ((BgL_retblockz00_bglt) BgL_nodez00_3588)));
					{	/* Ast/alphatize.scm 530 */
						long BgL_arg2040z00_3752;

						BgL_arg2040z00_3752 =
							BGL_CLASS_NUM(BGl_retblockzf2alphazf2zzast_alphatiza7eza7);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt)
								((BgL_retblockz00_bglt)
									((BgL_retblockz00_bglt) BgL_nodez00_3588))),
							BgL_arg2040z00_3752);
					}
					((BgL_retblockz00_bglt)
						((BgL_retblockz00_bglt) ((BgL_retblockz00_bglt) BgL_nodez00_3588)));
				}
				{
					BgL_retblockzf2alphazf2_bglt BgL_auxz00_4758;

					{
						obj_t BgL_auxz00_4759;

						{	/* Ast/alphatize.scm 531 */
							BgL_objectz00_bglt BgL_tmpz00_4760;

							BgL_tmpz00_4760 =
								((BgL_objectz00_bglt)
								((BgL_retblockz00_bglt)
									((BgL_retblockz00_bglt) BgL_nodez00_3588)));
							BgL_auxz00_4759 = BGL_OBJECT_WIDENING(BgL_tmpz00_4760);
						}
						BgL_auxz00_4758 = ((BgL_retblockzf2alphazf2_bglt) BgL_auxz00_4759);
					}
					((((BgL_retblockzf2alphazf2_bglt) COBJECT(BgL_auxz00_4758))->
							BgL_alphaz00) =
						((BgL_retblockz00_bglt) BgL_resz00_3747), BUNSPEC);
				}
				((BgL_retblockz00_bglt) ((BgL_retblockz00_bglt) BgL_nodez00_3588));
				{	/* Ast/alphatize.scm 532 */
					BgL_nodez00_bglt BgL_nbodyz00_3753;

					BgL_nbodyz00_3753 =
						BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(
						(((BgL_retblockz00_bglt) COBJECT(
									((BgL_retblockz00_bglt) BgL_nodez00_3588)))->BgL_bodyz00),
						BgL_locz00_3589);
					{	/* Ast/alphatize.scm 533 */
						long BgL_arg2041z00_3754;

						{	/* Ast/alphatize.scm 533 */
							obj_t BgL_arg2042z00_3755;

							{	/* Ast/alphatize.scm 533 */
								obj_t BgL_arg2044z00_3756;

								{	/* Ast/alphatize.scm 533 */
									obj_t BgL_arg1815z00_3757;
									long BgL_arg1816z00_3758;

									BgL_arg1815z00_3757 = (BGl_za2classesza2z00zz__objectz00);
									{	/* Ast/alphatize.scm 533 */
										long BgL_arg1817z00_3759;

										BgL_arg1817z00_3759 =
											BGL_OBJECT_CLASS_NUM(
											((BgL_objectz00_bglt)
												((BgL_retblockz00_bglt) BgL_nodez00_3588)));
										BgL_arg1816z00_3758 = (BgL_arg1817z00_3759 - OBJECT_TYPE);
									}
									BgL_arg2044z00_3756 =
										VECTOR_REF(BgL_arg1815z00_3757, BgL_arg1816z00_3758);
								}
								BgL_arg2042z00_3755 =
									BGl_classzd2superzd2zz__objectz00(BgL_arg2044z00_3756);
							}
							{	/* Ast/alphatize.scm 533 */
								obj_t BgL_tmpz00_4779;

								BgL_tmpz00_4779 = ((obj_t) BgL_arg2042z00_3755);
								BgL_arg2041z00_3754 = BGL_CLASS_NUM(BgL_tmpz00_4779);
						}}
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt)
								((BgL_retblockz00_bglt) BgL_nodez00_3588)),
							BgL_arg2041z00_3754);
					}
					{	/* Ast/alphatize.scm 533 */
						BgL_objectz00_bglt BgL_tmpz00_4785;

						BgL_tmpz00_4785 =
							((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_nodez00_3588));
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4785, BFALSE);
					}
					((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_nodez00_3588));
					((BgL_retblockz00_bglt) BgL_nodez00_3588);
					((((BgL_retblockz00_bglt) COBJECT(BgL_resz00_3747))->BgL_bodyz00) =
						((BgL_nodez00_bglt) BgL_nbodyz00_3753), BUNSPEC);
					return ((BgL_nodez00_bglt) BgL_resz00_3747);
				}
			}
		}

	}



/* &do-alphatize-jump-ex1669 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2jumpzd2ex1669z17zzast_alphatiza7eza7(obj_t
		BgL_envz00_3590, obj_t BgL_nodez00_3591, obj_t BgL_locz00_3592)
	{
		{	/* Ast/alphatize.scm 518 */
			{	/* Ast/alphatize.scm 519 */
				BgL_jumpzd2exzd2itz00_bglt BgL_new1415z00_3761;

				{	/* Ast/alphatize.scm 520 */
					BgL_jumpzd2exzd2itz00_bglt BgL_new1418z00_3762;

					BgL_new1418z00_3762 =
						((BgL_jumpzd2exzd2itz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_jumpzd2exzd2itz00_bgl))));
					{	/* Ast/alphatize.scm 520 */
						long BgL_arg2039z00_3763;

						BgL_arg2039z00_3763 =
							BGL_CLASS_NUM(BGl_jumpzd2exzd2itz00zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1418z00_3762), BgL_arg2039z00_3763);
					}
					{	/* Ast/alphatize.scm 520 */
						BgL_objectz00_bglt BgL_tmpz00_4798;

						BgL_tmpz00_4798 = ((BgL_objectz00_bglt) BgL_new1418z00_3762);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4798, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1418z00_3762);
					BgL_new1415z00_3761 = BgL_new1418z00_3762;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1415z00_3761)))->BgL_locz00) =
					((obj_t)
						BGl_getzd2locationzd2zzast_alphatiza7eza7(((BgL_nodez00_bglt) (
									(BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3591)),
							BgL_locz00_3592)), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1415z00_3761)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt)
								COBJECT(((BgL_nodez00_bglt) ((BgL_jumpzd2exzd2itz00_bglt)
											BgL_nodez00_3591))))->BgL_typez00)), BUNSPEC);
				{
					BgL_nodez00_bglt BgL_auxz00_4812;

					{	/* Ast/alphatize.scm 521 */
						BgL_nodez00_bglt BgL_arg2037z00_3764;

						BgL_arg2037z00_3764 =
							(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3591)))->
							BgL_exitz00);
						BgL_auxz00_4812 =
							BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(BgL_arg2037z00_3764,
							BgL_locz00_3592);
					}
					((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(BgL_new1415z00_3761))->
							BgL_exitz00) = ((BgL_nodez00_bglt) BgL_auxz00_4812), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_4817;

					{	/* Ast/alphatize.scm 522 */
						BgL_nodez00_bglt BgL_arg2038z00_3765;

						BgL_arg2038z00_3765 =
							(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3591)))->
							BgL_valuez00);
						BgL_auxz00_4817 =
							BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(BgL_arg2038z00_3765,
							BgL_locz00_3592);
					}
					((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(BgL_new1415z00_3761))->
							BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_4817), BUNSPEC);
				}
				return ((BgL_nodez00_bglt) BgL_new1415z00_3761);
			}
		}

	}



/* &do-alphatize-set-ex-1667 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2setzd2exzd21667zc5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3593, obj_t BgL_nodez00_3594, obj_t BgL_locz00_3595)
	{
		{	/* Ast/alphatize.scm 491 */
			{	/* Ast/alphatize.scm 492 */
				BgL_variablez00_bglt BgL_oldzd2varzd2_3767;

				BgL_oldzd2varzd2_3767 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
										((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3594)))->
								BgL_varz00)))->BgL_variablez00);
				{	/* Ast/alphatize.scm 492 */
					BgL_valuez00_bglt BgL_oldzd2exitzd2_3768;

					BgL_oldzd2exitzd2_3768 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_oldzd2varzd2_3767))))->BgL_valuez00);
					{	/* Ast/alphatize.scm 493 */
						obj_t BgL_oldzd2hdlgzd2_3769;

						BgL_oldzd2hdlgzd2_3769 =
							(((BgL_sexitz00_bglt) COBJECT(
									((BgL_sexitz00_bglt) BgL_oldzd2exitzd2_3768)))->
							BgL_handlerz00);
						{	/* Ast/alphatize.scm 494 */
							obj_t BgL_alphazd2hdlgzd2_3770;

							BgL_alphazd2hdlgzd2_3770 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_oldzd2hdlgzd2_3769)))->
								BgL_fastzd2alphazd2);
							{	/* Ast/alphatize.scm 495 */
								BgL_localz00_bglt BgL_newzd2varzd2_3771;

								{	/* Ast/alphatize.scm 496 */
									obj_t BgL_arg2030z00_3772;
									BgL_typez00_bglt BgL_arg2031z00_3773;
									BgL_sexitz00_bglt BgL_arg2033z00_3774;

									BgL_arg2030z00_3772 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_oldzd2varzd2_3767))))->
										BgL_idz00);
									BgL_arg2031z00_3773 =
										(((BgL_variablez00_bglt)
											COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
														BgL_oldzd2varzd2_3767))))->BgL_typez00);
									{	/* Ast/alphatize.scm 498 */
										BgL_sexitz00_bglt BgL_new1403z00_3775;

										{	/* Ast/alphatize.scm 499 */
											BgL_sexitz00_bglt BgL_new1406z00_3776;

											BgL_new1406z00_3776 =
												((BgL_sexitz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_sexitz00_bgl))));
											{	/* Ast/alphatize.scm 499 */
												long BgL_arg2034z00_3777;

												BgL_arg2034z00_3777 =
													BGL_CLASS_NUM(BGl_sexitz00zzast_varz00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1406z00_3776),
													BgL_arg2034z00_3777);
											}
											{	/* Ast/alphatize.scm 499 */
												BgL_objectz00_bglt BgL_tmpz00_4843;

												BgL_tmpz00_4843 =
													((BgL_objectz00_bglt) BgL_new1406z00_3776);
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4843, BFALSE);
											}
											((BgL_objectz00_bglt) BgL_new1406z00_3776);
											BgL_new1403z00_3775 = BgL_new1406z00_3776;
										}
										((((BgL_sexitz00_bglt) COBJECT(BgL_new1403z00_3775))->
												BgL_handlerz00) =
											((obj_t) BgL_alphazd2hdlgzd2_3770), BUNSPEC);
										((((BgL_sexitz00_bglt) COBJECT(BgL_new1403z00_3775))->
												BgL_detachedzf3zf3) =
											((bool_t) (((BgL_sexitz00_bglt)
														COBJECT(((BgL_sexitz00_bglt)
																BgL_oldzd2exitzd2_3768)))->BgL_detachedzf3zf3)),
											BUNSPEC);
										BgL_arg2033z00_3774 = BgL_new1403z00_3775;
									}
									BgL_newzd2varzd2_3771 =
										BGl_makezd2localzd2sexitz00zzast_localz00
										(BgL_arg2030z00_3772, BgL_arg2031z00_3773,
										BgL_arg2033z00_3774);
								}
								{	/* Ast/alphatize.scm 496 */
									BgL_nodez00_bglt BgL_oldzd2bodyzd2_3778;

									BgL_oldzd2bodyzd2_3778 =
										(((BgL_setzd2exzd2itz00_bglt) COBJECT(
												((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3594)))->
										BgL_bodyz00);
									{	/* Ast/alphatize.scm 501 */
										BgL_nodez00_bglt BgL_oldzd2onexitzd2_3779;

										BgL_oldzd2onexitzd2_3779 =
											(((BgL_setzd2exzd2itz00_bglt) COBJECT(
													((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3594)))->
											BgL_onexitz00);
										{	/* Ast/alphatize.scm 502 */

											{	/* Ast/alphatize.scm 503 */
												bool_t BgL_arg2020z00_3780;

												BgL_arg2020z00_3780 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_localz00_bglt) BgL_oldzd2varzd2_3767))))->
													BgL_userzf3zf3);
												((((BgL_variablez00_bglt)
															COBJECT(((BgL_variablez00_bglt)
																	BgL_newzd2varzd2_3771)))->BgL_userzf3zf3) =
													((bool_t) BgL_arg2020z00_3780), BUNSPEC);
											}
											{	/* Ast/alphatize.scm 504 */
												BgL_setzd2exzd2itz00_bglt BgL_new1407z00_3781;

												{	/* Ast/alphatize.scm 505 */
													BgL_setzd2exzd2itz00_bglt BgL_new1410z00_3782;

													BgL_new1410z00_3782 =
														((BgL_setzd2exzd2itz00_bglt)
														BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_setzd2exzd2itz00_bgl))));
													{	/* Ast/alphatize.scm 505 */
														long BgL_arg2029z00_3783;

														BgL_arg2029z00_3783 =
															BGL_CLASS_NUM(BGl_setzd2exzd2itz00zzast_nodez00);
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1410z00_3782),
															BgL_arg2029z00_3783);
													}
													{	/* Ast/alphatize.scm 505 */
														BgL_objectz00_bglt BgL_tmpz00_4865;

														BgL_tmpz00_4865 =
															((BgL_objectz00_bglt) BgL_new1410z00_3782);
														BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4865, BFALSE);
													}
													((BgL_objectz00_bglt) BgL_new1410z00_3782);
													BgL_new1407z00_3781 = BgL_new1410z00_3782;
												}
												((((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt) BgL_new1407z00_3781)))->
														BgL_locz00) =
													((obj_t)
														BGl_getzd2locationzd2zzast_alphatiza7eza7((
																(BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt)
																	BgL_nodez00_3594)), BgL_locz00_3595)),
													BUNSPEC);
												((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																	BgL_new1407z00_3781)))->BgL_typez00) =
													((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																COBJECT(((BgL_nodez00_bglt) (
																			(BgL_setzd2exzd2itz00_bglt)
																			BgL_nodez00_3594))))->BgL_typez00)),
													BUNSPEC);
												{
													BgL_varz00_bglt BgL_auxz00_4879;

													{	/* Ast/alphatize.scm 506 */
														BgL_nodez00_bglt BgL_duplicated1413z00_3784;
														BgL_refz00_bglt BgL_new1411z00_3785;

														BgL_duplicated1413z00_3784 =
															((BgL_nodez00_bglt)
															(((BgL_setzd2exzd2itz00_bglt) COBJECT(
																		((BgL_setzd2exzd2itz00_bglt)
																			BgL_nodez00_3594)))->BgL_varz00));
														{	/* Ast/alphatize.scm 507 */
															BgL_refz00_bglt BgL_new1414z00_3786;

															BgL_new1414z00_3786 =
																((BgL_refz00_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_refz00_bgl))));
															{	/* Ast/alphatize.scm 507 */
																long BgL_arg2021z00_3787;

																{	/* Ast/alphatize.scm 507 */
																	obj_t BgL_classz00_3788;

																	BgL_classz00_3788 = BGl_refz00zzast_nodez00;
																	BgL_arg2021z00_3787 =
																		BGL_CLASS_NUM(BgL_classz00_3788);
																}
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1414z00_3786),
																	BgL_arg2021z00_3787);
															}
															{	/* Ast/alphatize.scm 507 */
																BgL_objectz00_bglt BgL_tmpz00_4887;

																BgL_tmpz00_4887 =
																	((BgL_objectz00_bglt) BgL_new1414z00_3786);
																BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4887,
																	BFALSE);
															}
															((BgL_objectz00_bglt) BgL_new1414z00_3786);
															BgL_new1411z00_3785 = BgL_new1414z00_3786;
														}
														((((BgL_nodez00_bglt) COBJECT(
																		((BgL_nodez00_bglt) BgL_new1411z00_3785)))->
																BgL_locz00) =
															((obj_t)
																BGl_getzd2locationzd2zzast_alphatiza7eza7((
																		(BgL_nodez00_bglt) (
																			(BgL_setzd2exzd2itz00_bglt)
																			BgL_nodez00_3594)), BgL_locz00_3595)),
															BUNSPEC);
														((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																			BgL_new1411z00_3785)))->BgL_typez00) =
															((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																		COBJECT(BgL_duplicated1413z00_3784))->
																	BgL_typez00)), BUNSPEC);
														((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																			BgL_new1411z00_3785)))->BgL_variablez00) =
															((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
																	BgL_newzd2varzd2_3771)), BUNSPEC);
														BgL_auxz00_4879 =
															((BgL_varz00_bglt) BgL_new1411z00_3785);
													}
													((((BgL_setzd2exzd2itz00_bglt)
																COBJECT(BgL_new1407z00_3781))->BgL_varz00) =
														((BgL_varz00_bglt) BgL_auxz00_4879), BUNSPEC);
												}
												{
													BgL_nodez00_bglt BgL_auxz00_4904;

													{	/* Ast/alphatize.scm 509 */
														obj_t BgL_arg2022z00_3789;
														obj_t BgL_arg2024z00_3790;
														obj_t BgL_arg2025z00_3791;

														{	/* Ast/alphatize.scm 509 */
															obj_t BgL_list2026z00_3792;

															BgL_list2026z00_3792 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_oldzd2varzd2_3767), BNIL);
															BgL_arg2022z00_3789 = BgL_list2026z00_3792;
														}
														{	/* Ast/alphatize.scm 510 */
															obj_t BgL_list2027z00_3793;

															BgL_list2027z00_3793 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_newzd2varzd2_3771), BNIL);
															BgL_arg2024z00_3790 = BgL_list2027z00_3793;
														}
														BgL_arg2025z00_3791 =
															BGl_getzd2locationzd2zzast_alphatiza7eza7(
															((BgL_nodez00_bglt)
																((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3594)),
															BgL_locz00_3595);
														BgL_auxz00_4904 =
															BGl_alphatiza7eza7zzast_alphatiza7eza7
															(BgL_arg2022z00_3789, BgL_arg2024z00_3790,
															BgL_arg2025z00_3791, BgL_oldzd2bodyzd2_3778);
													}
													((((BgL_setzd2exzd2itz00_bglt)
																COBJECT(BgL_new1407z00_3781))->BgL_bodyz00) =
														((BgL_nodez00_bglt) BgL_auxz00_4904), BUNSPEC);
												}
												((((BgL_setzd2exzd2itz00_bglt)
															COBJECT(BgL_new1407z00_3781))->BgL_onexitz00) =
													((BgL_nodez00_bglt)
														BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7
														(BgL_oldzd2onexitzd2_3779, BgL_locz00_3595)),
													BUNSPEC);
												return ((BgL_nodez00_bglt) BgL_new1407z00_3781);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &do-alphatize-let-var1665 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2letzd2var1665z17zzast_alphatiza7eza7(obj_t
		BgL_envz00_3596, obj_t BgL_nodez00_3597, obj_t BgL_locz00_3598)
	{
		{	/* Ast/alphatize.scm 463 */
			{	/* Ast/alphatize.scm 464 */
				obj_t BgL_oldzd2localszd2_3795;

				{	/* Ast/alphatize.scm 464 */
					obj_t BgL_l1583z00_3796;

					BgL_l1583z00_3796 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_3597)))->BgL_bindingsz00);
					if (NULLP(BgL_l1583z00_3796))
						{	/* Ast/alphatize.scm 464 */
							BgL_oldzd2localszd2_3795 = BNIL;
						}
					else
						{	/* Ast/alphatize.scm 464 */
							obj_t BgL_head1585z00_3797;

							{	/* Ast/alphatize.scm 464 */
								obj_t BgL_arg2018z00_3798;

								{	/* Ast/alphatize.scm 464 */
									obj_t BgL_pairz00_3799;

									BgL_pairz00_3799 = CAR(((obj_t) BgL_l1583z00_3796));
									BgL_arg2018z00_3798 = CAR(BgL_pairz00_3799);
								}
								BgL_head1585z00_3797 =
									MAKE_YOUNG_PAIR(BgL_arg2018z00_3798, BNIL);
							}
							{	/* Ast/alphatize.scm 464 */
								obj_t BgL_g1588z00_3800;

								BgL_g1588z00_3800 = CDR(((obj_t) BgL_l1583z00_3796));
								{
									obj_t BgL_l1583z00_3802;
									obj_t BgL_tail1586z00_3803;

									BgL_l1583z00_3802 = BgL_g1588z00_3800;
									BgL_tail1586z00_3803 = BgL_head1585z00_3797;
								BgL_zc3z04anonymousza32013ze3z87_3801:
									if (NULLP(BgL_l1583z00_3802))
										{	/* Ast/alphatize.scm 464 */
											BgL_oldzd2localszd2_3795 = BgL_head1585z00_3797;
										}
									else
										{	/* Ast/alphatize.scm 464 */
											obj_t BgL_newtail1587z00_3804;

											{	/* Ast/alphatize.scm 464 */
												obj_t BgL_arg2016z00_3805;

												{	/* Ast/alphatize.scm 464 */
													obj_t BgL_pairz00_3806;

													BgL_pairz00_3806 = CAR(((obj_t) BgL_l1583z00_3802));
													BgL_arg2016z00_3805 = CAR(BgL_pairz00_3806);
												}
												BgL_newtail1587z00_3804 =
													MAKE_YOUNG_PAIR(BgL_arg2016z00_3805, BNIL);
											}
											SET_CDR(BgL_tail1586z00_3803, BgL_newtail1587z00_3804);
											{	/* Ast/alphatize.scm 464 */
												obj_t BgL_arg2015z00_3807;

												BgL_arg2015z00_3807 = CDR(((obj_t) BgL_l1583z00_3802));
												{
													obj_t BgL_tail1586z00_4937;
													obj_t BgL_l1583z00_4936;

													BgL_l1583z00_4936 = BgL_arg2015z00_3807;
													BgL_tail1586z00_4937 = BgL_newtail1587z00_3804;
													BgL_tail1586z00_3803 = BgL_tail1586z00_4937;
													BgL_l1583z00_3802 = BgL_l1583z00_4936;
													goto BgL_zc3z04anonymousza32013ze3z87_3801;
												}
											}
										}
								}
							}
						}
				}
				{	/* Ast/alphatize.scm 464 */
					obj_t BgL_newzd2localszd2_3808;

					if (NULLP(BgL_oldzd2localszd2_3795))
						{	/* Ast/alphatize.scm 465 */
							BgL_newzd2localszd2_3808 = BNIL;
						}
					else
						{	/* Ast/alphatize.scm 465 */
							obj_t BgL_head1592z00_3809;

							BgL_head1592z00_3809 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1590z00_3811;
								obj_t BgL_tail1593z00_3812;

								BgL_l1590z00_3811 = BgL_oldzd2localszd2_3795;
								BgL_tail1593z00_3812 = BgL_head1592z00_3809;
							BgL_zc3z04anonymousza32003ze3z87_3810:
								if (NULLP(BgL_l1590z00_3811))
									{	/* Ast/alphatize.scm 465 */
										BgL_newzd2localszd2_3808 = CDR(BgL_head1592z00_3809);
									}
								else
									{	/* Ast/alphatize.scm 465 */
										obj_t BgL_newtail1594z00_3813;

										{	/* Ast/alphatize.scm 465 */
											BgL_localz00_bglt BgL_arg2007z00_3814;

											{	/* Ast/alphatize.scm 465 */
												obj_t BgL_lz00_3815;

												BgL_lz00_3815 = CAR(((obj_t) BgL_l1590z00_3811));
												{	/* Ast/alphatize.scm 470 */
													BgL_localz00_bglt BgL_varz00_3816;

													{	/* Ast/alphatize.scm 470 */
														obj_t BgL_arg2010z00_3817;
														BgL_typez00_bglt BgL_arg2011z00_3818;

														BgL_arg2010z00_3817 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt) BgL_lz00_3815))))->
															BgL_idz00);
														BgL_arg2011z00_3818 =
															(((BgL_variablez00_bglt)
																COBJECT(((BgL_variablez00_bglt) (
																			(BgL_localz00_bglt) BgL_lz00_3815))))->
															BgL_typez00);
														BgL_varz00_3816 =
															BGl_makezd2localzd2svarz00zzast_localz00
															(BgL_arg2010z00_3817, BgL_arg2011z00_3818);
													}
													{	/* Ast/alphatize.scm 472 */
														bool_t BgL_arg2008z00_3819;

														BgL_arg2008z00_3819 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt) BgL_lz00_3815))))->
															BgL_userzf3zf3);
														((((BgL_variablez00_bglt)
																	COBJECT(((BgL_variablez00_bglt)
																			BgL_varz00_3816)))->BgL_userzf3zf3) =
															((bool_t) BgL_arg2008z00_3819), BUNSPEC);
													}
													{	/* Ast/alphatize.scm 473 */
														obj_t BgL_arg2009z00_3820;

														BgL_arg2009z00_3820 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt) BgL_lz00_3815))))->
															BgL_accessz00);
														((((BgL_variablez00_bglt)
																	COBJECT(((BgL_variablez00_bglt)
																			BgL_varz00_3816)))->BgL_accessz00) =
															((obj_t) BgL_arg2009z00_3820), BUNSPEC);
													}
													BgL_arg2007z00_3814 = BgL_varz00_3816;
												}
											}
											BgL_newtail1594z00_3813 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg2007z00_3814), BNIL);
										}
										SET_CDR(BgL_tail1593z00_3812, BgL_newtail1594z00_3813);
										{	/* Ast/alphatize.scm 465 */
											obj_t BgL_arg2006z00_3821;

											BgL_arg2006z00_3821 = CDR(((obj_t) BgL_l1590z00_3811));
											{
												obj_t BgL_tail1593z00_4969;
												obj_t BgL_l1590z00_4968;

												BgL_l1590z00_4968 = BgL_arg2006z00_3821;
												BgL_tail1593z00_4969 = BgL_newtail1594z00_3813;
												BgL_tail1593z00_3812 = BgL_tail1593z00_4969;
												BgL_l1590z00_3811 = BgL_l1590z00_4968;
												goto BgL_zc3z04anonymousza32003ze3z87_3810;
											}
										}
									}
							}
						}
					{	/* Ast/alphatize.scm 465 */
						obj_t BgL_newzd2bindingszd2_3822;

						{	/* Ast/alphatize.scm 476 */
							obj_t BgL_ll1595z00_3823;

							BgL_ll1595z00_3823 =
								(((BgL_letzd2varzd2_bglt) COBJECT(
										((BgL_letzd2varzd2_bglt) BgL_nodez00_3597)))->
								BgL_bindingsz00);
							if (NULLP(BgL_ll1595z00_3823))
								{	/* Ast/alphatize.scm 476 */
									BgL_newzd2bindingszd2_3822 = BNIL;
								}
							else
								{	/* Ast/alphatize.scm 476 */
									obj_t BgL_head1597z00_3824;

									BgL_head1597z00_3824 = MAKE_YOUNG_PAIR(BNIL, BNIL);
									{
										obj_t BgL_ll1595z00_3826;
										obj_t BgL_ll1596z00_3827;
										obj_t BgL_tail1598z00_3828;

										BgL_ll1595z00_3826 = BgL_ll1595z00_3823;
										BgL_ll1596z00_3827 = BgL_newzd2localszd2_3808;
										BgL_tail1598z00_3828 = BgL_head1597z00_3824;
									BgL_zc3z04anonymousza31995ze3z87_3825:
										if (NULLP(BgL_ll1595z00_3826))
											{	/* Ast/alphatize.scm 476 */
												BgL_newzd2bindingszd2_3822 = CDR(BgL_head1597z00_3824);
											}
										else
											{	/* Ast/alphatize.scm 476 */
												obj_t BgL_newtail1599z00_3829;

												{	/* Ast/alphatize.scm 476 */
													obj_t BgL_arg1999z00_3830;

													{	/* Ast/alphatize.scm 476 */
														obj_t BgL_bindingz00_3831;
														obj_t BgL_newzd2localzd2_3832;

														BgL_bindingz00_3831 =
															CAR(((obj_t) BgL_ll1595z00_3826));
														BgL_newzd2localzd2_3832 =
															CAR(((obj_t) BgL_ll1596z00_3827));
														{	/* Ast/alphatize.scm 477 */
															BgL_nodez00_bglt BgL_arg2000z00_3833;

															{	/* Ast/alphatize.scm 477 */
																obj_t BgL_arg2001z00_3834;

																BgL_arg2001z00_3834 =
																	CDR(((obj_t) BgL_bindingz00_3831));
																BgL_arg2000z00_3833 =
																	BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(
																	((BgL_nodez00_bglt) BgL_arg2001z00_3834),
																	BgL_locz00_3598);
															}
															BgL_arg1999z00_3830 =
																MAKE_YOUNG_PAIR(BgL_newzd2localzd2_3832,
																((obj_t) BgL_arg2000z00_3833));
														}
													}
													BgL_newtail1599z00_3829 =
														MAKE_YOUNG_PAIR(BgL_arg1999z00_3830, BNIL);
												}
												SET_CDR(BgL_tail1598z00_3828, BgL_newtail1599z00_3829);
												{	/* Ast/alphatize.scm 476 */
													obj_t BgL_arg1997z00_3835;
													obj_t BgL_arg1998z00_3836;

													BgL_arg1997z00_3835 =
														CDR(((obj_t) BgL_ll1595z00_3826));
													BgL_arg1998z00_3836 =
														CDR(((obj_t) BgL_ll1596z00_3827));
													{
														obj_t BgL_tail1598z00_4996;
														obj_t BgL_ll1596z00_4995;
														obj_t BgL_ll1595z00_4994;

														BgL_ll1595z00_4994 = BgL_arg1997z00_3835;
														BgL_ll1596z00_4995 = BgL_arg1998z00_3836;
														BgL_tail1598z00_4996 = BgL_newtail1599z00_3829;
														BgL_tail1598z00_3828 = BgL_tail1598z00_4996;
														BgL_ll1596z00_3827 = BgL_ll1596z00_4995;
														BgL_ll1595z00_3826 = BgL_ll1595z00_4994;
														goto BgL_zc3z04anonymousza31995ze3z87_3825;
													}
												}
											}
									}
								}
						}
						{	/* Ast/alphatize.scm 476 */

							{	/* Ast/alphatize.scm 480 */
								BgL_letzd2varzd2_bglt BgL_new1396z00_3837;

								{	/* Ast/alphatize.scm 481 */
									BgL_letzd2varzd2_bglt BgL_new1402z00_3838;

									BgL_new1402z00_3838 =
										((BgL_letzd2varzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_letzd2varzd2_bgl))));
									{	/* Ast/alphatize.scm 481 */
										long BgL_arg1993z00_3839;

										BgL_arg1993z00_3839 =
											BGL_CLASS_NUM(BGl_letzd2varzd2zzast_nodez00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1402z00_3838),
											BgL_arg1993z00_3839);
									}
									{	/* Ast/alphatize.scm 481 */
										BgL_objectz00_bglt BgL_tmpz00_5001;

										BgL_tmpz00_5001 =
											((BgL_objectz00_bglt) BgL_new1402z00_3838);
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5001, BFALSE);
									}
									((BgL_objectz00_bglt) BgL_new1402z00_3838);
									BgL_new1396z00_3837 = BgL_new1402z00_3838;
								}
								((((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt) BgL_new1396z00_3837)))->
										BgL_locz00) =
									((obj_t)
										BGl_getzd2locationzd2zzast_alphatiza7eza7((
												(BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt)
													BgL_nodez00_3597)), BgL_locz00_3598)), BUNSPEC);
								((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
													BgL_new1396z00_3837)))->BgL_typez00) =
									((BgL_typez00_bglt) (((BgL_nodez00_bglt)
												COBJECT(((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt)
															BgL_nodez00_3597))))->BgL_typez00)), BUNSPEC);
								((((BgL_nodezf2effectzf2_bglt)
											COBJECT(((BgL_nodezf2effectzf2_bglt)
													BgL_new1396z00_3837)))->BgL_sidezd2effectzd2) =
									((obj_t) (((BgL_nodezf2effectzf2_bglt)
												COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt)
															((BgL_letzd2varzd2_bglt) BgL_nodez00_3597)))))->
											BgL_sidezd2effectzd2)), BUNSPEC);
								((((BgL_nodezf2effectzf2_bglt)
											COBJECT(((BgL_nodezf2effectzf2_bglt)
													BgL_new1396z00_3837)))->BgL_keyz00) =
									((obj_t) (((BgL_nodezf2effectzf2_bglt)
												COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt)
															((BgL_letzd2varzd2_bglt) BgL_nodez00_3597)))))->
											BgL_keyz00)), BUNSPEC);
								((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1396z00_3837))->
										BgL_bindingsz00) =
									((obj_t) BgL_newzd2bindingszd2_3822), BUNSPEC);
								{
									BgL_nodez00_bglt BgL_auxz00_5028;

									{	/* Ast/alphatize.scm 485 */
										obj_t BgL_arg1991z00_3840;
										BgL_nodez00_bglt BgL_arg1992z00_3841;

										BgL_arg1991z00_3840 =
											BGl_getzd2locationzd2zzast_alphatiza7eza7(
											((BgL_nodez00_bglt)
												((BgL_letzd2varzd2_bglt) BgL_nodez00_3597)),
											BgL_locz00_3598);
										BgL_arg1992z00_3841 =
											(((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt)
														BgL_nodez00_3597)))->BgL_bodyz00);
										BgL_auxz00_5028 =
											BGl_alphatiza7eza7zzast_alphatiza7eza7
											(BgL_oldzd2localszd2_3795, BgL_newzd2localszd2_3808,
											BgL_arg1991z00_3840, BgL_arg1992z00_3841);
									}
									((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1396z00_3837))->
											BgL_bodyz00) =
										((BgL_nodez00_bglt) BgL_auxz00_5028), BUNSPEC);
								}
								((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1396z00_3837))->
										BgL_removablezf3zf3) =
									((bool_t) (((BgL_letzd2varzd2_bglt)
												COBJECT(((BgL_letzd2varzd2_bglt) ((BgL_nodez00_bglt) (
																(BgL_letzd2varzd2_bglt) BgL_nodez00_3597)))))->
											BgL_removablezf3zf3)), BUNSPEC);
								return ((BgL_nodez00_bglt) BgL_new1396z00_3837);
							}
						}
					}
				}
			}
		}

	}



/* &do-alphatize-let-fun1663 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2letzd2fun1663z17zzast_alphatiza7eza7(obj_t
		BgL_envz00_3599, obj_t BgL_nodez00_3600, obj_t BgL_locz00_3601)
	{
		{	/* Ast/alphatize.scm 423 */
			{	/* Ast/alphatize.scm 424 */
				obj_t BgL_oldzd2localszd2_3843;

				BgL_oldzd2localszd2_3843 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_3600)))->BgL_localsz00);
				{	/* Ast/alphatize.scm 424 */
					obj_t BgL_newzd2localszd2_3844;

					if (NULLP(BgL_oldzd2localszd2_3843))
						{	/* Ast/alphatize.scm 425 */
							BgL_newzd2localszd2_3844 = BNIL;
						}
					else
						{	/* Ast/alphatize.scm 425 */
							obj_t BgL_head1572z00_3845;

							BgL_head1572z00_3845 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1570z00_3847;
								obj_t BgL_tail1573z00_3848;

								BgL_l1570z00_3847 = BgL_oldzd2localszd2_3843;
								BgL_tail1573z00_3848 = BgL_head1572z00_3845;
							BgL_zc3z04anonymousza31983ze3z87_3846:
								if (NULLP(BgL_l1570z00_3847))
									{	/* Ast/alphatize.scm 425 */
										BgL_newzd2localszd2_3844 = CDR(BgL_head1572z00_3845);
									}
								else
									{	/* Ast/alphatize.scm 425 */
										obj_t BgL_newtail1574z00_3849;

										{	/* Ast/alphatize.scm 425 */
											BgL_localz00_bglt BgL_arg1986z00_3850;

											{	/* Ast/alphatize.scm 425 */
												obj_t BgL_lz00_3851;

												BgL_lz00_3851 = CAR(((obj_t) BgL_l1570z00_3847));
												{	/* Ast/alphatize.scm 426 */
													BgL_localz00_bglt BgL_vz00_3852;

													{	/* Ast/alphatize.scm 426 */
														obj_t BgL_arg1988z00_3853;
														BgL_typez00_bglt BgL_arg1989z00_3854;
														BgL_valuez00_bglt BgL_arg1990z00_3855;

														BgL_arg1988z00_3853 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt) BgL_lz00_3851))))->
															BgL_idz00);
														BgL_arg1989z00_3854 =
															(((BgL_variablez00_bglt)
																COBJECT(((BgL_variablez00_bglt) (
																			(BgL_localz00_bglt) BgL_lz00_3851))))->
															BgL_typez00);
														BgL_arg1990z00_3855 =
															(((BgL_variablez00_bglt)
																COBJECT(((BgL_variablez00_bglt) (
																			(BgL_localz00_bglt) BgL_lz00_3851))))->
															BgL_valuez00);
														BgL_vz00_3852 =
															BGl_makezd2localzd2sfunz00zzast_localz00
															(BgL_arg1988z00_3853, BgL_arg1989z00_3854,
															((BgL_sfunz00_bglt) BgL_arg1990z00_3855));
													}
													{	/* Ast/alphatize.scm 430 */
														long BgL_arg1987z00_3856;

														BgL_arg1987z00_3856 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt) BgL_lz00_3851)))->
															BgL_occurrencez00);
														((((BgL_variablez00_bglt)
																	COBJECT(((BgL_variablez00_bglt)
																			BgL_vz00_3852)))->BgL_occurrencez00) =
															((long) BgL_arg1987z00_3856), BUNSPEC);
													}
													BgL_arg1986z00_3850 = BgL_vz00_3852;
											}}
											BgL_newtail1574z00_3849 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg1986z00_3850), BNIL);
										}
										SET_CDR(BgL_tail1573z00_3848, BgL_newtail1574z00_3849);
										{	/* Ast/alphatize.scm 425 */
											obj_t BgL_arg1985z00_3857;

											BgL_arg1985z00_3857 = CDR(((obj_t) BgL_l1570z00_3847));
											{
												obj_t BgL_tail1573z00_5073;
												obj_t BgL_l1570z00_5072;

												BgL_l1570z00_5072 = BgL_arg1985z00_3857;
												BgL_tail1573z00_5073 = BgL_newtail1574z00_3849;
												BgL_tail1573z00_3848 = BgL_tail1573z00_5073;
												BgL_l1570z00_3847 = BgL_l1570z00_5072;
												goto BgL_zc3z04anonymousza31983ze3z87_3846;
											}
										}
									}
							}
						}
					{	/* Ast/alphatize.scm 425 */

						{
							obj_t BgL_ll1580z00_3859;
							obj_t BgL_ll1581z00_3860;

							BgL_ll1580z00_3859 = BgL_oldzd2localszd2_3843;
							BgL_ll1581z00_3860 = BgL_newzd2localszd2_3844;
						BgL_zc3z04anonymousza31963ze3z87_3858:
							if (NULLP(BgL_ll1580z00_3859))
								{	/* Ast/alphatize.scm 433 */
									((bool_t) 1);
								}
							else
								{	/* Ast/alphatize.scm 433 */
									{	/* Ast/alphatize.scm 434 */
										obj_t BgL_oldz00_3861;
										obj_t BgL_newz00_3862;

										BgL_oldz00_3861 = CAR(((obj_t) BgL_ll1580z00_3859));
										BgL_newz00_3862 = CAR(((obj_t) BgL_ll1581z00_3860));
										{	/* Ast/alphatize.scm 434 */
											BgL_valuez00_bglt BgL_oldzd2sfunzd2_3863;

											BgL_oldzd2sfunzd2_3863 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_localz00_bglt) BgL_oldz00_3861))))->
												BgL_valuez00);
											{	/* Ast/alphatize.scm 434 */
												obj_t BgL_oldzd2argszd2_3864;

												BgL_oldzd2argszd2_3864 =
													(((BgL_sfunz00_bglt) COBJECT(
															((BgL_sfunz00_bglt) BgL_oldzd2sfunzd2_3863)))->
													BgL_argsz00);
												{	/* Ast/alphatize.scm 435 */
													obj_t BgL_newzd2argszd2_3865;

													if (NULLP(BgL_oldzd2argszd2_3864))
														{	/* Ast/alphatize.scm 436 */
															BgL_newzd2argszd2_3865 = BNIL;
														}
													else
														{	/* Ast/alphatize.scm 436 */
															obj_t BgL_head1577z00_3866;

															BgL_head1577z00_3866 =
																MAKE_YOUNG_PAIR(BNIL, BNIL);
															{
																obj_t BgL_l1575z00_3868;
																obj_t BgL_tail1578z00_3869;

																BgL_l1575z00_3868 = BgL_oldzd2argszd2_3864;
																BgL_tail1578z00_3869 = BgL_head1577z00_3866;
															BgL_zc3z04anonymousza31971ze3z87_3867:
																if (NULLP(BgL_l1575z00_3868))
																	{	/* Ast/alphatize.scm 436 */
																		BgL_newzd2argszd2_3865 =
																			CDR(BgL_head1577z00_3866);
																	}
																else
																	{	/* Ast/alphatize.scm 436 */
																		obj_t BgL_newtail1579z00_3870;

																		{	/* Ast/alphatize.scm 436 */
																			BgL_localz00_bglt BgL_arg1974z00_3871;

																			{	/* Ast/alphatize.scm 436 */
																				obj_t BgL_lz00_3872;

																				BgL_lz00_3872 =
																					CAR(((obj_t) BgL_l1575z00_3868));
																				{	/* Ast/alphatize.scm 437 */
																					obj_t BgL_arg1975z00_3873;
																					BgL_typez00_bglt BgL_arg1976z00_3874;

																					BgL_arg1975z00_3873 =
																						(((BgL_variablez00_bglt) COBJECT(
																								((BgL_variablez00_bglt)
																									((BgL_localz00_bglt)
																										BgL_lz00_3872))))->
																						BgL_idz00);
																					BgL_arg1976z00_3874 =
																						(((BgL_variablez00_bglt)
																							COBJECT(((BgL_variablez00_bglt) (
																										(BgL_localz00_bglt)
																										BgL_lz00_3872))))->
																						BgL_typez00);
																					BgL_arg1974z00_3871 =
																						BGl_makezd2localzd2svarz00zzast_localz00
																						(BgL_arg1975z00_3873,
																						BgL_arg1976z00_3874);
																				}
																			}
																			BgL_newtail1579z00_3870 =
																				MAKE_YOUNG_PAIR(
																				((obj_t) BgL_arg1974z00_3871), BNIL);
																		}
																		SET_CDR(BgL_tail1578z00_3869,
																			BgL_newtail1579z00_3870);
																		{	/* Ast/alphatize.scm 436 */
																			obj_t BgL_arg1973z00_3875;

																			BgL_arg1973z00_3875 =
																				CDR(((obj_t) BgL_l1575z00_3868));
																			{
																				obj_t BgL_tail1578z00_5106;
																				obj_t BgL_l1575z00_5105;

																				BgL_l1575z00_5105 = BgL_arg1973z00_3875;
																				BgL_tail1578z00_5106 =
																					BgL_newtail1579z00_3870;
																				BgL_tail1578z00_3869 =
																					BgL_tail1578z00_5106;
																				BgL_l1575z00_3868 = BgL_l1575z00_5105;
																				goto
																					BgL_zc3z04anonymousza31971ze3z87_3867;
																			}
																		}
																	}
															}
														}
													{	/* Ast/alphatize.scm 436 */
														obj_t BgL_oldzd2bodyzd2_3876;

														BgL_oldzd2bodyzd2_3876 =
															(((BgL_sfunz00_bglt) COBJECT(
																	((BgL_sfunz00_bglt)
																		BgL_oldzd2sfunzd2_3863)))->BgL_bodyz00);
														{	/* Ast/alphatize.scm 440 */
															BgL_nodez00_bglt BgL_newzd2bodyzd2_3877;

															{	/* Ast/alphatize.scm 441 */
																obj_t BgL_arg1967z00_3878;
																obj_t BgL_arg1968z00_3879;
																obj_t BgL_arg1969z00_3880;

																BgL_arg1967z00_3878 =
																	BGl_appendzd221011zd2zzast_alphatiza7eza7
																	(BgL_oldzd2localszd2_3843,
																	BgL_oldzd2argszd2_3864);
																BgL_arg1968z00_3879 =
																	BGl_appendzd221011zd2zzast_alphatiza7eza7
																	(BgL_newzd2localszd2_3844,
																	BgL_newzd2argszd2_3865);
																BgL_arg1969z00_3880 =
																	BGl_getzd2locationzd2zzast_alphatiza7eza7((
																		(BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt)
																			BgL_nodez00_3600)), BgL_locz00_3601);
																BgL_newzd2bodyzd2_3877 =
																	BGl_alphatiza7eza7zzast_alphatiza7eza7
																	(BgL_arg1967z00_3878, BgL_arg1968z00_3879,
																	BgL_arg1969z00_3880,
																	((BgL_nodez00_bglt) BgL_oldzd2bodyzd2_3876));
															}
															{	/* Ast/alphatize.scm 441 */
																BgL_sfunz00_bglt BgL_newzd2sfunzd2_3881;

																{	/* Ast/alphatize.scm 445 */
																	BgL_sfunz00_bglt BgL_new1376z00_3882;

																	{	/* Ast/alphatize.scm 447 */
																		BgL_sfunz00_bglt BgL_new1389z00_3883;

																		BgL_new1389z00_3883 =
																			((BgL_sfunz00_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_sfunz00_bgl))));
																		{	/* Ast/alphatize.scm 447 */
																			long BgL_arg1966z00_3884;

																			BgL_arg1966z00_3884 =
																				BGL_CLASS_NUM(BGl_sfunz00zzast_varz00);
																			BGL_OBJECT_CLASS_NUM_SET(
																				((BgL_objectz00_bglt)
																					BgL_new1389z00_3883),
																				BgL_arg1966z00_3884);
																		}
																		{	/* Ast/alphatize.scm 447 */
																			BgL_objectz00_bglt BgL_tmpz00_5120;

																			BgL_tmpz00_5120 =
																				((BgL_objectz00_bglt)
																				BgL_new1389z00_3883);
																			BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5120,
																				BFALSE);
																		}
																		((BgL_objectz00_bglt) BgL_new1389z00_3883);
																		BgL_new1376z00_3882 = BgL_new1389z00_3883;
																	}
																	((((BgL_funz00_bglt) COBJECT(
																					((BgL_funz00_bglt)
																						BgL_new1376z00_3882)))->
																			BgL_arityz00) =
																		((long) (((BgL_funz00_bglt)
																					COBJECT(((BgL_funz00_bglt)
																							BgL_oldzd2sfunzd2_3863)))->
																				BgL_arityz00)), BUNSPEC);
																	((((BgL_funz00_bglt)
																				COBJECT(((BgL_funz00_bglt)
																						BgL_new1376z00_3882)))->
																			BgL_sidezd2effectzd2) =
																		((obj_t) (((BgL_funz00_bglt)
																					COBJECT(((BgL_funz00_bglt)
																							BgL_oldzd2sfunzd2_3863)))->
																				BgL_sidezd2effectzd2)), BUNSPEC);
																	((((BgL_funz00_bglt)
																				COBJECT(((BgL_funz00_bglt)
																						BgL_new1376z00_3882)))->
																			BgL_predicatezd2ofzd2) =
																		((obj_t) (((BgL_funz00_bglt)
																					COBJECT(((BgL_funz00_bglt)
																							BgL_oldzd2sfunzd2_3863)))->
																				BgL_predicatezd2ofzd2)), BUNSPEC);
																	((((BgL_funz00_bglt)
																				COBJECT(((BgL_funz00_bglt)
																						BgL_new1376z00_3882)))->
																			BgL_stackzd2allocatorzd2) =
																		((obj_t) (((BgL_funz00_bglt)
																					COBJECT(((BgL_funz00_bglt)
																							BgL_oldzd2sfunzd2_3863)))->
																				BgL_stackzd2allocatorzd2)), BUNSPEC);
																	((((BgL_funz00_bglt)
																				COBJECT(((BgL_funz00_bglt)
																						BgL_new1376z00_3882)))->
																			BgL_topzf3zf3) =
																		((bool_t) (((BgL_funz00_bglt)
																					COBJECT(((BgL_funz00_bglt)
																							BgL_oldzd2sfunzd2_3863)))->
																				BgL_topzf3zf3)), BUNSPEC);
																	((((BgL_funz00_bglt)
																				COBJECT(((BgL_funz00_bglt)
																						BgL_new1376z00_3882)))->
																			BgL_thezd2closurezd2) =
																		((obj_t) (((BgL_funz00_bglt)
																					COBJECT(((BgL_funz00_bglt)
																							BgL_oldzd2sfunzd2_3863)))->
																				BgL_thezd2closurezd2)), BUNSPEC);
																	((((BgL_funz00_bglt)
																				COBJECT(((BgL_funz00_bglt)
																						BgL_new1376z00_3882)))->
																			BgL_effectz00) =
																		((obj_t) (((BgL_funz00_bglt)
																					COBJECT(((BgL_funz00_bglt)
																							BgL_oldzd2sfunzd2_3863)))->
																				BgL_effectz00)), BUNSPEC);
																	((((BgL_funz00_bglt)
																				COBJECT(((BgL_funz00_bglt)
																						BgL_new1376z00_3882)))->
																			BgL_failsafez00) =
																		((obj_t) (((BgL_funz00_bglt)
																					COBJECT(((BgL_funz00_bglt)
																							BgL_oldzd2sfunzd2_3863)))->
																				BgL_failsafez00)), BUNSPEC);
																	((((BgL_funz00_bglt)
																				COBJECT(((BgL_funz00_bglt)
																						BgL_new1376z00_3882)))->
																			BgL_argszd2noescapezd2) =
																		((obj_t) (((BgL_funz00_bglt)
																					COBJECT(((BgL_funz00_bglt)
																							BgL_oldzd2sfunzd2_3863)))->
																				BgL_argszd2noescapezd2)), BUNSPEC);
																	((((BgL_funz00_bglt)
																				COBJECT(((BgL_funz00_bglt)
																						BgL_new1376z00_3882)))->
																			BgL_argszd2retescapezd2) =
																		((obj_t) (((BgL_funz00_bglt)
																					COBJECT(((BgL_funz00_bglt)
																							BgL_oldzd2sfunzd2_3863)))->
																				BgL_argszd2retescapezd2)), BUNSPEC);
																	((((BgL_sfunz00_bglt)
																				COBJECT(BgL_new1376z00_3882))->
																			BgL_propertyz00) =
																		((obj_t) (((BgL_sfunz00_bglt)
																					COBJECT(((BgL_sfunz00_bglt) (
																								(BgL_funz00_bglt)
																								BgL_oldzd2sfunzd2_3863))))->
																				BgL_propertyz00)), BUNSPEC);
																	((((BgL_sfunz00_bglt)
																				COBJECT(BgL_new1376z00_3882))->
																			BgL_argsz00) =
																		((obj_t) BgL_newzd2argszd2_3865), BUNSPEC);
																	((((BgL_sfunz00_bglt)
																				COBJECT(BgL_new1376z00_3882))->
																			BgL_argszd2namezd2) =
																		((obj_t) (((BgL_sfunz00_bglt)
																					COBJECT(((BgL_sfunz00_bglt) (
																								(BgL_funz00_bglt)
																								BgL_oldzd2sfunzd2_3863))))->
																				BgL_argszd2namezd2)), BUNSPEC);
																	((((BgL_sfunz00_bglt)
																				COBJECT(BgL_new1376z00_3882))->
																			BgL_bodyz00) =
																		((obj_t) ((obj_t) BgL_newzd2bodyzd2_3877)),
																		BUNSPEC);
																	((((BgL_sfunz00_bglt)
																				COBJECT(BgL_new1376z00_3882))->
																			BgL_classz00) =
																		((obj_t) (((BgL_sfunz00_bglt)
																					COBJECT(((BgL_sfunz00_bglt) (
																								(BgL_funz00_bglt)
																								BgL_oldzd2sfunzd2_3863))))->
																				BgL_classz00)), BUNSPEC);
																	((((BgL_sfunz00_bglt)
																				COBJECT(BgL_new1376z00_3882))->
																			BgL_dssslzd2keywordszd2) =
																		((obj_t) (((BgL_sfunz00_bglt)
																					COBJECT(((BgL_sfunz00_bglt) (
																								(BgL_funz00_bglt)
																								BgL_oldzd2sfunzd2_3863))))->
																				BgL_dssslzd2keywordszd2)), BUNSPEC);
																	((((BgL_sfunz00_bglt)
																				COBJECT(BgL_new1376z00_3882))->
																			BgL_locz00) =
																		((obj_t) (((BgL_sfunz00_bglt)
																					COBJECT(((BgL_sfunz00_bglt) (
																								(BgL_funz00_bglt)
																								BgL_oldzd2sfunzd2_3863))))->
																				BgL_locz00)), BUNSPEC);
																	((((BgL_sfunz00_bglt)
																				COBJECT(BgL_new1376z00_3882))->
																			BgL_optionalsz00) =
																		((obj_t) (((BgL_sfunz00_bglt)
																					COBJECT(((BgL_sfunz00_bglt) (
																								(BgL_funz00_bglt)
																								BgL_oldzd2sfunzd2_3863))))->
																				BgL_optionalsz00)), BUNSPEC);
																	((((BgL_sfunz00_bglt)
																				COBJECT(BgL_new1376z00_3882))->
																			BgL_keysz00) =
																		((obj_t) (((BgL_sfunz00_bglt)
																					COBJECT(((BgL_sfunz00_bglt) (
																								(BgL_funz00_bglt)
																								BgL_oldzd2sfunzd2_3863))))->
																				BgL_keysz00)), BUNSPEC);
																	((((BgL_sfunz00_bglt)
																				COBJECT(BgL_new1376z00_3882))->
																			BgL_thezd2closurezd2globalz00) =
																		((obj_t) (((BgL_sfunz00_bglt)
																					COBJECT(((BgL_sfunz00_bglt) (
																								(BgL_funz00_bglt)
																								BgL_oldzd2sfunzd2_3863))))->
																				BgL_thezd2closurezd2globalz00)),
																		BUNSPEC);
																	((((BgL_sfunz00_bglt)
																				COBJECT(BgL_new1376z00_3882))->
																			BgL_strengthz00) =
																		((obj_t) (((BgL_sfunz00_bglt)
																					COBJECT(((BgL_sfunz00_bglt) (
																								(BgL_funz00_bglt)
																								BgL_oldzd2sfunzd2_3863))))->
																				BgL_strengthz00)), BUNSPEC);
																	((((BgL_sfunz00_bglt)
																				COBJECT(BgL_new1376z00_3882))->
																			BgL_stackablez00) =
																		((obj_t) (((BgL_sfunz00_bglt)
																					COBJECT(((BgL_sfunz00_bglt) (
																								(BgL_funz00_bglt)
																								BgL_oldzd2sfunzd2_3863))))->
																				BgL_stackablez00)), BUNSPEC);
																	BgL_newzd2sfunzd2_3881 = BgL_new1376z00_3882;
																}
																{	/* Ast/alphatize.scm 445 */

																	{	/* Ast/alphatize.scm 448 */
																		bool_t BgL_arg1965z00_3885;

																		BgL_arg1965z00_3885 =
																			(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						((BgL_localz00_bglt)
																							BgL_oldz00_3861))))->
																			BgL_userzf3zf3);
																		((((BgL_variablez00_bglt)
																					COBJECT(((BgL_variablez00_bglt) (
																								(BgL_localz00_bglt)
																								BgL_newz00_3862))))->
																				BgL_userzf3zf3) =
																			((bool_t) BgL_arg1965z00_3885), BUNSPEC);
																	}
																	((((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						((BgL_localz00_bglt)
																							BgL_newz00_3862))))->
																			BgL_valuez00) =
																		((BgL_valuez00_bglt) ((BgL_valuez00_bglt)
																				BgL_newzd2sfunzd2_3881)), BUNSPEC);
									}}}}}}}}
									{	/* Ast/alphatize.scm 433 */
										obj_t BgL_arg1977z00_3886;
										obj_t BgL_arg1978z00_3887;

										BgL_arg1977z00_3886 = CDR(((obj_t) BgL_ll1580z00_3859));
										BgL_arg1978z00_3887 = CDR(((obj_t) BgL_ll1581z00_3860));
										{
											obj_t BgL_ll1581z00_5222;
											obj_t BgL_ll1580z00_5221;

											BgL_ll1580z00_5221 = BgL_arg1977z00_3886;
											BgL_ll1581z00_5222 = BgL_arg1978z00_3887;
											BgL_ll1581z00_3860 = BgL_ll1581z00_5222;
											BgL_ll1580z00_3859 = BgL_ll1580z00_5221;
											goto BgL_zc3z04anonymousza31963ze3z87_3858;
										}
									}
								}
						}
						{	/* Ast/alphatize.scm 452 */
							BgL_letzd2funzd2_bglt BgL_new1390z00_3888;

							{	/* Ast/alphatize.scm 453 */
								BgL_letzd2funzd2_bglt BgL_new1395z00_3889;

								BgL_new1395z00_3889 =
									((BgL_letzd2funzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_letzd2funzd2_bgl))));
								{	/* Ast/alphatize.scm 453 */
									long BgL_arg1981z00_3890;

									BgL_arg1981z00_3890 =
										BGL_CLASS_NUM(BGl_letzd2funzd2zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1395z00_3889),
										BgL_arg1981z00_3890);
								}
								{	/* Ast/alphatize.scm 453 */
									BgL_objectz00_bglt BgL_tmpz00_5227;

									BgL_tmpz00_5227 = ((BgL_objectz00_bglt) BgL_new1395z00_3889);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5227, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1395z00_3889);
								BgL_new1390z00_3888 = BgL_new1395z00_3889;
							}
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt) BgL_new1390z00_3888)))->BgL_locz00) =
								((obj_t)
									BGl_getzd2locationzd2zzast_alphatiza7eza7(((BgL_nodez00_bglt)
											((BgL_letzd2funzd2_bglt) BgL_nodez00_3600)),
										BgL_locz00_3601)), BUNSPEC);
							((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
												BgL_new1390z00_3888)))->BgL_typez00) =
								((BgL_typez00_bglt) (((BgL_nodez00_bglt)
											COBJECT(((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt)
														BgL_nodez00_3600))))->BgL_typez00)), BUNSPEC);
							((((BgL_nodezf2effectzf2_bglt)
										COBJECT(((BgL_nodezf2effectzf2_bglt)
												BgL_new1390z00_3888)))->BgL_sidezd2effectzd2) =
								((obj_t) (((BgL_nodezf2effectzf2_bglt)
											COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
															(BgL_letzd2funzd2_bglt) BgL_nodez00_3600)))))->
										BgL_sidezd2effectzd2)), BUNSPEC);
							((((BgL_nodezf2effectzf2_bglt)
										COBJECT(((BgL_nodezf2effectzf2_bglt)
												BgL_new1390z00_3888)))->BgL_keyz00) =
								((obj_t) (((BgL_nodezf2effectzf2_bglt)
											COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
															(BgL_letzd2funzd2_bglt) BgL_nodez00_3600)))))->
										BgL_keyz00)), BUNSPEC);
							((((BgL_letzd2funzd2_bglt) COBJECT(BgL_new1390z00_3888))->
									BgL_localsz00) = ((obj_t) BgL_newzd2localszd2_3844), BUNSPEC);
							{
								BgL_nodez00_bglt BgL_auxz00_5254;

								{	/* Ast/alphatize.scm 457 */
									obj_t BgL_arg1979z00_3891;
									BgL_nodez00_bglt BgL_arg1980z00_3892;

									BgL_arg1979z00_3891 =
										BGl_getzd2locationzd2zzast_alphatiza7eza7(
										((BgL_nodez00_bglt)
											((BgL_letzd2funzd2_bglt) BgL_nodez00_3600)),
										BgL_locz00_3601);
									BgL_arg1980z00_3892 =
										(((BgL_letzd2funzd2_bglt) COBJECT(((BgL_letzd2funzd2_bglt)
													BgL_nodez00_3600)))->BgL_bodyz00);
									BgL_auxz00_5254 =
										BGl_alphatiza7eza7zzast_alphatiza7eza7
										(BgL_oldzd2localszd2_3843, BgL_newzd2localszd2_3844,
										BgL_arg1979z00_3891, BgL_arg1980z00_3892);
								}
								((((BgL_letzd2funzd2_bglt) COBJECT(BgL_new1390z00_3888))->
										BgL_bodyz00) =
									((BgL_nodez00_bglt) BgL_auxz00_5254), BUNSPEC);
							}
							return ((BgL_nodez00_bglt) BgL_new1390z00_3888);
						}
					}
				}
			}
		}

	}



/* &do-alphatize-box-set1661 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2boxzd2set1661z17zzast_alphatiza7eza7(obj_t
		BgL_envz00_3602, obj_t BgL_nodez00_3603, obj_t BgL_locz00_3604)
	{
		{	/* Ast/alphatize.scm 414 */
			{	/* Ast/alphatize.scm 415 */
				BgL_boxzd2setz12zc0_bglt BgL_new1371z00_3894;

				{	/* Ast/alphatize.scm 416 */
					BgL_boxzd2setz12zc0_bglt BgL_new1375z00_3895;

					BgL_new1375z00_3895 =
						((BgL_boxzd2setz12zc0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_boxzd2setz12zc0_bgl))));
					{	/* Ast/alphatize.scm 416 */
						long BgL_arg1962z00_3896;

						BgL_arg1962z00_3896 =
							BGL_CLASS_NUM(BGl_boxzd2setz12zc0zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1375z00_3895), BgL_arg1962z00_3896);
					}
					{	/* Ast/alphatize.scm 416 */
						BgL_objectz00_bglt BgL_tmpz00_5267;

						BgL_tmpz00_5267 = ((BgL_objectz00_bglt) BgL_new1375z00_3895);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5267, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1375z00_3895);
					BgL_new1371z00_3894 = BgL_new1375z00_3895;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1371z00_3894)))->BgL_locz00) =
					((obj_t)
						BGl_getzd2locationzd2zzast_alphatiza7eza7(((BgL_nodez00_bglt) (
									(BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3603)),
							BgL_locz00_3604)), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1371z00_3894)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt)
								COBJECT(((BgL_nodez00_bglt) ((BgL_boxzd2setz12zc0_bglt)
											BgL_nodez00_3603))))->BgL_typez00)), BUNSPEC);
				{
					BgL_varz00_bglt BgL_auxz00_5281;

					{	/* Ast/alphatize.scm 417 */
						BgL_varz00_bglt BgL_arg1960z00_3897;

						BgL_arg1960z00_3897 =
							(((BgL_boxzd2setz12zc0_bglt) COBJECT(
									((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3603)))->BgL_varz00);
						BgL_auxz00_5281 =
							((BgL_varz00_bglt)
							BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(
								((BgL_nodez00_bglt) BgL_arg1960z00_3897), BgL_locz00_3604));
					}
					((((BgL_boxzd2setz12zc0_bglt) COBJECT(BgL_new1371z00_3894))->
							BgL_varz00) = ((BgL_varz00_bglt) BgL_auxz00_5281), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_5288;

					{	/* Ast/alphatize.scm 418 */
						BgL_nodez00_bglt BgL_arg1961z00_3898;

						BgL_arg1961z00_3898 =
							(((BgL_boxzd2setz12zc0_bglt) COBJECT(
									((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3603)))->
							BgL_valuez00);
						BgL_auxz00_5288 =
							BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(BgL_arg1961z00_3898,
							BgL_locz00_3604);
					}
					((((BgL_boxzd2setz12zc0_bglt) COBJECT(BgL_new1371z00_3894))->
							BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_5288), BUNSPEC);
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(BgL_new1371z00_3894))->
						BgL_vtypez00) =
					((BgL_typez00_bglt) (((BgL_boxzd2setz12zc0_bglt)
								COBJECT(((BgL_boxzd2setz12zc0_bglt) ((BgL_nodez00_bglt) (
												(BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3603)))))->
							BgL_vtypez00)), BUNSPEC);
				return ((BgL_nodez00_bglt) BgL_new1371z00_3894);
			}
		}

	}



/* &do-alphatize-box-ref1659 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2boxzd2ref1659z17zzast_alphatiza7eza7(obj_t
		BgL_envz00_3605, obj_t BgL_nodez00_3606, obj_t BgL_locz00_3607)
	{
		{	/* Ast/alphatize.scm 406 */
			{	/* Ast/alphatize.scm 407 */
				BgL_boxzd2refzd2_bglt BgL_new1364z00_3900;

				{	/* Ast/alphatize.scm 408 */
					BgL_boxzd2refzd2_bglt BgL_new1370z00_3901;

					BgL_new1370z00_3901 =
						((BgL_boxzd2refzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_boxzd2refzd2_bgl))));
					{	/* Ast/alphatize.scm 408 */
						long BgL_arg1959z00_3902;

						BgL_arg1959z00_3902 = BGL_CLASS_NUM(BGl_boxzd2refzd2zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1370z00_3901), BgL_arg1959z00_3902);
					}
					{	/* Ast/alphatize.scm 408 */
						BgL_objectz00_bglt BgL_tmpz00_5303;

						BgL_tmpz00_5303 = ((BgL_objectz00_bglt) BgL_new1370z00_3901);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5303, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1370z00_3901);
					BgL_new1364z00_3900 = BgL_new1370z00_3901;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1364z00_3900)))->BgL_locz00) =
					((obj_t)
						BGl_getzd2locationzd2zzast_alphatiza7eza7(((BgL_nodez00_bglt) (
									(BgL_boxzd2refzd2_bglt) BgL_nodez00_3606)), BgL_locz00_3607)),
					BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1364z00_3900)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt)
								COBJECT(((BgL_nodez00_bglt) ((BgL_boxzd2refzd2_bglt)
											BgL_nodez00_3606))))->BgL_typez00)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1364z00_3900)))->BgL_sidezd2effectzd2) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_boxzd2refzd2_bglt) BgL_nodez00_3606)))))->
							BgL_sidezd2effectzd2)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1364z00_3900)))->BgL_keyz00) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_boxzd2refzd2_bglt) BgL_nodez00_3606)))))->
							BgL_keyz00)), BUNSPEC);
				{
					BgL_varz00_bglt BgL_auxz00_5329;

					{	/* Ast/alphatize.scm 409 */
						BgL_varz00_bglt BgL_arg1958z00_3903;

						BgL_arg1958z00_3903 =
							(((BgL_boxzd2refzd2_bglt) COBJECT(
									((BgL_boxzd2refzd2_bglt) BgL_nodez00_3606)))->BgL_varz00);
						BgL_auxz00_5329 =
							((BgL_varz00_bglt)
							BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(
								((BgL_nodez00_bglt) BgL_arg1958z00_3903), BgL_locz00_3607));
					}
					((((BgL_boxzd2refzd2_bglt) COBJECT(BgL_new1364z00_3900))->
							BgL_varz00) = ((BgL_varz00_bglt) BgL_auxz00_5329), BUNSPEC);
				}
				((((BgL_boxzd2refzd2_bglt) COBJECT(BgL_new1364z00_3900))->
						BgL_vtypez00) =
					((BgL_typez00_bglt) (((BgL_boxzd2refzd2_bglt)
								COBJECT(((BgL_boxzd2refzd2_bglt) ((BgL_nodez00_bglt) (
												(BgL_boxzd2refzd2_bglt) BgL_nodez00_3606)))))->
							BgL_vtypez00)), BUNSPEC);
				return ((BgL_nodez00_bglt) BgL_new1364z00_3900);
			}
		}

	}



/* &do-alphatize-make-bo1657 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2makezd2bo1657z17zzast_alphatiza7eza7(obj_t
		BgL_envz00_3608, obj_t BgL_nodez00_3609, obj_t BgL_locz00_3610)
	{
		{	/* Ast/alphatize.scm 398 */
			{	/* Ast/alphatize.scm 399 */
				BgL_makezd2boxzd2_bglt BgL_new1356z00_3905;

				{	/* Ast/alphatize.scm 400 */
					BgL_makezd2boxzd2_bglt BgL_new1363z00_3906;

					BgL_new1363z00_3906 =
						((BgL_makezd2boxzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_makezd2boxzd2_bgl))));
					{	/* Ast/alphatize.scm 400 */
						long BgL_arg1957z00_3907;

						BgL_arg1957z00_3907 = BGL_CLASS_NUM(BGl_makezd2boxzd2zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1363z00_3906), BgL_arg1957z00_3907);
					}
					{	/* Ast/alphatize.scm 400 */
						BgL_objectz00_bglt BgL_tmpz00_5346;

						BgL_tmpz00_5346 = ((BgL_objectz00_bglt) BgL_new1363z00_3906);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5346, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1363z00_3906);
					BgL_new1356z00_3905 = BgL_new1363z00_3906;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1356z00_3905)))->BgL_locz00) =
					((obj_t)
						BGl_getzd2locationzd2zzast_alphatiza7eza7(((BgL_nodez00_bglt) (
									(BgL_makezd2boxzd2_bglt) BgL_nodez00_3609)),
							BgL_locz00_3610)), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1356z00_3905)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt)
								COBJECT(((BgL_nodez00_bglt) ((BgL_makezd2boxzd2_bglt)
											BgL_nodez00_3609))))->BgL_typez00)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1356z00_3905)))->BgL_sidezd2effectzd2) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_makezd2boxzd2_bglt) BgL_nodez00_3609)))))->
							BgL_sidezd2effectzd2)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1356z00_3905)))->BgL_keyz00) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_makezd2boxzd2_bglt) BgL_nodez00_3609)))))->
							BgL_keyz00)), BUNSPEC);
				{
					BgL_nodez00_bglt BgL_auxz00_5372;

					{	/* Ast/alphatize.scm 401 */
						BgL_nodez00_bglt BgL_arg1956z00_3908;

						BgL_arg1956z00_3908 =
							(((BgL_makezd2boxzd2_bglt) COBJECT(
									((BgL_makezd2boxzd2_bglt) BgL_nodez00_3609)))->BgL_valuez00);
						BgL_auxz00_5372 =
							BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(BgL_arg1956z00_3908,
							BgL_locz00_3610);
					}
					((((BgL_makezd2boxzd2_bglt) COBJECT(BgL_new1356z00_3905))->
							BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_5372), BUNSPEC);
				}
				((((BgL_makezd2boxzd2_bglt) COBJECT(BgL_new1356z00_3905))->
						BgL_vtypez00) =
					((BgL_typez00_bglt) (((BgL_makezd2boxzd2_bglt)
								COBJECT(((BgL_makezd2boxzd2_bglt) ((BgL_nodez00_bglt) (
												(BgL_makezd2boxzd2_bglt) BgL_nodez00_3609)))))->
							BgL_vtypez00)), BUNSPEC);
				((((BgL_makezd2boxzd2_bglt) COBJECT(BgL_new1356z00_3905))->
						BgL_stackablez00) =
					((obj_t) (((BgL_makezd2boxzd2_bglt)
								COBJECT(((BgL_makezd2boxzd2_bglt) ((BgL_nodez00_bglt) (
												(BgL_makezd2boxzd2_bglt) BgL_nodez00_3609)))))->
							BgL_stackablez00)), BUNSPEC);
				return ((BgL_nodez00_bglt) BgL_new1356z00_3905);
			}
		}

	}



/* &do-alphatize-switch1655 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2switch1655zc5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3611, obj_t BgL_nodez00_3612, obj_t BgL_locz00_3613)
	{
		{	/* Ast/alphatize.scm 386 */
			{	/* Ast/alphatize.scm 387 */
				BgL_switchz00_bglt BgL_new1349z00_3910;

				{	/* Ast/alphatize.scm 388 */
					BgL_switchz00_bglt BgL_new1355z00_3911;

					BgL_new1355z00_3911 =
						((BgL_switchz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_switchz00_bgl))));
					{	/* Ast/alphatize.scm 388 */
						long BgL_arg1955z00_3912;

						BgL_arg1955z00_3912 = BGL_CLASS_NUM(BGl_switchz00zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1355z00_3911), BgL_arg1955z00_3912);
					}
					{	/* Ast/alphatize.scm 388 */
						BgL_objectz00_bglt BgL_tmpz00_5392;

						BgL_tmpz00_5392 = ((BgL_objectz00_bglt) BgL_new1355z00_3911);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5392, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1355z00_3911);
					BgL_new1349z00_3910 = BgL_new1355z00_3911;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1349z00_3910)))->BgL_locz00) =
					((obj_t)
						BGl_getzd2locationzd2zzast_alphatiza7eza7(((BgL_nodez00_bglt) (
									(BgL_switchz00_bglt) BgL_nodez00_3612)), BgL_locz00_3613)),
					BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1349z00_3910)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt)
								COBJECT(((BgL_nodez00_bglt) ((BgL_switchz00_bglt)
											BgL_nodez00_3612))))->BgL_typez00)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1349z00_3910)))->BgL_sidezd2effectzd2) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_switchz00_bglt) BgL_nodez00_3612)))))->
							BgL_sidezd2effectzd2)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1349z00_3910)))->BgL_keyz00) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_switchz00_bglt) BgL_nodez00_3612)))))->
							BgL_keyz00)), BUNSPEC);
				{
					BgL_nodez00_bglt BgL_auxz00_5418;

					{	/* Ast/alphatize.scm 389 */
						BgL_nodez00_bglt BgL_arg1946z00_3913;

						BgL_arg1946z00_3913 =
							(((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt) BgL_nodez00_3612)))->BgL_testz00);
						BgL_auxz00_5418 =
							BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(BgL_arg1946z00_3913,
							BgL_locz00_3613);
					}
					((((BgL_switchz00_bglt) COBJECT(BgL_new1349z00_3910))->BgL_testz00) =
						((BgL_nodez00_bglt) BgL_auxz00_5418), BUNSPEC);
				}
				{
					obj_t BgL_auxz00_5423;

					{	/* Ast/alphatize.scm 390 */
						obj_t BgL_l1565z00_3914;

						BgL_l1565z00_3914 =
							(((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt) BgL_nodez00_3612)))->BgL_clausesz00);
						if (NULLP(BgL_l1565z00_3914))
							{	/* Ast/alphatize.scm 390 */
								BgL_auxz00_5423 = BNIL;
							}
						else
							{	/* Ast/alphatize.scm 390 */
								obj_t BgL_head1567z00_3915;

								BgL_head1567z00_3915 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1565z00_3917;
									obj_t BgL_tail1568z00_3918;

									BgL_l1565z00_3917 = BgL_l1565z00_3914;
									BgL_tail1568z00_3918 = BgL_head1567z00_3915;
								BgL_zc3z04anonymousza31948ze3z87_3916:
									if (NULLP(BgL_l1565z00_3917))
										{	/* Ast/alphatize.scm 390 */
											BgL_auxz00_5423 = CDR(BgL_head1567z00_3915);
										}
									else
										{	/* Ast/alphatize.scm 390 */
											obj_t BgL_newtail1569z00_3919;

											{	/* Ast/alphatize.scm 390 */
												obj_t BgL_arg1951z00_3920;

												{	/* Ast/alphatize.scm 390 */
													obj_t BgL_clausez00_3921;

													BgL_clausez00_3921 = CAR(((obj_t) BgL_l1565z00_3917));
													{	/* Ast/alphatize.scm 391 */
														obj_t BgL_arg1952z00_3922;
														BgL_nodez00_bglt BgL_arg1953z00_3923;

														BgL_arg1952z00_3922 =
															CAR(((obj_t) BgL_clausez00_3921));
														{	/* Ast/alphatize.scm 392 */
															obj_t BgL_arg1954z00_3924;

															BgL_arg1954z00_3924 =
																CDR(((obj_t) BgL_clausez00_3921));
															BgL_arg1953z00_3923 =
																BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(
																((BgL_nodez00_bglt) BgL_arg1954z00_3924),
																BgL_locz00_3613);
														}
														BgL_arg1951z00_3920 =
															MAKE_YOUNG_PAIR(BgL_arg1952z00_3922,
															((obj_t) BgL_arg1953z00_3923));
													}
												}
												BgL_newtail1569z00_3919 =
													MAKE_YOUNG_PAIR(BgL_arg1951z00_3920, BNIL);
											}
											SET_CDR(BgL_tail1568z00_3918, BgL_newtail1569z00_3919);
											{	/* Ast/alphatize.scm 390 */
												obj_t BgL_arg1950z00_3925;

												BgL_arg1950z00_3925 = CDR(((obj_t) BgL_l1565z00_3917));
												{
													obj_t BgL_tail1568z00_5447;
													obj_t BgL_l1565z00_5446;

													BgL_l1565z00_5446 = BgL_arg1950z00_3925;
													BgL_tail1568z00_5447 = BgL_newtail1569z00_3919;
													BgL_tail1568z00_3918 = BgL_tail1568z00_5447;
													BgL_l1565z00_3917 = BgL_l1565z00_5446;
													goto BgL_zc3z04anonymousza31948ze3z87_3916;
												}
											}
										}
								}
							}
					}
					((((BgL_switchz00_bglt) COBJECT(BgL_new1349z00_3910))->
							BgL_clausesz00) = ((obj_t) BgL_auxz00_5423), BUNSPEC);
				}
				((((BgL_switchz00_bglt) COBJECT(BgL_new1349z00_3910))->
						BgL_itemzd2typezd2) =
					((BgL_typez00_bglt) (((BgL_switchz00_bglt)
								COBJECT(((BgL_switchz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_switchz00_bglt) BgL_nodez00_3612)))))->
							BgL_itemzd2typezd2)), BUNSPEC);
				return ((BgL_nodez00_bglt) BgL_new1349z00_3910);
			}
		}

	}



/* &do-alphatize-fail1653 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2fail1653zc5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3614, obj_t BgL_nodez00_3615, obj_t BgL_locz00_3616)
	{
		{	/* Ast/alphatize.scm 376 */
			{	/* Ast/alphatize.scm 377 */
				BgL_failz00_bglt BgL_new1345z00_3927;

				{	/* Ast/alphatize.scm 378 */
					BgL_failz00_bglt BgL_new1348z00_3928;

					BgL_new1348z00_3928 =
						((BgL_failz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_failz00_bgl))));
					{	/* Ast/alphatize.scm 378 */
						long BgL_arg1945z00_3929;

						BgL_arg1945z00_3929 = BGL_CLASS_NUM(BGl_failz00zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1348z00_3928), BgL_arg1945z00_3929);
					}
					{	/* Ast/alphatize.scm 378 */
						BgL_objectz00_bglt BgL_tmpz00_5459;

						BgL_tmpz00_5459 = ((BgL_objectz00_bglt) BgL_new1348z00_3928);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5459, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1348z00_3928);
					BgL_new1345z00_3927 = BgL_new1348z00_3928;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1345z00_3927)))->BgL_locz00) =
					((obj_t)
						BGl_getzd2locationzd2zzast_alphatiza7eza7(((BgL_nodez00_bglt) (
									(BgL_failz00_bglt) BgL_nodez00_3615)), BgL_locz00_3616)),
					BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1345z00_3927)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt)
								COBJECT(((BgL_nodez00_bglt) ((BgL_failz00_bglt)
											BgL_nodez00_3615))))->BgL_typez00)), BUNSPEC);
				{
					BgL_nodez00_bglt BgL_auxz00_5473;

					{	/* Ast/alphatize.scm 379 */
						BgL_nodez00_bglt BgL_arg1942z00_3930;

						BgL_arg1942z00_3930 =
							(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nodez00_3615)))->BgL_procz00);
						BgL_auxz00_5473 =
							BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(BgL_arg1942z00_3930,
							BgL_locz00_3616);
					}
					((((BgL_failz00_bglt) COBJECT(BgL_new1345z00_3927))->BgL_procz00) =
						((BgL_nodez00_bglt) BgL_auxz00_5473), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_5478;

					{	/* Ast/alphatize.scm 380 */
						BgL_nodez00_bglt BgL_arg1943z00_3931;

						BgL_arg1943z00_3931 =
							(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nodez00_3615)))->BgL_msgz00);
						BgL_auxz00_5478 =
							BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(BgL_arg1943z00_3931,
							BgL_locz00_3616);
					}
					((((BgL_failz00_bglt) COBJECT(BgL_new1345z00_3927))->BgL_msgz00) =
						((BgL_nodez00_bglt) BgL_auxz00_5478), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_5483;

					{	/* Ast/alphatize.scm 381 */
						BgL_nodez00_bglt BgL_arg1944z00_3932;

						BgL_arg1944z00_3932 =
							(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nodez00_3615)))->BgL_objz00);
						BgL_auxz00_5483 =
							BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(BgL_arg1944z00_3932,
							BgL_locz00_3616);
					}
					((((BgL_failz00_bglt) COBJECT(BgL_new1345z00_3927))->BgL_objz00) =
						((BgL_nodez00_bglt) BgL_auxz00_5483), BUNSPEC);
				}
				return ((BgL_nodez00_bglt) BgL_new1345z00_3927);
			}
		}

	}



/* &do-alphatize-conditi1651 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2conditi1651zc5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3617, obj_t BgL_nodez00_3618, obj_t BgL_locz00_3619)
	{
		{	/* Ast/alphatize.scm 366 */
			{	/* Ast/alphatize.scm 367 */
				BgL_conditionalz00_bglt BgL_new1338z00_3934;

				{	/* Ast/alphatize.scm 368 */
					BgL_conditionalz00_bglt BgL_new1343z00_3935;

					BgL_new1343z00_3935 =
						((BgL_conditionalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_conditionalz00_bgl))));
					{	/* Ast/alphatize.scm 368 */
						long BgL_arg1941z00_3936;

						BgL_arg1941z00_3936 =
							BGL_CLASS_NUM(BGl_conditionalz00zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1343z00_3935), BgL_arg1941z00_3936);
					}
					{	/* Ast/alphatize.scm 368 */
						BgL_objectz00_bglt BgL_tmpz00_5493;

						BgL_tmpz00_5493 = ((BgL_objectz00_bglt) BgL_new1343z00_3935);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5493, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1343z00_3935);
					BgL_new1338z00_3934 = BgL_new1343z00_3935;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1338z00_3934)))->BgL_locz00) =
					((obj_t)
						BGl_getzd2locationzd2zzast_alphatiza7eza7(((BgL_nodez00_bglt) (
									(BgL_conditionalz00_bglt) BgL_nodez00_3618)),
							BgL_locz00_3619)), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1338z00_3934)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt)
								COBJECT(((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt)
											BgL_nodez00_3618))))->BgL_typez00)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1338z00_3934)))->BgL_sidezd2effectzd2) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_conditionalz00_bglt) BgL_nodez00_3618)))))->
							BgL_sidezd2effectzd2)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1338z00_3934)))->BgL_keyz00) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_conditionalz00_bglt) BgL_nodez00_3618)))))->
							BgL_keyz00)), BUNSPEC);
				{
					BgL_nodez00_bglt BgL_auxz00_5519;

					{	/* Ast/alphatize.scm 369 */
						BgL_nodez00_bglt BgL_arg1938z00_3937;

						BgL_arg1938z00_3937 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_3618)))->BgL_testz00);
						BgL_auxz00_5519 =
							BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(BgL_arg1938z00_3937,
							BgL_locz00_3619);
					}
					((((BgL_conditionalz00_bglt) COBJECT(BgL_new1338z00_3934))->
							BgL_testz00) = ((BgL_nodez00_bglt) BgL_auxz00_5519), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_5524;

					{	/* Ast/alphatize.scm 370 */
						BgL_nodez00_bglt BgL_arg1939z00_3938;

						BgL_arg1939z00_3938 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_3618)))->BgL_truez00);
						BgL_auxz00_5524 =
							BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(BgL_arg1939z00_3938,
							BgL_locz00_3619);
					}
					((((BgL_conditionalz00_bglt) COBJECT(BgL_new1338z00_3934))->
							BgL_truez00) = ((BgL_nodez00_bglt) BgL_auxz00_5524), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_5529;

					{	/* Ast/alphatize.scm 371 */
						BgL_nodez00_bglt BgL_arg1940z00_3939;

						BgL_arg1940z00_3939 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_3618)))->BgL_falsez00);
						BgL_auxz00_5529 =
							BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(BgL_arg1940z00_3939,
							BgL_locz00_3619);
					}
					((((BgL_conditionalz00_bglt) COBJECT(BgL_new1338z00_3934))->
							BgL_falsez00) = ((BgL_nodez00_bglt) BgL_auxz00_5529), BUNSPEC);
				}
				return ((BgL_nodez00_bglt) BgL_new1338z00_3934);
			}
		}

	}



/* &do-alphatize-setq1649 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2setq1649zc5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3620, obj_t BgL_nodez00_3621, obj_t BgL_locz00_3622)
	{
		{	/* Ast/alphatize.scm 339 */
			{	/* Ast/alphatize.scm 340 */
				BgL_varz00_bglt BgL_vz00_3941;

				BgL_vz00_3941 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_3621)))->BgL_varz00);
				{	/* Ast/alphatize.scm 340 */
					BgL_variablez00_bglt BgL_varz00_3942;

					BgL_varz00_3942 =
						(((BgL_varz00_bglt) COBJECT(BgL_vz00_3941))->BgL_variablez00);
					{	/* Ast/alphatize.scm 341 */
						obj_t BgL_alphaz00_3943;

						BgL_alphaz00_3943 =
							(((BgL_variablez00_bglt) COBJECT(BgL_varz00_3942))->
							BgL_fastzd2alphazd2);
						{	/* Ast/alphatize.scm 342 */

							if ((BgL_alphaz00_3943 == BUNSPEC))
								{	/* Ast/alphatize.scm 344 */
									{	/* Ast/alphatize.scm 345 */
										obj_t BgL_arg1928z00_3944;

										BgL_arg1928z00_3944 =
											(((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt)
														((BgL_setqz00_bglt) BgL_nodez00_3621))))->
											BgL_locz00);
										BGl_usezd2variablez12zc0zzast_sexpz00(BgL_varz00_3942,
											BgL_arg1928z00_3944, CNST_TABLE_REF(5));
									}
									{	/* Ast/alphatize.scm 346 */
										BgL_setqz00_bglt BgL_new1321z00_3945;

										{	/* Ast/alphatize.scm 347 */
											BgL_setqz00_bglt BgL_new1324z00_3946;

											BgL_new1324z00_3946 =
												((BgL_setqz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_setqz00_bgl))));
											{	/* Ast/alphatize.scm 347 */
												long BgL_arg1931z00_3947;

												BgL_arg1931z00_3947 =
													BGL_CLASS_NUM(BGl_setqz00zzast_nodez00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1324z00_3946),
													BgL_arg1931z00_3947);
											}
											{	/* Ast/alphatize.scm 347 */
												BgL_objectz00_bglt BgL_tmpz00_5550;

												BgL_tmpz00_5550 =
													((BgL_objectz00_bglt) BgL_new1324z00_3946);
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5550, BFALSE);
											}
											((BgL_objectz00_bglt) BgL_new1324z00_3946);
											BgL_new1321z00_3945 = BgL_new1324z00_3946;
										}
										((((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt) BgL_new1321z00_3945)))->
												BgL_locz00) =
											((obj_t)
												BGl_getzd2locationzd2zzast_alphatiza7eza7((
														(BgL_nodez00_bglt) ((BgL_setqz00_bglt)
															BgL_nodez00_3621)), BgL_locz00_3622)), BUNSPEC);
										((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
															BgL_new1321z00_3945)))->BgL_typez00) =
											((BgL_typez00_bglt) (((BgL_nodez00_bglt)
														COBJECT(((BgL_nodez00_bglt) ((BgL_setqz00_bglt)
																	BgL_nodez00_3621))))->BgL_typez00)), BUNSPEC);
										{
											BgL_varz00_bglt BgL_auxz00_5564;

											{	/* Ast/alphatize.scm 348 */
												BgL_refz00_bglt BgL_new1325z00_3948;

												{	/* Ast/alphatize.scm 348 */
													BgL_refz00_bglt BgL_new1329z00_3949;

													BgL_new1329z00_3949 =
														((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_refz00_bgl))));
													{	/* Ast/alphatize.scm 348 */
														long BgL_arg1929z00_3950;

														{	/* Ast/alphatize.scm 348 */
															obj_t BgL_classz00_3951;

															BgL_classz00_3951 = BGl_refz00zzast_nodez00;
															BgL_arg1929z00_3950 =
																BGL_CLASS_NUM(BgL_classz00_3951);
														}
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1329z00_3949),
															BgL_arg1929z00_3950);
													}
													{	/* Ast/alphatize.scm 348 */
														BgL_objectz00_bglt BgL_tmpz00_5569;

														BgL_tmpz00_5569 =
															((BgL_objectz00_bglt) BgL_new1329z00_3949);
														BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5569, BFALSE);
													}
													((BgL_objectz00_bglt) BgL_new1329z00_3949);
													BgL_new1325z00_3948 = BgL_new1329z00_3949;
												}
												((((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt) BgL_new1325z00_3948)))->
														BgL_locz00) =
													((obj_t)
														BGl_getzd2locationzd2zzast_alphatiza7eza7((
																(BgL_nodez00_bglt) ((BgL_setqz00_bglt)
																	BgL_nodez00_3621)), BgL_locz00_3622)),
													BUNSPEC);
												((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																	BgL_new1325z00_3948)))->BgL_typez00) =
													((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																COBJECT(((BgL_nodez00_bglt) BgL_vz00_3941)))->
															BgL_typez00)), BUNSPEC);
												((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																	BgL_new1325z00_3948)))->BgL_variablez00) =
													((BgL_variablez00_bglt) (((BgL_varz00_bglt)
																COBJECT(((BgL_varz00_bglt) ((BgL_nodez00_bglt)
																			BgL_vz00_3941))))->BgL_variablez00)),
													BUNSPEC);
												BgL_auxz00_5564 =
													((BgL_varz00_bglt) BgL_new1325z00_3948);
											}
											((((BgL_setqz00_bglt) COBJECT(BgL_new1321z00_3945))->
													BgL_varz00) =
												((BgL_varz00_bglt) BgL_auxz00_5564), BUNSPEC);
										}
										{
											BgL_nodez00_bglt BgL_auxz00_5589;

											{	/* Ast/alphatize.scm 349 */
												BgL_nodez00_bglt BgL_arg1930z00_3952;

												BgL_arg1930z00_3952 =
													(((BgL_setqz00_bglt) COBJECT(
															((BgL_setqz00_bglt) BgL_nodez00_3621)))->
													BgL_valuez00);
												BgL_auxz00_5589 =
													BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7
													(BgL_arg1930z00_3952, BgL_locz00_3622);
											}
											((((BgL_setqz00_bglt) COBJECT(BgL_new1321z00_3945))->
													BgL_valuez00) =
												((BgL_nodez00_bglt) BgL_auxz00_5589), BUNSPEC);
										}
										return ((BgL_nodez00_bglt) BgL_new1321z00_3945);
									}
								}
							else
								{	/* Ast/alphatize.scm 350 */
									bool_t BgL_test2283z00_5595;

									{	/* Ast/alphatize.scm 350 */
										obj_t BgL_classz00_3953;

										BgL_classz00_3953 = BGl_variablez00zzast_varz00;
										if (BGL_OBJECTP(BgL_alphaz00_3943))
											{	/* Ast/alphatize.scm 350 */
												BgL_objectz00_bglt BgL_arg1807z00_3954;

												BgL_arg1807z00_3954 =
													(BgL_objectz00_bglt) (BgL_alphaz00_3943);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Ast/alphatize.scm 350 */
														long BgL_idxz00_3955;

														BgL_idxz00_3955 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3954);
														BgL_test2283z00_5595 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3955 + 1L)) == BgL_classz00_3953);
													}
												else
													{	/* Ast/alphatize.scm 350 */
														bool_t BgL_res2135z00_3958;

														{	/* Ast/alphatize.scm 350 */
															obj_t BgL_oclassz00_3959;

															{	/* Ast/alphatize.scm 350 */
																obj_t BgL_arg1815z00_3960;
																long BgL_arg1816z00_3961;

																BgL_arg1815z00_3960 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Ast/alphatize.scm 350 */
																	long BgL_arg1817z00_3962;

																	BgL_arg1817z00_3962 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3954);
																	BgL_arg1816z00_3961 =
																		(BgL_arg1817z00_3962 - OBJECT_TYPE);
																}
																BgL_oclassz00_3959 =
																	VECTOR_REF(BgL_arg1815z00_3960,
																	BgL_arg1816z00_3961);
															}
															{	/* Ast/alphatize.scm 350 */
																bool_t BgL__ortest_1115z00_3963;

																BgL__ortest_1115z00_3963 =
																	(BgL_classz00_3953 == BgL_oclassz00_3959);
																if (BgL__ortest_1115z00_3963)
																	{	/* Ast/alphatize.scm 350 */
																		BgL_res2135z00_3958 =
																			BgL__ortest_1115z00_3963;
																	}
																else
																	{	/* Ast/alphatize.scm 350 */
																		long BgL_odepthz00_3964;

																		{	/* Ast/alphatize.scm 350 */
																			obj_t BgL_arg1804z00_3965;

																			BgL_arg1804z00_3965 =
																				(BgL_oclassz00_3959);
																			BgL_odepthz00_3964 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3965);
																		}
																		if ((1L < BgL_odepthz00_3964))
																			{	/* Ast/alphatize.scm 350 */
																				obj_t BgL_arg1802z00_3966;

																				{	/* Ast/alphatize.scm 350 */
																					obj_t BgL_arg1803z00_3967;

																					BgL_arg1803z00_3967 =
																						(BgL_oclassz00_3959);
																					BgL_arg1802z00_3966 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3967, 1L);
																				}
																				BgL_res2135z00_3958 =
																					(BgL_arg1802z00_3966 ==
																					BgL_classz00_3953);
																			}
																		else
																			{	/* Ast/alphatize.scm 350 */
																				BgL_res2135z00_3958 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2283z00_5595 = BgL_res2135z00_3958;
													}
											}
										else
											{	/* Ast/alphatize.scm 350 */
												BgL_test2283z00_5595 = ((bool_t) 0);
											}
									}
									if (BgL_test2283z00_5595)
										{	/* Ast/alphatize.scm 350 */
											{	/* Ast/alphatize.scm 351 */
												obj_t BgL_arg1933z00_3968;

												BgL_arg1933z00_3968 =
													(((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt)
																((BgL_setqz00_bglt) BgL_nodez00_3621))))->
													BgL_locz00);
												BGl_usezd2variablez12zc0zzast_sexpz00((
														(BgL_variablez00_bglt) BgL_alphaz00_3943),
													BgL_arg1933z00_3968, CNST_TABLE_REF(5));
											}
											{	/* Ast/alphatize.scm 352 */
												BgL_setqz00_bglt BgL_new1330z00_3969;

												{	/* Ast/alphatize.scm 353 */
													BgL_setqz00_bglt BgL_new1333z00_3970;

													BgL_new1333z00_3970 =
														((BgL_setqz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_setqz00_bgl))));
													{	/* Ast/alphatize.scm 353 */
														long BgL_arg1936z00_3971;

														BgL_arg1936z00_3971 =
															BGL_CLASS_NUM(BGl_setqz00zzast_nodez00);
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1333z00_3970),
															BgL_arg1936z00_3971);
													}
													{	/* Ast/alphatize.scm 353 */
														BgL_objectz00_bglt BgL_tmpz00_5628;

														BgL_tmpz00_5628 =
															((BgL_objectz00_bglt) BgL_new1333z00_3970);
														BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5628, BFALSE);
													}
													((BgL_objectz00_bglt) BgL_new1333z00_3970);
													BgL_new1330z00_3969 = BgL_new1333z00_3970;
												}
												((((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt) BgL_new1330z00_3969)))->
														BgL_locz00) =
													((obj_t)
														BGl_getzd2locationzd2zzast_alphatiza7eza7((
																(BgL_nodez00_bglt) ((BgL_setqz00_bglt)
																	BgL_nodez00_3621)), BgL_locz00_3622)),
													BUNSPEC);
												((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																	BgL_new1330z00_3969)))->BgL_typez00) =
													((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																COBJECT(((BgL_nodez00_bglt) ((BgL_setqz00_bglt)
																			BgL_nodez00_3621))))->BgL_typez00)),
													BUNSPEC);
												{
													BgL_varz00_bglt BgL_auxz00_5642;

													{	/* Ast/alphatize.scm 354 */
														BgL_refz00_bglt BgL_new1334z00_3972;

														{	/* Ast/alphatize.scm 355 */
															BgL_refz00_bglt BgL_new1337z00_3973;

															BgL_new1337z00_3973 =
																((BgL_refz00_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_refz00_bgl))));
															{	/* Ast/alphatize.scm 355 */
																long BgL_arg1934z00_3974;

																{	/* Ast/alphatize.scm 355 */
																	obj_t BgL_classz00_3975;

																	BgL_classz00_3975 = BGl_refz00zzast_nodez00;
																	BgL_arg1934z00_3974 =
																		BGL_CLASS_NUM(BgL_classz00_3975);
																}
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1337z00_3973),
																	BgL_arg1934z00_3974);
															}
															{	/* Ast/alphatize.scm 355 */
																BgL_objectz00_bglt BgL_tmpz00_5647;

																BgL_tmpz00_5647 =
																	((BgL_objectz00_bglt) BgL_new1337z00_3973);
																BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5647,
																	BFALSE);
															}
															((BgL_objectz00_bglt) BgL_new1337z00_3973);
															BgL_new1334z00_3972 = BgL_new1337z00_3973;
														}
														((((BgL_nodez00_bglt) COBJECT(
																		((BgL_nodez00_bglt) BgL_new1334z00_3972)))->
																BgL_locz00) =
															((obj_t)
																BGl_getzd2locationzd2zzast_alphatiza7eza7((
																		(BgL_nodez00_bglt) ((BgL_setqz00_bglt)
																			BgL_nodez00_3621)), BgL_locz00_3622)),
															BUNSPEC);
														((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																			BgL_new1334z00_3972)))->BgL_typez00) =
															((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																		COBJECT(((BgL_nodez00_bglt)
																				BgL_vz00_3941)))->BgL_typez00)),
															BUNSPEC);
														((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																			BgL_new1334z00_3972)))->BgL_variablez00) =
															((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
																	BgL_alphaz00_3943)), BUNSPEC);
														BgL_auxz00_5642 =
															((BgL_varz00_bglt) BgL_new1334z00_3972);
													}
													((((BgL_setqz00_bglt) COBJECT(BgL_new1330z00_3969))->
															BgL_varz00) =
														((BgL_varz00_bglt) BgL_auxz00_5642), BUNSPEC);
												}
												{
													BgL_nodez00_bglt BgL_auxz00_5665;

													{	/* Ast/alphatize.scm 357 */
														BgL_nodez00_bglt BgL_arg1935z00_3976;

														BgL_arg1935z00_3976 =
															(((BgL_setqz00_bglt) COBJECT(
																	((BgL_setqz00_bglt) BgL_nodez00_3621)))->
															BgL_valuez00);
														BgL_auxz00_5665 =
															BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7
															(BgL_arg1935z00_3976, BgL_locz00_3622);
													}
													((((BgL_setqz00_bglt) COBJECT(BgL_new1330z00_3969))->
															BgL_valuez00) =
														((BgL_nodez00_bglt) BgL_auxz00_5665), BUNSPEC);
												}
												return ((BgL_nodez00_bglt) BgL_new1330z00_3969);
											}
										}
									else
										{	/* Ast/alphatize.scm 361 */
											obj_t BgL_arg1937z00_3977;

											BgL_arg1937z00_3977 =
												BGl_shapez00zztools_shapez00(
												((obj_t) ((BgL_setqz00_bglt) BgL_nodez00_3621)));
											return
												((BgL_nodez00_bglt)
												BGl_internalzd2errorzd2zztools_errorz00
												(BGl_string2189z00zzast_alphatiza7eza7,
													BGl_string2190z00zzast_alphatiza7eza7,
													BgL_arg1937z00_3977));
										}
								}
						}
					}
				}
			}
		}

	}



/* &do-alphatize-cast1647 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2cast1647zc5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3623, obj_t BgL_nodez00_3624, obj_t BgL_locz00_3625)
	{
		{	/* Ast/alphatize.scm 331 */
			{	/* Ast/alphatize.scm 332 */
				BgL_castz00_bglt BgL_new1317z00_3979;

				{	/* Ast/alphatize.scm 333 */
					BgL_castz00_bglt BgL_new1320z00_3980;

					BgL_new1320z00_3980 =
						((BgL_castz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_castz00_bgl))));
					{	/* Ast/alphatize.scm 333 */
						long BgL_arg1927z00_3981;

						BgL_arg1927z00_3981 = BGL_CLASS_NUM(BGl_castz00zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1320z00_3980), BgL_arg1927z00_3981);
					}
					{	/* Ast/alphatize.scm 333 */
						BgL_objectz00_bglt BgL_tmpz00_5680;

						BgL_tmpz00_5680 = ((BgL_objectz00_bglt) BgL_new1320z00_3980);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5680, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1320z00_3980);
					BgL_new1317z00_3979 = BgL_new1320z00_3980;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1317z00_3979)))->BgL_locz00) =
					((obj_t)
						BGl_getzd2locationzd2zzast_alphatiza7eza7(((BgL_nodez00_bglt) (
									(BgL_castz00_bglt) BgL_nodez00_3624)), BgL_locz00_3625)),
					BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1317z00_3979)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt)
								COBJECT(((BgL_nodez00_bglt) ((BgL_castz00_bglt)
											BgL_nodez00_3624))))->BgL_typez00)), BUNSPEC);
				{
					BgL_nodez00_bglt BgL_auxz00_5694;

					{	/* Ast/alphatize.scm 334 */
						BgL_nodez00_bglt BgL_arg1926z00_3982;

						BgL_arg1926z00_3982 =
							(((BgL_castz00_bglt) COBJECT(
									((BgL_castz00_bglt) BgL_nodez00_3624)))->BgL_argz00);
						BgL_auxz00_5694 =
							BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(BgL_arg1926z00_3982,
							BgL_locz00_3625);
					}
					((((BgL_castz00_bglt) COBJECT(BgL_new1317z00_3979))->BgL_argz00) =
						((BgL_nodez00_bglt) BgL_auxz00_5694), BUNSPEC);
				}
				return ((BgL_nodez00_bglt) BgL_new1317z00_3979);
			}
		}

	}



/* &do-alphatize-instanc1645 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2instanc1645zc5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3626, obj_t BgL_nodez00_3627, obj_t BgL_locz00_3628)
	{
		{	/* Ast/alphatize.scm 323 */
			{	/* Ast/alphatize.scm 324 */
				BgL_instanceofz00_bglt BgL_new1308z00_3984;

				{	/* Ast/alphatize.scm 326 */
					BgL_instanceofz00_bglt BgL_new1316z00_3985;

					BgL_new1316z00_3985 =
						((BgL_instanceofz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_instanceofz00_bgl))));
					{	/* Ast/alphatize.scm 326 */
						long BgL_arg1925z00_3986;

						BgL_arg1925z00_3986 = BGL_CLASS_NUM(BGl_instanceofz00zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1316z00_3985), BgL_arg1925z00_3986);
					}
					{	/* Ast/alphatize.scm 326 */
						BgL_objectz00_bglt BgL_tmpz00_5704;

						BgL_tmpz00_5704 = ((BgL_objectz00_bglt) BgL_new1316z00_3985);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5704, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1316z00_3985);
					BgL_new1308z00_3984 = BgL_new1316z00_3985;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1308z00_3984)))->BgL_locz00) =
					((obj_t)
						BGl_getzd2locationzd2zzast_alphatiza7eza7(((BgL_nodez00_bglt) (
									(BgL_instanceofz00_bglt) BgL_nodez00_3627)),
							BgL_locz00_3628)), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1308z00_3984)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt)
								COBJECT(((BgL_nodez00_bglt) ((BgL_instanceofz00_bglt)
											BgL_nodez00_3627))))->BgL_typez00)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1308z00_3984)))->BgL_sidezd2effectzd2) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_instanceofz00_bglt) BgL_nodez00_3627)))))->
							BgL_sidezd2effectzd2)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1308z00_3984)))->BgL_keyz00) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_instanceofz00_bglt) BgL_nodez00_3627)))))->
							BgL_keyz00)), BUNSPEC);
				{
					obj_t BgL_auxz00_5730;

					{	/* Ast/alphatize.scm 325 */
						BgL_nodez00_bglt BgL_arg1920z00_3987;

						{	/* Ast/alphatize.scm 325 */
							obj_t BgL_arg1923z00_3988;

							{	/* Ast/alphatize.scm 325 */
								obj_t BgL_pairz00_3989;

								BgL_pairz00_3989 =
									(((BgL_externz00_bglt) COBJECT(
											((BgL_externz00_bglt)
												((BgL_instanceofz00_bglt) BgL_nodez00_3627))))->
									BgL_exprza2za2);
								BgL_arg1923z00_3988 = CAR(BgL_pairz00_3989);
							}
							BgL_arg1920z00_3987 =
								BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(
								((BgL_nodez00_bglt) BgL_arg1923z00_3988), BgL_locz00_3628);
						}
						{	/* Ast/alphatize.scm 325 */
							obj_t BgL_list1921z00_3990;

							BgL_list1921z00_3990 =
								MAKE_YOUNG_PAIR(((obj_t) BgL_arg1920z00_3987), BNIL);
							BgL_auxz00_5730 = BgL_list1921z00_3990;
					}}
					((((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt) BgL_new1308z00_3984)))->
							BgL_exprza2za2) = ((obj_t) BgL_auxz00_5730), BUNSPEC);
				}
				((((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_new1308z00_3984)))->BgL_effectz00) =
					((obj_t) (((BgL_externz00_bglt)
								COBJECT(((BgL_externz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_instanceofz00_bglt) BgL_nodez00_3627)))))->
							BgL_effectz00)), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1308z00_3984)))->BgL_czd2formatzd2) =
					((obj_t) (((BgL_privatez00_bglt)
								COBJECT(((BgL_privatez00_bglt) ((BgL_nodez00_bglt) (
												(BgL_instanceofz00_bglt) BgL_nodez00_3627)))))->
							BgL_czd2formatzd2)), BUNSPEC);
				((((BgL_instanceofz00_bglt) COBJECT(BgL_new1308z00_3984))->
						BgL_classz00) =
					((BgL_typez00_bglt) (((BgL_instanceofz00_bglt)
								COBJECT(((BgL_instanceofz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_instanceofz00_bglt) BgL_nodez00_3627)))))->
							BgL_classz00)), BUNSPEC);
				return ((BgL_nodez00_bglt) BgL_new1308z00_3984);
			}
		}

	}



/* &do-alphatize-valloc1643 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2valloc1643zc5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3629, obj_t BgL_nodez00_3630, obj_t BgL_locz00_3631)
	{
		{	/* Ast/alphatize.scm 315 */
			{	/* Ast/alphatize.scm 316 */
				BgL_vallocz00_bglt BgL_new1298z00_3992;

				{	/* Ast/alphatize.scm 317 */
					BgL_vallocz00_bglt BgL_new1307z00_3993;

					BgL_new1307z00_3993 =
						((BgL_vallocz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_vallocz00_bgl))));
					{	/* Ast/alphatize.scm 317 */
						long BgL_arg1919z00_3994;

						BgL_arg1919z00_3994 = BGL_CLASS_NUM(BGl_vallocz00zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1307z00_3993), BgL_arg1919z00_3994);
					}
					{	/* Ast/alphatize.scm 317 */
						BgL_objectz00_bglt BgL_tmpz00_5763;

						BgL_tmpz00_5763 = ((BgL_objectz00_bglt) BgL_new1307z00_3993);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5763, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1307z00_3993);
					BgL_new1298z00_3992 = BgL_new1307z00_3993;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1298z00_3992)))->BgL_locz00) =
					((obj_t)
						BGl_getzd2locationzd2zzast_alphatiza7eza7(((BgL_nodez00_bglt) (
									(BgL_vallocz00_bglt) BgL_nodez00_3630)), BgL_locz00_3631)),
					BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1298z00_3992)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt)
								COBJECT(((BgL_nodez00_bglt) ((BgL_vallocz00_bglt)
											BgL_nodez00_3630))))->BgL_typez00)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1298z00_3992)))->BgL_sidezd2effectzd2) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_vallocz00_bglt) BgL_nodez00_3630)))))->
							BgL_sidezd2effectzd2)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1298z00_3992)))->BgL_keyz00) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_vallocz00_bglt) BgL_nodez00_3630)))))->
							BgL_keyz00)), BUNSPEC);
				{
					obj_t BgL_auxz00_5789;

					{	/* Ast/alphatize.scm 318 */
						obj_t BgL_arg1918z00_3995;

						BgL_arg1918z00_3995 =
							(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt)
										((BgL_vallocz00_bglt) BgL_nodez00_3630))))->BgL_exprza2za2);
						BgL_auxz00_5789 =
							BGl_dozd2alphatiza7eza2zd7zzast_alphatiza7eza7
							(BgL_arg1918z00_3995, BgL_locz00_3631);
					}
					((((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt) BgL_new1298z00_3992)))->
							BgL_exprza2za2) = ((obj_t) BgL_auxz00_5789), BUNSPEC);
				}
				((((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_new1298z00_3992)))->BgL_effectz00) =
					((obj_t) (((BgL_externz00_bglt)
								COBJECT(((BgL_externz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_vallocz00_bglt) BgL_nodez00_3630)))))->
							BgL_effectz00)), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1298z00_3992)))->BgL_czd2formatzd2) =
					((obj_t) (((BgL_privatez00_bglt)
								COBJECT(((BgL_privatez00_bglt) ((BgL_nodez00_bglt) (
												(BgL_vallocz00_bglt) BgL_nodez00_3630)))))->
							BgL_czd2formatzd2)), BUNSPEC);
				((((BgL_vallocz00_bglt) COBJECT(BgL_new1298z00_3992))->BgL_ftypez00) =
					((BgL_typez00_bglt) (((BgL_vallocz00_bglt)
								COBJECT(((BgL_vallocz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_vallocz00_bglt) BgL_nodez00_3630)))))->
							BgL_ftypez00)), BUNSPEC);
				((((BgL_vallocz00_bglt) COBJECT(BgL_new1298z00_3992))->BgL_otypez00) =
					((BgL_typez00_bglt) (((BgL_vallocz00_bglt)
								COBJECT(((BgL_vallocz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_vallocz00_bglt) BgL_nodez00_3630)))))->
							BgL_otypez00)), BUNSPEC);
				return ((BgL_nodez00_bglt) BgL_new1298z00_3992);
			}
		}

	}



/* &do-alphatize-vset!1641 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2vsetz121641zd7zzast_alphatiza7eza7(obj_t
		BgL_envz00_3632, obj_t BgL_nodez00_3633, obj_t BgL_locz00_3634)
	{
		{	/* Ast/alphatize.scm 307 */
			{	/* Ast/alphatize.scm 308 */
				BgL_vsetz12z12_bglt BgL_new1286z00_3997;

				{	/* Ast/alphatize.scm 309 */
					BgL_vsetz12z12_bglt BgL_new1297z00_3998;

					BgL_new1297z00_3998 =
						((BgL_vsetz12z12_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_vsetz12z12_bgl))));
					{	/* Ast/alphatize.scm 309 */
						long BgL_arg1917z00_3999;

						BgL_arg1917z00_3999 = BGL_CLASS_NUM(BGl_vsetz12z12zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1297z00_3998), BgL_arg1917z00_3999);
					}
					{	/* Ast/alphatize.scm 309 */
						BgL_objectz00_bglt BgL_tmpz00_5823;

						BgL_tmpz00_5823 = ((BgL_objectz00_bglt) BgL_new1297z00_3998);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5823, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1297z00_3998);
					BgL_new1286z00_3997 = BgL_new1297z00_3998;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1286z00_3997)))->BgL_locz00) =
					((obj_t)
						BGl_getzd2locationzd2zzast_alphatiza7eza7(((BgL_nodez00_bglt) (
									(BgL_vsetz12z12_bglt) BgL_nodez00_3633)), BgL_locz00_3634)),
					BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1286z00_3997)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt)
								COBJECT(((BgL_nodez00_bglt) ((BgL_vsetz12z12_bglt)
											BgL_nodez00_3633))))->BgL_typez00)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1286z00_3997)))->BgL_sidezd2effectzd2) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_vsetz12z12_bglt) BgL_nodez00_3633)))))->
							BgL_sidezd2effectzd2)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1286z00_3997)))->BgL_keyz00) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_vsetz12z12_bglt) BgL_nodez00_3633)))))->
							BgL_keyz00)), BUNSPEC);
				{
					obj_t BgL_auxz00_5849;

					{	/* Ast/alphatize.scm 310 */
						obj_t BgL_arg1916z00_4000;

						BgL_arg1916z00_4000 =
							(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt)
										((BgL_vsetz12z12_bglt) BgL_nodez00_3633))))->
							BgL_exprza2za2);
						BgL_auxz00_5849 =
							BGl_dozd2alphatiza7eza2zd7zzast_alphatiza7eza7
							(BgL_arg1916z00_4000, BgL_locz00_3634);
					}
					((((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt) BgL_new1286z00_3997)))->
							BgL_exprza2za2) = ((obj_t) BgL_auxz00_5849), BUNSPEC);
				}
				((((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_new1286z00_3997)))->BgL_effectz00) =
					((obj_t) (((BgL_externz00_bglt)
								COBJECT(((BgL_externz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_vsetz12z12_bglt) BgL_nodez00_3633)))))->
							BgL_effectz00)), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1286z00_3997)))->BgL_czd2formatzd2) =
					((obj_t) (((BgL_privatez00_bglt)
								COBJECT(((BgL_privatez00_bglt) ((BgL_nodez00_bglt) (
												(BgL_vsetz12z12_bglt) BgL_nodez00_3633)))))->
							BgL_czd2formatzd2)), BUNSPEC);
				((((BgL_vsetz12z12_bglt) COBJECT(BgL_new1286z00_3997))->BgL_ftypez00) =
					((BgL_typez00_bglt) (((BgL_vsetz12z12_bglt)
								COBJECT(((BgL_vsetz12z12_bglt) ((BgL_nodez00_bglt) (
												(BgL_vsetz12z12_bglt) BgL_nodez00_3633)))))->
							BgL_ftypez00)), BUNSPEC);
				((((BgL_vsetz12z12_bglt) COBJECT(BgL_new1286z00_3997))->BgL_otypez00) =
					((BgL_typez00_bglt) (((BgL_vsetz12z12_bglt)
								COBJECT(((BgL_vsetz12z12_bglt) ((BgL_nodez00_bglt) (
												(BgL_vsetz12z12_bglt) BgL_nodez00_3633)))))->
							BgL_otypez00)), BUNSPEC);
				((((BgL_vsetz12z12_bglt) COBJECT(BgL_new1286z00_3997))->BgL_vtypez00) =
					((BgL_typez00_bglt) (((BgL_vsetz12z12_bglt)
								COBJECT(((BgL_vsetz12z12_bglt) ((BgL_nodez00_bglt) (
												(BgL_vsetz12z12_bglt) BgL_nodez00_3633)))))->
							BgL_vtypez00)), BUNSPEC);
				((((BgL_vsetz12z12_bglt) COBJECT(BgL_new1286z00_3997))->BgL_unsafez00) =
					((bool_t) (((BgL_vsetz12z12_bglt)
								COBJECT(((BgL_vsetz12z12_bglt) ((BgL_nodez00_bglt) (
												(BgL_vsetz12z12_bglt) BgL_nodez00_3633)))))->
							BgL_unsafez00)), BUNSPEC);
				return ((BgL_nodez00_bglt) BgL_new1286z00_3997);
			}
		}

	}



/* &do-alphatize-vref1639 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2vref1639zc5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3635, obj_t BgL_nodez00_3636, obj_t BgL_locz00_3637)
	{
		{	/* Ast/alphatize.scm 299 */
			{	/* Ast/alphatize.scm 300 */
				BgL_vrefz00_bglt BgL_new1274z00_4002;

				{	/* Ast/alphatize.scm 301 */
					BgL_vrefz00_bglt BgL_new1285z00_4003;

					BgL_new1285z00_4003 =
						((BgL_vrefz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_vrefz00_bgl))));
					{	/* Ast/alphatize.scm 301 */
						long BgL_arg1914z00_4004;

						BgL_arg1914z00_4004 = BGL_CLASS_NUM(BGl_vrefz00zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1285z00_4003), BgL_arg1914z00_4004);
					}
					{	/* Ast/alphatize.scm 301 */
						BgL_objectz00_bglt BgL_tmpz00_5893;

						BgL_tmpz00_5893 = ((BgL_objectz00_bglt) BgL_new1285z00_4003);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5893, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1285z00_4003);
					BgL_new1274z00_4002 = BgL_new1285z00_4003;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1274z00_4002)))->BgL_locz00) =
					((obj_t)
						BGl_getzd2locationzd2zzast_alphatiza7eza7(((BgL_nodez00_bglt) (
									(BgL_vrefz00_bglt) BgL_nodez00_3636)), BgL_locz00_3637)),
					BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1274z00_4002)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt)
								COBJECT(((BgL_nodez00_bglt) ((BgL_vrefz00_bglt)
											BgL_nodez00_3636))))->BgL_typez00)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1274z00_4002)))->BgL_sidezd2effectzd2) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_vrefz00_bglt) BgL_nodez00_3636)))))->
							BgL_sidezd2effectzd2)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1274z00_4002)))->BgL_keyz00) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_vrefz00_bglt) BgL_nodez00_3636)))))->BgL_keyz00)),
					BUNSPEC);
				{
					obj_t BgL_auxz00_5919;

					{	/* Ast/alphatize.scm 302 */
						obj_t BgL_arg1913z00_4005;

						BgL_arg1913z00_4005 =
							(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt)
										((BgL_vrefz00_bglt) BgL_nodez00_3636))))->BgL_exprza2za2);
						BgL_auxz00_5919 =
							BGl_dozd2alphatiza7eza2zd7zzast_alphatiza7eza7
							(BgL_arg1913z00_4005, BgL_locz00_3637);
					}
					((((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt) BgL_new1274z00_4002)))->
							BgL_exprza2za2) = ((obj_t) BgL_auxz00_5919), BUNSPEC);
				}
				((((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_new1274z00_4002)))->BgL_effectz00) =
					((obj_t) (((BgL_externz00_bglt)
								COBJECT(((BgL_externz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_vrefz00_bglt) BgL_nodez00_3636)))))->
							BgL_effectz00)), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1274z00_4002)))->BgL_czd2formatzd2) =
					((obj_t) (((BgL_privatez00_bglt)
								COBJECT(((BgL_privatez00_bglt) ((BgL_nodez00_bglt) (
												(BgL_vrefz00_bglt) BgL_nodez00_3636)))))->
							BgL_czd2formatzd2)), BUNSPEC);
				((((BgL_vrefz00_bglt) COBJECT(BgL_new1274z00_4002))->BgL_ftypez00) =
					((BgL_typez00_bglt) (((BgL_vrefz00_bglt)
								COBJECT(((BgL_vrefz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_vrefz00_bglt) BgL_nodez00_3636)))))->
							BgL_ftypez00)), BUNSPEC);
				((((BgL_vrefz00_bglt) COBJECT(BgL_new1274z00_4002))->BgL_otypez00) =
					((BgL_typez00_bglt) (((BgL_vrefz00_bglt)
								COBJECT(((BgL_vrefz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_vrefz00_bglt) BgL_nodez00_3636)))))->
							BgL_otypez00)), BUNSPEC);
				((((BgL_vrefz00_bglt) COBJECT(BgL_new1274z00_4002))->BgL_vtypez00) =
					((BgL_typez00_bglt) (((BgL_vrefz00_bglt)
								COBJECT(((BgL_vrefz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_vrefz00_bglt) BgL_nodez00_3636)))))->
							BgL_vtypez00)), BUNSPEC);
				((((BgL_vrefz00_bglt) COBJECT(BgL_new1274z00_4002))->BgL_unsafez00) =
					((bool_t) (((BgL_vrefz00_bglt)
								COBJECT(((BgL_vrefz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_vrefz00_bglt) BgL_nodez00_3636)))))->
							BgL_unsafez00)), BUNSPEC);
				return ((BgL_nodez00_bglt) BgL_new1274z00_4002);
			}
		}

	}



/* &do-alphatize-vlength1637 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2vlength1637zc5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3638, obj_t BgL_nodez00_3639, obj_t BgL_locz00_3640)
	{
		{	/* Ast/alphatize.scm 291 */
			{	/* Ast/alphatize.scm 292 */
				BgL_vlengthz00_bglt BgL_new1264z00_4007;

				{	/* Ast/alphatize.scm 293 */
					BgL_vlengthz00_bglt BgL_new1273z00_4008;

					BgL_new1273z00_4008 =
						((BgL_vlengthz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_vlengthz00_bgl))));
					{	/* Ast/alphatize.scm 293 */
						long BgL_arg1912z00_4009;

						BgL_arg1912z00_4009 = BGL_CLASS_NUM(BGl_vlengthz00zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1273z00_4008), BgL_arg1912z00_4009);
					}
					{	/* Ast/alphatize.scm 293 */
						BgL_objectz00_bglt BgL_tmpz00_5963;

						BgL_tmpz00_5963 = ((BgL_objectz00_bglt) BgL_new1273z00_4008);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5963, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1273z00_4008);
					BgL_new1264z00_4007 = BgL_new1273z00_4008;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1264z00_4007)))->BgL_locz00) =
					((obj_t)
						BGl_getzd2locationzd2zzast_alphatiza7eza7(((BgL_nodez00_bglt) (
									(BgL_vlengthz00_bglt) BgL_nodez00_3639)), BgL_locz00_3640)),
					BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1264z00_4007)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt)
								COBJECT(((BgL_nodez00_bglt) ((BgL_vlengthz00_bglt)
											BgL_nodez00_3639))))->BgL_typez00)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1264z00_4007)))->BgL_sidezd2effectzd2) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_vlengthz00_bglt) BgL_nodez00_3639)))))->
							BgL_sidezd2effectzd2)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1264z00_4007)))->BgL_keyz00) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_vlengthz00_bglt) BgL_nodez00_3639)))))->
							BgL_keyz00)), BUNSPEC);
				{
					obj_t BgL_auxz00_5989;

					{	/* Ast/alphatize.scm 294 */
						obj_t BgL_arg1911z00_4010;

						BgL_arg1911z00_4010 =
							(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt)
										((BgL_vlengthz00_bglt) BgL_nodez00_3639))))->
							BgL_exprza2za2);
						BgL_auxz00_5989 =
							BGl_dozd2alphatiza7eza2zd7zzast_alphatiza7eza7
							(BgL_arg1911z00_4010, BgL_locz00_3640);
					}
					((((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt) BgL_new1264z00_4007)))->
							BgL_exprza2za2) = ((obj_t) BgL_auxz00_5989), BUNSPEC);
				}
				((((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_new1264z00_4007)))->BgL_effectz00) =
					((obj_t) (((BgL_externz00_bglt)
								COBJECT(((BgL_externz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_vlengthz00_bglt) BgL_nodez00_3639)))))->
							BgL_effectz00)), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1264z00_4007)))->BgL_czd2formatzd2) =
					((obj_t) (((BgL_privatez00_bglt)
								COBJECT(((BgL_privatez00_bglt) ((BgL_nodez00_bglt) (
												(BgL_vlengthz00_bglt) BgL_nodez00_3639)))))->
							BgL_czd2formatzd2)), BUNSPEC);
				((((BgL_vlengthz00_bglt) COBJECT(BgL_new1264z00_4007))->BgL_vtypez00) =
					((BgL_typez00_bglt) (((BgL_vlengthz00_bglt)
								COBJECT(((BgL_vlengthz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_vlengthz00_bglt) BgL_nodez00_3639)))))->
							BgL_vtypez00)), BUNSPEC);
				((((BgL_vlengthz00_bglt) COBJECT(BgL_new1264z00_4007))->BgL_ftypez00) =
					((obj_t) (((BgL_vlengthz00_bglt)
								COBJECT(((BgL_vlengthz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_vlengthz00_bglt) BgL_nodez00_3639)))))->
							BgL_ftypez00)), BUNSPEC);
				return ((BgL_nodez00_bglt) BgL_new1264z00_4007);
			}
		}

	}



/* &do-alphatize-new1635 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2new1635zc5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3641, obj_t BgL_nodez00_3642, obj_t BgL_locz00_3643)
	{
		{	/* Ast/alphatize.scm 283 */
			{	/* Ast/alphatize.scm 284 */
				BgL_newz00_bglt BgL_new1255z00_4012;

				{	/* Ast/alphatize.scm 285 */
					BgL_newz00_bglt BgL_new1263z00_4013;

					BgL_new1263z00_4013 =
						((BgL_newz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_newz00_bgl))));
					{	/* Ast/alphatize.scm 285 */
						long BgL_arg1910z00_4014;

						BgL_arg1910z00_4014 = BGL_CLASS_NUM(BGl_newz00zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1263z00_4013), BgL_arg1910z00_4014);
					}
					{	/* Ast/alphatize.scm 285 */
						BgL_objectz00_bglt BgL_tmpz00_6023;

						BgL_tmpz00_6023 = ((BgL_objectz00_bglt) BgL_new1263z00_4013);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6023, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1263z00_4013);
					BgL_new1255z00_4012 = BgL_new1263z00_4013;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1255z00_4012)))->BgL_locz00) =
					((obj_t)
						BGl_getzd2locationzd2zzast_alphatiza7eza7(((BgL_nodez00_bglt) (
									(BgL_newz00_bglt) BgL_nodez00_3642)), BgL_locz00_3643)),
					BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1255z00_4012)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt)
								COBJECT(((BgL_nodez00_bglt) ((BgL_newz00_bglt)
											BgL_nodez00_3642))))->BgL_typez00)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1255z00_4012)))->BgL_sidezd2effectzd2) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_newz00_bglt) BgL_nodez00_3642)))))->
							BgL_sidezd2effectzd2)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1255z00_4012)))->BgL_keyz00) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_newz00_bglt) BgL_nodez00_3642)))))->BgL_keyz00)),
					BUNSPEC);
				{
					obj_t BgL_auxz00_6049;

					{	/* Ast/alphatize.scm 286 */
						obj_t BgL_arg1906z00_4015;

						BgL_arg1906z00_4015 =
							(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt)
										((BgL_newz00_bglt) BgL_nodez00_3642))))->BgL_exprza2za2);
						BgL_auxz00_6049 =
							BGl_dozd2alphatiza7eza2zd7zzast_alphatiza7eza7
							(BgL_arg1906z00_4015, BgL_locz00_3643);
					}
					((((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt) BgL_new1255z00_4012)))->
							BgL_exprza2za2) = ((obj_t) BgL_auxz00_6049), BUNSPEC);
				}
				((((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_new1255z00_4012)))->BgL_effectz00) =
					((obj_t) (((BgL_externz00_bglt)
								COBJECT(((BgL_externz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_newz00_bglt) BgL_nodez00_3642)))))->
							BgL_effectz00)), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1255z00_4012)))->BgL_czd2formatzd2) =
					((obj_t) (((BgL_privatez00_bglt)
								COBJECT(((BgL_privatez00_bglt) ((BgL_nodez00_bglt) (
												(BgL_newz00_bglt) BgL_nodez00_3642)))))->
							BgL_czd2formatzd2)), BUNSPEC);
				((((BgL_newz00_bglt) COBJECT(BgL_new1255z00_4012))->
						BgL_argszd2typezd2) =
					((obj_t) (((BgL_newz00_bglt)
								COBJECT(((BgL_newz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_newz00_bglt) BgL_nodez00_3642)))))->
							BgL_argszd2typezd2)), BUNSPEC);
				return ((BgL_nodez00_bglt) BgL_new1255z00_4012);
			}
		}

	}



/* &do-alphatize-setfiel1633 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2setfiel1633zc5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3644, obj_t BgL_nodez00_3645, obj_t BgL_locz00_3646)
	{
		{	/* Ast/alphatize.scm 275 */
			{	/* Ast/alphatize.scm 276 */
				BgL_setfieldz00_bglt BgL_new1244z00_4017;

				{	/* Ast/alphatize.scm 277 */
					BgL_setfieldz00_bglt BgL_new1254z00_4018;

					BgL_new1254z00_4018 =
						((BgL_setfieldz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_setfieldz00_bgl))));
					{	/* Ast/alphatize.scm 277 */
						long BgL_arg1904z00_4019;

						BgL_arg1904z00_4019 = BGL_CLASS_NUM(BGl_setfieldz00zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1254z00_4018), BgL_arg1904z00_4019);
					}
					{	/* Ast/alphatize.scm 277 */
						BgL_objectz00_bglt BgL_tmpz00_6078;

						BgL_tmpz00_6078 = ((BgL_objectz00_bglt) BgL_new1254z00_4018);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6078, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1254z00_4018);
					BgL_new1244z00_4017 = BgL_new1254z00_4018;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1244z00_4017)))->BgL_locz00) =
					((obj_t)
						BGl_getzd2locationzd2zzast_alphatiza7eza7(((BgL_nodez00_bglt) (
									(BgL_setfieldz00_bglt) BgL_nodez00_3645)), BgL_locz00_3646)),
					BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1244z00_4017)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt)
								COBJECT(((BgL_nodez00_bglt) ((BgL_setfieldz00_bglt)
											BgL_nodez00_3645))))->BgL_typez00)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1244z00_4017)))->BgL_sidezd2effectzd2) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_setfieldz00_bglt) BgL_nodez00_3645)))))->
							BgL_sidezd2effectzd2)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1244z00_4017)))->BgL_keyz00) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_setfieldz00_bglt) BgL_nodez00_3645)))))->
							BgL_keyz00)), BUNSPEC);
				{
					obj_t BgL_auxz00_6104;

					{	/* Ast/alphatize.scm 278 */
						obj_t BgL_arg1903z00_4020;

						BgL_arg1903z00_4020 =
							(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt)
										((BgL_setfieldz00_bglt) BgL_nodez00_3645))))->
							BgL_exprza2za2);
						BgL_auxz00_6104 =
							BGl_dozd2alphatiza7eza2zd7zzast_alphatiza7eza7
							(BgL_arg1903z00_4020, BgL_locz00_3646);
					}
					((((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt) BgL_new1244z00_4017)))->
							BgL_exprza2za2) = ((obj_t) BgL_auxz00_6104), BUNSPEC);
				}
				((((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_new1244z00_4017)))->BgL_effectz00) =
					((obj_t) (((BgL_externz00_bglt)
								COBJECT(((BgL_externz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_setfieldz00_bglt) BgL_nodez00_3645)))))->
							BgL_effectz00)), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1244z00_4017)))->BgL_czd2formatzd2) =
					((obj_t) (((BgL_privatez00_bglt)
								COBJECT(((BgL_privatez00_bglt) ((BgL_nodez00_bglt) (
												(BgL_setfieldz00_bglt) BgL_nodez00_3645)))))->
							BgL_czd2formatzd2)), BUNSPEC);
				((((BgL_setfieldz00_bglt) COBJECT(BgL_new1244z00_4017))->BgL_fnamez00) =
					((obj_t) (((BgL_setfieldz00_bglt)
								COBJECT(((BgL_setfieldz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_setfieldz00_bglt) BgL_nodez00_3645)))))->
							BgL_fnamez00)), BUNSPEC);
				((((BgL_setfieldz00_bglt) COBJECT(BgL_new1244z00_4017))->BgL_ftypez00) =
					((BgL_typez00_bglt) (((BgL_setfieldz00_bglt)
								COBJECT(((BgL_setfieldz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_setfieldz00_bglt) BgL_nodez00_3645)))))->
							BgL_ftypez00)), BUNSPEC);
				((((BgL_setfieldz00_bglt) COBJECT(BgL_new1244z00_4017))->BgL_otypez00) =
					((BgL_typez00_bglt) (((BgL_setfieldz00_bglt)
								COBJECT(((BgL_setfieldz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_setfieldz00_bglt) BgL_nodez00_3645)))))->
							BgL_otypez00)), BUNSPEC);
				return ((BgL_nodez00_bglt) BgL_new1244z00_4017);
			}
		}

	}



/* &do-alphatize-getfiel1631 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2getfiel1631zc5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3647, obj_t BgL_nodez00_3648, obj_t BgL_locz00_3649)
	{
		{	/* Ast/alphatize.scm 267 */
			{	/* Ast/alphatize.scm 268 */
				BgL_getfieldz00_bglt BgL_new1233z00_4022;

				{	/* Ast/alphatize.scm 269 */
					BgL_getfieldz00_bglt BgL_new1243z00_4023;

					BgL_new1243z00_4023 =
						((BgL_getfieldz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_getfieldz00_bgl))));
					{	/* Ast/alphatize.scm 269 */
						long BgL_arg1902z00_4024;

						BgL_arg1902z00_4024 = BGL_CLASS_NUM(BGl_getfieldz00zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1243z00_4023), BgL_arg1902z00_4024);
					}
					{	/* Ast/alphatize.scm 269 */
						BgL_objectz00_bglt BgL_tmpz00_6143;

						BgL_tmpz00_6143 = ((BgL_objectz00_bglt) BgL_new1243z00_4023);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6143, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1243z00_4023);
					BgL_new1233z00_4022 = BgL_new1243z00_4023;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1233z00_4022)))->BgL_locz00) =
					((obj_t)
						BGl_getzd2locationzd2zzast_alphatiza7eza7(((BgL_nodez00_bglt) (
									(BgL_getfieldz00_bglt) BgL_nodez00_3648)), BgL_locz00_3649)),
					BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1233z00_4022)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt)
								COBJECT(((BgL_nodez00_bglt) ((BgL_getfieldz00_bglt)
											BgL_nodez00_3648))))->BgL_typez00)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1233z00_4022)))->BgL_sidezd2effectzd2) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_getfieldz00_bglt) BgL_nodez00_3648)))))->
							BgL_sidezd2effectzd2)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1233z00_4022)))->BgL_keyz00) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_getfieldz00_bglt) BgL_nodez00_3648)))))->
							BgL_keyz00)), BUNSPEC);
				{
					obj_t BgL_auxz00_6169;

					{	/* Ast/alphatize.scm 270 */
						obj_t BgL_arg1901z00_4025;

						BgL_arg1901z00_4025 =
							(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt)
										((BgL_getfieldz00_bglt) BgL_nodez00_3648))))->
							BgL_exprza2za2);
						BgL_auxz00_6169 =
							BGl_dozd2alphatiza7eza2zd7zzast_alphatiza7eza7
							(BgL_arg1901z00_4025, BgL_locz00_3649);
					}
					((((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt) BgL_new1233z00_4022)))->
							BgL_exprza2za2) = ((obj_t) BgL_auxz00_6169), BUNSPEC);
				}
				((((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_new1233z00_4022)))->BgL_effectz00) =
					((obj_t) (((BgL_externz00_bglt)
								COBJECT(((BgL_externz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_getfieldz00_bglt) BgL_nodez00_3648)))))->
							BgL_effectz00)), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1233z00_4022)))->BgL_czd2formatzd2) =
					((obj_t) (((BgL_privatez00_bglt)
								COBJECT(((BgL_privatez00_bglt) ((BgL_nodez00_bglt) (
												(BgL_getfieldz00_bglt) BgL_nodez00_3648)))))->
							BgL_czd2formatzd2)), BUNSPEC);
				((((BgL_getfieldz00_bglt) COBJECT(BgL_new1233z00_4022))->BgL_fnamez00) =
					((obj_t) (((BgL_getfieldz00_bglt)
								COBJECT(((BgL_getfieldz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_getfieldz00_bglt) BgL_nodez00_3648)))))->
							BgL_fnamez00)), BUNSPEC);
				((((BgL_getfieldz00_bglt) COBJECT(BgL_new1233z00_4022))->BgL_ftypez00) =
					((BgL_typez00_bglt) (((BgL_getfieldz00_bglt)
								COBJECT(((BgL_getfieldz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_getfieldz00_bglt) BgL_nodez00_3648)))))->
							BgL_ftypez00)), BUNSPEC);
				((((BgL_getfieldz00_bglt) COBJECT(BgL_new1233z00_4022))->BgL_otypez00) =
					((BgL_typez00_bglt) (((BgL_getfieldz00_bglt)
								COBJECT(((BgL_getfieldz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_getfieldz00_bglt) BgL_nodez00_3648)))))->
							BgL_otypez00)), BUNSPEC);
				return ((BgL_nodez00_bglt) BgL_new1233z00_4022);
			}
		}

	}



/* &do-alphatize-cast-nu1628 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2castzd2nu1628z17zzast_alphatiza7eza7(obj_t
		BgL_envz00_3650, obj_t BgL_nodez00_3651, obj_t BgL_locz00_3652)
	{
		{	/* Ast/alphatize.scm 259 */
			{	/* Ast/alphatize.scm 260 */
				BgL_castzd2nullzd2_bglt BgL_new1225z00_4027;

				{	/* Ast/alphatize.scm 261 */
					BgL_castzd2nullzd2_bglt BgL_new1232z00_4028;

					BgL_new1232z00_4028 =
						((BgL_castzd2nullzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_castzd2nullzd2_bgl))));
					{	/* Ast/alphatize.scm 261 */
						long BgL_arg1899z00_4029;

						BgL_arg1899z00_4029 =
							BGL_CLASS_NUM(BGl_castzd2nullzd2zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1232z00_4028), BgL_arg1899z00_4029);
					}
					{	/* Ast/alphatize.scm 261 */
						BgL_objectz00_bglt BgL_tmpz00_6208;

						BgL_tmpz00_6208 = ((BgL_objectz00_bglt) BgL_new1232z00_4028);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6208, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1232z00_4028);
					BgL_new1225z00_4027 = BgL_new1232z00_4028;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1225z00_4027)))->BgL_locz00) =
					((obj_t)
						BGl_getzd2locationzd2zzast_alphatiza7eza7(((BgL_nodez00_bglt) (
									(BgL_castzd2nullzd2_bglt) BgL_nodez00_3651)),
							BgL_locz00_3652)), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1225z00_4027)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt)
								COBJECT(((BgL_nodez00_bglt) ((BgL_castzd2nullzd2_bglt)
											BgL_nodez00_3651))))->BgL_typez00)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1225z00_4027)))->BgL_sidezd2effectzd2) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_castzd2nullzd2_bglt) BgL_nodez00_3651)))))->
							BgL_sidezd2effectzd2)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1225z00_4027)))->BgL_keyz00) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_castzd2nullzd2_bglt) BgL_nodez00_3651)))))->
							BgL_keyz00)), BUNSPEC);
				{
					obj_t BgL_auxz00_6234;

					{	/* Ast/alphatize.scm 262 */
						obj_t BgL_arg1898z00_4030;

						BgL_arg1898z00_4030 =
							(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt)
										((BgL_castzd2nullzd2_bglt) BgL_nodez00_3651))))->
							BgL_exprza2za2);
						BgL_auxz00_6234 =
							BGl_dozd2alphatiza7eza2zd7zzast_alphatiza7eza7
							(BgL_arg1898z00_4030, BgL_locz00_3652);
					}
					((((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt) BgL_new1225z00_4027)))->
							BgL_exprza2za2) = ((obj_t) BgL_auxz00_6234), BUNSPEC);
				}
				((((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_new1225z00_4027)))->BgL_effectz00) =
					((obj_t) (((BgL_externz00_bglt)
								COBJECT(((BgL_externz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_castzd2nullzd2_bglt) BgL_nodez00_3651)))))->
							BgL_effectz00)), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1225z00_4027)))->BgL_czd2formatzd2) =
					((obj_t) (((BgL_privatez00_bglt)
								COBJECT(((BgL_privatez00_bglt) ((BgL_nodez00_bglt) (
												(BgL_castzd2nullzd2_bglt) BgL_nodez00_3651)))))->
							BgL_czd2formatzd2)), BUNSPEC);
				return ((BgL_nodez00_bglt) BgL_new1225z00_4027);
			}
		}

	}



/* &do-alphatize-pragma1626 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2pragma1626zc5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3653, obj_t BgL_nodez00_3654, obj_t BgL_locz00_3655)
	{
		{	/* Ast/alphatize.scm 251 */
			{	/* Ast/alphatize.scm 252 */
				BgL_pragmaz00_bglt BgL_new1216z00_4032;

				{	/* Ast/alphatize.scm 253 */
					BgL_pragmaz00_bglt BgL_new1224z00_4033;

					BgL_new1224z00_4033 =
						((BgL_pragmaz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_pragmaz00_bgl))));
					{	/* Ast/alphatize.scm 253 */
						long BgL_arg1897z00_4034;

						BgL_arg1897z00_4034 = BGL_CLASS_NUM(BGl_pragmaz00zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1224z00_4033), BgL_arg1897z00_4034);
					}
					{	/* Ast/alphatize.scm 253 */
						BgL_objectz00_bglt BgL_tmpz00_6258;

						BgL_tmpz00_6258 = ((BgL_objectz00_bglt) BgL_new1224z00_4033);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6258, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1224z00_4033);
					BgL_new1216z00_4032 = BgL_new1224z00_4033;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1216z00_4032)))->BgL_locz00) =
					((obj_t)
						BGl_getzd2locationzd2zzast_alphatiza7eza7(((BgL_nodez00_bglt) (
									(BgL_pragmaz00_bglt) BgL_nodez00_3654)), BgL_locz00_3655)),
					BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1216z00_4032)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt)
								COBJECT(((BgL_nodez00_bglt) ((BgL_pragmaz00_bglt)
											BgL_nodez00_3654))))->BgL_typez00)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1216z00_4032)))->BgL_sidezd2effectzd2) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_pragmaz00_bglt) BgL_nodez00_3654)))))->
							BgL_sidezd2effectzd2)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1216z00_4032)))->BgL_keyz00) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_pragmaz00_bglt) BgL_nodez00_3654)))))->
							BgL_keyz00)), BUNSPEC);
				{
					obj_t BgL_auxz00_6284;

					{	/* Ast/alphatize.scm 254 */
						obj_t BgL_arg1896z00_4035;

						BgL_arg1896z00_4035 =
							(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt)
										((BgL_pragmaz00_bglt) BgL_nodez00_3654))))->BgL_exprza2za2);
						BgL_auxz00_6284 =
							BGl_dozd2alphatiza7eza2zd7zzast_alphatiza7eza7
							(BgL_arg1896z00_4035, BgL_locz00_3655);
					}
					((((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt) BgL_new1216z00_4032)))->
							BgL_exprza2za2) = ((obj_t) BgL_auxz00_6284), BUNSPEC);
				}
				((((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_new1216z00_4032)))->BgL_effectz00) =
					((obj_t) (((BgL_externz00_bglt)
								COBJECT(((BgL_externz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_pragmaz00_bglt) BgL_nodez00_3654)))))->
							BgL_effectz00)), BUNSPEC);
				((((BgL_pragmaz00_bglt) COBJECT(BgL_new1216z00_4032))->BgL_formatz00) =
					((obj_t) (((BgL_pragmaz00_bglt)
								COBJECT(((BgL_pragmaz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_pragmaz00_bglt) BgL_nodez00_3654)))))->
							BgL_formatz00)), BUNSPEC);
				((((BgL_pragmaz00_bglt) COBJECT(BgL_new1216z00_4032))->BgL_srfi0z00) =
					((obj_t) (((BgL_pragmaz00_bglt)
								COBJECT(((BgL_pragmaz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_pragmaz00_bglt) BgL_nodez00_3654)))))->
							BgL_srfi0z00)), BUNSPEC);
				return ((BgL_nodez00_bglt) BgL_new1216z00_4032);
			}
		}

	}



/* &do-alphatize-funcall1624 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2funcall1624zc5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3656, obj_t BgL_nodez00_3657, obj_t BgL_locz00_3658)
	{
		{	/* Ast/alphatize.scm 237 */
			{	/* Ast/alphatize.scm 238 */
				BgL_nodez00_bglt BgL_funz00_4037;
				obj_t BgL_argsz00_4038;

				BgL_funz00_4037 =
					BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_3657)))->BgL_funz00),
					BgL_locz00_3658);
				BgL_argsz00_4038 =
					BGl_dozd2alphatiza7eza2zd7zzast_alphatiza7eza7((((BgL_funcallz00_bglt)
							COBJECT(((BgL_funcallz00_bglt) BgL_nodez00_3657)))->BgL_argsz00),
					BgL_locz00_3658);
				{	/* Ast/alphatize.scm 240 */
					bool_t BgL_test2288z00_6314;

					{	/* Ast/alphatize.scm 240 */
						obj_t BgL_classz00_4039;

						BgL_classz00_4039 = BGl_closurez00zzast_nodez00;
						{	/* Ast/alphatize.scm 240 */
							BgL_objectz00_bglt BgL_arg1807z00_4040;

							{	/* Ast/alphatize.scm 240 */
								obj_t BgL_tmpz00_6315;

								BgL_tmpz00_6315 =
									((obj_t) ((BgL_objectz00_bglt) BgL_funz00_4037));
								BgL_arg1807z00_4040 = (BgL_objectz00_bglt) (BgL_tmpz00_6315);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Ast/alphatize.scm 240 */
									long BgL_idxz00_4041;

									BgL_idxz00_4041 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4040);
									BgL_test2288z00_6314 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_4041 + 3L)) == BgL_classz00_4039);
								}
							else
								{	/* Ast/alphatize.scm 240 */
									bool_t BgL_res2133z00_4044;

									{	/* Ast/alphatize.scm 240 */
										obj_t BgL_oclassz00_4045;

										{	/* Ast/alphatize.scm 240 */
											obj_t BgL_arg1815z00_4046;
											long BgL_arg1816z00_4047;

											BgL_arg1815z00_4046 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Ast/alphatize.scm 240 */
												long BgL_arg1817z00_4048;

												BgL_arg1817z00_4048 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4040);
												BgL_arg1816z00_4047 =
													(BgL_arg1817z00_4048 - OBJECT_TYPE);
											}
											BgL_oclassz00_4045 =
												VECTOR_REF(BgL_arg1815z00_4046, BgL_arg1816z00_4047);
										}
										{	/* Ast/alphatize.scm 240 */
											bool_t BgL__ortest_1115z00_4049;

											BgL__ortest_1115z00_4049 =
												(BgL_classz00_4039 == BgL_oclassz00_4045);
											if (BgL__ortest_1115z00_4049)
												{	/* Ast/alphatize.scm 240 */
													BgL_res2133z00_4044 = BgL__ortest_1115z00_4049;
												}
											else
												{	/* Ast/alphatize.scm 240 */
													long BgL_odepthz00_4050;

													{	/* Ast/alphatize.scm 240 */
														obj_t BgL_arg1804z00_4051;

														BgL_arg1804z00_4051 = (BgL_oclassz00_4045);
														BgL_odepthz00_4050 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_4051);
													}
													if ((3L < BgL_odepthz00_4050))
														{	/* Ast/alphatize.scm 240 */
															obj_t BgL_arg1802z00_4052;

															{	/* Ast/alphatize.scm 240 */
																obj_t BgL_arg1803z00_4053;

																BgL_arg1803z00_4053 = (BgL_oclassz00_4045);
																BgL_arg1802z00_4052 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4053,
																	3L);
															}
															BgL_res2133z00_4044 =
																(BgL_arg1802z00_4052 == BgL_classz00_4039);
														}
													else
														{	/* Ast/alphatize.scm 240 */
															BgL_res2133z00_4044 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2288z00_6314 = BgL_res2133z00_4044;
								}
						}
					}
					if (BgL_test2288z00_6314)
						{	/* Ast/alphatize.scm 241 */
							obj_t BgL_arg1885z00_4054;
							obj_t BgL_arg1887z00_4055;

							{	/* Ast/alphatize.scm 241 */
								BgL_refz00_bglt BgL_arg1888z00_4056;
								obj_t BgL_arg1889z00_4057;

								{	/* Ast/alphatize.scm 241 */
									BgL_refz00_bglt BgL_new1205z00_4058;

									{	/* Ast/alphatize.scm 241 */
										BgL_refz00_bglt BgL_new1209z00_4059;

										BgL_new1209z00_4059 =
											((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_refz00_bgl))));
										{	/* Ast/alphatize.scm 241 */
											long BgL_arg1890z00_4060;

											BgL_arg1890z00_4060 =
												BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1209z00_4059),
												BgL_arg1890z00_4060);
										}
										{	/* Ast/alphatize.scm 241 */
											BgL_objectz00_bglt BgL_tmpz00_6342;

											BgL_tmpz00_6342 =
												((BgL_objectz00_bglt) BgL_new1209z00_4059);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6342, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1209z00_4059);
										BgL_new1205z00_4058 = BgL_new1209z00_4059;
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1205z00_4058)))->
											BgL_locz00) =
										((obj_t) (((BgL_nodez00_bglt) COBJECT(BgL_funz00_4037))->
												BgL_locz00)), BUNSPEC);
									((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_new1205z00_4058)))->BgL_typez00) =
										((BgL_typez00_bglt) (((BgL_nodez00_bglt)
													COBJECT(BgL_funz00_4037))->BgL_typez00)), BUNSPEC);
									((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
														BgL_new1205z00_4058)))->BgL_variablez00) =
										((BgL_variablez00_bglt) (((BgL_varz00_bglt)
													COBJECT(((BgL_varz00_bglt) BgL_funz00_4037)))->
												BgL_variablez00)), BUNSPEC);
									BgL_arg1888z00_4056 = BgL_new1205z00_4058;
								}
								BgL_arg1889z00_4057 =
									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(CDR(
										((obj_t) BgL_argsz00_4038)), BNIL);
								BgL_arg1885z00_4054 =
									MAKE_YOUNG_PAIR(
									((obj_t) BgL_arg1888z00_4056), BgL_arg1889z00_4057);
							}
							BgL_arg1887z00_4055 =
								(((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_funcallz00_bglt) BgL_nodez00_3657))))->BgL_locz00);
							return
								BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_arg1885z00_4054, BNIL,
								BgL_arg1887z00_4055, CNST_TABLE_REF(6));
						}
					else
						{	/* Ast/alphatize.scm 243 */
							BgL_funcallz00_bglt BgL_new1210z00_4061;

							{	/* Ast/alphatize.scm 244 */
								BgL_funcallz00_bglt BgL_new1215z00_4062;

								BgL_new1215z00_4062 =
									((BgL_funcallz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_funcallz00_bgl))));
								{	/* Ast/alphatize.scm 244 */
									long BgL_arg1892z00_4063;

									BgL_arg1892z00_4063 =
										BGL_CLASS_NUM(BGl_funcallz00zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1215z00_4062),
										BgL_arg1892z00_4063);
								}
								{	/* Ast/alphatize.scm 244 */
									BgL_objectz00_bglt BgL_tmpz00_6370;

									BgL_tmpz00_6370 = ((BgL_objectz00_bglt) BgL_new1215z00_4062);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6370, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1215z00_4062);
								BgL_new1210z00_4061 = BgL_new1215z00_4062;
							}
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt) BgL_new1210z00_4061)))->BgL_locz00) =
								((obj_t)
									BGl_getzd2locationzd2zzast_alphatiza7eza7(((BgL_nodez00_bglt)
											((BgL_funcallz00_bglt) BgL_nodez00_3657)),
										BgL_locz00_3658)), BUNSPEC);
							((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
												BgL_new1210z00_4061)))->BgL_typez00) =
								((BgL_typez00_bglt) (((BgL_nodez00_bglt)
											COBJECT(((BgL_nodez00_bglt) ((BgL_funcallz00_bglt)
														BgL_nodez00_3657))))->BgL_typez00)), BUNSPEC);
							((((BgL_funcallz00_bglt) COBJECT(BgL_new1210z00_4061))->
									BgL_funz00) = ((BgL_nodez00_bglt) BgL_funz00_4037), BUNSPEC);
							((((BgL_funcallz00_bglt) COBJECT(BgL_new1210z00_4061))->
									BgL_argsz00) = ((obj_t) BgL_argsz00_4038), BUNSPEC);
							((((BgL_funcallz00_bglt) COBJECT(BgL_new1210z00_4061))->
									BgL_strengthz00) =
								((obj_t) (((BgL_funcallz00_bglt)
											COBJECT(((BgL_funcallz00_bglt) ((BgL_nodez00_bglt) (
															(BgL_funcallz00_bglt) BgL_nodez00_3657)))))->
										BgL_strengthz00)), BUNSPEC);
							((((BgL_funcallz00_bglt) COBJECT(BgL_new1210z00_4061))->
									BgL_functionsz00) =
								((obj_t) (((BgL_funcallz00_bglt)
											COBJECT(((BgL_funcallz00_bglt) ((BgL_nodez00_bglt) (
															(BgL_funcallz00_bglt) BgL_nodez00_3657)))))->
										BgL_functionsz00)), BUNSPEC);
							return ((BgL_nodez00_bglt) BgL_new1210z00_4061);
						}
				}
			}
		}

	}



/* &do-alphatize-app-ly1622 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2appzd2ly1622z17zzast_alphatiza7eza7(obj_t
		BgL_envz00_3659, obj_t BgL_nodez00_3660, obj_t BgL_locz00_3661)
	{
		{	/* Ast/alphatize.scm 215 */
			{	/* Ast/alphatize.scm 216 */
				BgL_nodez00_bglt BgL_funz00_4065;
				BgL_nodez00_bglt BgL_argz00_4066;

				BgL_funz00_4065 =
					BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(
					(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_3660)))->BgL_funz00),
					BgL_locz00_3661);
				BgL_argz00_4066 =
					BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7((((BgL_appzd2lyzd2_bglt)
							COBJECT(((BgL_appzd2lyzd2_bglt) BgL_nodez00_3660)))->BgL_argz00),
					BgL_locz00_3661);
				{	/* Ast/alphatize.scm 218 */
					bool_t BgL_test2292z00_6403;

					{	/* Ast/alphatize.scm 218 */
						bool_t BgL_test2293z00_6404;

						{	/* Ast/alphatize.scm 218 */
							obj_t BgL_classz00_4067;

							BgL_classz00_4067 = BGl_closurez00zzast_nodez00;
							{	/* Ast/alphatize.scm 218 */
								BgL_objectz00_bglt BgL_arg1807z00_4068;

								{	/* Ast/alphatize.scm 218 */
									obj_t BgL_tmpz00_6405;

									BgL_tmpz00_6405 =
										((obj_t) ((BgL_objectz00_bglt) BgL_funz00_4065));
									BgL_arg1807z00_4068 = (BgL_objectz00_bglt) (BgL_tmpz00_6405);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Ast/alphatize.scm 218 */
										long BgL_idxz00_4069;

										BgL_idxz00_4069 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4068);
										BgL_test2293z00_6404 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_4069 + 3L)) == BgL_classz00_4067);
									}
								else
									{	/* Ast/alphatize.scm 218 */
										bool_t BgL_res2132z00_4072;

										{	/* Ast/alphatize.scm 218 */
											obj_t BgL_oclassz00_4073;

											{	/* Ast/alphatize.scm 218 */
												obj_t BgL_arg1815z00_4074;
												long BgL_arg1816z00_4075;

												BgL_arg1815z00_4074 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Ast/alphatize.scm 218 */
													long BgL_arg1817z00_4076;

													BgL_arg1817z00_4076 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4068);
													BgL_arg1816z00_4075 =
														(BgL_arg1817z00_4076 - OBJECT_TYPE);
												}
												BgL_oclassz00_4073 =
													VECTOR_REF(BgL_arg1815z00_4074, BgL_arg1816z00_4075);
											}
											{	/* Ast/alphatize.scm 218 */
												bool_t BgL__ortest_1115z00_4077;

												BgL__ortest_1115z00_4077 =
													(BgL_classz00_4067 == BgL_oclassz00_4073);
												if (BgL__ortest_1115z00_4077)
													{	/* Ast/alphatize.scm 218 */
														BgL_res2132z00_4072 = BgL__ortest_1115z00_4077;
													}
												else
													{	/* Ast/alphatize.scm 218 */
														long BgL_odepthz00_4078;

														{	/* Ast/alphatize.scm 218 */
															obj_t BgL_arg1804z00_4079;

															BgL_arg1804z00_4079 = (BgL_oclassz00_4073);
															BgL_odepthz00_4078 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_4079);
														}
														if ((3L < BgL_odepthz00_4078))
															{	/* Ast/alphatize.scm 218 */
																obj_t BgL_arg1802z00_4080;

																{	/* Ast/alphatize.scm 218 */
																	obj_t BgL_arg1803z00_4081;

																	BgL_arg1803z00_4081 = (BgL_oclassz00_4073);
																	BgL_arg1802z00_4080 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4081,
																		3L);
																}
																BgL_res2132z00_4072 =
																	(BgL_arg1802z00_4080 == BgL_classz00_4067);
															}
														else
															{	/* Ast/alphatize.scm 218 */
																BgL_res2132z00_4072 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2293z00_6404 = BgL_res2132z00_4072;
									}
							}
						}
						if (BgL_test2293z00_6404)
							{	/* Ast/alphatize.scm 219 */
								bool_t BgL_test2297z00_6428;

								{	/* Ast/alphatize.scm 219 */
									BgL_variablez00_bglt BgL_arg1880z00_4082;

									BgL_arg1880z00_4082 =
										(((BgL_varz00_bglt) COBJECT(
												((BgL_varz00_bglt) BgL_funz00_4065)))->BgL_variablez00);
									BgL_test2297z00_6428 =
										BGl_globalzd2optionalzf3z21zzast_varz00(
										((obj_t) BgL_arg1880z00_4082));
								}
								if (BgL_test2297z00_6428)
									{	/* Ast/alphatize.scm 219 */
										BgL_test2292z00_6403 = ((bool_t) 0);
									}
								else
									{	/* Ast/alphatize.scm 220 */
										bool_t BgL_test2298z00_6433;

										{	/* Ast/alphatize.scm 220 */
											BgL_variablez00_bglt BgL_arg1879z00_4083;

											BgL_arg1879z00_4083 =
												(((BgL_varz00_bglt) COBJECT(
														((BgL_varz00_bglt) BgL_funz00_4065)))->
												BgL_variablez00);
											BgL_test2298z00_6433 =
												BGl_globalzd2keyzf3z21zzast_varz00(((obj_t)
													BgL_arg1879z00_4083));
										}
										if (BgL_test2298z00_6433)
											{	/* Ast/alphatize.scm 220 */
												BgL_test2292z00_6403 = ((bool_t) 0);
											}
										else
											{	/* Ast/alphatize.scm 220 */
												BgL_test2292z00_6403 = ((bool_t) 1);
											}
									}
							}
						else
							{	/* Ast/alphatize.scm 218 */
								BgL_test2292z00_6403 = ((bool_t) 0);
							}
					}
					if (BgL_test2292z00_6403)
						{	/* Ast/alphatize.scm 222 */
							obj_t BgL_arg1875z00_4084;
							BgL_refz00_bglt BgL_arg1876z00_4085;

							BgL_arg1875z00_4084 =
								BGl_getzd2locationzd2zzast_alphatiza7eza7(
								((BgL_nodez00_bglt)
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_3660)), BgL_locz00_3661);
							{	/* Ast/alphatize.scm 223 */
								BgL_refz00_bglt BgL_new1196z00_4086;

								{	/* Ast/alphatize.scm 223 */
									BgL_refz00_bglt BgL_new1200z00_4087;

									BgL_new1200z00_4087 =
										((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_refz00_bgl))));
									{	/* Ast/alphatize.scm 223 */
										long BgL_arg1877z00_4088;

										BgL_arg1877z00_4088 =
											BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1200z00_4087),
											BgL_arg1877z00_4088);
									}
									{	/* Ast/alphatize.scm 223 */
										BgL_objectz00_bglt BgL_tmpz00_6445;

										BgL_tmpz00_6445 =
											((BgL_objectz00_bglt) BgL_new1200z00_4087);
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6445, BFALSE);
									}
									((BgL_objectz00_bglt) BgL_new1200z00_4087);
									BgL_new1196z00_4086 = BgL_new1200z00_4087;
								}
								((((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt) BgL_new1196z00_4086)))->
										BgL_locz00) =
									((obj_t) (((BgL_nodez00_bglt) COBJECT(BgL_funz00_4065))->
											BgL_locz00)), BUNSPEC);
								((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
													BgL_new1196z00_4086)))->BgL_typez00) =
									((BgL_typez00_bglt) (((BgL_nodez00_bglt)
												COBJECT(BgL_funz00_4065))->BgL_typez00)), BUNSPEC);
								((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
													BgL_new1196z00_4086)))->BgL_variablez00) =
									((BgL_variablez00_bglt) (((BgL_varz00_bglt)
												COBJECT(((BgL_varz00_bglt) BgL_funz00_4065)))->
											BgL_variablez00)), BUNSPEC);
								BgL_arg1876z00_4085 = BgL_new1196z00_4086;
							}
							return
								BGl_knownzd2appzd2lyzd2ze3nodez31zzast_applyz00(BNIL,
								BgL_arg1875z00_4084, ((BgL_nodez00_bglt) BgL_arg1876z00_4085),
								BgL_argz00_4066, CNST_TABLE_REF(7));
						}
					else
						{	/* Ast/alphatize.scm 226 */
							BgL_appzd2lyzd2_bglt BgL_new1201z00_4089;

							{	/* Ast/alphatize.scm 227 */
								BgL_appzd2lyzd2_bglt BgL_new1204z00_4090;

								BgL_new1204z00_4090 =
									((BgL_appzd2lyzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_appzd2lyzd2_bgl))));
								{	/* Ast/alphatize.scm 227 */
									long BgL_arg1878z00_4091;

									BgL_arg1878z00_4091 =
										BGL_CLASS_NUM(BGl_appzd2lyzd2zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1204z00_4090),
										BgL_arg1878z00_4091);
								}
								{	/* Ast/alphatize.scm 227 */
									BgL_objectz00_bglt BgL_tmpz00_6466;

									BgL_tmpz00_6466 = ((BgL_objectz00_bglt) BgL_new1204z00_4090);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6466, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1204z00_4090);
								BgL_new1201z00_4089 = BgL_new1204z00_4090;
							}
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt) BgL_new1201z00_4089)))->BgL_locz00) =
								((obj_t)
									BGl_getzd2locationzd2zzast_alphatiza7eza7(((BgL_nodez00_bglt)
											((BgL_appzd2lyzd2_bglt) BgL_nodez00_3660)),
										BgL_locz00_3661)), BUNSPEC);
							((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
												BgL_new1201z00_4089)))->BgL_typez00) =
								((BgL_typez00_bglt) (((BgL_nodez00_bglt)
											COBJECT(((BgL_nodez00_bglt) ((BgL_appzd2lyzd2_bglt)
														BgL_nodez00_3660))))->BgL_typez00)), BUNSPEC);
							((((BgL_appzd2lyzd2_bglt) COBJECT(BgL_new1201z00_4089))->
									BgL_funz00) = ((BgL_nodez00_bglt) BgL_funz00_4065), BUNSPEC);
							((((BgL_appzd2lyzd2_bglt) COBJECT(BgL_new1201z00_4089))->
									BgL_argz00) = ((BgL_nodez00_bglt) BgL_argz00_4066), BUNSPEC);
							return ((BgL_nodez00_bglt) BgL_new1201z00_4089);
						}
				}
			}
		}

	}



/* &do-alphatize-app1620 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2app1620zc5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3662, obj_t BgL_nodez00_3663, obj_t BgL_locz00_3664)
	{
		{	/* Ast/alphatize.scm 201 */
			{	/* Ast/alphatize.scm 204 */
				BgL_appz00_bglt BgL_new1184z00_4093;

				{	/* Ast/alphatize.scm 205 */
					BgL_appz00_bglt BgL_new1190z00_4094;

					BgL_new1190z00_4094 =
						((BgL_appz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_appz00_bgl))));
					{	/* Ast/alphatize.scm 205 */
						long BgL_arg1866z00_4095;

						BgL_arg1866z00_4095 = BGL_CLASS_NUM(BGl_appz00zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1190z00_4094), BgL_arg1866z00_4095);
					}
					{	/* Ast/alphatize.scm 205 */
						BgL_objectz00_bglt BgL_tmpz00_6487;

						BgL_tmpz00_6487 = ((BgL_objectz00_bglt) BgL_new1190z00_4094);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6487, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1190z00_4094);
					BgL_new1184z00_4093 = BgL_new1190z00_4094;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1184z00_4093)))->BgL_locz00) =
					((obj_t)
						BGl_getzd2locationzd2zzast_alphatiza7eza7(((BgL_nodez00_bglt) (
									(BgL_appz00_bglt) BgL_nodez00_3663)), BgL_locz00_3664)),
					BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1184z00_4093)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt)
								COBJECT(((BgL_nodez00_bglt) ((BgL_appz00_bglt)
											BgL_nodez00_3663))))->BgL_typez00)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1184z00_4093)))->BgL_sidezd2effectzd2) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_appz00_bglt) BgL_nodez00_3663)))))->
							BgL_sidezd2effectzd2)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1184z00_4093)))->BgL_keyz00) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_appz00_bglt) BgL_nodez00_3663)))))->BgL_keyz00)),
					BUNSPEC);
				{
					BgL_varz00_bglt BgL_auxz00_6513;

					{	/* Ast/alphatize.scm 206 */
						BgL_nodez00_bglt BgL_varz00_4096;

						{	/* Ast/alphatize.scm 206 */
							BgL_varz00_bglt BgL_arg1863z00_4097;

							BgL_arg1863z00_4097 =
								(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_nodez00_3663)))->BgL_funz00);
							BgL_varz00_4096 =
								BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(
								((BgL_nodez00_bglt) BgL_arg1863z00_4097), BgL_locz00_3664);
						}
						{	/* Ast/alphatize.scm 207 */
							bool_t BgL_test2299z00_6518;

							{	/* Ast/alphatize.scm 207 */
								obj_t BgL_classz00_4098;

								BgL_classz00_4098 = BGl_closurez00zzast_nodez00;
								{	/* Ast/alphatize.scm 207 */
									BgL_objectz00_bglt BgL_arg1807z00_4099;

									{	/* Ast/alphatize.scm 207 */
										obj_t BgL_tmpz00_6519;

										BgL_tmpz00_6519 =
											((obj_t) ((BgL_objectz00_bglt) BgL_varz00_4096));
										BgL_arg1807z00_4099 =
											(BgL_objectz00_bglt) (BgL_tmpz00_6519);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Ast/alphatize.scm 207 */
											long BgL_idxz00_4100;

											BgL_idxz00_4100 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4099);
											{	/* Ast/alphatize.scm 207 */
												obj_t BgL_arg1800z00_4101;

												{	/* Ast/alphatize.scm 207 */
													long BgL_arg1801z00_4102;

													BgL_arg1801z00_4102 = (BgL_idxz00_4100 + 3L);
													{	/* Ast/alphatize.scm 207 */
														obj_t BgL_vectorz00_4103;

														BgL_vectorz00_4103 =
															BGl_za2inheritancesza2z00zz__objectz00;
														BgL_arg1800z00_4101 =
															VECTOR_REF(BgL_vectorz00_4103,
															BgL_arg1801z00_4102);
												}}
												BgL_test2299z00_6518 =
													(BgL_arg1800z00_4101 == BgL_classz00_4098);
										}}
									else
										{	/* Ast/alphatize.scm 207 */
											bool_t BgL_res2131z00_4104;

											{	/* Ast/alphatize.scm 207 */
												obj_t BgL_oclassz00_4105;

												{	/* Ast/alphatize.scm 207 */
													obj_t BgL_arg1815z00_4106;
													long BgL_arg1816z00_4107;

													BgL_arg1815z00_4106 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Ast/alphatize.scm 207 */
														long BgL_arg1817z00_4108;

														BgL_arg1817z00_4108 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4099);
														BgL_arg1816z00_4107 =
															(BgL_arg1817z00_4108 - OBJECT_TYPE);
													}
													BgL_oclassz00_4105 =
														VECTOR_REF(BgL_arg1815z00_4106,
														BgL_arg1816z00_4107);
												}
												{	/* Ast/alphatize.scm 207 */
													bool_t BgL__ortest_1115z00_4109;

													BgL__ortest_1115z00_4109 =
														(BgL_classz00_4098 == BgL_oclassz00_4105);
													if (BgL__ortest_1115z00_4109)
														{	/* Ast/alphatize.scm 207 */
															BgL_res2131z00_4104 = BgL__ortest_1115z00_4109;
														}
													else
														{	/* Ast/alphatize.scm 207 */
															long BgL_odepthz00_4110;

															{	/* Ast/alphatize.scm 207 */
																obj_t BgL_arg1804z00_4111;

																BgL_arg1804z00_4111 = (BgL_oclassz00_4105);
																BgL_odepthz00_4110 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4111);
															}
															if ((3L < BgL_odepthz00_4110))
																{	/* Ast/alphatize.scm 207 */
																	obj_t BgL_arg1802z00_4112;

																	{	/* Ast/alphatize.scm 207 */
																		obj_t BgL_arg1803z00_4113;

																		BgL_arg1803z00_4113 = (BgL_oclassz00_4105);
																		BgL_arg1802z00_4112 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4113, 3L);
																	}
																	BgL_res2131z00_4104 =
																		(BgL_arg1802z00_4112 == BgL_classz00_4098);
																}
															else
																{	/* Ast/alphatize.scm 207 */
																	BgL_res2131z00_4104 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2299z00_6518 = BgL_res2131z00_4104;
										}
								}
							}
							if (BgL_test2299z00_6518)
								{	/* Ast/alphatize.scm 208 */
									BgL_refz00_bglt BgL_new1191z00_4114;

									{	/* Ast/alphatize.scm 208 */
										BgL_refz00_bglt BgL_new1195z00_4115;

										BgL_new1195z00_4115 =
											((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_refz00_bgl))));
										{	/* Ast/alphatize.scm 208 */
											long BgL_arg1862z00_4116;

											{	/* Ast/alphatize.scm 208 */
												obj_t BgL_classz00_4117;

												BgL_classz00_4117 = BGl_refz00zzast_nodez00;
												BgL_arg1862z00_4116 = BGL_CLASS_NUM(BgL_classz00_4117);
											}
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1195z00_4115),
												BgL_arg1862z00_4116);
										}
										{	/* Ast/alphatize.scm 208 */
											BgL_objectz00_bglt BgL_tmpz00_6546;

											BgL_tmpz00_6546 =
												((BgL_objectz00_bglt) BgL_new1195z00_4115);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6546, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1195z00_4115);
										BgL_new1191z00_4114 = BgL_new1195z00_4115;
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1191z00_4114)))->
											BgL_locz00) =
										((obj_t) (((BgL_nodez00_bglt) COBJECT(BgL_varz00_4096))->
												BgL_locz00)), BUNSPEC);
									((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_new1191z00_4114)))->BgL_typez00) =
										((BgL_typez00_bglt) (((BgL_nodez00_bglt)
													COBJECT(BgL_varz00_4096))->BgL_typez00)), BUNSPEC);
									((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
														BgL_new1191z00_4114)))->BgL_variablez00) =
										((BgL_variablez00_bglt) (((BgL_varz00_bglt)
													COBJECT(((BgL_varz00_bglt) BgL_varz00_4096)))->
												BgL_variablez00)), BUNSPEC);
									BgL_auxz00_6513 = ((BgL_varz00_bglt) BgL_new1191z00_4114);
								}
							else
								{	/* Ast/alphatize.scm 207 */
									BgL_auxz00_6513 = ((BgL_varz00_bglt) BgL_varz00_4096);
								}
						}
					}
					((((BgL_appz00_bglt) COBJECT(BgL_new1184z00_4093))->BgL_funz00) =
						((BgL_varz00_bglt) BgL_auxz00_6513), BUNSPEC);
				}
				{
					obj_t BgL_auxz00_6563;

					{	/* Ast/alphatize.scm 210 */
						obj_t BgL_arg1864z00_4118;

						BgL_arg1864z00_4118 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt) BgL_nodez00_3663)))->BgL_argsz00);
						BgL_auxz00_6563 =
							BGl_dozd2alphatiza7eza2zd7zzast_alphatiza7eza7
							(BgL_arg1864z00_4118, BgL_locz00_3664);
					}
					((((BgL_appz00_bglt) COBJECT(BgL_new1184z00_4093))->BgL_argsz00) =
						((obj_t) BgL_auxz00_6563), BUNSPEC);
				}
				((((BgL_appz00_bglt) COBJECT(BgL_new1184z00_4093))->BgL_stackablez00) =
					((obj_t) (((BgL_appz00_bglt)
								COBJECT(((BgL_appz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_appz00_bglt) BgL_nodez00_3663)))))->
							BgL_stackablez00)), BUNSPEC);
				return ((BgL_nodez00_bglt) BgL_new1184z00_4093);
			}
		}

	}



/* &do-alphatize-sync1618 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2sync1618zc5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3665, obj_t BgL_nodez00_3666, obj_t BgL_locz00_3667)
	{
		{	/* Ast/alphatize.scm 191 */
			{	/* Ast/alphatize.scm 192 */
				BgL_syncz00_bglt BgL_new1180z00_4120;

				{	/* Ast/alphatize.scm 193 */
					BgL_syncz00_bglt BgL_new1183z00_4121;

					BgL_new1183z00_4121 =
						((BgL_syncz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_syncz00_bgl))));
					{	/* Ast/alphatize.scm 193 */
						long BgL_arg1859z00_4122;

						BgL_arg1859z00_4122 = BGL_CLASS_NUM(BGl_syncz00zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1183z00_4121), BgL_arg1859z00_4122);
					}
					{	/* Ast/alphatize.scm 193 */
						BgL_objectz00_bglt BgL_tmpz00_6578;

						BgL_tmpz00_6578 = ((BgL_objectz00_bglt) BgL_new1183z00_4121);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6578, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1183z00_4121);
					BgL_new1180z00_4120 = BgL_new1183z00_4121;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1180z00_4120)))->BgL_locz00) =
					((obj_t)
						BGl_getzd2locationzd2zzast_alphatiza7eza7(((BgL_nodez00_bglt) (
									(BgL_syncz00_bglt) BgL_nodez00_3666)), BgL_locz00_3667)),
					BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1180z00_4120)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt)
								COBJECT(((BgL_nodez00_bglt) ((BgL_syncz00_bglt)
											BgL_nodez00_3666))))->BgL_typez00)), BUNSPEC);
				{
					BgL_nodez00_bglt BgL_auxz00_6592;

					{	/* Ast/alphatize.scm 194 */
						BgL_nodez00_bglt BgL_arg1856z00_4123;

						BgL_arg1856z00_4123 =
							(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nodez00_3666)))->BgL_mutexz00);
						BgL_auxz00_6592 =
							BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(BgL_arg1856z00_4123,
							BgL_locz00_3667);
					}
					((((BgL_syncz00_bglt) COBJECT(BgL_new1180z00_4120))->BgL_mutexz00) =
						((BgL_nodez00_bglt) BgL_auxz00_6592), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_6597;

					{	/* Ast/alphatize.scm 195 */
						BgL_nodez00_bglt BgL_arg1857z00_4124;

						BgL_arg1857z00_4124 =
							(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nodez00_3666)))->BgL_prelockz00);
						BgL_auxz00_6597 =
							BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(BgL_arg1857z00_4124,
							BgL_locz00_3667);
					}
					((((BgL_syncz00_bglt) COBJECT(BgL_new1180z00_4120))->BgL_prelockz00) =
						((BgL_nodez00_bglt) BgL_auxz00_6597), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_6602;

					{	/* Ast/alphatize.scm 196 */
						BgL_nodez00_bglt BgL_arg1858z00_4125;

						BgL_arg1858z00_4125 =
							(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nodez00_3666)))->BgL_bodyz00);
						BgL_auxz00_6602 =
							BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(BgL_arg1858z00_4125,
							BgL_locz00_3667);
					}
					((((BgL_syncz00_bglt) COBJECT(BgL_new1180z00_4120))->BgL_bodyz00) =
						((BgL_nodez00_bglt) BgL_auxz00_6602), BUNSPEC);
				}
				return ((BgL_nodez00_bglt) BgL_new1180z00_4120);
			}
		}

	}



/* &do-alphatize-sequenc1616 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2sequenc1616zc5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3668, obj_t BgL_nodez00_3669, obj_t BgL_locz00_3670)
	{
		{	/* Ast/alphatize.scm 183 */
			{	/* Ast/alphatize.scm 184 */
				BgL_sequencez00_bglt BgL_new1172z00_4127;

				{	/* Ast/alphatize.scm 185 */
					BgL_sequencez00_bglt BgL_new1179z00_4128;

					BgL_new1179z00_4128 =
						((BgL_sequencez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_sequencez00_bgl))));
					{	/* Ast/alphatize.scm 185 */
						long BgL_arg1854z00_4129;

						BgL_arg1854z00_4129 = BGL_CLASS_NUM(BGl_sequencez00zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1179z00_4128), BgL_arg1854z00_4129);
					}
					{	/* Ast/alphatize.scm 185 */
						BgL_objectz00_bglt BgL_tmpz00_6612;

						BgL_tmpz00_6612 = ((BgL_objectz00_bglt) BgL_new1179z00_4128);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6612, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1179z00_4128);
					BgL_new1172z00_4127 = BgL_new1179z00_4128;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1172z00_4127)))->BgL_locz00) =
					((obj_t)
						BGl_getzd2locationzd2zzast_alphatiza7eza7(((BgL_nodez00_bglt) (
									(BgL_sequencez00_bglt) BgL_nodez00_3669)), BgL_locz00_3670)),
					BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1172z00_4127)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt)
								COBJECT(((BgL_nodez00_bglt) ((BgL_sequencez00_bglt)
											BgL_nodez00_3669))))->BgL_typez00)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1172z00_4127)))->BgL_sidezd2effectzd2) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_sequencez00_bglt) BgL_nodez00_3669)))))->
							BgL_sidezd2effectzd2)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1172z00_4127)))->BgL_keyz00) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) (
												(BgL_sequencez00_bglt) BgL_nodez00_3669)))))->
							BgL_keyz00)), BUNSPEC);
				{
					obj_t BgL_auxz00_6638;

					{	/* Ast/alphatize.scm 186 */
						obj_t BgL_arg1853z00_4130;

						BgL_arg1853z00_4130 =
							(((BgL_sequencez00_bglt) COBJECT(
									((BgL_sequencez00_bglt) BgL_nodez00_3669)))->BgL_nodesz00);
						BgL_auxz00_6638 =
							BGl_dozd2alphatiza7eza2zd7zzast_alphatiza7eza7
							(BgL_arg1853z00_4130, BgL_locz00_3670);
					}
					((((BgL_sequencez00_bglt) COBJECT(BgL_new1172z00_4127))->
							BgL_nodesz00) = ((obj_t) BgL_auxz00_6638), BUNSPEC);
				}
				((((BgL_sequencez00_bglt) COBJECT(BgL_new1172z00_4127))->
						BgL_unsafez00) =
					((bool_t) (((BgL_sequencez00_bglt)
								COBJECT(((BgL_sequencez00_bglt) ((BgL_nodez00_bglt) (
												(BgL_sequencez00_bglt) BgL_nodez00_3669)))))->
							BgL_unsafez00)), BUNSPEC);
				((((BgL_sequencez00_bglt) COBJECT(BgL_new1172z00_4127))->BgL_metaz00) =
					((obj_t) (((BgL_sequencez00_bglt)
								COBJECT(((BgL_sequencez00_bglt) ((BgL_nodez00_bglt) (
												(BgL_sequencez00_bglt) BgL_nodez00_3669)))))->
							BgL_metaz00)), BUNSPEC);
				return ((BgL_nodez00_bglt) BgL_new1172z00_4127);
			}
		}

	}



/* &do-alphatize-kwote1614 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2kwote1614zc5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3671, obj_t BgL_nodez00_3672, obj_t BgL_locz00_3673)
	{
		{	/* Ast/alphatize.scm 177 */
			{	/* Ast/alphatize.scm 178 */
				BgL_kwotez00_bglt BgL_new1167z00_4132;

				{	/* Ast/alphatize.scm 178 */
					BgL_kwotez00_bglt BgL_new1171z00_4133;

					BgL_new1171z00_4133 =
						((BgL_kwotez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_kwotez00_bgl))));
					{	/* Ast/alphatize.scm 178 */
						long BgL_arg1852z00_4134;

						BgL_arg1852z00_4134 = BGL_CLASS_NUM(BGl_kwotez00zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1171z00_4133), BgL_arg1852z00_4134);
					}
					{	/* Ast/alphatize.scm 178 */
						BgL_objectz00_bglt BgL_tmpz00_6658;

						BgL_tmpz00_6658 = ((BgL_objectz00_bglt) BgL_new1171z00_4133);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6658, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1171z00_4133);
					BgL_new1167z00_4132 = BgL_new1171z00_4133;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1167z00_4132)))->BgL_locz00) =
					((obj_t)
						BGl_getzd2locationzd2zzast_alphatiza7eza7(((BgL_nodez00_bglt) (
									(BgL_kwotez00_bglt) BgL_nodez00_3672)), BgL_locz00_3673)),
					BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1167z00_4132)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt)
								COBJECT(((BgL_nodez00_bglt) ((BgL_kwotez00_bglt)
											BgL_nodez00_3672))))->BgL_typez00)), BUNSPEC);
				((((BgL_kwotez00_bglt) COBJECT(BgL_new1167z00_4132))->BgL_valuez00) =
					((obj_t) (((BgL_kwotez00_bglt)
								COBJECT(((BgL_kwotez00_bglt) ((BgL_nodez00_bglt) (
												(BgL_kwotez00_bglt) BgL_nodez00_3672)))))->
							BgL_valuez00)), BUNSPEC);
				return ((BgL_nodez00_bglt) BgL_new1167z00_4132);
			}
		}

	}



/* &do-alphatize-closure1612 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2closure1612zc5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3674, obj_t BgL_nodez00_3675, obj_t BgL_locz00_3676)
	{
		{	/* Ast/alphatize.scm 155 */
			{	/* Ast/alphatize.scm 156 */
				BgL_variablez00_bglt BgL_varz00_4136;

				BgL_varz00_4136 =
					(((BgL_varz00_bglt) COBJECT(
							((BgL_varz00_bglt)
								((BgL_closurez00_bglt) BgL_nodez00_3675))))->BgL_variablez00);
				if (
					(((obj_t) BgL_varz00_4136) ==
						BGl_za2nozd2alphatiza7ezd2closureza2za7zzast_alphatiza7eza7))
					{	/* Ast/alphatize.scm 157 */
						return
							((BgL_nodez00_bglt) ((BgL_closurez00_bglt) BgL_nodez00_3675));
					}
				else
					{	/* Ast/alphatize.scm 159 */
						obj_t BgL_alphaz00_4137;

						BgL_alphaz00_4137 =
							(((BgL_variablez00_bglt) COBJECT(BgL_varz00_4136))->
							BgL_fastzd2alphazd2);
						if ((BgL_alphaz00_4137 == BUNSPEC))
							{	/* Ast/alphatize.scm 161 */
								{	/* Ast/alphatize.scm 162 */
									obj_t BgL_arg1846z00_4138;

									BgL_arg1846z00_4138 =
										(((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt)
													((BgL_closurez00_bglt) BgL_nodez00_3675))))->
										BgL_locz00);
									BGl_usezd2variablez12zc0zzast_sexpz00(BgL_varz00_4136,
										BgL_arg1846z00_4138, CNST_TABLE_REF(7));
								}
								{	/* Ast/alphatize.scm 163 */
									BgL_closurez00_bglt BgL_new1158z00_4139;

									{	/* Ast/alphatize.scm 163 */
										BgL_closurez00_bglt BgL_new1162z00_4140;

										BgL_new1162z00_4140 =
											((BgL_closurez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_closurez00_bgl))));
										{	/* Ast/alphatize.scm 163 */
											long BgL_arg1847z00_4141;

											BgL_arg1847z00_4141 =
												BGL_CLASS_NUM(BGl_closurez00zzast_nodez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1162z00_4140),
												BgL_arg1847z00_4141);
										}
										{	/* Ast/alphatize.scm 163 */
											BgL_objectz00_bglt BgL_tmpz00_6698;

											BgL_tmpz00_6698 =
												((BgL_objectz00_bglt) BgL_new1162z00_4140);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6698, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1162z00_4140);
										BgL_new1158z00_4139 = BgL_new1162z00_4140;
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1158z00_4139)))->
											BgL_locz00) =
										((obj_t)
											BGl_getzd2locationzd2zzast_alphatiza7eza7((
													(BgL_nodez00_bglt) ((BgL_closurez00_bglt)
														BgL_nodez00_3675)), BgL_locz00_3676)), BUNSPEC);
									((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_new1158z00_4139)))->BgL_typez00) =
										((BgL_typez00_bglt) (((BgL_nodez00_bglt)
													COBJECT(((BgL_nodez00_bglt) ((BgL_closurez00_bglt)
																BgL_nodez00_3675))))->BgL_typez00)), BUNSPEC);
									((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
														BgL_new1158z00_4139)))->BgL_variablez00) =
										((BgL_variablez00_bglt) (((BgL_varz00_bglt)
													COBJECT(((BgL_varz00_bglt) ((BgL_nodez00_bglt) (
																	(BgL_closurez00_bglt) BgL_nodez00_3675)))))->
												BgL_variablez00)), BUNSPEC);
									return ((BgL_nodez00_bglt) BgL_new1158z00_4139);
								}
							}
						else
							{	/* Ast/alphatize.scm 164 */
								bool_t BgL_test2305z00_6719;

								{	/* Ast/alphatize.scm 164 */
									obj_t BgL_classz00_4142;

									BgL_classz00_4142 = BGl_variablez00zzast_varz00;
									if (BGL_OBJECTP(BgL_alphaz00_4137))
										{	/* Ast/alphatize.scm 164 */
											BgL_objectz00_bglt BgL_arg1807z00_4143;

											BgL_arg1807z00_4143 =
												(BgL_objectz00_bglt) (BgL_alphaz00_4137);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Ast/alphatize.scm 164 */
													long BgL_idxz00_4144;

													BgL_idxz00_4144 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4143);
													BgL_test2305z00_6719 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_4144 + 1L)) == BgL_classz00_4142);
												}
											else
												{	/* Ast/alphatize.scm 164 */
													bool_t BgL_res2130z00_4147;

													{	/* Ast/alphatize.scm 164 */
														obj_t BgL_oclassz00_4148;

														{	/* Ast/alphatize.scm 164 */
															obj_t BgL_arg1815z00_4149;
															long BgL_arg1816z00_4150;

															BgL_arg1815z00_4149 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Ast/alphatize.scm 164 */
																long BgL_arg1817z00_4151;

																BgL_arg1817z00_4151 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4143);
																BgL_arg1816z00_4150 =
																	(BgL_arg1817z00_4151 - OBJECT_TYPE);
															}
															BgL_oclassz00_4148 =
																VECTOR_REF(BgL_arg1815z00_4149,
																BgL_arg1816z00_4150);
														}
														{	/* Ast/alphatize.scm 164 */
															bool_t BgL__ortest_1115z00_4152;

															BgL__ortest_1115z00_4152 =
																(BgL_classz00_4142 == BgL_oclassz00_4148);
															if (BgL__ortest_1115z00_4152)
																{	/* Ast/alphatize.scm 164 */
																	BgL_res2130z00_4147 =
																		BgL__ortest_1115z00_4152;
																}
															else
																{	/* Ast/alphatize.scm 164 */
																	long BgL_odepthz00_4153;

																	{	/* Ast/alphatize.scm 164 */
																		obj_t BgL_arg1804z00_4154;

																		BgL_arg1804z00_4154 = (BgL_oclassz00_4148);
																		BgL_odepthz00_4153 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_4154);
																	}
																	if ((1L < BgL_odepthz00_4153))
																		{	/* Ast/alphatize.scm 164 */
																			obj_t BgL_arg1802z00_4155;

																			{	/* Ast/alphatize.scm 164 */
																				obj_t BgL_arg1803z00_4156;

																				BgL_arg1803z00_4156 =
																					(BgL_oclassz00_4148);
																				BgL_arg1802z00_4155 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_4156, 1L);
																			}
																			BgL_res2130z00_4147 =
																				(BgL_arg1802z00_4155 ==
																				BgL_classz00_4142);
																		}
																	else
																		{	/* Ast/alphatize.scm 164 */
																			BgL_res2130z00_4147 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2305z00_6719 = BgL_res2130z00_4147;
												}
										}
									else
										{	/* Ast/alphatize.scm 164 */
											BgL_test2305z00_6719 = ((bool_t) 0);
										}
								}
								if (BgL_test2305z00_6719)
									{	/* Ast/alphatize.scm 164 */
										{	/* Ast/alphatize.scm 165 */
											obj_t BgL_arg1849z00_4157;

											BgL_arg1849z00_4157 =
												(((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt)
															((BgL_closurez00_bglt) BgL_nodez00_3675))))->
												BgL_locz00);
											BGl_usezd2variablez12zc0zzast_sexpz00((
													(BgL_variablez00_bglt) BgL_alphaz00_4137),
												BgL_arg1849z00_4157, CNST_TABLE_REF(7));
										}
										{	/* Ast/alphatize.scm 166 */
											BgL_closurez00_bglt BgL_new1163z00_4158;

											{	/* Ast/alphatize.scm 167 */
												BgL_closurez00_bglt BgL_new1166z00_4159;

												BgL_new1166z00_4159 =
													((BgL_closurez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																BgL_closurez00_bgl))));
												{	/* Ast/alphatize.scm 167 */
													long BgL_arg1850z00_4160;

													BgL_arg1850z00_4160 =
														BGL_CLASS_NUM(BGl_closurez00zzast_nodez00);
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt) BgL_new1166z00_4159),
														BgL_arg1850z00_4160);
												}
												{	/* Ast/alphatize.scm 167 */
													BgL_objectz00_bglt BgL_tmpz00_6752;

													BgL_tmpz00_6752 =
														((BgL_objectz00_bglt) BgL_new1166z00_4159);
													BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6752, BFALSE);
												}
												((BgL_objectz00_bglt) BgL_new1166z00_4159);
												BgL_new1163z00_4158 = BgL_new1166z00_4159;
											}
											((((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt) BgL_new1163z00_4158)))->
													BgL_locz00) =
												((obj_t)
													BGl_getzd2locationzd2zzast_alphatiza7eza7((
															(BgL_nodez00_bglt) ((BgL_closurez00_bglt)
																BgL_nodez00_3675)), BgL_locz00_3676)), BUNSPEC);
											((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																BgL_new1163z00_4158)))->BgL_typez00) =
												((BgL_typez00_bglt) (((BgL_nodez00_bglt)
															COBJECT(((BgL_nodez00_bglt) ((BgL_closurez00_bglt)
																		BgL_nodez00_3675))))->BgL_typez00)),
												BUNSPEC);
											((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																BgL_new1163z00_4158)))->BgL_variablez00) =
												((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
														BgL_alphaz00_4137)), BUNSPEC);
											return ((BgL_nodez00_bglt) BgL_new1163z00_4158);
										}
									}
								else
									{	/* Ast/alphatize.scm 172 */
										obj_t BgL_arg1851z00_4161;

										BgL_arg1851z00_4161 =
											BGl_shapez00zztools_shapez00(
											((obj_t) ((BgL_closurez00_bglt) BgL_nodez00_3675)));
										return
											((BgL_nodez00_bglt)
											BGl_internalzd2errorzd2zztools_errorz00
											(BGl_string2189z00zzast_alphatiza7eza7,
												BGl_string2191z00zzast_alphatiza7eza7,
												BgL_arg1851z00_4161));
									}
							}
					}
			}
		}

	}



/* &do-alphatize-var1610 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2var1610zc5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3677, obj_t BgL_nodez00_3678, obj_t BgL_locz00_3679)
	{
		{	/* Ast/alphatize.scm 122 */
			{	/* Ast/alphatize.scm 123 */
				BgL_variablez00_bglt BgL_varz00_4163;

				BgL_varz00_4163 =
					(((BgL_varz00_bglt) COBJECT(
							((BgL_varz00_bglt) BgL_nodez00_3678)))->BgL_variablez00);
				{	/* Ast/alphatize.scm 123 */
					obj_t BgL_alphaz00_4164;

					BgL_alphaz00_4164 =
						(((BgL_variablez00_bglt) COBJECT(BgL_varz00_4163))->
						BgL_fastzd2alphazd2);
					{	/* Ast/alphatize.scm 124 */

						if ((BgL_alphaz00_4164 == BUNSPEC))
							{	/* Ast/alphatize.scm 126 */
								{	/* Ast/alphatize.scm 127 */
									obj_t BgL_arg1812z00_4165;

									BgL_arg1812z00_4165 =
										(((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt)
													((BgL_varz00_bglt) BgL_nodez00_3678))))->BgL_locz00);
									BGl_usezd2variablez12zc0zzast_sexpz00(BgL_varz00_4163,
										BgL_arg1812z00_4165, CNST_TABLE_REF(7));
								}
								{	/* Ast/alphatize.scm 128 */
									BgL_refz00_bglt BgL_new1129z00_4166;

									{	/* Ast/alphatize.scm 128 */
										BgL_refz00_bglt BgL_new1133z00_4167;

										BgL_new1133z00_4167 =
											((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_refz00_bgl))));
										{	/* Ast/alphatize.scm 128 */
											long BgL_arg1820z00_4168;

											BgL_arg1820z00_4168 =
												BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1133z00_4167),
												BgL_arg1820z00_4168);
										}
										{	/* Ast/alphatize.scm 128 */
											BgL_objectz00_bglt BgL_tmpz00_6789;

											BgL_tmpz00_6789 =
												((BgL_objectz00_bglt) BgL_new1133z00_4167);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6789, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1133z00_4167);
										BgL_new1129z00_4166 = BgL_new1133z00_4167;
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1129z00_4166)))->
											BgL_locz00) =
										((obj_t)
											BGl_getzd2locationzd2zzast_alphatiza7eza7((
													(BgL_nodez00_bglt) ((BgL_varz00_bglt)
														BgL_nodez00_3678)), BgL_locz00_3679)), BUNSPEC);
									((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_new1129z00_4166)))->BgL_typez00) =
										((BgL_typez00_bglt) (((BgL_nodez00_bglt)
													COBJECT(((BgL_nodez00_bglt) ((BgL_varz00_bglt)
																BgL_nodez00_3678))))->BgL_typez00)), BUNSPEC);
									((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
														BgL_new1129z00_4166)))->BgL_variablez00) =
										((BgL_variablez00_bglt) (((BgL_varz00_bglt)
													COBJECT(((BgL_varz00_bglt) ((BgL_nodez00_bglt) (
																	(BgL_varz00_bglt) BgL_nodez00_3678)))))->
												BgL_variablez00)), BUNSPEC);
									return ((BgL_nodez00_bglt) BgL_new1129z00_4166);
								}
							}
						else
							{	/* Ast/alphatize.scm 129 */
								bool_t BgL_test2311z00_6810;

								{	/* Ast/alphatize.scm 129 */
									obj_t BgL_classz00_4169;

									BgL_classz00_4169 = BGl_variablez00zzast_varz00;
									if (BGL_OBJECTP(BgL_alphaz00_4164))
										{	/* Ast/alphatize.scm 129 */
											BgL_objectz00_bglt BgL_arg1807z00_4170;

											BgL_arg1807z00_4170 =
												(BgL_objectz00_bglt) (BgL_alphaz00_4164);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Ast/alphatize.scm 129 */
													long BgL_idxz00_4171;

													BgL_idxz00_4171 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4170);
													BgL_test2311z00_6810 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_4171 + 1L)) == BgL_classz00_4169);
												}
											else
												{	/* Ast/alphatize.scm 129 */
													bool_t BgL_res2125z00_4174;

													{	/* Ast/alphatize.scm 129 */
														obj_t BgL_oclassz00_4175;

														{	/* Ast/alphatize.scm 129 */
															obj_t BgL_arg1815z00_4176;
															long BgL_arg1816z00_4177;

															BgL_arg1815z00_4176 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Ast/alphatize.scm 129 */
																long BgL_arg1817z00_4178;

																BgL_arg1817z00_4178 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4170);
																BgL_arg1816z00_4177 =
																	(BgL_arg1817z00_4178 - OBJECT_TYPE);
															}
															BgL_oclassz00_4175 =
																VECTOR_REF(BgL_arg1815z00_4176,
																BgL_arg1816z00_4177);
														}
														{	/* Ast/alphatize.scm 129 */
															bool_t BgL__ortest_1115z00_4179;

															BgL__ortest_1115z00_4179 =
																(BgL_classz00_4169 == BgL_oclassz00_4175);
															if (BgL__ortest_1115z00_4179)
																{	/* Ast/alphatize.scm 129 */
																	BgL_res2125z00_4174 =
																		BgL__ortest_1115z00_4179;
																}
															else
																{	/* Ast/alphatize.scm 129 */
																	long BgL_odepthz00_4180;

																	{	/* Ast/alphatize.scm 129 */
																		obj_t BgL_arg1804z00_4181;

																		BgL_arg1804z00_4181 = (BgL_oclassz00_4175);
																		BgL_odepthz00_4180 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_4181);
																	}
																	if ((1L < BgL_odepthz00_4180))
																		{	/* Ast/alphatize.scm 129 */
																			obj_t BgL_arg1802z00_4182;

																			{	/* Ast/alphatize.scm 129 */
																				obj_t BgL_arg1803z00_4183;

																				BgL_arg1803z00_4183 =
																					(BgL_oclassz00_4175);
																				BgL_arg1802z00_4182 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_4183, 1L);
																			}
																			BgL_res2125z00_4174 =
																				(BgL_arg1802z00_4182 ==
																				BgL_classz00_4169);
																		}
																	else
																		{	/* Ast/alphatize.scm 129 */
																			BgL_res2125z00_4174 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2311z00_6810 = BgL_res2125z00_4174;
												}
										}
									else
										{	/* Ast/alphatize.scm 129 */
											BgL_test2311z00_6810 = ((bool_t) 0);
										}
								}
								if (BgL_test2311z00_6810)
									{	/* Ast/alphatize.scm 129 */
										{	/* Ast/alphatize.scm 130 */
											obj_t BgL_arg1822z00_4184;

											BgL_arg1822z00_4184 =
												(((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt)
															((BgL_varz00_bglt) BgL_nodez00_3678))))->
												BgL_locz00);
											BGl_usezd2variablez12zc0zzast_sexpz00((
													(BgL_variablez00_bglt) BgL_alphaz00_4164),
												BgL_arg1822z00_4184, CNST_TABLE_REF(7));
										}
										{	/* Ast/alphatize.scm 131 */
											bool_t BgL_test2316z00_6839;

											{	/* Ast/alphatize.scm 131 */
												BgL_valuez00_bglt BgL_arg1834z00_4185;

												BgL_arg1834z00_4185 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt) BgL_alphaz00_4164)))->
													BgL_valuez00);
												{	/* Ast/alphatize.scm 131 */
													obj_t BgL_classz00_4186;

													BgL_classz00_4186 = BGl_funz00zzast_varz00;
													{	/* Ast/alphatize.scm 131 */
														BgL_objectz00_bglt BgL_arg1807z00_4187;

														{	/* Ast/alphatize.scm 131 */
															obj_t BgL_tmpz00_6842;

															BgL_tmpz00_6842 =
																((obj_t)
																((BgL_objectz00_bglt) BgL_arg1834z00_4185));
															BgL_arg1807z00_4187 =
																(BgL_objectz00_bglt) (BgL_tmpz00_6842);
														}
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Ast/alphatize.scm 131 */
																long BgL_idxz00_4188;

																BgL_idxz00_4188 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_4187);
																BgL_test2316z00_6839 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_4188 + 2L)) ==
																	BgL_classz00_4186);
															}
														else
															{	/* Ast/alphatize.scm 131 */
																bool_t BgL_res2126z00_4191;

																{	/* Ast/alphatize.scm 131 */
																	obj_t BgL_oclassz00_4192;

																	{	/* Ast/alphatize.scm 131 */
																		obj_t BgL_arg1815z00_4193;
																		long BgL_arg1816z00_4194;

																		BgL_arg1815z00_4193 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Ast/alphatize.scm 131 */
																			long BgL_arg1817z00_4195;

																			BgL_arg1817z00_4195 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_4187);
																			BgL_arg1816z00_4194 =
																				(BgL_arg1817z00_4195 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_4192 =
																			VECTOR_REF(BgL_arg1815z00_4193,
																			BgL_arg1816z00_4194);
																	}
																	{	/* Ast/alphatize.scm 131 */
																		bool_t BgL__ortest_1115z00_4196;

																		BgL__ortest_1115z00_4196 =
																			(BgL_classz00_4186 == BgL_oclassz00_4192);
																		if (BgL__ortest_1115z00_4196)
																			{	/* Ast/alphatize.scm 131 */
																				BgL_res2126z00_4191 =
																					BgL__ortest_1115z00_4196;
																			}
																		else
																			{	/* Ast/alphatize.scm 131 */
																				long BgL_odepthz00_4197;

																				{	/* Ast/alphatize.scm 131 */
																					obj_t BgL_arg1804z00_4198;

																					BgL_arg1804z00_4198 =
																						(BgL_oclassz00_4192);
																					BgL_odepthz00_4197 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_4198);
																				}
																				if ((2L < BgL_odepthz00_4197))
																					{	/* Ast/alphatize.scm 131 */
																						obj_t BgL_arg1802z00_4199;

																						{	/* Ast/alphatize.scm 131 */
																							obj_t BgL_arg1803z00_4200;

																							BgL_arg1803z00_4200 =
																								(BgL_oclassz00_4192);
																							BgL_arg1802z00_4199 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_4200, 2L);
																						}
																						BgL_res2126z00_4191 =
																							(BgL_arg1802z00_4199 ==
																							BgL_classz00_4186);
																					}
																				else
																					{	/* Ast/alphatize.scm 131 */
																						BgL_res2126z00_4191 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2316z00_6839 = BgL_res2126z00_4191;
															}
													}
												}
											}
											if (BgL_test2316z00_6839)
												{	/* Ast/alphatize.scm 132 */
													BgL_closurez00_bglt BgL_new1135z00_4201;

													{	/* Ast/alphatize.scm 133 */
														BgL_closurez00_bglt BgL_new1134z00_4202;

														BgL_new1134z00_4202 =
															((BgL_closurez00_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_closurez00_bgl))));
														{	/* Ast/alphatize.scm 133 */
															long BgL_arg1832z00_4203;

															BgL_arg1832z00_4203 =
																BGL_CLASS_NUM(BGl_closurez00zzast_nodez00);
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_new1134z00_4202),
																BgL_arg1832z00_4203);
														}
														{	/* Ast/alphatize.scm 133 */
															BgL_objectz00_bglt BgL_tmpz00_6869;

															BgL_tmpz00_6869 =
																((BgL_objectz00_bglt) BgL_new1134z00_4202);
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6869, BFALSE);
														}
														((BgL_objectz00_bglt) BgL_new1134z00_4202);
														BgL_new1135z00_4201 = BgL_new1134z00_4202;
													}
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_new1135z00_4201)))->
															BgL_locz00) =
														((obj_t)
															BGl_getzd2locationzd2zzast_alphatiza7eza7((
																	(BgL_nodez00_bglt) ((BgL_varz00_bglt)
																		BgL_nodez00_3678)), BgL_locz00_3679)),
														BUNSPEC);
													{
														BgL_typez00_bglt BgL_auxz00_6878;

														{	/* Ast/alphatize.scm 134 */
															BgL_typez00_bglt BgL_arg1831z00_4204;

															BgL_arg1831z00_4204 =
																(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			BgL_alphaz00_4164)))->BgL_typez00);
															BgL_auxz00_6878 =
																BGl_strictzd2nodezd2typez00zzast_nodez00((
																	(BgL_typez00_bglt)
																	BGl_za2procedureza2z00zztype_cachez00),
																BgL_arg1831z00_4204);
														}
														((((BgL_nodez00_bglt) COBJECT(
																		((BgL_nodez00_bglt) BgL_new1135z00_4201)))->
																BgL_typez00) =
															((BgL_typez00_bglt) BgL_auxz00_6878), BUNSPEC);
													}
													((((BgL_varz00_bglt) COBJECT(
																	((BgL_varz00_bglt) BgL_new1135z00_4201)))->
															BgL_variablez00) =
														((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
																BgL_alphaz00_4164)), BUNSPEC);
													return ((BgL_nodez00_bglt) BgL_new1135z00_4201);
												}
											else
												{	/* Ast/alphatize.scm 136 */
													BgL_refz00_bglt BgL_new1136z00_4205;

													{	/* Ast/alphatize.scm 137 */
														BgL_refz00_bglt BgL_new1139z00_4206;

														BgL_new1139z00_4206 =
															((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_refz00_bgl))));
														{	/* Ast/alphatize.scm 137 */
															long BgL_arg1833z00_4207;

															BgL_arg1833z00_4207 =
																BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_new1139z00_4206),
																BgL_arg1833z00_4207);
														}
														{	/* Ast/alphatize.scm 137 */
															BgL_objectz00_bglt BgL_tmpz00_6893;

															BgL_tmpz00_6893 =
																((BgL_objectz00_bglt) BgL_new1139z00_4206);
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6893, BFALSE);
														}
														((BgL_objectz00_bglt) BgL_new1139z00_4206);
														BgL_new1136z00_4205 = BgL_new1139z00_4206;
													}
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_new1136z00_4205)))->
															BgL_locz00) =
														((obj_t)
															BGl_getzd2locationzd2zzast_alphatiza7eza7((
																	(BgL_nodez00_bglt) ((BgL_varz00_bglt)
																		BgL_nodez00_3678)), BgL_locz00_3679)),
														BUNSPEC);
													((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																		BgL_new1136z00_4205)))->BgL_typez00) =
														((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																	COBJECT(((BgL_nodez00_bglt) ((BgL_varz00_bglt)
																				BgL_nodez00_3678))))->BgL_typez00)),
														BUNSPEC);
													((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																		BgL_new1136z00_4205)))->BgL_variablez00) =
														((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
																BgL_alphaz00_4164)), BUNSPEC);
													return ((BgL_nodez00_bglt) BgL_new1136z00_4205);
												}
										}
									}
								else
									{	/* Ast/alphatize.scm 139 */
										bool_t BgL_test2320z00_6911;

										{	/* Ast/alphatize.scm 139 */
											obj_t BgL_classz00_4208;

											BgL_classz00_4208 = BGl_literalz00zzast_nodez00;
											if (BGL_OBJECTP(BgL_alphaz00_4164))
												{	/* Ast/alphatize.scm 139 */
													BgL_objectz00_bglt BgL_arg1807z00_4209;

													BgL_arg1807z00_4209 =
														(BgL_objectz00_bglt) (BgL_alphaz00_4164);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Ast/alphatize.scm 139 */
															long BgL_idxz00_4210;

															BgL_idxz00_4210 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4209);
															BgL_test2320z00_6911 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_4210 + 3L)) == BgL_classz00_4208);
														}
													else
														{	/* Ast/alphatize.scm 139 */
															bool_t BgL_res2127z00_4213;

															{	/* Ast/alphatize.scm 139 */
																obj_t BgL_oclassz00_4214;

																{	/* Ast/alphatize.scm 139 */
																	obj_t BgL_arg1815z00_4215;
																	long BgL_arg1816z00_4216;

																	BgL_arg1815z00_4215 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Ast/alphatize.scm 139 */
																		long BgL_arg1817z00_4217;

																		BgL_arg1817z00_4217 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4209);
																		BgL_arg1816z00_4216 =
																			(BgL_arg1817z00_4217 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_4214 =
																		VECTOR_REF(BgL_arg1815z00_4215,
																		BgL_arg1816z00_4216);
																}
																{	/* Ast/alphatize.scm 139 */
																	bool_t BgL__ortest_1115z00_4218;

																	BgL__ortest_1115z00_4218 =
																		(BgL_classz00_4208 == BgL_oclassz00_4214);
																	if (BgL__ortest_1115z00_4218)
																		{	/* Ast/alphatize.scm 139 */
																			BgL_res2127z00_4213 =
																				BgL__ortest_1115z00_4218;
																		}
																	else
																		{	/* Ast/alphatize.scm 139 */
																			long BgL_odepthz00_4219;

																			{	/* Ast/alphatize.scm 139 */
																				obj_t BgL_arg1804z00_4220;

																				BgL_arg1804z00_4220 =
																					(BgL_oclassz00_4214);
																				BgL_odepthz00_4219 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_4220);
																			}
																			if ((3L < BgL_odepthz00_4219))
																				{	/* Ast/alphatize.scm 139 */
																					obj_t BgL_arg1802z00_4221;

																					{	/* Ast/alphatize.scm 139 */
																						obj_t BgL_arg1803z00_4222;

																						BgL_arg1803z00_4222 =
																							(BgL_oclassz00_4214);
																						BgL_arg1802z00_4221 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_4222, 3L);
																					}
																					BgL_res2127z00_4213 =
																						(BgL_arg1802z00_4221 ==
																						BgL_classz00_4208);
																				}
																			else
																				{	/* Ast/alphatize.scm 139 */
																					BgL_res2127z00_4213 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2320z00_6911 = BgL_res2127z00_4213;
														}
												}
											else
												{	/* Ast/alphatize.scm 139 */
													BgL_test2320z00_6911 = ((bool_t) 0);
												}
										}
										if (BgL_test2320z00_6911)
											{	/* Ast/alphatize.scm 140 */
												BgL_literalz00_bglt BgL_new1140z00_4223;

												{	/* Ast/alphatize.scm 140 */
													BgL_literalz00_bglt BgL_new1144z00_4224;

													BgL_new1144z00_4224 =
														((BgL_literalz00_bglt)
														BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_literalz00_bgl))));
													{	/* Ast/alphatize.scm 140 */
														long BgL_arg1836z00_4225;

														BgL_arg1836z00_4225 =
															BGL_CLASS_NUM(BGl_literalz00zzast_nodez00);
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1144z00_4224),
															BgL_arg1836z00_4225);
													}
													{	/* Ast/alphatize.scm 140 */
														BgL_objectz00_bglt BgL_tmpz00_6938;

														BgL_tmpz00_6938 =
															((BgL_objectz00_bglt) BgL_new1144z00_4224);
														BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6938, BFALSE);
													}
													((BgL_objectz00_bglt) BgL_new1144z00_4224);
													BgL_new1140z00_4223 = BgL_new1144z00_4224;
												}
												((((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt) BgL_new1140z00_4223)))->
														BgL_locz00) =
													((obj_t) (((BgL_nodez00_bglt)
																COBJECT(((BgL_nodez00_bglt)
																		BgL_alphaz00_4164)))->BgL_locz00)),
													BUNSPEC);
												((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																	BgL_new1140z00_4223)))->BgL_typez00) =
													((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																COBJECT(((BgL_nodez00_bglt)
																		BgL_alphaz00_4164)))->BgL_typez00)),
													BUNSPEC);
												((((BgL_atomz00_bglt) COBJECT(((BgL_atomz00_bglt)
																	BgL_new1140z00_4223)))->BgL_valuez00) =
													((obj_t) (((BgL_atomz00_bglt)
																COBJECT(((BgL_atomz00_bglt) ((BgL_nodez00_bglt)
																			BgL_alphaz00_4164))))->BgL_valuez00)),
													BUNSPEC);
												return ((BgL_nodez00_bglt) BgL_new1140z00_4223);
											}
										else
											{	/* Ast/alphatize.scm 141 */
												bool_t BgL_test2325z00_6956;

												{	/* Ast/alphatize.scm 141 */
													obj_t BgL_classz00_4226;

													BgL_classz00_4226 = BGl_patchz00zzast_nodez00;
													if (BGL_OBJECTP(BgL_alphaz00_4164))
														{	/* Ast/alphatize.scm 141 */
															BgL_objectz00_bglt BgL_arg1807z00_4227;

															BgL_arg1807z00_4227 =
																(BgL_objectz00_bglt) (BgL_alphaz00_4164);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Ast/alphatize.scm 141 */
																	long BgL_idxz00_4228;

																	BgL_idxz00_4228 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_4227);
																	BgL_test2325z00_6956 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_4228 + 3L)) ==
																		BgL_classz00_4226);
																}
															else
																{	/* Ast/alphatize.scm 141 */
																	bool_t BgL_res2128z00_4231;

																	{	/* Ast/alphatize.scm 141 */
																		obj_t BgL_oclassz00_4232;

																		{	/* Ast/alphatize.scm 141 */
																			obj_t BgL_arg1815z00_4233;
																			long BgL_arg1816z00_4234;

																			BgL_arg1815z00_4233 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Ast/alphatize.scm 141 */
																				long BgL_arg1817z00_4235;

																				BgL_arg1817z00_4235 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_4227);
																				BgL_arg1816z00_4234 =
																					(BgL_arg1817z00_4235 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_4232 =
																				VECTOR_REF(BgL_arg1815z00_4233,
																				BgL_arg1816z00_4234);
																		}
																		{	/* Ast/alphatize.scm 141 */
																			bool_t BgL__ortest_1115z00_4236;

																			BgL__ortest_1115z00_4236 =
																				(BgL_classz00_4226 ==
																				BgL_oclassz00_4232);
																			if (BgL__ortest_1115z00_4236)
																				{	/* Ast/alphatize.scm 141 */
																					BgL_res2128z00_4231 =
																						BgL__ortest_1115z00_4236;
																				}
																			else
																				{	/* Ast/alphatize.scm 141 */
																					long BgL_odepthz00_4237;

																					{	/* Ast/alphatize.scm 141 */
																						obj_t BgL_arg1804z00_4238;

																						BgL_arg1804z00_4238 =
																							(BgL_oclassz00_4232);
																						BgL_odepthz00_4237 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_4238);
																					}
																					if ((3L < BgL_odepthz00_4237))
																						{	/* Ast/alphatize.scm 141 */
																							obj_t BgL_arg1802z00_4239;

																							{	/* Ast/alphatize.scm 141 */
																								obj_t BgL_arg1803z00_4240;

																								BgL_arg1803z00_4240 =
																									(BgL_oclassz00_4232);
																								BgL_arg1802z00_4239 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_4240, 3L);
																							}
																							BgL_res2128z00_4231 =
																								(BgL_arg1802z00_4239 ==
																								BgL_classz00_4226);
																						}
																					else
																						{	/* Ast/alphatize.scm 141 */
																							BgL_res2128z00_4231 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2325z00_6956 = BgL_res2128z00_4231;
																}
														}
													else
														{	/* Ast/alphatize.scm 141 */
															BgL_test2325z00_6956 = ((bool_t) 0);
														}
												}
												if (BgL_test2325z00_6956)
													{	/* Ast/alphatize.scm 143 */
														BgL_patchz00_bglt BgL_new1146z00_4241;

														{	/* Ast/alphatize.scm 143 */
															BgL_patchz00_bglt BgL_new1152z00_4242;

															BgL_new1152z00_4242 =
																((BgL_patchz00_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_patchz00_bgl))));
															{	/* Ast/alphatize.scm 143 */
																long BgL_arg1839z00_4243;

																BgL_arg1839z00_4243 =
																	BGL_CLASS_NUM(BGl_patchz00zzast_nodez00);
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1152z00_4242),
																	BgL_arg1839z00_4243);
															}
															{	/* Ast/alphatize.scm 143 */
																BgL_objectz00_bglt BgL_tmpz00_6983;

																BgL_tmpz00_6983 =
																	((BgL_objectz00_bglt) BgL_new1152z00_4242);
																BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6983,
																	BFALSE);
															}
															((BgL_objectz00_bglt) BgL_new1152z00_4242);
															BgL_new1146z00_4241 = BgL_new1152z00_4242;
														}
														((((BgL_nodez00_bglt) COBJECT(
																		((BgL_nodez00_bglt) BgL_new1146z00_4241)))->
																BgL_locz00) =
															((obj_t) (((BgL_nodez00_bglt)
																		COBJECT(((BgL_nodez00_bglt)
																				BgL_alphaz00_4164)))->BgL_locz00)),
															BUNSPEC);
														((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																			BgL_new1146z00_4241)))->BgL_typez00) =
															((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																		COBJECT(((BgL_nodez00_bglt)
																				BgL_alphaz00_4164)))->BgL_typez00)),
															BUNSPEC);
														((((BgL_atomz00_bglt) COBJECT(((BgL_atomz00_bglt)
																			BgL_new1146z00_4241)))->BgL_valuez00) =
															((obj_t) (((BgL_atomz00_bglt)
																		COBJECT(((BgL_atomz00_bglt) (
																					(BgL_nodez00_bglt)
																					BgL_alphaz00_4164))))->BgL_valuez00)),
															BUNSPEC);
														{
															BgL_varz00_bglt BgL_auxz00_7000;

															{	/* Ast/alphatize.scm 144 */
																BgL_varz00_bglt BgL_arg1838z00_4244;

																BgL_arg1838z00_4244 =
																	(((BgL_patchz00_bglt) COBJECT(
																			((BgL_patchz00_bglt)
																				BgL_alphaz00_4164)))->BgL_refz00);
																BgL_auxz00_7000 =
																	((BgL_varz00_bglt)
																	BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7((
																			(BgL_nodez00_bglt) BgL_arg1838z00_4244),
																		BgL_locz00_3679));
															}
															((((BgL_patchz00_bglt)
																		COBJECT(BgL_new1146z00_4241))->BgL_refz00) =
																((BgL_varz00_bglt) BgL_auxz00_7000), BUNSPEC);
														}
														((((BgL_patchz00_bglt)
																	COBJECT(BgL_new1146z00_4241))->BgL_indexz00) =
															((long) (((BgL_patchz00_bglt)
																		COBJECT(((BgL_patchz00_bglt) (
																					(BgL_nodez00_bglt)
																					BgL_alphaz00_4164))))->BgL_indexz00)),
															BUNSPEC);
														((((BgL_patchz00_bglt)
																	COBJECT(BgL_new1146z00_4241))->
																BgL_patchidz00) =
															((obj_t) (((BgL_patchz00_bglt)
																		COBJECT(((BgL_patchz00_bglt) (
																					(BgL_nodez00_bglt)
																					BgL_alphaz00_4164))))->
																	BgL_patchidz00)), BUNSPEC);
														return ((BgL_nodez00_bglt) BgL_new1146z00_4241);
													}
												else
													{	/* Ast/alphatize.scm 145 */
														bool_t BgL_test2330z00_7016;

														{	/* Ast/alphatize.scm 145 */
															obj_t BgL_classz00_4245;

															BgL_classz00_4245 = BGl_kwotez00zzast_nodez00;
															if (BGL_OBJECTP(BgL_alphaz00_4164))
																{	/* Ast/alphatize.scm 145 */
																	BgL_objectz00_bglt BgL_arg1807z00_4246;

																	BgL_arg1807z00_4246 =
																		(BgL_objectz00_bglt) (BgL_alphaz00_4164);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Ast/alphatize.scm 145 */
																			long BgL_idxz00_4247;

																			BgL_idxz00_4247 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_4246);
																			BgL_test2330z00_7016 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_4247 + 2L)) ==
																				BgL_classz00_4245);
																		}
																	else
																		{	/* Ast/alphatize.scm 145 */
																			bool_t BgL_res2129z00_4250;

																			{	/* Ast/alphatize.scm 145 */
																				obj_t BgL_oclassz00_4251;

																				{	/* Ast/alphatize.scm 145 */
																					obj_t BgL_arg1815z00_4252;
																					long BgL_arg1816z00_4253;

																					BgL_arg1815z00_4252 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Ast/alphatize.scm 145 */
																						long BgL_arg1817z00_4254;

																						BgL_arg1817z00_4254 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_4246);
																						BgL_arg1816z00_4253 =
																							(BgL_arg1817z00_4254 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_4251 =
																						VECTOR_REF(BgL_arg1815z00_4252,
																						BgL_arg1816z00_4253);
																				}
																				{	/* Ast/alphatize.scm 145 */
																					bool_t BgL__ortest_1115z00_4255;

																					BgL__ortest_1115z00_4255 =
																						(BgL_classz00_4245 ==
																						BgL_oclassz00_4251);
																					if (BgL__ortest_1115z00_4255)
																						{	/* Ast/alphatize.scm 145 */
																							BgL_res2129z00_4250 =
																								BgL__ortest_1115z00_4255;
																						}
																					else
																						{	/* Ast/alphatize.scm 145 */
																							long BgL_odepthz00_4256;

																							{	/* Ast/alphatize.scm 145 */
																								obj_t BgL_arg1804z00_4257;

																								BgL_arg1804z00_4257 =
																									(BgL_oclassz00_4251);
																								BgL_odepthz00_4256 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_4257);
																							}
																							if ((2L < BgL_odepthz00_4256))
																								{	/* Ast/alphatize.scm 145 */
																									obj_t BgL_arg1802z00_4258;

																									{	/* Ast/alphatize.scm 145 */
																										obj_t BgL_arg1803z00_4259;

																										BgL_arg1803z00_4259 =
																											(BgL_oclassz00_4251);
																										BgL_arg1802z00_4258 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_4259, 2L);
																									}
																									BgL_res2129z00_4250 =
																										(BgL_arg1802z00_4258 ==
																										BgL_classz00_4245);
																								}
																							else
																								{	/* Ast/alphatize.scm 145 */
																									BgL_res2129z00_4250 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test2330z00_7016 =
																				BgL_res2129z00_4250;
																		}
																}
															else
																{	/* Ast/alphatize.scm 145 */
																	BgL_test2330z00_7016 = ((bool_t) 0);
																}
														}
														if (BgL_test2330z00_7016)
															{	/* Ast/alphatize.scm 146 */
																BgL_kwotez00_bglt BgL_new1153z00_4260;

																{	/* Ast/alphatize.scm 146 */
																	BgL_kwotez00_bglt BgL_new1157z00_4261;

																	BgL_new1157z00_4261 =
																		((BgL_kwotez00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_kwotez00_bgl))));
																	{	/* Ast/alphatize.scm 146 */
																		long BgL_arg1842z00_4262;

																		BgL_arg1842z00_4262 =
																			BGL_CLASS_NUM(BGl_kwotez00zzast_nodez00);
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1157z00_4261),
																			BgL_arg1842z00_4262);
																	}
																	{	/* Ast/alphatize.scm 146 */
																		BgL_objectz00_bglt BgL_tmpz00_7043;

																		BgL_tmpz00_7043 =
																			((BgL_objectz00_bglt)
																			BgL_new1157z00_4261);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7043,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1157z00_4261);
																	BgL_new1153z00_4260 = BgL_new1157z00_4261;
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1153z00_4260)))->BgL_locz00) =
																	((obj_t) (((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt)
																						BgL_alphaz00_4164)))->BgL_locz00)),
																	BUNSPEC);
																((((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt)
																					BgL_new1153z00_4260)))->BgL_typez00) =
																	((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt)
																						BgL_alphaz00_4164)))->BgL_typez00)),
																	BUNSPEC);
																((((BgL_kwotez00_bglt)
																			COBJECT(BgL_new1153z00_4260))->
																		BgL_valuez00) =
																	((obj_t) (((BgL_kwotez00_bglt)
																				COBJECT(((BgL_kwotez00_bglt) (
																							(BgL_nodez00_bglt)
																							BgL_alphaz00_4164))))->
																			BgL_valuez00)), BUNSPEC);
																return ((BgL_nodez00_bglt) BgL_new1153z00_4260);
															}
														else
															{	/* Ast/alphatize.scm 150 */
																obj_t BgL_arg1843z00_4263;

																{	/* Ast/alphatize.scm 150 */
																	obj_t BgL_arg1844z00_4264;
																	obj_t BgL_arg1845z00_4265;

																	BgL_arg1844z00_4264 =
																		BGl_shapez00zztools_shapez00(
																		((obj_t)
																			((BgL_varz00_bglt) BgL_nodez00_3678)));
																	BgL_arg1845z00_4265 =
																		BGl_shapez00zztools_shapez00
																		(BgL_alphaz00_4164);
																	BgL_arg1843z00_4263 =
																		MAKE_YOUNG_PAIR(BgL_arg1844z00_4264,
																		BgL_arg1845z00_4265);
																}
																return
																	((BgL_nodez00_bglt)
																	BGl_internalzd2errorzd2zztools_errorz00
																	(BGl_string2189z00zzast_alphatiza7eza7,
																		BGl_string2192z00zzast_alphatiza7eza7,
																		BgL_arg1843z00_4263));
															}
													}
											}
									}
							}
					}
				}
			}
		}

	}



/* &do-alphatize-patch1608 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2patch1608zc5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3680, obj_t BgL_nodez00_3681, obj_t BgL_locz00_3682)
	{
		{	/* Ast/alphatize.scm 112 */
			{	/* Ast/alphatize.scm 114 */
				BgL_patchz00_bglt BgL_new1122z00_4267;

				{	/* Ast/alphatize.scm 117 */
					BgL_patchz00_bglt BgL_new1127z00_4268;

					BgL_new1127z00_4268 =
						((BgL_patchz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_patchz00_bgl))));
					{	/* Ast/alphatize.scm 117 */
						long BgL_arg1808z00_4269;

						BgL_arg1808z00_4269 = BGL_CLASS_NUM(BGl_patchz00zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1127z00_4268), BgL_arg1808z00_4269);
					}
					{	/* Ast/alphatize.scm 117 */
						BgL_objectz00_bglt BgL_tmpz00_7071;

						BgL_tmpz00_7071 = ((BgL_objectz00_bglt) BgL_new1127z00_4268);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7071, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1127z00_4268);
					BgL_new1122z00_4267 = BgL_new1127z00_4268;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1122z00_4267)))->BgL_locz00) =
					((obj_t)
						BGl_getzd2locationzd2zzast_alphatiza7eza7(((BgL_nodez00_bglt) (
									(BgL_patchz00_bglt) BgL_nodez00_3681)), BgL_locz00_3682)),
					BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1122z00_4267)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt)
								COBJECT(((BgL_nodez00_bglt) ((BgL_patchz00_bglt)
											BgL_nodez00_3681))))->BgL_typez00)), BUNSPEC);
				{
					obj_t BgL_auxz00_7085;

					{	/* Ast/alphatize.scm 116 */
						obj_t BgL_arg1805z00_4270;

						BgL_arg1805z00_4270 =
							(((BgL_atomz00_bglt) COBJECT(
									((BgL_atomz00_bglt)
										((BgL_patchz00_bglt) BgL_nodez00_3681))))->BgL_valuez00);
						BgL_auxz00_7085 =
							((obj_t)
							BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(
								((BgL_nodez00_bglt) BgL_arg1805z00_4270), BgL_locz00_3682));
					}
					((((BgL_atomz00_bglt) COBJECT(
									((BgL_atomz00_bglt) BgL_new1122z00_4267)))->BgL_valuez00) =
						((obj_t) BgL_auxz00_7085), BUNSPEC);
				}
				{
					BgL_varz00_bglt BgL_auxz00_7094;

					{	/* Ast/alphatize.scm 115 */
						BgL_varz00_bglt BgL_arg1806z00_4271;

						BgL_arg1806z00_4271 =
							(((BgL_patchz00_bglt) COBJECT(
									((BgL_patchz00_bglt) BgL_nodez00_3681)))->BgL_refz00);
						BgL_auxz00_7094 =
							((BgL_varz00_bglt)
							BGl_dozd2alphatiza7ez75zzast_alphatiza7eza7(
								((BgL_nodez00_bglt) BgL_arg1806z00_4271), BgL_locz00_3682));
					}
					((((BgL_patchz00_bglt) COBJECT(BgL_new1122z00_4267))->BgL_refz00) =
						((BgL_varz00_bglt) BgL_auxz00_7094), BUNSPEC);
				}
				((((BgL_patchz00_bglt) COBJECT(BgL_new1122z00_4267))->BgL_indexz00) =
					((long) (((BgL_patchz00_bglt)
								COBJECT(((BgL_patchz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_patchz00_bglt) BgL_nodez00_3681)))))->
							BgL_indexz00)), BUNSPEC);
				((((BgL_patchz00_bglt) COBJECT(BgL_new1122z00_4267))->BgL_patchidz00) =
					((obj_t) (((BgL_patchz00_bglt)
								COBJECT(((BgL_patchz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_patchz00_bglt) BgL_nodez00_3681)))))->
							BgL_patchidz00)), BUNSPEC);
				return ((BgL_nodez00_bglt) BgL_new1122z00_4267);
			}
		}

	}



/* &do-alphatize-literal1606 */
	BgL_nodez00_bglt
		BGl_z62dozd2alphatiza7ezd2literal1606zc5zzast_alphatiza7eza7(obj_t
		BgL_envz00_3683, obj_t BgL_nodez00_3684, obj_t BgL_locz00_3685)
	{
		{	/* Ast/alphatize.scm 105 */
			{	/* Ast/alphatize.scm 106 */
				BgL_literalz00_bglt BgL_new1116z00_4273;

				{	/* Ast/alphatize.scm 107 */
					BgL_literalz00_bglt BgL_new1120z00_4274;

					BgL_new1120z00_4274 =
						((BgL_literalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_literalz00_bgl))));
					{	/* Ast/alphatize.scm 107 */
						long BgL_arg1799z00_4275;

						BgL_arg1799z00_4275 = BGL_CLASS_NUM(BGl_literalz00zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1120z00_4274), BgL_arg1799z00_4275);
					}
					{	/* Ast/alphatize.scm 107 */
						BgL_objectz00_bglt BgL_tmpz00_7116;

						BgL_tmpz00_7116 = ((BgL_objectz00_bglt) BgL_new1120z00_4274);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7116, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1120z00_4274);
					BgL_new1116z00_4273 = BgL_new1120z00_4274;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1116z00_4273)))->BgL_locz00) =
					((obj_t)
						BGl_getzd2locationzd2zzast_alphatiza7eza7(((BgL_nodez00_bglt) (
									(BgL_literalz00_bglt) BgL_nodez00_3684)), BgL_locz00_3685)),
					BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1116z00_4273)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt)
								COBJECT(((BgL_nodez00_bglt) ((BgL_literalz00_bglt)
											BgL_nodez00_3684))))->BgL_typez00)), BUNSPEC);
				((((BgL_atomz00_bglt) COBJECT(((BgL_atomz00_bglt)
									BgL_new1116z00_4273)))->BgL_valuez00) =
					((obj_t) (((BgL_atomz00_bglt)
								COBJECT(((BgL_atomz00_bglt) ((BgL_nodez00_bglt) (
												(BgL_literalz00_bglt) BgL_nodez00_3684)))))->
							BgL_valuez00)), BUNSPEC);
				return ((BgL_nodez00_bglt) BgL_new1116z00_4273);
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_alphatiza7eza7(void)
	{
		{	/* Ast/alphatize.scm 14 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2193z00zzast_alphatiza7eza7));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2193z00zzast_alphatiza7eza7));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2193z00zzast_alphatiza7eza7));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2193z00zzast_alphatiza7eza7));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2193z00zzast_alphatiza7eza7));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2193z00zzast_alphatiza7eza7));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string2193z00zzast_alphatiza7eza7));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string2193z00zzast_alphatiza7eza7));
			BGl_modulezd2initializa7ationz75zzast_applyz00(277780830L,
				BSTRING_TO_STRING(BGl_string2193z00zzast_alphatiza7eza7));
			BGl_modulezd2initializa7ationz75zzast_appz00(449859288L,
				BSTRING_TO_STRING(BGl_string2193z00zzast_alphatiza7eza7));
			return
				BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string2193z00zzast_alphatiza7eza7));
		}

	}

#ifdef __cplusplus
}
#endif
