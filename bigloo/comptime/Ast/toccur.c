/*===========================================================================*/
/*   (Ast/toccur.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Ast/toccur.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_AST_TYPEzd2OCCURzd2_TYPE_DEFINITIONS
#define BGL_BgL_AST_TYPEzd2OCCURzd2_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_scnstz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_nodez00;
		obj_t BgL_classz00;
		obj_t BgL_locz00;
	}               *BgL_scnstz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_getfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_getfieldz00_bglt;

	typedef struct BgL_setfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_setfieldz00_bglt;

	typedef struct BgL_newz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_argszd2typezd2;
	}             *BgL_newz00_bglt;

	typedef struct BgL_vallocz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                *BgL_vallocz00_bglt;

	typedef struct BgL_vrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}              *BgL_vrefz00_bglt;

	typedef struct BgL_vsetz12z12_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}                 *BgL_vsetz12z12_bglt;

	typedef struct BgL_vlengthz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_ftypez00;
	}                 *BgL_vlengthz00_bglt;

	typedef struct BgL_instanceofz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_classz00;
	}                    *BgL_instanceofz00_bglt;

	typedef struct BgL_castzd2nullzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
	}                     *BgL_castzd2nullzd2_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_retblockz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                  *BgL_retblockz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;


#endif													// BGL_BgL_AST_TYPEzd2OCCURzd2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t
		BGl_z62occurzd2nodez12zd2setfield1295z70zzast_typezd2occurzd2(obj_t, obj_t);
	static obj_t
		BGl_z62occurzd2nodez12zd2vsetz121309z62zzast_typezd2occurzd2(obj_t, obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t
		BGl_z62occurzd2nodez12zd2instance1303z70zzast_typezd2occurzd2(obj_t, obj_t);
	static obj_t BGl_z62occurzd2nodez12zd2new1297z70zzast_typezd2occurzd2(obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzast_typezd2occurzd2 = BUNSPEC;
	static obj_t
		BGl_z62occurzd2nodez12zd2jumpzd2exzd21327z70zzast_typezd2occurzd2(obj_t,
		obj_t);
	static obj_t BGl_za2globalza2z00zzast_typezd2occurzd2 = BUNSPEC;
	extern obj_t BGl_newz00zzast_nodez00;
	static obj_t BGl_z62occurzd2nodez12za2zzast_typezd2occurzd2(obj_t, obj_t);
	extern obj_t BGl_vlengthz00zzast_nodez00;
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t
		BGl_z62occurzd2nodez12zd2letzd2fun1321za2zzast_typezd2occurzd2(obj_t,
		obj_t);
	static obj_t
		BGl_z62occurzd2nodez12zd2extern1291z70zzast_typezd2occurzd2(obj_t, obj_t);
	static obj_t BGl_z62occurzd2nodez121276za2zzast_typezd2occurzd2(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t BGl_toplevelzd2initzd2zzast_typezd2occurzd2(void);
	static obj_t
		BGl_z62typezd2incrementzd2globalz12z70zzast_typezd2occurzd2(obj_t, obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static obj_t
		BGl_z62occurzd2nodez12zd2castzd2nul1301za2zzast_typezd2occurzd2(obj_t,
		obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzast_typezd2occurzd2(void);
	extern obj_t BGl_getfieldz00zzast_nodez00;
	static obj_t BGl_objectzd2initzd2zzast_typezd2occurzd2(void);
	static obj_t BGl_z62occurzd2nodez12zd2var1279z70zzast_typezd2occurzd2(obj_t,
		obj_t);
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	extern obj_t BGl_typez00zztype_typez00;
	static obj_t BGl_z62occurzd2nodez12zd2setq1313z70zzast_typezd2occurzd2(obj_t,
		obj_t);
	static obj_t BGl_typezd2incrementzd2sfunz12z12zzast_typezd2occurzd2(obj_t);
	static obj_t BGl_z62occurzd2nodez12zd2fail1317z70zzast_typezd2occurzd2(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzast_typezd2occurzd2(void);
	extern obj_t
		BGl_typezd2occurrencezd2incrementz12z12zztype_typez00(BgL_typez00_bglt);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t BGl_z62occurzd2nodez12zd2app1285z70zzast_typezd2occurzd2(obj_t,
		obj_t);
	static obj_t BGl_z62occurzd2nodez12zd2cast1299z70zzast_typezd2occurzd2(obj_t,
		obj_t);
	static obj_t
		BGl_z62occurzd2nodez12zd2sequence1281z70zzast_typezd2occurzd2(obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern BgL_typez00_bglt BGl_getzd2typezd2zztype_typeofz00(BgL_nodez00_bglt,
		bool_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t
		BGl_z62occurzd2nodez12zd2letzd2var1323za2zzast_typezd2occurzd2(obj_t,
		obj_t);
	static obj_t BGl_z62occurzd2nodez12zd2sync1283z70zzast_typezd2occurzd2(obj_t,
		obj_t);
	static obj_t
		BGl_z62occurzd2nodez12zd2retblock1335z70zzast_typezd2occurzd2(obj_t, obj_t);
	static obj_t
		BGl_z62occurzd2nodez12zd2boxzd2ref1331za2zzast_typezd2occurzd2(obj_t,
		obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzast_typezd2occurzd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	static obj_t BGl_z62occurzd2nodez12zd2vref1307z70zzast_typezd2occurzd2(obj_t,
		obj_t);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_cfunz00zzast_varz00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t
		BGl_z62occurzd2nodez12zd2funcall1289z70zzast_typezd2occurzd2(obj_t, obj_t);
	static obj_t
		BGl_z62occurzd2nodez12zd2valloc1311z70zzast_typezd2occurzd2(obj_t, obj_t);
	extern obj_t BGl_vrefz00zzast_nodez00;
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzast_typezd2occurzd2(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzast_typezd2occurzd2(void);
	extern obj_t BGl_scnstz00zzast_varz00;
	static obj_t BGl_importedzd2moduleszd2initz00zzast_typezd2occurzd2(void);
	static obj_t BGl_gczd2rootszd2initz00zzast_typezd2occurzd2(void);
	extern obj_t BGl_vallocz00zzast_nodez00;
	extern obj_t BGl_retblockz00zzast_nodez00;
	extern obj_t BGl_instanceofz00zzast_nodez00;
	static bool_t BGl_occurzd2nodeza2z12z62zzast_typezd2occurzd2(obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	static obj_t BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(BgL_nodez00_bglt);
	extern obj_t BGl_setfieldz00zzast_nodez00;
	extern obj_t BGl_castzd2nullzd2zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_typezd2incrementzd2globalz12z12zzast_typezd2occurzd2
		(BgL_globalz00_bglt);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	static obj_t
		BGl_z62occurzd2nodez12zd2getfield1293z70zzast_typezd2occurzd2(obj_t, obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t
		BGl_z62occurzd2nodez12zd2switch1319z70zzast_typezd2occurzd2(obj_t, obj_t);
	BGL_IMPORT obj_t
		BGl_findzd2superzd2classzd2methodzd2zz__objectz00(BgL_objectz00_bglt, obj_t,
		obj_t);
	static obj_t
		BGl_z62occurzd2nodez12zd2vlength1305z70zzast_typezd2occurzd2(obj_t, obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static obj_t
		BGl_z62occurzd2nodez12zd2appzd2ly1287za2zzast_typezd2occurzd2(obj_t, obj_t);
	static obj_t
		BGl_z62occurzd2nodez12zd2setzd2exzd2i1325z70zzast_typezd2occurzd2(obj_t,
		obj_t);
	extern obj_t BGl_vsetz12z12zzast_nodez00;
	static obj_t
		BGl_z62occurzd2nodez12zd2conditio1315z70zzast_typezd2occurzd2(obj_t, obj_t);
	static obj_t
		BGl_z62occurzd2nodez12zd2makezd2box1329za2zzast_typezd2occurzd2(obj_t,
		obj_t);
	static obj_t
		BGl_z62occurzd2nodez12zd2boxzd2setz121333zb0zzast_typezd2occurzd2(obj_t,
		obj_t);
	static obj_t __cnst[1];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_typezd2incrementzd2globalz12zd2envzc0zzast_typezd2occurzd2,
		BgL_bgl_za762typeza7d2increm1902z00,
		BGl_z62typezd2incrementzd2globalz12z70zzast_typezd2occurzd2, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string1900z00zzast_typezd2occurzd2,
		BgL_bgl_string1900za700za7za7a1903za7, "done ", 5);
	      DEFINE_STRING(BGl_string1868z00zzast_typezd2occurzd2,
		BgL_bgl_string1868za700za7za7a1904za7, "occur-node!1276", 15);
	      DEFINE_STRING(BGl_string1870z00zzast_typezd2occurzd2,
		BgL_bgl_string1870za700za7za7a1905za7, "occur-node!", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1867z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71906za7,
		BGl_z62occurzd2nodez121276za2zzast_typezd2occurzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1869z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71907za7,
		BGl_z62occurzd2nodez12zd2var1279z70zzast_typezd2occurzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1871z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71908za7,
		BGl_z62occurzd2nodez12zd2sequence1281z70zzast_typezd2occurzd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1872z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71909za7,
		BGl_z62occurzd2nodez12zd2sync1283z70zzast_typezd2occurzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1873z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71910za7,
		BGl_z62occurzd2nodez12zd2app1285z70zzast_typezd2occurzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1874z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71911za7,
		BGl_z62occurzd2nodez12zd2appzd2ly1287za2zzast_typezd2occurzd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1875z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71912za7,
		BGl_z62occurzd2nodez12zd2funcall1289z70zzast_typezd2occurzd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1876z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71913za7,
		BGl_z62occurzd2nodez12zd2extern1291z70zzast_typezd2occurzd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1877z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71914za7,
		BGl_z62occurzd2nodez12zd2getfield1293z70zzast_typezd2occurzd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1878z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71915za7,
		BGl_z62occurzd2nodez12zd2setfield1295z70zzast_typezd2occurzd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1879z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71916za7,
		BGl_z62occurzd2nodez12zd2new1297z70zzast_typezd2occurzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1880z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71917za7,
		BGl_z62occurzd2nodez12zd2cast1299z70zzast_typezd2occurzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1881z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71918za7,
		BGl_z62occurzd2nodez12zd2castzd2nul1301za2zzast_typezd2occurzd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1882z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71919za7,
		BGl_z62occurzd2nodez12zd2instance1303z70zzast_typezd2occurzd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1883z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71920za7,
		BGl_z62occurzd2nodez12zd2vlength1305z70zzast_typezd2occurzd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1884z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71921za7,
		BGl_z62occurzd2nodez12zd2vref1307z70zzast_typezd2occurzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1885z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71922za7,
		BGl_z62occurzd2nodez12zd2vsetz121309z62zzast_typezd2occurzd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1886z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71923za7,
		BGl_z62occurzd2nodez12zd2valloc1311z70zzast_typezd2occurzd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1887z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71924za7,
		BGl_z62occurzd2nodez12zd2setq1313z70zzast_typezd2occurzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1888z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71925za7,
		BGl_z62occurzd2nodez12zd2conditio1315z70zzast_typezd2occurzd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1889z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71926za7,
		BGl_z62occurzd2nodez12zd2fail1317z70zzast_typezd2occurzd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1899z00zzast_typezd2occurzd2,
		BgL_bgl_string1899za700za7za7a1927za7, "ast_type-occur", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1890z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71928za7,
		BGl_z62occurzd2nodez12zd2switch1319z70zzast_typezd2occurzd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1891z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71929za7,
		BGl_z62occurzd2nodez12zd2letzd2fun1321za2zzast_typezd2occurzd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1892z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71930za7,
		BGl_z62occurzd2nodez12zd2letzd2var1323za2zzast_typezd2occurzd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1893z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71931za7,
		BGl_z62occurzd2nodez12zd2setzd2exzd2i1325z70zzast_typezd2occurzd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1894z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71932za7,
		BGl_z62occurzd2nodez12zd2jumpzd2exzd21327z70zzast_typezd2occurzd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1895z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71933za7,
		BGl_z62occurzd2nodez12zd2makezd2box1329za2zzast_typezd2occurzd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1896z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71934za7,
		BGl_z62occurzd2nodez12zd2boxzd2ref1331za2zzast_typezd2occurzd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1897z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71935za7,
		BGl_z62occurzd2nodez12zd2boxzd2setz121333zb0zzast_typezd2occurzd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1898z00zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71936za7,
		BGl_z62occurzd2nodez12zd2retblock1335z70zzast_typezd2occurzd2, 0L, BUNSPEC,
		1);
	     
		DEFINE_STATIC_BGL_GENERIC(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
		BgL_bgl_za762occurza7d2nodeza71937za7,
		BGl_z62occurzd2nodez12za2zzast_typezd2occurzd2, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzast_typezd2occurzd2));
		     ADD_ROOT((void *) (&BGl_za2globalza2z00zzast_typezd2occurzd2));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzast_typezd2occurzd2(long
		BgL_checksumz00_2838, char *BgL_fromz00_2839)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzast_typezd2occurzd2))
				{
					BGl_requirezd2initializa7ationz75zzast_typezd2occurzd2 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzast_typezd2occurzd2();
					BGl_libraryzd2moduleszd2initz00zzast_typezd2occurzd2();
					BGl_cnstzd2initzd2zzast_typezd2occurzd2();
					BGl_importedzd2moduleszd2initz00zzast_typezd2occurzd2();
					BGl_genericzd2initzd2zzast_typezd2occurzd2();
					BGl_methodzd2initzd2zzast_typezd2occurzd2();
					return BGl_toplevelzd2initzd2zzast_typezd2occurzd2();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzast_typezd2occurzd2(void)
	{
		{	/* Ast/toccur.scm 14 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "ast_type-occur");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "ast_type-occur");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"ast_type-occur");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"ast_type-occur");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "ast_type-occur");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"ast_type-occur");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"ast_type-occur");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"ast_type-occur");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"ast_type-occur");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzast_typezd2occurzd2(void)
	{
		{	/* Ast/toccur.scm 14 */
			{	/* Ast/toccur.scm 14 */
				obj_t BgL_cportz00_2690;

				{	/* Ast/toccur.scm 14 */
					obj_t BgL_stringz00_2697;

					BgL_stringz00_2697 = BGl_string1900z00zzast_typezd2occurzd2;
					{	/* Ast/toccur.scm 14 */
						obj_t BgL_startz00_2698;

						BgL_startz00_2698 = BINT(0L);
						{	/* Ast/toccur.scm 14 */
							obj_t BgL_endz00_2699;

							BgL_endz00_2699 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2697)));
							{	/* Ast/toccur.scm 14 */

								BgL_cportz00_2690 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2697, BgL_startz00_2698, BgL_endz00_2699);
				}}}}
				{
					long BgL_iz00_2691;

					BgL_iz00_2691 = 0L;
				BgL_loopz00_2692:
					if ((BgL_iz00_2691 == -1L))
						{	/* Ast/toccur.scm 14 */
							return BUNSPEC;
						}
					else
						{	/* Ast/toccur.scm 14 */
							{	/* Ast/toccur.scm 14 */
								obj_t BgL_arg1901z00_2693;

								{	/* Ast/toccur.scm 14 */

									{	/* Ast/toccur.scm 14 */
										obj_t BgL_locationz00_2695;

										BgL_locationz00_2695 = BBOOL(((bool_t) 0));
										{	/* Ast/toccur.scm 14 */

											BgL_arg1901z00_2693 =
												BGl_readz00zz__readerz00(BgL_cportz00_2690,
												BgL_locationz00_2695);
										}
									}
								}
								{	/* Ast/toccur.scm 14 */
									int BgL_tmpz00_2868;

									BgL_tmpz00_2868 = (int) (BgL_iz00_2691);
									CNST_TABLE_SET(BgL_tmpz00_2868, BgL_arg1901z00_2693);
							}}
							{	/* Ast/toccur.scm 14 */
								int BgL_auxz00_2696;

								BgL_auxz00_2696 = (int) ((BgL_iz00_2691 - 1L));
								{
									long BgL_iz00_2873;

									BgL_iz00_2873 = (long) (BgL_auxz00_2696);
									BgL_iz00_2691 = BgL_iz00_2873;
									goto BgL_loopz00_2692;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzast_typezd2occurzd2(void)
	{
		{	/* Ast/toccur.scm 14 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzast_typezd2occurzd2(void)
	{
		{	/* Ast/toccur.scm 14 */
			BGl_za2globalza2z00zzast_typezd2occurzd2 = BUNSPEC;
			return BUNSPEC;
		}

	}



/* type-increment-global! */
	BGL_EXPORTED_DEF obj_t
		BGl_typezd2incrementzd2globalz12z12zzast_typezd2occurzd2(BgL_globalz00_bglt
		BgL_globalz00_3)
	{
		{	/* Ast/toccur.scm 33 */
			{	/* Ast/toccur.scm 34 */
				bool_t BgL_test1940z00_2876;

				{	/* Ast/toccur.scm 34 */
					BgL_valuez00_bglt BgL_arg1348z00_1615;

					BgL_arg1348z00_1615 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_globalz00_3)))->BgL_valuez00);
					{	/* Ast/toccur.scm 34 */
						obj_t BgL_classz00_2074;

						BgL_classz00_2074 = BGl_sfunz00zzast_varz00;
						{	/* Ast/toccur.scm 34 */
							BgL_objectz00_bglt BgL_arg1807z00_2076;

							{	/* Ast/toccur.scm 34 */
								obj_t BgL_tmpz00_2879;

								BgL_tmpz00_2879 =
									((obj_t) ((BgL_objectz00_bglt) BgL_arg1348z00_1615));
								BgL_arg1807z00_2076 = (BgL_objectz00_bglt) (BgL_tmpz00_2879);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Ast/toccur.scm 34 */
									long BgL_idxz00_2082;

									BgL_idxz00_2082 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2076);
									BgL_test1940z00_2876 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2082 + 3L)) == BgL_classz00_2074);
								}
							else
								{	/* Ast/toccur.scm 34 */
									bool_t BgL_res1849z00_2107;

									{	/* Ast/toccur.scm 34 */
										obj_t BgL_oclassz00_2090;

										{	/* Ast/toccur.scm 34 */
											obj_t BgL_arg1815z00_2098;
											long BgL_arg1816z00_2099;

											BgL_arg1815z00_2098 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Ast/toccur.scm 34 */
												long BgL_arg1817z00_2100;

												BgL_arg1817z00_2100 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2076);
												BgL_arg1816z00_2099 =
													(BgL_arg1817z00_2100 - OBJECT_TYPE);
											}
											BgL_oclassz00_2090 =
												VECTOR_REF(BgL_arg1815z00_2098, BgL_arg1816z00_2099);
										}
										{	/* Ast/toccur.scm 34 */
											bool_t BgL__ortest_1115z00_2091;

											BgL__ortest_1115z00_2091 =
												(BgL_classz00_2074 == BgL_oclassz00_2090);
											if (BgL__ortest_1115z00_2091)
												{	/* Ast/toccur.scm 34 */
													BgL_res1849z00_2107 = BgL__ortest_1115z00_2091;
												}
											else
												{	/* Ast/toccur.scm 34 */
													long BgL_odepthz00_2092;

													{	/* Ast/toccur.scm 34 */
														obj_t BgL_arg1804z00_2093;

														BgL_arg1804z00_2093 = (BgL_oclassz00_2090);
														BgL_odepthz00_2092 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2093);
													}
													if ((3L < BgL_odepthz00_2092))
														{	/* Ast/toccur.scm 34 */
															obj_t BgL_arg1802z00_2095;

															{	/* Ast/toccur.scm 34 */
																obj_t BgL_arg1803z00_2096;

																BgL_arg1803z00_2096 = (BgL_oclassz00_2090);
																BgL_arg1802z00_2095 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2096,
																	3L);
															}
															BgL_res1849z00_2107 =
																(BgL_arg1802z00_2095 == BgL_classz00_2074);
														}
													else
														{	/* Ast/toccur.scm 34 */
															BgL_res1849z00_2107 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1940z00_2876 = BgL_res1849z00_2107;
								}
						}
					}
				}
				if (BgL_test1940z00_2876)
					{	/* Ast/toccur.scm 34 */
						return
							BGl_typezd2incrementzd2sfunz12z12zzast_typezd2occurzd2(
							((obj_t) BgL_globalz00_3));
					}
				else
					{	/* Ast/toccur.scm 36 */
						BgL_typez00_bglt BgL_arg1346z00_1614;

						BgL_arg1346z00_1614 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_globalz00_3)))->BgL_typez00);
						return
							BGl_typezd2occurrencezd2incrementz12z12zztype_typez00
							(BgL_arg1346z00_1614);
					}
			}
		}

	}



/* &type-increment-global! */
	obj_t BGl_z62typezd2incrementzd2globalz12z70zzast_typezd2occurzd2(obj_t
		BgL_envz00_2596, obj_t BgL_globalz00_2597)
	{
		{	/* Ast/toccur.scm 33 */
			return
				BGl_typezd2incrementzd2globalz12z12zzast_typezd2occurzd2(
				((BgL_globalz00_bglt) BgL_globalz00_2597));
		}

	}



/* type-increment-sfun! */
	obj_t BGl_typezd2incrementzd2sfunz12z12zzast_typezd2occurzd2(obj_t
		BgL_varz00_4)
	{
		{	/* Ast/toccur.scm 41 */
			{	/* Ast/toccur.scm 43 */
				BgL_typez00_bglt BgL_arg1349z00_1617;

				BgL_arg1349z00_1617 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_varz00_4)))->BgL_typez00);
				BGl_typezd2occurrencezd2incrementz12z12zztype_typez00
					(BgL_arg1349z00_1617);
			}
			{	/* Ast/toccur.scm 44 */
				obj_t BgL_g1258z00_1618;

				BgL_g1258z00_1618 =
					(((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt)
								(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_varz00_4)))->
									BgL_valuez00))))->BgL_argsz00);
				{
					obj_t BgL_l1256z00_1620;

					BgL_l1256z00_1620 = BgL_g1258z00_1618;
				BgL_zc3z04anonymousza31350ze3z87_1621:
					if (PAIRP(BgL_l1256z00_1620))
						{	/* Ast/toccur.scm 50 */
							{	/* Ast/toccur.scm 46 */
								obj_t BgL_az00_1623;

								BgL_az00_1623 = CAR(BgL_l1256z00_1620);
								{	/* Ast/toccur.scm 46 */
									bool_t BgL_test1945z00_2919;

									{	/* Ast/toccur.scm 46 */
										obj_t BgL_classz00_2113;

										BgL_classz00_2113 = BGl_typez00zztype_typez00;
										if (BGL_OBJECTP(BgL_az00_1623))
											{	/* Ast/toccur.scm 46 */
												BgL_objectz00_bglt BgL_arg1807z00_2115;

												BgL_arg1807z00_2115 =
													(BgL_objectz00_bglt) (BgL_az00_1623);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Ast/toccur.scm 46 */
														long BgL_idxz00_2121;

														BgL_idxz00_2121 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2115);
														BgL_test1945z00_2919 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2121 + 1L)) == BgL_classz00_2113);
													}
												else
													{	/* Ast/toccur.scm 46 */
														bool_t BgL_res1850z00_2146;

														{	/* Ast/toccur.scm 46 */
															obj_t BgL_oclassz00_2129;

															{	/* Ast/toccur.scm 46 */
																obj_t BgL_arg1815z00_2137;
																long BgL_arg1816z00_2138;

																BgL_arg1815z00_2137 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Ast/toccur.scm 46 */
																	long BgL_arg1817z00_2139;

																	BgL_arg1817z00_2139 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2115);
																	BgL_arg1816z00_2138 =
																		(BgL_arg1817z00_2139 - OBJECT_TYPE);
																}
																BgL_oclassz00_2129 =
																	VECTOR_REF(BgL_arg1815z00_2137,
																	BgL_arg1816z00_2138);
															}
															{	/* Ast/toccur.scm 46 */
																bool_t BgL__ortest_1115z00_2130;

																BgL__ortest_1115z00_2130 =
																	(BgL_classz00_2113 == BgL_oclassz00_2129);
																if (BgL__ortest_1115z00_2130)
																	{	/* Ast/toccur.scm 46 */
																		BgL_res1850z00_2146 =
																			BgL__ortest_1115z00_2130;
																	}
																else
																	{	/* Ast/toccur.scm 46 */
																		long BgL_odepthz00_2131;

																		{	/* Ast/toccur.scm 46 */
																			obj_t BgL_arg1804z00_2132;

																			BgL_arg1804z00_2132 =
																				(BgL_oclassz00_2129);
																			BgL_odepthz00_2131 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2132);
																		}
																		if ((1L < BgL_odepthz00_2131))
																			{	/* Ast/toccur.scm 46 */
																				obj_t BgL_arg1802z00_2134;

																				{	/* Ast/toccur.scm 46 */
																					obj_t BgL_arg1803z00_2135;

																					BgL_arg1803z00_2135 =
																						(BgL_oclassz00_2129);
																					BgL_arg1802z00_2134 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2135, 1L);
																				}
																				BgL_res1850z00_2146 =
																					(BgL_arg1802z00_2134 ==
																					BgL_classz00_2113);
																			}
																		else
																			{	/* Ast/toccur.scm 46 */
																				BgL_res1850z00_2146 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test1945z00_2919 = BgL_res1850z00_2146;
													}
											}
										else
											{	/* Ast/toccur.scm 46 */
												BgL_test1945z00_2919 = ((bool_t) 0);
											}
									}
									if (BgL_test1945z00_2919)
										{	/* Ast/toccur.scm 46 */
											BGl_typezd2occurrencezd2incrementz12z12zztype_typez00(
												((BgL_typez00_bglt) BgL_az00_1623));
										}
									else
										{	/* Ast/toccur.scm 48 */
											bool_t BgL_test1950z00_2944;

											{	/* Ast/toccur.scm 48 */
												obj_t BgL_classz00_2147;

												BgL_classz00_2147 = BGl_localz00zzast_varz00;
												if (BGL_OBJECTP(BgL_az00_1623))
													{	/* Ast/toccur.scm 48 */
														BgL_objectz00_bglt BgL_arg1807z00_2149;

														BgL_arg1807z00_2149 =
															(BgL_objectz00_bglt) (BgL_az00_1623);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Ast/toccur.scm 48 */
																long BgL_idxz00_2155;

																BgL_idxz00_2155 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_2149);
																BgL_test1950z00_2944 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_2155 + 2L)) ==
																	BgL_classz00_2147);
															}
														else
															{	/* Ast/toccur.scm 48 */
																bool_t BgL_res1851z00_2180;

																{	/* Ast/toccur.scm 48 */
																	obj_t BgL_oclassz00_2163;

																	{	/* Ast/toccur.scm 48 */
																		obj_t BgL_arg1815z00_2171;
																		long BgL_arg1816z00_2172;

																		BgL_arg1815z00_2171 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Ast/toccur.scm 48 */
																			long BgL_arg1817z00_2173;

																			BgL_arg1817z00_2173 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_2149);
																			BgL_arg1816z00_2172 =
																				(BgL_arg1817z00_2173 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_2163 =
																			VECTOR_REF(BgL_arg1815z00_2171,
																			BgL_arg1816z00_2172);
																	}
																	{	/* Ast/toccur.scm 48 */
																		bool_t BgL__ortest_1115z00_2164;

																		BgL__ortest_1115z00_2164 =
																			(BgL_classz00_2147 == BgL_oclassz00_2163);
																		if (BgL__ortest_1115z00_2164)
																			{	/* Ast/toccur.scm 48 */
																				BgL_res1851z00_2180 =
																					BgL__ortest_1115z00_2164;
																			}
																		else
																			{	/* Ast/toccur.scm 48 */
																				long BgL_odepthz00_2165;

																				{	/* Ast/toccur.scm 48 */
																					obj_t BgL_arg1804z00_2166;

																					BgL_arg1804z00_2166 =
																						(BgL_oclassz00_2163);
																					BgL_odepthz00_2165 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_2166);
																				}
																				if ((2L < BgL_odepthz00_2165))
																					{	/* Ast/toccur.scm 48 */
																						obj_t BgL_arg1802z00_2168;

																						{	/* Ast/toccur.scm 48 */
																							obj_t BgL_arg1803z00_2169;

																							BgL_arg1803z00_2169 =
																								(BgL_oclassz00_2163);
																							BgL_arg1802z00_2168 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_2169, 2L);
																						}
																						BgL_res1851z00_2180 =
																							(BgL_arg1802z00_2168 ==
																							BgL_classz00_2147);
																					}
																				else
																					{	/* Ast/toccur.scm 48 */
																						BgL_res1851z00_2180 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test1950z00_2944 = BgL_res1851z00_2180;
															}
													}
												else
													{	/* Ast/toccur.scm 48 */
														BgL_test1950z00_2944 = ((bool_t) 0);
													}
											}
											if (BgL_test1950z00_2944)
												{	/* Ast/toccur.scm 49 */
													BgL_typez00_bglt BgL_arg1361z00_1626;

													BgL_arg1361z00_1626 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt) BgL_az00_1623))))->
														BgL_typez00);
													BGl_typezd2occurrencezd2incrementz12z12zztype_typez00
														(BgL_arg1361z00_1626);
												}
											else
												{	/* Ast/toccur.scm 48 */
													BFALSE;
												}
										}
								}
							}
							{
								obj_t BgL_l1256z00_2971;

								BgL_l1256z00_2971 = CDR(BgL_l1256z00_1620);
								BgL_l1256z00_1620 = BgL_l1256z00_2971;
								goto BgL_zc3z04anonymousza31350ze3z87_1621;
							}
						}
					else
						{	/* Ast/toccur.scm 50 */
							((bool_t) 1);
						}
				}
			}
			{	/* Ast/toccur.scm 51 */
				obj_t BgL_nodez00_1630;

				BgL_nodez00_1630 =
					(((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt)
								(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_varz00_4)))->
									BgL_valuez00))))->BgL_bodyz00);
				{	/* Ast/toccur.scm 52 */
					bool_t BgL_test1955z00_2977;

					{	/* Ast/toccur.scm 52 */
						obj_t BgL_classz00_2185;

						BgL_classz00_2185 = BGl_nodez00zzast_nodez00;
						if (BGL_OBJECTP(BgL_nodez00_1630))
							{	/* Ast/toccur.scm 52 */
								BgL_objectz00_bglt BgL_arg1807z00_2187;

								BgL_arg1807z00_2187 = (BgL_objectz00_bglt) (BgL_nodez00_1630);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Ast/toccur.scm 52 */
										long BgL_idxz00_2193;

										BgL_idxz00_2193 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2187);
										BgL_test1955z00_2977 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2193 + 1L)) == BgL_classz00_2185);
									}
								else
									{	/* Ast/toccur.scm 52 */
										bool_t BgL_res1852z00_2218;

										{	/* Ast/toccur.scm 52 */
											obj_t BgL_oclassz00_2201;

											{	/* Ast/toccur.scm 52 */
												obj_t BgL_arg1815z00_2209;
												long BgL_arg1816z00_2210;

												BgL_arg1815z00_2209 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Ast/toccur.scm 52 */
													long BgL_arg1817z00_2211;

													BgL_arg1817z00_2211 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2187);
													BgL_arg1816z00_2210 =
														(BgL_arg1817z00_2211 - OBJECT_TYPE);
												}
												BgL_oclassz00_2201 =
													VECTOR_REF(BgL_arg1815z00_2209, BgL_arg1816z00_2210);
											}
											{	/* Ast/toccur.scm 52 */
												bool_t BgL__ortest_1115z00_2202;

												BgL__ortest_1115z00_2202 =
													(BgL_classz00_2185 == BgL_oclassz00_2201);
												if (BgL__ortest_1115z00_2202)
													{	/* Ast/toccur.scm 52 */
														BgL_res1852z00_2218 = BgL__ortest_1115z00_2202;
													}
												else
													{	/* Ast/toccur.scm 52 */
														long BgL_odepthz00_2203;

														{	/* Ast/toccur.scm 52 */
															obj_t BgL_arg1804z00_2204;

															BgL_arg1804z00_2204 = (BgL_oclassz00_2201);
															BgL_odepthz00_2203 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2204);
														}
														if ((1L < BgL_odepthz00_2203))
															{	/* Ast/toccur.scm 52 */
																obj_t BgL_arg1802z00_2206;

																{	/* Ast/toccur.scm 52 */
																	obj_t BgL_arg1803z00_2207;

																	BgL_arg1803z00_2207 = (BgL_oclassz00_2201);
																	BgL_arg1802z00_2206 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2207,
																		1L);
																}
																BgL_res1852z00_2218 =
																	(BgL_arg1802z00_2206 == BgL_classz00_2185);
															}
														else
															{	/* Ast/toccur.scm 52 */
																BgL_res1852z00_2218 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1955z00_2977 = BgL_res1852z00_2218;
									}
							}
						else
							{	/* Ast/toccur.scm 52 */
								BgL_test1955z00_2977 = ((bool_t) 0);
							}
					}
					if (BgL_test1955z00_2977)
						{	/* Ast/toccur.scm 52 */
							return
								BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
								((BgL_nodez00_bglt) BgL_nodez00_1630));
						}
					else
						{	/* Ast/toccur.scm 52 */
							return BFALSE;
						}
				}
			}
		}

	}



/* occur-node*! */
	bool_t BGl_occurzd2nodeza2z12z62zzast_typezd2occurzd2(obj_t BgL_nodeza2za2_37)
	{
		{	/* Ast/toccur.scm 328 */
			{
				obj_t BgL_l1274z00_1634;

				BgL_l1274z00_1634 = BgL_nodeza2za2_37;
			BgL_zc3z04anonymousza31371ze3z87_1635:
				if (PAIRP(BgL_l1274z00_1634))
					{	/* Ast/toccur.scm 329 */
						{	/* Ast/toccur.scm 329 */
							obj_t BgL_arg1375z00_1637;

							BgL_arg1375z00_1637 = CAR(BgL_l1274z00_1634);
							BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
								((BgL_nodez00_bglt) BgL_arg1375z00_1637));
						}
						{
							obj_t BgL_l1274z00_3007;

							BgL_l1274z00_3007 = CDR(BgL_l1274z00_1634);
							BgL_l1274z00_1634 = BgL_l1274z00_3007;
							goto BgL_zc3z04anonymousza31371ze3z87_1635;
						}
					}
				else
					{	/* Ast/toccur.scm 329 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzast_typezd2occurzd2(void)
	{
		{	/* Ast/toccur.scm 14 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzast_typezd2occurzd2(void)
	{
		{	/* Ast/toccur.scm 14 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_proc1867z00zzast_typezd2occurzd2, BGl_nodez00zzast_nodez00,
				BGl_string1868z00zzast_typezd2occurzd2);
		}

	}



/* &occur-node!1276 */
	obj_t BGl_z62occurzd2nodez121276za2zzast_typezd2occurzd2(obj_t
		BgL_envz00_2599, obj_t BgL_nodez00_2600)
	{
		{	/* Ast/toccur.scm 70 */
			return CNST_TABLE_REF(0);
		}

	}



/* occur-node! */
	obj_t BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(BgL_nodez00_bglt
		BgL_nodez00_7)
	{
		{	/* Ast/toccur.scm 70 */
			{	/* Ast/toccur.scm 70 */
				obj_t BgL_method1277z00_1644;

				{	/* Ast/toccur.scm 70 */
					obj_t BgL_res1857z00_2251;

					{	/* Ast/toccur.scm 70 */
						long BgL_objzd2classzd2numz00_2222;

						BgL_objzd2classzd2numz00_2222 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_7));
						{	/* Ast/toccur.scm 70 */
							obj_t BgL_arg1811z00_2223;

							BgL_arg1811z00_2223 =
								PROCEDURE_REF(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
								(int) (1L));
							{	/* Ast/toccur.scm 70 */
								int BgL_offsetz00_2226;

								BgL_offsetz00_2226 = (int) (BgL_objzd2classzd2numz00_2222);
								{	/* Ast/toccur.scm 70 */
									long BgL_offsetz00_2227;

									BgL_offsetz00_2227 =
										((long) (BgL_offsetz00_2226) - OBJECT_TYPE);
									{	/* Ast/toccur.scm 70 */
										long BgL_modz00_2228;

										BgL_modz00_2228 =
											(BgL_offsetz00_2227 >> (int) ((long) ((int) (4L))));
										{	/* Ast/toccur.scm 70 */
											long BgL_restz00_2230;

											BgL_restz00_2230 =
												(BgL_offsetz00_2227 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Ast/toccur.scm 70 */

												{	/* Ast/toccur.scm 70 */
													obj_t BgL_bucketz00_2232;

													BgL_bucketz00_2232 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2223), BgL_modz00_2228);
													BgL_res1857z00_2251 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2232), BgL_restz00_2230);
					}}}}}}}}
					BgL_method1277z00_1644 = BgL_res1857z00_2251;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1277z00_1644, ((obj_t) BgL_nodez00_7));
			}
		}

	}



/* &occur-node! */
	obj_t BGl_z62occurzd2nodez12za2zzast_typezd2occurzd2(obj_t BgL_envz00_2601,
		obj_t BgL_nodez00_2602)
	{
		{	/* Ast/toccur.scm 70 */
			return
				BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
				((BgL_nodez00_bglt) BgL_nodez00_2602));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzast_typezd2occurzd2(void)
	{
		{	/* Ast/toccur.scm 14 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_varz00zzast_nodez00, BGl_proc1869z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_sequencez00zzast_nodez00, BGl_proc1871z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_syncz00zzast_nodez00, BGl_proc1872z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_appz00zzast_nodez00, BGl_proc1873z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1874z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_funcallz00zzast_nodez00, BGl_proc1875z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_externz00zzast_nodez00, BGl_proc1876z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_getfieldz00zzast_nodez00, BGl_proc1877z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_setfieldz00zzast_nodez00, BGl_proc1878z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_newz00zzast_nodez00, BGl_proc1879z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_castz00zzast_nodez00, BGl_proc1880z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_castzd2nullzd2zzast_nodez00, BGl_proc1881z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_instanceofz00zzast_nodez00, BGl_proc1882z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_vlengthz00zzast_nodez00, BGl_proc1883z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_vrefz00zzast_nodez00, BGl_proc1884z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_vsetz12z12zzast_nodez00, BGl_proc1885z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_vallocz00zzast_nodez00, BGl_proc1886z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_setqz00zzast_nodez00, BGl_proc1887z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_conditionalz00zzast_nodez00, BGl_proc1888z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_failz00zzast_nodez00, BGl_proc1889z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_switchz00zzast_nodez00, BGl_proc1890z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1891z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1892z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc1893z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc1894z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1895z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1896z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1897z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
				BGl_retblockz00zzast_nodez00, BGl_proc1898z00zzast_typezd2occurzd2,
				BGl_string1870z00zzast_typezd2occurzd2);
		}

	}



/* &occur-node!-retblock1335 */
	obj_t BGl_z62occurzd2nodez12zd2retblock1335z70zzast_typezd2occurzd2(obj_t
		BgL_envz00_2632, obj_t BgL_nodez00_2633)
	{
		{	/* Ast/toccur.scm 321 */
			return
				BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
				(((BgL_retblockz00_bglt) COBJECT(
							((BgL_retblockz00_bglt) BgL_nodez00_2633)))->BgL_bodyz00));
		}

	}



/* &occur-node!-box-set!1333 */
	obj_t BGl_z62occurzd2nodez12zd2boxzd2setz121333zb0zzast_typezd2occurzd2(obj_t
		BgL_envz00_2634, obj_t BgL_nodez00_2635)
	{
		{	/* Ast/toccur.scm 313 */
			{	/* Ast/toccur.scm 315 */
				BgL_varz00_bglt BgL_arg1708z00_2704;

				BgL_arg1708z00_2704 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2635)))->BgL_varz00);
				BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
					((BgL_nodez00_bglt) BgL_arg1708z00_2704));
			}
			return
				BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
				(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2635)))->BgL_valuez00));
		}

	}



/* &occur-node!-box-ref1331 */
	obj_t BGl_z62occurzd2nodez12zd2boxzd2ref1331za2zzast_typezd2occurzd2(obj_t
		BgL_envz00_2636, obj_t BgL_nodez00_2637)
	{
		{	/* Ast/toccur.scm 307 */
			{	/* Ast/toccur.scm 308 */
				BgL_varz00_bglt BgL_arg1705z00_2706;

				BgL_arg1705z00_2706 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_nodez00_2637)))->BgL_varz00);
				return
					BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
					((BgL_nodez00_bglt) BgL_arg1705z00_2706));
			}
		}

	}



/* &occur-node!-make-box1329 */
	obj_t BGl_z62occurzd2nodez12zd2makezd2box1329za2zzast_typezd2occurzd2(obj_t
		BgL_envz00_2638, obj_t BgL_nodez00_2639)
	{
		{	/* Ast/toccur.scm 301 */
			return
				BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
				(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_2639)))->BgL_valuez00));
		}

	}



/* &occur-node!-jump-ex-1327 */
	obj_t BGl_z62occurzd2nodez12zd2jumpzd2exzd21327z70zzast_typezd2occurzd2(obj_t
		BgL_envz00_2640, obj_t BgL_nodez00_2641)
	{
		{	/* Ast/toccur.scm 293 */
			BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2641)))->BgL_exitz00));
			return
				BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2641)))->BgL_valuez00));
		}

	}



/* &occur-node!-set-ex-i1325 */
	obj_t BGl_z62occurzd2nodez12zd2setzd2exzd2i1325z70zzast_typezd2occurzd2(obj_t
		BgL_envz00_2642, obj_t BgL_nodez00_2643)
	{
		{	/* Ast/toccur.scm 286 */
			BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2643)))->BgL_onexitz00));
			return
				BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2643)))->BgL_bodyz00));
		}

	}



/* &occur-node!-let-var1323 */
	obj_t BGl_z62occurzd2nodez12zd2letzd2var1323za2zzast_typezd2occurzd2(obj_t
		BgL_envz00_2644, obj_t BgL_nodez00_2645)
	{
		{	/* Ast/toccur.scm 274 */
			{	/* Ast/toccur.scm 276 */
				obj_t BgL_g1273z00_2711;

				BgL_g1273z00_2711 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2645)))->BgL_bindingsz00);
				{
					obj_t BgL_l1271z00_2713;

					BgL_l1271z00_2713 = BgL_g1273z00_2711;
				BgL_zc3z04anonymousza31682ze3z87_2712:
					if (PAIRP(BgL_l1271z00_2713))
						{	/* Ast/toccur.scm 276 */
							{	/* Ast/toccur.scm 278 */
								obj_t BgL_bindingz00_2714;

								BgL_bindingz00_2714 = CAR(BgL_l1271z00_2713);
								{	/* Ast/toccur.scm 277 */
									BgL_localz00_bglt BgL_i1126z00_2715;

									BgL_i1126z00_2715 =
										((BgL_localz00_bglt) CAR(((obj_t) BgL_bindingz00_2714)));
									{	/* Ast/toccur.scm 278 */
										BgL_typez00_bglt BgL_arg1688z00_2716;

										BgL_arg1688z00_2716 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_i1126z00_2715)))->
											BgL_typez00);
										BGl_typezd2occurrencezd2incrementz12z12zztype_typez00
											(BgL_arg1688z00_2716);
									}
								}
								{	/* Ast/toccur.scm 279 */
									obj_t BgL_arg1689z00_2717;

									BgL_arg1689z00_2717 = CDR(((obj_t) BgL_bindingz00_2714));
									BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
										((BgL_nodez00_bglt) BgL_arg1689z00_2717));
								}
							}
							{
								obj_t BgL_l1271z00_3116;

								BgL_l1271z00_3116 = CDR(BgL_l1271z00_2713);
								BgL_l1271z00_2713 = BgL_l1271z00_3116;
								goto BgL_zc3z04anonymousza31682ze3z87_2712;
							}
						}
					else
						{	/* Ast/toccur.scm 276 */
							((bool_t) 1);
						}
				}
			}
			return
				BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
				(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2645)))->BgL_bodyz00));
		}

	}



/* &occur-node!-let-fun1321 */
	obj_t BGl_z62occurzd2nodez12zd2letzd2fun1321za2zzast_typezd2occurzd2(obj_t
		BgL_envz00_2646, obj_t BgL_nodez00_2647)
	{
		{	/* Ast/toccur.scm 266 */
			{	/* Ast/toccur.scm 268 */
				obj_t BgL_g1270z00_2719;

				BgL_g1270z00_2719 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2647)))->BgL_localsz00);
				{
					obj_t BgL_l1268z00_2721;

					BgL_l1268z00_2721 = BgL_g1270z00_2719;
				BgL_zc3z04anonymousza31662ze3z87_2720:
					if (PAIRP(BgL_l1268z00_2721))
						{	/* Ast/toccur.scm 268 */
							BGl_typezd2incrementzd2sfunz12z12zzast_typezd2occurzd2(CAR
								(BgL_l1268z00_2721));
							{
								obj_t BgL_l1268z00_3127;

								BgL_l1268z00_3127 = CDR(BgL_l1268z00_2721);
								BgL_l1268z00_2721 = BgL_l1268z00_3127;
								goto BgL_zc3z04anonymousza31662ze3z87_2720;
							}
						}
					else
						{	/* Ast/toccur.scm 268 */
							((bool_t) 1);
						}
				}
			}
			return
				BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
				(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2647)))->BgL_bodyz00));
		}

	}



/* &occur-node!-switch1319 */
	obj_t BGl_z62occurzd2nodez12zd2switch1319z70zzast_typezd2occurzd2(obj_t
		BgL_envz00_2648, obj_t BgL_nodez00_2649)
	{
		{	/* Ast/toccur.scm 256 */
			{	/* Ast/toccur.scm 257 */
				bool_t BgL_tmpz00_3132;

				BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
					(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2649)))->BgL_testz00));
				{	/* Ast/toccur.scm 259 */
					obj_t BgL_g1267z00_2723;

					BgL_g1267z00_2723 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2649)))->BgL_clausesz00);
					{
						obj_t BgL_l1265z00_2725;

						BgL_l1265z00_2725 = BgL_g1267z00_2723;
					BgL_zc3z04anonymousza31651ze3z87_2724:
						if (PAIRP(BgL_l1265z00_2725))
							{	/* Ast/toccur.scm 259 */
								{	/* Ast/toccur.scm 260 */
									obj_t BgL_clausez00_2726;

									BgL_clausez00_2726 = CAR(BgL_l1265z00_2725);
									{	/* Ast/toccur.scm 260 */
										obj_t BgL_arg1654z00_2727;

										BgL_arg1654z00_2727 = CDR(((obj_t) BgL_clausez00_2726));
										BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
											((BgL_nodez00_bglt) BgL_arg1654z00_2727));
									}
								}
								{
									obj_t BgL_l1265z00_3145;

									BgL_l1265z00_3145 = CDR(BgL_l1265z00_2725);
									BgL_l1265z00_2725 = BgL_l1265z00_3145;
									goto BgL_zc3z04anonymousza31651ze3z87_2724;
								}
							}
						else
							{	/* Ast/toccur.scm 259 */
								BgL_tmpz00_3132 = ((bool_t) 1);
							}
					}
				}
				return BBOOL(BgL_tmpz00_3132);
			}
		}

	}



/* &occur-node!-fail1317 */
	obj_t BGl_z62occurzd2nodez12zd2fail1317z70zzast_typezd2occurzd2(obj_t
		BgL_envz00_2650, obj_t BgL_nodez00_2651)
	{
		{	/* Ast/toccur.scm 247 */
			BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2651)))->BgL_procz00));
			BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2651)))->BgL_msgz00));
			return
				BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2651)))->BgL_objz00));
		}

	}



/* &occur-node!-conditio1315 */
	obj_t BGl_z62occurzd2nodez12zd2conditio1315z70zzast_typezd2occurzd2(obj_t
		BgL_envz00_2652, obj_t BgL_nodez00_2653)
	{
		{	/* Ast/toccur.scm 238 */
			BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2653)))->BgL_testz00));
			BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2653)))->BgL_truez00));
			return
				BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2653)))->BgL_falsez00));
		}

	}



/* &occur-node!-setq1313 */
	obj_t BGl_z62occurzd2nodez12zd2setq1313z70zzast_typezd2occurzd2(obj_t
		BgL_envz00_2654, obj_t BgL_nodez00_2655)
	{
		{	/* Ast/toccur.scm 230 */
			{	/* Ast/toccur.scm 232 */
				BgL_varz00_bglt BgL_arg1616z00_2731;

				BgL_arg1616z00_2731 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_2655)))->BgL_varz00);
				BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
					((BgL_nodez00_bglt) BgL_arg1616z00_2731));
			}
			return
				BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
				(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_2655)))->BgL_valuez00));
		}

	}



/* &occur-node!-valloc1311 */
	obj_t BGl_z62occurzd2nodez12zd2valloc1311z70zzast_typezd2occurzd2(obj_t
		BgL_envz00_2656, obj_t BgL_nodez00_2657)
	{
		{	/* Ast/toccur.scm 220 */
			{

				{	/* Ast/toccur.scm 222 */
					BgL_typez00_bglt BgL_arg1611z00_2735;

					BgL_arg1611z00_2735 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_vallocz00_bglt) BgL_nodez00_2657))))->BgL_typez00);
					BGl_typezd2occurrencezd2incrementz12z12zztype_typez00
						(BgL_arg1611z00_2735);
				}
				BGl_typezd2occurrencezd2incrementz12z12zztype_typez00(
					(((BgL_vallocz00_bglt) COBJECT(
								((BgL_vallocz00_bglt) BgL_nodez00_2657)))->BgL_ftypez00));
				BGl_typezd2occurrencezd2incrementz12z12zztype_typez00(
					(((BgL_vallocz00_bglt) COBJECT(
								((BgL_vallocz00_bglt) BgL_nodez00_2657)))->BgL_otypez00));
				{	/* Ast/toccur.scm 220 */
					obj_t BgL_nextzd2method1310zd2_2734;

					BgL_nextzd2method1310zd2_2734 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_vallocz00_bglt) BgL_nodez00_2657)),
						BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
						BGl_vallocz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1310zd2_2734,
						((obj_t) ((BgL_vallocz00_bglt) BgL_nodez00_2657)));
				}
			}
		}

	}



/* &occur-node!-vset!1309 */
	obj_t BGl_z62occurzd2nodez12zd2vsetz121309z62zzast_typezd2occurzd2(obj_t
		BgL_envz00_2658, obj_t BgL_nodez00_2659)
	{
		{	/* Ast/toccur.scm 210 */
			{

				{	/* Ast/toccur.scm 212 */
					BgL_typez00_bglt BgL_arg1605z00_2739;

					BgL_arg1605z00_2739 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_vsetz12z12_bglt) BgL_nodez00_2659))))->BgL_typez00);
					BGl_typezd2occurrencezd2incrementz12z12zztype_typez00
						(BgL_arg1605z00_2739);
				}
				BGl_typezd2occurrencezd2incrementz12z12zztype_typez00(
					(((BgL_vsetz12z12_bglt) COBJECT(
								((BgL_vsetz12z12_bglt) BgL_nodez00_2659)))->BgL_ftypez00));
				BGl_typezd2occurrencezd2incrementz12z12zztype_typez00(
					(((BgL_vsetz12z12_bglt) COBJECT(
								((BgL_vsetz12z12_bglt) BgL_nodez00_2659)))->BgL_otypez00));
				{	/* Ast/toccur.scm 210 */
					obj_t BgL_nextzd2method1308zd2_2738;

					BgL_nextzd2method1308zd2_2738 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_vsetz12z12_bglt) BgL_nodez00_2659)),
						BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
						BGl_vsetz12z12zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1308zd2_2738,
						((obj_t) ((BgL_vsetz12z12_bglt) BgL_nodez00_2659)));
				}
			}
		}

	}



/* &occur-node!-vref1307 */
	obj_t BGl_z62occurzd2nodez12zd2vref1307z70zzast_typezd2occurzd2(obj_t
		BgL_envz00_2660, obj_t BgL_nodez00_2661)
	{
		{	/* Ast/toccur.scm 200 */
			{

				{	/* Ast/toccur.scm 202 */
					BgL_typez00_bglt BgL_arg1594z00_2743;

					BgL_arg1594z00_2743 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_vrefz00_bglt) BgL_nodez00_2661))))->BgL_typez00);
					BGl_typezd2occurrencezd2incrementz12z12zztype_typez00
						(BgL_arg1594z00_2743);
				}
				BGl_typezd2occurrencezd2incrementz12z12zztype_typez00(
					(((BgL_vrefz00_bglt) COBJECT(
								((BgL_vrefz00_bglt) BgL_nodez00_2661)))->BgL_ftypez00));
				BGl_typezd2occurrencezd2incrementz12z12zztype_typez00(
					(((BgL_vrefz00_bglt) COBJECT(
								((BgL_vrefz00_bglt) BgL_nodez00_2661)))->BgL_otypez00));
				{	/* Ast/toccur.scm 200 */
					obj_t BgL_nextzd2method1306zd2_2742;

					BgL_nextzd2method1306zd2_2742 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_vrefz00_bglt) BgL_nodez00_2661)),
						BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
						BGl_vrefz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1306zd2_2742,
						((obj_t) ((BgL_vrefz00_bglt) BgL_nodez00_2661)));
				}
			}
		}

	}



/* &occur-node!-vlength1305 */
	obj_t BGl_z62occurzd2nodez12zd2vlength1305z70zzast_typezd2occurzd2(obj_t
		BgL_envz00_2662, obj_t BgL_nodez00_2663)
	{
		{	/* Ast/toccur.scm 191 */
			{

				{	/* Ast/toccur.scm 193 */
					BgL_typez00_bglt BgL_arg1591z00_2747;

					BgL_arg1591z00_2747 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_vlengthz00_bglt) BgL_nodez00_2663))))->BgL_typez00);
					BGl_typezd2occurrencezd2incrementz12z12zztype_typez00
						(BgL_arg1591z00_2747);
				}
				BGl_typezd2occurrencezd2incrementz12z12zztype_typez00(
					(((BgL_vlengthz00_bglt) COBJECT(
								((BgL_vlengthz00_bglt) BgL_nodez00_2663)))->BgL_vtypez00));
				{	/* Ast/toccur.scm 191 */
					obj_t BgL_nextzd2method1304zd2_2746;

					BgL_nextzd2method1304zd2_2746 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_vlengthz00_bglt) BgL_nodez00_2663)),
						BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
						BGl_vlengthz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1304zd2_2746,
						((obj_t) ((BgL_vlengthz00_bglt) BgL_nodez00_2663)));
				}
			}
		}

	}



/* &occur-node!-instance1303 */
	obj_t BGl_z62occurzd2nodez12zd2instance1303z70zzast_typezd2occurzd2(obj_t
		BgL_envz00_2664, obj_t BgL_nodez00_2665)
	{
		{	/* Ast/toccur.scm 182 */
			{

				{	/* Ast/toccur.scm 184 */
					BgL_typez00_bglt BgL_arg1585z00_2751;

					BgL_arg1585z00_2751 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_instanceofz00_bglt) BgL_nodez00_2665))))->BgL_typez00);
					BGl_typezd2occurrencezd2incrementz12z12zztype_typez00
						(BgL_arg1585z00_2751);
				}
				BGl_typezd2occurrencezd2incrementz12z12zztype_typez00(
					(((BgL_instanceofz00_bglt) COBJECT(
								((BgL_instanceofz00_bglt) BgL_nodez00_2665)))->BgL_classz00));
				{	/* Ast/toccur.scm 182 */
					obj_t BgL_nextzd2method1302zd2_2750;

					BgL_nextzd2method1302zd2_2750 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_instanceofz00_bglt) BgL_nodez00_2665)),
						BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
						BGl_instanceofz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1302zd2_2750,
						((obj_t) ((BgL_instanceofz00_bglt) BgL_nodez00_2665)));
				}
			}
		}

	}



/* &occur-node!-cast-nul1301 */
	obj_t BGl_z62occurzd2nodez12zd2castzd2nul1301za2zzast_typezd2occurzd2(obj_t
		BgL_envz00_2666, obj_t BgL_nodez00_2667)
	{
		{	/* Ast/toccur.scm 175 */
			{	/* Ast/toccur.scm 177 */
				BgL_typez00_bglt BgL_arg1584z00_2753;

				BgL_arg1584z00_2753 =
					(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_castzd2nullzd2_bglt) BgL_nodez00_2667))))->BgL_typez00);
				return
					BGl_typezd2occurrencezd2incrementz12z12zztype_typez00
					(BgL_arg1584z00_2753);
			}
		}

	}



/* &occur-node!-cast1299 */
	obj_t BGl_z62occurzd2nodez12zd2cast1299z70zzast_typezd2occurzd2(obj_t
		BgL_envz00_2668, obj_t BgL_nodez00_2669)
	{
		{	/* Ast/toccur.scm 166 */
			{

				{	/* Ast/toccur.scm 168 */
					BgL_typez00_bglt BgL_arg1575z00_2757;

					BgL_arg1575z00_2757 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_castz00_bglt) BgL_nodez00_2669))))->BgL_typez00);
					BGl_typezd2occurrencezd2incrementz12z12zztype_typez00
						(BgL_arg1575z00_2757);
				}
				BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
					(((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nodez00_2669)))->BgL_argz00));
				{	/* Ast/toccur.scm 166 */
					obj_t BgL_nextzd2method1298zd2_2756;

					BgL_nextzd2method1298zd2_2756 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_castz00_bglt) BgL_nodez00_2669)),
						BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
						BGl_castz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1298zd2_2756,
						((obj_t) ((BgL_castz00_bglt) BgL_nodez00_2669)));
				}
			}
		}

	}



/* &occur-node!-new1297 */
	obj_t BGl_z62occurzd2nodez12zd2new1297z70zzast_typezd2occurzd2(obj_t
		BgL_envz00_2670, obj_t BgL_nodez00_2671)
	{
		{	/* Ast/toccur.scm 157 */
			{

				{	/* Ast/toccur.scm 159 */
					BgL_typez00_bglt BgL_arg1564z00_2761;

					BgL_arg1564z00_2761 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_newz00_bglt) BgL_nodez00_2671))))->BgL_typez00);
					BGl_typezd2occurrencezd2incrementz12z12zztype_typez00
						(BgL_arg1564z00_2761);
				}
				{	/* Ast/toccur.scm 160 */
					obj_t BgL_g1264z00_2762;

					BgL_g1264z00_2762 =
						(((BgL_newz00_bglt) COBJECT(
								((BgL_newz00_bglt) BgL_nodez00_2671)))->BgL_argszd2typezd2);
					{
						obj_t BgL_l1262z00_2764;

						BgL_l1262z00_2764 = BgL_g1264z00_2762;
					BgL_zc3z04anonymousza31565ze3z87_2763:
						if (PAIRP(BgL_l1262z00_2764))
							{	/* Ast/toccur.scm 160 */
								{	/* Ast/toccur.scm 160 */
									obj_t BgL_arg1571z00_2765;

									BgL_arg1571z00_2765 = CAR(BgL_l1262z00_2764);
									BGl_typezd2occurrencezd2incrementz12z12zztype_typez00(
										((BgL_typez00_bglt) BgL_arg1571z00_2765));
								}
								{
									obj_t BgL_l1262z00_3293;

									BgL_l1262z00_3293 = CDR(BgL_l1262z00_2764);
									BgL_l1262z00_2764 = BgL_l1262z00_3293;
									goto BgL_zc3z04anonymousza31565ze3z87_2763;
								}
							}
						else
							{	/* Ast/toccur.scm 160 */
								((bool_t) 1);
							}
					}
				}
				{	/* Ast/toccur.scm 157 */
					obj_t BgL_nextzd2method1296zd2_2760;

					BgL_nextzd2method1296zd2_2760 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_newz00_bglt) BgL_nodez00_2671)),
						BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
						BGl_newz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1296zd2_2760,
						((obj_t) ((BgL_newz00_bglt) BgL_nodez00_2671)));
				}
			}
		}

	}



/* &occur-node!-setfield1295 */
	obj_t BGl_z62occurzd2nodez12zd2setfield1295z70zzast_typezd2occurzd2(obj_t
		BgL_envz00_2672, obj_t BgL_nodez00_2673)
	{
		{	/* Ast/toccur.scm 147 */
			{

				{	/* Ast/toccur.scm 149 */
					BgL_typez00_bglt BgL_arg1553z00_2769;

					BgL_arg1553z00_2769 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_setfieldz00_bglt) BgL_nodez00_2673))))->BgL_typez00);
					BGl_typezd2occurrencezd2incrementz12z12zztype_typez00
						(BgL_arg1553z00_2769);
				}
				BGl_typezd2occurrencezd2incrementz12z12zztype_typez00(
					(((BgL_setfieldz00_bglt) COBJECT(
								((BgL_setfieldz00_bglt) BgL_nodez00_2673)))->BgL_otypez00));
				BGl_typezd2occurrencezd2incrementz12z12zztype_typez00(
					(((BgL_setfieldz00_bglt) COBJECT(
								((BgL_setfieldz00_bglt) BgL_nodez00_2673)))->BgL_ftypez00));
				{	/* Ast/toccur.scm 147 */
					obj_t BgL_nextzd2method1294zd2_2768;

					BgL_nextzd2method1294zd2_2768 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_setfieldz00_bglt) BgL_nodez00_2673)),
						BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
						BGl_setfieldz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1294zd2_2768,
						((obj_t) ((BgL_setfieldz00_bglt) BgL_nodez00_2673)));
				}
			}
		}

	}



/* &occur-node!-getfield1293 */
	obj_t BGl_z62occurzd2nodez12zd2getfield1293z70zzast_typezd2occurzd2(obj_t
		BgL_envz00_2674, obj_t BgL_nodez00_2675)
	{
		{	/* Ast/toccur.scm 137 */
			{

				{	/* Ast/toccur.scm 139 */
					BgL_typez00_bglt BgL_arg1544z00_2773;

					BgL_arg1544z00_2773 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_getfieldz00_bglt) BgL_nodez00_2675))))->BgL_typez00);
					BGl_typezd2occurrencezd2incrementz12z12zztype_typez00
						(BgL_arg1544z00_2773);
				}
				BGl_typezd2occurrencezd2incrementz12z12zztype_typez00(
					(((BgL_getfieldz00_bglt) COBJECT(
								((BgL_getfieldz00_bglt) BgL_nodez00_2675)))->BgL_otypez00));
				BGl_typezd2occurrencezd2incrementz12z12zztype_typez00(
					(((BgL_getfieldz00_bglt) COBJECT(
								((BgL_getfieldz00_bglt) BgL_nodez00_2675)))->BgL_ftypez00));
				{	/* Ast/toccur.scm 137 */
					obj_t BgL_nextzd2method1292zd2_2772;

					BgL_nextzd2method1292zd2_2772 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_getfieldz00_bglt) BgL_nodez00_2675)),
						BGl_occurzd2nodez12zd2envz12zzast_typezd2occurzd2,
						BGl_getfieldz00zzast_nodez00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1292zd2_2772,
						((obj_t) ((BgL_getfieldz00_bglt) BgL_nodez00_2675)));
				}
			}
		}

	}



/* &occur-node!-extern1291 */
	obj_t BGl_z62occurzd2nodez12zd2extern1291z70zzast_typezd2occurzd2(obj_t
		BgL_envz00_2676, obj_t BgL_nodez00_2677)
	{
		{	/* Ast/toccur.scm 129 */
			return
				BBOOL(BGl_occurzd2nodeza2z12z62zzast_typezd2occurzd2(
					(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_nodez00_2677)))->BgL_exprza2za2)));
		}

	}



/* &occur-node!-funcall1289 */
	obj_t BGl_z62occurzd2nodez12zd2funcall1289z70zzast_typezd2occurzd2(obj_t
		BgL_envz00_2678, obj_t BgL_nodez00_2679)
	{
		{	/* Ast/toccur.scm 120 */
			{	/* Ast/toccur.scm 121 */
				bool_t BgL_tmpz00_3346;

				{	/* Ast/toccur.scm 122 */
					BgL_typez00_bglt BgL_arg1516z00_2776;

					BgL_arg1516z00_2776 =
						BGl_getzd2typezd2zztype_typeofz00(
						((BgL_nodez00_bglt)
							((BgL_funcallz00_bglt) BgL_nodez00_2679)), ((bool_t) 0));
					BGl_typezd2occurrencezd2incrementz12z12zztype_typez00
						(BgL_arg1516z00_2776);
				}
				BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2679)))->BgL_funz00));
				BgL_tmpz00_3346 =
					BGl_occurzd2nodeza2z12z62zzast_typezd2occurzd2(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2679)))->BgL_argsz00));
				return BBOOL(BgL_tmpz00_3346);
			}
		}

	}



/* &occur-node!-app-ly1287 */
	obj_t BGl_z62occurzd2nodez12zd2appzd2ly1287za2zzast_typezd2occurzd2(obj_t
		BgL_envz00_2680, obj_t BgL_nodez00_2681)
	{
		{	/* Ast/toccur.scm 112 */
			BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
				(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_2681)))->BgL_funz00));
			return
				BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
				(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_2681)))->BgL_argz00));
		}

	}



/* &occur-node!-app1285 */
	obj_t BGl_z62occurzd2nodez12zd2app1285z70zzast_typezd2occurzd2(obj_t
		BgL_envz00_2682, obj_t BgL_nodez00_2683)
	{
		{	/* Ast/toccur.scm 99 */
			{	/* Ast/toccur.scm 100 */
				bool_t BgL_tmpz00_3364;

				{	/* Ast/toccur.scm 101 */
					BgL_varz00_bglt BgL_arg1454z00_2779;

					BgL_arg1454z00_2779 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_2683)))->BgL_funz00);
					BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
						((BgL_nodez00_bglt) BgL_arg1454z00_2779));
				}
				BGl_occurzd2nodeza2z12z62zzast_typezd2occurzd2(
					(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_2683)))->BgL_argsz00));
				{	/* Ast/toccur.scm 103 */
					BgL_variablez00_bglt BgL_varz00_2780;

					BgL_varz00_2780 =
						(((BgL_varz00_bglt) COBJECT(
								(((BgL_appz00_bglt) COBJECT(
											((BgL_appz00_bglt) BgL_nodez00_2683)))->BgL_funz00)))->
						BgL_variablez00);
					{	/* Ast/toccur.scm 103 */
						BgL_valuez00_bglt BgL_valz00_2781;

						BgL_valz00_2781 =
							(((BgL_variablez00_bglt) COBJECT(BgL_varz00_2780))->BgL_valuez00);
						{	/* Ast/toccur.scm 104 */

							{	/* Ast/toccur.scm 105 */
								bool_t BgL_test1965z00_3376;

								{	/* Ast/toccur.scm 105 */
									obj_t BgL_classz00_2782;

									BgL_classz00_2782 = BGl_cfunz00zzast_varz00;
									{	/* Ast/toccur.scm 105 */
										BgL_objectz00_bglt BgL_arg1807z00_2783;

										{	/* Ast/toccur.scm 105 */
											obj_t BgL_tmpz00_3377;

											BgL_tmpz00_3377 =
												((obj_t) ((BgL_objectz00_bglt) BgL_valz00_2781));
											BgL_arg1807z00_2783 =
												(BgL_objectz00_bglt) (BgL_tmpz00_3377);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Ast/toccur.scm 105 */
												long BgL_idxz00_2784;

												BgL_idxz00_2784 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2783);
												BgL_test1965z00_3376 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2784 + 3L)) == BgL_classz00_2782);
											}
										else
											{	/* Ast/toccur.scm 105 */
												bool_t BgL_res1860z00_2787;

												{	/* Ast/toccur.scm 105 */
													obj_t BgL_oclassz00_2788;

													{	/* Ast/toccur.scm 105 */
														obj_t BgL_arg1815z00_2789;
														long BgL_arg1816z00_2790;

														BgL_arg1815z00_2789 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Ast/toccur.scm 105 */
															long BgL_arg1817z00_2791;

															BgL_arg1817z00_2791 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2783);
															BgL_arg1816z00_2790 =
																(BgL_arg1817z00_2791 - OBJECT_TYPE);
														}
														BgL_oclassz00_2788 =
															VECTOR_REF(BgL_arg1815z00_2789,
															BgL_arg1816z00_2790);
													}
													{	/* Ast/toccur.scm 105 */
														bool_t BgL__ortest_1115z00_2792;

														BgL__ortest_1115z00_2792 =
															(BgL_classz00_2782 == BgL_oclassz00_2788);
														if (BgL__ortest_1115z00_2792)
															{	/* Ast/toccur.scm 105 */
																BgL_res1860z00_2787 = BgL__ortest_1115z00_2792;
															}
														else
															{	/* Ast/toccur.scm 105 */
																long BgL_odepthz00_2793;

																{	/* Ast/toccur.scm 105 */
																	obj_t BgL_arg1804z00_2794;

																	BgL_arg1804z00_2794 = (BgL_oclassz00_2788);
																	BgL_odepthz00_2793 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2794);
																}
																if ((3L < BgL_odepthz00_2793))
																	{	/* Ast/toccur.scm 105 */
																		obj_t BgL_arg1802z00_2795;

																		{	/* Ast/toccur.scm 105 */
																			obj_t BgL_arg1803z00_2796;

																			BgL_arg1803z00_2796 =
																				(BgL_oclassz00_2788);
																			BgL_arg1802z00_2795 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2796, 3L);
																		}
																		BgL_res1860z00_2787 =
																			(BgL_arg1802z00_2795 ==
																			BgL_classz00_2782);
																	}
																else
																	{	/* Ast/toccur.scm 105 */
																		BgL_res1860z00_2787 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test1965z00_3376 = BgL_res1860z00_2787;
											}
									}
								}
								if (BgL_test1965z00_3376)
									{	/* Ast/toccur.scm 105 */
										BGl_typezd2occurrencezd2incrementz12z12zztype_typez00(
											(((BgL_variablez00_bglt) COBJECT(BgL_varz00_2780))->
												BgL_typez00));
										{	/* Ast/toccur.scm 107 */
											obj_t BgL_g1261z00_2797;

											BgL_g1261z00_2797 =
												(((BgL_cfunz00_bglt) COBJECT(
														((BgL_cfunz00_bglt) BgL_valz00_2781)))->
												BgL_argszd2typezd2);
											{
												obj_t BgL_l1259z00_2799;

												BgL_l1259z00_2799 = BgL_g1261z00_2797;
											BgL_zc3z04anonymousza31486ze3z87_2798:
												if (PAIRP(BgL_l1259z00_2799))
													{	/* Ast/toccur.scm 107 */
														{	/* Ast/toccur.scm 107 */
															obj_t BgL_arg1489z00_2800;

															BgL_arg1489z00_2800 = CAR(BgL_l1259z00_2799);
															BGl_typezd2occurrencezd2incrementz12z12zztype_typez00
																(((BgL_typez00_bglt) BgL_arg1489z00_2800));
														}
														{
															obj_t BgL_l1259z00_3409;

															BgL_l1259z00_3409 = CDR(BgL_l1259z00_2799);
															BgL_l1259z00_2799 = BgL_l1259z00_3409;
															goto BgL_zc3z04anonymousza31486ze3z87_2798;
														}
													}
												else
													{	/* Ast/toccur.scm 107 */
														BgL_tmpz00_3364 = ((bool_t) 1);
													}
											}
										}
									}
								else
									{	/* Ast/toccur.scm 105 */
										BgL_tmpz00_3364 = ((bool_t) 0);
									}
							}
						}
					}
				}
				return BBOOL(BgL_tmpz00_3364);
			}
		}

	}



/* &occur-node!-sync1283 */
	obj_t BGl_z62occurzd2nodez12zd2sync1283z70zzast_typezd2occurzd2(obj_t
		BgL_envz00_2684, obj_t BgL_nodez00_2685)
	{
		{	/* Ast/toccur.scm 91 */
			BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2685)))->BgL_mutexz00));
			BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2685)))->BgL_prelockz00));
			return
				BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2685)))->BgL_bodyz00));
		}

	}



/* &occur-node!-sequence1281 */
	obj_t BGl_z62occurzd2nodez12zd2sequence1281z70zzast_typezd2occurzd2(obj_t
		BgL_envz00_2686, obj_t BgL_nodez00_2687)
	{
		{	/* Ast/toccur.scm 85 */
			return
				BBOOL(BGl_occurzd2nodeza2z12z62zzast_typezd2occurzd2(
					(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_2687)))->BgL_nodesz00)));
		}

	}



/* &occur-node!-var1279 */
	obj_t BGl_z62occurzd2nodez12zd2var1279z70zzast_typezd2occurzd2(obj_t
		BgL_envz00_2688, obj_t BgL_nodez00_2689)
	{
		{	/* Ast/toccur.scm 76 */
			{	/* Ast/toccur.scm 77 */
				BgL_variablez00_bglt BgL_vz00_2804;

				BgL_vz00_2804 =
					(((BgL_varz00_bglt) COBJECT(
							((BgL_varz00_bglt) BgL_nodez00_2689)))->BgL_variablez00);
				{	/* Ast/toccur.scm 78 */
					BgL_valuez00_bglt BgL_valuez00_2805;

					BgL_valuez00_2805 =
						(((BgL_variablez00_bglt) COBJECT(BgL_vz00_2804))->BgL_valuez00);
					{	/* Ast/toccur.scm 79 */
						bool_t BgL_test1970z00_3428;

						{	/* Ast/toccur.scm 79 */
							bool_t BgL_test1971z00_3429;

							{	/* Ast/toccur.scm 79 */
								obj_t BgL_classz00_2806;

								BgL_classz00_2806 = BGl_scnstz00zzast_varz00;
								{	/* Ast/toccur.scm 79 */
									BgL_objectz00_bglt BgL_arg1807z00_2807;

									{	/* Ast/toccur.scm 79 */
										obj_t BgL_tmpz00_3430;

										BgL_tmpz00_3430 =
											((obj_t) ((BgL_objectz00_bglt) BgL_valuez00_2805));
										BgL_arg1807z00_2807 =
											(BgL_objectz00_bglt) (BgL_tmpz00_3430);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Ast/toccur.scm 79 */
											long BgL_idxz00_2808;

											BgL_idxz00_2808 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2807);
											BgL_test1971z00_3429 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2808 + 2L)) == BgL_classz00_2806);
										}
									else
										{	/* Ast/toccur.scm 79 */
											bool_t BgL_res1858z00_2811;

											{	/* Ast/toccur.scm 79 */
												obj_t BgL_oclassz00_2812;

												{	/* Ast/toccur.scm 79 */
													obj_t BgL_arg1815z00_2813;
													long BgL_arg1816z00_2814;

													BgL_arg1815z00_2813 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Ast/toccur.scm 79 */
														long BgL_arg1817z00_2815;

														BgL_arg1817z00_2815 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2807);
														BgL_arg1816z00_2814 =
															(BgL_arg1817z00_2815 - OBJECT_TYPE);
													}
													BgL_oclassz00_2812 =
														VECTOR_REF(BgL_arg1815z00_2813,
														BgL_arg1816z00_2814);
												}
												{	/* Ast/toccur.scm 79 */
													bool_t BgL__ortest_1115z00_2816;

													BgL__ortest_1115z00_2816 =
														(BgL_classz00_2806 == BgL_oclassz00_2812);
													if (BgL__ortest_1115z00_2816)
														{	/* Ast/toccur.scm 79 */
															BgL_res1858z00_2811 = BgL__ortest_1115z00_2816;
														}
													else
														{	/* Ast/toccur.scm 79 */
															long BgL_odepthz00_2817;

															{	/* Ast/toccur.scm 79 */
																obj_t BgL_arg1804z00_2818;

																BgL_arg1804z00_2818 = (BgL_oclassz00_2812);
																BgL_odepthz00_2817 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2818);
															}
															if ((2L < BgL_odepthz00_2817))
																{	/* Ast/toccur.scm 79 */
																	obj_t BgL_arg1802z00_2819;

																	{	/* Ast/toccur.scm 79 */
																		obj_t BgL_arg1803z00_2820;

																		BgL_arg1803z00_2820 = (BgL_oclassz00_2812);
																		BgL_arg1802z00_2819 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2820, 2L);
																	}
																	BgL_res1858z00_2811 =
																		(BgL_arg1802z00_2819 == BgL_classz00_2806);
																}
															else
																{	/* Ast/toccur.scm 79 */
																	BgL_res1858z00_2811 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test1971z00_3429 = BgL_res1858z00_2811;
										}
								}
							}
							if (BgL_test1971z00_3429)
								{	/* Ast/toccur.scm 79 */
									obj_t BgL_arg1422z00_2821;

									BgL_arg1422z00_2821 =
										(((BgL_scnstz00_bglt) COBJECT(
												((BgL_scnstz00_bglt) BgL_valuez00_2805)))->BgL_nodez00);
									{	/* Ast/toccur.scm 79 */
										obj_t BgL_classz00_2822;

										BgL_classz00_2822 = BGl_nodez00zzast_nodez00;
										if (BGL_OBJECTP(BgL_arg1422z00_2821))
											{	/* Ast/toccur.scm 79 */
												BgL_objectz00_bglt BgL_arg1807z00_2823;

												BgL_arg1807z00_2823 =
													(BgL_objectz00_bglt) (BgL_arg1422z00_2821);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Ast/toccur.scm 79 */
														long BgL_idxz00_2824;

														BgL_idxz00_2824 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2823);
														BgL_test1970z00_3428 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2824 + 1L)) == BgL_classz00_2822);
													}
												else
													{	/* Ast/toccur.scm 79 */
														bool_t BgL_res1859z00_2827;

														{	/* Ast/toccur.scm 79 */
															obj_t BgL_oclassz00_2828;

															{	/* Ast/toccur.scm 79 */
																obj_t BgL_arg1815z00_2829;
																long BgL_arg1816z00_2830;

																BgL_arg1815z00_2829 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Ast/toccur.scm 79 */
																	long BgL_arg1817z00_2831;

																	BgL_arg1817z00_2831 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2823);
																	BgL_arg1816z00_2830 =
																		(BgL_arg1817z00_2831 - OBJECT_TYPE);
																}
																BgL_oclassz00_2828 =
																	VECTOR_REF(BgL_arg1815z00_2829,
																	BgL_arg1816z00_2830);
															}
															{	/* Ast/toccur.scm 79 */
																bool_t BgL__ortest_1115z00_2832;

																BgL__ortest_1115z00_2832 =
																	(BgL_classz00_2822 == BgL_oclassz00_2828);
																if (BgL__ortest_1115z00_2832)
																	{	/* Ast/toccur.scm 79 */
																		BgL_res1859z00_2827 =
																			BgL__ortest_1115z00_2832;
																	}
																else
																	{	/* Ast/toccur.scm 79 */
																		long BgL_odepthz00_2833;

																		{	/* Ast/toccur.scm 79 */
																			obj_t BgL_arg1804z00_2834;

																			BgL_arg1804z00_2834 =
																				(BgL_oclassz00_2828);
																			BgL_odepthz00_2833 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2834);
																		}
																		if ((1L < BgL_odepthz00_2833))
																			{	/* Ast/toccur.scm 79 */
																				obj_t BgL_arg1802z00_2835;

																				{	/* Ast/toccur.scm 79 */
																					obj_t BgL_arg1803z00_2836;

																					BgL_arg1803z00_2836 =
																						(BgL_oclassz00_2828);
																					BgL_arg1802z00_2835 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2836, 1L);
																				}
																				BgL_res1859z00_2827 =
																					(BgL_arg1802z00_2835 ==
																					BgL_classz00_2822);
																			}
																		else
																			{	/* Ast/toccur.scm 79 */
																				BgL_res1859z00_2827 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test1970z00_3428 = BgL_res1859z00_2827;
													}
											}
										else
											{	/* Ast/toccur.scm 79 */
												BgL_test1970z00_3428 = ((bool_t) 0);
											}
									}
								}
							else
								{	/* Ast/toccur.scm 79 */
									BgL_test1970z00_3428 = ((bool_t) 0);
								}
						}
						if (BgL_test1970z00_3428)
							{	/* Ast/toccur.scm 80 */
								obj_t BgL_arg1421z00_2837;

								BgL_arg1421z00_2837 =
									(((BgL_scnstz00_bglt) COBJECT(
											((BgL_scnstz00_bglt) BgL_valuez00_2805)))->BgL_nodez00);
								return
									BGl_occurzd2nodez12zc0zzast_typezd2occurzd2(
									((BgL_nodez00_bglt) BgL_arg1421z00_2837));
							}
						else
							{	/* Ast/toccur.scm 79 */
								return BFALSE;
							}
					}
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzast_typezd2occurzd2(void)
	{
		{	/* Ast/toccur.scm 14 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1899z00zzast_typezd2occurzd2));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1899z00zzast_typezd2occurzd2));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1899z00zzast_typezd2occurzd2));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1899z00zzast_typezd2occurzd2));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1899z00zzast_typezd2occurzd2));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1899z00zzast_typezd2occurzd2));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1899z00zzast_typezd2occurzd2));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string1899z00zzast_typezd2occurzd2));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string1899z00zzast_typezd2occurzd2));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1899z00zzast_typezd2occurzd2));
			return
				BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string1899z00zzast_typezd2occurzd2));
		}

	}

#ifdef __cplusplus
}
#endif
