/*===========================================================================*/
/*   (Object/creator.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Object/creator.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_OBJECT_CREATOR_TYPE_DEFINITIONS
#define BGL_OBJECT_CREATOR_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_jclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		obj_t BgL_packagez00;
	}                *BgL_jclassz00_bglt;


#endif													// BGL_OBJECT_CREATOR_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t
		BGl_z62importzd2javazd2classzd2creatorzb0zzobject_creatorz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	extern obj_t BGl_za2optimza2z00zzengine_paramz00;
	static obj_t BGl_requirezd2initializa7ationz75zzobject_creatorz00 = BUNSPEC;
	extern obj_t BGl_makezd2typedzd2identz00zzast_identz00(obj_t, obj_t);
	extern obj_t BGl_epairifyza2za2zztools_miscz00(obj_t, obj_t);
	extern obj_t BGl_fastzd2idzd2ofzd2idzd2zzast_identz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_importzd2javazd2classzd2creatorzd2zzobject_creatorz00(BgL_typez00_bglt,
		obj_t, obj_t);
	extern obj_t BGl_za2passza2z00zzengine_paramz00;
	BGL_IMPORT obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
	static obj_t
		BGl_z62genzd2javazd2classzd2constructorszb0zzobject_creatorz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzobject_creatorz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzobject_creatorz00(void);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t
		BGl_genzd2javazd2classzd2creatorzd2zzobject_creatorz00(BgL_typez00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzobject_creatorz00(void);
	static bool_t BGl_inlinezd2creatorszf3z21zzobject_creatorz00(void);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	static obj_t BGl_genzd2onezd2constructorze70ze7zzobject_creatorz00(obj_t,
		BgL_typez00_bglt, obj_t);
	extern obj_t BGl_producezd2modulezd2clausez12z12zzmodule_modulez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzobject_creatorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_impusez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_nilz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_privatez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	extern obj_t BGl_za2profilezd2modeza2zd2zzengine_paramz00;
	extern BgL_typez00_bglt BGl_typezd2ofzd2idz00zzast_identz00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
	extern obj_t BGl_makezd2privatezd2sexpz00zzast_privatez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zzobject_creatorz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzobject_creatorz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzobject_creatorz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzobject_creatorz00(void);
	BGL_IMPORT obj_t BGl_consza2za2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	extern obj_t BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_genzd2javazd2classzd2constructorszd2zzobject_creatorz00
		(BgL_typez00_bglt, obj_t, obj_t);
	static obj_t __cnst[10];


	   
		 
		DEFINE_STRING(BGl_string1345z00zzobject_creatorz00,
		BgL_bgl_string1345za700za7za7o1349za7, "object_creator", 14);
	      DEFINE_STRING(BGl_string1346z00zzobject_creatorz00,
		BgL_bgl_string1346za700za7za7o1350za7,
		"export define quote - static inline define-inline new %allocate- (make-heap make-add-heap) ",
		91);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genzd2javazd2classzd2constructorszd2envz00zzobject_creatorz00,
		BgL_bgl_za762genza7d2javaza7d21351za7,
		BGl_z62genzd2javazd2classzd2constructorszb0zzobject_creatorz00, 0L, BUNSPEC,
		3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_importzd2javazd2classzd2creatorzd2envz00zzobject_creatorz00,
		BgL_bgl_za762importza7d2java1352z00,
		BGl_z62importzd2javazd2classzd2creatorzb0zzobject_creatorz00, 0L, BUNSPEC,
		3);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzobject_creatorz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzobject_creatorz00(long
		BgL_checksumz00_1079, char *BgL_fromz00_1080)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzobject_creatorz00))
				{
					BGl_requirezd2initializa7ationz75zzobject_creatorz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzobject_creatorz00();
					BGl_libraryzd2moduleszd2initz00zzobject_creatorz00();
					BGl_cnstzd2initzd2zzobject_creatorz00();
					BGl_importedzd2moduleszd2initz00zzobject_creatorz00();
					return BGl_methodzd2initzd2zzobject_creatorz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzobject_creatorz00(void)
	{
		{	/* Object/creator.scm 21 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"object_creator");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"object_creator");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"object_creator");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "object_creator");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"object_creator");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"object_creator");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"object_creator");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzobject_creatorz00(void)
	{
		{	/* Object/creator.scm 21 */
			{	/* Object/creator.scm 21 */
				obj_t BgL_cportz00_1068;

				{	/* Object/creator.scm 21 */
					obj_t BgL_stringz00_1075;

					BgL_stringz00_1075 = BGl_string1346z00zzobject_creatorz00;
					{	/* Object/creator.scm 21 */
						obj_t BgL_startz00_1076;

						BgL_startz00_1076 = BINT(0L);
						{	/* Object/creator.scm 21 */
							obj_t BgL_endz00_1077;

							BgL_endz00_1077 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1075)));
							{	/* Object/creator.scm 21 */

								BgL_cportz00_1068 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1075, BgL_startz00_1076, BgL_endz00_1077);
				}}}}
				{
					long BgL_iz00_1069;

					BgL_iz00_1069 = 9L;
				BgL_loopz00_1070:
					if ((BgL_iz00_1069 == -1L))
						{	/* Object/creator.scm 21 */
							return BUNSPEC;
						}
					else
						{	/* Object/creator.scm 21 */
							{	/* Object/creator.scm 21 */
								obj_t BgL_arg1348z00_1071;

								{	/* Object/creator.scm 21 */

									{	/* Object/creator.scm 21 */
										obj_t BgL_locationz00_1073;

										BgL_locationz00_1073 = BBOOL(((bool_t) 0));
										{	/* Object/creator.scm 21 */

											BgL_arg1348z00_1071 =
												BGl_readz00zz__readerz00(BgL_cportz00_1068,
												BgL_locationz00_1073);
										}
									}
								}
								{	/* Object/creator.scm 21 */
									int BgL_tmpz00_1105;

									BgL_tmpz00_1105 = (int) (BgL_iz00_1069);
									CNST_TABLE_SET(BgL_tmpz00_1105, BgL_arg1348z00_1071);
							}}
							{	/* Object/creator.scm 21 */
								int BgL_auxz00_1074;

								BgL_auxz00_1074 = (int) ((BgL_iz00_1069 - 1L));
								{
									long BgL_iz00_1110;

									BgL_iz00_1110 = (long) (BgL_auxz00_1074);
									BgL_iz00_1069 = BgL_iz00_1110;
									goto BgL_loopz00_1070;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzobject_creatorz00(void)
	{
		{	/* Object/creator.scm 21 */
			return bgl_gc_roots_register();
		}

	}



/* inline-creators? */
	bool_t BGl_inlinezd2creatorszf3z21zzobject_creatorz00(void)
	{
		{	/* Object/creator.scm 51 */
			if (((long) CINT(BGl_za2optimza2z00zzengine_paramz00) >= 2L))
				{	/* Object/creator.scm 53 */
					bool_t BgL_test1357z00_1116;

					{	/* Object/creator.scm 53 */
						bool_t BgL__ortest_1079z00_817;

						if (BGl_numberzf3zf3zz__r4_numbers_6_5z00
							(BGl_za2profilezd2modeza2zd2zzengine_paramz00))
							{	/* Object/creator.scm 53 */
								BgL__ortest_1079z00_817 = ((bool_t) 0);
							}
						else
							{	/* Object/creator.scm 53 */
								BgL__ortest_1079z00_817 = ((bool_t) 1);
							}
						if (BgL__ortest_1079z00_817)
							{	/* Object/creator.scm 53 */
								BgL_test1357z00_1116 = BgL__ortest_1079z00_817;
							}
						else
							{	/* Object/creator.scm 53 */
								BgL_test1357z00_1116 =
									(
									(long) CINT(BGl_za2profilezd2modeza2zd2zzengine_paramz00) <
									1L);
					}}
					if (BgL_test1357z00_1116)
						{	/* Object/creator.scm 53 */
							if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
									(BGl_za2passza2z00zzengine_paramz00, CNST_TABLE_REF(0))))
								{	/* Object/creator.scm 55 */
									return ((bool_t) 0);
								}
							else
								{	/* Object/creator.scm 55 */
									return ((bool_t) 1);
								}
						}
					else
						{	/* Object/creator.scm 53 */
							return ((bool_t) 0);
						}
				}
			else
				{	/* Object/creator.scm 52 */
					return ((bool_t) 0);
				}
		}

	}



/* gen-java-class-creator */
	obj_t BGl_genzd2javazd2classzd2creatorzd2zzobject_creatorz00(BgL_typez00_bglt
		BgL_classz00_3, obj_t BgL_srczd2defzd2_4)
	{
		{	/* Object/creator.scm 60 */
			{	/* Object/creator.scm 61 */
				obj_t BgL_idz00_819;

				BgL_idz00_819 =
					(((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_classz00_3)))->BgL_idz00);
				{	/* Object/creator.scm 61 */
					obj_t BgL_alloczd2idzd2_820;

					{	/* Object/creator.scm 62 */
						obj_t BgL_arg1190z00_837;

						{	/* Object/creator.scm 62 */
							obj_t BgL_arg1191z00_838;
							obj_t BgL_arg1193z00_839;

							{	/* Object/creator.scm 62 */
								obj_t BgL_symbolz00_1019;

								BgL_symbolz00_1019 = CNST_TABLE_REF(1);
								{	/* Object/creator.scm 62 */
									obj_t BgL_arg1455z00_1020;

									BgL_arg1455z00_1020 = SYMBOL_TO_STRING(BgL_symbolz00_1019);
									BgL_arg1191z00_838 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_1020);
								}
							}
							{	/* Object/creator.scm 62 */
								obj_t BgL_arg1455z00_1022;

								BgL_arg1455z00_1022 = SYMBOL_TO_STRING(BgL_idz00_819);
								BgL_arg1193z00_839 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
									(BgL_arg1455z00_1022);
							}
							BgL_arg1190z00_837 =
								string_append(BgL_arg1191z00_838, BgL_arg1193z00_839);
						}
						BgL_alloczd2idzd2_820 = bstring_to_symbol(BgL_arg1190z00_837);
					}
					{	/* Object/creator.scm 62 */

						{

							{	/* Object/creator.scm 66 */
								obj_t BgL_arg1157z00_822;

								{	/* Object/creator.scm 66 */
									obj_t BgL_arg1158z00_823;

									{	/* Object/creator.scm 66 */
										obj_t BgL_arg1162z00_824;

										{	/* Object/creator.scm 66 */
											obj_t BgL_arg1164z00_825;

											BgL_arg1164z00_825 =
												MAKE_YOUNG_PAIR(BgL_alloczd2idzd2_820, BNIL);
											BgL_arg1162z00_824 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(4), BgL_arg1164z00_825);
										}
										BgL_arg1158z00_823 =
											MAKE_YOUNG_PAIR(BgL_arg1162z00_824, BNIL);
									}
									BgL_arg1157z00_822 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(5), BgL_arg1158z00_823);
								}
								BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
									(BgL_arg1157z00_822);
							}
							{	/* Object/creator.scm 67 */
								obj_t BgL_arg1166z00_826;

								{	/* Object/creator.scm 67 */
									obj_t BgL_arg1171z00_828;

									{	/* Object/creator.scm 64 */
										obj_t BgL_arg1182z00_831;

										{	/* Object/creator.scm 64 */
											obj_t BgL_arg1183z00_832;
											obj_t BgL_arg1187z00_833;

											BgL_arg1183z00_832 =
												MAKE_YOUNG_PAIR(BgL_alloczd2idzd2_820, BNIL);
											BgL_arg1187z00_833 =
												MAKE_YOUNG_PAIR
												(BGl_makezd2privatezd2sexpz00zzast_privatez00
												(CNST_TABLE_REF(2), BgL_idz00_819, BNIL), BNIL);
											BgL_arg1182z00_831 =
												MAKE_YOUNG_PAIR(BgL_arg1183z00_832, BgL_arg1187z00_833);
										}
										BgL_arg1171z00_828 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BgL_arg1182z00_831);
									}
									{	/* Object/creator.scm 67 */
										obj_t BgL_list1172z00_829;

										BgL_list1172z00_829 =
											MAKE_YOUNG_PAIR(BgL_srczd2defzd2_4, BNIL);
										BgL_arg1166z00_826 =
											BGl_epairifyza2za2zztools_miscz00(BgL_arg1171z00_828,
											BgL_list1172z00_829);
									}
								}
								{	/* Object/creator.scm 67 */
									obj_t BgL_list1167z00_827;

									BgL_list1167z00_827 =
										MAKE_YOUNG_PAIR(BgL_arg1166z00_826, BNIL);
									return BgL_list1167z00_827;
								}
							}
						}
					}
				}
			}
		}

	}



/* import-java-class-creator */
	BGL_EXPORTED_DEF obj_t
		BGl_importzd2javazd2classzd2creatorzd2zzobject_creatorz00(BgL_typez00_bglt
		BgL_classz00_5, obj_t BgL_constrsz00_6, obj_t BgL_srczd2defzd2_7)
	{
		{	/* Object/creator.scm 72 */
			if (NULLP(BgL_constrsz00_6))
				{	/* Object/creator.scm 73 */
					return BNIL;
				}
			else
				{	/* Object/creator.scm 73 */
					return
						BGl_genzd2javazd2classzd2creatorzd2zzobject_creatorz00
						(BgL_classz00_5, BgL_srczd2defzd2_7);
				}
		}

	}



/* &import-java-class-creator */
	obj_t BGl_z62importzd2javazd2classzd2creatorzb0zzobject_creatorz00(obj_t
		BgL_envz00_1058, obj_t BgL_classz00_1059, obj_t BgL_constrsz00_1060,
		obj_t BgL_srczd2defzd2_1061)
	{
		{	/* Object/creator.scm 72 */
			return
				BGl_importzd2javazd2classzd2creatorzd2zzobject_creatorz00(
				((BgL_typez00_bglt) BgL_classz00_1059), BgL_constrsz00_1060,
				BgL_srczd2defzd2_1061);
		}

	}



/* gen-java-class-constructors */
	BGL_EXPORTED_DEF obj_t
		BGl_genzd2javazd2classzd2constructorszd2zzobject_creatorz00(BgL_typez00_bglt
		BgL_classz00_8, obj_t BgL_constrsz00_9, obj_t BgL_srczd2defzd2_10)
	{
		{	/* Object/creator.scm 80 */
			if (NULLP(BgL_constrsz00_9))
				{	/* Object/creator.scm 110 */
					return BNIL;
				}
			else
				{	/* Object/creator.scm 110 */
					obj_t BgL_head1144z00_844;

					{	/* Object/creator.scm 110 */
						obj_t BgL_arg1201z00_856;

						{	/* Object/creator.scm 110 */
							obj_t BgL_arg1202z00_857;

							BgL_arg1202z00_857 = CAR(((obj_t) BgL_constrsz00_9));
							BgL_arg1201z00_856 =
								BGl_genzd2onezd2constructorze70ze7zzobject_creatorz00
								(BgL_constrsz00_9, BgL_classz00_8, BgL_arg1202z00_857);
						}
						BgL_head1144z00_844 = MAKE_YOUNG_PAIR(BgL_arg1201z00_856, BNIL);
					}
					{	/* Object/creator.scm 110 */
						obj_t BgL_g1147z00_845;

						BgL_g1147z00_845 = CDR(((obj_t) BgL_constrsz00_9));
						{
							obj_t BgL_l1142z00_847;
							obj_t BgL_tail1145z00_848;

							BgL_l1142z00_847 = BgL_g1147z00_845;
							BgL_tail1145z00_848 = BgL_head1144z00_844;
						BgL_zc3z04anonymousza31196ze3z87_849:
							if (NULLP(BgL_l1142z00_847))
								{	/* Object/creator.scm 110 */
									return BgL_head1144z00_844;
								}
							else
								{	/* Object/creator.scm 110 */
									obj_t BgL_newtail1146z00_851;

									{	/* Object/creator.scm 110 */
										obj_t BgL_arg1199z00_853;

										{	/* Object/creator.scm 110 */
											obj_t BgL_arg1200z00_854;

											BgL_arg1200z00_854 = CAR(((obj_t) BgL_l1142z00_847));
											BgL_arg1199z00_853 =
												BGl_genzd2onezd2constructorze70ze7zzobject_creatorz00
												(BgL_constrsz00_9, BgL_classz00_8, BgL_arg1200z00_854);
										}
										BgL_newtail1146z00_851 =
											MAKE_YOUNG_PAIR(BgL_arg1199z00_853, BNIL);
									}
									SET_CDR(BgL_tail1145z00_848, BgL_newtail1146z00_851);
									{	/* Object/creator.scm 110 */
										obj_t BgL_arg1198z00_852;

										BgL_arg1198z00_852 = CDR(((obj_t) BgL_l1142z00_847));
										{
											obj_t BgL_tail1145z00_1175;
											obj_t BgL_l1142z00_1174;

											BgL_l1142z00_1174 = BgL_arg1198z00_852;
											BgL_tail1145z00_1175 = BgL_newtail1146z00_851;
											BgL_tail1145z00_848 = BgL_tail1145z00_1175;
											BgL_l1142z00_847 = BgL_l1142z00_1174;
											goto BgL_zc3z04anonymousza31196ze3z87_849;
										}
									}
								}
						}
					}
				}
		}

	}



/* gen-one-constructor~0 */
	obj_t BGl_genzd2onezd2constructorze70ze7zzobject_creatorz00(obj_t
		BgL_constrsz00_1067, BgL_typez00_bglt BgL_classz00_1066,
		obj_t BgL_constrz00_858)
	{
		{	/* Object/creator.scm 109 */
			{
				obj_t BgL_identz00_860;
				obj_t BgL_argszd2typezd2_861;

				if (PAIRP(BgL_constrz00_858))
					{	/* Object/creator.scm 109 */
						obj_t BgL_arg1206z00_865;
						obj_t BgL_arg1208z00_866;

						BgL_arg1206z00_865 = CAR(((obj_t) BgL_constrz00_858));
						BgL_arg1208z00_866 = CDR(((obj_t) BgL_constrz00_858));
						BgL_identz00_860 = BgL_arg1206z00_865;
						BgL_argszd2typezd2_861 = BgL_arg1208z00_866;
						{	/* Object/creator.scm 87 */
							obj_t BgL_jidz00_867;

							BgL_jidz00_867 =
								(((BgL_typez00_bglt) COBJECT(
										((BgL_typez00_bglt) BgL_classz00_1066)))->BgL_idz00);
							{	/* Object/creator.scm 87 */
								obj_t BgL_locz00_868;

								BgL_locz00_868 =
									BGl_findzd2locationzd2zztools_locationz00
									(BgL_constrsz00_1067);
								{	/* Object/creator.scm 88 */
									obj_t BgL_cidz00_869;

									BgL_cidz00_869 =
										BGl_fastzd2idzd2ofzd2idzd2zzast_identz00(BgL_identz00_860,
										BgL_locz00_868);
									{	/* Object/creator.scm 89 */
										obj_t BgL_tidz00_870;

										{	/* Object/creator.scm 90 */
											obj_t BgL_arg1312z00_948;

											{	/* Object/creator.scm 90 */
												obj_t BgL_list1313z00_949;

												{	/* Object/creator.scm 90 */
													obj_t BgL_arg1314z00_950;

													{	/* Object/creator.scm 90 */
														obj_t BgL_arg1315z00_951;

														BgL_arg1315z00_951 =
															MAKE_YOUNG_PAIR(BgL_cidz00_869, BNIL);
														BgL_arg1314z00_950 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
															BgL_arg1315z00_951);
													}
													BgL_list1313z00_949 =
														MAKE_YOUNG_PAIR(BgL_jidz00_867, BgL_arg1314z00_950);
												}
												BgL_arg1312z00_948 =
													BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
													(BgL_list1313z00_949);
											}
											BgL_tidz00_870 =
												BGl_makezd2typedzd2identz00zzast_identz00
												(BgL_arg1312z00_948, BgL_jidz00_867);
										}
										{	/* Object/creator.scm 90 */
											obj_t BgL_argszd2idzd2_871;

											if (NULLP(BgL_argszd2typezd2_861))
												{	/* Object/creator.scm 91 */
													BgL_argszd2idzd2_871 = BNIL;
												}
											else
												{	/* Object/creator.scm 91 */
													obj_t BgL_head1128z00_935;

													BgL_head1128z00_935 = MAKE_YOUNG_PAIR(BNIL, BNIL);
													{
														obj_t BgL_l1126z00_937;
														obj_t BgL_tail1129z00_938;

														BgL_l1126z00_937 = BgL_argszd2typezd2_861;
														BgL_tail1129z00_938 = BgL_head1128z00_935;
													BgL_zc3z04anonymousza31306ze3z87_939:
														if (NULLP(BgL_l1126z00_937))
															{	/* Object/creator.scm 91 */
																BgL_argszd2idzd2_871 = CDR(BgL_head1128z00_935);
															}
														else
															{	/* Object/creator.scm 91 */
																obj_t BgL_newtail1130z00_941;

																{	/* Object/creator.scm 91 */
																	obj_t BgL_arg1310z00_943;

																	{	/* Object/creator.scm 91 */
																		obj_t BgL_arg1311z00_945;

																		{	/* Object/creator.scm 91 */

																			{	/* Object/creator.scm 91 */

																				BgL_arg1311z00_945 =
																					BGl_gensymz00zz__r4_symbols_6_4z00
																					(BFALSE);
																			}
																		}
																		BgL_arg1310z00_943 =
																			BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																			(BgL_arg1311z00_945);
																	}
																	BgL_newtail1130z00_941 =
																		MAKE_YOUNG_PAIR(BgL_arg1310z00_943, BNIL);
																}
																SET_CDR(BgL_tail1129z00_938,
																	BgL_newtail1130z00_941);
																{	/* Object/creator.scm 91 */
																	obj_t BgL_arg1308z00_942;

																	BgL_arg1308z00_942 =
																		CDR(((obj_t) BgL_l1126z00_937));
																	{
																		obj_t BgL_tail1129z00_1205;
																		obj_t BgL_l1126z00_1204;

																		BgL_l1126z00_1204 = BgL_arg1308z00_942;
																		BgL_tail1129z00_1205 =
																			BgL_newtail1130z00_941;
																		BgL_tail1129z00_938 = BgL_tail1129z00_1205;
																		BgL_l1126z00_937 = BgL_l1126z00_1204;
																		goto BgL_zc3z04anonymousza31306ze3z87_939;
																	}
																}
															}
													}
												}
											{	/* Object/creator.scm 91 */
												obj_t BgL_selfz00_872;

												{	/* Object/creator.scm 93 */
													obj_t BgL_arg1304z00_931;

													{	/* Object/creator.scm 93 */

														{	/* Object/creator.scm 93 */

															BgL_arg1304z00_931 =
																BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
														}
													}
													BgL_selfz00_872 =
														BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
														(BgL_arg1304z00_931);
												}
												{	/* Object/creator.scm 93 */
													obj_t BgL_tselfz00_873;

													BgL_tselfz00_873 =
														BGl_makezd2typedzd2identz00zzast_identz00
														(BgL_selfz00_872, BgL_jidz00_867);
													{	/* Object/creator.scm 94 */
														obj_t BgL_targsz00_874;

														if (NULLP(BgL_argszd2idzd2_871))
															{	/* Object/creator.scm 95 */
																BgL_targsz00_874 = BNIL;
															}
														else
															{	/* Object/creator.scm 95 */
																obj_t BgL_head1133z00_914;

																BgL_head1133z00_914 =
																	MAKE_YOUNG_PAIR(BNIL, BNIL);
																{
																	obj_t BgL_ll1131z00_916;
																	obj_t BgL_ll1132z00_917;
																	obj_t BgL_tail1134z00_918;

																	BgL_ll1131z00_916 = BgL_argszd2idzd2_871;
																	BgL_ll1132z00_917 = BgL_argszd2typezd2_861;
																	BgL_tail1134z00_918 = BgL_head1133z00_914;
																BgL_zc3z04anonymousza31243ze3z87_919:
																	if (NULLP(BgL_ll1131z00_916))
																		{	/* Object/creator.scm 95 */
																			BgL_targsz00_874 =
																				CDR(BgL_head1133z00_914);
																		}
																	else
																		{	/* Object/creator.scm 95 */
																			obj_t BgL_newtail1135z00_921;

																			{	/* Object/creator.scm 95 */
																				obj_t BgL_arg1252z00_924;

																				{	/* Object/creator.scm 95 */
																					obj_t BgL_idz00_925;
																					obj_t BgL_typez00_926;

																					BgL_idz00_925 =
																						CAR(((obj_t) BgL_ll1131z00_916));
																					BgL_typez00_926 =
																						CAR(((obj_t) BgL_ll1132z00_917));
																					{	/* Object/creator.scm 96 */
																						obj_t BgL_arg1268z00_927;

																						{	/* Object/creator.scm 96 */
																							obj_t BgL_arg1272z00_928;
																							obj_t BgL_arg1284z00_929;

																							{	/* Object/creator.scm 96 */
																								obj_t BgL_arg1455z00_1035;

																								BgL_arg1455z00_1035 =
																									SYMBOL_TO_STRING(
																									((obj_t) BgL_idz00_925));
																								BgL_arg1272z00_928 =
																									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																									(BgL_arg1455z00_1035);
																							}
																							{	/* Object/creator.scm 96 */
																								obj_t BgL_arg1455z00_1037;

																								BgL_arg1455z00_1037 =
																									SYMBOL_TO_STRING(
																									((obj_t) BgL_typez00_926));
																								BgL_arg1284z00_929 =
																									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																									(BgL_arg1455z00_1037);
																							}
																							BgL_arg1268z00_927 =
																								string_append
																								(BgL_arg1272z00_928,
																								BgL_arg1284z00_929);
																						}
																						BgL_arg1252z00_924 =
																							bstring_to_symbol
																							(BgL_arg1268z00_927);
																					}
																				}
																				BgL_newtail1135z00_921 =
																					MAKE_YOUNG_PAIR(BgL_arg1252z00_924,
																					BNIL);
																			}
																			SET_CDR(BgL_tail1134z00_918,
																				BgL_newtail1135z00_921);
																			{	/* Object/creator.scm 95 */
																				obj_t BgL_arg1248z00_922;
																				obj_t BgL_arg1249z00_923;

																				BgL_arg1248z00_922 =
																					CDR(((obj_t) BgL_ll1131z00_916));
																				BgL_arg1249z00_923 =
																					CDR(((obj_t) BgL_ll1132z00_917));
																				{
																					obj_t BgL_tail1134z00_1235;
																					obj_t BgL_ll1132z00_1234;
																					obj_t BgL_ll1131z00_1233;

																					BgL_ll1131z00_1233 =
																						BgL_arg1248z00_922;
																					BgL_ll1132z00_1234 =
																						BgL_arg1249z00_923;
																					BgL_tail1134z00_1235 =
																						BgL_newtail1135z00_921;
																					BgL_tail1134z00_918 =
																						BgL_tail1134z00_1235;
																					BgL_ll1132z00_917 =
																						BgL_ll1132z00_1234;
																					BgL_ll1131z00_916 =
																						BgL_ll1131z00_1233;
																					goto
																						BgL_zc3z04anonymousza31243ze3z87_919;
																				}
																			}
																		}
																}
															}
														{	/* Object/creator.scm 95 */
															obj_t BgL_defz00_875;

															{	/* Object/creator.scm 98 */
																obj_t BgL_arg1218z00_883;

																{	/* Object/creator.scm 98 */
																	obj_t BgL_arg1219z00_884;
																	obj_t BgL_arg1220z00_885;

																	BgL_arg1219z00_884 =
																		MAKE_YOUNG_PAIR(BgL_tidz00_870,
																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																		(BgL_targsz00_874, BNIL));
																	{	/* Object/creator.scm 100 */
																		obj_t BgL_arg1223z00_887;

																		{	/* Object/creator.scm 100 */
																			obj_t BgL_runner1241z00_910;

																			{	/* Object/creator.scm 101 */
																				obj_t BgL_arg1225z00_888;

																				{	/* Object/creator.scm 101 */
																					obj_t BgL_arg1229z00_892;

																					{	/* Object/creator.scm 101 */
																						obj_t BgL_arg1230z00_893;

																						if (NULLP(BgL_argszd2typezd2_861))
																							{	/* Object/creator.scm 101 */
																								BgL_arg1230z00_893 = BNIL;
																							}
																						else
																							{	/* Object/creator.scm 101 */
																								obj_t BgL_head1139z00_896;

																								BgL_head1139z00_896 =
																									MAKE_YOUNG_PAIR(BNIL, BNIL);
																								{
																									obj_t BgL_l1137z00_898;
																									obj_t BgL_tail1140z00_899;

																									BgL_l1137z00_898 =
																										BgL_argszd2typezd2_861;
																									BgL_tail1140z00_899 =
																										BgL_head1139z00_896;
																								BgL_zc3z04anonymousza31232ze3z87_900:
																									if (NULLP
																										(BgL_l1137z00_898))
																										{	/* Object/creator.scm 101 */
																											BgL_arg1230z00_893 =
																												CDR
																												(BgL_head1139z00_896);
																										}
																									else
																										{	/* Object/creator.scm 101 */
																											obj_t
																												BgL_newtail1141z00_902;
																											{	/* Object/creator.scm 101 */
																												obj_t
																													BgL_arg1236z00_904;
																												{	/* Object/creator.scm 101 */
																													obj_t BgL_az00_905;

																													BgL_az00_905 =
																														CAR(
																														((obj_t)
																															BgL_l1137z00_898));
																													BgL_arg1236z00_904 =
																														(((BgL_typez00_bglt)
																															COBJECT
																															(BGl_typezd2ofzd2idz00zzast_identz00
																																(BgL_az00_905,
																																	BgL_locz00_868)))->
																														BgL_idz00);
																												}
																												BgL_newtail1141z00_902 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1236z00_904,
																													BNIL);
																											}
																											SET_CDR
																												(BgL_tail1140z00_899,
																												BgL_newtail1141z00_902);
																											{	/* Object/creator.scm 101 */
																												obj_t
																													BgL_arg1234z00_903;
																												BgL_arg1234z00_903 =
																													CDR(((obj_t)
																														BgL_l1137z00_898));
																												{
																													obj_t
																														BgL_tail1140z00_1253;
																													obj_t
																														BgL_l1137z00_1252;
																													BgL_l1137z00_1252 =
																														BgL_arg1234z00_903;
																													BgL_tail1140z00_1253 =
																														BgL_newtail1141z00_902;
																													BgL_tail1140z00_899 =
																														BgL_tail1140z00_1253;
																													BgL_l1137z00_898 =
																														BgL_l1137z00_1252;
																													goto
																														BgL_zc3z04anonymousza31232ze3z87_900;
																												}
																											}
																										}
																								}
																							}
																						BgL_arg1229z00_892 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1230z00_893, BNIL);
																					}
																					BgL_arg1225z00_888 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(7),
																						BgL_arg1229z00_892);
																				}
																				{	/* Object/creator.scm 100 */
																					obj_t BgL_list1226z00_889;

																					{	/* Object/creator.scm 100 */
																						obj_t BgL_arg1227z00_890;

																						{	/* Object/creator.scm 100 */
																							obj_t BgL_arg1228z00_891;

																							BgL_arg1228z00_891 =
																								MAKE_YOUNG_PAIR
																								(BgL_argszd2idzd2_871, BNIL);
																							BgL_arg1227z00_890 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1225z00_888,
																								BgL_arg1228z00_891);
																						}
																						BgL_list1226z00_889 =
																							MAKE_YOUNG_PAIR(BgL_jidz00_867,
																							BgL_arg1227z00_890);
																					}
																					BgL_runner1241z00_910 =
																						BGl_consza2za2zz__r4_pairs_and_lists_6_3z00
																						(CNST_TABLE_REF(2),
																						BgL_list1226z00_889);
																				}
																			}
																			{	/* Object/creator.scm 100 */
																				obj_t BgL_aux1239z00_908;

																				BgL_aux1239z00_908 =
																					CAR(BgL_runner1241z00_910);
																				BgL_runner1241z00_910 =
																					CDR(BgL_runner1241z00_910);
																				{	/* Object/creator.scm 100 */
																					obj_t BgL_aux1240z00_909;

																					BgL_aux1240z00_909 =
																						CAR(BgL_runner1241z00_910);
																					BgL_runner1241z00_910 =
																						CDR(BgL_runner1241z00_910);
																					BgL_arg1223z00_887 =
																						BGl_makezd2privatezd2sexpz00zzast_privatez00
																						(BgL_aux1239z00_908,
																						BgL_aux1240z00_909,
																						BgL_runner1241z00_910);
																				}
																			}
																		}
																		BgL_arg1220z00_885 =
																			MAKE_YOUNG_PAIR(BgL_arg1223z00_887, BNIL);
																	}
																	BgL_arg1218z00_883 =
																		MAKE_YOUNG_PAIR(BgL_arg1219z00_884,
																		BgL_arg1220z00_885);
																}
																BgL_defz00_875 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(8),
																	BgL_arg1218z00_883);
															}
															{	/* Object/creator.scm 98 */
																obj_t BgL_scopez00_876;

																if (BGl_inlinezd2creatorszf3z21zzobject_creatorz00())
																	{	/* Object/creator.scm 106 */
																		BgL_scopez00_876 = CNST_TABLE_REF(5);
																	}
																else
																	{	/* Object/creator.scm 106 */
																		BgL_scopez00_876 = CNST_TABLE_REF(9);
																	}
																{	/* Object/creator.scm 106 */
																	obj_t BgL_modz00_877;

																	{	/* Object/creator.scm 107 */
																		obj_t BgL_arg1210z00_879;

																		{	/* Object/creator.scm 107 */
																			obj_t BgL_arg1212z00_880;

																			BgL_arg1212z00_880 =
																				MAKE_YOUNG_PAIR(BgL_tidz00_870,
																				BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																				(BgL_argszd2typezd2_861, BNIL));
																			BgL_arg1210z00_879 =
																				MAKE_YOUNG_PAIR(BgL_arg1212z00_880,
																				BNIL);
																		}
																		BgL_modz00_877 =
																			MAKE_YOUNG_PAIR(BgL_scopez00_876,
																			BgL_arg1210z00_879);
																	}
																	{	/* Object/creator.scm 107 */

																		BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
																			(BgL_modz00_877);
																		{	/* Object/creator.scm 109 */
																			obj_t BgL_list1209z00_878;

																			BgL_list1209z00_878 =
																				MAKE_YOUNG_PAIR(BgL_constrz00_858,
																				BNIL);
																			return
																				BGl_epairifyza2za2zztools_miscz00
																				(BgL_defz00_875, BgL_list1209z00_878);
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				else
					{	/* Object/creator.scm 109 */
						return BFALSE;
					}
			}
		}

	}



/* &gen-java-class-constructors */
	obj_t BGl_z62genzd2javazd2classzd2constructorszb0zzobject_creatorz00(obj_t
		BgL_envz00_1062, obj_t BgL_classz00_1063, obj_t BgL_constrsz00_1064,
		obj_t BgL_srczd2defzd2_1065)
	{
		{	/* Object/creator.scm 80 */
			return
				BGl_genzd2javazd2classzd2constructorszd2zzobject_creatorz00(
				((BgL_typez00_bglt) BgL_classz00_1063), BgL_constrsz00_1064,
				BgL_srczd2defzd2_1065);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzobject_creatorz00(void)
	{
		{	/* Object/creator.scm 21 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzobject_creatorz00(void)
	{
		{	/* Object/creator.scm 21 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzobject_creatorz00(void)
	{
		{	/* Object/creator.scm 21 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzobject_creatorz00(void)
	{
		{	/* Object/creator.scm 21 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1345z00zzobject_creatorz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1345z00zzobject_creatorz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1345z00zzobject_creatorz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1345z00zzobject_creatorz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1345z00zzobject_creatorz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1345z00zzobject_creatorz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1345z00zzobject_creatorz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1345z00zzobject_creatorz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1345z00zzobject_creatorz00));
			BGl_modulezd2initializa7ationz75zztype_toolsz00(453414928L,
				BSTRING_TO_STRING(BGl_string1345z00zzobject_creatorz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1345z00zzobject_creatorz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1345z00zzobject_creatorz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1345z00zzobject_creatorz00));
			BGl_modulezd2initializa7ationz75zzast_privatez00(135263837L,
				BSTRING_TO_STRING(BGl_string1345z00zzobject_creatorz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1345z00zzobject_creatorz00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string1345z00zzobject_creatorz00));
			BGl_modulezd2initializa7ationz75zzobject_toolsz00(196511171L,
				BSTRING_TO_STRING(BGl_string1345z00zzobject_creatorz00));
			BGl_modulezd2initializa7ationz75zzobject_nilz00(168473056L,
				BSTRING_TO_STRING(BGl_string1345z00zzobject_creatorz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1345z00zzobject_creatorz00));
			return
				BGl_modulezd2initializa7ationz75zzmodule_impusez00(478324304L,
				BSTRING_TO_STRING(BGl_string1345z00zzobject_creatorz00));
		}

	}

#ifdef __cplusplus
}
#endif
