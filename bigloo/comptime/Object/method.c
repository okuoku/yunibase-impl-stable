/*===========================================================================*/
/*   (Object/method.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Object/method.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_OBJECT_METHOD_TYPE_DEFINITIONS
#define BGL_OBJECT_METHOD_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_tclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		struct BgL_globalz00_bgl *BgL_holderz00;
		obj_t BgL_wideningz00;
		long BgL_depthz00;
		bool_t BgL_finalzf3zf3;
		obj_t BgL_constructorz00;
		obj_t BgL_virtualzd2slotszd2numberz00;
		bool_t BgL_abstractzf3zf3;
		obj_t BgL_widezd2typezd2;
		obj_t BgL_subclassesz00;
	}                *BgL_tclassz00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;


#endif													// BGL_OBJECT_METHOD_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t
		BGl_makezd2methodzd2nozd2dssslzd2bodyz00zzobject_methodz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzobject_methodz00 = BUNSPEC;
	extern obj_t BGl_makezd2typedzd2identz00zzast_identz00(obj_t, obj_t);
	static obj_t BGl_z62localzd2iszd2methodzf3z91zzobject_methodz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_tclassz00zzobject_classz00;
	static obj_t BGl_za2methodsza2z00zzobject_methodz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zzobject_methodz00(void);
	BGL_IMPORT obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_genericzd2initzd2zzobject_methodz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzobject_methodz00(void);
	BGL_EXPORTED_DECL obj_t BGl_markzd2methodz12zc0zzobject_methodz00(obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static bool_t BGl_dssslzd2methodzf3z21zzobject_methodz00(obj_t);
	extern obj_t BGl_za2debugzd2moduleza2zd2zzengine_paramz00;
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzobject_methodz00(void);
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2methodzd2dssslzd2bodyzd2zzobject_methodz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_idzd2ofzd2idz00zzast_identz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzobject_methodz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_dssslz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_argsz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__dssslz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	BGL_IMPORT obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_z62markzd2methodz12za2zzobject_methodz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_makezd2dssslzd2functionzd2preludezd2zz__dssslz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzobject_methodz00(void);
	BGL_IMPORT bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzobject_methodz00(void);
	extern long BGl_globalzd2arityzd2zztools_argsz00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzobject_methodz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzobject_methodz00(void);
	static obj_t BGl_z62makezd2methodzd2dssslzd2bodyzb0zzobject_methodz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t
		BGl_dssslzd2formalszd2ze3schemezd2typedzd2formalsze3zz__dssslz00(obj_t,
		obj_t, bool_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_localzd2iszd2methodzf3zf3zzobject_methodz00(BgL_localz00_bglt);
	extern bool_t BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_bglt);
	static obj_t
		BGl_z62makezd2methodzd2nozd2dssslzd2bodyz62zzobject_methodz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_pairzd2ze3listz31zzobject_methodz00(obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	BGL_IMPORT bool_t BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(obj_t);
	static obj_t __cnst[13];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2methodzd2dssslzd2bodyzd2envz00zzobject_methodz00,
		BgL_bgl_za762makeza7d2method1771z00,
		BGl_z62makezd2methodzd2dssslzd2bodyzb0zzobject_methodz00, 0L, BUNSPEC, 5);
	      DEFINE_STRING(BGl_string1762z00zzobject_methodz00,
		BgL_bgl_string1762za700za7za7o1772za7, "\"); ", 4);
	      DEFINE_STRING(BGl_string1763z00zzobject_methodz00,
		BgL_bgl_string1763za700za7za7o1773za7, " ::", 3);
	      DEFINE_STRING(BGl_string1764z00zzobject_methodz00,
		BgL_bgl_string1764za700za7za7o1774za7,
		"bgl_init_module_debug_string( \"generic-add-method: ", 51);
	      DEFINE_STRING(BGl_string1765z00zzobject_methodz00,
		BgL_bgl_string1765za700za7za7o1775za7, "Can't find generic for method", 29);
	      DEFINE_STRING(BGl_string1766z00zzobject_methodz00,
		BgL_bgl_string1766za700za7za7o1776za7,
		"method has a non-class dispatching type arg", 43);
	      DEFINE_STRING(BGl_string1767z00zzobject_methodz00,
		BgL_bgl_string1767za700za7za7o1777za7, "object_method", 13);
	      DEFINE_STRING(BGl_string1768z00zzobject_methodz00,
		BgL_bgl_string1768za700za7za7o1778za7,
		"(quote method-definition-error) generic-add-method! pragma::void module labels call-next-method let apply cons* find-super-class-method @ - next-method ",
		152);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_markzd2methodz12zd2envz12zzobject_methodz00,
		BgL_bgl_za762markza7d2method1779z00,
		BGl_z62markzd2methodz12za2zzobject_methodz00, 0L, BUNSPEC, 1);
	BGL_IMPORT obj_t BGl_errorzd2envzd2zz__errorz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2methodzd2nozd2dssslzd2bodyzd2envzd2zzobject_methodz00,
		BgL_bgl_za762makeza7d2method1780z00,
		BGl_z62makezd2methodzd2nozd2dssslzd2bodyz62zzobject_methodz00, 0L, BUNSPEC,
		5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzd2iszd2methodzf3zd2envz21zzobject_methodz00,
		BgL_bgl_za762localza7d2isza7d21781za7,
		BGl_z62localzd2iszd2methodzf3z91zzobject_methodz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzobject_methodz00));
		     ADD_ROOT((void *) (&BGl_za2methodsza2z00zzobject_methodz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzobject_methodz00(long
		BgL_checksumz00_1440, char *BgL_fromz00_1441)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzobject_methodz00))
				{
					BGl_requirezd2initializa7ationz75zzobject_methodz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzobject_methodz00();
					BGl_libraryzd2moduleszd2initz00zzobject_methodz00();
					BGl_cnstzd2initzd2zzobject_methodz00();
					BGl_importedzd2moduleszd2initz00zzobject_methodz00();
					return BGl_toplevelzd2initzd2zzobject_methodz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzobject_methodz00(void)
	{
		{	/* Object/method.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"object_method");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "object_method");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"object_method");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"object_method");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "object_method");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"object_method");
			BGl_modulezd2initializa7ationz75zz__dssslz00(0L, "object_method");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "object_method");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"object_method");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"object_method");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"object_method");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzobject_methodz00(void)
	{
		{	/* Object/method.scm 15 */
			{	/* Object/method.scm 15 */
				obj_t BgL_cportz00_1429;

				{	/* Object/method.scm 15 */
					obj_t BgL_stringz00_1436;

					BgL_stringz00_1436 = BGl_string1768z00zzobject_methodz00;
					{	/* Object/method.scm 15 */
						obj_t BgL_startz00_1437;

						BgL_startz00_1437 = BINT(0L);
						{	/* Object/method.scm 15 */
							obj_t BgL_endz00_1438;

							BgL_endz00_1438 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1436)));
							{	/* Object/method.scm 15 */

								BgL_cportz00_1429 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1436, BgL_startz00_1437, BgL_endz00_1438);
				}}}}
				{
					long BgL_iz00_1430;

					BgL_iz00_1430 = 12L;
				BgL_loopz00_1431:
					if ((BgL_iz00_1430 == -1L))
						{	/* Object/method.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Object/method.scm 15 */
							{	/* Object/method.scm 15 */
								obj_t BgL_arg1770z00_1432;

								{	/* Object/method.scm 15 */

									{	/* Object/method.scm 15 */
										obj_t BgL_locationz00_1434;

										BgL_locationz00_1434 = BBOOL(((bool_t) 0));
										{	/* Object/method.scm 15 */

											BgL_arg1770z00_1432 =
												BGl_readz00zz__readerz00(BgL_cportz00_1429,
												BgL_locationz00_1434);
										}
									}
								}
								{	/* Object/method.scm 15 */
									int BgL_tmpz00_1470;

									BgL_tmpz00_1470 = (int) (BgL_iz00_1430);
									CNST_TABLE_SET(BgL_tmpz00_1470, BgL_arg1770z00_1432);
							}}
							{	/* Object/method.scm 15 */
								int BgL_auxz00_1435;

								BgL_auxz00_1435 = (int) ((BgL_iz00_1430 - 1L));
								{
									long BgL_iz00_1475;

									BgL_iz00_1475 = (long) (BgL_auxz00_1435);
									BgL_iz00_1430 = BgL_iz00_1475;
									goto BgL_loopz00_1431;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzobject_methodz00(void)
	{
		{	/* Object/method.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzobject_methodz00(void)
	{
		{	/* Object/method.scm 15 */
			return (BGl_za2methodsza2z00zzobject_methodz00 = BNIL, BUNSPEC);
		}

	}



/* make-method-no-dsssl-body */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2methodzd2nozd2dssslzd2bodyz00zzobject_methodz00(obj_t
		BgL_identz00_3, obj_t BgL_argsz00_4, obj_t BgL_localsz00_5,
		obj_t BgL_bodyz00_6, obj_t BgL_srcz00_7)
	{
		{	/* Object/method.scm 37 */
			{	/* Object/method.scm 38 */
				obj_t BgL_idz00_781;

				BgL_idz00_781 =
					BGl_idzd2ofzd2idz00zzast_identz00(BgL_identz00_3,
					BGl_findzd2locationzd2zztools_locationz00(BgL_srcz00_7));
				{	/* Object/method.scm 38 */
					obj_t BgL_metz00_782;

					BgL_metz00_782 =
						BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(0));
					{	/* Object/method.scm 39 */
						long BgL_arityz00_783;

						BgL_arityz00_783 =
							BGl_globalzd2arityzd2zztools_argsz00(BgL_argsz00_4);
						{	/* Object/method.scm 40 */
							obj_t BgL_argszd2idzd2_784;

							if (NULLP(BgL_localsz00_5))
								{	/* Object/method.scm 41 */
									BgL_argszd2idzd2_784 = BNIL;
								}
							else
								{	/* Object/method.scm 41 */
									obj_t BgL_head1106z00_883;

									{	/* Object/method.scm 41 */
										obj_t BgL_arg1314z00_895;

										BgL_arg1314z00_895 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt)
															CAR(((obj_t) BgL_localsz00_5))))))->BgL_idz00);
										BgL_head1106z00_883 =
											MAKE_YOUNG_PAIR(BgL_arg1314z00_895, BNIL);
									}
									{	/* Object/method.scm 41 */
										obj_t BgL_g1109z00_884;

										BgL_g1109z00_884 = CDR(((obj_t) BgL_localsz00_5));
										{
											obj_t BgL_l1104z00_886;
											obj_t BgL_tail1107z00_887;

											BgL_l1104z00_886 = BgL_g1109z00_884;
											BgL_tail1107z00_887 = BgL_head1106z00_883;
										BgL_zc3z04anonymousza31308ze3z87_888:
											if (NULLP(BgL_l1104z00_886))
												{	/* Object/method.scm 41 */
													BgL_argszd2idzd2_784 = BgL_head1106z00_883;
												}
											else
												{	/* Object/method.scm 41 */
													obj_t BgL_newtail1108z00_890;

													{	/* Object/method.scm 41 */
														obj_t BgL_arg1311z00_892;

														BgL_arg1311z00_892 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt)
																			CAR(
																				((obj_t) BgL_l1104z00_886))))))->
															BgL_idz00);
														BgL_newtail1108z00_890 =
															MAKE_YOUNG_PAIR(BgL_arg1311z00_892, BNIL);
													}
													SET_CDR(BgL_tail1107z00_887, BgL_newtail1108z00_890);
													{	/* Object/method.scm 41 */
														obj_t BgL_arg1310z00_891;

														BgL_arg1310z00_891 =
															CDR(((obj_t) BgL_l1104z00_886));
														{
															obj_t BgL_tail1107z00_1505;
															obj_t BgL_l1104z00_1504;

															BgL_l1104z00_1504 = BgL_arg1310z00_891;
															BgL_tail1107z00_1505 = BgL_newtail1108z00_890;
															BgL_tail1107z00_887 = BgL_tail1107z00_1505;
															BgL_l1104z00_886 = BgL_l1104z00_1504;
															goto BgL_zc3z04anonymousza31308ze3z87_888;
														}
													}
												}
										}
									}
								}
							{	/* Object/method.scm 41 */
								BgL_typez00_bglt BgL_typez00_785;

								BgL_typez00_785 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_localz00_bglt)
													CAR(((obj_t) BgL_localsz00_5))))))->BgL_typez00);
								{	/* Object/method.scm 42 */
									obj_t BgL_mzd2idzd2_786;

									{	/* Object/method.scm 43 */
										obj_t BgL_arg1268z00_875;

										{	/* Object/method.scm 43 */
											obj_t BgL_arg1272z00_876;

											BgL_arg1272z00_876 =
												(((BgL_typez00_bglt) COBJECT(BgL_typez00_785))->
												BgL_idz00);
											{	/* Object/method.scm 43 */
												obj_t BgL_list1273z00_877;

												{	/* Object/method.scm 43 */
													obj_t BgL_arg1284z00_878;

													{	/* Object/method.scm 43 */
														obj_t BgL_arg1304z00_879;

														BgL_arg1304z00_879 =
															MAKE_YOUNG_PAIR(BgL_arg1272z00_876, BNIL);
														BgL_arg1284z00_878 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(1),
															BgL_arg1304z00_879);
													}
													BgL_list1273z00_877 =
														MAKE_YOUNG_PAIR(BgL_idz00_781, BgL_arg1284z00_878);
												}
												BgL_arg1268z00_875 =
													BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
													(BgL_list1273z00_877);
											}
										}
										BgL_mzd2idzd2_786 =
											BGl_gensymz00zz__r4_symbols_6_4z00(BgL_arg1268z00_875);
									}
									{	/* Object/method.scm 43 */

										{	/* Object/method.scm 45 */
											bool_t BgL_test1786z00_1518;

											{	/* Object/method.scm 45 */
												obj_t BgL_classz00_1109;

												BgL_classz00_1109 = BGl_tclassz00zzobject_classz00;
												{	/* Object/method.scm 45 */
													BgL_objectz00_bglt BgL_arg1807z00_1111;

													{	/* Object/method.scm 45 */
														obj_t BgL_tmpz00_1519;

														BgL_tmpz00_1519 =
															((obj_t) ((BgL_objectz00_bglt) BgL_typez00_785));
														BgL_arg1807z00_1111 =
															(BgL_objectz00_bglt) (BgL_tmpz00_1519);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Object/method.scm 45 */
															long BgL_idxz00_1117;

															BgL_idxz00_1117 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1111);
															BgL_test1786z00_1518 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_1117 + 2L)) == BgL_classz00_1109);
														}
													else
														{	/* Object/method.scm 45 */
															bool_t BgL_res1740z00_1142;

															{	/* Object/method.scm 45 */
																obj_t BgL_oclassz00_1125;

																{	/* Object/method.scm 45 */
																	obj_t BgL_arg1815z00_1133;
																	long BgL_arg1816z00_1134;

																	BgL_arg1815z00_1133 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Object/method.scm 45 */
																		long BgL_arg1817z00_1135;

																		BgL_arg1817z00_1135 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1111);
																		BgL_arg1816z00_1134 =
																			(BgL_arg1817z00_1135 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_1125 =
																		VECTOR_REF(BgL_arg1815z00_1133,
																		BgL_arg1816z00_1134);
																}
																{	/* Object/method.scm 45 */
																	bool_t BgL__ortest_1115z00_1126;

																	BgL__ortest_1115z00_1126 =
																		(BgL_classz00_1109 == BgL_oclassz00_1125);
																	if (BgL__ortest_1115z00_1126)
																		{	/* Object/method.scm 45 */
																			BgL_res1740z00_1142 =
																				BgL__ortest_1115z00_1126;
																		}
																	else
																		{	/* Object/method.scm 45 */
																			long BgL_odepthz00_1127;

																			{	/* Object/method.scm 45 */
																				obj_t BgL_arg1804z00_1128;

																				BgL_arg1804z00_1128 =
																					(BgL_oclassz00_1125);
																				BgL_odepthz00_1127 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_1128);
																			}
																			if ((2L < BgL_odepthz00_1127))
																				{	/* Object/method.scm 45 */
																					obj_t BgL_arg1802z00_1130;

																					{	/* Object/method.scm 45 */
																						obj_t BgL_arg1803z00_1131;

																						BgL_arg1803z00_1131 =
																							(BgL_oclassz00_1125);
																						BgL_arg1802z00_1130 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_1131, 2L);
																					}
																					BgL_res1740z00_1142 =
																						(BgL_arg1802z00_1130 ==
																						BgL_classz00_1109);
																				}
																			else
																				{	/* Object/method.scm 45 */
																					BgL_res1740z00_1142 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test1786z00_1518 = BgL_res1740z00_1142;
														}
												}
											}
											if (BgL_test1786z00_1518)
												{	/* Object/method.scm 48 */
													BgL_globalz00_bglt BgL_holderz00_788;

													{
														BgL_tclassz00_bglt BgL_auxz00_1542;

														{
															obj_t BgL_auxz00_1543;

															{	/* Object/method.scm 48 */
																BgL_objectz00_bglt BgL_tmpz00_1544;

																BgL_tmpz00_1544 =
																	((BgL_objectz00_bglt)
																	((BgL_typez00_bglt) BgL_typez00_785));
																BgL_auxz00_1543 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_1544);
															}
															BgL_auxz00_1542 =
																((BgL_tclassz00_bglt) BgL_auxz00_1543);
														}
														BgL_holderz00_788 =
															(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_1542))->
															BgL_holderz00);
													}
													{	/* Object/method.scm 48 */
														obj_t BgL_modulez00_789;

														BgL_modulez00_789 =
															(((BgL_globalz00_bglt)
																COBJECT(BgL_holderz00_788))->BgL_modulez00);
														{	/* Object/method.scm 49 */
															obj_t BgL_genericz00_790;

															BgL_genericz00_790 =
																BGl_findzd2globalzd2zzast_envz00(BgL_idz00_781,
																BNIL);
															{	/* Object/method.scm 50 */

																{	/* Object/method.scm 52 */
																	bool_t BgL_test1790z00_1552;

																	{	/* Object/method.scm 52 */
																		obj_t BgL_classz00_1146;

																		BgL_classz00_1146 =
																			BGl_globalz00zzast_varz00;
																		if (BGL_OBJECTP(BgL_genericz00_790))
																			{	/* Object/method.scm 52 */
																				BgL_objectz00_bglt BgL_arg1807z00_1148;

																				BgL_arg1807z00_1148 =
																					(BgL_objectz00_bglt)
																					(BgL_genericz00_790);
																				if (BGL_CONDEXPAND_ISA_ARCH64())
																					{	/* Object/method.scm 52 */
																						long BgL_idxz00_1154;

																						BgL_idxz00_1154 =
																							BGL_OBJECT_INHERITANCE_NUM
																							(BgL_arg1807z00_1148);
																						BgL_test1790z00_1552 =
																							(VECTOR_REF
																							(BGl_za2inheritancesza2z00zz__objectz00,
																								(BgL_idxz00_1154 + 2L)) ==
																							BgL_classz00_1146);
																					}
																				else
																					{	/* Object/method.scm 52 */
																						bool_t BgL_res1741z00_1179;

																						{	/* Object/method.scm 52 */
																							obj_t BgL_oclassz00_1162;

																							{	/* Object/method.scm 52 */
																								obj_t BgL_arg1815z00_1170;
																								long BgL_arg1816z00_1171;

																								BgL_arg1815z00_1170 =
																									(BGl_za2classesza2z00zz__objectz00);
																								{	/* Object/method.scm 52 */
																									long BgL_arg1817z00_1172;

																									BgL_arg1817z00_1172 =
																										BGL_OBJECT_CLASS_NUM
																										(BgL_arg1807z00_1148);
																									BgL_arg1816z00_1171 =
																										(BgL_arg1817z00_1172 -
																										OBJECT_TYPE);
																								}
																								BgL_oclassz00_1162 =
																									VECTOR_REF
																									(BgL_arg1815z00_1170,
																									BgL_arg1816z00_1171);
																							}
																							{	/* Object/method.scm 52 */
																								bool_t BgL__ortest_1115z00_1163;

																								BgL__ortest_1115z00_1163 =
																									(BgL_classz00_1146 ==
																									BgL_oclassz00_1162);
																								if (BgL__ortest_1115z00_1163)
																									{	/* Object/method.scm 52 */
																										BgL_res1741z00_1179 =
																											BgL__ortest_1115z00_1163;
																									}
																								else
																									{	/* Object/method.scm 52 */
																										long BgL_odepthz00_1164;

																										{	/* Object/method.scm 52 */
																											obj_t BgL_arg1804z00_1165;

																											BgL_arg1804z00_1165 =
																												(BgL_oclassz00_1162);
																											BgL_odepthz00_1164 =
																												BGL_CLASS_DEPTH
																												(BgL_arg1804z00_1165);
																										}
																										if (
																											(2L < BgL_odepthz00_1164))
																											{	/* Object/method.scm 52 */
																												obj_t
																													BgL_arg1802z00_1167;
																												{	/* Object/method.scm 52 */
																													obj_t
																														BgL_arg1803z00_1168;
																													BgL_arg1803z00_1168 =
																														(BgL_oclassz00_1162);
																													BgL_arg1802z00_1167 =
																														BGL_CLASS_ANCESTORS_REF
																														(BgL_arg1803z00_1168,
																														2L);
																												}
																												BgL_res1741z00_1179 =
																													(BgL_arg1802z00_1167
																													== BgL_classz00_1146);
																											}
																										else
																											{	/* Object/method.scm 52 */
																												BgL_res1741z00_1179 =
																													((bool_t) 0);
																											}
																									}
																							}
																						}
																						BgL_test1790z00_1552 =
																							BgL_res1741z00_1179;
																					}
																			}
																		else
																			{	/* Object/method.scm 52 */
																				BgL_test1790z00_1552 = ((bool_t) 0);
																			}
																	}
																	if (BgL_test1790z00_1552)
																		{	/* Object/method.scm 55 */
																			obj_t BgL_bodyz00_792;

																			{	/* Object/method.scm 57 */
																				obj_t BgL_arg1210z00_846;

																				{	/* Object/method.scm 57 */
																					obj_t BgL_arg1212z00_847;
																					obj_t BgL_arg1215z00_848;

																					{	/* Object/method.scm 57 */
																						obj_t BgL_arg1216z00_849;

																						{	/* Object/method.scm 57 */
																							obj_t BgL_arg1218z00_850;

																							{	/* Object/method.scm 57 */
																								obj_t BgL_arg1219z00_851;

																								{	/* Object/method.scm 57 */
																									obj_t BgL_arg1220z00_852;

																									{	/* Object/method.scm 57 */
																										obj_t BgL_arg1221z00_853;

																										{	/* Object/method.scm 57 */
																											obj_t BgL_arg1223z00_854;
																											obj_t BgL_arg1225z00_855;

																											{	/* Object/method.scm 57 */
																												obj_t
																													BgL_arg1226z00_856;
																												{	/* Object/method.scm 57 */
																													obj_t
																														BgL_arg1227z00_857;
																													{	/* Object/method.scm 57 */
																														obj_t
																															BgL_arg1228z00_858;
																														{	/* Object/method.scm 57 */
																															obj_t
																																BgL_arg1229z00_859;
																															{	/* Object/method.scm 57 */
																																obj_t
																																	BgL_arg1230z00_860;
																																obj_t
																																	BgL_arg1231z00_861;
																																BgL_arg1230z00_860
																																	=
																																	CAR(((obj_t)
																																		BgL_argszd2idzd2_784));
																																{	/* Object/method.scm 59 */
																																	obj_t
																																		BgL_arg1232z00_862;
																																	{	/* Object/method.scm 59 */
																																		obj_t
																																			BgL_arg1233z00_863;
																																		{	/* Object/method.scm 59 */
																																			obj_t
																																				BgL_arg1234z00_864;
																																			{	/* Object/method.scm 59 */
																																				obj_t
																																					BgL_arg1236z00_865;
																																				obj_t
																																					BgL_arg1238z00_866;
																																				BgL_arg1236z00_865
																																					=
																																					(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) BgL_holderz00_788)))->BgL_idz00);
																																				BgL_arg1238z00_866
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_modulez00_789,
																																					BNIL);
																																				BgL_arg1234z00_864
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1236z00_865,
																																					BgL_arg1238z00_866);
																																			}
																																			BgL_arg1233z00_863
																																				=
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(2),
																																				BgL_arg1234z00_864);
																																		}
																																		BgL_arg1232z00_862
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1233z00_863,
																																			BNIL);
																																	}
																																	BgL_arg1231z00_861
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_idz00_781,
																																		BgL_arg1232z00_862);
																																}
																																BgL_arg1229z00_859
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1230z00_860,
																																	BgL_arg1231z00_861);
																															}
																															BgL_arg1228z00_858
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(3),
																																BgL_arg1229z00_859);
																														}
																														BgL_arg1227z00_857 =
																															MAKE_YOUNG_PAIR
																															(BgL_arg1228z00_858,
																															BNIL);
																													}
																													BgL_arg1226z00_856 =
																														MAKE_YOUNG_PAIR
																														(BgL_metz00_782,
																														BgL_arg1227z00_857);
																												}
																												BgL_arg1223z00_854 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1226z00_856,
																													BNIL);
																											}
																											{	/* Object/method.scm 61 */
																												obj_t
																													BgL_arg1239z00_867;
																												if ((BgL_arityz00_783 >=
																														0L))
																													{	/* Object/method.scm 61 */
																														BgL_arg1239z00_867 =
																															MAKE_YOUNG_PAIR
																															(BgL_metz00_782,
																															BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																															(BgL_argszd2idzd2_784,
																																BNIL));
																													}
																												else
																													{	/* Object/method.scm 64 */
																														obj_t
																															BgL_arg1244z00_870;
																														{	/* Object/method.scm 64 */
																															obj_t
																																BgL_arg1248z00_871;
																															{	/* Object/method.scm 64 */
																																obj_t
																																	BgL_arg1249z00_872;
																																{	/* Object/method.scm 64 */
																																	obj_t
																																		BgL_arg1252z00_873;
																																	BgL_arg1252z00_873
																																		=
																																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																		(BgL_argszd2idzd2_784,
																																		BNIL);
																																	BgL_arg1249z00_872
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(4),
																																		BgL_arg1252z00_873);
																																}
																																BgL_arg1248z00_871
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1249z00_872,
																																	BNIL);
																															}
																															BgL_arg1244z00_870
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_metz00_782,
																																BgL_arg1248z00_871);
																														}
																														BgL_arg1239z00_867 =
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(5),
																															BgL_arg1244z00_870);
																													}
																												BgL_arg1225z00_855 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1239z00_867,
																													BNIL);
																											}
																											BgL_arg1221z00_853 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1223z00_854,
																												BgL_arg1225z00_855);
																										}
																										BgL_arg1220z00_852 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(6),
																											BgL_arg1221z00_853);
																									}
																									BgL_arg1219z00_851 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1220z00_852, BNIL);
																								}
																								BgL_arg1218z00_850 =
																									MAKE_YOUNG_PAIR(BNIL,
																									BgL_arg1219z00_851);
																							}
																							BgL_arg1216z00_849 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(7), BgL_arg1218z00_850);
																						}
																						BgL_arg1212z00_847 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1216z00_849, BNIL);
																					}
																					BgL_arg1215z00_848 =
																						MAKE_YOUNG_PAIR(BgL_bodyz00_6,
																						BNIL);
																					BgL_arg1210z00_846 =
																						MAKE_YOUNG_PAIR(BgL_arg1212z00_847,
																						BgL_arg1215z00_848);
																				}
																				BgL_bodyz00_792 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(8),
																					BgL_arg1210z00_846);
																			}
																			{	/* Object/method.scm 55 */
																				obj_t BgL_ebodyz00_793;

																				if (EPAIRP(BgL_srcz00_7))
																					{	/* Object/method.scm 67 */
																						obj_t BgL_arg1206z00_843;
																						obj_t BgL_arg1208z00_844;
																						obj_t BgL_arg1209z00_845;

																						BgL_arg1206z00_843 =
																							CAR(BgL_bodyz00_792);
																						BgL_arg1208z00_844 =
																							CDR(BgL_bodyz00_792);
																						BgL_arg1209z00_845 =
																							CER(((obj_t) BgL_srcz00_7));
																						{	/* Object/method.scm 67 */
																							obj_t BgL_res1742z00_1186;

																							BgL_res1742z00_1186 =
																								MAKE_YOUNG_EPAIR
																								(BgL_arg1206z00_843,
																								BgL_arg1208z00_844,
																								BgL_arg1209z00_845);
																							BgL_ebodyz00_793 =
																								BgL_res1742z00_1186;
																						}
																					}
																				else
																					{	/* Object/method.scm 66 */
																						BgL_ebodyz00_793 = BgL_bodyz00_792;
																					}
																				{	/* Object/method.scm 66 */
																					obj_t BgL_tmzd2idzd2_794;

																					{	/* Object/method.scm 69 */
																						bool_t BgL_test1797z00_1622;

																						{	/* Object/method.scm 69 */
																							BgL_typez00_bglt
																								BgL_arg1203z00_841;
																							BgL_arg1203z00_841 =
																								(((BgL_variablez00_bglt)
																									COBJECT((
																											(BgL_variablez00_bglt) (
																												(BgL_globalz00_bglt)
																												BgL_genericz00_790))))->
																								BgL_typez00);
																							BgL_test1797z00_1622 =
																								BGl_bigloozd2typezf3z21zztype_typez00
																								(BgL_arg1203z00_841);
																						}
																						if (BgL_test1797z00_1622)
																							{	/* Object/method.scm 70 */
																								obj_t BgL_arg1201z00_839;

																								BgL_arg1201z00_839 =
																									(((BgL_typez00_bglt) COBJECT(
																											(((BgL_variablez00_bglt)
																													COBJECT((
																															(BgL_variablez00_bglt)
																															((BgL_globalz00_bglt) BgL_genericz00_790))))->BgL_typez00)))->BgL_idz00);
																								BgL_tmzd2idzd2_794 =
																									BGl_makezd2typedzd2identz00zzast_identz00
																									(BgL_mzd2idzd2_786,
																									BgL_arg1201z00_839);
																							}
																						else
																							{	/* Object/method.scm 69 */
																								BgL_tmzd2idzd2_794 =
																									BgL_mzd2idzd2_786;
																							}
																					}
																					{	/* Object/method.scm 69 */
																						obj_t BgL_bdgz00_795;

																						{	/* Object/method.scm 72 */
																							obj_t BgL_arg1197z00_835;

																							{	/* Object/method.scm 72 */
																								obj_t BgL_arg1198z00_836;

																								BgL_arg1198z00_836 =
																									MAKE_YOUNG_PAIR
																									(BgL_ebodyz00_793, BNIL);
																								BgL_arg1197z00_835 =
																									MAKE_YOUNG_PAIR(BgL_argsz00_4,
																									BgL_arg1198z00_836);
																							}
																							BgL_bdgz00_795 =
																								MAKE_YOUNG_PAIR
																								(BgL_tmzd2idzd2_794,
																								BgL_arg1197z00_835);
																						}
																						{	/* Object/method.scm 72 */
																							obj_t BgL_ebdgz00_796;

																							if (EPAIRP(BgL_srcz00_7))
																								{	/* Object/method.scm 74 */
																									obj_t BgL_arg1193z00_832;
																									obj_t BgL_arg1194z00_833;
																									obj_t BgL_arg1196z00_834;

																									BgL_arg1193z00_832 =
																										CAR(BgL_bdgz00_795);
																									BgL_arg1194z00_833 =
																										CDR(BgL_bdgz00_795);
																									BgL_arg1196z00_834 =
																										CER(((obj_t) BgL_srcz00_7));
																									{	/* Object/method.scm 74 */
																										obj_t BgL_res1743z00_1193;

																										BgL_res1743z00_1193 =
																											MAKE_YOUNG_EPAIR
																											(BgL_arg1193z00_832,
																											BgL_arg1194z00_833,
																											BgL_arg1196z00_834);
																										BgL_ebdgz00_796 =
																											BgL_res1743z00_1193;
																									}
																								}
																							else
																								{	/* Object/method.scm 73 */
																									BgL_ebdgz00_796 =
																										BgL_bdgz00_795;
																								}
																							{	/* Object/method.scm 73 */

																								BGl_za2methodsza2z00zzobject_methodz00
																									=
																									MAKE_YOUNG_PAIR
																									(BgL_mzd2idzd2_786,
																									BGl_za2methodsza2z00zzobject_methodz00);
																								{	/* Object/method.scm 77 */
																									obj_t BgL_arg1126z00_797;

																									{	/* Object/method.scm 77 */
																										obj_t BgL_arg1129z00_799;

																										{	/* Object/method.scm 77 */
																											obj_t BgL_arg1131z00_800;
																											obj_t BgL_arg1132z00_801;

																											BgL_arg1131z00_800 =
																												MAKE_YOUNG_PAIR
																												(BgL_ebdgz00_796, BNIL);
																											{	/* Object/method.scm 78 */
																												obj_t
																													BgL_arg1137z00_802;
																												obj_t
																													BgL_arg1138z00_803;
																												{	/* Object/method.scm 78 */
																													bool_t
																														BgL_test1801z00_1644;
																													if (((long)
																															CINT
																															(BGl_za2debugzd2moduleza2zd2zzengine_paramz00)
																															>= 1L))
																														{	/* Object/method.scm 81 */
																															obj_t
																																BgL_arg1162z00_819;
																															{	/* Object/method.scm 81 */
																																obj_t
																																	BgL_arg1164z00_820;
																																BgL_arg1164z00_820
																																	=
																																	BGl_thezd2backendzd2zzbackend_backendz00
																																	();
																																BgL_arg1162z00_819
																																	=
																																	(((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt) BgL_arg1164z00_820)))->BgL_debugzd2supportzd2);
																															}
																															BgL_test1801z00_1644
																																=
																																CBOOL
																																(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																																(CNST_TABLE_REF
																																	(9),
																																	BgL_arg1162z00_819));
																														}
																													else
																														{	/* Object/method.scm 78 */
																															BgL_test1801z00_1644
																																= ((bool_t) 0);
																														}
																													if (BgL_test1801z00_1644)
																														{	/* Object/method.scm 83 */
																															obj_t
																																BgL_arg1143z00_808;
																															{	/* Object/method.scm 83 */
																																obj_t
																																	BgL_arg1145z00_809;
																																{	/* Object/method.scm 83 */
																																	obj_t
																																		BgL_arg1148z00_810;
																																	obj_t
																																		BgL_arg1149z00_811;
																																	{	/* Object/method.scm 83 */
																																		obj_t
																																			BgL_arg1455z00_1198;
																																		BgL_arg1455z00_1198
																																			=
																																			SYMBOL_TO_STRING
																																			(BgL_identz00_3);
																																		BgL_arg1148z00_810
																																			=
																																			BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																			(BgL_arg1455z00_1198);
																																	}
																																	{	/* Object/method.scm 83 */
																																		obj_t
																																			BgL_arg1158z00_817;
																																		BgL_arg1158z00_817
																																			=
																																			(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) BgL_holderz00_788)))->BgL_idz00);
																																		{	/* Object/method.scm 83 */
																																			obj_t
																																				BgL_arg1455z00_1201;
																																			BgL_arg1455z00_1201
																																				=
																																				SYMBOL_TO_STRING
																																				(BgL_arg1158z00_817);
																																			BgL_arg1149z00_811
																																				=
																																				BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																				(BgL_arg1455z00_1201);
																																		}
																																	}
																																	{	/* Object/method.scm 83 */
																																		obj_t
																																			BgL_list1150z00_812;
																																		{	/* Object/method.scm 83 */
																																			obj_t
																																				BgL_arg1152z00_813;
																																			{	/* Object/method.scm 83 */
																																				obj_t
																																					BgL_arg1153z00_814;
																																				{	/* Object/method.scm 83 */
																																					obj_t
																																						BgL_arg1154z00_815;
																																					{	/* Object/method.scm 83 */
																																						obj_t
																																							BgL_arg1157z00_816;
																																						BgL_arg1157z00_816
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_string1762z00zzobject_methodz00,
																																							BNIL);
																																						BgL_arg1154z00_815
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1149z00_811,
																																							BgL_arg1157z00_816);
																																					}
																																					BgL_arg1153z00_814
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_string1763z00zzobject_methodz00,
																																						BgL_arg1154z00_815);
																																				}
																																				BgL_arg1152z00_813
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1148z00_810,
																																					BgL_arg1153z00_814);
																																			}
																																			BgL_list1150z00_812
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_string1764z00zzobject_methodz00,
																																				BgL_arg1152z00_813);
																																		}
																																		BgL_arg1145z00_809
																																			=
																																			BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																																			(BgL_list1150z00_812);
																																	}
																																}
																																BgL_arg1143z00_808
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1145z00_809,
																																	BNIL);
																															}
																															BgL_arg1137z00_802
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(10),
																																BgL_arg1143z00_808);
																														}
																													else
																														{	/* Object/method.scm 78 */
																															BgL_arg1137z00_802
																																= BFALSE;
																														}
																												}
																												{	/* Object/method.scm 86 */
																													obj_t
																														BgL_arg1166z00_821;
																													{	/* Object/method.scm 86 */
																														obj_t
																															BgL_arg1171z00_822;
																														{	/* Object/method.scm 86 */
																															obj_t
																																BgL_arg1172z00_823;
																															{	/* Object/method.scm 86 */
																																obj_t
																																	BgL_arg1182z00_824;
																																obj_t
																																	BgL_arg1183z00_825;
																																{	/* Object/method.scm 86 */
																																	obj_t
																																		BgL_arg1187z00_826;
																																	{	/* Object/method.scm 86 */
																																		obj_t
																																			BgL_arg1188z00_827;
																																		obj_t
																																			BgL_arg1189z00_828;
																																		BgL_arg1188z00_827
																																			=
																																			(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) BgL_holderz00_788)))->BgL_idz00);
																																		BgL_arg1189z00_828
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_modulez00_789,
																																			BNIL);
																																		BgL_arg1187z00_826
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1188z00_827,
																																			BgL_arg1189z00_828);
																																	}
																																	BgL_arg1182z00_824
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(2),
																																		BgL_arg1187z00_826);
																																}
																																{	/* Object/method.scm 88 */
																																	obj_t
																																		BgL_arg1190z00_829;
																																	{	/* Object/method.scm 88 */
																																		obj_t
																																			BgL_arg1191z00_830;
																																		{	/* Object/method.scm 88 */
																																			obj_t
																																				BgL_arg1455z00_1204;
																																			BgL_arg1455z00_1204
																																				=
																																				SYMBOL_TO_STRING
																																				(BgL_identz00_3);
																																			BgL_arg1191z00_830
																																				=
																																				BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																				(BgL_arg1455z00_1204);
																																		}
																																		BgL_arg1190z00_829
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1191z00_830,
																																			BNIL);
																																	}
																																	BgL_arg1183z00_825
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_mzd2idzd2_786,
																																		BgL_arg1190z00_829);
																																}
																																BgL_arg1172z00_823
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1182z00_824,
																																	BgL_arg1183z00_825);
																															}
																															BgL_arg1171z00_822
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_idz00_781,
																																BgL_arg1172z00_823);
																														}
																														BgL_arg1166z00_821 =
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(11),
																															BgL_arg1171z00_822);
																													}
																													BgL_arg1138z00_803 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1166z00_821,
																														BNIL);
																												}
																												BgL_arg1132z00_801 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1137z00_802,
																													BgL_arg1138z00_803);
																											}
																											BgL_arg1129z00_799 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1131z00_800,
																												BgL_arg1132z00_801);
																										}
																										BgL_arg1126z00_797 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(8),
																											BgL_arg1129z00_799);
																									}
																									{	/* Object/method.scm 77 */
																										obj_t BgL_list1127z00_798;

																										BgL_list1127z00_798 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1126z00_797,
																											BNIL);
																										return BgL_list1127z00_798;
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	else
																		{	/* Object/method.scm 193 */
																			obj_t BgL_arg1700z00_1206;

																			{	/* Object/method.scm 193 */
																				obj_t BgL_list1702z00_1207;

																				BgL_list1702z00_1207 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(12),
																					BNIL);
																				BgL_arg1700z00_1206 =
																					BgL_list1702z00_1207;
																			}
																			{	/* Object/method.scm 193 */
																				obj_t BgL_list1701z00_1208;

																				BgL_list1701z00_1208 =
																					MAKE_YOUNG_PAIR(BgL_arg1700z00_1206,
																					BNIL);
																				return
																					BGl_userzd2errorzd2zztools_errorz00
																					(BgL_idz00_781,
																					BGl_string1765z00zzobject_methodz00,
																					BgL_srcz00_7, BgL_list1701z00_1208);
																			}
																		}
																}
															}
														}
													}
												}
											else
												{	/* Object/method.scm 193 */
													obj_t BgL_arg1700z00_1210;

													{	/* Object/method.scm 193 */
														obj_t BgL_list1702z00_1211;

														BgL_list1702z00_1211 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(12), BNIL);
														BgL_arg1700z00_1210 = BgL_list1702z00_1211;
													}
													{	/* Object/method.scm 193 */
														obj_t BgL_list1701z00_1212;

														BgL_list1701z00_1212 =
															MAKE_YOUNG_PAIR(BgL_arg1700z00_1210, BNIL);
														return
															BGl_userzd2errorzd2zztools_errorz00(BgL_idz00_781,
															BGl_string1766z00zzobject_methodz00, BgL_srcz00_7,
															BgL_list1701z00_1212);
													}
												}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &make-method-no-dsssl-body */
	obj_t BGl_z62makezd2methodzd2nozd2dssslzd2bodyz62zzobject_methodz00(obj_t
		BgL_envz00_1409, obj_t BgL_identz00_1410, obj_t BgL_argsz00_1411,
		obj_t BgL_localsz00_1412, obj_t BgL_bodyz00_1413, obj_t BgL_srcz00_1414)
	{
		{	/* Object/method.scm 37 */
			return
				BGl_makezd2methodzd2nozd2dssslzd2bodyz00zzobject_methodz00
				(BgL_identz00_1410, BgL_argsz00_1411, BgL_localsz00_1412,
				BgL_bodyz00_1413, BgL_srcz00_1414);
		}

	}



/* make-method-dsssl-body */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2methodzd2dssslzd2bodyzd2zzobject_methodz00(obj_t BgL_identz00_8,
		obj_t BgL_argsz00_9, obj_t BgL_localsz00_10, obj_t BgL_bodyz00_11,
		obj_t BgL_srcz00_12)
	{
		{	/* Object/method.scm 93 */
			{	/* Object/method.scm 94 */
				obj_t BgL_idz00_898;

				BgL_idz00_898 =
					BGl_idzd2ofzd2idz00zzast_identz00(BgL_identz00_8,
					BGl_findzd2locationzd2zztools_locationz00(BgL_srcz00_12));
				{	/* Object/method.scm 94 */
					obj_t BgL_metz00_899;

					BgL_metz00_899 =
						BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(0));
					{	/* Object/method.scm 95 */
						long BgL_arityz00_900;

						BgL_arityz00_900 =
							BGl_globalzd2arityzd2zztools_argsz00(BgL_argsz00_9);
						{	/* Object/method.scm 96 */
							bool_t BgL_dssslz00_901;

							BgL_dssslz00_901 =
								BGl_dssslzd2methodzf3z21zzobject_methodz00(BgL_argsz00_9);
							{	/* Object/method.scm 97 */
								obj_t BgL_argszd2idzd2_902;

								if (BgL_dssslz00_901)
									{	/* Object/method.scm 98 */
										BgL_argszd2idzd2_902 =
											BGl_pairzd2ze3listz31zzobject_methodz00
											(BGl_dssslzd2formalszd2ze3schemezd2typedzd2formalsze3zz__dssslz00
											(BgL_argsz00_9, BGl_errorzd2envzd2zz__errorz00,
												((bool_t) 0)));
									}
								else
									{	/* Object/method.scm 98 */
										if (NULLP(BgL_localsz00_10))
											{	/* Object/method.scm 100 */
												BgL_argszd2idzd2_902 = BNIL;
											}
										else
											{	/* Object/method.scm 100 */
												obj_t BgL_head1112z00_1008;

												{	/* Object/method.scm 100 */
													obj_t BgL_arg1661z00_1020;

													BgL_arg1661z00_1020 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt)
																		CAR(
																			((obj_t) BgL_localsz00_10))))))->
														BgL_idz00);
													BgL_head1112z00_1008 =
														MAKE_YOUNG_PAIR(BgL_arg1661z00_1020, BNIL);
												}
												{	/* Object/method.scm 100 */
													obj_t BgL_g1115z00_1009;

													BgL_g1115z00_1009 = CDR(((obj_t) BgL_localsz00_10));
													{
														obj_t BgL_l1110z00_1011;
														obj_t BgL_tail1113z00_1012;

														BgL_l1110z00_1011 = BgL_g1115z00_1009;
														BgL_tail1113z00_1012 = BgL_head1112z00_1008;
													BgL_zc3z04anonymousza31648ze3z87_1013:
														if (NULLP(BgL_l1110z00_1011))
															{	/* Object/method.scm 100 */
																BgL_argszd2idzd2_902 = BgL_head1112z00_1008;
															}
														else
															{	/* Object/method.scm 100 */
																obj_t BgL_newtail1114z00_1015;

																{	/* Object/method.scm 100 */
																	obj_t BgL_arg1651z00_1017;

																	BgL_arg1651z00_1017 =
																		(((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					((BgL_localz00_bglt)
																						CAR(
																							((obj_t) BgL_l1110z00_1011))))))->
																		BgL_idz00);
																	BgL_newtail1114z00_1015 =
																		MAKE_YOUNG_PAIR(BgL_arg1651z00_1017, BNIL);
																}
																SET_CDR(BgL_tail1113z00_1012,
																	BgL_newtail1114z00_1015);
																{	/* Object/method.scm 100 */
																	obj_t BgL_arg1650z00_1016;

																	BgL_arg1650z00_1016 =
																		CDR(((obj_t) BgL_l1110z00_1011));
																	{
																		obj_t BgL_tail1113z00_1729;
																		obj_t BgL_l1110z00_1728;

																		BgL_l1110z00_1728 = BgL_arg1650z00_1016;
																		BgL_tail1113z00_1729 =
																			BgL_newtail1114z00_1015;
																		BgL_tail1113z00_1012 = BgL_tail1113z00_1729;
																		BgL_l1110z00_1011 = BgL_l1110z00_1728;
																		goto BgL_zc3z04anonymousza31648ze3z87_1013;
																	}
																}
															}
													}
												}
											}
									}
								{	/* Object/method.scm 98 */
									BgL_typez00_bglt BgL_typez00_903;

									BgL_typez00_903 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt)
														CAR(((obj_t) BgL_localsz00_10))))))->BgL_typez00);
									{	/* Object/method.scm 101 */
										obj_t BgL_mzd2idzd2_904;

										{	/* Object/method.scm 102 */
											obj_t BgL_arg1625z00_999;

											{	/* Object/method.scm 102 */
												obj_t BgL_arg1626z00_1000;

												BgL_arg1626z00_1000 =
													(((BgL_typez00_bglt) COBJECT(BgL_typez00_903))->
													BgL_idz00);
												{	/* Object/method.scm 102 */
													obj_t BgL_list1627z00_1001;

													{	/* Object/method.scm 102 */
														obj_t BgL_arg1629z00_1002;

														{	/* Object/method.scm 102 */
															obj_t BgL_arg1630z00_1003;

															BgL_arg1630z00_1003 =
																MAKE_YOUNG_PAIR(BgL_arg1626z00_1000, BNIL);
															BgL_arg1629z00_1002 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(1),
																BgL_arg1630z00_1003);
														}
														BgL_list1627z00_1001 =
															MAKE_YOUNG_PAIR(BgL_idz00_898,
															BgL_arg1629z00_1002);
													}
													BgL_arg1625z00_999 =
														BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
														(BgL_list1627z00_1001);
												}
											}
											BgL_mzd2idzd2_904 =
												BGl_gensymz00zz__r4_symbols_6_4z00(BgL_arg1625z00_999);
										}
										{	/* Object/method.scm 102 */

											{	/* Object/method.scm 104 */
												bool_t BgL_test1809z00_1742;

												{	/* Object/method.scm 104 */
													obj_t BgL_classz00_1224;

													BgL_classz00_1224 = BGl_tclassz00zzobject_classz00;
													{	/* Object/method.scm 104 */
														BgL_objectz00_bglt BgL_arg1807z00_1226;

														{	/* Object/method.scm 104 */
															obj_t BgL_tmpz00_1743;

															BgL_tmpz00_1743 =
																((obj_t)
																((BgL_objectz00_bglt) BgL_typez00_903));
															BgL_arg1807z00_1226 =
																(BgL_objectz00_bglt) (BgL_tmpz00_1743);
														}
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Object/method.scm 104 */
																long BgL_idxz00_1232;

																BgL_idxz00_1232 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_1226);
																BgL_test1809z00_1742 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_1232 + 2L)) ==
																	BgL_classz00_1224);
															}
														else
															{	/* Object/method.scm 104 */
																bool_t BgL_res1747z00_1257;

																{	/* Object/method.scm 104 */
																	obj_t BgL_oclassz00_1240;

																	{	/* Object/method.scm 104 */
																		obj_t BgL_arg1815z00_1248;
																		long BgL_arg1816z00_1249;

																		BgL_arg1815z00_1248 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Object/method.scm 104 */
																			long BgL_arg1817z00_1250;

																			BgL_arg1817z00_1250 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_1226);
																			BgL_arg1816z00_1249 =
																				(BgL_arg1817z00_1250 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_1240 =
																			VECTOR_REF(BgL_arg1815z00_1248,
																			BgL_arg1816z00_1249);
																	}
																	{	/* Object/method.scm 104 */
																		bool_t BgL__ortest_1115z00_1241;

																		BgL__ortest_1115z00_1241 =
																			(BgL_classz00_1224 == BgL_oclassz00_1240);
																		if (BgL__ortest_1115z00_1241)
																			{	/* Object/method.scm 104 */
																				BgL_res1747z00_1257 =
																					BgL__ortest_1115z00_1241;
																			}
																		else
																			{	/* Object/method.scm 104 */
																				long BgL_odepthz00_1242;

																				{	/* Object/method.scm 104 */
																					obj_t BgL_arg1804z00_1243;

																					BgL_arg1804z00_1243 =
																						(BgL_oclassz00_1240);
																					BgL_odepthz00_1242 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_1243);
																				}
																				if ((2L < BgL_odepthz00_1242))
																					{	/* Object/method.scm 104 */
																						obj_t BgL_arg1802z00_1245;

																						{	/* Object/method.scm 104 */
																							obj_t BgL_arg1803z00_1246;

																							BgL_arg1803z00_1246 =
																								(BgL_oclassz00_1240);
																							BgL_arg1802z00_1245 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_1246, 2L);
																						}
																						BgL_res1747z00_1257 =
																							(BgL_arg1802z00_1245 ==
																							BgL_classz00_1224);
																					}
																				else
																					{	/* Object/method.scm 104 */
																						BgL_res1747z00_1257 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test1809z00_1742 = BgL_res1747z00_1257;
															}
													}
												}
												if (BgL_test1809z00_1742)
													{	/* Object/method.scm 107 */
														BgL_globalz00_bglt BgL_holderz00_906;

														{
															BgL_tclassz00_bglt BgL_auxz00_1766;

															{
																obj_t BgL_auxz00_1767;

																{	/* Object/method.scm 107 */
																	BgL_objectz00_bglt BgL_tmpz00_1768;

																	BgL_tmpz00_1768 =
																		((BgL_objectz00_bglt)
																		((BgL_typez00_bglt) BgL_typez00_903));
																	BgL_auxz00_1767 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_1768);
																}
																BgL_auxz00_1766 =
																	((BgL_tclassz00_bglt) BgL_auxz00_1767);
															}
															BgL_holderz00_906 =
																(((BgL_tclassz00_bglt)
																	COBJECT(BgL_auxz00_1766))->BgL_holderz00);
														}
														{	/* Object/method.scm 107 */
															obj_t BgL_modulez00_907;

															BgL_modulez00_907 =
																(((BgL_globalz00_bglt)
																	COBJECT(BgL_holderz00_906))->BgL_modulez00);
															{	/* Object/method.scm 108 */
																obj_t BgL_genericz00_908;

																BgL_genericz00_908 =
																	BGl_findzd2globalzd2zzast_envz00
																	(BgL_idz00_898, BNIL);
																{	/* Object/method.scm 109 */

																	{	/* Object/method.scm 111 */
																		bool_t BgL_test1813z00_1776;

																		{	/* Object/method.scm 111 */
																			obj_t BgL_classz00_1261;

																			BgL_classz00_1261 =
																				BGl_globalz00zzast_varz00;
																			if (BGL_OBJECTP(BgL_genericz00_908))
																				{	/* Object/method.scm 111 */
																					BgL_objectz00_bglt
																						BgL_arg1807z00_1263;
																					BgL_arg1807z00_1263 =
																						(BgL_objectz00_bglt)
																						(BgL_genericz00_908);
																					if (BGL_CONDEXPAND_ISA_ARCH64())
																						{	/* Object/method.scm 111 */
																							long BgL_idxz00_1269;

																							BgL_idxz00_1269 =
																								BGL_OBJECT_INHERITANCE_NUM
																								(BgL_arg1807z00_1263);
																							BgL_test1813z00_1776 =
																								(VECTOR_REF
																								(BGl_za2inheritancesza2z00zz__objectz00,
																									(BgL_idxz00_1269 + 2L)) ==
																								BgL_classz00_1261);
																						}
																					else
																						{	/* Object/method.scm 111 */
																							bool_t BgL_res1748z00_1294;

																							{	/* Object/method.scm 111 */
																								obj_t BgL_oclassz00_1277;

																								{	/* Object/method.scm 111 */
																									obj_t BgL_arg1815z00_1285;
																									long BgL_arg1816z00_1286;

																									BgL_arg1815z00_1285 =
																										(BGl_za2classesza2z00zz__objectz00);
																									{	/* Object/method.scm 111 */
																										long BgL_arg1817z00_1287;

																										BgL_arg1817z00_1287 =
																											BGL_OBJECT_CLASS_NUM
																											(BgL_arg1807z00_1263);
																										BgL_arg1816z00_1286 =
																											(BgL_arg1817z00_1287 -
																											OBJECT_TYPE);
																									}
																									BgL_oclassz00_1277 =
																										VECTOR_REF
																										(BgL_arg1815z00_1285,
																										BgL_arg1816z00_1286);
																								}
																								{	/* Object/method.scm 111 */
																									bool_t
																										BgL__ortest_1115z00_1278;
																									BgL__ortest_1115z00_1278 =
																										(BgL_classz00_1261 ==
																										BgL_oclassz00_1277);
																									if (BgL__ortest_1115z00_1278)
																										{	/* Object/method.scm 111 */
																											BgL_res1748z00_1294 =
																												BgL__ortest_1115z00_1278;
																										}
																									else
																										{	/* Object/method.scm 111 */
																											long BgL_odepthz00_1279;

																											{	/* Object/method.scm 111 */
																												obj_t
																													BgL_arg1804z00_1280;
																												BgL_arg1804z00_1280 =
																													(BgL_oclassz00_1277);
																												BgL_odepthz00_1279 =
																													BGL_CLASS_DEPTH
																													(BgL_arg1804z00_1280);
																											}
																											if (
																												(2L <
																													BgL_odepthz00_1279))
																												{	/* Object/method.scm 111 */
																													obj_t
																														BgL_arg1802z00_1282;
																													{	/* Object/method.scm 111 */
																														obj_t
																															BgL_arg1803z00_1283;
																														BgL_arg1803z00_1283
																															=
																															(BgL_oclassz00_1277);
																														BgL_arg1802z00_1282
																															=
																															BGL_CLASS_ANCESTORS_REF
																															(BgL_arg1803z00_1283,
																															2L);
																													}
																													BgL_res1748z00_1294 =
																														(BgL_arg1802z00_1282
																														==
																														BgL_classz00_1261);
																												}
																											else
																												{	/* Object/method.scm 111 */
																													BgL_res1748z00_1294 =
																														((bool_t) 0);
																												}
																										}
																								}
																							}
																							BgL_test1813z00_1776 =
																								BgL_res1748z00_1294;
																						}
																				}
																			else
																				{	/* Object/method.scm 111 */
																					BgL_test1813z00_1776 = ((bool_t) 0);
																				}
																		}
																		if (BgL_test1813z00_1776)
																			{	/* Object/method.scm 114 */
																				obj_t BgL_bodyz00_910;

																				{	/* Object/method.scm 116 */
																					obj_t BgL_arg1509z00_965;

																					{	/* Object/method.scm 116 */
																						obj_t BgL_arg1513z00_966;
																						obj_t BgL_arg1514z00_967;

																						{	/* Object/method.scm 116 */
																							obj_t BgL_arg1516z00_968;

																							{	/* Object/method.scm 116 */
																								obj_t BgL_arg1535z00_969;

																								{	/* Object/method.scm 116 */
																									obj_t BgL_arg1540z00_970;

																									{	/* Object/method.scm 116 */
																										obj_t BgL_arg1544z00_971;

																										{	/* Object/method.scm 116 */
																											obj_t BgL_arg1546z00_972;

																											{	/* Object/method.scm 116 */
																												obj_t
																													BgL_arg1552z00_973;
																												obj_t
																													BgL_arg1553z00_974;
																												{	/* Object/method.scm 116 */
																													obj_t
																														BgL_arg1559z00_975;
																													{	/* Object/method.scm 116 */
																														obj_t
																															BgL_arg1561z00_976;
																														{	/* Object/method.scm 116 */
																															obj_t
																																BgL_arg1564z00_977;
																															{	/* Object/method.scm 116 */
																																obj_t
																																	BgL_arg1565z00_978;
																																{	/* Object/method.scm 116 */
																																	obj_t
																																		BgL_arg1571z00_979;
																																	obj_t
																																		BgL_arg1573z00_980;
																																	BgL_arg1571z00_979
																																		=
																																		CAR(((obj_t)
																																			BgL_argszd2idzd2_902));
																																	{	/* Object/method.scm 118 */
																																		obj_t
																																			BgL_arg1575z00_981;
																																		{	/* Object/method.scm 118 */
																																			obj_t
																																				BgL_arg1576z00_982;
																																			{	/* Object/method.scm 118 */
																																				obj_t
																																					BgL_arg1584z00_983;
																																				{	/* Object/method.scm 118 */
																																					obj_t
																																						BgL_arg1585z00_984;
																																					obj_t
																																						BgL_arg1589z00_985;
																																					BgL_arg1585z00_984
																																						=
																																						(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) BgL_holderz00_906)))->BgL_idz00);
																																					BgL_arg1589z00_985
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_modulez00_907,
																																						BNIL);
																																					BgL_arg1584z00_983
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1585z00_984,
																																						BgL_arg1589z00_985);
																																				}
																																				BgL_arg1576z00_982
																																					=
																																					MAKE_YOUNG_PAIR
																																					(CNST_TABLE_REF
																																					(2),
																																					BgL_arg1584z00_983);
																																			}
																																			BgL_arg1575z00_981
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1576z00_982,
																																				BNIL);
																																		}
																																		BgL_arg1573z00_980
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_idz00_898,
																																			BgL_arg1575z00_981);
																																	}
																																	BgL_arg1565z00_978
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1571z00_979,
																																		BgL_arg1573z00_980);
																																}
																																BgL_arg1564z00_977
																																	=
																																	MAKE_YOUNG_PAIR
																																	(CNST_TABLE_REF
																																	(3),
																																	BgL_arg1565z00_978);
																															}
																															BgL_arg1561z00_976
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1564z00_977,
																																BNIL);
																														}
																														BgL_arg1559z00_975 =
																															MAKE_YOUNG_PAIR
																															(BgL_metz00_899,
																															BgL_arg1561z00_976);
																													}
																													BgL_arg1552z00_973 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1559z00_975,
																														BNIL);
																												}
																												{	/* Object/method.scm 120 */
																													obj_t
																														BgL_arg1591z00_986;
																													if (BgL_dssslz00_901)
																														{	/* Object/method.scm 121 */
																															obj_t
																																BgL_arg1593z00_987;
																															{	/* Object/method.scm 121 */
																																obj_t
																																	BgL_arg1594z00_988;
																																{	/* Object/method.scm 121 */
																																	obj_t
																																		BgL_arg1595z00_989;
																																	{	/* Object/method.scm 121 */
																																		obj_t
																																			BgL_arg1602z00_990;
																																		BgL_arg1602z00_990
																																			=
																																			BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																			(BgL_argszd2idzd2_902,
																																			BNIL);
																																		BgL_arg1595z00_989
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(4),
																																			BgL_arg1602z00_990);
																																	}
																																	BgL_arg1594z00_988
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1595z00_989,
																																		BNIL);
																																}
																																BgL_arg1593z00_987
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_metz00_899,
																																	BgL_arg1594z00_988);
																															}
																															BgL_arg1591z00_986
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(5),
																																BgL_arg1593z00_987);
																														}
																													else
																														{	/* Object/method.scm 120 */
																															if (
																																(BgL_arityz00_900
																																	>= 0L))
																																{	/* Object/method.scm 122 */
																																	BgL_arg1591z00_986
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_metz00_899,
																																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																		(BgL_argszd2idzd2_902,
																																			BNIL));
																																}
																															else
																																{	/* Object/method.scm 125 */
																																	obj_t
																																		BgL_arg1606z00_993;
																																	{	/* Object/method.scm 125 */
																																		obj_t
																																			BgL_arg1609z00_994;
																																		{	/* Object/method.scm 125 */
																																			obj_t
																																				BgL_arg1611z00_995;
																																			{	/* Object/method.scm 125 */
																																				obj_t
																																					BgL_arg1613z00_996;
																																				BgL_arg1613z00_996
																																					=
																																					BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																					(BgL_argszd2idzd2_902,
																																					BNIL);
																																				BgL_arg1611z00_995
																																					=
																																					MAKE_YOUNG_PAIR
																																					(CNST_TABLE_REF
																																					(4),
																																					BgL_arg1613z00_996);
																																			}
																																			BgL_arg1609z00_994
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1611z00_995,
																																				BNIL);
																																		}
																																		BgL_arg1606z00_993
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_metz00_899,
																																			BgL_arg1609z00_994);
																																	}
																																	BgL_arg1591z00_986
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(5),
																																		BgL_arg1606z00_993);
																																}
																														}
																													BgL_arg1553z00_974 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1591z00_986,
																														BNIL);
																												}
																												BgL_arg1546z00_972 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1552z00_973,
																													BgL_arg1553z00_974);
																											}
																											BgL_arg1544z00_971 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(6),
																												BgL_arg1546z00_972);
																										}
																										BgL_arg1540z00_970 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1544z00_971,
																											BNIL);
																									}
																									BgL_arg1535z00_969 =
																										MAKE_YOUNG_PAIR(BNIL,
																										BgL_arg1540z00_970);
																								}
																								BgL_arg1516z00_968 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(7), BgL_arg1535z00_969);
																							}
																							BgL_arg1513z00_966 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1516z00_968, BNIL);
																						}
																						{	/* Object/method.scm 126 */
																							obj_t BgL_arg1615z00_997;

																							if (BgL_dssslz00_901)
																								{	/* Object/method.scm 126 */
																									BgL_arg1615z00_997 =
																										BGl_makezd2dssslzd2functionzd2preludezd2zz__dssslz00
																										(BgL_identz00_8,
																										BgL_argsz00_9,
																										BgL_bodyz00_11,
																										BGl_errorzd2envzd2zz__errorz00);
																								}
																							else
																								{	/* Object/method.scm 126 */
																									BgL_arg1615z00_997 =
																										BgL_bodyz00_11;
																								}
																							BgL_arg1514z00_967 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1615z00_997, BNIL);
																						}
																						BgL_arg1509z00_965 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1513z00_966,
																							BgL_arg1514z00_967);
																					}
																					BgL_bodyz00_910 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(8),
																						BgL_arg1509z00_965);
																				}
																				{	/* Object/method.scm 114 */
																					obj_t BgL_ebodyz00_911;

																					if (EPAIRP(BgL_srcz00_12))
																						{	/* Object/method.scm 130 */
																							obj_t BgL_arg1485z00_962;
																							obj_t BgL_arg1489z00_963;
																							obj_t BgL_arg1502z00_964;

																							BgL_arg1485z00_962 =
																								CAR(BgL_bodyz00_910);
																							BgL_arg1489z00_963 =
																								CDR(BgL_bodyz00_910);
																							BgL_arg1502z00_964 =
																								CER(((obj_t) BgL_srcz00_12));
																							{	/* Object/method.scm 130 */
																								obj_t BgL_res1749z00_1301;

																								BgL_res1749z00_1301 =
																									MAKE_YOUNG_EPAIR
																									(BgL_arg1485z00_962,
																									BgL_arg1489z00_963,
																									BgL_arg1502z00_964);
																								BgL_ebodyz00_911 =
																									BgL_res1749z00_1301;
																							}
																						}
																					else
																						{	/* Object/method.scm 129 */
																							BgL_ebodyz00_911 =
																								BgL_bodyz00_910;
																						}
																					{	/* Object/method.scm 129 */
																						obj_t BgL_tmzd2idzd2_912;

																						{	/* Object/method.scm 132 */
																							bool_t BgL_test1824z00_1856;

																							{	/* Object/method.scm 132 */
																								BgL_typez00_bglt
																									BgL_arg1472z00_960;
																								BgL_arg1472z00_960 =
																									(((BgL_variablez00_bglt)
																										COBJECT((
																												(BgL_variablez00_bglt) (
																													(BgL_globalz00_bglt)
																													BgL_genericz00_908))))->
																									BgL_typez00);
																								BgL_test1824z00_1856 =
																									BGl_bigloozd2typezf3z21zztype_typez00
																									(BgL_arg1472z00_960);
																							}
																							if (BgL_test1824z00_1856)
																								{	/* Object/method.scm 133 */
																									obj_t BgL_arg1453z00_958;

																									BgL_arg1453z00_958 =
																										(((BgL_typez00_bglt)
																											COBJECT(((
																														(BgL_variablez00_bglt)
																														COBJECT((
																																(BgL_variablez00_bglt)
																																((BgL_globalz00_bglt) BgL_genericz00_908))))->BgL_typez00)))->BgL_idz00);
																									BgL_tmzd2idzd2_912 =
																										BGl_makezd2typedzd2identz00zzast_identz00
																										(BgL_mzd2idzd2_904,
																										BgL_arg1453z00_958);
																								}
																							else
																								{	/* Object/method.scm 132 */
																									BgL_tmzd2idzd2_912 =
																										BgL_mzd2idzd2_904;
																								}
																						}
																						{	/* Object/method.scm 132 */
																							obj_t BgL_bdgz00_913;

																							{	/* Object/method.scm 135 */
																								obj_t BgL_arg1422z00_953;

																								{	/* Object/method.scm 135 */
																									obj_t BgL_arg1434z00_954;
																									obj_t BgL_arg1437z00_955;

																									if (BgL_dssslz00_901)
																										{	/* Object/method.scm 135 */
																											BgL_arg1434z00_954 =
																												BGl_dssslzd2formalszd2ze3schemezd2typedzd2formalsze3zz__dssslz00
																												(BgL_argsz00_9,
																												BGl_errorzd2envzd2zz__errorz00,
																												((bool_t) 1));
																										}
																									else
																										{	/* Object/method.scm 135 */
																											BgL_arg1434z00_954 =
																												BgL_argsz00_9;
																										}
																									BgL_arg1437z00_955 =
																										MAKE_YOUNG_PAIR
																										(BgL_ebodyz00_911, BNIL);
																									BgL_arg1422z00_953 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1434z00_954,
																										BgL_arg1437z00_955);
																								}
																								BgL_bdgz00_913 =
																									MAKE_YOUNG_PAIR
																									(BgL_tmzd2idzd2_912,
																									BgL_arg1422z00_953);
																							}
																							{	/* Object/method.scm 135 */
																								obj_t BgL_ebdgz00_914;

																								if (EPAIRP(BgL_srcz00_12))
																									{	/* Object/method.scm 140 */
																										obj_t BgL_arg1408z00_950;
																										obj_t BgL_arg1410z00_951;
																										obj_t BgL_arg1421z00_952;

																										BgL_arg1408z00_950 =
																											CAR(BgL_bdgz00_913);
																										BgL_arg1410z00_951 =
																											CDR(BgL_bdgz00_913);
																										BgL_arg1421z00_952 =
																											CER(
																											((obj_t) BgL_srcz00_12));
																										{	/* Object/method.scm 140 */
																											obj_t BgL_res1750z00_1308;

																											BgL_res1750z00_1308 =
																												MAKE_YOUNG_EPAIR
																												(BgL_arg1408z00_950,
																												BgL_arg1410z00_951,
																												BgL_arg1421z00_952);
																											BgL_ebdgz00_914 =
																												BgL_res1750z00_1308;
																										}
																									}
																								else
																									{	/* Object/method.scm 139 */
																										BgL_ebdgz00_914 =
																											BgL_bdgz00_913;
																									}
																								{	/* Object/method.scm 139 */

																									BGl_za2methodsza2z00zzobject_methodz00
																										=
																										MAKE_YOUNG_PAIR
																										(BgL_mzd2idzd2_904,
																										BGl_za2methodsza2z00zzobject_methodz00);
																									{	/* Object/method.scm 143 */
																										obj_t BgL_arg1319z00_915;

																										{	/* Object/method.scm 143 */
																											obj_t BgL_arg1321z00_917;

																											{	/* Object/method.scm 143 */
																												obj_t
																													BgL_arg1322z00_918;
																												obj_t
																													BgL_arg1323z00_919;
																												BgL_arg1322z00_918 =
																													MAKE_YOUNG_PAIR
																													(BgL_ebdgz00_914,
																													BNIL);
																												{	/* Object/method.scm 144 */
																													obj_t
																														BgL_arg1325z00_920;
																													obj_t
																														BgL_arg1326z00_921;
																													{	/* Object/method.scm 144 */
																														bool_t
																															BgL_test1827z00_1880;
																														if (((long)
																																CINT
																																(BGl_za2debugzd2moduleza2zd2zzengine_paramz00)
																																>= 1L))
																															{	/* Object/method.scm 147 */
																																obj_t
																																	BgL_arg1348z00_937;
																																{	/* Object/method.scm 147 */
																																	obj_t
																																		BgL_arg1349z00_938;
																																	BgL_arg1349z00_938
																																		=
																																		BGl_thezd2backendzd2zzbackend_backendz00
																																		();
																																	BgL_arg1348z00_937
																																		=
																																		(((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt) BgL_arg1349z00_938)))->BgL_debugzd2supportzd2);
																																}
																																BgL_test1827z00_1880
																																	=
																																	CBOOL
																																	(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																																	(CNST_TABLE_REF
																																		(9),
																																		BgL_arg1348z00_937));
																															}
																														else
																															{	/* Object/method.scm 144 */
																																BgL_test1827z00_1880
																																	=
																																	((bool_t) 0);
																															}
																														if (BgL_test1827z00_1880)
																															{	/* Object/method.scm 149 */
																																obj_t
																																	BgL_arg1331z00_926;
																																{	/* Object/method.scm 149 */
																																	obj_t
																																		BgL_arg1332z00_927;
																																	{	/* Object/method.scm 149 */
																																		obj_t
																																			BgL_arg1333z00_928;
																																		obj_t
																																			BgL_arg1335z00_929;
																																		{	/* Object/method.scm 149 */
																																			obj_t
																																				BgL_arg1455z00_1313;
																																			BgL_arg1455z00_1313
																																				=
																																				SYMBOL_TO_STRING
																																				(BgL_identz00_8);
																																			BgL_arg1333z00_928
																																				=
																																				BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																				(BgL_arg1455z00_1313);
																																		}
																																		{	/* Object/method.scm 149 */
																																			obj_t
																																				BgL_arg1346z00_935;
																																			BgL_arg1346z00_935
																																				=
																																				(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) BgL_holderz00_906)))->BgL_idz00);
																																			{	/* Object/method.scm 149 */
																																				obj_t
																																					BgL_arg1455z00_1316;
																																				BgL_arg1455z00_1316
																																					=
																																					SYMBOL_TO_STRING
																																					(BgL_arg1346z00_935);
																																				BgL_arg1335z00_929
																																					=
																																					BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																					(BgL_arg1455z00_1316);
																																			}
																																		}
																																		{	/* Object/method.scm 149 */
																																			obj_t
																																				BgL_list1336z00_930;
																																			{	/* Object/method.scm 149 */
																																				obj_t
																																					BgL_arg1339z00_931;
																																				{	/* Object/method.scm 149 */
																																					obj_t
																																						BgL_arg1340z00_932;
																																					{	/* Object/method.scm 149 */
																																						obj_t
																																							BgL_arg1342z00_933;
																																						{	/* Object/method.scm 149 */
																																							obj_t
																																								BgL_arg1343z00_934;
																																							BgL_arg1343z00_934
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_string1762z00zzobject_methodz00,
																																								BNIL);
																																							BgL_arg1342z00_933
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg1335z00_929,
																																								BgL_arg1343z00_934);
																																						}
																																						BgL_arg1340z00_932
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BGl_string1763z00zzobject_methodz00,
																																							BgL_arg1342z00_933);
																																					}
																																					BgL_arg1339z00_931
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1333z00_928,
																																						BgL_arg1340z00_932);
																																				}
																																				BgL_list1336z00_930
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BGl_string1764z00zzobject_methodz00,
																																					BgL_arg1339z00_931);
																																			}
																																			BgL_arg1332z00_927
																																				=
																																				BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																																				(BgL_list1336z00_930);
																																		}
																																	}
																																	BgL_arg1331z00_926
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1332z00_927,
																																		BNIL);
																																}
																																BgL_arg1325z00_920
																																	=
																																	MAKE_YOUNG_PAIR
																																	(CNST_TABLE_REF
																																	(10),
																																	BgL_arg1331z00_926);
																															}
																														else
																															{	/* Object/method.scm 144 */
																																BgL_arg1325z00_920
																																	= BFALSE;
																															}
																													}
																													{	/* Object/method.scm 152 */
																														obj_t
																															BgL_arg1351z00_939;
																														{	/* Object/method.scm 152 */
																															obj_t
																																BgL_arg1352z00_940;
																															{	/* Object/method.scm 152 */
																																obj_t
																																	BgL_arg1361z00_941;
																																{	/* Object/method.scm 152 */
																																	obj_t
																																		BgL_arg1364z00_942;
																																	obj_t
																																		BgL_arg1367z00_943;
																																	{	/* Object/method.scm 152 */
																																		obj_t
																																			BgL_arg1370z00_944;
																																		{	/* Object/method.scm 152 */
																																			obj_t
																																				BgL_arg1371z00_945;
																																			obj_t
																																				BgL_arg1375z00_946;
																																			BgL_arg1371z00_945
																																				=
																																				(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) BgL_holderz00_906)))->BgL_idz00);
																																			BgL_arg1375z00_946
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_modulez00_907,
																																				BNIL);
																																			BgL_arg1370z00_944
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1371z00_945,
																																				BgL_arg1375z00_946);
																																		}
																																		BgL_arg1364z00_942
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(2),
																																			BgL_arg1370z00_944);
																																	}
																																	{	/* Object/method.scm 154 */
																																		obj_t
																																			BgL_arg1376z00_947;
																																		{	/* Object/method.scm 154 */
																																			obj_t
																																				BgL_arg1377z00_948;
																																			{	/* Object/method.scm 154 */
																																				obj_t
																																					BgL_arg1455z00_1319;
																																				BgL_arg1455z00_1319
																																					=
																																					SYMBOL_TO_STRING
																																					(BgL_identz00_8);
																																				BgL_arg1377z00_948
																																					=
																																					BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																					(BgL_arg1455z00_1319);
																																			}
																																			BgL_arg1376z00_947
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1377z00_948,
																																				BNIL);
																																		}
																																		BgL_arg1367z00_943
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_mzd2idzd2_904,
																																			BgL_arg1376z00_947);
																																	}
																																	BgL_arg1361z00_941
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1364z00_942,
																																		BgL_arg1367z00_943);
																																}
																																BgL_arg1352z00_940
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_idz00_898,
																																	BgL_arg1361z00_941);
																															}
																															BgL_arg1351z00_939
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(11),
																																BgL_arg1352z00_940);
																														}
																														BgL_arg1326z00_921 =
																															MAKE_YOUNG_PAIR
																															(BgL_arg1351z00_939,
																															BNIL);
																													}
																													BgL_arg1323z00_919 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1325z00_920,
																														BgL_arg1326z00_921);
																												}
																												BgL_arg1321z00_917 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1322z00_918,
																													BgL_arg1323z00_919);
																											}
																											BgL_arg1319z00_915 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(8),
																												BgL_arg1321z00_917);
																										}
																										{	/* Object/method.scm 143 */
																											obj_t BgL_list1320z00_916;

																											BgL_list1320z00_916 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1319z00_915,
																												BNIL);
																											return
																												BgL_list1320z00_916;
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		else
																			{	/* Object/method.scm 193 */
																				obj_t BgL_arg1700z00_1321;

																				{	/* Object/method.scm 193 */
																					obj_t BgL_list1702z00_1322;

																					BgL_list1702z00_1322 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(12),
																						BNIL);
																					BgL_arg1700z00_1321 =
																						BgL_list1702z00_1322;
																				}
																				{	/* Object/method.scm 193 */
																					obj_t BgL_list1701z00_1323;

																					BgL_list1701z00_1323 =
																						MAKE_YOUNG_PAIR(BgL_arg1700z00_1321,
																						BNIL);
																					return
																						BGl_userzd2errorzd2zztools_errorz00
																						(BgL_idz00_898,
																						BGl_string1765z00zzobject_methodz00,
																						BgL_srcz00_12,
																						BgL_list1701z00_1323);
																				}
																			}
																	}
																}
															}
														}
													}
												else
													{	/* Object/method.scm 193 */
														obj_t BgL_arg1700z00_1325;

														{	/* Object/method.scm 193 */
															obj_t BgL_list1702z00_1326;

															BgL_list1702z00_1326 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(12), BNIL);
															BgL_arg1700z00_1325 = BgL_list1702z00_1326;
														}
														{	/* Object/method.scm 193 */
															obj_t BgL_list1701z00_1327;

															BgL_list1701z00_1327 =
																MAKE_YOUNG_PAIR(BgL_arg1700z00_1325, BNIL);
															return
																BGl_userzd2errorzd2zztools_errorz00
																(BgL_idz00_898,
																BGl_string1766z00zzobject_methodz00,
																BgL_srcz00_12, BgL_list1701z00_1327);
														}
													}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &make-method-dsssl-body */
	obj_t BGl_z62makezd2methodzd2dssslzd2bodyzb0zzobject_methodz00(obj_t
		BgL_envz00_1415, obj_t BgL_identz00_1416, obj_t BgL_argsz00_1417,
		obj_t BgL_localsz00_1418, obj_t BgL_bodyz00_1419, obj_t BgL_srcz00_1420)
	{
		{	/* Object/method.scm 93 */
			return
				BGl_makezd2methodzd2dssslzd2bodyzd2zzobject_methodz00(BgL_identz00_1416,
				BgL_argsz00_1417, BgL_localsz00_1418, BgL_bodyz00_1419,
				BgL_srcz00_1420);
		}

	}



/* dsssl-method? */
	bool_t BGl_dssslzd2methodzf3z21zzobject_methodz00(obj_t BgL_argsz00_13)
	{
		{	/* Object/method.scm 159 */
			if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(BgL_argsz00_13))
				{
					obj_t BgL_l1116z00_1025;

					BgL_l1116z00_1025 = BgL_argsz00_13;
				BgL_zc3z04anonymousza31676ze3z87_1026:
					if (NULLP(BgL_l1116z00_1025))
						{	/* Object/method.scm 160 */
							return ((bool_t) 0);
						}
					else
						{	/* Object/method.scm 160 */
							bool_t BgL__ortest_1118z00_1028;

							{	/* Object/method.scm 160 */
								obj_t BgL_arg1681z00_1030;

								BgL_arg1681z00_1030 = CAR(((obj_t) BgL_l1116z00_1025));
								BgL__ortest_1118z00_1028 =
									BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00
									(BgL_arg1681z00_1030);
							}
							if (BgL__ortest_1118z00_1028)
								{	/* Object/method.scm 160 */
									return BgL__ortest_1118z00_1028;
								}
							else
								{	/* Object/method.scm 160 */
									obj_t BgL_arg1678z00_1029;

									BgL_arg1678z00_1029 = CDR(((obj_t) BgL_l1116z00_1025));
									{
										obj_t BgL_l1116z00_1944;

										BgL_l1116z00_1944 = BgL_arg1678z00_1029;
										BgL_l1116z00_1025 = BgL_l1116z00_1944;
										goto BgL_zc3z04anonymousza31676ze3z87_1026;
									}
								}
						}
				}
			else
				{	/* Object/method.scm 160 */
					return ((bool_t) 0);
				}
		}

	}



/* pair->list */
	obj_t BGl_pairzd2ze3listz31zzobject_methodz00(obj_t BgL_pz00_14)
	{
		{	/* Object/method.scm 166 */
			if (PAIRP(BgL_pz00_14))
				{	/* Object/method.scm 168 */
					return
						MAKE_YOUNG_PAIR(CAR(BgL_pz00_14),
						BGl_pairzd2ze3listz31zzobject_methodz00(CDR(BgL_pz00_14)));
				}
			else
				{	/* Object/method.scm 168 */
					if (NULLP(BgL_pz00_14))
						{	/* Object/method.scm 169 */
							return BNIL;
						}
					else
						{	/* Object/method.scm 170 */
							obj_t BgL_list1693z00_1037;

							BgL_list1693z00_1037 = MAKE_YOUNG_PAIR(BgL_pz00_14, BNIL);
							return BgL_list1693z00_1037;
						}
				}
		}

	}



/* mark-method! */
	BGL_EXPORTED_DEF obj_t BGl_markzd2methodz12zc0zzobject_methodz00(obj_t
		BgL_idz00_15)
	{
		{	/* Object/method.scm 180 */
			return (BGl_za2methodsza2z00zzobject_methodz00 =
				MAKE_YOUNG_PAIR(BgL_idz00_15, BGl_za2methodsza2z00zzobject_methodz00),
				BUNSPEC);
		}

	}



/* &mark-method! */
	obj_t BGl_z62markzd2methodz12za2zzobject_methodz00(obj_t BgL_envz00_1425,
		obj_t BgL_idz00_1426)
	{
		{	/* Object/method.scm 180 */
			return BGl_markzd2methodz12zc0zzobject_methodz00(BgL_idz00_1426);
		}

	}



/* local-is-method? */
	BGL_EXPORTED_DEF bool_t
		BGl_localzd2iszd2methodzf3zf3zzobject_methodz00(BgL_localz00_bglt
		BgL_localz00_16)
	{
		{	/* Object/method.scm 186 */
			{	/* Object/method.scm 187 */
				obj_t BgL_arg1699z00_1334;

				BgL_arg1699z00_1334 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_localz00_16)))->BgL_idz00);
				return
					CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_arg1699z00_1334,
						BGl_za2methodsza2z00zzobject_methodz00));
			}
		}

	}



/* &local-is-method? */
	obj_t BGl_z62localzd2iszd2methodzf3z91zzobject_methodz00(obj_t
		BgL_envz00_1427, obj_t BgL_localz00_1428)
	{
		{	/* Object/method.scm 186 */
			return
				BBOOL(BGl_localzd2iszd2methodzf3zf3zzobject_methodz00(
					((BgL_localz00_bglt) BgL_localz00_1428)));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzobject_methodz00(void)
	{
		{	/* Object/method.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzobject_methodz00(void)
	{
		{	/* Object/method.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzobject_methodz00(void)
	{
		{	/* Object/method.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzobject_methodz00(void)
	{
		{	/* Object/method.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_argsz00(47102372L,
				BSTRING_TO_STRING(BGl_string1767z00zzobject_methodz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1767z00zzobject_methodz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1767z00zzobject_methodz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1767z00zzobject_methodz00));
			BGl_modulezd2initializa7ationz75zztools_dssslz00(275867955L,
				BSTRING_TO_STRING(BGl_string1767z00zzobject_methodz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1767z00zzobject_methodz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1767z00zzobject_methodz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1767z00zzobject_methodz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1767z00zzobject_methodz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1767z00zzobject_methodz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1767z00zzobject_methodz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1767z00zzobject_methodz00));
			return
				BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1767z00zzobject_methodz00));
		}

	}

#ifdef __cplusplus
}
#endif
