/*===========================================================================*/
/*   (Object/slots.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Object/slots.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_OBJECT_SLOTS_TYPE_DEFINITIONS
#define BGL_OBJECT_SLOTS_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_tclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		struct BgL_globalz00_bgl *BgL_holderz00;
		obj_t BgL_wideningz00;
		long BgL_depthz00;
		bool_t BgL_finalzf3zf3;
		obj_t BgL_constructorz00;
		obj_t BgL_virtualzd2slotszd2numberz00;
		bool_t BgL_abstractzf3zf3;
		obj_t BgL_widezd2typezd2;
		obj_t BgL_subclassesz00;
	}                *BgL_tclassz00_bglt;

	typedef struct BgL_jclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		obj_t BgL_packagez00;
	}                *BgL_jclassz00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_slotz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_srcz00;
		obj_t BgL_classzd2ownerzd2;
		long BgL_indexz00;
		obj_t BgL_typez00;
		bool_t BgL_readzd2onlyzf3z21;
		obj_t BgL_defaultzd2valuezd2;
		obj_t BgL_virtualzd2numzd2;
		bool_t BgL_virtualzd2overridezd2;
		obj_t BgL_getterz00;
		obj_t BgL_setterz00;
		obj_t BgL_userzd2infozd2;
	}              *BgL_slotz00_bglt;


#endif													// BGL_OBJECT_SLOTS_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62lambda1927z62zzobject_slotsz00(obj_t, obj_t);
	static BgL_slotz00_bglt BGl_z62lambda1846z62zzobject_slotsz00(obj_t);
	static obj_t BGl_z62lambda1928z62zzobject_slotsz00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_ensurezd2typezd2definedz12z12zzobject_slotsz00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62lambda1852z62zzobject_slotsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1853z62zzobject_slotsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1934z62zzobject_slotsz00(obj_t, obj_t);
	static obj_t BGl_z62makezd2javazd2classzd2slotszb0zzobject_slotsz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1935z62zzobject_slotsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1858z62zzobject_slotsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1859z62zzobject_slotsz00(obj_t, obj_t, obj_t);
	BGL_IMPORT int BGl_bigloozd2warningzd2zz__paramz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2classzd2slotsz00zzobject_slotsz00(BgL_typez00_bglt, obj_t, obj_t,
		int, obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	extern obj_t BGl_za2astzd2casezd2sensitiveza2z00zzengine_paramz00;
	static obj_t BGl_requirezd2initializa7ationz75zzobject_slotsz00 = BUNSPEC;
	extern obj_t BGl_makezd2typedzd2identz00zzast_identz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31936ze3ze5zzobject_slotsz00(obj_t);
	static obj_t BGl_z62lambda1864z62zzobject_slotsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1865z62zzobject_slotsz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_userzd2errorzf2locationz20zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_slotzd2indexzd2zzobject_slotsz00(BgL_slotz00_bglt);
	extern obj_t BGl_findzd2locationzf2locz20zztools_locationz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1870z62zzobject_slotsz00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	static obj_t BGl_z62lambda1871z62zzobject_slotsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31929ze3ze5zzobject_slotsz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31848ze3ze5zzobject_slotsz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1875z62zzobject_slotsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1876z62zzobject_slotsz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_tclassz00zzobject_classz00;
	static obj_t BGl_z62slotzd2setterzb0zzobject_slotsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2classzd2makezd2formalszd2zzobject_slotsz00(obj_t);
	static obj_t BGl_z62lambda1880z62zzobject_slotsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1881z62zzobject_slotsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1886z62zzobject_slotsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1887z62zzobject_slotsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62slotzd2classzd2ownerz62zzobject_slotsz00(obj_t, obj_t);
	static obj_t BGl_slotzd2inlinezd2markz00zzobject_slotsz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zzobject_slotsz00(void);
	static obj_t BGl_z62slotzd2indexzb0zzobject_slotsz00(obj_t, obj_t);
	static obj_t BGl_z62slotzd2namezb0zzobject_slotsz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_z62lambda1893z62zzobject_slotsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1894z62zzobject_slotsz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_reverse(obj_t);
	static obj_t BGl_genericzd2initzd2zzobject_slotsz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_slotzd2virtualzd2numz00zzobject_slotsz00(BgL_slotz00_bglt);
	static obj_t BGl_objectzd2initzd2zzobject_slotsz00(void);
	static obj_t BGl_z62slotzd2defaultzf3z43zzobject_slotsz00(obj_t, obj_t);
	static obj_t BGl_z62slotzd2srczb0zzobject_slotsz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_findzd2slotzd2typez00zzobject_slotsz00(obj_t, obj_t);
	static obj_t BGl_z62slotzd2getterzb0zzobject_slotsz00(obj_t, obj_t);
	static obj_t BGl_z62makezd2classzd2slotsz62zzobject_slotsz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62slotzd2virtualzf3z43zzobject_slotsz00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	BGL_EXPORTED_DECL bool_t
		BGl_slotzd2virtualzd2overridez00zzobject_slotsz00(BgL_slotz00_bglt);
	static obj_t BGl_z62slotzd2defaultzd2inlinezd2valuezb0zzobject_slotsz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_slotzd2namezd2zzobject_slotsz00(BgL_slotz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_slotzd2defaultzd2valuez00zzobject_slotsz00(BgL_slotz00_bglt);
	static obj_t
		BGl_z62makezd2classzd2makezd2typedzd2formalsz62zzobject_slotsz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t bgl_bignum_add(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31895ze3ze5zzobject_slotsz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_slotzd2idzd2zzobject_slotsz00(BgL_slotz00_bglt);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_slotzd2setterzd2setz12z12zzobject_slotsz00(BgL_slotz00_bglt, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_slotz00zzobject_slotsz00 = BUNSPEC;
	extern obj_t BGl_za2warningzd2overridenzd2slotsza2z00zzengine_paramz00;
	static obj_t BGl_slotzd2memberzf3ze70zc6zzobject_slotsz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62slotzd2typezb0zzobject_slotsz00(obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzobject_slotsz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31888ze3ze5zzobject_slotsz00(obj_t);
	static obj_t BGl_methodzd2initzd2zzobject_slotsz00(void);
	static BgL_slotz00_bglt BGl_z62makezd2slotzb0zzobject_slotsz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62slotzd2virtualzd2overridez62zzobject_slotsz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_slotzd2defaultzd2inlinezd2valuezd2zzobject_slotsz00(BgL_slotz00_bglt);
	static obj_t BGl_z62slotzd2virtualzd2numz62zzobject_slotsz00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_long_to_bignum(long);
	static obj_t
		BGl_z62getzd2localzd2virtualzd2slotszd2numberz62zzobject_slotsz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_slotzd2typezd2zzobject_slotsz00(BgL_slotz00_bglt);
	BGL_EXPORTED_DECL BgL_slotz00_bglt BGl_makezd2slotzd2zzobject_slotsz00(obj_t,
		obj_t, obj_t, obj_t, long, obj_t, bool_t, obj_t, obj_t, bool_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	extern obj_t BGl_idzd2ze3namez31zzast_identz00(obj_t);
	static obj_t BGl_z62slotzd2setterzd2setz12z70zzobject_slotsz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t BGl_slotzd2srczd2zzobject_slotsz00(BgL_slotz00_bglt);
	extern obj_t BGl_jclassz00zzobject_classz00;
	BGL_EXPORTED_DECL obj_t
		BGl_slotzd2getterzd2setz12z12zzobject_slotsz00(BgL_slotz00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_slotzd2defaultzf3z21zzobject_slotsz00(BgL_slotz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_glozd2declzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztvector_tvectorz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	BGL_EXPORTED_DECL bool_t BGl_slotzf3zf3zzobject_slotsz00(obj_t);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	static obj_t BGl_z62slotzd2userzd2infoz62zzobject_slotsz00(obj_t, obj_t);
	static obj_t BGl_z62makezd2classzd2makezd2formalszb0zzobject_slotsz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_slotzd2virtualzf3z21zzobject_slotsz00(BgL_slotz00_bglt);
	static BgL_slotz00_bglt BGl_z62slotzd2nilzb0zzobject_slotsz00(obj_t);
	BGL_IMPORT obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_getzd2localzd2virtualzd2slotszd2numberz00zzobject_slotsz00
		(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2classzd2makezd2typedzd2formalsz00zzobject_slotsz00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzobject_slotsz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzobject_slotsz00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	extern BgL_globalz00_bglt
		BGl_declarezd2globalzd2cvarz12z12zzast_glozd2declzd2(obj_t, obj_t, obj_t,
		obj_t, bool_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_schemezd2symbolzd2ze3czd2stringz31zzobject_slotsz00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzobject_slotsz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_slotzd2virtualzd2numzd2setz12zc0zzobject_slotsz00(BgL_slotz00_bglt,
		obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzobject_slotsz00(void);
	extern obj_t BGl_parsezd2idzd2zzast_identz00(obj_t, obj_t);
	static obj_t BGl_z62slotzf3z91zzobject_slotsz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31902ze3ze5zzobject_slotsz00(obj_t);
	BGL_IMPORT obj_t BGl_objectz00zz__objectz00;
	static obj_t BGl_z62slotzd2getterzd2setz12z70zzobject_slotsz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_slotzd2userzd2infoz00zzobject_slotsz00(BgL_slotz00_bglt);
	static obj_t BGl_z62shapezd2slot1171zb0zzobject_slotsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_slotzd2readzd2onlyzf3zf3zzobject_slotsz00(BgL_slotz00_bglt);
	static obj_t BGl_z62slotzd2readzd2onlyzf3z91zzobject_slotsz00(obj_t, obj_t);
	static obj_t BGl_z62slotzd2idzb0zzobject_slotsz00(obj_t, obj_t);
	static obj_t BGl_z62slotzd2inlinezd2defaultzf3z91zzobject_slotsz00(obj_t,
		obj_t);
	extern obj_t BGl_userzd2warningzf2locationz20zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31921ze3ze5zzobject_slotsz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31913ze3ze5zzobject_slotsz00(obj_t);
	static obj_t BGl_z62slotzd2virtualzd2numzd2setz12za2zzobject_slotsz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62slotzd2defaultzd2valuez62zzobject_slotsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_slotzd2setterzd2zzobject_slotsz00(BgL_slotz00_bglt);
	extern obj_t BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_slotzd2inlinezd2defaultzf3zf3zzobject_slotsz00(BgL_slotz00_bglt);
	static obj_t BGl_loopze70ze7zzobject_slotsz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2javazd2classzd2slotszd2zzobject_slotsz00(BgL_typez00_bglt, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1900z62zzobject_slotsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1901z62zzobject_slotsz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_slotz00_bglt BGl_slotzd2nilzd2zzobject_slotsz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_slotzd2classzd2ownerz00zzobject_slotsz00(BgL_slotz00_bglt);
	static obj_t BGl_z62lambda1911z62zzobject_slotsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1912z62zzobject_slotsz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_stringzd2downcasezd2zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_slotzd2getterzd2zzobject_slotsz00(BgL_slotz00_bglt);
	static obj_t BGl_z62lambda1919z62zzobject_slotsz00(obj_t, obj_t);
	static obj_t
		BGl_makezd2attributezd2slotze70ze7zzobject_slotsz00(BgL_typez00_bglt, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, bool_t, long);
	static obj_t BGl_z62lambda1920z62zzobject_slotsz00(obj_t, obj_t, obj_t);
	static BgL_slotz00_bglt BGl_z62lambda1844z62zzobject_slotsz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t __cnst[32];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_slotzd2getterzd2setz12zd2envzc0zzobject_slotsz00,
		BgL_bgl_za762slotza7d2getter2039z00,
		BGl_z62slotzd2getterzd2setz12z70zzobject_slotsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2slotzd2envz00zzobject_slotsz00,
		BgL_bgl_za762makeza7d2slotza7b2040za7,
		BGl_z62makezd2slotzb0zzobject_slotsz00, 0L, BUNSPEC, 13);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2localzd2virtualzd2slotszd2numberzd2envzd2zzobject_slotsz00,
		BgL_bgl_za762getza7d2localza7d2041za7,
		BGl_z62getzd2localzd2virtualzd2slotszd2numberz62zzobject_slotsz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2javazd2classzd2slotszd2envz00zzobject_slotsz00,
		BgL_bgl_za762makeza7d2javaza7d2042za7,
		BGl_z62makezd2javazd2classzd2slotszb0zzobject_slotsz00, 0L, BUNSPEC, 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_slotzd2nilzd2envz00zzobject_slotsz00,
		BgL_bgl_za762slotza7d2nilza7b02043za7,
		BGl_z62slotzd2nilzb0zzobject_slotsz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_slotzd2getterzd2envz00zzobject_slotsz00,
		BgL_bgl_za762slotza7d2getter2044z00,
		BGl_z62slotzd2getterzb0zzobject_slotsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_slotzd2virtualzf3zd2envzf3zzobject_slotsz00,
		BgL_bgl_za762slotza7d2virtua2045z00,
		BGl_z62slotzd2virtualzf3z43zzobject_slotsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_slotzd2namezd2envz00zzobject_slotsz00,
		BgL_bgl_za762slotza7d2nameza7b2046za7,
		BGl_z62slotzd2namezb0zzobject_slotsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_slotzd2setterzd2setz12zd2envzc0zzobject_slotsz00,
		BgL_bgl_za762slotza7d2setter2047z00,
		BGl_z62slotzd2setterzd2setz12z70zzobject_slotsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_slotzd2virtualzd2numzd2setz12zd2envz12zzobject_slotsz00,
		BgL_bgl_za762slotza7d2virtua2048z00,
		BGl_z62slotzd2virtualzd2numzd2setz12za2zzobject_slotsz00, 0L, BUNSPEC, 2);
	extern obj_t BGl_shapezd2envzd2zztools_shapez00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_slotzd2srczd2envz00zzobject_slotsz00,
		BgL_bgl_za762slotza7d2srcza7b02049za7,
		BGl_z62slotzd2srczb0zzobject_slotsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2classzd2slotszd2envzd2zzobject_slotsz00,
		BgL_bgl_za762makeza7d2classza72050za7,
		BGl_z62makezd2classzd2slotsz62zzobject_slotsz00, 0L, BUNSPEC, 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_slotzd2classzd2ownerzd2envzd2zzobject_slotsz00,
		BgL_bgl_za762slotza7d2classza72051za7,
		BGl_z62slotzd2classzd2ownerz62zzobject_slotsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_slotzd2typezd2envz00zzobject_slotsz00,
		BgL_bgl_za762slotza7d2typeza7b2052za7,
		BGl_z62slotzd2typezb0zzobject_slotsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_slotzd2virtualzd2numzd2envzd2zzobject_slotsz00,
		BgL_bgl_za762slotza7d2virtua2053z00,
		BGl_z62slotzd2virtualzd2numz62zzobject_slotsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_slotzd2setterzd2envz00zzobject_slotsz00,
		BgL_bgl_za762slotza7d2setter2054z00,
		BGl_z62slotzd2setterzb0zzobject_slotsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_slotzd2virtualzd2overridezd2envzd2zzobject_slotsz00,
		BgL_bgl_za762slotza7d2virtua2055z00,
		BGl_z62slotzd2virtualzd2overridez62zzobject_slotsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_slotzd2defaultzd2inlinezd2valuezd2envz00zzobject_slotsz00,
		BgL_bgl_za762slotza7d2defaul2056z00,
		BGl_z62slotzd2defaultzd2inlinezd2valuezb0zzobject_slotsz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1984z00zzobject_slotsz00,
		BgL_bgl_string1984za700za7za7o2057za7, "Can't find type definition", 26);
	      DEFINE_STRING(BGl_string1985z00zzobject_slotsz00,
		BgL_bgl_string1985za700za7za7o2058za7,
		"slot already defined in super class", 35);
	      DEFINE_STRING(BGl_string1986z00zzobject_slotsz00,
		BgL_bgl_string1986za700za7za7o2059za7, "Illegal slot", 12);
	      DEFINE_STRING(BGl_string1987z00zzobject_slotsz00,
		BgL_bgl_string1987za700za7za7o2060za7, "Illegal duplicated virtual slot",
		31);
	      DEFINE_STRING(BGl_string1988z00zzobject_slotsz00,
		BgL_bgl_string1988za700za7za7o2061za7, "Overriden \"~a\" virtual slot", 27);
	      DEFINE_STRING(BGl_string1989z00zzobject_slotsz00,
		BgL_bgl_string1989za700za7za7o2062za7,
		"Illegal virtual slot (missing getter)", 37);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2classzd2makezd2typedzd2formalszd2envzd2zzobject_slotsz00,
		BgL_bgl_za762makeza7d2classza72063za7,
		BGl_z62makezd2classzd2makezd2typedzd2formalsz62zzobject_slotsz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_slotzd2userzd2infozd2envzd2zzobject_slotsz00,
		BgL_bgl_za762slotza7d2userza7d2064za7,
		BGl_z62slotzd2userzd2infoz62zzobject_slotsz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1990z00zzobject_slotsz00,
		BgL_bgl_string1990za700za7za7o2065za7, "Illegal virtual slot (read-only)",
		32);
	      DEFINE_STRING(BGl_string1991z00zzobject_slotsz00,
		BgL_bgl_string1991za700za7za7o2066za7,
		"Illegal virtual slot (missing setter)", 37);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1992z00zzobject_slotsz00,
		BgL_bgl_za762lambda1853za7622067z00, BGl_z62lambda1853z62zzobject_slotsz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1993z00zzobject_slotsz00,
		BgL_bgl_za762lambda1852za7622068z00, BGl_z62lambda1852z62zzobject_slotsz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1994z00zzobject_slotsz00,
		BgL_bgl_za762lambda1859za7622069z00, BGl_z62lambda1859z62zzobject_slotsz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1995z00zzobject_slotsz00,
		BgL_bgl_za762lambda1858za7622070z00, BGl_z62lambda1858z62zzobject_slotsz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1996z00zzobject_slotsz00,
		BgL_bgl_za762lambda1865za7622071z00, BGl_z62lambda1865z62zzobject_slotsz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1997z00zzobject_slotsz00,
		BgL_bgl_za762lambda1864za7622072z00, BGl_z62lambda1864z62zzobject_slotsz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1998z00zzobject_slotsz00,
		BgL_bgl_za762lambda1871za7622073z00, BGl_z62lambda1871z62zzobject_slotsz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1999z00zzobject_slotsz00,
		BgL_bgl_za762lambda1870za7622074z00, BGl_z62lambda1870z62zzobject_slotsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_slotzf3zd2envz21zzobject_slotsz00,
		BgL_bgl_za762slotza7f3za791za7za7o2075za7,
		BGl_z62slotzf3z91zzobject_slotsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2classzd2makezd2formalszd2envz00zzobject_slotsz00,
		BgL_bgl_za762makeza7d2classza72076za7,
		BGl_z62makezd2classzd2makezd2formalszb0zzobject_slotsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_slotzd2inlinezd2defaultzf3zd2envz21zzobject_slotsz00,
		BgL_bgl_za762slotza7d2inline2077z00,
		BGl_z62slotzd2inlinezd2defaultzf3z91zzobject_slotsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_slotzd2idzd2envz00zzobject_slotsz00,
		BgL_bgl_za762slotza7d2idza7b0za72078z00,
		BGl_z62slotzd2idzb0zzobject_slotsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_slotzd2readzd2onlyzf3zd2envz21zzobject_slotsz00,
		BgL_bgl_za762slotza7d2readza7d2079za7,
		BGl_z62slotzd2readzd2onlyzf3z91zzobject_slotsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2000z00zzobject_slotsz00,
		BgL_bgl_za762lambda1876za7622080z00, BGl_z62lambda1876z62zzobject_slotsz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2001z00zzobject_slotsz00,
		BgL_bgl_za762lambda1875za7622081z00, BGl_z62lambda1875z62zzobject_slotsz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2002z00zzobject_slotsz00,
		BgL_bgl_za762lambda1881za7622082z00, BGl_z62lambda1881z62zzobject_slotsz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2003z00zzobject_slotsz00,
		BgL_bgl_za762lambda1880za7622083z00, BGl_z62lambda1880z62zzobject_slotsz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2004z00zzobject_slotsz00,
		BgL_bgl_za762za7c3za704anonymo2084za7,
		BGl_z62zc3z04anonymousza31888ze3ze5zzobject_slotsz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2005z00zzobject_slotsz00,
		BgL_bgl_za762lambda1887za7622085z00, BGl_z62lambda1887z62zzobject_slotsz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2006z00zzobject_slotsz00,
		BgL_bgl_za762lambda1886za7622086z00, BGl_z62lambda1886z62zzobject_slotsz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2007z00zzobject_slotsz00,
		BgL_bgl_za762za7c3za704anonymo2087za7,
		BGl_z62zc3z04anonymousza31895ze3ze5zzobject_slotsz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_slotzd2indexzd2envz00zzobject_slotsz00,
		BgL_bgl_za762slotza7d2indexza72088za7,
		BGl_z62slotzd2indexzb0zzobject_slotsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2008z00zzobject_slotsz00,
		BgL_bgl_za762lambda1894za7622089z00, BGl_z62lambda1894z62zzobject_slotsz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2009z00zzobject_slotsz00,
		BgL_bgl_za762lambda1893za7622090z00, BGl_z62lambda1893z62zzobject_slotsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_slotzd2defaultzf3zd2envzf3zzobject_slotsz00,
		BgL_bgl_za762slotza7d2defaul2091z00,
		BGl_z62slotzd2defaultzf3z43zzobject_slotsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2010z00zzobject_slotsz00,
		BgL_bgl_za762za7c3za704anonymo2092za7,
		BGl_z62zc3z04anonymousza31902ze3ze5zzobject_slotsz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2011z00zzobject_slotsz00,
		BgL_bgl_za762lambda1901za7622093z00, BGl_z62lambda1901z62zzobject_slotsz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2012z00zzobject_slotsz00,
		BgL_bgl_za762lambda1900za7622094z00, BGl_z62lambda1900z62zzobject_slotsz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2013z00zzobject_slotsz00,
		BgL_bgl_za762za7c3za704anonymo2095za7,
		BGl_z62zc3z04anonymousza31913ze3ze5zzobject_slotsz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2014z00zzobject_slotsz00,
		BgL_bgl_za762lambda1912za7622096z00, BGl_z62lambda1912z62zzobject_slotsz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2015z00zzobject_slotsz00,
		BgL_bgl_za762lambda1911za7622097z00, BGl_z62lambda1911z62zzobject_slotsz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2016z00zzobject_slotsz00,
		BgL_bgl_za762za7c3za704anonymo2098za7,
		BGl_z62zc3z04anonymousza31921ze3ze5zzobject_slotsz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2017z00zzobject_slotsz00,
		BgL_bgl_za762lambda1920za7622099z00, BGl_z62lambda1920z62zzobject_slotsz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2018z00zzobject_slotsz00,
		BgL_bgl_za762lambda1919za7622100z00, BGl_z62lambda1919z62zzobject_slotsz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2019z00zzobject_slotsz00,
		BgL_bgl_za762za7c3za704anonymo2101za7,
		BGl_z62zc3z04anonymousza31929ze3ze5zzobject_slotsz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2028z00zzobject_slotsz00,
		BgL_bgl_string2028za700za7za7o2102za7, "", 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2020z00zzobject_slotsz00,
		BgL_bgl_za762lambda1928za7622103z00, BGl_z62lambda1928z62zzobject_slotsz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2021z00zzobject_slotsz00,
		BgL_bgl_za762lambda1927za7622104z00, BGl_z62lambda1927z62zzobject_slotsz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2022z00zzobject_slotsz00,
		BgL_bgl_za762za7c3za704anonymo2105za7,
		BGl_z62zc3z04anonymousza31936ze3ze5zzobject_slotsz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2023z00zzobject_slotsz00,
		BgL_bgl_za762lambda1935za7622106z00, BGl_z62lambda1935z62zzobject_slotsz00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2030z00zzobject_slotsz00,
		BgL_bgl_string2030za700za7za7o2107za7, "shape", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2024z00zzobject_slotsz00,
		BgL_bgl_za762lambda1934za7622108z00, BGl_z62lambda1934z62zzobject_slotsz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2031z00zzobject_slotsz00,
		BgL_bgl_string2031za700za7za7o2109za7, ")", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2025z00zzobject_slotsz00,
		BgL_bgl_za762za7c3za704anonymo2110za7,
		BGl_z62zc3z04anonymousza31848ze3ze5zzobject_slotsz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2032z00zzobject_slotsz00,
		BgL_bgl_string2032za700za7za7o2111za7, " type: ", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2026z00zzobject_slotsz00,
		BgL_bgl_za762lambda1846za7622112z00, BGl_z62lambda1846z62zzobject_slotsz00,
		0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2033z00zzobject_slotsz00,
		BgL_bgl_string2033za700za7za7o2113za7, " owner: ", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2027z00zzobject_slotsz00,
		BgL_bgl_za762lambda1844za7622114z00, BGl_z62lambda1844z62zzobject_slotsz00,
		0L, BUNSPEC, 13);
	      DEFINE_STRING(BGl_string2034z00zzobject_slotsz00,
		BgL_bgl_string2034za700za7za7o2115za7, " (", 2);
	      DEFINE_STRING(BGl_string2035z00zzobject_slotsz00,
		BgL_bgl_string2035za700za7za7o2116za7, "(", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2029z00zzobject_slotsz00,
		BgL_bgl_za762shapeza7d2slot12117z00,
		BGl_z62shapezd2slot1171zb0zzobject_slotsz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2036z00zzobject_slotsz00,
		BgL_bgl_string2036za700za7za7o2118za7, "object_slots", 12);
	      DEFINE_STRING(BGl_string2037z00zzobject_slotsz00,
		BgL_bgl_string2037za700za7za7o2119za7,
		"_ object_slots slot user-info setter getter virtual-override virtual-num default-value bool read-only? type long index class-owner obj src bstring name symbol static final - foreign read-only inline-default default info id #z1 set get ",
		235);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_slotzd2defaultzd2valuezd2envzd2zzobject_slotsz00,
		BgL_bgl_za762slotza7d2defaul2120z00,
		BGl_z62slotzd2defaultzd2valuez62zzobject_slotsz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzobject_slotsz00));
		     ADD_ROOT((void *) (&BGl_slotzd2inlinezd2markz00zzobject_slotsz00));
		     ADD_ROOT((void *) (&BGl_slotz00zzobject_slotsz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long
		BgL_checksumz00_2339, char *BgL_fromz00_2340)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzobject_slotsz00))
				{
					BGl_requirezd2initializa7ationz75zzobject_slotsz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzobject_slotsz00();
					BGl_libraryzd2moduleszd2initz00zzobject_slotsz00();
					BGl_cnstzd2initzd2zzobject_slotsz00();
					BGl_importedzd2moduleszd2initz00zzobject_slotsz00();
					BGl_objectzd2initzd2zzobject_slotsz00();
					BGl_methodzd2initzd2zzobject_slotsz00();
					return BGl_toplevelzd2initzd2zzobject_slotsz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzobject_slotsz00(void)
	{
		{	/* Object/slots.scm 15 */
			BGl_modulezd2initializa7ationz75zz__paramz00(0L, "object_slots");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "object_slots");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"object_slots");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "object_slots");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"object_slots");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"object_slots");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "object_slots");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "object_slots");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "object_slots");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"object_slots");
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(0L,
				"object_slots");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "object_slots");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"object_slots");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzobject_slotsz00(void)
	{
		{	/* Object/slots.scm 15 */
			{	/* Object/slots.scm 15 */
				obj_t BgL_cportz00_2250;

				{	/* Object/slots.scm 15 */
					obj_t BgL_stringz00_2257;

					BgL_stringz00_2257 = BGl_string2037z00zzobject_slotsz00;
					{	/* Object/slots.scm 15 */
						obj_t BgL_startz00_2258;

						BgL_startz00_2258 = BINT(0L);
						{	/* Object/slots.scm 15 */
							obj_t BgL_endz00_2259;

							BgL_endz00_2259 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2257)));
							{	/* Object/slots.scm 15 */

								BgL_cportz00_2250 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2257, BgL_startz00_2258, BgL_endz00_2259);
				}}}}
				{
					long BgL_iz00_2251;

					BgL_iz00_2251 = 31L;
				BgL_loopz00_2252:
					if ((BgL_iz00_2251 == -1L))
						{	/* Object/slots.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Object/slots.scm 15 */
							{	/* Object/slots.scm 15 */
								obj_t BgL_arg2038z00_2253;

								{	/* Object/slots.scm 15 */

									{	/* Object/slots.scm 15 */
										obj_t BgL_locationz00_2255;

										BgL_locationz00_2255 = BBOOL(((bool_t) 0));
										{	/* Object/slots.scm 15 */

											BgL_arg2038z00_2253 =
												BGl_readz00zz__readerz00(BgL_cportz00_2250,
												BgL_locationz00_2255);
										}
									}
								}
								{	/* Object/slots.scm 15 */
									int BgL_tmpz00_2373;

									BgL_tmpz00_2373 = (int) (BgL_iz00_2251);
									CNST_TABLE_SET(BgL_tmpz00_2373, BgL_arg2038z00_2253);
							}}
							{	/* Object/slots.scm 15 */
								int BgL_auxz00_2256;

								BgL_auxz00_2256 = (int) ((BgL_iz00_2251 - 1L));
								{
									long BgL_iz00_2378;

									BgL_iz00_2378 = (long) (BgL_auxz00_2256);
									BgL_iz00_2251 = BgL_iz00_2378;
									goto BgL_loopz00_2252;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzobject_slotsz00(void)
	{
		{	/* Object/slots.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzobject_slotsz00(void)
	{
		{	/* Object/slots.scm 15 */
			BGl_slotzd2inlinezd2markz00zzobject_slotsz00 =
				MAKE_YOUNG_PAIR(BINT(3L), BINT(4L));
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzobject_slotsz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_797;

				BgL_headz00_797 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_798;
					obj_t BgL_tailz00_799;

					BgL_prevz00_798 = BgL_headz00_797;
					BgL_tailz00_799 = BgL_l1z00_1;
				BgL_loopz00_800:
					if (PAIRP(BgL_tailz00_799))
						{
							obj_t BgL_newzd2prevzd2_802;

							BgL_newzd2prevzd2_802 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_799), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_798, BgL_newzd2prevzd2_802);
							{
								obj_t BgL_tailz00_2391;
								obj_t BgL_prevz00_2390;

								BgL_prevz00_2390 = BgL_newzd2prevzd2_802;
								BgL_tailz00_2391 = CDR(BgL_tailz00_799);
								BgL_tailz00_799 = BgL_tailz00_2391;
								BgL_prevz00_798 = BgL_prevz00_2390;
								goto BgL_loopz00_800;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_797);
				}
			}
		}

	}



/* make-slot */
	BGL_EXPORTED_DEF BgL_slotz00_bglt BGl_makezd2slotzd2zzobject_slotsz00(obj_t
		BgL_id1080z00_3, obj_t BgL_name1081z00_4, obj_t BgL_src1082z00_5,
		obj_t BgL_classzd2owner1083zd2_6, long BgL_index1084z00_7,
		obj_t BgL_type1085z00_8, bool_t BgL_readzd2onlyzf31086z21_9,
		obj_t BgL_defaultzd2value1087zd2_10, obj_t BgL_virtualzd2num1088zd2_11,
		bool_t BgL_virtualzd2override1089zd2_12, obj_t BgL_getter1090z00_13,
		obj_t BgL_setter1091z00_14, obj_t BgL_userzd2info1092zd2_15)
	{
		{	/* Object/slots.sch 37 */
			{	/* Object/slots.sch 37 */
				BgL_slotz00_bglt BgL_new1091z00_2261;

				{	/* Object/slots.sch 37 */
					BgL_slotz00_bglt BgL_new1090z00_2262;

					BgL_new1090z00_2262 =
						((BgL_slotz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_slotz00_bgl))));
					{	/* Object/slots.sch 37 */
						long BgL_arg1187z00_2263;

						BgL_arg1187z00_2263 = BGL_CLASS_NUM(BGl_slotz00zzobject_slotsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1090z00_2262), BgL_arg1187z00_2263);
					}
					BgL_new1091z00_2261 = BgL_new1090z00_2262;
				}
				((((BgL_slotz00_bglt) COBJECT(BgL_new1091z00_2261))->BgL_idz00) =
					((obj_t) BgL_id1080z00_3), BUNSPEC);
				((((BgL_slotz00_bglt) COBJECT(BgL_new1091z00_2261))->BgL_namez00) =
					((obj_t) BgL_name1081z00_4), BUNSPEC);
				((((BgL_slotz00_bglt) COBJECT(BgL_new1091z00_2261))->BgL_srcz00) =
					((obj_t) BgL_src1082z00_5), BUNSPEC);
				((((BgL_slotz00_bglt) COBJECT(BgL_new1091z00_2261))->
						BgL_classzd2ownerzd2) =
					((obj_t) BgL_classzd2owner1083zd2_6), BUNSPEC);
				((((BgL_slotz00_bglt) COBJECT(BgL_new1091z00_2261))->BgL_indexz00) =
					((long) BgL_index1084z00_7), BUNSPEC);
				((((BgL_slotz00_bglt) COBJECT(BgL_new1091z00_2261))->BgL_typez00) =
					((obj_t) BgL_type1085z00_8), BUNSPEC);
				((((BgL_slotz00_bglt) COBJECT(BgL_new1091z00_2261))->
						BgL_readzd2onlyzf3z21) =
					((bool_t) BgL_readzd2onlyzf31086z21_9), BUNSPEC);
				((((BgL_slotz00_bglt) COBJECT(BgL_new1091z00_2261))->
						BgL_defaultzd2valuezd2) =
					((obj_t) BgL_defaultzd2value1087zd2_10), BUNSPEC);
				((((BgL_slotz00_bglt) COBJECT(BgL_new1091z00_2261))->
						BgL_virtualzd2numzd2) =
					((obj_t) BgL_virtualzd2num1088zd2_11), BUNSPEC);
				((((BgL_slotz00_bglt) COBJECT(BgL_new1091z00_2261))->
						BgL_virtualzd2overridezd2) =
					((bool_t) BgL_virtualzd2override1089zd2_12), BUNSPEC);
				((((BgL_slotz00_bglt) COBJECT(BgL_new1091z00_2261))->BgL_getterz00) =
					((obj_t) BgL_getter1090z00_13), BUNSPEC);
				((((BgL_slotz00_bglt) COBJECT(BgL_new1091z00_2261))->BgL_setterz00) =
					((obj_t) BgL_setter1091z00_14), BUNSPEC);
				((((BgL_slotz00_bglt) COBJECT(BgL_new1091z00_2261))->
						BgL_userzd2infozd2) = ((obj_t) BgL_userzd2info1092zd2_15), BUNSPEC);
				return BgL_new1091z00_2261;
			}
		}

	}



/* &make-slot */
	BgL_slotz00_bglt BGl_z62makezd2slotzb0zzobject_slotsz00(obj_t BgL_envz00_2038,
		obj_t BgL_id1080z00_2039, obj_t BgL_name1081z00_2040,
		obj_t BgL_src1082z00_2041, obj_t BgL_classzd2owner1083zd2_2042,
		obj_t BgL_index1084z00_2043, obj_t BgL_type1085z00_2044,
		obj_t BgL_readzd2onlyzf31086z21_2045, obj_t BgL_defaultzd2value1087zd2_2046,
		obj_t BgL_virtualzd2num1088zd2_2047,
		obj_t BgL_virtualzd2override1089zd2_2048, obj_t BgL_getter1090z00_2049,
		obj_t BgL_setter1091z00_2050, obj_t BgL_userzd2info1092zd2_2051)
	{
		{	/* Object/slots.sch 37 */
			return
				BGl_makezd2slotzd2zzobject_slotsz00(BgL_id1080z00_2039,
				BgL_name1081z00_2040, BgL_src1082z00_2041,
				BgL_classzd2owner1083zd2_2042, (long) CINT(BgL_index1084z00_2043),
				BgL_type1085z00_2044, CBOOL(BgL_readzd2onlyzf31086z21_2045),
				BgL_defaultzd2value1087zd2_2046, BgL_virtualzd2num1088zd2_2047,
				CBOOL(BgL_virtualzd2override1089zd2_2048), BgL_getter1090z00_2049,
				BgL_setter1091z00_2050, BgL_userzd2info1092zd2_2051);
		}

	}



/* slot? */
	BGL_EXPORTED_DEF bool_t BGl_slotzf3zf3zzobject_slotsz00(obj_t BgL_objz00_16)
	{
		{	/* Object/slots.sch 38 */
			{	/* Object/slots.sch 38 */
				obj_t BgL_classz00_2264;

				BgL_classz00_2264 = BGl_slotz00zzobject_slotsz00;
				if (BGL_OBJECTP(BgL_objz00_16))
					{	/* Object/slots.sch 38 */
						BgL_objectz00_bglt BgL_arg1807z00_2265;

						BgL_arg1807z00_2265 = (BgL_objectz00_bglt) (BgL_objz00_16);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Object/slots.sch 38 */
								long BgL_idxz00_2266;

								BgL_idxz00_2266 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2265);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_2266 + 1L)) == BgL_classz00_2264);
							}
						else
							{	/* Object/slots.sch 38 */
								bool_t BgL_res1975z00_2269;

								{	/* Object/slots.sch 38 */
									obj_t BgL_oclassz00_2270;

									{	/* Object/slots.sch 38 */
										obj_t BgL_arg1815z00_2271;
										long BgL_arg1816z00_2272;

										BgL_arg1815z00_2271 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Object/slots.sch 38 */
											long BgL_arg1817z00_2273;

											BgL_arg1817z00_2273 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2265);
											BgL_arg1816z00_2272 = (BgL_arg1817z00_2273 - OBJECT_TYPE);
										}
										BgL_oclassz00_2270 =
											VECTOR_REF(BgL_arg1815z00_2271, BgL_arg1816z00_2272);
									}
									{	/* Object/slots.sch 38 */
										bool_t BgL__ortest_1115z00_2274;

										BgL__ortest_1115z00_2274 =
											(BgL_classz00_2264 == BgL_oclassz00_2270);
										if (BgL__ortest_1115z00_2274)
											{	/* Object/slots.sch 38 */
												BgL_res1975z00_2269 = BgL__ortest_1115z00_2274;
											}
										else
											{	/* Object/slots.sch 38 */
												long BgL_odepthz00_2275;

												{	/* Object/slots.sch 38 */
													obj_t BgL_arg1804z00_2276;

													BgL_arg1804z00_2276 = (BgL_oclassz00_2270);
													BgL_odepthz00_2275 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_2276);
												}
												if ((1L < BgL_odepthz00_2275))
													{	/* Object/slots.sch 38 */
														obj_t BgL_arg1802z00_2277;

														{	/* Object/slots.sch 38 */
															obj_t BgL_arg1803z00_2278;

															BgL_arg1803z00_2278 = (BgL_oclassz00_2270);
															BgL_arg1802z00_2277 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2278,
																1L);
														}
														BgL_res1975z00_2269 =
															(BgL_arg1802z00_2277 == BgL_classz00_2264);
													}
												else
													{	/* Object/slots.sch 38 */
														BgL_res1975z00_2269 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1975z00_2269;
							}
					}
				else
					{	/* Object/slots.sch 38 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &slot? */
	obj_t BGl_z62slotzf3z91zzobject_slotsz00(obj_t BgL_envz00_2052,
		obj_t BgL_objz00_2053)
	{
		{	/* Object/slots.sch 38 */
			return BBOOL(BGl_slotzf3zf3zzobject_slotsz00(BgL_objz00_2053));
		}

	}



/* slot-nil */
	BGL_EXPORTED_DEF BgL_slotz00_bglt BGl_slotzd2nilzd2zzobject_slotsz00(void)
	{
		{	/* Object/slots.sch 39 */
			{	/* Object/slots.sch 39 */
				obj_t BgL_classz00_1567;

				BgL_classz00_1567 = BGl_slotz00zzobject_slotsz00;
				{	/* Object/slots.sch 39 */
					obj_t BgL__ortest_1117z00_1568;

					BgL__ortest_1117z00_1568 = BGL_CLASS_NIL(BgL_classz00_1567);
					if (CBOOL(BgL__ortest_1117z00_1568))
						{	/* Object/slots.sch 39 */
							return ((BgL_slotz00_bglt) BgL__ortest_1117z00_1568);
						}
					else
						{	/* Object/slots.sch 39 */
							return
								((BgL_slotz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1567));
						}
				}
			}
		}

	}



/* &slot-nil */
	BgL_slotz00_bglt BGl_z62slotzd2nilzb0zzobject_slotsz00(obj_t BgL_envz00_2054)
	{
		{	/* Object/slots.sch 39 */
			return BGl_slotzd2nilzd2zzobject_slotsz00();
		}

	}



/* slot-user-info */
	BGL_EXPORTED_DEF obj_t
		BGl_slotzd2userzd2infoz00zzobject_slotsz00(BgL_slotz00_bglt BgL_oz00_17)
	{
		{	/* Object/slots.sch 40 */
			return (((BgL_slotz00_bglt) COBJECT(BgL_oz00_17))->BgL_userzd2infozd2);
		}

	}



/* &slot-user-info */
	obj_t BGl_z62slotzd2userzd2infoz62zzobject_slotsz00(obj_t BgL_envz00_2055,
		obj_t BgL_oz00_2056)
	{
		{	/* Object/slots.sch 40 */
			return
				BGl_slotzd2userzd2infoz00zzobject_slotsz00(
				((BgL_slotz00_bglt) BgL_oz00_2056));
		}

	}



/* slot-setter */
	BGL_EXPORTED_DEF obj_t BGl_slotzd2setterzd2zzobject_slotsz00(BgL_slotz00_bglt
		BgL_oz00_20)
	{
		{	/* Object/slots.sch 42 */
			return (((BgL_slotz00_bglt) COBJECT(BgL_oz00_20))->BgL_setterz00);
		}

	}



/* &slot-setter */
	obj_t BGl_z62slotzd2setterzb0zzobject_slotsz00(obj_t BgL_envz00_2057,
		obj_t BgL_oz00_2058)
	{
		{	/* Object/slots.sch 42 */
			return
				BGl_slotzd2setterzd2zzobject_slotsz00(
				((BgL_slotz00_bglt) BgL_oz00_2058));
		}

	}



/* slot-setter-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_slotzd2setterzd2setz12z12zzobject_slotsz00(BgL_slotz00_bglt BgL_oz00_21,
		obj_t BgL_vz00_22)
	{
		{	/* Object/slots.sch 43 */
			return
				((((BgL_slotz00_bglt) COBJECT(BgL_oz00_21))->BgL_setterz00) =
				((obj_t) BgL_vz00_22), BUNSPEC);
		}

	}



/* &slot-setter-set! */
	obj_t BGl_z62slotzd2setterzd2setz12z70zzobject_slotsz00(obj_t BgL_envz00_2059,
		obj_t BgL_oz00_2060, obj_t BgL_vz00_2061)
	{
		{	/* Object/slots.sch 43 */
			return
				BGl_slotzd2setterzd2setz12z12zzobject_slotsz00(
				((BgL_slotz00_bglt) BgL_oz00_2060), BgL_vz00_2061);
		}

	}



/* slot-getter */
	BGL_EXPORTED_DEF obj_t BGl_slotzd2getterzd2zzobject_slotsz00(BgL_slotz00_bglt
		BgL_oz00_23)
	{
		{	/* Object/slots.sch 44 */
			return (((BgL_slotz00_bglt) COBJECT(BgL_oz00_23))->BgL_getterz00);
		}

	}



/* &slot-getter */
	obj_t BGl_z62slotzd2getterzb0zzobject_slotsz00(obj_t BgL_envz00_2062,
		obj_t BgL_oz00_2063)
	{
		{	/* Object/slots.sch 44 */
			return
				BGl_slotzd2getterzd2zzobject_slotsz00(
				((BgL_slotz00_bglt) BgL_oz00_2063));
		}

	}



/* slot-getter-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_slotzd2getterzd2setz12z12zzobject_slotsz00(BgL_slotz00_bglt BgL_oz00_24,
		obj_t BgL_vz00_25)
	{
		{	/* Object/slots.sch 45 */
			return
				((((BgL_slotz00_bglt) COBJECT(BgL_oz00_24))->BgL_getterz00) =
				((obj_t) BgL_vz00_25), BUNSPEC);
		}

	}



/* &slot-getter-set! */
	obj_t BGl_z62slotzd2getterzd2setz12z70zzobject_slotsz00(obj_t BgL_envz00_2064,
		obj_t BgL_oz00_2065, obj_t BgL_vz00_2066)
	{
		{	/* Object/slots.sch 45 */
			return
				BGl_slotzd2getterzd2setz12z12zzobject_slotsz00(
				((BgL_slotz00_bglt) BgL_oz00_2065), BgL_vz00_2066);
		}

	}



/* slot-virtual-override */
	BGL_EXPORTED_DEF bool_t
		BGl_slotzd2virtualzd2overridez00zzobject_slotsz00(BgL_slotz00_bglt
		BgL_oz00_26)
	{
		{	/* Object/slots.sch 46 */
			return
				(((BgL_slotz00_bglt) COBJECT(BgL_oz00_26))->BgL_virtualzd2overridezd2);
		}

	}



/* &slot-virtual-override */
	obj_t BGl_z62slotzd2virtualzd2overridez62zzobject_slotsz00(obj_t
		BgL_envz00_2067, obj_t BgL_oz00_2068)
	{
		{	/* Object/slots.sch 46 */
			return
				BBOOL(BGl_slotzd2virtualzd2overridez00zzobject_slotsz00(
					((BgL_slotz00_bglt) BgL_oz00_2068)));
		}

	}



/* slot-virtual-num */
	BGL_EXPORTED_DEF obj_t
		BGl_slotzd2virtualzd2numz00zzobject_slotsz00(BgL_slotz00_bglt BgL_oz00_29)
	{
		{	/* Object/slots.sch 48 */
			return (((BgL_slotz00_bglt) COBJECT(BgL_oz00_29))->BgL_virtualzd2numzd2);
		}

	}



/* &slot-virtual-num */
	obj_t BGl_z62slotzd2virtualzd2numz62zzobject_slotsz00(obj_t BgL_envz00_2069,
		obj_t BgL_oz00_2070)
	{
		{	/* Object/slots.sch 48 */
			return
				BGl_slotzd2virtualzd2numz00zzobject_slotsz00(
				((BgL_slotz00_bglt) BgL_oz00_2070));
		}

	}



/* slot-virtual-num-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_slotzd2virtualzd2numzd2setz12zc0zzobject_slotsz00(BgL_slotz00_bglt
		BgL_oz00_30, obj_t BgL_vz00_31)
	{
		{	/* Object/slots.sch 49 */
			return
				((((BgL_slotz00_bglt) COBJECT(BgL_oz00_30))->BgL_virtualzd2numzd2) =
				((obj_t) BgL_vz00_31), BUNSPEC);
		}

	}



/* &slot-virtual-num-set! */
	obj_t BGl_z62slotzd2virtualzd2numzd2setz12za2zzobject_slotsz00(obj_t
		BgL_envz00_2071, obj_t BgL_oz00_2072, obj_t BgL_vz00_2073)
	{
		{	/* Object/slots.sch 49 */
			return
				BGl_slotzd2virtualzd2numzd2setz12zc0zzobject_slotsz00(
				((BgL_slotz00_bglt) BgL_oz00_2072), BgL_vz00_2073);
		}

	}



/* slot-default-value */
	BGL_EXPORTED_DEF obj_t
		BGl_slotzd2defaultzd2valuez00zzobject_slotsz00(BgL_slotz00_bglt BgL_oz00_32)
	{
		{	/* Object/slots.sch 50 */
			return
				(((BgL_slotz00_bglt) COBJECT(BgL_oz00_32))->BgL_defaultzd2valuezd2);
		}

	}



/* &slot-default-value */
	obj_t BGl_z62slotzd2defaultzd2valuez62zzobject_slotsz00(obj_t BgL_envz00_2074,
		obj_t BgL_oz00_2075)
	{
		{	/* Object/slots.sch 50 */
			return
				BGl_slotzd2defaultzd2valuez00zzobject_slotsz00(
				((BgL_slotz00_bglt) BgL_oz00_2075));
		}

	}



/* slot-read-only? */
	BGL_EXPORTED_DEF bool_t
		BGl_slotzd2readzd2onlyzf3zf3zzobject_slotsz00(BgL_slotz00_bglt BgL_oz00_35)
	{
		{	/* Object/slots.sch 52 */
			return (((BgL_slotz00_bglt) COBJECT(BgL_oz00_35))->BgL_readzd2onlyzf3z21);
		}

	}



/* &slot-read-only? */
	obj_t BGl_z62slotzd2readzd2onlyzf3z91zzobject_slotsz00(obj_t BgL_envz00_2076,
		obj_t BgL_oz00_2077)
	{
		{	/* Object/slots.sch 52 */
			return
				BBOOL(BGl_slotzd2readzd2onlyzf3zf3zzobject_slotsz00(
					((BgL_slotz00_bglt) BgL_oz00_2077)));
		}

	}



/* slot-type */
	BGL_EXPORTED_DEF obj_t BGl_slotzd2typezd2zzobject_slotsz00(BgL_slotz00_bglt
		BgL_oz00_38)
	{
		{	/* Object/slots.sch 54 */
			return (((BgL_slotz00_bglt) COBJECT(BgL_oz00_38))->BgL_typez00);
		}

	}



/* &slot-type */
	obj_t BGl_z62slotzd2typezb0zzobject_slotsz00(obj_t BgL_envz00_2078,
		obj_t BgL_oz00_2079)
	{
		{	/* Object/slots.sch 54 */
			return
				BGl_slotzd2typezd2zzobject_slotsz00(((BgL_slotz00_bglt) BgL_oz00_2079));
		}

	}



/* slot-index */
	BGL_EXPORTED_DEF long BGl_slotzd2indexzd2zzobject_slotsz00(BgL_slotz00_bglt
		BgL_oz00_41)
	{
		{	/* Object/slots.sch 56 */
			return (((BgL_slotz00_bglt) COBJECT(BgL_oz00_41))->BgL_indexz00);
		}

	}



/* &slot-index */
	obj_t BGl_z62slotzd2indexzb0zzobject_slotsz00(obj_t BgL_envz00_2080,
		obj_t BgL_oz00_2081)
	{
		{	/* Object/slots.sch 56 */
			return
				BINT(BGl_slotzd2indexzd2zzobject_slotsz00(
					((BgL_slotz00_bglt) BgL_oz00_2081)));
		}

	}



/* slot-class-owner */
	BGL_EXPORTED_DEF obj_t
		BGl_slotzd2classzd2ownerz00zzobject_slotsz00(BgL_slotz00_bglt BgL_oz00_44)
	{
		{	/* Object/slots.sch 58 */
			return (((BgL_slotz00_bglt) COBJECT(BgL_oz00_44))->BgL_classzd2ownerzd2);
		}

	}



/* &slot-class-owner */
	obj_t BGl_z62slotzd2classzd2ownerz62zzobject_slotsz00(obj_t BgL_envz00_2082,
		obj_t BgL_oz00_2083)
	{
		{	/* Object/slots.sch 58 */
			return
				BGl_slotzd2classzd2ownerz00zzobject_slotsz00(
				((BgL_slotz00_bglt) BgL_oz00_2083));
		}

	}



/* slot-src */
	BGL_EXPORTED_DEF obj_t BGl_slotzd2srczd2zzobject_slotsz00(BgL_slotz00_bglt
		BgL_oz00_47)
	{
		{	/* Object/slots.sch 60 */
			return (((BgL_slotz00_bglt) COBJECT(BgL_oz00_47))->BgL_srcz00);
		}

	}



/* &slot-src */
	obj_t BGl_z62slotzd2srczb0zzobject_slotsz00(obj_t BgL_envz00_2084,
		obj_t BgL_oz00_2085)
	{
		{	/* Object/slots.sch 60 */
			return
				BGl_slotzd2srczd2zzobject_slotsz00(((BgL_slotz00_bglt) BgL_oz00_2085));
		}

	}



/* slot-name */
	BGL_EXPORTED_DEF obj_t BGl_slotzd2namezd2zzobject_slotsz00(BgL_slotz00_bglt
		BgL_oz00_50)
	{
		{	/* Object/slots.sch 62 */
			return (((BgL_slotz00_bglt) COBJECT(BgL_oz00_50))->BgL_namez00);
		}

	}



/* &slot-name */
	obj_t BGl_z62slotzd2namezb0zzobject_slotsz00(obj_t BgL_envz00_2086,
		obj_t BgL_oz00_2087)
	{
		{	/* Object/slots.sch 62 */
			return
				BGl_slotzd2namezd2zzobject_slotsz00(((BgL_slotz00_bglt) BgL_oz00_2087));
		}

	}



/* slot-id */
	BGL_EXPORTED_DEF obj_t BGl_slotzd2idzd2zzobject_slotsz00(BgL_slotz00_bglt
		BgL_oz00_53)
	{
		{	/* Object/slots.sch 64 */
			return (((BgL_slotz00_bglt) COBJECT(BgL_oz00_53))->BgL_idz00);
		}

	}



/* &slot-id */
	obj_t BGl_z62slotzd2idzb0zzobject_slotsz00(obj_t BgL_envz00_2088,
		obj_t BgL_oz00_2089)
	{
		{	/* Object/slots.sch 64 */
			return
				BGl_slotzd2idzd2zzobject_slotsz00(((BgL_slotz00_bglt) BgL_oz00_2089));
		}

	}



/* slot-default? */
	BGL_EXPORTED_DEF bool_t
		BGl_slotzd2defaultzf3z21zzobject_slotsz00(BgL_slotz00_bglt BgL_slotz00_57)
	{
		{	/* Object/slots.scm 96 */
			{	/* Object/slots.scm 97 */
				bool_t BgL_test2129z00_2497;

				{	/* Object/slots.scm 97 */
					obj_t BgL_arg1191z00_811;
					obj_t BgL_arg1193z00_812;

					BgL_arg1191z00_811 =
						(((BgL_slotz00_bglt) COBJECT(BgL_slotz00_57))->
						BgL_defaultzd2valuezd2);
					BgL_arg1193z00_812 = MAKE_YOUNG_PAIR(BINT(1L), BINT(2L));
					BgL_test2129z00_2497 =
						BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BgL_arg1191z00_811,
						BgL_arg1193z00_812);
				}
				if (BgL_test2129z00_2497)
					{	/* Object/slots.scm 97 */
						return ((bool_t) 0);
					}
				else
					{	/* Object/slots.scm 97 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* &slot-default? */
	obj_t BGl_z62slotzd2defaultzf3z43zzobject_slotsz00(obj_t BgL_envz00_2090,
		obj_t BgL_slotz00_2091)
	{
		{	/* Object/slots.scm 96 */
			return
				BBOOL(BGl_slotzd2defaultzf3z21zzobject_slotsz00(
					((BgL_slotz00_bglt) BgL_slotz00_2091)));
		}

	}



/* slot-virtual? */
	BGL_EXPORTED_DEF bool_t
		BGl_slotzd2virtualzf3z21zzobject_slotsz00(BgL_slotz00_bglt BgL_slotz00_58)
	{
		{	/* Object/slots.scm 102 */
			return
				(
				(long) CINT(
					(((BgL_slotz00_bglt) COBJECT(BgL_slotz00_58))->
						BgL_virtualzd2numzd2)) >= 0L);
		}

	}



/* &slot-virtual? */
	obj_t BGl_z62slotzd2virtualzf3z43zzobject_slotsz00(obj_t BgL_envz00_2092,
		obj_t BgL_slotz00_2093)
	{
		{	/* Object/slots.scm 102 */
			return
				BBOOL(BGl_slotzd2virtualzf3z21zzobject_slotsz00(
					((BgL_slotz00_bglt) BgL_slotz00_2093)));
		}

	}



/* slot-inline-default? */
	BGL_EXPORTED_DEF bool_t
		BGl_slotzd2inlinezd2defaultzf3zf3zzobject_slotsz00(BgL_slotz00_bglt
		BgL_slotz00_59)
	{
		{	/* Object/slots.scm 108 */
			{	/* Object/slots.scm 109 */
				bool_t BgL_test2130z00_2512;

				{	/* Object/slots.scm 109 */
					obj_t BgL_tmpz00_2513;

					BgL_tmpz00_2513 =
						(((BgL_slotz00_bglt) COBJECT(BgL_slotz00_59))->
						BgL_defaultzd2valuezd2);
					BgL_test2130z00_2512 = PAIRP(BgL_tmpz00_2513);
				}
				if (BgL_test2130z00_2512)
					{	/* Object/slots.scm 110 */
						obj_t BgL_arg1196z00_815;

						BgL_arg1196z00_815 =
							CAR(
							(((BgL_slotz00_bglt) COBJECT(BgL_slotz00_59))->
								BgL_defaultzd2valuezd2));
						return (BgL_arg1196z00_815 ==
							BGl_slotzd2inlinezd2markz00zzobject_slotsz00);
					}
				else
					{	/* Object/slots.scm 109 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &slot-inline-default? */
	obj_t BGl_z62slotzd2inlinezd2defaultzf3z91zzobject_slotsz00(obj_t
		BgL_envz00_2094, obj_t BgL_slotz00_2095)
	{
		{	/* Object/slots.scm 108 */
			return
				BBOOL(BGl_slotzd2inlinezd2defaultzf3zf3zzobject_slotsz00(
					((BgL_slotz00_bglt) BgL_slotz00_2095)));
		}

	}



/* slot-default-inline-value */
	BGL_EXPORTED_DEF obj_t
		BGl_slotzd2defaultzd2inlinezd2valuezd2zzobject_slotsz00(BgL_slotz00_bglt
		BgL_slotz00_60)
	{
		{	/* Object/slots.scm 115 */
			return
				CDR(
				(((BgL_slotz00_bglt) COBJECT(BgL_slotz00_60))->BgL_defaultzd2valuezd2));
		}

	}



/* &slot-default-inline-value */
	obj_t BGl_z62slotzd2defaultzd2inlinezd2valuezb0zzobject_slotsz00(obj_t
		BgL_envz00_2096, obj_t BgL_slotz00_2097)
	{
		{	/* Object/slots.scm 115 */
			return
				BGl_slotzd2defaultzd2inlinezd2valuezd2zzobject_slotsz00(
				((BgL_slotz00_bglt) BgL_slotz00_2097));
		}

	}



/* ensure-type-defined! */
	obj_t BGl_ensurezd2typezd2definedz12z12zzobject_slotsz00(BgL_typez00_bglt
		BgL_typez00_61, obj_t BgL_srcz00_62)
	{
		{	/* Object/slots.scm 121 */
			if ((((BgL_typez00_bglt) COBJECT(BgL_typez00_61))->BgL_initzf3zf3))
				{	/* Object/slots.scm 122 */
					return BFALSE;
				}
			else
				{	/* Object/slots.scm 122 */
					return
						BGl_userzd2errorzd2zztools_errorz00
						(BGl_string1984z00zzobject_slotsz00,
						(((BgL_typez00_bglt) COBJECT(BgL_typez00_61))->BgL_idz00),
						BgL_srcz00_62, BNIL);
				}
		}

	}



/* make-class-slots */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2classzd2slotsz00zzobject_slotsz00(BgL_typez00_bglt
		BgL_classz00_63, obj_t BgL_clausesz00_64, obj_t BgL_superz00_65,
		int BgL_vnumz00_66, obj_t BgL_srcz00_67)
	{
		{	/* Object/slots.scm 128 */
			{
				obj_t BgL_attrz00_974;
				obj_t BgL_nslotz00_1056;
				obj_t BgL_sslotsz00_1057;
				BgL_typez00_bglt BgL_classz00_1058;
				obj_t BgL_nslotsz00_1085;
				obj_t BgL_sslotsz00_1086;
				BgL_typez00_bglt BgL_classz00_1087;
				obj_t BgL_slotzd2idzd2_1112;
				long BgL_indexz00_1113;

				{	/* Object/slots.scm 254 */
					obj_t BgL_sslotsz00_831;

					{	/* Object/slots.scm 255 */
						bool_t BgL_test2132z00_2530;

						{	/* Object/slots.scm 255 */
							obj_t BgL_classz00_1653;

							BgL_classz00_1653 = BGl_typez00zztype_typez00;
							if (BGL_OBJECTP(BgL_superz00_65))
								{	/* Object/slots.scm 255 */
									BgL_objectz00_bglt BgL_arg1807z00_1655;

									BgL_arg1807z00_1655 = (BgL_objectz00_bglt) (BgL_superz00_65);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Object/slots.scm 255 */
											long BgL_idxz00_1661;

											BgL_idxz00_1661 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1655);
											BgL_test2132z00_2530 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_1661 + 1L)) == BgL_classz00_1653);
										}
									else
										{	/* Object/slots.scm 255 */
											bool_t BgL_res1976z00_1686;

											{	/* Object/slots.scm 255 */
												obj_t BgL_oclassz00_1669;

												{	/* Object/slots.scm 255 */
													obj_t BgL_arg1815z00_1677;
													long BgL_arg1816z00_1678;

													BgL_arg1815z00_1677 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Object/slots.scm 255 */
														long BgL_arg1817z00_1679;

														BgL_arg1817z00_1679 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1655);
														BgL_arg1816z00_1678 =
															(BgL_arg1817z00_1679 - OBJECT_TYPE);
													}
													BgL_oclassz00_1669 =
														VECTOR_REF(BgL_arg1815z00_1677,
														BgL_arg1816z00_1678);
												}
												{	/* Object/slots.scm 255 */
													bool_t BgL__ortest_1115z00_1670;

													BgL__ortest_1115z00_1670 =
														(BgL_classz00_1653 == BgL_oclassz00_1669);
													if (BgL__ortest_1115z00_1670)
														{	/* Object/slots.scm 255 */
															BgL_res1976z00_1686 = BgL__ortest_1115z00_1670;
														}
													else
														{	/* Object/slots.scm 255 */
															long BgL_odepthz00_1671;

															{	/* Object/slots.scm 255 */
																obj_t BgL_arg1804z00_1672;

																BgL_arg1804z00_1672 = (BgL_oclassz00_1669);
																BgL_odepthz00_1671 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_1672);
															}
															if ((1L < BgL_odepthz00_1671))
																{	/* Object/slots.scm 255 */
																	obj_t BgL_arg1802z00_1674;

																	{	/* Object/slots.scm 255 */
																		obj_t BgL_arg1803z00_1675;

																		BgL_arg1803z00_1675 = (BgL_oclassz00_1669);
																		BgL_arg1802z00_1674 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_1675, 1L);
																	}
																	BgL_res1976z00_1686 =
																		(BgL_arg1802z00_1674 == BgL_classz00_1653);
																}
															else
																{	/* Object/slots.scm 255 */
																	BgL_res1976z00_1686 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2132z00_2530 = BgL_res1976z00_1686;
										}
								}
							else
								{	/* Object/slots.scm 255 */
									BgL_test2132z00_2530 = ((bool_t) 0);
								}
						}
						if (BgL_test2132z00_2530)
							{	/* Object/slots.scm 257 */
								bool_t BgL_test2137z00_2553;

								{	/* Object/slots.scm 257 */
									obj_t BgL_classz00_1687;

									BgL_classz00_1687 = BGl_tclassz00zzobject_classz00;
									if (BGL_OBJECTP(BgL_superz00_65))
										{	/* Object/slots.scm 257 */
											BgL_objectz00_bglt BgL_arg1807z00_1689;

											BgL_arg1807z00_1689 =
												(BgL_objectz00_bglt) (BgL_superz00_65);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Object/slots.scm 257 */
													long BgL_idxz00_1695;

													BgL_idxz00_1695 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1689);
													BgL_test2137z00_2553 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_1695 + 2L)) == BgL_classz00_1687);
												}
											else
												{	/* Object/slots.scm 257 */
													bool_t BgL_res1977z00_1720;

													{	/* Object/slots.scm 257 */
														obj_t BgL_oclassz00_1703;

														{	/* Object/slots.scm 257 */
															obj_t BgL_arg1815z00_1711;
															long BgL_arg1816z00_1712;

															BgL_arg1815z00_1711 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Object/slots.scm 257 */
																long BgL_arg1817z00_1713;

																BgL_arg1817z00_1713 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1689);
																BgL_arg1816z00_1712 =
																	(BgL_arg1817z00_1713 - OBJECT_TYPE);
															}
															BgL_oclassz00_1703 =
																VECTOR_REF(BgL_arg1815z00_1711,
																BgL_arg1816z00_1712);
														}
														{	/* Object/slots.scm 257 */
															bool_t BgL__ortest_1115z00_1704;

															BgL__ortest_1115z00_1704 =
																(BgL_classz00_1687 == BgL_oclassz00_1703);
															if (BgL__ortest_1115z00_1704)
																{	/* Object/slots.scm 257 */
																	BgL_res1977z00_1720 =
																		BgL__ortest_1115z00_1704;
																}
															else
																{	/* Object/slots.scm 257 */
																	long BgL_odepthz00_1705;

																	{	/* Object/slots.scm 257 */
																		obj_t BgL_arg1804z00_1706;

																		BgL_arg1804z00_1706 = (BgL_oclassz00_1703);
																		BgL_odepthz00_1705 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_1706);
																	}
																	if ((2L < BgL_odepthz00_1705))
																		{	/* Object/slots.scm 257 */
																			obj_t BgL_arg1802z00_1708;

																			{	/* Object/slots.scm 257 */
																				obj_t BgL_arg1803z00_1709;

																				BgL_arg1803z00_1709 =
																					(BgL_oclassz00_1703);
																				BgL_arg1802z00_1708 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_1709, 2L);
																			}
																			BgL_res1977z00_1720 =
																				(BgL_arg1802z00_1708 ==
																				BgL_classz00_1687);
																		}
																	else
																		{	/* Object/slots.scm 257 */
																			BgL_res1977z00_1720 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2137z00_2553 = BgL_res1977z00_1720;
												}
										}
									else
										{	/* Object/slots.scm 257 */
											BgL_test2137z00_2553 = ((bool_t) 0);
										}
								}
								if (BgL_test2137z00_2553)
									{
										BgL_tclassz00_bglt BgL_auxz00_2576;

										{
											obj_t BgL_auxz00_2577;

											{	/* Object/slots.scm 260 */
												BgL_objectz00_bglt BgL_tmpz00_2578;

												BgL_tmpz00_2578 =
													((BgL_objectz00_bglt)
													((BgL_typez00_bglt) BgL_superz00_65));
												BgL_auxz00_2577 = BGL_OBJECT_WIDENING(BgL_tmpz00_2578);
											}
											BgL_auxz00_2576 = ((BgL_tclassz00_bglt) BgL_auxz00_2577);
										}
										BgL_sslotsz00_831 =
											(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_2576))->
											BgL_slotsz00);
									}
								else
									{	/* Object/slots.scm 257 */
										BgL_sslotsz00_831 = BNIL;
									}
							}
						else
							{	/* Object/slots.scm 255 */
								BgL_sslotsz00_831 = BNIL;
							}
					}
					{	/* Object/slots.scm 261 */
						long BgL_g1099z00_833;

						BgL_g1099z00_833 = bgl_list_length(BgL_sslotsz00_831);
						{
							obj_t BgL_clausesz00_835;
							obj_t BgL_nslotsz00_836;
							obj_t BgL_sslotsz00_837;
							obj_t BgL_vnumz00_838;
							long BgL_indexz00_839;

							BgL_clausesz00_835 = BgL_clausesz00_64;
							BgL_nslotsz00_836 = BNIL;
							BgL_sslotsz00_837 = BgL_sslotsz00_831;
							BgL_vnumz00_838 = BINT(BgL_vnumz00_66);
							BgL_indexz00_839 = BgL_g1099z00_833;
						BgL_zc3z04anonymousza31203ze3z87_840:
							if (NULLP(BgL_clausesz00_835))
								{	/* Object/slots.scm 266 */
									BgL_nslotsz00_1085 = BgL_nslotsz00_836;
									BgL_sslotsz00_1086 = BgL_sslotsz00_837;
									BgL_classz00_1087 = BgL_classz00_63;
									{
										obj_t BgL_l1164z00_1090;

										BgL_l1164z00_1090 = BgL_nslotsz00_1085;
									BgL_zc3z04anonymousza31618ze3z87_1091:
										if (PAIRP(BgL_l1164z00_1090))
											{	/* Object/slots.scm 228 */
												BgL_nslotz00_1056 = CAR(BgL_l1164z00_1090);
												BgL_sslotsz00_1057 = BgL_sslotsz00_1086;
												BgL_classz00_1058 = BgL_classz00_1087;
												if (
													((long) CINT(
															(((BgL_slotz00_bglt) COBJECT(
																		((BgL_slotz00_bglt) BgL_nslotz00_1056)))->
																BgL_virtualzd2numzd2)) >= 0L))
													{	/* Object/slots.scm 219 */
														BFALSE;
													}
												else
													{	/* Object/slots.scm 220 */
														obj_t BgL_idz00_1061;

														BgL_idz00_1061 =
															(((BgL_slotz00_bglt) COBJECT(
																	((BgL_slotz00_bglt) BgL_nslotz00_1056)))->
															BgL_idz00);
														{	/* Object/slots.scm 221 */
															bool_t BgL_test2145z00_2596;

															{
																obj_t BgL_l1161z00_1077;

																BgL_l1161z00_1077 = BgL_sslotsz00_1057;
															BgL_zc3z04anonymousza31614ze3z87_1078:
																if (NULLP(BgL_l1161z00_1077))
																	{	/* Object/slots.scm 221 */
																		BgL_test2145z00_2596 = ((bool_t) 0);
																	}
																else
																	{	/* Object/slots.scm 221 */
																		bool_t BgL__ortest_1163z00_1080;

																		BgL__ortest_1163z00_1080 =
																			(
																			(((BgL_slotz00_bglt) COBJECT(
																						((BgL_slotz00_bglt)
																							CAR(
																								((obj_t)
																									BgL_l1161z00_1077)))))->
																				BgL_idz00) == BgL_idz00_1061);
																		if (BgL__ortest_1163z00_1080)
																			{	/* Object/slots.scm 221 */
																				BgL_test2145z00_2596 =
																					BgL__ortest_1163z00_1080;
																			}
																		else
																			{
																				obj_t BgL_l1161z00_2605;

																				BgL_l1161z00_2605 =
																					CDR(((obj_t) BgL_l1161z00_1077));
																				BgL_l1161z00_1077 = BgL_l1161z00_2605;
																				goto
																					BgL_zc3z04anonymousza31614ze3z87_1078;
																			}
																	}
															}
															if (BgL_test2145z00_2596)
																{	/* Object/slots.scm 222 */
																	obj_t BgL_arg1609z00_1072;
																	obj_t BgL_arg1611z00_1073;

																	{	/* Object/slots.scm 222 */
																		obj_t BgL_arg1613z00_1075;

																		BgL_arg1613z00_1075 =
																			(((BgL_slotz00_bglt) COBJECT(
																					((BgL_slotz00_bglt)
																						BgL_nslotz00_1056)))->BgL_srcz00);
																		BgL_arg1609z00_1072 =
																			BGl_findzd2locationzd2zztools_locationz00
																			(BgL_arg1613z00_1075);
																	}
																	BgL_arg1611z00_1073 =
																		(((BgL_typez00_bglt) COBJECT(
																				((BgL_typez00_bglt)
																					BgL_classz00_1058)))->BgL_idz00);
																	BGl_userzd2errorzf2locationz20zztools_errorz00
																		(BgL_arg1609z00_1072, BgL_arg1611z00_1073,
																		BGl_string1985z00zzobject_slotsz00,
																		BgL_idz00_1061, BNIL);
																}
															else
																{	/* Object/slots.scm 221 */
																	BFALSE;
																}
														}
													}
												{
													obj_t BgL_l1164z00_2615;

													BgL_l1164z00_2615 = CDR(BgL_l1164z00_1090);
													BgL_l1164z00_1090 = BgL_l1164z00_2615;
													goto BgL_zc3z04anonymousza31618ze3z87_1091;
												}
											}
										else
											{	/* Object/slots.scm 228 */
												((bool_t) 1);
											}
									}
									return
										BGl_appendzd221011zd2zzobject_slotsz00(BgL_sslotsz00_837,
										bgl_reverse(BgL_nslotsz00_836));
								}
							else
								{	/* Object/slots.scm 271 */
									obj_t BgL_sz00_843;

									BgL_sz00_843 = CAR(((obj_t) BgL_clausesz00_835));
									{
										obj_t BgL_idz00_844;
										obj_t BgL_attrz00_845;
										obj_t BgL_idz00_847;

										if (PAIRP(BgL_sz00_843))
											{	/* Object/slots.scm 272 */
												obj_t BgL_carzd2431zd2_852;

												BgL_carzd2431zd2_852 = CAR(((obj_t) BgL_sz00_843));
												if (PAIRP(BgL_carzd2431zd2_852))
													{	/* Object/slots.scm 272 */
														obj_t BgL_cdrzd2435zd2_854;

														BgL_cdrzd2435zd2_854 = CDR(BgL_carzd2431zd2_852);
														if (
															(CAR(BgL_carzd2431zd2_852) == CNST_TABLE_REF(3)))
															{	/* Object/slots.scm 272 */
																if (PAIRP(BgL_cdrzd2435zd2_854))
																	{	/* Object/slots.scm 272 */
																		if (NULLP(CDR(BgL_cdrzd2435zd2_854)))
																			{	/* Object/slots.scm 272 */
																				obj_t BgL_arg1215z00_860;
																				obj_t BgL_arg1216z00_861;

																				BgL_arg1215z00_860 =
																					CAR(BgL_cdrzd2435zd2_854);
																				BgL_arg1216z00_861 =
																					CDR(((obj_t) BgL_sz00_843));
																				BgL_idz00_844 = BgL_arg1215z00_860;
																				BgL_attrz00_845 = BgL_arg1216z00_861;
																				{	/* Object/slots.scm 274 */
																					obj_t BgL_vgetz00_873;

																					BgL_attrz00_974 = BgL_attrz00_845;
																					{
																						obj_t BgL_attrz00_977;
																						obj_t BgL_getz00_978;
																						obj_t BgL_setz00_979;

																						BgL_attrz00_977 = BgL_attrz00_974;
																						BgL_getz00_978 = BFALSE;
																						BgL_setz00_979 = BFALSE;
																					BgL_zc3z04anonymousza31377ze3z87_980:
																						if (PAIRP(BgL_attrz00_977))
																							{

																								{	/* Object/slots.scm 159 */
																									obj_t BgL_ezd2399zd2_987;

																									BgL_ezd2399zd2_987 =
																										CAR(BgL_attrz00_977);
																									if (PAIRP(BgL_ezd2399zd2_987))
																										{	/* Object/slots.scm 159 */
																											obj_t
																												BgL_cdrzd2404zd2_989;
																											BgL_cdrzd2404zd2_989 =
																												CDR(BgL_ezd2399zd2_987);
																											if ((CAR
																													(BgL_ezd2399zd2_987)
																													== CNST_TABLE_REF(0)))
																												{	/* Object/slots.scm 159 */
																													if (PAIRP
																														(BgL_cdrzd2404zd2_989))
																														{	/* Object/slots.scm 159 */
																															if (NULLP(CDR
																																	(BgL_cdrzd2404zd2_989)))
																																{
																																	obj_t
																																		BgL_getz00_2657;
																																	obj_t
																																		BgL_attrz00_2655;
																																	BgL_attrz00_2655
																																		=
																																		CDR
																																		(BgL_attrz00_977);
																																	BgL_getz00_2657
																																		=
																																		CAR
																																		(BgL_cdrzd2404zd2_989);
																																	BgL_getz00_978
																																		=
																																		BgL_getz00_2657;
																																	BgL_attrz00_977
																																		=
																																		BgL_attrz00_2655;
																																	goto
																																		BgL_zc3z04anonymousza31377ze3z87_980;
																																}
																															else
																																{	/* Object/slots.scm 159 */
																																BgL_tagzd2398zd2_986:
																																	{	/* Object/slots.scm 165 */
																																		obj_t
																																			BgL_arg1513z00_1009;
																																		BgL_arg1513z00_1009
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_attrz00_977));
																																		{
																																			obj_t
																																				BgL_attrz00_2661;
																																			BgL_attrz00_2661
																																				=
																																				BgL_arg1513z00_1009;
																																			BgL_attrz00_977
																																				=
																																				BgL_attrz00_2661;
																																			goto
																																				BgL_zc3z04anonymousza31377ze3z87_980;
																																		}
																																	}
																																}
																														}
																													else
																														{	/* Object/slots.scm 159 */
																															goto
																																BgL_tagzd2398zd2_986;
																														}
																												}
																											else
																												{	/* Object/slots.scm 159 */
																													if (
																														(CAR
																															(BgL_ezd2399zd2_987)
																															==
																															CNST_TABLE_REF
																															(1)))
																														{	/* Object/slots.scm 159 */
																															if (PAIRP
																																(BgL_cdrzd2404zd2_989))
																																{	/* Object/slots.scm 159 */
																																	if (NULLP(CDR
																																			(BgL_cdrzd2404zd2_989)))
																																		{
																																			obj_t
																																				BgL_setz00_2673;
																																			obj_t
																																				BgL_attrz00_2671;
																																			BgL_attrz00_2671
																																				=
																																				CDR
																																				(BgL_attrz00_977);
																																			BgL_setz00_2673
																																				=
																																				CAR
																																				(BgL_cdrzd2404zd2_989);
																																			BgL_setz00_979
																																				=
																																				BgL_setz00_2673;
																																			BgL_attrz00_977
																																				=
																																				BgL_attrz00_2671;
																																			goto
																																				BgL_zc3z04anonymousza31377ze3z87_980;
																																		}
																																	else
																																		{	/* Object/slots.scm 159 */
																																			goto
																																				BgL_tagzd2398zd2_986;
																																		}
																																}
																															else
																																{	/* Object/slots.scm 159 */
																																	goto
																																		BgL_tagzd2398zd2_986;
																																}
																														}
																													else
																														{	/* Object/slots.scm 159 */
																															goto
																																BgL_tagzd2398zd2_986;
																														}
																												}
																										}
																									else
																										{	/* Object/slots.scm 159 */
																											goto BgL_tagzd2398zd2_986;
																										}
																								}
																							}
																						else
																							{	/* Object/slots.scm 157 */
																								{	/* Object/slots.scm 158 */
																									int BgL_tmpz00_2675;

																									BgL_tmpz00_2675 = (int) (2L);
																									BGL_MVALUES_NUMBER_SET
																										(BgL_tmpz00_2675);
																								}
																								{	/* Object/slots.scm 158 */
																									int BgL_tmpz00_2678;

																									BgL_tmpz00_2678 = (int) (1L);
																									BGL_MVALUES_VAL_SET
																										(BgL_tmpz00_2678,
																										BgL_setz00_979);
																								}
																								BgL_vgetz00_873 =
																									BgL_getz00_978;
																					}}
																					{	/* Object/slots.scm 275 */
																						obj_t BgL_vsetz00_874;

																						{	/* Object/slots.scm 277 */
																							obj_t BgL_tmpz00_1724;

																							{	/* Object/slots.scm 277 */
																								int BgL_tmpz00_2681;

																								BgL_tmpz00_2681 = (int) (1L);
																								BgL_tmpz00_1724 =
																									BGL_MVALUES_VAL
																									(BgL_tmpz00_2681);
																							}
																							{	/* Object/slots.scm 277 */
																								int BgL_tmpz00_2684;

																								BgL_tmpz00_2684 = (int) (1L);
																								BGL_MVALUES_VAL_SET
																									(BgL_tmpz00_2684, BUNSPEC);
																							}
																							BgL_vsetz00_874 = BgL_tmpz00_1724;
																						}
																						{	/* Object/slots.scm 277 */
																							bool_t BgL_test2161z00_2687;

																							if (CBOOL(BgL_vgetz00_873))
																								{	/* Object/slots.scm 277 */
																									BgL_test2161z00_2687 =
																										CBOOL
																										(BGl_slotzd2memberzf3ze70zc6zzobject_slotsz00
																										(CAR(((obj_t)
																													BgL_idz00_844)),
																											BgL_nslotsz00_836));
																								}
																							else
																								{	/* Object/slots.scm 277 */
																									BgL_test2161z00_2687 =
																										((bool_t) 0);
																								}
																							if (BgL_test2161z00_2687)
																								{	/* Object/slots.scm 278 */
																									obj_t BgL_arg1230z00_877;
																									obj_t BgL_arg1231z00_878;
																									obj_t BgL_arg1232z00_879;

																									BgL_arg1230z00_877 =
																										BGl_findzd2locationzd2zztools_locationz00
																										(BgL_sz00_843);
																									BgL_arg1231z00_878 =
																										CAR(((obj_t)
																											BgL_idz00_844));
																									BgL_arg1232z00_879 =
																										CAR(((obj_t)
																											BgL_idz00_844));
																									return
																										BGl_userzd2errorzf2locationz20zztools_errorz00
																										(BgL_arg1230z00_877,
																										BgL_arg1231z00_878,
																										BGl_string1987z00zzobject_slotsz00,
																										BgL_arg1232z00_879, BNIL);
																								}
																							else
																								{	/* Object/slots.scm 282 */
																									obj_t BgL_g1101z00_881;

																									if (CBOOL(BgL_vgetz00_873))
																										{	/* Object/slots.scm 282 */
																											BgL_g1101z00_881 =
																												BGl_slotzd2memberzf3ze70zc6zzobject_slotsz00
																												(CAR(((obj_t)
																														BgL_idz00_844)),
																												BgL_sslotsz00_837);
																										}
																									else
																										{	/* Object/slots.scm 282 */
																											BgL_g1101z00_881 = BFALSE;
																										}
																									if (CBOOL(BgL_g1101z00_881))
																										{	/* Object/slots.scm 282 */
																											{	/* Object/slots.scm 285 */
																												bool_t
																													BgL_test2165z00_2707;
																												{	/* Object/slots.scm 285 */
																													bool_t
																														BgL_test2166z00_2708;
																													{	/* Object/slots.scm 285 */
																														int
																															BgL_a1166z00_900;
																														BgL_a1166z00_900 =
																															BGl_bigloozd2warningzd2zz__paramz00
																															();
																														{	/* Object/slots.scm 285 */

																															BgL_test2166z00_2708
																																=
																																((long)
																																(BgL_a1166z00_900)
																																>= 2L);
																													}}
																													if (BgL_test2166z00_2708)
																														{	/* Object/slots.scm 285 */
																															BgL_test2165z00_2707
																																=
																																CBOOL
																																(BGl_za2warningzd2overridenzd2slotsza2z00zzengine_paramz00);
																														}
																													else
																														{	/* Object/slots.scm 285 */
																															BgL_test2165z00_2707
																																= ((bool_t) 0);
																														}
																												}
																												if (BgL_test2165z00_2707)
																													{	/* Object/slots.scm 287 */
																														obj_t
																															BgL_arg1238z00_892;
																														obj_t
																															BgL_arg1239z00_893;
																														obj_t
																															BgL_arg1242z00_894;
																														obj_t
																															BgL_arg1244z00_895;
																														BgL_arg1238z00_892 =
																															BGl_findzd2locationzd2zztools_locationz00
																															(BgL_sz00_843);
																														BgL_arg1239z00_893 =
																															CAR(((obj_t)
																																BgL_idz00_844));
																														{	/* Object/slots.scm 290 */
																															obj_t
																																BgL_arg1248z00_896;
																															BgL_arg1248z00_896
																																=
																																(((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) (((BgL_slotz00_bglt) COBJECT(((BgL_slotz00_bglt) BgL_g1101z00_881)))->BgL_classzd2ownerzd2))))->BgL_idz00);
																															{	/* Object/slots.scm 289 */
																																obj_t
																																	BgL_list1249z00_897;
																																BgL_list1249z00_897
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1248z00_896,
																																	BNIL);
																																BgL_arg1242z00_894
																																	=
																																	BGl_formatz00zz__r4_output_6_10_3z00
																																	(BGl_string1988z00zzobject_slotsz00,
																																	BgL_list1249z00_897);
																															}
																														}
																														BgL_arg1244z00_895 =
																															CAR(
																															((obj_t)
																																BgL_idz00_844));
																														BGl_userzd2warningzf2locationz20zztools_errorz00
																															(BgL_arg1238z00_892,
																															BgL_arg1239z00_893,
																															BgL_arg1242z00_894,
																															BgL_arg1244z00_895);
																													}
																												else
																													{	/* Object/slots.scm 285 */
																														BFALSE;
																													}
																											}
																											{	/* Object/slots.scm 292 */
																												obj_t BgL_vnz00_903;
																												long BgL_viz00_904;

																												BgL_vnz00_903 =
																													(((BgL_slotz00_bglt)
																														COBJECT((
																																(BgL_slotz00_bglt)
																																BgL_g1101z00_881)))->
																													BgL_virtualzd2numzd2);
																												BgL_viz00_904 =
																													(((BgL_slotz00_bglt)
																														COBJECT((
																																(BgL_slotz00_bglt)
																																BgL_g1101z00_881)))->
																													BgL_indexz00);
																												{	/* Object/slots.scm 294 */
																													obj_t
																														BgL_arg1268z00_905;
																													obj_t
																														BgL_arg1272z00_906;
																													BgL_arg1268z00_905 =
																														CDR(((obj_t)
																															BgL_clausesz00_835));
																													BgL_arg1272z00_906 =
																														BGl_loopze70ze7zzobject_slotsz00
																														(BgL_g1101z00_881,
																														BGl_makezd2attributezd2slotze70ze7zzobject_slotsz00
																														(BgL_classz00_63,
																															BgL_sz00_843,
																															BgL_idz00_844,
																															BgL_attrz00_845,
																															BgL_vgetz00_873,
																															BgL_vsetz00_874,
																															BgL_vnz00_903,
																															((bool_t) 1),
																															BgL_viz00_904),
																														BgL_sslotsz00_837);
																													{
																														obj_t
																															BgL_sslotsz00_2734;
																														obj_t
																															BgL_clausesz00_2733;
																														BgL_clausesz00_2733
																															=
																															BgL_arg1268z00_905;
																														BgL_sslotsz00_2734 =
																															BgL_arg1272z00_906;
																														BgL_sslotsz00_837 =
																															BgL_sslotsz00_2734;
																														BgL_clausesz00_835 =
																															BgL_clausesz00_2733;
																														goto
																															BgL_zc3z04anonymousza31203ze3z87_840;
																													}
																												}
																											}
																										}
																									else
																										{	/* Object/slots.scm 302 */
																											obj_t BgL_arg1304z00_908;
																											obj_t BgL_arg1305z00_909;
																											obj_t BgL_arg1306z00_910;
																											long BgL_arg1307z00_911;

																											BgL_arg1304z00_908 =
																												CDR(
																												((obj_t)
																													BgL_clausesz00_835));
																											BgL_arg1305z00_909 =
																												MAKE_YOUNG_PAIR
																												(BGl_makezd2attributezd2slotze70ze7zzobject_slotsz00
																												(BgL_classz00_63,
																													BgL_sz00_843,
																													BgL_idz00_844,
																													BgL_attrz00_845,
																													BgL_vgetz00_873,
																													BgL_vsetz00_874,
																													BgL_vnumz00_838,
																													((bool_t) 0),
																													BgL_indexz00_839),
																												BgL_nslotsz00_836);
																											{	/* Object/slots.scm 306 */
																												bool_t
																													BgL_test2167z00_2739;
																												if (CBOOL
																													(BgL_vgetz00_873))
																													{	/* Object/slots.scm 306 */
																														BgL_test2167z00_2739
																															= ((bool_t) 1);
																													}
																												else
																													{	/* Object/slots.scm 306 */
																														BgL_test2167z00_2739
																															=
																															CBOOL
																															(BgL_vsetz00_874);
																													}
																												if (BgL_test2167z00_2739)
																													{	/* Object/slots.scm 306 */
																														if (INTEGERP
																															(BgL_vnumz00_838))
																															{	/* Object/slots.scm 306 */
																																obj_t
																																	BgL_tmpz00_1741;
																																BgL_tmpz00_1741
																																	= BINT(0L);
																																{	/* Object/slots.scm 306 */
																																	bool_t
																																		BgL_test2170z00_2746;
																																	{	/* Object/slots.scm 306 */
																																		obj_t
																																			BgL_tmpz00_2747;
																																		BgL_tmpz00_2747
																																			=
																																			BINT(1L);
																																		BgL_test2170z00_2746
																																			=
																																			BGL_ADDFX_OV
																																			(BgL_vnumz00_838,
																																			BgL_tmpz00_2747,
																																			BgL_tmpz00_1741);
																																	}
																																	if (BgL_test2170z00_2746)
																																		{	/* Object/slots.scm 306 */
																																			BgL_arg1306z00_910
																																				=
																																				bgl_bignum_add
																																				(bgl_long_to_bignum
																																				((long)
																																					CINT
																																					(BgL_vnumz00_838)),
																																				CNST_TABLE_REF
																																				(2));
																																		}
																																	else
																																		{	/* Object/slots.scm 306 */
																																			BgL_arg1306z00_910
																																				=
																																				BgL_tmpz00_1741;
																																		}
																																}
																															}
																														else
																															{	/* Object/slots.scm 306 */
																																BgL_arg1306z00_910
																																	=
																																	BGl_2zb2zb2zz__r4_numbers_6_5z00
																																	(BgL_vnumz00_838,
																																	BINT(1L));
																															}
																													}
																												else
																													{	/* Object/slots.scm 306 */
																														BgL_arg1306z00_910 =
																															BgL_vnumz00_838;
																													}
																											}
																											BgL_arg1307z00_911 =
																												(BgL_indexz00_839 + 1L);
																											{
																												long BgL_indexz00_2760;
																												obj_t BgL_vnumz00_2759;
																												obj_t
																													BgL_nslotsz00_2758;
																												obj_t
																													BgL_clausesz00_2757;
																												BgL_clausesz00_2757 =
																													BgL_arg1304z00_908;
																												BgL_nslotsz00_2758 =
																													BgL_arg1305z00_909;
																												BgL_vnumz00_2759 =
																													BgL_arg1306z00_910;
																												BgL_indexz00_2760 =
																													BgL_arg1307z00_911;
																												BgL_indexz00_839 =
																													BgL_indexz00_2760;
																												BgL_vnumz00_838 =
																													BgL_vnumz00_2759;
																												BgL_nslotsz00_836 =
																													BgL_nslotsz00_2758;
																												BgL_clausesz00_835 =
																													BgL_clausesz00_2757;
																												goto
																													BgL_zc3z04anonymousza31203ze3z87_840;
																											}
																										}
																								}
																						}
																					}
																				}
																			}
																		else
																			{	/* Object/slots.scm 272 */
																			BgL_tagzd2425zd2_849:
																				{	/* Object/slots.scm 315 */
																					obj_t BgL_arg1317z00_921;
																					obj_t BgL_arg1318z00_922;

																					BgL_arg1317z00_921 =
																						BGl_findzd2locationzd2zztools_locationz00
																						(BgL_sz00_843);
																					BgL_arg1318z00_922 =
																						(((BgL_typez00_bglt)
																							COBJECT(((BgL_typez00_bglt)
																									BgL_classz00_63)))->
																						BgL_idz00);
																					return
																						BGl_userzd2errorzf2locationz20zztools_errorz00
																						(BgL_arg1317z00_921,
																						BgL_arg1318z00_922,
																						BGl_string1986z00zzobject_slotsz00,
																						BgL_sz00_843, BNIL);
																				}
																			}
																	}
																else
																	{	/* Object/slots.scm 272 */
																		goto BgL_tagzd2425zd2_849;
																	}
															}
														else
															{	/* Object/slots.scm 272 */
																goto BgL_tagzd2425zd2_849;
															}
													}
												else
													{	/* Object/slots.scm 272 */
														obj_t BgL_cdrzd2450zd2_864;

														BgL_cdrzd2450zd2_864 = CDR(((obj_t) BgL_sz00_843));
														if ((BgL_carzd2431zd2_852 == CNST_TABLE_REF(3)))
															{	/* Object/slots.scm 272 */
																if (PAIRP(BgL_cdrzd2450zd2_864))
																	{	/* Object/slots.scm 272 */
																		if (NULLP(CDR(BgL_cdrzd2450zd2_864)))
																			{	/* Object/slots.scm 272 */
																				BgL_idz00_847 =
																					CAR(BgL_cdrzd2450zd2_864);
																				{	/* Object/slots.scm 309 */
																					obj_t BgL_arg1312z00_917;
																					obj_t BgL_arg1314z00_918;
																					long BgL_arg1315z00_919;

																					BgL_arg1312z00_917 =
																						CDR(((obj_t) BgL_clausesz00_835));
																					{	/* Object/slots.scm 310 */
																						BgL_slotz00_bglt BgL_arg1316z00_920;

																						BgL_slotzd2idzd2_1112 =
																							BgL_idz00_847;
																						BgL_indexz00_1113 =
																							BgL_indexz00_839;
																						{	/* Object/slots.scm 244 */
																							BgL_slotz00_bglt
																								BgL_new1097z00_1115;
																							{	/* Object/slots.scm 245 */
																								BgL_slotz00_bglt
																									BgL_new1096z00_1119;
																								BgL_new1096z00_1119 =
																									((BgL_slotz00_bglt)
																									BOBJECT(GC_MALLOC(sizeof
																											(struct
																												BgL_slotz00_bgl))));
																								{	/* Object/slots.scm 245 */
																									long BgL_arg1678z00_1120;

																									BgL_arg1678z00_1120 =
																										BGL_CLASS_NUM
																										(BGl_slotz00zzobject_slotsz00);
																									BGL_OBJECT_CLASS_NUM_SET((
																											(BgL_objectz00_bglt)
																											BgL_new1096z00_1119),
																										BgL_arg1678z00_1120);
																								}
																								BgL_new1097z00_1115 =
																									BgL_new1096z00_1119;
																							}
																							((((BgL_slotz00_bglt)
																										COBJECT
																										(BgL_new1097z00_1115))->
																									BgL_idz00) =
																								((obj_t) CAR(((obj_t)
																											BgL_slotzd2idzd2_1112))),
																								BUNSPEC);
																							{
																								obj_t BgL_auxz00_2784;

																								{	/* Object/slots.scm 247 */
																									obj_t BgL_arg1661z00_1116;

																									BgL_arg1661z00_1116 =
																										CAR(
																										((obj_t)
																											BgL_slotzd2idzd2_1112));
																									BgL_auxz00_2784 =
																										BGl_schemezd2symbolzd2ze3czd2stringz31zzobject_slotsz00
																										(BgL_arg1661z00_1116);
																								}
																								((((BgL_slotz00_bglt)
																											COBJECT
																											(BgL_new1097z00_1115))->
																										BgL_namez00) =
																									((obj_t) BgL_auxz00_2784),
																									BUNSPEC);
																							}
																							((((BgL_slotz00_bglt)
																										COBJECT
																										(BgL_new1097z00_1115))->
																									BgL_srcz00) =
																								((obj_t) BgL_slotzd2idzd2_1112),
																								BUNSPEC);
																							((((BgL_slotz00_bglt)
																										COBJECT
																										(BgL_new1097z00_1115))->
																									BgL_classzd2ownerzd2) =
																								((obj_t) ((obj_t)
																										BgL_classz00_63)), BUNSPEC);
																							((((BgL_slotz00_bglt)
																										COBJECT
																										(BgL_new1097z00_1115))->
																									BgL_indexz00) =
																								((long) BgL_indexz00_1113),
																								BUNSPEC);
																							((((BgL_slotz00_bglt)
																										COBJECT
																										(BgL_new1097z00_1115))->
																									BgL_typez00) =
																								((obj_t)
																									BGl_findzd2slotzd2typez00zzobject_slotsz00
																									(BgL_slotzd2idzd2_1112,
																										BgL_srcz00_67)), BUNSPEC);
																							((((BgL_slotz00_bglt)
																										COBJECT
																										(BgL_new1097z00_1115))->
																									BgL_readzd2onlyzf3z21) =
																								((bool_t) ((bool_t) 0)),
																								BUNSPEC);
																							{
																								obj_t BgL_auxz00_2796;

																								{	/* Object/slots.scm 251 */
																									obj_t BgL_arg1663z00_1117;

																									{	/* Object/slots.scm 251 */
																										obj_t BgL_arg1675z00_1118;

																										{	/* Object/slots.scm 251 */
																											obj_t BgL_classz00_1651;

																											BgL_classz00_1651 =
																												BGl_slotz00zzobject_slotsz00;
																											BgL_arg1675z00_1118 =
																												BGL_CLASS_ALL_FIELDS
																												(BgL_classz00_1651);
																										}
																										BgL_arg1663z00_1117 =
																											VECTOR_REF
																											(BgL_arg1675z00_1118, 7L);
																									}
																									BgL_auxz00_2796 =
																										BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																										(BgL_arg1663z00_1117);
																								}
																								((((BgL_slotz00_bglt)
																											COBJECT
																											(BgL_new1097z00_1115))->
																										BgL_defaultzd2valuezd2) =
																									((obj_t) BgL_auxz00_2796),
																									BUNSPEC);
																							}
																							((((BgL_slotz00_bglt)
																										COBJECT
																										(BgL_new1097z00_1115))->
																									BgL_virtualzd2numzd2) =
																								((obj_t) BINT(-1L)), BUNSPEC);
																							((((BgL_slotz00_bglt)
																										COBJECT
																										(BgL_new1097z00_1115))->
																									BgL_virtualzd2overridezd2) =
																								((bool_t) ((bool_t) 0)),
																								BUNSPEC);
																							((((BgL_slotz00_bglt)
																										COBJECT
																										(BgL_new1097z00_1115))->
																									BgL_getterz00) =
																								((obj_t) BFALSE), BUNSPEC);
																							((((BgL_slotz00_bglt)
																										COBJECT
																										(BgL_new1097z00_1115))->
																									BgL_setterz00) =
																								((obj_t) BFALSE), BUNSPEC);
																							((((BgL_slotz00_bglt)
																										COBJECT
																										(BgL_new1097z00_1115))->
																									BgL_userzd2infozd2) =
																								((obj_t) BFALSE), BUNSPEC);
																							BgL_arg1316z00_920 =
																								BgL_new1097z00_1115;
																						}
																						BgL_arg1314z00_918 =
																							MAKE_YOUNG_PAIR(
																							((obj_t) BgL_arg1316z00_920),
																							BgL_nslotsz00_836);
																					}
																					BgL_arg1315z00_919 =
																						(BgL_indexz00_839 + 1L);
																					{
																						long BgL_indexz00_2812;
																						obj_t BgL_nslotsz00_2811;
																						obj_t BgL_clausesz00_2810;

																						BgL_clausesz00_2810 =
																							BgL_arg1312z00_917;
																						BgL_nslotsz00_2811 =
																							BgL_arg1314z00_918;
																						BgL_indexz00_2812 =
																							BgL_arg1315z00_919;
																						BgL_indexz00_839 =
																							BgL_indexz00_2812;
																						BgL_nslotsz00_836 =
																							BgL_nslotsz00_2811;
																						BgL_clausesz00_835 =
																							BgL_clausesz00_2810;
																						goto
																							BgL_zc3z04anonymousza31203ze3z87_840;
																					}
																				}
																			}
																		else
																			{	/* Object/slots.scm 272 */
																				goto BgL_tagzd2425zd2_849;
																			}
																	}
																else
																	{	/* Object/slots.scm 272 */
																		goto BgL_tagzd2425zd2_849;
																	}
															}
														else
															{	/* Object/slots.scm 272 */
																goto BgL_tagzd2425zd2_849;
															}
													}
											}
										else
											{	/* Object/slots.scm 272 */
												goto BgL_tagzd2425zd2_849;
											}
									}
								}
						}
					}
				}
			}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zzobject_slotsz00(obj_t BgL_oldz00_2248,
		obj_t BgL_newz00_2247, obj_t BgL_lstz00_1101)
	{
		{	/* Object/slots.scm 233 */
			if (NULLP(BgL_lstz00_1101))
				{	/* Object/slots.scm 235 */
					return BgL_lstz00_1101;
				}
			else
				{	/* Object/slots.scm 235 */
					if ((CAR(((obj_t) BgL_lstz00_1101)) == BgL_oldz00_2248))
						{	/* Object/slots.scm 238 */
							obj_t BgL_arg1642z00_1106;

							BgL_arg1642z00_1106 = CDR(((obj_t) BgL_lstz00_1101));
							return MAKE_YOUNG_PAIR(BgL_newz00_2247, BgL_arg1642z00_1106);
						}
					else
						{	/* Object/slots.scm 240 */
							obj_t BgL_arg1646z00_1107;
							obj_t BgL_arg1650z00_1108;

							BgL_arg1646z00_1107 = CAR(((obj_t) BgL_lstz00_1101));
							{	/* Object/slots.scm 240 */
								obj_t BgL_arg1651z00_1109;

								BgL_arg1651z00_1109 = CDR(((obj_t) BgL_lstz00_1101));
								BgL_arg1650z00_1108 =
									BGl_loopze70ze7zzobject_slotsz00(BgL_oldz00_2248,
									BgL_newz00_2247, BgL_arg1651z00_1109);
							}
							return MAKE_YOUNG_PAIR(BgL_arg1646z00_1107, BgL_arg1650z00_1108);
						}
				}
		}

	}



/* slot-member?~0 */
	obj_t BGl_slotzd2memberzf3ze70zc6zzobject_slotsz00(obj_t BgL_idz00_1013,
		obj_t BgL_slotzd2listzd2_1014)
	{
		{	/* Object/slots.scm 177 */
			{
				obj_t BgL_slotzd2listzd2_1017;

				BgL_slotzd2listzd2_1017 = BgL_slotzd2listzd2_1014;
			BgL_zc3z04anonymousza31515ze3z87_1018:
				if (NULLP(BgL_slotzd2listzd2_1017))
					{	/* Object/slots.scm 172 */
						return BFALSE;
					}
				else
					{	/* Object/slots.scm 172 */
						if (
							((((BgL_slotz00_bglt) COBJECT(
											((BgL_slotz00_bglt)
												CAR(
													((obj_t) BgL_slotzd2listzd2_1017)))))->BgL_idz00) ==
								BgL_idz00_1013))
							{	/* Object/slots.scm 174 */
								return CAR(((obj_t) BgL_slotzd2listzd2_1017));
							}
						else
							{
								obj_t BgL_slotzd2listzd2_2840;

								BgL_slotzd2listzd2_2840 =
									CDR(((obj_t) BgL_slotzd2listzd2_1017));
								BgL_slotzd2listzd2_1017 = BgL_slotzd2listzd2_2840;
								goto BgL_zc3z04anonymousza31515ze3z87_1018;
							}
					}
			}
		}

	}



/* make-attribute-slot~0 */
	obj_t BGl_makezd2attributezd2slotze70ze7zzobject_slotsz00(BgL_typez00_bglt
		BgL_classz00_2249, obj_t BgL_sz00_1027, obj_t BgL_slotzd2idzd2_1028,
		obj_t BgL_attrz00_1029, obj_t BgL_vgetz00_1030, obj_t BgL_vsetz00_1031,
		obj_t BgL_vnumz00_1032, bool_t BgL_voverz00_1033, long BgL_indexz00_1034)
	{
		{	/* Object/slots.scm 216 */
			{
				obj_t BgL_attrz00_927;
				obj_t BgL_attrz00_956;

				{	/* Object/slots.scm 182 */
					obj_t BgL_readozf3zf3_1036;

					BgL_readozf3zf3_1036 =
						BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CNST_TABLE_REF(7),
						BgL_attrz00_1029);
					{	/* Object/slots.scm 184 */
						bool_t BgL_test2178z00_2845;

						if (CBOOL(BgL_vsetz00_1031))
							{	/* Object/slots.scm 184 */
								if (CBOOL(BgL_vgetz00_1030))
									{	/* Object/slots.scm 184 */
										BgL_test2178z00_2845 = ((bool_t) 0);
									}
								else
									{	/* Object/slots.scm 184 */
										BgL_test2178z00_2845 = ((bool_t) 1);
									}
							}
						else
							{	/* Object/slots.scm 184 */
								BgL_test2178z00_2845 = ((bool_t) 0);
							}
						if (BgL_test2178z00_2845)
							{	/* Object/slots.scm 185 */
								obj_t BgL_arg1559z00_1038;
								obj_t BgL_arg1561z00_1039;
								obj_t BgL_arg1564z00_1040;

								BgL_arg1559z00_1038 =
									BGl_findzd2locationzd2zztools_locationz00(BgL_sz00_1027);
								BgL_arg1561z00_1039 =
									(((BgL_typez00_bglt) COBJECT(
											((BgL_typez00_bglt) BgL_classz00_2249)))->BgL_idz00);
								BgL_arg1564z00_1040 = CAR(((obj_t) BgL_slotzd2idzd2_1028));
								{	/* Object/slots.scm 185 */
									obj_t BgL_list1565z00_1041;

									BgL_list1565z00_1041 = MAKE_YOUNG_PAIR(BNIL, BNIL);
									return
										BGl_userzd2errorzf2locationz20zztools_errorz00
										(BgL_arg1559z00_1038, BgL_arg1561z00_1039,
										BGl_string1989z00zzobject_slotsz00, BgL_arg1564z00_1040,
										BgL_list1565z00_1041);
								}
							}
						else
							{	/* Object/slots.scm 190 */
								bool_t BgL_test2181z00_2857;

								if (CBOOL(BgL_vsetz00_1031))
									{	/* Object/slots.scm 190 */
										BgL_test2181z00_2857 = CBOOL(BgL_readozf3zf3_1036);
									}
								else
									{	/* Object/slots.scm 190 */
										BgL_test2181z00_2857 = ((bool_t) 0);
									}
								if (BgL_test2181z00_2857)
									{	/* Object/slots.scm 191 */
										obj_t BgL_arg1571z00_1043;
										obj_t BgL_arg1573z00_1044;
										obj_t BgL_arg1575z00_1045;

										BgL_arg1571z00_1043 =
											BGl_findzd2locationzd2zztools_locationz00(BgL_sz00_1027);
										BgL_arg1573z00_1044 =
											(((BgL_typez00_bglt) COBJECT(
													((BgL_typez00_bglt) BgL_classz00_2249)))->BgL_idz00);
										BgL_arg1575z00_1045 = CAR(((obj_t) BgL_slotzd2idzd2_1028));
										{	/* Object/slots.scm 191 */
											obj_t BgL_list1576z00_1046;

											BgL_list1576z00_1046 = MAKE_YOUNG_PAIR(BNIL, BNIL);
											return
												BGl_userzd2errorzf2locationz20zztools_errorz00
												(BgL_arg1571z00_1043, BgL_arg1573z00_1044,
												BGl_string1990z00zzobject_slotsz00, BgL_arg1575z00_1045,
												BgL_list1576z00_1046);
										}
									}
								else
									{	/* Object/slots.scm 196 */
										bool_t BgL_test2183z00_2868;

										if (CBOOL(BgL_vgetz00_1030))
											{	/* Object/slots.scm 196 */
												if (CBOOL(BgL_vsetz00_1031))
													{	/* Object/slots.scm 196 */
														BgL_test2183z00_2868 = ((bool_t) 0);
													}
												else
													{	/* Object/slots.scm 196 */
														if (CBOOL(BgL_readozf3zf3_1036))
															{	/* Object/slots.scm 196 */
																BgL_test2183z00_2868 = ((bool_t) 0);
															}
														else
															{	/* Object/slots.scm 196 */
																BgL_test2183z00_2868 = ((bool_t) 1);
															}
													}
											}
										else
											{	/* Object/slots.scm 196 */
												BgL_test2183z00_2868 = ((bool_t) 0);
											}
										if (BgL_test2183z00_2868)
											{	/* Object/slots.scm 197 */
												obj_t BgL_arg1584z00_1048;
												obj_t BgL_arg1585z00_1049;
												obj_t BgL_arg1589z00_1050;

												BgL_arg1584z00_1048 =
													BGl_findzd2locationzd2zztools_locationz00
													(BgL_sz00_1027);
												BgL_arg1585z00_1049 =
													(((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
																BgL_classz00_2249)))->BgL_idz00);
												BgL_arg1589z00_1050 =
													CAR(((obj_t) BgL_slotzd2idzd2_1028));
												{	/* Object/slots.scm 197 */
													obj_t BgL_list1590z00_1051;

													BgL_list1590z00_1051 = MAKE_YOUNG_PAIR(BNIL, BNIL);
													return
														BGl_userzd2errorzf2locationz20zztools_errorz00
														(BgL_arg1584z00_1048, BgL_arg1585z00_1049,
														BGl_string1991z00zzobject_slotsz00,
														BgL_arg1589z00_1050, BgL_list1590z00_1051);
												}
											}
										else
											{	/* Object/slots.scm 203 */
												BgL_slotz00_bglt BgL_new1095z00_1052;

												{	/* Object/slots.scm 204 */
													BgL_slotz00_bglt BgL_new1094z00_1054;

													BgL_new1094z00_1054 =
														((BgL_slotz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_slotz00_bgl))));
													{	/* Object/slots.scm 204 */
														long BgL_arg1593z00_1055;

														BgL_arg1593z00_1055 =
															BGL_CLASS_NUM(BGl_slotz00zzobject_slotsz00);
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1094z00_1054),
															BgL_arg1593z00_1055);
													}
													BgL_new1095z00_1052 = BgL_new1094z00_1054;
												}
												((((BgL_slotz00_bglt) COBJECT(BgL_new1095z00_1052))->
														BgL_idz00) =
													((obj_t) CAR(((obj_t) BgL_slotzd2idzd2_1028))),
													BUNSPEC);
												{
													obj_t BgL_auxz00_2889;

													{	/* Object/slots.scm 206 */
														obj_t BgL_arg1591z00_1053;

														BgL_arg1591z00_1053 =
															CAR(((obj_t) BgL_slotzd2idzd2_1028));
														BgL_auxz00_2889 =
															BGl_schemezd2symbolzd2ze3czd2stringz31zzobject_slotsz00
															(BgL_arg1591z00_1053);
													}
													((((BgL_slotz00_bglt) COBJECT(BgL_new1095z00_1052))->
															BgL_namez00) =
														((obj_t) BgL_auxz00_2889), BUNSPEC);
												}
												((((BgL_slotz00_bglt) COBJECT(BgL_new1095z00_1052))->
														BgL_srcz00) = ((obj_t) BgL_sz00_1027), BUNSPEC);
												((((BgL_slotz00_bglt) COBJECT(BgL_new1095z00_1052))->
														BgL_classzd2ownerzd2) =
													((obj_t) ((obj_t) BgL_classz00_2249)), BUNSPEC);
												((((BgL_slotz00_bglt) COBJECT(BgL_new1095z00_1052))->
														BgL_indexz00) =
													((long) BgL_indexz00_1034), BUNSPEC);
												((((BgL_slotz00_bglt) COBJECT(BgL_new1095z00_1052))->
														BgL_typez00) =
													((obj_t)
														BGl_findzd2slotzd2typez00zzobject_slotsz00
														(BgL_slotzd2idzd2_1028, BgL_sz00_1027)), BUNSPEC);
												((((BgL_slotz00_bglt) COBJECT(BgL_new1095z00_1052))->
														BgL_readzd2onlyzf3z21) =
													((bool_t) CBOOL(BgL_readozf3zf3_1036)), BUNSPEC);
												{
													obj_t BgL_auxz00_2902;

													BgL_attrz00_927 = BgL_attrz00_1029;
												BgL_zc3z04anonymousza31322ze3z87_928:
													if (PAIRP(BgL_attrz00_927))
														{

															{	/* Object/slots.scm 134 */
																obj_t BgL_ezd2362zd2_935;

																BgL_ezd2362zd2_935 = CAR(BgL_attrz00_927);
																if (PAIRP(BgL_ezd2362zd2_935))
																	{	/* Object/slots.scm 134 */
																		obj_t BgL_cdrzd2366zd2_937;

																		BgL_cdrzd2366zd2_937 =
																			CDR(BgL_ezd2362zd2_935);
																		if (
																			(CAR(BgL_ezd2362zd2_935) ==
																				CNST_TABLE_REF(5)))
																			{	/* Object/slots.scm 134 */
																				if (PAIRP(BgL_cdrzd2366zd2_937))
																					{	/* Object/slots.scm 134 */
																						if (NULLP(CDR
																								(BgL_cdrzd2366zd2_937)))
																							{	/* Object/slots.scm 134 */
																								BgL_auxz00_2902 =
																									CAR(BgL_cdrzd2366zd2_937);
																							}
																						else
																							{	/* Object/slots.scm 134 */
																							BgL_tagzd2361zd2_934:
																								{	/* Object/slots.scm 140 */
																									obj_t BgL_arg1349z00_955;

																									BgL_arg1349z00_955 =
																										CDR(
																										((obj_t) BgL_attrz00_927));
																									{
																										obj_t BgL_attrz00_2921;

																										BgL_attrz00_2921 =
																											BgL_arg1349z00_955;
																										BgL_attrz00_927 =
																											BgL_attrz00_2921;
																										goto
																											BgL_zc3z04anonymousza31322ze3z87_928;
																									}
																								}
																							}
																					}
																				else
																					{	/* Object/slots.scm 134 */
																						goto BgL_tagzd2361zd2_934;
																					}
																			}
																		else
																			{	/* Object/slots.scm 134 */
																				if (
																					(CAR(BgL_ezd2362zd2_935) ==
																						CNST_TABLE_REF(6)))
																					{	/* Object/slots.scm 134 */
																						if (PAIRP(BgL_cdrzd2366zd2_937))
																							{	/* Object/slots.scm 134 */
																								if (NULLP(CDR
																										(BgL_cdrzd2366zd2_937)))
																									{	/* Object/slots.scm 134 */
																										obj_t BgL_arg1342z00_951;

																										BgL_arg1342z00_951 =
																											CAR(BgL_cdrzd2366zd2_937);
																										BgL_auxz00_2902 =
																											MAKE_YOUNG_PAIR
																											(BGl_slotzd2inlinezd2markz00zzobject_slotsz00,
																											BgL_arg1342z00_951);
																									}
																								else
																									{	/* Object/slots.scm 134 */
																										goto BgL_tagzd2361zd2_934;
																									}
																							}
																						else
																							{	/* Object/slots.scm 134 */
																								goto BgL_tagzd2361zd2_934;
																							}
																					}
																				else
																					{	/* Object/slots.scm 134 */
																						goto BgL_tagzd2361zd2_934;
																					}
																			}
																	}
																else
																	{	/* Object/slots.scm 134 */
																		goto BgL_tagzd2361zd2_934;
																	}
															}
														}
													else
														{	/* Object/slots.scm 132 */
															BgL_auxz00_2902 =
																MAKE_YOUNG_PAIR(BINT(1L), BINT(2L));
														}
													((((BgL_slotz00_bglt) COBJECT(BgL_new1095z00_1052))->
															BgL_defaultzd2valuezd2) =
														((obj_t) BgL_auxz00_2902), BUNSPEC);
												}
												{
													obj_t BgL_auxz00_2937;

													if (CBOOL(BgL_vgetz00_1030))
														{	/* Object/slots.scm 212 */
															BgL_auxz00_2937 = BgL_vnumz00_1032;
														}
													else
														{	/* Object/slots.scm 212 */
															BgL_auxz00_2937 = BINT(-1L);
														}
													((((BgL_slotz00_bglt) COBJECT(BgL_new1095z00_1052))->
															BgL_virtualzd2numzd2) =
														((obj_t) BgL_auxz00_2937), BUNSPEC);
												}
												((((BgL_slotz00_bglt) COBJECT(BgL_new1095z00_1052))->
														BgL_virtualzd2overridezd2) =
													((bool_t) BgL_voverz00_1033), BUNSPEC);
												((((BgL_slotz00_bglt) COBJECT(BgL_new1095z00_1052))->
														BgL_getterz00) =
													((obj_t) BgL_vgetz00_1030), BUNSPEC);
												((((BgL_slotz00_bglt) COBJECT(BgL_new1095z00_1052))->
														BgL_setterz00) =
													((obj_t) BgL_vsetz00_1031), BUNSPEC);
												{
													obj_t BgL_auxz00_2945;

													BgL_attrz00_956 = BgL_attrz00_1029;
												BgL_zc3z04anonymousza31350ze3z87_957:
													if (PAIRP(BgL_attrz00_956))
														{

															{	/* Object/slots.scm 146 */
																obj_t BgL_ezd2387zd2_962;

																BgL_ezd2387zd2_962 = CAR(BgL_attrz00_956);
																if (PAIRP(BgL_ezd2387zd2_962))
																	{	/* Object/slots.scm 146 */
																		obj_t BgL_cdrzd2391zd2_964;

																		BgL_cdrzd2391zd2_964 =
																			CDR(BgL_ezd2387zd2_962);
																		if (
																			(CAR(BgL_ezd2387zd2_962) ==
																				CNST_TABLE_REF(4)))
																			{	/* Object/slots.scm 146 */
																				if (PAIRP(BgL_cdrzd2391zd2_964))
																					{	/* Object/slots.scm 146 */
																						if (NULLP(CDR
																								(BgL_cdrzd2391zd2_964)))
																							{	/* Object/slots.scm 146 */
																								BgL_auxz00_2945 =
																									CAR(BgL_cdrzd2391zd2_964);
																							}
																						else
																							{	/* Object/slots.scm 146 */
																							BgL_tagzd2386zd2_961:
																								{
																									obj_t BgL_attrz00_2962;

																									BgL_attrz00_2962 =
																										CDR(
																										((obj_t) BgL_attrz00_956));
																									BgL_attrz00_956 =
																										BgL_attrz00_2962;
																									goto
																										BgL_zc3z04anonymousza31350ze3z87_957;
																								}
																							}
																					}
																				else
																					{	/* Object/slots.scm 146 */
																						goto BgL_tagzd2386zd2_961;
																					}
																			}
																		else
																			{	/* Object/slots.scm 146 */
																				goto BgL_tagzd2386zd2_961;
																			}
																	}
																else
																	{	/* Object/slots.scm 146 */
																		goto BgL_tagzd2386zd2_961;
																	}
															}
														}
													else
														{	/* Object/slots.scm 144 */
															BgL_auxz00_2945 = BFALSE;
														}
													((((BgL_slotz00_bglt) COBJECT(BgL_new1095z00_1052))->
															BgL_userzd2infozd2) =
														((obj_t) BgL_auxz00_2945), BUNSPEC);
												}
												return ((obj_t) BgL_new1095z00_1052);
											}
									}
							}
					}
				}
			}
		}

	}



/* &make-class-slots */
	obj_t BGl_z62makezd2classzd2slotsz62zzobject_slotsz00(obj_t BgL_envz00_2098,
		obj_t BgL_classz00_2099, obj_t BgL_clausesz00_2100, obj_t BgL_superz00_2101,
		obj_t BgL_vnumz00_2102, obj_t BgL_srcz00_2103)
	{
		{	/* Object/slots.scm 128 */
			return
				BGl_makezd2classzd2slotsz00zzobject_slotsz00(
				((BgL_typez00_bglt) BgL_classz00_2099), BgL_clausesz00_2100,
				BgL_superz00_2101, CINT(BgL_vnumz00_2102), BgL_srcz00_2103);
		}

	}



/* make-java-class-slots */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2javazd2classzd2slotszd2zzobject_slotsz00(BgL_typez00_bglt
		BgL_classz00_68, obj_t BgL_clausesz00_69, obj_t BgL_superz00_70,
		obj_t BgL_srcz00_71)
	{
		{	/* Object/slots.scm 323 */
			{	/* Object/slots.scm 325 */
				obj_t BgL_locz00_1130;

				BgL_locz00_1130 =
					BGl_findzd2locationzd2zztools_locationz00(BgL_srcz00_71);
				{
					obj_t BgL_idz00_1170;
					obj_t BgL_jnamez00_1171;
					obj_t BgL_readozf3zf3_1172;
					obj_t BgL_srcz00_1173;
					obj_t BgL_identz00_1157;
					obj_t BgL_jnamez00_1158;
					obj_t BgL_readozf3zf3_1159;
					obj_t BgL_sz00_1160;
					long BgL_indexz00_1161;

					{	/* Object/slots.scm 356 */
						obj_t BgL_sslotsz00_1133;

						{	/* Object/slots.scm 357 */
							bool_t BgL_test2201z00_2971;

							{	/* Object/slots.scm 357 */
								obj_t BgL_classz00_1782;

								BgL_classz00_1782 = BGl_typez00zztype_typez00;
								if (BGL_OBJECTP(BgL_superz00_70))
									{	/* Object/slots.scm 357 */
										BgL_objectz00_bglt BgL_arg1807z00_1784;

										BgL_arg1807z00_1784 =
											(BgL_objectz00_bglt) (BgL_superz00_70);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Object/slots.scm 357 */
												long BgL_idxz00_1790;

												BgL_idxz00_1790 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1784);
												BgL_test2201z00_2971 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_1790 + 1L)) == BgL_classz00_1782);
											}
										else
											{	/* Object/slots.scm 357 */
												bool_t BgL_res1978z00_1815;

												{	/* Object/slots.scm 357 */
													obj_t BgL_oclassz00_1798;

													{	/* Object/slots.scm 357 */
														obj_t BgL_arg1815z00_1806;
														long BgL_arg1816z00_1807;

														BgL_arg1815z00_1806 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Object/slots.scm 357 */
															long BgL_arg1817z00_1808;

															BgL_arg1817z00_1808 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1784);
															BgL_arg1816z00_1807 =
																(BgL_arg1817z00_1808 - OBJECT_TYPE);
														}
														BgL_oclassz00_1798 =
															VECTOR_REF(BgL_arg1815z00_1806,
															BgL_arg1816z00_1807);
													}
													{	/* Object/slots.scm 357 */
														bool_t BgL__ortest_1115z00_1799;

														BgL__ortest_1115z00_1799 =
															(BgL_classz00_1782 == BgL_oclassz00_1798);
														if (BgL__ortest_1115z00_1799)
															{	/* Object/slots.scm 357 */
																BgL_res1978z00_1815 = BgL__ortest_1115z00_1799;
															}
														else
															{	/* Object/slots.scm 357 */
																long BgL_odepthz00_1800;

																{	/* Object/slots.scm 357 */
																	obj_t BgL_arg1804z00_1801;

																	BgL_arg1804z00_1801 = (BgL_oclassz00_1798);
																	BgL_odepthz00_1800 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_1801);
																}
																if ((1L < BgL_odepthz00_1800))
																	{	/* Object/slots.scm 357 */
																		obj_t BgL_arg1802z00_1803;

																		{	/* Object/slots.scm 357 */
																			obj_t BgL_arg1803z00_1804;

																			BgL_arg1803z00_1804 =
																				(BgL_oclassz00_1798);
																			BgL_arg1802z00_1803 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_1804, 1L);
																		}
																		BgL_res1978z00_1815 =
																			(BgL_arg1802z00_1803 ==
																			BgL_classz00_1782);
																	}
																else
																	{	/* Object/slots.scm 357 */
																		BgL_res1978z00_1815 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2201z00_2971 = BgL_res1978z00_1815;
											}
									}
								else
									{	/* Object/slots.scm 357 */
										BgL_test2201z00_2971 = ((bool_t) 0);
									}
							}
							if (BgL_test2201z00_2971)
								{	/* Object/slots.scm 359 */
									bool_t BgL_test2206z00_2994;

									{	/* Object/slots.scm 359 */
										obj_t BgL_classz00_1816;

										BgL_classz00_1816 = BGl_jclassz00zzobject_classz00;
										if (BGL_OBJECTP(BgL_superz00_70))
											{	/* Object/slots.scm 359 */
												BgL_objectz00_bglt BgL_arg1807z00_1818;

												BgL_arg1807z00_1818 =
													(BgL_objectz00_bglt) (BgL_superz00_70);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Object/slots.scm 359 */
														long BgL_idxz00_1824;

														BgL_idxz00_1824 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1818);
														BgL_test2206z00_2994 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_1824 + 2L)) == BgL_classz00_1816);
													}
												else
													{	/* Object/slots.scm 359 */
														bool_t BgL_res1979z00_1849;

														{	/* Object/slots.scm 359 */
															obj_t BgL_oclassz00_1832;

															{	/* Object/slots.scm 359 */
																obj_t BgL_arg1815z00_1840;
																long BgL_arg1816z00_1841;

																BgL_arg1815z00_1840 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Object/slots.scm 359 */
																	long BgL_arg1817z00_1842;

																	BgL_arg1817z00_1842 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1818);
																	BgL_arg1816z00_1841 =
																		(BgL_arg1817z00_1842 - OBJECT_TYPE);
																}
																BgL_oclassz00_1832 =
																	VECTOR_REF(BgL_arg1815z00_1840,
																	BgL_arg1816z00_1841);
															}
															{	/* Object/slots.scm 359 */
																bool_t BgL__ortest_1115z00_1833;

																BgL__ortest_1115z00_1833 =
																	(BgL_classz00_1816 == BgL_oclassz00_1832);
																if (BgL__ortest_1115z00_1833)
																	{	/* Object/slots.scm 359 */
																		BgL_res1979z00_1849 =
																			BgL__ortest_1115z00_1833;
																	}
																else
																	{	/* Object/slots.scm 359 */
																		long BgL_odepthz00_1834;

																		{	/* Object/slots.scm 359 */
																			obj_t BgL_arg1804z00_1835;

																			BgL_arg1804z00_1835 =
																				(BgL_oclassz00_1832);
																			BgL_odepthz00_1834 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_1835);
																		}
																		if ((2L < BgL_odepthz00_1834))
																			{	/* Object/slots.scm 359 */
																				obj_t BgL_arg1802z00_1837;

																				{	/* Object/slots.scm 359 */
																					obj_t BgL_arg1803z00_1838;

																					BgL_arg1803z00_1838 =
																						(BgL_oclassz00_1832);
																					BgL_arg1802z00_1837 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_1838, 2L);
																				}
																				BgL_res1979z00_1849 =
																					(BgL_arg1802z00_1837 ==
																					BgL_classz00_1816);
																			}
																		else
																			{	/* Object/slots.scm 359 */
																				BgL_res1979z00_1849 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2206z00_2994 = BgL_res1979z00_1849;
													}
											}
										else
											{	/* Object/slots.scm 359 */
												BgL_test2206z00_2994 = ((bool_t) 0);
											}
									}
									if (BgL_test2206z00_2994)
										{	/* Object/slots.scm 362 */
											obj_t BgL_arg1702z00_1156;

											{
												BgL_jclassz00_bglt BgL_auxz00_3017;

												{
													obj_t BgL_auxz00_3018;

													{	/* Object/slots.scm 362 */
														BgL_objectz00_bglt BgL_tmpz00_3019;

														BgL_tmpz00_3019 =
															((BgL_objectz00_bglt)
															((BgL_typez00_bglt) BgL_superz00_70));
														BgL_auxz00_3018 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_3019);
													}
													BgL_auxz00_3017 =
														((BgL_jclassz00_bglt) BgL_auxz00_3018);
												}
												BgL_arg1702z00_1156 =
													(((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_3017))->
													BgL_slotsz00);
											}
											BgL_sslotsz00_1133 = bgl_reverse(BgL_arg1702z00_1156);
										}
									else
										{	/* Object/slots.scm 359 */
											BgL_sslotsz00_1133 = BNIL;
										}
								}
							else
								{	/* Object/slots.scm 357 */
									BgL_sslotsz00_1133 = BNIL;
								}
						}
						{	/* Object/slots.scm 363 */
							long BgL_g1105z00_1134;

							BgL_g1105z00_1134 = bgl_list_length(BgL_sslotsz00_1133);
							{
								obj_t BgL_clausesz00_1136;
								obj_t BgL_resz00_1137;
								long BgL_indexz00_1138;

								BgL_clausesz00_1136 = BgL_clausesz00_69;
								BgL_resz00_1137 = BgL_sslotsz00_1133;
								BgL_indexz00_1138 = BgL_g1105z00_1134;
							BgL_zc3z04anonymousza31679ze3z87_1139:
								if (NULLP(BgL_clausesz00_1136))
									{	/* Object/slots.scm 366 */
										return BgL_resz00_1137;
									}
								else
									{	/* Object/slots.scm 368 */
										obj_t BgL_sz00_1141;

										BgL_sz00_1141 = CAR(((obj_t) BgL_clausesz00_1136));
										{	/* Object/slots.scm 368 */
											obj_t BgL_srcz00_1142;

											BgL_srcz00_1142 = CAR(((obj_t) BgL_sz00_1141));
											{	/* Object/slots.scm 369 */
												obj_t BgL_idz00_1143;

												{	/* Object/slots.scm 370 */
													obj_t BgL_pairz00_1857;

													BgL_pairz00_1857 = CDR(((obj_t) BgL_sz00_1141));
													BgL_idz00_1143 = CAR(BgL_pairz00_1857);
												}
												{	/* Object/slots.scm 370 */
													obj_t BgL_jnamez00_1144;

													{	/* Object/slots.scm 371 */
														obj_t BgL_pairz00_1863;

														{	/* Object/slots.scm 371 */
															obj_t BgL_pairz00_1862;

															BgL_pairz00_1862 = CDR(((obj_t) BgL_sz00_1141));
															BgL_pairz00_1863 = CDR(BgL_pairz00_1862);
														}
														BgL_jnamez00_1144 = CAR(BgL_pairz00_1863);
													}
													{	/* Object/slots.scm 371 */
														obj_t BgL_modz00_1145;

														{	/* Object/slots.scm 372 */
															obj_t BgL_pairz00_1871;

															{	/* Object/slots.scm 372 */
																obj_t BgL_pairz00_1870;

																{	/* Object/slots.scm 372 */
																	obj_t BgL_pairz00_1869;

																	BgL_pairz00_1869 =
																		CDR(((obj_t) BgL_sz00_1141));
																	BgL_pairz00_1870 = CDR(BgL_pairz00_1869);
																}
																BgL_pairz00_1871 = CDR(BgL_pairz00_1870);
															}
															BgL_modz00_1145 = CAR(BgL_pairz00_1871);
														}
														{	/* Object/slots.scm 372 */
															obj_t BgL_readozf3zf3_1146;

															BgL_readozf3zf3_1146 =
																BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																(CNST_TABLE_REF(10), BgL_modz00_1145);
															{	/* Object/slots.scm 373 */

																if (CBOOL
																	(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																		(CNST_TABLE_REF(11), BgL_modz00_1145)))
																	{	/* Object/slots.scm 374 */
																		BgL_idz00_1170 = BgL_idz00_1143;
																		BgL_jnamez00_1171 = BgL_jnamez00_1144;
																		BgL_readozf3zf3_1172 = BgL_readozf3zf3_1146;
																		BgL_srcz00_1173 = BgL_srcz00_1142;
																		{	/* Object/slots.scm 340 */
																			obj_t BgL_locz00_1175;

																			BgL_locz00_1175 =
																				BGl_findzd2locationzf2locz20zztools_locationz00
																				(BgL_srcz00_1173, BgL_locz00_1130);
																			{	/* Object/slots.scm 340 */
																				obj_t BgL_slotzd2idzd2_1176;

																				BgL_slotzd2idzd2_1176 =
																					BGl_parsezd2idzd2zzast_identz00
																					(BgL_idz00_1170, BgL_locz00_1175);
																				{	/* Object/slots.scm 341 */
																					obj_t BgL_lzd2idzd2_1177;

																					if (
																						((((BgL_typez00_bglt) COBJECT(
																										((BgL_typez00_bglt)
																											BgL_classz00_68)))->
																								BgL_idz00) ==
																							CNST_TABLE_REF(8)))
																						{	/* Object/slots.scm 342 */
																							BgL_lzd2idzd2_1177 =
																								CAR(BgL_slotzd2idzd2_1176);
																						}
																					else
																						{	/* Object/slots.scm 344 */
																							obj_t BgL_arg1724z00_1189;
																							obj_t BgL_arg1733z00_1190;

																							BgL_arg1724z00_1189 =
																								(((BgL_typez00_bglt) COBJECT(
																										((BgL_typez00_bglt)
																											BgL_classz00_68)))->
																								BgL_idz00);
																							BgL_arg1733z00_1190 =
																								CAR(BgL_slotzd2idzd2_1176);
																							{	/* Object/slots.scm 344 */
																								obj_t BgL_list1734z00_1191;

																								{	/* Object/slots.scm 344 */
																									obj_t BgL_arg1735z00_1192;

																									{	/* Object/slots.scm 344 */
																										obj_t BgL_arg1736z00_1193;

																										BgL_arg1736z00_1193 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1733z00_1190,
																											BNIL);
																										BgL_arg1735z00_1192 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(9),
																											BgL_arg1736z00_1193);
																									}
																									BgL_list1734z00_1191 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1724z00_1189,
																										BgL_arg1735z00_1192);
																								}
																								BgL_lzd2idzd2_1177 =
																									BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																									(BgL_list1734z00_1191);
																							}
																						}
																					{	/* Object/slots.scm 342 */
																						obj_t BgL_tzd2idzd2_1178;

																						{	/* Object/slots.scm 345 */
																							bool_t BgL_test2214z00_3067;

																							{	/* Object/slots.scm 345 */
																								obj_t BgL_arg1720z00_1186;

																								BgL_arg1720z00_1186 =
																									CDR(BgL_slotzd2idzd2_1176);
																								BgL_test2214z00_3067 =
																									(BgL_arg1720z00_1186 ==
																									BGl_za2_za2z00zztype_cachez00);
																							}
																							if (BgL_test2214z00_3067)
																								{	/* Object/slots.scm 345 */
																									BgL_tzd2idzd2_1178 =
																										BGl_za2objza2z00zztype_cachez00;
																								}
																							else
																								{	/* Object/slots.scm 345 */
																									BgL_tzd2idzd2_1178 =
																										CDR(BgL_slotzd2idzd2_1176);
																								}
																						}
																						{	/* Object/slots.scm 348 */

																							BGl_ensurezd2typezd2definedz12z12zzobject_slotsz00
																								(((BgL_typez00_bglt)
																									BgL_tzd2idzd2_1178),
																								BgL_srcz00_1173);
																							{	/* Object/slots.scm 350 */
																								BgL_globalz00_bglt
																									BgL_gz00_1180;
																								{	/* Object/slots.scm 351 */
																									obj_t BgL_arg1717z00_1183;

																									BgL_arg1717z00_1183 =
																										(((BgL_typez00_bglt)
																											COBJECT((
																													(BgL_typez00_bglt)
																													BgL_tzd2idzd2_1178)))->
																										BgL_idz00);
																									BgL_gz00_1180 =
																										BGl_declarezd2globalzd2cvarz12z12zzast_glozd2declzd2
																										(BgL_lzd2idzd2_1177, BFALSE,
																										BgL_jnamez00_1171,
																										BgL_arg1717z00_1183,
																										CBOOL(BgL_readozf3zf3_1172),
																										BgL_srcz00_1173, BFALSE);
																								}
																								{	/* Object/slots.scm 352 */
																									obj_t BgL_arg1711z00_1181;

																									BgL_arg1711z00_1181 =
																										(((BgL_typez00_bglt)
																											COBJECT((
																													(BgL_typez00_bglt)
																													BgL_classz00_68)))->
																										BgL_idz00);
																									((((BgL_globalz00_bglt)
																												COBJECT
																												(BgL_gz00_1180))->
																											BgL_modulez00) =
																										((obj_t)
																											BgL_arg1711z00_1181),
																										BUNSPEC);
																								}
																								{	/* Object/slots.scm 353 */
																									obj_t BgL_arg1714z00_1182;

																									BgL_arg1714z00_1182 =
																										(((BgL_typez00_bglt)
																											COBJECT((
																													(BgL_typez00_bglt)
																													BgL_classz00_68)))->
																										BgL_namez00);
																									((((BgL_globalz00_bglt)
																												COBJECT
																												(BgL_gz00_1180))->
																											BgL_jvmzd2typezd2namez00)
																										=
																										((obj_t) ((obj_t)
																												BgL_arg1714z00_1182)),
																										BUNSPEC);
																								}
																								BgL_gz00_1180;
																							}
																						}
																					}
																				}
																			}
																		}
																		{	/* Object/slots.scm 377 */
																			obj_t BgL_arg1688z00_1148;

																			BgL_arg1688z00_1148 =
																				CDR(((obj_t) BgL_clausesz00_1136));
																			{
																				obj_t BgL_clausesz00_3086;

																				BgL_clausesz00_3086 =
																					BgL_arg1688z00_1148;
																				BgL_clausesz00_1136 =
																					BgL_clausesz00_3086;
																				goto
																					BgL_zc3z04anonymousza31679ze3z87_1139;
																			}
																		}
																	}
																else
																	{	/* Object/slots.scm 378 */
																		obj_t BgL_arg1689z00_1149;
																		obj_t BgL_arg1691z00_1150;
																		long BgL_arg1692z00_1151;

																		BgL_arg1689z00_1149 =
																			CDR(((obj_t) BgL_clausesz00_1136));
																		{	/* Object/slots.scm 379 */
																			BgL_slotz00_bglt BgL_arg1699z00_1152;

																			BgL_identz00_1157 = BgL_idz00_1143;
																			BgL_jnamez00_1158 = BgL_jnamez00_1144;
																			BgL_readozf3zf3_1159 =
																				BgL_readozf3zf3_1146;
																			BgL_sz00_1160 = BgL_srcz00_1142;
																			BgL_indexz00_1161 = BgL_indexz00_1138;
																			{	/* Object/slots.scm 328 */
																				obj_t BgL_locz00_1163;

																				BgL_locz00_1163 =
																					BGl_findzd2locationzf2locz20zztools_locationz00
																					(BgL_sz00_1160, BgL_locz00_1130);
																				{	/* Object/slots.scm 328 */
																					obj_t BgL_slotzd2idzd2_1164;

																					BgL_slotzd2idzd2_1164 =
																						BGl_parsezd2idzd2zzast_identz00
																						(BgL_identz00_1157,
																						BgL_locz00_1163);
																					{	/* Object/slots.scm 329 */

																						{	/* Object/slots.scm 330 */
																							BgL_slotz00_bglt
																								BgL_new1104z00_1165;
																							{	/* Object/slots.scm 331 */
																								BgL_slotz00_bglt
																									BgL_new1103z00_1168;
																								BgL_new1103z00_1168 =
																									((BgL_slotz00_bglt)
																									BOBJECT(GC_MALLOC(sizeof
																											(struct
																												BgL_slotz00_bgl))));
																								{	/* Object/slots.scm 331 */
																									long BgL_arg1709z00_1169;

																									BgL_arg1709z00_1169 =
																										BGL_CLASS_NUM
																										(BGl_slotz00zzobject_slotsz00);
																									BGL_OBJECT_CLASS_NUM_SET((
																											(BgL_objectz00_bglt)
																											BgL_new1103z00_1168),
																										BgL_arg1709z00_1169);
																								}
																								BgL_new1104z00_1165 =
																									BgL_new1103z00_1168;
																							}
																							((((BgL_slotz00_bglt)
																										COBJECT
																										(BgL_new1104z00_1165))->
																									BgL_idz00) =
																								((obj_t)
																									CAR(BgL_slotzd2idzd2_1164)),
																								BUNSPEC);
																							((((BgL_slotz00_bglt)
																										COBJECT
																										(BgL_new1104z00_1165))->
																									BgL_namez00) =
																								((obj_t) BgL_jnamez00_1158),
																								BUNSPEC);
																							((((BgL_slotz00_bglt)
																										COBJECT
																										(BgL_new1104z00_1165))->
																									BgL_srcz00) =
																								((obj_t) BgL_sz00_1160),
																								BUNSPEC);
																							((((BgL_slotz00_bglt)
																										COBJECT
																										(BgL_new1104z00_1165))->
																									BgL_classzd2ownerzd2) =
																								((obj_t) ((obj_t)
																										BgL_classz00_68)), BUNSPEC);
																							((((BgL_slotz00_bglt)
																										COBJECT
																										(BgL_new1104z00_1165))->
																									BgL_indexz00) =
																								((long) BgL_indexz00_1161),
																								BUNSPEC);
																							((((BgL_slotz00_bglt)
																										COBJECT
																										(BgL_new1104z00_1165))->
																									BgL_typez00) =
																								((obj_t)
																									BGl_findzd2slotzd2typez00zzobject_slotsz00
																									(BgL_slotzd2idzd2_1164,
																										BgL_srcz00_71)), BUNSPEC);
																							((((BgL_slotz00_bglt)
																										COBJECT
																										(BgL_new1104z00_1165))->
																									BgL_readzd2onlyzf3z21) =
																								((bool_t)
																									CBOOL(BgL_readozf3zf3_1159)),
																								BUNSPEC);
																							{
																								obj_t BgL_auxz00_3106;

																								{	/* Object/slots.scm 335 */
																									obj_t BgL_arg1705z00_1166;

																									{	/* Object/slots.scm 335 */
																										obj_t BgL_arg1708z00_1167;

																										{	/* Object/slots.scm 335 */
																											obj_t BgL_classz00_1767;

																											BgL_classz00_1767 =
																												BGl_slotz00zzobject_slotsz00;
																											BgL_arg1708z00_1167 =
																												BGL_CLASS_ALL_FIELDS
																												(BgL_classz00_1767);
																										}
																										BgL_arg1705z00_1166 =
																											VECTOR_REF
																											(BgL_arg1708z00_1167, 7L);
																									}
																									BgL_auxz00_3106 =
																										BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
																										(BgL_arg1705z00_1166);
																								}
																								((((BgL_slotz00_bglt)
																											COBJECT
																											(BgL_new1104z00_1165))->
																										BgL_defaultzd2valuezd2) =
																									((obj_t) BgL_auxz00_3106),
																									BUNSPEC);
																							}
																							((((BgL_slotz00_bglt)
																										COBJECT
																										(BgL_new1104z00_1165))->
																									BgL_virtualzd2numzd2) =
																								((obj_t) BINT(-1L)), BUNSPEC);
																							((((BgL_slotz00_bglt)
																										COBJECT
																										(BgL_new1104z00_1165))->
																									BgL_virtualzd2overridezd2) =
																								((bool_t) ((bool_t) 0)),
																								BUNSPEC);
																							((((BgL_slotz00_bglt)
																										COBJECT
																										(BgL_new1104z00_1165))->
																									BgL_getterz00) =
																								((obj_t) BFALSE), BUNSPEC);
																							((((BgL_slotz00_bglt)
																										COBJECT
																										(BgL_new1104z00_1165))->
																									BgL_setterz00) =
																								((obj_t) BFALSE), BUNSPEC);
																							((((BgL_slotz00_bglt)
																										COBJECT
																										(BgL_new1104z00_1165))->
																									BgL_userzd2infozd2) =
																								((obj_t) BUNSPEC), BUNSPEC);
																							BgL_arg1699z00_1152 =
																								BgL_new1104z00_1165;
																			}}}}
																			BgL_arg1691z00_1150 =
																				MAKE_YOUNG_PAIR(
																				((obj_t) BgL_arg1699z00_1152),
																				BgL_resz00_1137);
																		}
																		BgL_arg1692z00_1151 =
																			(1L + BgL_indexz00_1138);
																		{
																			long BgL_indexz00_3122;
																			obj_t BgL_resz00_3121;
																			obj_t BgL_clausesz00_3120;

																			BgL_clausesz00_3120 = BgL_arg1689z00_1149;
																			BgL_resz00_3121 = BgL_arg1691z00_1150;
																			BgL_indexz00_3122 = BgL_arg1692z00_1151;
																			BgL_indexz00_1138 = BgL_indexz00_3122;
																			BgL_resz00_1137 = BgL_resz00_3121;
																			BgL_clausesz00_1136 = BgL_clausesz00_3120;
																			goto
																				BgL_zc3z04anonymousza31679ze3z87_1139;
																		}
																	}
															}
														}
													}
												}
											}
										}
									}
							}
						}
					}
				}
			}
		}

	}



/* &make-java-class-slots */
	obj_t BGl_z62makezd2javazd2classzd2slotszb0zzobject_slotsz00(obj_t
		BgL_envz00_2104, obj_t BgL_classz00_2105, obj_t BgL_clausesz00_2106,
		obj_t BgL_superz00_2107, obj_t BgL_srcz00_2108)
	{
		{	/* Object/slots.scm 323 */
			return
				BGl_makezd2javazd2classzd2slotszd2zzobject_slotsz00(
				((BgL_typez00_bglt) BgL_classz00_2105), BgL_clausesz00_2106,
				BgL_superz00_2107, BgL_srcz00_2108);
		}

	}



/* find-slot-type */
	obj_t BGl_findzd2slotzd2typez00zzobject_slotsz00(obj_t BgL_slotzd2idzd2_72,
		obj_t BgL_srcz00_73)
	{
		{	/* Object/slots.scm 386 */
			{	/* Object/slots.scm 387 */
				obj_t BgL_tz00_1197;

				{	/* Object/slots.scm 387 */
					bool_t BgL_test2215z00_3125;

					{	/* Object/slots.scm 387 */
						obj_t BgL_arg1740z00_1200;

						BgL_arg1740z00_1200 = CDR(((obj_t) BgL_slotzd2idzd2_72));
						BgL_test2215z00_3125 =
							(BgL_arg1740z00_1200 == BGl_za2_za2z00zztype_cachez00);
					}
					if (BgL_test2215z00_3125)
						{	/* Object/slots.scm 387 */
							BgL_tz00_1197 = BGl_za2objza2z00zztype_cachez00;
						}
					else
						{	/* Object/slots.scm 387 */
							BgL_tz00_1197 = CDR(((obj_t) BgL_slotzd2idzd2_72));
						}
				}
				BGl_ensurezd2typezd2definedz12z12zzobject_slotsz00(
					((BgL_typez00_bglt) BgL_tz00_1197), BgL_srcz00_73);
				return BgL_tz00_1197;
			}
		}

	}



/* scheme-symbol->c-string */
	obj_t BGl_schemezd2symbolzd2ze3czd2stringz31zzobject_slotsz00(obj_t
		BgL_symbolz00_74)
	{
		{	/* Object/slots.scm 394 */
			if (CBOOL(BGl_za2astzd2casezd2sensitiveza2z00zzengine_paramz00))
				{	/* Object/slots.scm 395 */
					BGL_TAIL return BGl_idzd2ze3namez31zzast_identz00(BgL_symbolz00_74);
				}
			else
				{	/* Object/slots.scm 395 */
					return
						BGl_stringzd2downcasezd2zz__r4_strings_6_7z00
						(BGl_idzd2ze3namez31zzast_identz00(BgL_symbolz00_74));
				}
		}

	}



/* get-local-virtual-slots-number */
	BGL_EXPORTED_DEF obj_t
		BGl_getzd2localzd2virtualzd2slotszd2numberz00zzobject_slotsz00
		(BgL_typez00_bglt BgL_classz00_75, obj_t BgL_slotsz00_76)
	{
		{	/* Object/slots.scm 411 */
			{
				obj_t BgL_slotsz00_1203;
				obj_t BgL_numz00_1204;

				{	/* Object/slots.scm 412 */
					long BgL_tmpz00_3138;

					BgL_slotsz00_1203 = BgL_slotsz00_76;
					BgL_numz00_1204 = BINT(-1L);
				BgL_zc3z04anonymousza31747ze3z87_1205:
					if (NULLP(BgL_slotsz00_1203))
						{	/* Object/slots.scm 415 */
							BgL_tmpz00_3138 = ((long) CINT(BgL_numz00_1204) + 1L);
						}
					else
						{	/* Object/slots.scm 415 */
							if (
								((long) CINT(
										(((BgL_slotz00_bglt) COBJECT(
													((BgL_slotz00_bglt)
														CAR(
															((obj_t) BgL_slotsz00_1203)))))->
											BgL_virtualzd2numzd2)) >= 0L))
								{	/* Object/slots.scm 418 */
									obj_t BgL_lnumz00_1209;

									BgL_lnumz00_1209 =
										(((BgL_slotz00_bglt) COBJECT(
												((BgL_slotz00_bglt)
													CAR(
														((obj_t) BgL_slotsz00_1203)))))->
										BgL_virtualzd2numzd2);
									{
										obj_t BgL_numz00_3157;
										obj_t BgL_slotsz00_3154;

										BgL_slotsz00_3154 = CDR(((obj_t) BgL_slotsz00_1203));
										if (
											((long) CINT(BgL_lnumz00_1209) >
												(long) CINT(BgL_numz00_1204)))
											{	/* Object/slots.scm 420 */
												BgL_numz00_3157 = BgL_lnumz00_1209;
											}
										else
											{	/* Object/slots.scm 420 */
												BgL_numz00_3157 = BgL_numz00_1204;
											}
										BgL_numz00_1204 = BgL_numz00_3157;
										BgL_slotsz00_1203 = BgL_slotsz00_3154;
										goto BgL_zc3z04anonymousza31747ze3z87_1205;
									}
								}
							else
								{
									obj_t BgL_slotsz00_3162;

									BgL_slotsz00_3162 = CDR(((obj_t) BgL_slotsz00_1203));
									BgL_slotsz00_1203 = BgL_slotsz00_3162;
									goto BgL_zc3z04anonymousza31747ze3z87_1205;
								}
						}
					return BINT(BgL_tmpz00_3138);
				}
			}
		}

	}



/* &get-local-virtual-slots-number */
	obj_t BGl_z62getzd2localzd2virtualzd2slotszd2numberz62zzobject_slotsz00(obj_t
		BgL_envz00_2109, obj_t BgL_classz00_2110, obj_t BgL_slotsz00_2111)
	{
		{	/* Object/slots.scm 411 */
			return
				BGl_getzd2localzd2virtualzd2slotszd2numberz00zzobject_slotsz00(
				((BgL_typez00_bglt) BgL_classz00_2110), BgL_slotsz00_2111);
		}

	}



/* make-class-make-formals */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2classzd2makezd2formalszd2zzobject_slotsz00(obj_t BgL_slotsz00_77)
	{
		{	/* Object/slots.scm 429 */
			{
				obj_t BgL_slotsz00_1219;
				obj_t BgL_formalsz00_1220;

				BgL_slotsz00_1219 = BgL_slotsz00_77;
				BgL_formalsz00_1220 = BNIL;
			BgL_zc3z04anonymousza31762ze3z87_1221:
				if (NULLP(BgL_slotsz00_1219))
					{	/* Object/slots.scm 433 */
						BGL_TAIL return bgl_reverse_bang(BgL_formalsz00_1220);
					}
				else
					{	/* Object/slots.scm 433 */
						if (
							((long) CINT(
									(((BgL_slotz00_bglt) COBJECT(
												((BgL_slotz00_bglt)
													CAR(
														((obj_t) BgL_slotsz00_1219)))))->
										BgL_virtualzd2numzd2)) >= 0L))
							{	/* Object/slots.scm 436 */
								obj_t BgL_arg1767z00_1225;

								BgL_arg1767z00_1225 = CDR(((obj_t) BgL_slotsz00_1219));
								{
									obj_t BgL_slotsz00_3181;

									BgL_slotsz00_3181 = BgL_arg1767z00_1225;
									BgL_slotsz00_1219 = BgL_slotsz00_3181;
									goto BgL_zc3z04anonymousza31762ze3z87_1221;
								}
							}
						else
							{	/* Object/slots.scm 438 */
								obj_t BgL_arg1770z00_1226;
								obj_t BgL_arg1771z00_1227;

								BgL_arg1770z00_1226 = CDR(((obj_t) BgL_slotsz00_1219));
								{	/* Object/slots.scm 439 */
									obj_t BgL_arg1773z00_1228;

									{	/* Object/slots.scm 439 */
										obj_t BgL_arg1775z00_1229;

										{	/* Object/slots.scm 439 */
											obj_t BgL_arg1798z00_1230;

											BgL_arg1798z00_1230 =
												(((BgL_slotz00_bglt) COBJECT(
														((BgL_slotz00_bglt)
															CAR(((obj_t) BgL_slotsz00_1219)))))->BgL_idz00);
											BgL_arg1775z00_1229 =
												BGl_gensymz00zz__r4_symbols_6_4z00(BgL_arg1798z00_1230);
										}
										BgL_arg1773z00_1228 =
											BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
											(BgL_arg1775z00_1229);
									}
									BgL_arg1771z00_1227 =
										MAKE_YOUNG_PAIR(BgL_arg1773z00_1228, BgL_formalsz00_1220);
								}
								{
									obj_t BgL_formalsz00_3192;
									obj_t BgL_slotsz00_3191;

									BgL_slotsz00_3191 = BgL_arg1770z00_1226;
									BgL_formalsz00_3192 = BgL_arg1771z00_1227;
									BgL_formalsz00_1220 = BgL_formalsz00_3192;
									BgL_slotsz00_1219 = BgL_slotsz00_3191;
									goto BgL_zc3z04anonymousza31762ze3z87_1221;
								}
							}
					}
			}
		}

	}



/* &make-class-make-formals */
	obj_t BGl_z62makezd2classzd2makezd2formalszb0zzobject_slotsz00(obj_t
		BgL_envz00_2112, obj_t BgL_slotsz00_2113)
	{
		{	/* Object/slots.scm 429 */
			return
				BGl_makezd2classzd2makezd2formalszd2zzobject_slotsz00
				(BgL_slotsz00_2113);
		}

	}



/* make-class-make-typed-formals */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2classzd2makezd2typedzd2formalsz00zzobject_slotsz00(obj_t
		BgL_idsz00_78, obj_t BgL_slotsz00_79)
	{
		{	/* Object/slots.scm 445 */
			{
				obj_t BgL_slotsz00_1236;
				obj_t BgL_idsz00_1237;
				obj_t BgL_formalsz00_1238;

				BgL_slotsz00_1236 = BgL_slotsz00_79;
				BgL_idsz00_1237 = BgL_idsz00_78;
				BgL_formalsz00_1238 = BNIL;
			BgL_zc3z04anonymousza31806ze3z87_1239:
				if (NULLP(BgL_slotsz00_1236))
					{	/* Object/slots.scm 450 */
						return bgl_reverse_bang(BgL_formalsz00_1238);
					}
				else
					{	/* Object/slots.scm 450 */
						if (
							((long) CINT(
									(((BgL_slotz00_bglt) COBJECT(
												((BgL_slotz00_bglt)
													CAR(
														((obj_t) BgL_slotsz00_1236)))))->
										BgL_virtualzd2numzd2)) >= 0L))
							{	/* Object/slots.scm 453 */
								obj_t BgL_arg1812z00_1243;

								BgL_arg1812z00_1243 = CDR(((obj_t) BgL_slotsz00_1236));
								{
									obj_t BgL_slotsz00_3206;

									BgL_slotsz00_3206 = BgL_arg1812z00_1243;
									BgL_slotsz00_1236 = BgL_slotsz00_3206;
									goto BgL_zc3z04anonymousza31806ze3z87_1239;
								}
							}
						else
							{	/* Object/slots.scm 455 */
								obj_t BgL_arg1820z00_1244;
								obj_t BgL_arg1822z00_1245;
								obj_t BgL_arg1823z00_1246;

								BgL_arg1820z00_1244 = CDR(((obj_t) BgL_slotsz00_1236));
								BgL_arg1822z00_1245 = CDR(((obj_t) BgL_idsz00_1237));
								{	/* Object/slots.scm 457 */
									obj_t BgL_arg1831z00_1247;

									{	/* Object/slots.scm 457 */
										obj_t BgL_arg1832z00_1248;
										obj_t BgL_arg1833z00_1249;

										{	/* Object/slots.scm 457 */
											obj_t BgL_arg1834z00_1250;

											BgL_arg1834z00_1250 = CAR(((obj_t) BgL_idsz00_1237));
											BgL_arg1832z00_1248 =
												BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
												(BgL_arg1834z00_1250);
										}
										BgL_arg1833z00_1249 =
											(((BgL_typez00_bglt) COBJECT(
													((BgL_typez00_bglt)
														(((BgL_slotz00_bglt) COBJECT(
																	((BgL_slotz00_bglt)
																		CAR(
																			((obj_t) BgL_slotsz00_1236)))))->
															BgL_typez00))))->BgL_idz00);
										BgL_arg1831z00_1247 =
											BGl_makezd2typedzd2identz00zzast_identz00
											(BgL_arg1832z00_1248, BgL_arg1833z00_1249);
									}
									BgL_arg1823z00_1246 =
										MAKE_YOUNG_PAIR(BgL_arg1831z00_1247, BgL_formalsz00_1238);
								}
								{
									obj_t BgL_formalsz00_3224;
									obj_t BgL_idsz00_3223;
									obj_t BgL_slotsz00_3222;

									BgL_slotsz00_3222 = BgL_arg1820z00_1244;
									BgL_idsz00_3223 = BgL_arg1822z00_1245;
									BgL_formalsz00_3224 = BgL_arg1823z00_1246;
									BgL_formalsz00_1238 = BgL_formalsz00_3224;
									BgL_idsz00_1237 = BgL_idsz00_3223;
									BgL_slotsz00_1236 = BgL_slotsz00_3222;
									goto BgL_zc3z04anonymousza31806ze3z87_1239;
								}
							}
					}
			}
		}

	}



/* &make-class-make-typed-formals */
	obj_t BGl_z62makezd2classzd2makezd2typedzd2formalsz62zzobject_slotsz00(obj_t
		BgL_envz00_2114, obj_t BgL_idsz00_2115, obj_t BgL_slotsz00_2116)
	{
		{	/* Object/slots.scm 445 */
			return
				BGl_makezd2classzd2makezd2typedzd2formalsz00zzobject_slotsz00
				(BgL_idsz00_2115, BgL_slotsz00_2116);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzobject_slotsz00(void)
	{
		{	/* Object/slots.scm 15 */
			{	/* Object/slots.scm 33 */
				obj_t BgL_arg1842z00_1258;
				obj_t BgL_arg1843z00_1259;

				{	/* Object/slots.scm 33 */
					obj_t BgL_v1168z00_1282;

					BgL_v1168z00_1282 = create_vector(13L);
					{	/* Object/slots.scm 33 */
						obj_t BgL_arg1849z00_1283;

						BgL_arg1849z00_1283 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(3),
							BGl_proc1993z00zzobject_slotsz00,
							BGl_proc1992z00zzobject_slotsz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(12));
						VECTOR_SET(BgL_v1168z00_1282, 0L, BgL_arg1849z00_1283);
					}
					{	/* Object/slots.scm 33 */
						obj_t BgL_arg1854z00_1293;

						BgL_arg1854z00_1293 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(13),
							BGl_proc1995z00zzobject_slotsz00,
							BGl_proc1994z00zzobject_slotsz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(14));
						VECTOR_SET(BgL_v1168z00_1282, 1L, BgL_arg1854z00_1293);
					}
					{	/* Object/slots.scm 33 */
						obj_t BgL_arg1860z00_1303;

						BgL_arg1860z00_1303 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(15),
							BGl_proc1997z00zzobject_slotsz00,
							BGl_proc1996z00zzobject_slotsz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(16));
						VECTOR_SET(BgL_v1168z00_1282, 2L, BgL_arg1860z00_1303);
					}
					{	/* Object/slots.scm 33 */
						obj_t BgL_arg1866z00_1313;

						BgL_arg1866z00_1313 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(17),
							BGl_proc1999z00zzobject_slotsz00,
							BGl_proc1998z00zzobject_slotsz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(16));
						VECTOR_SET(BgL_v1168z00_1282, 3L, BgL_arg1866z00_1313);
					}
					{	/* Object/slots.scm 33 */
						obj_t BgL_arg1872z00_1323;

						BgL_arg1872z00_1323 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(18),
							BGl_proc2001z00zzobject_slotsz00,
							BGl_proc2000z00zzobject_slotsz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(19));
						VECTOR_SET(BgL_v1168z00_1282, 4L, BgL_arg1872z00_1323);
					}
					{	/* Object/slots.scm 33 */
						obj_t BgL_arg1877z00_1333;

						BgL_arg1877z00_1333 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(20),
							BGl_proc2003z00zzobject_slotsz00,
							BGl_proc2002z00zzobject_slotsz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(16));
						VECTOR_SET(BgL_v1168z00_1282, 5L, BgL_arg1877z00_1333);
					}
					{	/* Object/slots.scm 33 */
						obj_t BgL_arg1882z00_1343;

						BgL_arg1882z00_1343 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(21),
							BGl_proc2006z00zzobject_slotsz00,
							BGl_proc2005z00zzobject_slotsz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BGl_proc2004z00zzobject_slotsz00, CNST_TABLE_REF(22));
						VECTOR_SET(BgL_v1168z00_1282, 6L, BgL_arg1882z00_1343);
					}
					{	/* Object/slots.scm 33 */
						obj_t BgL_arg1889z00_1356;

						BgL_arg1889z00_1356 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(23),
							BGl_proc2009z00zzobject_slotsz00,
							BGl_proc2008z00zzobject_slotsz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BGl_proc2007z00zzobject_slotsz00, CNST_TABLE_REF(16));
						VECTOR_SET(BgL_v1168z00_1282, 7L, BgL_arg1889z00_1356);
					}
					{	/* Object/slots.scm 33 */
						obj_t BgL_arg1896z00_1369;

						BgL_arg1896z00_1369 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(24),
							BGl_proc2012z00zzobject_slotsz00,
							BGl_proc2011z00zzobject_slotsz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2010z00zzobject_slotsz00, CNST_TABLE_REF(16));
						VECTOR_SET(BgL_v1168z00_1282, 8L, BgL_arg1896z00_1369);
					}
					{	/* Object/slots.scm 33 */
						obj_t BgL_arg1903z00_1382;

						BgL_arg1903z00_1382 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(25),
							BGl_proc2015z00zzobject_slotsz00,
							BGl_proc2014z00zzobject_slotsz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BGl_proc2013z00zzobject_slotsz00, CNST_TABLE_REF(22));
						VECTOR_SET(BgL_v1168z00_1282, 9L, BgL_arg1903z00_1382);
					}
					{	/* Object/slots.scm 33 */
						obj_t BgL_arg1914z00_1395;

						BgL_arg1914z00_1395 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(26),
							BGl_proc2018z00zzobject_slotsz00,
							BGl_proc2017z00zzobject_slotsz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2016z00zzobject_slotsz00, CNST_TABLE_REF(16));
						VECTOR_SET(BgL_v1168z00_1282, 10L, BgL_arg1914z00_1395);
					}
					{	/* Object/slots.scm 33 */
						obj_t BgL_arg1923z00_1408;

						BgL_arg1923z00_1408 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(27),
							BGl_proc2021z00zzobject_slotsz00,
							BGl_proc2020z00zzobject_slotsz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2019z00zzobject_slotsz00, CNST_TABLE_REF(16));
						VECTOR_SET(BgL_v1168z00_1282, 11L, BgL_arg1923z00_1408);
					}
					{	/* Object/slots.scm 33 */
						obj_t BgL_arg1930z00_1421;

						BgL_arg1930z00_1421 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(28),
							BGl_proc2024z00zzobject_slotsz00,
							BGl_proc2023z00zzobject_slotsz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BGl_proc2022z00zzobject_slotsz00, CNST_TABLE_REF(16));
						VECTOR_SET(BgL_v1168z00_1282, 12L, BgL_arg1930z00_1421);
					}
					BgL_arg1842z00_1258 = BgL_v1168z00_1282;
				}
				{	/* Object/slots.scm 33 */
					obj_t BgL_v1169z00_1434;

					BgL_v1169z00_1434 = create_vector(0L);
					BgL_arg1843z00_1259 = BgL_v1169z00_1434;
				}
				return (BGl_slotz00zzobject_slotsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(29),
						CNST_TABLE_REF(30), BGl_objectz00zz__objectz00, 48870L,
						BGl_proc2027z00zzobject_slotsz00, BGl_proc2026z00zzobject_slotsz00,
						BFALSE, BGl_proc2025z00zzobject_slotsz00, BFALSE,
						BgL_arg1842z00_1258, BgL_arg1843z00_1259), BUNSPEC);
			}
		}

	}



/* &<@anonymous:1848> */
	obj_t BGl_z62zc3z04anonymousza31848ze3ze5zzobject_slotsz00(obj_t
		BgL_envz00_2153, obj_t BgL_new1079z00_2154)
	{
		{	/* Object/slots.scm 33 */
			{
				BgL_slotz00_bglt BgL_auxz00_3283;

				((((BgL_slotz00_bglt) COBJECT(
								((BgL_slotz00_bglt) BgL_new1079z00_2154)))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(31)), BUNSPEC);
				((((BgL_slotz00_bglt) COBJECT(((BgL_slotz00_bglt)
									BgL_new1079z00_2154)))->BgL_namez00) =
					((obj_t) BGl_string2028z00zzobject_slotsz00), BUNSPEC);
				((((BgL_slotz00_bglt) COBJECT(((BgL_slotz00_bglt)
									BgL_new1079z00_2154)))->BgL_srcz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_slotz00_bglt) COBJECT(((BgL_slotz00_bglt)
									BgL_new1079z00_2154)))->BgL_classzd2ownerzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_slotz00_bglt) COBJECT(((BgL_slotz00_bglt)
									BgL_new1079z00_2154)))->BgL_indexz00) = ((long) 0L), BUNSPEC);
				((((BgL_slotz00_bglt) COBJECT(((BgL_slotz00_bglt)
									BgL_new1079z00_2154)))->BgL_typez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_slotz00_bglt) COBJECT(((BgL_slotz00_bglt)
									BgL_new1079z00_2154)))->BgL_readzd2onlyzf3z21) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_slotz00_bglt) COBJECT(((BgL_slotz00_bglt)
									BgL_new1079z00_2154)))->BgL_defaultzd2valuezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_slotz00_bglt) COBJECT(((BgL_slotz00_bglt)
									BgL_new1079z00_2154)))->BgL_virtualzd2numzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_slotz00_bglt) COBJECT(((BgL_slotz00_bglt)
									BgL_new1079z00_2154)))->BgL_virtualzd2overridezd2) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_slotz00_bglt) COBJECT(((BgL_slotz00_bglt)
									BgL_new1079z00_2154)))->BgL_getterz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_slotz00_bglt) COBJECT(((BgL_slotz00_bglt)
									BgL_new1079z00_2154)))->BgL_setterz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_slotz00_bglt) COBJECT(((BgL_slotz00_bglt)
									BgL_new1079z00_2154)))->BgL_userzd2infozd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_3283 = ((BgL_slotz00_bglt) BgL_new1079z00_2154);
				return ((obj_t) BgL_auxz00_3283);
			}
		}

	}



/* &lambda1846 */
	BgL_slotz00_bglt BGl_z62lambda1846z62zzobject_slotsz00(obj_t BgL_envz00_2155)
	{
		{	/* Object/slots.scm 33 */
			{	/* Object/slots.scm 33 */
				BgL_slotz00_bglt BgL_new1078z00_2281;

				BgL_new1078z00_2281 =
					((BgL_slotz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_slotz00_bgl))));
				{	/* Object/slots.scm 33 */
					long BgL_arg1847z00_2282;

					BgL_arg1847z00_2282 = BGL_CLASS_NUM(BGl_slotz00zzobject_slotsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1078z00_2281), BgL_arg1847z00_2282);
				}
				return BgL_new1078z00_2281;
			}
		}

	}



/* &lambda1844 */
	BgL_slotz00_bglt BGl_z62lambda1844z62zzobject_slotsz00(obj_t BgL_envz00_2156,
		obj_t BgL_id1065z00_2157, obj_t BgL_name1066z00_2158,
		obj_t BgL_src1067z00_2159, obj_t BgL_classzd2owner1068zd2_2160,
		obj_t BgL_index1069z00_2161, obj_t BgL_type1070z00_2162,
		obj_t BgL_readzd2onlyzf31071z21_2163, obj_t BgL_defaultzd2value1072zd2_2164,
		obj_t BgL_virtualzd2num1073zd2_2165,
		obj_t BgL_virtualzd2override1074zd2_2166, obj_t BgL_getter1075z00_2167,
		obj_t BgL_setter1076z00_2168, obj_t BgL_userzd2info1077zd2_2169)
	{
		{	/* Object/slots.scm 33 */
			{	/* Object/slots.scm 33 */
				long BgL_index1069z00_2285;
				bool_t BgL_readzd2onlyzf31071z21_2286;
				bool_t BgL_virtualzd2override1074zd2_2287;

				BgL_index1069z00_2285 = (long) CINT(BgL_index1069z00_2161);
				BgL_readzd2onlyzf31071z21_2286 = CBOOL(BgL_readzd2onlyzf31071z21_2163);
				BgL_virtualzd2override1074zd2_2287 =
					CBOOL(BgL_virtualzd2override1074zd2_2166);
				{	/* Object/slots.scm 33 */
					BgL_slotz00_bglt BgL_new1109z00_2288;

					{	/* Object/slots.scm 33 */
						BgL_slotz00_bglt BgL_new1108z00_2289;

						BgL_new1108z00_2289 =
							((BgL_slotz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_slotz00_bgl))));
						{	/* Object/slots.scm 33 */
							long BgL_arg1845z00_2290;

							BgL_arg1845z00_2290 = BGL_CLASS_NUM(BGl_slotz00zzobject_slotsz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1108z00_2289),
								BgL_arg1845z00_2290);
						}
						BgL_new1109z00_2288 = BgL_new1108z00_2289;
					}
					((((BgL_slotz00_bglt) COBJECT(BgL_new1109z00_2288))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1065z00_2157)), BUNSPEC);
					((((BgL_slotz00_bglt) COBJECT(BgL_new1109z00_2288))->BgL_namez00) =
						((obj_t) ((obj_t) BgL_name1066z00_2158)), BUNSPEC);
					((((BgL_slotz00_bglt) COBJECT(BgL_new1109z00_2288))->BgL_srcz00) =
						((obj_t) BgL_src1067z00_2159), BUNSPEC);
					((((BgL_slotz00_bglt) COBJECT(BgL_new1109z00_2288))->
							BgL_classzd2ownerzd2) =
						((obj_t) BgL_classzd2owner1068zd2_2160), BUNSPEC);
					((((BgL_slotz00_bglt) COBJECT(BgL_new1109z00_2288))->BgL_indexz00) =
						((long) BgL_index1069z00_2285), BUNSPEC);
					((((BgL_slotz00_bglt) COBJECT(BgL_new1109z00_2288))->BgL_typez00) =
						((obj_t) BgL_type1070z00_2162), BUNSPEC);
					((((BgL_slotz00_bglt) COBJECT(BgL_new1109z00_2288))->
							BgL_readzd2onlyzf3z21) =
						((bool_t) BgL_readzd2onlyzf31071z21_2286), BUNSPEC);
					((((BgL_slotz00_bglt) COBJECT(BgL_new1109z00_2288))->
							BgL_defaultzd2valuezd2) =
						((obj_t) BgL_defaultzd2value1072zd2_2164), BUNSPEC);
					((((BgL_slotz00_bglt) COBJECT(BgL_new1109z00_2288))->
							BgL_virtualzd2numzd2) =
						((obj_t) BgL_virtualzd2num1073zd2_2165), BUNSPEC);
					((((BgL_slotz00_bglt) COBJECT(BgL_new1109z00_2288))->
							BgL_virtualzd2overridezd2) =
						((bool_t) BgL_virtualzd2override1074zd2_2287), BUNSPEC);
					((((BgL_slotz00_bglt) COBJECT(BgL_new1109z00_2288))->BgL_getterz00) =
						((obj_t) BgL_getter1075z00_2167), BUNSPEC);
					((((BgL_slotz00_bglt) COBJECT(BgL_new1109z00_2288))->BgL_setterz00) =
						((obj_t) BgL_setter1076z00_2168), BUNSPEC);
					((((BgL_slotz00_bglt) COBJECT(BgL_new1109z00_2288))->
							BgL_userzd2infozd2) =
						((obj_t) BgL_userzd2info1077zd2_2169), BUNSPEC);
					return BgL_new1109z00_2288;
				}
			}
		}

	}



/* &<@anonymous:1936> */
	obj_t BGl_z62zc3z04anonymousza31936ze3ze5zzobject_slotsz00(obj_t
		BgL_envz00_2170)
	{
		{	/* Object/slots.scm 33 */
			return BUNSPEC;
		}

	}



/* &lambda1935 */
	obj_t BGl_z62lambda1935z62zzobject_slotsz00(obj_t BgL_envz00_2171,
		obj_t BgL_oz00_2172, obj_t BgL_vz00_2173)
	{
		{	/* Object/slots.scm 33 */
			return
				((((BgL_slotz00_bglt) COBJECT(
							((BgL_slotz00_bglt) BgL_oz00_2172)))->BgL_userzd2infozd2) =
				((obj_t) BgL_vz00_2173), BUNSPEC);
		}

	}



/* &lambda1934 */
	obj_t BGl_z62lambda1934z62zzobject_slotsz00(obj_t BgL_envz00_2174,
		obj_t BgL_oz00_2175)
	{
		{	/* Object/slots.scm 33 */
			return
				(((BgL_slotz00_bglt) COBJECT(
						((BgL_slotz00_bglt) BgL_oz00_2175)))->BgL_userzd2infozd2);
		}

	}



/* &<@anonymous:1929> */
	obj_t BGl_z62zc3z04anonymousza31929ze3ze5zzobject_slotsz00(obj_t
		BgL_envz00_2176)
	{
		{	/* Object/slots.scm 33 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1928 */
	obj_t BGl_z62lambda1928z62zzobject_slotsz00(obj_t BgL_envz00_2177,
		obj_t BgL_oz00_2178, obj_t BgL_vz00_2179)
	{
		{	/* Object/slots.scm 33 */
			return
				((((BgL_slotz00_bglt) COBJECT(
							((BgL_slotz00_bglt) BgL_oz00_2178)))->BgL_setterz00) =
				((obj_t) BgL_vz00_2179), BUNSPEC);
		}

	}



/* &lambda1927 */
	obj_t BGl_z62lambda1927z62zzobject_slotsz00(obj_t BgL_envz00_2180,
		obj_t BgL_oz00_2181)
	{
		{	/* Object/slots.scm 33 */
			return
				(((BgL_slotz00_bglt) COBJECT(
						((BgL_slotz00_bglt) BgL_oz00_2181)))->BgL_setterz00);
		}

	}



/* &<@anonymous:1921> */
	obj_t BGl_z62zc3z04anonymousza31921ze3ze5zzobject_slotsz00(obj_t
		BgL_envz00_2182)
	{
		{	/* Object/slots.scm 33 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1920 */
	obj_t BGl_z62lambda1920z62zzobject_slotsz00(obj_t BgL_envz00_2183,
		obj_t BgL_oz00_2184, obj_t BgL_vz00_2185)
	{
		{	/* Object/slots.scm 33 */
			return
				((((BgL_slotz00_bglt) COBJECT(
							((BgL_slotz00_bglt) BgL_oz00_2184)))->BgL_getterz00) =
				((obj_t) BgL_vz00_2185), BUNSPEC);
		}

	}



/* &lambda1919 */
	obj_t BGl_z62lambda1919z62zzobject_slotsz00(obj_t BgL_envz00_2186,
		obj_t BgL_oz00_2187)
	{
		{	/* Object/slots.scm 33 */
			return
				(((BgL_slotz00_bglt) COBJECT(
						((BgL_slotz00_bglt) BgL_oz00_2187)))->BgL_getterz00);
		}

	}



/* &<@anonymous:1913> */
	obj_t BGl_z62zc3z04anonymousza31913ze3ze5zzobject_slotsz00(obj_t
		BgL_envz00_2188)
	{
		{	/* Object/slots.scm 33 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1912 */
	obj_t BGl_z62lambda1912z62zzobject_slotsz00(obj_t BgL_envz00_2189,
		obj_t BgL_oz00_2190, obj_t BgL_vz00_2191)
	{
		{	/* Object/slots.scm 33 */
			{	/* Object/slots.scm 33 */
				bool_t BgL_vz00_2298;

				BgL_vz00_2298 = CBOOL(BgL_vz00_2191);
				return
					((((BgL_slotz00_bglt) COBJECT(
								((BgL_slotz00_bglt) BgL_oz00_2190)))->
						BgL_virtualzd2overridezd2) = ((bool_t) BgL_vz00_2298), BUNSPEC);
			}
		}

	}



/* &lambda1911 */
	obj_t BGl_z62lambda1911z62zzobject_slotsz00(obj_t BgL_envz00_2192,
		obj_t BgL_oz00_2193)
	{
		{	/* Object/slots.scm 33 */
			return
				BBOOL(
				(((BgL_slotz00_bglt) COBJECT(
							((BgL_slotz00_bglt) BgL_oz00_2193)))->BgL_virtualzd2overridezd2));
		}

	}



/* &<@anonymous:1902> */
	obj_t BGl_z62zc3z04anonymousza31902ze3ze5zzobject_slotsz00(obj_t
		BgL_envz00_2194)
	{
		{	/* Object/slots.scm 33 */
			return BINT(-1L);
		}

	}



/* &lambda1901 */
	obj_t BGl_z62lambda1901z62zzobject_slotsz00(obj_t BgL_envz00_2195,
		obj_t BgL_oz00_2196, obj_t BgL_vz00_2197)
	{
		{	/* Object/slots.scm 33 */
			return
				((((BgL_slotz00_bglt) COBJECT(
							((BgL_slotz00_bglt) BgL_oz00_2196)))->BgL_virtualzd2numzd2) =
				((obj_t) BgL_vz00_2197), BUNSPEC);
		}

	}



/* &lambda1900 */
	obj_t BGl_z62lambda1900z62zzobject_slotsz00(obj_t BgL_envz00_2198,
		obj_t BgL_oz00_2199)
	{
		{	/* Object/slots.scm 33 */
			return
				(((BgL_slotz00_bglt) COBJECT(
						((BgL_slotz00_bglt) BgL_oz00_2199)))->BgL_virtualzd2numzd2);
		}

	}



/* &<@anonymous:1895> */
	obj_t BGl_z62zc3z04anonymousza31895ze3ze5zzobject_slotsz00(obj_t
		BgL_envz00_2200)
	{
		{	/* Object/slots.scm 33 */
			return MAKE_YOUNG_PAIR(BINT(1L), BINT(2L));
		}

	}



/* &lambda1894 */
	obj_t BGl_z62lambda1894z62zzobject_slotsz00(obj_t BgL_envz00_2201,
		obj_t BgL_oz00_2202, obj_t BgL_vz00_2203)
	{
		{	/* Object/slots.scm 33 */
			return
				((((BgL_slotz00_bglt) COBJECT(
							((BgL_slotz00_bglt) BgL_oz00_2202)))->BgL_defaultzd2valuezd2) =
				((obj_t) BgL_vz00_2203), BUNSPEC);
		}

	}



/* &lambda1893 */
	obj_t BGl_z62lambda1893z62zzobject_slotsz00(obj_t BgL_envz00_2204,
		obj_t BgL_oz00_2205)
	{
		{	/* Object/slots.scm 33 */
			return
				(((BgL_slotz00_bglt) COBJECT(
						((BgL_slotz00_bglt) BgL_oz00_2205)))->BgL_defaultzd2valuezd2);
		}

	}



/* &<@anonymous:1888> */
	obj_t BGl_z62zc3z04anonymousza31888ze3ze5zzobject_slotsz00(obj_t
		BgL_envz00_2206)
	{
		{	/* Object/slots.scm 33 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1887 */
	obj_t BGl_z62lambda1887z62zzobject_slotsz00(obj_t BgL_envz00_2207,
		obj_t BgL_oz00_2208, obj_t BgL_vz00_2209)
	{
		{	/* Object/slots.scm 33 */
			{	/* Object/slots.scm 33 */
				bool_t BgL_vz00_2305;

				BgL_vz00_2305 = CBOOL(BgL_vz00_2209);
				return
					((((BgL_slotz00_bglt) COBJECT(
								((BgL_slotz00_bglt) BgL_oz00_2208)))->BgL_readzd2onlyzf3z21) =
					((bool_t) BgL_vz00_2305), BUNSPEC);
			}
		}

	}



/* &lambda1886 */
	obj_t BGl_z62lambda1886z62zzobject_slotsz00(obj_t BgL_envz00_2210,
		obj_t BgL_oz00_2211)
	{
		{	/* Object/slots.scm 33 */
			return
				BBOOL(
				(((BgL_slotz00_bglt) COBJECT(
							((BgL_slotz00_bglt) BgL_oz00_2211)))->BgL_readzd2onlyzf3z21));
		}

	}



/* &lambda1881 */
	obj_t BGl_z62lambda1881z62zzobject_slotsz00(obj_t BgL_envz00_2212,
		obj_t BgL_oz00_2213, obj_t BgL_vz00_2214)
	{
		{	/* Object/slots.scm 33 */
			return
				((((BgL_slotz00_bglt) COBJECT(
							((BgL_slotz00_bglt) BgL_oz00_2213)))->BgL_typez00) =
				((obj_t) BgL_vz00_2214), BUNSPEC);
		}

	}



/* &lambda1880 */
	obj_t BGl_z62lambda1880z62zzobject_slotsz00(obj_t BgL_envz00_2215,
		obj_t BgL_oz00_2216)
	{
		{	/* Object/slots.scm 33 */
			return
				(((BgL_slotz00_bglt) COBJECT(
						((BgL_slotz00_bglt) BgL_oz00_2216)))->BgL_typez00);
		}

	}



/* &lambda1876 */
	obj_t BGl_z62lambda1876z62zzobject_slotsz00(obj_t BgL_envz00_2217,
		obj_t BgL_oz00_2218, obj_t BgL_vz00_2219)
	{
		{	/* Object/slots.scm 33 */
			{	/* Object/slots.scm 33 */
				long BgL_vz00_2310;

				BgL_vz00_2310 = (long) CINT(BgL_vz00_2219);
				return
					((((BgL_slotz00_bglt) COBJECT(
								((BgL_slotz00_bglt) BgL_oz00_2218)))->BgL_indexz00) =
					((long) BgL_vz00_2310), BUNSPEC);
		}}

	}



/* &lambda1875 */
	obj_t BGl_z62lambda1875z62zzobject_slotsz00(obj_t BgL_envz00_2220,
		obj_t BgL_oz00_2221)
	{
		{	/* Object/slots.scm 33 */
			return
				BINT(
				(((BgL_slotz00_bglt) COBJECT(
							((BgL_slotz00_bglt) BgL_oz00_2221)))->BgL_indexz00));
		}

	}



/* &lambda1871 */
	obj_t BGl_z62lambda1871z62zzobject_slotsz00(obj_t BgL_envz00_2222,
		obj_t BgL_oz00_2223, obj_t BgL_vz00_2224)
	{
		{	/* Object/slots.scm 33 */
			return
				((((BgL_slotz00_bglt) COBJECT(
							((BgL_slotz00_bglt) BgL_oz00_2223)))->BgL_classzd2ownerzd2) =
				((obj_t) BgL_vz00_2224), BUNSPEC);
		}

	}



/* &lambda1870 */
	obj_t BGl_z62lambda1870z62zzobject_slotsz00(obj_t BgL_envz00_2225,
		obj_t BgL_oz00_2226)
	{
		{	/* Object/slots.scm 33 */
			return
				(((BgL_slotz00_bglt) COBJECT(
						((BgL_slotz00_bglt) BgL_oz00_2226)))->BgL_classzd2ownerzd2);
		}

	}



/* &lambda1865 */
	obj_t BGl_z62lambda1865z62zzobject_slotsz00(obj_t BgL_envz00_2227,
		obj_t BgL_oz00_2228, obj_t BgL_vz00_2229)
	{
		{	/* Object/slots.scm 33 */
			return
				((((BgL_slotz00_bglt) COBJECT(
							((BgL_slotz00_bglt) BgL_oz00_2228)))->BgL_srcz00) =
				((obj_t) BgL_vz00_2229), BUNSPEC);
		}

	}



/* &lambda1864 */
	obj_t BGl_z62lambda1864z62zzobject_slotsz00(obj_t BgL_envz00_2230,
		obj_t BgL_oz00_2231)
	{
		{	/* Object/slots.scm 33 */
			return
				(((BgL_slotz00_bglt) COBJECT(
						((BgL_slotz00_bglt) BgL_oz00_2231)))->BgL_srcz00);
		}

	}



/* &lambda1859 */
	obj_t BGl_z62lambda1859z62zzobject_slotsz00(obj_t BgL_envz00_2232,
		obj_t BgL_oz00_2233, obj_t BgL_vz00_2234)
	{
		{	/* Object/slots.scm 33 */
			return
				((((BgL_slotz00_bglt) COBJECT(
							((BgL_slotz00_bglt) BgL_oz00_2233)))->BgL_namez00) = ((obj_t)
					((obj_t) BgL_vz00_2234)), BUNSPEC);
		}

	}



/* &lambda1858 */
	obj_t BGl_z62lambda1858z62zzobject_slotsz00(obj_t BgL_envz00_2235,
		obj_t BgL_oz00_2236)
	{
		{	/* Object/slots.scm 33 */
			return
				(((BgL_slotz00_bglt) COBJECT(
						((BgL_slotz00_bglt) BgL_oz00_2236)))->BgL_namez00);
		}

	}



/* &lambda1853 */
	obj_t BGl_z62lambda1853z62zzobject_slotsz00(obj_t BgL_envz00_2237,
		obj_t BgL_oz00_2238, obj_t BgL_vz00_2239)
	{
		{	/* Object/slots.scm 33 */
			return
				((((BgL_slotz00_bglt) COBJECT(
							((BgL_slotz00_bglt) BgL_oz00_2238)))->BgL_idz00) = ((obj_t)
					((obj_t) BgL_vz00_2239)), BUNSPEC);
		}

	}



/* &lambda1852 */
	obj_t BGl_z62lambda1852z62zzobject_slotsz00(obj_t BgL_envz00_2240,
		obj_t BgL_oz00_2241)
	{
		{	/* Object/slots.scm 33 */
			return
				(((BgL_slotz00_bglt) COBJECT(
						((BgL_slotz00_bglt) BgL_oz00_2241)))->BgL_idz00);
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzobject_slotsz00(void)
	{
		{	/* Object/slots.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzobject_slotsz00(void)
	{
		{	/* Object/slots.scm 15 */
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shapezd2envzd2zztools_shapez00, BGl_slotz00zzobject_slotsz00,
				BGl_proc2029z00zzobject_slotsz00, BGl_string2030z00zzobject_slotsz00);
		}

	}



/* &shape-slot1171 */
	obj_t BGl_z62shapezd2slot1171zb0zzobject_slotsz00(obj_t BgL_envz00_2243,
		obj_t BgL_sz00_2244)
	{
		{	/* Object/slots.scm 86 */
			{	/* Object/slots.scm 88 */
				obj_t BgL_arg1937z00_2323;
				obj_t BgL_arg1938z00_2324;
				obj_t BgL_arg1939z00_2325;
				obj_t BgL_arg1940z00_2326;

				{	/* Object/slots.scm 88 */
					obj_t BgL_arg1951z00_2327;

					BgL_arg1951z00_2327 =
						(((BgL_slotz00_bglt) COBJECT(
								((BgL_slotz00_bglt) BgL_sz00_2244)))->BgL_idz00);
					{	/* Object/slots.scm 88 */
						obj_t BgL_arg1455z00_2328;

						BgL_arg1455z00_2328 = SYMBOL_TO_STRING(BgL_arg1951z00_2327);
						BgL_arg1937z00_2323 =
							BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_2328);
					}
				}
				BgL_arg1938z00_2324 =
					(((BgL_slotz00_bglt) COBJECT(
							((BgL_slotz00_bglt) BgL_sz00_2244)))->BgL_namez00);
				BgL_arg1939z00_2325 =
					BGl_shapez00zztools_shapez00(
					(((BgL_slotz00_bglt) COBJECT(
								((BgL_slotz00_bglt) BgL_sz00_2244)))->BgL_classzd2ownerzd2));
				BgL_arg1940z00_2326 =
					BGl_shapez00zztools_shapez00(
					(((BgL_slotz00_bglt) COBJECT(
								((BgL_slotz00_bglt) BgL_sz00_2244)))->BgL_typez00));
				{	/* Object/slots.scm 88 */
					obj_t BgL_list1941z00_2329;

					{	/* Object/slots.scm 88 */
						obj_t BgL_arg1942z00_2330;

						{	/* Object/slots.scm 88 */
							obj_t BgL_arg1943z00_2331;

							{	/* Object/slots.scm 88 */
								obj_t BgL_arg1944z00_2332;

								{	/* Object/slots.scm 88 */
									obj_t BgL_arg1945z00_2333;

									{	/* Object/slots.scm 88 */
										obj_t BgL_arg1946z00_2334;

										{	/* Object/slots.scm 88 */
											obj_t BgL_arg1947z00_2335;

											{	/* Object/slots.scm 88 */
												obj_t BgL_arg1948z00_2336;

												{	/* Object/slots.scm 88 */
													obj_t BgL_arg1949z00_2337;

													{	/* Object/slots.scm 88 */
														obj_t BgL_arg1950z00_2338;

														BgL_arg1950z00_2338 =
															MAKE_YOUNG_PAIR
															(BGl_string2031z00zzobject_slotsz00, BNIL);
														BgL_arg1949z00_2337 =
															MAKE_YOUNG_PAIR(BgL_arg1940z00_2326,
															BgL_arg1950z00_2338);
													}
													BgL_arg1948z00_2336 =
														MAKE_YOUNG_PAIR(BGl_string2032z00zzobject_slotsz00,
														BgL_arg1949z00_2337);
												}
												BgL_arg1947z00_2335 =
													MAKE_YOUNG_PAIR(BgL_arg1939z00_2325,
													BgL_arg1948z00_2336);
											}
											BgL_arg1946z00_2334 =
												MAKE_YOUNG_PAIR(BGl_string2033z00zzobject_slotsz00,
												BgL_arg1947z00_2335);
										}
										BgL_arg1945z00_2333 =
											MAKE_YOUNG_PAIR(BGl_string2031z00zzobject_slotsz00,
											BgL_arg1946z00_2334);
									}
									BgL_arg1944z00_2332 =
										MAKE_YOUNG_PAIR(BgL_arg1938z00_2324, BgL_arg1945z00_2333);
								}
								BgL_arg1943z00_2331 =
									MAKE_YOUNG_PAIR(BGl_string2034z00zzobject_slotsz00,
									BgL_arg1944z00_2332);
							}
							BgL_arg1942z00_2330 =
								MAKE_YOUNG_PAIR(BgL_arg1937z00_2323, BgL_arg1943z00_2331);
						}
						BgL_list1941z00_2329 =
							MAKE_YOUNG_PAIR(BGl_string2035z00zzobject_slotsz00,
							BgL_arg1942z00_2330);
					}
					return
						BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list1941z00_2329);
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzobject_slotsz00(void)
	{
		{	/* Object/slots.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2036z00zzobject_slotsz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2036z00zzobject_slotsz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string2036z00zzobject_slotsz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2036z00zzobject_slotsz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2036z00zzobject_slotsz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2036z00zzobject_slotsz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string2036z00zzobject_slotsz00));
			BGl_modulezd2initializa7ationz75zztvector_tvectorz00(501518119L,
				BSTRING_TO_STRING(BGl_string2036z00zzobject_slotsz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2036z00zzobject_slotsz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string2036z00zzobject_slotsz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2036z00zzobject_slotsz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string2036z00zzobject_slotsz00));
			return
				BGl_modulezd2initializa7ationz75zzast_glozd2declzd2(374700232L,
				BSTRING_TO_STRING(BGl_string2036z00zzobject_slotsz00));
		}

	}

#ifdef __cplusplus
}
#endif
