/*===========================================================================*/
/*   (Object/coercion.scm)                                                   */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Object/coercion.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_OBJECT_COERCION_TYPE_DEFINITIONS
#define BGL_OBJECT_COERCION_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_tclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		struct BgL_globalz00_bgl *BgL_holderz00;
		obj_t BgL_wideningz00;
		long BgL_depthz00;
		bool_t BgL_finalzf3zf3;
		obj_t BgL_constructorz00;
		obj_t BgL_virtualzd2slotszd2numberz00;
		bool_t BgL_abstractzf3zf3;
		obj_t BgL_widezd2typezd2;
		obj_t BgL_subclassesz00;
	}                *BgL_tclassz00_bglt;

	typedef struct BgL_jclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		obj_t BgL_packagez00;
	}                *BgL_jclassz00_bglt;


#endif													// BGL_OBJECT_COERCION_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzobject_coercionz00 = BUNSPEC;
	extern obj_t BGl_makezd2typedzd2identz00zzast_identz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_genzd2javazd2classzd2coercionsz12zc0zzobject_coercionz00(obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_tclassz00zzobject_classz00;
	static obj_t BGl_z62genzd2coercionzd2clausez12z70zzobject_coercionz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_genericzd2initzd2zzobject_coercionz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_genzd2coercionzd2clausez12z12zzobject_coercionz00(BgL_typez00_bglt,
		obj_t, obj_t, obj_t);
	static obj_t
		BGl_makezd2coercionzd2clausez00zzobject_coercionz00(BgL_typez00_bglt, obj_t,
		obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzobject_coercionz00(void);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzobject_coercionz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzobject_coercionz00(void);
	static obj_t BGl_makezd2onezd2coercionze70ze7zzobject_coercionz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62genzd2classzd2coercionsz12z70zzobject_coercionz00(obj_t,
		obj_t);
	static obj_t BGl_z62genzd2classzd2coercersz12z70zzobject_coercionz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_genzd2classzd2coercionsz12z12zzobject_coercionz00(obj_t);
	extern obj_t BGl_jclassz00zzobject_classz00;
	extern obj_t BGl_producezd2modulezd2clausez12z12zzmodule_modulez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzobject_coercionz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_privatez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_impusez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_makezd2privatezd2sexpz00zzast_privatez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zzobject_coercionz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzobject_coercionz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzobject_coercionz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzobject_coercionz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_genzd2classzd2coercersz12z12zzobject_coercionz00(obj_t, obj_t);
	static bool_t BGl_czd2genzd2classzd2coercersz12zc0zzobject_coercionz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t
		BGl_z62genzd2javazd2classzd2coercionsz12za2zzobject_coercionz00(obj_t,
		obj_t);
	static obj_t __cnst[21];


	   
		 
		DEFINE_STRING(BGl_string1614z00zzobject_coercionz00,
		BgL_bgl_string1614za700za7za7o1626za7, "obj_t", 5);
	      DEFINE_STRING(BGl_string1615z00zzobject_coercionz00,
		BgL_bgl_string1615za700za7za7o1627za7, "(", 1);
	      DEFINE_STRING(BGl_string1616z00zzobject_coercionz00,
		BgL_bgl_string1616za700za7za7o1628za7, ")", 1);
	      DEFINE_STRING(BGl_string1617z00zzobject_coercionz00,
		BgL_bgl_string1617za700za7za7o1629za7, "object_coercion", 15);
	      DEFINE_STRING(BGl_string1618z00zzobject_coercionz00,
		BgL_bgl_string1618za700za7za7o1630za7,
		"macro pragma nesting args-safe side-effect-free no-cfa-top effect -> foreign type bool coerce @ isa? __object instanceof o lambda obj cast x ",
		141);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genzd2classzd2coercionsz12zd2envzc0zzobject_coercionz00,
		BgL_bgl_za762genza7d2classza7d1631za7,
		BGl_z62genzd2classzd2coercionsz12z70zzobject_coercionz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genzd2coercionzd2clausez12zd2envzc0zzobject_coercionz00,
		BgL_bgl_za762genza7d2coercio1632z00, va_generic_entry,
		BGl_z62genzd2coercionzd2clausez12z70zzobject_coercionz00, BUNSPEC, -4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genzd2classzd2coercersz12zd2envzc0zzobject_coercionz00,
		BgL_bgl_za762genza7d2classza7d1633za7,
		BGl_z62genzd2classzd2coercersz12z70zzobject_coercionz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genzd2javazd2classzd2coercionsz12zd2envz12zzobject_coercionz00,
		BgL_bgl_za762genza7d2javaza7d21634za7,
		BGl_z62genzd2javazd2classzd2coercionsz12za2zzobject_coercionz00, 0L,
		BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzobject_coercionz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzobject_coercionz00(long
		BgL_checksumz00_1413, char *BgL_fromz00_1414)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzobject_coercionz00))
				{
					BGl_requirezd2initializa7ationz75zzobject_coercionz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzobject_coercionz00();
					BGl_libraryzd2moduleszd2initz00zzobject_coercionz00();
					BGl_cnstzd2initzd2zzobject_coercionz00();
					BGl_importedzd2moduleszd2initz00zzobject_coercionz00();
					return BGl_methodzd2initzd2zzobject_coercionz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzobject_coercionz00(void)
	{
		{	/* Object/coercion.scm 21 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"object_coercion");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "object_coercion");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"object_coercion");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "object_coercion");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"object_coercion");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"object_coercion");
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(0L,
				"object_coercion");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"object_coercion");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"object_coercion");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"object_coercion");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzobject_coercionz00(void)
	{
		{	/* Object/coercion.scm 21 */
			{	/* Object/coercion.scm 21 */
				obj_t BgL_cportz00_1402;

				{	/* Object/coercion.scm 21 */
					obj_t BgL_stringz00_1409;

					BgL_stringz00_1409 = BGl_string1618z00zzobject_coercionz00;
					{	/* Object/coercion.scm 21 */
						obj_t BgL_startz00_1410;

						BgL_startz00_1410 = BINT(0L);
						{	/* Object/coercion.scm 21 */
							obj_t BgL_endz00_1411;

							BgL_endz00_1411 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1409)));
							{	/* Object/coercion.scm 21 */

								BgL_cportz00_1402 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1409, BgL_startz00_1410, BgL_endz00_1411);
				}}}}
				{
					long BgL_iz00_1403;

					BgL_iz00_1403 = 20L;
				BgL_loopz00_1404:
					if ((BgL_iz00_1403 == -1L))
						{	/* Object/coercion.scm 21 */
							return BUNSPEC;
						}
					else
						{	/* Object/coercion.scm 21 */
							{	/* Object/coercion.scm 21 */
								obj_t BgL_arg1625z00_1405;

								{	/* Object/coercion.scm 21 */

									{	/* Object/coercion.scm 21 */
										obj_t BgL_locationz00_1407;

										BgL_locationz00_1407 = BBOOL(((bool_t) 0));
										{	/* Object/coercion.scm 21 */

											BgL_arg1625z00_1405 =
												BGl_readz00zz__readerz00(BgL_cportz00_1402,
												BgL_locationz00_1407);
										}
									}
								}
								{	/* Object/coercion.scm 21 */
									int BgL_tmpz00_1442;

									BgL_tmpz00_1442 = (int) (BgL_iz00_1403);
									CNST_TABLE_SET(BgL_tmpz00_1442, BgL_arg1625z00_1405);
							}}
							{	/* Object/coercion.scm 21 */
								int BgL_auxz00_1408;

								BgL_auxz00_1408 = (int) ((BgL_iz00_1403 - 1L));
								{
									long BgL_iz00_1447;

									BgL_iz00_1447 = (long) (BgL_auxz00_1408);
									BgL_iz00_1403 = BgL_iz00_1447;
									goto BgL_loopz00_1404;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzobject_coercionz00(void)
	{
		{	/* Object/coercion.scm 21 */
			return bgl_gc_roots_register();
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzobject_coercionz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_812;

				BgL_headz00_812 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_813;
					obj_t BgL_tailz00_814;

					BgL_prevz00_813 = BgL_headz00_812;
					BgL_tailz00_814 = BgL_l1z00_1;
				BgL_loopz00_815:
					if (PAIRP(BgL_tailz00_814))
						{
							obj_t BgL_newzd2prevzd2_817;

							BgL_newzd2prevzd2_817 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_814), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_813, BgL_newzd2prevzd2_817);
							{
								obj_t BgL_tailz00_1457;
								obj_t BgL_prevz00_1456;

								BgL_prevz00_1456 = BgL_newzd2prevzd2_817;
								BgL_tailz00_1457 = CDR(BgL_tailz00_814);
								BgL_tailz00_814 = BgL_tailz00_1457;
								BgL_prevz00_813 = BgL_prevz00_1456;
								goto BgL_loopz00_815;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_812);
				}
			}
		}

	}



/* gen-class-coercions! */
	BGL_EXPORTED_DEF obj_t
		BGl_genzd2classzd2coercionsz12z12zzobject_coercionz00(obj_t BgL_classz00_3)
	{
		{	/* Object/coercion.scm 48 */
			{	/* Object/coercion.scm 50 */
				obj_t BgL_arg1127z00_821;
				obj_t BgL_arg1129z00_822;

				BgL_arg1127z00_821 =
					(((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt)
								((BgL_typez00_bglt) BgL_classz00_3))))->BgL_idz00);
				{
					BgL_tclassz00_bglt BgL_auxz00_1463;

					{
						obj_t BgL_auxz00_1464;

						{	/* Object/coercion.scm 50 */
							BgL_objectz00_bglt BgL_tmpz00_1465;

							BgL_tmpz00_1465 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_classz00_3));
							BgL_auxz00_1464 = BGL_OBJECT_WIDENING(BgL_tmpz00_1465);
						}
						BgL_auxz00_1463 = ((BgL_tclassz00_bglt) BgL_auxz00_1464);
					}
					BgL_arg1129z00_822 =
						(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_1463))->
						BgL_itszd2superzd2);
				}
				{	/* Object/coercion.scm 74 */
					obj_t BgL_arg1141z00_1080;

					BgL_arg1141z00_1080 =
						BGl_makezd2coercionzd2clausez00zzobject_coercionz00(
						((BgL_typez00_bglt) BgL_classz00_3), BgL_arg1127z00_821,
						BgL_arg1129z00_822, BNIL);
					BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
						(BgL_arg1141z00_1080);
				}
			}
			{	/* Object/coercion.scm 51 */
				obj_t BgL_arg1131z00_824;

				{
					BgL_tclassz00_bglt BgL_auxz00_1474;

					{
						obj_t BgL_auxz00_1475;

						{	/* Object/coercion.scm 51 */
							BgL_objectz00_bglt BgL_tmpz00_1476;

							BgL_tmpz00_1476 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_classz00_3));
							BgL_auxz00_1475 = BGL_OBJECT_WIDENING(BgL_tmpz00_1476);
						}
						BgL_auxz00_1474 = ((BgL_tclassz00_bglt) BgL_auxz00_1475);
					}
					BgL_arg1131z00_824 =
						(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_1474))->
						BgL_itszd2superzd2);
				}
				{	/* Object/coercion.scm 128 */
					bool_t BgL_test1638z00_1482;

					{	/* Object/coercion.scm 128 */
						obj_t BgL_arg1325z00_1083;

						BgL_arg1325z00_1083 = BGl_thezd2backendzd2zzbackend_backendz00();
						BgL_test1638z00_1482 =
							(((BgL_backendz00_bglt) COBJECT(
									((BgL_backendz00_bglt) BgL_arg1325z00_1083)))->
							BgL_pragmazd2supportzd2);
					}
					if (BgL_test1638z00_1482)
						{	/* Object/coercion.scm 128 */
							return
								BBOOL(BGl_czd2genzd2classzd2coercersz12zc0zzobject_coercionz00
								(BgL_classz00_3, BgL_arg1131z00_824));
						}
					else
						{	/* Object/coercion.scm 128 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &gen-class-coercions! */
	obj_t BGl_z62genzd2classzd2coercionsz12z70zzobject_coercionz00(obj_t
		BgL_envz00_1390, obj_t BgL_classz00_1391)
	{
		{	/* Object/coercion.scm 48 */
			return
				BGl_genzd2classzd2coercionsz12z12zzobject_coercionz00
				(BgL_classz00_1391);
		}

	}



/* gen-java-class-coercions! */
	BGL_EXPORTED_DEF obj_t
		BGl_genzd2javazd2classzd2coercionsz12zc0zzobject_coercionz00(obj_t
		BgL_classz00_4)
	{
		{	/* Object/coercion.scm 56 */
			{	/* Object/coercion.scm 58 */
				obj_t BgL_arg1132z00_826;
				obj_t BgL_arg1137z00_827;

				BgL_arg1132z00_826 =
					(((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt)
								((BgL_typez00_bglt) BgL_classz00_4))))->BgL_idz00);
				{
					BgL_jclassz00_bglt BgL_auxz00_1492;

					{
						obj_t BgL_auxz00_1493;

						{	/* Object/coercion.scm 58 */
							BgL_objectz00_bglt BgL_tmpz00_1494;

							BgL_tmpz00_1494 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_classz00_4));
							BgL_auxz00_1493 = BGL_OBJECT_WIDENING(BgL_tmpz00_1494);
						}
						BgL_auxz00_1492 = ((BgL_jclassz00_bglt) BgL_auxz00_1493);
					}
					BgL_arg1137z00_827 =
						(((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_1492))->
						BgL_itszd2superzd2);
				}
				{	/* Object/coercion.scm 74 */
					obj_t BgL_arg1141z00_1088;

					BgL_arg1141z00_1088 =
						BGl_makezd2coercionzd2clausez00zzobject_coercionz00(
						((BgL_typez00_bglt) BgL_classz00_4), BgL_arg1132z00_826,
						BgL_arg1137z00_827, BNIL);
					BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
						(BgL_arg1141z00_1088);
				}
			}
			{	/* Object/coercion.scm 59 */
				obj_t BgL_arg1140z00_829;

				{
					BgL_jclassz00_bglt BgL_auxz00_1503;

					{
						obj_t BgL_auxz00_1504;

						{	/* Object/coercion.scm 59 */
							BgL_objectz00_bglt BgL_tmpz00_1505;

							BgL_tmpz00_1505 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_classz00_4));
							BgL_auxz00_1504 = BGL_OBJECT_WIDENING(BgL_tmpz00_1505);
						}
						BgL_auxz00_1503 = ((BgL_jclassz00_bglt) BgL_auxz00_1504);
					}
					BgL_arg1140z00_829 =
						(((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_1503))->
						BgL_itszd2superzd2);
				}
				{	/* Object/coercion.scm 128 */
					bool_t BgL_test1639z00_1511;

					{	/* Object/coercion.scm 128 */
						obj_t BgL_arg1325z00_1091;

						BgL_arg1325z00_1091 = BGl_thezd2backendzd2zzbackend_backendz00();
						BgL_test1639z00_1511 =
							(((BgL_backendz00_bglt) COBJECT(
									((BgL_backendz00_bglt) BgL_arg1325z00_1091)))->
							BgL_pragmazd2supportzd2);
					}
					if (BgL_test1639z00_1511)
						{	/* Object/coercion.scm 128 */
							return
								BBOOL(BGl_czd2genzd2classzd2coercersz12zc0zzobject_coercionz00
								(BgL_classz00_4, BgL_arg1140z00_829));
						}
					else
						{	/* Object/coercion.scm 128 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &gen-java-class-coercions! */
	obj_t BGl_z62genzd2javazd2classzd2coercionsz12za2zzobject_coercionz00(obj_t
		BgL_envz00_1392, obj_t BgL_classz00_1393)
	{
		{	/* Object/coercion.scm 56 */
			return
				BGl_genzd2javazd2classzd2coercionsz12zc0zzobject_coercionz00
				(BgL_classz00_1393);
		}

	}



/* gen-coercion-clause! */
	BGL_EXPORTED_DEF obj_t
		BGl_genzd2coercionzd2clausez12z12zzobject_coercionz00(BgL_typez00_bglt
		BgL_classz00_5, obj_t BgL_czd2idzd2_6, obj_t BgL_superz00_7,
		obj_t BgL_testingz00_8)
	{
		{	/* Object/coercion.scm 72 */
			return
				BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
				(BGl_makezd2coercionzd2clausez00zzobject_coercionz00(BgL_classz00_5,
					BgL_czd2idzd2_6, BgL_superz00_7, BgL_testingz00_8));
		}

	}



/* &gen-coercion-clause! */
	obj_t BGl_z62genzd2coercionzd2clausez12z70zzobject_coercionz00(obj_t
		BgL_envz00_1394, obj_t BgL_classz00_1395, obj_t BgL_czd2idzd2_1396,
		obj_t BgL_superz00_1397, obj_t BgL_testingz00_1398)
	{
		{	/* Object/coercion.scm 72 */
			return
				BGl_genzd2coercionzd2clausez12z12zzobject_coercionz00(
				((BgL_typez00_bglt) BgL_classz00_1395), BgL_czd2idzd2_1396,
				BgL_superz00_1397, BgL_testingz00_1398);
		}

	}



/* make-coercion-clause */
	obj_t BGl_makezd2coercionzd2clausez00zzobject_coercionz00(BgL_typez00_bglt
		BgL_classz00_9, obj_t BgL_czd2idzd2_10, obj_t BgL_superz00_11,
		obj_t BgL_testingz00_12)
	{
		{	/* Object/coercion.scm 79 */
			{	/* Object/coercion.scm 80 */
				obj_t BgL_classzd2ze3objz31_831;

				{	/* Object/coercion.scm 80 */
					obj_t BgL_arg1317z00_924;

					{	/* Object/coercion.scm 80 */
						obj_t BgL_arg1318z00_925;
						obj_t BgL_arg1319z00_926;

						BgL_arg1318z00_925 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BNIL);
						{	/* Object/coercion.scm 81 */
							obj_t BgL_arg1320z00_927;

							{	/* Object/coercion.scm 81 */
								obj_t BgL_list1321z00_928;

								BgL_list1321z00_928 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BNIL);
								BgL_arg1320z00_927 =
									BGl_makezd2privatezd2sexpz00zzast_privatez00(CNST_TABLE_REF
									(1), CNST_TABLE_REF(2), BgL_list1321z00_928);
							}
							BgL_arg1319z00_926 = MAKE_YOUNG_PAIR(BgL_arg1320z00_927, BNIL);
						}
						BgL_arg1317z00_924 =
							MAKE_YOUNG_PAIR(BgL_arg1318z00_925, BgL_arg1319z00_926);
					}
					BgL_classzd2ze3objz31_831 =
						MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BgL_arg1317z00_924);
				}
				{	/* Object/coercion.scm 80 */
					obj_t BgL_objzd2ze3classz31_832;

					{	/* Object/coercion.scm 82 */
						obj_t BgL_arg1311z00_919;

						{	/* Object/coercion.scm 82 */
							obj_t BgL_arg1312z00_920;
							obj_t BgL_arg1314z00_921;

							BgL_arg1312z00_920 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BNIL);
							{	/* Object/coercion.scm 83 */
								obj_t BgL_arg1315z00_922;

								{	/* Object/coercion.scm 83 */
									obj_t BgL_list1316z00_923;

									BgL_list1316z00_923 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BNIL);
									BgL_arg1315z00_922 =
										BGl_makezd2privatezd2sexpz00zzast_privatez00(CNST_TABLE_REF
										(1), BgL_czd2idzd2_10, BgL_list1316z00_923);
								}
								BgL_arg1314z00_921 = MAKE_YOUNG_PAIR(BgL_arg1315z00_922, BNIL);
							}
							BgL_arg1311z00_919 =
								MAKE_YOUNG_PAIR(BgL_arg1312z00_920, BgL_arg1314z00_921);
						}
						BgL_objzd2ze3classz31_832 =
							MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BgL_arg1311z00_919);
					}
					{	/* Object/coercion.scm 82 */
						obj_t BgL_ttestz00_833;

						if (NULLP(BgL_testingz00_12))
							{	/* Object/coercion.scm 85 */
								obj_t BgL_oz00_901;

								BgL_oz00_901 =
									BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(4));
								{	/* Object/coercion.scm 86 */
									bool_t BgL_test1641z00_1547;

									{	/* Object/coercion.scm 86 */
										obj_t BgL_classz00_1094;

										BgL_classz00_1094 = BGl_jclassz00zzobject_classz00;
										{	/* Object/coercion.scm 86 */
											BgL_objectz00_bglt BgL_arg1807z00_1096;

											{	/* Object/coercion.scm 86 */
												obj_t BgL_tmpz00_1548;

												BgL_tmpz00_1548 =
													((obj_t) ((BgL_objectz00_bglt) BgL_classz00_9));
												BgL_arg1807z00_1096 =
													(BgL_objectz00_bglt) (BgL_tmpz00_1548);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Object/coercion.scm 86 */
													long BgL_idxz00_1102;

													BgL_idxz00_1102 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1096);
													BgL_test1641z00_1547 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_1102 + 2L)) == BgL_classz00_1094);
												}
											else
												{	/* Object/coercion.scm 86 */
													bool_t BgL_res1603z00_1127;

													{	/* Object/coercion.scm 86 */
														obj_t BgL_oclassz00_1110;

														{	/* Object/coercion.scm 86 */
															obj_t BgL_arg1815z00_1118;
															long BgL_arg1816z00_1119;

															BgL_arg1815z00_1118 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Object/coercion.scm 86 */
																long BgL_arg1817z00_1120;

																BgL_arg1817z00_1120 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1096);
																BgL_arg1816z00_1119 =
																	(BgL_arg1817z00_1120 - OBJECT_TYPE);
															}
															BgL_oclassz00_1110 =
																VECTOR_REF(BgL_arg1815z00_1118,
																BgL_arg1816z00_1119);
														}
														{	/* Object/coercion.scm 86 */
															bool_t BgL__ortest_1115z00_1111;

															BgL__ortest_1115z00_1111 =
																(BgL_classz00_1094 == BgL_oclassz00_1110);
															if (BgL__ortest_1115z00_1111)
																{	/* Object/coercion.scm 86 */
																	BgL_res1603z00_1127 =
																		BgL__ortest_1115z00_1111;
																}
															else
																{	/* Object/coercion.scm 86 */
																	long BgL_odepthz00_1112;

																	{	/* Object/coercion.scm 86 */
																		obj_t BgL_arg1804z00_1113;

																		BgL_arg1804z00_1113 = (BgL_oclassz00_1110);
																		BgL_odepthz00_1112 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_1113);
																	}
																	if ((2L < BgL_odepthz00_1112))
																		{	/* Object/coercion.scm 86 */
																			obj_t BgL_arg1802z00_1115;

																			{	/* Object/coercion.scm 86 */
																				obj_t BgL_arg1803z00_1116;

																				BgL_arg1803z00_1116 =
																					(BgL_oclassz00_1110);
																				BgL_arg1802z00_1115 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_1116, 2L);
																			}
																			BgL_res1603z00_1127 =
																				(BgL_arg1802z00_1115 ==
																				BgL_classz00_1094);
																		}
																	else
																		{	/* Object/coercion.scm 86 */
																			BgL_res1603z00_1127 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test1641z00_1547 = BgL_res1603z00_1127;
												}
										}
									}
									if (BgL_test1641z00_1547)
										{	/* Object/coercion.scm 87 */
											obj_t BgL_arg1238z00_903;

											{	/* Object/coercion.scm 87 */
												obj_t BgL_arg1239z00_904;

												{	/* Object/coercion.scm 87 */
													obj_t BgL_arg1242z00_905;
													obj_t BgL_arg1244z00_906;

													BgL_arg1242z00_905 =
														MAKE_YOUNG_PAIR(BgL_oz00_901, BNIL);
													{	/* Object/coercion.scm 88 */
														obj_t BgL_arg1248z00_907;

														{	/* Object/coercion.scm 88 */
															obj_t BgL_list1249z00_908;

															BgL_list1249z00_908 =
																MAKE_YOUNG_PAIR(BgL_oz00_901, BNIL);
															BgL_arg1248z00_907 =
																BGl_makezd2privatezd2sexpz00zzast_privatez00
																(CNST_TABLE_REF(5), BgL_czd2idzd2_10,
																BgL_list1249z00_908);
														}
														BgL_arg1244z00_906 =
															MAKE_YOUNG_PAIR(BgL_arg1248z00_907, BNIL);
													}
													BgL_arg1239z00_904 =
														MAKE_YOUNG_PAIR(BgL_arg1242z00_905,
														BgL_arg1244z00_906);
												}
												BgL_arg1238z00_903 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
													BgL_arg1239z00_904);
											}
											BgL_ttestz00_833 =
												MAKE_YOUNG_PAIR(BgL_arg1238z00_903, BNIL);
										}
									else
										{	/* Object/coercion.scm 89 */
											obj_t BgL_arg1252z00_909;

											{	/* Object/coercion.scm 89 */
												obj_t BgL_arg1268z00_910;

												{	/* Object/coercion.scm 89 */
													obj_t BgL_arg1272z00_911;
													obj_t BgL_arg1284z00_912;

													BgL_arg1272z00_911 =
														MAKE_YOUNG_PAIR(BgL_oz00_901, BNIL);
													{	/* Object/coercion.scm 90 */
														obj_t BgL_arg1304z00_913;

														{	/* Object/coercion.scm 90 */
															obj_t BgL_arg1305z00_914;
															obj_t BgL_arg1306z00_915;

															{	/* Object/coercion.scm 90 */
																obj_t BgL_arg1307z00_916;

																{	/* Object/coercion.scm 90 */
																	obj_t BgL_arg1308z00_917;

																	BgL_arg1308z00_917 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(6), BNIL);
																	BgL_arg1307z00_916 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(7),
																		BgL_arg1308z00_917);
																}
																BgL_arg1305z00_914 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(8),
																	BgL_arg1307z00_916);
															}
															{	/* Object/coercion.scm 90 */
																obj_t BgL_arg1310z00_918;

																BgL_arg1310z00_918 =
																	MAKE_YOUNG_PAIR(BgL_czd2idzd2_10, BNIL);
																BgL_arg1306z00_915 =
																	MAKE_YOUNG_PAIR(BgL_oz00_901,
																	BgL_arg1310z00_918);
															}
															BgL_arg1304z00_913 =
																MAKE_YOUNG_PAIR(BgL_arg1305z00_914,
																BgL_arg1306z00_915);
														}
														BgL_arg1284z00_912 =
															MAKE_YOUNG_PAIR(BgL_arg1304z00_913, BNIL);
													}
													BgL_arg1268z00_910 =
														MAKE_YOUNG_PAIR(BgL_arg1272z00_911,
														BgL_arg1284z00_912);
												}
												BgL_arg1252z00_909 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
													BgL_arg1268z00_910);
											}
											BgL_ttestz00_833 =
												MAKE_YOUNG_PAIR(BgL_arg1252z00_909, BNIL);
										}
								}
							}
						else
							{	/* Object/coercion.scm 84 */
								BgL_ttestz00_833 = BNIL;
							}
						{	/* Object/coercion.scm 84 */
							obj_t BgL_xz00_834;

							BgL_xz00_834 =
								BGl_makezd2typedzd2identz00zzast_identz00(CNST_TABLE_REF(0),
								BgL_czd2idzd2_10);
							{	/* Object/coercion.scm 92 */

								{	/* Object/coercion.scm 93 */
									obj_t BgL_g1072z00_835;

									{	/* Object/coercion.scm 94 */
										obj_t BgL_arg1201z00_875;
										obj_t BgL_arg1202z00_876;
										obj_t BgL_arg1203z00_877;

										{	/* Object/coercion.scm 94 */
											obj_t BgL_arg1209z00_881;

											{	/* Object/coercion.scm 94 */
												obj_t BgL_arg1210z00_882;

												{	/* Object/coercion.scm 94 */
													obj_t BgL_arg1212z00_883;

													{	/* Object/coercion.scm 94 */
														obj_t BgL_arg1215z00_884;

														{	/* Object/coercion.scm 94 */
															obj_t BgL_arg1216z00_885;

															BgL_arg1216z00_885 =
																MAKE_YOUNG_PAIR(BgL_objzd2ze3classz31_832,
																BNIL);
															BgL_arg1215z00_884 =
																MAKE_YOUNG_PAIR(BgL_arg1216z00_885, BNIL);
														}
														BgL_arg1212z00_883 =
															MAKE_YOUNG_PAIR(BgL_ttestz00_833,
															BgL_arg1215z00_884);
													}
													BgL_arg1210z00_882 =
														MAKE_YOUNG_PAIR(BgL_czd2idzd2_10,
														BgL_arg1212z00_883);
												}
												BgL_arg1209z00_881 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(2),
													BgL_arg1210z00_882);
											}
											BgL_arg1201z00_875 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(9), BgL_arg1209z00_881);
										}
										{	/* Object/coercion.scm 95 */
											obj_t BgL_arg1218z00_886;

											{	/* Object/coercion.scm 95 */
												obj_t BgL_arg1219z00_887;

												{	/* Object/coercion.scm 95 */
													obj_t BgL_arg1220z00_888;

													{	/* Object/coercion.scm 95 */
														obj_t BgL_arg1221z00_889;

														{	/* Object/coercion.scm 95 */
															obj_t BgL_arg1223z00_890;

															BgL_arg1223z00_890 =
																MAKE_YOUNG_PAIR(BgL_classzd2ze3objz31_831,
																BNIL);
															BgL_arg1221z00_889 =
																MAKE_YOUNG_PAIR(BgL_arg1223z00_890, BNIL);
														}
														BgL_arg1220z00_888 =
															MAKE_YOUNG_PAIR(BNIL, BgL_arg1221z00_889);
													}
													BgL_arg1219z00_887 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(2),
														BgL_arg1220z00_888);
												}
												BgL_arg1218z00_886 =
													MAKE_YOUNG_PAIR(BgL_czd2idzd2_10, BgL_arg1219z00_887);
											}
											BgL_arg1202z00_876 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(9), BgL_arg1218z00_886);
										}
										{	/* Object/coercion.scm 96 */
											obj_t BgL_arg1225z00_891;

											{	/* Object/coercion.scm 96 */
												obj_t BgL_arg1226z00_892;

												{	/* Object/coercion.scm 96 */
													obj_t BgL_arg1227z00_893;

													{	/* Object/coercion.scm 96 */
														obj_t BgL_arg1228z00_894;

														{	/* Object/coercion.scm 96 */
															obj_t BgL_arg1229z00_895;

															{	/* Object/coercion.scm 96 */
																obj_t BgL_arg1230z00_896;

																{	/* Object/coercion.scm 96 */
																	obj_t BgL_arg1231z00_897;

																	{	/* Object/coercion.scm 96 */
																		obj_t BgL_arg1232z00_898;
																		obj_t BgL_arg1233z00_899;

																		BgL_arg1232z00_898 =
																			MAKE_YOUNG_PAIR(BgL_xz00_834, BNIL);
																		BgL_arg1233z00_899 =
																			MAKE_YOUNG_PAIR(BTRUE, BNIL);
																		BgL_arg1231z00_897 =
																			MAKE_YOUNG_PAIR(BgL_arg1232z00_898,
																			BgL_arg1233z00_899);
																	}
																	BgL_arg1230z00_896 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
																		BgL_arg1231z00_897);
																}
																BgL_arg1229z00_895 =
																	MAKE_YOUNG_PAIR(BgL_arg1230z00_896, BNIL);
															}
															BgL_arg1228z00_894 =
																MAKE_YOUNG_PAIR(BgL_arg1229z00_895, BNIL);
														}
														BgL_arg1227z00_893 =
															MAKE_YOUNG_PAIR(BNIL, BgL_arg1228z00_894);
													}
													BgL_arg1226z00_892 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
														BgL_arg1227z00_893);
												}
												BgL_arg1225z00_891 =
													MAKE_YOUNG_PAIR(BgL_czd2idzd2_10, BgL_arg1226z00_892);
											}
											BgL_arg1203z00_877 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(9), BgL_arg1225z00_891);
										}
										{	/* Object/coercion.scm 94 */
											obj_t BgL_list1204z00_878;

											{	/* Object/coercion.scm 94 */
												obj_t BgL_arg1206z00_879;

												{	/* Object/coercion.scm 94 */
													obj_t BgL_arg1208z00_880;

													BgL_arg1208z00_880 =
														MAKE_YOUNG_PAIR(BgL_arg1203z00_877, BNIL);
													BgL_arg1206z00_879 =
														MAKE_YOUNG_PAIR(BgL_arg1202z00_876,
														BgL_arg1208z00_880);
												}
												BgL_list1204z00_878 =
													MAKE_YOUNG_PAIR(BgL_arg1201z00_875,
													BgL_arg1206z00_879);
											}
											BgL_g1072z00_835 = BgL_list1204z00_878;
										}
									}
									{
										obj_t BgL_superz00_837;
										obj_t BgL_coercerz00_838;

										BgL_superz00_837 = BgL_superz00_11;
										BgL_coercerz00_838 = BgL_g1072z00_835;
									BgL_zc3z04anonymousza31142ze3z87_839:
										{	/* Object/coercion.scm 97 */
											bool_t BgL_test1645z00_1629;

											{	/* Object/coercion.scm 97 */
												bool_t BgL_test1646z00_1630;

												{	/* Object/coercion.scm 97 */
													obj_t BgL_classz00_1129;

													BgL_classz00_1129 = BGl_jclassz00zzobject_classz00;
													if (BGL_OBJECTP(BgL_superz00_837))
														{	/* Object/coercion.scm 97 */
															BgL_objectz00_bglt BgL_arg1807z00_1131;

															BgL_arg1807z00_1131 =
																(BgL_objectz00_bglt) (BgL_superz00_837);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Object/coercion.scm 97 */
																	long BgL_idxz00_1137;

																	BgL_idxz00_1137 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_1131);
																	BgL_test1646z00_1630 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_1137 + 2L)) ==
																		BgL_classz00_1129);
																}
															else
																{	/* Object/coercion.scm 97 */
																	bool_t BgL_res1605z00_1162;

																	{	/* Object/coercion.scm 97 */
																		obj_t BgL_oclassz00_1145;

																		{	/* Object/coercion.scm 97 */
																			obj_t BgL_arg1815z00_1153;
																			long BgL_arg1816z00_1154;

																			BgL_arg1815z00_1153 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Object/coercion.scm 97 */
																				long BgL_arg1817z00_1155;

																				BgL_arg1817z00_1155 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_1131);
																				BgL_arg1816z00_1154 =
																					(BgL_arg1817z00_1155 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_1145 =
																				VECTOR_REF(BgL_arg1815z00_1153,
																				BgL_arg1816z00_1154);
																		}
																		{	/* Object/coercion.scm 97 */
																			bool_t BgL__ortest_1115z00_1146;

																			BgL__ortest_1115z00_1146 =
																				(BgL_classz00_1129 ==
																				BgL_oclassz00_1145);
																			if (BgL__ortest_1115z00_1146)
																				{	/* Object/coercion.scm 97 */
																					BgL_res1605z00_1162 =
																						BgL__ortest_1115z00_1146;
																				}
																			else
																				{	/* Object/coercion.scm 97 */
																					long BgL_odepthz00_1147;

																					{	/* Object/coercion.scm 97 */
																						obj_t BgL_arg1804z00_1148;

																						BgL_arg1804z00_1148 =
																							(BgL_oclassz00_1145);
																						BgL_odepthz00_1147 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_1148);
																					}
																					if ((2L < BgL_odepthz00_1147))
																						{	/* Object/coercion.scm 97 */
																							obj_t BgL_arg1802z00_1150;

																							{	/* Object/coercion.scm 97 */
																								obj_t BgL_arg1803z00_1151;

																								BgL_arg1803z00_1151 =
																									(BgL_oclassz00_1145);
																								BgL_arg1802z00_1150 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_1151, 2L);
																							}
																							BgL_res1605z00_1162 =
																								(BgL_arg1802z00_1150 ==
																								BgL_classz00_1129);
																						}
																					else
																						{	/* Object/coercion.scm 97 */
																							BgL_res1605z00_1162 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test1646z00_1630 = BgL_res1605z00_1162;
																}
														}
													else
														{	/* Object/coercion.scm 97 */
															BgL_test1646z00_1630 = ((bool_t) 0);
														}
												}
												if (BgL_test1646z00_1630)
													{	/* Object/coercion.scm 97 */
														BgL_test1645z00_1629 = ((bool_t) 1);
													}
												else
													{	/* Object/coercion.scm 97 */
														obj_t BgL_classz00_1163;

														BgL_classz00_1163 = BGl_tclassz00zzobject_classz00;
														if (BGL_OBJECTP(BgL_superz00_837))
															{	/* Object/coercion.scm 97 */
																BgL_objectz00_bglt BgL_arg1807z00_1165;

																BgL_arg1807z00_1165 =
																	(BgL_objectz00_bglt) (BgL_superz00_837);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Object/coercion.scm 97 */
																		long BgL_idxz00_1171;

																		BgL_idxz00_1171 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_1165);
																		BgL_test1645z00_1629 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_1171 + 2L)) ==
																			BgL_classz00_1163);
																	}
																else
																	{	/* Object/coercion.scm 97 */
																		bool_t BgL_res1606z00_1196;

																		{	/* Object/coercion.scm 97 */
																			obj_t BgL_oclassz00_1179;

																			{	/* Object/coercion.scm 97 */
																				obj_t BgL_arg1815z00_1187;
																				long BgL_arg1816z00_1188;

																				BgL_arg1815z00_1187 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Object/coercion.scm 97 */
																					long BgL_arg1817z00_1189;

																					BgL_arg1817z00_1189 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_1165);
																					BgL_arg1816z00_1188 =
																						(BgL_arg1817z00_1189 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_1179 =
																					VECTOR_REF(BgL_arg1815z00_1187,
																					BgL_arg1816z00_1188);
																			}
																			{	/* Object/coercion.scm 97 */
																				bool_t BgL__ortest_1115z00_1180;

																				BgL__ortest_1115z00_1180 =
																					(BgL_classz00_1163 ==
																					BgL_oclassz00_1179);
																				if (BgL__ortest_1115z00_1180)
																					{	/* Object/coercion.scm 97 */
																						BgL_res1606z00_1196 =
																							BgL__ortest_1115z00_1180;
																					}
																				else
																					{	/* Object/coercion.scm 97 */
																						long BgL_odepthz00_1181;

																						{	/* Object/coercion.scm 97 */
																							obj_t BgL_arg1804z00_1182;

																							BgL_arg1804z00_1182 =
																								(BgL_oclassz00_1179);
																							BgL_odepthz00_1181 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_1182);
																						}
																						if ((2L < BgL_odepthz00_1181))
																							{	/* Object/coercion.scm 97 */
																								obj_t BgL_arg1802z00_1184;

																								{	/* Object/coercion.scm 97 */
																									obj_t BgL_arg1803z00_1185;

																									BgL_arg1803z00_1185 =
																										(BgL_oclassz00_1179);
																									BgL_arg1802z00_1184 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_1185, 2L);
																								}
																								BgL_res1606z00_1196 =
																									(BgL_arg1802z00_1184 ==
																									BgL_classz00_1163);
																							}
																						else
																							{	/* Object/coercion.scm 97 */
																								BgL_res1606z00_1196 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test1645z00_1629 = BgL_res1606z00_1196;
																	}
															}
														else
															{	/* Object/coercion.scm 97 */
																BgL_test1645z00_1629 = ((bool_t) 0);
															}
													}
											}
											if (BgL_test1645z00_1629)
												{	/* Object/coercion.scm 99 */
													obj_t BgL_superzd2idzd2_842;

													{	/* Object/coercion.scm 99 */
														bool_t BgL_test1656z00_1675;

														{	/* Object/coercion.scm 99 */
															obj_t BgL_classz00_1197;

															BgL_classz00_1197 =
																BGl_tclassz00zzobject_classz00;
															if (BGL_OBJECTP(BgL_superz00_837))
																{	/* Object/coercion.scm 99 */
																	BgL_objectz00_bglt BgL_arg1807z00_1199;

																	BgL_arg1807z00_1199 =
																		(BgL_objectz00_bglt) (BgL_superz00_837);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Object/coercion.scm 99 */
																			long BgL_idxz00_1205;

																			BgL_idxz00_1205 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_1199);
																			BgL_test1656z00_1675 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_1205 + 2L)) ==
																				BgL_classz00_1197);
																		}
																	else
																		{	/* Object/coercion.scm 99 */
																			bool_t BgL_res1607z00_1230;

																			{	/* Object/coercion.scm 99 */
																				obj_t BgL_oclassz00_1213;

																				{	/* Object/coercion.scm 99 */
																					obj_t BgL_arg1815z00_1221;
																					long BgL_arg1816z00_1222;

																					BgL_arg1815z00_1221 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Object/coercion.scm 99 */
																						long BgL_arg1817z00_1223;

																						BgL_arg1817z00_1223 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_1199);
																						BgL_arg1816z00_1222 =
																							(BgL_arg1817z00_1223 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_1213 =
																						VECTOR_REF(BgL_arg1815z00_1221,
																						BgL_arg1816z00_1222);
																				}
																				{	/* Object/coercion.scm 99 */
																					bool_t BgL__ortest_1115z00_1214;

																					BgL__ortest_1115z00_1214 =
																						(BgL_classz00_1197 ==
																						BgL_oclassz00_1213);
																					if (BgL__ortest_1115z00_1214)
																						{	/* Object/coercion.scm 99 */
																							BgL_res1607z00_1230 =
																								BgL__ortest_1115z00_1214;
																						}
																					else
																						{	/* Object/coercion.scm 99 */
																							long BgL_odepthz00_1215;

																							{	/* Object/coercion.scm 99 */
																								obj_t BgL_arg1804z00_1216;

																								BgL_arg1804z00_1216 =
																									(BgL_oclassz00_1213);
																								BgL_odepthz00_1215 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_1216);
																							}
																							if ((2L < BgL_odepthz00_1215))
																								{	/* Object/coercion.scm 99 */
																									obj_t BgL_arg1802z00_1218;

																									{	/* Object/coercion.scm 99 */
																										obj_t BgL_arg1803z00_1219;

																										BgL_arg1803z00_1219 =
																											(BgL_oclassz00_1213);
																										BgL_arg1802z00_1218 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_1219, 2L);
																									}
																									BgL_res1607z00_1230 =
																										(BgL_arg1802z00_1218 ==
																										BgL_classz00_1197);
																								}
																							else
																								{	/* Object/coercion.scm 99 */
																									BgL_res1607z00_1230 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test1656z00_1675 =
																				BgL_res1607z00_1230;
																		}
																}
															else
																{	/* Object/coercion.scm 99 */
																	BgL_test1656z00_1675 = ((bool_t) 0);
																}
														}
														if (BgL_test1656z00_1675)
															{	/* Object/coercion.scm 99 */
																BgL_superzd2idzd2_842 =
																	(((BgL_typez00_bglt) COBJECT(
																			((BgL_typez00_bglt)
																				((BgL_typez00_bglt)
																					BgL_superz00_837))))->BgL_idz00);
															}
														else
															{	/* Object/coercion.scm 99 */
																BgL_superzd2idzd2_842 =
																	(((BgL_typez00_bglt) COBJECT(
																			((BgL_typez00_bglt)
																				((BgL_typez00_bglt)
																					BgL_superz00_837))))->BgL_idz00);
															}
													}
													{	/* Object/coercion.scm 99 */
														obj_t BgL_classzd2ze3superz31_843;

														{	/* Object/coercion.scm 102 */
															obj_t BgL_arg1193z00_866;

															{	/* Object/coercion.scm 102 */
																obj_t BgL_arg1194z00_867;
																obj_t BgL_arg1196z00_868;

																BgL_arg1194z00_867 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BNIL);
																{	/* Object/coercion.scm 103 */
																	obj_t BgL_arg1197z00_869;

																	{	/* Object/coercion.scm 103 */
																		obj_t BgL_list1198z00_870;

																		BgL_list1198z00_870 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BNIL);
																		BgL_arg1197z00_869 =
																			BGl_makezd2privatezd2sexpz00zzast_privatez00
																			(CNST_TABLE_REF(1), BgL_superzd2idzd2_842,
																			BgL_list1198z00_870);
																	}
																	BgL_arg1196z00_868 =
																		MAKE_YOUNG_PAIR(BgL_arg1197z00_869, BNIL);
																}
																BgL_arg1193z00_866 =
																	MAKE_YOUNG_PAIR(BgL_arg1194z00_867,
																	BgL_arg1196z00_868);
															}
															BgL_classzd2ze3superz31_843 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
																BgL_arg1193z00_866);
														}
														{	/* Object/coercion.scm 102 */
															obj_t BgL_superzd2ze3classz31_844;

															{	/* Object/coercion.scm 104 */
																obj_t BgL_arg1188z00_861;

																{	/* Object/coercion.scm 104 */
																	obj_t BgL_arg1189z00_862;
																	obj_t BgL_arg1190z00_863;

																	BgL_arg1189z00_862 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BNIL);
																	{	/* Object/coercion.scm 105 */
																		obj_t BgL_arg1191z00_864;

																		{	/* Object/coercion.scm 105 */
																			obj_t BgL_list1192z00_865;

																			BgL_list1192z00_865 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
																				BNIL);
																			BgL_arg1191z00_864 =
																				BGl_makezd2privatezd2sexpz00zzast_privatez00
																				(CNST_TABLE_REF(1), BgL_czd2idzd2_10,
																				BgL_list1192z00_865);
																		}
																		BgL_arg1190z00_863 =
																			MAKE_YOUNG_PAIR(BgL_arg1191z00_864, BNIL);
																	}
																	BgL_arg1188z00_861 =
																		MAKE_YOUNG_PAIR(BgL_arg1189z00_862,
																		BgL_arg1190z00_863);
																}
																BgL_superzd2ze3classz31_844 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
																	BgL_arg1188z00_861);
															}
															{	/* Object/coercion.scm 104 */

																{	/* Object/coercion.scm 106 */
																	obj_t BgL_arg1145z00_845;
																	obj_t BgL_arg1148z00_846;

																	{	/* Object/coercion.scm 106 */
																		bool_t BgL_test1661z00_1724;

																		{	/* Object/coercion.scm 106 */
																			obj_t BgL_classz00_1233;

																			BgL_classz00_1233 =
																				BGl_tclassz00zzobject_classz00;
																			if (BGL_OBJECTP(BgL_superz00_837))
																				{	/* Object/coercion.scm 106 */
																					BgL_objectz00_bglt
																						BgL_arg1807z00_1235;
																					BgL_arg1807z00_1235 =
																						(BgL_objectz00_bglt)
																						(BgL_superz00_837);
																					if (BGL_CONDEXPAND_ISA_ARCH64())
																						{	/* Object/coercion.scm 106 */
																							long BgL_idxz00_1241;

																							BgL_idxz00_1241 =
																								BGL_OBJECT_INHERITANCE_NUM
																								(BgL_arg1807z00_1235);
																							BgL_test1661z00_1724 =
																								(VECTOR_REF
																								(BGl_za2inheritancesza2z00zz__objectz00,
																									(BgL_idxz00_1241 + 2L)) ==
																								BgL_classz00_1233);
																						}
																					else
																						{	/* Object/coercion.scm 106 */
																							bool_t BgL_res1608z00_1266;

																							{	/* Object/coercion.scm 106 */
																								obj_t BgL_oclassz00_1249;

																								{	/* Object/coercion.scm 106 */
																									obj_t BgL_arg1815z00_1257;
																									long BgL_arg1816z00_1258;

																									BgL_arg1815z00_1257 =
																										(BGl_za2classesza2z00zz__objectz00);
																									{	/* Object/coercion.scm 106 */
																										long BgL_arg1817z00_1259;

																										BgL_arg1817z00_1259 =
																											BGL_OBJECT_CLASS_NUM
																											(BgL_arg1807z00_1235);
																										BgL_arg1816z00_1258 =
																											(BgL_arg1817z00_1259 -
																											OBJECT_TYPE);
																									}
																									BgL_oclassz00_1249 =
																										VECTOR_REF
																										(BgL_arg1815z00_1257,
																										BgL_arg1816z00_1258);
																								}
																								{	/* Object/coercion.scm 106 */
																									bool_t
																										BgL__ortest_1115z00_1250;
																									BgL__ortest_1115z00_1250 =
																										(BgL_classz00_1233 ==
																										BgL_oclassz00_1249);
																									if (BgL__ortest_1115z00_1250)
																										{	/* Object/coercion.scm 106 */
																											BgL_res1608z00_1266 =
																												BgL__ortest_1115z00_1250;
																										}
																									else
																										{	/* Object/coercion.scm 106 */
																											long BgL_odepthz00_1251;

																											{	/* Object/coercion.scm 106 */
																												obj_t
																													BgL_arg1804z00_1252;
																												BgL_arg1804z00_1252 =
																													(BgL_oclassz00_1249);
																												BgL_odepthz00_1251 =
																													BGL_CLASS_DEPTH
																													(BgL_arg1804z00_1252);
																											}
																											if (
																												(2L <
																													BgL_odepthz00_1251))
																												{	/* Object/coercion.scm 106 */
																													obj_t
																														BgL_arg1802z00_1254;
																													{	/* Object/coercion.scm 106 */
																														obj_t
																															BgL_arg1803z00_1255;
																														BgL_arg1803z00_1255
																															=
																															(BgL_oclassz00_1249);
																														BgL_arg1802z00_1254
																															=
																															BGL_CLASS_ANCESTORS_REF
																															(BgL_arg1803z00_1255,
																															2L);
																													}
																													BgL_res1608z00_1266 =
																														(BgL_arg1802z00_1254
																														==
																														BgL_classz00_1233);
																												}
																											else
																												{	/* Object/coercion.scm 106 */
																													BgL_res1608z00_1266 =
																														((bool_t) 0);
																												}
																										}
																								}
																							}
																							BgL_test1661z00_1724 =
																								BgL_res1608z00_1266;
																						}
																				}
																			else
																				{	/* Object/coercion.scm 106 */
																					BgL_test1661z00_1724 = ((bool_t) 0);
																				}
																		}
																		if (BgL_test1661z00_1724)
																			{
																				BgL_tclassz00_bglt BgL_auxz00_1747;

																				{
																					obj_t BgL_auxz00_1748;

																					{	/* Object/coercion.scm 107 */
																						BgL_objectz00_bglt BgL_tmpz00_1749;

																						BgL_tmpz00_1749 =
																							((BgL_objectz00_bglt)
																							((BgL_typez00_bglt)
																								BgL_superz00_837));
																						BgL_auxz00_1748 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_1749);
																					}
																					BgL_auxz00_1747 =
																						((BgL_tclassz00_bglt)
																						BgL_auxz00_1748);
																				}
																				BgL_arg1145z00_845 =
																					(((BgL_tclassz00_bglt)
																						COBJECT(BgL_auxz00_1747))->
																					BgL_itszd2superzd2);
																			}
																		else
																			{
																				BgL_jclassz00_bglt BgL_auxz00_1755;

																				{
																					obj_t BgL_auxz00_1756;

																					{	/* Object/coercion.scm 108 */
																						BgL_objectz00_bglt BgL_tmpz00_1757;

																						BgL_tmpz00_1757 =
																							((BgL_objectz00_bglt)
																							((BgL_typez00_bglt)
																								BgL_superz00_837));
																						BgL_auxz00_1756 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_1757);
																					}
																					BgL_auxz00_1755 =
																						((BgL_jclassz00_bglt)
																						BgL_auxz00_1756);
																				}
																				BgL_arg1145z00_845 =
																					(((BgL_jclassz00_bglt)
																						COBJECT(BgL_auxz00_1755))->
																					BgL_itszd2superzd2);
																			}
																	}
																	{	/* Object/coercion.scm 109 */
																		obj_t BgL_arg1152z00_848;
																		obj_t BgL_arg1153z00_849;

																		{	/* Object/coercion.scm 109 */
																			obj_t BgL_arg1154z00_850;

																			{	/* Object/coercion.scm 109 */
																				obj_t BgL_arg1157z00_851;

																				{	/* Object/coercion.scm 109 */
																					obj_t BgL_arg1158z00_852;

																					{	/* Object/coercion.scm 109 */
																						obj_t BgL_arg1162z00_853;

																						{	/* Object/coercion.scm 109 */
																							obj_t BgL_arg1164z00_854;

																							BgL_arg1164z00_854 =
																								MAKE_YOUNG_PAIR
																								(BgL_superzd2ze3classz31_844,
																								BNIL);
																							BgL_arg1162z00_853 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1164z00_854, BNIL);
																						}
																						BgL_arg1158z00_852 =
																							MAKE_YOUNG_PAIR(BgL_ttestz00_833,
																							BgL_arg1162z00_853);
																					}
																					BgL_arg1157z00_851 =
																						MAKE_YOUNG_PAIR(BgL_czd2idzd2_10,
																						BgL_arg1158z00_852);
																				}
																				BgL_arg1154z00_850 =
																					MAKE_YOUNG_PAIR(BgL_superzd2idzd2_842,
																					BgL_arg1157z00_851);
																			}
																			BgL_arg1152z00_848 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(9),
																				BgL_arg1154z00_850);
																		}
																		{	/* Object/coercion.scm 110 */
																			obj_t BgL_arg1166z00_855;

																			{	/* Object/coercion.scm 110 */
																				obj_t BgL_arg1171z00_856;

																				{	/* Object/coercion.scm 110 */
																					obj_t BgL_arg1172z00_857;

																					{	/* Object/coercion.scm 110 */
																						obj_t BgL_arg1182z00_858;

																						{	/* Object/coercion.scm 110 */
																							obj_t BgL_arg1183z00_859;

																							{	/* Object/coercion.scm 110 */
																								obj_t BgL_arg1187z00_860;

																								BgL_arg1187z00_860 =
																									MAKE_YOUNG_PAIR
																									(BgL_classzd2ze3superz31_843,
																									BNIL);
																								BgL_arg1183z00_859 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1187z00_860, BNIL);
																							}
																							BgL_arg1182z00_858 =
																								MAKE_YOUNG_PAIR(BNIL,
																								BgL_arg1183z00_859);
																						}
																						BgL_arg1172z00_857 =
																							MAKE_YOUNG_PAIR
																							(BgL_superzd2idzd2_842,
																							BgL_arg1182z00_858);
																					}
																					BgL_arg1171z00_856 =
																						MAKE_YOUNG_PAIR(BgL_czd2idzd2_10,
																						BgL_arg1172z00_857);
																				}
																				BgL_arg1166z00_855 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(9),
																					BgL_arg1171z00_856);
																			}
																			BgL_arg1153z00_849 =
																				MAKE_YOUNG_PAIR(BgL_arg1166z00_855,
																				BgL_coercerz00_838);
																		}
																		BgL_arg1148z00_846 =
																			MAKE_YOUNG_PAIR(BgL_arg1152z00_848,
																			BgL_arg1153z00_849);
																	}
																	{
																		obj_t BgL_coercerz00_1780;
																		obj_t BgL_superz00_1779;

																		BgL_superz00_1779 = BgL_arg1145z00_845;
																		BgL_coercerz00_1780 = BgL_arg1148z00_846;
																		BgL_coercerz00_838 = BgL_coercerz00_1780;
																		BgL_superz00_837 = BgL_superz00_1779;
																		goto BgL_zc3z04anonymousza31142ze3z87_839;
																	}
																}
															}
														}
													}
												}
											else
												{	/* Object/coercion.scm 98 */
													obj_t BgL_arg1200z00_872;

													BgL_arg1200z00_872 =
														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
														(BgL_coercerz00_838, BNIL);
													return MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
														BgL_arg1200z00_872);
												}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* gen-class-coercers! */
	BGL_EXPORTED_DEF obj_t
		BGl_genzd2classzd2coercersz12z12zzobject_coercionz00(obj_t BgL_classz00_13,
		obj_t BgL_superz00_14)
	{
		{	/* Object/coercion.scm 127 */
			{	/* Object/coercion.scm 128 */
				bool_t BgL_test1667z00_1784;

				{	/* Object/coercion.scm 128 */
					obj_t BgL_arg1325z00_1272;

					BgL_arg1325z00_1272 = BGl_thezd2backendzd2zzbackend_backendz00();
					BgL_test1667z00_1784 =
						(((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_arg1325z00_1272)))->
						BgL_pragmazd2supportzd2);
				}
				if (BgL_test1667z00_1784)
					{	/* Object/coercion.scm 128 */
						return
							BBOOL(BGl_czd2genzd2classzd2coercersz12zc0zzobject_coercionz00
							(BgL_classz00_13, BgL_superz00_14));
					}
				else
					{	/* Object/coercion.scm 128 */
						return BFALSE;
					}
			}
		}

	}



/* &gen-class-coercers! */
	obj_t BGl_z62genzd2classzd2coercersz12z70zzobject_coercionz00(obj_t
		BgL_envz00_1399, obj_t BgL_classz00_1400, obj_t BgL_superz00_1401)
	{
		{	/* Object/coercion.scm 127 */
			return
				BGl_genzd2classzd2coercersz12z12zzobject_coercionz00(BgL_classz00_1400,
				BgL_superz00_1401);
		}

	}



/* c-gen-class-coercers! */
	bool_t BGl_czd2genzd2classzd2coercersz12zc0zzobject_coercionz00(obj_t
		BgL_classz00_15, obj_t BgL_superz00_16)
	{
		{	/* Object/coercion.scm 134 */
			{	/* Object/coercion.scm 144 */
				obj_t BgL_tidz00_933;
				obj_t BgL_tnamez00_934;

				BgL_tidz00_933 =
					(((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_classz00_15)))->BgL_idz00);
				BgL_tnamez00_934 =
					(((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_classz00_15)))->BgL_namez00);
				{	/* Object/coercion.scm 146 */
					obj_t BgL_pragz00_935;

					BgL_pragz00_935 =
						BGl_makezd2onezd2coercionze70ze7zzobject_coercionz00(BgL_tidz00_933,
						BgL_tnamez00_934, CNST_TABLE_REF(2),
						BGl_string1614z00zzobject_coercionz00);
					{	/* Object/coercion.scm 147 */
						obj_t BgL_coercersz00_936;

						{	/* Object/coercion.scm 148 */
							obj_t BgL_tmpz00_1277;

							{	/* Object/coercion.scm 148 */
								int BgL_tmpz00_1797;

								BgL_tmpz00_1797 = (int) (1L);
								BgL_tmpz00_1277 = BGL_MVALUES_VAL(BgL_tmpz00_1797);
							}
							{	/* Object/coercion.scm 148 */
								int BgL_tmpz00_1800;

								BgL_tmpz00_1800 = (int) (1L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_1800, BUNSPEC);
							}
							BgL_coercersz00_936 = BgL_tmpz00_1277;
						}
						{	/* Object/coercion.scm 148 */
							obj_t BgL_g1073z00_937;

							{	/* Object/coercion.scm 150 */
								obj_t BgL_list1341z00_961;

								BgL_list1341z00_961 = MAKE_YOUNG_PAIR(BgL_pragz00_935, BNIL);
								BgL_g1073z00_937 = BgL_list1341z00_961;
							}
							{
								obj_t BgL_superz00_939;
								obj_t BgL_coercersz00_940;
								obj_t BgL_pragmasz00_941;

								BgL_superz00_939 = BgL_superz00_16;
								BgL_coercersz00_940 = BgL_coercersz00_936;
								BgL_pragmasz00_941 = BgL_g1073z00_937;
							BgL_zc3z04anonymousza31326ze3z87_942:
								{	/* Object/coercion.scm 151 */
									bool_t BgL_test1668z00_1804;

									{	/* Object/coercion.scm 151 */
										obj_t BgL_classz00_1279;

										BgL_classz00_1279 = BGl_tclassz00zzobject_classz00;
										if (BGL_OBJECTP(BgL_superz00_939))
											{	/* Object/coercion.scm 151 */
												BgL_objectz00_bglt BgL_arg1807z00_1281;

												BgL_arg1807z00_1281 =
													(BgL_objectz00_bglt) (BgL_superz00_939);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Object/coercion.scm 151 */
														long BgL_idxz00_1287;

														BgL_idxz00_1287 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1281);
														BgL_test1668z00_1804 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_1287 + 2L)) == BgL_classz00_1279);
													}
												else
													{	/* Object/coercion.scm 151 */
														bool_t BgL_res1611z00_1312;

														{	/* Object/coercion.scm 151 */
															obj_t BgL_oclassz00_1295;

															{	/* Object/coercion.scm 151 */
																obj_t BgL_arg1815z00_1303;
																long BgL_arg1816z00_1304;

																BgL_arg1815z00_1303 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Object/coercion.scm 151 */
																	long BgL_arg1817z00_1305;

																	BgL_arg1817z00_1305 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1281);
																	BgL_arg1816z00_1304 =
																		(BgL_arg1817z00_1305 - OBJECT_TYPE);
																}
																BgL_oclassz00_1295 =
																	VECTOR_REF(BgL_arg1815z00_1303,
																	BgL_arg1816z00_1304);
															}
															{	/* Object/coercion.scm 151 */
																bool_t BgL__ortest_1115z00_1296;

																BgL__ortest_1115z00_1296 =
																	(BgL_classz00_1279 == BgL_oclassz00_1295);
																if (BgL__ortest_1115z00_1296)
																	{	/* Object/coercion.scm 151 */
																		BgL_res1611z00_1312 =
																			BgL__ortest_1115z00_1296;
																	}
																else
																	{	/* Object/coercion.scm 151 */
																		long BgL_odepthz00_1297;

																		{	/* Object/coercion.scm 151 */
																			obj_t BgL_arg1804z00_1298;

																			BgL_arg1804z00_1298 =
																				(BgL_oclassz00_1295);
																			BgL_odepthz00_1297 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_1298);
																		}
																		if ((2L < BgL_odepthz00_1297))
																			{	/* Object/coercion.scm 151 */
																				obj_t BgL_arg1802z00_1300;

																				{	/* Object/coercion.scm 151 */
																					obj_t BgL_arg1803z00_1301;

																					BgL_arg1803z00_1301 =
																						(BgL_oclassz00_1295);
																					BgL_arg1802z00_1300 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_1301, 2L);
																				}
																				BgL_res1611z00_1312 =
																					(BgL_arg1802z00_1300 ==
																					BgL_classz00_1279);
																			}
																		else
																			{	/* Object/coercion.scm 151 */
																				BgL_res1611z00_1312 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test1668z00_1804 = BgL_res1611z00_1312;
													}
											}
										else
											{	/* Object/coercion.scm 151 */
												BgL_test1668z00_1804 = ((bool_t) 0);
											}
									}
									if (BgL_test1668z00_1804)
										{	/* Object/coercion.scm 155 */
											obj_t BgL_sidz00_944;
											obj_t BgL_snamez00_945;

											BgL_sidz00_944 =
												(((BgL_typez00_bglt) COBJECT(
														((BgL_typez00_bglt) BgL_superz00_939)))->BgL_idz00);
											BgL_snamez00_945 =
												(((BgL_typez00_bglt) COBJECT(
														((BgL_typez00_bglt) BgL_superz00_939)))->
												BgL_namez00);
											{	/* Object/coercion.scm 157 */
												obj_t BgL_pragz00_946;

												BgL_pragz00_946 =
													BGl_makezd2onezd2coercionze70ze7zzobject_coercionz00
													(BgL_tidz00_933, BgL_tnamez00_934, BgL_sidz00_944,
													BgL_snamez00_945);
												{	/* Object/coercion.scm 158 */
													obj_t BgL_coercsz00_947;

													{	/* Object/coercion.scm 159 */
														obj_t BgL_tmpz00_1315;

														{	/* Object/coercion.scm 159 */
															int BgL_tmpz00_1832;

															BgL_tmpz00_1832 = (int) (1L);
															BgL_tmpz00_1315 =
																BGL_MVALUES_VAL(BgL_tmpz00_1832);
														}
														{	/* Object/coercion.scm 159 */
															int BgL_tmpz00_1835;

															BgL_tmpz00_1835 = (int) (1L);
															BGL_MVALUES_VAL_SET(BgL_tmpz00_1835, BUNSPEC);
														}
														BgL_coercsz00_947 = BgL_tmpz00_1315;
													}
													{	/* Object/coercion.scm 159 */
														obj_t BgL_arg1328z00_948;
														obj_t BgL_arg1329z00_949;
														obj_t BgL_arg1331z00_950;

														{
															BgL_tclassz00_bglt BgL_auxz00_1838;

															{
																obj_t BgL_auxz00_1839;

																{	/* Object/coercion.scm 159 */
																	BgL_objectz00_bglt BgL_tmpz00_1840;

																	BgL_tmpz00_1840 =
																		((BgL_objectz00_bglt)
																		((BgL_typez00_bglt) BgL_superz00_939));
																	BgL_auxz00_1839 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_1840);
																}
																BgL_auxz00_1838 =
																	((BgL_tclassz00_bglt) BgL_auxz00_1839);
															}
															BgL_arg1328z00_948 =
																(((BgL_tclassz00_bglt)
																	COBJECT(BgL_auxz00_1838))->
																BgL_itszd2superzd2);
														}
														BgL_arg1329z00_949 =
															BGl_appendzd221011zd2zzobject_coercionz00
															(BgL_coercsz00_947, BgL_coercersz00_940);
														BgL_arg1331z00_950 =
															MAKE_YOUNG_PAIR(BgL_pragz00_946,
															BgL_pragmasz00_941);
														{
															obj_t BgL_pragmasz00_1850;
															obj_t BgL_coercersz00_1849;
															obj_t BgL_superz00_1848;

															BgL_superz00_1848 = BgL_arg1328z00_948;
															BgL_coercersz00_1849 = BgL_arg1329z00_949;
															BgL_pragmasz00_1850 = BgL_arg1331z00_950;
															BgL_pragmasz00_941 = BgL_pragmasz00_1850;
															BgL_coercersz00_940 = BgL_coercersz00_1849;
															BgL_superz00_939 = BgL_superz00_1848;
															goto BgL_zc3z04anonymousza31326ze3z87_942;
														}
													}
												}
											}
										}
									else
										{	/* Object/coercion.scm 151 */
											{	/* Object/coercion.scm 153 */
												obj_t BgL_arg1332z00_951;

												{	/* Object/coercion.scm 153 */
													obj_t BgL_arg1333z00_952;

													BgL_arg1333z00_952 =
														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
														(BgL_coercersz00_940, BNIL);
													BgL_arg1332z00_951 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(12),
														BgL_arg1333z00_952);
												}
												BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
													(BgL_arg1332z00_951);
											}
											{
												obj_t BgL_l1121z00_954;

												BgL_l1121z00_954 = BgL_pragmasz00_941;
											BgL_zc3z04anonymousza31334ze3z87_955:
												if (PAIRP(BgL_l1121z00_954))
													{	/* Object/coercion.scm 154 */
														BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
															(CAR(BgL_l1121z00_954));
														{
															obj_t BgL_l1121z00_1859;

															BgL_l1121z00_1859 = CDR(BgL_l1121z00_954);
															BgL_l1121z00_954 = BgL_l1121z00_1859;
															goto BgL_zc3z04anonymousza31334ze3z87_955;
														}
													}
												else
													{	/* Object/coercion.scm 154 */
														return ((bool_t) 1);
													}
											}
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* make-one-coercion~0 */
	obj_t BGl_makezd2onezd2coercionze70ze7zzobject_coercionz00(obj_t
		BgL_fromzd2idzd2_962, obj_t BgL_fromzd2namezd2_963,
		obj_t BgL_tozd2idzd2_964, obj_t BgL_tozd2namezd2_965)
	{
		{	/* Object/coercion.scm 143 */
			{	/* Object/coercion.scm 136 */
				obj_t BgL_tzd2ze3fz31_967;
				obj_t BgL_fzd2ze3tz31_968;

				{	/* Object/coercion.scm 136 */
					obj_t BgL_list1503z00_1003;

					{	/* Object/coercion.scm 136 */
						obj_t BgL_arg1509z00_1004;

						{	/* Object/coercion.scm 136 */
							obj_t BgL_arg1513z00_1005;

							BgL_arg1513z00_1005 = MAKE_YOUNG_PAIR(BgL_fromzd2idzd2_962, BNIL);
							BgL_arg1509z00_1004 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(13), BgL_arg1513z00_1005);
						}
						BgL_list1503z00_1003 =
							MAKE_YOUNG_PAIR(BgL_tozd2idzd2_964, BgL_arg1509z00_1004);
					}
					BgL_tzd2ze3fz31_967 =
						BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(BgL_list1503z00_1003);
				}
				{	/* Object/coercion.scm 137 */
					obj_t BgL_list1514z00_1006;

					{	/* Object/coercion.scm 137 */
						obj_t BgL_arg1516z00_1007;

						{	/* Object/coercion.scm 137 */
							obj_t BgL_arg1535z00_1008;

							BgL_arg1535z00_1008 = MAKE_YOUNG_PAIR(BgL_tozd2idzd2_964, BNIL);
							BgL_arg1516z00_1007 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(13), BgL_arg1535z00_1008);
						}
						BgL_list1514z00_1006 =
							MAKE_YOUNG_PAIR(BgL_fromzd2idzd2_962, BgL_arg1516z00_1007);
					}
					BgL_fzd2ze3tz31_968 =
						BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(BgL_list1514z00_1006);
				}
				{	/* Object/coercion.scm 138 */
					obj_t BgL_val0_1119z00_969;
					obj_t BgL_val1_1120z00_970;

					{	/* Object/coercion.scm 138 */
						obj_t BgL_arg1343z00_971;

						{	/* Object/coercion.scm 138 */
							obj_t BgL_arg1346z00_972;
							obj_t BgL_arg1348z00_973;

							{	/* Object/coercion.scm 138 */
								obj_t BgL_arg1349z00_974;

								{	/* Object/coercion.scm 138 */
									obj_t BgL_arg1351z00_975;

									{	/* Object/coercion.scm 138 */
										obj_t BgL_arg1352z00_976;

										{	/* Object/coercion.scm 138 */
											obj_t BgL_arg1361z00_977;

											{	/* Object/coercion.scm 138 */
												obj_t BgL_arg1364z00_978;

												{	/* Object/coercion.scm 138 */
													obj_t BgL_arg1367z00_979;

													BgL_arg1367z00_979 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(14), BNIL);
													BgL_arg1364z00_978 =
														MAKE_YOUNG_PAIR(BgL_arg1367z00_979, BNIL);
												}
												BgL_arg1361z00_977 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(15),
													BgL_arg1364z00_978);
											}
											BgL_arg1352z00_976 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(16), BgL_arg1361z00_977);
										}
										BgL_arg1351z00_975 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(17), BgL_arg1352z00_976);
									}
									BgL_arg1349z00_974 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(18), BgL_arg1351z00_975);
								}
								BgL_arg1346z00_972 =
									MAKE_YOUNG_PAIR(BgL_tzd2ze3fz31_967, BgL_arg1349z00_974);
							}
							{	/* Object/coercion.scm 139 */
								obj_t BgL_arg1370z00_980;

								{	/* Object/coercion.scm 139 */
									obj_t BgL_arg1371z00_981;

									{	/* Object/coercion.scm 139 */
										obj_t BgL_arg1375z00_982;

										{	/* Object/coercion.scm 139 */
											obj_t BgL_arg1376z00_983;

											{	/* Object/coercion.scm 139 */
												obj_t BgL_arg1377z00_984;

												{	/* Object/coercion.scm 139 */
													obj_t BgL_arg1378z00_985;

													{	/* Object/coercion.scm 139 */
														obj_t BgL_arg1379z00_986;

														BgL_arg1379z00_986 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(14), BNIL);
														BgL_arg1378z00_985 =
															MAKE_YOUNG_PAIR(BgL_arg1379z00_986, BNIL);
													}
													BgL_arg1377z00_984 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(15),
														BgL_arg1378z00_985);
												}
												BgL_arg1376z00_983 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(16),
													BgL_arg1377z00_984);
											}
											BgL_arg1375z00_982 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(17), BgL_arg1376z00_983);
										}
										BgL_arg1371z00_981 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(18), BgL_arg1375z00_982);
									}
									BgL_arg1370z00_980 =
										MAKE_YOUNG_PAIR(BgL_fzd2ze3tz31_968, BgL_arg1371z00_981);
								}
								BgL_arg1348z00_973 = MAKE_YOUNG_PAIR(BgL_arg1370z00_980, BNIL);
							}
							BgL_arg1343z00_971 =
								MAKE_YOUNG_PAIR(BgL_arg1346z00_972, BgL_arg1348z00_973);
						}
						BgL_val0_1119z00_969 =
							MAKE_YOUNG_PAIR(CNST_TABLE_REF(19), BgL_arg1343z00_971);
					}
					{	/* Object/coercion.scm 140 */
						obj_t BgL_arg1380z00_987;
						obj_t BgL_arg1408z00_988;

						{	/* Object/coercion.scm 140 */
							obj_t BgL_arg1421z00_991;

							{	/* Object/coercion.scm 140 */
								obj_t BgL_arg1422z00_992;

								{	/* Object/coercion.scm 140 */
									obj_t BgL_arg1434z00_993;

									{	/* Object/coercion.scm 140 */
										obj_t BgL_arg1437z00_994;
										obj_t BgL_arg1448z00_995;

										BgL_arg1437z00_994 =
											MAKE_YOUNG_PAIR(BgL_tozd2idzd2_964, BNIL);
										BgL_arg1448z00_995 =
											MAKE_YOUNG_PAIR(string_append_3
											(BGl_string1615z00zzobject_coercionz00,
												BgL_fromzd2namezd2_963,
												BGl_string1616z00zzobject_coercionz00), BNIL);
										BgL_arg1434z00_993 =
											MAKE_YOUNG_PAIR(BgL_arg1437z00_994, BgL_arg1448z00_995);
									}
									BgL_arg1422z00_992 =
										MAKE_YOUNG_PAIR(BgL_tzd2ze3fz31_967, BgL_arg1434z00_993);
								}
								BgL_arg1421z00_991 =
									MAKE_YOUNG_PAIR(BgL_fromzd2idzd2_962, BgL_arg1422z00_992);
							}
							BgL_arg1380z00_987 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(20), BgL_arg1421z00_991);
						}
						{	/* Object/coercion.scm 142 */
							obj_t BgL_arg1454z00_997;

							{	/* Object/coercion.scm 142 */
								obj_t BgL_arg1472z00_998;

								{	/* Object/coercion.scm 142 */
									obj_t BgL_arg1473z00_999;

									{	/* Object/coercion.scm 142 */
										obj_t BgL_arg1485z00_1000;
										obj_t BgL_arg1489z00_1001;

										BgL_arg1485z00_1000 =
											MAKE_YOUNG_PAIR(BgL_fromzd2idzd2_962, BNIL);
										BgL_arg1489z00_1001 =
											MAKE_YOUNG_PAIR(string_append_3
											(BGl_string1615z00zzobject_coercionz00,
												BgL_tozd2namezd2_965,
												BGl_string1616z00zzobject_coercionz00), BNIL);
										BgL_arg1473z00_999 =
											MAKE_YOUNG_PAIR(BgL_arg1485z00_1000, BgL_arg1489z00_1001);
									}
									BgL_arg1472z00_998 =
										MAKE_YOUNG_PAIR(BgL_fzd2ze3tz31_968, BgL_arg1473z00_999);
								}
								BgL_arg1454z00_997 =
									MAKE_YOUNG_PAIR(BgL_tozd2idzd2_964, BgL_arg1472z00_998);
							}
							BgL_arg1408z00_988 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(20), BgL_arg1454z00_997);
						}
						{	/* Object/coercion.scm 140 */
							obj_t BgL_list1409z00_989;

							{	/* Object/coercion.scm 140 */
								obj_t BgL_arg1410z00_990;

								BgL_arg1410z00_990 = MAKE_YOUNG_PAIR(BgL_arg1408z00_988, BNIL);
								BgL_list1409z00_989 =
									MAKE_YOUNG_PAIR(BgL_arg1380z00_987, BgL_arg1410z00_990);
							}
							BgL_val1_1120z00_970 = BgL_list1409z00_989;
						}
					}
					{	/* Object/coercion.scm 138 */
						int BgL_tmpz00_1917;

						BgL_tmpz00_1917 = (int) (2L);
						BGL_MVALUES_NUMBER_SET(BgL_tmpz00_1917);
					}
					{	/* Object/coercion.scm 138 */
						int BgL_tmpz00_1920;

						BgL_tmpz00_1920 = (int) (1L);
						BGL_MVALUES_VAL_SET(BgL_tmpz00_1920, BgL_val1_1120z00_970);
					}
					return BgL_val0_1119z00_969;
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzobject_coercionz00(void)
	{
		{	/* Object/coercion.scm 21 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzobject_coercionz00(void)
	{
		{	/* Object/coercion.scm 21 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzobject_coercionz00(void)
	{
		{	/* Object/coercion.scm 21 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzobject_coercionz00(void)
	{
		{	/* Object/coercion.scm 21 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1617z00zzobject_coercionz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1617z00zzobject_coercionz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1617z00zzobject_coercionz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1617z00zzobject_coercionz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1617z00zzobject_coercionz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1617z00zzobject_coercionz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1617z00zzobject_coercionz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1617z00zzobject_coercionz00));
			BGl_modulezd2initializa7ationz75zztype_toolsz00(453414928L,
				BSTRING_TO_STRING(BGl_string1617z00zzobject_coercionz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1617z00zzobject_coercionz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1617z00zzobject_coercionz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1617z00zzobject_coercionz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1617z00zzobject_coercionz00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string1617z00zzobject_coercionz00));
			BGl_modulezd2initializa7ationz75zzobject_toolsz00(196511171L,
				BSTRING_TO_STRING(BGl_string1617z00zzobject_coercionz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1617z00zzobject_coercionz00));
			BGl_modulezd2initializa7ationz75zzmodule_impusez00(478324304L,
				BSTRING_TO_STRING(BGl_string1617z00zzobject_coercionz00));
			return
				BGl_modulezd2initializa7ationz75zzast_privatez00(135263837L,
				BSTRING_TO_STRING(BGl_string1617z00zzobject_coercionz00));
		}

	}

#ifdef __cplusplus
}
#endif
