/*===========================================================================*/
/*   (Object/java_access.scm)                                                */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Object/java_access.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_OBJECT_JAVAzd2ACCESSzd2_TYPE_DEFINITIONS
#define BGL_BgL_OBJECT_JAVAzd2ACCESSzd2_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_jclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		obj_t BgL_packagez00;
	}                *BgL_jclassz00_bglt;


#endif													// BGL_BgL_OBJECT_JAVAzd2ACCESSzd2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t
		BGl_z62importzd2javazd2classzd2accessorsz12za2zzobject_javazd2accesszd2
		(obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	extern obj_t
		BGl_genzd2javazd2classzd2slotszd2accessz12z12zzobject_getterz00
		(BgL_typez00_bglt, obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzobject_javazd2accesszd2 =
		BUNSPEC;
	extern obj_t
		BGl_genzd2javazd2classzd2coercionsz12zc0zzobject_coercionz00(obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t
		BGl_importzd2javazd2classzd2creatorzd2zzobject_creatorz00(BgL_typez00_bglt,
		obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzobject_javazd2accesszd2(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzobject_javazd2accesszd2(void);
	extern obj_t
		BGl_importzd2javazd2classzd2predz12zc0zzobject_predicatez00
		(BgL_typez00_bglt, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzobject_javazd2accesszd2(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzobject_javazd2accesszd2(void);
	extern obj_t BGl_jclassz00zzobject_classz00;
	BGL_EXPORTED_DECL obj_t
		BGl_importzd2javazd2classzd2accessorsz12zc0zzobject_javazd2accesszd2(obj_t,
		obj_t, BgL_typez00_bglt, bool_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzobject_javazd2accesszd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_impusez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_coercionz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_predicatez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_creatorz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_getterz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	static obj_t BGl_cnstzd2initzd2zzobject_javazd2accesszd2(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzobject_javazd2accesszd2(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzobject_javazd2accesszd2(void);
	static obj_t BGl_gczd2rootszd2initz00zzobject_javazd2accesszd2(void);
	extern obj_t
		BGl_makezd2javazd2classzd2slotszd2zzobject_slotsz00(BgL_typez00_bglt, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	static bool_t
		BGl_correctzd2javazd2classzf3zf3zzobject_javazd2accesszd2(BgL_typez00_bglt,
		obj_t);
	extern obj_t
		BGl_genzd2javazd2classzd2constructorszd2zzobject_creatorz00
		(BgL_typez00_bglt, obj_t, obj_t);
	static obj_t __cnst[1];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_importzd2javazd2classzd2accessorsz12zd2envz12zzobject_javazd2accesszd2,
		BgL_bgl_za762importza7d2java1207z00,
		BGl_z62importzd2javazd2classzd2accessorsz12za2zzobject_javazd2accesszd2, 0L,
		BUNSPEC, 6);
	      DEFINE_STRING(BGl_string1201z00zzobject_javazd2accesszd2,
		BgL_bgl_string1201za700za7za7o1208za7, "super of `", 10);
	      DEFINE_STRING(BGl_string1202z00zzobject_javazd2accesszd2,
		BgL_bgl_string1202za700za7za7o1209za7, "' is not a class", 16);
	      DEFINE_STRING(BGl_string1203z00zzobject_javazd2accesszd2,
		BgL_bgl_string1203za700za7za7o1210za7, "object_java-access", 18);
	      DEFINE_STRING(BGl_string1204z00zzobject_javazd2accesszd2,
		BgL_bgl_string1204za700za7za7o1211za7, "foreign ", 8);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzobject_javazd2accesszd2));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzobject_javazd2accesszd2(long
		BgL_checksumz00_988, char *BgL_fromz00_989)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzobject_javazd2accesszd2))
				{
					BGl_requirezd2initializa7ationz75zzobject_javazd2accesszd2 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzobject_javazd2accesszd2();
					BGl_libraryzd2moduleszd2initz00zzobject_javazd2accesszd2();
					BGl_cnstzd2initzd2zzobject_javazd2accesszd2();
					BGl_importedzd2moduleszd2initz00zzobject_javazd2accesszd2();
					return BGl_methodzd2initzd2zzobject_javazd2accesszd2();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzobject_javazd2accesszd2(void)
	{
		{	/* Object/java_access.scm 21 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"object_java-access");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "object_java-access");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"object_java-access");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "object_java-access");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"object_java-access");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"object_java-access");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"object_java-access");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"object_java-access");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"object_java-access");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzobject_javazd2accesszd2(void)
	{
		{	/* Object/java_access.scm 21 */
			{	/* Object/java_access.scm 21 */
				obj_t BgL_cportz00_977;

				{	/* Object/java_access.scm 21 */
					obj_t BgL_stringz00_984;

					BgL_stringz00_984 = BGl_string1204z00zzobject_javazd2accesszd2;
					{	/* Object/java_access.scm 21 */
						obj_t BgL_startz00_985;

						BgL_startz00_985 = BINT(0L);
						{	/* Object/java_access.scm 21 */
							obj_t BgL_endz00_986;

							BgL_endz00_986 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_984)));
							{	/* Object/java_access.scm 21 */

								BgL_cportz00_977 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_984, BgL_startz00_985, BgL_endz00_986);
				}}}}
				{
					long BgL_iz00_978;

					BgL_iz00_978 = 0L;
				BgL_loopz00_979:
					if ((BgL_iz00_978 == -1L))
						{	/* Object/java_access.scm 21 */
							return BUNSPEC;
						}
					else
						{	/* Object/java_access.scm 21 */
							{	/* Object/java_access.scm 21 */
								obj_t BgL_arg1206z00_980;

								{	/* Object/java_access.scm 21 */

									{	/* Object/java_access.scm 21 */
										obj_t BgL_locationz00_982;

										BgL_locationz00_982 = BBOOL(((bool_t) 0));
										{	/* Object/java_access.scm 21 */

											BgL_arg1206z00_980 =
												BGl_readz00zz__readerz00(BgL_cportz00_977,
												BgL_locationz00_982);
										}
									}
								}
								{	/* Object/java_access.scm 21 */
									int BgL_tmpz00_1016;

									BgL_tmpz00_1016 = (int) (BgL_iz00_978);
									CNST_TABLE_SET(BgL_tmpz00_1016, BgL_arg1206z00_980);
							}}
							{	/* Object/java_access.scm 21 */
								int BgL_auxz00_983;

								BgL_auxz00_983 = (int) ((BgL_iz00_978 - 1L));
								{
									long BgL_iz00_1021;

									BgL_iz00_1021 = (long) (BgL_auxz00_983);
									BgL_iz00_978 = BgL_iz00_1021;
									goto BgL_loopz00_979;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzobject_javazd2accesszd2(void)
	{
		{	/* Object/java_access.scm 21 */
			return bgl_gc_roots_register();
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzobject_javazd2accesszd2(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_712;

				BgL_headz00_712 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_713;
					obj_t BgL_tailz00_714;

					BgL_prevz00_713 = BgL_headz00_712;
					BgL_tailz00_714 = BgL_l1z00_1;
				BgL_loopz00_715:
					if (PAIRP(BgL_tailz00_714))
						{
							obj_t BgL_newzd2prevzd2_717;

							BgL_newzd2prevzd2_717 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_714), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_713, BgL_newzd2prevzd2_717);
							{
								obj_t BgL_tailz00_1031;
								obj_t BgL_prevz00_1030;

								BgL_prevz00_1030 = BgL_newzd2prevzd2_717;
								BgL_tailz00_1031 = CDR(BgL_tailz00_714);
								BgL_tailz00_714 = BgL_tailz00_1031;
								BgL_prevz00_713 = BgL_prevz00_1030;
								goto BgL_loopz00_715;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_712);
				}
			}
		}

	}



/* import-java-class-accessors! */
	BGL_EXPORTED_DEF obj_t
		BGl_importzd2javazd2classzd2accessorsz12zc0zzobject_javazd2accesszd2(obj_t
		BgL_pslotsz00_3, obj_t BgL_constrsz00_4, BgL_typez00_bglt BgL_classz00_5,
		bool_t BgL_abstractzf3zf3_6, obj_t BgL_modulez00_7, obj_t BgL_srcz00_8)
	{
		{	/* Object/java_access.scm 49 */
			if (BGl_correctzd2javazd2classzf3zf3zzobject_javazd2accesszd2
				(BgL_classz00_5, BgL_srcz00_8))
				{	/* Object/java_access.scm 51 */
					{	/* Object/java_access.scm 107 */
						obj_t BgL_cslotsz00_813;

						{	/* Object/java_access.scm 109 */
							obj_t BgL_arg1148z00_814;

							{
								BgL_jclassz00_bglt BgL_auxz00_1036;

								{
									obj_t BgL_auxz00_1037;

									{	/* Object/java_access.scm 109 */
										BgL_objectz00_bglt BgL_tmpz00_1038;

										BgL_tmpz00_1038 = ((BgL_objectz00_bglt) BgL_classz00_5);
										BgL_auxz00_1037 = BGL_OBJECT_WIDENING(BgL_tmpz00_1038);
									}
									BgL_auxz00_1036 = ((BgL_jclassz00_bglt) BgL_auxz00_1037);
								}
								BgL_arg1148z00_814 =
									(((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_1036))->
									BgL_itszd2superzd2);
							}
							BgL_cslotsz00_813 =
								BGl_makezd2javazd2classzd2slotszd2zzobject_slotsz00
								(BgL_classz00_5, BgL_pslotsz00_3, BgL_arg1148z00_814,
								BgL_srcz00_8);
						}
						{
							BgL_jclassz00_bglt BgL_auxz00_1044;

							{
								obj_t BgL_auxz00_1045;

								{	/* Object/java_access.scm 112 */
									BgL_objectz00_bglt BgL_tmpz00_1046;

									BgL_tmpz00_1046 = ((BgL_objectz00_bglt) BgL_classz00_5);
									BgL_auxz00_1045 = BGL_OBJECT_WIDENING(BgL_tmpz00_1046);
								}
								BgL_auxz00_1044 = ((BgL_jclassz00_bglt) BgL_auxz00_1045);
							}
							((((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_1044))->BgL_slotsz00) =
								((obj_t) BgL_cslotsz00_813), BUNSPEC);
						}
					}
					BGl_genzd2javazd2classzd2coercionsz12zc0zzobject_coercionz00(
						((obj_t) BgL_classz00_5));
					{	/* Object/java_access.scm 60 */
						obj_t BgL_fieldsz00_722;

						{	/* Object/java_access.scm 60 */
							obj_t BgL_arg1137z00_732;

							{
								BgL_jclassz00_bglt BgL_auxz00_1053;

								{
									obj_t BgL_auxz00_1054;

									{	/* Object/java_access.scm 60 */
										BgL_objectz00_bglt BgL_tmpz00_1055;

										BgL_tmpz00_1055 = ((BgL_objectz00_bglt) BgL_classz00_5);
										BgL_auxz00_1054 = BGL_OBJECT_WIDENING(BgL_tmpz00_1055);
									}
									BgL_auxz00_1053 = ((BgL_jclassz00_bglt) BgL_auxz00_1054);
								}
								BgL_arg1137z00_732 =
									(((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_1053))->
									BgL_slotsz00);
							}
							BgL_fieldsz00_722 =
								BGl_genzd2javazd2classzd2slotszd2accessz12z12zzobject_getterz00
								(BgL_classz00_5, BgL_arg1137z00_732, BgL_srcz00_8);
						}
						if (
							((((BgL_typez00_bglt) COBJECT(
											((BgL_typez00_bglt) BgL_classz00_5)))->BgL_idz00) ==
								CNST_TABLE_REF(0)))
							{	/* Object/java_access.scm 61 */
								return BgL_fieldsz00_722;
							}
						else
							{	/* Object/java_access.scm 63 */
								obj_t BgL_predz00_725;

								BgL_predz00_725 =
									BGl_importzd2javazd2classzd2predz12zc0zzobject_predicatez00
									(BgL_classz00_5, BgL_srcz00_8, BgL_modulez00_7);
								if (BgL_abstractzf3zf3_6)
									{	/* Object/java_access.scm 64 */
										return
											BGl_appendzd221011zd2zzobject_javazd2accesszd2
											(BgL_predz00_725, BgL_fieldsz00_722);
									}
								else
									{	/* Object/java_access.scm 66 */
										obj_t BgL_constz00_726;

										BgL_constz00_726 =
											BGl_genzd2javazd2classzd2constructorszd2zzobject_creatorz00
											(BgL_classz00_5, BgL_constrsz00_4, BgL_srcz00_8);
										{	/* Object/java_access.scm 66 */
											obj_t BgL_creatorz00_727;

											BgL_creatorz00_727 =
												BGl_importzd2javazd2classzd2creatorzd2zzobject_creatorz00
												(BgL_classz00_5, BgL_constrsz00_4, BgL_srcz00_8);
											{	/* Object/java_access.scm 68 */

												return
													BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
													(BgL_predz00_725,
													BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
													(BgL_creatorz00_727,
														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
														(BgL_fieldsz00_722,
															BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
															(BgL_constz00_726, BNIL))));
											}
										}
									}
							}
					}
				}
			else
				{	/* Object/java_access.scm 51 */
					return BNIL;
				}
		}

	}



/* &import-java-class-accessors! */
	obj_t
		BGl_z62importzd2javazd2classzd2accessorsz12za2zzobject_javazd2accesszd2
		(obj_t BgL_envz00_970, obj_t BgL_pslotsz00_971, obj_t BgL_constrsz00_972,
		obj_t BgL_classz00_973, obj_t BgL_abstractzf3zf3_974,
		obj_t BgL_modulez00_975, obj_t BgL_srcz00_976)
	{
		{	/* Object/java_access.scm 49 */
			return
				BGl_importzd2javazd2classzd2accessorsz12zc0zzobject_javazd2accesszd2
				(BgL_pslotsz00_971, BgL_constrsz00_972,
				((BgL_typez00_bglt) BgL_classz00_973), CBOOL(BgL_abstractzf3zf3_974),
				BgL_modulez00_975, BgL_srcz00_976);
		}

	}



/* correct-java-class? */
	bool_t
		BGl_correctzd2javazd2classzf3zf3zzobject_javazd2accesszd2(BgL_typez00_bglt
		BgL_classz00_9, obj_t BgL_srcz00_10)
	{
		{	/* Object/java_access.scm 80 */
			{	/* Object/java_access.scm 81 */
				obj_t BgL_superz00_733;
				obj_t BgL_classzd2idzd2_734;

				{
					BgL_jclassz00_bglt BgL_auxz00_1078;

					{
						obj_t BgL_auxz00_1079;

						{	/* Object/java_access.scm 81 */
							BgL_objectz00_bglt BgL_tmpz00_1080;

							BgL_tmpz00_1080 = ((BgL_objectz00_bglt) BgL_classz00_9);
							BgL_auxz00_1079 = BGL_OBJECT_WIDENING(BgL_tmpz00_1080);
						}
						BgL_auxz00_1078 = ((BgL_jclassz00_bglt) BgL_auxz00_1079);
					}
					BgL_superz00_733 =
						(((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_1078))->
						BgL_itszd2superzd2);
				}
				BgL_classzd2idzd2_734 =
					(((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_classz00_9)))->BgL_idz00);
				{	/* Object/java_access.scm 86 */
					bool_t BgL_test1218z00_1087;

					{	/* Object/java_access.scm 86 */
						bool_t BgL_test1219z00_1088;

						{	/* Object/java_access.scm 86 */
							obj_t BgL_classz00_823;

							BgL_classz00_823 = BGl_typez00zztype_typez00;
							if (BGL_OBJECTP(BgL_superz00_733))
								{	/* Object/java_access.scm 86 */
									BgL_objectz00_bglt BgL_arg1807z00_825;

									BgL_arg1807z00_825 = (BgL_objectz00_bglt) (BgL_superz00_733);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Object/java_access.scm 86 */
											long BgL_idxz00_831;

											BgL_idxz00_831 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_825);
											BgL_test1219z00_1088 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_831 + 1L)) == BgL_classz00_823);
										}
									else
										{	/* Object/java_access.scm 86 */
											bool_t BgL_res1197z00_856;

											{	/* Object/java_access.scm 86 */
												obj_t BgL_oclassz00_839;

												{	/* Object/java_access.scm 86 */
													obj_t BgL_arg1815z00_847;
													long BgL_arg1816z00_848;

													BgL_arg1815z00_847 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Object/java_access.scm 86 */
														long BgL_arg1817z00_849;

														BgL_arg1817z00_849 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_825);
														BgL_arg1816z00_848 =
															(BgL_arg1817z00_849 - OBJECT_TYPE);
													}
													BgL_oclassz00_839 =
														VECTOR_REF(BgL_arg1815z00_847, BgL_arg1816z00_848);
												}
												{	/* Object/java_access.scm 86 */
													bool_t BgL__ortest_1115z00_840;

													BgL__ortest_1115z00_840 =
														(BgL_classz00_823 == BgL_oclassz00_839);
													if (BgL__ortest_1115z00_840)
														{	/* Object/java_access.scm 86 */
															BgL_res1197z00_856 = BgL__ortest_1115z00_840;
														}
													else
														{	/* Object/java_access.scm 86 */
															long BgL_odepthz00_841;

															{	/* Object/java_access.scm 86 */
																obj_t BgL_arg1804z00_842;

																BgL_arg1804z00_842 = (BgL_oclassz00_839);
																BgL_odepthz00_841 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_842);
															}
															if ((1L < BgL_odepthz00_841))
																{	/* Object/java_access.scm 86 */
																	obj_t BgL_arg1802z00_844;

																	{	/* Object/java_access.scm 86 */
																		obj_t BgL_arg1803z00_845;

																		BgL_arg1803z00_845 = (BgL_oclassz00_839);
																		BgL_arg1802z00_844 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_845, 1L);
																	}
																	BgL_res1197z00_856 =
																		(BgL_arg1802z00_844 == BgL_classz00_823);
																}
															else
																{	/* Object/java_access.scm 86 */
																	BgL_res1197z00_856 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test1219z00_1088 = BgL_res1197z00_856;
										}
								}
							else
								{	/* Object/java_access.scm 86 */
									BgL_test1219z00_1088 = ((bool_t) 0);
								}
						}
						if (BgL_test1219z00_1088)
							{	/* Object/java_access.scm 86 */
								bool_t BgL_test1224z00_1111;

								{	/* Object/java_access.scm 86 */
									obj_t BgL_classz00_857;

									BgL_classz00_857 = BGl_jclassz00zzobject_classz00;
									if (BGL_OBJECTP(BgL_superz00_733))
										{	/* Object/java_access.scm 86 */
											BgL_objectz00_bglt BgL_arg1807z00_859;

											BgL_arg1807z00_859 =
												(BgL_objectz00_bglt) (BgL_superz00_733);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Object/java_access.scm 86 */
													long BgL_idxz00_865;

													BgL_idxz00_865 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_859);
													BgL_test1224z00_1111 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_865 + 2L)) == BgL_classz00_857);
												}
											else
												{	/* Object/java_access.scm 86 */
													bool_t BgL_res1198z00_890;

													{	/* Object/java_access.scm 86 */
														obj_t BgL_oclassz00_873;

														{	/* Object/java_access.scm 86 */
															obj_t BgL_arg1815z00_881;
															long BgL_arg1816z00_882;

															BgL_arg1815z00_881 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Object/java_access.scm 86 */
																long BgL_arg1817z00_883;

																BgL_arg1817z00_883 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_859);
																BgL_arg1816z00_882 =
																	(BgL_arg1817z00_883 - OBJECT_TYPE);
															}
															BgL_oclassz00_873 =
																VECTOR_REF(BgL_arg1815z00_881,
																BgL_arg1816z00_882);
														}
														{	/* Object/java_access.scm 86 */
															bool_t BgL__ortest_1115z00_874;

															BgL__ortest_1115z00_874 =
																(BgL_classz00_857 == BgL_oclassz00_873);
															if (BgL__ortest_1115z00_874)
																{	/* Object/java_access.scm 86 */
																	BgL_res1198z00_890 = BgL__ortest_1115z00_874;
																}
															else
																{	/* Object/java_access.scm 86 */
																	long BgL_odepthz00_875;

																	{	/* Object/java_access.scm 86 */
																		obj_t BgL_arg1804z00_876;

																		BgL_arg1804z00_876 = (BgL_oclassz00_873);
																		BgL_odepthz00_875 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_876);
																	}
																	if ((2L < BgL_odepthz00_875))
																		{	/* Object/java_access.scm 86 */
																			obj_t BgL_arg1802z00_878;

																			{	/* Object/java_access.scm 86 */
																				obj_t BgL_arg1803z00_879;

																				BgL_arg1803z00_879 =
																					(BgL_oclassz00_873);
																				BgL_arg1802z00_878 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_879, 2L);
																			}
																			BgL_res1198z00_890 =
																				(BgL_arg1802z00_878 ==
																				BgL_classz00_857);
																		}
																	else
																		{	/* Object/java_access.scm 86 */
																			BgL_res1198z00_890 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test1224z00_1111 = BgL_res1198z00_890;
												}
										}
									else
										{	/* Object/java_access.scm 86 */
											BgL_test1224z00_1111 = ((bool_t) 0);
										}
								}
								if (BgL_test1224z00_1111)
									{	/* Object/java_access.scm 86 */
										BgL_test1218z00_1087 = ((bool_t) 0);
									}
								else
									{	/* Object/java_access.scm 86 */
										BgL_test1218z00_1087 = ((bool_t) 1);
									}
							}
						else
							{	/* Object/java_access.scm 86 */
								BgL_test1218z00_1087 = ((bool_t) 0);
							}
					}
					if (BgL_test1218z00_1087)
						{	/* Object/java_access.scm 86 */
							{	/* Object/java_access.scm 87 */
								obj_t BgL_arg1141z00_738;
								obj_t BgL_arg1142z00_739;

								BgL_arg1141z00_738 =
									(((BgL_typez00_bglt) COBJECT(
											((BgL_typez00_bglt) BgL_superz00_733)))->BgL_idz00);
								{	/* Object/java_access.scm 89 */
									obj_t BgL_arg1145z00_741;

									{	/* Object/java_access.scm 89 */
										obj_t BgL_arg1455z00_893;

										BgL_arg1455z00_893 =
											SYMBOL_TO_STRING(BgL_classzd2idzd2_734);
										BgL_arg1145z00_741 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_893);
									}
									BgL_arg1142z00_739 =
										string_append_3(BGl_string1201z00zzobject_javazd2accesszd2,
										BgL_arg1145z00_741,
										BGl_string1202z00zzobject_javazd2accesszd2);
								}
								{	/* Object/java_access.scm 87 */
									obj_t BgL_list1143z00_740;

									BgL_list1143z00_740 =
										MAKE_YOUNG_PAIR(BGl_typez00zztype_typez00, BNIL);
									BGl_userzd2errorzd2zztools_errorz00(BgL_arg1141z00_738,
										BgL_arg1142z00_739, BgL_srcz00_10, BgL_list1143z00_740);
								}
							}
							return ((bool_t) 0);
						}
					else
						{	/* Object/java_access.scm 86 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzobject_javazd2accesszd2(void)
	{
		{	/* Object/java_access.scm 21 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzobject_javazd2accesszd2(void)
	{
		{	/* Object/java_access.scm 21 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzobject_javazd2accesszd2(void)
	{
		{	/* Object/java_access.scm 21 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzobject_javazd2accesszd2(void)
	{
		{	/* Object/java_access.scm 21 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1203z00zzobject_javazd2accesszd2));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1203z00zzobject_javazd2accesszd2));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1203z00zzobject_javazd2accesszd2));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1203z00zzobject_javazd2accesszd2));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1203z00zzobject_javazd2accesszd2));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1203z00zzobject_javazd2accesszd2));
			BGl_modulezd2initializa7ationz75zztype_toolsz00(453414928L,
				BSTRING_TO_STRING(BGl_string1203z00zzobject_javazd2accesszd2));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1203z00zzobject_javazd2accesszd2));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1203z00zzobject_javazd2accesszd2));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1203z00zzobject_javazd2accesszd2));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1203z00zzobject_javazd2accesszd2));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string1203z00zzobject_javazd2accesszd2));
			BGl_modulezd2initializa7ationz75zzobject_toolsz00(196511171L,
				BSTRING_TO_STRING(BGl_string1203z00zzobject_javazd2accesszd2));
			BGl_modulezd2initializa7ationz75zzobject_getterz00(23938048L,
				BSTRING_TO_STRING(BGl_string1203z00zzobject_javazd2accesszd2));
			BGl_modulezd2initializa7ationz75zzobject_creatorz00(508385191L,
				BSTRING_TO_STRING(BGl_string1203z00zzobject_javazd2accesszd2));
			BGl_modulezd2initializa7ationz75zzobject_predicatez00(458696231L,
				BSTRING_TO_STRING(BGl_string1203z00zzobject_javazd2accesszd2));
			BGl_modulezd2initializa7ationz75zzobject_coercionz00(208160543L,
				BSTRING_TO_STRING(BGl_string1203z00zzobject_javazd2accesszd2));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1203z00zzobject_javazd2accesszd2));
			BGl_modulezd2initializa7ationz75zzmodule_impusez00(478324304L,
				BSTRING_TO_STRING(BGl_string1203z00zzobject_javazd2accesszd2));
			return
				BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1203z00zzobject_javazd2accesszd2));
		}

	}

#ifdef __cplusplus
}
#endif
