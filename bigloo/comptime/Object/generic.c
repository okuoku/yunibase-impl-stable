/*===========================================================================*/
/*   (Object/generic.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Object/generic.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_OBJECT_GENERIC_TYPE_DEFINITIONS
#define BGL_OBJECT_GENERIC_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;


#endif													// BGL_OBJECT_GENERIC_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzobject_genericz00 = BUNSPEC;
	extern obj_t BGl_makezd2typedzd2identz00zzast_identz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_tclassz00zzobject_classz00;
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t BGl_genericzd2initzd2zzobject_genericz00(void);
	extern BgL_typez00_bglt BGl_getzd2defaultzd2typez00zztype_cachez00(void);
	static obj_t BGl_objectzd2initzd2zzobject_genericz00(void);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_typez00zztype_typez00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzobject_genericz00(void);
	BGL_EXPORTED_DECL obj_t BGl_makezd2genericzd2bodyz00zzobject_genericz00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzobject_genericz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_privatez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_argsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	extern obj_t BGl_makezd2privatezd2sexpz00zzast_privatez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_makezd2objectzd2genericzd2bodyzd2zzobject_genericz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzobject_genericz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzobject_genericz00(void);
	extern long BGl_globalzd2arityzd2zztools_argsz00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzobject_genericz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzobject_genericz00(void);
	extern obj_t BGl_parsezd2idzd2zzast_identz00(obj_t, obj_t);
	static obj_t BGl_z62makezd2genericzd2bodyz62zzobject_genericz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_findzd2globalzf2modulez20zzast_envz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t
		BGl_makezd2nonzd2objectzd2genericzd2bodyz00zzobject_genericz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t __cnst[14];


	   
		 
		DEFINE_STRING(BGl_string1484z00zzobject_genericz00,
		BgL_bgl_string1484za700za7za7o1490za7,
		"Illegal generic definition (no formal arguments provided)", 57);
	      DEFINE_STRING(BGl_string1485z00zzobject_genericz00,
		BgL_bgl_string1485za700za7za7o1491za7,
		"Illegal global definition (internal error)", 42);
	      DEFINE_STRING(BGl_string1486z00zzobject_genericz00,
		BgL_bgl_string1486za700za7za7o1492za7,
		"generic function has a non-class dispatching type arg", 53);
	      DEFINE_STRING(BGl_string1487z00zzobject_genericz00,
		BgL_bgl_string1487za700za7za7o1493za7, "object_generic", 14);
	      DEFINE_STRING(BGl_string1488z00zzobject_genericz00,
		BgL_bgl_string1488za700za7za7o1494za7,
		"if object? __r4_pairs_and_lists_6_3 generic-default let apply cons* _ unsafe find-method @ procedure method sgfun ",
		114);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2genericzd2bodyzd2envzd2zzobject_genericz00,
		BgL_bgl_za762makeza7d2generi1495z00,
		BGl_z62makezd2genericzd2bodyz62zzobject_genericz00, 0L, BUNSPEC, 4);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzobject_genericz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzobject_genericz00(long
		BgL_checksumz00_1351, char *BgL_fromz00_1352)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzobject_genericz00))
				{
					BGl_requirezd2initializa7ationz75zzobject_genericz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzobject_genericz00();
					BGl_libraryzd2moduleszd2initz00zzobject_genericz00();
					BGl_cnstzd2initzd2zzobject_genericz00();
					BGl_importedzd2moduleszd2initz00zzobject_genericz00();
					return BGl_methodzd2initzd2zzobject_genericz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzobject_genericz00(void)
	{
		{	/* Object/generic.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"object_generic");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "object_generic");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "object_generic");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"object_generic");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"object_generic");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"object_generic");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"object_generic");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"object_generic");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"object_generic");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzobject_genericz00(void)
	{
		{	/* Object/generic.scm 15 */
			{	/* Object/generic.scm 15 */
				obj_t BgL_cportz00_1340;

				{	/* Object/generic.scm 15 */
					obj_t BgL_stringz00_1347;

					BgL_stringz00_1347 = BGl_string1488z00zzobject_genericz00;
					{	/* Object/generic.scm 15 */
						obj_t BgL_startz00_1348;

						BgL_startz00_1348 = BINT(0L);
						{	/* Object/generic.scm 15 */
							obj_t BgL_endz00_1349;

							BgL_endz00_1349 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1347)));
							{	/* Object/generic.scm 15 */

								BgL_cportz00_1340 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1347, BgL_startz00_1348, BgL_endz00_1349);
				}}}}
				{
					long BgL_iz00_1341;

					BgL_iz00_1341 = 13L;
				BgL_loopz00_1342:
					if ((BgL_iz00_1341 == -1L))
						{	/* Object/generic.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Object/generic.scm 15 */
							{	/* Object/generic.scm 15 */
								obj_t BgL_arg1489z00_1343;

								{	/* Object/generic.scm 15 */

									{	/* Object/generic.scm 15 */
										obj_t BgL_locationz00_1345;

										BgL_locationz00_1345 = BBOOL(((bool_t) 0));
										{	/* Object/generic.scm 15 */

											BgL_arg1489z00_1343 =
												BGl_readz00zz__readerz00(BgL_cportz00_1340,
												BgL_locationz00_1345);
										}
									}
								}
								{	/* Object/generic.scm 15 */
									int BgL_tmpz00_1379;

									BgL_tmpz00_1379 = (int) (BgL_iz00_1341);
									CNST_TABLE_SET(BgL_tmpz00_1379, BgL_arg1489z00_1343);
							}}
							{	/* Object/generic.scm 15 */
								int BgL_auxz00_1346;

								BgL_auxz00_1346 = (int) ((BgL_iz00_1341 - 1L));
								{
									long BgL_iz00_1384;

									BgL_iz00_1384 = (long) (BgL_auxz00_1346);
									BgL_iz00_1341 = BgL_iz00_1384;
									goto BgL_loopz00_1342;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzobject_genericz00(void)
	{
		{	/* Object/generic.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* make-generic-body */
	BGL_EXPORTED_DEF obj_t BGl_makezd2genericzd2bodyz00zzobject_genericz00(obj_t
		BgL_idz00_3, obj_t BgL_localsz00_4, obj_t BgL_argsz00_5, obj_t BgL_srcz00_6)
	{
		{	/* Object/generic.scm 34 */
			{	/* Object/generic.scm 37 */
				obj_t BgL_gz00_689;

				BgL_gz00_689 =
					BGl_findzd2globalzf2modulez20zzast_envz00(BgL_idz00_3,
					BGl_za2moduleza2z00zzmodule_modulez00);
				if (NULLP(BgL_argsz00_5))
					{	/* Object/generic.scm 36 */
						obj_t BgL_list1191z00_922;

						BgL_list1191z00_922 = MAKE_YOUNG_PAIR(BNIL, BNIL);
						return
							BGl_userzd2errorzd2zztools_errorz00(BgL_idz00_3,
							BGl_string1484z00zzobject_genericz00, BgL_srcz00_6,
							BgL_list1191z00_922);
					}
				else
					{	/* Object/generic.scm 41 */
						bool_t BgL_test1499z00_1392;

						{	/* Object/generic.scm 41 */
							bool_t BgL_test1500z00_1393;

							{	/* Object/generic.scm 41 */
								obj_t BgL_classz00_923;

								BgL_classz00_923 = BGl_globalz00zzast_varz00;
								if (BGL_OBJECTP(BgL_gz00_689))
									{	/* Object/generic.scm 41 */
										BgL_objectz00_bglt BgL_arg1807z00_925;

										BgL_arg1807z00_925 = (BgL_objectz00_bglt) (BgL_gz00_689);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Object/generic.scm 41 */
												long BgL_idxz00_931;

												BgL_idxz00_931 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_925);
												BgL_test1500z00_1393 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_931 + 2L)) == BgL_classz00_923);
											}
										else
											{	/* Object/generic.scm 41 */
												bool_t BgL_res1473z00_956;

												{	/* Object/generic.scm 41 */
													obj_t BgL_oclassz00_939;

													{	/* Object/generic.scm 41 */
														obj_t BgL_arg1815z00_947;
														long BgL_arg1816z00_948;

														BgL_arg1815z00_947 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Object/generic.scm 41 */
															long BgL_arg1817z00_949;

															BgL_arg1817z00_949 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_925);
															BgL_arg1816z00_948 =
																(BgL_arg1817z00_949 - OBJECT_TYPE);
														}
														BgL_oclassz00_939 =
															VECTOR_REF(BgL_arg1815z00_947,
															BgL_arg1816z00_948);
													}
													{	/* Object/generic.scm 41 */
														bool_t BgL__ortest_1115z00_940;

														BgL__ortest_1115z00_940 =
															(BgL_classz00_923 == BgL_oclassz00_939);
														if (BgL__ortest_1115z00_940)
															{	/* Object/generic.scm 41 */
																BgL_res1473z00_956 = BgL__ortest_1115z00_940;
															}
														else
															{	/* Object/generic.scm 41 */
																long BgL_odepthz00_941;

																{	/* Object/generic.scm 41 */
																	obj_t BgL_arg1804z00_942;

																	BgL_arg1804z00_942 = (BgL_oclassz00_939);
																	BgL_odepthz00_941 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_942);
																}
																if ((2L < BgL_odepthz00_941))
																	{	/* Object/generic.scm 41 */
																		obj_t BgL_arg1802z00_944;

																		{	/* Object/generic.scm 41 */
																			obj_t BgL_arg1803z00_945;

																			BgL_arg1803z00_945 = (BgL_oclassz00_939);
																			BgL_arg1802z00_944 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_945, 2L);
																		}
																		BgL_res1473z00_956 =
																			(BgL_arg1802z00_944 == BgL_classz00_923);
																	}
																else
																	{	/* Object/generic.scm 41 */
																		BgL_res1473z00_956 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test1500z00_1393 = BgL_res1473z00_956;
											}
									}
								else
									{	/* Object/generic.scm 41 */
										BgL_test1500z00_1393 = ((bool_t) 0);
									}
							}
							if (BgL_test1500z00_1393)
								{	/* Object/generic.scm 42 */
									bool_t BgL__ortest_1064z00_731;

									{	/* Object/generic.scm 42 */
										bool_t BgL_test1505z00_1416;

										{	/* Object/generic.scm 42 */
											BgL_valuez00_bglt BgL_arg1189z00_739;

											BgL_arg1189z00_739 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_globalz00_bglt) BgL_gz00_689))))->
												BgL_valuez00);
											{	/* Object/generic.scm 42 */
												obj_t BgL_classz00_958;

												BgL_classz00_958 = BGl_sfunz00zzast_varz00;
												{	/* Object/generic.scm 42 */
													BgL_objectz00_bglt BgL_arg1807z00_960;

													{	/* Object/generic.scm 42 */
														obj_t BgL_tmpz00_1420;

														BgL_tmpz00_1420 =
															((obj_t)
															((BgL_objectz00_bglt) BgL_arg1189z00_739));
														BgL_arg1807z00_960 =
															(BgL_objectz00_bglt) (BgL_tmpz00_1420);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Object/generic.scm 42 */
															long BgL_idxz00_966;

															BgL_idxz00_966 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_960);
															BgL_test1505z00_1416 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_966 + 3L)) == BgL_classz00_958);
														}
													else
														{	/* Object/generic.scm 42 */
															bool_t BgL_res1474z00_991;

															{	/* Object/generic.scm 42 */
																obj_t BgL_oclassz00_974;

																{	/* Object/generic.scm 42 */
																	obj_t BgL_arg1815z00_982;
																	long BgL_arg1816z00_983;

																	BgL_arg1815z00_982 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Object/generic.scm 42 */
																		long BgL_arg1817z00_984;

																		BgL_arg1817z00_984 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_960);
																		BgL_arg1816z00_983 =
																			(BgL_arg1817z00_984 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_974 =
																		VECTOR_REF(BgL_arg1815z00_982,
																		BgL_arg1816z00_983);
																}
																{	/* Object/generic.scm 42 */
																	bool_t BgL__ortest_1115z00_975;

																	BgL__ortest_1115z00_975 =
																		(BgL_classz00_958 == BgL_oclassz00_974);
																	if (BgL__ortest_1115z00_975)
																		{	/* Object/generic.scm 42 */
																			BgL_res1474z00_991 =
																				BgL__ortest_1115z00_975;
																		}
																	else
																		{	/* Object/generic.scm 42 */
																			long BgL_odepthz00_976;

																			{	/* Object/generic.scm 42 */
																				obj_t BgL_arg1804z00_977;

																				BgL_arg1804z00_977 =
																					(BgL_oclassz00_974);
																				BgL_odepthz00_976 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_977);
																			}
																			if ((3L < BgL_odepthz00_976))
																				{	/* Object/generic.scm 42 */
																					obj_t BgL_arg1802z00_979;

																					{	/* Object/generic.scm 42 */
																						obj_t BgL_arg1803z00_980;

																						BgL_arg1803z00_980 =
																							(BgL_oclassz00_974);
																						BgL_arg1802z00_979 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_980, 3L);
																					}
																					BgL_res1474z00_991 =
																						(BgL_arg1802z00_979 ==
																						BgL_classz00_958);
																				}
																			else
																				{	/* Object/generic.scm 42 */
																					BgL_res1474z00_991 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test1505z00_1416 = BgL_res1474z00_991;
														}
												}
											}
										}
										if (BgL_test1505z00_1416)
											{	/* Object/generic.scm 42 */
												BgL__ortest_1064z00_731 = ((bool_t) 0);
											}
										else
											{	/* Object/generic.scm 42 */
												BgL__ortest_1064z00_731 = ((bool_t) 1);
											}
									}
									if (BgL__ortest_1064z00_731)
										{	/* Object/generic.scm 42 */
											BgL_test1499z00_1392 = BgL__ortest_1064z00_731;
										}
									else
										{	/* Object/generic.scm 43 */
											bool_t BgL__ortest_1065z00_732;

											if (
												((((BgL_sfunz00_bglt) COBJECT(
																((BgL_sfunz00_bglt)
																	(((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					((BgL_globalz00_bglt)
																						BgL_gz00_689))))->BgL_valuez00))))->
														BgL_classz00) == CNST_TABLE_REF(0)))
												{	/* Object/generic.scm 43 */
													BgL__ortest_1065z00_732 = ((bool_t) 0);
												}
											else
												{	/* Object/generic.scm 43 */
													BgL__ortest_1065z00_732 = ((bool_t) 1);
												}
											if (BgL__ortest_1065z00_732)
												{	/* Object/generic.scm 43 */
													BgL_test1499z00_1392 = BgL__ortest_1065z00_732;
												}
											else
												{	/* Object/generic.scm 43 */
													BgL_test1499z00_1392 =
														NULLP(
														(((BgL_sfunz00_bglt) COBJECT(
																	((BgL_sfunz00_bglt)
																		(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						((BgL_globalz00_bglt)
																							BgL_gz00_689))))->
																			BgL_valuez00))))->BgL_argsz00));
												}
										}
								}
							else
								{	/* Object/generic.scm 41 */
									BgL_test1499z00_1392 = ((bool_t) 0);
								}
						}
						if (BgL_test1499z00_1392)
							{	/* Object/generic.scm 36 */
								obj_t BgL_list1191z00_996;

								BgL_list1191z00_996 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								return
									BGl_userzd2errorzd2zztools_errorz00(BgL_idz00_3,
									BGl_string1485z00zzobject_genericz00, BgL_srcz00_6,
									BgL_list1191z00_996);
							}
						else
							{	/* Object/generic.scm 47 */
								obj_t BgL_methodzd2argzd2_705;

								BgL_methodzd2argzd2_705 = CAR(((obj_t) BgL_localsz00_4));
								{	/* Object/generic.scm 47 */
									BgL_typez00_bglt BgL_methodzd2argzd2typez00_706;

									BgL_methodzd2argzd2typez00_706 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_methodzd2argzd2_705))))->
										BgL_typez00);
									{	/* Object/generic.scm 48 */
										obj_t BgL_ptypez00_707;

										{	/* Object/generic.scm 49 */
											bool_t BgL_test1512z00_1466;

											{	/* Object/generic.scm 49 */
												obj_t BgL_classz00_999;

												BgL_classz00_999 = BGl_globalz00zzast_varz00;
												if (BGL_OBJECTP(BgL_gz00_689))
													{	/* Object/generic.scm 49 */
														BgL_objectz00_bglt BgL_arg1807z00_1001;

														BgL_arg1807z00_1001 =
															(BgL_objectz00_bglt) (BgL_gz00_689);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Object/generic.scm 49 */
																long BgL_idxz00_1007;

																BgL_idxz00_1007 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_1001);
																BgL_test1512z00_1466 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_1007 + 2L)) ==
																	BgL_classz00_999);
															}
														else
															{	/* Object/generic.scm 49 */
																bool_t BgL_res1475z00_1032;

																{	/* Object/generic.scm 49 */
																	obj_t BgL_oclassz00_1015;

																	{	/* Object/generic.scm 49 */
																		obj_t BgL_arg1815z00_1023;
																		long BgL_arg1816z00_1024;

																		BgL_arg1815z00_1023 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Object/generic.scm 49 */
																			long BgL_arg1817z00_1025;

																			BgL_arg1817z00_1025 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_1001);
																			BgL_arg1816z00_1024 =
																				(BgL_arg1817z00_1025 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_1015 =
																			VECTOR_REF(BgL_arg1815z00_1023,
																			BgL_arg1816z00_1024);
																	}
																	{	/* Object/generic.scm 49 */
																		bool_t BgL__ortest_1115z00_1016;

																		BgL__ortest_1115z00_1016 =
																			(BgL_classz00_999 == BgL_oclassz00_1015);
																		if (BgL__ortest_1115z00_1016)
																			{	/* Object/generic.scm 49 */
																				BgL_res1475z00_1032 =
																					BgL__ortest_1115z00_1016;
																			}
																		else
																			{	/* Object/generic.scm 49 */
																				long BgL_odepthz00_1017;

																				{	/* Object/generic.scm 49 */
																					obj_t BgL_arg1804z00_1018;

																					BgL_arg1804z00_1018 =
																						(BgL_oclassz00_1015);
																					BgL_odepthz00_1017 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_1018);
																				}
																				if ((2L < BgL_odepthz00_1017))
																					{	/* Object/generic.scm 49 */
																						obj_t BgL_arg1802z00_1020;

																						{	/* Object/generic.scm 49 */
																							obj_t BgL_arg1803z00_1021;

																							BgL_arg1803z00_1021 =
																								(BgL_oclassz00_1015);
																							BgL_arg1802z00_1020 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_1021, 2L);
																						}
																						BgL_res1475z00_1032 =
																							(BgL_arg1802z00_1020 ==
																							BgL_classz00_999);
																					}
																				else
																					{	/* Object/generic.scm 49 */
																						BgL_res1475z00_1032 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test1512z00_1466 = BgL_res1475z00_1032;
															}
													}
												else
													{	/* Object/generic.scm 49 */
														BgL_test1512z00_1466 = ((bool_t) 0);
													}
											}
											if (BgL_test1512z00_1466)
												{	/* Object/generic.scm 49 */
													obj_t BgL_pairz00_1035;

													BgL_pairz00_1035 =
														(((BgL_sfunz00_bglt) COBJECT(
																((BgL_sfunz00_bglt)
																	(((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					((BgL_globalz00_bglt)
																						BgL_gz00_689))))->BgL_valuez00))))->
														BgL_argsz00);
													BgL_ptypez00_707 = CAR(BgL_pairz00_1035);
												}
											else
												{	/* Object/generic.scm 49 */
													BgL_ptypez00_707 = BFALSE;
												}
										}
										{	/* Object/generic.scm 49 */

											{	/* Object/generic.scm 51 */
												bool_t BgL_test1517z00_1495;

												{	/* Object/generic.scm 51 */
													bool_t BgL_test1518z00_1496;

													{	/* Object/generic.scm 51 */
														obj_t BgL_classz00_1036;

														BgL_classz00_1036 = BGl_tclassz00zzobject_classz00;
														{	/* Object/generic.scm 51 */
															BgL_objectz00_bglt BgL_arg1807z00_1038;

															{	/* Object/generic.scm 51 */
																obj_t BgL_tmpz00_1497;

																BgL_tmpz00_1497 =
																	((obj_t)
																	((BgL_objectz00_bglt)
																		BgL_methodzd2argzd2typez00_706));
																BgL_arg1807z00_1038 =
																	(BgL_objectz00_bglt) (BgL_tmpz00_1497);
															}
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Object/generic.scm 51 */
																	long BgL_idxz00_1044;

																	BgL_idxz00_1044 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_1038);
																	BgL_test1518z00_1496 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_1044 + 2L)) ==
																		BgL_classz00_1036);
																}
															else
																{	/* Object/generic.scm 51 */
																	bool_t BgL_res1476z00_1069;

																	{	/* Object/generic.scm 51 */
																		obj_t BgL_oclassz00_1052;

																		{	/* Object/generic.scm 51 */
																			obj_t BgL_arg1815z00_1060;
																			long BgL_arg1816z00_1061;

																			BgL_arg1815z00_1060 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Object/generic.scm 51 */
																				long BgL_arg1817z00_1062;

																				BgL_arg1817z00_1062 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_1038);
																				BgL_arg1816z00_1061 =
																					(BgL_arg1817z00_1062 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_1052 =
																				VECTOR_REF(BgL_arg1815z00_1060,
																				BgL_arg1816z00_1061);
																		}
																		{	/* Object/generic.scm 51 */
																			bool_t BgL__ortest_1115z00_1053;

																			BgL__ortest_1115z00_1053 =
																				(BgL_classz00_1036 ==
																				BgL_oclassz00_1052);
																			if (BgL__ortest_1115z00_1053)
																				{	/* Object/generic.scm 51 */
																					BgL_res1476z00_1069 =
																						BgL__ortest_1115z00_1053;
																				}
																			else
																				{	/* Object/generic.scm 51 */
																					long BgL_odepthz00_1054;

																					{	/* Object/generic.scm 51 */
																						obj_t BgL_arg1804z00_1055;

																						BgL_arg1804z00_1055 =
																							(BgL_oclassz00_1052);
																						BgL_odepthz00_1054 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_1055);
																					}
																					if ((2L < BgL_odepthz00_1054))
																						{	/* Object/generic.scm 51 */
																							obj_t BgL_arg1802z00_1057;

																							{	/* Object/generic.scm 51 */
																								obj_t BgL_arg1803z00_1058;

																								BgL_arg1803z00_1058 =
																									(BgL_oclassz00_1052);
																								BgL_arg1802z00_1057 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_1058, 2L);
																							}
																							BgL_res1476z00_1069 =
																								(BgL_arg1802z00_1057 ==
																								BgL_classz00_1036);
																						}
																					else
																						{	/* Object/generic.scm 51 */
																							BgL_res1476z00_1069 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test1518z00_1496 = BgL_res1476z00_1069;
																}
														}
													}
													if (BgL_test1518z00_1496)
														{	/* Object/generic.scm 51 */
															BgL_test1517z00_1495 = ((bool_t) 1);
														}
													else
														{	/* Object/generic.scm 51 */
															obj_t BgL_classz00_1070;

															BgL_classz00_1070 =
																BGl_tclassz00zzobject_classz00;
															if (BGL_OBJECTP(BgL_ptypez00_707))
																{	/* Object/generic.scm 51 */
																	BgL_objectz00_bglt BgL_arg1807z00_1072;

																	BgL_arg1807z00_1072 =
																		(BgL_objectz00_bglt) (BgL_ptypez00_707);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Object/generic.scm 51 */
																			long BgL_idxz00_1078;

																			BgL_idxz00_1078 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_1072);
																			BgL_test1517z00_1495 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_1078 + 2L)) ==
																				BgL_classz00_1070);
																		}
																	else
																		{	/* Object/generic.scm 51 */
																			bool_t BgL_res1477z00_1103;

																			{	/* Object/generic.scm 51 */
																				obj_t BgL_oclassz00_1086;

																				{	/* Object/generic.scm 51 */
																					obj_t BgL_arg1815z00_1094;
																					long BgL_arg1816z00_1095;

																					BgL_arg1815z00_1094 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Object/generic.scm 51 */
																						long BgL_arg1817z00_1096;

																						BgL_arg1817z00_1096 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_1072);
																						BgL_arg1816z00_1095 =
																							(BgL_arg1817z00_1096 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_1086 =
																						VECTOR_REF(BgL_arg1815z00_1094,
																						BgL_arg1816z00_1095);
																				}
																				{	/* Object/generic.scm 51 */
																					bool_t BgL__ortest_1115z00_1087;

																					BgL__ortest_1115z00_1087 =
																						(BgL_classz00_1070 ==
																						BgL_oclassz00_1086);
																					if (BgL__ortest_1115z00_1087)
																						{	/* Object/generic.scm 51 */
																							BgL_res1477z00_1103 =
																								BgL__ortest_1115z00_1087;
																						}
																					else
																						{	/* Object/generic.scm 51 */
																							long BgL_odepthz00_1088;

																							{	/* Object/generic.scm 51 */
																								obj_t BgL_arg1804z00_1089;

																								BgL_arg1804z00_1089 =
																									(BgL_oclassz00_1086);
																								BgL_odepthz00_1088 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_1089);
																							}
																							if ((2L < BgL_odepthz00_1088))
																								{	/* Object/generic.scm 51 */
																									obj_t BgL_arg1802z00_1091;

																									{	/* Object/generic.scm 51 */
																										obj_t BgL_arg1803z00_1092;

																										BgL_arg1803z00_1092 =
																											(BgL_oclassz00_1086);
																										BgL_arg1802z00_1091 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_1092, 2L);
																									}
																									BgL_res1477z00_1103 =
																										(BgL_arg1802z00_1091 ==
																										BgL_classz00_1070);
																								}
																							else
																								{	/* Object/generic.scm 51 */
																									BgL_res1477z00_1103 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test1517z00_1495 =
																				BgL_res1477z00_1103;
																		}
																}
															else
																{	/* Object/generic.scm 51 */
																	BgL_test1517z00_1495 = ((bool_t) 0);
																}
														}
												}
												if (BgL_test1517z00_1495)
													{	/* Object/generic.scm 51 */
														BGL_TAIL return
															BGl_makezd2objectzd2genericzd2bodyzd2zzobject_genericz00
															(BgL_idz00_3, BgL_localsz00_4, BgL_argsz00_5,
															BgL_srcz00_6);
													}
												else
													{	/* Object/generic.scm 53 */
														bool_t BgL_test1526z00_1543;

														{	/* Object/generic.scm 53 */
															bool_t BgL_test1527z00_1544;

															{	/* Object/generic.scm 53 */
																obj_t BgL_classz00_1104;

																BgL_classz00_1104 = BGl_typez00zztype_typez00;
																{	/* Object/generic.scm 53 */
																	BgL_objectz00_bglt BgL_arg1807z00_1106;

																	{	/* Object/generic.scm 53 */
																		obj_t BgL_tmpz00_1545;

																		BgL_tmpz00_1545 =
																			((obj_t)
																			((BgL_objectz00_bglt)
																				BgL_methodzd2argzd2typez00_706));
																		BgL_arg1807z00_1106 =
																			(BgL_objectz00_bglt) (BgL_tmpz00_1545);
																	}
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Object/generic.scm 53 */
																			long BgL_idxz00_1112;

																			BgL_idxz00_1112 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_1106);
																			BgL_test1527z00_1544 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_1112 + 1L)) ==
																				BgL_classz00_1104);
																		}
																	else
																		{	/* Object/generic.scm 53 */
																			bool_t BgL_res1478z00_1137;

																			{	/* Object/generic.scm 53 */
																				obj_t BgL_oclassz00_1120;

																				{	/* Object/generic.scm 53 */
																					obj_t BgL_arg1815z00_1128;
																					long BgL_arg1816z00_1129;

																					BgL_arg1815z00_1128 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Object/generic.scm 53 */
																						long BgL_arg1817z00_1130;

																						BgL_arg1817z00_1130 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_1106);
																						BgL_arg1816z00_1129 =
																							(BgL_arg1817z00_1130 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_1120 =
																						VECTOR_REF(BgL_arg1815z00_1128,
																						BgL_arg1816z00_1129);
																				}
																				{	/* Object/generic.scm 53 */
																					bool_t BgL__ortest_1115z00_1121;

																					BgL__ortest_1115z00_1121 =
																						(BgL_classz00_1104 ==
																						BgL_oclassz00_1120);
																					if (BgL__ortest_1115z00_1121)
																						{	/* Object/generic.scm 53 */
																							BgL_res1478z00_1137 =
																								BgL__ortest_1115z00_1121;
																						}
																					else
																						{	/* Object/generic.scm 53 */
																							long BgL_odepthz00_1122;

																							{	/* Object/generic.scm 53 */
																								obj_t BgL_arg1804z00_1123;

																								BgL_arg1804z00_1123 =
																									(BgL_oclassz00_1120);
																								BgL_odepthz00_1122 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_1123);
																							}
																							if ((1L < BgL_odepthz00_1122))
																								{	/* Object/generic.scm 53 */
																									obj_t BgL_arg1802z00_1125;

																									{	/* Object/generic.scm 53 */
																										obj_t BgL_arg1803z00_1126;

																										BgL_arg1803z00_1126 =
																											(BgL_oclassz00_1120);
																										BgL_arg1802z00_1125 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_1126, 1L);
																									}
																									BgL_res1478z00_1137 =
																										(BgL_arg1802z00_1125 ==
																										BgL_classz00_1104);
																								}
																							else
																								{	/* Object/generic.scm 53 */
																									BgL_res1478z00_1137 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test1527z00_1544 =
																				BgL_res1478z00_1137;
																		}
																}
															}
															if (BgL_test1527z00_1544)
																{	/* Object/generic.scm 53 */
																	if (
																		(((obj_t) BgL_methodzd2argzd2typez00_706) ==
																			BGl_za2objza2z00zztype_cachez00))
																		{	/* Object/generic.scm 54 */
																			BgL_test1526z00_1543 = ((bool_t) 0);
																		}
																	else
																		{	/* Object/generic.scm 55 */
																			bool_t BgL_test1532z00_1571;

																			{	/* Object/generic.scm 55 */
																				BgL_typez00_bglt BgL_arg1166z00_725;

																				BgL_arg1166z00_725 =
																					BGl_getzd2defaultzd2typez00zztype_cachez00
																					();
																				BgL_test1532z00_1571 =
																					(((obj_t)
																						BgL_methodzd2argzd2typez00_706) ==
																					((obj_t) BgL_arg1166z00_725));
																			}
																			if (BgL_test1532z00_1571)
																				{	/* Object/generic.scm 55 */
																					BgL_test1526z00_1543 = ((bool_t) 0);
																				}
																			else
																				{	/* Object/generic.scm 55 */
																					BgL_test1526z00_1543 = ((bool_t) 1);
																				}
																		}
																}
															else
																{	/* Object/generic.scm 53 */
																	BgL_test1526z00_1543 = ((bool_t) 0);
																}
														}
														if (BgL_test1526z00_1543)
															{	/* Object/generic.scm 36 */
																obj_t BgL_list1191z00_1138;

																BgL_list1191z00_1138 =
																	MAKE_YOUNG_PAIR(BNIL, BNIL);
																return
																	BGl_userzd2errorzd2zztools_errorz00
																	(BgL_idz00_3,
																	BGl_string1486z00zzobject_genericz00,
																	BgL_srcz00_6, BgL_list1191z00_1138);
															}
														else
															{	/* Object/generic.scm 57 */
																bool_t BgL_test1533z00_1578;

																{	/* Object/generic.scm 57 */
																	bool_t BgL_test1535z00_1579;

																	{	/* Object/generic.scm 57 */
																		obj_t BgL_classz00_1139;

																		BgL_classz00_1139 =
																			BGl_typez00zztype_typez00;
																		if (BGL_OBJECTP(BgL_ptypez00_707))
																			{	/* Object/generic.scm 57 */
																				BgL_objectz00_bglt BgL_arg1807z00_1141;

																				BgL_arg1807z00_1141 =
																					(BgL_objectz00_bglt)
																					(BgL_ptypez00_707);
																				if (BGL_CONDEXPAND_ISA_ARCH64())
																					{	/* Object/generic.scm 57 */
																						long BgL_idxz00_1147;

																						BgL_idxz00_1147 =
																							BGL_OBJECT_INHERITANCE_NUM
																							(BgL_arg1807z00_1141);
																						BgL_test1535z00_1579 =
																							(VECTOR_REF
																							(BGl_za2inheritancesza2z00zz__objectz00,
																								(BgL_idxz00_1147 + 1L)) ==
																							BgL_classz00_1139);
																					}
																				else
																					{	/* Object/generic.scm 57 */
																						bool_t BgL_res1479z00_1172;

																						{	/* Object/generic.scm 57 */
																							obj_t BgL_oclassz00_1155;

																							{	/* Object/generic.scm 57 */
																								obj_t BgL_arg1815z00_1163;
																								long BgL_arg1816z00_1164;

																								BgL_arg1815z00_1163 =
																									(BGl_za2classesza2z00zz__objectz00);
																								{	/* Object/generic.scm 57 */
																									long BgL_arg1817z00_1165;

																									BgL_arg1817z00_1165 =
																										BGL_OBJECT_CLASS_NUM
																										(BgL_arg1807z00_1141);
																									BgL_arg1816z00_1164 =
																										(BgL_arg1817z00_1165 -
																										OBJECT_TYPE);
																								}
																								BgL_oclassz00_1155 =
																									VECTOR_REF
																									(BgL_arg1815z00_1163,
																									BgL_arg1816z00_1164);
																							}
																							{	/* Object/generic.scm 57 */
																								bool_t BgL__ortest_1115z00_1156;

																								BgL__ortest_1115z00_1156 =
																									(BgL_classz00_1139 ==
																									BgL_oclassz00_1155);
																								if (BgL__ortest_1115z00_1156)
																									{	/* Object/generic.scm 57 */
																										BgL_res1479z00_1172 =
																											BgL__ortest_1115z00_1156;
																									}
																								else
																									{	/* Object/generic.scm 57 */
																										long BgL_odepthz00_1157;

																										{	/* Object/generic.scm 57 */
																											obj_t BgL_arg1804z00_1158;

																											BgL_arg1804z00_1158 =
																												(BgL_oclassz00_1155);
																											BgL_odepthz00_1157 =
																												BGL_CLASS_DEPTH
																												(BgL_arg1804z00_1158);
																										}
																										if (
																											(1L < BgL_odepthz00_1157))
																											{	/* Object/generic.scm 57 */
																												obj_t
																													BgL_arg1802z00_1160;
																												{	/* Object/generic.scm 57 */
																													obj_t
																														BgL_arg1803z00_1161;
																													BgL_arg1803z00_1161 =
																														(BgL_oclassz00_1155);
																													BgL_arg1802z00_1160 =
																														BGL_CLASS_ANCESTORS_REF
																														(BgL_arg1803z00_1161,
																														1L);
																												}
																												BgL_res1479z00_1172 =
																													(BgL_arg1802z00_1160
																													== BgL_classz00_1139);
																											}
																										else
																											{	/* Object/generic.scm 57 */
																												BgL_res1479z00_1172 =
																													((bool_t) 0);
																											}
																									}
																							}
																						}
																						BgL_test1535z00_1579 =
																							BgL_res1479z00_1172;
																					}
																			}
																		else
																			{	/* Object/generic.scm 57 */
																				BgL_test1535z00_1579 = ((bool_t) 0);
																			}
																	}
																	if (BgL_test1535z00_1579)
																		{	/* Object/generic.scm 57 */
																			if (
																				(BgL_ptypez00_707 ==
																					BGl_za2objza2z00zztype_cachez00))
																				{	/* Object/generic.scm 58 */
																					BgL_test1533z00_1578 = ((bool_t) 0);
																				}
																			else
																				{	/* Object/generic.scm 59 */
																					bool_t BgL_test1542z00_1604;

																					{	/* Object/generic.scm 59 */
																						BgL_typez00_bglt BgL_arg1164z00_722;

																						BgL_arg1164z00_722 =
																							BGl_getzd2defaultzd2typez00zztype_cachez00
																							();
																						BgL_test1542z00_1604 =
																							(BgL_ptypez00_707 ==
																							((obj_t) BgL_arg1164z00_722));
																					}
																					if (BgL_test1542z00_1604)
																						{	/* Object/generic.scm 59 */
																							BgL_test1533z00_1578 =
																								((bool_t) 0);
																						}
																					else
																						{	/* Object/generic.scm 59 */
																							BgL_test1533z00_1578 =
																								((bool_t) 1);
																						}
																				}
																		}
																	else
																		{	/* Object/generic.scm 57 */
																			BgL_test1533z00_1578 = ((bool_t) 0);
																		}
																}
																if (BgL_test1533z00_1578)
																	{	/* Object/generic.scm 36 */
																		obj_t BgL_list1191z00_1173;

																		BgL_list1191z00_1173 =
																			MAKE_YOUNG_PAIR(BNIL, BNIL);
																		return
																			BGl_userzd2errorzd2zztools_errorz00
																			(BgL_idz00_3,
																			BGl_string1486z00zzobject_genericz00,
																			BgL_srcz00_6, BgL_list1191z00_1173);
																	}
																else
																	{	/* Object/generic.scm 57 */
																		BGL_TAIL return
																			BGl_makezd2nonzd2objectzd2genericzd2bodyz00zzobject_genericz00
																			(BgL_idz00_3, BgL_localsz00_4,
																			BgL_argsz00_5, BgL_srcz00_6);
																	}
															}
													}
											}
										}
									}
								}
							}
					}
			}
		}

	}



/* &make-generic-body */
	obj_t BGl_z62makezd2genericzd2bodyz62zzobject_genericz00(obj_t
		BgL_envz00_1335, obj_t BgL_idz00_1336, obj_t BgL_localsz00_1337,
		obj_t BgL_argsz00_1338, obj_t BgL_srcz00_1339)
	{
		{	/* Object/generic.scm 34 */
			return
				BGl_makezd2genericzd2bodyz00zzobject_genericz00(BgL_idz00_1336,
				BgL_localsz00_1337, BgL_argsz00_1338, BgL_srcz00_1339);
		}

	}



/* make-object-generic-body */
	obj_t BGl_makezd2objectzd2genericzd2bodyzd2zzobject_genericz00(obj_t
		BgL_idz00_7, obj_t BgL_localsz00_8, obj_t BgL_argsz00_9,
		obj_t BgL_srcz00_10)
	{
		{	/* Object/generic.scm 67 */
			{	/* Object/generic.scm 68 */
				obj_t BgL_pidz00_744;

				BgL_pidz00_744 =
					BGl_parsezd2idzd2zzast_identz00(BgL_idz00_7,
					BGl_findzd2locationzd2zztools_locationz00(BgL_srcz00_10));
				{	/* Object/generic.scm 68 */
					obj_t BgL_idz00_745;

					BgL_idz00_745 = CAR(BgL_pidz00_744);
					{	/* Object/generic.scm 69 */
						long BgL_arityz00_746;

						BgL_arityz00_746 =
							BGl_globalzd2arityzd2zztools_argsz00(BgL_argsz00_9);
						{	/* Object/generic.scm 70 */
							obj_t BgL_argszd2idzd2_747;

							if (NULLP(BgL_localsz00_8))
								{	/* Object/generic.scm 71 */
									BgL_argszd2idzd2_747 = BNIL;
								}
							else
								{	/* Object/generic.scm 71 */
									obj_t BgL_head1112z00_775;

									{	/* Object/generic.scm 71 */
										obj_t BgL_arg1227z00_787;

										BgL_arg1227z00_787 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt)
															CAR(((obj_t) BgL_localsz00_8))))))->BgL_idz00);
										BgL_head1112z00_775 =
											MAKE_YOUNG_PAIR(BgL_arg1227z00_787, BNIL);
									}
									{	/* Object/generic.scm 71 */
										obj_t BgL_g1115z00_776;

										BgL_g1115z00_776 = CDR(((obj_t) BgL_localsz00_8));
										{
											obj_t BgL_l1110z00_778;
											obj_t BgL_tail1113z00_779;

											BgL_l1110z00_778 = BgL_g1115z00_776;
											BgL_tail1113z00_779 = BgL_head1112z00_775;
										BgL_zc3z04anonymousza31221ze3z87_780:
											if (NULLP(BgL_l1110z00_778))
												{	/* Object/generic.scm 71 */
													BgL_argszd2idzd2_747 = BgL_head1112z00_775;
												}
											else
												{	/* Object/generic.scm 71 */
													obj_t BgL_newtail1114z00_782;

													{	/* Object/generic.scm 71 */
														obj_t BgL_arg1225z00_784;

														BgL_arg1225z00_784 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt)
																			CAR(
																				((obj_t) BgL_l1110z00_778))))))->
															BgL_idz00);
														BgL_newtail1114z00_782 =
															MAKE_YOUNG_PAIR(BgL_arg1225z00_784, BNIL);
													}
													SET_CDR(BgL_tail1113z00_779, BgL_newtail1114z00_782);
													{	/* Object/generic.scm 71 */
														obj_t BgL_arg1223z00_783;

														BgL_arg1223z00_783 =
															CDR(((obj_t) BgL_l1110z00_778));
														{
															obj_t BgL_tail1113z00_1638;
															obj_t BgL_l1110z00_1637;

															BgL_l1110z00_1637 = BgL_arg1223z00_783;
															BgL_tail1113z00_1638 = BgL_newtail1114z00_782;
															BgL_tail1113z00_779 = BgL_tail1113z00_1638;
															BgL_l1110z00_778 = BgL_l1110z00_1637;
															goto BgL_zc3z04anonymousza31221ze3z87_780;
														}
													}
												}
										}
									}
								}
							{	/* Object/generic.scm 71 */
								obj_t BgL_methodzd2argzd2_748;

								BgL_methodzd2argzd2_748 = CAR(((obj_t) BgL_localsz00_8));
								{	/* Object/generic.scm 72 */
									obj_t BgL_methodzd2argzd2idz00_749;

									BgL_methodzd2argzd2idz00_749 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_methodzd2argzd2_748))))->
										BgL_idz00);
									{	/* Object/generic.scm 74 */
										obj_t BgL_methodz00_751;

										BgL_methodz00_751 =
											BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(1));
										{	/* Object/generic.scm 75 */
											obj_t BgL_tmethodz00_752;

											BgL_tmethodz00_752 =
												BGl_makezd2typedzd2identz00zzast_identz00
												(BgL_methodz00_751, CNST_TABLE_REF(2));
											{	/* Object/generic.scm 76 */

												{	/* Object/generic.scm 78 */
													obj_t BgL_arg1193z00_753;

													{	/* Object/generic.scm 78 */
														obj_t BgL_arg1194z00_754;
														obj_t BgL_arg1196z00_755;

														{	/* Object/generic.scm 78 */
															obj_t BgL_arg1197z00_756;

															{	/* Object/generic.scm 78 */
																obj_t BgL_arg1198z00_757;

																{	/* Object/generic.scm 78 */
																	obj_t BgL_arg1199z00_758;

																	{	/* Object/generic.scm 78 */
																		obj_t BgL_arg1200z00_759;

																		{	/* Object/generic.scm 78 */
																			obj_t BgL_arg1202z00_761;

																			{	/* Object/generic.scm 78 */
																				obj_t BgL_arg1203z00_762;

																				{	/* Object/generic.scm 78 */
																					obj_t BgL_arg1206z00_763;

																					{	/* Object/generic.scm 78 */
																						obj_t BgL_arg1208z00_764;

																						{	/* Object/generic.scm 78 */
																							obj_t BgL_arg1209z00_765;

																							BgL_arg1209z00_765 =
																								MAKE_YOUNG_PAIR
																								(BGl_za2moduleza2z00zzmodule_modulez00,
																								BNIL);
																							BgL_arg1208z00_764 =
																								MAKE_YOUNG_PAIR(BgL_idz00_745,
																								BgL_arg1209z00_765);
																						}
																						BgL_arg1206z00_763 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
																							BgL_arg1208z00_764);
																					}
																					BgL_arg1203z00_762 =
																						MAKE_YOUNG_PAIR(BgL_arg1206z00_763,
																						BNIL);
																				}
																				BgL_arg1202z00_761 =
																					MAKE_YOUNG_PAIR
																					(BgL_methodzd2argzd2idz00_749,
																					BgL_arg1203z00_762);
																			}
																			BgL_arg1200z00_759 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(4),
																				BgL_arg1202z00_761);
																		}
																		{	/* Object/generic.scm 77 */
																			obj_t BgL_list1201z00_760;

																			BgL_list1201z00_760 =
																				MAKE_YOUNG_PAIR(BgL_arg1200z00_759,
																				BNIL);
																			BgL_arg1199z00_758 =
																				BGl_makezd2privatezd2sexpz00zzast_privatez00
																				(CNST_TABLE_REF(5), CNST_TABLE_REF(6),
																				BgL_list1201z00_760);
																		}
																	}
																	BgL_arg1198z00_757 =
																		MAKE_YOUNG_PAIR(BgL_arg1199z00_758, BNIL);
																}
																BgL_arg1197z00_756 =
																	MAKE_YOUNG_PAIR(BgL_tmethodz00_752,
																	BgL_arg1198z00_757);
															}
															BgL_arg1194z00_754 =
																MAKE_YOUNG_PAIR(BgL_arg1197z00_756, BNIL);
														}
														{	/* Object/generic.scm 79 */
															obj_t BgL_arg1210z00_766;

															if ((BgL_arityz00_746 >= 0L))
																{	/* Object/generic.scm 79 */
																	BgL_arg1210z00_766 =
																		MAKE_YOUNG_PAIR(BgL_methodz00_751,
																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																		(BgL_argszd2idzd2_747, BNIL));
																}
															else
																{	/* Object/generic.scm 81 */
																	obj_t BgL_arg1215z00_769;

																	{	/* Object/generic.scm 81 */
																		obj_t BgL_arg1216z00_770;

																		{	/* Object/generic.scm 81 */
																			obj_t BgL_arg1218z00_771;

																			{	/* Object/generic.scm 81 */
																				obj_t BgL_arg1219z00_772;

																				BgL_arg1219z00_772 =
																					BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																					(BgL_argszd2idzd2_747, BNIL);
																				BgL_arg1218z00_771 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(7),
																					BgL_arg1219z00_772);
																			}
																			BgL_arg1216z00_770 =
																				MAKE_YOUNG_PAIR(BgL_arg1218z00_771,
																				BNIL);
																		}
																		BgL_arg1215z00_769 =
																			MAKE_YOUNG_PAIR(BgL_methodz00_751,
																			BgL_arg1216z00_770);
																	}
																	BgL_arg1210z00_766 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(8),
																		BgL_arg1215z00_769);
																}
															BgL_arg1196z00_755 =
																MAKE_YOUNG_PAIR(BgL_arg1210z00_766, BNIL);
														}
														BgL_arg1193z00_753 =
															MAKE_YOUNG_PAIR(BgL_arg1194z00_754,
															BgL_arg1196z00_755);
													}
													return
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(9),
														BgL_arg1193z00_753);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* make-non-object-generic-body */
	obj_t BGl_makezd2nonzd2objectzd2genericzd2bodyz00zzobject_genericz00(obj_t
		BgL_idz00_11, obj_t BgL_localsz00_12, obj_t BgL_argsz00_13,
		obj_t BgL_srcz00_14)
	{
		{	/* Object/generic.scm 86 */
			{	/* Object/generic.scm 87 */
				obj_t BgL_pidz00_790;

				BgL_pidz00_790 =
					BGl_parsezd2idzd2zzast_identz00(BgL_idz00_11,
					BGl_findzd2locationzd2zztools_locationz00(BgL_srcz00_14));
				{	/* Object/generic.scm 87 */
					obj_t BgL_idz00_791;

					BgL_idz00_791 = CAR(BgL_pidz00_790);
					{	/* Object/generic.scm 88 */
						long BgL_arityz00_792;

						BgL_arityz00_792 =
							BGl_globalzd2arityzd2zztools_argsz00(BgL_argsz00_13);
						{	/* Object/generic.scm 89 */
							obj_t BgL_argszd2idzd2_793;

							if (NULLP(BgL_localsz00_12))
								{	/* Object/generic.scm 90 */
									BgL_argszd2idzd2_793 = BNIL;
								}
							else
								{	/* Object/generic.scm 90 */
									obj_t BgL_head1118z00_847;

									{	/* Object/generic.scm 90 */
										obj_t BgL_arg1351z00_859;

										BgL_arg1351z00_859 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt)
															CAR(((obj_t) BgL_localsz00_12))))))->BgL_idz00);
										BgL_head1118z00_847 =
											MAKE_YOUNG_PAIR(BgL_arg1351z00_859, BNIL);
									}
									{	/* Object/generic.scm 90 */
										obj_t BgL_g1121z00_848;

										BgL_g1121z00_848 = CDR(((obj_t) BgL_localsz00_12));
										{
											obj_t BgL_l1116z00_850;
											obj_t BgL_tail1119z00_851;

											BgL_l1116z00_850 = BgL_g1121z00_848;
											BgL_tail1119z00_851 = BgL_head1118z00_847;
										BgL_zc3z04anonymousza31342ze3z87_852:
											if (NULLP(BgL_l1116z00_850))
												{	/* Object/generic.scm 90 */
													BgL_argszd2idzd2_793 = BgL_head1118z00_847;
												}
											else
												{	/* Object/generic.scm 90 */
													obj_t BgL_newtail1120z00_854;

													{	/* Object/generic.scm 90 */
														obj_t BgL_arg1348z00_856;

														BgL_arg1348z00_856 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt)
																			CAR(
																				((obj_t) BgL_l1116z00_850))))))->
															BgL_idz00);
														BgL_newtail1120z00_854 =
															MAKE_YOUNG_PAIR(BgL_arg1348z00_856, BNIL);
													}
													SET_CDR(BgL_tail1119z00_851, BgL_newtail1120z00_854);
													{	/* Object/generic.scm 90 */
														obj_t BgL_arg1346z00_855;

														BgL_arg1346z00_855 =
															CDR(((obj_t) BgL_l1116z00_850));
														{
															obj_t BgL_tail1119z00_1704;
															obj_t BgL_l1116z00_1703;

															BgL_l1116z00_1703 = BgL_arg1346z00_855;
															BgL_tail1119z00_1704 = BgL_newtail1120z00_854;
															BgL_tail1119z00_851 = BgL_tail1119z00_1704;
															BgL_l1116z00_850 = BgL_l1116z00_1703;
															goto BgL_zc3z04anonymousza31342ze3z87_852;
														}
													}
												}
										}
									}
								}
							{	/* Object/generic.scm 90 */
								obj_t BgL_defaultzd2bodyzd2_794;

								if ((BgL_arityz00_792 >= 0L))
									{	/* Object/generic.scm 93 */
										obj_t BgL_arg1316z00_827;
										obj_t BgL_arg1317z00_828;

										{	/* Object/generic.scm 93 */
											obj_t BgL_arg1318z00_829;

											{	/* Object/generic.scm 93 */
												obj_t BgL_arg1319z00_830;

												{	/* Object/generic.scm 93 */
													obj_t BgL_arg1320z00_831;

													{	/* Object/generic.scm 93 */
														obj_t BgL_arg1321z00_832;

														BgL_arg1321z00_832 =
															MAKE_YOUNG_PAIR
															(BGl_za2moduleza2z00zzmodule_modulez00, BNIL);
														BgL_arg1320z00_831 =
															MAKE_YOUNG_PAIR(BgL_idz00_791,
															BgL_arg1321z00_832);
													}
													BgL_arg1319z00_830 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
														BgL_arg1320z00_831);
												}
												BgL_arg1318z00_829 =
													MAKE_YOUNG_PAIR(BgL_arg1319z00_830, BNIL);
											}
											BgL_arg1316z00_827 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(10), BgL_arg1318z00_829);
										}
										BgL_arg1317z00_828 =
											BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
											(BgL_argszd2idzd2_793, BNIL);
										BgL_defaultzd2bodyzd2_794 =
											MAKE_YOUNG_PAIR(BgL_arg1316z00_827, BgL_arg1317z00_828);
									}
								else
									{	/* Object/generic.scm 96 */
										obj_t BgL_arg1322z00_833;

										{	/* Object/generic.scm 96 */
											obj_t BgL_arg1323z00_834;
											obj_t BgL_arg1325z00_835;

											{	/* Object/generic.scm 96 */
												obj_t BgL_arg1326z00_836;

												{	/* Object/generic.scm 96 */
													obj_t BgL_arg1327z00_837;

													{	/* Object/generic.scm 96 */
														obj_t BgL_arg1328z00_838;

														{	/* Object/generic.scm 96 */
															obj_t BgL_arg1329z00_839;

															BgL_arg1329z00_839 =
																MAKE_YOUNG_PAIR
																(BGl_za2moduleza2z00zzmodule_modulez00, BNIL);
															BgL_arg1328z00_838 =
																MAKE_YOUNG_PAIR(BgL_idz00_791,
																BgL_arg1329z00_839);
														}
														BgL_arg1327z00_837 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
															BgL_arg1328z00_838);
													}
													BgL_arg1326z00_836 =
														MAKE_YOUNG_PAIR(BgL_arg1327z00_837, BNIL);
												}
												BgL_arg1323z00_834 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
													BgL_arg1326z00_836);
											}
											{	/* Object/generic.scm 97 */
												obj_t BgL_arg1331z00_840;

												{	/* Object/generic.scm 97 */
													obj_t BgL_arg1332z00_841;
													obj_t BgL_arg1333z00_842;

													{	/* Object/generic.scm 97 */
														obj_t BgL_arg1335z00_843;

														{	/* Object/generic.scm 97 */
															obj_t BgL_arg1339z00_844;

															BgL_arg1339z00_844 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(11), BNIL);
															BgL_arg1335z00_843 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(7),
																BgL_arg1339z00_844);
														}
														BgL_arg1332z00_841 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
															BgL_arg1335z00_843);
													}
													BgL_arg1333z00_842 =
														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
														(BgL_argszd2idzd2_793, BNIL);
													BgL_arg1331z00_840 =
														MAKE_YOUNG_PAIR(BgL_arg1332z00_841,
														BgL_arg1333z00_842);
												}
												BgL_arg1325z00_835 =
													MAKE_YOUNG_PAIR(BgL_arg1331z00_840, BNIL);
											}
											BgL_arg1322z00_833 =
												MAKE_YOUNG_PAIR(BgL_arg1323z00_834, BgL_arg1325z00_835);
										}
										BgL_defaultzd2bodyzd2_794 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(8), BgL_arg1322z00_833);
									}
								{	/* Object/generic.scm 91 */
									obj_t BgL_methodzd2argzd2_795;

									BgL_methodzd2argzd2_795 = CAR(((obj_t) BgL_localsz00_12));
									{	/* Object/generic.scm 100 */
										obj_t BgL_methodzd2argzd2idz00_796;

										BgL_methodzd2argzd2idz00_796 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt) BgL_methodzd2argzd2_795))))->
											BgL_idz00);
										{	/* Object/generic.scm 102 */
											obj_t BgL_methodz00_798;

											BgL_methodz00_798 =
												BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(1));
											{	/* Object/generic.scm 103 */
												obj_t BgL_tmethodz00_799;

												BgL_tmethodz00_799 =
													BGl_makezd2typedzd2identz00zzast_identz00
													(BgL_methodz00_798, CNST_TABLE_REF(2));
												{	/* Object/generic.scm 104 */
													obj_t BgL_appzd2lyzd2methodz00_800;

													{	/* Object/generic.scm 109 */
														obj_t BgL_arg1236z00_806;

														{	/* Object/generic.scm 109 */
															obj_t BgL_arg1238z00_807;
															obj_t BgL_arg1239z00_808;

															{	/* Object/generic.scm 109 */
																obj_t BgL_arg1242z00_809;

																{	/* Object/generic.scm 109 */
																	obj_t BgL_arg1244z00_810;

																	{	/* Object/generic.scm 109 */
																		obj_t BgL_arg1248z00_811;

																		{	/* Object/generic.scm 109 */
																			obj_t BgL_arg1249z00_812;

																			{	/* Object/generic.scm 109 */
																				obj_t BgL_arg1252z00_814;

																				{	/* Object/generic.scm 109 */
																					obj_t BgL_arg1268z00_815;

																					{	/* Object/generic.scm 109 */
																						obj_t BgL_arg1272z00_816;

																						{	/* Object/generic.scm 109 */
																							obj_t BgL_arg1284z00_817;

																							{	/* Object/generic.scm 109 */
																								obj_t BgL_arg1304z00_818;

																								BgL_arg1304z00_818 =
																									MAKE_YOUNG_PAIR
																									(BGl_za2moduleza2z00zzmodule_modulez00,
																									BNIL);
																								BgL_arg1284z00_817 =
																									MAKE_YOUNG_PAIR(BgL_idz00_791,
																									BgL_arg1304z00_818);
																							}
																							BgL_arg1272z00_816 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(3), BgL_arg1284z00_817);
																						}
																						BgL_arg1268z00_815 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1272z00_816, BNIL);
																					}
																					BgL_arg1252z00_814 =
																						MAKE_YOUNG_PAIR
																						(BgL_methodzd2argzd2idz00_796,
																						BgL_arg1268z00_815);
																				}
																				BgL_arg1249z00_812 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(4),
																					BgL_arg1252z00_814);
																			}
																			{	/* Object/generic.scm 106 */
																				obj_t BgL_list1250z00_813;

																				BgL_list1250z00_813 =
																					MAKE_YOUNG_PAIR(BgL_arg1249z00_812,
																					BNIL);
																				BgL_arg1248z00_811 =
																					BGl_makezd2privatezd2sexpz00zzast_privatez00
																					(CNST_TABLE_REF(5), CNST_TABLE_REF(6),
																					BgL_list1250z00_813);
																			}
																		}
																		BgL_arg1244z00_810 =
																			MAKE_YOUNG_PAIR(BgL_arg1248z00_811, BNIL);
																	}
																	BgL_arg1242z00_809 =
																		MAKE_YOUNG_PAIR(BgL_tmethodz00_799,
																		BgL_arg1244z00_810);
																}
																BgL_arg1238z00_807 =
																	MAKE_YOUNG_PAIR(BgL_arg1242z00_809, BNIL);
															}
															{	/* Object/generic.scm 110 */
																obj_t BgL_arg1305z00_819;

																if ((BgL_arityz00_792 >= 0L))
																	{	/* Object/generic.scm 110 */
																		BgL_arg1305z00_819 =
																			MAKE_YOUNG_PAIR(BgL_methodz00_798,
																			BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																			(BgL_argszd2idzd2_793, BNIL));
																	}
																else
																	{	/* Object/generic.scm 112 */
																		obj_t BgL_arg1310z00_822;

																		{	/* Object/generic.scm 112 */
																			obj_t BgL_arg1311z00_823;

																			{	/* Object/generic.scm 112 */
																				obj_t BgL_arg1312z00_824;

																				{	/* Object/generic.scm 112 */
																					obj_t BgL_arg1314z00_825;

																					BgL_arg1314z00_825 =
																						BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																						(BgL_argszd2idzd2_793, BNIL);
																					BgL_arg1312z00_824 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(7),
																						BgL_arg1314z00_825);
																				}
																				BgL_arg1311z00_823 =
																					MAKE_YOUNG_PAIR(BgL_arg1312z00_824,
																					BNIL);
																			}
																			BgL_arg1310z00_822 =
																				MAKE_YOUNG_PAIR(BgL_methodz00_798,
																				BgL_arg1311z00_823);
																		}
																		BgL_arg1305z00_819 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(8),
																			BgL_arg1310z00_822);
																	}
																BgL_arg1239z00_808 =
																	MAKE_YOUNG_PAIR(BgL_arg1305z00_819, BNIL);
															}
															BgL_arg1236z00_806 =
																MAKE_YOUNG_PAIR(BgL_arg1238z00_807,
																BgL_arg1239z00_808);
														}
														BgL_appzd2lyzd2methodz00_800 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(9),
															BgL_arg1236z00_806);
													}
													{	/* Object/generic.scm 105 */

														{	/* Object/generic.scm 114 */
															obj_t BgL_arg1230z00_801;

															{	/* Object/generic.scm 114 */
																obj_t BgL_arg1231z00_802;
																obj_t BgL_arg1232z00_803;

																{	/* Object/generic.scm 114 */
																	obj_t BgL_arg1233z00_804;

																	BgL_arg1233z00_804 =
																		MAKE_YOUNG_PAIR
																		(BgL_methodzd2argzd2idz00_796, BNIL);
																	BgL_arg1231z00_802 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(12),
																		BgL_arg1233z00_804);
																}
																{	/* Object/generic.scm 114 */
																	obj_t BgL_arg1234z00_805;

																	BgL_arg1234z00_805 =
																		MAKE_YOUNG_PAIR(BgL_defaultzd2bodyzd2_794,
																		BNIL);
																	BgL_arg1232z00_803 =
																		MAKE_YOUNG_PAIR
																		(BgL_appzd2lyzd2methodz00_800,
																		BgL_arg1234z00_805);
																}
																BgL_arg1230z00_801 =
																	MAKE_YOUNG_PAIR(BgL_arg1231z00_802,
																	BgL_arg1232z00_803);
															}
															return
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(13),
																BgL_arg1230z00_801);
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzobject_genericz00(void)
	{
		{	/* Object/generic.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzobject_genericz00(void)
	{
		{	/* Object/generic.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzobject_genericz00(void)
	{
		{	/* Object/generic.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzobject_genericz00(void)
	{
		{	/* Object/generic.scm 15 */
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1487z00zzobject_genericz00));
			BGl_modulezd2initializa7ationz75zztools_argsz00(47102372L,
				BSTRING_TO_STRING(BGl_string1487z00zzobject_genericz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1487z00zzobject_genericz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1487z00zzobject_genericz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1487z00zzobject_genericz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1487z00zzobject_genericz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1487z00zzobject_genericz00));
			BGl_modulezd2initializa7ationz75zzast_privatez00(135263837L,
				BSTRING_TO_STRING(BGl_string1487z00zzobject_genericz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1487z00zzobject_genericz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1487z00zzobject_genericz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1487z00zzobject_genericz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1487z00zzobject_genericz00));
			return
				BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1487z00zzobject_genericz00));
		}

	}

#ifdef __cplusplus
}
#endif
