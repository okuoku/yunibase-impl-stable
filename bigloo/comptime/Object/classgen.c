/*===========================================================================*/
/*   (Object/classgen.scm)                                                   */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Object/classgen.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_OBJECT_CLASSGEN_TYPE_DEFINITIONS
#define BGL_OBJECT_CLASSGEN_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_tclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		struct BgL_globalz00_bgl *BgL_holderz00;
		obj_t BgL_wideningz00;
		long BgL_depthz00;
		bool_t BgL_finalzf3zf3;
		obj_t BgL_constructorz00;
		obj_t BgL_virtualzd2slotszd2numberz00;
		bool_t BgL_abstractzf3zf3;
		obj_t BgL_widezd2typezd2;
		obj_t BgL_subclassesz00;
	}                *BgL_tclassz00_bglt;

	typedef struct BgL_slotz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_srcz00;
		obj_t BgL_classzd2ownerzd2;
		long BgL_indexz00;
		obj_t BgL_typez00;
		bool_t BgL_readzd2onlyzf3z21;
		obj_t BgL_defaultzd2valuezd2;
		obj_t BgL_virtualzd2numzd2;
		bool_t BgL_virtualzd2overridezd2;
		obj_t BgL_getterz00;
		obj_t BgL_setterz00;
		obj_t BgL_userzd2infozd2;
	}              *BgL_slotz00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;


#endif													// BGL_OBJECT_CLASSGEN_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t
		BGl_classgenzd2allocatezd2exprz00zzobject_classgenz00(BgL_typez00_bglt);
	extern obj_t BGl_typezd2nilzd2valuez00zzobject_nilz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	extern obj_t BGl_za2destza2z00zzengine_paramz00;
	static obj_t BGl_z62classgenzd2widenzd2exprz62zzobject_classgenz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classgenzd2makezd2anonymousz00zzobject_classgenz00(BgL_typez00_bglt);
	static obj_t BGl_requirezd2initializa7ationz75zzobject_classgenz00 = BUNSPEC;
	extern obj_t BGl_makezd2typedzd2identz00zzast_identz00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_za2srczd2filesza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_tclassz00zzobject_classz00;
	extern obj_t BGl_stringzd2sanszd2z42z42zztype_toolsz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_classgenzd2walkzd2zzobject_classgenz00(void);
	extern obj_t BGl_makezd2classzd2makezd2formalszd2zzobject_slotsz00(obj_t);
	static obj_t BGl_classgenzd2makezd2zzobject_classgenz00(obj_t);
	extern obj_t BGl_za2passza2z00zzengine_paramz00;
	static obj_t BGl_classgenz00zzobject_classgenz00(obj_t);
	BGL_IMPORT obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzobject_classgenz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzobject_classgenz00(void);
	static obj_t BGl_z62classgenzd2slotzd2anonymousz62zzobject_classgenz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62classgenzd2predicatezd2anonymousz62zzobject_classgenz00(obj_t,
		obj_t);
	extern bool_t BGl_finalzd2classzf3z21zzobject_classz00(obj_t);
	static obj_t BGl_z62classgenzd2widenzd2anonymousz62zzobject_classgenz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_classgenzd2accessorszd2zzobject_classgenz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classgenzd2widenzd2exprz00zzobject_classgenz00(BgL_typez00_bglt, obj_t);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	static obj_t BGl_z62classgenzd2nilzd2anonymousz62zzobject_classgenz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t BGl_writez00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_z62classgenzd2allocatezd2exprz62zzobject_classgenz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzobject_classgenz00(obj_t, obj_t);
	extern obj_t BGl_makezd2typedzd2formalz00zzast_identz00(obj_t);
	static obj_t BGl_classgenzd2allocatezd2zzobject_classgenz00(BgL_typez00_bglt);
	static obj_t BGl_methodzd2initzd2zzobject_classgenz00(void);
	extern obj_t BGl_writezd2schemezd2commentz00zzwrite_schemez00(obj_t, obj_t);
	extern obj_t BGl_writezd2schemezd2filezd2headerzd2zzwrite_schemez00(obj_t,
		obj_t);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	BGL_IMPORT obj_t BGl_openzd2outputzd2filez00zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static bool_t BGl_bigloozd2domesticzd2classzf3zf3zzobject_classgenz00(obj_t);
	static obj_t BGl_unsafeze70ze7zzobject_classgenz00(BgL_typez00_bglt, obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classgenzd2allocatezd2anonymousz00zzobject_classgenz00
		(BgL_typez00_bglt);
	BGL_IMPORT obj_t bgl_close_output_port(obj_t);
	static obj_t BGl_classgenzd2nilzd2zzobject_classgenz00(obj_t);
	static obj_t BGl_classgenzd2slotzd2zzobject_classgenz00(obj_t, obj_t);
	static obj_t BGl_initzd2wideningze70z35zzobject_classgenz00(BgL_typez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classgenzd2widenzd2anonymousz00zzobject_classgenz00(BgL_typez00_bglt);
	static obj_t BGl_z62classgenzd2makezd2anonymousz62zzobject_classgenz00(obj_t,
		obj_t);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzobject_classgenz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzwrite_schemez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_privatez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_nilz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	extern obj_t BGl_fieldzd2accesszd2zzast_objectz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32166ze3ze5zzobject_classgenz00(obj_t,
		obj_t);
	extern bool_t BGl_slotzd2virtualzf3z21zzobject_slotsz00(BgL_slotz00_bglt);
	BGL_IMPORT obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t
		BGl_z62classgenzd2shrinkzd2anonymousz62zzobject_classgenz00(obj_t, obj_t);
	extern obj_t BGl_makezd2privatezd2sexpz00zzast_privatez00(obj_t, obj_t,
		obj_t);
	extern obj_t
		BGl_makezd2classzd2makezd2typedzd2formalsz00zzobject_slotsz00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzobject_classgenz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzobject_classgenz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzobject_classgenz00(void);
	BGL_IMPORT obj_t BGl_prefixz00zz__osz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzobject_classgenz00(void);
	extern obj_t BGl_parsezd2idzd2zzast_identz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classgenzd2slotzd2anonymousz00zzobject_classgenz00(BgL_typez00_bglt,
		BgL_slotz00_bglt);
	static obj_t
		BGl_z62classgenzd2allocatezd2anonymousz62zzobject_classgenz00(obj_t, obj_t);
	static obj_t BGl_z62classgenzd2walkzb0zzobject_classgenz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classgenzd2shrinkzd2anonymousz00zzobject_classgenz00(BgL_typez00_bglt);
	static obj_t BGl_czd2mallocze70z35zzobject_classgenz00(BgL_typez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classgenzd2predicatezd2anonymousz00zzobject_classgenz00
		(BgL_typez00_bglt);
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	extern obj_t BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00(obj_t);
	extern obj_t BGl_za2compilerzd2debugzd2traceza2z00zzengine_paramz00;
	static obj_t BGl_classgenzd2predicatezd2zzobject_classgenz00(obj_t);
	extern obj_t BGl_getzd2classzd2listz00zzobject_classz00(void);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classgenzd2nilzd2anonymousz00zzobject_classgenz00(BgL_typez00_bglt);
	static obj_t __cnst[45];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classgenzd2makezd2anonymouszd2envzd2zzobject_classgenz00,
		BgL_bgl_za762classgenza7d2ma2272z00,
		BGl_z62classgenzd2makezd2anonymousz62zzobject_classgenz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2249z00zzobject_classgenz00,
		BgL_bgl_string2249za700za7za7o2273za7, "Class accessors generation", 26);
	      DEFINE_STRING(BGl_string2250z00zzobject_classgenz00,
		BgL_bgl_string2250za700za7za7o2274za7, "   . ", 5);
	      DEFINE_STRING(BGl_string2251z00zzobject_classgenz00,
		BgL_bgl_string2251za700za7za7o2275za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string2252z00zzobject_classgenz00,
		BgL_bgl_string2252za700za7za7o2276za7, ".sch", 4);
	      DEFINE_STRING(BGl_string2253z00zzobject_classgenz00,
		BgL_bgl_string2253za700za7za7o2277za7, "Class accessors", 15);
	      DEFINE_STRING(BGl_string2254z00zzobject_classgenz00,
		BgL_bgl_string2254za700za7za7o2278za7, "The directives", 14);
	      DEFINE_STRING(BGl_string2255z00zzobject_classgenz00,
		BgL_bgl_string2255za700za7za7o2279za7, "(directives", 11);
	      DEFINE_STRING(BGl_string2256z00zzobject_classgenz00,
		BgL_bgl_string2256za700za7za7o2280za7,
		"(cond-expand ((and bigloo-class-sans (not bigloo-class-generate))\n", 66);
	      DEFINE_STRING(BGl_string2257z00zzobject_classgenz00,
		BgL_bgl_string2257za700za7za7o2281za7, "  (", 3);
	      DEFINE_STRING(BGl_string2258z00zzobject_classgenz00,
		BgL_bgl_string2258za700za7za7o2282za7, "    ", 4);
	      DEFINE_STRING(BGl_string2259z00zzobject_classgenz00,
		BgL_bgl_string2259za700za7za7o2283za7, ")))", 3);
	      DEFINE_STRING(BGl_string2260z00zzobject_classgenz00,
		BgL_bgl_string2260za700za7za7o2284za7, ")\n\n", 3);
	      DEFINE_STRING(BGl_string2261z00zzobject_classgenz00,
		BgL_bgl_string2261za700za7za7o2285za7, "The definitions", 15);
	      DEFINE_STRING(BGl_string2262z00zzobject_classgenz00,
		BgL_bgl_string2262za700za7za7o2286za7, "(cond-expand (bigloo-class-sans",
		31);
	      DEFINE_STRING(BGl_string2263z00zzobject_classgenz00,
		BgL_bgl_string2263za700za7za7o2287za7, "))\n", 3);
	      DEFINE_STRING(BGl_string2264z00zzobject_classgenz00,
		BgL_bgl_string2264za700za7za7o2288za7,
		"Can't allocate instance of abstract classes", 43);
	      DEFINE_STRING(BGl_string2265z00zzobject_classgenz00,
		BgL_bgl_string2265za700za7za7o2289za7, "bmem_set_allocation_type( $1, 0 )",
		33);
	      DEFINE_STRING(BGl_string2266z00zzobject_classgenz00,
		BgL_bgl_string2266za700za7za7o2290za7, ") )))", 5);
	      DEFINE_STRING(BGl_string2267z00zzobject_classgenz00,
		BgL_bgl_string2267za700za7za7o2291za7, ")BOBJECT( GC_MALLOC( sizeof(", 28);
	      DEFINE_STRING(BGl_string2268z00zzobject_classgenz00,
		BgL_bgl_string2268za700za7za7o2292za7, "((", 2);
	      DEFINE_STRING(BGl_string2269z00zzobject_classgenz00,
		BgL_bgl_string2269za700za7za7o2293za7, "object_classgen", 15);
	      DEFINE_STRING(BGl_string2270z00zzobject_classgenz00,
		BgL_bgl_string2270za700za7za7o2294za7,
		"v -set! - shrink! o begin cast wide tmp unsafe classgen object-widening-set! free-pragma pragma::void quote %allocate- current-dynamic-env alloc dynamic-env env let object-class-num-set! class-num l error set! -nil define class-nil new make- instantiate lambda inline ::obj bool ? define-inline obj @ isa? __object obj::obj --to-stdout pass-started ",
		349);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classgenzd2widenzd2exprzd2envzd2zzobject_classgenz00,
		BgL_bgl_za762classgenza7d2wi2295z00,
		BGl_z62classgenzd2widenzd2exprz62zzobject_classgenz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classgenzd2predicatezd2anonymouszd2envzd2zzobject_classgenz00,
		BgL_bgl_za762classgenza7d2pr2296z00,
		BGl_z62classgenzd2predicatezd2anonymousz62zzobject_classgenz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classgenzd2slotzd2anonymouszd2envzd2zzobject_classgenz00,
		BgL_bgl_za762classgenza7d2sl2297z00,
		BGl_z62classgenzd2slotzd2anonymousz62zzobject_classgenz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classgenzd2allocatezd2anonymouszd2envzd2zzobject_classgenz00,
		BgL_bgl_za762classgenza7d2al2298z00,
		BGl_z62classgenzd2allocatezd2anonymousz62zzobject_classgenz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classgenzd2walkzd2envz00zzobject_classgenz00,
		BgL_bgl_za762classgenza7d2wa2299z00,
		BGl_z62classgenzd2walkzb0zzobject_classgenz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classgenzd2nilzd2anonymouszd2envzd2zzobject_classgenz00,
		BgL_bgl_za762classgenza7d2ni2300z00,
		BGl_z62classgenzd2nilzd2anonymousz62zzobject_classgenz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classgenzd2allocatezd2exprzd2envzd2zzobject_classgenz00,
		BgL_bgl_za762classgenza7d2al2301z00,
		BGl_z62classgenzd2allocatezd2exprz62zzobject_classgenz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classgenzd2widenzd2anonymouszd2envzd2zzobject_classgenz00,
		BgL_bgl_za762classgenza7d2wi2302z00,
		BGl_z62classgenzd2widenzd2anonymousz62zzobject_classgenz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classgenzd2shrinkzd2anonymouszd2envzd2zzobject_classgenz00,
		BgL_bgl_za762classgenza7d2sh2303z00,
		BGl_z62classgenzd2shrinkzd2anonymousz62zzobject_classgenz00, 0L, BUNSPEC,
		1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzobject_classgenz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzobject_classgenz00(long
		BgL_checksumz00_3141, char *BgL_fromz00_3142)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzobject_classgenz00))
				{
					BGl_requirezd2initializa7ationz75zzobject_classgenz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzobject_classgenz00();
					BGl_libraryzd2moduleszd2initz00zzobject_classgenz00();
					BGl_cnstzd2initzd2zzobject_classgenz00();
					BGl_importedzd2moduleszd2initz00zzobject_classgenz00();
					return BGl_methodzd2initzd2zzobject_classgenz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzobject_classgenz00(void)
	{
		{	/* Object/classgen.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"object_classgen");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"object_classgen");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "object_classgen");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"object_classgen");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"object_classgen");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "object_classgen");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"object_classgen");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "object_classgen");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"object_classgen");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"object_classgen");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"object_classgen");
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(0L,
				"object_classgen");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzobject_classgenz00(void)
	{
		{	/* Object/classgen.scm 15 */
			{	/* Object/classgen.scm 15 */
				obj_t BgL_cportz00_3118;

				{	/* Object/classgen.scm 15 */
					obj_t BgL_stringz00_3125;

					BgL_stringz00_3125 = BGl_string2270z00zzobject_classgenz00;
					{	/* Object/classgen.scm 15 */
						obj_t BgL_startz00_3126;

						BgL_startz00_3126 = BINT(0L);
						{	/* Object/classgen.scm 15 */
							obj_t BgL_endz00_3127;

							BgL_endz00_3127 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3125)));
							{	/* Object/classgen.scm 15 */

								BgL_cportz00_3118 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3125, BgL_startz00_3126, BgL_endz00_3127);
				}}}}
				{
					long BgL_iz00_3119;

					BgL_iz00_3119 = 44L;
				BgL_loopz00_3120:
					if ((BgL_iz00_3119 == -1L))
						{	/* Object/classgen.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Object/classgen.scm 15 */
							{	/* Object/classgen.scm 15 */
								obj_t BgL_arg2271z00_3121;

								{	/* Object/classgen.scm 15 */

									{	/* Object/classgen.scm 15 */
										obj_t BgL_locationz00_3123;

										BgL_locationz00_3123 = BBOOL(((bool_t) 0));
										{	/* Object/classgen.scm 15 */

											BgL_arg2271z00_3121 =
												BGl_readz00zz__readerz00(BgL_cportz00_3118,
												BgL_locationz00_3123);
										}
									}
								}
								{	/* Object/classgen.scm 15 */
									int BgL_tmpz00_3172;

									BgL_tmpz00_3172 = (int) (BgL_iz00_3119);
									CNST_TABLE_SET(BgL_tmpz00_3172, BgL_arg2271z00_3121);
							}}
							{	/* Object/classgen.scm 15 */
								int BgL_auxz00_3124;

								BgL_auxz00_3124 = (int) ((BgL_iz00_3119 - 1L));
								{
									long BgL_iz00_3177;

									BgL_iz00_3177 = (long) (BgL_auxz00_3124);
									BgL_iz00_3119 = BgL_iz00_3177;
									goto BgL_loopz00_3120;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzobject_classgenz00(void)
	{
		{	/* Object/classgen.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzobject_classgenz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1729;

				BgL_headz00_1729 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1730;
					obj_t BgL_tailz00_1731;

					BgL_prevz00_1730 = BgL_headz00_1729;
					BgL_tailz00_1731 = BgL_l1z00_1;
				BgL_loopz00_1732:
					if (PAIRP(BgL_tailz00_1731))
						{
							obj_t BgL_newzd2prevzd2_1734;

							BgL_newzd2prevzd2_1734 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1731), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1730, BgL_newzd2prevzd2_1734);
							{
								obj_t BgL_tailz00_3187;
								obj_t BgL_prevz00_3186;

								BgL_prevz00_3186 = BgL_newzd2prevzd2_1734;
								BgL_tailz00_3187 = CDR(BgL_tailz00_1731);
								BgL_tailz00_1731 = BgL_tailz00_3187;
								BgL_prevz00_1730 = BgL_prevz00_3186;
								goto BgL_loopz00_1732;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1729);
				}
			}
		}

	}



/* classgen-walk */
	BGL_EXPORTED_DEF obj_t BGl_classgenzd2walkzd2zzobject_classgenz00(void)
	{
		{	/* Object/classgen.scm 47 */
			{	/* Object/classgen.scm 48 */
				obj_t BgL_list1316z00_1737;

				{	/* Object/classgen.scm 48 */
					obj_t BgL_arg1317z00_1738;

					{	/* Object/classgen.scm 48 */
						obj_t BgL_arg1318z00_1739;

						BgL_arg1318z00_1739 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1317z00_1738 =
							MAKE_YOUNG_PAIR(BGl_string2249z00zzobject_classgenz00,
							BgL_arg1318z00_1739);
					}
					BgL_list1316z00_1737 =
						MAKE_YOUNG_PAIR(BGl_string2250z00zzobject_classgenz00,
						BgL_arg1317z00_1738);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1316z00_1737);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string2249z00zzobject_classgenz00;
			{	/* Object/classgen.scm 48 */
				obj_t BgL_g1112z00_1740;

				BgL_g1112z00_1740 = BNIL;
				{
					obj_t BgL_hooksz00_1743;
					obj_t BgL_hnamesz00_1744;

					BgL_hooksz00_1743 = BgL_g1112z00_1740;
					BgL_hnamesz00_1744 = BNIL;
				BgL_zc3z04anonymousza31319ze3z87_1745:
					if (NULLP(BgL_hooksz00_1743))
						{	/* Object/classgen.scm 48 */
							CNST_TABLE_REF(0);
						}
					else
						{	/* Object/classgen.scm 48 */
							bool_t BgL_test2308z00_3200;

							{	/* Object/classgen.scm 48 */
								obj_t BgL_fun1327z00_1752;

								BgL_fun1327z00_1752 = CAR(((obj_t) BgL_hooksz00_1743));
								BgL_test2308z00_3200 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1327z00_1752));
							}
							if (BgL_test2308z00_3200)
								{	/* Object/classgen.scm 48 */
									obj_t BgL_arg1323z00_1749;
									obj_t BgL_arg1325z00_1750;

									BgL_arg1323z00_1749 = CDR(((obj_t) BgL_hooksz00_1743));
									BgL_arg1325z00_1750 = CDR(((obj_t) BgL_hnamesz00_1744));
									{
										obj_t BgL_hnamesz00_3212;
										obj_t BgL_hooksz00_3211;

										BgL_hooksz00_3211 = BgL_arg1323z00_1749;
										BgL_hnamesz00_3212 = BgL_arg1325z00_1750;
										BgL_hnamesz00_1744 = BgL_hnamesz00_3212;
										BgL_hooksz00_1743 = BgL_hooksz00_3211;
										goto BgL_zc3z04anonymousza31319ze3z87_1745;
									}
								}
							else
								{	/* Object/classgen.scm 48 */
									obj_t BgL_arg1326z00_1751;

									BgL_arg1326z00_1751 = CAR(((obj_t) BgL_hnamesz00_1744));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string2249z00zzobject_classgenz00,
										BGl_string2251z00zzobject_classgenz00, BgL_arg1326z00_1751);
								}
						}
				}
			}
			{	/* Object/classgen.scm 50 */
				obj_t BgL_onamez00_1755;

				if (STRINGP(BGl_za2destza2z00zzengine_paramz00))
					{	/* Object/classgen.scm 51 */
						BgL_onamez00_1755 = BGl_za2destza2z00zzengine_paramz00;
					}
				else
					{	/* Object/classgen.scm 51 */
						if ((BGl_za2destza2z00zzengine_paramz00 == CNST_TABLE_REF(1)))
							{	/* Object/classgen.scm 53 */
								BgL_onamez00_1755 = BFALSE;
							}
						else
							{	/* Object/classgen.scm 55 */
								bool_t BgL_test2311z00_3221;

								if (PAIRP(BGl_za2srczd2filesza2zd2zzengine_paramz00))
									{	/* Object/classgen.scm 55 */
										obj_t BgL_tmpz00_3224;

										BgL_tmpz00_3224 =
											CAR(BGl_za2srczd2filesza2zd2zzengine_paramz00);
										BgL_test2311z00_3221 = STRINGP(BgL_tmpz00_3224);
									}
								else
									{	/* Object/classgen.scm 55 */
										BgL_test2311z00_3221 = ((bool_t) 0);
									}
								if (BgL_test2311z00_3221)
									{	/* Object/classgen.scm 55 */
										BgL_onamez00_1755 =
											string_append(BGl_prefixz00zz__osz00(CAR
												(BGl_za2srczd2filesza2zd2zzengine_paramz00)),
											BGl_string2252z00zzobject_classgenz00);
									}
								else
									{	/* Object/classgen.scm 55 */
										BgL_onamez00_1755 = BFALSE;
									}
							}
					}
				{	/* Object/classgen.scm 50 */
					obj_t BgL_poz00_1756;

					if (STRINGP(BgL_onamez00_1755))
						{	/* Object/classgen.scm 60 */

							BgL_poz00_1756 =
								BGl_openzd2outputzd2filez00zz__r4_ports_6_10_1z00
								(BgL_onamez00_1755, BTRUE);
						}
					else
						{	/* Object/classgen.scm 61 */
							obj_t BgL_tmpz00_3233;

							BgL_tmpz00_3233 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_poz00_1756 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_3233);
						}
					{	/* Object/classgen.scm 59 */

						BGl_writezd2schemezd2filezd2headerzd2zzwrite_schemez00
							(BgL_poz00_1756, BGl_string2253z00zzobject_classgenz00);
						{	/* Object/classgen.scm 65 */
							obj_t BgL_protosz00_1757;
							obj_t BgL_defsz00_1758;

							BgL_protosz00_1757 = BNIL;
							BgL_defsz00_1758 = BNIL;
							{	/* Object/classgen.scm 67 */
								obj_t BgL_g1251z00_1759;

								BgL_g1251z00_1759 =
									BGl_getzd2classzd2listz00zzobject_classz00();
								{
									obj_t BgL_l1249z00_1761;

									BgL_l1249z00_1761 = BgL_g1251z00_1759;
								BgL_zc3z04anonymousza31329ze3z87_1762:
									if (PAIRP(BgL_l1249z00_1761))
										{	/* Object/classgen.scm 73 */
											{	/* Object/classgen.scm 68 */
												obj_t BgL_cz00_1764;

												BgL_cz00_1764 = CAR(BgL_l1249z00_1761);
												if (BGl_bigloozd2domesticzd2classzf3zf3zzobject_classgenz00(BgL_cz00_1764))
													{	/* Object/classgen.scm 69 */
														obj_t BgL_pz00_1766;

														BgL_pz00_1766 =
															BGl_classgenz00zzobject_classgenz00
															(BgL_cz00_1764);
														{	/* Object/classgen.scm 70 */
															obj_t BgL_dz00_1767;

															{	/* Object/classgen.scm 71 */
																obj_t BgL_tmpz00_2742;

																{	/* Object/classgen.scm 71 */
																	int BgL_tmpz00_3244;

																	BgL_tmpz00_3244 = (int) (1L);
																	BgL_tmpz00_2742 =
																		BGL_MVALUES_VAL(BgL_tmpz00_3244);
																}
																{	/* Object/classgen.scm 71 */
																	int BgL_tmpz00_3247;

																	BgL_tmpz00_3247 = (int) (1L);
																	BGL_MVALUES_VAL_SET(BgL_tmpz00_3247, BUNSPEC);
																}
																BgL_dz00_1767 = BgL_tmpz00_2742;
															}
															{	/* Object/classgen.scm 71 */
																obj_t BgL_arg1332z00_1768;

																BgL_arg1332z00_1768 =
																	MAKE_YOUNG_PAIR(BgL_cz00_1764, BgL_pz00_1766);
																BgL_protosz00_1757 =
																	MAKE_YOUNG_PAIR(BgL_arg1332z00_1768,
																	BgL_protosz00_1757);
															}
															{	/* Object/classgen.scm 72 */
																obj_t BgL_arg1333z00_1769;

																BgL_arg1333z00_1769 =
																	MAKE_YOUNG_PAIR(BgL_cz00_1764, BgL_dz00_1767);
																BgL_defsz00_1758 =
																	MAKE_YOUNG_PAIR(BgL_arg1333z00_1769,
																	BgL_defsz00_1758);
													}}}
												else
													{	/* Object/classgen.scm 68 */
														BFALSE;
													}
											}
											{
												obj_t BgL_l1249z00_3254;

												BgL_l1249z00_3254 = CDR(BgL_l1249z00_1761);
												BgL_l1249z00_1761 = BgL_l1249z00_3254;
												goto BgL_zc3z04anonymousza31329ze3z87_1762;
											}
										}
									else
										{	/* Object/classgen.scm 73 */
											((bool_t) 1);
										}
								}
							}
							if (NULLP(BgL_protosz00_1757))
								{	/* Object/classgen.scm 74 */
									return BFALSE;
								}
							else
								{	/* Object/classgen.scm 74 */
									{	/* Object/classgen.scm 75 */
										obj_t BgL_list1337z00_1773;

										BgL_list1337z00_1773 =
											MAKE_YOUNG_PAIR(BGl_string2254z00zzobject_classgenz00,
											BNIL);
										BGl_writezd2schemezd2commentz00zzwrite_schemez00
											(BgL_poz00_1756, BgL_list1337z00_1773);
									}
									{	/* Object/classgen.scm 76 */
										obj_t BgL_tmpz00_3260;

										BgL_tmpz00_3260 = ((obj_t) BgL_poz00_1756);
										bgl_display_string(BGl_string2255z00zzobject_classgenz00,
											BgL_tmpz00_3260);
									}
									{
										obj_t BgL_l1255z00_1775;

										BgL_l1255z00_1775 = BgL_protosz00_1757;
									BgL_zc3z04anonymousza31338ze3z87_1776:
										if (PAIRP(BgL_l1255z00_1775))
											{	/* Object/classgen.scm 77 */
												{	/* Object/classgen.scm 78 */
													obj_t BgL_pz00_1778;

													BgL_pz00_1778 = CAR(BgL_l1255z00_1775);
													{	/* Object/classgen.scm 78 */
														obj_t BgL_cz00_1779;
														obj_t BgL_protosz00_1780;

														BgL_cz00_1779 = CAR(((obj_t) BgL_pz00_1778));
														BgL_protosz00_1780 = CDR(((obj_t) BgL_pz00_1778));
														{	/* Object/classgen.scm 80 */
															obj_t BgL_tmpz00_3270;

															BgL_tmpz00_3270 = ((obj_t) BgL_poz00_1756);
															bgl_display_char(((unsigned char) 10),
																BgL_tmpz00_3270);
														}
														{	/* Object/classgen.scm 81 */
															obj_t BgL_tmpz00_3273;

															BgL_tmpz00_3273 = ((obj_t) BgL_poz00_1756);
															bgl_display_char(((unsigned char) 10),
																BgL_tmpz00_3273);
														}
														{	/* Object/classgen.scm 82 */
															obj_t BgL_arg1340z00_1781;

															BgL_arg1340z00_1781 =
																(((BgL_typez00_bglt) COBJECT(
																		((BgL_typez00_bglt)
																			((BgL_typez00_bglt) BgL_cz00_1779))))->
																BgL_idz00);
															{	/* Object/classgen.scm 82 */
																obj_t BgL_list1341z00_1782;

																BgL_list1341z00_1782 =
																	MAKE_YOUNG_PAIR(BgL_arg1340z00_1781, BNIL);
																BGl_writezd2schemezd2commentz00zzwrite_schemez00
																	(BgL_poz00_1756, BgL_list1341z00_1782);
														}}
														{	/* Object/classgen.scm 83 */
															obj_t BgL_tmpz00_3281;

															BgL_tmpz00_3281 = ((obj_t) BgL_poz00_1756);
															bgl_display_string
																(BGl_string2256z00zzobject_classgenz00,
																BgL_tmpz00_3281);
														}
														{	/* Object/classgen.scm 84 */
															obj_t BgL_tmpz00_3284;

															BgL_tmpz00_3284 = ((obj_t) BgL_poz00_1756);
															bgl_display_string
																(BGl_string2257z00zzobject_classgenz00,
																BgL_tmpz00_3284);
														}
														{	/* Object/classgen.scm 85 */
															obj_t BgL_arg1342z00_1783;

															BgL_arg1342z00_1783 =
																CAR(((obj_t) BgL_protosz00_1780));
															bgl_display_obj(BgL_arg1342z00_1783,
																BgL_poz00_1756);
														}
														{	/* Object/classgen.scm 86 */
															obj_t BgL_g1254z00_1784;

															BgL_g1254z00_1784 =
																CDR(((obj_t) BgL_protosz00_1780));
															{
																obj_t BgL_l1252z00_1786;

																BgL_l1252z00_1786 = BgL_g1254z00_1784;
															BgL_zc3z04anonymousza31343ze3z87_1787:
																if (PAIRP(BgL_l1252z00_1786))
																	{	/* Object/classgen.scm 90 */
																		{	/* Object/classgen.scm 87 */
																			obj_t BgL_pz00_1789;

																			BgL_pz00_1789 = CAR(BgL_l1252z00_1786);
																			{	/* Object/classgen.scm 87 */
																				obj_t BgL_tmpz00_3295;

																				BgL_tmpz00_3295 =
																					((obj_t) BgL_poz00_1756);
																				bgl_display_char(((unsigned char) 10),
																					BgL_tmpz00_3295);
																			}
																			{	/* Object/classgen.scm 88 */
																				obj_t BgL_tmpz00_3298;

																				BgL_tmpz00_3298 =
																					((obj_t) BgL_poz00_1756);
																				bgl_display_string
																					(BGl_string2258z00zzobject_classgenz00,
																					BgL_tmpz00_3298);
																			}
																			bgl_display_obj(BgL_pz00_1789,
																				BgL_poz00_1756);
																		}
																		{
																			obj_t BgL_l1252z00_3302;

																			BgL_l1252z00_3302 =
																				CDR(BgL_l1252z00_1786);
																			BgL_l1252z00_1786 = BgL_l1252z00_3302;
																			goto
																				BgL_zc3z04anonymousza31343ze3z87_1787;
																		}
																	}
																else
																	{	/* Object/classgen.scm 90 */
																		((bool_t) 1);
																	}
															}
														}
														{	/* Object/classgen.scm 91 */
															obj_t BgL_tmpz00_3304;

															BgL_tmpz00_3304 = ((obj_t) BgL_poz00_1756);
															bgl_display_string
																(BGl_string2259z00zzobject_classgenz00,
																BgL_tmpz00_3304);
														}
													}
												}
												{
													obj_t BgL_l1255z00_3307;

													BgL_l1255z00_3307 = CDR(BgL_l1255z00_1775);
													BgL_l1255z00_1775 = BgL_l1255z00_3307;
													goto BgL_zc3z04anonymousza31338ze3z87_1776;
												}
											}
										else
											{	/* Object/classgen.scm 77 */
												((bool_t) 1);
											}
									}
									{	/* Object/classgen.scm 93 */
										obj_t BgL_tmpz00_3309;

										BgL_tmpz00_3309 = ((obj_t) BgL_poz00_1756);
										bgl_display_string(BGl_string2260z00zzobject_classgenz00,
											BgL_tmpz00_3309);
									}
									{	/* Object/classgen.scm 95 */
										obj_t BgL_list1349z00_1794;

										BgL_list1349z00_1794 =
											MAKE_YOUNG_PAIR(BGl_string2261z00zzobject_classgenz00,
											BNIL);
										BGl_writezd2schemezd2commentz00zzwrite_schemez00
											(BgL_poz00_1756, BgL_list1349z00_1794);
									}
									{	/* Object/classgen.scm 96 */
										obj_t BgL_tmpz00_3314;

										BgL_tmpz00_3314 = ((obj_t) BgL_poz00_1756);
										bgl_display_string(BGl_string2262z00zzobject_classgenz00,
											BgL_tmpz00_3314);
									}
									{
										obj_t BgL_l1259z00_1796;

										BgL_l1259z00_1796 = BgL_defsz00_1758;
									BgL_zc3z04anonymousza31350ze3z87_1797:
										if (PAIRP(BgL_l1259z00_1796))
											{	/* Object/classgen.scm 97 */
												{	/* Object/classgen.scm 98 */
													obj_t BgL_pz00_1799;

													BgL_pz00_1799 = CAR(BgL_l1259z00_1796);
													{	/* Object/classgen.scm 98 */
														obj_t BgL_cz00_1800;
														obj_t BgL_defsz00_1801;

														BgL_cz00_1800 = CAR(((obj_t) BgL_pz00_1799));
														BgL_defsz00_1801 = CDR(((obj_t) BgL_pz00_1799));
														{	/* Object/classgen.scm 100 */
															obj_t BgL_tmpz00_3324;

															BgL_tmpz00_3324 = ((obj_t) BgL_poz00_1756);
															bgl_display_char(((unsigned char) 10),
																BgL_tmpz00_3324);
														}
														{	/* Object/classgen.scm 101 */
															obj_t BgL_arg1361z00_1802;

															BgL_arg1361z00_1802 =
																(((BgL_typez00_bglt) COBJECT(
																		((BgL_typez00_bglt)
																			((BgL_typez00_bglt) BgL_cz00_1800))))->
																BgL_idz00);
															{	/* Object/classgen.scm 101 */
																obj_t BgL_list1362z00_1803;

																BgL_list1362z00_1803 =
																	MAKE_YOUNG_PAIR(BgL_arg1361z00_1802, BNIL);
																BGl_writezd2schemezd2commentz00zzwrite_schemez00
																	(BgL_poz00_1756, BgL_list1362z00_1803);
														}}
														{
															obj_t BgL_l1257z00_1805;

															BgL_l1257z00_1805 = BgL_defsz00_1801;
														BgL_zc3z04anonymousza31363ze3z87_1806:
															if (PAIRP(BgL_l1257z00_1805))
																{	/* Object/classgen.scm 102 */
																	{	/* Object/classgen.scm 103 */
																		obj_t BgL_pz00_1808;

																		BgL_pz00_1808 = CAR(BgL_l1257z00_1805);
																		{	/* Object/classgen.scm 103 */
																			obj_t BgL_list1366z00_1809;

																			BgL_list1366z00_1809 =
																				MAKE_YOUNG_PAIR(BgL_poz00_1756, BNIL);
																			BGl_writez00zz__r4_output_6_10_3z00
																				(BgL_pz00_1808, BgL_list1366z00_1809);
																		}
																		{	/* Object/classgen.scm 104 */
																			obj_t BgL_tmpz00_3337;

																			BgL_tmpz00_3337 =
																				((obj_t) BgL_poz00_1756);
																			bgl_display_char(((unsigned char) 10),
																				BgL_tmpz00_3337);
																	}}
																	{
																		obj_t BgL_l1257z00_3340;

																		BgL_l1257z00_3340 = CDR(BgL_l1257z00_1805);
																		BgL_l1257z00_1805 = BgL_l1257z00_3340;
																		goto BgL_zc3z04anonymousza31363ze3z87_1806;
																	}
																}
															else
																{	/* Object/classgen.scm 102 */
																	((bool_t) 1);
																}
														}
													}
												}
												{
													obj_t BgL_l1259z00_3342;

													BgL_l1259z00_3342 = CDR(BgL_l1259z00_1796);
													BgL_l1259z00_1796 = BgL_l1259z00_3342;
													goto BgL_zc3z04anonymousza31350ze3z87_1797;
												}
											}
										else
											{	/* Object/classgen.scm 97 */
												((bool_t) 1);
											}
									}
									{	/* Object/classgen.scm 107 */
										obj_t BgL_tmpz00_3344;

										BgL_tmpz00_3344 = ((obj_t) BgL_poz00_1756);
										bgl_display_string(BGl_string2263z00zzobject_classgenz00,
											BgL_tmpz00_3344);
									}
									return bgl_close_output_port(((obj_t) BgL_poz00_1756));
								}
						}
					}
				}
			}
		}

	}



/* &classgen-walk */
	obj_t BGl_z62classgenzd2walkzb0zzobject_classgenz00(obj_t BgL_envz00_3091)
	{
		{	/* Object/classgen.scm 47 */
			return BGl_classgenzd2walkzd2zzobject_classgenz00();
		}

	}



/* bigloo-domestic-class? */
	bool_t BGl_bigloozd2domesticzd2classzf3zf3zzobject_classgenz00(obj_t
		BgL_cz00_3)
	{
		{	/* Object/classgen.scm 113 */
			{	/* Object/classgen.scm 114 */
				bool_t BgL_test2321z00_3350;

				{	/* Object/classgen.scm 114 */
					obj_t BgL_classz00_2783;

					BgL_classz00_2783 = BGl_tclassz00zzobject_classz00;
					if (BGL_OBJECTP(BgL_cz00_3))
						{	/* Object/classgen.scm 114 */
							BgL_objectz00_bglt BgL_arg1807z00_2785;

							BgL_arg1807z00_2785 = (BgL_objectz00_bglt) (BgL_cz00_3);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Object/classgen.scm 114 */
									long BgL_idxz00_2791;

									BgL_idxz00_2791 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2785);
									BgL_test2321z00_3350 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2791 + 2L)) == BgL_classz00_2783);
								}
							else
								{	/* Object/classgen.scm 114 */
									bool_t BgL_res2244z00_2816;

									{	/* Object/classgen.scm 114 */
										obj_t BgL_oclassz00_2799;

										{	/* Object/classgen.scm 114 */
											obj_t BgL_arg1815z00_2807;
											long BgL_arg1816z00_2808;

											BgL_arg1815z00_2807 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Object/classgen.scm 114 */
												long BgL_arg1817z00_2809;

												BgL_arg1817z00_2809 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2785);
												BgL_arg1816z00_2808 =
													(BgL_arg1817z00_2809 - OBJECT_TYPE);
											}
											BgL_oclassz00_2799 =
												VECTOR_REF(BgL_arg1815z00_2807, BgL_arg1816z00_2808);
										}
										{	/* Object/classgen.scm 114 */
											bool_t BgL__ortest_1115z00_2800;

											BgL__ortest_1115z00_2800 =
												(BgL_classz00_2783 == BgL_oclassz00_2799);
											if (BgL__ortest_1115z00_2800)
												{	/* Object/classgen.scm 114 */
													BgL_res2244z00_2816 = BgL__ortest_1115z00_2800;
												}
											else
												{	/* Object/classgen.scm 114 */
													long BgL_odepthz00_2801;

													{	/* Object/classgen.scm 114 */
														obj_t BgL_arg1804z00_2802;

														BgL_arg1804z00_2802 = (BgL_oclassz00_2799);
														BgL_odepthz00_2801 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2802);
													}
													if ((2L < BgL_odepthz00_2801))
														{	/* Object/classgen.scm 114 */
															obj_t BgL_arg1802z00_2804;

															{	/* Object/classgen.scm 114 */
																obj_t BgL_arg1803z00_2805;

																BgL_arg1803z00_2805 = (BgL_oclassz00_2799);
																BgL_arg1802z00_2804 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2805,
																	2L);
															}
															BgL_res2244z00_2816 =
																(BgL_arg1802z00_2804 == BgL_classz00_2783);
														}
													else
														{	/* Object/classgen.scm 114 */
															BgL_res2244z00_2816 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2321z00_3350 = BgL_res2244z00_2816;
								}
						}
					else
						{	/* Object/classgen.scm 114 */
							BgL_test2321z00_3350 = ((bool_t) 0);
						}
				}
				if (BgL_test2321z00_3350)
					{	/* Object/classgen.scm 114 */
						obj_t BgL_arg1422z00_1826;

						{	/* Object/classgen.scm 114 */
							BgL_globalz00_bglt BgL_arg1434z00_1827;

							{
								BgL_tclassz00_bglt BgL_auxz00_3373;

								{
									obj_t BgL_auxz00_3374;

									{	/* Object/classgen.scm 114 */
										BgL_objectz00_bglt BgL_tmpz00_3375;

										BgL_tmpz00_3375 =
											((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_cz00_3));
										BgL_auxz00_3374 = BGL_OBJECT_WIDENING(BgL_tmpz00_3375);
									}
									BgL_auxz00_3373 = ((BgL_tclassz00_bglt) BgL_auxz00_3374);
								}
								BgL_arg1434z00_1827 =
									(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3373))->
									BgL_holderz00);
							}
							BgL_arg1422z00_1826 =
								(((BgL_globalz00_bglt) COBJECT(BgL_arg1434z00_1827))->
								BgL_modulez00);
						}
						return
							(BgL_arg1422z00_1826 == BGl_za2moduleza2z00zzmodule_modulez00);
					}
				else
					{	/* Object/classgen.scm 114 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* classgen */
	obj_t BGl_classgenz00zzobject_classgenz00(obj_t BgL_cz00_4)
	{
		{	/* Object/classgen.scm 119 */
			{	/* Object/classgen.scm 120 */
				obj_t BgL_predzd2pzd2_1828;

				BgL_predzd2pzd2_1828 =
					BGl_classgenzd2predicatezd2zzobject_classgenz00(BgL_cz00_4);
				{	/* Object/classgen.scm 121 */
					obj_t BgL_predzd2dzd2_1829;

					{	/* Object/classgen.scm 122 */
						obj_t BgL_tmpz00_2820;

						{	/* Object/classgen.scm 122 */
							int BgL_tmpz00_3384;

							BgL_tmpz00_3384 = (int) (1L);
							BgL_tmpz00_2820 = BGL_MVALUES_VAL(BgL_tmpz00_3384);
						}
						{	/* Object/classgen.scm 122 */
							int BgL_tmpz00_3387;

							BgL_tmpz00_3387 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3387, BUNSPEC);
						}
						BgL_predzd2dzd2_1829 = BgL_tmpz00_2820;
					}
					{	/* Object/classgen.scm 122 */
						obj_t BgL_nilzd2pzd2_1830;

						BgL_nilzd2pzd2_1830 =
							BGl_classgenzd2nilzd2zzobject_classgenz00(BgL_cz00_4);
						{	/* Object/classgen.scm 123 */
							obj_t BgL_nilzd2dzd2_1831;

							{	/* Object/classgen.scm 124 */
								obj_t BgL_tmpz00_2821;

								{	/* Object/classgen.scm 124 */
									int BgL_tmpz00_3391;

									BgL_tmpz00_3391 = (int) (1L);
									BgL_tmpz00_2821 = BGL_MVALUES_VAL(BgL_tmpz00_3391);
								}
								{	/* Object/classgen.scm 124 */
									int BgL_tmpz00_3394;

									BgL_tmpz00_3394 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3394, BUNSPEC);
								}
								BgL_nilzd2dzd2_1831 = BgL_tmpz00_2821;
							}
							{	/* Object/classgen.scm 124 */
								obj_t BgL_accesszd2pzd2_1832;

								BgL_accesszd2pzd2_1832 =
									BGl_classgenzd2accessorszd2zzobject_classgenz00(BgL_cz00_4);
								{	/* Object/classgen.scm 125 */
									obj_t BgL_accesszd2dzd2_1833;

									{	/* Object/classgen.scm 126 */
										obj_t BgL_tmpz00_2822;

										{	/* Object/classgen.scm 126 */
											int BgL_tmpz00_3398;

											BgL_tmpz00_3398 = (int) (1L);
											BgL_tmpz00_2822 = BGL_MVALUES_VAL(BgL_tmpz00_3398);
										}
										{	/* Object/classgen.scm 126 */
											int BgL_tmpz00_3401;

											BgL_tmpz00_3401 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_3401, BUNSPEC);
										}
										BgL_accesszd2dzd2_1833 = BgL_tmpz00_2822;
									}
									{	/* Object/classgen.scm 126 */
										obj_t BgL_pz00_1834;
										obj_t BgL_dz00_1835;

										{	/* Object/classgen.scm 126 */
											obj_t BgL_arg1454z00_1846;

											BgL_arg1454z00_1846 =
												MAKE_YOUNG_PAIR(BgL_nilzd2pzd2_1830,
												BgL_accesszd2pzd2_1832);
											BgL_pz00_1834 =
												MAKE_YOUNG_PAIR(BgL_predzd2pzd2_1828,
												BgL_arg1454z00_1846);
										}
										{	/* Object/classgen.scm 127 */
											obj_t BgL_arg1472z00_1847;

											BgL_arg1472z00_1847 =
												MAKE_YOUNG_PAIR(BgL_nilzd2dzd2_1831,
												BgL_accesszd2dzd2_1833);
											BgL_dz00_1835 =
												MAKE_YOUNG_PAIR(BgL_predzd2dzd2_1829,
												BgL_arg1472z00_1847);
										}
										{	/* Object/classgen.scm 128 */
											bool_t BgL_test2326z00_3408;

											{
												BgL_tclassz00_bglt BgL_auxz00_3409;

												{
													obj_t BgL_auxz00_3410;

													{	/* Object/classgen.scm 128 */
														BgL_objectz00_bglt BgL_tmpz00_3411;

														BgL_tmpz00_3411 =
															((BgL_objectz00_bglt)
															((BgL_typez00_bglt) BgL_cz00_4));
														BgL_auxz00_3410 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_3411);
													}
													BgL_auxz00_3409 =
														((BgL_tclassz00_bglt) BgL_auxz00_3410);
												}
												BgL_test2326z00_3408 =
													(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3409))->
													BgL_abstractzf3zf3);
											}
											if (BgL_test2326z00_3408)
												{	/* Object/classgen.scm 129 */
													obj_t BgL_val0_1261z00_1837;

													{	/* Object/classgen.scm 129 */
														obj_t BgL_arg1437z00_1839;

														{	/* Object/classgen.scm 139 */
															BgL_globalz00_bglt BgL_arg1473z00_2825;

															{
																BgL_tclassz00_bglt BgL_auxz00_3417;

																{
																	obj_t BgL_auxz00_3418;

																	{	/* Object/classgen.scm 139 */
																		BgL_objectz00_bglt BgL_tmpz00_3419;

																		BgL_tmpz00_3419 =
																			((BgL_objectz00_bglt)
																			((BgL_typez00_bglt) BgL_cz00_4));
																		BgL_auxz00_3418 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_3419);
																	}
																	BgL_auxz00_3417 =
																		((BgL_tclassz00_bglt) BgL_auxz00_3418);
																}
																BgL_arg1473z00_2825 =
																	(((BgL_tclassz00_bglt)
																		COBJECT(BgL_auxz00_3417))->BgL_holderz00);
															}
															BgL_arg1437z00_1839 =
																(((BgL_globalz00_bglt)
																	COBJECT(BgL_arg1473z00_2825))->BgL_importz00);
														}
														BgL_val0_1261z00_1837 =
															MAKE_YOUNG_PAIR(BgL_arg1437z00_1839,
															BgL_pz00_1834);
													}
													{	/* Object/classgen.scm 129 */
														int BgL_tmpz00_3427;

														BgL_tmpz00_3427 = (int) (2L);
														BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3427);
													}
													{	/* Object/classgen.scm 129 */
														int BgL_tmpz00_3430;

														BgL_tmpz00_3430 = (int) (1L);
														BGL_MVALUES_VAL_SET(BgL_tmpz00_3430, BgL_dz00_1835);
													}
													return BgL_val0_1261z00_1837;
												}
											else
												{	/* Object/classgen.scm 130 */
													obj_t BgL_makezd2pzd2_1840;

													BgL_makezd2pzd2_1840 =
														BGl_classgenzd2makezd2zzobject_classgenz00
														(BgL_cz00_4);
													{	/* Object/classgen.scm 131 */
														obj_t BgL_makezd2dzd2_1841;

														{	/* Object/classgen.scm 132 */
															obj_t BgL_tmpz00_2829;

															{	/* Object/classgen.scm 132 */
																int BgL_tmpz00_3434;

																BgL_tmpz00_3434 = (int) (1L);
																BgL_tmpz00_2829 =
																	BGL_MVALUES_VAL(BgL_tmpz00_3434);
															}
															{	/* Object/classgen.scm 132 */
																int BgL_tmpz00_3437;

																BgL_tmpz00_3437 = (int) (1L);
																BGL_MVALUES_VAL_SET(BgL_tmpz00_3437, BUNSPEC);
															}
															BgL_makezd2dzd2_1841 = BgL_tmpz00_2829;
														}
														{	/* Object/classgen.scm 132 */
															obj_t BgL_val0_1263z00_1842;
															obj_t BgL_val1_1264z00_1843;

															{	/* Object/classgen.scm 132 */
																obj_t BgL_arg1448z00_1844;
																obj_t BgL_arg1453z00_1845;

																{	/* Object/classgen.scm 139 */
																	BgL_globalz00_bglt BgL_arg1473z00_2830;

																	{
																		BgL_tclassz00_bglt BgL_auxz00_3440;

																		{
																			obj_t BgL_auxz00_3441;

																			{	/* Object/classgen.scm 139 */
																				BgL_objectz00_bglt BgL_tmpz00_3442;

																				BgL_tmpz00_3442 =
																					((BgL_objectz00_bglt)
																					((BgL_typez00_bglt) BgL_cz00_4));
																				BgL_auxz00_3441 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_3442);
																			}
																			BgL_auxz00_3440 =
																				((BgL_tclassz00_bglt) BgL_auxz00_3441);
																		}
																		BgL_arg1473z00_2830 =
																			(((BgL_tclassz00_bglt)
																				COBJECT(BgL_auxz00_3440))->
																			BgL_holderz00);
																	}
																	BgL_arg1448z00_1844 =
																		(((BgL_globalz00_bglt)
																			COBJECT(BgL_arg1473z00_2830))->
																		BgL_importz00);
																}
																BgL_arg1453z00_1845 =
																	MAKE_YOUNG_PAIR(BgL_makezd2pzd2_1840,
																	BgL_pz00_1834);
																BgL_val0_1263z00_1842 =
																	MAKE_YOUNG_PAIR(BgL_arg1448z00_1844,
																	BgL_arg1453z00_1845);
															}
															BgL_val1_1264z00_1843 =
																MAKE_YOUNG_PAIR(BgL_makezd2dzd2_1841,
																BgL_dz00_1835);
															{	/* Object/classgen.scm 132 */
																int BgL_tmpz00_3452;

																BgL_tmpz00_3452 = (int) (2L);
																BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3452);
															}
															{	/* Object/classgen.scm 132 */
																int BgL_tmpz00_3455;

																BgL_tmpz00_3455 = (int) (1L);
																BGL_MVALUES_VAL_SET(BgL_tmpz00_3455,
																	BgL_val1_1264z00_1843);
															}
															return BgL_val0_1263z00_1842;
														}
													}
												}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* classgen-predicate */
	obj_t BGl_classgenzd2predicatezd2zzobject_classgenz00(obj_t BgL_cz00_6)
	{
		{	/* Object/classgen.scm 144 */
			{
				obj_t BgL_idz00_1861;
				BgL_globalz00_bglt BgL_gz00_1862;

				{	/* Object/classgen.scm 150 */
					BgL_globalz00_bglt BgL_holderz00_1850;

					{
						BgL_tclassz00_bglt BgL_auxz00_3458;

						{
							obj_t BgL_auxz00_3459;

							{	/* Object/classgen.scm 150 */
								BgL_objectz00_bglt BgL_tmpz00_3460;

								BgL_tmpz00_3460 =
									((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_cz00_6));
								BgL_auxz00_3459 = BGL_OBJECT_WIDENING(BgL_tmpz00_3460);
							}
							BgL_auxz00_3458 = ((BgL_tclassz00_bglt) BgL_auxz00_3459);
						}
						BgL_holderz00_1850 =
							(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3458))->BgL_holderz00);
					}
					{	/* Object/classgen.scm 150 */
						obj_t BgL_idz00_1851;

						{	/* Object/classgen.scm 151 */
							obj_t BgL_arg1502z00_1857;

							{	/* Object/classgen.scm 151 */
								obj_t BgL_arg1509z00_1858;
								obj_t BgL_arg1513z00_1859;

								{	/* Object/classgen.scm 151 */
									obj_t BgL_arg1514z00_1860;

									BgL_arg1514z00_1860 =
										(((BgL_typez00_bglt) COBJECT(
												((BgL_typez00_bglt)
													((BgL_typez00_bglt) BgL_cz00_6))))->BgL_idz00);
									{	/* Object/classgen.scm 151 */
										obj_t BgL_arg1455z00_2844;

										BgL_arg1455z00_2844 = SYMBOL_TO_STRING(BgL_arg1514z00_1860);
										BgL_arg1509z00_1858 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_2844);
									}
								}
								{	/* Object/classgen.scm 151 */
									obj_t BgL_symbolz00_2845;

									BgL_symbolz00_2845 = CNST_TABLE_REF(8);
									{	/* Object/classgen.scm 151 */
										obj_t BgL_arg1455z00_2846;

										BgL_arg1455z00_2846 = SYMBOL_TO_STRING(BgL_symbolz00_2845);
										BgL_arg1513z00_1859 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_2846);
									}
								}
								BgL_arg1502z00_1857 =
									string_append(BgL_arg1509z00_1858, BgL_arg1513z00_1859);
							}
							BgL_idz00_1851 = bstring_to_symbol(BgL_arg1502z00_1857);
						}
						{	/* Object/classgen.scm 151 */
							obj_t BgL_tidz00_1852;

							BgL_tidz00_1852 =
								BGl_makezd2typedzd2identz00zzast_identz00(BgL_idz00_1851,
								CNST_TABLE_REF(9));
							{	/* Object/classgen.scm 152 */

								{	/* Object/classgen.scm 153 */
									obj_t BgL_val0_1265z00_1853;
									obj_t BgL_val1_1266z00_1854;

									{	/* Object/classgen.scm 154 */
										obj_t BgL_arg1485z00_1855;

										{	/* Object/classgen.scm 154 */
											obj_t BgL_arg1489z00_1856;

											BgL_arg1489z00_1856 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(10), BNIL);
											BgL_arg1485z00_1855 =
												MAKE_YOUNG_PAIR(BgL_tidz00_1852, BgL_arg1489z00_1856);
										}
										BgL_val0_1265z00_1853 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(11), BgL_arg1485z00_1855);
									}
									BgL_idz00_1861 = BgL_tidz00_1852;
									BgL_gz00_1862 = BgL_holderz00_1850;
									{	/* Object/classgen.scm 147 */
										obj_t BgL_arg1516z00_1864;

										{	/* Object/classgen.scm 147 */
											obj_t BgL_arg1535z00_1865;
											obj_t BgL_arg1540z00_1866;

											{	/* Object/classgen.scm 147 */
												obj_t BgL_arg1544z00_1867;

												BgL_arg1544z00_1867 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(2), BNIL);
												BgL_arg1535z00_1865 =
													MAKE_YOUNG_PAIR(BgL_idz00_1861, BgL_arg1544z00_1867);
											}
											{	/* Object/classgen.scm 148 */
												obj_t BgL_arg1546z00_1868;

												{	/* Object/classgen.scm 148 */
													obj_t BgL_arg1552z00_1869;
													obj_t BgL_arg1553z00_1870;

													{	/* Object/classgen.scm 148 */
														obj_t BgL_arg1559z00_1871;

														{	/* Object/classgen.scm 148 */
															obj_t BgL_arg1561z00_1872;

															BgL_arg1561z00_1872 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
															BgL_arg1559z00_1871 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(4),
																BgL_arg1561z00_1872);
														}
														BgL_arg1552z00_1869 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
															BgL_arg1559z00_1871);
													}
													{	/* Object/classgen.scm 148 */
														obj_t BgL_arg1564z00_1873;

														{	/* Object/classgen.scm 148 */
															obj_t BgL_arg1565z00_1874;

															{	/* Object/classgen.scm 148 */
																obj_t BgL_arg1571z00_1875;

																{	/* Object/classgen.scm 148 */
																	obj_t BgL_arg1573z00_1876;
																	obj_t BgL_arg1575z00_1877;

																	BgL_arg1573z00_1876 =
																		(((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					BgL_gz00_1862)))->BgL_idz00);
																	BgL_arg1575z00_1877 =
																		MAKE_YOUNG_PAIR((((BgL_globalz00_bglt)
																				COBJECT(BgL_gz00_1862))->BgL_modulez00),
																		BNIL);
																	BgL_arg1571z00_1875 =
																		MAKE_YOUNG_PAIR(BgL_arg1573z00_1876,
																		BgL_arg1575z00_1877);
																}
																BgL_arg1565z00_1874 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
																	BgL_arg1571z00_1875);
															}
															BgL_arg1564z00_1873 =
																MAKE_YOUNG_PAIR(BgL_arg1565z00_1874, BNIL);
														}
														BgL_arg1553z00_1870 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
															BgL_arg1564z00_1873);
													}
													BgL_arg1546z00_1868 =
														MAKE_YOUNG_PAIR(BgL_arg1552z00_1869,
														BgL_arg1553z00_1870);
												}
												BgL_arg1540z00_1866 =
													MAKE_YOUNG_PAIR(BgL_arg1546z00_1868, BNIL);
											}
											BgL_arg1516z00_1864 =
												MAKE_YOUNG_PAIR(BgL_arg1535z00_1865,
												BgL_arg1540z00_1866);
										}
										BgL_val1_1266z00_1854 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(7), BgL_arg1516z00_1864);
									}
									{	/* Object/classgen.scm 153 */
										int BgL_tmpz00_3507;

										BgL_tmpz00_3507 = (int) (2L);
										BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3507);
									}
									{	/* Object/classgen.scm 153 */
										int BgL_tmpz00_3510;

										BgL_tmpz00_3510 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_3510, BgL_val1_1266z00_1854);
									}
									return BgL_val0_1265z00_1853;
								}
							}
						}
					}
				}
			}
		}

	}



/* classgen-predicate-anonymous */
	BGL_EXPORTED_DEF obj_t
		BGl_classgenzd2predicatezd2anonymousz00zzobject_classgenz00(BgL_typez00_bglt
		BgL_cz00_7)
	{
		{	/* Object/classgen.scm 160 */
			{	/* Object/classgen.scm 161 */
				obj_t BgL_protoz00_1880;

				BgL_protoz00_1880 =
					BGl_classgenzd2predicatezd2zzobject_classgenz00(((obj_t) BgL_cz00_7));
				{	/* Object/classgen.scm 162 */
					obj_t BgL_defz00_1881;

					{	/* Object/classgen.scm 163 */
						obj_t BgL_tmpz00_2848;

						{	/* Object/classgen.scm 163 */
							int BgL_tmpz00_3515;

							BgL_tmpz00_3515 = (int) (1L);
							BgL_tmpz00_2848 = BGL_MVALUES_VAL(BgL_tmpz00_3515);
						}
						{	/* Object/classgen.scm 163 */
							int BgL_tmpz00_3518;

							BgL_tmpz00_3518 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3518, BUNSPEC);
						}
						BgL_defz00_1881 = BgL_tmpz00_2848;
					}
					if (PAIRP(BgL_defz00_1881))
						{	/* Object/classgen.scm 163 */
							obj_t BgL_cdrzd2144zd2_1888;

							BgL_cdrzd2144zd2_1888 = CDR(((obj_t) BgL_defz00_1881));
							if (PAIRP(BgL_cdrzd2144zd2_1888))
								{	/* Object/classgen.scm 163 */
									obj_t BgL_carzd2148zd2_1890;
									obj_t BgL_cdrzd2149zd2_1891;

									BgL_carzd2148zd2_1890 = CAR(BgL_cdrzd2144zd2_1888);
									BgL_cdrzd2149zd2_1891 = CDR(BgL_cdrzd2144zd2_1888);
									if (PAIRP(BgL_carzd2148zd2_1890))
										{	/* Object/classgen.scm 163 */
											if (PAIRP(BgL_cdrzd2149zd2_1891))
												{	/* Object/classgen.scm 163 */
													if (NULLP(CDR(BgL_cdrzd2149zd2_1891)))
														{	/* Object/classgen.scm 163 */
															obj_t BgL_arg1591z00_1897;
															obj_t BgL_arg1593z00_1898;

															BgL_arg1591z00_1897 = CDR(BgL_carzd2148zd2_1890);
															BgL_arg1593z00_1898 = CAR(BgL_cdrzd2149zd2_1891);
															{	/* Object/classgen.scm 165 */
																obj_t BgL_arg1595z00_2859;
																obj_t BgL_arg1602z00_2860;

																BgL_arg1595z00_2859 =
																	BGl_makezd2typedzd2identz00zzast_identz00
																	(CNST_TABLE_REF(12), CNST_TABLE_REF(9));
																{	/* Object/classgen.scm 165 */
																	obj_t BgL_arg1605z00_2861;

																	BgL_arg1605z00_2861 =
																		MAKE_YOUNG_PAIR(BgL_arg1593z00_1898, BNIL);
																	BgL_arg1602z00_2860 =
																		MAKE_YOUNG_PAIR(BgL_arg1591z00_1897,
																		BgL_arg1605z00_2861);
																}
																return
																	MAKE_YOUNG_PAIR(BgL_arg1595z00_2859,
																	BgL_arg1602z00_2860);
															}
														}
													else
														{	/* Object/classgen.scm 163 */
															return BFALSE;
														}
												}
											else
												{	/* Object/classgen.scm 163 */
													return BFALSE;
												}
										}
									else
										{	/* Object/classgen.scm 163 */
											return BFALSE;
										}
								}
							else
								{	/* Object/classgen.scm 163 */
									return BFALSE;
								}
						}
					else
						{	/* Object/classgen.scm 163 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &classgen-predicate-anonymous */
	obj_t BGl_z62classgenzd2predicatezd2anonymousz62zzobject_classgenz00(obj_t
		BgL_envz00_3092, obj_t BgL_cz00_3093)
	{
		{	/* Object/classgen.scm 160 */
			return
				BGl_classgenzd2predicatezd2anonymousz00zzobject_classgenz00(
				((BgL_typez00_bglt) BgL_cz00_3093));
		}

	}



/* classgen-make */
	obj_t BGl_classgenzd2makezd2zzobject_classgenz00(obj_t BgL_cz00_8)
	{
		{	/* Object/classgen.scm 170 */
			{
				obj_t BgL_idz00_1917;
				obj_t BgL_tidz00_1918;
				obj_t BgL_slotsz00_1919;
				obj_t BgL_formalsz00_1920;
				obj_t BgL_tformalsz00_1921;

				{	/* Object/classgen.scm 180 */
					obj_t BgL_tidz00_1904;

					BgL_tidz00_1904 =
						(((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_cz00_8)))->BgL_idz00);
					{	/* Object/classgen.scm 180 */
						obj_t BgL_slotsz00_1905;

						{
							BgL_tclassz00_bglt BgL_auxz00_3548;

							{
								obj_t BgL_auxz00_3549;

								{	/* Object/classgen.scm 181 */
									BgL_objectz00_bglt BgL_tmpz00_3550;

									BgL_tmpz00_3550 =
										((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_cz00_8));
									BgL_auxz00_3549 = BGL_OBJECT_WIDENING(BgL_tmpz00_3550);
								}
								BgL_auxz00_3548 = ((BgL_tclassz00_bglt) BgL_auxz00_3549);
							}
							BgL_slotsz00_1905 =
								(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3548))->BgL_slotsz00);
						}
						{	/* Object/classgen.scm 181 */
							obj_t BgL_idz00_1906;

							{	/* Object/classgen.scm 182 */
								obj_t BgL_arg1611z00_1914;

								{	/* Object/classgen.scm 182 */
									obj_t BgL_arg1613z00_1915;
									obj_t BgL_arg1615z00_1916;

									{	/* Object/classgen.scm 182 */
										obj_t BgL_symbolz00_2879;

										BgL_symbolz00_2879 = CNST_TABLE_REF(14);
										{	/* Object/classgen.scm 182 */
											obj_t BgL_arg1455z00_2880;

											BgL_arg1455z00_2880 =
												SYMBOL_TO_STRING(BgL_symbolz00_2879);
											BgL_arg1613z00_1915 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_2880);
										}
									}
									{	/* Object/classgen.scm 182 */
										obj_t BgL_arg1455z00_2882;

										BgL_arg1455z00_2882 = SYMBOL_TO_STRING(BgL_tidz00_1904);
										BgL_arg1615z00_1916 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_2882);
									}
									BgL_arg1611z00_1914 =
										string_append(BgL_arg1613z00_1915, BgL_arg1615z00_1916);
								}
								BgL_idz00_1906 = bstring_to_symbol(BgL_arg1611z00_1914);
							}
							{	/* Object/classgen.scm 182 */
								obj_t BgL_mkzd2tidzd2_1907;

								BgL_mkzd2tidzd2_1907 =
									BGl_makezd2typedzd2identz00zzast_identz00(BgL_idz00_1906,
									BgL_tidz00_1904);
								{	/* Object/classgen.scm 183 */
									obj_t BgL_fzd2idszd2_1908;

									BgL_fzd2idszd2_1908 =
										BGl_makezd2classzd2makezd2formalszd2zzobject_slotsz00
										(BgL_slotsz00_1905);
									{	/* Object/classgen.scm 184 */
										obj_t BgL_fzd2tidszd2_1909;

										BgL_fzd2tidszd2_1909 =
											BGl_makezd2classzd2makezd2typedzd2formalsz00zzobject_slotsz00
											(BgL_fzd2idszd2_1908, BgL_slotsz00_1905);
										{	/* Object/classgen.scm 185 */

											{	/* Object/classgen.scm 186 */
												obj_t BgL_val0_1278z00_1910;
												obj_t BgL_val1_1279z00_1911;

												{	/* Object/classgen.scm 187 */
													obj_t BgL_arg1606z00_1912;

													BgL_arg1606z00_1912 =
														MAKE_YOUNG_PAIR(BgL_mkzd2tidzd2_1907,
														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
														(BgL_fzd2tidszd2_1909, BNIL));
													BgL_val0_1278z00_1910 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
														BgL_arg1606z00_1912);
												}
												BgL_idz00_1917 = BgL_mkzd2tidzd2_1907;
												BgL_tidz00_1918 = BgL_tidz00_1904;
												BgL_slotsz00_1919 = BgL_slotsz00_1905;
												BgL_formalsz00_1920 = BgL_fzd2idszd2_1908;
												BgL_tformalsz00_1921 = BgL_fzd2tidszd2_1909;
												{	/* Object/classgen.scm 173 */
													obj_t BgL_plainzd2slotszd2_1923;

													{	/* Object/classgen.scm 173 */
														obj_t BgL_hook1271z00_1952;

														BgL_hook1271z00_1952 =
															MAKE_YOUNG_PAIR(BFALSE, BNIL);
														{
															obj_t BgL_l1268z00_1954;
															obj_t BgL_h1269z00_1955;

															BgL_l1268z00_1954 = BgL_slotsz00_1919;
															BgL_h1269z00_1955 = BgL_hook1271z00_1952;
														BgL_zc3z04anonymousza31682ze3z87_1956:
															if (NULLP(BgL_l1268z00_1954))
																{	/* Object/classgen.scm 173 */
																	BgL_plainzd2slotszd2_1923 =
																		CDR(BgL_hook1271z00_1952);
																}
															else
																{	/* Object/classgen.scm 173 */
																	bool_t BgL_test2333z00_3574;

																	if (BGl_slotzd2virtualzf3z21zzobject_slotsz00(
																			((BgL_slotz00_bglt)
																				CAR(((obj_t) BgL_l1268z00_1954)))))
																		{	/* Object/classgen.scm 173 */
																			BgL_test2333z00_3574 = ((bool_t) 0);
																		}
																	else
																		{	/* Object/classgen.scm 173 */
																			BgL_test2333z00_3574 = ((bool_t) 1);
																		}
																	if (BgL_test2333z00_3574)
																		{	/* Object/classgen.scm 173 */
																			obj_t BgL_nh1270z00_1961;

																			{	/* Object/classgen.scm 173 */
																				obj_t BgL_arg1689z00_1963;

																				BgL_arg1689z00_1963 =
																					CAR(((obj_t) BgL_l1268z00_1954));
																				BgL_nh1270z00_1961 =
																					MAKE_YOUNG_PAIR(BgL_arg1689z00_1963,
																					BNIL);
																			}
																			SET_CDR(BgL_h1269z00_1955,
																				BgL_nh1270z00_1961);
																			{	/* Object/classgen.scm 173 */
																				obj_t BgL_arg1688z00_1962;

																				BgL_arg1688z00_1962 =
																					CDR(((obj_t) BgL_l1268z00_1954));
																				{
																					obj_t BgL_h1269z00_3587;
																					obj_t BgL_l1268z00_3586;

																					BgL_l1268z00_3586 =
																						BgL_arg1688z00_1962;
																					BgL_h1269z00_3587 =
																						BgL_nh1270z00_1961;
																					BgL_h1269z00_1955 = BgL_h1269z00_3587;
																					BgL_l1268z00_1954 = BgL_l1268z00_3586;
																					goto
																						BgL_zc3z04anonymousza31682ze3z87_1956;
																				}
																			}
																		}
																	else
																		{	/* Object/classgen.scm 173 */
																			obj_t BgL_arg1691z00_1964;

																			BgL_arg1691z00_1964 =
																				CDR(((obj_t) BgL_l1268z00_1954));
																			{
																				obj_t BgL_l1268z00_3590;

																				BgL_l1268z00_3590 = BgL_arg1691z00_1964;
																				BgL_l1268z00_1954 = BgL_l1268z00_3590;
																				goto
																					BgL_zc3z04anonymousza31682ze3z87_1956;
																			}
																		}
																}
														}
													}
													{	/* Object/classgen.scm 174 */
														obj_t BgL_arg1625z00_1924;

														{	/* Object/classgen.scm 174 */
															obj_t BgL_arg1626z00_1925;
															obj_t BgL_arg1627z00_1926;

															BgL_arg1626z00_1925 =
																MAKE_YOUNG_PAIR(BgL_idz00_1917,
																BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																(BgL_tformalsz00_1921, BNIL));
															{	/* Object/classgen.scm 175 */
																obj_t BgL_arg1630z00_1928;

																{	/* Object/classgen.scm 175 */
																	obj_t BgL_arg1642z00_1929;
																	obj_t BgL_arg1646z00_1930;

																	BgL_arg1642z00_1929 =
																		BGl_makezd2typedzd2identz00zzast_identz00
																		(CNST_TABLE_REF(13), BgL_tidz00_1918);
																	{	/* Object/classgen.scm 176 */
																		obj_t BgL_arg1650z00_1931;

																		if (NULLP(BgL_plainzd2slotszd2_1923))
																			{	/* Object/classgen.scm 176 */
																				BgL_arg1650z00_1931 = BNIL;
																			}
																		else
																			{	/* Object/classgen.scm 176 */
																				obj_t BgL_head1274z00_1935;

																				BgL_head1274z00_1935 =
																					MAKE_YOUNG_PAIR(BNIL, BNIL);
																				{
																					obj_t BgL_ll1272z00_1937;
																					obj_t BgL_ll1273z00_1938;
																					obj_t BgL_tail1275z00_1939;

																					BgL_ll1272z00_1937 =
																						BgL_plainzd2slotszd2_1923;
																					BgL_ll1273z00_1938 =
																						BgL_formalsz00_1920;
																					BgL_tail1275z00_1939 =
																						BgL_head1274z00_1935;
																				BgL_zc3z04anonymousza31653ze3z87_1940:
																					if (NULLP(BgL_ll1272z00_1937))
																						{	/* Object/classgen.scm 176 */
																							BgL_arg1650z00_1931 =
																								CDR(BgL_head1274z00_1935);
																						}
																					else
																						{	/* Object/classgen.scm 176 */
																							obj_t BgL_newtail1276z00_1942;

																							{	/* Object/classgen.scm 176 */
																								obj_t BgL_arg1675z00_1945;

																								{	/* Object/classgen.scm 176 */
																									obj_t BgL_sz00_1946;
																									obj_t BgL_fz00_1947;

																									BgL_sz00_1946 =
																										CAR(
																										((obj_t)
																											BgL_ll1272z00_1937));
																									BgL_fz00_1947 =
																										CAR(((obj_t)
																											BgL_ll1273z00_1938));
																									{	/* Object/classgen.scm 177 */
																										obj_t BgL_arg1678z00_1948;

																										BgL_arg1678z00_1948 =
																											(((BgL_slotz00_bglt)
																												COBJECT((
																														(BgL_slotz00_bglt)
																														BgL_sz00_1946)))->
																											BgL_idz00);
																										{	/* Object/classgen.scm 177 */
																											obj_t
																												BgL_list1679z00_1949;
																											{	/* Object/classgen.scm 177 */
																												obj_t
																													BgL_arg1681z00_1950;
																												BgL_arg1681z00_1950 =
																													MAKE_YOUNG_PAIR
																													(BgL_fz00_1947, BNIL);
																												BgL_list1679z00_1949 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1678z00_1948,
																													BgL_arg1681z00_1950);
																											}
																											BgL_arg1675z00_1945 =
																												BgL_list1679z00_1949;
																										}
																									}
																								}
																								BgL_newtail1276z00_1942 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1675z00_1945, BNIL);
																							}
																							SET_CDR(BgL_tail1275z00_1939,
																								BgL_newtail1276z00_1942);
																							{	/* Object/classgen.scm 176 */
																								obj_t BgL_arg1661z00_1943;
																								obj_t BgL_arg1663z00_1944;

																								BgL_arg1661z00_1943 =
																									CDR(
																									((obj_t) BgL_ll1272z00_1937));
																								BgL_arg1663z00_1944 =
																									CDR(
																									((obj_t) BgL_ll1273z00_1938));
																								{
																									obj_t BgL_tail1275z00_3617;
																									obj_t BgL_ll1273z00_3616;
																									obj_t BgL_ll1272z00_3615;

																									BgL_ll1272z00_3615 =
																										BgL_arg1661z00_1943;
																									BgL_ll1273z00_3616 =
																										BgL_arg1663z00_1944;
																									BgL_tail1275z00_3617 =
																										BgL_newtail1276z00_1942;
																									BgL_tail1275z00_1939 =
																										BgL_tail1275z00_3617;
																									BgL_ll1273z00_1938 =
																										BgL_ll1273z00_3616;
																									BgL_ll1272z00_1937 =
																										BgL_ll1272z00_3615;
																									goto
																										BgL_zc3z04anonymousza31653ze3z87_1940;
																								}
																							}
																						}
																				}
																			}
																		BgL_arg1646z00_1930 =
																			BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																			(BgL_arg1650z00_1931, BNIL);
																	}
																	BgL_arg1630z00_1928 =
																		MAKE_YOUNG_PAIR(BgL_arg1642z00_1929,
																		BgL_arg1646z00_1930);
																}
																BgL_arg1627z00_1926 =
																	MAKE_YOUNG_PAIR(BgL_arg1630z00_1928, BNIL);
															}
															BgL_arg1625z00_1924 =
																MAKE_YOUNG_PAIR(BgL_arg1626z00_1925,
																BgL_arg1627z00_1926);
														}
														BgL_val1_1279z00_1911 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(7),
															BgL_arg1625z00_1924);
													}
												}
												{	/* Object/classgen.scm 186 */
													int BgL_tmpz00_3624;

													BgL_tmpz00_3624 = (int) (2L);
													BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3624);
												}
												{	/* Object/classgen.scm 186 */
													int BgL_tmpz00_3627;

													BgL_tmpz00_3627 = (int) (1L);
													BGL_MVALUES_VAL_SET(BgL_tmpz00_3627,
														BgL_val1_1279z00_1911);
												}
												return BgL_val0_1278z00_1910;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* classgen-nil */
	obj_t BGl_classgenzd2nilzd2zzobject_classgenz00(obj_t BgL_cz00_9)
	{
		{	/* Object/classgen.scm 193 */
			{
				obj_t BgL_idz00_1980;
				obj_t BgL_tidz00_1981;
				obj_t BgL_slotsz00_1982;

				{	/* Object/classgen.scm 203 */
					obj_t BgL_tidz00_1970;

					BgL_tidz00_1970 =
						(((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_cz00_9)))->BgL_idz00);
					{	/* Object/classgen.scm 203 */
						obj_t BgL_slotsz00_1971;

						{
							BgL_tclassz00_bglt BgL_auxz00_3632;

							{
								obj_t BgL_auxz00_3633;

								{	/* Object/classgen.scm 204 */
									BgL_objectz00_bglt BgL_tmpz00_3634;

									BgL_tmpz00_3634 =
										((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_cz00_9));
									BgL_auxz00_3633 = BGL_OBJECT_WIDENING(BgL_tmpz00_3634);
								}
								BgL_auxz00_3632 = ((BgL_tclassz00_bglt) BgL_auxz00_3633);
							}
							BgL_slotsz00_1971 =
								(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3632))->BgL_slotsz00);
						}
						{	/* Object/classgen.scm 204 */
							obj_t BgL_idz00_1972;

							{	/* Object/classgen.scm 205 */
								obj_t BgL_arg1692z00_1976;

								{	/* Object/classgen.scm 205 */
									obj_t BgL_arg1699z00_1977;
									obj_t BgL_arg1700z00_1978;

									{	/* Object/classgen.scm 205 */
										obj_t BgL_arg1701z00_1979;

										BgL_arg1701z00_1979 =
											(((BgL_typez00_bglt) COBJECT(
													((BgL_typez00_bglt) BgL_cz00_9)))->BgL_idz00);
										{	/* Object/classgen.scm 205 */
											obj_t BgL_arg1455z00_2899;

											BgL_arg1455z00_2899 =
												SYMBOL_TO_STRING(BgL_arg1701z00_1979);
											BgL_arg1699z00_1977 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_2899);
										}
									}
									{	/* Object/classgen.scm 205 */
										obj_t BgL_symbolz00_2900;

										BgL_symbolz00_2900 = CNST_TABLE_REF(18);
										{	/* Object/classgen.scm 205 */
											obj_t BgL_arg1455z00_2901;

											BgL_arg1455z00_2901 =
												SYMBOL_TO_STRING(BgL_symbolz00_2900);
											BgL_arg1700z00_1978 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_2901);
										}
									}
									BgL_arg1692z00_1976 =
										string_append(BgL_arg1699z00_1977, BgL_arg1700z00_1978);
								}
								BgL_idz00_1972 = bstring_to_symbol(BgL_arg1692z00_1976);
							}
							{	/* Object/classgen.scm 205 */
								obj_t BgL_nilzd2tidzd2_1973;

								BgL_nilzd2tidzd2_1973 =
									BGl_makezd2typedzd2identz00zzast_identz00(BgL_idz00_1972,
									BgL_tidz00_1970);
								{	/* Object/classgen.scm 206 */

									{	/* Object/classgen.scm 207 */
										obj_t BgL_val0_1285z00_1974;
										obj_t BgL_val1_1286z00_1975;

										BgL_val0_1285z00_1974 =
											MAKE_YOUNG_PAIR(BgL_nilzd2tidzd2_1973, BNIL);
										BgL_idz00_1980 = BgL_nilzd2tidzd2_1973;
										BgL_tidz00_1981 = BgL_tidz00_1970;
										BgL_slotsz00_1982 = BgL_slotsz00_1971;
										{	/* Object/classgen.scm 196 */
											obj_t BgL_plainzd2slotszd2_1984;

											{	/* Object/classgen.scm 196 */
												obj_t BgL_hook1284z00_1998;

												BgL_hook1284z00_1998 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
												{
													obj_t BgL_l1281z00_2000;
													obj_t BgL_h1282z00_2001;

													BgL_l1281z00_2000 = BgL_slotsz00_1982;
													BgL_h1282z00_2001 = BgL_hook1284z00_1998;
												BgL_zc3z04anonymousza31721ze3z87_2002:
													if (NULLP(BgL_l1281z00_2000))
														{	/* Object/classgen.scm 196 */
															BgL_plainzd2slotszd2_1984 =
																CDR(BgL_hook1284z00_1998);
														}
													else
														{	/* Object/classgen.scm 196 */
															bool_t BgL_test2338z00_3655;

															if (BGl_slotzd2virtualzf3z21zzobject_slotsz00(
																	((BgL_slotz00_bglt)
																		CAR(((obj_t) BgL_l1281z00_2000)))))
																{	/* Object/classgen.scm 196 */
																	BgL_test2338z00_3655 = ((bool_t) 0);
																}
															else
																{	/* Object/classgen.scm 196 */
																	BgL_test2338z00_3655 = ((bool_t) 1);
																}
															if (BgL_test2338z00_3655)
																{	/* Object/classgen.scm 196 */
																	obj_t BgL_nh1283z00_2007;

																	{	/* Object/classgen.scm 196 */
																		obj_t BgL_arg1734z00_2009;

																		BgL_arg1734z00_2009 =
																			CAR(((obj_t) BgL_l1281z00_2000));
																		BgL_nh1283z00_2007 =
																			MAKE_YOUNG_PAIR(BgL_arg1734z00_2009,
																			BNIL);
																	}
																	SET_CDR(BgL_h1282z00_2001,
																		BgL_nh1283z00_2007);
																	{	/* Object/classgen.scm 196 */
																		obj_t BgL_arg1733z00_2008;

																		BgL_arg1733z00_2008 =
																			CDR(((obj_t) BgL_l1281z00_2000));
																		{
																			obj_t BgL_h1282z00_3668;
																			obj_t BgL_l1281z00_3667;

																			BgL_l1281z00_3667 = BgL_arg1733z00_2008;
																			BgL_h1282z00_3668 = BgL_nh1283z00_2007;
																			BgL_h1282z00_2001 = BgL_h1282z00_3668;
																			BgL_l1281z00_2000 = BgL_l1281z00_3667;
																			goto
																				BgL_zc3z04anonymousza31721ze3z87_2002;
																		}
																	}
																}
															else
																{	/* Object/classgen.scm 196 */
																	obj_t BgL_arg1735z00_2010;

																	BgL_arg1735z00_2010 =
																		CDR(((obj_t) BgL_l1281z00_2000));
																	{
																		obj_t BgL_l1281z00_3671;

																		BgL_l1281z00_3671 = BgL_arg1735z00_2010;
																		BgL_l1281z00_2000 = BgL_l1281z00_3671;
																		goto BgL_zc3z04anonymousza31721ze3z87_2002;
																	}
																}
														}
												}
											}
											{	/* Object/classgen.scm 196 */
												obj_t BgL_newz00_1985;

												BgL_newz00_1985 =
													BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF
													(15));
												{	/* Object/classgen.scm 197 */
													obj_t BgL_tnewz00_1986;

													BgL_tnewz00_1986 =
														BGl_makezd2typedzd2identz00zzast_identz00
														(BgL_newz00_1985, BgL_tidz00_1981);
													{	/* Object/classgen.scm 198 */
														BgL_globalz00_bglt BgL_holderz00_1987;

														{
															BgL_tclassz00_bglt BgL_auxz00_3675;

															{
																obj_t BgL_auxz00_3676;

																{	/* Object/classgen.scm 199 */
																	BgL_objectz00_bglt BgL_tmpz00_3677;

																	BgL_tmpz00_3677 =
																		((BgL_objectz00_bglt)
																		((BgL_typez00_bglt) BgL_cz00_9));
																	BgL_auxz00_3676 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_3677);
																}
																BgL_auxz00_3675 =
																	((BgL_tclassz00_bglt) BgL_auxz00_3676);
															}
															BgL_holderz00_1987 =
																(((BgL_tclassz00_bglt)
																	COBJECT(BgL_auxz00_3675))->BgL_holderz00);
														}
														{	/* Object/classgen.scm 199 */

															{	/* Object/classgen.scm 200 */
																obj_t BgL_arg1703z00_1988;

																{	/* Object/classgen.scm 200 */
																	obj_t BgL_arg1705z00_1989;
																	obj_t BgL_arg1708z00_1990;

																	BgL_arg1705z00_1989 =
																		MAKE_YOUNG_PAIR(BgL_idz00_1980, BNIL);
																	{	/* Object/classgen.scm 201 */
																		obj_t BgL_arg1709z00_1991;

																		{	/* Object/classgen.scm 201 */
																			obj_t BgL_arg1710z00_1992;

																			{	/* Object/classgen.scm 201 */
																				obj_t BgL_arg1711z00_1993;

																				{	/* Object/classgen.scm 201 */
																					obj_t BgL_arg1714z00_1994;

																					{	/* Object/classgen.scm 201 */
																						obj_t BgL_arg1717z00_1995;
																						obj_t BgL_arg1718z00_1996;

																						BgL_arg1717z00_1995 =
																							(((BgL_variablez00_bglt) COBJECT(
																									((BgL_variablez00_bglt)
																										BgL_holderz00_1987)))->
																							BgL_idz00);
																						BgL_arg1718z00_1996 =
																							MAKE_YOUNG_PAIR(((
																									(BgL_globalz00_bglt)
																									COBJECT(BgL_holderz00_1987))->
																								BgL_modulez00), BNIL);
																						BgL_arg1714z00_1994 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1717z00_1995,
																							BgL_arg1718z00_1996);
																					}
																					BgL_arg1711z00_1993 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
																						BgL_arg1714z00_1994);
																				}
																				BgL_arg1710z00_1992 =
																					MAKE_YOUNG_PAIR(BgL_arg1711z00_1993,
																					BNIL);
																			}
																			BgL_arg1709z00_1991 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(16),
																				BgL_arg1710z00_1992);
																		}
																		BgL_arg1708z00_1990 =
																			MAKE_YOUNG_PAIR(BgL_arg1709z00_1991,
																			BNIL);
																	}
																	BgL_arg1703z00_1988 =
																		MAKE_YOUNG_PAIR(BgL_arg1705z00_1989,
																		BgL_arg1708z00_1990);
																}
																BgL_val1_1286z00_1975 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(17),
																	BgL_arg1703z00_1988);
															}
														}
													}
												}
											}
										}
										{	/* Object/classgen.scm 207 */
											int BgL_tmpz00_3698;

											BgL_tmpz00_3698 = (int) (2L);
											BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3698);
										}
										{	/* Object/classgen.scm 207 */
											int BgL_tmpz00_3701;

											BgL_tmpz00_3701 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_3701,
												BgL_val1_1286z00_1975);
										}
										return BgL_val0_1285z00_1974;
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* classgen-nil-anonymous */
	BGL_EXPORTED_DEF obj_t
		BGl_classgenzd2nilzd2anonymousz00zzobject_classgenz00(BgL_typez00_bglt
		BgL_cz00_10)
	{
		{	/* Object/classgen.scm 214 */
			{	/* Object/classgen.scm 215 */
				obj_t BgL_tidz00_2015;

				BgL_tidz00_2015 =
					(((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_cz00_10)))->BgL_idz00);
				{	/* Object/classgen.scm 215 */
					obj_t BgL_slotsz00_2016;

					{
						BgL_tclassz00_bglt BgL_auxz00_3706;

						{
							obj_t BgL_auxz00_3707;

							{	/* Object/classgen.scm 216 */
								BgL_objectz00_bglt BgL_tmpz00_3708;

								BgL_tmpz00_3708 = ((BgL_objectz00_bglt) BgL_cz00_10);
								BgL_auxz00_3707 = BGL_OBJECT_WIDENING(BgL_tmpz00_3708);
							}
							BgL_auxz00_3706 = ((BgL_tclassz00_bglt) BgL_auxz00_3707);
						}
						BgL_slotsz00_2016 =
							(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3706))->BgL_slotsz00);
					}
					{	/* Object/classgen.scm 216 */
						obj_t BgL_idz00_2017;

						{	/* Object/classgen.scm 217 */
							obj_t BgL_arg1767z00_2061;

							{	/* Object/classgen.scm 217 */
								obj_t BgL_arg1770z00_2062;
								obj_t BgL_arg1771z00_2063;

								{	/* Object/classgen.scm 217 */
									obj_t BgL_arg1773z00_2064;

									BgL_arg1773z00_2064 =
										(((BgL_typez00_bglt) COBJECT(
												((BgL_typez00_bglt) BgL_cz00_10)))->BgL_idz00);
									{	/* Object/classgen.scm 217 */
										obj_t BgL_arg1455z00_2908;

										BgL_arg1455z00_2908 = SYMBOL_TO_STRING(BgL_arg1773z00_2064);
										BgL_arg1770z00_2062 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_2908);
									}
								}
								{	/* Object/classgen.scm 217 */
									obj_t BgL_symbolz00_2909;

									BgL_symbolz00_2909 = CNST_TABLE_REF(18);
									{	/* Object/classgen.scm 217 */
										obj_t BgL_arg1455z00_2910;

										BgL_arg1455z00_2910 = SYMBOL_TO_STRING(BgL_symbolz00_2909);
										BgL_arg1771z00_2063 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_2910);
									}
								}
								BgL_arg1767z00_2061 =
									string_append(BgL_arg1770z00_2062, BgL_arg1771z00_2063);
							}
							BgL_idz00_2017 = bstring_to_symbol(BgL_arg1767z00_2061);
						}
						{	/* Object/classgen.scm 217 */
							obj_t BgL_plainzd2slotszd2_2018;

							{	/* Object/classgen.scm 218 */
								obj_t BgL_hook1291z00_2045;

								BgL_hook1291z00_2045 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
								{
									obj_t BgL_l1288z00_2047;
									obj_t BgL_h1289z00_2048;

									BgL_l1288z00_2047 = BgL_slotsz00_2016;
									BgL_h1289z00_2048 = BgL_hook1291z00_2045;
								BgL_zc3z04anonymousza31754ze3z87_2049:
									if (NULLP(BgL_l1288z00_2047))
										{	/* Object/classgen.scm 218 */
											BgL_plainzd2slotszd2_2018 = CDR(BgL_hook1291z00_2045);
										}
									else
										{	/* Object/classgen.scm 218 */
											bool_t BgL_test2341z00_3726;

											if (BGl_slotzd2virtualzf3z21zzobject_slotsz00(
													((BgL_slotz00_bglt)
														CAR(((obj_t) BgL_l1288z00_2047)))))
												{	/* Object/classgen.scm 218 */
													BgL_test2341z00_3726 = ((bool_t) 0);
												}
											else
												{	/* Object/classgen.scm 218 */
													BgL_test2341z00_3726 = ((bool_t) 1);
												}
											if (BgL_test2341z00_3726)
												{	/* Object/classgen.scm 218 */
													obj_t BgL_nh1290z00_2054;

													{	/* Object/classgen.scm 218 */
														obj_t BgL_arg1762z00_2056;

														BgL_arg1762z00_2056 =
															CAR(((obj_t) BgL_l1288z00_2047));
														BgL_nh1290z00_2054 =
															MAKE_YOUNG_PAIR(BgL_arg1762z00_2056, BNIL);
													}
													SET_CDR(BgL_h1289z00_2048, BgL_nh1290z00_2054);
													{	/* Object/classgen.scm 218 */
														obj_t BgL_arg1761z00_2055;

														BgL_arg1761z00_2055 =
															CDR(((obj_t) BgL_l1288z00_2047));
														{
															obj_t BgL_h1289z00_3739;
															obj_t BgL_l1288z00_3738;

															BgL_l1288z00_3738 = BgL_arg1761z00_2055;
															BgL_h1289z00_3739 = BgL_nh1290z00_2054;
															BgL_h1289z00_2048 = BgL_h1289z00_3739;
															BgL_l1288z00_2047 = BgL_l1288z00_3738;
															goto BgL_zc3z04anonymousza31754ze3z87_2049;
														}
													}
												}
											else
												{	/* Object/classgen.scm 218 */
													obj_t BgL_arg1765z00_2057;

													BgL_arg1765z00_2057 =
														CDR(((obj_t) BgL_l1288z00_2047));
													{
														obj_t BgL_l1288z00_3742;

														BgL_l1288z00_3742 = BgL_arg1765z00_2057;
														BgL_l1288z00_2047 = BgL_l1288z00_3742;
														goto BgL_zc3z04anonymousza31754ze3z87_2049;
													}
												}
										}
								}
							}
							{	/* Object/classgen.scm 218 */
								obj_t BgL_newz00_2019;

								BgL_newz00_2019 =
									BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(15));
								{	/* Object/classgen.scm 219 */
									obj_t BgL_tnewz00_2020;

									BgL_tnewz00_2020 =
										BGl_makezd2typedzd2identz00zzast_identz00(BgL_newz00_2019,
										BgL_tidz00_2015);
									{	/* Object/classgen.scm 220 */

										{	/* Object/classgen.scm 221 */
											obj_t BgL_arg1736z00_2021;

											{	/* Object/classgen.scm 221 */
												obj_t BgL_arg1737z00_2022;
												obj_t BgL_arg1738z00_2023;

												BgL_arg1737z00_2022 =
													MAKE_YOUNG_PAIR(BgL_tnewz00_2020, BNIL);
												{	/* Object/classgen.scm 222 */
													obj_t BgL_arg1739z00_2024;
													obj_t BgL_arg1740z00_2025;

													if (NULLP(BgL_plainzd2slotszd2_2018))
														{	/* Object/classgen.scm 222 */
															BgL_arg1739z00_2024 = BNIL;
														}
													else
														{	/* Object/classgen.scm 222 */
															obj_t BgL_head1294z00_2028;

															BgL_head1294z00_2028 =
																MAKE_YOUNG_PAIR(BNIL, BNIL);
															{
																obj_t BgL_l1292z00_2030;
																obj_t BgL_tail1295z00_2031;

																BgL_l1292z00_2030 = BgL_plainzd2slotszd2_2018;
																BgL_tail1295z00_2031 = BgL_head1294z00_2028;
															BgL_zc3z04anonymousza31742ze3z87_2032:
																if (NULLP(BgL_l1292z00_2030))
																	{	/* Object/classgen.scm 222 */
																		BgL_arg1739z00_2024 =
																			CDR(BgL_head1294z00_2028);
																	}
																else
																	{	/* Object/classgen.scm 222 */
																		obj_t BgL_newtail1296z00_2034;

																		{	/* Object/classgen.scm 222 */
																			obj_t BgL_arg1747z00_2036;

																			{	/* Object/classgen.scm 222 */
																				obj_t BgL_sz00_2037;

																				BgL_sz00_2037 =
																					CAR(((obj_t) BgL_l1292z00_2030));
																				{	/* Object/classgen.scm 223 */
																					obj_t BgL_arg1748z00_2038;

																					{	/* Object/classgen.scm 223 */
																						obj_t BgL_arg1749z00_2039;
																						obj_t BgL_arg1750z00_2040;

																						{	/* Object/classgen.scm 223 */
																							obj_t BgL_arg1751z00_2041;

																							BgL_arg1751z00_2041 =
																								(((BgL_slotz00_bglt) COBJECT(
																										((BgL_slotz00_bglt)
																											BgL_sz00_2037)))->
																								BgL_idz00);
																							BgL_arg1749z00_2039 =
																								BGl_fieldzd2accesszd2zzast_objectz00
																								(BgL_newz00_2019,
																								BgL_arg1751z00_2041, BTRUE);
																						}
																						{	/* Object/classgen.scm 224 */
																							obj_t BgL_arg1752z00_2042;

																							{	/* Object/classgen.scm 224 */
																								obj_t BgL_arg1753z00_2043;

																								BgL_arg1753z00_2043 =
																									(((BgL_slotz00_bglt) COBJECT(
																											((BgL_slotz00_bglt)
																												BgL_sz00_2037)))->
																									BgL_typez00);
																								BgL_arg1752z00_2042 =
																									BGl_typezd2nilzd2valuez00zzobject_nilz00
																									(BgL_arg1753z00_2043,
																									BgL_sz00_2037);
																							}
																							BgL_arg1750z00_2040 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1752z00_2042, BNIL);
																						}
																						BgL_arg1748z00_2038 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1749z00_2039,
																							BgL_arg1750z00_2040);
																					}
																					BgL_arg1747z00_2036 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(19),
																						BgL_arg1748z00_2038);
																				}
																			}
																			BgL_newtail1296z00_2034 =
																				MAKE_YOUNG_PAIR(BgL_arg1747z00_2036,
																				BNIL);
																		}
																		SET_CDR(BgL_tail1295z00_2031,
																			BgL_newtail1296z00_2034);
																		{	/* Object/classgen.scm 222 */
																			obj_t BgL_arg1746z00_2035;

																			BgL_arg1746z00_2035 =
																				CDR(((obj_t) BgL_l1292z00_2030));
																			{
																				obj_t BgL_tail1295z00_3770;
																				obj_t BgL_l1292z00_3769;

																				BgL_l1292z00_3769 = BgL_arg1746z00_2035;
																				BgL_tail1295z00_3770 =
																					BgL_newtail1296z00_2034;
																				BgL_tail1295z00_2031 =
																					BgL_tail1295z00_3770;
																				BgL_l1292z00_2030 = BgL_l1292z00_3769;
																				goto
																					BgL_zc3z04anonymousza31742ze3z87_2032;
																			}
																		}
																	}
															}
														}
													BgL_arg1740z00_2025 =
														MAKE_YOUNG_PAIR(BgL_newz00_2019, BNIL);
													BgL_arg1738z00_2023 =
														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
														(BgL_arg1739z00_2024, BgL_arg1740z00_2025);
												}
												BgL_arg1736z00_2021 =
													MAKE_YOUNG_PAIR(BgL_arg1737z00_2022,
													BgL_arg1738z00_2023);
											}
											return
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(12),
												BgL_arg1736z00_2021);
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &classgen-nil-anonymous */
	obj_t BGl_z62classgenzd2nilzd2anonymousz62zzobject_classgenz00(obj_t
		BgL_envz00_3094, obj_t BgL_cz00_3095)
	{
		{	/* Object/classgen.scm 214 */
			return
				BGl_classgenzd2nilzd2anonymousz00zzobject_classgenz00(
				((BgL_typez00_bglt) BgL_cz00_3095));
		}

	}



/* classgen-make-anonymous */
	BGL_EXPORTED_DEF obj_t
		BGl_classgenzd2makezd2anonymousz00zzobject_classgenz00(BgL_typez00_bglt
		BgL_cz00_11)
	{
		{	/* Object/classgen.scm 231 */
			{	/* Object/classgen.scm 232 */
				bool_t BgL_test2345z00_3778;

				{
					BgL_tclassz00_bglt BgL_auxz00_3779;

					{
						obj_t BgL_auxz00_3780;

						{	/* Object/classgen.scm 232 */
							BgL_objectz00_bglt BgL_tmpz00_3781;

							BgL_tmpz00_3781 = ((BgL_objectz00_bglt) BgL_cz00_11);
							BgL_auxz00_3780 = BGL_OBJECT_WIDENING(BgL_tmpz00_3781);
						}
						BgL_auxz00_3779 = ((BgL_tclassz00_bglt) BgL_auxz00_3780);
					}
					BgL_test2345z00_3778 =
						(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3779))->
						BgL_abstractzf3zf3);
				}
				if (BgL_test2345z00_3778)
					{	/* Object/classgen.scm 234 */
						obj_t BgL_arg1775z00_2066;

						{	/* Object/classgen.scm 234 */
							obj_t BgL_arg1798z00_2067;

							{	/* Object/classgen.scm 234 */
								obj_t BgL_arg1799z00_2068;

								{	/* Object/classgen.scm 234 */
									obj_t BgL_arg1805z00_2069;

									{	/* Object/classgen.scm 234 */
										obj_t BgL_arg1806z00_2070;
										obj_t BgL_arg1808z00_2071;

										{	/* Object/classgen.scm 234 */
											obj_t BgL_arg1812z00_2072;

											BgL_arg1812z00_2072 =
												(((BgL_typez00_bglt) COBJECT(
														((BgL_typez00_bglt) BgL_cz00_11)))->BgL_idz00);
											{	/* Object/classgen.scm 234 */
												obj_t BgL_arg1455z00_2928;

												BgL_arg1455z00_2928 =
													SYMBOL_TO_STRING(BgL_arg1812z00_2072);
												BgL_arg1806z00_2070 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_2928);
											}
										}
										{	/* Object/classgen.scm 234 */
											obj_t BgL_arg1820z00_2073;

											BgL_arg1820z00_2073 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
											BgL_arg1808z00_2071 =
												MAKE_YOUNG_PAIR(BGl_string2264z00zzobject_classgenz00,
												BgL_arg1820z00_2073);
										}
										BgL_arg1805z00_2069 =
											MAKE_YOUNG_PAIR(BgL_arg1806z00_2070, BgL_arg1808z00_2071);
									}
									BgL_arg1799z00_2068 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(20), BgL_arg1805z00_2069);
								}
								BgL_arg1798z00_2067 =
									MAKE_YOUNG_PAIR(BgL_arg1799z00_2068, BNIL);
							}
							BgL_arg1775z00_2066 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(21), BgL_arg1798z00_2067);
						}
						return MAKE_YOUNG_PAIR(CNST_TABLE_REF(12), BgL_arg1775z00_2066);
					}
				else
					{	/* Object/classgen.scm 237 */
						obj_t BgL_protoz00_2074;

						BgL_protoz00_2074 =
							BGl_classgenzd2makezd2zzobject_classgenz00(((obj_t) BgL_cz00_11));
						{	/* Object/classgen.scm 238 */
							obj_t BgL_defz00_2075;

							{	/* Object/classgen.scm 239 */
								obj_t BgL_tmpz00_2929;

								{	/* Object/classgen.scm 239 */
									int BgL_tmpz00_3802;

									BgL_tmpz00_3802 = (int) (1L);
									BgL_tmpz00_2929 = BGL_MVALUES_VAL(BgL_tmpz00_3802);
								}
								{	/* Object/classgen.scm 239 */
									int BgL_tmpz00_3805;

									BgL_tmpz00_3805 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3805, BUNSPEC);
								}
								BgL_defz00_2075 = BgL_tmpz00_2929;
							}
							if (PAIRP(BgL_defz00_2075))
								{	/* Object/classgen.scm 239 */
									obj_t BgL_cdrzd2169zd2_2082;

									BgL_cdrzd2169zd2_2082 = CDR(((obj_t) BgL_defz00_2075));
									if (PAIRP(BgL_cdrzd2169zd2_2082))
										{	/* Object/classgen.scm 239 */
											obj_t BgL_carzd2173zd2_2084;
											obj_t BgL_cdrzd2174zd2_2085;

											BgL_carzd2173zd2_2084 = CAR(BgL_cdrzd2169zd2_2082);
											BgL_cdrzd2174zd2_2085 = CDR(BgL_cdrzd2169zd2_2082);
											if (PAIRP(BgL_carzd2173zd2_2084))
												{	/* Object/classgen.scm 239 */
													if (PAIRP(BgL_cdrzd2174zd2_2085))
														{	/* Object/classgen.scm 239 */
															if (NULLP(CDR(BgL_cdrzd2174zd2_2085)))
																{	/* Object/classgen.scm 239 */
																	obj_t BgL_arg1834z00_2091;
																	obj_t BgL_arg1835z00_2092;

																	BgL_arg1834z00_2091 =
																		CDR(BgL_carzd2173zd2_2084);
																	BgL_arg1835z00_2092 =
																		CAR(BgL_cdrzd2174zd2_2085);
																	{	/* Object/classgen.scm 241 */
																		obj_t BgL_arg1837z00_2942;
																		obj_t BgL_arg1838z00_2943;

																		{	/* Object/classgen.scm 241 */
																			obj_t BgL_arg1839z00_2944;

																			BgL_arg1839z00_2944 =
																				(((BgL_typez00_bglt) COBJECT(
																						((BgL_typez00_bglt) BgL_cz00_11)))->
																				BgL_idz00);
																			BgL_arg1837z00_2942 =
																				BGl_makezd2typedzd2identz00zzast_identz00
																				(CNST_TABLE_REF(12),
																				BgL_arg1839z00_2944);
																		}
																		{	/* Object/classgen.scm 241 */
																			obj_t BgL_arg1840z00_2945;

																			BgL_arg1840z00_2945 =
																				MAKE_YOUNG_PAIR(BgL_arg1835z00_2092,
																				BNIL);
																			BgL_arg1838z00_2943 =
																				MAKE_YOUNG_PAIR(BgL_arg1834z00_2091,
																				BgL_arg1840z00_2945);
																		}
																		return
																			MAKE_YOUNG_PAIR(BgL_arg1837z00_2942,
																			BgL_arg1838z00_2943);
																	}
																}
															else
																{	/* Object/classgen.scm 239 */
																	return BFALSE;
																}
														}
													else
														{	/* Object/classgen.scm 239 */
															return BFALSE;
														}
												}
											else
												{	/* Object/classgen.scm 239 */
													return BFALSE;
												}
										}
									else
										{	/* Object/classgen.scm 239 */
											return BFALSE;
										}
								}
							else
								{	/* Object/classgen.scm 239 */
									return BFALSE;
								}
						}
					}
			}
		}

	}



/* &classgen-make-anonymous */
	obj_t BGl_z62classgenzd2makezd2anonymousz62zzobject_classgenz00(obj_t
		BgL_envz00_3096, obj_t BgL_cz00_3097)
	{
		{	/* Object/classgen.scm 231 */
			return
				BGl_classgenzd2makezd2anonymousz00zzobject_classgenz00(
				((BgL_typez00_bglt) BgL_cz00_3097));
		}

	}



/* classgen-allocate */
	obj_t BGl_classgenzd2allocatezd2zzobject_classgenz00(BgL_typez00_bglt
		BgL_cz00_12)
	{
		{	/* Object/classgen.scm 248 */
			{
				obj_t BgL_idz00_2142;
				obj_t BgL_tidz00_2143;
				BgL_globalz00_bglt BgL_gz00_2144;
				obj_t BgL_idz00_2236;
				obj_t BgL_tidz00_2237;
				BgL_globalz00_bglt BgL_gz00_2238;

				{	/* Object/classgen.scm 309 */
					obj_t BgL_tidz00_2103;

					BgL_tidz00_2103 =
						(((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_cz00_12)))->BgL_idz00);
					{	/* Object/classgen.scm 309 */
						obj_t BgL_alloczd2idzd2_2104;

						{	/* Object/classgen.scm 310 */
							obj_t BgL_arg1846z00_2113;

							{	/* Object/classgen.scm 310 */
								obj_t BgL_arg1847z00_2114;
								obj_t BgL_arg1848z00_2115;

								{	/* Object/classgen.scm 310 */
									obj_t BgL_symbolz00_2967;

									BgL_symbolz00_2967 = CNST_TABLE_REF(29);
									{	/* Object/classgen.scm 310 */
										obj_t BgL_arg1455z00_2968;

										BgL_arg1455z00_2968 = SYMBOL_TO_STRING(BgL_symbolz00_2967);
										BgL_arg1847z00_2114 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_2968);
									}
								}
								{	/* Object/classgen.scm 310 */
									obj_t BgL_arg1455z00_2970;

									BgL_arg1455z00_2970 = SYMBOL_TO_STRING(BgL_tidz00_2103);
									BgL_arg1848z00_2115 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_2970);
								}
								BgL_arg1846z00_2113 =
									string_append(BgL_arg1847z00_2114, BgL_arg1848z00_2115);
							}
							BgL_alloczd2idzd2_2104 = bstring_to_symbol(BgL_arg1846z00_2113);
						}
						{	/* Object/classgen.scm 310 */
							obj_t BgL_alloczd2tidzd2_2105;

							BgL_alloczd2tidzd2_2105 =
								BGl_makezd2typedzd2identz00zzast_identz00
								(BgL_alloczd2idzd2_2104, BgL_tidz00_2103);
							{	/* Object/classgen.scm 311 */
								BgL_globalz00_bglt BgL_holderz00_2106;

								{
									BgL_tclassz00_bglt BgL_auxz00_3844;

									{
										obj_t BgL_auxz00_3845;

										{	/* Object/classgen.scm 312 */
											BgL_objectz00_bglt BgL_tmpz00_3846;

											BgL_tmpz00_3846 = ((BgL_objectz00_bglt) BgL_cz00_12);
											BgL_auxz00_3845 = BGL_OBJECT_WIDENING(BgL_tmpz00_3846);
										}
										BgL_auxz00_3844 = ((BgL_tclassz00_bglt) BgL_auxz00_3845);
									}
									BgL_holderz00_2106 =
										(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3844))->
										BgL_holderz00);
								}
								{	/* Object/classgen.scm 312 */

									{	/* Object/classgen.scm 313 */
										obj_t BgL_val0_1297z00_2107;
										obj_t BgL_val1_1298z00_2108;

										{	/* Object/classgen.scm 314 */
											obj_t BgL_arg1842z00_2109;

											BgL_arg1842z00_2109 =
												MAKE_YOUNG_PAIR(BgL_alloczd2tidzd2_2105, BNIL);
											BgL_val0_1297z00_2107 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
												BgL_arg1842z00_2109);
										}
										{	/* Object/classgen.scm 315 */
											bool_t BgL_test2351z00_3854;

											{	/* Object/classgen.scm 315 */
												obj_t BgL_arg1845z00_2112;

												BgL_arg1845z00_2112 =
													BGl_thezd2backendzd2zzbackend_backendz00();
												BgL_test2351z00_3854 =
													(((BgL_backendz00_bglt) COBJECT(
															((BgL_backendz00_bglt) BgL_arg1845z00_2112)))->
													BgL_pragmazd2supportzd2);
											}
											if (BgL_test2351z00_3854)
												{	/* Object/classgen.scm 315 */
													BgL_idz00_2142 = BgL_alloczd2tidzd2_2105;
													BgL_tidz00_2143 = BgL_tidz00_2103;
													BgL_gz00_2144 = BgL_holderz00_2106;
													{	/* Object/classgen.scm 271 */
														obj_t BgL_newz00_2146;

														BgL_newz00_2146 =
															BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
															(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF
																(15)));
														{	/* Object/classgen.scm 272 */
															obj_t BgL_arg1874z00_2147;

															{	/* Object/classgen.scm 272 */
																obj_t BgL_arg1875z00_2148;
																obj_t BgL_arg1876z00_2149;

																BgL_arg1875z00_2148 =
																	MAKE_YOUNG_PAIR(BgL_idz00_2142, BNIL);
																{	/* Object/classgen.scm 274 */
																	obj_t BgL_arg1877z00_2150;

																	{	/* Object/classgen.scm 274 */
																		obj_t BgL_arg1878z00_2151;

																		if (
																			((long)
																				CINT
																				(BGl_za2compilerzd2debugzd2traceza2z00zzengine_paramz00)
																				>= 1L))
																			{	/* Object/classgen.scm 275 */
																				obj_t BgL_envz00_2153;

																				BgL_envz00_2153 =
																					BGl_gensymz00zz__r4_symbols_6_4z00
																					(CNST_TABLE_REF(25));
																				{	/* Object/classgen.scm 275 */
																					obj_t BgL_tenvz00_2154;

																					BgL_tenvz00_2154 =
																						BGl_makezd2typedzd2identz00zzast_identz00
																						(BgL_envz00_2153,
																						CNST_TABLE_REF(26));
																					{	/* Object/classgen.scm 276 */
																						obj_t BgL_aidz00_2155;

																						BgL_aidz00_2155 =
																							BGl_gensymz00zz__r4_symbols_6_4z00
																							(CNST_TABLE_REF(27));
																						{	/* Object/classgen.scm 277 */

																							{	/* Object/classgen.scm 278 */
																								obj_t BgL_arg1880z00_2156;

																								{	/* Object/classgen.scm 278 */
																									obj_t BgL_arg1882z00_2157;
																									obj_t BgL_arg1883z00_2158;

																									{	/* Object/classgen.scm 278 */
																										obj_t BgL_arg1884z00_2159;
																										obj_t BgL_arg1885z00_2160;

																										{	/* Object/classgen.scm 278 */
																											obj_t BgL_arg1887z00_2161;

																											{	/* Object/classgen.scm 278 */
																												obj_t
																													BgL_arg1888z00_2162;
																												BgL_arg1888z00_2162 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(28),
																													BNIL);
																												BgL_arg1887z00_2161 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1888z00_2162,
																													BNIL);
																											}
																											BgL_arg1884z00_2159 =
																												MAKE_YOUNG_PAIR
																												(BgL_tenvz00_2154,
																												BgL_arg1887z00_2161);
																										}
																										{	/* Object/classgen.scm 279 */
																											obj_t BgL_arg1889z00_2163;

																											{	/* Object/classgen.scm 279 */
																												obj_t
																													BgL_arg1890z00_2164;
																												{	/* Object/classgen.scm 279 */
																													obj_t
																														BgL_arg1891z00_2165;
																													{	/* Object/classgen.scm 279 */
																														obj_t
																															BgL_arg1892z00_2166;
																														{	/* Object/classgen.scm 279 */
																															obj_t
																																BgL_arg1893z00_2167;
																															{	/* Object/classgen.scm 279 */
																																obj_t
																																	BgL_arg1894z00_2168;
																																{	/* Object/classgen.scm 279 */
																																	obj_t
																																		BgL_arg1896z00_2169;
																																	obj_t
																																		BgL_arg1897z00_2170;
																																	{	/* Object/classgen.scm 279 */
																																		obj_t
																																			BgL_symbolz00_2953;
																																		BgL_symbolz00_2953
																																			=
																																			CNST_TABLE_REF
																																			(29);
																																		{	/* Object/classgen.scm 279 */
																																			obj_t
																																				BgL_arg1455z00_2954;
																																			BgL_arg1455z00_2954
																																				=
																																				SYMBOL_TO_STRING
																																				(BgL_symbolz00_2953);
																																			BgL_arg1896z00_2169
																																				=
																																				BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																				(BgL_arg1455z00_2954);
																																	}}
																																	{	/* Object/classgen.scm 279 */
																																		obj_t
																																			BgL_arg1455z00_2956;
																																		BgL_arg1455z00_2956
																																			=
																																			SYMBOL_TO_STRING
																																			(BgL_tidz00_2143);
																																		BgL_arg1897z00_2170
																																			=
																																			BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																			(BgL_arg1455z00_2956);
																																	}
																																	BgL_arg1894z00_2168
																																		=
																																		string_append
																																		(BgL_arg1896z00_2169,
																																		BgL_arg1897z00_2170);
																																}
																																BgL_arg1893z00_2167
																																	=
																																	bstring_to_symbol
																																	(BgL_arg1894z00_2168);
																															}
																															BgL_arg1892z00_2166
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1893z00_2167,
																																BNIL);
																														}
																														BgL_arg1891z00_2165
																															=
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(30),
																															BgL_arg1892z00_2166);
																													}
																													BgL_arg1890z00_2164 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1891z00_2165,
																														BNIL);
																												}
																												BgL_arg1889z00_2163 =
																													MAKE_YOUNG_PAIR
																													(BgL_aidz00_2155,
																													BgL_arg1890z00_2164);
																											}
																											BgL_arg1885z00_2160 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1889z00_2163,
																												BNIL);
																										}
																										BgL_arg1882z00_2157 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1884z00_2159,
																											BgL_arg1885z00_2160);
																									}
																									{	/* Object/classgen.scm 282 */
																										obj_t BgL_arg1898z00_2171;

																										{	/* Object/classgen.scm 282 */
																											obj_t BgL_arg1899z00_2172;

																											{	/* Object/classgen.scm 282 */
																												obj_t
																													BgL_arg1901z00_2173;
																												{	/* Object/classgen.scm 282 */
																													obj_t
																														BgL_arg1902z00_2174;
																													obj_t
																														BgL_arg1903z00_2175;
																													{	/* Object/classgen.scm 282 */
																														obj_t
																															BgL_arg1904z00_2176;
																														{	/* Object/classgen.scm 282 */
																															obj_t
																																BgL_arg1906z00_2177;
																															{	/* Object/classgen.scm 282 */
																																obj_t
																																	BgL_arg1910z00_2178;
																																{	/* Object/classgen.scm 282 */
																																	obj_t
																																		BgL_arg1911z00_2179;
																																	obj_t
																																		BgL_arg1912z00_2180;
																																	{	/* Object/classgen.scm 282 */
																																		obj_t
																																			BgL_arg1913z00_2181;
																																		{	/* Object/classgen.scm 282 */
																																			obj_t
																																				BgL_arg1914z00_2182;
																																			BgL_arg1914z00_2182
																																				=
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(3),
																																				BNIL);
																																			BgL_arg1913z00_2181
																																				=
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(22),
																																				BgL_arg1914z00_2182);
																																		}
																																		BgL_arg1911z00_2179
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(5),
																																			BgL_arg1913z00_2181);
																																	}
																																	{	/* Object/classgen.scm 283 */
																																		obj_t
																																			BgL_arg1916z00_2183;
																																		{	/* Object/classgen.scm 283 */
																																			obj_t
																																				BgL_arg1917z00_2184;
																																			{	/* Object/classgen.scm 283 */
																																				obj_t
																																					BgL_arg1918z00_2185;
																																				obj_t
																																					BgL_arg1919z00_2186;
																																				BgL_arg1918z00_2185
																																					=
																																					(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) BgL_gz00_2144)))->BgL_idz00);
																																				BgL_arg1919z00_2186
																																					=
																																					MAKE_YOUNG_PAIR
																																					((((BgL_globalz00_bglt) COBJECT(BgL_gz00_2144))->BgL_modulez00), BNIL);
																																				BgL_arg1917z00_2184
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1918z00_2185,
																																					BgL_arg1919z00_2186);
																																			}
																																			BgL_arg1916z00_2183
																																				=
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(5),
																																				BgL_arg1917z00_2184);
																																		}
																																		BgL_arg1912z00_2180
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1916z00_2183,
																																			BNIL);
																																	}
																																	BgL_arg1910z00_2178
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1911z00_2179,
																																		BgL_arg1912z00_2180);
																																}
																																BgL_arg1906z00_2177
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1910z00_2178,
																																	BNIL);
																															}
																															BgL_arg1904z00_2176
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_string2265z00zzobject_classgenz00,
																																BgL_arg1906z00_2177);
																														}
																														BgL_arg1902z00_2174
																															=
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(31),
																															BgL_arg1904z00_2176);
																													}
																													{	/* Object/classgen.scm 284 */
																														obj_t
																															BgL_arg1923z00_2188;
																														{	/* Object/classgen.scm 284 */
																															obj_t
																																BgL_arg1924z00_2189;
																															{	/* Object/classgen.scm 284 */
																																obj_t
																																	BgL_arg1925z00_2190;
																																obj_t
																																	BgL_arg1926z00_2191;
																																{	/* Object/classgen.scm 284 */
																																	obj_t
																																		BgL_arg1927z00_2192;
																																	{	/* Object/classgen.scm 284 */
																																		obj_t
																																			BgL_arg1928z00_2193;
																																		obj_t
																																			BgL_arg1929z00_2194;
																																		BgL_arg1928z00_2193
																																			=
																																			BGl_makezd2typedzd2identz00zzast_identz00
																																			(BgL_newz00_2146,
																																			BgL_tidz00_2143);
																																		BgL_arg1929z00_2194
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BGl_czd2mallocze70z35zzobject_classgenz00
																																			(BgL_cz00_12,
																																				BgL_tidz00_2143),
																																			BNIL);
																																		BgL_arg1927z00_2192
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1928z00_2193,
																																			BgL_arg1929z00_2194);
																																	}
																																	BgL_arg1925z00_2190
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1927z00_2192,
																																		BNIL);
																																}
																																{	/* Object/classgen.scm 286 */
																																	obj_t
																																		BgL_arg1931z00_2196;
																																	obj_t
																																		BgL_arg1932z00_2197;
																																	{	/* Object/classgen.scm 286 */
																																		obj_t
																																			BgL_arg1933z00_2198;
																																		{	/* Object/classgen.scm 286 */
																																			obj_t
																																				BgL_arg1934z00_2199;
																																			{	/* Object/classgen.scm 286 */
																																				obj_t
																																					BgL_arg1935z00_2200;
																																				{	/* Object/classgen.scm 286 */
																																					obj_t
																																						BgL_arg1936z00_2201;
																																					obj_t
																																						BgL_arg1937z00_2202;
																																					{	/* Object/classgen.scm 286 */
																																						obj_t
																																							BgL_arg1938z00_2203;
																																						{	/* Object/classgen.scm 286 */
																																							obj_t
																																								BgL_arg1939z00_2204;
																																							BgL_arg1939z00_2204
																																								=
																																								MAKE_YOUNG_PAIR
																																								(CNST_TABLE_REF
																																								(3),
																																								BNIL);
																																							BgL_arg1938z00_2203
																																								=
																																								MAKE_YOUNG_PAIR
																																								(CNST_TABLE_REF
																																								(22),
																																								BgL_arg1939z00_2204);
																																						}
																																						BgL_arg1936z00_2201
																																							=
																																							MAKE_YOUNG_PAIR
																																							(CNST_TABLE_REF
																																							(5),
																																							BgL_arg1938z00_2203);
																																					}
																																					{	/* Object/classgen.scm 287 */
																																						obj_t
																																							BgL_arg1940z00_2205;
																																						{	/* Object/classgen.scm 287 */
																																							obj_t
																																								BgL_arg1941z00_2206;
																																							{	/* Object/classgen.scm 287 */
																																								obj_t
																																									BgL_arg1942z00_2207;
																																								obj_t
																																									BgL_arg1943z00_2208;
																																								BgL_arg1942z00_2207
																																									=
																																									(
																																									((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) BgL_gz00_2144)))->BgL_idz00);
																																								BgL_arg1943z00_2208
																																									=
																																									MAKE_YOUNG_PAIR
																																									(
																																									(((BgL_globalz00_bglt) COBJECT(BgL_gz00_2144))->BgL_modulez00), BNIL);
																																								BgL_arg1941z00_2206
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg1942z00_2207,
																																									BgL_arg1943z00_2208);
																																							}
																																							BgL_arg1940z00_2205
																																								=
																																								MAKE_YOUNG_PAIR
																																								(CNST_TABLE_REF
																																								(5),
																																								BgL_arg1941z00_2206);
																																						}
																																						BgL_arg1937z00_2202
																																							=
																																							MAKE_YOUNG_PAIR
																																							(BgL_arg1940z00_2205,
																																							BNIL);
																																					}
																																					BgL_arg1935z00_2200
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1936z00_2201,
																																						BgL_arg1937z00_2202);
																																				}
																																				BgL_arg1934z00_2199
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1935z00_2200,
																																					BNIL);
																																			}
																																			BgL_arg1933z00_2198
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_newz00_2146,
																																				BgL_arg1934z00_2199);
																																		}
																																		BgL_arg1931z00_2196
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(23),
																																			BgL_arg1933z00_2198);
																																	}
																																	{	/* Object/classgen.scm 288 */
																																		obj_t
																																			BgL_arg1945z00_2210;
																																		obj_t
																																			BgL_arg1946z00_2211;
																																		BgL_arg1945z00_2210
																																			=
																																			BGl_initzd2wideningze70z35zzobject_classgenz00
																																			(BgL_cz00_12,
																																			BgL_newz00_2146);
																																		BgL_arg1946z00_2211
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_newz00_2146,
																																			BNIL);
																																		BgL_arg1932z00_2197
																																			=
																																			BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																			(BgL_arg1945z00_2210,
																																			BgL_arg1946z00_2211);
																																	}
																																	BgL_arg1926z00_2191
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1931z00_2196,
																																		BgL_arg1932z00_2197);
																																}
																																BgL_arg1924z00_2189
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1925z00_2190,
																																	BgL_arg1926z00_2191);
																															}
																															BgL_arg1923z00_2188
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(24),
																																BgL_arg1924z00_2189);
																														}
																														BgL_arg1903z00_2175
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1923z00_2188,
																															BNIL);
																													}
																													BgL_arg1901z00_2173 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1902z00_2174,
																														BgL_arg1903z00_2175);
																												}
																												BgL_arg1899z00_2172 =
																													MAKE_YOUNG_PAIR(BNIL,
																													BgL_arg1901z00_2173);
																											}
																											BgL_arg1898z00_2171 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(24),
																												BgL_arg1899z00_2172);
																										}
																										BgL_arg1883z00_2158 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1898z00_2171,
																											BNIL);
																									}
																									BgL_arg1880z00_2156 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1882z00_2157,
																										BgL_arg1883z00_2158);
																								}
																								BgL_arg1878z00_2151 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(24), BgL_arg1880z00_2156);
																			}}}}}
																		else
																			{	/* Object/classgen.scm 290 */
																				obj_t BgL_arg1947z00_2212;

																				{	/* Object/classgen.scm 290 */
																					obj_t BgL_arg1948z00_2213;
																					obj_t BgL_arg1949z00_2214;

																					{	/* Object/classgen.scm 290 */
																						obj_t BgL_arg1950z00_2215;

																						{	/* Object/classgen.scm 290 */
																							obj_t BgL_arg1951z00_2216;
																							obj_t BgL_arg1952z00_2217;

																							BgL_arg1951z00_2216 =
																								BGl_makezd2typedzd2identz00zzast_identz00
																								(BgL_newz00_2146,
																								BgL_tidz00_2143);
																							BgL_arg1952z00_2217 =
																								MAKE_YOUNG_PAIR
																								(BGl_czd2mallocze70z35zzobject_classgenz00
																								(BgL_cz00_12, BgL_tidz00_2143),
																								BNIL);
																							BgL_arg1950z00_2215 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1951z00_2216,
																								BgL_arg1952z00_2217);
																						}
																						BgL_arg1948z00_2213 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1950z00_2215, BNIL);
																					}
																					{	/* Object/classgen.scm 292 */
																						obj_t BgL_arg1954z00_2219;
																						obj_t BgL_arg1955z00_2220;

																						{	/* Object/classgen.scm 292 */
																							obj_t BgL_arg1956z00_2221;

																							{	/* Object/classgen.scm 292 */
																								obj_t BgL_arg1957z00_2222;

																								{	/* Object/classgen.scm 292 */
																									obj_t BgL_arg1958z00_2223;

																									{	/* Object/classgen.scm 292 */
																										obj_t BgL_arg1959z00_2224;
																										obj_t BgL_arg1960z00_2225;

																										{	/* Object/classgen.scm 292 */
																											obj_t BgL_arg1961z00_2226;

																											{	/* Object/classgen.scm 292 */
																												obj_t
																													BgL_arg1962z00_2227;
																												BgL_arg1962z00_2227 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(3),
																													BNIL);
																												BgL_arg1961z00_2226 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(22),
																													BgL_arg1962z00_2227);
																											}
																											BgL_arg1959z00_2224 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(5),
																												BgL_arg1961z00_2226);
																										}
																										{	/* Object/classgen.scm 293 */
																											obj_t BgL_arg1963z00_2228;

																											{	/* Object/classgen.scm 293 */
																												obj_t
																													BgL_arg1964z00_2229;
																												{	/* Object/classgen.scm 293 */
																													obj_t
																														BgL_arg1965z00_2230;
																													obj_t
																														BgL_arg1966z00_2231;
																													BgL_arg1965z00_2230 =
																														(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) BgL_gz00_2144)))->BgL_idz00);
																													BgL_arg1966z00_2231 =
																														MAKE_YOUNG_PAIR(((
																																(BgL_globalz00_bglt)
																																COBJECT
																																(BgL_gz00_2144))->
																															BgL_modulez00),
																														BNIL);
																													BgL_arg1964z00_2229 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1965z00_2230,
																														BgL_arg1966z00_2231);
																												}
																												BgL_arg1963z00_2228 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(5),
																													BgL_arg1964z00_2229);
																											}
																											BgL_arg1960z00_2225 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1963z00_2228,
																												BNIL);
																										}
																										BgL_arg1958z00_2223 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1959z00_2224,
																											BgL_arg1960z00_2225);
																									}
																									BgL_arg1957z00_2222 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1958z00_2223, BNIL);
																								}
																								BgL_arg1956z00_2221 =
																									MAKE_YOUNG_PAIR
																									(BgL_newz00_2146,
																									BgL_arg1957z00_2222);
																							}
																							BgL_arg1954z00_2219 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(23), BgL_arg1956z00_2221);
																						}
																						{	/* Object/classgen.scm 294 */
																							obj_t BgL_arg1968z00_2233;
																							obj_t BgL_arg1969z00_2234;

																							BgL_arg1968z00_2233 =
																								BGl_initzd2wideningze70z35zzobject_classgenz00
																								(BgL_cz00_12, BgL_newz00_2146);
																							BgL_arg1969z00_2234 =
																								MAKE_YOUNG_PAIR(BgL_newz00_2146,
																								BNIL);
																							BgL_arg1955z00_2220 =
																								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																								(BgL_arg1968z00_2233,
																								BgL_arg1969z00_2234);
																						}
																						BgL_arg1949z00_2214 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1954z00_2219,
																							BgL_arg1955z00_2220);
																					}
																					BgL_arg1947z00_2212 =
																						MAKE_YOUNG_PAIR(BgL_arg1948z00_2213,
																						BgL_arg1949z00_2214);
																				}
																				BgL_arg1878z00_2151 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(24),
																					BgL_arg1947z00_2212);
																			}
																		BgL_arg1877z00_2150 =
																			BGl_unsafeze70ze7zzobject_classgenz00
																			(BgL_cz00_12, BgL_arg1878z00_2151);
																	}
																	BgL_arg1876z00_2149 =
																		MAKE_YOUNG_PAIR(BgL_arg1877z00_2150, BNIL);
																}
																BgL_arg1874z00_2147 =
																	MAKE_YOUNG_PAIR(BgL_arg1875z00_2148,
																	BgL_arg1876z00_2149);
															}
															BgL_val1_1298z00_2108 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(7),
																BgL_arg1874z00_2147);
														}
													}
												}
											else
												{	/* Object/classgen.scm 315 */
													BgL_idz00_2236 = BgL_alloczd2tidzd2_2105;
													BgL_tidz00_2237 = BgL_tidz00_2103;
													BgL_gz00_2238 = BgL_holderz00_2106;
													{	/* Object/classgen.scm 298 */
														obj_t BgL_newz00_2240;

														BgL_newz00_2240 =
															BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
															(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF
																(15)));
														{	/* Object/classgen.scm 299 */
															obj_t BgL_arg1972z00_2241;

															{	/* Object/classgen.scm 299 */
																obj_t BgL_arg1973z00_2242;
																obj_t BgL_arg1974z00_2243;

																BgL_arg1973z00_2242 =
																	MAKE_YOUNG_PAIR(BgL_idz00_2236, BNIL);
																{	/* Object/classgen.scm 301 */
																	obj_t BgL_arg1975z00_2244;

																	{	/* Object/classgen.scm 301 */
																		obj_t BgL_arg1976z00_2245;

																		{	/* Object/classgen.scm 301 */
																			obj_t BgL_arg1977z00_2246;

																			{	/* Object/classgen.scm 301 */
																				obj_t BgL_arg1978z00_2247;
																				obj_t BgL_arg1979z00_2248;

																				{	/* Object/classgen.scm 301 */
																					obj_t BgL_arg1980z00_2249;

																					{	/* Object/classgen.scm 301 */
																						obj_t BgL_arg1981z00_2250;
																						obj_t BgL_arg1982z00_2251;

																						BgL_arg1981z00_2250 =
																							BGl_makezd2typedzd2identz00zzast_identz00
																							(BgL_newz00_2240,
																							BgL_tidz00_2237);
																						BgL_arg1982z00_2251 =
																							MAKE_YOUNG_PAIR
																							(BGl_makezd2privatezd2sexpz00zzast_privatez00
																							(CNST_TABLE_REF(15),
																								BgL_tidz00_2237, BNIL), BNIL);
																						BgL_arg1980z00_2249 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1981z00_2250,
																							BgL_arg1982z00_2251);
																					}
																					BgL_arg1978z00_2247 =
																						MAKE_YOUNG_PAIR(BgL_arg1980z00_2249,
																						BNIL);
																				}
																				{	/* Object/classgen.scm 303 */
																					obj_t BgL_arg1985z00_2254;
																					obj_t BgL_arg1986z00_2255;

																					{	/* Object/classgen.scm 303 */
																						obj_t BgL_arg1987z00_2256;

																						{	/* Object/classgen.scm 303 */
																							obj_t BgL_arg1988z00_2257;

																							{	/* Object/classgen.scm 303 */
																								obj_t BgL_arg1989z00_2258;

																								{	/* Object/classgen.scm 303 */
																									obj_t BgL_arg1990z00_2259;
																									obj_t BgL_arg1991z00_2260;

																									{	/* Object/classgen.scm 303 */
																										obj_t BgL_arg1992z00_2261;

																										{	/* Object/classgen.scm 303 */
																											obj_t BgL_arg1993z00_2262;

																											BgL_arg1993z00_2262 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(3),
																												BNIL);
																											BgL_arg1992z00_2261 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(22),
																												BgL_arg1993z00_2262);
																										}
																										BgL_arg1990z00_2259 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(5),
																											BgL_arg1992z00_2261);
																									}
																									{	/* Object/classgen.scm 304 */
																										obj_t BgL_arg1994z00_2263;

																										{	/* Object/classgen.scm 304 */
																											obj_t BgL_arg1995z00_2264;

																											{	/* Object/classgen.scm 304 */
																												obj_t
																													BgL_arg1996z00_2265;
																												obj_t
																													BgL_arg1997z00_2266;
																												BgL_arg1996z00_2265 =
																													(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) BgL_gz00_2238)))->BgL_idz00);
																												BgL_arg1997z00_2266 =
																													MAKE_YOUNG_PAIR(((
																															(BgL_globalz00_bglt)
																															COBJECT
																															(BgL_gz00_2238))->
																														BgL_modulez00),
																													BNIL);
																												BgL_arg1995z00_2264 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1996z00_2265,
																													BgL_arg1997z00_2266);
																											}
																											BgL_arg1994z00_2263 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(5),
																												BgL_arg1995z00_2264);
																										}
																										BgL_arg1991z00_2260 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1994z00_2263,
																											BNIL);
																									}
																									BgL_arg1989z00_2258 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1990z00_2259,
																										BgL_arg1991z00_2260);
																								}
																								BgL_arg1988z00_2257 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1989z00_2258, BNIL);
																							}
																							BgL_arg1987z00_2256 =
																								MAKE_YOUNG_PAIR(BgL_newz00_2240,
																								BgL_arg1988z00_2257);
																						}
																						BgL_arg1985z00_2254 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(23), BgL_arg1987z00_2256);
																					}
																					{	/* Object/classgen.scm 305 */
																						obj_t BgL_arg1999z00_2268;
																						obj_t BgL_arg2000z00_2269;

																						BgL_arg1999z00_2268 =
																							BGl_initzd2wideningze70z35zzobject_classgenz00
																							(BgL_cz00_12, BgL_newz00_2240);
																						BgL_arg2000z00_2269 =
																							MAKE_YOUNG_PAIR(BgL_newz00_2240,
																							BNIL);
																						BgL_arg1986z00_2255 =
																							BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																							(BgL_arg1999z00_2268,
																							BgL_arg2000z00_2269);
																					}
																					BgL_arg1979z00_2248 =
																						MAKE_YOUNG_PAIR(BgL_arg1985z00_2254,
																						BgL_arg1986z00_2255);
																				}
																				BgL_arg1977z00_2246 =
																					MAKE_YOUNG_PAIR(BgL_arg1978z00_2247,
																					BgL_arg1979z00_2248);
																			}
																			BgL_arg1976z00_2245 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(24),
																				BgL_arg1977z00_2246);
																		}
																		BgL_arg1975z00_2244 =
																			BGl_unsafeze70ze7zzobject_classgenz00
																			(BgL_cz00_12, BgL_arg1976z00_2245);
																	}
																	BgL_arg1974z00_2243 =
																		MAKE_YOUNG_PAIR(BgL_arg1975z00_2244, BNIL);
																}
																BgL_arg1972z00_2241 =
																	MAKE_YOUNG_PAIR(BgL_arg1973z00_2242,
																	BgL_arg1974z00_2243);
															}
															BgL_val1_1298z00_2108 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(7),
																BgL_arg1972z00_2241);
														}
													}
												}
										}
										{	/* Object/classgen.scm 313 */
											int BgL_tmpz00_4025;

											BgL_tmpz00_4025 = (int) (2L);
											BGL_MVALUES_NUMBER_SET(BgL_tmpz00_4025);
										}
										{	/* Object/classgen.scm 313 */
											int BgL_tmpz00_4028;

											BgL_tmpz00_4028 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_4028,
												BgL_val1_1298z00_2108);
										}
										return BgL_val0_1297z00_2107;
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* c-malloc~0 */
	obj_t BGl_czd2mallocze70z35zzobject_classgenz00(BgL_typez00_bglt
		BgL_cz00_3115, obj_t BgL_tidz00_2120)
	{
		{	/* Object/classgen.scm 263 */
			{	/* Object/classgen.scm 257 */
				obj_t BgL_tnamez00_2122;
				obj_t BgL_siza7eofza7_2123;

				{	/* Object/classgen.scm 257 */
					obj_t BgL_arg1863z00_2132;

					BgL_arg1863z00_2132 =
						(((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_cz00_3115)))->BgL_namez00);
					BgL_tnamez00_2122 =
						BGl_stringzd2sanszd2z42z42zztype_toolsz00(BgL_arg1863z00_2132);
				}
				{	/* Object/classgen.scm 258 */
					bool_t BgL_test2353z00_4034;

					{	/* Object/classgen.scm 258 */
						obj_t BgL_tmpz00_4035;

						BgL_tmpz00_4035 =
							(((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt) BgL_cz00_3115)))->BgL_siza7eza7);
						BgL_test2353z00_4034 = STRINGP(BgL_tmpz00_4035);
					}
					if (BgL_test2353z00_4034)
						{	/* Object/classgen.scm 258 */
							BgL_siza7eofza7_2123 =
								(((BgL_typez00_bglt) COBJECT(
										((BgL_typez00_bglt) BgL_cz00_3115)))->BgL_siza7eza7);
						}
					else
						{	/* Object/classgen.scm 258 */
							BgL_siza7eofza7_2123 =
								(((BgL_typez00_bglt) COBJECT(
										((BgL_typez00_bglt) BgL_cz00_3115)))->BgL_namez00);
						}
				}
				{	/* Object/classgen.scm 261 */
					obj_t BgL_arg1853z00_2124;
					obj_t BgL_arg1854z00_2125;

					BgL_arg1853z00_2124 =
						BGl_makezd2typedzd2identz00zzast_identz00(CNST_TABLE_REF(32),
						BgL_tidz00_2120);
					{	/* Object/classgen.scm 262 */
						obj_t BgL_arg1856z00_2126;

						{	/* Object/classgen.scm 262 */
							obj_t BgL_list1857z00_2127;

							{	/* Object/classgen.scm 262 */
								obj_t BgL_arg1858z00_2128;

								{	/* Object/classgen.scm 262 */
									obj_t BgL_arg1859z00_2129;

									{	/* Object/classgen.scm 262 */
										obj_t BgL_arg1860z00_2130;

										{	/* Object/classgen.scm 262 */
											obj_t BgL_arg1862z00_2131;

											BgL_arg1862z00_2131 =
												MAKE_YOUNG_PAIR(BGl_string2266z00zzobject_classgenz00,
												BNIL);
											BgL_arg1860z00_2130 =
												MAKE_YOUNG_PAIR(BgL_siza7eofza7_2123,
												BgL_arg1862z00_2131);
										}
										BgL_arg1859z00_2129 =
											MAKE_YOUNG_PAIR(BGl_string2267z00zzobject_classgenz00,
											BgL_arg1860z00_2130);
									}
									BgL_arg1858z00_2128 =
										MAKE_YOUNG_PAIR(BgL_tnamez00_2122, BgL_arg1859z00_2129);
								}
								BgL_list1857z00_2127 =
									MAKE_YOUNG_PAIR(BGl_string2268z00zzobject_classgenz00,
									BgL_arg1858z00_2128);
							}
							BgL_arg1856z00_2126 =
								BGl_stringzd2appendzd2zz__r4_strings_6_7z00
								(BgL_list1857z00_2127);
						}
						BgL_arg1854z00_2125 = MAKE_YOUNG_PAIR(BgL_arg1856z00_2126, BNIL);
					}
					return MAKE_YOUNG_PAIR(BgL_arg1853z00_2124, BgL_arg1854z00_2125);
				}
			}
		}

	}



/* init-widening~0 */
	obj_t BGl_initzd2wideningze70z35zzobject_classgenz00(BgL_typez00_bglt
		BgL_cz00_3116, obj_t BgL_newz00_2136)
	{
		{	/* Object/classgen.scm 268 */
			if (BGl_finalzd2classzf3z21zzobject_classz00(((obj_t) BgL_cz00_3116)))
				{	/* Object/classgen.scm 267 */
					obj_t BgL_arg1869z00_2139;

					{	/* Object/classgen.scm 267 */
						obj_t BgL_arg1870z00_2140;

						{	/* Object/classgen.scm 267 */
							obj_t BgL_arg1872z00_2141;

							BgL_arg1872z00_2141 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
							BgL_arg1870z00_2140 =
								MAKE_YOUNG_PAIR(BgL_newz00_2136, BgL_arg1872z00_2141);
						}
						BgL_arg1869z00_2139 =
							MAKE_YOUNG_PAIR(CNST_TABLE_REF(33), BgL_arg1870z00_2140);
					}
					return MAKE_YOUNG_PAIR(BgL_arg1869z00_2139, BNIL);
				}
			else
				{	/* Object/classgen.scm 266 */
					return BNIL;
				}
		}

	}



/* unsafe~0 */
	obj_t BGl_unsafeze70ze7zzobject_classgenz00(BgL_typez00_bglt BgL_cz00_3117,
		obj_t BgL_exprz00_2116)
	{
		{	/* Object/classgen.scm 254 */
			if ((BGl_za2passza2z00zzengine_paramz00 == CNST_TABLE_REF(34)))
				{	/* Object/classgen.scm 251 */
					return BgL_exprz00_2116;
				}
			else
				{	/* Object/classgen.scm 254 */
					obj_t BgL_arg1850z00_2118;

					BgL_arg1850z00_2118 =
						(((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_cz00_3117)))->BgL_idz00);
					{	/* Object/classgen.scm 254 */
						obj_t BgL_list1851z00_2119;

						BgL_list1851z00_2119 = MAKE_YOUNG_PAIR(BgL_exprz00_2116, BNIL);
						return
							BGl_makezd2privatezd2sexpz00zzast_privatez00(CNST_TABLE_REF(35),
							BgL_arg1850z00_2118, BgL_list1851z00_2119);
					}
				}
		}

	}



/* classgen-allocate-expr */
	BGL_EXPORTED_DEF obj_t
		BGl_classgenzd2allocatezd2exprz00zzobject_classgenz00(BgL_typez00_bglt
		BgL_cz00_13)
	{
		{	/* Object/classgen.scm 322 */
			{	/* Object/classgen.scm 323 */
				obj_t BgL_protoz00_2276;

				BgL_protoz00_2276 =
					BGl_classgenzd2allocatezd2zzobject_classgenz00(BgL_cz00_13);
				{	/* Object/classgen.scm 324 */
					obj_t BgL_defz00_2277;

					{	/* Object/classgen.scm 325 */
						obj_t BgL_tmpz00_2975;

						{	/* Object/classgen.scm 325 */
							int BgL_tmpz00_4070;

							BgL_tmpz00_4070 = (int) (1L);
							BgL_tmpz00_2975 = BGL_MVALUES_VAL(BgL_tmpz00_4070);
						}
						{	/* Object/classgen.scm 325 */
							int BgL_tmpz00_4073;

							BgL_tmpz00_4073 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_4073, BUNSPEC);
						}
						BgL_defz00_2277 = BgL_tmpz00_2975;
					}
					if (PAIRP(BgL_defz00_2277))
						{	/* Object/classgen.scm 325 */
							obj_t BgL_cdrzd2190zd2_2282;

							BgL_cdrzd2190zd2_2282 = CDR(BgL_defz00_2277);
							if (PAIRP(BgL_cdrzd2190zd2_2282))
								{	/* Object/classgen.scm 325 */
									obj_t BgL_cdrzd2193zd2_2284;

									BgL_cdrzd2193zd2_2284 = CDR(BgL_cdrzd2190zd2_2282);
									if (PAIRP(BgL_cdrzd2193zd2_2284))
										{	/* Object/classgen.scm 325 */
											if (NULLP(CDR(BgL_cdrzd2193zd2_2284)))
												{	/* Object/classgen.scm 325 */
													return CAR(BgL_cdrzd2193zd2_2284);
												}
											else
												{	/* Object/classgen.scm 325 */
													return BFALSE;
												}
										}
									else
										{	/* Object/classgen.scm 325 */
											return BFALSE;
										}
								}
							else
								{	/* Object/classgen.scm 325 */
									return BFALSE;
								}
						}
					else
						{	/* Object/classgen.scm 325 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &classgen-allocate-expr */
	obj_t BGl_z62classgenzd2allocatezd2exprz62zzobject_classgenz00(obj_t
		BgL_envz00_3098, obj_t BgL_cz00_3099)
	{
		{	/* Object/classgen.scm 322 */
			return
				BGl_classgenzd2allocatezd2exprz00zzobject_classgenz00(
				((BgL_typez00_bglt) BgL_cz00_3099));
		}

	}



/* classgen-allocate-anonymous */
	BGL_EXPORTED_DEF obj_t
		BGl_classgenzd2allocatezd2anonymousz00zzobject_classgenz00(BgL_typez00_bglt
		BgL_cz00_14)
	{
		{	/* Object/classgen.scm 332 */
			{	/* Object/classgen.scm 333 */
				obj_t BgL_arg2010z00_2290;
				obj_t BgL_arg2011z00_2291;

				{	/* Object/classgen.scm 333 */
					obj_t BgL_arg2012z00_2292;

					BgL_arg2012z00_2292 =
						(((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_cz00_14)))->BgL_idz00);
					BgL_arg2010z00_2290 =
						BGl_makezd2typedzd2identz00zzast_identz00(CNST_TABLE_REF(12),
						BgL_arg2012z00_2292);
				}
				{	/* Object/classgen.scm 333 */
					obj_t BgL_arg2013z00_2293;

					BgL_arg2013z00_2293 =
						MAKE_YOUNG_PAIR
						(BGl_classgenzd2allocatezd2exprz00zzobject_classgenz00(BgL_cz00_14),
						BNIL);
					BgL_arg2011z00_2291 = MAKE_YOUNG_PAIR(BNIL, BgL_arg2013z00_2293);
				}
				return MAKE_YOUNG_PAIR(BgL_arg2010z00_2290, BgL_arg2011z00_2291);
			}
		}

	}



/* &classgen-allocate-anonymous */
	obj_t BGl_z62classgenzd2allocatezd2anonymousz62zzobject_classgenz00(obj_t
		BgL_envz00_3100, obj_t BgL_cz00_3101)
	{
		{	/* Object/classgen.scm 332 */
			return
				BGl_classgenzd2allocatezd2anonymousz00zzobject_classgenz00(
				((BgL_typez00_bglt) BgL_cz00_3101));
		}

	}



/* classgen-widen-expr */
	BGL_EXPORTED_DEF obj_t
		BGl_classgenzd2widenzd2exprz00zzobject_classgenz00(BgL_typez00_bglt
		BgL_cz00_15, obj_t BgL_oz00_16)
	{
		{	/* Object/classgen.scm 338 */
			{
				obj_t BgL_exprz00_2368;
				obj_t BgL_wz00_2347;

				{	/* Object/classgen.scm 358 */
					obj_t BgL_wz00_2298;

					{
						BgL_tclassz00_bglt BgL_auxz00_4100;

						{
							obj_t BgL_auxz00_4101;

							{	/* Object/classgen.scm 358 */
								BgL_objectz00_bglt BgL_tmpz00_4102;

								BgL_tmpz00_4102 = ((BgL_objectz00_bglt) BgL_cz00_15);
								BgL_auxz00_4101 = BGL_OBJECT_WIDENING(BgL_tmpz00_4102);
							}
							BgL_auxz00_4100 = ((BgL_tclassz00_bglt) BgL_auxz00_4101);
						}
						BgL_wz00_2298 =
							(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_4100))->
							BgL_widezd2typezd2);
					}
					{	/* Object/classgen.scm 358 */
						obj_t BgL_sz00_2299;

						{
							BgL_tclassz00_bglt BgL_auxz00_4107;

							{
								obj_t BgL_auxz00_4108;

								{	/* Object/classgen.scm 359 */
									BgL_objectz00_bglt BgL_tmpz00_4109;

									BgL_tmpz00_4109 = ((BgL_objectz00_bglt) BgL_cz00_15);
									BgL_auxz00_4108 = BGL_OBJECT_WIDENING(BgL_tmpz00_4109);
								}
								BgL_auxz00_4107 = ((BgL_tclassz00_bglt) BgL_auxz00_4108);
							}
							BgL_sz00_2299 =
								(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_4107))->
								BgL_itszd2superzd2);
						}
						{	/* Object/classgen.scm 359 */
							obj_t BgL_widz00_2300;

							BgL_widz00_2300 =
								(((BgL_typez00_bglt) COBJECT(
										((BgL_typez00_bglt) BgL_wz00_2298)))->BgL_idz00);
							{	/* Object/classgen.scm 360 */
								obj_t BgL_sidz00_2301;

								BgL_sidz00_2301 =
									(((BgL_typez00_bglt) COBJECT(
											((BgL_typez00_bglt) BgL_sz00_2299)))->BgL_idz00);
								{	/* Object/classgen.scm 361 */
									obj_t BgL_tmpz00_2302;

									BgL_tmpz00_2302 =
										BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
										(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(36)));
									{	/* Object/classgen.scm 362 */
										obj_t BgL_ttmpz00_2303;

										BgL_ttmpz00_2303 =
											BGl_makezd2typedzd2identz00zzast_identz00(BgL_tmpz00_2302,
											BgL_sidz00_2301);
										{	/* Object/classgen.scm 363 */
											obj_t BgL_widez00_2304;

											BgL_widez00_2304 =
												BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
												(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF
													(37)));
											{	/* Object/classgen.scm 364 */
												obj_t BgL_twidez00_2305;

												BgL_twidez00_2305 =
													BGl_makezd2typedzd2identz00zzast_identz00
													(BgL_widez00_2304, BgL_widz00_2300);
												{	/* Object/classgen.scm 365 */
													BgL_globalz00_bglt BgL_holderz00_2306;

													{
														BgL_tclassz00_bglt BgL_auxz00_4126;

														{
															obj_t BgL_auxz00_4127;

															{	/* Object/classgen.scm 366 */
																BgL_objectz00_bglt BgL_tmpz00_4128;

																BgL_tmpz00_4128 =
																	((BgL_objectz00_bglt) BgL_cz00_15);
																BgL_auxz00_4127 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_4128);
															}
															BgL_auxz00_4126 =
																((BgL_tclassz00_bglt) BgL_auxz00_4127);
														}
														BgL_holderz00_2306 =
															(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_4126))->
															BgL_holderz00);
													}
													{	/* Object/classgen.scm 366 */

														{	/* Object/classgen.scm 367 */
															obj_t BgL_arg2015z00_2307;

															{	/* Object/classgen.scm 367 */
																obj_t BgL_arg2016z00_2308;
																obj_t BgL_arg2017z00_2309;

																{	/* Object/classgen.scm 367 */
																	obj_t BgL_arg2018z00_2310;
																	obj_t BgL_arg2019z00_2311;

																	{	/* Object/classgen.scm 367 */
																		obj_t BgL_arg2020z00_2312;

																		{	/* Object/classgen.scm 367 */
																			obj_t BgL_arg2021z00_2313;

																			{	/* Object/classgen.scm 367 */
																				obj_t BgL_list2022z00_2314;

																				BgL_list2022z00_2314 =
																					MAKE_YOUNG_PAIR(BgL_oz00_16, BNIL);
																				BgL_arg2021z00_2313 =
																					BGl_makezd2privatezd2sexpz00zzast_privatez00
																					(CNST_TABLE_REF(38), BgL_sidz00_2301,
																					BgL_list2022z00_2314);
																			}
																			BgL_arg2020z00_2312 =
																				MAKE_YOUNG_PAIR(BgL_arg2021z00_2313,
																				BNIL);
																		}
																		BgL_arg2018z00_2310 =
																			MAKE_YOUNG_PAIR(BgL_ttmpz00_2303,
																			BgL_arg2020z00_2312);
																	}
																	{	/* Object/classgen.scm 368 */
																		obj_t BgL_arg2024z00_2315;

																		{	/* Object/classgen.scm 368 */
																			obj_t BgL_arg2025z00_2316;

																			{	/* Object/classgen.scm 368 */
																				obj_t BgL_arg2026z00_2317;

																				{	/* Object/classgen.scm 368 */
																					bool_t BgL_test2360z00_4138;

																					{	/* Object/classgen.scm 368 */
																						obj_t BgL_arg2029z00_2320;

																						BgL_arg2029z00_2320 =
																							BGl_thezd2backendzd2zzbackend_backendz00
																							();
																						BgL_test2360z00_4138 =
																							(((BgL_backendz00_bglt)
																								COBJECT(((BgL_backendz00_bglt)
																										BgL_arg2029z00_2320)))->
																							BgL_pragmazd2supportzd2);
																					}
																					if (BgL_test2360z00_4138)
																						{	/* Object/classgen.scm 368 */
																							BgL_wz00_2347 = BgL_wz00_2298;
																							{	/* Object/classgen.scm 341 */
																								obj_t BgL_tnamez00_2349;
																								obj_t BgL_siza7eofza7_2350;

																								{	/* Object/classgen.scm 341 */
																									obj_t BgL_arg2072z00_2360;

																									BgL_arg2072z00_2360 =
																										(((BgL_typez00_bglt)
																											COBJECT((
																													(BgL_typez00_bglt)
																													BgL_wz00_2347)))->
																										BgL_namez00);
																									BgL_tnamez00_2349 =
																										BGl_stringzd2sanszd2z42z42zztype_toolsz00
																										(BgL_arg2072z00_2360);
																								}
																								{	/* Object/classgen.scm 342 */
																									bool_t BgL_test2361z00_4145;

																									{	/* Object/classgen.scm 342 */
																										obj_t BgL_tmpz00_4146;

																										BgL_tmpz00_4146 =
																											(((BgL_typez00_bglt)
																												COBJECT((
																														(BgL_typez00_bglt)
																														BgL_wz00_2347)))->
																											BgL_siza7eza7);
																										BgL_test2361z00_4145 =
																											STRINGP(BgL_tmpz00_4146);
																									}
																									if (BgL_test2361z00_4145)
																										{	/* Object/classgen.scm 342 */
																											BgL_siza7eofza7_2350 =
																												(((BgL_typez00_bglt)
																													COBJECT((
																															(BgL_typez00_bglt)
																															BgL_wz00_2347)))->
																												BgL_siza7eza7);
																										}
																									else
																										{	/* Object/classgen.scm 342 */
																											BgL_siza7eofza7_2350 =
																												(((BgL_typez00_bglt)
																													COBJECT((
																															(BgL_typez00_bglt)
																															BgL_wz00_2347)))->
																												BgL_namez00);
																										}
																								}
																								{	/* Object/classgen.scm 345 */
																									obj_t BgL_arg2062z00_2351;
																									obj_t BgL_arg2063z00_2352;

																									{	/* Object/classgen.scm 345 */
																										obj_t BgL_arg2064z00_2353;

																										BgL_arg2064z00_2353 =
																											(((BgL_typez00_bglt)
																												COBJECT((
																														(BgL_typez00_bglt)
																														BgL_wz00_2347)))->
																											BgL_idz00);
																										BgL_arg2062z00_2351 =
																											BGl_makezd2typedzd2identz00zzast_identz00
																											(CNST_TABLE_REF(32),
																											BgL_arg2064z00_2353);
																									}
																									{	/* Object/classgen.scm 346 */
																										obj_t BgL_arg2065z00_2354;

																										{	/* Object/classgen.scm 346 */
																											obj_t
																												BgL_list2066z00_2355;
																											{	/* Object/classgen.scm 346 */
																												obj_t
																													BgL_arg2067z00_2356;
																												{	/* Object/classgen.scm 346 */
																													obj_t
																														BgL_arg2068z00_2357;
																													{	/* Object/classgen.scm 346 */
																														obj_t
																															BgL_arg2069z00_2358;
																														{	/* Object/classgen.scm 346 */
																															obj_t
																																BgL_arg2070z00_2359;
																															BgL_arg2070z00_2359
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_string2266z00zzobject_classgenz00,
																																BNIL);
																															BgL_arg2069z00_2358
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_siza7eofza7_2350,
																																BgL_arg2070z00_2359);
																														}
																														BgL_arg2068z00_2357
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_string2267z00zzobject_classgenz00,
																															BgL_arg2069z00_2358);
																													}
																													BgL_arg2067z00_2356 =
																														MAKE_YOUNG_PAIR
																														(BgL_tnamez00_2349,
																														BgL_arg2068z00_2357);
																												}
																												BgL_list2066z00_2355 =
																													MAKE_YOUNG_PAIR
																													(BGl_string2268z00zzobject_classgenz00,
																													BgL_arg2067z00_2356);
																											}
																											BgL_arg2065z00_2354 =
																												BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																												(BgL_list2066z00_2355);
																										}
																										BgL_arg2063z00_2352 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg2065z00_2354,
																											BNIL);
																									}
																									BgL_arg2026z00_2317 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg2062z00_2351,
																										BgL_arg2063z00_2352);
																								}
																							}
																						}
																					else
																						{	/* Object/classgen.scm 350 */
																							obj_t BgL_arg2078z00_2999;

																							BgL_arg2078z00_2999 =
																								(((BgL_typez00_bglt) COBJECT(
																										((BgL_typez00_bglt)
																											BgL_wz00_2298)))->
																								BgL_idz00);
																							BgL_arg2026z00_2317 =
																								BGl_makezd2privatezd2sexpz00zzast_privatez00
																								(CNST_TABLE_REF(15),
																								BgL_arg2078z00_2999, BNIL);
																						}
																				}
																				BgL_arg2025z00_2316 =
																					MAKE_YOUNG_PAIR(BgL_arg2026z00_2317,
																					BNIL);
																			}
																			BgL_arg2024z00_2315 =
																				MAKE_YOUNG_PAIR(BgL_twidez00_2305,
																				BgL_arg2025z00_2316);
																		}
																		BgL_arg2019z00_2311 =
																			MAKE_YOUNG_PAIR(BgL_arg2024z00_2315,
																			BNIL);
																	}
																	BgL_arg2016z00_2308 =
																		MAKE_YOUNG_PAIR(BgL_arg2018z00_2310,
																		BgL_arg2019z00_2311);
																}
																{	/* Object/classgen.scm 373 */
																	obj_t BgL_arg2030z00_2321;

																	{	/* Object/classgen.scm 373 */
																		obj_t BgL_arg2031z00_2322;

																		{	/* Object/classgen.scm 373 */
																			obj_t BgL_arg2033z00_2323;

																			{	/* Object/classgen.scm 373 */
																				obj_t BgL_arg2034z00_2324;
																				obj_t BgL_arg2036z00_2325;

																				{	/* Object/classgen.scm 373 */
																					obj_t BgL_arg2037z00_2326;

																					{	/* Object/classgen.scm 373 */
																						obj_t BgL_arg2038z00_2327;

																						BgL_arg2038z00_2327 =
																							MAKE_YOUNG_PAIR(BgL_widez00_2304,
																							BNIL);
																						BgL_arg2037z00_2326 =
																							MAKE_YOUNG_PAIR(BgL_tmpz00_2302,
																							BgL_arg2038z00_2327);
																					}
																					BgL_arg2034z00_2324 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(33),
																						BgL_arg2037z00_2326);
																				}
																				{	/* Object/classgen.scm 374 */
																					obj_t BgL_arg2039z00_2328;
																					obj_t BgL_arg2040z00_2329;

																					{	/* Object/classgen.scm 374 */
																						obj_t BgL_arg2041z00_2330;
																						obj_t BgL_arg2042z00_2331;

																						{	/* Object/classgen.scm 374 */
																							obj_t BgL_arg2044z00_2332;

																							{	/* Object/classgen.scm 374 */
																								obj_t BgL_arg2045z00_2333;

																								BgL_arg2045z00_2333 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(3), BNIL);
																								BgL_arg2044z00_2332 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(23), BgL_arg2045z00_2333);
																							}
																							BgL_arg2041z00_2330 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(5), BgL_arg2044z00_2332);
																						}
																						{	/* Object/classgen.scm 376 */
																							obj_t BgL_arg2046z00_2334;

																							{	/* Object/classgen.scm 376 */
																								obj_t BgL_arg2047z00_2335;

																								{	/* Object/classgen.scm 376 */
																									obj_t BgL_arg2048z00_2336;
																									obj_t BgL_arg2049z00_2337;

																									{	/* Object/classgen.scm 376 */
																										obj_t BgL_arg2050z00_2338;

																										{	/* Object/classgen.scm 376 */
																											obj_t BgL_arg2051z00_2339;

																											BgL_arg2051z00_2339 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(3),
																												BNIL);
																											BgL_arg2050z00_2338 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(22),
																												BgL_arg2051z00_2339);
																										}
																										BgL_arg2048z00_2336 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(5),
																											BgL_arg2050z00_2338);
																									}
																									{	/* Object/classgen.scm 377 */
																										obj_t BgL_arg2052z00_2340;

																										{	/* Object/classgen.scm 377 */
																											obj_t BgL_arg2055z00_2341;

																											{	/* Object/classgen.scm 377 */
																												obj_t
																													BgL_arg2056z00_2342;
																												obj_t
																													BgL_arg2057z00_2343;
																												BgL_arg2056z00_2342 =
																													(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) BgL_holderz00_2306)))->BgL_idz00);
																												BgL_arg2057z00_2343 =
																													MAKE_YOUNG_PAIR(((
																															(BgL_globalz00_bglt)
																															COBJECT
																															(BgL_holderz00_2306))->
																														BgL_modulez00),
																													BNIL);
																												BgL_arg2055z00_2341 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg2056z00_2342,
																													BgL_arg2057z00_2343);
																											}
																											BgL_arg2052z00_2340 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(5),
																												BgL_arg2055z00_2341);
																										}
																										BgL_arg2049z00_2337 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg2052z00_2340,
																											BNIL);
																									}
																									BgL_arg2047z00_2335 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg2048z00_2336,
																										BgL_arg2049z00_2337);
																								}
																								BgL_arg2046z00_2334 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg2047z00_2335, BNIL);
																							}
																							BgL_arg2042z00_2331 =
																								MAKE_YOUNG_PAIR(BgL_tmpz00_2302,
																								BgL_arg2046z00_2334);
																						}
																						BgL_arg2039z00_2328 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2041z00_2330,
																							BgL_arg2042z00_2331);
																					}
																					BgL_arg2040z00_2329 =
																						MAKE_YOUNG_PAIR(BgL_tmpz00_2302,
																						BNIL);
																					BgL_arg2036z00_2325 =
																						MAKE_YOUNG_PAIR(BgL_arg2039z00_2328,
																						BgL_arg2040z00_2329);
																				}
																				BgL_arg2033z00_2323 =
																					MAKE_YOUNG_PAIR(BgL_arg2034z00_2324,
																					BgL_arg2036z00_2325);
																			}
																			BgL_arg2031z00_2322 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(39),
																				BgL_arg2033z00_2323);
																		}
																		BgL_exprz00_2368 = BgL_arg2031z00_2322;
																		if (
																			(BGl_za2passza2z00zzengine_paramz00 ==
																				CNST_TABLE_REF(34)))
																			{	/* Object/classgen.scm 353 */
																				BgL_arg2030z00_2321 = BgL_exprz00_2368;
																			}
																		else
																			{	/* Object/classgen.scm 356 */
																				obj_t BgL_arg2081z00_2370;

																				BgL_arg2081z00_2370 =
																					(((BgL_typez00_bglt) COBJECT(
																							((BgL_typez00_bglt)
																								BgL_cz00_15)))->BgL_idz00);
																				{	/* Object/classgen.scm 356 */
																					obj_t BgL_list2082z00_2371;

																					BgL_list2082z00_2371 =
																						MAKE_YOUNG_PAIR(BgL_exprz00_2368,
																						BNIL);
																					BgL_arg2030z00_2321 =
																						BGl_makezd2privatezd2sexpz00zzast_privatez00
																						(CNST_TABLE_REF(35),
																						BgL_arg2081z00_2370,
																						BgL_list2082z00_2371);
																				}
																			}
																	}
																	BgL_arg2017z00_2309 =
																		MAKE_YOUNG_PAIR(BgL_arg2030z00_2321, BNIL);
																}
																BgL_arg2015z00_2307 =
																	MAKE_YOUNG_PAIR(BgL_arg2016z00_2308,
																	BgL_arg2017z00_2309);
															}
															return
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(24),
																BgL_arg2015z00_2307);
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &classgen-widen-expr */
	obj_t BGl_z62classgenzd2widenzd2exprz62zzobject_classgenz00(obj_t
		BgL_envz00_3102, obj_t BgL_cz00_3103, obj_t BgL_oz00_3104)
	{
		{	/* Object/classgen.scm 338 */
			return
				BGl_classgenzd2widenzd2exprz00zzobject_classgenz00(
				((BgL_typez00_bglt) BgL_cz00_3103), BgL_oz00_3104);
		}

	}



/* classgen-widen-anonymous */
	BGL_EXPORTED_DEF obj_t
		BGl_classgenzd2widenzd2anonymousz00zzobject_classgenz00(BgL_typez00_bglt
		BgL_cz00_17)
	{
		{	/* Object/classgen.scm 383 */
			{	/* Object/classgen.scm 384 */
				obj_t BgL_sz00_2375;

				{
					BgL_tclassz00_bglt BgL_auxz00_4221;

					{
						obj_t BgL_auxz00_4222;

						{	/* Object/classgen.scm 384 */
							BgL_objectz00_bglt BgL_tmpz00_4223;

							BgL_tmpz00_4223 = ((BgL_objectz00_bglt) BgL_cz00_17);
							BgL_auxz00_4222 = BGL_OBJECT_WIDENING(BgL_tmpz00_4223);
						}
						BgL_auxz00_4221 = ((BgL_tclassz00_bglt) BgL_auxz00_4222);
					}
					BgL_sz00_2375 =
						(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_4221))->
						BgL_itszd2superzd2);
				}
				{	/* Object/classgen.scm 384 */
					obj_t BgL_sidz00_2376;

					BgL_sidz00_2376 =
						(((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_sz00_2375)))->BgL_idz00);
					{	/* Object/classgen.scm 385 */
						obj_t BgL_cidz00_2377;

						BgL_cidz00_2377 =
							(((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt) BgL_cz00_17)))->BgL_idz00);
						{	/* Object/classgen.scm 386 */
							obj_t BgL_oz00_2378;

							BgL_oz00_2378 =
								BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(40));
							{	/* Object/classgen.scm 387 */
								obj_t BgL_toz00_2379;

								BgL_toz00_2379 =
									BGl_makezd2typedzd2identz00zzast_identz00(BgL_oz00_2378,
									BgL_sidz00_2376);
								{	/* Object/classgen.scm 388 */
									obj_t BgL_lamz00_2380;

									BgL_lamz00_2380 =
										BGl_makezd2typedzd2identz00zzast_identz00(CNST_TABLE_REF
										(12), BgL_cidz00_2377);
									{	/* Object/classgen.scm 389 */

										{	/* Object/classgen.scm 390 */
											obj_t BgL_arg2083z00_2381;

											{	/* Object/classgen.scm 390 */
												obj_t BgL_arg2084z00_2382;
												obj_t BgL_arg2086z00_2383;

												BgL_arg2084z00_2382 =
													MAKE_YOUNG_PAIR(BgL_toz00_2379, BNIL);
												BgL_arg2086z00_2383 =
													MAKE_YOUNG_PAIR
													(BGl_classgenzd2widenzd2exprz00zzobject_classgenz00
													(BgL_cz00_17, BgL_oz00_2378), BNIL);
												BgL_arg2083z00_2381 =
													MAKE_YOUNG_PAIR(BgL_arg2084z00_2382,
													BgL_arg2086z00_2383);
											}
											return
												MAKE_YOUNG_PAIR(BgL_lamz00_2380, BgL_arg2083z00_2381);
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &classgen-widen-anonymous */
	obj_t BGl_z62classgenzd2widenzd2anonymousz62zzobject_classgenz00(obj_t
		BgL_envz00_3105, obj_t BgL_cz00_3106)
	{
		{	/* Object/classgen.scm 383 */
			return
				BGl_classgenzd2widenzd2anonymousz00zzobject_classgenz00(
				((BgL_typez00_bglt) BgL_cz00_3106));
		}

	}



/* classgen-shrink-anonymous */
	BGL_EXPORTED_DEF obj_t
		BGl_classgenzd2shrinkzd2anonymousz00zzobject_classgenz00(BgL_typez00_bglt
		BgL_cz00_18)
	{
		{	/* Object/classgen.scm 395 */
			{	/* Object/classgen.scm 396 */
				obj_t BgL_oz00_2385;

				BgL_oz00_2385 = BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(40));
				{	/* Object/classgen.scm 396 */
					obj_t BgL_toz00_2386;

					{	/* Object/classgen.scm 397 */
						obj_t BgL_arg2096z00_2396;

						BgL_arg2096z00_2396 =
							(((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt) BgL_cz00_18)))->BgL_idz00);
						BgL_toz00_2386 =
							BGl_makezd2typedzd2identz00zzast_identz00(BgL_oz00_2385,
							BgL_arg2096z00_2396);
					}
					{	/* Object/classgen.scm 397 */
						obj_t BgL_superz00_2387;

						{
							BgL_tclassz00_bglt BgL_auxz00_4249;

							{
								obj_t BgL_auxz00_4250;

								{	/* Object/classgen.scm 398 */
									BgL_objectz00_bglt BgL_tmpz00_4251;

									BgL_tmpz00_4251 = ((BgL_objectz00_bglt) BgL_cz00_18);
									BgL_auxz00_4250 = BGL_OBJECT_WIDENING(BgL_tmpz00_4251);
								}
								BgL_auxz00_4249 = ((BgL_tclassz00_bglt) BgL_auxz00_4250);
							}
							BgL_superz00_2387 =
								(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_4249))->
								BgL_itszd2superzd2);
						}
						{	/* Object/classgen.scm 398 */
							obj_t BgL_sidz00_2388;

							BgL_sidz00_2388 =
								(((BgL_typez00_bglt) COBJECT(
										((BgL_typez00_bglt) BgL_superz00_2387)))->BgL_idz00);
							{	/* Object/classgen.scm 399 */

								{	/* Object/classgen.scm 400 */
									obj_t BgL_arg2088z00_2389;
									obj_t BgL_arg2089z00_2390;

									BgL_arg2088z00_2389 =
										BGl_makezd2typedzd2identz00zzast_identz00(CNST_TABLE_REF
										(12), BgL_sidz00_2388);
									{	/* Object/classgen.scm 401 */
										obj_t BgL_arg2090z00_2391;
										obj_t BgL_arg2091z00_2392;

										BgL_arg2090z00_2391 = MAKE_YOUNG_PAIR(BgL_toz00_2386, BNIL);
										{	/* Object/classgen.scm 402 */
											obj_t BgL_arg2093z00_2393;

											{	/* Object/classgen.scm 402 */
												obj_t BgL_arg2094z00_2394;
												obj_t BgL_arg2095z00_2395;

												BgL_arg2094z00_2394 =
													BGl_makezd2typedzd2identz00zzast_identz00
													(CNST_TABLE_REF(41), BgL_sidz00_2388);
												BgL_arg2095z00_2395 =
													MAKE_YOUNG_PAIR(BgL_oz00_2385, BNIL);
												BgL_arg2093z00_2393 =
													MAKE_YOUNG_PAIR(BgL_arg2094z00_2394,
													BgL_arg2095z00_2395);
											}
											BgL_arg2091z00_2392 =
												MAKE_YOUNG_PAIR(BgL_arg2093z00_2393, BNIL);
										}
										BgL_arg2089z00_2390 =
											MAKE_YOUNG_PAIR(BgL_arg2090z00_2391, BgL_arg2091z00_2392);
									}
									return
										MAKE_YOUNG_PAIR(BgL_arg2088z00_2389, BgL_arg2089z00_2390);
								}
							}
						}
					}
				}
			}
		}

	}



/* &classgen-shrink-anonymous */
	obj_t BGl_z62classgenzd2shrinkzd2anonymousz62zzobject_classgenz00(obj_t
		BgL_envz00_3107, obj_t BgL_cz00_3108)
	{
		{	/* Object/classgen.scm 395 */
			return
				BGl_classgenzd2shrinkzd2anonymousz00zzobject_classgenz00(
				((BgL_typez00_bglt) BgL_cz00_3108));
		}

	}



/* classgen-accessors */
	obj_t BGl_classgenzd2accessorszd2zzobject_classgenz00(obj_t BgL_cz00_19)
	{
		{	/* Object/classgen.scm 407 */
			{	/* Object/classgen.scm 408 */
				obj_t BgL_protosz00_2397;
				obj_t BgL_defsz00_2398;

				BgL_protosz00_2397 = BNIL;
				BgL_defsz00_2398 = BNIL;
				{	/* Object/classgen.scm 410 */
					obj_t BgL_g1301z00_2399;

					{
						BgL_tclassz00_bglt BgL_auxz00_4270;

						{
							obj_t BgL_auxz00_4271;

							{	/* Object/classgen.scm 420 */
								BgL_objectz00_bglt BgL_tmpz00_4272;

								BgL_tmpz00_4272 =
									((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_cz00_19));
								BgL_auxz00_4271 = BGL_OBJECT_WIDENING(BgL_tmpz00_4272);
							}
							BgL_auxz00_4270 = ((BgL_tclassz00_bglt) BgL_auxz00_4271);
						}
						BgL_g1301z00_2399 =
							(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_4270))->BgL_slotsz00);
					}
					{
						obj_t BgL_l1299z00_2401;

						BgL_l1299z00_2401 = BgL_g1301z00_2399;
					BgL_zc3z04anonymousza32097ze3z87_2402:
						if (PAIRP(BgL_l1299z00_2401))
							{	/* Object/classgen.scm 420 */
								{	/* Object/classgen.scm 411 */
									obj_t BgL_sz00_2404;

									BgL_sz00_2404 = CAR(BgL_l1299z00_2401);
									{	/* Object/classgen.scm 411 */
										obj_t BgL_pz00_2405;

										BgL_pz00_2405 =
											BGl_classgenzd2slotzd2zzobject_classgenz00(BgL_cz00_19,
											BgL_sz00_2404);
										{	/* Object/classgen.scm 412 */
											obj_t BgL_dz00_2406;

											{	/* Object/classgen.scm 413 */
												obj_t BgL_tmpz00_3015;

												{	/* Object/classgen.scm 413 */
													int BgL_tmpz00_4282;

													BgL_tmpz00_4282 = (int) (1L);
													BgL_tmpz00_3015 = BGL_MVALUES_VAL(BgL_tmpz00_4282);
												}
												{	/* Object/classgen.scm 413 */
													int BgL_tmpz00_4285;

													BgL_tmpz00_4285 = (int) (1L);
													BGL_MVALUES_VAL_SET(BgL_tmpz00_4285, BUNSPEC);
												}
												BgL_dz00_2406 = BgL_tmpz00_3015;
											}
											if (
												(((BgL_slotz00_bglt) COBJECT(
															((BgL_slotz00_bglt) BgL_sz00_2404)))->
													BgL_readzd2onlyzf3z21))
												{	/* Object/classgen.scm 413 */
													{	/* Object/classgen.scm 415 */
														obj_t BgL_arg2100z00_2408;

														BgL_arg2100z00_2408 = CAR(((obj_t) BgL_pz00_2405));
														BgL_protosz00_2397 =
															MAKE_YOUNG_PAIR(BgL_arg2100z00_2408,
															BgL_protosz00_2397);
													}
													BgL_defsz00_2398 =
														BGl_appendzd221011zd2zzobject_classgenz00
														(BgL_dz00_2406, BgL_defsz00_2398);
												}
											else
												{	/* Object/classgen.scm 413 */
													BgL_protosz00_2397 =
														BGl_appendzd221011zd2zzobject_classgenz00
														(BgL_pz00_2405, BgL_protosz00_2397);
													BgL_defsz00_2398 =
														BGl_appendzd221011zd2zzobject_classgenz00
														(BgL_dz00_2406, BgL_defsz00_2398);
												}
										}
									}
								}
								{
									obj_t BgL_l1299z00_4297;

									BgL_l1299z00_4297 = CDR(BgL_l1299z00_2401);
									BgL_l1299z00_2401 = BgL_l1299z00_4297;
									goto BgL_zc3z04anonymousza32097ze3z87_2402;
								}
							}
						else
							{	/* Object/classgen.scm 420 */
								((bool_t) 1);
							}
					}
				}
				{	/* Object/classgen.scm 421 */
					obj_t BgL_val0_1302z00_2411;
					obj_t BgL_val1_1303z00_2412;

					BgL_val0_1302z00_2411 = BgL_protosz00_2397;
					BgL_val1_1303z00_2412 = BgL_defsz00_2398;
					{	/* Object/classgen.scm 421 */
						int BgL_tmpz00_4299;

						BgL_tmpz00_4299 = (int) (2L);
						BGL_MVALUES_NUMBER_SET(BgL_tmpz00_4299);
					}
					{	/* Object/classgen.scm 421 */
						int BgL_tmpz00_4302;

						BgL_tmpz00_4302 = (int) (1L);
						BGL_MVALUES_VAL_SET(BgL_tmpz00_4302, BgL_val1_1303z00_2412);
					}
					return BgL_val0_1302z00_2411;
				}
			}
		}

	}



/* classgen-slot */
	obj_t BGl_classgenzd2slotzd2zzobject_classgenz00(obj_t BgL_cz00_20,
		obj_t BgL_sz00_21)
	{
		{	/* Object/classgen.scm 426 */
			{
				obj_t BgL_sz00_2477;
				obj_t BgL_sz00_2460;
				obj_t BgL_sz00_2444;
				obj_t BgL_sz00_2430;

				{	/* Object/classgen.scm 460 */
					obj_t BgL_val0_1304z00_2417;
					obj_t BgL_val1_1305z00_2418;

					{	/* Object/classgen.scm 460 */
						obj_t BgL_arg2102z00_2419;
						obj_t BgL_arg2103z00_2420;

						BgL_sz00_2430 = BgL_sz00_21;
						{	/* Object/classgen.scm 429 */
							obj_t BgL_tidz00_2432;

							BgL_tidz00_2432 =
								(((BgL_typez00_bglt) COBJECT(
										((BgL_typez00_bglt) BgL_cz00_20)))->BgL_idz00);
							{	/* Object/classgen.scm 429 */
								obj_t BgL_idz00_2433;

								{	/* Object/classgen.scm 430 */
									obj_t BgL_arg2117z00_2440;

									BgL_arg2117z00_2440 =
										(((BgL_slotz00_bglt) COBJECT(
												((BgL_slotz00_bglt) BgL_sz00_2430)))->BgL_idz00);
									{	/* Object/classgen.scm 430 */
										obj_t BgL_list2118z00_2441;

										{	/* Object/classgen.scm 430 */
											obj_t BgL_arg2119z00_2442;

											{	/* Object/classgen.scm 430 */
												obj_t BgL_arg2120z00_2443;

												BgL_arg2120z00_2443 =
													MAKE_YOUNG_PAIR(BgL_arg2117z00_2440, BNIL);
												BgL_arg2119z00_2442 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(42),
													BgL_arg2120z00_2443);
											}
											BgL_list2118z00_2441 =
												MAKE_YOUNG_PAIR(BgL_tidz00_2432, BgL_arg2119z00_2442);
										}
										BgL_idz00_2433 =
											BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
											(BgL_list2118z00_2441);
									}
								}
								{	/* Object/classgen.scm 430 */
									obj_t BgL_gidz00_2434;

									{	/* Object/classgen.scm 431 */
										obj_t BgL_arg2115z00_2438;

										BgL_arg2115z00_2438 =
											(((BgL_typez00_bglt) COBJECT(
													((BgL_typez00_bglt)
														(((BgL_slotz00_bglt) COBJECT(
																	((BgL_slotz00_bglt) BgL_sz00_2430)))->
															BgL_typez00))))->BgL_idz00);
										BgL_gidz00_2434 =
											BGl_makezd2typedzd2identz00zzast_identz00(BgL_idz00_2433,
											BgL_arg2115z00_2438);
									}
									{	/* Object/classgen.scm 431 */
										obj_t BgL_oz00_2435;

										BgL_oz00_2435 =
											BGl_makezd2typedzd2formalz00zzast_identz00
											(BgL_tidz00_2432);
										{	/* Object/classgen.scm 432 */

											{	/* Object/classgen.scm 433 */
												obj_t BgL_arg2113z00_2436;

												{	/* Object/classgen.scm 433 */
													obj_t BgL_arg2114z00_2437;

													BgL_arg2114z00_2437 =
														MAKE_YOUNG_PAIR(BgL_oz00_2435, BNIL);
													BgL_arg2113z00_2436 =
														MAKE_YOUNG_PAIR(BgL_gidz00_2434,
														BgL_arg2114z00_2437);
												}
												BgL_arg2102z00_2419 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
													BgL_arg2113z00_2436);
											}
										}
									}
								}
							}
						}
						BgL_sz00_2444 = BgL_sz00_21;
						{	/* Object/classgen.scm 436 */
							obj_t BgL_tidz00_2446;

							BgL_tidz00_2446 =
								(((BgL_typez00_bglt) COBJECT(
										((BgL_typez00_bglt) BgL_cz00_20)))->BgL_idz00);
							{	/* Object/classgen.scm 436 */
								obj_t BgL_idz00_2447;

								{	/* Object/classgen.scm 437 */
									obj_t BgL_arg2127z00_2455;

									BgL_arg2127z00_2455 =
										(((BgL_slotz00_bglt) COBJECT(
												((BgL_slotz00_bglt) BgL_sz00_2444)))->BgL_idz00);
									{	/* Object/classgen.scm 437 */
										obj_t BgL_list2128z00_2456;

										{	/* Object/classgen.scm 437 */
											obj_t BgL_arg2129z00_2457;

											{	/* Object/classgen.scm 437 */
												obj_t BgL_arg2130z00_2458;

												{	/* Object/classgen.scm 437 */
													obj_t BgL_arg2131z00_2459;

													BgL_arg2131z00_2459 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(43), BNIL);
													BgL_arg2130z00_2458 =
														MAKE_YOUNG_PAIR(BgL_arg2127z00_2455,
														BgL_arg2131z00_2459);
												}
												BgL_arg2129z00_2457 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(42),
													BgL_arg2130z00_2458);
											}
											BgL_list2128z00_2456 =
												MAKE_YOUNG_PAIR(BgL_tidz00_2446, BgL_arg2129z00_2457);
										}
										BgL_idz00_2447 =
											BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
											(BgL_list2128z00_2456);
									}
								}
								{	/* Object/classgen.scm 437 */
									obj_t BgL_oz00_2448;

									BgL_oz00_2448 =
										BGl_makezd2typedzd2formalz00zzast_identz00(BgL_tidz00_2446);
									{	/* Object/classgen.scm 438 */
										obj_t BgL_vz00_2449;

										{	/* Object/classgen.scm 439 */
											obj_t BgL_arg2125z00_2453;

											BgL_arg2125z00_2453 =
												(((BgL_typez00_bglt) COBJECT(
														((BgL_typez00_bglt)
															(((BgL_slotz00_bglt) COBJECT(
																		((BgL_slotz00_bglt) BgL_sz00_2444)))->
																BgL_typez00))))->BgL_idz00);
											BgL_vz00_2449 =
												BGl_makezd2typedzd2formalz00zzast_identz00
												(BgL_arg2125z00_2453);
										}
										{	/* Object/classgen.scm 439 */

											{	/* Object/classgen.scm 440 */
												obj_t BgL_arg2122z00_2450;

												{	/* Object/classgen.scm 440 */
													obj_t BgL_arg2123z00_2451;

													{	/* Object/classgen.scm 440 */
														obj_t BgL_arg2124z00_2452;

														BgL_arg2124z00_2452 =
															MAKE_YOUNG_PAIR(BgL_vz00_2449, BNIL);
														BgL_arg2123z00_2451 =
															MAKE_YOUNG_PAIR(BgL_oz00_2448,
															BgL_arg2124z00_2452);
													}
													BgL_arg2122z00_2450 =
														MAKE_YOUNG_PAIR(BgL_idz00_2447,
														BgL_arg2123z00_2451);
												}
												BgL_arg2103z00_2420 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
													BgL_arg2122z00_2450);
											}
										}
									}
								}
							}
						}
						{	/* Object/classgen.scm 460 */
							obj_t BgL_list2104z00_2421;

							{	/* Object/classgen.scm 460 */
								obj_t BgL_arg2105z00_2422;

								BgL_arg2105z00_2422 =
									MAKE_YOUNG_PAIR(BgL_arg2103z00_2420, BNIL);
								BgL_list2104z00_2421 =
									MAKE_YOUNG_PAIR(BgL_arg2102z00_2419, BgL_arg2105z00_2422);
							}
							BgL_val0_1304z00_2417 = BgL_list2104z00_2421;
						}
					}
					{	/* Object/classgen.scm 463 */
						obj_t BgL_arg2106z00_2423;
						obj_t BgL_arg2107z00_2424;

						BgL_sz00_2460 = BgL_sz00_21;
						{	/* Object/classgen.scm 443 */
							obj_t BgL_tidz00_2462;

							BgL_tidz00_2462 =
								(((BgL_typez00_bglt) COBJECT(
										((BgL_typez00_bglt) BgL_cz00_20)))->BgL_idz00);
							{	/* Object/classgen.scm 443 */
								obj_t BgL_sidz00_2463;

								BgL_sidz00_2463 =
									(((BgL_slotz00_bglt) COBJECT(
											((BgL_slotz00_bglt) BgL_sz00_2460)))->BgL_idz00);
								{	/* Object/classgen.scm 444 */
									obj_t BgL_oz00_2464;

									BgL_oz00_2464 =
										BGl_makezd2typedzd2identz00zzast_identz00(CNST_TABLE_REF
										(40), BgL_tidz00_2462);
									{	/* Object/classgen.scm 445 */
										obj_t BgL_idz00_2465;

										{	/* Object/classgen.scm 446 */
											obj_t BgL_list2140z00_2474;

											{	/* Object/classgen.scm 446 */
												obj_t BgL_arg2141z00_2475;

												{	/* Object/classgen.scm 446 */
													obj_t BgL_arg2142z00_2476;

													BgL_arg2142z00_2476 =
														MAKE_YOUNG_PAIR(BgL_sidz00_2463, BNIL);
													BgL_arg2141z00_2475 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(42),
														BgL_arg2142z00_2476);
												}
												BgL_list2140z00_2474 =
													MAKE_YOUNG_PAIR(BgL_tidz00_2462, BgL_arg2141z00_2475);
											}
											BgL_idz00_2465 =
												BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
												(BgL_list2140z00_2474);
										}
										{	/* Object/classgen.scm 446 */
											obj_t BgL_gidz00_2466;

											{	/* Object/classgen.scm 447 */
												obj_t BgL_arg2138z00_2472;

												BgL_arg2138z00_2472 =
													(((BgL_typez00_bglt) COBJECT(
															((BgL_typez00_bglt)
																(((BgL_slotz00_bglt) COBJECT(
																			((BgL_slotz00_bglt) BgL_sz00_2460)))->
																	BgL_typez00))))->BgL_idz00);
												BgL_gidz00_2466 =
													BGl_makezd2typedzd2identz00zzast_identz00
													(BgL_idz00_2465, BgL_arg2138z00_2472);
											}
											{	/* Object/classgen.scm 447 */

												{	/* Object/classgen.scm 448 */
													obj_t BgL_arg2133z00_2467;

													{	/* Object/classgen.scm 448 */
														obj_t BgL_arg2134z00_2468;
														obj_t BgL_arg2135z00_2469;

														{	/* Object/classgen.scm 448 */
															obj_t BgL_arg2136z00_2470;

															BgL_arg2136z00_2470 =
																MAKE_YOUNG_PAIR(BgL_oz00_2464, BNIL);
															BgL_arg2134z00_2468 =
																MAKE_YOUNG_PAIR(BgL_gidz00_2466,
																BgL_arg2136z00_2470);
														}
														BgL_arg2135z00_2469 =
															MAKE_YOUNG_PAIR
															(BGl_fieldzd2accesszd2zzast_objectz00
															(CNST_TABLE_REF(40), BgL_sidz00_2463, BTRUE),
															BNIL);
														BgL_arg2133z00_2467 =
															MAKE_YOUNG_PAIR(BgL_arg2134z00_2468,
															BgL_arg2135z00_2469);
													}
													BgL_arg2106z00_2423 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(7),
														BgL_arg2133z00_2467);
												}
											}
										}
									}
								}
							}
						}
						{	/* Object/classgen.scm 464 */
							bool_t BgL_test2365z00_4372;

							if (
								(((BgL_slotz00_bglt) COBJECT(
											((BgL_slotz00_bglt) BgL_sz00_21)))->
									BgL_readzd2onlyzf3z21))
								{	/* Object/classgen.scm 464 */
									BgL_test2365z00_4372 =
										BGl_slotzd2virtualzf3z21zzobject_slotsz00(
										((BgL_slotz00_bglt) BgL_sz00_21));
								}
							else
								{	/* Object/classgen.scm 464 */
									BgL_test2365z00_4372 = ((bool_t) 0);
								}
							if (BgL_test2365z00_4372)
								{	/* Object/classgen.scm 464 */
									BgL_arg2107z00_2424 = BFALSE;
								}
							else
								{	/* Object/classgen.scm 464 */
									BgL_sz00_2477 = BgL_sz00_21;
									{	/* Object/classgen.scm 452 */
										obj_t BgL_tidz00_2479;

										BgL_tidz00_2479 =
											(((BgL_typez00_bglt) COBJECT(
													((BgL_typez00_bglt) BgL_cz00_20)))->BgL_idz00);
										{	/* Object/classgen.scm 452 */
											obj_t BgL_sidz00_2480;

											BgL_sidz00_2480 =
												(((BgL_slotz00_bglt) COBJECT(
														((BgL_slotz00_bglt) BgL_sz00_2477)))->BgL_idz00);
											{	/* Object/classgen.scm 453 */
												obj_t BgL_oz00_2481;

												BgL_oz00_2481 =
													BGl_makezd2typedzd2identz00zzast_identz00
													(CNST_TABLE_REF(40), BgL_tidz00_2479);
												{	/* Object/classgen.scm 454 */
													obj_t BgL_idz00_2482;

													{	/* Object/classgen.scm 455 */
														obj_t BgL_list2156z00_2495;

														{	/* Object/classgen.scm 455 */
															obj_t BgL_arg2157z00_2496;

															{	/* Object/classgen.scm 455 */
																obj_t BgL_arg2158z00_2497;

																{	/* Object/classgen.scm 455 */
																	obj_t BgL_arg2159z00_2498;

																	BgL_arg2159z00_2498 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(43), BNIL);
																	BgL_arg2158z00_2497 =
																		MAKE_YOUNG_PAIR(BgL_sidz00_2480,
																		BgL_arg2159z00_2498);
																}
																BgL_arg2157z00_2496 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(42),
																	BgL_arg2158z00_2497);
															}
															BgL_list2156z00_2495 =
																MAKE_YOUNG_PAIR(BgL_tidz00_2479,
																BgL_arg2157z00_2496);
														}
														BgL_idz00_2482 =
															BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
															(BgL_list2156z00_2495);
													}
													{	/* Object/classgen.scm 455 */
														obj_t BgL_vz00_2483;

														{	/* Object/classgen.scm 456 */
															obj_t BgL_arg2154z00_2493;

															BgL_arg2154z00_2493 =
																(((BgL_typez00_bglt) COBJECT(
																		((BgL_typez00_bglt)
																			(((BgL_slotz00_bglt) COBJECT(
																						((BgL_slotz00_bglt)
																							BgL_sz00_2477)))->
																				BgL_typez00))))->BgL_idz00);
															BgL_vz00_2483 =
																BGl_makezd2typedzd2identz00zzast_identz00
																(CNST_TABLE_REF(44), BgL_arg2154z00_2493);
														}
														{	/* Object/classgen.scm 456 */

															{	/* Object/classgen.scm 457 */
																obj_t BgL_arg2144z00_2484;

																{	/* Object/classgen.scm 457 */
																	obj_t BgL_arg2145z00_2485;
																	obj_t BgL_arg2146z00_2486;

																	{	/* Object/classgen.scm 457 */
																		obj_t BgL_arg2147z00_2487;

																		{	/* Object/classgen.scm 457 */
																			obj_t BgL_arg2148z00_2488;

																			BgL_arg2148z00_2488 =
																				MAKE_YOUNG_PAIR(BgL_vz00_2483, BNIL);
																			BgL_arg2147z00_2487 =
																				MAKE_YOUNG_PAIR(BgL_oz00_2481,
																				BgL_arg2148z00_2488);
																		}
																		BgL_arg2145z00_2485 =
																			MAKE_YOUNG_PAIR(BgL_idz00_2482,
																			BgL_arg2147z00_2487);
																	}
																	{	/* Object/classgen.scm 458 */
																		obj_t BgL_arg2149z00_2489;

																		{	/* Object/classgen.scm 458 */
																			obj_t BgL_arg2150z00_2490;

																			{	/* Object/classgen.scm 458 */
																				obj_t BgL_arg2151z00_2491;
																				obj_t BgL_arg2152z00_2492;

																				BgL_arg2151z00_2491 =
																					BGl_fieldzd2accesszd2zzast_objectz00
																					(CNST_TABLE_REF(40), BgL_sidz00_2480,
																					BTRUE);
																				BgL_arg2152z00_2492 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(44),
																					BNIL);
																				BgL_arg2150z00_2490 =
																					MAKE_YOUNG_PAIR(BgL_arg2151z00_2491,
																					BgL_arg2152z00_2492);
																			}
																			BgL_arg2149z00_2489 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(19),
																				BgL_arg2150z00_2490);
																		}
																		BgL_arg2146z00_2486 =
																			MAKE_YOUNG_PAIR(BgL_arg2149z00_2489,
																			BNIL);
																	}
																	BgL_arg2144z00_2484 =
																		MAKE_YOUNG_PAIR(BgL_arg2145z00_2485,
																		BgL_arg2146z00_2486);
																}
																BgL_arg2107z00_2424 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(7),
																	BgL_arg2144z00_2484);
															}
														}
													}
												}
											}
										}
									}
								}
						}
						{	/* Object/classgen.scm 463 */
							obj_t BgL_list2108z00_2425;

							{	/* Object/classgen.scm 463 */
								obj_t BgL_arg2109z00_2426;

								BgL_arg2109z00_2426 =
									MAKE_YOUNG_PAIR(BgL_arg2107z00_2424, BNIL);
								BgL_list2108z00_2425 =
									MAKE_YOUNG_PAIR(BgL_arg2106z00_2423, BgL_arg2109z00_2426);
							}
							BgL_val1_1305z00_2418 = BgL_list2108z00_2425;
						}
					}
					{	/* Object/classgen.scm 460 */
						int BgL_tmpz00_4413;

						BgL_tmpz00_4413 = (int) (2L);
						BGL_MVALUES_NUMBER_SET(BgL_tmpz00_4413);
					}
					{	/* Object/classgen.scm 460 */
						int BgL_tmpz00_4416;

						BgL_tmpz00_4416 = (int) (1L);
						BGL_MVALUES_VAL_SET(BgL_tmpz00_4416, BgL_val1_1305z00_2418);
					}
					return BgL_val0_1304z00_2417;
				}
			}
		}

	}



/* classgen-slot-anonymous */
	BGL_EXPORTED_DEF obj_t
		BGl_classgenzd2slotzd2anonymousz00zzobject_classgenz00(BgL_typez00_bglt
		BgL_classz00_22, BgL_slotz00_bglt BgL_sz00_23)
	{
		{	/* Object/classgen.scm 470 */
			{	/* Object/classgen.scm 471 */
				obj_t BgL__z00_2503;

				BgL__z00_2503 =
					BGl_classgenzd2slotzd2zzobject_classgenz00(
					((obj_t) BgL_classz00_22), ((obj_t) BgL_sz00_23));
				{	/* Object/classgen.scm 472 */
					obj_t BgL_dz00_2504;

					{	/* Object/classgen.scm 473 */
						obj_t BgL_tmpz00_3038;

						{	/* Object/classgen.scm 473 */
							int BgL_tmpz00_4422;

							BgL_tmpz00_4422 = (int) (1L);
							BgL_tmpz00_3038 = BGL_MVALUES_VAL(BgL_tmpz00_4422);
						}
						{	/* Object/classgen.scm 473 */
							int BgL_tmpz00_4425;

							BgL_tmpz00_4425 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_4425, BUNSPEC);
						}
						BgL_dz00_2504 = BgL_tmpz00_3038;
					}
					{	/* Object/classgen.scm 473 */
						obj_t BgL_fun1311z00_2505;

						{	/* Object/classgen.scm 473 */
							obj_t BgL_zc3z04anonymousza32166ze3z87_3109;

							{
								int BgL_tmpz00_4428;

								BgL_tmpz00_4428 = (int) (0L);
								BgL_zc3z04anonymousza32166ze3z87_3109 =
									MAKE_EL_PROCEDURE(BgL_tmpz00_4428);
							}
							BgL_fun1311z00_2505 = BgL_zc3z04anonymousza32166ze3z87_3109;
						}
						if (NULLP(BgL_dz00_2504))
							{	/* Object/classgen.scm 473 */
								return BNIL;
							}
						else
							{	/* Object/classgen.scm 473 */
								obj_t BgL_head1308z00_2508;

								BgL_head1308z00_2508 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1306z00_2510;
									obj_t BgL_tail1309z00_2511;

									BgL_l1306z00_2510 = BgL_dz00_2504;
									BgL_tail1309z00_2511 = BgL_head1308z00_2508;
								BgL_zc3z04anonymousza32161ze3z87_2512:
									if (NULLP(BgL_l1306z00_2510))
										{	/* Object/classgen.scm 473 */
											return CDR(BgL_head1308z00_2508);
										}
									else
										{	/* Object/classgen.scm 473 */
											obj_t BgL_newtail1310z00_2514;

											{	/* Object/classgen.scm 473 */
												obj_t BgL_arg2164z00_2516;

												{	/* Object/classgen.scm 473 */
													obj_t BgL_arg2165z00_2517;

													BgL_arg2165z00_2517 =
														CAR(((obj_t) BgL_l1306z00_2510));
													BgL_arg2164z00_2516 =
														BGl_z62zc3z04anonymousza32166ze3ze5zzobject_classgenz00
														(BgL_fun1311z00_2505, BgL_arg2165z00_2517);
												}
												BgL_newtail1310z00_2514 =
													MAKE_YOUNG_PAIR(BgL_arg2164z00_2516, BNIL);
											}
											SET_CDR(BgL_tail1309z00_2511, BgL_newtail1310z00_2514);
											{	/* Object/classgen.scm 473 */
												obj_t BgL_arg2163z00_2515;

												BgL_arg2163z00_2515 = CDR(((obj_t) BgL_l1306z00_2510));
												{
													obj_t BgL_tail1309z00_4448;
													obj_t BgL_l1306z00_4447;

													BgL_l1306z00_4447 = BgL_arg2163z00_2515;
													BgL_tail1309z00_4448 = BgL_newtail1310z00_2514;
													BgL_tail1309z00_2511 = BgL_tail1309z00_4448;
													BgL_l1306z00_2510 = BgL_l1306z00_4447;
													goto BgL_zc3z04anonymousza32161ze3z87_2512;
												}
											}
										}
								}
							}
					}
				}
			}
		}

	}



/* &classgen-slot-anonymous */
	obj_t BGl_z62classgenzd2slotzd2anonymousz62zzobject_classgenz00(obj_t
		BgL_envz00_3110, obj_t BgL_classz00_3111, obj_t BgL_sz00_3112)
	{
		{	/* Object/classgen.scm 470 */
			return
				BGl_classgenzd2slotzd2anonymousz00zzobject_classgenz00(
				((BgL_typez00_bglt) BgL_classz00_3111),
				((BgL_slotz00_bglt) BgL_sz00_3112));
		}

	}



/* &<@anonymous:2166> */
	obj_t BGl_z62zc3z04anonymousza32166ze3ze5zzobject_classgenz00(obj_t
		BgL_envz00_3113, obj_t BgL_ezd2199zd2_3114)
	{
		{	/* Object/classgen.scm 473 */
			{
				obj_t BgL_idz00_3130;
				obj_t BgL_argsz00_3131;
				obj_t BgL_bodyz00_3132;

				if (PAIRP(BgL_ezd2199zd2_3114))
					{	/* Object/classgen.scm 473 */
						obj_t BgL_cdrzd2207zd2_3138;

						BgL_cdrzd2207zd2_3138 = CDR(BgL_ezd2199zd2_3114);
						if (PAIRP(BgL_cdrzd2207zd2_3138))
							{	/* Object/classgen.scm 473 */
								obj_t BgL_carzd2211zd2_3139;
								obj_t BgL_cdrzd2212zd2_3140;

								BgL_carzd2211zd2_3139 = CAR(BgL_cdrzd2207zd2_3138);
								BgL_cdrzd2212zd2_3140 = CDR(BgL_cdrzd2207zd2_3138);
								if (PAIRP(BgL_carzd2211zd2_3139))
									{	/* Object/classgen.scm 473 */
										if (PAIRP(BgL_cdrzd2212zd2_3140))
											{	/* Object/classgen.scm 473 */
												if (NULLP(CDR(BgL_cdrzd2212zd2_3140)))
													{	/* Object/classgen.scm 473 */
														BgL_idz00_3130 = CAR(BgL_carzd2211zd2_3139);
														BgL_argsz00_3131 = CDR(BgL_carzd2211zd2_3139);
														BgL_bodyz00_3132 = CAR(BgL_cdrzd2212zd2_3140);
														{	/* Object/classgen.scm 475 */
															obj_t BgL_pidz00_3133;

															BgL_pidz00_3133 =
																BGl_parsezd2idzd2zzast_identz00(BgL_idz00_3130,
																BFALSE);
															{	/* Object/classgen.scm 475 */
																obj_t BgL_tlamz00_3134;

																{	/* Object/classgen.scm 476 */
																	obj_t BgL_arg2179z00_3135;

																	BgL_arg2179z00_3135 =
																		(((BgL_typez00_bglt) COBJECT(
																				((BgL_typez00_bglt)
																					CDR(BgL_pidz00_3133))))->BgL_idz00);
																	BgL_tlamz00_3134 =
																		BGl_makezd2typedzd2identz00zzast_identz00
																		(CNST_TABLE_REF(12), BgL_arg2179z00_3135);
																}
																{	/* Object/classgen.scm 476 */

																	{	/* Object/classgen.scm 477 */
																		obj_t BgL_arg2177z00_3136;

																		{	/* Object/classgen.scm 477 */
																			obj_t BgL_arg2178z00_3137;

																			BgL_arg2178z00_3137 =
																				MAKE_YOUNG_PAIR(BgL_bodyz00_3132, BNIL);
																			BgL_arg2177z00_3136 =
																				MAKE_YOUNG_PAIR(BgL_argsz00_3131,
																				BgL_arg2178z00_3137);
																		}
																		return
																			MAKE_YOUNG_PAIR(BgL_tlamz00_3134,
																			BgL_arg2177z00_3136);
																	}
																}
															}
														}
													}
												else
													{	/* Object/classgen.scm 473 */
														return BFALSE;
													}
											}
										else
											{	/* Object/classgen.scm 473 */
												return BFALSE;
											}
									}
								else
									{	/* Object/classgen.scm 473 */
										return BFALSE;
									}
							}
						else
							{	/* Object/classgen.scm 473 */
								return BFALSE;
							}
					}
				else
					{	/* Object/classgen.scm 473 */
						return BFALSE;
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzobject_classgenz00(void)
	{
		{	/* Object/classgen.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzobject_classgenz00(void)
	{
		{	/* Object/classgen.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzobject_classgenz00(void)
	{
		{	/* Object/classgen.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzobject_classgenz00(void)
	{
		{	/* Object/classgen.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string2269z00zzobject_classgenz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2269z00zzobject_classgenz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string2269z00zzobject_classgenz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2269z00zzobject_classgenz00));
			BGl_modulezd2initializa7ationz75zztype_toolsz00(453414928L,
				BSTRING_TO_STRING(BGl_string2269z00zzobject_classgenz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2269z00zzobject_classgenz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string2269z00zzobject_classgenz00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string2269z00zzobject_classgenz00));
			BGl_modulezd2initializa7ationz75zzobject_nilz00(168473056L,
				BSTRING_TO_STRING(BGl_string2269z00zzobject_classgenz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2269z00zzobject_classgenz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string2269z00zzobject_classgenz00));
			BGl_modulezd2initializa7ationz75zzast_privatez00(135263837L,
				BSTRING_TO_STRING(BGl_string2269z00zzobject_classgenz00));
			BGl_modulezd2initializa7ationz75zzast_objectz00(520121776L,
				BSTRING_TO_STRING(BGl_string2269z00zzobject_classgenz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2269z00zzobject_classgenz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2269z00zzobject_classgenz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string2269z00zzobject_classgenz00));
			BGl_modulezd2initializa7ationz75zzwrite_schemez00(305499406L,
				BSTRING_TO_STRING(BGl_string2269z00zzobject_classgenz00));
			return
				BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2269z00zzobject_classgenz00));
		}

	}

#ifdef __cplusplus
}
#endif
