/*===========================================================================*/
/*   (Object/getter.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Object/getter.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_OBJECT_GETTER_TYPE_DEFINITIONS
#define BGL_OBJECT_GETTER_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_jclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		obj_t BgL_packagez00;
	}                *BgL_jclassz00_bglt;

	typedef struct BgL_slotz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_srcz00;
		obj_t BgL_classzd2ownerzd2;
		long BgL_indexz00;
		obj_t BgL_typez00;
		bool_t BgL_readzd2onlyzf3z21;
		obj_t BgL_defaultzd2valuezd2;
		obj_t BgL_virtualzd2numzd2;
		bool_t BgL_virtualzd2overridezd2;
		obj_t BgL_getterz00;
		obj_t BgL_setterz00;
		obj_t BgL_userzd2infozd2;
	}              *BgL_slotz00_bglt;


#endif													// BGL_OBJECT_GETTER_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t
		BGl_genzd2javazd2classzd2slotszd2accessz12z12zzobject_getterz00
		(BgL_typez00_bglt, obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzobject_getterz00 = BUNSPEC;
	extern obj_t BGl_makezd2typedzd2identz00zzast_identz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_epairifyza2za2zztools_miscz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_genericzd2initzd2zzobject_getterz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzobject_getterz00(void);
	BGL_IMPORT obj_t BGl_appendz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t
		BGl_genzd2javazd2classzd2slotzd2accessze70ze7zzobject_getterz00(obj_t,
		BgL_typez00_bglt, BgL_typez00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	extern obj_t BGl_slotz00zzobject_slotsz00;
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzobject_getterz00(obj_t, obj_t);
	extern obj_t BGl_makezd2typedzd2formalz00zzast_identz00(obj_t);
	static obj_t BGl_methodzd2initzd2zzobject_getterz00(void);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	static obj_t
		BGl_z62genzd2javazd2classzd2slotszd2accessz12z70zzobject_getterz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_slotzd2directzd2refz00zzobject_getterz00(obj_t,
		BgL_typez00_bglt, obj_t, bool_t, obj_t);
	extern obj_t BGl_producezd2modulezd2clausez12z12zzmodule_modulez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzobject_getterz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_impusez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_epsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	static obj_t BGl_cnstzd2initzd2zzobject_getterz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzobject_getterz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzobject_getterz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzobject_getterz00(void);
	extern obj_t BGl_makezd2classzd2refz00zzobject_toolsz00(BgL_typez00_bglt,
		BgL_slotz00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	extern obj_t BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00(obj_t);
	static obj_t BGl_slotzd2directzd2setz12z12zzobject_getterz00(obj_t,
		BgL_typez00_bglt, obj_t, bool_t, obj_t);
	extern obj_t BGl_makezd2classzd2setz12z12zzobject_toolsz00(BgL_typez00_bglt,
		BgL_slotz00_bglt, obj_t, obj_t);
	extern obj_t BGl_addzd2macrozd2aliasz12z12zzexpand_epsz00(obj_t, obj_t);
	static obj_t __cnst[14];


	   
		 
		DEFINE_STRING(BGl_string1490z00zzobject_getterz00,
		BgL_bgl_string1490za700za7za7o1503za7, "object_getter", 13);
	      DEFINE_STRING(BGl_string1491z00zzobject_getterz00,
		BgL_bgl_string1491za700za7za7o1504za7,
		"write val ::obj -set! define-inline pragma side-effect-free no-cfa-top effect read static inline obj - ",
		103);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genzd2javazd2classzd2slotszd2accessz12zd2envzc0zzobject_getterz00,
		BgL_bgl_za762genza7d2javaza7d21505za7,
		BGl_z62genzd2javazd2classzd2slotszd2accessz12z70zzobject_getterz00, 0L,
		BUNSPEC, 3);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzobject_getterz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzobject_getterz00(long
		BgL_checksumz00_1065, char *BgL_fromz00_1066)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzobject_getterz00))
				{
					BGl_requirezd2initializa7ationz75zzobject_getterz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzobject_getterz00();
					BGl_libraryzd2moduleszd2initz00zzobject_getterz00();
					BGl_cnstzd2initzd2zzobject_getterz00();
					BGl_importedzd2moduleszd2initz00zzobject_getterz00();
					return BGl_methodzd2initzd2zzobject_getterz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzobject_getterz00(void)
	{
		{	/* Object/getter.scm 18 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "object_getter");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"object_getter");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"object_getter");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"object_getter");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "object_getter");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"object_getter");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"object_getter");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"object_getter");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"object_getter");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzobject_getterz00(void)
	{
		{	/* Object/getter.scm 18 */
			{	/* Object/getter.scm 18 */
				obj_t BgL_cportz00_1054;

				{	/* Object/getter.scm 18 */
					obj_t BgL_stringz00_1061;

					BgL_stringz00_1061 = BGl_string1491z00zzobject_getterz00;
					{	/* Object/getter.scm 18 */
						obj_t BgL_startz00_1062;

						BgL_startz00_1062 = BINT(0L);
						{	/* Object/getter.scm 18 */
							obj_t BgL_endz00_1063;

							BgL_endz00_1063 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1061)));
							{	/* Object/getter.scm 18 */

								BgL_cportz00_1054 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1061, BgL_startz00_1062, BgL_endz00_1063);
				}}}}
				{
					long BgL_iz00_1055;

					BgL_iz00_1055 = 13L;
				BgL_loopz00_1056:
					if ((BgL_iz00_1055 == -1L))
						{	/* Object/getter.scm 18 */
							return BUNSPEC;
						}
					else
						{	/* Object/getter.scm 18 */
							{	/* Object/getter.scm 18 */
								obj_t BgL_arg1502z00_1057;

								{	/* Object/getter.scm 18 */

									{	/* Object/getter.scm 18 */
										obj_t BgL_locationz00_1059;

										BgL_locationz00_1059 = BBOOL(((bool_t) 0));
										{	/* Object/getter.scm 18 */

											BgL_arg1502z00_1057 =
												BGl_readz00zz__readerz00(BgL_cportz00_1054,
												BgL_locationz00_1059);
										}
									}
								}
								{	/* Object/getter.scm 18 */
									int BgL_tmpz00_1093;

									BgL_tmpz00_1093 = (int) (BgL_iz00_1055);
									CNST_TABLE_SET(BgL_tmpz00_1093, BgL_arg1502z00_1057);
							}}
							{	/* Object/getter.scm 18 */
								int BgL_auxz00_1060;

								BgL_auxz00_1060 = (int) ((BgL_iz00_1055 - 1L));
								{
									long BgL_iz00_1098;

									BgL_iz00_1098 = (long) (BgL_auxz00_1060);
									BgL_iz00_1055 = BgL_iz00_1098;
									goto BgL_loopz00_1056;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzobject_getterz00(void)
	{
		{	/* Object/getter.scm 18 */
			return bgl_gc_roots_register();
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzobject_getterz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_714;

				BgL_headz00_714 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_715;
					obj_t BgL_tailz00_716;

					BgL_prevz00_715 = BgL_headz00_714;
					BgL_tailz00_716 = BgL_l1z00_1;
				BgL_loopz00_717:
					if (PAIRP(BgL_tailz00_716))
						{
							obj_t BgL_newzd2prevzd2_719;

							BgL_newzd2prevzd2_719 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_716), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_715, BgL_newzd2prevzd2_719);
							{
								obj_t BgL_tailz00_1108;
								obj_t BgL_prevz00_1107;

								BgL_prevz00_1107 = BgL_newzd2prevzd2_719;
								BgL_tailz00_1108 = CDR(BgL_tailz00_716);
								BgL_tailz00_716 = BgL_tailz00_1108;
								BgL_prevz00_715 = BgL_prevz00_1107;
								goto BgL_loopz00_717;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_714);
				}
			}
		}

	}



/* gen-java-class-slots-access! */
	BGL_EXPORTED_DEF obj_t
		BGl_genzd2javazd2classzd2slotszd2accessz12z12zzobject_getterz00
		(BgL_typez00_bglt BgL_jclassz00_3, obj_t BgL_slotsz00_4,
		obj_t BgL_srczd2defzd2_5)
	{
		{	/* Object/getter.scm 41 */
			{	/* Object/getter.scm 48 */
				obj_t BgL_runner1138z00_740;

				if (NULLP(BgL_slotsz00_4))
					{	/* Object/getter.scm 48 */
						BgL_runner1138z00_740 = BNIL;
					}
				else
					{	/* Object/getter.scm 48 */
						obj_t BgL_head1117z00_726;

						{	/* Object/getter.scm 48 */
							obj_t BgL_arg1132z00_738;

							{	/* Object/getter.scm 48 */
								obj_t BgL_arg1137z00_739;

								BgL_arg1137z00_739 = CAR(((obj_t) BgL_slotsz00_4));
								BgL_arg1132z00_738 =
									BGl_genzd2javazd2classzd2slotzd2accessze70ze7zzobject_getterz00
									(BgL_srczd2defzd2_5, BgL_jclassz00_3, BgL_jclassz00_3,
									BgL_arg1137z00_739);
							}
							BgL_head1117z00_726 = MAKE_YOUNG_PAIR(BgL_arg1132z00_738, BNIL);
						}
						{	/* Object/getter.scm 48 */
							obj_t BgL_g1120z00_727;

							BgL_g1120z00_727 = CDR(((obj_t) BgL_slotsz00_4));
							{
								obj_t BgL_l1115z00_729;
								obj_t BgL_tail1118z00_730;

								BgL_l1115z00_729 = BgL_g1120z00_727;
								BgL_tail1118z00_730 = BgL_head1117z00_726;
							BgL_zc3z04anonymousza31125ze3z87_731:
								if (NULLP(BgL_l1115z00_729))
									{	/* Object/getter.scm 48 */
										BgL_runner1138z00_740 = BgL_head1117z00_726;
									}
								else
									{	/* Object/getter.scm 48 */
										obj_t BgL_newtail1119z00_733;

										{	/* Object/getter.scm 48 */
											obj_t BgL_arg1129z00_735;

											{	/* Object/getter.scm 48 */
												obj_t BgL_arg1131z00_736;

												BgL_arg1131z00_736 = CAR(((obj_t) BgL_l1115z00_729));
												BgL_arg1129z00_735 =
													BGl_genzd2javazd2classzd2slotzd2accessze70ze7zzobject_getterz00
													(BgL_srczd2defzd2_5, BgL_jclassz00_3, BgL_jclassz00_3,
													BgL_arg1131z00_736);
											}
											BgL_newtail1119z00_733 =
												MAKE_YOUNG_PAIR(BgL_arg1129z00_735, BNIL);
										}
										SET_CDR(BgL_tail1118z00_730, BgL_newtail1119z00_733);
										{	/* Object/getter.scm 48 */
											obj_t BgL_arg1127z00_734;

											BgL_arg1127z00_734 = CDR(((obj_t) BgL_l1115z00_729));
											{
												obj_t BgL_tail1118z00_1129;
												obj_t BgL_l1115z00_1128;

												BgL_l1115z00_1128 = BgL_arg1127z00_734;
												BgL_tail1118z00_1129 = BgL_newtail1119z00_733;
												BgL_tail1118z00_730 = BgL_tail1118z00_1129;
												BgL_l1115z00_729 = BgL_l1115z00_1128;
												goto BgL_zc3z04anonymousza31125ze3z87_731;
											}
										}
									}
							}
						}
					}
				return
					BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_runner1138z00_740);
			}
		}

	}



/* gen-java-class-slot-access~0 */
	obj_t BGl_genzd2javazd2classzd2slotzd2accessze70ze7zzobject_getterz00(obj_t
		BgL_srczd2defzd2_1053, BgL_typez00_bglt BgL_jclassz00_1052,
		BgL_typez00_bglt BgL_i1069z00_1051, obj_t BgL_slotz00_741)
	{
		{	/* Object/getter.scm 47 */
			{	/* Object/getter.scm 44 */
				obj_t BgL_refz00_743;

				{	/* Object/getter.scm 44 */
					obj_t BgL_arg1143z00_747;

					BgL_arg1143z00_747 =
						(((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_i1069z00_1051)))->BgL_idz00);
					BgL_refz00_743 =
						BGl_slotzd2directzd2refz00zzobject_getterz00(BgL_arg1143z00_747,
						BgL_jclassz00_1052, BgL_slotz00_741, ((bool_t) 0),
						BgL_srczd2defzd2_1053);
				}
				if (
					(((BgL_slotz00_bglt) COBJECT(
								((BgL_slotz00_bglt) BgL_slotz00_741)))->BgL_readzd2onlyzf3z21))
					{	/* Object/getter.scm 45 */
						return BgL_refz00_743;
					}
				else
					{	/* Object/getter.scm 47 */
						obj_t BgL_arg1141z00_745;

						{	/* Object/getter.scm 47 */
							obj_t BgL_arg1142z00_746;

							BgL_arg1142z00_746 =
								(((BgL_typez00_bglt) COBJECT(
										((BgL_typez00_bglt) BgL_i1069z00_1051)))->BgL_idz00);
							BgL_arg1141z00_745 =
								BGl_slotzd2directzd2setz12z12zzobject_getterz00
								(BgL_arg1142z00_746, BgL_jclassz00_1052, BgL_slotz00_741,
								((bool_t) 0), BgL_srczd2defzd2_1053);
						}
						return
							BGl_appendzd221011zd2zzobject_getterz00(BgL_refz00_743,
							BgL_arg1141z00_745);
					}
			}
		}

	}



/* &gen-java-class-slots-access! */
	obj_t BGl_z62genzd2javazd2classzd2slotszd2accessz12z70zzobject_getterz00(obj_t
		BgL_envz00_1047, obj_t BgL_jclassz00_1048, obj_t BgL_slotsz00_1049,
		obj_t BgL_srczd2defzd2_1050)
	{
		{	/* Object/getter.scm 41 */
			return
				BGl_genzd2javazd2classzd2slotszd2accessz12z12zzobject_getterz00(
				((BgL_typez00_bglt) BgL_jclassz00_1048), BgL_slotsz00_1049,
				BgL_srczd2defzd2_1050);
		}

	}



/* slot-direct-ref */
	obj_t BGl_slotzd2directzd2refz00zzobject_getterz00(obj_t BgL_classzd2idzd2_6,
		BgL_typez00_bglt BgL_classz00_7, obj_t BgL_slotz00_8,
		bool_t BgL_wideningz00_9, obj_t BgL_srczd2defzd2_10)
	{
		{	/* Object/getter.scm 53 */
			{	/* Object/getter.scm 55 */
				obj_t BgL_slotzd2refzd2idz00_750;

				{	/* Object/getter.scm 55 */
					obj_t BgL_arg1226z00_802;

					BgL_arg1226z00_802 =
						(((BgL_slotz00_bglt) COBJECT(
								((BgL_slotz00_bglt) BgL_slotz00_8)))->BgL_idz00);
					{	/* Object/getter.scm 55 */
						obj_t BgL_list1227z00_803;

						{	/* Object/getter.scm 55 */
							obj_t BgL_arg1228z00_804;

							{	/* Object/getter.scm 55 */
								obj_t BgL_arg1229z00_805;

								BgL_arg1229z00_805 = MAKE_YOUNG_PAIR(BgL_arg1226z00_802, BNIL);
								BgL_arg1228z00_804 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BgL_arg1229z00_805);
							}
							BgL_list1227z00_803 =
								MAKE_YOUNG_PAIR(BgL_classzd2idzd2_6, BgL_arg1228z00_804);
						}
						BgL_slotzd2refzd2idz00_750 =
							BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(BgL_list1227z00_803);
					}
				}
				{	/* Object/getter.scm 56 */
					obj_t BgL_slotzd2refzd2tidz00_752;

					{	/* Object/getter.scm 57 */
						obj_t BgL_arg1223z00_800;

						BgL_arg1223z00_800 =
							(((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt)
										(((BgL_slotz00_bglt) COBJECT(
													((BgL_slotz00_bglt) BgL_slotz00_8)))->
											BgL_typez00))))->BgL_idz00);
						BgL_slotzd2refzd2tidz00_752 =
							BGl_makezd2typedzd2identz00zzast_identz00
							(BgL_slotzd2refzd2idz00_750, BgL_arg1223z00_800);
					}
					{	/* Object/getter.scm 57 */
						obj_t BgL_objz00_753;

						BgL_objz00_753 =
							BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
							(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(1)));
						{	/* Object/getter.scm 58 */
							obj_t BgL_tidz00_754;

							{	/* Object/getter.scm 59 */
								obj_t BgL_arg1220z00_798;

								BgL_arg1220z00_798 =
									(((BgL_typez00_bglt) COBJECT(
											((BgL_typez00_bglt) BgL_classz00_7)))->BgL_idz00);
								BgL_tidz00_754 =
									BGl_makezd2typedzd2formalz00zzast_identz00
									(BgL_arg1220z00_798);
							}
							{	/* Object/getter.scm 59 */

								if (
									(BgL_classzd2idzd2_6 ==
										(((BgL_typez00_bglt) COBJECT(
													((BgL_typez00_bglt)
														(((BgL_slotz00_bglt) COBJECT(
																	((BgL_slotz00_bglt) BgL_slotz00_8)))->
															BgL_classzd2ownerzd2))))->BgL_idz00)))
									{	/* Object/getter.scm 60 */
										{	/* Object/getter.scm 66 */
											obj_t BgL_arg1152z00_758;

											{	/* Object/getter.scm 66 */
												obj_t BgL_arg1153z00_759;

												{	/* Object/getter.scm 66 */
													obj_t BgL_arg1154z00_760;

													{	/* Object/getter.scm 66 */
														obj_t BgL_arg1157z00_761;

														{	/* Object/getter.scm 66 */
															obj_t BgL_arg1158z00_762;

															BgL_arg1158z00_762 =
																MAKE_YOUNG_PAIR(BgL_tidz00_754, BNIL);
															BgL_arg1157z00_761 =
																MAKE_YOUNG_PAIR(BgL_slotzd2refzd2tidz00_752,
																BgL_arg1158z00_762);
														}
														BgL_arg1154z00_760 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(2),
															BgL_arg1157z00_761);
													}
													BgL_arg1153z00_759 =
														MAKE_YOUNG_PAIR(BgL_arg1154z00_760, BNIL);
												}
												BgL_arg1152z00_758 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
													BgL_arg1153z00_759);
											}
											BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
												(BgL_arg1152z00_758);
										}
										{	/* Object/getter.scm 69 */
											obj_t BgL_arg1162z00_763;

											{	/* Object/getter.scm 69 */
												obj_t BgL_arg1164z00_764;

												{	/* Object/getter.scm 69 */
													obj_t BgL_arg1166z00_765;

													{	/* Object/getter.scm 69 */
														obj_t BgL_arg1171z00_766;

														{	/* Object/getter.scm 69 */
															obj_t BgL_arg1172z00_767;

															{	/* Object/getter.scm 69 */
																obj_t BgL_arg1182z00_768;

																{	/* Object/getter.scm 69 */
																	obj_t BgL_arg1183z00_769;

																	{	/* Object/getter.scm 69 */
																		obj_t BgL_arg1187z00_770;

																		{	/* Object/getter.scm 69 */
																			obj_t BgL_arg1188z00_771;

																			{	/* Object/getter.scm 69 */
																				obj_t BgL_arg1189z00_772;

																				{	/* Object/getter.scm 69 */
																					obj_t BgL_arg1190z00_773;

																					BgL_arg1190z00_773 =
																						MAKE_YOUNG_PAIR
																						(BgL_slotzd2refzd2idz00_750, BNIL);
																					BgL_arg1189z00_772 =
																						MAKE_YOUNG_PAIR(BgL_arg1190z00_773,
																						BNIL);
																				}
																				BgL_arg1188z00_771 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(4),
																					BgL_arg1189z00_772);
																			}
																			BgL_arg1187z00_770 =
																				MAKE_YOUNG_PAIR(BgL_arg1188z00_771,
																				BNIL);
																		}
																		BgL_arg1183z00_769 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
																			BgL_arg1187z00_770);
																	}
																	BgL_arg1182z00_768 =
																		MAKE_YOUNG_PAIR(BgL_arg1183z00_769, BNIL);
																}
																BgL_arg1172z00_767 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
																	BgL_arg1182z00_768);
															}
															BgL_arg1171z00_766 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(7),
																BgL_arg1172z00_767);
														}
														BgL_arg1166z00_765 =
															MAKE_YOUNG_PAIR(BgL_slotzd2refzd2idz00_750,
															BgL_arg1171z00_766);
													}
													BgL_arg1164z00_764 =
														MAKE_YOUNG_PAIR(BgL_arg1166z00_765, BNIL);
												}
												BgL_arg1162z00_763 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(8),
													BgL_arg1164z00_764);
											}
											BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
												(BgL_arg1162z00_763);
										}
										{	/* Object/getter.scm 72 */
											obj_t BgL_arg1191z00_774;

											{	/* Object/getter.scm 72 */
												obj_t BgL_arg1193z00_776;
												obj_t BgL_arg1194z00_777;

												{	/* Object/getter.scm 72 */
													obj_t BgL_arg1197z00_780;

													{	/* Object/getter.scm 72 */
														obj_t BgL_arg1198z00_781;
														obj_t BgL_arg1199z00_782;

														{	/* Object/getter.scm 72 */
															obj_t BgL_arg1200z00_783;

															{	/* Object/getter.scm 72 */
																obj_t BgL_arg1201z00_784;

																{	/* Object/getter.scm 72 */
																	obj_t BgL_arg1202z00_785;

																	{	/* Object/getter.scm 72 */
																		obj_t BgL_arg1203z00_786;
																		obj_t BgL_arg1206z00_787;

																		{	/* Object/getter.scm 72 */
																			obj_t BgL_arg1455z00_948;

																			BgL_arg1455z00_948 =
																				SYMBOL_TO_STRING(BgL_objz00_753);
																			BgL_arg1203z00_786 =
																				BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																				(BgL_arg1455z00_948);
																		}
																		{	/* Object/getter.scm 72 */
																			obj_t BgL_arg1455z00_950;

																			BgL_arg1455z00_950 =
																				SYMBOL_TO_STRING(BgL_tidz00_754);
																			BgL_arg1206z00_787 =
																				BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																				(BgL_arg1455z00_950);
																		}
																		BgL_arg1202z00_785 =
																			string_append(BgL_arg1203z00_786,
																			BgL_arg1206z00_787);
																	}
																	BgL_arg1201z00_784 =
																		bstring_to_symbol(BgL_arg1202z00_785);
																}
																BgL_arg1200z00_783 =
																	MAKE_YOUNG_PAIR(BgL_arg1201z00_784, BNIL);
															}
															BgL_arg1198z00_781 =
																MAKE_YOUNG_PAIR(BgL_slotzd2refzd2tidz00_752,
																BgL_arg1200z00_783);
														}
														{	/* Object/getter.scm 73 */
															obj_t BgL_arg1208z00_788;

															BgL_arg1208z00_788 =
																BGl_makezd2classzd2refz00zzobject_toolsz00(
																((BgL_typez00_bglt) BgL_classz00_7),
																((BgL_slotz00_bglt) BgL_slotz00_8),
																BgL_objz00_753);
															BgL_arg1199z00_782 =
																MAKE_YOUNG_PAIR(BgL_arg1208z00_788, BNIL);
														}
														BgL_arg1197z00_780 =
															MAKE_YOUNG_PAIR(BgL_arg1198z00_781,
															BgL_arg1199z00_782);
													}
													BgL_arg1193z00_776 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(9),
														BgL_arg1197z00_780);
												}
												BgL_arg1194z00_777 =
													(((BgL_slotz00_bglt) COBJECT(
															((BgL_slotz00_bglt) BgL_slotz00_8)))->BgL_srcz00);
												{	/* Object/getter.scm 71 */
													obj_t BgL_list1195z00_778;

													{	/* Object/getter.scm 71 */
														obj_t BgL_arg1196z00_779;

														BgL_arg1196z00_779 =
															MAKE_YOUNG_PAIR(BgL_srczd2defzd2_10, BNIL);
														BgL_list1195z00_778 =
															MAKE_YOUNG_PAIR(BgL_arg1194z00_777,
															BgL_arg1196z00_779);
													}
													BgL_arg1191z00_774 =
														BGl_epairifyza2za2zztools_miscz00
														(BgL_arg1193z00_776, BgL_list1195z00_778);
												}
											}
											{	/* Object/getter.scm 70 */
												obj_t BgL_list1192z00_775;

												BgL_list1192z00_775 =
													MAKE_YOUNG_PAIR(BgL_arg1191z00_774, BNIL);
												return BgL_list1192z00_775;
											}
										}
									}
								else
									{	/* Object/getter.scm 77 */
										obj_t BgL_slotzd2refzd2oidz00_789;

										{	/* Object/getter.scm 77 */
											obj_t BgL_arg1209z00_790;
											obj_t BgL_arg1210z00_791;

											BgL_arg1209z00_790 =
												(((BgL_typez00_bglt) COBJECT(
														((BgL_typez00_bglt)
															(((BgL_slotz00_bglt) COBJECT(
																		((BgL_slotz00_bglt) BgL_slotz00_8)))->
																BgL_classzd2ownerzd2))))->BgL_idz00);
											BgL_arg1210z00_791 =
												(((BgL_slotz00_bglt) COBJECT(((BgL_slotz00_bglt)
															BgL_slotz00_8)))->BgL_idz00);
											{	/* Object/getter.scm 77 */
												obj_t BgL_list1211z00_792;

												{	/* Object/getter.scm 77 */
													obj_t BgL_arg1212z00_793;

													{	/* Object/getter.scm 77 */
														obj_t BgL_arg1215z00_794;

														BgL_arg1215z00_794 =
															MAKE_YOUNG_PAIR(BgL_arg1210z00_791, BNIL);
														BgL_arg1212z00_793 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
															BgL_arg1215z00_794);
													}
													BgL_list1211z00_792 =
														MAKE_YOUNG_PAIR(BgL_arg1209z00_790,
														BgL_arg1212z00_793);
												}
												BgL_slotzd2refzd2oidz00_789 =
													BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
													(BgL_list1211z00_792);
											}
										}
										BGl_addzd2macrozd2aliasz12z12zzexpand_epsz00
											(BgL_slotzd2refzd2idz00_750, BgL_slotzd2refzd2oidz00_789);
										return BNIL;
									}
							}
						}
					}
				}
			}
		}

	}



/* slot-direct-set! */
	obj_t BGl_slotzd2directzd2setz12z12zzobject_getterz00(obj_t
		BgL_classzd2idzd2_11, BgL_typez00_bglt BgL_classz00_12,
		obj_t BgL_slotz00_13, bool_t BgL_wideningz00_14, obj_t BgL_srczd2defzd2_15)
	{
		{	/* Object/getter.scm 84 */
			{	/* Object/getter.scm 85 */
				obj_t BgL_slotzd2refzd2idz00_806;

				{	/* Object/getter.scm 85 */
					obj_t BgL_arg1352z00_867;

					BgL_arg1352z00_867 =
						(((BgL_slotz00_bglt) COBJECT(
								((BgL_slotz00_bglt) BgL_slotz00_13)))->BgL_idz00);
					{	/* Object/getter.scm 85 */
						obj_t BgL_list1353z00_868;

						{	/* Object/getter.scm 85 */
							obj_t BgL_arg1361z00_869;

							{	/* Object/getter.scm 85 */
								obj_t BgL_arg1364z00_870;

								BgL_arg1364z00_870 = MAKE_YOUNG_PAIR(BgL_arg1352z00_867, BNIL);
								BgL_arg1361z00_869 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BgL_arg1364z00_870);
							}
							BgL_list1353z00_868 =
								MAKE_YOUNG_PAIR(BgL_classzd2idzd2_11, BgL_arg1361z00_869);
						}
						BgL_slotzd2refzd2idz00_806 =
							BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(BgL_list1353z00_868);
					}
				}
				{	/* Object/getter.scm 85 */
					obj_t BgL_slotzd2setz12zd2idz12_807;

					{	/* Object/getter.scm 86 */
						obj_t BgL_arg1348z00_864;

						{	/* Object/getter.scm 86 */
							obj_t BgL_arg1349z00_865;
							obj_t BgL_arg1351z00_866;

							{	/* Object/getter.scm 86 */
								obj_t BgL_arg1455z00_956;

								BgL_arg1455z00_956 =
									SYMBOL_TO_STRING(BgL_slotzd2refzd2idz00_806);
								BgL_arg1349z00_865 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_956);
							}
							{	/* Object/getter.scm 86 */
								obj_t BgL_symbolz00_957;

								BgL_symbolz00_957 = CNST_TABLE_REF(10);
								{	/* Object/getter.scm 86 */
									obj_t BgL_arg1455z00_958;

									BgL_arg1455z00_958 = SYMBOL_TO_STRING(BgL_symbolz00_957);
									BgL_arg1351z00_866 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_958);
								}
							}
							BgL_arg1348z00_864 =
								string_append(BgL_arg1349z00_865, BgL_arg1351z00_866);
						}
						BgL_slotzd2setz12zd2idz12_807 =
							bstring_to_symbol(BgL_arg1348z00_864);
					}
					{	/* Object/getter.scm 86 */
						obj_t BgL_slotzd2setz12zd2tidz12_808;

						{	/* Object/getter.scm 87 */
							obj_t BgL_arg1342z00_861;

							{	/* Object/getter.scm 87 */
								obj_t BgL_arg1343z00_862;
								obj_t BgL_arg1346z00_863;

								{	/* Object/getter.scm 87 */
									obj_t BgL_arg1455z00_961;

									BgL_arg1455z00_961 =
										SYMBOL_TO_STRING(BgL_slotzd2setz12zd2idz12_807);
									BgL_arg1343z00_862 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_961);
								}
								{	/* Object/getter.scm 87 */
									obj_t BgL_symbolz00_962;

									BgL_symbolz00_962 = CNST_TABLE_REF(11);
									{	/* Object/getter.scm 87 */
										obj_t BgL_arg1455z00_963;

										BgL_arg1455z00_963 = SYMBOL_TO_STRING(BgL_symbolz00_962);
										BgL_arg1346z00_863 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_963);
									}
								}
								BgL_arg1342z00_861 =
									string_append(BgL_arg1343z00_862, BgL_arg1346z00_863);
							}
							BgL_slotzd2setz12zd2tidz12_808 =
								bstring_to_symbol(BgL_arg1342z00_861);
						}
						{	/* Object/getter.scm 87 */
							obj_t BgL_tidz00_809;

							{	/* Object/getter.scm 88 */
								obj_t BgL_arg1340z00_860;

								BgL_arg1340z00_860 =
									(((BgL_typez00_bglt) COBJECT(
											((BgL_typez00_bglt) BgL_classz00_12)))->BgL_idz00);
								BgL_tidz00_809 =
									BGl_makezd2typedzd2formalz00zzast_identz00
									(BgL_arg1340z00_860);
							}
							{	/* Object/getter.scm 88 */
								obj_t BgL_vzd2idzd2_810;

								BgL_vzd2idzd2_810 =
									BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
									(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(12)));
								{	/* Object/getter.scm 89 */
									obj_t BgL_objz00_811;

									BgL_objz00_811 =
										BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
										(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(1)));
									{	/* Object/getter.scm 90 */
										obj_t BgL_vzd2tidzd2_812;

										{	/* Object/getter.scm 91 */
											obj_t BgL_arg1332z00_856;

											BgL_arg1332z00_856 =
												(((BgL_typez00_bglt) COBJECT(
														((BgL_typez00_bglt)
															(((BgL_slotz00_bglt) COBJECT(
																		((BgL_slotz00_bglt) BgL_slotz00_13)))->
																BgL_typez00))))->BgL_idz00);
											BgL_vzd2tidzd2_812 =
												BGl_makezd2typedzd2identz00zzast_identz00
												(BgL_vzd2idzd2_810, BgL_arg1332z00_856);
										}
										{	/* Object/getter.scm 91 */
											obj_t BgL_classzd2ownerzd2_813;

											BgL_classzd2ownerzd2_813 =
												(((BgL_slotz00_bglt) COBJECT(
														((BgL_slotz00_bglt) BgL_slotz00_13)))->
												BgL_classzd2ownerzd2);
											{	/* Object/getter.scm 92 */

												if (
													(BgL_classzd2idzd2_11 ==
														(((BgL_typez00_bglt) COBJECT(
																	((BgL_typez00_bglt)
																		BgL_classzd2ownerzd2_813)))->BgL_idz00)))
													{	/* Object/getter.scm 93 */
														{	/* Object/getter.scm 97 */
															obj_t BgL_arg1232z00_816;

															{	/* Object/getter.scm 97 */
																obj_t BgL_arg1233z00_817;

																{	/* Object/getter.scm 97 */
																	obj_t BgL_arg1234z00_818;

																	{	/* Object/getter.scm 97 */
																		obj_t BgL_arg1236z00_819;

																		{	/* Object/getter.scm 97 */
																			obj_t BgL_arg1238z00_820;

																			{	/* Object/getter.scm 97 */
																				obj_t BgL_arg1239z00_821;

																				BgL_arg1239z00_821 =
																					MAKE_YOUNG_PAIR(BgL_vzd2tidzd2_812,
																					BNIL);
																				BgL_arg1238z00_820 =
																					MAKE_YOUNG_PAIR(BgL_tidz00_809,
																					BgL_arg1239z00_821);
																			}
																			BgL_arg1236z00_819 =
																				MAKE_YOUNG_PAIR
																				(BgL_slotzd2setz12zd2tidz12_808,
																				BgL_arg1238z00_820);
																		}
																		BgL_arg1234z00_818 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(2),
																			BgL_arg1236z00_819);
																	}
																	BgL_arg1233z00_817 =
																		MAKE_YOUNG_PAIR(BgL_arg1234z00_818, BNIL);
																}
																BgL_arg1232z00_816 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
																	BgL_arg1233z00_817);
															}
															BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
																(BgL_arg1232z00_816);
														}
														{	/* Object/getter.scm 99 */
															obj_t BgL_arg1242z00_822;

															{	/* Object/getter.scm 99 */
																obj_t BgL_arg1244z00_823;

																{	/* Object/getter.scm 99 */
																	obj_t BgL_arg1248z00_824;

																	{	/* Object/getter.scm 99 */
																		obj_t BgL_arg1249z00_825;

																		{	/* Object/getter.scm 99 */
																			obj_t BgL_arg1252z00_826;

																			{	/* Object/getter.scm 99 */
																				obj_t BgL_arg1268z00_827;

																				{	/* Object/getter.scm 99 */
																					obj_t BgL_arg1272z00_828;

																					{	/* Object/getter.scm 99 */
																						obj_t BgL_arg1284z00_829;

																						{	/* Object/getter.scm 99 */
																							obj_t BgL_arg1304z00_830;

																							BgL_arg1304z00_830 =
																								MAKE_YOUNG_PAIR
																								(BgL_slotzd2refzd2idz00_806,
																								BNIL);
																							BgL_arg1284z00_829 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1304z00_830, BNIL);
																						}
																						BgL_arg1272z00_828 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(13), BgL_arg1284z00_829);
																					}
																					BgL_arg1268z00_827 =
																						MAKE_YOUNG_PAIR(BgL_arg1272z00_828,
																						BNIL);
																				}
																				BgL_arg1252z00_826 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
																					BgL_arg1268z00_827);
																			}
																			BgL_arg1249z00_825 =
																				MAKE_YOUNG_PAIR(BgL_arg1252z00_826,
																				BNIL);
																		}
																		BgL_arg1248z00_824 =
																			MAKE_YOUNG_PAIR
																			(BgL_slotzd2setz12zd2idz12_807,
																			BgL_arg1249z00_825);
																	}
																	BgL_arg1244z00_823 =
																		MAKE_YOUNG_PAIR(BgL_arg1248z00_824, BNIL);
																}
																BgL_arg1242z00_822 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(8),
																	BgL_arg1244z00_823);
															}
															BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
																(BgL_arg1242z00_822);
														}
														{	/* Object/getter.scm 102 */
															obj_t BgL_arg1305z00_831;

															{	/* Object/getter.scm 102 */
																obj_t BgL_arg1307z00_833;
																obj_t BgL_arg1308z00_834;

																{	/* Object/getter.scm 102 */
																	obj_t BgL_arg1311z00_837;

																	{	/* Object/getter.scm 102 */
																		obj_t BgL_arg1312z00_838;
																		obj_t BgL_arg1314z00_839;

																		{	/* Object/getter.scm 102 */
																			obj_t BgL_arg1315z00_840;

																			{	/* Object/getter.scm 102 */
																				obj_t BgL_arg1316z00_841;
																				obj_t BgL_arg1317z00_842;

																				{	/* Object/getter.scm 102 */
																					obj_t BgL_arg1318z00_843;

																					{	/* Object/getter.scm 102 */
																						obj_t BgL_arg1319z00_844;
																						obj_t BgL_arg1320z00_845;

																						{	/* Object/getter.scm 102 */
																							obj_t BgL_arg1455z00_971;

																							BgL_arg1455z00_971 =
																								SYMBOL_TO_STRING
																								(BgL_objz00_811);
																							BgL_arg1319z00_844 =
																								BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																								(BgL_arg1455z00_971);
																						}
																						{	/* Object/getter.scm 102 */
																							obj_t BgL_arg1455z00_973;

																							BgL_arg1455z00_973 =
																								SYMBOL_TO_STRING
																								(BgL_tidz00_809);
																							BgL_arg1320z00_845 =
																								BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																								(BgL_arg1455z00_973);
																						}
																						BgL_arg1318z00_843 =
																							string_append(BgL_arg1319z00_844,
																							BgL_arg1320z00_845);
																					}
																					BgL_arg1316z00_841 =
																						bstring_to_symbol
																						(BgL_arg1318z00_843);
																				}
																				BgL_arg1317z00_842 =
																					MAKE_YOUNG_PAIR(BgL_vzd2tidzd2_812,
																					BNIL);
																				BgL_arg1315z00_840 =
																					MAKE_YOUNG_PAIR(BgL_arg1316z00_841,
																					BgL_arg1317z00_842);
																			}
																			BgL_arg1312z00_838 =
																				MAKE_YOUNG_PAIR
																				(BgL_slotzd2setz12zd2tidz12_808,
																				BgL_arg1315z00_840);
																		}
																		{	/* Object/getter.scm 103 */
																			obj_t BgL_arg1321z00_846;

																			BgL_arg1321z00_846 =
																				BGl_makezd2classzd2setz12z12zzobject_toolsz00
																				(((BgL_typez00_bglt) BgL_classz00_12),
																				((BgL_slotz00_bglt) BgL_slotz00_13),
																				BgL_objz00_811, BgL_vzd2idzd2_810);
																			BgL_arg1314z00_839 =
																				MAKE_YOUNG_PAIR(BgL_arg1321z00_846,
																				BNIL);
																		}
																		BgL_arg1311z00_837 =
																			MAKE_YOUNG_PAIR(BgL_arg1312z00_838,
																			BgL_arg1314z00_839);
																	}
																	BgL_arg1307z00_833 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(9),
																		BgL_arg1311z00_837);
																}
																{	/* Object/getter.scm 104 */
																	bool_t BgL_test1514z00_1304;

																	{	/* Object/getter.scm 104 */
																		obj_t BgL_classz00_975;

																		BgL_classz00_975 =
																			BGl_slotz00zzobject_slotsz00;
																		if (BGL_OBJECTP(BgL_slotz00_13))
																			{	/* Object/getter.scm 104 */
																				BgL_objectz00_bglt BgL_arg1807z00_977;

																				BgL_arg1807z00_977 =
																					(BgL_objectz00_bglt) (BgL_slotz00_13);
																				if (BGL_CONDEXPAND_ISA_ARCH64())
																					{	/* Object/getter.scm 104 */
																						long BgL_idxz00_983;

																						BgL_idxz00_983 =
																							BGL_OBJECT_INHERITANCE_NUM
																							(BgL_arg1807z00_977);
																						BgL_test1514z00_1304 =
																							(VECTOR_REF
																							(BGl_za2inheritancesza2z00zz__objectz00,
																								(BgL_idxz00_983 + 1L)) ==
																							BgL_classz00_975);
																					}
																				else
																					{	/* Object/getter.scm 104 */
																						bool_t BgL_res1487z00_1008;

																						{	/* Object/getter.scm 104 */
																							obj_t BgL_oclassz00_991;

																							{	/* Object/getter.scm 104 */
																								obj_t BgL_arg1815z00_999;
																								long BgL_arg1816z00_1000;

																								BgL_arg1815z00_999 =
																									(BGl_za2classesza2z00zz__objectz00);
																								{	/* Object/getter.scm 104 */
																									long BgL_arg1817z00_1001;

																									BgL_arg1817z00_1001 =
																										BGL_OBJECT_CLASS_NUM
																										(BgL_arg1807z00_977);
																									BgL_arg1816z00_1000 =
																										(BgL_arg1817z00_1001 -
																										OBJECT_TYPE);
																								}
																								BgL_oclassz00_991 =
																									VECTOR_REF(BgL_arg1815z00_999,
																									BgL_arg1816z00_1000);
																							}
																							{	/* Object/getter.scm 104 */
																								bool_t BgL__ortest_1115z00_992;

																								BgL__ortest_1115z00_992 =
																									(BgL_classz00_975 ==
																									BgL_oclassz00_991);
																								if (BgL__ortest_1115z00_992)
																									{	/* Object/getter.scm 104 */
																										BgL_res1487z00_1008 =
																											BgL__ortest_1115z00_992;
																									}
																								else
																									{	/* Object/getter.scm 104 */
																										long BgL_odepthz00_993;

																										{	/* Object/getter.scm 104 */
																											obj_t BgL_arg1804z00_994;

																											BgL_arg1804z00_994 =
																												(BgL_oclassz00_991);
																											BgL_odepthz00_993 =
																												BGL_CLASS_DEPTH
																												(BgL_arg1804z00_994);
																										}
																										if (
																											(1L < BgL_odepthz00_993))
																											{	/* Object/getter.scm 104 */
																												obj_t
																													BgL_arg1802z00_996;
																												{	/* Object/getter.scm 104 */
																													obj_t
																														BgL_arg1803z00_997;
																													BgL_arg1803z00_997 =
																														(BgL_oclassz00_991);
																													BgL_arg1802z00_996 =
																														BGL_CLASS_ANCESTORS_REF
																														(BgL_arg1803z00_997,
																														1L);
																												}
																												BgL_res1487z00_1008 =
																													(BgL_arg1802z00_996 ==
																													BgL_classz00_975);
																											}
																										else
																											{	/* Object/getter.scm 104 */
																												BgL_res1487z00_1008 =
																													((bool_t) 0);
																											}
																									}
																							}
																						}
																						BgL_test1514z00_1304 =
																							BgL_res1487z00_1008;
																					}
																			}
																		else
																			{	/* Object/getter.scm 104 */
																				BgL_test1514z00_1304 = ((bool_t) 0);
																			}
																	}
																	if (BgL_test1514z00_1304)
																		{	/* Object/getter.scm 104 */
																			BgL_arg1308z00_834 =
																				(((BgL_slotz00_bglt) COBJECT(
																						((BgL_slotz00_bglt)
																							BgL_slotz00_13)))->BgL_srcz00);
																		}
																	else
																		{	/* Object/getter.scm 104 */
																			BgL_arg1308z00_834 = BgL_slotz00_13;
																		}
																}
																{	/* Object/getter.scm 101 */
																	obj_t BgL_list1309z00_835;

																	{	/* Object/getter.scm 101 */
																		obj_t BgL_arg1310z00_836;

																		BgL_arg1310z00_836 =
																			MAKE_YOUNG_PAIR(BgL_srczd2defzd2_15,
																			BNIL);
																		BgL_list1309z00_835 =
																			MAKE_YOUNG_PAIR(BgL_arg1308z00_834,
																			BgL_arg1310z00_836);
																	}
																	BgL_arg1305z00_831 =
																		BGl_epairifyza2za2zztools_miscz00
																		(BgL_arg1307z00_833, BgL_list1309z00_835);
																}
															}
															{	/* Object/getter.scm 100 */
																obj_t BgL_list1306z00_832;

																BgL_list1306z00_832 =
																	MAKE_YOUNG_PAIR(BgL_arg1305z00_831, BNIL);
																return BgL_list1306z00_832;
															}
														}
													}
												else
													{	/* Object/getter.scm 108 */
														obj_t BgL_slotzd2setz12zd2oidz12_848;

														{	/* Object/getter.scm 108 */
															obj_t BgL_arg1323z00_849;
															obj_t BgL_arg1325z00_850;

															BgL_arg1323z00_849 =
																(((BgL_typez00_bglt) COBJECT(
																		((BgL_typez00_bglt)
																			BgL_classzd2ownerzd2_813)))->BgL_idz00);
															BgL_arg1325z00_850 =
																(((BgL_slotz00_bglt) COBJECT(((BgL_slotz00_bglt)
																			BgL_slotz00_13)))->BgL_idz00);
															{	/* Object/getter.scm 108 */
																obj_t BgL_list1326z00_851;

																{	/* Object/getter.scm 108 */
																	obj_t BgL_arg1327z00_852;

																	{	/* Object/getter.scm 108 */
																		obj_t BgL_arg1328z00_853;

																		{	/* Object/getter.scm 108 */
																			obj_t BgL_arg1329z00_854;

																			BgL_arg1329z00_854 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
																				BNIL);
																			BgL_arg1328z00_853 =
																				MAKE_YOUNG_PAIR(BgL_arg1325z00_850,
																				BgL_arg1329z00_854);
																		}
																		BgL_arg1327z00_852 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
																			BgL_arg1328z00_853);
																	}
																	BgL_list1326z00_851 =
																		MAKE_YOUNG_PAIR(BgL_arg1323z00_849,
																		BgL_arg1327z00_852);
																}
																BgL_slotzd2setz12zd2oidz12_848 =
																	BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																	(BgL_list1326z00_851);
															}
														}
														BGl_addzd2macrozd2aliasz12z12zzexpand_epsz00
															(BgL_slotzd2setz12zd2idz12_807,
															BgL_slotzd2setz12zd2oidz12_848);
														return BNIL;
													}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzobject_getterz00(void)
	{
		{	/* Object/getter.scm 18 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzobject_getterz00(void)
	{
		{	/* Object/getter.scm 18 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzobject_getterz00(void)
	{
		{	/* Object/getter.scm 18 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzobject_getterz00(void)
	{
		{	/* Object/getter.scm 18 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1490z00zzobject_getterz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1490z00zzobject_getterz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1490z00zzobject_getterz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1490z00zzobject_getterz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1490z00zzobject_getterz00));
			BGl_modulezd2initializa7ationz75zztype_toolsz00(453414928L,
				BSTRING_TO_STRING(BGl_string1490z00zzobject_getterz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1490z00zzobject_getterz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1490z00zzobject_getterz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1490z00zzobject_getterz00));
			BGl_modulezd2initializa7ationz75zzexpand_epsz00(359337187L,
				BSTRING_TO_STRING(BGl_string1490z00zzobject_getterz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1490z00zzobject_getterz00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string1490z00zzobject_getterz00));
			BGl_modulezd2initializa7ationz75zzobject_toolsz00(196511171L,
				BSTRING_TO_STRING(BGl_string1490z00zzobject_getterz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1490z00zzobject_getterz00));
			BGl_modulezd2initializa7ationz75zzmodule_impusez00(478324304L,
				BSTRING_TO_STRING(BGl_string1490z00zzobject_getterz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1490z00zzobject_getterz00));
			return
				BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1490z00zzobject_getterz00));
		}

	}

#ifdef __cplusplus
}
#endif
