/*===========================================================================*/
/*   (Object/predicate.scm)                                                  */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Object/predicate.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_OBJECT_PREDICATE_TYPE_DEFINITIONS
#define BGL_OBJECT_PREDICATE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_tclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		struct BgL_globalz00_bgl *BgL_holderz00;
		obj_t BgL_wideningz00;
		long BgL_depthz00;
		bool_t BgL_finalzf3zf3;
		obj_t BgL_constructorz00;
		obj_t BgL_virtualzd2slotszd2numberz00;
		bool_t BgL_abstractzf3zf3;
		obj_t BgL_widezd2typezd2;
		obj_t BgL_subclassesz00;
	}                *BgL_tclassz00_bglt;

	typedef struct BgL_jclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		obj_t BgL_packagez00;
	}                *BgL_jclassz00_bglt;


#endif													// BGL_OBJECT_PREDICATE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzobject_predicatez00 = BUNSPEC;
	extern obj_t BGl_makezd2typedzd2identz00zzast_identz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_tclassz00zzobject_classz00;
	extern obj_t BGl_epairifyza2za2zztools_miscz00(obj_t, obj_t);
	extern obj_t BGl_za2passza2z00zzengine_paramz00;
	static obj_t BGl_genericzd2initzd2zzobject_predicatez00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_genzd2classzd2predz12z12zzobject_predicatez00(BgL_typez00_bglt, obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zzobject_predicatez00(void);
	static obj_t BGl_z62genzd2classzd2predz12z70zzobject_predicatez00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_importzd2javazd2classzd2predz12zc0zzobject_predicatez00
		(BgL_typez00_bglt, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzobject_predicatez00(void);
	static obj_t BGl_z62importzd2classzd2predz12z70zzobject_predicatez00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	static obj_t
		BGl_genzd2javazd2classzd2predz12zc0zzobject_predicatez00(BgL_typez00_bglt,
		obj_t, obj_t);
	static bool_t BGl_inlinezd2predzf3z21zzobject_predicatez00(void);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	extern obj_t BGl_producezd2modulezd2clausez12z12zzmodule_modulez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzobject_predicatez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_impusez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_privatez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_za2profilezd2modeza2zd2zzengine_paramz00;
	BGL_IMPORT bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
	extern obj_t BGl_makezd2privatezd2sexpz00zzast_privatez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_importzd2parserzd2zzmodule_impusez00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zzobject_predicatez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzobject_predicatez00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzobject_predicatez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzobject_predicatez00(void);
	static obj_t
		BGl_z62importzd2javazd2classzd2predz12za2zzobject_predicatez00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_importzd2classzd2predz12z12zzobject_predicatez00(BgL_typez00_bglt,
		obj_t, obj_t);
	extern obj_t BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00(obj_t);
	static obj_t __cnst[20];


	   
		 
		DEFINE_STRING(BGl_string1491z00zzobject_predicatez00,
		BgL_bgl_string1491za700za7za7o1503za7, "object_predicate", 16);
	      DEFINE_STRING(BGl_string1492z00zzobject_predicatez00,
		BgL_bgl_string1492za700za7za7o1504za7,
		"export instanceof static define define-inline pragma no-cfa-top effect predicate-of inline ::obj super super- @ isa? __object obj ?::bool ? (make-heap make-add-heap) ",
		166);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_importzd2classzd2predz12zd2envzc0zzobject_predicatez00,
		BgL_bgl_za762importza7d2clas1505z00,
		BGl_z62importzd2classzd2predz12z70zzobject_predicatez00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genzd2classzd2predz12zd2envzc0zzobject_predicatez00,
		BgL_bgl_za762genza7d2classza7d1506za7,
		BGl_z62genzd2classzd2predz12z70zzobject_predicatez00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_importzd2javazd2classzd2predz12zd2envz12zzobject_predicatez00,
		BgL_bgl_za762importza7d2java1507z00,
		BGl_z62importzd2javazd2classzd2predz12za2zzobject_predicatez00, 0L, BUNSPEC,
		3);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzobject_predicatez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzobject_predicatez00(long
		BgL_checksumz00_1121, char *BgL_fromz00_1122)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzobject_predicatez00))
				{
					BGl_requirezd2initializa7ationz75zzobject_predicatez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzobject_predicatez00();
					BGl_libraryzd2moduleszd2initz00zzobject_predicatez00();
					BGl_cnstzd2initzd2zzobject_predicatez00();
					BGl_importedzd2moduleszd2initz00zzobject_predicatez00();
					return BGl_methodzd2initzd2zzobject_predicatez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzobject_predicatez00(void)
	{
		{	/* Object/predicate.scm 21 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "object_predicate");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"object_predicate");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"object_predicate");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "object_predicate");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"object_predicate");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"object_predicate");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"object_predicate");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"object_predicate");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"object_predicate");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"object_predicate");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzobject_predicatez00(void)
	{
		{	/* Object/predicate.scm 21 */
			{	/* Object/predicate.scm 21 */
				obj_t BgL_cportz00_1110;

				{	/* Object/predicate.scm 21 */
					obj_t BgL_stringz00_1117;

					BgL_stringz00_1117 = BGl_string1492z00zzobject_predicatez00;
					{	/* Object/predicate.scm 21 */
						obj_t BgL_startz00_1118;

						BgL_startz00_1118 = BINT(0L);
						{	/* Object/predicate.scm 21 */
							obj_t BgL_endz00_1119;

							BgL_endz00_1119 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1117)));
							{	/* Object/predicate.scm 21 */

								BgL_cportz00_1110 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1117, BgL_startz00_1118, BgL_endz00_1119);
				}}}}
				{
					long BgL_iz00_1111;

					BgL_iz00_1111 = 19L;
				BgL_loopz00_1112:
					if ((BgL_iz00_1111 == -1L))
						{	/* Object/predicate.scm 21 */
							return BUNSPEC;
						}
					else
						{	/* Object/predicate.scm 21 */
							{	/* Object/predicate.scm 21 */
								obj_t BgL_arg1502z00_1113;

								{	/* Object/predicate.scm 21 */

									{	/* Object/predicate.scm 21 */
										obj_t BgL_locationz00_1115;

										BgL_locationz00_1115 = BBOOL(((bool_t) 0));
										{	/* Object/predicate.scm 21 */

											BgL_arg1502z00_1113 =
												BGl_readz00zz__readerz00(BgL_cportz00_1110,
												BgL_locationz00_1115);
										}
									}
								}
								{	/* Object/predicate.scm 21 */
									int BgL_tmpz00_1150;

									BgL_tmpz00_1150 = (int) (BgL_iz00_1111);
									CNST_TABLE_SET(BgL_tmpz00_1150, BgL_arg1502z00_1113);
							}}
							{	/* Object/predicate.scm 21 */
								int BgL_auxz00_1116;

								BgL_auxz00_1116 = (int) ((BgL_iz00_1111 - 1L));
								{
									long BgL_iz00_1155;

									BgL_iz00_1155 = (long) (BgL_auxz00_1116);
									BgL_iz00_1111 = BgL_iz00_1155;
									goto BgL_loopz00_1112;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzobject_predicatez00(void)
	{
		{	/* Object/predicate.scm 21 */
			return bgl_gc_roots_register();
		}

	}



/* inline-pred? */
	bool_t BGl_inlinezd2predzf3z21zzobject_predicatez00(void)
	{
		{	/* Object/predicate.scm 48 */
			{	/* Object/predicate.scm 49 */
				bool_t BgL_test1510z00_1158;

				{	/* Object/predicate.scm 49 */
					bool_t BgL__ortest_1079z00_723;

					if (BGl_numberzf3zf3zz__r4_numbers_6_5z00
						(BGl_za2profilezd2modeza2zd2zzengine_paramz00))
						{	/* Object/predicate.scm 49 */
							BgL__ortest_1079z00_723 = ((bool_t) 0);
						}
					else
						{	/* Object/predicate.scm 49 */
							BgL__ortest_1079z00_723 = ((bool_t) 1);
						}
					if (BgL__ortest_1079z00_723)
						{	/* Object/predicate.scm 49 */
							BgL_test1510z00_1158 = BgL__ortest_1079z00_723;
						}
					else
						{	/* Object/predicate.scm 49 */
							BgL_test1510z00_1158 =
								(
								(long) CINT(BGl_za2profilezd2modeza2zd2zzengine_paramz00) < 1L);
				}}
				if (BgL_test1510z00_1158)
					{	/* Object/predicate.scm 49 */
						if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
								(BGl_za2passza2z00zzengine_paramz00, CNST_TABLE_REF(0))))
							{	/* Object/predicate.scm 51 */
								return ((bool_t) 0);
							}
						else
							{	/* Object/predicate.scm 51 */
								return ((bool_t) 1);
							}
					}
				else
					{	/* Object/predicate.scm 49 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* gen-class-pred! */
	BGL_EXPORTED_DEF obj_t
		BGl_genzd2classzd2predz12z12zzobject_predicatez00(BgL_typez00_bglt
		BgL_classz00_3, obj_t BgL_srczd2defzd2_4, obj_t BgL_importz00_5)
	{
		{	/* Object/predicate.scm 59 */
			{	/* Object/predicate.scm 60 */
				obj_t BgL_idz00_725;

				BgL_idz00_725 =
					(((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_classz00_3)))->BgL_idz00);
				{	/* Object/predicate.scm 60 */
					obj_t BgL_idzf3zf3_726;

					{	/* Object/predicate.scm 61 */
						obj_t BgL_arg1226z00_785;

						{	/* Object/predicate.scm 61 */
							obj_t BgL_arg1227z00_786;
							obj_t BgL_arg1228z00_787;

							{	/* Object/predicate.scm 61 */
								obj_t BgL_arg1455z00_927;

								BgL_arg1455z00_927 = SYMBOL_TO_STRING(BgL_idz00_725);
								BgL_arg1227z00_786 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_927);
							}
							{	/* Object/predicate.scm 61 */
								obj_t BgL_symbolz00_928;

								BgL_symbolz00_928 = CNST_TABLE_REF(1);
								{	/* Object/predicate.scm 61 */
									obj_t BgL_arg1455z00_929;

									BgL_arg1455z00_929 = SYMBOL_TO_STRING(BgL_symbolz00_928);
									BgL_arg1228z00_787 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_929);
								}
							}
							BgL_arg1226z00_785 =
								string_append(BgL_arg1227z00_786, BgL_arg1228z00_787);
						}
						BgL_idzf3zf3_726 = bstring_to_symbol(BgL_arg1226z00_785);
					}
					{	/* Object/predicate.scm 61 */
						obj_t BgL_predzd2idzd2_727;

						{	/* Object/predicate.scm 62 */
							obj_t BgL_arg1221z00_782;

							{	/* Object/predicate.scm 62 */
								obj_t BgL_arg1223z00_783;
								obj_t BgL_arg1225z00_784;

								{	/* Object/predicate.scm 62 */
									obj_t BgL_arg1455z00_932;

									BgL_arg1455z00_932 = SYMBOL_TO_STRING(BgL_idz00_725);
									BgL_arg1223z00_783 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_932);
								}
								{	/* Object/predicate.scm 62 */
									obj_t BgL_symbolz00_933;

									BgL_symbolz00_933 = CNST_TABLE_REF(2);
									{	/* Object/predicate.scm 62 */
										obj_t BgL_arg1455z00_934;

										BgL_arg1455z00_934 = SYMBOL_TO_STRING(BgL_symbolz00_933);
										BgL_arg1225z00_784 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_934);
									}
								}
								BgL_arg1221z00_782 =
									string_append(BgL_arg1223z00_783, BgL_arg1225z00_784);
							}
							BgL_predzd2idzd2_727 = bstring_to_symbol(BgL_arg1221z00_782);
						}
						{	/* Object/predicate.scm 62 */
							BgL_globalz00_bglt BgL_holderz00_728;

							{
								BgL_tclassz00_bglt BgL_auxz00_1184;

								{
									obj_t BgL_auxz00_1185;

									{	/* Object/predicate.scm 63 */
										BgL_objectz00_bglt BgL_tmpz00_1186;

										BgL_tmpz00_1186 = ((BgL_objectz00_bglt) BgL_classz00_3);
										BgL_auxz00_1185 = BGL_OBJECT_WIDENING(BgL_tmpz00_1186);
									}
									BgL_auxz00_1184 = ((BgL_tclassz00_bglt) BgL_auxz00_1185);
								}
								BgL_holderz00_728 =
									(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_1184))->
									BgL_holderz00);
							}
							{	/* Object/predicate.scm 63 */
								obj_t BgL_objz00_729;

								BgL_objz00_729 =
									BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
									(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(3)));
								{	/* Object/predicate.scm 64 */
									obj_t BgL_superz00_730;

									{
										BgL_tclassz00_bglt BgL_auxz00_1194;

										{
											obj_t BgL_auxz00_1195;

											{	/* Object/predicate.scm 65 */
												BgL_objectz00_bglt BgL_tmpz00_1196;

												BgL_tmpz00_1196 = ((BgL_objectz00_bglt) BgL_classz00_3);
												BgL_auxz00_1195 = BGL_OBJECT_WIDENING(BgL_tmpz00_1196);
											}
											BgL_auxz00_1194 = ((BgL_tclassz00_bglt) BgL_auxz00_1195);
										}
										BgL_superz00_730 =
											(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_1194))->
											BgL_itszd2superzd2);
									}
									{	/* Object/predicate.scm 65 */

										{

											{	/* Object/predicate.scm 68 */
												bool_t BgL_test1514z00_1201;

												{	/* Object/predicate.scm 68 */
													obj_t BgL_classz00_942;

													BgL_classz00_942 = BGl_tclassz00zzobject_classz00;
													if (BGL_OBJECTP(BgL_superz00_730))
														{	/* Object/predicate.scm 68 */
															BgL_objectz00_bglt BgL_arg1807z00_944;

															BgL_arg1807z00_944 =
																(BgL_objectz00_bglt) (BgL_superz00_730);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Object/predicate.scm 68 */
																	long BgL_idxz00_950;

																	BgL_idxz00_950 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_944);
																	BgL_test1514z00_1201 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_950 + 2L)) ==
																		BgL_classz00_942);
																}
															else
																{	/* Object/predicate.scm 68 */
																	bool_t BgL_res1486z00_975;

																	{	/* Object/predicate.scm 68 */
																		obj_t BgL_oclassz00_958;

																		{	/* Object/predicate.scm 68 */
																			obj_t BgL_arg1815z00_966;
																			long BgL_arg1816z00_967;

																			BgL_arg1815z00_966 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Object/predicate.scm 68 */
																				long BgL_arg1817z00_968;

																				BgL_arg1817z00_968 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_944);
																				BgL_arg1816z00_967 =
																					(BgL_arg1817z00_968 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_958 =
																				VECTOR_REF(BgL_arg1815z00_966,
																				BgL_arg1816z00_967);
																		}
																		{	/* Object/predicate.scm 68 */
																			bool_t BgL__ortest_1115z00_959;

																			BgL__ortest_1115z00_959 =
																				(BgL_classz00_942 == BgL_oclassz00_958);
																			if (BgL__ortest_1115z00_959)
																				{	/* Object/predicate.scm 68 */
																					BgL_res1486z00_975 =
																						BgL__ortest_1115z00_959;
																				}
																			else
																				{	/* Object/predicate.scm 68 */
																					long BgL_odepthz00_960;

																					{	/* Object/predicate.scm 68 */
																						obj_t BgL_arg1804z00_961;

																						BgL_arg1804z00_961 =
																							(BgL_oclassz00_958);
																						BgL_odepthz00_960 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_961);
																					}
																					if ((2L < BgL_odepthz00_960))
																						{	/* Object/predicate.scm 68 */
																							obj_t BgL_arg1802z00_963;

																							{	/* Object/predicate.scm 68 */
																								obj_t BgL_arg1803z00_964;

																								BgL_arg1803z00_964 =
																									(BgL_oclassz00_958);
																								BgL_arg1802z00_963 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_964, 2L);
																							}
																							BgL_res1486z00_975 =
																								(BgL_arg1802z00_963 ==
																								BgL_classz00_942);
																						}
																					else
																						{	/* Object/predicate.scm 68 */
																							BgL_res1486z00_975 = ((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test1514z00_1201 = BgL_res1486z00_975;
																}
														}
													else
														{	/* Object/predicate.scm 68 */
															BgL_test1514z00_1201 = ((bool_t) 0);
														}
												}
												if (BgL_test1514z00_1201)
													{	/* Object/predicate.scm 72 */
														obj_t BgL_superzd2predzd2idz00_733;
														obj_t BgL_superzd2typedzd2_734;

														{	/* Object/predicate.scm 72 */
															obj_t BgL_arg1198z00_765;

															{	/* Object/predicate.scm 72 */
																obj_t BgL_arg1199z00_766;
																obj_t BgL_arg1200z00_767;

																{	/* Object/predicate.scm 72 */
																	obj_t BgL_symbolz00_976;

																	BgL_symbolz00_976 = CNST_TABLE_REF(7);
																	{	/* Object/predicate.scm 72 */
																		obj_t BgL_arg1455z00_977;

																		BgL_arg1455z00_977 =
																			SYMBOL_TO_STRING(BgL_symbolz00_976);
																		BgL_arg1199z00_766 =
																			BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																			(BgL_arg1455z00_977);
																	}
																}
																{	/* Object/predicate.scm 72 */
																	obj_t BgL_arg1455z00_979;

																	BgL_arg1455z00_979 =
																		SYMBOL_TO_STRING(BgL_predzd2idzd2_727);
																	BgL_arg1200z00_767 =
																		BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																		(BgL_arg1455z00_979);
																}
																BgL_arg1198z00_765 =
																	string_append(BgL_arg1199z00_766,
																	BgL_arg1200z00_767);
															}
															BgL_superzd2predzd2idz00_733 =
																bstring_to_symbol(BgL_arg1198z00_765);
														}
														{	/* Object/predicate.scm 73 */
															obj_t BgL_arg1201z00_768;

															BgL_arg1201z00_768 =
																(((BgL_typez00_bglt) COBJECT(
																		((BgL_typez00_bglt) BgL_superz00_730)))->
																BgL_idz00);
															BgL_superzd2typedzd2_734 =
																BGl_makezd2typedzd2identz00zzast_identz00
																(CNST_TABLE_REF(8), BgL_arg1201z00_768);
														}
														{	/* Object/predicate.scm 76 */
															obj_t BgL_arg1137z00_735;

															if (BGl_inlinezd2predzf3z21zzobject_predicatez00
																())
																{	/* Object/predicate.scm 77 */
																	obj_t BgL_arg1140z00_737;

																	{	/* Object/predicate.scm 77 */
																		obj_t BgL_arg1141z00_738;

																		{	/* Object/predicate.scm 77 */
																			obj_t BgL_arg1142z00_739;

																			{	/* Object/predicate.scm 77 */
																				obj_t BgL_arg1143z00_740;

																				BgL_arg1143z00_740 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(9),
																					BNIL);
																				BgL_arg1142z00_739 =
																					MAKE_YOUNG_PAIR(BgL_predzd2idzd2_727,
																					BgL_arg1143z00_740);
																			}
																			BgL_arg1141z00_738 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
																				BgL_arg1142z00_739);
																		}
																		BgL_arg1140z00_737 =
																			MAKE_YOUNG_PAIR(BgL_arg1141z00_738, BNIL);
																	}
																	BgL_arg1137z00_735 =
																		MAKE_YOUNG_PAIR(BgL_importz00_5,
																		BgL_arg1140z00_737);
																}
															else
																{	/* Object/predicate.scm 78 */
																	obj_t BgL_arg1145z00_741;

																	{	/* Object/predicate.scm 78 */
																		obj_t BgL_arg1148z00_742;

																		{	/* Object/predicate.scm 78 */
																			obj_t BgL_arg1149z00_743;

																			BgL_arg1149z00_743 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(9),
																				BNIL);
																			BgL_arg1148z00_742 =
																				MAKE_YOUNG_PAIR(BgL_predzd2idzd2_727,
																				BgL_arg1149z00_743);
																		}
																		BgL_arg1145z00_741 =
																			MAKE_YOUNG_PAIR(BgL_arg1148z00_742, BNIL);
																	}
																	BgL_arg1137z00_735 =
																		MAKE_YOUNG_PAIR(BgL_importz00_5,
																		BgL_arg1145z00_741);
																}
															BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
																(BgL_arg1137z00_735);
														}
														{	/* Object/predicate.scm 80 */
															obj_t BgL_arg1152z00_744;

															{	/* Object/predicate.scm 80 */
																obj_t BgL_arg1153z00_745;

																{	/* Object/predicate.scm 80 */
																	obj_t BgL_arg1154z00_746;

																	{	/* Object/predicate.scm 80 */
																		obj_t BgL_arg1157z00_747;

																		{	/* Object/predicate.scm 80 */
																			obj_t BgL_arg1158z00_748;
																			obj_t BgL_arg1162z00_749;

																			{	/* Object/predicate.scm 80 */
																				obj_t BgL_arg1164z00_750;

																				{	/* Object/predicate.scm 80 */
																					obj_t BgL_arg1166z00_751;

																					BgL_arg1166z00_751 =
																						(((BgL_typez00_bglt) COBJECT(
																								((BgL_typez00_bglt)
																									BgL_classz00_3)))->BgL_idz00);
																					BgL_arg1164z00_750 =
																						MAKE_YOUNG_PAIR(BgL_arg1166z00_751,
																						BNIL);
																				}
																				BgL_arg1158z00_748 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
																					BgL_arg1164z00_750);
																			}
																			{	/* Object/predicate.scm 81 */
																				obj_t BgL_arg1171z00_752;

																				{	/* Object/predicate.scm 81 */
																					obj_t BgL_arg1172z00_753;

																					BgL_arg1172z00_753 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(12),
																						BNIL);
																					BgL_arg1171z00_752 =
																						MAKE_YOUNG_PAIR(BgL_arg1172z00_753,
																						BNIL);
																				}
																				BgL_arg1162z00_749 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(13),
																					BgL_arg1171z00_752);
																			}
																			BgL_arg1157z00_747 =
																				MAKE_YOUNG_PAIR(BgL_arg1158z00_748,
																				BgL_arg1162z00_749);
																		}
																		BgL_arg1154z00_746 =
																			MAKE_YOUNG_PAIR(BgL_idzf3zf3_726,
																			BgL_arg1157z00_747);
																	}
																	BgL_arg1153z00_745 =
																		MAKE_YOUNG_PAIR(BgL_arg1154z00_746, BNIL);
																}
																BgL_arg1152z00_744 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(14),
																	BgL_arg1153z00_745);
															}
															BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
																(BgL_arg1152z00_744);
														}
														{	/* Object/predicate.scm 84 */
															obj_t BgL_arg1182z00_754;

															{	/* Object/predicate.scm 84 */
																obj_t BgL_arg1187z00_756;

																{	/* Object/predicate.scm 84 */
																	obj_t BgL_arg1189z00_758;
																	obj_t BgL_arg1190z00_759;

																	if (BGl_inlinezd2predzf3z21zzobject_predicatez00())
																		{	/* Object/predicate.scm 84 */
																			BgL_arg1189z00_758 = CNST_TABLE_REF(15);
																		}
																	else
																		{	/* Object/predicate.scm 84 */
																			BgL_arg1189z00_758 = CNST_TABLE_REF(16);
																		}
																	{	/* Object/predicate.scm 85 */
																		obj_t BgL_arg1193z00_761;
																		obj_t BgL_arg1194z00_762;

																		{	/* Object/predicate.scm 85 */
																			obj_t BgL_arg1196z00_763;

																			BgL_arg1196z00_763 =
																				MAKE_YOUNG_PAIR(BgL_objz00_729, BNIL);
																			BgL_arg1193z00_761 =
																				MAKE_YOUNG_PAIR(BgL_predzd2idzd2_727,
																				BgL_arg1196z00_763);
																		}
																		{	/* Object/predicate.scm 84 */
																			obj_t BgL_tmpz00_1272;

																			{	/* Object/predicate.scm 67 */
																				obj_t BgL_arg1203z00_770;
																				obj_t BgL_arg1206z00_771;

																				{	/* Object/predicate.scm 67 */
																					obj_t BgL_arg1208z00_772;

																					{	/* Object/predicate.scm 67 */
																						obj_t BgL_arg1209z00_773;

																						BgL_arg1209z00_773 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF(4),
																							BNIL);
																						BgL_arg1208z00_772 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
																							BgL_arg1209z00_773);
																					}
																					BgL_arg1203z00_770 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
																						BgL_arg1208z00_772);
																				}
																				{	/* Object/predicate.scm 67 */
																					obj_t BgL_arg1210z00_774;

																					{	/* Object/predicate.scm 67 */
																						obj_t BgL_arg1212z00_775;

																						{	/* Object/predicate.scm 67 */
																							obj_t BgL_arg1215z00_776;

																							{	/* Object/predicate.scm 67 */
																								obj_t BgL_arg1216z00_777;
																								obj_t BgL_arg1218z00_778;

																								BgL_arg1216z00_777 =
																									(((BgL_variablez00_bglt)
																										COBJECT((
																												(BgL_variablez00_bglt)
																												BgL_holderz00_728)))->
																									BgL_idz00);
																								BgL_arg1218z00_778 =
																									MAKE_YOUNG_PAIR(((
																											(BgL_globalz00_bglt)
																											COBJECT
																											(BgL_holderz00_728))->
																										BgL_modulez00), BNIL);
																								BgL_arg1215z00_776 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1216z00_777,
																									BgL_arg1218z00_778);
																							}
																							BgL_arg1212z00_775 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(6), BgL_arg1215z00_776);
																						}
																						BgL_arg1210z00_774 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1212z00_775, BNIL);
																					}
																					BgL_arg1206z00_771 =
																						MAKE_YOUNG_PAIR(BgL_objz00_729,
																						BgL_arg1210z00_774);
																				}
																				BgL_tmpz00_1272 =
																					MAKE_YOUNG_PAIR(BgL_arg1203z00_770,
																					BgL_arg1206z00_771);
																			}
																			BgL_arg1194z00_762 =
																				MAKE_YOUNG_PAIR(BgL_tmpz00_1272, BNIL);
																		}
																		BgL_arg1190z00_759 =
																			MAKE_YOUNG_PAIR(BgL_arg1193z00_761,
																			BgL_arg1194z00_762);
																	}
																	BgL_arg1187z00_756 =
																		MAKE_YOUNG_PAIR(BgL_arg1189z00_758,
																		BgL_arg1190z00_759);
																}
																{	/* Object/predicate.scm 84 */
																	obj_t BgL_list1188z00_757;

																	BgL_list1188z00_757 =
																		MAKE_YOUNG_PAIR(BgL_srczd2defzd2_4, BNIL);
																	BgL_arg1182z00_754 =
																		BGl_epairifyza2za2zztools_miscz00
																		(BgL_arg1187z00_756, BgL_list1188z00_757);
																}
															}
															{	/* Object/predicate.scm 83 */
																obj_t BgL_list1183z00_755;

																BgL_list1183z00_755 =
																	MAKE_YOUNG_PAIR(BgL_arg1182z00_754, BNIL);
																return BgL_list1183z00_755;
															}
														}
													}
												else
													{	/* Object/predicate.scm 68 */
														return BNIL;
													}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &gen-class-pred! */
	obj_t BGl_z62genzd2classzd2predz12z70zzobject_predicatez00(obj_t
		BgL_envz00_1098, obj_t BgL_classz00_1099, obj_t BgL_srczd2defzd2_1100,
		obj_t BgL_importz00_1101)
	{
		{	/* Object/predicate.scm 59 */
			return
				BGl_genzd2classzd2predz12z12zzobject_predicatez00(
				((BgL_typez00_bglt) BgL_classz00_1099), BgL_srczd2defzd2_1100,
				BgL_importz00_1101);
		}

	}



/* import-class-pred! */
	BGL_EXPORTED_DEF obj_t
		BGl_importzd2classzd2predz12z12zzobject_predicatez00(BgL_typez00_bglt
		BgL_classz00_6, obj_t BgL_srczd2defzd2_7, obj_t BgL_modulez00_8)
	{
		{	/* Object/predicate.scm 92 */
			if (BGl_inlinezd2predzf3z21zzobject_predicatez00())
				{	/* Object/predicate.scm 93 */
					BGL_TAIL return
						BGl_genzd2classzd2predz12z12zzobject_predicatez00(BgL_classz00_6,
						BgL_srczd2defzd2_7, CNST_TABLE_REF(17));
				}
			else
				{	/* Object/predicate.scm 97 */
					obj_t BgL_idz00_789;

					BgL_idz00_789 =
						(((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_classz00_6)))->BgL_idz00);
					{	/* Object/predicate.scm 97 */
						obj_t BgL_idzf3zf3_790;

						{	/* Object/predicate.scm 98 */
							obj_t BgL_arg1239z00_801;

							{	/* Object/predicate.scm 98 */
								obj_t BgL_arg1242z00_802;
								obj_t BgL_arg1244z00_803;

								{	/* Object/predicate.scm 98 */
									obj_t BgL_arg1455z00_986;

									BgL_arg1455z00_986 = SYMBOL_TO_STRING(BgL_idz00_789);
									BgL_arg1242z00_802 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_986);
								}
								{	/* Object/predicate.scm 98 */
									obj_t BgL_symbolz00_987;

									BgL_symbolz00_987 = CNST_TABLE_REF(1);
									{	/* Object/predicate.scm 98 */
										obj_t BgL_arg1455z00_988;

										BgL_arg1455z00_988 = SYMBOL_TO_STRING(BgL_symbolz00_987);
										BgL_arg1244z00_803 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_988);
									}
								}
								BgL_arg1239z00_801 =
									string_append(BgL_arg1242z00_802, BgL_arg1244z00_803);
							}
							BgL_idzf3zf3_790 = bstring_to_symbol(BgL_arg1239z00_801);
						}
						{	/* Object/predicate.scm 98 */
							obj_t BgL_predzd2idzd2_791;

							{	/* Object/predicate.scm 99 */
								obj_t BgL_arg1234z00_798;

								{	/* Object/predicate.scm 99 */
									obj_t BgL_arg1236z00_799;
									obj_t BgL_arg1238z00_800;

									{	/* Object/predicate.scm 99 */
										obj_t BgL_arg1455z00_991;

										BgL_arg1455z00_991 = SYMBOL_TO_STRING(BgL_idz00_789);
										BgL_arg1236z00_799 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_991);
									}
									{	/* Object/predicate.scm 99 */
										obj_t BgL_symbolz00_992;

										BgL_symbolz00_992 = CNST_TABLE_REF(2);
										{	/* Object/predicate.scm 99 */
											obj_t BgL_arg1455z00_993;

											BgL_arg1455z00_993 = SYMBOL_TO_STRING(BgL_symbolz00_992);
											BgL_arg1238z00_800 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_993);
										}
									}
									BgL_arg1234z00_798 =
										string_append(BgL_arg1236z00_799, BgL_arg1238z00_800);
								}
								BgL_predzd2idzd2_791 = bstring_to_symbol(BgL_arg1234z00_798);
							}
							{	/* Object/predicate.scm 100 */
								obj_t BgL_superz00_793;

								{
									BgL_tclassz00_bglt BgL_auxz00_1317;

									{
										obj_t BgL_auxz00_1318;

										{	/* Object/predicate.scm 101 */
											BgL_objectz00_bglt BgL_tmpz00_1319;

											BgL_tmpz00_1319 = ((BgL_objectz00_bglt) BgL_classz00_6);
											BgL_auxz00_1318 = BGL_OBJECT_WIDENING(BgL_tmpz00_1319);
										}
										BgL_auxz00_1317 = ((BgL_tclassz00_bglt) BgL_auxz00_1318);
									}
									BgL_superz00_793 =
										(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_1317))->
										BgL_itszd2superzd2);
								}
								{	/* Object/predicate.scm 101 */

									{	/* Object/predicate.scm 102 */
										bool_t BgL_test1522z00_1324;

										{	/* Object/predicate.scm 102 */
											obj_t BgL_classz00_999;

											BgL_classz00_999 = BGl_tclassz00zzobject_classz00;
											if (BGL_OBJECTP(BgL_superz00_793))
												{	/* Object/predicate.scm 102 */
													BgL_objectz00_bglt BgL_arg1807z00_1001;

													BgL_arg1807z00_1001 =
														(BgL_objectz00_bglt) (BgL_superz00_793);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Object/predicate.scm 102 */
															long BgL_idxz00_1007;

															BgL_idxz00_1007 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1001);
															BgL_test1522z00_1324 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_1007 + 2L)) == BgL_classz00_999);
														}
													else
														{	/* Object/predicate.scm 102 */
															bool_t BgL_res1488z00_1032;

															{	/* Object/predicate.scm 102 */
																obj_t BgL_oclassz00_1015;

																{	/* Object/predicate.scm 102 */
																	obj_t BgL_arg1815z00_1023;
																	long BgL_arg1816z00_1024;

																	BgL_arg1815z00_1023 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Object/predicate.scm 102 */
																		long BgL_arg1817z00_1025;

																		BgL_arg1817z00_1025 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1001);
																		BgL_arg1816z00_1024 =
																			(BgL_arg1817z00_1025 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_1015 =
																		VECTOR_REF(BgL_arg1815z00_1023,
																		BgL_arg1816z00_1024);
																}
																{	/* Object/predicate.scm 102 */
																	bool_t BgL__ortest_1115z00_1016;

																	BgL__ortest_1115z00_1016 =
																		(BgL_classz00_999 == BgL_oclassz00_1015);
																	if (BgL__ortest_1115z00_1016)
																		{	/* Object/predicate.scm 102 */
																			BgL_res1488z00_1032 =
																				BgL__ortest_1115z00_1016;
																		}
																	else
																		{	/* Object/predicate.scm 102 */
																			long BgL_odepthz00_1017;

																			{	/* Object/predicate.scm 102 */
																				obj_t BgL_arg1804z00_1018;

																				BgL_arg1804z00_1018 =
																					(BgL_oclassz00_1015);
																				BgL_odepthz00_1017 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_1018);
																			}
																			if ((2L < BgL_odepthz00_1017))
																				{	/* Object/predicate.scm 102 */
																					obj_t BgL_arg1802z00_1020;

																					{	/* Object/predicate.scm 102 */
																						obj_t BgL_arg1803z00_1021;

																						BgL_arg1803z00_1021 =
																							(BgL_oclassz00_1015);
																						BgL_arg1802z00_1020 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_1021, 2L);
																					}
																					BgL_res1488z00_1032 =
																						(BgL_arg1802z00_1020 ==
																						BgL_classz00_999);
																				}
																			else
																				{	/* Object/predicate.scm 102 */
																					BgL_res1488z00_1032 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test1522z00_1324 = BgL_res1488z00_1032;
														}
												}
											else
												{	/* Object/predicate.scm 102 */
													BgL_test1522z00_1324 = ((bool_t) 0);
												}
										}
										if (BgL_test1522z00_1324)
											{	/* Object/predicate.scm 104 */
												obj_t BgL_arg1231z00_795;

												{	/* Object/predicate.scm 104 */
													obj_t BgL_arg1233z00_797;

													BgL_arg1233z00_797 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(9), BNIL);
													BgL_arg1231z00_795 =
														MAKE_YOUNG_PAIR(BgL_predzd2idzd2_791,
														BgL_arg1233z00_797);
												}
												BGl_importzd2parserzd2zzmodule_impusez00
													(BgL_modulez00_8, BgL_arg1231z00_795, BFALSE, BNIL);
											}
										else
											{	/* Object/predicate.scm 102 */
												BFALSE;
											}
									}
									return BNIL;
								}
							}
						}
					}
				}
		}

	}



/* &import-class-pred! */
	obj_t BGl_z62importzd2classzd2predz12z70zzobject_predicatez00(obj_t
		BgL_envz00_1102, obj_t BgL_classz00_1103, obj_t BgL_srczd2defzd2_1104,
		obj_t BgL_modulez00_1105)
	{
		{	/* Object/predicate.scm 92 */
			return
				BGl_importzd2classzd2predz12z12zzobject_predicatez00(
				((BgL_typez00_bglt) BgL_classz00_1103), BgL_srczd2defzd2_1104,
				BgL_modulez00_1105);
		}

	}



/* gen-java-class-pred! */
	obj_t
		BGl_genzd2javazd2classzd2predz12zc0zzobject_predicatez00(BgL_typez00_bglt
		BgL_classz00_9, obj_t BgL_srczd2defzd2_10, obj_t BgL_mclausez00_11)
	{
		{	/* Object/predicate.scm 114 */
			{	/* Object/predicate.scm 115 */
				obj_t BgL_idz00_804;

				BgL_idz00_804 =
					(((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_classz00_9)))->BgL_idz00);
				{	/* Object/predicate.scm 115 */
					obj_t BgL_idzf3zf3_805;

					{	/* Object/predicate.scm 116 */
						obj_t BgL_arg1335z00_843;

						{	/* Object/predicate.scm 116 */
							obj_t BgL_arg1339z00_844;
							obj_t BgL_arg1340z00_845;

							{	/* Object/predicate.scm 116 */
								obj_t BgL_arg1455z00_1035;

								BgL_arg1455z00_1035 = SYMBOL_TO_STRING(BgL_idz00_804);
								BgL_arg1339z00_844 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
									(BgL_arg1455z00_1035);
							}
							{	/* Object/predicate.scm 116 */
								obj_t BgL_symbolz00_1036;

								BgL_symbolz00_1036 = CNST_TABLE_REF(1);
								{	/* Object/predicate.scm 116 */
									obj_t BgL_arg1455z00_1037;

									BgL_arg1455z00_1037 = SYMBOL_TO_STRING(BgL_symbolz00_1036);
									BgL_arg1340z00_845 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_1037);
								}
							}
							BgL_arg1335z00_843 =
								string_append(BgL_arg1339z00_844, BgL_arg1340z00_845);
						}
						BgL_idzf3zf3_805 = bstring_to_symbol(BgL_arg1335z00_843);
					}
					{	/* Object/predicate.scm 116 */
						obj_t BgL_predzd2idzd2_806;

						{	/* Object/predicate.scm 117 */
							obj_t BgL_arg1331z00_840;

							{	/* Object/predicate.scm 117 */
								obj_t BgL_arg1332z00_841;
								obj_t BgL_arg1333z00_842;

								{	/* Object/predicate.scm 117 */
									obj_t BgL_arg1455z00_1040;

									BgL_arg1455z00_1040 = SYMBOL_TO_STRING(BgL_idz00_804);
									BgL_arg1332z00_841 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_1040);
								}
								{	/* Object/predicate.scm 117 */
									obj_t BgL_symbolz00_1041;

									BgL_symbolz00_1041 = CNST_TABLE_REF(2);
									{	/* Object/predicate.scm 117 */
										obj_t BgL_arg1455z00_1042;

										BgL_arg1455z00_1042 = SYMBOL_TO_STRING(BgL_symbolz00_1041);
										BgL_arg1333z00_842 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_1042);
									}
								}
								BgL_arg1331z00_840 =
									string_append(BgL_arg1332z00_841, BgL_arg1333z00_842);
							}
							BgL_predzd2idzd2_806 = bstring_to_symbol(BgL_arg1331z00_840);
						}
						{	/* Object/predicate.scm 117 */
							obj_t BgL_objz00_807;

							BgL_objz00_807 =
								BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
								(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(3)));
							{	/* Object/predicate.scm 119 */

								{

									{	/* Object/predicate.scm 124 */
										obj_t BgL_arg1248z00_810;

										{	/* Object/predicate.scm 124 */
											obj_t BgL_arg1249z00_811;

											{	/* Object/predicate.scm 124 */
												obj_t BgL_arg1252z00_812;

												{	/* Object/predicate.scm 124 */
													obj_t BgL_arg1268z00_813;

													{	/* Object/predicate.scm 124 */
														obj_t BgL_arg1272z00_814;

														BgL_arg1272z00_814 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(9), BNIL);
														BgL_arg1268z00_813 =
															MAKE_YOUNG_PAIR(BgL_predzd2idzd2_806,
															BgL_arg1272z00_814);
													}
													BgL_arg1252z00_812 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
														BgL_arg1268z00_813);
												}
												BgL_arg1249z00_811 =
													MAKE_YOUNG_PAIR(BgL_arg1252z00_812, BNIL);
											}
											BgL_arg1248z00_810 =
												MAKE_YOUNG_PAIR(BgL_mclausez00_11, BgL_arg1249z00_811);
										}
										BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
											(BgL_arg1248z00_810);
									}
									{	/* Object/predicate.scm 126 */
										obj_t BgL_arg1284z00_815;

										{	/* Object/predicate.scm 126 */
											obj_t BgL_arg1304z00_816;

											{	/* Object/predicate.scm 126 */
												obj_t BgL_arg1305z00_817;

												{	/* Object/predicate.scm 126 */
													obj_t BgL_arg1306z00_818;

													{	/* Object/predicate.scm 126 */
														obj_t BgL_arg1307z00_819;
														obj_t BgL_arg1308z00_820;

														{	/* Object/predicate.scm 126 */
															obj_t BgL_arg1310z00_821;

															{	/* Object/predicate.scm 126 */
																obj_t BgL_arg1311z00_822;

																BgL_arg1311z00_822 =
																	(((BgL_typez00_bglt) COBJECT(
																			((BgL_typez00_bglt) BgL_classz00_9)))->
																	BgL_idz00);
																BgL_arg1310z00_821 =
																	MAKE_YOUNG_PAIR(BgL_arg1311z00_822, BNIL);
															}
															BgL_arg1307z00_819 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
																BgL_arg1310z00_821);
														}
														{	/* Object/predicate.scm 126 */
															obj_t BgL_arg1312z00_823;

															{	/* Object/predicate.scm 126 */
																obj_t BgL_arg1314z00_824;

																BgL_arg1314z00_824 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(12), BNIL);
																BgL_arg1312z00_823 =
																	MAKE_YOUNG_PAIR(BgL_arg1314z00_824, BNIL);
															}
															BgL_arg1308z00_820 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(13),
																BgL_arg1312z00_823);
														}
														BgL_arg1306z00_818 =
															MAKE_YOUNG_PAIR(BgL_arg1307z00_819,
															BgL_arg1308z00_820);
													}
													BgL_arg1305z00_817 =
														MAKE_YOUNG_PAIR(BgL_idzf3zf3_805,
														BgL_arg1306z00_818);
												}
												BgL_arg1304z00_816 =
													MAKE_YOUNG_PAIR(BgL_arg1305z00_817, BNIL);
											}
											BgL_arg1284z00_815 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(14), BgL_arg1304z00_816);
										}
										BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
											(BgL_arg1284z00_815);
									}
									{	/* Object/predicate.scm 129 */
										obj_t BgL_arg1315z00_825;

										{	/* Object/predicate.scm 129 */
											obj_t BgL_arg1317z00_827;

											{	/* Object/predicate.scm 129 */
												obj_t BgL_arg1319z00_829;
												obj_t BgL_arg1320z00_830;

												if (BGl_inlinezd2predzf3z21zzobject_predicatez00())
													{	/* Object/predicate.scm 129 */
														BgL_arg1319z00_829 = CNST_TABLE_REF(15);
													}
												else
													{	/* Object/predicate.scm 129 */
														BgL_arg1319z00_829 = CNST_TABLE_REF(16);
													}
												{	/* Object/predicate.scm 130 */
													obj_t BgL_arg1322z00_832;
													obj_t BgL_arg1323z00_833;

													{	/* Object/predicate.scm 130 */
														obj_t BgL_arg1325z00_834;

														BgL_arg1325z00_834 =
															MAKE_YOUNG_PAIR(BgL_objz00_807, BNIL);
														BgL_arg1322z00_832 =
															MAKE_YOUNG_PAIR(BgL_predzd2idzd2_806,
															BgL_arg1325z00_834);
													}
													{	/* Object/predicate.scm 129 */
														obj_t BgL_tmpz00_1402;

														{	/* Object/predicate.scm 121 */
															obj_t BgL_list1328z00_837;

															BgL_list1328z00_837 =
																MAKE_YOUNG_PAIR(BgL_objz00_807, BNIL);
															BgL_tmpz00_1402 =
																BGl_makezd2privatezd2sexpz00zzast_privatez00
																(CNST_TABLE_REF(18), BgL_idz00_804,
																BgL_list1328z00_837);
														}
														BgL_arg1323z00_833 =
															MAKE_YOUNG_PAIR(BgL_tmpz00_1402, BNIL);
													}
													BgL_arg1320z00_830 =
														MAKE_YOUNG_PAIR(BgL_arg1322z00_832,
														BgL_arg1323z00_833);
												}
												BgL_arg1317z00_827 =
													MAKE_YOUNG_PAIR(BgL_arg1319z00_829,
													BgL_arg1320z00_830);
											}
											{	/* Object/predicate.scm 129 */
												obj_t BgL_list1318z00_828;

												BgL_list1318z00_828 =
													MAKE_YOUNG_PAIR(BgL_srczd2defzd2_10, BNIL);
												BgL_arg1315z00_825 =
													BGl_epairifyza2za2zztools_miscz00(BgL_arg1317z00_827,
													BgL_list1318z00_828);
											}
										}
										{	/* Object/predicate.scm 128 */
											obj_t BgL_list1316z00_826;

											BgL_list1316z00_826 =
												MAKE_YOUNG_PAIR(BgL_arg1315z00_825, BNIL);
											return BgL_list1316z00_826;
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* import-java-class-pred! */
	BGL_EXPORTED_DEF obj_t
		BGl_importzd2javazd2classzd2predz12zc0zzobject_predicatez00(BgL_typez00_bglt
		BgL_classz00_12, obj_t BgL_srczd2defzd2_13, obj_t BgL_modulez00_14)
	{
		{	/* Object/predicate.scm 137 */
			if (BGl_inlinezd2predzf3z21zzobject_predicatez00())
				{	/* Object/predicate.scm 142 */
					obj_t BgL_arg1342z00_847;

					if ((BgL_modulez00_14 == BGl_za2moduleza2z00zzmodule_modulez00))
						{	/* Object/predicate.scm 142 */
							BgL_arg1342z00_847 = CNST_TABLE_REF(19);
						}
					else
						{	/* Object/predicate.scm 142 */
							BgL_arg1342z00_847 = CNST_TABLE_REF(17);
						}
					BGL_TAIL return
						BGl_genzd2javazd2classzd2predz12zc0zzobject_predicatez00
						(BgL_classz00_12, BgL_srczd2defzd2_13, BgL_arg1342z00_847);
				}
			else
				{	/* Object/predicate.scm 144 */
					obj_t BgL_idz00_848;

					BgL_idz00_848 =
						(((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_classz00_12)))->BgL_idz00);
					{	/* Object/predicate.scm 144 */
						obj_t BgL_idzf3zf3_849;

						{	/* Object/predicate.scm 145 */
							obj_t BgL_arg1352z00_858;

							{	/* Object/predicate.scm 145 */
								obj_t BgL_arg1361z00_859;
								obj_t BgL_arg1364z00_860;

								{	/* Object/predicate.scm 145 */
									obj_t BgL_arg1455z00_1050;

									BgL_arg1455z00_1050 = SYMBOL_TO_STRING(BgL_idz00_848);
									BgL_arg1361z00_859 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_1050);
								}
								{	/* Object/predicate.scm 145 */
									obj_t BgL_symbolz00_1051;

									BgL_symbolz00_1051 = CNST_TABLE_REF(1);
									{	/* Object/predicate.scm 145 */
										obj_t BgL_arg1455z00_1052;

										BgL_arg1455z00_1052 = SYMBOL_TO_STRING(BgL_symbolz00_1051);
										BgL_arg1364z00_860 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_1052);
									}
								}
								BgL_arg1352z00_858 =
									string_append(BgL_arg1361z00_859, BgL_arg1364z00_860);
							}
							BgL_idzf3zf3_849 = bstring_to_symbol(BgL_arg1352z00_858);
						}
						{	/* Object/predicate.scm 145 */
							obj_t BgL_predzd2idzd2_850;

							{	/* Object/predicate.scm 146 */
								obj_t BgL_arg1348z00_855;

								{	/* Object/predicate.scm 146 */
									obj_t BgL_arg1349z00_856;
									obj_t BgL_arg1351z00_857;

									{	/* Object/predicate.scm 146 */
										obj_t BgL_arg1455z00_1055;

										BgL_arg1455z00_1055 = SYMBOL_TO_STRING(BgL_idz00_848);
										BgL_arg1349z00_856 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_1055);
									}
									{	/* Object/predicate.scm 146 */
										obj_t BgL_symbolz00_1056;

										BgL_symbolz00_1056 = CNST_TABLE_REF(2);
										{	/* Object/predicate.scm 146 */
											obj_t BgL_arg1455z00_1057;

											BgL_arg1455z00_1057 =
												SYMBOL_TO_STRING(BgL_symbolz00_1056);
											BgL_arg1351z00_857 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_1057);
										}
									}
									BgL_arg1348z00_855 =
										string_append(BgL_arg1349z00_856, BgL_arg1351z00_857);
								}
								BgL_predzd2idzd2_850 = bstring_to_symbol(BgL_arg1348z00_855);
							}
							{	/* Object/predicate.scm 147 */

								{	/* Object/predicate.scm 149 */
									obj_t BgL_arg1343z00_852;

									{	/* Object/predicate.scm 149 */
										obj_t BgL_arg1346z00_854;

										BgL_arg1346z00_854 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(9), BNIL);
										BgL_arg1343z00_852 =
											MAKE_YOUNG_PAIR(BgL_predzd2idzd2_850, BgL_arg1346z00_854);
									}
									BGl_importzd2parserzd2zzmodule_impusez00(BgL_modulez00_14,
										BgL_arg1343z00_852, BFALSE, BNIL);
								}
								return BNIL;
							}
						}
					}
				}
		}

	}



/* &import-java-class-pred! */
	obj_t BGl_z62importzd2javazd2classzd2predz12za2zzobject_predicatez00(obj_t
		BgL_envz00_1106, obj_t BgL_classz00_1107, obj_t BgL_srczd2defzd2_1108,
		obj_t BgL_modulez00_1109)
	{
		{	/* Object/predicate.scm 137 */
			return
				BGl_importzd2javazd2classzd2predz12zc0zzobject_predicatez00(
				((BgL_typez00_bglt) BgL_classz00_1107), BgL_srczd2defzd2_1108,
				BgL_modulez00_1109);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzobject_predicatez00(void)
	{
		{	/* Object/predicate.scm 21 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzobject_predicatez00(void)
	{
		{	/* Object/predicate.scm 21 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzobject_predicatez00(void)
	{
		{	/* Object/predicate.scm 21 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzobject_predicatez00(void)
	{
		{	/* Object/predicate.scm 21 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1491z00zzobject_predicatez00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1491z00zzobject_predicatez00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1491z00zzobject_predicatez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1491z00zzobject_predicatez00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1491z00zzobject_predicatez00));
			BGl_modulezd2initializa7ationz75zztype_toolsz00(453414928L,
				BSTRING_TO_STRING(BGl_string1491z00zzobject_predicatez00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1491z00zzobject_predicatez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1491z00zzobject_predicatez00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1491z00zzobject_predicatez00));
			BGl_modulezd2initializa7ationz75zzast_privatez00(135263837L,
				BSTRING_TO_STRING(BGl_string1491z00zzobject_predicatez00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1491z00zzobject_predicatez00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string1491z00zzobject_predicatez00));
			BGl_modulezd2initializa7ationz75zzobject_toolsz00(196511171L,
				BSTRING_TO_STRING(BGl_string1491z00zzobject_predicatez00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1491z00zzobject_predicatez00));
			BGl_modulezd2initializa7ationz75zzmodule_impusez00(478324304L,
				BSTRING_TO_STRING(BGl_string1491z00zzobject_predicatez00));
			return
				BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1491z00zzobject_predicatez00));
		}

	}

#ifdef __cplusplus
}
#endif
