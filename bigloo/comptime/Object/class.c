/*===========================================================================*/
/*   (Object/class.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Object/class.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_OBJECT_CLASS_TYPE_DEFINITIONS
#define BGL_OBJECT_CLASS_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_tclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		struct BgL_globalz00_bgl *BgL_holderz00;
		obj_t BgL_wideningz00;
		long BgL_depthz00;
		bool_t BgL_finalzf3zf3;
		obj_t BgL_constructorz00;
		obj_t BgL_virtualzd2slotszd2numberz00;
		bool_t BgL_abstractzf3zf3;
		obj_t BgL_widezd2typezd2;
		obj_t BgL_subclassesz00;
	}                *BgL_tclassz00_bglt;

	typedef struct BgL_jclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		obj_t BgL_packagez00;
	}                *BgL_jclassz00_bglt;

	typedef struct BgL_wclassz00_bgl
	{
		obj_t BgL_itszd2classzd2;
	}                *BgL_wclassz00_bglt;


#endif													// BGL_OBJECT_CLASS_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62lambda1846z62zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2occurrencezd2setz12z12zzobject_classz00(BgL_typez00_bglt, int);
	static obj_t BGl_z62lambda1847z62zzobject_classz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2slotszd2setz12z12zzobject_classz00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62tclasszd2constructorzb0zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2locationzd2zzobject_classz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2slotszd2setz12z12zzobject_classz00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2subclasseszd2setz12z12zzobject_classz00(BgL_typez00_bglt,
		obj_t);
	static obj_t BGl_z62jclasszd2locationzb0zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2aliaszd2setz12z12zzobject_classz00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2magiczf3zd2setz12ze1zzobject_classz00(BgL_typez00_bglt,
		bool_t);
	static BgL_globalz00_bglt BGl_z62lambda1690z62zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62jclasszd2z42zf2zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1691z62zzobject_classz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1772z62zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1853z62zzobject_classz00(obj_t, obj_t);
	static obj_t
		BGl_z62jclasszd2pointedzd2tozd2byzd2setz12z70zzobject_classz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda1773z62zzobject_classz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1854z62zzobject_classz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_wclasszd2magiczf3z21zzobject_classz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2aliaszd2setz12z12zzobject_classz00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62wclasszd2z42zd2setz12z32zzobject_classz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62jclasszd2packagezb0zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62wclasszd2coercezd2toz62zzobject_classz00(obj_t, obj_t);
	extern obj_t BGl_makezd2classzd2slotsz00zzobject_slotsz00(BgL_typez00_bglt,
		obj_t, obj_t, int, obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2siza7ezd2setz12zb5zzobject_classz00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzobject_classz00 = BUNSPEC;
	static obj_t BGl_z62tclasszd2slotszb0zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31774ze3ze5zzobject_classz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31855ze3ze5zzobject_classz00(obj_t);
	static BgL_typez00_bglt BGl_z62lambda1863z62zzobject_classz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62jclasszd2tvectorzb0zzobject_classz00(obj_t, obj_t);
	static obj_t
		BGl_z62wclasszd2importzd2locationzd2setz12za2zzobject_classz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_wclasszd2importzd2locationz00zzobject_classz00(BgL_typez00_bglt);
	static BgL_typez00_bglt BGl_z62lambda1867z62zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2widezd2typezd2setz12zc0zzobject_classz00(BgL_typez00_bglt,
		obj_t);
	static obj_t BGl_z62wclasszd2initzf3zd2setz12z83zzobject_classz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62jclasszd2namezd2setz12z70zzobject_classz00(obj_t, obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62lambda1870z62zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_wclasszd2parentszd2zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31848ze3ze5zzobject_classz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2importzd2locationz00zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31678ze3ze5zzobject_classz00(obj_t);
	static obj_t BGl_z62lambda1879z62zzobject_classz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62jclasszd2initzf3z43zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_tclassz00zzobject_classz00 = BUNSPEC;
	BGL_EXPORTED_DECL bool_t
		BGl_tclasszd2initzf3z21zzobject_classz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_widezd2classzf3z21zzobject_classz00(obj_t);
	BGL_EXPORTED_DECL long
		BGl_tclasszd2depthzd2zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62wclasszd2classzd2setz12z70zzobject_classz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62tclasszd2abstractzf3z43zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_declarezd2javazd2classzd2typez12zc0zzobject_classz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62tclasszd2classzb0zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31881ze3ze5zzobject_classz00(obj_t);
	static obj_t BGl_z62lambda1880z62zzobject_classz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62findzd2classzd2constructorz62zzobject_classz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2itszd2superzd2setz12zc0zzobject_classz00(BgL_typez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2itszd2superz00zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62tclasszd2tvectorzb0zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzobject_classz00(void);
	BGL_EXPORTED_DECL bool_t
		BGl_wclasszd2initzf3z21zzobject_classz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_jclasszd2magiczf3z21zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62tclasszf3z91zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2subclasseszd2zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62jclasszd2z42zd2setz12z32zzobject_classz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2pointedzd2tozd2byzd2setz12z12zzobject_classz00
		(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62wclasszd2locationzd2setz12z70zzobject_classz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_tclasszd2holderzd2zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62wclasszd2classzb0zzobject_classz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2slotszd2zzobject_classz00(BgL_typez00_bglt);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_wclasszd2itszd2classz00zzobject_classz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2namezd2zzobject_classz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_checkzd2classzd2declarationzf3zf3zzobject_classz00(BgL_typez00_bglt,
		obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62wclasszd2magiczf3zd2setz12z83zzobject_classz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2initzf3zd2setz12ze1zzobject_classz00(BgL_typez00_bglt, bool_t);
	static obj_t BGl_z62wclasszd2namezd2setz12z70zzobject_classz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_wclasszd2z42z90zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_genericzd2initzd2zzobject_classz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	extern BgL_typez00_bglt BGl_declarezd2typez12zc0zztype_envz00(obj_t, obj_t,
		obj_t);
	extern obj_t
		BGl_genzd2coercionzd2clausez12z12zzobject_coercionz00(BgL_typez00_bglt,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2initzf3zd2setz12ze1zzobject_classz00(BgL_typez00_bglt, bool_t);
	static BgL_typez00_bglt
		BGl_z62declarezd2classzd2typez12z70zzobject_classz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_wclasszf3zf3zzobject_classz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_wclasszd2locationzd2setz12z12zzobject_classz00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62tclasszd2widezd2typezd2setz12za2zzobject_classz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2widezd2typez00zzobject_classz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_wclasszd2parentszd2setz12z12zzobject_classz00(BgL_typez00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_getzd2objectzd2typez00zztype_cachez00(void);
	static obj_t BGl_objectzd2initzd2zzobject_classz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2packagezd2zzobject_classz00(BgL_typez00_bglt);
	static BgL_typez00_bglt BGl_z62makezd2tclasszb0zzobject_classz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_wclasszd2tvectorzd2setz12z12zzobject_classz00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2classzd2setz12z12zzobject_classz00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62jclasszd2idzb0zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62tclasszd2z42zf2zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2classzd2zzobject_classz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2classzd2setz12z12zzobject_classz00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2importzd2locationzd2setz12zc0zzobject_classz00
		(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_finalzd2classzf3z21zzobject_classz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_widezd2chunkzd2classzd2idzd2zzobject_classz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2virtualzd2slotszd2numberzd2setz12z12zzobject_classz00
		(BgL_typez00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62tclasszd2siza7ezd2setz12zd7zzobject_classz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2tvectorzd2zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31869ze3ze5zzobject_classz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_wclasszd2namezd2zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62findzd2commonzd2superzd2classzb0zzobject_classz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2sawza2z00zzengine_paramz00;
	BGL_EXPORTED_DECL bool_t
		BGl_tclasszd2finalzf3z21zzobject_classz00(BgL_typez00_bglt);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	static obj_t BGl_z62typezd2occurrencezd2incr1234z62zzobject_classz00(obj_t,
		obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	static obj_t BGl_z62tclasszd2itszd2superzd2setz12za2zzobject_classz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62jclasszf3z91zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62jclasszd2magiczf3zd2setz12z83zzobject_classz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62tclasszd2parentszd2setz12z70zzobject_classz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2z42zd2setz12z50zzobject_classz00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2virtualzd2slotszd2numberzd2zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62tclasszd2tvectorzd2setz12z70zzobject_classz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62wclasszd2occurrencezd2setz12z70zzobject_classz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2namezd2zzobject_classz00(BgL_typez00_bglt);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2tvectorzd2zzobject_classz00(BgL_typez00_bglt);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_z62tclasszd2occurrencezd2setz12z70zzobject_classz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62tclasszd2itszd2superz62zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62tclasszd2subclasseszd2setz12z70zzobject_classz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2z42z90zzobject_classz00(BgL_typez00_bglt);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_jclasszd2idzd2zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62jclasszd2aliaszb0zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2aliaszd2zzobject_classz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2siza7ezd2setz12zb5zzobject_classz00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_methodzd2initzd2zzobject_classz00(void);
	static obj_t BGl_z62wclasszd2tvectorzb0zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62wclasszd2occurrencezb0zzobject_classz00(obj_t, obj_t);
	extern obj_t
		BGl_typezd2occurrencezd2incrementz12z12zztype_typez00(BgL_typez00_bglt);
	static obj_t BGl_z62tclasszd2locationzd2setz12z70zzobject_classz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62widezd2classzf3z43zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2importzd2locationzd2setz12zc0zzobject_classz00
		(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62wclasszd2itszd2classz62zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classzd2predicatezd2zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62classzd2predicatezb0zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62wclasszd2importzd2locationz62zzobject_classz00(obj_t,
		obj_t);
	static obj_t BGl_z62jclasszd2slotszd2setz12z70zzobject_classz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_wclasszd2aliaszd2zzobject_classz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_makezd2jclasszd2zzobject_classz00(obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, bool_t, bool_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, int, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62tclasszd2slotszd2setz12z70zzobject_classz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2locationzd2zzobject_classz00(BgL_typez00_bglt);
	static bool_t
		BGl_checkzd2plainzd2classzd2declarationzf3z21zzobject_classz00
		(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62tclasszd2widezd2typez62zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62tclasszd2locationzb0zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62jclasszd2aliaszd2setz12z70zzobject_classz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_wclasszd2z42zd2setz12z50zzobject_classz00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62tclasszd2importzd2locationz62zzobject_classz00(obj_t,
		obj_t);
	static obj_t BGl_z62tclasszd2aliaszd2setz12z70zzobject_classz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62jclasszd2locationzd2setz12z70zzobject_classz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2locationzd2setz12z12zzobject_classz00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62classzd2allocatezb0zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2occurrencezd2setz12z12zzobject_classz00(BgL_typez00_bglt, int);
	static obj_t BGl_z62typezd2subclasszf3z43zzobject_classz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62jclasszd2namezb0zzobject_classz00(obj_t, obj_t);
	static obj_t
		BGl_z62tclasszd2virtualzd2slotszd2numberzd2setz12z70zzobject_classz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_makezd2wclasszd2zzobject_classz00(obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, bool_t, bool_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, int,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2coercezd2toz00zzobject_classz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2itszd2superz00zzobject_classz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2constructorzd2zzobject_classz00(BgL_typez00_bglt);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_declarezd2classzd2typez12z12zzobject_classz00(obj_t, BgL_globalz00_bglt,
		obj_t, bool_t, bool_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2coercezd2tozd2setz12zc0zzobject_classz00(BgL_typez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_wclasszd2siza7ezd2setz12zb5zzobject_classz00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL int
		BGl_jclasszd2occurrencezd2zzobject_classz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2locationzd2setz12z12zzobject_classz00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL int
		BGl_tclasszd2occurrencezd2zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62wclasszd2idzb0zzobject_classz00(obj_t, obj_t);
	static obj_t
		BGl_z62jclasszd2importzd2locationzd2setz12za2zzobject_classz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setzd2classzd2slotsz12z12zzobject_classz00(BgL_typez00_bglt, obj_t,
		obj_t);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	extern obj_t BGl_idzd2ze3namez31zzast_identz00(obj_t);
	static obj_t BGl_z62wclasszd2namezb0zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2depthzd2setz12z12zzobject_classz00(BgL_typez00_bglt, long);
	BGL_EXPORTED_DECL obj_t
		BGl_classzd2fillzd2zzobject_classz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2z42zd2setz12z50zzobject_classz00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2importzd2locationz00zzobject_classz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2parentszd2setz12z12zzobject_classz00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62tclasszd2subclasseszb0zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2tvectorzd2setz12z12zzobject_classz00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2z42z90zzobject_classz00(BgL_typez00_bglt);
	static obj_t
		BGl_z62tclasszd2virtualzd2slotszd2numberzb0zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62tclasszd2magiczf3z43zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_wclasszd2aliaszd2setz12z12zzobject_classz00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_jclassz00zzobject_classz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_wclasszd2tvectorzd2zzobject_classz00(BgL_typez00_bglt);
	static obj_t
		BGl_z62tclasszd2pointedzd2tozd2byzd2setz12z70zzobject_classz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62jclasszd2siza7ezd2setz12zd7zzobject_classz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62wclasszd2pointedzd2tozd2byzb0zzobject_classz00(obj_t,
		obj_t);
	static obj_t BGl_z62tclasszd2initzf3z43zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62tclasszd2depthzb0zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt BGl_wclasszd2nilzd2zzobject_classz00(void);
	static obj_t BGl_z62tclasszd2namezb0zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62jclasszd2initzf3zd2setz12z83zzobject_classz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2siza7ez75zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62tclasszd2initzf3zd2setz12z83zzobject_classz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_wclassz00zzobject_classz00 = BUNSPEC;
	static obj_t BGl_z62jclasszd2coercezd2tozd2setz12za2zzobject_classz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62classzd2fillzb0zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2namezd2setz12z12zzobject_classz00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62wclasszd2initzf3z43zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_wclasszd2coercezd2tozd2setz12zc0zzobject_classz00(BgL_typez00_bglt,
		obj_t);
	static obj_t BGl_z62tclasszd2magiczf3zd2setz12z83zzobject_classz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_jtypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_coercionz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_coercionz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	static obj_t BGl_z62jclasszd2classzd2setz12z70zzobject_classz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_wclasszd2pointedzd2tozd2byzd2zzobject_classz00(BgL_typez00_bglt);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_wclasszd2idzd2zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62jclasszd2slotszb0zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classzd2allocatezd2zzobject_classz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2slotszd2zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62tclasszd2classzd2setz12z70zzobject_classz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_typezd2subclasszf3z21zzobject_classz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31704ze3ze5zzobject_classz00(obj_t);
	static obj_t BGl_z62jclasszd2parentszb0zzobject_classz00(obj_t, obj_t);
	extern obj_t BGl_getzd2classzd2typez00zztype_cachez00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_wclasszd2magiczf3zd2setz12ze1zzobject_classz00(BgL_typez00_bglt,
		bool_t);
	static obj_t
		BGl_z62tclasszd2importzd2locationzd2setz12za2zzobject_classz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62tclasszd2coercezd2toz62zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62jclasszd2itszd2superz62zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classzd2makezd2zzobject_classz00(BgL_typez00_bglt);
	static BgL_typez00_bglt BGl_z62wclasszd2nilzb0zzobject_classz00(obj_t);
	extern obj_t
		BGl_getzd2localzd2virtualzd2slotszd2numberz00zzobject_slotsz00
		(BgL_typez00_bglt, obj_t);
	static obj_t
		BGl_checkzd2widezd2classzd2declarationzf3z21zzobject_classz00
		(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_tclasszf3zf3zzobject_classz00(obj_t);
	static obj_t BGl_z62jclasszd2parentszd2setz12z70zzobject_classz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31713ze3ze5zzobject_classz00(obj_t);
	static BgL_typez00_bglt BGl_z62makezd2jclasszb0zzobject_classz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62jclasszd2tvectorzd2setz12z70zzobject_classz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62wclasszd2siza7ezd2setz12zd7zzobject_classz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_wclasszd2siza7ez75zzobject_classz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2pointedzd2tozd2byzd2setz12z12zzobject_classz00
		(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62wclasszd2magiczf3z43zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_jclasszd2initzf3z21zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62jclasszd2classzb0zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2classzd2zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62finalzd2classzf3z43zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62setzd2classzd2slotsz12z70zzobject_classz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzobject_classz00(void);
	static obj_t BGl_z62tclasszd2parentszb0zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzobject_classz00(void);
	static BgL_typez00_bglt BGl_z62makezd2wclasszb0zzobject_classz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62classzd2makezb0zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_wclasszd2initzf3zd2setz12ze1zzobject_classz00(BgL_typez00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2magiczf3zd2setz12ze1zzobject_classz00(BgL_typez00_bglt,
		bool_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzobject_classz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzobject_classz00(void);
	static obj_t BGl_z62zc3z04anonymousza31723ze3ze5zzobject_classz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_wclasszd2classzd2zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31626ze3ze5zzobject_classz00(obj_t,
		obj_t);
	static obj_t BGl_z62wclasszf3z91zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2siza7ez75zzobject_classz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2coercezd2tozd2setz12zc0zzobject_classz00(BgL_typez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_wclasszd2locationzd2zzobject_classz00(BgL_typez00_bglt);
	extern obj_t BGl_parsezd2idzd2zzast_identz00(obj_t, obj_t);
	static obj_t BGl_z62wclasszd2locationzb0zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt BGl_tclasszd2nilzd2zzobject_classz00(void);
	static obj_t BGl_z62wclasszd2coercezd2tozd2setz12za2zzobject_classz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_genzd2classzd2coercersz12z12zzobject_coercionz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_wclasszd2itszd2classzd2setz12zc0zzobject_classz00(BgL_typez00_bglt,
		obj_t);
	static obj_t BGl_z62tclasszd2idzb0zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_wclasszd2classzd2setz12z12zzobject_classz00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2wideningzd2zzobject_classz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2coercezd2toz00zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62tclasszd2wideningzb0zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_typezd2classzd2namez00zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62jclasszd2occurrencezd2setz12z70zzobject_classz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_findzd2commonzd2superzd2classzd2zzobject_classz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_z62typezd2classzd2namez62zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_jclasszf3zf3zzobject_classz00(obj_t);
	static obj_t BGl_za2classzd2typezd2listza2z00zzobject_classz00 = BUNSPEC;
	static obj_t BGl_z62tclasszd2pointedzd2tozd2byzb0zzobject_classz00(obj_t,
		obj_t);
	static obj_t BGl_z62getzd2classzd2listz62zzobject_classz00(obj_t);
	static BgL_typez00_bglt BGl_z62lambda1614z62zzobject_classz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62jclasszd2magiczf3z43zzobject_classz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda1617z62zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62tclasszd2namezd2setz12z70zzobject_classz00(obj_t, obj_t,
		obj_t);
	static BgL_globalz00_bglt BGl_z62tclasszd2holderzb0zzobject_classz00(obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62tclasszd2nilzb0zzobject_classz00(obj_t);
	static obj_t
		BGl_z62wclasszd2pointedzd2tozd2byzd2setz12z70zzobject_classz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62tclasszd2aliaszb0zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2parentszd2zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62lambda1702z62zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31823ze3ze5zzobject_classz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1703z62zzobject_classz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_wclasszd2importzd2locationzd2setz12zc0zzobject_classz00
		(BgL_typez00_bglt, obj_t);
	static BgL_typez00_bglt BGl_z62lambda1627z62zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_findzd2classzd2constructorz00zzobject_classz00(BgL_typez00_bglt);
	static BgL_typez00_bglt
		BGl_z62declarezd2javazd2classzd2typez12za2zzobject_classz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62jclasszd2pointedzd2tozd2byzb0zzobject_classz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2pointedzd2tozd2byzd2zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62wclasszd2aliaszb0zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62jclasszd2siza7ez17zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31840ze3ze5zzobject_classz00(obj_t);
	static obj_t BGl_z62lambda1711z62zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1712z62zzobject_classz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31743ze3ze5zzobject_classz00(obj_t);
	static obj_t BGl_z62jclasszd2importzd2locationz62zzobject_classz00(obj_t,
		obj_t);
	static obj_t BGl_z62tclasszd2coercezd2tozd2setz12za2zzobject_classz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_heapzd2addzd2classz12z12zzobject_classz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_wclasszd2coercezd2toz00zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62jclasszd2occurrencezb0zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_tclasszd2superza2ze70z97zzobject_classz00(obj_t);
	static obj_t BGl_z62heapzd2addzd2classz12z70zzobject_classz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t
		BGl_globalzd2setzd2readzd2onlyz12zc0zzast_varz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_tclasszd2idzd2zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62tclasszd2occurrencezb0zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62wclasszd2itszd2classzd2setz12za2zzobject_classz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda1721z62zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31752ze3ze5zzobject_classz00(obj_t);
	static obj_t BGl_z62lambda1722z62zzobject_classz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2itszd2superzd2setz12zc0zzobject_classz00(BgL_typez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tclasszd2parentszd2zzobject_classz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2pointedzd2tozd2byzd2zzobject_classz00(BgL_typez00_bglt);
	static BgL_typez00_bglt BGl_z62lambda1809z62zzobject_classz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62checkzd2classzd2declarationzf3z91zzobject_classz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62wclasszd2z42zf2zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2parentszd2setz12z12zzobject_classz00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62tclasszd2finalzf3z43zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2namezd2setz12z12zzobject_classz00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62tclasszd2depthzd2setz12z70zzobject_classz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2tvectorzd2setz12z12zzobject_classz00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt BGl_jclasszd2nilzd2zzobject_classz00(void);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	static obj_t BGl_z62lambda1652z62zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1653z62zzobject_classz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1735z62zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jclasszd2aliaszd2zzobject_classz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_tclasszd2magiczf3z21zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62lambda1736z62zzobject_classz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62wclasszd2siza7ez17zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62tclasszd2z42zd2setz12z32zzobject_classz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62wclasszd2parentszb0zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_wclasszd2pointedzd2tozd2byzd2setz12z12zzobject_classz00
		(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62wclasszd2aliaszd2setz12z70zzobject_classz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_tclasszd2abstractzf3z21zzobject_classz00(BgL_typez00_bglt);
	BGL_IMPORT obj_t
		BGl_findzd2superzd2classzd2methodzd2zz__objectz00(BgL_objectz00_bglt, obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62lambda1821z62zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1741z62zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62jclasszd2coercezd2toz62zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1742z62zzobject_classz00(obj_t, obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda1824z62zzobject_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_getzd2classzd2listz00zzobject_classz00(void);
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t BGl_z62lambda1750z62zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1751z62zzobject_classz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_makezd2tclasszd2zzobject_classz00(obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, bool_t, bool_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, int, obj_t,
		obj_t, BgL_globalz00_bglt, obj_t, long, bool_t, obj_t, obj_t, bool_t, obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62jclasszd2nilzb0zzobject_classz00(obj_t);
	static obj_t BGl_z62wclasszd2parentszd2setz12z70zzobject_classz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62tclasszd2siza7ez17zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1676z62zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1838z62zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1677z62zzobject_classz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1839z62zzobject_classz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_wclasszd2occurrencezd2setz12z12zzobject_classz00(BgL_typez00_bglt, int);
	static obj_t BGl_z62wclasszd2tvectorzd2setz12z70zzobject_classz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_wclasszd2occurrencezd2zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_z62widezd2chunkzd2classzd2idzb0zzobject_classz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31764ze3ze5zzobject_classz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_wclasszd2namezd2setz12z12zzobject_classz00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62lambda1762z62zzobject_classz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1763z62zzobject_classz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62jclasszd2itszd2superzd2setz12za2zzobject_classz00(obj_t,
		obj_t, obj_t);
	static obj_t __cnst[30];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_jclasszd2parentszd2envz00zzobject_classz00,
		BgL_bgl_za762jclassza7d2pare2034z00,
		BGl_z62jclasszd2parentszb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2constructorzd2envz00zzobject_classz00,
		BgL_bgl_za762tclassza7d2cons2035z00,
		BGl_z62tclasszd2constructorzb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_tclasszd2tvectorzd2envz00zzobject_classz00,
		BgL_bgl_za762tclassza7d2tvec2036z00,
		BGl_z62tclasszd2tvectorzb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2pointedzd2tozd2byzd2envz00zzobject_classz00,
		BgL_bgl_za762tclassza7d2poin2037z00,
		BGl_z62tclasszd2pointedzd2tozd2byzb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2widezd2typezd2envzd2zzobject_classz00,
		BgL_bgl_za762tclassza7d2wide2038z00,
		BGl_z62tclasszd2widezd2typez62zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_wclasszd2locationzd2envz00zzobject_classz00,
		BgL_bgl_za762wclassza7d2loca2039z00,
		BGl_z62wclasszd2locationzb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2importzd2locationzd2envzd2zzobject_classz00,
		BgL_bgl_za762tclassza7d2impo2040z00,
		BGl_z62tclasszd2importzd2locationz62zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2classzd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762tclassza7d2clas2041z00,
		BGl_z62tclasszd2classzd2setz12z70zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_tclasszd2slotszd2envz00zzobject_classz00,
		BgL_bgl_za762tclassza7d2slot2042z00,
		BGl_z62tclasszd2slotszb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_typezd2classzd2namezd2envzd2zzobject_classz00,
		BgL_bgl_za762typeza7d2classza72043za7,
		BGl_z62typezd2classzd2namez62zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_wclasszd2magiczf3zd2setz12zd2envz33zzobject_classz00,
		BgL_bgl_za762wclassza7d2magi2044z00,
		BGl_z62wclasszd2magiczf3zd2setz12z83zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2subclasseszd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762tclassza7d2subc2045z00,
		BGl_z62tclasszd2subclasseszd2setz12z70zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jclasszd2aliaszd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762jclassza7d2alia2046z00,
		BGl_z62jclasszd2aliaszd2setz12z70zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2magiczf3zd2setz12zd2envz33zzobject_classz00,
		BgL_bgl_za762tclassza7d2magi2047z00,
		BGl_z62tclasszd2magiczf3zd2setz12z83zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2pointedzd2tozd2byzd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762tclassza7d2poin2048z00,
		BGl_z62tclasszd2pointedzd2tozd2byzd2setz12z70zzobject_classz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2abstractzf3zd2envzf3zzobject_classz00,
		BgL_bgl_za762tclassza7d2abst2049z00,
		BGl_z62tclasszd2abstractzf3z43zzobject_classz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_wclasszd2nilzd2envz00zzobject_classz00,
		BgL_bgl_za762wclassza7d2nilza72050za7,
		BGl_z62wclasszd2nilzb0zzobject_classz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2widezd2typezd2setz12zd2envz12zzobject_classz00,
		BgL_bgl_za762tclassza7d2wide2051z00,
		BGl_z62tclasszd2widezd2typezd2setz12za2zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_wclasszd2coercezd2tozd2setz12zd2envz12zzobject_classz00,
		BgL_bgl_za762wclassza7d2coer2052z00,
		BGl_z62wclasszd2coercezd2tozd2setz12za2zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jclasszd2occurrencezd2envz00zzobject_classz00,
		BgL_bgl_za762jclassza7d2occu2053z00,
		BGl_z62jclasszd2occurrencezb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_wclasszd2coercezd2tozd2envzd2zzobject_classz00,
		BgL_bgl_za762wclassza7d2coer2054z00,
		BGl_z62wclasszd2coercezd2toz62zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_wclasszd2pointedzd2tozd2byzd2envz00zzobject_classz00,
		BgL_bgl_za762wclassza7d2poin2055z00,
		BGl_z62wclasszd2pointedzd2tozd2byzb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jclasszd2importzd2locationzd2setz12zd2envz12zzobject_classz00,
		BgL_bgl_za762jclassza7d2impo2056z00,
		BGl_z62jclasszd2importzd2locationzd2setz12za2zzobject_classz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_tclasszd2wideningzd2envz00zzobject_classz00,
		BgL_bgl_za762tclassza7d2wide2057z00,
		BGl_z62tclasszd2wideningzb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2virtualzd2slotszd2numberzd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762tclassza7d2virt2058z00,
		BGl_z62tclasszd2virtualzd2slotszd2numberzd2setz12z70zzobject_classz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jclasszd2itszd2superzd2setz12zd2envz12zzobject_classz00,
		BgL_bgl_za762jclassza7d2itsza72059za7,
		BGl_z62jclasszd2itszd2superzd2setz12za2zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2occurrencezd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762tclassza7d2occu2060z00,
		BGl_z62tclasszd2occurrencezd2setz12z70zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_tclasszd2finalzf3zd2envzf3zzobject_classz00,
		BgL_bgl_za762tclassza7d2fina2061z00,
		BGl_z62tclasszd2finalzf3z43zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2depthzd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762tclassza7d2dept2062z00,
		BGl_z62tclasszd2depthzd2setz12z70zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jclasszd2importzd2locationzd2envzd2zzobject_classz00,
		BgL_bgl_za762jclassza7d2impo2063z00,
		BGl_z62jclasszd2importzd2locationz62zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2parentszd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762tclassza7d2pare2064z00,
		BGl_z62tclasszd2parentszd2setz12z70zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_wclasszd2siza7ezd2envza7zzobject_classz00,
		BgL_bgl_za762wclassza7d2siza7a2065za7,
		BGl_z62wclasszd2siza7ez17zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_wclasszd2magiczf3zd2envzf3zzobject_classz00,
		BgL_bgl_za762wclassza7d2magi2066z00,
		BGl_z62wclasszd2magiczf3z43zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_wclasszd2aliaszd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762wclassza7d2alia2067z00,
		BGl_z62wclasszd2aliaszd2setz12z70zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_tclasszd2classzd2envz00zzobject_classz00,
		BgL_bgl_za762tclassza7d2clas2068z00,
		BGl_z62tclasszd2classzb0zzobject_classz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_tclasszd2namezd2envz00zzobject_classz00,
		BgL_bgl_za762tclassza7d2name2069z00,
		BGl_z62tclasszd2namezb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_wclasszd2siza7ezd2setz12zd2envz67zzobject_classz00,
		BgL_bgl_za762wclassza7d2siza7a2070za7,
		BGl_z62wclasszd2siza7ezd2setz12zd7zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_wclasszd2parentszd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762wclassza7d2pare2071z00,
		BGl_z62wclasszd2parentszd2setz12z70zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_jclasszd2packagezd2envz00zzobject_classz00,
		BgL_bgl_za762jclassza7d2pack2072z00,
		BGl_z62jclasszd2packagezb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_wclasszd2occurrencezd2envz00zzobject_classz00,
		BgL_bgl_za762wclassza7d2occu2073z00,
		BGl_z62wclasszd2occurrencezb0zzobject_classz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_tclasszd2idzd2envz00zzobject_classz00,
		BgL_bgl_za762tclassza7d2idza7b2074za7,
		BGl_z62tclasszd2idzb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_tclasszd2initzf3zd2envzf3zzobject_classz00,
		BgL_bgl_za762tclassza7d2init2075z00,
		BGl_z62tclasszd2initzf3z43zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_typezd2subclasszf3zd2envzf3zzobject_classz00,
		BgL_bgl_za762typeza7d2subcla2076z00,
		BGl_z62typezd2subclasszf3z43zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_declarezd2javazd2classzd2typez12zd2envz12zzobject_classz00,
		BgL_bgl_za762declareza7d2jav2077z00,
		BGl_z62declarezd2javazd2classzd2typez12za2zzobject_classz00, 0L, BUNSPEC,
		5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_tclasszd2parentszd2envz00zzobject_classz00,
		BgL_bgl_za762tclassza7d2pare2078z00,
		BGl_z62tclasszd2parentszb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jclasszd2parentszd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762jclassza7d2pare2079z00,
		BGl_z62jclasszd2parentszd2setz12z70zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_tclasszd2locationzd2envz00zzobject_classz00,
		BgL_bgl_za762tclassza7d2loca2080z00,
		BGl_z62tclasszd2locationzb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jclasszd2pointedzd2tozd2byzd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762jclassza7d2poin2081z00,
		BGl_z62jclasszd2pointedzd2tozd2byzd2setz12z70zzobject_classz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_wclasszd2z42zd2setz12zd2envz82zzobject_classz00,
		BgL_bgl_za762wclassza7d2za742za72082z00,
		BGl_z62wclasszd2z42zd2setz12z32zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2aliaszd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762tclassza7d2alia2083z00,
		BGl_z62tclasszd2aliaszd2setz12z70zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_jclasszd2slotszd2envz00zzobject_classz00,
		BgL_bgl_za762jclassza7d2slot2084z00,
		BGl_z62jclasszd2slotszb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jclasszd2occurrencezd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762jclassza7d2occu2085z00,
		BGl_z62jclasszd2occurrencezd2setz12z70zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_wclasszd2importzd2locationzd2setz12zd2envz12zzobject_classz00,
		BgL_bgl_za762wclassza7d2impo2086z00,
		BGl_z62wclasszd2importzd2locationzd2setz12za2zzobject_classz00, 0L, BUNSPEC,
		2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2wclasszd2envz00zzobject_classz00,
		BgL_bgl_za762makeza7d2wclass2087z00,
		BGl_z62makezd2wclasszb0zzobject_classz00, 0L, BUNSPEC, 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2coercezd2tozd2envzd2zzobject_classz00,
		BgL_bgl_za762tclassza7d2coer2088z00,
		BGl_z62tclasszd2coercezd2toz62zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jclasszd2initzf3zd2setz12zd2envz33zzobject_classz00,
		BgL_bgl_za762jclassza7d2init2089z00,
		BGl_z62jclasszd2initzf3zd2setz12z83zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_wclasszd2importzd2locationzd2envzd2zzobject_classz00,
		BgL_bgl_za762wclassza7d2impo2090z00,
		BGl_z62wclasszd2importzd2locationz62zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jclasszd2coercezd2tozd2setz12zd2envz12zzobject_classz00,
		BgL_bgl_za762jclassza7d2coer2091z00,
		BGl_z62jclasszd2coercezd2tozd2setz12za2zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2classzd2listzd2envzd2zzobject_classz00,
		BgL_bgl_za762getza7d2classza7d2092za7,
		BGl_z62getzd2classzd2listz62zzobject_classz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_jclasszd2z42zd2envz42zzobject_classz00,
		BgL_bgl_za762jclassza7d2za742za72093z00,
		BGl_z62jclasszd2z42zf2zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jclasszd2locationzd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762jclassza7d2loca2094z00,
		BGl_z62jclasszd2locationzd2setz12z70zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2occurrencezd2envz00zzobject_classz00,
		BgL_bgl_za762tclassza7d2occu2095z00,
		BGl_z62tclasszd2occurrencezb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_tclasszd2aliaszd2envz00zzobject_classz00,
		BgL_bgl_za762tclassza7d2alia2096z00,
		BGl_z62tclasszd2aliaszb0zzobject_classz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_wclasszd2idzd2envz00zzobject_classz00,
		BgL_bgl_za762wclassza7d2idza7b2097za7,
		BGl_z62wclasszd2idzb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_widezd2classzf3zd2envzf3zzobject_classz00,
		BgL_bgl_za762wideza7d2classza72098za7,
		BGl_z62widezd2classzf3z43zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jclasszd2coercezd2tozd2envzd2zzobject_classz00,
		BgL_bgl_za762jclassza7d2coer2099z00,
		BGl_z62jclasszd2coercezd2toz62zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_findzd2commonzd2superzd2classzd2envz00zzobject_classz00,
		BgL_bgl_za762findza7d2common2100z00,
		BGl_z62findzd2commonzd2superzd2classzb0zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jclasszd2slotszd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762jclassza7d2slot2101z00,
		BGl_z62jclasszd2slotszd2setz12z70zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2itszd2superzd2envzd2zzobject_classz00,
		BgL_bgl_za762tclassza7d2itsza72102za7,
		BGl_z62tclasszd2itszd2superz62zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2tvectorzd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762tclassza7d2tvec2103z00,
		BGl_z62tclasszd2tvectorzd2setz12z70zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2siza7ezd2setz12zd2envz67zzobject_classz00,
		BgL_bgl_za762tclassza7d2siza7a2104za7,
		BGl_z62tclasszd2siza7ezd2setz12zd7zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_checkzd2classzd2declarationzf3zd2envz21zzobject_classz00,
		BgL_bgl_za762checkza7d2class2105z00,
		BGl_z62checkzd2classzd2declarationzf3z91zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jclasszd2itszd2superzd2envzd2zzobject_classz00,
		BgL_bgl_za762jclassza7d2itsza72106za7,
		BGl_z62jclasszd2itszd2superz62zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_wclasszd2initzf3zd2setz12zd2envz33zzobject_classz00,
		BgL_bgl_za762wclassza7d2init2107z00,
		BGl_z62wclasszd2initzf3zd2setz12z83zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jclasszd2magiczf3zd2setz12zd2envz33zzobject_classz00,
		BgL_bgl_za762jclassza7d2magi2108z00,
		BGl_z62jclasszd2magiczf3zd2setz12z83zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_jclasszd2siza7ezd2envza7zzobject_classz00,
		BgL_bgl_za762jclassza7d2siza7a2109za7,
		BGl_z62jclasszd2siza7ez17zzobject_classz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1961z00zzobject_classz00,
		BgL_bgl_string1961za700za7za7o2110za7, "#!", 2);
	      DEFINE_STRING(BGl_string1962z00zzobject_classz00,
		BgL_bgl_string1962za700za7za7o2111za7, "struct ", 7);
	      DEFINE_STRING(BGl_string1963z00zzobject_classz00,
		BgL_bgl_string1963za700za7za7o2112za7, "_bgl", 4);
	      DEFINE_STRING(BGl_string1964z00zzobject_classz00,
		BgL_bgl_string1964za700za7za7o2113za7, "_bglt", 5);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2tclasszd2envz00zzobject_classz00,
		BgL_bgl_za762makeza7d2tclass2114z00,
		BGl_z62makezd2tclasszb0zzobject_classz00, 0L, BUNSPEC, 26);
	      DEFINE_STRING(BGl_string1965z00zzobject_classz00,
		BgL_bgl_string1965za700za7za7o2115za7, "super of \"~a\" is not a class",
		28);
	      DEFINE_STRING(BGl_string1966z00zzobject_classz00,
		BgL_bgl_string1966za700za7za7o2116za7, "check-plain-class-declation?", 28);
	      DEFINE_STRING(BGl_string1967z00zzobject_classz00,
		BgL_bgl_string1967za700za7za7o2117za7,
		"Should not be able to see a wide class here", 43);
	      DEFINE_STRING(BGl_string1968z00zzobject_classz00,
		BgL_bgl_string1968za700za7za7o2118za7, "super of \"~a\" is a wide class",
		29);
	      DEFINE_STRING(BGl_string1969z00zzobject_classz00,
		BgL_bgl_string1969za700za7za7o2119za7,
		"Only wide classes can inherit of final classes", 46);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_jclasszd2idzd2envz00zzobject_classz00,
		BgL_bgl_za762jclassza7d2idza7b2120za7,
		BGl_z62jclasszd2idzb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_wclasszd2tvectorzd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762wclassza7d2tvec2121z00,
		BGl_z62wclasszd2tvectorzd2setz12z70zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_wclasszd2occurrencezd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762wclassza7d2occu2122z00,
		BGl_z62wclasszd2occurrencezd2setz12z70zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jclasszd2siza7ezd2setz12zd2envz67zzobject_classz00,
		BgL_bgl_za762jclassza7d2siza7a2123za7,
		BGl_z62jclasszd2siza7ezd2setz12zd7zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_jclasszd2classzd2envz00zzobject_classz00,
		BgL_bgl_za762jclassza7d2clas2124z00,
		BGl_z62jclasszd2classzb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_wclasszd2itszd2classzd2setz12zd2envz12zzobject_classz00,
		BgL_bgl_za762wclassza7d2itsza72125za7,
		BGl_z62wclasszd2itszd2classzd2setz12za2zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzd2allocatezd2envz00zzobject_classz00,
		BgL_bgl_za762classza7d2alloc2126z00,
		BGl_z62classzd2allocatezb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_tclasszd2depthzd2envz00zzobject_classz00,
		BgL_bgl_za762tclassza7d2dept2127z00,
		BGl_z62tclasszd2depthzb0zzobject_classz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1970z00zzobject_classz00,
		BgL_bgl_string1970za700za7za7o2128za7,
		"A class can't be \"wide\" and \"final\"", 35);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2importzd2locationzd2setz12zd2envz12zzobject_classz00,
		BgL_bgl_za762tclassza7d2impo2129z00,
		BGl_z62tclasszd2importzd2locationzd2setz12za2zzobject_classz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string1971z00zzobject_classz00,
		BgL_bgl_string1971za700za7za7o2130za7,
		"super of wide class \"~a\" is not a final class", 45);
	      DEFINE_STRING(BGl_string1972z00zzobject_classz00,
		BgL_bgl_string1972za700za7za7o2131za7, "check-wide-class-declaration?", 29);
	      DEFINE_STRING(BGl_string1973z00zzobject_classz00,
		BgL_bgl_string1973za700za7za7o2132za7,
		"Should not be able to see a plain class here", 44);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_wclasszd2namezd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762wclassza7d2name2133z00,
		BGl_z62wclasszd2namezd2setz12z70zzobject_classz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_wclasszd2namezd2envz00zzobject_classz00,
		BgL_bgl_za762wclassza7d2name2134z00,
		BGl_z62wclasszd2namezb0zzobject_classz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_jclasszf3zd2envz21zzobject_classz00,
		BgL_bgl_za762jclassza7f3za791za72135z00,
		BGl_z62jclasszf3z91zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jclasszd2tvectorzd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762jclassza7d2tvec2136z00,
		BGl_z62jclasszd2tvectorzd2setz12z70zzobject_classz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1974z00zzobject_classz00,
		BgL_bgl_za762lambda1653za7622137z00, BGl_z62lambda1653z62zzobject_classz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1975z00zzobject_classz00,
		BgL_bgl_za762lambda1652za7622138z00, BGl_z62lambda1652z62zzobject_classz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1976z00zzobject_classz00,
		BgL_bgl_za762za7c3za704anonymo2139za7,
		BGl_z62zc3z04anonymousza31678ze3ze5zzobject_classz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1977z00zzobject_classz00,
		BgL_bgl_za762lambda1677za7622140z00, BGl_z62lambda1677z62zzobject_classz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1978z00zzobject_classz00,
		BgL_bgl_za762lambda1676za7622141z00, BGl_z62lambda1676z62zzobject_classz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1979z00zzobject_classz00,
		BgL_bgl_za762lambda1691za7622142z00, BGl_z62lambda1691z62zzobject_classz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_jclasszd2initzf3zd2envzf3zzobject_classz00,
		BgL_bgl_za762jclassza7d2init2143z00,
		BGl_z62jclasszd2initzf3z43zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_wclasszd2tvectorzd2envz00zzobject_classz00,
		BgL_bgl_za762wclassza7d2tvec2144z00,
		BGl_z62wclasszd2tvectorzb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2locationzd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762tclassza7d2loca2145z00,
		BGl_z62tclasszd2locationzd2setz12z70zzobject_classz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1980z00zzobject_classz00,
		BgL_bgl_za762lambda1690za7622146z00, BGl_z62lambda1690z62zzobject_classz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1981z00zzobject_classz00,
		BgL_bgl_za762za7c3za704anonymo2147za7,
		BGl_z62zc3z04anonymousza31704ze3ze5zzobject_classz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1982z00zzobject_classz00,
		BgL_bgl_za762lambda1703za7622148z00, BGl_z62lambda1703z62zzobject_classz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1983z00zzobject_classz00,
		BgL_bgl_za762lambda1702za7622149z00, BGl_z62lambda1702z62zzobject_classz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1984z00zzobject_classz00,
		BgL_bgl_za762za7c3za704anonymo2150za7,
		BGl_z62zc3z04anonymousza31713ze3ze5zzobject_classz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1985z00zzobject_classz00,
		BgL_bgl_za762lambda1712za7622151z00, BGl_z62lambda1712z62zzobject_classz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1986z00zzobject_classz00,
		BgL_bgl_za762lambda1711za7622152z00, BGl_z62lambda1711z62zzobject_classz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1987z00zzobject_classz00,
		BgL_bgl_za762za7c3za704anonymo2153za7,
		BGl_z62zc3z04anonymousza31723ze3ze5zzobject_classz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_findzd2classzd2constructorzd2envzd2zzobject_classz00,
		BgL_bgl_za762findza7d2classza72154za7,
		BGl_z62findzd2classzd2constructorz62zzobject_classz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1988z00zzobject_classz00,
		BgL_bgl_za762lambda1722za7622155z00, BGl_z62lambda1722z62zzobject_classz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1989z00zzobject_classz00,
		BgL_bgl_za762lambda1721za7622156z00, BGl_z62lambda1721z62zzobject_classz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2initzf3zd2setz12zd2envz33zzobject_classz00,
		BgL_bgl_za762tclassza7d2init2157z00,
		BGl_z62tclasszd2initzf3zd2setz12z83zzobject_classz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_wclasszf3zd2envz21zzobject_classz00,
		BgL_bgl_za762wclassza7f3za791za72158z00,
		BGl_z62wclasszf3z91zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_wclasszd2classzd2envz00zzobject_classz00,
		BgL_bgl_za762wclassza7d2clas2159z00,
		BGl_z62wclasszd2classzb0zzobject_classz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1990z00zzobject_classz00,
		BgL_bgl_za762lambda1736za7622160z00, BGl_z62lambda1736z62zzobject_classz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1991z00zzobject_classz00,
		BgL_bgl_za762lambda1735za7622161z00, BGl_z62lambda1735z62zzobject_classz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1992z00zzobject_classz00,
		BgL_bgl_za762za7c3za704anonymo2162za7,
		BGl_z62zc3z04anonymousza31743ze3ze5zzobject_classz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1993z00zzobject_classz00,
		BgL_bgl_za762lambda1742za7622163z00, BGl_z62lambda1742z62zzobject_classz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1994z00zzobject_classz00,
		BgL_bgl_za762lambda1741za7622164z00, BGl_z62lambda1741z62zzobject_classz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1995z00zzobject_classz00,
		BgL_bgl_za762za7c3za704anonymo2165za7,
		BGl_z62zc3z04anonymousza31752ze3ze5zzobject_classz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1996z00zzobject_classz00,
		BgL_bgl_za762lambda1751za7622166z00, BGl_z62lambda1751z62zzobject_classz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1997z00zzobject_classz00,
		BgL_bgl_za762lambda1750za7622167z00, BGl_z62lambda1750z62zzobject_classz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1998z00zzobject_classz00,
		BgL_bgl_za762za7c3za704anonymo2168za7,
		BGl_z62zc3z04anonymousza31764ze3ze5zzobject_classz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1999z00zzobject_classz00,
		BgL_bgl_za762lambda1763za7622169z00, BGl_z62lambda1763z62zzobject_classz00,
		0L, BUNSPEC, 2);
	extern obj_t BGl_typezd2occurrencezd2incrementz12zd2envzc0zztype_typez00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_finalzd2classzf3zd2envzf3zzobject_classz00,
		BgL_bgl_za762finalza7d2class2170z00,
		BGl_z62finalzd2classzf3z43zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_widezd2chunkzd2classzd2idzd2envz00zzobject_classz00,
		BgL_bgl_za762wideza7d2chunkza72171za7,
		BGl_z62widezd2chunkzd2classzd2idzb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2classzd2slotsz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762setza7d2classza7d2172za7,
		BGl_z62setzd2classzd2slotsz12z70zzobject_classz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_jclasszd2tvectorzd2envz00zzobject_classz00,
		BgL_bgl_za762jclassza7d2tvec2173z00,
		BGl_z62jclasszd2tvectorzb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2itszd2superzd2setz12zd2envz12zzobject_classz00,
		BgL_bgl_za762tclassza7d2itsza72174za7,
		BGl_z62tclasszd2itszd2superzd2setz12za2zzobject_classz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzd2makezd2envz00zzobject_classz00,
		BgL_bgl_za762classza7d2makeza72175za7,
		BGl_z62classzd2makezb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_jclasszd2locationzd2envz00zzobject_classz00,
		BgL_bgl_za762jclassza7d2loca2176z00,
		BGl_z62jclasszd2locationzb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2slotszd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762tclassza7d2slot2177z00,
		BGl_z62tclasszd2slotszd2setz12z70zzobject_classz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_tclasszf3zd2envz21zzobject_classz00,
		BgL_bgl_za762tclassza7f3za791za72178z00,
		BGl_z62tclasszf3z91zzobject_classz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzd2fillzd2envz00zzobject_classz00,
		BgL_bgl_za762classza7d2fillza72179za7,
		BGl_z62classzd2fillzb0zzobject_classz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2jclasszd2envz00zzobject_classz00,
		BgL_bgl_za762makeza7d2jclass2180z00,
		BGl_z62makezd2jclasszb0zzobject_classz00, 0L, BUNSPEC, 18);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_wclasszd2initzf3zd2envzf3zzobject_classz00,
		BgL_bgl_za762wclassza7d2init2181z00,
		BGl_z62wclasszd2initzf3z43zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_jclasszd2magiczf3zd2envzf3zzobject_classz00,
		BgL_bgl_za762jclassza7d2magi2182z00,
		BGl_z62jclasszd2magiczf3z43zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_tclasszd2holderzd2envz00zzobject_classz00,
		BgL_bgl_za762tclassza7d2hold2183z00,
		BGl_z62tclasszd2holderzb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_jclasszd2aliaszd2envz00zzobject_classz00,
		BgL_bgl_za762jclassza7d2alia2184z00,
		BGl_z62jclasszd2aliaszb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_tclasszd2magiczf3zd2envzf3zzobject_classz00,
		BgL_bgl_za762tclassza7d2magi2185z00,
		BGl_z62tclasszd2magiczf3z43zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_wclasszd2pointedzd2tozd2byzd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762wclassza7d2poin2186z00,
		BGl_z62wclasszd2pointedzd2tozd2byzd2setz12z70zzobject_classz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_wclasszd2itszd2classzd2envzd2zzobject_classz00,
		BgL_bgl_za762wclassza7d2itsza72187za7,
		BGl_z62wclasszd2itszd2classz62zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_declarezd2classzd2typez12zd2envzc0zzobject_classz00,
		BgL_bgl_za762declareza7d2cla2188z00,
		BGl_z62declarezd2classzd2typez12z70zzobject_classz00, 0L, BUNSPEC, 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_wclasszd2locationzd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762wclassza7d2loca2189z00,
		BGl_z62wclasszd2locationzd2setz12z70zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2namezd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762tclassza7d2name2190z00,
		BGl_z62tclasszd2namezd2setz12z70zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_heapzd2addzd2classz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762heapza7d2addza7d22191za7,
		BGl_z62heapzd2addzd2classz12z70zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jclasszd2classzd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762jclassza7d2clas2192z00,
		BGl_z62jclasszd2classzd2setz12z70zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzd2predicatezd2envz00zzobject_classz00,
		BgL_bgl_za762classza7d2predi2193z00,
		BGl_z62classzd2predicatezb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jclasszd2pointedzd2tozd2byzd2envz00zzobject_classz00,
		BgL_bgl_za762jclassza7d2poin2194z00,
		BGl_z62jclasszd2pointedzd2tozd2byzb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_wclasszd2aliaszd2envz00zzobject_classz00,
		BgL_bgl_za762wclassza7d2alia2195z00,
		BGl_z62wclasszd2aliaszb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jclasszd2namezd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762jclassza7d2name2196z00,
		BGl_z62jclasszd2namezd2setz12z70zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jclasszd2z42zd2setz12zd2envz82zzobject_classz00,
		BgL_bgl_za762jclassza7d2za742za72197z00,
		BGl_z62jclasszd2z42zd2setz12z32zzobject_classz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_jclasszd2nilzd2envz00zzobject_classz00,
		BgL_bgl_za762jclassza7d2nilza72198za7,
		BGl_z62jclasszd2nilzb0zzobject_classz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2z42zd2setz12zd2envz82zzobject_classz00,
		BgL_bgl_za762tclassza7d2za742za72199z00,
		BGl_z62tclasszd2z42zd2setz12z32zzobject_classz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_tclasszd2z42zd2envz42zzobject_classz00,
		BgL_bgl_za762tclassza7d2za742za72200z00,
		BGl_z62tclasszd2z42zf2zzobject_classz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_jclasszd2namezd2envz00zzobject_classz00,
		BgL_bgl_za762jclassza7d2name2201z00,
		BGl_z62jclasszd2namezb0zzobject_classz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_wclasszd2z42zd2envz42zzobject_classz00,
		BgL_bgl_za762wclassza7d2za742za72202z00,
		BGl_z62wclasszd2z42zf2zzobject_classz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2000z00zzobject_classz00,
		BgL_bgl_za762lambda1762za7622203z00, BGl_z62lambda1762z62zzobject_classz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2001z00zzobject_classz00,
		BgL_bgl_za762za7c3za704anonymo2204za7,
		BGl_z62zc3z04anonymousza31774ze3ze5zzobject_classz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2002z00zzobject_classz00,
		BgL_bgl_za762lambda1773za7622205z00, BGl_z62lambda1773z62zzobject_classz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2003z00zzobject_classz00,
		BgL_bgl_za762lambda1772za7622206z00, BGl_z62lambda1772z62zzobject_classz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2004z00zzobject_classz00,
		BgL_bgl_za762lambda1627za7622207z00, BGl_z62lambda1627z62zzobject_classz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2005z00zzobject_classz00,
		BgL_bgl_za762za7c3za704anonymo2208za7,
		BGl_z62zc3z04anonymousza31626ze3ze5zzobject_classz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2006z00zzobject_classz00,
		BgL_bgl_za762lambda1617za7622209z00, BGl_z62lambda1617z62zzobject_classz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2007z00zzobject_classz00,
		BgL_bgl_za762lambda1614za7622210z00, BGl_z62lambda1614z62zzobject_classz00,
		0L, BUNSPEC, 27);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2008z00zzobject_classz00,
		BgL_bgl_za762za7c3za704anonymo2211za7,
		BGl_z62zc3z04anonymousza31840ze3ze5zzobject_classz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2009z00zzobject_classz00,
		BgL_bgl_za762lambda1839za7622212z00, BGl_z62lambda1839z62zzobject_classz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_tclasszd2siza7ezd2envza7zzobject_classz00,
		BgL_bgl_za762tclassza7d2siza7a2213za7,
		BGl_z62tclasszd2siza7ez17zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2coercezd2tozd2setz12zd2envz12zzobject_classz00,
		BgL_bgl_za762tclassza7d2coer2214z00,
		BGl_z62tclasszd2coercezd2tozd2setz12za2zzobject_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_wclasszd2parentszd2envz00zzobject_classz00,
		BgL_bgl_za762wclassza7d2pare2215z00,
		BGl_z62wclasszd2parentszb0zzobject_classz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_tclasszd2nilzd2envz00zzobject_classz00,
		BgL_bgl_za762tclassza7d2nilza72216za7,
		BGl_z62tclasszd2nilzb0zzobject_classz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2010z00zzobject_classz00,
		BgL_bgl_za762lambda1838za7622217z00, BGl_z62lambda1838z62zzobject_classz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2011z00zzobject_classz00,
		BgL_bgl_za762za7c3za704anonymo2218za7,
		BGl_z62zc3z04anonymousza31848ze3ze5zzobject_classz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2012z00zzobject_classz00,
		BgL_bgl_za762lambda1847za7622219z00, BGl_z62lambda1847z62zzobject_classz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2013z00zzobject_classz00,
		BgL_bgl_za762lambda1846za7622220z00, BGl_z62lambda1846z62zzobject_classz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2014z00zzobject_classz00,
		BgL_bgl_za762za7c3za704anonymo2221za7,
		BGl_z62zc3z04anonymousza31855ze3ze5zzobject_classz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2015z00zzobject_classz00,
		BgL_bgl_za762lambda1854za7622222z00, BGl_z62lambda1854z62zzobject_classz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2016z00zzobject_classz00,
		BgL_bgl_za762lambda1853za7622223z00, BGl_z62lambda1853z62zzobject_classz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2017z00zzobject_classz00,
		BgL_bgl_za762lambda1824za7622224z00, BGl_z62lambda1824z62zzobject_classz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2018z00zzobject_classz00,
		BgL_bgl_za762za7c3za704anonymo2225za7,
		BGl_z62zc3z04anonymousza31823ze3ze5zzobject_classz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2019z00zzobject_classz00,
		BgL_bgl_za762lambda1821za7622226z00, BGl_z62lambda1821z62zzobject_classz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_wclasszd2classzd2setz12zd2envzc0zzobject_classz00,
		BgL_bgl_za762wclassza7d2clas2227z00,
		BGl_z62wclasszd2classzd2setz12z70zzobject_classz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2028z00zzobject_classz00,
		BgL_bgl_string2028za700za7za7o2228za7, "", 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2020z00zzobject_classz00,
		BgL_bgl_za762lambda1809za7622229z00, BGl_z62lambda1809z62zzobject_classz00,
		0L, BUNSPEC, 19);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2021z00zzobject_classz00,
		BgL_bgl_za762za7c3za704anonymo2230za7,
		BGl_z62zc3z04anonymousza31881ze3ze5zzobject_classz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2022z00zzobject_classz00,
		BgL_bgl_za762lambda1880za7622231z00, BGl_z62lambda1880z62zzobject_classz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2023z00zzobject_classz00,
		BgL_bgl_za762lambda1879za7622232z00, BGl_z62lambda1879z62zzobject_classz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2030z00zzobject_classz00,
		BgL_bgl_string2030za700za7za7o2233za7, "type-occurrence-increment!", 26);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2024z00zzobject_classz00,
		BgL_bgl_za762lambda1870za7622234z00, BGl_z62lambda1870z62zzobject_classz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2031z00zzobject_classz00,
		BgL_bgl_string2031za700za7za7o2235za7, "object_class", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2025z00zzobject_classz00,
		BgL_bgl_za762za7c3za704anonymo2236za7,
		BGl_z62zc3z04anonymousza31869ze3ze5zzobject_classz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2032z00zzobject_classz00,
		BgL_bgl_string2032za700za7za7o2237za7,
		"_ wclass its-class jclass bstring package object_class tclass pair-nil subclasses wide-type abstract? virtual-slots-number constructor bool final? long depth holder slots obj its-super %allocate- ? fill- ! make- java widening bigloo ",
		233);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2026z00zzobject_classz00,
		BgL_bgl_za762lambda1867za7622238z00, BGl_z62lambda1867z62zzobject_classz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2027z00zzobject_classz00,
		BgL_bgl_za762lambda1863za7622239z00, BGl_z62lambda1863z62zzobject_classz00,
		0L, BUNSPEC, 17);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2029z00zzobject_classz00,
		BgL_bgl_za762typeza7d2occurr2240z00,
		BGl_z62typezd2occurrencezd2incr1234z62zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2subclasseszd2envz00zzobject_classz00,
		BgL_bgl_za762tclassza7d2subc2241z00,
		BGl_z62tclasszd2subclasseszb0zzobject_classz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tclasszd2virtualzd2slotszd2numberzd2envz00zzobject_classz00,
		BgL_bgl_za762tclassza7d2virt2242z00,
		BGl_z62tclasszd2virtualzd2slotszd2numberzb0zzobject_classz00, 0L, BUNSPEC,
		1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzobject_classz00));
		     ADD_ROOT((void *) (&BGl_tclassz00zzobject_classz00));
		     ADD_ROOT((void *) (&BGl_jclassz00zzobject_classz00));
		     ADD_ROOT((void *) (&BGl_wclassz00zzobject_classz00));
		   
			 ADD_ROOT((void *) (&BGl_za2classzd2typezd2listza2z00zzobject_classz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long
		BgL_checksumz00_3612, char *BgL_fromz00_3613)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzobject_classz00))
				{
					BGl_requirezd2initializa7ationz75zzobject_classz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzobject_classz00();
					BGl_libraryzd2moduleszd2initz00zzobject_classz00();
					BGl_cnstzd2initzd2zzobject_classz00();
					BGl_importedzd2moduleszd2initz00zzobject_classz00();
					BGl_objectzd2initzd2zzobject_classz00();
					BGl_methodzd2initzd2zzobject_classz00();
					return BGl_toplevelzd2initzd2zzobject_classz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzobject_classz00(void)
	{
		{	/* Object/class.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "object_class");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "object_class");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"object_class");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "object_class");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"object_class");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "object_class");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"object_class");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"object_class");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "object_class");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"object_class");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzobject_classz00(void)
	{
		{	/* Object/class.scm 15 */
			{	/* Object/class.scm 15 */
				obj_t BgL_cportz00_3427;

				{	/* Object/class.scm 15 */
					obj_t BgL_stringz00_3434;

					BgL_stringz00_3434 = BGl_string2032z00zzobject_classz00;
					{	/* Object/class.scm 15 */
						obj_t BgL_startz00_3435;

						BgL_startz00_3435 = BINT(0L);
						{	/* Object/class.scm 15 */
							obj_t BgL_endz00_3436;

							BgL_endz00_3436 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3434)));
							{	/* Object/class.scm 15 */

								BgL_cportz00_3427 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3434, BgL_startz00_3435, BgL_endz00_3436);
				}}}}
				{
					long BgL_iz00_3428;

					BgL_iz00_3428 = 29L;
				BgL_loopz00_3429:
					if ((BgL_iz00_3428 == -1L))
						{	/* Object/class.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Object/class.scm 15 */
							{	/* Object/class.scm 15 */
								obj_t BgL_arg2033z00_3430;

								{	/* Object/class.scm 15 */

									{	/* Object/class.scm 15 */
										obj_t BgL_locationz00_3432;

										BgL_locationz00_3432 = BBOOL(((bool_t) 0));
										{	/* Object/class.scm 15 */

											BgL_arg2033z00_3430 =
												BGl_readz00zz__readerz00(BgL_cportz00_3427,
												BgL_locationz00_3432);
										}
									}
								}
								{	/* Object/class.scm 15 */
									int BgL_tmpz00_3643;

									BgL_tmpz00_3643 = (int) (BgL_iz00_3428);
									CNST_TABLE_SET(BgL_tmpz00_3643, BgL_arg2033z00_3430);
							}}
							{	/* Object/class.scm 15 */
								int BgL_auxz00_3433;

								BgL_auxz00_3433 = (int) ((BgL_iz00_3428 - 1L));
								{
									long BgL_iz00_3648;

									BgL_iz00_3648 = (long) (BgL_auxz00_3433);
									BgL_iz00_3428 = BgL_iz00_3648;
									goto BgL_loopz00_3429;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzobject_classz00(void)
	{
		{	/* Object/class.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzobject_classz00(void)
	{
		{	/* Object/class.scm 15 */
			BGl_za2classzd2typezd2listza2z00zzobject_classz00 = BNIL;
			return BUNSPEC;
		}

	}



/* make-tclass */
	BGL_EXPORTED_DEF BgL_typez00_bglt BGl_makezd2tclasszd2zzobject_classz00(obj_t
		BgL_id1179z00_3, obj_t BgL_name1180z00_4, obj_t BgL_siza7e1181za7_5,
		obj_t BgL_class1182z00_6, obj_t BgL_coercezd2to1183zd2_7,
		obj_t BgL_parents1184z00_8, bool_t BgL_initzf31185zf3_9,
		bool_t BgL_magiczf31186zf3_10, obj_t BgL_z421187z42_11,
		obj_t BgL_alias1188z00_12, obj_t BgL_pointedzd2tozd2by1189z00_13,
		obj_t BgL_tvector1190z00_14, obj_t BgL_location1191z00_15,
		obj_t BgL_importzd2location1192zd2_16, int BgL_occurrence1193z00_17,
		obj_t BgL_itszd2super1194zd2_18, obj_t BgL_slots1195z00_19,
		BgL_globalz00_bglt BgL_holder1196z00_20, obj_t BgL_widening1197z00_21,
		long BgL_depth1198z00_22, bool_t BgL_finalzf31199zf3_23,
		obj_t BgL_constructor1200z00_24,
		obj_t BgL_virtualzd2slotszd2number1201z00_25,
		bool_t BgL_abstractzf31202zf3_26, obj_t BgL_widezd2type1203zd2_27,
		obj_t BgL_subclasses1204z00_28)
	{
		{	/* Object/class.sch 146 */
			{	/* Object/class.sch 146 */
				BgL_typez00_bglt BgL_new1147z00_3438;

				{	/* Object/class.sch 146 */
					BgL_typez00_bglt BgL_tmp1145z00_3439;
					BgL_tclassz00_bglt BgL_wide1146z00_3440;

					{
						BgL_typez00_bglt BgL_auxz00_3651;

						{	/* Object/class.sch 146 */
							BgL_typez00_bglt BgL_new1144z00_3441;

							BgL_new1144z00_3441 =
								((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_typez00_bgl))));
							{	/* Object/class.sch 146 */
								long BgL_arg1242z00_3442;

								BgL_arg1242z00_3442 = BGL_CLASS_NUM(BGl_typez00zztype_typez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1144z00_3441),
									BgL_arg1242z00_3442);
							}
							{	/* Object/class.sch 146 */
								BgL_objectz00_bglt BgL_tmpz00_3656;

								BgL_tmpz00_3656 = ((BgL_objectz00_bglt) BgL_new1144z00_3441);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3656, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1144z00_3441);
							BgL_auxz00_3651 = BgL_new1144z00_3441;
						}
						BgL_tmp1145z00_3439 = ((BgL_typez00_bglt) BgL_auxz00_3651);
					}
					BgL_wide1146z00_3440 =
						((BgL_tclassz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_tclassz00_bgl))));
					{	/* Object/class.sch 146 */
						obj_t BgL_auxz00_3664;
						BgL_objectz00_bglt BgL_tmpz00_3662;

						BgL_auxz00_3664 = ((obj_t) BgL_wide1146z00_3440);
						BgL_tmpz00_3662 = ((BgL_objectz00_bglt) BgL_tmp1145z00_3439);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3662, BgL_auxz00_3664);
					}
					((BgL_objectz00_bglt) BgL_tmp1145z00_3439);
					{	/* Object/class.sch 146 */
						long BgL_arg1239z00_3443;

						BgL_arg1239z00_3443 = BGL_CLASS_NUM(BGl_tclassz00zzobject_classz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1145z00_3439), BgL_arg1239z00_3443);
					}
					BgL_new1147z00_3438 = ((BgL_typez00_bglt) BgL_tmp1145z00_3439);
				}
				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_new1147z00_3438)))->BgL_idz00) =
					((obj_t) BgL_id1179z00_3), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1147z00_3438)))->BgL_namez00) =
					((obj_t) BgL_name1180z00_4), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1147z00_3438)))->BgL_siza7eza7) =
					((obj_t) BgL_siza7e1181za7_5), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1147z00_3438)))->BgL_classz00) =
					((obj_t) BgL_class1182z00_6), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1147z00_3438)))->BgL_coercezd2tozd2) =
					((obj_t) BgL_coercezd2to1183zd2_7), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1147z00_3438)))->BgL_parentsz00) =
					((obj_t) BgL_parents1184z00_8), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1147z00_3438)))->BgL_initzf3zf3) =
					((bool_t) BgL_initzf31185zf3_9), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1147z00_3438)))->BgL_magiczf3zf3) =
					((bool_t) BgL_magiczf31186zf3_10), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1147z00_3438)))->BgL_nullz00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1147z00_3438)))->BgL_z42z42) =
					((obj_t) BgL_z421187z42_11), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1147z00_3438)))->BgL_aliasz00) =
					((obj_t) BgL_alias1188z00_12), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1147z00_3438)))->BgL_pointedzd2tozd2byz00) =
					((obj_t) BgL_pointedzd2tozd2by1189z00_13), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1147z00_3438)))->BgL_tvectorz00) =
					((obj_t) BgL_tvector1190z00_14), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1147z00_3438)))->BgL_locationz00) =
					((obj_t) BgL_location1191z00_15), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1147z00_3438)))->BgL_importzd2locationzd2) =
					((obj_t) BgL_importzd2location1192zd2_16), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1147z00_3438)))->BgL_occurrencez00) =
					((int) BgL_occurrence1193z00_17), BUNSPEC);
				{
					BgL_tclassz00_bglt BgL_auxz00_3704;

					{
						obj_t BgL_auxz00_3705;

						{	/* Object/class.sch 146 */
							BgL_objectz00_bglt BgL_tmpz00_3706;

							BgL_tmpz00_3706 = ((BgL_objectz00_bglt) BgL_new1147z00_3438);
							BgL_auxz00_3705 = BGL_OBJECT_WIDENING(BgL_tmpz00_3706);
						}
						BgL_auxz00_3704 = ((BgL_tclassz00_bglt) BgL_auxz00_3705);
					}
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3704))->
							BgL_itszd2superzd2) =
						((obj_t) BgL_itszd2super1194zd2_18), BUNSPEC);
				}
				{
					BgL_tclassz00_bglt BgL_auxz00_3711;

					{
						obj_t BgL_auxz00_3712;

						{	/* Object/class.sch 146 */
							BgL_objectz00_bglt BgL_tmpz00_3713;

							BgL_tmpz00_3713 = ((BgL_objectz00_bglt) BgL_new1147z00_3438);
							BgL_auxz00_3712 = BGL_OBJECT_WIDENING(BgL_tmpz00_3713);
						}
						BgL_auxz00_3711 = ((BgL_tclassz00_bglt) BgL_auxz00_3712);
					}
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3711))->BgL_slotsz00) =
						((obj_t) BgL_slots1195z00_19), BUNSPEC);
				}
				{
					BgL_tclassz00_bglt BgL_auxz00_3718;

					{
						obj_t BgL_auxz00_3719;

						{	/* Object/class.sch 146 */
							BgL_objectz00_bglt BgL_tmpz00_3720;

							BgL_tmpz00_3720 = ((BgL_objectz00_bglt) BgL_new1147z00_3438);
							BgL_auxz00_3719 = BGL_OBJECT_WIDENING(BgL_tmpz00_3720);
						}
						BgL_auxz00_3718 = ((BgL_tclassz00_bglt) BgL_auxz00_3719);
					}
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3718))->BgL_holderz00) =
						((BgL_globalz00_bglt) BgL_holder1196z00_20), BUNSPEC);
				}
				{
					BgL_tclassz00_bglt BgL_auxz00_3725;

					{
						obj_t BgL_auxz00_3726;

						{	/* Object/class.sch 146 */
							BgL_objectz00_bglt BgL_tmpz00_3727;

							BgL_tmpz00_3727 = ((BgL_objectz00_bglt) BgL_new1147z00_3438);
							BgL_auxz00_3726 = BGL_OBJECT_WIDENING(BgL_tmpz00_3727);
						}
						BgL_auxz00_3725 = ((BgL_tclassz00_bglt) BgL_auxz00_3726);
					}
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3725))->BgL_wideningz00) =
						((obj_t) BgL_widening1197z00_21), BUNSPEC);
				}
				{
					BgL_tclassz00_bglt BgL_auxz00_3732;

					{
						obj_t BgL_auxz00_3733;

						{	/* Object/class.sch 146 */
							BgL_objectz00_bglt BgL_tmpz00_3734;

							BgL_tmpz00_3734 = ((BgL_objectz00_bglt) BgL_new1147z00_3438);
							BgL_auxz00_3733 = BGL_OBJECT_WIDENING(BgL_tmpz00_3734);
						}
						BgL_auxz00_3732 = ((BgL_tclassz00_bglt) BgL_auxz00_3733);
					}
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3732))->BgL_depthz00) =
						((long) BgL_depth1198z00_22), BUNSPEC);
				}
				{
					BgL_tclassz00_bglt BgL_auxz00_3739;

					{
						obj_t BgL_auxz00_3740;

						{	/* Object/class.sch 146 */
							BgL_objectz00_bglt BgL_tmpz00_3741;

							BgL_tmpz00_3741 = ((BgL_objectz00_bglt) BgL_new1147z00_3438);
							BgL_auxz00_3740 = BGL_OBJECT_WIDENING(BgL_tmpz00_3741);
						}
						BgL_auxz00_3739 = ((BgL_tclassz00_bglt) BgL_auxz00_3740);
					}
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3739))->BgL_finalzf3zf3) =
						((bool_t) BgL_finalzf31199zf3_23), BUNSPEC);
				}
				{
					BgL_tclassz00_bglt BgL_auxz00_3746;

					{
						obj_t BgL_auxz00_3747;

						{	/* Object/class.sch 146 */
							BgL_objectz00_bglt BgL_tmpz00_3748;

							BgL_tmpz00_3748 = ((BgL_objectz00_bglt) BgL_new1147z00_3438);
							BgL_auxz00_3747 = BGL_OBJECT_WIDENING(BgL_tmpz00_3748);
						}
						BgL_auxz00_3746 = ((BgL_tclassz00_bglt) BgL_auxz00_3747);
					}
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3746))->
							BgL_constructorz00) =
						((obj_t) BgL_constructor1200z00_24), BUNSPEC);
				}
				{
					BgL_tclassz00_bglt BgL_auxz00_3753;

					{
						obj_t BgL_auxz00_3754;

						{	/* Object/class.sch 146 */
							BgL_objectz00_bglt BgL_tmpz00_3755;

							BgL_tmpz00_3755 = ((BgL_objectz00_bglt) BgL_new1147z00_3438);
							BgL_auxz00_3754 = BGL_OBJECT_WIDENING(BgL_tmpz00_3755);
						}
						BgL_auxz00_3753 = ((BgL_tclassz00_bglt) BgL_auxz00_3754);
					}
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3753))->
							BgL_virtualzd2slotszd2numberz00) =
						((obj_t) BgL_virtualzd2slotszd2number1201z00_25), BUNSPEC);
				}
				{
					BgL_tclassz00_bglt BgL_auxz00_3760;

					{
						obj_t BgL_auxz00_3761;

						{	/* Object/class.sch 146 */
							BgL_objectz00_bglt BgL_tmpz00_3762;

							BgL_tmpz00_3762 = ((BgL_objectz00_bglt) BgL_new1147z00_3438);
							BgL_auxz00_3761 = BGL_OBJECT_WIDENING(BgL_tmpz00_3762);
						}
						BgL_auxz00_3760 = ((BgL_tclassz00_bglt) BgL_auxz00_3761);
					}
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3760))->
							BgL_abstractzf3zf3) =
						((bool_t) BgL_abstractzf31202zf3_26), BUNSPEC);
				}
				{
					BgL_tclassz00_bglt BgL_auxz00_3767;

					{
						obj_t BgL_auxz00_3768;

						{	/* Object/class.sch 146 */
							BgL_objectz00_bglt BgL_tmpz00_3769;

							BgL_tmpz00_3769 = ((BgL_objectz00_bglt) BgL_new1147z00_3438);
							BgL_auxz00_3768 = BGL_OBJECT_WIDENING(BgL_tmpz00_3769);
						}
						BgL_auxz00_3767 = ((BgL_tclassz00_bglt) BgL_auxz00_3768);
					}
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3767))->
							BgL_widezd2typezd2) =
						((obj_t) BgL_widezd2type1203zd2_27), BUNSPEC);
				}
				{
					BgL_tclassz00_bglt BgL_auxz00_3774;

					{
						obj_t BgL_auxz00_3775;

						{	/* Object/class.sch 146 */
							BgL_objectz00_bglt BgL_tmpz00_3776;

							BgL_tmpz00_3776 = ((BgL_objectz00_bglt) BgL_new1147z00_3438);
							BgL_auxz00_3775 = BGL_OBJECT_WIDENING(BgL_tmpz00_3776);
						}
						BgL_auxz00_3774 = ((BgL_tclassz00_bglt) BgL_auxz00_3775);
					}
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3774))->
							BgL_subclassesz00) = ((obj_t) BgL_subclasses1204z00_28), BUNSPEC);
				}
				return BgL_new1147z00_3438;
			}
		}

	}



/* &make-tclass */
	BgL_typez00_bglt BGl_z62makezd2tclasszb0zzobject_classz00(obj_t
		BgL_envz00_2805, obj_t BgL_id1179z00_2806, obj_t BgL_name1180z00_2807,
		obj_t BgL_siza7e1181za7_2808, obj_t BgL_class1182z00_2809,
		obj_t BgL_coercezd2to1183zd2_2810, obj_t BgL_parents1184z00_2811,
		obj_t BgL_initzf31185zf3_2812, obj_t BgL_magiczf31186zf3_2813,
		obj_t BgL_z421187z42_2814, obj_t BgL_alias1188z00_2815,
		obj_t BgL_pointedzd2tozd2by1189z00_2816, obj_t BgL_tvector1190z00_2817,
		obj_t BgL_location1191z00_2818, obj_t BgL_importzd2location1192zd2_2819,
		obj_t BgL_occurrence1193z00_2820, obj_t BgL_itszd2super1194zd2_2821,
		obj_t BgL_slots1195z00_2822, obj_t BgL_holder1196z00_2823,
		obj_t BgL_widening1197z00_2824, obj_t BgL_depth1198z00_2825,
		obj_t BgL_finalzf31199zf3_2826, obj_t BgL_constructor1200z00_2827,
		obj_t BgL_virtualzd2slotszd2number1201z00_2828,
		obj_t BgL_abstractzf31202zf3_2829, obj_t BgL_widezd2type1203zd2_2830,
		obj_t BgL_subclasses1204z00_2831)
	{
		{	/* Object/class.sch 146 */
			return
				BGl_makezd2tclasszd2zzobject_classz00(BgL_id1179z00_2806,
				BgL_name1180z00_2807, BgL_siza7e1181za7_2808, BgL_class1182z00_2809,
				BgL_coercezd2to1183zd2_2810, BgL_parents1184z00_2811,
				CBOOL(BgL_initzf31185zf3_2812), CBOOL(BgL_magiczf31186zf3_2813),
				BgL_z421187z42_2814, BgL_alias1188z00_2815,
				BgL_pointedzd2tozd2by1189z00_2816, BgL_tvector1190z00_2817,
				BgL_location1191z00_2818, BgL_importzd2location1192zd2_2819,
				CINT(BgL_occurrence1193z00_2820), BgL_itszd2super1194zd2_2821,
				BgL_slots1195z00_2822, ((BgL_globalz00_bglt) BgL_holder1196z00_2823),
				BgL_widening1197z00_2824, (long) CINT(BgL_depth1198z00_2825),
				CBOOL(BgL_finalzf31199zf3_2826), BgL_constructor1200z00_2827,
				BgL_virtualzd2slotszd2number1201z00_2828,
				CBOOL(BgL_abstractzf31202zf3_2829), BgL_widezd2type1203zd2_2830,
				BgL_subclasses1204z00_2831);
		}

	}



/* tclass? */
	BGL_EXPORTED_DEF bool_t BGl_tclasszf3zf3zzobject_classz00(obj_t BgL_objz00_29)
	{
		{	/* Object/class.sch 147 */
			{	/* Object/class.sch 147 */
				obj_t BgL_classz00_3444;

				BgL_classz00_3444 = BGl_tclassz00zzobject_classz00;
				if (BGL_OBJECTP(BgL_objz00_29))
					{	/* Object/class.sch 147 */
						BgL_objectz00_bglt BgL_arg1807z00_3445;

						BgL_arg1807z00_3445 = (BgL_objectz00_bglt) (BgL_objz00_29);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Object/class.sch 147 */
								long BgL_idxz00_3446;

								BgL_idxz00_3446 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3445);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3446 + 2L)) == BgL_classz00_3444);
							}
						else
							{	/* Object/class.sch 147 */
								bool_t BgL_res1904z00_3449;

								{	/* Object/class.sch 147 */
									obj_t BgL_oclassz00_3450;

									{	/* Object/class.sch 147 */
										obj_t BgL_arg1815z00_3451;
										long BgL_arg1816z00_3452;

										BgL_arg1815z00_3451 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Object/class.sch 147 */
											long BgL_arg1817z00_3453;

											BgL_arg1817z00_3453 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3445);
											BgL_arg1816z00_3452 = (BgL_arg1817z00_3453 - OBJECT_TYPE);
										}
										BgL_oclassz00_3450 =
											VECTOR_REF(BgL_arg1815z00_3451, BgL_arg1816z00_3452);
									}
									{	/* Object/class.sch 147 */
										bool_t BgL__ortest_1115z00_3454;

										BgL__ortest_1115z00_3454 =
											(BgL_classz00_3444 == BgL_oclassz00_3450);
										if (BgL__ortest_1115z00_3454)
											{	/* Object/class.sch 147 */
												BgL_res1904z00_3449 = BgL__ortest_1115z00_3454;
											}
										else
											{	/* Object/class.sch 147 */
												long BgL_odepthz00_3455;

												{	/* Object/class.sch 147 */
													obj_t BgL_arg1804z00_3456;

													BgL_arg1804z00_3456 = (BgL_oclassz00_3450);
													BgL_odepthz00_3455 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3456);
												}
												if ((2L < BgL_odepthz00_3455))
													{	/* Object/class.sch 147 */
														obj_t BgL_arg1802z00_3457;

														{	/* Object/class.sch 147 */
															obj_t BgL_arg1803z00_3458;

															BgL_arg1803z00_3458 = (BgL_oclassz00_3450);
															BgL_arg1802z00_3457 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3458,
																2L);
														}
														BgL_res1904z00_3449 =
															(BgL_arg1802z00_3457 == BgL_classz00_3444);
													}
												else
													{	/* Object/class.sch 147 */
														BgL_res1904z00_3449 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1904z00_3449;
							}
					}
				else
					{	/* Object/class.sch 147 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &tclass? */
	obj_t BGl_z62tclasszf3z91zzobject_classz00(obj_t BgL_envz00_2832,
		obj_t BgL_objz00_2833)
	{
		{	/* Object/class.sch 147 */
			return BBOOL(BGl_tclasszf3zf3zzobject_classz00(BgL_objz00_2833));
		}

	}



/* tclass-nil */
	BGL_EXPORTED_DEF BgL_typez00_bglt BGl_tclasszd2nilzd2zzobject_classz00(void)
	{
		{	/* Object/class.sch 148 */
			{	/* Object/class.sch 148 */
				obj_t BgL_classz00_1490;

				BgL_classz00_1490 = BGl_tclassz00zzobject_classz00;
				{	/* Object/class.sch 148 */
					obj_t BgL__ortest_1117z00_1491;

					BgL__ortest_1117z00_1491 = BGL_CLASS_NIL(BgL_classz00_1490);
					if (CBOOL(BgL__ortest_1117z00_1491))
						{	/* Object/class.sch 148 */
							return ((BgL_typez00_bglt) BgL__ortest_1117z00_1491);
						}
					else
						{	/* Object/class.sch 148 */
							return
								((BgL_typez00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1490));
						}
				}
			}
		}

	}



/* &tclass-nil */
	BgL_typez00_bglt BGl_z62tclasszd2nilzb0zzobject_classz00(obj_t
		BgL_envz00_2834)
	{
		{	/* Object/class.sch 148 */
			return BGl_tclasszd2nilzd2zzobject_classz00();
		}

	}



/* tclass-subclasses */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2subclasseszd2zzobject_classz00(BgL_typez00_bglt BgL_oz00_30)
	{
		{	/* Object/class.sch 149 */
			{
				BgL_tclassz00_bglt BgL_auxz00_3820;

				{
					obj_t BgL_auxz00_3821;

					{	/* Object/class.sch 149 */
						BgL_objectz00_bglt BgL_tmpz00_3822;

						BgL_tmpz00_3822 = ((BgL_objectz00_bglt) BgL_oz00_30);
						BgL_auxz00_3821 = BGL_OBJECT_WIDENING(BgL_tmpz00_3822);
					}
					BgL_auxz00_3820 = ((BgL_tclassz00_bglt) BgL_auxz00_3821);
				}
				return
					(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3820))->BgL_subclassesz00);
			}
		}

	}



/* &tclass-subclasses */
	obj_t BGl_z62tclasszd2subclasseszb0zzobject_classz00(obj_t BgL_envz00_2835,
		obj_t BgL_oz00_2836)
	{
		{	/* Object/class.sch 149 */
			return
				BGl_tclasszd2subclasseszd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2836));
		}

	}



/* tclass-subclasses-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2subclasseszd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_31, obj_t BgL_vz00_32)
	{
		{	/* Object/class.sch 150 */
			{
				BgL_tclassz00_bglt BgL_auxz00_3829;

				{
					obj_t BgL_auxz00_3830;

					{	/* Object/class.sch 150 */
						BgL_objectz00_bglt BgL_tmpz00_3831;

						BgL_tmpz00_3831 = ((BgL_objectz00_bglt) BgL_oz00_31);
						BgL_auxz00_3830 = BGL_OBJECT_WIDENING(BgL_tmpz00_3831);
					}
					BgL_auxz00_3829 = ((BgL_tclassz00_bglt) BgL_auxz00_3830);
				}
				return
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3829))->
						BgL_subclassesz00) = ((obj_t) BgL_vz00_32), BUNSPEC);
			}
		}

	}



/* &tclass-subclasses-set! */
	obj_t BGl_z62tclasszd2subclasseszd2setz12z70zzobject_classz00(obj_t
		BgL_envz00_2837, obj_t BgL_oz00_2838, obj_t BgL_vz00_2839)
	{
		{	/* Object/class.sch 150 */
			return
				BGl_tclasszd2subclasseszd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2838), BgL_vz00_2839);
		}

	}



/* tclass-wide-type */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2widezd2typez00zzobject_classz00(BgL_typez00_bglt BgL_oz00_33)
	{
		{	/* Object/class.sch 151 */
			{
				BgL_tclassz00_bglt BgL_auxz00_3838;

				{
					obj_t BgL_auxz00_3839;

					{	/* Object/class.sch 151 */
						BgL_objectz00_bglt BgL_tmpz00_3840;

						BgL_tmpz00_3840 = ((BgL_objectz00_bglt) BgL_oz00_33);
						BgL_auxz00_3839 = BGL_OBJECT_WIDENING(BgL_tmpz00_3840);
					}
					BgL_auxz00_3838 = ((BgL_tclassz00_bglt) BgL_auxz00_3839);
				}
				return
					(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3838))->BgL_widezd2typezd2);
			}
		}

	}



/* &tclass-wide-type */
	obj_t BGl_z62tclasszd2widezd2typez62zzobject_classz00(obj_t BgL_envz00_2840,
		obj_t BgL_oz00_2841)
	{
		{	/* Object/class.sch 151 */
			return
				BGl_tclasszd2widezd2typez00zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2841));
		}

	}



/* tclass-wide-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2widezd2typezd2setz12zc0zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_34, obj_t BgL_vz00_35)
	{
		{	/* Object/class.sch 152 */
			{
				BgL_tclassz00_bglt BgL_auxz00_3847;

				{
					obj_t BgL_auxz00_3848;

					{	/* Object/class.sch 152 */
						BgL_objectz00_bglt BgL_tmpz00_3849;

						BgL_tmpz00_3849 = ((BgL_objectz00_bglt) BgL_oz00_34);
						BgL_auxz00_3848 = BGL_OBJECT_WIDENING(BgL_tmpz00_3849);
					}
					BgL_auxz00_3847 = ((BgL_tclassz00_bglt) BgL_auxz00_3848);
				}
				return
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3847))->
						BgL_widezd2typezd2) = ((obj_t) BgL_vz00_35), BUNSPEC);
			}
		}

	}



/* &tclass-wide-type-set! */
	obj_t BGl_z62tclasszd2widezd2typezd2setz12za2zzobject_classz00(obj_t
		BgL_envz00_2842, obj_t BgL_oz00_2843, obj_t BgL_vz00_2844)
	{
		{	/* Object/class.sch 152 */
			return
				BGl_tclasszd2widezd2typezd2setz12zc0zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2843), BgL_vz00_2844);
		}

	}



/* tclass-abstract? */
	BGL_EXPORTED_DEF bool_t
		BGl_tclasszd2abstractzf3z21zzobject_classz00(BgL_typez00_bglt BgL_oz00_36)
	{
		{	/* Object/class.sch 153 */
			{
				BgL_tclassz00_bglt BgL_auxz00_3856;

				{
					obj_t BgL_auxz00_3857;

					{	/* Object/class.sch 153 */
						BgL_objectz00_bglt BgL_tmpz00_3858;

						BgL_tmpz00_3858 = ((BgL_objectz00_bglt) BgL_oz00_36);
						BgL_auxz00_3857 = BGL_OBJECT_WIDENING(BgL_tmpz00_3858);
					}
					BgL_auxz00_3856 = ((BgL_tclassz00_bglt) BgL_auxz00_3857);
				}
				return
					(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3856))->BgL_abstractzf3zf3);
			}
		}

	}



/* &tclass-abstract? */
	obj_t BGl_z62tclasszd2abstractzf3z43zzobject_classz00(obj_t BgL_envz00_2845,
		obj_t BgL_oz00_2846)
	{
		{	/* Object/class.sch 153 */
			return
				BBOOL(BGl_tclasszd2abstractzf3z21zzobject_classz00(
					((BgL_typez00_bglt) BgL_oz00_2846)));
		}

	}



/* tclass-virtual-slots-number */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2virtualzd2slotszd2numberzd2zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_39)
	{
		{	/* Object/class.sch 155 */
			{
				BgL_tclassz00_bglt BgL_auxz00_3866;

				{
					obj_t BgL_auxz00_3867;

					{	/* Object/class.sch 155 */
						BgL_objectz00_bglt BgL_tmpz00_3868;

						BgL_tmpz00_3868 = ((BgL_objectz00_bglt) BgL_oz00_39);
						BgL_auxz00_3867 = BGL_OBJECT_WIDENING(BgL_tmpz00_3868);
					}
					BgL_auxz00_3866 = ((BgL_tclassz00_bglt) BgL_auxz00_3867);
				}
				return
					(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3866))->
					BgL_virtualzd2slotszd2numberz00);
			}
		}

	}



/* &tclass-virtual-slots-number */
	obj_t BGl_z62tclasszd2virtualzd2slotszd2numberzb0zzobject_classz00(obj_t
		BgL_envz00_2847, obj_t BgL_oz00_2848)
	{
		{	/* Object/class.sch 155 */
			return
				BGl_tclasszd2virtualzd2slotszd2numberzd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2848));
		}

	}



/* tclass-virtual-slots-number-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2virtualzd2slotszd2numberzd2setz12z12zzobject_classz00
		(BgL_typez00_bglt BgL_oz00_40, obj_t BgL_vz00_41)
	{
		{	/* Object/class.sch 156 */
			{
				BgL_tclassz00_bglt BgL_auxz00_3875;

				{
					obj_t BgL_auxz00_3876;

					{	/* Object/class.sch 156 */
						BgL_objectz00_bglt BgL_tmpz00_3877;

						BgL_tmpz00_3877 = ((BgL_objectz00_bglt) BgL_oz00_40);
						BgL_auxz00_3876 = BGL_OBJECT_WIDENING(BgL_tmpz00_3877);
					}
					BgL_auxz00_3875 = ((BgL_tclassz00_bglt) BgL_auxz00_3876);
				}
				return
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3875))->
						BgL_virtualzd2slotszd2numberz00) = ((obj_t) BgL_vz00_41), BUNSPEC);
			}
		}

	}



/* &tclass-virtual-slots-number-set! */
	obj_t
		BGl_z62tclasszd2virtualzd2slotszd2numberzd2setz12z70zzobject_classz00(obj_t
		BgL_envz00_2849, obj_t BgL_oz00_2850, obj_t BgL_vz00_2851)
	{
		{	/* Object/class.sch 156 */
			return
				BGl_tclasszd2virtualzd2slotszd2numberzd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2850), BgL_vz00_2851);
		}

	}



/* tclass-constructor */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2constructorzd2zzobject_classz00(BgL_typez00_bglt BgL_oz00_42)
	{
		{	/* Object/class.sch 157 */
			{
				BgL_tclassz00_bglt BgL_auxz00_3884;

				{
					obj_t BgL_auxz00_3885;

					{	/* Object/class.sch 157 */
						BgL_objectz00_bglt BgL_tmpz00_3886;

						BgL_tmpz00_3886 = ((BgL_objectz00_bglt) BgL_oz00_42);
						BgL_auxz00_3885 = BGL_OBJECT_WIDENING(BgL_tmpz00_3886);
					}
					BgL_auxz00_3884 = ((BgL_tclassz00_bglt) BgL_auxz00_3885);
				}
				return
					(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3884))->BgL_constructorz00);
			}
		}

	}



/* &tclass-constructor */
	obj_t BGl_z62tclasszd2constructorzb0zzobject_classz00(obj_t BgL_envz00_2852,
		obj_t BgL_oz00_2853)
	{
		{	/* Object/class.sch 157 */
			return
				BGl_tclasszd2constructorzd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2853));
		}

	}



/* tclass-final? */
	BGL_EXPORTED_DEF bool_t
		BGl_tclasszd2finalzf3z21zzobject_classz00(BgL_typez00_bglt BgL_oz00_45)
	{
		{	/* Object/class.sch 159 */
			{
				BgL_tclassz00_bglt BgL_auxz00_3893;

				{
					obj_t BgL_auxz00_3894;

					{	/* Object/class.sch 159 */
						BgL_objectz00_bglt BgL_tmpz00_3895;

						BgL_tmpz00_3895 = ((BgL_objectz00_bglt) BgL_oz00_45);
						BgL_auxz00_3894 = BGL_OBJECT_WIDENING(BgL_tmpz00_3895);
					}
					BgL_auxz00_3893 = ((BgL_tclassz00_bglt) BgL_auxz00_3894);
				}
				return
					(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3893))->BgL_finalzf3zf3);
			}
		}

	}



/* &tclass-final? */
	obj_t BGl_z62tclasszd2finalzf3z43zzobject_classz00(obj_t BgL_envz00_2854,
		obj_t BgL_oz00_2855)
	{
		{	/* Object/class.sch 159 */
			return
				BBOOL(BGl_tclasszd2finalzf3z21zzobject_classz00(
					((BgL_typez00_bglt) BgL_oz00_2855)));
		}

	}



/* tclass-depth */
	BGL_EXPORTED_DEF long BGl_tclasszd2depthzd2zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_48)
	{
		{	/* Object/class.sch 161 */
			{
				BgL_tclassz00_bglt BgL_auxz00_3903;

				{
					obj_t BgL_auxz00_3904;

					{	/* Object/class.sch 161 */
						BgL_objectz00_bglt BgL_tmpz00_3905;

						BgL_tmpz00_3905 = ((BgL_objectz00_bglt) BgL_oz00_48);
						BgL_auxz00_3904 = BGL_OBJECT_WIDENING(BgL_tmpz00_3905);
					}
					BgL_auxz00_3903 = ((BgL_tclassz00_bglt) BgL_auxz00_3904);
				}
				return (((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3903))->BgL_depthz00);
			}
		}

	}



/* &tclass-depth */
	obj_t BGl_z62tclasszd2depthzb0zzobject_classz00(obj_t BgL_envz00_2856,
		obj_t BgL_oz00_2857)
	{
		{	/* Object/class.sch 161 */
			return
				BINT(BGl_tclasszd2depthzd2zzobject_classz00(
					((BgL_typez00_bglt) BgL_oz00_2857)));
		}

	}



/* tclass-depth-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2depthzd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_49, long BgL_vz00_50)
	{
		{	/* Object/class.sch 162 */
			{
				BgL_tclassz00_bglt BgL_auxz00_3913;

				{
					obj_t BgL_auxz00_3914;

					{	/* Object/class.sch 162 */
						BgL_objectz00_bglt BgL_tmpz00_3915;

						BgL_tmpz00_3915 = ((BgL_objectz00_bglt) BgL_oz00_49);
						BgL_auxz00_3914 = BGL_OBJECT_WIDENING(BgL_tmpz00_3915);
					}
					BgL_auxz00_3913 = ((BgL_tclassz00_bglt) BgL_auxz00_3914);
				}
				return
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3913))->BgL_depthz00) =
					((long) BgL_vz00_50), BUNSPEC);
		}}

	}



/* &tclass-depth-set! */
	obj_t BGl_z62tclasszd2depthzd2setz12z70zzobject_classz00(obj_t
		BgL_envz00_2858, obj_t BgL_oz00_2859, obj_t BgL_vz00_2860)
	{
		{	/* Object/class.sch 162 */
			return
				BGl_tclasszd2depthzd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2859), (long) CINT(BgL_vz00_2860));
		}

	}



/* tclass-widening */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2wideningzd2zzobject_classz00(BgL_typez00_bglt BgL_oz00_51)
	{
		{	/* Object/class.sch 163 */
			{
				BgL_tclassz00_bglt BgL_auxz00_3923;

				{
					obj_t BgL_auxz00_3924;

					{	/* Object/class.sch 163 */
						BgL_objectz00_bglt BgL_tmpz00_3925;

						BgL_tmpz00_3925 = ((BgL_objectz00_bglt) BgL_oz00_51);
						BgL_auxz00_3924 = BGL_OBJECT_WIDENING(BgL_tmpz00_3925);
					}
					BgL_auxz00_3923 = ((BgL_tclassz00_bglt) BgL_auxz00_3924);
				}
				return
					(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3923))->BgL_wideningz00);
			}
		}

	}



/* &tclass-widening */
	obj_t BGl_z62tclasszd2wideningzb0zzobject_classz00(obj_t BgL_envz00_2861,
		obj_t BgL_oz00_2862)
	{
		{	/* Object/class.sch 163 */
			return
				BGl_tclasszd2wideningzd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2862));
		}

	}



/* tclass-holder */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_tclasszd2holderzd2zzobject_classz00(BgL_typez00_bglt BgL_oz00_54)
	{
		{	/* Object/class.sch 165 */
			{
				BgL_tclassz00_bglt BgL_auxz00_3932;

				{
					obj_t BgL_auxz00_3933;

					{	/* Object/class.sch 165 */
						BgL_objectz00_bglt BgL_tmpz00_3934;

						BgL_tmpz00_3934 = ((BgL_objectz00_bglt) BgL_oz00_54);
						BgL_auxz00_3933 = BGL_OBJECT_WIDENING(BgL_tmpz00_3934);
					}
					BgL_auxz00_3932 = ((BgL_tclassz00_bglt) BgL_auxz00_3933);
				}
				return (((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3932))->BgL_holderz00);
			}
		}

	}



/* &tclass-holder */
	BgL_globalz00_bglt BGl_z62tclasszd2holderzb0zzobject_classz00(obj_t
		BgL_envz00_2863, obj_t BgL_oz00_2864)
	{
		{	/* Object/class.sch 165 */
			return
				BGl_tclasszd2holderzd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2864));
		}

	}



/* tclass-slots */
	BGL_EXPORTED_DEF obj_t BGl_tclasszd2slotszd2zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_57)
	{
		{	/* Object/class.sch 167 */
			{
				BgL_tclassz00_bglt BgL_auxz00_3941;

				{
					obj_t BgL_auxz00_3942;

					{	/* Object/class.sch 167 */
						BgL_objectz00_bglt BgL_tmpz00_3943;

						BgL_tmpz00_3943 = ((BgL_objectz00_bglt) BgL_oz00_57);
						BgL_auxz00_3942 = BGL_OBJECT_WIDENING(BgL_tmpz00_3943);
					}
					BgL_auxz00_3941 = ((BgL_tclassz00_bglt) BgL_auxz00_3942);
				}
				return (((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3941))->BgL_slotsz00);
			}
		}

	}



/* &tclass-slots */
	obj_t BGl_z62tclasszd2slotszb0zzobject_classz00(obj_t BgL_envz00_2865,
		obj_t BgL_oz00_2866)
	{
		{	/* Object/class.sch 167 */
			return
				BGl_tclasszd2slotszd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2866));
		}

	}



/* tclass-slots-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2slotszd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_58, obj_t BgL_vz00_59)
	{
		{	/* Object/class.sch 168 */
			{
				BgL_tclassz00_bglt BgL_auxz00_3950;

				{
					obj_t BgL_auxz00_3951;

					{	/* Object/class.sch 168 */
						BgL_objectz00_bglt BgL_tmpz00_3952;

						BgL_tmpz00_3952 = ((BgL_objectz00_bglt) BgL_oz00_58);
						BgL_auxz00_3951 = BGL_OBJECT_WIDENING(BgL_tmpz00_3952);
					}
					BgL_auxz00_3950 = ((BgL_tclassz00_bglt) BgL_auxz00_3951);
				}
				return
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3950))->BgL_slotsz00) =
					((obj_t) BgL_vz00_59), BUNSPEC);
			}
		}

	}



/* &tclass-slots-set! */
	obj_t BGl_z62tclasszd2slotszd2setz12z70zzobject_classz00(obj_t
		BgL_envz00_2867, obj_t BgL_oz00_2868, obj_t BgL_vz00_2869)
	{
		{	/* Object/class.sch 168 */
			return
				BGl_tclasszd2slotszd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2868), BgL_vz00_2869);
		}

	}



/* tclass-its-super */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2itszd2superz00zzobject_classz00(BgL_typez00_bglt BgL_oz00_60)
	{
		{	/* Object/class.sch 169 */
			{
				BgL_tclassz00_bglt BgL_auxz00_3959;

				{
					obj_t BgL_auxz00_3960;

					{	/* Object/class.sch 169 */
						BgL_objectz00_bglt BgL_tmpz00_3961;

						BgL_tmpz00_3961 = ((BgL_objectz00_bglt) BgL_oz00_60);
						BgL_auxz00_3960 = BGL_OBJECT_WIDENING(BgL_tmpz00_3961);
					}
					BgL_auxz00_3959 = ((BgL_tclassz00_bglt) BgL_auxz00_3960);
				}
				return
					(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3959))->BgL_itszd2superzd2);
			}
		}

	}



/* &tclass-its-super */
	obj_t BGl_z62tclasszd2itszd2superz62zzobject_classz00(obj_t BgL_envz00_2870,
		obj_t BgL_oz00_2871)
	{
		{	/* Object/class.sch 169 */
			return
				BGl_tclasszd2itszd2superz00zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2871));
		}

	}



/* tclass-its-super-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2itszd2superzd2setz12zc0zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_61, obj_t BgL_vz00_62)
	{
		{	/* Object/class.sch 170 */
			{
				BgL_tclassz00_bglt BgL_auxz00_3968;

				{
					obj_t BgL_auxz00_3969;

					{	/* Object/class.sch 170 */
						BgL_objectz00_bglt BgL_tmpz00_3970;

						BgL_tmpz00_3970 = ((BgL_objectz00_bglt) BgL_oz00_61);
						BgL_auxz00_3969 = BGL_OBJECT_WIDENING(BgL_tmpz00_3970);
					}
					BgL_auxz00_3968 = ((BgL_tclassz00_bglt) BgL_auxz00_3969);
				}
				return
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3968))->
						BgL_itszd2superzd2) = ((obj_t) BgL_vz00_62), BUNSPEC);
			}
		}

	}



/* &tclass-its-super-set! */
	obj_t BGl_z62tclasszd2itszd2superzd2setz12za2zzobject_classz00(obj_t
		BgL_envz00_2872, obj_t BgL_oz00_2873, obj_t BgL_vz00_2874)
	{
		{	/* Object/class.sch 170 */
			return
				BGl_tclasszd2itszd2superzd2setz12zc0zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2873), BgL_vz00_2874);
		}

	}



/* tclass-occurrence */
	BGL_EXPORTED_DEF int
		BGl_tclasszd2occurrencezd2zzobject_classz00(BgL_typez00_bglt BgL_oz00_63)
	{
		{	/* Object/class.sch 171 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_63)))->BgL_occurrencez00);
		}

	}



/* &tclass-occurrence */
	obj_t BGl_z62tclasszd2occurrencezb0zzobject_classz00(obj_t BgL_envz00_2875,
		obj_t BgL_oz00_2876)
	{
		{	/* Object/class.sch 171 */
			return
				BINT(BGl_tclasszd2occurrencezd2zzobject_classz00(
					((BgL_typez00_bglt) BgL_oz00_2876)));
		}

	}



/* tclass-occurrence-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2occurrencezd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_64, int BgL_vz00_65)
	{
		{	/* Object/class.sch 172 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_64)))->BgL_occurrencez00) =
				((int) BgL_vz00_65), BUNSPEC);
		}

	}



/* &tclass-occurrence-set! */
	obj_t BGl_z62tclasszd2occurrencezd2setz12z70zzobject_classz00(obj_t
		BgL_envz00_2877, obj_t BgL_oz00_2878, obj_t BgL_vz00_2879)
	{
		{	/* Object/class.sch 172 */
			return
				BGl_tclasszd2occurrencezd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2878), CINT(BgL_vz00_2879));
		}

	}



/* tclass-import-location */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2importzd2locationz00zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_66)
	{
		{	/* Object/class.sch 173 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_66)))->BgL_importzd2locationzd2);
		}

	}



/* &tclass-import-location */
	obj_t BGl_z62tclasszd2importzd2locationz62zzobject_classz00(obj_t
		BgL_envz00_2880, obj_t BgL_oz00_2881)
	{
		{	/* Object/class.sch 173 */
			return
				BGl_tclasszd2importzd2locationz00zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2881));
		}

	}



/* tclass-import-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2importzd2locationzd2setz12zc0zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_67, obj_t BgL_vz00_68)
	{
		{	/* Object/class.sch 174 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_67)))->BgL_importzd2locationzd2) =
				((obj_t) BgL_vz00_68), BUNSPEC);
		}

	}



/* &tclass-import-location-set! */
	obj_t BGl_z62tclasszd2importzd2locationzd2setz12za2zzobject_classz00(obj_t
		BgL_envz00_2882, obj_t BgL_oz00_2883, obj_t BgL_vz00_2884)
	{
		{	/* Object/class.sch 174 */
			return
				BGl_tclasszd2importzd2locationzd2setz12zc0zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2883), BgL_vz00_2884);
		}

	}



/* tclass-location */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2locationzd2zzobject_classz00(BgL_typez00_bglt BgL_oz00_69)
	{
		{	/* Object/class.sch 175 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_69)))->BgL_locationz00);
		}

	}



/* &tclass-location */
	obj_t BGl_z62tclasszd2locationzb0zzobject_classz00(obj_t BgL_envz00_2885,
		obj_t BgL_oz00_2886)
	{
		{	/* Object/class.sch 175 */
			return
				BGl_tclasszd2locationzd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2886));
		}

	}



/* tclass-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2locationzd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_70, obj_t BgL_vz00_71)
	{
		{	/* Object/class.sch 176 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_70)))->BgL_locationz00) =
				((obj_t) BgL_vz00_71), BUNSPEC);
		}

	}



/* &tclass-location-set! */
	obj_t BGl_z62tclasszd2locationzd2setz12z70zzobject_classz00(obj_t
		BgL_envz00_2887, obj_t BgL_oz00_2888, obj_t BgL_vz00_2889)
	{
		{	/* Object/class.sch 176 */
			return
				BGl_tclasszd2locationzd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2888), BgL_vz00_2889);
		}

	}



/* tclass-tvector */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2tvectorzd2zzobject_classz00(BgL_typez00_bglt BgL_oz00_72)
	{
		{	/* Object/class.sch 177 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_72)))->BgL_tvectorz00);
		}

	}



/* &tclass-tvector */
	obj_t BGl_z62tclasszd2tvectorzb0zzobject_classz00(obj_t BgL_envz00_2890,
		obj_t BgL_oz00_2891)
	{
		{	/* Object/class.sch 177 */
			return
				BGl_tclasszd2tvectorzd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2891));
		}

	}



/* tclass-tvector-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2tvectorzd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_73, obj_t BgL_vz00_74)
	{
		{	/* Object/class.sch 178 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_73)))->BgL_tvectorz00) =
				((obj_t) BgL_vz00_74), BUNSPEC);
		}

	}



/* &tclass-tvector-set! */
	obj_t BGl_z62tclasszd2tvectorzd2setz12z70zzobject_classz00(obj_t
		BgL_envz00_2892, obj_t BgL_oz00_2893, obj_t BgL_vz00_2894)
	{
		{	/* Object/class.sch 178 */
			return
				BGl_tclasszd2tvectorzd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2893), BgL_vz00_2894);
		}

	}



/* tclass-pointed-to-by */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2pointedzd2tozd2byzd2zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_75)
	{
		{	/* Object/class.sch 179 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_75)))->BgL_pointedzd2tozd2byz00);
		}

	}



/* &tclass-pointed-to-by */
	obj_t BGl_z62tclasszd2pointedzd2tozd2byzb0zzobject_classz00(obj_t
		BgL_envz00_2895, obj_t BgL_oz00_2896)
	{
		{	/* Object/class.sch 179 */
			return
				BGl_tclasszd2pointedzd2tozd2byzd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2896));
		}

	}



/* tclass-pointed-to-by-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2pointedzd2tozd2byzd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_76, obj_t BgL_vz00_77)
	{
		{	/* Object/class.sch 180 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_76)))->BgL_pointedzd2tozd2byz00) =
				((obj_t) BgL_vz00_77), BUNSPEC);
		}

	}



/* &tclass-pointed-to-by-set! */
	obj_t BGl_z62tclasszd2pointedzd2tozd2byzd2setz12z70zzobject_classz00(obj_t
		BgL_envz00_2897, obj_t BgL_oz00_2898, obj_t BgL_vz00_2899)
	{
		{	/* Object/class.sch 180 */
			return
				BGl_tclasszd2pointedzd2tozd2byzd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2898), BgL_vz00_2899);
		}

	}



/* tclass-alias */
	BGL_EXPORTED_DEF obj_t BGl_tclasszd2aliaszd2zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_78)
	{
		{	/* Object/class.sch 181 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_78)))->BgL_aliasz00);
		}

	}



/* &tclass-alias */
	obj_t BGl_z62tclasszd2aliaszb0zzobject_classz00(obj_t BgL_envz00_2900,
		obj_t BgL_oz00_2901)
	{
		{	/* Object/class.sch 181 */
			return
				BGl_tclasszd2aliaszd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2901));
		}

	}



/* tclass-alias-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2aliaszd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_79, obj_t BgL_vz00_80)
	{
		{	/* Object/class.sch 182 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_79)))->BgL_aliasz00) =
				((obj_t) BgL_vz00_80), BUNSPEC);
		}

	}



/* &tclass-alias-set! */
	obj_t BGl_z62tclasszd2aliaszd2setz12z70zzobject_classz00(obj_t
		BgL_envz00_2902, obj_t BgL_oz00_2903, obj_t BgL_vz00_2904)
	{
		{	/* Object/class.sch 182 */
			return
				BGl_tclasszd2aliaszd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2903), BgL_vz00_2904);
		}

	}



/* tclass-$ */
	BGL_EXPORTED_DEF obj_t BGl_tclasszd2z42z90zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_81)
	{
		{	/* Object/class.sch 183 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_81)))->BgL_z42z42);
		}

	}



/* &tclass-$ */
	obj_t BGl_z62tclasszd2z42zf2zzobject_classz00(obj_t BgL_envz00_2905,
		obj_t BgL_oz00_2906)
	{
		{	/* Object/class.sch 183 */
			return
				BGl_tclasszd2z42z90zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2906));
		}

	}



/* tclass-$-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2z42zd2setz12z50zzobject_classz00(BgL_typez00_bglt BgL_oz00_82,
		obj_t BgL_vz00_83)
	{
		{	/* Object/class.sch 184 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_82)))->BgL_z42z42) =
				((obj_t) BgL_vz00_83), BUNSPEC);
		}

	}



/* &tclass-$-set! */
	obj_t BGl_z62tclasszd2z42zd2setz12z32zzobject_classz00(obj_t BgL_envz00_2907,
		obj_t BgL_oz00_2908, obj_t BgL_vz00_2909)
	{
		{	/* Object/class.sch 184 */
			return
				BGl_tclasszd2z42zd2setz12z50zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2908), BgL_vz00_2909);
		}

	}



/* tclass-magic? */
	BGL_EXPORTED_DEF bool_t
		BGl_tclasszd2magiczf3z21zzobject_classz00(BgL_typez00_bglt BgL_oz00_84)
	{
		{	/* Object/class.sch 185 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_84)))->BgL_magiczf3zf3);
		}

	}



/* &tclass-magic? */
	obj_t BGl_z62tclasszd2magiczf3z43zzobject_classz00(obj_t BgL_envz00_2910,
		obj_t BgL_oz00_2911)
	{
		{	/* Object/class.sch 185 */
			return
				BBOOL(BGl_tclasszd2magiczf3z21zzobject_classz00(
					((BgL_typez00_bglt) BgL_oz00_2911)));
		}

	}



/* tclass-magic?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2magiczf3zd2setz12ze1zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_85, bool_t BgL_vz00_86)
	{
		{	/* Object/class.sch 186 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_85)))->BgL_magiczf3zf3) =
				((bool_t) BgL_vz00_86), BUNSPEC);
		}

	}



/* &tclass-magic?-set! */
	obj_t BGl_z62tclasszd2magiczf3zd2setz12z83zzobject_classz00(obj_t
		BgL_envz00_2912, obj_t BgL_oz00_2913, obj_t BgL_vz00_2914)
	{
		{	/* Object/class.sch 186 */
			return
				BGl_tclasszd2magiczf3zd2setz12ze1zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2913), CBOOL(BgL_vz00_2914));
		}

	}



/* tclass-init? */
	BGL_EXPORTED_DEF bool_t
		BGl_tclasszd2initzf3z21zzobject_classz00(BgL_typez00_bglt BgL_oz00_87)
	{
		{	/* Object/class.sch 187 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_87)))->BgL_initzf3zf3);
		}

	}



/* &tclass-init? */
	obj_t BGl_z62tclasszd2initzf3z43zzobject_classz00(obj_t BgL_envz00_2915,
		obj_t BgL_oz00_2916)
	{
		{	/* Object/class.sch 187 */
			return
				BBOOL(BGl_tclasszd2initzf3z21zzobject_classz00(
					((BgL_typez00_bglt) BgL_oz00_2916)));
		}

	}



/* tclass-init?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2initzf3zd2setz12ze1zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_88, bool_t BgL_vz00_89)
	{
		{	/* Object/class.sch 188 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_88)))->BgL_initzf3zf3) =
				((bool_t) BgL_vz00_89), BUNSPEC);
		}

	}



/* &tclass-init?-set! */
	obj_t BGl_z62tclasszd2initzf3zd2setz12z83zzobject_classz00(obj_t
		BgL_envz00_2917, obj_t BgL_oz00_2918, obj_t BgL_vz00_2919)
	{
		{	/* Object/class.sch 188 */
			return
				BGl_tclasszd2initzf3zd2setz12ze1zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2918), CBOOL(BgL_vz00_2919));
		}

	}



/* tclass-parents */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2parentszd2zzobject_classz00(BgL_typez00_bglt BgL_oz00_90)
	{
		{	/* Object/class.sch 189 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_90)))->BgL_parentsz00);
		}

	}



/* &tclass-parents */
	obj_t BGl_z62tclasszd2parentszb0zzobject_classz00(obj_t BgL_envz00_2920,
		obj_t BgL_oz00_2921)
	{
		{	/* Object/class.sch 189 */
			return
				BGl_tclasszd2parentszd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2921));
		}

	}



/* tclass-parents-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2parentszd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_91, obj_t BgL_vz00_92)
	{
		{	/* Object/class.sch 190 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_91)))->BgL_parentsz00) =
				((obj_t) BgL_vz00_92), BUNSPEC);
		}

	}



/* &tclass-parents-set! */
	obj_t BGl_z62tclasszd2parentszd2setz12z70zzobject_classz00(obj_t
		BgL_envz00_2922, obj_t BgL_oz00_2923, obj_t BgL_vz00_2924)
	{
		{	/* Object/class.sch 190 */
			return
				BGl_tclasszd2parentszd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2923), BgL_vz00_2924);
		}

	}



/* tclass-coerce-to */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2coercezd2toz00zzobject_classz00(BgL_typez00_bglt BgL_oz00_93)
	{
		{	/* Object/class.sch 191 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_93)))->BgL_coercezd2tozd2);
		}

	}



/* &tclass-coerce-to */
	obj_t BGl_z62tclasszd2coercezd2toz62zzobject_classz00(obj_t BgL_envz00_2925,
		obj_t BgL_oz00_2926)
	{
		{	/* Object/class.sch 191 */
			return
				BGl_tclasszd2coercezd2toz00zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2926));
		}

	}



/* tclass-coerce-to-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2coercezd2tozd2setz12zc0zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_94, obj_t BgL_vz00_95)
	{
		{	/* Object/class.sch 192 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_94)))->BgL_coercezd2tozd2) =
				((obj_t) BgL_vz00_95), BUNSPEC);
		}

	}



/* &tclass-coerce-to-set! */
	obj_t BGl_z62tclasszd2coercezd2tozd2setz12za2zzobject_classz00(obj_t
		BgL_envz00_2927, obj_t BgL_oz00_2928, obj_t BgL_vz00_2929)
	{
		{	/* Object/class.sch 192 */
			return
				BGl_tclasszd2coercezd2tozd2setz12zc0zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2928), BgL_vz00_2929);
		}

	}



/* tclass-class */
	BGL_EXPORTED_DEF obj_t BGl_tclasszd2classzd2zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_96)
	{
		{	/* Object/class.sch 193 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_96)))->BgL_classz00);
		}

	}



/* &tclass-class */
	obj_t BGl_z62tclasszd2classzb0zzobject_classz00(obj_t BgL_envz00_2930,
		obj_t BgL_oz00_2931)
	{
		{	/* Object/class.sch 193 */
			return
				BGl_tclasszd2classzd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2931));
		}

	}



/* tclass-class-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2classzd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_97, obj_t BgL_vz00_98)
	{
		{	/* Object/class.sch 194 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_97)))->BgL_classz00) =
				((obj_t) BgL_vz00_98), BUNSPEC);
		}

	}



/* &tclass-class-set! */
	obj_t BGl_z62tclasszd2classzd2setz12z70zzobject_classz00(obj_t
		BgL_envz00_2932, obj_t BgL_oz00_2933, obj_t BgL_vz00_2934)
	{
		{	/* Object/class.sch 194 */
			return
				BGl_tclasszd2classzd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2933), BgL_vz00_2934);
		}

	}



/* tclass-size */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2siza7ez75zzobject_classz00(BgL_typez00_bglt BgL_oz00_99)
	{
		{	/* Object/class.sch 195 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_99)))->BgL_siza7eza7);
		}

	}



/* &tclass-size */
	obj_t BGl_z62tclasszd2siza7ez17zzobject_classz00(obj_t BgL_envz00_2935,
		obj_t BgL_oz00_2936)
	{
		{	/* Object/class.sch 195 */
			return
				BGl_tclasszd2siza7ez75zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2936));
		}

	}



/* tclass-size-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2siza7ezd2setz12zb5zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_100, obj_t BgL_vz00_101)
	{
		{	/* Object/class.sch 196 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_100)))->BgL_siza7eza7) =
				((obj_t) BgL_vz00_101), BUNSPEC);
		}

	}



/* &tclass-size-set! */
	obj_t BGl_z62tclasszd2siza7ezd2setz12zd7zzobject_classz00(obj_t
		BgL_envz00_2937, obj_t BgL_oz00_2938, obj_t BgL_vz00_2939)
	{
		{	/* Object/class.sch 196 */
			return
				BGl_tclasszd2siza7ezd2setz12zb5zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2938), BgL_vz00_2939);
		}

	}



/* tclass-name */
	BGL_EXPORTED_DEF obj_t BGl_tclasszd2namezd2zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_102)
	{
		{	/* Object/class.sch 197 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_102)))->BgL_namez00);
		}

	}



/* &tclass-name */
	obj_t BGl_z62tclasszd2namezb0zzobject_classz00(obj_t BgL_envz00_2940,
		obj_t BgL_oz00_2941)
	{
		{	/* Object/class.sch 197 */
			return
				BGl_tclasszd2namezd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2941));
		}

	}



/* tclass-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tclasszd2namezd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_103, obj_t BgL_vz00_104)
	{
		{	/* Object/class.sch 198 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_103)))->BgL_namez00) =
				((obj_t) BgL_vz00_104), BUNSPEC);
		}

	}



/* &tclass-name-set! */
	obj_t BGl_z62tclasszd2namezd2setz12z70zzobject_classz00(obj_t BgL_envz00_2942,
		obj_t BgL_oz00_2943, obj_t BgL_vz00_2944)
	{
		{	/* Object/class.sch 198 */
			return
				BGl_tclasszd2namezd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2943), BgL_vz00_2944);
		}

	}



/* tclass-id */
	BGL_EXPORTED_DEF obj_t BGl_tclasszd2idzd2zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_105)
	{
		{	/* Object/class.sch 199 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_105)))->BgL_idz00);
		}

	}



/* &tclass-id */
	obj_t BGl_z62tclasszd2idzb0zzobject_classz00(obj_t BgL_envz00_2945,
		obj_t BgL_oz00_2946)
	{
		{	/* Object/class.sch 199 */
			return
				BGl_tclasszd2idzd2zzobject_classz00(((BgL_typez00_bglt) BgL_oz00_2946));
		}

	}



/* make-jclass */
	BGL_EXPORTED_DEF BgL_typez00_bglt BGl_makezd2jclasszd2zzobject_classz00(obj_t
		BgL_id1159z00_108, obj_t BgL_name1160z00_109, obj_t BgL_siza7e1161za7_110,
		obj_t BgL_class1162z00_111, obj_t BgL_coercezd2to1163zd2_112,
		obj_t BgL_parents1164z00_113, bool_t BgL_initzf31165zf3_114,
		bool_t BgL_magiczf31166zf3_115, obj_t BgL_z421167z42_116,
		obj_t BgL_alias1168z00_117, obj_t BgL_pointedzd2tozd2by1169z00_118,
		obj_t BgL_tvector1170z00_119, obj_t BgL_location1171z00_120,
		obj_t BgL_importzd2location1172zd2_121, int BgL_occurrence1173z00_122,
		obj_t BgL_itszd2super1174zd2_123, obj_t BgL_slots1175z00_124,
		obj_t BgL_package1176z00_125)
	{
		{	/* Object/class.sch 203 */
			{	/* Object/class.sch 203 */
				BgL_typez00_bglt BgL_new1151z00_3459;

				{	/* Object/class.sch 203 */
					BgL_typez00_bglt BgL_tmp1149z00_3460;
					BgL_jclassz00_bglt BgL_wide1150z00_3461;

					{
						BgL_typez00_bglt BgL_auxz00_4099;

						{	/* Object/class.sch 203 */
							BgL_typez00_bglt BgL_new1148z00_3462;

							BgL_new1148z00_3462 =
								((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_typez00_bgl))));
							{	/* Object/class.sch 203 */
								long BgL_arg1248z00_3463;

								BgL_arg1248z00_3463 = BGL_CLASS_NUM(BGl_typez00zztype_typez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1148z00_3462),
									BgL_arg1248z00_3463);
							}
							{	/* Object/class.sch 203 */
								BgL_objectz00_bglt BgL_tmpz00_4104;

								BgL_tmpz00_4104 = ((BgL_objectz00_bglt) BgL_new1148z00_3462);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4104, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1148z00_3462);
							BgL_auxz00_4099 = BgL_new1148z00_3462;
						}
						BgL_tmp1149z00_3460 = ((BgL_typez00_bglt) BgL_auxz00_4099);
					}
					BgL_wide1150z00_3461 =
						((BgL_jclassz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_jclassz00_bgl))));
					{	/* Object/class.sch 203 */
						obj_t BgL_auxz00_4112;
						BgL_objectz00_bglt BgL_tmpz00_4110;

						BgL_auxz00_4112 = ((obj_t) BgL_wide1150z00_3461);
						BgL_tmpz00_4110 = ((BgL_objectz00_bglt) BgL_tmp1149z00_3460);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4110, BgL_auxz00_4112);
					}
					((BgL_objectz00_bglt) BgL_tmp1149z00_3460);
					{	/* Object/class.sch 203 */
						long BgL_arg1244z00_3464;

						BgL_arg1244z00_3464 = BGL_CLASS_NUM(BGl_jclassz00zzobject_classz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1149z00_3460), BgL_arg1244z00_3464);
					}
					BgL_new1151z00_3459 = ((BgL_typez00_bglt) BgL_tmp1149z00_3460);
				}
				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_new1151z00_3459)))->BgL_idz00) =
					((obj_t) BgL_id1159z00_108), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1151z00_3459)))->BgL_namez00) =
					((obj_t) BgL_name1160z00_109), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1151z00_3459)))->BgL_siza7eza7) =
					((obj_t) BgL_siza7e1161za7_110), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1151z00_3459)))->BgL_classz00) =
					((obj_t) BgL_class1162z00_111), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1151z00_3459)))->BgL_coercezd2tozd2) =
					((obj_t) BgL_coercezd2to1163zd2_112), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1151z00_3459)))->BgL_parentsz00) =
					((obj_t) BgL_parents1164z00_113), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1151z00_3459)))->BgL_initzf3zf3) =
					((bool_t) BgL_initzf31165zf3_114), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1151z00_3459)))->BgL_magiczf3zf3) =
					((bool_t) BgL_magiczf31166zf3_115), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1151z00_3459)))->BgL_nullz00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1151z00_3459)))->BgL_z42z42) =
					((obj_t) BgL_z421167z42_116), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1151z00_3459)))->BgL_aliasz00) =
					((obj_t) BgL_alias1168z00_117), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1151z00_3459)))->BgL_pointedzd2tozd2byz00) =
					((obj_t) BgL_pointedzd2tozd2by1169z00_118), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1151z00_3459)))->BgL_tvectorz00) =
					((obj_t) BgL_tvector1170z00_119), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1151z00_3459)))->BgL_locationz00) =
					((obj_t) BgL_location1171z00_120), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1151z00_3459)))->BgL_importzd2locationzd2) =
					((obj_t) BgL_importzd2location1172zd2_121), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1151z00_3459)))->BgL_occurrencez00) =
					((int) BgL_occurrence1173z00_122), BUNSPEC);
				{
					BgL_jclassz00_bglt BgL_auxz00_4152;

					{
						obj_t BgL_auxz00_4153;

						{	/* Object/class.sch 203 */
							BgL_objectz00_bglt BgL_tmpz00_4154;

							BgL_tmpz00_4154 = ((BgL_objectz00_bglt) BgL_new1151z00_3459);
							BgL_auxz00_4153 = BGL_OBJECT_WIDENING(BgL_tmpz00_4154);
						}
						BgL_auxz00_4152 = ((BgL_jclassz00_bglt) BgL_auxz00_4153);
					}
					((((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_4152))->
							BgL_itszd2superzd2) =
						((obj_t) BgL_itszd2super1174zd2_123), BUNSPEC);
				}
				{
					BgL_jclassz00_bglt BgL_auxz00_4159;

					{
						obj_t BgL_auxz00_4160;

						{	/* Object/class.sch 203 */
							BgL_objectz00_bglt BgL_tmpz00_4161;

							BgL_tmpz00_4161 = ((BgL_objectz00_bglt) BgL_new1151z00_3459);
							BgL_auxz00_4160 = BGL_OBJECT_WIDENING(BgL_tmpz00_4161);
						}
						BgL_auxz00_4159 = ((BgL_jclassz00_bglt) BgL_auxz00_4160);
					}
					((((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_4159))->BgL_slotsz00) =
						((obj_t) BgL_slots1175z00_124), BUNSPEC);
				}
				{
					BgL_jclassz00_bglt BgL_auxz00_4166;

					{
						obj_t BgL_auxz00_4167;

						{	/* Object/class.sch 203 */
							BgL_objectz00_bglt BgL_tmpz00_4168;

							BgL_tmpz00_4168 = ((BgL_objectz00_bglt) BgL_new1151z00_3459);
							BgL_auxz00_4167 = BGL_OBJECT_WIDENING(BgL_tmpz00_4168);
						}
						BgL_auxz00_4166 = ((BgL_jclassz00_bglt) BgL_auxz00_4167);
					}
					((((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_4166))->BgL_packagez00) =
						((obj_t) BgL_package1176z00_125), BUNSPEC);
				}
				return BgL_new1151z00_3459;
			}
		}

	}



/* &make-jclass */
	BgL_typez00_bglt BGl_z62makezd2jclasszb0zzobject_classz00(obj_t
		BgL_envz00_2947, obj_t BgL_id1159z00_2948, obj_t BgL_name1160z00_2949,
		obj_t BgL_siza7e1161za7_2950, obj_t BgL_class1162z00_2951,
		obj_t BgL_coercezd2to1163zd2_2952, obj_t BgL_parents1164z00_2953,
		obj_t BgL_initzf31165zf3_2954, obj_t BgL_magiczf31166zf3_2955,
		obj_t BgL_z421167z42_2956, obj_t BgL_alias1168z00_2957,
		obj_t BgL_pointedzd2tozd2by1169z00_2958, obj_t BgL_tvector1170z00_2959,
		obj_t BgL_location1171z00_2960, obj_t BgL_importzd2location1172zd2_2961,
		obj_t BgL_occurrence1173z00_2962, obj_t BgL_itszd2super1174zd2_2963,
		obj_t BgL_slots1175z00_2964, obj_t BgL_package1176z00_2965)
	{
		{	/* Object/class.sch 203 */
			return
				BGl_makezd2jclasszd2zzobject_classz00(BgL_id1159z00_2948,
				BgL_name1160z00_2949, BgL_siza7e1161za7_2950, BgL_class1162z00_2951,
				BgL_coercezd2to1163zd2_2952, BgL_parents1164z00_2953,
				CBOOL(BgL_initzf31165zf3_2954), CBOOL(BgL_magiczf31166zf3_2955),
				BgL_z421167z42_2956, BgL_alias1168z00_2957,
				BgL_pointedzd2tozd2by1169z00_2958, BgL_tvector1170z00_2959,
				BgL_location1171z00_2960, BgL_importzd2location1172zd2_2961,
				CINT(BgL_occurrence1173z00_2962), BgL_itszd2super1174zd2_2963,
				BgL_slots1175z00_2964, BgL_package1176z00_2965);
		}

	}



/* jclass? */
	BGL_EXPORTED_DEF bool_t BGl_jclasszf3zf3zzobject_classz00(obj_t
		BgL_objz00_126)
	{
		{	/* Object/class.sch 204 */
			{	/* Object/class.sch 204 */
				obj_t BgL_classz00_3465;

				BgL_classz00_3465 = BGl_jclassz00zzobject_classz00;
				if (BGL_OBJECTP(BgL_objz00_126))
					{	/* Object/class.sch 204 */
						BgL_objectz00_bglt BgL_arg1807z00_3466;

						BgL_arg1807z00_3466 = (BgL_objectz00_bglt) (BgL_objz00_126);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Object/class.sch 204 */
								long BgL_idxz00_3467;

								BgL_idxz00_3467 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3466);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3467 + 2L)) == BgL_classz00_3465);
							}
						else
							{	/* Object/class.sch 204 */
								bool_t BgL_res1905z00_3470;

								{	/* Object/class.sch 204 */
									obj_t BgL_oclassz00_3471;

									{	/* Object/class.sch 204 */
										obj_t BgL_arg1815z00_3472;
										long BgL_arg1816z00_3473;

										BgL_arg1815z00_3472 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Object/class.sch 204 */
											long BgL_arg1817z00_3474;

											BgL_arg1817z00_3474 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3466);
											BgL_arg1816z00_3473 = (BgL_arg1817z00_3474 - OBJECT_TYPE);
										}
										BgL_oclassz00_3471 =
											VECTOR_REF(BgL_arg1815z00_3472, BgL_arg1816z00_3473);
									}
									{	/* Object/class.sch 204 */
										bool_t BgL__ortest_1115z00_3475;

										BgL__ortest_1115z00_3475 =
											(BgL_classz00_3465 == BgL_oclassz00_3471);
										if (BgL__ortest_1115z00_3475)
											{	/* Object/class.sch 204 */
												BgL_res1905z00_3470 = BgL__ortest_1115z00_3475;
											}
										else
											{	/* Object/class.sch 204 */
												long BgL_odepthz00_3476;

												{	/* Object/class.sch 204 */
													obj_t BgL_arg1804z00_3477;

													BgL_arg1804z00_3477 = (BgL_oclassz00_3471);
													BgL_odepthz00_3476 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3477);
												}
												if ((2L < BgL_odepthz00_3476))
													{	/* Object/class.sch 204 */
														obj_t BgL_arg1802z00_3478;

														{	/* Object/class.sch 204 */
															obj_t BgL_arg1803z00_3479;

															BgL_arg1803z00_3479 = (BgL_oclassz00_3471);
															BgL_arg1802z00_3478 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3479,
																2L);
														}
														BgL_res1905z00_3470 =
															(BgL_arg1802z00_3478 == BgL_classz00_3465);
													}
												else
													{	/* Object/class.sch 204 */
														BgL_res1905z00_3470 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1905z00_3470;
							}
					}
				else
					{	/* Object/class.sch 204 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &jclass? */
	obj_t BGl_z62jclasszf3z91zzobject_classz00(obj_t BgL_envz00_2966,
		obj_t BgL_objz00_2967)
	{
		{	/* Object/class.sch 204 */
			return BBOOL(BGl_jclasszf3zf3zzobject_classz00(BgL_objz00_2967));
		}

	}



/* jclass-nil */
	BGL_EXPORTED_DEF BgL_typez00_bglt BGl_jclasszd2nilzd2zzobject_classz00(void)
	{
		{	/* Object/class.sch 205 */
			{	/* Object/class.sch 205 */
				obj_t BgL_classz00_1560;

				BgL_classz00_1560 = BGl_jclassz00zzobject_classz00;
				{	/* Object/class.sch 205 */
					obj_t BgL__ortest_1117z00_1561;

					BgL__ortest_1117z00_1561 = BGL_CLASS_NIL(BgL_classz00_1560);
					if (CBOOL(BgL__ortest_1117z00_1561))
						{	/* Object/class.sch 205 */
							return ((BgL_typez00_bglt) BgL__ortest_1117z00_1561);
						}
					else
						{	/* Object/class.sch 205 */
							return
								((BgL_typez00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1560));
						}
				}
			}
		}

	}



/* &jclass-nil */
	BgL_typez00_bglt BGl_z62jclasszd2nilzb0zzobject_classz00(obj_t
		BgL_envz00_2968)
	{
		{	/* Object/class.sch 205 */
			return BGl_jclasszd2nilzd2zzobject_classz00();
		}

	}



/* jclass-package */
	BGL_EXPORTED_DEF obj_t
		BGl_jclasszd2packagezd2zzobject_classz00(BgL_typez00_bglt BgL_oz00_127)
	{
		{	/* Object/class.sch 206 */
			{
				BgL_jclassz00_bglt BgL_auxz00_4208;

				{
					obj_t BgL_auxz00_4209;

					{	/* Object/class.sch 206 */
						BgL_objectz00_bglt BgL_tmpz00_4210;

						BgL_tmpz00_4210 = ((BgL_objectz00_bglt) BgL_oz00_127);
						BgL_auxz00_4209 = BGL_OBJECT_WIDENING(BgL_tmpz00_4210);
					}
					BgL_auxz00_4208 = ((BgL_jclassz00_bglt) BgL_auxz00_4209);
				}
				return
					(((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_4208))->BgL_packagez00);
			}
		}

	}



/* &jclass-package */
	obj_t BGl_z62jclasszd2packagezb0zzobject_classz00(obj_t BgL_envz00_2969,
		obj_t BgL_oz00_2970)
	{
		{	/* Object/class.sch 206 */
			return
				BGl_jclasszd2packagezd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2970));
		}

	}



/* jclass-slots */
	BGL_EXPORTED_DEF obj_t BGl_jclasszd2slotszd2zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_130)
	{
		{	/* Object/class.sch 208 */
			{
				BgL_jclassz00_bglt BgL_auxz00_4217;

				{
					obj_t BgL_auxz00_4218;

					{	/* Object/class.sch 208 */
						BgL_objectz00_bglt BgL_tmpz00_4219;

						BgL_tmpz00_4219 = ((BgL_objectz00_bglt) BgL_oz00_130);
						BgL_auxz00_4218 = BGL_OBJECT_WIDENING(BgL_tmpz00_4219);
					}
					BgL_auxz00_4217 = ((BgL_jclassz00_bglt) BgL_auxz00_4218);
				}
				return (((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_4217))->BgL_slotsz00);
			}
		}

	}



/* &jclass-slots */
	obj_t BGl_z62jclasszd2slotszb0zzobject_classz00(obj_t BgL_envz00_2971,
		obj_t BgL_oz00_2972)
	{
		{	/* Object/class.sch 208 */
			return
				BGl_jclasszd2slotszd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2972));
		}

	}



/* jclass-slots-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jclasszd2slotszd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_131, obj_t BgL_vz00_132)
	{
		{	/* Object/class.sch 209 */
			{
				BgL_jclassz00_bglt BgL_auxz00_4226;

				{
					obj_t BgL_auxz00_4227;

					{	/* Object/class.sch 209 */
						BgL_objectz00_bglt BgL_tmpz00_4228;

						BgL_tmpz00_4228 = ((BgL_objectz00_bglt) BgL_oz00_131);
						BgL_auxz00_4227 = BGL_OBJECT_WIDENING(BgL_tmpz00_4228);
					}
					BgL_auxz00_4226 = ((BgL_jclassz00_bglt) BgL_auxz00_4227);
				}
				return
					((((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_4226))->BgL_slotsz00) =
					((obj_t) BgL_vz00_132), BUNSPEC);
			}
		}

	}



/* &jclass-slots-set! */
	obj_t BGl_z62jclasszd2slotszd2setz12z70zzobject_classz00(obj_t
		BgL_envz00_2973, obj_t BgL_oz00_2974, obj_t BgL_vz00_2975)
	{
		{	/* Object/class.sch 209 */
			return
				BGl_jclasszd2slotszd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2974), BgL_vz00_2975);
		}

	}



/* jclass-its-super */
	BGL_EXPORTED_DEF obj_t
		BGl_jclasszd2itszd2superz00zzobject_classz00(BgL_typez00_bglt BgL_oz00_133)
	{
		{	/* Object/class.sch 210 */
			{
				BgL_jclassz00_bglt BgL_auxz00_4235;

				{
					obj_t BgL_auxz00_4236;

					{	/* Object/class.sch 210 */
						BgL_objectz00_bglt BgL_tmpz00_4237;

						BgL_tmpz00_4237 = ((BgL_objectz00_bglt) BgL_oz00_133);
						BgL_auxz00_4236 = BGL_OBJECT_WIDENING(BgL_tmpz00_4237);
					}
					BgL_auxz00_4235 = ((BgL_jclassz00_bglt) BgL_auxz00_4236);
				}
				return
					(((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_4235))->BgL_itszd2superzd2);
			}
		}

	}



/* &jclass-its-super */
	obj_t BGl_z62jclasszd2itszd2superz62zzobject_classz00(obj_t BgL_envz00_2976,
		obj_t BgL_oz00_2977)
	{
		{	/* Object/class.sch 210 */
			return
				BGl_jclasszd2itszd2superz00zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2977));
		}

	}



/* jclass-its-super-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jclasszd2itszd2superzd2setz12zc0zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_134, obj_t BgL_vz00_135)
	{
		{	/* Object/class.sch 211 */
			{
				BgL_jclassz00_bglt BgL_auxz00_4244;

				{
					obj_t BgL_auxz00_4245;

					{	/* Object/class.sch 211 */
						BgL_objectz00_bglt BgL_tmpz00_4246;

						BgL_tmpz00_4246 = ((BgL_objectz00_bglt) BgL_oz00_134);
						BgL_auxz00_4245 = BGL_OBJECT_WIDENING(BgL_tmpz00_4246);
					}
					BgL_auxz00_4244 = ((BgL_jclassz00_bglt) BgL_auxz00_4245);
				}
				return
					((((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_4244))->
						BgL_itszd2superzd2) = ((obj_t) BgL_vz00_135), BUNSPEC);
			}
		}

	}



/* &jclass-its-super-set! */
	obj_t BGl_z62jclasszd2itszd2superzd2setz12za2zzobject_classz00(obj_t
		BgL_envz00_2978, obj_t BgL_oz00_2979, obj_t BgL_vz00_2980)
	{
		{	/* Object/class.sch 211 */
			return
				BGl_jclasszd2itszd2superzd2setz12zc0zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2979), BgL_vz00_2980);
		}

	}



/* jclass-occurrence */
	BGL_EXPORTED_DEF int
		BGl_jclasszd2occurrencezd2zzobject_classz00(BgL_typez00_bglt BgL_oz00_136)
	{
		{	/* Object/class.sch 212 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_136)))->BgL_occurrencez00);
		}

	}



/* &jclass-occurrence */
	obj_t BGl_z62jclasszd2occurrencezb0zzobject_classz00(obj_t BgL_envz00_2981,
		obj_t BgL_oz00_2982)
	{
		{	/* Object/class.sch 212 */
			return
				BINT(BGl_jclasszd2occurrencezd2zzobject_classz00(
					((BgL_typez00_bglt) BgL_oz00_2982)));
		}

	}



/* jclass-occurrence-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jclasszd2occurrencezd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_137, int BgL_vz00_138)
	{
		{	/* Object/class.sch 213 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_137)))->BgL_occurrencez00) =
				((int) BgL_vz00_138), BUNSPEC);
		}

	}



/* &jclass-occurrence-set! */
	obj_t BGl_z62jclasszd2occurrencezd2setz12z70zzobject_classz00(obj_t
		BgL_envz00_2983, obj_t BgL_oz00_2984, obj_t BgL_vz00_2985)
	{
		{	/* Object/class.sch 213 */
			return
				BGl_jclasszd2occurrencezd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2984), CINT(BgL_vz00_2985));
		}

	}



/* jclass-import-location */
	BGL_EXPORTED_DEF obj_t
		BGl_jclasszd2importzd2locationz00zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_139)
	{
		{	/* Object/class.sch 214 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_139)))->BgL_importzd2locationzd2);
		}

	}



/* &jclass-import-location */
	obj_t BGl_z62jclasszd2importzd2locationz62zzobject_classz00(obj_t
		BgL_envz00_2986, obj_t BgL_oz00_2987)
	{
		{	/* Object/class.sch 214 */
			return
				BGl_jclasszd2importzd2locationz00zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2987));
		}

	}



/* jclass-import-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jclasszd2importzd2locationzd2setz12zc0zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_140, obj_t BgL_vz00_141)
	{
		{	/* Object/class.sch 215 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_140)))->BgL_importzd2locationzd2) =
				((obj_t) BgL_vz00_141), BUNSPEC);
		}

	}



/* &jclass-import-location-set! */
	obj_t BGl_z62jclasszd2importzd2locationzd2setz12za2zzobject_classz00(obj_t
		BgL_envz00_2988, obj_t BgL_oz00_2989, obj_t BgL_vz00_2990)
	{
		{	/* Object/class.sch 215 */
			return
				BGl_jclasszd2importzd2locationzd2setz12zc0zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2989), BgL_vz00_2990);
		}

	}



/* jclass-location */
	BGL_EXPORTED_DEF obj_t
		BGl_jclasszd2locationzd2zzobject_classz00(BgL_typez00_bglt BgL_oz00_142)
	{
		{	/* Object/class.sch 216 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_142)))->BgL_locationz00);
		}

	}



/* &jclass-location */
	obj_t BGl_z62jclasszd2locationzb0zzobject_classz00(obj_t BgL_envz00_2991,
		obj_t BgL_oz00_2992)
	{
		{	/* Object/class.sch 216 */
			return
				BGl_jclasszd2locationzd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2992));
		}

	}



/* jclass-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jclasszd2locationzd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_143, obj_t BgL_vz00_144)
	{
		{	/* Object/class.sch 217 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_143)))->BgL_locationz00) =
				((obj_t) BgL_vz00_144), BUNSPEC);
		}

	}



/* &jclass-location-set! */
	obj_t BGl_z62jclasszd2locationzd2setz12z70zzobject_classz00(obj_t
		BgL_envz00_2993, obj_t BgL_oz00_2994, obj_t BgL_vz00_2995)
	{
		{	/* Object/class.sch 217 */
			return
				BGl_jclasszd2locationzd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2994), BgL_vz00_2995);
		}

	}



/* jclass-tvector */
	BGL_EXPORTED_DEF obj_t
		BGl_jclasszd2tvectorzd2zzobject_classz00(BgL_typez00_bglt BgL_oz00_145)
	{
		{	/* Object/class.sch 218 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_145)))->BgL_tvectorz00);
		}

	}



/* &jclass-tvector */
	obj_t BGl_z62jclasszd2tvectorzb0zzobject_classz00(obj_t BgL_envz00_2996,
		obj_t BgL_oz00_2997)
	{
		{	/* Object/class.sch 218 */
			return
				BGl_jclasszd2tvectorzd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2997));
		}

	}



/* jclass-tvector-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jclasszd2tvectorzd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_146, obj_t BgL_vz00_147)
	{
		{	/* Object/class.sch 219 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_146)))->BgL_tvectorz00) =
				((obj_t) BgL_vz00_147), BUNSPEC);
		}

	}



/* &jclass-tvector-set! */
	obj_t BGl_z62jclasszd2tvectorzd2setz12z70zzobject_classz00(obj_t
		BgL_envz00_2998, obj_t BgL_oz00_2999, obj_t BgL_vz00_3000)
	{
		{	/* Object/class.sch 219 */
			return
				BGl_jclasszd2tvectorzd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_2999), BgL_vz00_3000);
		}

	}



/* jclass-pointed-to-by */
	BGL_EXPORTED_DEF obj_t
		BGl_jclasszd2pointedzd2tozd2byzd2zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_148)
	{
		{	/* Object/class.sch 220 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_148)))->BgL_pointedzd2tozd2byz00);
		}

	}



/* &jclass-pointed-to-by */
	obj_t BGl_z62jclasszd2pointedzd2tozd2byzb0zzobject_classz00(obj_t
		BgL_envz00_3001, obj_t BgL_oz00_3002)
	{
		{	/* Object/class.sch 220 */
			return
				BGl_jclasszd2pointedzd2tozd2byzd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3002));
		}

	}



/* jclass-pointed-to-by-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jclasszd2pointedzd2tozd2byzd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_149, obj_t BgL_vz00_150)
	{
		{	/* Object/class.sch 221 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_149)))->BgL_pointedzd2tozd2byz00) =
				((obj_t) BgL_vz00_150), BUNSPEC);
		}

	}



/* &jclass-pointed-to-by-set! */
	obj_t BGl_z62jclasszd2pointedzd2tozd2byzd2setz12z70zzobject_classz00(obj_t
		BgL_envz00_3003, obj_t BgL_oz00_3004, obj_t BgL_vz00_3005)
	{
		{	/* Object/class.sch 221 */
			return
				BGl_jclasszd2pointedzd2tozd2byzd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3004), BgL_vz00_3005);
		}

	}



/* jclass-alias */
	BGL_EXPORTED_DEF obj_t BGl_jclasszd2aliaszd2zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_151)
	{
		{	/* Object/class.sch 222 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_151)))->BgL_aliasz00);
		}

	}



/* &jclass-alias */
	obj_t BGl_z62jclasszd2aliaszb0zzobject_classz00(obj_t BgL_envz00_3006,
		obj_t BgL_oz00_3007)
	{
		{	/* Object/class.sch 222 */
			return
				BGl_jclasszd2aliaszd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3007));
		}

	}



/* jclass-alias-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jclasszd2aliaszd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_152, obj_t BgL_vz00_153)
	{
		{	/* Object/class.sch 223 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_152)))->BgL_aliasz00) =
				((obj_t) BgL_vz00_153), BUNSPEC);
		}

	}



/* &jclass-alias-set! */
	obj_t BGl_z62jclasszd2aliaszd2setz12z70zzobject_classz00(obj_t
		BgL_envz00_3008, obj_t BgL_oz00_3009, obj_t BgL_vz00_3010)
	{
		{	/* Object/class.sch 223 */
			return
				BGl_jclasszd2aliaszd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3009), BgL_vz00_3010);
		}

	}



/* jclass-$ */
	BGL_EXPORTED_DEF obj_t BGl_jclasszd2z42z90zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_154)
	{
		{	/* Object/class.sch 224 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_154)))->BgL_z42z42);
		}

	}



/* &jclass-$ */
	obj_t BGl_z62jclasszd2z42zf2zzobject_classz00(obj_t BgL_envz00_3011,
		obj_t BgL_oz00_3012)
	{
		{	/* Object/class.sch 224 */
			return
				BGl_jclasszd2z42z90zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3012));
		}

	}



/* jclass-$-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jclasszd2z42zd2setz12z50zzobject_classz00(BgL_typez00_bglt BgL_oz00_155,
		obj_t BgL_vz00_156)
	{
		{	/* Object/class.sch 225 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_155)))->BgL_z42z42) =
				((obj_t) BgL_vz00_156), BUNSPEC);
		}

	}



/* &jclass-$-set! */
	obj_t BGl_z62jclasszd2z42zd2setz12z32zzobject_classz00(obj_t BgL_envz00_3013,
		obj_t BgL_oz00_3014, obj_t BgL_vz00_3015)
	{
		{	/* Object/class.sch 225 */
			return
				BGl_jclasszd2z42zd2setz12z50zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3014), BgL_vz00_3015);
		}

	}



/* jclass-magic? */
	BGL_EXPORTED_DEF bool_t
		BGl_jclasszd2magiczf3z21zzobject_classz00(BgL_typez00_bglt BgL_oz00_157)
	{
		{	/* Object/class.sch 226 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_157)))->BgL_magiczf3zf3);
		}

	}



/* &jclass-magic? */
	obj_t BGl_z62jclasszd2magiczf3z43zzobject_classz00(obj_t BgL_envz00_3016,
		obj_t BgL_oz00_3017)
	{
		{	/* Object/class.sch 226 */
			return
				BBOOL(BGl_jclasszd2magiczf3z21zzobject_classz00(
					((BgL_typez00_bglt) BgL_oz00_3017)));
		}

	}



/* jclass-magic?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jclasszd2magiczf3zd2setz12ze1zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_158, bool_t BgL_vz00_159)
	{
		{	/* Object/class.sch 227 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_158)))->BgL_magiczf3zf3) =
				((bool_t) BgL_vz00_159), BUNSPEC);
		}

	}



/* &jclass-magic?-set! */
	obj_t BGl_z62jclasszd2magiczf3zd2setz12z83zzobject_classz00(obj_t
		BgL_envz00_3018, obj_t BgL_oz00_3019, obj_t BgL_vz00_3020)
	{
		{	/* Object/class.sch 227 */
			return
				BGl_jclasszd2magiczf3zd2setz12ze1zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3019), CBOOL(BgL_vz00_3020));
		}

	}



/* jclass-init? */
	BGL_EXPORTED_DEF bool_t
		BGl_jclasszd2initzf3z21zzobject_classz00(BgL_typez00_bglt BgL_oz00_160)
	{
		{	/* Object/class.sch 228 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_160)))->BgL_initzf3zf3);
		}

	}



/* &jclass-init? */
	obj_t BGl_z62jclasszd2initzf3z43zzobject_classz00(obj_t BgL_envz00_3021,
		obj_t BgL_oz00_3022)
	{
		{	/* Object/class.sch 228 */
			return
				BBOOL(BGl_jclasszd2initzf3z21zzobject_classz00(
					((BgL_typez00_bglt) BgL_oz00_3022)));
		}

	}



/* jclass-init?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jclasszd2initzf3zd2setz12ze1zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_161, bool_t BgL_vz00_162)
	{
		{	/* Object/class.sch 229 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_161)))->BgL_initzf3zf3) =
				((bool_t) BgL_vz00_162), BUNSPEC);
		}

	}



/* &jclass-init?-set! */
	obj_t BGl_z62jclasszd2initzf3zd2setz12z83zzobject_classz00(obj_t
		BgL_envz00_3023, obj_t BgL_oz00_3024, obj_t BgL_vz00_3025)
	{
		{	/* Object/class.sch 229 */
			return
				BGl_jclasszd2initzf3zd2setz12ze1zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3024), CBOOL(BgL_vz00_3025));
		}

	}



/* jclass-parents */
	BGL_EXPORTED_DEF obj_t
		BGl_jclasszd2parentszd2zzobject_classz00(BgL_typez00_bglt BgL_oz00_163)
	{
		{	/* Object/class.sch 230 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_163)))->BgL_parentsz00);
		}

	}



/* &jclass-parents */
	obj_t BGl_z62jclasszd2parentszb0zzobject_classz00(obj_t BgL_envz00_3026,
		obj_t BgL_oz00_3027)
	{
		{	/* Object/class.sch 230 */
			return
				BGl_jclasszd2parentszd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3027));
		}

	}



/* jclass-parents-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jclasszd2parentszd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_164, obj_t BgL_vz00_165)
	{
		{	/* Object/class.sch 231 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_164)))->BgL_parentsz00) =
				((obj_t) BgL_vz00_165), BUNSPEC);
		}

	}



/* &jclass-parents-set! */
	obj_t BGl_z62jclasszd2parentszd2setz12z70zzobject_classz00(obj_t
		BgL_envz00_3028, obj_t BgL_oz00_3029, obj_t BgL_vz00_3030)
	{
		{	/* Object/class.sch 231 */
			return
				BGl_jclasszd2parentszd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3029), BgL_vz00_3030);
		}

	}



/* jclass-coerce-to */
	BGL_EXPORTED_DEF obj_t
		BGl_jclasszd2coercezd2toz00zzobject_classz00(BgL_typez00_bglt BgL_oz00_166)
	{
		{	/* Object/class.sch 232 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_166)))->BgL_coercezd2tozd2);
		}

	}



/* &jclass-coerce-to */
	obj_t BGl_z62jclasszd2coercezd2toz62zzobject_classz00(obj_t BgL_envz00_3031,
		obj_t BgL_oz00_3032)
	{
		{	/* Object/class.sch 232 */
			return
				BGl_jclasszd2coercezd2toz00zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3032));
		}

	}



/* jclass-coerce-to-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jclasszd2coercezd2tozd2setz12zc0zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_167, obj_t BgL_vz00_168)
	{
		{	/* Object/class.sch 233 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_167)))->BgL_coercezd2tozd2) =
				((obj_t) BgL_vz00_168), BUNSPEC);
		}

	}



/* &jclass-coerce-to-set! */
	obj_t BGl_z62jclasszd2coercezd2tozd2setz12za2zzobject_classz00(obj_t
		BgL_envz00_3033, obj_t BgL_oz00_3034, obj_t BgL_vz00_3035)
	{
		{	/* Object/class.sch 233 */
			return
				BGl_jclasszd2coercezd2tozd2setz12zc0zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3034), BgL_vz00_3035);
		}

	}



/* jclass-class */
	BGL_EXPORTED_DEF obj_t BGl_jclasszd2classzd2zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_169)
	{
		{	/* Object/class.sch 234 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_169)))->BgL_classz00);
		}

	}



/* &jclass-class */
	obj_t BGl_z62jclasszd2classzb0zzobject_classz00(obj_t BgL_envz00_3036,
		obj_t BgL_oz00_3037)
	{
		{	/* Object/class.sch 234 */
			return
				BGl_jclasszd2classzd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3037));
		}

	}



/* jclass-class-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jclasszd2classzd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_170, obj_t BgL_vz00_171)
	{
		{	/* Object/class.sch 235 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_170)))->BgL_classz00) =
				((obj_t) BgL_vz00_171), BUNSPEC);
		}

	}



/* &jclass-class-set! */
	obj_t BGl_z62jclasszd2classzd2setz12z70zzobject_classz00(obj_t
		BgL_envz00_3038, obj_t BgL_oz00_3039, obj_t BgL_vz00_3040)
	{
		{	/* Object/class.sch 235 */
			return
				BGl_jclasszd2classzd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3039), BgL_vz00_3040);
		}

	}



/* jclass-size */
	BGL_EXPORTED_DEF obj_t
		BGl_jclasszd2siza7ez75zzobject_classz00(BgL_typez00_bglt BgL_oz00_172)
	{
		{	/* Object/class.sch 236 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_172)))->BgL_siza7eza7);
		}

	}



/* &jclass-size */
	obj_t BGl_z62jclasszd2siza7ez17zzobject_classz00(obj_t BgL_envz00_3041,
		obj_t BgL_oz00_3042)
	{
		{	/* Object/class.sch 236 */
			return
				BGl_jclasszd2siza7ez75zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3042));
		}

	}



/* jclass-size-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jclasszd2siza7ezd2setz12zb5zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_173, obj_t BgL_vz00_174)
	{
		{	/* Object/class.sch 237 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_173)))->BgL_siza7eza7) =
				((obj_t) BgL_vz00_174), BUNSPEC);
		}

	}



/* &jclass-size-set! */
	obj_t BGl_z62jclasszd2siza7ezd2setz12zd7zzobject_classz00(obj_t
		BgL_envz00_3043, obj_t BgL_oz00_3044, obj_t BgL_vz00_3045)
	{
		{	/* Object/class.sch 237 */
			return
				BGl_jclasszd2siza7ezd2setz12zb5zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3044), BgL_vz00_3045);
		}

	}



/* jclass-name */
	BGL_EXPORTED_DEF obj_t BGl_jclasszd2namezd2zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_175)
	{
		{	/* Object/class.sch 238 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_175)))->BgL_namez00);
		}

	}



/* &jclass-name */
	obj_t BGl_z62jclasszd2namezb0zzobject_classz00(obj_t BgL_envz00_3046,
		obj_t BgL_oz00_3047)
	{
		{	/* Object/class.sch 238 */
			return
				BGl_jclasszd2namezd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3047));
		}

	}



/* jclass-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jclasszd2namezd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_176, obj_t BgL_vz00_177)
	{
		{	/* Object/class.sch 239 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_176)))->BgL_namez00) =
				((obj_t) BgL_vz00_177), BUNSPEC);
		}

	}



/* &jclass-name-set! */
	obj_t BGl_z62jclasszd2namezd2setz12z70zzobject_classz00(obj_t BgL_envz00_3048,
		obj_t BgL_oz00_3049, obj_t BgL_vz00_3050)
	{
		{	/* Object/class.sch 239 */
			return
				BGl_jclasszd2namezd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3049), BgL_vz00_3050);
		}

	}



/* jclass-id */
	BGL_EXPORTED_DEF obj_t BGl_jclasszd2idzd2zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_178)
	{
		{	/* Object/class.sch 240 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_178)))->BgL_idz00);
		}

	}



/* &jclass-id */
	obj_t BGl_z62jclasszd2idzb0zzobject_classz00(obj_t BgL_envz00_3051,
		obj_t BgL_oz00_3052)
	{
		{	/* Object/class.sch 240 */
			return
				BGl_jclasszd2idzd2zzobject_classz00(((BgL_typez00_bglt) BgL_oz00_3052));
		}

	}



/* make-wclass */
	BGL_EXPORTED_DEF BgL_typez00_bglt BGl_makezd2wclasszd2zzobject_classz00(obj_t
		BgL_id1141z00_181, obj_t BgL_name1142z00_182, obj_t BgL_siza7e1143za7_183,
		obj_t BgL_class1144z00_184, obj_t BgL_coercezd2to1145zd2_185,
		obj_t BgL_parents1146z00_186, bool_t BgL_initzf31147zf3_187,
		bool_t BgL_magiczf31148zf3_188, obj_t BgL_z421149z42_189,
		obj_t BgL_alias1150z00_190, obj_t BgL_pointedzd2tozd2by1151z00_191,
		obj_t BgL_tvector1152z00_192, obj_t BgL_location1153z00_193,
		obj_t BgL_importzd2location1154zd2_194, int BgL_occurrence1155z00_195,
		obj_t BgL_itszd2class1156zd2_196)
	{
		{	/* Object/class.sch 244 */
			{	/* Object/class.sch 244 */
				BgL_typez00_bglt BgL_new1156z00_3480;

				{	/* Object/class.sch 244 */
					BgL_typez00_bglt BgL_tmp1153z00_3481;
					BgL_wclassz00_bglt BgL_wide1154z00_3482;

					{
						BgL_typez00_bglt BgL_auxz00_4375;

						{	/* Object/class.sch 244 */
							BgL_typez00_bglt BgL_new1152z00_3483;

							BgL_new1152z00_3483 =
								((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_typez00_bgl))));
							{	/* Object/class.sch 244 */
								long BgL_arg1252z00_3484;

								BgL_arg1252z00_3484 = BGL_CLASS_NUM(BGl_typez00zztype_typez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1152z00_3483),
									BgL_arg1252z00_3484);
							}
							{	/* Object/class.sch 244 */
								BgL_objectz00_bglt BgL_tmpz00_4380;

								BgL_tmpz00_4380 = ((BgL_objectz00_bglt) BgL_new1152z00_3483);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4380, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1152z00_3483);
							BgL_auxz00_4375 = BgL_new1152z00_3483;
						}
						BgL_tmp1153z00_3481 = ((BgL_typez00_bglt) BgL_auxz00_4375);
					}
					BgL_wide1154z00_3482 =
						((BgL_wclassz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_wclassz00_bgl))));
					{	/* Object/class.sch 244 */
						obj_t BgL_auxz00_4388;
						BgL_objectz00_bglt BgL_tmpz00_4386;

						BgL_auxz00_4388 = ((obj_t) BgL_wide1154z00_3482);
						BgL_tmpz00_4386 = ((BgL_objectz00_bglt) BgL_tmp1153z00_3481);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4386, BgL_auxz00_4388);
					}
					((BgL_objectz00_bglt) BgL_tmp1153z00_3481);
					{	/* Object/class.sch 244 */
						long BgL_arg1249z00_3485;

						BgL_arg1249z00_3485 = BGL_CLASS_NUM(BGl_wclassz00zzobject_classz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1153z00_3481), BgL_arg1249z00_3485);
					}
					BgL_new1156z00_3480 = ((BgL_typez00_bglt) BgL_tmp1153z00_3481);
				}
				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_new1156z00_3480)))->BgL_idz00) =
					((obj_t) BgL_id1141z00_181), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1156z00_3480)))->BgL_namez00) =
					((obj_t) BgL_name1142z00_182), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1156z00_3480)))->BgL_siza7eza7) =
					((obj_t) BgL_siza7e1143za7_183), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1156z00_3480)))->BgL_classz00) =
					((obj_t) BgL_class1144z00_184), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1156z00_3480)))->BgL_coercezd2tozd2) =
					((obj_t) BgL_coercezd2to1145zd2_185), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1156z00_3480)))->BgL_parentsz00) =
					((obj_t) BgL_parents1146z00_186), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1156z00_3480)))->BgL_initzf3zf3) =
					((bool_t) BgL_initzf31147zf3_187), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1156z00_3480)))->BgL_magiczf3zf3) =
					((bool_t) BgL_magiczf31148zf3_188), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1156z00_3480)))->BgL_nullz00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1156z00_3480)))->BgL_z42z42) =
					((obj_t) BgL_z421149z42_189), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1156z00_3480)))->BgL_aliasz00) =
					((obj_t) BgL_alias1150z00_190), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1156z00_3480)))->BgL_pointedzd2tozd2byz00) =
					((obj_t) BgL_pointedzd2tozd2by1151z00_191), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1156z00_3480)))->BgL_tvectorz00) =
					((obj_t) BgL_tvector1152z00_192), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1156z00_3480)))->BgL_locationz00) =
					((obj_t) BgL_location1153z00_193), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1156z00_3480)))->BgL_importzd2locationzd2) =
					((obj_t) BgL_importzd2location1154zd2_194), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1156z00_3480)))->BgL_occurrencez00) =
					((int) BgL_occurrence1155z00_195), BUNSPEC);
				{
					BgL_wclassz00_bglt BgL_auxz00_4428;

					{
						obj_t BgL_auxz00_4429;

						{	/* Object/class.sch 244 */
							BgL_objectz00_bglt BgL_tmpz00_4430;

							BgL_tmpz00_4430 = ((BgL_objectz00_bglt) BgL_new1156z00_3480);
							BgL_auxz00_4429 = BGL_OBJECT_WIDENING(BgL_tmpz00_4430);
						}
						BgL_auxz00_4428 = ((BgL_wclassz00_bglt) BgL_auxz00_4429);
					}
					((((BgL_wclassz00_bglt) COBJECT(BgL_auxz00_4428))->
							BgL_itszd2classzd2) =
						((obj_t) BgL_itszd2class1156zd2_196), BUNSPEC);
				}
				return BgL_new1156z00_3480;
			}
		}

	}



/* &make-wclass */
	BgL_typez00_bglt BGl_z62makezd2wclasszb0zzobject_classz00(obj_t
		BgL_envz00_3053, obj_t BgL_id1141z00_3054, obj_t BgL_name1142z00_3055,
		obj_t BgL_siza7e1143za7_3056, obj_t BgL_class1144z00_3057,
		obj_t BgL_coercezd2to1145zd2_3058, obj_t BgL_parents1146z00_3059,
		obj_t BgL_initzf31147zf3_3060, obj_t BgL_magiczf31148zf3_3061,
		obj_t BgL_z421149z42_3062, obj_t BgL_alias1150z00_3063,
		obj_t BgL_pointedzd2tozd2by1151z00_3064, obj_t BgL_tvector1152z00_3065,
		obj_t BgL_location1153z00_3066, obj_t BgL_importzd2location1154zd2_3067,
		obj_t BgL_occurrence1155z00_3068, obj_t BgL_itszd2class1156zd2_3069)
	{
		{	/* Object/class.sch 244 */
			return
				BGl_makezd2wclasszd2zzobject_classz00(BgL_id1141z00_3054,
				BgL_name1142z00_3055, BgL_siza7e1143za7_3056, BgL_class1144z00_3057,
				BgL_coercezd2to1145zd2_3058, BgL_parents1146z00_3059,
				CBOOL(BgL_initzf31147zf3_3060), CBOOL(BgL_magiczf31148zf3_3061),
				BgL_z421149z42_3062, BgL_alias1150z00_3063,
				BgL_pointedzd2tozd2by1151z00_3064, BgL_tvector1152z00_3065,
				BgL_location1153z00_3066, BgL_importzd2location1154zd2_3067,
				CINT(BgL_occurrence1155z00_3068), BgL_itszd2class1156zd2_3069);
		}

	}



/* wclass? */
	BGL_EXPORTED_DEF bool_t BGl_wclasszf3zf3zzobject_classz00(obj_t
		BgL_objz00_197)
	{
		{	/* Object/class.sch 245 */
			{	/* Object/class.sch 245 */
				obj_t BgL_classz00_3486;

				BgL_classz00_3486 = BGl_wclassz00zzobject_classz00;
				if (BGL_OBJECTP(BgL_objz00_197))
					{	/* Object/class.sch 245 */
						BgL_objectz00_bglt BgL_arg1807z00_3487;

						BgL_arg1807z00_3487 = (BgL_objectz00_bglt) (BgL_objz00_197);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Object/class.sch 245 */
								long BgL_idxz00_3488;

								BgL_idxz00_3488 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3487);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3488 + 2L)) == BgL_classz00_3486);
							}
						else
							{	/* Object/class.sch 245 */
								bool_t BgL_res1906z00_3491;

								{	/* Object/class.sch 245 */
									obj_t BgL_oclassz00_3492;

									{	/* Object/class.sch 245 */
										obj_t BgL_arg1815z00_3493;
										long BgL_arg1816z00_3494;

										BgL_arg1815z00_3493 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Object/class.sch 245 */
											long BgL_arg1817z00_3495;

											BgL_arg1817z00_3495 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3487);
											BgL_arg1816z00_3494 = (BgL_arg1817z00_3495 - OBJECT_TYPE);
										}
										BgL_oclassz00_3492 =
											VECTOR_REF(BgL_arg1815z00_3493, BgL_arg1816z00_3494);
									}
									{	/* Object/class.sch 245 */
										bool_t BgL__ortest_1115z00_3496;

										BgL__ortest_1115z00_3496 =
											(BgL_classz00_3486 == BgL_oclassz00_3492);
										if (BgL__ortest_1115z00_3496)
											{	/* Object/class.sch 245 */
												BgL_res1906z00_3491 = BgL__ortest_1115z00_3496;
											}
										else
											{	/* Object/class.sch 245 */
												long BgL_odepthz00_3497;

												{	/* Object/class.sch 245 */
													obj_t BgL_arg1804z00_3498;

													BgL_arg1804z00_3498 = (BgL_oclassz00_3492);
													BgL_odepthz00_3497 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3498);
												}
												if ((2L < BgL_odepthz00_3497))
													{	/* Object/class.sch 245 */
														obj_t BgL_arg1802z00_3499;

														{	/* Object/class.sch 245 */
															obj_t BgL_arg1803z00_3500;

															BgL_arg1803z00_3500 = (BgL_oclassz00_3492);
															BgL_arg1802z00_3499 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3500,
																2L);
														}
														BgL_res1906z00_3491 =
															(BgL_arg1802z00_3499 == BgL_classz00_3486);
													}
												else
													{	/* Object/class.sch 245 */
														BgL_res1906z00_3491 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1906z00_3491;
							}
					}
				else
					{	/* Object/class.sch 245 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &wclass? */
	obj_t BGl_z62wclasszf3z91zzobject_classz00(obj_t BgL_envz00_3070,
		obj_t BgL_objz00_3071)
	{
		{	/* Object/class.sch 245 */
			return BBOOL(BGl_wclasszf3zf3zzobject_classz00(BgL_objz00_3071));
		}

	}



/* wclass-nil */
	BGL_EXPORTED_DEF BgL_typez00_bglt BGl_wclasszd2nilzd2zzobject_classz00(void)
	{
		{	/* Object/class.sch 246 */
			{	/* Object/class.sch 246 */
				obj_t BgL_classz00_1616;

				BgL_classz00_1616 = BGl_wclassz00zzobject_classz00;
				{	/* Object/class.sch 246 */
					obj_t BgL__ortest_1117z00_1617;

					BgL__ortest_1117z00_1617 = BGL_CLASS_NIL(BgL_classz00_1616);
					if (CBOOL(BgL__ortest_1117z00_1617))
						{	/* Object/class.sch 246 */
							return ((BgL_typez00_bglt) BgL__ortest_1117z00_1617);
						}
					else
						{	/* Object/class.sch 246 */
							return
								((BgL_typez00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1616));
						}
				}
			}
		}

	}



/* &wclass-nil */
	BgL_typez00_bglt BGl_z62wclasszd2nilzb0zzobject_classz00(obj_t
		BgL_envz00_3072)
	{
		{	/* Object/class.sch 246 */
			return BGl_wclasszd2nilzd2zzobject_classz00();
		}

	}



/* wclass-its-class */
	BGL_EXPORTED_DEF obj_t
		BGl_wclasszd2itszd2classz00zzobject_classz00(BgL_typez00_bglt BgL_oz00_198)
	{
		{	/* Object/class.sch 247 */
			{
				BgL_wclassz00_bglt BgL_auxz00_4470;

				{
					obj_t BgL_auxz00_4471;

					{	/* Object/class.sch 247 */
						BgL_objectz00_bglt BgL_tmpz00_4472;

						BgL_tmpz00_4472 = ((BgL_objectz00_bglt) BgL_oz00_198);
						BgL_auxz00_4471 = BGL_OBJECT_WIDENING(BgL_tmpz00_4472);
					}
					BgL_auxz00_4470 = ((BgL_wclassz00_bglt) BgL_auxz00_4471);
				}
				return
					(((BgL_wclassz00_bglt) COBJECT(BgL_auxz00_4470))->BgL_itszd2classzd2);
			}
		}

	}



/* &wclass-its-class */
	obj_t BGl_z62wclasszd2itszd2classz62zzobject_classz00(obj_t BgL_envz00_3073,
		obj_t BgL_oz00_3074)
	{
		{	/* Object/class.sch 247 */
			return
				BGl_wclasszd2itszd2classz00zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3074));
		}

	}



/* wclass-its-class-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_wclasszd2itszd2classzd2setz12zc0zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_199, obj_t BgL_vz00_200)
	{
		{	/* Object/class.sch 248 */
			{
				BgL_wclassz00_bglt BgL_auxz00_4479;

				{
					obj_t BgL_auxz00_4480;

					{	/* Object/class.sch 248 */
						BgL_objectz00_bglt BgL_tmpz00_4481;

						BgL_tmpz00_4481 = ((BgL_objectz00_bglt) BgL_oz00_199);
						BgL_auxz00_4480 = BGL_OBJECT_WIDENING(BgL_tmpz00_4481);
					}
					BgL_auxz00_4479 = ((BgL_wclassz00_bglt) BgL_auxz00_4480);
				}
				return
					((((BgL_wclassz00_bglt) COBJECT(BgL_auxz00_4479))->
						BgL_itszd2classzd2) = ((obj_t) BgL_vz00_200), BUNSPEC);
			}
		}

	}



/* &wclass-its-class-set! */
	obj_t BGl_z62wclasszd2itszd2classzd2setz12za2zzobject_classz00(obj_t
		BgL_envz00_3075, obj_t BgL_oz00_3076, obj_t BgL_vz00_3077)
	{
		{	/* Object/class.sch 248 */
			return
				BGl_wclasszd2itszd2classzd2setz12zc0zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3076), BgL_vz00_3077);
		}

	}



/* wclass-occurrence */
	BGL_EXPORTED_DEF int
		BGl_wclasszd2occurrencezd2zzobject_classz00(BgL_typez00_bglt BgL_oz00_201)
	{
		{	/* Object/class.sch 249 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_201)))->BgL_occurrencez00);
		}

	}



/* &wclass-occurrence */
	obj_t BGl_z62wclasszd2occurrencezb0zzobject_classz00(obj_t BgL_envz00_3078,
		obj_t BgL_oz00_3079)
	{
		{	/* Object/class.sch 249 */
			return
				BINT(BGl_wclasszd2occurrencezd2zzobject_classz00(
					((BgL_typez00_bglt) BgL_oz00_3079)));
		}

	}



/* wclass-occurrence-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_wclasszd2occurrencezd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_202, int BgL_vz00_203)
	{
		{	/* Object/class.sch 250 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_202)))->BgL_occurrencez00) =
				((int) BgL_vz00_203), BUNSPEC);
		}

	}



/* &wclass-occurrence-set! */
	obj_t BGl_z62wclasszd2occurrencezd2setz12z70zzobject_classz00(obj_t
		BgL_envz00_3080, obj_t BgL_oz00_3081, obj_t BgL_vz00_3082)
	{
		{	/* Object/class.sch 250 */
			return
				BGl_wclasszd2occurrencezd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3081), CINT(BgL_vz00_3082));
		}

	}



/* wclass-import-location */
	BGL_EXPORTED_DEF obj_t
		BGl_wclasszd2importzd2locationz00zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_204)
	{
		{	/* Object/class.sch 251 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_204)))->BgL_importzd2locationzd2);
		}

	}



/* &wclass-import-location */
	obj_t BGl_z62wclasszd2importzd2locationz62zzobject_classz00(obj_t
		BgL_envz00_3083, obj_t BgL_oz00_3084)
	{
		{	/* Object/class.sch 251 */
			return
				BGl_wclasszd2importzd2locationz00zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3084));
		}

	}



/* wclass-import-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_wclasszd2importzd2locationzd2setz12zc0zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_205, obj_t BgL_vz00_206)
	{
		{	/* Object/class.sch 252 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_205)))->BgL_importzd2locationzd2) =
				((obj_t) BgL_vz00_206), BUNSPEC);
		}

	}



/* &wclass-import-location-set! */
	obj_t BGl_z62wclasszd2importzd2locationzd2setz12za2zzobject_classz00(obj_t
		BgL_envz00_3085, obj_t BgL_oz00_3086, obj_t BgL_vz00_3087)
	{
		{	/* Object/class.sch 252 */
			return
				BGl_wclasszd2importzd2locationzd2setz12zc0zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3086), BgL_vz00_3087);
		}

	}



/* wclass-location */
	BGL_EXPORTED_DEF obj_t
		BGl_wclasszd2locationzd2zzobject_classz00(BgL_typez00_bglt BgL_oz00_207)
	{
		{	/* Object/class.sch 253 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_207)))->BgL_locationz00);
		}

	}



/* &wclass-location */
	obj_t BGl_z62wclasszd2locationzb0zzobject_classz00(obj_t BgL_envz00_3088,
		obj_t BgL_oz00_3089)
	{
		{	/* Object/class.sch 253 */
			return
				BGl_wclasszd2locationzd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3089));
		}

	}



/* wclass-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_wclasszd2locationzd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_208, obj_t BgL_vz00_209)
	{
		{	/* Object/class.sch 254 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_208)))->BgL_locationz00) =
				((obj_t) BgL_vz00_209), BUNSPEC);
		}

	}



/* &wclass-location-set! */
	obj_t BGl_z62wclasszd2locationzd2setz12z70zzobject_classz00(obj_t
		BgL_envz00_3090, obj_t BgL_oz00_3091, obj_t BgL_vz00_3092)
	{
		{	/* Object/class.sch 254 */
			return
				BGl_wclasszd2locationzd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3091), BgL_vz00_3092);
		}

	}



/* wclass-tvector */
	BGL_EXPORTED_DEF obj_t
		BGl_wclasszd2tvectorzd2zzobject_classz00(BgL_typez00_bglt BgL_oz00_210)
	{
		{	/* Object/class.sch 255 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_210)))->BgL_tvectorz00);
		}

	}



/* &wclass-tvector */
	obj_t BGl_z62wclasszd2tvectorzb0zzobject_classz00(obj_t BgL_envz00_3093,
		obj_t BgL_oz00_3094)
	{
		{	/* Object/class.sch 255 */
			return
				BGl_wclasszd2tvectorzd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3094));
		}

	}



/* wclass-tvector-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_wclasszd2tvectorzd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_211, obj_t BgL_vz00_212)
	{
		{	/* Object/class.sch 256 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_211)))->BgL_tvectorz00) =
				((obj_t) BgL_vz00_212), BUNSPEC);
		}

	}



/* &wclass-tvector-set! */
	obj_t BGl_z62wclasszd2tvectorzd2setz12z70zzobject_classz00(obj_t
		BgL_envz00_3095, obj_t BgL_oz00_3096, obj_t BgL_vz00_3097)
	{
		{	/* Object/class.sch 256 */
			return
				BGl_wclasszd2tvectorzd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3096), BgL_vz00_3097);
		}

	}



/* wclass-pointed-to-by */
	BGL_EXPORTED_DEF obj_t
		BGl_wclasszd2pointedzd2tozd2byzd2zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_213)
	{
		{	/* Object/class.sch 257 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_213)))->BgL_pointedzd2tozd2byz00);
		}

	}



/* &wclass-pointed-to-by */
	obj_t BGl_z62wclasszd2pointedzd2tozd2byzb0zzobject_classz00(obj_t
		BgL_envz00_3098, obj_t BgL_oz00_3099)
	{
		{	/* Object/class.sch 257 */
			return
				BGl_wclasszd2pointedzd2tozd2byzd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3099));
		}

	}



/* wclass-pointed-to-by-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_wclasszd2pointedzd2tozd2byzd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_214, obj_t BgL_vz00_215)
	{
		{	/* Object/class.sch 258 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_214)))->BgL_pointedzd2tozd2byz00) =
				((obj_t) BgL_vz00_215), BUNSPEC);
		}

	}



/* &wclass-pointed-to-by-set! */
	obj_t BGl_z62wclasszd2pointedzd2tozd2byzd2setz12z70zzobject_classz00(obj_t
		BgL_envz00_3100, obj_t BgL_oz00_3101, obj_t BgL_vz00_3102)
	{
		{	/* Object/class.sch 258 */
			return
				BGl_wclasszd2pointedzd2tozd2byzd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3101), BgL_vz00_3102);
		}

	}



/* wclass-alias */
	BGL_EXPORTED_DEF obj_t BGl_wclasszd2aliaszd2zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_216)
	{
		{	/* Object/class.sch 259 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_216)))->BgL_aliasz00);
		}

	}



/* &wclass-alias */
	obj_t BGl_z62wclasszd2aliaszb0zzobject_classz00(obj_t BgL_envz00_3103,
		obj_t BgL_oz00_3104)
	{
		{	/* Object/class.sch 259 */
			return
				BGl_wclasszd2aliaszd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3104));
		}

	}



/* wclass-alias-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_wclasszd2aliaszd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_217, obj_t BgL_vz00_218)
	{
		{	/* Object/class.sch 260 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_217)))->BgL_aliasz00) =
				((obj_t) BgL_vz00_218), BUNSPEC);
		}

	}



/* &wclass-alias-set! */
	obj_t BGl_z62wclasszd2aliaszd2setz12z70zzobject_classz00(obj_t
		BgL_envz00_3105, obj_t BgL_oz00_3106, obj_t BgL_vz00_3107)
	{
		{	/* Object/class.sch 260 */
			return
				BGl_wclasszd2aliaszd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3106), BgL_vz00_3107);
		}

	}



/* wclass-$ */
	BGL_EXPORTED_DEF obj_t BGl_wclasszd2z42z90zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_219)
	{
		{	/* Object/class.sch 261 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_219)))->BgL_z42z42);
		}

	}



/* &wclass-$ */
	obj_t BGl_z62wclasszd2z42zf2zzobject_classz00(obj_t BgL_envz00_3108,
		obj_t BgL_oz00_3109)
	{
		{	/* Object/class.sch 261 */
			return
				BGl_wclasszd2z42z90zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3109));
		}

	}



/* wclass-$-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_wclasszd2z42zd2setz12z50zzobject_classz00(BgL_typez00_bglt BgL_oz00_220,
		obj_t BgL_vz00_221)
	{
		{	/* Object/class.sch 262 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_220)))->BgL_z42z42) =
				((obj_t) BgL_vz00_221), BUNSPEC);
		}

	}



/* &wclass-$-set! */
	obj_t BGl_z62wclasszd2z42zd2setz12z32zzobject_classz00(obj_t BgL_envz00_3110,
		obj_t BgL_oz00_3111, obj_t BgL_vz00_3112)
	{
		{	/* Object/class.sch 262 */
			return
				BGl_wclasszd2z42zd2setz12z50zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3111), BgL_vz00_3112);
		}

	}



/* wclass-magic? */
	BGL_EXPORTED_DEF bool_t
		BGl_wclasszd2magiczf3z21zzobject_classz00(BgL_typez00_bglt BgL_oz00_222)
	{
		{	/* Object/class.sch 263 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_222)))->BgL_magiczf3zf3);
		}

	}



/* &wclass-magic? */
	obj_t BGl_z62wclasszd2magiczf3z43zzobject_classz00(obj_t BgL_envz00_3113,
		obj_t BgL_oz00_3114)
	{
		{	/* Object/class.sch 263 */
			return
				BBOOL(BGl_wclasszd2magiczf3z21zzobject_classz00(
					((BgL_typez00_bglt) BgL_oz00_3114)));
		}

	}



/* wclass-magic?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_wclasszd2magiczf3zd2setz12ze1zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_223, bool_t BgL_vz00_224)
	{
		{	/* Object/class.sch 264 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_223)))->BgL_magiczf3zf3) =
				((bool_t) BgL_vz00_224), BUNSPEC);
		}

	}



/* &wclass-magic?-set! */
	obj_t BGl_z62wclasszd2magiczf3zd2setz12z83zzobject_classz00(obj_t
		BgL_envz00_3115, obj_t BgL_oz00_3116, obj_t BgL_vz00_3117)
	{
		{	/* Object/class.sch 264 */
			return
				BGl_wclasszd2magiczf3zd2setz12ze1zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3116), CBOOL(BgL_vz00_3117));
		}

	}



/* wclass-init? */
	BGL_EXPORTED_DEF bool_t
		BGl_wclasszd2initzf3z21zzobject_classz00(BgL_typez00_bglt BgL_oz00_225)
	{
		{	/* Object/class.sch 265 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_225)))->BgL_initzf3zf3);
		}

	}



/* &wclass-init? */
	obj_t BGl_z62wclasszd2initzf3z43zzobject_classz00(obj_t BgL_envz00_3118,
		obj_t BgL_oz00_3119)
	{
		{	/* Object/class.sch 265 */
			return
				BBOOL(BGl_wclasszd2initzf3z21zzobject_classz00(
					((BgL_typez00_bglt) BgL_oz00_3119)));
		}

	}



/* wclass-init?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_wclasszd2initzf3zd2setz12ze1zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_226, bool_t BgL_vz00_227)
	{
		{	/* Object/class.sch 266 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_226)))->BgL_initzf3zf3) =
				((bool_t) BgL_vz00_227), BUNSPEC);
		}

	}



/* &wclass-init?-set! */
	obj_t BGl_z62wclasszd2initzf3zd2setz12z83zzobject_classz00(obj_t
		BgL_envz00_3120, obj_t BgL_oz00_3121, obj_t BgL_vz00_3122)
	{
		{	/* Object/class.sch 266 */
			return
				BGl_wclasszd2initzf3zd2setz12ze1zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3121), CBOOL(BgL_vz00_3122));
		}

	}



/* wclass-parents */
	BGL_EXPORTED_DEF obj_t
		BGl_wclasszd2parentszd2zzobject_classz00(BgL_typez00_bglt BgL_oz00_228)
	{
		{	/* Object/class.sch 267 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_228)))->BgL_parentsz00);
		}

	}



/* &wclass-parents */
	obj_t BGl_z62wclasszd2parentszb0zzobject_classz00(obj_t BgL_envz00_3123,
		obj_t BgL_oz00_3124)
	{
		{	/* Object/class.sch 267 */
			return
				BGl_wclasszd2parentszd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3124));
		}

	}



/* wclass-parents-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_wclasszd2parentszd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_229, obj_t BgL_vz00_230)
	{
		{	/* Object/class.sch 268 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_229)))->BgL_parentsz00) =
				((obj_t) BgL_vz00_230), BUNSPEC);
		}

	}



/* &wclass-parents-set! */
	obj_t BGl_z62wclasszd2parentszd2setz12z70zzobject_classz00(obj_t
		BgL_envz00_3125, obj_t BgL_oz00_3126, obj_t BgL_vz00_3127)
	{
		{	/* Object/class.sch 268 */
			return
				BGl_wclasszd2parentszd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3126), BgL_vz00_3127);
		}

	}



/* wclass-coerce-to */
	BGL_EXPORTED_DEF obj_t
		BGl_wclasszd2coercezd2toz00zzobject_classz00(BgL_typez00_bglt BgL_oz00_231)
	{
		{	/* Object/class.sch 269 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_231)))->BgL_coercezd2tozd2);
		}

	}



/* &wclass-coerce-to */
	obj_t BGl_z62wclasszd2coercezd2toz62zzobject_classz00(obj_t BgL_envz00_3128,
		obj_t BgL_oz00_3129)
	{
		{	/* Object/class.sch 269 */
			return
				BGl_wclasszd2coercezd2toz00zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3129));
		}

	}



/* wclass-coerce-to-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_wclasszd2coercezd2tozd2setz12zc0zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_232, obj_t BgL_vz00_233)
	{
		{	/* Object/class.sch 270 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_232)))->BgL_coercezd2tozd2) =
				((obj_t) BgL_vz00_233), BUNSPEC);
		}

	}



/* &wclass-coerce-to-set! */
	obj_t BGl_z62wclasszd2coercezd2tozd2setz12za2zzobject_classz00(obj_t
		BgL_envz00_3130, obj_t BgL_oz00_3131, obj_t BgL_vz00_3132)
	{
		{	/* Object/class.sch 270 */
			return
				BGl_wclasszd2coercezd2tozd2setz12zc0zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3131), BgL_vz00_3132);
		}

	}



/* wclass-class */
	BGL_EXPORTED_DEF obj_t BGl_wclasszd2classzd2zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_234)
	{
		{	/* Object/class.sch 271 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_234)))->BgL_classz00);
		}

	}



/* &wclass-class */
	obj_t BGl_z62wclasszd2classzb0zzobject_classz00(obj_t BgL_envz00_3133,
		obj_t BgL_oz00_3134)
	{
		{	/* Object/class.sch 271 */
			return
				BGl_wclasszd2classzd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3134));
		}

	}



/* wclass-class-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_wclasszd2classzd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_235, obj_t BgL_vz00_236)
	{
		{	/* Object/class.sch 272 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_235)))->BgL_classz00) =
				((obj_t) BgL_vz00_236), BUNSPEC);
		}

	}



/* &wclass-class-set! */
	obj_t BGl_z62wclasszd2classzd2setz12z70zzobject_classz00(obj_t
		BgL_envz00_3135, obj_t BgL_oz00_3136, obj_t BgL_vz00_3137)
	{
		{	/* Object/class.sch 272 */
			return
				BGl_wclasszd2classzd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3136), BgL_vz00_3137);
		}

	}



/* wclass-size */
	BGL_EXPORTED_DEF obj_t
		BGl_wclasszd2siza7ez75zzobject_classz00(BgL_typez00_bglt BgL_oz00_237)
	{
		{	/* Object/class.sch 273 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_237)))->BgL_siza7eza7);
		}

	}



/* &wclass-size */
	obj_t BGl_z62wclasszd2siza7ez17zzobject_classz00(obj_t BgL_envz00_3138,
		obj_t BgL_oz00_3139)
	{
		{	/* Object/class.sch 273 */
			return
				BGl_wclasszd2siza7ez75zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3139));
		}

	}



/* wclass-size-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_wclasszd2siza7ezd2setz12zb5zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_238, obj_t BgL_vz00_239)
	{
		{	/* Object/class.sch 274 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_238)))->BgL_siza7eza7) =
				((obj_t) BgL_vz00_239), BUNSPEC);
		}

	}



/* &wclass-size-set! */
	obj_t BGl_z62wclasszd2siza7ezd2setz12zd7zzobject_classz00(obj_t
		BgL_envz00_3140, obj_t BgL_oz00_3141, obj_t BgL_vz00_3142)
	{
		{	/* Object/class.sch 274 */
			return
				BGl_wclasszd2siza7ezd2setz12zb5zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3141), BgL_vz00_3142);
		}

	}



/* wclass-name */
	BGL_EXPORTED_DEF obj_t BGl_wclasszd2namezd2zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_240)
	{
		{	/* Object/class.sch 275 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_240)))->BgL_namez00);
		}

	}



/* &wclass-name */
	obj_t BGl_z62wclasszd2namezb0zzobject_classz00(obj_t BgL_envz00_3143,
		obj_t BgL_oz00_3144)
	{
		{	/* Object/class.sch 275 */
			return
				BGl_wclasszd2namezd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3144));
		}

	}



/* wclass-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_wclasszd2namezd2setz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_241, obj_t BgL_vz00_242)
	{
		{	/* Object/class.sch 276 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_241)))->BgL_namez00) =
				((obj_t) BgL_vz00_242), BUNSPEC);
		}

	}



/* &wclass-name-set! */
	obj_t BGl_z62wclasszd2namezd2setz12z70zzobject_classz00(obj_t BgL_envz00_3145,
		obj_t BgL_oz00_3146, obj_t BgL_vz00_3147)
	{
		{	/* Object/class.sch 276 */
			return
				BGl_wclasszd2namezd2setz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_oz00_3146), BgL_vz00_3147);
		}

	}



/* wclass-id */
	BGL_EXPORTED_DEF obj_t BGl_wclasszd2idzd2zzobject_classz00(BgL_typez00_bglt
		BgL_oz00_243)
	{
		{	/* Object/class.sch 277 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_243)))->BgL_idz00);
		}

	}



/* &wclass-id */
	obj_t BGl_z62wclasszd2idzb0zzobject_classz00(obj_t BgL_envz00_3148,
		obj_t BgL_oz00_3149)
	{
		{	/* Object/class.sch 277 */
			return
				BGl_wclasszd2idzd2zzobject_classz00(((BgL_typez00_bglt) BgL_oz00_3149));
		}

	}



/* get-class-list */
	BGL_EXPORTED_DEF obj_t BGl_getzd2classzd2listz00zzobject_classz00(void)
	{
		{	/* Object/class.scm 96 */
			return BGl_za2classzd2typezd2listza2z00zzobject_classz00;
		}

	}



/* &get-class-list */
	obj_t BGl_z62getzd2classzd2listz62zzobject_classz00(obj_t BgL_envz00_3150)
	{
		{	/* Object/class.scm 96 */
			return BGl_getzd2classzd2listz00zzobject_classz00();
		}

	}



/* heap-add-class! */
	BGL_EXPORTED_DEF obj_t
		BGl_heapzd2addzd2classz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_typez00_246)
	{
		{	/* Object/class.scm 105 */
			return (BGl_za2classzd2typezd2listza2z00zzobject_classz00 =
				MAKE_YOUNG_PAIR(
					((obj_t) BgL_typez00_246),
					BGl_za2classzd2typezd2listza2z00zzobject_classz00), BUNSPEC);
		}

	}



/* &heap-add-class! */
	obj_t BGl_z62heapzd2addzd2classz12z70zzobject_classz00(obj_t BgL_envz00_3151,
		obj_t BgL_typez00_3152)
	{
		{	/* Object/class.scm 105 */
			return
				BGl_heapzd2addzd2classz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_typez00_3152));
		}

	}



/* wide-chunk-class-id */
	BGL_EXPORTED_DEF obj_t BGl_widezd2chunkzd2classzd2idzd2zzobject_classz00(obj_t
		BgL_classzd2idzd2_247)
	{
		{	/* Object/class.scm 115 */
			{	/* Object/class.scm 116 */
				obj_t BgL_arg1268z00_1620;

				{	/* Object/class.scm 116 */
					obj_t BgL_arg1272z00_1621;

					{	/* Object/class.scm 116 */
						obj_t BgL_arg1455z00_1623;

						BgL_arg1455z00_1623 = SYMBOL_TO_STRING(BgL_classzd2idzd2_247);
						BgL_arg1272z00_1621 =
							BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_1623);
					}
					BgL_arg1268z00_1620 =
						string_append(BGl_string1961z00zzobject_classz00,
						BgL_arg1272z00_1621);
				}
				return bstring_to_symbol(BgL_arg1268z00_1620);
			}
		}

	}



/* &wide-chunk-class-id */
	obj_t BGl_z62widezd2chunkzd2classzd2idzb0zzobject_classz00(obj_t
		BgL_envz00_3153, obj_t BgL_classzd2idzd2_3154)
	{
		{	/* Object/class.scm 115 */
			return
				BGl_widezd2chunkzd2classzd2idzd2zzobject_classz00
				(BgL_classzd2idzd2_3154);
		}

	}



/* type-class-name */
	BGL_EXPORTED_DEF obj_t
		BGl_typezd2classzd2namez00zzobject_classz00(BgL_typez00_bglt
		BgL_classz00_248)
	{
		{	/* Object/class.scm 121 */
			{	/* Object/class.scm 123 */
				bool_t BgL_test2260z00_4620;

				{	/* Object/class.scm 123 */
					obj_t BgL_classz00_1625;

					BgL_classz00_1625 = BGl_tclassz00zzobject_classz00;
					{	/* Object/class.scm 123 */
						BgL_objectz00_bglt BgL_arg1807z00_1627;

						{	/* Object/class.scm 123 */
							obj_t BgL_tmpz00_4621;

							BgL_tmpz00_4621 = ((obj_t) BgL_classz00_248);
							BgL_arg1807z00_1627 = (BgL_objectz00_bglt) (BgL_tmpz00_4621);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Object/class.scm 123 */
								long BgL_idxz00_1633;

								BgL_idxz00_1633 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1627);
								BgL_test2260z00_4620 =
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_1633 + 2L)) == BgL_classz00_1625);
							}
						else
							{	/* Object/class.scm 123 */
								bool_t BgL_res1907z00_1658;

								{	/* Object/class.scm 123 */
									obj_t BgL_oclassz00_1641;

									{	/* Object/class.scm 123 */
										obj_t BgL_arg1815z00_1649;
										long BgL_arg1816z00_1650;

										BgL_arg1815z00_1649 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Object/class.scm 123 */
											long BgL_arg1817z00_1651;

											BgL_arg1817z00_1651 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1627);
											BgL_arg1816z00_1650 = (BgL_arg1817z00_1651 - OBJECT_TYPE);
										}
										BgL_oclassz00_1641 =
											VECTOR_REF(BgL_arg1815z00_1649, BgL_arg1816z00_1650);
									}
									{	/* Object/class.scm 123 */
										bool_t BgL__ortest_1115z00_1642;

										BgL__ortest_1115z00_1642 =
											(BgL_classz00_1625 == BgL_oclassz00_1641);
										if (BgL__ortest_1115z00_1642)
											{	/* Object/class.scm 123 */
												BgL_res1907z00_1658 = BgL__ortest_1115z00_1642;
											}
										else
											{	/* Object/class.scm 123 */
												long BgL_odepthz00_1643;

												{	/* Object/class.scm 123 */
													obj_t BgL_arg1804z00_1644;

													BgL_arg1804z00_1644 = (BgL_oclassz00_1641);
													BgL_odepthz00_1643 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_1644);
												}
												if ((2L < BgL_odepthz00_1643))
													{	/* Object/class.scm 123 */
														obj_t BgL_arg1802z00_1646;

														{	/* Object/class.scm 123 */
															obj_t BgL_arg1803z00_1647;

															BgL_arg1803z00_1647 = (BgL_oclassz00_1641);
															BgL_arg1802z00_1646 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_1647,
																2L);
														}
														BgL_res1907z00_1658 =
															(BgL_arg1802z00_1646 == BgL_classz00_1625);
													}
												else
													{	/* Object/class.scm 123 */
														BgL_res1907z00_1658 = ((bool_t) 0);
													}
											}
									}
								}
								BgL_test2260z00_4620 = BgL_res1907z00_1658;
							}
					}
				}
				if (BgL_test2260z00_4620)
					{	/* Object/class.scm 125 */
						bool_t BgL_test2264z00_4643;

						{	/* Object/class.scm 125 */
							bool_t BgL_test2265z00_4644;

							{	/* Object/class.scm 125 */
								obj_t BgL_tmpz00_4645;

								{
									BgL_tclassz00_bglt BgL_auxz00_4646;

									{
										obj_t BgL_auxz00_4647;

										{	/* Object/class.scm 125 */
											BgL_objectz00_bglt BgL_tmpz00_4648;

											BgL_tmpz00_4648 =
												((BgL_objectz00_bglt)
												((BgL_typez00_bglt) BgL_classz00_248));
											BgL_auxz00_4647 = BGL_OBJECT_WIDENING(BgL_tmpz00_4648);
										}
										BgL_auxz00_4646 = ((BgL_tclassz00_bglt) BgL_auxz00_4647);
									}
									BgL_tmpz00_4645 =
										(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_4646))->
										BgL_wideningz00);
								}
								BgL_test2265z00_4644 = CBOOL(BgL_tmpz00_4645);
							}
							if (BgL_test2265z00_4644)
								{	/* Object/class.scm 125 */
									BgL_test2264z00_4643 =
										CBOOL(BGl_za2sawza2z00zzengine_paramz00);
								}
							else
								{	/* Object/class.scm 125 */
									BgL_test2264z00_4643 = ((bool_t) 0);
								}
						}
						if (BgL_test2264z00_4643)
							{	/* Object/class.scm 126 */
								obj_t BgL_arg1284z00_846;

								{
									BgL_tclassz00_bglt BgL_auxz00_4656;

									{
										obj_t BgL_auxz00_4657;

										{	/* Object/class.scm 126 */
											BgL_objectz00_bglt BgL_tmpz00_4658;

											BgL_tmpz00_4658 =
												((BgL_objectz00_bglt)
												((BgL_typez00_bglt) BgL_classz00_248));
											BgL_auxz00_4657 = BGL_OBJECT_WIDENING(BgL_tmpz00_4658);
										}
										BgL_auxz00_4656 = ((BgL_tclassz00_bglt) BgL_auxz00_4657);
									}
									BgL_arg1284z00_846 =
										(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_4656))->
										BgL_widezd2typezd2);
								}
								return
									(((BgL_typez00_bglt) COBJECT(
											((BgL_typez00_bglt) BgL_arg1284z00_846)))->BgL_namez00);
							}
						else
							{	/* Object/class.scm 125 */
								return
									(((BgL_typez00_bglt) COBJECT(BgL_classz00_248))->BgL_namez00);
							}
					}
				else
					{	/* Object/class.scm 123 */
						return
							(((BgL_typez00_bglt) COBJECT(BgL_classz00_248))->BgL_namez00);
					}
			}
		}

	}



/* &type-class-name */
	obj_t BGl_z62typezd2classzd2namez62zzobject_classz00(obj_t BgL_envz00_3155,
		obj_t BgL_classz00_3156)
	{
		{	/* Object/class.scm 121 */
			return
				BGl_typezd2classzd2namez00zzobject_classz00(
				((BgL_typez00_bglt) BgL_classz00_3156));
		}

	}



/* declare-class-type! */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_declarezd2classzd2typez12z12zzobject_classz00(obj_t
		BgL_classzd2defzd2_249, BgL_globalz00_bglt BgL_classzd2holderzd2_250,
		obj_t BgL_wideningz00_251, bool_t BgL_finalzf3zf3_252,
		bool_t BgL_abstractzf3zf3_253, obj_t BgL_srcz00_254)
	{
		{	/* Object/class.scm 141 */
			{	/* Object/class.scm 142 */
				obj_t BgL_classzd2identzd2_848;

				{	/* Object/class.scm 142 */
					obj_t BgL_arg1318z00_878;
					obj_t BgL_arg1319z00_879;

					BgL_arg1318z00_878 = CAR(((obj_t) BgL_classzd2defzd2_249));
					BgL_arg1319z00_879 =
						BGl_findzd2locationzd2zztools_locationz00(BgL_srcz00_254);
					BgL_classzd2identzd2_848 =
						BGl_parsezd2idzd2zzast_identz00(BgL_arg1318z00_878,
						BgL_arg1319z00_879);
				}
				{	/* Object/class.scm 142 */
					obj_t BgL_classzd2idzd2_849;

					BgL_classzd2idzd2_849 = CAR(BgL_classzd2identzd2_848);
					{	/* Object/class.scm 143 */
						obj_t BgL_superz00_850;

						{	/* Object/class.scm 144 */
							obj_t BgL_superz00_874;

							BgL_superz00_874 = CDR(BgL_classzd2identzd2_848);
							if (
								((((BgL_typez00_bglt) COBJECT(
												((BgL_typez00_bglt) BgL_superz00_874)))->BgL_idz00) ==
									BgL_classzd2idzd2_849))
								{	/* Object/class.scm 146 */
									BgL_superz00_850 = BFALSE;
								}
							else
								{	/* Object/class.scm 146 */
									if ((BgL_superz00_874 == BGl_za2_za2z00zztype_cachez00))
										{	/* Object/class.scm 148 */
											BgL_superz00_850 =
												BGl_getzd2objectzd2typez00zztype_cachez00();
										}
									else
										{	/* Object/class.scm 148 */
											BgL_superz00_850 = BgL_superz00_874;
										}
								}
						}
						{	/* Object/class.scm 144 */
							obj_t BgL_namez00_851;

							BgL_namez00_851 =
								BGl_idzd2ze3namez31zzast_identz00(BgL_classzd2idzd2_849);
							{	/* Object/class.scm 152 */
								obj_t BgL_siza7eofza7_852;

								BgL_siza7eofza7_852 =
									string_append_3(BGl_string1962z00zzobject_classz00,
									BgL_namez00_851, BGl_string1963z00zzobject_classz00);
								{	/* Object/class.scm 153 */
									obj_t BgL_tzd2namezd2_853;

									BgL_tzd2namezd2_853 =
										string_append(BgL_namez00_851,
										BGl_string1964z00zzobject_classz00);
									{	/* Object/class.scm 154 */
										BgL_typez00_bglt BgL_typez00_854;

										BgL_typez00_854 =
											BGl_declarezd2typez12zc0zztype_envz00
											(BgL_classzd2idzd2_849, BgL_tzd2namezd2_853,
											CNST_TABLE_REF(0));
										{	/* Object/class.scm 155 */

											BGl_globalzd2setzd2readzd2onlyz12zc0zzast_varz00
												(BgL_classzd2holderzd2_250);
											((((BgL_globalz00_bglt)
														COBJECT(BgL_classzd2holderzd2_250))->
													BgL_evaluablezf3zf3) =
												((bool_t) ((bool_t) 1)), BUNSPEC);
											{	/* Object/class.scm 159 */
												obj_t BgL_arg1304z00_855;

												BgL_arg1304z00_855 =
													BGl_getzd2classzd2typez00zztype_cachez00();
												((((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	BgL_classzd2holderzd2_250)))->BgL_typez00) =
													((BgL_typez00_bglt) ((BgL_typez00_bglt)
															BgL_arg1304z00_855)), BUNSPEC);
											}
											{	/* Object/class.scm 163 */
												BgL_tclassz00_bglt BgL_wide1159z00_858;

												BgL_wide1159z00_858 =
													((BgL_tclassz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																BgL_tclassz00_bgl))));
												{	/* Object/class.scm 163 */
													obj_t BgL_auxz00_4698;
													BgL_objectz00_bglt BgL_tmpz00_4695;

													BgL_auxz00_4698 = ((obj_t) BgL_wide1159z00_858);
													BgL_tmpz00_4695 =
														((BgL_objectz00_bglt)
														((BgL_typez00_bglt) BgL_typez00_854));
													BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4695,
														BgL_auxz00_4698);
												}
												((BgL_objectz00_bglt)
													((BgL_typez00_bglt) BgL_typez00_854));
												{	/* Object/class.scm 163 */
													long BgL_arg1305z00_859;

													BgL_arg1305z00_859 =
														BGL_CLASS_NUM(BGl_tclassz00zzobject_classz00);
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt)
															((BgL_typez00_bglt) BgL_typez00_854)),
														BgL_arg1305z00_859);
												}
												((BgL_typez00_bglt)
													((BgL_typez00_bglt) BgL_typez00_854));
											}
											{
												BgL_tclassz00_bglt BgL_auxz00_4709;

												{
													obj_t BgL_auxz00_4710;

													{	/* Object/class.scm 164 */
														BgL_objectz00_bglt BgL_tmpz00_4711;

														BgL_tmpz00_4711 =
															((BgL_objectz00_bglt)
															((BgL_typez00_bglt) BgL_typez00_854));
														BgL_auxz00_4710 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_4711);
													}
													BgL_auxz00_4709 =
														((BgL_tclassz00_bglt) BgL_auxz00_4710);
												}
												((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_4709))->
														BgL_itszd2superzd2) =
													((obj_t) BgL_superz00_850), BUNSPEC);
											}
											{
												BgL_tclassz00_bglt BgL_auxz00_4717;

												{
													obj_t BgL_auxz00_4718;

													{	/* Object/class.scm 56 */
														BgL_objectz00_bglt BgL_tmpz00_4719;

														BgL_tmpz00_4719 =
															((BgL_objectz00_bglt)
															((BgL_typez00_bglt) BgL_typez00_854));
														BgL_auxz00_4718 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_4719);
													}
													BgL_auxz00_4717 =
														((BgL_tclassz00_bglt) BgL_auxz00_4718);
												}
												((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_4717))->
														BgL_slotsz00) = ((obj_t) BUNSPEC), BUNSPEC);
											}
											{
												BgL_tclassz00_bglt BgL_auxz00_4725;

												{
													obj_t BgL_auxz00_4726;

													{	/* Object/class.scm 166 */
														BgL_objectz00_bglt BgL_tmpz00_4727;

														BgL_tmpz00_4727 =
															((BgL_objectz00_bglt)
															((BgL_typez00_bglt) BgL_typez00_854));
														BgL_auxz00_4726 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_4727);
													}
													BgL_auxz00_4725 =
														((BgL_tclassz00_bglt) BgL_auxz00_4726);
												}
												((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_4725))->
														BgL_holderz00) =
													((BgL_globalz00_bglt) BgL_classzd2holderzd2_250),
													BUNSPEC);
											}
											{
												BgL_tclassz00_bglt BgL_auxz00_4733;

												{
													obj_t BgL_auxz00_4734;

													{	/* Object/class.scm 167 */
														BgL_objectz00_bglt BgL_tmpz00_4735;

														BgL_tmpz00_4735 =
															((BgL_objectz00_bglt)
															((BgL_typez00_bglt) BgL_typez00_854));
														BgL_auxz00_4734 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_4735);
													}
													BgL_auxz00_4733 =
														((BgL_tclassz00_bglt) BgL_auxz00_4734);
												}
												((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_4733))->
														BgL_wideningz00) =
													((obj_t) BgL_wideningz00_251), BUNSPEC);
											}
											{
												BgL_tclassz00_bglt BgL_auxz00_4741;

												{
													obj_t BgL_auxz00_4742;

													{	/* Object/class.scm 165 */
														BgL_objectz00_bglt BgL_tmpz00_4743;

														BgL_tmpz00_4743 =
															((BgL_objectz00_bglt)
															((BgL_typez00_bglt) BgL_typez00_854));
														BgL_auxz00_4742 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_4743);
													}
													BgL_auxz00_4741 =
														((BgL_tclassz00_bglt) BgL_auxz00_4742);
												}
												((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_4741))->
														BgL_depthz00) = ((long) 0L), BUNSPEC);
											}
											{
												BgL_tclassz00_bglt BgL_auxz00_4749;

												{
													obj_t BgL_auxz00_4750;

													{	/* Object/class.scm 168 */
														BgL_objectz00_bglt BgL_tmpz00_4751;

														BgL_tmpz00_4751 =
															((BgL_objectz00_bglt)
															((BgL_typez00_bglt) BgL_typez00_854));
														BgL_auxz00_4750 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_4751);
													}
													BgL_auxz00_4749 =
														((BgL_tclassz00_bglt) BgL_auxz00_4750);
												}
												((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_4749))->
														BgL_finalzf3zf3) =
													((bool_t) BgL_finalzf3zf3_252), BUNSPEC);
											}
											{
												obj_t BgL_auxz00_4764;
												BgL_tclassz00_bglt BgL_auxz00_4757;

												{	/* Object/class.scm 170 */
													obj_t BgL_pairz00_1688;

													BgL_pairz00_1688 =
														CDR(((obj_t) BgL_classzd2defzd2_249));
													BgL_auxz00_4764 = CAR(BgL_pairz00_1688);
												}
												{
													obj_t BgL_auxz00_4758;

													{	/* Object/class.scm 170 */
														BgL_objectz00_bglt BgL_tmpz00_4759;

														BgL_tmpz00_4759 =
															((BgL_objectz00_bglt)
															((BgL_typez00_bglt) BgL_typez00_854));
														BgL_auxz00_4758 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_4759);
													}
													BgL_auxz00_4757 =
														((BgL_tclassz00_bglt) BgL_auxz00_4758);
												}
												((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_4757))->
														BgL_constructorz00) =
													((obj_t) BgL_auxz00_4764), BUNSPEC);
											}
											{
												BgL_tclassz00_bglt BgL_auxz00_4769;

												{
													obj_t BgL_auxz00_4770;

													{	/* Object/class.scm 56 */
														BgL_objectz00_bglt BgL_tmpz00_4771;

														BgL_tmpz00_4771 =
															((BgL_objectz00_bglt)
															((BgL_typez00_bglt) BgL_typez00_854));
														BgL_auxz00_4770 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_4771);
													}
													BgL_auxz00_4769 =
														((BgL_tclassz00_bglt) BgL_auxz00_4770);
												}
												((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_4769))->
														BgL_virtualzd2slotszd2numberz00) =
													((obj_t) BINT(0L)), BUNSPEC);
											}
											{
												BgL_tclassz00_bglt BgL_auxz00_4778;

												{
													obj_t BgL_auxz00_4779;

													{	/* Object/class.scm 169 */
														BgL_objectz00_bglt BgL_tmpz00_4780;

														BgL_tmpz00_4780 =
															((BgL_objectz00_bglt)
															((BgL_typez00_bglt) BgL_typez00_854));
														BgL_auxz00_4779 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_4780);
													}
													BgL_auxz00_4778 =
														((BgL_tclassz00_bglt) BgL_auxz00_4779);
												}
												((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_4778))->
														BgL_abstractzf3zf3) =
													((bool_t) BgL_abstractzf3zf3_253), BUNSPEC);
											}
											{
												BgL_tclassz00_bglt BgL_auxz00_4786;

												{
													obj_t BgL_auxz00_4787;

													{	/* Object/class.scm 56 */
														BgL_objectz00_bglt BgL_tmpz00_4788;

														BgL_tmpz00_4788 =
															((BgL_objectz00_bglt)
															((BgL_typez00_bglt) BgL_typez00_854));
														BgL_auxz00_4787 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_4788);
													}
													BgL_auxz00_4786 =
														((BgL_tclassz00_bglt) BgL_auxz00_4787);
												}
												((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_4786))->
														BgL_widezd2typezd2) = ((obj_t) BFALSE), BUNSPEC);
											}
											{
												BgL_tclassz00_bglt BgL_auxz00_4794;

												{
													obj_t BgL_auxz00_4795;

													{	/* Object/class.scm 56 */
														BgL_objectz00_bglt BgL_tmpz00_4796;

														BgL_tmpz00_4796 =
															((BgL_objectz00_bglt)
															((BgL_typez00_bglt) BgL_typez00_854));
														BgL_auxz00_4795 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_4796);
													}
													BgL_auxz00_4794 =
														((BgL_tclassz00_bglt) BgL_auxz00_4795);
												}
												((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_4794))->
														BgL_subclassesz00) = ((obj_t) BNIL), BUNSPEC);
											}
											((BgL_typez00_bglt) BgL_typez00_854);
											{	/* Object/class.scm 171 */
												bool_t BgL_test2268z00_4803;

												{	/* Object/class.scm 171 */
													obj_t BgL_classz00_1693;

													BgL_classz00_1693 = BGl_tclassz00zzobject_classz00;
													if (BGL_OBJECTP(BgL_superz00_850))
														{	/* Object/class.scm 171 */
															BgL_objectz00_bglt BgL_arg1807z00_1695;

															BgL_arg1807z00_1695 =
																(BgL_objectz00_bglt) (BgL_superz00_850);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Object/class.scm 171 */
																	long BgL_idxz00_1701;

																	BgL_idxz00_1701 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_1695);
																	BgL_test2268z00_4803 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_1701 + 2L)) ==
																		BgL_classz00_1693);
																}
															else
																{	/* Object/class.scm 171 */
																	bool_t BgL_res1908z00_1726;

																	{	/* Object/class.scm 171 */
																		obj_t BgL_oclassz00_1709;

																		{	/* Object/class.scm 171 */
																			obj_t BgL_arg1815z00_1717;
																			long BgL_arg1816z00_1718;

																			BgL_arg1815z00_1717 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Object/class.scm 171 */
																				long BgL_arg1817z00_1719;

																				BgL_arg1817z00_1719 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_1695);
																				BgL_arg1816z00_1718 =
																					(BgL_arg1817z00_1719 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_1709 =
																				VECTOR_REF(BgL_arg1815z00_1717,
																				BgL_arg1816z00_1718);
																		}
																		{	/* Object/class.scm 171 */
																			bool_t BgL__ortest_1115z00_1710;

																			BgL__ortest_1115z00_1710 =
																				(BgL_classz00_1693 ==
																				BgL_oclassz00_1709);
																			if (BgL__ortest_1115z00_1710)
																				{	/* Object/class.scm 171 */
																					BgL_res1908z00_1726 =
																						BgL__ortest_1115z00_1710;
																				}
																			else
																				{	/* Object/class.scm 171 */
																					long BgL_odepthz00_1711;

																					{	/* Object/class.scm 171 */
																						obj_t BgL_arg1804z00_1712;

																						BgL_arg1804z00_1712 =
																							(BgL_oclassz00_1709);
																						BgL_odepthz00_1711 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_1712);
																					}
																					if ((2L < BgL_odepthz00_1711))
																						{	/* Object/class.scm 171 */
																							obj_t BgL_arg1802z00_1714;

																							{	/* Object/class.scm 171 */
																								obj_t BgL_arg1803z00_1715;

																								BgL_arg1803z00_1715 =
																									(BgL_oclassz00_1709);
																								BgL_arg1802z00_1714 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_1715, 2L);
																							}
																							BgL_res1908z00_1726 =
																								(BgL_arg1802z00_1714 ==
																								BgL_classz00_1693);
																						}
																					else
																						{	/* Object/class.scm 171 */
																							BgL_res1908z00_1726 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2268z00_4803 = BgL_res1908z00_1726;
																}
														}
													else
														{	/* Object/class.scm 171 */
															BgL_test2268z00_4803 = ((bool_t) 0);
														}
												}
												if (BgL_test2268z00_4803)
													{
														obj_t BgL_auxz00_4833;
														BgL_tclassz00_bglt BgL_auxz00_4826;

														{	/* Object/class.scm 173 */
															obj_t BgL_arg1308z00_863;

															{
																BgL_tclassz00_bglt BgL_auxz00_4834;

																{
																	obj_t BgL_auxz00_4835;

																	{	/* Object/class.scm 173 */
																		BgL_objectz00_bglt BgL_tmpz00_4836;

																		BgL_tmpz00_4836 =
																			((BgL_objectz00_bglt)
																			((BgL_typez00_bglt) BgL_superz00_850));
																		BgL_auxz00_4835 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_4836);
																	}
																	BgL_auxz00_4834 =
																		((BgL_tclassz00_bglt) BgL_auxz00_4835);
																}
																BgL_arg1308z00_863 =
																	(((BgL_tclassz00_bglt)
																		COBJECT(BgL_auxz00_4834))->
																	BgL_subclassesz00);
															}
															BgL_auxz00_4833 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_typez00_854), BgL_arg1308z00_863);
														}
														{
															obj_t BgL_auxz00_4827;

															{	/* Object/class.scm 173 */
																BgL_objectz00_bglt BgL_tmpz00_4828;

																BgL_tmpz00_4828 =
																	((BgL_objectz00_bglt)
																	((BgL_typez00_bglt) BgL_superz00_850));
																BgL_auxz00_4827 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_4828);
															}
															BgL_auxz00_4826 =
																((BgL_tclassz00_bglt) BgL_auxz00_4827);
														}
														((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_4826))->
																BgL_subclassesz00) =
															((obj_t) BgL_auxz00_4833), BUNSPEC);
													}
												else
													{	/* Object/class.scm 171 */
														BFALSE;
													}
											}
											if ((BgL_wideningz00_251 == CNST_TABLE_REF(1)))
												{	/* Object/class.scm 178 */
													obj_t BgL_wtidz00_864;

													{	/* Object/class.scm 116 */
														obj_t BgL_arg1268z00_1730;

														{	/* Object/class.scm 116 */
															obj_t BgL_arg1272z00_1731;

															{	/* Object/class.scm 116 */
																obj_t BgL_arg1455z00_1733;

																BgL_arg1455z00_1733 =
																	SYMBOL_TO_STRING(
																	((obj_t) BgL_classzd2idzd2_849));
																BgL_arg1272z00_1731 =
																	BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																	(BgL_arg1455z00_1733);
															}
															BgL_arg1268z00_1730 =
																string_append
																(BGl_string1961z00zzobject_classz00,
																BgL_arg1272z00_1731);
														}
														BgL_wtidz00_864 =
															bstring_to_symbol(BgL_arg1268z00_1730);
													}
													{	/* Object/class.scm 178 */
														BgL_typez00_bglt BgL_wtz00_865;

														{	/* Object/class.scm 179 */
															BgL_typez00_bglt BgL_tmp1162z00_869;

															BgL_tmp1162z00_869 =
																BGl_declarezd2typez12zc0zztype_envz00
																(BgL_wtidz00_864, BgL_tzd2namezd2_853,
																CNST_TABLE_REF(0));
															{	/* Object/class.scm 179 */
																BgL_wclassz00_bglt BgL_wide1164z00_871;

																BgL_wide1164z00_871 =
																	((BgL_wclassz00_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_wclassz00_bgl))));
																{	/* Object/class.scm 179 */
																	obj_t BgL_auxz00_4859;
																	BgL_objectz00_bglt BgL_tmpz00_4856;

																	BgL_auxz00_4859 =
																		((obj_t) BgL_wide1164z00_871);
																	BgL_tmpz00_4856 =
																		((BgL_objectz00_bglt)
																		((BgL_typez00_bglt) BgL_tmp1162z00_869));
																	BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4856,
																		BgL_auxz00_4859);
																}
																((BgL_objectz00_bglt)
																	((BgL_typez00_bglt) BgL_tmp1162z00_869));
																{	/* Object/class.scm 179 */
																	long BgL_arg1314z00_872;

																	BgL_arg1314z00_872 =
																		BGL_CLASS_NUM
																		(BGl_wclassz00zzobject_classz00);
																	BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																			((BgL_typez00_bglt) BgL_tmp1162z00_869)),
																		BgL_arg1314z00_872);
																}
																((BgL_typez00_bglt)
																	((BgL_typez00_bglt) BgL_tmp1162z00_869));
															}
															{
																BgL_wclassz00_bglt BgL_auxz00_4870;

																{
																	obj_t BgL_auxz00_4871;

																	{	/* Object/class.scm 180 */
																		BgL_objectz00_bglt BgL_tmpz00_4872;

																		BgL_tmpz00_4872 =
																			((BgL_objectz00_bglt)
																			((BgL_typez00_bglt) BgL_tmp1162z00_869));
																		BgL_auxz00_4871 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_4872);
																	}
																	BgL_auxz00_4870 =
																		((BgL_wclassz00_bglt) BgL_auxz00_4871);
																}
																((((BgL_wclassz00_bglt)
																			COBJECT(BgL_auxz00_4870))->
																		BgL_itszd2classzd2) =
																	((obj_t) ((obj_t) BgL_typez00_854)), BUNSPEC);
															}
															BgL_wtz00_865 =
																((BgL_typez00_bglt) BgL_tmp1162z00_869);
														}
														{	/* Object/class.scm 179 */

															((((BgL_typez00_bglt) COBJECT(
																			((BgL_typez00_bglt) BgL_wtz00_865)))->
																	BgL_siza7eza7) =
																((obj_t) BgL_siza7eofza7_852), BUNSPEC);
															{
																BgL_tclassz00_bglt BgL_auxz00_4882;

																{
																	obj_t BgL_auxz00_4883;

																	{	/* Object/class.scm 182 */
																		BgL_objectz00_bglt BgL_tmpz00_4884;

																		BgL_tmpz00_4884 =
																			((BgL_objectz00_bglt)
																			((BgL_typez00_bglt) BgL_typez00_854));
																		BgL_auxz00_4883 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_4884);
																	}
																	BgL_auxz00_4882 =
																		((BgL_tclassz00_bglt) BgL_auxz00_4883);
																}
																((((BgL_tclassz00_bglt)
																			COBJECT(BgL_auxz00_4882))->
																		BgL_widezd2typezd2) =
																	((obj_t) ((obj_t) BgL_wtz00_865)), BUNSPEC);
															}
															{	/* Object/class.scm 183 */
																obj_t BgL_arg1310z00_866;

																BgL_arg1310z00_866 =
																	(((BgL_typez00_bglt) COBJECT(
																			((BgL_typez00_bglt) BgL_superz00_850)))->
																	BgL_namez00);
																((((BgL_typez00_bglt)
																			COBJECT(BgL_typez00_854))->BgL_namez00) =
																	((obj_t) BgL_arg1310z00_866), BUNSPEC);
															}
															{	/* Object/class.scm 184 */
																obj_t BgL_arg1311z00_867;

																BgL_arg1311z00_867 =
																	(((BgL_typez00_bglt) COBJECT(
																			((BgL_typez00_bglt) BgL_superz00_850)))->
																	BgL_siza7eza7);
																((((BgL_typez00_bglt)
																			COBJECT(BgL_typez00_854))->
																		BgL_siza7eza7) =
																	((obj_t) BgL_arg1311z00_867), BUNSPEC);
															}
															{	/* Object/class.scm 185 */
																obj_t BgL_list1312z00_868;

																BgL_list1312z00_868 =
																	MAKE_YOUNG_PAIR(BFALSE, BNIL);
																BGl_genzd2coercionzd2clausez12z12zzobject_coercionz00
																	(BgL_typez00_854, BgL_wtidz00_864,
																	BgL_superz00_850, BgL_list1312z00_868);
															}
															BGl_genzd2classzd2coercersz12z12zzobject_coercionz00
																(((obj_t) BgL_wtz00_865), BgL_superz00_850);
												}}}
											else
												{	/* Object/class.scm 177 */
													((((BgL_typez00_bglt) COBJECT(BgL_typez00_854))->
															BgL_siza7eza7) =
														((obj_t) BgL_siza7eofza7_852), BUNSPEC);
												}
											BGl_za2classzd2typezd2listza2z00zzobject_classz00 =
												MAKE_YOUNG_PAIR(
												((obj_t) BgL_typez00_854),
												BGl_za2classzd2typezd2listza2z00zzobject_classz00);
											return BgL_typez00_854;
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &declare-class-type! */
	BgL_typez00_bglt BGl_z62declarezd2classzd2typez12z70zzobject_classz00(obj_t
		BgL_envz00_3157, obj_t BgL_classzd2defzd2_3158,
		obj_t BgL_classzd2holderzd2_3159, obj_t BgL_wideningz00_3160,
		obj_t BgL_finalzf3zf3_3161, obj_t BgL_abstractzf3zf3_3162,
		obj_t BgL_srcz00_3163)
	{
		{	/* Object/class.scm 141 */
			return
				BGl_declarezd2classzd2typez12z12zzobject_classz00
				(BgL_classzd2defzd2_3158,
				((BgL_globalz00_bglt) BgL_classzd2holderzd2_3159), BgL_wideningz00_3160,
				CBOOL(BgL_finalzf3zf3_3161), CBOOL(BgL_abstractzf3zf3_3162),
				BgL_srcz00_3163);
		}

	}



/* declare-java-class-type! */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_declarezd2javazd2classzd2typez12zc0zzobject_classz00(obj_t
		BgL_classzd2idzd2_255, obj_t BgL_superz00_256, obj_t BgL_jnamez00_257,
		obj_t BgL_packagez00_258, obj_t BgL_srcz00_259)
	{
		{	/* Object/class.scm 199 */
			{	/* Object/class.scm 200 */
				obj_t BgL_superz00_880;
				BgL_typez00_bglt BgL_typez00_881;

				if (
					((((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt) BgL_superz00_256)))->BgL_idz00) ==
						BgL_classzd2idzd2_255))
					{	/* Object/class.scm 201 */
						BgL_superz00_880 = BFALSE;
					}
				else
					{	/* Object/class.scm 201 */
						if ((BgL_superz00_256 == BGl_za2_za2z00zztype_cachez00))
							{	/* Object/class.scm 203 */
								BgL_superz00_880 = BFALSE;
							}
						else
							{	/* Object/class.scm 203 */
								BgL_superz00_880 = BgL_superz00_256;
							}
					}
				BgL_typez00_881 =
					BGl_declarezd2typez12zc0zztype_envz00(BgL_classzd2idzd2_255,
					BgL_jnamez00_257, CNST_TABLE_REF(2));
				{	/* Object/class.scm 211 */
					BgL_jclassz00_bglt BgL_wide1168z00_884;

					BgL_wide1168z00_884 =
						((BgL_jclassz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_jclassz00_bgl))));
					{	/* Object/class.scm 211 */
						obj_t BgL_auxz00_4920;
						BgL_objectz00_bglt BgL_tmpz00_4917;

						BgL_auxz00_4920 = ((obj_t) BgL_wide1168z00_884);
						BgL_tmpz00_4917 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_typez00_881));
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4917, BgL_auxz00_4920);
					}
					((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_typez00_881));
					{	/* Object/class.scm 211 */
						long BgL_arg1320z00_885;

						BgL_arg1320z00_885 = BGL_CLASS_NUM(BGl_jclassz00zzobject_classz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt)
								((BgL_typez00_bglt) BgL_typez00_881)), BgL_arg1320z00_885);
					}
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_typez00_881));
				}
				{
					BgL_jclassz00_bglt BgL_auxz00_4931;

					{
						obj_t BgL_auxz00_4932;

						{	/* Object/class.scm 212 */
							BgL_objectz00_bglt BgL_tmpz00_4933;

							BgL_tmpz00_4933 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_typez00_881));
							BgL_auxz00_4932 = BGL_OBJECT_WIDENING(BgL_tmpz00_4933);
						}
						BgL_auxz00_4931 = ((BgL_jclassz00_bglt) BgL_auxz00_4932);
					}
					((((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_4931))->
							BgL_itszd2superzd2) = ((obj_t) BgL_superz00_880), BUNSPEC);
				}
				{
					BgL_jclassz00_bglt BgL_auxz00_4939;

					{
						obj_t BgL_auxz00_4940;

						{	/* Object/class.scm 213 */
							BgL_objectz00_bglt BgL_tmpz00_4941;

							BgL_tmpz00_4941 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_typez00_881));
							BgL_auxz00_4940 = BGL_OBJECT_WIDENING(BgL_tmpz00_4941);
						}
						BgL_auxz00_4939 = ((BgL_jclassz00_bglt) BgL_auxz00_4940);
					}
					((((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_4939))->BgL_slotsz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_jclassz00_bglt BgL_auxz00_4947;

					{
						obj_t BgL_auxz00_4948;

						{	/* Object/class.scm 213 */
							BgL_objectz00_bglt BgL_tmpz00_4949;

							BgL_tmpz00_4949 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_typez00_881));
							BgL_auxz00_4948 = BGL_OBJECT_WIDENING(BgL_tmpz00_4949);
						}
						BgL_auxz00_4947 = ((BgL_jclassz00_bglt) BgL_auxz00_4948);
					}
					((((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_4947))->BgL_packagez00) =
						((obj_t) BgL_packagez00_258), BUNSPEC);
				}
				((BgL_typez00_bglt) BgL_typez00_881);
				return BgL_typez00_881;
			}
		}

	}



/* &declare-java-class-type! */
	BgL_typez00_bglt
		BGl_z62declarezd2javazd2classzd2typez12za2zzobject_classz00(obj_t
		BgL_envz00_3164, obj_t BgL_classzd2idzd2_3165, obj_t BgL_superz00_3166,
		obj_t BgL_jnamez00_3167, obj_t BgL_packagez00_3168, obj_t BgL_srcz00_3169)
	{
		{	/* Object/class.scm 199 */
			return
				BGl_declarezd2javazd2classzd2typez12zc0zzobject_classz00
				(BgL_classzd2idzd2_3165, BgL_superz00_3166, BgL_jnamez00_3167,
				BgL_packagez00_3168, BgL_srcz00_3169);
		}

	}



/* final-class? */
	BGL_EXPORTED_DEF bool_t BGl_finalzd2classzf3z21zzobject_classz00(obj_t
		BgL_classz00_260)
	{
		{	/* Object/class.scm 222 */
			{	/* Object/class.scm 223 */
				bool_t BgL_test2276z00_4957;

				{	/* Object/class.scm 223 */
					obj_t BgL_classz00_1757;

					BgL_classz00_1757 = BGl_tclassz00zzobject_classz00;
					if (BGL_OBJECTP(BgL_classz00_260))
						{	/* Object/class.scm 223 */
							BgL_objectz00_bglt BgL_arg1807z00_1759;

							BgL_arg1807z00_1759 = (BgL_objectz00_bglt) (BgL_classz00_260);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Object/class.scm 223 */
									long BgL_idxz00_1765;

									BgL_idxz00_1765 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1759);
									BgL_test2276z00_4957 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_1765 + 2L)) == BgL_classz00_1757);
								}
							else
								{	/* Object/class.scm 223 */
									bool_t BgL_res1909z00_1790;

									{	/* Object/class.scm 223 */
										obj_t BgL_oclassz00_1773;

										{	/* Object/class.scm 223 */
											obj_t BgL_arg1815z00_1781;
											long BgL_arg1816z00_1782;

											BgL_arg1815z00_1781 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Object/class.scm 223 */
												long BgL_arg1817z00_1783;

												BgL_arg1817z00_1783 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1759);
												BgL_arg1816z00_1782 =
													(BgL_arg1817z00_1783 - OBJECT_TYPE);
											}
											BgL_oclassz00_1773 =
												VECTOR_REF(BgL_arg1815z00_1781, BgL_arg1816z00_1782);
										}
										{	/* Object/class.scm 223 */
											bool_t BgL__ortest_1115z00_1774;

											BgL__ortest_1115z00_1774 =
												(BgL_classz00_1757 == BgL_oclassz00_1773);
											if (BgL__ortest_1115z00_1774)
												{	/* Object/class.scm 223 */
													BgL_res1909z00_1790 = BgL__ortest_1115z00_1774;
												}
											else
												{	/* Object/class.scm 223 */
													long BgL_odepthz00_1775;

													{	/* Object/class.scm 223 */
														obj_t BgL_arg1804z00_1776;

														BgL_arg1804z00_1776 = (BgL_oclassz00_1773);
														BgL_odepthz00_1775 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_1776);
													}
													if ((2L < BgL_odepthz00_1775))
														{	/* Object/class.scm 223 */
															obj_t BgL_arg1802z00_1778;

															{	/* Object/class.scm 223 */
																obj_t BgL_arg1803z00_1779;

																BgL_arg1803z00_1779 = (BgL_oclassz00_1773);
																BgL_arg1802z00_1778 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_1779,
																	2L);
															}
															BgL_res1909z00_1790 =
																(BgL_arg1802z00_1778 == BgL_classz00_1757);
														}
													else
														{	/* Object/class.scm 223 */
															BgL_res1909z00_1790 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2276z00_4957 = BgL_res1909z00_1790;
								}
						}
					else
						{	/* Object/class.scm 223 */
							BgL_test2276z00_4957 = ((bool_t) 0);
						}
				}
				if (BgL_test2276z00_4957)
					{
						BgL_tclassz00_bglt BgL_auxz00_4980;

						{
							obj_t BgL_auxz00_4981;

							{	/* Object/class.scm 223 */
								BgL_objectz00_bglt BgL_tmpz00_4982;

								BgL_tmpz00_4982 =
									((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_classz00_260));
								BgL_auxz00_4981 = BGL_OBJECT_WIDENING(BgL_tmpz00_4982);
							}
							BgL_auxz00_4980 = ((BgL_tclassz00_bglt) BgL_auxz00_4981);
						}
						return
							(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_4980))->
							BgL_finalzf3zf3);
					}
				else
					{	/* Object/class.scm 223 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &final-class? */
	obj_t BGl_z62finalzd2classzf3z43zzobject_classz00(obj_t BgL_envz00_3170,
		obj_t BgL_classz00_3171)
	{
		{	/* Object/class.scm 222 */
			return BBOOL(BGl_finalzd2classzf3z21zzobject_classz00(BgL_classz00_3171));
		}

	}



/* wide-class? */
	BGL_EXPORTED_DEF bool_t BGl_widezd2classzf3z21zzobject_classz00(obj_t
		BgL_classz00_261)
	{
		{	/* Object/class.scm 230 */
			{	/* Object/class.scm 231 */
				bool_t BgL_test2281z00_4990;

				{	/* Object/class.scm 231 */
					obj_t BgL_classz00_1794;

					BgL_classz00_1794 = BGl_tclassz00zzobject_classz00;
					if (BGL_OBJECTP(BgL_classz00_261))
						{	/* Object/class.scm 231 */
							BgL_objectz00_bglt BgL_arg1807z00_1796;

							BgL_arg1807z00_1796 = (BgL_objectz00_bglt) (BgL_classz00_261);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Object/class.scm 231 */
									long BgL_idxz00_1802;

									BgL_idxz00_1802 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1796);
									BgL_test2281z00_4990 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_1802 + 2L)) == BgL_classz00_1794);
								}
							else
								{	/* Object/class.scm 231 */
									bool_t BgL_res1910z00_1827;

									{	/* Object/class.scm 231 */
										obj_t BgL_oclassz00_1810;

										{	/* Object/class.scm 231 */
											obj_t BgL_arg1815z00_1818;
											long BgL_arg1816z00_1819;

											BgL_arg1815z00_1818 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Object/class.scm 231 */
												long BgL_arg1817z00_1820;

												BgL_arg1817z00_1820 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1796);
												BgL_arg1816z00_1819 =
													(BgL_arg1817z00_1820 - OBJECT_TYPE);
											}
											BgL_oclassz00_1810 =
												VECTOR_REF(BgL_arg1815z00_1818, BgL_arg1816z00_1819);
										}
										{	/* Object/class.scm 231 */
											bool_t BgL__ortest_1115z00_1811;

											BgL__ortest_1115z00_1811 =
												(BgL_classz00_1794 == BgL_oclassz00_1810);
											if (BgL__ortest_1115z00_1811)
												{	/* Object/class.scm 231 */
													BgL_res1910z00_1827 = BgL__ortest_1115z00_1811;
												}
											else
												{	/* Object/class.scm 231 */
													long BgL_odepthz00_1812;

													{	/* Object/class.scm 231 */
														obj_t BgL_arg1804z00_1813;

														BgL_arg1804z00_1813 = (BgL_oclassz00_1810);
														BgL_odepthz00_1812 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_1813);
													}
													if ((2L < BgL_odepthz00_1812))
														{	/* Object/class.scm 231 */
															obj_t BgL_arg1802z00_1815;

															{	/* Object/class.scm 231 */
																obj_t BgL_arg1803z00_1816;

																BgL_arg1803z00_1816 = (BgL_oclassz00_1810);
																BgL_arg1802z00_1815 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_1816,
																	2L);
															}
															BgL_res1910z00_1827 =
																(BgL_arg1802z00_1815 == BgL_classz00_1794);
														}
													else
														{	/* Object/class.scm 231 */
															BgL_res1910z00_1827 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2281z00_4990 = BgL_res1910z00_1827;
								}
						}
					else
						{	/* Object/class.scm 231 */
							BgL_test2281z00_4990 = ((bool_t) 0);
						}
				}
				if (BgL_test2281z00_4990)
					{	/* Object/class.scm 231 */
						obj_t BgL_tmpz00_5013;

						{
							BgL_tclassz00_bglt BgL_auxz00_5014;

							{
								obj_t BgL_auxz00_5015;

								{	/* Object/class.scm 231 */
									BgL_objectz00_bglt BgL_tmpz00_5016;

									BgL_tmpz00_5016 =
										((BgL_objectz00_bglt)
										((BgL_typez00_bglt) BgL_classz00_261));
									BgL_auxz00_5015 = BGL_OBJECT_WIDENING(BgL_tmpz00_5016);
								}
								BgL_auxz00_5014 = ((BgL_tclassz00_bglt) BgL_auxz00_5015);
							}
							BgL_tmpz00_5013 =
								(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_5014))->
								BgL_wideningz00);
						}
						return CBOOL(BgL_tmpz00_5013);
					}
				else
					{	/* Object/class.scm 231 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &wide-class? */
	obj_t BGl_z62widezd2classzf3z43zzobject_classz00(obj_t BgL_envz00_3172,
		obj_t BgL_classz00_3173)
	{
		{	/* Object/class.scm 230 */
			return BBOOL(BGl_widezd2classzf3z21zzobject_classz00(BgL_classz00_3173));
		}

	}



/* type-subclass? */
	BGL_EXPORTED_DEF bool_t
		BGl_typezd2subclasszf3z21zzobject_classz00(BgL_typez00_bglt
		BgL_subclassz00_262, BgL_typez00_bglt BgL_classz00_263)
	{
		{	/* Object/class.scm 236 */
			{	/* Object/class.scm 238 */
				bool_t BgL_test2286z00_5025;

				{	/* Object/class.scm 238 */
					bool_t BgL_test2287z00_5026;

					{	/* Object/class.scm 238 */
						obj_t BgL_classz00_1830;

						BgL_classz00_1830 = BGl_tclassz00zzobject_classz00;
						{	/* Object/class.scm 238 */
							BgL_objectz00_bglt BgL_arg1807z00_1832;

							{	/* Object/class.scm 238 */
								obj_t BgL_tmpz00_5027;

								BgL_tmpz00_5027 = ((obj_t) BgL_classz00_263);
								BgL_arg1807z00_1832 = (BgL_objectz00_bglt) (BgL_tmpz00_5027);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Object/class.scm 238 */
									long BgL_idxz00_1838;

									BgL_idxz00_1838 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1832);
									BgL_test2287z00_5026 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_1838 + 2L)) == BgL_classz00_1830);
								}
							else
								{	/* Object/class.scm 238 */
									bool_t BgL_res1911z00_1863;

									{	/* Object/class.scm 238 */
										obj_t BgL_oclassz00_1846;

										{	/* Object/class.scm 238 */
											obj_t BgL_arg1815z00_1854;
											long BgL_arg1816z00_1855;

											BgL_arg1815z00_1854 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Object/class.scm 238 */
												long BgL_arg1817z00_1856;

												BgL_arg1817z00_1856 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1832);
												BgL_arg1816z00_1855 =
													(BgL_arg1817z00_1856 - OBJECT_TYPE);
											}
											BgL_oclassz00_1846 =
												VECTOR_REF(BgL_arg1815z00_1854, BgL_arg1816z00_1855);
										}
										{	/* Object/class.scm 238 */
											bool_t BgL__ortest_1115z00_1847;

											BgL__ortest_1115z00_1847 =
												(BgL_classz00_1830 == BgL_oclassz00_1846);
											if (BgL__ortest_1115z00_1847)
												{	/* Object/class.scm 238 */
													BgL_res1911z00_1863 = BgL__ortest_1115z00_1847;
												}
											else
												{	/* Object/class.scm 238 */
													long BgL_odepthz00_1848;

													{	/* Object/class.scm 238 */
														obj_t BgL_arg1804z00_1849;

														BgL_arg1804z00_1849 = (BgL_oclassz00_1846);
														BgL_odepthz00_1848 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_1849);
													}
													if ((2L < BgL_odepthz00_1848))
														{	/* Object/class.scm 238 */
															obj_t BgL_arg1802z00_1851;

															{	/* Object/class.scm 238 */
																obj_t BgL_arg1803z00_1852;

																BgL_arg1803z00_1852 = (BgL_oclassz00_1846);
																BgL_arg1802z00_1851 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_1852,
																	2L);
															}
															BgL_res1911z00_1863 =
																(BgL_arg1802z00_1851 == BgL_classz00_1830);
														}
													else
														{	/* Object/class.scm 238 */
															BgL_res1911z00_1863 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2287z00_5026 = BgL_res1911z00_1863;
								}
						}
					}
					if (BgL_test2287z00_5026)
						{	/* Object/class.scm 238 */
							obj_t BgL_classz00_1864;

							BgL_classz00_1864 = BGl_tclassz00zzobject_classz00;
							{	/* Object/class.scm 238 */
								BgL_objectz00_bglt BgL_arg1807z00_1866;

								{	/* Object/class.scm 238 */
									obj_t BgL_tmpz00_5049;

									BgL_tmpz00_5049 = ((obj_t) BgL_subclassz00_262);
									BgL_arg1807z00_1866 = (BgL_objectz00_bglt) (BgL_tmpz00_5049);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Object/class.scm 238 */
										long BgL_idxz00_1872;

										BgL_idxz00_1872 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1866);
										BgL_test2286z00_5025 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_1872 + 2L)) == BgL_classz00_1864);
									}
								else
									{	/* Object/class.scm 238 */
										bool_t BgL_res1912z00_1897;

										{	/* Object/class.scm 238 */
											obj_t BgL_oclassz00_1880;

											{	/* Object/class.scm 238 */
												obj_t BgL_arg1815z00_1888;
												long BgL_arg1816z00_1889;

												BgL_arg1815z00_1888 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Object/class.scm 238 */
													long BgL_arg1817z00_1890;

													BgL_arg1817z00_1890 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1866);
													BgL_arg1816z00_1889 =
														(BgL_arg1817z00_1890 - OBJECT_TYPE);
												}
												BgL_oclassz00_1880 =
													VECTOR_REF(BgL_arg1815z00_1888, BgL_arg1816z00_1889);
											}
											{	/* Object/class.scm 238 */
												bool_t BgL__ortest_1115z00_1881;

												BgL__ortest_1115z00_1881 =
													(BgL_classz00_1864 == BgL_oclassz00_1880);
												if (BgL__ortest_1115z00_1881)
													{	/* Object/class.scm 238 */
														BgL_res1912z00_1897 = BgL__ortest_1115z00_1881;
													}
												else
													{	/* Object/class.scm 238 */
														long BgL_odepthz00_1882;

														{	/* Object/class.scm 238 */
															obj_t BgL_arg1804z00_1883;

															BgL_arg1804z00_1883 = (BgL_oclassz00_1880);
															BgL_odepthz00_1882 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_1883);
														}
														if ((2L < BgL_odepthz00_1882))
															{	/* Object/class.scm 238 */
																obj_t BgL_arg1802z00_1885;

																{	/* Object/class.scm 238 */
																	obj_t BgL_arg1803z00_1886;

																	BgL_arg1803z00_1886 = (BgL_oclassz00_1880);
																	BgL_arg1802z00_1885 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_1886,
																		2L);
																}
																BgL_res1912z00_1897 =
																	(BgL_arg1802z00_1885 == BgL_classz00_1864);
															}
														else
															{	/* Object/class.scm 238 */
																BgL_res1912z00_1897 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2286z00_5025 = BgL_res1912z00_1897;
									}
							}
						}
					else
						{	/* Object/class.scm 238 */
							BgL_test2286z00_5025 = ((bool_t) 0);
						}
				}
				if (BgL_test2286z00_5025)
					{
						obj_t BgL_subclassz00_895;

						BgL_subclassz00_895 = ((obj_t) BgL_subclassz00_262);
					BgL_zc3z04anonymousza31326ze3z87_896:
						if ((BgL_subclassz00_895 == ((obj_t) BgL_classz00_263)))
							{	/* Object/class.scm 241 */
								return ((bool_t) 1);
							}
						else
							{	/* Object/class.scm 243 */
								bool_t BgL_test2295z00_5074;

								{	/* Object/class.scm 243 */
									obj_t BgL_classz00_1898;

									BgL_classz00_1898 = BGl_tclassz00zzobject_classz00;
									if (BGL_OBJECTP(BgL_subclassz00_895))
										{	/* Object/class.scm 243 */
											BgL_objectz00_bglt BgL_arg1807z00_1900;

											BgL_arg1807z00_1900 =
												(BgL_objectz00_bglt) (BgL_subclassz00_895);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Object/class.scm 243 */
													long BgL_idxz00_1906;

													BgL_idxz00_1906 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1900);
													BgL_test2295z00_5074 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_1906 + 2L)) == BgL_classz00_1898);
												}
											else
												{	/* Object/class.scm 243 */
													bool_t BgL_res1913z00_1931;

													{	/* Object/class.scm 243 */
														obj_t BgL_oclassz00_1914;

														{	/* Object/class.scm 243 */
															obj_t BgL_arg1815z00_1922;
															long BgL_arg1816z00_1923;

															BgL_arg1815z00_1922 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Object/class.scm 243 */
																long BgL_arg1817z00_1924;

																BgL_arg1817z00_1924 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1900);
																BgL_arg1816z00_1923 =
																	(BgL_arg1817z00_1924 - OBJECT_TYPE);
															}
															BgL_oclassz00_1914 =
																VECTOR_REF(BgL_arg1815z00_1922,
																BgL_arg1816z00_1923);
														}
														{	/* Object/class.scm 243 */
															bool_t BgL__ortest_1115z00_1915;

															BgL__ortest_1115z00_1915 =
																(BgL_classz00_1898 == BgL_oclassz00_1914);
															if (BgL__ortest_1115z00_1915)
																{	/* Object/class.scm 243 */
																	BgL_res1913z00_1931 =
																		BgL__ortest_1115z00_1915;
																}
															else
																{	/* Object/class.scm 243 */
																	long BgL_odepthz00_1916;

																	{	/* Object/class.scm 243 */
																		obj_t BgL_arg1804z00_1917;

																		BgL_arg1804z00_1917 = (BgL_oclassz00_1914);
																		BgL_odepthz00_1916 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_1917);
																	}
																	if ((2L < BgL_odepthz00_1916))
																		{	/* Object/class.scm 243 */
																			obj_t BgL_arg1802z00_1919;

																			{	/* Object/class.scm 243 */
																				obj_t BgL_arg1803z00_1920;

																				BgL_arg1803z00_1920 =
																					(BgL_oclassz00_1914);
																				BgL_arg1802z00_1919 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_1920, 2L);
																			}
																			BgL_res1913z00_1931 =
																				(BgL_arg1802z00_1919 ==
																				BgL_classz00_1898);
																		}
																	else
																		{	/* Object/class.scm 243 */
																			BgL_res1913z00_1931 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2295z00_5074 = BgL_res1913z00_1931;
												}
										}
									else
										{	/* Object/class.scm 243 */
											BgL_test2295z00_5074 = ((bool_t) 0);
										}
								}
								if (BgL_test2295z00_5074)
									{	/* Object/class.scm 245 */
										bool_t BgL_test2300z00_5097;

										{	/* Object/class.scm 245 */
											obj_t BgL_arg1332z00_901;

											{
												BgL_tclassz00_bglt BgL_auxz00_5098;

												{
													obj_t BgL_auxz00_5099;

													{	/* Object/class.scm 245 */
														BgL_objectz00_bglt BgL_tmpz00_5100;

														BgL_tmpz00_5100 =
															((BgL_objectz00_bglt)
															((BgL_typez00_bglt) BgL_subclassz00_895));
														BgL_auxz00_5099 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_5100);
													}
													BgL_auxz00_5098 =
														((BgL_tclassz00_bglt) BgL_auxz00_5099);
												}
												BgL_arg1332z00_901 =
													(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_5098))->
													BgL_itszd2superzd2);
											}
											BgL_test2300z00_5097 =
												(BgL_arg1332z00_901 == BgL_subclassz00_895);
										}
										if (BgL_test2300z00_5097)
											{	/* Object/class.scm 245 */
												return ((bool_t) 0);
											}
										else
											{	/* Object/class.scm 248 */
												obj_t BgL_arg1331z00_900;

												{
													BgL_tclassz00_bglt BgL_auxz00_5107;

													{
														obj_t BgL_auxz00_5108;

														{	/* Object/class.scm 248 */
															BgL_objectz00_bglt BgL_tmpz00_5109;

															BgL_tmpz00_5109 =
																((BgL_objectz00_bglt)
																((BgL_typez00_bglt) BgL_subclassz00_895));
															BgL_auxz00_5108 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_5109);
														}
														BgL_auxz00_5107 =
															((BgL_tclassz00_bglt) BgL_auxz00_5108);
													}
													BgL_arg1331z00_900 =
														(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_5107))->
														BgL_itszd2superzd2);
												}
												{
													obj_t BgL_subclassz00_5115;

													BgL_subclassz00_5115 = BgL_arg1331z00_900;
													BgL_subclassz00_895 = BgL_subclassz00_5115;
													goto BgL_zc3z04anonymousza31326ze3z87_896;
												}
											}
									}
								else
									{	/* Object/class.scm 243 */
										return ((bool_t) 0);
									}
							}
					}
				else
					{	/* Object/class.scm 249 */
						bool_t BgL_test2301z00_5117;

						{	/* Object/class.scm 249 */
							bool_t BgL_test2302z00_5118;

							{	/* Object/class.scm 249 */
								obj_t BgL_classz00_1936;

								BgL_classz00_1936 = BGl_jclassz00zzobject_classz00;
								{	/* Object/class.scm 249 */
									BgL_objectz00_bglt BgL_arg1807z00_1938;

									{	/* Object/class.scm 249 */
										obj_t BgL_tmpz00_5119;

										BgL_tmpz00_5119 = ((obj_t) BgL_classz00_263);
										BgL_arg1807z00_1938 =
											(BgL_objectz00_bglt) (BgL_tmpz00_5119);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Object/class.scm 249 */
											long BgL_idxz00_1944;

											BgL_idxz00_1944 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1938);
											BgL_test2302z00_5118 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_1944 + 2L)) == BgL_classz00_1936);
										}
									else
										{	/* Object/class.scm 249 */
											bool_t BgL_res1914z00_1969;

											{	/* Object/class.scm 249 */
												obj_t BgL_oclassz00_1952;

												{	/* Object/class.scm 249 */
													obj_t BgL_arg1815z00_1960;
													long BgL_arg1816z00_1961;

													BgL_arg1815z00_1960 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Object/class.scm 249 */
														long BgL_arg1817z00_1962;

														BgL_arg1817z00_1962 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1938);
														BgL_arg1816z00_1961 =
															(BgL_arg1817z00_1962 - OBJECT_TYPE);
													}
													BgL_oclassz00_1952 =
														VECTOR_REF(BgL_arg1815z00_1960,
														BgL_arg1816z00_1961);
												}
												{	/* Object/class.scm 249 */
													bool_t BgL__ortest_1115z00_1953;

													BgL__ortest_1115z00_1953 =
														(BgL_classz00_1936 == BgL_oclassz00_1952);
													if (BgL__ortest_1115z00_1953)
														{	/* Object/class.scm 249 */
															BgL_res1914z00_1969 = BgL__ortest_1115z00_1953;
														}
													else
														{	/* Object/class.scm 249 */
															long BgL_odepthz00_1954;

															{	/* Object/class.scm 249 */
																obj_t BgL_arg1804z00_1955;

																BgL_arg1804z00_1955 = (BgL_oclassz00_1952);
																BgL_odepthz00_1954 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_1955);
															}
															if ((2L < BgL_odepthz00_1954))
																{	/* Object/class.scm 249 */
																	obj_t BgL_arg1802z00_1957;

																	{	/* Object/class.scm 249 */
																		obj_t BgL_arg1803z00_1958;

																		BgL_arg1803z00_1958 = (BgL_oclassz00_1952);
																		BgL_arg1802z00_1957 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_1958, 2L);
																	}
																	BgL_res1914z00_1969 =
																		(BgL_arg1802z00_1957 == BgL_classz00_1936);
																}
															else
																{	/* Object/class.scm 249 */
																	BgL_res1914z00_1969 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2302z00_5118 = BgL_res1914z00_1969;
										}
								}
							}
							if (BgL_test2302z00_5118)
								{	/* Object/class.scm 249 */
									obj_t BgL_classz00_1970;

									BgL_classz00_1970 = BGl_jclassz00zzobject_classz00;
									{	/* Object/class.scm 249 */
										BgL_objectz00_bglt BgL_arg1807z00_1972;

										{	/* Object/class.scm 249 */
											obj_t BgL_tmpz00_5141;

											BgL_tmpz00_5141 = ((obj_t) BgL_subclassz00_262);
											BgL_arg1807z00_1972 =
												(BgL_objectz00_bglt) (BgL_tmpz00_5141);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Object/class.scm 249 */
												long BgL_idxz00_1978;

												BgL_idxz00_1978 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1972);
												BgL_test2301z00_5117 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_1978 + 2L)) == BgL_classz00_1970);
											}
										else
											{	/* Object/class.scm 249 */
												bool_t BgL_res1915z00_2003;

												{	/* Object/class.scm 249 */
													obj_t BgL_oclassz00_1986;

													{	/* Object/class.scm 249 */
														obj_t BgL_arg1815z00_1994;
														long BgL_arg1816z00_1995;

														BgL_arg1815z00_1994 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Object/class.scm 249 */
															long BgL_arg1817z00_1996;

															BgL_arg1817z00_1996 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1972);
															BgL_arg1816z00_1995 =
																(BgL_arg1817z00_1996 - OBJECT_TYPE);
														}
														BgL_oclassz00_1986 =
															VECTOR_REF(BgL_arg1815z00_1994,
															BgL_arg1816z00_1995);
													}
													{	/* Object/class.scm 249 */
														bool_t BgL__ortest_1115z00_1987;

														BgL__ortest_1115z00_1987 =
															(BgL_classz00_1970 == BgL_oclassz00_1986);
														if (BgL__ortest_1115z00_1987)
															{	/* Object/class.scm 249 */
																BgL_res1915z00_2003 = BgL__ortest_1115z00_1987;
															}
														else
															{	/* Object/class.scm 249 */
																long BgL_odepthz00_1988;

																{	/* Object/class.scm 249 */
																	obj_t BgL_arg1804z00_1989;

																	BgL_arg1804z00_1989 = (BgL_oclassz00_1986);
																	BgL_odepthz00_1988 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_1989);
																}
																if ((2L < BgL_odepthz00_1988))
																	{	/* Object/class.scm 249 */
																		obj_t BgL_arg1802z00_1991;

																		{	/* Object/class.scm 249 */
																			obj_t BgL_arg1803z00_1992;

																			BgL_arg1803z00_1992 =
																				(BgL_oclassz00_1986);
																			BgL_arg1802z00_1991 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_1992, 2L);
																		}
																		BgL_res1915z00_2003 =
																			(BgL_arg1802z00_1991 ==
																			BgL_classz00_1970);
																	}
																else
																	{	/* Object/class.scm 249 */
																		BgL_res1915z00_2003 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2301z00_5117 = BgL_res1915z00_2003;
											}
									}
								}
							else
								{	/* Object/class.scm 249 */
									BgL_test2301z00_5117 = ((bool_t) 0);
								}
						}
						if (BgL_test2301z00_5117)
							{
								obj_t BgL_subclassz00_906;

								BgL_subclassz00_906 = ((obj_t) BgL_subclassz00_262);
							BgL_zc3z04anonymousza31336ze3z87_907:
								if ((BgL_subclassz00_906 == ((obj_t) BgL_classz00_263)))
									{	/* Object/class.scm 252 */
										return ((bool_t) 1);
									}
								else
									{	/* Object/class.scm 254 */
										bool_t BgL_test2310z00_5166;

										{	/* Object/class.scm 254 */
											obj_t BgL_classz00_2004;

											BgL_classz00_2004 = BGl_jclassz00zzobject_classz00;
											if (BGL_OBJECTP(BgL_subclassz00_906))
												{	/* Object/class.scm 254 */
													BgL_objectz00_bglt BgL_arg1807z00_2006;

													BgL_arg1807z00_2006 =
														(BgL_objectz00_bglt) (BgL_subclassz00_906);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Object/class.scm 254 */
															long BgL_idxz00_2012;

															BgL_idxz00_2012 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2006);
															BgL_test2310z00_5166 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_2012 + 2L)) == BgL_classz00_2004);
														}
													else
														{	/* Object/class.scm 254 */
															bool_t BgL_res1916z00_2037;

															{	/* Object/class.scm 254 */
																obj_t BgL_oclassz00_2020;

																{	/* Object/class.scm 254 */
																	obj_t BgL_arg1815z00_2028;
																	long BgL_arg1816z00_2029;

																	BgL_arg1815z00_2028 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Object/class.scm 254 */
																		long BgL_arg1817z00_2030;

																		BgL_arg1817z00_2030 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2006);
																		BgL_arg1816z00_2029 =
																			(BgL_arg1817z00_2030 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_2020 =
																		VECTOR_REF(BgL_arg1815z00_2028,
																		BgL_arg1816z00_2029);
																}
																{	/* Object/class.scm 254 */
																	bool_t BgL__ortest_1115z00_2021;

																	BgL__ortest_1115z00_2021 =
																		(BgL_classz00_2004 == BgL_oclassz00_2020);
																	if (BgL__ortest_1115z00_2021)
																		{	/* Object/class.scm 254 */
																			BgL_res1916z00_2037 =
																				BgL__ortest_1115z00_2021;
																		}
																	else
																		{	/* Object/class.scm 254 */
																			long BgL_odepthz00_2022;

																			{	/* Object/class.scm 254 */
																				obj_t BgL_arg1804z00_2023;

																				BgL_arg1804z00_2023 =
																					(BgL_oclassz00_2020);
																				BgL_odepthz00_2022 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_2023);
																			}
																			if ((2L < BgL_odepthz00_2022))
																				{	/* Object/class.scm 254 */
																					obj_t BgL_arg1802z00_2025;

																					{	/* Object/class.scm 254 */
																						obj_t BgL_arg1803z00_2026;

																						BgL_arg1803z00_2026 =
																							(BgL_oclassz00_2020);
																						BgL_arg1802z00_2025 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_2026, 2L);
																					}
																					BgL_res1916z00_2037 =
																						(BgL_arg1802z00_2025 ==
																						BgL_classz00_2004);
																				}
																			else
																				{	/* Object/class.scm 254 */
																					BgL_res1916z00_2037 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2310z00_5166 = BgL_res1916z00_2037;
														}
												}
											else
												{	/* Object/class.scm 254 */
													BgL_test2310z00_5166 = ((bool_t) 0);
												}
										}
										if (BgL_test2310z00_5166)
											{	/* Object/class.scm 256 */
												bool_t BgL_test2315z00_5189;

												{	/* Object/class.scm 256 */
													obj_t BgL_arg1343z00_912;

													{
														BgL_jclassz00_bglt BgL_auxz00_5190;

														{
															obj_t BgL_auxz00_5191;

															{	/* Object/class.scm 256 */
																BgL_objectz00_bglt BgL_tmpz00_5192;

																BgL_tmpz00_5192 =
																	((BgL_objectz00_bglt)
																	((BgL_typez00_bglt) BgL_subclassz00_906));
																BgL_auxz00_5191 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5192);
															}
															BgL_auxz00_5190 =
																((BgL_jclassz00_bglt) BgL_auxz00_5191);
														}
														BgL_arg1343z00_912 =
															(((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_5190))->
															BgL_itszd2superzd2);
													}
													BgL_test2315z00_5189 =
														(BgL_arg1343z00_912 == BgL_subclassz00_906);
												}
												if (BgL_test2315z00_5189)
													{	/* Object/class.scm 256 */
														return ((bool_t) 0);
													}
												else
													{	/* Object/class.scm 259 */
														obj_t BgL_arg1342z00_911;

														{
															BgL_jclassz00_bglt BgL_auxz00_5199;

															{
																obj_t BgL_auxz00_5200;

																{	/* Object/class.scm 259 */
																	BgL_objectz00_bglt BgL_tmpz00_5201;

																	BgL_tmpz00_5201 =
																		((BgL_objectz00_bglt)
																		((BgL_typez00_bglt) BgL_subclassz00_906));
																	BgL_auxz00_5200 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_5201);
																}
																BgL_auxz00_5199 =
																	((BgL_jclassz00_bglt) BgL_auxz00_5200);
															}
															BgL_arg1342z00_911 =
																(((BgL_jclassz00_bglt)
																	COBJECT(BgL_auxz00_5199))->
																BgL_itszd2superzd2);
														}
														{
															obj_t BgL_subclassz00_5207;

															BgL_subclassz00_5207 = BgL_arg1342z00_911;
															BgL_subclassz00_906 = BgL_subclassz00_5207;
															goto BgL_zc3z04anonymousza31336ze3z87_907;
														}
													}
											}
										else
											{	/* Object/class.scm 254 */
												return ((bool_t) 0);
											}
									}
							}
						else
							{	/* Object/class.scm 249 */
								return ((bool_t) 0);
							}
					}
			}
		}

	}



/* &type-subclass? */
	obj_t BGl_z62typezd2subclasszf3z43zzobject_classz00(obj_t BgL_envz00_3174,
		obj_t BgL_subclassz00_3175, obj_t BgL_classz00_3176)
	{
		{	/* Object/class.scm 236 */
			return
				BBOOL(BGl_typezd2subclasszf3z21zzobject_classz00(
					((BgL_typez00_bglt) BgL_subclassz00_3175),
					((BgL_typez00_bglt) BgL_classz00_3176)));
		}

	}



/* find-class-constructor */
	BGL_EXPORTED_DEF obj_t
		BGl_findzd2classzd2constructorz00zzobject_classz00(BgL_typez00_bglt
		BgL_classz00_264)
	{
		{	/* Object/class.scm 266 */
			{
				obj_t BgL_classz00_917;

				BgL_classz00_917 = ((obj_t) BgL_classz00_264);
			BgL_zc3z04anonymousza31344ze3z87_918:
				{	/* Object/class.scm 268 */
					bool_t BgL_test2316z00_5213;

					{	/* Object/class.scm 268 */
						obj_t BgL_classz00_2042;

						BgL_classz00_2042 = BGl_tclassz00zzobject_classz00;
						if (BGL_OBJECTP(BgL_classz00_917))
							{	/* Object/class.scm 268 */
								BgL_objectz00_bglt BgL_arg1807z00_2044;

								BgL_arg1807z00_2044 = (BgL_objectz00_bglt) (BgL_classz00_917);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Object/class.scm 268 */
										long BgL_idxz00_2050;

										BgL_idxz00_2050 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2044);
										BgL_test2316z00_5213 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2050 + 2L)) == BgL_classz00_2042);
									}
								else
									{	/* Object/class.scm 268 */
										bool_t BgL_res1917z00_2075;

										{	/* Object/class.scm 268 */
											obj_t BgL_oclassz00_2058;

											{	/* Object/class.scm 268 */
												obj_t BgL_arg1815z00_2066;
												long BgL_arg1816z00_2067;

												BgL_arg1815z00_2066 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Object/class.scm 268 */
													long BgL_arg1817z00_2068;

													BgL_arg1817z00_2068 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2044);
													BgL_arg1816z00_2067 =
														(BgL_arg1817z00_2068 - OBJECT_TYPE);
												}
												BgL_oclassz00_2058 =
													VECTOR_REF(BgL_arg1815z00_2066, BgL_arg1816z00_2067);
											}
											{	/* Object/class.scm 268 */
												bool_t BgL__ortest_1115z00_2059;

												BgL__ortest_1115z00_2059 =
													(BgL_classz00_2042 == BgL_oclassz00_2058);
												if (BgL__ortest_1115z00_2059)
													{	/* Object/class.scm 268 */
														BgL_res1917z00_2075 = BgL__ortest_1115z00_2059;
													}
												else
													{	/* Object/class.scm 268 */
														long BgL_odepthz00_2060;

														{	/* Object/class.scm 268 */
															obj_t BgL_arg1804z00_2061;

															BgL_arg1804z00_2061 = (BgL_oclassz00_2058);
															BgL_odepthz00_2060 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2061);
														}
														if ((2L < BgL_odepthz00_2060))
															{	/* Object/class.scm 268 */
																obj_t BgL_arg1802z00_2063;

																{	/* Object/class.scm 268 */
																	obj_t BgL_arg1803z00_2064;

																	BgL_arg1803z00_2064 = (BgL_oclassz00_2058);
																	BgL_arg1802z00_2063 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2064,
																		2L);
																}
																BgL_res1917z00_2075 =
																	(BgL_arg1802z00_2063 == BgL_classz00_2042);
															}
														else
															{	/* Object/class.scm 268 */
																BgL_res1917z00_2075 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2316z00_5213 = BgL_res1917z00_2075;
									}
							}
						else
							{	/* Object/class.scm 268 */
								BgL_test2316z00_5213 = ((bool_t) 0);
							}
					}
					if (BgL_test2316z00_5213)
						{	/* Object/class.scm 271 */
							bool_t BgL_test2321z00_5236;

							{	/* Object/class.scm 271 */
								obj_t BgL_arg1351z00_925;

								{
									BgL_tclassz00_bglt BgL_auxz00_5237;

									{
										obj_t BgL_auxz00_5238;

										{	/* Object/class.scm 271 */
											BgL_objectz00_bglt BgL_tmpz00_5239;

											BgL_tmpz00_5239 =
												((BgL_objectz00_bglt)
												((BgL_typez00_bglt) BgL_classz00_917));
											BgL_auxz00_5238 = BGL_OBJECT_WIDENING(BgL_tmpz00_5239);
										}
										BgL_auxz00_5237 = ((BgL_tclassz00_bglt) BgL_auxz00_5238);
									}
									BgL_arg1351z00_925 =
										(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_5237))->
										BgL_itszd2superzd2);
								}
								BgL_test2321z00_5236 = (BgL_classz00_917 == BgL_arg1351z00_925);
							}
							if (BgL_test2321z00_5236)
								{	/* Object/class.scm 271 */
									return BFALSE;
								}
							else
								{	/* Object/class.scm 272 */
									bool_t BgL_test2322z00_5246;

									{	/* Object/class.scm 272 */
										obj_t BgL_tmpz00_5247;

										{
											BgL_tclassz00_bglt BgL_auxz00_5248;

											{
												obj_t BgL_auxz00_5249;

												{	/* Object/class.scm 272 */
													BgL_objectz00_bglt BgL_tmpz00_5250;

													BgL_tmpz00_5250 =
														((BgL_objectz00_bglt)
														((BgL_typez00_bglt) BgL_classz00_917));
													BgL_auxz00_5249 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_5250);
												}
												BgL_auxz00_5248 =
													((BgL_tclassz00_bglt) BgL_auxz00_5249);
											}
											BgL_tmpz00_5247 =
												(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_5248))->
												BgL_constructorz00);
										}
										BgL_test2322z00_5246 = CBOOL(BgL_tmpz00_5247);
									}
									if (BgL_test2322z00_5246)
										{
											BgL_tclassz00_bglt BgL_auxz00_5257;

											{
												obj_t BgL_auxz00_5258;

												{	/* Object/class.scm 272 */
													BgL_objectz00_bglt BgL_tmpz00_5259;

													BgL_tmpz00_5259 =
														((BgL_objectz00_bglt)
														((BgL_typez00_bglt) BgL_classz00_917));
													BgL_auxz00_5258 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_5259);
												}
												BgL_auxz00_5257 =
													((BgL_tclassz00_bglt) BgL_auxz00_5258);
											}
											return
												(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_5257))->
												BgL_constructorz00);
										}
									else
										{	/* Object/class.scm 273 */
											obj_t BgL_arg1349z00_924;

											{
												BgL_tclassz00_bglt BgL_auxz00_5265;

												{
													obj_t BgL_auxz00_5266;

													{	/* Object/class.scm 273 */
														BgL_objectz00_bglt BgL_tmpz00_5267;

														BgL_tmpz00_5267 =
															((BgL_objectz00_bglt)
															((BgL_typez00_bglt) BgL_classz00_917));
														BgL_auxz00_5266 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_5267);
													}
													BgL_auxz00_5265 =
														((BgL_tclassz00_bglt) BgL_auxz00_5266);
												}
												BgL_arg1349z00_924 =
													(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_5265))->
													BgL_itszd2superzd2);
											}
											{
												obj_t BgL_classz00_5273;

												BgL_classz00_5273 = BgL_arg1349z00_924;
												BgL_classz00_917 = BgL_classz00_5273;
												goto BgL_zc3z04anonymousza31344ze3z87_918;
											}
										}
								}
						}
					else
						{	/* Object/class.scm 268 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &find-class-constructor */
	obj_t BGl_z62findzd2classzd2constructorz62zzobject_classz00(obj_t
		BgL_envz00_3177, obj_t BgL_classz00_3178)
	{
		{	/* Object/class.scm 266 */
			return
				BGl_findzd2classzd2constructorz00zzobject_classz00(
				((BgL_typez00_bglt) BgL_classz00_3178));
		}

	}



/* find-common-super-class */
	BGL_EXPORTED_DEF obj_t
		BGl_findzd2commonzd2superzd2classzd2zzobject_classz00(BgL_typez00_bglt
		BgL_c1z00_265, BgL_typez00_bglt BgL_c2z00_266)
	{
		{	/* Object/class.scm 278 */
			if (BGl_typezd2subclasszf3z21zzobject_classz00(
					((BgL_typez00_bglt) BgL_c1z00_265),
					((BgL_typez00_bglt) BgL_c2z00_266)))
				{	/* Object/class.scm 285 */
					return ((obj_t) BgL_c2z00_266);
				}
			else
				{	/* Object/class.scm 285 */
					if (BGl_typezd2subclasszf3z21zzobject_classz00(
							((BgL_typez00_bglt) BgL_c2z00_266),
							((BgL_typez00_bglt) BgL_c1z00_265)))
						{	/* Object/class.scm 287 */
							return ((obj_t) BgL_c1z00_265);
						}
					else
						{	/* Object/class.scm 290 */
							obj_t BgL_l1z00_930;
							obj_t BgL_l2z00_931;

							BgL_l1z00_930 =
								BGl_tclasszd2superza2ze70z97zzobject_classz00(
								((obj_t) BgL_c1z00_265));
							BgL_l2z00_931 =
								BGl_tclasszd2superza2ze70z97zzobject_classz00(
								((obj_t) BgL_c2z00_266));
							{
								obj_t BgL_lz00_933;

								BgL_lz00_933 = BgL_l1z00_930;
							BgL_zc3z04anonymousza31356ze3z87_934:
								if (NULLP(BgL_lz00_933))
									{	/* Object/class.scm 294 */
										return BFALSE;
									}
								else
									{	/* Object/class.scm 294 */
										if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR(
														((obj_t) BgL_lz00_933)), BgL_l2z00_931)))
											{	/* Object/class.scm 296 */
												return CAR(((obj_t) BgL_lz00_933));
											}
										else
											{
												obj_t BgL_lz00_5300;

												BgL_lz00_5300 = CDR(((obj_t) BgL_lz00_933));
												BgL_lz00_933 = BgL_lz00_5300;
												goto BgL_zc3z04anonymousza31356ze3z87_934;
											}
									}
							}
						}
				}
		}

	}



/* tclass-super*~0 */
	obj_t BGl_tclasszd2superza2ze70z97zzobject_classz00(obj_t BgL_cz00_941)
	{
		{	/* Object/class.scm 283 */
			{	/* Object/class.scm 281 */
				bool_t BgL_test2327z00_5303;

				{	/* Object/class.scm 281 */
					bool_t BgL_test2328z00_5304;

					{	/* Object/class.scm 281 */
						obj_t BgL_tmpz00_5305;

						{
							BgL_tclassz00_bglt BgL_auxz00_5306;

							{
								obj_t BgL_auxz00_5307;

								{	/* Object/class.scm 281 */
									BgL_objectz00_bglt BgL_tmpz00_5308;

									BgL_tmpz00_5308 =
										((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_cz00_941));
									BgL_auxz00_5307 = BGL_OBJECT_WIDENING(BgL_tmpz00_5308);
								}
								BgL_auxz00_5306 = ((BgL_tclassz00_bglt) BgL_auxz00_5307);
							}
							BgL_tmpz00_5305 =
								(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_5306))->
								BgL_itszd2superzd2);
						}
						BgL_test2328z00_5304 = CBOOL(BgL_tmpz00_5305);
					}
					if (BgL_test2328z00_5304)
						{	/* Object/class.scm 281 */
							obj_t BgL_arg1377z00_950;

							{
								BgL_tclassz00_bglt BgL_auxz00_5315;

								{
									obj_t BgL_auxz00_5316;

									{	/* Object/class.scm 281 */
										BgL_objectz00_bglt BgL_tmpz00_5317;

										BgL_tmpz00_5317 =
											((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_cz00_941));
										BgL_auxz00_5316 = BGL_OBJECT_WIDENING(BgL_tmpz00_5317);
									}
									BgL_auxz00_5315 = ((BgL_tclassz00_bglt) BgL_auxz00_5316);
								}
								BgL_arg1377z00_950 =
									(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_5315))->
									BgL_itszd2superzd2);
							}
							BgL_test2327z00_5303 = (BgL_arg1377z00_950 == BgL_cz00_941);
						}
					else
						{	/* Object/class.scm 281 */
							BgL_test2327z00_5303 = ((bool_t) 1);
						}
				}
				if (BgL_test2327z00_5303)
					{	/* Object/class.scm 281 */
						return BNIL;
					}
				else
					{	/* Object/class.scm 283 */
						obj_t BgL_arg1375z00_947;

						{	/* Object/class.scm 283 */
							obj_t BgL_arg1376z00_948;

							{
								BgL_tclassz00_bglt BgL_auxz00_5324;

								{
									obj_t BgL_auxz00_5325;

									{	/* Object/class.scm 283 */
										BgL_objectz00_bglt BgL_tmpz00_5326;

										BgL_tmpz00_5326 =
											((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_cz00_941));
										BgL_auxz00_5325 = BGL_OBJECT_WIDENING(BgL_tmpz00_5326);
									}
									BgL_auxz00_5324 = ((BgL_tclassz00_bglt) BgL_auxz00_5325);
								}
								BgL_arg1376z00_948 =
									(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_5324))->
									BgL_itszd2superzd2);
							}
							BgL_arg1375z00_947 =
								BGl_tclasszd2superza2ze70z97zzobject_classz00
								(BgL_arg1376z00_948);
						}
						return MAKE_YOUNG_PAIR(BgL_cz00_941, BgL_arg1375z00_947);
					}
			}
		}

	}



/* &find-common-super-class */
	obj_t BGl_z62findzd2commonzd2superzd2classzb0zzobject_classz00(obj_t
		BgL_envz00_3179, obj_t BgL_c1z00_3180, obj_t BgL_c2z00_3181)
	{
		{	/* Object/class.scm 278 */
			return
				BGl_findzd2commonzd2superzd2classzd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_c1z00_3180),
				((BgL_typez00_bglt) BgL_c2z00_3181));
		}

	}



/* class-make */
	BGL_EXPORTED_DEF obj_t BGl_classzd2makezd2zzobject_classz00(BgL_typez00_bglt
		BgL_tz00_267)
	{
		{	/* Object/class.scm 306 */
			{	/* Object/class.scm 307 */
				bool_t BgL_test2329z00_5337;

				{
					BgL_tclassz00_bglt BgL_auxz00_5338;

					{
						obj_t BgL_auxz00_5339;

						{	/* Object/class.scm 307 */
							BgL_objectz00_bglt BgL_tmpz00_5340;

							BgL_tmpz00_5340 = ((BgL_objectz00_bglt) BgL_tz00_267);
							BgL_auxz00_5339 = BGL_OBJECT_WIDENING(BgL_tmpz00_5340);
						}
						BgL_auxz00_5338 = ((BgL_tclassz00_bglt) BgL_auxz00_5339);
					}
					BgL_test2329z00_5337 =
						(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_5338))->
						BgL_abstractzf3zf3);
				}
				if (BgL_test2329z00_5337)
					{	/* Object/class.scm 307 */
						return BFALSE;
					}
				else
					{	/* Object/class.scm 309 */
						obj_t BgL_arg1408z00_953;

						{	/* Object/class.scm 309 */
							obj_t BgL_arg1410z00_954;
							obj_t BgL_arg1421z00_955;

							{	/* Object/class.scm 309 */
								obj_t BgL_symbolz00_2088;

								BgL_symbolz00_2088 = CNST_TABLE_REF(3);
								{	/* Object/class.scm 309 */
									obj_t BgL_arg1455z00_2089;

									BgL_arg1455z00_2089 = SYMBOL_TO_STRING(BgL_symbolz00_2088);
									BgL_arg1410z00_954 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_2089);
								}
							}
							{	/* Object/class.scm 309 */
								obj_t BgL_arg1422z00_956;

								BgL_arg1422z00_956 =
									(((BgL_typez00_bglt) COBJECT(
											((BgL_typez00_bglt) BgL_tz00_267)))->BgL_idz00);
								{	/* Object/class.scm 309 */
									obj_t BgL_arg1455z00_2092;

									BgL_arg1455z00_2092 = SYMBOL_TO_STRING(BgL_arg1422z00_956);
									BgL_arg1421z00_955 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_2092);
								}
							}
							BgL_arg1408z00_953 =
								string_append(BgL_arg1410z00_954, BgL_arg1421z00_955);
						}
						return bstring_to_symbol(BgL_arg1408z00_953);
					}
			}
		}

	}



/* &class-make */
	obj_t BGl_z62classzd2makezb0zzobject_classz00(obj_t BgL_envz00_3182,
		obj_t BgL_tz00_3183)
	{
		{	/* Object/class.scm 306 */
			return
				BGl_classzd2makezd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_tz00_3183));
		}

	}



/* class-fill */
	BGL_EXPORTED_DEF obj_t BGl_classzd2fillzd2zzobject_classz00(BgL_typez00_bglt
		BgL_tz00_268)
	{
		{	/* Object/class.scm 316 */
			{	/* Object/class.scm 317 */
				bool_t BgL_test2330z00_5356;

				{
					BgL_tclassz00_bglt BgL_auxz00_5357;

					{
						obj_t BgL_auxz00_5358;

						{	/* Object/class.scm 317 */
							BgL_objectz00_bglt BgL_tmpz00_5359;

							BgL_tmpz00_5359 = ((BgL_objectz00_bglt) BgL_tz00_268);
							BgL_auxz00_5358 = BGL_OBJECT_WIDENING(BgL_tmpz00_5359);
						}
						BgL_auxz00_5357 = ((BgL_tclassz00_bglt) BgL_auxz00_5358);
					}
					BgL_test2330z00_5356 =
						(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_5357))->
						BgL_abstractzf3zf3);
				}
				if (BgL_test2330z00_5356)
					{	/* Object/class.scm 317 */
						return BFALSE;
					}
				else
					{	/* Object/class.scm 319 */
						obj_t BgL_arg1434z00_958;

						BgL_arg1434z00_958 =
							(((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt) BgL_tz00_268)))->BgL_idz00);
						{	/* Object/class.scm 319 */
							obj_t BgL_list1435z00_959;

							{	/* Object/class.scm 319 */
								obj_t BgL_arg1437z00_960;

								{	/* Object/class.scm 319 */
									obj_t BgL_arg1448z00_961;

									BgL_arg1448z00_961 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(4), BNIL);
									BgL_arg1437z00_960 =
										MAKE_YOUNG_PAIR(BgL_arg1434z00_958, BgL_arg1448z00_961);
								}
								BgL_list1435z00_959 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(5), BgL_arg1437z00_960);
							}
							return
								BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
								(BgL_list1435z00_959);
						}
					}
			}
		}

	}



/* &class-fill */
	obj_t BGl_z62classzd2fillzb0zzobject_classz00(obj_t BgL_envz00_3184,
		obj_t BgL_tz00_3185)
	{
		{	/* Object/class.scm 316 */
			return
				BGl_classzd2fillzd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_tz00_3185));
		}

	}



/* class-predicate */
	BGL_EXPORTED_DEF obj_t
		BGl_classzd2predicatezd2zzobject_classz00(BgL_typez00_bglt BgL_tz00_269)
	{
		{	/* Object/class.scm 326 */
			{	/* Object/class.scm 327 */
				obj_t BgL_arg1453z00_962;

				{	/* Object/class.scm 327 */
					obj_t BgL_arg1454z00_963;
					obj_t BgL_arg1472z00_964;

					{	/* Object/class.scm 327 */
						obj_t BgL_arg1473z00_965;

						BgL_arg1473z00_965 =
							(((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt) BgL_tz00_269)))->BgL_idz00);
						{	/* Object/class.scm 327 */
							obj_t BgL_arg1455z00_2099;

							BgL_arg1455z00_2099 = SYMBOL_TO_STRING(BgL_arg1473z00_965);
							BgL_arg1454z00_963 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_2099);
						}
					}
					{	/* Object/class.scm 327 */
						obj_t BgL_symbolz00_2100;

						BgL_symbolz00_2100 = CNST_TABLE_REF(6);
						{	/* Object/class.scm 327 */
							obj_t BgL_arg1455z00_2101;

							BgL_arg1455z00_2101 = SYMBOL_TO_STRING(BgL_symbolz00_2100);
							BgL_arg1472z00_964 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_2101);
						}
					}
					BgL_arg1453z00_962 =
						string_append(BgL_arg1454z00_963, BgL_arg1472z00_964);
				}
				return bstring_to_symbol(BgL_arg1453z00_962);
			}
		}

	}



/* &class-predicate */
	obj_t BGl_z62classzd2predicatezb0zzobject_classz00(obj_t BgL_envz00_3186,
		obj_t BgL_tz00_3187)
	{
		{	/* Object/class.scm 326 */
			return
				BGl_classzd2predicatezd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_tz00_3187));
		}

	}



/* class-allocate */
	BGL_EXPORTED_DEF obj_t
		BGl_classzd2allocatezd2zzobject_classz00(BgL_typez00_bglt BgL_tz00_270)
	{
		{	/* Object/class.scm 334 */
			{	/* Object/class.scm 335 */
				obj_t BgL_arg1485z00_966;

				{	/* Object/class.scm 335 */
					obj_t BgL_arg1489z00_967;
					obj_t BgL_arg1502z00_968;

					{	/* Object/class.scm 335 */
						obj_t BgL_symbolz00_2103;

						BgL_symbolz00_2103 = CNST_TABLE_REF(7);
						{	/* Object/class.scm 335 */
							obj_t BgL_arg1455z00_2104;

							BgL_arg1455z00_2104 = SYMBOL_TO_STRING(BgL_symbolz00_2103);
							BgL_arg1489z00_967 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_2104);
						}
					}
					{	/* Object/class.scm 335 */
						obj_t BgL_arg1509z00_969;

						BgL_arg1509z00_969 =
							(((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt) BgL_tz00_270)))->BgL_idz00);
						{	/* Object/class.scm 335 */
							obj_t BgL_arg1455z00_2107;

							BgL_arg1455z00_2107 = SYMBOL_TO_STRING(BgL_arg1509z00_969);
							BgL_arg1502z00_968 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_2107);
						}
					}
					BgL_arg1485z00_966 =
						string_append(BgL_arg1489z00_967, BgL_arg1502z00_968);
				}
				return bstring_to_symbol(BgL_arg1485z00_966);
			}
		}

	}



/* &class-allocate */
	obj_t BGl_z62classzd2allocatezb0zzobject_classz00(obj_t BgL_envz00_3188,
		obj_t BgL_tz00_3189)
	{
		{	/* Object/class.scm 334 */
			return
				BGl_classzd2allocatezd2zzobject_classz00(
				((BgL_typez00_bglt) BgL_tz00_3189));
		}

	}



/* check-class-declaration? */
	BGL_EXPORTED_DEF bool_t
		BGl_checkzd2classzd2declarationzf3zf3zzobject_classz00(BgL_typez00_bglt
		BgL_classz00_272, obj_t BgL_srczd2defzd2_273)
	{
		{	/* Object/class.scm 354 */
			{	/* Object/class.scm 355 */
				bool_t BgL_test2331z00_5396;

				{	/* Object/class.scm 355 */
					bool_t BgL_res1919z00_2147;

					{	/* Object/class.scm 231 */
						bool_t BgL_test2332z00_5397;

						{	/* Object/class.scm 231 */
							obj_t BgL_classz00_2111;

							BgL_classz00_2111 = BGl_tclassz00zzobject_classz00;
							{	/* Object/class.scm 231 */
								BgL_objectz00_bglt BgL_arg1807z00_2113;

								{	/* Object/class.scm 231 */
									obj_t BgL_tmpz00_5398;

									BgL_tmpz00_5398 = ((obj_t) BgL_classz00_272);
									BgL_arg1807z00_2113 = (BgL_objectz00_bglt) (BgL_tmpz00_5398);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Object/class.scm 231 */
										long BgL_idxz00_2119;

										BgL_idxz00_2119 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2113);
										BgL_test2332z00_5397 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2119 + 2L)) == BgL_classz00_2111);
									}
								else
									{	/* Object/class.scm 231 */
										bool_t BgL_res1918z00_2144;

										{	/* Object/class.scm 231 */
											obj_t BgL_oclassz00_2127;

											{	/* Object/class.scm 231 */
												obj_t BgL_arg1815z00_2135;
												long BgL_arg1816z00_2136;

												BgL_arg1815z00_2135 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Object/class.scm 231 */
													long BgL_arg1817z00_2137;

													BgL_arg1817z00_2137 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2113);
													BgL_arg1816z00_2136 =
														(BgL_arg1817z00_2137 - OBJECT_TYPE);
												}
												BgL_oclassz00_2127 =
													VECTOR_REF(BgL_arg1815z00_2135, BgL_arg1816z00_2136);
											}
											{	/* Object/class.scm 231 */
												bool_t BgL__ortest_1115z00_2128;

												BgL__ortest_1115z00_2128 =
													(BgL_classz00_2111 == BgL_oclassz00_2127);
												if (BgL__ortest_1115z00_2128)
													{	/* Object/class.scm 231 */
														BgL_res1918z00_2144 = BgL__ortest_1115z00_2128;
													}
												else
													{	/* Object/class.scm 231 */
														long BgL_odepthz00_2129;

														{	/* Object/class.scm 231 */
															obj_t BgL_arg1804z00_2130;

															BgL_arg1804z00_2130 = (BgL_oclassz00_2127);
															BgL_odepthz00_2129 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2130);
														}
														if ((2L < BgL_odepthz00_2129))
															{	/* Object/class.scm 231 */
																obj_t BgL_arg1802z00_2132;

																{	/* Object/class.scm 231 */
																	obj_t BgL_arg1803z00_2133;

																	BgL_arg1803z00_2133 = (BgL_oclassz00_2127);
																	BgL_arg1802z00_2132 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2133,
																		2L);
																}
																BgL_res1918z00_2144 =
																	(BgL_arg1802z00_2132 == BgL_classz00_2111);
															}
														else
															{	/* Object/class.scm 231 */
																BgL_res1918z00_2144 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2332z00_5397 = BgL_res1918z00_2144;
									}
							}
						}
						if (BgL_test2332z00_5397)
							{	/* Object/class.scm 231 */
								obj_t BgL_tmpz00_5420;

								{
									BgL_tclassz00_bglt BgL_auxz00_5421;

									{
										obj_t BgL_auxz00_5422;

										{	/* Object/class.scm 231 */
											BgL_objectz00_bglt BgL_tmpz00_5423;

											BgL_tmpz00_5423 = ((BgL_objectz00_bglt) BgL_classz00_272);
											BgL_auxz00_5422 = BGL_OBJECT_WIDENING(BgL_tmpz00_5423);
										}
										BgL_auxz00_5421 = ((BgL_tclassz00_bglt) BgL_auxz00_5422);
									}
									BgL_tmpz00_5420 =
										(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_5421))->
										BgL_wideningz00);
								}
								BgL_res1919z00_2147 = CBOOL(BgL_tmpz00_5420);
							}
						else
							{	/* Object/class.scm 231 */
								BgL_res1919z00_2147 = ((bool_t) 0);
							}
					}
					BgL_test2331z00_5396 = BgL_res1919z00_2147;
				}
				if (BgL_test2331z00_5396)
					{	/* Object/class.scm 355 */
						return
							CBOOL
							(BGl_checkzd2widezd2classzd2declarationzf3z21zzobject_classz00
							(BgL_classz00_272, BgL_srczd2defzd2_273));
					}
				else
					{	/* Object/class.scm 355 */
						BGL_TAIL return
							BGl_checkzd2plainzd2classzd2declarationzf3z21zzobject_classz00
							(BgL_classz00_272, BgL_srczd2defzd2_273);
					}
			}
		}

	}



/* &check-class-declaration? */
	obj_t BGl_z62checkzd2classzd2declarationzf3z91zzobject_classz00(obj_t
		BgL_envz00_3190, obj_t BgL_classz00_3191, obj_t BgL_srczd2defzd2_3192)
	{
		{	/* Object/class.scm 354 */
			return
				BBOOL(BGl_checkzd2classzd2declarationzf3zf3zzobject_classz00(
					((BgL_typez00_bglt) BgL_classz00_3191), BgL_srczd2defzd2_3192));
		}

	}



/* check-plain-class-declaration? */
	bool_t
		BGl_checkzd2plainzd2classzd2declarationzf3z21zzobject_classz00
		(BgL_typez00_bglt BgL_classz00_274, obj_t BgL_srczd2defzd2_275)
	{
		{	/* Object/class.scm 366 */
			{	/* Object/class.scm 367 */
				obj_t BgL_superz00_971;
				obj_t BgL_classzd2idzd2_972;

				{
					BgL_tclassz00_bglt BgL_auxz00_5435;

					{
						obj_t BgL_auxz00_5436;

						{	/* Object/class.scm 367 */
							BgL_objectz00_bglt BgL_tmpz00_5437;

							BgL_tmpz00_5437 = ((BgL_objectz00_bglt) BgL_classz00_274);
							BgL_auxz00_5436 = BGL_OBJECT_WIDENING(BgL_tmpz00_5437);
						}
						BgL_auxz00_5435 = ((BgL_tclassz00_bglt) BgL_auxz00_5436);
					}
					BgL_superz00_971 =
						(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_5435))->
						BgL_itszd2superzd2);
				}
				BgL_classzd2idzd2_972 =
					(((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_classz00_274)))->BgL_idz00);
				{	/* Object/class.scm 372 */
					bool_t BgL_test2336z00_5444;

					{	/* Object/class.scm 372 */
						bool_t BgL_test2337z00_5445;

						{	/* Object/class.scm 372 */
							obj_t BgL_classz00_2151;

							BgL_classz00_2151 = BGl_typez00zztype_typez00;
							if (BGL_OBJECTP(BgL_superz00_971))
								{	/* Object/class.scm 372 */
									BgL_objectz00_bglt BgL_arg1807z00_2153;

									BgL_arg1807z00_2153 = (BgL_objectz00_bglt) (BgL_superz00_971);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Object/class.scm 372 */
											long BgL_idxz00_2159;

											BgL_idxz00_2159 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2153);
											BgL_test2337z00_5445 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2159 + 1L)) == BgL_classz00_2151);
										}
									else
										{	/* Object/class.scm 372 */
											bool_t BgL_res1920z00_2184;

											{	/* Object/class.scm 372 */
												obj_t BgL_oclassz00_2167;

												{	/* Object/class.scm 372 */
													obj_t BgL_arg1815z00_2175;
													long BgL_arg1816z00_2176;

													BgL_arg1815z00_2175 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Object/class.scm 372 */
														long BgL_arg1817z00_2177;

														BgL_arg1817z00_2177 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2153);
														BgL_arg1816z00_2176 =
															(BgL_arg1817z00_2177 - OBJECT_TYPE);
													}
													BgL_oclassz00_2167 =
														VECTOR_REF(BgL_arg1815z00_2175,
														BgL_arg1816z00_2176);
												}
												{	/* Object/class.scm 372 */
													bool_t BgL__ortest_1115z00_2168;

													BgL__ortest_1115z00_2168 =
														(BgL_classz00_2151 == BgL_oclassz00_2167);
													if (BgL__ortest_1115z00_2168)
														{	/* Object/class.scm 372 */
															BgL_res1920z00_2184 = BgL__ortest_1115z00_2168;
														}
													else
														{	/* Object/class.scm 372 */
															long BgL_odepthz00_2169;

															{	/* Object/class.scm 372 */
																obj_t BgL_arg1804z00_2170;

																BgL_arg1804z00_2170 = (BgL_oclassz00_2167);
																BgL_odepthz00_2169 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2170);
															}
															if ((1L < BgL_odepthz00_2169))
																{	/* Object/class.scm 372 */
																	obj_t BgL_arg1802z00_2172;

																	{	/* Object/class.scm 372 */
																		obj_t BgL_arg1803z00_2173;

																		BgL_arg1803z00_2173 = (BgL_oclassz00_2167);
																		BgL_arg1802z00_2172 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2173, 1L);
																	}
																	BgL_res1920z00_2184 =
																		(BgL_arg1802z00_2172 == BgL_classz00_2151);
																}
															else
																{	/* Object/class.scm 372 */
																	BgL_res1920z00_2184 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2337z00_5445 = BgL_res1920z00_2184;
										}
								}
							else
								{	/* Object/class.scm 372 */
									BgL_test2337z00_5445 = ((bool_t) 0);
								}
						}
						if (BgL_test2337z00_5445)
							{	/* Object/class.scm 372 */
								bool_t BgL_test2342z00_5468;

								{	/* Object/class.scm 372 */
									obj_t BgL_classz00_2185;

									BgL_classz00_2185 = BGl_tclassz00zzobject_classz00;
									if (BGL_OBJECTP(BgL_superz00_971))
										{	/* Object/class.scm 372 */
											BgL_objectz00_bglt BgL_arg1807z00_2187;

											BgL_arg1807z00_2187 =
												(BgL_objectz00_bglt) (BgL_superz00_971);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Object/class.scm 372 */
													long BgL_idxz00_2193;

													BgL_idxz00_2193 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2187);
													BgL_test2342z00_5468 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_2193 + 2L)) == BgL_classz00_2185);
												}
											else
												{	/* Object/class.scm 372 */
													bool_t BgL_res1921z00_2218;

													{	/* Object/class.scm 372 */
														obj_t BgL_oclassz00_2201;

														{	/* Object/class.scm 372 */
															obj_t BgL_arg1815z00_2209;
															long BgL_arg1816z00_2210;

															BgL_arg1815z00_2209 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Object/class.scm 372 */
																long BgL_arg1817z00_2211;

																BgL_arg1817z00_2211 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2187);
																BgL_arg1816z00_2210 =
																	(BgL_arg1817z00_2211 - OBJECT_TYPE);
															}
															BgL_oclassz00_2201 =
																VECTOR_REF(BgL_arg1815z00_2209,
																BgL_arg1816z00_2210);
														}
														{	/* Object/class.scm 372 */
															bool_t BgL__ortest_1115z00_2202;

															BgL__ortest_1115z00_2202 =
																(BgL_classz00_2185 == BgL_oclassz00_2201);
															if (BgL__ortest_1115z00_2202)
																{	/* Object/class.scm 372 */
																	BgL_res1921z00_2218 =
																		BgL__ortest_1115z00_2202;
																}
															else
																{	/* Object/class.scm 372 */
																	long BgL_odepthz00_2203;

																	{	/* Object/class.scm 372 */
																		obj_t BgL_arg1804z00_2204;

																		BgL_arg1804z00_2204 = (BgL_oclassz00_2201);
																		BgL_odepthz00_2203 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_2204);
																	}
																	if ((2L < BgL_odepthz00_2203))
																		{	/* Object/class.scm 372 */
																			obj_t BgL_arg1802z00_2206;

																			{	/* Object/class.scm 372 */
																				obj_t BgL_arg1803z00_2207;

																				BgL_arg1803z00_2207 =
																					(BgL_oclassz00_2201);
																				BgL_arg1802z00_2206 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_2207, 2L);
																			}
																			BgL_res1921z00_2218 =
																				(BgL_arg1802z00_2206 ==
																				BgL_classz00_2185);
																		}
																	else
																		{	/* Object/class.scm 372 */
																			BgL_res1921z00_2218 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2342z00_5468 = BgL_res1921z00_2218;
												}
										}
									else
										{	/* Object/class.scm 372 */
											BgL_test2342z00_5468 = ((bool_t) 0);
										}
								}
								if (BgL_test2342z00_5468)
									{	/* Object/class.scm 372 */
										BgL_test2336z00_5444 = ((bool_t) 0);
									}
								else
									{	/* Object/class.scm 372 */
										BgL_test2336z00_5444 = ((bool_t) 1);
									}
							}
						else
							{	/* Object/class.scm 372 */
								BgL_test2336z00_5444 = ((bool_t) 0);
							}
					}
					if (BgL_test2336z00_5444)
						{	/* Object/class.scm 372 */
							{	/* Object/class.scm 373 */
								obj_t BgL_arg1514z00_976;
								obj_t BgL_arg1516z00_977;

								{	/* Object/class.scm 373 */
									obj_t BgL_arg1535z00_979;

									BgL_arg1535z00_979 =
										(((BgL_typez00_bglt) COBJECT(
												((BgL_typez00_bglt) BgL_superz00_971)))->BgL_idz00);
									{	/* Object/class.scm 373 */
										obj_t BgL_arg1455z00_2221;

										BgL_arg1455z00_2221 = SYMBOL_TO_STRING(BgL_arg1535z00_979);
										BgL_arg1514z00_976 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_2221);
									}
								}
								{	/* Object/class.scm 374 */
									obj_t BgL_list1536z00_980;

									BgL_list1536z00_980 =
										MAKE_YOUNG_PAIR(BgL_classzd2idzd2_972, BNIL);
									BgL_arg1516z00_977 =
										BGl_formatz00zz__r4_output_6_10_3z00
										(BGl_string1965z00zzobject_classz00, BgL_list1536z00_980);
								}
								{	/* Object/class.scm 373 */
									obj_t BgL_list1517z00_978;

									BgL_list1517z00_978 =
										MAKE_YOUNG_PAIR(BGl_typez00zztype_typez00, BNIL);
									BGl_userzd2errorzd2zztools_errorz00(BgL_arg1514z00_976,
										BgL_arg1516z00_977, BgL_srczd2defzd2_275,
										BgL_list1517z00_978);
								}
							}
							return ((bool_t) 0);
						}
					else
						{	/* Object/class.scm 378 */
							bool_t BgL_test2347z00_5499;

							{	/* Object/class.scm 378 */
								bool_t BgL_res1923z00_2259;

								{	/* Object/class.scm 231 */
									bool_t BgL_test2348z00_5500;

									{	/* Object/class.scm 231 */
										obj_t BgL_classz00_2223;

										BgL_classz00_2223 = BGl_tclassz00zzobject_classz00;
										{	/* Object/class.scm 231 */
											BgL_objectz00_bglt BgL_arg1807z00_2225;

											{	/* Object/class.scm 231 */
												obj_t BgL_tmpz00_5501;

												BgL_tmpz00_5501 =
													((obj_t) ((BgL_objectz00_bglt) BgL_classz00_274));
												BgL_arg1807z00_2225 =
													(BgL_objectz00_bglt) (BgL_tmpz00_5501);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Object/class.scm 231 */
													long BgL_idxz00_2231;

													BgL_idxz00_2231 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2225);
													BgL_test2348z00_5500 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_2231 + 2L)) == BgL_classz00_2223);
												}
											else
												{	/* Object/class.scm 231 */
													bool_t BgL_res1922z00_2256;

													{	/* Object/class.scm 231 */
														obj_t BgL_oclassz00_2239;

														{	/* Object/class.scm 231 */
															obj_t BgL_arg1815z00_2247;
															long BgL_arg1816z00_2248;

															BgL_arg1815z00_2247 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Object/class.scm 231 */
																long BgL_arg1817z00_2249;

																BgL_arg1817z00_2249 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2225);
																BgL_arg1816z00_2248 =
																	(BgL_arg1817z00_2249 - OBJECT_TYPE);
															}
															BgL_oclassz00_2239 =
																VECTOR_REF(BgL_arg1815z00_2247,
																BgL_arg1816z00_2248);
														}
														{	/* Object/class.scm 231 */
															bool_t BgL__ortest_1115z00_2240;

															BgL__ortest_1115z00_2240 =
																(BgL_classz00_2223 == BgL_oclassz00_2239);
															if (BgL__ortest_1115z00_2240)
																{	/* Object/class.scm 231 */
																	BgL_res1922z00_2256 =
																		BgL__ortest_1115z00_2240;
																}
															else
																{	/* Object/class.scm 231 */
																	long BgL_odepthz00_2241;

																	{	/* Object/class.scm 231 */
																		obj_t BgL_arg1804z00_2242;

																		BgL_arg1804z00_2242 = (BgL_oclassz00_2239);
																		BgL_odepthz00_2241 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_2242);
																	}
																	if ((2L < BgL_odepthz00_2241))
																		{	/* Object/class.scm 231 */
																			obj_t BgL_arg1802z00_2244;

																			{	/* Object/class.scm 231 */
																				obj_t BgL_arg1803z00_2245;

																				BgL_arg1803z00_2245 =
																					(BgL_oclassz00_2239);
																				BgL_arg1802z00_2244 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_2245, 2L);
																			}
																			BgL_res1922z00_2256 =
																				(BgL_arg1802z00_2244 ==
																				BgL_classz00_2223);
																		}
																	else
																		{	/* Object/class.scm 231 */
																			BgL_res1922z00_2256 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2348z00_5500 = BgL_res1922z00_2256;
												}
										}
									}
									if (BgL_test2348z00_5500)
										{	/* Object/class.scm 231 */
											obj_t BgL_tmpz00_5524;

											{
												BgL_tclassz00_bglt BgL_auxz00_5525;

												{
													obj_t BgL_auxz00_5526;

													{	/* Object/class.scm 231 */
														BgL_objectz00_bglt BgL_tmpz00_5527;

														BgL_tmpz00_5527 =
															((BgL_objectz00_bglt) BgL_classz00_274);
														BgL_auxz00_5526 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_5527);
													}
													BgL_auxz00_5525 =
														((BgL_tclassz00_bglt) BgL_auxz00_5526);
												}
												BgL_tmpz00_5524 =
													(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_5525))->
													BgL_wideningz00);
											}
											BgL_res1923z00_2259 = CBOOL(BgL_tmpz00_5524);
										}
									else
										{	/* Object/class.scm 231 */
											BgL_res1923z00_2259 = ((bool_t) 0);
										}
								}
								BgL_test2347z00_5499 = BgL_res1923z00_2259;
							}
							if (BgL_test2347z00_5499)
								{	/* Object/class.scm 378 */
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string1966z00zzobject_classz00,
										BGl_string1967z00zzobject_classz00, BgL_srczd2defzd2_275);
									return ((bool_t) 0);
								}
							else
								{	/* Object/class.scm 385 */
									bool_t BgL_test2352z00_5534;

									{	/* Object/class.scm 385 */
										bool_t BgL_res1925z00_2297;

										{	/* Object/class.scm 231 */
											bool_t BgL_test2353z00_5535;

											{	/* Object/class.scm 231 */
												obj_t BgL_classz00_2261;

												BgL_classz00_2261 = BGl_tclassz00zzobject_classz00;
												if (BGL_OBJECTP(BgL_superz00_971))
													{	/* Object/class.scm 231 */
														BgL_objectz00_bglt BgL_arg1807z00_2263;

														BgL_arg1807z00_2263 =
															(BgL_objectz00_bglt) (BgL_superz00_971);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Object/class.scm 231 */
																long BgL_idxz00_2269;

																BgL_idxz00_2269 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_2263);
																BgL_test2353z00_5535 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_2269 + 2L)) ==
																	BgL_classz00_2261);
															}
														else
															{	/* Object/class.scm 231 */
																bool_t BgL_res1924z00_2294;

																{	/* Object/class.scm 231 */
																	obj_t BgL_oclassz00_2277;

																	{	/* Object/class.scm 231 */
																		obj_t BgL_arg1815z00_2285;
																		long BgL_arg1816z00_2286;

																		BgL_arg1815z00_2285 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Object/class.scm 231 */
																			long BgL_arg1817z00_2287;

																			BgL_arg1817z00_2287 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_2263);
																			BgL_arg1816z00_2286 =
																				(BgL_arg1817z00_2287 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_2277 =
																			VECTOR_REF(BgL_arg1815z00_2285,
																			BgL_arg1816z00_2286);
																	}
																	{	/* Object/class.scm 231 */
																		bool_t BgL__ortest_1115z00_2278;

																		BgL__ortest_1115z00_2278 =
																			(BgL_classz00_2261 == BgL_oclassz00_2277);
																		if (BgL__ortest_1115z00_2278)
																			{	/* Object/class.scm 231 */
																				BgL_res1924z00_2294 =
																					BgL__ortest_1115z00_2278;
																			}
																		else
																			{	/* Object/class.scm 231 */
																				long BgL_odepthz00_2279;

																				{	/* Object/class.scm 231 */
																					obj_t BgL_arg1804z00_2280;

																					BgL_arg1804z00_2280 =
																						(BgL_oclassz00_2277);
																					BgL_odepthz00_2279 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_2280);
																				}
																				if ((2L < BgL_odepthz00_2279))
																					{	/* Object/class.scm 231 */
																						obj_t BgL_arg1802z00_2282;

																						{	/* Object/class.scm 231 */
																							obj_t BgL_arg1803z00_2283;

																							BgL_arg1803z00_2283 =
																								(BgL_oclassz00_2277);
																							BgL_arg1802z00_2282 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_2283, 2L);
																						}
																						BgL_res1924z00_2294 =
																							(BgL_arg1802z00_2282 ==
																							BgL_classz00_2261);
																					}
																				else
																					{	/* Object/class.scm 231 */
																						BgL_res1924z00_2294 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2353z00_5535 = BgL_res1924z00_2294;
															}
													}
												else
													{	/* Object/class.scm 231 */
														BgL_test2353z00_5535 = ((bool_t) 0);
													}
											}
											if (BgL_test2353z00_5535)
												{	/* Object/class.scm 231 */
													obj_t BgL_tmpz00_5558;

													{
														BgL_tclassz00_bglt BgL_auxz00_5559;

														{
															obj_t BgL_auxz00_5560;

															{	/* Object/class.scm 231 */
																BgL_objectz00_bglt BgL_tmpz00_5561;

																BgL_tmpz00_5561 =
																	((BgL_objectz00_bglt)
																	((BgL_typez00_bglt) BgL_superz00_971));
																BgL_auxz00_5560 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5561);
															}
															BgL_auxz00_5559 =
																((BgL_tclassz00_bglt) BgL_auxz00_5560);
														}
														BgL_tmpz00_5558 =
															(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_5559))->
															BgL_wideningz00);
													}
													BgL_res1925z00_2297 = CBOOL(BgL_tmpz00_5558);
												}
											else
												{	/* Object/class.scm 231 */
													BgL_res1925z00_2297 = ((bool_t) 0);
												}
										}
										BgL_test2352z00_5534 = BgL_res1925z00_2297;
									}
									if (BgL_test2352z00_5534)
										{	/* Object/class.scm 385 */
											{	/* Object/class.scm 387 */
												obj_t BgL_arg1540z00_983;
												obj_t BgL_arg1544z00_984;

												{	/* Object/class.scm 387 */
													obj_t BgL_arg1546z00_986;

													BgL_arg1546z00_986 =
														(((BgL_typez00_bglt) COBJECT(
																((BgL_typez00_bglt) BgL_superz00_971)))->
														BgL_idz00);
													{	/* Object/class.scm 387 */
														obj_t BgL_arg1455z00_2300;

														BgL_arg1455z00_2300 =
															SYMBOL_TO_STRING(BgL_arg1546z00_986);
														BgL_arg1540z00_983 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1455z00_2300);
													}
												}
												{	/* Object/class.scm 388 */
													obj_t BgL_list1547z00_987;

													BgL_list1547z00_987 =
														MAKE_YOUNG_PAIR(BgL_classzd2idzd2_972, BNIL);
													BgL_arg1544z00_984 =
														BGl_formatz00zz__r4_output_6_10_3z00
														(BGl_string1968z00zzobject_classz00,
														BgL_list1547z00_987);
												}
												{	/* Object/class.scm 387 */
													obj_t BgL_list1545z00_985;

													BgL_list1545z00_985 =
														MAKE_YOUNG_PAIR(BGl_typez00zztype_typez00, BNIL);
													BGl_userzd2errorzd2zztools_errorz00
														(BgL_arg1540z00_983, BgL_arg1544z00_984,
														BgL_srczd2defzd2_275, BgL_list1545z00_985);
												}
											}
											return ((bool_t) 0);
										}
									else
										{	/* Object/class.scm 392 */
											bool_t BgL_test2358z00_5576;

											{	/* Object/class.scm 223 */
												bool_t BgL_test2359z00_5577;

												{	/* Object/class.scm 223 */
													obj_t BgL_classz00_2302;

													BgL_classz00_2302 = BGl_tclassz00zzobject_classz00;
													if (BGL_OBJECTP(BgL_superz00_971))
														{	/* Object/class.scm 223 */
															BgL_objectz00_bglt BgL_arg1807z00_2304;

															BgL_arg1807z00_2304 =
																(BgL_objectz00_bglt) (BgL_superz00_971);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Object/class.scm 223 */
																	long BgL_idxz00_2310;

																	BgL_idxz00_2310 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_2304);
																	BgL_test2359z00_5577 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_2310 + 2L)) ==
																		BgL_classz00_2302);
																}
															else
																{	/* Object/class.scm 223 */
																	bool_t BgL_res1926z00_2335;

																	{	/* Object/class.scm 223 */
																		obj_t BgL_oclassz00_2318;

																		{	/* Object/class.scm 223 */
																			obj_t BgL_arg1815z00_2326;
																			long BgL_arg1816z00_2327;

																			BgL_arg1815z00_2326 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Object/class.scm 223 */
																				long BgL_arg1817z00_2328;

																				BgL_arg1817z00_2328 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_2304);
																				BgL_arg1816z00_2327 =
																					(BgL_arg1817z00_2328 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_2318 =
																				VECTOR_REF(BgL_arg1815z00_2326,
																				BgL_arg1816z00_2327);
																		}
																		{	/* Object/class.scm 223 */
																			bool_t BgL__ortest_1115z00_2319;

																			BgL__ortest_1115z00_2319 =
																				(BgL_classz00_2302 ==
																				BgL_oclassz00_2318);
																			if (BgL__ortest_1115z00_2319)
																				{	/* Object/class.scm 223 */
																					BgL_res1926z00_2335 =
																						BgL__ortest_1115z00_2319;
																				}
																			else
																				{	/* Object/class.scm 223 */
																					long BgL_odepthz00_2320;

																					{	/* Object/class.scm 223 */
																						obj_t BgL_arg1804z00_2321;

																						BgL_arg1804z00_2321 =
																							(BgL_oclassz00_2318);
																						BgL_odepthz00_2320 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_2321);
																					}
																					if ((2L < BgL_odepthz00_2320))
																						{	/* Object/class.scm 223 */
																							obj_t BgL_arg1802z00_2323;

																							{	/* Object/class.scm 223 */
																								obj_t BgL_arg1803z00_2324;

																								BgL_arg1803z00_2324 =
																									(BgL_oclassz00_2318);
																								BgL_arg1802z00_2323 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_2324, 2L);
																							}
																							BgL_res1926z00_2335 =
																								(BgL_arg1802z00_2323 ==
																								BgL_classz00_2302);
																						}
																					else
																						{	/* Object/class.scm 223 */
																							BgL_res1926z00_2335 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2359z00_5577 = BgL_res1926z00_2335;
																}
														}
													else
														{	/* Object/class.scm 223 */
															BgL_test2359z00_5577 = ((bool_t) 0);
														}
												}
												if (BgL_test2359z00_5577)
													{
														BgL_tclassz00_bglt BgL_auxz00_5600;

														{
															obj_t BgL_auxz00_5601;

															{	/* Object/class.scm 223 */
																BgL_objectz00_bglt BgL_tmpz00_5602;

																BgL_tmpz00_5602 =
																	((BgL_objectz00_bglt)
																	((BgL_typez00_bglt) BgL_superz00_971));
																BgL_auxz00_5601 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5602);
															}
															BgL_auxz00_5600 =
																((BgL_tclassz00_bglt) BgL_auxz00_5601);
														}
														BgL_test2358z00_5576 =
															(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_5600))->
															BgL_finalzf3zf3);
													}
												else
													{	/* Object/class.scm 223 */
														BgL_test2358z00_5576 = ((bool_t) 0);
													}
											}
											if (BgL_test2358z00_5576)
												{	/* Object/class.scm 392 */
													{	/* Object/class.scm 394 */
														obj_t BgL_arg1552z00_989;

														{	/* Object/class.scm 394 */
															obj_t BgL_arg1559z00_991;

															BgL_arg1559z00_991 =
																(((BgL_typez00_bglt) COBJECT(
																		((BgL_typez00_bglt) BgL_superz00_971)))->
																BgL_idz00);
															{	/* Object/class.scm 394 */
																obj_t BgL_arg1455z00_2340;

																BgL_arg1455z00_2340 =
																	SYMBOL_TO_STRING(BgL_arg1559z00_991);
																BgL_arg1552z00_989 =
																	BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																	(BgL_arg1455z00_2340);
															}
														}
														{	/* Object/class.scm 394 */
															obj_t BgL_list1553z00_990;

															BgL_list1553z00_990 =
																MAKE_YOUNG_PAIR(BGl_typez00zztype_typez00,
																BNIL);
															BGl_userzd2errorzd2zztools_errorz00
																(BgL_arg1552z00_989,
																BGl_string1969z00zzobject_classz00,
																BgL_srczd2defzd2_275, BgL_list1553z00_990);
														}
													}
													return ((bool_t) 0);
												}
											else
												{	/* Object/class.scm 392 */
													return ((bool_t) 1);
												}
										}
								}
						}
				}
			}
		}

	}



/* check-wide-class-declaration? */
	obj_t
		BGl_checkzd2widezd2classzd2declarationzf3z21zzobject_classz00
		(BgL_typez00_bglt BgL_classz00_276, obj_t BgL_srczd2defzd2_277)
	{
		{	/* Object/class.scm 409 */
			{	/* Object/class.scm 410 */
				obj_t BgL_superz00_994;

				{
					BgL_tclassz00_bglt BgL_auxz00_5614;

					{
						obj_t BgL_auxz00_5615;

						{	/* Object/class.scm 410 */
							BgL_objectz00_bglt BgL_tmpz00_5616;

							BgL_tmpz00_5616 = ((BgL_objectz00_bglt) BgL_classz00_276);
							BgL_auxz00_5615 = BGL_OBJECT_WIDENING(BgL_tmpz00_5616);
						}
						BgL_auxz00_5614 = ((BgL_tclassz00_bglt) BgL_auxz00_5615);
					}
					BgL_superz00_994 =
						(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_5614))->
						BgL_itszd2superzd2);
				}
				{	/* Object/class.scm 410 */
					obj_t BgL_classzd2idzd2_995;

					BgL_classzd2idzd2_995 =
						(((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_classz00_276)))->BgL_idz00);
					{	/* Object/class.scm 411 */

						{	/* Object/class.scm 415 */
							bool_t BgL_test2364z00_5623;

							{	/* Object/class.scm 415 */
								bool_t BgL_test2365z00_5624;

								{	/* Object/class.scm 415 */
									obj_t BgL_classz00_2344;

									BgL_classz00_2344 = BGl_typez00zztype_typez00;
									if (BGL_OBJECTP(BgL_superz00_994))
										{	/* Object/class.scm 415 */
											BgL_objectz00_bglt BgL_arg1807z00_2346;

											BgL_arg1807z00_2346 =
												(BgL_objectz00_bglt) (BgL_superz00_994);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Object/class.scm 415 */
													long BgL_idxz00_2352;

													BgL_idxz00_2352 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2346);
													BgL_test2365z00_5624 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_2352 + 1L)) == BgL_classz00_2344);
												}
											else
												{	/* Object/class.scm 415 */
													bool_t BgL_res1927z00_2377;

													{	/* Object/class.scm 415 */
														obj_t BgL_oclassz00_2360;

														{	/* Object/class.scm 415 */
															obj_t BgL_arg1815z00_2368;
															long BgL_arg1816z00_2369;

															BgL_arg1815z00_2368 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Object/class.scm 415 */
																long BgL_arg1817z00_2370;

																BgL_arg1817z00_2370 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2346);
																BgL_arg1816z00_2369 =
																	(BgL_arg1817z00_2370 - OBJECT_TYPE);
															}
															BgL_oclassz00_2360 =
																VECTOR_REF(BgL_arg1815z00_2368,
																BgL_arg1816z00_2369);
														}
														{	/* Object/class.scm 415 */
															bool_t BgL__ortest_1115z00_2361;

															BgL__ortest_1115z00_2361 =
																(BgL_classz00_2344 == BgL_oclassz00_2360);
															if (BgL__ortest_1115z00_2361)
																{	/* Object/class.scm 415 */
																	BgL_res1927z00_2377 =
																		BgL__ortest_1115z00_2361;
																}
															else
																{	/* Object/class.scm 415 */
																	long BgL_odepthz00_2362;

																	{	/* Object/class.scm 415 */
																		obj_t BgL_arg1804z00_2363;

																		BgL_arg1804z00_2363 = (BgL_oclassz00_2360);
																		BgL_odepthz00_2362 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_2363);
																	}
																	if ((1L < BgL_odepthz00_2362))
																		{	/* Object/class.scm 415 */
																			obj_t BgL_arg1802z00_2365;

																			{	/* Object/class.scm 415 */
																				obj_t BgL_arg1803z00_2366;

																				BgL_arg1803z00_2366 =
																					(BgL_oclassz00_2360);
																				BgL_arg1802z00_2365 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_2366, 1L);
																			}
																			BgL_res1927z00_2377 =
																				(BgL_arg1802z00_2365 ==
																				BgL_classz00_2344);
																		}
																	else
																		{	/* Object/class.scm 415 */
																			BgL_res1927z00_2377 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2365z00_5624 = BgL_res1927z00_2377;
												}
										}
									else
										{	/* Object/class.scm 415 */
											BgL_test2365z00_5624 = ((bool_t) 0);
										}
								}
								if (BgL_test2365z00_5624)
									{	/* Object/class.scm 415 */
										bool_t BgL_test2370z00_5647;

										{	/* Object/class.scm 415 */
											obj_t BgL_classz00_2378;

											BgL_classz00_2378 = BGl_tclassz00zzobject_classz00;
											if (BGL_OBJECTP(BgL_superz00_994))
												{	/* Object/class.scm 415 */
													BgL_objectz00_bglt BgL_arg1807z00_2380;

													BgL_arg1807z00_2380 =
														(BgL_objectz00_bglt) (BgL_superz00_994);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Object/class.scm 415 */
															long BgL_idxz00_2386;

															BgL_idxz00_2386 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2380);
															BgL_test2370z00_5647 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_2386 + 2L)) == BgL_classz00_2378);
														}
													else
														{	/* Object/class.scm 415 */
															bool_t BgL_res1928z00_2411;

															{	/* Object/class.scm 415 */
																obj_t BgL_oclassz00_2394;

																{	/* Object/class.scm 415 */
																	obj_t BgL_arg1815z00_2402;
																	long BgL_arg1816z00_2403;

																	BgL_arg1815z00_2402 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Object/class.scm 415 */
																		long BgL_arg1817z00_2404;

																		BgL_arg1817z00_2404 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2380);
																		BgL_arg1816z00_2403 =
																			(BgL_arg1817z00_2404 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_2394 =
																		VECTOR_REF(BgL_arg1815z00_2402,
																		BgL_arg1816z00_2403);
																}
																{	/* Object/class.scm 415 */
																	bool_t BgL__ortest_1115z00_2395;

																	BgL__ortest_1115z00_2395 =
																		(BgL_classz00_2378 == BgL_oclassz00_2394);
																	if (BgL__ortest_1115z00_2395)
																		{	/* Object/class.scm 415 */
																			BgL_res1928z00_2411 =
																				BgL__ortest_1115z00_2395;
																		}
																	else
																		{	/* Object/class.scm 415 */
																			long BgL_odepthz00_2396;

																			{	/* Object/class.scm 415 */
																				obj_t BgL_arg1804z00_2397;

																				BgL_arg1804z00_2397 =
																					(BgL_oclassz00_2394);
																				BgL_odepthz00_2396 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_2397);
																			}
																			if ((2L < BgL_odepthz00_2396))
																				{	/* Object/class.scm 415 */
																					obj_t BgL_arg1802z00_2399;

																					{	/* Object/class.scm 415 */
																						obj_t BgL_arg1803z00_2400;

																						BgL_arg1803z00_2400 =
																							(BgL_oclassz00_2394);
																						BgL_arg1802z00_2399 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_2400, 2L);
																					}
																					BgL_res1928z00_2411 =
																						(BgL_arg1802z00_2399 ==
																						BgL_classz00_2378);
																				}
																			else
																				{	/* Object/class.scm 415 */
																					BgL_res1928z00_2411 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2370z00_5647 = BgL_res1928z00_2411;
														}
												}
											else
												{	/* Object/class.scm 415 */
													BgL_test2370z00_5647 = ((bool_t) 0);
												}
										}
										if (BgL_test2370z00_5647)
											{	/* Object/class.scm 415 */
												BgL_test2364z00_5623 = ((bool_t) 0);
											}
										else
											{	/* Object/class.scm 415 */
												BgL_test2364z00_5623 = ((bool_t) 1);
											}
									}
								else
									{	/* Object/class.scm 415 */
										BgL_test2364z00_5623 = ((bool_t) 0);
									}
							}
							if (BgL_test2364z00_5623)
								{	/* Object/class.scm 416 */
									obj_t BgL_arg1564z00_999;
									obj_t BgL_arg1565z00_1000;

									{	/* Object/class.scm 416 */
										obj_t BgL_arg1571z00_1002;

										BgL_arg1571z00_1002 =
											(((BgL_typez00_bglt) COBJECT(
													((BgL_typez00_bglt) BgL_superz00_994)))->BgL_idz00);
										{	/* Object/class.scm 416 */
											obj_t BgL_arg1455z00_2414;

											BgL_arg1455z00_2414 =
												SYMBOL_TO_STRING(BgL_arg1571z00_1002);
											BgL_arg1564z00_999 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_2414);
										}
									}
									{	/* Object/class.scm 417 */
										obj_t BgL_list1572z00_1003;

										BgL_list1572z00_1003 =
											MAKE_YOUNG_PAIR(BgL_classzd2idzd2_995, BNIL);
										BgL_arg1565z00_1000 =
											BGl_formatz00zz__r4_output_6_10_3z00
											(BGl_string1965z00zzobject_classz00,
											BgL_list1572z00_1003);
									}
									{	/* Object/class.scm 416 */
										obj_t BgL_list1566z00_1001;

										BgL_list1566z00_1001 =
											MAKE_YOUNG_PAIR(BGl_typez00zztype_typez00, BNIL);
										return
											BGl_userzd2errorzd2zztools_errorz00(BgL_arg1564z00_999,
											BgL_arg1565z00_1000, BgL_srczd2defzd2_277,
											BgL_list1566z00_1001);
									}
								}
							else
								{	/* Object/class.scm 420 */
									bool_t BgL_test2375z00_5678;

									{	/* Object/class.scm 420 */
										bool_t BgL_res1930z00_2452;

										{	/* Object/class.scm 231 */
											bool_t BgL_test2376z00_5679;

											{	/* Object/class.scm 231 */
												obj_t BgL_classz00_2416;

												BgL_classz00_2416 = BGl_tclassz00zzobject_classz00;
												{	/* Object/class.scm 231 */
													BgL_objectz00_bglt BgL_arg1807z00_2418;

													{	/* Object/class.scm 231 */
														obj_t BgL_tmpz00_5680;

														BgL_tmpz00_5680 =
															((obj_t) ((BgL_objectz00_bglt) BgL_classz00_276));
														BgL_arg1807z00_2418 =
															(BgL_objectz00_bglt) (BgL_tmpz00_5680);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Object/class.scm 231 */
															long BgL_idxz00_2424;

															BgL_idxz00_2424 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2418);
															BgL_test2376z00_5679 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_2424 + 2L)) == BgL_classz00_2416);
														}
													else
														{	/* Object/class.scm 231 */
															bool_t BgL_res1929z00_2449;

															{	/* Object/class.scm 231 */
																obj_t BgL_oclassz00_2432;

																{	/* Object/class.scm 231 */
																	obj_t BgL_arg1815z00_2440;
																	long BgL_arg1816z00_2441;

																	BgL_arg1815z00_2440 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Object/class.scm 231 */
																		long BgL_arg1817z00_2442;

																		BgL_arg1817z00_2442 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2418);
																		BgL_arg1816z00_2441 =
																			(BgL_arg1817z00_2442 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_2432 =
																		VECTOR_REF(BgL_arg1815z00_2440,
																		BgL_arg1816z00_2441);
																}
																{	/* Object/class.scm 231 */
																	bool_t BgL__ortest_1115z00_2433;

																	BgL__ortest_1115z00_2433 =
																		(BgL_classz00_2416 == BgL_oclassz00_2432);
																	if (BgL__ortest_1115z00_2433)
																		{	/* Object/class.scm 231 */
																			BgL_res1929z00_2449 =
																				BgL__ortest_1115z00_2433;
																		}
																	else
																		{	/* Object/class.scm 231 */
																			long BgL_odepthz00_2434;

																			{	/* Object/class.scm 231 */
																				obj_t BgL_arg1804z00_2435;

																				BgL_arg1804z00_2435 =
																					(BgL_oclassz00_2432);
																				BgL_odepthz00_2434 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_2435);
																			}
																			if ((2L < BgL_odepthz00_2434))
																				{	/* Object/class.scm 231 */
																					obj_t BgL_arg1802z00_2437;

																					{	/* Object/class.scm 231 */
																						obj_t BgL_arg1803z00_2438;

																						BgL_arg1803z00_2438 =
																							(BgL_oclassz00_2432);
																						BgL_arg1802z00_2437 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_2438, 2L);
																					}
																					BgL_res1929z00_2449 =
																						(BgL_arg1802z00_2437 ==
																						BgL_classz00_2416);
																				}
																			else
																				{	/* Object/class.scm 231 */
																					BgL_res1929z00_2449 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2376z00_5679 = BgL_res1929z00_2449;
														}
												}
											}
											if (BgL_test2376z00_5679)
												{	/* Object/class.scm 231 */
													obj_t BgL_tmpz00_5703;

													{
														BgL_tclassz00_bglt BgL_auxz00_5704;

														{
															obj_t BgL_auxz00_5705;

															{	/* Object/class.scm 231 */
																BgL_objectz00_bglt BgL_tmpz00_5706;

																BgL_tmpz00_5706 =
																	((BgL_objectz00_bglt) BgL_classz00_276);
																BgL_auxz00_5705 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5706);
															}
															BgL_auxz00_5704 =
																((BgL_tclassz00_bglt) BgL_auxz00_5705);
														}
														BgL_tmpz00_5703 =
															(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_5704))->
															BgL_wideningz00);
													}
													BgL_res1930z00_2452 = CBOOL(BgL_tmpz00_5703);
												}
											else
												{	/* Object/class.scm 231 */
													BgL_res1930z00_2452 = ((bool_t) 0);
												}
										}
										BgL_test2375z00_5678 = BgL_res1930z00_2452;
									}
									if (BgL_test2375z00_5678)
										{	/* Object/class.scm 426 */
											bool_t BgL_test2380z00_5712;

											{	/* Object/class.scm 426 */
												bool_t BgL_res1932z00_2490;

												{	/* Object/class.scm 231 */
													bool_t BgL_test2381z00_5713;

													{	/* Object/class.scm 231 */
														obj_t BgL_classz00_2454;

														BgL_classz00_2454 = BGl_tclassz00zzobject_classz00;
														if (BGL_OBJECTP(BgL_superz00_994))
															{	/* Object/class.scm 231 */
																BgL_objectz00_bglt BgL_arg1807z00_2456;

																BgL_arg1807z00_2456 =
																	(BgL_objectz00_bglt) (BgL_superz00_994);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Object/class.scm 231 */
																		long BgL_idxz00_2462;

																		BgL_idxz00_2462 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_2456);
																		BgL_test2381z00_5713 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_2462 + 2L)) ==
																			BgL_classz00_2454);
																	}
																else
																	{	/* Object/class.scm 231 */
																		bool_t BgL_res1931z00_2487;

																		{	/* Object/class.scm 231 */
																			obj_t BgL_oclassz00_2470;

																			{	/* Object/class.scm 231 */
																				obj_t BgL_arg1815z00_2478;
																				long BgL_arg1816z00_2479;

																				BgL_arg1815z00_2478 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Object/class.scm 231 */
																					long BgL_arg1817z00_2480;

																					BgL_arg1817z00_2480 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_2456);
																					BgL_arg1816z00_2479 =
																						(BgL_arg1817z00_2480 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_2470 =
																					VECTOR_REF(BgL_arg1815z00_2478,
																					BgL_arg1816z00_2479);
																			}
																			{	/* Object/class.scm 231 */
																				bool_t BgL__ortest_1115z00_2471;

																				BgL__ortest_1115z00_2471 =
																					(BgL_classz00_2454 ==
																					BgL_oclassz00_2470);
																				if (BgL__ortest_1115z00_2471)
																					{	/* Object/class.scm 231 */
																						BgL_res1931z00_2487 =
																							BgL__ortest_1115z00_2471;
																					}
																				else
																					{	/* Object/class.scm 231 */
																						long BgL_odepthz00_2472;

																						{	/* Object/class.scm 231 */
																							obj_t BgL_arg1804z00_2473;

																							BgL_arg1804z00_2473 =
																								(BgL_oclassz00_2470);
																							BgL_odepthz00_2472 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_2473);
																						}
																						if ((2L < BgL_odepthz00_2472))
																							{	/* Object/class.scm 231 */
																								obj_t BgL_arg1802z00_2475;

																								{	/* Object/class.scm 231 */
																									obj_t BgL_arg1803z00_2476;

																									BgL_arg1803z00_2476 =
																										(BgL_oclassz00_2470);
																									BgL_arg1802z00_2475 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_2476, 2L);
																								}
																								BgL_res1931z00_2487 =
																									(BgL_arg1802z00_2475 ==
																									BgL_classz00_2454);
																							}
																						else
																							{	/* Object/class.scm 231 */
																								BgL_res1931z00_2487 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2381z00_5713 = BgL_res1931z00_2487;
																	}
															}
														else
															{	/* Object/class.scm 231 */
																BgL_test2381z00_5713 = ((bool_t) 0);
															}
													}
													if (BgL_test2381z00_5713)
														{	/* Object/class.scm 231 */
															obj_t BgL_tmpz00_5736;

															{
																BgL_tclassz00_bglt BgL_auxz00_5737;

																{
																	obj_t BgL_auxz00_5738;

																	{	/* Object/class.scm 231 */
																		BgL_objectz00_bglt BgL_tmpz00_5739;

																		BgL_tmpz00_5739 =
																			((BgL_objectz00_bglt)
																			((BgL_typez00_bglt) BgL_superz00_994));
																		BgL_auxz00_5738 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_5739);
																	}
																	BgL_auxz00_5737 =
																		((BgL_tclassz00_bglt) BgL_auxz00_5738);
																}
																BgL_tmpz00_5736 =
																	(((BgL_tclassz00_bglt)
																		COBJECT(BgL_auxz00_5737))->BgL_wideningz00);
															}
															BgL_res1932z00_2490 = CBOOL(BgL_tmpz00_5736);
														}
													else
														{	/* Object/class.scm 231 */
															BgL_res1932z00_2490 = ((bool_t) 0);
														}
												}
												BgL_test2380z00_5712 = BgL_res1932z00_2490;
											}
											if (BgL_test2380z00_5712)
												{	/* Object/class.scm 428 */
													obj_t BgL_arg1575z00_1006;
													obj_t BgL_arg1576z00_1007;

													{	/* Object/class.scm 428 */
														obj_t BgL_arg1584z00_1009;

														BgL_arg1584z00_1009 =
															(((BgL_typez00_bglt) COBJECT(
																	((BgL_typez00_bglt) BgL_superz00_994)))->
															BgL_idz00);
														{	/* Object/class.scm 428 */
															obj_t BgL_arg1455z00_2493;

															BgL_arg1455z00_2493 =
																SYMBOL_TO_STRING(BgL_arg1584z00_1009);
															BgL_arg1575z00_1006 =
																BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																(BgL_arg1455z00_2493);
														}
													}
													{	/* Object/class.scm 429 */
														obj_t BgL_list1585z00_1010;

														BgL_list1585z00_1010 =
															MAKE_YOUNG_PAIR(BgL_classzd2idzd2_995, BNIL);
														BgL_arg1576z00_1007 =
															BGl_formatz00zz__r4_output_6_10_3z00
															(BGl_string1968z00zzobject_classz00,
															BgL_list1585z00_1010);
													}
													{	/* Object/class.scm 428 */
														obj_t BgL_list1577z00_1008;

														BgL_list1577z00_1008 =
															MAKE_YOUNG_PAIR(BGl_typez00zztype_typez00, BNIL);
														return
															BGl_userzd2errorzd2zztools_errorz00
															(BgL_arg1575z00_1006, BgL_arg1576z00_1007,
															BgL_srczd2defzd2_277, BgL_list1577z00_1008);
													}
												}
											else
												{	/* Object/class.scm 432 */
													bool_t BgL_test2386z00_5754;

													{	/* Object/class.scm 223 */
														bool_t BgL_test2387z00_5755;

														{	/* Object/class.scm 223 */
															obj_t BgL_classz00_2495;

															BgL_classz00_2495 =
																BGl_tclassz00zzobject_classz00;
															if (BGL_OBJECTP(BgL_superz00_994))
																{	/* Object/class.scm 223 */
																	BgL_objectz00_bglt BgL_arg1807z00_2497;

																	BgL_arg1807z00_2497 =
																		(BgL_objectz00_bglt) (BgL_superz00_994);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Object/class.scm 223 */
																			long BgL_idxz00_2503;

																			BgL_idxz00_2503 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_2497);
																			BgL_test2387z00_5755 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_2503 + 2L)) ==
																				BgL_classz00_2495);
																		}
																	else
																		{	/* Object/class.scm 223 */
																			bool_t BgL_res1933z00_2528;

																			{	/* Object/class.scm 223 */
																				obj_t BgL_oclassz00_2511;

																				{	/* Object/class.scm 223 */
																					obj_t BgL_arg1815z00_2519;
																					long BgL_arg1816z00_2520;

																					BgL_arg1815z00_2519 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Object/class.scm 223 */
																						long BgL_arg1817z00_2521;

																						BgL_arg1817z00_2521 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_2497);
																						BgL_arg1816z00_2520 =
																							(BgL_arg1817z00_2521 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_2511 =
																						VECTOR_REF(BgL_arg1815z00_2519,
																						BgL_arg1816z00_2520);
																				}
																				{	/* Object/class.scm 223 */
																					bool_t BgL__ortest_1115z00_2512;

																					BgL__ortest_1115z00_2512 =
																						(BgL_classz00_2495 ==
																						BgL_oclassz00_2511);
																					if (BgL__ortest_1115z00_2512)
																						{	/* Object/class.scm 223 */
																							BgL_res1933z00_2528 =
																								BgL__ortest_1115z00_2512;
																						}
																					else
																						{	/* Object/class.scm 223 */
																							long BgL_odepthz00_2513;

																							{	/* Object/class.scm 223 */
																								obj_t BgL_arg1804z00_2514;

																								BgL_arg1804z00_2514 =
																									(BgL_oclassz00_2511);
																								BgL_odepthz00_2513 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_2514);
																							}
																							if ((2L < BgL_odepthz00_2513))
																								{	/* Object/class.scm 223 */
																									obj_t BgL_arg1802z00_2516;

																									{	/* Object/class.scm 223 */
																										obj_t BgL_arg1803z00_2517;

																										BgL_arg1803z00_2517 =
																											(BgL_oclassz00_2511);
																										BgL_arg1802z00_2516 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_2517, 2L);
																									}
																									BgL_res1933z00_2528 =
																										(BgL_arg1802z00_2516 ==
																										BgL_classz00_2495);
																								}
																							else
																								{	/* Object/class.scm 223 */
																									BgL_res1933z00_2528 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test2387z00_5755 =
																				BgL_res1933z00_2528;
																		}
																}
															else
																{	/* Object/class.scm 223 */
																	BgL_test2387z00_5755 = ((bool_t) 0);
																}
														}
														if (BgL_test2387z00_5755)
															{
																BgL_tclassz00_bglt BgL_auxz00_5778;

																{
																	obj_t BgL_auxz00_5779;

																	{	/* Object/class.scm 223 */
																		BgL_objectz00_bglt BgL_tmpz00_5780;

																		BgL_tmpz00_5780 =
																			((BgL_objectz00_bglt)
																			((BgL_typez00_bglt) BgL_superz00_994));
																		BgL_auxz00_5779 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_5780);
																	}
																	BgL_auxz00_5778 =
																		((BgL_tclassz00_bglt) BgL_auxz00_5779);
																}
																BgL_test2386z00_5754 =
																	(((BgL_tclassz00_bglt)
																		COBJECT(BgL_auxz00_5778))->BgL_finalzf3zf3);
															}
														else
															{	/* Object/class.scm 223 */
																BgL_test2386z00_5754 = ((bool_t) 0);
															}
													}
													if (BgL_test2386z00_5754)
														{	/* Object/class.scm 439 */
															bool_t BgL_test2392z00_5786;

															{	/* Object/class.scm 223 */
																bool_t BgL_test2393z00_5787;

																{	/* Object/class.scm 223 */
																	obj_t BgL_classz00_2532;

																	BgL_classz00_2532 =
																		BGl_tclassz00zzobject_classz00;
																	{	/* Object/class.scm 223 */
																		BgL_objectz00_bglt BgL_arg1807z00_2534;

																		{	/* Object/class.scm 223 */
																			obj_t BgL_tmpz00_5788;

																			BgL_tmpz00_5788 =
																				((obj_t)
																				((BgL_objectz00_bglt)
																					BgL_classz00_276));
																			BgL_arg1807z00_2534 =
																				(BgL_objectz00_bglt) (BgL_tmpz00_5788);
																		}
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Object/class.scm 223 */
																				long BgL_idxz00_2540;

																				BgL_idxz00_2540 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_2534);
																				BgL_test2393z00_5787 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_2540 + 2L)) ==
																					BgL_classz00_2532);
																			}
																		else
																			{	/* Object/class.scm 223 */
																				bool_t BgL_res1934z00_2565;

																				{	/* Object/class.scm 223 */
																					obj_t BgL_oclassz00_2548;

																					{	/* Object/class.scm 223 */
																						obj_t BgL_arg1815z00_2556;
																						long BgL_arg1816z00_2557;

																						BgL_arg1815z00_2556 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Object/class.scm 223 */
																							long BgL_arg1817z00_2558;

																							BgL_arg1817z00_2558 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_2534);
																							BgL_arg1816z00_2557 =
																								(BgL_arg1817z00_2558 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_2548 =
																							VECTOR_REF(BgL_arg1815z00_2556,
																							BgL_arg1816z00_2557);
																					}
																					{	/* Object/class.scm 223 */
																						bool_t BgL__ortest_1115z00_2549;

																						BgL__ortest_1115z00_2549 =
																							(BgL_classz00_2532 ==
																							BgL_oclassz00_2548);
																						if (BgL__ortest_1115z00_2549)
																							{	/* Object/class.scm 223 */
																								BgL_res1934z00_2565 =
																									BgL__ortest_1115z00_2549;
																							}
																						else
																							{	/* Object/class.scm 223 */
																								long BgL_odepthz00_2550;

																								{	/* Object/class.scm 223 */
																									obj_t BgL_arg1804z00_2551;

																									BgL_arg1804z00_2551 =
																										(BgL_oclassz00_2548);
																									BgL_odepthz00_2550 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_2551);
																								}
																								if ((2L < BgL_odepthz00_2550))
																									{	/* Object/class.scm 223 */
																										obj_t BgL_arg1802z00_2553;

																										{	/* Object/class.scm 223 */
																											obj_t BgL_arg1803z00_2554;

																											BgL_arg1803z00_2554 =
																												(BgL_oclassz00_2548);
																											BgL_arg1802z00_2553 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_2554,
																												2L);
																										}
																										BgL_res1934z00_2565 =
																											(BgL_arg1802z00_2553 ==
																											BgL_classz00_2532);
																									}
																								else
																									{	/* Object/class.scm 223 */
																										BgL_res1934z00_2565 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test2393z00_5787 =
																					BgL_res1934z00_2565;
																			}
																	}
																}
																if (BgL_test2393z00_5787)
																	{
																		BgL_tclassz00_bglt BgL_auxz00_5811;

																		{
																			obj_t BgL_auxz00_5812;

																			{	/* Object/class.scm 223 */
																				BgL_objectz00_bglt BgL_tmpz00_5813;

																				BgL_tmpz00_5813 =
																					((BgL_objectz00_bglt)
																					BgL_classz00_276);
																				BgL_auxz00_5812 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_5813);
																			}
																			BgL_auxz00_5811 =
																				((BgL_tclassz00_bglt) BgL_auxz00_5812);
																		}
																		BgL_test2392z00_5786 =
																			(((BgL_tclassz00_bglt)
																				COBJECT(BgL_auxz00_5811))->
																			BgL_finalzf3zf3);
																	}
																else
																	{	/* Object/class.scm 223 */
																		BgL_test2392z00_5786 = ((bool_t) 0);
																	}
															}
															if (BgL_test2392z00_5786)
																{	/* Object/class.scm 441 */
																	obj_t BgL_list1588z00_1013;

																	BgL_list1588z00_1013 =
																		MAKE_YOUNG_PAIR(BGl_typez00zztype_typez00,
																		BNIL);
																	return
																		BGl_userzd2errorzd2zztools_errorz00
																		(BgL_classzd2idzd2_995,
																		BGl_string1970z00zzobject_classz00,
																		BgL_srczd2defzd2_277, BgL_list1588z00_1013);
																}
															else
																{	/* Object/class.scm 439 */
																	return BTRUE;
																}
														}
													else
														{	/* Object/class.scm 434 */
															obj_t BgL_arg1589z00_1014;
															obj_t BgL_arg1591z00_1015;

															{	/* Object/class.scm 434 */
																obj_t BgL_arg1593z00_1017;

																BgL_arg1593z00_1017 =
																	(((BgL_typez00_bglt) COBJECT(
																			((BgL_typez00_bglt) BgL_superz00_994)))->
																	BgL_idz00);
																{	/* Object/class.scm 434 */
																	obj_t BgL_arg1455z00_2570;

																	BgL_arg1455z00_2570 =
																		SYMBOL_TO_STRING(BgL_arg1593z00_1017);
																	BgL_arg1589z00_1014 =
																		BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																		(BgL_arg1455z00_2570);
																}
															}
															{	/* Object/class.scm 435 */
																obj_t BgL_list1594z00_1018;

																BgL_list1594z00_1018 =
																	MAKE_YOUNG_PAIR(BgL_classzd2idzd2_995, BNIL);
																BgL_arg1591z00_1015 =
																	BGl_formatz00zz__r4_output_6_10_3z00
																	(BGl_string1971z00zzobject_classz00,
																	BgL_list1594z00_1018);
															}
															{	/* Object/class.scm 434 */
																obj_t BgL_list1592z00_1016;

																BgL_list1592z00_1016 =
																	MAKE_YOUNG_PAIR(BGl_typez00zztype_typez00,
																	BNIL);
																return
																	BGl_userzd2errorzd2zztools_errorz00
																	(BgL_arg1589z00_1014, BgL_arg1591z00_1015,
																	BgL_srczd2defzd2_277, BgL_list1592z00_1016);
															}
														}
												}
										}
									else
										{	/* Object/class.scm 420 */
											return
												BGl_internalzd2errorzd2zztools_errorz00
												(BGl_string1972z00zzobject_classz00,
												BGl_string1973z00zzobject_classz00,
												BgL_srczd2defzd2_277);
										}
								}
						}
					}
				}
			}
		}

	}



/* set-class-slots! */
	BGL_EXPORTED_DEF obj_t
		BGl_setzd2classzd2slotsz12z12zzobject_classz00(BgL_typez00_bglt
		BgL_classz00_278, obj_t BgL_classzd2defzd2_279, obj_t BgL_srczd2defzd2_280)
	{
		{	/* Object/class.scm 457 */
			{	/* Object/class.scm 458 */
				obj_t BgL_superz00_1021;

				{	/* Object/class.scm 458 */
					obj_t BgL_superz00_1027;

					{
						BgL_tclassz00_bglt BgL_auxz00_5829;

						{
							obj_t BgL_auxz00_5830;

							{	/* Object/class.scm 458 */
								BgL_objectz00_bglt BgL_tmpz00_5831;

								BgL_tmpz00_5831 = ((BgL_objectz00_bglt) BgL_classz00_278);
								BgL_auxz00_5830 = BGL_OBJECT_WIDENING(BgL_tmpz00_5831);
							}
							BgL_auxz00_5829 = ((BgL_tclassz00_bglt) BgL_auxz00_5830);
						}
						BgL_superz00_1027 =
							(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_5829))->
							BgL_itszd2superzd2);
					}
					if ((BgL_superz00_1027 == ((obj_t) BgL_classz00_278)))
						{	/* Object/class.scm 459 */
							BgL_superz00_1021 = BFALSE;
						}
					else
						{	/* Object/class.scm 459 */
							BgL_superz00_1021 = BgL_superz00_1027;
						}
				}
				{	/* Object/class.scm 458 */
					obj_t BgL_superzd2vnumzd2_1022;

					{	/* Object/class.scm 462 */
						bool_t BgL_test2398z00_5839;

						{	/* Object/class.scm 462 */
							obj_t BgL_classz00_2573;

							BgL_classz00_2573 = BGl_tclassz00zzobject_classz00;
							if (BGL_OBJECTP(BgL_superz00_1021))
								{	/* Object/class.scm 462 */
									BgL_objectz00_bglt BgL_arg1807z00_2575;

									BgL_arg1807z00_2575 =
										(BgL_objectz00_bglt) (BgL_superz00_1021);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Object/class.scm 462 */
											long BgL_idxz00_2581;

											BgL_idxz00_2581 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2575);
											BgL_test2398z00_5839 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2581 + 2L)) == BgL_classz00_2573);
										}
									else
										{	/* Object/class.scm 462 */
											bool_t BgL_res1935z00_2606;

											{	/* Object/class.scm 462 */
												obj_t BgL_oclassz00_2589;

												{	/* Object/class.scm 462 */
													obj_t BgL_arg1815z00_2597;
													long BgL_arg1816z00_2598;

													BgL_arg1815z00_2597 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Object/class.scm 462 */
														long BgL_arg1817z00_2599;

														BgL_arg1817z00_2599 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2575);
														BgL_arg1816z00_2598 =
															(BgL_arg1817z00_2599 - OBJECT_TYPE);
													}
													BgL_oclassz00_2589 =
														VECTOR_REF(BgL_arg1815z00_2597,
														BgL_arg1816z00_2598);
												}
												{	/* Object/class.scm 462 */
													bool_t BgL__ortest_1115z00_2590;

													BgL__ortest_1115z00_2590 =
														(BgL_classz00_2573 == BgL_oclassz00_2589);
													if (BgL__ortest_1115z00_2590)
														{	/* Object/class.scm 462 */
															BgL_res1935z00_2606 = BgL__ortest_1115z00_2590;
														}
													else
														{	/* Object/class.scm 462 */
															long BgL_odepthz00_2591;

															{	/* Object/class.scm 462 */
																obj_t BgL_arg1804z00_2592;

																BgL_arg1804z00_2592 = (BgL_oclassz00_2589);
																BgL_odepthz00_2591 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2592);
															}
															if ((2L < BgL_odepthz00_2591))
																{	/* Object/class.scm 462 */
																	obj_t BgL_arg1802z00_2594;

																	{	/* Object/class.scm 462 */
																		obj_t BgL_arg1803z00_2595;

																		BgL_arg1803z00_2595 = (BgL_oclassz00_2589);
																		BgL_arg1802z00_2594 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2595, 2L);
																	}
																	BgL_res1935z00_2606 =
																		(BgL_arg1802z00_2594 == BgL_classz00_2573);
																}
															else
																{	/* Object/class.scm 462 */
																	BgL_res1935z00_2606 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2398z00_5839 = BgL_res1935z00_2606;
										}
								}
							else
								{	/* Object/class.scm 462 */
									BgL_test2398z00_5839 = ((bool_t) 0);
								}
						}
						if (BgL_test2398z00_5839)
							{
								BgL_tclassz00_bglt BgL_auxz00_5862;

								{
									obj_t BgL_auxz00_5863;

									{	/* Object/class.scm 463 */
										BgL_objectz00_bglt BgL_tmpz00_5864;

										BgL_tmpz00_5864 =
											((BgL_objectz00_bglt)
											((BgL_typez00_bglt) BgL_superz00_1021));
										BgL_auxz00_5863 = BGL_OBJECT_WIDENING(BgL_tmpz00_5864);
									}
									BgL_auxz00_5862 = ((BgL_tclassz00_bglt) BgL_auxz00_5863);
								}
								BgL_superzd2vnumzd2_1022 =
									(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_5862))->
									BgL_virtualzd2slotszd2numberz00);
							}
						else
							{	/* Object/class.scm 462 */
								BgL_superzd2vnumzd2_1022 = BINT(0L);
							}
					}
					{	/* Object/class.scm 462 */
						obj_t BgL_slotsz00_1023;

						{	/* Object/class.scm 465 */
							obj_t BgL_pairz00_2612;

							BgL_pairz00_2612 = CDR(((obj_t) BgL_classzd2defzd2_279));
							BgL_slotsz00_1023 = CDR(BgL_pairz00_2612);
						}
						{	/* Object/class.scm 465 */

							{	/* Object/class.scm 469 */
								obj_t BgL_cslotsz00_1024;

								BgL_cslotsz00_1024 =
									BGl_makezd2classzd2slotsz00zzobject_slotsz00(BgL_classz00_278,
									BgL_slotsz00_1023, BgL_superz00_1021,
									CINT(BgL_superzd2vnumzd2_1022), BgL_srczd2defzd2_280);
								{	/* Object/class.scm 469 */
									obj_t BgL_localzd2vnumzd2_1025;

									BgL_localzd2vnumzd2_1025 =
										BGl_getzd2localzd2virtualzd2slotszd2numberz00zzobject_slotsz00
										(BgL_classz00_278, BgL_cslotsz00_1024);
									{	/* Object/class.scm 470 */

										{
											BgL_tclassz00_bglt BgL_auxz00_5877;

											{
												obj_t BgL_auxz00_5878;

												{	/* Object/class.scm 472 */
													BgL_objectz00_bglt BgL_tmpz00_5879;

													BgL_tmpz00_5879 =
														((BgL_objectz00_bglt) BgL_classz00_278);
													BgL_auxz00_5878 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_5879);
												}
												BgL_auxz00_5877 =
													((BgL_tclassz00_bglt) BgL_auxz00_5878);
											}
											((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_5877))->
													BgL_virtualzd2slotszd2numberz00) =
												((obj_t) BgL_localzd2vnumzd2_1025), BUNSPEC);
										}
										{
											BgL_tclassz00_bglt BgL_auxz00_5884;

											{
												obj_t BgL_auxz00_5885;

												{	/* Object/class.scm 474 */
													BgL_objectz00_bglt BgL_tmpz00_5886;

													BgL_tmpz00_5886 =
														((BgL_objectz00_bglt) BgL_classz00_278);
													BgL_auxz00_5885 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_5886);
												}
												BgL_auxz00_5884 =
													((BgL_tclassz00_bglt) BgL_auxz00_5885);
											}
											return
												((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_5884))->
													BgL_slotsz00) =
												((obj_t) BgL_cslotsz00_1024), BUNSPEC);
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &set-class-slots! */
	obj_t BGl_z62setzd2classzd2slotsz12z70zzobject_classz00(obj_t BgL_envz00_3193,
		obj_t BgL_classz00_3194, obj_t BgL_classzd2defzd2_3195,
		obj_t BgL_srczd2defzd2_3196)
	{
		{	/* Object/class.scm 457 */
			return
				BGl_setzd2classzd2slotsz12z12zzobject_classz00(
				((BgL_typez00_bglt) BgL_classz00_3194), BgL_classzd2defzd2_3195,
				BgL_srczd2defzd2_3196);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzobject_classz00(void)
	{
		{	/* Object/class.scm 15 */
			{	/* Object/class.scm 34 */
				obj_t BgL_arg1611z00_1032;
				obj_t BgL_arg1613z00_1033;

				{	/* Object/class.scm 34 */
					obj_t BgL_v1227z00_1081;

					BgL_v1227z00_1081 = create_vector(11L);
					{	/* Object/class.scm 34 */
						obj_t BgL_arg1646z00_1082;

						BgL_arg1646z00_1082 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc1975z00zzobject_classz00,
							BGl_proc1974z00zzobject_classz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(9));
						VECTOR_SET(BgL_v1227z00_1081, 0L, BgL_arg1646z00_1082);
					}
					{	/* Object/class.scm 34 */
						obj_t BgL_arg1654z00_1092;

						BgL_arg1654z00_1092 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(10),
							BGl_proc1978z00zzobject_classz00,
							BGl_proc1977z00zzobject_classz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1976z00zzobject_classz00, CNST_TABLE_REF(9));
						VECTOR_SET(BgL_v1227z00_1081, 1L, BgL_arg1654z00_1092);
					}
					{	/* Object/class.scm 34 */
						obj_t BgL_arg1681z00_1105;

						BgL_arg1681z00_1105 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(11),
							BGl_proc1980z00zzobject_classz00,
							BGl_proc1979z00zzobject_classz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, BGl_globalz00zzast_varz00);
						VECTOR_SET(BgL_v1227z00_1081, 2L, BgL_arg1681z00_1105);
					}
					{	/* Object/class.scm 34 */
						obj_t BgL_arg1692z00_1115;

						BgL_arg1692z00_1115 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc1983z00zzobject_classz00,
							BGl_proc1982z00zzobject_classz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BGl_proc1981z00zzobject_classz00, CNST_TABLE_REF(9));
						VECTOR_SET(BgL_v1227z00_1081, 3L, BgL_arg1692z00_1115);
					}
					{	/* Object/class.scm 34 */
						obj_t BgL_arg1705z00_1128;

						BgL_arg1705z00_1128 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(12),
							BGl_proc1986z00zzobject_classz00,
							BGl_proc1985z00zzobject_classz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1984z00zzobject_classz00, CNST_TABLE_REF(13));
						VECTOR_SET(BgL_v1227z00_1081, 4L, BgL_arg1705z00_1128);
					}
					{	/* Object/class.scm 34 */
						obj_t BgL_arg1714z00_1141;

						BgL_arg1714z00_1141 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(14),
							BGl_proc1989z00zzobject_classz00,
							BGl_proc1988z00zzobject_classz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BGl_proc1987z00zzobject_classz00, CNST_TABLE_REF(15));
						VECTOR_SET(BgL_v1227z00_1081, 5L, BgL_arg1714z00_1141);
					}
					{	/* Object/class.scm 34 */
						obj_t BgL_arg1724z00_1154;

						BgL_arg1724z00_1154 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(16),
							BGl_proc1991z00zzobject_classz00,
							BGl_proc1990z00zzobject_classz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(9));
						VECTOR_SET(BgL_v1227z00_1081, 6L, BgL_arg1724z00_1154);
					}
					{	/* Object/class.scm 34 */
						obj_t BgL_arg1737z00_1164;

						BgL_arg1737z00_1164 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(17),
							BGl_proc1994z00zzobject_classz00,
							BGl_proc1993z00zzobject_classz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1992z00zzobject_classz00, CNST_TABLE_REF(9));
						VECTOR_SET(BgL_v1227z00_1081, 7L, BgL_arg1737z00_1164);
					}
					{	/* Object/class.scm 34 */
						obj_t BgL_arg1746z00_1177;

						BgL_arg1746z00_1177 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(18),
							BGl_proc1997z00zzobject_classz00,
							BGl_proc1996z00zzobject_classz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BGl_proc1995z00zzobject_classz00, CNST_TABLE_REF(15));
						VECTOR_SET(BgL_v1227z00_1081, 8L, BgL_arg1746z00_1177);
					}
					{	/* Object/class.scm 34 */
						obj_t BgL_arg1753z00_1190;

						BgL_arg1753z00_1190 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(19),
							BGl_proc2000z00zzobject_classz00,
							BGl_proc1999z00zzobject_classz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1998z00zzobject_classz00, CNST_TABLE_REF(9));
						VECTOR_SET(BgL_v1227z00_1081, 9L, BgL_arg1753z00_1190);
					}
					{	/* Object/class.scm 34 */
						obj_t BgL_arg1765z00_1203;

						BgL_arg1765z00_1203 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(20),
							BGl_proc2003z00zzobject_classz00,
							BGl_proc2002z00zzobject_classz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2001z00zzobject_classz00, CNST_TABLE_REF(21));
						VECTOR_SET(BgL_v1227z00_1081, 10L, BgL_arg1765z00_1203);
					}
					BgL_arg1611z00_1032 = BgL_v1227z00_1081;
				}
				{	/* Object/class.scm 34 */
					obj_t BgL_v1228z00_1216;

					BgL_v1228z00_1216 = create_vector(0L);
					BgL_arg1613z00_1033 = BgL_v1228z00_1216;
				}
				BGl_tclassz00zzobject_classz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(22),
					CNST_TABLE_REF(23), BGl_typez00zztype_typez00, 18700L,
					BGl_proc2007z00zzobject_classz00, BGl_proc2006z00zzobject_classz00,
					BFALSE, BGl_proc2005z00zzobject_classz00,
					BGl_proc2004z00zzobject_classz00, BgL_arg1611z00_1032,
					BgL_arg1613z00_1033);
			}
			{	/* Object/class.scm 58 */
				obj_t BgL_arg1806z00_1225;
				obj_t BgL_arg1808z00_1226;

				{	/* Object/class.scm 58 */
					obj_t BgL_v1229z00_1266;

					BgL_v1229z00_1266 = create_vector(3L);
					{	/* Object/class.scm 58 */
						obj_t BgL_arg1834z00_1267;

						BgL_arg1834z00_1267 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc2010z00zzobject_classz00,
							BGl_proc2009z00zzobject_classz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2008z00zzobject_classz00, CNST_TABLE_REF(9));
						VECTOR_SET(BgL_v1229z00_1266, 0L, BgL_arg1834z00_1267);
					}
					{	/* Object/class.scm 58 */
						obj_t BgL_arg1842z00_1280;

						BgL_arg1842z00_1280 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(10),
							BGl_proc2013z00zzobject_classz00,
							BGl_proc2012z00zzobject_classz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2011z00zzobject_classz00, CNST_TABLE_REF(9));
						VECTOR_SET(BgL_v1229z00_1266, 1L, BgL_arg1842z00_1280);
					}
					{	/* Object/class.scm 58 */
						obj_t BgL_arg1849z00_1293;

						BgL_arg1849z00_1293 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(24),
							BGl_proc2016z00zzobject_classz00,
							BGl_proc2015z00zzobject_classz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BGl_proc2014z00zzobject_classz00, CNST_TABLE_REF(25));
						VECTOR_SET(BgL_v1229z00_1266, 2L, BgL_arg1849z00_1293);
					}
					BgL_arg1806z00_1225 = BgL_v1229z00_1266;
				}
				{	/* Object/class.scm 58 */
					obj_t BgL_v1230z00_1306;

					BgL_v1230z00_1306 = create_vector(0L);
					BgL_arg1808z00_1226 = BgL_v1230z00_1306;
				}
				BGl_jclassz00zzobject_classz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(26),
					CNST_TABLE_REF(23), BGl_typez00zztype_typez00, 63374L,
					BGl_proc2020z00zzobject_classz00, BGl_proc2019z00zzobject_classz00,
					BFALSE, BGl_proc2018z00zzobject_classz00,
					BGl_proc2017z00zzobject_classz00, BgL_arg1806z00_1225,
					BgL_arg1808z00_1226);
			}
			{	/* Object/class.scm 66 */
				obj_t BgL_arg1860z00_1315;
				obj_t BgL_arg1862z00_1316;

				{	/* Object/class.scm 66 */
					obj_t BgL_v1231z00_1354;

					BgL_v1231z00_1354 = create_vector(1L);
					{	/* Object/class.scm 66 */
						obj_t BgL_arg1875z00_1355;

						BgL_arg1875z00_1355 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(27),
							BGl_proc2023z00zzobject_classz00,
							BGl_proc2022z00zzobject_classz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2021z00zzobject_classz00, CNST_TABLE_REF(9));
						VECTOR_SET(BgL_v1231z00_1354, 0L, BgL_arg1875z00_1355);
					}
					BgL_arg1860z00_1315 = BgL_v1231z00_1354;
				}
				{	/* Object/class.scm 66 */
					obj_t BgL_v1232z00_1368;

					BgL_v1232z00_1368 = create_vector(0L);
					BgL_arg1862z00_1316 = BgL_v1232z00_1368;
				}
				return (BGl_wclassz00zzobject_classz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(28),
						CNST_TABLE_REF(23), BGl_typez00zztype_typez00, 34641L,
						BGl_proc2027z00zzobject_classz00, BGl_proc2026z00zzobject_classz00,
						BFALSE, BGl_proc2025z00zzobject_classz00,
						BGl_proc2024z00zzobject_classz00, BgL_arg1860z00_1315,
						BgL_arg1862z00_1316), BUNSPEC);
			}
		}

	}



/* &lambda1870 */
	BgL_typez00_bglt BGl_z62lambda1870z62zzobject_classz00(obj_t BgL_envz00_3251,
		obj_t BgL_o1143z00_3252)
	{
		{	/* Object/class.scm 66 */
			{	/* Object/class.scm 66 */
				long BgL_arg1872z00_3502;

				{	/* Object/class.scm 66 */
					obj_t BgL_arg1873z00_3503;

					{	/* Object/class.scm 66 */
						obj_t BgL_arg1874z00_3504;

						{	/* Object/class.scm 66 */
							obj_t BgL_arg1815z00_3505;
							long BgL_arg1816z00_3506;

							BgL_arg1815z00_3505 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Object/class.scm 66 */
								long BgL_arg1817z00_3507;

								BgL_arg1817z00_3507 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_typez00_bglt) BgL_o1143z00_3252)));
								BgL_arg1816z00_3506 = (BgL_arg1817z00_3507 - OBJECT_TYPE);
							}
							BgL_arg1874z00_3504 =
								VECTOR_REF(BgL_arg1815z00_3505, BgL_arg1816z00_3506);
						}
						BgL_arg1873z00_3503 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1874z00_3504);
					}
					{	/* Object/class.scm 66 */
						obj_t BgL_tmpz00_5974;

						BgL_tmpz00_5974 = ((obj_t) BgL_arg1873z00_3503);
						BgL_arg1872z00_3502 = BGL_CLASS_NUM(BgL_tmpz00_5974);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_typez00_bglt) BgL_o1143z00_3252)), BgL_arg1872z00_3502);
			}
			{	/* Object/class.scm 66 */
				BgL_objectz00_bglt BgL_tmpz00_5980;

				BgL_tmpz00_5980 =
					((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_o1143z00_3252));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5980, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_o1143z00_3252));
			return ((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1143z00_3252));
		}

	}



/* &<@anonymous:1869> */
	obj_t BGl_z62zc3z04anonymousza31869ze3ze5zzobject_classz00(obj_t
		BgL_envz00_3253, obj_t BgL_new1142z00_3254)
	{
		{	/* Object/class.scm 66 */
			{
				BgL_typez00_bglt BgL_auxz00_5988;

				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt)
									((BgL_typez00_bglt) BgL_new1142z00_3254))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(29)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1142z00_3254))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1142z00_3254))))->BgL_siza7eza7) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1142z00_3254))))->BgL_classz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1142z00_3254))))->BgL_coercezd2tozd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1142z00_3254))))->BgL_parentsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1142z00_3254))))->BgL_initzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1142z00_3254))))->BgL_magiczf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1142z00_3254))))->BgL_nullz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1142z00_3254))))->BgL_z42z42) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1142z00_3254))))->BgL_aliasz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1142z00_3254))))->BgL_pointedzd2tozd2byz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1142z00_3254))))->BgL_tvectorz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1142z00_3254))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1142z00_3254))))->BgL_importzd2locationzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1142z00_3254))))->BgL_occurrencez00) =
					((int) (int) (0L)), BUNSPEC);
				{
					BgL_wclassz00_bglt BgL_auxz00_6039;

					{
						obj_t BgL_auxz00_6040;

						{	/* Object/class.scm 66 */
							BgL_objectz00_bglt BgL_tmpz00_6041;

							BgL_tmpz00_6041 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1142z00_3254));
							BgL_auxz00_6040 = BGL_OBJECT_WIDENING(BgL_tmpz00_6041);
						}
						BgL_auxz00_6039 = ((BgL_wclassz00_bglt) BgL_auxz00_6040);
					}
					((((BgL_wclassz00_bglt) COBJECT(BgL_auxz00_6039))->
							BgL_itszd2classzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				BgL_auxz00_5988 = ((BgL_typez00_bglt) BgL_new1142z00_3254);
				return ((obj_t) BgL_auxz00_5988);
			}
		}

	}



/* &lambda1867 */
	BgL_typez00_bglt BGl_z62lambda1867z62zzobject_classz00(obj_t BgL_envz00_3255,
		obj_t BgL_o1139z00_3256)
	{
		{	/* Object/class.scm 66 */
			{	/* Object/class.scm 66 */
				BgL_wclassz00_bglt BgL_wide1141z00_3510;

				BgL_wide1141z00_3510 =
					((BgL_wclassz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_wclassz00_bgl))));
				{	/* Object/class.scm 66 */
					obj_t BgL_auxz00_6054;
					BgL_objectz00_bglt BgL_tmpz00_6050;

					BgL_auxz00_6054 = ((obj_t) BgL_wide1141z00_3510);
					BgL_tmpz00_6050 =
						((BgL_objectz00_bglt)
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1139z00_3256)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6050, BgL_auxz00_6054);
				}
				((BgL_objectz00_bglt)
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1139z00_3256)));
				{	/* Object/class.scm 66 */
					long BgL_arg1868z00_3511;

					BgL_arg1868z00_3511 = BGL_CLASS_NUM(BGl_wclassz00zzobject_classz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_typez00_bglt)
								((BgL_typez00_bglt) BgL_o1139z00_3256))), BgL_arg1868z00_3511);
				}
				return
					((BgL_typez00_bglt)
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1139z00_3256)));
			}
		}

	}



/* &lambda1863 */
	BgL_typez00_bglt BGl_z62lambda1863z62zzobject_classz00(obj_t BgL_envz00_3257,
		obj_t BgL_id1122z00_3258, obj_t BgL_name1123z00_3259,
		obj_t BgL_siza7e1124za7_3260, obj_t BgL_class1125z00_3261,
		obj_t BgL_coercezd2to1126zd2_3262, obj_t BgL_parents1127z00_3263,
		obj_t BgL_initzf31128zf3_3264, obj_t BgL_magiczf31129zf3_3265,
		obj_t BgL_null1130z00_3266, obj_t BgL_z421131z42_3267,
		obj_t BgL_alias1132z00_3268, obj_t BgL_pointedzd2tozd2by1133z00_3269,
		obj_t BgL_tvector1134z00_3270, obj_t BgL_location1135z00_3271,
		obj_t BgL_importzd2location1136zd2_3272, obj_t BgL_occurrence1137z00_3273,
		obj_t BgL_itszd2class1138zd2_3274)
	{
		{	/* Object/class.scm 66 */
			{	/* Object/class.scm 66 */
				bool_t BgL_initzf31128zf3_3513;
				bool_t BgL_magiczf31129zf3_3514;
				int BgL_occurrence1137z00_3515;

				BgL_initzf31128zf3_3513 = CBOOL(BgL_initzf31128zf3_3264);
				BgL_magiczf31129zf3_3514 = CBOOL(BgL_magiczf31129zf3_3265);
				BgL_occurrence1137z00_3515 = CINT(BgL_occurrence1137z00_3273);
				{	/* Object/class.scm 66 */
					BgL_typez00_bglt BgL_new1189z00_3516;

					{	/* Object/class.scm 66 */
						BgL_typez00_bglt BgL_tmp1186z00_3517;
						BgL_wclassz00_bglt BgL_wide1187z00_3518;

						{
							BgL_typez00_bglt BgL_auxz00_6071;

							{	/* Object/class.scm 66 */
								BgL_typez00_bglt BgL_new1185z00_3519;

								BgL_new1185z00_3519 =
									((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_typez00_bgl))));
								{	/* Object/class.scm 66 */
									long BgL_arg1866z00_3520;

									BgL_arg1866z00_3520 =
										BGL_CLASS_NUM(BGl_typez00zztype_typez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1185z00_3519),
										BgL_arg1866z00_3520);
								}
								{	/* Object/class.scm 66 */
									BgL_objectz00_bglt BgL_tmpz00_6076;

									BgL_tmpz00_6076 = ((BgL_objectz00_bglt) BgL_new1185z00_3519);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6076, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1185z00_3519);
								BgL_auxz00_6071 = BgL_new1185z00_3519;
							}
							BgL_tmp1186z00_3517 = ((BgL_typez00_bglt) BgL_auxz00_6071);
						}
						BgL_wide1187z00_3518 =
							((BgL_wclassz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_wclassz00_bgl))));
						{	/* Object/class.scm 66 */
							obj_t BgL_auxz00_6084;
							BgL_objectz00_bglt BgL_tmpz00_6082;

							BgL_auxz00_6084 = ((obj_t) BgL_wide1187z00_3518);
							BgL_tmpz00_6082 = ((BgL_objectz00_bglt) BgL_tmp1186z00_3517);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6082, BgL_auxz00_6084);
						}
						((BgL_objectz00_bglt) BgL_tmp1186z00_3517);
						{	/* Object/class.scm 66 */
							long BgL_arg1864z00_3521;

							BgL_arg1864z00_3521 =
								BGL_CLASS_NUM(BGl_wclassz00zzobject_classz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1186z00_3517),
								BgL_arg1864z00_3521);
						}
						BgL_new1189z00_3516 = ((BgL_typez00_bglt) BgL_tmp1186z00_3517);
					}
					((((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt) BgL_new1189z00_3516)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1122z00_3258)), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1189z00_3516)))->BgL_namez00) =
						((obj_t) BgL_name1123z00_3259), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1189z00_3516)))->BgL_siza7eza7) =
						((obj_t) BgL_siza7e1124za7_3260), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1189z00_3516)))->BgL_classz00) =
						((obj_t) BgL_class1125z00_3261), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1189z00_3516)))->BgL_coercezd2tozd2) =
						((obj_t) BgL_coercezd2to1126zd2_3262), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1189z00_3516)))->BgL_parentsz00) =
						((obj_t) BgL_parents1127z00_3263), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1189z00_3516)))->BgL_initzf3zf3) =
						((bool_t) BgL_initzf31128zf3_3513), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1189z00_3516)))->BgL_magiczf3zf3) =
						((bool_t) BgL_magiczf31129zf3_3514), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1189z00_3516)))->BgL_nullz00) =
						((obj_t) BgL_null1130z00_3266), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1189z00_3516)))->BgL_z42z42) =
						((obj_t) BgL_z421131z42_3267), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1189z00_3516)))->BgL_aliasz00) =
						((obj_t) BgL_alias1132z00_3268), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1189z00_3516)))->BgL_pointedzd2tozd2byz00) =
						((obj_t) BgL_pointedzd2tozd2by1133z00_3269), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1189z00_3516)))->BgL_tvectorz00) =
						((obj_t) BgL_tvector1134z00_3270), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1189z00_3516)))->BgL_locationz00) =
						((obj_t) BgL_location1135z00_3271), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1189z00_3516)))->BgL_importzd2locationzd2) =
						((obj_t) BgL_importzd2location1136zd2_3272), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1189z00_3516)))->BgL_occurrencez00) =
						((int) BgL_occurrence1137z00_3515), BUNSPEC);
					{
						BgL_wclassz00_bglt BgL_auxz00_6125;

						{
							obj_t BgL_auxz00_6126;

							{	/* Object/class.scm 66 */
								BgL_objectz00_bglt BgL_tmpz00_6127;

								BgL_tmpz00_6127 = ((BgL_objectz00_bglt) BgL_new1189z00_3516);
								BgL_auxz00_6126 = BGL_OBJECT_WIDENING(BgL_tmpz00_6127);
							}
							BgL_auxz00_6125 = ((BgL_wclassz00_bglt) BgL_auxz00_6126);
						}
						((((BgL_wclassz00_bglt) COBJECT(BgL_auxz00_6125))->
								BgL_itszd2classzd2) =
							((obj_t) BgL_itszd2class1138zd2_3274), BUNSPEC);
					}
					return BgL_new1189z00_3516;
				}
			}
		}

	}



/* &<@anonymous:1881> */
	obj_t BGl_z62zc3z04anonymousza31881ze3ze5zzobject_classz00(obj_t
		BgL_envz00_3275)
	{
		{	/* Object/class.scm 66 */
			return BUNSPEC;
		}

	}



/* &lambda1880 */
	obj_t BGl_z62lambda1880z62zzobject_classz00(obj_t BgL_envz00_3276,
		obj_t BgL_oz00_3277, obj_t BgL_vz00_3278)
	{
		{	/* Object/class.scm 66 */
			{
				BgL_wclassz00_bglt BgL_auxz00_6132;

				{
					obj_t BgL_auxz00_6133;

					{	/* Object/class.scm 66 */
						BgL_objectz00_bglt BgL_tmpz00_6134;

						BgL_tmpz00_6134 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3277));
						BgL_auxz00_6133 = BGL_OBJECT_WIDENING(BgL_tmpz00_6134);
					}
					BgL_auxz00_6132 = ((BgL_wclassz00_bglt) BgL_auxz00_6133);
				}
				return
					((((BgL_wclassz00_bglt) COBJECT(BgL_auxz00_6132))->
						BgL_itszd2classzd2) = ((obj_t) BgL_vz00_3278), BUNSPEC);
			}
		}

	}



/* &lambda1879 */
	obj_t BGl_z62lambda1879z62zzobject_classz00(obj_t BgL_envz00_3279,
		obj_t BgL_oz00_3280)
	{
		{	/* Object/class.scm 66 */
			{
				BgL_wclassz00_bglt BgL_auxz00_6140;

				{
					obj_t BgL_auxz00_6141;

					{	/* Object/class.scm 66 */
						BgL_objectz00_bglt BgL_tmpz00_6142;

						BgL_tmpz00_6142 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3280));
						BgL_auxz00_6141 = BGL_OBJECT_WIDENING(BgL_tmpz00_6142);
					}
					BgL_auxz00_6140 = ((BgL_wclassz00_bglt) BgL_auxz00_6141);
				}
				return
					(((BgL_wclassz00_bglt) COBJECT(BgL_auxz00_6140))->BgL_itszd2classzd2);
			}
		}

	}



/* &lambda1824 */
	BgL_typez00_bglt BGl_z62lambda1824z62zzobject_classz00(obj_t BgL_envz00_3281,
		obj_t BgL_o1120z00_3282)
	{
		{	/* Object/class.scm 58 */
			{	/* Object/class.scm 58 */
				long BgL_arg1831z00_3525;

				{	/* Object/class.scm 58 */
					obj_t BgL_arg1832z00_3526;

					{	/* Object/class.scm 58 */
						obj_t BgL_arg1833z00_3527;

						{	/* Object/class.scm 58 */
							obj_t BgL_arg1815z00_3528;
							long BgL_arg1816z00_3529;

							BgL_arg1815z00_3528 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Object/class.scm 58 */
								long BgL_arg1817z00_3530;

								BgL_arg1817z00_3530 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_typez00_bglt) BgL_o1120z00_3282)));
								BgL_arg1816z00_3529 = (BgL_arg1817z00_3530 - OBJECT_TYPE);
							}
							BgL_arg1833z00_3527 =
								VECTOR_REF(BgL_arg1815z00_3528, BgL_arg1816z00_3529);
						}
						BgL_arg1832z00_3526 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1833z00_3527);
					}
					{	/* Object/class.scm 58 */
						obj_t BgL_tmpz00_6155;

						BgL_tmpz00_6155 = ((obj_t) BgL_arg1832z00_3526);
						BgL_arg1831z00_3525 = BGL_CLASS_NUM(BgL_tmpz00_6155);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_typez00_bglt) BgL_o1120z00_3282)), BgL_arg1831z00_3525);
			}
			{	/* Object/class.scm 58 */
				BgL_objectz00_bglt BgL_tmpz00_6161;

				BgL_tmpz00_6161 =
					((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_o1120z00_3282));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6161, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_o1120z00_3282));
			return ((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1120z00_3282));
		}

	}



/* &<@anonymous:1823> */
	obj_t BGl_z62zc3z04anonymousza31823ze3ze5zzobject_classz00(obj_t
		BgL_envz00_3283, obj_t BgL_new1119z00_3284)
	{
		{	/* Object/class.scm 58 */
			{
				BgL_typez00_bglt BgL_auxz00_6169;

				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt)
									((BgL_typez00_bglt) BgL_new1119z00_3284))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(29)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1119z00_3284))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1119z00_3284))))->BgL_siza7eza7) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1119z00_3284))))->BgL_classz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1119z00_3284))))->BgL_coercezd2tozd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1119z00_3284))))->BgL_parentsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1119z00_3284))))->BgL_initzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1119z00_3284))))->BgL_magiczf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1119z00_3284))))->BgL_nullz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1119z00_3284))))->BgL_z42z42) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1119z00_3284))))->BgL_aliasz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1119z00_3284))))->BgL_pointedzd2tozd2byz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1119z00_3284))))->BgL_tvectorz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1119z00_3284))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1119z00_3284))))->BgL_importzd2locationzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1119z00_3284))))->BgL_occurrencez00) =
					((int) (int) (0L)), BUNSPEC);
				{
					BgL_jclassz00_bglt BgL_auxz00_6220;

					{
						obj_t BgL_auxz00_6221;

						{	/* Object/class.scm 58 */
							BgL_objectz00_bglt BgL_tmpz00_6222;

							BgL_tmpz00_6222 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1119z00_3284));
							BgL_auxz00_6221 = BGL_OBJECT_WIDENING(BgL_tmpz00_6222);
						}
						BgL_auxz00_6220 = ((BgL_jclassz00_bglt) BgL_auxz00_6221);
					}
					((((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_6220))->
							BgL_itszd2superzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_jclassz00_bglt BgL_auxz00_6228;

					{
						obj_t BgL_auxz00_6229;

						{	/* Object/class.scm 58 */
							BgL_objectz00_bglt BgL_tmpz00_6230;

							BgL_tmpz00_6230 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1119z00_3284));
							BgL_auxz00_6229 = BGL_OBJECT_WIDENING(BgL_tmpz00_6230);
						}
						BgL_auxz00_6228 = ((BgL_jclassz00_bglt) BgL_auxz00_6229);
					}
					((((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_6228))->BgL_slotsz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_jclassz00_bglt BgL_auxz00_6236;

					{
						obj_t BgL_auxz00_6237;

						{	/* Object/class.scm 58 */
							BgL_objectz00_bglt BgL_tmpz00_6238;

							BgL_tmpz00_6238 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1119z00_3284));
							BgL_auxz00_6237 = BGL_OBJECT_WIDENING(BgL_tmpz00_6238);
						}
						BgL_auxz00_6236 = ((BgL_jclassz00_bglt) BgL_auxz00_6237);
					}
					((((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_6236))->BgL_packagez00) =
						((obj_t) BGl_string2028z00zzobject_classz00), BUNSPEC);
				}
				BgL_auxz00_6169 = ((BgL_typez00_bglt) BgL_new1119z00_3284);
				return ((obj_t) BgL_auxz00_6169);
			}
		}

	}



/* &lambda1821 */
	BgL_typez00_bglt BGl_z62lambda1821z62zzobject_classz00(obj_t BgL_envz00_3285,
		obj_t BgL_o1116z00_3286)
	{
		{	/* Object/class.scm 58 */
			{	/* Object/class.scm 58 */
				BgL_jclassz00_bglt BgL_wide1118z00_3533;

				BgL_wide1118z00_3533 =
					((BgL_jclassz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_jclassz00_bgl))));
				{	/* Object/class.scm 58 */
					obj_t BgL_auxz00_6251;
					BgL_objectz00_bglt BgL_tmpz00_6247;

					BgL_auxz00_6251 = ((obj_t) BgL_wide1118z00_3533);
					BgL_tmpz00_6247 =
						((BgL_objectz00_bglt)
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1116z00_3286)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6247, BgL_auxz00_6251);
				}
				((BgL_objectz00_bglt)
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1116z00_3286)));
				{	/* Object/class.scm 58 */
					long BgL_arg1822z00_3534;

					BgL_arg1822z00_3534 = BGL_CLASS_NUM(BGl_jclassz00zzobject_classz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_typez00_bglt)
								((BgL_typez00_bglt) BgL_o1116z00_3286))), BgL_arg1822z00_3534);
				}
				return
					((BgL_typez00_bglt)
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1116z00_3286)));
			}
		}

	}



/* &lambda1809 */
	BgL_typez00_bglt BGl_z62lambda1809z62zzobject_classz00(obj_t BgL_envz00_3287,
		obj_t BgL_id1097z00_3288, obj_t BgL_name1098z00_3289,
		obj_t BgL_siza7e1099za7_3290, obj_t BgL_class1100z00_3291,
		obj_t BgL_coercezd2to1101zd2_3292, obj_t BgL_parents1102z00_3293,
		obj_t BgL_initzf31103zf3_3294, obj_t BgL_magiczf31104zf3_3295,
		obj_t BgL_null1105z00_3296, obj_t BgL_z421106z42_3297,
		obj_t BgL_alias1107z00_3298, obj_t BgL_pointedzd2tozd2by1108z00_3299,
		obj_t BgL_tvector1109z00_3300, obj_t BgL_location1110z00_3301,
		obj_t BgL_importzd2location1111zd2_3302, obj_t BgL_occurrence1112z00_3303,
		obj_t BgL_itszd2super1113zd2_3304, obj_t BgL_slots1114z00_3305,
		obj_t BgL_package1115z00_3306)
	{
		{	/* Object/class.scm 58 */
			{	/* Object/class.scm 58 */
				bool_t BgL_initzf31103zf3_3536;
				bool_t BgL_magiczf31104zf3_3537;
				int BgL_occurrence1112z00_3538;

				BgL_initzf31103zf3_3536 = CBOOL(BgL_initzf31103zf3_3294);
				BgL_magiczf31104zf3_3537 = CBOOL(BgL_magiczf31104zf3_3295);
				BgL_occurrence1112z00_3538 = CINT(BgL_occurrence1112z00_3303);
				{	/* Object/class.scm 58 */
					BgL_typez00_bglt BgL_new1183z00_3540;

					{	/* Object/class.scm 58 */
						BgL_typez00_bglt BgL_tmp1181z00_3541;
						BgL_jclassz00_bglt BgL_wide1182z00_3542;

						{
							BgL_typez00_bglt BgL_auxz00_6268;

							{	/* Object/class.scm 58 */
								BgL_typez00_bglt BgL_new1180z00_3543;

								BgL_new1180z00_3543 =
									((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_typez00_bgl))));
								{	/* Object/class.scm 58 */
									long BgL_arg1820z00_3544;

									BgL_arg1820z00_3544 =
										BGL_CLASS_NUM(BGl_typez00zztype_typez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1180z00_3543),
										BgL_arg1820z00_3544);
								}
								{	/* Object/class.scm 58 */
									BgL_objectz00_bglt BgL_tmpz00_6273;

									BgL_tmpz00_6273 = ((BgL_objectz00_bglt) BgL_new1180z00_3543);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6273, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1180z00_3543);
								BgL_auxz00_6268 = BgL_new1180z00_3543;
							}
							BgL_tmp1181z00_3541 = ((BgL_typez00_bglt) BgL_auxz00_6268);
						}
						BgL_wide1182z00_3542 =
							((BgL_jclassz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_jclassz00_bgl))));
						{	/* Object/class.scm 58 */
							obj_t BgL_auxz00_6281;
							BgL_objectz00_bglt BgL_tmpz00_6279;

							BgL_auxz00_6281 = ((obj_t) BgL_wide1182z00_3542);
							BgL_tmpz00_6279 = ((BgL_objectz00_bglt) BgL_tmp1181z00_3541);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6279, BgL_auxz00_6281);
						}
						((BgL_objectz00_bglt) BgL_tmp1181z00_3541);
						{	/* Object/class.scm 58 */
							long BgL_arg1812z00_3545;

							BgL_arg1812z00_3545 =
								BGL_CLASS_NUM(BGl_jclassz00zzobject_classz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1181z00_3541),
								BgL_arg1812z00_3545);
						}
						BgL_new1183z00_3540 = ((BgL_typez00_bglt) BgL_tmp1181z00_3541);
					}
					((((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt) BgL_new1183z00_3540)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1097z00_3288)), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1183z00_3540)))->BgL_namez00) =
						((obj_t) BgL_name1098z00_3289), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1183z00_3540)))->BgL_siza7eza7) =
						((obj_t) BgL_siza7e1099za7_3290), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1183z00_3540)))->BgL_classz00) =
						((obj_t) BgL_class1100z00_3291), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1183z00_3540)))->BgL_coercezd2tozd2) =
						((obj_t) BgL_coercezd2to1101zd2_3292), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1183z00_3540)))->BgL_parentsz00) =
						((obj_t) BgL_parents1102z00_3293), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1183z00_3540)))->BgL_initzf3zf3) =
						((bool_t) BgL_initzf31103zf3_3536), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1183z00_3540)))->BgL_magiczf3zf3) =
						((bool_t) BgL_magiczf31104zf3_3537), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1183z00_3540)))->BgL_nullz00) =
						((obj_t) BgL_null1105z00_3296), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1183z00_3540)))->BgL_z42z42) =
						((obj_t) BgL_z421106z42_3297), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1183z00_3540)))->BgL_aliasz00) =
						((obj_t) BgL_alias1107z00_3298), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1183z00_3540)))->BgL_pointedzd2tozd2byz00) =
						((obj_t) BgL_pointedzd2tozd2by1108z00_3299), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1183z00_3540)))->BgL_tvectorz00) =
						((obj_t) BgL_tvector1109z00_3300), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1183z00_3540)))->BgL_locationz00) =
						((obj_t) BgL_location1110z00_3301), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1183z00_3540)))->BgL_importzd2locationzd2) =
						((obj_t) BgL_importzd2location1111zd2_3302), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1183z00_3540)))->BgL_occurrencez00) =
						((int) BgL_occurrence1112z00_3538), BUNSPEC);
					{
						BgL_jclassz00_bglt BgL_auxz00_6322;

						{
							obj_t BgL_auxz00_6323;

							{	/* Object/class.scm 58 */
								BgL_objectz00_bglt BgL_tmpz00_6324;

								BgL_tmpz00_6324 = ((BgL_objectz00_bglt) BgL_new1183z00_3540);
								BgL_auxz00_6323 = BGL_OBJECT_WIDENING(BgL_tmpz00_6324);
							}
							BgL_auxz00_6322 = ((BgL_jclassz00_bglt) BgL_auxz00_6323);
						}
						((((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_6322))->
								BgL_itszd2superzd2) =
							((obj_t) BgL_itszd2super1113zd2_3304), BUNSPEC);
					}
					{
						BgL_jclassz00_bglt BgL_auxz00_6329;

						{
							obj_t BgL_auxz00_6330;

							{	/* Object/class.scm 58 */
								BgL_objectz00_bglt BgL_tmpz00_6331;

								BgL_tmpz00_6331 = ((BgL_objectz00_bglt) BgL_new1183z00_3540);
								BgL_auxz00_6330 = BGL_OBJECT_WIDENING(BgL_tmpz00_6331);
							}
							BgL_auxz00_6329 = ((BgL_jclassz00_bglt) BgL_auxz00_6330);
						}
						((((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_6329))->BgL_slotsz00) =
							((obj_t) BgL_slots1114z00_3305), BUNSPEC);
					}
					{
						BgL_jclassz00_bglt BgL_auxz00_6336;

						{
							obj_t BgL_auxz00_6337;

							{	/* Object/class.scm 58 */
								BgL_objectz00_bglt BgL_tmpz00_6338;

								BgL_tmpz00_6338 = ((BgL_objectz00_bglt) BgL_new1183z00_3540);
								BgL_auxz00_6337 = BGL_OBJECT_WIDENING(BgL_tmpz00_6338);
							}
							BgL_auxz00_6336 = ((BgL_jclassz00_bglt) BgL_auxz00_6337);
						}
						((((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_6336))->BgL_packagez00) =
							((obj_t) ((obj_t) BgL_package1115z00_3306)), BUNSPEC);
					}
					return BgL_new1183z00_3540;
				}
			}
		}

	}



/* &<@anonymous:1855> */
	obj_t BGl_z62zc3z04anonymousza31855ze3ze5zzobject_classz00(obj_t
		BgL_envz00_3307)
	{
		{	/* Object/class.scm 58 */
			return BGl_string2028z00zzobject_classz00;
		}

	}



/* &lambda1854 */
	obj_t BGl_z62lambda1854z62zzobject_classz00(obj_t BgL_envz00_3308,
		obj_t BgL_oz00_3309, obj_t BgL_vz00_3310)
	{
		{	/* Object/class.scm 58 */
			{
				BgL_jclassz00_bglt BgL_auxz00_6344;

				{
					obj_t BgL_auxz00_6345;

					{	/* Object/class.scm 58 */
						BgL_objectz00_bglt BgL_tmpz00_6346;

						BgL_tmpz00_6346 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3309));
						BgL_auxz00_6345 = BGL_OBJECT_WIDENING(BgL_tmpz00_6346);
					}
					BgL_auxz00_6344 = ((BgL_jclassz00_bglt) BgL_auxz00_6345);
				}
				return
					((((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_6344))->BgL_packagez00) =
					((obj_t) ((obj_t) BgL_vz00_3310)), BUNSPEC);
			}
		}

	}



/* &lambda1853 */
	obj_t BGl_z62lambda1853z62zzobject_classz00(obj_t BgL_envz00_3311,
		obj_t BgL_oz00_3312)
	{
		{	/* Object/class.scm 58 */
			{
				BgL_jclassz00_bglt BgL_auxz00_6353;

				{
					obj_t BgL_auxz00_6354;

					{	/* Object/class.scm 58 */
						BgL_objectz00_bglt BgL_tmpz00_6355;

						BgL_tmpz00_6355 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3312));
						BgL_auxz00_6354 = BGL_OBJECT_WIDENING(BgL_tmpz00_6355);
					}
					BgL_auxz00_6353 = ((BgL_jclassz00_bglt) BgL_auxz00_6354);
				}
				return
					(((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_6353))->BgL_packagez00);
			}
		}

	}



/* &<@anonymous:1848> */
	obj_t BGl_z62zc3z04anonymousza31848ze3ze5zzobject_classz00(obj_t
		BgL_envz00_3313)
	{
		{	/* Object/class.scm 58 */
			return BUNSPEC;
		}

	}



/* &lambda1847 */
	obj_t BGl_z62lambda1847z62zzobject_classz00(obj_t BgL_envz00_3314,
		obj_t BgL_oz00_3315, obj_t BgL_vz00_3316)
	{
		{	/* Object/class.scm 58 */
			{
				BgL_jclassz00_bglt BgL_auxz00_6361;

				{
					obj_t BgL_auxz00_6362;

					{	/* Object/class.scm 58 */
						BgL_objectz00_bglt BgL_tmpz00_6363;

						BgL_tmpz00_6363 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3315));
						BgL_auxz00_6362 = BGL_OBJECT_WIDENING(BgL_tmpz00_6363);
					}
					BgL_auxz00_6361 = ((BgL_jclassz00_bglt) BgL_auxz00_6362);
				}
				return
					((((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_6361))->BgL_slotsz00) =
					((obj_t) BgL_vz00_3316), BUNSPEC);
			}
		}

	}



/* &lambda1846 */
	obj_t BGl_z62lambda1846z62zzobject_classz00(obj_t BgL_envz00_3317,
		obj_t BgL_oz00_3318)
	{
		{	/* Object/class.scm 58 */
			{
				BgL_jclassz00_bglt BgL_auxz00_6369;

				{
					obj_t BgL_auxz00_6370;

					{	/* Object/class.scm 58 */
						BgL_objectz00_bglt BgL_tmpz00_6371;

						BgL_tmpz00_6371 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3318));
						BgL_auxz00_6370 = BGL_OBJECT_WIDENING(BgL_tmpz00_6371);
					}
					BgL_auxz00_6369 = ((BgL_jclassz00_bglt) BgL_auxz00_6370);
				}
				return (((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_6369))->BgL_slotsz00);
			}
		}

	}



/* &<@anonymous:1840> */
	obj_t BGl_z62zc3z04anonymousza31840ze3ze5zzobject_classz00(obj_t
		BgL_envz00_3319)
	{
		{	/* Object/class.scm 58 */
			return BUNSPEC;
		}

	}



/* &lambda1839 */
	obj_t BGl_z62lambda1839z62zzobject_classz00(obj_t BgL_envz00_3320,
		obj_t BgL_oz00_3321, obj_t BgL_vz00_3322)
	{
		{	/* Object/class.scm 58 */
			{
				BgL_jclassz00_bglt BgL_auxz00_6377;

				{
					obj_t BgL_auxz00_6378;

					{	/* Object/class.scm 58 */
						BgL_objectz00_bglt BgL_tmpz00_6379;

						BgL_tmpz00_6379 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3321));
						BgL_auxz00_6378 = BGL_OBJECT_WIDENING(BgL_tmpz00_6379);
					}
					BgL_auxz00_6377 = ((BgL_jclassz00_bglt) BgL_auxz00_6378);
				}
				return
					((((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_6377))->
						BgL_itszd2superzd2) = ((obj_t) BgL_vz00_3322), BUNSPEC);
			}
		}

	}



/* &lambda1838 */
	obj_t BGl_z62lambda1838z62zzobject_classz00(obj_t BgL_envz00_3323,
		obj_t BgL_oz00_3324)
	{
		{	/* Object/class.scm 58 */
			{
				BgL_jclassz00_bglt BgL_auxz00_6385;

				{
					obj_t BgL_auxz00_6386;

					{	/* Object/class.scm 58 */
						BgL_objectz00_bglt BgL_tmpz00_6387;

						BgL_tmpz00_6387 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3324));
						BgL_auxz00_6386 = BGL_OBJECT_WIDENING(BgL_tmpz00_6387);
					}
					BgL_auxz00_6385 = ((BgL_jclassz00_bglt) BgL_auxz00_6386);
				}
				return
					(((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_6385))->BgL_itszd2superzd2);
			}
		}

	}



/* &lambda1627 */
	BgL_typez00_bglt BGl_z62lambda1627z62zzobject_classz00(obj_t BgL_envz00_3325,
		obj_t BgL_o1095z00_3326)
	{
		{	/* Object/class.scm 34 */
			{	/* Object/class.scm 34 */
				long BgL_arg1629z00_3554;

				{	/* Object/class.scm 34 */
					obj_t BgL_arg1630z00_3555;

					{	/* Object/class.scm 34 */
						obj_t BgL_arg1642z00_3556;

						{	/* Object/class.scm 34 */
							obj_t BgL_arg1815z00_3557;
							long BgL_arg1816z00_3558;

							BgL_arg1815z00_3557 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Object/class.scm 34 */
								long BgL_arg1817z00_3559;

								BgL_arg1817z00_3559 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_typez00_bglt) BgL_o1095z00_3326)));
								BgL_arg1816z00_3558 = (BgL_arg1817z00_3559 - OBJECT_TYPE);
							}
							BgL_arg1642z00_3556 =
								VECTOR_REF(BgL_arg1815z00_3557, BgL_arg1816z00_3558);
						}
						BgL_arg1630z00_3555 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1642z00_3556);
					}
					{	/* Object/class.scm 34 */
						obj_t BgL_tmpz00_6400;

						BgL_tmpz00_6400 = ((obj_t) BgL_arg1630z00_3555);
						BgL_arg1629z00_3554 = BGL_CLASS_NUM(BgL_tmpz00_6400);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_typez00_bglt) BgL_o1095z00_3326)), BgL_arg1629z00_3554);
			}
			{	/* Object/class.scm 34 */
				BgL_objectz00_bglt BgL_tmpz00_6406;

				BgL_tmpz00_6406 =
					((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_o1095z00_3326));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6406, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_o1095z00_3326));
			return ((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1095z00_3326));
		}

	}



/* &<@anonymous:1626> */
	obj_t BGl_z62zc3z04anonymousza31626ze3ze5zzobject_classz00(obj_t
		BgL_envz00_3327, obj_t BgL_new1094z00_3328)
	{
		{	/* Object/class.scm 34 */
			{
				BgL_typez00_bglt BgL_auxz00_6414;

				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt)
									((BgL_typez00_bglt) BgL_new1094z00_3328))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(29)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3328))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3328))))->BgL_siza7eza7) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3328))))->BgL_classz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3328))))->BgL_coercezd2tozd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3328))))->BgL_parentsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3328))))->BgL_initzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3328))))->BgL_magiczf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3328))))->BgL_nullz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3328))))->BgL_z42z42) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3328))))->BgL_aliasz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3328))))->BgL_pointedzd2tozd2byz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3328))))->BgL_tvectorz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3328))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3328))))->BgL_importzd2locationzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3328))))->BgL_occurrencez00) =
					((int) (int) (0L)), BUNSPEC);
				{
					BgL_tclassz00_bglt BgL_auxz00_6465;

					{
						obj_t BgL_auxz00_6466;

						{	/* Object/class.scm 34 */
							BgL_objectz00_bglt BgL_tmpz00_6467;

							BgL_tmpz00_6467 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1094z00_3328));
							BgL_auxz00_6466 = BGL_OBJECT_WIDENING(BgL_tmpz00_6467);
						}
						BgL_auxz00_6465 = ((BgL_tclassz00_bglt) BgL_auxz00_6466);
					}
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6465))->
							BgL_itszd2superzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_tclassz00_bglt BgL_auxz00_6473;

					{
						obj_t BgL_auxz00_6474;

						{	/* Object/class.scm 34 */
							BgL_objectz00_bglt BgL_tmpz00_6475;

							BgL_tmpz00_6475 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1094z00_3328));
							BgL_auxz00_6474 = BGL_OBJECT_WIDENING(BgL_tmpz00_6475);
						}
						BgL_auxz00_6473 = ((BgL_tclassz00_bglt) BgL_auxz00_6474);
					}
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6473))->BgL_slotsz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_globalz00_bglt BgL_auxz00_6488;
					BgL_tclassz00_bglt BgL_auxz00_6481;

					{	/* Object/class.scm 34 */
						obj_t BgL_classz00_3561;

						BgL_classz00_3561 = BGl_globalz00zzast_varz00;
						{	/* Object/class.scm 34 */
							obj_t BgL__ortest_1117z00_3562;

							BgL__ortest_1117z00_3562 = BGL_CLASS_NIL(BgL_classz00_3561);
							if (CBOOL(BgL__ortest_1117z00_3562))
								{	/* Object/class.scm 34 */
									BgL_auxz00_6488 =
										((BgL_globalz00_bglt) BgL__ortest_1117z00_3562);
								}
							else
								{	/* Object/class.scm 34 */
									BgL_auxz00_6488 =
										((BgL_globalz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3561));
								}
						}
					}
					{
						obj_t BgL_auxz00_6482;

						{	/* Object/class.scm 34 */
							BgL_objectz00_bglt BgL_tmpz00_6483;

							BgL_tmpz00_6483 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1094z00_3328));
							BgL_auxz00_6482 = BGL_OBJECT_WIDENING(BgL_tmpz00_6483);
						}
						BgL_auxz00_6481 = ((BgL_tclassz00_bglt) BgL_auxz00_6482);
					}
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6481))->BgL_holderz00) =
						((BgL_globalz00_bglt) BgL_auxz00_6488), BUNSPEC);
				}
				{
					BgL_tclassz00_bglt BgL_auxz00_6496;

					{
						obj_t BgL_auxz00_6497;

						{	/* Object/class.scm 34 */
							BgL_objectz00_bglt BgL_tmpz00_6498;

							BgL_tmpz00_6498 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1094z00_3328));
							BgL_auxz00_6497 = BGL_OBJECT_WIDENING(BgL_tmpz00_6498);
						}
						BgL_auxz00_6496 = ((BgL_tclassz00_bglt) BgL_auxz00_6497);
					}
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6496))->BgL_wideningz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_tclassz00_bglt BgL_auxz00_6504;

					{
						obj_t BgL_auxz00_6505;

						{	/* Object/class.scm 34 */
							BgL_objectz00_bglt BgL_tmpz00_6506;

							BgL_tmpz00_6506 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1094z00_3328));
							BgL_auxz00_6505 = BGL_OBJECT_WIDENING(BgL_tmpz00_6506);
						}
						BgL_auxz00_6504 = ((BgL_tclassz00_bglt) BgL_auxz00_6505);
					}
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6504))->BgL_depthz00) =
						((long) 0L), BUNSPEC);
				}
				{
					BgL_tclassz00_bglt BgL_auxz00_6512;

					{
						obj_t BgL_auxz00_6513;

						{	/* Object/class.scm 34 */
							BgL_objectz00_bglt BgL_tmpz00_6514;

							BgL_tmpz00_6514 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1094z00_3328));
							BgL_auxz00_6513 = BGL_OBJECT_WIDENING(BgL_tmpz00_6514);
						}
						BgL_auxz00_6512 = ((BgL_tclassz00_bglt) BgL_auxz00_6513);
					}
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6512))->BgL_finalzf3zf3) =
						((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				{
					BgL_tclassz00_bglt BgL_auxz00_6520;

					{
						obj_t BgL_auxz00_6521;

						{	/* Object/class.scm 34 */
							BgL_objectz00_bglt BgL_tmpz00_6522;

							BgL_tmpz00_6522 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1094z00_3328));
							BgL_auxz00_6521 = BGL_OBJECT_WIDENING(BgL_tmpz00_6522);
						}
						BgL_auxz00_6520 = ((BgL_tclassz00_bglt) BgL_auxz00_6521);
					}
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6520))->
							BgL_constructorz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_tclassz00_bglt BgL_auxz00_6528;

					{
						obj_t BgL_auxz00_6529;

						{	/* Object/class.scm 34 */
							BgL_objectz00_bglt BgL_tmpz00_6530;

							BgL_tmpz00_6530 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1094z00_3328));
							BgL_auxz00_6529 = BGL_OBJECT_WIDENING(BgL_tmpz00_6530);
						}
						BgL_auxz00_6528 = ((BgL_tclassz00_bglt) BgL_auxz00_6529);
					}
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6528))->
							BgL_virtualzd2slotszd2numberz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_tclassz00_bglt BgL_auxz00_6536;

					{
						obj_t BgL_auxz00_6537;

						{	/* Object/class.scm 34 */
							BgL_objectz00_bglt BgL_tmpz00_6538;

							BgL_tmpz00_6538 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1094z00_3328));
							BgL_auxz00_6537 = BGL_OBJECT_WIDENING(BgL_tmpz00_6538);
						}
						BgL_auxz00_6536 = ((BgL_tclassz00_bglt) BgL_auxz00_6537);
					}
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6536))->
							BgL_abstractzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				{
					BgL_tclassz00_bglt BgL_auxz00_6544;

					{
						obj_t BgL_auxz00_6545;

						{	/* Object/class.scm 34 */
							BgL_objectz00_bglt BgL_tmpz00_6546;

							BgL_tmpz00_6546 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1094z00_3328));
							BgL_auxz00_6545 = BGL_OBJECT_WIDENING(BgL_tmpz00_6546);
						}
						BgL_auxz00_6544 = ((BgL_tclassz00_bglt) BgL_auxz00_6545);
					}
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6544))->
							BgL_widezd2typezd2) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_tclassz00_bglt BgL_auxz00_6552;

					{
						obj_t BgL_auxz00_6553;

						{	/* Object/class.scm 34 */
							BgL_objectz00_bglt BgL_tmpz00_6554;

							BgL_tmpz00_6554 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1094z00_3328));
							BgL_auxz00_6553 = BGL_OBJECT_WIDENING(BgL_tmpz00_6554);
						}
						BgL_auxz00_6552 = ((BgL_tclassz00_bglt) BgL_auxz00_6553);
					}
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6552))->
							BgL_subclassesz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_6414 = ((BgL_typez00_bglt) BgL_new1094z00_3328);
				return ((obj_t) BgL_auxz00_6414);
			}
		}

	}



/* &lambda1617 */
	BgL_typez00_bglt BGl_z62lambda1617z62zzobject_classz00(obj_t BgL_envz00_3329,
		obj_t BgL_o1091z00_3330)
	{
		{	/* Object/class.scm 34 */
			{	/* Object/class.scm 34 */
				BgL_tclassz00_bglt BgL_wide1093z00_3564;

				BgL_wide1093z00_3564 =
					((BgL_tclassz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_tclassz00_bgl))));
				{	/* Object/class.scm 34 */
					obj_t BgL_auxz00_6567;
					BgL_objectz00_bglt BgL_tmpz00_6563;

					BgL_auxz00_6567 = ((obj_t) BgL_wide1093z00_3564);
					BgL_tmpz00_6563 =
						((BgL_objectz00_bglt)
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1091z00_3330)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6563, BgL_auxz00_6567);
				}
				((BgL_objectz00_bglt)
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1091z00_3330)));
				{	/* Object/class.scm 34 */
					long BgL_arg1625z00_3565;

					BgL_arg1625z00_3565 = BGL_CLASS_NUM(BGl_tclassz00zzobject_classz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_typez00_bglt)
								((BgL_typez00_bglt) BgL_o1091z00_3330))), BgL_arg1625z00_3565);
				}
				return
					((BgL_typez00_bglt)
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1091z00_3330)));
			}
		}

	}



/* &lambda1614 */
	BgL_typez00_bglt BGl_z62lambda1614z62zzobject_classz00(obj_t BgL_envz00_3331,
		obj_t BgL_id1064z00_3332, obj_t BgL_name1065z00_3333,
		obj_t BgL_siza7e1066za7_3334, obj_t BgL_class1067z00_3335,
		obj_t BgL_coercezd2to1068zd2_3336, obj_t BgL_parents1069z00_3337,
		obj_t BgL_initzf31070zf3_3338, obj_t BgL_magiczf31071zf3_3339,
		obj_t BgL_null1072z00_3340, obj_t BgL_z421073z42_3341,
		obj_t BgL_alias1074z00_3342, obj_t BgL_pointedzd2tozd2by1075z00_3343,
		obj_t BgL_tvector1076z00_3344, obj_t BgL_location1077z00_3345,
		obj_t BgL_importzd2location1078zd2_3346, obj_t BgL_occurrence1079z00_3347,
		obj_t BgL_itszd2super1080zd2_3348, obj_t BgL_slots1081z00_3349,
		obj_t BgL_holder1082z00_3350, obj_t BgL_widening1083z00_3351,
		obj_t BgL_depth1084z00_3352, obj_t BgL_finalzf31085zf3_3353,
		obj_t BgL_constructor1086z00_3354,
		obj_t BgL_virtualzd2slotszd2number1087z00_3355,
		obj_t BgL_abstractzf31088zf3_3356, obj_t BgL_widezd2type1089zd2_3357,
		obj_t BgL_subclasses1090z00_3358)
	{
		{	/* Object/class.scm 34 */
			{	/* Object/class.scm 34 */
				bool_t BgL_initzf31070zf3_3567;
				bool_t BgL_magiczf31071zf3_3568;
				int BgL_occurrence1079z00_3569;
				long BgL_depth1084z00_3571;
				bool_t BgL_finalzf31085zf3_3572;
				bool_t BgL_abstractzf31088zf3_3573;

				BgL_initzf31070zf3_3567 = CBOOL(BgL_initzf31070zf3_3338);
				BgL_magiczf31071zf3_3568 = CBOOL(BgL_magiczf31071zf3_3339);
				BgL_occurrence1079z00_3569 = CINT(BgL_occurrence1079z00_3347);
				BgL_depth1084z00_3571 = (long) CINT(BgL_depth1084z00_3352);
				BgL_finalzf31085zf3_3572 = CBOOL(BgL_finalzf31085zf3_3353);
				BgL_abstractzf31088zf3_3573 = CBOOL(BgL_abstractzf31088zf3_3356);
				{	/* Object/class.scm 34 */
					BgL_typez00_bglt BgL_new1178z00_3575;

					{	/* Object/class.scm 34 */
						BgL_typez00_bglt BgL_tmp1176z00_3576;
						BgL_tclassz00_bglt BgL_wide1177z00_3577;

						{
							BgL_typez00_bglt BgL_auxz00_6587;

							{	/* Object/class.scm 34 */
								BgL_typez00_bglt BgL_new1175z00_3578;

								BgL_new1175z00_3578 =
									((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_typez00_bgl))));
								{	/* Object/class.scm 34 */
									long BgL_arg1616z00_3579;

									BgL_arg1616z00_3579 =
										BGL_CLASS_NUM(BGl_typez00zztype_typez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1175z00_3578),
										BgL_arg1616z00_3579);
								}
								{	/* Object/class.scm 34 */
									BgL_objectz00_bglt BgL_tmpz00_6592;

									BgL_tmpz00_6592 = ((BgL_objectz00_bglt) BgL_new1175z00_3578);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6592, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1175z00_3578);
								BgL_auxz00_6587 = BgL_new1175z00_3578;
							}
							BgL_tmp1176z00_3576 = ((BgL_typez00_bglt) BgL_auxz00_6587);
						}
						BgL_wide1177z00_3577 =
							((BgL_tclassz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_tclassz00_bgl))));
						{	/* Object/class.scm 34 */
							obj_t BgL_auxz00_6600;
							BgL_objectz00_bglt BgL_tmpz00_6598;

							BgL_auxz00_6600 = ((obj_t) BgL_wide1177z00_3577);
							BgL_tmpz00_6598 = ((BgL_objectz00_bglt) BgL_tmp1176z00_3576);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6598, BgL_auxz00_6600);
						}
						((BgL_objectz00_bglt) BgL_tmp1176z00_3576);
						{	/* Object/class.scm 34 */
							long BgL_arg1615z00_3580;

							BgL_arg1615z00_3580 =
								BGL_CLASS_NUM(BGl_tclassz00zzobject_classz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1176z00_3576),
								BgL_arg1615z00_3580);
						}
						BgL_new1178z00_3575 = ((BgL_typez00_bglt) BgL_tmp1176z00_3576);
					}
					((((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt) BgL_new1178z00_3575)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1064z00_3332)), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1178z00_3575)))->BgL_namez00) =
						((obj_t) BgL_name1065z00_3333), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1178z00_3575)))->BgL_siza7eza7) =
						((obj_t) BgL_siza7e1066za7_3334), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1178z00_3575)))->BgL_classz00) =
						((obj_t) BgL_class1067z00_3335), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1178z00_3575)))->BgL_coercezd2tozd2) =
						((obj_t) BgL_coercezd2to1068zd2_3336), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1178z00_3575)))->BgL_parentsz00) =
						((obj_t) BgL_parents1069z00_3337), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1178z00_3575)))->BgL_initzf3zf3) =
						((bool_t) BgL_initzf31070zf3_3567), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1178z00_3575)))->BgL_magiczf3zf3) =
						((bool_t) BgL_magiczf31071zf3_3568), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1178z00_3575)))->BgL_nullz00) =
						((obj_t) BgL_null1072z00_3340), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1178z00_3575)))->BgL_z42z42) =
						((obj_t) BgL_z421073z42_3341), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1178z00_3575)))->BgL_aliasz00) =
						((obj_t) BgL_alias1074z00_3342), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1178z00_3575)))->BgL_pointedzd2tozd2byz00) =
						((obj_t) BgL_pointedzd2tozd2by1075z00_3343), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1178z00_3575)))->BgL_tvectorz00) =
						((obj_t) BgL_tvector1076z00_3344), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1178z00_3575)))->BgL_locationz00) =
						((obj_t) BgL_location1077z00_3345), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1178z00_3575)))->BgL_importzd2locationzd2) =
						((obj_t) BgL_importzd2location1078zd2_3346), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1178z00_3575)))->BgL_occurrencez00) =
						((int) BgL_occurrence1079z00_3569), BUNSPEC);
					{
						BgL_tclassz00_bglt BgL_auxz00_6641;

						{
							obj_t BgL_auxz00_6642;

							{	/* Object/class.scm 34 */
								BgL_objectz00_bglt BgL_tmpz00_6643;

								BgL_tmpz00_6643 = ((BgL_objectz00_bglt) BgL_new1178z00_3575);
								BgL_auxz00_6642 = BGL_OBJECT_WIDENING(BgL_tmpz00_6643);
							}
							BgL_auxz00_6641 = ((BgL_tclassz00_bglt) BgL_auxz00_6642);
						}
						((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6641))->
								BgL_itszd2superzd2) =
							((obj_t) BgL_itszd2super1080zd2_3348), BUNSPEC);
					}
					{
						BgL_tclassz00_bglt BgL_auxz00_6648;

						{
							obj_t BgL_auxz00_6649;

							{	/* Object/class.scm 34 */
								BgL_objectz00_bglt BgL_tmpz00_6650;

								BgL_tmpz00_6650 = ((BgL_objectz00_bglt) BgL_new1178z00_3575);
								BgL_auxz00_6649 = BGL_OBJECT_WIDENING(BgL_tmpz00_6650);
							}
							BgL_auxz00_6648 = ((BgL_tclassz00_bglt) BgL_auxz00_6649);
						}
						((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6648))->BgL_slotsz00) =
							((obj_t) BgL_slots1081z00_3349), BUNSPEC);
					}
					{
						BgL_tclassz00_bglt BgL_auxz00_6655;

						{
							obj_t BgL_auxz00_6656;

							{	/* Object/class.scm 34 */
								BgL_objectz00_bglt BgL_tmpz00_6657;

								BgL_tmpz00_6657 = ((BgL_objectz00_bglt) BgL_new1178z00_3575);
								BgL_auxz00_6656 = BGL_OBJECT_WIDENING(BgL_tmpz00_6657);
							}
							BgL_auxz00_6655 = ((BgL_tclassz00_bglt) BgL_auxz00_6656);
						}
						((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6655))->BgL_holderz00) =
							((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
									BgL_holder1082z00_3350)), BUNSPEC);
					}
					{
						BgL_tclassz00_bglt BgL_auxz00_6663;

						{
							obj_t BgL_auxz00_6664;

							{	/* Object/class.scm 34 */
								BgL_objectz00_bglt BgL_tmpz00_6665;

								BgL_tmpz00_6665 = ((BgL_objectz00_bglt) BgL_new1178z00_3575);
								BgL_auxz00_6664 = BGL_OBJECT_WIDENING(BgL_tmpz00_6665);
							}
							BgL_auxz00_6663 = ((BgL_tclassz00_bglt) BgL_auxz00_6664);
						}
						((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6663))->
								BgL_wideningz00) = ((obj_t) BgL_widening1083z00_3351), BUNSPEC);
					}
					{
						BgL_tclassz00_bglt BgL_auxz00_6670;

						{
							obj_t BgL_auxz00_6671;

							{	/* Object/class.scm 34 */
								BgL_objectz00_bglt BgL_tmpz00_6672;

								BgL_tmpz00_6672 = ((BgL_objectz00_bglt) BgL_new1178z00_3575);
								BgL_auxz00_6671 = BGL_OBJECT_WIDENING(BgL_tmpz00_6672);
							}
							BgL_auxz00_6670 = ((BgL_tclassz00_bglt) BgL_auxz00_6671);
						}
						((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6670))->BgL_depthz00) =
							((long) BgL_depth1084z00_3571), BUNSPEC);
					}
					{
						BgL_tclassz00_bglt BgL_auxz00_6677;

						{
							obj_t BgL_auxz00_6678;

							{	/* Object/class.scm 34 */
								BgL_objectz00_bglt BgL_tmpz00_6679;

								BgL_tmpz00_6679 = ((BgL_objectz00_bglt) BgL_new1178z00_3575);
								BgL_auxz00_6678 = BGL_OBJECT_WIDENING(BgL_tmpz00_6679);
							}
							BgL_auxz00_6677 = ((BgL_tclassz00_bglt) BgL_auxz00_6678);
						}
						((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6677))->
								BgL_finalzf3zf3) =
							((bool_t) BgL_finalzf31085zf3_3572), BUNSPEC);
					}
					{
						BgL_tclassz00_bglt BgL_auxz00_6684;

						{
							obj_t BgL_auxz00_6685;

							{	/* Object/class.scm 34 */
								BgL_objectz00_bglt BgL_tmpz00_6686;

								BgL_tmpz00_6686 = ((BgL_objectz00_bglt) BgL_new1178z00_3575);
								BgL_auxz00_6685 = BGL_OBJECT_WIDENING(BgL_tmpz00_6686);
							}
							BgL_auxz00_6684 = ((BgL_tclassz00_bglt) BgL_auxz00_6685);
						}
						((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6684))->
								BgL_constructorz00) =
							((obj_t) BgL_constructor1086z00_3354), BUNSPEC);
					}
					{
						BgL_tclassz00_bglt BgL_auxz00_6691;

						{
							obj_t BgL_auxz00_6692;

							{	/* Object/class.scm 34 */
								BgL_objectz00_bglt BgL_tmpz00_6693;

								BgL_tmpz00_6693 = ((BgL_objectz00_bglt) BgL_new1178z00_3575);
								BgL_auxz00_6692 = BGL_OBJECT_WIDENING(BgL_tmpz00_6693);
							}
							BgL_auxz00_6691 = ((BgL_tclassz00_bglt) BgL_auxz00_6692);
						}
						((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6691))->
								BgL_virtualzd2slotszd2numberz00) =
							((obj_t) BgL_virtualzd2slotszd2number1087z00_3355), BUNSPEC);
					}
					{
						BgL_tclassz00_bglt BgL_auxz00_6698;

						{
							obj_t BgL_auxz00_6699;

							{	/* Object/class.scm 34 */
								BgL_objectz00_bglt BgL_tmpz00_6700;

								BgL_tmpz00_6700 = ((BgL_objectz00_bglt) BgL_new1178z00_3575);
								BgL_auxz00_6699 = BGL_OBJECT_WIDENING(BgL_tmpz00_6700);
							}
							BgL_auxz00_6698 = ((BgL_tclassz00_bglt) BgL_auxz00_6699);
						}
						((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6698))->
								BgL_abstractzf3zf3) =
							((bool_t) BgL_abstractzf31088zf3_3573), BUNSPEC);
					}
					{
						BgL_tclassz00_bglt BgL_auxz00_6705;

						{
							obj_t BgL_auxz00_6706;

							{	/* Object/class.scm 34 */
								BgL_objectz00_bglt BgL_tmpz00_6707;

								BgL_tmpz00_6707 = ((BgL_objectz00_bglt) BgL_new1178z00_3575);
								BgL_auxz00_6706 = BGL_OBJECT_WIDENING(BgL_tmpz00_6707);
							}
							BgL_auxz00_6705 = ((BgL_tclassz00_bglt) BgL_auxz00_6706);
						}
						((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6705))->
								BgL_widezd2typezd2) =
							((obj_t) BgL_widezd2type1089zd2_3357), BUNSPEC);
					}
					{
						BgL_tclassz00_bglt BgL_auxz00_6712;

						{
							obj_t BgL_auxz00_6713;

							{	/* Object/class.scm 34 */
								BgL_objectz00_bglt BgL_tmpz00_6714;

								BgL_tmpz00_6714 = ((BgL_objectz00_bglt) BgL_new1178z00_3575);
								BgL_auxz00_6713 = BGL_OBJECT_WIDENING(BgL_tmpz00_6714);
							}
							BgL_auxz00_6712 = ((BgL_tclassz00_bglt) BgL_auxz00_6713);
						}
						((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6712))->
								BgL_subclassesz00) =
							((obj_t) ((obj_t) BgL_subclasses1090z00_3358)), BUNSPEC);
					}
					return BgL_new1178z00_3575;
				}
			}
		}

	}



/* &<@anonymous:1774> */
	obj_t BGl_z62zc3z04anonymousza31774ze3ze5zzobject_classz00(obj_t
		BgL_envz00_3359)
	{
		{	/* Object/class.scm 34 */
			return BNIL;
		}

	}



/* &lambda1773 */
	obj_t BGl_z62lambda1773z62zzobject_classz00(obj_t BgL_envz00_3360,
		obj_t BgL_oz00_3361, obj_t BgL_vz00_3362)
	{
		{	/* Object/class.scm 34 */
			{
				BgL_tclassz00_bglt BgL_auxz00_6720;

				{
					obj_t BgL_auxz00_6721;

					{	/* Object/class.scm 34 */
						BgL_objectz00_bglt BgL_tmpz00_6722;

						BgL_tmpz00_6722 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3361));
						BgL_auxz00_6721 = BGL_OBJECT_WIDENING(BgL_tmpz00_6722);
					}
					BgL_auxz00_6720 = ((BgL_tclassz00_bglt) BgL_auxz00_6721);
				}
				return
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6720))->
						BgL_subclassesz00) = ((obj_t) ((obj_t) BgL_vz00_3362)), BUNSPEC);
			}
		}

	}



/* &lambda1772 */
	obj_t BGl_z62lambda1772z62zzobject_classz00(obj_t BgL_envz00_3363,
		obj_t BgL_oz00_3364)
	{
		{	/* Object/class.scm 34 */
			{
				BgL_tclassz00_bglt BgL_auxz00_6729;

				{
					obj_t BgL_auxz00_6730;

					{	/* Object/class.scm 34 */
						BgL_objectz00_bglt BgL_tmpz00_6731;

						BgL_tmpz00_6731 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3364));
						BgL_auxz00_6730 = BGL_OBJECT_WIDENING(BgL_tmpz00_6731);
					}
					BgL_auxz00_6729 = ((BgL_tclassz00_bglt) BgL_auxz00_6730);
				}
				return
					(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6729))->BgL_subclassesz00);
			}
		}

	}



/* &<@anonymous:1764> */
	obj_t BGl_z62zc3z04anonymousza31764ze3ze5zzobject_classz00(obj_t
		BgL_envz00_3365)
	{
		{	/* Object/class.scm 34 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1763 */
	obj_t BGl_z62lambda1763z62zzobject_classz00(obj_t BgL_envz00_3366,
		obj_t BgL_oz00_3367, obj_t BgL_vz00_3368)
	{
		{	/* Object/class.scm 34 */
			{
				BgL_tclassz00_bglt BgL_auxz00_6738;

				{
					obj_t BgL_auxz00_6739;

					{	/* Object/class.scm 34 */
						BgL_objectz00_bglt BgL_tmpz00_6740;

						BgL_tmpz00_6740 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3367));
						BgL_auxz00_6739 = BGL_OBJECT_WIDENING(BgL_tmpz00_6740);
					}
					BgL_auxz00_6738 = ((BgL_tclassz00_bglt) BgL_auxz00_6739);
				}
				return
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6738))->
						BgL_widezd2typezd2) = ((obj_t) BgL_vz00_3368), BUNSPEC);
			}
		}

	}



/* &lambda1762 */
	obj_t BGl_z62lambda1762z62zzobject_classz00(obj_t BgL_envz00_3369,
		obj_t BgL_oz00_3370)
	{
		{	/* Object/class.scm 34 */
			{
				BgL_tclassz00_bglt BgL_auxz00_6746;

				{
					obj_t BgL_auxz00_6747;

					{	/* Object/class.scm 34 */
						BgL_objectz00_bglt BgL_tmpz00_6748;

						BgL_tmpz00_6748 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3370));
						BgL_auxz00_6747 = BGL_OBJECT_WIDENING(BgL_tmpz00_6748);
					}
					BgL_auxz00_6746 = ((BgL_tclassz00_bglt) BgL_auxz00_6747);
				}
				return
					(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6746))->BgL_widezd2typezd2);
			}
		}

	}



/* &<@anonymous:1752> */
	obj_t BGl_z62zc3z04anonymousza31752ze3ze5zzobject_classz00(obj_t
		BgL_envz00_3371)
	{
		{	/* Object/class.scm 34 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1751 */
	obj_t BGl_z62lambda1751z62zzobject_classz00(obj_t BgL_envz00_3372,
		obj_t BgL_oz00_3373, obj_t BgL_vz00_3374)
	{
		{	/* Object/class.scm 34 */
			{	/* Object/class.scm 34 */
				bool_t BgL_vz00_3587;

				BgL_vz00_3587 = CBOOL(BgL_vz00_3374);
				{
					BgL_tclassz00_bglt BgL_auxz00_6756;

					{
						obj_t BgL_auxz00_6757;

						{	/* Object/class.scm 34 */
							BgL_objectz00_bglt BgL_tmpz00_6758;

							BgL_tmpz00_6758 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3373));
							BgL_auxz00_6757 = BGL_OBJECT_WIDENING(BgL_tmpz00_6758);
						}
						BgL_auxz00_6756 = ((BgL_tclassz00_bglt) BgL_auxz00_6757);
					}
					return
						((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6756))->
							BgL_abstractzf3zf3) = ((bool_t) BgL_vz00_3587), BUNSPEC);
				}
			}
		}

	}



/* &lambda1750 */
	obj_t BGl_z62lambda1750z62zzobject_classz00(obj_t BgL_envz00_3375,
		obj_t BgL_oz00_3376)
	{
		{	/* Object/class.scm 34 */
			{	/* Object/class.scm 34 */
				bool_t BgL_tmpz00_6764;

				{
					BgL_tclassz00_bglt BgL_auxz00_6765;

					{
						obj_t BgL_auxz00_6766;

						{	/* Object/class.scm 34 */
							BgL_objectz00_bglt BgL_tmpz00_6767;

							BgL_tmpz00_6767 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3376));
							BgL_auxz00_6766 = BGL_OBJECT_WIDENING(BgL_tmpz00_6767);
						}
						BgL_auxz00_6765 = ((BgL_tclassz00_bglt) BgL_auxz00_6766);
					}
					BgL_tmpz00_6764 =
						(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6765))->
						BgL_abstractzf3zf3);
				}
				return BBOOL(BgL_tmpz00_6764);
			}
		}

	}



/* &<@anonymous:1743> */
	obj_t BGl_z62zc3z04anonymousza31743ze3ze5zzobject_classz00(obj_t
		BgL_envz00_3377)
	{
		{	/* Object/class.scm 34 */
			return BINT(0L);
		}

	}



/* &lambda1742 */
	obj_t BGl_z62lambda1742z62zzobject_classz00(obj_t BgL_envz00_3378,
		obj_t BgL_oz00_3379, obj_t BgL_vz00_3380)
	{
		{	/* Object/class.scm 34 */
			{
				BgL_tclassz00_bglt BgL_auxz00_6775;

				{
					obj_t BgL_auxz00_6776;

					{	/* Object/class.scm 34 */
						BgL_objectz00_bglt BgL_tmpz00_6777;

						BgL_tmpz00_6777 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3379));
						BgL_auxz00_6776 = BGL_OBJECT_WIDENING(BgL_tmpz00_6777);
					}
					BgL_auxz00_6775 = ((BgL_tclassz00_bglt) BgL_auxz00_6776);
				}
				return
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6775))->
						BgL_virtualzd2slotszd2numberz00) =
					((obj_t) BgL_vz00_3380), BUNSPEC);
			}
		}

	}



/* &lambda1741 */
	obj_t BGl_z62lambda1741z62zzobject_classz00(obj_t BgL_envz00_3381,
		obj_t BgL_oz00_3382)
	{
		{	/* Object/class.scm 34 */
			{
				BgL_tclassz00_bglt BgL_auxz00_6783;

				{
					obj_t BgL_auxz00_6784;

					{	/* Object/class.scm 34 */
						BgL_objectz00_bglt BgL_tmpz00_6785;

						BgL_tmpz00_6785 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3382));
						BgL_auxz00_6784 = BGL_OBJECT_WIDENING(BgL_tmpz00_6785);
					}
					BgL_auxz00_6783 = ((BgL_tclassz00_bglt) BgL_auxz00_6784);
				}
				return
					(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6783))->
					BgL_virtualzd2slotszd2numberz00);
			}
		}

	}



/* &lambda1736 */
	obj_t BGl_z62lambda1736z62zzobject_classz00(obj_t BgL_envz00_3383,
		obj_t BgL_oz00_3384, obj_t BgL_vz00_3385)
	{
		{	/* Object/class.scm 34 */
			{
				BgL_tclassz00_bglt BgL_auxz00_6791;

				{
					obj_t BgL_auxz00_6792;

					{	/* Object/class.scm 34 */
						BgL_objectz00_bglt BgL_tmpz00_6793;

						BgL_tmpz00_6793 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3384));
						BgL_auxz00_6792 = BGL_OBJECT_WIDENING(BgL_tmpz00_6793);
					}
					BgL_auxz00_6791 = ((BgL_tclassz00_bglt) BgL_auxz00_6792);
				}
				return
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6791))->
						BgL_constructorz00) = ((obj_t) BgL_vz00_3385), BUNSPEC);
			}
		}

	}



/* &lambda1735 */
	obj_t BGl_z62lambda1735z62zzobject_classz00(obj_t BgL_envz00_3386,
		obj_t BgL_oz00_3387)
	{
		{	/* Object/class.scm 34 */
			{
				BgL_tclassz00_bglt BgL_auxz00_6799;

				{
					obj_t BgL_auxz00_6800;

					{	/* Object/class.scm 34 */
						BgL_objectz00_bglt BgL_tmpz00_6801;

						BgL_tmpz00_6801 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3387));
						BgL_auxz00_6800 = BGL_OBJECT_WIDENING(BgL_tmpz00_6801);
					}
					BgL_auxz00_6799 = ((BgL_tclassz00_bglt) BgL_auxz00_6800);
				}
				return
					(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6799))->BgL_constructorz00);
			}
		}

	}



/* &<@anonymous:1723> */
	obj_t BGl_z62zc3z04anonymousza31723ze3ze5zzobject_classz00(obj_t
		BgL_envz00_3388)
	{
		{	/* Object/class.scm 34 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1722 */
	obj_t BGl_z62lambda1722z62zzobject_classz00(obj_t BgL_envz00_3389,
		obj_t BgL_oz00_3390, obj_t BgL_vz00_3391)
	{
		{	/* Object/class.scm 34 */
			{	/* Object/class.scm 34 */
				bool_t BgL_vz00_3594;

				BgL_vz00_3594 = CBOOL(BgL_vz00_3391);
				{
					BgL_tclassz00_bglt BgL_auxz00_6809;

					{
						obj_t BgL_auxz00_6810;

						{	/* Object/class.scm 34 */
							BgL_objectz00_bglt BgL_tmpz00_6811;

							BgL_tmpz00_6811 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3390));
							BgL_auxz00_6810 = BGL_OBJECT_WIDENING(BgL_tmpz00_6811);
						}
						BgL_auxz00_6809 = ((BgL_tclassz00_bglt) BgL_auxz00_6810);
					}
					return
						((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6809))->
							BgL_finalzf3zf3) = ((bool_t) BgL_vz00_3594), BUNSPEC);
				}
			}
		}

	}



/* &lambda1721 */
	obj_t BGl_z62lambda1721z62zzobject_classz00(obj_t BgL_envz00_3392,
		obj_t BgL_oz00_3393)
	{
		{	/* Object/class.scm 34 */
			{	/* Object/class.scm 34 */
				bool_t BgL_tmpz00_6817;

				{
					BgL_tclassz00_bglt BgL_auxz00_6818;

					{
						obj_t BgL_auxz00_6819;

						{	/* Object/class.scm 34 */
							BgL_objectz00_bglt BgL_tmpz00_6820;

							BgL_tmpz00_6820 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3393));
							BgL_auxz00_6819 = BGL_OBJECT_WIDENING(BgL_tmpz00_6820);
						}
						BgL_auxz00_6818 = ((BgL_tclassz00_bglt) BgL_auxz00_6819);
					}
					BgL_tmpz00_6817 =
						(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6818))->BgL_finalzf3zf3);
				}
				return BBOOL(BgL_tmpz00_6817);
			}
		}

	}



/* &<@anonymous:1713> */
	obj_t BGl_z62zc3z04anonymousza31713ze3ze5zzobject_classz00(obj_t
		BgL_envz00_3394)
	{
		{	/* Object/class.scm 34 */
			return BINT(0L);
		}

	}



/* &lambda1712 */
	obj_t BGl_z62lambda1712z62zzobject_classz00(obj_t BgL_envz00_3395,
		obj_t BgL_oz00_3396, obj_t BgL_vz00_3397)
	{
		{	/* Object/class.scm 34 */
			{	/* Object/class.scm 34 */
				long BgL_vz00_3597;

				BgL_vz00_3597 = (long) CINT(BgL_vz00_3397);
				{
					BgL_tclassz00_bglt BgL_auxz00_6829;

					{
						obj_t BgL_auxz00_6830;

						{	/* Object/class.scm 34 */
							BgL_objectz00_bglt BgL_tmpz00_6831;

							BgL_tmpz00_6831 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3396));
							BgL_auxz00_6830 = BGL_OBJECT_WIDENING(BgL_tmpz00_6831);
						}
						BgL_auxz00_6829 = ((BgL_tclassz00_bglt) BgL_auxz00_6830);
					}
					return
						((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6829))->BgL_depthz00) =
						((long) BgL_vz00_3597), BUNSPEC);
		}}}

	}



/* &lambda1711 */
	obj_t BGl_z62lambda1711z62zzobject_classz00(obj_t BgL_envz00_3398,
		obj_t BgL_oz00_3399)
	{
		{	/* Object/class.scm 34 */
			{	/* Object/class.scm 34 */
				long BgL_tmpz00_6837;

				{
					BgL_tclassz00_bglt BgL_auxz00_6838;

					{
						obj_t BgL_auxz00_6839;

						{	/* Object/class.scm 34 */
							BgL_objectz00_bglt BgL_tmpz00_6840;

							BgL_tmpz00_6840 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3399));
							BgL_auxz00_6839 = BGL_OBJECT_WIDENING(BgL_tmpz00_6840);
						}
						BgL_auxz00_6838 = ((BgL_tclassz00_bglt) BgL_auxz00_6839);
					}
					BgL_tmpz00_6837 =
						(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6838))->BgL_depthz00);
				}
				return BINT(BgL_tmpz00_6837);
			}
		}

	}



/* &<@anonymous:1704> */
	obj_t BGl_z62zc3z04anonymousza31704ze3ze5zzobject_classz00(obj_t
		BgL_envz00_3400)
	{
		{	/* Object/class.scm 34 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1703 */
	obj_t BGl_z62lambda1703z62zzobject_classz00(obj_t BgL_envz00_3401,
		obj_t BgL_oz00_3402, obj_t BgL_vz00_3403)
	{
		{	/* Object/class.scm 34 */
			{
				BgL_tclassz00_bglt BgL_auxz00_6848;

				{
					obj_t BgL_auxz00_6849;

					{	/* Object/class.scm 34 */
						BgL_objectz00_bglt BgL_tmpz00_6850;

						BgL_tmpz00_6850 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3402));
						BgL_auxz00_6849 = BGL_OBJECT_WIDENING(BgL_tmpz00_6850);
					}
					BgL_auxz00_6848 = ((BgL_tclassz00_bglt) BgL_auxz00_6849);
				}
				return
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6848))->BgL_wideningz00) =
					((obj_t) BgL_vz00_3403), BUNSPEC);
			}
		}

	}



/* &lambda1702 */
	obj_t BGl_z62lambda1702z62zzobject_classz00(obj_t BgL_envz00_3404,
		obj_t BgL_oz00_3405)
	{
		{	/* Object/class.scm 34 */
			{
				BgL_tclassz00_bglt BgL_auxz00_6856;

				{
					obj_t BgL_auxz00_6857;

					{	/* Object/class.scm 34 */
						BgL_objectz00_bglt BgL_tmpz00_6858;

						BgL_tmpz00_6858 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3405));
						BgL_auxz00_6857 = BGL_OBJECT_WIDENING(BgL_tmpz00_6858);
					}
					BgL_auxz00_6856 = ((BgL_tclassz00_bglt) BgL_auxz00_6857);
				}
				return
					(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6856))->BgL_wideningz00);
			}
		}

	}



/* &lambda1691 */
	obj_t BGl_z62lambda1691z62zzobject_classz00(obj_t BgL_envz00_3406,
		obj_t BgL_oz00_3407, obj_t BgL_vz00_3408)
	{
		{	/* Object/class.scm 34 */
			{
				BgL_tclassz00_bglt BgL_auxz00_6864;

				{
					obj_t BgL_auxz00_6865;

					{	/* Object/class.scm 34 */
						BgL_objectz00_bglt BgL_tmpz00_6866;

						BgL_tmpz00_6866 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3407));
						BgL_auxz00_6865 = BGL_OBJECT_WIDENING(BgL_tmpz00_6866);
					}
					BgL_auxz00_6864 = ((BgL_tclassz00_bglt) BgL_auxz00_6865);
				}
				return
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6864))->BgL_holderz00) =
					((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_vz00_3408)), BUNSPEC);
			}
		}

	}



/* &lambda1690 */
	BgL_globalz00_bglt BGl_z62lambda1690z62zzobject_classz00(obj_t
		BgL_envz00_3409, obj_t BgL_oz00_3410)
	{
		{	/* Object/class.scm 34 */
			{
				BgL_tclassz00_bglt BgL_auxz00_6873;

				{
					obj_t BgL_auxz00_6874;

					{	/* Object/class.scm 34 */
						BgL_objectz00_bglt BgL_tmpz00_6875;

						BgL_tmpz00_6875 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3410));
						BgL_auxz00_6874 = BGL_OBJECT_WIDENING(BgL_tmpz00_6875);
					}
					BgL_auxz00_6873 = ((BgL_tclassz00_bglt) BgL_auxz00_6874);
				}
				return (((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6873))->BgL_holderz00);
			}
		}

	}



/* &<@anonymous:1678> */
	obj_t BGl_z62zc3z04anonymousza31678ze3ze5zzobject_classz00(obj_t
		BgL_envz00_3411)
	{
		{	/* Object/class.scm 34 */
			return BUNSPEC;
		}

	}



/* &lambda1677 */
	obj_t BGl_z62lambda1677z62zzobject_classz00(obj_t BgL_envz00_3412,
		obj_t BgL_oz00_3413, obj_t BgL_vz00_3414)
	{
		{	/* Object/class.scm 34 */
			{
				BgL_tclassz00_bglt BgL_auxz00_6881;

				{
					obj_t BgL_auxz00_6882;

					{	/* Object/class.scm 34 */
						BgL_objectz00_bglt BgL_tmpz00_6883;

						BgL_tmpz00_6883 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3413));
						BgL_auxz00_6882 = BGL_OBJECT_WIDENING(BgL_tmpz00_6883);
					}
					BgL_auxz00_6881 = ((BgL_tclassz00_bglt) BgL_auxz00_6882);
				}
				return
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6881))->BgL_slotsz00) =
					((obj_t) BgL_vz00_3414), BUNSPEC);
			}
		}

	}



/* &lambda1676 */
	obj_t BGl_z62lambda1676z62zzobject_classz00(obj_t BgL_envz00_3415,
		obj_t BgL_oz00_3416)
	{
		{	/* Object/class.scm 34 */
			{
				BgL_tclassz00_bglt BgL_auxz00_6889;

				{
					obj_t BgL_auxz00_6890;

					{	/* Object/class.scm 34 */
						BgL_objectz00_bglt BgL_tmpz00_6891;

						BgL_tmpz00_6891 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3416));
						BgL_auxz00_6890 = BGL_OBJECT_WIDENING(BgL_tmpz00_6891);
					}
					BgL_auxz00_6889 = ((BgL_tclassz00_bglt) BgL_auxz00_6890);
				}
				return (((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6889))->BgL_slotsz00);
			}
		}

	}



/* &lambda1653 */
	obj_t BGl_z62lambda1653z62zzobject_classz00(obj_t BgL_envz00_3417,
		obj_t BgL_oz00_3418, obj_t BgL_vz00_3419)
	{
		{	/* Object/class.scm 34 */
			{
				BgL_tclassz00_bglt BgL_auxz00_6897;

				{
					obj_t BgL_auxz00_6898;

					{	/* Object/class.scm 34 */
						BgL_objectz00_bglt BgL_tmpz00_6899;

						BgL_tmpz00_6899 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3418));
						BgL_auxz00_6898 = BGL_OBJECT_WIDENING(BgL_tmpz00_6899);
					}
					BgL_auxz00_6897 = ((BgL_tclassz00_bglt) BgL_auxz00_6898);
				}
				return
					((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6897))->
						BgL_itszd2superzd2) = ((obj_t) BgL_vz00_3419), BUNSPEC);
			}
		}

	}



/* &lambda1652 */
	obj_t BGl_z62lambda1652z62zzobject_classz00(obj_t BgL_envz00_3420,
		obj_t BgL_oz00_3421)
	{
		{	/* Object/class.scm 34 */
			{
				BgL_tclassz00_bglt BgL_auxz00_6905;

				{
					obj_t BgL_auxz00_6906;

					{	/* Object/class.scm 34 */
						BgL_objectz00_bglt BgL_tmpz00_6907;

						BgL_tmpz00_6907 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3421));
						BgL_auxz00_6906 = BGL_OBJECT_WIDENING(BgL_tmpz00_6907);
					}
					BgL_auxz00_6905 = ((BgL_tclassz00_bglt) BgL_auxz00_6906);
				}
				return
					(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6905))->BgL_itszd2superzd2);
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzobject_classz00(void)
	{
		{	/* Object/class.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzobject_classz00(void)
	{
		{	/* Object/class.scm 15 */
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2occurrencezd2incrementz12zd2envzc0zztype_typez00,
				BGl_tclassz00zzobject_classz00, BGl_proc2029z00zzobject_classz00,
				BGl_string2030z00zzobject_classz00);
		}

	}



/* &type-occurrence-incr1234 */
	obj_t BGl_z62typezd2occurrencezd2incr1234z62zzobject_classz00(obj_t
		BgL_envz00_3425, obj_t BgL_tz00_3426)
	{
		{	/* Object/class.scm 340 */
			{

				{	/* Object/class.scm 340 */
					obj_t BgL_nextzd2method1233zd2_3610;

					BgL_nextzd2method1233zd2_3610 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_typez00_bglt) BgL_tz00_3426)),
						BGl_typezd2occurrencezd2incrementz12zd2envzc0zztype_typez00,
						BGl_tclassz00zzobject_classz00);
					BGL_PROCEDURE_CALL1(BgL_nextzd2method1233zd2_3610,
						((obj_t) ((BgL_typez00_bglt) BgL_tz00_3426)));
				}
				{	/* Object/class.scm 343 */
					bool_t BgL_test2404z00_6923;

					{	/* Object/class.scm 343 */
						obj_t BgL_tmpz00_6924;

						{
							BgL_tclassz00_bglt BgL_auxz00_6925;

							{
								obj_t BgL_auxz00_6926;

								{	/* Object/class.scm 343 */
									BgL_objectz00_bglt BgL_tmpz00_6927;

									BgL_tmpz00_6927 =
										((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_tz00_3426));
									BgL_auxz00_6926 = BGL_OBJECT_WIDENING(BgL_tmpz00_6927);
								}
								BgL_auxz00_6925 = ((BgL_tclassz00_bglt) BgL_auxz00_6926);
							}
							BgL_tmpz00_6924 =
								(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6925))->
								BgL_wideningz00);
						}
						BgL_test2404z00_6923 = CBOOL(BgL_tmpz00_6924);
					}
					if (BgL_test2404z00_6923)
						{	/* Object/class.scm 343 */
							obj_t BgL_arg1883z00_3611;

							{
								BgL_tclassz00_bglt BgL_auxz00_6934;

								{
									obj_t BgL_auxz00_6935;

									{	/* Object/class.scm 343 */
										BgL_objectz00_bglt BgL_tmpz00_6936;

										BgL_tmpz00_6936 =
											((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_tz00_3426));
										BgL_auxz00_6935 = BGL_OBJECT_WIDENING(BgL_tmpz00_6936);
									}
									BgL_auxz00_6934 = ((BgL_tclassz00_bglt) BgL_auxz00_6935);
								}
								BgL_arg1883z00_3611 =
									(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_6934))->
									BgL_itszd2superzd2);
							}
							return
								BGl_typezd2occurrencezd2incrementz12z12zztype_typez00(
								((BgL_typez00_bglt) BgL_arg1883z00_3611));
						}
					else
						{	/* Object/class.scm 343 */
							return BFALSE;
						}
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzobject_classz00(void)
	{
		{	/* Object/class.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2031z00zzobject_classz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2031z00zzobject_classz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2031z00zzobject_classz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string2031z00zzobject_classz00));
			BGl_modulezd2initializa7ationz75zztype_coercionz00(116865673L,
				BSTRING_TO_STRING(BGl_string2031z00zzobject_classz00));
			BGl_modulezd2initializa7ationz75zzobject_toolsz00(196511171L,
				BSTRING_TO_STRING(BGl_string2031z00zzobject_classz00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string2031z00zzobject_classz00));
			BGl_modulezd2initializa7ationz75zzobject_coercionz00(208160543L,
				BSTRING_TO_STRING(BGl_string2031z00zzobject_classz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string2031z00zzobject_classz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2031z00zzobject_classz00));
			BGl_modulezd2initializa7ationz75zzforeign_jtypez00(287572863L,
				BSTRING_TO_STRING(BGl_string2031z00zzobject_classz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2031z00zzobject_classz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string2031z00zzobject_classz00));
			return
				BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string2031z00zzobject_classz00));
		}

	}

#ifdef __cplusplus
}
#endif
