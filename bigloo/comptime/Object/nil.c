/*===========================================================================*/
/*   (Object/nil.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Object/nil.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_OBJECT_NIL_TYPE_DEFINITIONS
#define BGL_OBJECT_NIL_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_tclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		struct BgL_globalz00_bgl *BgL_holderz00;
		obj_t BgL_wideningz00;
		long BgL_depthz00;
		bool_t BgL_finalzf3zf3;
		obj_t BgL_constructorz00;
		obj_t BgL_virtualzd2slotszd2numberz00;
		bool_t BgL_abstractzf3zf3;
		obj_t BgL_widezd2typezd2;
		obj_t BgL_subclassesz00;
	}                *BgL_tclassz00_bglt;

	typedef struct BgL_slotz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_srcz00;
		obj_t BgL_classzd2ownerzd2;
		long BgL_indexz00;
		obj_t BgL_typez00;
		bool_t BgL_readzd2onlyzf3z21;
		obj_t BgL_defaultzd2valuezd2;
		obj_t BgL_virtualzd2numzd2;
		bool_t BgL_virtualzd2overridezd2;
		obj_t BgL_getterz00;
		obj_t BgL_setterz00;
		obj_t BgL_userzd2infozd2;
	}              *BgL_slotz00_bglt;


#endif													// BGL_OBJECT_NIL_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_za2symbolza2z00zztype_cachez00;
	static obj_t BGl_fillzd2nilzd2zzobject_nilz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_typezd2nilzd2valuez00zzobject_nilz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	extern obj_t BGl_za2intza2z00zztype_cachez00;
	static obj_t BGl_requirezd2initializa7ationz75zzobject_nilz00 = BUNSPEC;
	extern obj_t BGl_makezd2typedzd2identz00zzast_identz00(obj_t, obj_t);
	extern obj_t BGl_za2vectorza2z00zztype_cachez00;
	extern obj_t BGl_za2pairzd2nilza2zd2zztype_cachez00;
	extern obj_t BGl_za2bignumza2z00zztype_cachez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_tclassz00zzobject_classz00;
	static obj_t BGl_z62genzd2plainzd2classzd2nilzb0zzobject_nilz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_epairifyz00zztools_miscz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzobject_nilz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	extern obj_t BGl_za2cellza2z00zztype_cachez00;
	static obj_t BGl_objectzd2initzd2zzobject_nilz00(void);
	extern obj_t BGl_za2boolza2z00zztype_cachez00;
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	extern obj_t BGl_za2longza2z00zztype_cachez00;
	extern obj_t BGl_za2brealza2z00zztype_cachez00;
	extern obj_t BGl_za2stringza2z00zztype_cachez00;
	extern obj_t BGl_za2bstringza2z00zztype_cachez00;
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzobject_nilz00(void);
	extern obj_t BGl_jarrayz00zzforeign_jtypez00;
	BGL_EXPORTED_DECL obj_t
		BGl_genzd2plainzd2classzd2nilzd2zzobject_nilz00(BgL_typez00_bglt, obj_t,
		obj_t);
	static obj_t BGl_z62genzd2widezd2classzd2nilzb0zzobject_nilz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2realza2z00zztype_cachez00;
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	extern obj_t BGl_za2pairza2z00zztype_cachez00;
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2procedureza2z00zztype_cachez00;
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	extern obj_t BGl_jclassz00zzobject_classz00;
	extern obj_t BGl_za2belongza2z00zztype_cachez00;
	extern obj_t BGl_producezd2modulezd2clausez12z12zzmodule_modulez00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzobject_nilz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_jtypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_impusez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_privatez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztvector_tvectorz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_slotzd2getzd2nilz00zzobject_nilz00(obj_t);
	extern obj_t BGl_za2elongza2z00zztype_cachez00;
	static obj_t BGl_externzd2typezd2nilz00zzobject_nilz00(obj_t, obj_t);
	extern bool_t BGl_slotzd2virtualzf3z21zzobject_slotsz00(BgL_slotz00_bglt);
	static obj_t BGl_bigloozd2primitivezd2typezd2nilzd2zzobject_nilz00(obj_t,
		obj_t);
	extern obj_t BGl_za2bllongza2z00zztype_cachez00;
	extern obj_t BGl_makezd2privatezd2sexpz00zzast_privatez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_importzd2parserzd2zzmodule_impusez00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zzobject_nilz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzobject_nilz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzobject_nilz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzobject_nilz00(void);
	extern obj_t BGl_za2llongza2z00zztype_cachez00;
	extern obj_t BGl_tvecz00zztvector_tvectorz00;
	BGL_EXPORTED_DECL obj_t
		BGl_importzd2classzd2nilz00zzobject_nilz00(BgL_typez00_bglt, obj_t, obj_t);
	extern obj_t BGl_za2bnilza2z00zztype_cachez00;
	extern obj_t BGl_za2keywordza2z00zztype_cachez00;
	extern obj_t BGl_za2unspecza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t
		BGl_genzd2widezd2classzd2nilzd2zzobject_nilz00(BgL_typez00_bglt, obj_t,
		obj_t);
	extern obj_t BGl_za2epairza2z00zztype_cachez00;
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	extern obj_t BGl_za2bintza2z00zztype_cachez00;
	static obj_t BGl_z62typezd2nilzd2valuez62zzobject_nilz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2bcharza2z00zztype_cachez00;
	extern bool_t BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_bglt);
	static obj_t BGl_z62importzd2classzd2nilz62zzobject_nilz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2charza2z00zztype_cachez00;
	static obj_t __cnst[110];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genzd2plainzd2classzd2nilzd2envz00zzobject_nilz00,
		BgL_bgl_za762genza7d2plainza7d1884za7,
		BGl_z62genzd2plainzd2classzd2nilzb0zzobject_nilz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_genzd2widezd2classzd2nilzd2envz00zzobject_nilz00,
		BgL_bgl_za762genza7d2wideza7d21885za7,
		BGl_z62genzd2widezd2classzd2nilzb0zzobject_nilz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_importzd2classzd2nilzd2envzd2zzobject_nilz00,
		BgL_bgl_za762importza7d2clas1886z00,
		BGl_z62importzd2classzd2nilz62zzobject_nilz00, 0L, BUNSPEC, 3);
	      DEFINE_REAL(BGl_real1877z00zzobject_nilz00,
		BgL_bgl_real1877za700za7za7obj1887za7, 0.0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_typezd2nilzd2valuezd2envzd2zzobject_nilz00,
		BgL_bgl_za762typeza7d2nilza7d21888za7,
		BGl_z62typezd2nilzd2valuez62zzobject_nilz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1878z00zzobject_nilz00,
		BgL_bgl_string1878za700za7za7o1889za7, "", 0);
	      DEFINE_STRING(BGl_string1879z00zzobject_nilz00,
		BgL_bgl_string1879za700za7za7o1890za7, "Illegal slot type `~a'", 22);
	      DEFINE_STRING(BGl_string1880z00zzobject_nilz00,
		BgL_bgl_string1880za700za7za7o1891za7, "0L", 2);
	      DEFINE_STRING(BGl_string1881z00zzobject_nilz00,
		BgL_bgl_string1881za700za7za7o1892za7, "object_nil", 10);
	      DEFINE_STRING(BGl_string1882z00zzobject_nilz00,
		BgL_bgl_string1882za700za7za7o1893za7,
		"free-pragma string char double float byte long int cast-null (make-f64vector 0) f64vector (make-f32vector 0) f32vector (make-u64vector 0) u64vector (make-s64vector 0) s64vector (make-u32vector 0) u32vector (make-s32vector 0) s32vector (make-u16vector 0) u16vector (make-s16vector 0) s16vector (make-u8vector 0) u8vector (make-s8vector 0) s8vector (condition-variable-nil) condvar (mutex-nil) mutex (utf8-string->ucs2-string \"\") ucs2string (char->ucs2 #\\_) bucs2 (make-datagram-server-socket) datagram-socket (make-server-socket) socket (opaque-nil) opaque (custom-nil) custom (process-nil) process procedure make-struct struct vector keyword symbol bstring epair pair pair-nil nil bchar breal bbool bool elong llong bint cell unspec (current-date) date (string->mmap \"\") mmap binary-port error-port (current-output-port) output-port (current-input-port) input-port cons (quote #()) :_ (quote _) (econs #f #f #f) (cons #f #f) (quote ()) #z0 (string->elong \"0\") (string->llong \"0\") (make-cell #unspecified) class-nil "
		"make- list->tvector quote object-widening-set! object-class-num-set! class-num @ ? - unsafe if let set! isa? define fill- ! %allocate- obj %the- -nil ",
		1165);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzobject_nilz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzobject_nilz00(long
		BgL_checksumz00_1912, char *BgL_fromz00_1913)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzobject_nilz00))
				{
					BGl_requirezd2initializa7ationz75zzobject_nilz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzobject_nilz00();
					BGl_libraryzd2moduleszd2initz00zzobject_nilz00();
					BGl_cnstzd2initzd2zzobject_nilz00();
					BGl_importedzd2moduleszd2initz00zzobject_nilz00();
					return BGl_methodzd2initzd2zzobject_nilz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzobject_nilz00(void)
	{
		{	/* Object/nil.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"object_nil");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "object_nil");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "object_nil");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "object_nil");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "object_nil");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "object_nil");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "object_nil");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "object_nil");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"object_nil");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "object_nil");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"object_nil");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzobject_nilz00(void)
	{
		{	/* Object/nil.scm 15 */
			{	/* Object/nil.scm 15 */
				obj_t BgL_cportz00_1901;

				{	/* Object/nil.scm 15 */
					obj_t BgL_stringz00_1908;

					BgL_stringz00_1908 = BGl_string1882z00zzobject_nilz00;
					{	/* Object/nil.scm 15 */
						obj_t BgL_startz00_1909;

						BgL_startz00_1909 = BINT(0L);
						{	/* Object/nil.scm 15 */
							obj_t BgL_endz00_1910;

							BgL_endz00_1910 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1908)));
							{	/* Object/nil.scm 15 */

								BgL_cportz00_1901 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1908, BgL_startz00_1909, BgL_endz00_1910);
				}}}}
				{
					long BgL_iz00_1902;

					BgL_iz00_1902 = 109L;
				BgL_loopz00_1903:
					if ((BgL_iz00_1902 == -1L))
						{	/* Object/nil.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Object/nil.scm 15 */
							{	/* Object/nil.scm 15 */
								obj_t BgL_arg1883z00_1904;

								{	/* Object/nil.scm 15 */

									{	/* Object/nil.scm 15 */
										obj_t BgL_locationz00_1906;

										BgL_locationz00_1906 = BBOOL(((bool_t) 0));
										{	/* Object/nil.scm 15 */

											BgL_arg1883z00_1904 =
												BGl_readz00zz__readerz00(BgL_cportz00_1901,
												BgL_locationz00_1906);
										}
									}
								}
								{	/* Object/nil.scm 15 */
									int BgL_tmpz00_1942;

									BgL_tmpz00_1942 = (int) (BgL_iz00_1902);
									CNST_TABLE_SET(BgL_tmpz00_1942, BgL_arg1883z00_1904);
							}}
							{	/* Object/nil.scm 15 */
								int BgL_auxz00_1907;

								BgL_auxz00_1907 = (int) ((BgL_iz00_1902 - 1L));
								{
									long BgL_iz00_1947;

									BgL_iz00_1947 = (long) (BgL_auxz00_1907);
									BgL_iz00_1902 = BgL_iz00_1947;
									goto BgL_loopz00_1903;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzobject_nilz00(void)
	{
		{	/* Object/nil.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* import-class-nil */
	BGL_EXPORTED_DEF obj_t
		BGl_importzd2classzd2nilz00zzobject_nilz00(BgL_typez00_bglt BgL_classz00_3,
		obj_t BgL_srczd2defzd2_4, obj_t BgL_modulez00_5)
	{
		{	/* Object/nil.scm 44 */
			{	/* Object/nil.scm 45 */
				obj_t BgL_idz00_945;

				BgL_idz00_945 =
					(((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_classz00_3)))->BgL_idz00);
				{	/* Object/nil.scm 45 */
					obj_t BgL_idzd2nilzd2_946;

					{	/* Object/nil.scm 46 */
						obj_t BgL_arg1148z00_950;

						{	/* Object/nil.scm 46 */
							obj_t BgL_arg1149z00_951;
							obj_t BgL_arg1152z00_952;

							{	/* Object/nil.scm 46 */
								obj_t BgL_arg1455z00_1344;

								BgL_arg1455z00_1344 = SYMBOL_TO_STRING(BgL_idz00_945);
								BgL_arg1149z00_951 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
									(BgL_arg1455z00_1344);
							}
							{	/* Object/nil.scm 46 */
								obj_t BgL_symbolz00_1345;

								BgL_symbolz00_1345 = CNST_TABLE_REF(0);
								{	/* Object/nil.scm 46 */
									obj_t BgL_arg1455z00_1346;

									BgL_arg1455z00_1346 = SYMBOL_TO_STRING(BgL_symbolz00_1345);
									BgL_arg1152z00_952 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_1346);
								}
							}
							BgL_arg1148z00_950 =
								string_append(BgL_arg1149z00_951, BgL_arg1152z00_952);
						}
						BgL_idzd2nilzd2_946 = bstring_to_symbol(BgL_arg1148z00_950);
					}
					{	/* Object/nil.scm 46 */
						obj_t BgL_tidzd2nilzd2_947;

						BgL_tidzd2nilzd2_947 =
							BGl_makezd2typedzd2identz00zzast_identz00(BgL_idzd2nilzd2_946,
							BgL_idz00_945);
						{	/* Object/nil.scm 47 */

							{	/* Object/nil.scm 48 */
								obj_t BgL_arg1145z00_948;

								BgL_arg1145z00_948 =
									MAKE_YOUNG_PAIR(BgL_tidzd2nilzd2_947, BNIL);
								return
									BGl_importzd2parserzd2zzmodule_impusez00(BgL_modulez00_5,
									BgL_arg1145z00_948, BFALSE, BNIL);
							}
						}
					}
				}
			}
		}

	}



/* &import-class-nil */
	obj_t BGl_z62importzd2classzd2nilz62zzobject_nilz00(obj_t BgL_envz00_1886,
		obj_t BgL_classz00_1887, obj_t BgL_srczd2defzd2_1888,
		obj_t BgL_modulez00_1889)
	{
		{	/* Object/nil.scm 44 */
			return
				BGl_importzd2classzd2nilz00zzobject_nilz00(
				((BgL_typez00_bglt) BgL_classz00_1887), BgL_srczd2defzd2_1888,
				BgL_modulez00_1889);
		}

	}



/* gen-plain-class-nil */
	BGL_EXPORTED_DEF obj_t
		BGl_genzd2plainzd2classzd2nilzd2zzobject_nilz00(BgL_typez00_bglt
		BgL_classz00_6, obj_t BgL_srczd2defzd2_7, obj_t BgL_importz00_8)
	{
		{	/* Object/nil.scm 55 */
			{	/* Object/nil.scm 57 */
				obj_t BgL_idz00_954;

				BgL_idz00_954 =
					(((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_classz00_6)))->BgL_idz00);
				{	/* Object/nil.scm 57 */
					obj_t BgL_idzd2nilzd2_955;

					{	/* Object/nil.scm 58 */
						obj_t BgL_arg1233z00_1012;

						{	/* Object/nil.scm 58 */
							obj_t BgL_arg1234z00_1013;
							obj_t BgL_arg1236z00_1014;

							{	/* Object/nil.scm 58 */
								obj_t BgL_arg1455z00_1350;

								BgL_arg1455z00_1350 = SYMBOL_TO_STRING(BgL_idz00_954);
								BgL_arg1234z00_1013 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
									(BgL_arg1455z00_1350);
							}
							{	/* Object/nil.scm 58 */
								obj_t BgL_symbolz00_1351;

								BgL_symbolz00_1351 = CNST_TABLE_REF(0);
								{	/* Object/nil.scm 58 */
									obj_t BgL_arg1455z00_1352;

									BgL_arg1455z00_1352 = SYMBOL_TO_STRING(BgL_symbolz00_1351);
									BgL_arg1236z00_1014 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_1352);
								}
							}
							BgL_arg1233z00_1012 =
								string_append(BgL_arg1234z00_1013, BgL_arg1236z00_1014);
						}
						BgL_idzd2nilzd2_955 = bstring_to_symbol(BgL_arg1233z00_1012);
					}
					{	/* Object/nil.scm 58 */
						obj_t BgL_tidzd2nilzd2_956;

						BgL_tidzd2nilzd2_956 =
							BGl_makezd2typedzd2identz00zzast_identz00(BgL_idzd2nilzd2_955,
							BgL_idz00_954);
						{	/* Object/nil.scm 59 */
							obj_t BgL_thezd2idzd2nilz00_957;

							{	/* Object/nil.scm 60 */
								obj_t BgL_arg1230z00_1009;

								{	/* Object/nil.scm 60 */
									obj_t BgL_arg1231z00_1010;
									obj_t BgL_arg1232z00_1011;

									{	/* Object/nil.scm 60 */
										obj_t BgL_symbolz00_1354;

										BgL_symbolz00_1354 = CNST_TABLE_REF(1);
										{	/* Object/nil.scm 60 */
											obj_t BgL_arg1455z00_1355;

											BgL_arg1455z00_1355 =
												SYMBOL_TO_STRING(BgL_symbolz00_1354);
											BgL_arg1231z00_1010 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_1355);
										}
									}
									{	/* Object/nil.scm 60 */
										obj_t BgL_arg1455z00_1357;

										BgL_arg1455z00_1357 = SYMBOL_TO_STRING(BgL_idzd2nilzd2_955);
										BgL_arg1232z00_1011 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_1357);
									}
									BgL_arg1230z00_1009 =
										string_append(BgL_arg1231z00_1010, BgL_arg1232z00_1011);
								}
								BgL_thezd2idzd2nilz00_957 =
									bstring_to_symbol(BgL_arg1230z00_1009);
							}
							{	/* Object/nil.scm 60 */
								obj_t BgL_thezd2tidzd2nilz00_958;

								BgL_thezd2tidzd2nilz00_958 =
									BGl_makezd2typedzd2identz00zzast_identz00
									(BgL_thezd2idzd2nilz00_957, CNST_TABLE_REF(2));
								{	/* Object/nil.scm 61 */
									obj_t BgL_allocz00_959;

									{	/* Object/nil.scm 62 */
										obj_t BgL_arg1227z00_1006;

										{	/* Object/nil.scm 62 */
											obj_t BgL_arg1228z00_1007;
											obj_t BgL_arg1229z00_1008;

											{	/* Object/nil.scm 62 */
												obj_t BgL_symbolz00_1359;

												BgL_symbolz00_1359 = CNST_TABLE_REF(3);
												{	/* Object/nil.scm 62 */
													obj_t BgL_arg1455z00_1360;

													BgL_arg1455z00_1360 =
														SYMBOL_TO_STRING(BgL_symbolz00_1359);
													BgL_arg1228z00_1007 =
														BGl_stringzd2copyzd2zz__r4_strings_6_7z00
														(BgL_arg1455z00_1360);
												}
											}
											{	/* Object/nil.scm 62 */
												obj_t BgL_arg1455z00_1362;

												BgL_arg1455z00_1362 = SYMBOL_TO_STRING(BgL_idz00_954);
												BgL_arg1229z00_1008 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_1362);
											}
											BgL_arg1227z00_1006 =
												string_append(BgL_arg1228z00_1007, BgL_arg1229z00_1008);
										}
										BgL_allocz00_959 = bstring_to_symbol(BgL_arg1227z00_1006);
									}
									{	/* Object/nil.scm 62 */
										obj_t BgL_fillz00_960;

										{	/* Object/nil.scm 63 */
											obj_t BgL_list1224z00_1003;

											{	/* Object/nil.scm 63 */
												obj_t BgL_arg1225z00_1004;

												{	/* Object/nil.scm 63 */
													obj_t BgL_arg1226z00_1005;

													BgL_arg1226z00_1005 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(4), BNIL);
													BgL_arg1225z00_1004 =
														MAKE_YOUNG_PAIR(BgL_idz00_954, BgL_arg1226z00_1005);
												}
												BgL_list1224z00_1003 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
													BgL_arg1225z00_1004);
											}
											BgL_fillz00_960 =
												BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
												(BgL_list1224z00_1003);
										}
										{	/* Object/nil.scm 63 */
											obj_t BgL_tmpz00_961;

											{	/* Object/nil.scm 64 */

												{	/* Object/nil.scm 64 */

													BgL_tmpz00_961 =
														BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
												}
											}
											{	/* Object/nil.scm 64 */
												obj_t BgL_tmptz00_962;

												BgL_tmptz00_962 =
													BGl_makezd2typedzd2identz00zzast_identz00
													(BgL_tmpz00_961, BgL_idz00_954);
												{	/* Object/nil.scm 65 */

													{	/* Object/nil.scm 66 */
														obj_t BgL_arg1153z00_963;

														{	/* Object/nil.scm 66 */
															obj_t BgL_arg1154z00_964;

															{	/* Object/nil.scm 66 */
																obj_t BgL_arg1157z00_965;

																BgL_arg1157z00_965 =
																	MAKE_YOUNG_PAIR(BgL_tidzd2nilzd2_956, BNIL);
																BgL_arg1154z00_964 =
																	MAKE_YOUNG_PAIR(BgL_arg1157z00_965, BNIL);
															}
															BgL_arg1153z00_963 =
																MAKE_YOUNG_PAIR(BgL_importz00_8,
																BgL_arg1154z00_964);
														}
														BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
															(BgL_arg1153z00_963);
													}
													{	/* Object/nil.scm 68 */
														obj_t BgL_arg1158z00_966;
														obj_t BgL_arg1162z00_967;

														{	/* Object/nil.scm 68 */
															obj_t BgL_arg1166z00_970;

															{	/* Object/nil.scm 68 */
																obj_t BgL_arg1171z00_971;

																BgL_arg1171z00_971 =
																	MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
																BgL_arg1166z00_970 =
																	MAKE_YOUNG_PAIR(BgL_thezd2tidzd2nilz00_958,
																	BgL_arg1171z00_971);
															}
															BgL_arg1158z00_966 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
																BgL_arg1166z00_970);
														}
														{	/* Object/nil.scm 69 */
															obj_t BgL_arg1172z00_972;

															{	/* Object/nil.scm 69 */
																obj_t BgL_arg1182z00_973;

																{	/* Object/nil.scm 69 */
																	obj_t BgL_arg1183z00_974;
																	obj_t BgL_arg1187z00_975;

																	BgL_arg1183z00_974 =
																		MAKE_YOUNG_PAIR(BgL_tidzd2nilzd2_956, BNIL);
																	{	/* Object/nil.scm 71 */
																		obj_t BgL_arg1188z00_976;

																		{	/* Object/nil.scm 71 */
																			obj_t BgL_arg1189z00_977;

																			{	/* Object/nil.scm 71 */
																				obj_t BgL_arg1191z00_979;

																				{	/* Object/nil.scm 71 */
																					obj_t BgL_arg1193z00_980;
																					obj_t BgL_arg1194z00_981;

																					{	/* Object/nil.scm 71 */
																						obj_t BgL_arg1196z00_982;

																						{	/* Object/nil.scm 71 */
																							obj_t BgL_arg1197z00_983;

																							BgL_arg1197z00_983 =
																								MAKE_YOUNG_PAIR(BgL_idz00_954,
																								BNIL);
																							BgL_arg1196z00_982 =
																								MAKE_YOUNG_PAIR
																								(BgL_thezd2idzd2nilz00_957,
																								BgL_arg1197z00_983);
																						}
																						BgL_arg1193z00_980 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF(7),
																							BgL_arg1196z00_982);
																					}
																					{	/* Object/nil.scm 73 */
																						obj_t BgL_arg1198z00_984;

																						{	/* Object/nil.scm 73 */
																							obj_t BgL_arg1199z00_985;

																							{	/* Object/nil.scm 73 */
																								obj_t BgL_arg1200z00_986;

																								{	/* Object/nil.scm 73 */
																									obj_t BgL_arg1201z00_987;
																									obj_t BgL_arg1202z00_988;

																									{	/* Object/nil.scm 73 */
																										obj_t BgL_arg1203z00_989;

																										{	/* Object/nil.scm 73 */
																											obj_t BgL_arg1206z00_990;

																											{	/* Object/nil.scm 73 */
																												obj_t
																													BgL_arg1208z00_991;
																												BgL_arg1208z00_991 =
																													MAKE_YOUNG_PAIR
																													(BgL_allocz00_959,
																													BNIL);
																												BgL_arg1206z00_990 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1208z00_991,
																													BNIL);
																											}
																											BgL_arg1203z00_989 =
																												MAKE_YOUNG_PAIR
																												(BgL_tmptz00_962,
																												BgL_arg1206z00_990);
																										}
																										BgL_arg1201z00_987 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1203z00_989,
																											BNIL);
																									}
																									{	/* Object/nil.scm 74 */
																										obj_t BgL_arg1209z00_992;
																										obj_t BgL_arg1210z00_993;

																										{	/* Object/nil.scm 74 */
																											obj_t BgL_arg1212z00_994;

																											{	/* Object/nil.scm 74 */
																												obj_t
																													BgL_arg1215z00_995;
																												BgL_arg1215z00_995 =
																													MAKE_YOUNG_PAIR
																													(BgL_tmpz00_961,
																													BNIL);
																												BgL_arg1212z00_994 =
																													MAKE_YOUNG_PAIR
																													(BgL_thezd2idzd2nilz00_957,
																													BgL_arg1215z00_995);
																											}
																											BgL_arg1209z00_992 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(8),
																												BgL_arg1212z00_994);
																										}
																										{	/* Object/nil.scm 75 */
																											obj_t BgL_arg1216z00_996;
																											obj_t BgL_arg1218z00_997;

																											{	/* Object/nil.scm 75 */
																												obj_t
																													BgL_arg1219z00_998;
																												{	/* Object/nil.scm 75 */
																													obj_t
																														BgL_arg1220z00_999;
																													{	/* Object/nil.scm 75 */
																														obj_t
																															BgL_arg1221z00_1000;
																														{	/* Object/nil.scm 75 */
																															obj_t
																																BgL_arg1223z00_1001;
																															{
																																BgL_tclassz00_bglt
																																	BgL_auxz00_2019;
																																{
																																	obj_t
																																		BgL_auxz00_2020;
																																	{	/* Object/nil.scm 75 */
																																		BgL_objectz00_bglt
																																			BgL_tmpz00_2021;
																																		BgL_tmpz00_2021
																																			=
																																			(
																																			(BgL_objectz00_bglt)
																																			BgL_classz00_6);
																																		BgL_auxz00_2020
																																			=
																																			BGL_OBJECT_WIDENING
																																			(BgL_tmpz00_2021);
																																	}
																																	BgL_auxz00_2019
																																		=
																																		(
																																		(BgL_tclassz00_bglt)
																																		BgL_auxz00_2020);
																																}
																																BgL_arg1223z00_1001
																																	=
																																	(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_2019))->BgL_slotsz00);
																															}
																															BgL_arg1221z00_1000
																																=
																																BGl_fillzd2nilzd2zzobject_nilz00
																																(BgL_arg1223z00_1001);
																														}
																														BgL_arg1220z00_999 =
																															BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																															(BgL_arg1221z00_1000,
																															BNIL);
																													}
																													BgL_arg1219z00_998 =
																														MAKE_YOUNG_PAIR
																														(BgL_tmpz00_961,
																														BgL_arg1220z00_999);
																												}
																												BgL_arg1216z00_996 =
																													MAKE_YOUNG_PAIR
																													(BgL_fillz00_960,
																													BgL_arg1219z00_998);
																											}
																											BgL_arg1218z00_997 =
																												MAKE_YOUNG_PAIR
																												(BgL_tmpz00_961, BNIL);
																											BgL_arg1210z00_993 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1216z00_996,
																												BgL_arg1218z00_997);
																										}
																										BgL_arg1202z00_988 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1209z00_992,
																											BgL_arg1210z00_993);
																									}
																									BgL_arg1200z00_986 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1201z00_987,
																										BgL_arg1202z00_988);
																								}
																								BgL_arg1199z00_985 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(9), BgL_arg1200z00_986);
																							}
																							BgL_arg1198z00_984 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1199z00_985, BNIL);
																						}
																						BgL_arg1194z00_981 =
																							MAKE_YOUNG_PAIR
																							(BgL_thezd2idzd2nilz00_957,
																							BgL_arg1198z00_984);
																					}
																					BgL_arg1191z00_979 =
																						MAKE_YOUNG_PAIR(BgL_arg1193z00_980,
																						BgL_arg1194z00_981);
																				}
																				BgL_arg1189z00_977 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
																					BgL_arg1191z00_979);
																			}
																			{	/* Object/nil.scm 70 */
																				obj_t BgL_list1190z00_978;

																				BgL_list1190z00_978 =
																					MAKE_YOUNG_PAIR(BgL_arg1189z00_977,
																					BNIL);
																				BgL_arg1188z00_976 =
																					BGl_makezd2privatezd2sexpz00zzast_privatez00
																					(CNST_TABLE_REF(11), BgL_idz00_954,
																					BgL_list1190z00_978);
																			}
																		}
																		BgL_arg1187z00_975 =
																			MAKE_YOUNG_PAIR(BgL_arg1188z00_976, BNIL);
																	}
																	BgL_arg1182z00_973 =
																		MAKE_YOUNG_PAIR(BgL_arg1183z00_974,
																		BgL_arg1187z00_975);
																}
																BgL_arg1172z00_972 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
																	BgL_arg1182z00_973);
															}
															BgL_arg1162z00_967 =
																BGl_epairifyz00zztools_miscz00
																(BgL_arg1172z00_972, BgL_srczd2defzd2_7);
														}
														{	/* Object/nil.scm 67 */
															obj_t BgL_list1163z00_968;

															{	/* Object/nil.scm 67 */
																obj_t BgL_arg1164z00_969;

																BgL_arg1164z00_969 =
																	MAKE_YOUNG_PAIR(BgL_arg1162z00_967, BNIL);
																BgL_list1163z00_968 =
																	MAKE_YOUNG_PAIR(BgL_arg1158z00_966,
																	BgL_arg1164z00_969);
															}
															return BgL_list1163z00_968;
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &gen-plain-class-nil */
	obj_t BGl_z62genzd2plainzd2classzd2nilzb0zzobject_nilz00(obj_t
		BgL_envz00_1890, obj_t BgL_classz00_1891, obj_t BgL_srczd2defzd2_1892,
		obj_t BgL_importz00_1893)
	{
		{	/* Object/nil.scm 55 */
			return
				BGl_genzd2plainzd2classzd2nilzd2zzobject_nilz00(
				((BgL_typez00_bglt) BgL_classz00_1891), BgL_srczd2defzd2_1892,
				BgL_importz00_1893);
		}

	}



/* gen-wide-class-nil */
	BGL_EXPORTED_DEF obj_t
		BGl_genzd2widezd2classzd2nilzd2zzobject_nilz00(BgL_typez00_bglt
		BgL_classz00_9, obj_t BgL_srczd2defzd2_10, obj_t BgL_importz00_11)
	{
		{	/* Object/nil.scm 84 */
			{	/* Object/nil.scm 86 */
				obj_t BgL_idz00_1016;

				BgL_idz00_1016 =
					(((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_classz00_9)))->BgL_idz00);
				{	/* Object/nil.scm 86 */
					obj_t BgL_superzd2idzd2_1017;

					{	/* Object/nil.scm 87 */
						obj_t BgL_arg1544z00_1108;

						{
							BgL_tclassz00_bglt BgL_auxz00_2055;

							{
								obj_t BgL_auxz00_2056;

								{	/* Object/nil.scm 87 */
									BgL_objectz00_bglt BgL_tmpz00_2057;

									BgL_tmpz00_2057 = ((BgL_objectz00_bglt) BgL_classz00_9);
									BgL_auxz00_2056 = BGL_OBJECT_WIDENING(BgL_tmpz00_2057);
								}
								BgL_auxz00_2055 = ((BgL_tclassz00_bglt) BgL_auxz00_2056);
							}
							BgL_arg1544z00_1108 =
								(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_2055))->
								BgL_itszd2superzd2);
						}
						BgL_superzd2idzd2_1017 =
							(((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt)
										((BgL_typez00_bglt) BgL_arg1544z00_1108))))->BgL_idz00);
					}
					{	/* Object/nil.scm 87 */
						obj_t BgL_superzd2slotszd2_1018;

						{	/* Object/nil.scm 88 */
							obj_t BgL_arg1540z00_1107;

							{
								BgL_tclassz00_bglt BgL_auxz00_2065;

								{
									obj_t BgL_auxz00_2066;

									{	/* Object/nil.scm 88 */
										BgL_objectz00_bglt BgL_tmpz00_2067;

										BgL_tmpz00_2067 = ((BgL_objectz00_bglt) BgL_classz00_9);
										BgL_auxz00_2066 = BGL_OBJECT_WIDENING(BgL_tmpz00_2067);
									}
									BgL_auxz00_2065 = ((BgL_tclassz00_bglt) BgL_auxz00_2066);
								}
								BgL_arg1540z00_1107 =
									(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_2065))->
									BgL_itszd2superzd2);
							}
							{
								BgL_tclassz00_bglt BgL_auxz00_2072;

								{
									obj_t BgL_auxz00_2073;

									{	/* Object/nil.scm 88 */
										BgL_objectz00_bglt BgL_tmpz00_2074;

										BgL_tmpz00_2074 =
											((BgL_objectz00_bglt)
											((BgL_typez00_bglt) BgL_arg1540z00_1107));
										BgL_auxz00_2073 = BGL_OBJECT_WIDENING(BgL_tmpz00_2074);
									}
									BgL_auxz00_2072 = ((BgL_tclassz00_bglt) BgL_auxz00_2073);
								}
								BgL_superzd2slotszd2_1018 =
									(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_2072))->
									BgL_slotsz00);
							}
						}
						{	/* Object/nil.scm 88 */
							obj_t BgL_idzd2nilzd2_1019;

							{	/* Object/nil.scm 89 */
								obj_t BgL_arg1514z00_1104;

								{	/* Object/nil.scm 89 */
									obj_t BgL_arg1516z00_1105;
									obj_t BgL_arg1535z00_1106;

									{	/* Object/nil.scm 89 */
										obj_t BgL_arg1455z00_1373;

										BgL_arg1455z00_1373 = SYMBOL_TO_STRING(BgL_idz00_1016);
										BgL_arg1516z00_1105 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_1373);
									}
									{	/* Object/nil.scm 89 */
										obj_t BgL_symbolz00_1374;

										BgL_symbolz00_1374 = CNST_TABLE_REF(0);
										{	/* Object/nil.scm 89 */
											obj_t BgL_arg1455z00_1375;

											BgL_arg1455z00_1375 =
												SYMBOL_TO_STRING(BgL_symbolz00_1374);
											BgL_arg1535z00_1106 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_1375);
										}
									}
									BgL_arg1514z00_1104 =
										string_append(BgL_arg1516z00_1105, BgL_arg1535z00_1106);
								}
								BgL_idzd2nilzd2_1019 = bstring_to_symbol(BgL_arg1514z00_1104);
							}
							{	/* Object/nil.scm 89 */
								obj_t BgL_tidzd2nilzd2_1020;

								BgL_tidzd2nilzd2_1020 =
									BGl_makezd2typedzd2identz00zzast_identz00
									(BgL_idzd2nilzd2_1019, BgL_idz00_1016);
								{	/* Object/nil.scm 90 */
									obj_t BgL_thezd2idzd2nilz00_1021;

									{	/* Object/nil.scm 91 */
										obj_t BgL_arg1502z00_1101;

										{	/* Object/nil.scm 91 */
											obj_t BgL_arg1509z00_1102;
											obj_t BgL_arg1513z00_1103;

											{	/* Object/nil.scm 91 */
												obj_t BgL_symbolz00_1377;

												BgL_symbolz00_1377 = CNST_TABLE_REF(1);
												{	/* Object/nil.scm 91 */
													obj_t BgL_arg1455z00_1378;

													BgL_arg1455z00_1378 =
														SYMBOL_TO_STRING(BgL_symbolz00_1377);
													BgL_arg1509z00_1102 =
														BGl_stringzd2copyzd2zz__r4_strings_6_7z00
														(BgL_arg1455z00_1378);
												}
											}
											{	/* Object/nil.scm 91 */
												obj_t BgL_arg1455z00_1380;

												BgL_arg1455z00_1380 =
													SYMBOL_TO_STRING(BgL_idzd2nilzd2_1019);
												BgL_arg1513z00_1103 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_1380);
											}
											BgL_arg1502z00_1101 =
												string_append(BgL_arg1509z00_1102, BgL_arg1513z00_1103);
										}
										BgL_thezd2idzd2nilz00_1021 =
											bstring_to_symbol(BgL_arg1502z00_1101);
									}
									{	/* Object/nil.scm 91 */
										obj_t BgL_thezd2tidzd2nilz00_1022;

										BgL_thezd2tidzd2nilz00_1022 =
											BGl_makezd2typedzd2identz00zzast_identz00
											(BgL_thezd2idzd2nilz00_1021, CNST_TABLE_REF(2));
										{	/* Object/nil.scm 92 */
											obj_t BgL_wideningz00_1023;

											{	/* Object/nil.scm 93 */
												obj_t BgL_arg1472z00_1097;

												{
													BgL_tclassz00_bglt BgL_auxz00_2097;

													{
														obj_t BgL_auxz00_2098;

														{	/* Object/nil.scm 93 */
															BgL_objectz00_bglt BgL_tmpz00_2099;

															BgL_tmpz00_2099 =
																((BgL_objectz00_bglt) BgL_classz00_9);
															BgL_auxz00_2098 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_2099);
														}
														BgL_auxz00_2097 =
															((BgL_tclassz00_bglt) BgL_auxz00_2098);
													}
													BgL_arg1472z00_1097 =
														(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_2097))->
														BgL_wideningz00);
												}
												{	/* Object/nil.scm 93 */
													obj_t BgL_list1473z00_1098;

													{	/* Object/nil.scm 93 */
														obj_t BgL_arg1485z00_1099;

														{	/* Object/nil.scm 93 */
															obj_t BgL_arg1489z00_1100;

															BgL_arg1489z00_1100 =
																MAKE_YOUNG_PAIR(BgL_idz00_1016, BNIL);
															BgL_arg1485z00_1099 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(12),
																BgL_arg1489z00_1100);
														}
														BgL_list1473z00_1098 =
															MAKE_YOUNG_PAIR(BgL_arg1472z00_1097,
															BgL_arg1485z00_1099);
													}
													BgL_wideningz00_1023 =
														BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
														(BgL_list1473z00_1098);
												}
											}
											{	/* Object/nil.scm 93 */
												obj_t BgL_superzd2alloczd2_1024;

												{	/* Object/nil.scm 94 */
													obj_t BgL_arg1448z00_1094;

													{	/* Object/nil.scm 94 */
														obj_t BgL_arg1453z00_1095;
														obj_t BgL_arg1454z00_1096;

														{	/* Object/nil.scm 94 */
															obj_t BgL_symbolz00_1384;

															BgL_symbolz00_1384 = CNST_TABLE_REF(3);
															{	/* Object/nil.scm 94 */
																obj_t BgL_arg1455z00_1385;

																BgL_arg1455z00_1385 =
																	SYMBOL_TO_STRING(BgL_symbolz00_1384);
																BgL_arg1453z00_1095 =
																	BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																	(BgL_arg1455z00_1385);
															}
														}
														{	/* Object/nil.scm 94 */
															obj_t BgL_arg1455z00_1387;

															BgL_arg1455z00_1387 =
																SYMBOL_TO_STRING(BgL_superzd2idzd2_1017);
															BgL_arg1454z00_1096 =
																BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																(BgL_arg1455z00_1387);
														}
														BgL_arg1448z00_1094 =
															string_append(BgL_arg1453z00_1095,
															BgL_arg1454z00_1096);
													}
													BgL_superzd2alloczd2_1024 =
														bstring_to_symbol(BgL_arg1448z00_1094);
												}
												{	/* Object/nil.scm 94 */
													obj_t BgL_superzd2fillzd2_1025;

													{	/* Object/nil.scm 95 */
														obj_t BgL_list1422z00_1091;

														{	/* Object/nil.scm 95 */
															obj_t BgL_arg1434z00_1092;

															{	/* Object/nil.scm 95 */
																obj_t BgL_arg1437z00_1093;

																BgL_arg1437z00_1093 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(4), BNIL);
																BgL_arg1434z00_1092 =
																	MAKE_YOUNG_PAIR(BgL_superzd2idzd2_1017,
																	BgL_arg1437z00_1093);
															}
															BgL_list1422z00_1091 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
																BgL_arg1434z00_1092);
														}
														BgL_superzd2fillzd2_1025 =
															BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
															(BgL_list1422z00_1091);
													}
													{	/* Object/nil.scm 95 */
														obj_t BgL_idzf3zf3_1026;

														{	/* Object/nil.scm 96 */
															obj_t BgL_arg1408z00_1088;

															{	/* Object/nil.scm 96 */
																obj_t BgL_arg1410z00_1089;
																obj_t BgL_arg1421z00_1090;

																{	/* Object/nil.scm 96 */
																	obj_t BgL_arg1455z00_1390;

																	BgL_arg1455z00_1390 =
																		SYMBOL_TO_STRING(BgL_idz00_1016);
																	BgL_arg1410z00_1089 =
																		BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																		(BgL_arg1455z00_1390);
																}
																{	/* Object/nil.scm 96 */
																	obj_t BgL_symbolz00_1391;

																	BgL_symbolz00_1391 = CNST_TABLE_REF(13);
																	{	/* Object/nil.scm 96 */
																		obj_t BgL_arg1455z00_1392;

																		BgL_arg1455z00_1392 =
																			SYMBOL_TO_STRING(BgL_symbolz00_1391);
																		BgL_arg1421z00_1090 =
																			BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																			(BgL_arg1455z00_1392);
																	}
																}
																BgL_arg1408z00_1088 =
																	string_append(BgL_arg1410z00_1089,
																	BgL_arg1421z00_1090);
															}
															BgL_idzf3zf3_1026 =
																bstring_to_symbol(BgL_arg1408z00_1088);
														}
														{	/* Object/nil.scm 96 */
															obj_t BgL_tmpz00_1027;

															{	/* Object/nil.scm 97 */

																{	/* Object/nil.scm 97 */

																	BgL_tmpz00_1027 =
																		BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
																}
															}
															{	/* Object/nil.scm 97 */
																obj_t BgL_tmptz00_1028;

																BgL_tmptz00_1028 =
																	BGl_makezd2typedzd2identz00zzast_identz00
																	(BgL_tmpz00_1027, BgL_idz00_1016);
																{	/* Object/nil.scm 98 */

																	{	/* Object/nil.scm 99 */
																		obj_t BgL_arg1238z00_1029;

																		{	/* Object/nil.scm 99 */
																			obj_t BgL_arg1239z00_1030;

																			{	/* Object/nil.scm 99 */
																				obj_t BgL_arg1242z00_1031;

																				BgL_arg1242z00_1031 =
																					MAKE_YOUNG_PAIR(BgL_tidzd2nilzd2_1020,
																					BNIL);
																				BgL_arg1239z00_1030 =
																					MAKE_YOUNG_PAIR(BgL_arg1242z00_1031,
																					BNIL);
																			}
																			BgL_arg1238z00_1029 =
																				MAKE_YOUNG_PAIR(BgL_importz00_11,
																				BgL_arg1239z00_1030);
																		}
																		BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
																			(BgL_arg1238z00_1029);
																	}
																	{	/* Object/nil.scm 101 */
																		obj_t BgL_arg1244z00_1032;
																		obj_t BgL_arg1248z00_1033;

																		{	/* Object/nil.scm 101 */
																			obj_t BgL_arg1268z00_1036;

																			{	/* Object/nil.scm 101 */
																				obj_t BgL_arg1272z00_1037;

																				BgL_arg1272z00_1037 =
																					MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
																				BgL_arg1268z00_1036 =
																					MAKE_YOUNG_PAIR
																					(BgL_thezd2tidzd2nilz00_1022,
																					BgL_arg1272z00_1037);
																			}
																			BgL_arg1244z00_1032 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
																				BgL_arg1268z00_1036);
																		}
																		{	/* Object/nil.scm 102 */
																			obj_t BgL_arg1284z00_1038;

																			{	/* Object/nil.scm 102 */
																				obj_t BgL_arg1304z00_1039;

																				{	/* Object/nil.scm 102 */
																					obj_t BgL_arg1305z00_1040;
																					obj_t BgL_arg1306z00_1041;

																					BgL_arg1305z00_1040 =
																						MAKE_YOUNG_PAIR
																						(BgL_tidzd2nilzd2_1020, BNIL);
																					{	/* Object/nil.scm 104 */
																						obj_t BgL_arg1307z00_1042;

																						{	/* Object/nil.scm 104 */
																							obj_t BgL_arg1308z00_1043;

																							{	/* Object/nil.scm 104 */
																								obj_t BgL_arg1310z00_1045;

																								{	/* Object/nil.scm 104 */
																									obj_t BgL_arg1311z00_1046;
																									obj_t BgL_arg1312z00_1047;

																									{	/* Object/nil.scm 104 */
																										obj_t BgL_arg1314z00_1048;

																										BgL_arg1314z00_1048 =
																											MAKE_YOUNG_PAIR
																											(BgL_thezd2idzd2nilz00_1021,
																											BNIL);
																										BgL_arg1311z00_1046 =
																											MAKE_YOUNG_PAIR
																											(BgL_idzf3zf3_1026,
																											BgL_arg1314z00_1048);
																									}
																									{	/* Object/nil.scm 107 */
																										obj_t BgL_arg1315z00_1049;

																										{	/* Object/nil.scm 107 */
																											obj_t BgL_arg1316z00_1050;

																											{	/* Object/nil.scm 107 */
																												obj_t
																													BgL_arg1317z00_1051;
																												{	/* Object/nil.scm 107 */
																													obj_t
																														BgL_arg1318z00_1052;
																													obj_t
																														BgL_arg1319z00_1053;
																													{	/* Object/nil.scm 107 */
																														obj_t
																															BgL_arg1320z00_1054;
																														{	/* Object/nil.scm 107 */
																															obj_t
																																BgL_arg1321z00_1055;
																															{	/* Object/nil.scm 107 */
																																obj_t
																																	BgL_arg1322z00_1056;
																																BgL_arg1322z00_1056
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_superzd2alloczd2_1024,
																																	BNIL);
																																BgL_arg1321z00_1055
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1322z00_1056,
																																	BNIL);
																															}
																															BgL_arg1320z00_1054
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_tmptz00_1028,
																																BgL_arg1321z00_1055);
																														}
																														BgL_arg1318z00_1052
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1320z00_1054,
																															BNIL);
																													}
																													{	/* Object/nil.scm 108 */
																														obj_t
																															BgL_arg1323z00_1057;
																														obj_t
																															BgL_arg1325z00_1058;
																														{	/* Object/nil.scm 108 */
																															obj_t
																																BgL_arg1326z00_1059;
																															{	/* Object/nil.scm 108 */
																																obj_t
																																	BgL_arg1327z00_1060;
																																BgL_arg1327z00_1060
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_tmpz00_1027,
																																	BNIL);
																																BgL_arg1326z00_1059
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_thezd2idzd2nilz00_1021,
																																	BgL_arg1327z00_1060);
																															}
																															BgL_arg1323z00_1057
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(8),
																																BgL_arg1326z00_1059);
																														}
																														{	/* Object/nil.scm 110 */
																															obj_t
																																BgL_arg1328z00_1061;
																															obj_t
																																BgL_arg1329z00_1062;
																															{	/* Object/nil.scm 110 */
																																obj_t
																																	BgL_arg1331z00_1063;
																																BgL_arg1331z00_1063
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_tmpz00_1027,
																																	BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																	(BGl_fillzd2nilzd2zzobject_nilz00
																																		(BgL_superzd2slotszd2_1018),
																																		BNIL));
																																BgL_arg1328z00_1061
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_superzd2fillzd2_1025,
																																	BgL_arg1331z00_1063);
																															}
																															{	/* Object/nil.scm 114 */
																																obj_t
																																	BgL_arg1335z00_1066;
																																obj_t
																																	BgL_arg1339z00_1067;
																																{	/* Object/nil.scm 114 */
																																	obj_t
																																		BgL_arg1340z00_1068;
																																	{	/* Object/nil.scm 114 */
																																		obj_t
																																			BgL_arg1342z00_1069;
																																		{	/* Object/nil.scm 114 */
																																			obj_t
																																				BgL_arg1343z00_1070;
																																			{	/* Object/nil.scm 114 */
																																				obj_t
																																					BgL_arg1346z00_1071;
																																				{	/* Object/nil.scm 114 */
																																					obj_t
																																						BgL_arg1348z00_1072;
																																					{	/* Object/nil.scm 114 */
																																						obj_t
																																							BgL_arg1349z00_1073;
																																						{	/* Object/nil.scm 114 */
																																							obj_t
																																								BgL_arg1351z00_1074;
																																							obj_t
																																								BgL_arg1352z00_1075;
																																							{	/* Object/nil.scm 114 */
																																								BgL_globalz00_bglt
																																									BgL_arg1361z00_1076;
																																								{
																																									BgL_tclassz00_bglt
																																										BgL_auxz00_2154;
																																									{
																																										obj_t
																																											BgL_auxz00_2155;
																																										{	/* Object/nil.scm 114 */
																																											BgL_objectz00_bglt
																																												BgL_tmpz00_2156;
																																											BgL_tmpz00_2156
																																												=
																																												(
																																												(BgL_objectz00_bglt)
																																												BgL_classz00_9);
																																											BgL_auxz00_2155
																																												=
																																												BGL_OBJECT_WIDENING
																																												(BgL_tmpz00_2156);
																																										}
																																										BgL_auxz00_2154
																																											=
																																											(
																																											(BgL_tclassz00_bglt)
																																											BgL_auxz00_2155);
																																									}
																																									BgL_arg1361z00_1076
																																										=
																																										(
																																										((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_2154))->BgL_holderz00);
																																								}
																																								BgL_arg1351z00_1074
																																									=
																																									(
																																									((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) BgL_arg1361z00_1076)))->BgL_idz00);
																																							}
																																							{	/* Object/nil.scm 115 */
																																								obj_t
																																									BgL_arg1364z00_1077;
																																								{	/* Object/nil.scm 115 */
																																									BgL_globalz00_bglt
																																										BgL_arg1367z00_1078;
																																									{
																																										BgL_tclassz00_bglt
																																											BgL_auxz00_2163;
																																										{
																																											obj_t
																																												BgL_auxz00_2164;
																																											{	/* Object/nil.scm 115 */
																																												BgL_objectz00_bglt
																																													BgL_tmpz00_2165;
																																												BgL_tmpz00_2165
																																													=
																																													(
																																													(BgL_objectz00_bglt)
																																													BgL_classz00_9);
																																												BgL_auxz00_2164
																																													=
																																													BGL_OBJECT_WIDENING
																																													(BgL_tmpz00_2165);
																																											}
																																											BgL_auxz00_2163
																																												=
																																												(
																																												(BgL_tclassz00_bglt)
																																												BgL_auxz00_2164);
																																										}
																																										BgL_arg1367z00_1078
																																											=
																																											(
																																											((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_2163))->BgL_holderz00);
																																									}
																																									BgL_arg1364z00_1077
																																										=
																																										(
																																										((BgL_globalz00_bglt) COBJECT(BgL_arg1367z00_1078))->BgL_modulez00);
																																								}
																																								BgL_arg1352z00_1075
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg1364z00_1077,
																																									BNIL);
																																							}
																																							BgL_arg1349z00_1073
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg1351z00_1074,
																																								BgL_arg1352z00_1075);
																																						}
																																						BgL_arg1348z00_1072
																																							=
																																							MAKE_YOUNG_PAIR
																																							(CNST_TABLE_REF
																																							(14),
																																							BgL_arg1349z00_1073);
																																					}
																																					BgL_arg1346z00_1071
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1348z00_1072,
																																						BNIL);
																																				}
																																				BgL_arg1343z00_1070
																																					=
																																					MAKE_YOUNG_PAIR
																																					(CNST_TABLE_REF
																																					(15),
																																					BgL_arg1346z00_1071);
																																			}
																																			BgL_arg1342z00_1069
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1343z00_1070,
																																				BNIL);
																																		}
																																		BgL_arg1340z00_1068
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_tmpz00_1027,
																																			BgL_arg1342z00_1069);
																																	}
																																	BgL_arg1335z00_1066
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(16),
																																		BgL_arg1340z00_1068);
																																}
																																{	/* Object/nil.scm 119 */
																																	obj_t
																																		BgL_arg1370z00_1079;
																																	obj_t
																																		BgL_arg1371z00_1080;
																																	{	/* Object/nil.scm 119 */
																																		obj_t
																																			BgL_arg1375z00_1081;
																																		{	/* Object/nil.scm 119 */
																																			obj_t
																																				BgL_arg1376z00_1082;
																																			{	/* Object/nil.scm 119 */
																																				obj_t
																																					BgL_arg1377z00_1083;
																																				{	/* Object/nil.scm 119 */
																																					obj_t
																																						BgL_arg1378z00_1084;
																																					{	/* Object/nil.scm 119 */
																																						obj_t
																																							BgL_arg1379z00_1085;
																																						{	/* Object/nil.scm 119 */
																																							obj_t
																																								BgL_arg1380z00_1086;
																																							{
																																								BgL_tclassz00_bglt
																																									BgL_auxz00_2182;
																																								{
																																									obj_t
																																										BgL_auxz00_2183;
																																									{	/* Object/nil.scm 119 */
																																										BgL_objectz00_bglt
																																											BgL_tmpz00_2184;
																																										BgL_tmpz00_2184
																																											=
																																											(
																																											(BgL_objectz00_bglt)
																																											BgL_classz00_9);
																																										BgL_auxz00_2183
																																											=
																																											BGL_OBJECT_WIDENING
																																											(BgL_tmpz00_2184);
																																									}
																																									BgL_auxz00_2182
																																										=
																																										(
																																										(BgL_tclassz00_bglt)
																																										BgL_auxz00_2183);
																																								}
																																								BgL_arg1380z00_1086
																																									=
																																									(
																																									((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_2182))->BgL_slotsz00);
																																							}
																																							BgL_arg1379z00_1085
																																								=
																																								BGl_fillzd2nilzd2zzobject_nilz00
																																								(BgL_arg1380z00_1086);
																																						}
																																						BgL_arg1378z00_1084
																																							=
																																							BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																							(BgL_arg1379z00_1085,
																																							BNIL);
																																					}
																																					BgL_arg1377z00_1083
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_wideningz00_1023,
																																						BgL_arg1378z00_1084);
																																				}
																																				BgL_arg1376z00_1082
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1377z00_1083,
																																					BNIL);
																																			}
																																			BgL_arg1375z00_1081
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_tmpz00_1027,
																																				BgL_arg1376z00_1082);
																																		}
																																		BgL_arg1370z00_1079
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(17),
																																			BgL_arg1375z00_1081);
																																	}
																																	BgL_arg1371z00_1080
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_tmpz00_1027,
																																		BNIL);
																																	BgL_arg1339z00_1067
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1370z00_1079,
																																		BgL_arg1371z00_1080);
																																}
																																BgL_arg1329z00_1062
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1335z00_1066,
																																	BgL_arg1339z00_1067);
																															}
																															BgL_arg1325z00_1058
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1328z00_1061,
																																BgL_arg1329z00_1062);
																														}
																														BgL_arg1319z00_1053
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1323z00_1057,
																															BgL_arg1325z00_1058);
																													}
																													BgL_arg1317z00_1051 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1318z00_1052,
																														BgL_arg1319z00_1053);
																												}
																												BgL_arg1316z00_1050 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(9),
																													BgL_arg1317z00_1051);
																											}
																											BgL_arg1315z00_1049 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1316z00_1050,
																												BNIL);
																										}
																										BgL_arg1312z00_1047 =
																											MAKE_YOUNG_PAIR
																											(BgL_thezd2idzd2nilz00_1021,
																											BgL_arg1315z00_1049);
																									}
																									BgL_arg1310z00_1045 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1311z00_1046,
																										BgL_arg1312z00_1047);
																								}
																								BgL_arg1308z00_1043 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(10), BgL_arg1310z00_1045);
																							}
																							{	/* Object/nil.scm 103 */
																								obj_t BgL_list1309z00_1044;

																								BgL_list1309z00_1044 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1308z00_1043, BNIL);
																								BgL_arg1307z00_1042 =
																									BGl_makezd2privatezd2sexpz00zzast_privatez00
																									(CNST_TABLE_REF(11),
																									BgL_idz00_1016,
																									BgL_list1309z00_1044);
																							}
																						}
																						BgL_arg1306z00_1041 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1307z00_1042, BNIL);
																					}
																					BgL_arg1304z00_1039 =
																						MAKE_YOUNG_PAIR(BgL_arg1305z00_1040,
																						BgL_arg1306z00_1041);
																				}
																				BgL_arg1284z00_1038 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
																					BgL_arg1304z00_1039);
																			}
																			BgL_arg1248z00_1033 =
																				BGl_epairifyz00zztools_miscz00
																				(BgL_arg1284z00_1038,
																				BgL_srczd2defzd2_10);
																		}
																		{	/* Object/nil.scm 100 */
																			obj_t BgL_list1249z00_1034;

																			{	/* Object/nil.scm 100 */
																				obj_t BgL_arg1252z00_1035;

																				BgL_arg1252z00_1035 =
																					MAKE_YOUNG_PAIR(BgL_arg1248z00_1033,
																					BNIL);
																				BgL_list1249z00_1034 =
																					MAKE_YOUNG_PAIR(BgL_arg1244z00_1032,
																					BgL_arg1252z00_1035);
																			}
																			return BgL_list1249z00_1034;
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &gen-wide-class-nil */
	obj_t BGl_z62genzd2widezd2classzd2nilzb0zzobject_nilz00(obj_t BgL_envz00_1894,
		obj_t BgL_classz00_1895, obj_t BgL_srczd2defzd2_1896,
		obj_t BgL_importz00_1897)
	{
		{	/* Object/nil.scm 84 */
			return
				BGl_genzd2widezd2classzd2nilzd2zzobject_nilz00(
				((BgL_typez00_bglt) BgL_classz00_1895), BgL_srczd2defzd2_1896,
				BgL_importz00_1897);
		}

	}



/* fill-nil */
	obj_t BGl_fillzd2nilzd2zzobject_nilz00(obj_t BgL_slotsz00_12)
	{
		{	/* Object/nil.scm 126 */
			{
				obj_t BgL_slotsz00_1111;
				obj_t BgL_initsz00_1112;

				BgL_slotsz00_1111 = BgL_slotsz00_12;
				BgL_initsz00_1112 = BNIL;
			BgL_zc3z04anonymousza31545ze3z87_1113:
				if (NULLP(BgL_slotsz00_1111))
					{	/* Object/nil.scm 129 */
						return bgl_reverse_bang(BgL_initsz00_1112);
					}
				else
					{	/* Object/nil.scm 131 */
						obj_t BgL_slotz00_1115;

						BgL_slotz00_1115 = CAR(((obj_t) BgL_slotsz00_1111));
						if (BGl_slotzd2virtualzf3z21zzobject_slotsz00(
								((BgL_slotz00_bglt) BgL_slotz00_1115)))
							{	/* Object/nil.scm 134 */
								obj_t BgL_arg1552z00_1117;

								BgL_arg1552z00_1117 = CDR(((obj_t) BgL_slotsz00_1111));
								{
									obj_t BgL_slotsz00_2231;

									BgL_slotsz00_2231 = BgL_arg1552z00_1117;
									BgL_slotsz00_1111 = BgL_slotsz00_2231;
									goto BgL_zc3z04anonymousza31545ze3z87_1113;
								}
							}
						else
							{	/* Object/nil.scm 136 */
								obj_t BgL_arg1553z00_1118;
								obj_t BgL_arg1559z00_1119;

								BgL_arg1553z00_1118 = CDR(((obj_t) BgL_slotsz00_1111));
								BgL_arg1559z00_1119 =
									MAKE_YOUNG_PAIR(BGl_slotzd2getzd2nilz00zzobject_nilz00
									(BgL_slotz00_1115), BgL_initsz00_1112);
								{
									obj_t BgL_initsz00_2237;
									obj_t BgL_slotsz00_2236;

									BgL_slotsz00_2236 = BgL_arg1553z00_1118;
									BgL_initsz00_2237 = BgL_arg1559z00_1119;
									BgL_initsz00_1112 = BgL_initsz00_2237;
									BgL_slotsz00_1111 = BgL_slotsz00_2236;
									goto BgL_zc3z04anonymousza31545ze3z87_1113;
								}
							}
					}
			}
		}

	}



/* slot-get-nil */
	obj_t BGl_slotzd2getzd2nilz00zzobject_nilz00(obj_t BgL_slotz00_13)
	{
		{	/* Object/nil.scm 142 */
			{	/* Object/nil.scm 145 */
				bool_t BgL_test1898z00_2238;

				{	/* Object/nil.scm 145 */
					obj_t BgL_arg1663z00_1157;

					BgL_arg1663z00_1157 =
						(((BgL_slotz00_bglt) COBJECT(
								((BgL_slotz00_bglt) BgL_slotz00_13)))->BgL_typez00);
					{	/* Object/nil.scm 145 */
						obj_t BgL_classz00_1403;

						BgL_classz00_1403 = BGl_tclassz00zzobject_classz00;
						if (BGL_OBJECTP(BgL_arg1663z00_1157))
							{	/* Object/nil.scm 145 */
								BgL_objectz00_bglt BgL_arg1807z00_1405;

								BgL_arg1807z00_1405 =
									(BgL_objectz00_bglt) (BgL_arg1663z00_1157);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Object/nil.scm 145 */
										long BgL_idxz00_1411;

										BgL_idxz00_1411 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1405);
										BgL_test1898z00_2238 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_1411 + 2L)) == BgL_classz00_1403);
									}
								else
									{	/* Object/nil.scm 145 */
										bool_t BgL_res1865z00_1436;

										{	/* Object/nil.scm 145 */
											obj_t BgL_oclassz00_1419;

											{	/* Object/nil.scm 145 */
												obj_t BgL_arg1815z00_1427;
												long BgL_arg1816z00_1428;

												BgL_arg1815z00_1427 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Object/nil.scm 145 */
													long BgL_arg1817z00_1429;

													BgL_arg1817z00_1429 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1405);
													BgL_arg1816z00_1428 =
														(BgL_arg1817z00_1429 - OBJECT_TYPE);
												}
												BgL_oclassz00_1419 =
													VECTOR_REF(BgL_arg1815z00_1427, BgL_arg1816z00_1428);
											}
											{	/* Object/nil.scm 145 */
												bool_t BgL__ortest_1115z00_1420;

												BgL__ortest_1115z00_1420 =
													(BgL_classz00_1403 == BgL_oclassz00_1419);
												if (BgL__ortest_1115z00_1420)
													{	/* Object/nil.scm 145 */
														BgL_res1865z00_1436 = BgL__ortest_1115z00_1420;
													}
												else
													{	/* Object/nil.scm 145 */
														long BgL_odepthz00_1421;

														{	/* Object/nil.scm 145 */
															obj_t BgL_arg1804z00_1422;

															BgL_arg1804z00_1422 = (BgL_oclassz00_1419);
															BgL_odepthz00_1421 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_1422);
														}
														if ((2L < BgL_odepthz00_1421))
															{	/* Object/nil.scm 145 */
																obj_t BgL_arg1802z00_1424;

																{	/* Object/nil.scm 145 */
																	obj_t BgL_arg1803z00_1425;

																	BgL_arg1803z00_1425 = (BgL_oclassz00_1419);
																	BgL_arg1802z00_1424 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_1425,
																		2L);
																}
																BgL_res1865z00_1436 =
																	(BgL_arg1802z00_1424 == BgL_classz00_1403);
															}
														else
															{	/* Object/nil.scm 145 */
																BgL_res1865z00_1436 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1898z00_2238 = BgL_res1865z00_1436;
									}
							}
						else
							{	/* Object/nil.scm 145 */
								BgL_test1898z00_2238 = ((bool_t) 0);
							}
					}
				}
				if (BgL_test1898z00_2238)
					{	/* Object/nil.scm 146 */
						obj_t BgL_arg1571z00_1125;

						{	/* Object/nil.scm 146 */
							obj_t BgL_arg1573z00_1126;

							{	/* Object/nil.scm 146 */
								obj_t BgL_arg1575z00_1127;
								obj_t BgL_arg1576z00_1128;

								{	/* Object/nil.scm 146 */
									obj_t BgL_arg1584z00_1129;

									BgL_arg1584z00_1129 =
										(((BgL_typez00_bglt) COBJECT(
												((BgL_typez00_bglt)
													(((BgL_slotz00_bglt) COBJECT(
																((BgL_slotz00_bglt) BgL_slotz00_13)))->
														BgL_typez00))))->BgL_idz00);
									{	/* Object/nil.scm 146 */
										obj_t BgL_arg1455z00_1439;

										BgL_arg1455z00_1439 = SYMBOL_TO_STRING(BgL_arg1584z00_1129);
										BgL_arg1575z00_1127 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_1439);
									}
								}
								{	/* Object/nil.scm 146 */
									obj_t BgL_symbolz00_1440;

									BgL_symbolz00_1440 = CNST_TABLE_REF(0);
									{	/* Object/nil.scm 146 */
										obj_t BgL_arg1455z00_1441;

										BgL_arg1455z00_1441 = SYMBOL_TO_STRING(BgL_symbolz00_1440);
										BgL_arg1576z00_1128 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_1441);
									}
								}
								BgL_arg1573z00_1126 =
									string_append(BgL_arg1575z00_1127, BgL_arg1576z00_1128);
							}
							BgL_arg1571z00_1125 = bstring_to_symbol(BgL_arg1573z00_1126);
						}
						return MAKE_YOUNG_PAIR(BgL_arg1571z00_1125, BNIL);
					}
				else
					{	/* Object/nil.scm 147 */
						bool_t BgL_test1903z00_2275;

						{	/* Object/nil.scm 147 */
							obj_t BgL_arg1661z00_1156;

							BgL_arg1661z00_1156 =
								(((BgL_slotz00_bglt) COBJECT(
										((BgL_slotz00_bglt) BgL_slotz00_13)))->BgL_typez00);
							{	/* Object/nil.scm 147 */
								obj_t BgL_classz00_1443;

								BgL_classz00_1443 = BGl_tvecz00zztvector_tvectorz00;
								if (BGL_OBJECTP(BgL_arg1661z00_1156))
									{	/* Object/nil.scm 147 */
										BgL_objectz00_bglt BgL_arg1807z00_1445;

										BgL_arg1807z00_1445 =
											(BgL_objectz00_bglt) (BgL_arg1661z00_1156);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Object/nil.scm 147 */
												long BgL_idxz00_1451;

												BgL_idxz00_1451 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1445);
												BgL_test1903z00_2275 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_1451 + 2L)) == BgL_classz00_1443);
											}
										else
											{	/* Object/nil.scm 147 */
												bool_t BgL_res1866z00_1476;

												{	/* Object/nil.scm 147 */
													obj_t BgL_oclassz00_1459;

													{	/* Object/nil.scm 147 */
														obj_t BgL_arg1815z00_1467;
														long BgL_arg1816z00_1468;

														BgL_arg1815z00_1467 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Object/nil.scm 147 */
															long BgL_arg1817z00_1469;

															BgL_arg1817z00_1469 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1445);
															BgL_arg1816z00_1468 =
																(BgL_arg1817z00_1469 - OBJECT_TYPE);
														}
														BgL_oclassz00_1459 =
															VECTOR_REF(BgL_arg1815z00_1467,
															BgL_arg1816z00_1468);
													}
													{	/* Object/nil.scm 147 */
														bool_t BgL__ortest_1115z00_1460;

														BgL__ortest_1115z00_1460 =
															(BgL_classz00_1443 == BgL_oclassz00_1459);
														if (BgL__ortest_1115z00_1460)
															{	/* Object/nil.scm 147 */
																BgL_res1866z00_1476 = BgL__ortest_1115z00_1460;
															}
														else
															{	/* Object/nil.scm 147 */
																long BgL_odepthz00_1461;

																{	/* Object/nil.scm 147 */
																	obj_t BgL_arg1804z00_1462;

																	BgL_arg1804z00_1462 = (BgL_oclassz00_1459);
																	BgL_odepthz00_1461 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_1462);
																}
																if ((2L < BgL_odepthz00_1461))
																	{	/* Object/nil.scm 147 */
																		obj_t BgL_arg1802z00_1464;

																		{	/* Object/nil.scm 147 */
																			obj_t BgL_arg1803z00_1465;

																			BgL_arg1803z00_1465 =
																				(BgL_oclassz00_1459);
																			BgL_arg1802z00_1464 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_1465, 2L);
																		}
																		BgL_res1866z00_1476 =
																			(BgL_arg1802z00_1464 ==
																			BgL_classz00_1443);
																	}
																else
																	{	/* Object/nil.scm 147 */
																		BgL_res1866z00_1476 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test1903z00_2275 = BgL_res1866z00_1476;
											}
									}
								else
									{	/* Object/nil.scm 147 */
										BgL_test1903z00_2275 = ((bool_t) 0);
									}
							}
						}
						if (BgL_test1903z00_2275)
							{	/* Object/nil.scm 148 */
								obj_t BgL_arg1593z00_1133;

								{	/* Object/nil.scm 148 */
									obj_t BgL_arg1594z00_1134;
									obj_t BgL_arg1595z00_1135;

									{	/* Object/nil.scm 148 */
										obj_t BgL_arg1602z00_1136;

										{	/* Object/nil.scm 148 */
											obj_t BgL_arg1605z00_1137;

											BgL_arg1605z00_1137 =
												(((BgL_typez00_bglt) COBJECT(
														((BgL_typez00_bglt)
															(((BgL_slotz00_bglt) COBJECT(
																		((BgL_slotz00_bglt) BgL_slotz00_13)))->
																BgL_typez00))))->BgL_idz00);
											BgL_arg1602z00_1136 =
												MAKE_YOUNG_PAIR(BgL_arg1605z00_1137, BNIL);
										}
										BgL_arg1594z00_1134 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(18), BgL_arg1602z00_1136);
									}
									{	/* Object/nil.scm 148 */
										obj_t BgL_arg1609z00_1139;

										{	/* Object/nil.scm 148 */
											obj_t BgL_arg1611z00_1140;

											BgL_arg1611z00_1140 = MAKE_YOUNG_PAIR(BNIL, BNIL);
											BgL_arg1609z00_1139 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
												BgL_arg1611z00_1140);
										}
										BgL_arg1595z00_1135 =
											MAKE_YOUNG_PAIR(BgL_arg1609z00_1139, BNIL);
									}
									BgL_arg1593z00_1133 =
										MAKE_YOUNG_PAIR(BgL_arg1594z00_1134, BgL_arg1595z00_1135);
								}
								return MAKE_YOUNG_PAIR(CNST_TABLE_REF(19), BgL_arg1593z00_1133);
							}
						else
							{	/* Object/nil.scm 149 */
								bool_t BgL_test1908z00_2314;

								{	/* Object/nil.scm 149 */
									obj_t BgL_arg1654z00_1155;

									BgL_arg1654z00_1155 =
										(((BgL_slotz00_bglt) COBJECT(
												((BgL_slotz00_bglt) BgL_slotz00_13)))->BgL_typez00);
									{	/* Object/nil.scm 149 */
										obj_t BgL_classz00_1478;

										BgL_classz00_1478 = BGl_jarrayz00zzforeign_jtypez00;
										if (BGL_OBJECTP(BgL_arg1654z00_1155))
											{	/* Object/nil.scm 149 */
												BgL_objectz00_bglt BgL_arg1807z00_1480;

												BgL_arg1807z00_1480 =
													(BgL_objectz00_bglt) (BgL_arg1654z00_1155);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Object/nil.scm 149 */
														long BgL_idxz00_1486;

														BgL_idxz00_1486 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1480);
														BgL_test1908z00_2314 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_1486 + 2L)) == BgL_classz00_1478);
													}
												else
													{	/* Object/nil.scm 149 */
														bool_t BgL_res1867z00_1511;

														{	/* Object/nil.scm 149 */
															obj_t BgL_oclassz00_1494;

															{	/* Object/nil.scm 149 */
																obj_t BgL_arg1815z00_1502;
																long BgL_arg1816z00_1503;

																BgL_arg1815z00_1502 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Object/nil.scm 149 */
																	long BgL_arg1817z00_1504;

																	BgL_arg1817z00_1504 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1480);
																	BgL_arg1816z00_1503 =
																		(BgL_arg1817z00_1504 - OBJECT_TYPE);
																}
																BgL_oclassz00_1494 =
																	VECTOR_REF(BgL_arg1815z00_1502,
																	BgL_arg1816z00_1503);
															}
															{	/* Object/nil.scm 149 */
																bool_t BgL__ortest_1115z00_1495;

																BgL__ortest_1115z00_1495 =
																	(BgL_classz00_1478 == BgL_oclassz00_1494);
																if (BgL__ortest_1115z00_1495)
																	{	/* Object/nil.scm 149 */
																		BgL_res1867z00_1511 =
																			BgL__ortest_1115z00_1495;
																	}
																else
																	{	/* Object/nil.scm 149 */
																		long BgL_odepthz00_1496;

																		{	/* Object/nil.scm 149 */
																			obj_t BgL_arg1804z00_1497;

																			BgL_arg1804z00_1497 =
																				(BgL_oclassz00_1494);
																			BgL_odepthz00_1496 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_1497);
																		}
																		if ((2L < BgL_odepthz00_1496))
																			{	/* Object/nil.scm 149 */
																				obj_t BgL_arg1802z00_1499;

																				{	/* Object/nil.scm 149 */
																					obj_t BgL_arg1803z00_1500;

																					BgL_arg1803z00_1500 =
																						(BgL_oclassz00_1494);
																					BgL_arg1802z00_1499 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_1500, 2L);
																				}
																				BgL_res1867z00_1511 =
																					(BgL_arg1802z00_1499 ==
																					BgL_classz00_1478);
																			}
																		else
																			{	/* Object/nil.scm 149 */
																				BgL_res1867z00_1511 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test1908z00_2314 = BgL_res1867z00_1511;
													}
											}
										else
											{	/* Object/nil.scm 149 */
												BgL_test1908z00_2314 = ((bool_t) 0);
											}
									}
								}
								if (BgL_test1908z00_2314)
									{	/* Object/nil.scm 150 */
										obj_t BgL_arg1615z00_1143;
										obj_t BgL_arg1616z00_1144;

										{	/* Object/nil.scm 150 */
											obj_t BgL_arg1625z00_1145;

											{	/* Object/nil.scm 150 */
												obj_t BgL_arg1626z00_1146;
												obj_t BgL_arg1627z00_1147;

												{	/* Object/nil.scm 150 */
													obj_t BgL_symbolz00_1512;

													BgL_symbolz00_1512 = CNST_TABLE_REF(20);
													{	/* Object/nil.scm 150 */
														obj_t BgL_arg1455z00_1513;

														BgL_arg1455z00_1513 =
															SYMBOL_TO_STRING(BgL_symbolz00_1512);
														BgL_arg1626z00_1146 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1455z00_1513);
													}
												}
												{	/* Object/nil.scm 150 */
													obj_t BgL_arg1629z00_1148;

													BgL_arg1629z00_1148 =
														(((BgL_typez00_bglt) COBJECT(
																((BgL_typez00_bglt)
																	(((BgL_slotz00_bglt) COBJECT(
																				((BgL_slotz00_bglt) BgL_slotz00_13)))->
																		BgL_typez00))))->BgL_idz00);
													{	/* Object/nil.scm 150 */
														obj_t BgL_arg1455z00_1516;

														BgL_arg1455z00_1516 =
															SYMBOL_TO_STRING(BgL_arg1629z00_1148);
														BgL_arg1627z00_1147 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1455z00_1516);
													}
												}
												BgL_arg1625z00_1145 =
													string_append(BgL_arg1626z00_1146,
													BgL_arg1627z00_1147);
											}
											BgL_arg1615z00_1143 =
												bstring_to_symbol(BgL_arg1625z00_1145);
										}
										BgL_arg1616z00_1144 = MAKE_YOUNG_PAIR(BINT(0L), BNIL);
										return
											MAKE_YOUNG_PAIR(BgL_arg1615z00_1143, BgL_arg1616z00_1144);
									}
								else
									{	/* Object/nil.scm 151 */
										bool_t BgL_test1913z00_2353;

										{	/* Object/nil.scm 151 */
											obj_t BgL_arg1651z00_1154;

											BgL_arg1651z00_1154 =
												(((BgL_slotz00_bglt) COBJECT(
														((BgL_slotz00_bglt) BgL_slotz00_13)))->BgL_typez00);
											BgL_test1913z00_2353 =
												BGl_bigloozd2typezf3z21zztype_typez00(
												((BgL_typez00_bglt) BgL_arg1651z00_1154));
										}
										if (BgL_test1913z00_2353)
											{	/* Object/nil.scm 152 */
												obj_t BgL_arg1646z00_1152;

												BgL_arg1646z00_1152 =
													(((BgL_slotz00_bglt) COBJECT(
															((BgL_slotz00_bglt) BgL_slotz00_13)))->
													BgL_typez00);
												return
													BGl_bigloozd2primitivezd2typezd2nilzd2zzobject_nilz00
													(BgL_arg1646z00_1152, BgL_slotz00_13);
											}
										else
											{	/* Object/nil.scm 154 */
												obj_t BgL_arg1650z00_1153;

												BgL_arg1650z00_1153 =
													(((BgL_slotz00_bglt) COBJECT(
															((BgL_slotz00_bglt) BgL_slotz00_13)))->
													BgL_typez00);
												return
													BGl_externzd2typezd2nilz00zzobject_nilz00
													(BgL_arg1650z00_1153, BgL_slotz00_13);
											}
									}
							}
					}
			}
		}

	}



/* type-nil-value */
	BGL_EXPORTED_DEF obj_t BGl_typezd2nilzd2valuez00zzobject_nilz00(obj_t
		BgL_typez00_14, obj_t BgL_slotz00_15)
	{
		{	/* Object/nil.scm 159 */
			{	/* Object/nil.scm 161 */
				bool_t BgL_test1914z00_2364;

				{	/* Object/nil.scm 161 */
					obj_t BgL_classz00_1518;

					BgL_classz00_1518 = BGl_tclassz00zzobject_classz00;
					if (BGL_OBJECTP(BgL_typez00_14))
						{	/* Object/nil.scm 161 */
							BgL_objectz00_bglt BgL_arg1807z00_1520;

							BgL_arg1807z00_1520 = (BgL_objectz00_bglt) (BgL_typez00_14);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Object/nil.scm 161 */
									long BgL_idxz00_1526;

									BgL_idxz00_1526 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1520);
									BgL_test1914z00_2364 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_1526 + 2L)) == BgL_classz00_1518);
								}
							else
								{	/* Object/nil.scm 161 */
									bool_t BgL_res1868z00_1551;

									{	/* Object/nil.scm 161 */
										obj_t BgL_oclassz00_1534;

										{	/* Object/nil.scm 161 */
											obj_t BgL_arg1815z00_1542;
											long BgL_arg1816z00_1543;

											BgL_arg1815z00_1542 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Object/nil.scm 161 */
												long BgL_arg1817z00_1544;

												BgL_arg1817z00_1544 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1520);
												BgL_arg1816z00_1543 =
													(BgL_arg1817z00_1544 - OBJECT_TYPE);
											}
											BgL_oclassz00_1534 =
												VECTOR_REF(BgL_arg1815z00_1542, BgL_arg1816z00_1543);
										}
										{	/* Object/nil.scm 161 */
											bool_t BgL__ortest_1115z00_1535;

											BgL__ortest_1115z00_1535 =
												(BgL_classz00_1518 == BgL_oclassz00_1534);
											if (BgL__ortest_1115z00_1535)
												{	/* Object/nil.scm 161 */
													BgL_res1868z00_1551 = BgL__ortest_1115z00_1535;
												}
											else
												{	/* Object/nil.scm 161 */
													long BgL_odepthz00_1536;

													{	/* Object/nil.scm 161 */
														obj_t BgL_arg1804z00_1537;

														BgL_arg1804z00_1537 = (BgL_oclassz00_1534);
														BgL_odepthz00_1536 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_1537);
													}
													if ((2L < BgL_odepthz00_1536))
														{	/* Object/nil.scm 161 */
															obj_t BgL_arg1802z00_1539;

															{	/* Object/nil.scm 161 */
																obj_t BgL_arg1803z00_1540;

																BgL_arg1803z00_1540 = (BgL_oclassz00_1534);
																BgL_arg1802z00_1539 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_1540,
																	2L);
															}
															BgL_res1868z00_1551 =
																(BgL_arg1802z00_1539 == BgL_classz00_1518);
														}
													else
														{	/* Object/nil.scm 161 */
															BgL_res1868z00_1551 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1914z00_2364 = BgL_res1868z00_1551;
								}
						}
					else
						{	/* Object/nil.scm 161 */
							BgL_test1914z00_2364 = ((bool_t) 0);
						}
				}
				if (BgL_test1914z00_2364)
					{	/* Object/nil.scm 162 */
						obj_t BgL_arg1675z00_1159;

						{	/* Object/nil.scm 162 */
							obj_t BgL_arg1678z00_1160;

							BgL_arg1678z00_1160 =
								(((BgL_typez00_bglt) COBJECT(
										((BgL_typez00_bglt) BgL_typez00_14)))->BgL_idz00);
							BgL_arg1675z00_1159 = MAKE_YOUNG_PAIR(BgL_arg1678z00_1160, BNIL);
						}
						return MAKE_YOUNG_PAIR(CNST_TABLE_REF(21), BgL_arg1675z00_1159);
					}
				else
					{	/* Object/nil.scm 163 */
						bool_t BgL_test1919z00_2392;

						{	/* Object/nil.scm 163 */
							obj_t BgL_classz00_1553;

							BgL_classz00_1553 = BGl_tvecz00zztvector_tvectorz00;
							if (BGL_OBJECTP(BgL_typez00_14))
								{	/* Object/nil.scm 163 */
									BgL_objectz00_bglt BgL_arg1807z00_1555;

									BgL_arg1807z00_1555 = (BgL_objectz00_bglt) (BgL_typez00_14);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Object/nil.scm 163 */
											long BgL_idxz00_1561;

											BgL_idxz00_1561 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1555);
											BgL_test1919z00_2392 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_1561 + 2L)) == BgL_classz00_1553);
										}
									else
										{	/* Object/nil.scm 163 */
											bool_t BgL_res1869z00_1586;

											{	/* Object/nil.scm 163 */
												obj_t BgL_oclassz00_1569;

												{	/* Object/nil.scm 163 */
													obj_t BgL_arg1815z00_1577;
													long BgL_arg1816z00_1578;

													BgL_arg1815z00_1577 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Object/nil.scm 163 */
														long BgL_arg1817z00_1579;

														BgL_arg1817z00_1579 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1555);
														BgL_arg1816z00_1578 =
															(BgL_arg1817z00_1579 - OBJECT_TYPE);
													}
													BgL_oclassz00_1569 =
														VECTOR_REF(BgL_arg1815z00_1577,
														BgL_arg1816z00_1578);
												}
												{	/* Object/nil.scm 163 */
													bool_t BgL__ortest_1115z00_1570;

													BgL__ortest_1115z00_1570 =
														(BgL_classz00_1553 == BgL_oclassz00_1569);
													if (BgL__ortest_1115z00_1570)
														{	/* Object/nil.scm 163 */
															BgL_res1869z00_1586 = BgL__ortest_1115z00_1570;
														}
													else
														{	/* Object/nil.scm 163 */
															long BgL_odepthz00_1571;

															{	/* Object/nil.scm 163 */
																obj_t BgL_arg1804z00_1572;

																BgL_arg1804z00_1572 = (BgL_oclassz00_1569);
																BgL_odepthz00_1571 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_1572);
															}
															if ((2L < BgL_odepthz00_1571))
																{	/* Object/nil.scm 163 */
																	obj_t BgL_arg1802z00_1574;

																	{	/* Object/nil.scm 163 */
																		obj_t BgL_arg1803z00_1575;

																		BgL_arg1803z00_1575 = (BgL_oclassz00_1569);
																		BgL_arg1802z00_1574 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_1575, 2L);
																	}
																	BgL_res1869z00_1586 =
																		(BgL_arg1802z00_1574 == BgL_classz00_1553);
																}
															else
																{	/* Object/nil.scm 163 */
																	BgL_res1869z00_1586 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test1919z00_2392 = BgL_res1869z00_1586;
										}
								}
							else
								{	/* Object/nil.scm 163 */
									BgL_test1919z00_2392 = ((bool_t) 0);
								}
						}
						if (BgL_test1919z00_2392)
							{	/* Object/nil.scm 164 */
								obj_t BgL_arg1681z00_1162;

								{	/* Object/nil.scm 164 */
									obj_t BgL_arg1688z00_1163;
									obj_t BgL_arg1689z00_1164;

									{	/* Object/nil.scm 164 */
										obj_t BgL_arg1691z00_1165;

										{	/* Object/nil.scm 164 */
											obj_t BgL_arg1692z00_1166;

											BgL_arg1692z00_1166 =
												(((BgL_typez00_bglt) COBJECT(
														((BgL_typez00_bglt) BgL_typez00_14)))->BgL_idz00);
											BgL_arg1691z00_1165 =
												MAKE_YOUNG_PAIR(BgL_arg1692z00_1166, BNIL);
										}
										BgL_arg1688z00_1163 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(18), BgL_arg1691z00_1165);
									}
									{	/* Object/nil.scm 164 */
										obj_t BgL_arg1699z00_1167;

										{	/* Object/nil.scm 164 */
											obj_t BgL_arg1700z00_1168;

											BgL_arg1700z00_1168 = MAKE_YOUNG_PAIR(BNIL, BNIL);
											BgL_arg1699z00_1167 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
												BgL_arg1700z00_1168);
										}
										BgL_arg1689z00_1164 =
											MAKE_YOUNG_PAIR(BgL_arg1699z00_1167, BNIL);
									}
									BgL_arg1681z00_1162 =
										MAKE_YOUNG_PAIR(BgL_arg1688z00_1163, BgL_arg1689z00_1164);
								}
								return MAKE_YOUNG_PAIR(CNST_TABLE_REF(19), BgL_arg1681z00_1162);
							}
						else
							{	/* Object/nil.scm 165 */
								bool_t BgL_test1924z00_2427;

								{	/* Object/nil.scm 165 */
									obj_t BgL_classz00_1588;

									BgL_classz00_1588 = BGl_jarrayz00zzforeign_jtypez00;
									if (BGL_OBJECTP(BgL_typez00_14))
										{	/* Object/nil.scm 165 */
											BgL_objectz00_bglt BgL_arg1807z00_1590;

											BgL_arg1807z00_1590 =
												(BgL_objectz00_bglt) (BgL_typez00_14);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Object/nil.scm 165 */
													long BgL_idxz00_1596;

													BgL_idxz00_1596 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1590);
													BgL_test1924z00_2427 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_1596 + 2L)) == BgL_classz00_1588);
												}
											else
												{	/* Object/nil.scm 165 */
													bool_t BgL_res1870z00_1621;

													{	/* Object/nil.scm 165 */
														obj_t BgL_oclassz00_1604;

														{	/* Object/nil.scm 165 */
															obj_t BgL_arg1815z00_1612;
															long BgL_arg1816z00_1613;

															BgL_arg1815z00_1612 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Object/nil.scm 165 */
																long BgL_arg1817z00_1614;

																BgL_arg1817z00_1614 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1590);
																BgL_arg1816z00_1613 =
																	(BgL_arg1817z00_1614 - OBJECT_TYPE);
															}
															BgL_oclassz00_1604 =
																VECTOR_REF(BgL_arg1815z00_1612,
																BgL_arg1816z00_1613);
														}
														{	/* Object/nil.scm 165 */
															bool_t BgL__ortest_1115z00_1605;

															BgL__ortest_1115z00_1605 =
																(BgL_classz00_1588 == BgL_oclassz00_1604);
															if (BgL__ortest_1115z00_1605)
																{	/* Object/nil.scm 165 */
																	BgL_res1870z00_1621 =
																		BgL__ortest_1115z00_1605;
																}
															else
																{	/* Object/nil.scm 165 */
																	long BgL_odepthz00_1606;

																	{	/* Object/nil.scm 165 */
																		obj_t BgL_arg1804z00_1607;

																		BgL_arg1804z00_1607 = (BgL_oclassz00_1604);
																		BgL_odepthz00_1606 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_1607);
																	}
																	if ((2L < BgL_odepthz00_1606))
																		{	/* Object/nil.scm 165 */
																			obj_t BgL_arg1802z00_1609;

																			{	/* Object/nil.scm 165 */
																				obj_t BgL_arg1803z00_1610;

																				BgL_arg1803z00_1610 =
																					(BgL_oclassz00_1604);
																				BgL_arg1802z00_1609 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_1610, 2L);
																			}
																			BgL_res1870z00_1621 =
																				(BgL_arg1802z00_1609 ==
																				BgL_classz00_1588);
																		}
																	else
																		{	/* Object/nil.scm 165 */
																			BgL_res1870z00_1621 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test1924z00_2427 = BgL_res1870z00_1621;
												}
										}
									else
										{	/* Object/nil.scm 165 */
											BgL_test1924z00_2427 = ((bool_t) 0);
										}
								}
								if (BgL_test1924z00_2427)
									{	/* Object/nil.scm 166 */
										obj_t BgL_arg1702z00_1170;
										obj_t BgL_arg1703z00_1171;

										{	/* Object/nil.scm 166 */
											obj_t BgL_arg1705z00_1172;

											{	/* Object/nil.scm 166 */
												obj_t BgL_arg1708z00_1173;
												obj_t BgL_arg1709z00_1174;

												{	/* Object/nil.scm 166 */
													obj_t BgL_symbolz00_1622;

													BgL_symbolz00_1622 = CNST_TABLE_REF(20);
													{	/* Object/nil.scm 166 */
														obj_t BgL_arg1455z00_1623;

														BgL_arg1455z00_1623 =
															SYMBOL_TO_STRING(BgL_symbolz00_1622);
														BgL_arg1708z00_1173 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1455z00_1623);
													}
												}
												{	/* Object/nil.scm 166 */
													obj_t BgL_arg1710z00_1175;

													BgL_arg1710z00_1175 =
														(((BgL_typez00_bglt) COBJECT(
																((BgL_typez00_bglt) BgL_typez00_14)))->
														BgL_idz00);
													{	/* Object/nil.scm 166 */
														obj_t BgL_arg1455z00_1626;

														BgL_arg1455z00_1626 =
															SYMBOL_TO_STRING(BgL_arg1710z00_1175);
														BgL_arg1709z00_1174 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1455z00_1626);
													}
												}
												BgL_arg1705z00_1172 =
													string_append(BgL_arg1708z00_1173,
													BgL_arg1709z00_1174);
											}
											BgL_arg1702z00_1170 =
												bstring_to_symbol(BgL_arg1705z00_1172);
										}
										BgL_arg1703z00_1171 = MAKE_YOUNG_PAIR(BINT(0L), BNIL);
										return
											MAKE_YOUNG_PAIR(BgL_arg1702z00_1170, BgL_arg1703z00_1171);
									}
								else
									{	/* Object/nil.scm 165 */
										if (BGl_bigloozd2typezf3z21zztype_typez00(
												((BgL_typez00_bglt) BgL_typez00_14)))
											{	/* Object/nil.scm 167 */
												BGL_TAIL return
													BGl_bigloozd2primitivezd2typezd2nilzd2zzobject_nilz00
													(BgL_typez00_14, BgL_slotz00_15);
											}
										else
											{	/* Object/nil.scm 167 */
												BGL_TAIL return
													BGl_externzd2typezd2nilz00zzobject_nilz00
													(BgL_typez00_14, BgL_slotz00_15);
											}
									}
							}
					}
			}
		}

	}



/* &type-nil-value */
	obj_t BGl_z62typezd2nilzd2valuez62zzobject_nilz00(obj_t BgL_envz00_1898,
		obj_t BgL_typez00_1899, obj_t BgL_slotz00_1900)
	{
		{	/* Object/nil.scm 159 */
			return
				BGl_typezd2nilzd2valuez00zzobject_nilz00(BgL_typez00_1899,
				BgL_slotz00_1900);
		}

	}



/* bigloo-primitive-type-nil */
	obj_t BGl_bigloozd2primitivezd2typezd2nilzd2zzobject_nilz00(obj_t
		BgL_typez00_16, obj_t BgL_slotz00_17)
	{
		{	/* Object/nil.scm 175 */
			{	/* Object/nil.scm 177 */
				bool_t BgL_test1930z00_2468;

				if ((BgL_typez00_16 == BGl_za2objza2z00zztype_cachez00))
					{	/* Object/nil.scm 177 */
						BgL_test1930z00_2468 = ((bool_t) 1);
					}
				else
					{	/* Object/nil.scm 177 */
						BgL_test1930z00_2468 =
							(BgL_typez00_16 == BGl_za2unspecza2z00zztype_cachez00);
					}
				if (BgL_test1930z00_2468)
					{	/* Object/nil.scm 177 */
						return BUNSPEC;
					}
				else
					{	/* Object/nil.scm 177 */
						if ((BgL_typez00_16 == BGl_za2cellza2z00zztype_cachez00))
							{	/* Object/nil.scm 178 */
								return CNST_TABLE_REF(22);
							}
						else
							{	/* Object/nil.scm 178 */
								if ((BgL_typez00_16 == BGl_za2bintza2z00zztype_cachez00))
									{	/* Object/nil.scm 179 */
										return BINT(0L);
									}
								else
									{	/* Object/nil.scm 179 */
										if ((BgL_typez00_16 == BGl_za2bllongza2z00zztype_cachez00))
											{	/* Object/nil.scm 180 */
												return CNST_TABLE_REF(23);
											}
										else
											{	/* Object/nil.scm 180 */
												if (
													(BgL_typez00_16 ==
														BGl_za2belongza2z00zztype_cachez00))
													{	/* Object/nil.scm 181 */
														return CNST_TABLE_REF(24);
													}
												else
													{	/* Object/nil.scm 181 */
														if (
															(BgL_typez00_16 ==
																BGl_za2bignumza2z00zztype_cachez00))
															{	/* Object/nil.scm 182 */
																return CNST_TABLE_REF(25);
															}
														else
															{	/* Object/nil.scm 182 */
																if (
																	(BgL_typez00_16 ==
																		BGl_za2boolza2z00zztype_cachez00))
																	{	/* Object/nil.scm 183 */
																		return BFALSE;
																	}
																else
																	{	/* Object/nil.scm 183 */
																		if (
																			(BgL_typez00_16 ==
																				BGl_za2brealza2z00zztype_cachez00))
																			{	/* Object/nil.scm 184 */
																				return
																					BGL_REAL_CNST
																					(BGl_real1877z00zzobject_nilz00);
																			}
																		else
																			{	/* Object/nil.scm 184 */
																				if (
																					(BgL_typez00_16 ==
																						BGl_za2bcharza2z00zztype_cachez00))
																					{	/* Object/nil.scm 185 */
																						return BCHAR(((unsigned char) '_'));
																					}
																				else
																					{	/* Object/nil.scm 186 */
																						bool_t BgL_test1940z00_2494;

																						if (
																							(BgL_typez00_16 ==
																								BGl_za2bnilza2z00zztype_cachez00))
																							{	/* Object/nil.scm 186 */
																								BgL_test1940z00_2494 =
																									((bool_t) 1);
																							}
																						else
																							{	/* Object/nil.scm 186 */
																								BgL_test1940z00_2494 =
																									(BgL_typez00_16 ==
																									BGl_za2pairzd2nilza2zd2zztype_cachez00);
																							}
																						if (BgL_test1940z00_2494)
																							{	/* Object/nil.scm 186 */
																								return CNST_TABLE_REF(26);
																							}
																						else
																							{	/* Object/nil.scm 186 */
																								if (
																									(BgL_typez00_16 ==
																										BGl_za2pairza2z00zztype_cachez00))
																									{	/* Object/nil.scm 187 */
																										return CNST_TABLE_REF(27);
																									}
																								else
																									{	/* Object/nil.scm 187 */
																										if (
																											(BgL_typez00_16 ==
																												BGl_za2epairza2z00zztype_cachez00))
																											{	/* Object/nil.scm 188 */
																												return
																													CNST_TABLE_REF(28);
																											}
																										else
																											{	/* Object/nil.scm 188 */
																												if (
																													(BgL_typez00_16 ==
																														BGl_za2bstringza2z00zztype_cachez00))
																													{	/* Object/nil.scm 189 */
																														return
																															BGl_string1878z00zzobject_nilz00;
																													}
																												else
																													{	/* Object/nil.scm 189 */
																														if (
																															(BgL_typez00_16 ==
																																BGl_za2symbolza2z00zztype_cachez00))
																															{	/* Object/nil.scm 190 */
																																return
																																	CNST_TABLE_REF
																																	(29);
																															}
																														else
																															{	/* Object/nil.scm 190 */
																																if (
																																	(BgL_typez00_16
																																		==
																																		BGl_za2keywordza2z00zztype_cachez00))
																																	{	/* Object/nil.scm 191 */
																																		return
																																			CNST_TABLE_REF
																																			(30);
																																	}
																																else
																																	{	/* Object/nil.scm 191 */
																																		if (
																																			(BgL_typez00_16
																																				==
																																				BGl_za2vectorza2z00zztype_cachez00))
																																			{	/* Object/nil.scm 192 */
																																				return
																																					CNST_TABLE_REF
																																					(31);
																																			}
																																		else
																																			{	/* Object/nil.scm 192 */
																																				if (
																																					(BgL_typez00_16
																																						==
																																						BGl_za2procedureza2z00zztype_cachez00))
																																					{	/* Object/nil.scm 193 */
																																						return
																																							CNST_TABLE_REF
																																							(32);
																																					}
																																				else
																																					{	/* Object/nil.scm 195 */
																																						obj_t
																																							BgL_casezd2valuezd2_1179;
																																						BgL_casezd2valuezd2_1179
																																							=
																																							(((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) BgL_typez00_16)))->BgL_idz00);
																																						if (
																																							(BgL_casezd2valuezd2_1179
																																								==
																																								CNST_TABLE_REF
																																								(33)))
																																							{	/* Object/nil.scm 195 */
																																								return
																																									CNST_TABLE_REF
																																									(34);
																																							}
																																						else
																																							{	/* Object/nil.scm 195 */
																																								if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(35)))
																																									{	/* Object/nil.scm 195 */
																																										return
																																											CNST_TABLE_REF
																																											(36);
																																									}
																																								else
																																									{	/* Object/nil.scm 195 */
																																										if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(37)))
																																											{	/* Object/nil.scm 195 */
																																												return
																																													CNST_TABLE_REF
																																													(36);
																																											}
																																										else
																																											{	/* Object/nil.scm 195 */
																																												if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(38)))
																																													{	/* Object/nil.scm 195 */
																																														return
																																															CNST_TABLE_REF
																																															(36);
																																													}
																																												else
																																													{	/* Object/nil.scm 195 */
																																														if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(39)))
																																															{	/* Object/nil.scm 195 */
																																																return
																																																	CNST_TABLE_REF
																																																	(40);
																																															}
																																														else
																																															{	/* Object/nil.scm 195 */
																																																if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(41)))
																																																	{	/* Object/nil.scm 195 */
																																																		return
																																																			CNST_TABLE_REF
																																																			(42);
																																																	}
																																																else
																																																	{	/* Object/nil.scm 195 */
																																																		bool_t
																																																			BgL_test1955z00_2545;
																																																		{	/* Object/nil.scm 195 */
																																																			bool_t
																																																				BgL__ortest_1079z00_1240;
																																																			BgL__ortest_1079z00_1240
																																																				=
																																																				(BgL_casezd2valuezd2_1179
																																																				==
																																																				CNST_TABLE_REF
																																																				(2));
																																																			if (BgL__ortest_1079z00_1240)
																																																				{	/* Object/nil.scm 195 */
																																																					BgL_test1955z00_2545
																																																						=
																																																						BgL__ortest_1079z00_1240;
																																																				}
																																																			else
																																																				{	/* Object/nil.scm 195 */
																																																					BgL_test1955z00_2545
																																																						=
																																																						(BgL_casezd2valuezd2_1179
																																																						==
																																																						CNST_TABLE_REF
																																																						(43));
																																																				}
																																																		}
																																																		if (BgL_test1955z00_2545)
																																																			{	/* Object/nil.scm 195 */
																																																				return
																																																					BUNSPEC;
																																																			}
																																																		else
																																																			{	/* Object/nil.scm 195 */
																																																				if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(44)))
																																																					{	/* Object/nil.scm 195 */
																																																						return
																																																							CNST_TABLE_REF
																																																							(22);
																																																					}
																																																				else
																																																					{	/* Object/nil.scm 195 */
																																																						if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(45)))
																																																							{	/* Object/nil.scm 195 */
																																																								return
																																																									BINT
																																																									(0L);
																																																							}
																																																						else
																																																							{	/* Object/nil.scm 195 */
																																																								if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(46)))
																																																									{	/* Object/nil.scm 195 */
																																																										return
																																																											CNST_TABLE_REF
																																																											(23);
																																																									}
																																																								else
																																																									{	/* Object/nil.scm 195 */
																																																										if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(47)))
																																																											{	/* Object/nil.scm 195 */
																																																												return
																																																													CNST_TABLE_REF
																																																													(24);
																																																											}
																																																										else
																																																											{	/* Object/nil.scm 195 */
																																																												bool_t
																																																													BgL_test1961z00_2567;
																																																												{	/* Object/nil.scm 195 */
																																																													bool_t
																																																														BgL__ortest_1080z00_1239;
																																																													BgL__ortest_1080z00_1239
																																																														=
																																																														(BgL_casezd2valuezd2_1179
																																																														==
																																																														CNST_TABLE_REF
																																																														(48));
																																																													if (BgL__ortest_1080z00_1239)
																																																														{	/* Object/nil.scm 195 */
																																																															BgL_test1961z00_2567
																																																																=
																																																																BgL__ortest_1080z00_1239;
																																																														}
																																																													else
																																																														{	/* Object/nil.scm 195 */
																																																															BgL_test1961z00_2567
																																																																=
																																																																(BgL_casezd2valuezd2_1179
																																																																==
																																																																CNST_TABLE_REF
																																																																(49));
																																																														}
																																																												}
																																																												if (BgL_test1961z00_2567)
																																																													{	/* Object/nil.scm 195 */
																																																														return
																																																															BFALSE;
																																																													}
																																																												else
																																																													{	/* Object/nil.scm 195 */
																																																														if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(50)))
																																																															{	/* Object/nil.scm 195 */
																																																																return
																																																																	BGL_REAL_CNST
																																																																	(BGl_real1877z00zzobject_nilz00);
																																																															}
																																																														else
																																																															{	/* Object/nil.scm 195 */
																																																																if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(51)))
																																																																	{	/* Object/nil.scm 195 */
																																																																		return
																																																																			BCHAR
																																																																			(
																																																																			((unsigned char) '_'));
																																																																	}
																																																																else
																																																																	{	/* Object/nil.scm 195 */
																																																																		bool_t
																																																																			BgL_test1965z00_2580;
																																																																		{	/* Object/nil.scm 195 */
																																																																			bool_t
																																																																				BgL__ortest_1081z00_1238;
																																																																			BgL__ortest_1081z00_1238
																																																																				=
																																																																				(BgL_casezd2valuezd2_1179
																																																																				==
																																																																				CNST_TABLE_REF
																																																																				(52));
																																																																			if (BgL__ortest_1081z00_1238)
																																																																				{	/* Object/nil.scm 195 */
																																																																					BgL_test1965z00_2580
																																																																						=
																																																																						BgL__ortest_1081z00_1238;
																																																																				}
																																																																			else
																																																																				{	/* Object/nil.scm 195 */
																																																																					BgL_test1965z00_2580
																																																																						=
																																																																						(BgL_casezd2valuezd2_1179
																																																																						==
																																																																						CNST_TABLE_REF
																																																																						(53));
																																																																				}
																																																																		}
																																																																		if (BgL_test1965z00_2580)
																																																																			{	/* Object/nil.scm 195 */
																																																																				return
																																																																					CNST_TABLE_REF
																																																																					(26);
																																																																			}
																																																																		else
																																																																			{	/* Object/nil.scm 195 */
																																																																				if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(54)))
																																																																					{	/* Object/nil.scm 195 */
																																																																						return
																																																																							CNST_TABLE_REF
																																																																							(27);
																																																																					}
																																																																				else
																																																																					{	/* Object/nil.scm 195 */
																																																																						if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(55)))
																																																																							{	/* Object/nil.scm 195 */
																																																																								return
																																																																									CNST_TABLE_REF
																																																																									(28);
																																																																							}
																																																																						else
																																																																							{	/* Object/nil.scm 195 */
																																																																								if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(56)))
																																																																									{	/* Object/nil.scm 195 */
																																																																										return
																																																																											BGl_string1878z00zzobject_nilz00;
																																																																									}
																																																																								else
																																																																									{	/* Object/nil.scm 195 */
																																																																										if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(57)))
																																																																											{	/* Object/nil.scm 195 */
																																																																												return
																																																																													CNST_TABLE_REF
																																																																													(29);
																																																																											}
																																																																										else
																																																																											{	/* Object/nil.scm 195 */
																																																																												if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(58)))
																																																																													{	/* Object/nil.scm 195 */
																																																																														return
																																																																															CNST_TABLE_REF
																																																																															(30);
																																																																													}
																																																																												else
																																																																													{	/* Object/nil.scm 195 */
																																																																														if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(59)))
																																																																															{	/* Object/nil.scm 195 */
																																																																																return
																																																																																	CNST_TABLE_REF
																																																																																	(31);
																																																																															}
																																																																														else
																																																																															{	/* Object/nil.scm 195 */
																																																																																if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(60)))
																																																																																	{	/* Object/nil.scm 217 */
																																																																																		obj_t
																																																																																			BgL_arg1746z00_1205;
																																																																																		{	/* Object/nil.scm 217 */
																																																																																			obj_t
																																																																																				BgL_arg1747z00_1206;
																																																																																			obj_t
																																																																																				BgL_arg1748z00_1207;
																																																																																			{	/* Object/nil.scm 217 */
																																																																																				obj_t
																																																																																					BgL_arg1749z00_1208;
																																																																																				{	/* Object/nil.scm 217 */
																																																																																					obj_t
																																																																																						BgL_arg1750z00_1209;
																																																																																					{	/* Object/nil.scm 217 */

																																																																																						{	/* Object/nil.scm 217 */

																																																																																							BgL_arg1750z00_1209
																																																																																								=
																																																																																								BGl_gensymz00zz__r4_symbols_6_4z00
																																																																																								(BFALSE);
																																																																																						}
																																																																																					}
																																																																																					BgL_arg1749z00_1208
																																																																																						=
																																																																																						MAKE_YOUNG_PAIR
																																																																																						(BgL_arg1750z00_1209,
																																																																																						BNIL);
																																																																																				}
																																																																																				BgL_arg1747z00_1206
																																																																																					=
																																																																																					MAKE_YOUNG_PAIR
																																																																																					(CNST_TABLE_REF
																																																																																					(18),
																																																																																					BgL_arg1749z00_1208);
																																																																																			}
																																																																																			{	/* Object/nil.scm 217 */
																																																																																				obj_t
																																																																																					BgL_arg1751z00_1211;
																																																																																				BgL_arg1751z00_1211
																																																																																					=
																																																																																					MAKE_YOUNG_PAIR
																																																																																					(BFALSE,
																																																																																					BNIL);
																																																																																				BgL_arg1748z00_1207
																																																																																					=
																																																																																					MAKE_YOUNG_PAIR
																																																																																					(BINT
																																																																																					(0L),
																																																																																					BgL_arg1751z00_1211);
																																																																																			}
																																																																																			BgL_arg1746z00_1205
																																																																																				=
																																																																																				MAKE_YOUNG_PAIR
																																																																																				(BgL_arg1747z00_1206,
																																																																																				BgL_arg1748z00_1207);
																																																																																		}
																																																																																		return
																																																																																			MAKE_YOUNG_PAIR
																																																																																			(CNST_TABLE_REF
																																																																																			(61),
																																																																																			BgL_arg1746z00_1205);
																																																																																	}
																																																																																else
																																																																																	{	/* Object/nil.scm 195 */
																																																																																		if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(62)))
																																																																																			{	/* Object/nil.scm 195 */
																																																																																				return
																																																																																					CNST_TABLE_REF
																																																																																					(32);
																																																																																			}
																																																																																		else
																																																																																			{	/* Object/nil.scm 195 */
																																																																																				if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(63)))
																																																																																					{	/* Object/nil.scm 195 */
																																																																																						return
																																																																																							CNST_TABLE_REF
																																																																																							(64);
																																																																																					}
																																																																																				else
																																																																																					{	/* Object/nil.scm 195 */
																																																																																						if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(65)))
																																																																																							{	/* Object/nil.scm 195 */
																																																																																								return
																																																																																									CNST_TABLE_REF
																																																																																									(66);
																																																																																							}
																																																																																						else
																																																																																							{	/* Object/nil.scm 195 */
																																																																																								if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(67)))
																																																																																									{	/* Object/nil.scm 195 */
																																																																																										return
																																																																																											CNST_TABLE_REF
																																																																																											(68);
																																																																																									}
																																																																																								else
																																																																																									{	/* Object/nil.scm 195 */
																																																																																										if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(69)))
																																																																																											{	/* Object/nil.scm 195 */
																																																																																												return
																																																																																													CNST_TABLE_REF
																																																																																													(70);
																																																																																											}
																																																																																										else
																																																																																											{	/* Object/nil.scm 195 */
																																																																																												if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(71)))
																																																																																													{	/* Object/nil.scm 195 */
																																																																																														return
																																																																																															CNST_TABLE_REF
																																																																																															(72);
																																																																																													}
																																																																																												else
																																																																																													{	/* Object/nil.scm 195 */
																																																																																														if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(73)))
																																																																																															{	/* Object/nil.scm 195 */
																																																																																																return
																																																																																																	CNST_TABLE_REF
																																																																																																	(74);
																																																																																															}
																																																																																														else
																																																																																															{	/* Object/nil.scm 195 */
																																																																																																if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(75)))
																																																																																																	{	/* Object/nil.scm 195 */
																																																																																																		return
																																																																																																			CNST_TABLE_REF
																																																																																																			(76);
																																																																																																	}
																																																																																																else
																																																																																																	{	/* Object/nil.scm 195 */
																																																																																																		if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(77)))
																																																																																																			{	/* Object/nil.scm 195 */
																																																																																																				return
																																																																																																					CNST_TABLE_REF
																																																																																																					(78);
																																																																																																			}
																																																																																																		else
																																																																																																			{	/* Object/nil.scm 195 */
																																																																																																				if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(79)))
																																																																																																					{	/* Object/nil.scm 195 */
																																																																																																						return
																																																																																																							CNST_TABLE_REF
																																																																																																							(80);
																																																																																																					}
																																																																																																				else
																																																																																																					{	/* Object/nil.scm 195 */
																																																																																																						if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(81)))
																																																																																																							{	/* Object/nil.scm 195 */
																																																																																																								return
																																																																																																									CNST_TABLE_REF
																																																																																																									(82);
																																																																																																							}
																																																																																																						else
																																																																																																							{	/* Object/nil.scm 195 */
																																																																																																								if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(83)))
																																																																																																									{	/* Object/nil.scm 195 */
																																																																																																										return
																																																																																																											CNST_TABLE_REF
																																																																																																											(84);
																																																																																																									}
																																																																																																								else
																																																																																																									{	/* Object/nil.scm 195 */
																																																																																																										if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(85)))
																																																																																																											{	/* Object/nil.scm 195 */
																																																																																																												return
																																																																																																													CNST_TABLE_REF
																																																																																																													(86);
																																																																																																											}
																																																																																																										else
																																																																																																											{	/* Object/nil.scm 195 */
																																																																																																												if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(87)))
																																																																																																													{	/* Object/nil.scm 195 */
																																																																																																														return
																																																																																																															CNST_TABLE_REF
																																																																																																															(88);
																																																																																																													}
																																																																																																												else
																																																																																																													{	/* Object/nil.scm 195 */
																																																																																																														if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(89)))
																																																																																																															{	/* Object/nil.scm 195 */
																																																																																																																return
																																																																																																																	CNST_TABLE_REF
																																																																																																																	(90);
																																																																																																															}
																																																																																																														else
																																																																																																															{	/* Object/nil.scm 195 */
																																																																																																																if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(91)))
																																																																																																																	{	/* Object/nil.scm 195 */
																																																																																																																		return
																																																																																																																			CNST_TABLE_REF
																																																																																																																			(92);
																																																																																																																	}
																																																																																																																else
																																																																																																																	{	/* Object/nil.scm 195 */
																																																																																																																		if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(93)))
																																																																																																																			{	/* Object/nil.scm 195 */
																																																																																																																				return
																																																																																																																					CNST_TABLE_REF
																																																																																																																					(94);
																																																																																																																			}
																																																																																																																		else
																																																																																																																			{	/* Object/nil.scm 195 */
																																																																																																																				if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(95)))
																																																																																																																					{	/* Object/nil.scm 195 */
																																																																																																																						return
																																																																																																																							CNST_TABLE_REF
																																																																																																																							(96);
																																																																																																																					}
																																																																																																																				else
																																																																																																																					{	/* Object/nil.scm 195 */
																																																																																																																						if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(97)))
																																																																																																																							{	/* Object/nil.scm 195 */
																																																																																																																								return
																																																																																																																									CNST_TABLE_REF
																																																																																																																									(98);
																																																																																																																							}
																																																																																																																						else
																																																																																																																							{	/* Object/nil.scm 195 */
																																																																																																																								if ((BgL_casezd2valuezd2_1179 == CNST_TABLE_REF(99)))
																																																																																																																									{	/* Object/nil.scm 195 */
																																																																																																																										return
																																																																																																																											CNST_TABLE_REF
																																																																																																																											(100);
																																																																																																																									}
																																																																																																																								else
																																																																																																																									{	/* Object/nil.scm 238 */
																																																																																																																										obj_t
																																																																																																																											BgL_arg1775z00_1232;
																																																																																																																										obj_t
																																																																																																																											BgL_arg1798z00_1233;
																																																																																																																										obj_t
																																																																																																																											BgL_arg1799z00_1234;
																																																																																																																										BgL_arg1775z00_1232
																																																																																																																											=
																																																																																																																											(
																																																																																																																											((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) (((BgL_slotz00_bglt) COBJECT(((BgL_slotz00_bglt) BgL_slotz00_17)))->BgL_classzd2ownerzd2))))->BgL_idz00);
																																																																																																																										{	/* Object/nil.scm 239 */
																																																																																																																											obj_t
																																																																																																																												BgL_arg1806z00_1236;
																																																																																																																											BgL_arg1806z00_1236
																																																																																																																												=
																																																																																																																												(
																																																																																																																												((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) BgL_typez00_16)))->BgL_idz00);
																																																																																																																											{	/* Object/nil.scm 239 */
																																																																																																																												obj_t
																																																																																																																													BgL_list1807z00_1237;
																																																																																																																												BgL_list1807z00_1237
																																																																																																																													=
																																																																																																																													MAKE_YOUNG_PAIR
																																																																																																																													(BgL_arg1806z00_1236,
																																																																																																																													BNIL);
																																																																																																																												BgL_arg1798z00_1233
																																																																																																																													=
																																																																																																																													BGl_formatz00zz__r4_output_6_10_3z00
																																																																																																																													(BGl_string1879z00zzobject_nilz00,
																																																																																																																													BgL_list1807z00_1237);
																																																																																																																											}
																																																																																																																										}
																																																																																																																										BgL_arg1799z00_1234
																																																																																																																											=
																																																																																																																											(
																																																																																																																											((BgL_slotz00_bglt) COBJECT(((BgL_slotz00_bglt) BgL_slotz00_17)))->BgL_idz00);
																																																																																																																										return
																																																																																																																											BGl_errorz00zz__errorz00
																																																																																																																											(BgL_arg1775z00_1232,
																																																																																																																											BgL_arg1798z00_1233,
																																																																																																																											BgL_arg1799z00_1234);
																																																																																																																									}
																																																																																																																							}
																																																																																																																					}
																																																																																																																			}
																																																																																																																	}
																																																																																																															}
																																																																																																													}
																																																																																																											}
																																																																																																									}
																																																																																																							}
																																																																																																					}
																																																																																																			}
																																																																																																	}
																																																																																															}
																																																																																													}
																																																																																											}
																																																																																									}
																																																																																							}
																																																																																					}
																																																																																			}
																																																																																	}
																																																																															}
																																																																													}
																																																																											}
																																																																									}
																																																																							}
																																																																					}
																																																																			}
																																																																	}
																																																															}
																																																													}
																																																											}
																																																									}
																																																							}
																																																					}
																																																			}
																																																	}
																																															}
																																													}
																																											}
																																									}
																																							}
																																					}
																																			}
																																	}
																															}
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* extern-type-nil */
	obj_t BGl_externzd2typezd2nilz00zzobject_nilz00(obj_t BgL_typez00_18,
		obj_t BgL_slotz00_19)
	{
		{	/* Object/nil.scm 245 */
			{	/* Object/nil.scm 247 */
				bool_t BgL_test1994z00_2714;

				if ((BgL_typez00_18 == BGl_za2intza2z00zztype_cachez00))
					{	/* Object/nil.scm 247 */
						BgL_test1994z00_2714 = ((bool_t) 1);
					}
				else
					{	/* Object/nil.scm 247 */
						BgL_test1994z00_2714 =
							(BgL_typez00_18 == BGl_za2longza2z00zztype_cachez00);
					}
				if (BgL_test1994z00_2714)
					{	/* Object/nil.scm 247 */
						return BINT(0L);
					}
				else
					{	/* Object/nil.scm 247 */
						if ((BgL_typez00_18 == BGl_za2boolza2z00zztype_cachez00))
							{	/* Object/nil.scm 248 */
								return BFALSE;
							}
						else
							{	/* Object/nil.scm 248 */
								if ((BgL_typez00_18 == BGl_za2realza2z00zztype_cachez00))
									{	/* Object/nil.scm 249 */
										return BGL_REAL_CNST(BGl_real1877z00zzobject_nilz00);
									}
								else
									{	/* Object/nil.scm 249 */
										if ((BgL_typez00_18 == BGl_za2elongza2z00zztype_cachez00))
											{	/* Object/nil.scm 250 */
												return CNST_TABLE_REF(24);
											}
										else
											{	/* Object/nil.scm 250 */
												if (
													(BgL_typez00_18 == BGl_za2llongza2z00zztype_cachez00))
													{	/* Object/nil.scm 251 */
														return CNST_TABLE_REF(23);
													}
												else
													{	/* Object/nil.scm 251 */
														if (
															(BgL_typez00_18 ==
																BGl_za2charza2z00zztype_cachez00))
															{	/* Object/nil.scm 252 */
																return BCHAR(((unsigned char) '_'));
															}
														else
															{	/* Object/nil.scm 252 */
																if (
																	(BgL_typez00_18 ==
																		BGl_za2stringza2z00zztype_cachez00))
																	{	/* Object/nil.scm 253 */
																		return BGl_string1878z00zzobject_nilz00;
																	}
																else
																	{	/* Object/nil.scm 254 */
																		bool_t BgL_test2002z00_2734;

																		{	/* Object/nil.scm 254 */
																			bool_t BgL_test2003z00_2735;

																			{	/* Object/nil.scm 254 */
																				obj_t BgL_classz00_1672;

																				BgL_classz00_1672 =
																					BGl_jclassz00zzobject_classz00;
																				if (BGL_OBJECTP(BgL_typez00_18))
																					{	/* Object/nil.scm 254 */
																						BgL_objectz00_bglt
																							BgL_arg1807z00_1674;
																						BgL_arg1807z00_1674 =
																							(BgL_objectz00_bglt)
																							(BgL_typez00_18);
																						if (BGL_CONDEXPAND_ISA_ARCH64())
																							{	/* Object/nil.scm 254 */
																								long BgL_idxz00_1680;

																								BgL_idxz00_1680 =
																									BGL_OBJECT_INHERITANCE_NUM
																									(BgL_arg1807z00_1674);
																								BgL_test2003z00_2735 =
																									(VECTOR_REF
																									(BGl_za2inheritancesza2z00zz__objectz00,
																										(BgL_idxz00_1680 + 2L)) ==
																									BgL_classz00_1672);
																							}
																						else
																							{	/* Object/nil.scm 254 */
																								bool_t BgL_res1871z00_1705;

																								{	/* Object/nil.scm 254 */
																									obj_t BgL_oclassz00_1688;

																									{	/* Object/nil.scm 254 */
																										obj_t BgL_arg1815z00_1696;
																										long BgL_arg1816z00_1697;

																										BgL_arg1815z00_1696 =
																											(BGl_za2classesza2z00zz__objectz00);
																										{	/* Object/nil.scm 254 */
																											long BgL_arg1817z00_1698;

																											BgL_arg1817z00_1698 =
																												BGL_OBJECT_CLASS_NUM
																												(BgL_arg1807z00_1674);
																											BgL_arg1816z00_1697 =
																												(BgL_arg1817z00_1698 -
																												OBJECT_TYPE);
																										}
																										BgL_oclassz00_1688 =
																											VECTOR_REF
																											(BgL_arg1815z00_1696,
																											BgL_arg1816z00_1697);
																									}
																									{	/* Object/nil.scm 254 */
																										bool_t
																											BgL__ortest_1115z00_1689;
																										BgL__ortest_1115z00_1689 =
																											(BgL_classz00_1672 ==
																											BgL_oclassz00_1688);
																										if (BgL__ortest_1115z00_1689)
																											{	/* Object/nil.scm 254 */
																												BgL_res1871z00_1705 =
																													BgL__ortest_1115z00_1689;
																											}
																										else
																											{	/* Object/nil.scm 254 */
																												long BgL_odepthz00_1690;

																												{	/* Object/nil.scm 254 */
																													obj_t
																														BgL_arg1804z00_1691;
																													BgL_arg1804z00_1691 =
																														(BgL_oclassz00_1688);
																													BgL_odepthz00_1690 =
																														BGL_CLASS_DEPTH
																														(BgL_arg1804z00_1691);
																												}
																												if (
																													(2L <
																														BgL_odepthz00_1690))
																													{	/* Object/nil.scm 254 */
																														obj_t
																															BgL_arg1802z00_1693;
																														{	/* Object/nil.scm 254 */
																															obj_t
																																BgL_arg1803z00_1694;
																															BgL_arg1803z00_1694
																																=
																																(BgL_oclassz00_1688);
																															BgL_arg1802z00_1693
																																=
																																BGL_CLASS_ANCESTORS_REF
																																(BgL_arg1803z00_1694,
																																2L);
																														}
																														BgL_res1871z00_1705
																															=
																															(BgL_arg1802z00_1693
																															==
																															BgL_classz00_1672);
																													}
																												else
																													{	/* Object/nil.scm 254 */
																														BgL_res1871z00_1705
																															= ((bool_t) 0);
																													}
																											}
																									}
																								}
																								BgL_test2003z00_2735 =
																									BgL_res1871z00_1705;
																							}
																					}
																				else
																					{	/* Object/nil.scm 254 */
																						BgL_test2003z00_2735 = ((bool_t) 0);
																					}
																			}
																			if (BgL_test2003z00_2735)
																				{	/* Object/nil.scm 254 */
																					BgL_test2002z00_2734 = ((bool_t) 1);
																				}
																			else
																				{	/* Object/nil.scm 254 */
																					obj_t BgL_classz00_1706;

																					BgL_classz00_1706 =
																						BGl_jarrayz00zzforeign_jtypez00;
																					if (BGL_OBJECTP(BgL_typez00_18))
																						{	/* Object/nil.scm 254 */
																							BgL_objectz00_bglt
																								BgL_arg1807z00_1708;
																							BgL_arg1807z00_1708 =
																								(BgL_objectz00_bglt)
																								(BgL_typez00_18);
																							if (BGL_CONDEXPAND_ISA_ARCH64())
																								{	/* Object/nil.scm 254 */
																									long BgL_idxz00_1714;

																									BgL_idxz00_1714 =
																										BGL_OBJECT_INHERITANCE_NUM
																										(BgL_arg1807z00_1708);
																									BgL_test2002z00_2734 =
																										(VECTOR_REF
																										(BGl_za2inheritancesza2z00zz__objectz00,
																											(BgL_idxz00_1714 + 2L)) ==
																										BgL_classz00_1706);
																								}
																							else
																								{	/* Object/nil.scm 254 */
																									bool_t BgL_res1872z00_1739;

																									{	/* Object/nil.scm 254 */
																										obj_t BgL_oclassz00_1722;

																										{	/* Object/nil.scm 254 */
																											obj_t BgL_arg1815z00_1730;
																											long BgL_arg1816z00_1731;

																											BgL_arg1815z00_1730 =
																												(BGl_za2classesza2z00zz__objectz00);
																											{	/* Object/nil.scm 254 */
																												long
																													BgL_arg1817z00_1732;
																												BgL_arg1817z00_1732 =
																													BGL_OBJECT_CLASS_NUM
																													(BgL_arg1807z00_1708);
																												BgL_arg1816z00_1731 =
																													(BgL_arg1817z00_1732 -
																													OBJECT_TYPE);
																											}
																											BgL_oclassz00_1722 =
																												VECTOR_REF
																												(BgL_arg1815z00_1730,
																												BgL_arg1816z00_1731);
																										}
																										{	/* Object/nil.scm 254 */
																											bool_t
																												BgL__ortest_1115z00_1723;
																											BgL__ortest_1115z00_1723 =
																												(BgL_classz00_1706 ==
																												BgL_oclassz00_1722);
																											if (BgL__ortest_1115z00_1723)
																												{	/* Object/nil.scm 254 */
																													BgL_res1872z00_1739 =
																														BgL__ortest_1115z00_1723;
																												}
																											else
																												{	/* Object/nil.scm 254 */
																													long
																														BgL_odepthz00_1724;
																													{	/* Object/nil.scm 254 */
																														obj_t
																															BgL_arg1804z00_1725;
																														BgL_arg1804z00_1725
																															=
																															(BgL_oclassz00_1722);
																														BgL_odepthz00_1724 =
																															BGL_CLASS_DEPTH
																															(BgL_arg1804z00_1725);
																													}
																													if (
																														(2L <
																															BgL_odepthz00_1724))
																														{	/* Object/nil.scm 254 */
																															obj_t
																																BgL_arg1802z00_1727;
																															{	/* Object/nil.scm 254 */
																																obj_t
																																	BgL_arg1803z00_1728;
																																BgL_arg1803z00_1728
																																	=
																																	(BgL_oclassz00_1722);
																																BgL_arg1802z00_1727
																																	=
																																	BGL_CLASS_ANCESTORS_REF
																																	(BgL_arg1803z00_1728,
																																	2L);
																															}
																															BgL_res1872z00_1739
																																=
																																(BgL_arg1802z00_1727
																																==
																																BgL_classz00_1706);
																														}
																													else
																														{	/* Object/nil.scm 254 */
																															BgL_res1872z00_1739
																																= ((bool_t) 0);
																														}
																												}
																										}
																									}
																									BgL_test2002z00_2734 =
																										BgL_res1872z00_1739;
																								}
																						}
																					else
																						{	/* Object/nil.scm 254 */
																							BgL_test2002z00_2734 =
																								((bool_t) 0);
																						}
																				}
																		}
																		if (BgL_test2002z00_2734)
																			{	/* Object/nil.scm 255 */
																				obj_t BgL_arg1812z00_1244;

																				BgL_arg1812z00_1244 =
																					(((BgL_typez00_bglt) COBJECT(
																							((BgL_typez00_bglt)
																								BgL_typez00_18)))->BgL_idz00);
																				return
																					BGl_makezd2privatezd2sexpz00zzast_privatez00
																					(CNST_TABLE_REF(101),
																					BgL_arg1812z00_1244, BNIL);
																			}
																		else
																			{	/* Object/nil.scm 257 */
																				obj_t BgL_casezd2valuezd2_1246;

																				BgL_casezd2valuezd2_1246 =
																					(((BgL_typez00_bglt) COBJECT(
																							((BgL_typez00_bglt)
																								BgL_typez00_18)))->BgL_idz00);
																				{	/* Object/nil.scm 257 */
																					bool_t BgL_test2012z00_2786;

																					{	/* Object/nil.scm 257 */
																						bool_t BgL__ortest_1082z00_1264;

																						BgL__ortest_1082z00_1264 =
																							(BgL_casezd2valuezd2_1246 ==
																							CNST_TABLE_REF(102));
																						if (BgL__ortest_1082z00_1264)
																							{	/* Object/nil.scm 257 */
																								BgL_test2012z00_2786 =
																									BgL__ortest_1082z00_1264;
																							}
																						else
																							{	/* Object/nil.scm 257 */
																								bool_t BgL__ortest_1083z00_1265;

																								BgL__ortest_1083z00_1265 =
																									(BgL_casezd2valuezd2_1246 ==
																									CNST_TABLE_REF(103));
																								if (BgL__ortest_1083z00_1265)
																									{	/* Object/nil.scm 257 */
																										BgL_test2012z00_2786 =
																											BgL__ortest_1083z00_1265;
																									}
																								else
																									{	/* Object/nil.scm 257 */
																										BgL_test2012z00_2786 =
																											(BgL_casezd2valuezd2_1246
																											== CNST_TABLE_REF(104));
																									}
																							}
																					}
																					if (BgL_test2012z00_2786)
																						{	/* Object/nil.scm 257 */
																							return BINT(0L);
																						}
																					else
																						{	/* Object/nil.scm 257 */
																							if (
																								(BgL_casezd2valuezd2_1246 ==
																									CNST_TABLE_REF(48)))
																								{	/* Object/nil.scm 257 */
																									return BFALSE;
																								}
																							else
																								{	/* Object/nil.scm 257 */
																									bool_t BgL_test2016z00_2799;

																									{	/* Object/nil.scm 257 */
																										bool_t
																											BgL__ortest_1084z00_1263;
																										BgL__ortest_1084z00_1263 =
																											(BgL_casezd2valuezd2_1246
																											== CNST_TABLE_REF(105));
																										if (BgL__ortest_1084z00_1263)
																											{	/* Object/nil.scm 257 */
																												BgL_test2016z00_2799 =
																													BgL__ortest_1084z00_1263;
																											}
																										else
																											{	/* Object/nil.scm 257 */
																												BgL_test2016z00_2799 =
																													(BgL_casezd2valuezd2_1246
																													==
																													CNST_TABLE_REF(106));
																											}
																									}
																									if (BgL_test2016z00_2799)
																										{	/* Object/nil.scm 257 */
																											return
																												BGL_REAL_CNST
																												(BGl_real1877z00zzobject_nilz00);
																										}
																									else
																										{	/* Object/nil.scm 257 */
																											if (
																												(BgL_casezd2valuezd2_1246
																													==
																													CNST_TABLE_REF(107)))
																												{	/* Object/nil.scm 257 */
																													return
																														BCHAR(((unsigned
																																char) '_'));
																												}
																											else
																												{	/* Object/nil.scm 257 */
																													if (
																														(BgL_casezd2valuezd2_1246
																															==
																															CNST_TABLE_REF
																															(108)))
																														{	/* Object/nil.scm 257 */
																															return
																																BGl_string1878z00zzobject_nilz00;
																														}
																													else
																														{	/* Object/nil.scm 264 */
																															bool_t
																																BgL_test2020z00_2812;
																															{	/* Object/nil.scm 264 */
																																obj_t
																																	BgL_arg1834z00_1262;
																																BgL_arg1834z00_1262
																																	=
																																	BGl_thezd2backendzd2zzbackend_backendz00
																																	();
																																BgL_test2020z00_2812
																																	=
																																	(((BgL_backendz00_bglt) COBJECT(((BgL_backendz00_bglt) BgL_arg1834z00_1262)))->BgL_pragmazd2supportzd2);
																															}
																															if (BgL_test2020z00_2812)
																																{	/* Object/nil.scm 265 */
																																	obj_t
																																		BgL_arg1822z00_1257;
																																	obj_t
																																		BgL_arg1823z00_1258;
																																	{	/* Object/nil.scm 265 */
																																		obj_t
																																			BgL_arg1831z00_1259;
																																		BgL_arg1831z00_1259
																																			=
																																			(((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) BgL_typez00_18)))->BgL_idz00);
																																		BgL_arg1822z00_1257
																																			=
																																			BGl_makezd2typedzd2identz00zzast_identz00
																																			(CNST_TABLE_REF
																																			(109),
																																			BgL_arg1831z00_1259);
																																	}
																																	BgL_arg1823z00_1258
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_string1880z00zzobject_nilz00,
																																		BNIL);
																																	return
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1822z00_1257,
																																		BgL_arg1823z00_1258);
																																}
																															else
																																{	/* Object/nil.scm 266 */
																																	obj_t
																																		BgL_arg1832z00_1260;
																																	BgL_arg1832z00_1260
																																		=
																																		(((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) BgL_typez00_18)))->BgL_idz00);
																																	return
																																		BGl_makezd2privatezd2sexpz00zzast_privatez00
																																		(CNST_TABLE_REF
																																		(101),
																																		BgL_arg1832z00_1260,
																																		BNIL);
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																			}
																	}
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzobject_nilz00(void)
	{
		{	/* Object/nil.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzobject_nilz00(void)
	{
		{	/* Object/nil.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzobject_nilz00(void)
	{
		{	/* Object/nil.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzobject_nilz00(void)
	{
		{	/* Object/nil.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1881z00zzobject_nilz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1881z00zzobject_nilz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1881z00zzobject_nilz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1881z00zzobject_nilz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1881z00zzobject_nilz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1881z00zzobject_nilz00));
			BGl_modulezd2initializa7ationz75zztype_toolsz00(453414928L,
				BSTRING_TO_STRING(BGl_string1881z00zzobject_nilz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1881z00zzobject_nilz00));
			BGl_modulezd2initializa7ationz75zztvector_tvectorz00(501518119L,
				BSTRING_TO_STRING(BGl_string1881z00zzobject_nilz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1881z00zzobject_nilz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1881z00zzobject_nilz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1881z00zzobject_nilz00));
			BGl_modulezd2initializa7ationz75zzast_privatez00(135263837L,
				BSTRING_TO_STRING(BGl_string1881z00zzobject_nilz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1881z00zzobject_nilz00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string1881z00zzobject_nilz00));
			BGl_modulezd2initializa7ationz75zzobject_toolsz00(196511171L,
				BSTRING_TO_STRING(BGl_string1881z00zzobject_nilz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1881z00zzobject_nilz00));
			BGl_modulezd2initializa7ationz75zzmodule_impusez00(478324304L,
				BSTRING_TO_STRING(BGl_string1881z00zzobject_nilz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1881z00zzobject_nilz00));
			return
				BGl_modulezd2initializa7ationz75zzforeign_jtypez00(287572863L,
				BSTRING_TO_STRING(BGl_string1881z00zzobject_nilz00));
		}

	}

#ifdef __cplusplus
}
#endif
