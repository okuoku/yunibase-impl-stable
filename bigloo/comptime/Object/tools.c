/*===========================================================================*/
/*   (Object/tools.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Object/tools.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_OBJECT_TOOLS_TYPE_DEFINITIONS
#define BGL_OBJECT_TOOLS_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_tclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		struct BgL_globalz00_bgl *BgL_holderz00;
		obj_t BgL_wideningz00;
		long BgL_depthz00;
		bool_t BgL_finalzf3zf3;
		obj_t BgL_constructorz00;
		obj_t BgL_virtualzd2slotszd2numberz00;
		bool_t BgL_abstractzf3zf3;
		obj_t BgL_widezd2typezd2;
		obj_t BgL_subclassesz00;
	}                *BgL_tclassz00_bglt;

	typedef struct BgL_slotz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_srcz00;
		obj_t BgL_classzd2ownerzd2;
		long BgL_indexz00;
		obj_t BgL_typez00;
		bool_t BgL_readzd2onlyzf3z21;
		obj_t BgL_defaultzd2valuezd2;
		obj_t BgL_virtualzd2numzd2;
		bool_t BgL_virtualzd2overridezd2;
		obj_t BgL_getterz00;
		obj_t BgL_setterz00;
		obj_t BgL_userzd2infozd2;
	}              *BgL_slotz00_bglt;


#endif													// BGL_OBJECT_TOOLS_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62objzd2ze3classzd2idz81zzobject_toolsz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzobject_toolsz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_tclassz00zzobject_classz00;
	BGL_IMPORT obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_z62makezd2directzd2setz12z70zzobject_toolsz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzobject_toolsz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_classzd2ze3superzd2idze3zzobject_toolsz00(obj_t,
		obj_t);
	static obj_t BGl_z62classzd2ze3superzd2idz81zzobject_toolsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_classzd2ze3objzd2idze3zzobject_toolsz00(obj_t);
	static obj_t BGl_objectzd2initzd2zzobject_toolsz00(void);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t
		BGl_findzd2classzd2slotz00zzobject_toolsz00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62findzd2classzd2slotz62zzobject_toolsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62classzf3zd2idz43zzobject_toolsz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzobject_toolsz00(void);
	BGL_EXPORTED_DECL obj_t BGl_objzd2ze3classzd2idze3zzobject_toolsz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_classzf3zd2idz21zzobject_toolsz00(obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzobject_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_privatez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	static obj_t BGl_z62makezd2classzd2refz62zzobject_toolsz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_makezd2privatezd2sexpz00zzast_privatez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_superzd2ze3classzd2idze3zzobject_toolsz00(obj_t,
		obj_t);
	static obj_t BGl_z62superzd2ze3classzd2idz81zzobject_toolsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zzobject_toolsz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzobject_toolsz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzobject_toolsz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzobject_toolsz00(void);
	static obj_t BGl_makezd2directzd2refz00zzobject_toolsz00(obj_t,
		BgL_slotz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2classzd2refz00zzobject_toolsz00(BgL_typez00_bglt,
		BgL_slotz00_bglt, obj_t);
	static obj_t BGl_z62classzd2ze3objzd2idz81zzobject_toolsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2classzd2setz12z12zzobject_toolsz00(BgL_typez00_bglt,
		BgL_slotz00_bglt, obj_t, obj_t);
	static obj_t BGl_z62makezd2classzd2setz12z70zzobject_toolsz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2directzd2setz12z12zzobject_toolsz00(BgL_typez00_bglt,
		BgL_slotz00_bglt, obj_t, obj_t);
	static obj_t __cnst[7];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_objzd2ze3classzd2idzd2envz31zzobject_toolsz00,
		BgL_bgl_za762objza7d2za7e3clas1664za7,
		BGl_z62objzd2ze3classzd2idz81zzobject_toolsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2classzd2refzd2envzd2zzobject_toolsz00,
		BgL_bgl_za762makeza7d2classza71665za7,
		BGl_z62makezd2classzd2refz62zzobject_toolsz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_classzf3zd2idzd2envzf3zzobject_toolsz00,
		BgL_bgl_za762classza7f3za7d2id1666za7,
		BGl_z62classzf3zd2idz43zzobject_toolsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2classzd2setz12zd2envzc0zzobject_toolsz00,
		BgL_bgl_za762makeza7d2classza71667za7,
		BGl_z62makezd2classzd2setz12z70zzobject_toolsz00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string1658z00zzobject_toolsz00,
		BgL_bgl_string1658za700za7za7o1668za7, "(((~a)COBJECT($1))->~a)", 23);
	      DEFINE_STRING(BGl_string1659z00zzobject_toolsz00,
		BgL_bgl_string1659za700za7za7o1669za7,
		"((((~a)COBJECT($1))->~a)=((~a)$2),BUNSPEC)", 42);
	      DEFINE_STRING(BGl_string1660z00zzobject_toolsz00,
		BgL_bgl_string1660za700za7za7o1670za7, "object_tools", 12);
	      DEFINE_STRING(BGl_string1661z00zzobject_toolsz00,
		BgL_bgl_string1661za700za7za7o1671za7,
		"setfield getfield object-widening -> ? obj-> ->obj ", 51);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classzd2ze3superzd2idzd2envz31zzobject_toolsz00,
		BgL_bgl_za762classza7d2za7e3su1672za7,
		BGl_z62classzd2ze3superzd2idz81zzobject_toolsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_findzd2classzd2slotzd2envzd2zzobject_toolsz00,
		BgL_bgl_za762findza7d2classza71673za7,
		BGl_z62findzd2classzd2slotz62zzobject_toolsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_superzd2ze3classzd2idzd2envz31zzobject_toolsz00,
		BgL_bgl_za762superza7d2za7e3cl1674za7,
		BGl_z62superzd2ze3classzd2idz81zzobject_toolsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classzd2ze3objzd2idzd2envz31zzobject_toolsz00,
		BgL_bgl_za762classza7d2za7e3ob1675za7,
		BGl_z62classzd2ze3objzd2idz81zzobject_toolsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2directzd2setz12zd2envzc0zzobject_toolsz00,
		BgL_bgl_za762makeza7d2direct1676z00,
		BGl_z62makezd2directzd2setz12z70zzobject_toolsz00, 0L, BUNSPEC, 4);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzobject_toolsz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzobject_toolsz00(long
		BgL_checksumz00_2180, char *BgL_fromz00_2181)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzobject_toolsz00))
				{
					BGl_requirezd2initializa7ationz75zzobject_toolsz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzobject_toolsz00();
					BGl_libraryzd2moduleszd2initz00zzobject_toolsz00();
					BGl_cnstzd2initzd2zzobject_toolsz00();
					BGl_importedzd2moduleszd2initz00zzobject_toolsz00();
					return BGl_methodzd2initzd2zzobject_toolsz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzobject_toolsz00(void)
	{
		{	/* Object/tools.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "object_tools");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "object_tools");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"object_tools");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "object_tools");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "object_tools");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"object_tools");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"object_tools");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"object_tools");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "object_tools");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"object_tools");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzobject_toolsz00(void)
	{
		{	/* Object/tools.scm 15 */
			{	/* Object/tools.scm 15 */
				obj_t BgL_cportz00_2169;

				{	/* Object/tools.scm 15 */
					obj_t BgL_stringz00_2176;

					BgL_stringz00_2176 = BGl_string1661z00zzobject_toolsz00;
					{	/* Object/tools.scm 15 */
						obj_t BgL_startz00_2177;

						BgL_startz00_2177 = BINT(0L);
						{	/* Object/tools.scm 15 */
							obj_t BgL_endz00_2178;

							BgL_endz00_2178 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2176)));
							{	/* Object/tools.scm 15 */

								BgL_cportz00_2169 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2176, BgL_startz00_2177, BgL_endz00_2178);
				}}}}
				{
					long BgL_iz00_2170;

					BgL_iz00_2170 = 6L;
				BgL_loopz00_2171:
					if ((BgL_iz00_2170 == -1L))
						{	/* Object/tools.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Object/tools.scm 15 */
							{	/* Object/tools.scm 15 */
								obj_t BgL_arg1663z00_2172;

								{	/* Object/tools.scm 15 */

									{	/* Object/tools.scm 15 */
										obj_t BgL_locationz00_2174;

										BgL_locationz00_2174 = BBOOL(((bool_t) 0));
										{	/* Object/tools.scm 15 */

											BgL_arg1663z00_2172 =
												BGl_readz00zz__readerz00(BgL_cportz00_2169,
												BgL_locationz00_2174);
										}
									}
								}
								{	/* Object/tools.scm 15 */
									int BgL_tmpz00_2209;

									BgL_tmpz00_2209 = (int) (BgL_iz00_2170);
									CNST_TABLE_SET(BgL_tmpz00_2209, BgL_arg1663z00_2172);
							}}
							{	/* Object/tools.scm 15 */
								int BgL_auxz00_2175;

								BgL_auxz00_2175 = (int) ((BgL_iz00_2170 - 1L));
								{
									long BgL_iz00_2214;

									BgL_iz00_2214 = (long) (BgL_auxz00_2175);
									BgL_iz00_2170 = BgL_iz00_2214;
									goto BgL_loopz00_2171;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzobject_toolsz00(void)
	{
		{	/* Object/tools.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* class->obj-id */
	BGL_EXPORTED_DEF obj_t BGl_classzd2ze3objzd2idze3zzobject_toolsz00(obj_t
		BgL_idz00_3)
	{
		{	/* Object/tools.scm 43 */
			{	/* Object/tools.scm 44 */
				obj_t BgL_arg1249z00_1730;

				{	/* Object/tools.scm 44 */
					obj_t BgL_arg1252z00_1731;
					obj_t BgL_arg1268z00_1732;

					{	/* Object/tools.scm 44 */
						obj_t BgL_arg1455z00_1981;

						BgL_arg1455z00_1981 = SYMBOL_TO_STRING(BgL_idz00_3);
						BgL_arg1252z00_1731 =
							BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_1981);
					}
					{	/* Object/tools.scm 44 */
						obj_t BgL_symbolz00_1982;

						BgL_symbolz00_1982 = CNST_TABLE_REF(0);
						{	/* Object/tools.scm 44 */
							obj_t BgL_arg1455z00_1983;

							BgL_arg1455z00_1983 = SYMBOL_TO_STRING(BgL_symbolz00_1982);
							BgL_arg1268z00_1732 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_1983);
						}
					}
					BgL_arg1249z00_1730 =
						string_append(BgL_arg1252z00_1731, BgL_arg1268z00_1732);
				}
				return bstring_to_symbol(BgL_arg1249z00_1730);
			}
		}

	}



/* &class->obj-id */
	obj_t BGl_z62classzd2ze3objzd2idz81zzobject_toolsz00(obj_t BgL_envz00_2140,
		obj_t BgL_idz00_2141)
	{
		{	/* Object/tools.scm 43 */
			return BGl_classzd2ze3objzd2idze3zzobject_toolsz00(BgL_idz00_2141);
		}

	}



/* obj->class-id */
	BGL_EXPORTED_DEF obj_t BGl_objzd2ze3classzd2idze3zzobject_toolsz00(obj_t
		BgL_idz00_4)
	{
		{	/* Object/tools.scm 49 */
			{	/* Object/tools.scm 50 */
				obj_t BgL_arg1272z00_1733;

				{	/* Object/tools.scm 50 */
					obj_t BgL_arg1284z00_1734;
					obj_t BgL_arg1304z00_1735;

					{	/* Object/tools.scm 50 */
						obj_t BgL_symbolz00_1985;

						BgL_symbolz00_1985 = CNST_TABLE_REF(1);
						{	/* Object/tools.scm 50 */
							obj_t BgL_arg1455z00_1986;

							BgL_arg1455z00_1986 = SYMBOL_TO_STRING(BgL_symbolz00_1985);
							BgL_arg1284z00_1734 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_1986);
						}
					}
					{	/* Object/tools.scm 50 */
						obj_t BgL_arg1455z00_1988;

						BgL_arg1455z00_1988 = SYMBOL_TO_STRING(BgL_idz00_4);
						BgL_arg1304z00_1735 =
							BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_1988);
					}
					BgL_arg1272z00_1733 =
						string_append(BgL_arg1284z00_1734, BgL_arg1304z00_1735);
				}
				return bstring_to_symbol(BgL_arg1272z00_1733);
			}
		}

	}



/* &obj->class-id */
	obj_t BGl_z62objzd2ze3classzd2idz81zzobject_toolsz00(obj_t BgL_envz00_2142,
		obj_t BgL_idz00_2143)
	{
		{	/* Object/tools.scm 49 */
			return BGl_objzd2ze3classzd2idze3zzobject_toolsz00(BgL_idz00_2143);
		}

	}



/* class?-id */
	BGL_EXPORTED_DEF obj_t BGl_classzf3zd2idz21zzobject_toolsz00(obj_t
		BgL_idz00_5)
	{
		{	/* Object/tools.scm 55 */
			{	/* Object/tools.scm 56 */
				obj_t BgL_arg1305z00_1736;

				{	/* Object/tools.scm 56 */
					obj_t BgL_arg1306z00_1737;
					obj_t BgL_arg1307z00_1738;

					{	/* Object/tools.scm 56 */
						obj_t BgL_arg1455z00_1991;

						BgL_arg1455z00_1991 = SYMBOL_TO_STRING(BgL_idz00_5);
						BgL_arg1306z00_1737 =
							BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_1991);
					}
					{	/* Object/tools.scm 56 */
						obj_t BgL_symbolz00_1992;

						BgL_symbolz00_1992 = CNST_TABLE_REF(2);
						{	/* Object/tools.scm 56 */
							obj_t BgL_arg1455z00_1993;

							BgL_arg1455z00_1993 = SYMBOL_TO_STRING(BgL_symbolz00_1992);
							BgL_arg1307z00_1738 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_1993);
						}
					}
					BgL_arg1305z00_1736 =
						string_append(BgL_arg1306z00_1737, BgL_arg1307z00_1738);
				}
				return bstring_to_symbol(BgL_arg1305z00_1736);
			}
		}

	}



/* &class?-id */
	obj_t BGl_z62classzf3zd2idz43zzobject_toolsz00(obj_t BgL_envz00_2144,
		obj_t BgL_idz00_2145)
	{
		{	/* Object/tools.scm 55 */
			return BGl_classzf3zd2idz21zzobject_toolsz00(BgL_idz00_2145);
		}

	}



/* class->super-id */
	BGL_EXPORTED_DEF obj_t BGl_classzd2ze3superzd2idze3zzobject_toolsz00(obj_t
		BgL_classz00_6, obj_t BgL_superz00_7)
	{
		{	/* Object/tools.scm 61 */
			{	/* Object/tools.scm 62 */
				obj_t BgL_list1308z00_1995;

				{	/* Object/tools.scm 62 */
					obj_t BgL_arg1310z00_1996;

					{	/* Object/tools.scm 62 */
						obj_t BgL_arg1311z00_1997;

						BgL_arg1311z00_1997 = MAKE_YOUNG_PAIR(BgL_superz00_7, BNIL);
						BgL_arg1310z00_1996 =
							MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BgL_arg1311z00_1997);
					}
					BgL_list1308z00_1995 =
						MAKE_YOUNG_PAIR(BgL_classz00_6, BgL_arg1310z00_1996);
				}
				return
					BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(BgL_list1308z00_1995);
			}
		}

	}



/* &class->super-id */
	obj_t BGl_z62classzd2ze3superzd2idz81zzobject_toolsz00(obj_t BgL_envz00_2146,
		obj_t BgL_classz00_2147, obj_t BgL_superz00_2148)
	{
		{	/* Object/tools.scm 61 */
			return
				BGl_classzd2ze3superzd2idze3zzobject_toolsz00(BgL_classz00_2147,
				BgL_superz00_2148);
		}

	}



/* super->class-id */
	BGL_EXPORTED_DEF obj_t BGl_superzd2ze3classzd2idze3zzobject_toolsz00(obj_t
		BgL_superz00_8, obj_t BgL_classz00_9)
	{
		{	/* Object/tools.scm 67 */
			{	/* Object/tools.scm 68 */
				obj_t BgL_list1312z00_1998;

				{	/* Object/tools.scm 68 */
					obj_t BgL_arg1314z00_1999;

					{	/* Object/tools.scm 68 */
						obj_t BgL_arg1315z00_2000;

						BgL_arg1315z00_2000 = MAKE_YOUNG_PAIR(BgL_classz00_9, BNIL);
						BgL_arg1314z00_1999 =
							MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BgL_arg1315z00_2000);
					}
					BgL_list1312z00_1998 =
						MAKE_YOUNG_PAIR(BgL_superz00_8, BgL_arg1314z00_1999);
				}
				return
					BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(BgL_list1312z00_1998);
			}
		}

	}



/* &super->class-id */
	obj_t BGl_z62superzd2ze3classzd2idz81zzobject_toolsz00(obj_t BgL_envz00_2149,
		obj_t BgL_superz00_2150, obj_t BgL_classz00_2151)
	{
		{	/* Object/tools.scm 67 */
			return
				BGl_superzd2ze3classzd2idze3zzobject_toolsz00(BgL_superz00_2150,
				BgL_classz00_2151);
		}

	}



/* make-class-ref */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2classzd2refz00zzobject_toolsz00(BgL_typez00_bglt BgL_typez00_10,
		BgL_slotz00_bglt BgL_slotz00_11, obj_t BgL_objz00_12)
	{
		{	/* Object/tools.scm 73 */
			{	/* Object/tools.scm 74 */
				obj_t BgL_klassz00_1745;

				BgL_klassz00_1745 =
					(((BgL_slotz00_bglt) COBJECT(BgL_slotz00_11))->BgL_classzd2ownerzd2);
				{	/* Object/tools.scm 74 */
					obj_t BgL_wideningz00_1746;

					{	/* Object/tools.scm 75 */
						bool_t BgL_test1681z00_2254;

						{	/* Object/tools.scm 75 */
							obj_t BgL_classz00_2002;

							BgL_classz00_2002 = BGl_tclassz00zzobject_classz00;
							if (BGL_OBJECTP(BgL_klassz00_1745))
								{	/* Object/tools.scm 75 */
									BgL_objectz00_bglt BgL_arg1807z00_2004;

									BgL_arg1807z00_2004 =
										(BgL_objectz00_bglt) (BgL_klassz00_1745);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Object/tools.scm 75 */
											long BgL_idxz00_2010;

											BgL_idxz00_2010 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2004);
											BgL_test1681z00_2254 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2010 + 2L)) == BgL_classz00_2002);
										}
									else
										{	/* Object/tools.scm 75 */
											bool_t BgL_res1655z00_2035;

											{	/* Object/tools.scm 75 */
												obj_t BgL_oclassz00_2018;

												{	/* Object/tools.scm 75 */
													obj_t BgL_arg1815z00_2026;
													long BgL_arg1816z00_2027;

													BgL_arg1815z00_2026 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Object/tools.scm 75 */
														long BgL_arg1817z00_2028;

														BgL_arg1817z00_2028 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2004);
														BgL_arg1816z00_2027 =
															(BgL_arg1817z00_2028 - OBJECT_TYPE);
													}
													BgL_oclassz00_2018 =
														VECTOR_REF(BgL_arg1815z00_2026,
														BgL_arg1816z00_2027);
												}
												{	/* Object/tools.scm 75 */
													bool_t BgL__ortest_1115z00_2019;

													BgL__ortest_1115z00_2019 =
														(BgL_classz00_2002 == BgL_oclassz00_2018);
													if (BgL__ortest_1115z00_2019)
														{	/* Object/tools.scm 75 */
															BgL_res1655z00_2035 = BgL__ortest_1115z00_2019;
														}
													else
														{	/* Object/tools.scm 75 */
															long BgL_odepthz00_2020;

															{	/* Object/tools.scm 75 */
																obj_t BgL_arg1804z00_2021;

																BgL_arg1804z00_2021 = (BgL_oclassz00_2018);
																BgL_odepthz00_2020 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2021);
															}
															if ((2L < BgL_odepthz00_2020))
																{	/* Object/tools.scm 75 */
																	obj_t BgL_arg1802z00_2023;

																	{	/* Object/tools.scm 75 */
																		obj_t BgL_arg1803z00_2024;

																		BgL_arg1803z00_2024 = (BgL_oclassz00_2018);
																		BgL_arg1802z00_2023 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2024, 2L);
																	}
																	BgL_res1655z00_2035 =
																		(BgL_arg1802z00_2023 == BgL_classz00_2002);
																}
															else
																{	/* Object/tools.scm 75 */
																	BgL_res1655z00_2035 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test1681z00_2254 = BgL_res1655z00_2035;
										}
								}
							else
								{	/* Object/tools.scm 75 */
									BgL_test1681z00_2254 = ((bool_t) 0);
								}
						}
						if (BgL_test1681z00_2254)
							{
								BgL_tclassz00_bglt BgL_auxz00_2277;

								{
									obj_t BgL_auxz00_2278;

									{	/* Object/tools.scm 75 */
										BgL_objectz00_bglt BgL_tmpz00_2279;

										BgL_tmpz00_2279 =
											((BgL_objectz00_bglt)
											((BgL_typez00_bglt) BgL_klassz00_1745));
										BgL_auxz00_2278 = BGL_OBJECT_WIDENING(BgL_tmpz00_2279);
									}
									BgL_auxz00_2277 = ((BgL_tclassz00_bglt) BgL_auxz00_2278);
								}
								BgL_wideningz00_1746 =
									(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_2277))->
									BgL_wideningz00);
							}
						else
							{	/* Object/tools.scm 75 */
								BgL_wideningz00_1746 = BFALSE;
							}
					}
					{	/* Object/tools.scm 75 */

						if (CBOOL(BgL_wideningz00_1746))
							{	/* Object/tools.scm 78 */
								obj_t BgL_arg1316z00_1747;
								obj_t BgL_arg1317z00_1748;

								{
									BgL_tclassz00_bglt BgL_auxz00_2287;

									{
										obj_t BgL_auxz00_2288;

										{	/* Object/tools.scm 78 */
											BgL_objectz00_bglt BgL_tmpz00_2289;

											BgL_tmpz00_2289 =
												((BgL_objectz00_bglt)
												((BgL_typez00_bglt) BgL_typez00_10));
											BgL_auxz00_2288 = BGL_OBJECT_WIDENING(BgL_tmpz00_2289);
										}
										BgL_auxz00_2287 = ((BgL_tclassz00_bglt) BgL_auxz00_2288);
									}
									BgL_arg1316z00_1747 =
										(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_2287))->
										BgL_widezd2typezd2);
								}
								{	/* Object/tools.scm 78 */
									obj_t BgL_arg1318z00_1749;

									BgL_arg1318z00_1749 = MAKE_YOUNG_PAIR(BgL_objz00_12, BNIL);
									BgL_arg1317z00_1748 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(4), BgL_arg1318z00_1749);
								}
								return
									BGl_makezd2directzd2refz00zzobject_toolsz00
									(BgL_arg1316z00_1747, BgL_slotz00_11, BgL_arg1317z00_1748);
							}
						else
							{	/* Object/tools.scm 76 */
								return
									BGl_makezd2directzd2refz00zzobject_toolsz00(
									((obj_t) BgL_typez00_10), BgL_slotz00_11, BgL_objz00_12);
							}
					}
				}
			}
		}

	}



/* &make-class-ref */
	obj_t BGl_z62makezd2classzd2refz62zzobject_toolsz00(obj_t BgL_envz00_2152,
		obj_t BgL_typez00_2153, obj_t BgL_slotz00_2154, obj_t BgL_objz00_2155)
	{
		{	/* Object/tools.scm 73 */
			return
				BGl_makezd2classzd2refz00zzobject_toolsz00(
				((BgL_typez00_bglt) BgL_typez00_2153),
				((BgL_slotz00_bglt) BgL_slotz00_2154), BgL_objz00_2155);
		}

	}



/* make-class-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2classzd2setz12z12zzobject_toolsz00(BgL_typez00_bglt
		BgL_typez00_13, BgL_slotz00_bglt BgL_slotz00_14, obj_t BgL_objz00_15,
		obj_t BgL_valz00_16)
	{
		{	/* Object/tools.scm 83 */
			{	/* Object/tools.scm 84 */
				obj_t BgL_klassz00_1751;

				BgL_klassz00_1751 =
					(((BgL_slotz00_bglt) COBJECT(BgL_slotz00_14))->BgL_classzd2ownerzd2);
				{	/* Object/tools.scm 84 */
					obj_t BgL_wideningz00_1752;

					{	/* Object/tools.scm 85 */
						bool_t BgL_test1687z00_2305;

						{	/* Object/tools.scm 85 */
							obj_t BgL_classz00_2041;

							BgL_classz00_2041 = BGl_tclassz00zzobject_classz00;
							if (BGL_OBJECTP(BgL_klassz00_1751))
								{	/* Object/tools.scm 85 */
									BgL_objectz00_bglt BgL_arg1807z00_2043;

									BgL_arg1807z00_2043 =
										(BgL_objectz00_bglt) (BgL_klassz00_1751);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Object/tools.scm 85 */
											long BgL_idxz00_2049;

											BgL_idxz00_2049 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2043);
											BgL_test1687z00_2305 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2049 + 2L)) == BgL_classz00_2041);
										}
									else
										{	/* Object/tools.scm 85 */
											bool_t BgL_res1656z00_2074;

											{	/* Object/tools.scm 85 */
												obj_t BgL_oclassz00_2057;

												{	/* Object/tools.scm 85 */
													obj_t BgL_arg1815z00_2065;
													long BgL_arg1816z00_2066;

													BgL_arg1815z00_2065 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Object/tools.scm 85 */
														long BgL_arg1817z00_2067;

														BgL_arg1817z00_2067 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2043);
														BgL_arg1816z00_2066 =
															(BgL_arg1817z00_2067 - OBJECT_TYPE);
													}
													BgL_oclassz00_2057 =
														VECTOR_REF(BgL_arg1815z00_2065,
														BgL_arg1816z00_2066);
												}
												{	/* Object/tools.scm 85 */
													bool_t BgL__ortest_1115z00_2058;

													BgL__ortest_1115z00_2058 =
														(BgL_classz00_2041 == BgL_oclassz00_2057);
													if (BgL__ortest_1115z00_2058)
														{	/* Object/tools.scm 85 */
															BgL_res1656z00_2074 = BgL__ortest_1115z00_2058;
														}
													else
														{	/* Object/tools.scm 85 */
															long BgL_odepthz00_2059;

															{	/* Object/tools.scm 85 */
																obj_t BgL_arg1804z00_2060;

																BgL_arg1804z00_2060 = (BgL_oclassz00_2057);
																BgL_odepthz00_2059 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2060);
															}
															if ((2L < BgL_odepthz00_2059))
																{	/* Object/tools.scm 85 */
																	obj_t BgL_arg1802z00_2062;

																	{	/* Object/tools.scm 85 */
																		obj_t BgL_arg1803z00_2063;

																		BgL_arg1803z00_2063 = (BgL_oclassz00_2057);
																		BgL_arg1802z00_2062 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2063, 2L);
																	}
																	BgL_res1656z00_2074 =
																		(BgL_arg1802z00_2062 == BgL_classz00_2041);
																}
															else
																{	/* Object/tools.scm 85 */
																	BgL_res1656z00_2074 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test1687z00_2305 = BgL_res1656z00_2074;
										}
								}
							else
								{	/* Object/tools.scm 85 */
									BgL_test1687z00_2305 = ((bool_t) 0);
								}
						}
						if (BgL_test1687z00_2305)
							{
								BgL_tclassz00_bglt BgL_auxz00_2328;

								{
									obj_t BgL_auxz00_2329;

									{	/* Object/tools.scm 85 */
										BgL_objectz00_bglt BgL_tmpz00_2330;

										BgL_tmpz00_2330 =
											((BgL_objectz00_bglt)
											((BgL_typez00_bglt) BgL_klassz00_1751));
										BgL_auxz00_2329 = BGL_OBJECT_WIDENING(BgL_tmpz00_2330);
									}
									BgL_auxz00_2328 = ((BgL_tclassz00_bglt) BgL_auxz00_2329);
								}
								BgL_wideningz00_1752 =
									(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_2328))->
									BgL_wideningz00);
							}
						else
							{	/* Object/tools.scm 85 */
								BgL_wideningz00_1752 = BFALSE;
							}
					}
					{	/* Object/tools.scm 85 */

						if (CBOOL(BgL_wideningz00_1752))
							{	/* Object/tools.scm 88 */
								obj_t BgL_arg1319z00_1753;
								obj_t BgL_arg1320z00_1754;

								{
									BgL_tclassz00_bglt BgL_auxz00_2338;

									{
										obj_t BgL_auxz00_2339;

										{	/* Object/tools.scm 88 */
											BgL_objectz00_bglt BgL_tmpz00_2340;

											BgL_tmpz00_2340 =
												((BgL_objectz00_bglt)
												((BgL_typez00_bglt) BgL_typez00_13));
											BgL_auxz00_2339 = BGL_OBJECT_WIDENING(BgL_tmpz00_2340);
										}
										BgL_auxz00_2338 = ((BgL_tclassz00_bglt) BgL_auxz00_2339);
									}
									BgL_arg1319z00_1753 =
										(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_2338))->
										BgL_widezd2typezd2);
								}
								{	/* Object/tools.scm 88 */
									obj_t BgL_arg1321z00_1755;

									BgL_arg1321z00_1755 = MAKE_YOUNG_PAIR(BgL_objz00_15, BNIL);
									BgL_arg1320z00_1754 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(4), BgL_arg1321z00_1755);
								}
								BGL_TAIL return
									BGl_makezd2directzd2setz12z12zzobject_toolsz00(
									((BgL_typez00_bglt) BgL_arg1319z00_1753), BgL_slotz00_14,
									BgL_arg1320z00_1754, BgL_valz00_16);
							}
						else
							{	/* Object/tools.scm 86 */
								BGL_TAIL return
									BGl_makezd2directzd2setz12z12zzobject_toolsz00(BgL_typez00_13,
									BgL_slotz00_14, BgL_objz00_15, BgL_valz00_16);
							}
					}
				}
			}
		}

	}



/* &make-class-set! */
	obj_t BGl_z62makezd2classzd2setz12z70zzobject_toolsz00(obj_t BgL_envz00_2156,
		obj_t BgL_typez00_2157, obj_t BgL_slotz00_2158, obj_t BgL_objz00_2159,
		obj_t BgL_valz00_2160)
	{
		{	/* Object/tools.scm 83 */
			return
				BGl_makezd2classzd2setz12z12zzobject_toolsz00(
				((BgL_typez00_bglt) BgL_typez00_2157),
				((BgL_slotz00_bglt) BgL_slotz00_2158), BgL_objz00_2159,
				BgL_valz00_2160);
		}

	}



/* make-direct-ref */
	obj_t BGl_makezd2directzd2refz00zzobject_toolsz00(obj_t BgL_typez00_17,
		BgL_slotz00_bglt BgL_slotz00_18, obj_t BgL_objz00_19)
	{
		{	/* Object/tools.scm 93 */
			{	/* Object/tools.scm 94 */
				obj_t BgL_fnamez00_1757;

				BgL_fnamez00_1757 =
					(((BgL_slotz00_bglt) COBJECT(BgL_slotz00_18))->BgL_namez00);
				{	/* Object/tools.scm 94 */
					obj_t BgL_tnamez00_1758;

					BgL_tnamez00_1758 =
						(((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_typez00_17)))->BgL_namez00);
					{	/* Object/tools.scm 95 */
						obj_t BgL_fmtz00_1759;

						{	/* Object/tools.scm 96 */
							obj_t BgL_list1329z00_1767;

							{	/* Object/tools.scm 96 */
								obj_t BgL_arg1331z00_1768;

								BgL_arg1331z00_1768 = MAKE_YOUNG_PAIR(BgL_fnamez00_1757, BNIL);
								BgL_list1329z00_1767 =
									MAKE_YOUNG_PAIR(BgL_tnamez00_1758, BgL_arg1331z00_1768);
							}
							BgL_fmtz00_1759 =
								BGl_formatz00zz__r4_output_6_10_3z00
								(BGl_string1658z00zzobject_toolsz00, BgL_list1329z00_1767);
						}
						{	/* Object/tools.scm 96 */

							{	/* Object/tools.scm 98 */
								obj_t BgL_arg1322z00_1760;
								obj_t BgL_arg1323z00_1761;

								BgL_arg1322z00_1760 =
									(((BgL_typez00_bglt) COBJECT(
											((BgL_typez00_bglt)
												(((BgL_slotz00_bglt) COBJECT(BgL_slotz00_18))->
													BgL_typez00))))->BgL_idz00);
								BgL_arg1323z00_1761 =
									(((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
												BgL_typez00_17)))->BgL_idz00);
								{	/* Object/tools.scm 97 */
									obj_t BgL_list1324z00_1762;

									{	/* Object/tools.scm 97 */
										obj_t BgL_arg1325z00_1763;

										{	/* Object/tools.scm 97 */
											obj_t BgL_arg1326z00_1764;

											{	/* Object/tools.scm 97 */
												obj_t BgL_arg1327z00_1765;

												BgL_arg1327z00_1765 =
													MAKE_YOUNG_PAIR(BgL_objz00_19, BNIL);
												BgL_arg1326z00_1764 =
													MAKE_YOUNG_PAIR(BgL_fmtz00_1759, BgL_arg1327z00_1765);
											}
											BgL_arg1325z00_1763 =
												MAKE_YOUNG_PAIR(BgL_fnamez00_1757, BgL_arg1326z00_1764);
										}
										BgL_list1324z00_1762 =
											MAKE_YOUNG_PAIR(BgL_arg1323z00_1761, BgL_arg1325z00_1763);
									}
									return
										BGl_makezd2privatezd2sexpz00zzast_privatez00(CNST_TABLE_REF
										(5), BgL_arg1322z00_1760, BgL_list1324z00_1762);
								}
							}
						}
					}
				}
			}
		}

	}



/* make-direct-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2directzd2setz12z12zzobject_toolsz00(BgL_typez00_bglt
		BgL_typez00_20, BgL_slotz00_bglt BgL_slotz00_21, obj_t BgL_objz00_22,
		obj_t BgL_valz00_23)
	{
		{	/* Object/tools.scm 107 */
			{	/* Object/tools.scm 108 */
				obj_t BgL_fnamez00_1769;

				BgL_fnamez00_1769 =
					(((BgL_slotz00_bglt) COBJECT(BgL_slotz00_21))->BgL_namez00);
				{	/* Object/tools.scm 108 */
					obj_t BgL_tnamez00_1770;

					BgL_tnamez00_1770 =
						(((BgL_typez00_bglt) COBJECT(BgL_typez00_20))->BgL_namez00);
					{	/* Object/tools.scm 109 */
						obj_t BgL_fmtz00_1771;

						{	/* Object/tools.scm 111 */
							obj_t BgL_arg1348z00_1781;

							BgL_arg1348z00_1781 =
								(((BgL_typez00_bglt) COBJECT(
										((BgL_typez00_bglt)
											(((BgL_slotz00_bglt) COBJECT(BgL_slotz00_21))->
												BgL_typez00))))->BgL_namez00);
							{	/* Object/tools.scm 110 */
								obj_t BgL_list1349z00_1782;

								{	/* Object/tools.scm 110 */
									obj_t BgL_arg1351z00_1783;

									{	/* Object/tools.scm 110 */
										obj_t BgL_arg1352z00_1784;

										BgL_arg1352z00_1784 =
											MAKE_YOUNG_PAIR(BgL_arg1348z00_1781, BNIL);
										BgL_arg1351z00_1783 =
											MAKE_YOUNG_PAIR(BgL_fnamez00_1769, BgL_arg1352z00_1784);
									}
									BgL_list1349z00_1782 =
										MAKE_YOUNG_PAIR(BgL_tnamez00_1770, BgL_arg1351z00_1783);
								}
								BgL_fmtz00_1771 =
									BGl_formatz00zz__r4_output_6_10_3z00
									(BGl_string1659z00zzobject_toolsz00, BgL_list1349z00_1782);
							}
						}
						{	/* Object/tools.scm 110 */

							{	/* Object/tools.scm 113 */
								obj_t BgL_arg1332z00_1772;
								obj_t BgL_arg1333z00_1773;
								obj_t BgL_arg1335z00_1774;

								BgL_arg1332z00_1772 =
									(((BgL_typez00_bglt) COBJECT(
											((BgL_typez00_bglt)
												(((BgL_slotz00_bglt) COBJECT(BgL_slotz00_21))->
													BgL_typez00))))->BgL_idz00);
								BgL_arg1333z00_1773 =
									(((BgL_typez00_bglt) COBJECT(BgL_typez00_20))->BgL_idz00);
								BgL_arg1335z00_1774 =
									(((BgL_slotz00_bglt) COBJECT(BgL_slotz00_21))->BgL_namez00);
								{	/* Object/tools.scm 112 */
									obj_t BgL_list1336z00_1775;

									{	/* Object/tools.scm 112 */
										obj_t BgL_arg1339z00_1776;

										{	/* Object/tools.scm 112 */
											obj_t BgL_arg1340z00_1777;

											{	/* Object/tools.scm 112 */
												obj_t BgL_arg1342z00_1778;

												{	/* Object/tools.scm 112 */
													obj_t BgL_arg1343z00_1779;

													BgL_arg1343z00_1779 =
														MAKE_YOUNG_PAIR(BgL_valz00_23, BNIL);
													BgL_arg1342z00_1778 =
														MAKE_YOUNG_PAIR(BgL_objz00_22, BgL_arg1343z00_1779);
												}
												BgL_arg1340z00_1777 =
													MAKE_YOUNG_PAIR(BgL_fmtz00_1771, BgL_arg1342z00_1778);
											}
											BgL_arg1339z00_1776 =
												MAKE_YOUNG_PAIR(BgL_arg1335z00_1774,
												BgL_arg1340z00_1777);
										}
										BgL_list1336z00_1775 =
											MAKE_YOUNG_PAIR(BgL_arg1333z00_1773, BgL_arg1339z00_1776);
									}
									return
										BGl_makezd2privatezd2sexpz00zzast_privatez00(CNST_TABLE_REF
										(6), BgL_arg1332z00_1772, BgL_list1336z00_1775);
								}
							}
						}
					}
				}
			}
		}

	}



/* &make-direct-set! */
	obj_t BGl_z62makezd2directzd2setz12z70zzobject_toolsz00(obj_t BgL_envz00_2161,
		obj_t BgL_typez00_2162, obj_t BgL_slotz00_2163, obj_t BgL_objz00_2164,
		obj_t BgL_valz00_2165)
	{
		{	/* Object/tools.scm 107 */
			return
				BGl_makezd2directzd2setz12z12zzobject_toolsz00(
				((BgL_typez00_bglt) BgL_typez00_2162),
				((BgL_slotz00_bglt) BgL_slotz00_2163), BgL_objz00_2164,
				BgL_valz00_2165);
		}

	}



/* find-class-slot */
	BGL_EXPORTED_DEF obj_t
		BGl_findzd2classzd2slotz00zzobject_toolsz00(BgL_typez00_bglt
		BgL_klassz00_24, obj_t BgL_idz00_25)
	{
		{	/* Object/tools.scm 122 */
		BGl_findzd2classzd2slotz00zzobject_toolsz00:
			{	/* Object/tools.scm 123 */
				obj_t BgL_g1109z00_1786;

				{
					BgL_tclassz00_bglt BgL_auxz00_2396;

					{
						obj_t BgL_auxz00_2397;

						{	/* Object/tools.scm 123 */
							BgL_objectz00_bglt BgL_tmpz00_2398;

							BgL_tmpz00_2398 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_klassz00_24));
							BgL_auxz00_2397 = BGL_OBJECT_WIDENING(BgL_tmpz00_2398);
						}
						BgL_auxz00_2396 = ((BgL_tclassz00_bglt) BgL_auxz00_2397);
					}
					BgL_g1109z00_1786 =
						(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_2396))->BgL_slotsz00);
				}
				{
					obj_t BgL_slotsz00_1788;

					BgL_slotsz00_1788 = BgL_g1109z00_1786;
				BgL_zc3z04anonymousza31362ze3z87_1789:
					if (NULLP(BgL_slotsz00_1788))
						{	/* Object/tools.scm 126 */
							bool_t BgL_test1694z00_2406;

							{	/* Object/tools.scm 126 */
								obj_t BgL_tmpz00_2407;

								{
									BgL_tclassz00_bglt BgL_auxz00_2408;

									{
										obj_t BgL_auxz00_2409;

										{	/* Object/tools.scm 126 */
											BgL_objectz00_bglt BgL_tmpz00_2410;

											BgL_tmpz00_2410 =
												((BgL_objectz00_bglt)
												((BgL_typez00_bglt) BgL_klassz00_24));
											BgL_auxz00_2409 = BGL_OBJECT_WIDENING(BgL_tmpz00_2410);
										}
										BgL_auxz00_2408 = ((BgL_tclassz00_bglt) BgL_auxz00_2409);
									}
									BgL_tmpz00_2407 =
										(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_2408))->
										BgL_wideningz00);
								}
								BgL_test1694z00_2406 = CBOOL(BgL_tmpz00_2407);
							}
							if (BgL_test1694z00_2406)
								{	/* Object/tools.scm 127 */
									obj_t BgL_arg1367z00_1792;

									{
										BgL_tclassz00_bglt BgL_auxz00_2417;

										{
											obj_t BgL_auxz00_2418;

											{	/* Object/tools.scm 127 */
												BgL_objectz00_bglt BgL_tmpz00_2419;

												BgL_tmpz00_2419 =
													((BgL_objectz00_bglt)
													((BgL_typez00_bglt) BgL_klassz00_24));
												BgL_auxz00_2418 = BGL_OBJECT_WIDENING(BgL_tmpz00_2419);
											}
											BgL_auxz00_2417 = ((BgL_tclassz00_bglt) BgL_auxz00_2418);
										}
										BgL_arg1367z00_1792 =
											(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_2417))->
											BgL_itszd2superzd2);
									}
									{
										BgL_typez00_bglt BgL_klassz00_2425;

										BgL_klassz00_2425 =
											((BgL_typez00_bglt) BgL_arg1367z00_1792);
										BgL_klassz00_24 = BgL_klassz00_2425;
										goto BGl_findzd2classzd2slotz00zzobject_toolsz00;
									}
								}
							else
								{	/* Object/tools.scm 126 */
									return BFALSE;
								}
						}
					else
						{	/* Object/tools.scm 125 */
							if (
								((((BgL_slotz00_bglt) COBJECT(
												((BgL_slotz00_bglt)
													CAR(
														((obj_t) BgL_slotsz00_1788)))))->BgL_idz00) ==
									BgL_idz00_25))
								{	/* Object/tools.scm 128 */
									return CAR(((obj_t) BgL_slotsz00_1788));
								}
							else
								{	/* Object/tools.scm 131 */
									obj_t BgL_arg1375z00_1796;

									BgL_arg1375z00_1796 = CDR(((obj_t) BgL_slotsz00_1788));
									{
										obj_t BgL_slotsz00_2437;

										BgL_slotsz00_2437 = BgL_arg1375z00_1796;
										BgL_slotsz00_1788 = BgL_slotsz00_2437;
										goto BgL_zc3z04anonymousza31362ze3z87_1789;
									}
								}
						}
				}
			}
		}

	}



/* &find-class-slot */
	obj_t BGl_z62findzd2classzd2slotz62zzobject_toolsz00(obj_t BgL_envz00_2166,
		obj_t BgL_klassz00_2167, obj_t BgL_idz00_2168)
	{
		{	/* Object/tools.scm 122 */
			return
				BGl_findzd2classzd2slotz00zzobject_toolsz00(
				((BgL_typez00_bglt) BgL_klassz00_2167), BgL_idz00_2168);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzobject_toolsz00(void)
	{
		{	/* Object/tools.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzobject_toolsz00(void)
	{
		{	/* Object/tools.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzobject_toolsz00(void)
	{
		{	/* Object/tools.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzobject_toolsz00(void)
	{
		{	/* Object/tools.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1660z00zzobject_toolsz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1660z00zzobject_toolsz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1660z00zzobject_toolsz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1660z00zzobject_toolsz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1660z00zzobject_toolsz00));
			BGl_modulezd2initializa7ationz75zztype_toolsz00(453414928L,
				BSTRING_TO_STRING(BGl_string1660z00zzobject_toolsz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1660z00zzobject_toolsz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1660z00zzobject_toolsz00));
			BGl_modulezd2initializa7ationz75zzast_privatez00(135263837L,
				BSTRING_TO_STRING(BGl_string1660z00zzobject_toolsz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1660z00zzobject_toolsz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1660z00zzobject_toolsz00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string1660z00zzobject_toolsz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1660z00zzobject_toolsz00));
			return
				BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1660z00zzobject_toolsz00));
		}

	}

#ifdef __cplusplus
}
#endif
