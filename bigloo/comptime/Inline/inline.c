/*===========================================================================*/
/*   (Inline/inline.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Inline/inline.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_INLINE_INLINE_TYPE_DEFINITIONS
#define BGL_INLINE_INLINE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_retblockz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                  *BgL_retblockz00_bglt;

	typedef struct BgL_returnz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_retblockz00_bgl *BgL_blockz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                *BgL_returnz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_isfunz00_bgl
	{
		struct BgL_nodez00_bgl *BgL_originalzd2bodyzd2;
		obj_t BgL_recursivezd2callszd2;
		bool_t BgL_tailrecz00;
	}               *BgL_isfunz00_bglt;


#endif													// BGL_INLINE_INLINE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL long
		BGl_isfunzd2arityzd2zzinline_inlinez00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_inlinezd2nodeza2z12z62zzinline_inlinez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2dssslzd2keywordsz00zzinline_inlinez00(BgL_sfunz00_bglt);
	static BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2switch1338z62zzinline_inlinez00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2recursivezd2callsz00zzinline_inlinez00(BgL_sfunz00_bglt);
	static obj_t BGl_z62isfunzd2strengthzb0zzinline_inlinez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2tailreczd2setz12z12zzinline_inlinez00(BgL_sfunz00_bglt, bool_t);
	BGL_EXPORTED_DECL BgL_sfunz00_bglt BGl_makezd2isfunzd2zzinline_inlinez00(long,
		obj_t, obj_t, obj_t, bool_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, BgL_nodez00_bglt,
		obj_t, bool_t);
	extern obj_t BGl_za2optimza2z00zzengine_paramz00;
	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t BGl_z62isfunzd2effectzd2setz12z70zzinline_inlinez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_returnz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2propertyzd2setz12z12zzinline_inlinez00(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzinline_inlinez00 = BUNSPEC;
	BGL_EXPORTED_DECL bool_t
		BGl_isfunzd2tailreczd2zzinline_inlinez00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_inlinezd2sfunz12zc0zzinline_inlinez00(BgL_variablez00_bglt, long,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2argszd2namez00zzinline_inlinez00(BgL_sfunz00_bglt);
	static obj_t BGl_z62isfunzd2argszd2namez62zzinline_inlinez00(obj_t, obj_t);
	static BgL_sfunz00_bglt BGl_z62makezd2isfunzb0zzinline_inlinez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	extern BgL_nodez00_bglt BGl_alphatiza7eza7zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t, BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62isfunzd2classzd2setz12z70zzinline_inlinez00(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62isfunzd2originalzd2bodyz62zzinline_inlinez00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2app1322z62zzinline_inlinez00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2thezd2closurezd2globalzd2zzinline_inlinez00(BgL_sfunz00_bglt);
	extern obj_t BGl_sfunz00zzast_varz00;
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2predicatezd2ofz00zzinline_inlinez00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2classzd2zzinline_inlinez00(BgL_sfunz00_bglt);
	static obj_t BGl_z62isfunzd2recursivezd2callsz62zzinline_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_toplevelzd2initzd2zzinline_inlinez00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	extern obj_t BGl_za2nonzd2inlinedzd2callsza2z00zzinline_walkz00;
	static obj_t BGl_z62isfunzd2failsafezd2setz12z70zzinline_inlinez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_inlinezd2nodezd2zzinline_inlinez00(BgL_nodez00_bglt, long, obj_t);
	static BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2cast1330z62zzinline_inlinez00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2setzd2exzd2i1344z62zzinline_inlinez00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzinline_inlinez00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2strengthzd2zzinline_inlinez00(BgL_sfunz00_bglt);
	static BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2conditio1334z62zzinline_inlinez00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62isfunzf3z91zzinline_inlinez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2boxzd2setz121356za2zzinline_inlinez00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzinline_inlinez00(void);
	static BgL_nodez00_bglt
		BGl_disablezd2inliningz12zc0zzinline_inlinez00(BgL_nodez00_bglt);
	static BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2makezd2box1352zb0zzinline_inlinez00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2setq1332z62zzinline_inlinez00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static obj_t BGl_z62isfunzd2arityzb0zzinline_inlinez00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2sync1320z62zzinline_inlinez00(obj_t, obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2letzd2fun1340zb0zzinline_inlinez00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2fail1336z62zzinline_inlinez00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2effectzd2zzinline_inlinez00(BgL_sfunz00_bglt);
	static BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2jumpzd2exzd21346z62zzinline_inlinez00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2keyszd2zzinline_inlinez00(BgL_sfunz00_bglt);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2optionalszd2zzinline_inlinez00(BgL_sfunz00_bglt);
	static obj_t BGl_z62isfunzd2optionalszb0zzinline_inlinez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2sidezd2effectz00zzinline_inlinez00(BgL_sfunz00_bglt);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_isfunzd2originalzd2bodyz00zzinline_inlinez00(BgL_sfunz00_bglt);
	static obj_t BGl_methodzd2initzd2zzinline_inlinez00(void);
	extern bool_t BGl_inlinezd2appzf3z21zzinline_appz00(BgL_variablez00_bglt,
		long, long, obj_t);
	static obj_t BGl_z62isfunzd2keyszb0zzinline_inlinez00(obj_t, obj_t);
	extern obj_t BGl_externz00zzast_nodez00;
	static BgL_sfunz00_bglt BGl_z62isfunzd2nilzb0zzinline_inlinez00(obj_t);
	static obj_t BGl_z62isfunzd2classzb0zzinline_inlinez00(obj_t, obj_t);
	static obj_t BGl_z62isfunzd2loczd2setz12z70zzinline_inlinez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_za2kfactorza2z00zzinline_walkz00;
	static obj_t
		BGl_z62isfunzd2dssslzd2keywordszd2setz12za2zzinline_inlinez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2bodyzd2zzinline_inlinez00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2failsafezd2setz12z12zzinline_inlinez00(BgL_sfunz00_bglt, obj_t);
	static BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2sequence1318z62zzinline_inlinez00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62isfunzd2loczb0zzinline_inlinez00(obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62inlinezd2nodezb0zzinline_inlinez00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2thezd2closurez00zzinline_inlinez00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2predicatezd2ofzd2setz12zc0zzinline_inlinez00(BgL_sfunz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_isfunzd2topzf3z21zzinline_inlinez00(BgL_sfunz00_bglt);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2recursivezd2callszd2setz12zc0zzinline_inlinez00
		(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62isfunzd2bodyzb0zzinline_inlinez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_sfunz00_bglt BGl_isfunzd2nilzd2zzinline_inlinez00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2thezd2closurezd2setz12zc0zzinline_inlinez00(BgL_sfunz00_bglt,
		obj_t);
	static obj_t
		BGl_z62isfunzd2thezd2closurezd2globalzd2setz12z70zzinline_inlinez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_isfunz00zzinline_inlinez00 = BUNSPEC;
	static obj_t BGl_z62isfunzd2topzf3z43zzinline_inlinez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2sidezd2effectzd2setz12zc0zzinline_inlinez00(BgL_sfunz00_bglt,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31612ze3ze5zzinline_inlinez00(obj_t);
	static obj_t BGl_z62inlinezd2sfunz12za2zzinline_inlinez00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2dssslzd2keywordszd2setz12zc0zzinline_inlinez00(BgL_sfunz00_bglt,
		obj_t);
	static obj_t BGl_z62inlinezd2node1309zb0zzinline_inlinez00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62isfunzd2recursivezd2callszd2setz12za2zzinline_inlinez00(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62disablezd2inliningz12zd2le1360z70zzinline_inlinez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2loczd2zzinline_inlinez00(BgL_sfunz00_bglt);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2loczd2setz12z12zzinline_inlinez00(BgL_sfunz00_bglt, obj_t);
	static BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2funcall1326z62zzinline_inlinez00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62isfunzd2tailreczd2setz12z70zzinline_inlinez00(obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2kwote1316z62zzinline_inlinez00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzinline_inlinez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_alphatiza7eza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinline_appz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinline_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2topzf3zd2setz12ze1zzinline_inlinez00(BgL_sfunz00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2argszd2zzinline_inlinez00(BgL_sfunz00_bglt);
	static BgL_nodez00_bglt
		BGl_z62disablezd2inliningz12za2zzinline_inlinez00(obj_t, obj_t);
	extern obj_t BGl_appz00zzast_nodez00;
	static obj_t BGl_z62isfunzd2topzf3zd2setz12z83zzinline_inlinez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t BGl_z62isfunzd2strengthzd2setz12z70zzinline_inlinez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62inlinezd2nodeza2z12z00zzinline_inlinez00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2letzd2var1342zb0zzinline_inlinez00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2effectzd2setz12z12zzinline_inlinez00(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62isfunzd2argszb0zzinline_inlinez00(obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_z62isfunzd2failsafezb0zzinline_inlinez00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzinline_inlinez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzinline_inlinez00(void);
	static obj_t BGl_z62isfunzd2tailreczb0zzinline_inlinez00(obj_t, obj_t);
	BGL_IMPORT long bgl_list_length(obj_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62isfunzd2predicatezd2ofz62zzinline_inlinez00(obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzinline_inlinez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzinline_inlinez00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2stackzd2allocatorzd2setz12zc0zzinline_inlinez00
		(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62isfunzd2sidezd2effectz62zzinline_inlinez00(obj_t, obj_t);
	extern BgL_nodez00_bglt BGl_walk0z12z12zzast_walkz00(BgL_nodez00_bglt, obj_t);
	static BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2boxzd2ref1354zb0zzinline_inlinez00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62isfunzd2effectzb0zzinline_inlinez00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2retblock1348z62zzinline_inlinez00(obj_t, obj_t,
		obj_t, obj_t);
	extern BgL_nodez00_bglt BGl_inlinezd2appzd2zzinline_appz00(BgL_appz00_bglt,
		long, obj_t);
	extern obj_t BGl_retblockz00zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2stackzd2allocatorz00zzinline_inlinez00(BgL_sfunz00_bglt);
	static BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2atom1312z62zzinline_inlinez00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62isfunzd2propertyzb0zzinline_inlinez00(obj_t, obj_t);
	extern obj_t BGl_za2inlinezd2modeza2zd2zzinline_walkz00;
	static obj_t BGl_z62lambda1610z62zzinline_inlinez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1611z62zzinline_inlinez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31628ze3ze5zzinline_inlinez00(obj_t);
	static obj_t
		BGl_z62isfunzd2stackzd2allocatorzd2setz12za2zzinline_inlinez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda1626z62zzinline_inlinez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1627z62zzinline_inlinez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62isfunzd2propertyzd2setz12z70zzinline_inlinez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	static obj_t BGl_z62isfunzd2thezd2closurezd2setz12za2zzinline_inlinez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62isfunzd2thezd2closurezd2globalzb0zzinline_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62isfunzd2thezd2closurez62zzinline_inlinez00(obj_t, obj_t);
	static obj_t BGl_z62isfunzd2sidezd2effectzd2setz12za2zzinline_inlinez00(obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2appzd2ly1324zb0zzinline_inlinez00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2failsafezd2zzinline_inlinez00(BgL_sfunz00_bglt);
	static obj_t BGl_z62disablezd2inliningz121357za2zzinline_inlinez00(obj_t,
		obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2thezd2closurezd2globalzd2setz12z12zzinline_inlinez00
		(BgL_sfunz00_bglt, obj_t);
	static BgL_sfunz00_bglt BGl_z62lambda1565z62zzinline_inlinez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2strengthzd2setz12z12zzinline_inlinez00(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62isfunzd2stackzd2allocatorz62zzinline_inlinez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_isfunzf3zf3zzinline_inlinez00(obj_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static BgL_sfunz00_bglt BGl_z62lambda1574z62zzinline_inlinez00(obj_t, obj_t);
	static BgL_sfunz00_bglt BGl_z62lambda1577z62zzinline_inlinez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2classzd2setz12z12zzinline_inlinez00(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31576ze3ze5zzinline_inlinez00(obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2var1314z62zzinline_inlinez00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2propertyzd2zzinline_inlinez00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2bodyzd2setz12z12zzinline_inlinez00(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62isfunzd2bodyzd2setz12z70zzinline_inlinez00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62isfunzd2predicatezd2ofzd2setz12za2zzinline_inlinez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static obj_t BGl_z62isfunzd2dssslzd2keywordsz62zzinline_inlinez00(obj_t,
		obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	BGL_EXPORTED_DECL obj_t
		BGl_isfunzd2argszd2setz12z12zzinline_inlinez00(BgL_sfunz00_bglt, obj_t);
	static BgL_nodez00_bglt BGl_z62lambda1595z62zzinline_inlinez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1596z62zzinline_inlinez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62isfunzd2argszd2setz12z70zzinline_inlinez00(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2extern1328z62zzinline_inlinez00(obj_t, obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2return1350z62zzinline_inlinez00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t __cnst[16];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_isfunzd2keyszd2envz00zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2keysza72030za7,
		BGl_z62isfunzd2keyszb0zzinline_inlinez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isfunzd2thezd2closurezd2envzd2zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2theza7d2031za7,
		BGl_z62isfunzd2thezd2closurez62zzinline_inlinez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isfunzd2argszd2setz12zd2envzc0zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2argsza72032za7,
		BGl_z62isfunzd2argszd2setz12z70zzinline_inlinez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_isfunzd2nilzd2envz00zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2nilza7b2033za7,
		BGl_z62isfunzd2nilzb0zzinline_inlinez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_isfunzd2classzd2envz00zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2class2034z00,
		BGl_z62isfunzd2classzb0zzinline_inlinez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_isfunzd2effectzd2envz00zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2effec2035z00,
		BGl_z62isfunzd2effectzb0zzinline_inlinez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isfunzd2classzd2setz12zd2envzc0zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2class2036z00,
		BGl_z62isfunzd2classzd2setz12z70zzinline_inlinez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isfunzd2propertyzd2setz12zd2envzc0zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2prope2037z00,
		BGl_z62isfunzd2propertyzd2setz12z70zzinline_inlinez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isfunzd2bodyzd2setz12zd2envzc0zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2bodyza72038za7,
		BGl_z62isfunzd2bodyzd2setz12z70zzinline_inlinez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_inlinezd2sfunz12zd2envz12zzinline_inlinez00,
		BgL_bgl_za762inlineza7d2sfun2039z00,
		BGl_z62inlinezd2sfunz12za2zzinline_inlinez00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isfunzd2strengthzd2setz12zd2envzc0zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2stren2040z00,
		BGl_z62isfunzd2strengthzd2setz12z70zzinline_inlinez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isfunzd2failsafezd2setz12zd2envzc0zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2fails2041z00,
		BGl_z62isfunzd2failsafezd2setz12z70zzinline_inlinez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isfunzd2predicatezd2ofzd2setz12zd2envz12zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2predi2042z00,
		BGl_z62isfunzd2predicatezd2ofzd2setz12za2zzinline_inlinez00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_isfunzd2strengthzd2envz00zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2stren2043z00,
		BGl_z62isfunzd2strengthzb0zzinline_inlinez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isfunzd2topzf3zd2setz12zd2envz33zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2topza7f2044za7,
		BGl_z62isfunzd2topzf3zd2setz12z83zzinline_inlinez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isfunzd2thezd2closurezd2setz12zd2envz12zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2theza7d2045za7,
		BGl_z62isfunzd2thezd2closurezd2setz12za2zzinline_inlinez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_isfunzd2arityzd2envz00zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2arity2046z00,
		BGl_z62isfunzd2arityzb0zzinline_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_isfunzf3zd2envz21zzinline_inlinez00,
		BgL_bgl_za762isfunza7f3za791za7za72047za7,
		BGl_z62isfunzf3z91zzinline_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_isfunzd2bodyzd2envz00zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2bodyza72048za7,
		BGl_z62isfunzd2bodyzb0zzinline_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_inlinezd2nodezd2envz00zzinline_inlinez00,
		BgL_bgl_za762inlineza7d2node2049z00,
		BGl_z62inlinezd2nodezb0zzinline_inlinez00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isfunzd2sidezd2effectzd2envzd2zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2sideza72050za7,
		BGl_z62isfunzd2sidezd2effectz62zzinline_inlinez00, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_GENERIC
		(BGl_disablezd2inliningz12zd2envz12zzinline_inlinez00,
		BgL_bgl_za762disableza7d2inl2051z00,
		BGl_z62disablezd2inliningz12za2zzinline_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_isfunzd2argszd2envz00zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2argsza72052za7,
		BGl_z62isfunzd2argszb0zzinline_inlinez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isfunzd2recursivezd2callszd2setz12zd2envz12zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2recur2053z00,
		BGl_z62isfunzd2recursivezd2callszd2setz12za2zzinline_inlinez00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_isfunzd2failsafezd2envz00zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2fails2054z00,
		BGl_z62isfunzd2failsafezb0zzinline_inlinez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isfunzd2thezd2closurezd2globalzd2setz12zd2envzc0zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2theza7d2055za7,
		BGl_z62isfunzd2thezd2closurezd2globalzd2setz12z70zzinline_inlinez00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isfunzd2stackzd2allocatorzd2envzd2zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2stack2056z00,
		BGl_z62isfunzd2stackzd2allocatorz62zzinline_inlinez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isfunzd2thezd2closurezd2globalzd2envz00zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2theza7d2057za7,
		BGl_z62isfunzd2thezd2closurezd2globalzb0zzinline_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_isfunzd2loczd2envz00zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2locza7b2058za7,
		BGl_z62isfunzd2loczb0zzinline_inlinez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isfunzd2originalzd2bodyzd2envzd2zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2origi2059z00,
		BGl_z62isfunzd2originalzd2bodyz62zzinline_inlinez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inlinezd2nodeza2z12zd2envzb0zzinline_inlinez00,
		BgL_bgl_za762inlineza7d2node2060z00,
		BGl_z62inlinezd2nodeza2z12z00zzinline_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1984z00zzinline_inlinez00,
		BgL_bgl_za762lambda1596za7622061z00, BGl_z62lambda1596z62zzinline_inlinez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1985z00zzinline_inlinez00,
		BgL_bgl_za762lambda1595za7622062z00, BGl_z62lambda1595z62zzinline_inlinez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1986z00zzinline_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2063za7,
		BGl_z62zc3z04anonymousza31612ze3ze5zzinline_inlinez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1987z00zzinline_inlinez00,
		BgL_bgl_za762lambda1611za7622064z00, BGl_z62lambda1611z62zzinline_inlinez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1988z00zzinline_inlinez00,
		BgL_bgl_za762lambda1610za7622065z00, BGl_z62lambda1610z62zzinline_inlinez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1989z00zzinline_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2066za7,
		BGl_z62zc3z04anonymousza31628ze3ze5zzinline_inlinez00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1997z00zzinline_inlinez00,
		BgL_bgl_string1997za700za7za7i2067za7, "inline-node1309", 15);
	      DEFINE_STRING(BGl_string1999z00zzinline_inlinez00,
		BgL_bgl_string1999za700za7za7i2068za7, "disable-inlining!1357", 21);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isfunzd2optionalszd2envz00zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2optio2069z00,
		BGl_z62isfunzd2optionalszb0zzinline_inlinez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isfunzd2argszd2namezd2envzd2zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2argsza72070za7,
		BGl_z62isfunzd2argszd2namez62zzinline_inlinez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isfunzd2stackzd2allocatorzd2setz12zd2envz12zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2stack2071z00,
		BGl_z62isfunzd2stackzd2allocatorzd2setz12za2zzinline_inlinez00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1990z00zzinline_inlinez00,
		BgL_bgl_za762lambda1627za7622072z00, BGl_z62lambda1627z62zzinline_inlinez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1991z00zzinline_inlinez00,
		BgL_bgl_za762lambda1626za7622073z00, BGl_z62lambda1626z62zzinline_inlinez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1992z00zzinline_inlinez00,
		BgL_bgl_za762lambda1577za7622074z00, BGl_z62lambda1577z62zzinline_inlinez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1993z00zzinline_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2075za7,
		BGl_z62zc3z04anonymousza31576ze3ze5zzinline_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1994z00zzinline_inlinez00,
		BgL_bgl_za762lambda1574za7622076z00, BGl_z62lambda1574z62zzinline_inlinez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1995z00zzinline_inlinez00,
		BgL_bgl_za762lambda1565za7622077z00, BGl_z62lambda1565z62zzinline_inlinez00,
		0L, BUNSPEC, 25);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1996z00zzinline_inlinez00,
		BgL_bgl_za762inlineza7d2node2078z00,
		BGl_z62inlinezd2node1309zb0zzinline_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1998z00zzinline_inlinez00,
		BgL_bgl_za762disableza7d2inl2079z00,
		BGl_z62disablezd2inliningz121357za2zzinline_inlinez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isfunzd2predicatezd2ofzd2envzd2zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2predi2080z00,
		BGl_z62isfunzd2predicatezd2ofz62zzinline_inlinez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isfunzd2recursivezd2callszd2envzd2zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2recur2081z00,
		BGl_z62isfunzd2recursivezd2callsz62zzinline_inlinez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isfunzd2effectzd2setz12zd2envzc0zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2effec2082z00,
		BGl_z62isfunzd2effectzd2setz12z70zzinline_inlinez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isfunzd2dssslzd2keywordszd2setz12zd2envz12zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2dsssl2083z00,
		BGl_z62isfunzd2dssslzd2keywordszd2setz12za2zzinline_inlinez00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_isfunzd2tailreczd2envz00zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2tailr2084z00,
		BGl_z62isfunzd2tailreczb0zzinline_inlinez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_isfunzd2propertyzd2envz00zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2prope2085z00,
		BGl_z62isfunzd2propertyzb0zzinline_inlinez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isfunzd2loczd2setz12zd2envzc0zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2locza7d2086za7,
		BGl_z62isfunzd2loczd2setz12z70zzinline_inlinez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isfunzd2sidezd2effectzd2setz12zd2envz12zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2sideza72087za7,
		BGl_z62isfunzd2sidezd2effectzd2setz12za2zzinline_inlinez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2000z00zzinline_inlinez00,
		BgL_bgl_string2000za700za7za7i2088za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string2002z00zzinline_inlinez00,
		BgL_bgl_string2002za700za7za7i2089za7, "inline-node", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2001z00zzinline_inlinez00,
		BgL_bgl_za762inlineza7d2node2090z00,
		BGl_z62inlinezd2nodezd2atom1312z62zzinline_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2003z00zzinline_inlinez00,
		BgL_bgl_za762inlineza7d2node2091z00,
		BGl_z62inlinezd2nodezd2var1314z62zzinline_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2004z00zzinline_inlinez00,
		BgL_bgl_za762inlineza7d2node2092z00,
		BGl_z62inlinezd2nodezd2kwote1316z62zzinline_inlinez00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_isfunzd2topzf3zd2envzf3zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2topza7f2093za7,
		BGl_z62isfunzd2topzf3z43zzinline_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2005z00zzinline_inlinez00,
		BgL_bgl_za762inlineza7d2node2094z00,
		BGl_z62inlinezd2nodezd2sequence1318z62zzinline_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2006z00zzinline_inlinez00,
		BgL_bgl_za762inlineza7d2node2095z00,
		BGl_z62inlinezd2nodezd2sync1320z62zzinline_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2007z00zzinline_inlinez00,
		BgL_bgl_za762inlineza7d2node2096z00,
		BGl_z62inlinezd2nodezd2app1322z62zzinline_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2008z00zzinline_inlinez00,
		BgL_bgl_za762inlineza7d2node2097z00,
		BGl_z62inlinezd2nodezd2appzd2ly1324zb0zzinline_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2009z00zzinline_inlinez00,
		BgL_bgl_za762inlineza7d2node2098z00,
		BGl_z62inlinezd2nodezd2funcall1326z62zzinline_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2010z00zzinline_inlinez00,
		BgL_bgl_za762inlineza7d2node2099z00,
		BGl_z62inlinezd2nodezd2extern1328z62zzinline_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2011z00zzinline_inlinez00,
		BgL_bgl_za762inlineza7d2node2100z00,
		BGl_z62inlinezd2nodezd2cast1330z62zzinline_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2012z00zzinline_inlinez00,
		BgL_bgl_za762inlineza7d2node2101z00,
		BGl_z62inlinezd2nodezd2setq1332z62zzinline_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2013z00zzinline_inlinez00,
		BgL_bgl_za762inlineza7d2node2102z00,
		BGl_z62inlinezd2nodezd2conditio1334z62zzinline_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2014z00zzinline_inlinez00,
		BgL_bgl_za762inlineza7d2node2103z00,
		BGl_z62inlinezd2nodezd2fail1336z62zzinline_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2015z00zzinline_inlinez00,
		BgL_bgl_za762inlineza7d2node2104z00,
		BGl_z62inlinezd2nodezd2switch1338z62zzinline_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2016z00zzinline_inlinez00,
		BgL_bgl_za762inlineza7d2node2105z00,
		BGl_z62inlinezd2nodezd2letzd2fun1340zb0zzinline_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2017z00zzinline_inlinez00,
		BgL_bgl_za762inlineza7d2node2106z00,
		BGl_z62inlinezd2nodezd2letzd2var1342zb0zzinline_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2018z00zzinline_inlinez00,
		BgL_bgl_za762inlineza7d2node2107z00,
		BGl_z62inlinezd2nodezd2setzd2exzd2i1344z62zzinline_inlinez00, 0L, BUNSPEC,
		3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2019z00zzinline_inlinez00,
		BgL_bgl_za762inlineza7d2node2108z00,
		BGl_z62inlinezd2nodezd2jumpzd2exzd21346z62zzinline_inlinez00, 0L, BUNSPEC,
		3);
	      DEFINE_STRING(BGl_string2026z00zzinline_inlinez00,
		BgL_bgl_string2026za700za7za7i2109za7, "disable-inlining!::node", 23);
	      DEFINE_STRING(BGl_string2027z00zzinline_inlinez00,
		BgL_bgl_string2027za700za7za7i2110za7, "inline_inline", 13);
	      DEFINE_STRING(BGl_string2028z00zzinline_inlinez00,
		BgL_bgl_string2028za700za7za7i2111za7,
		"read (noinline) noinline inline-node1309 _ inline_inline isfun bool tailrec obj recursive-calls original-body done static cgen all ",
		131);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isfunzd2dssslzd2keywordszd2envzd2zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2dsssl2112z00,
		BGl_z62isfunzd2dssslzd2keywordsz62zzinline_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2020z00zzinline_inlinez00,
		BgL_bgl_za762inlineza7d2node2113z00,
		BGl_z62inlinezd2nodezd2retblock1348z62zzinline_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2021z00zzinline_inlinez00,
		BgL_bgl_za762inlineza7d2node2114z00,
		BGl_z62inlinezd2nodezd2return1350z62zzinline_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2022z00zzinline_inlinez00,
		BgL_bgl_za762inlineza7d2node2115z00,
		BGl_z62inlinezd2nodezd2makezd2box1352zb0zzinline_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2023z00zzinline_inlinez00,
		BgL_bgl_za762inlineza7d2node2116z00,
		BGl_z62inlinezd2nodezd2boxzd2ref1354zb0zzinline_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2024z00zzinline_inlinez00,
		BgL_bgl_za762inlineza7d2node2117z00,
		BGl_z62inlinezd2nodezd2boxzd2setz121356za2zzinline_inlinez00, 0L, BUNSPEC,
		3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2025z00zzinline_inlinez00,
		BgL_bgl_za762disableza7d2inl2118z00,
		BGl_z62disablezd2inliningz12zd2le1360z70zzinline_inlinez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_isfunzd2tailreczd2setz12zd2envzc0zzinline_inlinez00,
		BgL_bgl_za762isfunza7d2tailr2119z00,
		BGl_z62isfunzd2tailreczd2setz12z70zzinline_inlinez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2isfunzd2envz00zzinline_inlinez00,
		BgL_bgl_za762makeza7d2isfunza72120za7,
		BGl_z62makezd2isfunzb0zzinline_inlinez00, 0L, BUNSPEC, 22);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzinline_inlinez00));
		     ADD_ROOT((void *) (&BGl_isfunz00zzinline_inlinez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzinline_inlinez00(long
		BgL_checksumz00_3339, char *BgL_fromz00_3340)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzinline_inlinez00))
				{
					BGl_requirezd2initializa7ationz75zzinline_inlinez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzinline_inlinez00();
					BGl_libraryzd2moduleszd2initz00zzinline_inlinez00();
					BGl_cnstzd2initzd2zzinline_inlinez00();
					BGl_importedzd2moduleszd2initz00zzinline_inlinez00();
					BGl_objectzd2initzd2zzinline_inlinez00();
					BGl_genericzd2initzd2zzinline_inlinez00();
					BGl_methodzd2initzd2zzinline_inlinez00();
					return BGl_toplevelzd2initzd2zzinline_inlinez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzinline_inlinez00(void)
	{
		{	/* Inline/inline.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "inline_inline");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"inline_inline");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "inline_inline");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"inline_inline");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "inline_inline");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"inline_inline");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "inline_inline");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"inline_inline");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"inline_inline");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"inline_inline");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzinline_inlinez00(void)
	{
		{	/* Inline/inline.scm 15 */
			{	/* Inline/inline.scm 15 */
				obj_t BgL_cportz00_3134;

				{	/* Inline/inline.scm 15 */
					obj_t BgL_stringz00_3141;

					BgL_stringz00_3141 = BGl_string2028z00zzinline_inlinez00;
					{	/* Inline/inline.scm 15 */
						obj_t BgL_startz00_3142;

						BgL_startz00_3142 = BINT(0L);
						{	/* Inline/inline.scm 15 */
							obj_t BgL_endz00_3143;

							BgL_endz00_3143 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3141)));
							{	/* Inline/inline.scm 15 */

								BgL_cportz00_3134 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3141, BgL_startz00_3142, BgL_endz00_3143);
				}}}}
				{
					long BgL_iz00_3135;

					BgL_iz00_3135 = 15L;
				BgL_loopz00_3136:
					if ((BgL_iz00_3135 == -1L))
						{	/* Inline/inline.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Inline/inline.scm 15 */
							{	/* Inline/inline.scm 15 */
								obj_t BgL_arg2029z00_3137;

								{	/* Inline/inline.scm 15 */

									{	/* Inline/inline.scm 15 */
										obj_t BgL_locationz00_3139;

										BgL_locationz00_3139 = BBOOL(((bool_t) 0));
										{	/* Inline/inline.scm 15 */

											BgL_arg2029z00_3137 =
												BGl_readz00zz__readerz00(BgL_cportz00_3134,
												BgL_locationz00_3139);
										}
									}
								}
								{	/* Inline/inline.scm 15 */
									int BgL_tmpz00_3371;

									BgL_tmpz00_3371 = (int) (BgL_iz00_3135);
									CNST_TABLE_SET(BgL_tmpz00_3371, BgL_arg2029z00_3137);
							}}
							{	/* Inline/inline.scm 15 */
								int BgL_auxz00_3140;

								BgL_auxz00_3140 = (int) ((BgL_iz00_3135 - 1L));
								{
									long BgL_iz00_3376;

									BgL_iz00_3376 = (long) (BgL_auxz00_3140);
									BgL_iz00_3135 = BgL_iz00_3376;
									goto BgL_loopz00_3136;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzinline_inlinez00(void)
	{
		{	/* Inline/inline.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzinline_inlinez00(void)
	{
		{	/* Inline/inline.scm 15 */
			return BUNSPEC;
		}

	}



/* make-isfun */
	BGL_EXPORTED_DEF BgL_sfunz00_bglt BGl_makezd2isfunzd2zzinline_inlinez00(long
		BgL_arity1127z00_3, obj_t BgL_sidezd2effect1128zd2_4,
		obj_t BgL_predicatezd2of1129zd2_5, obj_t BgL_stackzd2allocator1130zd2_6,
		bool_t BgL_topzf31131zf3_7, obj_t BgL_thezd2closure1132zd2_8,
		obj_t BgL_effect1133z00_9, obj_t BgL_failsafe1134z00_10,
		obj_t BgL_property1135z00_11, obj_t BgL_args1136z00_12,
		obj_t BgL_argszd2name1137zd2_13, obj_t BgL_body1138z00_14,
		obj_t BgL_class1139z00_15, obj_t BgL_dssslzd2keywords1140zd2_16,
		obj_t BgL_loc1141z00_17, obj_t BgL_optionals1142z00_18,
		obj_t BgL_keys1143z00_19, obj_t BgL_thezd2closurezd2global1144z00_20,
		obj_t BgL_strength1145z00_21,
		BgL_nodez00_bglt BgL_originalzd2body1146zd2_22,
		obj_t BgL_recursivezd2calls1147zd2_23, bool_t BgL_tailrec1148z00_24)
	{
		{	/* Inline/inline.sch 60 */
			{	/* Inline/inline.sch 60 */
				BgL_sfunz00_bglt BgL_new1150z00_3145;

				{	/* Inline/inline.sch 60 */
					BgL_sfunz00_bglt BgL_tmp1148z00_3146;
					BgL_isfunz00_bglt BgL_wide1149z00_3147;

					{
						BgL_sfunz00_bglt BgL_auxz00_3379;

						{	/* Inline/inline.sch 60 */
							BgL_sfunz00_bglt BgL_new1147z00_3148;

							BgL_new1147z00_3148 =
								((BgL_sfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_sfunz00_bgl))));
							{	/* Inline/inline.sch 60 */
								long BgL_arg1371z00_3149;

								BgL_arg1371z00_3149 = BGL_CLASS_NUM(BGl_sfunz00zzast_varz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1147z00_3148),
									BgL_arg1371z00_3149);
							}
							{	/* Inline/inline.sch 60 */
								BgL_objectz00_bglt BgL_tmpz00_3384;

								BgL_tmpz00_3384 = ((BgL_objectz00_bglt) BgL_new1147z00_3148);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3384, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1147z00_3148);
							BgL_auxz00_3379 = BgL_new1147z00_3148;
						}
						BgL_tmp1148z00_3146 = ((BgL_sfunz00_bglt) BgL_auxz00_3379);
					}
					BgL_wide1149z00_3147 =
						((BgL_isfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_isfunz00_bgl))));
					{	/* Inline/inline.sch 60 */
						obj_t BgL_auxz00_3392;
						BgL_objectz00_bglt BgL_tmpz00_3390;

						BgL_auxz00_3392 = ((obj_t) BgL_wide1149z00_3147);
						BgL_tmpz00_3390 = ((BgL_objectz00_bglt) BgL_tmp1148z00_3146);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3390, BgL_auxz00_3392);
					}
					((BgL_objectz00_bglt) BgL_tmp1148z00_3146);
					{	/* Inline/inline.sch 60 */
						long BgL_arg1370z00_3150;

						BgL_arg1370z00_3150 = BGL_CLASS_NUM(BGl_isfunz00zzinline_inlinez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1148z00_3146), BgL_arg1370z00_3150);
					}
					BgL_new1150z00_3145 = ((BgL_sfunz00_bglt) BgL_tmp1148z00_3146);
				}
				((((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt) BgL_new1150z00_3145)))->BgL_arityz00) =
					((long) BgL_arity1127z00_3), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1150z00_3145)))->
						BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1128zd2_4), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1150z00_3145)))->
						BgL_predicatezd2ofzd2) =
					((obj_t) BgL_predicatezd2of1129zd2_5), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1150z00_3145)))->
						BgL_stackzd2allocatorzd2) =
					((obj_t) BgL_stackzd2allocator1130zd2_6), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1150z00_3145)))->
						BgL_topzf3zf3) = ((bool_t) BgL_topzf31131zf3_7), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1150z00_3145)))->
						BgL_thezd2closurezd2) =
					((obj_t) BgL_thezd2closure1132zd2_8), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1150z00_3145)))->
						BgL_effectz00) = ((obj_t) BgL_effect1133z00_9), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1150z00_3145)))->
						BgL_failsafez00) = ((obj_t) BgL_failsafe1134z00_10), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1150z00_3145)))->
						BgL_argszd2noescapezd2) = ((obj_t) BNIL), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1150z00_3145)))->
						BgL_argszd2retescapezd2) = ((obj_t) BNIL), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1150z00_3145)))->BgL_propertyz00) =
					((obj_t) BgL_property1135z00_11), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1150z00_3145)))->BgL_argsz00) =
					((obj_t) BgL_args1136z00_12), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1150z00_3145)))->BgL_argszd2namezd2) =
					((obj_t) BgL_argszd2name1137zd2_13), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1150z00_3145)))->BgL_bodyz00) =
					((obj_t) BgL_body1138z00_14), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1150z00_3145)))->BgL_classz00) =
					((obj_t) BgL_class1139z00_15), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1150z00_3145)))->BgL_dssslzd2keywordszd2) =
					((obj_t) BgL_dssslzd2keywords1140zd2_16), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1150z00_3145)))->BgL_locz00) =
					((obj_t) BgL_loc1141z00_17), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1150z00_3145)))->BgL_optionalsz00) =
					((obj_t) BgL_optionals1142z00_18), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1150z00_3145)))->BgL_keysz00) =
					((obj_t) BgL_keys1143z00_19), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1150z00_3145)))->BgL_thezd2closurezd2globalz00) =
					((obj_t) BgL_thezd2closurezd2global1144z00_20), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1150z00_3145)))->BgL_strengthz00) =
					((obj_t) BgL_strength1145z00_21), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1150z00_3145)))->BgL_stackablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_isfunz00_bglt BgL_auxz00_3444;

					{
						obj_t BgL_auxz00_3445;

						{	/* Inline/inline.sch 60 */
							BgL_objectz00_bglt BgL_tmpz00_3446;

							BgL_tmpz00_3446 = ((BgL_objectz00_bglt) BgL_new1150z00_3145);
							BgL_auxz00_3445 = BGL_OBJECT_WIDENING(BgL_tmpz00_3446);
						}
						BgL_auxz00_3444 = ((BgL_isfunz00_bglt) BgL_auxz00_3445);
					}
					((((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_3444))->
							BgL_originalzd2bodyzd2) =
						((BgL_nodez00_bglt) BgL_originalzd2body1146zd2_22), BUNSPEC);
				}
				{
					BgL_isfunz00_bglt BgL_auxz00_3451;

					{
						obj_t BgL_auxz00_3452;

						{	/* Inline/inline.sch 60 */
							BgL_objectz00_bglt BgL_tmpz00_3453;

							BgL_tmpz00_3453 = ((BgL_objectz00_bglt) BgL_new1150z00_3145);
							BgL_auxz00_3452 = BGL_OBJECT_WIDENING(BgL_tmpz00_3453);
						}
						BgL_auxz00_3451 = ((BgL_isfunz00_bglt) BgL_auxz00_3452);
					}
					((((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_3451))->
							BgL_recursivezd2callszd2) =
						((obj_t) BgL_recursivezd2calls1147zd2_23), BUNSPEC);
				}
				{
					BgL_isfunz00_bglt BgL_auxz00_3458;

					{
						obj_t BgL_auxz00_3459;

						{	/* Inline/inline.sch 60 */
							BgL_objectz00_bglt BgL_tmpz00_3460;

							BgL_tmpz00_3460 = ((BgL_objectz00_bglt) BgL_new1150z00_3145);
							BgL_auxz00_3459 = BGL_OBJECT_WIDENING(BgL_tmpz00_3460);
						}
						BgL_auxz00_3458 = ((BgL_isfunz00_bglt) BgL_auxz00_3459);
					}
					((((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_3458))->BgL_tailrecz00) =
						((bool_t) BgL_tailrec1148z00_24), BUNSPEC);
				}
				return BgL_new1150z00_3145;
			}
		}

	}



/* &make-isfun */
	BgL_sfunz00_bglt BGl_z62makezd2isfunzb0zzinline_inlinez00(obj_t
		BgL_envz00_2812, obj_t BgL_arity1127z00_2813,
		obj_t BgL_sidezd2effect1128zd2_2814, obj_t BgL_predicatezd2of1129zd2_2815,
		obj_t BgL_stackzd2allocator1130zd2_2816, obj_t BgL_topzf31131zf3_2817,
		obj_t BgL_thezd2closure1132zd2_2818, obj_t BgL_effect1133z00_2819,
		obj_t BgL_failsafe1134z00_2820, obj_t BgL_property1135z00_2821,
		obj_t BgL_args1136z00_2822, obj_t BgL_argszd2name1137zd2_2823,
		obj_t BgL_body1138z00_2824, obj_t BgL_class1139z00_2825,
		obj_t BgL_dssslzd2keywords1140zd2_2826, obj_t BgL_loc1141z00_2827,
		obj_t BgL_optionals1142z00_2828, obj_t BgL_keys1143z00_2829,
		obj_t BgL_thezd2closurezd2global1144z00_2830,
		obj_t BgL_strength1145z00_2831, obj_t BgL_originalzd2body1146zd2_2832,
		obj_t BgL_recursivezd2calls1147zd2_2833, obj_t BgL_tailrec1148z00_2834)
	{
		{	/* Inline/inline.sch 60 */
			return
				BGl_makezd2isfunzd2zzinline_inlinez00(
				(long) CINT(BgL_arity1127z00_2813), BgL_sidezd2effect1128zd2_2814,
				BgL_predicatezd2of1129zd2_2815, BgL_stackzd2allocator1130zd2_2816,
				CBOOL(BgL_topzf31131zf3_2817), BgL_thezd2closure1132zd2_2818,
				BgL_effect1133z00_2819, BgL_failsafe1134z00_2820,
				BgL_property1135z00_2821, BgL_args1136z00_2822,
				BgL_argszd2name1137zd2_2823, BgL_body1138z00_2824,
				BgL_class1139z00_2825, BgL_dssslzd2keywords1140zd2_2826,
				BgL_loc1141z00_2827, BgL_optionals1142z00_2828, BgL_keys1143z00_2829,
				BgL_thezd2closurezd2global1144z00_2830, BgL_strength1145z00_2831,
				((BgL_nodez00_bglt) BgL_originalzd2body1146zd2_2832),
				BgL_recursivezd2calls1147zd2_2833, CBOOL(BgL_tailrec1148z00_2834));
		}

	}



/* isfun? */
	BGL_EXPORTED_DEF bool_t BGl_isfunzf3zf3zzinline_inlinez00(obj_t BgL_objz00_25)
	{
		{	/* Inline/inline.sch 61 */
			{	/* Inline/inline.sch 61 */
				obj_t BgL_classz00_3151;

				BgL_classz00_3151 = BGl_isfunz00zzinline_inlinez00;
				if (BGL_OBJECTP(BgL_objz00_25))
					{	/* Inline/inline.sch 61 */
						BgL_objectz00_bglt BgL_arg1807z00_3152;

						BgL_arg1807z00_3152 = (BgL_objectz00_bglt) (BgL_objz00_25);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Inline/inline.sch 61 */
								long BgL_idxz00_3153;

								BgL_idxz00_3153 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3152);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3153 + 4L)) == BgL_classz00_3151);
							}
						else
							{	/* Inline/inline.sch 61 */
								bool_t BgL_res1962z00_3156;

								{	/* Inline/inline.sch 61 */
									obj_t BgL_oclassz00_3157;

									{	/* Inline/inline.sch 61 */
										obj_t BgL_arg1815z00_3158;
										long BgL_arg1816z00_3159;

										BgL_arg1815z00_3158 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Inline/inline.sch 61 */
											long BgL_arg1817z00_3160;

											BgL_arg1817z00_3160 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3152);
											BgL_arg1816z00_3159 = (BgL_arg1817z00_3160 - OBJECT_TYPE);
										}
										BgL_oclassz00_3157 =
											VECTOR_REF(BgL_arg1815z00_3158, BgL_arg1816z00_3159);
									}
									{	/* Inline/inline.sch 61 */
										bool_t BgL__ortest_1115z00_3161;

										BgL__ortest_1115z00_3161 =
											(BgL_classz00_3151 == BgL_oclassz00_3157);
										if (BgL__ortest_1115z00_3161)
											{	/* Inline/inline.sch 61 */
												BgL_res1962z00_3156 = BgL__ortest_1115z00_3161;
											}
										else
											{	/* Inline/inline.sch 61 */
												long BgL_odepthz00_3162;

												{	/* Inline/inline.sch 61 */
													obj_t BgL_arg1804z00_3163;

													BgL_arg1804z00_3163 = (BgL_oclassz00_3157);
													BgL_odepthz00_3162 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3163);
												}
												if ((4L < BgL_odepthz00_3162))
													{	/* Inline/inline.sch 61 */
														obj_t BgL_arg1802z00_3164;

														{	/* Inline/inline.sch 61 */
															obj_t BgL_arg1803z00_3165;

															BgL_arg1803z00_3165 = (BgL_oclassz00_3157);
															BgL_arg1802z00_3164 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3165,
																4L);
														}
														BgL_res1962z00_3156 =
															(BgL_arg1802z00_3164 == BgL_classz00_3151);
													}
												else
													{	/* Inline/inline.sch 61 */
														BgL_res1962z00_3156 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1962z00_3156;
							}
					}
				else
					{	/* Inline/inline.sch 61 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &isfun? */
	obj_t BGl_z62isfunzf3z91zzinline_inlinez00(obj_t BgL_envz00_2835,
		obj_t BgL_objz00_2836)
	{
		{	/* Inline/inline.sch 61 */
			return BBOOL(BGl_isfunzf3zf3zzinline_inlinez00(BgL_objz00_2836));
		}

	}



/* isfun-nil */
	BGL_EXPORTED_DEF BgL_sfunz00_bglt BGl_isfunzd2nilzd2zzinline_inlinez00(void)
	{
		{	/* Inline/inline.sch 62 */
			{	/* Inline/inline.sch 62 */
				obj_t BgL_classz00_2236;

				BgL_classz00_2236 = BGl_isfunz00zzinline_inlinez00;
				{	/* Inline/inline.sch 62 */
					obj_t BgL__ortest_1117z00_2237;

					BgL__ortest_1117z00_2237 = BGL_CLASS_NIL(BgL_classz00_2236);
					if (CBOOL(BgL__ortest_1117z00_2237))
						{	/* Inline/inline.sch 62 */
							return ((BgL_sfunz00_bglt) BgL__ortest_1117z00_2237);
						}
					else
						{	/* Inline/inline.sch 62 */
							return
								((BgL_sfunz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2236));
						}
				}
			}
		}

	}



/* &isfun-nil */
	BgL_sfunz00_bglt BGl_z62isfunzd2nilzb0zzinline_inlinez00(obj_t
		BgL_envz00_2837)
	{
		{	/* Inline/inline.sch 62 */
			return BGl_isfunzd2nilzd2zzinline_inlinez00();
		}

	}



/* isfun-tailrec */
	BGL_EXPORTED_DEF bool_t
		BGl_isfunzd2tailreczd2zzinline_inlinez00(BgL_sfunz00_bglt BgL_oz00_26)
	{
		{	/* Inline/inline.sch 63 */
			{
				BgL_isfunz00_bglt BgL_auxz00_3501;

				{
					obj_t BgL_auxz00_3502;

					{	/* Inline/inline.sch 63 */
						BgL_objectz00_bglt BgL_tmpz00_3503;

						BgL_tmpz00_3503 = ((BgL_objectz00_bglt) BgL_oz00_26);
						BgL_auxz00_3502 = BGL_OBJECT_WIDENING(BgL_tmpz00_3503);
					}
					BgL_auxz00_3501 = ((BgL_isfunz00_bglt) BgL_auxz00_3502);
				}
				return (((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_3501))->BgL_tailrecz00);
			}
		}

	}



/* &isfun-tailrec */
	obj_t BGl_z62isfunzd2tailreczb0zzinline_inlinez00(obj_t BgL_envz00_2838,
		obj_t BgL_oz00_2839)
	{
		{	/* Inline/inline.sch 63 */
			return
				BBOOL(BGl_isfunzd2tailreczd2zzinline_inlinez00(
					((BgL_sfunz00_bglt) BgL_oz00_2839)));
		}

	}



/* isfun-tailrec-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2tailreczd2setz12z12zzinline_inlinez00(BgL_sfunz00_bglt
		BgL_oz00_27, bool_t BgL_vz00_28)
	{
		{	/* Inline/inline.sch 64 */
			{
				BgL_isfunz00_bglt BgL_auxz00_3511;

				{
					obj_t BgL_auxz00_3512;

					{	/* Inline/inline.sch 64 */
						BgL_objectz00_bglt BgL_tmpz00_3513;

						BgL_tmpz00_3513 = ((BgL_objectz00_bglt) BgL_oz00_27);
						BgL_auxz00_3512 = BGL_OBJECT_WIDENING(BgL_tmpz00_3513);
					}
					BgL_auxz00_3511 = ((BgL_isfunz00_bglt) BgL_auxz00_3512);
				}
				return
					((((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_3511))->BgL_tailrecz00) =
					((bool_t) BgL_vz00_28), BUNSPEC);
			}
		}

	}



/* &isfun-tailrec-set! */
	obj_t BGl_z62isfunzd2tailreczd2setz12z70zzinline_inlinez00(obj_t
		BgL_envz00_2840, obj_t BgL_oz00_2841, obj_t BgL_vz00_2842)
	{
		{	/* Inline/inline.sch 64 */
			return
				BGl_isfunzd2tailreczd2setz12z12zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2841), CBOOL(BgL_vz00_2842));
		}

	}



/* isfun-recursive-calls */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2recursivezd2callsz00zzinline_inlinez00(BgL_sfunz00_bglt
		BgL_oz00_29)
	{
		{	/* Inline/inline.sch 65 */
			{
				BgL_isfunz00_bglt BgL_auxz00_3521;

				{
					obj_t BgL_auxz00_3522;

					{	/* Inline/inline.sch 65 */
						BgL_objectz00_bglt BgL_tmpz00_3523;

						BgL_tmpz00_3523 = ((BgL_objectz00_bglt) BgL_oz00_29);
						BgL_auxz00_3522 = BGL_OBJECT_WIDENING(BgL_tmpz00_3523);
					}
					BgL_auxz00_3521 = ((BgL_isfunz00_bglt) BgL_auxz00_3522);
				}
				return
					(((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_3521))->
					BgL_recursivezd2callszd2);
			}
		}

	}



/* &isfun-recursive-calls */
	obj_t BGl_z62isfunzd2recursivezd2callsz62zzinline_inlinez00(obj_t
		BgL_envz00_2843, obj_t BgL_oz00_2844)
	{
		{	/* Inline/inline.sch 65 */
			return
				BGl_isfunzd2recursivezd2callsz00zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2844));
		}

	}



/* isfun-recursive-calls-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2recursivezd2callszd2setz12zc0zzinline_inlinez00(BgL_sfunz00_bglt
		BgL_oz00_30, obj_t BgL_vz00_31)
	{
		{	/* Inline/inline.sch 66 */
			{
				BgL_isfunz00_bglt BgL_auxz00_3530;

				{
					obj_t BgL_auxz00_3531;

					{	/* Inline/inline.sch 66 */
						BgL_objectz00_bglt BgL_tmpz00_3532;

						BgL_tmpz00_3532 = ((BgL_objectz00_bglt) BgL_oz00_30);
						BgL_auxz00_3531 = BGL_OBJECT_WIDENING(BgL_tmpz00_3532);
					}
					BgL_auxz00_3530 = ((BgL_isfunz00_bglt) BgL_auxz00_3531);
				}
				return
					((((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_3530))->
						BgL_recursivezd2callszd2) = ((obj_t) BgL_vz00_31), BUNSPEC);
			}
		}

	}



/* &isfun-recursive-calls-set! */
	obj_t BGl_z62isfunzd2recursivezd2callszd2setz12za2zzinline_inlinez00(obj_t
		BgL_envz00_2845, obj_t BgL_oz00_2846, obj_t BgL_vz00_2847)
	{
		{	/* Inline/inline.sch 66 */
			return
				BGl_isfunzd2recursivezd2callszd2setz12zc0zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2846), BgL_vz00_2847);
		}

	}



/* isfun-original-body */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_isfunzd2originalzd2bodyz00zzinline_inlinez00(BgL_sfunz00_bglt
		BgL_oz00_32)
	{
		{	/* Inline/inline.sch 67 */
			{
				BgL_isfunz00_bglt BgL_auxz00_3539;

				{
					obj_t BgL_auxz00_3540;

					{	/* Inline/inline.sch 67 */
						BgL_objectz00_bglt BgL_tmpz00_3541;

						BgL_tmpz00_3541 = ((BgL_objectz00_bglt) BgL_oz00_32);
						BgL_auxz00_3540 = BGL_OBJECT_WIDENING(BgL_tmpz00_3541);
					}
					BgL_auxz00_3539 = ((BgL_isfunz00_bglt) BgL_auxz00_3540);
				}
				return
					(((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_3539))->
					BgL_originalzd2bodyzd2);
			}
		}

	}



/* &isfun-original-body */
	BgL_nodez00_bglt BGl_z62isfunzd2originalzd2bodyz62zzinline_inlinez00(obj_t
		BgL_envz00_2848, obj_t BgL_oz00_2849)
	{
		{	/* Inline/inline.sch 67 */
			return
				BGl_isfunzd2originalzd2bodyz00zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2849));
		}

	}



/* isfun-strength */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2strengthzd2zzinline_inlinez00(BgL_sfunz00_bglt BgL_oz00_35)
	{
		{	/* Inline/inline.sch 69 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_35)))->BgL_strengthz00);
		}

	}



/* &isfun-strength */
	obj_t BGl_z62isfunzd2strengthzb0zzinline_inlinez00(obj_t BgL_envz00_2850,
		obj_t BgL_oz00_2851)
	{
		{	/* Inline/inline.sch 69 */
			return
				BGl_isfunzd2strengthzd2zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2851));
		}

	}



/* isfun-strength-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2strengthzd2setz12z12zzinline_inlinez00(BgL_sfunz00_bglt
		BgL_oz00_36, obj_t BgL_vz00_37)
	{
		{	/* Inline/inline.sch 70 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_36)))->BgL_strengthz00) =
				((obj_t) BgL_vz00_37), BUNSPEC);
		}

	}



/* &isfun-strength-set! */
	obj_t BGl_z62isfunzd2strengthzd2setz12z70zzinline_inlinez00(obj_t
		BgL_envz00_2852, obj_t BgL_oz00_2853, obj_t BgL_vz00_2854)
	{
		{	/* Inline/inline.sch 70 */
			return
				BGl_isfunzd2strengthzd2setz12z12zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2853), BgL_vz00_2854);
		}

	}



/* isfun-the-closure-global */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2thezd2closurezd2globalzd2zzinline_inlinez00(BgL_sfunz00_bglt
		BgL_oz00_38)
	{
		{	/* Inline/inline.sch 71 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_38)))->BgL_thezd2closurezd2globalz00);
		}

	}



/* &isfun-the-closure-global */
	obj_t BGl_z62isfunzd2thezd2closurezd2globalzb0zzinline_inlinez00(obj_t
		BgL_envz00_2855, obj_t BgL_oz00_2856)
	{
		{	/* Inline/inline.sch 71 */
			return
				BGl_isfunzd2thezd2closurezd2globalzd2zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2856));
		}

	}



/* isfun-the-closure-global-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2thezd2closurezd2globalzd2setz12z12zzinline_inlinez00
		(BgL_sfunz00_bglt BgL_oz00_39, obj_t BgL_vz00_40)
	{
		{	/* Inline/inline.sch 72 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_39)))->
					BgL_thezd2closurezd2globalz00) = ((obj_t) BgL_vz00_40), BUNSPEC);
		}

	}



/* &isfun-the-closure-global-set! */
	obj_t
		BGl_z62isfunzd2thezd2closurezd2globalzd2setz12z70zzinline_inlinez00(obj_t
		BgL_envz00_2857, obj_t BgL_oz00_2858, obj_t BgL_vz00_2859)
	{
		{	/* Inline/inline.sch 72 */
			return
				BGl_isfunzd2thezd2closurezd2globalzd2setz12z12zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2858), BgL_vz00_2859);
		}

	}



/* isfun-keys */
	BGL_EXPORTED_DEF obj_t BGl_isfunzd2keyszd2zzinline_inlinez00(BgL_sfunz00_bglt
		BgL_oz00_41)
	{
		{	/* Inline/inline.sch 73 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_41)))->BgL_keysz00);
		}

	}



/* &isfun-keys */
	obj_t BGl_z62isfunzd2keyszb0zzinline_inlinez00(obj_t BgL_envz00_2860,
		obj_t BgL_oz00_2861)
	{
		{	/* Inline/inline.sch 73 */
			return
				BGl_isfunzd2keyszd2zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2861));
		}

	}



/* isfun-optionals */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2optionalszd2zzinline_inlinez00(BgL_sfunz00_bglt BgL_oz00_44)
	{
		{	/* Inline/inline.sch 75 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_44)))->BgL_optionalsz00);
		}

	}



/* &isfun-optionals */
	obj_t BGl_z62isfunzd2optionalszb0zzinline_inlinez00(obj_t BgL_envz00_2862,
		obj_t BgL_oz00_2863)
	{
		{	/* Inline/inline.sch 75 */
			return
				BGl_isfunzd2optionalszd2zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2863));
		}

	}



/* isfun-loc */
	BGL_EXPORTED_DEF obj_t BGl_isfunzd2loczd2zzinline_inlinez00(BgL_sfunz00_bglt
		BgL_oz00_47)
	{
		{	/* Inline/inline.sch 77 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_47)))->BgL_locz00);
		}

	}



/* &isfun-loc */
	obj_t BGl_z62isfunzd2loczb0zzinline_inlinez00(obj_t BgL_envz00_2864,
		obj_t BgL_oz00_2865)
	{
		{	/* Inline/inline.sch 77 */
			return
				BGl_isfunzd2loczd2zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2865));
		}

	}



/* isfun-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2loczd2setz12z12zzinline_inlinez00(BgL_sfunz00_bglt BgL_oz00_48,
		obj_t BgL_vz00_49)
	{
		{	/* Inline/inline.sch 78 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_48)))->BgL_locz00) =
				((obj_t) BgL_vz00_49), BUNSPEC);
		}

	}



/* &isfun-loc-set! */
	obj_t BGl_z62isfunzd2loczd2setz12z70zzinline_inlinez00(obj_t BgL_envz00_2866,
		obj_t BgL_oz00_2867, obj_t BgL_vz00_2868)
	{
		{	/* Inline/inline.sch 78 */
			return
				BGl_isfunzd2loczd2setz12z12zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2867), BgL_vz00_2868);
		}

	}



/* isfun-dsssl-keywords */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2dssslzd2keywordsz00zzinline_inlinez00(BgL_sfunz00_bglt
		BgL_oz00_50)
	{
		{	/* Inline/inline.sch 79 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_50)))->BgL_dssslzd2keywordszd2);
		}

	}



/* &isfun-dsssl-keywords */
	obj_t BGl_z62isfunzd2dssslzd2keywordsz62zzinline_inlinez00(obj_t
		BgL_envz00_2869, obj_t BgL_oz00_2870)
	{
		{	/* Inline/inline.sch 79 */
			return
				BGl_isfunzd2dssslzd2keywordsz00zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2870));
		}

	}



/* isfun-dsssl-keywords-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2dssslzd2keywordszd2setz12zc0zzinline_inlinez00(BgL_sfunz00_bglt
		BgL_oz00_51, obj_t BgL_vz00_52)
	{
		{	/* Inline/inline.sch 80 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_51)))->BgL_dssslzd2keywordszd2) =
				((obj_t) BgL_vz00_52), BUNSPEC);
		}

	}



/* &isfun-dsssl-keywords-set! */
	obj_t BGl_z62isfunzd2dssslzd2keywordszd2setz12za2zzinline_inlinez00(obj_t
		BgL_envz00_2871, obj_t BgL_oz00_2872, obj_t BgL_vz00_2873)
	{
		{	/* Inline/inline.sch 80 */
			return
				BGl_isfunzd2dssslzd2keywordszd2setz12zc0zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2872), BgL_vz00_2873);
		}

	}



/* isfun-class */
	BGL_EXPORTED_DEF obj_t BGl_isfunzd2classzd2zzinline_inlinez00(BgL_sfunz00_bglt
		BgL_oz00_53)
	{
		{	/* Inline/inline.sch 81 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_53)))->BgL_classz00);
		}

	}



/* &isfun-class */
	obj_t BGl_z62isfunzd2classzb0zzinline_inlinez00(obj_t BgL_envz00_2874,
		obj_t BgL_oz00_2875)
	{
		{	/* Inline/inline.sch 81 */
			return
				BGl_isfunzd2classzd2zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2875));
		}

	}



/* isfun-class-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2classzd2setz12z12zzinline_inlinez00(BgL_sfunz00_bglt
		BgL_oz00_54, obj_t BgL_vz00_55)
	{
		{	/* Inline/inline.sch 82 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_54)))->BgL_classz00) =
				((obj_t) BgL_vz00_55), BUNSPEC);
		}

	}



/* &isfun-class-set! */
	obj_t BGl_z62isfunzd2classzd2setz12z70zzinline_inlinez00(obj_t
		BgL_envz00_2876, obj_t BgL_oz00_2877, obj_t BgL_vz00_2878)
	{
		{	/* Inline/inline.sch 82 */
			return
				BGl_isfunzd2classzd2setz12z12zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2877), BgL_vz00_2878);
		}

	}



/* isfun-body */
	BGL_EXPORTED_DEF obj_t BGl_isfunzd2bodyzd2zzinline_inlinez00(BgL_sfunz00_bglt
		BgL_oz00_56)
	{
		{	/* Inline/inline.sch 83 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_56)))->BgL_bodyz00);
		}

	}



/* &isfun-body */
	obj_t BGl_z62isfunzd2bodyzb0zzinline_inlinez00(obj_t BgL_envz00_2879,
		obj_t BgL_oz00_2880)
	{
		{	/* Inline/inline.sch 83 */
			return
				BGl_isfunzd2bodyzd2zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2880));
		}

	}



/* isfun-body-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2bodyzd2setz12z12zzinline_inlinez00(BgL_sfunz00_bglt BgL_oz00_57,
		obj_t BgL_vz00_58)
	{
		{	/* Inline/inline.sch 84 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_57)))->BgL_bodyz00) =
				((obj_t) BgL_vz00_58), BUNSPEC);
		}

	}



/* &isfun-body-set! */
	obj_t BGl_z62isfunzd2bodyzd2setz12z70zzinline_inlinez00(obj_t BgL_envz00_2881,
		obj_t BgL_oz00_2882, obj_t BgL_vz00_2883)
	{
		{	/* Inline/inline.sch 84 */
			return
				BGl_isfunzd2bodyzd2setz12z12zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2882), BgL_vz00_2883);
		}

	}



/* isfun-args-name */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2argszd2namez00zzinline_inlinez00(BgL_sfunz00_bglt BgL_oz00_59)
	{
		{	/* Inline/inline.sch 85 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_59)))->BgL_argszd2namezd2);
		}

	}



/* &isfun-args-name */
	obj_t BGl_z62isfunzd2argszd2namez62zzinline_inlinez00(obj_t BgL_envz00_2884,
		obj_t BgL_oz00_2885)
	{
		{	/* Inline/inline.sch 85 */
			return
				BGl_isfunzd2argszd2namez00zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2885));
		}

	}



/* isfun-args */
	BGL_EXPORTED_DEF obj_t BGl_isfunzd2argszd2zzinline_inlinez00(BgL_sfunz00_bglt
		BgL_oz00_62)
	{
		{	/* Inline/inline.sch 87 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_62)))->BgL_argsz00);
		}

	}



/* &isfun-args */
	obj_t BGl_z62isfunzd2argszb0zzinline_inlinez00(obj_t BgL_envz00_2886,
		obj_t BgL_oz00_2887)
	{
		{	/* Inline/inline.sch 87 */
			return
				BGl_isfunzd2argszd2zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2887));
		}

	}



/* isfun-args-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2argszd2setz12z12zzinline_inlinez00(BgL_sfunz00_bglt BgL_oz00_63,
		obj_t BgL_vz00_64)
	{
		{	/* Inline/inline.sch 88 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_63)))->BgL_argsz00) =
				((obj_t) BgL_vz00_64), BUNSPEC);
		}

	}



/* &isfun-args-set! */
	obj_t BGl_z62isfunzd2argszd2setz12z70zzinline_inlinez00(obj_t BgL_envz00_2888,
		obj_t BgL_oz00_2889, obj_t BgL_vz00_2890)
	{
		{	/* Inline/inline.sch 88 */
			return
				BGl_isfunzd2argszd2setz12z12zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2889), BgL_vz00_2890);
		}

	}



/* isfun-property */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2propertyzd2zzinline_inlinez00(BgL_sfunz00_bglt BgL_oz00_65)
	{
		{	/* Inline/inline.sch 89 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_65)))->BgL_propertyz00);
		}

	}



/* &isfun-property */
	obj_t BGl_z62isfunzd2propertyzb0zzinline_inlinez00(obj_t BgL_envz00_2891,
		obj_t BgL_oz00_2892)
	{
		{	/* Inline/inline.sch 89 */
			return
				BGl_isfunzd2propertyzd2zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2892));
		}

	}



/* isfun-property-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2propertyzd2setz12z12zzinline_inlinez00(BgL_sfunz00_bglt
		BgL_oz00_66, obj_t BgL_vz00_67)
	{
		{	/* Inline/inline.sch 90 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_66)))->BgL_propertyz00) =
				((obj_t) BgL_vz00_67), BUNSPEC);
		}

	}



/* &isfun-property-set! */
	obj_t BGl_z62isfunzd2propertyzd2setz12z70zzinline_inlinez00(obj_t
		BgL_envz00_2893, obj_t BgL_oz00_2894, obj_t BgL_vz00_2895)
	{
		{	/* Inline/inline.sch 90 */
			return
				BGl_isfunzd2propertyzd2setz12z12zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2894), BgL_vz00_2895);
		}

	}



/* isfun-failsafe */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2failsafezd2zzinline_inlinez00(BgL_sfunz00_bglt BgL_oz00_68)
	{
		{	/* Inline/inline.sch 91 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_68)))->BgL_failsafez00);
		}

	}



/* &isfun-failsafe */
	obj_t BGl_z62isfunzd2failsafezb0zzinline_inlinez00(obj_t BgL_envz00_2896,
		obj_t BgL_oz00_2897)
	{
		{	/* Inline/inline.sch 91 */
			return
				BGl_isfunzd2failsafezd2zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2897));
		}

	}



/* isfun-failsafe-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2failsafezd2setz12z12zzinline_inlinez00(BgL_sfunz00_bglt
		BgL_oz00_69, obj_t BgL_vz00_70)
	{
		{	/* Inline/inline.sch 92 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_69)))->BgL_failsafez00) =
				((obj_t) BgL_vz00_70), BUNSPEC);
		}

	}



/* &isfun-failsafe-set! */
	obj_t BGl_z62isfunzd2failsafezd2setz12z70zzinline_inlinez00(obj_t
		BgL_envz00_2898, obj_t BgL_oz00_2899, obj_t BgL_vz00_2900)
	{
		{	/* Inline/inline.sch 92 */
			return
				BGl_isfunzd2failsafezd2setz12z12zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2899), BgL_vz00_2900);
		}

	}



/* isfun-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2effectzd2zzinline_inlinez00(BgL_sfunz00_bglt BgL_oz00_71)
	{
		{	/* Inline/inline.sch 93 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_71)))->BgL_effectz00);
		}

	}



/* &isfun-effect */
	obj_t BGl_z62isfunzd2effectzb0zzinline_inlinez00(obj_t BgL_envz00_2901,
		obj_t BgL_oz00_2902)
	{
		{	/* Inline/inline.sch 93 */
			return
				BGl_isfunzd2effectzd2zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2902));
		}

	}



/* isfun-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2effectzd2setz12z12zzinline_inlinez00(BgL_sfunz00_bglt
		BgL_oz00_72, obj_t BgL_vz00_73)
	{
		{	/* Inline/inline.sch 94 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_72)))->BgL_effectz00) =
				((obj_t) BgL_vz00_73), BUNSPEC);
		}

	}



/* &isfun-effect-set! */
	obj_t BGl_z62isfunzd2effectzd2setz12z70zzinline_inlinez00(obj_t
		BgL_envz00_2903, obj_t BgL_oz00_2904, obj_t BgL_vz00_2905)
	{
		{	/* Inline/inline.sch 94 */
			return
				BGl_isfunzd2effectzd2setz12z12zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2904), BgL_vz00_2905);
		}

	}



/* isfun-the-closure */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2thezd2closurez00zzinline_inlinez00(BgL_sfunz00_bglt BgL_oz00_74)
	{
		{	/* Inline/inline.sch 95 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_74)))->BgL_thezd2closurezd2);
		}

	}



/* &isfun-the-closure */
	obj_t BGl_z62isfunzd2thezd2closurez62zzinline_inlinez00(obj_t BgL_envz00_2906,
		obj_t BgL_oz00_2907)
	{
		{	/* Inline/inline.sch 95 */
			return
				BGl_isfunzd2thezd2closurez00zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2907));
		}

	}



/* isfun-the-closure-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2thezd2closurezd2setz12zc0zzinline_inlinez00(BgL_sfunz00_bglt
		BgL_oz00_75, obj_t BgL_vz00_76)
	{
		{	/* Inline/inline.sch 96 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_75)))->BgL_thezd2closurezd2) =
				((obj_t) BgL_vz00_76), BUNSPEC);
		}

	}



/* &isfun-the-closure-set! */
	obj_t BGl_z62isfunzd2thezd2closurezd2setz12za2zzinline_inlinez00(obj_t
		BgL_envz00_2908, obj_t BgL_oz00_2909, obj_t BgL_vz00_2910)
	{
		{	/* Inline/inline.sch 96 */
			return
				BGl_isfunzd2thezd2closurezd2setz12zc0zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2909), BgL_vz00_2910);
		}

	}



/* isfun-top? */
	BGL_EXPORTED_DEF bool_t
		BGl_isfunzd2topzf3z21zzinline_inlinez00(BgL_sfunz00_bglt BgL_oz00_77)
	{
		{	/* Inline/inline.sch 97 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_77)))->BgL_topzf3zf3);
		}

	}



/* &isfun-top? */
	obj_t BGl_z62isfunzd2topzf3z43zzinline_inlinez00(obj_t BgL_envz00_2911,
		obj_t BgL_oz00_2912)
	{
		{	/* Inline/inline.sch 97 */
			return
				BBOOL(BGl_isfunzd2topzf3z21zzinline_inlinez00(
					((BgL_sfunz00_bglt) BgL_oz00_2912)));
		}

	}



/* isfun-top?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2topzf3zd2setz12ze1zzinline_inlinez00(BgL_sfunz00_bglt
		BgL_oz00_78, bool_t BgL_vz00_79)
	{
		{	/* Inline/inline.sch 98 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_78)))->BgL_topzf3zf3) =
				((bool_t) BgL_vz00_79), BUNSPEC);
		}

	}



/* &isfun-top?-set! */
	obj_t BGl_z62isfunzd2topzf3zd2setz12z83zzinline_inlinez00(obj_t
		BgL_envz00_2913, obj_t BgL_oz00_2914, obj_t BgL_vz00_2915)
	{
		{	/* Inline/inline.sch 98 */
			return
				BGl_isfunzd2topzf3zd2setz12ze1zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2914), CBOOL(BgL_vz00_2915));
		}

	}



/* isfun-stack-allocator */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2stackzd2allocatorz00zzinline_inlinez00(BgL_sfunz00_bglt
		BgL_oz00_80)
	{
		{	/* Inline/inline.sch 99 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_80)))->BgL_stackzd2allocatorzd2);
		}

	}



/* &isfun-stack-allocator */
	obj_t BGl_z62isfunzd2stackzd2allocatorz62zzinline_inlinez00(obj_t
		BgL_envz00_2916, obj_t BgL_oz00_2917)
	{
		{	/* Inline/inline.sch 99 */
			return
				BGl_isfunzd2stackzd2allocatorz00zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2917));
		}

	}



/* isfun-stack-allocator-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2stackzd2allocatorzd2setz12zc0zzinline_inlinez00(BgL_sfunz00_bglt
		BgL_oz00_81, obj_t BgL_vz00_82)
	{
		{	/* Inline/inline.sch 100 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_81)))->BgL_stackzd2allocatorzd2) =
				((obj_t) BgL_vz00_82), BUNSPEC);
		}

	}



/* &isfun-stack-allocator-set! */
	obj_t BGl_z62isfunzd2stackzd2allocatorzd2setz12za2zzinline_inlinez00(obj_t
		BgL_envz00_2918, obj_t BgL_oz00_2919, obj_t BgL_vz00_2920)
	{
		{	/* Inline/inline.sch 100 */
			return
				BGl_isfunzd2stackzd2allocatorzd2setz12zc0zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2919), BgL_vz00_2920);
		}

	}



/* isfun-predicate-of */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2predicatezd2ofz00zzinline_inlinez00(BgL_sfunz00_bglt
		BgL_oz00_83)
	{
		{	/* Inline/inline.sch 101 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_83)))->BgL_predicatezd2ofzd2);
		}

	}



/* &isfun-predicate-of */
	obj_t BGl_z62isfunzd2predicatezd2ofz62zzinline_inlinez00(obj_t
		BgL_envz00_2921, obj_t BgL_oz00_2922)
	{
		{	/* Inline/inline.sch 101 */
			return
				BGl_isfunzd2predicatezd2ofz00zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2922));
		}

	}



/* isfun-predicate-of-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2predicatezd2ofzd2setz12zc0zzinline_inlinez00(BgL_sfunz00_bglt
		BgL_oz00_84, obj_t BgL_vz00_85)
	{
		{	/* Inline/inline.sch 102 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_84)))->BgL_predicatezd2ofzd2) =
				((obj_t) BgL_vz00_85), BUNSPEC);
		}

	}



/* &isfun-predicate-of-set! */
	obj_t BGl_z62isfunzd2predicatezd2ofzd2setz12za2zzinline_inlinez00(obj_t
		BgL_envz00_2923, obj_t BgL_oz00_2924, obj_t BgL_vz00_2925)
	{
		{	/* Inline/inline.sch 102 */
			return
				BGl_isfunzd2predicatezd2ofzd2setz12zc0zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2924), BgL_vz00_2925);
		}

	}



/* isfun-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2sidezd2effectz00zzinline_inlinez00(BgL_sfunz00_bglt BgL_oz00_86)
	{
		{	/* Inline/inline.sch 103 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_86)))->BgL_sidezd2effectzd2);
		}

	}



/* &isfun-side-effect */
	obj_t BGl_z62isfunzd2sidezd2effectz62zzinline_inlinez00(obj_t BgL_envz00_2926,
		obj_t BgL_oz00_2927)
	{
		{	/* Inline/inline.sch 103 */
			return
				BGl_isfunzd2sidezd2effectz00zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2927));
		}

	}



/* isfun-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_isfunzd2sidezd2effectzd2setz12zc0zzinline_inlinez00(BgL_sfunz00_bglt
		BgL_oz00_87, obj_t BgL_vz00_88)
	{
		{	/* Inline/inline.sch 104 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_87)))->BgL_sidezd2effectzd2) =
				((obj_t) BgL_vz00_88), BUNSPEC);
		}

	}



/* &isfun-side-effect-set! */
	obj_t BGl_z62isfunzd2sidezd2effectzd2setz12za2zzinline_inlinez00(obj_t
		BgL_envz00_2928, obj_t BgL_oz00_2929, obj_t BgL_vz00_2930)
	{
		{	/* Inline/inline.sch 104 */
			return
				BGl_isfunzd2sidezd2effectzd2setz12zc0zzinline_inlinez00(
				((BgL_sfunz00_bglt) BgL_oz00_2929), BgL_vz00_2930);
		}

	}



/* isfun-arity */
	BGL_EXPORTED_DEF long BGl_isfunzd2arityzd2zzinline_inlinez00(BgL_sfunz00_bglt
		BgL_oz00_89)
	{
		{	/* Inline/inline.sch 105 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_89)))->BgL_arityz00);
		}

	}



/* &isfun-arity */
	obj_t BGl_z62isfunzd2arityzb0zzinline_inlinez00(obj_t BgL_envz00_2931,
		obj_t BgL_oz00_2932)
	{
		{	/* Inline/inline.sch 105 */
			return
				BINT(BGl_isfunzd2arityzd2zzinline_inlinez00(
					((BgL_sfunz00_bglt) BgL_oz00_2932)));
		}

	}



/* inline-sfun! */
	BGL_EXPORTED_DEF obj_t
		BGl_inlinezd2sfunz12zc0zzinline_inlinez00(BgL_variablez00_bglt
		BgL_variablez00_92, long BgL_kfactorz00_93, obj_t BgL_stackz00_94)
	{
		{	/* Inline/inline.scm 38 */
			{	/* Inline/inline.scm 42 */
				BgL_valuez00_bglt BgL_sfunz00_1527;

				BgL_sfunz00_1527 =
					(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_92))->BgL_valuez00);
				{	/* Inline/inline.scm 42 */
					BgL_sfunz00_bglt BgL_isfunz00_1528;

					{	/* Inline/inline.scm 43 */
						obj_t BgL_classz00_2244;

						BgL_classz00_2244 = BGl_isfunz00zzinline_inlinez00;
						((bool_t) 0);
					}
					{	/* Inline/inline.scm 45 */
						BgL_isfunz00_bglt BgL_wide1153z00_1563;

						BgL_wide1153z00_1563 =
							((BgL_isfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_isfunz00_bgl))));
						{	/* Inline/inline.scm 45 */
							obj_t BgL_auxz00_3693;
							BgL_objectz00_bglt BgL_tmpz00_3689;

							BgL_auxz00_3693 = ((obj_t) BgL_wide1153z00_1563);
							BgL_tmpz00_3689 =
								((BgL_objectz00_bglt)
								((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_sfunz00_1527)));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3689, BgL_auxz00_3693);
						}
						((BgL_objectz00_bglt)
							((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_sfunz00_1527)));
						{	/* Inline/inline.scm 45 */
							long BgL_arg1516z00_1564;

							BgL_arg1516z00_1564 =
								BGL_CLASS_NUM(BGl_isfunz00zzinline_inlinez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_sfunz00_bglt)
										((BgL_sfunz00_bglt) BgL_sfunz00_1527))),
								BgL_arg1516z00_1564);
						}
						((BgL_sfunz00_bglt)
							((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_sfunz00_1527)));
					}
					{
						BgL_isfunz00_bglt BgL_auxz00_3707;

						{
							obj_t BgL_auxz00_3708;

							{	/* Inline/inline.scm 45 */
								BgL_objectz00_bglt BgL_tmpz00_3709;

								BgL_tmpz00_3709 =
									((BgL_objectz00_bglt)
									((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_sfunz00_1527)));
								BgL_auxz00_3708 = BGL_OBJECT_WIDENING(BgL_tmpz00_3709);
							}
							BgL_auxz00_3707 = ((BgL_isfunz00_bglt) BgL_auxz00_3708);
						}
						((((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_3707))->
								BgL_originalzd2bodyzd2) =
							((BgL_nodez00_bglt) ((BgL_nodez00_bglt) (((BgL_sfunz00_bglt)
											COBJECT(((BgL_sfunz00_bglt) BgL_sfunz00_1527)))->
										BgL_bodyz00))), BUNSPEC);
					}
					{
						BgL_isfunz00_bglt BgL_auxz00_3719;

						{
							obj_t BgL_auxz00_3720;

							{	/* Inline/inline.scm 45 */
								BgL_objectz00_bglt BgL_tmpz00_3721;

								BgL_tmpz00_3721 =
									((BgL_objectz00_bglt)
									((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_sfunz00_1527)));
								BgL_auxz00_3720 = BGL_OBJECT_WIDENING(BgL_tmpz00_3721);
							}
							BgL_auxz00_3719 = ((BgL_isfunz00_bglt) BgL_auxz00_3720);
						}
						((((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_3719))->
								BgL_recursivezd2callszd2) = ((obj_t) BUNSPEC), BUNSPEC);
					}
					{
						BgL_isfunz00_bglt BgL_auxz00_3728;

						{
							obj_t BgL_auxz00_3729;

							{	/* Inline/inline.scm 45 */
								BgL_objectz00_bglt BgL_tmpz00_3730;

								BgL_tmpz00_3730 =
									((BgL_objectz00_bglt)
									((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_sfunz00_1527)));
								BgL_auxz00_3729 = BGL_OBJECT_WIDENING(BgL_tmpz00_3730);
							}
							BgL_auxz00_3728 = ((BgL_isfunz00_bglt) BgL_auxz00_3729);
						}
						((((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_3728))->BgL_tailrecz00) =
							((bool_t) ((bool_t) 1)), BUNSPEC);
					}
					BgL_isfunz00_1528 =
						((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_sfunz00_1527));
					{	/* Inline/inline.scm 43 */
						BgL_nodez00_bglt BgL_ozd2bodyzd2_1529;

						{
							BgL_isfunz00_bglt BgL_auxz00_3739;

							{
								obj_t BgL_auxz00_3740;

								{	/* Inline/inline.scm 46 */
									BgL_objectz00_bglt BgL_tmpz00_3741;

									BgL_tmpz00_3741 = ((BgL_objectz00_bglt) BgL_isfunz00_1528);
									BgL_auxz00_3740 = BGL_OBJECT_WIDENING(BgL_tmpz00_3741);
								}
								BgL_auxz00_3739 = ((BgL_isfunz00_bglt) BgL_auxz00_3740);
							}
							BgL_ozd2bodyzd2_1529 =
								(((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_3739))->
								BgL_originalzd2bodyzd2);
						}
						{	/* Inline/inline.scm 46 */
							BgL_nodez00_bglt BgL_inlzd2bodyzd2_1530;

							{	/* Inline/inline.scm 47 */
								bool_t BgL_test2128z00_3746;

								{	/* Inline/inline.scm 48 */
									long BgL_arg1509z00_1557;

									BgL_arg1509z00_1557 =
										(1L +
										bgl_list_length(
											(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_sfunz00_1527)))->
												BgL_argsz00)));
									BgL_test2128z00_3746 =
										BGl_inlinezd2appzf3z21zzinline_appz00(BgL_variablez00_92,
										(long) CINT(BGl_za2kfactorza2z00zzinline_walkz00),
										BgL_arg1509z00_1557, BNIL);
								}
								if (BgL_test2128z00_3746)
									{	/* Inline/inline.scm 47 */
										BgL_inlzd2bodyzd2_1530 =
											BGl_alphatiza7eza7zzast_alphatiza7eza7(BNIL, BNIL, BFALSE,
											BgL_ozd2bodyzd2_1529);
									}
								else
									{	/* Inline/inline.scm 47 */
										BgL_inlzd2bodyzd2_1530 = BgL_ozd2bodyzd2_1529;
									}
							}
							{	/* Inline/inline.scm 47 */

								{	/* Inline/inline.scm 57 */
									BgL_nodez00_bglt BgL_arg1375z00_1531;

									{	/* Inline/inline.scm 57 */
										bool_t BgL_test2129z00_3754;

										if (
											(BGl_za2inlinezd2modeza2zd2zzinline_walkz00 ==
												CNST_TABLE_REF(0)))
											{	/* Inline/inline.scm 57 */
												BgL_test2129z00_3754 = ((bool_t) 1);
											}
										else
											{	/* Inline/inline.scm 58 */
												bool_t BgL_test2131z00_3758;

												if (
													((((BgL_variablez00_bglt)
																COBJECT(BgL_variablez00_92))->
															BgL_occurrencez00) > 0L))
													{	/* Inline/inline.scm 58 */
														BgL_test2131z00_3758 = ((bool_t) 1);
													}
												else
													{	/* Inline/inline.scm 58 */
														BgL_test2131z00_3758 =
															(
															(((BgL_variablez00_bglt)
																	COBJECT(BgL_variablez00_92))->
																BgL_removablez00) == CNST_TABLE_REF(1));
													}
												if (BgL_test2131z00_3758)
													{	/* Inline/inline.scm 58 */
														BgL_test2129z00_3754 = ((bool_t) 1);
													}
												else
													{	/* Inline/inline.scm 60 */
														bool_t BgL_test2133z00_3765;

														{	/* Inline/inline.scm 60 */
															obj_t BgL_classz00_2293;

															BgL_classz00_2293 = BGl_globalz00zzast_varz00;
															{	/* Inline/inline.scm 60 */
																BgL_objectz00_bglt BgL_arg1807z00_2295;

																{	/* Inline/inline.scm 60 */
																	obj_t BgL_tmpz00_3766;

																	BgL_tmpz00_3766 =
																		((obj_t) BgL_variablez00_92);
																	BgL_arg1807z00_2295 =
																		(BgL_objectz00_bglt) (BgL_tmpz00_3766);
																}
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Inline/inline.scm 60 */
																		long BgL_idxz00_2301;

																		BgL_idxz00_2301 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_2295);
																		BgL_test2133z00_3765 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_2301 + 2L)) ==
																			BgL_classz00_2293);
																	}
																else
																	{	/* Inline/inline.scm 60 */
																		bool_t BgL_res1964z00_2326;

																		{	/* Inline/inline.scm 60 */
																			obj_t BgL_oclassz00_2309;

																			{	/* Inline/inline.scm 60 */
																				obj_t BgL_arg1815z00_2317;
																				long BgL_arg1816z00_2318;

																				BgL_arg1815z00_2317 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Inline/inline.scm 60 */
																					long BgL_arg1817z00_2319;

																					BgL_arg1817z00_2319 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_2295);
																					BgL_arg1816z00_2318 =
																						(BgL_arg1817z00_2319 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_2309 =
																					VECTOR_REF(BgL_arg1815z00_2317,
																					BgL_arg1816z00_2318);
																			}
																			{	/* Inline/inline.scm 60 */
																				bool_t BgL__ortest_1115z00_2310;

																				BgL__ortest_1115z00_2310 =
																					(BgL_classz00_2293 ==
																					BgL_oclassz00_2309);
																				if (BgL__ortest_1115z00_2310)
																					{	/* Inline/inline.scm 60 */
																						BgL_res1964z00_2326 =
																							BgL__ortest_1115z00_2310;
																					}
																				else
																					{	/* Inline/inline.scm 60 */
																						long BgL_odepthz00_2311;

																						{	/* Inline/inline.scm 60 */
																							obj_t BgL_arg1804z00_2312;

																							BgL_arg1804z00_2312 =
																								(BgL_oclassz00_2309);
																							BgL_odepthz00_2311 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_2312);
																						}
																						if ((2L < BgL_odepthz00_2311))
																							{	/* Inline/inline.scm 60 */
																								obj_t BgL_arg1802z00_2314;

																								{	/* Inline/inline.scm 60 */
																									obj_t BgL_arg1803z00_2315;

																									BgL_arg1803z00_2315 =
																										(BgL_oclassz00_2309);
																									BgL_arg1802z00_2314 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_2315, 2L);
																								}
																								BgL_res1964z00_2326 =
																									(BgL_arg1802z00_2314 ==
																									BgL_classz00_2293);
																							}
																						else
																							{	/* Inline/inline.scm 60 */
																								BgL_res1964z00_2326 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2133z00_3765 = BgL_res1964z00_2326;
																	}
															}
														}
														if (BgL_test2133z00_3765)
															{	/* Inline/inline.scm 60 */
																if (
																	((((BgL_globalz00_bglt) COBJECT(
																					((BgL_globalz00_bglt)
																						BgL_variablez00_92)))->
																			BgL_importz00) == CNST_TABLE_REF(2)))
																	{	/* Inline/inline.scm 61 */
																		BgL_test2129z00_3754 = ((bool_t) 0);
																	}
																else
																	{	/* Inline/inline.scm 61 */
																		BgL_test2129z00_3754 = ((bool_t) 1);
																	}
															}
														else
															{	/* Inline/inline.scm 60 */
																BgL_test2129z00_3754 = ((bool_t) 0);
															}
													}
											}
										if (BgL_test2129z00_3754)
											{	/* Inline/inline.scm 62 */
												obj_t BgL_arg1453z00_1545;

												BgL_arg1453z00_1545 =
													MAKE_YOUNG_PAIR(
													((obj_t) BgL_variablez00_92), BgL_stackz00_94);
												BgL_arg1375z00_1531 =
													BGl_inlinezd2nodezd2zzinline_inlinez00
													(BgL_inlzd2bodyzd2_1530, BgL_kfactorz00_93,
													BgL_arg1453z00_1545);
											}
										else
											{	/* Inline/inline.scm 57 */
												BgL_arg1375z00_1531 = BgL_inlzd2bodyzd2_1530;
											}
									}
									((((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_sfunz00_1527)))->
											BgL_bodyz00) =
										((obj_t) ((obj_t) BgL_arg1375z00_1531)), BUNSPEC);
								}
							}
						}
					}
				}
			}
			return BNIL;
		}

	}



/* &inline-sfun! */
	obj_t BGl_z62inlinezd2sfunz12za2zzinline_inlinez00(obj_t BgL_envz00_2933,
		obj_t BgL_variablez00_2934, obj_t BgL_kfactorz00_2935,
		obj_t BgL_stackz00_2936)
	{
		{	/* Inline/inline.scm 38 */
			return
				BGl_inlinezd2sfunz12zc0zzinline_inlinez00(
				((BgL_variablez00_bglt) BgL_variablez00_2934),
				(long) CINT(BgL_kfactorz00_2935), BgL_stackz00_2936);
		}

	}



/* inline-node*! */
	BGL_EXPORTED_DEF obj_t BGl_inlinezd2nodeza2z12z62zzinline_inlinez00(obj_t
		BgL_nodeza2za2_167, obj_t BgL_kfactorz00_168, obj_t BgL_stackz00_169)
	{
		{	/* Inline/inline.scm 294 */
		BGl_inlinezd2nodeza2z12z62zzinline_inlinez00:
			if (NULLP(BgL_nodeza2za2_167))
				{	/* Inline/inline.scm 295 */
					return CNST_TABLE_REF(3);
				}
			else
				{	/* Inline/inline.scm 295 */
					{	/* Inline/inline.scm 298 */
						BgL_nodez00_bglt BgL_arg1535z00_1567;

						{	/* Inline/inline.scm 298 */
							obj_t BgL_arg1540z00_1568;

							BgL_arg1540z00_1568 = CAR(((obj_t) BgL_nodeza2za2_167));
							BgL_arg1535z00_1567 =
								BGl_inlinezd2nodezd2zzinline_inlinez00(
								((BgL_nodez00_bglt) BgL_arg1540z00_1568),
								(long) CINT(BgL_kfactorz00_168), BgL_stackz00_169);
						}
						{	/* Inline/inline.scm 298 */
							obj_t BgL_auxz00_3812;
							obj_t BgL_tmpz00_3810;

							BgL_auxz00_3812 = ((obj_t) BgL_arg1535z00_1567);
							BgL_tmpz00_3810 = ((obj_t) BgL_nodeza2za2_167);
							SET_CAR(BgL_tmpz00_3810, BgL_auxz00_3812);
					}}
					{	/* Inline/inline.scm 299 */
						obj_t BgL_arg1544z00_1569;

						BgL_arg1544z00_1569 = CDR(((obj_t) BgL_nodeza2za2_167));
						{
							obj_t BgL_nodeza2za2_3817;

							BgL_nodeza2za2_3817 = BgL_arg1544z00_1569;
							BgL_nodeza2za2_167 = BgL_nodeza2za2_3817;
							goto BGl_inlinezd2nodeza2z12z62zzinline_inlinez00;
						}
					}
				}
		}

	}



/* &inline-node*! */
	obj_t BGl_z62inlinezd2nodeza2z12z00zzinline_inlinez00(obj_t BgL_envz00_2937,
		obj_t BgL_nodeza2za2_2938, obj_t BgL_kfactorz00_2939,
		obj_t BgL_stackz00_2940)
	{
		{	/* Inline/inline.scm 294 */
			return
				BGl_inlinezd2nodeza2z12z62zzinline_inlinez00(BgL_nodeza2za2_2938,
				BgL_kfactorz00_2939, BgL_stackz00_2940);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzinline_inlinez00(void)
	{
		{	/* Inline/inline.scm 15 */
			{	/* Inline/inline.scm 28 */
				obj_t BgL_arg1561z00_1574;
				obj_t BgL_arg1564z00_1575;

				{	/* Inline/inline.scm 28 */
					obj_t BgL_v1307z00_1621;

					BgL_v1307z00_1621 = create_vector(3L);
					{	/* Inline/inline.scm 28 */
						obj_t BgL_arg1591z00_1622;

						BgL_arg1591z00_1622 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc1985z00zzinline_inlinez00,
							BGl_proc1984z00zzinline_inlinez00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, BGl_nodez00zzast_nodez00);
						VECTOR_SET(BgL_v1307z00_1621, 0L, BgL_arg1591z00_1622);
					}
					{	/* Inline/inline.scm 28 */
						obj_t BgL_arg1602z00_1632;

						BgL_arg1602z00_1632 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(5),
							BGl_proc1988z00zzinline_inlinez00,
							BGl_proc1987z00zzinline_inlinez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1986z00zzinline_inlinez00, CNST_TABLE_REF(6));
						VECTOR_SET(BgL_v1307z00_1621, 1L, BgL_arg1602z00_1632);
					}
					{	/* Inline/inline.scm 28 */
						obj_t BgL_arg1613z00_1645;

						BgL_arg1613z00_1645 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc1991z00zzinline_inlinez00,
							BGl_proc1990z00zzinline_inlinez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1989z00zzinline_inlinez00, CNST_TABLE_REF(8));
						VECTOR_SET(BgL_v1307z00_1621, 2L, BgL_arg1613z00_1645);
					}
					BgL_arg1561z00_1574 = BgL_v1307z00_1621;
				}
				{	/* Inline/inline.scm 28 */
					obj_t BgL_v1308z00_1658;

					BgL_v1308z00_1658 = create_vector(0L);
					BgL_arg1564z00_1575 = BgL_v1308z00_1658;
				}
				return (BGl_isfunz00zzinline_inlinez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(9),
						CNST_TABLE_REF(10), BGl_sfunz00zzast_varz00, 31916L,
						BGl_proc1995z00zzinline_inlinez00,
						BGl_proc1994z00zzinline_inlinez00, BFALSE,
						BGl_proc1993z00zzinline_inlinez00,
						BGl_proc1992z00zzinline_inlinez00, BgL_arg1561z00_1574,
						BgL_arg1564z00_1575), BUNSPEC);
			}
		}

	}



/* &lambda1577 */
	BgL_sfunz00_bglt BGl_z62lambda1577z62zzinline_inlinez00(obj_t BgL_envz00_2953,
		obj_t BgL_o1132z00_2954)
	{
		{	/* Inline/inline.scm 28 */
			{	/* Inline/inline.scm 28 */
				long BgL_arg1584z00_3167;

				{	/* Inline/inline.scm 28 */
					obj_t BgL_arg1585z00_3168;

					{	/* Inline/inline.scm 28 */
						obj_t BgL_arg1589z00_3169;

						{	/* Inline/inline.scm 28 */
							obj_t BgL_arg1815z00_3170;
							long BgL_arg1816z00_3171;

							BgL_arg1815z00_3170 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Inline/inline.scm 28 */
								long BgL_arg1817z00_3172;

								BgL_arg1817z00_3172 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_sfunz00_bglt) BgL_o1132z00_2954)));
								BgL_arg1816z00_3171 = (BgL_arg1817z00_3172 - OBJECT_TYPE);
							}
							BgL_arg1589z00_3169 =
								VECTOR_REF(BgL_arg1815z00_3170, BgL_arg1816z00_3171);
						}
						BgL_arg1585z00_3168 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1589z00_3169);
					}
					{	/* Inline/inline.scm 28 */
						obj_t BgL_tmpz00_3842;

						BgL_tmpz00_3842 = ((obj_t) BgL_arg1585z00_3168);
						BgL_arg1584z00_3167 = BGL_CLASS_NUM(BgL_tmpz00_3842);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_sfunz00_bglt) BgL_o1132z00_2954)), BgL_arg1584z00_3167);
			}
			{	/* Inline/inline.scm 28 */
				BgL_objectz00_bglt BgL_tmpz00_3848;

				BgL_tmpz00_3848 =
					((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_o1132z00_2954));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3848, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_o1132z00_2954));
			return ((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_o1132z00_2954));
		}

	}



/* &<@anonymous:1576> */
	obj_t BGl_z62zc3z04anonymousza31576ze3ze5zzinline_inlinez00(obj_t
		BgL_envz00_2955, obj_t BgL_new1131z00_2956)
	{
		{	/* Inline/inline.scm 28 */
			{
				BgL_sfunz00_bglt BgL_auxz00_3856;

				((((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt)
									((BgL_sfunz00_bglt) BgL_new1131z00_2956))))->BgL_arityz00) =
					((long) 0L), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1131z00_2956))))->BgL_sidezd2effectzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1131z00_2956))))->BgL_predicatezd2ofzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1131z00_2956))))->BgL_stackzd2allocatorzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1131z00_2956))))->BgL_topzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1131z00_2956))))->BgL_thezd2closurezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1131z00_2956))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1131z00_2956))))->BgL_failsafez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1131z00_2956))))->BgL_argszd2noescapezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1131z00_2956))))->BgL_argszd2retescapezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1131z00_2956))))->BgL_propertyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1131z00_2956))))->BgL_argsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1131z00_2956))))->BgL_argszd2namezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1131z00_2956))))->BgL_bodyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1131z00_2956))))->BgL_classz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1131z00_2956))))->BgL_dssslzd2keywordszd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1131z00_2956))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1131z00_2956))))->BgL_optionalsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1131z00_2956))))->BgL_keysz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1131z00_2956))))->BgL_thezd2closurezd2globalz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1131z00_2956))))->BgL_strengthz00) =
					((obj_t) CNST_TABLE_REF(11)), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1131z00_2956))))->BgL_stackablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_nodez00_bglt BgL_auxz00_3931;
					BgL_isfunz00_bglt BgL_auxz00_3924;

					{	/* Inline/inline.scm 28 */
						obj_t BgL_classz00_3174;

						BgL_classz00_3174 = BGl_nodez00zzast_nodez00;
						{	/* Inline/inline.scm 28 */
							obj_t BgL__ortest_1117z00_3175;

							BgL__ortest_1117z00_3175 = BGL_CLASS_NIL(BgL_classz00_3174);
							if (CBOOL(BgL__ortest_1117z00_3175))
								{	/* Inline/inline.scm 28 */
									BgL_auxz00_3931 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_3175);
								}
							else
								{	/* Inline/inline.scm 28 */
									BgL_auxz00_3931 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3174));
								}
						}
					}
					{
						obj_t BgL_auxz00_3925;

						{	/* Inline/inline.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_3926;

							BgL_tmpz00_3926 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1131z00_2956));
							BgL_auxz00_3925 = BGL_OBJECT_WIDENING(BgL_tmpz00_3926);
						}
						BgL_auxz00_3924 = ((BgL_isfunz00_bglt) BgL_auxz00_3925);
					}
					((((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_3924))->
							BgL_originalzd2bodyzd2) =
						((BgL_nodez00_bglt) BgL_auxz00_3931), BUNSPEC);
				}
				{
					BgL_isfunz00_bglt BgL_auxz00_3939;

					{
						obj_t BgL_auxz00_3940;

						{	/* Inline/inline.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_3941;

							BgL_tmpz00_3941 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1131z00_2956));
							BgL_auxz00_3940 = BGL_OBJECT_WIDENING(BgL_tmpz00_3941);
						}
						BgL_auxz00_3939 = ((BgL_isfunz00_bglt) BgL_auxz00_3940);
					}
					((((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_3939))->
							BgL_recursivezd2callszd2) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_isfunz00_bglt BgL_auxz00_3947;

					{
						obj_t BgL_auxz00_3948;

						{	/* Inline/inline.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_3949;

							BgL_tmpz00_3949 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1131z00_2956));
							BgL_auxz00_3948 = BGL_OBJECT_WIDENING(BgL_tmpz00_3949);
						}
						BgL_auxz00_3947 = ((BgL_isfunz00_bglt) BgL_auxz00_3948);
					}
					((((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_3947))->BgL_tailrecz00) =
						((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				BgL_auxz00_3856 = ((BgL_sfunz00_bglt) BgL_new1131z00_2956);
				return ((obj_t) BgL_auxz00_3856);
			}
		}

	}



/* &lambda1574 */
	BgL_sfunz00_bglt BGl_z62lambda1574z62zzinline_inlinez00(obj_t BgL_envz00_2957,
		obj_t BgL_o1128z00_2958)
	{
		{	/* Inline/inline.scm 28 */
			{	/* Inline/inline.scm 28 */
				BgL_isfunz00_bglt BgL_wide1130z00_3177;

				BgL_wide1130z00_3177 =
					((BgL_isfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_isfunz00_bgl))));
				{	/* Inline/inline.scm 28 */
					obj_t BgL_auxz00_3962;
					BgL_objectz00_bglt BgL_tmpz00_3958;

					BgL_auxz00_3962 = ((obj_t) BgL_wide1130z00_3177);
					BgL_tmpz00_3958 =
						((BgL_objectz00_bglt)
						((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_o1128z00_2958)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3958, BgL_auxz00_3962);
				}
				((BgL_objectz00_bglt)
					((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_o1128z00_2958)));
				{	/* Inline/inline.scm 28 */
					long BgL_arg1575z00_3178;

					BgL_arg1575z00_3178 = BGL_CLASS_NUM(BGl_isfunz00zzinline_inlinez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_sfunz00_bglt)
								((BgL_sfunz00_bglt) BgL_o1128z00_2958))), BgL_arg1575z00_3178);
				}
				return
					((BgL_sfunz00_bglt)
					((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_o1128z00_2958)));
			}
		}

	}



/* &lambda1565 */
	BgL_sfunz00_bglt BGl_z62lambda1565z62zzinline_inlinez00(obj_t BgL_envz00_2959,
		obj_t BgL_arity1103z00_2960, obj_t BgL_sidezd2effect1104zd2_2961,
		obj_t BgL_predicatezd2of1105zd2_2962,
		obj_t BgL_stackzd2allocator1106zd2_2963, obj_t BgL_topzf31107zf3_2964,
		obj_t BgL_thezd2closure1108zd2_2965, obj_t BgL_effect1109z00_2966,
		obj_t BgL_failsafe1110z00_2967, obj_t BgL_argszd2noescape1111zd2_2968,
		obj_t BgL_argszd2retescape1112zd2_2969, obj_t BgL_property1113z00_2970,
		obj_t BgL_args1114z00_2971, obj_t BgL_argszd2name1115zd2_2972,
		obj_t BgL_body1116z00_2973, obj_t BgL_class1117z00_2974,
		obj_t BgL_dssslzd2keywords1118zd2_2975, obj_t BgL_loc1119z00_2976,
		obj_t BgL_optionals1120z00_2977, obj_t BgL_keys1121z00_2978,
		obj_t BgL_thezd2closurezd2global1122z00_2979,
		obj_t BgL_strength1123z00_2980, obj_t BgL_stackable1124z00_2981,
		obj_t BgL_originalzd2body1125zd2_2982,
		obj_t BgL_recursivezd2calls1126zd2_2983, obj_t BgL_tailrec1127z00_2984)
	{
		{	/* Inline/inline.scm 28 */
			{	/* Inline/inline.scm 28 */
				long BgL_arity1103z00_3179;
				bool_t BgL_topzf31107zf3_3180;
				bool_t BgL_tailrec1127z00_3183;

				BgL_arity1103z00_3179 = (long) CINT(BgL_arity1103z00_2960);
				BgL_topzf31107zf3_3180 = CBOOL(BgL_topzf31107zf3_2964);
				BgL_tailrec1127z00_3183 = CBOOL(BgL_tailrec1127z00_2984);
				{	/* Inline/inline.scm 28 */
					BgL_sfunz00_bglt BgL_new1176z00_3184;

					{	/* Inline/inline.scm 28 */
						BgL_sfunz00_bglt BgL_tmp1174z00_3185;
						BgL_isfunz00_bglt BgL_wide1175z00_3186;

						{
							BgL_sfunz00_bglt BgL_auxz00_3979;

							{	/* Inline/inline.scm 28 */
								BgL_sfunz00_bglt BgL_new1173z00_3187;

								BgL_new1173z00_3187 =
									((BgL_sfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_sfunz00_bgl))));
								{	/* Inline/inline.scm 28 */
									long BgL_arg1573z00_3188;

									BgL_arg1573z00_3188 = BGL_CLASS_NUM(BGl_sfunz00zzast_varz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1173z00_3187),
										BgL_arg1573z00_3188);
								}
								{	/* Inline/inline.scm 28 */
									BgL_objectz00_bglt BgL_tmpz00_3984;

									BgL_tmpz00_3984 = ((BgL_objectz00_bglt) BgL_new1173z00_3187);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3984, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1173z00_3187);
								BgL_auxz00_3979 = BgL_new1173z00_3187;
							}
							BgL_tmp1174z00_3185 = ((BgL_sfunz00_bglt) BgL_auxz00_3979);
						}
						BgL_wide1175z00_3186 =
							((BgL_isfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_isfunz00_bgl))));
						{	/* Inline/inline.scm 28 */
							obj_t BgL_auxz00_3992;
							BgL_objectz00_bglt BgL_tmpz00_3990;

							BgL_auxz00_3992 = ((obj_t) BgL_wide1175z00_3186);
							BgL_tmpz00_3990 = ((BgL_objectz00_bglt) BgL_tmp1174z00_3185);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3990, BgL_auxz00_3992);
						}
						((BgL_objectz00_bglt) BgL_tmp1174z00_3185);
						{	/* Inline/inline.scm 28 */
							long BgL_arg1571z00_3189;

							BgL_arg1571z00_3189 =
								BGL_CLASS_NUM(BGl_isfunz00zzinline_inlinez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1174z00_3185),
								BgL_arg1571z00_3189);
						}
						BgL_new1176z00_3184 = ((BgL_sfunz00_bglt) BgL_tmp1174z00_3185);
					}
					((((BgL_funz00_bglt) COBJECT(
									((BgL_funz00_bglt) BgL_new1176z00_3184)))->BgL_arityz00) =
						((long) BgL_arity1103z00_3179), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1176z00_3184)))->BgL_sidezd2effectzd2) =
						((obj_t) BgL_sidezd2effect1104zd2_2961), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1176z00_3184)))->BgL_predicatezd2ofzd2) =
						((obj_t) BgL_predicatezd2of1105zd2_2962), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1176z00_3184)))->BgL_stackzd2allocatorzd2) =
						((obj_t) BgL_stackzd2allocator1106zd2_2963), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1176z00_3184)))->BgL_topzf3zf3) =
						((bool_t) BgL_topzf31107zf3_3180), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1176z00_3184)))->BgL_thezd2closurezd2) =
						((obj_t) BgL_thezd2closure1108zd2_2965), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1176z00_3184)))->BgL_effectz00) =
						((obj_t) BgL_effect1109z00_2966), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1176z00_3184)))->BgL_failsafez00) =
						((obj_t) BgL_failsafe1110z00_2967), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1176z00_3184)))->BgL_argszd2noescapezd2) =
						((obj_t) BgL_argszd2noescape1111zd2_2968), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1176z00_3184)))->BgL_argszd2retescapezd2) =
						((obj_t) BgL_argszd2retescape1112zd2_2969), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1176z00_3184)))->BgL_propertyz00) =
						((obj_t) BgL_property1113z00_2970), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1176z00_3184)))->BgL_argsz00) =
						((obj_t) BgL_args1114z00_2971), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1176z00_3184)))->BgL_argszd2namezd2) =
						((obj_t) BgL_argszd2name1115zd2_2972), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1176z00_3184)))->BgL_bodyz00) =
						((obj_t) BgL_body1116z00_2973), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1176z00_3184)))->BgL_classz00) =
						((obj_t) BgL_class1117z00_2974), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1176z00_3184)))->BgL_dssslzd2keywordszd2) =
						((obj_t) BgL_dssslzd2keywords1118zd2_2975), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1176z00_3184)))->BgL_locz00) =
						((obj_t) BgL_loc1119z00_2976), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1176z00_3184)))->BgL_optionalsz00) =
						((obj_t) BgL_optionals1120z00_2977), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1176z00_3184)))->BgL_keysz00) =
						((obj_t) BgL_keys1121z00_2978), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1176z00_3184)))->BgL_thezd2closurezd2globalz00) =
						((obj_t) BgL_thezd2closurezd2global1122z00_2979), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1176z00_3184)))->BgL_strengthz00) =
						((obj_t) ((obj_t) BgL_strength1123z00_2980)), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1176z00_3184)))->BgL_stackablez00) =
						((obj_t) BgL_stackable1124z00_2981), BUNSPEC);
					{
						BgL_isfunz00_bglt BgL_auxz00_4045;

						{
							obj_t BgL_auxz00_4046;

							{	/* Inline/inline.scm 28 */
								BgL_objectz00_bglt BgL_tmpz00_4047;

								BgL_tmpz00_4047 = ((BgL_objectz00_bglt) BgL_new1176z00_3184);
								BgL_auxz00_4046 = BGL_OBJECT_WIDENING(BgL_tmpz00_4047);
							}
							BgL_auxz00_4045 = ((BgL_isfunz00_bglt) BgL_auxz00_4046);
						}
						((((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_4045))->
								BgL_originalzd2bodyzd2) =
							((BgL_nodez00_bglt) ((BgL_nodez00_bglt)
									BgL_originalzd2body1125zd2_2982)), BUNSPEC);
					}
					{
						BgL_isfunz00_bglt BgL_auxz00_4053;

						{
							obj_t BgL_auxz00_4054;

							{	/* Inline/inline.scm 28 */
								BgL_objectz00_bglt BgL_tmpz00_4055;

								BgL_tmpz00_4055 = ((BgL_objectz00_bglt) BgL_new1176z00_3184);
								BgL_auxz00_4054 = BGL_OBJECT_WIDENING(BgL_tmpz00_4055);
							}
							BgL_auxz00_4053 = ((BgL_isfunz00_bglt) BgL_auxz00_4054);
						}
						((((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_4053))->
								BgL_recursivezd2callszd2) =
							((obj_t) BgL_recursivezd2calls1126zd2_2983), BUNSPEC);
					}
					{
						BgL_isfunz00_bglt BgL_auxz00_4060;

						{
							obj_t BgL_auxz00_4061;

							{	/* Inline/inline.scm 28 */
								BgL_objectz00_bglt BgL_tmpz00_4062;

								BgL_tmpz00_4062 = ((BgL_objectz00_bglt) BgL_new1176z00_3184);
								BgL_auxz00_4061 = BGL_OBJECT_WIDENING(BgL_tmpz00_4062);
							}
							BgL_auxz00_4060 = ((BgL_isfunz00_bglt) BgL_auxz00_4061);
						}
						((((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_4060))->BgL_tailrecz00) =
							((bool_t) BgL_tailrec1127z00_3183), BUNSPEC);
					}
					return BgL_new1176z00_3184;
				}
			}
		}

	}



/* &<@anonymous:1628> */
	obj_t BGl_z62zc3z04anonymousza31628ze3ze5zzinline_inlinez00(obj_t
		BgL_envz00_2985)
	{
		{	/* Inline/inline.scm 28 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &lambda1627 */
	obj_t BGl_z62lambda1627z62zzinline_inlinez00(obj_t BgL_envz00_2986,
		obj_t BgL_oz00_2987, obj_t BgL_vz00_2988)
	{
		{	/* Inline/inline.scm 28 */
			{	/* Inline/inline.scm 28 */
				bool_t BgL_vz00_3191;

				BgL_vz00_3191 = CBOOL(BgL_vz00_2988);
				{
					BgL_isfunz00_bglt BgL_auxz00_4069;

					{
						obj_t BgL_auxz00_4070;

						{	/* Inline/inline.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_4071;

							BgL_tmpz00_4071 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_2987));
							BgL_auxz00_4070 = BGL_OBJECT_WIDENING(BgL_tmpz00_4071);
						}
						BgL_auxz00_4069 = ((BgL_isfunz00_bglt) BgL_auxz00_4070);
					}
					return
						((((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_4069))->BgL_tailrecz00) =
						((bool_t) BgL_vz00_3191), BUNSPEC);
				}
			}
		}

	}



/* &lambda1626 */
	obj_t BGl_z62lambda1626z62zzinline_inlinez00(obj_t BgL_envz00_2989,
		obj_t BgL_oz00_2990)
	{
		{	/* Inline/inline.scm 28 */
			{	/* Inline/inline.scm 28 */
				bool_t BgL_tmpz00_4077;

				{
					BgL_isfunz00_bglt BgL_auxz00_4078;

					{
						obj_t BgL_auxz00_4079;

						{	/* Inline/inline.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_4080;

							BgL_tmpz00_4080 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_2990));
							BgL_auxz00_4079 = BGL_OBJECT_WIDENING(BgL_tmpz00_4080);
						}
						BgL_auxz00_4078 = ((BgL_isfunz00_bglt) BgL_auxz00_4079);
					}
					BgL_tmpz00_4077 =
						(((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_4078))->BgL_tailrecz00);
				}
				return BBOOL(BgL_tmpz00_4077);
			}
		}

	}



/* &<@anonymous:1612> */
	obj_t BGl_z62zc3z04anonymousza31612ze3ze5zzinline_inlinez00(obj_t
		BgL_envz00_2991)
	{
		{	/* Inline/inline.scm 28 */
			return BUNSPEC;
		}

	}



/* &lambda1611 */
	obj_t BGl_z62lambda1611z62zzinline_inlinez00(obj_t BgL_envz00_2992,
		obj_t BgL_oz00_2993, obj_t BgL_vz00_2994)
	{
		{	/* Inline/inline.scm 28 */
			{
				BgL_isfunz00_bglt BgL_auxz00_4087;

				{
					obj_t BgL_auxz00_4088;

					{	/* Inline/inline.scm 28 */
						BgL_objectz00_bglt BgL_tmpz00_4089;

						BgL_tmpz00_4089 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_2993));
						BgL_auxz00_4088 = BGL_OBJECT_WIDENING(BgL_tmpz00_4089);
					}
					BgL_auxz00_4087 = ((BgL_isfunz00_bglt) BgL_auxz00_4088);
				}
				return
					((((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_4087))->
						BgL_recursivezd2callszd2) = ((obj_t) BgL_vz00_2994), BUNSPEC);
			}
		}

	}



/* &lambda1610 */
	obj_t BGl_z62lambda1610z62zzinline_inlinez00(obj_t BgL_envz00_2995,
		obj_t BgL_oz00_2996)
	{
		{	/* Inline/inline.scm 28 */
			{
				BgL_isfunz00_bglt BgL_auxz00_4095;

				{
					obj_t BgL_auxz00_4096;

					{	/* Inline/inline.scm 28 */
						BgL_objectz00_bglt BgL_tmpz00_4097;

						BgL_tmpz00_4097 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_2996));
						BgL_auxz00_4096 = BGL_OBJECT_WIDENING(BgL_tmpz00_4097);
					}
					BgL_auxz00_4095 = ((BgL_isfunz00_bglt) BgL_auxz00_4096);
				}
				return
					(((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_4095))->
					BgL_recursivezd2callszd2);
			}
		}

	}



/* &lambda1596 */
	obj_t BGl_z62lambda1596z62zzinline_inlinez00(obj_t BgL_envz00_2997,
		obj_t BgL_oz00_2998, obj_t BgL_vz00_2999)
	{
		{	/* Inline/inline.scm 28 */
			{
				BgL_isfunz00_bglt BgL_auxz00_4103;

				{
					obj_t BgL_auxz00_4104;

					{	/* Inline/inline.scm 28 */
						BgL_objectz00_bglt BgL_tmpz00_4105;

						BgL_tmpz00_4105 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_2998));
						BgL_auxz00_4104 = BGL_OBJECT_WIDENING(BgL_tmpz00_4105);
					}
					BgL_auxz00_4103 = ((BgL_isfunz00_bglt) BgL_auxz00_4104);
				}
				return
					((((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_4103))->
						BgL_originalzd2bodyzd2) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_vz00_2999)), BUNSPEC);
			}
		}

	}



/* &lambda1595 */
	BgL_nodez00_bglt BGl_z62lambda1595z62zzinline_inlinez00(obj_t BgL_envz00_3000,
		obj_t BgL_oz00_3001)
	{
		{	/* Inline/inline.scm 28 */
			{
				BgL_isfunz00_bglt BgL_auxz00_4112;

				{
					obj_t BgL_auxz00_4113;

					{	/* Inline/inline.scm 28 */
						BgL_objectz00_bglt BgL_tmpz00_4114;

						BgL_tmpz00_4114 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3001));
						BgL_auxz00_4113 = BGL_OBJECT_WIDENING(BgL_tmpz00_4114);
					}
					BgL_auxz00_4112 = ((BgL_isfunz00_bglt) BgL_auxz00_4113);
				}
				return
					(((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_4112))->
					BgL_originalzd2bodyzd2);
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzinline_inlinez00(void)
	{
		{	/* Inline/inline.scm 15 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_inlinezd2nodezd2envz00zzinline_inlinez00,
				BGl_proc1996z00zzinline_inlinez00, BGl_nodez00zzast_nodez00,
				BGl_string1997z00zzinline_inlinez00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_disablezd2inliningz12zd2envz12zzinline_inlinez00,
				BGl_proc1998z00zzinline_inlinez00, BGl_nodez00zzast_nodez00,
				BGl_string1999z00zzinline_inlinez00);
		}

	}



/* &disable-inlining!1357 */
	obj_t BGl_z62disablezd2inliningz121357za2zzinline_inlinez00(obj_t
		BgL_envz00_3006, obj_t BgL_nz00_3007)
	{
		{	/* Inline/inline.scm 304 */
			return
				((obj_t)
				BGl_walk0z12z12zzast_walkz00(
					((BgL_nodez00_bglt) BgL_nz00_3007),
					BGl_disablezd2inliningz12zd2envz12zzinline_inlinez00));
		}

	}



/* &inline-node1309 */
	obj_t BGl_z62inlinezd2node1309zb0zzinline_inlinez00(obj_t BgL_envz00_3008,
		obj_t BgL_nodez00_3009, obj_t BgL_kfactorz00_3010, obj_t BgL_stackz00_3011)
	{
		{	/* Inline/inline.scm 70 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(12),
				BGl_string2000z00zzinline_inlinez00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_3009)));
		}

	}



/* inline-node */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_inlinezd2nodezd2zzinline_inlinez00(BgL_nodez00_bglt BgL_nodez00_95,
		long BgL_kfactorz00_96, obj_t BgL_stackz00_97)
	{
		{	/* Inline/inline.scm 70 */
			{	/* Inline/inline.scm 70 */
				obj_t BgL_method1310z00_1684;

				{	/* Inline/inline.scm 70 */
					obj_t BgL_res1969z00_2411;

					{	/* Inline/inline.scm 70 */
						long BgL_objzd2classzd2numz00_2382;

						BgL_objzd2classzd2numz00_2382 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_95));
						{	/* Inline/inline.scm 70 */
							obj_t BgL_arg1811z00_2383;

							BgL_arg1811z00_2383 =
								PROCEDURE_REF(BGl_inlinezd2nodezd2envz00zzinline_inlinez00,
								(int) (1L));
							{	/* Inline/inline.scm 70 */
								int BgL_offsetz00_2386;

								BgL_offsetz00_2386 = (int) (BgL_objzd2classzd2numz00_2382);
								{	/* Inline/inline.scm 70 */
									long BgL_offsetz00_2387;

									BgL_offsetz00_2387 =
										((long) (BgL_offsetz00_2386) - OBJECT_TYPE);
									{	/* Inline/inline.scm 70 */
										long BgL_modz00_2388;

										BgL_modz00_2388 =
											(BgL_offsetz00_2387 >> (int) ((long) ((int) (4L))));
										{	/* Inline/inline.scm 70 */
											long BgL_restz00_2390;

											BgL_restz00_2390 =
												(BgL_offsetz00_2387 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Inline/inline.scm 70 */

												{	/* Inline/inline.scm 70 */
													obj_t BgL_bucketz00_2392;

													BgL_bucketz00_2392 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2383), BgL_modz00_2388);
													BgL_res1969z00_2411 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2392), BgL_restz00_2390);
					}}}}}}}}
					BgL_method1310z00_1684 = BgL_res1969z00_2411;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL3(BgL_method1310z00_1684,
						((obj_t) BgL_nodez00_95),
						BINT(BgL_kfactorz00_96), BgL_stackz00_97));
			}
		}

	}



/* &inline-node */
	BgL_nodez00_bglt BGl_z62inlinezd2nodezb0zzinline_inlinez00(obj_t
		BgL_envz00_3012, obj_t BgL_nodez00_3013, obj_t BgL_kfactorz00_3014,
		obj_t BgL_stackz00_3015)
	{
		{	/* Inline/inline.scm 70 */
			return
				BGl_inlinezd2nodezd2zzinline_inlinez00(
				((BgL_nodez00_bglt) BgL_nodez00_3013),
				(long) CINT(BgL_kfactorz00_3014), BgL_stackz00_3015);
		}

	}



/* disable-inlining! */
	BgL_nodez00_bglt
		BGl_disablezd2inliningz12zc0zzinline_inlinez00(BgL_nodez00_bglt
		BgL_nz00_170)
	{
		{	/* Inline/inline.scm 304 */
			{	/* Inline/inline.scm 304 */
				obj_t BgL_method1358z00_1685;

				{	/* Inline/inline.scm 304 */
					obj_t BgL_res1974z00_2442;

					{	/* Inline/inline.scm 304 */
						long BgL_objzd2classzd2numz00_2413;

						BgL_objzd2classzd2numz00_2413 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nz00_170));
						{	/* Inline/inline.scm 304 */
							obj_t BgL_arg1811z00_2414;

							BgL_arg1811z00_2414 =
								PROCEDURE_REF
								(BGl_disablezd2inliningz12zd2envz12zzinline_inlinez00,
								(int) (1L));
							{	/* Inline/inline.scm 304 */
								int BgL_offsetz00_2417;

								BgL_offsetz00_2417 = (int) (BgL_objzd2classzd2numz00_2413);
								{	/* Inline/inline.scm 304 */
									long BgL_offsetz00_2418;

									BgL_offsetz00_2418 =
										((long) (BgL_offsetz00_2417) - OBJECT_TYPE);
									{	/* Inline/inline.scm 304 */
										long BgL_modz00_2419;

										BgL_modz00_2419 =
											(BgL_offsetz00_2418 >> (int) ((long) ((int) (4L))));
										{	/* Inline/inline.scm 304 */
											long BgL_restz00_2421;

											BgL_restz00_2421 =
												(BgL_offsetz00_2418 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Inline/inline.scm 304 */

												{	/* Inline/inline.scm 304 */
													obj_t BgL_bucketz00_2423;

													BgL_bucketz00_2423 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2414), BgL_modz00_2419);
													BgL_res1974z00_2442 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2423), BgL_restz00_2421);
					}}}}}}}}
					BgL_method1358z00_1685 = BgL_res1974z00_2442;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL1(BgL_method1358z00_1685, ((obj_t) BgL_nz00_170)));
			}
		}

	}



/* &disable-inlining! */
	BgL_nodez00_bglt BGl_z62disablezd2inliningz12za2zzinline_inlinez00(obj_t
		BgL_envz00_3003, obj_t BgL_nz00_3004)
	{
		{	/* Inline/inline.scm 304 */
			return
				BGl_disablezd2inliningz12zc0zzinline_inlinez00(
				((BgL_nodez00_bglt) BgL_nz00_3004));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzinline_inlinez00(void)
	{
		{	/* Inline/inline.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inlinezd2nodezd2envz00zzinline_inlinez00, BGl_atomz00zzast_nodez00,
				BGl_proc2001z00zzinline_inlinez00, BGl_string2002z00zzinline_inlinez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inlinezd2nodezd2envz00zzinline_inlinez00, BGl_varz00zzast_nodez00,
				BGl_proc2003z00zzinline_inlinez00, BGl_string2002z00zzinline_inlinez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inlinezd2nodezd2envz00zzinline_inlinez00,
				BGl_kwotez00zzast_nodez00, BGl_proc2004z00zzinline_inlinez00,
				BGl_string2002z00zzinline_inlinez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inlinezd2nodezd2envz00zzinline_inlinez00,
				BGl_sequencez00zzast_nodez00, BGl_proc2005z00zzinline_inlinez00,
				BGl_string2002z00zzinline_inlinez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inlinezd2nodezd2envz00zzinline_inlinez00, BGl_syncz00zzast_nodez00,
				BGl_proc2006z00zzinline_inlinez00, BGl_string2002z00zzinline_inlinez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inlinezd2nodezd2envz00zzinline_inlinez00, BGl_appz00zzast_nodez00,
				BGl_proc2007z00zzinline_inlinez00, BGl_string2002z00zzinline_inlinez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inlinezd2nodezd2envz00zzinline_inlinez00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc2008z00zzinline_inlinez00,
				BGl_string2002z00zzinline_inlinez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inlinezd2nodezd2envz00zzinline_inlinez00,
				BGl_funcallz00zzast_nodez00, BGl_proc2009z00zzinline_inlinez00,
				BGl_string2002z00zzinline_inlinez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inlinezd2nodezd2envz00zzinline_inlinez00,
				BGl_externz00zzast_nodez00, BGl_proc2010z00zzinline_inlinez00,
				BGl_string2002z00zzinline_inlinez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inlinezd2nodezd2envz00zzinline_inlinez00, BGl_castz00zzast_nodez00,
				BGl_proc2011z00zzinline_inlinez00, BGl_string2002z00zzinline_inlinez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inlinezd2nodezd2envz00zzinline_inlinez00, BGl_setqz00zzast_nodez00,
				BGl_proc2012z00zzinline_inlinez00, BGl_string2002z00zzinline_inlinez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inlinezd2nodezd2envz00zzinline_inlinez00,
				BGl_conditionalz00zzast_nodez00, BGl_proc2013z00zzinline_inlinez00,
				BGl_string2002z00zzinline_inlinez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inlinezd2nodezd2envz00zzinline_inlinez00, BGl_failz00zzast_nodez00,
				BGl_proc2014z00zzinline_inlinez00, BGl_string2002z00zzinline_inlinez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inlinezd2nodezd2envz00zzinline_inlinez00,
				BGl_switchz00zzast_nodez00, BGl_proc2015z00zzinline_inlinez00,
				BGl_string2002z00zzinline_inlinez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inlinezd2nodezd2envz00zzinline_inlinez00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc2016z00zzinline_inlinez00,
				BGl_string2002z00zzinline_inlinez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inlinezd2nodezd2envz00zzinline_inlinez00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc2017z00zzinline_inlinez00,
				BGl_string2002z00zzinline_inlinez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inlinezd2nodezd2envz00zzinline_inlinez00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc2018z00zzinline_inlinez00,
				BGl_string2002z00zzinline_inlinez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inlinezd2nodezd2envz00zzinline_inlinez00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc2019z00zzinline_inlinez00,
				BGl_string2002z00zzinline_inlinez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inlinezd2nodezd2envz00zzinline_inlinez00,
				BGl_retblockz00zzast_nodez00, BGl_proc2020z00zzinline_inlinez00,
				BGl_string2002z00zzinline_inlinez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inlinezd2nodezd2envz00zzinline_inlinez00,
				BGl_returnz00zzast_nodez00, BGl_proc2021z00zzinline_inlinez00,
				BGl_string2002z00zzinline_inlinez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inlinezd2nodezd2envz00zzinline_inlinez00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc2022z00zzinline_inlinez00,
				BGl_string2002z00zzinline_inlinez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inlinezd2nodezd2envz00zzinline_inlinez00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc2023z00zzinline_inlinez00,
				BGl_string2002z00zzinline_inlinez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_inlinezd2nodezd2envz00zzinline_inlinez00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc2024z00zzinline_inlinez00,
				BGl_string2002z00zzinline_inlinez00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_disablezd2inliningz12zd2envz12zzinline_inlinez00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc2025z00zzinline_inlinez00,
				BGl_string2026z00zzinline_inlinez00);
		}

	}



/* &disable-inlining!-le1360 */
	BgL_nodez00_bglt
		BGl_z62disablezd2inliningz12zd2le1360z70zzinline_inlinez00(obj_t
		BgL_envz00_3040, obj_t BgL_nz00_3041)
	{
		{	/* Inline/inline.scm 310 */
			{
				obj_t BgL_bodyz00_3203;

				{	/* Inline/inline.scm 318 */
					obj_t BgL_g1306z00_3219;

					BgL_g1306z00_3219 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nz00_3041)))->BgL_localsz00);
					{
						obj_t BgL_l1304z00_3221;

						BgL_l1304z00_3221 = BgL_g1306z00_3219;
					BgL_zc3z04anonymousza31883ze3z87_3220:
						if (PAIRP(BgL_l1304z00_3221))
							{	/* Inline/inline.scm 318 */
								{	/* Inline/inline.scm 319 */
									obj_t BgL_funz00_3222;

									BgL_funz00_3222 = CAR(BgL_l1304z00_3221);
									{	/* Inline/inline.scm 320 */
										BgL_sfunz00_bglt BgL_i1169z00_3223;

										BgL_i1169z00_3223 =
											((BgL_sfunz00_bglt)
											(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_localz00_bglt) BgL_funz00_3222))))->
												BgL_valuez00));
										{	/* Inline/inline.scm 321 */
											obj_t BgL_arg1885z00_3224;

											BgL_arg1885z00_3224 =
												(((BgL_sfunz00_bglt) COBJECT(BgL_i1169z00_3223))->
												BgL_bodyz00);
											BGl_disablezd2inliningz12zc0zzinline_inlinez00((
													(BgL_nodez00_bglt) BgL_arg1885z00_3224));
										}
										{	/* Inline/inline.scm 322 */
											bool_t BgL_test2141z00_4235;

											{	/* Inline/inline.scm 322 */
												obj_t BgL_tmpz00_4236;

												BgL_bodyz00_3203 =
													(((BgL_sfunz00_bglt) COBJECT(BgL_i1169z00_3223))->
													BgL_bodyz00);
												{	/* Inline/inline.scm 313 */
													bool_t BgL_test2142z00_4237;

													{	/* Inline/inline.scm 313 */
														obj_t BgL_classz00_3204;

														BgL_classz00_3204 = BGl_sequencez00zzast_nodez00;
														if (BGL_OBJECTP(BgL_bodyz00_3203))
															{	/* Inline/inline.scm 313 */
																BgL_objectz00_bglt BgL_arg1807z00_3205;

																BgL_arg1807z00_3205 =
																	(BgL_objectz00_bglt) (BgL_bodyz00_3203);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Inline/inline.scm 313 */
																		long BgL_idxz00_3206;

																		BgL_idxz00_3206 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_3205);
																		BgL_test2142z00_4237 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_3206 + 3L)) ==
																			BgL_classz00_3204);
																	}
																else
																	{	/* Inline/inline.scm 313 */
																		bool_t BgL_res1978z00_3209;

																		{	/* Inline/inline.scm 313 */
																			obj_t BgL_oclassz00_3210;

																			{	/* Inline/inline.scm 313 */
																				obj_t BgL_arg1815z00_3211;
																				long BgL_arg1816z00_3212;

																				BgL_arg1815z00_3211 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Inline/inline.scm 313 */
																					long BgL_arg1817z00_3213;

																					BgL_arg1817z00_3213 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_3205);
																					BgL_arg1816z00_3212 =
																						(BgL_arg1817z00_3213 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_3210 =
																					VECTOR_REF(BgL_arg1815z00_3211,
																					BgL_arg1816z00_3212);
																			}
																			{	/* Inline/inline.scm 313 */
																				bool_t BgL__ortest_1115z00_3214;

																				BgL__ortest_1115z00_3214 =
																					(BgL_classz00_3204 ==
																					BgL_oclassz00_3210);
																				if (BgL__ortest_1115z00_3214)
																					{	/* Inline/inline.scm 313 */
																						BgL_res1978z00_3209 =
																							BgL__ortest_1115z00_3214;
																					}
																				else
																					{	/* Inline/inline.scm 313 */
																						long BgL_odepthz00_3215;

																						{	/* Inline/inline.scm 313 */
																							obj_t BgL_arg1804z00_3216;

																							BgL_arg1804z00_3216 =
																								(BgL_oclassz00_3210);
																							BgL_odepthz00_3215 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_3216);
																						}
																						if ((3L < BgL_odepthz00_3215))
																							{	/* Inline/inline.scm 313 */
																								obj_t BgL_arg1802z00_3217;

																								{	/* Inline/inline.scm 313 */
																									obj_t BgL_arg1803z00_3218;

																									BgL_arg1803z00_3218 =
																										(BgL_oclassz00_3210);
																									BgL_arg1802z00_3217 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_3218, 3L);
																								}
																								BgL_res1978z00_3209 =
																									(BgL_arg1802z00_3217 ==
																									BgL_classz00_3204);
																							}
																						else
																							{	/* Inline/inline.scm 313 */
																								BgL_res1978z00_3209 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2142z00_4237 = BgL_res1978z00_3209;
																	}
															}
														else
															{	/* Inline/inline.scm 313 */
																BgL_test2142z00_4237 = ((bool_t) 0);
															}
													}
													if (BgL_test2142z00_4237)
														{	/* Inline/inline.scm 313 */
															BgL_tmpz00_4236 =
																BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																(CNST_TABLE_REF(13),
																(((BgL_sequencez00_bglt)
																		COBJECT(((BgL_sequencez00_bglt)
																				BgL_bodyz00_3203)))->BgL_metaz00));
														}
													else
														{	/* Inline/inline.scm 313 */
															BgL_tmpz00_4236 = BFALSE;
														}
												}
												BgL_test2141z00_4235 = CBOOL(BgL_tmpz00_4236);
											}
											if (BgL_test2141z00_4235)
												{	/* Inline/inline.scm 322 */
													BFALSE;
												}
											else
												{	/* Inline/inline.scm 323 */
													BgL_nodez00_bglt BgL_i1170z00_3225;

													BgL_i1170z00_3225 =
														((BgL_nodez00_bglt)
														(((BgL_sfunz00_bglt) COBJECT(BgL_i1169z00_3223))->
															BgL_bodyz00));
													{
														obj_t BgL_auxz00_4268;

														{	/* Inline/inline.scm 325 */
															BgL_sequencez00_bglt BgL_new1172z00_3226;

															{	/* Inline/inline.scm 328 */
																BgL_sequencez00_bglt BgL_new1171z00_3227;

																BgL_new1171z00_3227 =
																	((BgL_sequencez00_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_sequencez00_bgl))));
																{	/* Inline/inline.scm 328 */
																	long BgL_arg1891z00_3228;

																	{	/* Inline/inline.scm 328 */
																		obj_t BgL_classz00_3229;

																		BgL_classz00_3229 =
																			BGl_sequencez00zzast_nodez00;
																		BgL_arg1891z00_3228 =
																			BGL_CLASS_NUM(BgL_classz00_3229);
																	}
																	BGL_OBJECT_CLASS_NUM_SET(
																		((BgL_objectz00_bglt) BgL_new1171z00_3227),
																		BgL_arg1891z00_3228);
																}
																{	/* Inline/inline.scm 328 */
																	BgL_objectz00_bglt BgL_tmpz00_4273;

																	BgL_tmpz00_4273 =
																		((BgL_objectz00_bglt) BgL_new1171z00_3227);
																	BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4273,
																		BFALSE);
																}
																((BgL_objectz00_bglt) BgL_new1171z00_3227);
																BgL_new1172z00_3226 = BgL_new1171z00_3227;
															}
															((((BgL_nodez00_bglt) COBJECT(
																			((BgL_nodez00_bglt)
																				BgL_new1172z00_3226)))->BgL_locz00) =
																((obj_t) BFALSE), BUNSPEC);
															((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																				BgL_new1172z00_3226)))->BgL_typez00) =
																((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																			COBJECT(BgL_i1170z00_3225))->
																		BgL_typez00)), BUNSPEC);
															((((BgL_nodezf2effectzf2_bglt)
																		COBJECT(((BgL_nodezf2effectzf2_bglt)
																				BgL_new1172z00_3226)))->
																	BgL_sidezd2effectzd2) =
																((obj_t) BUNSPEC), BUNSPEC);
															((((BgL_nodezf2effectzf2_bglt)
																		COBJECT(((BgL_nodezf2effectzf2_bglt)
																				BgL_new1172z00_3226)))->BgL_keyz00) =
																((obj_t) BINT(-1L)), BUNSPEC);
															{
																obj_t BgL_auxz00_4287;

																{	/* Inline/inline.scm 326 */
																	obj_t BgL_arg1889z00_3230;

																	BgL_arg1889z00_3230 =
																		(((BgL_sfunz00_bglt)
																			COBJECT(BgL_i1169z00_3223))->BgL_bodyz00);
																	{	/* Inline/inline.scm 326 */
																		obj_t BgL_list1890z00_3231;

																		BgL_list1890z00_3231 =
																			MAKE_YOUNG_PAIR(BgL_arg1889z00_3230,
																			BNIL);
																		BgL_auxz00_4287 = BgL_list1890z00_3231;
																}}
																((((BgL_sequencez00_bglt)
																			COBJECT(BgL_new1172z00_3226))->
																		BgL_nodesz00) =
																	((obj_t) BgL_auxz00_4287), BUNSPEC);
															}
															((((BgL_sequencez00_bglt)
																		COBJECT(BgL_new1172z00_3226))->
																	BgL_unsafez00) =
																((bool_t) ((bool_t) 0)), BUNSPEC);
															((((BgL_sequencez00_bglt)
																		COBJECT(BgL_new1172z00_3226))->
																	BgL_metaz00) =
																((obj_t) CNST_TABLE_REF(14)), BUNSPEC);
															BgL_auxz00_4268 = ((obj_t) BgL_new1172z00_3226);
														}
														((((BgL_sfunz00_bglt) COBJECT(BgL_i1169z00_3223))->
																BgL_bodyz00) =
															((obj_t) BgL_auxz00_4268), BUNSPEC);
								}}}}}
								{
									obj_t BgL_l1304z00_4296;

									BgL_l1304z00_4296 = CDR(BgL_l1304z00_3221);
									BgL_l1304z00_3221 = BgL_l1304z00_4296;
									goto BgL_zc3z04anonymousza31883ze3z87_3220;
								}
							}
						else
							{	/* Inline/inline.scm 318 */
								((bool_t) 1);
							}
					}
				}
				{
					BgL_nodez00_bglt BgL_auxz00_4298;

					{	/* Inline/inline.scm 330 */
						BgL_nodez00_bglt BgL_arg1894z00_3232;

						BgL_arg1894z00_3232 =
							(((BgL_letzd2funzd2_bglt) COBJECT(
									((BgL_letzd2funzd2_bglt) BgL_nz00_3041)))->BgL_bodyz00);
						BgL_auxz00_4298 =
							BGl_disablezd2inliningz12zc0zzinline_inlinez00
							(BgL_arg1894z00_3232);
					}
					((((BgL_letzd2funzd2_bglt) COBJECT(
									((BgL_letzd2funzd2_bglt) BgL_nz00_3041)))->BgL_bodyz00) =
						((BgL_nodez00_bglt) BgL_auxz00_4298), BUNSPEC);
				}
				return ((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nz00_3041));
			}
		}

	}



/* &inline-node-box-set!1356 */
	BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2boxzd2setz121356za2zzinline_inlinez00(obj_t
		BgL_envz00_3042, obj_t BgL_nodez00_3043, obj_t BgL_kfactorz00_3044,
		obj_t BgL_stackz00_3045)
	{
		{	/* Inline/inline.scm 285 */
			{
				BgL_varz00_bglt BgL_auxz00_4306;

				{	/* Inline/inline.scm 287 */
					BgL_varz00_bglt BgL_arg1880z00_3234;

					BgL_arg1880z00_3234 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3043)))->BgL_varz00);
					BgL_auxz00_4306 =
						((BgL_varz00_bglt)
						BGl_inlinezd2nodezd2zzinline_inlinez00(
							((BgL_nodez00_bglt) BgL_arg1880z00_3234),
							(long) CINT(BgL_kfactorz00_3044), BgL_stackz00_3045));
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3043)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_4306), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4315;

				{	/* Inline/inline.scm 288 */
					BgL_nodez00_bglt BgL_arg1882z00_3235;

					BgL_arg1882z00_3235 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3043)))->BgL_valuez00);
					BgL_auxz00_4315 =
						BGl_inlinezd2nodezd2zzinline_inlinez00(BgL_arg1882z00_3235,
						(long) CINT(BgL_kfactorz00_3044), BgL_stackz00_3045);
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3043)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_4315), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3043));
		}

	}



/* &inline-node-box-ref1354 */
	BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2boxzd2ref1354zb0zzinline_inlinez00(obj_t
		BgL_envz00_3046, obj_t BgL_nodez00_3047, obj_t BgL_kfactorz00_3048,
		obj_t BgL_stackz00_3049)
	{
		{	/* Inline/inline.scm 278 */
			{	/* Inline/inline.scm 279 */
				BgL_nodez00_bglt BgL_arg1878z00_3237;

				{	/* Inline/inline.scm 279 */
					BgL_varz00_bglt BgL_arg1879z00_3238;

					BgL_arg1879z00_3238 =
						(((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_3047)))->BgL_varz00);
					BgL_arg1878z00_3237 =
						BGl_inlinezd2nodezd2zzinline_inlinez00(
						((BgL_nodez00_bglt) BgL_arg1879z00_3238),
						(long) CINT(BgL_kfactorz00_3048), BgL_stackz00_3049);
				}
				((((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_3047)))->BgL_varz00) =
					((BgL_varz00_bglt) ((BgL_varz00_bglt) BgL_arg1878z00_3237)), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_3047));
		}

	}



/* &inline-node-make-box1352 */
	BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2makezd2box1352zb0zzinline_inlinez00(obj_t
		BgL_envz00_3050, obj_t BgL_nodez00_3051, obj_t BgL_kfactorz00_3052,
		obj_t BgL_stackz00_3053)
	{
		{	/* Inline/inline.scm 271 */
			((((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_3051)))->BgL_valuez00) =
				((BgL_nodez00_bglt)
					BGl_inlinezd2nodezd2zzinline_inlinez00((((BgL_makezd2boxzd2_bglt)
								COBJECT(((BgL_makezd2boxzd2_bglt) BgL_nodez00_3051)))->
							BgL_valuez00), (long) CINT(BgL_kfactorz00_3052),
						BgL_stackz00_3053)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_3051));
		}

	}



/* &inline-node-return1350 */
	BgL_nodez00_bglt BGl_z62inlinezd2nodezd2return1350z62zzinline_inlinez00(obj_t
		BgL_envz00_3054, obj_t BgL_nodez00_3055, obj_t BgL_kfactorz00_3056,
		obj_t BgL_stackz00_3057)
	{
		{	/* Inline/inline.scm 264 */
			((((BgL_returnz00_bglt) COBJECT(
							((BgL_returnz00_bglt) BgL_nodez00_3055)))->BgL_valuez00) =
				((BgL_nodez00_bglt)
					BGl_inlinezd2nodezd2zzinline_inlinez00((((BgL_returnz00_bglt)
								COBJECT(((BgL_returnz00_bglt) BgL_nodez00_3055)))->
							BgL_valuez00), (long) CINT(BgL_kfactorz00_3056),
						BgL_stackz00_3057)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_returnz00_bglt) BgL_nodez00_3055));
		}

	}



/* &inline-node-retblock1348 */
	BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2retblock1348z62zzinline_inlinez00(obj_t
		BgL_envz00_3058, obj_t BgL_nodez00_3059, obj_t BgL_kfactorz00_3060,
		obj_t BgL_stackz00_3061)
	{
		{	/* Inline/inline.scm 257 */
			((((BgL_retblockz00_bglt) COBJECT(
							((BgL_retblockz00_bglt) BgL_nodez00_3059)))->BgL_bodyz00) =
				((BgL_nodez00_bglt)
					BGl_inlinezd2nodezd2zzinline_inlinez00((((BgL_retblockz00_bglt)
								COBJECT(((BgL_retblockz00_bglt) BgL_nodez00_3059)))->
							BgL_bodyz00), (long) CINT(BgL_kfactorz00_3060),
						BgL_stackz00_3061)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_retblockz00_bglt) BgL_nodez00_3059));
		}

	}



/* &inline-node-jump-ex-1346 */
	BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2jumpzd2exzd21346z62zzinline_inlinez00(obj_t
		BgL_envz00_3062, obj_t BgL_nodez00_3063, obj_t BgL_kfactorz00_3064,
		obj_t BgL_stackz00_3065)
	{
		{	/* Inline/inline.scm 248 */
			{
				BgL_nodez00_bglt BgL_auxz00_4358;

				{	/* Inline/inline.scm 250 */
					BgL_nodez00_bglt BgL_arg1869z00_3243;

					BgL_arg1869z00_3243 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3063)))->BgL_exitz00);
					BgL_auxz00_4358 =
						BGl_inlinezd2nodezd2zzinline_inlinez00(BgL_arg1869z00_3243,
						(long) CINT(BgL_kfactorz00_3064), BgL_stackz00_3065);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3063)))->
						BgL_exitz00) = ((BgL_nodez00_bglt) BgL_auxz00_4358), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4365;

				{	/* Inline/inline.scm 251 */
					BgL_nodez00_bglt BgL_arg1870z00_3244;

					BgL_arg1870z00_3244 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3063)))->
						BgL_valuez00);
					BgL_auxz00_4365 =
						BGl_inlinezd2nodezd2zzinline_inlinez00(BgL_arg1870z00_3244,
						(long) CINT(BgL_kfactorz00_3064), BgL_stackz00_3065);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3063)))->
						BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_4365), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3063));
		}

	}



/* &inline-node-set-ex-i1344 */
	BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2setzd2exzd2i1344z62zzinline_inlinez00(obj_t
		BgL_envz00_3066, obj_t BgL_nodez00_3067, obj_t BgL_kfactorz00_3068,
		obj_t BgL_stackz00_3069)
	{
		{	/* Inline/inline.scm 240 */
			((((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3067)))->BgL_bodyz00) =
				((BgL_nodez00_bglt)
					BGl_inlinezd2nodezd2zzinline_inlinez00((((BgL_setzd2exzd2itz00_bglt)
								COBJECT(((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3067)))->
							BgL_bodyz00), (long) CINT(BgL_kfactorz00_3068),
						BgL_stackz00_3069)), BUNSPEC);
			((((BgL_setzd2exzd2itz00_bglt) COBJECT(((BgL_setzd2exzd2itz00_bglt)
								BgL_nodez00_3067)))->BgL_onexitz00) =
				((BgL_nodez00_bglt)
					BGl_inlinezd2nodezd2zzinline_inlinez00((((BgL_setzd2exzd2itz00_bglt)
								COBJECT(((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3067)))->
							BgL_onexitz00), (long) CINT(BgL_kfactorz00_3068),
						BgL_stackz00_3069)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt)
					BgL_nodez00_3067));
		}

	}



/* &inline-node-let-var1342 */
	BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2letzd2var1342zb0zzinline_inlinez00(obj_t
		BgL_envz00_3070, obj_t BgL_nodez00_3071, obj_t BgL_kfactorz00_3072,
		obj_t BgL_stackz00_3073)
	{
		{	/* Inline/inline.scm 228 */
			{	/* Inline/inline.scm 229 */
				obj_t BgL_g1303z00_3247;

				BgL_g1303z00_3247 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_3071)))->BgL_bindingsz00);
				{
					obj_t BgL_l1301z00_3249;

					BgL_l1301z00_3249 = BgL_g1303z00_3247;
				BgL_zc3z04anonymousza31855ze3z87_3248:
					if (PAIRP(BgL_l1301z00_3249))
						{	/* Inline/inline.scm 233 */
							{	/* Inline/inline.scm 231 */
								obj_t BgL_bindingz00_3250;

								BgL_bindingz00_3250 = CAR(BgL_l1301z00_3249);
								{	/* Inline/inline.scm 232 */
									BgL_nodez00_bglt BgL_arg1857z00_3251;

									{	/* Inline/inline.scm 232 */
										obj_t BgL_arg1858z00_3252;

										BgL_arg1858z00_3252 = CDR(((obj_t) BgL_bindingz00_3250));
										BgL_arg1857z00_3251 =
											BGl_inlinezd2nodezd2zzinline_inlinez00(
											((BgL_nodez00_bglt) BgL_arg1858z00_3252),
											(long) CINT(BgL_kfactorz00_3072), BgL_stackz00_3073);
									}
									{	/* Inline/inline.scm 232 */
										obj_t BgL_auxz00_4400;
										obj_t BgL_tmpz00_4398;

										BgL_auxz00_4400 = ((obj_t) BgL_arg1857z00_3251);
										BgL_tmpz00_4398 = ((obj_t) BgL_bindingz00_3250);
										SET_CDR(BgL_tmpz00_4398, BgL_auxz00_4400);
							}}}
							{
								obj_t BgL_l1301z00_4403;

								BgL_l1301z00_4403 = CDR(BgL_l1301z00_3249);
								BgL_l1301z00_3249 = BgL_l1301z00_4403;
								goto BgL_zc3z04anonymousza31855ze3z87_3248;
							}
						}
					else
						{	/* Inline/inline.scm 233 */
							((bool_t) 1);
						}
				}
			}
			((((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_3071)))->BgL_bodyz00) =
				((BgL_nodez00_bglt)
					BGl_inlinezd2nodezd2zzinline_inlinez00((((BgL_letzd2varzd2_bglt)
								COBJECT(((BgL_letzd2varzd2_bglt) BgL_nodez00_3071)))->
							BgL_bodyz00), (long) CINT(BgL_kfactorz00_3072),
						BgL_stackz00_3073)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nodez00_3071));
		}

	}



/* &inline-node-let-fun1340 */
	BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2letzd2fun1340zb0zzinline_inlinez00(obj_t
		BgL_envz00_3074, obj_t BgL_nodez00_3075, obj_t BgL_kfactorz00_3076,
		obj_t BgL_stackz00_3077)
	{
		{	/* Inline/inline.scm 213 */
			{	/* Inline/inline.scm 217 */
				obj_t BgL_g1300z00_3254;

				BgL_g1300z00_3254 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_3075)))->BgL_localsz00);
				{
					obj_t BgL_l1298z00_3256;

					BgL_l1298z00_3256 = BgL_g1300z00_3254;
				BgL_zc3z04anonymousza31850ze3z87_3255:
					if (PAIRP(BgL_l1298z00_3256))
						{	/* Inline/inline.scm 219 */
							{	/* Inline/inline.scm 218 */
								obj_t BgL_localz00_3257;

								BgL_localz00_3257 = CAR(BgL_l1298z00_3256);
								BGl_inlinezd2sfunz12zc0zzinline_inlinez00(
									((BgL_variablez00_bglt) BgL_localz00_3257),
									(long) CINT(BgL_kfactorz00_3076), BgL_stackz00_3077);
							}
							{
								obj_t BgL_l1298z00_4421;

								BgL_l1298z00_4421 = CDR(BgL_l1298z00_3256);
								BgL_l1298z00_3256 = BgL_l1298z00_4421;
								goto BgL_zc3z04anonymousza31850ze3z87_3255;
							}
						}
					else
						{	/* Inline/inline.scm 219 */
							((bool_t) 1);
						}
				}
			}
			((((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_3075)))->BgL_bodyz00) =
				((BgL_nodez00_bglt)
					BGl_inlinezd2nodezd2zzinline_inlinez00((((BgL_letzd2funzd2_bglt)
								COBJECT(((BgL_letzd2funzd2_bglt) BgL_nodez00_3075)))->
							BgL_bodyz00), (long) CINT(BgL_kfactorz00_3076),
						BgL_stackz00_3077)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nodez00_3075));
		}

	}



/* &inline-node-switch1338 */
	BgL_nodez00_bglt BGl_z62inlinezd2nodezd2switch1338z62zzinline_inlinez00(obj_t
		BgL_envz00_3078, obj_t BgL_nodez00_3079, obj_t BgL_kfactorz00_3080,
		obj_t BgL_stackz00_3081)
	{
		{	/* Inline/inline.scm 203 */
			((((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_3079)))->BgL_testz00) =
				((BgL_nodez00_bglt)
					BGl_inlinezd2nodezd2zzinline_inlinez00((((BgL_switchz00_bglt)
								COBJECT(((BgL_switchz00_bglt) BgL_nodez00_3079)))->BgL_testz00),
						(long) CINT(BgL_kfactorz00_3080), BgL_stackz00_3081)), BUNSPEC);
			{	/* Inline/inline.scm 205 */
				obj_t BgL_g1297z00_3259;

				BgL_g1297z00_3259 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_3079)))->BgL_clausesz00);
				{
					obj_t BgL_l1295z00_3261;

					BgL_l1295z00_3261 = BgL_g1297z00_3259;
				BgL_zc3z04anonymousza31845ze3z87_3260:
					if (PAIRP(BgL_l1295z00_3261))
						{	/* Inline/inline.scm 207 */
							{	/* Inline/inline.scm 206 */
								obj_t BgL_clausez00_3262;

								BgL_clausez00_3262 = CAR(BgL_l1295z00_3261);
								{	/* Inline/inline.scm 206 */
									BgL_nodez00_bglt BgL_arg1847z00_3263;

									{	/* Inline/inline.scm 206 */
										obj_t BgL_arg1848z00_3264;

										BgL_arg1848z00_3264 = CDR(((obj_t) BgL_clausez00_3262));
										BgL_arg1847z00_3263 =
											BGl_inlinezd2nodezd2zzinline_inlinez00(
											((BgL_nodez00_bglt) BgL_arg1848z00_3264),
											(long) CINT(BgL_kfactorz00_3080), BgL_stackz00_3081);
									}
									{	/* Inline/inline.scm 206 */
										obj_t BgL_auxz00_4449;
										obj_t BgL_tmpz00_4447;

										BgL_auxz00_4449 = ((obj_t) BgL_arg1847z00_3263);
										BgL_tmpz00_4447 = ((obj_t) BgL_clausez00_3262);
										SET_CDR(BgL_tmpz00_4447, BgL_auxz00_4449);
							}}}
							{
								obj_t BgL_l1295z00_4452;

								BgL_l1295z00_4452 = CDR(BgL_l1295z00_3261);
								BgL_l1295z00_3261 = BgL_l1295z00_4452;
								goto BgL_zc3z04anonymousza31845ze3z87_3260;
							}
						}
					else
						{	/* Inline/inline.scm 207 */
							((bool_t) 1);
						}
				}
			}
			return ((BgL_nodez00_bglt) ((BgL_switchz00_bglt) BgL_nodez00_3079));
		}

	}



/* &inline-node-fail1336 */
	BgL_nodez00_bglt BGl_z62inlinezd2nodezd2fail1336z62zzinline_inlinez00(obj_t
		BgL_envz00_3082, obj_t BgL_nodez00_3083, obj_t BgL_kfactorz00_3084,
		obj_t BgL_stackz00_3085)
	{
		{	/* Inline/inline.scm 193 */
			{
				BgL_nodez00_bglt BgL_auxz00_4456;

				{	/* Inline/inline.scm 195 */
					BgL_nodez00_bglt BgL_arg1839z00_3266;

					BgL_arg1839z00_3266 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_3083)))->BgL_procz00);
					BgL_auxz00_4456 =
						BGl_inlinezd2nodezd2zzinline_inlinez00(BgL_arg1839z00_3266,
						(long) CINT(BgL_kfactorz00_3084), BgL_stackz00_3085);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_3083)))->BgL_procz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4456), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4463;

				{	/* Inline/inline.scm 196 */
					BgL_nodez00_bglt BgL_arg1840z00_3267;

					BgL_arg1840z00_3267 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_3083)))->BgL_msgz00);
					BgL_auxz00_4463 =
						BGl_inlinezd2nodezd2zzinline_inlinez00(BgL_arg1840z00_3267,
						(long) CINT(BgL_kfactorz00_3084), BgL_stackz00_3085);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_3083)))->BgL_msgz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4463), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4470;

				{	/* Inline/inline.scm 197 */
					BgL_nodez00_bglt BgL_arg1842z00_3268;

					BgL_arg1842z00_3268 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_3083)))->BgL_objz00);
					BgL_auxz00_4470 =
						BGl_inlinezd2nodezd2zzinline_inlinez00(BgL_arg1842z00_3268,
						(long) CINT(BgL_kfactorz00_3084), BgL_stackz00_3085);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_3083)))->BgL_objz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4470), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_failz00_bglt) BgL_nodez00_3083));
		}

	}



/* &inline-node-conditio1334 */
	BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2conditio1334z62zzinline_inlinez00(obj_t
		BgL_envz00_3086, obj_t BgL_nodez00_3087, obj_t BgL_kfactorz00_3088,
		obj_t BgL_stackz00_3089)
	{
		{	/* Inline/inline.scm 181 */
			{
				BgL_nodez00_bglt BgL_auxz00_4479;

				{	/* Tools/trace.sch 53 */
					BgL_nodez00_bglt BgL_arg1836z00_3270;

					BgL_arg1836z00_3270 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_3087)))->BgL_testz00);
					BgL_auxz00_4479 =
						BGl_inlinezd2nodezd2zzinline_inlinez00(BgL_arg1836z00_3270,
						(long) CINT(BgL_kfactorz00_3088), BgL_stackz00_3089);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_3087)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4479), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4486;

				{	/* Tools/trace.sch 53 */
					BgL_nodez00_bglt BgL_arg1837z00_3271;

					BgL_arg1837z00_3271 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_3087)))->BgL_truez00);
					BgL_auxz00_4486 =
						BGl_inlinezd2nodezd2zzinline_inlinez00(BgL_arg1837z00_3271,
						(long) CINT(BgL_kfactorz00_3088), BgL_stackz00_3089);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_3087)))->BgL_truez00) =
					((BgL_nodez00_bglt) BgL_auxz00_4486), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4493;

				{	/* Tools/trace.sch 53 */
					BgL_nodez00_bglt BgL_arg1838z00_3272;

					BgL_arg1838z00_3272 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_3087)))->BgL_falsez00);
					BgL_auxz00_4493 =
						BGl_inlinezd2nodezd2zzinline_inlinez00(BgL_arg1838z00_3272,
						(long) CINT(BgL_kfactorz00_3088), BgL_stackz00_3089);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_3087)))->BgL_falsez00) =
					((BgL_nodez00_bglt) BgL_auxz00_4493), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt) BgL_nodez00_3087));
		}

	}



/* &inline-node-setq1332 */
	BgL_nodez00_bglt BGl_z62inlinezd2nodezd2setq1332z62zzinline_inlinez00(obj_t
		BgL_envz00_3090, obj_t BgL_nodez00_3091, obj_t BgL_kfactorz00_3092,
		obj_t BgL_stackz00_3093)
	{
		{	/* Inline/inline.scm 174 */
			((((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_3091)))->BgL_valuez00) =
				((BgL_nodez00_bglt)
					BGl_inlinezd2nodezd2zzinline_inlinez00((((BgL_setqz00_bglt)
								COBJECT(((BgL_setqz00_bglt) BgL_nodez00_3091)))->BgL_valuez00),
						(long) CINT(BgL_kfactorz00_3092), BgL_stackz00_3093)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_setqz00_bglt) BgL_nodez00_3091));
		}

	}



/* &inline-node-cast1330 */
	BgL_nodez00_bglt BGl_z62inlinezd2nodezd2cast1330z62zzinline_inlinez00(obj_t
		BgL_envz00_3094, obj_t BgL_nodez00_3095, obj_t BgL_kfactorz00_3096,
		obj_t BgL_stackz00_3097)
	{
		{	/* Inline/inline.scm 167 */
			((((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_3095)))->BgL_argz00) =
				((BgL_nodez00_bglt)
					BGl_inlinezd2nodezd2zzinline_inlinez00((((BgL_castz00_bglt)
								COBJECT(((BgL_castz00_bglt) BgL_nodez00_3095)))->BgL_argz00),
						(long) CINT(BgL_kfactorz00_3096), BgL_stackz00_3097)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_castz00_bglt) BgL_nodez00_3095));
		}

	}



/* &inline-node-extern1328 */
	BgL_nodez00_bglt BGl_z62inlinezd2nodezd2extern1328z62zzinline_inlinez00(obj_t
		BgL_envz00_3098, obj_t BgL_nodez00_3099, obj_t BgL_kfactorz00_3100,
		obj_t BgL_stackz00_3101)
	{
		{	/* Inline/inline.scm 160 */
			BGl_inlinezd2nodeza2z12z62zzinline_inlinez00(
				(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_3099)))->BgL_exprza2za2),
				BgL_kfactorz00_3100, BgL_stackz00_3101);
			return ((BgL_nodez00_bglt) ((BgL_externz00_bglt) BgL_nodez00_3099));
		}

	}



/* &inline-node-funcall1326 */
	BgL_nodez00_bglt BGl_z62inlinezd2nodezd2funcall1326z62zzinline_inlinez00(obj_t
		BgL_envz00_3102, obj_t BgL_nodez00_3103, obj_t BgL_kfactorz00_3104,
		obj_t BgL_stackz00_3105)
	{
		{	/* Inline/inline.scm 150 */
			BGl_za2nonzd2inlinedzd2callsza2z00zzinline_walkz00 =
				ADDFX(BGl_za2nonzd2inlinedzd2callsza2z00zzinline_walkz00, BINT(1L));
			((((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_3103)))->BgL_funz00) =
				((BgL_nodez00_bglt)
					BGl_inlinezd2nodezd2zzinline_inlinez00((((BgL_funcallz00_bglt)
								COBJECT(((BgL_funcallz00_bglt) BgL_nodez00_3103)))->BgL_funz00),
						(long) CINT(BgL_kfactorz00_3104), BgL_stackz00_3105)), BUNSPEC);
			BGl_inlinezd2nodeza2z12z62zzinline_inlinez00((((BgL_funcallz00_bglt)
						COBJECT(((BgL_funcallz00_bglt) BgL_nodez00_3103)))->BgL_argsz00),
				BgL_kfactorz00_3104, BgL_stackz00_3105);
			return ((BgL_nodez00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_3103));
		}

	}



/* &inline-node-app-ly1324 */
	BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2appzd2ly1324zb0zzinline_inlinez00(obj_t
		BgL_envz00_3106, obj_t BgL_nodez00_3107, obj_t BgL_kfactorz00_3108,
		obj_t BgL_stackz00_3109)
	{
		{	/* Inline/inline.scm 141 */
			{
				BgL_nodez00_bglt BgL_auxz00_4536;

				{	/* Inline/inline.scm 143 */
					BgL_nodez00_bglt BgL_arg1808z00_3278;

					BgL_arg1808z00_3278 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_3107)))->BgL_funz00);
					BgL_auxz00_4536 =
						BGl_inlinezd2nodezd2zzinline_inlinez00(BgL_arg1808z00_3278,
						(long) CINT(BgL_kfactorz00_3108), BgL_stackz00_3109);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_3107)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4536), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4543;

				{	/* Inline/inline.scm 144 */
					BgL_nodez00_bglt BgL_arg1812z00_3279;

					BgL_arg1812z00_3279 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_3107)))->BgL_argz00);
					BgL_auxz00_4543 =
						BGl_inlinezd2nodezd2zzinline_inlinez00(BgL_arg1812z00_3279,
						(long) CINT(BgL_kfactorz00_3108), BgL_stackz00_3109);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_3107)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4543), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_3107));
		}

	}



/* &inline-node-app1322 */
	BgL_nodez00_bglt BGl_z62inlinezd2nodezd2app1322z62zzinline_inlinez00(obj_t
		BgL_envz00_3110, obj_t BgL_nodez00_3111, obj_t BgL_kfactorz00_3112,
		obj_t BgL_stackz00_3113)
	{
		{	/* Inline/inline.scm 134 */
			BGl_inlinezd2nodeza2z12z62zzinline_inlinez00(
				(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_3111)))->BgL_argsz00),
				BgL_kfactorz00_3112, BgL_stackz00_3113);
			return BGl_inlinezd2appzd2zzinline_appz00(((BgL_appz00_bglt)
					BgL_nodez00_3111), (long) CINT(BgL_kfactorz00_3112),
				BgL_stackz00_3113);
		}

	}



/* &inline-node-sync1320 */
	BgL_nodez00_bglt BGl_z62inlinezd2nodezd2sync1320z62zzinline_inlinez00(obj_t
		BgL_envz00_3114, obj_t BgL_nodez00_3115, obj_t BgL_kfactorz00_3116,
		obj_t BgL_stackz00_3117)
	{
		{	/* Inline/inline.scm 125 */
			((((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_3115)))->BgL_mutexz00) =
				((BgL_nodez00_bglt)
					BGl_inlinezd2nodezd2zzinline_inlinez00((((BgL_syncz00_bglt)
								COBJECT(((BgL_syncz00_bglt) BgL_nodez00_3115)))->BgL_mutexz00),
						(long) CINT(BgL_kfactorz00_3116), BgL_stackz00_3117)), BUNSPEC);
			((((BgL_syncz00_bglt) COBJECT(((BgL_syncz00_bglt) BgL_nodez00_3115)))->
					BgL_prelockz00) =
				((BgL_nodez00_bglt)
					BGl_inlinezd2nodezd2zzinline_inlinez00((((BgL_syncz00_bglt)
								COBJECT(((BgL_syncz00_bglt) BgL_nodez00_3115)))->
							BgL_prelockz00), (long) CINT(BgL_kfactorz00_3116),
						BgL_stackz00_3117)), BUNSPEC);
			((((BgL_syncz00_bglt) COBJECT(((BgL_syncz00_bglt) BgL_nodez00_3115)))->
					BgL_bodyz00) =
				((BgL_nodez00_bglt)
					BGl_inlinezd2nodezd2zzinline_inlinez00((((BgL_syncz00_bglt)
								COBJECT(((BgL_syncz00_bglt) BgL_nodez00_3115)))->BgL_bodyz00),
						(long) CINT(BgL_kfactorz00_3116), BgL_stackz00_3117)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_syncz00_bglt) BgL_nodez00_3115));
		}

	}



/* &inline-node-sequence1318 */
	BgL_nodez00_bglt
		BGl_z62inlinezd2nodezd2sequence1318z62zzinline_inlinez00(obj_t
		BgL_envz00_3118, obj_t BgL_nodez00_3119, obj_t BgL_kfactorz00_3120,
		obj_t BgL_stackz00_3121)
	{
		{	/* Inline/inline.scm 113 */
			if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CNST_TABLE_REF(13),
						(((BgL_sequencez00_bglt) COBJECT(
									((BgL_sequencez00_bglt) BgL_nodez00_3119)))->BgL_metaz00))))
				{	/* Tools/trace.sch 53 */
					((obj_t)
						BGl_disablezd2inliningz12zc0zzinline_inlinez00(
							((BgL_nodez00_bglt) ((BgL_sequencez00_bglt) BgL_nodez00_3119))));
				}
			else
				{	/* Tools/trace.sch 53 */
					BGl_inlinezd2nodeza2z12z62zzinline_inlinez00(
						(((BgL_sequencez00_bglt) COBJECT(
									((BgL_sequencez00_bglt) BgL_nodez00_3119)))->BgL_nodesz00),
						BgL_kfactorz00_3120, BgL_stackz00_3121);
				}
			return ((BgL_nodez00_bglt) ((BgL_sequencez00_bglt) BgL_nodez00_3119));
		}

	}



/* &inline-node-kwote1316 */
	BgL_nodez00_bglt BGl_z62inlinezd2nodezd2kwote1316z62zzinline_inlinez00(obj_t
		BgL_envz00_3122, obj_t BgL_nodez00_3123, obj_t BgL_kfactorz00_3124,
		obj_t BgL_stackz00_3125)
	{
		{	/* Inline/inline.scm 107 */
			return ((BgL_nodez00_bglt) ((BgL_kwotez00_bglt) BgL_nodez00_3123));
		}

	}



/* &inline-node-var1314 */
	BgL_nodez00_bglt BGl_z62inlinezd2nodezd2var1314z62zzinline_inlinez00(obj_t
		BgL_envz00_3126, obj_t BgL_nodez00_3127, obj_t BgL_kfactorz00_3128,
		obj_t BgL_stackz00_3129)
	{
		{	/* Inline/inline.scm 81 */
			{	/* Inline/inline.scm 83 */
				bool_t BgL_test2151z00_4595;

				if (((long) CINT(BGl_za2optimza2z00zzengine_paramz00) > 0L))
					{	/* Inline/inline.scm 84 */
						bool_t BgL_test2153z00_4599;

						{	/* Inline/inline.scm 84 */
							BgL_variablez00_bglt BgL_arg1762z00_3285;

							BgL_arg1762z00_3285 =
								(((BgL_varz00_bglt) COBJECT(
										((BgL_varz00_bglt) BgL_nodez00_3127)))->BgL_variablez00);
							{	/* Inline/inline.scm 84 */
								obj_t BgL_classz00_3286;

								BgL_classz00_3286 = BGl_globalz00zzast_varz00;
								{	/* Inline/inline.scm 84 */
									BgL_objectz00_bglt BgL_arg1807z00_3287;

									{	/* Inline/inline.scm 84 */
										obj_t BgL_tmpz00_4602;

										BgL_tmpz00_4602 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg1762z00_3285));
										BgL_arg1807z00_3287 =
											(BgL_objectz00_bglt) (BgL_tmpz00_4602);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Inline/inline.scm 84 */
											long BgL_idxz00_3288;

											BgL_idxz00_3288 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3287);
											BgL_test2153z00_4599 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_3288 + 2L)) == BgL_classz00_3286);
										}
									else
										{	/* Inline/inline.scm 84 */
											bool_t BgL_res1975z00_3291;

											{	/* Inline/inline.scm 84 */
												obj_t BgL_oclassz00_3292;

												{	/* Inline/inline.scm 84 */
													obj_t BgL_arg1815z00_3293;
													long BgL_arg1816z00_3294;

													BgL_arg1815z00_3293 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Inline/inline.scm 84 */
														long BgL_arg1817z00_3295;

														BgL_arg1817z00_3295 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3287);
														BgL_arg1816z00_3294 =
															(BgL_arg1817z00_3295 - OBJECT_TYPE);
													}
													BgL_oclassz00_3292 =
														VECTOR_REF(BgL_arg1815z00_3293,
														BgL_arg1816z00_3294);
												}
												{	/* Inline/inline.scm 84 */
													bool_t BgL__ortest_1115z00_3296;

													BgL__ortest_1115z00_3296 =
														(BgL_classz00_3286 == BgL_oclassz00_3292);
													if (BgL__ortest_1115z00_3296)
														{	/* Inline/inline.scm 84 */
															BgL_res1975z00_3291 = BgL__ortest_1115z00_3296;
														}
													else
														{	/* Inline/inline.scm 84 */
															long BgL_odepthz00_3297;

															{	/* Inline/inline.scm 84 */
																obj_t BgL_arg1804z00_3298;

																BgL_arg1804z00_3298 = (BgL_oclassz00_3292);
																BgL_odepthz00_3297 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_3298);
															}
															if ((2L < BgL_odepthz00_3297))
																{	/* Inline/inline.scm 84 */
																	obj_t BgL_arg1802z00_3299;

																	{	/* Inline/inline.scm 84 */
																		obj_t BgL_arg1803z00_3300;

																		BgL_arg1803z00_3300 = (BgL_oclassz00_3292);
																		BgL_arg1802z00_3299 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_3300, 2L);
																	}
																	BgL_res1975z00_3291 =
																		(BgL_arg1802z00_3299 == BgL_classz00_3286);
																}
															else
																{	/* Inline/inline.scm 84 */
																	BgL_res1975z00_3291 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2153z00_4599 = BgL_res1975z00_3291;
										}
								}
							}
						}
						if (BgL_test2153z00_4599)
							{	/* Inline/inline.scm 84 */
								if (
									((((BgL_globalz00_bglt) COBJECT(
													((BgL_globalz00_bglt)
														(((BgL_varz00_bglt) COBJECT(
																	((BgL_varz00_bglt) BgL_nodez00_3127)))->
															BgL_variablez00))))->BgL_importz00) ==
										CNST_TABLE_REF(2)))
									{	/* Inline/inline.scm 85 */
										if (
											(((BgL_globalz00_bglt) COBJECT(
														((BgL_globalz00_bglt)
															(((BgL_varz00_bglt) COBJECT(
																		((BgL_varz00_bglt) BgL_nodez00_3127)))->
																BgL_variablez00))))->BgL_evalzf3zf3))
											{	/* Inline/inline.scm 86 */
												BgL_test2151z00_4595 = ((bool_t) 0);
											}
										else
											{	/* Inline/inline.scm 87 */
												bool_t BgL__ortest_1157z00_3301;

												BgL__ortest_1157z00_3301 =
													(
													(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_globalz00_bglt)
																		(((BgL_varz00_bglt) COBJECT(
																					((BgL_varz00_bglt)
																						BgL_nodez00_3127)))->
																			BgL_variablez00)))))->BgL_accessz00) ==
													CNST_TABLE_REF(15));
												if (BgL__ortest_1157z00_3301)
													{	/* Inline/inline.scm 87 */
														BgL_test2151z00_4595 = BgL__ortest_1157z00_3301;
													}
												else
													{	/* Inline/inline.scm 87 */
														if (
															((((BgL_globalz00_bglt) COBJECT(
																			((BgL_globalz00_bglt)
																				(((BgL_varz00_bglt) COBJECT(
																							((BgL_varz00_bglt)
																								BgL_nodez00_3127)))->
																					BgL_variablez00))))->BgL_initz00) ==
																BTRUE))
															{	/* Inline/inline.scm 89 */

																BgL_test2151z00_4595 =
																	(
																	(((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					((BgL_globalz00_bglt)
																						(((BgL_varz00_bglt) COBJECT(
																									((BgL_varz00_bglt)
																										BgL_nodez00_3127)))->
																							BgL_variablez00)))))->
																		BgL_occurrencewz00) == 1L);
															}
														else
															{	/* Inline/inline.scm 88 */
																BgL_test2151z00_4595 = ((bool_t) 0);
															}
													}
											}
									}
								else
									{	/* Inline/inline.scm 85 */
										BgL_test2151z00_4595 = ((bool_t) 0);
									}
							}
						else
							{	/* Inline/inline.scm 84 */
								BgL_test2151z00_4595 = ((bool_t) 0);
							}
					}
				else
					{	/* Inline/inline.scm 83 */
						BgL_test2151z00_4595 = ((bool_t) 0);
					}
				if (BgL_test2151z00_4595)
					{	/* Inline/inline.scm 91 */
						bool_t BgL_test2161z00_4657;

						{	/* Inline/inline.scm 91 */
							bool_t BgL_test2162z00_4658;

							{	/* Inline/inline.scm 91 */
								obj_t BgL_arg1747z00_3302;

								BgL_arg1747z00_3302 =
									(((BgL_globalz00_bglt) COBJECT(
											((BgL_globalz00_bglt)
												(((BgL_varz00_bglt) COBJECT(
															((BgL_varz00_bglt) BgL_nodez00_3127)))->
													BgL_variablez00))))->BgL_srcz00);
								{	/* Inline/inline.scm 91 */
									obj_t BgL_classz00_3303;

									BgL_classz00_3303 = BGl_atomz00zzast_nodez00;
									if (BGL_OBJECTP(BgL_arg1747z00_3302))
										{	/* Inline/inline.scm 91 */
											BgL_objectz00_bglt BgL_arg1807z00_3304;

											BgL_arg1807z00_3304 =
												(BgL_objectz00_bglt) (BgL_arg1747z00_3302);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Inline/inline.scm 91 */
													long BgL_idxz00_3305;

													BgL_idxz00_3305 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3304);
													BgL_test2162z00_4658 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_3305 + 2L)) == BgL_classz00_3303);
												}
											else
												{	/* Inline/inline.scm 91 */
													bool_t BgL_res1976z00_3308;

													{	/* Inline/inline.scm 91 */
														obj_t BgL_oclassz00_3309;

														{	/* Inline/inline.scm 91 */
															obj_t BgL_arg1815z00_3310;
															long BgL_arg1816z00_3311;

															BgL_arg1815z00_3310 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Inline/inline.scm 91 */
																long BgL_arg1817z00_3312;

																BgL_arg1817z00_3312 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3304);
																BgL_arg1816z00_3311 =
																	(BgL_arg1817z00_3312 - OBJECT_TYPE);
															}
															BgL_oclassz00_3309 =
																VECTOR_REF(BgL_arg1815z00_3310,
																BgL_arg1816z00_3311);
														}
														{	/* Inline/inline.scm 91 */
															bool_t BgL__ortest_1115z00_3313;

															BgL__ortest_1115z00_3313 =
																(BgL_classz00_3303 == BgL_oclassz00_3309);
															if (BgL__ortest_1115z00_3313)
																{	/* Inline/inline.scm 91 */
																	BgL_res1976z00_3308 =
																		BgL__ortest_1115z00_3313;
																}
															else
																{	/* Inline/inline.scm 91 */
																	long BgL_odepthz00_3314;

																	{	/* Inline/inline.scm 91 */
																		obj_t BgL_arg1804z00_3315;

																		BgL_arg1804z00_3315 = (BgL_oclassz00_3309);
																		BgL_odepthz00_3314 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_3315);
																	}
																	if ((2L < BgL_odepthz00_3314))
																		{	/* Inline/inline.scm 91 */
																			obj_t BgL_arg1802z00_3316;

																			{	/* Inline/inline.scm 91 */
																				obj_t BgL_arg1803z00_3317;

																				BgL_arg1803z00_3317 =
																					(BgL_oclassz00_3309);
																				BgL_arg1802z00_3316 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_3317, 2L);
																			}
																			BgL_res1976z00_3308 =
																				(BgL_arg1802z00_3316 ==
																				BgL_classz00_3303);
																		}
																	else
																		{	/* Inline/inline.scm 91 */
																			BgL_res1976z00_3308 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2162z00_4658 = BgL_res1976z00_3308;
												}
										}
									else
										{	/* Inline/inline.scm 91 */
											BgL_test2162z00_4658 = ((bool_t) 0);
										}
								}
							}
							if (BgL_test2162z00_4658)
								{	/* Inline/inline.scm 93 */
									bool_t BgL_test2167z00_4685;

									{	/* Inline/inline.scm 93 */
										obj_t BgL_tmpz00_4686;

										BgL_tmpz00_4686 =
											(((BgL_atomz00_bglt) COBJECT(
													((BgL_atomz00_bglt)
														(((BgL_globalz00_bglt) COBJECT(
																	((BgL_globalz00_bglt)
																		(((BgL_varz00_bglt) COBJECT(
																					((BgL_varz00_bglt)
																						BgL_nodez00_3127)))->
																			BgL_variablez00))))->BgL_srcz00))))->
											BgL_valuez00);
										BgL_test2167z00_4685 = BIGNUMP(BgL_tmpz00_4686);
									}
									if (BgL_test2167z00_4685)
										{	/* Inline/inline.scm 93 */
											BgL_test2161z00_4657 = ((bool_t) 0);
										}
									else
										{	/* Inline/inline.scm 93 */
											BgL_test2161z00_4657 = ((bool_t) 1);
										}
								}
							else
								{	/* Inline/inline.scm 91 */
									BgL_test2161z00_4657 = ((bool_t) 0);
								}
						}
						if (BgL_test2161z00_4657)
							{	/* Inline/inline.scm 91 */
								{	/* Inline/inline.scm 96 */
									obj_t BgL_arg1714z00_3318;
									obj_t BgL_arg1717z00_3319;

									BgL_arg1714z00_3318 =
										(((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt)
													((BgL_varz00_bglt) BgL_nodez00_3127))))->BgL_locz00);
									BgL_arg1717z00_3319 =
										(((BgL_globalz00_bglt) COBJECT(
												((BgL_globalz00_bglt)
													(((BgL_varz00_bglt) COBJECT(
																((BgL_varz00_bglt) BgL_nodez00_3127)))->
														BgL_variablez00))))->BgL_srcz00);
									return BGl_alphatiza7eza7zzast_alphatiza7eza7(BNIL, BNIL,
										BgL_arg1714z00_3318,
										((BgL_nodez00_bglt) BgL_arg1717z00_3319));
								}
							}
						else
							{	/* Inline/inline.scm 97 */
								bool_t BgL_test2168z00_4703;

								{	/* Inline/inline.scm 97 */
									bool_t BgL_test2169z00_4704;

									{	/* Inline/inline.scm 97 */
										obj_t BgL_arg1738z00_3320;

										BgL_arg1738z00_3320 =
											(((BgL_globalz00_bglt) COBJECT(
													((BgL_globalz00_bglt)
														(((BgL_varz00_bglt) COBJECT(
																	((BgL_varz00_bglt) BgL_nodez00_3127)))->
															BgL_variablez00))))->BgL_srcz00);
										{	/* Inline/inline.scm 97 */
											obj_t BgL_classz00_3321;

											BgL_classz00_3321 = BGl_kwotez00zzast_nodez00;
											if (BGL_OBJECTP(BgL_arg1738z00_3320))
												{	/* Inline/inline.scm 97 */
													BgL_objectz00_bglt BgL_arg1807z00_3322;

													BgL_arg1807z00_3322 =
														(BgL_objectz00_bglt) (BgL_arg1738z00_3320);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Inline/inline.scm 97 */
															long BgL_idxz00_3323;

															BgL_idxz00_3323 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3322);
															BgL_test2169z00_4704 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_3323 + 2L)) == BgL_classz00_3321);
														}
													else
														{	/* Inline/inline.scm 97 */
															bool_t BgL_res1977z00_3326;

															{	/* Inline/inline.scm 97 */
																obj_t BgL_oclassz00_3327;

																{	/* Inline/inline.scm 97 */
																	obj_t BgL_arg1815z00_3328;
																	long BgL_arg1816z00_3329;

																	BgL_arg1815z00_3328 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Inline/inline.scm 97 */
																		long BgL_arg1817z00_3330;

																		BgL_arg1817z00_3330 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3322);
																		BgL_arg1816z00_3329 =
																			(BgL_arg1817z00_3330 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_3327 =
																		VECTOR_REF(BgL_arg1815z00_3328,
																		BgL_arg1816z00_3329);
																}
																{	/* Inline/inline.scm 97 */
																	bool_t BgL__ortest_1115z00_3331;

																	BgL__ortest_1115z00_3331 =
																		(BgL_classz00_3321 == BgL_oclassz00_3327);
																	if (BgL__ortest_1115z00_3331)
																		{	/* Inline/inline.scm 97 */
																			BgL_res1977z00_3326 =
																				BgL__ortest_1115z00_3331;
																		}
																	else
																		{	/* Inline/inline.scm 97 */
																			long BgL_odepthz00_3332;

																			{	/* Inline/inline.scm 97 */
																				obj_t BgL_arg1804z00_3333;

																				BgL_arg1804z00_3333 =
																					(BgL_oclassz00_3327);
																				BgL_odepthz00_3332 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_3333);
																			}
																			if ((2L < BgL_odepthz00_3332))
																				{	/* Inline/inline.scm 97 */
																					obj_t BgL_arg1802z00_3334;

																					{	/* Inline/inline.scm 97 */
																						obj_t BgL_arg1803z00_3335;

																						BgL_arg1803z00_3335 =
																							(BgL_oclassz00_3327);
																						BgL_arg1802z00_3334 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_3335, 2L);
																					}
																					BgL_res1977z00_3326 =
																						(BgL_arg1802z00_3334 ==
																						BgL_classz00_3321);
																				}
																			else
																				{	/* Inline/inline.scm 97 */
																					BgL_res1977z00_3326 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2169z00_4704 = BgL_res1977z00_3326;
														}
												}
											else
												{	/* Inline/inline.scm 97 */
													BgL_test2169z00_4704 = ((bool_t) 0);
												}
										}
									}
									if (BgL_test2169z00_4704)
										{	/* Inline/inline.scm 97 */
											BgL_test2168z00_4703 =
												(BGl_za2inlinezd2modeza2zd2zzinline_walkz00 ==
												CNST_TABLE_REF(0));
										}
									else
										{	/* Inline/inline.scm 97 */
											BgL_test2168z00_4703 = ((bool_t) 0);
										}
								}
								if (BgL_test2168z00_4703)
									{	/* Inline/inline.scm 97 */
										{	/* Inline/inline.scm 99 */
											obj_t BgL_arg1735z00_3336;
											obj_t BgL_arg1736z00_3337;

											BgL_arg1735z00_3336 =
												(((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt)
															((BgL_varz00_bglt) BgL_nodez00_3127))))->
												BgL_locz00);
											BgL_arg1736z00_3337 =
												(((BgL_globalz00_bglt)
													COBJECT(((BgL_globalz00_bglt) (((BgL_varz00_bglt)
																	COBJECT(((BgL_varz00_bglt)
																			BgL_nodez00_3127)))->BgL_variablez00))))->
												BgL_srcz00);
											return BGl_alphatiza7eza7zzast_alphatiza7eza7(BNIL, BNIL,
												BgL_arg1735z00_3336,
												((BgL_nodez00_bglt) BgL_arg1736z00_3337));
										}
									}
								else
									{	/* Inline/inline.scm 97 */
										return
											((BgL_nodez00_bglt) ((BgL_varz00_bglt) BgL_nodez00_3127));
									}
							}
					}
				else
					{	/* Inline/inline.scm 83 */
						return ((BgL_nodez00_bglt) ((BgL_varz00_bglt) BgL_nodez00_3127));
					}
			}
		}

	}



/* &inline-node-atom1312 */
	BgL_nodez00_bglt BGl_z62inlinezd2nodezd2atom1312z62zzinline_inlinez00(obj_t
		BgL_envz00_3130, obj_t BgL_nodez00_3131, obj_t BgL_kfactorz00_3132,
		obj_t BgL_stackz00_3133)
	{
		{	/* Inline/inline.scm 75 */
			return ((BgL_nodez00_bglt) ((BgL_atomz00_bglt) BgL_nodez00_3131));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzinline_inlinez00(void)
	{
		{	/* Inline/inline.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2027z00zzinline_inlinez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2027z00zzinline_inlinez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2027z00zzinline_inlinez00));
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2027z00zzinline_inlinez00));
			BGl_modulezd2initializa7ationz75zzinline_walkz00(385476819L,
				BSTRING_TO_STRING(BGl_string2027z00zzinline_inlinez00));
			BGl_modulezd2initializa7ationz75zzinline_appz00(148760855L,
				BSTRING_TO_STRING(BGl_string2027z00zzinline_inlinez00));
			BGl_modulezd2initializa7ationz75zzast_alphatiza7eza7(53595773L,
				BSTRING_TO_STRING(BGl_string2027z00zzinline_inlinez00));
			BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string2027z00zzinline_inlinez00));
			BGl_modulezd2initializa7ationz75zzast_walkz00(343174225L,
				BSTRING_TO_STRING(BGl_string2027z00zzinline_inlinez00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2027z00zzinline_inlinez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2027z00zzinline_inlinez00));
			return
				BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2027z00zzinline_inlinez00));
		}

	}

#ifdef __cplusplus
}
#endif
