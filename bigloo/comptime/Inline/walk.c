/*===========================================================================*/
/*   (Inline/walk.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Inline/walk.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_INLINE_WALK_TYPE_DEFINITIONS
#define BGL_INLINE_WALK_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;


#endif													// BGL_INLINE_WALK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_za2optimza2z00zzengine_paramz00;
	static obj_t BGl_requirezd2initializa7ationz75zzinline_walkz00 = BUNSPEC;
	extern obj_t BGl_inlinezd2sfunz12zc0zzinline_inlinez00(BgL_variablez00_bglt,
		long, obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzinline_walkz00(void);
	BGL_EXPORTED_DEF obj_t BGl_za2nonzd2inlinedzd2callsza2z00zzinline_walkz00 =
		BUNSPEC;
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzinline_walkz00(void);
	extern obj_t BGl_leavezd2functionzd2zztools_errorz00(void);
	static obj_t BGl_objectzd2initzd2zzinline_walkz00(void);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_inlinezd2setupz12zc0zzinline_walkz00(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzinline_walkz00(void);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	BGL_EXPORTED_DEF obj_t BGl_za2kfactorza2z00zzinline_walkz00 = BUNSPEC;
	extern obj_t BGl_za2inliningzd2kfactorza2zd2zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t BGl_inlinezd2walkz12zc0zzinline_walkz00(obj_t, obj_t);
	extern obj_t BGl_removezd2varzd2zzast_removez00(obj_t, obj_t);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzinline_walkz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinline_inlinez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_occurz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_removez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_enterzd2functionzd2zztools_errorz00(obj_t);
	extern obj_t BGl_occurzd2varzd2zzast_occurz00(obj_t);
	static obj_t BGl_cnstzd2initzd2zzinline_walkz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzinline_walkz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzinline_walkz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzinline_walkz00(void);
	static obj_t BGl_z62showzd2statz12za2zzinline_walkz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2inlinedzd2callsza2zd2zzinline_walkz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2inlinezd2modeza2zd2zzinline_walkz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_2maxz00zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	static obj_t BGl_z62resetzd2statz12za2zzinline_walkz00(obj_t);
	static obj_t BGl_z62inlinezd2walkz12za2zzinline_walkz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62inlinezd2setupz12za2zzinline_walkz00(obj_t, obj_t);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t __cnst[9];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_showzd2statz12zd2envz12zzinline_walkz00,
		BgL_bgl_za762showza7d2statza711628za7,
		BGl_z62showzd2statz12za2zzinline_walkz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_inlinezd2setupz12zd2envz12zzinline_walkz00,
		BgL_bgl_za762inlineza7d2setu1629z00,
		BGl_z62inlinezd2setupz12za2zzinline_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1612z00zzinline_walkz00,
		BgL_bgl_string1612za700za7za7i1630za7, "Inlining", 8);
	      DEFINE_STRING(BGl_string1613z00zzinline_walkz00,
		BgL_bgl_string1613za700za7za7i1631za7, "   . ", 5);
	      DEFINE_STRING(BGl_string1614z00zzinline_walkz00,
		BgL_bgl_string1614za700za7za7i1632za7, "failure during prelude hook", 27);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_resetzd2statz12zd2envz12zzinline_walkz00,
		BgL_bgl_za762resetza7d2statza71633za7,
		BGl_z62resetzd2statz12za2zzinline_walkz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1615z00zzinline_walkz00,
		BgL_bgl_string1615za700za7za7i1634za7, " error", 6);
	      DEFINE_STRING(BGl_string1616z00zzinline_walkz00,
		BgL_bgl_string1616za700za7za7i1635za7, "s", 1);
	      DEFINE_STRING(BGl_string1617z00zzinline_walkz00,
		BgL_bgl_string1617za700za7za7i1636za7, "", 0);
	      DEFINE_STRING(BGl_string1618z00zzinline_walkz00,
		BgL_bgl_string1618za700za7za7i1637za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string1619z00zzinline_walkz00,
		BgL_bgl_string1619za700za7za7i1638za7, "failure during postlude hook", 28);
	      DEFINE_STRING(BGl_string1620z00zzinline_walkz00,
		BgL_bgl_string1620za700za7za7i1639za7, "\n", 1);
	      DEFINE_STRING(BGl_string1621z00zzinline_walkz00,
		BgL_bgl_string1621za700za7za7i1640za7, "      inlined calls: ", 21);
	      DEFINE_STRING(BGl_string1622z00zzinline_walkz00,
		BgL_bgl_string1622za700za7za7i1641za7, "      non inlined calls: ", 25);
	      DEFINE_STRING(BGl_string1623z00zzinline_walkz00,
		BgL_bgl_string1623za700za7za7i1642za7, "inline-setup!", 13);
	      DEFINE_STRING(BGl_string1624z00zzinline_walkz00,
		BgL_bgl_string1624za700za7za7i1643za7, "Illegal mode", 12);
	      DEFINE_STRING(BGl_string1625z00zzinline_walkz00,
		BgL_bgl_string1625za700za7za7i1644za7, "inline_walk", 11);
	      DEFINE_STRING(BGl_string1626z00zzinline_walkz00,
		BgL_bgl_string1626za700za7za7i1645za7,
		"predicate reducer (show-stat!) inline sifun (all reducer) pass-started (reset-stat!) all ",
		89);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_inlinezd2walkz12zd2envz12zzinline_walkz00,
		BgL_bgl_za762inlineza7d2walk1646z00,
		BGl_z62inlinezd2walkz12za2zzinline_walkz00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzinline_walkz00));
		   
			 ADD_ROOT((void *) (&BGl_za2nonzd2inlinedzd2callsza2z00zzinline_walkz00));
		     ADD_ROOT((void *) (&BGl_za2kfactorza2z00zzinline_walkz00));
		     ADD_ROOT((void *) (&BGl_za2inlinedzd2callsza2zd2zzinline_walkz00));
		     ADD_ROOT((void *) (&BGl_za2inlinezd2modeza2zd2zzinline_walkz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzinline_walkz00(long
		BgL_checksumz00_1666, char *BgL_fromz00_1667)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzinline_walkz00))
				{
					BGl_requirezd2initializa7ationz75zzinline_walkz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzinline_walkz00();
					BGl_libraryzd2moduleszd2initz00zzinline_walkz00();
					BGl_cnstzd2initzd2zzinline_walkz00();
					BGl_importedzd2moduleszd2initz00zzinline_walkz00();
					return BGl_toplevelzd2initzd2zzinline_walkz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzinline_walkz00(void)
	{
		{	/* Inline/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"inline_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"inline_walk");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"inline_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "inline_walk");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "inline_walk");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "inline_walk");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "inline_walk");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "inline_walk");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"inline_walk");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzinline_walkz00(void)
	{
		{	/* Inline/walk.scm 15 */
			{	/* Inline/walk.scm 15 */
				obj_t BgL_cportz00_1649;

				{	/* Inline/walk.scm 15 */
					obj_t BgL_stringz00_1656;

					BgL_stringz00_1656 = BGl_string1626z00zzinline_walkz00;
					{	/* Inline/walk.scm 15 */
						obj_t BgL_startz00_1657;

						BgL_startz00_1657 = BINT(0L);
						{	/* Inline/walk.scm 15 */
							obj_t BgL_endz00_1658;

							BgL_endz00_1658 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1656)));
							{	/* Inline/walk.scm 15 */

								BgL_cportz00_1649 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1656, BgL_startz00_1657, BgL_endz00_1658);
				}}}}
				{
					long BgL_iz00_1650;

					BgL_iz00_1650 = 8L;
				BgL_loopz00_1651:
					if ((BgL_iz00_1650 == -1L))
						{	/* Inline/walk.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Inline/walk.scm 15 */
							{	/* Inline/walk.scm 15 */
								obj_t BgL_arg1627z00_1652;

								{	/* Inline/walk.scm 15 */

									{	/* Inline/walk.scm 15 */
										obj_t BgL_locationz00_1654;

										BgL_locationz00_1654 = BBOOL(((bool_t) 0));
										{	/* Inline/walk.scm 15 */

											BgL_arg1627z00_1652 =
												BGl_readz00zz__readerz00(BgL_cportz00_1649,
												BgL_locationz00_1654);
										}
									}
								}
								{	/* Inline/walk.scm 15 */
									int BgL_tmpz00_1694;

									BgL_tmpz00_1694 = (int) (BgL_iz00_1650);
									CNST_TABLE_SET(BgL_tmpz00_1694, BgL_arg1627z00_1652);
							}}
							{	/* Inline/walk.scm 15 */
								int BgL_auxz00_1655;

								BgL_auxz00_1655 = (int) ((BgL_iz00_1650 - 1L));
								{
									long BgL_iz00_1699;

									BgL_iz00_1699 = (long) (BgL_auxz00_1655);
									BgL_iz00_1650 = BgL_iz00_1699;
									goto BgL_loopz00_1651;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzinline_walkz00(void)
	{
		{	/* Inline/walk.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzinline_walkz00(void)
	{
		{	/* Inline/walk.scm 15 */
			BGl_za2inlinezd2modeza2zd2zzinline_walkz00 = CNST_TABLE_REF(0);
			BGl_za2inlinedzd2callsza2zd2zzinline_walkz00 = BINT(0L);
			BGl_za2nonzd2inlinedzd2callsza2z00zzinline_walkz00 = BINT(0L);
			return (BGl_za2kfactorza2z00zzinline_walkz00 = BINT(1L), BUNSPEC);
		}

	}



/* inline-walk! */
	BGL_EXPORTED_DEF obj_t BGl_inlinezd2walkz12zc0zzinline_walkz00(obj_t
		BgL_globalsz00_3, obj_t BgL_whatz00_4)
	{
		{	/* Inline/walk.scm 41 */
			{	/* Inline/walk.scm 43 */
				obj_t BgL_list1245z00_1364;

				{	/* Inline/walk.scm 43 */
					obj_t BgL_arg1248z00_1365;

					{	/* Inline/walk.scm 43 */
						obj_t BgL_arg1249z00_1366;

						BgL_arg1249z00_1366 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1248z00_1365 =
							MAKE_YOUNG_PAIR(BGl_string1612z00zzinline_walkz00,
							BgL_arg1249z00_1366);
					}
					BgL_list1245z00_1364 =
						MAKE_YOUNG_PAIR(BGl_string1613z00zzinline_walkz00,
						BgL_arg1248z00_1365);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1245z00_1364);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string1612z00zzinline_walkz00;
			{	/* Inline/walk.scm 43 */
				obj_t BgL_g1111z00_1367;
				obj_t BgL_g1112z00_1368;

				{	/* Inline/walk.scm 43 */
					obj_t BgL_list1286z00_1381;

					BgL_list1286z00_1381 =
						MAKE_YOUNG_PAIR(BGl_resetzd2statz12zd2envz12zzinline_walkz00, BNIL);
					BgL_g1111z00_1367 = BgL_list1286z00_1381;
				}
				BgL_g1112z00_1368 = CNST_TABLE_REF(1);
				{
					obj_t BgL_hooksz00_1370;
					obj_t BgL_hnamesz00_1371;

					BgL_hooksz00_1370 = BgL_g1111z00_1367;
					BgL_hnamesz00_1371 = BgL_g1112z00_1368;
				BgL_zc3z04anonymousza31250ze3z87_1372:
					if (NULLP(BgL_hooksz00_1370))
						{	/* Inline/walk.scm 43 */
							CNST_TABLE_REF(2);
						}
					else
						{	/* Inline/walk.scm 43 */
							bool_t BgL_test1650z00_1718;

							{	/* Inline/walk.scm 43 */
								obj_t BgL_fun1285z00_1379;

								BgL_fun1285z00_1379 = CAR(((obj_t) BgL_hooksz00_1370));
								BgL_test1650z00_1718 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1285z00_1379));
							}
							if (BgL_test1650z00_1718)
								{	/* Inline/walk.scm 43 */
									obj_t BgL_arg1268z00_1376;
									obj_t BgL_arg1272z00_1377;

									BgL_arg1268z00_1376 = CDR(((obj_t) BgL_hooksz00_1370));
									BgL_arg1272z00_1377 = CDR(((obj_t) BgL_hnamesz00_1371));
									{
										obj_t BgL_hnamesz00_1730;
										obj_t BgL_hooksz00_1729;

										BgL_hooksz00_1729 = BgL_arg1268z00_1376;
										BgL_hnamesz00_1730 = BgL_arg1272z00_1377;
										BgL_hnamesz00_1371 = BgL_hnamesz00_1730;
										BgL_hooksz00_1370 = BgL_hooksz00_1729;
										goto BgL_zc3z04anonymousza31250ze3z87_1372;
									}
								}
							else
								{	/* Inline/walk.scm 43 */
									obj_t BgL_arg1284z00_1378;

									BgL_arg1284z00_1378 = CAR(((obj_t) BgL_hnamesz00_1371));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string1612z00zzinline_walkz00,
										BGl_string1614z00zzinline_walkz00, BgL_arg1284z00_1378);
								}
						}
				}
			}
			BGl_inlinezd2setupz12zc0zzinline_walkz00(BgL_whatz00_4);
			if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_whatz00_4,
						CNST_TABLE_REF(3))))
				{	/* Inline/walk.scm 49 */
					BGl_occurzd2varzd2zzast_occurz00(BgL_globalsz00_3);
				}
			else
				{	/* Inline/walk.scm 49 */
					BFALSE;
				}
			{
				obj_t BgL_l1235z00_1384;

				BgL_l1235z00_1384 = BgL_globalsz00_3;
			BgL_zc3z04anonymousza31288ze3z87_1385:
				if (PAIRP(BgL_l1235z00_1384))
					{	/* Inline/walk.scm 52 */
						{	/* Inline/walk.scm 53 */
							obj_t BgL_gz00_1387;

							BgL_gz00_1387 = CAR(BgL_l1235z00_1384);
							{	/* Inline/walk.scm 53 */
								obj_t BgL_kfactorz00_1388;

								if (
									((((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt)
														(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_globalz00_bglt) BgL_gz00_1387))))->
															BgL_valuez00))))->BgL_classz00) ==
										CNST_TABLE_REF(4)))
									{	/* Inline/walk.scm 53 */
										BgL_kfactorz00_1388 = BINT(1L);
									}
								else
									{	/* Inline/walk.scm 53 */
										BgL_kfactorz00_1388 = BGl_za2kfactorza2z00zzinline_walkz00;
									}
								{	/* Inline/walk.scm 56 */
									obj_t BgL_arg1304z00_1389;

									BgL_arg1304z00_1389 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_gz00_1387))))->BgL_idz00);
									BGl_enterzd2functionzd2zztools_errorz00(BgL_arg1304z00_1389);
								}
								BGl_inlinezd2sfunz12zc0zzinline_inlinez00(
									((BgL_variablez00_bglt) BgL_gz00_1387),
									(long) CINT(BgL_kfactorz00_1388), BNIL);
								BGl_leavezd2functionzd2zztools_errorz00();
						}}
						{
							obj_t BgL_l1235z00_1760;

							BgL_l1235z00_1760 = CDR(BgL_l1235z00_1384);
							BgL_l1235z00_1384 = BgL_l1235z00_1760;
							goto BgL_zc3z04anonymousza31288ze3z87_1385;
						}
					}
				else
					{	/* Inline/walk.scm 52 */
						((bool_t) 1);
					}
			}
			{
				obj_t BgL_globalsz00_1399;
				obj_t BgL_newzd2globalszd2_1400;

				BgL_globalsz00_1399 = BgL_globalsz00_3;
				BgL_newzd2globalszd2_1400 = BNIL;
			BgL_zc3z04anonymousza31312ze3z87_1401:
				if (NULLP(BgL_globalsz00_1399))
					{	/* Inline/walk.scm 65 */
						obj_t BgL_valuez00_1403;

						{	/* Inline/walk.scm 66 */
							obj_t BgL_arg1329z00_1426;

							BgL_arg1329z00_1426 = bgl_reverse_bang(BgL_newzd2globalszd2_1400);
							BgL_valuez00_1403 =
								BGl_removezd2varzd2zzast_removez00(CNST_TABLE_REF(5),
								BgL_arg1329z00_1426);
						}
						if (
							((long) CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
								> 0L))
							{	/* Inline/walk.scm 65 */
								{	/* Inline/walk.scm 65 */
									obj_t BgL_port1237z00_1405;

									{	/* Inline/walk.scm 65 */
										obj_t BgL_tmpz00_1770;

										BgL_tmpz00_1770 = BGL_CURRENT_DYNAMIC_ENV();
										BgL_port1237z00_1405 =
											BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_1770);
									}
									bgl_display_obj
										(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
										BgL_port1237z00_1405);
									bgl_display_string(BGl_string1615z00zzinline_walkz00,
										BgL_port1237z00_1405);
									{	/* Inline/walk.scm 65 */
										obj_t BgL_arg1315z00_1406;

										{	/* Inline/walk.scm 65 */
											bool_t BgL_test1657z00_1775;

											if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
												(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
												{	/* Inline/walk.scm 65 */
													if (INTEGERP
														(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
														{	/* Inline/walk.scm 65 */
															BgL_test1657z00_1775 =
																(
																(long)
																CINT
																(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
																> 1L);
														}
													else
														{	/* Inline/walk.scm 65 */
															BgL_test1657z00_1775 =
																BGl_2ze3ze3zz__r4_numbers_6_5z00
																(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
																BINT(1L));
														}
												}
											else
												{	/* Inline/walk.scm 65 */
													BgL_test1657z00_1775 = ((bool_t) 0);
												}
											if (BgL_test1657z00_1775)
												{	/* Inline/walk.scm 65 */
													BgL_arg1315z00_1406 =
														BGl_string1616z00zzinline_walkz00;
												}
											else
												{	/* Inline/walk.scm 65 */
													BgL_arg1315z00_1406 =
														BGl_string1617z00zzinline_walkz00;
												}
										}
										bgl_display_obj(BgL_arg1315z00_1406, BgL_port1237z00_1405);
									}
									bgl_display_string(BGl_string1618z00zzinline_walkz00,
										BgL_port1237z00_1405);
									bgl_display_char(((unsigned char) 10), BgL_port1237z00_1405);
								}
								{	/* Inline/walk.scm 65 */
									obj_t BgL_list1318z00_1410;

									BgL_list1318z00_1410 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
									return BGl_exitz00zz__errorz00(BgL_list1318z00_1410);
								}
							}
						else
							{	/* Inline/walk.scm 65 */
								obj_t BgL_g1114z00_1411;
								obj_t BgL_g1115z00_1412;

								{	/* Inline/walk.scm 65 */
									obj_t BgL_list1328z00_1425;

									BgL_list1328z00_1425 =
										MAKE_YOUNG_PAIR(BGl_showzd2statz12zd2envz12zzinline_walkz00,
										BNIL);
									BgL_g1114z00_1411 = BgL_list1328z00_1425;
								}
								BgL_g1115z00_1412 = CNST_TABLE_REF(6);
								{
									obj_t BgL_hooksz00_1414;
									obj_t BgL_hnamesz00_1415;

									BgL_hooksz00_1414 = BgL_g1114z00_1411;
									BgL_hnamesz00_1415 = BgL_g1115z00_1412;
								BgL_zc3z04anonymousza31319ze3z87_1416:
									if (NULLP(BgL_hooksz00_1414))
										{	/* Inline/walk.scm 65 */
											return BgL_valuez00_1403;
										}
									else
										{	/* Inline/walk.scm 65 */
											bool_t BgL_test1661z00_1794;

											{	/* Inline/walk.scm 65 */
												obj_t BgL_fun1327z00_1423;

												BgL_fun1327z00_1423 = CAR(((obj_t) BgL_hooksz00_1414));
												BgL_test1661z00_1794 =
													CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1327z00_1423));
											}
											if (BgL_test1661z00_1794)
												{	/* Inline/walk.scm 65 */
													obj_t BgL_arg1323z00_1420;
													obj_t BgL_arg1325z00_1421;

													BgL_arg1323z00_1420 =
														CDR(((obj_t) BgL_hooksz00_1414));
													BgL_arg1325z00_1421 =
														CDR(((obj_t) BgL_hnamesz00_1415));
													{
														obj_t BgL_hnamesz00_1806;
														obj_t BgL_hooksz00_1805;

														BgL_hooksz00_1805 = BgL_arg1323z00_1420;
														BgL_hnamesz00_1806 = BgL_arg1325z00_1421;
														BgL_hnamesz00_1415 = BgL_hnamesz00_1806;
														BgL_hooksz00_1414 = BgL_hooksz00_1805;
														goto BgL_zc3z04anonymousza31319ze3z87_1416;
													}
												}
											else
												{	/* Inline/walk.scm 65 */
													obj_t BgL_arg1326z00_1422;

													BgL_arg1326z00_1422 =
														CAR(((obj_t) BgL_hnamesz00_1415));
													return
														BGl_internalzd2errorzd2zztools_errorz00
														(BGl_za2currentzd2passza2zd2zzengine_passz00,
														BGl_string1619z00zzinline_walkz00,
														BgL_arg1326z00_1422);
												}
										}
								}
							}
					}
				else
					{	/* Inline/walk.scm 67 */
						bool_t BgL_test1663z00_1810;

						{	/* Inline/walk.scm 67 */
							obj_t BgL_arg1343z00_1434;

							BgL_arg1343z00_1434 =
								(((BgL_globalz00_bglt) COBJECT(
										((BgL_globalz00_bglt)
											CAR(((obj_t) BgL_globalsz00_1399)))))->BgL_modulez00);
							BgL_test1663z00_1810 =
								(BgL_arg1343z00_1434 == BGl_za2moduleza2z00zzmodule_modulez00);
						}
						if (BgL_test1663z00_1810)
							{	/* Inline/walk.scm 68 */
								obj_t BgL_arg1335z00_1430;
								obj_t BgL_arg1339z00_1431;

								BgL_arg1335z00_1430 = CDR(((obj_t) BgL_globalsz00_1399));
								{	/* Inline/walk.scm 68 */
									obj_t BgL_arg1340z00_1432;

									BgL_arg1340z00_1432 = CAR(((obj_t) BgL_globalsz00_1399));
									BgL_arg1339z00_1431 =
										MAKE_YOUNG_PAIR(BgL_arg1340z00_1432,
										BgL_newzd2globalszd2_1400);
								}
								{
									obj_t BgL_newzd2globalszd2_1822;
									obj_t BgL_globalsz00_1821;

									BgL_globalsz00_1821 = BgL_arg1335z00_1430;
									BgL_newzd2globalszd2_1822 = BgL_arg1339z00_1431;
									BgL_newzd2globalszd2_1400 = BgL_newzd2globalszd2_1822;
									BgL_globalsz00_1399 = BgL_globalsz00_1821;
									goto BgL_zc3z04anonymousza31312ze3z87_1401;
								}
							}
						else
							{	/* Inline/walk.scm 70 */
								obj_t BgL_arg1342z00_1433;

								BgL_arg1342z00_1433 = CDR(((obj_t) BgL_globalsz00_1399));
								{
									obj_t BgL_globalsz00_1825;

									BgL_globalsz00_1825 = BgL_arg1342z00_1433;
									BgL_globalsz00_1399 = BgL_globalsz00_1825;
									goto BgL_zc3z04anonymousza31312ze3z87_1401;
								}
							}
					}
			}
		}

	}



/* &inline-walk! */
	obj_t BGl_z62inlinezd2walkz12za2zzinline_walkz00(obj_t BgL_envz00_1642,
		obj_t BgL_globalsz00_1643, obj_t BgL_whatz00_1644)
	{
		{	/* Inline/walk.scm 41 */
			return
				BGl_inlinezd2walkz12zc0zzinline_walkz00(BgL_globalsz00_1643,
				BgL_whatz00_1644);
		}

	}



/* &reset-stat! */
	obj_t BGl_z62resetzd2statz12za2zzinline_walkz00(obj_t BgL_envz00_1645)
	{
		{	/* Inline/walk.scm 81 */
			BGl_za2inlinedzd2callsza2zd2zzinline_walkz00 = BINT(0L);
			return (BGl_za2nonzd2inlinedzd2callsza2z00zzinline_walkz00 =
				BINT(0L), BUNSPEC);
		}

	}



/* &show-stat! */
	obj_t BGl_z62showzd2statz12za2zzinline_walkz00(obj_t BgL_envz00_1646)
	{
		{	/* Inline/walk.scm 88 */
			{	/* Inline/walk.scm 89 */
				bool_t BgL_tmpz00_1829;

				{	/* Inline/walk.scm 89 */
					obj_t BgL_list1347z00_1660;

					{	/* Inline/walk.scm 89 */
						obj_t BgL_arg1348z00_1661;

						{	/* Inline/walk.scm 89 */
							obj_t BgL_arg1349z00_1662;

							BgL_arg1349z00_1662 =
								MAKE_YOUNG_PAIR(BGl_string1620z00zzinline_walkz00, BNIL);
							BgL_arg1348z00_1661 =
								MAKE_YOUNG_PAIR(BGl_za2inlinedzd2callsza2zd2zzinline_walkz00,
								BgL_arg1349z00_1662);
						}
						BgL_list1347z00_1660 =
							MAKE_YOUNG_PAIR(BGl_string1621z00zzinline_walkz00,
							BgL_arg1348z00_1661);
					}
					BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list1347z00_1660);
				}
				{	/* Inline/walk.scm 90 */
					obj_t BgL_list1350z00_1663;

					{	/* Inline/walk.scm 90 */
						obj_t BgL_arg1351z00_1664;

						{	/* Inline/walk.scm 90 */
							obj_t BgL_arg1352z00_1665;

							BgL_arg1352z00_1665 =
								MAKE_YOUNG_PAIR(BGl_string1620z00zzinline_walkz00, BNIL);
							BgL_arg1351z00_1664 =
								MAKE_YOUNG_PAIR
								(BGl_za2nonzd2inlinedzd2callsza2z00zzinline_walkz00,
								BgL_arg1352z00_1665);
						}
						BgL_list1350z00_1663 =
							MAKE_YOUNG_PAIR(BGl_string1622z00zzinline_walkz00,
							BgL_arg1351z00_1664);
					}
					BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list1350z00_1663);
				}
				BgL_tmpz00_1829 = ((bool_t) 1);
				return BBOOL(BgL_tmpz00_1829);
			}
		}

	}



/* inline-setup! */
	BGL_EXPORTED_DEF obj_t BGl_inlinezd2setupz12zc0zzinline_walkz00(obj_t
		BgL_whatz00_5)
	{
		{	/* Inline/walk.scm 96 */
			if ((BgL_whatz00_5 == CNST_TABLE_REF(0)))
				{	/* Inline/walk.scm 98 */
					BGl_za2inlinezd2modeza2zd2zzinline_walkz00 = CNST_TABLE_REF(0);
					{	/* Inline/walk.scm 101 */
						obj_t BgL_yz00_1446;

						BgL_yz00_1446 =
							BGL_PROCEDURE_CALL1
							(BGl_za2inliningzd2kfactorza2zd2zzengine_paramz00,
							BGl_za2optimza2z00zzengine_paramz00);
						return (BGl_za2kfactorza2z00zzinline_walkz00 =
							BGl_2maxz00zz__r4_numbers_6_5z00(BINT(1L), BgL_yz00_1446),
							BUNSPEC);
					}
				}
			else
				{	/* Inline/walk.scm 98 */
					if ((BgL_whatz00_5 == CNST_TABLE_REF(7)))
						{	/* Inline/walk.scm 98 */
							BGl_za2inlinezd2modeza2zd2zzinline_walkz00 = CNST_TABLE_REF(7);
							return (BGl_za2kfactorza2z00zzinline_walkz00 = BINT(1L), BUNSPEC);
						}
					else
						{	/* Inline/walk.scm 98 */
							if ((BgL_whatz00_5 == CNST_TABLE_REF(8)))
								{	/* Inline/walk.scm 98 */
									BGl_za2inlinezd2modeza2zd2zzinline_walkz00 =
										CNST_TABLE_REF(8);
									return (BGl_za2kfactorza2z00zzinline_walkz00 =
										BINT(1L), BUNSPEC);
								}
							else
								{	/* Inline/walk.scm 98 */
									return
										BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string1623z00zzinline_walkz00,
										BGl_string1624z00zzinline_walkz00, BgL_whatz00_5);
								}
						}
				}
		}

	}



/* &inline-setup! */
	obj_t BGl_z62inlinezd2setupz12za2zzinline_walkz00(obj_t BgL_envz00_1647,
		obj_t BgL_whatz00_1648)
	{
		{	/* Inline/walk.scm 96 */
			return BGl_inlinezd2setupz12zc0zzinline_walkz00(BgL_whatz00_1648);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzinline_walkz00(void)
	{
		{	/* Inline/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzinline_walkz00(void)
	{
		{	/* Inline/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzinline_walkz00(void)
	{
		{	/* Inline/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzinline_walkz00(void)
	{
		{	/* Inline/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1625z00zzinline_walkz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1625z00zzinline_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1625z00zzinline_walkz00));
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1625z00zzinline_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1625z00zzinline_walkz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1625z00zzinline_walkz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1625z00zzinline_walkz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1625z00zzinline_walkz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1625z00zzinline_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1625z00zzinline_walkz00));
			BGl_modulezd2initializa7ationz75zzast_removez00(383247627L,
				BSTRING_TO_STRING(BGl_string1625z00zzinline_walkz00));
			BGl_modulezd2initializa7ationz75zzast_occurz00(282085879L,
				BSTRING_TO_STRING(BGl_string1625z00zzinline_walkz00));
			return
				BGl_modulezd2initializa7ationz75zzinline_inlinez00(20504962L,
				BSTRING_TO_STRING(BGl_string1625z00zzinline_walkz00));
		}

	}

#ifdef __cplusplus
}
#endif
